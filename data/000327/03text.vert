<s>
Antarktida	Antarktida	k1gFnSc1	Antarktida
(	(	kIx(	(
<g/>
název	název	k1gInSc1	název
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
naproti	naproti	k7c3	naproti
Arktidě	Arktida	k1gFnSc3	Arktida
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
přeneseném	přenesený	k2eAgInSc6d1	přenesený
významu	význam	k1gInSc6	význam
"	"	kIx"	"
<g/>
naproti	naproti	k7c3	naproti
severu	sever	k1gInSc3	sever
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
největší	veliký	k2eAgInSc4d3	veliký
kontinent	kontinent	k1gInSc4	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
jižní	jižní	k2eAgFnSc1d1	jižní
polární	polární	k2eAgFnSc1d1	polární
oblast	oblast	k1gFnSc1	oblast
rozprostírající	rozprostírající	k2eAgFnSc1d1	rozprostírající
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Krajina	Krajina	k1gFnSc1	Krajina
i	i	k8xC	i
podnebí	podnebí	k1gNnSc1	podnebí
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Antarktidě	Antarktida	k1gFnSc6	Antarktida
natolik	natolik	k6eAd1	natolik
drsné	drsný	k2eAgNnSc1d1	drsné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
žádní	žádný	k3yNgMnPc1	žádný
lidé	člověk	k1gMnPc1	člověk
nikdy	nikdy	k6eAd1	nikdy
neusadili	usadit	k5eNaPmAgMnP	usadit
natrvalo	natrvalo	k6eAd1	natrvalo
<g/>
.	.	kIx.	.
</s>
<s>
Jedinými	jediný	k2eAgMnPc7d1	jediný
obyvateli	obyvatel	k1gMnPc7	obyvatel
tohoto	tento	k3xDgInSc2	tento
kontinentu	kontinent	k1gInSc2	kontinent
jsou	být	k5eAaImIp3nP	být
vědci	vědec	k1gMnPc1	vědec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
tu	tu	k6eAd1	tu
pracují	pracovat	k5eAaImIp3nP	pracovat
na	na	k7c6	na
výzkumných	výzkumný	k2eAgInPc6d1	výzkumný
projektech	projekt	k1gInPc6	projekt
<g/>
.	.	kIx.	.
</s>
<s>
Antarktický	antarktický	k2eAgInSc1d1	antarktický
smluvní	smluvní	k2eAgInSc1d1	smluvní
systém	systém	k1gInSc1	systém
zmrazuje	zmrazovat	k5eAaImIp3nS	zmrazovat
nároky	nárok	k1gInPc4	nárok
států	stát	k1gInPc2	stát
na	na	k7c4	na
území	území	k1gNnSc4	území
Antarktidy	Antarktida	k1gFnSc2	Antarktida
<g/>
,	,	kIx,	,
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
jakékoliv	jakýkoliv	k3yIgFnPc4	jakýkoliv
vojenské	vojenský	k2eAgFnPc4d1	vojenská
aktivity	aktivita	k1gFnPc4	aktivita
a	a	k8xC	a
také	také	k9	také
například	například	k6eAd1	například
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
těžbu	těžba	k1gFnSc4	těžba
nerostných	nerostný	k2eAgFnPc2d1	nerostná
surovin	surovina	k1gFnPc2	surovina
(	(	kIx(	(
<g/>
minimálně	minimálně	k6eAd1	minimálně
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2048	[number]	k4	2048
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2012	[number]	k4	2012
vzneslo	vznést	k5eAaPmAgNnS	vznést
územní	územní	k2eAgInPc4d1	územní
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
Antarktidu	Antarktida	k1gFnSc4	Antarktida
7	[number]	k4	7
různých	různý	k2eAgInPc2d1	různý
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
požadavky	požadavek	k1gInPc1	požadavek
jsou	být	k5eAaImIp3nP	být
díky	díky	k7c3	díky
Smlouvě	smlouva	k1gFnSc3	smlouva
o	o	k7c6	o
Antarktidě	Antarktida	k1gFnSc6	Antarktida
pozastaveny	pozastaven	k2eAgInPc1d1	pozastaven
<g/>
.	.	kIx.	.
</s>
<s>
Antarktida	Antarktida	k1gFnSc1	Antarktida
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
Jižním	jižní	k2eAgNnSc6d1	jižní
pólu	pólo	k1gNnSc6	pólo
<g/>
.	.	kIx.	.
</s>
<s>
Nemá	mít	k5eNaImIp3nS	mít
tedy	tedy	k9	tedy
nejzápadnější	západní	k2eAgInSc1d3	nejzápadnější
ani	ani	k8xC	ani
nejvýchodnější	východní	k2eAgInSc1d3	nejvýchodnější
bod	bod	k1gInSc1	bod
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
na	na	k7c6	na
pólu	pólo	k1gNnSc6	pólo
se	se	k3xPyFc4	se
sbíhají	sbíhat	k5eAaImIp3nP	sbíhat
všechny	všechen	k3xTgInPc1	všechen
poledníky	poledník	k1gInPc1	poledník
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ostatních	ostatní	k2eAgInPc2d1	ostatní
kontinentů	kontinent	k1gInPc2	kontinent
se	se	k3xPyFc4	se
nejvíce	nejvíce	k6eAd1	nejvíce
přibližuje	přibližovat	k5eAaImIp3nS	přibližovat
Jižní	jižní	k2eAgFnSc3d1	jižní
Americe	Amerika	k1gFnSc3	Amerika
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
od	od	k7c2	od
Antarktického	antarktický	k2eAgInSc2d1	antarktický
poloostrova	poloostrov	k1gInSc2	poloostrov
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
asi	asi	k9	asi
1000	[number]	k4	1000
km	km	kA	km
široký	široký	k2eAgInSc1d1	široký
Drakeův	Drakeův	k2eAgInSc1d1	Drakeův
průliv	průliv	k1gInSc1	průliv
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Antarktidě	Antarktida	k1gFnSc3	Antarktida
lze	lze	k6eAd1	lze
přiřadit	přiřadit	k5eAaPmF	přiřadit
některé	některý	k3yIgInPc4	některý
vesměs	vesměs	k6eAd1	vesměs
neobydlené	obydlený	k2eNgInPc4d1	neobydlený
ostrovy	ostrov	k1gInPc4	ostrov
v	v	k7c6	v
Jižním	jižní	k2eAgNnSc6d1	jižní
<g/>
,	,	kIx,	,
Atlantském	atlantský	k2eAgNnSc6d1	Atlantské
<g/>
,	,	kIx,	,
Indickém	indický	k2eAgInSc6d1	indický
a	a	k8xC	a
Tichém	tichý	k2eAgInSc6d1	tichý
oceánu	oceán	k1gInSc6	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Nejzazší	zadní	k2eAgInPc1d3	nejzazší
body	bod	k1gInPc1	bod
na	na	k7c6	na
pevnině	pevnina	k1gFnSc6	pevnina
<g/>
:	:	kIx,	:
Nejsevernější	severní	k2eAgInSc1d3	nejsevernější
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
Brown	Brown	k1gMnSc1	Brown
Bluff	Bluff	k1gMnSc1	Bluff
na	na	k7c6	na
Antarktickém	antarktický	k2eAgInSc6d1	antarktický
poloostrově	poloostrov	k1gInSc6	poloostrov
(	(	kIx(	(
<g/>
63	[number]	k4	63
<g/>
°	°	k?	°
<g/>
13	[number]	k4	13
<g/>
'	'	kIx"	'
j.š.	j.š.	k?	j.š.
<g/>
)	)	kIx)	)
Nejjižnější	jižní	k2eAgInSc1d3	nejjižnější
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
Jižní	jižní	k2eAgInSc1d1	jižní
pól	pól	k1gInSc1	pól
(	(	kIx(	(
<g/>
90	[number]	k4	90
<g/>
°	°	k?	°
j.š.	j.š.	k?	j.š.
<g/>
)	)	kIx)	)
Nejzazší	zadní	k2eAgInPc4d3	nejzazší
body	bod	k1gInPc4	bod
včetně	včetně	k7c2	včetně
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
:	:	kIx,	:
Nejsevernější	severní	k2eAgInSc1d3	nejsevernější
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
Crozetovy	Crozetův	k2eAgInPc1d1	Crozetův
ostrovy	ostrov	k1gInPc1	ostrov
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
Francie	Francie	k1gFnSc2	Francie
(	(	kIx(	(
<g/>
45	[number]	k4	45
<g/>
°	°	k?	°
<g/>
57	[number]	k4	57
<g/>
'	'	kIx"	'
j.š.	j.š.	k?	j.š.
<g/>
)	)	kIx)	)
Nejjižnější	jižní	k2eAgInSc1d3	nejjižnější
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
Jižní	jižní	k2eAgInSc1d1	jižní
pól	pól	k1gInSc1	pól
(	(	kIx(	(
<g/>
90	[number]	k4	90
<g/>
°	°	k?	°
j.š.	j.š.	k?	j.š.
<g/>
)	)	kIx)	)
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc4	dějiny
Antarktidy	Antarktida	k1gFnSc2	Antarktida
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
desítkami	desítka	k1gFnPc7	desítka
miliónů	milión	k4xCgInPc2	milión
let	léto	k1gNnPc2	léto
v	v	k7c6	v
Antarktidě	Antarktida	k1gFnSc6	Antarktida
nebyla	být	k5eNaImAgFnS	být
ledová	ledový	k2eAgFnSc1d1	ledová
pokrývka	pokrývka	k1gFnSc1	pokrývka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lesy	les	k1gInPc1	les
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
led	led	k1gInSc1	led
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
i	i	k9	i
před	před	k7c7	před
14	[number]	k4	14
milióny	milión	k4xCgInPc7	milión
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgInSc1d1	jižní
polární	polární	k2eAgInSc1d1	polární
kruh	kruh	k1gInSc1	kruh
jako	jako	k9	jako
první	první	k4xOgMnPc1	první
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
překročili	překročit	k5eAaPmAgMnP	překročit
novozélandští	novozélandský	k2eAgMnPc1d1	novozélandský
Maorové	Maor	k1gMnPc1	Maor
<g/>
.	.	kIx.	.
</s>
<s>
Anglický	anglický	k2eAgMnSc1d1	anglický
mořeplavec	mořeplavec	k1gMnSc1	mořeplavec
James	James	k1gMnSc1	James
Cook	Cook	k1gMnSc1	Cook
roku	rok	k1gInSc2	rok
1773	[number]	k4	1773
zavítal	zavítat	k5eAaPmAgInS	zavítat
na	na	k7c4	na
ledovou	ledový	k2eAgFnSc4d1	ledová
návrš	návrš	k1gFnSc4	návrš
<g/>
,	,	kIx,	,
obklopující	obklopující	k2eAgInSc4d1	obklopující
kontinent	kontinent	k1gInSc4	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
následovaly	následovat	k5eAaImAgFnP	následovat
britské	britský	k2eAgFnPc1d1	britská
<g/>
,	,	kIx,	,
ruské	ruský	k2eAgFnPc1d1	ruská
a	a	k8xC	a
francouzské	francouzský	k2eAgFnPc1d1	francouzská
expedice	expedice	k1gFnPc1	expedice
a	a	k8xC	a
také	také	k9	také
lovci	lovec	k1gMnPc1	lovec
tuleňů	tuleň	k1gMnPc2	tuleň
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
bylo	být	k5eAaImAgNnS	být
vnitrozemí	vnitrozemí	k1gNnSc1	vnitrozemí
Antarktidy	Antarktida	k1gFnSc2	Antarktida
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
posledních	poslední	k2eAgNnPc2d1	poslední
dosud	dosud	k6eAd1	dosud
neprozkoumaných	prozkoumaný	k2eNgNnPc2d1	neprozkoumané
míst	místo	k1gNnPc2	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
Falcon	Falcon	k1gMnSc1	Falcon
Scott	Scott	k1gMnSc1	Scott
sem	sem	k6eAd1	sem
vedl	vést	k5eAaImAgMnS	vést
výpravu	výprava	k1gFnSc4	výprava
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1900	[number]	k4	1900
až	až	k9	až
1904	[number]	k4	1904
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1909	[number]	k4	1909
se	se	k3xPyFc4	se
Ernest	Ernest	k1gMnSc1	Ernest
Shackleton	Shackleton	k1gInSc4	Shackleton
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
150	[number]	k4	150
km	km	kA	km
od	od	k7c2	od
jižního	jižní	k2eAgInSc2d1	jižní
pólu	pól	k1gInSc2	pól
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
nakonec	nakonec	k6eAd1	nakonec
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
pólu	pól	k1gInSc3	pól
Nor	Nor	k1gMnSc1	Nor
Roald	Roald	k1gMnSc1	Roald
Amundsen	Amundsno	k1gNnPc2	Amundsno
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
měsíc	měsíc	k1gInSc4	měsíc
později	pozdě	k6eAd2	pozdě
sem	sem	k6eAd1	sem
dorazila	dorazit	k5eAaPmAgFnS	dorazit
další	další	k2eAgFnSc1d1	další
expedice	expedice	k1gFnSc1	expedice
<g/>
,	,	kIx,	,
vedená	vedený	k2eAgFnSc1d1	vedená
Robert	Robert	k1gMnSc1	Robert
Falcon	Falcon	k1gMnSc1	Falcon
Scottem	Scott	k1gMnSc7	Scott
<g/>
,	,	kIx,	,
všichni	všechen	k3xTgMnPc1	všechen
její	její	k3xOp3gMnPc1	její
členové	člen	k1gMnPc1	člen
však	však	k9	však
na	na	k7c6	na
zpáteční	zpáteční	k2eAgFnSc6d1	zpáteční
cestě	cesta	k1gFnSc6	cesta
zahynuli	zahynout	k5eAaPmAgMnP	zahynout
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgFnSc2d2	pozdější
výpravy	výprava	k1gFnSc2	výprava
už	už	k6eAd1	už
používaly	používat	k5eAaImAgInP	používat
ledoborce	ledoborec	k1gInPc1	ledoborec
<g/>
,	,	kIx,	,
letadla	letadlo	k1gNnPc1	letadlo
a	a	k8xC	a
pásová	pásový	k2eAgNnPc4d1	pásové
vozidla	vozidlo	k1gNnPc4	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Antarktida	Antarktida	k1gFnSc1	Antarktida
nepatří	patřit	k5eNaImIp3nS	patřit
žádnému	žádný	k3yNgInSc3	žádný
státu	stát	k1gInSc3	stát
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
však	však	k9	však
bohaté	bohatý	k2eAgFnPc4d1	bohatá
zásoby	zásoba	k1gFnPc4	zásoba
nerostů	nerost	k1gInPc2	nerost
a	a	k8xC	a
ryb	ryba	k1gFnPc2	ryba
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
si	se	k3xPyFc3	se
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
různé	různý	k2eAgFnPc1d1	různá
země	zem	k1gFnPc1	zem
dělaly	dělat	k5eAaImAgFnP	dělat
nárok	nárok	k1gInSc4	nárok
<g/>
.	.	kIx.	.
</s>
<s>
Dvanáct	dvanáct	k4xCc1	dvanáct
států	stát	k1gInPc2	stát
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
smlouvu	smlouva	k1gFnSc4	smlouva
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
vědeckého	vědecký	k2eAgInSc2d1	vědecký
výzkumu	výzkum	k1gInSc2	výzkum
a	a	k8xC	a
míru	mír	k1gInSc2	mír
na	na	k7c6	na
Antarktidě	Antarktida	k1gFnSc6	Antarktida
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
smlouvu	smlouva	k1gFnSc4	smlouva
dosud	dosud	k6eAd1	dosud
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
osmatřicet	osmatřicet	k4xCc1	osmatřicet
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
úsilí	úsilí	k1gNnSc2	úsilí
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
si	se	k3xPyFc3	se
klade	klást	k5eAaImIp3nS	klást
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
uchovat	uchovat	k5eAaPmF	uchovat
tuto	tento	k3xDgFnSc4	tento
divočinu	divočina	k1gFnSc4	divočina
neporušenou	porušený	k2eNgFnSc7d1	neporušená
činností	činnost	k1gFnSc7	činnost
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
50	[number]	k4	50
let	léto	k1gNnPc2	léto
zakázána	zakázán	k2eAgFnSc1d1	zakázána
těžba	těžba	k1gFnSc1	těžba
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Antarktidy	Antarktida	k1gFnSc2	Antarktida
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Územní	územní	k2eAgInPc1d1	územní
nároky	nárok	k1gInPc1	nárok
na	na	k7c4	na
Antarktidu	Antarktida	k1gFnSc4	Antarktida
<g/>
.	.	kIx.	.
</s>
<s>
Antarktida	Antarktida	k1gFnSc1	Antarktida
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
13	[number]	k4	13
829	[number]	k4	829
430	[number]	k4	430
km2	km2	k4	km2
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
včetně	včetně	k7c2	včetně
šelfových	šelfový	k2eAgInPc2d1	šelfový
ledovců	ledovec	k1gInPc2	ledovec
a	a	k8xC	a
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
bez	bez	k7c2	bez
nich	on	k3xPp3gMnPc2	on
činí	činit	k5eAaImIp3nS	činit
12	[number]	k4	12
272	[number]	k4	272
800	[number]	k4	800
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
je	být	k5eAaImIp3nS	být
Vinson	Vinson	k1gMnSc1	Vinson
Massif	Massif	k1gMnSc1	Massif
v	v	k7c6	v
Ellsworthově	Ellsworthův	k2eAgNnSc6d1	Ellsworthovo
pohoří	pohoří	k1gNnSc6	pohoří
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
4892	[number]	k4	4892
m.	m.	k?	m.
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
kontinentu	kontinent	k1gInSc2	kontinent
činí	činit	k5eAaImIp3nS	činit
1958	[number]	k4	1958
m	m	kA	m
včetně	včetně	k7c2	včetně
šelfových	šelfový	k2eAgInPc2d1	šelfový
ledovců	ledovec	k1gInPc2	ledovec
a	a	k8xC	a
2194	[number]	k4	2194
m	m	kA	m
bez	bez	k7c2	bez
nich	on	k3xPp3gFnPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Antarktida	Antarktida	k1gFnSc1	Antarktida
je	být	k5eAaImIp3nS	být
pátým	pátý	k4xOgInSc7	pátý
největším	veliký	k2eAgInSc7d3	veliký
světadílem	světadíl	k1gInSc7	světadíl
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
a	a	k8xC	a
je	on	k3xPp3gFnPc4	on
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
nejchladnější	chladný	k2eAgFnSc1d3	nejchladnější
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
zaznamenaná	zaznamenaný	k2eAgFnSc1d1	zaznamenaná
teplota	teplota	k1gFnSc1	teplota
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
-	-	kIx~	-
<g/>
89,2	[number]	k4	89,2
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
naměřena	naměřit	k5eAaBmNgFnS	naměřit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
na	na	k7c6	na
ruské	ruský	k2eAgFnSc6d1	ruská
vědecké	vědecký	k2eAgFnSc6d1	vědecká
základně	základna	k1gFnSc6	základna
Vostok	Vostok	k1gInSc1	Vostok
<g/>
.	.	kIx.	.
</s>
<s>
Objem	objem	k1gInSc1	objem
ledu	led	k1gInSc2	led
na	na	k7c6	na
Antarktidě	Antarktida	k1gFnSc6	Antarktida
je	být	k5eAaImIp3nS	být
25,4	[number]	k4	25,4
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
km	km	kA	km
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Jediná	jediný	k2eAgNnPc1d1	jediné
území	území	k1gNnPc1	území
bez	bez	k7c2	bez
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
lze	lze	k6eAd1	lze
v	v	k7c6	v
Antarktidě	Antarktida	k1gFnSc6	Antarktida
spatřit	spatřit	k5eAaPmF	spatřit
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
několik	několik	k4yIc4	několik
horských	horský	k2eAgInPc2d1	horský
vrcholů	vrchol	k1gInPc2	vrchol
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
pusté	pustý	k2eAgFnPc4d1	pustá
<g/>
,	,	kIx,	,
skalnaté	skalnatý	k2eAgFnPc4d1	skalnatá
oblasti	oblast	k1gFnPc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Ledový	ledový	k2eAgInSc1d1	ledový
příkop	příkop	k1gInSc1	příkop
podél	podél	k7c2	podél
jižního	jižní	k2eAgNnSc2d1	jižní
pobřeží	pobřeží	k1gNnSc2	pobřeží
během	během	k7c2	během
léta	léto	k1gNnSc2	léto
taje	taj	k1gInSc2	taj
a	a	k8xC	a
odlamují	odlamovat	k5eAaImIp3nP	odlamovat
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
velké	velký	k2eAgInPc1d1	velký
kusy	kus	k1gInPc1	kus
<g/>
,	,	kIx,	,
ledové	ledový	k2eAgFnPc1d1	ledová
kry	kra	k1gFnPc1	kra
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
kry	kra	k1gFnPc1	kra
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
až	až	k9	až
60	[number]	k4	60
metrů	metr	k1gInPc2	metr
vysoké	vysoká	k1gFnSc2	vysoká
a	a	k8xC	a
mnoho	mnoho	k4c4	mnoho
kilometrů	kilometr	k1gInPc2	kilometr
dlouhé	dlouhý	k2eAgInPc1d1	dlouhý
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
lodě	loď	k1gFnPc4	loď
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
je	on	k3xPp3gNnSc4	on
míjejí	míjet	k5eAaImIp3nP	míjet
<g/>
,	,	kIx,	,
představují	představovat	k5eAaImIp3nP	představovat
nesmírné	smírný	k2eNgNnSc4d1	nesmírné
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
mocnost	mocnost	k1gFnSc1	mocnost
antarktického	antarktický	k2eAgInSc2d1	antarktický
ledovce	ledovec	k1gInSc2	ledovec
je	být	k5eAaImIp3nS	být
4776	[number]	k4	4776
m	m	kA	m
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
průměrná	průměrný	k2eAgFnSc1d1	průměrná
mocnost	mocnost	k1gFnSc1	mocnost
činí	činit	k5eAaImIp3nS	činit
1829	[number]	k4	1829
m.	m.	k?	m.
Napříč	napříč	k7c7	napříč
tímto	tento	k3xDgInSc7	tento
ledovým	ledový	k2eAgInSc7d1	ledový
kontinentem	kontinent	k1gInSc7	kontinent
se	se	k3xPyFc4	se
táhne	táhnout	k5eAaImIp3nS	táhnout
Transantarktické	Transantarktický	k2eAgNnSc4d1	Transantarktické
pohoří	pohoří	k1gNnSc4	pohoří
a	a	k8xC	a
dělí	dělit	k5eAaImIp3nP	dělit
jej	on	k3xPp3gMnSc4	on
na	na	k7c4	na
západní	západní	k2eAgFnSc4d1	západní
a	a	k8xC	a
východní	východní	k2eAgFnSc4d1	východní
část	část	k1gFnSc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Dělení	dělení	k1gNnSc1	dělení
Antarktidy	Antarktida	k1gFnSc2	Antarktida
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
a	a	k8xC	a
západní	západní	k2eAgFnSc6d1	západní
zavedl	zavést	k5eAaPmAgInS	zavést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
E.	E.	kA	E.
S.	S.	kA	S.
Balch	Balch	k1gMnSc1	Balch
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnSc1d1	východní
Antarktida	Antarktida	k1gFnSc1	Antarktida
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
Zemi	zem	k1gFnSc4	zem
královny	královna	k1gFnSc2	královna
Maud	Maudo	k1gNnPc2	Maudo
<g/>
,	,	kIx,	,
Enderbyho	Enderby	k1gMnSc4	Enderby
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
zemi	zem	k1gFnSc6	zem
Mac	Mac	kA	Mac
Robertsona	Robertson	k1gMnSc2	Robertson
<g/>
,	,	kIx,	,
Americkou	americký	k2eAgFnSc4d1	americká
vysočinu	vysočina	k1gFnSc4	vysočina
<g/>
,	,	kIx,	,
Wilkesovu	Wilkesův	k2eAgFnSc4d1	Wilkesova
zemi	zem	k1gFnSc4	zem
a	a	k8xC	a
Viktoriinu	Viktoriin	k2eAgFnSc4d1	Viktoriina
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Skalní	skalní	k2eAgInSc1d1	skalní
podklad	podklad	k1gInSc1	podklad
východní	východní	k2eAgFnSc2d1	východní
Antarktidy	Antarktida	k1gFnSc2	Antarktida
leží	ležet	k5eAaImIp3nS	ležet
převážně	převážně	k6eAd1	převážně
nad	nad	k7c7	nad
hladinou	hladina	k1gFnSc7	hladina
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
souvislý	souvislý	k2eAgInSc4d1	souvislý
kontinent	kontinent	k1gInSc4	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
celá	celý	k2eAgFnSc1d1	celá
je	být	k5eAaImIp3nS	být
překryta	překrýt	k5eAaPmNgFnS	překrýt
Východoantarktickým	Východoantarktický	k2eAgInSc7d1	Východoantarktický
ledovcovým	ledovcový	k2eAgInSc7d1	ledovcový
štítem	štít	k1gInSc7	štít
(	(	kIx(	(
<g/>
EAIS	EAIS	kA	EAIS
<g/>
)	)	kIx)	)
o	o	k7c6	o
průměrné	průměrný	k2eAgFnSc6d1	průměrná
mocnosti	mocnost	k1gFnSc6	mocnost
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2200	[number]	k4	2200
m.	m.	k?	m.
Na	na	k7c6	na
východě	východ	k1gInSc6	východ
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
vysoká	vysoký	k2eAgFnSc1d1	vysoká
náhorní	náhorní	k2eAgFnSc1d1	náhorní
plošina	plošina	k1gFnSc1	plošina
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
led	led	k1gInSc1	led
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
tvoří	tvořit	k5eAaImIp3nS	tvořit
shluk	shluk	k1gInSc1	shluk
hornatých	hornatý	k2eAgInPc2d1	hornatý
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
spojených	spojený	k2eAgMnPc2d1	spojený
ledem	led	k1gInSc7	led
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tu	tu	k6eAd1	tu
několik	několik	k4yIc4	několik
sopek	sopka	k1gFnPc2	sopka
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
nejaktivnější	aktivní	k2eAgFnSc2d3	nejaktivnější
sopky	sopka	k1gFnSc2	sopka
celého	celý	k2eAgInSc2d1	celý
kontinentu	kontinent	k1gInSc2	kontinent
Mount	Mount	k1gMnSc1	Mount
Erebus	Erebus	k1gMnSc1	Erebus
<g/>
.	.	kIx.	.
</s>
<s>
Antarktida	Antarktida	k1gFnSc1	Antarktida
je	být	k5eAaImIp3nS	být
nejchladnější	chladný	k2eAgInSc4d3	nejchladnější
kontinent	kontinent	k1gInSc4	kontinent
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
drtivé	drtivý	k2eAgFnSc6d1	drtivá
většině	většina	k1gFnSc6	většina
Antarktidy	Antarktida	k1gFnSc2	Antarktida
je	být	k5eAaImIp3nS	být
ledové	ledový	k2eAgNnSc1d1	ledové
podnebí	podnebí	k1gNnSc1	podnebí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
extrémně	extrémně	k6eAd1	extrémně
studené	studený	k2eAgNnSc1d1	studené
<g/>
,	,	kIx,	,
v	v	k7c6	v
centrálních	centrální	k2eAgFnPc6d1	centrální
oblastech	oblast	k1gFnPc6	oblast
navíc	navíc	k6eAd1	navíc
extrémně	extrémně	k6eAd1	extrémně
suché	suchý	k2eAgInPc1d1	suchý
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
spadne	spadnout	k5eAaPmIp3nS	spadnout
pod	pod	k7c4	pod
50	[number]	k4	50
mm	mm	kA	mm
srážek	srážka	k1gFnPc2	srážka
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
centrálních	centrální	k2eAgFnPc6d1	centrální
částech	část	k1gFnPc6	část
Antarktidy	Antarktida	k1gFnSc2	Antarktida
je	být	k5eAaImIp3nS	být
stálá	stálý	k2eAgFnSc1d1	stálá
anticyklonální	anticyklonální	k2eAgFnSc1d1	anticyklonální
situace	situace	k1gFnSc1	situace
s	s	k7c7	s
téměř	téměř	k6eAd1	téměř
nepřetržitou	přetržitý	k2eNgFnSc7d1	nepřetržitá
bezoblačnou	bezoblačný	k2eAgFnSc7d1	bezoblačná
oblohou	obloha	k1gFnSc7	obloha
a	a	k8xC	a
srážkami	srážka	k1gFnPc7	srážka
pouze	pouze	k6eAd1	pouze
kolem	kolem	k7c2	kolem
několika	několik	k4yIc2	několik
mm	mm	kA	mm
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
však	však	k9	však
o	o	k7c4	o
pevné	pevný	k2eAgFnPc4d1	pevná
srážky	srážka	k1gFnPc4	srážka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
sníh	sníh	k1gInSc4	sníh
přivátý	přivátý	k2eAgInSc4d1	přivátý
větrem	vítr	k1gInSc7	vítr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
centrální	centrální	k2eAgFnSc6d1	centrální
plošině	plošina	k1gFnSc6	plošina
Východní	východní	k2eAgFnSc2d1	východní
Antarktidy	Antarktida	k1gFnSc2	Antarktida
byla	být	k5eAaImAgFnS	být
ve	v	k7c4	v
stanici	stanice	k1gFnSc4	stanice
Vostok	Vostok	k1gInSc4	Vostok
dne	den	k1gInSc2	den
21	[number]	k4	21
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1983	[number]	k4	1983
naměřena	naměřen	k2eAgFnSc1d1	naměřena
absolutně	absolutně	k6eAd1	absolutně
nejnižší	nízký	k2eAgFnSc1d3	nejnižší
teplota	teplota	k1gFnSc1	teplota
na	na	k7c6	na
zeměkouli	zeměkoule	k1gFnSc6	zeměkoule
-	-	kIx~	-
<g/>
89,2	[number]	k4	89,2
<g/>
°	°	k?	°
<g/>
C.	C.	kA	C.
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
teplota	teplota	k1gFnSc1	teplota
na	na	k7c6	na
Antarktidě	Antarktida	k1gFnSc6	Antarktida
byla	být	k5eAaImAgFnS	být
naměřená	naměřený	k2eAgFnSc1d1	naměřená
na	na	k7c6	na
české	český	k2eAgFnSc6d1	Česká
polární	polární	k2eAgFnSc6d1	polární
stanici	stanice	k1gFnSc6	stanice
Mendel	Mendel	k1gMnSc1	Mendel
dne	den	k1gInSc2	den
23.3	[number]	k4	23.3
<g/>
.2015	.2015	k4	.2015
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
+17,8	+17,8	k4	+17,8
°	°	k?	°
<g/>
<g />
.	.	kIx.	.
</s>
<s>
C.	C.	kA	C.
Teploty	teplota	k1gFnSc2	teplota
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
se	se	k3xPyFc4	se
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
(	(	kIx(	(
<g/>
červenec	červenec	k1gInSc1	červenec
<g/>
)	)	kIx)	)
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
průměrně	průměrně	k6eAd1	průměrně
od	od	k7c2	od
-	-	kIx~	-
<g/>
40	[number]	k4	40
°	°	k?	°
<g/>
C	C	kA	C
do	do	k7c2	do
-	-	kIx~	-
<g/>
70	[number]	k4	70
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
extrémy	extrém	k1gInPc1	extrém
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
i	i	k9	i
-	-	kIx~	-
<g/>
90	[number]	k4	90
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
(	(	kIx(	(
<g/>
leden	leden	k1gInSc1	leden
<g/>
)	)	kIx)	)
kolísají	kolísat	k5eAaImIp3nP	kolísat
mezi	mezi	k7c7	mezi
-	-	kIx~	-
<g/>
10	[number]	k4	10
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
-	-	kIx~	-
<g/>
40	[number]	k4	40
°	°	k?	°
<g/>
C.	C.	kA	C.
Východní	východní	k2eAgFnSc1d1	východní
Antarktida	Antarktida	k1gFnSc1	Antarktida
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
chladnější	chladný	k2eAgMnSc1d2	chladnější
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
má	mít	k5eAaImIp3nS	mít
ročně	ročně	k6eAd1	ročně
srážek	srážka	k1gFnPc2	srážka
mezi	mezi	k7c4	mezi
50	[number]	k4	50
milimetrů	milimetr	k1gInPc2	milimetr
až	až	k9	až
250	[number]	k4	250
mm	mm	kA	mm
<g/>
,	,	kIx,	,
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
pobřeží	pobřeží	k1gNnSc3	pobřeží
množství	množství	k1gNnSc2	množství
srážek	srážka	k1gFnPc2	srážka
stoupá	stoupat	k5eAaImIp3nS	stoupat
až	až	k9	až
na	na	k7c4	na
500	[number]	k4	500
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Výjimečně	výjimečně	k6eAd1	výjimečně
bývá	bývat	k5eAaImIp3nS	bývat
srážek	srážka	k1gFnPc2	srážka
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
Antarktický	antarktický	k2eAgInSc1d1	antarktický
poloostrov	poloostrov	k1gInSc1	poloostrov
má	mít	k5eAaImIp3nS	mít
mezi	mezi	k7c7	mezi
500	[number]	k4	500
až	až	k9	až
1000	[number]	k4	1000
mm	mm	kA	mm
srážek	srážka	k1gFnPc2	srážka
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
i	i	k9	i
přilehlé	přilehlý	k2eAgInPc4d1	přilehlý
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Antarktida	Antarktida	k1gFnSc1	Antarktida
je	být	k5eAaImIp3nS	být
navíc	navíc	k6eAd1	navíc
největrnějším	větrný	k2eAgInSc7d3	větrný
kontinentem	kontinent	k1gInSc7	kontinent
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gInPc6	jeho
okrajích	okraj	k1gInPc6	okraj
a	a	k8xC	a
v	v	k7c6	v
pobřežních	pobřežní	k2eAgFnPc6d1	pobřežní
oblastech	oblast	k1gFnPc6	oblast
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
velmi	velmi	k6eAd1	velmi
častému	častý	k2eAgNnSc3d1	časté
silnému	silný	k2eAgNnSc3d1	silné
až	až	k8xS	až
bouřlivému	bouřlivý	k2eAgNnSc3d1	bouřlivé
větrnému	větrný	k2eAgNnSc3d1	větrné
proudění	proudění	k1gNnSc3	proudění
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
síly	síla	k1gFnSc2	síla
uragánu	uragán	k1gInSc2	uragán
<g/>
.	.	kIx.	.
</s>
<s>
Směrem	směr	k1gInSc7	směr
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
se	se	k3xPyFc4	se
rychlost	rychlost	k1gFnSc1	rychlost
větru	vítr	k1gInSc2	vítr
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Flóra	Flóra	k1gFnSc1	Flóra
Antarktidy	Antarktida	k1gFnSc2	Antarktida
a	a	k8xC	a
Fauna	Faun	k1gMnSc2	Faun
Antarktidy	Antarktida	k1gFnSc2	Antarktida
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nejstudenějším	studený	k2eAgInSc6d3	nejstudenější
kontinentu	kontinent	k1gInSc6	kontinent
byly	být	k5eAaImAgFnP	být
objeveny	objevit	k5eAaPmNgFnP	objevit
pouze	pouze	k6eAd1	pouze
dva	dva	k4xCgInPc1	dva
druhy	druh	k1gInPc1	druh
původních	původní	k2eAgFnPc2d1	původní
kvetoucích	kvetoucí	k2eAgFnPc2d1	kvetoucí
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
nepůvodní	původní	k2eNgFnSc1d1	nepůvodní
invazní	invazní	k2eAgFnSc1d1	invazní
kvetoucí	kvetoucí	k2eAgFnSc1d1	kvetoucí
bylina	bylina	k1gFnSc1	bylina
(	(	kIx(	(
<g/>
Poa	Poa	k1gMnSc1	Poa
annua	annua	k1gMnSc1	annua
<g/>
)	)	kIx)	)
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
druhů	druh	k1gInPc2	druh
mechů	mech	k1gInPc2	mech
a	a	k8xC	a
lišejníků	lišejník	k1gInPc2	lišejník
<g/>
.	.	kIx.	.
</s>
<s>
Zaznamenány	zaznamenán	k2eAgFnPc1d1	zaznamenána
byly	být	k5eAaImAgFnP	být
také	také	k9	také
některé	některý	k3yIgFnPc1	některý
řasy	řasa	k1gFnPc1	řasa
<g/>
,	,	kIx,	,
houby	houba	k1gFnPc1	houba
a	a	k8xC	a
bakterie	bakterie	k1gFnPc1	bakterie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
podali	podat	k5eAaPmAgMnP	podat
vědci	vědec	k1gMnPc1	vědec
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
prudkém	prudký	k2eAgInSc6d1	prudký
nárůstu	nárůst	k1gInSc6	nárůst
počtu	počet	k1gInSc2	počet
pobřežních	pobřežní	k2eAgFnPc2d1	pobřežní
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zřejmě	zřejmě	k6eAd1	zřejmě
podporuje	podporovat	k5eAaImIp3nS	podporovat
teorii	teorie	k1gFnSc4	teorie
o	o	k7c6	o
globálním	globální	k2eAgNnSc6d1	globální
oteplování	oteplování	k1gNnSc6	oteplování
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolních	okolní	k2eAgNnPc6d1	okolní
mořích	moře	k1gNnPc6	moře
žije	žít	k5eAaImIp3nS	žít
spousta	spousta	k1gFnSc1	spousta
drobných	drobný	k2eAgMnPc2d1	drobný
korýšů	korýš	k1gMnPc2	korýš
<g/>
,	,	kIx,	,
živočichů	živočich	k1gMnPc2	živočich
s	s	k7c7	s
tvrdou	tvrdý	k2eAgFnSc7d1	tvrdá
ulitou	ulita	k1gFnSc7	ulita
zvaných	zvaný	k2eAgFnPc2d1	zvaná
krill	krilla	k1gFnPc2	krilla
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c4	mnoho
obratlovců	obratlovec	k1gMnPc2	obratlovec
(	(	kIx(	(
<g/>
kytovci	kytovec	k1gMnPc1	kytovec
<g/>
,	,	kIx,	,
rypouši	rypouš	k1gMnPc1	rypouš
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
ryby	ryba	k1gFnPc1	ryba
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
)	)	kIx)	)
i	i	k8xC	i
bezobratlých	bezobratlý	k2eAgMnPc2d1	bezobratlý
živočichů	živočich	k1gMnPc2	živočich
(	(	kIx(	(
<g/>
plankton	plankton	k1gInSc1	plankton
<g/>
,	,	kIx,	,
hlavonožci	hlavonožec	k1gMnPc1	hlavonožec
<g/>
,	,	kIx,	,
ostnokožci	ostnokožec	k1gMnPc1	ostnokožec
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Najdeme	najít	k5eAaPmIp1nP	najít
tu	ten	k3xDgFnSc4	ten
hojnost	hojnost	k1gFnSc4	hojnost
mořských	mořský	k2eAgMnPc2d1	mořský
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
chaluhy	chaluha	k1gFnSc2	chaluha
a	a	k8xC	a
albatrosi	albatros	k1gMnPc1	albatros
<g/>
.	.	kIx.	.
</s>
<s>
Faunu	Faun	k1gMnSc3	Faun
také	také	k9	také
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
osm	osm	k4xCc4	osm
druhů	druh	k1gInPc2	druh
tučňáků	tučňák	k1gMnPc2	tučňák
<g/>
,	,	kIx,	,
rackovitý	rackovitý	k2eAgMnSc1d1	rackovitý
pták	pták	k1gMnSc1	pták
chaluha	chaluha	k1gFnSc1	chaluha
velká	velká	k1gFnSc1	velká
a	a	k8xC	a
5	[number]	k4	5
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
z	z	k7c2	z
buřňákovitých	buřňákovitý	k2eAgFnPc2d1	buřňákovitý
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
drsným	drsný	k2eAgFnPc3d1	drsná
klimatickým	klimatický	k2eAgFnPc3d1	klimatická
a	a	k8xC	a
přírodním	přírodní	k2eAgFnPc3d1	přírodní
podmínkám	podmínka	k1gFnPc3	podmínka
je	být	k5eAaImIp3nS	být
Antarktida	Antarktida	k1gFnSc1	Antarktida
obývána	obýván	k2eAgFnSc1d1	obývána
pouze	pouze	k6eAd1	pouze
vědeckými	vědecký	k2eAgMnPc7d1	vědecký
pracovníky	pracovník	k1gMnPc7	pracovník
výzkumných	výzkumný	k2eAgFnPc2d1	výzkumná
polárních	polární	k2eAgFnPc2d1	polární
stanic	stanice	k1gFnPc2	stanice
mnoha	mnoho	k4c2	mnoho
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letní	letní	k2eAgFnSc6d1	letní
sezóně	sezóna	k1gFnSc6	sezóna
jich	on	k3xPp3gNnPc2	on
je	být	k5eAaImIp3nS	být
přes	přes	k7c4	přes
4	[number]	k4	4
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
v	v	k7c4	v
zimní	zimní	k2eAgInSc4d1	zimní
kolem	kolem	k7c2	kolem
1	[number]	k4	1
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Antarktida	Antarktida	k1gFnSc1	Antarktida
má	mít	k5eAaImIp3nS	mít
značná	značný	k2eAgNnPc4d1	značné
ložiska	ložisko	k1gNnPc4	ložisko
nerostných	nerostný	k2eAgFnPc2d1	nerostná
surovin	surovina	k1gFnPc2	surovina
<g/>
.	.	kIx.	.
</s>
<s>
Využití	využití	k1gNnSc1	využití
těchto	tento	k3xDgNnPc2	tento
ložisek	ložisko	k1gNnPc2	ložisko
nastálo	nastálo	k6eAd1	nastálo
zakazuje	zakazovat	k5eAaImIp3nS	zakazovat
Protokol	protokol	k1gInSc1	protokol
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
Smlouvy	smlouva	k1gFnSc2	smlouva
o	o	k7c6	o
Antarktidě	Antarktida	k1gFnSc6	Antarktida
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
zákaz	zákaz	k1gInSc1	zákaz
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
přehodnocen	přehodnocen	k2eAgInSc1d1	přehodnocen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2048	[number]	k4	2048
<g/>
.	.	kIx.	.
</s>
<s>
Případné	případný	k2eAgNnSc1d1	případné
využití	využití	k1gNnSc1	využití
také	také	k6eAd1	také
budou	být	k5eAaImBp3nP	být
ztěžovat	ztěžovat	k5eAaImF	ztěžovat
nepříznivé	příznivý	k2eNgFnPc4d1	nepříznivá
přírodní	přírodní	k2eAgFnPc4d1	přírodní
podmínky	podmínka	k1gFnPc4	podmínka
i	i	k8xC	i
odlehlost	odlehlost	k1gFnSc4	odlehlost
oblasti	oblast	k1gFnSc2	oblast
od	od	k7c2	od
vyspělých	vyspělý	k2eAgNnPc2d1	vyspělé
průmyslových	průmyslový	k2eAgNnPc2d1	průmyslové
center	centrum	k1gNnPc2	centrum
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
jsou	být	k5eAaImIp3nP	být
ložiska	ložisko	k1gNnSc2	ložisko
uhlí	uhlí	k1gNnSc2	uhlí
ve	v	k7c6	v
Transantarktickém	Transantarktický	k2eAgNnSc6d1	Transantarktické
pohoří	pohoří	k1gNnSc6	pohoří
a	a	k8xC	a
Viktoriině	Viktoriin	k2eAgFnSc3d1	Viktoriina
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Ross-Weddellovy	Ross-Weddellův	k2eAgFnSc2d1	Ross-Weddellův
deprese	deprese	k1gFnSc2	deprese
<g/>
,	,	kIx,	,
železných	železný	k2eAgFnPc2d1	železná
rud	ruda	k1gFnPc2	ruda
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
Královny	královna	k1gFnPc1	královna
Maud	Mauda	k1gFnPc2	Mauda
<g/>
,	,	kIx,	,
cínu	cín	k1gInSc2	cín
<g/>
,	,	kIx,	,
mědi	měď	k1gFnSc2	měď
<g/>
,	,	kIx,	,
uranu	uran	k1gInSc2	uran
<g/>
,	,	kIx,	,
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
diamantů	diamant	k1gInPc2	diamant
<g/>
,	,	kIx,	,
olova	olovo	k1gNnSc2	olovo
<g/>
,	,	kIx,	,
manganu	mangan	k1gInSc2	mangan
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Oceány	oceán	k1gInPc1	oceán
obklopující	obklopující	k2eAgFnSc4d1	obklopující
Antarktidu	Antarktida	k1gFnSc4	Antarktida
jsou	být	k5eAaImIp3nP	být
bohaté	bohatý	k2eAgInPc1d1	bohatý
na	na	k7c4	na
tuleně	tuleň	k1gMnPc4	tuleň
a	a	k8xC	a
velryby	velryba	k1gFnPc4	velryba
i	i	k9	i
některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
ryb	ryba	k1gFnPc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
mohutnému	mohutný	k2eAgInSc3d1	mohutný
ledovcovému	ledovcový	k2eAgInSc3d1	ledovcový
příkrovu	příkrov	k1gInSc3	příkrov
je	být	k5eAaImIp3nS	být
kontinent	kontinent	k1gInSc4	kontinent
největším	veliký	k2eAgInSc7d3	veliký
rezervoárem	rezervoár	k1gInSc7	rezervoár
sladké	sladký	k2eAgFnSc2d1	sladká
vody	voda	k1gFnSc2	voda
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
samotném	samotný	k2eAgInSc6d1	samotný
kontinentě	kontinent	k1gInSc6	kontinent
není	být	k5eNaImIp3nS	být
provozována	provozovat	k5eAaImNgFnS	provozovat
téměř	téměř	k6eAd1	téměř
žádná	žádný	k3yNgFnSc1	žádný
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
aktivita	aktivita	k1gFnSc1	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okolních	okolní	k2eAgNnPc6d1	okolní
mořích	moře	k1gNnPc6	moře
je	být	k5eAaImIp3nS	být
provozován	provozován	k2eAgInSc4d1	provozován
rybolov	rybolov	k1gInSc4	rybolov
(	(	kIx(	(
<g/>
128	[number]	k4	128
081	[number]	k4	081
tun	tuna	k1gFnPc2	tuna
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
a	a	k8xC	a
turistika	turistika	k1gFnSc1	turistika
(	(	kIx(	(
<g/>
36	[number]	k4	36
460	[number]	k4	460
návštěvníků	návštěvník	k1gMnPc2	návštěvník
na	na	k7c6	na
lodích	loď	k1gFnPc6	loď
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
letecké	letecký	k2eAgFnPc1d1	letecká
společnosti	společnost	k1gFnPc1	společnost
také	také	k9	také
provozují	provozovat	k5eAaImIp3nP	provozovat
nad	nad	k7c7	nad
Antarktidou	Antarktida	k1gFnSc7	Antarktida
vyhlídkové	vyhlídkový	k2eAgInPc1d1	vyhlídkový
lety	let	k1gInPc1	let
<g/>
.	.	kIx.	.
</s>
<s>
Doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Antarktidě	Antarktida	k1gFnSc6	Antarktida
je	být	k5eAaImIp3nS	být
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
extrémním	extrémní	k2eAgFnPc3d1	extrémní
podmínkám	podmínka	k1gFnPc3	podmínka
velmi	velmi	k6eAd1	velmi
náročná	náročný	k2eAgFnSc1d1	náročná
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
průzkumníci	průzkumník	k1gMnPc1	průzkumník
byli	být	k5eAaImAgMnP	být
odkázáni	odkázat	k5eAaPmNgMnP	odkázat
především	především	k9	především
na	na	k7c4	na
lidskou	lidský	k2eAgFnSc4d1	lidská
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
zvířecí	zvířecí	k2eAgFnSc4d1	zvířecí
sílu	síla	k1gFnSc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
moderním	moderní	k2eAgFnPc3d1	moderní
technologiím	technologie	k1gFnPc3	technologie
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
i	i	k9	i
mechanizovaná	mechanizovaný	k2eAgFnSc1d1	mechanizovaná
přeprava	přeprava	k1gFnSc1	přeprava
osob	osoba	k1gFnPc2	osoba
a	a	k8xC	a
nákladů	náklad	k1gInPc2	náklad
<g/>
.	.	kIx.	.
</s>
<s>
Dopravní	dopravní	k2eAgInPc1d1	dopravní
prostředky	prostředek	k1gInPc1	prostředek
musí	muset	k5eAaImIp3nP	muset
odolávat	odolávat	k5eAaImF	odolávat
silným	silný	k2eAgInPc3d1	silný
mrazům	mráz	k1gInPc3	mráz
a	a	k8xC	a
větru	vítr	k1gInSc3	vítr
i	i	k8xC	i
všudypřítomnému	všudypřítomný	k2eAgInSc3d1	všudypřítomný
ledu	led	k1gInSc3	led
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
dbát	dbát	k5eAaImF	dbát
také	také	k9	také
na	na	k7c4	na
minimalizaci	minimalizace	k1gFnSc4	minimalizace
ekologických	ekologický	k2eAgInPc2d1	ekologický
dopadů	dopad	k1gInPc2	dopad
v	v	k7c6	v
relativně	relativně	k6eAd1	relativně
nenarušeném	narušený	k2eNgInSc6d1	nenarušený
antarktickém	antarktický	k2eAgInSc6d1	antarktický
ekosystému	ekosystém	k1gInSc6	ekosystém
<g/>
.	.	kIx.	.
</s>
<s>
Ukládání	ukládání	k1gNnSc1	ukládání
odpadů	odpad	k1gInPc2	odpad
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
starých	starý	k2eAgNnPc2d1	staré
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
od	od	k7c2	od
platnosti	platnost	k1gFnSc2	platnost
Protokolu	protokol	k1gInSc2	protokol
o	o	k7c6	o
ochraně	ochrana	k1gFnSc6	ochrana
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozemní	pozemní	k2eAgFnSc6d1	pozemní
dopravě	doprava	k1gFnSc6	doprava
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
lidských	lidský	k2eAgFnPc2d1	lidská
nohou	noha	k1gFnPc2	noha
uplatňují	uplatňovat	k5eAaImIp3nP	uplatňovat
lyže	lyže	k1gFnPc4	lyže
a	a	k8xC	a
sněžnice	sněžnice	k1gFnPc4	sněžnice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
různá	různý	k2eAgNnPc1d1	různé
speciálně	speciálně	k6eAd1	speciálně
upravená	upravený	k2eAgNnPc1d1	upravené
vozidla	vozidlo	k1gNnPc1	vozidlo
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
pásová	pásový	k2eAgFnSc1d1	pásová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
hojně	hojně	k6eAd1	hojně
využívalo	využívat	k5eAaImAgNnS	využívat
saní	saně	k1gFnPc2	saně
tažených	tažený	k2eAgFnPc2d1	tažená
psím	psí	k2eAgNnSc7d1	psí
spřežením	spřežení	k1gNnSc7	spřežení
<g/>
.	.	kIx.	.
</s>
<s>
Polární	polární	k2eAgFnSc4d1	polární
stanici	stanice	k1gFnSc4	stanice
Amundsen-Scott	Amundsen-Scotta	k1gFnPc2	Amundsen-Scotta
na	na	k7c6	na
Jižním	jižní	k2eAgNnSc6d1	jižní
pólu	pólo	k1gNnSc6	pólo
a	a	k8xC	a
pobřežní	pobřežní	k2eAgFnSc3d1	pobřežní
Polární	polární	k2eAgFnSc3d1	polární
stanici	stanice	k1gFnSc3	stanice
McMurdo	McMurdo	k1gNnSc1	McMurdo
spojuje	spojovat	k5eAaImIp3nS	spojovat
1	[number]	k4	1
500	[number]	k4	500
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
"	"	kIx"	"
<g/>
dálnice	dálnice	k1gFnSc1	dálnice
<g/>
"	"	kIx"	"
McMurdo	McMurdo	k1gNnSc1	McMurdo
<g/>
-	-	kIx~	-
<g/>
South	South	k1gInSc1	South
Pole	pole	k1gFnSc2	pole
<g/>
,	,	kIx,	,
sjízdná	sjízdný	k2eAgFnSc1d1	sjízdná
pásovými	pásový	k2eAgNnPc7d1	pásové
vozidly	vozidlo	k1gNnPc7	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgInSc1d1	jediný
námořní	námořní	k2eAgInSc1d1	námořní
přístav	přístav	k1gInSc1	přístav
na	na	k7c6	na
Antarktidě	Antarktida	k1gFnSc6	Antarktida
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
polární	polární	k2eAgFnSc2d1	polární
stanice	stanice	k1gFnSc2	stanice
McMurdo	McMurdo	k1gNnSc1	McMurdo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jiných	jiný	k2eAgNnPc6d1	jiné
místech	místo	k1gNnPc6	místo
musí	muset	k5eAaImIp3nP	muset
lodě	loď	k1gFnPc1	loď
zakotvit	zakotvit	k5eAaPmF	zakotvit
dál	daleko	k6eAd2	daleko
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
a	a	k8xC	a
zásoby	zásoba	k1gFnPc1	zásoba
nebo	nebo	k8xC	nebo
cestující	cestující	k1gMnPc1	cestující
se	se	k3xPyFc4	se
do	do	k7c2	do
cíle	cíl	k1gInSc2	cíl
přepravují	přepravovat	k5eAaImIp3nP	přepravovat
malými	malý	k2eAgInPc7d1	malý
čluny	člun	k1gInPc7	člun
nebo	nebo	k8xC	nebo
vrtulníkem	vrtulník	k1gInSc7	vrtulník
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
letních	letní	k2eAgInPc2d1	letní
měsíců	měsíc	k1gInPc2	měsíc
(	(	kIx(	(
<g/>
leden	leden	k1gInSc1	leden
<g/>
-	-	kIx~	-
<g/>
březen	březen	k1gInSc1	březen
<g/>
)	)	kIx)	)
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
antarktické	antarktický	k2eAgNnSc4d1	antarktické
pobřeží	pobřeží	k1gNnSc4	pobřeží
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
Antarktický	antarktický	k2eAgInSc4d1	antarktický
poloostrov	poloostrov	k1gInSc4	poloostrov
<g/>
)	)	kIx)	)
kromě	kromě	k7c2	kromě
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
expedicí	expedice	k1gFnPc2	expedice
také	také	k9	také
množství	množství	k1gNnSc1	množství
soukromých	soukromý	k2eAgFnPc2d1	soukromá
jachet	jachta	k1gFnPc2	jachta
a	a	k8xC	a
turistických	turistický	k2eAgFnPc2d1	turistická
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gInSc7	jejich
výchozím	výchozí	k2eAgInSc7d1	výchozí
přístavem	přístav	k1gInSc7	přístav
Ushuaia	Ushuaium	k1gNnSc2	Ushuaium
v	v	k7c6	v
Argentině	Argentina	k1gFnSc6	Argentina
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
letecké	letecký	k2eAgFnSc3d1	letecká
dopravě	doprava	k1gFnSc3	doprava
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
letouny	letoun	k1gInPc1	letoun
nebo	nebo	k8xC	nebo
vrtulníky	vrtulník	k1gInPc1	vrtulník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Antarktidě	Antarktida	k1gFnSc6	Antarktida
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
25	[number]	k4	25
letištních	letištní	k2eAgFnPc2d1	letištní
ploch	plocha	k1gFnPc2	plocha
a	a	k8xC	a
53	[number]	k4	53
heliportů	heliport	k1gInPc2	heliport
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
první	první	k4xOgFnSc4	první
pravidelnou	pravidelný	k2eAgFnSc4d1	pravidelná
leteckou	letecký	k2eAgFnSc4d1	letecká
linku	linka	k1gFnSc4	linka
na	na	k7c4	na
Antarktidu	Antarktida	k1gFnSc4	Antarktida
začala	začít	k5eAaPmAgFnS	začít
provozovat	provozovat	k5eAaImF	provozovat
americká	americký	k2eAgFnSc1d1	americká
společnost	společnost	k1gFnSc1	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
určena	určit	k5eAaPmNgFnS	určit
jen	jen	k9	jen
pro	pro	k7c4	pro
vědce	vědec	k1gMnPc4	vědec
a	a	k8xC	a
výzkumníky	výzkumník	k1gMnPc4	výzkumník
<g/>
.	.	kIx.	.
</s>
<s>
Přistávací	přistávací	k2eAgFnSc1d1	přistávací
plocha	plocha	k1gFnSc1	plocha
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
podle	podle	k7c2	podle
australského	australský	k2eAgMnSc2d1	australský
dobrodruha	dobrodruh	k1gMnSc2	dobrodruh
a	a	k8xC	a
letce	letec	k1gMnSc2	letec
sira	sir	k1gMnSc2	sir
Huberta	Hubert	k1gMnSc2	Hubert
Wilkinsona	Wilkinson	k1gMnSc2	Wilkinson
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
podnikl	podniknout	k5eAaPmAgMnS	podniknout
let	let	k1gInSc4	let
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
kontinent	kontinent	k1gInSc4	kontinent
jako	jako	k8xC	jako
první	první	k4xOgInSc4	první
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
<g/>
.	.	kIx.	.
</s>
