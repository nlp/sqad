<p>
<s>
Cookovy	Cookův	k2eAgInPc1d1	Cookův
ostrovy	ostrov	k1gInPc1	ostrov
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
<g/>
:	:	kIx,	:
Cook	Cook	k1gMnSc1	Cook
Islands	Islandsa	k1gFnPc2	Islandsa
<g/>
,	,	kIx,	,
maorsky	maorsky	k6eAd1	maorsky
Kū	Kū	k1gFnSc1	Kū
'	'	kIx"	'
<g/>
Ā	Ā	k?	Ā
<g/>
;	;	kIx,	;
další	další	k2eAgInPc4d1	další
názvy	název	k1gInPc4	název
<g/>
:	:	kIx,	:
Mangaiské	Mangaiský	k2eAgInPc4d1	Mangaiský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
Herveyho	Herveyha	k1gFnSc5	Herveyha
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
svrchovaný	svrchovaný	k2eAgInSc1d1	svrchovaný
stát	stát	k1gInSc1	stát
a	a	k8xC	a
skupina	skupina	k1gFnSc1	skupina
ostrovů	ostrov	k1gInPc2	ostrov
ležících	ležící	k2eAgInPc2d1	ležící
v	v	k7c6	v
Tichém	tichý	k2eAgInSc6d1	tichý
oceáně	oceán	k1gInSc6	oceán
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
měřící	měřící	k2eAgFnSc1d1	měřící
asi	asi	k9	asi
1500	[number]	k4	1500
km	km	kA	km
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
na	na	k7c4	na
jih	jih	k1gInSc4	jih
a	a	k8xC	a
900	[number]	k4	900
km	km	kA	km
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
sousedí	sousedit	k5eAaImIp3nS	sousedit
se	s	k7c7	s
souostrovím	souostroví	k1gNnSc7	souostroví
Kiribati	Kiribati	k1gFnSc2	Kiribati
(	(	kIx(	(
<g/>
asi	asi	k9	asi
400	[number]	k4	400
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
leží	ležet	k5eAaImIp3nP	ležet
Společenské	společenský	k2eAgInPc1d1	společenský
ostrovy	ostrov	k1gInPc1	ostrov
a	a	k8xC	a
souostroví	souostroví	k1gNnSc1	souostroví
Tufuai	Tufua	k1gFnSc2	Tufua
(	(	kIx(	(
<g/>
300	[number]	k4	300
až	až	k9	až
500	[number]	k4	500
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obojí	obojí	k4xRgMnSc1	obojí
patřící	patřící	k2eAgMnSc1d1	patřící
pod	pod	k7c4	pod
Francouzskou	francouzský	k2eAgFnSc4d1	francouzská
Polynésii	Polynésie	k1gFnSc4	Polynésie
<g/>
.	.	kIx.	.
</s>
<s>
Nejbližšími	blízký	k2eAgInPc7d3	Nejbližší
ostrovy	ostrov	k1gInPc7	ostrov
na	na	k7c6	na
západě	západ	k1gInSc6	západ
jsou	být	k5eAaImIp3nP	být
Niue	Niue	k1gFnSc4	Niue
(	(	kIx(	(
<g/>
asi	asi	k9	asi
700	[number]	k4	700
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tonga	Tonga	k1gFnSc1	Tonga
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1100	[number]	k4	1100
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
Samoa	Samoa	k1gFnSc1	Samoa
(	(	kIx(	(
<g/>
asi	asi	k9	asi
500	[number]	k4	500
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ostrovy	ostrov	k1gInPc4	ostrov
tvoří	tvořit	k5eAaImIp3nS	tvořit
severní	severní	k2eAgFnSc1d1	severní
skupina	skupina	k1gFnSc1	skupina
(	(	kIx(	(
<g/>
největších	veliký	k2eAgInPc2d3	veliký
ostrovů	ostrov	k1gInPc2	ostrov
je	být	k5eAaImIp3nS	být
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
celkem	celkem	k6eAd1	celkem
28	[number]	k4	28
km	km	kA	km
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
a	a	k8xC	a
jižní	jižní	k2eAgFnSc1d1	jižní
skupina	skupina	k1gFnSc1	skupina
–	–	k?	–
212	[number]	k4	212
km	km	kA	km
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
(	(	kIx(	(
<g/>
8	[number]	k4	8
největších	veliký	k2eAgInPc2d3	veliký
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
plocha	plocha	k1gFnSc1	plocha
ostrovů	ostrov	k1gInPc2	ostrov
je	být	k5eAaImIp3nS	být
240	[number]	k4	240
km2	km2	k4	km2
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
21	[number]	k4	21
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
většina	většina	k1gFnSc1	většina
jsou	být	k5eAaImIp3nP	být
Maorové	Maorové	k2eAgMnPc1d1	Maorové
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
postupně	postupně	k6eAd1	postupně
<g/>
,	,	kIx,	,
počínaje	počínaje	k7c7	počínaje
rokem	rok	k1gInSc7	rok
1823	[number]	k4	1823
obráceni	obrátit	k5eAaPmNgMnP	obrátit
evropskými	evropský	k2eAgMnPc7d1	evropský
misionáři	misionář	k1gMnPc7	misionář
na	na	k7c4	na
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
Avarua	Avaru	k2eAgFnSc1d1	Avaru
a	a	k8xC	a
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Rarotonga	Rarotonga	k1gFnSc1	Rarotonga
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
Cookovy	Cookův	k2eAgInPc4d1	Cookův
ostrovy	ostrov	k1gInPc4	ostrov
osídlili	osídlit	k5eAaPmAgMnP	osídlit
polynéští	polynéský	k2eAgMnPc1d1	polynéský
Maorové	Maor	k1gMnPc1	Maor
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
evropský	evropský	k2eAgInSc4d1	evropský
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
Cookovými	Cookův	k2eAgInPc7d1	Cookův
ostrovy	ostrov	k1gInPc7	ostrov
je	být	k5eAaImIp3nS	být
datován	datován	k2eAgInSc1d1	datován
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1595	[number]	k4	1595
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Španěl	Španěl	k1gMnSc1	Španěl
Alvaro	Alvara	k1gFnSc5	Alvara
de	de	k?	de
Mendañ	Mendañ	k1gMnSc3	Mendañ
de	de	k?	de
Neyra	Neyra	k1gMnSc1	Neyra
přistál	přistát	k5eAaImAgMnS	přistát
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
ostrově	ostrov	k1gInSc6	ostrov
Pukapuka	Pukapuk	k1gMnSc2	Pukapuk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1606	[number]	k4	1606
přistáli	přistát	k5eAaImAgMnP	přistát
Španělé	Španěl	k1gMnPc1	Španěl
pod	pod	k7c7	pod
portugalskou	portugalský	k2eAgFnSc7d1	portugalská
vlajkou	vlajka	k1gFnSc7	vlajka
vedení	vedení	k1gNnSc2	vedení
kapitánem	kapitán	k1gMnSc7	kapitán
Pedrem	Pedr	k1gMnSc7	Pedr
Fernandezem	Fernandez	k1gInSc7	Fernandez
de	de	k?	de
Quiros	Quirosa	k1gFnPc2	Quirosa
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Rakahanga	Rakahanga	k1gFnSc1	Rakahanga
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
přistáli	přistát	k5eAaPmAgMnP	přistát
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1764	[number]	k4	1764
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Pukapuka	Pukapuk	k1gMnSc2	Pukapuk
a	a	k8xC	a
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
jej	on	k3xPp3gInSc4	on
Danger	Danger	k1gInSc4	Danger
Island	Island	k1gInSc1	Island
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
nepodařilo	podařit	k5eNaPmAgNnS	podařit
přistát	přistát	k5eAaPmF	přistát
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1773	[number]	k4	1773
a	a	k8xC	a
1779	[number]	k4	1779
byly	být	k5eAaImAgInP	být
jižní	jižní	k2eAgInPc1d1	jižní
ostrovy	ostrov	k1gInPc1	ostrov
souostroví	souostroví	k1gNnSc2	souostroví
Jamesem	James	k1gMnSc7	James
Cookem	Cooek	k1gMnSc7	Cooek
vícekrát	vícekrát	k6eAd1	vícekrát
navštíveny	navštíven	k2eAgMnPc4d1	navštíven
<g/>
,	,	kIx,	,
přitom	přitom	k6eAd1	přitom
byla	být	k5eAaImAgFnS	být
Rarotonga	Rarotonga	k1gFnSc1	Rarotonga
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgInSc1d1	hlavní
ostrov	ostrov	k1gInSc1	ostrov
souostroví	souostroví	k1gNnSc2	souostroví
<g/>
,	,	kIx,	,
spatřena	spatřit	k5eAaPmNgFnS	spatřit
poprvé	poprvé	k6eAd1	poprvé
<g/>
.	.	kIx.	.
</s>
<s>
Slavný	slavný	k2eAgMnSc1d1	slavný
kapitán	kapitán	k1gMnSc1	kapitán
William	William	k1gInSc4	William
Bligh	Bligh	k1gInSc1	Bligh
přistál	přistát	k5eAaPmAgInS	přistát
s	s	k7c7	s
Bounty	Bount	k1gMnPc7	Bount
1789	[number]	k4	1789
na	na	k7c6	na
Aitutaki	Aitutak	k1gFnSc6	Aitutak
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
přivezl	přivézt	k5eAaPmAgMnS	přivézt
na	na	k7c4	na
ostrovy	ostrov	k1gInPc4	ostrov
chlebovník	chlebovník	k1gInSc4	chlebovník
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gMnSc1	James
Cook	Cook	k1gMnSc1	Cook
dal	dát	k5eAaPmAgMnS	dát
prvnímu	první	k4xOgNnSc3	první
ostrovu	ostrov	k1gInSc3	ostrov
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
přistál	přistát	k5eAaImAgMnS	přistát
<g/>
,	,	kIx,	,
Manuae	Manuae	k1gInSc1	Manuae
<g/>
,	,	kIx,	,
jméno	jméno	k1gNnSc1	jméno
Hervey	Hervea	k1gFnSc2	Hervea
Islands	Islandsa	k1gFnPc2	Islandsa
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Cookovy	Cookův	k2eAgInPc1d1	Cookův
ostrovy	ostrov	k1gInPc1	ostrov
dali	dát	k5eAaPmAgMnP	dát
ostrovům	ostrov	k1gInPc3	ostrov
Rusové	Rus	k1gMnPc1	Rus
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
Jamese	Jamese	k1gFnSc2	Jamese
Cooka	Cooek	k1gInSc2	Cooek
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
se	se	k3xPyFc4	se
objevilo	objevit	k5eAaPmAgNnS	objevit
na	na	k7c6	na
ruských	ruský	k2eAgFnPc6d1	ruská
mapách	mapa	k1gFnPc6	mapa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
<g/>
/	/	kIx~	/
<g/>
říjnu	říjen	k1gInSc3	říjen
roku	rok	k1gInSc2	rok
1813	[number]	k4	1813
přistáli	přistát	k5eAaPmAgMnP	přistát
na	na	k7c6	na
Cookových	Cookův	k2eAgInPc6d1	Cookův
ostrovech	ostrov	k1gInPc6	ostrov
první	první	k4xOgMnPc1	první
evropští	evropský	k2eAgMnPc1d1	evropský
úředníci	úředník	k1gMnPc1	úředník
s	s	k7c7	s
lodí	loď	k1gFnSc7	loď
Endeavour	Endeavour	k1gInSc4	Endeavour
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1814	[number]	k4	1814
<g/>
,	,	kIx,	,
přistála	přistát	k5eAaImAgFnS	přistát
Cumberland	Cumberland	k1gInSc4	Cumberland
s	s	k7c7	s
obchodníky	obchodník	k1gMnPc7	obchodník
z	z	k7c2	z
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
a	a	k8xC	a
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
hledali	hledat	k5eAaImAgMnP	hledat
santálové	santálový	k2eAgNnSc4d1	santálové
dřevo	dřevo	k1gNnSc4	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Rarotonze	Rarotonza	k1gFnSc6	Rarotonza
nenašli	najít	k5eNaPmAgMnP	najít
žádné	žádný	k3yNgInPc4	žádný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vypukly	vypuknout	k5eAaPmAgInP	vypuknout
spory	spor	k1gInPc1	spor
mezi	mezi	k7c7	mezi
námořníky	námořník	k1gMnPc7	námořník
a	a	k8xC	a
ostrovany	ostrovan	k1gMnPc7	ostrovan
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
byli	být	k5eAaImAgMnP	být
zabiti	zabít	k5eAaPmNgMnP	zabít
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
také	také	k9	také
přítelkyně	přítelkyně	k1gFnSc1	přítelkyně
kapitána	kapitán	k1gMnSc2	kapitán
Ann	Ann	k1gFnSc1	Ann
Butchers	Butchers	k1gInSc1	Butchers
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
snědena	sněden	k2eAgFnSc1d1	snědena
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
kosti	kost	k1gFnPc1	kost
byly	být	k5eAaImAgFnP	být
vystaveny	vystavit	k5eAaPmNgFnP	vystavit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jediná	jediný	k2eAgFnSc1d1	jediná
známá	známý	k2eAgFnSc1d1	známá
evropská	evropský	k2eAgFnSc1d1	Evropská
žena	žena	k1gFnSc1	žena
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
kanibaly	kanibal	k1gMnPc4	kanibal
na	na	k7c6	na
Cookových	Cookův	k2eAgInPc6d1	Cookův
ostrovech	ostrov	k1gInPc6	ostrov
zabita	zabít	k5eAaPmNgFnS	zabít
a	a	k8xC	a
snědena	sníst	k5eAaPmNgFnS	sníst
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Cookovy	Cookův	k2eAgInPc1d1	Cookův
ostrovy	ostrov	k1gInPc1	ostrov
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
jižní	jižní	k2eAgFnSc4d1	jižní
skupinu	skupina	k1gFnSc4	skupina
Rarotonga	Rarotong	k1gMnSc2	Rarotong
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgInSc1d1	hlavní
ostrov	ostrov	k1gInSc1	ostrov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Aitutaki	Aitutake	k1gFnSc4	Aitutake
<g/>
,	,	kIx,	,
Atiu	Atia	k1gMnSc4	Atia
<g/>
,	,	kIx,	,
Mangaia	Mangaius	k1gMnSc4	Mangaius
<g/>
,	,	kIx,	,
Manuae	Manuae	k1gFnSc5	Manuae
<g/>
,	,	kIx,	,
Mauke	Mauke	k1gFnSc5	Mauke
<g/>
,	,	kIx,	,
Mitiaro	Mitiara	k1gFnSc5	Mitiara
<g/>
,	,	kIx,	,
Palmerston	Palmerston	k1gInSc4	Palmerston
a	a	k8xC	a
Takutea	Takuteus	k1gMnSc4	Takuteus
a	a	k8xC	a
severní	severní	k2eAgFnSc4d1	severní
skupinu	skupina	k1gFnSc4	skupina
s	s	k7c7	s
ostrovy	ostrov	k1gInPc7	ostrov
Manihiki	Manihiki	k1gNnSc2	Manihiki
<g/>
,	,	kIx,	,
Nassau	Nassaus	k1gInSc2	Nassaus
<g/>
,	,	kIx,	,
Penrhyn	Penrhyn	k1gInSc1	Penrhyn
(	(	kIx(	(
<g/>
Tongareva	Tongareva	k1gFnSc1	Tongareva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Pukapuka	Pukapuk	k1gMnSc4	Pukapuk
<g/>
,	,	kIx,	,
Rakahanga	Rakahang	k1gMnSc4	Rakahang
a	a	k8xC	a
Suwarrow	Suwarrow	k1gMnSc4	Suwarrow
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
nízké	nízký	k2eAgFnPc1d1	nízká
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
korálovým	korálový	k2eAgInPc3d1	korálový
útesům	útes	k1gInPc3	útes
velmi	velmi	k6eAd1	velmi
těžko	těžko	k6eAd1	těžko
přístupné	přístupný	k2eAgInPc1d1	přístupný
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
jsou	být	k5eAaImIp3nP	být
vysoké	vysoký	k2eAgInPc1d1	vysoký
a	a	k8xC	a
porostlé	porostlý	k2eAgInPc1d1	porostlý
hustou	hustý	k2eAgFnSc7d1	hustá
vegetací	vegetace	k1gFnSc7	vegetace
<g/>
.	.	kIx.	.
</s>
<s>
Rostou	růst	k5eAaImIp3nP	růst
na	na	k7c6	na
nich	on	k3xPp3gInPc6	on
kokosové	kokosový	k2eAgFnPc1d1	kokosová
palmy	palma	k1gFnPc1	palma
a	a	k8xC	a
chlebovníky	chlebovník	k1gInPc1	chlebovník
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
skupina	skupina	k1gFnSc1	skupina
ostrovů	ostrov	k1gInPc2	ostrov
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
většinou	většina	k1gFnSc7	většina
z	z	k7c2	z
atolů	atol	k1gInPc2	atol
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politika	politikum	k1gNnSc2	politikum
==	==	k?	==
</s>
</p>
<p>
<s>
Zdejší	zdejší	k2eAgNnSc1d1	zdejší
polynéské	polynéský	k2eAgNnSc1d1	polynéské
království	království	k1gNnSc1	království
bylo	být	k5eAaImAgNnS	být
roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
Velkou	velký	k2eAgFnSc7d1	velká
Británií	Británie	k1gFnSc7	Británie
"	"	kIx"	"
<g/>
vzato	vzít	k5eAaPmNgNnS	vzít
pod	pod	k7c4	pod
ochranu	ochrana	k1gFnSc4	ochrana
<g/>
"	"	kIx"	"
,	,	kIx,	,
tj.	tj.	kA	tj.
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
protektorát	protektorát	k1gInSc1	protektorát
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
federace	federace	k1gFnSc2	federace
Rarotongy	Rarotong	k1gInPc1	Rarotong
<g/>
,	,	kIx,	,
Penrhynu	Penrhyn	k1gInSc2	Penrhyn
<g/>
=	=	kIx~	=
<g/>
Tongarevy	Tongareva	k1gFnSc2	Tongareva
<g/>
,	,	kIx,	,
Aitutaki	Aitutak	k1gFnSc2	Aitutak
a	a	k8xC	a
Niue	Niu	k1gFnSc2	Niu
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
byly	být	k5eAaImAgInP	být
anektovány	anektován	k2eAgInPc1d1	anektován
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1901	[number]	k4	1901
byly	být	k5eAaImAgFnP	být
předány	předat	k5eAaPmNgFnP	předat
Novému	nový	k2eAgInSc3d1	nový
Zélandu	Zéland	k1gInSc3	Zéland
jako	jako	k8xC	jako
jeho	jeho	k3xOp3gFnPc1	jeho
dependence	dependence	k1gFnPc1	dependence
<g/>
.	.	kIx.	.
</s>
<s>
Politicky	politicky	k6eAd1	politicky
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
samosprávným	samosprávný	k2eAgNnSc7d1	samosprávné
územím	území	k1gNnSc7	území
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
obranu	obrana	k1gFnSc4	obrana
a	a	k8xC	a
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
politiku	politika	k1gFnSc4	politika
<g/>
.	.	kIx.	.
</s>
<s>
Status	status	k1gInSc1	status
Cookových	Cookův	k2eAgInPc2d1	Cookův
ostrovů	ostrov	k1gInPc2	ostrov
je	být	k5eAaImIp3nS	být
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
<g/>
.	.	kIx.	.
</s>
<s>
Cookovy	Cookův	k2eAgInPc1d1	Cookův
ostrovy	ostrov	k1gInPc1	ostrov
jsou	být	k5eAaImIp3nP	být
samosprávné	samosprávný	k2eAgNnSc4d1	samosprávné
území	území	k1gNnSc4	území
volně	volně	k6eAd1	volně
přidružené	přidružený	k2eAgNnSc1d1	přidružené
k	k	k7c3	k
Novému	nový	k2eAgInSc3d1	nový
Zélandu	Zéland	k1gInSc3	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
status	status	k1gInSc1	status
byl	být	k5eAaImAgInS	být
Cookovými	Cookův	k2eAgFnPc7d1	Cookova
ostrovy	ostrov	k1gInPc1	ostrov
zvolen	zvolen	k2eAgMnSc1d1	zvolen
v	v	k7c6	v
referendu	referendum	k1gNnSc6	referendum
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
kontrolováno	kontrolovat	k5eAaImNgNnS	kontrolovat
OSN	OSN	kA	OSN
a	a	k8xC	a
schváleno	schválit	k5eAaPmNgNnS	schválit
rezolucí	rezoluce	k1gFnSc7	rezoluce
1514	[number]	k4	1514
(	(	kIx(	(
<g/>
XV	XV	kA	XV
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednomyslně	jednomyslně	k6eAd1	jednomyslně
přijatou	přijatý	k2eAgFnSc4d1	přijatá
Valným	valný	k2eAgNnSc7d1	Valné
shromážděním	shromáždění	k1gNnSc7	shromáždění
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Cookovy	Cookův	k2eAgInPc1d1	Cookův
ostrovy	ostrov	k1gInPc1	ostrov
se	se	k3xPyFc4	se
spravují	spravovat	k5eAaImIp3nP	spravovat
samy	sám	k3xTgInPc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInPc1	jejich
zákony	zákon	k1gInPc1	zákon
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
nezávislé	závislý	k2eNgNnSc1d1	nezávislé
<g/>
.	.	kIx.	.
</s>
<s>
Zákonná	zákonný	k2eAgFnSc1d1	zákonná
a	a	k8xC	a
výkonná	výkonný	k2eAgFnSc1d1	výkonná
moc	moc	k1gFnSc1	moc
nemá	mít	k5eNaImIp3nS	mít
žádná	žádný	k3yNgNnPc4	žádný
omezení	omezení	k1gNnSc4	omezení
od	od	k7c2	od
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgFnSc1d1	státní
příslušnost	příslušnost	k1gFnSc1	příslušnost
Cookových	Cookův	k2eAgInPc2d1	Cookův
ostrovů	ostrov	k1gInPc2	ostrov
ale	ale	k8xC	ale
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
,	,	kIx,	,
obyvatelé	obyvatel	k1gMnPc1	obyvatel
Cookových	Cookův	k2eAgInPc2d1	Cookův
ostrovů	ostrov	k1gInPc2	ostrov
jsou	být	k5eAaImIp3nP	být
občané	občan	k1gMnPc1	občan
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
<g/>
.	.	kIx.	.
81	[number]	k4	81
%	%	kIx~	%
obyv	obyv	k1gInSc1	obyv
<g/>
.	.	kIx.	.
jsou	být	k5eAaImIp3nP	být
místní	místní	k2eAgMnPc1d1	místní
Maoři	Maor	k1gMnPc1	Maor
a	a	k8xC	a
přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
ze	z	k7c2	z
sousedních	sousední	k2eAgInPc2d1	sousední
polynéských	polynéský	k2eAgInPc2d1	polynéský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
16	[number]	k4	16
%	%	kIx~	%
jsou	být	k5eAaImIp3nP	být
míšenci	míšenec	k1gMnPc1	míšenec
<g/>
,	,	kIx,	,
malou	malý	k2eAgFnSc7d1	malá
minoritou	minorita	k1gFnSc7	minorita
jsou	být	k5eAaImIp3nP	být
běloši	běloch	k1gMnPc1	běloch
-	-	kIx~	-
Novozélanďané	Novozélanďan	k1gMnPc1	Novozélanďan
a	a	k8xC	a
Evropané	Evropan	k1gMnPc1	Evropan
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
obyv	obyva	k1gFnPc2	obyva
<g/>
.	.	kIx.	.
<g/>
(	(	kIx(	(
<g/>
asi	asi	k9	asi
70	[number]	k4	70
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
místní	místní	k2eAgFnSc3d1	místní
Křesťanské	křesťanský	k2eAgFnSc3d1	křesťanská
církvi	církev	k1gFnSc3	církev
Cookových	Cookův	k2eAgInPc2d1	Cookův
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
další	další	k2eAgMnPc1d1	další
jsou	být	k5eAaImIp3nP	být
římští	římský	k2eAgMnPc1d1	římský
katolíci	katolík	k1gMnPc1	katolík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Novozélandští	novozélandský	k2eAgMnPc1d1	novozélandský
vysocí	vysoký	k2eAgMnPc1d1	vysoký
komisaři	komisar	k1gMnPc1	komisar
<g/>
:	:	kIx,	:
1975-1984	[number]	k4	1975-1984
Sir	sir	k1gMnSc1	sir
Gaven	Gavna	k1gFnPc2	Gavna
Donne	Donn	k1gInSc5	Donn
<g/>
,	,	kIx,	,
1994-1998	[number]	k4	1994-1998
Darryl	Darryl	k1gInSc1	Darryl
Dunn	Dunn	k1gInSc4	Dunn
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
(	(	kIx(	(
<g/>
krátce	krátce	k6eAd1	krátce
<g/>
)	)	kIx)	)
James	James	k1gMnSc1	James
Kember	Kember	k1gMnSc1	Kember
<g/>
,	,	kIx,	,
od	od	k7c2	od
1998	[number]	k4	1998
Rob	roba	k1gFnPc2	roba
Moore-Jones	Moore-Jonesa	k1gFnPc2	Moore-Jonesa
<g/>
.	.	kIx.	.
</s>
<s>
Cookovy	Cookův	k2eAgInPc1d1	Cookův
ostrovy	ostrov	k1gInPc1	ostrov
jsou	být	k5eAaImIp3nP	být
suverénním	suverénní	k2eAgInSc7d1	suverénní
subjektem	subjekt	k1gInSc7	subjekt
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
plnoprávně	plnoprávně	k6eAd1	plnoprávně
vstupujícím	vstupující	k2eAgMnSc7d1	vstupující
do	do	k7c2	do
mezinárodních	mezinárodní	k2eAgMnPc2d1	mezinárodní
vztahů	vztah	k1gInPc2	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc7d3	veliký
překážkou	překážka	k1gFnSc7	překážka
členství	členství	k1gNnSc2	členství
Cookových	Cookův	k2eAgInPc2d1	Cookův
ostrovů	ostrov	k1gInPc2	ostrov
v	v	k7c6	v
OSN	OSN	kA	OSN
je	být	k5eAaImIp3nS	být
neexistence	neexistence	k1gFnSc1	neexistence
občanství	občanství	k1gNnSc2	občanství
Cookových	Cookův	k2eAgInPc2d1	Cookův
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Řada	řada	k1gFnSc1	řada
států	stát	k1gInPc2	stát
s	s	k7c7	s
Cookovými	Cookův	k2eAgInPc7d1	Cookův
ostrovy	ostrov	k1gInPc7	ostrov
udržuje	udržovat	k5eAaImIp3nS	udržovat
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
styky	styk	k1gInPc4	styk
jako	jako	k8xC	jako
s	s	k7c7	s
nezávislým	závislý	k2eNgInSc7d1	nezávislý
státem	stát	k1gInSc7	stát
(	(	kIx(	(
<g/>
např.	např.	kA	např.
většina	většina	k1gFnSc1	většina
zemí	zem	k1gFnPc2	zem
západní	západní	k2eAgFnSc2d1	západní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc2d1	jižní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
Čína	Čína	k1gFnSc1	Čína
a	a	k8xC	a
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
navázala	navázat	k5eAaPmAgFnS	navázat
s	s	k7c7	s
Cookovými	Cookův	k2eAgInPc7d1	Cookův
ostrovy	ostrov	k1gInPc7	ostrov
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
styky	styk	k1gInPc4	styk
a	a	k8xC	a
uznala	uznat	k5eAaPmAgFnS	uznat
de	de	k?	de
iure	iur	k1gFnSc2	iur
jejich	jejich	k3xOp3gFnSc4	jejich
nezávislost	nezávislost	k1gFnSc4	nezávislost
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
dohody	dohoda	k1gFnSc2	dohoda
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
převzal	převzít	k5eAaPmAgInS	převzít
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
zahraniční	zahraniční	k2eAgInSc1d1	zahraniční
zastoupení	zastoupení	k1gNnSc2	zastoupení
a	a	k8xC	a
obranu	obrana	k1gFnSc4	obrana
Cookových	Cookův	k2eAgInPc2d1	Cookův
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
mají	mít	k5eAaImIp3nP	mít
Cookovy	Cookův	k2eAgInPc1d1	Cookův
ostrovy	ostrov	k1gInPc1	ostrov
vlastní	vlastní	k2eAgFnSc4d1	vlastní
nezávislou	závislý	k2eNgFnSc4d1	nezávislá
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
politiku	politika	k1gFnSc4	politika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oficiálním	oficiální	k2eAgNnSc7d1	oficiální
platidlem	platidlo	k1gNnSc7	platidlo
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Cookových	Cookův	k2eAgInPc6d1	Cookův
ostrovech	ostrov	k1gInPc6	ostrov
novozélandský	novozélandský	k2eAgInSc4d1	novozélandský
dolar	dolar	k1gInSc4	dolar
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
ISO	ISO	kA	ISO
4217	[number]	k4	4217
kód	kód	k1gInSc1	kód
je	být	k5eAaImIp3nS	být
NZD	NZD	kA	NZD
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
novozélandského	novozélandský	k2eAgInSc2d1	novozélandský
dolaru	dolar	k1gInSc2	dolar
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
i	i	k9	i
dolar	dolar	k1gInSc1	dolar
Cookových	Cookův	k2eAgInPc2d1	Cookův
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
místní	místní	k2eAgFnSc1d1	místní
verze	verze	k1gFnSc1	verze
novozélandského	novozélandský	k2eAgInSc2d1	novozélandský
dolaru	dolar	k1gInSc2	dolar
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
pevně	pevně	k6eAd1	pevně
navázána	navázán	k2eAgFnSc1d1	navázána
na	na	k7c4	na
mateřskou	mateřský	k2eAgFnSc4d1	mateřská
měnu	měna	k1gFnSc4	měna
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
platná	platný	k2eAgFnSc1d1	platná
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hlavy	hlava	k1gFnSc2	hlava
státu	stát	k1gInSc2	stát
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Commonwealthu	Commonwealth	k1gInSc6	Commonwealth
je	být	k5eAaImIp3nS	být
přidruženým	přidružený	k2eAgInSc7d1	přidružený
členem	člen	k1gInSc7	člen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Správní	správní	k2eAgNnSc4d1	správní
členění	členění	k1gNnSc4	členění
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
všech	všecek	k3xTgInPc6	všecek
obydlených	obydlený	k2eAgInPc6d1	obydlený
vnějších	vnější	k2eAgInPc6d1	vnější
ostrovech	ostrov	k1gInPc6	ostrov
jsou	být	k5eAaImIp3nP	být
ostrovní	ostrovní	k2eAgFnPc1d1	ostrovní
rady	rada	k1gFnPc1	rada
(	(	kIx(	(
<g/>
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
místní	místní	k2eAgFnSc6d1	místní
vládě	vláda	k1gFnSc6	vláda
na	na	k7c6	na
vnějších	vnější	k2eAgInPc6d1	vnější
ostrovech	ostrov	k1gInPc6	ostrov
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
s	s	k7c7	s
pozměňovacími	pozměňovací	k2eAgInPc7d1	pozměňovací
návrhy	návrh	k1gInPc7	návrh
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
a	a	k8xC	a
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
místní	místní	k2eAgFnSc6d1	místní
správě	správa	k1gFnSc6	správa
ostrova	ostrov	k1gInSc2	ostrov
Palmerston	Palmerston	k1gInSc1	Palmerston
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Nassau	Nassaus	k1gInSc2	Nassaus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
řídí	řídit	k5eAaImIp3nS	řídit
Pukapukou	Pukapuka	k1gFnSc7	Pukapuka
(	(	kIx(	(
<g/>
Suwarrow	Suwarrow	k1gFnSc7	Suwarrow
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
jediný	jediný	k2eAgMnSc1d1	jediný
správce	správce	k1gMnSc1	správce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
řízený	řízený	k2eAgInSc1d1	řízený
Pukapukou	Pukapuka	k1gFnSc7	Pukapuka
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
kontextu	kontext	k1gInSc6	kontext
započítán	započítat	k5eAaPmNgInS	započítat
s	s	k7c7	s
obydlenými	obydlený	k2eAgInPc7d1	obydlený
ostrovy	ostrov	k1gInPc7	ostrov
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
rada	rada	k1gFnSc1	rada
je	být	k5eAaImIp3nS	být
vedena	vést	k5eAaImNgFnS	vést
starostou	starosta	k1gMnSc7	starosta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tři	tři	k4xCgNnPc1	tři
Vaka	Vakum	k1gNnPc1	Vakum
rady	rada	k1gFnSc2	rada
Rarotongy	Rarotong	k1gInPc1	Rarotong
zřízené	zřízený	k2eAgInPc1d1	zřízený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
(	(	kIx(	(
<g/>
Rarotonga	Rarotonga	k1gFnSc1	Rarotonga
Local	Local	k1gMnSc1	Local
Government	Government	k1gMnSc1	Government
Act	Act	k1gMnSc1	Act
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
vedené	vedený	k2eAgMnPc4d1	vedený
starosty	starosta	k1gMnPc4	starosta
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
i	i	k9	i
přes	přes	k7c4	přes
velkou	velký	k2eAgFnSc4d1	velká
diskusi	diskuse	k1gFnSc4	diskuse
zrušeny	zrušit	k5eAaPmNgInP	zrušit
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
nejnižší	nízký	k2eAgFnSc6d3	nejnižší
úrovni	úroveň	k1gFnSc6	úroveň
existují	existovat	k5eAaImIp3nP	existovat
vesnické	vesnický	k2eAgInPc1d1	vesnický
výbory	výbor	k1gInPc1	výbor
<g/>
.	.	kIx.	.
</s>
<s>
Nassau	Nassau	k5eAaPmIp1nS	Nassau
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
řízen	řídit	k5eAaImNgInS	řídit
Pukapukou	Pukapuka	k1gFnSc7	Pukapuka
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
ostrovní	ostrovní	k2eAgInSc1d1	ostrovní
výbor	výbor	k1gInSc1	výbor
(	(	kIx(	(
<g/>
Nassau	Nassaus	k1gInSc2	Nassaus
Island	Island	k1gInSc1	Island
Committee	Committe	k1gInPc1	Committe
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
řídí	řídit	k5eAaImIp3nS	řídit
Pukapuka	Pukapuk	k1gMnSc4	Pukapuk
ostrovní	ostrovní	k2eAgFnSc4d1	ostrovní
radu	rada	k1gFnSc4	rada
ve	v	k7c6	v
věcech	věc	k1gFnPc6	věc
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
vlastního	vlastní	k2eAgInSc2d1	vlastní
ostrova	ostrov	k1gInSc2	ostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Cookových	Cookův	k2eAgInPc2d1	Cookův
ostrovů	ostrov	k1gInPc2	ostrov
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Cook	Cook	k1gInSc1	Cook
Islands	Islands	k1gInSc1	Islands
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Cookovy	Cookův	k2eAgInPc1d1	Cookův
ostrovy	ostrov	k1gInPc1	ostrov
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
Cookovy	Cookův	k2eAgInPc4d1	Cookův
ostrovy	ostrov	k1gInPc4	ostrov
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
