<s>
Tom	Tom	k1gMnSc1
Martinsen	Martinsna	k1gFnPc2
</s>
<s>
Tom	Tom	k1gMnSc1
Martinsen	Martinsen	k2eAgMnSc1d1
Narození	narození	k1gNnSc2
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1943	#num#	k4
Úmrtí	úmrť	k1gFnPc2
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2007	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
63	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Povolání	povolání	k1gNnPc2
</s>
<s>
fotograf	fotograf	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Tom	Tom	k1gMnSc1
Martinsen	Martinsna	k1gFnPc2
(	(	kIx(
<g/>
4	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1943	#num#	k4
–	–	k?
31	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2007	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
norský	norský	k2eAgMnSc1d1
fotograf	fotograf	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Životopis	životopis	k1gInSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
Tø	Tø	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
1968	#num#	k4
<g/>
–	–	k?
<g/>
1971	#num#	k4
studoval	studovat	k5eAaImAgMnS
fotografii	fotografia	k1gFnSc4
a	a	k8xC
dokumentární	dokumentární	k2eAgInSc4d1
film	film	k1gInSc4
u	u	k7c2
Christera	Christer	k1gMnSc2
Strömholma	Strömholm	k1gMnSc2
ve	v	k7c6
Stockholmu	Stockholm	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sám	sám	k3xTgMnSc1
popsal	popsat	k5eAaPmAgMnS
setkání	setkání	k1gNnSc4
se	s	k7c7
školou	škola	k1gFnSc7
a	a	k8xC
Strömholmem	Strömholmo	k1gNnSc7
jako	jako	k9
„	„	k?
<g/>
dostat	dostat	k5eAaPmF
pánví	pánev	k1gFnSc7
do	do	k7c2
hlavy	hlava	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roky	rok	k1gInPc4
ve	v	k7c6
Fotoskolanu	Fotoskolan	k1gInSc6
byly	být	k5eAaImAgInP
pro	pro	k7c4
Martinsenův	Martinsenův	k2eAgInSc4d1
pohled	pohled	k1gInSc4
na	na	k7c4
fotografie	fotografia	k1gFnPc4
rozhodující	rozhodující	k2eAgFnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1973	#num#	k4
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
zpravodajským	zpravodajský	k2eAgMnSc7d1
fotografem	fotograf	k1gMnSc7
pro	pro	k7c4
noviny	novina	k1gFnPc4
Dagbladet	Dagbladeta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1972	#num#	k4
získal	získat	k5eAaPmAgMnS
cenu	cena	k1gFnSc4
za	za	k7c4
Fotografii	fotografia	k1gFnSc4
roku	rok	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
spoluzakladatelem	spoluzakladatel	k1gMnSc7
galerie	galerie	k1gFnSc2
Fotogalleriet	Fotogallerieta	k1gFnPc2
v	v	k7c6
Oslu	Oslo	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
1977	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Martinsenovy	Martinsenův	k2eAgFnPc1d1
fotografie	fotografia	k1gFnPc1
byly	být	k5eAaImAgFnP
zakoupeny	zakoupit	k5eAaPmNgFnP
Norskou	norský	k2eAgFnSc7d1
kulturní	kulturní	k2eAgFnSc7d1
radou	rada	k1gFnSc7
<g/>
,	,	kIx,
Norským	norský	k2eAgNnSc7d1
muzeem	muzeum	k1gNnSc7
fotografie	fotografia	k1gFnSc2
–	–	k?
Preus	Preus	k1gInSc1
museum	museum	k1gNnSc1
v	v	k7c4
Hortenu	Horten	k2eAgFnSc4d1
<g/>
,	,	kIx,
Lillehammer	Lillehammer	k1gInSc4
Art	Art	k1gFnPc2
Museum	museum	k1gNnSc1
a	a	k8xC
Moderna	Moderna	k1gFnSc1
Museet	Museeta	k1gFnPc2
ve	v	k7c6
Stockholmu	Stockholm	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Nová	nový	k2eAgFnSc1d1
generace	generace	k1gFnSc1
norských	norský	k2eAgMnPc2d1
fotožurnalistů	fotožurnalista	k1gMnPc2
</s>
<s>
Patřila	patřit	k5eAaImAgFnS
k	k	k7c3
ní	on	k3xPp3gFnSc3
generace	generace	k1gFnPc1
fotografů	fotograf	k1gMnPc2
Toma	Tom	k1gMnSc2
Martinsena	Martinsen	k2eAgFnSc1d1
a	a	k8xC
s	s	k7c7
jeho	jeho	k3xOp3gNnSc7
fotografickým	fotografický	k2eAgNnSc7d1
vzděláním	vzdělání	k1gNnSc7
představoval	představovat	k5eAaImAgMnS
generační	generační	k2eAgFnSc4d1
změnu	změna	k1gFnSc4
v	v	k7c6
norské	norský	k2eAgFnSc6d1
novinářské	novinářský	k2eAgFnSc6d1
fotografii	fotografia	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejen	nejen	k6eAd1
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
měl	mít	k5eAaImAgMnS
fotografické	fotografický	k2eAgNnSc4d1
vzdělání	vzdělání	k1gNnSc4
(	(	kIx(
<g/>
fotožurnalisté	fotožurnalista	k1gMnPc5
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
málokdy	málokdy	k6eAd1
měli	mít	k5eAaImAgMnP
významné	významný	k2eAgNnSc4d1
odborné	odborný	k2eAgNnSc4d1
vzdělání	vzdělání	k1gNnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
díky	díky	k7c3
svému	svůj	k3xOyFgInSc3
přístupu	přístup	k1gInSc3
k	k	k7c3
fotografii	fotografia	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednou	jeden	k4xCgFnSc7
řekl	říct	k5eAaPmAgInS
<g/>
:	:	kIx,
„	„	k?
<g/>
ohnisková	ohniskový	k2eAgFnSc1d1
vzdálenost	vzdálenost	k1gFnSc1
je	být	k5eAaImIp3nS
věcí	věc	k1gFnSc7
morálky	morálka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teleobjektiv	teleobjektiv	k1gInSc1
je	být	k5eAaImIp3nS
nemorální	morální	k2eNgFnSc4d1
věc	věc	k1gFnSc4
<g/>
,	,	kIx,
pokud	pokud	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
lidi	člověk	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Se	s	k7c7
širokým	široký	k2eAgInSc7d1
objektivem	objektiv	k1gInSc7
se	se	k3xPyFc4
musíte	muset	k5eAaImIp2nP
ukázat	ukázat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postavíte	postavit	k5eAaPmIp2nP
se	se	k3xPyFc4
na	na	k7c4
pozici	pozice	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
je	být	k5eAaImIp3nS
otevřená	otevřený	k2eAgFnSc1d1
a	a	k8xC
čestná	čestný	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Některé	některý	k3yIgFnPc1
z	z	k7c2
nejslavnějších	slavný	k2eAgFnPc2d3
fotografií	fotografia	k1gFnPc2
v	v	k7c6
norských	norský	k2eAgNnPc6d1
médiích	médium	k1gNnPc6
z	z	k7c2
posledních	poslední	k2eAgNnPc2d1
let	léto	k1gNnPc2
pocházejí	pocházet	k5eAaImIp3nP
z	z	k7c2
ruky	ruka	k1gFnSc2
a	a	k8xC
fotoaparátu	fotoaparát	k1gInSc2
Toma	Tom	k1gMnSc2
Martinsena	Martinsen	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příkladem	příklad	k1gInSc7
tohoto	tento	k3xDgInSc2
jsou	být	k5eAaImIp3nP
jeho	jeho	k3xOp3gFnPc1
známé	známý	k2eAgFnPc1d1
fotografie	fotografia	k1gFnPc1
<g/>
:	:	kIx,
osvícený	osvícený	k2eAgMnSc1d1
Bob	Bob	k1gMnSc1
Dylan	Dylan	k1gMnSc1
(	(	kIx(
<g/>
Scandinavium	Scandinavium	k1gNnSc1
<g/>
,	,	kIx,
Göteborg	Göteborg	k1gInSc1
<g/>
)	)	kIx)
fotografovaný	fotografovaný	k2eAgMnSc1d1
v	v	k7c6
zákulisí	zákulisí	k1gNnSc6
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
barevný	barevný	k2eAgInSc1d1
portrét	portrét	k1gInSc1
spisovatele	spisovatel	k1gMnSc2
Daga	Dagus	k1gMnSc2
Solstada	Solstada	k1gFnSc1
na	na	k7c6
noční	noční	k2eAgFnSc6d1
zasněžené	zasněžený	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Martinsen	Martinsen	k1gInSc1
byl	být	k5eAaImAgInS
také	také	k9
vášnivým	vášnivý	k2eAgMnSc7d1
fotografem	fotograf	k1gMnSc7
přírody	příroda	k1gFnSc2
a	a	k8xC
byl	být	k5eAaImAgInS
také	také	k9
známý	známý	k2eAgInSc1d1
svými	svůj	k3xOyFgFnPc7
přírodovědnými	přírodovědný	k2eAgFnPc7d1
studiemi	studie	k1gFnPc7
v	v	k7c6
černobílém	černobílý	k2eAgNnSc6d1
provedení	provedení	k1gNnSc6
<g/>
,	,	kIx,
zejména	zejména	k9
z	z	k7c2
Tjø	Tjø	k1gInSc5
Vestfold	Vestfolda	k1gFnPc2
a	a	k8xC
krajiny	krajina	k1gFnSc2
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s>
Martinsen	Martinsno	k1gNnPc2
mnoho	mnoho	k4c4
let	léto	k1gNnPc2
editoval	editovat	k5eAaImAgInS
pravidelný	pravidelný	k2eAgInSc1d1
sloupec	sloupec	k1gInSc1
Moment	moment	k1gInSc1
v	v	k7c6
sobotní	sobotní	k2eAgFnSc6d1
příloze	příloha	k1gFnSc6
Magasinet	Magasinet	k1gMnSc1
novin	novina	k1gFnPc2
Dagbladet	Dagbladet	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgInSc1
týden	týden	k1gInSc1
popisoval	popisovat	k5eAaImAgInS
klíčová	klíčový	k2eAgNnPc4d1
díla	dílo	k1gNnPc4
z	z	k7c2
historie	historie	k1gFnSc2
fotografie	fotografia	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Název	název	k1gInSc1
sloupce	sloupec	k1gInSc2
vzal	vzít	k5eAaPmAgInS
z	z	k7c2
výrazu	výraz	k1gInSc2
Henri	Henr	k1gFnSc2
Cartier-Bressona	Cartier-Bresson	k1gMnSc2
„	„	k?
<g/>
rozhodující	rozhodující	k2eAgInSc4d1
okamžik	okamžik	k1gInSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
Bresson	Bresson	k1gInSc1
definoval	definovat	k5eAaBmAgInS
jako	jako	k9
„	„	k?
<g/>
současné	současný	k2eAgNnSc1d1
zachycení	zachycení	k1gNnSc1
významného	významný	k2eAgInSc2d1
okamžiku	okamžik	k1gInSc2
ve	v	k7c6
zlomku	zlomek	k1gInSc6
vteřiny	vteřina	k1gFnSc2
<g/>
,	,	kIx,
jakož	jakož	k8xC
i	i	k9
přesné	přesný	k2eAgNnSc4d1
uspořádání	uspořádání	k1gNnSc4
prvků	prvek	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
dají	dát	k5eAaPmIp3nP
události	událost	k1gFnPc4
správný	správný	k2eAgInSc4d1
výraz	výraz	k1gInSc4
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výstavy	výstava	k1gFnPc1
</s>
<s>
Bilder	Bilder	k1gInSc1
fra	fra	k?
Verdens	Verdens	k1gInSc1
Ende	Ende	k1gInSc1
<g/>
,	,	kIx,
Fotogalleriet	Fotogalleriet	k1gInSc1
<g/>
,	,	kIx,
Oslo	Oslo	k1gNnSc1
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Separatutstilling	Separatutstilling	k1gInSc1
<g/>
,	,	kIx,
Preus	Preus	k1gInSc1
Fotomuseum	Fotomuseum	k1gInSc1
<g/>
,	,	kIx,
Horten	Horten	k2eAgInSc1d1
</s>
<s>
Min	min	kA
egen	egen	k1gMnSc1
road	road	k1gMnSc1
movie	movie	k1gFnSc1
<g/>
,	,	kIx,
Filmfestivalen	Filmfestivalen	k2eAgInSc1d1
<g/>
,	,	kIx,
Haugesund	Haugesund	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vest	vesta	k1gFnPc2
–	–	k?
en	en	k?
veifortelling	veifortelling	k1gInSc1
<g/>
,	,	kIx,
Fotogalleriet	Fotogalleriet	k1gInSc1
<g/>
,	,	kIx,
Oslo	Oslo	k1gNnSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Lillehammer	Lillehammer	k1gInSc1
kunstmuseum	kunstmuseum	k1gInSc1
<g/>
,	,	kIx,
Lillehammer	Lillehammer	k1gInSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
med	med	k1gInSc1
Rolf	Rolf	k1gMnSc1
M.	M.	kA
Aagard	Aagard	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
samostatná	samostatný	k2eAgFnSc1d1
výstava	výstava	k1gFnSc1
</s>
<s>
Kunstforeningen	Kunstforeningen	k1gInSc1
Verdens	Verdensa	k1gFnPc2
Ende	End	k1gInSc2
<g/>
,	,	kIx,
Tjø	Tjø	k1gMnSc5
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
samostatná	samostatný	k2eAgFnSc1d1
výstava	výstava	k1gFnSc1
</s>
<s>
Nord	Nord	k6eAd1
for	forum	k1gNnPc2
havets	havets	k6eAd1
ende	endat	k5eAaPmIp3nS
<g/>
,	,	kIx,
Preus	Preus	k1gInSc1
Fotomuseum	Fotomuseum	k1gInSc1
<g/>
,	,	kIx,
Horten	Horten	k2eAgMnSc1d1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Knihy	kniha	k1gFnPc1
</s>
<s>
Mezi	mezi	k7c4
jeho	jeho	k3xOp3gFnPc4
nejznámější	známý	k2eAgFnPc4d3
publikace	publikace	k1gFnPc4
patří	patřit	k5eAaImIp3nS
kniha	kniha	k1gFnSc1
118	#num#	k4
ø	ø	k1gFnPc2
(	(	kIx(
<g/>
118	#num#	k4
momentů	moment	k1gInPc2
<g/>
)	)	kIx)
z	z	k7c2
roku	rok	k1gInSc2
2003	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
118	#num#	k4
Ø	Ø	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Norske	Norsk	k1gInSc2
reportasjefotografier	reportasjefotografier	k1gMnSc1
utvalgt	utvalgt	k1gMnSc1
og	og	k?
presentert	presentert	k1gMnSc1
av	av	k?
Tom	Tom	k1gMnSc1
Martinsen	Martinsen	k2eAgMnSc1d1
Dinamo	Dinama	k1gFnSc5
Forlag	Forlag	k1gMnSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
ISBN	ISBN	kA
82-8071-052-3	82-8071-052-3	k4
</s>
<s>
Tom	Tom	k1gMnSc1
Martinsen	Martinsen	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rolf	Rolf	k1gInSc1
M.	M.	kA
Aagard	Aagard	k1gInSc1
Labyrinth	Labyrinth	k1gInSc1
Press	Press	k1gInSc1
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Tom	Tom	k1gMnSc1
Martinsen	Martinsna	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Skau	Skaa	k1gFnSc4
<g/>
,	,	kIx,
Enok	Enok	k1gInSc4
a	a	k8xC
další	další	k2eAgInSc4d1
Norske	Norske	k1gInSc4
pressefotografer	pressefotografra	k1gFnPc2
<g/>
,	,	kIx,
J.	J.	kA
M.	M.	kA
Stenersens	Stenersensa	k1gFnPc2
Forlag	Forlaga	k1gFnPc2
<g/>
,	,	kIx,
Oslo	Oslo	k1gNnSc7
1986	#num#	k4
ISBN	ISBN	kA
82	#num#	k4
<g/>
-	-	kIx~
<g/>
7201	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
42	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
↑	↑	k?
Listir	Listir	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Tek	teka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
no	no	k9
-	-	kIx~
Tester	tester	k1gInSc1
<g/>
,	,	kIx,
guider	guider	k1gInSc1
<g/>
,	,	kIx,
teknologi	teknologi	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tek	teka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
no	no	k9
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
norsky	norsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
The	The	k1gMnPc1
Decisive	Decisiev	k1gFnSc2
Moment	moment	k1gInSc1
by	by	kYmCp3nP
Henri	Henr	k1gFnPc1
Cartier-Bresson	Cartier-Bressona	k1gFnPc2
<g/>
.	.	kIx.
www.e-photobooks.com	www.e-photobooks.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Store	Stor	k1gInSc5
norske	norske	k1gNnSc3
leksikon	leksikon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tom	Tom	k1gMnSc1
Martinsen	Martinsen	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Redakce	redakce	k1gFnSc1
Bolstad	Bolstad	k1gInSc4
Erik	Erik	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oslo	Oslo	k1gNnSc1
<g/>
:	:	kIx,
Norsk	Norsk	k1gInSc1
nettleksikon	nettleksikon	k1gNnSc1
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
norsky	norsky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
encyclopedia	encyclopedium	k1gNnPc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Fotografie	fotografia	k1gFnPc1
v	v	k7c6
Norsku	Norsko	k1gNnSc6
</s>
<s>
Seznam	seznam	k1gInSc1
norských	norský	k2eAgMnPc2d1
fotografů	fotograf	k1gMnPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Tom	Tom	k1gMnSc1
Martinsen	Martinsen	k2eAgMnSc1d1
na	na	k7c6
fotografi	fotograf	k1gFnSc6
<g/>
.	.	kIx.
<g/>
no	no	k9
</s>
<s>
Tom	Tom	k1gMnSc1
Martinsen	Martinsen	k2eAgMnSc1d1
v	v	k7c6
muzeu	muzeum	k1gNnSc6
Preus	Preus	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Fotografie	fotografia	k1gFnPc4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
5150	#num#	k4
1690	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
nr	nr	k?
<g/>
2006000906	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
44258968	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-nr	lccn-nr	k1gInSc1
<g/>
2006000906	#num#	k4
</s>
