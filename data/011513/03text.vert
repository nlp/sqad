<p>
<s>
Burkina	Burkina	k1gMnSc1	Burkina
Faso	Faso	k1gMnSc1	Faso
je	být	k5eAaImIp3nS	být
vnitrozemský	vnitrozemský	k2eAgInSc4d1	vnitrozemský
stát	stát	k1gInSc4	stát
ležící	ležící	k2eAgInSc4d1	ležící
v	v	k7c6	v
Západní	západní	k2eAgFnSc6d1	západní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc4	tento
stát	stát	k1gInSc4	stát
jmenoval	jmenovat	k5eAaBmAgMnS	jmenovat
republika	republika	k1gFnSc1	republika
Horní	horní	k2eAgFnSc1d1	horní
Volta	Volta	k1gFnSc1	Volta
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
přejmenován	přejmenovat	k5eAaPmNgInS	přejmenovat
na	na	k7c4	na
Burkina	Burkin	k2eAgMnSc4d1	Burkin
Faso	Faso	k6eAd1	Faso
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
republika	republika	k1gFnSc1	republika
Země	zem	k1gFnSc2	zem
spravedlivých	spravedlivý	k2eAgInPc2d1	spravedlivý
<g/>
.	.	kIx.	.
</s>
<s>
Hraničí	hraničit	k5eAaImIp3nS	hraničit
se	s	k7c7	s
šesti	šest	k4xCc7	šest
státy	stát	k1gInPc7	stát
<g/>
:	:	kIx,	:
Mali	Mali	k1gNnPc6	Mali
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
Nigerem	Niger	k1gInSc7	Niger
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
Beninem	Benin	k1gInSc7	Benin
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
<g/>
,	,	kIx,	,
Togem	Togo	k1gNnSc7	Togo
a	a	k8xC	a
Ghanou	Ghana	k1gFnSc7	Ghana
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
a	a	k8xC	a
s	s	k7c7	s
Pobřeží	pobřeží	k1gNnSc2	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
274	[number]	k4	274
200	[number]	k4	200
km	km	kA	km
<g/>
2	[number]	k4	2
žije	žít	k5eAaImIp3nS	žít
15,2	[number]	k4	15,2
miliónů	milión	k4xCgInPc2	milión
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Ouagadougou	Ouagadouga	k1gFnSc7	Ouagadouga
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Předkoloniální	Předkoloniální	k2eAgNnPc4d1	Předkoloniální
a	a	k8xC	a
koloniální	koloniální	k2eAgNnPc4d1	koloniální
období	období	k1gNnPc4	období
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
11	[number]	k4	11
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
stoletím	století	k1gNnSc7	století
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgInSc2d1	dnešní
státu	stát	k1gInSc2	stát
silné	silný	k2eAgInPc1d1	silný
mossijské	mossijský	k2eAgInPc1d1	mossijský
státy	stát	k1gInPc1	stát
(	(	kIx(	(
<g/>
Vagadugu-Ouagadougou	Vagadugu-Ouagadouga	k1gFnSc7	Vagadugu-Ouagadouga
<g/>
,	,	kIx,	,
Jatenga	Jatenga	k1gFnSc1	Jatenga
<g/>
,	,	kIx,	,
Dagomba	Dagomba	k1gFnSc1	Dagomba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
s	s	k7c7	s
to	ten	k3xDgNnSc4	ten
vzdorovat	vzdorovat	k5eAaImF	vzdorovat
expanzi	expanze	k1gFnSc4	expanze
sousedů	soused	k1gMnPc2	soused
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
si	se	k3xPyFc3	se
zajistila	zajistit	k5eAaPmAgFnS	zajistit
kontrolu	kontrola	k1gFnSc4	kontrola
území	území	k1gNnSc2	území
až	až	k9	až
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
ustanovila	ustanovit	k5eAaPmAgFnS	ustanovit
Francie	Francie	k1gFnSc1	Francie
protektorát	protektorát	k1gInSc1	protektorát
nad	nad	k7c7	nad
královstvím	království	k1gNnSc7	království
Wagadugu	Wagadug	k1gInSc2	Wagadug
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
1904-1919	[number]	k4	1904-1919
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
území	území	k1gNnSc1	území
součástí	součást	k1gFnPc2	součást
kolonie	kolonie	k1gFnSc2	kolonie
Horní	horní	k2eAgInSc4d1	horní
Senegal-Niger	Senegal-Niger	k1gInSc4	Senegal-Niger
(	(	kIx(	(
<g/>
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
Západní	západní	k2eAgFnSc2d1	západní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1919-1932	[number]	k4	1919-1932
fungovala	fungovat	k5eAaImAgFnS	fungovat
už	už	k6eAd1	už
jako	jako	k8xC	jako
samostatná	samostatný	k2eAgFnSc1d1	samostatná
kolonie	kolonie	k1gFnSc1	kolonie
Horní	horní	k2eAgFnSc1d1	horní
Volta	Volta	k1gFnSc1	Volta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1932-47	[number]	k4	1932-47
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
území	území	k1gNnSc1	území
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
mezi	mezi	k7c4	mezi
Francouzský	francouzský	k2eAgInSc4d1	francouzský
Súdán	Súdán	k1gInSc4	Súdán
<g/>
,	,	kIx,	,
Pobřeží	pobřeží	k1gNnSc4	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
a	a	k8xC	a
Niger	Niger	k1gInSc1	Niger
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1947	[number]	k4	1947
byla	být	k5eAaImAgFnS	být
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Burkina	Burkina	k1gFnSc1	Burkina
Faso	Faso	k6eAd1	Faso
obnovena	obnoven	k2eAgFnSc1d1	obnovena
v	v	k7c6	v
původních	původní	k2eAgFnPc6d1	původní
hranicích	hranice	k1gFnPc6	hranice
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
jako	jako	k8xC	jako
zámořské	zámořský	k2eAgNnSc4d1	zámořské
území	území	k1gNnSc4	území
Horní	horní	k2eAgFnSc1d1	horní
Volta	Volta	k1gFnSc1	Volta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Samostatnost	samostatnost	k1gFnSc4	samostatnost
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1958	[number]	k4	1958
získala	získat	k5eAaPmAgFnS	získat
statut	statut	k1gInSc4	statut
autonomní	autonomní	k2eAgFnSc2d1	autonomní
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Francouzského	francouzský	k2eAgNnSc2d1	francouzské
společenství	společenství	k1gNnSc2	společenství
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
5.8	[number]	k4	5.8
<g/>
.1960	.1960	k4	.1960
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
nezávislost	nezávislost	k1gFnSc1	nezávislost
republiky	republika	k1gFnSc2	republika
Horní	horní	k2eAgFnSc1d1	horní
Volta	Volta	k1gFnSc1	Volta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1966	[number]	k4	1966
prezident	prezident	k1gMnSc1	prezident
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
výjimečný	výjimečný	k2eAgInSc4d1	výjimečný
stav	stav	k1gInSc4	stav
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
proveden	provést	k5eAaPmNgInS	provést
vojenský	vojenský	k2eAgInSc1d1	vojenský
převrat	převrat	k1gInSc1	převrat
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
státu	stát	k1gInSc2	stát
příslušníci	příslušník	k1gMnPc1	příslušník
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
kapitán	kapitán	k1gMnSc1	kapitán
Thomas	Thomas	k1gMnSc1	Thomas
Sankara	Sankara	k1gFnSc1	Sankara
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
ustanovena	ustanoven	k2eAgFnSc1d1	ustanovena
vojensko-civilní	vojenskoivilní	k2eAgFnSc1d1	vojensko-civilní
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
hleděla	hledět	k5eAaImAgFnS	hledět
do	do	k7c2	do
budoucnosti	budoucnost	k1gFnSc2	budoucnost
s	s	k7c7	s
optimismem	optimismus	k1gInSc7	optimismus
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
změnila	změnit	k5eAaPmAgFnS	změnit
jméno	jméno	k1gNnSc4	jméno
a	a	k8xC	a
symboly	symbol	k1gInPc4	symbol
země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
,	,	kIx,	,
hymnu	hymna	k1gFnSc4	hymna
i	i	k8xC	i
ústavu	ústava	k1gFnSc4	ústava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
této	tento	k3xDgFnSc2	tento
vlády	vláda	k1gFnSc2	vláda
však	však	k9	však
byl	být	k5eAaImAgInS	být
nastolen	nastolen	k2eAgInSc1d1	nastolen
socialismus	socialismus	k1gInSc1	socialismus
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
postupnému	postupný	k2eAgNnSc3d1	postupné
zestátňování	zestátňování	k1gNnSc3	zestátňování
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
slíbila	slíbit	k5eAaPmAgFnS	slíbit
emancipaci	emancipace	k1gFnSc3	emancipace
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
zdvojnásobení	zdvojnásobení	k1gNnSc4	zdvojnásobení
pracovních	pracovní	k2eAgFnPc2d1	pracovní
příležitostí	příležitost	k1gFnPc2	příležitost
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
však	však	k9	však
zakázala	zakázat	k5eAaPmAgFnS	zakázat
politické	politický	k2eAgFnPc4d1	politická
strany	strana	k1gFnPc4	strana
a	a	k8xC	a
rozpustila	rozpustit	k5eAaPmAgFnS	rozpustit
parlament	parlament	k1gInSc4	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
bylo	být	k5eAaImAgNnS	být
touto	tento	k3xDgFnSc7	tento
vládou	vláda	k1gFnSc7	vláda
změněno	změněn	k2eAgNnSc4d1	změněno
koloniální	koloniální	k2eAgNnSc4d1	koloniální
jméno	jméno	k1gNnSc4	jméno
Horní	horní	k2eAgFnSc1d1	horní
Volta	Volta	k1gFnSc1	Volta
na	na	k7c4	na
Burkinu	Burkina	k1gFnSc4	Burkina
Faso	Faso	k6eAd1	Faso
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
státní	státní	k2eAgInSc1d1	státní
převrat	převrat	k1gInSc1	převrat
následoval	následovat	k5eAaImAgInS	následovat
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
když	když	k8xS	když
Sankara	Sankar	k1gMnSc4	Sankar
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
k	k	k7c3	k
založení	založení	k1gNnSc3	založení
jediné	jediný	k2eAgFnSc2d1	jediná
socialistické	socialistický	k2eAgFnSc2d1	socialistická
politické	politický	k2eAgFnSc2d1	politická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
však	však	k9	však
pro	pro	k7c4	pro
lidovou	lidový	k2eAgFnSc4d1	lidová
frontu	fronta	k1gFnSc4	fronta
vedenou	vedený	k2eAgFnSc4d1	vedená
kapitánem	kapitán	k1gMnSc7	kapitán
Blaisem	Blais	k1gInSc7	Blais
Compaorém	Compaorý	k2eAgInSc6d1	Compaorý
příliš	příliš	k6eAd1	příliš
<g/>
.	.	kIx.	.
</s>
<s>
Vojenské	vojenský	k2eAgNnSc1d1	vojenské
komando	komando	k1gNnSc1	komando
zavraždilo	zavraždit	k5eAaPmAgNnS	zavraždit
Sankaru	Sankara	k1gFnSc4	Sankara
i	i	k8xC	i
třináct	třináct	k4xCc1	třináct
jeho	jeho	k3xOp3gMnPc2	jeho
nejbližších	blízký	k2eAgMnPc2d3	nejbližší
spolupracovníků	spolupracovník	k1gMnPc2	spolupracovník
a	a	k8xC	a
připravilo	připravit	k5eAaPmAgNnS	připravit
Lidové	lidový	k2eAgFnSc3d1	lidová
frontě	fronta	k1gFnSc3	fronta
cestu	cesta	k1gFnSc4	cesta
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
zavedl	zavést	k5eAaPmAgMnS	zavést
Compaoré	Compaorý	k2eAgFnPc4d1	Compaorý
hospodářské	hospodářský	k2eAgFnPc4d1	hospodářská
reformy	reforma	k1gFnPc4	reforma
podporující	podporující	k2eAgInSc4d1	podporující
soukromý	soukromý	k2eAgInSc4d1	soukromý
sektor	sektor	k1gInSc4	sektor
a	a	k8xC	a
zahraniční	zahraniční	k2eAgFnPc4d1	zahraniční
investice	investice	k1gFnPc4	investice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
se	se	k3xPyFc4	se
však	však	k9	však
změnilo	změnit	k5eAaPmAgNnS	změnit
málo	málo	k1gNnSc1	málo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Současnost	současnost	k1gFnSc4	současnost
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
velké	velký	k2eAgFnSc3d1	velká
akumulaci	akumulace	k1gFnSc3	akumulace
moci	moc	k1gFnSc2	moc
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1992	[number]	k4	1992
se	se	k3xPyFc4	se
bídou	bída	k1gFnSc7	bída
sužovaná	sužovaný	k2eAgFnSc1d1	sužovaná
země	země	k1gFnSc1	země
pokusila	pokusit	k5eAaPmAgFnS	pokusit
o	o	k7c4	o
první	první	k4xOgInSc4	první
krok	krok	k1gInSc4	krok
k	k	k7c3	k
demokracii	demokracie	k1gFnSc3	demokracie
konáním	konání	k1gNnSc7	konání
voleb	volba	k1gFnPc2	volba
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
však	však	k9	však
kýženou	kýžený	k2eAgFnSc4d1	kýžená
změnu	změna	k1gFnSc4	změna
nepřinesly	přinést	k5eNaPmAgInP	přinést
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
se	se	k3xPyFc4	se
Compaoré	Compaorý	k2eAgNnSc4d1	Compaorý
opět	opět	k6eAd1	opět
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
státu	stát	k1gInSc2	stát
díky	díky	k7c3	díky
drtivému	drtivý	k2eAgNnSc3d1	drtivé
volebnímu	volební	k2eAgNnSc3d1	volební
vítězství	vítězství	k1gNnSc3	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Vojenský	vojenský	k2eAgInSc1d1	vojenský
převrat	převrat	k1gInSc1	převrat
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
byl	být	k5eAaImAgInS	být
neúspěšný	úspěšný	k2eNgInSc1d1	neúspěšný
a	a	k8xC	a
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
Compaoré	Compaorý	k2eAgInPc4d1	Compaorý
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
čela	čelo	k1gNnSc2	čelo
Burkiny	Burkina	k1gFnSc2	Burkina
Faso	Faso	k6eAd1	Faso
i	i	k9	i
po	po	k7c6	po
třetí	třetí	k4xOgFnSc6	třetí
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
vláda	vláda	k1gFnSc1	vláda
byla	být	k5eAaImAgFnS	být
poznamenána	poznamenat	k5eAaPmNgFnS	poznamenat
diktátorskými	diktátorský	k2eAgInPc7d1	diktátorský
prvky	prvek	k1gInPc7	prvek
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
silnou	silný	k2eAgFnSc7d1	silná
mocí	moc	k1gFnSc7	moc
tajné	tajný	k2eAgFnSc2d1	tajná
policie	policie	k1gFnSc2	policie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
sloužila	sloužit	k5eAaImAgFnS	sloužit
k	k	k7c3	k
pronásledování	pronásledování	k1gNnSc3	pronásledování
a	a	k8xC	a
mučení	mučení	k1gNnSc6	mučení
mnoha	mnoho	k4c2	mnoho
odpůrců	odpůrce	k1gMnPc2	odpůrce
Compaorého	Compaorý	k2eAgInSc2d1	Compaorý
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
názoru	názor	k1gInSc2	názor
veřejného	veřejný	k2eAgNnSc2d1	veřejné
mínění	mínění	k1gNnSc2	mínění
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
lidskoprávních	lidskoprávní	k2eAgFnPc2d1	lidskoprávní
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
volby	volba	k1gFnPc1	volba
během	během	k7c2	během
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
silně	silně	k6eAd1	silně
falšovány	falšován	k2eAgFnPc1d1	falšována
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
října	říjen	k1gInSc2	říjen
2014	[number]	k4	2014
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
Compaorého	Compaorý	k2eAgInSc2d1	Compaorý
snaze	snaha	k1gFnSc6	snaha
dál	daleko	k6eAd2	daleko
vést	vést	k5eAaImF	vést
zemi	zem	k1gFnSc4	zem
zvedla	zvednout	k5eAaPmAgFnS	zvednout
vlna	vlna	k1gFnSc1	vlna
nepokojů	nepokoj	k1gInPc2	nepokoj
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
demonstranti	demonstrant	k1gMnPc1	demonstrant
zapálili	zapálit	k5eAaPmAgMnP	zapálit
budovu	budova	k1gFnSc4	budova
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
prezident	prezident	k1gMnSc1	prezident
dočasně	dočasně	k6eAd1	dočasně
výjimečný	výjimečný	k2eAgInSc4d1	výjimečný
stav	stav	k1gInSc4	stav
<g/>
.	.	kIx.	.
</s>
<s>
Compaoré	Compaorý	k2eAgNnSc1d1	Compaorý
demonstrantům	demonstrant	k1gMnPc3	demonstrant
nabídl	nabídnout	k5eAaPmAgMnS	nabídnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
povede	povést	k5eAaPmIp3nS	povést
po	po	k7c6	po
dohodě	dohoda	k1gFnSc6	dohoda
s	s	k7c7	s
opozicí	opozice	k1gFnSc7	opozice
přechodnou	přechodný	k2eAgFnSc4d1	přechodná
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc4	ten
ale	ale	k9	ale
protestující	protestující	k2eAgMnPc1d1	protestující
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
<g/>
,	,	kIx,	,
Compaoré	Compaorý	k2eAgFnSc2d1	Compaorý
tedy	tedy	k9	tedy
podal	podat	k5eAaPmAgMnS	podat
demisi	demise	k1gFnSc4	demise
<g/>
.	.	kIx.	.
</s>
<s>
Funkci	funkce	k1gFnSc4	funkce
prezidenta	prezident	k1gMnSc2	prezident
přechodně	přechodně	k6eAd1	přechodně
převzal	převzít	k5eAaPmAgMnS	převzít
plukovník	plukovník	k1gMnSc1	plukovník
Isaac	Isaac	k1gInSc4	Isaac
Zida	Zidum	k1gNnSc2	Zidum
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
funkci	funkce	k1gFnSc6	funkce
podpořila	podpořit	k5eAaPmAgFnS	podpořit
armáda	armáda	k1gFnSc1	armáda
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
chtěl	chtít	k5eAaImAgMnS	chtít
tuto	tento	k3xDgFnSc4	tento
funkci	funkce	k1gFnSc4	funkce
převzít	převzít	k5eAaPmF	převzít
náčelník	náčelník	k1gInSc4	náčelník
generálního	generální	k2eAgInSc2d1	generální
štábu	štáb	k1gInSc2	štáb
Honoré	Honorý	k2eAgNnSc1d1	Honoré
Traoré	Traorý	k2eAgNnSc1d1	Traoré
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
ale	ale	k9	ale
neměl	mít	k5eNaImAgMnS	mít
armádní	armádní	k2eAgFnSc4d1	armádní
podporu	podpora	k1gFnSc4	podpora
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Demonstranti	demonstrant	k1gMnPc1	demonstrant
ale	ale	k9	ale
převzetí	převzetí	k1gNnSc4	převzetí
prezidentského	prezidentský	k2eAgInSc2d1	prezidentský
úřadu	úřad	k1gInSc2	úřad
armádou	armáda	k1gFnSc7	armáda
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
<g/>
.	.	kIx.	.
<g/>
Krátce	krátce	k6eAd1	krátce
po	po	k7c4	po
převzetí	převzetí	k1gNnSc4	převzetí
moci	moct	k5eAaImF	moct
armádou	armáda	k1gFnSc7	armáda
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
schůzka	schůzka	k1gFnSc1	schůzka
zástpců	zástpec	k1gInPc2	zástpec
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
opozice	opozice	k1gFnSc2	opozice
<g/>
,	,	kIx,	,
příznivců	příznivec	k1gMnPc2	příznivec
bývalého	bývalý	k2eAgMnSc2d1	bývalý
prezidenta	prezident	k1gMnSc2	prezident
Compaorého	Compaorý	k2eAgMnSc2d1	Compaorý
a	a	k8xC	a
náboženských	náboženský	k2eAgInPc2d1	náboženský
a	a	k8xC	a
tradičních	tradiční	k2eAgInPc2d1	tradiční
předáků	předák	k1gInPc2	předák
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
dohodnuto	dohodnut	k2eAgNnSc1d1	dohodnuto
složení	složení	k1gNnSc1	složení
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zemi	zem	k1gFnSc4	zem
povede	povést	k5eAaPmIp3nS	povést
do	do	k7c2	do
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
schůzce	schůzka	k1gFnSc6	schůzka
však	však	k9	však
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
žádné	žádný	k3yNgFnSc3	žádný
dohodě	dohoda	k1gFnSc3	dohoda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
listopadu	listopad	k1gInSc2	listopad
se	se	k3xPyFc4	se
ale	ale	k9	ale
nakonec	nakonec	k6eAd1	nakonec
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
)	)	kIx)	)
armáda	armáda	k1gFnSc1	armáda
dohodla	dohodnout	k5eAaPmAgFnS	dohodnout
s	s	k7c7	s
opozicí	opozice	k1gFnSc7	opozice
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
přechodné	přechodný	k2eAgFnSc2d1	přechodná
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
by	by	kYmCp3nS	by
post	post	k1gInSc1	post
prezidenta	prezident	k1gMnSc2	prezident
a	a	k8xC	a
předsedy	předseda	k1gMnSc2	předseda
Přechodné	přechodný	k2eAgFnSc2d1	přechodná
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
(	(	kIx(	(
<g/>
Conseil	Conseil	k1gInSc1	Conseil
national	nationat	k5eAaPmAgInS	nationat
du	du	k?	du
transition	transition	k1gInSc1	transition
<g/>
)	)	kIx)	)
měli	mít	k5eAaImAgMnP	mít
civilisté	civilista	k1gMnPc1	civilista
<g/>
,	,	kIx,	,
post	post	k1gInSc4	post
premiéra	premiér	k1gMnSc2	premiér
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
obsadit	obsadit	k5eAaPmF	obsadit
představitel	představitel	k1gMnSc1	představitel
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
dohodě	dohoda	k1gFnSc6	dohoda
byl	být	k5eAaImAgMnS	být
prezidentem	prezident	k1gMnSc7	prezident
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
diplomat	diplomat	k1gMnSc1	diplomat
Michel	Michel	k1gMnSc1	Michel
Kafando	Kafanda	k1gFnSc5	Kafanda
<g/>
,	,	kIx,	,
premiérem	premiér	k1gMnSc7	premiér
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
přechodný	přechodný	k2eAgMnSc1d1	přechodný
prezident	prezident	k1gMnSc1	prezident
Isaac	Isaac	k1gFnSc1	Isaac
Zida	Zida	k1gFnSc1	Zida
<g/>
.	.	kIx.	.
<g/>
Nová	nový	k2eAgFnSc1d1	nová
vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
distancovala	distancovat	k5eAaBmAgFnS	distancovat
od	od	k7c2	od
předchozího	předchozí	k2eAgInSc2d1	předchozí
režimu	režim	k1gInSc2	režim
<g/>
:	:	kIx,	:
rozpustila	rozpustit	k5eAaPmAgFnS	rozpustit
stranu	strana	k1gFnSc4	strana
Congrè	Congrè	k1gFnSc3	Congrè
pour	poura	k1gFnPc2	poura
la	la	k1gNnSc2	la
démocratie	démocratie	k1gFnSc2	démocratie
et	et	k?	et
le	le	k?	le
progrè	progrè	k?	progrè
<g/>
,	,	kIx,	,
spojenou	spojený	k2eAgFnSc4d1	spojená
s	s	k7c7	s
bývalým	bývalý	k2eAgMnSc7d1	bývalý
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
slíbila	slíbit	k5eAaPmAgFnS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
prošetří	prošetřit	k5eAaPmIp3nS	prošetřit
období	období	k1gNnSc4	období
vlády	vláda	k1gFnSc2	vláda
Blaise	Blaise	k1gFnSc2	Blaise
Compaorého	Compaorý	k2eAgInSc2d1	Compaorý
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
vyžadovat	vyžadovat	k5eAaImF	vyžadovat
jeho	jeho	k3xOp3gNnSc4	jeho
vydání	vydání	k1gNnSc4	vydání
(	(	kIx(	(
<g/>
po	po	k7c6	po
převratu	převrat	k1gInSc6	převrat
utekl	utéct	k5eAaPmAgMnS	utéct
nejdříve	dříve	k6eAd3	dříve
do	do	k7c2	do
Pobřeží	pobřeží	k1gNnSc2	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
<g/>
,	,	kIx,	,
posléze	posléze	k6eAd1	posléze
do	do	k7c2	do
Maroka	Maroko	k1gNnSc2	Maroko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2015	[number]	k4	2015
prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
stráž	stráž	k1gFnSc1	stráž
<g/>
,	,	kIx,	,
podporující	podporující	k2eAgFnSc1d1	podporující
bývalého	bývalý	k2eAgMnSc2d1	bývalý
prezidenta	prezident	k1gMnSc2	prezident
Compaorého	Compaorý	k2eAgMnSc2d1	Compaorý
<g/>
,	,	kIx,	,
zajala	zajmout	k5eAaPmAgFnS	zajmout
prezidenta	prezident	k1gMnSc4	prezident
Kafandoa	Kafandous	k1gMnSc4	Kafandous
<g/>
,	,	kIx,	,
premiéra	premiér	k1gMnSc4	premiér
Zidu	Zida	k1gMnSc4	Zida
a	a	k8xC	a
několik	několik	k4yIc1	několik
ministrů	ministr	k1gMnPc2	ministr
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
dne	den	k1gInSc2	den
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
státní	státní	k2eAgFnSc1d1	státní
instituce	instituce	k1gFnSc1	instituce
za	za	k7c4	za
zrušené	zrušený	k2eAgInPc4d1	zrušený
a	a	k8xC	a
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
Národní	národní	k2eAgFnSc4d1	národní
radu	rada	k1gFnSc4	rada
pro	pro	k7c4	pro
demokracii	demokracie	k1gFnSc4	demokracie
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
generálem	generál	k1gMnSc7	generál
Gilbertem	Gilbert	k1gMnSc7	Gilbert
Diendérém	Diendérý	k2eAgInSc6d1	Diendérý
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
armády	armáda	k1gFnSc2	armáda
se	se	k3xPyFc4	se
však	však	k9	však
k	k	k7c3	k
puči	puč	k1gInSc3	puč
nepřidal	přidat	k5eNaPmAgMnS	přidat
a	a	k8xC	a
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
pučisty	pučista	k1gMnPc4	pučista
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
složí	složit	k5eAaPmIp3nS	složit
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
puči	puč	k1gInSc6	puč
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
10	[number]	k4	10
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
bylo	být	k5eAaImAgNnS	být
raněno	ranit	k5eAaPmNgNnS	ranit
<g/>
.	.	kIx.	.
<g/>
Dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgMnSc1	tento
stát	stát	k5eAaImF	stát
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejchudších	chudý	k2eAgFnPc2d3	nejchudší
zemí	zem	k1gFnPc2	zem
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
problémem	problém	k1gInSc7	problém
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
neutěšená	utěšený	k2eNgFnSc1d1	neutěšená
sociální	sociální	k2eAgFnSc1d1	sociální
situace	situace	k1gFnSc1	situace
<g/>
,	,	kIx,	,
negramotnost	negramotnost	k1gFnSc1	negramotnost
a	a	k8xC	a
vysoká	vysoký	k2eAgFnSc1d1	vysoká
inflace	inflace	k1gFnSc1	inflace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Stát	stát	k1gInSc1	stát
Burkina	Burkina	k1gMnSc1	Burkina
Faso	Faso	k1gMnSc1	Faso
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Západní	západní	k2eAgFnSc6d1	západní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Hraničí	hraničit	k5eAaImIp3nP	hraničit
se	se	k3xPyFc4	se
státy	stát	k1gInPc1	stát
<g/>
:	:	kIx,	:
Mali	Mali	k1gNnSc1	Mali
<g/>
,	,	kIx,	,
Niger	Niger	k1gInSc1	Niger
<g/>
,	,	kIx,	,
Benin	Benin	k1gInSc1	Benin
<g/>
,	,	kIx,	,
Ghana	Ghana	k1gFnSc1	Ghana
<g/>
,	,	kIx,	,
Pobřeží	pobřeží	k1gNnSc1	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
a	a	k8xC	a
Togo	Togo	k1gNnSc4	Togo
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vnitrozemský	vnitrozemský	k2eAgInSc1d1	vnitrozemský
stát	stát	k1gInSc1	stát
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
rovinatá	rovinatý	k2eAgFnSc1d1	rovinatá
<g/>
,	,	kIx,	,
místy	místo	k1gNnPc7	místo
mírně	mírně	k6eAd1	mírně
zvlněná	zvlněný	k2eAgFnSc1d1	zvlněná
plošina	plošina	k1gFnSc1	plošina
s	s	k7c7	s
výškami	výška	k1gFnPc7	výška
250	[number]	k4	250
–	–	k?	–
350	[number]	k4	350
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
střední	střední	k2eAgFnSc4d1	střední
a	a	k8xC	a
nižší	nízký	k2eAgFnSc4d2	nižší
část	část	k1gFnSc4	část
Hornoguinejské	Hornoguinejský	k2eAgFnSc2d1	Hornoguinejský
vysočiny	vysočina	k1gFnSc2	vysočina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zde	zde	k6eAd1	zde
formuje	formovat	k5eAaImIp3nS	formovat
jakoby	jakoby	k8xS	jakoby
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
vyvýšené	vyvýšený	k2eAgInPc1d1	vyvýšený
ostrovy	ostrov	k1gInPc1	ostrov
a	a	k8xC	a
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
západ	západ	k1gInSc4	západ
strmě	strmě	k6eAd1	strmě
spadá	spadat	k5eAaImIp3nS	spadat
do	do	k7c2	do
příkopového	příkopový	k2eAgInSc2d1	příkopový
zlomu	zlom	k1gInSc2	zlom
Falaise	Falaise	k1gFnSc2	Falaise
de	de	k?	de
Banfora	Banfora	k1gFnSc1	Banfora
<g/>
.	.	kIx.	.
</s>
<s>
Protékají	protékat	k5eAaImIp3nP	protékat
zde	zde	k6eAd1	zde
tři	tři	k4xCgFnPc1	tři
řeky	řeka	k1gFnPc1	řeka
<g/>
:	:	kIx,	:
Červená	červený	k2eAgFnSc1d1	červená
<g/>
,	,	kIx,	,
Bílá	bílý	k2eAgFnSc1d1	bílá
a	a	k8xC	a
Černá	černý	k2eAgFnSc1d1	černá
Volta	Volta	k1gFnSc1	Volta
(	(	kIx(	(
<g/>
jediný	jediný	k2eAgInSc1d1	jediný
stálý	stálý	k2eAgInSc1d1	stálý
tok	tok	k1gInSc1	tok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
najdeme	najít	k5eAaPmIp1nP	najít
až	až	k9	až
750	[number]	k4	750
metrů	metr	k1gInPc2	metr
vysoké	vysoký	k2eAgFnSc2d1	vysoká
pískovcové	pískovcový	k2eAgFnSc2d1	pískovcová
tabule	tabule	k1gFnSc2	tabule
s	s	k7c7	s
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
Ténakourou	Ténakoura	k1gFnSc7	Ténakoura
(	(	kIx(	(
<g/>
749	[number]	k4	749
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Nigerijskou	nigerijský	k2eAgFnSc4d1	nigerijská
pánev	pánev	k1gFnSc4	pánev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Sahelu	Sahel	k1gInSc2	Sahel
převládá	převládat	k5eAaImIp3nS	převládat
tropické	tropický	k2eAgNnSc1d1	tropické
suché	suchý	k2eAgNnSc1d1	suché
podnebí	podnebí	k1gNnSc1	podnebí
<g/>
,	,	kIx,	,
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
zase	zase	k9	zase
vlhké	vlhký	k2eAgInPc1d1	vlhký
tropy	trop	k1gInPc1	trop
<g/>
.	.	kIx.	.
</s>
<s>
Půda	půda	k1gFnSc1	půda
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
mezi	mezi	k7c7	mezi
lesy	les	k1gInPc7	les
(	(	kIx(	(
<g/>
50	[number]	k4	50
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pastviny	pastvina	k1gFnPc1	pastvina
(	(	kIx(	(
<g/>
22	[number]	k4	22
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ornou	orný	k2eAgFnSc4d1	orná
půdu	půda	k1gFnSc4	půda
(	(	kIx(	(
<g/>
13	[number]	k4	13
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
ostatní	ostatní	k2eAgMnPc1d1	ostatní
(	(	kIx(	(
<g/>
15	[number]	k4	15
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Burkina	Burkina	k1gMnSc1	Burkina
Faso	Faso	k1gMnSc1	Faso
má	mít	k5eAaImIp3nS	mít
dva	dva	k4xCgInPc4	dva
hlavní	hlavní	k2eAgInPc4d1	hlavní
typy	typ	k1gInPc4	typ
krajiny	krajina	k1gFnSc2	krajina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Severní	severní	k2eAgFnSc1d1	severní
<g/>
,	,	kIx,	,
východní	východní	k2eAgFnSc1d1	východní
a	a	k8xC	a
střední	střední	k2eAgFnSc1d1	střední
část	část	k1gFnSc1	část
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
mírně	mírně	k6eAd1	mírně
zvlněnou	zvlněný	k2eAgFnSc4d1	zvlněná
krajinu	krajina	k1gFnSc4	krajina
<g/>
,	,	kIx,	,
místy	místy	k6eAd1	místy
prostoupenou	prostoupený	k2eAgFnSc4d1	prostoupená
několika	několik	k4yIc2	několik
kopci	kopec	k1gInPc7	kopec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jihozápad	jihozápad	k1gInSc1	jihozápad
Burkiny	Burkina	k1gFnSc2	Burkina
Faso	Faso	k6eAd1	Faso
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
pískovcovým	pískovcový	k2eAgInSc7d1	pískovcový
masívem	masív	k1gInSc7	masív
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
vrchol	vrchol	k1gInSc1	vrchol
státu	stát	k1gInSc2	stát
–	–	k?	–
Ténakourou	Ténakoura	k1gFnSc7	Ténakoura
<g/>
.	.	kIx.	.
<g/>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
400	[number]	k4	400
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
a	a	k8xC	a
rozdíl	rozdíl	k1gInSc1	rozdíl
mezi	mezi	k7c7	mezi
nejvýše	vysoce	k6eAd3	vysoce
a	a	k8xC	a
nejníže	nízce	k6eAd3	nízce
položeným	položený	k2eAgInSc7d1	položený
bodem	bod	k1gInSc7	bod
není	být	k5eNaImIp3nS	být
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
600	[number]	k4	600
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Burkina	Burkina	k1gMnSc1	Burkina
Faso	Faso	k1gMnSc1	Faso
tedy	tedy	k9	tedy
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
relativně	relativně	k6eAd1	relativně
rovném	rovný	k2eAgInSc6d1	rovný
terénu	terén	k1gInSc6	terén
bez	bez	k7c2	bez
významných	významný	k2eAgNnPc2d1	významné
převýšení	převýšení	k1gNnPc2	převýšení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
===	===	k?	===
Podnebí	podnebí	k1gNnSc2	podnebí
===	===	k?	===
</s>
</p>
<p>
<s>
Burkina	Burkina	k1gFnSc1	Burkina
Faso	Faso	k6eAd1	Faso
má	mít	k5eAaImIp3nS	mít
především	především	k9	především
tropické	tropický	k2eAgNnSc1d1	tropické
klima	klima	k1gNnSc1	klima
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
rozdílnými	rozdílný	k2eAgFnPc7d1	rozdílná
sezónami	sezóna	k1gFnPc7	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Deštivé	deštivý	k2eAgNnSc1d1	deštivé
období	období	k1gNnSc1	období
je	být	k5eAaImIp3nS	být
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
srážkami	srážka	k1gFnPc7	srážka
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
600-900	[number]	k4	600-900
milimetrů	milimetr	k1gInPc2	milimetr
a	a	k8xC	a
suché	suchý	k2eAgNnSc1d1	suché
období	období	k1gNnSc1	období
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterého	který	k3yQgMnSc2	který
vane	vanout	k5eAaImIp3nS	vanout
saharský	saharský	k2eAgInSc1d1	saharský
suchý	suchý	k2eAgInSc1d1	suchý
vítr	vítr	k1gInSc1	vítr
harmattan	harmattan	k1gInSc1	harmattan
<g/>
.	.	kIx.	.
</s>
<s>
Deštivé	deštivý	k2eAgNnSc1d1	deštivé
období	období	k1gNnSc1	období
trvá	trvat	k5eAaImIp3nS	trvat
asi	asi	k9	asi
čtyři	čtyři	k4xCgInPc4	čtyři
měsíce	měsíc	k1gInPc4	měsíc
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
kratší	krátký	k2eAgFnSc1d2	kratší
na	na	k7c6	na
severu	sever	k1gInSc6	sever
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Burkina	Burkina	k1gMnSc1	Burkina
Faso	Faso	k1gMnSc1	Faso
má	mít	k5eAaImIp3nS	mít
tři	tři	k4xCgInPc4	tři
hlavní	hlavní	k2eAgInPc4d1	hlavní
klimatické	klimatický	k2eAgInPc4d1	klimatický
pásy	pás	k1gInPc4	pás
<g/>
/	/	kIx~	/
<g/>
zóny	zóna	k1gFnPc4	zóna
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Sahelský	Sahelský	k2eAgInSc1d1	Sahelský
pás	pás	k1gInSc1	pás
–	–	k?	–
Oblast	oblast	k1gFnSc4	oblast
suché	suchý	k2eAgFnSc2d1	suchá
tropické	tropický	k2eAgFnSc2d1	tropická
savany	savana	k1gFnSc2	savana
<g/>
,	,	kIx,	,
s	s	k7c7	s
nízkými	nízký	k2eAgFnPc7d1	nízká
srážkami	srážka	k1gFnPc7	srážka
a	a	k8xC	a
vysokými	vysoký	k2eAgFnPc7d1	vysoká
teplotami	teplota	k1gFnPc7	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
rostlo	růst	k5eAaImAgNnS	růst
mnoho	mnoho	k4c1	mnoho
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dnes	dnes	k6eAd1	dnes
ve	v	k7c6	v
vyprahlé	vyprahlý	k2eAgFnSc6d1	vyprahlá
krajině	krajina	k1gFnSc6	krajina
rostou	růst	k5eAaImIp3nP	růst
stromy	strom	k1gInPc1	strom
jen	jen	k6eAd1	jen
sporadicky	sporadicky	k6eAd1	sporadicky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
někdy	někdy	k6eAd1	někdy
po	po	k7c4	po
celá	celý	k2eAgNnPc4d1	celé
léta	léto	k1gNnPc4	léto
neprší	pršet	k5eNaImIp3nP	pršet
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
různě	různě	k6eAd1	různě
intenzivní	intenzivní	k2eAgInPc4d1	intenzivní
hladomory	hladomor	k1gInPc4	hladomor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Súdánsko-sahelský	Súdánskoahelský	k2eAgInSc1d1	Súdánsko-sahelský
pás	pás	k1gInSc1	pás
–	–	k?	–
Přechodná	přechodný	k2eAgFnSc1d1	přechodná
zóna	zóna	k1gFnSc1	zóna
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
hlavními	hlavní	k2eAgInPc7d1	hlavní
typy	typ	k1gInPc7	typ
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
především	především	k9	především
vydatnějšími	vydatný	k2eAgFnPc7d2	vydatnější
srážkami	srážka	k1gFnPc7	srážka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Súdánsko-guinejský	súdánskouinejský	k2eAgInSc1d1	súdánsko-guinejský
pás	pás	k1gInSc1	pás
–	–	k?	–
Tento	tento	k3xDgInSc4	tento
pás	pás	k1gInSc4	pás
je	být	k5eAaImIp3nS	být
typický	typický	k2eAgInSc1d1	typický
nižšími	nízký	k2eAgFnPc7d2	nižší
průměrnými	průměrný	k2eAgFnPc7d1	průměrná
teplotami	teplota	k1gFnPc7	teplota
a	a	k8xC	a
deštivějším	deštivý	k2eAgNnSc7d2	deštivější
počasím	počasí	k1gNnSc7	počasí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
rozdělení	rozdělení	k1gNnSc1	rozdělení
==	==	k?	==
</s>
</p>
<p>
<s>
Burkina	Burkina	k1gFnSc1	Burkina
Faso	Faso	k6eAd1	Faso
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
13	[number]	k4	13
regionů	region	k1gInPc2	region
<g/>
,	,	kIx,	,
45	[number]	k4	45
provincií	provincie	k1gFnPc2	provincie
a	a	k8xC	a
301	[number]	k4	301
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
následující	následující	k2eAgInPc1d1	následující
regiony	region	k1gInPc1	region
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Boucle	Boucle	k1gFnSc1	Boucle
du	du	k?	du
Mouhoun	Mouhoun	k1gInSc1	Mouhoun
</s>
</p>
<p>
<s>
Cascades	Cascades	k1gMnSc1	Cascades
</s>
</p>
<p>
<s>
Centre	centr	k1gInSc5	centr
</s>
</p>
<p>
<s>
Centre-Est	Centre-Est	k1gFnSc1	Centre-Est
</s>
</p>
<p>
<s>
Centre-Nord	Centre-Nord	k6eAd1	Centre-Nord
</s>
</p>
<p>
<s>
Centre-Ouest	Centre-Ouest	k1gFnSc1	Centre-Ouest
</s>
</p>
<p>
<s>
Centre-Sud	Centre-Sud	k6eAd1	Centre-Sud
</s>
</p>
<p>
<s>
Est	Est	k1gMnSc1	Est
</s>
</p>
<p>
<s>
Hauts-Bassins	Hauts-Bassins	k6eAd1	Hauts-Bassins
</s>
</p>
<p>
<s>
Nord	Nord	k6eAd1	Nord
</s>
</p>
<p>
<s>
Plateau-Central	Plateau-Centrat	k5eAaImAgMnS	Plateau-Centrat
</s>
</p>
<p>
<s>
Sahel	Sahel	k1gMnSc1	Sahel
</s>
</p>
<p>
<s>
Sud-Ouest	Sud-Ouest	k1gFnSc1	Sud-Ouest
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Burkiny	Burkina	k1gFnSc2	Burkina
Faso	Faso	k6eAd1	Faso
trvale	trvale	k6eAd1	trvale
žije	žít	k5eAaImIp3nS	žít
přes	přes	k7c4	přes
160	[number]	k4	160
různých	různý	k2eAgNnPc2d1	různé
etnik	etnikum	k1gNnPc2	etnikum
<g/>
.	.	kIx.	.
</s>
<s>
Dominantním	dominantní	k2eAgNnSc7d1	dominantní
etnikem	etnikum	k1gNnSc7	etnikum
jsou	být	k5eAaImIp3nP	být
Mossiové	Mossius	k1gMnPc1	Mossius
(	(	kIx(	(
<g/>
40	[number]	k4	40
%	%	kIx~	%
<g/>
-	-	kIx~	-
<g/>
50	[number]	k4	50
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
odvozují	odvozovat	k5eAaImIp3nP	odvozovat
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
od	od	k7c2	od
válečníků	válečník	k1gMnPc2	válečník
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
na	na	k7c4	na
dnešní	dnešní	k2eAgNnSc4d1	dnešní
území	území	k1gNnSc4	území
přišli	přijít	k5eAaPmAgMnP	přijít
v	v	k7c6	v
dávné	dávný	k2eAgFnSc6d1	dávná
minulosti	minulost	k1gFnSc6	minulost
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
zastoupeni	zastoupen	k2eAgMnPc1d1	zastoupen
Fulbové	Fulb	k1gMnPc1	Fulb
(	(	kIx(	(
<g/>
8	[number]	k4	8
%	%	kIx~	%
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
%	%	kIx~	%
<g/>
)	)	kIx)	)
či	či	k8xC	či
Lobiové	Lobius	k1gMnPc1	Lobius
(	(	kIx(	(
<g/>
7	[number]	k4	7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
50	[number]	k4	50
%	%	kIx~	%
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
islám	islám	k1gInSc1	islám
<g/>
,	,	kIx,	,
40	[number]	k4	40
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
tradiční	tradiční	k2eAgNnPc4d1	tradiční
africká	africký	k2eAgNnPc4d1	africké
náboženství	náboženství	k1gNnPc4	náboženství
a	a	k8xC	a
asi	asi	k9	asi
10	[number]	k4	10
%	%	kIx~	%
jsou	být	k5eAaImIp3nP	být
křesťané	křesťan	k1gMnPc1	křesťan
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
katolíci	katolík	k1gMnPc1	katolík
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
francouzština	francouzština	k1gFnSc1	francouzština
<g/>
,	,	kIx,	,
obyvatelé	obyvatel	k1gMnPc1	obyvatel
mluví	mluvit	k5eAaImIp3nP	mluvit
také	také	k9	také
arabsky	arabsky	k6eAd1	arabsky
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
místními	místní	k2eAgInPc7d1	místní
domorodými	domorodý	k2eAgInPc7d1	domorodý
jazyky	jazyk	k1gInPc7	jazyk
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
more	mor	k1gInSc5	mor
<g/>
,	,	kIx,	,
mande	mande	k6eAd1	mande
<g/>
,	,	kIx,	,
pularština	pularština	k1gFnSc1	pularština
a	a	k8xC	a
fulbština	fulbština	k1gFnSc1	fulbština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvíce	hodně	k6eAd3	hodně
obyvatel	obyvatel	k1gMnPc2	obyvatel
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
oblastech	oblast	k1gFnPc6	oblast
Burkiny	Burkina	k1gFnSc2	Burkina
Faso	Faso	k6eAd1	Faso
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
také	také	k6eAd1	také
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
každoroční	každoroční	k2eAgFnSc7d1	každoroční
migrací	migrace	k1gFnSc7	migrace
statisíců	statisíce	k1gInPc2	statisíce
obyvatel	obyvatel	k1gMnPc2	obyvatel
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
do	do	k7c2	do
sousední	sousední	k2eAgFnSc2d1	sousední
Ghany	Ghana	k1gFnSc2	Ghana
a	a	k8xC	a
Pobřeží	pobřeží	k1gNnSc2	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
za	za	k7c7	za
sezónními	sezónní	k2eAgFnPc7d1	sezónní
pracemi	práce	k1gFnPc7	práce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
vysoká	vysoký	k2eAgFnSc1d1	vysoká
negramotnost	negramotnost	k1gFnSc1	negramotnost
–	–	k?	–
82	[number]	k4	82
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
80	[number]	k4	80
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobná	pravděpodobný	k2eAgFnSc1d1	pravděpodobná
míra	míra	k1gFnSc1	míra
dožití	dožití	k1gNnSc4	dožití
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
51	[number]	k4	51
a	a	k8xC	a
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
54	[number]	k4	54
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Populační	populační	k2eAgInSc1d1	populační
přírůstek	přírůstek	k1gInSc1	přírůstek
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
3,1	[number]	k4	3,1
%	%	kIx~	%
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
na	na	k7c4	na
jednu	jeden	k4xCgFnSc4	jeden
ženu	žena	k1gFnSc4	žena
zde	zde	k6eAd1	zde
připadá	připadat	k5eAaImIp3nS	připadat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
6	[number]	k4	6
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
HDP	HDP	kA	HDP
Burkiny	Burkina	k1gFnPc1	Burkina
Faso	Faso	k1gMnSc1	Faso
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
asi	asi	k9	asi
6	[number]	k4	6
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
dominantním	dominantní	k2eAgInSc7d1	dominantní
zdrojem	zdroj	k1gInSc7	zdroj
zisků	zisk	k1gInPc2	zisk
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
páteří	páteř	k1gFnSc7	páteř
ekonomiky	ekonomika	k1gFnSc2	ekonomika
Burkiny	Burkina	k1gFnSc2	Burkina
Faso	Faso	k1gNnSc1	Faso
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zemědělství	zemědělství	k1gNnSc1	zemědělství
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
celkové	celkový	k2eAgFnSc6d1	celková
tvorbě	tvorba	k1gFnSc6	tvorba
HDP	HDP	kA	HDP
se	se	k3xPyFc4	se
sektor	sektor	k1gInSc1	sektor
zemědělství	zemědělství	k1gNnSc2	zemědělství
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
podílelo	podílet	k5eAaImAgNnS	podílet
34,4	[number]	k4	34,4
%	%	kIx~	%
<g/>
,	,	kIx,	,
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
však	však	k9	však
až	až	k9	až
84	[number]	k4	84
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
pěstovanou	pěstovaný	k2eAgFnSc7d1	pěstovaná
plodinou	plodina	k1gFnSc7	plodina
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
národního	národní	k2eAgNnSc2d1	národní
hospodářství	hospodářství	k1gNnSc2	hospodářství
je	být	k5eAaImIp3nS	být
bavlna	bavlna	k1gFnSc1	bavlna
–	–	k?	–
Burkina	Burkina	k1gMnSc1	Burkina
Faso	Faso	k1gMnSc1	Faso
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgMnSc7d3	veliký
africkým	africký	k2eAgMnSc7d1	africký
producentem	producent	k1gMnSc7	producent
této	tento	k3xDgFnSc2	tento
komodity	komodita	k1gFnSc2	komodita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
sklidilo	sklidit	k5eAaPmAgNnS	sklidit
rekordních	rekordní	k2eAgInPc2d1	rekordní
0,74	[number]	k4	0,74
mil	míle	k1gFnPc2	míle
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
aktivně	aktivně	k6eAd1	aktivně
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
omezení	omezení	k1gNnSc4	omezení
subvencí	subvence	k1gFnPc2	subvence
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
USA	USA	kA	USA
a	a	k8xC	a
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
tyto	tento	k3xDgFnPc4	tento
subvence	subvence	k1gFnSc1	subvence
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
i	i	k9	i
ve	v	k7c6	v
WTO	WTO	kA	WTO
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
pěstované	pěstovaný	k2eAgFnPc1d1	pěstovaná
plodiny	plodina	k1gFnPc1	plodina
<g/>
:	:	kIx,	:
proso	proso	k1gNnSc1	proso
<g/>
,	,	kIx,	,
rýže	rýže	k1gFnSc1	rýže
<g/>
,	,	kIx,	,
cukrová	cukrový	k2eAgFnSc1d1	cukrová
třtina	třtina	k1gFnSc1	třtina
<g/>
,	,	kIx,	,
čirok	čirok	k1gInSc1	čirok
<g/>
,	,	kIx,	,
kukuřice	kukuřice	k1gFnSc1	kukuřice
<g/>
,	,	kIx,	,
zelenina	zelenina	k1gFnSc1	zelenina
–	–	k?	–
obdělávána	obděláván	k2eAgFnSc1d1	obdělávána
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
území	území	k1gNnSc2	území
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Burkina	Burkina	k1gFnSc1	Burkina
Faso	Faso	k6eAd1	Faso
má	mít	k5eAaImIp3nS	mít
proto	proto	k8xC	proto
problém	problém	k1gInSc1	problém
s	s	k7c7	s
dosažením	dosažení	k1gNnSc7	dosažení
potravinové	potravinový	k2eAgFnSc2d1	potravinová
soběstačnosti	soběstačnost	k1gFnSc2	soběstačnost
<g/>
.	.	kIx.	.
</s>
<s>
Dobrým	dobrý	k2eAgInSc7d1	dobrý
vývozním	vývozní	k2eAgInSc7d1	vývozní
artiklem	artikl	k1gInSc7	artikl
posledních	poslední	k2eAgNnPc2d1	poslední
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
mango	mango	k1gNnSc1	mango
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Průmysl	průmysl	k1gInSc1	průmysl
===	===	k?	===
</s>
</p>
<p>
<s>
Průmysl	průmysl	k1gInSc1	průmysl
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
HDP	HDP	kA	HDP
asi	asi	k9	asi
18	[number]	k4	18
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Nosným	nosný	k2eAgNnSc7d1	nosné
odvětvím	odvětví	k1gNnSc7	odvětví
je	být	k5eAaImIp3nS	být
těžební	těžební	k2eAgInSc1d1	těžební
průmysl	průmysl	k1gInSc1	průmysl
<g/>
;	;	kIx,	;
v	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
dvou	dva	k4xCgInPc6	dva
letech	let	k1gInPc6	let
vláda	vláda	k1gFnSc1	vláda
vydala	vydat	k5eAaPmAgFnS	vydat
370	[number]	k4	370
nových	nový	k2eAgFnPc2d1	nová
licencí	licence	k1gFnPc2	licence
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
má	mít	k5eAaImIp3nS	mít
začít	začít	k5eAaPmF	začít
těžba	těžba	k1gFnSc1	těžba
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
nových	nový	k2eAgInPc2d1	nový
dolů	dol	k1gInPc2	dol
(	(	kIx(	(
<g/>
investice	investice	k1gFnPc1	investice
kanadských	kanadský	k2eAgFnPc2d1	kanadská
a	a	k8xC	a
britských	britský	k2eAgFnPc2d1	britská
firem	firma	k1gFnPc2	firma
<g/>
)	)	kIx)	)
–	–	k?	–
celková	celkový	k2eAgFnSc1d1	celková
těžba	těžba	k1gFnSc1	těžba
zlata	zlato	k1gNnSc2	zlato
má	mít	k5eAaImIp3nS	mít
do	do	k7c2	do
tří	tři	k4xCgNnPc2	tři
let	léto	k1gNnPc2	léto
růst	růst	k5eAaImF	růst
na	na	k7c4	na
9,4	[number]	k4	9,4
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
těženým	těžený	k2eAgInSc7d1	těžený
kovem	kov	k1gInSc7	kov
je	být	k5eAaImIp3nS	být
zinek	zinek	k1gInSc1	zinek
(	(	kIx(	(
<g/>
70	[number]	k4	70
tisíc	tisíc	k4xCgInSc4	tisíc
tun	tuna	k1gFnPc2	tuna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výrobní	výrobní	k2eAgInSc1d1	výrobní
sektor	sektor	k1gInSc1	sektor
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgInSc1d1	malý
–	–	k?	–
důležitějšími	důležitý	k2eAgFnPc7d2	důležitější
odvětvími	odvětví	k1gNnPc7	odvětví
jsou	být	k5eAaImIp3nP	být
zemědělsko	zemědělsko	k6eAd1	zemědělsko
<g/>
–	–	k?	–
<g/>
potravinářský	potravinářský	k2eAgInSc1d1	potravinářský
<g/>
,	,	kIx,	,
a	a	k8xC	a
textilní	textilní	k2eAgInSc1d1	textilní
<g/>
.	.	kIx.	.
</s>
<s>
Výrobní	výrobní	k2eAgInPc1d1	výrobní
podniky	podnik	k1gInPc1	podnik
v	v	k7c6	v
Burkině	Burkina	k1gFnSc6	Burkina
Faso	Faso	k6eAd1	Faso
se	se	k3xPyFc4	se
potýkají	potýkat	k5eAaImIp3nP	potýkat
se	s	k7c7	s
značnými	značný	k2eAgInPc7d1	značný
problémy	problém	k1gInPc7	problém
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
díky	díky	k7c3	díky
vysokým	vysoký	k2eAgFnPc3d1	vysoká
cenám	cena	k1gFnPc3	cena
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
vysokým	vysoký	k2eAgInPc3d1	vysoký
přepravním	přepravní	k2eAgInPc3d1	přepravní
nákladům	náklad	k1gInPc3	náklad
z	z	k7c2	z
přístavu	přístav	k1gInSc2	přístav
v	v	k7c6	v
Lomé	Lomá	k1gFnSc6	Lomá
(	(	kIx(	(
<g/>
Togo	Togo	k1gNnSc1	Togo
<g/>
)	)	kIx)	)
a	a	k8xC	a
Tema	Tema	k?	Tema
(	(	kIx(	(
<g/>
Ghana	Ghana	k1gFnSc1	Ghana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
stavební	stavební	k2eAgInSc1d1	stavební
sektor	sektor	k1gInSc1	sektor
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
na	na	k7c6	na
vzestupu	vzestup	k1gInSc6	vzestup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Služby	služba	k1gFnPc1	služba
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
tvorbě	tvorba	k1gFnSc6	tvorba
HDP	HDP	kA	HDP
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
39,6	[number]	k4	39,6
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Burkina	Burkina	k1gFnSc1	Burkina
Faso	Faso	k6eAd1	Faso
má	mít	k5eAaImIp3nS	mít
malý	malý	k2eAgInSc1d1	malý
finanční	finanční	k2eAgInSc1d1	finanční
sektor	sektor	k1gInSc1	sektor
(	(	kIx(	(
<g/>
8	[number]	k4	8
větších	veliký	k2eAgFnPc2d2	veliký
komerčních	komerční	k2eAgFnPc2d1	komerční
bank	banka	k1gFnPc2	banka
<g/>
,	,	kIx,	,
3	[number]	k4	3
pojišťovací	pojišťovací	k2eAgInPc1d1	pojišťovací
ústavy	ústav	k1gInPc1	ústav
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Turistický	turistický	k2eAgInSc1d1	turistický
potenciál	potenciál	k1gInSc1	potenciál
není	být	k5eNaImIp3nS	být
využíván	využívat	k5eAaPmNgInS	využívat
–	–	k?	–
větší	veliký	k2eAgFnSc1d2	veliký
návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
je	být	k5eAaImIp3nS	být
registrována	registrovat	k5eAaBmNgFnS	registrovat
v	v	k7c6	v
období	období	k1gNnSc6	období
Festpaco	Festpaco	k1gMnSc1	Festpaco
(	(	kIx(	(
<g/>
největší	veliký	k2eAgInSc1d3	veliký
africký	africký	k2eAgInSc1d1	africký
filmový	filmový	k2eAgInSc1d1	filmový
festival	festival	k1gInSc1	festival
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
národního	národní	k2eAgInSc2d1	národní
dne	den	k1gInSc2	den
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
výstavy	výstava	k1gFnSc2	výstava
řemesel	řemeslo	k1gNnPc2	řemeslo
(	(	kIx(	(
<g/>
SIAO	SIAO	kA	SIAO
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Doprava	doprava	k1gFnSc1	doprava
===	===	k?	===
</s>
</p>
<p>
<s>
Země	země	k1gFnSc1	země
má	mít	k5eAaImIp3nS	mít
kolem	kolem	k7c2	kolem
15	[number]	k4	15
000	[number]	k4	000
km	km	kA	km
silnic	silnice	k1gFnPc2	silnice
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
4800	[number]	k4	4800
km	km	kA	km
asfaltovaných	asfaltovaný	k2eAgFnPc2d1	asfaltovaná
<g/>
.	.	kIx.	.
</s>
<s>
Železnice	železnice	k1gFnSc1	železnice
vede	vést	k5eAaImIp3nS	vést
z	z	k7c2	z
města	město	k1gNnSc2	město
Kaya	Kay	k1gInSc2	Kay
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
Ouagadougou	Ouagadouga	k1gFnSc7	Ouagadouga
a	a	k8xC	a
Bobo-Dioulasso	Bobo-Dioulassa	k1gFnSc5	Bobo-Dioulassa
<g/>
)	)	kIx)	)
do	do	k7c2	do
Abidjanu	Abidjan	k1gInSc2	Abidjan
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
krádeže	krádež	k1gFnPc1	krádež
a	a	k8xC	a
přepadení	přepadení	k1gNnPc1	přepadení
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
na	na	k7c6	na
denním	denní	k2eAgInSc6d1	denní
pořádku	pořádek	k1gInSc6	pořádek
<g/>
.	.	kIx.	.
</s>
<s>
Podél	podél	k7c2	podél
této	tento	k3xDgFnSc2	tento
železnice	železnice	k1gFnSc2	železnice
také	také	k9	také
leží	ležet	k5eAaImIp3nS	ležet
téměř	téměř	k6eAd1	téměř
všechna	všechen	k3xTgNnPc4	všechen
velká	velký	k2eAgNnPc4d1	velké
burkinská	burkinský	k2eAgNnPc4d1	burkinský
města	město	k1gNnPc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Ouagadougou	Ouagadouga	k1gFnSc7	Ouagadouga
a	a	k8xC	a
Burkina	Burkina	k1gFnSc1	Burkina
Faso	Faso	k6eAd1	Faso
má	mít	k5eAaImIp3nS	mít
pravidelné	pravidelný	k2eAgNnSc4d1	pravidelné
spojení	spojení	k1gNnSc4	spojení
linkou	linka	k1gFnSc7	linka
Air	Air	k1gMnSc2	Air
France	Franc	k1gMnSc2	Franc
a	a	k8xC	a
Brussles	Brussles	k1gMnSc1	Brussles
Airlines	Airlines	k1gMnSc1	Airlines
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgFnSc1d1	místní
společnost	společnost	k1gFnSc1	společnost
Air	Air	k1gMnSc2	Air
Burkina	Burkin	k1gMnSc2	Burkin
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
spojení	spojení	k1gNnSc4	spojení
do	do	k7c2	do
některých	některý	k3yIgFnPc2	některý
okolních	okolní	k2eAgFnPc2d1	okolní
západoafrických	západoafrický	k2eAgFnPc2d1	západoafrická
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Telekomunikace	telekomunikace	k1gFnSc2	telekomunikace
===	===	k?	===
</s>
</p>
<p>
<s>
Telekomunikační	telekomunikační	k2eAgFnSc1d1	telekomunikační
síť	síť	k1gFnSc1	síť
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
řídká	řídký	k2eAgFnSc1d1	řídká
–	–	k?	–
přípojku	přípojek	k1gInSc3	přípojek
má	mít	k5eAaImIp3nS	mít
zhruba	zhruba	k6eAd1	zhruba
100	[number]	k4	100
000	[number]	k4	000
účastníků	účastník	k1gMnPc2	účastník
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
rychle	rychle	k6eAd1	rychle
roste	růst	k5eAaImIp3nS	růst
počet	počet	k1gInSc1	počet
mobilních	mobilní	k2eAgInPc2d1	mobilní
telefonů	telefon	k1gInPc2	telefon
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
má	mít	k5eAaImIp3nS	mít
kolem	kolem	k7c2	kolem
500	[number]	k4	500
000	[number]	k4	000
účastníků	účastník	k1gMnPc2	účastník
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
chce	chtít	k5eAaImIp3nS	chtít
povzbudit	povzbudit	k5eAaPmF	povzbudit
rozvoj	rozvoj	k1gInSc4	rozvoj
telekomunikací	telekomunikace	k1gFnPc2	telekomunikace
privatizací	privatizace	k1gFnPc2	privatizace
státní	státní	k2eAgFnSc3d1	státní
telekomunikační	telekomunikační	k2eAgFnSc3d1	telekomunikační
společnosti	společnost	k1gFnSc3	společnost
ONATEL	ONATEL	kA	ONATEL
<g/>
.	.	kIx.	.
51	[number]	k4	51
%	%	kIx~	%
akcií	akcie	k1gFnPc2	akcie
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
prodáno	prodat	k5eAaPmNgNnS	prodat
Maroc	Maroc	k1gFnSc1	Maroc
Telecom	Telecom	k1gInSc1	Telecom
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Burkině	Burkina	k1gFnSc6	Burkina
Faso	Faso	k6eAd1	Faso
působí	působit	k5eAaImIp3nP	působit
tři	tři	k4xCgMnPc1	tři
operátoři	operátor	k1gMnPc1	operátor
mobilních	mobilní	k2eAgFnPc2d1	mobilní
sítí	síť	k1gFnPc2	síť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Energetika	energetika	k1gFnSc1	energetika
===	===	k?	===
</s>
</p>
<p>
<s>
Pouze	pouze	k6eAd1	pouze
1	[number]	k4	1
%	%	kIx~	%
energetických	energetický	k2eAgFnPc2d1	energetická
potřeb	potřeba	k1gFnPc2	potřeba
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
kryto	krýt	k5eAaImNgNnS	krýt
elektrickou	elektrický	k2eAgFnSc7d1	elektrická
energií	energie	k1gFnSc7	energie
(	(	kIx(	(
<g/>
91	[number]	k4	91
%	%	kIx~	%
je	být	k5eAaImIp3nS	být
kryto	krýt	k5eAaImNgNnS	krýt
tradičními	tradiční	k2eAgInPc7d1	tradiční
palivy-dřevným	palivyřevný	k2eAgNnSc7d1	palivy-dřevný
uhlím	uhlí	k1gNnSc7	uhlí
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
asi	asi	k9	asi
400	[number]	k4	400
MW	MW	kA	MW
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
76	[number]	k4	76
%	%	kIx~	%
v	v	k7c6	v
tepelných	tepelný	k2eAgFnPc6d1	tepelná
<g/>
/	/	kIx~	/
<g/>
generátorových	generátorový	k2eAgFnPc6d1	generátorová
elektrárnách	elektrárna	k1gFnPc6	elektrárna
a	a	k8xC	a
24	[number]	k4	24
%	%	kIx~	%
v	v	k7c6	v
malých	malý	k2eAgFnPc6d1	malá
vodních	vodní	k2eAgFnPc6d1	vodní
elektrárnách	elektrárna	k1gFnPc6	elektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Elektřina	elektřina	k1gFnSc1	elektřina
je	být	k5eAaImIp3nS	být
dostupná	dostupný	k2eAgFnSc1d1	dostupná
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
městech	město	k1gNnPc6	město
(	(	kIx(	(
<g/>
7	[number]	k4	7
%	%	kIx~	%
území	území	k1gNnSc6	území
je	být	k5eAaImIp3nS	být
elektrifikováno	elektrifikovat	k5eAaBmNgNnS	elektrifikovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Monopolním	monopolní	k2eAgMnSc7d1	monopolní
výrobcem	výrobce	k1gMnSc7	výrobce
a	a	k8xC	a
distributorem	distributor	k1gMnSc7	distributor
je	být	k5eAaImIp3nS	být
státní	státní	k2eAgFnSc1d1	státní
společnost	společnost	k1gFnSc1	společnost
Sonabel	Sonabela	k1gFnPc2	Sonabela
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Politika	politikum	k1gNnSc2	politikum
==	==	k?	==
</s>
</p>
<p>
<s>
Burkina	Burkina	k1gFnSc1	Burkina
Faso	Faso	k6eAd1	Faso
je	být	k5eAaImIp3nS	být
prezidentská	prezidentský	k2eAgFnSc1d1	prezidentská
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
prezident	prezident	k1gMnSc1	prezident
plní	plnit	k5eAaImIp3nS	plnit
funkci	funkce	k1gFnSc4	funkce
jak	jak	k8xS	jak
hlavy	hlava	k1gFnPc4	hlava
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Exekutivní	exekutivní	k2eAgFnSc1d1	exekutivní
moc	moc	k1gFnSc1	moc
je	být	k5eAaImIp3nS	být
vykonávána	vykonávat	k5eAaImNgFnS	vykonávat
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Legislativní	legislativní	k2eAgFnSc1d1	legislativní
moc	moc	k1gFnSc1	moc
má	mít	k5eAaImIp3nS	mít
jak	jak	k6eAd1	jak
parlament	parlament	k1gInSc1	parlament
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
také	také	k9	také
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Soudní	soudní	k2eAgFnSc1d1	soudní
moc	moc	k1gFnSc1	moc
je	být	k5eAaImIp3nS	být
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
na	na	k7c6	na
vládě	vláda	k1gFnSc6	vláda
a	a	k8xC	a
parlamentu	parlament	k1gInSc6	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Prezidentem	prezident	k1gMnSc7	prezident
byl	být	k5eAaImAgMnS	být
Blaise	Blaise	k1gFnSc2	Blaise
Compaoré	Compaorý	k2eAgFnSc2d1	Compaorý
<g/>
,	,	kIx,	,
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
byl	být	k5eAaImAgMnS	být
Tertious	Tertious	k1gMnSc1	Tertious
Zongo	Zongo	k1gMnSc1	Zongo
<g/>
,	,	kIx,	,
zvolený	zvolený	k2eAgMnSc1d1	zvolený
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgMnS	volit
na	na	k7c4	na
7	[number]	k4	7
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
s	s	k7c7	s
neomezeným	omezený	k2eNgNnSc7d1	neomezené
množstvím	množství	k1gNnSc7	množství
následujících	následující	k2eAgInPc2d1	následující
mandátů	mandát	k1gInPc2	mandát
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
je	být	k5eAaImIp3nS	být
vybírán	vybírat	k5eAaImNgInS	vybírat
prezidentem	prezident	k1gMnSc7	prezident
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
schválen	schválit	k5eAaPmNgInS	schválit
parlamentem	parlament	k1gInSc7	parlament
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
svolala	svolat	k5eAaPmAgFnS	svolat
Národní	národní	k2eAgFnSc1d1	národní
fronta	fronta	k1gFnSc1	fronta
první	první	k4xOgInSc4	první
národní	národní	k2eAgInSc4d1	národní
kongres	kongres	k1gInSc4	kongres
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ustanovil	ustanovit	k5eAaPmAgInS	ustanovit
komisi	komise	k1gFnSc4	komise
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
měla	mít	k5eAaImAgFnS	mít
vytvořit	vytvořit	k5eAaPmF	vytvořit
ústavu	ústava	k1gFnSc4	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
schválena	schválit	k5eAaPmNgFnS	schválit
v	v	k7c6	v
referendu	referendum	k1gNnSc6	referendum
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
prezidenta	prezident	k1gMnSc4	prezident
hladce	hladko	k6eAd1	hladko
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Blaise	Blaise	k1gFnPc4	Blaise
Compaoré	Compaorý	k2eAgFnPc1d1	Compaorý
díky	díky	k7c3	díky
bojkotu	bojkot	k1gInSc3	bojkot
voleb	volba	k1gFnPc2	volba
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
opozice	opozice	k1gFnSc2	opozice
<g/>
.	.	kIx.	.
</s>
<s>
Opozice	opozice	k1gFnSc1	opozice
se	se	k3xPyFc4	se
však	však	k9	však
aktivně	aktivně	k6eAd1	aktivně
účastnila	účastnit	k5eAaImAgFnS	účastnit
následných	následný	k2eAgFnPc2d1	následná
voleb	volba	k1gFnPc2	volba
do	do	k7c2	do
parlamentu	parlament	k1gInSc2	parlament
a	a	k8xC	a
strana	strana	k1gFnSc1	strana
ODP	ODP	kA	ODP
<g/>
/	/	kIx~	/
<g/>
MT	MT	kA	MT
získala	získat	k5eAaPmAgFnS	získat
většinu	většina	k1gFnSc4	většina
křesel	křeslo	k1gNnPc2	křeslo
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Současný	současný	k2eAgInSc1d1	současný
vládní	vládní	k2eAgInSc1d1	vládní
systém	systém	k1gInSc1	systém
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
silný	silný	k2eAgInSc4d1	silný
prezidentský	prezidentský	k2eAgInSc4d1	prezidentský
úřad	úřad	k1gInSc4	úřad
<g/>
,	,	kIx,	,
předsedu	předseda	k1gMnSc4	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
radu	rada	k1gFnSc4	rada
ministrů	ministr	k1gMnPc2	ministr
dohlížející	dohlížející	k2eAgNnSc1d1	dohlížející
na	na	k7c4	na
prezidenta	prezident	k1gMnSc4	prezident
<g/>
,	,	kIx,	,
dvoukomorové	dvoukomorový	k2eAgNnSc4d1	dvoukomorové
národní	národní	k2eAgNnSc4d1	národní
shromáždění	shromáždění	k1gNnSc4	shromáždění
a	a	k8xC	a
soudní	soudní	k2eAgFnSc4d1	soudní
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Legislativa	legislativa	k1gFnSc1	legislativa
a	a	k8xC	a
soudní	soudní	k2eAgFnSc1d1	soudní
moc	moc	k1gFnSc1	moc
jsou	být	k5eAaImIp3nP	být
nezávislé	závislý	k2eNgInPc1d1	nezávislý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stále	stále	k6eAd1	stále
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
náchylné	náchylný	k2eAgFnPc1d1	náchylná
k	k	k7c3	k
vnějším	vnější	k2eAgInPc3d1	vnější
vlivům	vliv	k1gInPc3	vliv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
se	se	k3xPyFc4	se
v	v	k7c6	v
Burkině	Burkina	k1gFnSc6	Burkina
Faso	Faso	k6eAd1	Faso
konaly	konat	k5eAaImAgInP	konat
první	první	k4xOgFnPc4	první
pluralitní	pluralitní	k2eAgFnPc4d1	pluralitní
volby	volba	k1gFnPc4	volba
do	do	k7c2	do
obecních	obecní	k2eAgNnPc2d1	obecní
zastupitelstev	zastupitelstvo	k1gNnPc2	zastupitelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
několika	několik	k4yIc2	několik
výjimek	výjimka	k1gFnPc2	výjimka
byly	být	k5eAaImAgFnP	být
volby	volba	k1gFnPc1	volba
shledány	shledat	k5eAaPmNgFnP	shledat
bezúhonnými	bezúhonný	k2eAgFnPc7d1	bezúhonná
a	a	k8xC	a
svobodnými	svobodný	k2eAgFnPc7d1	svobodná
místními	místní	k2eAgFnPc7d1	místní
organizacemi	organizace	k1gFnPc7	organizace
ochraňujícími	ochraňující	k2eAgFnPc7d1	ochraňující
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Vládnoucí	vládnoucí	k2eAgFnSc1d1	vládnoucí
strana	strana	k1gFnSc1	strana
ODP	ODP	kA	ODP
<g/>
/	/	kIx~	/
<g/>
MT	MT	kA	MT
získala	získat	k5eAaPmAgFnS	získat
1100	[number]	k4	1100
křesel	křeslo	k1gNnPc2	křeslo
z	z	k7c2	z
1700	[number]	k4	1700
možných	možný	k2eAgInPc2d1	možný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1996	[number]	k4	1996
se	s	k7c7	s
vládní	vládní	k2eAgFnSc7d1	vládní
ODP	ODP	kA	ODP
<g/>
/	/	kIx~	/
<g/>
MT	MT	kA	MT
sloučila	sloučit	k5eAaPmAgFnS	sloučit
s	s	k7c7	s
několika	několik	k4yIc7	několik
malými	malý	k2eAgFnPc7d1	malá
opozičními	opoziční	k2eAgFnPc7d1	opoziční
stranami	strana	k1gFnPc7	strana
a	a	k8xC	a
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
Kongres	kongres	k1gInSc4	kongres
pro	pro	k7c4	pro
demokracii	demokracie	k1gFnSc4	demokracie
a	a	k8xC	a
pokrok	pokrok	k1gInSc4	pokrok
(	(	kIx(	(
<g/>
CDP	CDP	kA	CDP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zbylé	zbylý	k2eAgFnPc1d1	zbylá
opoziční	opoziční	k2eAgFnPc1d1	opoziční
strany	strana	k1gFnPc1	strana
se	se	k3xPyFc4	se
připravovaly	připravovat	k5eAaImAgFnP	připravovat
na	na	k7c4	na
další	další	k2eAgFnPc4d1	další
volby	volba	k1gFnPc4	volba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
a	a	k8xC	a
1998	[number]	k4	1998
(	(	kIx(	(
<g/>
parlamentní	parlamentní	k2eAgFnSc2d1	parlamentní
a	a	k8xC	a
prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
získal	získat	k5eAaPmAgMnS	získat
CDP	CDP	kA	CDP
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
101	[number]	k4	101
z	z	k7c2	z
111	[number]	k4	111
možných	možný	k2eAgNnPc2d1	možné
křesel	křeslo	k1gNnPc2	křeslo
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
r.	r.	kA	r.
<g/>
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
Kongres	kongres	k1gInSc1	kongres
pro	pro	k7c4	pro
demokracii	demokracie	k1gFnSc4	demokracie
a	a	k8xC	a
pokrok	pokrok	k1gInSc4	pokrok
u	u	k7c2	u
moci	moc	k1gFnSc2	moc
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Blaisem	Blais	k1gInSc7	Blais
Compaorém	Compaorý	k2eAgInSc6d1	Compaorý
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
po	po	k7c6	po
protestech	protest	k1gInPc6	protest
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
/	/	kIx~	/
VIZ	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
odstavec	odstavec	k1gInSc1	odstavec
Současnost	současnost	k1gFnSc4	současnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Přehled	přehled	k1gInSc4	přehled
hlav	hlava	k1gFnPc2	hlava
státu	stát	k1gInSc2	stát
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Burkina	Burkino	k1gNnSc2	Burkino
Faso	Faso	k6eAd1	Faso
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Burkina	Burkino	k1gNnSc2	Burkino
Faso	Faso	k6eAd1	Faso
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnPc1	kategorie
Burkina	Burkino	k1gNnSc2	Burkino
Faso	Faso	k6eAd1	Faso
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
Umění	umění	k1gNnSc1	umění
v	v	k7c6	v
Burkině	Burkina	k1gFnSc6	Burkina
Faso	Faso	k6eAd1	Faso
</s>
</p>
<p>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c4	o
Burkina	Burkin	k2eAgNnPc4d1	Burkin
Faso	Faso	k1gNnSc4	Faso
<g/>
,	,	kIx,	,
fotogalerie	fotogalerie	k1gFnPc4	fotogalerie
a	a	k8xC	a
tipy	tip	k1gInPc4	tip
na	na	k7c4	na
výlet	výlet	k1gInSc4	výlet
–	–	k?	–
česky	česky	k6eAd1	česky
</s>
</p>
<p>
<s>
Radio	radio	k1gNnSc1	radio
Impuls	impuls	k1gInSc1	impuls
<g/>
:	:	kIx,	:
Cestování	cestování	k1gNnSc1	cestování
s	s	k7c7	s
Jiřím	Jiří	k1gMnSc7	Jiří
Kolbabou	Kolbaba	k1gMnSc7	Kolbaba
-	-	kIx~	-
21.12	[number]	k4	21.12
<g/>
.2008	.2008	k4	.2008
</s>
</p>
<p>
<s>
Burkina	Burkina	k1gMnSc1	Burkina
Faso	Faso	k1gMnSc1	Faso
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Burkina	Burkina	k1gMnSc1	Burkina
Faso	Faso	k1gMnSc1	Faso
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
Burkina	Burkina	k1gMnSc1	Burkina
Faso	Faso	k1gMnSc1	Faso
Country	country	k2eAgInSc4d1	country
Report	report	k1gInSc4	report
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
African	African	k1gInSc1	African
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Not	k1gFnSc2	Not
<g/>
:	:	kIx,	:
Burkina	Burkina	k1gMnSc1	Burkina
Faso	Faso	k1gMnSc1	Faso
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011-07-25	[number]	k4	2011-07-25
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Burkina	Burkina	k1gMnSc1	Burkina
Faso	Faso	k1gMnSc1	Faso
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2011-07-07	[number]	k4	2011-07-07
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Akkře	Akkra	k1gFnSc6	Akkra
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Burkina	Burkina	k1gMnSc1	Burkina
Faso	Faso	k1gMnSc1	Faso
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2008-11-10	[number]	k4	2008-11-10
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
DESCHAMPS	DESCHAMPS	kA	DESCHAMPS
<g/>
,	,	kIx,	,
Hubert	Hubert	k1gMnSc1	Hubert
Jules	Jules	k1gMnSc1	Jules
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Burkina	Burkina	k1gMnSc1	Burkina
Faso	Faso	k1gMnSc1	Faso
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
