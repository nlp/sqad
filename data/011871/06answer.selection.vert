<s>
Staňkovský	Staňkovský	k2eAgInSc1d1	Staňkovský
rybník	rybník	k1gInSc1	rybník
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
známý	známý	k2eAgInSc1d1	známý
pod	pod	k7c7	pod
názvy	název	k1gInPc7	název
Velký	velký	k2eAgMnSc1d1	velký
Soused	soused	k1gMnSc1	soused
a	a	k8xC	a
Velký	velký	k2eAgInSc1d1	velký
Bystřický	bystřický	k2eAgInSc1d1	bystřický
rybník	rybník	k1gInSc1	rybník
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
osmý	osmý	k4xOgMnSc1	osmý
největší	veliký	k2eAgInSc1d3	veliký
rybník	rybník	k1gInSc1	rybník
v	v	k7c6	v
Jihočeském	jihočeský	k2eAgInSc6d1	jihočeský
kraji	kraj	k1gInSc6	kraj
a	a	k8xC	a
dvanáctý	dvanáctý	k4xOgInSc4	dvanáctý
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
