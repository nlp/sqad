<s>
Zinédine	Zinédin	k1gMnSc5	Zinédin
Zidane	Zidan	k1gMnSc5	Zidan
(	(	kIx(	(
<g/>
*	*	kIx~	*
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1972	[number]	k4	1972
<g/>
,	,	kIx,	,
Marseille	Marseille	k1gFnSc1	Marseille
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
francouzský	francouzský	k2eAgMnSc1d1	francouzský
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
záložník	záložník	k1gMnSc1	záložník
a	a	k8xC	a
reprezentant	reprezentant	k1gMnSc1	reprezentant
alžírského	alžírský	k2eAgInSc2d1	alžírský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
trenér	trenér	k1gMnSc1	trenér
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecně	všeobecně	k6eAd1	všeobecně
je	být	k5eAaImIp3nS	být
považovaný	považovaný	k2eAgInSc1d1	považovaný
za	za	k7c4	za
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
fotbalistů	fotbalista	k1gMnPc2	fotbalista
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
s	s	k7c7	s
francouzským	francouzský	k2eAgInSc7d1	francouzský
národním	národní	k2eAgInSc7d1	národní
týmem	tým	k1gInSc7	tým
je	být	k5eAaImIp3nS	být
mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
i	i	k8xC	i
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Pelé	Pelé	k1gNnSc1	Pelé
ho	on	k3xPp3gInSc2	on
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
zařadil	zařadit	k5eAaPmAgInS	zařadit
mezi	mezi	k7c4	mezi
125	[number]	k4	125
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
žijících	žijící	k2eAgMnPc2d1	žijící
fotbalistů	fotbalista	k1gMnPc2	fotbalista
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ledna	leden	k1gInSc2	leden
2016	[number]	k4	2016
je	být	k5eAaImIp3nS	být
trenérem	trenér	k1gMnSc7	trenér
Realu	Real	k1gInSc2	Real
Madrid	Madrid	k1gInSc1	Madrid
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
silnější	silný	k2eAgFnSc1d2	silnější
noha	noha	k1gFnSc1	noha
je	být	k5eAaImIp3nS	být
levá	levý	k2eAgFnSc1d1	levá
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
fotbalem	fotbal	k1gInSc7	fotbal
začínal	začínat	k5eAaImAgMnS	začínat
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
AS	as	k1gNnSc2	as
Cannes	Cannes	k1gNnSc2	Cannes
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrál	hrát	k5eAaImAgInS	hrát
v	v	k7c6	v
juniorských	juniorský	k2eAgFnPc6d1	juniorská
kategoriích	kategorie	k1gFnPc6	kategorie
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
v	v	k7c6	v
A-týmu	Aým	k1gInSc6	A-tým
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
měl	mít	k5eAaImAgMnS	mít
přestoupit	přestoupit	k5eAaPmF	přestoupit
do	do	k7c2	do
anglického	anglický	k2eAgInSc2d1	anglický
týmu	tým	k1gInSc2	tým
Newcastle	Newcastle	k1gMnSc1	Newcastle
United	United	k1gMnSc1	United
FC	FC	kA	FC
<g/>
,	,	kIx,	,
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
trenér	trenér	k1gMnSc1	trenér
ho	on	k3xPp3gNnSc4	on
ale	ale	k9	ale
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
,	,	kIx,	,
Zidane	Zidan	k1gMnSc5	Zidan
prý	prý	k9	prý
nebyl	být	k5eNaImAgInS	být
dost	dost	k6eAd1	dost
dobrý	dobrý	k2eAgInSc1d1	dobrý
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
hrál	hrát	k5eAaImAgInS	hrát
Premier	Premier	k1gInSc1	Premier
League	Leagu	k1gInSc2	Leagu
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Proto	proto	k8xC	proto
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
do	do	k7c2	do
jiného	jiný	k2eAgInSc2d1	jiný
francouzského	francouzský	k2eAgInSc2d1	francouzský
týmu	tým	k1gInSc2	tým
<g/>
,	,	kIx,	,
FC	FC	kA	FC
Girondins	Girondins	k1gInSc1	Girondins
de	de	k?	de
Bordeaux	Bordeaux	k1gNnSc2	Bordeaux
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bordeaux	Bordeaux	k1gNnSc6	Bordeaux
strávil	strávit	k5eAaPmAgMnS	strávit
"	"	kIx"	"
<g/>
Zizou	Ziza	k1gMnSc7	Ziza
<g/>
"	"	kIx"	"
4	[number]	k4	4
sezony	sezona	k1gFnSc2	sezona
<g/>
.	.	kIx.	.
</s>
<s>
Zaujal	zaujmout	k5eAaPmAgMnS	zaujmout
i	i	k9	i
trenéra	trenér	k1gMnSc4	trenér
francouzské	francouzský	k2eAgFnSc2d1	francouzská
reprezentace	reprezentace	k1gFnSc2	reprezentace
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
debutoval	debutovat	k5eAaBmAgMnS	debutovat
za	za	k7c4	za
A-mužstvo	Aužstvo	k1gNnSc4	A-mužstvo
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
EURU	euro	k1gNnSc6	euro
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
skončia	skončia	k1gFnSc1	skončia
Francie	Francie	k1gFnSc2	Francie
třetí	třetí	k4xOgFnSc7	třetí
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
do	do	k7c2	do
italského	italský	k2eAgInSc2d1	italský
týmu	tým	k1gInSc2	tým
Juventus	Juventus	k1gInSc1	Juventus
FC	FC	kA	FC
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Juventusu	Juventus	k1gInSc6	Juventus
se	se	k3xPyFc4	se
ihned	ihned	k6eAd1	ihned
po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
stal	stát	k5eAaPmAgMnS	stát
klíčovým	klíčový	k2eAgMnSc7d1	klíčový
hráčem	hráč	k1gMnSc7	hráč
<g/>
,	,	kIx,	,
za	za	k7c4	za
5	[number]	k4	5
let	léto	k1gNnPc2	léto
zde	zde	k6eAd1	zde
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
2	[number]	k4	2
<g/>
x	x	k?	x
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
italskou	italský	k2eAgFnSc4d1	italská
ligu	liga	k1gFnSc4	liga
<g/>
,	,	kIx,	,
zahrál	zahrát	k5eAaPmAgMnS	zahrát
si	se	k3xPyFc3	se
dvakrát	dvakrát	k6eAd1	dvakrát
finále	finále	k1gNnSc4	finále
Champions	Championsa	k1gFnPc2	Championsa
League	Leagu	k1gInSc2	Leagu
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInSc1	jeho
tým	tým	k1gInSc1	tým
prohrál	prohrát	k5eAaPmAgInS	prohrát
s	s	k7c7	s
Borussií	Borussie	k1gFnSc7	Borussie
Dortmund	Dortmund	k1gInSc1	Dortmund
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
pro	pro	k7c4	pro
změnu	změna	k1gFnSc4	změna
s	s	k7c7	s
Realem	Real	k1gInSc7	Real
Madrid	Madrid	k1gInSc1	Madrid
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
1998	[number]	k4	1998
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
byl	být	k5eAaImAgInS	být
zlomový	zlomový	k2eAgInSc1d1	zlomový
<g/>
,	,	kIx,	,
Juventus	Juventus	k1gInSc1	Juventus
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
italskou	italský	k2eAgFnSc4d1	italská
ligu	liga	k1gFnSc4	liga
<g/>
,	,	kIx,	,
zahrál	zahrát	k5eAaPmAgMnS	zahrát
si	se	k3xPyFc3	se
finále	finále	k1gNnSc4	finále
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
a	a	k8xC	a
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
probíhalo	probíhat	k5eAaImAgNnS	probíhat
právě	právě	k9	právě
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
Zlatý	zlatý	k2eAgInSc1d1	zlatý
míč	míč	k1gInSc1	míč
za	za	k7c4	za
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
fotbalistu	fotbalista	k1gMnSc4	fotbalista
Evropy	Evropa	k1gFnSc2	Evropa
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Realu	Real	k1gInSc2	Real
Madrid	Madrid	k1gInSc1	Madrid
přišel	přijít	k5eAaPmAgInS	přijít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
z	z	k7c2	z
Juventusu	Juventus	k1gInSc2	Juventus
Turín	Turín	k1gInSc1	Turín
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
jeho	jeho	k3xOp3gInSc4	jeho
přestup	přestup	k1gInSc4	přestup
zaplatil	zaplatit	k5eAaPmAgInS	zaplatit
Real	Real	k1gInSc1	Real
66	[number]	k4	66
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nejdražším	drahý	k2eAgMnSc7d3	nejdražší
hráčem	hráč	k1gMnSc7	hráč
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
v	v	k7c6	v
první	první	k4xOgFnSc6	první
sezoně	sezona	k1gFnSc6	sezona
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
s	s	k7c7	s
Realem	Real	k1gInSc7	Real
Ligu	liga	k1gFnSc4	liga
mistrů	mistr	k1gMnPc2	mistr
<g/>
,	,	kIx,	,
proslavil	proslavit	k5eAaPmAgMnS	proslavit
se	se	k3xPyFc4	se
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
hlavně	hlavně	k9	hlavně
gólem	gól	k1gInSc7	gól
ve	v	k7c4	v
finále	finále	k1gNnSc4	finále
proti	proti	k7c3	proti
Bayeru	Bayer	k1gMnSc3	Bayer
Leverkusen	Leverkusna	k1gFnPc2	Leverkusna
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Realu	Real	k1gInSc6	Real
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
španělskou	španělský	k2eAgFnSc4d1	španělská
La	la	k1gNnSc2	la
Ligu	liga	k1gFnSc4	liga
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Realu	Real	k1gInSc6	Real
skončil	skončit	k5eAaPmAgInS	skončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
po	po	k7c6	po
Mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
2006	[number]	k4	2006
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Francie	Francie	k1gFnSc1	Francie
skončila	skončit	k5eAaPmAgFnS	skončit
druhá	druhý	k4xOgFnSc1	druhý
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
ukončit	ukončit	k5eAaPmF	ukončit
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
EURO	euro	k1gNnSc1	euro
1996	[number]	k4	1996
byl	být	k5eAaImAgMnS	být
Zidanův	Zidanův	k2eAgMnSc1d1	Zidanův
první	první	k4xOgMnSc1	první
velký	velký	k2eAgInSc1d1	velký
turnaj	turnaj	k1gInSc1	turnaj
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
skončila	skončit	k5eAaPmAgFnS	skončit
třetí	třetí	k4xOgFnSc1	třetí
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
prohrála	prohrát	k5eAaPmAgFnS	prohrát
s	s	k7c7	s
Českou	český	k2eAgFnSc7d1	Česká
republikou	republika	k1gFnSc7	republika
na	na	k7c6	na
penalty	penalty	k1gNnPc6	penalty
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Zidane	Zidanout	k5eAaPmIp3nS	Zidanout
svoji	svůj	k3xOyFgFnSc4	svůj
penaltu	penalta	k1gFnSc4	penalta
v	v	k7c6	v
rozstřelu	rozstřel	k1gInSc6	rozstřel
proměnil	proměnit	k5eAaPmAgMnS	proměnit
<g/>
.	.	kIx.	.
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
1998	[number]	k4	1998
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
celý	celý	k2eAgInSc4d1	celý
turnaj	turnaj	k1gInSc4	turnaj
i	i	k9	i
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Zidane	Zidanout	k5eAaPmIp3nS	Zidanout
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
jako	jako	k8xS	jako
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
hráč	hráč	k1gMnSc1	hráč
turnaje	turnaj	k1gInSc2	turnaj
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgMnSc3	ten
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
i	i	k9	i
Zlatý	zlatý	k2eAgInSc1d1	zlatý
míč	míč	k1gInSc1	míč
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
porazila	porazit	k5eAaPmAgFnS	porazit
Brazílii	Brazílie	k1gFnSc4	Brazílie
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
Zidane	Zidan	k1gMnSc5	Zidan
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
2	[number]	k4	2
góly	gól	k1gInPc4	gól
<g/>
.	.	kIx.	.
</s>
<s>
EURO	euro	k1gNnSc1	euro
2000	[number]	k4	2000
se	se	k3xPyFc4	se
konalo	konat	k5eAaImAgNnS	konat
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
a	a	k8xC	a
Nizozemsku	Nizozemsko	k1gNnSc6	Nizozemsko
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
tento	tento	k3xDgInSc4	tento
turnaj	turnaj	k1gInSc4	turnaj
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
<g/>
,	,	kIx,	,
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
porazila	porazit	k5eAaPmAgFnS	porazit
Itálii	Itálie	k1gFnSc4	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
před	před	k7c7	před
2	[number]	k4	2
roky	rok	k1gInPc7	rok
byl	být	k5eAaImAgMnS	být
Zidane	Zidan	k1gMnSc5	Zidan
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
hráčem	hráč	k1gMnSc7	hráč
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Finále	finále	k1gNnSc1	finále
v	v	k7c6	v
Rotterdamu	Rotterdam	k1gInSc6	Rotterdam
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
útočník	útočník	k1gMnSc1	útočník
David	David	k1gMnSc1	David
Trézéguet	Trézéguet	k1gMnSc1	Trézéguet
v	v	k7c6	v
prodloužení	prodloužení	k1gNnSc6	prodloužení
<g/>
.	.	kIx.	.
</s>
<s>
Zidane	Zidanout	k5eAaPmIp3nS	Zidanout
byl	být	k5eAaImAgMnS	být
také	také	k6eAd1	také
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
All-Stars	All-Starsa	k1gFnPc2	All-Starsa
turnaje	turnaj	k1gInSc2	turnaj
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Francie	Francie	k1gFnSc2	Francie
se	se	k3xPyFc4	se
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
turnaji	turnaj	k1gInSc6	turnaj
očekávalo	očekávat	k5eAaImAgNnS	očekávat
hodně	hodně	k6eAd1	hodně
<g/>
,	,	kIx,	,
tým	tým	k1gInSc1	tým
galského	galský	k2eAgMnSc2d1	galský
kohouta	kohout	k1gMnSc2	kohout
ale	ale	k8xC	ale
vypadl	vypadnout	k5eAaPmAgMnS	vypadnout
už	už	k6eAd1	už
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
skupině	skupina	k1gFnSc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Turnaj	turnaj	k1gInSc1	turnaj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
a	a	k8xC	a
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ve	v	k7c4	v
finále	finále	k1gNnSc4	finále
porazila	porazit	k5eAaPmAgFnS	porazit
Německo	Německo	k1gNnSc4	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
Senegalu	Senegal	k1gInSc2	Senegal
a	a	k8xC	a
Dánsku	Dánsko	k1gNnSc6	Dánsko
<g/>
,	,	kIx,	,
jediný	jediný	k2eAgInSc4d1	jediný
bod	bod	k1gInSc4	bod
uhrála	uhrát	k5eAaPmAgFnS	uhrát
s	s	k7c7	s
Uruguayí	Uruguay	k1gFnSc7	Uruguay
<g/>
.	.	kIx.	.
</s>
<s>
Zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
se	se	k3xPyFc4	se
Mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
jej	on	k3xPp3gMnSc4	on
nominoval	nominovat	k5eAaBmAgMnS	nominovat
trenér	trenér	k1gMnSc1	trenér
Jacques	Jacques	k1gMnSc1	Jacques
Santini	Santin	k2eAgMnPc1d1	Santin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgMnSc6	první
ostře	ostro	k6eAd1	ostro
sledovaném	sledovaný	k2eAgNnSc6d1	sledované
utkání	utkání	k1gNnSc6	utkání
s	s	k7c7	s
rivalem	rival	k1gMnSc7	rival
Anglií	Anglie	k1gFnSc7	Anglie
dokázal	dokázat	k5eAaPmAgInS	dokázat
dvěma	dva	k4xCgInPc7	dva
góly	gól	k1gInPc7	gól
v	v	k7c6	v
nastaveném	nastavený	k2eAgInSc6d1	nastavený
čase	čas	k1gInSc6	čas
(	(	kIx(	(
<g/>
první	první	k4xOgMnPc1	první
z	z	k7c2	z
přímého	přímý	k2eAgInSc2d1	přímý
kopu	kop	k1gInSc2	kop
a	a	k8xC	a
druhý	druhý	k4xOgInSc4	druhý
z	z	k7c2	z
pokutového	pokutový	k2eAgInSc2d1	pokutový
kopu	kop	k1gInSc2	kop
<g/>
)	)	kIx)	)
zvrátit	zvrátit	k5eAaPmF	zvrátit
nepříznivý	příznivý	k2eNgInSc4d1	nepříznivý
stav	stav	k1gInSc4	stav
na	na	k7c6	na
konečných	konečný	k2eAgInPc6d1	konečný
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
Chorvatsku	Chorvatsko	k1gNnSc3	Chorvatsko
se	se	k3xPyFc4	se
gólově	gólově	k6eAd1	gólově
neprosadil	prosadit	k5eNaPmAgMnS	prosadit
<g/>
,	,	kIx,	,
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
se	se	k3xPyFc4	se
zrodila	zrodit	k5eAaPmAgFnS	zrodit
remíza	remíza	k1gFnSc1	remíza
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Zato	zato	k6eAd1	zato
se	se	k3xPyFc4	se
střelecky	střelecky	k6eAd1	střelecky
prosadil	prosadit	k5eAaPmAgMnS	prosadit
v	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
zápase	zápas	k1gInSc6	zápas
v	v	k7c6	v
základní	základní	k2eAgFnSc6d1	základní
skupině	skupina	k1gFnSc6	skupina
B	B	kA	B
proti	proti	k7c3	proti
Švýcarsku	Švýcarsko	k1gNnSc3	Švýcarsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vsítil	vsítit	k5eAaPmAgMnS	vsítit
první	první	k4xOgFnSc4	první
branku	branka	k1gFnSc4	branka
(	(	kIx(	(
<g/>
výhra	výhra	k1gFnSc1	výhra
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
s	s	k7c7	s
Řeckem	Řecko	k1gNnSc7	Řecko
nedokázal	dokázat	k5eNaPmAgMnS	dokázat
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
spoluhráči	spoluhráč	k1gMnPc7	spoluhráč
najít	najít	k5eAaPmF	najít
recept	recept	k1gInSc4	recept
na	na	k7c4	na
pozornou	pozorný	k2eAgFnSc4d1	pozorná
řeckou	řecký	k2eAgFnSc4d1	řecká
defensivu	defensiva	k1gFnSc4	defensiva
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
pozdějšímu	pozdní	k2eAgMnSc3d2	pozdější
vítězi	vítěz	k1gMnSc3	vítěz
šampionátu	šampionát	k1gInSc2	šampionát
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sezoně	sezona	k1gFnSc6	sezona
2005	[number]	k4	2005
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
skončil	skončit	k5eAaPmAgMnS	skončit
Zidane	Zidan	k1gMnSc5	Zidan
s	s	k7c7	s
fotbalem	fotbal	k1gInSc7	fotbal
v	v	k7c6	v
Realu	Real	k1gInSc6	Real
<g/>
,	,	kIx,	,
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
2006	[number]	k4	2006
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
se	se	k3xPyFc4	se
Francie	Francie	k1gFnSc1	Francie
probojovala	probojovat	k5eAaPmAgFnS	probojovat
až	až	k9	až
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
Itálii	Itálie	k1gFnSc4	Itálie
na	na	k7c4	na
penalty	penalta	k1gFnPc4	penalta
<g/>
.	.	kIx.	.
</s>
<s>
Zidane	Zidanout	k5eAaPmIp3nS	Zidanout
se	se	k3xPyFc4	se
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
postaral	postarat	k5eAaPmAgMnS	postarat
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejvýraznějších	výrazný	k2eAgInPc2d3	nejvýraznější
momentů	moment	k1gInPc2	moment
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
nastaveném	nastavený	k2eAgInSc6d1	nastavený
čase	čas	k1gInSc6	čas
udeřil	udeřit	k5eAaPmAgInS	udeřit
Itala	Ital	k1gMnSc4	Ital
Marca	Marcus	k1gMnSc4	Marcus
Materazziho	Materazzi	k1gMnSc4	Materazzi
hlavou	hlava	k1gFnSc7	hlava
do	do	k7c2	do
hrudi	hruď	k1gFnSc2	hruď
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yRnSc4	což
byl	být	k5eAaImAgInS	být
vyloučen	vyloučen	k2eAgInSc1d1	vyloučen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
2013	[number]	k4	2013
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
vedl	vést	k5eAaImAgInS	vést
Real	Real	k1gInSc1	Real
Madrid	Madrid	k1gInSc1	Madrid
Ital	Ital	k1gMnSc1	Ital
Carlo	Carlo	k1gNnSc1	Carlo
Ancelotti	Ancelotti	k1gNnSc3	Ancelotti
<g/>
.	.	kIx.	.
</s>
<s>
Ancelotti	Ancelotť	k1gFnPc4	Ancelotť
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgInS	vzít
Zidana	Zidan	k1gMnSc4	Zidan
do	do	k7c2	do
týmu	tým	k1gInSc2	tým
jako	jako	k8xC	jako
jeho	jeho	k3xOp3gMnSc2	jeho
asistenta	asistent	k1gMnSc2	asistent
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
sezóně	sezóna	k1gFnSc6	sezóna
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Real	Real	k1gInSc4	Real
Ligu	liga	k1gFnSc4	liga
mistrů	mistr	k1gMnPc2	mistr
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
porazil	porazit	k5eAaPmAgMnS	porazit
městského	městský	k2eAgMnSc4d1	městský
rivala	rival	k1gMnSc4	rival
Atlético	Atlético	k6eAd1	Atlético
Madrid	Madrid	k1gInSc1	Madrid
<g/>
,	,	kIx,	,
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
minutě	minuta	k1gFnSc6	minuta
srovnal	srovnat	k5eAaPmAgMnS	srovnat
stav	stav	k1gInSc4	stav
zápasu	zápas	k1gInSc2	zápas
Sergio	Sergio	k1gMnSc1	Sergio
Ramos	Ramos	k1gMnSc1	Ramos
<g/>
,	,	kIx,	,
v	v	k7c6	v
prodloužení	prodloužení	k1gNnSc6	prodloužení
vstřelili	vstřelit	k5eAaPmAgMnP	vstřelit
góly	gól	k1gInPc4	gól
Gareth	Gareth	k1gInSc1	Gareth
Bale	bal	k1gInSc5	bal
<g/>
,	,	kIx,	,
Marcelo	Marcela	k1gFnSc5	Marcela
a	a	k8xC	a
Cristiano	Cristiana	k1gFnSc5	Cristiana
Ronaldo	Ronalda	k1gMnSc5	Ronalda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
sezoně	sezona	k1gFnSc6	sezona
taktéž	taktéž	k?	taktéž
Real	Real	k1gInSc4	Real
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Copa	Copa	k1gMnSc1	Copa
del	del	k?	del
Rey	Rea	k1gFnSc2	Rea
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úspěšné	úspěšný	k2eAgFnSc6d1	úspěšná
sezoně	sezona	k1gFnSc6	sezona
jako	jako	k9	jako
asistent	asistent	k1gMnSc1	asistent
převzal	převzít	k5eAaPmAgMnS	převzít
"	"	kIx"	"
<g/>
B-tým	Bý	k1gMnSc7	B-tý
<g/>
"	"	kIx"	"
Realu	Real	k1gInSc2	Real
<g/>
,	,	kIx,	,
takzvaný	takzvaný	k2eAgInSc1d1	takzvaný
Real	Real	k1gInSc1	Real
Madrid	Madrid	k1gInSc1	Madrid
Castilla	Castilla	k1gFnSc1	Castilla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sezoně	sezona	k1gFnSc6	sezona
2013	[number]	k4	2013
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
Castilla	Castilla	k1gFnSc1	Castilla
sestoupila	sestoupit	k5eAaPmAgFnS	sestoupit
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
ligy	liga	k1gFnSc2	liga
do	do	k7c2	do
třetí	třetí	k4xOgFnSc2	třetí
<g/>
,	,	kIx,	,
Zidanův	Zidanův	k2eAgInSc4d1	Zidanův
cíl	cíl	k1gInSc4	cíl
v	v	k7c6	v
nadcházející	nadcházející	k2eAgFnSc6d1	nadcházející
sezóně	sezóna	k1gFnSc6	sezóna
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
15	[number]	k4	15
byl	být	k5eAaImAgInS	být
tedy	tedy	k9	tedy
jasný	jasný	k2eAgInSc1d1	jasný
<g/>
:	:	kIx,	:
měl	mít	k5eAaImAgInS	mít
se	se	k3xPyFc4	se
vrátit	vrátit	k5eAaPmF	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
tým	tým	k1gInSc1	tým
nakonec	nakonec	k6eAd1	nakonec
skončil	skončit	k5eAaPmAgInS	skončit
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
na	na	k7c4	na
postup	postup	k1gInSc4	postup
nestačilo	stačit	k5eNaBmAgNnS	stačit
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
však	však	k9	však
byl	být	k5eAaImAgInS	být
Florentino	Florentin	k2eAgNnSc4d1	Florentino
Peréz	Peréza	k1gFnPc2	Peréza
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
Realu	Real	k1gInSc2	Real
s	s	k7c7	s
Zidanovou	Zidanový	k2eAgFnSc7d1	Zidanový
hrou	hra	k1gFnSc7	hra
spokojený	spokojený	k2eAgMnSc1d1	spokojený
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
ho	on	k3xPp3gNnSc4	on
trénovat	trénovat	k5eAaImF	trénovat
Castillu	Castilla	k1gFnSc4	Castilla
i	i	k8xC	i
další	další	k2eAgFnSc4d1	další
sezonu	sezona	k1gFnSc4	sezona
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sezóně	sezóna	k1gFnSc6	sezóna
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
15	[number]	k4	15
byl	být	k5eAaImAgMnS	být
odvolán	odvolat	k5eAaPmNgMnS	odvolat
od	od	k7c2	od
A-týmu	Aým	k1gInSc2	A-tým
Realu	Real	k1gInSc2	Real
trenér	trenér	k1gMnSc1	trenér
Carlo	Carlo	k1gNnSc1	Carlo
Ancelotti	Ancelotti	k1gNnSc1	Ancelotti
a	a	k8xC	a
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
Španěl	Španěl	k1gMnSc1	Španěl
Rafael	Rafael	k1gMnSc1	Rafael
Benítez	Benítez	k1gMnSc1	Benítez
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nevydařeném	vydařený	k2eNgInSc6d1	nevydařený
podzimu	podzim	k1gInSc6	podzim
2015	[number]	k4	2015
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
odvolán	odvolán	k2eAgMnSc1d1	odvolán
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
funkci	funkce	k1gFnSc4	funkce
převzal	převzít	k5eAaPmAgMnS	převzít
právě	právě	k9	právě
Zidane	Zidan	k1gMnSc5	Zidan
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
zápas	zápas	k1gInSc1	zápas
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gNnSc7	jeho
vedením	vedení	k1gNnSc7	vedení
proti	proti	k7c3	proti
Deportivu	Deportivum	k1gNnSc3	Deportivum
La	la	k1gNnSc2	la
Coruňa	Coruň	k1gInSc2	Coruň
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
Real	Real	k1gInSc4	Real
Madrid	Madrid	k1gInSc4	Madrid
vysoko	vysoko	k6eAd1	vysoko
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
osmifinále	osmifinále	k1gNnSc6	osmifinále
Liga	liga	k1gFnSc1	liga
mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
porazil	porazit	k5eAaPmAgInS	porazit
jeho	jeho	k3xOp3gInSc1	jeho
tým	tým	k1gInSc1	tým
AS	as	k9	as
Řím	Řím	k1gInSc1	Řím
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
Benitéze	Benitéza	k1gFnSc3	Benitéza
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
Real	Real	k1gInSc1	Real
Madrid	Madrid	k1gInSc1	Madrid
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
sezoně	sezona	k1gFnSc6	sezona
La	la	k1gNnSc2	la
Ligu	liga	k1gFnSc4	liga
nevyhraje	vyhrát	k5eNaPmIp3nS	vyhrát
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
pro	pro	k7c4	pro
Zidana	Zidan	k1gMnSc4	Zidan
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
prioritou	priorita	k1gFnSc7	priorita
právě	právě	k9	právě
milionářská	milionářský	k2eAgFnSc1d1	milionářská
Liga	liga	k1gFnSc1	liga
mistrů	mistr	k1gMnPc2	mistr
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtfinálový	čtvrtfinálový	k2eAgInSc1d1	čtvrtfinálový
los	los	k1gInSc1	los
Champions	Championsa	k1gFnPc2	Championsa
League	League	k1gFnPc2	League
udělil	udělit	k5eAaPmAgInS	udělit
Realu	Real	k1gInSc3	Real
německý	německý	k2eAgInSc1d1	německý
tým	tým	k1gInSc1	tým
VfL	VfL	k1gMnSc1	VfL
Wolfsburg	Wolfsburg	k1gInSc1	Wolfsburg
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
zápas	zápas	k1gInSc4	zápas
Zidanův	Zidanův	k2eAgInSc4d1	Zidanův
tým	tým	k1gInSc4	tým
ale	ale	k8xC	ale
nezvládl	zvládnout	k5eNaPmAgMnS	zvládnout
<g/>
,	,	kIx,	,
prohrál	prohrát	k5eAaPmAgMnS	prohrát
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
týden	týden	k1gInSc4	týden
později	pozdě	k6eAd2	pozdě
ale	ale	k8xC	ale
Real	Real	k1gInSc4	Real
Madrid	Madrid	k1gInSc1	Madrid
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
díky	díky	k7c3	díky
hattricku	hattrick	k1gInSc3	hattrick
Portugalce	Portugalec	k1gMnSc2	Portugalec
Cristiana	Cristian	k1gMnSc2	Cristian
Ronalda	Ronald	k1gMnSc2	Ronald
<g/>
.	.	kIx.	.
</s>
<s>
Zidanův	Zidanův	k2eAgInSc1d1	Zidanův
tým	tým	k1gInSc1	tým
dotáhl	dotáhnout	k5eAaPmAgInS	dotáhnout
v	v	k7c6	v
La	la	k1gNnSc6	la
Lize	liga	k1gFnSc6	liga
ztrátu	ztráta	k1gFnSc4	ztráta
Realu	Real	k1gInSc2	Real
na	na	k7c4	na
Barcelonu	Barcelona	k1gFnSc4	Barcelona
z	z	k7c2	z
13	[number]	k4	13
bodů	bod	k1gInPc2	bod
na	na	k7c4	na
pouhý	pouhý	k2eAgInSc4d1	pouhý
jediný	jediný	k2eAgInSc4d1	jediný
bod	bod	k1gInSc4	bod
a	a	k8xC	a
oživil	oživit	k5eAaPmAgMnS	oživit
tak	tak	k6eAd1	tak
šanci	šance	k1gFnSc4	šance
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
Real	Real	k1gInSc1	Real
Madrid	Madrid	k1gInSc4	Madrid
mohl	moct	k5eAaImAgInS	moct
vyhrát	vyhrát	k5eAaPmF	vyhrát
i	i	k9	i
domácí	domácí	k2eAgFnSc4d1	domácí
ligovou	ligový	k2eAgFnSc4d1	ligová
soutěž	soutěž	k1gFnSc4	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
ale	ale	k8xC	ale
Zidane	Zidan	k1gMnSc5	Zidan
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
první	první	k4xOgFnSc6	první
sezóně	sezóna	k1gFnSc6	sezóna
jako	jako	k9	jako
kouč	kouč	k1gMnSc1	kouč
Ligu	liga	k1gFnSc4	liga
mistrů	mistr	k1gMnPc2	mistr
<g/>
,	,	kIx,	,
v	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
vyřadil	vyřadit	k5eAaPmAgInS	vyřadit
Manchester	Manchester	k1gInSc1	Manchester
City	City	k1gFnSc2	City
FC	FC	kA	FC
a	a	k8xC	a
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
jeho	jeho	k3xOp3gInSc1	jeho
tým	tým	k1gInSc1	tým
porazil	porazit	k5eAaPmAgInS	porazit
na	na	k7c4	na
penalty	penalty	k1gNnSc4	penalty
městského	městský	k2eAgMnSc2d1	městský
rivala	rival	k1gMnSc2	rival
Atlético	Atlético	k1gMnSc1	Atlético
Madrid	Madrid	k1gInSc1	Madrid
<g/>
.	.	kIx.	.
</s>
<s>
Vítěz	vítěz	k1gMnSc1	vítěz
Mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Vítěz	vítěz	k1gMnSc1	vítěz
Mistrovství	mistrovství	k1gNnSc2	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Vítěz	vítězit	k5eAaImRp2nS	vítězit
ankety	anketa	k1gFnSc2	anketa
Zlatý	zlatý	k2eAgInSc1d1	zlatý
míč	míč	k1gInSc1	míč
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
Vítěz	vítěz	k1gMnSc1	vítěz
ankety	anketa	k1gFnSc2	anketa
Fotbalista	fotbalista	k1gMnSc1	fotbalista
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
FIFA	FIFA	kA	FIFA
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Mistr	mistr	k1gMnSc1	mistr
<g />
.	.	kIx.	.
</s>
<s>
Serie	serie	k1gFnSc1	serie
A	A	kA	A
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
s	s	k7c7	s
Juventusem	Juventus	k1gInSc7	Juventus
Turín	Turín	k1gInSc1	Turín
Mistr	mistr	k1gMnSc1	mistr
Primera	primera	k1gFnSc1	primera
División	División	k1gMnSc1	División
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
s	s	k7c7	s
Realem	Real	k1gInSc7	Real
Madrid	Madrid	k1gInSc4	Madrid
Vítěz	vítěz	k1gMnSc1	vítěz
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
s	s	k7c7	s
Realem	Real	k1gInSc7	Real
Madrid	Madrid	k1gInSc1	Madrid
Vítěz	vítěz	k1gMnSc1	vítěz
Interkontinentálního	interkontinentální	k2eAgInSc2d1	interkontinentální
poháru	pohár	k1gInSc2	pohár
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Vítěz	vítěz	k1gMnSc1	vítěz
Superpoháru	superpohár	k1gInSc2	superpohár
UEFA	UEFA	kA	UEFA
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Vítěz	vítěz	k1gMnSc1	vítěz
španělského	španělský	k2eAgInSc2d1	španělský
superpoháru	superpohár	k1gInSc2	superpohár
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Vítěz	vítěz	k1gMnSc1	vítěz
italského	italský	k2eAgInSc2d1	italský
superpoháru	superpohár	k1gInSc2	superpohár
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
Vítěz	vítěz	k1gMnSc1	vítěz
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
asistent	asistent	k1gMnSc1	asistent
Carla	Carl	k1gMnSc2	Carl
Ancelottiho	Ancelotti	k1gMnSc2	Ancelotti
s	s	k7c7	s
Realem	Real	k1gInSc7	Real
Madrid	Madrid	k1gInSc1	Madrid
Vítěz	vítěz	k1gMnSc1	vítěz
Copa	Copa	k1gMnSc1	Copa
del	del	k?	del
Rey	Rea	k1gFnSc2	Rea
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
asistent	asistent	k1gMnSc1	asistent
Carla	Carl	k1gMnSc2	Carl
Ancelottiho	Ancelotti	k1gMnSc2	Ancelotti
s	s	k7c7	s
Realem	Real	k1gInSc7	Real
Madrid	Madrid	k1gInSc4	Madrid
Vítěz	vítěz	k1gMnSc1	vítěz
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
trenér	trenér	k1gMnSc1	trenér
Realu	Real	k1gInSc2	Real
Madrid	Madrid	k1gInSc1	Madrid
</s>
