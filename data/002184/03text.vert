<s>
Charleston	charleston	k1gInSc1	charleston
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgNnSc7d1	hlavní
a	a	k8xC	a
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
amerického	americký	k2eAgInSc2d1	americký
státu	stát	k1gInSc2	stát
Západní	západní	k2eAgFnSc2d1	západní
Virginie	Virginie	k1gFnSc2	Virginie
nalézající	nalézající	k2eAgFnSc2d1	nalézající
se	se	k3xPyFc4	se
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
řek	řek	k1gMnSc1	řek
Elk	Elk	k1gMnSc1	Elk
a	a	k8xC	a
Kanawha	Kanawha	k1gMnSc1	Kanawha
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Kanawha	Kanawha	k1gFnSc1	Kanawha
<g/>
.	.	kIx.	.
</s>
<s>
Údaje	údaj	k1gInPc1	údaj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
uvádějí	uvádět	k5eAaImIp3nP	uvádět
53	[number]	k4	53
421	[number]	k4	421
obyvatel	obyvatel	k1gMnPc2	obyvatel
centrálního	centrální	k2eAgNnSc2d1	centrální
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
byly	být	k5eAaImAgFnP	být
nejdůležitějšími	důležitý	k2eAgInPc7d3	nejdůležitější
průmyslovými	průmyslový	k2eAgInPc7d1	průmyslový
zdroji	zdroj	k1gInPc7	zdroj
sůl	sůl	k1gFnSc4	sůl
a	a	k8xC	a
plyn	plyn	k1gInSc4	plyn
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
jimi	on	k3xPp3gMnPc7	on
stala	stát	k5eAaPmAgFnS	stát
ložiska	ložisko	k1gNnSc2	ložisko
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
hrají	hrát	k5eAaImIp3nP	hrát
nejvýznamnější	významný	k2eAgFnSc4d3	nejvýznamnější
roli	role	k1gFnSc4	role
v	v	k7c6	v
ekonomice	ekonomika	k1gFnSc6	ekonomika
města	město	k1gNnSc2	město
obchod	obchod	k1gInSc1	obchod
<g/>
,	,	kIx,	,
podniky	podnik	k1gInPc1	podnik
veřejné	veřejný	k2eAgFnSc2d1	veřejná
služby	služba	k1gFnSc2	služba
<g/>
,	,	kIx,	,
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
lékařství	lékařství	k1gNnSc1	lékařství
a	a	k8xC	a
vzdělávání	vzdělávání	k1gNnSc1	vzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
trvalé	trvalý	k2eAgNnSc1d1	trvalé
sídlo	sídlo	k1gNnSc1	sídlo
<g/>
,	,	kIx,	,
Ft	Ft	k1gFnSc1	Ft
<g/>
.	.	kIx.	.
</s>
<s>
Lee	Lea	k1gFnSc3	Lea
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1788	[number]	k4	1788
<g/>
.	.	kIx.	.
</s>
<s>
Charleston	charleston	k1gInSc1	charleston
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
baseballového	baseballový	k2eAgInSc2d1	baseballový
týmu	tým	k1gInSc2	tým
nižší	nízký	k2eAgFnSc2d2	nižší
ligy	liga	k1gFnSc2	liga
West	Westa	k1gFnPc2	Westa
Virginia	Virginium	k1gNnSc2	Virginium
Power	Powra	k1gFnPc2	Powra
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Charleston	charleston	k1gInSc1	charleston
Alley	Allea	k1gFnSc2	Allea
Cats	Catsa	k1gFnPc2	Catsa
<g/>
)	)	kIx)	)
a	a	k8xC	a
každoročně	každoročně	k6eAd1	každoročně
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
koná	konat	k5eAaImIp3nS	konat
patnáctimílový	patnáctimílový	k2eAgMnSc1d1	patnáctimílový
(	(	kIx(	(
<g/>
24	[number]	k4	24
km	km	kA	km
<g/>
)	)	kIx)	)
běh	běh	k1gInSc1	běh
Charleston	charleston	k1gInSc1	charleston
Distance	distance	k1gFnSc2	distance
Run	Runa	k1gFnPc2	Runa
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
také	také	k9	také
nachází	nacházet	k5eAaImIp3nS	nacházet
letiště	letiště	k1gNnSc4	letiště
Yeager	Yeagra	k1gFnPc2	Yeagra
Airport	Airport	k1gInSc1	Airport
a	a	k8xC	a
University	universita	k1gFnPc1	universita
of	of	k?	of
Charleston	charleston	k1gInSc1	charleston
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
též	též	k9	též
domovem	domov	k1gInSc7	domov
letky	letka	k1gFnSc2	letka
jménem	jméno	k1gNnSc7	jméno
130	[number]	k4	130
<g/>
th	th	k?	th
Airlift	Airlift	k2eAgMnSc1d1	Airlift
Wing	Wing	k1gMnSc1	Wing
of	of	k?	of
the	the	k?	the
West	West	k1gMnSc1	West
Virginia	Virginium	k1gNnSc2	Virginium
Air	Air	k1gMnSc1	Air
National	National	k1gMnSc1	National
Guard	Guard	k1gMnSc1	Guard
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
najdeme	najít	k5eAaPmIp1nP	najít
parky	park	k1gInPc1	park
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
Cato	Cato	k1gNnSc4	Cato
Park	park	k1gInSc1	park
či	či	k8xC	či
Coonskin	Coonskin	k2eAgInSc1d1	Coonskin
Park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
a	a	k8xC	a
Kanawha	Kanawha	k1gFnSc1	Kanawha
State	status	k1gInSc5	status
Forest	Forest	k1gFnSc4	Forest
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
rozlehlý	rozlehlý	k2eAgInSc1d1	rozlehlý
státní	státní	k2eAgInSc1d1	státní
park	park	k1gInSc1	park
s	s	k7c7	s
bazénem	bazén	k1gInSc7	bazén
<g/>
,	,	kIx,	,
kempy	kemp	k1gInPc7	kemp
<g/>
,	,	kIx,	,
cyklostezkami	cyklostezka	k1gFnPc7	cyklostezka
a	a	k8xC	a
stezkami	stezka	k1gFnPc7	stezka
pro	pro	k7c4	pro
pěší	pěší	k2eAgNnSc4d1	pěší
<g/>
,	,	kIx,	,
místy	místy	k6eAd1	místy
pro	pro	k7c4	pro
piknik	piknik	k1gInSc4	piknik
a	a	k8xC	a
i	i	k9	i
několik	několik	k4yIc1	několik
chat	chata	k1gFnPc2	chata
pro	pro	k7c4	pro
rekreační	rekreační	k2eAgInPc4d1	rekreační
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Americké	americký	k2eAgFnSc6d1	americká
válce	válka	k1gFnSc6	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
se	se	k3xPyFc4	se
vydali	vydat	k5eAaPmAgMnP	vydat
první	první	k4xOgMnPc1	první
průzkumníci	průzkumník	k1gMnPc1	průzkumník
pryč	pryč	k6eAd1	pryč
od	od	k7c2	od
původních	původní	k2eAgFnPc2d1	původní
osad	osada	k1gFnPc2	osada
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
pomalu	pomalu	k6eAd1	pomalu
stěhovala	stěhovat	k5eAaImAgFnS	stěhovat
do	do	k7c2	do
západních	západní	k2eAgFnPc2d1	západní
částí	část	k1gFnPc2	část
Virginie	Virginie	k1gFnSc1	Virginie
<g/>
.	.	kIx.	.
</s>
<s>
Využíváním	využívání	k1gNnSc7	využívání
jejích	její	k3xOp3gInPc2	její
bohatých	bohatý	k2eAgInPc2d1	bohatý
zdrojů	zdroj	k1gInPc2	zdroj
se	se	k3xPyFc4	se
Charleston	charleston	k1gInSc1	charleston
stal	stát	k5eAaPmAgInS	stát
významnou	významný	k2eAgFnSc7d1	významná
součástí	součást	k1gFnSc7	součást
historie	historie	k1gFnSc2	historie
Virginie	Virginie	k1gFnSc2	Virginie
a	a	k8xC	a
Západní	západní	k2eAgFnSc2d1	západní
Virginie	Virginie	k1gFnSc2	Virginie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
Charleston	charleston	k1gInSc1	charleston
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
státu	stát	k1gInSc2	stát
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
jeho	jeho	k3xOp3gInPc4	jeho
městem	město	k1gNnSc7	město
hlavním	hlavní	k2eAgNnSc7d1	hlavní
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Charlestonu	charleston	k1gInSc2	charleston
začínají	začínat	k5eAaImIp3nP	začínat
v	v	k7c6	v
osmnáctém	osmnáctý	k4xOgNnSc6	osmnáctý
století	století	k1gNnSc6	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1774	[number]	k4	1774
získala	získat	k5eAaPmAgFnS	získat
rodina	rodina	k1gFnSc1	rodina
Bullittů	Bullitt	k1gInPc2	Bullitt
5	[number]	k4	5
km2	km2	k4	km2
půdy	půda	k1gFnSc2	půda
poblíž	poblíž	k7c2	poblíž
ústí	ústí	k1gNnSc2	ústí
řeky	řeka	k1gFnSc2	řeka
Elk	Elk	k1gFnSc2	Elk
<g/>
.	.	kIx.	.
</s>
<s>
Pozemek	pozemek	k1gInSc1	pozemek
byl	být	k5eAaImAgInS	být
o	o	k7c4	o
dvanáct	dvanáct	k4xCc4	dvanáct
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
prodán	prodat	k5eAaPmNgInS	prodat
plk.	plk.	kA	plk.
Georgi	Georg	k1gMnSc3	Georg
Clendeninovi	Clendenin	k1gMnSc3	Clendenin
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
trvalá	trvalý	k2eAgFnSc1d1	trvalá
osada	osada	k1gFnSc1	osada
<g/>
,	,	kIx,	,
Fort	Fort	k?	Fort
Lee	Lea	k1gFnSc6	Lea
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1788	[number]	k4	1788
díky	díky	k7c3	díky
plk.	plk.	kA	plk.
Clendeninovi	Clendeninův	k2eAgMnPc1d1	Clendeninův
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
skupině	skupina	k1gFnSc3	skupina
Virginia	Virginium	k1gNnSc2	Virginium
Rangerů	Ranger	k1gInPc2	Ranger
<g/>
.	.	kIx.	.
</s>
<s>
Osada	osada	k1gFnSc1	osada
zaujímala	zaujímat	k5eAaImAgFnS	zaujímat
plochu	plocha	k1gFnSc4	plocha
nynější	nynější	k2eAgFnSc2d1	nynější
křižovatky	křižovatka	k1gFnSc2	křižovatka
ulic	ulice	k1gFnPc2	ulice
Brooks	Brooks	k1gInSc1	Brooks
Street	Street	k1gMnSc1	Street
a	a	k8xC	a
Kanawha	Kanawha	k1gMnSc1	Kanawha
Boulevard	Boulevard	k1gMnSc1	Boulevard
<g/>
.	.	kIx.	.
</s>
<s>
Historikové	historik	k1gMnPc1	historik
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Charleston	charleston	k1gInSc1	charleston
byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
po	po	k7c6	po
otci	otec	k1gMnSc6	otec
plk.	plk.	kA	plk.
Clendenina	Clendenina	k1gFnSc1	Clendenina
Charlesovi	Charles	k1gMnSc3	Charles
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
města	město	k1gNnSc2	město
Charles	Charles	k1gMnSc1	Charles
Town	Town	k1gMnSc1	Town
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
zkrátil	zkrátit	k5eAaPmAgMnS	zkrátit
na	na	k7c4	na
Charleston	charleston	k1gInSc4	charleston
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
předešlo	předejít	k5eAaPmAgNnS	předejít
záměně	záměna	k1gFnSc3	záměna
s	s	k7c7	s
jiným	jiný	k1gMnSc7	jiný
Charles	Charles	k1gMnSc1	Charles
Townem	Town	k1gMnSc7	Town
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
Západní	západní	k2eAgFnSc3d1	západní
Virginii	Virginie	k1gFnSc3	Virginie
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
Charleston	charleston	k1gInSc1	charleston
úředně	úředně	k6eAd1	úředně
ustanovil	ustanovit	k5eAaPmAgInS	ustanovit
parlament	parlament	k1gInSc1	parlament
virginského	virginský	k2eAgInSc2d1	virginský
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Virginia	Virginium	k1gNnSc2	Virginium
General	General	k1gMnSc1	General
Assembly	Assembly	k1gMnSc1	Assembly
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
40	[number]	k4	40
akrech	akr	k1gInPc6	akr
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
město	město	k1gNnSc1	město
tvořily	tvořit	k5eAaImAgInP	tvořit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1794	[number]	k4	1794
<g/>
,	,	kIx,	,
obývalo	obývat	k5eAaImAgNnS	obývat
sedm	sedm	k4xCc1	sedm
domů	dům	k1gInPc2	dům
35	[number]	k4	35
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Charleston	charleston	k1gInSc1	charleston
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
okresu	okres	k1gInSc2	okres
Kanawha	Kanawha	k1gMnSc1	Kanawha
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
Kanawha	Kanawh	k1gMnSc2	Kanawh
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
výrazu	výraz	k1gInSc2	výraz
indiánského	indiánský	k2eAgInSc2d1	indiánský
kmene	kmen	k1gInSc2	kmen
Arawaků	Arawak	k1gMnPc2	Arawak
pro	pro	k7c4	pro
kánoi	kánoe	k1gFnSc4	kánoe
z	z	k7c2	z
vydlabaného	vydlabaný	k2eAgInSc2d1	vydlabaný
kmene	kmen	k1gInSc2	kmen
<g/>
.	.	kIx.	.
</s>
<s>
Vůbec	vůbec	k9	vůbec
první	první	k4xOgFnSc7	první
budovou	budova	k1gFnSc7	budova
okresu	okres	k1gInSc2	okres
bylo	být	k5eAaImAgNnS	být
dvoupatrové	dvoupatrový	k2eAgNnSc1d1	dvoupatrové
vězení	vězení	k1gNnSc1	vězení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
doslova	doslova	k6eAd1	doslova
vydlabáno	vydlabat	k5eAaPmNgNnS	vydlabat
do	do	k7c2	do
břehu	břeh	k1gInSc2	břeh
řeky	řeka	k1gFnSc2	řeka
Kanawha	Kanawha	k1gFnSc1	Kanawha
<g/>
.	.	kIx.	.
</s>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
Boone	Boon	k1gInSc5	Boon
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
zmocněn	zmocněn	k2eAgMnSc1d1	zmocněn
jako	jako	k8xC	jako
zástupce	zástupce	k1gMnSc1	zástupce
Kanawahské	Kanawahský	k2eAgFnSc2d1	Kanawahský
domobrany	domobrana	k1gFnSc2	domobrana
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1791	[number]	k4	1791
zvolen	zvolit	k5eAaPmNgInS	zvolit
do	do	k7c2	do
Virginského	virginský	k2eAgInSc2d1	virginský
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
historických	historický	k2eAgInPc6d1	historický
dokumentech	dokument	k1gInPc6	dokument
stojí	stát	k5eAaImIp3nS	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
šel	jít	k5eAaImAgMnS	jít
až	až	k6eAd1	až
do	do	k7c2	do
Richmondu	Richmond	k1gInSc2	Richmond
pěšky	pěšky	k6eAd1	pěšky
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
počátkem	počátek	k1gInSc7	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
podél	podél	k7c2	podél
řeky	řeka	k1gFnSc2	řeka
Kanawhy	Kanawha	k1gFnSc2	Kanawha
objevena	objeven	k2eAgNnPc1d1	objeveno
ložiska	ložisko	k1gNnPc1	ložisko
soli	sůl	k1gFnSc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
sůl	sůl	k1gFnSc1	sůl
byla	být	k5eAaImAgFnS	být
dolována	dolovat	k5eAaImNgFnS	dolovat
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1806	[number]	k4	1806
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
oblasti	oblast	k1gFnPc4	oblast
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
prosperitu	prosperita	k1gFnSc4	prosperita
a	a	k8xC	a
rychlý	rychlý	k2eAgInSc4d1	rychlý
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
růst	růst	k1gInSc4	růst
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1808	[number]	k4	1808
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
vyrábělo	vyrábět	k5eAaImAgNnS	vyrábět
1250	[number]	k4	1250
liber	libra	k1gFnPc2	libra
soli	sůl	k1gFnSc2	sůl
denně	denně	k6eAd1	denně
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
kolem	kolem	k7c2	kolem
Charlestonu	charleston	k1gInSc2	charleston
<g/>
,	,	kIx,	,
Kanawhaské	Kanawhaská	k1gFnSc2	Kanawhaská
solné	solný	k2eAgFnSc2d1	solná
pánve	pánev	k1gFnSc2	pánev
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
Malden	Maldna	k1gFnPc2	Maldna
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgInP	mít
stát	stát	k5eAaPmF	stát
největším	veliký	k2eAgMnSc7d3	veliký
producentem	producent	k1gMnSc7	producent
soli	sůl	k1gFnSc2	sůl
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1818	[number]	k4	1818
začala	začít	k5eAaPmAgFnS	začít
fungovat	fungovat	k5eAaImF	fungovat
Kanawha	Kanawha	k1gFnSc1	Kanawha
Salt	salto	k1gNnPc2	salto
Company	Compana	k1gFnSc2	Compana
<g/>
,	,	kIx,	,
první	první	k4xOgInSc4	první
trust	trust	k1gInSc4	trust
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
dolování	dolování	k1gNnSc2	dolování
soli	sůl	k1gFnSc2	sůl
narazil	narazit	k5eAaPmAgMnS	narazit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1815	[number]	k4	1815
kapitán	kapitán	k1gMnSc1	kapitán
James	James	k1gMnSc1	James
Wilson	Wilson	k1gNnSc4	Wilson
na	na	k7c4	na
první	první	k4xOgNnSc4	první
ložisko	ložisko	k1gNnSc4	ložisko
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dnešní	dnešní	k2eAgFnSc2d1	dnešní
křižovatky	křižovatka	k1gFnSc2	křižovatka
ulic	ulice	k1gFnPc2	ulice
Brooks	Brooks	k1gInSc1	Brooks
Street	Street	k1gMnSc1	Street
a	a	k8xC	a
Kanawha	Kanawha	k1gMnSc1	Kanawha
Boulevard	Boulevard	k1gMnSc1	Boulevard
(	(	kIx(	(
<g/>
poblíž	poblíž	k7c2	poblíž
dnešního	dnešní	k2eAgInSc2d1	dnešní
komplexu	komplex	k1gInSc2	komplex
parlamentu	parlament	k1gInSc2	parlament
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1817	[number]	k4	1817
bylo	být	k5eAaImAgNnS	být
objeveno	objeven	k2eAgNnSc1d1	objeveno
uhlí	uhlí	k1gNnSc1	uhlí
a	a	k8xC	a
začalo	začít	k5eAaPmAgNnS	začít
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
v	v	k7c6	v
solných	solný	k2eAgFnPc6d1	solná
továrnách	továrna	k1gFnPc6	továrna
používat	používat	k5eAaImF	používat
jako	jako	k9	jako
palivo	palivo	k1gNnSc4	palivo
<g/>
.	.	kIx.	.
</s>
<s>
Důležitost	důležitost	k1gFnSc1	důležitost
kanawhaského	kanawhaský	k2eAgInSc2d1	kanawhaský
solného	solný	k2eAgInSc2d1	solný
průmyslu	průmysl	k1gInSc2	průmysl
začala	začít	k5eAaPmAgFnS	začít
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
upadat	upadat	k5eAaImF	upadat
<g/>
,	,	kIx,	,
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
nástup	nástup	k1gInSc1	nástup
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
přinesla	přinést	k5eAaPmAgFnS	přinést
další	další	k2eAgFnSc4d1	další
poptávku	poptávka	k1gFnSc4	poptávka
po	po	k7c6	po
chemických	chemický	k2eAgFnPc6d1	chemická
surovinách	surovina	k1gFnPc6	surovina
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
chlór	chlór	k1gInSc1	chlór
a	a	k8xC	a
žíraviny	žíravina	k1gFnPc1	žíravina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
ze	z	k7c2	z
solných	solný	k2eAgInPc2d1	solný
roztoků	roztok	k1gInPc2	roztok
mohly	moct	k5eAaImAgInP	moct
vyrobit	vyrobit	k5eAaPmF	vyrobit
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
dále	daleko	k6eAd2	daleko
rostlo	růst	k5eAaImAgNnS	růst
<g/>
,	,	kIx,	,
až	až	k8xS	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1861	[number]	k4	1861
přišla	přijít	k5eAaPmAgFnS	přijít
Americká	americký	k2eAgFnSc1d1	americká
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Stát	stát	k1gInSc1	stát
Virginie	Virginie	k1gFnSc2	Virginie
se	se	k3xPyFc4	se
odtrhl	odtrhnout	k5eAaPmAgInS	odtrhnout
od	od	k7c2	od
tzv.	tzv.	kA	tzv.
Unie	unie	k1gFnSc2	unie
a	a	k8xC	a
Charleston	charleston	k1gInSc1	charleston
se	se	k3xPyFc4	se
rozdělil	rozdělit	k5eAaPmAgInS	rozdělit
na	na	k7c6	na
věrné	věrný	k2eAgFnSc6d1	věrná
Unii	unie	k1gFnSc6	unie
a	a	k8xC	a
Konfederaci	konfederace	k1gFnSc6	konfederace
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1862	[number]	k4	1862
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
Bitva	bitva	k1gFnSc1	bitva
o	o	k7c4	o
Charleston	charleston	k1gInSc4	charleston
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
vojska	vojsko	k1gNnSc2	vojsko
Konfederace	konfederace	k1gFnSc2	konfederace
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
<g/>
,	,	kIx,	,
město	město	k1gNnSc4	město
udržela	udržet	k5eAaPmAgFnS	udržet
jen	jen	k9	jen
krátce	krátce	k6eAd1	krátce
<g/>
.	.	kIx.	.
</s>
<s>
Vojáci	voják	k1gMnPc1	voják
Unie	unie	k1gFnSc2	unie
se	se	k3xPyFc4	se
jen	jen	k9	jen
o	o	k7c4	o
šest	šest	k4xCc4	šest
týdnů	týden	k1gInPc2	týden
později	pozdě	k6eAd2	pozdě
vrátili	vrátit	k5eAaPmAgMnP	vrátit
a	a	k8xC	a
zůstali	zůstat	k5eAaPmAgMnP	zůstat
až	až	k9	až
do	do	k7c2	do
samého	samý	k3xTgInSc2	samý
konce	konec	k1gInSc2	konec
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Sever	sever	k1gInSc4	sever
si	se	k3xPyFc3	se
Charleston	charleston	k1gInSc4	charleston
držel	držet	k5eAaImAgInS	držet
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
západní	západní	k2eAgFnSc2d1	západní
části	část	k1gFnSc2	část
Virginie	Virginie	k1gFnSc1	Virginie
způsobila	způsobit	k5eAaPmAgFnS	způsobit
ještě	ještě	k6eAd1	ještě
větší	veliký	k2eAgInSc4d2	veliký
problém	problém	k1gInSc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Virginie	Virginie	k1gFnSc1	Virginie
už	už	k6eAd1	už
od	od	k7c2	od
Unie	unie	k1gFnSc2	unie
byla	být	k5eAaImAgFnS	být
odtržená	odtržený	k2eAgFnSc1d1	odtržená
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
její	její	k3xOp3gFnSc1	její
severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
měla	mít	k5eAaImAgFnS	mít
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
Unie	unie	k1gFnSc1	unie
a	a	k8xC	a
vyvstala	vyvstat	k5eAaPmAgFnS	vyvstat
tak	tak	k9	tak
otázka	otázka	k1gFnSc1	otázka
státní	státní	k2eAgFnSc2d1	státní
suverenity	suverenita	k1gFnSc2	suverenita
<g/>
.	.	kIx.	.
</s>
<s>
Uprostřed	uprostřed	k7c2	uprostřed
bouřlivé	bouřlivý	k2eAgFnSc2d1	bouřlivá
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
Západní	západní	k2eAgFnSc1d1	západní
Virginie	Virginie	k1gFnSc1	Virginie
díky	díky	k7c3	díky
prohlášení	prohlášení	k1gNnSc3	prohlášení
prezidenta	prezident	k1gMnSc2	prezident
stala	stát	k5eAaPmAgFnS	stát
úředně	úředně	k6eAd1	úředně
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Abraham	Abraham	k1gMnSc1	Abraham
Lincoln	Lincoln	k1gMnSc1	Lincoln
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
severozápadní	severozápadní	k2eAgFnSc1d1	severozápadní
oblast	oblast	k1gFnSc1	oblast
Virginie	Virginie	k1gFnSc1	Virginie
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
zpět	zpět	k6eAd1	zpět
pod	pod	k7c4	pod
Unii	unie	k1gFnSc4	unie
<g/>
,	,	kIx,	,
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1863	[number]	k4	1863
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
Západní	západní	k2eAgFnSc1d1	západní
Virginie	Virginie	k1gFnSc1	Virginie
stala	stát	k5eAaPmAgFnS	stát
35	[number]	k4	35
<g/>
.	.	kIx.	.
státem	stát	k1gInSc7	stát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
často	často	k6eAd1	často
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Západní	západní	k2eAgFnPc1d1	západní
Virginie	Virginie	k1gFnPc1	Virginie
se	se	k3xPyFc4	se
od	od	k7c2	od
Virginie	Virginie	k1gFnSc2	Virginie
odtrhla	odtrhnout	k5eAaPmAgFnS	odtrhnout
kvůli	kvůli	k7c3	kvůli
rozdílným	rozdílný	k2eAgInPc3d1	rozdílný
postojům	postoj	k1gInPc3	postoj
k	k	k7c3	k
otroctví	otroctví	k1gNnSc3	otroctví
<g/>
,	,	kIx,	,
skutečné	skutečný	k2eAgInPc1d1	skutečný
důvody	důvod	k1gInPc1	důvod
byly	být	k5eAaImAgInP	být
ekonomické	ekonomický	k2eAgInPc1d1	ekonomický
<g/>
.	.	kIx.	.
</s>
<s>
Těžký	těžký	k2eAgInSc1d1	těžký
průmysl	průmysl	k1gInSc1	průmysl
Severu	sever	k1gInSc2	sever
<g/>
,	,	kIx,	,
a	a	k8xC	a
především	především	k9	především
ocelářství	ocelářství	k1gNnSc4	ocelářství
horní	horní	k2eAgFnSc2d1	horní
oblasti	oblast	k1gFnSc2	oblast
řeky	řeka	k1gFnSc2	řeka
Ohio	Ohio	k1gNnSc1	Ohio
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
závislé	závislý	k2eAgInPc1d1	závislý
na	na	k7c6	na
uhlí	uhlí	k1gNnSc6	uhlí
dostupném	dostupný	k2eAgNnSc6d1	dostupné
v	v	k7c6	v
dolech	dol	k1gInPc6	dol
západní	západní	k2eAgFnSc2d1	západní
Virginie	Virginie	k1gFnSc2	Virginie
<g/>
.	.	kIx.	.
</s>
<s>
Jednotky	jednotka	k1gFnPc1	jednotka
federalizované	federalizovaný	k2eAgFnSc2d1	federalizovaná
armády	armáda	k1gFnSc2	armáda
byly	být	k5eAaImAgFnP	být
hned	hned	k6eAd1	hned
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
války	válka	k1gFnSc2	válka
poslány	poslat	k5eAaPmNgInP	poslat
z	z	k7c2	z
Ohia	Ohio	k1gNnSc2	Ohio
na	na	k7c4	na
západ	západ	k1gInSc4	západ
Virginie	Virginie	k1gFnSc2	Virginie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
tak	tak	k6eAd1	tak
zajistily	zajistit	k5eAaPmAgFnP	zajistit
bezpečný	bezpečný	k2eAgInSc4d1	bezpečný
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
uhelným	uhelný	k2eAgInPc3d1	uhelný
dolům	dol	k1gInPc3	dol
a	a	k8xC	a
dopravě	doprava	k1gFnSc6	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
už	už	k6eAd1	už
nyní	nyní	k6eAd1	nyní
existoval	existovat	k5eAaImAgInS	existovat
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
osidlování	osidlování	k1gNnSc1	osidlování
oblasti	oblast	k1gFnSc2	oblast
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
státu	stát	k1gInSc2	stát
bylo	být	k5eAaImAgNnS	být
komplikované	komplikovaný	k2eAgNnSc1d1	komplikované
<g/>
:	:	kIx,	:
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Západní	západní	k2eAgFnSc2d1	západní
Virginie	Virginie	k1gFnSc2	Virginie
se	se	k3xPyFc4	se
několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
stěhovalo	stěhovat	k5eAaImAgNnS	stěhovat
mezi	mezi	k7c7	mezi
Wheelingem	Wheeling	k1gInSc7	Wheeling
a	a	k8xC	a
Charlestonem	charleston	k1gInSc7	charleston
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1877	[number]	k4	1877
ale	ale	k8xC	ale
občané	občan	k1gMnPc1	občan
státu	stát	k1gInSc2	stát
jako	jako	k8xC	jako
konečné	konečný	k2eAgNnSc4d1	konečné
umístění	umístění	k1gNnSc4	umístění
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
zvolili	zvolit	k5eAaPmAgMnP	zvolit
Charleston	charleston	k1gInSc4	charleston
a	a	k8xC	a
o	o	k7c4	o
osm	osm	k4xCc4	osm
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
první	první	k4xOgFnSc1	první
budova	budova	k1gFnSc1	budova
kapitolu	kapitola	k1gFnSc4	kapitola
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
požáru	požár	k1gInSc6	požár
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
byla	být	k5eAaImAgFnS	být
rychle	rychle	k6eAd1	rychle
postavena	postaven	k2eAgFnSc1d1	postavena
budova	budova	k1gFnSc1	budova
nová	nový	k2eAgFnSc1d1	nová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1927	[number]	k4	1927
také	také	k9	také
vyhořela	vyhořet	k5eAaPmAgFnS	vyhořet
<g/>
.	.	kIx.	.
</s>
<s>
Výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
výstavbu	výstavba	k1gFnSc4	výstavba
kapitolu	kapitola	k1gFnSc4	kapitola
<g/>
,	,	kIx,	,
ustanovený	ustanovený	k2eAgInSc4d1	ustanovený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
legislativou	legislativa	k1gFnSc7	legislativa
<g/>
,	,	kIx,	,
však	však	k9	však
povolil	povolit	k5eAaPmAgMnS	povolit
výstavbu	výstavba	k1gFnSc4	výstavba
současného	současný	k2eAgNnSc2d1	současné
kapitolu	kapitola	k1gFnSc4	kapitola
<g/>
.	.	kIx.	.
</s>
<s>
Architekt	architekt	k1gMnSc1	architekt
Cass	Cassa	k1gFnPc2	Cassa
Gilbert	Gilbert	k1gMnSc1	Gilbert
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
žlutohnědou	žlutohnědý	k2eAgFnSc4d1	žlutohnědá
stavbu	stavba	k1gFnSc4	stavba
z	z	k7c2	z
vápence	vápenec	k1gInSc2	vápenec
v	v	k7c6	v
italském	italský	k2eAgInSc6d1	italský
renesančním	renesanční	k2eAgInSc6d1	renesanční
stylu	styl	k1gInSc6	styl
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
celkem	celkem	k6eAd1	celkem
stát	stát	k1gInSc1	stát
téměř	téměř	k6eAd1	téměř
10	[number]	k4	10
miliónů	milión	k4xCgInPc2	milión
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k8xS	co
byly	být	k5eAaImAgInP	být
hotové	hotový	k2eAgFnPc4d1	hotová
první	první	k4xOgFnPc4	první
tři	tři	k4xCgFnPc4	tři
etapy	etapa	k1gFnPc4	etapa
stavby	stavba	k1gFnSc2	stavba
<g/>
,	,	kIx,	,
guvernér	guvernér	k1gMnSc1	guvernér
William	William	k1gInSc1	William
G.	G.	kA	G.
Conley	Conlea	k1gFnSc2	Conlea
kapitol	kapitola	k1gFnPc2	kapitola
slavnostně	slavnostně	k6eAd1	slavnostně
otevřel	otevřít	k5eAaPmAgInS	otevřít
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1932	[number]	k4	1932
<g/>
.	.	kIx.	.
</s>
<s>
Charleston	charleston	k1gInSc1	charleston
byl	být	k5eAaImAgInS	být
nyní	nyní	k6eAd1	nyní
centrem	centrum	k1gNnSc7	centrum
vlády	vláda	k1gFnSc2	vláda
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Přírodní	přírodní	k2eAgInPc1d1	přírodní
zdroje	zdroj	k1gInPc1	zdroj
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
uhlí	uhlí	k1gNnSc4	uhlí
a	a	k8xC	a
plyn	plyn	k1gInSc4	plyn
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
rozšiřováním	rozšiřování	k1gNnSc7	rozšiřování
železnice	železnice	k1gFnSc2	železnice
k	k	k7c3	k
růstu	růst	k1gInSc3	růst
také	také	k6eAd1	také
přispěly	přispět	k5eAaPmAgFnP	přispět
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
státu	stát	k1gInSc2	stát
přišla	přijít	k5eAaPmAgNnP	přijít
nová	nový	k2eAgNnPc1d1	nové
odvětví	odvětví	k1gNnPc1	odvětví
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
,	,	kIx,	,
např.	např.	kA	např.
chemický	chemický	k2eAgInSc4d1	chemický
<g/>
,	,	kIx,	,
sklářský	sklářský	k2eAgInSc4d1	sklářský
<g/>
,	,	kIx,	,
dřevařský	dřevařský	k2eAgInSc4d1	dřevařský
a	a	k8xC	a
ocelářský	ocelářský	k2eAgInSc4d1	ocelářský
<g/>
,	,	kIx,	,
přitáhly	přitáhnout	k5eAaPmAgInP	přitáhnout
právě	právě	k6eAd1	právě
přírodní	přírodní	k2eAgInPc4d1	přírodní
zdroje	zdroj	k1gInPc4	zdroj
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Charlestonu	charleston	k1gInSc6	charleston
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
stavělo	stavět	k5eAaImAgNnS	stavět
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc4d1	mnohý
z	z	k7c2	z
těch	ten	k3xDgInPc2	ten
domů	dům	k1gInPc2	dům
včetně	včetně	k7c2	včetně
kostelů	kostel	k1gInPc2	kostel
a	a	k8xC	a
kancelářských	kancelářský	k2eAgFnPc2d1	kancelářská
budov	budova	k1gFnPc2	budova
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
samém	samý	k3xTgNnSc6	samý
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
kolem	kolem	k7c2	kolem
ulice	ulice	k1gFnSc2	ulice
Capitol	Capitola	k1gFnPc2	Capitola
Street	Street	k1gMnSc1	Street
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
nedaleko	nedaleko	k7c2	nedaleko
Charlestonu	charleston	k1gInSc2	charleston
otevřela	otevřít	k5eAaPmAgFnS	otevřít
první	první	k4xOgFnSc1	první
a	a	k8xC	a
největší	veliký	k2eAgFnSc1d3	veliký
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
syntetického	syntetický	k2eAgInSc2d1	syntetický
kaučuku	kaučuk	k1gInSc2	kaučuk
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
poskytovala	poskytovat	k5eAaImAgFnS	poskytovat
pro	pro	k7c4	pro
válku	válka	k1gFnSc4	válka
velmi	velmi	k6eAd1	velmi
důležité	důležitý	k2eAgInPc1d1	důležitý
výrobky	výrobek	k1gInPc1	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
válka	válka	k1gFnSc1	válka
skončila	skončit	k5eAaPmAgFnS	skončit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Charleston	charleston	k1gInSc1	charleston
ocitl	ocitnout	k5eAaPmAgInS	ocitnout
na	na	k7c6	na
kraji	kraj	k1gInSc6	kraj
důležitých	důležitý	k2eAgFnPc2d1	důležitá
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
bylo	být	k5eAaImAgNnS	být
letiště	letiště	k1gNnSc4	letiště
Kanawha	Kanawha	k1gFnSc1	Kanawha
Airport	Airport	k1gInSc1	Airport
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
Yeager	Yeager	k1gInSc1	Yeager
Airport	Airport	k1gInSc1	Airport
<g/>
,	,	kIx,	,
pojmenované	pojmenovaný	k2eAgFnSc2d1	pojmenovaná
po	po	k7c6	po
generálovi	generál	k1gMnSc3	generál
Chucku	Chuck	k1gInSc2	Chuck
Yeagerovi	Yeager	k1gMnSc3	Yeager
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
asi	asi	k9	asi
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejpozoruhodnějších	pozoruhodný	k2eAgFnPc2d3	nejpozoruhodnější
inženýrských	inženýrský	k2eAgFnPc2d1	inženýrská
úspěchů	úspěch	k1gInPc2	úspěch
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Postaveno	postaven	k2eAgNnSc4d1	postaveno
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
výstavba	výstavba	k1gFnSc1	výstavba
zahrnovala	zahrnovat	k5eAaImAgFnS	zahrnovat
vykácení	vykácení	k1gNnSc4	vykácení
1.5	[number]	k4	1.5
km2	km2	k4	km2
na	na	k7c6	na
třech	tři	k4xCgInPc6	tři
vrcholcích	vrcholek	k1gInPc6	vrcholek
a	a	k8xC	a
přestěhování	přestěhování	k1gNnSc2	přestěhování
více	hodně	k6eAd2	hodně
než	než	k8xS	než
devíti	devět	k4xCc2	devět
miliónů	milión	k4xCgInPc2	milión
krychlových	krychlový	k2eAgMnPc2d1	krychlový
yardů	yard	k1gInPc2	yard
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
Charleston	charleston	k1gInSc1	charleston
Civic	Civice	k1gFnPc2	Civice
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
stojí	stát	k5eAaImIp3nS	stát
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
zcela	zcela	k6eAd1	zcela
zrekonstruované	zrekonstruovaný	k2eAgNnSc1d1	zrekonstruované
<g/>
,	,	kIx,	,
a	a	k8xC	a
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
nejrozlehlejší	rozlehlý	k2eAgFnPc4d3	nejrozlehlejší
prostory	prostora	k1gFnPc4	prostora
pro	pro	k7c4	pro
schůze	schůze	k1gFnPc4	schůze
a	a	k8xC	a
výstavy	výstava	k1gFnPc4	výstava
v	v	k7c6	v
Západní	západní	k2eAgFnSc6d1	západní
Virginii	Virginie	k1gFnSc6	Virginie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
prezident	prezident	k1gMnSc1	prezident
Dwight	Dwight	k1gMnSc1	Dwight
D.	D.	kA	D.
Eisenhower	Eisenhower	k1gMnSc1	Eisenhower
podepsal	podepsat	k5eAaPmAgMnS	podepsat
Federal	Federal	k1gFnSc7	Federal
Aid	Aida	k1gFnPc2	Aida
Highway	Highwaa	k1gMnSc2	Highwaa
Act	Act	k1gMnSc2	Act
<g/>
,	,	kIx,	,
zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
budování	budování	k1gNnSc6	budování
systému	systém	k1gInSc2	systém
dálnic	dálnice	k1gFnPc2	dálnice
<g/>
.	.	kIx.	.
</s>
<s>
Charleston	charleston	k1gInSc1	charleston
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
součástí	součást	k1gFnSc7	součást
tohoto	tento	k3xDgInSc2	tento
systému	systém	k1gInSc2	systém
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
když	když	k8xS	když
byly	být	k5eAaImAgInP	být
vystavěny	vystavěn	k2eAgInPc1d1	vystavěn
tři	tři	k4xCgInPc1	tři
velké	velký	k2eAgInPc1d1	velký
mezistátní	mezistátní	k2eAgInPc1d1	mezistátní
dálniční	dálniční	k2eAgInPc1d1	dálniční
systémy	systém	k1gInPc1	systém
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
sbíhaly	sbíhat	k5eAaImAgInP	sbíhat
v	v	k7c6	v
Charlestonu	charleston	k1gInSc6	charleston
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
díky	díky	k7c3	díky
nim	on	k3xPp3gMnPc3	on
je	být	k5eAaImIp3nS	být
Charleston	charleston	k1gInSc1	charleston
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
den	den	k1gInSc4	den
cesty	cesta	k1gFnSc2	cesta
od	od	k7c2	od
60	[number]	k4	60
procent	procento	k1gNnPc2	procento
obyvatel	obyvatel	k1gMnPc2	obyvatel
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
nákupní	nákupní	k2eAgNnSc1d1	nákupní
centrum	centrum	k1gNnSc1	centrum
Charleston	charleston	k1gInSc1	charleston
Town	Towna	k1gFnPc2	Towna
Center	centrum	k1gNnPc2	centrum
Mall	Mall	k1gMnSc1	Mall
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
největším	veliký	k2eAgInSc7d3	veliký
městským	městský	k2eAgInSc7d1	městský
mallem	mall	k1gInSc7	mall
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Mississippi	Mississippi	k1gFnSc2	Mississippi
a	a	k8xC	a
pyšnile	pyšnile	k6eAd1	pyšnile
se	s	k7c7	s
třemi	tři	k4xCgNnPc7	tři
patry	patro	k1gNnPc7	patro
obchodů	obchod	k1gInPc2	obchod
a	a	k8xC	a
restaurací	restaurace	k1gFnPc2	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
začala	začít	k5eAaPmAgFnS	začít
i	i	k9	i
skutečná	skutečný	k2eAgFnSc1d1	skutečná
revitalizace	revitalizace	k1gFnSc1	revitalizace
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
zřízeny	zřízen	k2eAgInPc1d1	zřízen
fondy	fond	k1gInPc1	fond
pro	pro	k7c4	pro
vylepšení	vylepšení	k1gNnSc4	vylepšení
vzhledu	vzhled	k1gInSc2	vzhled
ulic	ulice	k1gFnPc2	ulice
a	a	k8xC	a
začalo	začít	k5eAaPmAgNnS	začít
se	se	k3xPyFc4	se
otevírat	otevírat	k5eAaImF	otevírat
mnoho	mnoho	k6eAd1	mnoho
malých	malý	k2eAgFnPc2d1	malá
firem	firma	k1gFnPc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
jsou	být	k5eAaImIp3nP	být
ulice	ulice	k1gFnPc1	ulice
Capitol	Capitola	k1gFnPc2	Capitola
Street	Streeta	k1gFnPc2	Streeta
<g/>
,	,	kIx,	,
Hale	hala	k1gFnSc3	hala
Street	Streeta	k1gFnPc2	Streeta
a	a	k8xC	a
další	další	k2eAgFnSc2d1	další
sousedící	sousedící	k2eAgFnSc2d1	sousedící
ulice	ulice	k1gFnSc2	ulice
vybranou	vybraný	k2eAgFnSc7d1	vybraná
směsí	směs	k1gFnSc7	směs
restaurancí	restaurance	k1gFnPc2	restaurance
<g/>
,	,	kIx,	,
obchodů	obchod	k1gInPc2	obchod
a	a	k8xC	a
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
lidí	člověk	k1gMnPc2	člověk
staly	stát	k5eAaPmAgInP	stát
opravdovým	opravdový	k2eAgInSc7d1	opravdový
středem	střed	k1gInSc7	střed
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Robert	Robert	k1gMnSc1	Robert
C.	C.	kA	C.
Byrd	Byrd	k1gMnSc1	Byrd
Federal	Federal	k1gMnSc1	Federal
Building	Building	k1gInSc1	Building
<g/>
,	,	kIx,	,
Haddad	Haddad	k1gInSc1	Haddad
Riverfront	Riverfronta	k1gFnPc2	Riverfronta
Park	park	k1gInSc1	park
a	a	k8xC	a
Capitol	Capitol	k1gInSc1	Capitol
Market	market	k1gInSc1	market
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
několika	několik	k4yIc2	několik
z	z	k7c2	z
nově	nově	k6eAd1	nově
vystavěných	vystavěný	k2eAgFnPc2d1	vystavěná
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
napomohly	napomoct	k5eAaPmAgFnP	napomoct
rozvoji	rozvoj	k1gInSc3	rozvoj
středu	středa	k1gFnSc4	středa
města	město	k1gNnSc2	město
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Charleston	charleston	k1gInSc1	charleston
se	se	k3xPyFc4	se
také	také	k9	také
stal	stát	k5eAaPmAgInS	stát
známým	známý	k2eAgMnSc7d1	známý
jako	jako	k8xC	jako
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgNnPc2d3	nejlepší
zdravotních	zdravotní	k2eAgNnPc2d1	zdravotní
středisek	středisko	k1gNnPc2	středisko
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Plány	plán	k1gInPc1	plán
na	na	k7c4	na
další	další	k2eAgNnPc4d1	další
místa	místo	k1gNnPc4	místo
pro	pro	k7c4	pro
zábavu	zábava	k1gFnSc4	zábava
a	a	k8xC	a
obchod	obchod	k1gInSc4	obchod
udržují	udržovat	k5eAaImIp3nP	udržovat
Charleston	charleston	k1gInSc4	charleston
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
aktivní	aktivní	k2eAgMnSc1d1	aktivní
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
2003	[number]	k4	2003
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
významným	významný	k2eAgNnSc7d1	významné
díky	dík	k1gInPc7	dík
otevření	otevření	k1gNnSc2	otevření
Clay	Claa	k1gMnSc2	Claa
Center	centrum	k1gNnPc2	centrum
for	forum	k1gNnPc2	forum
the	the	k?	the
Arts	Arts	k1gInSc1	Arts
&	&	k?	&
Sciences	Sciences	k1gInSc1	Sciences
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
Maier	Maier	k1gInSc4	Maier
Foundation	Foundation	k1gInSc1	Foundation
Performance	performance	k1gFnSc1	performance
Hall	Hall	k1gMnSc1	Hall
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Walker	Walker	k1gMnSc1	Walker
Theatre	Theatr	k1gMnSc5	Theatr
<g/>
,	,	kIx,	,
The	The	k1gMnSc5	The
Avampato	Avampat	k2eAgNnSc1d1	Avampat
Discovery	Discovera	k1gFnPc4	Discovera
Museum	museum	k1gNnSc1	museum
a	a	k8xC	a
také	také	k9	také
umělecké	umělecký	k2eAgNnSc1d1	umělecké
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Centru	centrum	k1gNnSc6	centrum
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
ElectricSky	ElectricSka	k1gFnPc1	ElectricSka
Theater	Theater	k1gInSc1	Theater
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
kombinací	kombinace	k1gFnSc7	kombinace
planetária	planetárium	k1gNnSc2	planetárium
a	a	k8xC	a
kina	kino	k1gNnSc2	kino
s	s	k7c7	s
175	[number]	k4	175
sedadly	sedadlo	k1gNnPc7	sedadlo
<g/>
.	.	kIx.	.
</s>
<s>
Filmy	film	k1gInPc4	film
promítané	promítaný	k2eAgInPc4d1	promítaný
v	v	k7c6	v
kině	kino	k1gNnSc6	kino
mívají	mívat	k5eAaImIp3nP	mívat
vzdělávací	vzdělávací	k2eAgInPc4d1	vzdělávací
velkoformátové	velkoformátový	k2eAgInPc4d1	velkoformátový
(	(	kIx(	(
<g/>
70	[number]	k4	70
mm	mm	kA	mm
<g/>
)	)	kIx)	)
prezentace	prezentace	k1gFnSc1	prezentace
<g/>
,	,	kIx,	,
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
v	v	k7c6	v
podobných	podobný	k2eAgNnPc6d1	podobné
omnimaxových	omnimaxový	k2eAgNnPc6d1	omnimaxový
kinech	kino	k1gNnPc6	kino
<g/>
.	.	kIx.	.
</s>
<s>
Představení	představení	k1gNnSc1	představení
v	v	k7c6	v
planetáriu	planetárium	k1gNnSc6	planetárium
se	se	k3xPyFc4	se
inscenují	inscenovat	k5eAaBmIp3nP	inscenovat
jako	jako	k9	jako
kombinace	kombinace	k1gFnPc1	kombinace
předtočených	předtočený	k2eAgInPc2d1	předtočený
a	a	k8xC	a
živých	živý	k2eAgInPc2d1	živý
programů	program	k1gInPc2	program
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
charlestonském	charlestonský	k2eAgInSc6d1	charlestonský
kalendáři	kalendář	k1gInSc6	kalendář
lze	lze	k6eAd1	lze
také	také	k9	také
nalézt	nalézt	k5eAaPmF	nalézt
mnoho	mnoho	k4c4	mnoho
festivalů	festival	k1gInPc2	festival
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Multifestu	Multifest	k1gInSc2	Multifest
<g/>
,	,	kIx,	,
Vandalia	Vandalia	k1gFnSc1	Vandalia
Festivalu	festival	k1gInSc2	festival
<g/>
,	,	kIx,	,
oslav	oslava	k1gFnPc2	oslava
Dne	den	k1gInSc2	den
nezávislosti	nezávislost	k1gFnSc2	nezávislost
s	s	k7c7	s
ohňostrojem	ohňostroj	k1gInSc7	ohňostroj
v	v	k7c6	v
Parku	park	k1gInSc6	park
Haddad	Haddad	k1gInSc1	Haddad
Riverfront	Riverfront	k1gInSc1	Riverfront
<g/>
,	,	kIx,	,
a	a	k8xC	a
oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
regatou	regata	k1gFnSc7	regata
Sternwheel	Sternwheela	k1gFnPc2	Sternwheela
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Charleston	charleston	k1gInSc1	charleston
má	mít	k5eAaImIp3nS	mít
jednu	jeden	k4xCgFnSc4	jeden
centrální	centrální	k2eAgFnSc4d1	centrální
agenturu	agentura	k1gFnSc4	agentura
pro	pro	k7c4	pro
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
rozvoj	rozvoj	k1gInSc4	rozvoj
<g/>
,	,	kIx,	,
Charleston	charleston	k1gInSc4	charleston
Area	Ares	k1gMnSc2	Ares
Alliance	Allianec	k1gMnSc2	Allianec
<g/>
.	.	kIx.	.
</s>
<s>
Alliance	Alliance	k1gFnSc1	Alliance
stále	stále	k6eAd1	stále
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
místními	místní	k2eAgFnPc7d1	místní
vůdčími	vůdčí	k2eAgFnPc7d1	vůdčí
osobnostmi	osobnost	k1gFnPc7	osobnost
a	a	k8xC	a
obchodní	obchodní	k2eAgFnSc7d1	obchodní
komunitou	komunita	k1gFnSc7	komunita
při	při	k7c6	při
úspěšném	úspěšný	k2eAgNnSc6d1	úspěšné
budování	budování	k1gNnSc6	budování
ekonomiky	ekonomika	k1gFnSc2	ekonomika
oblasti	oblast	k1gFnSc2	oblast
a	a	k8xC	a
revitalizace	revitalizace	k1gFnSc2	revitalizace
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Charleston	charleston	k1gInSc1	charleston
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
hisorickou	hisorický	k2eAgFnSc4d1	hisorický
část	část	k1gFnSc4	část
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
East	East	k1gMnSc1	East
End	End	k1gMnSc1	End
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Charlestonu	charleston	k1gInSc6	charleston
se	se	k3xPyFc4	se
střídají	střídat	k5eAaImIp3nP	střídat
všechna	všechen	k3xTgNnPc4	všechen
čtyři	čtyři	k4xCgNnPc4	čtyři
roční	roční	k2eAgNnPc4d1	roční
období	období	k1gNnPc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
okraji	okraj	k1gInSc6	okraj
vlhkého	vlhký	k2eAgInSc2d1	vlhký
subtropického	subtropický	k2eAgInSc2d1	subtropický
pásu	pás	k1gInSc2	pás
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
základem	základ	k1gInSc7	základ
jsou	být	k5eAaImIp3nP	být
především	především	k9	především
rozdíly	rozdíl	k1gInPc1	rozdíl
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
<g/>
.	.	kIx.	.
</s>
<s>
Průměrné	průměrný	k2eAgFnPc1d1	průměrná
teploty	teplota	k1gFnPc1	teplota
v	v	k7c6	v
Charlestonu	charleston	k1gInSc6	charleston
jsou	být	k5eAaImIp3nP	být
povětšinou	povětšinou	k6eAd1	povětšinou
teplejší	teplý	k2eAgFnPc1d2	teplejší
než	než	k8xS	než
ve	v	k7c6	v
zbytku	zbytek	k1gInSc6	zbytek
Západní	západní	k2eAgFnSc2d1	západní
Virginie	Virginie	k1gFnSc2	Virginie
(	(	kIx(	(
<g/>
vyjma	vyjma	k7c2	vyjma
Huntingtonu	Huntington	k1gInSc2	Huntington
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
poloze	poloha	k1gFnSc3	poloha
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
vyšších	vysoký	k2eAgFnPc2d2	vyšší
nadmořských	nadmořský	k2eAgFnPc2d1	nadmořská
výšek	výška	k1gFnPc2	výška
<g/>
.	.	kIx.	.
</s>
<s>
Jaro	jaro	k1gNnSc1	jaro
je	být	k5eAaImIp3nS	být
nejméně	málo	k6eAd3	málo
předvídatelné	předvídatelný	k2eAgNnSc4d1	předvídatelné
roční	roční	k2eAgNnSc4d1	roční
období	období	k1gNnSc4	období
a	a	k8xC	a
doopravdy	doopravdy	k6eAd1	doopravdy
většinou	většinou	k6eAd1	většinou
přichází	přicházet	k5eAaImIp3nS	přicházet
na	na	k7c6	na
konci	konec	k1gInSc6	konec
března	březen	k1gInSc2	březen
či	či	k8xC	či
začátkem	začátkem	k7c2	začátkem
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
března	březen	k1gInSc2	březen
až	až	k6eAd1	až
do	do	k7c2	do
prvních	první	k4xOgInPc2	první
květnových	květnový	k2eAgInPc2d1	květnový
dnů	den	k1gInPc2	den
jsou	být	k5eAaImIp3nP	být
teploty	teplota	k1gFnPc4	teplota
značně	značně	k6eAd1	značně
proměnlivé	proměnlivý	k2eAgNnSc1d1	proměnlivé
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
nijak	nijak	k6eAd1	nijak
neobvyklé	obvyklý	k2eNgNnSc1d1	neobvyklé
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
slunečném	slunečný	k2eAgInSc6d1	slunečný
teplém	teplý	k2eAgInSc6d1	teplý
dni	den	k1gInSc6	den
o	o	k7c4	o
24	[number]	k4	24
°	°	k?	°
<g/>
C	C	kA	C
přijde	přijít	k5eAaPmIp3nS	přijít
chladný	chladný	k2eAgInSc1d1	chladný
deštivý	deštivý	k2eAgInSc1d1	deštivý
den	den	k1gInSc1	den
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
teplota	teplota	k1gFnSc1	teplota
vystoupá	vystoupat	k5eAaPmIp3nS	vystoupat
na	na	k7c4	na
pouhých	pouhý	k2eAgInPc2d1	pouhý
sedm	sedm	k4xCc4	sedm
stupňů	stupeň	k1gInPc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Teploty	teplota	k1gFnPc1	teplota
jdou	jít	k5eAaImIp3nP	jít
během	během	k7c2	během
května	květen	k1gInSc2	květen
znatelně	znatelně	k6eAd1	znatelně
vzhůru	vzhůru	k6eAd1	vzhůru
a	a	k8xC	a
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
i	i	k9	i
dny	den	k1gInPc1	den
až	až	k6eAd1	až
letní	letní	k2eAgInPc1d1	letní
<g/>
.	.	kIx.	.
</s>
<s>
Letní	letní	k2eAgNnSc1d1	letní
počasí	počasí	k1gNnSc1	počasí
bývá	bývat	k5eAaImIp3nS	bývat
horké	horký	k2eAgNnSc1d1	horké
a	a	k8xC	a
vlhké	vlhký	k2eAgInPc1d1	vlhký
s	s	k7c7	s
nejvyššími	vysoký	k2eAgFnPc7d3	nejvyšší
denními	denní	k2eAgFnPc7d1	denní
teplotami	teplota	k1gFnPc7	teplota
až	až	k9	až
mezi	mezi	k7c7	mezi
30-35	[number]	k4	30-35
stupni	stupeň	k1gInPc7	stupeň
a	a	k8xC	a
bývá	bývat	k5eAaImIp3nS	bývat
velmi	velmi	k6eAd1	velmi
vlhko	vlhko	k6eAd1	vlhko
<g/>
.	.	kIx.	.
</s>
<s>
Dny	den	k1gInPc1	den
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
teplé	teplý	k2eAgFnPc1d1	teplá
a	a	k8xC	a
suché	suchý	k2eAgFnPc1d1	suchá
až	až	k9	až
do	do	k7c2	do
pozdního	pozdní	k2eAgInSc2d1	pozdní
října	říjen	k1gInSc2	říjen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
noc	noc	k1gFnSc4	noc
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
ochlazovat	ochlazovat	k5eAaImF	ochlazovat
<g/>
.	.	kIx.	.
</s>
<s>
Počasí	počasí	k1gNnSc1	počasí
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
počasí	počasí	k1gNnSc2	počasí
obvyklého	obvyklý	k2eAgNnSc2d1	obvyklé
ve	v	k7c6	v
zbytku	zbytek	k1gInSc6	zbytek
Západní	západní	k2eAgFnSc2d1	západní
Virginie	Virginie	k1gFnSc2	Virginie
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Huntingtonem	Huntington	k1gInSc7	Huntington
bývá	bývat	k5eAaImIp3nS	bývat
Charleston	charleston	k1gInSc1	charleston
posledním	poslední	k2eAgNnSc7d1	poslední
městem	město	k1gNnSc7	město
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
státu	stát	k1gInSc6	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začne	začít	k5eAaPmIp3nS	začít
sněžit	sněžit	k5eAaImF	sněžit
<g/>
.	.	kIx.	.
</s>
<s>
Zimy	zima	k1gFnPc4	zima
má	mít	k5eAaImIp3nS	mít
Charleston	charleston	k1gInSc1	charleston
mírné	mírný	k2eAgInPc1d1	mírný
s	s	k7c7	s
průměrnými	průměrný	k2eAgFnPc7d1	průměrná
teplotami	teplota	k1gFnPc7	teplota
kolem	kolem	k7c2	kolem
5	[number]	k4	5
°	°	k?	°
<g/>
C	C	kA	C
během	během	k7c2	během
všech	všecek	k3xTgInPc2	všecek
zimních	zimní	k2eAgInPc2d1	zimní
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
občasná	občasný	k2eAgNnPc1d1	občasné
ochlazení	ochlazení	k1gNnPc1	ochlazení
srazí	srazit	k5eAaPmIp3nP	srazit
noční	noční	k2eAgFnPc1d1	noční
teploty	teplota	k1gFnPc1	teplota
až	až	k9	až
pod	pod	k7c7	pod
-20	-20	k4	-20
°	°	k?	°
<g/>
C.	C.	kA	C.
Kdykoli	kdykoli	k6eAd1	kdykoli
od	od	k7c2	od
konce	konec	k1gInSc2	konec
listopadu	listopad	k1gInSc2	listopad
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
dubna	duben	k1gInSc2	duben
může	moct	k5eAaImIp3nS	moct
začít	začít	k5eAaPmF	začít
sněžit	sněžit	k5eAaImF	sněžit
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nejvyšší	vysoký	k2eAgFnPc1d3	nejvyšší
srážky	srážka	k1gFnPc1	srážka
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
mezi	mezi	k7c7	mezi
lednem	leden	k1gInSc7	leden
a	a	k8xC	a
únorem	únor	k1gInSc7	únor
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
velké	velký	k2eAgFnSc2d1	velká
sněhové	sněhový	k2eAgFnSc2d1	sněhová
bouře	bouř	k1gFnSc2	bouř
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
vzácností	vzácnost	k1gFnPc2	vzácnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
Charlestonu	charleston	k1gInSc6	charleston
je	být	k5eAaImIp3nS	být
dokonce	dokonce	k9	dokonce
vzácné	vzácný	k2eAgNnSc1d1	vzácné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
najednou	najednou	k6eAd1	najednou
napadlo	napadnout	k5eAaPmAgNnS	napadnout
více	hodně	k6eAd2	hodně
než	než	k8xS	než
30	[number]	k4	30
cm	cm	kA	cm
sněhu	sníh	k1gInSc2	sníh
<g/>
.	.	kIx.	.
</s>
<s>
Charleston	charleston	k1gInSc1	charleston
dostává	dostávat	k5eAaImIp3nS	dostávat
velký	velký	k2eAgInSc1d1	velký
příděl	příděl	k1gInSc1	příděl
deště	dešť	k1gInSc2	dešť
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
je	být	k5eAaImIp3nS	být
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
jara	jaro	k1gNnSc2	jaro
a	a	k8xC	a
během	během	k7c2	během
léta	léto	k1gNnSc2	léto
jsou	být	k5eAaImIp3nP	být
časté	častý	k2eAgFnPc1d1	častá
bouřky	bouřka	k1gFnPc1	bouřka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
občas	občas	k6eAd1	občas
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
velmi	velmi	k6eAd1	velmi
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
<g/>
.	.	kIx.	.
</s>
<s>
Tornáda	tornádo	k1gNnPc1	tornádo
jsou	být	k5eAaImIp3nP	být
vzácností	vzácnost	k1gFnSc7	vzácnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
během	během	k7c2	během
těchto	tento	k3xDgFnPc2	tento
bouří	bouř	k1gFnPc2	bouř
se	se	k3xPyFc4	se
objevit	objevit	k5eAaPmF	objevit
mohou	moct	k5eAaImIp3nP	moct
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
podzimní	podzimní	k2eAgFnSc2d1	podzimní
a	a	k8xC	a
zimní	zimní	k2eAgFnSc2d1	zimní
vláhy	vláha	k1gFnSc2	vláha
má	mít	k5eAaImIp3nS	mít
formu	forma	k1gFnSc4	forma
mrholení	mrholení	k1gNnSc1	mrholení
<g/>
.	.	kIx.	.
</s>
<s>
Nejsušším	suchý	k2eAgNnSc7d3	nejsušší
obdobím	období	k1gNnSc7	období
bývají	bývat	k5eAaImIp3nP	bývat
měsíce	měsíc	k1gInPc1	měsíc
září	září	k1gNnSc2	září
a	a	k8xC	a
říjen	říjen	k1gInSc1	říjen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
Charlestonu	charleston	k1gInSc6	charleston
1050	[number]	k4	1050
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
desetiletích	desetiletí	k1gNnPc6	desetiletí
jejich	jejich	k3xOp3gNnSc1	jejich
počet	počet	k1gInSc1	počet
každé	každý	k3xTgInPc4	každý
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
stoupal	stoupat	k5eAaImAgInS	stoupat
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
až	až	k8xS	až
dva	dva	k4xCgInPc4	dva
tisíce	tisíc	k4xCgInPc4	tisíc
a	a	k8xC	a
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
století	století	k1gNnSc2	století
už	už	k6eAd1	už
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
činil	činit	k5eAaImAgInS	činit
11	[number]	k4	11
0	[number]	k4	0
<g/>
99	[number]	k4	99
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
nárůst	nárůst	k1gInSc1	nárůst
(	(	kIx(	(
<g/>
cca	cca	kA	cca
20	[number]	k4	20
tisíc	tisíc	k4xCgInSc1	tisíc
<g/>
)	)	kIx)	)
přišel	přijít	k5eAaPmAgInS	přijít
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
v	v	k7c6	v
Charlestonu	charleston	k1gInSc6	charleston
žilo	žít	k5eAaImAgNnS	žít
vůbec	vůbec	k9	vůbec
nejvíce	nejvíce	k6eAd1	nejvíce
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
:	:	kIx,	:
85	[number]	k4	85
796	[number]	k4	796
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
bydlelo	bydlet	k5eAaImAgNnS	bydlet
v	v	k7c6	v
Charlestonu	charleston	k1gInSc6	charleston
53	[number]	k4	53
421	[number]	k4	421
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
24	[number]	k4	24
505	[number]	k4	505
domácností	domácnost	k1gFnPc2	domácnost
a	a	k8xC	a
13	[number]	k4	13
624	[number]	k4	624
rodin	rodina	k1gFnPc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Hustota	hustota	k1gFnSc1	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
byla	být	k5eAaImAgFnS	být
652,7	[number]	k4	652,7
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
tam	tam	k6eAd1	tam
27	[number]	k4	27
131	[number]	k4	131
bytových	bytový	k2eAgFnPc2d1	bytová
jednotek	jednotka	k1gFnPc2	jednotka
při	při	k7c6	při
průměrné	průměrný	k2eAgFnSc6d1	průměrná
hustotě	hustota	k1gFnSc6	hustota
331,5	[number]	k4	331,5
<g/>
/	/	kIx~	/
<g/>
km2	km2	k4	km2
<g/>
.	.	kIx.	.
</s>
<s>
Rasově	rasově	k6eAd1	rasově
se	se	k3xPyFc4	se
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
80,63	[number]	k4	80,63
%	%	kIx~	%
bílých	bílý	k1gMnPc2	bílý
<g/>
,	,	kIx,	,
15,07	[number]	k4	15,07
%	%	kIx~	%
Afroameričanů	Afroameričan	k1gMnPc2	Afroameričan
<g/>
,	,	kIx,	,
0,24	[number]	k4	0,24
%	%	kIx~	%
Američanů	Američan	k1gMnPc2	Američan
<g/>
,	,	kIx,	,
1,83	[number]	k4	1,83
%	%	kIx~	%
Asiatů	Asiat	k1gMnPc2	Asiat
<g/>
,	,	kIx,	,
0,03	[number]	k4	0,03
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Oceánie	Oceánie	k1gFnSc2	Oceánie
<g/>
,	,	kIx,	,
0,30	[number]	k4	0,30
%	%	kIx~	%
jiných	jiný	k2eAgFnPc2d1	jiná
ras	rasa	k1gFnPc2	rasa
<g/>
,	,	kIx,	,
a	a	k8xC	a
1,91	[number]	k4	1,91
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
se	se	k3xPyFc4	se
hlásí	hlásit	k5eAaImIp3nS	hlásit
ke	k	k7c3	k
dvěma	dva	k4xCgInPc3	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
rasám	rasa	k1gFnPc3	rasa
<g/>
.	.	kIx.	.
0,81	[number]	k4	0,81
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
bylo	být	k5eAaImAgNnS	být
hispánského	hispánský	k2eAgInSc2d1	hispánský
nebo	nebo	k8xC	nebo
Latino	latina	k1gFnSc5	latina
původu	původ	k1gInSc2	původ
z	z	k7c2	z
jakékoli	jakýkoli	k3yIgFnSc2	jakýkoli
rasy	rasa	k1gFnSc2	rasa
<g/>
.	.	kIx.	.
</s>
<s>
Pět	pět	k4xCc1	pět
nejčastějších	častý	k2eAgMnPc2d3	nejčastější
původů	původ	k1gMnPc2	původ
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
německé	německý	k2eAgMnPc4d1	německý
Američany	Američan	k1gMnPc4	Američan
(	(	kIx(	(
<g/>
12,4	[number]	k4	12,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Angloameričany	Angloameričan	k1gMnPc7	Angloameričan
(	(	kIx(	(
<g/>
11,6	[number]	k4	11,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Američany	Američan	k1gMnPc7	Američan
(	(	kIx(	(
<g/>
11,4	[number]	k4	11,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
irské	irský	k2eAgFnPc4d1	irská
Američany	Američan	k1gMnPc7	Američan
(	(	kIx(	(
<g/>
10,6	[number]	k4	10,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
italské	italský	k2eAgMnPc4d1	italský
Američany	Američan	k1gMnPc4	Američan
(	(	kIx(	(
<g/>
3,9	[number]	k4	3,9
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
[	[	kIx(	[
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Charlestonu	charleston	k1gInSc6	charleston
bylo	být	k5eAaImAgNnS	být
dle	dle	k7c2	dle
průzkumu	průzkum	k1gInSc2	průzkum
24	[number]	k4	24
505	[number]	k4	505
domácností	domácnost	k1gFnPc2	domácnost
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgFnPc6	který
ze	z	k7c2	z
23,7	[number]	k4	23,7
%	%	kIx~	%
žily	žít	k5eAaImAgFnP	žít
děti	dítě	k1gFnPc1	dítě
do	do	k7c2	do
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
38,9	[number]	k4	38,9
%	%	kIx~	%
byly	být	k5eAaImAgInP	být
spolu	spolu	k6eAd1	spolu
žijící	žijící	k2eAgFnSc2d1	žijící
manželské	manželský	k2eAgFnSc2d1	manželská
dvojice	dvojice	k1gFnSc2	dvojice
<g/>
,	,	kIx,	,
13,5	[number]	k4	13,5
%	%	kIx~	%
bylo	být	k5eAaImAgNnS	být
samostatně	samostatně	k6eAd1	samostatně
žijících	žijící	k2eAgFnPc2d1	žijící
žen	žena	k1gFnPc2	žena
bez	bez	k7c2	bez
manžela	manžel	k1gMnSc2	manžel
<g/>
,	,	kIx,	,
a	a	k8xC	a
44,4	[number]	k4	44,4
%	%	kIx~	%
nebyly	být	k5eNaImAgFnP	být
rodiny	rodina	k1gFnPc1	rodina
<g/>
.	.	kIx.	.
38,9	[number]	k4	38,9
%	%	kIx~	%
všech	všecek	k3xTgFnPc2	všecek
domácností	domácnost	k1gFnPc2	domácnost
sestávala	sestávat	k5eAaImAgFnS	sestávat
z	z	k7c2	z
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
a	a	k8xC	a
ve	v	k7c6	v
14,5	[number]	k4	14,5
%	%	kIx~	%
případů	případ	k1gInPc2	případ
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgMnS	být
někdo	někdo	k3yInSc1	někdo
<g/>
,	,	kIx,	,
komu	kdo	k3yRnSc3	kdo
bylo	být	k5eAaImAgNnS	být
nejméně	málo	k6eAd3	málo
65	[number]	k4	65
let	léto	k1gNnPc2	léto
a	a	k8xC	a
žil	žít	k5eAaImAgMnS	žít
sám	sám	k3xTgMnSc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
velikost	velikost	k1gFnSc1	velikost
domácnosti	domácnost	k1gFnSc2	domácnost
byla	být	k5eAaImAgFnS	být
2,11	[number]	k4	2,11
a	a	k8xC	a
průměrná	průměrný	k2eAgFnSc1d1	průměrná
velikost	velikost	k1gFnSc1	velikost
rodiny	rodina	k1gFnSc2	rodina
byla	být	k5eAaImAgFnS	být
2,82	[number]	k4	2,82
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
věkového	věkový	k2eAgNnSc2d1	věkové
rozložení	rozložení	k1gNnSc2	rozložení
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
20,7	[number]	k4	20,7
%	%	kIx~	%
pod	pod	k7c4	pod
18	[number]	k4	18
let	léto	k1gNnPc2	léto
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
8,4	[number]	k4	8,4
%	%	kIx~	%
od	od	k7c2	od
18	[number]	k4	18
do	do	k7c2	do
24	[number]	k4	24
<g/>
,	,	kIx,	,
27,9	[number]	k4	27,9
%	%	kIx~	%
od	od	k7c2	od
25	[number]	k4	25
do	do	k7c2	do
44	[number]	k4	44
<g/>
,	,	kIx,	,
25,3	[number]	k4	25,3
%	%	kIx~	%
od	od	k7c2	od
45	[number]	k4	45
do	do	k7c2	do
64	[number]	k4	64
a	a	k8xC	a
17,6	[number]	k4	17,6
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
mělo	mít	k5eAaImAgNnS	mít
65	[number]	k4	65
let	léto	k1gNnPc2	léto
či	či	k8xC	či
bylo	být	k5eAaImAgNnS	být
starších	starý	k2eAgNnPc2d2	starší
<g/>
.	.	kIx.	.
</s>
<s>
Středový	středový	k2eAgInSc1d1	středový
věk	věk	k1gInSc1	věk
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgInS	být
41	[number]	k4	41
roků	rok	k1gInPc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
každých	každý	k3xTgInPc2	každý
100	[number]	k4	100
žen	žena	k1gFnPc2	žena
připadalo	připadat	k5eAaPmAgNnS	připadat
87,3	[number]	k4	87,3
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
na	na	k7c4	na
každých	každý	k3xTgFnPc2	každý
100	[number]	k4	100
žen	žena	k1gFnPc2	žena
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
18	[number]	k4	18
a	a	k8xC	a
vyšším	vysoký	k2eAgMnSc6d2	vyšší
bylo	být	k5eAaImAgNnS	být
83,7	[number]	k4	83,7
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgInSc1d1	střední
příjem	příjem	k1gInSc1	příjem
městské	městský	k2eAgFnSc2d1	městská
domácnosti	domácnost	k1gFnSc2	domácnost
ve	v	k7c6	v
městě	město	k1gNnSc6	město
byl	být	k5eAaImAgMnS	být
$	$	kIx~	$
<g/>
34	[number]	k4	34
009	[number]	k4	009
a	a	k8xC	a
střední	střední	k2eAgInSc4d1	střední
rodinný	rodinný	k2eAgInSc4d1	rodinný
příjem	příjem	k1gInSc4	příjem
byl	být	k5eAaImAgInS	být
$	$	kIx~	$
<g/>
47	[number]	k4	47
975	[number]	k4	975
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
měli	mít	k5eAaImAgMnP	mít
střední	střední	k2eAgInSc4d1	střední
příjem	příjem	k1gInSc4	příjem
$	$	kIx~	$
<g/>
38	[number]	k4	38
257	[number]	k4	257
oproti	oproti	k7c3	oproti
$	$	kIx~	$
<g/>
26	[number]	k4	26
671	[number]	k4	671
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Příjem	příjem	k1gInSc1	příjem
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
činil	činit	k5eAaImAgInS	činit
ve	v	k7c6	v
městě	město	k1gNnSc6	město
$	$	kIx~	$
<g/>
26	[number]	k4	26
0	[number]	k4	0
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
12,7	[number]	k4	12,7
%	%	kIx~	%
rodin	rodina	k1gFnPc2	rodina
a	a	k8xC	a
16,7	[number]	k4	16,7
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
žilo	žít	k5eAaImAgNnS	žít
pod	pod	k7c7	pod
hranicí	hranice	k1gFnSc7	hranice
chudoby	chudoba	k1gFnSc2	chudoba
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
včetně	včetně	k7c2	včetně
24,5	[number]	k4	24,5
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
do	do	k7c2	do
věku	věk	k1gInSc2	věk
18	[number]	k4	18
let	léto	k1gNnPc2	léto
a	a	k8xC	a
11,3	[number]	k4	11,3
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
65	[number]	k4	65
let	léto	k1gNnPc2	léto
nebo	nebo	k8xC	nebo
starších	starší	k1gMnPc2	starší
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Charleston	charleston	k1gInSc1	charleston
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
velkého	velký	k2eAgNnSc2d1	velké
množství	množství	k1gNnSc2	množství
základních	základní	k2eAgFnPc2d1	základní
a	a	k8xC	a
středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
middle	middle	k6eAd1	middle
schools	schools	k6eAd1	schools
(	(	kIx(	(
<g/>
přechod	přechod	k1gInSc1	přechod
mezi	mezi	k7c7	mezi
ZŠ	ZŠ	kA	ZŠ
a	a	k8xC	a
SŠ	SŠ	kA	SŠ
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
mezi	mezi	k7c7	mezi
11	[number]	k4	11
a	a	k8xC	a
14	[number]	k4	14
lety	let	k1gInPc7	let
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
součást	součást	k1gFnSc1	součást
školského	školský	k2eAgInSc2d1	školský
systému	systém	k1gInSc2	systém
okresu	okres	k1gInSc2	okres
Kanawha	Kanawha	k1gFnSc1	Kanawha
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
třemi	tři	k4xCgFnPc7	tři
středními	střední	k2eAgFnPc7d1	střední
školami	škola	k1gFnPc7	škola
jsou	být	k5eAaImIp3nP	být
Capital	Capital	k1gMnSc1	Capital
High	High	k1gMnSc1	High
School	Schoola	k1gFnPc2	Schoola
<g/>
,	,	kIx,	,
sloučené	sloučený	k2eAgFnPc1d1	sloučená
Charleston	charleston	k1gInSc4	charleston
High	Higha	k1gFnPc2	Higha
School	Schoola	k1gFnPc2	Schoola
and	and	k?	and
Stonewall	Stonewall	k1gMnSc1	Stonewall
Jackson	Jackson	k1gMnSc1	Jackson
High	High	k1gMnSc1	High
School	Schoola	k1gFnPc2	Schoola
<g/>
;	;	kIx,	;
George	George	k1gFnPc2	George
Washington	Washington	k1gInSc1	Washington
High	High	k1gMnSc1	High
School	School	k1gInSc1	School
a	a	k8xC	a
Charleston	charleston	k1gInSc1	charleston
Catholic	Catholice	k1gFnPc2	Catholice
High	Higha	k1gFnPc2	Higha
School	School	k1gInSc1	School
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
škola	škola	k1gFnSc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Bývalými	bývalý	k2eAgFnPc7d1	bývalá
středními	střední	k2eAgFnPc7d1	střední
školami	škola	k1gFnPc7	škola
jsou	být	k5eAaImIp3nP	být
Charleston	charleston	k1gInSc4	charleston
High	High	k1gInSc1	High
School	School	k1gInSc1	School
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nemocnice	nemocnice	k1gFnSc2	nemocnice
CAMC	CAMC	kA	CAMC
General	General	k1gMnSc1	General
Hospital	Hospital	k1gMnSc1	Hospital
<g/>
,	,	kIx,	,
a	a	k8xC	a
Stonewall	Stonewall	k1gMnSc1	Stonewall
Jackson	Jackson	k1gMnSc1	Jackson
High	High	k1gMnSc1	High
School	Schoola	k1gFnPc2	Schoola
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yIgFnSc2	který
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
pouze	pouze	k6eAd1	pouze
middle	middle	k6eAd1	middle
school	schoola	k1gFnPc2	schoola
<g/>
.	.	kIx.	.
</s>
<s>
Middle	Middle	k6eAd1	Middle
schools	schools	k6eAd1	schools
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Junior	junior	k1gMnSc1	junior
High	Higha	k1gFnPc2	Higha
Schools	Schoolsa	k1gFnPc2	Schoolsa
<g/>
)	)	kIx)	)
tedy	tedy	k9	tedy
kromě	kromě	k7c2	kromě
John	John	k1gMnSc1	John
Adams	Adams	k1gInSc4	Adams
Middle	Middle	k1gFnSc2	Middle
School	Schoola	k1gFnPc2	Schoola
a	a	k8xC	a
Horace	Horace	k1gFnSc2	Horace
Mann	Mann	k1gMnSc1	Mann
Middle	Middle	k1gFnPc2	Middle
School	Schoola	k1gFnPc2	Schoola
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
také	také	k9	také
Stonewall	Stonewall	k1gMnSc1	Stonewall
Jackson	Jackson	k1gMnSc1	Jackson
Middle	Middle	k1gMnSc1	Middle
School	School	k1gInSc4	School
<g/>
.	.	kIx.	.
</s>
<s>
Základních	základní	k2eAgFnPc2d1	základní
škol	škola	k1gFnPc2	škola
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Charlestonu	charleston	k1gInSc6	charleston
celkem	celkem	k6eAd1	celkem
jedenáct	jedenáct	k4xCc1	jedenáct
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Charlestonu	charleston	k1gInSc6	charleston
má	mít	k5eAaImIp3nS	mít
sídlo	sídlo	k1gNnSc1	sídlo
West	Westa	k1gFnPc2	Westa
Virginia	Virginium	k1gNnSc2	Virginium
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
nemocniční	nemocniční	k2eAgInSc1d1	nemocniční
kampus	kampus	k1gInSc1	kampus
pro	pro	k7c4	pro
její	její	k3xOp3gMnPc4	její
studenty	student	k1gMnPc4	student
na	na	k7c6	na
školách	škola	k1gFnPc6	škola
medicíny	medicína	k1gFnSc2	medicína
a	a	k8xC	a
stomatologie	stomatologie	k1gFnSc2	stomatologie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
školách	škola	k1gFnPc6	škola
musí	muset	k5eAaImIp3nP	muset
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
hlavním	hlavní	k2eAgInSc6d1	hlavní
kampusu	kampus	k1gInSc6	kampus
v	v	k7c6	v
Morgantownu	Morgantown	k1gInSc6	Morgantown
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemocniční	nemocniční	k2eAgFnSc4d1	nemocniční
praxi	praxe	k1gFnSc4	praxe
mohou	moct	k5eAaImIp3nP	moct
dělat	dělat	k5eAaImF	dělat
v	v	k7c6	v
Morgantownu	Morgantown	k1gInSc6	Morgantown
nebo	nebo	k8xC	nebo
právě	právě	k6eAd1	právě
v	v	k7c6	v
Charlestonu	charleston	k1gInSc6	charleston
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
také	také	k9	také
funguje	fungovat	k5eAaImIp3nS	fungovat
soukromá	soukromý	k2eAgFnSc1d1	soukromá
vysoká	vysoký	k2eAgFnSc1d1	vysoká
škola	škola	k1gFnSc1	škola
pro	pro	k7c4	pro
1000	[number]	k4	1000
studentů	student	k1gMnPc2	student
<g/>
,	,	kIx,	,
University	universita	k1gFnPc1	universita
of	of	k?	of
Charleston	charleston	k1gInSc1	charleston
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
Morris	Morris	k1gFnSc1	Morris
Harvey	Harvea	k1gMnSc2	Harvea
College	Colleg	k1gMnSc2	Colleg
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c4	na
MacCorkle	MacCorkl	k1gInSc5	MacCorkl
Avenue	avenue	k1gFnSc6	avenue
podél	podél	k7c2	podél
řeky	řeka	k1gFnSc2	řeka
Kanawha	Kanawh	k1gMnSc2	Kanawh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
tato	tento	k3xDgFnSc1	tento
VŠ	vš	k0	vš
otevřela	otevřít	k5eAaPmAgFnS	otevřít
obor	obor	k1gInSc4	obor
farmacie	farmacie	k1gFnSc2	farmacie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bezprostřední	bezprostřední	k2eAgFnSc6d1	bezprostřední
blízkosti	blízkost	k1gFnSc6	blízkost
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
West	West	k1gInSc1	West
Virginia	Virginium	k1gNnSc2	Virginium
State	status	k1gInSc5	status
University	universita	k1gFnPc4	universita
a	a	k8xC	a
Marshall	Marshall	k1gInSc4	Marshall
University	universita	k1gFnSc2	universita
Graduate	Graduat	k1gMnSc5	Graduat
College	Collegus	k1gMnSc5	Collegus
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
postgraduální	postgraduální	k2eAgInSc1d1	postgraduální
obor	obor	k1gInSc1	obor
Marshall	Marshalla	k1gFnPc2	Marshalla
University	universita	k1gFnSc2	universita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
je	být	k5eAaImIp3nS	být
též	též	k9	též
sídlo	sídlo	k1gNnSc4	sídlo
Charlestonské	Charlestonský	k2eAgFnSc2d1	Charlestonský
pobočky	pobočka	k1gFnSc2	pobočka
Institutu	institut	k1gInSc2	institut
Roberta	Robert	k1gMnSc2	Robert
C.	C.	kA	C.
Byrda	Byrdo	k1gNnSc2	Byrdo
pro	pro	k7c4	pro
Advanced	Advanced	k1gInSc4	Advanced
Flexible	Flexible	k1gFnSc3	Flexible
Manufacturing	Manufacturing	k1gInSc1	Manufacturing
<g/>
,	,	kIx,	,
nezávislý	závislý	k2eNgInSc1d1	nezávislý
program	program	k1gInSc1	program
spravovaný	spravovaný	k2eAgInSc4d1	spravovaný
Marshall	Marshall	k1gInSc4	Marshall
University	universita	k1gFnSc2	universita
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
zařízení	zařízení	k1gNnSc3	zařízení
pro	pro	k7c4	pro
computer	computer	k1gInSc4	computer
numerical	numericat	k5eAaPmAgInS	numericat
control	control	k1gInSc1	control
(	(	kIx(	(
<g/>
CNC	CNC	kA	CNC
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
firmy	firma	k1gFnPc4	firma
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
50	[number]	k4	50
km	km	kA	km
od	od	k7c2	od
Charlestonu	charleston	k1gInSc2	charleston
<g/>
,	,	kIx,	,
v	v	k7c4	v
Montgomery	Montgomera	k1gFnPc4	Montgomera
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
kampus	kampus	k1gInSc1	kampus
West	Westa	k1gFnPc2	Westa
Virginia	Virginium	k1gNnSc2	Virginium
University	universita	k1gFnSc2	universita
Institute	institut	k1gInSc5	institut
of	of	k?	of
Technology	technolog	k1gMnPc7	technolog
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgInSc7d3	veliký
místním	místní	k2eAgInSc7d1	místní
kampusem	kampus	k1gInSc7	kampus
této	tento	k3xDgFnSc2	tento
university	universita	k1gFnSc2	universita
a	a	k8xC	a
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
se	se	k3xPyFc4	se
především	především	k9	především
na	na	k7c4	na
strojírenské	strojírenský	k2eAgInPc4d1	strojírenský
studentské	studentský	k2eAgInPc4d1	studentský
programy	program	k1gInPc4	program
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Charlestonu	charleston	k1gInSc6	charleston
sídlí	sídlet	k5eAaImIp3nS	sídlet
i	i	k9	i
kampus	kampus	k1gInSc1	kampus
West	Westa	k1gFnPc2	Westa
Virginia	Virginium	k1gNnSc2	Virginium
Junior	junior	k1gMnSc1	junior
College	Colleg	k1gFnSc2	Colleg
nacházející	nacházející	k2eAgFnSc2d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
na	na	k7c6	na
adrese	adresa	k1gFnSc6	adresa
1000	[number]	k4	1000
Virginia	Virginium	k1gNnSc2	Virginium
Street	Streeta	k1gFnPc2	Streeta
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
akreditaci	akreditace	k1gFnSc4	akreditace
na	na	k7c4	na
udělování	udělování	k1gNnSc4	udělování
diplomů	diplom	k1gInPc2	diplom
a	a	k8xC	a
příbuzných	příbuzný	k2eAgInPc2d1	příbuzný
titulů	titul	k1gInPc2	titul
a	a	k8xC	a
v	v	k7c6	v
Kanawha	Kanawh	k1gMnSc2	Kanawh
Valley	Valley	k1gInPc4	Valley
funguje	fungovat	k5eAaImIp3nS	fungovat
už	už	k6eAd1	už
téměř	téměř	k6eAd1	téměř
115	[number]	k4	115
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
jako	jako	k9	jako
Capitol	Capitol	k1gInSc1	Capitol
City	city	k1gNnSc1	city
Commercial	Commercial	k1gInSc1	Commercial
College	College	k1gFnSc1	College
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1892	[number]	k4	1892
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
školila	školit	k5eAaImAgFnS	školit
studenty	student	k1gMnPc4	student
v	v	k7c6	v
sekretářských	sekretářský	k2eAgFnPc6d1	sekretářská
a	a	k8xC	a
obchodních	obchodní	k2eAgFnPc6d1	obchodní
dovednostech	dovednost	k1gFnPc6	dovednost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
během	během	k7c2	během
let	léto	k1gNnPc2	léto
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
proběhly	proběhnout	k5eAaPmAgFnP	proběhnout
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
osnovách	osnova	k1gFnPc6	osnova
i	i	k8xC	i
jejím	její	k3xOp3gNnSc6	její
umístění	umístění	k1gNnSc6	umístění
<g/>
.	.	kIx.	.
</s>
<s>
Appalachian	Appalachian	k1gMnSc1	Appalachian
Power	Power	k1gMnSc1	Power
<g/>
,	,	kIx,	,
vlastněná	vlastněný	k2eAgFnSc1d1	vlastněná
firmou	firma	k1gFnSc7	firma
American	American	k1gMnSc1	American
Electric	Electric	k1gMnSc1	Electric
Power	Power	k1gMnSc1	Power
<g/>
[	[	kIx(	[
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
v	v	k7c6	v
Columbusu	Columbus	k1gInSc6	Columbus
Charleston	charleston	k1gInSc1	charleston
Newspapers	Newspapers	k1gInSc1	Newspapers
Charleston	charleston	k1gInSc1	charleston
Stamping	Stamping	k1gInSc1	Stamping
and	and	k?	and
Manufacturing	Manufacturing	k1gInSc1	Manufacturing
Gino	Gino	k1gMnSc1	Gino
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Pizza	pizza	k1gFnSc1	pizza
and	and	k?	and
Spaghetti	spaghetti	k1gInPc1	spaghetti
MATRIC	matrice	k1gFnPc2	matrice
(	(	kIx(	(
<g/>
Mid-Atlantic	Mid-Atlantice	k1gFnPc2	Mid-Atlantice
Technology	technolog	k1gMnPc4	technolog
<g/>
,	,	kIx,	,
Research	Research	k1gInSc1	Research
and	and	k?	and
Innovation	Innovation	k1gInSc1	Innovation
Center	centrum	k1gNnPc2	centrum
<g/>
)	)	kIx)	)
Tudor	tudor	k1gInSc1	tudor
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Biscuit	Biscuit	k1gMnSc1	Biscuit
World	World	k1gMnSc1	World
United	United	k1gMnSc1	United
Bank	banka	k1gFnPc2	banka
of	of	k?	of
West	West	k1gMnSc1	West
Virginia	Virginium	k1gNnSc2	Virginium
<g/>
,	,	kIx,	,
Inc	Inc	k1gFnSc2	Inc
<g/>
.	.	kIx.	.
</s>
<s>
Walker	Walker	k1gMnSc1	Walker
Machinery	Machiner	k1gMnPc4	Machiner
se	se	k3xPyFc4	se
sídlem	sídlo	k1gNnSc7	sídlo
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Belle	bell	k1gInSc5	bell
West	West	k1gMnSc1	West
Virginia-American	Virginia-Americany	k1gInPc2	Virginia-Americany
Water	Water	k1gInSc4	Water
Company	Compana	k1gFnSc2	Compana
<g/>
,	,	kIx,	,
vlastněná	vlastněný	k2eAgFnSc1d1	vlastněná
společností	společnost	k1gFnSc7	společnost
RWE	RWE	kA	RWE
AG	AG	kA	AG
z	z	k7c2	z
německého	německý	k2eAgInSc2d1	německý
Essenu	Essen	k1gInSc2	Essen
Restauranty	restaurant	k1gInPc1	restaurant
Shoney	Shonea	k1gFnSc2	Shonea
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Diskontní	diskontní	k2eAgFnSc7d1	diskontní
obchodní	obchodní	k2eAgFnSc7d1	obchodní
domy	dům	k1gInPc1	dům
Heck	Heck	k1gInSc1	Heck
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
/	/	kIx~	/
L.A.	L.A.	k1gMnSc2	L.A.
Joe	Joe	k1gFnSc1	Joe
Charleston	charleston	k1gInSc1	charleston
každoročně	každoročně	k6eAd1	každoročně
pořádá	pořádat	k5eAaImIp3nS	pořádat
množství	množství	k1gNnSc4	množství
kulturních	kulturní	k2eAgFnPc2d1	kulturní
akcí	akce	k1gFnPc2	akce
a	a	k8xC	a
veletrhů	veletrh	k1gInPc2	veletrh
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
od	od	k7c2	od
břehů	břeh	k1gInPc2	břeh
řeky	řeka	k1gFnSc2	řeka
Kanawha	Kanawha	k1gFnSc1	Kanawha
až	až	k6eAd1	až
na	na	k7c4	na
plochy	plocha	k1gFnPc4	plocha
budov	budova	k1gFnPc2	budova
kapitolu	kapitola	k1gFnSc4	kapitola
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
West	Westa	k1gFnPc2	Westa
Virginia	Virginium	k1gNnSc2	Virginium
Dance	Danka	k1gFnSc6	Danka
Festivalu	festival	k1gInSc6	festival
<g/>
,	,	kIx,	,
konajícím	konající	k2eAgMnSc7d1	konající
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
25	[number]	k4	25
<g/>
.	.	kIx.	.
a	a	k8xC	a
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubnem	duben	k1gInSc7	duben
<g/>
,	,	kIx,	,
studenti	student	k1gMnPc1	student
tance	tanec	k1gInSc2	tanec
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
státu	stát	k1gInSc2	stát
navštěvují	navštěvovat	k5eAaImIp3nP	navštěvovat
hodiny	hodina	k1gFnPc1	hodina
a	a	k8xC	a
workshopy	workshop	k1gInPc1	workshop
baletu	balet	k1gInSc2	balet
<g/>
,	,	kIx,	,
jazzu	jazz	k1gInSc2	jazz
a	a	k8xC	a
moderního	moderní	k2eAgInSc2d1	moderní
tance	tanec	k1gInSc2	tanec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
mají	mít	k5eAaImIp3nP	mít
studenti	student	k1gMnPc1	student
představení	představení	k1gNnSc2	představení
v	v	k7c6	v
divadle	divadlo	k1gNnSc6	divadlo
West	Westa	k1gFnPc2	Westa
Virginia	Virginium	k1gNnSc2	Virginium
State	status	k1gInSc5	status
Theatre	Theatr	k1gMnSc5	Theatr
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
koná	konat	k5eAaImIp3nS	konat
zdarma	zdarma	k6eAd1	zdarma
<g/>
.	.	kIx.	.
</s>
<s>
Dvakrát	dvakrát	k6eAd1	dvakrát
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
na	na	k7c6	na
konci	konec	k1gInSc6	konec
dubna	duben	k1gInSc2	duben
a	a	k8xC	a
opět	opět	k6eAd1	opět
znovu	znovu	k6eAd1	znovu
začátkem	začátkem	k7c2	začátkem
listopadu	listopad	k1gInSc2	listopad
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
West	West	k1gInSc1	West
Virginia	Virginium	k1gNnSc2	Virginium
International	International	k1gFnSc2	International
Film	film	k1gInSc1	film
Festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
promítá	promítat	k5eAaImIp3nS	promítat
mnoho	mnoho	k4c1	mnoho
domácích	domácí	k2eAgMnPc2d1	domácí
i	i	k8xC	i
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
filmy	film	k1gInPc1	film
celovečerní	celovečerní	k2eAgInPc1d1	celovečerní
<g/>
,	,	kIx,	,
krátké	krátká	k1gFnPc1	krátká
<g/>
,	,	kIx,	,
dokumentární	dokumentární	k2eAgInPc1d1	dokumentární
<g/>
,	,	kIx,	,
animované	animovaný	k2eAgInPc1d1	animovaný
i	i	k8xC	i
studentské	studentský	k2eAgInPc1d1	studentský
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
září	září	k1gNnSc2	září
Charleston	charleston	k1gInSc1	charleston
hostí	hostit	k5eAaImIp3nP	hostit
Daily	Dail	k1gInPc1	Dail
Mail	mail	k1gInSc1	mail
Kanawha	Kanawh	k1gMnSc4	Kanawh
County	Counta	k1gFnSc2	Counta
Majorette	Majorett	k1gInSc5	Majorett
and	and	k?	and
Band	band	k1gInSc1	band
Festival	festival	k1gInSc1	festival
pro	pro	k7c4	pro
osm	osm	k4xCc4	osm
státních	státní	k2eAgFnPc2d1	státní
středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
z	z	k7c2	z
okresu	okres	k1gInSc2	okres
Kanawha	Kanawha	k1gFnSc1	Kanawha
<g/>
.	.	kIx.	.
</s>
<s>
Festival	festival	k1gInSc1	festival
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
a	a	k8xC	a
v	v	k7c6	v
každoroční	každoroční	k2eAgFnSc6d1	každoroční
tradici	tradice	k1gFnSc6	tradice
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Koná	konat	k5eAaImIp3nS	konat
se	se	k3xPyFc4	se
na	na	k7c6	na
stadiónu	stadión	k1gInSc6	stadión
University	universita	k1gFnSc2	universita
of	of	k?	of
Charleston	charleston	k1gInSc1	charleston
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
mívá	mívat	k5eAaImIp3nS	mívat
v	v	k7c4	v
Clay	Claa	k1gFnPc4	Claa
Centeru	Center	k1gInSc2	Center
představení	představení	k1gNnSc1	představení
Kanawha	Kanawha	k1gMnSc1	Kanawha
Kordsmen	Kordsmen	k2eAgInSc1d1	Kordsmen
Barbershop	Barbershop	k1gInSc1	Barbershop
Chorus	chorus	k1gInSc1	chorus
<g/>
.	.	kIx.	.
40	[number]	k4	40
<g/>
-členný	-členný	k2eAgInSc1d1	-členný
sbor	sbor	k1gInSc1	sbor
zpívá	zpívat	k5eAaImIp3nS	zpívat
v	v	k7c6	v
představení	představení	k1gNnSc6	představení
nazvaném	nazvaný	k2eAgNnSc6d1	nazvané
"	"	kIx"	"
<g/>
Songs	Songs	k1gInSc1	Songs
America	Americ	k1gInSc2	Americ
Sings	Singsa	k1gFnPc2	Singsa
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Písně	píseň	k1gFnPc1	píseň
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
si	se	k3xPyFc3	se
zpívá	zpívat	k5eAaImIp3nS	zpívat
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
víkendu	víkend	k1gInSc6	víkend
před	před	k7c4	před
Memorial	Memorial	k1gInSc4	Memorial
Day	Day	k1gFnSc2	Day
<g/>
,	,	kIx,	,
posledním	poslední	k2eAgNnSc7d1	poslední
květnovým	květnový	k2eAgNnSc7d1	květnové
pondělím	pondělí	k1gNnSc7	pondělí
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
státního	státní	k2eAgInSc2d1	státní
kapitolu	kapitola	k1gFnSc4	kapitola
koná	konat	k5eAaImIp3nS	konat
Vandalia	Vandalia	k1gFnSc1	Vandalia
Gathering	Gathering	k1gInSc1	Gathering
<g/>
.	.	kIx.	.
</s>
<s>
Tisíce	tisíc	k4xCgInPc1	tisíc
návštěvníků	návštěvník	k1gMnPc2	návštěvník
se	se	k3xPyFc4	se
baví	bavit	k5eAaImIp3nS	bavit
tradiční	tradiční	k2eAgFnSc7d1	tradiční
hudbou	hudba	k1gFnSc7	hudba
<g/>
,	,	kIx,	,
uměním	umění	k1gNnSc7	umění
<g/>
,	,	kIx,	,
tancem	tanec	k1gInSc7	tanec
<g/>
,	,	kIx,	,
příběhy	příběh	k1gInPc7	příběh
<g/>
,	,	kIx,	,
řemesly	řemeslo	k1gNnPc7	řemeslo
a	a	k8xC	a
jídlem	jídlo	k1gNnSc7	jídlo
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
"	"	kIx"	"
<g/>
unikátnosti	unikátnost	k1gFnSc2	unikátnost
západovirginské	západovirginský	k2eAgFnSc2d1	západovirginský
horské	horský	k2eAgFnSc2d1	horská
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Za	za	k7c4	za
vstup	vstup	k1gInSc4	vstup
se	se	k3xPyFc4	se
neplatí	platit	k5eNaImIp3nS	platit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
v	v	k7c6	v
Charlestonu	charleston	k1gInSc6	charleston
týden	týden	k1gInSc1	týden
kulturních	kulturní	k2eAgFnPc2d1	kulturní
a	a	k8xC	a
uměleckých	umělecký	k2eAgFnPc2d1	umělecká
akcí	akce	k1gFnPc2	akce
FestivALL	FestivALL	k1gFnSc2	FestivALL
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
začíná	začínat	k5eAaImIp3nS	začínat
20	[number]	k4	20
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
Dnem	den	k1gInSc7	den
Západní	západní	k2eAgFnSc2d1	západní
Virginie	Virginie	k1gFnSc2	Virginie
a	a	k8xC	a
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
tanec	tanec	k1gInSc4	tanec
<g/>
,	,	kIx,	,
divadlo	divadlo	k1gNnSc4	divadlo
a	a	k8xC	a
hudbu	hudba	k1gFnSc4	hudba
<g/>
.	.	kIx.	.
</s>
<s>
FestivALL	FestivALL	k?	FestivALL
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
místním	místní	k2eAgMnPc3d1	místní
umělcům	umělec	k1gMnPc3	umělec
skvělou	skvělý	k2eAgFnSc4d1	skvělá
šanci	šance	k1gFnSc4	šance
vystavit	vystavit	k5eAaPmF	vystavit
své	svůj	k3xOyFgNnSc4	svůj
dílo	dílo	k1gNnSc4	dílo
a	a	k8xC	a
zaujmout	zaujmout	k5eAaPmF	zaujmout
a	a	k8xC	a
začlenit	začlenit	k5eAaPmF	začlenit
do	do	k7c2	do
něj	on	k3xPp3gMnSc2	on
ostatní	ostatní	k2eAgMnPc4d1	ostatní
lidi	člověk	k1gMnPc4	člověk
z	z	k7c2	z
místní	místní	k2eAgFnSc2d1	místní
umělecké	umělecký	k2eAgFnSc2d1	umělecká
komunity	komunita	k1gFnSc2	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Zlatým	zlatý	k2eAgInSc7d1	zlatý
hřebem	hřeb	k1gInSc7	hřeb
bývá	bývat	k5eAaImIp3nS	bývat
umělecký	umělecký	k2eAgInSc4d1	umělecký
veletrh	veletrh	k1gInSc4	veletrh
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
Capitol	Capitola	k1gFnPc2	Capitola
Street	Street	k1gInSc1	Street
a	a	k8xC	a
místní	místní	k2eAgFnPc1d1	místní
skupiny	skupina	k1gFnPc1	skupina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
hrají	hrát	k5eAaImIp3nP	hrát
naživo	naživo	k1gNnSc4	naživo
na	na	k7c6	na
pódiích	pódium	k1gNnPc6	pódium
postavených	postavený	k2eAgNnPc6d1	postavené
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k8xC	tak
se	se	k3xPyFc4	se
oblibě	obliba	k1gFnSc3	obliba
těší	těšit	k5eAaImIp3nS	těšit
i	i	k9	i
festival	festival	k1gInSc1	festival
jazzu	jazz	k1gInSc2	jazz
a	a	k8xC	a
vína	víno	k1gNnSc2	víno
v	v	k7c6	v
kampusu	kampus	k1gInSc6	kampus
University	universita	k1gFnSc2	universita
of	of	k?	of
Charleston	charleston	k1gInSc1	charleston
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nS	účastnit
místní	místní	k2eAgFnSc1d1	místní
i	i	k9	i
celonárodně	celonárodně	k6eAd1	celonárodně
známí	známý	k2eAgMnPc1d1	známý
jazzmani	jazzman	k1gMnPc1	jazzman
a	a	k8xC	a
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
prezentují	prezentovat	k5eAaBmIp3nP	prezentovat
výrobky	výrobek	k1gInPc1	výrobek
západovirginských	západovirginský	k2eAgFnPc2d1	západovirginský
vinic	vinice	k1gFnPc2	vinice
<g/>
.	.	kIx.	.
</s>
<s>
Avampato	Avampato	k6eAd1	Avampato
Discovery	Discovera	k1gFnSc2	Discovera
Museum	museum	k1gNnSc1	museum
v	v	k7c4	v
Clay	Claa	k1gFnPc4	Claa
Centeru	Center	k1gInSc2	Center
Sunrise	Sunrise	k1gFnSc2	Sunrise
Museum	museum	k1gNnSc4	museum
-	-	kIx~	-
nyní	nyní	k6eAd1	nyní
součástí	součást	k1gFnSc7	součást
Clay	Claa	k1gFnSc2	Claa
Centeru	Center	k1gInSc2	Center
West	West	k1gInSc1	West
Virginia	Virginium	k1gNnSc2	Virginium
State	status	k1gInSc5	status
Museum	museum	k1gNnSc1	museum
South	South	k1gInSc1	South
Charleston	charleston	k1gInSc1	charleston
Museum	museum	k1gNnSc4	museum
St.	st.	kA	st.
George	George	k1gNnSc4	George
Orthodox	Orthodox	k1gInSc1	Orthodox
Cathedral	Cathedral	k1gFnSc4	Cathedral
založená	založený	k2eAgFnSc1d1	založená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1892	[number]	k4	1892
Stadión	stadión	k1gInSc1	stadión
University	universita	k1gFnSc2	universita
of	of	k?	of
Charleston	charleston	k1gInSc1	charleston
na	na	k7c4	na
Laidley	Laidlea	k1gFnPc4	Laidlea
Field	Fielda	k1gFnPc2	Fielda
používané	používaný	k2eAgFnSc2d1	používaná
pro	pro	k7c4	pro
americký	americký	k2eAgInSc4d1	americký
fotbal	fotbal	k1gInSc4	fotbal
<g/>
,	,	kIx,	,
fotbal	fotbal	k1gInSc4	fotbal
<g/>
,	,	kIx,	,
atletiku	atletika	k1gFnSc4	atletika
a	a	k8xC	a
festivaly	festival	k1gInPc4	festival
Appalachian	Appalachiana	k1gFnPc2	Appalachiana
Power	Powra	k1gFnPc2	Powra
Park	park	k1gInSc1	park
<g />
.	.	kIx.	.
</s>
<s>
-	-	kIx~	-
stadión	stadión	k1gInSc4	stadión
West	Westa	k1gFnPc2	Westa
Virginia	Virginium	k1gNnSc2	Virginium
Power	Power	k1gMnSc1	Power
Cato	Cato	k1gMnSc1	Cato
Park	park	k1gInSc1	park
-	-	kIx~	-
největší	veliký	k2eAgInSc1d3	veliký
městský	městský	k2eAgInSc1d1	městský
park	park	k1gInSc1	park
Charlestonu	charleston	k1gInSc2	charleston
s	s	k7c7	s
golfovým	golfový	k2eAgNnSc7d1	golfové
hřištěm	hřiště	k1gNnSc7	hřiště
<g/>
,	,	kIx,	,
velkým	velký	k2eAgInSc7d1	velký
bazénem	bazén	k1gInSc7	bazén
a	a	k8xC	a
místy	místo	k1gNnPc7	místo
pro	pro	k7c4	pro
piknik	piknik	k1gInSc4	piknik
Coonskin	Coonskin	k2eAgInSc1d1	Coonskin
Park	park	k1gInSc1	park
-	-	kIx~	-
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
bazén	bazén	k1gInSc4	bazén
<g/>
,	,	kIx,	,
přístřešek	přístřešek	k1gInSc4	přístřešek
pro	pro	k7c4	pro
čluny	člun	k1gInPc4	člun
<g/>
,	,	kIx,	,
klubovnu	klubovna	k1gFnSc4	klubovna
s	s	k7c7	s
možnostmi	možnost	k1gFnPc7	možnost
stravování	stravování	k1gNnSc2	stravování
<g/>
,	,	kIx,	,
tenisové	tenisový	k2eAgInPc4d1	tenisový
kurty	kurt	k1gInPc4	kurt
<g/>
,	,	kIx,	,
minigolf	minigolf	k1gInSc1	minigolf
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
-jamkové	amkový	k2eAgNnSc4d1	-jamkový
golfové	golfový	k2eAgNnSc4d1	golfové
hřiště	hřiště	k1gNnSc4	hřiště
a	a	k8xC	a
jezero	jezero	k1gNnSc4	jezero
pro	pro	k7c4	pro
rybaření	rybařený	k2eAgMnPc5d1	rybařený
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Schoenbaum	Schoenbaum	k1gInSc4	Schoenbaum
Soccer	Soccer	k1gMnSc1	Soccer
Field	Field	k1gMnSc1	Field
(	(	kIx(	(
<g/>
fotbalové	fotbalový	k2eAgNnSc4d1	fotbalové
hřiště	hřiště	k1gNnSc4	hřiště
<g/>
)	)	kIx)	)
a	a	k8xC	a
Amphitheatre	Amphitheatr	k1gInSc5	Amphitheatr
uvnitř	uvnitř	k7c2	uvnitř
parku	park	k1gInSc2	park
má	mít	k5eAaImIp3nS	mít
sídlo	sídlo	k1gNnSc4	sídlo
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
West	Westa	k1gFnPc2	Westa
Virginia	Virginium	k1gNnSc2	Virginium
Chaos	chaos	k1gInSc1	chaos
Daniel	Daniel	k1gMnSc1	Daniel
Boone	Boon	k1gInSc5	Boon
Park	park	k1gInSc1	park
-	-	kIx~	-
čtyřakrový	čtyřakrový	k2eAgInSc1d1	čtyřakrový
park	park	k1gInSc1	park
s	s	k7c7	s
rampou	rampa	k1gFnSc7	rampa
pro	pro	k7c4	pro
čluny	člun	k1gInPc4	člun
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
také	také	k9	také
rybařit	rybařit	k5eAaImF	rybařit
a	a	k8xC	a
piknikovat	piknikovat	k5eAaBmF	piknikovat
Kanawha	Kanawh	k1gMnSc4	Kanawh
State	status	k1gInSc5	status
Forest	Forest	k1gInSc1	Forest
-	-	kIx~	-
les	les	k1gInSc1	les
o	o	k7c4	o
38	[number]	k4	38
km2	km2	k4	km2
s	s	k7c7	s
46	[number]	k4	46
<g />
.	.	kIx.	.
</s>
<s>
kempy	kemp	k1gInPc1	kemp
nacházející	nacházející	k2eAgInPc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
části	část	k1gFnSc6	část
Loudendale	Loudendala	k1gFnSc6	Loudendala
Little	Little	k1gFnSc1	Little
Creek	Creek	k1gInSc4	Creek
Park	park	k1gInSc1	park
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
basketball	basketball	k1gInSc4	basketball
<g/>
,	,	kIx,	,
tenis	tenis	k1gInSc4	tenis
<g/>
,	,	kIx,	,
a	a	k8xC	a
závody	závod	k1gInPc4	závod
mmotokár	mmotokár	k1gInSc1	mmotokár
(	(	kIx(	(
<g/>
soap	soap	k1gInSc1	soap
box	box	k1gInSc1	box
cars	cars	k1gInSc4	cars
<g/>
)	)	kIx)	)
Magic	Magice	k1gInPc2	Magice
Island	Island	k1gInSc1	Island
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc4	oblast
nacházející	nacházející	k2eAgFnSc4d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
řek	řek	k1gMnSc1	řek
Elk	Elk	k1gMnSc1	Elk
a	a	k8xC	a
Kanawha	Kanawha	k1gMnSc1	Kanawha
poblíž	poblíž	k6eAd1	poblíž
Kanawha	Kanawha	k1gMnSc1	Kanawha
Boulevardu	Boulevard	k1gInSc2	Boulevard
Davis	Davis	k1gFnSc2	Davis
Park	park	k1gInSc1	park
Haddad	Haddad	k1gInSc1	Haddad
Riverfront	Riverfronta	k1gFnPc2	Riverfronta
Park	park	k1gInSc1	park
Ruffner	Ruffner	k1gMnSc1	Ruffner
Park	park	k1gInSc1	park
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Charlestonu	charleston	k1gInSc6	charleston
dvě	dva	k4xCgNnPc1	dva
nákupní	nákupní	k2eAgNnPc1d1	nákupní
centra	centrum	k1gNnPc1	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
je	být	k5eAaImIp3nS	být
Charleston	charleston	k1gInSc1	charleston
Town	Towna	k1gFnPc2	Towna
Center	centrum	k1gNnPc2	centrum
otevřené	otevřený	k2eAgFnPc1d1	otevřená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
patry	patro	k1gNnPc7	patro
obchodů	obchod	k1gInPc2	obchod
a	a	k8xC	a
restaurací	restaurace	k1gFnPc2	restaurace
<g/>
.	.	kIx.	.
</s>
<s>
Základ	základ	k1gInSc1	základ
nákupního	nákupní	k2eAgNnSc2d1	nákupní
centra	centrum	k1gNnSc2	centrum
tvoří	tvořit	k5eAaImIp3nP	tvořit
obchody	obchod	k1gInPc1	obchod
Macy	Maca	k1gFnSc2	Maca
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
<g/>
,	,	kIx,	,
Sears	Searsa	k1gFnPc2	Searsa
a	a	k8xC	a
J.C.	J.C.	k1gFnPc2	J.C.
Penney	Pennea	k1gFnSc2	Pennea
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtým	čtvrtý	k4xOgInSc7	čtvrtý
základním	základní	k2eAgInSc7d1	základní
kamenem	kámen	k1gInSc7	kámen
je	být	k5eAaImIp3nS	být
BrickStreet	BrickStreet	k1gMnSc1	BrickStreet
Mutual	Mutual	k1gMnSc1	Mutual
Insurance	Insurance	k1gFnPc4	Insurance
Co	co	k9	co
<g/>
..	..	k?	..
Centrum	centrum	k1gNnSc1	centrum
také	také	k9	také
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
šest	šest	k4xCc4	šest
velkých	velký	k2eAgFnPc2d1	velká
restaurací	restaurace	k1gFnPc2	restaurace
umístěných	umístěný	k2eAgFnPc2d1	umístěná
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
silnice	silnice	k1gFnSc2	silnice
a	a	k8xC	a
12	[number]	k4	12
fast	fasta	k1gFnPc2	fasta
food	food	k6eAd1	food
restaurantů	restaurant	k1gInPc2	restaurant
jako	jako	k9	jako
zástupce	zástupce	k1gMnSc1	zástupce
tzv.	tzv.	kA	tzv.
food	food	k1gMnSc1	food
courtu	court	k1gInSc2	court
(	(	kIx(	(
<g/>
=	=	kIx~	=
velká	velký	k2eAgFnSc1d1	velká
otevřená	otevřený	k2eAgFnSc1d1	otevřená
plocha	plocha	k1gFnSc1	plocha
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
stánky	stánek	k1gInPc7	stánek
fast	fast	k1gMnSc1	fast
food	food	k1gMnSc1	food
restaurací	restaurace	k1gFnPc2	restaurace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
třetím	třetí	k4xOgInSc6	třetí
podlaží	podlaží	k1gNnSc6	podlaží
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
městským	městský	k2eAgInSc7d1	městský
nákupním	nákupní	k2eAgInSc7d1	nákupní
centrem	centr	k1gInSc7	centr
je	být	k5eAaImIp3nS	být
Kanawha	Kanawha	k1gMnSc1	Kanawha
Mall	Mall	k1gMnSc1	Mall
postavený	postavený	k2eAgMnSc1d1	postavený
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
části	část	k1gFnSc6	část
Kanawha	Kanawha	k1gFnSc1	Kanawha
City	City	k1gFnSc2	City
<g/>
.	.	kIx.	.
</s>
<s>
Velikostí	velikost	k1gFnSc7	velikost
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
mnoho	mnoho	k6eAd1	mnoho
menší	malý	k2eAgFnPc4d2	menší
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc6	jeho
pilíři	pilíř	k1gInSc6	pilíř
jsou	být	k5eAaImIp3nP	být
obchody	obchod	k1gInPc7	obchod
Elder-Beerman	Elder-Beerman	k1gMnSc1	Elder-Beerman
a	a	k8xC	a
Gabriel	Gabriel	k1gMnSc1	Gabriel
Brothers	Brothersa	k1gFnPc2	Brothersa
<g/>
.	.	kIx.	.
</s>
<s>
Třemi	tři	k4xCgFnPc7	tři
hlavními	hlavní	k2eAgFnPc7d1	hlavní
nákupními	nákupní	k2eAgFnPc7d1	nákupní
oblastmi	oblast	k1gFnPc7	oblast
v	v	k7c6	v
Southridgi	Southridg	k1gFnSc6	Southridg
jsou	být	k5eAaImIp3nP	být
Southridge	Southridg	k1gInPc1	Southridg
Centre	centr	k1gInSc5	centr
<g/>
,	,	kIx,	,
Dudley	Dudleum	k1gNnPc7	Dudleum
Farms	Farms	k1gInSc1	Farms
Plaza	plaz	k1gMnSc2	plaz
a	a	k8xC	a
The	The	k1gMnSc2	The
Shops	Shopsa	k1gFnPc2	Shopsa
at	at	k?	at
Trace	Trace	k1gMnSc1	Trace
Fork	Fork	k1gMnSc1	Fork
<g/>
.	.	kIx.	.
</s>
<s>
Charleston	charleston	k1gInSc1	charleston
má	mít	k5eAaImIp3nS	mít
dvoje	dvoje	k4xRgFnPc4	dvoje
hlavní	hlavní	k2eAgFnPc4d1	hlavní
noviny	novina	k1gFnPc4	novina
<g/>
.	.	kIx.	.
</s>
<s>
Charleston	charleston	k1gInSc1	charleston
Gazette	Gazett	k1gInSc5	Gazett
jsou	být	k5eAaImIp3nP	být
noviny	novina	k1gFnPc4	novina
s	s	k7c7	s
největším	veliký	k2eAgInSc7d3	veliký
nákladem	náklad	k1gInSc7	náklad
v	v	k7c6	v
Západní	západní	k2eAgMnPc1d1	západní
Virgini	Virgin	k1gMnPc1	Virgin
a	a	k8xC	a
vycházejí	vycházet	k5eAaImIp3nP	vycházet
od	od	k7c2	od
pondělka	pondělek	k1gInSc2	pondělek
do	do	k7c2	do
pátku	pátek	k1gInSc2	pátek
ráno	ráno	k6eAd1	ráno
<g/>
.	.	kIx.	.
</s>
<s>
Charleston	charleston	k1gInSc4	charleston
Daily	Daila	k1gFnSc2	Daila
Mail	mail	k1gInSc1	mail
vychází	vycházet	k5eAaImIp3nS	vycházet
ve	v	k7c4	v
všední	všední	k2eAgInPc4d1	všední
dny	den	k1gInPc4	den
odpoledne	odpoledne	k1gNnSc2	odpoledne
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
a	a	k8xC	a
neděli	neděle	k1gFnSc4	neděle
Charleston	charleston	k1gInSc1	charleston
Gazette	Gazett	k1gInSc5	Gazett
a	a	k8xC	a
Daily	Dail	k1gInPc4	Dail
Mail	mail	k1gInSc1	mail
spoluvytváří	spoluvytvářet	k5eAaImIp3nS	spoluvytvářet
noviny	novina	k1gFnPc1	novina
nazvané	nazvaný	k2eAgFnPc1d1	nazvaná
Charleston	charleston	k1gInSc4	charleston
Gazette-Mail	Gazette-Maila	k1gFnPc2	Gazette-Maila
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Charlestonu	charleston	k1gInSc6	charleston
existuje	existovat	k5eAaImIp3nS	existovat
celkem	celkem	k6eAd1	celkem
11	[number]	k4	11
stanic	stanice	k1gFnPc2	stanice
s	s	k7c7	s
městskou	městský	k2eAgFnSc7d1	městská
licencí	licence	k1gFnSc7	licence
(	(	kIx(	(
<g/>
AM	AM	kA	AM
a	a	k8xC	a
FM	FM	kA	FM
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
buď	buď	k8xC	buď
West	West	k1gInSc1	West
Virginia	Virginium	k1gNnSc2	Virginium
Radio	radio	k1gNnSc1	radio
Corporation	Corporation	k1gInSc1	Corporation
nebo	nebo	k8xC	nebo
Bristol	Bristol	k1gInSc1	Bristol
Broadcasting	Broadcasting	k1gInSc1	Broadcasting
Company	Compana	k1gFnSc2	Compana
<g/>
.	.	kIx.	.
</s>
<s>
Jon	Jon	k?	Jon
Andrew	Andrew	k1gMnSc1	Andrew
McBride	McBrid	k1gInSc5	McBrid
(	(	kIx(	(
<g/>
*	*	kIx~	*
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pilot	pilot	k1gMnSc1	pilot
a	a	k8xC	a
astronaut	astronaut	k1gMnSc1	astronaut
Jennifer	Jennifer	k1gMnSc1	Jennifer
Garnerová	Garnerová	k1gFnSc1	Garnerová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Banská	banský	k2eAgFnSc1d1	Banská
Bystrica	Bystrica	k1gFnSc1	Bystrica
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Charleston	charleston	k1gInSc1	charleston
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
