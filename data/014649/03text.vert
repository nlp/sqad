<s>
Statek	statek	k1gInSc1
(	(	kIx(
<g/>
zemědělství	zemědělství	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Na	na	k7c4
tento	tento	k3xDgInSc4
článek	článek	k1gInSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Farma	farma	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Farma	farma	k1gFnSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Statek	statek	k1gInSc1
v	v	k7c6
Maďarsku	Maďarsko	k1gNnSc6
</s>
<s>
Statek	statek	k1gInSc1
(	(	kIx(
<g/>
cizím	cizit	k5eAaImIp1nS
slovem	slovo	k1gNnSc7
farma	farma	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
základní	základní	k2eAgFnSc7d1
hospodářskou	hospodářský	k2eAgFnSc7d1
jednotkou	jednotka	k1gFnSc7
v	v	k7c6
zemědělství	zemědělství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
v	v	k7c6
něm	on	k3xPp3gNnSc6
umístěn	umístěn	k2eAgInSc1d1
chov	chov	k1gInSc1
hospodářských	hospodářský	k2eAgNnPc2d1
zvířat	zvíře	k1gNnPc2
a	a	k8xC
v	v	k7c6
okolí	okolí	k1gNnSc6
statku	statek	k1gInSc2
se	se	k3xPyFc4
rozprostírají	rozprostírat	k5eAaImIp3nP
ohraničené	ohraničený	k2eAgInPc1d1
pozemky	pozemek	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1
slouží	sloužit	k5eAaImIp3nP
k	k	k7c3
výběhu	výběh	k1gInSc3
dobytka	dobytek	k1gInSc2
a	a	k8xC
pěstování	pěstování	k1gNnSc4
plodin	plodina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
historické	historický	k2eAgFnSc6d1
době	doba	k1gFnSc6
patřil	patřit	k5eAaImAgInS
statek	statek	k1gInSc1
sedlákovi	sedlák	k1gMnSc3
obhospodařujícímu	obhospodařující	k2eAgInSc3d1
lán	lán	k1gInSc1
polí	pole	k1gFnPc2
(	(	kIx(
<g/>
láník	láník	k1gMnSc1
<g/>
,	,	kIx,
der	drát	k5eAaImRp2nS
Laaniger	Laaniger	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
drobnější	drobný	k2eAgMnPc1d2
zemědělci	zemědělec	k1gMnPc1
obývali	obývat	k5eAaImAgMnP
chalupu	chalupa	k1gFnSc4
(	(	kIx(
<g/>
chalupníci	chalupník	k1gMnPc1
<g/>
,	,	kIx,
zahradníci	zahradník	k1gMnPc1
a	a	k8xC
domkáři	domkář	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Rostlinná	rostlinný	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
</s>
<s>
Rostlinná	rostlinný	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
představuje	představovat	k5eAaImIp3nS
někdy	někdy	k6eAd1
hlavní	hlavní	k2eAgFnSc4d1
část	část	k1gFnSc4
hospodaření	hospodaření	k1gNnSc2
zemědělského	zemědělský	k2eAgInSc2d1
statku	statek	k1gInSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
je	být	k5eAaImIp3nS
jen	jen	k9
doplňkem	doplněk	k1gInSc7
živočišné	živočišný	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
obdělávání	obdělávání	k1gNnSc6
půdy	půda	k1gFnSc2
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
zemědělské	zemědělský	k2eAgInPc1d1
stroje	stroj	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Živočišná	živočišný	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
</s>
<s>
V	v	k7c6
zemědělských	zemědělský	k2eAgInPc6d1
podnicích	podnik	k1gInPc6
(	(	kIx(
<g/>
družstvech	družstvo	k1gNnPc6
<g/>
)	)	kIx)
se	se	k3xPyFc4
zaměřením	zaměření	k1gNnSc7
na	na	k7c4
živočišnou	živočišný	k2eAgFnSc4d1
produkci	produkce	k1gFnSc4
se	se	k3xPyFc4
chovají	chovat	k5eAaImIp3nP
zvířata	zvíře	k1gNnPc1
jednoho	jeden	k4xCgMnSc2
druhu	druh	k1gInSc2
(	(	kIx(
<g/>
zpravidla	zpravidla	k6eAd1
i	i	k9
stejného	stejný	k2eAgNnSc2d1
plemene	plemeno	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
v	v	k7c6
rámci	rámec	k1gInSc6
podniku	podnik	k1gInSc2
chová	chovat	k5eAaImIp3nS
více	hodně	k6eAd2
druhů	druh	k1gInPc2
zvířat	zvíře	k1gNnPc2
<g/>
,	,	kIx,
děje	dít	k5eAaImIp3nS
se	se	k3xPyFc4
to	ten	k3xDgNnSc1
obvykle	obvykle	k6eAd1
ve	v	k7c4
vícero	vícero	k1gNnSc4
budovách	budova	k1gFnPc6
(	(	kIx(
<g/>
jako	jako	k8xC,k8xS
drůbežárna	drůbežárna	k1gFnSc1
<g/>
,	,	kIx,
vepřín	vepřín	k1gInSc1
<g/>
,	,	kIx,
kravín	kravín	k1gInSc1
<g/>
,	,	kIx,
hřebčín	hřebčín	k1gInSc1
a	a	k8xC
podobně	podobně	k6eAd1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
většinou	většinou	k6eAd1
ve	v	k7c6
specializovaných	specializovaný	k2eAgInPc6d1
chovech	chov	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
mechanizaci	mechanizace	k1gFnSc6
živočišné	živočišný	k2eAgFnSc2d1
výroby	výroba	k1gFnSc2
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
dopravníky	dopravník	k1gInPc1
<g/>
,	,	kIx,
dojírny	dojírna	k1gFnPc1
a	a	k8xC
krmné	krmný	k2eAgInPc1d1
vozy	vůz	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Dojící	dojící	k2eAgNnSc1d1
zařízení	zařízení	k1gNnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Farma	farma	k1gFnSc1
(	(	kIx(
<g/>
gazdovstvo	gazdovstvo	k1gNnSc1
<g/>
)	)	kIx)
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
statek	statek	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
statek	statek	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4004770-2	4004770-2	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85047299	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85047299	#num#	k4
</s>
