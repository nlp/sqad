<s desamb="1">
Je	být	k5eAaImIp3nS
v	v	k7c6
něm	on	k3xPp3gNnSc6
umístěn	umístěn	k2eAgInSc1d1
chov	chov	k1gInSc1
hospodářských	hospodářský	k2eAgNnPc2d1
zvířat	zvíře	k1gNnPc2
a	a	k8xC
v	v	k7c6
okolí	okolí	k1gNnSc6
statku	statek	k1gInSc2
se	se	k3xPyFc4
rozprostírají	rozprostírat	k5eAaImIp3nP
ohraničené	ohraničený	k2eAgInPc1d1
pozemky	pozemek	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1
slouží	sloužit	k5eAaImIp3nP
k	k	k7c3
výběhu	výběh	k1gInSc3
dobytka	dobytek	k1gInSc2
a	a	k8xC
pěstování	pěstování	k1gNnSc4
plodin	plodina	k1gFnPc2
<g/>
.	.	kIx.
</s>