<s>
Malmbanan	Malmbanan	k1gInSc1	Malmbanan
-	-	kIx~	-
v	v	k7c6	v
norském	norský	k2eAgInSc6d1	norský
úseku	úsek	k1gInSc6	úsek
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
Ofotbanen	Ofotbanen	k1gInSc1	Ofotbanen
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
město	město	k1gNnSc4	město
Luleå	Luleå	k1gFnSc2	Luleå
na	na	k7c6	na
švédském	švédský	k2eAgNnSc6d1	švédské
pobřeží	pobřeží	k1gNnSc6	pobřeží
Botnického	botnický	k2eAgInSc2d1	botnický
zálivu	záliv	k1gInSc2	záliv
s	s	k7c7	s
norským	norský	k2eAgInSc7d1	norský
přístavem	přístav	k1gInSc7	přístav
Narvik	Narvika	k1gFnPc2	Narvika
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
Norského	norský	k2eAgNnSc2d1	norské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
podle	podle	k7c2	podle
slova	slovo	k1gNnSc2	slovo
"	"	kIx"	"
<g/>
malm	malm	k1gInSc1	malm
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ve	v	k7c6	v
švédštině	švédština	k1gFnSc6	švédština
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
železná	železný	k2eAgFnSc1d1	železná
ruda	ruda	k1gFnSc1	ruda
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Norské	norský	k2eAgNnSc1d1	norské
pojmenování	pojmenování	k1gNnSc1	pojmenování
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
podle	podle	k7c2	podle
fjordu	fjord	k1gInSc2	fjord
Ofotfjorden	Ofotfjordna	k1gFnPc2	Ofotfjordna
<g/>
,	,	kIx,	,
podél	podél	k7c2	podél
kterého	který	k3yQgNnSc2	který
vede	vést	k5eAaImIp3nS	vést
tato	tento	k3xDgFnSc1	tento
železnice	železnice	k1gFnSc1	železnice
do	do	k7c2	do
Narviku	Narvik	k1gInSc2	Narvik
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
železnice	železnice	k1gFnSc2	železnice
činí	činit	k5eAaImIp3nS	činit
474	[number]	k4	474
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
trati	trať	k1gFnSc2	trať
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1884	[number]	k4	1884
a	a	k8xC	a
první	první	k4xOgMnSc1	první
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1886	[number]	k4	1886
dokončen	dokončit	k5eAaPmNgInS	dokončit
úsek	úsek	k1gInSc1	úsek
Luleå	Luleå	k1gFnSc2	Luleå
–	–	k?	–
Boden	bůst	k5eAaImNgInS	bůst
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1887	[number]	k4	1887
byla	být	k5eAaImAgFnS	být
železnice	železnice	k1gFnSc1	železnice
prodloužena	prodloužit	k5eAaPmNgFnS	prodloužit
až	až	k6eAd1	až
do	do	k7c2	do
Gällivare	Gällivar	k1gInSc5	Gällivar
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
vlak	vlak	k1gInSc1	vlak
s	s	k7c7	s
železnou	železný	k2eAgFnSc7d1	železná
rudou	ruda	k1gFnSc7	ruda
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
na	na	k7c4	na
trasu	trasa	k1gFnSc4	trasa
Gällivare	Gällivar	k1gInSc5	Gällivar
-	-	kIx~	-
Luleå	Luleå	k1gFnPc2	Luleå
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1888	[number]	k4	1888
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
provoz	provoz	k1gInSc1	provoz
zastaven	zastavit	k5eAaPmNgInS	zastavit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
trať	trať	k1gFnSc1	trať
byla	být	k5eAaImAgFnS	být
poškozena	poškodit	k5eAaPmNgFnS	poškodit
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
tání	tání	k1gNnSc2	tání
sněhu	sníh	k1gInSc2	sníh
a	a	k8xC	a
rozmrzání	rozmrzání	k1gNnSc2	rozmrzání
spodku	spodek	k1gInSc2	spodek
<g/>
.	.	kIx.	.
</s>
<s>
Oprava	oprava	k1gFnSc1	oprava
poškozené	poškozený	k2eAgFnSc2d1	poškozená
trati	trať	k1gFnSc2	trať
se	se	k3xPyFc4	se
protáhla	protáhnout	k5eAaPmAgFnS	protáhnout
a	a	k8xC	a
provoz	provoz	k1gInSc1	provoz
byl	být	k5eAaImAgInS	být
obnoven	obnovit	k5eAaPmNgInS	obnovit
až	až	k9	až
23	[number]	k4	23
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1892	[number]	k4	1892
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
dalších	další	k2eAgInPc2d1	další
úseků	úsek	k1gInPc2	úsek
se	se	k3xPyFc4	se
rozeběhla	rozeběhnout	k5eAaPmAgFnS	rozeběhnout
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
<g/>
.	.	kIx.	.
</s>
<s>
Úsek	úsek	k1gInSc1	úsek
Gällivare	Gällivar	k1gInSc5	Gällivar
–	–	k?	–
Kiruna	Kiruna	k1gFnSc1	Kiruna
byl	být	k5eAaImAgInS	být
dokončen	dokončen	k2eAgInSc1d1	dokončen
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
roku	rok	k1gInSc2	rok
1899	[number]	k4	1899
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
osobní	osobní	k2eAgInSc1d1	osobní
vlak	vlak	k1gInSc1	vlak
do	do	k7c2	do
Narviku	Narvik	k1gInSc2	Narvik
dorazil	dorazit	k5eAaPmAgInS	dorazit
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1902	[number]	k4	1902
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
byla	být	k5eAaImAgFnS	být
trať	trať	k1gFnSc1	trať
otevřena	otevřen	k2eAgFnSc1d1	otevřena
králem	král	k1gMnSc7	král
Oskarem	Oskar	k1gMnSc7	Oskar
II	II	kA	II
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1903	[number]	k4	1903
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
celé	celý	k2eAgFnSc6d1	celá
trati	trať	k1gFnSc6	trať
byl	být	k5eAaImAgInS	být
nejdříve	dříve	k6eAd3	dříve
parní	parní	k2eAgInSc1d1	parní
provoz	provoz	k1gInSc1	provoz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
pohraničním	pohraniční	k2eAgInSc6d1	pohraniční
horském	horský	k2eAgInSc6d1	horský
úseku	úsek	k1gInSc6	úsek
zvládaly	zvládat	k5eAaImAgFnP	zvládat
parní	parní	k2eAgFnPc1d1	parní
lokomotivy	lokomotiva	k1gFnPc1	lokomotiva
vozbu	vozba	k1gFnSc4	vozba
těžkých	těžký	k2eAgInPc2d1	těžký
rudných	rudný	k2eAgInPc2d1	rudný
vlaků	vlak	k1gInPc2	vlak
jen	jen	k9	jen
s	s	k7c7	s
velkými	velký	k2eAgFnPc7d1	velká
obtížemi	obtíž	k1gFnPc7	obtíž
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
švédské	švédský	k2eAgFnPc1d1	švédská
státní	státní	k2eAgFnPc1d1	státní
dráhy	dráha	k1gFnPc1	dráha
SJ	SJ	kA	SJ
zahájily	zahájit	k5eAaPmAgFnP	zahájit
elektrifikaci	elektrifikace	k1gFnSc4	elektrifikace
nejnáročnějšího	náročný	k2eAgInSc2d3	nejnáročnější
úseku	úsek	k1gInSc2	úsek
Kiruna	Kiruna	k1gFnSc1	Kiruna
–	–	k?	–
Riksgränsen	Riksgränsen	k1gInSc1	Riksgränsen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
úseku	úsek	k1gInSc6	úsek
elektrifikovaném	elektrifikovaný	k2eAgInSc6d1	elektrifikovaný
napájecí	napájecí	k2eAgInSc4d1	napájecí
soustavou	soustava	k1gFnSc7	soustava
16	[number]	k4	16
kV	kV	k?	kV
15	[number]	k4	15
Hz	Hz	kA	Hz
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
upravena	upraven	k2eAgFnSc1d1	upravena
16	[number]	k4	16
kV	kV	k?	kV
a	a	k8xC	a
16	[number]	k4	16
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
Hz	Hz	kA	Hz
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
elektrický	elektrický	k2eAgInSc1d1	elektrický
provoz	provoz	k1gInSc1	provoz
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
a	a	k8xC	a
během	během	k7c2	během
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
postupně	postupně	k6eAd1	postupně
elektrifikována	elektrifikován	k2eAgFnSc1d1	elektrifikována
celá	celý	k2eAgFnSc1d1	celá
trať	trať	k1gFnSc1	trať
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byly	být	k5eAaImAgFnP	být
provozovatelem	provozovatel	k1gMnSc7	provozovatel
dráhy	dráha	k1gFnPc1	dráha
jednotlivé	jednotlivý	k2eAgFnSc2d1	jednotlivá
unitární	unitární	k2eAgFnSc2d1	unitární
státní	státní	k2eAgFnSc2d1	státní
železnice	železnice	k1gFnSc2	železnice
<g/>
:	:	kIx,	:
Statens	Statens	k1gInSc1	Statens
Järnvägar	Järnvägar	k1gInSc1	Järnvägar
(	(	kIx(	(
<g/>
SJ	SJ	kA	SJ
<g/>
)	)	kIx)	)
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
a	a	k8xC	a
Norges	Norges	k1gMnSc1	Norges
Statsbaner	Statsbaner	k1gMnSc1	Statsbaner
(	(	kIx(	(
<g/>
NSB	NSB	kA	NSB
<g/>
)	)	kIx)	)
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
<g/>
.	.	kIx.	.
</s>
<s>
Provozovatelem	provozovatel	k1gMnSc7	provozovatel
švédského	švédský	k2eAgInSc2d1	švédský
úseku	úsek	k1gInSc2	úsek
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
stala	stát	k5eAaPmAgFnS	stát
společnost	společnost	k1gFnSc1	společnost
Banverket	Banverketa	k1gFnPc2	Banverketa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
vyčleněním	vyčlenění	k1gNnSc7	vyčlenění
od	od	k7c2	od
SJ	SJ	kA	SJ
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2010	[number]	k4	2010
převzala	převzít	k5eAaPmAgFnS	převzít
její	její	k3xOp3gFnSc4	její
úlohu	úloha	k1gFnSc4	úloha
agentura	agentura	k1gFnSc1	agentura
Trafikverket	Trafikverketa	k1gFnPc2	Trafikverketa
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
je	být	k5eAaImIp3nS	být
provozovatelem	provozovatel	k1gMnSc7	provozovatel
norského	norský	k2eAgInSc2d1	norský
úseku	úsek	k1gInSc2	úsek
firma	firma	k1gFnSc1	firma
Jernbaneverket	Jernbaneverket	k1gInSc1	Jernbaneverket
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
společnosti	společnost	k1gFnPc1	společnost
jsou	být	k5eAaImIp3nP	být
odpovědné	odpovědný	k2eAgFnPc1d1	odpovědná
také	také	k9	také
za	za	k7c4	za
údržbu	údržba	k1gFnSc4	údržba
a	a	k8xC	a
rozvoj	rozvoj	k1gInSc4	rozvoj
této	tento	k3xDgFnSc2	tento
železnice	železnice	k1gFnSc2	železnice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
směřovaly	směřovat	k5eAaImAgFnP	směřovat
významné	významný	k2eAgFnPc1d1	významná
investice	investice	k1gFnPc1	investice
do	do	k7c2	do
zesilování	zesilování	k1gNnSc2	zesilování
železničního	železniční	k2eAgInSc2d1	železniční
spodku	spodek	k1gInSc2	spodek
a	a	k8xC	a
svršku	svršek	k1gInSc2	svršek
pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
hmotnosti	hmotnost	k1gFnSc2	hmotnost
na	na	k7c4	na
nápravu	náprava	k1gFnSc4	náprava
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
by	by	kYmCp3nS	by
tak	tak	k9	tak
měla	mít	k5eAaImAgFnS	mít
celá	celý	k2eAgFnSc1d1	celá
trať	trať	k1gFnSc1	trať
zvládat	zvládat	k5eAaImF	zvládat
provoz	provoz	k1gInSc4	provoz
vozidel	vozidlo	k1gNnPc2	vozidlo
s	s	k7c7	s
nápravovým	nápravový	k2eAgInSc7d1	nápravový
tlakem	tlak	k1gInSc7	tlak
30	[number]	k4	30
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
bylo	být	k5eAaImAgNnS	být
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
provozovatelem	provozovatel	k1gMnSc7	provozovatel
drážní	drážní	k2eAgFnSc2d1	drážní
dopravy	doprava	k1gFnSc2	doprava
na	na	k7c6	na
území	území	k1gNnSc6	území
daného	daný	k2eAgInSc2d1	daný
státu	stát	k1gInSc2	stát
byla	být	k5eAaImAgFnS	být
vždy	vždy	k6eAd1	vždy
státní	státní	k2eAgFnSc1d1	státní
železnice	železnice	k1gFnSc1	železnice
a	a	k8xC	a
nejinak	nejinak	k6eAd1	nejinak
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
na	na	k7c4	na
Malmbanan	Malmbanan	k1gInSc4	Malmbanan
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rychlou	rychlý	k2eAgFnSc7d1	rychlá
liberalizací	liberalizace	k1gFnSc7	liberalizace
železniční	železniční	k2eAgFnSc2d1	železniční
dopravy	doprava	k1gFnSc2	doprava
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
se	se	k3xPyFc4	se
však	však	k9	však
situace	situace	k1gFnSc1	situace
začala	začít	k5eAaPmAgFnS	začít
měnit	měnit	k5eAaImF	měnit
a	a	k8xC	a
dnes	dnes	k6eAd1	dnes
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
trati	trať	k1gFnSc6	trať
narazíme	narazit	k5eAaPmIp1nP	narazit
na	na	k7c4	na
nákladní	nákladní	k2eAgInPc4d1	nákladní
vlaky	vlak	k1gInPc4	vlak
několika	několik	k4yIc2	několik
dopravců	dopravce	k1gMnPc2	dopravce
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
vlaků	vlak	k1gInPc2	vlak
zde	zde	k6eAd1	zde
provozuje	provozovat	k5eAaImIp3nS	provozovat
společnost	společnost	k1gFnSc1	společnost
MTAB	MTAB	kA	MTAB
<g/>
/	/	kIx~	/
<g/>
MTAS	MTAS	kA	MTAS
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
přepravuje	přepravovat	k5eAaImIp3nS	přepravovat
především	především	k6eAd1	především
železnou	železný	k2eAgFnSc4d1	železná
rudu	ruda	k1gFnSc4	ruda
do	do	k7c2	do
přístavů	přístav	k1gInPc2	přístav
na	na	k7c6	na
obou	dva	k4xCgInPc6	dva
koncích	konec	k1gInPc6	konec
trati	trať	k1gFnSc2	trať
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgMnPc4d1	další
významné	významný	k2eAgMnPc4d1	významný
dopravce	dopravce	k1gMnPc4	dopravce
patří	patřit	k5eAaImIp3nS	patřit
společnosti	společnost	k1gFnSc3	společnost
Green	Green	k2eAgMnSc1d1	Green
Cargo	Cargo	k1gMnSc1	Cargo
AB	AB	kA	AB
(	(	kIx(	(
<g/>
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
rozdělením	rozdělení	k1gNnSc7	rozdělení
SJ	SJ	kA	SJ
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
CargoNet	CargoNet	k1gInSc1	CargoNet
AS	as	k9	as
(	(	kIx(	(
<g/>
společný	společný	k2eAgInSc1d1	společný
podnik	podnik	k1gInSc1	podnik
NSB	NSB	kA	NSB
a	a	k8xC	a
Green	Green	k2eAgMnSc1d1	Green
Cargo	Cargo	k1gMnSc1	Cargo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ofotbanen	Ofotbanen	k2eAgInSc1d1	Ofotbanen
AS	as	k1gInSc1	as
či	či	k8xC	či
TGOJ	TGOJ	kA	TGOJ
Trafik	trafika	k1gFnPc2	trafika
AB	AB	kA	AB
<g/>
.	.	kIx.	.
</s>
<s>
Dopravu	doprava	k1gFnSc4	doprava
osobních	osobní	k2eAgInPc2d1	osobní
vlaků	vlak	k1gInPc2	vlak
převzala	převzít	k5eAaPmAgFnS	převzít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
od	od	k7c2	od
státních	státní	k2eAgFnPc2d1	státní
drah	draha	k1gFnPc2	draha
společnost	společnost	k1gFnSc1	společnost
Svenska	Svensko	k1gNnSc2	Svensko
Tå	Tå	k1gFnSc2	Tå
AB	AB	kA	AB
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
provozovala	provozovat	k5eAaImAgFnS	provozovat
je	být	k5eAaImIp3nS	být
jen	jen	k6eAd1	jen
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gMnSc7	její
nástupcem	nástupce	k1gMnSc7	nástupce
je	být	k5eAaImIp3nS	být
dopravce	dopravce	k1gMnSc1	dopravce
Connex	Connex	k1gInSc4	Connex
Sverige	Sverig	k1gInSc2	Sverig
AB	AB	kA	AB
(	(	kIx(	(
<g/>
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
změnila	změnit	k5eAaPmAgFnS	změnit
název	název	k1gInSc4	název
na	na	k7c4	na
Veolia	Veolium	k1gNnPc4	Veolium
Transport	transport	k1gInSc1	transport
Sverige	Sverig	k1gInSc2	Sverig
AB	AB	kA	AB
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Osobní	osobní	k2eAgInPc4d1	osobní
vlaky	vlak	k1gInPc4	vlak
této	tento	k3xDgFnSc2	tento
firmy	firma	k1gFnSc2	firma
jezdí	jezdit	k5eAaImIp3nP	jezdit
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
trati	trať	k1gFnSc6	trať
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
norské	norský	k2eAgFnSc6d1	norská
straně	strana	k1gFnSc6	strana
je	být	k5eAaImIp3nS	být
oficiálně	oficiálně	k6eAd1	oficiálně
dopravcem	dopravce	k1gMnSc7	dopravce
společnost	společnost	k1gFnSc4	společnost
Ofotbanen	Ofotbanen	k2eAgInSc4d1	Ofotbanen
AS	as	k1gInSc4	as
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
firma	firma	k1gFnSc1	firma
pak	pak	k6eAd1	pak
také	také	k9	také
provozuje	provozovat	k5eAaImIp3nS	provozovat
charterové	charterový	k2eAgInPc4d1	charterový
osobní	osobní	k2eAgInPc4d1	osobní
vlaky	vlak	k1gInPc4	vlak
z	z	k7c2	z
Narviku	Narvik	k1gInSc2	Narvik
podél	podél	k7c2	podél
fjordu	fjord	k1gInSc2	fjord
Ofotfjorden	Ofotfjordna	k1gFnPc2	Ofotfjordna
do	do	k7c2	do
pohraniční	pohraniční	k2eAgFnSc2d1	pohraniční
stanice	stanice	k1gFnSc2	stanice
Riksgränsen	Riksgränsna	k1gFnPc2	Riksgränsna
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
Štefek	Štefek	k1gMnSc1	Štefek
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Malmbanan	Malmbanan	k1gInSc1	Malmbanan
po	po	k7c6	po
deseti	deset	k4xCc6	deset
letech	let	k1gInPc6	let
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
časopis	časopis	k1gInSc1	časopis
Dráha	dráha	k1gFnSc1	dráha
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2007	[number]	k4	2007
</s>
