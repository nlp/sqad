<s>
Malmbanan	Malmbanan	k1gInSc1	Malmbanan
-	-	kIx~	-
v	v	k7c6	v
norském	norský	k2eAgInSc6d1	norský
úseku	úsek	k1gInSc6	úsek
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
Ofotbanen	Ofotbanen	k1gInSc1	Ofotbanen
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
město	město	k1gNnSc4	město
Luleå	Luleå	k1gFnSc2	Luleå
na	na	k7c6	na
švédském	švédský	k2eAgNnSc6d1	švédské
pobřeží	pobřeží	k1gNnSc6	pobřeží
Botnického	botnický	k2eAgInSc2d1	botnický
zálivu	záliv	k1gInSc2	záliv
s	s	k7c7	s
norským	norský	k2eAgInSc7d1	norský
přístavem	přístav	k1gInSc7	přístav
Narvik	Narvika	k1gFnPc2	Narvika
u	u	k7c2	u
pobřeží	pobřeží	k1gNnSc2	pobřeží
Norského	norský	k2eAgNnSc2d1	norské
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
