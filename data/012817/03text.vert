<p>
<s>
Agronomie	agronomie	k1gFnSc1	agronomie
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc4	soubor
vědních	vědní	k2eAgInPc2d1	vědní
oborů	obor	k1gInPc2	obor
zkoumajících	zkoumající	k2eAgFnPc2d1	zkoumající
zemědělskou	zemědělský	k2eAgFnSc4d1	zemědělská
výrobu	výroba	k1gFnSc4	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělské	zemědělský	k2eAgFnPc1d1	zemědělská
vědy	věda	k1gFnPc1	věda
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
vyčlenily	vyčlenit	k5eAaPmAgInP	vyčlenit
z	z	k7c2	z
biologických	biologický	k2eAgFnPc2d1	biologická
věd	věda	k1gFnPc2	věda
pro	pro	k7c4	pro
aplikaci	aplikace	k1gFnSc4	aplikace
poznatků	poznatek	k1gInPc2	poznatek
v	v	k7c4	v
pěstování	pěstování	k1gNnSc4	pěstování
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
chovu	chov	k1gInSc2	chov
hospodářských	hospodářský	k2eAgNnPc2d1	hospodářské
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obory	obora	k1gFnPc1	obora
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
věd	věda	k1gFnPc2	věda
mají	mít	k5eAaImIp3nP	mít
částečně	částečně	k6eAd1	částečně
charakter	charakter	k1gInSc4	charakter
specializovaných	specializovaný	k2eAgInPc2d1	specializovaný
přírodovědných	přírodovědný	k2eAgInPc2d1	přírodovědný
oborů	obor	k1gInPc2	obor
(	(	kIx(	(
<g/>
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
botanika	botanika	k1gFnSc1	botanika
<g/>
,	,	kIx,	,
agrochemie	agrochemie	k1gFnSc1	agrochemie
<g/>
,	,	kIx,	,
zoologie	zoologie	k1gFnSc1	zoologie
hospodářských	hospodářský	k2eAgNnPc2d1	hospodářské
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
technologických	technologický	k2eAgInPc2d1	technologický
oborů	obor	k1gInPc2	obor
(	(	kIx(	(
<g/>
šlechtitelství	šlechtitelství	k1gNnSc1	šlechtitelství
<g/>
,	,	kIx,	,
ochrana	ochrana	k1gFnSc1	ochrana
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
meliorace	meliorace	k1gFnSc2	meliorace
<g/>
,	,	kIx,	,
zemědělské	zemědělský	k2eAgInPc4d1	zemědělský
stroje	stroj	k1gInPc4	stroj
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
lékařských	lékařský	k2eAgFnPc2d1	lékařská
věd	věda	k1gFnPc2	věda
(	(	kIx(	(
<g/>
veterinární	veterinární	k2eAgNnSc1d1	veterinární
lékařství	lékařství	k1gNnSc1	lékařství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vznik	vznik	k1gInSc1	vznik
středoevropských	středoevropský	k2eAgFnPc2d1	středoevropská
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
věd	věda	k1gFnPc2	věda
je	být	k5eAaImIp3nS	být
úzce	úzko	k6eAd1	úzko
spojen	spojen	k2eAgInSc1d1	spojen
s	s	k7c7	s
antickou	antický	k2eAgFnSc7d1	antická
zemědělskou	zemědělský	k2eAgFnSc7d1	zemědělská
naukou	nauka	k1gFnSc7	nauka
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yQgFnSc3	který
přispěly	přispět	k5eAaPmAgFnP	přispět
zejména	zejména	k9	zejména
Hésiodos	Hésiodos	k1gMnSc1	Hésiodos
<g/>
,	,	kIx,	,
Platón	Platón	k1gMnSc1	Platón
<g/>
,	,	kIx,	,
Aristotelés	Aristotelés	k1gInSc1	Aristotelés
<g/>
,	,	kIx,	,
Teofrastos	Teofrastos	k1gInSc1	Teofrastos
<g/>
,	,	kIx,	,
Xenofón	Xenofón	k1gInSc1	Xenofón
<g/>
,	,	kIx,	,
Cato	Cata	k1gFnSc5	Cata
<g/>
,	,	kIx,	,
Varro	Varra	k1gFnSc5	Varra
<g/>
,	,	kIx,	,
Vergilius	Vergilius	k1gMnSc1	Vergilius
<g/>
,	,	kIx,	,
Plinius	Plinius	k1gMnSc1	Plinius
a	a	k8xC	a
Columella	Columella	k1gMnSc1	Columella
Palladius	Palladius	k1gMnSc1	Palladius
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Agronómia	Agronómium	k1gNnSc2	Agronómium
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
agronomie	agronomie	k1gFnSc2	agronomie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Agronomie	agronomie	k1gFnSc2	agronomie
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Heslo	heslo	k1gNnSc4	heslo
v	v	k7c6	v
encyklopedii	encyklopedie	k1gFnSc6	encyklopedie
Britannica	Britannic	k1gInSc2	Britannic
</s>
</p>
