<s>
Genitální	genitální	k2eAgFnSc1d1	genitální
HPV	HPV	kA	HPV
infekce	infekce	k1gFnPc1	infekce
mají	mít	k5eAaImIp3nP	mít
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
odhadovanou	odhadovaný	k2eAgFnSc4d1	odhadovaná
prevalenci	prevalence	k1gFnSc4	prevalence
mezi	mezi	k7c7	mezi
10	[number]	k4	10
až	až	k9	až
20	[number]	k4	20
%	%	kIx~	%
a	a	k8xC	a
klinický	klinický	k2eAgInSc4d1	klinický
projev	projev	k1gInSc4	projev
u	u	k7c2	u
1	[number]	k4	1
%	%	kIx~	%
dospělé	dospělý	k2eAgFnSc2d1	dospělá
sexuálně	sexuálně	k6eAd1	sexuálně
aktivní	aktivní	k2eAgFnSc2d1	aktivní
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
