<s>
České	český	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
ve	v	k7c6
vzpírání	vzpírání	k1gNnSc6
</s>
<s>
Na	na	k7c6
této	tento	k3xDgFnSc6
stránce	stránka	k1gFnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
přehled	přehled	k1gInSc1
českých	český	k2eAgInPc2d1
rekordů	rekord	k1gInPc2
ve	v	k7c6
vzpírání	vzpírání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
svaz	svaz	k1gInSc1
vzpírání	vzpírání	k1gNnSc2
(	(	kIx(
<g/>
ČSV	ČSV	kA
<g/>
)	)	kIx)
eviduje	evidovat	k5eAaImIp3nS
kromě	kromě	k7c2
seniorských	seniorský	k2eAgInPc2d1
rekordů	rekord	k1gInPc2
také	také	k9
juniorské	juniorský	k2eAgInPc4d1
rekordy	rekord	k1gInPc4
(	(	kIx(
<g/>
do	do	k7c2
20	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
mládežnické	mládežnický	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
(	(	kIx(
<g/>
do	do	k7c2
17	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rekordy	rekord	k1gInPc1
starších	starší	k1gMnPc2
i	i	k8xC
mladších	mladý	k2eAgMnPc2d2
žáků	žák	k1gMnPc2
a	a	k8xC
rekordy	rekord	k1gInPc1
kategorií	kategorie	k1gFnPc2
masters	mastersa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rekordy	rekord	k1gInPc1
jsou	být	k5eAaImIp3nP
vedeny	vést	k5eAaImNgInP
v	v	k7c6
trhu	trh	k1gInSc6
<g/>
,	,	kIx,
nadhozu	nadhoz	k1gInSc6
a	a	k8xC
olympijském	olympijský	k2eAgInSc6d1
dvojboji	dvojboj	k1gInSc6
(	(	kIx(
<g/>
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
mladších	mladý	k2eAgMnPc2d2
žáků	žák	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
soutěží	soutěžit	k5eAaImIp3nP
ve	v	k7c6
čtyřboji	čtyřboj	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Seniorské	seniorský	k2eAgInPc1d1
české	český	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
–	–	k?
<g/>
)	)	kIx)
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2019	#num#	k4
jsou	být	k5eAaImIp3nP
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
pravidly	pravidlo	k1gNnPc7
Mezinárodní	mezinárodní	k2eAgFnSc2d1
vzpěračské	vzpěračský	k2eAgFnSc2d1
federace	federace	k1gFnSc2
vyhlášeny	vyhlášen	k2eAgFnPc4d1
nové	nový	k2eAgFnPc4d1
hmotnostní	hmotnostní	k2eAgFnPc4d1
kategorie	kategorie	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
přechodném	přechodný	k2eAgNnSc6d1
období	období	k1gNnSc6
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2019	#num#	k4
neexistovaly	existovat	k5eNaImAgInP
oficiální	oficiální	k2eAgInPc1d1
české	český	k2eAgInPc1d1
seniorské	seniorský	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgFnPc1
byly	být	k5eAaImAgFnP
vyhlášeny	vyhlásit	k5eAaPmNgFnP
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
květnu	květen	k1gInSc3
2019	#num#	k4
<g/>
,	,	kIx,
na	na	k7c6
základě	základ	k1gInSc6
nejlepších	dobrý	k2eAgInPc2d3
výkonů	výkon	k1gInPc2
dosažených	dosažený	k2eAgInPc2d1
od	od	k7c2
začátku	začátek	k1gInSc2
roku	rok	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Muži	muž	k1gMnPc1
</s>
<s>
Kategorie	kategorie	k1gFnPc1
</s>
<s>
Disciplína	disciplína	k1gFnSc1
</s>
<s>
Výkon	výkon	k1gInSc1
</s>
<s>
Držitel	držitel	k1gMnSc1
</s>
<s>
Datum	datum	k1gNnSc1
stanovení	stanovení	k1gNnSc2
</s>
<s>
Místo	místo	k7c2
stanovení	stanovení	k1gNnSc2
</s>
<s>
Do	do	k7c2
55	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
99	#num#	k4
kg	kg	kA
</s>
<s>
František	František	k1gMnSc1
Polák	Polák	k1gMnSc1
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
2019	#num#	k4
</s>
<s>
Bukurešť	Bukurešť	k1gFnSc1
<g/>
,	,	kIx,
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
127	#num#	k4
kg	kg	kA
</s>
<s>
František	František	k1gMnSc1
Polák	Polák	k1gMnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2021	#num#	k4
</s>
<s>
Moskva	Moskva	k1gFnSc1
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
224	#num#	k4
kg	kg	kA
</s>
<s>
František	František	k1gMnSc1
Polák	Polák	k1gMnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2021	#num#	k4
</s>
<s>
Moskva	Moskva	k1gFnSc1
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
</s>
<s>
Do	do	k7c2
61	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
107	#num#	k4
kg	kg	kA
</s>
<s>
František	František	k1gMnSc1
Polák	Polák	k1gMnSc1
</s>
<s>
27	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2019	#num#	k4
</s>
<s>
Sokolov	Sokolov	k1gInSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
135	#num#	k4
kg	kg	kA
</s>
<s>
František	František	k1gMnSc1
Polák	Polák	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2019	#num#	k4
</s>
<s>
Batumi	Batumi	k1gNnSc1
<g/>
,	,	kIx,
Gruzie	Gruzie	k1gFnSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
241	#num#	k4
kg	kg	kA
</s>
<s>
František	František	k1gMnSc1
Polák	Polák	k1gMnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2019	#num#	k4
</s>
<s>
Batumi	Batumi	k1gNnSc1
<g/>
,	,	kIx,
Gruzie	Gruzie	k1gFnSc1
</s>
<s>
Do	do	k7c2
67	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
132	#num#	k4
kg	kg	kA
</s>
<s>
Petr	Petr	k1gMnSc1
Petrov	Petrov	k1gInSc1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
2019	#num#	k4
</s>
<s>
Gaziantep	Gaziantep	k1gInSc1
<g/>
,	,	kIx,
Turecko	Turecko	k1gNnSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
165	#num#	k4
kg	kg	kA
</s>
<s>
Petr	Petr	k1gMnSc1
Petrov	Petrov	k1gInSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2021	#num#	k4
</s>
<s>
Moskva	Moskva	k1gFnSc1
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
294	#num#	k4
kg	kg	kA
</s>
<s>
Petr	Petr	k1gMnSc1
Petrov	Petrov	k1gInSc1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
2019	#num#	k4
</s>
<s>
Gaziantep	Gaziantep	k1gInSc1
<g/>
,	,	kIx,
Turecko	Turecko	k1gNnSc1
</s>
<s>
Do	do	k7c2
73	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
135	#num#	k4
kg	kg	kA
</s>
<s>
Petr	Petr	k1gMnSc1
Petrov	Petrov	k1gInSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
2019	#num#	k4
</s>
<s>
Coventry	Coventr	k1gInPc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
království	království	k1gNnSc4
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
171	#num#	k4
kg	kg	kA
</s>
<s>
Petr	Petr	k1gMnSc1
Petrov	Petrov	k1gInSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2019	#num#	k4
</s>
<s>
Batumi	Batumi	k1gNnSc1
<g/>
,	,	kIx,
Gruzie	Gruzie	k1gFnSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
304	#num#	k4
kg	kg	kA
</s>
<s>
Petr	Petr	k1gMnSc1
Petrov	Petrov	k1gInSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2019	#num#	k4
</s>
<s>
Batumi	Batumi	k1gNnSc1
<g/>
,	,	kIx,
Gruzie	Gruzie	k1gFnSc1
</s>
<s>
Do	do	k7c2
81	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
151	#num#	k4
kg	kg	kA
</s>
<s>
Petr	Petr	k1gMnSc1
Mareček	Mareček	k1gMnSc1
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
2019	#num#	k4
</s>
<s>
Boskovice	Boskovice	k1gInPc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
173	#num#	k4
kg	kg	kA
</s>
<s>
Petr	Petr	k1gMnSc1
Mareček	Mareček	k1gMnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2019	#num#	k4
</s>
<s>
Batumi	Batumi	k1gNnSc1
<g/>
,	,	kIx,
Gruzie	Gruzie	k1gFnSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
321	#num#	k4
kg	kg	kA
</s>
<s>
Petr	Petr	k1gMnSc1
Mareček	Mareček	k1gMnSc1
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
2019	#num#	k4
</s>
<s>
Boskovice	Boskovice	k1gInPc1
</s>
<s>
Do	do	k7c2
89	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
150	#num#	k4
kg	kg	kA
</s>
<s>
Petr	Petr	k1gMnSc1
Mareček	Mareček	k1gMnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
2019	#num#	k4
</s>
<s>
Holešov	Holešov	k1gInSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
180	#num#	k4
kg	kg	kA
</s>
<s>
Petr	Petr	k1gMnSc1
Mareček	Mareček	k1gMnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
2019	#num#	k4
</s>
<s>
Holešov	Holešov	k1gInSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
330	#num#	k4
kg	kg	kA
</s>
<s>
Petr	Petr	k1gMnSc1
Mareček	Mareček	k1gMnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
2019	#num#	k4
</s>
<s>
Holešov	Holešov	k1gInSc1
</s>
<s>
Do	do	k7c2
96	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
147	#num#	k4
kg	kg	kA
</s>
<s>
Josef	Josef	k1gMnSc1
Kolář	Kolář	k1gMnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
2020	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
187	#num#	k4
kg	kg	kA
</s>
<s>
Josef	Josef	k1gMnSc1
Kolář	Kolář	k1gMnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
2020	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
334	#num#	k4
kg	kg	kA
</s>
<s>
Josef	Josef	k1gMnSc1
Kolář	Kolář	k1gMnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
2020	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
</s>
<s>
Do	do	k7c2
102	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
156	#num#	k4
kg	kg	kA
</s>
<s>
Josef	Josef	k1gMnSc1
Kolář	Kolář	k1gMnSc1
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2021	#num#	k4
</s>
<s>
Havířov	Havířov	k1gInSc1
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
186	#num#	k4
kg	kg	kA
</s>
<s>
Josef	Josef	k1gMnSc1
Kolář	Kolář	k1gMnSc1
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2021	#num#	k4
</s>
<s>
Havířov	Havířov	k1gInSc1
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
342	#num#	k4
kg	kg	kA
</s>
<s>
Josef	Josef	k1gMnSc1
Kolář	Kolář	k1gMnSc1
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2021	#num#	k4
</s>
<s>
Havířov	Havířov	k1gInSc1
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
</s>
<s>
Do	do	k7c2
109	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
158	#num#	k4
kg	kg	kA
</s>
<s>
Patrik	Patrik	k1gMnSc1
Krywult	Krywult	k1gMnSc1
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
srpen	srpen	k1gInSc1
2019	#num#	k4
</s>
<s>
Míšeň	Míšeň	k1gFnSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
193	#num#	k4
kg	kg	kA
</s>
<s>
Jiří	Jiří	k1gMnSc1
Gasior	Gasior	k1gMnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
2020	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
339	#num#	k4
kg	kg	kA
</s>
<s>
Patrik	Patrik	k1gMnSc1
Krywult	Krywult	k1gMnSc1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2019	#num#	k4
</s>
<s>
Batumi	Batumi	k1gNnSc1
<g/>
,	,	kIx,
Gruzie	Gruzie	k1gFnSc1
</s>
<s>
Nad	nad	k7c7
109	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
182	#num#	k4
kg	kg	kA
</s>
<s>
Kamil	Kamil	k1gMnSc1
Kučera	Kučera	k1gMnSc1
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2019	#num#	k4
</s>
<s>
Batumi	Batumi	k1gNnSc1
<g/>
,	,	kIx,
Gruzie	Gruzie	k1gFnSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
241	#num#	k4
kg	kg	kA
</s>
<s>
Jiří	Jiří	k1gMnSc1
Orság	Orság	k1gMnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2020	#num#	k4
</s>
<s>
Cospicua	Cospicua	k1gFnSc1
<g/>
,	,	kIx,
Malta	Malta	k1gFnSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
418	#num#	k4
kg	kg	kA
</s>
<s>
Kamil	Kamil	k1gMnSc1
Kučera	Kučera	k1gMnSc1
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2019	#num#	k4
</s>
<s>
Batumi	Batumi	k1gNnSc1
<g/>
,	,	kIx,
Gruzie	Gruzie	k1gFnSc1
</s>
<s>
Ženy	žena	k1gFnPc1
</s>
<s>
Kategorie	kategorie	k1gFnPc1
</s>
<s>
Disciplína	disciplína	k1gFnSc1
</s>
<s>
Výkon	výkon	k1gInSc1
</s>
<s>
Držitelka	držitelka	k1gFnSc1
</s>
<s>
Datum	datum	k1gNnSc1
stanovení	stanovení	k1gNnSc2
</s>
<s>
Místo	místo	k7c2
stanovení	stanovení	k1gNnSc2
</s>
<s>
Do	do	k7c2
45	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
35	#num#	k4
kg	kg	kA
</s>
<s>
Lucie	Lucie	k1gFnSc1
Bariová	Bariový	k2eAgFnSc1d1
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
2019	#num#	k4
</s>
<s>
Sokolov	Sokolov	k1gInSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
50	#num#	k4
kg	kg	kA
</s>
<s>
Lucie	Lucie	k1gFnSc1
Bariová	Bariový	k2eAgFnSc1d1
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
2019	#num#	k4
</s>
<s>
Sokolov	Sokolov	k1gInSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
85	#num#	k4
kg	kg	kA
</s>
<s>
Lucie	Lucie	k1gFnSc1
Bariová	Bariový	k2eAgFnSc1d1
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
2019	#num#	k4
</s>
<s>
Sokolov	Sokolov	k1gInSc1
</s>
<s>
Do	do	k7c2
49	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
55	#num#	k4
kg	kg	kA
</s>
<s>
Dorota	Dorota	k1gFnSc1
Dziková	Dziková	k1gFnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
2019	#num#	k4
</s>
<s>
Olomouc	Olomouc	k1gFnSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
75	#num#	k4
kg	kg	kA
</s>
<s>
Dorota	Dorota	k1gFnSc1
Dziková	Dziková	k1gFnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
2019	#num#	k4
</s>
<s>
Olomouc	Olomouc	k1gFnSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
130	#num#	k4
kg	kg	kA
</s>
<s>
Dorota	Dorota	k1gFnSc1
Dziková	Dziková	k1gFnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
2019	#num#	k4
</s>
<s>
Olomouc	Olomouc	k1gFnSc1
</s>
<s>
Do	do	k7c2
55	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
70	#num#	k4
kg	kg	kA
</s>
<s>
Vendula	Vendula	k1gFnSc1
Šafratová	Šafratový	k2eAgFnSc1d1
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
2020	#num#	k4
</s>
<s>
Třinec	Třinec	k1gInSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
84	#num#	k4
kg	kg	kA
</s>
<s>
Vendula	Vendula	k1gFnSc1
Šafratová	Šafratový	k2eAgFnSc1d1
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
2020	#num#	k4
</s>
<s>
Třinec	Třinec	k1gInSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
154	#num#	k4
kg	kg	kA
</s>
<s>
Vendula	Vendula	k1gFnSc1
Šafratová	Šafratový	k2eAgFnSc1d1
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
2020	#num#	k4
</s>
<s>
Třinec	Třinec	k1gInSc1
</s>
<s>
Do	do	k7c2
59	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
72	#num#	k4
kg	kg	kA
</s>
<s>
Petra	Petra	k1gFnSc1
Žampová	Žampová	k1gFnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2020	#num#	k4
</s>
<s>
Olomouc	Olomouc	k1gFnSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
95	#num#	k4
kg	kg	kA
</s>
<s>
Petra	Petra	k1gFnSc1
Žampová	Žampová	k1gFnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2020	#num#	k4
</s>
<s>
Olomouc	Olomouc	k1gFnSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
167	#num#	k4
kg	kg	kA
</s>
<s>
Petra	Petra	k1gFnSc1
Žampová	Žampová	k1gFnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2020	#num#	k4
</s>
<s>
Olomouc	Olomouc	k1gFnSc1
</s>
<s>
Do	do	k7c2
64	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
80	#num#	k4
kg	kg	kA
</s>
<s>
Eliška	Eliška	k1gFnSc1
Pudivítrová	Pudivítrová	k1gFnSc1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
2019	#num#	k4
</s>
<s>
Holešov	Holešov	k1gInSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
102	#num#	k4
kg	kg	kA
</s>
<s>
Eliška	Eliška	k1gFnSc1
Pudivítrová	Pudivítrová	k1gFnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2020	#num#	k4
</s>
<s>
Olomouc	Olomouc	k1gFnSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
182	#num#	k4
kg	kg	kA
</s>
<s>
Eliška	Eliška	k1gFnSc1
Pudivítrová	Pudivítrová	k1gFnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2020	#num#	k4
</s>
<s>
Olomouc	Olomouc	k1gFnSc1
</s>
<s>
Do	do	k7c2
71	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
90	#num#	k4
kg	kg	kA
</s>
<s>
Simona	Simona	k1gFnSc1
Hertlová	Hertlová	k1gFnSc1
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
2019	#num#	k4
</s>
<s>
Olomouc	Olomouc	k1gFnSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
106	#num#	k4
kg	kg	kA
</s>
<s>
Soňa	Soňa	k1gFnSc1
Karásková	Karásková	k1gFnSc1
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
2019	#num#	k4
</s>
<s>
Havířov	Havířov	k1gInSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
193	#num#	k4
kg	kg	kA
</s>
<s>
Soňa	Soňa	k1gFnSc1
Karásková	Karásková	k1gFnSc1
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
2019	#num#	k4
</s>
<s>
Havířov	Havířov	k1gInSc1
</s>
<s>
Do	do	k7c2
76	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
89	#num#	k4
kg	kg	kA
</s>
<s>
Simona	Simona	k1gFnSc1
Hertlová	Hertlová	k1gFnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
2019	#num#	k4
</s>
<s>
Coventry	Coventr	k1gInPc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
království	království	k1gNnSc4
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
114	#num#	k4
kg	kg	kA
</s>
<s>
Michaela	Michaela	k1gFnSc1
Skleničková	Skleničková	k1gFnSc1
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2021	#num#	k4
</s>
<s>
Havířov	Havířov	k1gInSc1
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
203	#num#	k4
kg	kg	kA
</s>
<s>
Michaela	Michaela	k1gFnSc1
Skleničková	Skleničková	k1gFnSc1
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2021	#num#	k4
</s>
<s>
Havířov	Havířov	k1gInSc1
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
</s>
<s>
Do	do	k7c2
81	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
86	#num#	k4
kg	kg	kA
</s>
<s>
Michaela	Michaela	k1gFnSc1
Skleničková	Skleničková	k1gFnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2021	#num#	k4
</s>
<s>
Moskva	Moskva	k1gFnSc1
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
112	#num#	k4
kg	kg	kA
</s>
<s>
Michaela	Michaela	k1gFnSc1
Skleničková	Skleničková	k1gFnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2021	#num#	k4
</s>
<s>
Moskva	Moskva	k1gFnSc1
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
198	#num#	k4
kg	kg	kA
</s>
<s>
Michaela	Michaela	k1gFnSc1
Skleničková	Skleničková	k1gFnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2021	#num#	k4
</s>
<s>
Moskva	Moskva	k1gFnSc1
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
</s>
<s>
Do	do	k7c2
87	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
80	#num#	k4
kg	kg	kA
</s>
<s>
Michaela	Michaela	k1gFnSc1
Skleničková	Skleničková	k1gFnSc1
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
srpen	srpen	k1gInSc1
2019	#num#	k4
</s>
<s>
Míšeň	Míšeň	k1gFnSc1
<g/>
,	,	kIx,
Německo	Německo	k1gNnSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
110	#num#	k4
kg	kg	kA
</s>
<s>
Michaela	Michaela	k1gFnSc1
Skleničková	Skleničková	k1gFnSc1
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
2019	#num#	k4
</s>
<s>
Gaziantep	Gaziantep	k1gInSc1
<g/>
,	,	kIx,
Turecko	Turecko	k1gNnSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
190	#num#	k4
kg	kg	kA
</s>
<s>
Michaela	Michaela	k1gFnSc1
Skleničková	Skleničková	k1gFnSc1
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
2019	#num#	k4
</s>
<s>
Gaziantep	Gaziantep	k1gInSc1
<g/>
,	,	kIx,
Turecko	Turecko	k1gNnSc1
</s>
<s>
Nad	nad	k7c7
87	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
85	#num#	k4
kg	kg	kA
</s>
<s>
Tereza	Tereza	k1gFnSc1
Králová	Králová	k1gFnSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
2019	#num#	k4
</s>
<s>
Brno	Brno	k1gNnSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
107	#num#	k4
kg	kg	kA
</s>
<s>
Tereza	Tereza	k1gFnSc1
Králová	Králová	k1gFnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2021	#num#	k4
</s>
<s>
Moskva	Moskva	k1gFnSc1
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
187	#num#	k4
kg	kg	kA
</s>
<s>
Tereza	Tereza	k1gFnSc1
Králová	Králová	k1gFnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2021	#num#	k4
</s>
<s>
Moskva	Moskva	k1gFnSc1
<g/>
,	,	kIx,
Rusko	Rusko	k1gNnSc1
</s>
<s>
Seniorské	seniorský	k2eAgInPc1d1
české	český	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
–	–	k?
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Níže	nízce	k6eAd2
jsou	být	k5eAaImIp3nP
všechny	všechen	k3xTgInPc4
platné	platný	k2eAgInPc4d1
české	český	k2eAgInPc4d1
seniorské	seniorský	k2eAgInPc4d1
rekordy	rekord	k1gInPc4
stanovené	stanovený	k2eAgInPc4d1
mezi	mezi	k7c7
lety	let	k1gInPc7
1998	#num#	k4
<g/>
–	–	k?
<g/>
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
v	v	k7c6
důsledku	důsledek	k1gInSc6
zařazení	zařazení	k1gNnSc2
ženského	ženský	k2eAgNnSc2d1
vzpírání	vzpírání	k1gNnSc2
na	na	k7c4
Letní	letní	k2eAgFnPc4d1
olympijské	olympijský	k2eAgFnPc4d1
hry	hra	k1gFnPc4
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
zúžení	zúžení	k1gNnSc3
soutěží	soutěž	k1gFnPc2
na	na	k7c4
15	#num#	k4
hmotnostních	hmotnostní	k2eAgFnPc2d1
kategorií	kategorie	k1gFnPc2
(	(	kIx(
<g/>
resp.	resp.	kA
16	#num#	k4
kategorií	kategorie	k1gFnPc2
mezi	mezi	k7c7
roky	rok	k1gInPc7
2017	#num#	k4
<g/>
–	–	k?
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obdobně	obdobně	k6eAd1
jako	jako	k8xC,k8xS
u	u	k7c2
světových	světový	k2eAgInPc2d1
rekordů	rekord	k1gInPc2
je	být	k5eAaImIp3nS
i	i	k9
pro	pro	k7c4
stanovení	stanovení	k1gNnSc4
nového	nový	k2eAgInSc2d1
českého	český	k2eAgInSc2d1
rekordu	rekord	k1gInSc2
od	od	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
bylo	být	k5eAaImAgNnS
zavedeno	zavést	k5eAaPmNgNnS
„	„	k?
<g/>
kilové	kilový	k2eAgNnSc1d1
pravidlo	pravidlo	k1gNnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
třeba	třeba	k6eAd1
překonat	překonat	k5eAaPmF
původní	původní	k2eAgFnSc4d1
hodnotu	hodnota	k1gFnSc4
rekordu	rekord	k1gInSc2
o	o	k7c4
celý	celý	k2eAgInSc4d1
jeden	jeden	k4xCgInSc4
kilogram	kilogram	k1gInSc4
(	(	kIx(
<g/>
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
stačilo	stačit	k5eAaBmAgNnS
v	v	k7c6
dílčích	dílčí	k2eAgFnPc6d1
disciplínách	disciplína	k1gFnPc6
překonat	překonat	k5eAaPmF
rekord	rekord	k1gInSc4
o	o	k7c4
0,5	0,5	k4
kg	kg	kA
a	a	k8xC
v	v	k7c6
olympijském	olympijský	k2eAgInSc6d1
dvojboji	dvojboj	k1gInSc6
bylo	být	k5eAaImAgNnS
třeba	třeba	k6eAd1
rekord	rekord	k1gInSc4
překonat	překonat	k5eAaPmF
o	o	k7c4
2,5	2,5	k4
kg	kg	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
tím	ten	k3xDgInSc7
byly	být	k5eAaImAgInP
všechny	všechen	k3xTgInPc1
platné	platný	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
měly	mít	k5eAaImAgFnP
hodnotu	hodnota	k1gFnSc4
…	…	k?
<g/>
,5	,5	k4
kg	kg	kA
<g/>
,	,	kIx,
bez	bez	k7c2
výjimek	výjimka	k1gFnPc2
poníženy	ponížen	k2eAgFnPc4d1
na	na	k7c6
nižší	nízký	k2eAgFnSc6d2
celé	celá	k1gFnSc6
číslo	číslo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
konci	konec	k1gInSc3
roku	rok	k1gInSc2
2018	#num#	k4
byly	být	k5eAaImAgInP
nejlepší	dobrý	k2eAgInPc1d3
výkony	výkon	k1gInPc1
zamraženy	zamražen	k2eAgInPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Muži	muž	k1gMnPc1
</s>
<s>
Kategorie	kategorie	k1gFnPc1
</s>
<s>
Disciplína	disciplína	k1gFnSc1
</s>
<s>
Výkon	výkon	k1gInSc1
</s>
<s>
Držitel	držitel	k1gMnSc1
</s>
<s>
Datum	datum	k1gNnSc1
stanovení	stanovení	k1gNnSc2
</s>
<s>
Místo	místo	k7c2
stanovení	stanovení	k1gNnSc2
</s>
<s>
Do	do	k7c2
56	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
104	#num#	k4
kg	kg	kA
</s>
<s>
František	František	k1gMnSc1
Polák	Polák	k1gMnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
2018	#num#	k4
</s>
<s>
Buenos	Buenos	k1gMnSc1
Aires	Aires	k1gMnSc1
<g/>
,	,	kIx,
Argentina	Argentina	k1gFnSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
130	#num#	k4
kg	kg	kA
</s>
<s>
Petr	Petr	k1gMnSc1
Slabý	Slabý	k1gMnSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
2003	#num#	k4
</s>
<s>
Valencie	Valencie	k1gFnSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
233	#num#	k4
kg	kg	kA
</s>
<s>
František	František	k1gMnSc1
Polák	Polák	k1gMnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
2018	#num#	k4
</s>
<s>
Buenos	Buenos	k1gMnSc1
Aires	Aires	k1gMnSc1
<g/>
,	,	kIx,
Argentina	Argentina	k1gFnSc1
</s>
<s>
Do	do	k7c2
62	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
117	#num#	k4
kg	kg	kA
</s>
<s>
Petr	Petr	k1gMnSc1
Stanislav	Stanislav	k1gMnSc1
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
1999	#num#	k4
</s>
<s>
Teplice	Teplice	k1gFnPc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
150	#num#	k4
kg	kg	kA
</s>
<s>
Petr	Petr	k1gMnSc1
Stanislav	Stanislav	k1gMnSc1
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
1999	#num#	k4
</s>
<s>
Teplice	Teplice	k1gFnPc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
267	#num#	k4
kg	kg	kA
</s>
<s>
Petr	Petr	k1gMnSc1
Stanislav	Stanislav	k1gMnSc1
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
1999	#num#	k4
</s>
<s>
Teplice	Teplice	k1gFnPc1
</s>
<s>
Do	do	k7c2
69	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
137	#num#	k4
kg	kg	kA
</s>
<s>
Petr	Petr	k1gMnSc1
Petrov	Petrov	k1gInSc1
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2018	#num#	k4
</s>
<s>
Bukurešť	Bukurešť	k1gFnSc1
<g/>
,	,	kIx,
Rumunsko	Rumunsko	k1gNnSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
169	#num#	k4
kg	kg	kA
</s>
<s>
Petr	Petr	k1gMnSc1
Petrov	Petrov	k1gInSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2017	#num#	k4
</s>
<s>
Split	Split	k1gInSc1
<g/>
,	,	kIx,
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
305	#num#	k4
kg	kg	kA
</s>
<s>
Petr	Petr	k1gMnSc1
Petrov	Petrov	k1gInSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2017	#num#	k4
</s>
<s>
Split	Split	k1gInSc1
<g/>
,	,	kIx,
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
Do	do	k7c2
77	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
151	#num#	k4
kg	kg	kA
</s>
<s>
Zbyněk	Zbyněk	k1gMnSc1
Vacura	Vacur	k1gMnSc2
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2003	#num#	k4
</s>
<s>
Bohumín	Bohumín	k1gInSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
175	#num#	k4
kg	kg	kA
</s>
<s>
Zbyněk	Zbyněk	k1gMnSc1
Vacura	Vacur	k1gMnSc2
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2000	#num#	k4
</s>
<s>
Sofie	Sofie	k1gFnSc1
<g/>
,	,	kIx,
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
325	#num#	k4
kg	kg	kA
</s>
<s>
Zbyněk	Zbyněk	k1gMnSc1
Vacura	Vacur	k1gMnSc2
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2000	#num#	k4
</s>
<s>
Sofie	Sofie	k1gFnSc1
<g/>
,	,	kIx,
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
Do	do	k7c2
85	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
160	#num#	k4
kg	kg	kA
</s>
<s>
Zbyněk	Zbyněk	k1gMnSc1
Vacura	Vacur	k1gMnSc2
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2004	#num#	k4
</s>
<s>
Třinec	Třinec	k1gInSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
190	#num#	k4
kg	kg	kA
</s>
<s>
Zbyněk	Zbyněk	k1gMnSc1
Vacura	Vacur	k1gMnSc2
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2004	#num#	k4
</s>
<s>
Třinec	Třinec	k1gInSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
350	#num#	k4
kg	kg	kA
</s>
<s>
Zbyněk	Zbyněk	k1gMnSc1
Vacura	Vacur	k1gMnSc2
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2004	#num#	k4
</s>
<s>
Třinec	Třinec	k1gInSc1
</s>
<s>
Do	do	k7c2
94	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
166	#num#	k4
kg	kg	kA
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Matykiewicz	Matykiewicz	k1gMnSc1
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
2011	#num#	k4
</s>
<s>
Havířov	Havířov	k1gInSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
200	#num#	k4
kg	kg	kA
</s>
<s>
Jiří	Jiří	k1gMnSc1
Mandát	mandát	k1gInSc4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2002	#num#	k4
</s>
<s>
Bohumín	Bohumín	k1gInSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
361	#num#	k4
kg	kg	kA
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Matykiewicz	Matykiewicz	k1gMnSc1
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
2011	#num#	k4
</s>
<s>
Havířov	Havířov	k1gInSc1
</s>
<s>
Do	do	k7c2
105	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
180	#num#	k4
kg	kg	kA
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Matykiewicz	Matykiewicz	k1gMnSc1
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
2012	#num#	k4
</s>
<s>
Sokolov	Sokolov	k1gInSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
220	#num#	k4
kg	kg	kA
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Matykiewicz	Matykiewicz	k1gMnSc1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
2004	#num#	k4
</s>
<s>
Havířov	Havířov	k1gInSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
392	#num#	k4
kg	kg	kA
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Matykiewicz	Matykiewicz	k1gMnSc1
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
2003	#num#	k4
</s>
<s>
Vancouver	Vancouver	k1gInSc1
<g/>
,	,	kIx,
Kanada	Kanada	k1gFnSc1
</s>
<s>
Nad	nad	k7c7
105	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
200	#num#	k4
kg	kg	kA
</s>
<s>
Petr	Petr	k1gMnSc1
Sobotka	Sobotka	k1gMnSc1
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
2005	#num#	k4
</s>
<s>
Bohumín	Bohumín	k1gInSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
245	#num#	k4
kg	kg	kA
</s>
<s>
Jiří	Jiří	k1gMnSc1
Orság	Orság	k1gMnSc1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
2017	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
435	#num#	k4
kg	kg	kA
</s>
<s>
Jiří	Jiří	k1gMnSc1
Orság	Orság	k1gMnSc1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
2017	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
</s>
<s>
Ženy	žena	k1gFnPc1
</s>
<s>
Kategorie	kategorie	k1gFnPc1
</s>
<s>
Disciplína	disciplína	k1gFnSc1
</s>
<s>
Výkon	výkon	k1gInSc1
</s>
<s>
Držitelka	držitelka	k1gFnSc1
</s>
<s>
Datum	datum	k1gNnSc1
stanovení	stanovení	k1gNnSc2
</s>
<s>
Místo	místo	k7c2
stanovení	stanovení	k1gNnSc2
</s>
<s>
Do	do	k7c2
48	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
62	#num#	k4
kg	kg	kA
</s>
<s>
Veronika	Veronika	k1gFnSc1
Věžníková	Věžníková	k1gFnSc1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2016	#num#	k4
</s>
<s>
Fø	Fø	k6eAd1
<g/>
,	,	kIx,
Norsko	Norsko	k1gNnSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
73	#num#	k4
kg	kg	kA
</s>
<s>
Veronika	Veronika	k1gFnSc1
Věžníková	Věžníková	k1gFnSc1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2016	#num#	k4
</s>
<s>
Fø	Fø	k6eAd1
<g/>
,	,	kIx,
Norsko	Norsko	k1gNnSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
135	#num#	k4
kg	kg	kA
</s>
<s>
Veronika	Veronika	k1gFnSc1
Věžníková	Věžníková	k1gFnSc1
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2016	#num#	k4
</s>
<s>
Fø	Fø	k6eAd1
<g/>
,	,	kIx,
Norsko	Norsko	k1gNnSc1
</s>
<s>
Do	do	k7c2
53	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
67	#num#	k4
kg	kg	kA
</s>
<s>
Veronika	Veronika	k1gFnSc1
Věžníková	Věžníková	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
2015	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
78	#num#	k4
kg	kg	kA
</s>
<s>
Veronika	Veronika	k1gFnSc1
Věžníková	Věžníková	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
2015	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
145	#num#	k4
kg	kg	kA
</s>
<s>
Veronika	Veronika	k1gFnSc1
Věžníková	Věžníková	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
2015	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
</s>
<s>
Do	do	k7c2
58	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
77	#num#	k4
kg	kg	kA
</s>
<s>
Marie	Marie	k1gFnSc1
Korčianová	Korčianová	k1gFnSc1
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
2000	#num#	k4
</s>
<s>
Sofie	Sofie	k1gFnSc1
<g/>
,	,	kIx,
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
100	#num#	k4
kg	kg	kA
</s>
<s>
Marie	Marie	k1gFnSc1
Korčianová	Korčianová	k1gFnSc1
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
2000	#num#	k4
</s>
<s>
Sofie	Sofie	k1gFnSc1
<g/>
,	,	kIx,
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
177	#num#	k4
kg	kg	kA
</s>
<s>
Marie	Marie	k1gFnSc1
Korčianová	Korčianová	k1gFnSc1
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
2000	#num#	k4
</s>
<s>
Sofie	Sofie	k1gFnSc1
<g/>
,	,	kIx,
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
Do	do	k7c2
63	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
95	#num#	k4
kg	kg	kA
</s>
<s>
Lenka	Lenka	k1gFnSc1
Orságová	Orságový	k2eAgFnSc1d1
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2005	#num#	k4
</s>
<s>
Sofie	Sofie	k1gFnSc1
<g/>
,	,	kIx,
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
117	#num#	k4
kg	kg	kA
</s>
<s>
Lenka	Lenka	k1gFnSc1
Orságová	Orságový	k2eAgFnSc1d1
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2005	#num#	k4
</s>
<s>
Sofie	Sofie	k1gFnSc1
<g/>
,	,	kIx,
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
212	#num#	k4
kg	kg	kA
</s>
<s>
Lenka	Lenka	k1gFnSc1
Orságová	Orságový	k2eAgFnSc1d1
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2005	#num#	k4
</s>
<s>
Sofie	Sofie	k1gFnSc1
<g/>
,	,	kIx,
Bulharsko	Bulharsko	k1gNnSc1
</s>
<s>
Do	do	k7c2
69	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
93	#num#	k4
kg	kg	kA
</s>
<s>
Lenka	Lenka	k1gFnSc1
Orságová	Orságový	k2eAgFnSc1d1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
2005	#num#	k4
</s>
<s>
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
116	#num#	k4
kg	kg	kA
</s>
<s>
Lenka	Lenka	k1gFnSc1
Orságová	Orságový	k2eAgFnSc1d1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
2005	#num#	k4
</s>
<s>
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
209	#num#	k4
kg	kg	kA
</s>
<s>
Lenka	Lenka	k1gFnSc1
Orságová	Orságový	k2eAgFnSc1d1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
2005	#num#	k4
</s>
<s>
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Do	do	k7c2
75	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
102	#num#	k4
kg	kg	kA
</s>
<s>
Radomíra	Radomíra	k1gFnSc1
Ševčíková	Ševčíková	k1gFnSc1
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
1999	#num#	k4
</s>
<s>
A	a	k9
Coruñ	Coruñ	k1gFnSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
127	#num#	k4
kg	kg	kA
</s>
<s>
Radomíra	Radomíra	k1gFnSc1
Ševčíková	Ševčíková	k1gFnSc1
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
1999	#num#	k4
</s>
<s>
A	a	k9
Coruñ	Coruñ	k1gFnSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
230	#num#	k4
kg	kg	kA
</s>
<s>
Radomíra	Radomíra	k1gFnSc1
Ševčíková	Ševčíková	k1gFnSc1
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
1999	#num#	k4
</s>
<s>
A	a	k9
Coruñ	Coruñ	k1gFnSc1
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Do	do	k7c2
90	#num#	k4
kg	kg	kA
<g/>
(	(	kIx(
<g/>
do	do	k7c2
r.	r.	kA
2016	#num#	k4
<g/>
nad	nad	k7c4
75	#num#	k4
kg	kg	kA
<g/>
)	)	kIx)
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
102	#num#	k4
kg	kg	kA
</s>
<s>
Radomíra	Radomíra	k1gFnSc1
Ševčíková	Ševčíková	k1gFnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
2002	#num#	k4
</s>
<s>
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
133	#num#	k4
kg	kg	kA
</s>
<s>
Radomíra	Radomíra	k1gFnSc1
Ševčíková	Ševčíková	k1gFnSc1
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
2002	#num#	k4
</s>
<s>
Bohumín	Bohumín	k1gInSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
232	#num#	k4
kg	kg	kA
</s>
<s>
Radomíra	Radomíra	k1gFnSc1
Ševčíková	Ševčíková	k1gFnSc1
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
2001	#num#	k4
</s>
<s>
Bohumín	Bohumín	k1gInSc1
</s>
<s>
Nad	nad	k7c7
90	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
80	#num#	k4
kg	kg	kA
</s>
<s>
Tereza	Tereza	k1gFnSc1
Králová	Králová	k1gFnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2017	#num#	k4
</s>
<s>
Split	Split	k1gInSc1
<g/>
,	,	kIx,
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
96	#num#	k4
kg	kg	kA
</s>
<s>
Lenka	Lenka	k1gFnSc1
Ledvinová	Ledvinová	k1gFnSc1
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
2010	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
172	#num#	k4
kg	kg	kA
</s>
<s>
Tereza	Tereza	k1gFnSc1
Králová	Králová	k1gFnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
2017	#num#	k4
</s>
<s>
Split	Split	k1gInSc1
<g/>
,	,	kIx,
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
Seniorské	seniorský	k2eAgInPc1d1
české	český	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
–	–	k?
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Níže	nízce	k6eAd2
jsou	být	k5eAaImIp3nP
všechny	všechen	k3xTgInPc4
platné	platný	k2eAgInPc4d1
české	český	k2eAgInPc4d1
seniorské	seniorský	k2eAgInPc4d1
rekordy	rekord	k1gInPc4
stanovené	stanovený	k2eAgInPc4d1
mezi	mezi	k7c7
lety	let	k1gInPc7
1993	#num#	k4
<g/>
–	–	k?
<g/>
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
platností	platnost	k1gFnSc7
od	od	k7c2
roku	rok	k1gInSc2
1993	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
první	první	k4xOgFnSc3
celkové	celkový	k2eAgFnSc3d1
reformě	reforma	k1gFnSc3
struktury	struktura	k1gFnSc2
hmotnostních	hmotnostní	k2eAgFnPc2d1
kategorií	kategorie	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Před	před	k7c7
založením	založení	k1gNnSc7
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
byly	být	k5eAaImAgInP
evidovány	evidován	k2eAgInPc1d1
československé	československý	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Muži	muž	k1gMnPc1
</s>
<s>
Kategorie	kategorie	k1gFnPc1
</s>
<s>
Disciplína	disciplína	k1gFnSc1
</s>
<s>
Výkon	výkon	k1gInSc1
</s>
<s>
Držitel	držitel	k1gMnSc1
</s>
<s>
Datum	datum	k1gNnSc1
stanovení	stanovení	k1gNnSc2
</s>
<s>
Místo	místo	k7c2
stanovení	stanovení	k1gNnSc2
</s>
<s>
Do	do	k7c2
54	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
102,5	102,5	k4
kg	kg	kA
</s>
<s>
Pavel	Pavel	k1gMnSc1
Kudrna	Kudrna	k1gMnSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
1994	#num#	k4
</s>
<s>
Sokolov	Sokolov	k1gInSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
127,5	127,5	k4
kg	kg	kA
</s>
<s>
Pavel	Pavel	k1gMnSc1
Kudrna	Kudrna	k1gMnSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
1994	#num#	k4
</s>
<s>
Sokolov	Sokolov	k1gInSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
230	#num#	k4
kg	kg	kA
</s>
<s>
Pavel	Pavel	k1gMnSc1
Kudrna	Kudrna	k1gMnSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
1994	#num#	k4
</s>
<s>
Sokolov	Sokolov	k1gInSc1
</s>
<s>
Do	do	k7c2
59	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
112,5	112,5	k4
kg	kg	kA
</s>
<s>
Petr	Petr	k1gMnSc1
Stanislav	Stanislav	k1gMnSc1
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
1996	#num#	k4
</s>
<s>
Atlanta	Atlanta	k1gFnSc1
<g/>
,	,	kIx,
USA	USA	kA
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
142,5	142,5	k4
kg	kg	kA
</s>
<s>
Petr	Petr	k1gMnSc1
Stanislav	Stanislav	k1gMnSc1
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
1996	#num#	k4
</s>
<s>
Atlanta	Atlanta	k1gFnSc1
<g/>
,	,	kIx,
USA	USA	kA
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
255	#num#	k4
kg	kg	kA
</s>
<s>
Petr	Petr	k1gMnSc1
Stanislav	Stanislav	k1gMnSc1
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
červenec	červenec	k1gInSc1
1996	#num#	k4
</s>
<s>
Atlanta	Atlanta	k1gFnSc1
<g/>
,	,	kIx,
USA	USA	kA
</s>
<s>
Do	do	k7c2
64	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
125	#num#	k4
kg	kg	kA
</s>
<s>
Martin	Martin	k1gMnSc1
Takáč	Takáč	k1gMnSc1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
1995	#num#	k4
</s>
<s>
Beerševa	Beerševa	k1gFnSc1
<g/>
,	,	kIx,
Izrael	Izrael	k1gInSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
150	#num#	k4
kg	kg	kA
</s>
<s>
Petr	Petr	k1gMnSc1
Stanislav	Stanislav	k1gMnSc1
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
1996	#num#	k4
</s>
<s>
Atlanta	Atlanta	k1gFnSc1
<g/>
,	,	kIx,
USA	USA	kA
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
270	#num#	k4
kg	kg	kA
</s>
<s>
Martin	Martin	k1gMnSc1
Takáč	Takáč	k1gMnSc1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
1995	#num#	k4
</s>
<s>
Beerševa	Beerševa	k1gFnSc1
<g/>
,	,	kIx,
Izrael	Izrael	k1gInSc1
</s>
<s>
Do	do	k7c2
70	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
132,5	132,5	k4
kg	kg	kA
</s>
<s>
Petr	Petr	k1gMnSc1
Hrubý	Hrubý	k1gMnSc1
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
1997	#num#	k4
</s>
<s>
Kapské	kapský	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
,	,	kIx,
JAR	jar	k1gFnSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
157,5	157,5	k4
kg	kg	kA
</s>
<s>
Petr	Petr	k1gMnSc1
Hrubý	Hrubý	k1gMnSc1
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
1997	#num#	k4
</s>
<s>
Kapské	kapský	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
,	,	kIx,
JAR	jar	k1gFnSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
290	#num#	k4
kg	kg	kA
</s>
<s>
Petr	Petr	k1gMnSc1
Hrubý	Hrubý	k1gMnSc1
</s>
<s>
30	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
1997	#num#	k4
</s>
<s>
Kapské	kapský	k2eAgNnSc1d1
Město	město	k1gNnSc1
<g/>
,	,	kIx,
JAR	jar	k1gFnSc1
</s>
<s>
Do	do	k7c2
76	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
132,5	132,5	k4
kg	kg	kA
</s>
<s>
Petr	Petr	k1gMnSc1
Hrubý	Hrubý	k1gMnSc1
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
1997	#num#	k4
</s>
<s>
Teplice	Teplice	k1gFnPc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
158	#num#	k4
kg	kg	kA
</s>
<s>
Zbyněk	Zbyněk	k1gMnSc1
Vacura	Vacur	k1gMnSc2
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
1997	#num#	k4
</s>
<s>
Sokolov	Sokolov	k1gInSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
287,5	287,5	k4
kg	kg	kA
</s>
<s>
Petr	Petr	k1gMnSc1
Hrubý	Hrubý	k1gMnSc1
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
1997	#num#	k4
</s>
<s>
Teplice	Teplice	k1gFnPc1
</s>
<s>
Do	do	k7c2
83	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
147,5	147,5	k4
kg	kg	kA
</s>
<s>
Daniel	Daniel	k1gMnSc1
Bango	Bango	k1gMnSc1
</s>
<s>
23	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
1997	#num#	k4
</s>
<s>
Rijeka	Rijeka	k1gFnSc1
<g/>
,	,	kIx,
Chorvatsko	Chorvatsko	k1gNnSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
185	#num#	k4
kg	kg	kA
</s>
<s>
Daniel	Daniel	k1gMnSc1
Bango	Bango	k1gMnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
1995	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
330	#num#	k4
kg	kg	kA
</s>
<s>
Daniel	Daniel	k1gMnSc1
Bango	Bango	k1gMnSc1
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
duben	duben	k1gInSc1
1996	#num#	k4
</s>
<s>
Stavanger	Stavanger	k1gInSc1
<g/>
,	,	kIx,
Norsko	Norsko	k1gNnSc1
</s>
<s>
Do	do	k7c2
91	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
160	#num#	k4
kg	kg	kA
</s>
<s>
Roman	Roman	k1gMnSc1
Polom	polom	k1gInSc4
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
1994	#num#	k4
</s>
<s>
Teplice	Teplice	k1gFnPc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
193	#num#	k4
kg	kg	kA
</s>
<s>
Miloš	Miloš	k1gMnSc1
Kopecký	Kopecký	k1gMnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
1995	#num#	k4
</s>
<s>
Bohumín	Bohumín	k1gInSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
361	#num#	k4
kg	kg	kA
</s>
<s>
Roman	Roman	k1gMnSc1
Polom	polom	k1gInSc4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
1995	#num#	k4
</s>
<s>
Varšava	Varšava	k1gFnSc1
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
</s>
<s>
Do	do	k7c2
99	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
170	#num#	k4
kg	kg	kA
</s>
<s>
Petr	Petr	k1gMnSc1
Krol	Krol	k1gMnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
1994	#num#	k4
</s>
<s>
Sokolov	Sokolov	k1gInSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
200	#num#	k4
kg	kg	kA
</s>
<s>
Petr	Petr	k1gMnSc1
Krol	Krol	k1gMnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
1994	#num#	k4
</s>
<s>
Sokolov	Sokolov	k1gInSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
370	#num#	k4
kg	kg	kA
</s>
<s>
Petr	Petr	k1gMnSc1
Krol	Krol	k1gMnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
1994	#num#	k4
</s>
<s>
Sokolov	Sokolov	k1gInSc1
</s>
<s>
Do	do	k7c2
108	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
175	#num#	k4
kg	kg	kA
</s>
<s>
Petr	Petr	k1gMnSc1
Krol	Krol	k1gMnSc1
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
1994	#num#	k4
</s>
<s>
Teplice	Teplice	k1gFnPc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
201	#num#	k4
kg	kg	kA
</s>
<s>
Petr	Petr	k1gMnSc1
Krol	Krol	k1gMnSc1
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
1994	#num#	k4
</s>
<s>
Havířov	Havířov	k1gInSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
375	#num#	k4
kg	kg	kA
</s>
<s>
Petr	Petr	k1gMnSc1
Krol	Krol	k1gMnSc1
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
1994	#num#	k4
</s>
<s>
Teplice	Teplice	k1gFnPc1
</s>
<s>
Nad	nad	k7c7
108	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
178	#num#	k4
kg	kg	kA
</s>
<s>
Petr	Petr	k1gMnSc1
Sobotka	Sobotka	k1gMnSc1
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
1995	#num#	k4
</s>
<s>
Beerševa	Beerševa	k1gFnSc1
<g/>
,	,	kIx,
Izrael	Izrael	k1gInSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
222,5	222,5	k4
kg	kg	kA
</s>
<s>
Jiří	Jiří	k1gMnSc1
Zubrický	Zubrický	k2eAgMnSc1d1
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
1993	#num#	k4
</s>
<s>
Melbourne	Melbourne	k1gNnSc1
<g/>
,	,	kIx,
Austrálie	Austrálie	k1gFnSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
392,5	392,5	k4
kg	kg	kA
</s>
<s>
Jiří	Jiří	k1gMnSc1
Zubrický	Zubrický	k2eAgMnSc1d1
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
1993	#num#	k4
</s>
<s>
Melbourne	Melbourne	k1gNnSc1
<g/>
,	,	kIx,
Austrálie	Austrálie	k1gFnSc1
</s>
<s>
Ženy	žena	k1gFnPc1
</s>
<s>
Kategorie	kategorie	k1gFnPc1
</s>
<s>
Disciplína	disciplína	k1gFnSc1
</s>
<s>
Výkon	výkon	k1gInSc1
</s>
<s>
Držitelka	držitelka	k1gFnSc1
</s>
<s>
Datum	datum	k1gNnSc1
stanovení	stanovení	k1gNnSc2
</s>
<s>
Místo	místo	k7c2
stanovení	stanovení	k1gNnSc2
</s>
<s>
Do	do	k7c2
46	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
45	#num#	k4
kg	kg	kA
</s>
<s>
Ester	Ester	k1gFnSc1
Cichá	Cichý	k2eAgFnSc1d1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
1996	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
60	#num#	k4
kg	kg	kA
</s>
<s>
Ester	Ester	k1gFnSc1
Cichá	Cichý	k2eAgFnSc1d1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
1996	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
105	#num#	k4
kg	kg	kA
</s>
<s>
Ester	Ester	k1gFnSc1
Cichá	Cichý	k2eAgFnSc1d1
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
1996	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
</s>
<s>
Do	do	k7c2
50	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
52,5	52,5	k4
kg	kg	kA
</s>
<s>
Lenka	Lenka	k1gFnSc1
Hanusová	Hanusová	k1gFnSc1
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
srpen	srpen	k1gInSc1
1994	#num#	k4
</s>
<s>
Lublaň	Lublaň	k1gFnSc1
<g/>
,	,	kIx,
Slovinsko	Slovinsko	k1gNnSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
70	#num#	k4
kg	kg	kA
</s>
<s>
Ester	Ester	k1gFnSc1
Cichá	Cichý	k2eAgFnSc1d1
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
1997	#num#	k4
</s>
<s>
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
120	#num#	k4
kg	kg	kA
</s>
<s>
Lenka	Lenka	k1gFnSc1
Hanusová	Hanusová	k1gFnSc1
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
srpen	srpen	k1gInSc1
1994	#num#	k4
</s>
<s>
Lublaň	Lublaň	k1gFnSc1
<g/>
,	,	kIx,
Slovinsko	Slovinsko	k1gNnSc1
</s>
<s>
Do	do	k7c2
54	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
65	#num#	k4
kg	kg	kA
</s>
<s>
Marie	Marie	k1gFnSc1
Korčianová	Korčianová	k1gFnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
1994	#num#	k4
</s>
<s>
Řím	Řím	k1gInSc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
77,5	77,5	k4
kg	kg	kA
</s>
<s>
Marie	Marie	k1gFnSc1
Korčianová	Korčianová	k1gFnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
1994	#num#	k4
</s>
<s>
Řím	Řím	k1gInSc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
142,5	142,5	k4
kg	kg	kA
</s>
<s>
Marie	Marie	k1gFnSc1
Korčianová	Korčianová	k1gFnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
říjen	říjen	k1gInSc1
1994	#num#	k4
</s>
<s>
Řím	Řím	k1gInSc1
<g/>
,	,	kIx,
Itálie	Itálie	k1gFnSc1
</s>
<s>
Do	do	k7c2
59	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
77,5	77,5	k4
kg	kg	kA
</s>
<s>
Marie	Marie	k1gFnSc1
Korčianová	Korčianová	k1gFnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
1996	#num#	k4
</s>
<s>
Varšava	Varšava	k1gFnSc1
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
95	#num#	k4
kg	kg	kA
</s>
<s>
Marie	Marie	k1gFnSc1
Korčianová	Korčianová	k1gFnSc1
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
1995	#num#	k4
</s>
<s>
Kanton	Kanton	k1gInSc1
<g/>
,	,	kIx,
ČLR	ČLR	kA
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
170	#num#	k4
kg	kg	kA
</s>
<s>
Marie	Marie	k1gFnSc1
Korčianová	Korčianová	k1gFnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
1996	#num#	k4
</s>
<s>
Varšava	Varšava	k1gFnSc1
<g/>
,	,	kIx,
Polsko	Polsko	k1gNnSc1
</s>
<s>
Do	do	k7c2
64	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
82,5	82,5	k4
kg	kg	kA
</s>
<s>
Marie	Marie	k1gFnSc1
Korčianová	Korčianová	k1gFnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
1996	#num#	k4
</s>
<s>
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
105	#num#	k4
kg	kg	kA
</s>
<s>
Marie	Marie	k1gFnSc1
Korčianová	Korčianová	k1gFnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
1996	#num#	k4
</s>
<s>
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
187,5	187,5	k4
kg	kg	kA
</s>
<s>
Marie	Marie	k1gFnSc1
Korčianová	Korčianová	k1gFnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
1996	#num#	k4
</s>
<s>
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Do	do	k7c2
70	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
85,5	85,5	k4
kg	kg	kA
</s>
<s>
Radomíra	Radomíra	k1gFnSc1
Ševčíková	Ševčíková	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
1995	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
110,5	110,5	k4
kg	kg	kA
</s>
<s>
Radomíra	Radomíra	k1gFnSc1
Ševčíková	Ševčíková	k1gFnSc1
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
1995	#num#	k4
</s>
<s>
Praha	Praha	k1gFnSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
195	#num#	k4
kg	kg	kA
</s>
<s>
Radomíra	Radomíra	k1gFnSc1
Ševčíková	Ševčíková	k1gFnSc1
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
1995	#num#	k4
</s>
<s>
Kanton	Kanton	k1gInSc1
<g/>
,	,	kIx,
ČLR	ČLR	kA
</s>
<s>
Do	do	k7c2
76	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
92,5	92,5	k4
kg	kg	kA
</s>
<s>
Radomíra	Radomíra	k1gFnSc1
Ševčíková	Ševčíková	k1gFnSc1
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1996	#num#	k4
</s>
<s>
Sokolov	Sokolov	k1gInSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
116	#num#	k4
kg	kg	kA
</s>
<s>
Radomíra	Radomíra	k1gFnSc1
Ševčíková	Ševčíková	k1gFnSc1
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1996	#num#	k4
</s>
<s>
Sokolov	Sokolov	k1gInSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
207,5	207,5	k4
kg	kg	kA
</s>
<s>
Radomíra	Radomíra	k1gFnSc1
Ševčíková	Ševčíková	k1gFnSc1
</s>
<s>
21	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1996	#num#	k4
</s>
<s>
Sokolov	Sokolov	k1gInSc1
</s>
<s>
Do	do	k7c2
83	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
95	#num#	k4
kg	kg	kA
</s>
<s>
Radomíra	Radomíra	k1gFnSc1
Ševčíková	Ševčíková	k1gFnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
květen	květen	k1gInSc4
1997	#num#	k4
</s>
<s>
Nový	nový	k2eAgInSc1d1
Hrozenkov	Hrozenkov	k1gInSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
125	#num#	k4
kg	kg	kA
</s>
<s>
Radomíra	Radomíra	k1gFnSc1
Ševčíková	Ševčíková	k1gFnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
1996	#num#	k4
</s>
<s>
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
217,5	217,5	k4
kg	kg	kA
</s>
<s>
Radomíra	Radomíra	k1gFnSc1
Ševčíková	Ševčíková	k1gFnSc1
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
prosinec	prosinec	k1gInSc1
1996	#num#	k4
</s>
<s>
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Nad	nad	k7c7
83	#num#	k4
kg	kg	kA
</s>
<s>
Trh	trh	k1gInSc1
</s>
<s>
85	#num#	k4
kg	kg	kA
</s>
<s>
Soňa	Soňa	k1gFnSc1
Vašíčková	Vašíčková	k1gFnSc1
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
1993	#num#	k4
</s>
<s>
Melbourne	Melbourne	k1gNnSc1
<g/>
,	,	kIx,
Austrálie	Austrálie	k1gFnSc1
</s>
<s>
Nadhoz	nadhoz	k1gInSc1
</s>
<s>
108	#num#	k4
kg	kg	kA
</s>
<s>
Soňa	Soňa	k1gFnSc1
Vašíčková	Vašíčková	k1gFnSc1
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
červen	červen	k1gInSc1
1993	#num#	k4
</s>
<s>
Mödling	Mödling	k1gInSc1
<g/>
,	,	kIx,
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
Dvojboj	dvojboj	k1gInSc1
</s>
<s>
192,5	192,5	k4
kg	kg	kA
</s>
<s>
Soňa	Soňa	k1gFnSc1
Vašíčková	Vašíčková	k1gFnSc1
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
1993	#num#	k4
</s>
<s>
Melbourne	Melbourne	k1gNnSc1
<g/>
,	,	kIx,
Austrálie	Austrálie	k1gFnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
DUSPIVA	DUSPIVA	kA
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Informace	informace	k1gFnSc1
č.	č.	k?
86	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzpírání	vzpírání	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-11-28	2018-11-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
PROHL	PROHL	kA
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ohlédnutí	ohlédnutí	k1gNnSc1
za	za	k7c7
rekordy	rekord	k1gInPc7
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzpírání	vzpírání	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-07-01	2019-07-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
POLES	POLES	kA
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSV	ČSV	kA
-	-	kIx~
Rekordy	rekord	k1gInPc1
svazu	svaz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Powerlifter	Powerlifter	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
PROHL	PROHL	kA
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgInPc1d1
rekordy	rekord	k1gInPc4
mužů	muž	k1gMnPc2
k	k	k7c3
31	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
,	,	kIx,
České	český	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
žen	žena	k1gFnPc2
k	k	k7c3
31	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzpírání	vzpírání	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-12-26	2018-12-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
PROHL	PROHL	kA
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgInPc1d1
rekordy	rekord	k1gInPc4
mužů	muž	k1gMnPc2
k	k	k7c3
31	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
1997	#num#	k4
<g/>
,	,	kIx,
České	český	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
žen	žena	k1gFnPc2
k	k	k7c3
31	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzpírání	vzpírání	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-12-26	2018-12-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
PROHL	PROHL	kA
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přehlídka	přehlídka	k1gFnSc1
historických	historický	k2eAgInPc2d1
rekordů	rekord	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzpírání	vzpírání	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-12-26	2018-12-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Světové	světový	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
ve	v	k7c6
vzpírání	vzpírání	k1gNnSc6
</s>
<s>
Evropské	evropský	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
ve	v	k7c6
vzpírání	vzpírání	k1gNnSc6
</s>
<s>
stránky	stránka	k1gFnPc4
Českého	český	k2eAgInSc2d1
svazu	svaz	k1gInSc2
vzpírání	vzpírání	k1gNnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Rekordy	rekord	k1gInPc1
evidované	evidovaný	k2eAgInPc1d1
ČSV	ČSV	kA
k	k	k7c3
31	#num#	k4
<g/>
.	.	kIx.
12	#num#	k4
<g/>
.	.	kIx.
2010	#num#	k4
</s>
