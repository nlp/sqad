<s desamb="1">
Český	český	k2eAgInSc1d1
svaz	svaz	k1gInSc1
vzpírání	vzpírání	k1gNnSc2
(	(	kIx(
<g/>
ČSV	ČSV	kA
<g/>
)	)	kIx)
eviduje	evidovat	k5eAaImIp3nS
kromě	kromě	k7c2
seniorských	seniorský	k2eAgInPc2d1
rekordů	rekord	k1gInPc2
také	také	k9
juniorské	juniorský	k2eAgInPc4d1
rekordy	rekord	k1gInPc4
(	(	kIx(
<g/>
do	do	k7c2
20	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
mládežnické	mládežnický	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
(	(	kIx(
<g/>
do	do	k7c2
17	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rekordy	rekord	k1gInPc1
starších	starší	k1gMnPc2
i	i	k8xC
mladších	mladý	k2eAgMnPc2d2
žáků	žák	k1gMnPc2
a	a	k8xC
rekordy	rekord	k1gInPc1
kategorií	kategorie	k1gFnPc2
masters	mastersa	k1gFnPc2
<g/>
.	.	kIx.
</s>