<s>
Réva	réva	k1gFnSc1	réva
vinná	vinný	k2eAgFnSc1d1	vinná
(	(	kIx(	(
<g/>
Vitis	Vitis	k1gFnSc1	Vitis
vinifera	vinifera	k1gFnSc1	vinifera
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rostlina	rostlina	k1gFnSc1	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
révovitých	révovití	k1gMnPc2	révovití
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
též	též	k9	též
jako	jako	k9	jako
evropská	evropský	k2eAgFnSc1d1	Evropská
réva	réva	k1gFnSc1	réva
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
ušlechtilá	ušlechtilý	k2eAgFnSc1d1	ušlechtilá
réva	réva	k1gFnSc1	réva
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInPc1	její
plody	plod	k1gInPc1	plod
(	(	kIx(	(
<g/>
bobule	bobule	k1gFnSc1	bobule
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
přímé	přímý	k2eAgFnSc3d1	přímá
konzumaci	konzumace	k1gFnSc3	konzumace
<g/>
,	,	kIx,	,
k	k	k7c3	k
sušení	sušení	k1gNnSc3	sušení
a	a	k8xC	a
kandování	kandování	k1gNnSc3	kandování
a	a	k8xC	a
především	především	k6eAd1	především
jako	jako	k9	jako
surovina	surovina	k1gFnSc1	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
vína	víno	k1gNnSc2	víno
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
nápojů	nápoj	k1gInPc2	nápoj
a	a	k8xC	a
oleje	olej	k1gInSc2	olej
<g/>
.	.	kIx.	.
</s>

