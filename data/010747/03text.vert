<p>
<s>
Réva	réva	k1gFnSc1	réva
vinná	vinný	k2eAgFnSc1d1	vinná
(	(	kIx(	(
<g/>
Vitis	Vitis	k1gFnSc1	Vitis
vinifera	vinifera	k1gFnSc1	vinifera
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rostlina	rostlina	k1gFnSc1	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
révovitých	révovití	k1gMnPc2	révovití
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
též	též	k9	též
jako	jako	k9	jako
evropská	evropský	k2eAgFnSc1d1	Evropská
réva	réva	k1gFnSc1	réva
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
ušlechtilá	ušlechtilý	k2eAgFnSc1d1	ušlechtilá
réva	réva	k1gFnSc1	réva
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInPc1	její
plody	plod	k1gInPc1	plod
(	(	kIx(	(
<g/>
bobule	bobule	k1gFnSc1	bobule
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
přímé	přímý	k2eAgFnSc3d1	přímá
konzumaci	konzumace	k1gFnSc3	konzumace
<g/>
,	,	kIx,	,
k	k	k7c3	k
sušení	sušení	k1gNnSc3	sušení
a	a	k8xC	a
kandování	kandování	k1gNnSc3	kandování
a	a	k8xC	a
především	především	k6eAd1	především
jako	jako	k9	jako
surovina	surovina	k1gFnSc1	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
vína	víno	k1gNnSc2	víno
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
nápojů	nápoj	k1gInPc2	nápoj
a	a	k8xC	a
oleje	olej	k1gInSc2	olej
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělské	zemědělský	k2eAgNnSc1d1	zemědělské
odvětví	odvětví	k1gNnSc1	odvětví
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
pěstováním	pěstování	k1gNnSc7	pěstování
a	a	k8xC	a
zkoumáním	zkoumání	k1gNnSc7	zkoumání
révy	réva	k1gFnSc2	réva
vinné	vinný	k2eAgFnPc1d1	vinná
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
vinařství	vinařství	k1gNnSc1	vinařství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nomenklatura	nomenklatura	k1gFnSc1	nomenklatura
==	==	k?	==
</s>
</p>
<p>
<s>
Vitis	Vitis	k1gFnSc1	Vitis
vinifera	vinifero	k1gNnSc2	vinifero
L.	L.	kA	L.
<g/>
,	,	kIx,	,
1753	[number]	k4	1753
</s>
</p>
<p>
<s>
Vitis	Vitis	k1gFnSc1	Vitis
vinifera	vinifero	k1gNnSc2	vinifero
L.	L.	kA	L.
<g/>
,	,	kIx,	,
1753	[number]	k4	1753
subsp	subsp	k1gInSc1	subsp
<g/>
.	.	kIx.	.
vinifera	vinifera	k1gFnSc1	vinifera
–	–	k?	–
réva	réva	k1gFnSc1	réva
vinná	vinný	k2eAgFnSc1d1	vinná
pravá	pravý	k2eAgFnSc1d1	pravá
</s>
</p>
<p>
<s>
Nomenklatorická	Nomenklatorický	k2eAgNnPc1d1	Nomenklatorický
synonyma	synonymum	k1gNnPc1	synonymum
</s>
</p>
<p>
<s>
≡	≡	k?	≡
<g/>
Vitis	Vitis	k1gInSc1	Vitis
vinifera	vinifer	k1gMnSc2	vinifer
L.	L.	kA	L.
var.	var.	k?	var.
sativa	sativa	k1gFnSc1	sativa
DC	DC	kA	DC
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1805	[number]	k4	1805
(	(	kIx(	(
<g/>
nom.	nom.	k?	nom.
superfl	superfl	k1gInSc1	superfl
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
≡	≡	k?	≡
<g/>
Vitis	Vitis	k1gInSc1	Vitis
vinifera	vinifer	k1gMnSc2	vinifer
L.	L.	kA	L.
subsp	subsp	k1gInSc1	subsp
<g/>
.	.	kIx.	.
sativa	sativa	k1gFnSc1	sativa
(	(	kIx(	(
<g/>
DC	DC	kA	DC
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Beger	Beger	k1gInSc1	Beger
in	in	k?	in
Hegi	Heg	k1gFnSc2	Heg
<g/>
,	,	kIx,	,
1925	[number]	k4	1925
(	(	kIx(	(
<g/>
nom.	nom.	k?	nom.
superfl	superfl	k1gInSc1	superfl
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
Taxonomická	taxonomický	k2eAgNnPc4d1	taxonomické
synonyma	synonymum	k1gNnPc4	synonymum
</s>
</p>
<p>
<s>
=	=	kIx~	=
<g/>
Vitis	Vitis	k1gInSc1	Vitis
laciniosa	laciniosa	k1gFnSc1	laciniosa
L.	L.	kA	L.
<g/>
,	,	kIx,	,
1753	[number]	k4	1753
</s>
</p>
<p>
<s>
=	=	kIx~	=
<g/>
Vitis	Vitis	k1gInSc1	Vitis
vinifera	vinifer	k1gMnSc2	vinifer
L.	L.	kA	L.
var.	var.	k?	var.
laciniosa	laciniosa	k1gFnSc1	laciniosa
(	(	kIx(	(
<g/>
L.	L.	kA	L.
<g/>
)	)	kIx)	)
Fiori	Fiori	k1gNnSc1	Fiori
<g/>
,	,	kIx,	,
1925	[number]	k4	1925
</s>
</p>
<p>
<s>
=	=	kIx~	=
<g/>
Vitis	Vitis	k1gFnSc2	Vitis
corinthiaca	corinthiaca	k1gMnSc1	corinthiaca
Rafin	Rafin	k1gMnSc1	Rafin
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1830	[number]	k4	1830
</s>
</p>
<p>
<s>
=	=	kIx~	=
<g/>
Vitis	Vitis	k1gFnSc2	Vitis
cylindrica	cylindrica	k1gMnSc1	cylindrica
Rafin	Rafin	k1gMnSc1	Rafin
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1830	[number]	k4	1830
</s>
</p>
<p>
<s>
=	=	kIx~	=
<g/>
Vitis	Vitis	k1gInSc1	Vitis
bryoniifolia	bryoniifolius	k1gMnSc2	bryoniifolius
Bunge	Bung	k1gMnSc2	Bung
<g/>
,	,	kIx,	,
1835	[number]	k4	1835
</s>
</p>
<p>
<s>
=	=	kIx~	=
<g/>
Vitis	Vitis	k1gFnPc6	Vitis
amurensis	amurensis	k1gFnSc2	amurensis
Rupr	Rupr	k1gInSc1	Rupr
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1857	[number]	k4	1857
</s>
</p>
<p>
<s>
=	=	kIx~	=
<g/>
Vitis	Vitis	k1gFnSc3	Vitis
alemannica	alemannic	k2eAgFnSc1d1	alemannic
Andrasovsky	Andrasovsky	k1gFnSc1	Andrasovsky
in	in	k?	in
Jáv	Jáva	k1gFnPc2	Jáva
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1925	[number]	k4	1925
</s>
</p>
<p>
<s>
=	=	kIx~	=
<g/>
Vitis	Vitis	k1gInSc1	Vitis
antiquorum	antiquorum	k1gNnSc1	antiquorum
Andrasovsky	Andrasovsky	k1gFnPc2	Andrasovsky
in	in	k?	in
Jáv	Jáva	k1gFnPc2	Jáva
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1925	[number]	k4	1925
</s>
</p>
<p>
<s>
=	=	kIx~	=
<g/>
Vitis	Vitis	k1gFnSc3	Vitis
byzantina	byzantin	k2eAgFnSc1d1	byzantin
Andrasovsky	Andrasovsky	k1gFnSc1	Andrasovsky
in	in	k?	in
Jáv	Jáva	k1gFnPc2	Jáva
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1925	[number]	k4	1925
</s>
</p>
<p>
<s>
=	=	kIx~	=
<g/>
Vitis	Vitis	k1gFnSc1	Vitis
deliciosa	deliciosa	k1gFnSc1	deliciosa
Andrasovsky	Andrasovsky	k1gFnSc1	Andrasovsky
in	in	k?	in
Jáv	Jáva	k1gFnPc2	Jáva
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1925	[number]	k4	1925
</s>
</p>
<p>
<s>
=	=	kIx~	=
<g/>
Vitis	Vitis	k1gFnSc3	Vitis
mediterranea	mediterrane	k2eAgFnSc1d1	mediterrane
Andrasovsky	Andrasovsky	k1gFnSc1	Andrasovsky
in	in	k?	in
Jáv	Jáva	k1gFnPc2	Jáva
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1925	[number]	k4	1925
<g/>
Vitis	Vitis	k1gFnPc2	Vitis
vinifera	vinifero	k1gNnSc2	vinifero
L.	L.	kA	L.
<g/>
,	,	kIx,	,
1753	[number]	k4	1753
subsp	subsp	k1gInSc1	subsp
<g/>
.	.	kIx.	.
sylvestris	sylvestris	k1gFnSc1	sylvestris
(	(	kIx(	(
<g/>
C.C.	C.C.	k1gMnSc1	C.C.
<g/>
Gmel	Gmel	k1gMnSc1	Gmel
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Hegi	Hegi	k1gNnSc1	Hegi
<g/>
,	,	kIx,	,
1925	[number]	k4	1925
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
silvestris	silvestris	k1gFnSc1	silvestris
<g/>
)	)	kIx)	)
–	–	k?	–
réva	réva	k1gFnSc1	réva
vinná	vinný	k2eAgFnSc1d1	vinná
lesní	lesní	k2eAgFnSc1d1	lesní
</s>
</p>
<p>
<s>
Basionym	Basionym	k1gInSc1	Basionym
</s>
</p>
<p>
<s>
Vitis	Vitis	k1gInSc1	Vitis
sylvestris	sylvestris	k1gFnSc2	sylvestris
C.C.	C.C.	k1gMnSc1	C.C.
<g/>
Gmel	Gmel	k1gMnSc1	Gmel
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1805	[number]	k4	1805
<g/>
Nomenklatorická	Nomenklatorický	k2eAgFnSc1d1	Nomenklatorický
synonyma	synonymum	k1gNnSc2	synonymum
</s>
</p>
<p>
<s>
≡	≡	k?	≡
<g/>
Vitis	Vitis	k1gInSc1	Vitis
vinifera	vinifera	k1gFnSc1	vinifera
var.	var.	k?	var.
sylvestris	sylvestris	k1gFnSc1	sylvestris
(	(	kIx(	(
<g/>
C.C.	C.C.	k1gMnSc1	C.C.
<g/>
Gmel	Gmel	k1gMnSc1	Gmel
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Willd	Willd	k1gInSc1	Willd
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1809	[number]	k4	1809
<g/>
Taxonomická	taxonomický	k2eAgFnSc1d1	Taxonomická
synonyma	synonymum	k1gNnSc2	synonymum
</s>
</p>
<p>
<s>
=	=	kIx~	=
<g/>
Vitis	Vitis	k1gFnSc3	Vitis
vinifera	vinifero	k1gNnSc2	vinifero
DC	DC	kA	DC
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1815	[number]	k4	1815
non	non	k?	non
L.	L.	kA	L.
<g/>
,	,	kIx,	,
1753	[number]	k4	1753
</s>
</p>
<p>
<s>
=	=	kIx~	=
<g/>
Vitis	Vitis	k1gFnSc3	Vitis
vinifera	vinifero	k1gNnSc2	vinifero
var.	var.	k?	var.
anebophylla	anebophyllo	k1gNnSc2	anebophyllo
Kolen	kolna	k1gFnPc2	kolna
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1846	[number]	k4	1846
</s>
</p>
<p>
<s>
=	=	kIx~	=
<g/>
Vitis	Vitis	k1gFnSc3	Vitis
vinifera	vinifero	k1gNnSc2	vinifero
var.	var.	k?	var.
trichophylla	trichophyllo	k1gNnSc2	trichophyllo
Kolen	kolna	k1gFnPc2	kolna
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1846	[number]	k4	1846
</s>
</p>
<p>
<s>
==	==	k?	==
Odrůdy	odrůda	k1gFnSc2	odrůda
==	==	k?	==
</s>
</p>
<p>
<s>
Původní	původní	k2eAgFnSc1d1	původní
volně	volně	k6eAd1	volně
rostoucí	rostoucí	k2eAgFnSc1d1	rostoucí
réva	réva	k1gFnSc1	réva
lesní	lesní	k2eAgFnSc1d1	lesní
(	(	kIx(	(
<g/>
Vitis	Vitis	k1gFnSc1	Vitis
vinifera	vinifera	k1gFnSc1	vinifera
subsp	subsp	k1gInSc1	subsp
<g/>
.	.	kIx.	.
sylvestris	sylvestris	k1gInSc1	sylvestris
<g/>
)	)	kIx)	)
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
velkou	velký	k2eAgFnSc4d1	velká
rozmanitost	rozmanitost	k1gFnSc4	rozmanitost
hroznů	hrozen	k1gInPc2	hrozen
v	v	k7c6	v
chuti	chuť	k1gFnSc6	chuť
<g/>
,	,	kIx,	,
barvě	barva	k1gFnSc6	barva
<g/>
,	,	kIx,	,
velikosti	velikost	k1gFnSc6	velikost
i	i	k8xC	i
tvaru	tvar	k1gInSc6	tvar
bobulí	bobule	k1gFnPc2	bobule
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
její	její	k3xOp3gFnSc3	její
velké	velká	k1gFnSc3	velká
heterozygotnosti	heterozygotnost	k1gFnSc2	heterozygotnost
mohly	moct	k5eAaImAgInP	moct
postupně	postupně	k6eAd1	postupně
vzniknout	vzniknout	k5eAaPmF	vzniknout
tisíce	tisíc	k4xCgInPc4	tisíc
kulturních	kulturní	k2eAgFnPc2d1	kulturní
odrůd	odrůda	k1gFnPc2	odrůda
révy	réva	k1gFnSc2	réva
vinné	vinný	k2eAgFnSc2d1	vinná
(	(	kIx(	(
<g/>
Vitis	Vitis	k1gFnSc2	Vitis
vinifera	vinifer	k1gMnSc4	vinifer
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
vinifera	vinifera	k1gFnSc1	vinifera
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ať	ať	k8xS	ať
již	již	k6eAd1	již
samovolným	samovolný	k2eAgNnSc7d1	samovolné
křížením	křížení	k1gNnSc7	křížení
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
kultivary	kultivar	k1gInPc4	kultivar
šlechtěním	šlechtění	k1gNnSc7	šlechtění
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
odrůdy	odrůda	k1gFnPc4	odrůda
pěstované	pěstovaný	k2eAgFnPc4d1	pěstovaná
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
patří	patřit	k5eAaImIp3nS	patřit
zejména	zejména	k9	zejména
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Aurelius	Aurelius	k1gMnSc1	Aurelius
</s>
</p>
<p>
<s>
Chardonnay	Chardonnaa	k1gFnPc1	Chardonnaa
</s>
</p>
<p>
<s>
Irsai	Irsai	k6eAd1	Irsai
Oliver	Oliver	k1gInSc1	Oliver
</s>
</p>
<p>
<s>
Muškát	muškát	k1gInSc1	muškát
Othonel	Othonela	k1gFnPc2	Othonela
</s>
</p>
<p>
<s>
Müller	Müller	k1gMnSc1	Müller
Thurgau	Thurgaus	k1gInSc2	Thurgaus
</s>
</p>
<p>
<s>
Neuburské	Neuburský	k2eAgNnSc1d1	Neuburské
</s>
</p>
<p>
<s>
Pálava	Pálava	k1gFnSc1	Pálava
</s>
</p>
<p>
<s>
Rulandské	rulandský	k2eAgFnPc1d1	rulandský
bílé	bílý	k2eAgFnPc1d1	bílá
</s>
</p>
<p>
<s>
Rulandské	rulandský	k2eAgInPc1d1	rulandský
šedé	šedý	k2eAgInPc1d1	šedý
</s>
</p>
<p>
<s>
Ryzlink	ryzlink	k1gInSc1	ryzlink
rýnský	rýnský	k2eAgInSc1d1	rýnský
</s>
</p>
<p>
<s>
Ryzlink	ryzlink	k1gInSc1	ryzlink
vlašský	vlašský	k2eAgInSc1d1	vlašský
</s>
</p>
<p>
<s>
Sauvignon	Sauvignon	k1gMnSc1	Sauvignon
</s>
</p>
<p>
<s>
Sylvánské	sylvánské	k1gNnSc1	sylvánské
zelené	zelený	k2eAgInPc1d1	zelený
</s>
</p>
<p>
<s>
Tramín	tramín	k1gInSc1	tramín
červený	červený	k2eAgInSc1d1	červený
</s>
</p>
<p>
<s>
Veltlínské	veltlínský	k2eAgInPc1d1	veltlínský
zelené	zelený	k2eAgInPc1d1	zelený
</s>
</p>
<p>
<s>
Veltlínské	veltlínský	k2eAgFnPc1d1	veltlínský
červené	červený	k2eAgFnPc1d1	červená
rané	raný	k2eAgFnPc1d1	raná
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Réva	réva	k1gFnSc1	réva
vinná	vinný	k2eAgFnSc1d1	vinná
je	být	k5eAaImIp3nS	být
popínavá	popínavý	k2eAgFnSc1d1	popínavá
dřevnatá	dřevnatý	k2eAgFnSc1d1	dřevnatá
liána	liána	k1gFnSc1	liána
<g/>
,	,	kIx,	,
pnoucí	pnoucí	k2eAgFnSc1d1	pnoucí
se	se	k3xPyFc4	se
po	po	k7c6	po
oporách	opora	k1gFnPc6	opora
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgMnPc3	jenž
se	se	k3xPyFc4	se
přichycuje	přichycovat	k5eAaImIp3nS	přichycovat
pomocí	pomocí	k7c2	pomocí
úponků	úponek	k1gInPc2	úponek
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
<g/>
-li	i	k?	-li
možnost	možnost	k1gFnSc4	možnost
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
jedinci	jedinec	k1gMnPc1	jedinec
divokých	divoký	k2eAgFnPc2d1	divoká
odrůd	odrůda	k1gFnPc2	odrůda
mohou	moct	k5eAaImIp3nP	moct
ve	v	k7c6	v
volné	volný	k2eAgFnSc6d1	volná
přírodě	příroda	k1gFnSc6	příroda
dosahovat	dosahovat	k5eAaImF	dosahovat
výšek	výška	k1gFnPc2	výška
až	až	k9	až
30	[number]	k4	30
m	m	kA	m
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
průměr	průměr	k1gInSc1	průměr
kmene	kmen	k1gInSc2	kmen
u	u	k7c2	u
země	zem	k1gFnSc2	zem
může	moct	k5eAaImIp3nS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
až	až	k9	až
1,5	[number]	k4	1,5
m	m	kA	m
<g/>
;	;	kIx,	;
jsou	být	k5eAaImIp3nP	být
popsány	popsat	k5eAaPmNgInP	popsat
případy	případ	k1gInPc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
rostlina	rostlina	k1gFnSc1	rostlina
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
výšky	výška	k1gFnSc2	výška
až	až	k8xS	až
43	[number]	k4	43
m.	m.	k?	m.
Jedinci	jedinec	k1gMnPc7	jedinec
kulturních	kulturní	k2eAgFnPc2d1	kulturní
odrůd	odrůda	k1gFnPc2	odrůda
na	na	k7c6	na
vinicích	vinice	k1gFnPc6	vinice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
vinaři	vinař	k1gMnPc1	vinař
nenechávají	nechávat	k5eNaImIp3nP	nechávat
dožít	dožít	k5eAaPmF	dožít
takového	takový	k3xDgNnSc2	takový
stáří	stáří	k1gNnSc2	stáří
<g/>
,	,	kIx,	,
dorůstají	dorůstat	k5eAaImIp3nP	dorůstat
výšky	výška	k1gFnSc2	výška
nejvýše	vysoce	k6eAd3	vysoce
4	[number]	k4	4
m	m	kA	m
při	při	k7c6	při
průměru	průměr	k1gInSc6	průměr
kmene	kmen	k1gInSc2	kmen
do	do	k7c2	do
50	[number]	k4	50
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
okrouhlé	okrouhlý	k2eAgNnSc1d1	okrouhlé
<g/>
,	,	kIx,	,
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
až	až	k6eAd1	až
pěti	pět	k4xCc7	pět
laloky	lalok	k1gInPc7	lalok
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
do	do	k7c2	do
15	[number]	k4	15
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Borka	borka	k1gFnSc1	borka
kmene	kmen	k1gInSc2	kmen
je	být	k5eAaImIp3nS	být
světlehnědá	světlehnědý	k2eAgFnSc1d1	světlehnědá
a	a	k8xC	a
loupe	loupat	k5eAaImIp3nS	loupat
se	se	k3xPyFc4	se
v	v	k7c6	v
dlouhých	dlouhý	k2eAgInPc6d1	dlouhý
pruzích	pruh	k1gInPc6	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Letorosty	letorost	k1gInPc1	letorost
jsou	být	k5eAaImIp3nP	být
sytěji	sytě	k6eAd2	sytě
zabarvené	zabarvený	k2eAgFnPc1d1	zabarvená
<g/>
,	,	kIx,	,
žlutohnědé	žlutohnědý	k2eAgFnPc1d1	žlutohnědá
nebo	nebo	k8xC	nebo
červenohnědé	červenohnědý	k2eAgFnPc1d1	červenohnědá
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
žlutozelené	žlutozelený	k2eAgFnSc2d1	žlutozelená
barvy	barva	k1gFnSc2	barva
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
bohaté	bohatý	k2eAgInPc1d1	bohatý
laty	lat	k1gInPc1	lat
<g/>
.	.	kIx.	.
</s>
<s>
Kulturní	kulturní	k2eAgFnPc1d1	kulturní
odrůdy	odrůda	k1gFnPc1	odrůda
(	(	kIx(	(
<g/>
V.	V.	kA	V.
vinifera	vinifera	k1gFnSc1	vinifera
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
vinifera	vinifera	k1gFnSc1	vinifera
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
jednodomé	jednodomý	k2eAgInPc1d1	jednodomý
<g/>
,	,	kIx,	,
divoké	divoký	k2eAgInPc1d1	divoký
(	(	kIx(	(
<g/>
V.	V.	kA	V.
vinifera	vinifera	k1gFnSc1	vinifera
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
sylvestris	sylvestris	k1gFnSc1	sylvestris
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
dvoudomé	dvoudomý	k2eAgFnPc1d1	dvoudomá
<g/>
.	.	kIx.	.
</s>
<s>
Plodem	plod	k1gInSc7	plod
jsou	být	k5eAaImIp3nP	být
bobule	bobule	k1gFnPc4	bobule
kulovitého	kulovitý	k2eAgNnSc2d1	kulovité
<g/>
,	,	kIx,	,
vejčitého	vejčitý	k2eAgMnSc2d1	vejčitý
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zaobleně	zaobleně	k6eAd1	zaobleně
válcovitého	válcovitý	k2eAgInSc2d1	válcovitý
tvaru	tvar	k1gInSc2	tvar
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
0,4	[number]	k4	0,4
<g/>
–	–	k?	–
<g/>
1,5	[number]	k4	1,5
cm	cm	kA	cm
a	a	k8xC	a
délce	délka	k1gFnSc6	délka
až	až	k9	až
2,5	[number]	k4	2,5
cm	cm	kA	cm
<g/>
;	;	kIx,	;
u	u	k7c2	u
divokých	divoký	k2eAgFnPc2d1	divoká
odrůd	odrůda	k1gFnPc2	odrůda
bývají	bývat	k5eAaImIp3nP	bývat
drobnější	drobný	k2eAgMnPc1d2	drobnější
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc1	jejich
barvy	barva	k1gFnPc1	barva
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
rozmanité	rozmanitý	k2eAgFnPc1d1	rozmanitá
<g/>
,	,	kIx,	,
od	od	k7c2	od
zelené	zelená	k1gFnSc2	zelená
<g/>
,	,	kIx,	,
zelenožluté	zelenožlutý	k2eAgFnSc2d1	zelenožlutá
<g/>
,	,	kIx,	,
žluté	žlutý	k2eAgFnSc2d1	žlutá
po	po	k7c4	po
červenou	červený	k2eAgFnSc4d1	červená
až	až	k8xS	až
tmavofialovou	tmavofialový	k2eAgFnSc4d1	tmavofialová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kořeny	kořen	k1gInPc1	kořen
běžně	běžně	k6eAd1	běžně
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
délky	délka	k1gFnPc4	délka
přes	přes	k7c4	přes
10	[number]	k4	10
m	m	kA	m
i	i	k8xC	i
na	na	k7c6	na
skalnatém	skalnatý	k2eAgNnSc6d1	skalnaté
podloží	podloží	k1gNnSc6	podloží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stanoviště	stanoviště	k1gNnSc2	stanoviště
==	==	k?	==
</s>
</p>
<p>
<s>
Kulturní	kulturní	k2eAgFnPc1d1	kulturní
formy	forma	k1gFnPc1	forma
se	se	k3xPyFc4	se
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
na	na	k7c6	na
vinicích	vinice	k1gFnPc6	vinice
<g/>
.	.	kIx.	.
</s>
<s>
Vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
teplé	teplý	k2eAgInPc1d1	teplý
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
propustné	propustný	k2eAgFnPc1d1	propustná
půdy	půda	k1gFnPc1	půda
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
bohaté	bohatý	k2eAgFnPc1d1	bohatá
na	na	k7c4	na
živiny	živina	k1gFnPc4	živina
<g/>
,	,	kIx,	,
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
světlomilné	světlomilný	k2eAgFnPc1d1	světlomilná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Topografie	topografie	k1gFnSc1	topografie
stanoviště	stanoviště	k1gNnSc2	stanoviště
<g/>
:	:	kIx,	:
Svahovité	svahovitý	k2eAgInPc1d1	svahovitý
pozemky	pozemek	k1gInPc1	pozemek
mají	mít	k5eAaImIp3nP	mít
pro	pro	k7c4	pro
pěstování	pěstování	k1gNnSc4	pěstování
révy	réva	k1gFnSc2	réva
vinné	vinný	k2eAgInPc4d1	vinný
význam	význam	k1gInSc4	význam
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
a	a	k8xC	a
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
jako	jako	k8xS	jako
ochrana	ochrana	k1gFnSc1	ochrana
proti	proti	k7c3	proti
mrazům	mráz	k1gInPc3	mráz
<g/>
.	.	kIx.	.
</s>
<s>
Rovinaté	rovinatý	k2eAgInPc1d1	rovinatý
pozemky	pozemek	k1gInPc1	pozemek
nejsou	být	k5eNaImIp3nP	být
pro	pro	k7c4	pro
produkci	produkce	k1gFnSc4	produkce
kvalitních	kvalitní	k2eAgNnPc2d1	kvalitní
vín	víno	k1gNnPc2	víno
příznivé	příznivý	k2eAgFnPc1d1	příznivá
<g/>
.	.	kIx.	.
</s>
<s>
Expozice	expozice	k1gFnSc1	expozice
ke	k	k7c3	k
světovým	světový	k2eAgFnPc3d1	světová
stranám	strana	k1gFnPc3	strana
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
příjem	příjem	k1gInSc4	příjem
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
pěstování	pěstování	k1gNnSc4	pěstování
jsou	být	k5eAaImIp3nP	být
nejvhodnější	vhodný	k2eAgFnSc2d3	nejvhodnější
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc2d1	jižní
a	a	k8xC	a
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
expozice	expozice	k1gFnSc2	expozice
<g/>
.	.	kIx.	.
</s>
<s>
Absolutně	absolutně	k6eAd1	absolutně
nevhodné	vhodný	k2eNgFnPc1d1	nevhodná
jsou	být	k5eAaImIp3nP	být
severní	severní	k2eAgFnPc1d1	severní
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
divoké	divoký	k2eAgFnPc1d1	divoká
formy	forma	k1gFnPc1	forma
rostou	růst	k5eAaImIp3nP	růst
ve	v	k7c6	v
vlhkých	vlhký	k2eAgInPc6d1	vlhký
lesích	les	k1gInPc6	les
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
lužního	lužní	k2eAgInSc2d1	lužní
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
křovinách	křovina	k1gFnPc6	křovina
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
vodních	vodní	k2eAgInPc2d1	vodní
toků	tok	k1gInPc2	tok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
===	===	k?	===
</s>
</p>
<p>
<s>
Réva	réva	k1gFnSc1	réva
vinná	vinný	k2eAgFnSc1d1	vinná
se	se	k3xPyFc4	se
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
zpravidla	zpravidla	k6eAd1	zpravidla
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
do	do	k7c2	do
300	[number]	k4	300
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
vytvořené	vytvořený	k2eAgFnPc4d1	vytvořená
příznivé	příznivý	k2eAgFnPc4d1	příznivá
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
pěstovat	pěstovat	k5eAaImF	pěstovat
až	až	k9	až
do	do	k7c2	do
500	[number]	k4	500
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ve	v	k7c6	v
skleníku	skleník	k1gInSc6	skleník
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
zvyšující	zvyšující	k2eAgFnSc7d1	zvyšující
se	se	k3xPyFc4	se
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
a	a	k8xC	a
úbytkem	úbytek	k1gInSc7	úbytek
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
ubývá	ubývat	k5eAaImIp3nS	ubývat
v	v	k7c6	v
bobulích	bobule	k1gFnPc6	bobule
obsah	obsah	k1gInSc1	obsah
cukru	cukr	k1gInSc2	cukr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Expozice	expozice	k1gFnSc1	expozice
svahu	svah	k1gInSc2	svah
===	===	k?	===
</s>
</p>
<p>
<s>
Optimální	optimální	k2eAgInPc1d1	optimální
jsou	být	k5eAaImIp3nP	být
svahy	svah	k1gInPc1	svah
s	s	k7c7	s
jižní	jižní	k2eAgFnSc7d1	jižní
expozicí	expozice	k1gFnSc7	expozice
(	(	kIx(	(
<g/>
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
i	i	k8xC	i
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
svazích	svah	k1gInPc6	svah
s	s	k7c7	s
východní	východní	k2eAgFnSc7d1	východní
expozicí	expozice	k1gFnSc7	expozice
hrozí	hrozit	k5eAaImIp3nS	hrozit
větší	veliký	k2eAgNnSc4d2	veliký
nebezpečí	nebezpečí	k1gNnSc4	nebezpečí
přízemních	přízemní	k2eAgInPc2d1	přízemní
mrazíků	mrazík	k1gInPc2	mrazík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Srážky	srážka	k1gFnPc1	srážka
===	===	k?	===
</s>
</p>
<p>
<s>
Optimální	optimální	k2eAgNnSc1d1	optimální
je	být	k5eAaImIp3nS	být
množství	množství	k1gNnSc1	množství
srážek	srážka	k1gFnPc2	srážka
kolem	kolem	k7c2	kolem
600	[number]	k4	600
milimetrů	milimetr	k1gInPc2	milimetr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
réva	réva	k1gFnSc1	réva
vinná	vinný	k2eAgFnSc1d1	vinná
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
úhrem	úhrum	k1gNnSc7	úhrum
srážek	srážka	k1gFnPc2	srážka
400-500	[number]	k4	400-500
milimetrů	milimetr	k1gInPc2	milimetr
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgNnSc1d1	důležité
je	být	k5eAaImIp3nS	být
rozložení	rozložení	k1gNnSc1	rozložení
srážek	srážka	k1gFnPc2	srážka
během	během	k7c2	během
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
před	před	k7c7	před
rašením	rašení	k1gNnSc7	rašení
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
nasazování	nasazování	k1gNnSc1	nasazování
bobulí	bobule	k1gFnPc2	bobule
a	a	k8xC	a
při	při	k7c6	při
zaměkání	zaměkání	k1gNnSc6	zaměkání
bobulí	bobule	k1gFnPc2	bobule
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nedostaku	nedostak	k1gInSc6	nedostak
srážek	srážka	k1gFnPc2	srážka
se	se	k3xPyFc4	se
praktikuje	praktikovat	k5eAaImIp3nS	praktikovat
kapková	kapkový	k2eAgFnSc1d1	kapková
závlaha	závlaha	k1gFnSc1	závlaha
<g/>
.	.	kIx.	.
</s>
<s>
Nebezpečné	bezpečný	k2eNgFnPc1d1	nebezpečná
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
révu	réva	k1gFnSc4	réva
vinnou	vinný	k2eAgFnSc4d1	vinná
přívalové	přívalový	k2eAgInPc1d1	přívalový
deště	dešť	k1gInPc1	dešť
<g/>
,	,	kIx,	,
krupobití	krupobití	k1gNnPc1	krupobití
a	a	k8xC	a
také	také	k9	také
rosa	rosa	k1gFnSc1	rosa
během	během	k7c2	během
léta	léto	k1gNnSc2	léto
(	(	kIx(	(
<g/>
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
se	se	k3xPyFc4	se
riziko	riziko	k1gNnSc1	riziko
infekce	infekce	k1gFnSc2	infekce
houbových	houbový	k2eAgFnPc2d1	houbová
chorob	choroba	k1gFnPc2	choroba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
má	mít	k5eAaImIp3nS	mít
příznivý	příznivý	k2eAgInSc4d1	příznivý
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
zrání	zrání	k1gNnSc4	zrání
bobulí	bobule	k1gFnPc2	bobule
(	(	kIx(	(
<g/>
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
se	se	k3xPyFc4	se
obsah	obsah	k1gInSc1	obsah
aromatických	aromatický	k2eAgFnPc2d1	aromatická
látek	látka	k1gFnPc2	látka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Půda	půda	k1gFnSc1	půda
===	===	k?	===
</s>
</p>
<p>
<s>
Ideální	ideální	k2eAgFnSc1d1	ideální
je	být	k5eAaImIp3nS	být
kamenitá	kamenitý	k2eAgFnSc1d1	kamenitá
půda	půda	k1gFnSc1	půda
(	(	kIx(	(
<g/>
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
kyprost	kyprost	k1gFnSc1	kyprost
půdy-obsah	půdybsaha	k1gFnPc2	půdy-obsaha
kyslíku	kyslík	k1gInSc2	kyslík
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
<g/>
,	,	kIx,	,
zabraňuje	zabraňovat	k5eAaImIp3nS	zabraňovat
půdní	půdní	k2eAgInSc1d1	půdní
erozi	eroze	k1gFnSc3	eroze
<g/>
,	,	kIx,	,
vyhřívá	vyhřívat	k5eAaImIp3nS	vyhřívat
půdu	půda	k1gFnSc4	půda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Optimální	optimální	k2eAgFnSc1d1	optimální
je	být	k5eAaImIp3nS	být
půda	půda	k1gFnSc1	půda
písčitá	písčitý	k2eAgFnSc1d1	písčitá
<g/>
,	,	kIx,	,
hlinitopísčitá	hlinitopísčitý	k2eAgFnSc1d1	hlinitopísčitá
<g/>
,	,	kIx,	,
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
hladinou	hladina	k1gFnSc7	hladina
spodní	spodní	k2eAgFnSc2d1	spodní
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Vhodné	vhodný	k2eAgNnSc1d1	vhodné
je	být	k5eAaImIp3nS	být
také	také	k9	také
navozit	navozit	k5eAaPmF	navozit
do	do	k7c2	do
vinice	vinice	k1gFnSc2	vinice
kamení	kamení	k1gNnSc2	kamení
a	a	k8xC	a
pokrýt	pokrýt	k5eAaPmF	pokrýt
jím	jíst	k5eAaImIp1nS	jíst
vinici	vinice	k1gFnSc4	vinice
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nevysychala	vysychat	k5eNaImAgFnS	vysychat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Teplota	teplota	k1gFnSc1	teplota
===	===	k?	===
</s>
</p>
<p>
<s>
Réva	réva	k1gFnSc1	réva
vinná	vinný	k2eAgFnSc1d1	vinná
je	být	k5eAaImIp3nS	být
teplomilná	teplomilný	k2eAgFnSc1d1	teplomilná
rostlina	rostlina	k1gFnSc1	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rašení	rašení	k1gNnSc3	rašení
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
průměrnou	průměrný	k2eAgFnSc4d1	průměrná
denní	denní	k2eAgFnSc4d1	denní
teplotu	teplota	k1gFnSc4	teplota
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
10	[number]	k4	10
°	°	k?	°
<g/>
C.	C.	kA	C.
Réva	réva	k1gFnSc1	réva
je	být	k5eAaImIp3nS	být
nejnáročnější	náročný	k2eAgFnSc1d3	nejnáročnější
na	na	k7c4	na
teplotu	teplota	k1gFnSc4	teplota
v	v	k7c6	v
období	období	k1gNnSc6	období
kvetení	kvetení	k1gNnSc2	kvetení
(	(	kIx(	(
<g/>
30	[number]	k4	30
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
35	[number]	k4	35
°	°	k?	°
<g/>
C	C	kA	C
révu	réva	k1gFnSc4	réva
poškozuje	poškozovat	k5eAaImIp3nS	poškozovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Suma	suma	k1gFnSc1	suma
aktivních	aktivní	k2eAgFnPc2d1	aktivní
teplot	teplota	k1gFnPc2	teplota
(	(	kIx(	(
<g/>
součet	součet	k1gInSc1	součet
průměrných	průměrný	k2eAgFnPc2d1	průměrná
denních	denní	k2eAgFnPc2d1	denní
teplot	teplota	k1gFnPc2	teplota
ve	v	k7c6	v
dnech	den	k1gInPc6	den
nad	nad	k7c7	nad
10	[number]	k4	10
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
činí	činit	k5eAaImIp3nS	činit
2500	[number]	k4	2500
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Původní	původní	k2eAgInSc1d1	původní
areál	areál	k1gInSc1	areál
rozšíření	rozšíření	k1gNnSc2	rozšíření
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
jižní	jižní	k2eAgFnSc4d1	jižní
a	a	k8xC	a
střední	střední	k2eAgFnSc4d1	střední
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
;	;	kIx,	;
v	v	k7c6	v
nejteplejších	teplý	k2eAgNnPc6d3	nejteplejší
obdobích	období	k1gNnPc6	období
čtvrtohor	čtvrtohory	k1gFnPc2	čtvrtohory
se	se	k3xPyFc4	se
divoké	divoký	k2eAgFnSc2d1	divoká
odrůdy	odrůda	k1gFnSc2	odrůda
dostaly	dostat	k5eAaPmAgFnP	dostat
až	až	k9	až
do	do	k7c2	do
jižní	jižní	k2eAgFnSc2d1	jižní
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
východ	východ	k1gInSc4	východ
oblast	oblast	k1gFnSc4	oblast
přirozeného	přirozený	k2eAgInSc2d1	přirozený
výskytu	výskyt	k1gInSc2	výskyt
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
do	do	k7c2	do
Přední	přední	k2eAgFnSc2d1	přední
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
podhůří	podhůří	k1gNnSc2	podhůří
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
a	a	k8xC	a
Střední	střední	k2eAgFnSc2d1	střední
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Roztroušeně	roztroušeně	k6eAd1	roztroušeně
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Íránu	Írán	k1gInSc2	Írán
a	a	k8xC	a
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
severní	severní	k2eAgFnSc2d1	severní
hranice	hranice	k1gFnSc2	hranice
přirozeného	přirozený	k2eAgInSc2d1	přirozený
areálu	areál	k1gInSc2	areál
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
přibližně	přibližně	k6eAd1	přibližně
50	[number]	k4	50
<g/>
°	°	k?	°
s.	s.	k?	s.
š.	š.	k?	š.
</s>
</p>
<p>
<s>
Kulturní	kulturní	k2eAgFnPc1d1	kulturní
odrůdy	odrůda	k1gFnPc1	odrůda
se	se	k3xPyFc4	se
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
v	v	k7c6	v
mírném	mírný	k2eAgInSc6d1	mírný
pásu	pás	k1gInSc6	pás
celé	celý	k2eAgFnSc2d1	celá
zeměkoule	zeměkoule	k1gFnSc2	zeměkoule
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
všechny	všechen	k3xTgInPc4	všechen
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgFnPc4	dva
základní	základní	k2eAgFnPc4d1	základní
vinohradnické	vinohradnický	k2eAgFnPc4d1	vinohradnická
oblasti	oblast	k1gFnPc4	oblast
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
česká	český	k2eAgFnSc1d1	Česká
–	–	k?	–
podoblasti	podoblast	k1gFnSc2	podoblast
mělnická	mělnický	k2eAgFnSc1d1	Mělnická
a	a	k8xC	a
litoměřická	litoměřický	k2eAgFnSc1d1	Litoměřická
</s>
</p>
<p>
<s>
moravská	moravský	k2eAgFnSc1d1	Moravská
–	–	k?	–
podoblasti	podoblast	k1gFnSc3	podoblast
mikulovská	mikulovský	k2eAgFnSc1d1	Mikulovská
<g/>
,	,	kIx,	,
slovácká	slovácký	k2eAgFnSc1d1	Slovácká
<g/>
,	,	kIx,	,
velkopavlovická	velkopavlovický	k2eAgFnSc1d1	velkopavlovická
a	a	k8xC	a
znojemskáJednotlivé	znojemskáJednotlivý	k2eAgFnPc1d1	znojemskáJednotlivý
podoblasti	podoblast	k1gFnPc1	podoblast
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
detailněji	detailně	k6eAd2	detailně
podle	podle	k7c2	podle
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
vinařských	vinařský	k2eAgFnPc2d1	vinařská
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Réva	réva	k1gFnSc1	réva
vinná	vinný	k2eAgFnSc1d1	vinná
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
kulturních	kulturní	k2eAgFnPc2d1	kulturní
rostlin	rostlina	k1gFnPc2	rostlina
pěstovaných	pěstovaný	k2eAgFnPc2d1	pěstovaná
člověkem	člověk	k1gMnSc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
kulturních	kulturní	k2eAgFnPc2d1	kulturní
odrůd	odrůda	k1gFnPc2	odrůda
V.	V.	kA	V.
vinifera	vinifera	k1gFnSc1	vinifera
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
vinifera	vinifera	k1gFnSc1	vinifera
není	být	k5eNaImIp3nS	být
zcela	zcela	k6eAd1	zcela
jednoznačně	jednoznačně	k6eAd1	jednoznačně
vysvětlen	vysvětlen	k2eAgInSc1d1	vysvětlen
<g/>
;	;	kIx,	;
spekuluje	spekulovat	k5eAaImIp3nS	spekulovat
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
<g/>
,	,	kIx,	,
že	že	k8xS	že
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
šlechtěním	šlechtění	k1gNnSc7	šlechtění
divoké	divoký	k2eAgFnSc2d1	divoká
révy	réva	k1gFnSc2	réva
vinné	vinný	k2eAgFnSc2d1	vinná
lesní	lesní	k2eAgFnSc2d1	lesní
(	(	kIx(	(
<g/>
V.	V.	kA	V.
vinifera	vinifera	k1gFnSc1	vinifera
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
sylvestris	sylvestris	k1gInSc1	sylvestris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
někteří	některý	k3yIgMnPc1	některý
autoři	autor	k1gMnPc1	autor
toto	tento	k3xDgNnSc4	tento
zpochybňují	zpochybňovat	k5eAaImIp3nP	zpochybňovat
a	a	k8xC	a
její	její	k3xOp3gFnSc3	její
genezi	geneze	k1gFnSc3	geneze
odvozují	odvozovat	k5eAaImIp3nP	odvozovat
od	od	k7c2	od
dnes	dnes	k6eAd1	dnes
již	již	k6eAd1	již
vyhynulých	vyhynulý	k2eAgInPc2d1	vyhynulý
třetihorních	třetihorní	k2eAgInPc2d1	třetihorní
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
byl	být	k5eAaImAgMnS	být
zapsán	zapsat	k5eAaPmNgMnS	zapsat
do	do	k7c2	do
Guinessovy	Guinessův	k2eAgFnSc2d1	Guinessova
knihy	kniha	k1gFnSc2	kniha
rekordů	rekord	k1gInPc2	rekord
podle	podle	k7c2	podle
historicky	historicky	k6eAd1	historicky
dochovaných	dochovaný	k2eAgInPc2d1	dochovaný
pramenů	pramen	k1gInPc2	pramen
<g/>
,	,	kIx,	,
dobových	dobový	k2eAgFnPc2d1	dobová
kreseb	kresba	k1gFnPc2	kresba
a	a	k8xC	a
analýz	analýza	k1gFnPc2	analýza
vzorků	vzorek	k1gInPc2	vzorek
úředně	úředně	k6eAd1	úředně
nejstarší	starý	k2eAgInSc1d3	nejstarší
exemplář	exemplář	k1gInSc1	exemplář
révy	réva	k1gFnSc2	réva
vinné	vinný	k2eAgFnSc2d1	vinná
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Stara	Stara	k1gMnSc1	Stara
trta	trta	k1gMnSc1	trta
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
můžeme	moct	k5eAaImIp1nP	moct
najít	najít	k5eAaPmF	najít
ve	v	k7c6	v
slovinském	slovinský	k2eAgInSc6d1	slovinský
Mariboru	Maribor	k1gInSc6	Maribor
na	na	k7c4	na
nábřeží	nábřeží	k1gNnSc4	nábřeží
u	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
Dravy	Dravy	k?	Dravy
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
věk	věk	k1gInSc1	věk
se	se	k3xPyFc4	se
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
440	[number]	k4	440
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
průčelí	průčelí	k1gNnSc2	průčelí
jednoposchoďové	jednoposchoďový	k2eAgFnSc2d1	jednoposchoďová
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Kmen	kmen	k1gInSc1	kmen
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
25	[number]	k4	25
cm	cm	kA	cm
se	se	k3xPyFc4	se
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
dvou	dva	k4xCgInPc2	dva
metrů	metr	k1gInPc2	metr
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
na	na	k7c4	na
vodorovné	vodorovný	k2eAgNnSc4d1	vodorovné
vedení	vedení	k1gNnSc4	vedení
a	a	k8xC	a
její	její	k3xOp3gFnPc4	její
větve	větev	k1gFnPc1	větev
mají	mít	k5eAaImIp3nP	mít
délku	délka	k1gFnSc4	délka
přes	přes	k7c4	přes
15	[number]	k4	15
m.	m.	k?	m.
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
původní	původní	k2eAgFnSc1d1	původní
červená	červený	k2eAgFnSc1d1	červená
odrůda	odrůda	k1gFnSc1	odrůda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
plodná	plodný	k2eAgFnSc1d1	plodná
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
odolná	odolný	k2eAgFnSc1d1	odolná
proti	proti	k7c3	proti
nemocem	nemoc	k1gFnPc3	nemoc
a	a	k8xC	a
škůdcům	škůdce	k1gMnPc3	škůdce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Choroby	choroba	k1gFnPc1	choroba
a	a	k8xC	a
škůdci	škůdce	k1gMnPc1	škůdce
==	==	k?	==
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
nejnebezpečnější	bezpečný	k2eNgMnPc4d3	nejnebezpečnější
škůdce	škůdce	k1gMnPc4	škůdce
patří	patřit	k5eAaImIp3nS	patřit
mšička	mšička	k1gFnSc1	mšička
révokaz	révokaz	k1gMnSc1	révokaz
(	(	kIx(	(
<g/>
Viteus	Viteus	k1gMnSc1	Viteus
vitifoliae	vitifolia	k1gFnSc2	vitifolia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
saje	sát	k5eAaImIp3nS	sát
na	na	k7c6	na
kořenech	kořen	k1gInPc6	kořen
révy	réva	k1gFnSc2	réva
a	a	k8xC	a
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
jejich	jejich	k3xOp3gNnPc4	jejich
uhnívání	uhnívání	k1gNnPc4	uhnívání
<g/>
,	,	kIx,	,
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
čehož	což	k3yRnSc2	což
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
úhynu	úhyn	k1gInSc3	úhyn
celé	celý	k2eAgFnSc2d1	celá
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
mšice	mšice	k1gFnSc1	mšice
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zdecimovala	zdecimovat	k5eAaPmAgFnS	zdecimovat
vinice	vinice	k1gFnPc4	vinice
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byla	být	k5eAaImAgFnS	být
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1860	[number]	k4	1860
zavlečena	zavleknout	k5eAaPmNgFnS	zavleknout
z	z	k7c2	z
Ameriky	Amerika	k1gFnSc2	Amerika
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
okrasnou	okrasný	k2eAgFnSc7d1	okrasná
americkou	americký	k2eAgFnSc7d1	americká
révou	réva	k1gFnSc7	réva
(	(	kIx(	(
<g/>
Vitis	Vitis	k1gInSc1	Vitis
labrusca	labrusc	k1gInSc2	labrusc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
révokaz	révokaz	k1gMnSc1	révokaz
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
kontinentu	kontinent	k1gInSc6	kontinent
<g/>
,	,	kIx,	,
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
poprvé	poprvé	k6eAd1	poprvé
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1890	[number]	k4	1890
v	v	k7c4	v
Šatově	šatově	k6eAd1	šatově
<g/>
.	.	kIx.	.
</s>
<s>
Révokazová	Révokazový	k2eAgFnSc1d1	Révokazový
kalamita	kalamita	k1gFnSc1	kalamita
způsobila	způsobit	k5eAaPmAgFnS	způsobit
likvidaci	likvidace	k1gFnSc4	likvidace
většiny	většina	k1gFnSc2	většina
vinic	vinice	k1gFnPc2	vinice
a	a	k8xC	a
musely	muset	k5eAaImAgFnP	muset
být	být	k5eAaImF	být
hledány	hledán	k2eAgFnPc4d1	hledána
cesty	cesta	k1gFnPc4	cesta
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
tomuto	tento	k3xDgMnSc3	tento
škůdci	škůdce	k1gMnSc3	škůdce
čelit	čelit	k5eAaImF	čelit
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
nejúčinnější	účinný	k2eAgInPc1d3	nejúčinnější
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
štěpování	štěpování	k1gNnSc4	štěpování
odrůd	odrůda	k1gFnPc2	odrůda
evropské	evropský	k2eAgFnSc2d1	Evropská
révy	réva	k1gFnSc2	réva
na	na	k7c4	na
odolné	odolný	k2eAgFnPc4d1	odolná
podnože	podnož	k1gFnPc4	podnož
vyšlechtěné	vyšlechtěný	k2eAgFnPc4d1	vyšlechtěná
z	z	k7c2	z
révy	réva	k1gFnSc2	réva
americké	americký	k2eAgFnSc2d1	americká
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
byly	být	k5eAaImAgFnP	být
vyšlechtěny	vyšlechtěn	k2eAgFnPc1d1	vyšlechtěna
hybridní	hybridní	k2eAgFnPc1d1	hybridní
rezistentní	rezistentní	k2eAgFnPc1d1	rezistentní
odrůdy	odrůda	k1gFnPc1	odrůda
<g/>
,	,	kIx,	,
vzniklé	vzniklý	k2eAgNnSc1d1	vzniklé
křížením	křížení	k1gNnSc7	křížení
odrůd	odrůda	k1gFnPc2	odrůda
evropské	evropský	k2eAgFnSc2d1	Evropská
révy	réva	k1gFnSc2	réva
s	s	k7c7	s
révou	réva	k1gFnSc7	réva
americkou	americký	k2eAgFnSc7d1	americká
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
vinice	vinice	k1gFnPc4	vinice
stále	stále	k6eAd1	stále
révokazem	révokaz	k1gMnSc7	révokaz
napadeny	napaden	k2eAgInPc4d1	napaden
<g/>
,	,	kIx,	,
i	i	k8xC	i
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
používat	používat	k5eAaImF	používat
sazenice	sazenice	k1gFnPc4	sazenice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
vůči	vůči	k7c3	vůči
němu	on	k3xPp3gNnSc3	on
odolné	odolný	k2eAgFnPc1d1	odolná
<g/>
.	.	kIx.	.
</s>
<s>
Mšička	mšička	k1gFnSc1	mšička
révokaz	révokaz	k1gMnSc1	révokaz
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
karanténním	karanténní	k2eAgInSc7d1	karanténní
druhem	druh	k1gInSc7	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalšími	další	k2eAgMnPc7d1	další
častými	častý	k2eAgMnPc7d1	častý
škůdci	škůdce	k1gMnPc7	škůdce
jsou	být	k5eAaImIp3nP	být
vlnovník	vlnovník	k1gMnSc1	vlnovník
révový	révový	k2eAgMnSc1d1	révový
(	(	kIx(	(
<g/>
Colomerus	Colomerus	k1gInSc1	Colomerus
vitis	vitis	k1gFnSc2	vitis
<g/>
)	)	kIx)	)
způsobující	způsobující	k2eAgFnSc1d1	způsobující
plstnatost	plstnatost	k1gFnSc1	plstnatost
rubu	rub	k1gInSc2	rub
listů	list	k1gInPc2	list
(	(	kIx(	(
<g/>
spodní	spodní	k2eAgFnPc1d1	spodní
strany	strana	k1gFnPc1	strana
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sviluška	sviluška	k1gFnSc1	sviluška
ovocná	ovocný	k2eAgFnSc1d1	ovocná
(	(	kIx(	(
<g/>
Panonychus	Panonychus	k1gMnSc1	Panonychus
ulmi	ulm	k1gFnSc2	ulm
<g/>
)	)	kIx)	)
a	a	k8xC	a
s.	s.	k?	s.
chmelová	chmelový	k2eAgFnSc1d1	chmelová
(	(	kIx(	(
<g/>
Tetranychus	Tetranychus	k1gInSc1	Tetranychus
urticae	urtica	k1gInSc2	urtica
<g/>
)	)	kIx)	)
sající	sající	k2eAgFnSc4d1	sající
na	na	k7c6	na
listech	list	k1gInPc6	list
a	a	k8xC	a
housenky	housenka	k1gFnPc1	housenka
obaleče	obaleč	k1gMnSc2	obaleč
jednopásého	jednopásý	k2eAgMnSc2d1	jednopásý
(	(	kIx(	(
<g/>
Eupoecilia	Eupoecilius	k1gMnSc2	Eupoecilius
ambiguella	ambiguell	k1gMnSc2	ambiguell
<g/>
)	)	kIx)	)
a	a	k8xC	a
obaleče	obaleč	k1gMnSc2	obaleč
mramorovaného	mramorovaný	k2eAgMnSc2d1	mramorovaný
(	(	kIx(	(
<g/>
Lobesia	Lobesius	k1gMnSc2	Lobesius
botrana	botran	k1gMnSc2	botran
<g/>
)	)	kIx)	)
poškozující	poškozující	k2eAgFnSc2d1	poškozující
rostliny	rostlina	k1gFnSc2	rostlina
žírem	žír	k1gInSc7	žír
poupat	poupě	k1gNnPc2	poupě
a	a	k8xC	a
bobulí	bobule	k1gFnPc2	bobule
<g/>
.	.	kIx.	.
</s>
<s>
Částečná	částečný	k2eAgNnPc1d1	částečné
poškození	poškození	k1gNnPc1	poškození
působí	působit	k5eAaImIp3nP	působit
i	i	k9	i
zobonoska	zobonoska	k1gFnSc1	zobonoska
révová	révový	k2eAgFnSc1d1	révová
(	(	kIx(	(
<g/>
Byctiscus	Byctiscus	k1gMnSc1	Byctiscus
betulae	betulaat	k5eAaPmIp3nS	betulaat
<g/>
)	)	kIx)	)
a	a	k8xC	a
hálčivec	hálčivec	k1gMnSc1	hálčivec
révový	révový	k2eAgMnSc1d1	révový
(	(	kIx(	(
<g/>
Calepitrimerus	Calepitrimerus	k1gInSc1	Calepitrimerus
vitis	vitis	k1gFnSc2	vitis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
zobonosek	zobonoska	k1gFnPc2	zobonoska
vykusují	vykusovat	k5eAaImIp3nP	vykusovat
pruhy	pruh	k1gInPc4	pruh
v	v	k7c6	v
listech	list	k1gInPc6	list
a	a	k8xC	a
následně	následně	k6eAd1	následně
je	on	k3xPp3gInPc4	on
stáčejí	stáčet	k5eAaImIp3nP	stáčet
do	do	k7c2	do
kornoutků	kornoutek	k1gInPc2	kornoutek
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
zničené	zničený	k2eAgInPc1d1	zničený
listy	list	k1gInPc1	list
opadávají	opadávat	k5eAaImIp3nP	opadávat
<g/>
.	.	kIx.	.
</s>
<s>
Hálčivec	Hálčivec	k1gMnSc1	Hálčivec
naopak	naopak	k6eAd1	naopak
saje	sát	k5eAaImIp3nS	sát
na	na	k7c6	na
mladých	mladý	k2eAgInPc6d1	mladý
listech	list	k1gInPc6	list
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
může	moct	k5eAaImIp3nS	moct
při	při	k7c6	při
silném	silný	k2eAgNnSc6d1	silné
napadení	napadení	k1gNnSc6	napadení
vézt	vézt	k5eAaImF	vézt
k	k	k7c3	k
oslabení	oslabení	k1gNnSc3	oslabení
rostliny	rostlina	k1gFnSc2	rostlina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
nejčastější	častý	k2eAgFnPc4d3	nejčastější
choroby	choroba	k1gFnPc4	choroba
révy	réva	k1gFnSc2	réva
vinné	vinný	k2eAgFnSc6d1	vinná
patří	patřit	k5eAaImIp3nS	patřit
virový	virový	k2eAgInSc1d1	virový
roncet	roncet	k5eAaPmF	roncet
(	(	kIx(	(
<g/>
způsobovaný	způsobovaný	k2eAgInSc1d1	způsobovaný
virem	vir	k1gInSc7	vir
roncetu	roncet	k1gInSc2	roncet
révy	réva	k1gFnSc2	réva
vinné	vinný	k2eAgFnSc2d1	vinná
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
houbové	houbový	k2eAgFnPc1d1	houbová
choroby	choroba	k1gFnPc1	choroba
padlí	padlí	k1gNnPc2	padlí
révové	révový	k2eAgFnPc1d1	révová
(	(	kIx(	(
<g/>
Oidium	Oidium	k1gNnSc4	Oidium
tuckerii	tuckerie	k1gFnSc4	tuckerie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plíseň	plíseň	k1gFnSc4	plíseň
révy	réva	k1gFnSc2	réva
vinné	vinný	k2eAgNnSc1d1	vinné
(	(	kIx(	(
<g/>
způsobovaná	způsobovaný	k2eAgFnSc1d1	způsobovaná
houbou	houba	k1gFnSc7	houba
Plasmopara	Plasmopara	k1gFnSc1	Plasmopara
viticola	viticola	k1gFnSc1	viticola
<g/>
)	)	kIx)	)
a	a	k8xC	a
šedá	šedý	k2eAgFnSc1d1	šedá
plesnivost	plesnivost	k1gFnSc1	plesnivost
čili	čili	k8xC	čili
plíseň	plíseň	k1gFnSc1	plíseň
šedá	šedá	k1gFnSc1	šedá
(	(	kIx(	(
<g/>
způsobovaná	způsobovaný	k2eAgFnSc1d1	způsobovaná
vřeckovýtrusnou	vřeckovýtrusný	k2eAgFnSc7d1	vřeckovýtrusná
houbou	houba	k1gFnSc7	houba
Botryotinia	Botryotinium	k1gNnSc2	Botryotinium
fuckeliana	fuckeliana	k1gFnSc1	fuckeliana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Taxonomická	taxonomický	k2eAgFnSc1d1	Taxonomická
poznámka	poznámka	k1gFnSc1	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
zahradnické	zahradnický	k2eAgFnSc6d1	zahradnická
<g/>
,	,	kIx,	,
zemědělské	zemědělský	k2eAgFnSc3d1	zemědělská
a	a	k8xC	a
zejména	zejména	k9	zejména
vinařské	vinařský	k2eAgFnSc3d1	vinařská
literatuře	literatura	k1gFnSc3	literatura
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
označování	označování	k1gNnSc4	označování
pěstované	pěstovaný	k2eAgFnSc2d1	pěstovaná
ušlechtilé	ušlechtilý	k2eAgFnSc2d1	ušlechtilá
révy	réva	k1gFnSc2	réva
rozšířeno	rozšířen	k2eAgNnSc1d1	rozšířeno
používání	používání	k1gNnSc1	používání
jména	jméno	k1gNnSc2	jméno
Vitis	Vitis	k1gFnSc2	Vitis
vinifera	vinifera	k1gFnSc1	vinifera
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
sativa	sativa	k1gFnSc1	sativa
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
jméno	jméno	k1gNnSc1	jméno
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
poddruhu	poddruh	k1gInSc2	poddruh
zavedl	zavést	k5eAaPmAgInS	zavést
do	do	k7c2	do
botanické	botanický	k2eAgFnSc2d1	botanická
nomenklatury	nomenklatura	k1gFnSc2	nomenklatura
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
obsáhlém	obsáhlý	k2eAgInSc6d1	obsáhlý
mnohasvazkovém	mnohasvazkový	k2eAgInSc6d1	mnohasvazkový
díle	díl	k1gInSc6	díl
"	"	kIx"	"
<g/>
Illustrierte	Illustriert	k1gInSc5	Illustriert
Flora	Flora	k1gFnSc1	Flora
Mitteleuropas	Mitteleuropas	k1gMnSc1	Mitteleuropas
<g/>
"	"	kIx"	"
německý	německý	k2eAgMnSc1d1	německý
botanik	botanik	k1gMnSc1	botanik
Gustav	Gustav	k1gMnSc1	Gustav
Hegi	Hegi	k1gNnSc4	Hegi
z	z	k7c2	z
mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
však	však	k9	však
jím	jíst	k5eAaImIp1nS	jíst
publikovaný	publikovaný	k2eAgInSc1d1	publikovaný
poddruh	poddruh	k1gInSc1	poddruh
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
základnímu	základní	k2eAgInSc3d1	základní
typu	typ	k1gInSc3	typ
V.	V.	kA	V.
vinifera	vinifero	k1gNnPc4	vinifero
<g/>
,	,	kIx,	,
popsanému	popsaný	k2eAgInSc3d1	popsaný
Linnéem	Linné	k1gInSc7	Linné
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1753	[number]	k4	1753
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
automaticky	automaticky	k6eAd1	automaticky
podle	podle	k7c2	podle
současných	současný	k2eAgNnPc2d1	současné
pravidel	pravidlo	k1gNnPc2	pravidlo
botanické	botanický	k2eAgFnSc2d1	botanická
nomenklatury	nomenklatura	k1gFnSc2	nomenklatura
implikuje	implikovat	k5eAaImIp3nS	implikovat
existenci	existence	k1gFnSc4	existence
všech	všecek	k3xTgFnPc2	všecek
základních	základní	k2eAgFnPc2d1	základní
infraspecifických	infraspecifický	k2eAgFnPc2d1	infraspecifický
kategorií	kategorie	k1gFnPc2	kategorie
a	a	k8xC	a
tedy	tedy	k9	tedy
i	i	k9	i
V.	V.	kA	V.
vinifera	vinifera	k1gFnSc1	vinifera
subsp	subsp	k1gMnSc1	subsp
<g/>
.	.	kIx.	.
vinifera	vinifera	k1gFnSc1	vinifera
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
Hegiho	Hegi	k1gMnSc2	Hegi
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
platně	platně	k6eAd1	platně
publikované	publikovaný	k2eAgFnPc4d1	publikovaná
<g/>
,	,	kIx,	,
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
mladší	mladý	k2eAgNnSc4d2	mladší
synonymum	synonymum	k1gNnSc4	synonymum
a	a	k8xC	a
jako	jako	k8xC	jako
takové	takový	k3xDgNnSc1	takový
je	být	k5eAaImIp3nS	být
odmítnout	odmítnout	k5eAaPmF	odmítnout
jako	jako	k9	jako
jméno	jméno	k1gNnSc1	jméno
nadbytečné	nadbytečný	k2eAgNnSc1d1	nadbytečné
(	(	kIx(	(
<g/>
nomen	nomen	k2eAgInSc1d1	nomen
superfluum	superfluum	k1gInSc1	superfluum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
pro	pro	k7c4	pro
basionym	basionym	k1gInSc4	basionym
Hegiho	Hegi	k1gMnSc2	Hegi
jména	jméno	k1gNnSc2	jméno
V.	V.	kA	V.
vinifera	vinifer	k1gMnSc2	vinifer
var.	var.	k?	var.
sativa	sativ	k1gMnSc2	sativ
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1805	[number]	k4	1805
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Původ	původ	k1gInSc1	původ
jména	jméno	k1gNnSc2	jméno
==	==	k?	==
</s>
</p>
<p>
<s>
Vědecké	vědecký	k2eAgNnSc1d1	vědecké
jméno	jméno	k1gNnSc1	jméno
rodu	rod	k1gInSc2	rod
Vitis	Vitis	k1gFnSc2	Vitis
bylo	být	k5eAaImAgNnS	být
převzato	převzít	k5eAaPmNgNnS	převzít
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
názvu	název	k1gInSc2	název
pro	pro	k7c4	pro
keř	keř	k1gInSc4	keř
révy	réva	k1gFnSc2	réva
vinné	vinný	k2eAgFnSc2d1	vinná
<g/>
,	,	kIx,	,
v	v	k7c6	v
přeneseném	přenesený	k2eAgInSc6d1	přenesený
významu	význam	k1gInSc6	význam
používaný	používaný	k2eAgInSc4d1	používaný
též	též	k9	též
pro	pro	k7c4	pro
pojem	pojem	k1gInSc4	pojem
"	"	kIx"	"
<g/>
úponek	úponek	k1gInSc4	úponek
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
druhový	druhový	k2eAgInSc1d1	druhový
přívlastek	přívlastek	k1gInSc1	přívlastek
vinifera	vinifero	k1gNnSc2	vinifero
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
sloučením	sloučení	k1gNnSc7	sloučení
kmenů	kmen	k1gInPc2	kmen
latinských	latinský	k2eAgNnPc2d1	latinské
slov	slovo	k1gNnPc2	slovo
vinum	vinum	k1gInSc1	vinum
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
"	"	kIx"	"
<g/>
víno	víno	k1gNnSc1	víno
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
ferens	ferens	k6eAd1	ferens
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
nesoucí	nesoucí	k2eAgMnSc1d1	nesoucí
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
významu	význam	k1gInSc6	význam
"	"	kIx"	"
<g/>
přinášející	přinášející	k2eAgNnSc1d1	přinášející
víno	víno	k1gNnSc1	víno
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Gastronomie	gastronomie	k1gFnSc2	gastronomie
==	==	k?	==
</s>
</p>
<p>
<s>
Réva	réva	k1gFnSc1	réva
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
i	i	k9	i
jako	jako	k9	jako
koření	kořenit	k5eAaImIp3nS	kořenit
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
malým	malý	k2eAgNnSc7d1	malé
množstvím	množství	k1gNnSc7	množství
vína	víno	k1gNnSc2	víno
k	k	k7c3	k
dochucování	dochucování	k1gNnSc3	dochucování
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
,	,	kIx,	,
zvěřiny	zvěřina	k1gFnSc2	zvěřina
a	a	k8xC	a
masa	maso	k1gNnSc2	maso
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
révy	réva	k1gFnSc2	réva
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
kvasný	kvasný	k2eAgInSc1d1	kvasný
vinný	vinný	k2eAgInSc1d1	vinný
ocet	ocet	k1gInSc1	ocet
<g/>
,	,	kIx,	,
vhodný	vhodný	k2eAgInSc1d1	vhodný
do	do	k7c2	do
jarních	jarní	k2eAgInPc2d1	jarní
salátů	salát	k1gInPc2	salát
a	a	k8xC	a
oblíbené	oblíbený	k2eAgFnPc4d1	oblíbená
rozinky	rozinka	k1gFnPc4	rozinka
či	či	k8xC	či
sultánky	sultánka	k1gFnPc4	sultánka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Bohumil	Bohumil	k1gMnSc1	Bohumil
Slavík	Slavík	k1gMnSc1	Slavík
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
:	:	kIx,	:
Květena	květena	k1gFnSc1	květena
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
–	–	k?	–
díl	díl	k1gInSc1	díl
5	[number]	k4	5
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
590	[number]	k4	590
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
442-444	[number]	k4	442-444
</s>
</p>
<p>
<s>
Christian	Christian	k1gMnSc1	Christian
Callec	Callec	k1gMnSc1	Callec
<g/>
:	:	kIx,	:
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
vína	víno	k1gNnSc2	víno
<g/>
,	,	kIx,	,
Rebo	Rebo	k1gNnSc1	Rebo
<g/>
,	,	kIx,	,
Čestlice	Čestlice	k1gFnSc1	Čestlice
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7234-068-9	[number]	k4	80-7234-068-9
</s>
</p>
<p>
<s>
Tom	Tom	k1gMnSc1	Tom
Stevenson	Stevenson	k1gMnSc1	Stevenson
<g/>
:	:	kIx,	:
Nová	nový	k2eAgFnSc1d1	nová
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
vín	víno	k1gNnPc2	víno
<g/>
,	,	kIx,	,
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-242-0856-3	[number]	k4	80-242-0856-3
</s>
</p>
<p>
<s>
Tom	Tom	k1gMnSc1	Tom
Stevenson	Stevenson	k1gMnSc1	Stevenson
<g/>
:	:	kIx,	:
Světová	světový	k2eAgFnSc1d1	světová
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
vín	víno	k1gNnPc2	víno
–	–	k?	–
unikátní	unikátní	k2eAgMnSc1d1	unikátní
průvodce	průvodce	k1gMnSc1	průvodce
víny	vína	k1gFnSc2	vína
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
Balios	Balios	k1gInSc1	Balios
–	–	k?	–
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1999	[number]	k4	1999
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-242-0222-0	[number]	k4	80-242-0222-0
</s>
</p>
<p>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
Kraus	Kraus	k1gMnSc1	Kraus
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Kuttelvašer	Kuttelvašer	k1gMnSc1	Kuttelvašer
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
Vurm	Vurm	k1gMnSc1	Vurm
<g/>
:	:	kIx,	:
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
českého	český	k2eAgNnSc2d1	české
a	a	k8xC	a
moravského	moravský	k2eAgNnSc2d1	Moravské
vína	víno	k1gNnSc2	víno
<g/>
,	,	kIx,	,
Melantrich	Melantrich	k1gInSc1	Melantrich
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7023-250-1	[number]	k4	80-7023-250-1
</s>
</p>
<p>
<s>
Vilém	Vilém	k1gMnSc1	Vilém
Kraus	Kraus	k1gMnSc1	Kraus
<g/>
,	,	kIx,	,
Zuzana	Zuzana	k1gFnSc1	Zuzana
Foffová	Foffový	k2eAgFnSc1d1	Foffová
<g/>
,	,	kIx,	,
Bohumil	Bohumil	k1gMnSc1	Bohumil
Vurm	Vurm	k1gMnSc1	Vurm
<g/>
,	,	kIx,	,
Dáša	Dáša	k1gFnSc1	Dáša
Krausová	Krausová	k1gFnSc1	Krausová
<g/>
:	:	kIx,	:
Nová	nový	k2eAgFnSc1d1	nová
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
českého	český	k2eAgNnSc2d1	české
a	a	k8xC	a
moravského	moravský	k2eAgNnSc2d1	Moravské
vína	víno	k1gNnSc2	víno
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
Praga	Praga	k1gFnSc1	Praga
Mystica	Mystica	k1gFnSc1	Mystica
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86767	[number]	k4	86767
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
48-49	[number]	k4	48-49
</s>
</p>
<p>
<s>
ŘÍHA	Říha	k1gMnSc1	Říha
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
ovoce	ovoce	k1gNnSc1	ovoce
:	:	kIx,	:
Díl	díl	k1gInSc4	díl
V.	V.	kA	V.
Vinná	vinný	k2eAgFnSc1d1	vinná
réva	réva	k1gFnSc1	réva
ku	k	k7c3	k
pěstování	pěstování	k1gNnSc3	pěstování
ovoce	ovoce	k1gNnSc1	ovoce
tabulového	tabulový	k2eAgNnSc2d1	tabulové
a	a	k8xC	a
ku	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
vína	víno	k1gNnSc2	víno
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ovocnický	ovocnický	k2eAgInSc1d1	ovocnický
spolek	spolek	k1gInSc1	spolek
pro	pro	k7c4	pro
království	království	k1gNnSc4	království
České	český	k2eAgFnSc2d1	Česká
<g/>
,	,	kIx,	,
1918	[number]	k4	1918
<g/>
.	.	kIx.	.
61	[number]	k4	61
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
réva	réva	k1gFnSc1	réva
vinná	vinný	k2eAgFnSc1d1	vinná
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
réva	réva	k1gFnSc1	réva
vinná	vinný	k2eAgFnSc1d1	vinná
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Vitis	Vitis	k1gFnSc2	Vitis
vinifera	vinifero	k1gNnSc2	vinifero
ve	v	k7c6	v
Wikidruzích	Wikidruh	k1gInPc6	Wikidruh
</s>
</p>
<p>
<s>
Réva	réva	k1gFnSc1	réva
vinná	vinný	k2eAgFnSc1d1	vinná
na	na	k7c6	na
biolibu	biolib	k1gInSc6	biolib
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Genofondové	Genofondový	k2eAgFnPc4d1	Genofondový
informace	informace	k1gFnPc4	informace
====	====	k?	====
</s>
</p>
<p>
<s>
Soubor	soubor	k1gInSc1	soubor
České	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
kolekce	kolekce	k1gFnSc2	kolekce
rodu	rod	k1gInSc2	rod
Vitis	Vitis	k1gFnSc2	Vitis
</s>
</p>
<p>
<s>
Soubor	soubor	k1gInSc1	soubor
kolekce	kolekce	k1gFnSc2	kolekce
rodu	rod	k1gInSc2	rod
Vitis	Vitis	k1gFnSc2	Vitis
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
evropské	evropský	k2eAgFnSc2d1	Evropská
databáze	databáze	k1gFnSc2	databáze
</s>
</p>
