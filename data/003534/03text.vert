<s>
Počasí	počasí	k1gNnSc1	počasí
je	být	k5eAaImIp3nS	být
okamžitý	okamžitý	k2eAgInSc4d1	okamžitý
stav	stav	k1gInSc4	stav
v	v	k7c6	v
ovzduší	ovzduší	k1gNnSc6	ovzduší
na	na	k7c6	na
určitém	určitý	k2eAgNnSc6d1	určité
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
stavem	stav	k1gInSc7	stav
všech	všecek	k3xTgInPc2	všecek
atmosférických	atmosférický	k2eAgInPc2d1	atmosférický
jevů	jev	k1gInPc2	jev
pozorovaných	pozorovaný	k2eAgInPc2d1	pozorovaný
na	na	k7c6	na
určitém	určitý	k2eAgNnSc6d1	určité
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
v	v	k7c6	v
určitém	určitý	k2eAgInSc6d1	určitý
krátkém	krátký	k2eAgInSc6d1	krátký
časovém	časový	k2eAgInSc6d1	časový
úseku	úsek	k1gInSc6	úsek
nebo	nebo	k8xC	nebo
okamžiku	okamžik	k1gInSc6	okamžik
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
se	se	k3xPyFc4	se
popisuje	popisovat	k5eAaImIp3nS	popisovat
souborem	soubor	k1gInSc7	soubor
hodnot	hodnota	k1gFnPc2	hodnota
meteorologických	meteorologický	k2eAgInPc2d1	meteorologický
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgFnP	být
naměřeny	naměřen	k2eAgInPc1d1	naměřen
meteorologickými	meteorologický	k2eAgInPc7d1	meteorologický
přístroji	přístroj	k1gInPc7	přístroj
nebo	nebo	k8xC	nebo
zjištěny	zjistit	k5eAaPmNgInP	zjistit
pozorovatelem	pozorovatel	k1gMnSc7	pozorovatel
(	(	kIx(	(
<g/>
např.	např.	kA	např.
teplota	teplota	k1gFnSc1	teplota
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
stav	stav	k1gInSc1	stav
oblačnosti	oblačnost	k1gFnSc2	oblačnost
<g/>
,	,	kIx,	,
rychlost	rychlost	k1gFnSc4	rychlost
a	a	k8xC	a
směr	směr	k1gInSc4	směr
větru	vítr	k1gInSc2	vítr
<g/>
,	,	kIx,	,
déšť	déšť	k1gInSc1	déšť
<g/>
,	,	kIx,	,
sněžení	sněžení	k1gNnSc1	sněžení
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
</s>
<s>
Změny	změna	k1gFnPc1	změna
počasí	počasí	k1gNnSc2	počasí
jsou	být	k5eAaImIp3nP	být
způsobeny	způsoben	k2eAgFnPc1d1	způsobena
především	především	k9	především
zemskou	zemský	k2eAgFnSc7d1	zemská
rotací	rotace	k1gFnSc7	rotace
<g/>
.	.	kIx.	.
</s>
<s>
Ohromné	ohromný	k2eAgFnPc1d1	ohromná
masy	masa	k1gFnPc1	masa
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
vody	voda	k1gFnSc2	voda
vlivem	vlivem	k7c2	vlivem
zemské	zemský	k2eAgFnSc2d1	zemská
rotace	rotace	k1gFnSc2	rotace
mají	mít	k5eAaImIp3nP	mít
na	na	k7c4	na
severní	severní	k2eAgFnSc4d1	severní
polokouli	polokoule	k1gFnSc4	polokoule
tendenci	tendence	k1gFnSc4	tendence
pohybovat	pohybovat	k5eAaImF	pohybovat
se	se	k3xPyFc4	se
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
se	se	k3xPyFc4	se
tyto	tento	k3xDgFnPc1	tento
masy	masa	k1gFnPc1	masa
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
opačným	opačný	k2eAgInSc7d1	opačný
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Počasí	počasí	k1gNnSc1	počasí
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
jedinečným	jedinečný	k2eAgInSc7d1	jedinečný
stavem	stav	k1gInSc7	stav
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Počasí	počasí	k1gNnSc1	počasí
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
chápáno	chápat	k5eAaImNgNnS	chápat
jako	jako	k8xC	jako
stav	stav	k1gInSc1	stav
troposféry	troposféra	k1gFnSc2	troposféra
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
člověku	člověk	k1gMnSc3	člověk
nejblíže	blízce	k6eAd3	blízce
a	a	k8xC	a
bezprostředně	bezprostředně	k6eAd1	bezprostředně
ho	on	k3xPp3gMnSc4	on
obklopuje	obklopovat	k5eAaImIp3nS	obklopovat
<g/>
.	.	kIx.	.
</s>
<s>
Obecný	obecný	k2eAgInSc1d1	obecný
typ	typ	k1gInSc1	typ
počasí	počasí	k1gNnSc2	počasí
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
podnebí	podnebí	k1gNnSc1	podnebí
nebo	nebo	k8xC	nebo
klima	klima	k1gNnSc1	klima
<g/>
.	.	kIx.	.
</s>
<s>
Počasí	počasí	k1gNnSc1	počasí
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
měnit	měnit	k5eAaImF	měnit
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
<g/>
,	,	kIx,	,
změna	změna	k1gFnSc1	změna
klimatu	klima	k1gNnSc2	klima
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
velmi	velmi	k6eAd1	velmi
pozvolná	pozvolný	k2eAgFnSc1d1	pozvolná
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
počasí	počasí	k1gNnSc2	počasí
se	se	k3xPyFc4	se
také	také	k9	také
používá	používat	k5eAaImIp3nS	používat
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
používalo	používat	k5eAaImAgNnS	používat
<g/>
,	,	kIx,	,
termínu	termín	k1gInSc3	termín
"	"	kIx"	"
<g/>
povětří	povětří	k1gNnSc3	povětří
<g/>
"	"	kIx"	"
či	či	k8xC	či
"	"	kIx"	"
<g/>
povětrnost	povětrnost	k1gFnSc1	povětrnost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
termíny	termín	k1gInPc1	termín
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
zastaralé	zastaralý	k2eAgFnPc1d1	zastaralá
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
také	také	k9	také
jiný	jiný	k2eAgInSc4d1	jiný
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
pozornost	pozornost	k1gFnSc1	pozornost
je	být	k5eAaImIp3nS	být
věnována	věnovat	k5eAaPmNgFnS	věnovat
předpovědi	předpověď	k1gFnSc3	předpověď
počasí	počasí	k1gNnSc1	počasí
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
počasí	počasí	k1gNnSc1	počasí
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
všechny	všechen	k3xTgFnPc4	všechen
lidské	lidský	k2eAgFnPc4d1	lidská
činnosti	činnost	k1gFnPc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Počasím	počasí	k1gNnSc7	počasí
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
meteorologie	meteorologie	k1gFnSc1	meteorologie
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
fyzika	fyzika	k1gFnSc1	fyzika
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Nejchladnější	chladný	k2eAgFnSc1d3	nejchladnější
teplota	teplota	k1gFnSc1	teplota
byla	být	k5eAaImAgFnS	být
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
na	na	k7c6	na
výzkumné	výzkumný	k2eAgFnSc6d1	výzkumná
stanici	stanice	k1gFnSc6	stanice
Vostok	Vostok	k1gInSc1	Vostok
v	v	k7c6	v
Antarktidě	Antarktida	k1gFnSc6	Antarktida
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
21	[number]	k4	21
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1983	[number]	k4	1983
naměřili	naměřit	k5eAaBmAgMnP	naměřit
-89,2	-89,2	k4	-89,2
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
<s>
Nejtepleji	teple	k6eAd3	teple
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
libyjské	libyjský	k2eAgFnSc6d1	Libyjská
El	Ela	k1gFnPc2	Ela
'	'	kIx"	'
<g/>
Azizie	Azizie	k1gFnSc1	Azizie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	s	k7c7	s
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1922	[number]	k4	1922
teplota	teplota	k1gFnSc1	teplota
vyšplhala	vyšplhat	k5eAaPmAgFnS	vyšplhat
na	na	k7c4	na
57,8	[number]	k4	57,8
°	°	k?	°
<g/>
C.	C.	kA	C.
Nejsušším	suchý	k2eAgNnSc7d3	nejsušší
místem	místo	k1gNnSc7	místo
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
Arica	Arica	k1gFnSc1	Arica
v	v	k7c6	v
Chile	Chile	k1gNnSc6	Chile
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
téměř	téměř	k6eAd1	téměř
vůbec	vůbec	k9	vůbec
neprší	pršet	k5eNaImIp3nS	pršet
<g/>
.	.	kIx.	.
</s>
<s>
Žádné	žádný	k3yNgFnPc1	žádný
srážky	srážka	k1gFnPc1	srážka
nebyly	být	k5eNaImAgFnP	být
zaznamenány	zaznamenat	k5eAaPmNgFnP	zaznamenat
téměř	téměř	k6eAd1	téměř
14	[number]	k4	14
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc4	podnebí
Klima	klima	k1gNnSc4	klima
Globální	globální	k2eAgFnSc2d1	globální
cirkulace	cirkulace	k1gFnSc2	cirkulace
atmosféry	atmosféra	k1gFnSc2	atmosféra
Atmosféra	atmosféra	k1gFnSc1	atmosféra
Atmosféra	atmosféra	k1gFnSc1	atmosféra
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
počasí	počasí	k1gNnSc2	počasí
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
počasí	počasí	k1gNnSc2	počasí
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
www.chmu.cz	www.chmu.cz	k1gInSc1	www.chmu.cz
–	–	k?	–
Český	český	k2eAgInSc1d1	český
hydrometeorologický	hydrometeorologický	k2eAgInSc1d1	hydrometeorologický
ústav	ústav	k1gInSc1	ústav
www.yr.no	www.yr.no	k1gNnSc1	www.yr.no
www.meteocentrum.cz	www.meteocentrum.cz	k1gInSc1	www.meteocentrum.cz
www.meteopress.cz	www.meteopress.cz	k1gMnSc1	www.meteopress.cz
</s>
