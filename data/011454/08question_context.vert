<s>
Andrew	Andrew	k?	Andrew
Jackson	Jackson	k1gMnSc1	Jackson
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1767	[number]	k4	1767
-	-	kIx~	-
8	[number]	k4	8
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1845	[number]	k4	1845
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
sedmý	sedmý	k4xOgMnSc1	sedmý
prezident	prezident	k1gMnSc1	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
(	(	kIx(	(
<g/>
1829	[number]	k4	1829
<g/>
-	-	kIx~	-
<g/>
1837	[number]	k4	1837
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
slogan	slogan	k1gInSc1	slogan
zněl	znět	k5eAaImAgInS	znět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nechť	nechť	k9	nechť
vládne	vládnout	k5eAaImIp3nS	vládnout
lid	lid	k1gInSc1	lid
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
lid	lid	k1gInSc4	lid
nepovažoval	považovat	k5eNaImAgMnS	považovat
indiánské	indiánský	k2eAgNnSc4d1	indiánské
a	a	k8xC	a
černošské	černošský	k2eAgNnSc4d1	černošské
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
