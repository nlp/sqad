<s>
Válka	válka	k1gFnSc1
o	o	k7c4
španělské	španělský	k2eAgNnSc4d1
dědictví	dědictví	k1gNnSc4
</s>
<s>
O	o	k7c6
severoamerickém	severoamerický	k2eAgInSc6d1
konfliktu	konflikt	k1gInSc6
války	válka	k1gFnSc2
o	o	k7c4
španělské	španělský	k2eAgNnSc4d1
dědictví	dědictví	k1gNnSc4
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Válka	Válek	k1gMnSc4
královny	královna	k1gFnSc2
Anny	Anna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Válka	válka	k1gFnSc1
o	o	k7c4
dědictví	dědictví	k1gNnSc4
španělské	španělský	k2eAgFnSc2d1
</s>
<s>
Vévoda	vévoda	k1gMnSc1
z	z	k7c2
Vendôme	Vendôm	k1gInSc5
a	a	k8xC
Filip	Filip	k1gMnSc1
V.	V.	kA
Španělský	španělský	k2eAgMnSc1d1
po	po	k7c6
vítězné	vítězný	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
u	u	k7c2
Villaviciosa	Villaviciosa	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1710	#num#	k4
</s>
<s>
trvání	trvání	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
1701	#num#	k4
<g/>
–	–	k?
<g/>
1714	#num#	k4
</s>
<s>
místo	místo	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
Západní	západní	k2eAgFnSc1d1
Evropa	Evropa	k1gFnSc1
<g/>
,	,	kIx,
Severní	severní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
<g/>
,	,	kIx,
Jižní	jižní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
</s>
<s>
casus	casus	k1gInSc4
belli	bell	k1gMnPc1
<g/>
:	:	kIx,
</s>
<s>
vymření	vymření	k1gNnSc1
španělské	španělský	k2eAgFnSc2d1
linie	linie	k1gFnSc2
Habsburků	Habsburk	k1gMnPc2
</s>
<s>
výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Nerozhodný	rozhodný	k2eNgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzský	francouzský	k2eAgMnSc1d1
princ	princ	k1gMnSc1
sice	sice	k8xC
získal	získat	k5eAaPmAgMnS
španělský	španělský	k2eAgInSc4d1
trůn	trůn	k1gInSc4
<g/>
,	,	kIx,
ovšem	ovšem	k9
za	za	k7c4
cenu	cena	k1gFnSc4
velkých	velký	k2eAgInPc2d1
územních	územní	k2eAgInPc2d1
ústupků	ústupek	k1gInPc2
<g/>
,	,	kIx,
zejména	zejména	k9
rakouským	rakouský	k2eAgInPc3d1
Habsburkům	Habsburk	k1gInPc3
<g/>
,	,	kIx,
a	a	k8xC
příslibu	příslib	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
nikdy	nikdy	k6eAd1
nedojde	dojít	k5eNaPmIp3nS
ke	k	k7c3
spojení	spojení	k1gNnSc3
Francie	Francie	k1gFnSc2
a	a	k8xC
Španělska	Španělsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
strany	strana	k1gFnPc1
</s>
<s>
Haagská	haagský	k2eAgFnSc1d1
velká	velký	k2eAgFnSc1d1
aliance	aliance	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Pro-habsburské	Pro-habsburský	k2eAgNnSc1d1
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Aragonie	Aragonie	k1gFnSc1
</s>
<s>
Katalánsko	Katalánsko	k1gNnSc1
</s>
<s>
Valencie	Valencie	k1gFnSc1
</s>
<s>
Mallorca	Mallorca	k6eAd1
</s>
<s>
Kastilie-část	Kastilie-část	k1gFnSc1
</s>
<s>
Svatá	svatý	k2eAgFnSc1d1
říše	říše	k1gFnSc1
římská	římský	k2eAgFnSc1d1
Svatá	svatý	k2eAgFnSc1d1
říše	říše	k1gFnSc1
římská	římský	k2eAgFnSc1d1
</s>
<s>
Rakousko	Rakousko	k1gNnSc1
</s>
<s>
Savojsko	Savojsko	k1gNnSc1
(	(	kIx(
<g/>
od	od	k7c2
1703	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Hesensko-Kasselsko	Hesensko-Kasselsko	k6eAd1
</s>
<s>
Hannoversko	Hannoversko	k6eAd1
</s>
<s>
Rýnská	rýnský	k2eAgFnSc1d1
Falc	Falc	k1gFnSc1
</s>
<s>
Nizozemí	Nizozemí	k1gNnSc1
</s>
<s>
Prusko	Prusko	k1gNnSc1
(	(	kIx(
<g/>
od	od	k7c2
1702	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
(	(	kIx(
<g/>
od	od	k7c2
1707	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Anglie	Anglie	k1gFnSc1
(	(	kIx(
<g/>
do	do	k7c2
1707	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Skotsko	Skotsko	k1gNnSc1
(	(	kIx(
<g/>
do	do	k7c2
1707	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
(	(	kIx(
<g/>
od	od	k7c2
1703	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Dánsko-Norsko	Dánsko-Norsko	k6eAd1
Dánsko-Norsko	Dánsko-Norsko	k1gNnSc1
(	(	kIx(
<g/>
podpora	podpora	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
spojenci	spojenec	k1gMnPc1
<g/>
:	:	kIx,
</s>
<s>
francouzští	francouzský	k2eAgMnPc1d1
hugenoti	hugenot	k1gMnPc1
</s>
<s>
uherští	uherský	k2eAgMnPc1d1
loajalisté	loajalista	k1gMnPc1
</s>
<s>
podpora	podpora	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Maroko	Maroko	k1gNnSc1
</s>
<s>
Bourboni	Bourbon	k1gMnPc1
se	s	k7c7
spojenci	spojenec	k1gMnPc7
<g/>
:	:	kIx,
</s>
<s>
Pro-bourbonské	Pro-bourbonský	k2eAgNnSc1d1
Španělsko	Španělsko	k1gNnSc1
</s>
<s>
Kastilie	Kastilie	k1gFnSc1
</s>
<s>
Neapolsko	Neapolsko	k6eAd1
</s>
<s>
Sicílie	Sicílie	k1gFnSc1
</s>
<s>
Španělské	španělský	k2eAgNnSc1d1
Nizozemí	Nizozemí	k1gNnSc1
</s>
<s>
Sardinie	Sardinie	k1gFnSc1
</s>
<s>
Mantova	Mantova	k1gFnSc1
</s>
<s>
Milánsko	Milánsko	k6eAd1
</s>
<s>
Francie	Francie	k1gFnSc1
</s>
<s>
Bavorsko	Bavorsko	k1gNnSc1
(	(	kIx(
<g/>
do	do	k7c2
1704	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Uhersko-Sedmihradsko	Uhersko-Sedmihradsko	k1gNnSc1
(	(	kIx(
<g/>
1703	#num#	k4
<g/>
–	–	k?
<g/>
1711	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kolín	Kolín	k1gInSc1
(	(	kIx(
<g/>
do	do	k7c2
1702	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Lutyšsko	Lutyšsko	k1gNnSc1
(	(	kIx(
<g/>
do	do	k7c2
1702	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Portugalsko	Portugalsko	k1gNnSc1
(	(	kIx(
<g/>
do	do	k7c2
1703	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Savojsko	Savojsko	k1gNnSc1
(	(	kIx(
<g/>
do	do	k7c2
1703	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
velitelé	velitel	k1gMnPc1
</s>
<s>
Karel	Karel	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
VI	VI	kA
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
Leopold	Leopold	k1gMnSc1
I.	I.	kA
</s>
<s>
Josef	Josef	k1gMnSc1
I.	I.	kA
</s>
<s>
Rafael	Rafael	k1gMnSc1
Casanova	Casanův	k2eAgInSc2d1
</s>
<s>
Basset	Basset	k1gMnSc1
y	y	k?
Ramos	Ramos	k1gMnSc1
</s>
<s>
Evžen	Evžen	k1gMnSc1
Savojský	savojský	k2eAgMnSc1d1
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
Vilém	Vilém	k1gMnSc1
</s>
<s>
Guido	Guido	k1gNnSc1
Starhemberg	Starhemberg	k1gMnSc1
</s>
<s>
Viktor	Viktor	k1gMnSc1
Amadeus	Amadeus	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Fridrich	Fridrich	k1gMnSc1
Hesensko-Kasselský	hesensko-kasselský	k2eAgMnSc1d1
</s>
<s>
Karel	Karel	k1gMnSc1
Alexandr	Alexandr	k1gMnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Nasavsko-Weilburský	Nasavsko-Weilburský	k2eAgMnSc1d1
</s>
<s>
Anna	Anna	k1gFnSc1
</s>
<s>
John	John	k1gMnSc1
Churchill	Churchill	k1gMnSc1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Marlborough	Marlborougha	k1gFnPc2
</s>
<s>
Henri	Henri	k6eAd1
de	de	k?
Massue	Massue	k1gInSc1
<g/>
,	,	kIx,
hrabě	hrabě	k1gMnSc1
z	z	k7c2
Galway	Galwaa	k1gFnSc2
</s>
<s>
George	Georg	k1gMnSc4
Rooke	Rook	k1gMnSc4
</s>
<s>
Anthonie	Anthonie	k1gFnSc1
Heinsius	Heinsius	k1gInSc1
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
Nasavský	Nasavský	k2eAgMnSc1d1
</s>
<s>
Arnold	Arnold	k1gMnSc1
van	vana	k1gFnPc2
Keppell	Keppell	k1gMnSc1
<g/>
,	,	kIx,
hrabě	hrabě	k1gMnSc1
z	z	k7c2
Albemarle	Albemarle	k1gFnSc2
</s>
<s>
António	António	k6eAd1
Luís	Luís	k1gInSc1
de	de	k?
Sousa	Sousa	k1gFnSc1
</s>
<s>
Ismaíl	Ismaíl	k1gMnSc1
íbn	íbn	k?
Šarif	Šarif	k1gMnSc1
</s>
<s>
Filip	Filip	k1gMnSc1
V.	V.	kA
</s>
<s>
Francisco	Francisca	k1gMnSc5
Castillo	Castilla	k1gMnSc5
Fajardo	Fajarda	k1gMnSc5
<g/>
,	,	kIx,
markýz	markýz	k1gMnSc1
z	z	k7c2
Villadarias	Villadariasa	k1gFnPc2
</s>
<s>
Alexandre	Alexandr	k1gInSc5
Maître	Maîtr	k1gInSc5
<g/>
,	,	kIx,
markýz	markýz	k1gMnSc1
z	z	k7c2
Bay	Bay	k1gFnSc2
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
XIV	XIV	kA
<g/>
.	.	kIx.
</s>
<s>
Claude	Claude	k6eAd1
de	de	k?
Villars	Villars	k1gInSc1
</s>
<s>
James	James	k1gMnSc1
Fitzjames	Fitzjames	k1gMnSc1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Berwicku	Berwick	k1gInSc2
</s>
<s>
Louis	Louis	k1gMnSc1
Joseph	Joseph	k1gMnSc1
de	de	k?
Bourbon	bourbon	k1gInSc1
<g/>
,	,	kIx,
vévoda	vévoda	k1gMnSc1
z	z	k7c2
Vendôme	Vendôm	k1gMnSc5
</s>
<s>
Louis-François	Louis-François	k1gFnSc1
de	de	k?
Boufflers	Boufflers	k1gInSc1
</s>
<s>
François	François	k1gFnSc1
de	de	k?
Neufville	Neufville	k1gFnSc2
de	de	k?
Villeroy	Villeroa	k1gFnSc2
</s>
<s>
René	René	k1gMnSc1
de	de	k?
Froulay	Froulaa	k1gFnSc2
de	de	k?
Tessé	Tessý	k2eAgNnSc1d1
</s>
<s>
Maxmilián	Maxmilián	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s>
Karel	Karel	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gonzaga	Gonzaga	k1gFnSc1
</s>
<s>
František	František	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rákóczi	Rákócze	k1gFnSc6
</s>
<s>
ztráty	ztráta	k1gFnPc1
</s>
<s>
95	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
110	#num#	k4
000	#num#	k4
40	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
52	#num#	k4
000	#num#	k4
30	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
40	#num#	k4
000	#num#	k4
20	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
35	#num#	k4
000	#num#	k4
24	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
30	#num#	k4
000	#num#	k4
5	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
000	#num#	k4
</s>
<s>
115	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
140	#num#	k4
000	#num#	k4
10	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
12	#num#	k4
000	#num#	k4
4	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
000	#num#	k4
</s>
<s>
dohromady	dohromady	k6eAd1
až	až	k9
400	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
–	–	k?
<g/>
700	#num#	k4
000	#num#	k4
mrtvých	mrtvý	k1gMnPc2
</s>
<s>
Válka	válka	k1gFnSc1
o	o	k7c4
španělské	španělský	k2eAgNnSc4d1
dědictví	dědictví	k1gNnSc4
(	(	kIx(
<g/>
1701	#num#	k4
<g/>
–	–	k?
<g/>
1714	#num#	k4
<g/>
)	)	kIx)
představovala	představovat	k5eAaImAgFnS
největší	veliký	k2eAgInSc4d3
ozbrojený	ozbrojený	k2eAgInSc4d1
konflikt	konflikt	k1gInSc4
první	první	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gMnSc1
příčinou	příčina	k1gFnSc7
byl	být	k5eAaImAgInS
spor	spor	k1gInSc1
o	o	k7c4
následnictví	následnictví	k1gNnSc4
na	na	k7c6
španělském	španělský	k2eAgInSc6d1
trůně	trůn	k1gInSc6
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
v	v	k7c6
roce	rok	k1gInSc6
1700	#num#	k4
vymřela	vymřít	k5eAaPmAgFnS
španělská	španělský	k2eAgFnSc1d1
větev	větev	k1gFnSc1
Habsburků	Habsburk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konflikt	konflikt	k1gInSc1
skončil	skončit	k5eAaPmAgInS
bez	bez	k7c2
jasného	jasný	k2eAgMnSc2d1
vítěze	vítěz	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Původ	původ	k1gInSc1
války	válka	k1gFnSc2
</s>
<s>
Karel	Karel	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
poslední	poslední	k2eAgInSc1d1
Habsburk	Habsburk	k1gInSc1
na	na	k7c6
španělském	španělský	k2eAgInSc6d1
trůně	trůn	k1gInSc6
</s>
<s>
Vymření	vymření	k1gNnSc1
rodu	rod	k1gInSc2
španělských	španělský	k2eAgInPc2d1
Habsburků	Habsburk	k1gInPc2
se	se	k3xPyFc4
předem	předem	k6eAd1
očekávalo	očekávat	k5eAaImAgNnS
a	a	k8xC
ještě	ještě	k6eAd1
před	před	k7c7
smrtí	smrt	k1gFnSc7
posledního	poslední	k2eAgMnSc2d1
habsburského	habsburský	k2eAgMnSc2d1
krále	král	k1gMnSc2
Karla	Karel	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
byly	být	k5eAaImAgFnP
uzavřeny	uzavřít	k5eAaPmNgFnP
smlouvy	smlouva	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
měly	mít	k5eAaImAgFnP
válkám	válka	k1gFnPc3
zabránit	zabránit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
dohod	dohoda	k1gFnPc2
mezi	mezi	k7c7
velmocemi	velmoc	k1gFnPc7
z	z	k7c2
roku	rok	k1gInSc2
1698	#num#	k4
a	a	k8xC
rozhodnutí	rozhodnutí	k1gNnSc4
Karla	Karel	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
měl	mít	k5eAaImAgMnS
být	být	k5eAaImF
následníkem	následník	k1gMnSc7
Josef	Josef	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
Bavorský	bavorský	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
nečekané	čekaný	k2eNgFnSc6d1
smrti	smrt	k1gFnSc6
(	(	kIx(
<g/>
1699	#num#	k4
<g/>
)	)	kIx)
pak	pak	k6eAd1
Rakousko	Rakousko	k1gNnSc1
<g/>
,	,	kIx,
Anglie	Anglie	k1gFnSc1
a	a	k8xC
Nizozemí	Nizozemí	k1gNnSc1
uzavřely	uzavřít	k5eAaPmAgInP
smlouvu	smlouva	k1gFnSc4
<g/>
,	,	kIx,
podle	podle	k7c2
které	který	k3yRgFnSc2,k3yIgFnSc2,k3yQgFnSc2
se	se	k3xPyFc4
měl	mít	k5eAaImAgMnS
následníkem	následník	k1gMnSc7
stát	stát	k1gInSc1
mladší	mladý	k2eAgMnSc1d2
syn	syn	k1gMnSc1
římského	římský	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
Leopolda	Leopold	k1gMnSc2
I.	I.	kA
Karel	Karel	k1gMnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
španělský	španělský	k2eAgMnSc1d1
král	král	k1gMnSc1
Karel	Karel	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
určil	určit	k5eAaPmAgInS
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
závěti	závěť	k1gFnSc6
za	za	k7c2
následníka	následník	k1gMnSc2
trůnu	trůn	k1gInSc2
svého	svůj	k1gMnSc4
<g />
.	.	kIx.
</s>
<s hack="1">
prasynovce	prasynovec	k1gMnSc2
Filipa	Filip	k1gMnSc2
z	z	k7c2
Anjou	Anjá	k1gFnSc4
<g/>
,	,	kIx,
vnuka	vnuk	k1gMnSc4
Ludvíka	Ludvík	k1gMnSc4
XIV	XIV	kA
<g/>
.	.	kIx.
(	(	kIx(
<g/>
předpokládá	předpokládat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
podvod	podvod	k1gInSc4
a	a	k8xC
že	že	k8xS
panovník	panovník	k1gMnSc1
buďto	buďto	k8xC
vůbec	vůbec	k9
neměl	mít	k5eNaImAgMnS
ponětí	ponětí	k1gNnSc4
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
podepisuje	podepisovat	k5eAaImIp3nS
<g/>
,	,	kIx,
nebo	nebo	k8xC
že	že	k8xS
byl	být	k5eAaImAgMnS
daný	daný	k2eAgInSc4d1
text	text	k1gInSc4
na	na	k7c4
listinu	listina	k1gFnSc4
doplněn	doplněn	k2eAgInSc1d1
později	pozdě	k6eAd2
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
Ludvík	Ludvík	k1gMnSc1
XIV	XIV	kA
<g/>
.	.	kIx.
tuto	tento	k3xDgFnSc4
volbu	volba	k1gFnSc4
uznal	uznat	k5eAaPmAgMnS
<g/>
,	,	kIx,
Leopold	Leopold	k1gMnSc1
I.	I.	kA
mu	on	k3xPp3gMnSc3
vyhlásil	vyhlásit	k5eAaPmAgMnS
válku	válka	k1gFnSc4
<g/>
,	,	kIx,
k	k	k7c3
němuž	jenž	k3xRgInSc3
se	se	k3xPyFc4
postupně	postupně	k6eAd1
přidala	přidat	k5eAaPmAgFnS
Anglie	Anglie	k1gFnSc1
s	s	k7c7
Nizozemím	Nizozemí	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
státy	stát	k1gInPc1
se	se	k3xPyFc4
obávaly	obávat	k5eAaImAgInP
spojení	spojení	k1gNnSc1
Francie	Francie	k1gFnSc2
a	a	k8xC
Španělska	Španělsko	k1gNnSc2
v	v	k7c6
jedněch	jeden	k4xCgFnPc6
rukou	ruka	k1gFnPc6
a	a	k8xC
požadovaly	požadovat	k5eAaImAgFnP
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
jim	on	k3xPp3gMnPc3
Ludvík	Ludvík	k1gMnSc1
poskytl	poskytnout	k5eAaPmAgMnS
záruky	záruka	k1gFnPc4
a	a	k8xC
slib	slib	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
k	k	k7c3
tomu	ten	k3xDgNnSc3
nedojde	dojít	k5eNaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
ale	ale	k8xC
nic	nic	k6eAd1
takového	takový	k3xDgMnSc4
zaručit	zaručit	k5eAaPmF
nehodlal	hodlat	k5eNaImAgMnS
a	a	k8xC
navíc	navíc	k6eAd1
oba	dva	k4xCgInPc4
státy	stát	k1gInPc4
provokoval	provokovat	k5eAaImAgMnS
neuváženými	uvážený	k2eNgInPc7d1
nepřátelskými	přátelský	k2eNgInPc7d1
kroky	krok	k1gInPc7
(	(	kIx(
<g/>
evidentně	evidentně	k6eAd1
byl	být	k5eAaImAgMnS
přesvědčen	přesvědčit	k5eAaPmNgMnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
schopen	schopen	k2eAgMnSc1d1
porazit	porazit	k5eAaPmF
celou	celý	k2eAgFnSc4d1
koalici	koalice	k1gFnSc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
přinutil	přinutit	k5eAaPmAgInS
podobnou	podobný	k2eAgFnSc4d1
k	k	k7c3
ústupu	ústup	k1gInSc3
v	v	k7c6
roce	rok	k1gInSc6
1697	#num#	k4
-	-	kIx~
viz	vidět	k5eAaImRp2nS
devítiletá	devítiletý	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Účastníci	účastník	k1gMnPc1
konfliktu	konflikt	k1gInSc2
</s>
<s>
Mapa	mapa	k1gFnSc1
Evropy	Evropa	k1gFnSc2
na	na	k7c6
počátku	počátek	k1gInSc6
Války	válka	k1gFnSc2
o	o	k7c4
španělské	španělský	k2eAgNnSc4d1
dědictví	dědictví	k1gNnSc4
</s>
<s>
Josef	Josef	k1gMnSc1
Ferdinand	Ferdinand	k1gMnSc1
<g/>
,	,	kIx,
princ	princ	k1gMnSc1
asturijský	asturijský	k2eAgMnSc1d1
a	a	k8xC
kurprinc	kurprinc	k6eAd1
bavorský	bavorský	k2eAgMnSc1d1
<g/>
,	,	kIx,
všemi	všecek	k3xTgFnPc7
zúčastněnými	zúčastněný	k2eAgFnPc7d1
velmocemi	velmoc	k1gFnPc7
uznaný	uznaný	k2eAgMnSc1d1
následník	následník	k1gMnSc1
Karla	Karel	k1gMnSc2
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
nečekaná	čekaný	k2eNgFnSc1d1
smrt	smrt	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
1699	#num#	k4
zničila	zničit	k5eAaPmAgFnS
křehké	křehký	k2eAgFnPc4d1
dohody	dohoda	k1gFnPc4
o	o	k7c4
následnictví	následnictví	k1gNnSc4
na	na	k7c6
španělském	španělský	k2eAgInSc6d1
trůně	trůn	k1gInSc6
a	a	k8xC
fakticky	fakticky	k6eAd1
ukončila	ukončit	k5eAaPmAgFnS
naděje	naděje	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
celá	celý	k2eAgFnSc1d1
věc	věc	k1gFnSc1
obešla	obejít	k5eAaPmAgFnS
bez	bez	k7c2
konfliktu	konflikt	k1gInSc2
</s>
<s>
Konfliktu	konflikt	k1gInSc3
se	se	k3xPyFc4
zúčastnily	zúčastnit	k5eAaPmAgFnP
všechny	všechen	k3xTgFnPc1
tehdejší	tehdejší	k2eAgFnPc1d1
evropské	evropský	k2eAgFnPc1d1
mocnosti	mocnost	k1gFnPc1
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
Švédska	Švédsko	k1gNnSc2
a	a	k8xC
Ruska	Rusko	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
si	se	k3xPyFc3
řešily	řešit	k5eAaImAgFnP
své	svůj	k3xOyFgFnPc4
vzájemné	vzájemný	k2eAgFnPc4d1
mocenské	mocenský	k2eAgFnPc4d1
rozpory	rozpora	k1gFnPc4
v	v	k7c6
severní	severní	k2eAgFnSc6d1
válce	válka	k1gFnSc6
(	(	kIx(
<g/>
1700	#num#	k4
<g/>
-	-	kIx~
<g/>
1721	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
Osmanské	osmanský	k2eAgFnSc2d1
říše	říš	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
zotavovala	zotavovat	k5eAaImAgFnS
ze	z	k7c2
série	série	k1gFnSc2
drtivých	drtivý	k2eAgFnPc2d1
porážek	porážka	k1gFnPc2
utrpěných	utrpěný	k2eAgFnPc2d1
v	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
rané	raný	k2eAgFnSc2d1
fáze	fáze	k1gFnSc2
konfliktu	konflikt	k1gInSc2
se	se	k3xPyFc4
postupně	postupně	k6eAd1
vyprofilovaly	vyprofilovat	k5eAaPmAgFnP
dvě	dva	k4xCgFnPc1
strany	strana	k1gFnPc1
<g/>
:	:	kIx,
francouzská	francouzský	k2eAgFnSc1d1
(	(	kIx(
<g/>
tvořená	tvořený	k2eAgFnSc1d1
především	především	k9
Francií	Francie	k1gFnSc7
<g/>
,	,	kIx,
větší	veliký	k2eAgFnSc7d2
částí	část	k1gFnSc7
Španělska	Španělsko	k1gNnSc2
<g/>
,	,	kIx,
Savojskem	Savojsko	k1gNnSc7
a	a	k8xC
Bavorskem	Bavorsko	k1gNnSc7
<g/>
)	)	kIx)
a	a	k8xC
protifrancouzská	protifrancouzský	k2eAgFnSc1d1
(	(	kIx(
<g/>
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
utvořila	utvořit	k5eAaPmAgFnS
Svatá	svatý	k2eAgFnSc1d1
říše	říše	k1gFnSc1
římská	římský	k2eAgFnSc1d1
<g/>
,	,	kIx,
rakouští	rakouský	k2eAgMnPc1d1
Habsburkové	Habsburk	k1gMnPc1
<g/>
,	,	kIx,
Anglie	Anglie	k1gFnPc1
a	a	k8xC
Nizozemsko	Nizozemsko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Posléze	posléze	k6eAd1
ještě	ještě	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
několika	několik	k4yIc3
menším	malý	k2eAgFnPc3d2
změnám	změna	k1gFnPc3
(	(	kIx(
<g/>
Savojsko	Savojsko	k1gNnSc1
přešlo	přejít	k5eAaPmAgNnS
na	na	k7c4
stranu	strana	k1gFnSc4
protifrancouzské	protifrancouzský	k2eAgFnSc2d1
koalice	koalice	k1gFnSc2
<g/>
,	,	kIx,
ke	k	k7c3
které	který	k3yIgFnSc3,k3yQgFnSc3,k3yRgFnSc3
se	se	k3xPyFc4
též	též	k9
připojilo	připojit	k5eAaPmAgNnS
Portugalsko	Portugalsko	k1gNnSc1
a	a	k8xC
část	část	k1gFnSc1
Španělska	Španělsko	k1gNnSc2
/	/	kIx~
<g/>
především	především	k6eAd1
Katalánsko	Katalánsko	k1gNnSc1
<g/>
/	/	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
bojů	boj	k1gInPc2
</s>
<s>
Válka	válka	k1gFnSc1
začala	začít	k5eAaPmAgFnS
dobře	dobře	k6eAd1
pro	pro	k7c4
Francii	Francie	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
pak	pak	k6eAd1
se	se	k3xPyFc4
postupně	postupně	k6eAd1
vyvíjela	vyvíjet	k5eAaImAgFnS
ve	v	k7c4
prospěch	prospěch	k1gInSc4
protifrancouzské	protifrancouzský	k2eAgFnSc2d1
koalice	koalice	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
uštědřila	uštědřit	k5eAaPmAgFnS
Francii	Francie	k1gFnSc4
sérii	série	k1gFnSc4
drtivých	drtivý	k2eAgFnPc2d1
porážek	porážka	k1gFnPc2
(	(	kIx(
<g/>
bitva	bitva	k1gFnSc1
v	v	k7c6
zátoce	zátoka	k1gFnSc6
Vigo	Vigo	k1gMnSc1
(	(	kIx(
<g/>
1702	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
bitva	bitva	k1gFnSc1
u	u	k7c2
Höchstädtu	Höchstädt	k1gInSc2
(	(	kIx(
<g/>
1704	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
bitva	bitva	k1gFnSc1
u	u	k7c2
Ramillies	Ramilliesa	k1gFnPc2
(	(	kIx(
<g/>
1706	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
bitva	bitva	k1gFnSc1
u	u	k7c2
Turína	Turín	k1gInSc2
(	(	kIx(
<g/>
1706	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
bitva	bitva	k1gFnSc1
u	u	k7c2
Oudenaarde	Oudenaard	k1gMnSc5
(	(	kIx(
<g/>
1708	#num#	k4
<g/>
))	))	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francouzská	francouzský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
byla	být	k5eAaImAgFnS
vytlačena	vytlačit	k5eAaPmNgFnS
z	z	k7c2
Itálie	Itálie	k1gFnSc2
<g/>
,	,	kIx,
Říše	říš	k1gFnSc2
<g/>
,	,	kIx,
Španělského	španělský	k2eAgNnSc2d1
Nizozemí	Nizozemí	k1gNnSc2
i	i	k8xC
většiny	většina	k1gFnSc2
Španělska	Španělsko	k1gNnSc2
a	a	k8xC
musela	muset	k5eAaImAgFnS
se	se	k3xPyFc4
soustředit	soustředit	k5eAaPmF
na	na	k7c4
obranu	obrana	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
však	však	k9
mírové	mírový	k2eAgFnPc4d1
podmínky	podmínka	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
dostala	dostat	k5eAaPmAgFnS
<g/>
,	,	kIx,
shledal	shledat	k5eAaPmAgMnS
Ludvík	Ludvík	k1gMnSc1
XIV	XIV	kA
<g/>
.	.	kIx.
nepřijatelnými	přijatelný	k2eNgMnPc7d1
<g/>
,	,	kIx,
válčil	válčit	k5eAaImAgMnS
dál	daleko	k6eAd2
a	a	k8xC
obrana	obrana	k1gFnSc1
se	se	k3xPyFc4
jeho	jeho	k3xOp3gMnSc4,k3xPp3gMnSc4
vojskům	vojsko	k1gNnPc3
dařila	dařit	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1709	#num#	k4
byl	být	k5eAaImAgInS
postup	postup	k1gInSc1
Evžena	Evžen	k1gMnSc2
Savojského	savojský	k2eAgMnSc2d1
a	a	k8xC
Marlborougha	Marlborough	k1gMnSc2
zastaven	zastavit	k5eAaPmNgInS
v	v	k7c6
krvavé	krvavý	k2eAgFnSc6d1
bitvě	bitva	k1gFnSc6
u	u	k7c2
Malplaquet	Malplaqueta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protifrancouzskou	protifrancouzský	k2eAgFnSc4d1
koalici	koalice	k1gFnSc4
začaly	začít	k5eAaPmAgFnP
postupně	postupně	k6eAd1
rozrušovat	rozrušovat	k5eAaImF
spory	spor	k1gInPc4
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gNnSc2
řešení	řešení	k1gNnSc2
neprospěly	prospět	k5eNaPmAgFnP
ani	ani	k9
další	další	k2eAgInPc4d1
neúspěšné	úspěšný	k2eNgInPc4d1
pokusy	pokus	k1gInPc4
o	o	k7c4
rychlé	rychlý	k2eAgNnSc4d1
ukončení	ukončení	k1gNnSc4
konfliktu	konflikt	k1gInSc2
<g/>
,	,	kIx,
ani	ani	k8xC
nastávající	nastávající	k2eAgFnSc1d1
mocenská	mocenský	k2eAgFnSc1d1
změna	změna	k1gFnSc1
v	v	k7c6
Anglii	Anglie	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
ve	v	k7c6
volbách	volba	k1gFnPc6
zvítězili	zvítězit	k5eAaPmAgMnP
toryové	tory	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
válku	válka	k1gFnSc4
nepodporovali	podporovat	k5eNaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Definitivní	definitivní	k2eAgFnSc4d1
ránu	rána	k1gFnSc4
pak	pak	k6eAd1
celé	celý	k2eAgFnSc6d1
koalici	koalice	k1gFnSc6
uštědřila	uštědřit	k5eAaPmAgFnS
smrt	smrt	k1gFnSc1
německého	německý	k2eAgMnSc2d1
císaře	císař	k1gMnSc2
Josefa	Josef	k1gMnSc2
I.	I.	kA
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
vynesla	vynést	k5eAaPmAgFnS
do	do	k7c2
čela	čelo	k1gNnSc2
impéria	impérium	k1gNnSc2
rakouských	rakouský	k2eAgInPc2d1
Habsburků	Habsburk	k1gInPc2
jejich	jejich	k3xOp3gMnSc2
uchazeče	uchazeč	k1gMnSc2
o	o	k7c4
španělský	španělský	k2eAgInSc4d1
trůn	trůn	k1gInSc4
Karla	Karel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Rozpad	rozpad	k1gInSc1
protifrancouzské	protifrancouzský	k2eAgFnSc2d1
aliance	aliance	k1gFnSc2
a	a	k8xC
konec	konec	k1gInSc1
bojů	boj	k1gInPc2
</s>
<s>
Evropa	Evropa	k1gFnSc1
po	po	k7c6
Utrechtském	Utrechtský	k2eAgInSc6d1
míru	mír	k1gInSc6
</s>
<s>
Představa	představa	k1gFnSc1
spojení	spojení	k1gNnSc2
španělského	španělský	k2eAgNnSc2d1
a	a	k8xC
„	„	k?
<g/>
rakousko-říšského	rakousko-říšský	k2eAgNnSc2d1
<g/>
“	“	k?
impéria	impérium	k1gNnSc2
v	v	k7c6
jedněch	jeden	k4xCgFnPc6
rukou	ruka	k1gFnPc6
nelákala	lákat	k5eNaImAgFnS
Anglii	Anglie	k1gFnSc4
a	a	k8xC
Nizozemsko	Nizozemsko	k1gNnSc4
o	o	k7c4
nic	nic	k3yNnSc4
více	hodně	k6eAd2
než	než	k8xS
spojení	spojení	k1gNnSc4
Francie	Francie	k1gFnSc2
a	a	k8xC
Španělska	Španělsko	k1gNnSc2
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1713	#num#	k4
(	(	kIx(
<g/>
po	po	k7c6
patřičných	patřičný	k2eAgFnPc6d1
zárukách	záruka	k1gFnPc6
ze	z	k7c2
strany	strana	k1gFnSc2
Ludvíka	Ludvík	k1gMnSc2
XIV	XIV	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
francouzsko-španělskou	francouzsko-španělský	k2eAgFnSc7d1
personální	personální	k2eAgFnSc6d1
unii	unie	k1gFnSc6
vyloučily	vyloučit	k5eAaPmAgFnP
<g/>
)	)	kIx)
se	se	k3xPyFc4
obě	dva	k4xCgFnPc1
námořní	námořní	k2eAgFnPc1d1
mocnosti	mocnost	k1gFnPc1
z	z	k7c2
války	válka	k1gFnSc2
stáhly	stáhnout	k5eAaPmAgInP
(	(	kIx(
<g/>
tzv.	tzv.	kA
Utrechtský	Utrechtský	k2eAgInSc1d1
mír	mír	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
okolnost	okolnost	k1gFnSc1
nakonec	nakonec	k6eAd1
přinutila	přinutit	k5eAaPmAgFnS
k	k	k7c3
míru	mír	k1gInSc3
i	i	k9
rakouské	rakouský	k2eAgMnPc4d1
Habsburky	Habsburk	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
uzavřeli	uzavřít	k5eAaPmAgMnP
patřičné	patřičný	k2eAgFnPc4d1
smlouvy	smlouva	k1gFnPc4
s	s	k7c7
Francií	Francie	k1gFnSc7
v	v	k7c6
roce	rok	k1gInSc6
1714	#num#	k4
(	(	kIx(
<g/>
Rastattský	Rastattský	k2eAgInSc1d1
mír	mír	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzniklý	vzniklý	k2eAgInSc1d1
systém	systém	k1gInSc1
smluv	smlouva	k1gFnPc2
dával	dávat	k5eAaImAgInS
španělský	španělský	k2eAgInSc4d1
trůn	trůn	k1gInSc4
Ludvíkovu	Ludvíkův	k2eAgMnSc3d1
vnukovi	vnuk	k1gMnSc3
Filipovi	Filip	k1gMnSc3
z	z	k7c2
rodu	rod	k1gInSc2
Bourbonů	bourbon	k1gInPc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
však	však	k9
nedostal	dostat	k5eNaPmAgInS
říši	říše	k1gFnSc4
celou	celý	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rakouští	rakouský	k2eAgMnPc1d1
Habsburkové	Habsburk	k1gMnPc1
byli	být	k5eAaImAgMnP
odškodněni	odškodnit	k5eAaPmNgMnP
Španělským	španělský	k2eAgNnSc7d1
Nizozemím	Nizozemí	k1gNnSc7
<g/>
,	,	kIx,
Neapolským	neapolský	k2eAgNnSc7d1
královstvím	království	k1gNnSc7
<g/>
,	,	kIx,
Sardinií	Sardinie	k1gFnSc7
a	a	k8xC
také	také	k9
většinou	většina	k1gFnSc7
Milánska	Milánsko	k1gNnSc2
<g/>
,	,	kIx,
o	o	k7c4
něž	jenž	k3xRgMnPc4
se	se	k3xPyFc4
podělili	podělit	k5eAaPmAgMnP
s	s	k7c7
vévodou	vévoda	k1gMnSc7
savojským	savojský	k2eAgMnSc7d1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
mimo	mimo	k7c4
to	ten	k3xDgNnSc4
obdržel	obdržet	k5eAaPmAgInS
královskou	královský	k2eAgFnSc4d1
korunu	koruna	k1gFnSc4
a	a	k8xC
Sicílii	Sicílie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
si	se	k3xPyFc3
navíc	navíc	k6eAd1
ponechala	ponechat	k5eAaPmAgFnS
dvě	dva	k4xCgFnPc4
důležité	důležitý	k2eAgFnPc4d1
základny	základna	k1gFnPc4
ve	v	k7c6
Středomoří	středomoří	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
dříve	dříve	k6eAd2
náležely	náležet	k5eAaImAgInP
Španělsku	Španělsko	k1gNnSc3
<g/>
:	:	kIx,
Menorcu	Menorcus	k1gInSc2
a	a	k8xC
Gibraltar	Gibraltar	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
PERNES	PERNES	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
;	;	kIx,
FUČÍK	Fučík	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
;	;	kIx,
HAVEL	Havel	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
císařským	císařský	k2eAgInSc7d1
praporem	prapor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historie	historie	k1gFnSc1
habsburské	habsburský	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
1526	#num#	k4
<g/>
-	-	kIx~
<g/>
1918	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Elka	Elka	k1gFnSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
.	.	kIx.
555	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
902745	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
VLNAS	VLNAS	kA
<g/>
,	,	kIx,
Vít	Vít	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Princ	princ	k1gMnSc1
Evžen	Evžen	k1gMnSc1
Savojský	savojský	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Život	život	k1gInSc4
a	a	k8xC
sláva	sláva	k1gFnSc1
barokního	barokní	k2eAgMnSc2d1
válečníka	válečník	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
;	;	kIx,
Litomyšl	Litomyšl	k1gFnSc1
<g/>
:	:	kIx,
Paseka	paseka	k1gFnSc1
;	;	kIx,
Národní	národní	k2eAgFnSc1d1
galerie	galerie	k1gFnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
849	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7185	#num#	k4
<g/>
-	-	kIx~
<g/>
380	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Novověk	novověk	k1gInSc1
|	|	kIx~
Španělsko	Španělsko	k1gNnSc1
|	|	kIx~
Válka	válka	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ph	ph	kA
<g/>
570644	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4134877-1	4134877-1	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85126301	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85126301	#num#	k4
</s>
