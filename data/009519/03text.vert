<p>
<s>
Sardinie	Sardinie	k1gFnSc1	Sardinie
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
Sardegna	Sardegen	k2eAgFnSc1d1	Sardegna
<g/>
,	,	kIx,	,
sardinsky	sardinsky	k6eAd1	sardinsky
Sardigna	Sardigno	k1gNnSc2	Sardigno
<g/>
,	,	kIx,	,
Sardinna	Sardinno	k1gNnSc2	Sardinno
nebo	nebo	k8xC	nebo
Sardinnia	Sardinnium	k1gNnSc2	Sardinnium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
autonomní	autonomní	k2eAgFnSc1d1	autonomní
oblast	oblast	k1gFnSc1	oblast
na	na	k7c6	na
stejnojmenném	stejnojmenný	k2eAgInSc6d1	stejnojmenný
ostrově	ostrov	k1gInSc6	ostrov
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
Itálie	Itálie	k1gFnSc2	Itálie
ve	v	k7c6	v
Středozemním	středozemní	k2eAgNnSc6d1	středozemní
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
Apeninským	apeninský	k2eAgInSc7d1	apeninský
poloostrovem	poloostrov	k1gInSc7	poloostrov
<g/>
,	,	kIx,	,
Španělskem	Španělsko	k1gNnSc7	Španělsko
<g/>
,	,	kIx,	,
Korsikou	Korsika	k1gFnSc7	Korsika
a	a	k8xC	a
Tuniskem	Tunisko	k1gNnSc7	Tunisko
<g/>
.	.	kIx.	.
</s>
<s>
Region	region	k1gInSc1	region
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
24	[number]	k4	24
100	[number]	k4	100
km2	km2	k4	km2
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
1,6	[number]	k4	1,6
miliónu	milión	k4xCgInSc2	milión
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
4	[number]	k4	4
provincií	provincie	k1gFnPc2	provincie
a	a	k8xC	a
metropolitní	metropolitní	k2eAgNnSc4d1	metropolitní
město	město	k1gNnSc4	město
Cagliari	Cagliar	k1gFnSc2	Cagliar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
regionu	region	k1gInSc6	region
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
kromě	kromě	k7c2	kromě
italštiny	italština	k1gFnSc2	italština
také	také	k6eAd1	také
sardinštinou	sardinština	k1gFnSc7	sardinština
a	a	k8xC	a
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Alghero	Alghero	k1gNnSc4	Alghero
katalánštinou	katalánština	k1gFnSc7	katalánština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
hornatý	hornatý	k2eAgInSc1d1	hornatý
s	s	k7c7	s
členitým	členitý	k2eAgNnSc7d1	členité
pobřežím	pobřeží	k1gNnSc7	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
je	být	k5eAaImIp3nS	být
Punta	punto	k1gNnPc4	punto
La	la	k1gNnSc2	la
Marmora	Marmor	k1gMnSc2	Marmor
(	(	kIx(	(
<g/>
1	[number]	k4	1
834	[number]	k4	834
m	m	kA	m
<g/>
)	)	kIx)	)
v	v	k7c6	v
masivu	masiv	k1gInSc6	masiv
Gennargent	Gennargent	k1gInSc1	Gennargent
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
hlavní	hlavní	k2eAgInSc4d1	hlavní
ostrov	ostrov	k1gInSc4	ostrov
Sardinie	Sardinie	k1gFnSc2	Sardinie
a	a	k8xC	a
okolní	okolní	k2eAgInPc4d1	okolní
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
např.	např.	kA	např.
Sant	Sant	k1gMnSc1	Sant
<g/>
'	'	kIx"	'
<g/>
Antioco	Antioco	k1gMnSc1	Antioco
<g/>
,	,	kIx,	,
San	San	k1gFnSc1	San
Pietro	Pietro	k1gNnSc1	Pietro
<g/>
,	,	kIx,	,
Asinara	Asinara	k1gFnSc1	Asinara
<g/>
,	,	kIx,	,
La	la	k1gNnPc1	la
Maddalena	Maddalen	k2eAgNnPc1d1	Maddalen
a	a	k8xC	a
Caprera	Caprero	k1gNnPc1	Caprero
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Film	film	k1gInSc1	film
==	==	k?	==
</s>
</p>
<p>
<s>
O	o	k7c6	o
Sardinii	Sardinie	k1gFnSc6	Sardinie
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
česko-italský	českotalský	k2eAgInSc4d1	česko-italský
celovečerní	celovečerní	k2eAgInSc4d1	celovečerní
film	film	k1gInSc4	film
Mír	Míra	k1gFnPc2	Míra
s	s	k7c7	s
tuleni	tuleň	k1gMnPc7	tuleň
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnPc4d1	obsahující
mimořádně	mimořádně	k6eAd1	mimořádně
zajímavé	zajímavý	k2eAgFnPc4d1	zajímavá
archivní	archivní	k2eAgFnPc4d1	archivní
i	i	k8xC	i
nově	nově	k6eAd1	nově
pořízené	pořízený	k2eAgInPc1d1	pořízený
záběry	záběr	k1gInPc1	záběr
Sardinie	Sardinie	k1gFnSc2	Sardinie
ze	z	k7c2	z
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
pevniny	pevnina	k1gFnSc2	pevnina
i	i	k8xC	i
z	z	k7c2	z
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
se	s	k7c7	s
sardskými	sardský	k2eAgMnPc7d1	sardský
obyvateli	obyvatel	k1gMnPc7	obyvatel
městečka	městečko	k1gNnSc2	městečko
Cala	Cal	k1gInSc2	Cal
Gonone	Gonon	k1gInSc5	Gonon
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Sardinii	Sardinie	k1gFnSc6	Sardinie
promítán	promítat	k5eAaImNgInS	promítat
také	také	k9	také
v	v	k7c6	v
kinech	kino	k1gNnPc6	kino
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dostupnost	dostupnost	k1gFnSc1	dostupnost
a	a	k8xC	a
turismus	turismus	k1gInSc1	turismus
==	==	k?	==
</s>
</p>
<p>
<s>
Turisté	turist	k1gMnPc1	turist
navštěvují	navštěvovat	k5eAaImIp3nP	navštěvovat
horo-skalnatá	horokalnatý	k2eAgFnSc1d1	horo-skalnatý
oblast	oblast	k1gFnSc1	oblast
Aggius	Aggius	k1gMnSc1	Aggius
(	(	kIx(	(
<g/>
5	[number]	k4	5
km	km	kA	km
SZ	SZ	kA	SZ
od	od	k7c2	od
Tempia	Tempium	k1gNnSc2	Tempium
<g/>
)	)	kIx)	)
se	s	k7c7	s
stometrovými	stometrový	k2eAgInPc7d1	stometrový
oblými	oblý	k2eAgInPc7d1	oblý
monolity	monolit	k1gInPc7	monolit
<g/>
.	.	kIx.	.
</s>
<s>
Hodnotné	hodnotný	k2eAgFnPc1d1	hodnotná
možnosti	možnost	k1gFnPc1	možnost
tu	tu	k6eAd1	tu
mají	mít	k5eAaImIp3nP	mít
horolezci	horolezec	k1gMnPc1	horolezec
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
pobřeží	pobřeží	k1gNnSc6	pobřeží
u	u	k7c2	u
Capo	capa	k1gFnSc5	capa
Testa	testa	k1gFnSc1	testa
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
pás	pás	k1gInSc1	pás
60	[number]	k4	60
<g/>
–	–	k?	–
<g/>
90	[number]	k4	90
m	m	kA	m
vysokých	vysoká	k1gFnPc2	vysoká
<g/>
,	,	kIx,	,
oblých	oblý	k2eAgFnPc2d1	oblá
balvanových	balvanový	k2eAgFnPc2d1	balvanová
kup	kupa	k1gFnPc2	kupa
<g/>
,	,	kIx,	,
přístupných	přístupný	k2eAgFnPc2d1	přístupná
z	z	k7c2	z
moře	moře	k1gNnSc2	moře
i	i	k8xC	i
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Lezecké	lezecký	k2eAgFnPc1d1	lezecká
stěny	stěna	k1gFnPc1	stěna
se	se	k3xPyFc4	se
zvedají	zvedat	k5eAaImIp3nP	zvedat
z	z	k7c2	z
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
pláží	pláž	k1gFnPc2	pláž
v	v	k7c4	v
Golfo	Golfo	k1gNnSc4	Golfo
di	di	k?	di
Orosei	Orosei	k1gNnSc2	Orosei
u	u	k7c2	u
Cala	Cal	k1gInSc2	Cal
Gonone	Gonon	k1gInSc5	Gonon
a	a	k8xC	a
jižněji	jižně	k6eAd2	jižně
u	u	k7c2	u
Cala	Cal	k1gInSc2	Cal
Fuili	Fuile	k1gFnSc3	Fuile
a	a	k8xC	a
Cala	Cal	k2eAgFnSc1d1	Cala
Luna	luna	k1gFnSc1	luna
<g/>
.	.	kIx.	.
</s>
<s>
Efektní	efektní	k2eAgFnPc1d1	efektní
červené	červený	k2eAgFnPc1d1	červená
skalní	skalní	k2eAgFnPc1d1	skalní
stěny	stěna	k1gFnPc1	stěna
vystupující	vystupující	k2eAgFnPc1d1	vystupující
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
u	u	k7c2	u
Arbatax	Arbatax	k1gInSc4	Arbatax
(	(	kIx(	(
<g/>
poblíž	poblíž	k6eAd1	poblíž
Tortoli	Tortole	k1gFnSc4	Tortole
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
zvětralost	zvětralost	k1gFnSc4	zvětralost
využívají	využívat	k5eAaImIp3nP	využívat
méně	málo	k6eAd2	málo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Sardinii	Sardinie	k1gFnSc6	Sardinie
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
neznámé	známý	k2eNgFnSc2d1	neznámá
prehistorické	prehistorický	k2eAgFnSc2d1	prehistorická
stavby	stavba	k1gFnSc2	stavba
-	-	kIx~	-
nuraghy	nuragha	k1gFnSc2	nuragha
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1500	[number]	k4	1500
<g/>
–	–	k?	–
<g/>
300	[number]	k4	300
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
V	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Oristano	Oristana	k1gFnSc5	Oristana
je	být	k5eAaImIp3nS	být
archeologické	archeologický	k2eAgNnSc4d1	Archeologické
naleziště	naleziště	k1gNnSc4	naleziště
starobylého	starobylý	k2eAgNnSc2d1	starobylé
města	město	k1gNnSc2	město
Tharros	Tharrosa	k1gFnPc2	Tharrosa
<g/>
.	.	kIx.	.
</s>
<s>
Přístup	přístup	k1gInSc1	přístup
je	být	k5eAaImIp3nS	být
nejčastější	častý	k2eAgFnSc7d3	nejčastější
lodí	loď	k1gFnSc7	loď
z	z	k7c2	z
Itálie	Itálie	k1gFnSc2	Itálie
anebo	anebo	k8xC	anebo
Korsiky	Korsika	k1gFnSc2	Korsika
přes	přes	k7c4	přes
Bonifácký	Bonifácký	k2eAgInSc4d1	Bonifácký
průliv	průliv	k1gInSc4	průliv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sardinie	Sardinie	k1gFnSc2	Sardinie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Sardinie	Sardinie	k1gFnSc2	Sardinie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
web	web	k1gInSc1	web
autonomní	autonomní	k2eAgFnSc2d1	autonomní
oblasti	oblast	k1gFnSc2	oblast
Sardinie	Sardinie	k1gFnSc2	Sardinie
</s>
</p>
<p>
<s>
Sardegna	Sardegen	k2eAgFnSc1d1	Sardegna
Cultura	Cultura	k1gFnSc1	Cultura
(	(	kIx(	(
<g/>
od	od	k7c2	od
archeologie	archeologie	k1gFnSc2	archeologie
<g/>
,	,	kIx,	,
architektury	architektura	k1gFnSc2	architektura
<g/>
,	,	kIx,	,
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
literatury	literatura	k1gFnSc2	literatura
až	až	k9	až
po	po	k7c6	po
umění	umění	k1gNnSc6	umění
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sardegna	Sardegna	k1gFnSc1	Sardegna
DigitalLibrary	DigitalLibrara	k1gFnSc2	DigitalLibrara
</s>
</p>
<p>
<s>
Dny	den	k1gInPc1	den
sardské	sardský	k2eAgFnSc2d1	sardská
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
přírodopisu	přírodopis	k1gInSc2	přírodopis
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Sardinie	Sardinie	k1gFnSc1	Sardinie
-	-	kIx~	-
Středomořská	středomořský	k2eAgFnSc1d1	středomořská
kráska	kráska	k1gFnSc1	kráska
</s>
</p>
<p>
<s>
Na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
po	po	k7c6	po
Sardinii	Sardinie	k1gFnSc6	Sardinie
<g/>
,	,	kIx,	,
dokument	dokument	k1gInSc1	dokument
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
</s>
</p>
