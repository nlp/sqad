<s>
Sardinie	Sardinie	k1gFnSc1	Sardinie
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
Sardegna	Sardegen	k2eAgFnSc1d1	Sardegna
<g/>
,	,	kIx,	,
sardinsky	sardinsky	k6eAd1	sardinsky
Sardigna	Sardigno	k1gNnSc2	Sardigno
<g/>
,	,	kIx,	,
Sardinna	Sardinno	k1gNnSc2	Sardinno
nebo	nebo	k8xC	nebo
Sardinnia	Sardinnium	k1gNnSc2	Sardinnium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
autonomní	autonomní	k2eAgFnSc1d1	autonomní
oblast	oblast	k1gFnSc1	oblast
na	na	k7c6	na
stejnojmenném	stejnojmenný	k2eAgInSc6d1	stejnojmenný
ostrově	ostrov	k1gInSc6	ostrov
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
Itálie	Itálie	k1gFnSc2	Itálie
ve	v	k7c6	v
Středozemním	středozemní	k2eAgNnSc6d1	středozemní
moři	moře	k1gNnSc6	moře
<g/>
.	.	kIx.	.
</s>
