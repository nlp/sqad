<s>
Lucembursko	Lucembursko	k1gNnSc1	Lucembursko
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
Velkovévodství	velkovévodství	k1gNnSc1	velkovévodství
lucemburské	lucemburský	k2eAgNnSc1d1	lucemburské
(	(	kIx(	(
<g/>
lucembursky	lucembursky	k6eAd1	lucembursky
Groussherzogtum	Groussherzogtum	k1gNnSc1	Groussherzogtum
Lëtzebuerg	Lëtzebuerg	k1gInSc1	Lëtzebuerg
<g/>
,	,	kIx,	,
německy	německy	k6eAd1	německy
Großherzogtum	Großherzogtum	k1gNnSc1	Großherzogtum
Luxemburg	Luxemburg	k1gInSc1	Luxemburg
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
Grand-Duché	Grand-Duchý	k2eAgNnSc4d1	Grand-Duchý
de	de	k?	de
Luxembourg	Luxembourg	k1gInSc4	Luxembourg
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgInSc1d1	malý
západoevropský	západoevropský	k2eAgInSc1d1	západoevropský
vnitrozemský	vnitrozemský	k2eAgInSc1d1	vnitrozemský
stát	stát	k1gInSc1	stát
sousedící	sousedící	k2eAgInSc1d1	sousedící
s	s	k7c7	s
Belgií	Belgie	k1gFnSc7	Belgie
<g/>
,	,	kIx,	,
Německem	Německo	k1gNnSc7	Německo
a	a	k8xC	a
Francií	Francie	k1gFnSc7	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
kolem	kolem	k6eAd1	kolem
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
římskokatolického	římskokatolický	k2eAgNnSc2d1	římskokatolické
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
.	.	kIx.	.
</s>
<s>
Úředními	úřední	k2eAgMnPc7d1	úřední
jazyky	jazyk	k1gMnPc7	jazyk
jsou	být	k5eAaImIp3nP	být
lucemburština	lucemburština	k1gFnSc1	lucemburština
<g/>
,	,	kIx,	,
němčina	němčina	k1gFnSc1	němčina
a	a	k8xC	a
francouzština	francouzština	k1gFnSc1	francouzština
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
zakládajícím	zakládající	k2eAgMnSc7d1	zakládající
členem	člen	k1gMnSc7	člen
Beneluxu	Benelux	k1gInSc2	Benelux
<g/>
,	,	kIx,	,
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
OECD	OECD	kA	OECD
a	a	k8xC	a
hlavní	hlavní	k2eAgNnSc1d1	hlavní
a	a	k8xC	a
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
Lucemburk	Lucemburk	k1gInSc1	Lucemburk
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
mnoha	mnoho	k4c2	mnoho
jejich	jejich	k3xOp3gMnPc2	jejich
institucí	instituce	k1gFnPc2	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Lucembursko	Lucembursko	k1gNnSc1	Lucembursko
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
rozvinutou	rozvinutý	k2eAgFnSc4d1	rozvinutá
ekonomiku	ekonomika	k1gFnSc4	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
měnového	měnový	k2eAgInSc2d1	měnový
fondu	fond	k1gInSc2	fond
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
ekonomiku	ekonomika	k1gFnSc4	ekonomika
s	s	k7c7	s
nejvyšším	vysoký	k2eAgNnSc7d3	nejvyšší
nominálním	nominální	k2eAgNnSc7d1	nominální
HDP	HDP	kA	HDP
na	na	k7c4	na
jednoho	jeden	k4xCgMnSc4	jeden
člověka	člověk	k1gMnSc4	člověk
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
velkovévodou	velkovévoda	k1gMnSc7	velkovévoda
Jindřich	Jindřich	k1gMnSc1	Jindřich
I.	I.	kA	I.
Lucemburský	lucemburský	k2eAgInSc5d1	lucemburský
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Lucemburska	Lucembursko	k1gNnSc2	Lucembursko
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
samostatného	samostatný	k2eAgNnSc2d1	samostatné
Lucemburska	Lucembursko	k1gNnSc2	Lucembursko
počínají	počínat	k5eAaImIp3nP	počínat
vznikem	vznik	k1gInSc7	vznik
lucemburského	lucemburský	k2eAgInSc2d1	lucemburský
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
hradu	hrad	k1gInSc2	hrad
postupně	postupně	k6eAd1	postupně
vznikalo	vznikat	k5eAaImAgNnS	vznikat
středověké	středověký	k2eAgNnSc1d1	středověké
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
střediskem	středisko	k1gNnSc7	středisko
nevelkého	velký	k2eNgMnSc2d1	nevelký
<g/>
,	,	kIx,	,
leč	leč	k8xS	leč
svou	svůj	k3xOyFgFnSc7	svůj
polohou	poloha	k1gFnSc7	poloha
nikoliv	nikoliv	k9	nikoliv
nepodstatného	podstatný	k2eNgNnSc2d1	nepodstatné
panství	panství	k1gNnSc2	panství
<g/>
.	.	kIx.	.
</s>
<s>
Lucemburské	lucemburský	k2eAgNnSc1d1	lucemburské
hrabství	hrabství	k1gNnSc1	hrabství
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
roku	rok	k1gInSc2	rok
963	[number]	k4	963
a	a	k8xC	a
na	na	k7c4	na
vévodství	vévodství	k1gNnSc4	vévodství
bylo	být	k5eAaImAgNnS	být
povýšeno	povýšit	k5eAaPmNgNnS	povýšit
roku	rok	k1gInSc2	rok
1354	[number]	k4	1354
Karlem	Karel	k1gMnSc7	Karel
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgMnS	být
zároveň	zároveň	k6eAd1	zároveň
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
český	český	k2eAgInSc4d1	český
trůn	trůn	k1gInSc4	trůn
se	se	k3xPyFc4	se
Lucemburkové	Lucemburk	k1gMnPc1	Lucemburk
dostali	dostat	k5eAaPmAgMnP	dostat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1310	[number]	k4	1310
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Eliškou	Eliška	k1gFnSc7	Eliška
Přemyslovnou	Přemyslovna	k1gFnSc7	Přemyslovna
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dlouhém	dlouhý	k2eAgNnSc6d1	dlouhé
období	období	k1gNnSc6	období
nadvlády	nadvláda	k1gFnSc2	nadvláda
jiných	jiný	k2eAgFnPc2d1	jiná
zemí	zem	k1gFnPc2	zem
získalo	získat	k5eAaPmAgNnS	získat
Lucemburské	lucemburský	k2eAgNnSc1d1	lucemburské
velkovévodství	velkovévodství	k1gNnSc1	velkovévodství
samostatnost	samostatnost	k1gFnSc1	samostatnost
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Lucembursko	Lucembursko	k1gNnSc1	Lucembursko
zpočátku	zpočátku	k6eAd1	zpočátku
vystupovalo	vystupovat	k5eAaImAgNnS	vystupovat
jako	jako	k8xS	jako
neutrální	neutrální	k2eAgFnPc1d1	neutrální
země	zem	k1gFnPc1	zem
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
opakovaném	opakovaný	k2eAgNnSc6d1	opakované
obsazení	obsazení	k1gNnSc6	obsazení
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Německa	Německo	k1gNnSc2	Německo
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
nuceno	nucen	k2eAgNnSc1d1	nuceno
tento	tento	k3xDgInSc4	tento
stav	stav	k1gInSc4	stav
změnit	změnit	k5eAaPmF	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
stálo	stát	k5eAaImAgNnS	stát
Lucemburské	lucemburský	k2eAgNnSc1d1	lucemburské
velkovévodství	velkovévodství	k1gNnSc1	velkovévodství
u	u	k7c2	u
zrodu	zrod	k1gInSc2	zrod
Severoatlantické	severoatlantický	k2eAgFnSc2d1	Severoatlantická
aliance	aliance	k1gFnSc2	aliance
<g/>
,	,	kIx,	,
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
a	a	k8xC	a
Evropského	evropský	k2eAgNnSc2d1	Evropské
hospodářského	hospodářský	k2eAgNnSc2d1	hospodářské
společenství	společenství	k1gNnSc2	společenství
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Lucemburska	Lucembursko	k1gNnSc2	Lucembursko
<g/>
.	.	kIx.	.
</s>
<s>
Lucembursko	Lucembursko	k1gNnSc1	Lucembursko
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
dost	dost	k6eAd1	dost
odlišné	odlišný	k2eAgFnPc1d1	odlišná
části	část	k1gFnPc1	část
<g/>
:	:	kIx,	:
Oesling	Oesling	k1gInSc1	Oesling
-	-	kIx~	-
část	část	k1gFnSc1	část
na	na	k7c6	na
severu	sever	k1gInSc6	sever
tvoří	tvořit	k5eAaImIp3nS	tvořit
přibližně	přibližně	k6eAd1	přibližně
třetinu	třetina	k1gFnSc4	třetina
území	území	k1gNnSc2	území
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
Porýnská	porýnský	k2eAgFnSc1d1	porýnská
břidličnatá	břidličnatý	k2eAgFnSc1d1	břidličnatá
vrchovina	vrchovina	k1gFnSc1	vrchovina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Středoněmecké	středoněmecký	k2eAgFnSc2d1	Středoněmecká
vysočiny	vysočina	k1gFnSc2	vysočina
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
je	být	k5eAaImIp3nS	být
400	[number]	k4	400
<g/>
-	-	kIx~	-
<g/>
500	[number]	k4	500
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
a	a	k8xC	a
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
je	být	k5eAaImIp3nS	být
současně	současně	k6eAd1	současně
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
celého	celý	k2eAgNnSc2d1	celé
velkovévodství	velkovévodství	k1gNnSc2	velkovévodství
Kneiff	Kneiff	k1gInSc1	Kneiff
(	(	kIx(	(
<g/>
560	[number]	k4	560
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krajina	Krajina	k1gFnSc1	Krajina
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
náhorní	náhorní	k2eAgFnSc7d1	náhorní
rovinou	rovina	k1gFnSc7	rovina
rozdělenou	rozdělený	k2eAgFnSc7d1	rozdělená
koryty	koryto	k1gNnPc7	koryto
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
Saner	Saner	k1gMnSc1	Saner
<g/>
,	,	kIx,	,
Clerf	Clerf	k1gMnSc1	Clerf
nebo	nebo	k8xC	nebo
Wiltz	Wiltz	k1gMnSc1	Wiltz
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
oblasti	oblast	k1gFnSc2	oblast
je	být	k5eAaImIp3nS	být
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
lesy	les	k1gInPc7	les
<g/>
,	,	kIx,	,
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
řek	řeka	k1gFnPc2	řeka
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
bažiny	bažina	k1gFnPc1	bažina
a	a	k8xC	a
rašeliniště	rašeliniště	k1gNnPc1	rašeliniště
<g/>
.	.	kIx.	.
</s>
<s>
Gutland	Gutland	k1gInSc1	Gutland
(	(	kIx(	(
<g/>
francouzsky	francouzsky	k6eAd1	francouzsky
<g/>
:	:	kIx,	:
Bon	bon	k1gInSc1	bon
Pays	Pays	k1gInSc1	Pays
<g/>
)	)	kIx)	)
-	-	kIx~	-
kopcovitá	kopcovitý	k2eAgFnSc1d1	kopcovitá
krajina	krajina	k1gFnSc1	krajina
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
střední	střední	k2eAgFnSc1d1	střední
a	a	k8xC	a
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
250	[number]	k4	250
<g/>
-	-	kIx~	-
<g/>
300	[number]	k4	300
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
kopce	kopka	k1gFnSc6	kopka
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
oblasti	oblast	k1gFnSc2	oblast
je	být	k5eAaImIp3nS	být
Mont-Saint-Jean	Mont-Saint-Jean	k1gInSc1	Mont-Saint-Jean
se	s	k7c7	s
472	[number]	k4	472
metry	metro	k1gNnPc7	metro
<g/>
.	.	kIx.	.
</s>
<s>
Krajina	Krajina	k1gFnSc1	Krajina
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
pískovci	pískovec	k1gInPc7	pískovec
<g/>
,	,	kIx,	,
vápenci	vápenec	k1gInPc7	vápenec
a	a	k8xC	a
dolomity	dolomit	k1gInPc7	dolomit
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
také	také	k9	také
bohatá	bohatý	k2eAgNnPc4d1	bohaté
naleziště	naleziště	k1gNnPc4	naleziště
rud	ruda	k1gFnPc2	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgFnSc1d1	zdejší
půda	půda	k1gFnSc1	půda
je	být	k5eAaImIp3nS	být
úrodná	úrodný	k2eAgFnSc1d1	úrodná
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
u	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
Mosely	Mosela	k1gFnSc2	Mosela
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
rozsáhlé	rozsáhlý	k2eAgFnPc4d1	rozsáhlá
vinice	vinice	k1gFnPc4	vinice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Lucembursku	Lucembursko	k1gNnSc6	Lucembursko
je	být	k5eAaImIp3nS	být
bohatá	bohatý	k2eAgFnSc1d1	bohatá
říční	říční	k2eAgFnSc1d1	říční
síť	síť	k1gFnSc1	síť
a	a	k8xC	a
až	až	k9	až
na	na	k7c4	na
říčku	říčka	k1gFnSc4	říčka
Korn	Korna	k1gFnPc2	Korna
náleží	náležet	k5eAaImIp3nP	náležet
všechny	všechen	k3xTgFnPc1	všechen
do	do	k7c2	do
povodí	povodí	k1gNnSc2	povodí
největší	veliký	k2eAgFnSc2d3	veliký
lucemburské	lucemburský	k2eAgFnSc2d1	Lucemburská
řeky	řeka	k1gFnSc2	řeka
Mosely	Mosela	k1gFnSc2	Mosela
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
svým	svůj	k3xOyFgInSc7	svůj
tokem	tok	k1gInSc7	tok
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
přírodní	přírodní	k2eAgFnSc4d1	přírodní
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
Lucemburskem	Lucembursko	k1gNnSc7	Lucembursko
a	a	k8xC	a
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Mosela	Mosela	k1gFnSc1	Mosela
má	mít	k5eAaImIp3nS	mít
sice	sice	k8xC	sice
největší	veliký	k2eAgInSc1d3	veliký
průtok	průtok	k1gInSc1	průtok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
je	být	k5eAaImIp3nS	být
se	s	k7c7	s
160	[number]	k4	160
km	km	kA	km
řeka	řeka	k1gFnSc1	řeka
Sauer	Sauer	k1gMnSc1	Sauer
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
Sû	Sû	k1gFnSc1	Sû
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgFnSc7d1	národní
řekou	řeka	k1gFnSc7	řeka
je	být	k5eAaImIp3nS	být
Alzette	Alzett	k1gMnSc5	Alzett
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Moselou	Mosela	k1gFnSc7	Mosela
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
východní	východní	k2eAgFnSc4d1	východní
hranici	hranice	k1gFnSc4	hranice
státu	stát	k1gInSc2	stát
řeky	řeka	k1gFnSc2	řeka
Sauer	Saura	k1gFnPc2	Saura
a	a	k8xC	a
Our	Our	k1gFnPc2	Our
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
třetinu	třetina	k1gFnSc4	třetina
země	zem	k1gFnSc2	zem
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
lesy	les	k1gInPc1	les
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
především	především	k9	především
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Oesling	Oesling	k1gInSc4	Oesling
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Gutland	Gutland	k1gInSc1	Gutland
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
je	být	k5eAaImIp3nS	být
využíván	využívat	k5eAaPmNgInS	využívat
spíše	spíše	k9	spíše
zemědělsky	zemědělsky	k6eAd1	zemědělsky
<g/>
.	.	kIx.	.
</s>
<s>
Lesy	les	k1gInPc1	les
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
listnaté	listnatý	k2eAgInPc1d1	listnatý
<g/>
,	,	kIx,	,
jehličnaté	jehličnatý	k2eAgInPc1d1	jehličnatý
stromy	strom	k1gInPc1	strom
byly	být	k5eAaImAgInP	být
vysázeny	vysázet	k5eAaPmNgInP	vysázet
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
živočichů	živočich	k1gMnPc2	živočich
zde	zde	k6eAd1	zde
žijí	žít	k5eAaImIp3nP	žít
jeleni	jelen	k1gMnPc1	jelen
<g/>
,	,	kIx,	,
srnci	srnec	k1gMnPc1	srnec
<g/>
,	,	kIx,	,
kanci	kanec	k1gMnPc1	kanec
<g/>
,	,	kIx,	,
lišky	liška	k1gFnPc1	liška
<g/>
,	,	kIx,	,
kuny	kuna	k1gFnPc1	kuna
<g/>
,	,	kIx,	,
bažanti	bažant	k1gMnPc1	bažant
<g/>
,	,	kIx,	,
různí	různý	k2eAgMnPc1d1	různý
draví	dravý	k2eAgMnPc1d1	dravý
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
čápi	čáp	k1gMnPc1	čáp
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
zvěře	zvěř	k1gFnSc2	zvěř
je	být	k5eAaImIp3nS	být
chráněna	chránit	k5eAaImNgFnS	chránit
<g/>
.	.	kIx.	.
</s>
<s>
Východní	východní	k2eAgFnSc1d1	východní
část	část	k1gFnSc1	část
Oeslingu	Oesling	k1gInSc2	Oesling
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
turisticky	turisticky	k6eAd1	turisticky
atraktivní	atraktivní	k2eAgNnSc4d1	atraktivní
Malé	Malé	k2eAgNnSc4d1	Malé
lucemburské	lucemburský	k2eAgNnSc4d1	lucemburské
Švýcarsko	Švýcarsko	k1gNnSc4	Švýcarsko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
přírodního	přírodní	k2eAgInSc2d1	přírodní
parku	park	k1gInSc2	park
zasahujícího	zasahující	k2eAgInSc2d1	zasahující
i	i	k9	i
do	do	k7c2	do
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Lucemburk	Lucemburk	k1gInSc1	Lucemburk
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgNnPc4d1	další
důležitá	důležitý	k2eAgNnPc4d1	důležité
sídla	sídlo	k1gNnPc4	sídlo
patří	patřit	k5eAaImIp3nS	patřit
Esch-sur-Alzette	Eschur-Alzett	k1gInSc5	Esch-sur-Alzett
<g/>
,	,	kIx,	,
Echternach	Echternach	k1gInSc1	Echternach
<g/>
,	,	kIx,	,
Diekirch	Diekirch	k1gInSc1	Diekirch
<g/>
,	,	kIx,	,
Differdange	Differdange	k1gInSc1	Differdange
<g/>
,	,	kIx,	,
Dudelange	Dudelange	k1gInSc1	Dudelange
a	a	k8xC	a
Sanem	Sanem	k1gInSc1	Sanem
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
je	být	k5eAaImIp3nS	být
i	i	k9	i
Vianden	Viandna	k1gFnPc2	Viandna
s	s	k7c7	s
rozsáhlým	rozsáhlý	k2eAgInSc7d1	rozsáhlý
románsko-gotickým	románskootický	k2eAgInSc7d1	románsko-gotický
hradem	hrad	k1gInSc7	hrad
a	a	k8xC	a
domem	dům	k1gInSc7	dům
spisovatele	spisovatel	k1gMnSc2	spisovatel
Viktora	Viktor	k1gMnSc2	Viktor
Huga	Hugo	k1gMnSc2	Hugo
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
země	zem	k1gFnSc2	zem
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
Atlantský	atlantský	k2eAgInSc1d1	atlantský
oceán	oceán	k1gInSc1	oceán
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
kontinentální	kontinentální	k2eAgNnSc1d1	kontinentální
klima	klima	k1gNnSc1	klima
<g/>
.	.	kIx.	.
</s>
<s>
Lucembursko	Lucembursko	k1gNnSc1	Lucembursko
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
západoevropského	západoevropský	k2eAgInSc2d1	západoevropský
kontinentálního	kontinentální	k2eAgInSc2d1	kontinentální
klimatického	klimatický	k2eAgInSc2d1	klimatický
regionu	region	k1gInSc2	region
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
zde	zde	k6eAd1	zde
nejsou	být	k5eNaImIp3nP	být
žádné	žádný	k3yNgFnPc4	žádný
extrémní	extrémní	k2eAgFnPc4d1	extrémní
teploty	teplota	k1gFnPc4	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
lednová	lednový	k2eAgFnSc1d1	lednová
teplota	teplota	k1gFnSc1	teplota
přibližně	přibližně	k6eAd1	přibližně
0	[number]	k4	0
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
okolo	okolo	k7c2	okolo
17	[number]	k4	17
stupňů	stupeň	k1gInPc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Úhrn	úhrn	k1gInSc1	úhrn
ročních	roční	k2eAgFnPc2d1	roční
srážek	srážka	k1gFnPc2	srážka
činí	činit	k5eAaImIp3nS	činit
700	[number]	k4	700
-	-	kIx~	-
1000	[number]	k4	1000
mm	mm	kA	mm
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
někde	někde	k6eAd1	někde
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
až	až	k9	až
k	k	k7c3	k
1200	[number]	k4	1200
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Klima	klima	k1gNnSc1	klima
Oeslingu	Oesling	k1gInSc2	Oesling
je	být	k5eAaImIp3nS	být
vlhčí	vlhký	k2eAgInSc1d2	vlhčí
a	a	k8xC	a
chladnější	chladný	k2eAgMnSc1d2	chladnější
než	než	k8xS	než
klima	klima	k1gNnSc1	klima
Gutlandu	Gutland	k1gInSc2	Gutland
<g/>
.	.	kIx.	.
</s>
<s>
Nejtepleji	teple	k6eAd3	teple
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
řeky	řeka	k1gFnSc2	řeka
Mosely	Mosela	k1gFnSc2	Mosela
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
vinná	vinný	k2eAgFnSc1d1	vinná
réva	réva	k1gFnSc1	réva
<g/>
.	.	kIx.	.
</s>
<s>
Lucemburk	Lucemburk	k1gInSc1	Lucemburk
103	[number]	k4	103
641	[number]	k4	641
obyvatel	obyvatel	k1gMnPc2	obyvatel
Esch-sur-Alzette	Eschur-Alzett	k1gInSc5	Esch-sur-Alzett
31	[number]	k4	31
898	[number]	k4	898
obyvatel	obyvatel	k1gMnPc2	obyvatel
Differdange	Differdang	k1gFnSc2	Differdang
22	[number]	k4	22
769	[number]	k4	769
obyvatel	obyvatel	k1gMnPc2	obyvatel
Dudelange	Dudelang	k1gFnSc2	Dudelang
19	[number]	k4	19
292	[number]	k4	292
obyvatel	obyvatel	k1gMnPc2	obyvatel
Pétange	Pétang	k1gFnSc2	Pétang
16	[number]	k4	16
762	[number]	k4	762
obyvatel	obyvatel	k1gMnPc2	obyvatel
Sanem	Sanem	k1gInSc4	Sanem
14	[number]	k4	14
832	[number]	k4	832
obyvatel	obyvatel	k1gMnPc2	obyvatel
Hesperange	Hesperang	k1gFnSc2	Hesperang
14	[number]	k4	14
027	[number]	k4	027
obyvatel	obyvatel	k1gMnPc2	obyvatel
Související	související	k2eAgFnSc2d1	související
informace	informace	k1gFnSc2	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
Lucemburska	Lucembursko	k1gNnSc2	Lucembursko
a	a	k8xC	a
Politické	politický	k2eAgFnPc1d1	politická
strany	strana	k1gFnPc1	strana
v	v	k7c6	v
Lucembursku	Lucembursko	k1gNnSc6	Lucembursko
<g/>
.	.	kIx.	.
</s>
<s>
Lucembursko	Lucembursko	k1gNnSc1	Lucembursko
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
ústavy	ústava	k1gFnSc2	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1868	[number]	k4	1868
dědičnou	dědičný	k2eAgFnSc7d1	dědičná
konstituční	konstituční	k2eAgFnSc7d1	konstituční
monarchií	monarchie	k1gFnSc7	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
velkovévoda	velkovévoda	k1gMnSc1	velkovévoda
<g/>
;	;	kIx,	;
země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgNnSc7d1	jediné
velkovévodstvím	velkovévodství	k1gNnSc7	velkovévodství
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Pravomocemi	pravomoc	k1gFnPc7	pravomoc
velkovévody	velkovévoda	k1gMnPc7	velkovévoda
je	být	k5eAaImIp3nS	být
svolávat	svolávat	k5eAaImF	svolávat
a	a	k8xC	a
rozpouštět	rozpouštět	k5eAaImF	rozpouštět
parlament	parlament	k1gInSc4	parlament
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
výsledků	výsledek	k1gInPc2	výsledek
voleb	volba	k1gFnPc2	volba
jmenovat	jmenovat	k5eAaBmF	jmenovat
vládu	vláda	k1gFnSc4	vláda
a	a	k8xC	a
doživotní	doživotní	k2eAgInPc4d1	doživotní
členy	člen	k1gInPc4	člen
Státní	státní	k2eAgFnSc2d1	státní
rady	rada	k1gFnSc2	rada
<g/>
,	,	kIx,	,
schvalovat	schvalovat	k5eAaImF	schvalovat
zákony	zákon	k1gInPc4	zákon
a	a	k8xC	a
také	také	k9	také
má	mít	k5eAaImIp3nS	mít
právo	právo	k1gNnSc4	právo
veta	veto	k1gNnSc2	veto
<g/>
.	.	kIx.	.
</s>
<s>
Zákonodárným	zákonodárný	k2eAgInSc7d1	zákonodárný
orgánem	orgán	k1gInSc7	orgán
Lucemburska	Lucembursko	k1gNnSc2	Lucembursko
je	být	k5eAaImIp3nS	být
jednokomorový	jednokomorový	k2eAgInSc4d1	jednokomorový
parlament	parlament	k1gInSc4	parlament
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
60	[number]	k4	60
poslanců	poslanec	k1gMnPc2	poslanec
volených	volená	k1gFnPc2	volená
na	na	k7c4	na
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Sněmovna	sněmovna	k1gFnSc1	sněmovna
také	také	k9	také
schvaluje	schvalovat	k5eAaImIp3nS	schvalovat
složení	složení	k1gNnSc4	složení
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
je	být	k5eAaImIp3nS	být
složena	složit	k5eAaPmNgFnS	složit
z	z	k7c2	z
ministerského	ministerský	k2eAgMnSc2d1	ministerský
předsedy	předseda	k1gMnSc2	předseda
a	a	k8xC	a
dvanácti	dvanáct	k4xCc2	dvanáct
ministrů	ministr	k1gMnPc2	ministr
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
důležitým	důležitý	k2eAgInSc7d1	důležitý
orgánem	orgán	k1gInSc7	orgán
je	být	k5eAaImIp3nS	být
Státní	státní	k2eAgFnSc1d1	státní
rada	rada	k1gFnSc1	rada
s	s	k7c7	s
21	[number]	k4	21
členy	člen	k1gMnPc7	člen
jmenovanými	jmenovaný	k2eAgMnPc7d1	jmenovaný
velkovévodou	velkovévoda	k1gMnSc7	velkovévoda
na	na	k7c4	na
doživotí	doživotí	k1gNnSc4	doživotí
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
soudní	soudní	k2eAgFnSc7d1	soudní
instancí	instance	k1gFnSc7	instance
třístupňového	třístupňový	k2eAgNnSc2d1	třístupňové
soudnictví	soudnictví	k1gNnSc2	soudnictví
je	být	k5eAaImIp3nS	být
Vrchní	vrchní	k2eAgInSc1d1	vrchní
soudní	soudní	k2eAgInSc1d1	soudní
dvůr	dvůr	k1gInSc1	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Občané	občan	k1gMnPc1	občan
starší	starý	k2eAgMnPc1d2	starší
osmnácti	osmnáct	k4xCc2	osmnáct
let	léto	k1gNnPc2	léto
mají	mít	k5eAaImIp3nP	mít
povinnost	povinnost	k1gFnSc4	povinnost
účastnit	účastnit	k5eAaImF	účastnit
se	se	k3xPyFc4	se
voleb	volba	k1gFnPc2	volba
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Administrativní	administrativní	k2eAgNnSc1d1	administrativní
dělení	dělení	k1gNnSc1	dělení
Lucemburska	Lucembursko	k1gNnSc2	Lucembursko
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
12	[number]	k4	12
kantonů	kanton	k1gInPc2	kanton
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
dále	daleko	k6eAd2	daleko
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
do	do	k7c2	do
celkem	celkem	k6eAd1	celkem
105	[number]	k4	105
komunit	komunita	k1gFnPc2	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Kanton	Kanton	k1gInSc1	Kanton
Capellen	Capellen	k1gInSc1	Capellen
-	-	kIx~	-
10	[number]	k4	10
obcí	obec	k1gFnPc2	obec
Kanton	Kanton	k1gInSc1	Kanton
Clerf	Clerf	k1gMnSc1	Clerf
-	-	kIx~	-
5	[number]	k4	5
obcí	obec	k1gFnPc2	obec
Kanton	Kanton	k1gInSc1	Kanton
Diekirch	Diekirch	k1gMnSc1	Diekirch
-	-	kIx~	-
10	[number]	k4	10
obcí	obec	k1gFnPc2	obec
Kanton	Kanton	k1gInSc1	Kanton
Echternach	Echternach	k1gMnSc1	Echternach
-	-	kIx~	-
8	[number]	k4	8
obcí	obec	k1gFnPc2	obec
Kanton	Kanton	k1gInSc1	Kanton
Esch	Esch	k1gMnSc1	Esch
an	an	k?	an
der	drát	k5eAaImRp2nS	drát
Alzette	Alzett	k1gMnSc5	Alzett
-	-	kIx~	-
14	[number]	k4	14
obcí	obec	k1gFnPc2	obec
Kanton	Kanton	k1gInSc4	Kanton
Grevenmacher	Grevenmachra	k1gFnPc2	Grevenmachra
-	-	kIx~	-
8	[number]	k4	8
obcí	obec	k1gFnPc2	obec
Kanton	Kanton	k1gInSc1	Kanton
Lucemburk	Lucemburk	k1gMnSc1	Lucemburk
-	-	kIx~	-
11	[number]	k4	11
obcí	obec	k1gFnPc2	obec
Kanton	Kanton	k1gInSc1	Kanton
Mersch	Mersch	k1gMnSc1	Mersch
-	-	kIx~	-
11	[number]	k4	11
obcí	obec	k1gFnPc2	obec
Kanton	Kanton	k1gInSc4	Kanton
Redingen	Redingen	k1gInSc1	Redingen
-	-	kIx~	-
10	[number]	k4	10
obcí	obec	k1gFnPc2	obec
Kanton	Kanton	k1gInSc1	Kanton
Remich	Remich	k1gMnSc1	Remich
-	-	kIx~	-
8	[number]	k4	8
obcí	obec	k1gFnPc2	obec
Kanton	Kanton	k1gInSc4	Kanton
Vianden	Viandna	k1gFnPc2	Viandna
-	-	kIx~	-
3	[number]	k4	3
obce	obec	k1gFnSc2	obec
Kanton	Kanton	k1gInSc1	Kanton
Wiltz	Wiltz	k1gMnSc1	Wiltz
-	-	kIx~	-
7	[number]	k4	7
obcí	obec	k1gFnPc2	obec
Související	související	k2eAgFnSc2d1	související
informace	informace	k1gFnSc2	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Lucemburska	Lucembursko	k1gNnSc2	Lucembursko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Belgicko-lucemburská	belgickoucemburský	k2eAgFnSc1d1	belgicko-lucemburský
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
unie	unie	k1gFnSc1	unie
(	(	kIx(	(
<g/>
do	do	k7c2	do
účinnosti	účinnost	k1gFnSc2	účinnost
přišla	přijít	k5eAaPmAgFnS	přijít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spolupráce	spolupráce	k1gFnPc1	spolupráce
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
státy	stát	k1gInPc7	stát
se	se	k3xPyFc4	se
projevovala	projevovat	k5eAaImAgFnS	projevovat
např.	např.	kA	např.
vytvořením	vytvoření	k1gNnSc7	vytvoření
měnové	měnový	k2eAgFnSc2d1	měnová
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
používat	používat	k5eAaImF	používat
až	až	k9	až
do	do	k7c2	do
zavedení	zavedení	k1gNnSc2	zavedení
eura	euro	k1gNnSc2	euro
lucemburský	lucemburský	k2eAgInSc4d1	lucemburský
frank	frank	k1gInSc4	frank
na	na	k7c6	na
belgickém	belgický	k2eAgNnSc6d1	Belgické
území	území	k1gNnSc6	území
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Lucembursko	Lucembursko	k1gNnSc1	Lucembursko
má	mít	k5eAaImIp3nS	mít
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
HDP	HDP	kA	HDP
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
přepočteno	přepočten	k2eAgNnSc1d1	přepočteno
dle	dle	k7c2	dle
parity	parita	k1gFnSc2	parita
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
činil	činit	k5eAaImAgInS	činit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
70	[number]	k4	70
000	[number]	k4	000
$	$	kIx~	$
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
této	tento	k3xDgFnSc2	tento
vysoce	vysoce	k6eAd1	vysoce
nadprůměrné	nadprůměrný	k2eAgFnSc2d1	nadprůměrná
hodnoty	hodnota	k1gFnSc2	hodnota
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
asi	asi	k9	asi
třetinu	třetina	k1gFnSc4	třetina
pracovních	pracovní	k2eAgFnPc2d1	pracovní
sil	síla	k1gFnPc2	síla
(	(	kIx(	(
<g/>
cca	cca	kA	cca
100	[number]	k4	100
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nP	tvořit
příslušníci	příslušník	k1gMnPc1	příslušník
okolních	okolní	k2eAgInPc2d1	okolní
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
do	do	k7c2	do
Lucemburska	Lucembursko	k1gNnSc2	Lucembursko
dojíždějí	dojíždět	k5eAaImIp3nP	dojíždět
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
tudíž	tudíž	k8xC	tudíž
do	do	k7c2	do
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
započítáni	započítat	k5eAaPmNgMnP	započítat
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělství	zemědělství	k1gNnSc1	zemědělství
je	být	k5eAaImIp3nS	být
zastoupeno	zastoupit	k5eAaPmNgNnS	zastoupit
pouze	pouze	k6eAd1	pouze
0,5	[number]	k4	0,5
procenty	procento	k1gNnPc7	procento
v	v	k7c6	v
celkovém	celkový	k2eAgInSc6d1	celkový
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
</s>
<s>
Služby	služba	k1gFnPc1	služba
naopak	naopak	k6eAd1	naopak
tvoří	tvořit	k5eAaImIp3nP	tvořit
přes	přes	k7c4	přes
80	[number]	k4	80
%	%	kIx~	%
<g/>
:	:	kIx,	:
v	v	k7c6	v
Lucembursku	Lucembursko	k1gNnSc6	Lucembursko
sídlí	sídlet	k5eAaImIp3nS	sídlet
přes	přes	k7c4	přes
200	[number]	k4	200
bank	banka	k1gFnPc2	banka
<g/>
.	.	kIx.	.
</s>
<s>
Důležitá	důležitý	k2eAgFnSc1d1	důležitá
je	být	k5eAaImIp3nS	být
též	též	k9	též
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
výroba	výroba	k1gFnSc1	výroba
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
průmysl	průmysl	k1gInSc1	průmysl
slévárenský	slévárenský	k2eAgInSc1d1	slévárenský
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
v	v	k7c6	v
celkovém	celkový	k2eAgInSc6d1	celkový
úpadku	úpadek	k1gInSc6	úpadek
<g/>
.	.	kIx.	.
</s>
<s>
EU	EU	kA	EU
je	být	k5eAaImIp3nS	být
jednoznačně	jednoznačně	k6eAd1	jednoznačně
největším	veliký	k2eAgMnSc7d3	veliký
obchodním	obchodní	k2eAgMnSc7d1	obchodní
partnerem	partner	k1gMnSc7	partner
Lucemburska	Lucembursko	k1gNnSc2	Lucembursko
<g/>
;	;	kIx,	;
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
tvoří	tvořit	k5eAaImIp3nS	tvořit
téměř	téměř	k6eAd1	téměř
86	[number]	k4	86
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
zahraniční	zahraniční	k2eAgFnSc2d1	zahraniční
obchodní	obchodní	k2eAgFnSc2d1	obchodní
výměny	výměna	k1gFnSc2	výměna
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
má	mít	k5eAaImIp3nS	mít
sídlo	sídlo	k1gNnSc1	sídlo
několik	několik	k4yIc4	několik
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
institucí	instituce	k1gFnPc2	instituce
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
institucí	instituce	k1gFnPc2	instituce
EU	EU	kA	EU
(	(	kIx(	(
<g/>
Evropský	evropský	k2eAgInSc1d1	evropský
statistický	statistický	k2eAgInSc1d1	statistický
úřad	úřad	k1gInSc1	úřad
Eurostat	Eurostat	k1gInSc1	Eurostat
<g/>
,	,	kIx,	,
Soudní	soudní	k2eAgInSc1d1	soudní
dvůr	dvůr	k1gInSc1	dvůr
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
generální	generální	k2eAgInSc1d1	generální
sekretariát	sekretariát	k1gInSc1	sekretariát
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
Evropská	evropský	k2eAgFnSc1d1	Evropská
investiční	investiční	k2eAgFnSc1d1	investiční
banka	banka	k1gFnSc1	banka
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2007	[number]	k4	2007
otevřelo	otevřít	k5eAaPmAgNnS	otevřít
Lucembursko	Lucembursko	k1gNnSc1	Lucembursko
pracovní	pracovní	k2eAgNnSc1d1	pracovní
trh	trh	k1gInSc4	trh
též	též	k6eAd1	též
pro	pro	k7c4	pro
občany	občan	k1gMnPc4	občan
Česka	Česko	k1gNnSc2	Česko
bez	bez	k7c2	bez
jakéhokoliv	jakýkoliv	k3yIgNnSc2	jakýkoliv
omezení	omezení	k1gNnSc2	omezení
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Demografie	demografie	k1gFnSc2	demografie
Lucemburska	Lucembursko	k1gNnSc2	Lucembursko
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
červenci	červenec	k1gInSc3	červenec
2009	[number]	k4	2009
491775	[number]	k4	491775
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
žilo	žít	k5eAaImAgNnS	žít
82	[number]	k4	82
<g/>
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
ve	v	k7c6	v
městech	město	k1gNnPc6	město
a	a	k8xC	a
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
se	se	k3xPyFc4	se
míra	míra	k1gFnSc1	míra
urbanizace	urbanizace	k1gFnSc2	urbanizace
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
o	o	k7c4	o
procento	procento	k1gNnSc4	procento
<g/>
.	.	kIx.	.
</s>
<s>
Složení	složení	k1gNnSc1	složení
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
pestré	pestrý	k2eAgNnSc1d1	pestré
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
vysokou	vysoký	k2eAgFnSc7d1	vysoká
životní	životní	k2eAgFnSc7d1	životní
úrovní	úroveň	k1gFnSc7	úroveň
<g/>
,	,	kIx,	,
nízkou	nízký	k2eAgFnSc7d1	nízká
nezaměstnaností	nezaměstnanost	k1gFnSc7	nezaměstnanost
<g/>
,	,	kIx,	,
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
faktory	faktor	k1gInPc7	faktor
<g/>
.	.	kIx.	.
</s>
<s>
Lucemburčané	Lucemburčan	k1gMnPc1	Lucemburčan
tvoří	tvořit	k5eAaImIp3nS	tvořit
nejpočetnější	početní	k2eAgNnSc4d3	nejpočetnější
etnikum	etnikum	k1gNnSc4	etnikum
(	(	kIx(	(
<g/>
63,1	[number]	k4	63,1
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavně	hlavně	k9	hlavně
slévárenský	slévárenský	k2eAgInSc1d1	slévárenský
průmysl	průmysl	k1gInSc1	průmysl
do	do	k7c2	do
země	zem	k1gFnSc2	zem
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
přilákal	přilákat	k5eAaPmAgMnS	přilákat
mnoho	mnoho	k4c4	mnoho
Portugalců	Portugalec	k1gMnPc2	Portugalec
(	(	kIx(	(
<g/>
13,3	[number]	k4	13,3
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Italů	Ital	k1gMnPc2	Ital
(	(	kIx(	(
<g/>
4,3	[number]	k4	4,3
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
dnes	dnes	k6eAd1	dnes
tvoří	tvořit	k5eAaImIp3nP	tvořit
významnou	významný	k2eAgFnSc4d1	významná
menšinu	menšina	k1gFnSc4	menšina
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
menšinami	menšina	k1gFnPc7	menšina
jsou	být	k5eAaImIp3nP	být
Francouzi	Francouz	k1gMnPc1	Francouz
(	(	kIx(	(
<g/>
4,5	[number]	k4	4,5
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Němci	Němec	k1gMnPc1	Němec
(	(	kIx(	(
<g/>
2,3	[number]	k4	2,3
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
menšiny	menšina	k1gFnPc1	menšina
pocházejí	pocházet	k5eAaImIp3nP	pocházet
ze	z	k7c2	z
států	stát	k1gInPc2	stát
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
(	(	kIx(	(
<g/>
7,3	[number]	k4	7,3
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
ze	z	k7c2	z
zbytku	zbytek	k1gInSc2	zbytek
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
5,2	[number]	k4	5,2
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
pěti	pět	k4xCc2	pět
let	léto	k1gNnPc2	léto
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
povinná	povinný	k2eAgFnSc1d1	povinná
předškolní	předškolní	k2eAgFnSc1d1	předškolní
docházka	docházka	k1gFnSc1	docházka
<g/>
.	.	kIx.	.
</s>
<s>
Školní	školní	k2eAgFnSc4d1	školní
docházku	docházka	k1gFnSc4	docházka
musí	muset	k5eAaImIp3nP	muset
děti	dítě	k1gFnPc1	dítě
plnit	plnit	k5eAaImF	plnit
od	od	k7c2	od
šesti	šest	k4xCc2	šest
do	do	k7c2	do
patnácti	patnáct	k4xCc2	patnáct
let	léto	k1gNnPc2	léto
a	a	k8xC	a
vyučování	vyučování	k1gNnSc1	vyučování
probíhá	probíhat	k5eAaImIp3nS	probíhat
vícejazyčně	vícejazyčně	k6eAd1	vícejazyčně
<g/>
.	.	kIx.	.
</s>
<s>
Vysokou	vysoký	k2eAgFnSc4d1	vysoká
školu	škola	k1gFnSc4	škola
studují	studovat	k5eAaImIp3nP	studovat
Lucemburčané	Lucemburčan	k1gMnPc1	Lucemburčan
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
v	v	k7c6	v
Belgii	Belgie	k1gFnSc6	Belgie
a	a	k8xC	a
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Lucemburčané	Lucemburčan	k1gMnPc1	Lucemburčan
jsou	být	k5eAaImIp3nP	být
první	první	k4xOgInSc4	první
na	na	k7c6	na
světě	svět	k1gInSc6	svět
ve	v	k7c6	v
spotřebě	spotřeba	k1gFnSc6	spotřeba
vína	víno	k1gNnSc2	víno
a	a	k8xC	a
šestí	šestý	k4xOgMnPc1	šestý
ve	v	k7c6	v
spotřebě	spotřeba	k1gFnSc6	spotřeba
piva	pivo	k1gNnSc2	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Úředními	úřední	k2eAgInPc7d1	úřední
jazyky	jazyk	k1gInPc7	jazyk
v	v	k7c6	v
Lucembursku	Lucembursko	k1gNnSc6	Lucembursko
jsou	být	k5eAaImIp3nP	být
francouzština	francouzština	k1gFnSc1	francouzština
<g/>
,	,	kIx,	,
němčina	němčina	k1gFnSc1	němčina
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
i	i	k8xC	i
lucemburština	lucemburština	k1gFnSc1	lucemburština
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
z	z	k7c2	z
pofrancouzštělého	pofrancouzštělý	k2eAgInSc2d1	pofrancouzštělý
dialektu	dialekt	k1gInSc2	dialekt
moselské	moselský	k2eAgFnSc2d1	moselská
němčiny	němčina	k1gFnSc2	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Francouzština	francouzština	k1gFnSc1	francouzština
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
v	v	k7c6	v
politických	politický	k2eAgInPc6d1	politický
kruzích	kruh	k1gInPc6	kruh
<g/>
,	,	kIx,	,
státních	státní	k2eAgFnPc6d1	státní
institucích	instituce	k1gFnPc6	instituce
<g/>
,	,	kIx,	,
literatuře	literatura	k1gFnSc6	literatura
a	a	k8xC	a
vyšším	vysoký	k2eAgInSc6d2	vyšší
druhu	druh	k1gInSc6	druh
školství	školství	k1gNnSc2	školství
<g/>
.	.	kIx.	.
</s>
<s>
Němčina	němčina	k1gFnSc1	němčina
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
jazyk	jazyk	k1gInSc4	jazyk
obchodníků	obchodník	k1gMnPc2	obchodník
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vyučována	vyučovat	k5eAaImNgFnS	vyučovat
především	především	k9	především
na	na	k7c6	na
základních	základní	k2eAgFnPc6d1	základní
školách	škola	k1gFnPc6	škola
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
převážné	převážný	k2eAgFnSc6d1	převážná
míře	míra	k1gFnSc6	míra
používána	používán	k2eAgFnSc1d1	používána
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
<g/>
.	.	kIx.	.
</s>
<s>
Lucemburština	Lucemburština	k1gFnSc1	Lucemburština
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
místní	místní	k2eAgInSc4d1	místní
dialekt	dialekt	k1gInSc4	dialekt
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
hlavně	hlavně	k9	hlavně
doma	doma	k6eAd1	doma
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
vychází	vycházet	k5eAaImIp3nS	vycházet
i	i	k9	i
tisk	tisk	k1gInSc1	tisk
<g/>
,	,	kIx,	,
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
v	v	k7c6	v
rozhlase	rozhlas	k1gInSc6	rozhlas
i	i	k8xC	i
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
a	a	k8xC	a
v	v	k7c6	v
lucemburštině	lucemburština	k1gFnSc6	lucemburština
vychází	vycházet	k5eAaImIp3nS	vycházet
už	už	k6eAd1	už
i	i	k9	i
první	první	k4xOgFnPc1	první
knihy	kniha	k1gFnPc1	kniha
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vjezdů	vjezd	k1gInPc2	vjezd
do	do	k7c2	do
měst	město	k1gNnPc2	město
a	a	k8xC	a
vesnic	vesnice	k1gFnPc2	vesnice
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
běžně	běžně	k6eAd1	běžně
užívaným	užívaný	k2eAgInSc7d1	užívaný
názvem	název	k1gInSc7	název
na	na	k7c4	na
ceduli	cedule	k1gFnSc4	cedule
zobrazena	zobrazen	k2eAgFnSc1d1	zobrazena
i	i	k8xC	i
jeho	jeho	k3xOp3gFnSc1	jeho
podoba	podoba	k1gFnSc1	podoba
v	v	k7c6	v
lucemburštině	lucemburština	k1gFnSc6	lucemburština
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zákona	zákon	k1gInSc2	zákon
z	z	k7c2	z
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1984	[number]	k4	1984
je	být	k5eAaImIp3nS	být
národním	národní	k2eAgInSc7d1	národní
jazykem	jazyk	k1gInSc7	jazyk
lucemburština	lucemburština	k1gFnSc1	lucemburština
<g/>
,	,	kIx,	,
texty	text	k1gInPc1	text
zákonů	zákon	k1gInPc2	zákon
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc2	jejich
prováděcích	prováděcí	k2eAgInPc2d1	prováděcí
předpisů	předpis	k1gInPc2	předpis
se	se	k3xPyFc4	se
píší	psát	k5eAaImIp3nP	psát
ve	v	k7c6	v
francouzštině	francouzština	k1gFnSc6	francouzština
<g/>
,	,	kIx,	,
ve	v	k7c6	v
správním	správní	k2eAgInSc6d1	správní
a	a	k8xC	a
právním	právní	k2eAgInSc6d1	právní
styku	styk	k1gInSc6	styk
lze	lze	k6eAd1	lze
používat	používat	k5eAaImF	používat
francouzštinu	francouzština	k1gFnSc4	francouzština
<g/>
,	,	kIx,	,
němčinu	němčina	k1gFnSc4	němčina
nebo	nebo	k8xC	nebo
lucemburštinu	lucemburština	k1gFnSc4	lucemburština
<g/>
.	.	kIx.	.
</s>
<s>
Občané	občan	k1gMnPc1	občan
mohou	moct	k5eAaImIp3nP	moct
psát	psát	k5eAaImF	psát
ve	v	k7c6	v
styku	styk	k1gInSc6	styk
s	s	k7c7	s
úřady	úřada	k1gMnPc7	úřada
lucembursky	lucembursky	k6eAd1	lucembursky
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
nebo	nebo	k8xC	nebo
německy	německy	k6eAd1	německy
<g/>
,	,	kIx,	,
odpověď	odpověď	k1gFnSc1	odpověď
musí	muset	k5eAaImIp3nS	muset
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
dalece	dalece	k?	dalece
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
následovat	následovat	k5eAaImF	následovat
v	v	k7c6	v
témž	týž	k3xTgInSc6	týž
jazyce	jazyk	k1gInSc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
už	už	k6eAd1	už
němčina	němčina	k1gFnSc1	němčina
není	být	k5eNaImIp3nS	být
národním	národní	k2eAgInSc7d1	národní
jazykem	jazyk	k1gInSc7	jazyk
(	(	kIx(	(
<g/>
ačkoliv	ačkoliv	k8xS	ačkoliv
je	být	k5eAaImIp3nS	být
jazykem	jazyk	k1gInSc7	jazyk
úředním	úřední	k2eAgInSc7d1	úřední
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nemohlo	moct	k5eNaImAgNnS	moct
se	se	k3xPyFc4	se
Lucembursko	Lucembursko	k1gNnSc1	Lucembursko
například	například	k6eAd1	například
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
diskuzí	diskuze	k1gFnSc7	diskuze
o	o	k7c6	o
poslední	poslední	k2eAgFnSc6d1	poslední
reformě	reforma	k1gFnSc6	reforma
jejího	její	k3xOp3gInSc2	její
pravopisu	pravopis	k1gInSc2	pravopis
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Náboženství	náboženství	k1gNnSc2	náboženství
v	v	k7c6	v
Lucembursku	Lucembursko	k1gNnSc6	Lucembursko
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
87	[number]	k4	87
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
římskokatolického	římskokatolický	k2eAgNnSc2d1	římskokatolické
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
.	.	kIx.	.
</s>
<s>
Zbylých	zbylý	k2eAgNnPc2d1	zbylé
13	[number]	k4	13
%	%	kIx~	%
tvoří	tvořit	k5eAaImIp3nP	tvořit
protestanti	protestant	k1gMnPc1	protestant
<g/>
,	,	kIx,	,
židé	žid	k1gMnPc1	žid
<g/>
,	,	kIx,	,
muslimové	muslim	k1gMnPc1	muslim
a	a	k8xC	a
jiní	jiný	k2eAgMnPc1d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kultura	kultura	k1gFnSc1	kultura
Lucemburska	Lucembursko	k1gNnSc2	Lucembursko
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Historie	historie	k1gFnSc2	historie
lucemburské	lucemburský	k2eAgFnSc2d1	Lucemburská
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Fotografie	fotografia	k1gFnPc1	fotografia
v	v	k7c6	v
Lucembursku	Lucembursko	k1gNnSc6	Lucembursko
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
spojována	spojovat	k5eAaImNgFnS	spojovat
se	s	k7c7	s
dvěma	dva	k4xCgMnPc7	dva
významnými	významný	k2eAgMnPc7d1	významný
muži	muž	k1gMnPc7	muž
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
Lucembursku	Lucembursko	k1gNnSc6	Lucembursko
narodili	narodit	k5eAaPmAgMnP	narodit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odešli	odejít	k5eAaPmAgMnP	odejít
ještě	ještě	k6eAd1	ještě
když	když	k8xS	když
byli	být	k5eAaImAgMnP	být
velmi	velmi	k6eAd1	velmi
mladí	mladý	k2eAgMnPc1d1	mladý
<g/>
:	:	kIx,	:
Edward	Edward	k1gMnSc1	Edward
Steichen	Steichna	k1gFnPc2	Steichna
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
-	-	kIx~	-
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
Američan	Američan	k1gMnSc1	Američan
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
dělal	dělat	k5eAaImAgMnS	dělat
vynikající	vynikající	k2eAgMnSc1d1	vynikající
módní	módní	k2eAgMnSc1d1	módní
a	a	k8xC	a
válečné	válečný	k2eAgFnPc1d1	válečná
fotografie	fotografia	k1gFnPc1	fotografia
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Gabriel	Gabriel	k1gMnSc1	Gabriel
Lippmann	Lippmann	k1gMnSc1	Lippmann
(	(	kIx(	(
<g/>
1845	[number]	k4	1845
<g/>
-	-	kIx~	-
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
Francouz	Francouz	k1gMnSc1	Francouz
získal	získat	k5eAaPmAgMnS	získat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
za	za	k7c4	za
své	svůj	k3xOyFgInPc4	svůj
úspěchy	úspěch	k1gInPc4	úspěch
v	v	k7c6	v
barevné	barevný	k2eAgFnSc6d1	barevná
fotografii	fotografia	k1gFnSc6	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
však	však	k9	však
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgMnPc2d1	další
Lucemburčanů	Lucemburčan	k1gMnPc2	Lucemburčan
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
dokumentovali	dokumentovat	k5eAaBmAgMnP	dokumentovat
rozvoj	rozvoj	k1gInSc4	rozvoj
města	město	k1gNnSc2	město
Lucemburku	Lucemburk	k1gInSc2	Lucemburk
a	a	k8xC	a
země	zem	k1gFnSc2	zem
jako	jako	k8xS	jako
celku	celek	k1gInSc2	celek
od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
až	až	k9	až
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
zemích	zem	k1gFnPc6	zem
byl	být	k5eAaImAgInS	být
vývoj	vývoj	k1gInSc1	vývoj
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
řemesla	řemeslo	k1gNnSc2	řemeslo
a	a	k8xC	a
umění	umění	k1gNnSc2	umění
fotografie	fotografia	k1gFnSc2	fotografia
v	v	k7c6	v
Lucembursku	Lucembursko	k1gNnSc6	Lucembursko
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
důsledkem	důsledek	k1gInSc7	důsledek
změn	změna	k1gFnPc2	změna
technologie	technologie	k1gFnSc1	technologie
<g/>
,	,	kIx,	,
zlepšování	zlepšování	k1gNnSc1	zlepšování
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
podmínek	podmínka	k1gFnPc2	podmínka
a	a	k8xC	a
míry	míra	k1gFnSc2	míra
uznání	uznání	k1gNnSc2	uznání
fotografie	fotografia	k1gFnSc2	fotografia
jako	jako	k8xC	jako
svéprávné	svéprávný	k2eAgFnSc2d1	svéprávná
formy	forma	k1gFnSc2	forma
umění	umění	k1gNnSc2	umění
Lucemburska	Lucembursko	k1gNnSc2	Lucembursko
<g/>
.	.	kIx.	.
</s>
