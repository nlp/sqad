<s>
Zákonodárným	zákonodárný	k2eAgInSc7d1	zákonodárný
orgánem	orgán	k1gInSc7	orgán
Lucemburska	Lucembursko	k1gNnSc2	Lucembursko
je	být	k5eAaImIp3nS	být
jednokomorový	jednokomorový	k2eAgInSc4d1	jednokomorový
parlament	parlament	k1gInSc4	parlament
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
60	[number]	k4	60
poslanců	poslanec	k1gMnPc2	poslanec
volených	volená	k1gFnPc2	volená
na	na	k7c4	na
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
