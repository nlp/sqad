<s>
Knockout	Knockout	k1gMnSc1
</s>
<s>
Odpočítávání	odpočítávání	k1gNnSc1
ležícího	ležící	k2eAgMnSc2d1
boxera	boxer	k1gMnSc2
</s>
<s>
Knockout	Knockout	k1gMnSc1
<g/>
,	,	kIx,
počeštěně	počeštěně	k6eAd1
knokaut	knokaut	k1gInSc1
<g/>
,	,	kIx,
zkráceně	zkráceně	k6eAd1
K.	K.	kA
O.	O.	kA
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
způsob	způsob	k1gInSc1
předčasného	předčasný	k2eAgNnSc2d1
ukončení	ukončení	k1gNnSc2
utkání	utkání	k1gNnSc2
v	v	k7c6
MMA	MMA	kA
<g/>
,	,	kIx,
boxu	box	k1gInSc2
<g/>
,	,	kIx,
kickboxu	kickbox	k1gInSc2
<g/>
,	,	kIx,
muay	muaa	k1gFnSc2
thai	tha	k1gFnSc2
<g/>
,	,	kIx,
taekwondu	taekwondo	k1gNnSc6
a	a	k8xC
karate	karate	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postižený	postižený	k1gMnSc1
zápasník	zápasník	k1gMnSc1
obdrží	obdržet	k5eAaPmIp3nS
úder	úder	k1gInSc4
na	na	k7c4
hlavu	hlava	k1gFnSc4
(	(	kIx(
<g/>
nejčastěji	často	k6eAd3
na	na	k7c4
spodní	spodní	k2eAgFnSc4d1
čelist	čelist	k1gFnSc4
<g/>
)	)	kIx)
natolik	natolik	k6eAd1
silný	silný	k2eAgInSc1d1
<g/>
,	,	kIx,
že	že	k8xS
mu	on	k3xPp3gMnSc3
způsobí	způsobit	k5eAaPmIp3nS
otřes	otřes	k1gInSc1
mozku	mozek	k1gInSc2
–	–	k?
v	v	k7c6
důsledku	důsledek	k1gInSc6
toho	ten	k3xDgNnSc2
ztrácí	ztrácet	k5eAaImIp3nP
orientaci	orientace	k1gFnSc3
a	a	k8xC
nemůže	moct	k5eNaImIp3nS
se	se	k3xPyFc4
udržet	udržet	k5eAaPmF
na	na	k7c6
nohou	noha	k1gFnPc6
(	(	kIx(
<g/>
tzv.	tzv.	kA
knockdown	knockdown	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhodčí	rozhodčí	k1gMnSc1
pak	pak	k9
musí	muset	k5eAaImIp3nS
posoudit	posoudit	k5eAaPmF
<g/>
,	,	kIx,
zda	zda	k8xS
je	být	k5eAaImIp3nS
sportovec	sportovec	k1gMnSc1
schopen	schopen	k2eAgMnSc1d1
znovu	znovu	k6eAd1
se	se	k3xPyFc4
zapojit	zapojit	k5eAaPmF
do	do	k7c2
zápasu	zápas	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
boxu	box	k1gInSc6
se	se	k3xPyFc4
tradičně	tradičně	k6eAd1
používá	používat	k5eAaImIp3nS
hlasité	hlasitý	k2eAgNnSc4d1
odpočítávání	odpočítávání	k1gNnSc4
do	do	k7c2
deseti	deset	k4xCc2
sekund	sekunda	k1gFnPc2
<g/>
,	,	kIx,
pokud	pokud	k8xS
se	se	k3xPyFc4
boxer	boxer	k1gInSc1
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
nestačí	stačit	k5eNaBmIp3nS
zvednout	zvednout	k5eAaPmF
(	(	kIx(
<g/>
nesmí	smět	k5eNaImIp3nS
se	se	k3xPyFc4
opírat	opírat	k5eAaImF
o	o	k7c4
podlahu	podlaha	k1gFnSc4
ani	ani	k8xC
o	o	k7c4
provazy	provaz	k1gInPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
prohlášen	prohlásit	k5eAaPmNgMnS
za	za	k7c4
poraženého	poražený	k2eAgMnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
mixed	mixed	k1gInSc4
martial	martial	k1gInSc4
arts	arts	k6eAd1
se	se	k3xPyFc4
za	za	k7c4
knockout	knockout	k1gMnSc1
označuje	označovat	k5eAaImIp3nS
ztráta	ztráta	k1gFnSc1
vědomí	vědomí	k1gNnSc2
po	po	k7c6
úderu	úder	k1gInSc6
či	či	k8xC
kopu	kopat	k5eAaImIp1nS
automaticky	automaticky	k6eAd1
<g/>
,	,	kIx,
bez	bez	k7c2
odpočítávání	odpočítávání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Rozhodčí	rozhodčí	k1gMnSc1
může	moct	k5eAaImIp3nS
vyhlásit	vyhlásit	k5eAaPmF
také	také	k9
takzvaný	takzvaný	k2eAgMnSc1d1
technický	technický	k2eAgMnSc1d1
knockout	knockout	k1gMnSc1
bez	bez	k7c2
předchozího	předchozí	k2eAgInSc2d1
knockdownu	knockdown	k1gInSc2
(	(	kIx(
<g/>
zpravidla	zpravidla	k6eAd1
po	po	k7c6
poradě	porada	k1gFnSc6
s	s	k7c7
lékařem	lékař	k1gMnSc7
nebo	nebo	k8xC
trenéry	trenér	k1gMnPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Technický	technický	k2eAgInSc1d1
knockout	knockout	k1gMnSc1
(	(	kIx(
<g/>
T.	T.	kA
K.	K.	kA
O.	O.	kA
<g/>
)	)	kIx)
nastává	nastávat	k5eAaImIp3nS
v	v	k7c6
situaci	situace	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
zápasník	zápasník	k1gMnSc1
nemůže	moct	k5eNaImIp3nS
pokračovat	pokračovat	k5eAaImF
v	v	k7c6
boji	boj	k1gInSc6
kvůli	kvůli	k7c3
zranění	zranění	k1gNnSc3
nebo	nebo	k8xC
vyčerpání	vyčerpání	k1gNnSc3
<g/>
,	,	kIx,
případně	případně	k6eAd1
tehdy	tehdy	k6eAd1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
převaha	převaha	k1gFnSc1
jednoho	jeden	k4xCgMnSc2
ze	z	k7c2
soupeřů	soupeř	k1gMnPc2
natolik	natolik	k6eAd1
zřetelná	zřetelný	k2eAgFnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
další	další	k2eAgNnSc1d1
prodlužování	prodlužování	k1gNnSc1
nemá	mít	k5eNaImIp3nS
smysl	smysl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
profesionálním	profesionální	k2eAgInSc6d1
boxu	box	k1gInSc6
se	se	k3xPyFc4
zavedl	zavést	k5eAaPmAgInS
zvyk	zvyk	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
sekundant	sekundant	k1gMnSc1
signalizuje	signalizovat	k5eAaImIp3nS
žádost	žádost	k1gFnSc4
o	o	k7c4
technický	technický	k2eAgInSc4d1
knockout	knockout	k5eAaPmF,k5eAaImF
vhozením	vhození	k1gNnSc7
ručníku	ručník	k1gInSc2
do	do	k7c2
ringu	ring	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Termín	termín	k1gInSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
také	také	k9
v	v	k7c6
přeneseném	přenesený	k2eAgInSc6d1
významu	význam	k1gInSc6
jako	jako	k9
rozhodující	rozhodující	k2eAgInSc4d1
<g/>
,	,	kIx,
definitivní	definitivní	k2eAgInSc4d1
úder	úder	k1gInSc4
<g/>
,	,	kIx,
např.	např.	kA
genový	genový	k2eAgMnSc1d1
knockout	knockout	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyřazovací	vyřazovací	k2eAgInSc1d1
systém	systém	k1gInSc1
sportovních	sportovní	k2eAgInPc2d1
turnajů	turnaj	k1gInPc2
bývá	bývat	k5eAaImIp3nS
zkráceně	zkráceně	k6eAd1
označován	označovat	k5eAaImNgInS
jako	jako	k9
K.	K.	kA
O.	O.	kA
systém	systém	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
http://www.pharmapoint.cz/zajimavosti/knokaut-jak-funguje-nejslavnejsi-boxersky-uder/	http://www.pharmapoint.cz/zajimavosti/knokaut-jak-funguje-nejslavnejsi-boxersky-uder/	k?
</s>
<s>
https://web.archive.org/web/20170315085026/http://www.mma-shop.cz/knockout-ko.html	https://web.archive.org/web/20170315085026/http://www.mma-shop.cz/knockout-ko.html	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sport	sport	k1gInSc1
</s>
