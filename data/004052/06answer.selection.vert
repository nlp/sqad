<s>
Jahody	jahoda	k1gFnPc1	jahoda
se	se	k3xPyFc4	se
využívají	využívat	k5eAaPmIp3nP	využívat
také	také	k9	také
v	v	k7c6	v
kosmetice	kosmetika	k1gFnSc6	kosmetika
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
manganu	mangan	k1gInSc2	mangan
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
prospěšný	prospěšný	k2eAgInSc1d1	prospěšný
pro	pro	k7c4	pro
pevnost	pevnost	k1gFnSc4	pevnost
a	a	k8xC	a
růst	růst	k1gInSc4	růst
vlasů	vlas	k1gInPc2	vlas
a	a	k8xC	a
pokožky	pokožka	k1gFnSc2	pokožka
<g/>
.	.	kIx.	.
</s>
