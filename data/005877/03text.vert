<s>
Sophia	Sophia	k1gFnSc1	Sophia
Anna	Anna	k1gFnSc1	Anna
Bushová	Bushová	k1gFnSc1	Bushová
(	(	kIx(	(
<g/>
*	*	kIx~	*
8	[number]	k4	8
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
Pasadena	Pasaden	k2eAgFnSc1d1	Pasadena
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
režisérka	režisérka	k1gFnSc1	režisérka
a	a	k8xC	a
mluvčí	mluvčí	k1gFnSc1	mluvčí
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
se	se	k3xPyFc4	se
proslavila	proslavit	k5eAaPmAgFnS	proslavit
rolí	role	k1gFnPc2	role
v	v	k7c6	v
televizním	televizní	k2eAgInSc6d1	televizní
seriálu	seriál	k1gInSc6	seriál
stanice	stanice	k1gFnSc2	stanice
CW	CW	kA	CW
One	One	k1gMnSc1	One
Tree	Tre	k1gFnSc2	Tre
Hill	Hill	k1gMnSc1	Hill
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
znázorňovala	znázorňovat	k5eAaImAgFnS	znázorňovat
Brooke	Brooke	k1gFnSc4	Brooke
Davisovou	Davisový	k2eAgFnSc4d1	Davisová
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
svými	svůj	k3xOyFgFnPc7	svůj
filmovými	filmový	k2eAgFnPc7d1	filmová
rolemi	role	k1gFnPc7	role
v	v	k7c6	v
John	John	k1gMnSc1	John
Tucker	Tucker	k1gMnSc1	Tucker
musí	muset	k5eAaImIp3nS	muset
zemřít	zemřít	k5eAaPmF	zemřít
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Stopař	stopař	k1gMnSc1	stopař
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
a	a	k8xC	a
Narrows	Narrows	k1gInSc1	Narrows
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
hraje	hrát	k5eAaImIp3nS	hrát
detektivku	detektivka	k1gFnSc4	detektivka
Erin	Erin	k1gInSc4	Erin
Lindsay	Lindsaa	k1gFnSc2	Lindsaa
v	v	k7c6	v
dramatickém	dramatický	k2eAgInSc6d1	dramatický
seriálu	seriál	k1gInSc6	seriál
stanice	stanice	k1gFnSc2	stanice
NBC	NBC	kA	NBC
Chicago	Chicago	k1gNnSc1	Chicago
P.	P.	kA	P.
<g/>
D.	D.	kA	D.
<g/>
.	.	kIx.	.
</s>
<s>
Bushová	Bushová	k1gFnSc1	Bushová
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
v	v	k7c6	v
kalifornském	kalifornský	k2eAgNnSc6d1	kalifornské
městě	město	k1gNnSc6	město
Pasadeně	Pasadena	k1gFnSc6	Pasadena
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jediným	jediný	k2eAgNnSc7d1	jediné
dítětem	dítě	k1gNnSc7	dítě
Maureen	Maurena	k1gFnPc2	Maurena
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k1gMnSc1	vedoucí
fotografického	fotografický	k2eAgNnSc2d1	fotografické
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
a	a	k8xC	a
Charlese	Charles	k1gMnSc2	Charles
Williama	William	k1gMnSc2	William
Bushe	Bush	k1gMnSc2	Bush
<g/>
,	,	kIx,	,
fotografa	fotograf	k1gMnSc2	fotograf
celebrit	celebrita	k1gFnPc2	celebrita
a	a	k8xC	a
reklam	reklama	k1gFnPc2	reklama
<g/>
.	.	kIx.	.
</s>
<s>
Bushová	Bushová	k1gFnSc1	Bushová
je	být	k5eAaImIp3nS	být
italského	italský	k2eAgInSc2d1	italský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
ukončila	ukončit	k5eAaPmAgFnS	ukončit
docházku	docházka	k1gFnSc4	docházka
na	na	k7c6	na
dívčí	dívčí	k2eAgFnSc6d1	dívčí
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
Westridge	Westridg	k1gInSc2	Westridg
School	School	k1gInSc4	School
for	forum	k1gNnPc2	forum
Girls	girl	k1gFnPc7	girl
v	v	k7c6	v
Pasadeně	Pasadena	k1gFnSc6	Pasadena
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
členkou	členka	k1gFnSc7	členka
volejbalového	volejbalový	k2eAgInSc2d1	volejbalový
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Westridge	Westridge	k1gFnSc6	Westridge
byla	být	k5eAaImAgFnS	být
žádána	žádán	k2eAgFnSc1d1	žádána
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
tamějšího	tamější	k2eAgInSc2d1	tamější
programu	program	k1gInSc2	program
divadelních	divadelní	k2eAgNnPc2d1	divadelní
umění	umění	k1gNnPc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Bushová	Bushová	k1gFnSc1	Bushová
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Částí	část	k1gFnPc2	část
požadavků	požadavek	k1gInPc2	požadavek
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
na	na	k7c4	na
mne	já	k3xPp1nSc4	já
byly	být	k5eAaImAgInP	být
kladeny	klást	k5eAaImNgInP	klást
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
hrála	hrát	k5eAaImAgFnS	hrát
ve	v	k7c6	v
školní	školní	k2eAgFnSc6d1	školní
hře	hra	k1gFnSc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
jsem	být	k5eAaImIp1nS	být
vážně	vážně	k6eAd1	vážně
naštvaná	naštvaný	k2eAgFnSc1d1	naštvaná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsem	být	k5eAaImIp1nS	být
chtěla	chtít	k5eAaImAgFnS	chtít
hrát	hrát	k5eAaImF	hrát
volejbal	volejbal	k1gInSc4	volejbal
a	a	k8xC	a
musela	muset	k5eAaImAgFnS	muset
jsem	být	k5eAaImIp1nS	být
odejít	odejít	k5eAaPmF	odejít
a	a	k8xC	a
hrát	hrát	k5eAaImF	hrát
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
hře	hra	k1gFnSc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k8xC	ale
objevil	objevit	k5eAaPmAgInS	objevit
se	se	k3xPyFc4	se
okamžik	okamžik	k1gInSc1	okamžik
následující	následující	k2eAgInSc1d1	následující
po	po	k7c6	po
vystoupení	vystoupení	k1gNnSc6	vystoupení
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
uvědomila	uvědomit	k5eAaPmAgFnS	uvědomit
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsem	být	k5eAaImIp1nS	být
byla	být	k5eAaImAgFnS	být
duchem	duch	k1gMnSc7	duch
jinde	jinde	k6eAd1	jinde
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
někým	někdo	k3yInSc7	někdo
jiným	jiný	k2eAgNnSc7d1	jiné
<g/>
.	.	kIx.	.
</s>
<s>
Pomyslela	pomyslet	k5eAaPmAgFnS	pomyslet
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
Kéž	kéž	k9	kéž
bych	by	kYmCp1nS	by
tak	tak	k9	tak
tohle	tenhle	k3xDgNnSc4	tenhle
mohla	moct	k5eAaImAgFnS	moct
dělat	dělat	k5eAaImF	dělat
po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
mého	můj	k3xOp1gInSc2	můj
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
jsem	být	k5eAaImIp1nS	být
připravena	připravit	k5eAaPmNgFnS	připravit
<g/>
.	.	kIx.	.
<g/>
'	'	kIx"	'
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc4	ten
jako	jako	k9	jako
láska	láska	k1gFnSc1	láska
na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
17	[number]	k4	17
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
Bushová	Bushová	k1gFnSc1	Bushová
jmenována	jmenovat	k5eAaImNgFnS	jmenovat
královnou	královna	k1gFnSc7	královna
přehlídky	přehlídka	k1gFnSc2	přehlídka
Tournament	Tournament	k1gInSc1	Tournament
of	of	k?	of
Roses	Roses	k1gInSc1	Roses
Parade	Parad	k1gInSc5	Parad
<g/>
.	.	kIx.	.
</s>
<s>
Docházela	docházet	k5eAaImAgFnS	docházet
na	na	k7c4	na
Univerzitu	univerzita	k1gFnSc4	univerzita
Jižní	jižní	k2eAgFnSc2d1	jižní
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
(	(	kIx(	(
<g/>
USC	USC	kA	USC
<g/>
)	)	kIx)	)
na	na	k7c4	na
obor	obor	k1gInSc4	obor
žurnalistika	žurnalistika	k1gFnSc1	žurnalistika
druhým	druhý	k4xOgInSc7	druhý
rokem	rok	k1gInSc7	rok
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
nováčkovském	nováčkovský	k2eAgInSc6d1	nováčkovský
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
členkou	členka	k1gFnSc7	členka
klubu	klub	k1gInSc2	klub
Social	Social	k1gMnSc1	Social
Chair	Chair	k1gMnSc1	Chair
sestřenstva	sestřenstvo	k1gNnSc2	sestřenstvo
Kappa	kappa	k1gNnSc2	kappa
Kappa	kappa	k1gNnSc2	kappa
Gamma	Gammum	k1gNnSc2	Gammum
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
velkou	velký	k2eAgFnSc7d1	velká
rolí	role	k1gFnSc7	role
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
Sophii	Sophie	k1gFnSc4	Sophie
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
Sexy	sex	k1gInPc4	sex
párty	párty	k1gFnSc3	párty
<g/>
,	,	kIx,	,
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Ryana	Ryan	k1gMnSc2	Ryan
Reynoldse	Reynolds	k1gMnSc2	Reynolds
<g/>
.	.	kIx.	.
</s>
<s>
Následovali	následovat	k5eAaImAgMnP	následovat
menší	malý	k2eAgFnPc4d2	menší
role	role	k1gFnPc4	role
v	v	k7c6	v
seriálech	seriál	k1gInPc6	seriál
jako	jako	k8xS	jako
Sabrina	Sabrina	k1gFnSc1	Sabrina
-	-	kIx~	-
mladá	mladý	k2eAgFnSc1d1	mladá
čarodejnice	čarodejnice	k1gFnSc1	čarodejnice
,	,	kIx,	,
Napálené	napálený	k2eAgFnPc1d1	napálená
celebrity	celebrita	k1gFnPc1	celebrita
<g/>
,	,	kIx,	,
Plastická	plastický	k2eAgFnSc1d1	plastická
chirurgie	chirurgie	k1gFnSc1	chirurgie
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
a	a	k8xC	a
Bořiči	bořič	k1gMnPc1	bořič
mýtů	mýtus	k1gInPc2	mýtus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
byla	být	k5eAaImAgFnS	být
obsazena	obsadit	k5eAaPmNgFnS	obsadit
do	do	k7c2	do
role	role	k1gFnSc2	role
Kate	kat	k1gInSc5	kat
Brewster	Brewstrum	k1gNnPc2	Brewstrum
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Terminátor	terminátor	k1gInSc1	terminátor
3	[number]	k4	3
<g/>
:	:	kIx,	:
Vzpoura	vzpoura	k1gFnSc1	vzpoura
strojů	stroj	k1gInPc2	stroj
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
Claire	Clair	k1gInSc5	Clair
Danes	Danes	k1gInSc1	Danes
týden	týden	k1gInSc4	týden
po	po	k7c6	po
natáčení	natáčení	k1gNnSc6	natáčení
<g/>
.	.	kIx.	.
</s>
<s>
Režisér	režisér	k1gMnSc1	režisér
filmu	film	k1gInSc2	film
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
pro	pro	k7c4	pro
roli	role	k1gFnSc4	role
zdála	zdát	k5eAaImAgFnS	zdát
moc	moc	k6eAd1	moc
mladá	mladý	k2eAgFnSc1d1	mladá
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc4	ten
samý	samý	k3xTgInSc4	samý
rok	rok	k1gInSc4	rok
získala	získat	k5eAaPmAgFnS	získat
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
stanice	stanice	k1gFnSc2	stanice
The	The	k1gFnSc2	The
CW	CW	kA	CW
One	One	k1gMnSc4	One
Tree	Tre	k1gMnSc4	Tre
Hill	Hill	k1gMnSc1	Hill
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
si	se	k3xPyFc3	se
ihned	ihned	k6eAd1	ihned
našel	najít	k5eAaPmAgMnS	najít
své	svůj	k3xOyFgMnPc4	svůj
diváky	divák	k1gMnPc4	divák
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejúspěšnějších	úspěšný	k2eAgInPc2d3	nejúspěšnější
seriálů	seriál	k1gInPc2	seriál
stanice	stanice	k1gFnSc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
Sophia	Sophia	k1gFnSc1	Sophia
režírovala	režírovat	k5eAaImAgFnS	režírovat
tři	tři	k4xCgFnPc4	tři
epizody	epizoda	k1gFnPc4	epizoda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2006	[number]	k4	2006
zářila	zářit	k5eAaImAgFnS	zářit
ve	v	k7c6	v
filmu	film	k1gInSc6	film
John	John	k1gMnSc1	John
Tucker	Tucker	k1gMnSc1	Tucker
musí	muset	k5eAaImIp3nS	muset
zemřít	zemřít	k5eAaPmF	zemřít
<g/>
,	,	kIx,	,
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Brittany	Brittan	k1gInPc4	Brittan
Snow	Snow	k1gMnSc4	Snow
a	a	k8xC	a
Jesseho	Jesse	k1gMnSc4	Jesse
Metcalfeho	Metcalfe	k1gMnSc4	Metcalfe
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
vydělal	vydělat	k5eAaPmAgInS	vydělat
přes	přes	k7c4	přes
60	[number]	k4	60
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
samý	samý	k3xTgInSc1	samý
rok	rok	k1gInSc1	rok
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
v	v	k7c6	v
thrillerovém	thrillerový	k2eAgInSc6d1	thrillerový
filmu	film	k1gInSc6	film
Stay	Staa	k1gFnSc2	Staa
Alive	Aliev	k1gFnSc2	Aliev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
získala	získat	k5eAaPmAgFnS	získat
roli	role	k1gFnSc4	role
Grace	Grace	k1gFnSc1	Grace
Andrews	Andrewsa	k1gFnPc2	Andrewsa
v	v	k7c6	v
remaku	remak	k1gInSc6	remak
klasického	klasický	k2eAgInSc2d1	klasický
hororového	hororový	k2eAgInSc2d1	hororový
filmu	film	k1gInSc2	film
Stopař	stopař	k1gMnSc1	stopař
<g/>
,	,	kIx,	,
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Seana	Sean	k1gMnSc2	Sean
Beana	Bean	k1gMnSc2	Bean
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
Narrows	Narrowsa	k1gFnPc2	Narrowsa
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
měl	mít	k5eAaImAgMnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
na	na	k7c6	na
Filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
si	se	k3xPyFc3	se
zahrála	zahrát	k5eAaPmAgFnS	zahrát
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Holka	holka	k1gFnSc1	holka
s	s	k7c7	s
prknem	prkno	k1gNnSc7	prkno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2012	[number]	k4	2012
se	se	k3xPyFc4	se
připojila	připojit	k5eAaPmAgFnS	připojit
k	k	k7c3	k
obsazení	obsazení	k1gNnSc3	obsazení
komediálního	komediální	k2eAgInSc2d1	komediální
seriálu	seriál	k1gInSc2	seriál
stanice	stanice	k1gFnSc2	stanice
CBS	CBS	kA	CBS
Parťáci	parťák	k1gMnPc1	parťák
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třinácti	třináct	k4xCc2	třináct
epizodách	epizoda	k1gFnPc6	epizoda
byl	být	k5eAaImAgInS	být
seriál	seriál	k1gInSc1	seriál
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2013	[number]	k4	2013
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
získala	získat	k5eAaPmAgFnS	získat
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
stanice	stanice	k1gFnSc2	stanice
NBC	NBC	kA	NBC
Chicago	Chicago	k1gNnSc1	Chicago
P.	P.	kA	P.
<g/>
D.	D.	kA	D.
<g/>
.	.	kIx.	.
</s>
<s>
Seriál	seriál	k1gInSc1	seriál
měl	mít	k5eAaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
druhou	druhý	k4xOgFnSc4	druhý
sérii	série	k1gFnSc4	série
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
byla	být	k5eAaImAgFnS	být
zasnoubená	zasnoubená	k1gFnSc1	zasnoubená
s	s	k7c7	s
hercem	herec	k1gMnSc7	herec
Chadem	Chad	k1gMnSc7	Chad
Michaelem	Michael	k1gMnSc7	Michael
Murrayem	Murray	k1gMnSc7	Murray
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
si	se	k3xPyFc3	se
vzala	vzít	k5eAaPmAgFnS	vzít
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2005	[number]	k4	2005
v	v	k7c6	v
Santa	Sant	k1gMnSc2	Sant
Monice	Monika	k1gFnSc6	Monika
<g/>
.	.	kIx.	.
</s>
<s>
Pár	pár	k1gInSc1	pár
ohlásil	ohlásit	k5eAaPmAgInS	ohlásit
svůj	svůj	k3xOyFgInSc4	svůj
rozchod	rozchod	k1gInSc4	rozchod
po	po	k7c6	po
pěti	pět	k4xCc6	pět
měsících	měsíc	k1gInPc6	měsíc
manželství	manželství	k1gNnSc2	manželství
a	a	k8xC	a
rozvedli	rozvést	k5eAaPmAgMnP	rozvést
se	se	k3xPyFc4	se
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
chodila	chodit	k5eAaImAgFnS	chodit
s	s	k7c7	s
dalším	další	k2eAgMnSc7d1	další
hercem	herec	k1gMnSc7	herec
ze	z	k7c2	z
seriálu	seriál	k1gInSc2	seriál
One	One	k1gMnSc1	One
Tree	Tre	k1gFnSc2	Tre
Hill	Hill	k1gMnSc1	Hill
Jamesem	James	k1gInSc7	James
Laffertym	Laffertym	k1gInSc1	Laffertym
<g/>
.	.	kIx.	.
</s>
<s>
Vztah	vztah	k1gInSc1	vztah
s	s	k7c7	s
Austinem	Austin	k1gMnSc7	Austin
Nicholsem	Nichols	k1gMnSc7	Nichols
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
herečka	herečka	k1gFnSc1	herečka
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2012	[number]	k4	2012
se	se	k3xPyFc4	se
pár	pár	k4xCyI	pár
rozešel	rozejít	k5eAaPmAgMnS	rozejít
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
ledna	leden	k1gInSc2	leden
2013	[number]	k4	2013
chodila	chodit	k5eAaImAgFnS	chodit
s	s	k7c7	s
programovým	programový	k2eAgInSc7d1	programový
manažerem	manažer	k1gInSc7	manažer
Googlu	Googl	k1gInSc2	Googl
Danem	Dan	k1gMnSc7	Dan
Fredinburgem	Fredinburg	k1gMnSc7	Fredinburg
<g/>
.	.	kIx.	.
</s>
<s>
Dvojice	dvojice	k1gFnSc1	dvojice
se	se	k3xPyFc4	se
rozešla	rozejít	k5eAaPmAgFnS	rozejít
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Sophia	Sophium	k1gNnSc2	Sophium
Bush	Bush	k1gMnSc1	Bush
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
