<p>
<s>
Federace	federace	k1gFnSc1	federace
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
fœ	fœ	k?	fœ
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
typ	typ	k1gInSc1	typ
státního	státní	k2eAgNnSc2d1	státní
zřízení	zřízení	k1gNnSc2	zřízení
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
složený	složený	k2eAgInSc4d1	složený
stát	stát	k1gInSc4	stát
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
skládající	skládající	k2eAgNnSc1d1	skládající
se	se	k3xPyFc4	se
ze	z	k7c2	z
států	stát	k1gInPc2	stát
-	-	kIx~	-
členů	člen	k1gInPc2	člen
federace	federace	k1gFnSc2	federace
s	s	k7c7	s
různým	různý	k2eAgNnSc7d1	různé
označením	označení	k1gNnSc7	označení
(	(	kIx(	(
<g/>
státy	stát	k1gInPc4	stát
<g/>
,	,	kIx,	,
země	zem	k1gFnPc4	zem
<g/>
,	,	kIx,	,
republiky	republika	k1gFnPc4	republika
<g/>
,	,	kIx,	,
kantony	kanton	k1gInPc4	kanton
<g/>
,	,	kIx,	,
provincie	provincie	k1gFnPc4	provincie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
federacích	federace	k1gFnPc6	federace
je	být	k5eAaImIp3nS	být
svéprávné	svéprávný	k2eAgNnSc4d1	svéprávné
postavení	postavení	k1gNnSc4	postavení
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
zajištěno	zajistit	k5eAaPmNgNnS	zajistit
ústavou	ústava	k1gFnSc7	ústava
nebo	nebo	k8xC	nebo
smluvně	smluvně	k6eAd1	smluvně
a	a	k8xC	a
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
měněno	měnit	k5eAaImNgNnS	měnit
jednostranným	jednostranný	k2eAgNnSc7d1	jednostranné
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
centrální	centrální	k2eAgFnSc2d1	centrální
vlády	vláda	k1gFnSc2	vláda
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
,	,	kIx,	,
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
i	i	k8xC	i
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgFnSc1d1	státní
moc	moc	k1gFnSc1	moc
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
federaci	federace	k1gFnSc6	federace
rozložena	rozložit	k5eAaPmNgFnS	rozložit
mezi	mezi	k7c4	mezi
federální	federální	k2eAgInPc4d1	federální
orgány	orgán	k1gInPc4	orgán
a	a	k8xC	a
orgány	orgán	k1gInPc4	orgán
členských	členský	k2eAgInPc2d1	členský
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Právním	právní	k2eAgInSc7d1	právní
základem	základ	k1gInSc7	základ
federace	federace	k1gFnSc2	federace
je	být	k5eAaImIp3nS	být
ústava	ústava	k1gFnSc1	ústava
(	(	kIx(	(
<g/>
u	u	k7c2	u
konfederace	konfederace	k1gFnSc2	konfederace
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
smlouva	smlouva	k1gFnSc1	smlouva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
dvojí	dvojí	k4xRgNnSc4	dvojí
občanství	občanství	k1gNnSc4	občanství
(	(	kIx(	(
<g/>
subjektu	subjekt	k1gInSc2	subjekt
federace	federace	k1gFnSc2	federace
a	a	k8xC	a
federace	federace	k1gFnSc2	federace
jako	jako	k8xS	jako
celku	celek	k1gInSc2	celek
<g/>
)	)	kIx)	)
a	a	k8xC	a
právní	právní	k2eAgInPc4d1	právní
akty	akt	k1gInPc4	akt
federace	federace	k1gFnSc2	federace
jsou	být	k5eAaImIp3nP	být
přímo	přímo	k6eAd1	přímo
závazné	závazný	k2eAgFnPc1d1	závazná
pro	pro	k7c4	pro
osoby	osoba	k1gFnPc4	osoba
(	(	kIx(	(
<g/>
u	u	k7c2	u
konfederace	konfederace	k1gFnSc2	konfederace
jen	jen	k9	jen
pro	pro	k7c4	pro
členské	členský	k2eAgInPc4d1	členský
státy	stát	k1gInPc4	stát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k8xS	jako
federace	federace	k1gFnSc1	federace
se	se	k3xPyFc4	se
také	také	k9	také
označují	označovat	k5eAaImIp3nP	označovat
sdružení	sdružení	k1gNnSc4	sdružení
jiných	jiný	k2eAgInPc2d1	jiný
typů	typ	k1gInPc2	typ
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
zastřešující	zastřešující	k2eAgFnPc1d1	zastřešující
sportovní	sportovní	k2eAgFnPc1d1	sportovní
a	a	k8xC	a
podobné	podobný	k2eAgFnPc1d1	podobná
organizace	organizace	k1gFnPc1	organizace
(	(	kIx(	(
<g/>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
federace	federace	k1gFnSc1	federace
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
<g/>
,	,	kIx,	,
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
šachová	šachový	k2eAgFnSc1d1	šachová
federace	federace	k1gFnSc1	federace
<g/>
,	,	kIx,	,
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
tenisová	tenisový	k2eAgFnSc1d1	tenisová
federace	federace	k1gFnSc1	federace
<g/>
,	,	kIx,	,
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
federace	federace	k1gFnSc1	federace
stolního	stolní	k2eAgInSc2d1	stolní
tenisu	tenis	k1gInSc2	tenis
<g/>
,	,	kIx,	,
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
florbalová	florbalový	k2eAgFnSc1d1	florbalová
federace	federace	k1gFnSc1	federace
<g/>
,	,	kIx,	,
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
federace	federace	k1gFnSc1	federace
badmintonu	badminton	k1gInSc2	badminton
<g/>
,	,	kIx,	,
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
<g />
.	.	kIx.	.
</s>
<s>
lyžařská	lyžařský	k2eAgFnSc1d1	lyžařská
federace	federace	k1gFnSc1	federace
<g/>
,	,	kIx,	,
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
šermířská	šermířský	k2eAgFnSc1d1	šermířská
federace	federace	k1gFnSc1	federace
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
federace	federace	k1gFnSc1	federace
aikido	aikida	k1gFnSc5	aikida
<g/>
,	,	kIx,	,
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
automobilová	automobilový	k2eAgFnSc1d1	automobilová
federace	federace	k1gFnSc1	federace
<g/>
,	,	kIx,	,
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
federace	federace	k1gFnSc1	federace
motocyklistů	motocyklista	k1gMnPc2	motocyklista
<g/>
,	,	kIx,	,
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
kynologická	kynologický	k2eAgFnSc1d1	kynologická
federace	federace	k1gFnSc1	federace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politická	politický	k2eAgNnPc1d1	politické
uskupení	uskupení	k1gNnPc1	uskupení
(	(	kIx(	(
<g/>
Československá	československý	k2eAgFnSc1d1	Československá
anarchistická	anarchistický	k2eAgFnSc1d1	anarchistická
federace	federace	k1gFnSc1	federace
<g/>
,	,	kIx,	,
Světová	světový	k2eAgFnSc1d1	světová
federace	federace	k1gFnSc1	federace
demokratické	demokratický	k2eAgFnSc2d1	demokratická
mládeže	mládež	k1gFnSc2	mládež
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
odborové	odborový	k2eAgFnSc2d1	odborová
organizace	organizace	k1gFnSc2	organizace
(	(	kIx(	(
<g/>
Federace	federace	k1gFnSc1	federace
strojvůdců	strojvůdce	k1gMnPc2	strojvůdce
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
náboženské	náboženský	k2eAgFnPc1d1	náboženská
organizace	organizace	k1gFnPc1	organizace
(	(	kIx(	(
<g/>
Světová	světový	k2eAgFnSc1d1	světová
luterská	luterský	k2eAgFnSc1d1	luterská
federace	federace	k1gFnSc1	federace
<g/>
,	,	kIx,	,
Evropská	evropský	k2eAgFnSc1d1	Evropská
baptistická	baptistický	k2eAgFnSc1d1	baptistická
federace	federace	k1gFnSc1	federace
<g/>
,	,	kIx,	,
Federace	federace	k1gFnSc1	federace
židovských	židovský	k2eAgFnPc2d1	židovská
obcí	obec	k1gFnPc2	obec
<g/>
,	,	kIx,	,
Pohanská	pohanský	k2eAgFnSc1d1	pohanská
federace	federace	k1gFnSc1	federace
<g/>
)	)	kIx)	)
a	a	k8xC	a
mnohé	mnohý	k2eAgFnPc4d1	mnohá
další	další	k2eAgNnPc4d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
Federace	federace	k1gFnSc1	federace
má	mít	k5eAaImIp3nS	mít
vždy	vždy	k6eAd1	vždy
plnou	plný	k2eAgFnSc4d1	plná
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
subjektivitu	subjektivita	k1gFnSc4	subjektivita
a	a	k8xC	a
dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
federaci	federace	k1gFnSc4	federace
vnitrostátní	vnitrostátní	k2eAgFnSc4d1	vnitrostátní
–	–	k?	–
členské	členský	k2eAgInPc1d1	členský
státy	stát	k1gInPc1	stát
nemají	mít	k5eNaImIp3nP	mít
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
subjektivitu	subjektivita	k1gFnSc4	subjektivita
–	–	k?	–
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc1	Československo
1969	[number]	k4	1969
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
federaci	federace	k1gFnSc4	federace
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
–	–	k?	–
členské	členský	k2eAgInPc1d1	členský
státy	stát	k1gInPc1	stát
mají	mít	k5eAaImIp3nP	mít
omezenou	omezený	k2eAgFnSc4d1	omezená
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
subjektivitu	subjektivita	k1gFnSc4	subjektivita
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
uzavírat	uzavírat	k5eAaPmF	uzavírat
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
smlouvy	smlouva	k1gFnSc2	smlouva
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
stanoveném	stanovený	k2eAgInSc6d1	stanovený
federální	federální	k2eAgMnSc1d1	federální
ústavou	ústava	k1gFnSc7	ústava
–	–	k?	–
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc1	Československo
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Členské	členský	k2eAgInPc1d1	členský
státy	stát	k1gInPc1	stát
zpravidla	zpravidla	k6eAd1	zpravidla
nemají	mít	k5eNaImIp3nP	mít
právo	právo	k1gNnSc4	právo
z	z	k7c2	z
federace	federace	k1gFnSc2	federace
vystoupit	vystoupit	k5eAaPmF	vystoupit
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
federace	federace	k1gFnPc1	federace
však	však	k9	však
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
vystoupení	vystoupení	k1gNnSc4	vystoupení
–	–	k?	–
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
<g/>
,	,	kIx,	,
Československo	Československo	k1gNnSc1	Československo
1991	[number]	k4	1991
<g/>
–	–	k?	–
<g/>
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
sporu	spor	k1gInSc2	spor
o	o	k7c4	o
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
výstup	výstup	k1gInSc4	výstup
je	být	k5eAaImIp3nS	být
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
USA	USA	kA	USA
1861	[number]	k4	1861
<g/>
–	–	k?	–
<g/>
1865	[number]	k4	1865
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jižní	jižní	k2eAgInPc1d1	jižní
státy	stát	k1gInPc1	stát
vystoupily	vystoupit	k5eAaPmAgInP	vystoupit
a	a	k8xC	a
založily	založit	k5eAaPmAgInP	založit
konfederaci	konfederace	k1gFnSc4	konfederace
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
severní	severní	k2eAgMnSc1d1	severní
to	ten	k3xDgNnSc1	ten
považovaly	považovat	k5eAaImAgInP	považovat
za	za	k7c4	za
protiústavní	protiústavní	k2eAgNnSc4d1	protiústavní
povstání	povstání	k1gNnSc4	povstání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Federace	federace	k1gFnSc1	federace
bývá	bývat	k5eAaImIp3nS	bývat
založena	založit	k5eAaPmNgFnS	založit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
buď	buď	k8xC	buď
na	na	k7c6	na
základě	základ	k1gInSc6	základ
etnického	etnický	k2eAgNnSc2d1	etnické
hlediska	hledisko	k1gNnSc2	hledisko
(	(	kIx(	(
<g/>
společný	společný	k2eAgInSc1d1	společný
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
kultura	kultura	k1gFnSc1	kultura
<g/>
,	,	kIx,	,
náboženství	náboženství	k1gNnSc1	náboženství
<g/>
,	,	kIx,	,
ideologie	ideologie	k1gFnSc1	ideologie
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
nebo	nebo	k8xC	nebo
na	na	k7c6	na
jiném	jiný	k2eAgInSc6d1	jiný
principu	princip	k1gInSc6	princip
(	(	kIx(	(
<g/>
územním	územní	k2eAgNnSc6d1	územní
<g/>
,	,	kIx,	,
hospodářském	hospodářský	k2eAgNnSc6d1	hospodářské
<g/>
,	,	kIx,	,
obranném	obranný	k2eAgNnSc6d1	obranné
<g/>
)	)	kIx)	)
<g/>
Takový	takový	k3xDgInSc4	takový
společný	společný	k2eAgInSc4d1	společný
účel	účel	k1gInSc4	účel
<g/>
,	,	kIx,	,
společná	společný	k2eAgFnSc1d1	společná
identifikace	identifikace	k1gFnSc1	identifikace
mohou	moct	k5eAaImIp3nP	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
dobrovolně	dobrovolně	k6eAd1	dobrovolně
<g/>
,	,	kIx,	,
politickým	politický	k2eAgInSc7d1	politický
konsenzem	konsenz	k1gInSc7	konsenz
(	(	kIx(	(
<g/>
dobrovolný	dobrovolný	k2eAgInSc4d1	dobrovolný
svazek	svazek	k1gInSc4	svazek
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
národ	národ	k1gInSc1	národ
z	z	k7c2	z
vůle	vůle	k1gFnSc2	vůle
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
nebo	nebo	k8xC	nebo
byly	být	k5eAaImAgFnP	být
vynuceny	vynutit	k5eAaPmNgFnP	vynutit
politickou	politický	k2eAgFnSc7d1	politická
a	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
nebo	nebo	k8xC	nebo
vojenskou	vojenský	k2eAgFnSc7d1	vojenská
silou	síla	k1gFnSc7	síla
zakladatele	zakladatel	k1gMnSc2	zakladatel
<g/>
,	,	kIx,	,
zakladatelů	zakladatel	k1gMnPc2	zakladatel
(	(	kIx(	(
<g/>
doprovázené	doprovázený	k2eAgNnSc1d1	doprovázené
vnucením	vnucení	k1gNnSc7	vnucení
jednoho	jeden	k4xCgMnSc4	jeden
nebo	nebo	k8xC	nebo
tzv.	tzv.	kA	tzv.
nadřazeného	nadřazený	k2eAgInSc2d1	nadřazený
jazyka	jazyk	k1gInSc2	jazyk
nebo	nebo	k8xC	nebo
ideologie	ideologie	k1gFnSc2	ideologie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Federace	federace	k1gFnPc1	federace
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
smluvně	smluvně	k6eAd1	smluvně
potvrzeny	potvrdit	k5eAaPmNgInP	potvrdit
jejími	její	k3xOp3gInPc7	její
členy	člen	k1gInPc7	člen
(	(	kIx(	(
<g/>
signatářskými	signatářský	k2eAgInPc7d1	signatářský
státy	stát	k1gInPc7	stát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přičemž	přičemž	k6eAd1	přičemž
jednotliví	jednotlivý	k2eAgMnPc1d1	jednotlivý
členové	člen	k1gMnPc1	člen
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
buď	buď	k8xC	buď
nemají	mít	k5eNaImIp3nP	mít
právo	právo	k1gNnSc4	právo
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
(	(	kIx(	(
<g/>
jednostranně	jednostranně	k6eAd1	jednostranně
<g/>
)	)	kIx)	)
vystoupit	vystoupit	k5eAaPmF	vystoupit
(	(	kIx(	(
<g/>
ať	ať	k9	ať
de	de	k?	de
iure	iure	k1gInSc1	iure
nebo	nebo	k8xC	nebo
de	de	k?	de
facto	facto	k1gNnSc1	facto
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
nebo	nebo	k8xC	nebo
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
<g/>
,	,	kIx,	,
jim	on	k3xPp3gFnPc3	on
je	být	k5eAaImIp3nS	být
takové	takový	k3xDgNnSc4	takový
právo	právo	k1gNnSc4	právo
federální	federální	k2eAgFnSc7d1	federální
ústavou	ústava	k1gFnSc7	ústava
nebo	nebo	k8xC	nebo
smluvně	smluvně	k6eAd1	smluvně
zaručeno	zaručit	k5eAaPmNgNnS	zaručit
<g/>
.	.	kIx.	.
<g/>
Quasi	quasi	k6eAd1	quasi
federace	federace	k1gFnSc1	federace
(	(	kIx(	(
<g/>
též	též	k9	též
kvazifederace	kvazifederace	k1gFnSc1	kvazifederace
<g/>
)	)	kIx)	)
–	–	k?	–
definice	definice	k1gFnSc1	definice
federace	federace	k1gFnSc1	federace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
ústav	ústava	k1gFnPc2	ústava
daných	daný	k2eAgInPc2d1	daný
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
samy	sám	k3xTgFnPc1	sám
jako	jako	k8xC	jako
federace	federace	k1gFnSc1	federace
definují	definovat	k5eAaBmIp3nP	definovat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
SRN	SRN	kA	SRN
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nominální	nominální	k2eAgFnSc2d1	nominální
federace	federace	k1gFnSc2	federace
–	–	k?	–
federativní	federativní	k2eAgNnSc1d1	federativní
zřízení	zřízení	k1gNnSc4	zřízení
zakotveno	zakotven	k2eAgNnSc4d1	zakotveno
v	v	k7c6	v
ústavě	ústava	k1gFnSc6	ústava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
prakticky	prakticky	k6eAd1	prakticky
se	se	k3xPyFc4	se
tak	tak	k9	tak
vůbec	vůbec	k9	vůbec
neprojevuje	projevovat	k5eNaImIp3nS	projevovat
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ČSSR	ČSSR	kA	ČSSR
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Symetrická	symetrický	k2eAgFnSc1d1	symetrická
federace	federace	k1gFnSc1	federace
–	–	k?	–
členské	členský	k2eAgFnPc4d1	členská
jednotky	jednotka	k1gFnPc4	jednotka
mají	mít	k5eAaImIp3nP	mít
stejné	stejný	k2eAgNnSc4d1	stejné
postavení	postavení	k1gNnSc4	postavení
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Asymetrická	asymetrický	k2eAgFnSc1d1	asymetrická
federace	federace	k1gFnSc1	federace
–	–	k?	–
členské	členský	k2eAgFnPc4d1	členská
jednotky	jednotka	k1gFnPc4	jednotka
nemají	mít	k5eNaImIp3nP	mít
stejné	stejný	k2eAgNnSc4d1	stejné
postavení	postavení	k1gNnSc4	postavení
(	(	kIx(	(
<g/>
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Federalismus	federalismus	k1gInSc4	federalismus
==	==	k?	==
</s>
</p>
<p>
<s>
Pojem	pojem	k1gInSc1	pojem
federalismus	federalismus	k1gInSc1	federalismus
označuje	označovat	k5eAaImIp3nS	označovat
politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
federací	federace	k1gFnPc2	federace
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc1	jejich
formu	forma	k1gFnSc4	forma
vlády	vláda	k1gFnSc2	vláda
nebo	nebo	k8xC	nebo
správy	správa	k1gFnSc2	správa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
politologii	politologie	k1gFnSc6	politologie
tento	tento	k3xDgInSc4	tento
pojem	pojem	k1gInSc4	pojem
popisuje	popisovat	k5eAaImIp3nS	popisovat
společensko-politické	společenskoolitický	k2eAgNnSc1d1	společensko-politické
uspořádání	uspořádání	k1gNnSc1	uspořádání
(	(	kIx(	(
<g/>
politický	politický	k2eAgInSc1d1	politický
systém	systém	k1gInSc1	systém
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc2	jeho
zkoumání	zkoumání	k1gNnSc2	zkoumání
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
empirické	empirický	k2eAgInPc1d1	empirický
a	a	k8xC	a
zahrnující	zahrnující	k2eAgInPc1d1	zahrnující
i	i	k8xC	i
další	další	k2eAgInPc1d1	další
důležité	důležitý	k2eAgInPc1d1	důležitý
faktory	faktor	k1gInPc1	faktor
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
ekonomii	ekonomie	k1gFnSc4	ekonomie
nebo	nebo	k8xC	nebo
spokojenost	spokojenost	k1gFnSc4	spokojenost
individuí	individuum	k1gNnPc2	individuum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Také	také	k9	také
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
označována	označován	k2eAgFnSc1d1	označována
ideologie	ideologie	k1gFnSc1	ideologie
jeho	jeho	k3xOp3gMnPc2	jeho
stoupenců	stoupenec	k1gMnPc2	stoupenec
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
jejich	jejich	k3xOp3gFnPc4	jejich
skutečné	skutečný	k2eAgFnPc4d1	skutečná
znalosti	znalost	k1gFnPc4	znalost
nebo	nebo	k8xC	nebo
záměry	záměr	k1gInPc4	záměr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
federací	federace	k1gFnPc2	federace
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Citáty	citát	k1gInPc1	citát
==	==	k?	==
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgNnPc1	dva
slova	slovo	k1gNnPc1	slovo
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nejde	jít	k5eNaImIp3nS	jít
přeložit	přeložit	k5eAaPmF	přeložit
<g/>
.	.	kIx.	.
</s>
<s>
Federalismus	federalismus	k1gInSc1	federalismus
a	a	k8xC	a
liberalismus	liberalismus	k1gInSc1	liberalismus
<g/>
.	.	kIx.	.
...	...	k?	...
Filosofie	filosofie	k1gFnSc1	filosofie
federalismu	federalismus	k1gInSc2	federalismus
znamená	znamenat	k5eAaImIp3nS	znamenat
respekt	respekt	k1gInSc4	respekt
vůči	vůči	k7c3	vůči
rozdílnosti	rozdílnost	k1gFnSc3	rozdílnost
<g/>
.	.	kIx.	.
...	...	k?	...
Ale	ale	k9	ale
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
federalista	federalista	k1gMnSc1	federalista
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
politika	politika	k1gFnSc1	politika
se	se	k3xPyFc4	se
rozdíly	rozdíl	k1gInPc1	rozdíl
snaží	snažit	k5eAaImIp3nP	snažit
odstraňovat	odstraňovat	k5eAaImF	odstraňovat
<g/>
.	.	kIx.	.
...	...	k?	...
V	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
federalista	federalista	k1gMnSc1	federalista
žádné	žádný	k3yNgInPc4	žádný
rozdíly	rozdíl	k1gInPc4	rozdíl
nechce	chtít	k5eNaImIp3nS	chtít
<g/>
.	.	kIx.	.
</s>
<s>
Quebečané	Quebečan	k1gMnPc1	Quebečan
se	se	k3xPyFc4	se
definují	definovat	k5eAaBmIp3nP	definovat
jako	jako	k9	jako
antifederalisté	antifederalista	k1gMnPc1	antifederalista
<g/>
.	.	kIx.	.
</s>
<s>
Být	být	k5eAaImF	být
federalista	federalista	k1gMnSc1	federalista
pro	pro	k7c4	pro
mne	já	k3xPp1nSc4	já
znamená	znamenat	k5eAaImIp3nS	znamenat
respekt	respekt	k1gInSc1	respekt
k	k	k7c3	k
odlišnosti	odlišnost	k1gFnSc3	odlišnost
při	při	k7c6	při
spolupráci	spolupráce	k1gFnSc6	spolupráce
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
–	–	k?	–
<g/>
François	François	k1gFnSc2	François
Bayrou	Bayra	k1gMnSc7	Bayra
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
květen	květen	k1gInSc1	květen
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
==	==	k?	==
Fiktivní	fiktivní	k2eAgFnSc2d1	fiktivní
federace	federace	k1gFnSc2	federace
==	==	k?	==
</s>
</p>
<p>
<s>
Spojená	spojený	k2eAgFnSc1d1	spojená
federace	federace	k1gFnSc1	federace
planet	planeta	k1gFnPc2	planeta
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
Star	star	k1gFnSc2	star
Treku	Trek	k1gInSc2	Trek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Galaktická	galaktický	k2eAgFnSc1d1	Galaktická
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
spolková	spolkový	k2eAgFnSc1d1	spolková
<g/>
/	/	kIx~	/
<g/>
federativní	federativní	k2eAgFnSc1d1	federativní
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
a	a	k8xC	a
Konfederace	konfederace	k1gFnSc1	konfederace
nezávislých	závislý	k2eNgInPc2d1	nezávislý
systémů	systém	k1gInPc2	systém
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
Star	Star	kA	Star
Wars	Wars	k1gInSc1	Wars
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dvanáct	dvanáct	k4xCc1	dvanáct
kolonií	kolonie	k1gFnPc2	kolonie
Kobolu	Kobol	k1gInSc2	Kobol
<g/>
,	,	kIx,	,
federace	federace	k1gFnPc1	federace
dvanácti	dvanáct	k4xCc2	dvanáct
planet	planeta	k1gFnPc2	planeta
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
Battlestar	Battlestar	k1gMnSc1	Battlestar
Galacticy	Galactica	k1gFnSc2	Galactica
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
BECKER	BECKER	kA	BECKER
<g/>
,	,	kIx,	,
Winfried	Winfried	k1gInSc1	Winfried
<g/>
.	.	kIx.	.
</s>
<s>
Die	Die	k?	Die
Nouvelles	Nouvelles	k1gMnSc1	Nouvelles
Equipes	Equipes	k1gMnSc1	Equipes
Internationales	Internationales	k1gMnSc1	Internationales
<g/>
:	:	kIx,	:
Internationaler	Internationaler	k1gInSc1	Internationaler
Foderalismus	Foderalismus	k1gInSc1	Foderalismus
<g/>
.	.	kIx.	.
</s>
<s>
Prague	Prague	k1gFnSc1	Prague
Papers	Papersa	k1gFnPc2	Papersa
on	on	k3xPp3gMnSc1	on
the	the	k?	the
History	Histor	k1gInPc4	Histor
of	of	k?	of
International	International	k1gFnSc2	International
Relations	Relationsa	k1gFnPc2	Relationsa
<g/>
.	.	kIx.	.
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
,	,	kIx,	,
s.	s.	k?	s.
369	[number]	k4	369
<g/>
-	-	kIx~	-
<g/>
392	[number]	k4	392
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
[	[	kIx(	[
<g/>
PDF	PDF	kA	PDF
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7308	[number]	k4	7308
<g/>
-	-	kIx~	-
<g/>
208	[number]	k4	208
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Autonomní	autonomní	k2eAgNnSc1d1	autonomní
území	území	k1gNnSc1	území
</s>
</p>
<p>
<s>
Unitární	unitární	k2eAgInSc1d1	unitární
stát	stát	k1gInSc1	stát
</s>
</p>
<p>
<s>
Foederati	Foederat	k1gMnPc1	Foederat
</s>
</p>
<p>
<s>
Konfederace	konfederace	k1gFnSc1	konfederace
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
federace	federace	k1gFnSc2	federace
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
federace	federace	k1gFnSc2	federace
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
federalism	federalism	k1gMnSc1	federalism
<g/>
,	,	kIx,	,
Stanford	Stanford	k1gMnSc1	Stanford
Encyclopedia	Encyclopedium	k1gNnSc2	Encyclopedium
of	of	k?	of
Philosophy	Philosopha	k1gFnSc2	Philosopha
<g/>
,	,	kIx,	,
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Stanfordovy	Stanfordův	k2eAgFnSc2d1	Stanfordova
univerzity	univerzita	k1gFnSc2	univerzita
plato	plato	k1gNnSc4	plato
<g/>
.	.	kIx.	.
<g/>
stanford	stanford	k1gInSc1	stanford
<g/>
.	.	kIx.	.
<g/>
edu	edu	k?	edu
</s>
</p>
