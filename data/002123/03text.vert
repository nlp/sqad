<s>
Alžír	Alžír	k1gInSc1	Alžír
(	(	kIx(	(
<g/>
arabsky	arabsky	k6eAd1	arabsky
ا	ا	k?	ا
(	(	kIx(	(
<g/>
Al-Džazaír	Al-Džazaír	k1gMnSc1	Al-Džazaír
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
francouzsky	francouzsky	k6eAd1	francouzsky
Alger	Alger	k1gInSc1	Alger
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Alžírska	Alžírsko	k1gNnSc2	Alžírsko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
významný	významný	k2eAgInSc1d1	významný
přístav	přístav	k1gInSc1	přístav
s	s	k7c7	s
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2	[number]	k4	2
miliony	milion	k4xCgInPc1	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
aglomerace	aglomerace	k1gFnSc2	aglomerace
cca	cca	kA	cca
3,5	[number]	k4	3,5
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
421	[number]	k4	421
km	km	kA	km
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
město	město	k1gNnSc1	město
Constantine	Constantin	k1gInSc5	Constantin
<g/>
,	,	kIx,	,
434	[number]	k4	434
km	km	kA	km
Oran	Orana	k1gFnPc2	Orana
a	a	k8xC	a
1	[number]	k4	1
975	[number]	k4	975
km	km	kA	km
Tamanrasset	Tamanrasseta	k1gFnPc2	Tamanrasseta
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
též	též	k9	též
přezdíváno	přezdíván	k2eAgNnSc1d1	přezdíváno
jako	jako	k8xC	jako
Bílý	bílý	k2eAgInSc1d1	bílý
Alžír	Alžír	k1gInSc1	Alžír
(	(	kIx(	(
<g/>
Alger	Alger	k1gInSc1	Alger
la	la	k1gNnSc1	la
Blanche	Blanche	k1gInSc1	Blanche
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
tento	tento	k3xDgInSc4	tento
název	název	k1gInSc4	název
získalo	získat	k5eAaPmAgNnS	získat
díky	díky	k7c3	díky
bílým	bílý	k2eAgFnPc3d1	bílá
zdem	zeď	k1gFnPc3	zeď
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
z	z	k7c2	z
kopců	kopec	k1gInPc2	kopec
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
města	město	k1gNnSc2	město
svažují	svažovat	k5eAaImIp3nP	svažovat
až	až	k9	až
ke	k	k7c3	k
břehu	břeh	k1gInSc3	břeh
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Založen	založen	k2eAgMnSc1d1	založen
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
944	[number]	k4	944
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
starší	starý	k2eAgNnSc4d2	starší
osídlení	osídlení	k1gNnSc4	osídlení
fénického	fénický	k2eAgInSc2d1	fénický
a	a	k8xC	a
řeckého	řecký	k2eAgInSc2d1	řecký
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
tu	tu	k6eAd1	tu
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
hrad	hrad	k1gInSc1	hrad
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
též	též	k9	též
sídlo	sídlo	k1gNnSc1	sídlo
pirátů	pirát	k1gMnPc2	pirát
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1830	[number]	k4	1830
jej	on	k3xPp3gInSc4	on
obsadili	obsadit	k5eAaPmAgMnP	obsadit
Francouzi	Francouz	k1gMnPc1	Francouz
a	a	k8xC	a
přebudovali	přebudovat	k5eAaPmAgMnP	přebudovat
ve	v	k7c6	v
francouzském	francouzský	k2eAgInSc6d1	francouzský
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1943	[number]	k4	1943
a	a	k8xC	a
1944	[number]	k4	1944
sídlila	sídlit	k5eAaImAgFnS	sídlit
v	v	k7c6	v
Alžíru	Alžír	k1gInSc6	Alžír
též	též	k9	též
prozatímní	prozatímní	k2eAgFnSc1d1	prozatímní
francouzská	francouzský	k2eAgFnSc1d1	francouzská
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
ozývaly	ozývat	k5eAaImAgInP	ozývat
hlasy	hlas	k1gInPc1	hlas
<g/>
,	,	kIx,	,
požadující	požadující	k2eAgFnSc4d1	požadující
nezávislost	nezávislost	k1gFnSc4	nezávislost
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
postupně	postupně	k6eAd1	postupně
přerostly	přerůst	k5eAaPmAgFnP	přerůst
až	až	k9	až
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
násilnosti	násilnost	k1gFnSc6	násilnost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
je	být	k5eAaImIp3nS	být
Alžír	Alžír	k1gInSc1	Alžír
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Alžírska	Alžírsko	k1gNnSc2	Alžírsko
<g/>
;	;	kIx,	;
prudce	prudko	k6eAd1	prudko
se	se	k3xPyFc4	se
rozvíjel	rozvíjet	k5eAaImAgInS	rozvíjet
-	-	kIx~	-
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
různé	různý	k2eAgInPc1d1	různý
závody	závod	k1gInPc1	závod
<g/>
,	,	kIx,	,
postavena	postaven	k2eAgFnSc1d1	postavena
byla	být	k5eAaImAgNnP	být
nová	nový	k2eAgNnPc1d1	nové
panelová	panelový	k2eAgNnPc1d1	panelové
sídliště	sídliště	k1gNnPc1	sídliště
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
ve	v	k7c6	v
městě	město	k1gNnSc6	město
i	i	k8xC	i
jeho	jeho	k3xOp3gNnSc6	jeho
okolí	okolí	k1gNnSc6	okolí
žije	žít	k5eAaImIp3nS	žít
přes	přes	k7c4	přes
2,5	[number]	k4	2,5
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Postupný	postupný	k2eAgInSc1d1	postupný
rozvoj	rozvoj	k1gInSc1	rozvoj
Alžíru	Alžír	k1gInSc2	Alžír
na	na	k7c4	na
centrum	centrum	k1gNnSc4	centrum
celé	celý	k2eAgFnSc2d1	celá
oblasti	oblast	k1gFnSc2	oblast
započal	započnout	k5eAaPmAgInS	započnout
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1550	[number]	k4	1550
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ho	on	k3xPp3gMnSc4	on
turečtí	turecký	k2eAgMnPc1d1	turecký
korzáři	korzár	k1gMnPc1	korzár
povýšili	povýšit	k5eAaPmAgMnP	povýšit
na	na	k7c4	na
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Zisky	zisk	k1gInPc1	zisk
z	z	k7c2	z
námořního	námořní	k2eAgInSc2d1	námořní
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
řemesel	řemeslo	k1gNnPc2	řemeslo
a	a	k8xC	a
zemědělství	zemědělství	k1gNnSc2	zemědělství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
taky	taky	k6eAd1	taky
pirátské	pirátský	k2eAgFnPc1d1	pirátská
kořisti	kořist	k1gFnPc1	kořist
plnily	plnit	k5eAaImAgFnP	plnit
pokladny	pokladna	k1gFnPc4	pokladna
osmanských	osmanský	k2eAgMnPc2d1	osmanský
panovníků	panovník	k1gMnPc2	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
si	se	k3xPyFc3	se
budovali	budovat	k5eAaImAgMnP	budovat
přepychové	přepychový	k2eAgInPc4d1	přepychový
paláce	palác	k1gInPc4	palác
<g/>
,	,	kIx,	,
obklopené	obklopený	k2eAgInPc1d1	obklopený
palmovými	palmový	k2eAgInPc7d1	palmový
háji	háj	k1gInPc7	háj
a	a	k8xC	a
kvetoucími	kvetoucí	k2eAgFnPc7d1	kvetoucí
zahradami	zahrada	k1gFnPc7	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
jako	jako	k8xS	jako
součást	součást	k1gFnSc4	součást
Turecka	Turecko	k1gNnSc2	Turecko
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc4	území
kolem	kolem	k7c2	kolem
města	město	k1gNnSc2	město
Alžíru	Alžír	k1gInSc2	Alžír
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1711	[number]	k4	1711
de	de	k?	de
facto	facto	k1gNnSc1	facto
nezávislé	závislý	k2eNgNnSc1d1	nezávislé
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc4	všechen
pokusy	pokus	k1gInPc4	pokus
evropských	evropský	k2eAgFnPc2d1	Evropská
mocností	mocnost	k1gFnPc2	mocnost
o	o	k7c4	o
zastavení	zastavení	k1gNnSc4	zastavení
loupeživých	loupeživý	k2eAgNnPc2d1	loupeživé
pirátských	pirátský	k2eAgNnPc2d1	pirátské
tažení	tažení	k1gNnPc2	tažení
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
měla	mít	k5eAaImAgFnS	mít
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
v	v	k7c6	v
Alžíru	Alžír	k1gInSc6	Alžír
<g/>
,	,	kIx,	,
však	však	k9	však
ztroskotaly	ztroskotat	k5eAaPmAgFnP	ztroskotat
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
pirátskou	pirátský	k2eAgFnSc7d1	pirátská
kořistí	kořist	k1gFnSc7	kořist
bylo	být	k5eAaImAgNnS	být
zlato	zlato	k1gNnSc1	zlato
<g/>
,	,	kIx,	,
drahé	drahý	k2eAgInPc1d1	drahý
kameny	kámen	k1gInPc1	kámen
a	a	k8xC	a
cenné	cenný	k2eAgFnPc1d1	cenná
suroviny	surovina	k1gFnPc1	surovina
<g/>
.	.	kIx.	.
</s>
<s>
Piráti	pirát	k1gMnPc1	pirát
však	však	k9	však
také	také	k9	také
prodávali	prodávat	k5eAaImAgMnP	prodávat
posádky	posádka	k1gFnPc4	posádka
přepadených	přepadený	k2eAgFnPc2d1	přepadená
lodí	loď	k1gFnPc2	loď
na	na	k7c6	na
trhu	trh	k1gInSc6	trh
s	s	k7c7	s
otroky	otrok	k1gMnPc7	otrok
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
Alžíru	Alžír	k1gInSc2	Alžír
<g/>
.	.	kIx.	.
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1830	[number]	k4	1830
posloužilo	posloužit	k5eAaPmAgNnS	posloužit
Francouzům	Francouz	k1gMnPc3	Francouz
právě	právě	k6eAd1	právě
pirátství	pirátství	k1gNnSc1	pirátství
jako	jako	k8xC	jako
zástěrka	zástěrka	k1gFnSc1	zástěrka
pro	pro	k7c4	pro
dobytí	dobytí	k1gNnSc4	dobytí
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Alžír	Alžír	k1gInSc1	Alžír
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
základnou	základna	k1gFnSc7	základna
pro	pro	k7c4	pro
postup	postup	k1gInSc4	postup
Evropanů	Evropan	k1gMnPc2	Evropan
do	do	k7c2	do
celé	celý	k2eAgFnSc2d1	celá
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
kolonie	kolonie	k1gFnSc2	kolonie
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
pro	pro	k7c4	pro
cizí	cizí	k2eAgMnPc4d1	cizí
mocipány	mocipán	k1gMnPc4	mocipán
ohniskem	ohnisko	k1gNnSc7	ohnisko
nepokojů	nepokoj	k1gInPc2	nepokoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Alžíru	Alžír	k1gInSc6	Alžír
odpor	odpor	k1gInSc4	odpor
vůči	vůči	k7c3	vůči
Francouzům	Francouz	k1gMnPc3	Francouz
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
nikdy	nikdy	k6eAd1	nikdy
neutichl	utichnout	k5eNaPmAgMnS	utichnout
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
bojů	boj	k1gInPc2	boj
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1954	[number]	k4	1954
až	až	k9	až
1962	[number]	k4	1962
byla	být	k5eAaImAgFnS	být
Kasba	Kasba	k1gFnSc1	Kasba
dějištěm	dějiště	k1gNnSc7	dějiště
tří	tři	k4xCgFnPc2	tři
proslulých	proslulý	k2eAgFnPc2d1	proslulá
bitev	bitva	k1gFnPc2	bitva
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc2	jenž
velel	velet	k5eAaImAgMnS	velet
generál	generál	k1gMnSc1	generál
Jacques	Jacques	k1gMnSc1	Jacques
Massu	Massa	k1gFnSc4	Massa
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
mírou	míra	k1gFnSc7	míra
brutality	brutalita	k1gFnSc2	brutalita
byla	být	k5eAaImAgFnS	být
nejdříve	dříve	k6eAd3	dříve
rozbita	rozbit	k2eAgFnSc1d1	rozbita
povstalecká	povstalecký	k2eAgFnSc1d1	povstalecká
Fronta	fronta	k1gFnSc1	fronta
národního	národní	k2eAgNnSc2d1	národní
osvobození	osvobození	k1gNnSc2	osvobození
(	(	kIx(	(
<g/>
FLN	FLN	kA	FLN
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nakonec	nakonec	k6eAd1	nakonec
Alžířané	Alžířan	k1gMnPc1	Alžířan
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
nezávislosti	nezávislost	k1gFnSc2	nezávislost
přece	přece	k8xC	přece
jen	jen	k9	jen
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
je	být	k5eAaImIp3nS	být
život	život	k1gInSc4	život
v	v	k7c6	v
Alžíru	Alžír	k1gInSc6	Alžír
poznamenán	poznamenat	k5eAaPmNgInS	poznamenat
novou	nový	k2eAgFnSc7d1	nová
občanskou	občanský	k2eAgFnSc7d1	občanská
válkou	válka	k1gFnSc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
při	při	k7c6	při
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1991-1992	[number]	k4	1991-1992
ukazovalo	ukazovat	k5eAaImAgNnS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zvítězí	zvítězit	k5eAaPmIp3nS	zvítězit
fundamentalistická	fundamentalistický	k2eAgFnSc1d1	fundamentalistická
Islámská	islámský	k2eAgFnSc1d1	islámská
fronta	fronta	k1gFnSc1	fronta
spásy	spása	k1gFnSc2	spása
(	(	kIx(	(
<g/>
FIS	FIS	kA	FIS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přerušila	přerušit	k5eAaPmAgFnS	přerušit
armáda	armáda	k1gFnSc1	armáda
volby	volba	k1gFnSc2	volba
a	a	k8xC	a
donutila	donutit	k5eAaPmAgFnS	donutit
prezidenta	prezident	k1gMnSc4	prezident
Chadliho	Chadli	k1gMnSc4	Chadli
k	k	k7c3	k
odstoupení	odstoupení	k1gNnSc3	odstoupení
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
vládne	vládnout	k5eAaImIp3nS	vládnout
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
vojenská	vojenský	k2eAgFnSc1d1	vojenská
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Alžír	Alžír	k1gInSc1	Alžír
je	být	k5eAaImIp3nS	být
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgInS	vyhlásit
výjimečný	výjimečný	k2eAgInSc1d1	výjimečný
stav	stav	k1gInSc1	stav
<g/>
.	.	kIx.	.
</s>
<s>
Fundamentalistická	fundamentalistický	k2eAgFnSc1d1	fundamentalistická
opozice	opozice	k1gFnSc1	opozice
opakovaně	opakovaně	k6eAd1	opakovaně
vraždí	vraždit	k5eAaImIp3nS	vraždit
intelektuály	intelektuál	k1gMnPc4	intelektuál
<g/>
,	,	kIx,	,
novináře	novinář	k1gMnPc4	novinář
<g/>
,	,	kIx,	,
turisty	turist	k1gMnPc4	turist
nebo	nebo	k8xC	nebo
zahraniční	zahraniční	k2eAgMnPc4d1	zahraniční
úředníky	úředník	k1gMnPc4	úředník
<g/>
.	.	kIx.	.
</s>
<s>
Vojsko	vojsko	k1gNnSc1	vojsko
se	s	k7c7	s
stejnou	stejný	k2eAgFnSc7d1	stejná
tvrdostí	tvrdost	k1gFnSc7	tvrdost
údery	úder	k1gInPc4	úder
vrací	vracet	k5eAaImIp3nS	vracet
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
podporuje	podporovat	k5eAaImIp3nS	podporovat
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nebude	být	k5eNaImBp3nS	být
moci	moct	k5eAaImF	moct
ukončit	ukončit	k5eAaPmF	ukončit
občanskou	občanský	k2eAgFnSc4d1	občanská
válku	válka	k1gFnSc4	válka
bez	bez	k7c2	bez
svobodných	svobodný	k2eAgFnPc2d1	svobodná
voleb	volba	k1gFnPc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Alžír	Alžír	k1gInSc1	Alžír
splňuje	splňovat	k5eAaImIp3nS	splňovat
všechny	všechen	k3xTgInPc4	všechen
předpoklady	předpoklad	k1gInPc4	předpoklad
pro	pro	k7c4	pro
prosperující	prosperující	k2eAgNnSc4d1	prosperující
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Starý	starý	k2eAgInSc1d1	starý
přístav	přístav	k1gInSc1	přístav
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
masivu	masiv	k1gInSc2	masiv
Bouzareah	Bouzareah	k1gInSc1	Bouzareah
je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
přístavní	přístavní	k2eAgFnSc7d1	přístavní
hrází	hráz	k1gFnSc7	hráz
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
už	už	k6eAd1	už
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
spojením	spojení	k1gNnSc7	spojení
přilehlých	přilehlý	k2eAgInPc2d1	přilehlý
skalnatých	skalnatý	k2eAgInPc2d1	skalnatý
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Alžír	Alžír	k1gInSc1	Alžír
má	mít	k5eAaImIp3nS	mít
zemědělské	zemědělský	k2eAgNnSc4d1	zemědělské
zázemí	zázemí	k1gNnSc4	zázemí
v	v	k7c6	v
nížině	nížina	k1gFnSc6	nížina
Mitidža	Mitidžum	k1gNnSc2	Mitidžum
a	a	k8xC	a
v	v	k7c4	v
Sahelu	Sahela	k1gFnSc4	Sahela
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterému	který	k3yIgInSc3	který
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
město	město	k1gNnSc1	město
odedávna	odedávna	k6eAd1	odedávna
zásobováno	zásobovat	k5eAaImNgNnS	zásobovat
zemědělskými	zemědělský	k2eAgInPc7d1	zemědělský
produkty	produkt	k1gInPc7	produkt
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc4	tento
území	území	k1gNnSc4	území
nejdůležitější	důležitý	k2eAgFnSc7d3	nejdůležitější
zemědělskou	zemědělský	k2eAgFnSc7d1	zemědělská
oblastí	oblast	k1gFnSc7	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
víno	víno	k1gNnSc1	víno
<g/>
,	,	kIx,	,
tabák	tabák	k1gInSc1	tabák
<g/>
,	,	kIx,	,
citrusové	citrusový	k2eAgInPc1d1	citrusový
plody	plod	k1gInPc1	plod
a	a	k8xC	a
obilniny	obilnina	k1gFnPc1	obilnina
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
přístav	přístav	k1gInSc1	přístav
tvořily	tvořit	k5eAaImAgInP	tvořit
po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
předmostí	předmostí	k1gNnSc2	předmostí
k	k	k7c3	k
Evropě	Evropa	k1gFnSc3	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
století	století	k1gNnSc2	století
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
dokonce	dokonce	k9	dokonce
městské	městský	k2eAgFnPc1d1	městská
čtvrti	čtvrt	k1gFnPc1	čtvrt
evropského	evropský	k2eAgInSc2d1	evropský
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
se	se	k3xPyFc4	se
posunulo	posunout	k5eAaPmAgNnS	posunout
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
jih	jih	k1gInSc4	jih
a	a	k8xC	a
Kasba	Kasb	k1gMnSc2	Kasb
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgNnSc4d1	vlastní
historické	historický	k2eAgNnSc4d1	historické
centrum	centrum	k1gNnSc4	centrum
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
okraj	okraj	k1gInSc4	okraj
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgNnSc1d1	historické
centrum	centrum	k1gNnSc1	centrum
dodnes	dodnes	k6eAd1	dodnes
neztratilo	ztratit	k5eNaPmAgNnS	ztratit
svůj	svůj	k3xOyFgInSc4	svůj
zvláštní	zvláštní	k2eAgInSc4d1	zvláštní
půvab	půvab	k1gInSc4	půvab
<g/>
.	.	kIx.	.
</s>
<s>
Domy	dům	k1gInPc1	dům
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
kostek	kostka	k1gFnPc2	kostka
se	se	k3xPyFc4	se
podobají	podobat	k5eAaImIp3nP	podobat
pevnostem	pevnost	k1gFnPc3	pevnost
ve	v	k7c6	v
změti	změt	k1gFnSc6	změt
neproniknutelných	proniknutelný	k2eNgFnPc2d1	neproniknutelná
uliček	ulička	k1gFnPc2	ulička
<g/>
.	.	kIx.	.
</s>
<s>
Miniaturní	miniaturní	k2eAgInPc1d1	miniaturní
obchůdky	obchůdek	k1gInPc1	obchůdek
vypadají	vypadat	k5eAaImIp3nP	vypadat
jako	jako	k9	jako
jeskyně	jeskyně	k1gFnPc1	jeskyně
a	a	k8xC	a
na	na	k7c6	na
malých	malý	k2eAgNnPc6d1	malé
tržištích	tržiště	k1gNnPc6	tržiště
se	se	k3xPyFc4	se
mísí	mísit	k5eAaImIp3nS	mísit
zápach	zápach	k1gInSc1	zápach
s	s	k7c7	s
lákavými	lákavý	k2eAgFnPc7d1	lákavá
vůněmi	vůně	k1gFnPc7	vůně
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jiných	jiný	k2eAgNnPc6d1	jiné
místech	místo	k1gNnPc6	místo
prodělalo	prodělat	k5eAaPmAgNnS	prodělat
město	město	k1gNnSc1	město
značnou	značný	k2eAgFnSc4d1	značná
proměnu	proměna	k1gFnSc4	proměna
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
někdejších	někdejší	k2eAgFnPc2d1	někdejší
vesnic	vesnice	k1gFnPc2	vesnice
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgNnP	stát
šedá	šedý	k2eAgNnPc1d1	šedé
předměstí	předměstí	k1gNnPc1	předměstí
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
nekontrolovatelně	kontrolovatelně	k6eNd1	kontrolovatelně
bují	bujet	k5eAaImIp3nP	bujet
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
prudkého	prudký	k2eAgInSc2d1	prudký
růstu	růst	k1gInSc2	růst
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
mělo	mít	k5eAaImAgNnS	mít
město	město	k1gNnSc1	město
Alžír	Alžír	k1gInSc1	Alžír
850	[number]	k4	850
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
asi	asi	k9	asi
tři	tři	k4xCgInPc1	tři
miliony	milion	k4xCgInPc1	milion
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
proud	proud	k1gInSc1	proud
nových	nový	k2eAgMnPc2d1	nový
obyvatel	obyvatel	k1gMnPc2	obyvatel
přišel	přijít	k5eAaPmAgMnS	přijít
do	do	k7c2	do
Alžíru	Alžír	k1gInSc2	Alžír
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1954	[number]	k4	1954
až	až	k9	až
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
vyrovnal	vyrovnat	k5eAaBmAgInS	vyrovnat
úbytek	úbytek	k1gInSc1	úbytek
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Alžírska	Alžírsko	k1gNnSc2	Alžírsko
odešli	odejít	k5eAaPmAgMnP	odejít
evropští	evropský	k2eAgMnPc1d1	evropský
osadníci	osadník	k1gMnPc1	osadník
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Francouzi	Francouz	k1gMnPc1	Francouz
<g/>
,	,	kIx,	,
hromadně	hromadně	k6eAd1	hromadně
do	do	k7c2	do
Francie	Francie	k1gFnSc2	Francie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
novém	nový	k2eAgNnSc6d1	nové
městě	město	k1gNnSc6	město
mezitím	mezitím	k6eAd1	mezitím
chátrají	chátrat	k5eAaImIp3nP	chátrat
klasicistní	klasicistní	k2eAgInPc1d1	klasicistní
činžovní	činžovní	k2eAgInPc1d1	činžovní
domy	dům	k1gInPc1	dům
a	a	k8xC	a
ani	ani	k8xC	ani
z	z	k7c2	z
evropské	evropský	k2eAgFnSc2d1	Evropská
elegance	elegance	k1gFnSc2	elegance
minulých	minulý	k2eAgFnPc2d1	minulá
dob	doba	k1gFnPc2	doba
už	už	k6eAd1	už
mnoho	mnoho	k6eAd1	mnoho
nezbylo	zbýt	k5eNaPmAgNnS	zbýt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
proslulé	proslulý	k2eAgFnSc6d1	proslulá
obchodní	obchodní	k2eAgFnSc6d1	obchodní
třídě	třída	k1gFnSc6	třída
Rue	Rue	k1gFnSc2	Rue
Michelet	Michelet	k1gInSc4	Michelet
ustoupily	ustoupit	k5eAaPmAgInP	ustoupit
luxusní	luxusní	k2eAgInPc1d1	luxusní
butiky	butik	k1gInPc1	butik
vetešnickým	vetešnický	k2eAgMnPc3d1	vetešnický
krámkům	krámek	k1gInPc3	krámek
<g/>
.	.	kIx.	.
</s>
<s>
Občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
a	a	k8xC	a
přistěhovalectví	přistěhovalectví	k1gNnSc1	přistěhovalectví
propůjčily	propůjčit	k5eAaPmAgInP	propůjčit
tomuto	tento	k3xDgNnSc3	tento
kdysi	kdysi	k6eAd1	kdysi
"	"	kIx"	"
<g/>
bílému	bílý	k2eAgNnSc3d1	bílé
městu	město	k1gNnSc3	město
<g/>
"	"	kIx"	"
šedivý	šedivý	k2eAgInSc4d1	šedivý
nádech	nádech	k1gInSc4	nádech
a	a	k8xC	a
učinily	učinit	k5eAaImAgFnP	učinit
z	z	k7c2	z
něj	on	k3xPp3gInSc2	on
těžko	těžko	k6eAd1	těžko
opravitelný	opravitelný	k2eAgInSc1d1	opravitelný
kolos	kolos	k1gInSc1	kolos
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Alžíru	Alžír	k1gInSc6	Alžír
se	se	k3xPyFc4	se
mísí	mísit	k5eAaImIp3nP	mísit
vlivy	vliv	k1gInPc1	vliv
pěti	pět	k4xCc2	pět
různých	různý	k2eAgFnPc2d1	různá
kultur	kultura	k1gFnPc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Islámští	islámský	k2eAgMnPc1d1	islámský
fundamentalisté	fundamentalista	k1gMnPc1	fundamentalista
se	se	k3xPyFc4	se
však	však	k9	však
už	už	k6eAd1	už
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
pokoušejí	pokoušet	k5eAaImIp3nP	pokoušet
této	tento	k3xDgFnSc6	tento
mnohojakosti	mnohojakost	k1gFnSc6	mnohojakost
násilným	násilný	k2eAgInSc7d1	násilný
způsobem	způsob	k1gInSc7	způsob
zbavit	zbavit	k5eAaPmF	zbavit
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgInSc7d1	významný
přístavem	přístav	k1gInSc7	přístav
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
centrem	centrum	k1gNnSc7	centrum
vývozu	vývoz	k1gInSc2	vývoz
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
zemědělských	zemědělský	k2eAgInPc2d1	zemědělský
produktů	produkt	k1gInPc2	produkt
a	a	k8xC	a
textilií	textilie	k1gFnPc2	textilie
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
centrem	centrum	k1gNnSc7	centrum
služeb	služba	k1gFnPc2	služba
celého	celý	k2eAgNnSc2d1	celé
Alžírska	Alžírsko	k1gNnSc2	Alžírsko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
letiště	letiště	k1gNnPc4	letiště
Houariho	Houari	k1gMnSc2	Houari
Boumédiè	Boumédiè	k1gMnSc2	Boumédiè
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Alžíru	Alžír	k1gInSc6	Alžír
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
konaly	konat	k5eAaImAgInP	konat
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
třetí	třetí	k4xOgFnSc2	třetí
Africké	africký	k2eAgFnSc2d1	africká
hry	hra	k1gFnSc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
antice	antika	k1gFnSc6	antika
se	se	k3xPyFc4	se
Alžír	Alžír	k1gInSc1	Alžír
jmenoval	jmenovat	k5eAaImAgInS	jmenovat
Icosium	Icosium	k1gNnSc4	Icosium
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgNnSc1d1	dnešní
město	město	k1gNnSc1	město
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
10	[number]	k4	10
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
našeho	náš	k3xOp1gInSc2	náš
letopočtu	letopočet	k1gInSc2	letopočet
na	na	k7c6	na
ruinách	ruina	k1gFnPc6	ruina
někdejšího	někdejší	k2eAgMnSc2d1	někdejší
Icosia	Icosius	k1gMnSc2	Icosius
<g/>
.	.	kIx.	.
</s>
<s>
Miguel	Miguel	k1gMnSc1	Miguel
de	de	k?	de
Cervantes	Cervantes	k1gMnSc1	Cervantes
<g/>
,	,	kIx,	,
španělský	španělský	k2eAgMnSc1d1	španělský
básník	básník	k1gMnSc1	básník
a	a	k8xC	a
autor	autor	k1gMnSc1	autor
Dona	Don	k1gMnSc4	Don
Quijota	Quijot	k1gMnSc4	Quijot
<g/>
,	,	kIx,	,
padl	padnout	k5eAaPmAgInS	padnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1575	[number]	k4	1575
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
alžírských	alžírský	k2eAgMnPc2d1	alžírský
pirátů	pirát	k1gMnPc2	pirát
a	a	k8xC	a
celých	celý	k2eAgNnPc2d1	celé
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
strávil	strávit	k5eAaPmAgMnS	strávit
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
koloniální	koloniální	k2eAgFnSc2d1	koloniální
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
uprchlo	uprchnout	k5eAaPmAgNnS	uprchnout
z	z	k7c2	z
Alžíru	Alžír	k1gInSc2	Alžír
200	[number]	k4	200
000	[number]	k4	000
Evropanů	Evropan	k1gMnPc2	Evropan
<g/>
.	.	kIx.	.
</s>
<s>
Jacques	Jacques	k1gMnSc1	Jacques
Attali	Attali	k1gMnSc1	Attali
<g/>
,	,	kIx,	,
ekonom	ekonom	k1gMnSc1	ekonom
Daniel	Daniel	k1gMnSc1	Daniel
Auteuil	Auteuil	k1gMnSc1	Auteuil
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
Albert	Albert	k1gMnSc1	Albert
Camus	Camus	k1gMnSc1	Camus
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
filozof	filozof	k1gMnSc1	filozof
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
Françoise	Françoise	k1gFnSc2	Françoise
Durrová	Durrová	k1gFnSc1	Durrová
<g/>
,	,	kIx,	,
tenistka	tenistka	k1gFnSc1	tenistka
Marlè	Marlè	k1gFnSc1	Marlè
Jobertová	Jobertová	k1gFnSc1	Jobertová
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Nouria	Nourium	k1gNnSc2	Nourium
Mérahová-Benidaová	Mérahová-Benidaová	k1gFnSc1	Mérahová-Benidaová
<g/>
,	,	kIx,	,
atletka	atletka	k1gFnSc1	atletka
Jacques	Jacques	k1gMnSc1	Jacques
Ranciè	Ranciè	k1gMnSc1	Ranciè
<g/>
,	,	kIx,	,
filozof	filozof	k1gMnSc1	filozof
Camille	Camille	k1gFnSc2	Camille
Saint-Saëns	Saint-Saëns	k1gInSc1	Saint-Saëns
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
Charles	Charles	k1gMnSc1	Charles
Martial	Martial	k1gMnSc1	Martial
Lavigerie	Lavigerie	k1gFnSc1	Lavigerie
<g/>
,	,	kIx,	,
římskokatolický	římskokatolický	k2eAgMnSc1d1	římskokatolický
duchovní	duchovní	k1gMnSc1	duchovní
<g/>
,	,	kIx,	,
kardinál	kardinál	k1gMnSc1	kardinál
</s>
