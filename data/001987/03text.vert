<s>
Network	network	k1gInSc1	network
File	File	k1gNnSc2	File
System	Syst	k1gInSc7	Syst
(	(	kIx(	(
<g/>
NFS	NFS	kA	NFS
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
internetový	internetový	k2eAgInSc4d1	internetový
protokol	protokol	k1gInSc4	protokol
pro	pro	k7c4	pro
vzdálený	vzdálený	k2eAgInSc4d1	vzdálený
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
souborům	soubor	k1gInPc3	soubor
přes	přes	k7c4	přes
počítačovou	počítačový	k2eAgFnSc4d1	počítačová
síť	síť	k1gFnSc4	síť
<g/>
.	.	kIx.	.
</s>
<s>
Protokol	protokol	k1gInSc1	protokol
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
společností	společnost	k1gFnSc7	společnost
Sun	suna	k1gFnPc2	suna
Microsystems	Microsystemsa	k1gFnPc2	Microsystemsa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
má	mít	k5eAaImIp3nS	mít
jeho	jeho	k3xOp3gInSc1	jeho
další	další	k2eAgInSc1d1	další
vývoj	vývoj	k1gInSc1	vývoj
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
organizace	organizace	k1gFnSc1	organizace
Internet	Internet	k1gInSc4	Internet
Engineering	Engineering	k1gInSc1	Engineering
Task	Task	k1gInSc1	Task
Force	force	k1gFnSc1	force
(	(	kIx(	(
<g/>
IETF	IETF	kA	IETF
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Funguje	fungovat	k5eAaImIp3nS	fungovat
především	především	k9	především
nad	nad	k7c7	nad
transportním	transportní	k2eAgInSc7d1	transportní
protokolem	protokol	k1gInSc7	protokol
UDP	UDP	kA	UDP
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
od	od	k7c2	od
verze	verze	k1gFnSc2	verze
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
ho	on	k3xPp3gMnSc4	on
provozovat	provozovat	k5eAaImF	provozovat
také	také	k9	také
nad	nad	k7c7	nad
protokolem	protokol	k1gInSc7	protokol
TCP	TCP	kA	TCP
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
si	se	k3xPyFc3	se
můžete	moct	k5eAaImIp2nP	moct
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
NFS	NFS	kA	NFS
klienta	klient	k1gMnSc4	klient
připojit	připojit	k5eAaPmF	připojit
disk	disk	k1gInSc4	disk
ze	z	k7c2	z
vzdáleného	vzdálený	k2eAgInSc2d1	vzdálený
serveru	server	k1gInSc2	server
a	a	k8xC	a
pracovat	pracovat	k5eAaImF	pracovat
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
jako	jako	k8xS	jako
s	s	k7c7	s
lokálním	lokální	k2eAgInSc7d1	lokální
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
Linuxu	linux	k1gInSc2	linux
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
asi	asi	k9	asi
o	o	k7c4	o
nejpoužívanější	používaný	k2eAgInSc4d3	nejpoužívanější
protokol	protokol	k1gInSc4	protokol
pro	pro	k7c4	pro
tyto	tento	k3xDgInPc4	tento
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
kolegu	kolega	k1gMnSc4	kolega
NFS	NFS	kA	NFS
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
protokol	protokol	k1gInSc4	protokol
NIS	Nisa	k1gFnPc2	Nisa
(	(	kIx(	(
<g/>
Network	network	k1gInSc1	network
Information	Information	k1gInSc1	Information
Service	Service	k1gFnSc1	Service
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
distribuci	distribuce	k1gFnSc3	distribuce
nejrůznějších	různý	k2eAgNnPc2d3	nejrůznější
konfiguračních	konfigurační	k2eAgNnPc2d1	konfigurační
dat	datum	k1gNnPc2	datum
v	v	k7c6	v
počítačové	počítačový	k2eAgFnSc6d1	počítačová
síti	síť	k1gFnSc6	síť
(	(	kIx(	(
<g/>
např.	např.	kA	např.
uživatelé	uživatel	k1gMnPc1	uživatel
<g/>
,	,	kIx,	,
doménová	doménový	k2eAgNnPc1d1	doménové
jména	jméno	k1gNnPc1	jméno
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
NFS	NFS	kA	NFS
server	server	k1gInSc1	server
se	se	k3xPyFc4	se
nastavuje	nastavovat	k5eAaImIp3nS	nastavovat
pomocí	pomocí	k7c2	pomocí
konfiguračního	konfigurační	k2eAgInSc2d1	konfigurační
souboru	soubor	k1gInSc2	soubor
/	/	kIx~	/
<g/>
etc	etc	k?	etc
<g/>
/	/	kIx~	/
<g/>
exports	exports	k1gInSc1	exports
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
na	na	k7c6	na
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
řádcích	řádek	k1gInPc6	řádek
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
definice	definice	k1gFnSc1	definice
sdílených	sdílený	k2eAgInPc2d1	sdílený
adresářů	adresář	k1gInPc2	adresář
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
první	první	k4xOgFnSc1	první
je	být	k5eAaImIp3nS	být
název	název	k1gInSc1	název
adresáře	adresář	k1gInSc2	adresář
a	a	k8xC	a
pak	pak	k6eAd1	pak
seznam	seznam	k1gInSc1	seznam
povolených	povolený	k2eAgMnPc2d1	povolený
klientů	klient	k1gMnPc2	klient
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
jsou	být	k5eAaImIp3nP	být
uvedeny	uveden	k2eAgInPc1d1	uveden
názvy	název	k1gInPc1	název
server	server	k1gInSc4	server
<g/>
,	,	kIx,	,
stanice	stanice	k1gFnSc1	stanice
a	a	k8xC	a
IP	IP	kA	IP
adresa	adresa	k1gFnSc1	adresa
<g/>
)	)	kIx)	)
s	s	k7c7	s
přidanými	přidaný	k2eAgInPc7d1	přidaný
volitelnými	volitelný	k2eAgInPc7d1	volitelný
parametry	parametr	k1gInPc7	parametr
<g/>
:	:	kIx,	:
/	/	kIx~	/
<g/>
usr	usr	k?	usr
10.1.2.3	[number]	k4	10.1.2.3
<g/>
(	(	kIx(	(
<g/>
ro	ro	k?	ro
<g/>
)	)	kIx)	)
stanice	stanice	k1gFnSc2	stanice
<g/>
(	(	kIx(	(
<g/>
ro	ro	k?	ro
<g/>
)	)	kIx)	)
/	/	kIx~	/
<g/>
<g />
.	.	kIx.	.
</s>
<s>
home	home	k1gInSc1	home
10.1.2.3	[number]	k4	10.1.2.3
<g/>
(	(	kIx(	(
<g/>
rw	rw	k?	rw
<g/>
,	,	kIx,	,
<g/>
no_root_squash	no_root_squash	k1gInSc1	no_root_squash
<g/>
)	)	kIx)	)
stanice	stanice	k1gFnSc1	stanice
<g/>
(	(	kIx(	(
<g/>
rw	rw	k?	rw
<g/>
)	)	kIx)	)
ro	ro	k?	ro
(	(	kIx(	(
<g/>
read-only	readnla	k1gMnSc2	read-onla
<g/>
)	)	kIx)	)
-	-	kIx~	-
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
čtení	čtení	k1gNnSc4	čtení
rw	rw	k?	rw
(	(	kIx(	(
<g/>
read-write	readrit	k1gInSc5	read-writ
<g/>
)	)	kIx)	)
-	-	kIx~	-
povoleno	povolen	k2eAgNnSc1d1	povoleno
čtení	čtení	k1gNnSc1	čtení
i	i	k8xC	i
zápis	zápis	k1gInSc1	zápis
no_root_squash	no_root_squasha	k1gFnPc2	no_root_squasha
-	-	kIx~	-
nemapovat	mapovat	k5eNaImF	mapovat
požadavky	požadavek	k1gInPc4	požadavek
uživatele	uživatel	k1gMnSc2	uživatel
root	root	k5eAaPmF	root
na	na	k7c4	na
běžného	běžný	k2eAgMnSc4d1	běžný
uživatele	uživatel	k1gMnSc4	uživatel
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
nobody	noboda	k1gFnPc4	noboda
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
server	server	k1gInSc1	server
tak	tak	k6eAd1	tak
bere	brát	k5eAaImIp3nS	brát
id	idy	k1gFnPc2	idy
uživatele	uživatel	k1gMnPc4	uživatel
od	od	k7c2	od
klienta	klient	k1gMnSc2	klient
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
bezpečnostní	bezpečnostní	k2eAgNnSc1d1	bezpečnostní
riziko	riziko	k1gNnSc1	riziko
<g/>
)	)	kIx)	)
Klient	klient	k1gMnSc1	klient
připojuje	připojovat	k5eAaImIp3nS	připojovat
adresář	adresář	k1gInSc4	adresář
ze	z	k7c2	z
serveru	server	k1gInSc2	server
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
adresářového	adresářový	k2eAgInSc2d1	adresářový
stromu	strom	k1gInSc2	strom
stejným	stejný	k2eAgInSc7d1	stejný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
připojovány	připojován	k2eAgInPc4d1	připojován
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
systémy	systém	k1gInPc4	systém
souborů	soubor	k1gInPc2	soubor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
nutné	nutný	k2eAgNnSc1d1	nutné
na	na	k7c4	na
klientovi	klient	k1gMnSc3	klient
spustit	spustit	k5eAaPmF	spustit
též	též	k9	též
démona	démon	k1gMnSc4	démon
portmap	portmap	k1gMnSc1	portmap
<g/>
:	:	kIx,	:
mount	mount	k1gMnSc1	mount
-t	-t	k?	-t
nfs	nfs	k?	nfs
server	server	k1gInSc1	server
<g/>
:	:	kIx,	:
<g/>
/	/	kIx~	/
<g/>
home	home	k1gInSc1	home
/	/	kIx~	/
<g/>
home	homat	k5eAaPmIp3nS	homat
mount	mount	k1gMnSc1	mount
-t	-t	k?	-t
nfs	nfs	k?	nfs
server	server	k1gInSc1	server
<g/>
:	:	kIx,	:
<g/>
/	/	kIx~	/
<g/>
usr	usr	k?	usr
/	/	kIx~	/
<g/>
mnt	mnt	k?	mnt
<g/>
/	/	kIx~	/
<g/>
usr-from-server	usrromerver	k1gMnSc1	usr-from-server
Od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
chvíle	chvíle	k1gFnSc2	chvíle
může	moct	k5eAaImIp3nS	moct
klient	klient	k1gMnSc1	klient
s	s	k7c7	s
daty	datum	k1gNnPc7	datum
v	v	k7c6	v
adresáři	adresář	k1gInSc6	adresář
/	/	kIx~	/
<g/>
home	home	k6eAd1	home
a	a	k8xC	a
/	/	kIx~	/
<g/>
mnt	mnt	k?	mnt
<g/>
/	/	kIx~	/
<g/>
usr-from-server	usrromerver	k1gInSc4	usr-from-server
pracovat	pracovat	k5eAaImF	pracovat
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
kdyby	kdyby	kYmCp3nP	kdyby
byly	být	k5eAaImAgInP	být
umístěny	umístit	k5eAaPmNgInP	umístit
na	na	k7c6	na
lokálním	lokální	k2eAgInSc6d1	lokální
počítači	počítač	k1gInSc6	počítač
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešních	dnešní	k2eAgFnPc6d1	dnešní
moderních	moderní	k2eAgFnPc6d1	moderní
distribucích	distribuce	k1gFnPc6	distribuce
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
setkáme	setkat	k5eAaPmIp1nP	setkat
s	s	k7c7	s
nástupcem	nástupce	k1gMnSc7	nástupce
NFS	NFS	kA	NFS
verze	verze	k1gFnSc1	verze
4	[number]	k4	4
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
z	z	k7c2	z
uživatelského	uživatelský	k2eAgNnSc2d1	Uživatelské
hlediska	hledisko	k1gNnSc2	hledisko
vypadá	vypadat	k5eAaPmIp3nS	vypadat
téměř	téměř	k6eAd1	téměř
totožně	totožně	k6eAd1	totožně
<g/>
,	,	kIx,	,
přece	přece	k9	přece
jen	jen	k9	jen
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
předchůdce	předchůdce	k1gMnSc1	předchůdce
hodně	hodně	k6eAd1	hodně
liší	lišit	k5eAaImIp3nS	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnPc1d1	základní
odlišnosti	odlišnost	k1gFnPc1	odlišnost
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Pseudo	Pseudo	k1gNnSc1	Pseudo
file	fil	k1gInSc2	fil
systém	systém	k1gInSc1	systém
-	-	kIx~	-
NFSv	NFSv	k1gInSc1	NFSv
<g/>
4	[number]	k4	4
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
exportovat	exportovat	k5eAaBmF	exportovat
nějaký	nějaký	k3yIgInSc4	nějaký
adresář	adresář	k1gInSc4	adresář
jako	jako	k8xS	jako
kořen	kořen	k1gInSc4	kořen
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
dodatečným	dodatečný	k2eAgInSc7d1	dodatečný
parametrem	parametr	k1gInSc7	parametr
fsid	fsida	k1gFnPc2	fsida
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
Stavový	stavový	k2eAgInSc1d1	stavový
protokol	protokol	k1gInSc1	protokol
-	-	kIx~	-
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
předchůdce	předchůdce	k1gMnSc2	předchůdce
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
jádru	jádro	k1gNnSc6	jádro
bezestavový	bezestavový	k2eAgMnSc1d1	bezestavový
(	(	kIx(	(
<g/>
démony	démon	k1gMnPc7	démon
rpclockd	rpclockd	k6eAd1	rpclockd
a	a	k8xC	a
spol	spol	k1gInSc1	spol
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
mu	on	k3xPp3gMnSc3	on
sice	sice	k8xC	sice
dodávají	dodávat	k5eAaImIp3nP	dodávat
stavovost	stavovost	k1gFnSc4	stavovost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejsou	být	k5eNaImIp3nP	být
definovány	definovat	k5eAaBmNgInP	definovat
v	v	k7c6	v
RFC	RFC	kA	RFC
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
součástí	součást	k1gFnSc7	součást
protokolu	protokol	k1gInSc2	protokol
NFS	NFS	kA	NFS
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
NFSv	NFSv	k1gMnSc1	NFSv
<g/>
4	[number]	k4	4
protokol	protokol	k1gInSc1	protokol
stavový	stavový	k2eAgInSc1d1	stavový
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mu	on	k3xPp3gMnSc3	on
dodává	dodávat	k5eAaImIp3nS	dodávat
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
nevýhody	nevýhoda	k1gFnSc2	nevýhoda
větší	veliký	k2eAgFnSc2d2	veliký
složitosti	složitost	k1gFnSc2	složitost
i	i	k9	i
pár	pár	k4xCyI	pár
výhod	výhoda	k1gFnPc2	výhoda
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
delegování	delegování	k1gNnSc1	delegování
čtení	čtení	k1gNnSc2	čtení
nebo	nebo	k8xC	nebo
zápisu	zápis	k1gInSc2	zápis
na	na	k7c4	na
klienta	klient	k1gMnSc4	klient
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
například	například	k6eAd1	například
zamykání	zamykání	k1gNnSc1	zamykání
souborů	soubor	k1gInPc2	soubor
<g/>
.	.	kIx.	.
</s>
<s>
NFSv	NFSv	k1gInSc1	NFSv
<g/>
4	[number]	k4	4
nepotřebuje	potřebovat	k5eNaImIp3nS	potřebovat
portmapper	portmapper	k1gMnSc1	portmapper
<g/>
.	.	kIx.	.
</s>
<s>
Přenos	přenos	k1gInSc1	přenos
ACL	ACL	kA	ACL
-	-	kIx~	-
NFSv	NFSv	k1gMnSc1	NFSv
<g/>
3	[number]	k4	3
přenos	přenos	k1gInSc1	přenos
přístupových	přístupový	k2eAgNnPc2d1	přístupové
práv	právo	k1gNnPc2	právo
neřeší	řešit	k5eNaImIp3nS	řešit
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
jsou	být	k5eAaImIp3nP	být
ACL	ACL	kA	ACL
nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
protokolu	protokol	k1gInSc2	protokol
NFSv	NFSv	k1gMnSc1	NFSv
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Nutno	nutno	k6eAd1	nutno
ale	ale	k9	ale
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
protokol	protokol	k1gInSc4	protokol
NFSv	NFSva	k1gFnPc2	NFSva
<g/>
4	[number]	k4	4
definuje	definovat	k5eAaBmIp3nS	definovat
vlastní	vlastní	k2eAgInSc4d1	vlastní
formát	formát	k1gInSc4	formát
ACL	ACL	kA	ACL
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
odlišný	odlišný	k2eAgInSc1d1	odlišný
(	(	kIx(	(
<g/>
lepší	dobrý	k2eAgMnSc1d2	lepší
<g/>
)	)	kIx)	)
od	od	k7c2	od
formátu	formát	k1gInSc2	formát
Posix	Posix	k1gInSc1	Posix
ACL	ACL	kA	ACL
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ale	ale	k9	ale
nebyl	být	k5eNaImAgInS	být
nikdy	nikdy	k6eAd1	nikdy
standardizován	standardizovat	k5eAaBmNgInS	standardizovat
<g/>
.	.	kIx.	.
</s>
<s>
Jednodušší	jednoduchý	k2eAgNnSc1d2	jednodušší
tunelování	tunelování	k1gNnSc1	tunelování
přes	přes	k7c4	přes
firewall	firewall	k1gInSc4	firewall
-	-	kIx~	-
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
předchůdce	předchůdce	k1gMnSc2	předchůdce
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
NFSv	NFSv	k1gInSc4	NFSv
<g/>
4	[number]	k4	4
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
běh	běh	k1gInSc4	běh
mezi	mezi	k7c7	mezi
klientem	klient	k1gMnSc7	klient
a	a	k8xC	a
serverem	server	k1gInSc7	server
otevřen	otevřít	k5eAaPmNgInS	otevřít
jen	jen	k9	jen
jediný	jediný	k2eAgInSc1d1	jediný
<g/>
,	,	kIx,	,
předem	předem	k6eAd1	předem
definovaný	definovaný	k2eAgInSc1d1	definovaný
port	port	k1gInSc1	port
<g/>
.	.	kIx.	.
mapování	mapování	k1gNnSc1	mapování
uživatelů	uživatel	k1gMnPc2	uživatel
-	-	kIx~	-
NFSv	NFSv	k1gInSc1	NFSv
<g/>
4	[number]	k4	4
byl	být	k5eAaImAgInS	být
vyvíjen	vyvíjet	k5eAaImNgInS	vyvíjet
primárně	primárně	k6eAd1	primárně
jako	jako	k8xS	jako
multiplatformní	multiplatformní	k2eAgInSc1d1	multiplatformní
síťový	síťový	k2eAgInSc1d1	síťový
souborový	souborový	k2eAgInSc1d1	souborový
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
předchůdce	předchůdce	k1gMnSc2	předchůdce
nepřenáší	přenášet	k5eNaImIp3nS	přenášet
User	usrat	k5eAaPmRp2nS	usrat
ID	ido	k1gNnPc2	ido
(	(	kIx(	(
<g/>
UID	UID	kA	UID
<g/>
,	,	kIx,	,
typické	typický	k2eAgNnSc1d1	typické
pro	pro	k7c4	pro
unix	unix	k1gInSc4	unix
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
doménové	doménový	k2eAgNnSc1d1	doménové
jméno	jméno	k1gNnSc1	jméno
ve	v	k7c6	v
formátu	formát	k1gInSc6	formát
username	usernam	k1gInSc5	usernam
<g/>
@	@	kIx~	@
<g/>
doména	doména	k1gFnSc1	doména
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
na	na	k7c6	na
Unixových	unixový	k2eAgInPc6d1	unixový
systémech	systém	k1gInPc6	systém
(	(	kIx(	(
<g/>
klientu	klient	k1gMnSc3	klient
i	i	k8xC	i
serveru	server	k1gInSc2	server
<g/>
)	)	kIx)	)
běžel	běžet	k5eAaImAgInS	běžet
pomocný	pomocný	k2eAgInSc1d1	pomocný
program	program	k1gInSc1	program
starající	starající	k2eAgFnSc2d1	starající
se	se	k3xPyFc4	se
o	o	k7c4	o
překlad	překlad	k1gInSc4	překlad
jmen	jméno	k1gNnPc2	jméno
na	na	k7c4	na
UID	UID	kA	UID
-	-	kIx~	-
obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
démon	démon	k1gMnSc1	démon
rpc	rpc	k?	rpc
<g/>
.	.	kIx.	.
<g/>
idmapd	idmapd	k6eAd1	idmapd
větší	veliký	k2eAgFnSc4d2	veliký
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
-	-	kIx~	-
kromě	kromě	k7c2	kromě
klasické	klasický	k2eAgFnSc2d1	klasická
"	"	kIx"	"
<g/>
systémové	systémový	k2eAgFnSc2d1	systémová
<g/>
"	"	kIx"	"
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
notoricky	notoricky	k6eAd1	notoricky
nebezpečná	bezpečný	k2eNgFnSc1d1	nebezpečná
<g/>
,	,	kIx,	,
protokol	protokol	k1gInSc1	protokol
NFSv	NFSv	k1gInSc1	NFSv
<g/>
4	[number]	k4	4
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
využití	využití	k1gNnSc4	využití
i	i	k9	i
silnějších	silný	k2eAgInPc2d2	silnější
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
mechanismů	mechanismus	k1gInPc2	mechanismus
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgMnSc7	jaký
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
Kerberos	Kerberos	k1gMnSc1	Kerberos
<g/>
.	.	kIx.	.
</s>
<s>
Souborové	souborový	k2eAgInPc1d1	souborový
systémy	systém	k1gInPc1	systém
NFS	NFS	kA	NFS
a	a	k8xC	a
ONC	ONC	kA	ONC
(	(	kIx(	(
<g/>
Sun	Sun	kA	Sun
RPC	RPC	kA	RPC
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
objevovaly	objevovat	k5eAaImAgFnP	objevovat
v	v	k7c6	v
síťově-výpočetních	síťověýpočetní	k2eAgFnPc6d1	síťově-výpočetní
válkách	válka	k1gFnPc6	válka
mezi	mezi	k7c7	mezi
Sun	Sun	kA	Sun
Microsystems	Microsystems	k1gInSc1	Microsystems
a	a	k8xC	a
Apollo	Apollo	k1gMnSc1	Apollo
Computer	computer	k1gInSc1	computer
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
v	v	k7c6	v
Unixových	unixový	k2eAgFnPc6d1	unixová
válkách	válka	k1gFnPc6	válka
(	(	kIx(	(
<g/>
1987	[number]	k4	1987
<g/>
-	-	kIx~	-
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
mezi	mezi	k7c7	mezi
AT	AT	kA	AT
<g/>
&	&	k?	&
<g/>
T	T	kA	T
Corporation	Corporation	k1gInSc1	Corporation
a	a	k8xC	a
Sun	sun	k1gInSc1	sun
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
a	a	k8xC	a
Digital	Digital	kA	Digital
Equipment	Equipment	k1gInSc1	Equipment
Corporation	Corporation	k1gInSc1	Corporation
<g/>
,	,	kIx,	,
Hewlett-Packard	Hewlett-Packard	k1gInSc1	Hewlett-Packard
a	a	k8xC	a
IBM	IBM	kA	IBM
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
druhé	druhý	k4xOgFnSc6	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
vývoje	vývoj	k1gInSc2	vývoj
ONC	ONC	kA	ONC
protokolu	protokol	k1gInSc2	protokol
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
zvaného	zvaný	k2eAgMnSc2d1	zvaný
Sun	Sun	kA	Sun
RPC	RPC	kA	RPC
<g/>
)	)	kIx)	)
nabízel	nabízet	k5eAaImAgInS	nabízet
podobnou	podobný	k2eAgFnSc4d1	podobná
funkcionalitu	funkcionalita	k1gFnSc4	funkcionalita
pouze	pouze	k6eAd1	pouze
Apollův	Apollův	k2eAgInSc1d1	Apollův
síťový	síťový	k2eAgInSc1d1	síťový
souborový	souborový	k2eAgInSc1d1	souborový
systém	systém	k1gInSc1	systém
(	(	kIx(	(
<g/>
NCS	NCS	kA	NCS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
tedy	tedy	k9	tedy
dvě	dva	k4xCgFnPc1	dva
soupeřící	soupeřící	k2eAgFnPc1d1	soupeřící
strany	strana	k1gFnPc1	strana
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
lišily	lišit	k5eAaImAgFnP	lišit
základními	základní	k2eAgFnPc7d1	základní
rozdíly	rozdíl	k1gInPc1	rozdíl
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
dvou	dva	k4xCgFnPc6	dva
RPC	RPC	kA	RPC
systémech	systém	k1gInPc6	systém
(	(	kIx(	(
<g/>
vzdálené	vzdálený	k2eAgNnSc4d1	vzdálené
volání	volání	k1gNnSc4	volání
procedur	procedura	k1gFnPc2	procedura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Metoda	metoda	k1gFnSc1	metoda
ONC	ONC	kA	ONC
pro	pro	k7c4	pro
kódování	kódování	k1gNnSc4	kódování
dat	datum	k1gNnPc2	datum
–	–	k?	–
Externí	externí	k2eAgFnSc1d1	externí
Datová	datový	k2eAgFnSc1d1	datová
Reprezentace	reprezentace	k1gFnSc1	reprezentace
(	(	kIx(	(
<g/>
XDR	XDR	kA	XDR
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
renderovala	renderovat	k5eAaImAgFnS	renderovat
čísla	číslo	k1gNnPc4	číslo
v	v	k7c4	v
big-endian	bigndian	k1gInSc4	big-endian
pořadí	pořadí	k1gNnSc2	pořadí
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
oba	dva	k4xCgMnPc1	dva
účastníci	účastník	k1gMnPc1	účastník
spojení	spojení	k1gNnSc2	spojení
měli	mít	k5eAaImAgMnP	mít
little-endian	littlendian	k1gInSc4	little-endian
strojové	strojový	k2eAgFnSc2d1	strojová
architektury	architektura	k1gFnSc2	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
metoda	metoda	k1gFnSc1	metoda
od	od	k7c2	od
NCS	NCS	kA	NCS
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
vyhnout	vyhnout	k5eAaPmF	vyhnout
prohození	prohození	k1gNnSc4	prohození
bajtů	bajt	k1gInPc2	bajt
kdykoli	kdykoli	k6eAd1	kdykoli
dva	dva	k4xCgMnPc1	dva
účastníci	účastník	k1gMnPc1	účastník
sdíleli	sdílet	k5eAaImAgMnP	sdílet
stejnou	stejný	k2eAgFnSc4d1	stejná
architekturu	architektura	k1gFnSc4	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
společnost	společnost	k1gFnSc1	společnost
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Network	network	k1gInSc4	network
Computing	Computing	k1gInSc1	Computing
Forum	forum	k1gNnSc1	forum
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
(	(	kIx(	(
<g/>
Březen	březen	k1gInSc1	březen
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
o	o	k7c4	o
sloučení	sloučení	k1gNnSc4	sloučení
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgNnPc2	dva
síťově-výpočetních	síťověýpočetní	k2eAgNnPc2d1	síťově-výpočetní
prostředí	prostředí	k1gNnPc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
Sun	sun	k1gInSc1	sun
a	a	k8xC	a
AT	AT	kA	AT
<g/>
&	&	k?	&
<g/>
T	T	kA	T
prohlásili	prohlásit	k5eAaPmAgMnP	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
na	na	k7c6	na
vývoji	vývoj	k1gInSc6	vývoj
Unixového	unixový	k2eAgInSc2d1	unixový
systému	systém	k1gInSc2	systém
V	v	k7c4	v
verze	verze	k1gFnPc4	verze
4	[number]	k4	4
od	od	k7c2	od
AT	AT	kA	AT
<g/>
&	&	k?	&
<g/>
T.	T.	kA	T.
Kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
mnoho	mnoho	k4c4	mnoho
držitelů	držitel	k1gMnPc2	držitel
AT	AT	kA	AT
<g/>
&	&	k?	&
<g/>
T	T	kA	T
licence	licence	k1gFnSc2	licence
Unixového	unixový	k2eAgInSc2d1	unixový
systému	systém	k1gInSc2	systém
V	V	kA	V
začalo	začít	k5eAaPmAgNnS	začít
mít	mít	k5eAaImF	mít
obavy	obava	k1gFnPc4	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc4	tento
postaví	postavit	k5eAaPmIp3nS	postavit
Sun	Sun	kA	Sun
do	do	k7c2	do
výhodnější	výhodný	k2eAgFnSc2d2	výhodnější
pozice	pozice	k1gFnSc2	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Ultimátně	Ultimátně	k6eAd1	Ultimátně
to	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
Open	Open	k1gInSc1	Open
Software	software	k1gInSc1	software
Foundation	Foundation	k1gInSc1	Foundation
(	(	kIx(	(
<g/>
OSF	OSF	kA	OSF
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
(	(	kIx(	(
<g/>
podílelo	podílet	k5eAaImAgNnS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
Digital	Digital	kA	Digital
Equipment	Equipment	k1gInSc1	Equipment
<g/>
,	,	kIx,	,
HP	HP	kA	HP
<g/>
,	,	kIx,	,
IBM	IBM	kA	IBM
a	a	k8xC	a
ostatní	ostatní	k2eAgMnPc1d1	ostatní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
Sun	Sun	kA	Sun
a	a	k8xC	a
AT	AT	kA	AT
<g/>
&	&	k?	&
<g/>
T	T	kA	T
předtím	předtím	k6eAd1	předtím
vedli	vést	k5eAaImAgMnP	vést
souboj	souboj	k1gInSc4	souboj
NFS	NFS	kA	NFS
vs	vs	k?	vs
<g/>
.	.	kIx.	.
</s>
<s>
RFS	RFS	kA	RFS
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
následná	následný	k2eAgFnSc1d1	následná
rychlá	rychlý	k2eAgFnSc1d1	rychlá
adopce	adopce	k1gFnSc1	adopce
NFS	NFS	kA	NFS
přes	přes	k7c4	přes
RFS	RFS	kA	RFS
od	od	k7c2	od
Digital	Digital	kA	Digital
Equipment	Equipment	k1gInSc1	Equipment
<g/>
,	,	kIx,	,
HP	HP	kA	HP
<g/>
,	,	kIx,	,
IBM	IBM	kA	IBM
(	(	kIx(	(
<g/>
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgMnPc2d1	další
počítačových	počítačový	k2eAgMnPc2d1	počítačový
prodejců	prodejce	k1gMnPc2	prodejce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přetáhla	přetáhnout	k5eAaPmAgFnS	přetáhnout
většinu	většina	k1gFnSc4	většina
uživatelů	uživatel	k1gMnPc2	uživatel
k	k	k7c3	k
používání	používání	k1gNnSc3	používání
NFS	NFS	kA	NFS
<g/>
.	.	kIx.	.
</s>
<s>
Popularita	popularita	k1gFnSc1	popularita
NFS	NFS	kA	NFS
byla	být	k5eAaImAgFnS	být
navíc	navíc	k6eAd1	navíc
podpořena	podpořit	k5eAaPmNgFnS	podpořit
i	i	k9	i
událostí	událost	k1gFnSc7	událost
zvanou	zvaný	k2eAgFnSc7d1	zvaná
"	"	kIx"	"
<g/>
Connectathons	Connectathonsa	k1gFnPc2	Connectathonsa
<g/>
"	"	kIx"	"
počínající	počínající	k2eAgFnSc2d1	počínající
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
umožnila	umožnit	k5eAaPmAgFnS	umožnit
neutrálním	neutrální	k2eAgMnPc3d1	neutrální
prodejcům	prodejce	k1gMnPc3	prodejce
testovat	testovat	k5eAaImF	testovat
implementace	implementace	k1gFnSc2	implementace
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
OSF	OSF	kA	OSF
si	se	k3xPyFc3	se
vyžádal	vyžádat	k5eAaPmAgMnS	vyžádat
návrhy	návrh	k1gInPc4	návrh
na	na	k7c4	na
různé	různý	k2eAgFnPc4d1	různá
technologie	technologie	k1gFnPc4	technologie
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgMnPc7	jenž
byl	být	k5eAaImAgMnS	být
i	i	k9	i
systém	systém	k1gInSc4	systém
vzdáleného	vzdálený	k2eAgNnSc2d1	vzdálené
volaní	volaný	k2eAgMnPc1d1	volaný
procedur	procedura	k1gFnPc2	procedura
(	(	kIx(	(
<g/>
RPC	RPC	kA	RPC
<g/>
)	)	kIx)	)
a	a	k8xC	a
protokol	protokol	k1gInSc4	protokol
vzdáleného	vzdálený	k2eAgInSc2d1	vzdálený
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
souborům	soubor	k1gInPc3	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Čímž	což	k3yQnSc7	což
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
dvě	dva	k4xCgFnPc4	dva
nové	nový	k2eAgFnPc4d1	nová
technologie	technologie	k1gFnPc4	technologie
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
Distribuované	distribuovaný	k2eAgNnSc4d1	distribuované
výpočetní	výpočetní	k2eAgNnSc4d1	výpočetní
prostředí	prostředí	k1gNnSc4	prostředí
(	(	kIx(	(
<g/>
DCE	DCE	kA	DCE
<g/>
)	)	kIx)	)
a	a	k8xC	a
Distribuovaný	distribuovaný	k2eAgInSc1d1	distribuovaný
souborový	souborový	k2eAgInSc1d1	souborový
systém	systém	k1gInSc1	systém
(	(	kIx(	(
<g/>
DFS	DFS	kA	DFS
<g/>
)	)	kIx)	)
respektive	respektive	k9	respektive
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vyhrály	vyhrát	k5eAaPmAgInP	vyhrát
nad	nad	k7c4	nad
ONC	ONC	kA	ONC
a	a	k8xC	a
NFS	NFS	kA	NFS
co	co	k9	co
navrhoval	navrhovat	k5eAaImAgInS	navrhovat
Sun	sun	k1gInSc1	sun
<g/>
.	.	kIx.	.
</s>
<s>
DCE	DCE	kA	DCE
bylo	být	k5eAaImAgNnS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
několika	několik	k4yIc2	několik
technologií	technologie	k1gFnPc2	technologie
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
NCS	NCS	kA	NCS
a	a	k8xC	a
Kerberos	Kerberos	k1gMnSc1	Kerberos
a	a	k8xC	a
DFS	DFS	kA	DFS
použilo	použít	k5eAaPmAgNnS	použít
DCE	DCE	kA	DCE
jako	jako	k8xC	jako
RPC	RPC	kA	RPC
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
Andrewova	Andrewův	k2eAgInSc2d1	Andrewův
souborového	souborový	k2eAgInSc2d1	souborový
systému	systém	k1gInSc2	systém
(	(	kIx(	(
<g/>
AFS	AFS	kA	AFS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sun	Sun	kA	Sun
Microsystems	Microsystems	k1gInSc1	Microsystems
a	a	k8xC	a
Internet	Internet	k1gInSc1	Internet
Society	societa	k1gFnSc2	societa
(	(	kIx(	(
<g/>
ISOC	ISOC	kA	ISOC
<g/>
)	)	kIx)	)
povolili	povolit	k5eAaPmAgMnP	povolit
ONC	ONC	kA	ONC
RPC	RPC	kA	RPC
přístup	přístup	k1gInSc4	přístup
ke	k	k7c3	k
správě	správa	k1gFnSc3	správa
změn	změna	k1gFnPc2	změna
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
inženýrských	inženýrský	k2eAgInPc2d1	inženýrský
standardů	standard	k1gInPc2	standard
od	od	k7c2	od
ISOC	ISOC	kA	ISOC
a	a	k8xC	a
Internet	Internet	k1gInSc1	Internet
Engineering	Engineering	k1gInSc1	Engineering
Task	Task	k1gInSc4	Task
Force	force	k1gFnSc2	force
(	(	kIx(	(
<g/>
IETF	IETF	kA	IETF
<g/>
)	)	kIx)	)
mohli	moct	k5eAaImAgMnP	moct
vydat	vydat	k5eAaPmF	vydat
dokumentace	dokumentace	k1gFnPc4	dokumentace
standardů	standard	k1gInPc2	standard
(	(	kIx(	(
<g/>
zvané	zvaný	k2eAgNnSc1d1	zvané
RFC	RFC	kA	RFC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
vztahovaly	vztahovat	k5eAaImAgFnP	vztahovat
k	k	k7c3	k
ONC	ONC	kA	ONC
RPC	RPC	kA	RPC
protokolům	protokol	k1gInPc3	protokol
a	a	k8xC	a
mohly	moct	k5eAaImAgFnP	moct
tak	tak	k6eAd1	tak
rozšířit	rozšířit	k5eAaPmF	rozšířit
ONC	ONC	kA	ONC
RPC	RPC	kA	RPC
<g/>
.	.	kIx.	.
</s>
<s>
OSF	OSF	kA	OSF
se	se	k3xPyFc4	se
pokusilo	pokusit	k5eAaPmAgNnS	pokusit
vytvořit	vytvořit	k5eAaPmF	vytvořit
DCE	DCE	kA	DCE
RPC	RPC	kA	RPC
jako	jako	k8xC	jako
IETF	IETF	kA	IETF
standard	standard	k1gInSc1	standard
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
nebyli	být	k5eNaImAgMnP	být
ochotni	ochoten	k2eAgMnPc1d1	ochoten
vzdát	vzdát	k5eAaPmF	vzdát
se	se	k3xPyFc4	se
správy	správa	k1gFnSc2	správa
změn	změna	k1gFnPc2	změna
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
IETF	IETF	kA	IETF
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
rozšířit	rozšířit	k5eAaPmF	rozšířit
ONC	ONC	kA	ONC
RPC	RPC	kA	RPC
přidáním	přidání	k1gNnSc7	přidání
nového	nový	k2eAgInSc2d1	nový
typu	typ	k1gInSc2	typ
autentifikace	autentifikace	k1gFnSc2	autentifikace
<g/>
,	,	kIx,	,
založeného	založený	k2eAgNnSc2d1	založené
na	na	k7c6	na
GSSAPI	GSSAPI	kA	GSSAPI
a	a	k8xC	a
RPCSEC	RPCSEC	kA	RPCSEC
GSS	GSS	kA	GSS
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
to	ten	k3xDgNnSc1	ten
souhlasilo	souhlasit	k5eAaImAgNnS	souhlasit
s	s	k7c7	s
požadavky	požadavek	k1gInPc7	požadavek
IETF	IETF	kA	IETF
na	na	k7c4	na
adekvátní	adekvátní	k2eAgFnSc4d1	adekvátní
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
protokolových	protokolový	k2eAgInPc2d1	protokolový
standardů	standard	k1gInPc2	standard
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
Sun	sun	k1gInSc1	sun
a	a	k8xC	a
ISOC	ISOC	kA	ISOC
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
podobné	podobný	k2eAgFnPc4d1	podobná
dohody	dohoda	k1gFnPc4	dohoda
o	o	k7c6	o
předání	předání	k1gNnSc6	předání
ISOC	ISOC	kA	ISOC
pravomocí	pravomoc	k1gFnSc7	pravomoc
ke	k	k7c3	k
správě	správa	k1gFnSc3	správa
změn	změna	k1gFnPc2	změna
u	u	k7c2	u
NFS	NFS	kA	NFS
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
smlouva	smlouva	k1gFnSc1	smlouva
byla	být	k5eAaImAgFnS	být
napsaná	napsaný	k2eAgFnSc1d1	napsaná
opatrně	opatrně	k6eAd1	opatrně
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
neobsahovala	obsahovat	k5eNaImAgFnS	obsahovat
NFS	NFS	kA	NFS
verzi	verze	k1gFnSc4	verze
2	[number]	k4	2
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgInSc2	ten
ISOC	ISOC	kA	ISOC
dostal	dostat	k5eAaPmAgMnS	dostat
pravomoci	pravomoc	k1gFnPc4	pravomoc
pro	pro	k7c4	pro
přidávání	přidávání	k1gNnSc4	přidávání
nových	nový	k2eAgFnPc2d1	nová
verzí	verze	k1gFnPc2	verze
do	do	k7c2	do
NFS	NFS	kA	NFS
protokolu	protokol	k1gInSc2	protokol
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
IETF	IETF	kA	IETF
bylo	být	k5eAaImAgNnS	být
schopno	schopen	k2eAgNnSc1d1	schopno
specifikovat	specifikovat	k5eAaBmF	specifikovat
NFS	NFS	kA	NFS
verzi	verze	k1gFnSc6	verze
4	[number]	k4	4
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Příchodem	příchod	k1gInSc7	příchod
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
nedosáhl	dosáhnout	k5eNaPmAgInS	dosáhnout
ani	ani	k8xC	ani
DFS	DFS	kA	DFS
ani	ani	k8xC	ani
AFS	AFS	kA	AFS
žádných	žádný	k3yNgInPc2	žádný
hlavních	hlavní	k2eAgInPc2d1	hlavní
komerčních	komerční	k2eAgInPc2d1	komerční
úspěchů	úspěch	k1gInPc2	úspěch
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
CIFS	CIFS	kA	CIFS
nebo	nebo	k8xC	nebo
NFS	NFS	kA	NFS
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
IBM	IBM	kA	IBM
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
předtím	předtím	k6eAd1	předtím
získala	získat	k5eAaPmAgFnS	získat
hlavního	hlavní	k2eAgMnSc4d1	hlavní
komerčního	komerční	k2eAgMnSc4d1	komerční
prodejce	prodejce	k1gMnSc4	prodejce
technologií	technologie	k1gFnPc2	technologie
DFS	DFS	kA	DFS
a	a	k8xC	a
AFS	AFS	kA	AFS
<g/>
,	,	kIx,	,
Transarc	Transarc	k1gFnSc1	Transarc
<g/>
,	,	kIx,	,
otevřela	otevřít	k5eAaPmAgFnS	otevřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
většinu	většina	k1gFnSc4	většina
zdrojového	zdrojový	k2eAgInSc2d1	zdrojový
kódu	kód	k1gInSc2	kód
AFS	AFS	kA	AFS
jako	jako	k8xS	jako
otevřený	otevřený	k2eAgInSc1d1	otevřený
software	software	k1gInSc1	software
(	(	kIx(	(
<g/>
open	open	k1gInSc1	open
source	source	k1gMnSc1	source
software	software	k1gInSc1	software
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yQnSc3	což
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
projekt	projekt	k1gInSc4	projekt
OpenAFS	OpenAFS	k1gMnPc2	OpenAFS
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
IBM	IBM	kA	IBM
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
konec	konec	k1gInSc4	konec
prodeje	prodej	k1gInSc2	prodej
technologií	technologie	k1gFnPc2	technologie
AFS	AFS	kA	AFS
a	a	k8xC	a
DFS	DFS	kA	DFS
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2010	[number]	k4	2010
Panasas	Panasas	k1gInSc1	Panasas
nabídnul	nabídnout	k5eAaPmAgInS	nabídnout
NFSv	NFSv	k1gInSc4	NFSv
<g/>
4.1	[number]	k4	4.1
<g/>
,	,	kIx,	,
založený	založený	k2eAgInSc1d1	založený
na	na	k7c4	na
jejich	jejich	k3xOp3gInSc4	jejich
paralelní	paralelní	k2eAgInSc4d1	paralelní
NFS	NFS	kA	NFS
(	(	kIx(	(
<g/>
pNFS	pNFS	k?	pNFS
<g/>
)	)	kIx)	)
technologii	technologie	k1gFnSc4	technologie
<g/>
,	,	kIx,	,
o	o	k7c4	o
které	který	k3yIgFnPc4	který
tvrdili	tvrdit	k5eAaImAgMnP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zlepší	zlepšit	k5eAaPmIp3nS	zlepšit
schopnost	schopnost	k1gFnSc4	schopnost
paralelního	paralelní	k2eAgInSc2d1	paralelní
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
datům	datum	k1gNnPc3	datum
<g/>
.	.	kIx.	.
</s>
<s>
Protokol	protokol	k1gInSc1	protokol
NFSv	NFSv	k1gInSc1	NFSv
<g/>
4.1	[number]	k4	4.1
definuje	definovat	k5eAaBmIp3nS	definovat
metodu	metoda	k1gFnSc4	metoda
separace	separace	k1gFnSc1	separace
systémových	systémový	k2eAgFnPc2d1	systémová
metadat	metadat	k5eAaPmF	metadat
od	od	k7c2	od
lokace	lokace	k1gFnSc2	lokace
souborových	souborový	k2eAgNnPc2d1	souborové
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
Liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
od	od	k7c2	od
tradičního	tradiční	k2eAgInSc2d1	tradiční
NFS	NFS	kA	NFS
serveru	server	k1gInSc2	server
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
uchovává	uchovávat	k5eAaImIp3nS	uchovávat
jména	jméno	k1gNnPc1	jméno
souborů	soubor	k1gInPc2	soubor
i	i	k8xC	i
jejich	jejich	k3xOp3gNnSc2	jejich
data	datum	k1gNnSc2	datum
u	u	k7c2	u
sebe	se	k3xPyFc2	se
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
separované	separovaný	k2eAgFnPc1d1	separovaná
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
produkty	produkt	k1gInPc1	produkt
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
více-uzlové	vícezlová	k1gFnSc2	více-uzlová
NFS	NFS	kA	NFS
servery	server	k1gInPc1	server
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
účast	účast	k1gFnSc1	účast
klienta	klient	k1gMnSc2	klient
v	v	k7c6	v
separaci	separace	k1gFnSc6	separace
metadat	metadat	k5eAaImF	metadat
a	a	k8xC	a
dat	datum	k1gNnPc2	datum
je	být	k5eAaImIp3nS	být
omezena	omezit	k5eAaPmNgFnS	omezit
<g/>
.	.	kIx.	.
</s>
<s>
Server	server	k1gInSc1	server
NFSv	NFSv	k1gInSc1	NFSv
<g/>
4.1	[number]	k4	4.1
pNFS	pNFS	k?	pNFS
je	být	k5eAaImIp3nS	být
kolekcí	kolekce	k1gFnSc7	kolekce
serverových	serverový	k2eAgInPc2d1	serverový
prostředků	prostředek	k1gInPc2	prostředek
nebo	nebo	k8xC	nebo
komponent	komponenta	k1gFnPc2	komponenta
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
ovládány	ovládat	k5eAaImNgInP	ovládat
serverem	server	k1gInSc7	server
metadat	metadat	k5eAaPmF	metadat
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
klient	klient	k1gMnSc1	klient
pNFS	pNFS	k?	pNFS
pořád	pořád	k6eAd1	pořád
přistupuje	přistupovat	k5eAaImIp3nS	přistupovat
k	k	k7c3	k
jedinému	jediné	k1gNnSc3	jediné
serveru	server	k1gInSc2	server
metadat	metadat	k5eAaImF	metadat
při	při	k7c6	při
interakci	interakce	k1gFnSc6	interakce
s	s	k7c7	s
daty	datum	k1gNnPc7	datum
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
stát	stát	k5eAaPmF	stát
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
přesouvá	přesouvat	k5eAaImIp3nS	přesouvat
data	datum	k1gNnPc4	datum
z	z	k7c2	z
a	a	k8xC	a
na	na	k7c4	na
server	server	k1gInSc4	server
<g/>
,	,	kIx,	,
přímo	přímo	k6eAd1	přímo
pracuje	pracovat	k5eAaImIp3nS	pracovat
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
data	datum	k1gNnSc2	datum
serverů	server	k1gInPc2	server
náležícím	náležící	k2eAgFnPc3d1	náležící
k	k	k7c3	k
pNFS	pNFS	k?	pNFS
kolekci	kolekce	k1gFnSc4	kolekce
serverů	server	k1gInPc2	server
<g/>
.	.	kIx.	.
</s>
<s>
Klientovi	klient	k1gMnSc3	klient
FNSv	FNSv	k1gInSc1	FNSv
<g/>
4.1	[number]	k4	4.1
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
umožněno	umožněn	k2eAgNnSc1d1	umožněno
zadávání	zadávání	k1gNnSc1	zadávání
přesné	přesný	k2eAgFnSc2d1	přesná
lokace	lokace	k1gFnSc2	lokace
souborových	souborový	k2eAgNnPc2d1	souborové
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
RFC	RFC	kA	RFC
1094	[number]	k4	1094
<g/>
:	:	kIx,	:
NFS	NFS	kA	NFS
<g/>
:	:	kIx,	:
Network	network	k1gInSc1	network
File	Fil	k1gInSc2	Fil
System	Systo	k1gNnSc7	Systo
Protocol	Protocol	k1gInSc1	Protocol
specification	specification	k1gInSc1	specification
<g/>
.	.	kIx.	.
</s>
<s>
RFC	RFC	kA	RFC
1813	[number]	k4	1813
<g/>
:	:	kIx,	:
NFS	NFS	kA	NFS
Version	Version	k1gInSc4	Version
3	[number]	k4	3
Protocol	Protocol	k1gInSc1	Protocol
Specification	Specification	k1gInSc4	Specification
<g/>
.	.	kIx.	.
</s>
<s>
RFC	RFC	kA	RFC
2224	[number]	k4	2224
<g/>
:	:	kIx,	:
NFS	NFS	kA	NFS
URL	URL	kA	URL
Scheme	Schem	k1gInSc5	Schem
<g/>
.	.	kIx.	.
</s>
<s>
RFC	RFC	kA	RFC
2339	[number]	k4	2339
<g/>
:	:	kIx,	:
An	An	k1gFnSc1	An
Agreement	Agreement	k1gMnSc1	Agreement
Between	Between	k1gInSc1	Between
the	the	k?	the
Internet	Internet	k1gInSc1	Internet
Society	societa	k1gFnSc2	societa
<g/>
,	,	kIx,	,
the	the	k?	the
IETF	IETF	kA	IETF
<g/>
,	,	kIx,	,
and	and	k?	and
Sun	Sun	kA	Sun
Microsystems	Microsystems	k1gInSc1	Microsystems
<g/>
,	,	kIx,	,
Inc	Inc	k1gFnSc1	Inc
<g/>
.	.	kIx.	.
in	in	k?	in
the	the	k?	the
matter	matter	k1gInSc1	matter
of	of	k?	of
NFS	NFS	kA	NFS
V	v	k7c4	v
<g/>
.4	.4	k4	.4
Protocols	Protocolsa	k1gFnPc2	Protocolsa
<g/>
.	.	kIx.	.
</s>
<s>
RFC	RFC	kA	RFC
2623	[number]	k4	2623
<g/>
:	:	kIx,	:
NFS	NFS	kA	NFS
Version	Version	k1gInSc4	Version
2	[number]	k4	2
and	and	k?	and
Version	Version	k1gInSc1	Version
3	[number]	k4	3
Security	Securita	k1gFnSc2	Securita
Issues	Issuesa	k1gFnPc2	Issuesa
and	and	k?	and
the	the	k?	the
NFS	NFS	kA	NFS
Protocol	Protocol	k1gInSc1	Protocol
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Use	usus	k1gInSc5	usus
of	of	k?	of
RPCSEC_GSS	RPCSEC_GSS	k1gMnSc3	RPCSEC_GSS
and	and	k?	and
Kerberos	Kerberos	k1gMnSc1	Kerberos
V	v	k7c6	v
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
RFC	RFC	kA	RFC
2624	[number]	k4	2624
<g/>
:	:	kIx,	:
NFS	NFS	kA	NFS
Version	Version	k1gInSc4	Version
4	[number]	k4	4
Design	design	k1gInSc1	design
Considerations	Considerations	k1gInSc4	Considerations
<g/>
.	.	kIx.	.
</s>
<s>
RFC	RFC	kA	RFC
3010	[number]	k4	3010
<g/>
:	:	kIx,	:
NFS	NFS	kA	NFS
version	version	k1gInSc4	version
4	[number]	k4	4
Protocol	Protocola	k1gFnPc2	Protocola
<g/>
.	.	kIx.	.
</s>
<s>
RFC	RFC	kA	RFC
3530	[number]	k4	3530
<g/>
:	:	kIx,	:
Network	network	k1gInSc1	network
File	Fil	k1gInSc2	Fil
System	Systo	k1gNnSc7	Systo
(	(	kIx(	(
<g/>
NFS	NFS	kA	NFS
<g/>
)	)	kIx)	)
version	version	k1gInSc1	version
4	[number]	k4	4
Protocol	Protocola	k1gFnPc2	Protocola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Network	network	k1gInSc1	network
File	Fil	k1gFnSc2	Fil
System	Syst	k1gInSc7	Syst
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Global	globat	k5eAaImAgInS	globat
File	File	k1gInSc1	File
System	Syst	k1gInSc7	Syst
Google	Google	k1gNnSc1	Google
File	Fil	k1gFnSc2	Fil
System	Syst	k1gInSc7	Syst
Linux	linux	k1gInSc4	linux
Dokumentační	dokumentační	k2eAgInSc1d1	dokumentační
Projekt	projekt	k1gInSc1	projekt
Příručka	příručka	k1gFnSc1	příručka
správce	správce	k1gMnSc2	správce
sítě	síť	k1gFnSc2	síť
-	-	kIx~	-
NFS	NFS	kA	NFS
</s>
