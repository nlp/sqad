<s>
Protokol	protokol	k1gInSc1	protokol
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
společností	společnost	k1gFnSc7	společnost
Sun	suna	k1gFnPc2	suna
Microsystems	Microsystemsa	k1gFnPc2	Microsystemsa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
má	mít	k5eAaImIp3nS	mít
jeho	jeho	k3xOp3gInSc1	jeho
další	další	k2eAgInSc1d1	další
vývoj	vývoj	k1gInSc1	vývoj
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
organizace	organizace	k1gFnSc1	organizace
Internet	Internet	k1gInSc4	Internet
Engineering	Engineering	k1gInSc1	Engineering
Task	Task	k1gInSc1	Task
Force	force	k1gFnSc1	force
(	(	kIx(	(
<g/>
IETF	IETF	kA	IETF
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
