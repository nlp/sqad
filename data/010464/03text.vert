<p>
<s>
Harrachov	Harrachov	k1gInSc1	Harrachov
(	(	kIx(	(
<g/>
něm.	něm.	k?	něm.
Harrachsdorf	Harrachsdorf	k1gInSc1	Harrachsdorf
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
a	a	k8xC	a
horské	horský	k2eAgNnSc1d1	horské
letovisko	letovisko	k1gNnSc1	letovisko
v	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
Čertovou	čertový	k2eAgFnSc7d1	Čertová
horou	hora	k1gFnSc7	hora
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
říčky	říčka	k1gFnSc2	říčka
Mumlavy	Mumlavy	k?	Mumlavy
<g/>
.	.	kIx.	.
</s>
<s>
Administrativně	administrativně	k6eAd1	administrativně
spadá	spadat	k5eAaImIp3nS	spadat
pod	pod	k7c4	pod
okres	okres	k1gInSc4	okres
Semily	Semily	k1gInPc1	Semily
v	v	k7c6	v
Libereckém	liberecký	k2eAgInSc6d1	liberecký
kraji	kraj	k1gInSc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
města	město	k1gNnSc2	město
přímo	přímo	k6eAd1	přímo
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
je	být	k5eAaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
silnicí	silnice	k1gFnSc7	silnice
I	i	k8xC	i
<g/>
/	/	kIx~	/
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
vlastního	vlastní	k2eAgInSc2d1	vlastní
Harrachova	Harrachov	k1gInSc2	Harrachov
<g/>
,	,	kIx,	,
Mýtin	mýtina	k1gFnPc2	mýtina
<g/>
,	,	kIx,	,
Nového	Nového	k2eAgInSc2d1	Nového
Světa	svět	k1gInSc2	svět
a	a	k8xC	a
Ryžoviště	ryžoviště	k1gNnSc2	ryžoviště
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
400	[number]	k4	400
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
dnešního	dnešní	k2eAgInSc2d1	dnešní
Harrachova	Harrachov	k1gInSc2	Harrachov
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
založena	založen	k2eAgFnSc1d1	založena
ves	ves	k1gFnSc1	ves
Dörfl	Dörflum	k1gNnPc2	Dörflum
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
byla	být	k5eAaImAgNnP	být
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
přejmenována	přejmenován	k2eAgFnSc1d1	přejmenována
podle	podle	k7c2	podle
majitelů	majitel	k1gMnPc2	majitel
místního	místní	k2eAgNnSc2d1	místní
panství	panství	k1gNnSc2	panství
na	na	k7c4	na
Harrachov	Harrachov	k1gInSc4	Harrachov
<g/>
.	.	kIx.	.
</s>
<s>
Harrachové	Harrach	k1gMnPc1	Harrach
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
český	český	k2eAgInSc4d1	český
šlechtický	šlechtický	k2eAgInSc4d1	šlechtický
rod	rod	k1gInSc4	rod
<g/>
,	,	kIx,	,
odvozují	odvozovat	k5eAaImIp3nP	odvozovat
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
od	od	k7c2	od
místa	místo	k1gNnSc2	místo
v	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
Čechách	Čechy	k1gFnPc6	Čechy
(	(	kIx(	(
<g/>
de	de	k?	de
Horach	Horach	k1gInSc1	Horach
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sklárny	sklárna	k1gFnSc2	sklárna
===	===	k?	===
</s>
</p>
<p>
<s>
Obživou	obživa	k1gFnSc7	obživa
místních	místní	k2eAgMnPc2d1	místní
obyvatel	obyvatel	k1gMnPc2	obyvatel
byla	být	k5eAaImAgFnS	být
tradiční	tradiční	k2eAgFnSc1d1	tradiční
sklářská	sklářský	k2eAgFnSc1d1	sklářská
výroba	výroba	k1gFnSc1	výroba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Harrachova	Harrachov	k1gInSc2	Harrachov
existuje	existovat	k5eAaImIp3nS	existovat
již	již	k6eAd1	již
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgFnSc1d1	dnešní
harrachovská	harrachovský	k2eAgFnSc1d1	Harrachovská
sklárna	sklárna	k1gFnSc1	sklárna
Novosad	Novosad	k1gMnSc1	Novosad
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1712	[number]	k4	1712
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
druhou	druhý	k4xOgFnSc7	druhý
nejstarší	starý	k2eAgFnSc7d3	nejstarší
sklárnou	sklárna	k1gFnSc7	sklárna
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
rozšíření	rozšíření	k1gNnSc3	rozšíření
výroby	výroba	k1gFnSc2	výroba
došlo	dojít	k5eAaPmAgNnS	dojít
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1719	[number]	k4	1719
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
hrabě	hrabě	k1gMnSc1	hrabě
Karel	Karel	k1gMnSc1	Karel
Antonín	Antonín	k1gMnSc1	Antonín
Václav	Václav	k1gMnSc1	Václav
Harrach	Harrach	k1gMnSc1	Harrach
na	na	k7c6	na
Rohrau	Rohraus	k1gInSc6	Rohraus
(	(	kIx(	(
<g/>
1699	[number]	k4	1699
<g/>
-	-	kIx~	-
<g/>
1768	[number]	k4	1768
<g/>
)	)	kIx)	)
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
Kateřinou	Kateřina	k1gFnSc7	Kateřina
Buquoyovou	Buquoyová	k1gFnSc7	Buquoyová
z	z	k7c2	z
panství	panství	k1gNnSc2	panství
Nové	Nové	k2eAgInPc4d1	Nové
Hrady	hrad	k1gInPc4	hrad
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
spojily	spojit	k5eAaPmAgFnP	spojit
dovednosti	dovednost	k1gFnPc1	dovednost
dvou	dva	k4xCgFnPc2	dva
sklářských	sklářský	k2eAgFnPc2d1	sklářská
hutí	huť	k1gFnPc2	huť
<g/>
.	.	kIx.	.
</s>
<s>
Barokní	barokní	k2eAgNnSc1d1	barokní
harrachovské	harrachovský	k2eAgNnSc1d1	harrachovské
sklo	sklo	k1gNnSc1	sklo
brzy	brzy	k6eAd1	brzy
dostihlo	dostihnout	k5eAaPmAgNnS	dostihnout
konkurenční	konkurenční	k2eAgInPc4d1	konkurenční
podniky	podnik	k1gInPc4	podnik
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
těžko	těžko	k6eAd1	těžko
rozeznatelné	rozeznatelný	k2eAgNnSc1d1	rozeznatelné
od	od	k7c2	od
jiných	jiný	k2eAgInPc2d1	jiný
výrobků	výrobek	k1gInPc2	výrobek
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Krkonoš	Krkonoše	k1gFnPc2	Krkonoše
(	(	kIx(	(
<g/>
Deštné	deštný	k2eAgFnSc2d1	Deštná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sousední	sousední	k2eAgInSc1d1	sousední
Nový	nový	k2eAgInSc1d1	nový
Svět	svět	k1gInSc1	svět
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
barevným	barevný	k2eAgNnSc7d1	barevné
broušeným	broušený	k2eAgNnSc7d1	broušené
a	a	k8xC	a
řezaným	řezaný	k2eAgNnSc7d1	řezané
sklem	sklo	k1gNnSc7	sklo
z	z	k7c2	z
Harrachova	Harrachov	k1gInSc2	Harrachov
však	však	k9	však
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
soupeřily	soupeřit	k5eAaImAgFnP	soupeřit
také	také	k9	také
výrobny	výrobna	k1gFnPc1	výrobna
v	v	k7c6	v
Kamenickém	kamenický	k2eAgInSc6d1	kamenický
Šenově	Šenov	k1gInSc6	Šenov
a	a	k8xC	a
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Boru	bor	k1gInSc6	bor
<g/>
.	.	kIx.	.
</s>
<s>
Zdejší	zdejší	k2eAgFnSc1d1	zdejší
sklárna	sklárna	k1gFnSc1	sklárna
patřila	patřit	k5eAaImAgFnS	patřit
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
rodu	rod	k1gInSc2	rod
Harrachů	Harrach	k1gInPc2	Harrach
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
ji	on	k3xPp3gFnSc4	on
převzal	převzít	k5eAaPmAgInS	převzít
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterého	který	k3yQgMnSc2	který
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
odkoupil	odkoupit	k5eAaPmAgMnS	odkoupit
soukromý	soukromý	k2eAgMnSc1d1	soukromý
majitel	majitel	k1gMnSc1	majitel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozsah	rozsah	k1gInSc1	rozsah
obce	obec	k1gFnSc2	obec
===	===	k?	===
</s>
</p>
<p>
<s>
Obec	obec	k1gFnSc1	obec
Harrachov	Harrachov	k1gInSc1	Harrachov
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgFnPc4	svůj
dnešní	dnešní	k2eAgFnSc1d1	dnešní
hranice	hranice	k1gFnSc1	hranice
od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
byly	být	k5eAaImAgFnP	být
připojeny	připojit	k5eAaPmNgFnP	připojit
Nový	nový	k2eAgInSc1d1	nový
Svět	svět	k1gInSc1	svět
<g/>
,	,	kIx,	,
Ryžoviště	ryžoviště	k1gNnPc1	ryžoviště
a	a	k8xC	a
(	(	kIx(	(
<g/>
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
mezistátní	mezistátní	k2eAgFnSc2d1	mezistátní
dohody	dohoda	k1gFnSc2	dohoda
uzavřené	uzavřený	k2eAgInPc4d1	uzavřený
před	před	k7c7	před
14	[number]	k4	14
<g/>
.	.	kIx.	.
únorem	únor	k1gInSc7	únor
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
také	také	k9	také
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
polské	polský	k2eAgNnSc1d1	polské
(	(	kIx(	(
<g/>
respektive	respektive	k9	respektive
slezské	slezský	k2eAgFnSc2d1	Slezská
<g/>
)	)	kIx)	)
Mýtiny	mýtina	k1gFnSc2	mýtina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výnosem	výnos	k1gInSc7	výnos
Krajského	krajský	k2eAgInSc2d1	krajský
národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
v	v	k7c6	v
Hradci	Hradec	k1gInSc6	Hradec
Králové	Králová	k1gFnSc2	Králová
č.	č.	k?	č.
89	[number]	k4	89
<g/>
/	/	kIx~	/
<g/>
65	[number]	k4	65
ze	z	k7c2	z
dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1965	[number]	k4	1965
byl	být	k5eAaImAgInS	být
Harrachov	Harrachov	k1gInSc1	Harrachov
v	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
podle	podle	k7c2	podle
§	§	k?	§
13	[number]	k4	13
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
65	[number]	k4	65
<g/>
/	/	kIx~	/
<g/>
1960	[number]	k4	1960
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
povýšen	povýšit	k5eAaPmNgInS	povýšit
na	na	k7c4	na
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
přípis	přípis	k1gInSc1	přípis
došel	dojít	k5eAaPmAgInS	dojít
do	do	k7c2	do
Harrachova	Harrachov	k1gInSc2	Harrachov
dne	den	k1gInSc2	den
6	[number]	k4	6
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
byla	být	k5eAaImAgFnS	být
provedena	provést	k5eAaPmNgFnS	provést
na	na	k7c4	na
žádost	žádost	k1gFnSc4	žádost
předsedy	předseda	k1gMnSc2	předseda
KNV	KNV	kA	KNV
Václava	Václav	k1gMnSc2	Václav
Hadravy	Hadrava	k1gFnSc2	Hadrava
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ji	on	k3xPp3gFnSc4	on
přednesl	přednést	k5eAaPmAgMnS	přednést
radě	rada	k1gFnSc3	rada
Krajského	krajský	k2eAgInSc2d1	krajský
národního	národní	k2eAgInSc2d1	národní
výboru	výbor	k1gInSc2	výbor
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Současnost	současnost	k1gFnSc4	současnost
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
komunismu	komunismus	k1gInSc2	komunismus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
v	v	k7c6	v
Harrachově	Harrachov	k1gInSc6	Harrachov
začal	začít	k5eAaPmAgInS	začít
prudce	prudko	k6eAd1	prudko
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
turistický	turistický	k2eAgInSc4d1	turistický
ruch	ruch	k1gInSc4	ruch
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
vystavěno	vystavět	k5eAaPmNgNnS	vystavět
a	a	k8xC	a
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
mnoho	mnoho	k6eAd1	mnoho
nových	nový	k2eAgNnPc2d1	nové
ubytovacích	ubytovací	k2eAgNnPc2d1	ubytovací
a	a	k8xC	a
stravovacích	stravovací	k2eAgNnPc2d1	stravovací
zařízení	zařízení	k1gNnPc2	zařízení
všeho	všecek	k3xTgInSc2	všecek
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgFnSc1d1	místní
sklárna	sklárna	k1gFnSc1	sklárna
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
současně	současně	k6eAd1	současně
také	také	k9	také
turistickou	turistický	k2eAgFnSc7d1	turistická
atrakcí	atrakce	k1gFnSc7	atrakce
–	–	k?	–
pořádají	pořádat	k5eAaImIp3nP	pořádat
se	se	k3xPyFc4	se
různé	různý	k2eAgFnPc1d1	různá
prohlídky	prohlídka	k1gFnPc1	prohlídka
ukazující	ukazující	k2eAgFnSc2d1	ukazující
metody	metoda	k1gFnSc2	metoda
výroby	výroba	k1gFnSc2	výroba
skla	sklo	k1gNnSc2	sklo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
areálu	areál	k1gInSc6	areál
sklárny	sklárna	k1gFnSc2	sklárna
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
také	také	k9	také
malý	malý	k2eAgInSc1d1	malý
pivovar	pivovar	k1gInSc1	pivovar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Lyžařské	lyžařský	k2eAgNnSc1d1	lyžařské
středisko	středisko	k1gNnSc1	středisko
==	==	k?	==
</s>
</p>
<p>
<s>
Zimní	zimní	k2eAgInPc4d1	zimní
sporty	sport	k1gInPc4	sport
se	se	k3xPyFc4	se
v	v	k7c6	v
Harrachově	Harrachov	k1gInSc6	Harrachov
začaly	začít	k5eAaPmAgFnP	začít
provozovat	provozovat	k5eAaImF	provozovat
brzy	brzy	k6eAd1	brzy
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
hrabě	hrabě	k1gMnSc1	hrabě
Jan	Jan	k1gMnSc1	Jan
Harrach	Harrach	k1gMnSc1	Harrach
přivezl	přivézt	k5eAaPmAgMnS	přivézt
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
první	první	k4xOgFnPc4	první
lyže	lyže	k1gFnPc4	lyže
(	(	kIx(	(
<g/>
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1908	[number]	k4	1908
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
"	"	kIx"	"
<g/>
Spolek	spolek	k1gInSc1	spolek
zimních	zimní	k2eAgInPc2d1	zimní
sportů	sport	k1gInPc2	sport
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
skokanské	skokanský	k2eAgInPc1d1	skokanský
můstky	můstek	k1gInPc1	můstek
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc3	který
Harrachov	Harrachov	k1gInSc1	Harrachov
tolik	tolik	k6eAd1	tolik
proslavily	proslavit	k5eAaPmAgFnP	proslavit
<g/>
,	,	kIx,	,
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1923	[number]	k4	1923
se	se	k3xPyFc4	se
v	v	k7c6	v
Harrachově	Harrachov	k1gInSc6	Harrachov
konaly	konat	k5eAaImAgInP	konat
první	první	k4xOgInSc4	první
mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
lyžařské	lyžařský	k2eAgInPc4d1	lyžařský
závody	závod	k1gInPc4	závod
<g/>
,	,	kIx,	,
kterých	který	k3yQgInPc2	který
se	se	k3xPyFc4	se
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
nejlepší	dobrý	k2eAgMnPc1d3	nejlepší
lyžaři	lyžař	k1gMnPc1	lyžař
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
zemí	zem	k1gFnPc2	zem
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
i	i	k8xC	i
z	z	k7c2	z
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
Mezinárodní	mezinárodní	k2eAgInPc1d1	mezinárodní
lyžařské	lyžařský	k2eAgInPc1d1	lyžařský
závody	závod	k1gInPc1	závod
již	již	k6eAd1	již
pravidelněji	pravidelně	k6eAd2	pravidelně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
byl	být	k5eAaImAgInS	být
skokanský	skokanský	k2eAgInSc1d1	skokanský
a	a	k8xC	a
běžecký	běžecký	k2eAgInSc1d1	běžecký
areál	areál	k1gInSc1	areál
zrekonstruován	zrekonstruovat	k5eAaPmNgInS	zrekonstruovat
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
lyžařský	lyžařský	k2eAgInSc1d1	lyžařský
můstek	můstek	k1gInSc1	můstek
–	–	k?	–
mamutí	mamutí	k2eAgInSc1d1	mamutí
můstek	můstek	k1gInSc1	můstek
K-	K-	k1gFnSc2	K-
<g/>
185	[number]	k4	185
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
zrekonstruována	zrekonstruovat	k5eAaPmNgFnS	zrekonstruovat
lanovka	lanovka	k1gFnSc1	lanovka
na	na	k7c4	na
Čertovu	čertův	k2eAgFnSc4d1	Čertova
horu	hora	k1gFnSc4	hora
<g/>
.	.	kIx.	.
</s>
<s>
Lanovka	lanovka	k1gFnSc1	lanovka
nabízí	nabízet	k5eAaImIp3nS	nabízet
130	[number]	k4	130
čtyřmístných	čtyřmístný	k2eAgFnPc2d1	čtyřmístná
sedaček	sedačka	k1gFnPc2	sedačka
a	a	k8xC	a
projížďku	projížďka	k1gFnSc4	projížďka
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
1303	[number]	k4	1303
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
urazí	urazit	k5eAaPmIp3nS	urazit
za	za	k7c4	za
8,7	[number]	k4	8,7
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dolní	dolní	k2eAgFnSc2d1	dolní
stanice	stanice	k1gFnSc2	stanice
(	(	kIx(	(
<g/>
665	[number]	k4	665
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
překonává	překonávat	k5eAaImIp3nS	překonávat
převýšení	převýšení	k1gNnSc4	převýšení
357	[number]	k4	357
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Přepravní	přepravní	k2eAgFnSc1d1	přepravní
kapacita	kapacita	k1gFnSc1	kapacita
je	být	k5eAaImIp3nS	být
1782	[number]	k4	1782
osob	osoba	k1gFnPc2	osoba
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejnovější	nový	k2eAgFnPc4d3	nejnovější
turistické	turistický	k2eAgFnPc4d1	turistická
atrakce	atrakce	k1gFnPc4	atrakce
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
bobová	bobový	k2eAgFnSc1d1	bobová
dráha	dráha	k1gFnSc1	dráha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Památky	památka	k1gFnPc1	památka
==	==	k?	==
</s>
</p>
<p>
<s>
klasicistní	klasicistní	k2eAgInSc1d1	klasicistní
kostel	kostel	k1gInSc1	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1822-8	[number]	k4	1822-8
</s>
</p>
<p>
<s>
kaple	kaple	k1gFnSc1	kaple
svaté	svatý	k2eAgFnSc2d1	svatá
Alžběty	Alžběta	k1gFnSc2	Alžběta
(	(	kIx(	(
<g/>
Harrachov	Harrachov	k1gInSc1	Harrachov
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
lyžařské	lyžařský	k2eAgNnSc1d1	lyžařské
muzeum	muzeum	k1gNnSc1	muzeum
</s>
</p>
<p>
<s>
Harrachovské	harrachovský	k2eAgInPc1d1	harrachovský
skokanské	skokanský	k2eAgInPc1d1	skokanský
můstky	můstek	k1gInPc1	můstek
(	(	kIx(	(
<g/>
technická	technický	k2eAgFnSc1d1	technická
památka	památka	k1gFnSc1	památka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Brusírna	brusírna	k1gFnSc1	brusírna
skla	sklo	k1gNnSc2	sklo
čp.	čp.	k?	čp.
73	[number]	k4	73
</s>
</p>
<p>
<s>
Pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
prof.	prof.	kA	prof.
MUDr.	MUDr.	kA	MUDr.
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Jedličky	Jedlička	k1gMnSc2	Jedlička
na	na	k7c6	na
budově	budova	k1gFnSc6	budova
sanatoria	sanatorium	k1gNnSc2	sanatorium
</s>
</p>
<p>
<s>
Usedlost	usedlost	k1gFnSc4	usedlost
Nový	nový	k2eAgInSc4d1	nový
Svět	svět	k1gInSc4	svět
čp.	čp.	k?	čp.
27	[number]	k4	27
</s>
</p>
<p>
<s>
Sklárna	sklárna	k1gFnSc1	sklárna
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
čp.	čp.	k?	čp.
46	[number]	k4	46
<g/>
,	,	kIx,	,
95	[number]	k4	95
<g/>
,	,	kIx,	,
96	[number]	k4	96
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
==	==	k?	==
</s>
</p>
<p>
<s>
Harrachov	Harrachov	k1gInSc1	Harrachov
leží	ležet	k5eAaImIp3nS	ležet
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
34	[number]	k4	34
km	km	kA	km
východně	východně	k6eAd1	východně
od	od	k7c2	od
Liberce	Liberec	k1gInSc2	Liberec
(	(	kIx(	(
<g/>
kraj	kraj	k1gInSc1	kraj
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
86	[number]	k4	86
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Hradce	Hradec	k1gInSc2	Hradec
Králové	Králová	k1gFnSc2	Králová
</s>
</p>
<p>
<s>
33	[number]	k4	33
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
Semil	Semily	k1gInPc2	Semily
(	(	kIx(	(
<g/>
okres	okres	k1gInSc1	okres
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
123	[number]	k4	123
km	km	kA	km
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
Prahy	Praha	k1gFnSc2	Praha
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Harrachovskou	harrachovský	k2eAgFnSc7d1	Harrachovská
částí	část	k1gFnSc7	část
Nový	nový	k2eAgInSc1d1	nový
Svět	svět	k1gInSc1	svět
prochází	procházet	k5eAaImIp3nS	procházet
významná	významný	k2eAgFnSc1d1	významná
a	a	k8xC	a
frekventovaná	frekventovaný	k2eAgFnSc1d1	frekventovaná
silnice	silnice	k1gFnSc1	silnice
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
č.	č.	k?	č.
10	[number]	k4	10
Praha-Turnov-Harrachov	Praha-Turnov-Harrachovo	k1gNnPc2	Praha-Turnov-Harrachovo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
do	do	k7c2	do
Turnova	Turnov	k1gInSc2	Turnov
dálnicí	dálnice	k1gFnSc7	dálnice
D	D	kA	D
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lesích	les	k1gInPc6	les
<g/>
,	,	kIx,	,
3	[number]	k4	3
km	km	kA	km
severně	severně	k6eAd1	severně
nad	nad	k7c7	nad
Harrachovem	Harrachov	k1gInSc7	Harrachov
<g/>
,	,	kIx,	,
najdeme	najít	k5eAaPmIp1nP	najít
důležitý	důležitý	k2eAgInSc4d1	důležitý
hraniční	hraniční	k2eAgInSc4d1	hraniční
přechod	přechod	k1gInSc4	přechod
Harrachov-Jakuszyce	Harrachov-Jakuszyce	k1gFnSc2	Harrachov-Jakuszyce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lokalitě	lokalita	k1gFnSc6	lokalita
"	"	kIx"	"
<g/>
Na	na	k7c6	na
Mýtě	mýto	k1gNnSc6	mýto
<g/>
"	"	kIx"	"
při	při	k7c6	při
soutoku	soutok	k1gInSc6	soutok
Jizery	Jizera	k1gFnSc2	Jizera
a	a	k8xC	a
Mumlavy	Mumlavy	k?	Mumlavy
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
silnice	silnice	k1gFnSc1	silnice
křižuje	křižovat	k5eAaImIp3nS	křižovat
se	s	k7c7	s
silnicí	silnice	k1gFnSc7	silnice
I.	I.	kA	I.
třídy	třída	k1gFnSc2	třída
č.	č.	k?	č.
14	[number]	k4	14
v	v	k7c6	v
trase	trasa	k1gFnSc6	trasa
Liberec	Liberec	k1gInSc1	Liberec
–	–	k?	–
Jablonec	Jablonec	k1gInSc1	Jablonec
nad	nad	k7c7	nad
Nisou	Nisa	k1gFnSc7	Nisa
–	–	k?	–
Tanvald	Tanvald	k1gMnSc1	Tanvald
–	–	k?	–
Rokytnice	Rokytnice	k1gFnSc1	Rokytnice
nad	nad	k7c7	nad
Jizerou	Jizera	k1gFnSc7	Jizera
–	–	k?	–
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
–	–	k?	–
Trutnov	Trutnov	k1gInSc1	Trutnov
–	–	k?	–
Úpice	Úpice	k1gFnSc2	Úpice
–	–	k?	–
Náchod	Náchod	k1gInSc1	Náchod
–	–	k?	–
Nové	Nové	k2eAgNnSc1d1	Nové
Město	město	k1gNnSc1	město
nad	nad	k7c7	nad
Metují	Metuje	k1gFnSc7	Metuje
–	–	k?	–
Dobruška	Dobruška	k1gFnSc1	Dobruška
–	–	k?	–
Rychnov	Rychnov	k1gInSc1	Rychnov
nad	nad	k7c7	nad
Kněžnou	kněžna	k1gFnSc7	kněžna
–	–	k?	–
Vamberk	Vamberk	k1gInSc4	Vamberk
–	–	k?	–
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
–	–	k?	–
Česká	český	k2eAgFnSc1d1	Česká
Třebová	Třebová	k1gFnSc1	Třebová
–	–	k?	–
Anenská	anenský	k2eAgFnSc1d1	Anenská
Studánka	studánka	k1gFnSc1	studánka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Železniční	železniční	k2eAgFnSc4d1	železniční
dopravu	doprava	k1gFnSc4	doprava
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
železnice	železnice	k1gFnSc1	železnice
Tanvald-Harrachov	Tanvald-Harrachovo	k1gNnPc2	Tanvald-Harrachovo
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
ozubnicová	ozubnicový	k2eAgFnSc1d1	ozubnicová
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
vážně	vážně	k6eAd1	vážně
hrozilo	hrozit	k5eAaImAgNnS	hrozit
zrušení	zrušení	k1gNnSc1	zrušení
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2010	[number]	k4	2010
byla	být	k5eAaImAgFnS	být
znovuzprovozněna	znovuzprovozněn	k2eAgFnSc1d1	znovuzprovozněn
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
do	do	k7c2	do
železniční	železniční	k2eAgFnSc2d1	železniční
stanice	stanice	k1gFnSc2	stanice
Szklarska	Szklarsek	k1gMnSc2	Szklarsek
Poręba	Poręb	k1gMnSc2	Poręb
Górna	Górn	k1gMnSc2	Górn
<g/>
,	,	kIx,	,
prvního	první	k4xOgMnSc2	první
pravidelně	pravidelně	k6eAd1	pravidelně
provozovaného	provozovaný	k2eAgInSc2d1	provozovaný
tarifního	tarifní	k2eAgInSc2d1	tarifní
bodu	bod	k1gInSc2	bod
na	na	k7c6	na
území	území	k1gNnSc6	území
Polské	polský	k2eAgFnSc2d1	polská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
takto	takto	k6eAd1	takto
zprovozněném	zprovozněný	k2eAgInSc6d1	zprovozněný
úseku	úsek	k1gInSc6	úsek
železniční	železniční	k2eAgFnSc2d1	železniční
tratě	trať	k1gFnSc2	trať
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jedna	jeden	k4xCgFnSc1	jeden
mezilehlá	mezilehlý	k2eAgFnSc1d1	mezilehlá
železniční	železniční	k2eAgFnSc1d1	železniční
stanice	stanice	k1gFnSc1	stanice
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
dvoukolejná	dvoukolejný	k2eAgFnSc1d1	dvoukolejná
výhybna	výhybna	k1gFnSc1	výhybna
Jakuszyce	Jakuszyce	k1gFnSc2	Jakuszyce
jakožto	jakožto	k8xS	jakožto
nejvýše	nejvýše	k6eAd1	nejvýše
položená	položený	k2eAgFnSc1d1	položená
železniční	železniční	k2eAgFnSc1d1	železniční
stanice	stanice	k1gFnSc1	stanice
na	na	k7c6	na
území	území	k1gNnSc6	území
Polské	polský	k2eAgFnSc2d1	polská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
rovněž	rovněž	k9	rovněž
železniční	železniční	k2eAgFnSc1d1	železniční
zastávka	zastávka	k1gFnSc1	zastávka
Szklarska	Szklarsek	k1gMnSc2	Szklarsek
Poręba	Poręb	k1gMnSc2	Poręb
Huta	Hutus	k1gMnSc2	Hutus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hromadnou	hromadný	k2eAgFnSc4d1	hromadná
dopravu	doprava	k1gFnSc4	doprava
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
především	především	k9	především
autobusy	autobus	k1gInPc1	autobus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
harrachovském	harrachovský	k2eAgNnSc6d1	harrachovské
autobusovém	autobusový	k2eAgNnSc6d1	autobusové
nádraží	nádraží	k1gNnSc6	nádraží
končí	končit	k5eAaImIp3nP	končit
svou	svůj	k3xOyFgFnSc4	svůj
trasu	trasa	k1gFnSc4	trasa
mnoho	mnoho	k6eAd1	mnoho
dálkových	dálkový	k2eAgFnPc2d1	dálková
i	i	k8xC	i
místních	místní	k2eAgFnPc2d1	místní
linek	linka	k1gFnPc2	linka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
vlakového	vlakový	k2eAgNnSc2d1	vlakové
nádraží	nádraží	k1gNnSc2	nádraží
(	(	kIx(	(
<g/>
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
lesích	les	k1gInPc6	les
<g/>
,	,	kIx,	,
skoro	skoro	k6eAd1	skoro
4	[number]	k4	4
km	km	kA	km
od	od	k7c2	od
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
Nový	nový	k2eAgInSc4d1	nový
Svět	svět	k1gInSc4	svět
a	a	k8xC	a
centrum	centrum	k1gNnSc4	centrum
Harrachova	Harrachov	k1gInSc2	Harrachov
do	do	k7c2	do
Rýžoviště	rýžoviště	k1gNnSc2	rýžoviště
jezdí	jezdit	k5eAaImIp3nP	jezdit
v	v	k7c6	v
letní	letní	k2eAgFnSc6d1	letní
a	a	k8xC	a
v	v	k7c6	v
zimní	zimní	k2eAgFnSc6d1	zimní
sezóně	sezóna	k1gFnSc6	sezóna
městská	městský	k2eAgFnSc1d1	městská
autobusová	autobusový	k2eAgFnSc1d1	autobusová
linka	linka	k1gFnSc1	linka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Szklarska	Szklarska	k1gFnSc1	Szklarska
Poręba	Poręba	k1gFnSc1	Poręba
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Harrachov	Harrachov	k1gInSc1	Harrachov
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Harrachov	Harrachov	k1gInSc1	Harrachov
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Ze	z	k7c2	z
života	život	k1gInSc2	život
lidu	lid	k1gInSc2	lid
v	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Harrachov	Harrachov	k1gInSc1	Harrachov
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Průvodce	průvodce	k1gMnSc1	průvodce
Harrachov	Harrachov	k1gInSc1	Harrachov
ve	v	k7c6	v
Wikicestách	Wikicesta	k1gFnPc6	Wikicesta
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Harrachov	Harrachov	k1gInSc1	Harrachov
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
města	město	k1gNnSc2	město
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgInSc1d1	oficiální
turistický	turistický	k2eAgInSc1d1	turistický
portál	portál	k1gInSc1	portál
města	město	k1gNnSc2	město
Harrachov	Harrachov	k1gInSc1	Harrachov
</s>
</p>
