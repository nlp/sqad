<s>
Kostarika	Kostarika	k1gFnSc1	Kostarika
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
Costa	Cost	k1gMnSc2	Cost
Rica	Ricus	k1gMnSc2	Ricus
nebo	nebo	k8xC	nebo
plným	plný	k2eAgInSc7d1	plný
názvem	název	k1gInSc7	název
República	Repúblicus	k1gMnSc2	Repúblicus
de	de	k?	de
Costa	Cost	k1gMnSc2	Cost
Rica	Ricus	k1gMnSc2	Ricus
<g/>
,	,	kIx,	,
český	český	k2eAgInSc4d1	český
doslovný	doslovný	k2eAgInSc4d1	doslovný
překlad	překlad	k1gInSc4	překlad
Bohaté	bohatý	k2eAgNnSc4d1	bohaté
pobřeží	pobřeží	k1gNnSc4	pobřeží
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
republika	republika	k1gFnSc1	republika
ve	v	k7c6	v
Střední	střední	k2eAgFnSc6d1	střední
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Sousedními	sousední	k2eAgInPc7d1	sousední
státy	stát	k1gInPc7	stát
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Nikaragua	Nikaragua	k1gFnSc1	Nikaragua
(	(	kIx(	(
<g/>
délka	délka	k1gFnSc1	délka
hranice	hranice	k1gFnSc1	hranice
300	[number]	k4	300
km	km	kA	km
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Panama	panama	k2eAgInSc6d1	panama
(	(	kIx(	(
<g/>
363	[number]	k4	363
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgNnSc4d1	západní
pobřeží	pobřeží	k1gNnSc4	pobřeží
Kostariky	Kostarika	k1gFnSc2	Kostarika
omývají	omývat	k5eAaImIp3nP	omývat
vody	voda	k1gFnPc1	voda
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
východní	východní	k2eAgFnSc1d1	východní
Karibské	karibský	k2eAgNnSc4d1	Karibské
moře	moře	k1gNnSc4	moře
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
délka	délka	k1gFnSc1	délka
kostarického	kostarický	k2eAgNnSc2d1	kostarické
pobřeží	pobřeží	k1gNnSc2	pobřeží
je	být	k5eAaImIp3nS	být
1	[number]	k4	1
466	[number]	k4	466
km	km	kA	km
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
212	[number]	k4	212
km	km	kA	km
na	na	k7c4	na
Karibiku	Karibika	k1gFnSc4	Karibika
a	a	k8xC	a
1	[number]	k4	1
254	[number]	k4	254
km	km	kA	km
na	na	k7c6	na
Pacifiku	Pacifik	k1gInSc6	Pacifik
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
bylo	být	k5eAaImAgNnS	být
území	území	k1gNnSc1	území
Kostariky	Kostarika	k1gFnSc2	Kostarika
prozkoumáno	prozkoumat	k5eAaPmNgNnS	prozkoumat
španělskými	španělský	k2eAgMnPc7d1	španělský
conquistadory	conquistador	k1gMnPc7	conquistador
a	a	k8xC	a
objeviteli	objevitel	k1gMnPc7	objevitel
v	v	k7c6	v
raném	raný	k2eAgNnSc6d1	rané
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
kolonizace	kolonizace	k1gFnSc1	kolonizace
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1563	[number]	k4	1563
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
španělské	španělský	k2eAgFnSc2d1	španělská
nadvlády	nadvláda	k1gFnSc2	nadvláda
byla	být	k5eAaImAgFnS	být
Kostarika	Kostarika	k1gFnSc1	Kostarika
součástí	součást	k1gFnSc7	součást
guatemalského	guatemalský	k2eAgInSc2d1	guatemalský
generálního	generální	k2eAgInSc2d1	generální
kapitanátu	kapitanát	k1gInSc2	kapitanát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
Kostarika	Kostarika	k1gFnSc1	Kostarika
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ostatními	ostatní	k2eAgInPc7d1	ostatní
státy	stát	k1gInPc7	stát
sjednocena	sjednotit	k5eAaPmNgFnS	sjednotit
v	v	k7c4	v
Unii	unie	k1gFnSc4	unie
středoamerických	středoamerický	k2eAgInPc2d1	středoamerický
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgInPc4	který
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1821	[number]	k4	1821
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1838	[number]	k4	1838
samostatnost	samostatnost	k1gFnSc4	samostatnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
politické	politický	k2eAgFnSc2d1	politická
nestability	nestabilita	k1gFnSc2	nestabilita
a	a	k8xC	a
nejistoty	nejistota	k1gFnSc2	nejistota
<g/>
,	,	kIx,	,
či	či	k8xC	či
vojenských	vojenský	k2eAgFnPc2d1	vojenská
vlád	vláda	k1gFnPc2	vláda
a	a	k8xC	a
občanských	občanský	k2eAgFnPc2d1	občanská
válek	válka	k1gFnPc2	válka
ve	v	k7c6	v
středoamerickém	středoamerický	k2eAgInSc6d1	středoamerický
regionu	region	k1gInSc6	region
během	během	k7c2	během
2	[number]	k4	2
<g/>
.	.	kIx.	.
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
Kostarika	Kostarika	k1gFnSc1	Kostarika
světlá	světlý	k2eAgFnSc1d1	světlá
výjimka	výjimka	k1gFnSc1	výjimka
demokratické	demokratický	k2eAgFnSc2d1	demokratická
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zákona	zákon	k1gInSc2	zákon
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
dáno	dát	k5eAaPmNgNnS	dát
<g/>
,	,	kIx,	,
že	že	k8xS	že
každý	každý	k3xTgMnSc1	každý
občan	občan	k1gMnSc1	občan
(	(	kIx(	(
<g/>
muž	muž	k1gMnSc1	muž
či	či	k8xC	či
žena	žena	k1gFnSc1	žena
<g/>
)	)	kIx)	)
starší	starý	k2eAgMnPc1d2	starší
18	[number]	k4	18
let	léto	k1gNnPc2	léto
musí	muset	k5eAaImIp3nS	muset
jít	jít	k5eAaImF	jít
k	k	k7c3	k
volbám	volba	k1gFnPc3	volba
a	a	k8xC	a
jejich	jejich	k3xOp3gMnPc3	jejich
hlas	hlas	k1gInSc1	hlas
skutečně	skutečně	k6eAd1	skutečně
rozhoduje	rozhodovat	k5eAaImIp3nS	rozhodovat
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
strana	strana	k1gFnSc1	strana
bude	být	k5eAaImBp3nS	být
vládnout	vládnout	k5eAaImF	vládnout
<g/>
.	.	kIx.	.
</s>
<s>
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
vláda	vláda	k1gFnSc1	vláda
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
moci	moc	k1gFnSc2	moc
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1902	[number]	k4	1902
a	a	k8xC	a
jediná	jediný	k2eAgFnSc1d1	jediná
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
<g/>
.	.	kIx.	.
</s>
<s>
Trvala	trvat	k5eAaImAgFnS	trvat
pouhé	pouhý	k2eAgInPc4d1	pouhý
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
a	a	k8xC	a
jejím	její	k3xOp3gInSc7	její
důsledkem	důsledek	k1gInSc7	důsledek
bylo	být	k5eAaImAgNnS	být
rozpuštění	rozpuštění	k1gNnSc1	rozpuštění
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
následované	následovaný	k2eAgNnSc1d1	následované
změnou	změna	k1gFnSc7	změna
ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
roku	rok	k1gInSc2	rok
1949	[number]	k4	1949
zakázala	zakázat	k5eAaPmAgFnS	zakázat
vytvořit	vytvořit	k5eAaPmF	vytvořit
státu	stát	k1gInSc2	stát
armádu	armáda	k1gFnSc4	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
situace	situace	k1gFnSc1	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
stát	stát	k1gInSc1	stát
nemá	mít	k5eNaImIp3nS	mít
stálou	stálý	k2eAgFnSc4d1	stálá
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
trvá	trvat	k5eAaImIp3nS	trvat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Páteří	páteř	k1gFnSc7	páteř
země	zem	k1gFnSc2	zem
jsou	být	k5eAaImIp3nP	být
tři	tři	k4xCgNnPc1	tři
pohoří	pohoří	k1gNnPc1	pohoří
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
táhnou	táhnout	k5eAaImIp3nP	táhnout
souvisle	souvisle	k6eAd1	souvisle
od	od	k7c2	od
severozápadu	severozápad	k1gInSc2	severozápad
k	k	k7c3	k
jihovýchodu	jihovýchod	k1gInSc3	jihovýchod
celým	celý	k2eAgInSc7d1	celý
územím	území	k1gNnSc7	území
státu	stát	k1gInSc2	stát
<g/>
;	;	kIx,	;
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
Cordillera	Cordiller	k1gMnSc2	Cordiller
de	de	k?	de
Guanacaste	Guanacast	k1gMnSc5	Guanacast
<g/>
,	,	kIx,	,
Cordillera	Cordiller	k1gMnSc2	Cordiller
Central	Central	k1gMnSc2	Central
a	a	k8xC	a
Cordillera	Cordiller	k1gMnSc2	Cordiller
de	de	k?	de
Talamanca	Talamancus	k1gMnSc2	Talamancus
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
Kostariky	Kostarika	k1gFnSc2	Kostarika
je	být	k5eAaImIp3nS	být
3	[number]	k4	3
820	[number]	k4	820
metrů	metr	k1gInPc2	metr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
hora	hora	k1gFnSc1	hora
Chirripó	Chirripó	k1gFnSc2	Chirripó
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
Cordillera	Cordiller	k1gMnSc2	Cordiller
de	de	k?	de
Talamanca	Talamancus	k1gMnSc2	Talamancus
<g/>
.	.	kIx.	.
</s>
<s>
Pohoří	pohořet	k5eAaPmIp3nP	pohořet
tak	tak	k8xC	tak
oddělují	oddělovat	k5eAaImIp3nP	oddělovat
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
dvě	dva	k4xCgFnPc1	dva
nížiny	nížina	k1gFnPc1	nížina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
pánevní	pánevní	k2eAgFnSc1d1	pánevní
oblast	oblast	k1gFnSc1	oblast
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Centrální	centrální	k2eAgNnSc4d1	centrální
údolí	údolí	k1gNnSc4	údolí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
většina	většina	k1gFnSc1	většina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
nížin	nížina	k1gFnPc2	nížina
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
oceánu	oceán	k1gInSc2	oceán
je	být	k5eAaImIp3nS	být
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
mokřinami	mokřina	k1gFnPc7	mokřina
a	a	k8xC	a
bažinami	bažina	k1gFnPc7	bažina
<g/>
.	.	kIx.	.
</s>
<s>
Karibské	karibský	k2eAgNnSc1d1	Karibské
pobřeží	pobřeží	k1gNnSc1	pobřeží
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
málo	málo	k6eAd1	málo
členité	členitý	k2eAgInPc1d1	členitý
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
pacifické	pacifický	k2eAgNnSc1d1	pacifické
pobřeží	pobřeží	k1gNnSc1	pobřeží
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
několika	několik	k4yIc7	několik
většími	veliký	k2eAgInPc7d2	veliký
poloostrovy	poloostrov	k1gInPc7	poloostrov
a	a	k8xC	a
zálivy	záliv	k1gInPc7	záliv
-	-	kIx~	-
např.	např.	kA	např.
poloostrov	poloostrov	k1gInSc4	poloostrov
Nicoya	Nicoy	k1gInSc2	Nicoy
a	a	k8xC	a
stejnojmenný	stejnojmenný	k2eAgInSc4d1	stejnojmenný
záliv	záliv	k1gInSc4	záliv
Nicoya	Nicoy	k1gInSc2	Nicoy
nebo	nebo	k8xC	nebo
poloostrov	poloostrov	k1gInSc1	poloostrov
Osa	osa	k1gFnSc1	osa
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
Kostariky	Kostarika	k1gFnSc2	Kostarika
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
51	[number]	k4	51
<g/>
%	%	kIx~	%
pokryto	pokrýt	k5eAaPmNgNnS	pokrýt
lesními	lesní	k2eAgInPc7d1	lesní
porosty	porost	k1gInPc7	porost
<g/>
,	,	kIx,	,
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
půda	půda	k1gFnSc1	půda
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
37	[number]	k4	37
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
existovalo	existovat	k5eAaImAgNnS	existovat
28	[number]	k4	28
kostarických	kostarický	k2eAgInPc2d1	kostarický
národních	národní	k2eAgInPc2d1	národní
parků	park	k1gInPc2	park
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
pokrývaly	pokrývat	k5eAaImAgInP	pokrývat
téměř	téměř	k6eAd1	téměř
630000	[number]	k4	630000
ha	ha	kA	ha
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
12	[number]	k4	12
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Kostarika	Kostarika	k1gFnSc1	Kostarika
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
první	první	k4xOgFnSc7	první
středoamerickou	středoamerický	k2eAgFnSc7d1	středoamerická
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
začala	začít	k5eAaPmAgFnS	začít
vyvážet	vyvážet	k5eAaImF	vyvážet
do	do	k7c2	do
světa	svět	k1gInSc2	svět
banány	banán	k1gInPc4	banán
a	a	k8xC	a
kávu	káva	k1gFnSc4	káva
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
dodnes	dodnes	k6eAd1	dodnes
táhne	táhnout	k5eAaImIp3nS	táhnout
kostarické	kostarický	k2eAgNnSc1d1	kostarické
hospodářství	hospodářství	k1gNnSc1	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Vhodnou	vhodný	k2eAgFnSc4d1	vhodná
situaci	situace	k1gFnSc4	situace
nastartovala	nastartovat	k5eAaPmAgFnS	nastartovat
osvícená	osvícený	k2eAgFnSc1d1	osvícená
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nabídla	nabídnout	k5eAaPmAgFnS	nabídnout
bezplatně	bezplatně	k6eAd1	bezplatně
půdu	půda	k1gFnSc4	půda
všem	všecek	k3xTgMnPc3	všecek
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
zavázali	zavázat	k5eAaPmAgMnP	zavázat
pěstovat	pěstovat	k5eAaImF	pěstovat
kávu	káva	k1gFnSc4	káva
na	na	k7c4	na
vývoz	vývoz	k1gInSc4	vývoz
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
paradoxně	paradoxně	k6eAd1	paradoxně
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
přineslo	přinést	k5eAaPmAgNnS	přinést
zemi	zem	k1gFnSc4	zem
stabilitu	stabilita	k1gFnSc4	stabilita
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
zde	zde	k6eAd1	zde
stejná	stejný	k2eAgFnSc1d1	stejná
situace	situace	k1gFnSc1	situace
jako	jako	k8xC	jako
v	v	k7c6	v
okolních	okolní	k2eAgFnPc6d1	okolní
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
většinu	většina	k1gFnSc4	většina
půdy	půda	k1gFnSc2	půda
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
pár	pár	k4xCyI	pár
nejbohatších	bohatý	k2eAgMnPc2d3	nejbohatší
statkářů	statkář	k1gMnPc2	statkář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
však	však	k9	však
byla	být	k5eAaImAgFnS	být
země	země	k1gFnSc1	země
zasažena	zasáhnout	k5eAaPmNgFnS	zasáhnout
ekonomickou	ekonomický	k2eAgFnSc7d1	ekonomická
krizí	krize	k1gFnSc7	krize
spojenou	spojený	k2eAgFnSc4d1	spojená
s	s	k7c7	s
poklesem	pokles	k1gInSc7	pokles
světových	světový	k2eAgFnPc2d1	světová
cen	cena	k1gFnPc2	cena
kávy	káva	k1gFnSc2	káva
a	a	k8xC	a
banánu	banán	k1gInSc2	banán
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
následovala	následovat	k5eAaImAgFnS	následovat
vysoká	vysoký	k2eAgFnSc1d1	vysoká
inflace	inflace	k1gFnSc1	inflace
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
ještě	ještě	k9	ještě
více	hodně	k6eAd2	hodně
podlomila	podlomit	k5eAaPmAgFnS	podlomit
hospodářství	hospodářství	k1gNnSc4	hospodářství
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
úsporný	úsporný	k2eAgInSc4d1	úsporný
program	program	k1gInSc4	program
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
však	však	k9	však
nepřinesl	přinést	k5eNaPmAgMnS	přinést
plánované	plánovaný	k2eAgInPc4d1	plánovaný
výsledky	výsledek	k1gInPc4	výsledek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
přes	přes	k7c4	přes
to	ten	k3xDgNnSc4	ten
se	se	k3xPyFc4	se
hospodářství	hospodářství	k1gNnSc2	hospodářství
dostalo	dostat	k5eAaPmAgNnS	dostat
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
do	do	k7c2	do
6	[number]	k4	6
<g/>
%	%	kIx~	%
růstu	růst	k1gInSc2	růst
<g/>
.	.	kIx.	.
</s>
<s>
Bohužel	bohužel	k9	bohužel
pro	pro	k7c4	pro
Kostariku	Kostarika	k1gFnSc4	Kostarika
neměl	mít	k5eNaImAgInS	mít
tento	tento	k3xDgInSc1	tento
výsledek	výsledek	k1gInSc1	výsledek
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
trvání	trvání	k1gNnSc2	trvání
a	a	k8xC	a
následoval	následovat	k5eAaImAgInS	následovat
opětovný	opětovný	k2eAgInSc4d1	opětovný
útlum	útlum	k1gInSc4	útlum
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
podpořen	podpořit	k5eAaPmNgInS	podpořit
vysokými	vysoký	k2eAgInPc7d1	vysoký
veřejnými	veřejný	k2eAgInPc7d1	veřejný
výdaji	výdaj	k1gInPc7	výdaj
<g/>
.	.	kIx.	.
</s>
<s>
Kostarika	Kostarika	k1gFnSc1	Kostarika
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
DR-CAFTA	DR-CAFTA	k1gFnSc2	DR-CAFTA
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgInPc1d1	významný
exportní	exportní	k2eAgInPc1d1	exportní
výrobky	výrobek	k1gInPc1	výrobek
jsou	být	k5eAaImIp3nP	být
produkty	produkt	k1gInPc4	produkt
strojního	strojní	k2eAgInSc2d1	strojní
průmyslu	průmysl	k1gInSc2	průmysl
(	(	kIx(	(
<g/>
kancelářské	kancelářský	k2eAgInPc1d1	kancelářský
stroje	stroj	k1gInPc1	stroj
<g/>
,	,	kIx,	,
čerpadla	čerpadlo	k1gNnPc1	čerpadlo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zdravotnický	zdravotnický	k2eAgInSc1d1	zdravotnický
materiál	materiál	k1gInSc1	materiál
a	a	k8xC	a
medikamenty	medikament	k1gInPc1	medikament
<g/>
,	,	kIx,	,
banány	banán	k1gInPc1	banán
<g/>
,	,	kIx,	,
káva	káva	k1gFnSc1	káva
<g/>
,	,	kIx,	,
tropické	tropický	k2eAgNnSc4d1	tropické
ovoce	ovoce	k1gNnSc4	ovoce
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
má	mít	k5eAaImIp3nS	mít
velká	velký	k2eAgNnPc4d1	velké
ložiska	ložisko	k1gNnPc4	ložisko
bauxitu	bauxit	k1gInSc2	bauxit
<g/>
,	,	kIx,	,
síry	síra	k1gFnSc2	síra
a	a	k8xC	a
rud	ruda	k1gFnPc2	ruda
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
zemi	zem	k1gFnSc4	zem
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
pozitivní	pozitivní	k2eAgFnPc4d1	pozitivní
vyhlídky	vyhlídka	k1gFnPc4	vyhlídka
na	na	k7c4	na
opětovný	opětovný	k2eAgInSc4d1	opětovný
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
růst	růst	k1gInSc4	růst
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešních	dnešní	k2eAgInPc6d1	dnešní
dnech	den	k1gInPc6	den
probíhá	probíhat	k5eAaImIp3nS	probíhat
na	na	k7c6	na
území	území	k1gNnSc6	území
Kostariky	Kostarika	k1gFnSc2	Kostarika
pouze	pouze	k6eAd1	pouze
těžba	těžba	k1gFnSc1	těžba
zlata	zlato	k1gNnSc2	zlato
a	a	k8xC	a
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Horský	horský	k2eAgInSc4d1	horský
ráz	ráz	k1gInSc4	ráz
krajiny	krajina	k1gFnSc2	krajina
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
příhodné	příhodný	k2eAgFnSc2d1	příhodná
podmínky	podmínka	k1gFnSc2	podmínka
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
hydroelektráren	hydroelektrárna	k1gFnPc2	hydroelektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
energeticky	energeticky	k6eAd1	energeticky
soběstačná	soběstačný	k2eAgFnSc1d1	soběstačná
<g/>
.	.	kIx.	.
</s>
<s>
Horská	horský	k2eAgFnSc1d1	horská
krajina	krajina	k1gFnSc1	krajina
také	také	k9	také
láká	lákat	k5eAaImIp3nS	lákat
turisty	turist	k1gMnPc4	turist
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
začínají	začínat	k5eAaImIp3nP	začínat
stávat	stávat	k5eAaImF	stávat
důležitou	důležitý	k2eAgFnSc7d1	důležitá
složkou	složka	k1gFnSc7	složka
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Příjmy	příjem	k1gInPc1	příjem
z	z	k7c2	z
turismu	turismus	k1gInSc2	turismus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
představovaly	představovat	k5eAaImAgFnP	představovat
8,5	[number]	k4	8,5
<g/>
%	%	kIx~	%
celkového	celkový	k2eAgNnSc2d1	celkové
státního	státní	k2eAgNnSc2d1	státní
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Provincie	provincie	k1gFnSc2	provincie
Kostariky	Kostarika	k1gFnSc2	Kostarika
<g/>
.	.	kIx.	.
</s>
<s>
Kostarika	Kostarika	k1gFnSc1	Kostarika
je	být	k5eAaImIp3nS	být
unitární	unitární	k2eAgInSc4d1	unitární
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
7	[number]	k4	7
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
81	[number]	k4	81
kantonů	kanton	k1gInPc2	kanton
a	a	k8xC	a
473	[number]	k4	473
distriktů	distrikt	k1gInPc2	distrikt
<g/>
.	.	kIx.	.
</s>
<s>
Kostarika	Kostarika	k1gFnSc1	Kostarika
se	se	k3xPyFc4	se
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
od	od	k7c2	od
jiných	jiný	k2eAgFnPc2d1	jiná
zemí	zem	k1gFnPc2	zem
svým	svůj	k3xOyFgInSc7	svůj
jasným	jasný	k2eAgInSc7d1	jasný
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	Tom	k1gMnSc3	Tom
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaPmIp3nS	věnovat
rovnou	rovnou	k6eAd1	rovnou
14	[number]	k4	14
článků	článek	k1gInPc2	článek
Ústavy	ústava	k1gFnSc2	ústava
<g/>
.	.	kIx.	.
</s>
<s>
Podpora	podpora	k1gFnSc1	podpora
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
a	a	k8xC	a
vytváření	vytváření	k1gNnSc2	vytváření
příznivých	příznivý	k2eAgFnPc2d1	příznivá
podmínek	podmínka	k1gFnPc2	podmínka
pro	pro	k7c4	pro
vzdělávání	vzdělávání	k1gNnSc4	vzdělávání
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
vnímáno	vnímat	k5eAaImNgNnS	vnímat
již	již	k6eAd1	již
od	od	k7c2	od
získání	získání	k1gNnSc2	získání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1821	[number]	k4	1821
jako	jako	k8xC	jako
první	první	k4xOgFnSc4	první
povinnost	povinnost	k1gFnSc4	povinnost
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Historici	historik	k1gMnPc1	historik
hovoří	hovořit	k5eAaImIp3nP	hovořit
o	o	k7c4	o
Estado	Estada	k1gFnSc5	Estada
educador	educadora	k1gFnPc2	educadora
<g/>
.	.	kIx.	.
</s>
<s>
Vzdělávací	vzdělávací	k2eAgFnSc1d1	vzdělávací
složka	složka	k1gFnSc1	složka
tvoří	tvořit	k5eAaImIp3nS	tvořit
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
od	od	k7c2	od
20	[number]	k4	20
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
%	%	kIx~	%
veřejného	veřejný	k2eAgInSc2d1	veřejný
rozpočtu	rozpočet	k1gInSc2	rozpočet
<g/>
.	.	kIx.	.
</s>
<s>
Kostarika	Kostarika	k1gFnSc1	Kostarika
je	být	k5eAaImIp3nS	být
zapojena	zapojit	k5eAaPmNgFnS	zapojit
do	do	k7c2	do
několika	několik	k4yIc2	několik
projektů	projekt	k1gInPc2	projekt
a	a	k8xC	a
organizací	organizace	k1gFnPc2	organizace
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
latinskoamerické	latinskoamerický	k2eAgFnSc2d1	latinskoamerická
integrace	integrace	k1gFnSc2	integrace
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členským	členský	k2eAgInSc7d1	členský
státem	stát	k1gInSc7	stát
např.	např.	kA	např.
organizací	organizace	k1gFnSc7	organizace
Společenství	společenství	k1gNnSc1	společenství
latinskoamerických	latinskoamerický	k2eAgInPc2d1	latinskoamerický
a	a	k8xC	a
karibských	karibský	k2eAgInPc2d1	karibský
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
Pacifická	pacifický	k2eAgFnSc1d1	Pacifická
aliance	aliance	k1gFnSc1	aliance
<g/>
,	,	kIx,	,
Latinskoamerický	latinskoamerický	k2eAgInSc1d1	latinskoamerický
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
Středoamerický	středoamerický	k2eAgInSc1d1	středoamerický
integrační	integrační	k2eAgInSc1d1	integrační
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
Sdružení	sdružení	k1gNnSc1	sdružení
karibských	karibský	k2eAgInPc2d1	karibský
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
