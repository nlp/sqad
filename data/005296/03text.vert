<s>
Pozitron	pozitron	k1gInSc1	pozitron
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
antielektron	antielektron	k1gInSc1	antielektron
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
subatomární	subatomární	k2eAgFnSc1d1	subatomární
částice	částice	k1gFnSc1	částice
-	-	kIx~	-
antičástice	antičástice	k1gFnSc1	antičástice
elektronu	elektron	k1gInSc2	elektron
<g/>
.	.	kIx.	.
</s>
<s>
symbol	symbol	k1gInSc1	symbol
<g/>
:	:	kIx,	:
e	e	k0	e
<g/>
+	+	kIx~	+
klidová	klidový	k2eAgFnSc1d1	klidová
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
:	:	kIx,	:
m	m	kA	m
<g/>
0	[number]	k4	0
=	=	kIx~	=
511	[number]	k4	511
keV	keV	k?	keV
<g/>
/	/	kIx~	/
<g/>
c	c	k0	c
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
9,109	[number]	k4	9,109
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
31	[number]	k4	31
kg	kg	kA	kg
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
shodná	shodný	k2eAgFnSc1d1	shodná
s	s	k7c7	s
elektronem	elektron	k1gInSc7	elektron
s	s	k7c7	s
relativní	relativní	k2eAgFnSc7d1	relativní
nejistotou	nejistota	k1gFnSc7	nejistota
<	<	kIx(	<
8	[number]	k4	8
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
9	[number]	k4	9
</s>
<s>
elektrický	elektrický	k2eAgInSc4d1	elektrický
náboj	náboj	k1gInSc4	náboj
<g/>
:	:	kIx,	:
q	q	k?	q
=	=	kIx~	=
+	+	kIx~	+
e	e	k0	e
=	=	kIx~	=
+	+	kIx~	+
1,602	[number]	k4	1,602
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
19	[number]	k4	19
C	C	kA	C
(	(	kIx(	(
<g/>
kladný	kladný	k2eAgInSc4d1	kladný
elementární	elementární	k2eAgInSc4d1	elementární
náboj	náboj	k1gInSc4	náboj
<g/>
)	)	kIx)	)
</s>
<s>
spin	spin	k1gInSc4	spin
<g/>
:	:	kIx,	:
1⁄	1⁄	k?	1⁄
<g/>
,	,	kIx,	,
pozitron	pozitron	k1gInSc1	pozitron
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
fermion	fermion	k1gInSc1	fermion
antičástice	antičástice	k1gFnSc2	antičástice
<g/>
:	:	kIx,	:
elektron	elektron	k1gInSc1	elektron
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
stabilní	stabilní	k2eAgFnSc6d1	stabilní
částici	částice	k1gFnSc6	částice
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
elementární	elementární	k2eAgFnSc4d1	elementární
částici	částice	k1gFnSc4	částice
(	(	kIx(	(
<g/>
lepton	lepton	k1gInSc4	lepton
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozitron	pozitron	k1gInSc1	pozitron
je	být	k5eAaImIp3nS	být
řazen	řadit	k5eAaImNgInS	řadit
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
1	[number]	k4	1
<g/>
.	.	kIx.	.
generace	generace	k1gFnSc1	generace
leptonů	lepton	k1gInPc2	lepton
<g/>
.	.	kIx.	.
</s>
<s>
Existenci	existence	k1gFnSc4	existence
pozitronu	pozitron	k1gInSc2	pozitron
poprvé	poprvé	k6eAd1	poprvé
předpověděl	předpovědět	k5eAaPmAgMnS	předpovědět
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
Paul	Paul	k1gMnSc1	Paul
Dirac	Dirac	k1gFnSc4	Dirac
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
pozitron	pozitron	k1gInSc1	pozitron
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
Carl	Carl	k1gMnSc1	Carl
D.	D.	kA	D.
Anderson	Anderson	k1gMnSc1	Anderson
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
mu	on	k3xPp3gMnSc3	on
dal	dát	k5eAaPmAgInS	dát
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Anderson	Anderson	k1gMnSc1	Anderson
také	také	k9	také
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
přejmenovat	přejmenovat	k5eAaPmF	přejmenovat
elektron	elektron	k1gInSc4	elektron
na	na	k7c4	na
negatron	negatron	k1gInSc4	negatron
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
neujalo	ujmout	k5eNaPmAgNnS	ujmout
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
pozorování	pozorování	k1gNnSc3	pozorování
využil	využít	k5eAaPmAgInS	využít
kosmické	kosmický	k2eAgNnSc4d1	kosmické
záření	záření	k1gNnSc4	záření
<g/>
,	,	kIx,	,
dopadající	dopadající	k2eAgInSc4d1	dopadající
do	do	k7c2	do
mlžné	mlžný	k2eAgFnSc2d1	mlžná
komory	komora	k1gFnSc2	komora
<g/>
.	.	kIx.	.
</s>
<s>
Stopu	stopa	k1gFnSc4	stopa
pozitronu	pozitron	k1gInSc2	pozitron
objevil	objevit	k5eAaPmAgInS	objevit
na	na	k7c6	na
jednom	jeden	k4xCgInSc6	jeden
ze	z	k7c2	z
13000	[number]	k4	13000
snímků	snímek	k1gInPc2	snímek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
při	při	k7c6	při
pozorování	pozorování	k1gNnSc6	pozorování
zhotovil	zhotovit	k5eAaPmAgMnS	zhotovit
<g/>
.	.	kIx.	.
</s>
<s>
Pozitron	pozitron	k1gInSc1	pozitron
byl	být	k5eAaImAgInS	být
patrně	patrně	k6eAd1	patrně
pozorován	pozorovat	k5eAaImNgInS	pozorovat
už	už	k6eAd1	už
dříve	dříve	k6eAd2	dříve
F.	F.	kA	F.
Joliot-Curiem	Joliot-Curium	k1gNnSc7	Joliot-Curium
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
jej	on	k3xPp3gMnSc4	on
ale	ale	k8xC	ale
na	na	k7c6	na
snímcích	snímek	k1gInPc6	snímek
chybně	chybně	k6eAd1	chybně
vyhodnotil	vyhodnotit	k5eAaPmAgInS	vyhodnotit
jako	jako	k9	jako
elektron	elektron	k1gInSc1	elektron
<g/>
,	,	kIx,	,
letící	letící	k2eAgInSc1d1	letící
nezvyklým	zvyklý	k2eNgInSc7d1	nezvyklý
směrem	směr	k1gInSc7	směr
(	(	kIx(	(
<g/>
pozitrony	pozitron	k1gInPc1	pozitron
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
aparatuře	aparatura	k1gFnSc6	aparatura
opsaly	opsat	k5eAaPmAgFnP	opsat
uzavřený	uzavřený	k2eAgInSc4d1	uzavřený
kruh	kruh	k1gInSc4	kruh
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
z	z	k7c2	z
fotografií	fotografia	k1gFnPc2	fotografia
usoudil	usoudit	k5eAaPmAgMnS	usoudit
<g/>
,	,	kIx,	,
že	že	k8xS	že
letí	letět	k5eAaImIp3nS	letět
opačným	opačný	k2eAgInSc7d1	opačný
směrem	směr	k1gInSc7	směr
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
odpovídalo	odpovídat	k5eAaImAgNnS	odpovídat
elektronům	elektron	k1gInPc3	elektron
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozitrony	pozitron	k1gInPc1	pozitron
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
v	v	k7c6	v
pozemské	pozemský	k2eAgFnSc6d1	pozemská
přírodě	příroda	k1gFnSc6	příroda
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
vznikají	vznikat	k5eAaImIp3nP	vznikat
při	při	k7c6	při
řadě	řada	k1gFnSc6	řada
procesů	proces	k1gInPc2	proces
<g/>
,	,	kIx,	,
krátce	krátce	k6eAd1	krátce
(	(	kIx(	(
<g/>
za	za	k7c4	za
cca	cca	kA	cca
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
s	s	k7c7	s
<g/>
)	)	kIx)	)
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
vzniku	vznik	k1gInSc6	vznik
zaniknou	zaniknout	k5eAaPmIp3nP	zaniknout
při	při	k7c6	při
anihilaci	anihilace	k1gFnSc6	anihilace
s	s	k7c7	s
elektrony	elektron	k1gInPc7	elektron
běžné	běžný	k2eAgFnSc2d1	běžná
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Pozitrony	pozitron	k1gInPc1	pozitron
vznikají	vznikat	k5eAaImIp3nP	vznikat
při	při	k7c6	při
β	β	k?	β
<g/>
+	+	kIx~	+
radioaktivním	radioaktivní	k2eAgInSc6d1	radioaktivní
rozpadu	rozpad	k1gInSc6	rozpad
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
interakcí	interakce	k1gFnSc7	interakce
hmoty	hmota	k1gFnSc2	hmota
s	s	k7c7	s
fotony	foton	k1gInPc7	foton
γ	γ	k?	γ
s	s	k7c7	s
energií	energie	k1gFnSc7	energie
nad	nad	k7c4	nad
1,022	[number]	k4	1,022
MeV	MeV	k1gFnPc2	MeV
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
procesu	proces	k1gInSc6	proces
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
pár	pár	k4xCyI	pár
elektron-pozitron	elektronozitron	k1gInSc1	elektron-pozitron
<g/>
.	.	kIx.	.
</s>
<s>
Pozitrony	pozitron	k1gInPc1	pozitron
jsou	být	k5eAaImIp3nP	být
také	také	k6eAd1	také
složkou	složka	k1gFnSc7	složka
kosmického	kosmický	k2eAgNnSc2d1	kosmické
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
vznikají	vznikat	k5eAaImIp3nP	vznikat
při	při	k7c6	při
rozpadech	rozpad	k1gInPc6	rozpad
mionů	mion	k1gInPc2	mion
a	a	k8xC	a
pionů	pion	k1gInPc2	pion
<g/>
,	,	kIx,	,
vzniklých	vzniklý	k2eAgInPc2d1	vzniklý
při	při	k7c6	při
interakcích	interakce	k1gFnPc6	interakce
jiných	jiný	k2eAgFnPc2d1	jiná
částic	částice	k1gFnPc2	částice
s	s	k7c7	s
atmosférou	atmosféra	k1gFnSc7	atmosféra
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgNnSc1d3	nejznámější
užití	užití	k1gNnSc1	užití
pozitronu	pozitron	k1gInSc2	pozitron
v	v	k7c6	v
beletrii	beletrie	k1gFnSc6	beletrie
je	být	k5eAaImIp3nS	být
Pozitronický	Pozitronický	k2eAgInSc1d1	Pozitronický
mozek	mozek	k1gInSc1	mozek
robotů	robot	k1gInPc2	robot
v	v	k7c6	v
dílech	díl	k1gInPc6	díl
Isaaca	Isaaca	k1gFnSc1	Isaaca
Asimova	Asimův	k2eAgFnSc1d1	Asimova
<g/>
.	.	kIx.	.
</s>
<s>
Pozitrony	pozitron	k1gInPc4	pozitron
zvolil	zvolit	k5eAaPmAgInS	zvolit
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
novinku	novinka	k1gFnSc4	novinka
<g/>
,	,	kIx,	,
když	když	k8xS	když
začal	začít	k5eAaPmAgMnS	začít
psát	psát	k5eAaImF	psát
sci-fi	scii	k1gFnSc4	sci-fi
<g/>
.	.	kIx.	.
</s>
<s>
Možná	možná	k9	možná
jako	jako	k9	jako
hold	hold	k1gInSc1	hold
Asimovovi	Asimova	k1gMnSc3	Asimova
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
Star	star	k1gFnSc2	star
Treku	Trek	k1gInSc2	Trek
android	android	k1gInSc1	android
Dat	datum	k1gNnPc2	datum
(	(	kIx(	(
<g/>
jeho	jeho	k3xOp3gMnSc1	jeho
"	"	kIx"	"
<g/>
bratr	bratr	k1gMnSc1	bratr
<g/>
"	"	kIx"	"
Lore	Lore	k1gFnSc1	Lore
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
podobní	podobný	k2eAgMnPc1d1	podobný
androidi	android	k1gMnPc1	android
soongovského	soongovský	k2eAgInSc2d1	soongovský
typu	typ	k1gInSc2	typ
<g/>
)	)	kIx)	)
pozitronický	pozitronický	k2eAgInSc1d1	pozitronický
mozek	mozek	k1gInSc1	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Elektron	elektron	k1gInSc1	elektron
Neutrino	neutrino	k1gNnSc1	neutrino
Elektromagnetismus	elektromagnetismus	k1gInSc4	elektromagnetismus
Leptonové	Leptonový	k2eAgNnSc4d1	Leptonové
číslo	číslo	k1gNnSc4	číslo
</s>
