<p>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
stanice	stanice	k1gFnSc1	stanice
Ben	Ben	k1gInSc4	Ben
Gurionovo	Gurionův	k2eAgNnSc4d1	Gurionovo
letiště	letiště	k1gNnSc4	letiště
(	(	kIx(	(
<g/>
hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
:	:	kIx,	:
ת	ת	k?	ת
ה	ה	k?	ה
נ	נ	k?	נ
ה	ה	k?	ה
ב	ב	k?	ב
<g/>
,	,	kIx,	,
tachanat	tachanat	k5eAaImF	tachanat
ha-rakevet	haakevet	k1gInSc4	ha-rakevet
Nemal	málit	k5eNaImRp2nS	málit
ha-te	ha	k1gMnSc5	ha-t
<g/>
'	'	kIx"	'
<g/>
ufa	ufa	k?	ufa
Ben	Ben	k1gInSc1	Ben
Gurion	Gurion	k1gInSc1	Gurion
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
železniční	železniční	k2eAgFnSc1d1	železniční
stanice	stanice	k1gFnSc1	stanice
na	na	k7c6	na
železniční	železniční	k2eAgFnSc6d1	železniční
trati	trať	k1gFnSc6	trať
Tel	tel	kA	tel
Aviv	Aviv	k1gInSc1	Aviv
–	–	k?	–
Modi	Mod	k1gFnSc2	Mod
<g/>
'	'	kIx"	'
<g/>
in	in	k?	in
v	v	k7c6	v
Izraeli	Izrael	k1gInSc6	Izrael
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
Izraele	Izrael	k1gInSc2	Izrael
v	v	k7c6	v
pobřežní	pobřežní	k2eAgFnSc6d1	pobřežní
nížině	nížina	k1gFnSc6	nížina
<g/>
,	,	kIx,	,
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
cca	cca	kA	cca
40	[number]	k4	40
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
situována	situovat	k5eAaBmNgFnS	situovat
na	na	k7c4	na
východní	východní	k2eAgInSc4d1	východní
okraj	okraj	k1gInSc4	okraj
aglomerace	aglomerace	k1gFnSc2	aglomerace
Tel	tel	kA	tel
Avivu	Aviv	k1gInSc2	Aviv
<g/>
,	,	kIx,	,
do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
Ben	Ben	k1gInSc4	Ben
Gurionova	Gurionův	k2eAgNnSc2d1	Gurionovo
mezinárodního	mezinárodní	k2eAgNnSc2d1	mezinárodní
letiště	letiště	k1gNnSc2	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
začleněna	začlenit	k5eAaPmNgFnS	začlenit
do	do	k7c2	do
nových	nový	k2eAgFnPc2d1	nová
odbavovacích	odbavovací	k2eAgFnPc2d1	odbavovací
hal	hala	k1gFnPc2	hala
letiště	letiště	k1gNnSc2	letiště
dokončených	dokončený	k2eAgInPc2d1	dokončený
počátkem	počátkem	k7c2	počátkem
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
silniční	silniční	k2eAgFnSc4d1	silniční
síť	síť	k1gFnSc4	síť
je	být	k5eAaImIp3nS	být
napojena	napojit	k5eAaPmNgFnS	napojit
pomocí	pomocí	k7c2	pomocí
dálnice	dálnice	k1gFnSc2	dálnice
číslo	číslo	k1gNnSc1	číslo
1	[number]	k4	1
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
prochází	procházet	k5eAaImIp3nS	procházet
okolo	okolo	k7c2	okolo
letiště	letiště	k1gNnSc2	letiště
<g/>
.	.	kIx.	.
<g/>
Stanice	stanice	k1gFnSc1	stanice
byla	být	k5eAaImAgFnS	být
otevřena	otevřen	k2eAgFnSc1d1	otevřena
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
jako	jako	k8xC	jako
tehdy	tehdy	k6eAd1	tehdy
první	první	k4xOgFnPc4	první
stanice	stanice	k1gFnPc4	stanice
nové	nový	k2eAgFnSc2d1	nová
železniční	železniční	k2eAgFnSc2d1	železniční
trati	trať	k1gFnSc2	trať
z	z	k7c2	z
Tel	tel	kA	tel
Avivu	Aviv	k1gInSc2	Aviv
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
starší	starý	k2eAgFnSc2d2	starší
trati	trať	k1gFnSc2	trať
vedoucí	vedoucí	k2eAgFnSc6d1	vedoucí
z	z	k7c2	z
Tel	tel	kA	tel
Avivu	Aviva	k1gFnSc4	Aviva
do	do	k7c2	do
města	město	k1gNnSc2	město
Lod	Lod	k1gFnSc2	Lod
se	se	k3xPyFc4	se
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
nové	nový	k2eAgNnSc1d1	nové
kolejové	kolejový	k2eAgNnSc1d1	kolejové
těleso	těleso	k1gNnSc1	těleso
poblíž	poblíž	k7c2	poblíž
dálniční	dálniční	k2eAgFnSc2d1	dálniční
křižovatky	křižovatka	k1gFnSc2	křižovatka
Šapirim	Šapirima	k1gFnPc2	Šapirima
u	u	k7c2	u
dálnice	dálnice	k1gFnSc2	dálnice
číslo	číslo	k1gNnSc1	číslo
1	[number]	k4	1
nedaleko	nedaleko	k7c2	nedaleko
města	město	k1gNnSc2	město
Bejt	Bejt	k?	Bejt
Dagan	Dagan	k1gMnSc1	Dagan
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
okraji	okraj	k1gInSc6	okraj
areálu	areál	k1gInSc2	areál
letiště	letiště	k1gNnSc2	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
obsluhována	obsluhovat	k5eAaImNgFnS	obsluhovat
autobusovými	autobusový	k2eAgFnPc7d1	autobusová
linkami	linka	k1gFnPc7	linka
společnosti	společnost	k1gFnSc2	společnost
Egged	Egged	k1gInSc1	Egged
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
tu	tu	k6eAd1	tu
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
parkovací	parkovací	k2eAgNnSc4d1	parkovací
místa	místo	k1gNnPc4	místo
pro	pro	k7c4	pro
automobily	automobil	k1gInPc4	automobil
<g/>
,	,	kIx,	,
prodejní	prodejní	k2eAgInPc4d1	prodejní
stánky	stánek	k1gInPc4	stánek
a	a	k8xC	a
obchody	obchod	k1gInPc4	obchod
<g/>
,	,	kIx,	,
nápojové	nápojový	k2eAgInPc4d1	nápojový
automaty	automat	k1gInPc4	automat
<g/>
,	,	kIx,	,
bankomat	bankomat	k1gInSc4	bankomat
a	a	k8xC	a
veřejný	veřejný	k2eAgInSc4d1	veřejný
telefon	telefon	k1gInSc4	telefon
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Izraelské	izraelský	k2eAgFnPc1d1	izraelská
dráhy	dráha	k1gFnPc1	dráha
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Železniční	železniční	k2eAgFnSc2d1	železniční
stanice	stanice	k1gFnSc2	stanice
Ben	Ben	k1gInSc4	Ben
Gurionovo	Gurionův	k2eAgNnSc4d1	Gurionovo
letiště	letiště	k1gNnSc4	letiště
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
