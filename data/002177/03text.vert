<s>
Santa	Santa	k1gMnSc1	Santa
Fe	Fe	k1gMnSc1	Fe
(	(	kIx(	(
<g/>
originální	originální	k2eAgInSc1d1	originální
název	název	k1gInSc1	název
<g/>
:	:	kIx,	:
La	la	k1gNnSc1	la
Villa	Vill	k1gMnSc2	Vill
Real	Real	k1gInSc1	Real
de	de	k?	de
la	la	k1gNnSc1	la
Santa	Santa	k1gMnSc1	Santa
Fé	Fé	k1gMnSc1	Fé
de	de	k?	de
San	San	k1gMnSc1	San
Francisco	Francisco	k1gMnSc1	Francisco
de	de	k?	de
Asís	Asís	k1gInSc1	Asís
<g/>
,	,	kIx,	,
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
<g/>
:	:	kIx,	:
Královské	královský	k2eAgNnSc1d1	královské
město	město	k1gNnSc1	město
svaté	svatý	k2eAgFnSc2d1	svatá
víry	víra	k1gFnSc2	víra
svatého	svatý	k2eAgMnSc2d1	svatý
Františka	František	k1gMnSc2	František
z	z	k7c2	z
Assisi	Assis	k1gMnSc5	Assis
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Nového	Nového	k2eAgNnSc2d1	Nového
Mexika	Mexiko	k1gNnSc2	Mexiko
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
oficiálního	oficiální	k2eAgNnSc2d1	oficiální
založení	založení	k1gNnSc2	založení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1610	[number]	k4	1610
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
Santa	Sant	k1gMnSc4	Sant
Fe	Fe	k1gMnSc4	Fe
hlavní	hlavní	k2eAgFnSc4d1	hlavní
městem	město	k1gNnSc7	město
provincie	provincie	k1gFnSc2	provincie
Nové	Nové	k2eAgNnSc1d1	Nové
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
,	,	kIx,	,
spadající	spadající	k2eAgMnSc1d1	spadající
nejprve	nejprve	k6eAd1	nejprve
pod	pod	k7c4	pod
Místokrálovství	Místokrálovství	k1gNnSc4	Místokrálovství
Nové	Nové	k2eAgNnSc1d1	Nové
Španělsko	Španělsko	k1gNnSc1	Španělsko
a	a	k8xC	a
poté	poté	k6eAd1	poté
pod	pod	k7c4	pod
Mexiko	Mexiko	k1gNnSc4	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Američané	Američan	k1gMnPc1	Američan
je	on	k3xPp3gFnPc4	on
vojensky	vojensky	k6eAd1	vojensky
obsadili	obsadit	k5eAaPmAgMnP	obsadit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1846	[number]	k4	1846
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1850	[number]	k4	1850
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
městem	město	k1gNnSc7	město
teritoria	teritorium	k1gNnSc2	teritorium
Nové	Nové	k2eAgNnSc4d1	Nové
Mexiko	Mexiko	k1gNnSc4	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
formálním	formální	k2eAgNnSc7d1	formální
centrem	centrum	k1gNnSc7	centrum
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
arcidiecéze	arcidiecéze	k1gFnSc2	arcidiecéze
Santa	Santa	k1gMnSc1	Santa
Fe	Fe	k1gMnSc1	Fe
(	(	kIx(	(
<g/>
mezi	mezi	k7c4	mezi
význačné	význačný	k2eAgFnPc4d1	význačná
stavby	stavba	k1gFnPc4	stavba
patří	patřit	k5eAaImIp3nS	patřit
katedrála	katedrála	k1gFnSc1	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Františka	František	k1gMnSc2	František
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
správa	správa	k1gFnSc1	správa
arcidiecéze	arcidiecéze	k1gFnSc1	arcidiecéze
však	však	k9	však
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
Albuquerque	Albuquerque	k1gFnSc6	Albuquerque
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
67	[number]	k4	67
947	[number]	k4	947
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
78,9	[number]	k4	78,9
<g/>
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
1,0	[number]	k4	1,0
<g/>
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
2,1	[number]	k4	2,1
<g/>
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
1,4	[number]	k4	1,4
<g/>
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
0,1	[number]	k4	0,1
<g/>
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
12,8	[number]	k4	12,8
<g/>
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
3,7	[number]	k4	3,7
<g/>
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
ras	ras	k1gMnSc1	ras
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
hispánského	hispánský	k2eAgMnSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
48,7	[number]	k4	48,7
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Buchara	Buchara	k1gFnSc1	Buchara
<g/>
,	,	kIx,	,
Uzbekistán	Uzbekistán	k1gInSc1	Uzbekistán
Holguín	Holguín	k1gInSc1	Holguín
<g/>
,	,	kIx,	,
Kuba	Kuba	k1gFnSc1	Kuba
Parral	Parral	k1gFnSc1	Parral
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
Santa	Sant	k1gInSc2	Sant
Fe	Fe	k1gFnSc2	Fe
<g/>
,	,	kIx,	,
Španělsko	Španělsko	k1gNnSc1	Španělsko
Sorrento	Sorrento	k1gNnSc1	Sorrento
<g/>
,	,	kIx,	,
Itálie	Itálie	k1gFnSc1	Itálie
Tsuyama	Tsuyama	k1gFnSc1	Tsuyama
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
Arcidiecéze	arcidiecéze	k1gFnSc2	arcidiecéze
Santa	Santo	k1gNnSc2	Santo
Fe	Fe	k1gFnSc2	Fe
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Santa	Sant	k1gInSc2	Sant
Fe	Fe	k1gMnPc2	Fe
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
města	město	k1gNnSc2	město
</s>
