<s>
Od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
oficiálního	oficiální	k2eAgNnSc2d1	oficiální
založení	založení	k1gNnSc2	založení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1610	[number]	k4	1610
do	do	k7c2	do
počátku	počátek	k1gInSc2	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
bylo	být	k5eAaImAgNnS	být
Santa	Sant	k1gMnSc4	Sant
Fe	Fe	k1gMnSc4	Fe
hlavní	hlavní	k2eAgFnSc4d1	hlavní
městem	město	k1gNnSc7	město
provincie	provincie	k1gFnSc2	provincie
Nové	Nové	k2eAgNnSc1d1	Nové
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
,	,	kIx,	,
spadající	spadající	k2eAgMnSc1d1	spadající
nejprve	nejprve	k6eAd1	nejprve
pod	pod	k7c4	pod
Místokrálovství	Místokrálovství	k1gNnSc4	Místokrálovství
Nové	Nové	k2eAgNnSc1d1	Nové
Španělsko	Španělsko	k1gNnSc1	Španělsko
a	a	k8xC	a
poté	poté	k6eAd1	poté
pod	pod	k7c4	pod
Mexiko	Mexiko	k1gNnSc4	Mexiko
<g/>
.	.	kIx.	.
</s>
