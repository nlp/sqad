<s>
Vektorový	vektorový	k2eAgInSc1d1	vektorový
prostor	prostor	k1gInSc1	prostor
(	(	kIx(	(
<g/>
též	též	k9	též
lineární	lineární	k2eAgInSc1d1	lineární
prostor	prostor	k1gInSc1	prostor
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
vector	vector	k1gMnSc1	vector
space	space	k1gMnSc1	space
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ústředním	ústřední	k2eAgInSc7d1	ústřední
objektem	objekt	k1gInSc7	objekt
studia	studio	k1gNnSc2	studio
lineární	lineární	k2eAgFnSc2d1	lineární
algebry	algebra	k1gFnSc2	algebra
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
rámci	rámec	k1gInSc6	rámec
jsou	být	k5eAaImIp3nP	být
definovány	definovat	k5eAaBmNgInP	definovat
všechny	všechen	k3xTgInPc1	všechen
ostatní	ostatní	k2eAgInPc1d1	ostatní
důležité	důležitý	k2eAgInPc1d1	důležitý
pojmy	pojem	k1gInPc1	pojem
této	tento	k3xDgFnSc2	tento
disciplíny	disciplína	k1gFnSc2	disciplína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jistém	jistý	k2eAgInSc6d1	jistý
smyslu	smysl	k1gInSc6	smysl
můžeme	moct	k5eAaImIp1nP	moct
vektorový	vektorový	k2eAgInSc4d1	vektorový
prostor	prostor	k1gInSc4	prostor
chápat	chápat	k5eAaImF	chápat
jako	jako	k8xS	jako
zobecnění	zobecnění	k1gNnSc1	zobecnění
množiny	množina	k1gFnSc2	množina
reálných	reálný	k2eAgFnPc2d1	reálná
<g/>
,	,	kIx,	,
potažmo	potažmo	k6eAd1	potažmo
komplexních	komplexní	k2eAgFnPc6d1	komplexní
<g/>
,	,	kIx,	,
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
množinách	množina	k1gFnPc6	množina
je	být	k5eAaImIp3nS	být
i	i	k9	i
ve	v	k7c6	v
vektorovém	vektorový	k2eAgInSc6d1	vektorový
prostoru	prostor	k1gInSc6	prostor
definována	definovat	k5eAaBmNgFnS	definovat
operace	operace	k1gFnSc1	operace
sčítání	sčítání	k1gNnSc2	sčítání
a	a	k8xC	a
násobení	násobení	k1gNnSc2	násobení
s	s	k7c7	s
jistými	jistý	k2eAgNnPc7d1	jisté
přirozenými	přirozený	k2eAgNnPc7d1	přirozené
omezeními	omezení	k1gNnPc7	omezení
jako	jako	k8xC	jako
asociativita	asociativita	k1gFnSc1	asociativita
apod.	apod.	kA	apod.
</s>
<s>
Prvek	prvek	k1gInSc1	prvek
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
vektor	vektor	k1gInSc1	vektor
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
vector	vector	k1gInSc1	vector
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vektorovém	vektorový	k2eAgInSc6d1	vektorový
prostoru	prostor	k1gInSc6	prostor
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
lineární	lineární	k2eAgFnSc4d1	lineární
matematickou	matematický	k2eAgFnSc4d1	matematická
strukturu	struktura	k1gFnSc4	struktura
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
dva	dva	k4xCgInPc4	dva
vektory	vektor	k1gInPc4	vektor
lze	lze	k6eAd1	lze
sečíst	sečíst	k5eAaPmF	sečíst
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
tento	tento	k3xDgInSc1	tento
součet	součet	k1gInSc1	součet
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
prvkem	prvek	k1gInSc7	prvek
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
a	a	k8xC	a
totéž	týž	k3xTgNnSc1	týž
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
pro	pro	k7c4	pro
násobek	násobek	k1gInSc4	násobek
vektoru	vektor	k1gInSc2	vektor
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
konceptem	koncept	k1gInSc7	koncept
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
v	v	k7c6	v
nejrůznějších	různý	k2eAgNnPc6d3	nejrůznější
odvětvích	odvětví	k1gNnPc6	odvětví
matematiky	matematika	k1gFnSc2	matematika
i	i	k8xC	i
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
základ	základ	k1gInSc4	základ
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
něhož	jenž	k3xRgNnSc2	jenž
lze	lze	k6eAd1	lze
elegantně	elegantně	k6eAd1	elegantně
popisovat	popisovat	k5eAaImF	popisovat
a	a	k8xC	a
řešit	řešit	k5eAaImF	řešit
jak	jak	k6eAd1	jak
úlohy	úloha	k1gFnPc4	úloha
numerické	numerický	k2eAgFnSc2d1	numerická
matematiky	matematika	k1gFnSc2	matematika
<g/>
,	,	kIx,	,
tak	tak	k9	tak
třeba	třeba	k9	třeba
i	i	k9	i
úlohy	úloha	k1gFnPc1	úloha
chování	chování	k1gNnSc2	chování
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
systémů	systém	k1gInPc2	systém
v	v	k7c6	v
klasické	klasický	k2eAgFnSc6d1	klasická
či	či	k8xC	či
kvantové	kvantový	k2eAgFnSc6d1	kvantová
mechanice	mechanika	k1gFnSc6	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
Vektorový	vektorový	k2eAgInSc1d1	vektorový
prostor	prostor	k1gInSc1	prostor
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
mladý	mladý	k2eAgInSc1d1	mladý
matematický	matematický	k2eAgInSc1d1	matematický
pojem	pojem	k1gInSc1	pojem
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
abstrakcí	abstrakce	k1gFnSc7	abstrakce
dosud	dosud	k6eAd1	dosud
známých	známý	k2eAgInPc2d1	známý
matematických	matematický	k2eAgInPc2d1	matematický
objektů	objekt	k1gInPc2	objekt
jako	jako	k8xS	jako
byly	být	k5eAaImAgFnP	být
matice	matice	k1gFnPc1	matice
<g/>
,	,	kIx,	,
soustavy	soustava	k1gFnPc1	soustava
lineárních	lineární	k2eAgFnPc2d1	lineární
rovnic	rovnice	k1gFnPc2	rovnice
nebo	nebo	k8xC	nebo
vektory	vektor	k1gInPc4	vektor
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
samotné	samotný	k2eAgFnSc2d1	samotná
lineární	lineární	k2eAgFnSc2d1	lineární
algebry	algebra	k1gFnSc2	algebra
lze	lze	k6eAd1	lze
jeho	on	k3xPp3gInSc4	on
vznik	vznik	k1gInSc4	vznik
klást	klást	k5eAaImF	klást
do	do	k7c2	do
konce	konec	k1gInSc2	konec
devatenáctého	devatenáctý	k4xOgNnSc2	devatenáctý
a	a	k8xC	a
počátku	počátek	k1gInSc6	počátek
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
vektor	vektor	k1gInSc1	vektor
pak	pak	k6eAd1	pak
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
latinského	latinský	k2eAgInSc2d1	latinský
vector	vector	k1gMnSc1	vector
znamenající	znamenající	k2eAgInSc1d1	znamenající
nosič	nosič	k1gInSc1	nosič
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
skaláru	skalár	k1gInSc3	skalár
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
navíc	navíc	k6eAd1	navíc
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
nese	nést	k5eAaImIp3nS	nést
<g/>
"	"	kIx"	"
i	i	k8xC	i
směr	směr	k1gInSc4	směr
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
náznak	náznak	k1gInSc1	náznak
pojmu	pojem	k1gInSc2	pojem
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Die	Die	k1gMnSc5	Die
lineale	lineal	k1gMnSc5	lineal
Ausdehnungslehre	Ausdehnungslehr	k1gMnSc5	Ausdehnungslehr
<g/>
,	,	kIx,	,
ein	ein	k?	ein
neuer	neuer	k1gMnSc1	neuer
Zweig	Zweig	k1gMnSc1	Zweig
der	drát	k5eAaImRp2nS	drát
Mathematik	Mathematika	k1gFnPc2	Mathematika
od	od	k7c2	od
Hermanna	Hermann	k1gMnSc2	Hermann
Grassmanna	Grassmann	k1gMnSc2	Grassmann
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1844	[number]	k4	1844
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
práce	práce	k1gFnSc1	práce
však	však	k9	však
zůstala	zůstat	k5eAaPmAgFnS	zůstat
téměř	téměř	k6eAd1	téměř
nepovšimnuta	povšimnut	k2eNgFnSc1d1	nepovšimnuta
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Grassmann	Grassmann	k1gMnSc1	Grassmann
nebyl	být	k5eNaImAgMnS	být
profesionální	profesionální	k2eAgMnSc1d1	profesionální
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
svoji	svůj	k3xOyFgFnSc4	svůj
teorii	teorie	k1gFnSc4	teorie
popisoval	popisovat	k5eAaImAgInS	popisovat
filozofickým	filozofický	k2eAgInSc7d1	filozofický
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgMnPc4d1	ostatní
matematiky	matematik	k1gMnPc4	matematik
těžko	těžko	k6eAd1	těžko
srozumitelný	srozumitelný	k2eAgMnSc1d1	srozumitelný
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
to	ten	k3xDgNnSc4	ten
byl	být	k5eAaImAgMnS	být
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
jako	jako	k9	jako
první	první	k4xOgFnSc7	první
zavedl	zavést	k5eAaPmAgMnS	zavést
pojmy	pojem	k1gInPc1	pojem
lineární	lineární	k2eAgFnSc2d1	lineární
kombinace	kombinace	k1gFnSc2	kombinace
<g/>
,	,	kIx,	,
lineární	lineární	k2eAgFnSc2d1	lineární
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
,	,	kIx,	,
dimenze	dimenze	k1gFnSc2	dimenze
<g/>
,	,	kIx,	,
lineární	lineární	k2eAgInSc1d1	lineární
obal	obal	k1gInSc1	obal
<g/>
,	,	kIx,	,
báze	báze	k1gFnSc1	báze
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
též	též	k9	též
dokázal	dokázat	k5eAaPmAgInS	dokázat
tvrzení	tvrzení	k1gNnPc4	tvrzení
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
nyní	nyní	k6eAd1	nyní
známe	znát	k5eAaImIp1nP	znát
pod	pod	k7c4	pod
názvy	název	k1gInPc4	název
Steinitzova	Steinitzův	k2eAgFnSc1d1	Steinitzova
věta	věta	k1gFnSc1	věta
o	o	k7c6	o
výměně	výměna	k1gFnSc6	výměna
<g/>
,	,	kIx,	,
první	první	k4xOgFnSc1	první
věta	věta	k1gFnSc1	věta
o	o	k7c4	o
dimenzi	dimenze	k1gFnSc4	dimenze
<g/>
,	,	kIx,	,
nezávislost	nezávislost	k1gFnSc4	nezávislost
dimenze	dimenze	k1gFnSc2	dimenze
na	na	k7c6	na
volbě	volba	k1gFnSc6	volba
báze	báze	k1gFnSc2	báze
<g/>
,	,	kIx,	,
vzorec	vzorec	k1gInSc4	vzorec
pro	pro	k7c4	pro
transformaci	transformace	k1gFnSc4	transformace
souřadnic	souřadnice	k1gFnPc2	souřadnice
při	při	k7c6	při
přechodu	přechod	k1gInSc6	přechod
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgFnPc7	dva
bázemi	báze	k1gFnPc7	báze
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
a	a	k8xC	a
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
.	.	kIx.	.
</s>
<s>
Axiomatickou	axiomatický	k2eAgFnSc4d1	axiomatická
definici	definice	k1gFnSc4	definice
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
pak	pak	k6eAd1	pak
jako	jako	k8xC	jako
první	první	k4xOgFnSc1	první
podává	podávat	k5eAaImIp3nS	podávat
Giuseppe	Giusepp	k1gInSc5	Giusepp
Peano	Peaen	k2eAgNnSc1d1	Peaen
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
Calcolo	Calcola	k1gFnSc5	Calcola
geometrico	geometrico	k6eAd1	geometrico
secundo	secundo	k1gNnSc1	secundo
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Ausdehnungslehre	Ausdehnungslehr	k1gInSc5	Ausdehnungslehr
di	di	k?	di
H.	H.	kA	H.
Grassmann	Grassmann	k1gInSc1	Grassmann
<g/>
,	,	kIx,	,
precedutto	precedutt	k2eAgNnSc1d1	precedutt
dalle	dalle	k1gNnSc1	dalle
operazioni	operazioň	k1gFnSc3	operazioň
della	della	k1gFnSc1	della
logica	logica	k1gFnSc1	logica
deduttiva	deduttiva	k1gFnSc1	deduttiva
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1888	[number]	k4	1888
<g/>
.	.	kIx.	.
</s>
<s>
Vektorový	vektorový	k2eAgInSc1d1	vektorový
prostor	prostor	k1gInSc1	prostor
v	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
podobě	podoba	k1gFnSc6	podoba
je	být	k5eAaImIp3nS	být
poprvé	poprvé	k6eAd1	poprvé
definován	definovat	k5eAaBmNgInS	definovat
v	v	k7c6	v
dizertační	dizertační	k2eAgFnSc6d1	dizertační
práci	práce	k1gFnSc6	práce
Stefana	Stefan	k1gMnSc2	Stefan
Banacha	Banach	k1gMnSc2	Banach
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
a	a	k8xC	a
v	v	k7c6	v
moderních	moderní	k2eAgFnPc6d1	moderní
učebnicích	učebnice	k1gFnPc6	učebnice
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
pojem	pojem	k1gInSc1	pojem
poprvé	poprvé	k6eAd1	poprvé
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
učebnici	učebnice	k1gFnSc6	učebnice
Modern	Modern	k1gMnSc1	Modern
Algebra	algebra	k1gFnSc1	algebra
od	od	k7c2	od
van	vana	k1gFnPc2	vana
der	drát	k5eAaImRp2nS	drát
Waerdena	Waerden	k2eAgNnPc4d1	Waerden
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
viz	vidět	k5eAaImRp2nS	vidět
a	a	k8xC	a
článek	článek	k1gInSc4	článek
Lineární	lineární	k2eAgFnSc1d1	lineární
algebra	algebra	k1gFnSc1	algebra
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
motivaci	motivace	k1gFnSc4	motivace
pro	pro	k7c4	pro
zavedení	zavedení	k1gNnSc4	zavedení
pojmu	pojem	k1gInSc2	pojem
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
uvažujme	uvažovat	k5eAaImRp1nP	uvažovat
dva	dva	k4xCgInPc1	dva
případy	případ	k1gInPc1	případ
matematických	matematický	k2eAgInPc2d1	matematický
objektů	objekt	k1gInPc2	objekt
-	-	kIx~	-
fyzikální	fyzikální	k2eAgInPc1d1	fyzikální
vektory	vektor	k1gInPc1	vektor
(	(	kIx(	(
<g/>
coby	coby	k?	coby
šipky	šipka	k1gFnSc2	šipka
<g/>
)	)	kIx)	)
a	a	k8xC	a
polynomy	polynom	k1gInPc1	polynom
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
případů	případ	k1gInPc2	případ
podáme	podat	k5eAaPmIp1nP	podat
jeho	jeho	k3xOp3gFnSc4	jeho
základní	základní	k2eAgFnSc4d1	základní
charakteristiku	charakteristika	k1gFnSc4	charakteristika
a	a	k8xC	a
pokusíme	pokusit	k5eAaPmIp1nP	pokusit
se	se	k3xPyFc4	se
najít	najít	k5eAaPmF	najít
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
oba	dva	k4xCgMnPc1	dva
tyto	tento	k3xDgInPc1	tento
případy	případ	k1gInPc1	případ
společné	společný	k2eAgInPc1d1	společný
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
nás	my	k3xPp1nPc4	my
už	už	k6eAd1	už
přímo	přímo	k6eAd1	přímo
povede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
definování	definování	k1gNnSc3	definování
matematické	matematický	k2eAgFnSc2d1	matematická
struktury	struktura	k1gFnSc2	struktura
<g/>
,	,	kIx,	,
splňující	splňující	k2eAgFnSc2d1	splňující
jisté	jistý	k2eAgFnSc2d1	jistá
vlastnosti	vlastnost	k1gFnSc2	vlastnost
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
půjde	jít	k5eAaImIp3nS	jít
použít	použít	k5eAaPmF	použít
jak	jak	k6eAd1	jak
k	k	k7c3	k
popisu	popis	k1gInSc3	popis
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
vektorů	vektor	k1gInPc2	vektor
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
k	k	k7c3	k
popisu	popis	k1gInSc3	popis
polynomů	polynom	k1gInPc2	polynom
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
matematickou	matematický	k2eAgFnSc7d1	matematická
strukturou	struktura	k1gFnSc7	struktura
bude	být	k5eAaImBp3nS	být
právě	právě	k9	právě
vektorový	vektorový	k2eAgInSc1d1	vektorový
prostor	prostor	k1gInSc1	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgFnSc4	první
si	se	k3xPyFc3	se
představme	představit	k5eAaPmRp1nP	představit
fyzikální	fyzikální	k2eAgInPc4d1	fyzikální
vektory	vektor	k1gInPc4	vektor
<g/>
,	,	kIx,	,
chápané	chápaný	k2eAgFnSc2d1	chápaná
jako	jako	k8xS	jako
šipky	šipka	k1gFnSc2	šipka
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
názornost	názornost	k1gFnSc4	názornost
mějme	mít	k5eAaImRp1nP	mít
puk	puk	k1gInSc4	puk
na	na	k7c6	na
ledové	ledový	k2eAgFnSc6d1	ledová
ploše	plocha	k1gFnSc6	plocha
a	a	k8xC	a
snažme	snažit	k5eAaImRp1nP	snažit
se	se	k3xPyFc4	se
popsat	popsat	k5eAaPmF	popsat
jeho	jeho	k3xOp3gInPc4	jeho
pohyb	pohyb	k1gInSc4	pohyb
po	po	k7c6	po
ledě	led	k1gInSc6	led
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
úderech	úder	k1gInPc6	úder
hokejky	hokejka	k1gFnSc2	hokejka
dopadající	dopadající	k2eAgFnPc1d1	dopadající
na	na	k7c4	na
jeho	jeho	k3xOp3gFnPc4	jeho
strany	strana	k1gFnPc4	strana
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
intenzitou	intenzita	k1gFnSc7	intenzita
a	a	k8xC	a
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
směrech	směr	k1gInPc6	směr
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc4	každý
úder	úder	k1gInSc4	úder
hokejky	hokejka	k1gFnSc2	hokejka
lze	lze	k6eAd1	lze
popsat	popsat	k5eAaPmF	popsat
velikostí	velikost	k1gFnSc7	velikost
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
puk	puk	k1gInSc4	puk
<g/>
,	,	kIx,	,
a	a	k8xC	a
směrem	směr	k1gInSc7	směr
jejího	její	k3xOp3gNnSc2	její
působení	působení	k1gNnSc2	působení
<g/>
.	.	kIx.	.
</s>
<s>
Úder	úder	k1gInSc4	úder
můžeme	moct	k5eAaImIp1nP	moct
tedy	tedy	k9	tedy
přirozeně	přirozeně	k6eAd1	přirozeně
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k8xS	jako
šipku	šipka	k1gFnSc4	šipka
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
délka	délka	k1gFnSc1	délka
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
působící	působící	k2eAgFnSc3d1	působící
síle	síla	k1gFnSc3	síla
a	a	k8xC	a
její	její	k3xOp3gInSc4	její
směr	směr	k1gInSc4	směr
směru	směr	k1gInSc2	směr
působící	působící	k2eAgFnSc2d1	působící
síly	síla	k1gFnSc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Zajímá	zajímat	k5eAaImIp3nS	zajímat
nás	my	k3xPp1nPc4	my
nyní	nyní	k6eAd1	nyní
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
puk	puk	k1gInSc1	puk
posune	posunout	k5eAaPmIp3nS	posunout
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
<g/>
-li	i	k?	-li
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
v	v	k7c4	v
jeden	jeden	k4xCgInSc4	jeden
okamžik	okamžik	k1gInSc4	okamžik
působit	působit	k5eAaImF	působit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
jedna	jeden	k4xCgFnSc1	jeden
hokejka	hokejka	k1gFnSc1	hokejka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
puk	puk	k1gInSc1	puk
posune	posunout	k5eAaPmIp3nS	posunout
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
výslednice	výslednice	k1gFnSc2	výslednice
působících	působící	k2eAgFnPc2d1	působící
sil	síla	k1gFnPc2	síla
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
posunutí	posunutí	k1gNnSc1	posunutí
bude	být	k5eAaImBp3nS	být
úměrné	úměrný	k2eAgNnSc1d1	úměrné
velikosti	velikost	k1gFnSc6	velikost
této	tento	k3xDgFnSc2	tento
výslednice	výslednice	k1gFnSc2	výslednice
<g/>
.	.	kIx.	.
</s>
<s>
Výslednici	výslednice	k1gFnSc4	výslednice
sil	síla	k1gFnPc2	síla
přitom	přitom	k6eAd1	přitom
obdržíme	obdržet	k5eAaPmIp1nP	obdržet
složením	složení	k1gNnSc7	složení
všech	všecek	k3xTgFnPc2	všecek
"	"	kIx"	"
<g/>
šipek	šipka	k1gFnPc2	šipka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
v	v	k7c4	v
daný	daný	k2eAgInSc4d1	daný
okamžik	okamžik	k1gInSc4	okamžik
na	na	k7c4	na
puk	puk	k1gInSc4	puk
působí	působit	k5eAaImIp3nS	působit
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
konkrétnost	konkrétnost	k1gFnSc4	konkrétnost
uvažujme	uvažovat	k5eAaImRp1nP	uvažovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
nacházíme	nacházet	k5eAaImIp1nP	nacházet
v	v	k7c6	v
situaci	situace	k1gFnSc6	situace
popsané	popsaný	k2eAgFnSc6d1	popsaná
obrázkem	obrázek	k1gInSc7	obrázek
Obr	obr	k1gMnSc1	obr
<g/>
.	.	kIx.	.
1	[number]	k4	1
vpravo	vpravo	k6eAd1	vpravo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
máme	mít	k5eAaImIp1nP	mít
dvě	dva	k4xCgFnPc4	dva
působící	působící	k2eAgFnPc4d1	působící
síly	síla	k1gFnPc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Abychom	aby	kYmCp1nP	aby
mohli	moct	k5eAaImAgMnP	moct
s	s	k7c7	s
těmito	tento	k3xDgFnPc7	tento
šipkami	šipka	k1gFnPc7	šipka
lépe	dobře	k6eAd2	dobře
pracovat	pracovat	k5eAaImF	pracovat
<g/>
,	,	kIx,	,
zaveďme	zavést	k5eAaPmRp1nP	zavést
si	se	k3xPyFc3	se
na	na	k7c6	na
ledové	ledový	k2eAgFnSc6d1	ledová
ploše	plocha	k1gFnSc6	plocha
souřadnicovou	souřadnicový	k2eAgFnSc4d1	souřadnicová
soustavu	soustava	k1gFnSc4	soustava
a	a	k8xC	a
označme	označit	k5eAaPmRp1nP	označit
si	se	k3xPyFc3	se
šipky	šipka	k1gFnPc4	šipka
po	po	k7c6	po
řadě	řada	k1gFnSc6	řada
symboly	symbol	k1gInPc4	symbol
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
ukázáno	ukázat	k5eAaPmNgNnS	ukázat
na	na	k7c4	na
Obr	obr	k1gMnSc1	obr
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Výslednice	výslednice	k1gFnPc4	výslednice
těchto	tento	k3xDgFnPc2	tento
dvou	dva	k4xCgFnPc2	dva
šipek	šipka	k1gFnPc2	šipka
pak	pak	k6eAd1	pak
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
jejich	jejich	k3xOp3gNnSc7	jejich
složením	složení	k1gNnSc7	složení
<g/>
,	,	kIx,	,
označme	označit	k5eAaPmRp1nP	označit
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
Obr	obr	k1gMnSc1	obr
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Složením	složení	k1gNnSc7	složení
dvou	dva	k4xCgFnPc2	dva
šipek	šipka	k1gFnPc2	šipka
jsme	být	k5eAaImIp1nP	být
tedy	tedy	k9	tedy
opět	opět	k6eAd1	opět
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
nějakou	nějaký	k3yIgFnSc4	nějaký
šipku	šipka	k1gFnSc4	šipka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nyní	nyní	k6eAd1	nyní
popisuje	popisovat	k5eAaImIp3nS	popisovat
výslednici	výslednice	k1gFnSc4	výslednice
působících	působící	k2eAgFnPc2d1	působící
sil	síla	k1gFnPc2	síla
popsaných	popsaný	k2eAgFnPc2d1	popsaná
šipkami	šipka	k1gFnPc7	šipka
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
-	-	kIx~	-
výsledek	výsledek	k1gInSc1	výsledek
naprosto	naprosto	k6eAd1	naprosto
zjevný	zjevný	k2eAgInSc1d1	zjevný
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
další	další	k2eAgFnSc4d1	další
diskuzi	diskuze	k1gFnSc4	diskuze
však	však	k9	však
klíčový	klíčový	k2eAgInSc1d1	klíčový
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
pohlédneme	pohlédnout	k5eAaPmIp1nP	pohlédnout
na	na	k7c4	na
souřadnice	souřadnice	k1gFnPc4	souřadnice
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
šipek	šipka	k1gFnPc2	šipka
v	v	k7c6	v
námi	my	k3xPp1nPc7	my
zavedené	zavedený	k2eAgFnSc6d1	zavedená
souřadnicové	souřadnicový	k2eAgFnSc6d1	souřadnicová
soustavě	soustava	k1gFnSc6	soustava
<g/>
,	,	kIx,	,
dostáváme	dostávat	k5eAaImIp1nP	dostávat
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
,	,	kIx,	,
5	[number]	k4	5
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
color	color	k1gInSc4	color
{	{	kIx(	{
<g/>
blue	blue	k6eAd1	blue
<g/>
}	}	kIx)	}
<g/>
{{	{{	k?	{{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
begin	begin	k2eAgInSc4d1	begin
<g/>
{	{	kIx(	{
<g/>
pmatrix	pmatrix	k1gInSc4	pmatrix
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s>
<g/>
\\	\\	k?	\\
<g/>
0,5	[number]	k4	0,5
<g/>
\	\	kIx~	\
<g/>
end	end	k?	end
<g/>
{	{	kIx(	{
<g/>
pmatrix	pmatrix	k1gInSc1	pmatrix
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
color	color	k1gMnSc1	color
{	{	kIx(	{
<g/>
black	black	k1gMnSc1	black
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
quad	quad	k1gMnSc1	quad
\	\	kIx~	\
<g/>
color	color	k1gMnSc1	color
{	{	kIx(	{
<g/>
red	red	k?	red
<g/>
}	}	kIx)	}
<g/>
{{	{{	k?	{{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s>
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
begin	begin	k1gInSc1	begin
<g/>
{	{	kIx(	{
<g/>
pmatrix	pmatrix	k1gInSc1	pmatrix
<g/>
}	}	kIx)	}
<g/>
2	[number]	k4	2
<g/>
\\	\\	k?	\\
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
end	end	k?	end
<g/>
{	{	kIx(	{
<g/>
pmatrix	pmatrix	k1gInSc1	pmatrix
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<g/>
color	color	k1gMnSc1	color
{	{	kIx(	{
<g/>
black	black	k1gMnSc1	black
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
,	,	kIx,	,
<g/>
}}	}}	k?	}}
:	:	kIx,	:
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
barvy	barva	k1gFnPc4	barva
zvoleny	zvolen	k2eAgFnPc4d1	zvolena
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgInS	být
zjevný	zjevný	k2eAgInSc1d1	zjevný
vztah	vztah	k1gInSc1	vztah
čísel	číslo	k1gNnPc2	číslo
k	k	k7c3	k
obrázkům	obrázek	k1gInPc3	obrázek
vpravo	vpravo	k6eAd1	vpravo
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
souřadnicemi	souřadnice	k1gFnPc7	souřadnice
šipky	šipka	k1gFnPc4	šipka
jsou	být	k5eAaImIp3nP	být
myšleny	myšlen	k2eAgFnPc4d1	myšlena
souřadnice	souřadnice	k1gFnPc4	souřadnice
bodu	bod	k1gInSc2	bod
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
šipka	šipka	k1gFnSc1	šipka
"	"	kIx"	"
<g/>
končí	končit	k5eAaImIp3nS	končit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
třeba	třeba	k6eAd1	třeba
vypisovat	vypisovat	k5eAaImF	vypisovat
souřadnice	souřadnice	k1gFnPc4	souřadnice
bodu	bod	k1gInSc2	bod
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
šipka	šipka	k1gFnSc1	šipka
"	"	kIx"	"
<g/>
začíná	začínat	k5eAaImIp3nS	začínat
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
všechny	všechen	k3xTgFnPc1	všechen
šipky	šipka	k1gFnPc1	šipka
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
výslednic	výslednice	k1gFnPc2	výslednice
a	a	k8xC	a
násobků	násobek	k1gInPc2	násobek
<g/>
,	,	kIx,	,
začínají	začínat	k5eAaImIp3nP	začínat
v	v	k7c6	v
tomtéž	týž	k3xTgInSc6	týž
bodě	bod	k1gInSc6	bod
-	-	kIx~	-
puku	puk	k1gInSc2	puk
(	(	kIx(	(
<g/>
který	který	k3yRgMnSc1	který
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
naší	náš	k3xOp1gFnSc6	náš
souřadné	souřadný	k2eAgFnSc6d1	souřadná
soustavě	soustava	k1gFnSc6	soustava
souřadnice	souřadnice	k1gFnSc2	souřadnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
binom	binom	k1gInSc1	binom
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}	}}}	k?	}}}
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
podíváme	podívat	k5eAaImIp1nP	podívat
na	na	k7c4	na
souřadnice	souřadnice	k1gFnPc4	souřadnice
výslednice	výslednice	k1gFnSc2	výslednice
sil	síla	k1gFnPc2	síla
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
3	[number]	k4	3
<g/>
}}	}}	k?	}}
,	,	kIx,	,
vidíme	vidět	k5eAaImIp1nP	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
horní	horní	k2eAgFnSc1d1	horní
složka	složka	k1gFnSc1	složka
má	mít	k5eAaImIp3nS	mít
hodnotu	hodnota	k1gFnSc4	hodnota
1	[number]	k4	1
(	(	kIx(	(
<g/>
x-ová	xvý	k2eAgFnSc1d1	x-ová
souřadnice	souřadnice	k1gFnSc1	souřadnice
<g/>
)	)	kIx)	)
a	a	k8xC	a
spodní	spodní	k2eAgFnSc1d1	spodní
(	(	kIx(	(
<g/>
y-ová	yvý	k2eAgFnSc1d1	y-ová
souřadnice	souřadnice	k1gFnSc1	souřadnice
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
hodnotu	hodnota	k1gFnSc4	hodnota
1,5	[number]	k4	1,5
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Obr	obr	k1gMnSc1	obr
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc4	jeden
dílek	dílek	k1gInSc4	dílek
na	na	k7c6	na
ose	osa	k1gFnSc6	osa
představuje	představovat	k5eAaImIp3nS	představovat
hodnotu	hodnota	k1gFnSc4	hodnota
0,5	[number]	k4	0,5
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
souřadnice	souřadnice	k1gFnPc4	souřadnice
vektorů	vektor	k1gInPc2	vektor
tedy	tedy	k8xC	tedy
platí	platit	k5eAaImIp3nS	platit
vztah	vztah	k1gInSc4	vztah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
,	,	kIx,	,
5	[number]	k4	5
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
,	,	kIx,	,
5	[number]	k4	5
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
color	color	k1gInSc4	color
{	{	kIx(	{
<g/>
blue	blue	k6eAd1	blue
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
begin	begin	k2eAgInSc4d1	begin
<g/>
{	{	kIx(	{
<g/>
pmatrix	pmatrix	k1gInSc4	pmatrix
<g/>
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
\\	\\	k?	\\
<g/>
0,5	[number]	k4	0,5
<g/>
\	\	kIx~	\
<g/>
end	end	k?	end
<g/>
{	{	kIx(	{
<g/>
pmatrix	pmatrix	k1gInSc1	pmatrix
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
color	color	k1gMnSc1	color
{	{	kIx(	{
<g/>
black	black	k1gMnSc1	black
<g />
.	.	kIx.	.
</s>
<s>
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
color	color	k1gInSc1	color
{	{	kIx(	{
<g/>
red	red	k?	red
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
begin	begin	k1gInSc1	begin
<g/>
{	{	kIx(	{
<g/>
pmatrix	pmatrix	k1gInSc1	pmatrix
<g/>
}	}	kIx)	}
<g/>
2	[number]	k4	2
<g/>
\\	\\	k?	\\
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
end	end	k?	end
<g/>
{	{	kIx(	{
<g/>
pmatrix	pmatrix	k1gInSc1	pmatrix
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
color	color	k1gInSc1	color
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
black	black	k6eAd1	black
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
begin	begin	k2eAgInSc4d1	begin
<g/>
{	{	kIx(	{
<g/>
pmatrix	pmatrix	k1gInSc4	pmatrix
<g/>
}	}	kIx)	}
<g/>
1	[number]	k4	1
<g/>
\\	\\	k?	\\
<g/>
1,5	[number]	k4	1,5
<g/>
\	\	kIx~	\
<g/>
end	end	k?	end
<g/>
{	{	kIx(	{
<g/>
pmatrix	pmatrix	k1gInSc4	pmatrix
<g/>
}}}	}}}	k?	}}}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Pokud	pokud	k8xS	pokud
výše	vysoce	k6eAd2	vysoce
uvedenou	uvedený	k2eAgFnSc4d1	uvedená
rovnost	rovnost	k1gFnSc4	rovnost
přepíšeme	přepsat	k5eAaPmIp1nP	přepsat
do	do	k7c2	do
kompaktnějšího	kompaktní	k2eAgInSc2d2	kompaktnější
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
máme	mít	k5eAaImIp1nP	mít
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
ukázat	ukázat	k5eAaPmF	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
vztah	vztah	k1gInSc1	vztah
platí	platit	k5eAaImIp3nS	platit
obecně	obecně	k6eAd1	obecně
pro	pro	k7c4	pro
jakoukoli	jakýkoli	k3yIgFnSc4	jakýkoli
volbu	volba	k1gFnSc4	volba
šipek	šipka	k1gFnPc2	šipka
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
výslednici	výslednice	k1gFnSc4	výslednice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Vidíme	vidět	k5eAaImIp1nP	vidět
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
že	že	k8xS	že
výslednici	výslednice	k1gFnSc4	výslednice
dvou	dva	k4xCgFnPc2	dva
působících	působící	k2eAgFnPc2d1	působící
sil	síla	k1gFnPc2	síla
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}}	}}	k?	}}
můžeme	moct	k5eAaImIp1nP	moct
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k9	jako
jejich	jejich	k3xOp3gInSc4	jejich
součet	součet	k1gInSc4	součet
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
na	na	k7c4	na
puk	puk	k1gInSc4	puk
udeříme	udeřit	k5eAaPmIp1nP	udeřit
dvakrát	dvakrát	k6eAd1	dvakrát
vyšší	vysoký	k2eAgFnSc7d2	vyšší
silou	síla	k1gFnSc7	síla
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
odpovídající	odpovídající	k2eAgFnSc1d1	odpovídající
šipka	šipka	k1gFnSc1	šipka
bude	být	k5eAaImBp3nS	být
dvakrát	dvakrát	k6eAd1	dvakrát
delší	dlouhý	k2eAgMnSc1d2	delší
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zachová	zachovat	k5eAaPmIp3nS	zachovat
si	se	k3xPyFc3	se
svůj	svůj	k3xOyFgInSc4	svůj
směr	směr	k1gInSc4	směr
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
mějme	mít	k5eAaImRp1nP	mít
nějakou	nějaký	k3yIgFnSc4	nějaký
šipku	šipka	k1gFnSc4	šipka
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
odpovídající	odpovídající	k2eAgFnSc3d1	odpovídající
jisté	jistý	k2eAgFnSc3d1	jistá
síle	síla	k1gFnSc3	síla
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
}	}	kIx)	}
-krát	rát	k1gMnSc1	-krát
vyšší	vysoký	k2eAgFnSc3d2	vyšší
síle	síla	k1gFnSc3	síla
bude	být	k5eAaImBp3nS	být
odpovídat	odpovídat	k5eAaImF	odpovídat
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
}	}	kIx)	}
-krát	rát	k1gInSc1	-krát
delší	dlouhý	k2eAgFnSc1d2	delší
šipka	šipka	k1gFnSc1	šipka
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
která	který	k3yRgFnSc1	který
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
stejný	stejný	k2eAgInSc4d1	stejný
směr	směr	k1gInSc4	směr
jako	jako	k8xS	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
tedy	tedy	k9	tedy
šipky	šipka	k1gFnPc1	šipka
různě	různě	k6eAd1	různě
násobit	násobit	k5eAaImF	násobit
číslem	číslo	k1gNnSc7	číslo
a	a	k8xC	a
vzájemně	vzájemně	k6eAd1	vzájemně
sčítat	sčítat	k5eAaImF	sčítat
a	a	k8xC	a
opět	opět	k6eAd1	opět
dostaneme	dostat	k5eAaPmIp1nP	dostat
nějakou	nějaký	k3yIgFnSc4	nějaký
šipku	šipka	k1gFnSc4	šipka
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
jestli	jestli	k8xS	jestli
složím	složit	k5eAaPmIp1nS	složit
šipku	šipka	k1gFnSc4	šipka
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
se	se	k3xPyFc4	se
šipkou	šipka	k1gFnSc7	šipka
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
,	,	kIx,	,
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Neboli	neboli	k8xC	neboli
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s hack="1">
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Této	tento	k3xDgFnSc2	tento
vlastnosti	vlastnost	k1gFnSc2	vlastnost
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
komutativita	komutativita	k1gFnSc1	komutativita
<g/>
.	.	kIx.	.
</s>
<s>
Mějme	mít	k5eAaImRp1nP	mít
nyní	nyní	k6eAd1	nyní
tři	tři	k4xCgFnPc4	tři
šipky	šipka	k1gFnPc4	šipka
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
předchozím	předchozí	k2eAgInSc6d1	předchozí
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
nejdříve	dříve	k6eAd3	dříve
složím	složit	k5eAaPmIp1nS	složit
šipku	šipka	k1gFnSc4	šipka
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
se	se	k3xPyFc4	se
šipkou	šipka	k1gFnSc7	šipka
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
výslednici	výslednice	k1gFnSc4	výslednice
složím	složit	k5eAaPmIp1nS	složit
se	s	k7c7	s
šipkou	šipka	k1gFnSc7	šipka
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jestli	jestli	k8xS	jestli
nejdříve	dříve	k6eAd3	dříve
složím	složit	k5eAaPmIp1nS	složit
šipku	šipka	k1gFnSc4	šipka
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
se	se	k3xPyFc4	se
šipkou	šipka	k1gFnSc7	šipka
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
výslednici	výslednice	k1gFnSc4	výslednice
se	s	k7c7	s
šipkou	šipka	k1gFnSc7	šipka
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Neboli	neboli	k8xC	neboli
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g />
.	.	kIx.	.
</s>
<s>
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Této	tento	k3xDgFnSc2	tento
vlastnosti	vlastnost	k1gFnSc2	vlastnost
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
asociativita	asociativita	k1gFnSc1	asociativita
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
k	k	k7c3	k
jakémukoli	jakýkoli	k3yIgInSc3	jakýkoli
vektoru	vektor	k1gInSc3	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
přičtu	přičíst	k5eAaPmIp1nS	přičíst
vektor	vektor	k1gInSc4	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
begin	begin	k1gInSc1	begin
<g/>
{	{	kIx(	{
<g/>
pmatrix	pmatrix	k1gInSc1	pmatrix
<g/>
}	}	kIx)	}
<g/>
0	[number]	k4	0
<g/>
\\	\\	k?	\\
<g/>
0	[number]	k4	0
<g/>
\	\	kIx~	\
<g/>
end	end	k?	end
<g/>
{	{	kIx(	{
<g/>
pmatrix	pmatrix	k1gInSc1	pmatrix
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
tak	tak	k6eAd1	tak
dostanu	dostat	k5eAaPmIp1nS	dostat
opět	opět	k6eAd1	opět
vektor	vektor	k1gInSc4	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Vektoru	vektor	k1gInSc2	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}	}}}	k?	}}}
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
"	"	kIx"	"
<g/>
šipka	šipka	k1gFnSc1	šipka
<g/>
"	"	kIx"	"
nulové	nulový	k2eAgFnSc2d1	nulová
délky	délka	k1gFnSc2	délka
a	a	k8xC	a
říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
nulový	nulový	k2eAgInSc4d1	nulový
vektor	vektor	k1gInSc4	vektor
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
konečně	konečně	k6eAd1	konečně
také	také	k9	také
vidíme	vidět	k5eAaImIp1nP	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
přesně	přesně	k6eAd1	přesně
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
na	na	k7c4	na
puk	puk	k1gInSc4	puk
působí	působit	k5eAaImIp3nP	působit
dvě	dva	k4xCgFnPc4	dva
síly	síla	k1gFnPc4	síla
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
a	a	k8xC	a
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}}	}}}	k?	}}}
stejné	stejný	k2eAgFnSc6d1	stejná
velikosti	velikost	k1gFnSc6	velikost
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInSc1	jejich
účinek	účinek	k1gInSc1	účinek
vyruší	vyrušit	k5eAaPmIp3nS	vyrušit
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
puk	puk	k1gInSc1	puk
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Neboli	neboli	k8xC	neboli
jejich	jejich	k3xOp3gFnSc1	jejich
výslednice	výslednice	k1gFnSc1	výslednice
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
součet	součet	k1gInSc1	součet
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nulový	nulový	k2eAgInSc4d1	nulový
vektor	vektor	k1gInSc4	vektor
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}	}}}	k?	}}}
,	,	kIx,	,
neboli	neboli	k8xC	neboli
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
.	.	kIx.	.
</s>
<s>
Máme	mít	k5eAaImIp1nP	mít
<g/>
-li	i	k?	-li
nějaký	nějaký	k3yIgInSc4	nějaký
vektor	vektor	k1gInSc4	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
,	,	kIx,	,
tak	tak	k8xS	tak
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
vždy	vždy	k6eAd1	vždy
jak	jak	k6eAd1	jak
vidíme	vidět	k5eAaImIp1nP	vidět
existuje	existovat	k5eAaImIp3nS	existovat
jistý	jistý	k2eAgInSc4d1	jistý
vektor	vektor	k1gInSc4	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
takový	takový	k3xDgMnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gFnSc1	jejich
výslednice	výslednice	k1gFnSc1	výslednice
je	být	k5eAaImIp3nS	být
nulový	nulový	k2eAgInSc4d1	nulový
vektor	vektor	k1gInSc4	vektor
<g/>
,	,	kIx,	,
vektoru	vektor	k1gInSc2	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
říkáme	říkat	k5eAaImIp1nP	říkat
vektor	vektor	k1gInSc4	vektor
opačný	opačný	k2eAgInSc4d1	opačný
k	k	k7c3	k
vektoru	vektor	k1gInSc3	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
.	.	kIx.	.
</s>
<s>
Shrňme	shrnout	k5eAaPmRp1nP	shrnout
si	se	k3xPyFc3	se
nyní	nyní	k6eAd1	nyní
<g/>
,	,	kIx,	,
na	na	k7c4	na
co	co	k3yRnSc4	co
jsme	být	k5eAaImIp1nP	být
zatím	zatím	k6eAd1	zatím
přišli	přijít	k5eAaPmAgMnP	přijít
<g/>
:	:	kIx,	:
Šipky	šipka	k1gFnPc4	šipka
lze	lze	k6eAd1	lze
sčítat	sčítat	k5eAaImF	sčítat
a	a	k8xC	a
násobit	násobit	k5eAaImF	násobit
číslem	číslo	k1gNnSc7	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Součet	součet	k1gInSc1	součet
dvou	dva	k4xCgFnPc2	dva
šipek	šipka	k1gFnPc2	šipka
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
šipka	šipka	k1gFnSc1	šipka
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
násobek	násobek	k1gInSc1	násobek
šipky	šipka	k1gFnSc2	šipka
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
šipka	šipka	k1gFnSc1	šipka
<g/>
.	.	kIx.	.
</s>
<s>
Sčítání	sčítání	k1gNnSc1	sčítání
šipek	šipka	k1gFnPc2	šipka
je	být	k5eAaImIp3nS	být
komutativní	komutativní	k2eAgFnSc1d1	komutativní
<g/>
.	.	kIx.	.
</s>
<s>
Sčítání	sčítání	k1gNnSc1	sčítání
šipek	šipka	k1gFnPc2	šipka
je	být	k5eAaImIp3nS	být
asociativní	asociativní	k2eAgNnSc1d1	asociativní
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Šipku	šipka	k1gFnSc4	šipka
nulové	nulový	k2eAgFnSc2d1	nulová
délky	délka	k1gFnSc2	délka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nulový	nulový	k2eAgInSc1d1	nulový
vektor	vektor	k1gInSc1	vektor
<g/>
,	,	kIx,	,
můžu	můžu	k?	můžu
přičíst	přičíst	k5eAaPmF	přičíst
k	k	k7c3	k
libovolné	libovolný	k2eAgFnSc3d1	libovolná
jiné	jiný	k2eAgFnSc3d1	jiná
šipce	šipka	k1gFnSc3	šipka
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
bych	by	kYmCp1nS	by
tuto	tento	k3xDgFnSc4	tento
změnil	změnit	k5eAaPmAgInS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
každé	každý	k3xTgFnSc3	každý
šipce	šipka	k1gFnSc3	šipka
najdu	najít	k5eAaPmIp1nS	najít
šipku	šipka	k1gFnSc4	šipka
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
opačnou	opačný	k2eAgFnSc4d1	opačná
<g/>
,	,	kIx,	,
opačný	opačný	k2eAgInSc4d1	opačný
vektor	vektor	k1gInSc4	vektor
<g/>
.	.	kIx.	.
</s>
<s>
Podobnou	podobný	k2eAgFnSc4d1	podobná
diskuzi	diskuze	k1gFnSc4	diskuze
vztahu	vztah	k1gInSc2	vztah
šipek	šipka	k1gFnPc2	šipka
a	a	k8xC	a
"	"	kIx"	"
<g/>
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
<g/>
"	"	kIx"	"
vektorů	vektor	k1gInPc2	vektor
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
v	v	k7c6	v
oddíle	oddíl	k1gInSc6	oddíl
Geometrická	geometrický	k2eAgFnSc1d1	geometrická
interpretace	interpretace	k1gFnSc1	interpretace
v	v	k7c6	v
článku	článek	k1gInSc6	článek
lineární	lineární	k2eAgFnSc2d1	lineární
kombinace	kombinace	k1gFnSc2	kombinace
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jsme	být	k5eAaImIp1nP	být
nyní	nyní	k6eAd1	nyní
vypsali	vypsat	k5eAaPmAgMnP	vypsat
základní	základní	k2eAgFnPc4d1	základní
vlastnosti	vlastnost	k1gFnPc4	vlastnost
množiny	množina	k1gFnSc2	množina
šipek	šipka	k1gFnPc2	šipka
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jejich	jejich	k3xOp3gNnSc7	jejich
sčítáním	sčítání	k1gNnSc7	sčítání
a	a	k8xC	a
násobením	násobení	k1gNnSc7	násobení
číslem	číslo	k1gNnSc7	číslo
<g/>
,	,	kIx,	,
přesuňme	přesunout	k5eAaPmRp1nP	přesunout
svoji	svůj	k3xOyFgFnSc4	svůj
pozornost	pozornost	k1gFnSc4	pozornost
na	na	k7c4	na
polynomy	polynom	k1gInPc4	polynom
<g/>
.	.	kIx.	.
</s>
<s>
Neboli	neboli	k8xC	neboli
na	na	k7c4	na
funkce	funkce	k1gFnPc4	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
p	p	k?	p
<g/>
}	}	kIx)	}
tvaru	tvar	k1gInSc2	tvar
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
-	-	kIx~	-
1	[number]	k4	1
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
-	-	kIx~	-
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
...	...	k?	...
+	+	kIx~	+
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
+	+	kIx~	+
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n-	n-	k?	n-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n-	n-	k?	n-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
+	+	kIx~	+
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
x	x	k?	x
<g/>
+	+	kIx~	+
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
sečteme	sečíst	k5eAaPmIp1nP	sečíst
dva	dva	k4xCgInPc4	dva
polynomy	polynom	k1gInPc4	polynom
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
p	p	k?	p
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
q	q	k?	q
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
q	q	k?	q
<g/>
}	}	kIx)	}
,	,	kIx,	,
tak	tak	k6eAd1	tak
dostaneme	dostat	k5eAaPmIp1nP	dostat
funkci	funkce	k1gFnSc4	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
<g />
.	.	kIx.	.
</s>
<s hack="1">
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
+	+	kIx~	+
q	q	k?	q
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
=	=	kIx~	=
(	(	kIx(	(
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
-	-	kIx~	-
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
-	-	kIx~	-
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
...	...	k?	...
+	+	kIx~	+
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
+	+	kIx~	+
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
+	+	kIx~	+
(	(	kIx(	(
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
n	n	k0	n
-	-	kIx~	-
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
-	-	kIx~	-
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
...	...	k?	...
+	+	kIx~	+
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
+	+	kIx~	+
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
b	b	k?	b
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
(	(	kIx(	(
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
(	(	kIx(	(
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
-	-	kIx~	-
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
n	n	k0	n
-	-	kIx~	-
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
n	n	k0	n
-	-	kIx~	-
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
...	...	k?	...
+	+	kIx~	+
(	(	kIx(	(
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
x	x	k?	x
+	+	kIx~	+
(	(	kIx(	(
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
q	q	k?	q
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
n-	n-	k?	n-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n-	n-	k?	n-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
+	+	kIx~	+
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
x	x	k?	x
<g/>
+	+	kIx~	+
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
n-	n-	k?	n-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n-	n-	k?	n-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
+	+	kIx~	+
<g />
.	.	kIx.	.
</s>
<s>
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
x	x	k?	x
<g/>
+	+	kIx~	+
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g />
.	.	kIx.	.
</s>
<s>
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n-	n-	k?	n-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
n-	n-	k?	n-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n-	n-	k?	n-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s>
<g/>
ldots	ldots	k1gInSc1	ldots
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
x	x	k?	x
<g/>
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
opět	opět	k6eAd1	opět
polynom	polynom	k1gInSc1	polynom
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
koeficienty	koeficient	k1gInPc1	koeficient
jsou	být	k5eAaImIp3nP	být
nyní	nyní	k6eAd1	nyní
rovny	roven	k2eAgFnPc1d1	rovna
součtům	součet	k1gInPc3	součet
koeficientů	koeficient	k1gInPc2	koeficient
polynomu	polynom	k1gInSc2	polynom
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
p	p	k?	p
<g/>
}	}	kIx)	}
a	a	k8xC	a
polynomu	polynom	k1gInSc2	polynom
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
q	q	k?	q
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
q	q	k?	q
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
koeficienty	koeficient	k1gInPc1	koeficient
jsou	být	k5eAaImIp3nP	být
zjevně	zjevně	k6eAd1	zjevně
stejné	stejný	k2eAgFnPc1d1	stejná
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
pořadí	pořadí	k1gNnSc4	pořadí
sčítání	sčítání	k1gNnSc2	sčítání
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
p	p	k?	p
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
q	q	k?	q
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
q	q	k?	q
<g/>
}	}	kIx)	}
,	,	kIx,	,
neboli	neboli	k8xC	neboli
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
+	+	kIx~	+
q	q	k?	q
=	=	kIx~	=
q	q	k?	q
+	+	kIx~	+
p	p	k?	p
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
+	+	kIx~	+
<g/>
q	q	k?	q
<g/>
=	=	kIx~	=
<g/>
q	q	k?	q
<g/>
+	+	kIx~	+
<g/>
p.	p.	k?	p.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Platí	platit	k5eAaImIp3nS	platit
tedy	tedy	k9	tedy
komutativita	komutativita	k1gFnSc1	komutativita
<g/>
.	.	kIx.	.
</s>
<s>
Snadno	snadno	k6eAd1	snadno
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
ověřila	ověřit	k5eAaPmAgFnS	ověřit
i	i	k9	i
asociativita	asociativita	k1gFnSc1	asociativita
sčítání	sčítání	k1gNnSc2	sčítání
<g/>
.	.	kIx.	.
</s>
<s>
Též	též	k9	též
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
funkce	funkce	k1gFnSc1	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alph	k1gMnSc4	alph
p	p	k?	p
<g/>
}	}	kIx)	}
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
vynásobením	vynásobení	k1gNnSc7	vynásobení
polynomu	polynom	k1gInSc2	polynom
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
p	p	k?	p
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
číslem	číslo	k1gNnSc7	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
polynom	polynom	k1gInSc4	polynom
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
nyní	nyní	k6eAd1	nyní
koeficienty	koeficient	k1gInPc4	koeficient
rovné	rovný	k2eAgInPc4d1	rovný
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alph	k1gMnSc4	alph
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
jsou	být	k5eAaImIp3nP	být
koeficienty	koeficient	k1gInPc1	koeficient
polynomu	polynom	k1gInSc2	polynom
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
p	p	k?	p
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
<g/>
,	,	kIx,	,
že	že	k8xS	že
nulová	nulový	k2eAgFnSc1d1	nulová
funkce	funkce	k1gFnSc1	funkce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
každému	každý	k3xTgMnSc3	každý
bodu	bod	k1gInSc2	bod
přiřazuje	přiřazovat	k5eAaImIp3nS	přiřazovat
nulu	nula	k1gFnSc4	nula
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
polynom	polynom	k1gInSc1	polynom
(	(	kIx(	(
<g/>
jehož	jehož	k3xOyRp3gInPc1	jehož
všechny	všechen	k3xTgInPc1	všechen
koeficienty	koeficient	k1gInPc1	koeficient
jsou	být	k5eAaImIp3nP	být
nulové	nulový	k2eAgInPc1d1	nulový
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Říkáme	říkat	k5eAaImIp1nP	říkat
mu	on	k3xPp3gNnSc3	on
nulový	nulový	k2eAgInSc4d1	nulový
polynom	polynom	k1gInSc4	polynom
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
k	k	k7c3	k
nulovému	nulový	k2eAgInSc3d1	nulový
polynomu	polynom	k1gInSc3	polynom
přičteme	přičíst	k5eAaPmIp1nP	přičíst
libovolný	libovolný	k2eAgInSc4d1	libovolný
polynom	polynom	k1gInSc4	polynom
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
součet	součet	k1gInSc1	součet
bude	být	k5eAaImBp3nS	být
roven	roven	k2eAgInSc1d1	roven
přičítanému	přičítaný	k2eAgInSc3d1	přičítaný
polynomu	polynom	k1gInSc3	polynom
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
<g/>
,	,	kIx,	,
mějme	mít	k5eAaImRp1nP	mít
nějaký	nějaký	k3yIgInSc4	nějaký
polynom	polynom	k1gInSc4	polynom
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
p	p	k?	p
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ho	on	k3xPp3gMnSc4	on
vynásobíme	vynásobit	k5eAaPmIp1nP	vynásobit
číslem	číslo	k1gNnSc7	číslo
-1	-1	k4	-1
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
dostaneme	dostat	k5eAaPmIp1nP	dostat
polynom	polynom	k1gInSc4	polynom
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
-p	-p	k?	-p
<g/>
}	}	kIx)	}
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
říkáme	říkat	k5eAaImIp1nP	říkat
opačný	opačný	k2eAgInSc4d1	opačný
polynom	polynom	k1gInSc4	polynom
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
-	-	kIx~	-
p	p	k?	p
)	)	kIx)	)
+	+	kIx~	+
p	p	k?	p
=	=	kIx~	=
0	[number]	k4	0
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
-p	-p	k?	-p
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
p	p	k?	p
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Součet	součet	k1gInSc1	součet
polynomu	polynom	k1gInSc2	polynom
a	a	k8xC	a
k	k	k7c3	k
němu	on	k3xPp3gInSc3	on
opačného	opačný	k2eAgInSc2d1	opačný
polynomu	polynom	k1gInSc2	polynom
tedy	tedy	k9	tedy
dává	dávat	k5eAaImIp3nS	dávat
nulový	nulový	k2eAgInSc4d1	nulový
polynom	polynom	k1gInSc4	polynom
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
si	se	k3xPyFc3	se
shrňme	shrnout	k5eAaPmRp1nP	shrnout
dosavadní	dosavadní	k2eAgNnSc4d1	dosavadní
zjištění	zjištění	k1gNnSc4	zjištění
<g/>
:	:	kIx,	:
Součet	součet	k1gInSc1	součet
dvou	dva	k4xCgInPc2	dva
polynomů	polynom	k1gInPc2	polynom
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
polynom	polynom	k1gInSc4	polynom
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
násobek	násobek	k1gInSc1	násobek
polynomu	polynom	k1gInSc2	polynom
číslem	číslo	k1gNnSc7	číslo
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
polynom	polynom	k1gInSc4	polynom
<g/>
.	.	kIx.	.
</s>
<s>
Sčítání	sčítání	k1gNnSc1	sčítání
polynomů	polynom	k1gInPc2	polynom
je	být	k5eAaImIp3nS	být
komutativní	komutativní	k2eAgFnSc1d1	komutativní
<g/>
.	.	kIx.	.
</s>
<s>
Sčítání	sčítání	k1gNnSc1	sčítání
polynomů	polynom	k1gInPc2	polynom
je	být	k5eAaImIp3nS	být
asociativní	asociativní	k2eAgFnSc1d1	asociativní
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
k	k	k7c3	k
nulovému	nulový	k2eAgInSc3d1	nulový
polynomu	polynom	k1gInSc3	polynom
přičtu	přičíst	k5eAaPmIp1nS	přičíst
libovolný	libovolný	k2eAgInSc4d1	libovolný
polynom	polynom	k1gInSc4	polynom
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
bude	být	k5eAaImBp3nS	být
součet	součet	k1gInSc1	součet
roven	roven	k2eAgInSc1d1	roven
přičítanému	přičítaný	k2eAgInSc3d1	přičítaný
polynomu	polynom	k1gInSc3	polynom
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
každému	každý	k3xTgInSc3	každý
polynomu	polynom	k1gInSc3	polynom
najdu	najít	k5eAaPmIp1nS	najít
polynom	polynom	k1gInSc4	polynom
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
opačný	opačný	k2eAgInSc4d1	opačný
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
vlastností	vlastnost	k1gFnPc2	vlastnost
šipek	šipka	k1gFnPc2	šipka
i	i	k9	i
seznam	seznam	k1gInSc4	seznam
vlastností	vlastnost	k1gFnPc2	vlastnost
polynomů	polynom	k1gInPc2	polynom
výše	výše	k1gFnSc2	výše
byly	být	k5eAaImAgFnP	být
záměrně	záměrně	k6eAd1	záměrně
napsány	napsat	k5eAaPmNgFnP	napsat
v	v	k7c6	v
co	co	k9	co
nejshodnější	shodný	k2eAgFnSc6d3	shodný
podobě	podoba	k1gFnSc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
vidíme	vidět	k5eAaImIp1nP	vidět
<g/>
,	,	kIx,	,
i	i	k8xC	i
přes	přes	k7c4	přes
zjevnou	zjevný	k2eAgFnSc4d1	zjevná
rozdílnost	rozdílnost	k1gFnSc4	rozdílnost
mají	mít	k5eAaImIp3nP	mít
šipky	šipka	k1gFnPc1	šipka
i	i	k8xC	i
polynomy	polynom	k1gInPc1	polynom
mnoho	mnoho	k6eAd1	mnoho
vlastností	vlastnost	k1gFnPc2	vlastnost
totožných	totožný	k2eAgInPc2d1	totožný
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
například	například	k6eAd1	například
o	o	k7c4	o
komutativitu	komutativita	k1gFnSc4	komutativita
či	či	k8xC	či
asociativitu	asociativita	k1gFnSc4	asociativita
sčítání	sčítání	k1gNnSc2	sčítání
nebo	nebo	k8xC	nebo
o	o	k7c4	o
existenci	existence	k1gFnSc4	existence
nulového	nulový	k2eAgInSc2d1	nulový
prvku	prvek	k1gInSc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Podobnosti	podobnost	k1gFnPc1	podobnost
mezi	mezi	k7c7	mezi
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
různých	různý	k2eAgMnPc2d1	různý
matematických	matematický	k2eAgMnPc2d1	matematický
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
těch	ten	k3xDgFnPc2	ten
dvou	dva	k4xCgFnPc2	dva
výše	vysoce	k6eAd2	vysoce
zmíněných	zmíněný	k2eAgFnPc2d1	zmíněná
<g/>
,	,	kIx,	,
vedly	vést	k5eAaImAgFnP	vést
matematiky	matematik	k1gMnPc4	matematik
k	k	k7c3	k
zavedení	zavedení	k1gNnSc3	zavedení
matematické	matematický	k2eAgFnSc2d1	matematická
struktury	struktura	k1gFnSc2	struktura
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
právě	právě	k9	právě
těmito	tento	k3xDgInPc7	tento
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
množinu	množina	k1gFnSc4	množina
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
není	být	k5eNaImIp3nS	být
specifikován	specifikován	k2eAgInSc4d1	specifikován
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
tvar	tvar	k1gInSc4	tvar
jejích	její	k3xOp3gInPc2	její
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zajímají	zajímat	k5eAaImIp3nP	zajímat
nás	my	k3xPp1nPc4	my
především	především	k9	především
jejich	jejich	k3xOp3gInPc1	jejich
vzájemné	vzájemný	k2eAgInPc1d1	vzájemný
vztahy	vztah	k1gInPc1	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Nezáleží	záležet	k5eNaImIp3nS	záležet
tedy	tedy	k9	tedy
moc	moc	k6eAd1	moc
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
uvažujeme	uvažovat	k5eAaImIp1nP	uvažovat
šipky	šipka	k1gFnPc1	šipka
či	či	k8xC	či
polynomy	polynom	k1gInPc1	polynom
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velkou	velký	k2eAgFnSc4d1	velká
roli	role	k1gFnSc4	role
hraje	hrát	k5eAaImIp3nS	hrát
např.	např.	kA	např.
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
oboje	oboj	k1gFnPc4	oboj
lze	lze	k6eAd1	lze
sčítat	sčítat	k5eAaImF	sčítat
a	a	k8xC	a
nezáleží	záležet	k5eNaImIp3nS	záležet
na	na	k7c6	na
pořadí	pořadí	k1gNnSc6	pořadí
sčítání	sčítání	k1gNnSc2	sčítání
<g/>
.	.	kIx.	.
</s>
<s>
Mějme	mít	k5eAaImRp1nP	mít
nyní	nyní	k6eAd1	nyní
tedy	tedy	k9	tedy
množinu	množina	k1gFnSc4	množina
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
si	se	k3xPyFc3	se
označme	označit	k5eAaPmRp1nP	označit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	V	kA	V
<g/>
}	}	kIx)	}
a	a	k8xC	a
jejíž	jejíž	k3xOyRp3gInPc1	jejíž
prvky	prvek	k1gInPc1	prvek
budeme	být	k5eAaImBp1nP	být
značit	značit	k5eAaImF	značit
v	v	k7c6	v
analogii	analogie	k1gFnSc6	analogie
se	s	k7c7	s
šipkami	šipka	k1gFnPc7	šipka
jako	jako	k8xC	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
...	...	k?	...
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k6eAd1	ldots
}	}	kIx)	}
,	,	kIx,	,
aniž	aniž	k8xS	aniž
bychom	by	kYmCp1nP	by
nějak	nějak	k6eAd1	nějak
blíže	blízce	k6eAd2	blízce
věděli	vědět	k5eAaImAgMnP	vědět
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
tyto	tento	k3xDgInPc4	tento
prvky	prvek	k1gInPc4	prvek
jsou	být	k5eAaImIp3nP	být
<g/>
.	.	kIx.	.
</s>
<s>
Nezajímá	zajímat	k5eNaImIp3nS	zajímat
nás	my	k3xPp1nPc4	my
tedy	tedy	k8xC	tedy
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
šipka	šipka	k1gFnSc1	šipka
či	či	k8xC	či
polynom	polynom	k1gInSc1	polynom
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
těchto	tento	k3xDgInPc6	tento
prvcích	prvek	k1gInPc6	prvek
však	však	k9	však
požadujeme	požadovat	k5eAaImIp1nP	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
splňovaly	splňovat	k5eAaImAgFnP	splňovat
následující	následující	k2eAgFnPc4d1	následující
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
:	:	kIx,	:
Součet	součet	k1gInSc1	součet
dvou	dva	k4xCgInPc2	dva
prvků	prvek	k1gInPc2	prvek
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
prvek	prvek	k1gInSc4	prvek
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	V	kA	V
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
násobek	násobek	k1gInSc1	násobek
prvku	prvek	k1gInSc2	prvek
číslem	číslo	k1gNnSc7	číslo
leží	ležet	k5eAaImIp3nS	ležet
opět	opět	k6eAd1	opět
ve	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	V	kA	V
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Jinými	jiný	k2eAgNnPc7d1	jiné
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
množina	množina	k1gFnSc1	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	V	kA	V
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
na	na	k7c4	na
operace	operace	k1gFnPc4	operace
sčítání	sčítání	k1gNnSc1	sčítání
a	a	k8xC	a
násobení	násobený	k2eAgMnPc1d1	násobený
číslem	číslo	k1gNnSc7	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Sčítání	sčítání	k1gNnSc1	sčítání
prvků	prvek	k1gInPc2	prvek
je	být	k5eAaImIp3nS	být
komutativní	komutativní	k2eAgFnSc1d1	komutativní
<g/>
.	.	kIx.	.
</s>
<s>
Sčítání	sčítání	k1gNnSc1	sčítání
prvků	prvek	k1gInPc2	prvek
je	být	k5eAaImIp3nS	být
asociativní	asociativní	k2eAgFnSc1d1	asociativní
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
k	k	k7c3	k
nulovému	nulový	k2eAgInSc3d1	nulový
prvku	prvek	k1gInSc3	prvek
přičtu	přičíst	k5eAaPmIp1nS	přičíst
libovolný	libovolný	k2eAgInSc4d1	libovolný
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
bude	být	k5eAaImBp3nS	být
součet	součet	k1gInSc1	součet
roven	roven	k2eAgInSc1d1	roven
přičítanému	přičítaný	k2eAgInSc3d1	přičítaný
prvku	prvek	k1gInSc3	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
každému	každý	k3xTgInSc3	každý
prvku	prvek	k1gInSc3	prvek
najdu	najít	k5eAaPmIp1nS	najít
prvek	prvek	k1gInSc4	prvek
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
opačný	opačný	k2eAgInSc4d1	opačný
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
jsme	být	k5eAaImIp1nP	být
uvažovali	uvažovat	k5eAaImAgMnP	uvažovat
prvky	prvek	k1gInPc4	prvek
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	v	k7c6	v
<g/>
}	}	kIx)	}
násobené	násobený	k2eAgFnSc6d1	násobená
číslem	číslo	k1gNnSc7	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
čísla	číslo	k1gNnSc2	číslo
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
pro	pro	k7c4	pro
matematiku	matematika	k1gFnSc4	matematika
příliš	příliš	k6eAd1	příliš
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
a	a	k8xC	a
tak	tak	k6eAd1	tak
lze	lze	k6eAd1	lze
místo	místo	k7c2	místo
množiny	množina	k1gFnSc2	množina
čísel	číslo	k1gNnPc2	číslo
uvažovat	uvažovat	k5eAaImF	uvažovat
obecnější	obecní	k2eAgFnSc4d2	obecní
množinu	množina	k1gFnSc4	množina
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
těleso	těleso	k1gNnSc1	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Těleso	těleso	k1gNnSc1	těleso
samotné	samotný	k2eAgNnSc1d1	samotné
má	mít	k5eAaImIp3nS	mít
pak	pak	k6eAd1	pak
také	také	k9	také
dodatečnou	dodatečný	k2eAgFnSc4d1	dodatečná
strukturu	struktura	k1gFnSc4	struktura
určenou	určený	k2eAgFnSc4d1	určená
požadavky	požadavek	k1gInPc4	požadavek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
podobají	podobat	k5eAaImIp3nP	podobat
těm	ten	k3xDgFnPc3	ten
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
klademe	klást	k5eAaImIp1nP	klást
na	na	k7c4	na
prvky	prvek	k1gInPc4	prvek
množiny	množina	k1gFnSc2	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	V	kA	V
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Označme	označit	k5eAaPmRp1nP	označit
si	se	k3xPyFc3	se
těleso	těleso	k1gNnSc4	těleso
písmenem	písmeno	k1gNnSc7	písmeno
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
od	od	k7c2	od
něho	on	k3xPp3gNnSc2	on
požadujeme	požadovat	k5eAaImIp1nP	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
pro	pro	k7c4	pro
libovolné	libovolný	k2eAgInPc4d1	libovolný
dva	dva	k4xCgInPc4	dva
prvky	prvek	k1gInPc4	prvek
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
,	,	kIx,	,
β	β	k?	β
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alph	k1gMnSc4	alph
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc2	beta
}	}	kIx)	}
tělesa	těleso	k1gNnSc2	těleso
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
}	}	kIx)	}
platilo	platit	k5eAaImAgNnS	platit
<g/>
:	:	kIx,	:
Součet	součet	k1gInSc1	součet
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
+	+	kIx~	+
β	β	k?	β
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
také	také	k9	také
prvek	prvek	k1gInSc1	prvek
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Násobek	násobek	k1gInSc1	násobek
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
⋅	⋅	k?	⋅
β	β	k?	β
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
také	také	k9	také
prvek	prvek	k1gInSc1	prvek
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Prvek	prvek	k1gInSc1	prvek
opačný	opačný	k2eAgInSc1d1	opačný
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
}	}	kIx)	}
,	,	kIx,	,
tj.	tj.	kA	tj.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
také	také	k9	také
prvek	prvek	k1gInSc1	prvek
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
nenulový	nulový	k2eNgInSc4d1	nenulový
prvek	prvek	k1gInSc4	prvek
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
}}}	}}}	k?	}}}
také	také	k9	také
prvek	prvek	k1gInSc1	prvek
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Těleso	těleso	k1gNnSc1	těleso
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
množina	množina	k1gFnSc1	množina
vybavená	vybavený	k2eAgFnSc1d1	vybavená
operací	operace	k1gFnSc7	operace
sčítání	sčítání	k1gNnSc2	sčítání
a	a	k8xC	a
operací	operace	k1gFnPc2	operace
násobení	násobení	k1gNnSc4	násobení
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
dva	dva	k4xCgInPc4	dva
požadavky	požadavek	k1gInPc4	požadavek
výše	vysoce	k6eAd2	vysoce
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
těleso	těleso	k1gNnSc4	těleso
pro	pro	k7c4	pro
tyto	tento	k3xDgFnPc4	tento
dvě	dva	k4xCgFnPc4	dva
operace	operace	k1gFnPc4	operace
uzavřené	uzavřený	k2eAgFnPc4d1	uzavřená
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
navíc	navíc	k6eAd1	navíc
najít	najít	k5eAaPmF	najít
jak	jak	k8xC	jak
neutrální	neutrální	k2eAgInSc4d1	neutrální
prvek	prvek	k1gInSc4	prvek
vůči	vůči	k7c3	vůči
operaci	operace	k1gFnSc3	operace
sčítání	sčítání	k1gNnSc2	sčítání
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
označujeme	označovat	k5eAaImIp1nP	označovat
0	[number]	k4	0
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
neutrální	neutrální	k2eAgInSc4d1	neutrální
prvek	prvek	k1gInSc4	prvek
vůči	vůči	k7c3	vůči
operaci	operace	k1gFnSc3	operace
násobení	násobení	k1gNnSc2	násobení
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
označujeme	označovat	k5eAaImIp1nP	označovat
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
}}}	}}}	k?	}}}
ve	v	k7c6	v
čtvrtém	čtvrtý	k4xOgInSc6	čtvrtý
bodě	bod	k1gInSc6	bod
výše	výše	k1gFnSc2	výše
pak	pak	k6eAd1	pak
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
inverzní	inverzní	k2eAgInSc4d1	inverzní
prvek	prvek	k1gInSc4	prvek
pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alph	k1gMnSc2	alph
}	}	kIx)	}
vůči	vůči	k7c3	vůči
operaci	operace	k1gFnSc3	operace
násobení	násobení	k1gNnSc2	násobení
<g/>
.	.	kIx.	.
</s>
<s>
Dospěli	dochvít	k5eAaPmAgMnP	dochvít
jsme	být	k5eAaImIp1nP	být
tak	tak	k6eAd1	tak
zatím	zatím	k6eAd1	zatím
k	k	k7c3	k
matematické	matematický	k2eAgFnSc3d1	matematická
struktuře	struktura	k1gFnSc3	struktura
sestávající	sestávající	k2eAgFnSc2d1	sestávající
z	z	k7c2	z
množiny	množina	k1gFnSc2	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	V	kA	V
<g/>
}	}	kIx)	}
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yIgFnSc3	který
je	být	k5eAaImIp3nS	být
přidruženo	přidružen	k2eAgNnSc1d1	přidruženo
těleso	těleso	k1gNnSc1	těleso
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
<g />
.	.	kIx.	.
</s>
<s hack="1">
T	T	kA	T
<g/>
}	}	kIx)	}
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
součet	součet	k1gInSc1	součet
prvků	prvek	k1gInPc2	prvek
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	V	kA	V
<g/>
}	}	kIx)	}
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc4	jejich
násobek	násobek	k1gInSc4	násobek
prvkem	prvek	k1gInSc7	prvek
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
}	}	kIx)	}
leží	ležet	k5eAaImIp3nS	ležet
opět	opět	k6eAd1	opět
ve	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	V	kA	V
<g/>
}	}	kIx)	}
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
jsou	být	k5eAaImIp3nP	být
splněny	splnit	k5eAaPmNgFnP	splnit
jisté	jistý	k2eAgFnPc1d1	jistá
dodatečné	dodatečný	k2eAgFnPc1d1	dodatečná
podmínky	podmínka	k1gFnPc1	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
matematicky	matematicky	k6eAd1	matematicky
precizně	precizně	k6eAd1	precizně
přeformulujeme	přeformulovat	k5eAaPmIp1nP	přeformulovat
právě	právě	k9	právě
uvedené	uvedený	k2eAgInPc4d1	uvedený
požadavky	požadavek	k1gInPc4	požadavek
<g/>
,	,	kIx,	,
dodáme	dodat	k5eAaPmIp1nP	dodat
pár	pár	k4xCyI	pár
požadavků	požadavek	k1gInPc2	požadavek
dalších	další	k2eAgInPc2d1	další
a	a	k8xC	a
celou	celý	k2eAgFnSc4d1	celá
věc	věc	k1gFnSc4	věc
trochu	trochu	k6eAd1	trochu
zobecníme	zobecnit	k5eAaPmIp1nP	zobecnit
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
dospíváme	dospívat	k5eAaImIp1nP	dospívat
k	k	k7c3	k
současné	současný	k2eAgFnSc3d1	současná
definici	definice	k1gFnSc3	definice
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
podána	podat	k5eAaPmNgFnS	podat
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
oddíle	oddíl	k1gInSc6	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Nechť	nechť	k8xS	nechť
jsou	být	k5eAaImIp3nP	být
dány	dát	k5eAaPmNgFnP	dát
neprázdná	prázdný	k2eNgFnSc1d1	neprázdná
množina	množina	k1gFnSc1	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	V	kA	V
<g/>
}	}	kIx)	}
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInPc1	jejíž
prvky	prvek	k1gInPc1	prvek
nazýváme	nazývat	k5eAaImIp1nP	nazývat
vektory	vektor	k1gInPc1	vektor
<g/>
,	,	kIx,	,
těleso	těleso	k1gNnSc1	těleso
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
(	(	kIx(	(
<g/>
s	s	k7c7	s
operacemi	operace	k1gFnPc7	operace
sčítání	sčítání	k1gNnSc2	sčítání
+	+	kIx~	+
a	a	k8xC	a
násobení	násobený	k2eAgMnPc1d1	násobený
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⋅	⋅	k?	⋅
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
}	}	kIx)	}
)	)	kIx)	)
<g/>
,	,	kIx,	,
zobrazení	zobrazení	k1gNnSc1	zobrazení
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⊕	⊕	k?	⊕
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
×	×	k?	×
V	v	k7c6	v
→	→	k?	→
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
oplus	oplus	k1gInSc4	oplus
:	:	kIx,	:
<g/>
\	\	kIx~	\
V	V	kA	V
<g/>
\	\	kIx~	\
<g/>
times	times	k1gMnSc1	times
V	v	k7c6	v
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc4	ten
V	V	kA	V
<g/>
}	}	kIx)	}
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
nazýváme	nazývat	k5eAaImIp1nP	nazývat
sčítání	sčítání	k1gNnSc4	sčítání
vektorů	vektor	k1gInPc2	vektor
<g/>
,	,	kIx,	,
zobrazení	zobrazení	k1gNnSc1	zobrazení
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⊙	⊙	k?	⊙
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
×	×	k?	×
V	v	k7c6	v
→	→	k?	→
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
odot	odot	k5eAaPmF	odot
:	:	kIx,	:
<g/>
\	\	kIx~	\
T	T	kA	T
<g/>
\	\	kIx~	\
<g/>
times	times	k1gMnSc1	times
V	v	k7c6	v
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc4	ten
V	V	kA	V
<g/>
}	}	kIx)	}
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
nazýváme	nazývat	k5eAaImIp1nP	nazývat
násobení	násobení	k1gNnSc3	násobení
vektorů	vektor	k1gInPc2	vektor
(	(	kIx(	(
<g/>
prvkem	prvek	k1gInSc7	prvek
z	z	k7c2	z
tělesa	těleso	k1gNnSc2	těleso
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řekneme	říct	k5eAaPmIp1nP	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	V	kA	V
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
vektorový	vektorový	k2eAgInSc1d1	vektorový
prostor	prostor	k1gInSc1	prostor
nad	nad	k7c7	nad
tělesem	těleso	k1gNnSc7	těleso
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
}	}	kIx)	}
s	s	k7c7	s
vektorovými	vektorový	k2eAgFnPc7d1	vektorová
operacemi	operace	k1gFnPc7	operace
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⊕	⊕	k?	⊕
,	,	kIx,	,
⊙	⊙	k?	⊙
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
oplus	oplus	k1gInSc1	oplus
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
odot	odot	k1gInSc1	odot
}	}	kIx)	}
,	,	kIx,	,
právě	právě	k9	právě
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
množina	množina	k1gFnSc1	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	v	k7c6	v
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
na	na	k7c4	na
operace	operace	k1gFnPc4	operace
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⊕	⊕	k?	⊕
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
oplus	oplus	k1gInSc4	oplus
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⊙	⊙	k?	⊙
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
odot	odotum	k1gNnPc2	odotum
}	}	kIx)	}
a	a	k8xC	a
současně	současně	k6eAd1	současně
platí	platit	k5eAaImIp3nS	platit
tzv.	tzv.	kA	tzv.
axiomy	axiom	k1gInPc4	axiom
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Těchto	tento	k3xDgInPc2	tento
axiomů	axiom	k1gInPc2	axiom
je	být	k5eAaImIp3nS	být
osm	osm	k4xCc1	osm
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc6	jejich
znění	znění	k1gNnSc6	znění
je	být	k5eAaImIp3nS	být
následující	následující	k2eAgInSc1d1	následující
<g/>
:	:	kIx,	:
Axiomy	axiom	k1gInPc7	axiom
1	[number]	k4	1
až	až	k9	až
4	[number]	k4	4
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
množina	množina	k1gFnSc1	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	V	kA	V
<g/>
}	}	kIx)	}
tvoří	tvořit	k5eAaImIp3nS	tvořit
vzhledem	vzhled	k1gInSc7	vzhled
ke	k	k7c3	k
sčítání	sčítání	k1gNnSc3	sčítání
vektorů	vektor	k1gInPc2	vektor
komutativní	komutativní	k2eAgFnSc4d1	komutativní
grupu	grupa	k1gFnSc4	grupa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nulový	nulový	k2eAgInSc1d1	nulový
vektor	vektor	k1gInSc1	vektor
představuje	představovat	k5eAaImIp3nS	představovat
neutrální	neutrální	k2eAgInSc1d1	neutrální
prvek	prvek	k1gInSc1	prvek
grupy	grupa	k1gFnSc2	grupa
a	a	k8xC	a
opačný	opačný	k2eAgInSc1d1	opačný
vektor	vektor	k1gInSc1	vektor
představuje	představovat	k5eAaImIp3nS	představovat
inverzní	inverzní	k2eAgInSc1d1	inverzní
prvek	prvek	k1gInSc1	prvek
k	k	k7c3	k
danému	daný	k2eAgInSc3d1	daný
prvku	prvek	k1gInSc3	prvek
grupy	grupa	k1gFnSc2	grupa
alias	alias	k9	alias
vektoru	vektor	k1gInSc2	vektor
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
definice	definice	k1gFnSc2	definice
operací	operace	k1gFnPc2	operace
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⊕	⊕	k?	⊕
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
oplus	oplus	k1gInSc4	oplus
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⊙	⊙	k?	⊙
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
odot	odot	k5eAaPmF	odot
}	}	kIx)	}
implicitně	implicitně	k6eAd1	implicitně
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
uvažujeme	uvažovat	k5eAaImIp1nP	uvažovat
pouze	pouze	k6eAd1	pouze
ty	ten	k3xDgFnPc1	ten
operace	operace	k1gFnPc1	operace
sčítání	sčítání	k1gNnSc2	sčítání
vektorů	vektor	k1gInPc2	vektor
a	a	k8xC	a
násobení	násobení	k1gNnSc4	násobení
vektoru	vektor	k1gInSc2	vektor
prvkem	prvek	k1gInSc7	prvek
z	z	k7c2	z
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něž	jenž	k3xRgFnPc4	jenž
je	být	k5eAaImIp3nS	být
množina	množina	k1gFnSc1	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	v	k7c4	v
<g/>
}	}	kIx)	}
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
znaku	znak	k1gInSc2	znak
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⊕	⊕	k?	⊕
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
oplus	oplus	k1gInSc4	oplus
}	}	kIx)	}
pro	pro	k7c4	pro
sčítání	sčítání	k1gNnSc4	sčítání
používá	používat	k5eAaImIp3nS	používat
znaménko	znaménko	k1gNnSc4	znaménko
+	+	kIx~	+
a	a	k8xC	a
místo	místo	k7c2	místo
znaku	znak	k1gInSc2	znak
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⊙	⊙	k?	⊙
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
odot	odot	k5eAaPmF	odot
}	}	kIx)	}
pro	pro	k7c4	pro
násobení	násobení	k1gNnSc4	násobení
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
znaménka	znaménko	k1gNnSc2	znaménko
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⋅	⋅	k?	⋅
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
cdot	cdotum	k1gNnPc2	cdotum
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
byly	být	k5eAaImAgInP	být
tyto	tento	k3xDgInPc1	tento
symboly	symbol	k1gInPc1	symbol
použity	použít	k5eAaPmNgInP	použít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
odlišilo	odlišit	k5eAaPmAgNnS	odlišit
sčítání	sčítání	k1gNnSc2	sčítání
dvou	dva	k4xCgInPc2	dva
vektorů	vektor	k1gInPc2	vektor
a	a	k8xC	a
sčítání	sčítání	k1gNnSc2	sčítání
dvou	dva	k4xCgInPc2	dva
prvků	prvek	k1gInPc2	prvek
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
násobení	násobení	k1gNnSc4	násobení
vektoru	vektor	k1gInSc2	vektor
prvkem	prvek	k1gInSc7	prvek
tělesa	těleso	k1gNnSc2	těleso
a	a	k8xC	a
násobení	násobení	k1gNnSc2	násobení
dvou	dva	k4xCgInPc2	dva
prvků	prvek	k1gInPc2	prvek
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
kontextu	kontext	k1gInSc2	kontext
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
vždy	vždy	k6eAd1	vždy
patrné	patrný	k2eAgNnSc1d1	patrné
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
operaci	operace	k1gFnSc4	operace
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
použít	použít	k5eAaPmF	použít
<g/>
.	.	kIx.	.
</s>
<s>
Zdaleka	zdaleka	k6eAd1	zdaleka
nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
uvažují	uvažovat	k5eAaImIp3nP	uvažovat
číselná	číselný	k2eAgNnPc1d1	číselné
tělesa	těleso	k1gNnPc1	těleso
reálných	reálný	k2eAgNnPc2d1	reálné
či	či	k8xC	či
komplexních	komplexní	k2eAgNnPc2d1	komplexní
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Vektorový	vektorový	k2eAgInSc1d1	vektorový
prostor	prostor	k1gInSc1	prostor
nad	nad	k7c7	nad
tělesem	těleso	k1gNnSc7	těleso
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
}	}	kIx)	}
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
reálný	reálný	k2eAgInSc1d1	reálný
vektorový	vektorový	k2eAgInSc1d1	vektorový
prostor	prostor	k1gInSc1	prostor
<g/>
,	,	kIx,	,
vektorový	vektorový	k2eAgInSc1d1	vektorový
prostor	prostor	k1gInSc1	prostor
nad	nad	k7c7	nad
tělesem	těleso	k1gNnSc7	těleso
komplexních	komplexní	k2eAgNnPc2d1	komplexní
čísel	číslo	k1gNnPc2	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
C	C	kA	C
<g/>
}	}	kIx)	}
}	}	kIx)	}
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
komplexní	komplexní	k2eAgInSc4d1	komplexní
vektorový	vektorový	k2eAgInSc4d1	vektorový
prostor	prostor	k1gInSc4	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Vektorový	vektorový	k2eAgInSc1d1	vektorový
prostor	prostor	k1gInSc1	prostor
obsahující	obsahující	k2eAgInSc1d1	obsahující
pouze	pouze	k6eAd1	pouze
nulový	nulový	k2eAgInSc1d1	nulový
vektor	vektor	k1gInSc1	vektor
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
nulový	nulový	k2eAgMnSc1d1	nulový
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
triviální	triviální	k2eAgMnSc1d1	triviální
<g/>
)	)	kIx)	)
vektorový	vektorový	k2eAgInSc1d1	vektorový
prostor	prostor	k1gInSc1	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Triviální	triviální	k2eAgInSc1d1	triviální
prostor	prostor	k1gInSc1	prostor
je	být	k5eAaImIp3nS	být
nejjednodušším	jednoduchý	k2eAgInSc7d3	nejjednodušší
příkladem	příklad	k1gInSc7	příklad
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
z	z	k7c2	z
definice	definice	k1gFnSc2	definice
tělesa	těleso	k1gNnSc2	těleso
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
těleso	těleso	k1gNnSc1	těleso
samotné	samotný	k2eAgNnSc1d1	samotné
je	být	k5eAaImIp3nS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
operací	operace	k1gFnSc7	operace
sčítání	sčítání	k1gNnSc2	sčítání
a	a	k8xC	a
násobení	násobení	k1gNnSc2	násobení
prvkem	prvek	k1gInSc7	prvek
z	z	k7c2	z
tělesa	těleso	k1gNnSc2	těleso
vektorovým	vektorový	k2eAgInSc7d1	vektorový
prostorem	prostor	k1gInSc7	prostor
samo	sám	k3xTgNnSc1	sám
nad	nad	k7c7	nad
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
značení	značení	k1gNnSc4	značení
vektorů	vektor	k1gInPc2	vektor
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
různé	různý	k2eAgFnPc1d1	různá
notace	notace	k1gFnPc1	notace
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
buď	buď	k8xC	buď
s	s	k7c7	s
polotučným	polotučný	k2eAgNnSc7d1	polotučné
sázením	sázení	k1gNnSc7	sázení
symbolů	symbol	k1gInPc2	symbol
pro	pro	k7c4	pro
vektory	vektor	k1gInPc4	vektor
jako	jako	k8xC	jako
x	x	k?	x
<g/>
,	,	kIx,	,
y	y	k?	y
<g/>
,	,	kIx,	,
z	z	k7c2	z
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
se	s	k7c7	s
symboly	symbol	k1gInPc7	symbol
vysázenými	vysázený	k2eAgInPc7d1	vysázený
italikou	italika	k1gFnSc7	italika
majícími	mající	k2eAgFnPc7d1	mající
nad	nad	k7c7	nad
sebou	se	k3xPyFc7	se
šipku	šipka	k1gFnSc4	šipka
jako	jako	k8xS	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}}}	}}}	k?	}}}
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
budeme	být	k5eAaImBp1nP	být
držet	držet	k5eAaImF	držet
druhé	druhý	k4xOgFnPc1	druhý
jmenované	jmenovaný	k2eAgFnPc1d1	jmenovaná
konvence	konvence	k1gFnPc1	konvence
<g/>
.	.	kIx.	.
</s>
<s>
Přísně	přísně	k6eAd1	přísně
vzato	vzat	k2eAgNnSc1d1	vzato
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
definice	definice	k1gFnSc2	definice
vektorový	vektorový	k2eAgInSc1d1	vektorový
prostor	prostor	k1gInSc1	prostor
uspořádaná	uspořádaný	k2eAgFnSc1d1	uspořádaná
čtveřice	čtveřice	k1gFnSc1	čtveřice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
V	V	kA	V
,	,	kIx,	,
T	T	kA	T
,	,	kIx,	,
⊕	⊕	k?	⊕
,	,	kIx,	,
⊙	⊙	k?	⊙
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
(	(	kIx(	(
<g/>
V	V	kA	V
<g/>
,	,	kIx,	,
<g/>
T	T	kA	T
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
oplus	oplus	k1gInSc1	oplus
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
odot	odot	k1gInSc1	odot
<g />
.	.	kIx.	.
</s>
<s hack="1">
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
ale	ale	k9	ale
takový	takový	k3xDgInSc1	takový
vektorový	vektorový	k2eAgInSc1d1	vektorový
prostor	prostor	k1gInSc1	prostor
značí	značit	k5eAaImIp3nS	značit
prostě	prostě	k9	prostě
jako	jako	k9	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	V	kA	V
<g/>
}	}	kIx)	}
a	a	k8xC	a
buď	buď	k8xC	buď
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
čtenář	čtenář	k1gMnSc1	čtenář
ví	vědět	k5eAaImIp3nS	vědět
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
těleso	těleso	k1gNnSc1	těleso
a	a	k8xC	a
operace	operace	k1gFnPc1	operace
jsou	být	k5eAaImIp3nP	být
použity	použít	k5eAaPmNgFnP	použít
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc4	tento
specifikovány	specifikován	k2eAgFnPc4d1	specifikována
vždy	vždy	k6eAd1	vždy
při	při	k7c6	při
definici	definice	k1gFnSc6	definice
množiny	množina	k1gFnSc2	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	V	kA	V
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
běžně	běžně	k6eAd1	běžně
používaných	používaný	k2eAgInPc2d1	používaný
vektorových	vektorový	k2eAgInPc2d1	vektorový
prostorů	prostor	k1gInPc2	prostor
jako	jako	k8xS	jako
např.	např.	kA	např.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
jsou	být	k5eAaImIp3nP	být
příslušné	příslušný	k2eAgFnPc4d1	příslušná
operace	operace	k1gFnPc4	operace
a	a	k8xC	a
dané	daný	k2eAgNnSc4d1	dané
těleso	těleso	k1gNnSc4	těleso
definovány	definovat	k5eAaBmNgFnP	definovat
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
kanonicky	kanonicky	k6eAd1	kanonicky
a	a	k8xC	a
nemůže	moct	k5eNaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
nejasnostem	nejasnost	k1gFnPc3	nejasnost
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
u	u	k7c2	u
každé	každý	k3xTgFnSc2	každý
jiné	jiný	k2eAgFnSc2d1	jiná
množiny	množina	k1gFnSc2	množina
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
i	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
uvažovat	uvažovat	k5eAaImF	uvažovat
jeho	jeho	k3xOp3gFnPc4	jeho
podmnožiny	podmnožina	k1gFnPc4	podmnožina
<g/>
.	.	kIx.	.
</s>
<s>
Máme	mít	k5eAaImIp1nP	mít
na	na	k7c6	na
mysli	mysl	k1gFnSc6	mysl
konkrétně	konkrétně	k6eAd1	konkrétně
podmnožiny	podmnožina	k1gFnSc2	podmnožina
množiny	množina	k1gFnSc2	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	V	kA	V
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
V	V	kA	V
,	,	kIx,	,
T	T	kA	T
,	,	kIx,	,
⊕	⊕	k?	⊕
,	,	kIx,	,
⊙	⊙	k?	⊙
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
(	(	kIx(	(
<g/>
V	V	kA	V
<g/>
,	,	kIx,	,
<g/>
T	T	kA	T
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
oplus	oplus	k1gInSc1	oplus
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
odot	odot	k1gMnSc1	odot
)	)	kIx)	)
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
daný	daný	k2eAgInSc1d1	daný
vektorový	vektorový	k2eAgInSc1d1	vektorový
prostor	prostor	k1gInSc1	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Výsadní	výsadní	k2eAgNnSc1d1	výsadní
postavení	postavení	k1gNnSc1	postavení
mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgFnPc7	všecek
podmnožinami	podmnožina	k1gFnPc7	podmnožina
mají	mít	k5eAaImIp3nP	mít
pak	pak	k6eAd1	pak
ty	ten	k3xDgInPc1	ten
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
jsou	být	k5eAaImIp3nP	být
sami	sám	k3xTgMnPc1	sám
o	o	k7c4	o
sobě	se	k3xPyFc3	se
vektorovými	vektorový	k2eAgInPc7d1	vektorový
prostory	prostor	k1gInPc7	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
podmnožiny	podmnožina	k1gFnPc4	podmnožina
nazýváme	nazývat	k5eAaImIp1nP	nazývat
podprostory	podprostor	k1gMnPc7	podprostor
daného	daný	k2eAgInSc2d1	daný
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
viz	vidět	k5eAaImRp2nS	vidět
článek	článek	k1gInSc1	článek
Vektorový	vektorový	k2eAgInSc4d1	vektorový
podprostor	podprostor	k1gInSc4	podprostor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
uvažujeme	uvažovat	k5eAaImIp1nP	uvažovat
vektorový	vektorový	k2eAgInSc4d1	vektorový
prostor	prostor	k1gInSc4	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	v	k7c4	v
<g/>
}	}	kIx)	}
nad	nad	k7c7	nad
tělesem	těleso	k1gNnSc7	těleso
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
definice	definice	k1gFnSc2	definice
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
lze	lze	k6eAd1	lze
dokázat	dokázat	k5eAaPmF	dokázat
například	například	k6eAd1	například
tyto	tento	k3xDgFnPc4	tento
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
:	:	kIx,	:
Nulový	nulový	k2eAgInSc4d1	nulový
vektor	vektor	k1gInSc4	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
V	V	kA	V
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
jeden	jeden	k4xCgInSc1	jeden
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
∃	∃	k?	∃
!	!	kIx.	!
</s>
<s desamb="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
V	V	kA	V
)	)	kIx)	)
(	(	kIx(	(
∀	∀	k?	∀
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
V	V	kA	V
)	)	kIx)	)
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
exists	exists	k1gInSc1	exists
!	!	kIx.	!
</s>
<s>
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
V	V	kA	V
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
forall	forall	k1gInSc1	forall
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
V	V	kA	V
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
Důkaz	důkaz	k1gInSc4	důkaz
<g/>
:	:	kIx,	:
Z	z	k7c2	z
axiómů	axióm	k1gInPc2	axióm
máme	mít	k5eAaImIp1nP	mít
zajištěnu	zajištěn	k2eAgFnSc4d1	zajištěna
existenci	existence	k1gFnSc4	existence
přinejmenším	přinejmenším	k6eAd1	přinejmenším
jednoho	jeden	k4xCgInSc2	jeden
nulového	nulový	k2eAgInSc2d1	nulový
vektoru	vektor	k1gInSc2	vektor
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládejme	předpokládat	k5eAaImRp1nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
víc	hodně	k6eAd2	hodně
a	a	k8xC	a
uvažujme	uvažovat	k5eAaImRp1nP	uvažovat
dva	dva	k4xCgInPc4	dva
nějaké	nějaký	k3yIgInPc4	nějaký
nulové	nulový	k2eAgInPc4d1	nulový
vektory	vektor	k1gInPc4	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
≠	≠	k?	≠
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
neq	neq	k?	neq
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsme	být	k5eAaImIp1nP	být
po	po	k7c6	po
řadě	řada	k1gFnSc6	řada
využili	využít	k5eAaPmAgMnP	využít
axiomů	axiom	k1gInPc2	axiom
3	[number]	k4	3
<g/>
,	,	kIx,	,
1	[number]	k4	1
a	a	k8xC	a
opět	opět	k6eAd1	opět
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Vidíme	vidět	k5eAaImIp1nP	vidět
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
že	že	k8xS	že
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
spor	spor	k1gInSc4	spor
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
každému	každý	k3xTgInSc3	každý
vektoru	vektor	k1gInSc3	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
V	V	kA	V
<g/>
}	}	kIx)	}
existuje	existovat	k5eAaImIp3nS	existovat
právě	právě	k9	právě
jeden	jeden	k4xCgInSc1	jeden
vektor	vektor	k1gInSc1	vektor
opačný	opačný	k2eAgInSc1d1	opačný
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
značí	značit	k5eAaImIp3nS	značit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
matematickém	matematický	k2eAgInSc6d1	matematický
zápise	zápis	k1gInSc6	zápis
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
∀	∀	k?	∀
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
V	V	kA	V
)	)	kIx)	)
(	(	kIx(	(
∃	∃	k?	∃
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
∈	∈	k?	∈
V	V	kA	V
)	)	kIx)	)
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
(	(	kIx(	(
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
forall	forall	k1gInSc1	forall
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
V	V	kA	V
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
exists	exists	k1gInSc1	exists
!	!	kIx.	!
</s>
<s>
<g/>
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
V	V	kA	V
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
Důkaz	důkaz	k1gInSc4	důkaz
<g/>
:	:	kIx,	:
Opět	opět	k6eAd1	opět
máme	mít	k5eAaImIp1nP	mít
z	z	k7c2	z
axiomů	axiom	k1gInPc2	axiom
zajištěnu	zajištěn	k2eAgFnSc4d1	zajištěna
existenci	existence	k1gFnSc4	existence
alespoň	alespoň	k9	alespoň
jednoho	jeden	k4xCgInSc2	jeden
opačného	opačný	k2eAgInSc2d1	opačný
vektoru	vektor	k1gInSc2	vektor
k	k	k7c3	k
danému	daný	k2eAgInSc3d1	daný
vektoru	vektor	k1gInSc3	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
.	.	kIx.	.
</s>
<s>
Předpokládejme	předpokládat	k5eAaImRp1nP	předpokládat
existenci	existence	k1gFnSc4	existence
alespoň	alespoň	k9	alespoň
dvou	dva	k4xCgMnPc6	dva
opačných	opačný	k2eAgInPc2d1	opačný
navzájem	navzájem	k6eAd1	navzájem
různých	různý	k2eAgInPc2d1	různý
vektorů	vektor	k1gInPc2	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s hack="1">
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsme	být	k5eAaImIp1nP	být
použili	použít	k5eAaPmAgMnP	použít
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
,1	,1	k4	,1
<g/>
.	.	kIx.	.
a	a	k8xC	a
opět	opět	k6eAd1	opět
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
axiomu	axiom	k1gInSc2	axiom
<g/>
.	.	kIx.	.
</s>
<s>
Máme	mít	k5eAaImIp1nP	mít
tedy	tedy	k9	tedy
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
spor	spor	k1gInSc4	spor
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
každé	každý	k3xTgInPc4	každý
dva	dva	k4xCgInPc4	dva
vektory	vektor	k1gInPc4	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
V	V	kA	V
<g/>
}	}	kIx)	}
má	mít	k5eAaImIp3nS	mít
rovnice	rovnice	k1gFnSc1	rovnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
právě	právě	k9	právě
jedno	jeden	k4xCgNnSc1	jeden
řešení	řešení	k1gNnSc1	řešení
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}}	}}}	k?	}}}
,	,	kIx,	,
tj.	tj.	kA	tj.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
∀	∀	k?	∀
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
V	V	kA	V
)	)	kIx)	)
(	(	kIx(	(
∀	∀	k?	∀
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
V	V	kA	V
)	)	kIx)	)
(	(	kIx(	(
∀	∀	k?	∀
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
V	V	kA	V
)	)	kIx)	)
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
⇒	⇒	k?	⇒
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
forall	forall	k1gInSc1	forall
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
V	V	kA	V
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
forall	forall	k1gInSc1	forall
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
V	V	kA	V
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
forall	forall	k1gInSc1	forall
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
V	V	kA	V
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}}	}}	k?	}}
<g/>
<g />
.	.	kIx.	.
</s>
<s>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
quad	quad	k1gMnSc1	quad
\	\	kIx~	\
<g/>
Rightarrow	Rightarrow	k1gMnSc1	Rightarrow
\	\	kIx~	\
<g/>
quad	quad	k1gMnSc1	quad
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
Důkaz	důkaz	k1gInSc4	důkaz
<g/>
:	:	kIx,	:
Daný	daný	k2eAgInSc4d1	daný
předpis	předpis	k1gInSc4	předpis
pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
vypsaný	vypsaný	k2eAgMnSc1d1	vypsaný
výše	vysoce	k6eAd2	vysoce
zřejmě	zřejmě	k6eAd1	zřejmě
řeší	řešit	k5eAaImIp3nP	řešit
danou	daný	k2eAgFnSc4d1	daná
rovnici	rovnice	k1gFnSc4	rovnice
<g/>
,	,	kIx,	,
stačí	stačit	k5eAaBmIp3nS	stačit
dosadit	dosadit	k5eAaPmF	dosadit
a	a	k8xC	a
použít	použít	k5eAaPmF	použít
po	po	k7c6	po
řadě	řada	k1gFnSc6	řada
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
axiom	axiom	k1gInSc1	axiom
<g/>
.	.	kIx.	.
</s>
<s>
Dokažme	dokázat	k5eAaPmRp1nP	dokázat
jednoznačnost	jednoznačnost	k1gFnSc4	jednoznačnost
řešení	řešení	k1gNnPc4	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
spor	spor	k1gInSc4	spor
předpokládejme	předpokládat	k5eAaImRp1nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
existují	existovat	k5eAaImIp3nP	existovat
dvě	dva	k4xCgNnPc1	dva
řešení	řešení	k1gNnPc1	řešení
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
≠	≠	k?	≠
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
neq	neq	k?	neq
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s hack="1">
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
poslední	poslední	k2eAgFnSc3d1	poslední
rovnosti	rovnost	k1gFnSc3	rovnost
můžeme	moct	k5eAaImIp1nP	moct
zleva	zleva	k6eAd1	zleva
přičíst	přičíst	k5eAaPmF	přičíst
opačný	opačný	k2eAgInSc4d1	opačný
vektor	vektor	k1gInSc4	vektor
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}}}	}}}	k?	}}}
,	,	kIx,	,
tj.	tj.	kA	tj.
dostaneme	dostat	k5eAaPmIp1nP	dostat
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
+	+	kIx~	+
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
(	(	kIx(	(
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
+	+	kIx~	+
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
b	b	k?	b
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s>
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
však	však	k9	však
podle	podle	k7c2	podle
2	[number]	k4	2
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
axiomu	axiom	k1gInSc2	axiom
ekvivalentní	ekvivalentní	k2eAgMnSc1d1	ekvivalentní
výrazu	výraz	k1gInSc2	výraz
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Dospěli	dochvít	k5eAaPmAgMnP	dochvít
jsme	být	k5eAaImIp1nP	být
tedy	tedy	k9	tedy
ke	k	k7c3	k
sporu	spor	k1gInSc3	spor
<g/>
.	.	kIx.	.
</s>
<s>
Libovolný	libovolný	k2eAgInSc4d1	libovolný
násobek	násobek	k1gInSc4	násobek
nulového	nulový	k2eAgInSc2d1	nulový
vektoru	vektor	k1gInSc2	vektor
je	být	k5eAaImIp3nS	být
nulový	nulový	k2eAgInSc4d1	nulový
vektor	vektor	k1gInSc4	vektor
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
nulový	nulový	k2eAgInSc4d1	nulový
násobek	násobek	k1gInSc4	násobek
libovolného	libovolný	k2eAgInSc2d1	libovolný
vektoru	vektor	k1gInSc2	vektor
je	být	k5eAaImIp3nS	být
nulový	nulový	k2eAgInSc4d1	nulový
vektor	vektor	k1gInSc4	vektor
<g/>
.	.	kIx.	.
</s>
<s>
Neboli	neboli	k8xC	neboli
(	(	kIx(	(
<g/>
0	[number]	k4	0
je	být	k5eAaImIp3nS	být
neutrální	neutrální	k2eAgInSc4d1	neutrální
prvek	prvek	k1gInSc4	prvek
pro	pro	k7c4	pro
sčítání	sčítání	k1gNnSc4	sčítání
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
}	}	kIx)	}
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
∀	∀	k?	∀
α	α	k?	α
∈	∈	k?	∈
T	T	kA	T
)	)	kIx)	)
(	(	kIx(	(
α	α	k?	α
⋅	⋅	k?	⋅
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
forall	forall	k1gMnSc1	forall
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
\	\	kIx~	\
<g/>
in	in	k?	in
T	T	kA	T
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
∀	∀	k?	∀
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
V	V	kA	V
)	)	kIx)	)
(	(	kIx(	(
0	[number]	k4	0
⋅	⋅	k?	⋅
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
forall	forall	k1gInSc1	forall
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
V	V	kA	V
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
Důkaz	důkaz	k1gInSc4	důkaz
<g/>
:	:	kIx,	:
Uvažujme	uvažovat	k5eAaImRp1nP	uvažovat
nejprve	nejprve	k6eAd1	nejprve
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
≠	≠	k?	≠
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alph	k1gMnSc2	alph
\	\	kIx~	\
<g/>
neq	neq	k?	neq
0	[number]	k4	0
<g/>
}	}	kIx)	}
a	a	k8xC	a
dokazujme	dokazovat	k5eAaImRp1nP	dokazovat
první	první	k4xOgFnSc4	první
vlastnost	vlastnost	k1gFnSc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Mějme	mít	k5eAaImRp1nP	mít
libovolný	libovolný	k2eAgInSc4d1	libovolný
vektor	vektor	k1gInSc4	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
a	a	k8xC	a
výraz	výraz	k1gInSc4	výraz
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
<g />
.	.	kIx.	.
</s>
<s hack="1">
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
α	α	k?	α
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
α	α	k?	α
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s>
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
.	.	kIx.	.
</s>
<s>
Vektor	vektor	k1gInSc1	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}	}}}	k?	}}}
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
с	с	k?	с
jako	jako	k8xS	jako
nulový	nulový	k2eAgInSc4d1	nulový
vektor	vektor	k1gInSc4	vektor
a	a	k8xC	a
z	z	k7c2	z
již	již	k6eAd1	již
dokázané	dokázaný	k2eAgFnSc2d1	dokázaná
jednoznačnosti	jednoznačnost	k1gFnSc2	jednoznačnost
nulového	nulový	k2eAgInSc2d1	nulový
vektoru	vektor	k1gInSc2	vektor
<g />
.	.	kIx.	.
</s>
<s hack="1">
musí	muset	k5eAaImIp3nS	muset
platit	platit	k5eAaImF	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}	}}}	k?	}}}
.	.	kIx.	.
</s>
<s>
Případ	případ	k1gInSc1	případ
pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
podpřípadem	podpřípad	k1gInSc7	podpřípad
druhé	druhý	k4xOgFnSc2	druhý
vlastnosti	vlastnost	k1gFnSc2	vlastnost
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
nyní	nyní	k6eAd1	nyní
dokážeme	dokázat	k5eAaPmIp1nP	dokázat
<g/>
.	.	kIx.	.
</s>
<s>
Mějme	mít	k5eAaImRp1nP	mít
rovnici	rovnice	k1gFnSc4	rovnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alph	k1gMnSc4	alph
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
naší	náš	k3xOp1gFnSc7	náš
neznámou	neznámá	k1gFnSc7	neznámá
vektor	vektor	k1gInSc4	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}}	}}}	k?	}}}
.	.	kIx.	.
</s>
<s>
Vidíme	vidět	k5eAaImIp1nP	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
rovnici	rovnice	k1gFnSc4	rovnice
vyhovuje	vyhovovat	k5eAaImIp3nS	vyhovovat
volba	volba	k1gFnSc1	volba
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}	}}}	k?	}}}
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
ale	ale	k8xC	ale
dosadíme	dosadit	k5eAaPmIp1nP	dosadit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
,	,	kIx,	,
tak	tak	k6eAd1	tak
máme	mít	k5eAaImIp1nP	mít
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
0	[number]	k4	0
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}}}	}}}	k?	}}}
,	,	kIx,	,
tj.	tj.	kA	tj.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
α	α	k?	α
+	+	kIx~	+
0	[number]	k4	0
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
+0	+0	k4	+0
<g/>
)	)	kIx)	)
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
levá	levý	k2eAgFnSc1d1	levá
strana	strana	k1gFnSc1	strana
je	být	k5eAaImIp3nS	být
rovna	roven	k2eAgFnSc1d1	rovna
straně	strana	k1gFnSc3	strana
pravé	pravý	k2eAgFnSc3d1	pravá
<g/>
.	.	kIx.	.
</s>
<s>
Rovnost	rovnost	k1gFnSc1	rovnost
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
splněna	splnit	k5eAaPmNgFnS	splnit
i	i	k9	i
pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
0	[number]	k4	0
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
a	a	k8xC	a
z	z	k7c2	z
jednoznačnosti	jednoznačnost	k1gFnSc2	jednoznačnost
řešení	řešení	k1gNnSc2	řešení
dokázané	dokázaný	k2eAgFnSc2d1	dokázaná
výše	výše	k1gFnSc2	výše
musí	muset	k5eAaImIp3nS	muset
nutně	nutně	k6eAd1	nutně
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
.	.	kIx.	.
</s>
<s>
Vlastnost	vlastnost	k1gFnSc1	vlastnost
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
∀	∀	k?	∀
α	α	k?	α
∈	∈	k?	∈
T	T	kA	T
)	)	kIx)	)
(	(	kIx(	(
∀	∀	k?	∀
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
V	V	kA	V
)	)	kIx)	)
(	(	kIx(	(
α	α	k?	α
⋅	⋅	k?	⋅
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⇔	⇔	k?	⇔
:	:	kIx,	:
α	α	k?	α
=	=	kIx~	=
0	[number]	k4	0
<g />
.	.	kIx.	.
</s>
<s hack="1">
∨	∨	k?	∨
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
0	[number]	k4	0
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
forall	forall	k1gMnSc1	forall
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
\	\	kIx~	\
<g/>
in	in	k?	in
T	T	kA	T
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
forall	forall	k1gInSc1	forall
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
V	V	kA	V
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s>
cdot	cdot	k1gInSc1	cdot
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
quad	quad	k1gMnSc1	quad
\	\	kIx~	\
<g/>
Leftrightarrow	Leftrightarrow	k1gMnSc1	Leftrightarrow
\	\	kIx~	\
<g/>
quad	quad	k1gMnSc1	quad
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
\	\	kIx~	\
<g/>
lor	lor	k?	lor
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
Důkaz	důkaz	k1gInSc1	důkaz
<g/>
:	:	kIx,	:
Implikace	implikace	k1gFnSc1	implikace
zprava	zprava	k6eAd1	zprava
doleva	doleva	k6eAd1	doleva
plyne	plynout	k5eAaImIp3nS	plynout
z	z	k7c2	z
předchozího	předchozí	k2eAgNnSc2d1	předchozí
tvrzení	tvrzení	k1gNnSc2	tvrzení
<g/>
.	.	kIx.	.
</s>
<s>
Implikaci	implikace	k1gFnSc4	implikace
zleva	zleva	k6eAd1	zleva
doprava	doprava	k6eAd1	doprava
dokažme	dokázat	k5eAaPmRp1nP	dokázat
sporem	spor	k1gInSc7	spor
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládejme	předpokládat	k5eAaImRp1nP	předpokládat
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
že	že	k8xS	že
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alph	k1gMnSc4	alph
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}	}}}	k?	}}}
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
≠	≠	k?	≠
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
\	\	kIx~	\
<g/>
neq	neq	k?	neq
0	[number]	k4	0
<g/>
}	}	kIx)	}
a	a	k8xC	a
současně	současně	k6eAd1	současně
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
≠	≠	k?	≠
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
neq	neq	k?	neq
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}	}}}	k?	}}}
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
rovnice	rovnice	k1gFnPc1	rovnice
můžu	můžu	k?	můžu
tedy	tedy	k8xC	tedy
vynásobit	vynásobit	k5eAaPmF	vynásobit
prvkem	prvek	k1gInSc7	prvek
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
}}}	}}}	k?	}}}
dostávajíc	dostávat	k5eAaImSgFnS	dostávat
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}	}}}	k?	}}}
.	.	kIx.	.
</s>
<s>
Pravá	pravý	k2eAgFnSc1d1	pravá
strana	strana	k1gFnSc1	strana
rovnosti	rovnost	k1gFnSc2	rovnost
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
podle	podle	k7c2	podle
předchozího	předchozí	k2eAgNnSc2d1	předchozí
tvrzení	tvrzení	k1gNnSc2	tvrzení
rovna	roven	k2eAgFnSc1d1	rovna
nule	nula	k1gFnSc3	nula
a	a	k8xC	a
tedy	tedy	k9	tedy
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}	}}}	k?	}}}
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
spor	spor	k1gInSc4	spor
<g/>
.	.	kIx.	.
</s>
<s>
Vlastnost	vlastnost	k1gFnSc1	vlastnost
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
∀	∀	k?	∀
α	α	k?	α
∈	∈	k?	∈
T	T	kA	T
)	)	kIx)	)
(	(	kIx(	(
∀	∀	k?	∀
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
V	V	kA	V
)	)	kIx)	)
(	(	kIx(	(
(	(	kIx(	(
-	-	kIx~	-
α	α	k?	α
)	)	kIx)	)
⋅	⋅	k?	⋅
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
-	-	kIx~	-
(	(	kIx(	(
α	α	k?	α
⋅	⋅	k?	⋅
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
α	α	k?	α
⋅	⋅	k?	⋅
(	(	kIx(	(
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
forall	forall	k1gMnSc1	forall
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
\	\	kIx~	\
<g/>
in	in	k?	in
T	T	kA	T
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
forall	forall	k1gInSc1	forall
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
V	V	kA	V
<g/>
)	)	kIx)	)
<g/>
((	((	k?	((
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
))	))	k?	))
<g/>
}	}	kIx)	}
:	:	kIx,	:
Důkaz	důkaz	k1gInSc4	důkaz
<g/>
:	:	kIx,	:
Uvažujme	uvažovat	k5eAaImRp1nP	uvažovat
rovnici	rovnice	k1gFnSc4	rovnice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
0	[number]	k4	0
<g/>
}}}	}}}	k?	}}}
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
řešení	řešení	k1gNnSc1	řešení
je	být	k5eAaImIp3nS	být
zjevně	zjevně	k6eAd1	zjevně
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
-	-	kIx~	-
(	(	kIx(	(
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
ale	ale	k9	ale
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
(	(	kIx(	(
-	-	kIx~	-
α	α	k?	α
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
(	(	kIx(	(
α	α	k?	α
+	+	kIx~	+
(	(	kIx(	(
-	-	kIx~	-
α	α	k?	α
)	)	kIx)	)
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s hack="1">
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
)	)	kIx)	)
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s>
alpha	alpha	k1gFnSc1	alpha
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
))	))	k?	))
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}	}}}	k?	}}}
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
-	-	kIx~	-
α	α	k?	α
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
)	)	kIx)	)
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
též	též	k9	též
řešením	řešení	k1gNnSc7	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
α	α	k?	α
(	(	kIx(	(
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
α	α	k?	α
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
(	(	kIx(	(
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s hack="1">
=	=	kIx~	=
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
))	))	k?	))
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}}	}}}	k?	}}}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
(	(	kIx(	(
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
též	též	k9	též
řešením	řešení	k1gNnSc7	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
již	již	k6eAd1	již
výše	vysoce	k6eAd2	vysoce
dokázané	dokázaný	k2eAgFnSc2d1	dokázaná
jednoznačnosti	jednoznačnost	k1gFnSc2	jednoznačnost
řešení	řešení	k1gNnSc2	řešení
rovnice	rovnice	k1gFnSc2	rovnice
tedy	tedy	k9	tedy
plyne	plynout	k5eAaImIp3nS	plynout
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
(	(	kIx(	(
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
(	(	kIx(	(
-	-	kIx~	-
α	α	k?	α
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
α	α	k?	α
(	(	kIx(	(
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
)	)	kIx)	)
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Opačný	opačný	k2eAgInSc1d1	opačný
vektor	vektor	k1gInSc1	vektor
k	k	k7c3	k
danému	daný	k2eAgInSc3d1	daný
vektoru	vektor	k1gInSc3	vektor
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gNnSc4	on
vynásobíme	vynásobit	k5eAaPmIp1nP	vynásobit
prvkem	prvek	k1gInSc7	prvek
z	z	k7c2	z
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
opačný	opačný	k2eAgMnSc1d1	opačný
k	k	k7c3	k
jednotkovému	jednotkový	k2eAgInSc3d1	jednotkový
prvku	prvek	k1gInSc3	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Neboli	neboli	k8xC	neboli
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
∀	∀	k?	∀
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
V	V	kA	V
)	)	kIx)	)
(	(	kIx(	(
(	(	kIx(	(
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
-	-	kIx~	-
1	[number]	k4	1
⋅	⋅	k?	⋅
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
forall	foralnout	k5eAaPmAgInS	foralnout
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
V	V	kA	V
<g/>
)	)	kIx)	)
<g/>
((	((	k?	((
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
Důkaz	důkaz	k1gInSc4	důkaz
<g/>
:	:	kIx,	:
Plyne	plynout	k5eAaImIp3nS	plynout
z	z	k7c2	z
předchozího	předchozí	k2eAgNnSc2d1	předchozí
tvrzení	tvrzení	k1gNnSc2	tvrzení
položením	položení	k1gNnSc7	položení
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
aritmetických	aritmetický	k2eAgFnPc2d1	aritmetická
operací	operace	k1gFnPc2	operace
definovaných	definovaný	k2eAgFnPc2d1	definovaná
nad	nad	k7c7	nad
samotnými	samotný	k2eAgInPc7d1	samotný
vektory	vektor	k1gInPc7	vektor
a	a	k8xC	a
prvky	prvek	k1gInPc1	prvek
tělesa	těleso	k1gNnSc2	těleso
můžeme	moct	k5eAaImIp1nP	moct
též	též	k9	též
uvažovat	uvažovat	k5eAaImF	uvažovat
obdobné	obdobný	k2eAgFnPc4d1	obdobná
operace	operace	k1gFnPc4	operace
nad	nad	k7c7	nad
celými	celý	k2eAgFnPc7d1	celá
množinami	množina	k1gFnPc7	množina
vektorů	vektor	k1gInPc2	vektor
potažmo	potažmo	k6eAd1	potažmo
prvků	prvek	k1gInPc2	prvek
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Zavedení	zavedení	k1gNnSc1	zavedení
těchto	tento	k3xDgFnPc2	tento
operací	operace	k1gFnPc2	operace
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
kompaktnější	kompaktní	k2eAgInSc4d2	kompaktnější
zápis	zápis	k1gInSc4	zápis
některých	některý	k3yIgInPc2	některý
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c7	mezi
množinami	množina	k1gFnPc7	množina
vektorů	vektor	k1gInPc2	vektor
a	a	k8xC	a
dává	dávat	k5eAaImIp3nS	dávat
tak	tak	k6eAd1	tak
vyniknout	vyniknout	k5eAaPmF	vyniknout
jejich	jejich	k3xOp3gFnPc3	jejich
vzájemným	vzájemný	k2eAgFnPc3d1	vzájemná
souvislostem	souvislost	k1gFnPc3	souvislost
<g/>
.	.	kIx.	.
</s>
<s>
Mějme	mít	k5eAaImRp1nP	mít
vektorový	vektorový	k2eAgInSc4d1	vektorový
prostor	prostor	k1gInSc4	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	v	k7c4	v
<g/>
}	}	kIx)	}
nad	nad	k7c7	nad
tělesem	těleso	k1gNnSc7	těleso
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
uvažujme	uvažovat	k5eAaImRp1nP	uvažovat
dvě	dva	k4xCgFnPc4	dva
neprázdné	prázdný	k2eNgFnPc4d1	neprázdná
podmnožiny	podmnožina	k1gFnPc4	podmnožina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
A	a	k9	a
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
B	B	kA	B
<g/>
}	}	kIx)	}
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	V	kA	V
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tyto	tento	k3xDgInPc4	tento
můžeme	moct	k5eAaImIp1nP	moct
definovat	definovat	k5eAaBmF	definovat
jejich	jejich	k3xOp3gInSc4	jejich
součet	součet	k1gInSc4	součet
následujícím	následující	k2eAgInSc7d1	následující
způsobem	způsob	k1gInSc7	způsob
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	A	kA	A
+	+	kIx~	+
B	B	kA	B
≡	≡	k?	≡
{	{	kIx(	{
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
V	V	kA	V
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
(	(	kIx(	(
∃	∃	k?	∃
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
A	A	kA	A
)	)	kIx)	)
(	(	kIx(	(
∃	∃	k?	∃
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
→	→	k?	→
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
B	B	kA	B
)	)	kIx)	)
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
}	}	kIx)	}
=	=	kIx~	=
{	{	kIx(	{
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
<g />
.	.	kIx.	.
</s>
<s hack="1">
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
A	A	kA	A
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
B	B	kA	B
}	}	kIx)	}
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g/>
+	+	kIx~	+
<g/>
B	B	kA	B
<g/>
\	\	kIx~	\
<g/>
equiv	equiv	k6eAd1	equiv
\	\	kIx~	\
<g/>
{{	{{	k?	{{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
V	V	kA	V
<g/>
|	|	kIx~	|
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
exists	exists	k6eAd1	exists
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
<g/>
<g />
.	.	kIx.	.
</s>
<s>
\	\	kIx~	\
<g/>
in	in	k?	in
A	A	kA	A
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
exists	exists	k6eAd1	exists
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
B	B	kA	B
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
{{	{{	k?	{{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}}	}}	k?	}}
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
A	A	kA	A
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
B	B	kA	B
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Součet	součet	k1gInSc1	součet
dvou	dva	k4xCgFnPc2	dva
podmnožin	podmnožina	k1gFnPc2	podmnožina
vektorového	vektorový	k2eAgNnSc2d1	vektorové
<g />
.	.	kIx.	.
</s>
<s hack="1">
prostoru	prostor	k1gInSc2	prostor
nazýváme	nazývat	k5eAaImIp1nP	nazývat
direktní	direktní	k2eAgInSc4d1	direktní
součet	součet	k1gInSc4	součet
<g/>
,	,	kIx,	,
právě	právě	k6eAd1	právě
když	když	k8xS	když
lze	lze	k6eAd1	lze
každý	každý	k3xTgInSc4	každý
vektor	vektor	k1gInSc4	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
z	z	k7c2	z
množiny	množina	k1gFnSc2	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
A	A	kA	A
+	+	kIx~	+
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
A	a	k9	a
<g/>
+	+	kIx~	+
<g/>
B	B	kA	B
<g/>
}	}	kIx)	}
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
<g />
.	.	kIx.	.
</s>
<s hack="1">
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}}}	}}}	k?	}}}
právě	právě	k6eAd1	právě
jedním	jeden	k4xCgInSc7	jeden
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
A	A	kA	A
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
A	a	k9	a
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
B	B	kA	B
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Direktní	direktní	k2eAgInSc4d1	direktní
součet	součet	k1gInSc4	součet
množin	množina	k1gFnPc2	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
A	a	k9	a
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
B	B	kA	B
<g/>
}	}	kIx)	}
značíme	značit	k5eAaImIp1nP	značit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
⊕	⊕	k?	⊕
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
A	a	k9	a
<g/>
\	\	kIx~	\
<g/>
oplus	oplus	k1gInSc4	oplus
B	B	kA	B
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Neboli	neboli	k8xC	neboli
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
⊕	⊕	k?	⊕
B	B	kA	B
≡	≡	k?	≡
{	{	kIx(	{
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
V	V	kA	V
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
(	(	kIx(	(
∃	∃	k?	∃
!	!	kIx.	!
</s>
<s desamb="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
A	A	kA	A
)	)	kIx)	)
(	(	kIx(	(
∃	∃	k?	∃
!	!	kIx.	!
</s>
<s desamb="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
B	B	kA	B
)	)	kIx)	)
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
}	}	kIx)	}
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g/>
\	\	kIx~	\
<g/>
oplus	oplus	k1gInSc1	oplus
B	B	kA	B
<g/>
\	\	kIx~	\
<g/>
equiv	equiv	k6eAd1	equiv
\	\	kIx~	\
<g/>
{{	{{	k?	{{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
V	V	kA	V
<g/>
|	|	kIx~	|
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
exists	exists	k1gInSc1	exists
!	!	kIx.	!
</s>
<s>
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
A	A	kA	A
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
exists	exists	k1gInSc1	exists
!	!	kIx.	!
</s>
<s>
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
B	B	kA	B
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
vec	vec	k?	vec
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Uvažujeme	uvažovat	k5eAaImIp1nP	uvažovat
<g/>
-li	i	k?	-li
ještě	ještě	k9	ještě
neprázdnou	prázdný	k2eNgFnSc4d1	neprázdná
podmnožinu	podmnožina	k1gFnSc4	podmnožina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
S	s	k7c7	s
<g/>
}	}	kIx)	}
tělesa	těleso	k1gNnPc4	těleso
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
}	}	kIx)	}
,	,	kIx,	,
tak	tak	k6eAd1	tak
jako	jako	k8xC	jako
násobek	násobek	k1gInSc4	násobek
množiny	množina	k1gFnSc2	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
S	s	k7c7	s
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
A	a	k9	a
<g/>
}	}	kIx)	}
označujeme	označovat	k5eAaImIp1nP	označovat
množinu	množina	k1gFnSc4	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
⋅	⋅	k?	⋅
A	a	k8xC	a
≡	≡	k?	≡
{	{	kIx(	{
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
V	V	kA	V
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
(	(	kIx(	(
∃	∃	k?	∃
α	α	k?	α
∈	∈	k?	∈
S	s	k7c7	s
)	)	kIx)	)
(	(	kIx(	(
∃	∃	k?	∃
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
A	A	kA	A
)	)	kIx)	)
(	(	kIx(	(
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
}	}	kIx)	}
=	=	kIx~	=
{	{	kIx(	{
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
α	α	k?	α
∈	∈	k?	∈
S	s	k7c7	s
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
A	A	kA	A
}	}	kIx)	}
.	.	kIx.	.
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
S	s	k7c7	s
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
A	a	k9	a
<g/>
\	\	kIx~	\
<g/>
equiv	equiv	k6eAd1	equiv
\	\	kIx~	\
<g/>
{{	{{	k?	{{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
V	V	kA	V
<g/>
|	|	kIx~	|
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
exists	exists	k1gInSc1	exists
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
\	\	kIx~	\
<g/>
in	in	k?	in
S	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
exists	exists	k6eAd1	exists
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
A	A	kA	A
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
<g />
.	.	kIx.	.
</s>
<s>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
\	\	kIx~	\
<g/>
in	in	k?	in
S	s	k7c7	s
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
in	in	k?	in
A	A	kA	A
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
zjednodušení	zjednodušení	k1gNnSc4	zjednodušení
zápisu	zápis	k1gInSc2	zápis
používají	používat	k5eAaImIp3nP	používat
následující	následující	k2eAgFnPc4d1	následující
konvence	konvence	k1gFnPc4	konvence
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
V	V	kA	V
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
-	-	kIx~	-
1	[number]	k4	1
}	}	kIx)	}
⋅	⋅	k?	⋅
A	a	k8xC	a
≡	≡	k?	≡
-	-	kIx~	-
A	A	kA	A
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
A	A	kA	A
<g/>
\	\	kIx~	\
<g/>
equiv	equiv	k6eAd1	equiv
-A	-A	k?	-A
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
}	}	kIx)	}
+	+	kIx~	+
A	a	k9	a
≡	≡	k?	≡
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
A	a	k9	a
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
{{	{{	k?	{{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
A	a	k9	a
<g/>
\	\	kIx~	\
<g/>
equiv	equiv	k6eAd1	equiv
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
A	a	k9	a
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
+	+	kIx~	+
(	(	kIx(	(
-	-	kIx~	-
B	B	kA	B
)	)	kIx)	)
≡	≡	k?	≡
A	A	kA	A
-	-	kIx~	-
B	B	kA	B
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
A	a	k9	a
<g/>
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
-B	-B	k?	-B
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
equiv	equiv	k6eAd1	equiv
A-B	A-B	k1gFnSc1	A-B
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
α	α	k?	α
}	}	kIx)	}
⋅	⋅	k?	⋅
A	a	k8xC	a
≡	≡	k?	≡
α	α	k?	α
A	A	kA	A
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
A	A	kA	A
<g/>
\	\	kIx~	\
<g/>
equiv	equiv	k6eAd1	equiv
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
A.	A.	kA	A.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Pro	pro	k7c4	pro
operace	operace	k1gFnPc4	operace
sčítání	sčítání	k1gNnSc2	sčítání
a	a	k8xC	a
násobení	násobení	k1gNnSc2	násobení
<g />
.	.	kIx.	.
</s>
<s hack="1">
nad	nad	k7c7	nad
podmnožinami	podmnožina	k1gFnPc7	podmnožina
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
snadno	snadno	k6eAd1	snadno
odvodit	odvodit	k5eAaPmF	odvodit
následující	následující	k2eAgFnPc4d1	následující
vlastnosti	vlastnost	k1gFnPc4	vlastnost
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
⊂	⊂	k?	⊂
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
S_	S_	k1gFnSc6	S_
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
S_	S_	k1gMnSc1	S_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
subset	subset	k1gInSc1	subset
T	T	kA	T
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
:	:	kIx,	:
operace	operace	k1gFnPc1	operace
sčítání	sčítání	k1gNnSc2	sčítání
množin	množina	k1gFnPc2	množina
je	být	k5eAaImIp3nS	být
komutativní	komutativní	k2eAgFnSc1d1	komutativní
a	a	k8xC	a
asociativní	asociativní	k2eAgFnSc1d1	asociativní
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
A	a	k9	a
,	,	kIx,	,
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
A	a	k9	a
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
A	a	k9	a
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
1	[number]	k4	1
}	}	kIx)	}
⋅	⋅	k?	⋅
A	A	kA	A
=	=	kIx~	=
A	A	kA	A
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
A	A	kA	A
<g/>
=	=	kIx~	=
<g/>
A	A	kA	A
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
⋅	⋅	k?	⋅
(	(	kIx(	(
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
⋅	⋅	k?	⋅
A	A	kA	A
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s hack="1">
=	=	kIx~	=
(	(	kIx(	(
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
⋅	⋅	k?	⋅
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
⋅	⋅	k?	⋅
A	A	kA	A
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
S_	S_	k1gMnSc6	S_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
(	(	kIx(	(
<g/>
S_	S_	k1gMnSc1	S_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
A	A	kA	A
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
S_	S_	k1gMnSc1	S_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
S_	S_	k1gMnSc1	S_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
A	A	kA	A
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
⋅	⋅	k?	⋅
(	(	kIx(	(
A	a	k9	a
<g />
.	.	kIx.	.
</s>
<s hack="1">
+	+	kIx~	+
B	B	kA	B
)	)	kIx)	)
⊂	⊂	k?	⊂
S	s	k7c7	s
⋅	⋅	k?	⋅
A	A	kA	A
+	+	kIx~	+
S	s	k7c7	s
⋅	⋅	k?	⋅
B	B	kA	B
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
S	s	k7c7	s
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
+	+	kIx~	+
<g/>
B	B	kA	B
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
subset	subset	k1gMnSc1	subset
S	s	k7c7	s
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
A	A	kA	A
<g/>
+	+	kIx~	+
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
S	s	k7c7	s
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
B	B	kA	B
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
⋅	⋅	k?	⋅
A	a	k8xC	a
⊂	⊂	k?	⊂
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
⋅	⋅	k?	⋅
A	A	kA	A
+	+	kIx~	+
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
⋅	⋅	k?	⋅
A	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s hack="1">
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
(	(	kIx(	(
<g/>
S_	S_	k1gMnSc6	S_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
S_	S_	k1gMnSc1	S_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
A	A	kA	A
<g/>
\	\	kIx~	\
<g/>
subset	subset	k1gMnSc1	subset
S_	S_	k1gMnSc1	S_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
A	A	kA	A
<g/>
+	+	kIx~	+
<g/>
S_	S_	k1gMnSc1	S_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
A	A	kA	A
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
platí	platit	k5eAaImIp3nS	platit
zřejmě	zřejmě	k6eAd1	zřejmě
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
A	A	kA	A
-	-	kIx~	-
A	A	kA	A
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
A-A	A-A	k1gFnSc1	A-A
<g/>
}	}	kIx)	}
,	,	kIx,	,
ale	ale	k8xC	ale
obecně	obecně	k6eAd1	obecně
rozhodně	rozhodně	k6eAd1	rozhodně
neplatí	platit	k5eNaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
rozdíl	rozdíl	k1gInSc1	rozdíl
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	A	kA	A
-	-	kIx~	-
A	A	kA	A
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
A-A	A-A	k1gMnSc6	A-A
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
byla	být	k5eAaImAgFnS	být
množina	množina	k1gFnSc1	množina
obsahující	obsahující	k2eAgFnSc1d1	obsahující
jen	jen	k9	jen
nulový	nulový	k2eAgInSc4d1	nulový
vektor	vektor	k1gInSc4	vektor
<g/>
;	;	kIx,	;
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
A	a	k9	a
<g/>
}	}	kIx)	}
vektorový	vektorový	k2eAgInSc1d1	vektorový
podprostor	podprostor	k1gInSc1	podprostor
prostoru	prostor	k1gInSc2	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	V	kA	V
<g/>
}	}	kIx)	}
,	,	kIx,	,
tak	tak	k9	tak
dokonce	dokonce	k9	dokonce
platí	platit	k5eAaImIp3nS	platit
rovnost	rovnost	k1gFnSc1	rovnost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	A	kA	A
-	-	kIx~	-
A	A	kA	A
=	=	kIx~	=
A	A	kA	A
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
A-A	A-A	k1gMnSc1	A-A
<g/>
=	=	kIx~	=
<g/>
A	A	kA	A
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
výše	vysoce	k6eAd2	vysoce
místo	místo	k7c2	místo
rovnosti	rovnost	k1gFnSc2	rovnost
vyznačená	vyznačený	k2eAgFnSc1d1	vyznačená
jen	jen	k9	jen
inkluze	inkluze	k1gFnSc1	inkluze
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
rovnost	rovnost	k1gFnSc1	rovnost
neplatí	platit	k5eNaImIp3nS	platit
<g/>
.	.	kIx.	.
</s>
<s>
Ukažme	ukázat	k5eAaPmRp1nP	ukázat
si	se	k3xPyFc3	se
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
inkluzi	inkluze	k1gFnSc3	inkluze
ještě	ještě	k6eAd1	ještě
jedno	jeden	k4xCgNnSc4	jeden
další	další	k2eAgNnSc4d1	další
tvrzení	tvrzení	k1gNnSc4	tvrzení
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
důkaz	důkaz	k1gInSc1	důkaz
je	být	k5eAaImIp3nS	být
triviální	triviální	k2eAgMnSc1d1	triviální
<g/>
:	:	kIx,	:
Buď	buď	k8xC	buď
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	v	k7c4	v
<g/>
}	}	kIx)	}
vektorový	vektorový	k2eAgInSc4d1	vektorový
prostor	prostor	k1gInSc4	prostor
nad	nad	k7c7	nad
tělesem	těleso	k1gNnSc7	těleso
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	A	kA	A
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
A_	A_	k1gMnSc6	A_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
B_	B_	k1gMnSc1	B_
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	A	kA	A
,	,	kIx,	,
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
A	A	kA	A
<g/>
,	,	kIx,	,
<g/>
B	B	kA	B
<g/>
}	}	kIx)	}
jeho	jeho	k3xOp3gFnPc4	jeho
neprázdné	prázdný	k2eNgFnPc4d1	neprázdná
podmnožiny	podmnožina	k1gFnPc4	podmnožina
splňující	splňující	k2eAgFnSc1d1	splňující
vztah	vztah	k1gInSc1	vztah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	A	kA	A
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
⊂	⊂	k?	⊂
A	A	kA	A
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
A_	A_	k1gMnSc6	A_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
subset	subset	k1gInSc1	subset
A	A	kA	A
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
⊂	⊂	k?	⊂
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
B_	B_	k1gMnSc6	B_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
subset	subset	k1gInSc1	subset
B	B	kA	B
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Buďte	budit	k5eAaImRp2nP	budit
dále	daleko	k6eAd2	daleko
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
S	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
S_	S_	k1gMnSc6	S_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
S	s	k7c7	s
<g/>
}	}	kIx)	}
neprázdné	prázdný	k2eNgFnSc2d1	neprázdná
podmnožiny	podmnožina	k1gFnSc2	podmnožina
tělesa	těleso	k1gNnSc2	těleso
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
}	}	kIx)	}
splňující	splňující	k2eAgFnSc4d1	splňující
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
⊂	⊂	k?	⊂
S	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
S_	S_	k1gMnSc6	S_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
subset	subset	k1gMnSc1	subset
S	s	k7c7	s
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
⋅	⋅	k?	⋅
:	:	kIx,	:
A	A	kA	A
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
⊂	⊂	k?	⊂
S	s	k7c7	s
⋅	⋅	k?	⋅
A	a	k9	a
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	A	kA	A
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
⊂	⊂	k?	⊂
A	A	kA	A
+	+	kIx~	+
B	B	kA	B
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
displaystyle	displaystyl	k1gInSc5	displaystyl
S_	S_	k1gMnSc6	S_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
A_	A_	k1gMnSc1	A_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
subset	subset	k1gMnSc1	subset
S	s	k7c7	s
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
A	A	kA	A
<g/>
\	\	kIx~	\
<g/>
quad	quad	k1gMnSc1	quad
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
quad	quad	k1gMnSc1	quad
A_	A_	k1gMnSc1	A_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
B_	B_	k1gMnSc1	B_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
subset	subset	k1gInSc1	subset
A	A	kA	A
<g/>
+	+	kIx~	+
<g/>
B.	B.	kA	B.
<g/>
}	}	kIx)	}
:	:	kIx,	:
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
direktním	direktní	k2eAgInSc7d1	direktní
součtem	součet	k1gInSc7	součet
dvou	dva	k4xCgInPc2	dva
vektorových	vektorový	k2eAgInPc2d1	vektorový
podprostorů	podprostor	k1gInPc2	podprostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	A	kA	A
,	,	kIx,	,
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
A	a	k9	a
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
,	,	kIx,	,
<g/>
B	B	kA	B
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
užitečné	užitečný	k2eAgNnSc1d1	užitečné
si	se	k3xPyFc3	se
uvézt	uvézt	k5eAaPmF	uvézt
následující	následující	k2eAgNnSc4d1	následující
tvrzení	tvrzení	k1gNnSc4	tvrzení
<g/>
:	:	kIx,	:
Součet	součet	k1gInSc1	součet
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	A	kA	A
+	+	kIx~	+
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
A	a	k9	a
<g/>
+	+	kIx~	+
<g/>
B	B	kA	B
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
direktním	direktní	k2eAgInSc7d1	direktní
součtem	součet	k1gInSc7	součet
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
A	a	k8xC	a
⊕	⊕	k?	⊕
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
A	a	k9	a
<g/>
\	\	kIx~	\
<g/>
oplus	oplus	k1gInSc1	oplus
B	B	kA	B
<g/>
}	}	kIx)	}
,	,	kIx,	,
právě	právě	k6eAd1	právě
když	když	k8xS	když
v	v	k7c6	v
průniku	průnik	k1gInSc6	průnik
podprostorů	podprostor	k1gInPc2	podprostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
A	a	k9	a
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
B	B	kA	B
<g/>
}	}	kIx)	}
leží	ležet	k5eAaImIp3nS	ležet
právě	právě	k9	právě
jen	jen	k9	jen
nulový	nulový	k2eAgInSc4d1	nulový
vektor	vektor	k1gInSc4	vektor
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jest	být	k5eAaImIp3nS	být
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	A	kA	A
+	+	kIx~	+
B	B	kA	B
=	=	kIx~	=
A	A	kA	A
⊕	⊕	k?	⊕
B	B	kA	B
:	:	kIx,	:
⇔	⇔	k?	⇔
:	:	kIx,	:
A	a	k9	a
∩	∩	k?	∩
B	B	kA	B
=	=	kIx~	=
{	{	kIx(	{
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
}	}	kIx)	}
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g/>
+	+	kIx~	+
<g/>
B	B	kA	B
<g/>
=	=	kIx~	=
<g/>
A	A	kA	A
<g/>
\	\	kIx~	\
<g/>
oplus	oplus	k1gMnSc1	oplus
B	B	kA	B
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
quad	quad	k1gMnSc1	quad
\	\	kIx~	\
<g/>
Leftrightarrow	Leftrightarrow	k1gMnSc1	Leftrightarrow
\	\	kIx~	\
<g/>
quad	quad	k1gMnSc1	quad
A	A	kA	A
<g/>
\	\	kIx~	\
<g/>
cap	cap	k1gMnSc1	cap
B	B	kA	B
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
{{	{{	k?	{{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Pro	pro	k7c4	pro
důkaz	důkaz	k1gInSc4	důkaz
viz	vidět	k5eAaImRp2nS	vidět
oddíl	oddíl	k1gInSc1	oddíl
Rovnosti	rovnost	k1gFnPc1	rovnost
a	a	k8xC	a
inkluze	inkluze	k1gFnPc1	inkluze
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vektorový	vektorový	k2eAgInSc4d1	vektorový
podprostor	podprostor	k1gInSc4	podprostor
<g/>
.	.	kIx.	.
</s>
<s>
Uveďme	uvést	k5eAaPmRp1nP	uvést
si	se	k3xPyFc3	se
nejprve	nejprve	k6eAd1	nejprve
několik	několik	k4yIc1	několik
pojmů	pojem	k1gInPc2	pojem
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
úzce	úzko	k6eAd1	úzko
vážou	vázat	k5eAaImIp3nP	vázat
k	k	k7c3	k
vektorům	vektor	k1gInPc3	vektor
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc3	jejich
množinám	množina	k1gFnPc3	množina
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
pojmy	pojem	k1gInPc1	pojem
nám	my	k3xPp1nPc3	my
pak	pak	k6eAd1	pak
pomohou	pomoct	k5eAaPmIp3nP	pomoct
lépe	dobře	k6eAd2	dobře
pochopit	pochopit	k5eAaPmF	pochopit
strukturu	struktura	k1gFnSc4	struktura
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
a	a	k8xC	a
umožní	umožnit	k5eAaPmIp3nS	umožnit
i	i	k9	i
snazší	snadný	k2eAgNnSc1d2	snazší
zacházení	zacházení	k1gNnSc1	zacházení
s	s	k7c7	s
vektory	vektor	k1gInPc7	vektor
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Lineární	lineární	k2eAgFnSc2d1	lineární
kombinace	kombinace	k1gFnSc2	kombinace
<g/>
.	.	kIx.	.
</s>
<s>
Nejzákladnější	základní	k2eAgFnSc1d3	nejzákladnější
věc	věc	k1gFnSc1	věc
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
můžeme	moct	k5eAaImIp1nP	moct
s	s	k7c7	s
vektory	vektor	k1gInPc7	vektor
udělat	udělat	k5eAaPmF	udělat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
sečíst	sečíst	k5eAaPmF	sečíst
je	on	k3xPp3gInPc4	on
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
vynásobit	vynásobit	k5eAaPmF	vynásobit
prvkem	prvek	k1gInSc7	prvek
z	z	k7c2	z
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
budeme	být	k5eAaImBp1nP	být
mít	mít	k5eAaImF	mít
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
k	k	k7c3	k
<g/>
}	}	kIx)	}
vektorů	vektor	k1gInPc2	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
...	...	k?	...
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}}	}}	k?	}}
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
každý	každý	k3xTgMnSc1	každý
vynásobíme	vynásobit	k5eAaPmIp1nP	vynásobit
<g />
.	.	kIx.	.
</s>
<s hack="1">
prvkem	prvek	k1gInSc7	prvek
z	z	k7c2	z
tělesa	těleso	k1gNnSc2	těleso
(	(	kIx(	(
<g/>
prvky	prvek	k1gInPc4	prvek
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
...	...	k?	...
,	,	kIx,	,
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}}	}}	k?	}}
)	)	kIx)	)
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
je	být	k5eAaImIp3nS	být
všechny	všechen	k3xTgMnPc4	všechen
sečteme	sečíst	k5eAaPmIp1nP	sečíst
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
dostaneme	dostat	k5eAaPmIp1nP	dostat
vektor	vektor	k1gInSc4	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
...	...	k?	...
+	+	kIx~	+
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
i	i	k9	i
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k6eAd1	ldots
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s>
k	k	k7c3	k
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Tento	tento	k3xDgInSc4	tento
vektor	vektor	k1gInSc4	vektor
nazýváme	nazývat	k5eAaImIp1nP	nazývat
lineární	lineární	k2eAgFnPc4d1	lineární
kombinace	kombinace	k1gFnPc4	kombinace
vektorů	vektor	k1gInPc2	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
...	...	k?	...
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Lineární	lineární	k2eAgFnSc4d1	lineární
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
lineární	lineární	k2eAgFnSc2d1	lineární
nezávislosti	nezávislost	k1gFnSc2	nezávislost
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
intuitivní	intuitivní	k2eAgInSc1d1	intuitivní
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
některé	některý	k3yIgInPc1	některý
vektory	vektor	k1gInPc1	vektor
si	se	k3xPyFc3	se
jsou	být	k5eAaImIp3nP	být
podobnější	podobný	k2eAgNnSc4d2	podobnější
než	než	k8xS	než
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
.	.	kIx.	.
</s>
<s>
Máme	mít	k5eAaImIp1nP	mít
<g/>
-li	i	k?	-li
například	například	k6eAd1	například
dvě	dva	k4xCgFnPc1	dva
šipky	šipka	k1gFnPc1	šipka
stejného	stejný	k2eAgInSc2d1	stejný
směru	směr	k1gInSc2	směr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
různé	různý	k2eAgFnPc4d1	různá
délky	délka	k1gFnPc4	délka
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
dokážeme	dokázat	k5eAaPmIp1nP	dokázat
jednu	jeden	k4xCgFnSc4	jeden
šipku	šipka	k1gFnSc4	šipka
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k9	jako
násobek	násobek	k1gInSc1	násobek
druhé	druhý	k4xOgFnSc2	druhý
šipky	šipka	k1gFnSc2	šipka
<g/>
.	.	kIx.	.
</s>
<s>
Šipky	šipka	k1gFnPc1	šipka
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
v	v	k7c6	v
jistém	jistý	k2eAgInSc6d1	jistý
smyslu	smysl	k1gInSc6	smysl
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
závislé	závislý	k2eAgInPc1d1	závislý
<g/>
.	.	kIx.	.
</s>
<s>
Budeme	být	k5eAaImBp1nP	být
<g/>
-li	i	k?	-li
ale	ale	k8xC	ale
mít	mít	k5eAaImF	mít
dvě	dva	k4xCgFnPc1	dva
šipky	šipka	k1gFnPc1	šipka
různých	různý	k2eAgInPc2d1	různý
směrů	směr	k1gInPc2	směr
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
jednu	jeden	k4xCgFnSc4	jeden
nedokážeme	dokázat	k5eNaPmIp1nP	dokázat
vhodným	vhodný	k2eAgInSc7d1	vhodný
násobkem	násobek	k1gInSc7	násobek
převést	převést	k5eAaPmF	převést
na	na	k7c4	na
šipku	šipka	k1gFnSc4	šipka
druhou	druhý	k4xOgFnSc4	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
šipky	šipka	k1gFnPc1	šipka
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
nezávislé	závislý	k2eNgNnSc4d1	nezávislé
<g/>
.	.	kIx.	.
</s>
<s>
Obecněji	obecně	k6eAd2	obecně
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
máme	mít	k5eAaImIp1nP	mít
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
k	k	k7c3	k
<g/>
}	}	kIx)	}
šipek	šipka	k1gFnPc2	šipka
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
k	k	k7c3	k
<g/>
}	}	kIx)	}
-tou	ou	k6eAd1	-tou
šipku	šipka	k1gFnSc4	šipka
jsme	být	k5eAaImIp1nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k8xS	jako
lineární	lineární	k2eAgFnSc4d1	lineární
kombinaci	kombinace	k1gFnSc4	kombinace
předchozích	předchozí	k2eAgMnPc2d1	předchozí
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
-	-	kIx~	-
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
k-	k-	k?	k-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
šipek	šipka	k1gFnPc2	šipka
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
říkáme	říkat	k5eAaImIp1nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc1	tento
šipky	šipka	k1gFnPc1	šipka
alias	alias	k9	alias
vektory	vektor	k1gInPc1	vektor
lineárně	lineárně	k6eAd1	lineárně
závislé	závislý	k2eAgInPc1d1	závislý
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
nejsme	být	k5eNaImIp1nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
žádnou	žádný	k3yNgFnSc7	žádný
ze	z	k7c2	z
šipek	šipka	k1gFnPc2	šipka
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k9	jako
lineární	lineární	k2eAgFnSc4d1	lineární
kombinaci	kombinace	k1gFnSc4	kombinace
šipek	šipka	k1gFnPc2	šipka
ostatních	ostatní	k2eAgMnPc2d1	ostatní
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
říkáme	říkat	k5eAaImIp1nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
tyto	tento	k3xDgFnPc1	tento
šipky	šipka	k1gFnPc1	šipka
lineárně	lineárně	k6eAd1	lineárně
nezávislé	závislý	k2eNgFnPc1d1	nezávislá
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dimenze	dimenze	k1gFnSc2	dimenze
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Vraťme	vrátit	k5eAaPmRp1nP	vrátit
se	se	k3xPyFc4	se
na	na	k7c4	na
chvíli	chvíle	k1gFnSc4	chvíle
k	k	k7c3	k
oddílu	oddíl	k1gInSc3	oddíl
Motivace	motivace	k1gFnSc2	motivace
výše	výše	k1gFnSc2	výše
na	na	k7c4	na
případ	případ	k1gInSc4	případ
šipek	šipka	k1gFnPc2	šipka
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
a	a	k8xC	a
na	na	k7c4	na
polynomy	polynom	k1gInPc4	polynom
<g/>
.	.	kIx.	.
</s>
<s>
Abychom	aby	kYmCp1nP	aby
mohli	moct	k5eAaImAgMnP	moct
šipky	šipka	k1gFnSc2	šipka
vhodně	vhodně	k6eAd1	vhodně
popisovat	popisovat	k5eAaImF	popisovat
<g/>
,	,	kIx,	,
zavedli	zavést	k5eAaPmAgMnP	zavést
jsme	být	k5eAaImIp1nP	být
si	se	k3xPyFc3	se
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
souřadnicovou	souřadnicový	k2eAgFnSc4d1	souřadnicová
soustavu	soustava	k1gFnSc4	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
šipce	šipka	k1gFnSc3	šipka
jsme	být	k5eAaImIp1nP	být
tedy	tedy	k9	tedy
schopni	schopen	k2eAgMnPc1d1	schopen
jednoznačně	jednoznačně	k6eAd1	jednoznačně
přiřadit	přiřadit	k5eAaPmF	přiřadit
dvojici	dvojice	k1gFnSc4	dvojice
čísel	číslo	k1gNnPc2	číslo
-	-	kIx~	-
její	její	k3xOp3gInSc1	její
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
x	x	k?	x
<g/>
}	}	kIx)	}
-ovou	vou	k6eAd1	-ovou
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
y	y	k?	y
<g/>
}	}	kIx)	}
-ovou	vý	k2eAgFnSc4d1	-ový
souřadnici	souřadnice	k1gFnSc4	souřadnice
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
souřadnicové	souřadnicový	k2eAgFnSc6d1	souřadnicová
soustavě	soustava	k1gFnSc6	soustava
<g/>
.	.	kIx.	.
</s>
<s>
Vidíme	vidět	k5eAaImIp1nP	vidět
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
že	že	k8xS	že
nám	my	k3xPp1nPc3	my
k	k	k7c3	k
plnému	plný	k2eAgNnSc3d1	plné
určení	určení	k1gNnSc3	určení
šipky	šipka	k1gFnSc2	šipka
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
stačí	stačit	k5eAaBmIp3nS	stačit
dvě	dva	k4xCgNnPc4	dva
čísla	číslo	k1gNnPc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Zkoumejme	zkoumat	k5eAaImRp1nP	zkoumat
nyní	nyní	k6eAd1	nyní
případ	případ	k1gInSc4	případ
polynomů	polynom	k1gInPc2	polynom
<g/>
.	.	kIx.	.
</s>
<s>
Označme	označit	k5eAaPmRp1nP	označit
si	se	k3xPyFc3	se
množinu	množina	k1gFnSc4	množina
všech	všecek	k3xTgInPc2	všecek
polynomů	polynom	k1gInPc2	polynom
jako	jako	k8xS	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
P	P	kA	P
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
obecný	obecný	k2eAgInSc1d1	obecný
tvar	tvar	k1gInSc1	tvar
polynomu	polynom	k1gInSc2	polynom
vypadá	vypadat	k5eAaPmIp3nS	vypadat
následovně	následovně	k6eAd1	následovně
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
-	-	kIx~	-
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
-	-	kIx~	-
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
...	...	k?	...
+	+	kIx~	+
:	:	kIx,	:
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
+	+	kIx~	+
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
a_	a_	k?	a_
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
n-	n-	k?	n-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
x	x	k?	x
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n-	n-	k?	n-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
+	+	kIx~	+
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
x	x	k?	x
<g/>
+	+	kIx~	+
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
n	n	k0	n
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
nějaké	nějaký	k3yIgNnSc4	nějaký
přirozené	přirozený	k2eAgNnSc4d1	přirozené
číslo	číslo	k1gNnSc4	číslo
vyjadřující	vyjadřující	k2eAgInSc1d1	vyjadřující
stupeň	stupeň	k1gInSc1	stupeň
polynomu	polynom	k1gInSc2	polynom
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
nenaložíme	naložit	k5eNaPmIp1nP	naložit
omezení	omezení	k1gNnSc4	omezení
na	na	k7c4	na
velikost	velikost	k1gFnSc4	velikost
tohoto	tento	k3xDgNnSc2	tento
čísla	číslo	k1gNnSc2	číslo
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
může	moct	k5eAaImIp3nS	moct
nabývat	nabývat	k5eAaImF	nabývat
libovolně	libovolně	k6eAd1	libovolně
velké	velký	k2eAgFnPc4d1	velká
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Máme	mít	k5eAaImIp1nP	mít
<g/>
-li	i	k?	-li
tedy	tedy	k8xC	tedy
množinu	množina	k1gFnSc4	množina
všech	všecek	k3xTgInPc2	všecek
polynomů	polynom	k1gInPc2	polynom
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
P	P	kA	P
<g/>
}	}	kIx)	}
,	,	kIx,	,
tak	tak	k9	tak
ať	ať	k9	ať
nás	my	k3xPp1nPc4	my
napadne	napadnout	k5eAaPmIp3nS	napadnout
jakkoli	jakkoli	k6eAd1	jakkoli
velké	velký	k2eAgNnSc1d1	velké
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
n	n	k0	n
<g/>
}	}	kIx)	}
,	,	kIx,	,
tak	tak	k6eAd1	tak
určitě	určitě	k6eAd1	určitě
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
množině	množina	k1gFnSc6	množina
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
polynom	polynom	k1gInSc4	polynom
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
stupeň	stupeň	k1gInSc1	stupeň
je	být	k5eAaImIp3nS	být
právě	právě	k6eAd1	právě
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
n	n	k0	n
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
polynom	polynom	k1gInSc1	polynom
je	být	k5eAaImIp3nS	být
popsán	popsán	k2eAgInSc1d1	popsán
svými	svůj	k3xOyFgInPc7	svůj
koeficienty	koeficient	k1gInPc7	koeficient
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
ale	ale	k8xC	ale
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
n	n	k0	n
<g/>
}	}	kIx)	}
libovolně	libovolně	k6eAd1	libovolně
velké	velký	k2eAgNnSc1d1	velké
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
libovolně	libovolně	k6eAd1	libovolně
velký	velký	k2eAgInSc1d1	velký
i	i	k8xC	i
počet	počet	k1gInSc1	počet
koeficientů	koeficient	k1gInPc2	koeficient
polynomu	polynom	k1gInSc2	polynom
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
"	"	kIx"	"
<g/>
náhodně	náhodně	k6eAd1	náhodně
vytáhneme	vytáhnout	k5eAaPmIp1nP	vytáhnout
<g/>
"	"	kIx"	"
z	z	k7c2	z
množiny	množina	k1gFnSc2	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
P	P	kA	P
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
dopředu	dopředu	k6eAd1	dopředu
nevíme	vědět	k5eNaImIp1nP	vědět
<g/>
,	,	kIx,	,
jaký	jaký	k3yRgInSc1	jaký
polynom	polynom	k1gInSc1	polynom
"	"	kIx"	"
<g/>
vytáhneme	vytáhnout	k5eAaPmIp1nP	vytáhnout
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
musíme	muset	k5eAaImIp1nP	muset
uvažovat	uvažovat	k5eAaImF	uvažovat
nejhorší	zlý	k2eAgInSc4d3	Nejhorší
možný	možný	k2eAgInSc4d1	možný
případ	případ	k1gInSc4	případ
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
koeficientů	koeficient	k1gInPc2	koeficient
nekonečně	konečně	k6eNd1	konečně
mnoho	mnoho	k4c1	mnoho
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
popis	popis	k1gInSc4	popis
libovolného	libovolný	k2eAgInSc2d1	libovolný
polynomu	polynom	k1gInSc2	polynom
z	z	k7c2	z
množiny	množina	k1gFnSc2	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
P	P	kA	P
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nutno	nutno	k6eAd1	nutno
nekonečně	konečně	k6eNd1	konečně
mnoho	mnoho	k4c4	mnoho
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
protikladu	protiklad	k1gInSc6	protiklad
k	k	k7c3	k
případu	případ	k1gInSc3	případ
šipek	šipka	k1gFnPc2	šipka
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
<g/>
,	,	kIx,	,
k	k	k7c3	k
jejichž	jejichž	k3xOyRp3gNnSc3	jejichž
plnému	plný	k2eAgNnSc3d1	plné
určení	určení	k1gNnSc3	určení
stačila	stačit	k5eAaBmAgFnS	stačit
čísla	číslo	k1gNnPc4	číslo
dvě	dva	k4xCgFnPc4	dva
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
prostoru	prostor	k1gInSc6	prostor
šipek	šipka	k1gFnPc2	šipka
pak	pak	k6eAd1	pak
říkáme	říkat	k5eAaImIp1nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
dimenzi	dimenze	k1gFnSc4	dimenze
dva	dva	k4xCgInPc4	dva
<g/>
,	,	kIx,	,
o	o	k7c6	o
prostoru	prostor	k1gInSc6	prostor
všech	všecek	k3xTgInPc2	všecek
polynomů	polynom	k1gInPc2	polynom
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
P	P	kA	P
<g/>
}	}	kIx)	}
pak	pak	k6eAd1	pak
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
dimenze	dimenze	k1gFnSc1	dimenze
je	být	k5eAaImIp3nS	být
nekonečná	konečný	k2eNgFnSc1d1	nekonečná
<g/>
.	.	kIx.	.
</s>
<s>
Získali	získat	k5eAaPmAgMnP	získat
jsme	být	k5eAaImIp1nP	být
tak	tak	k6eAd1	tak
nejvýraznější	výrazný	k2eAgFnSc4d3	nejvýraznější
charakteristiku	charakteristika	k1gFnSc4	charakteristika
vektorových	vektorový	k2eAgInPc2d1	vektorový
prostorů	prostor	k1gInPc2	prostor
-	-	kIx~	-
buď	buď	k8xC	buď
mají	mít	k5eAaImIp3nP	mít
konečnou	konečný	k2eAgFnSc4d1	konečná
dimenzi	dimenze	k1gFnSc4	dimenze
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
nekonečnou	konečný	k2eNgFnSc7d1	nekonečná
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
dimenze	dimenze	k1gFnSc2	dimenze
lze	lze	k6eAd1	lze
formalizovat	formalizovat	k5eAaBmF	formalizovat
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
pokud	pokud	k8xS	pokud
ve	v	k7c6	v
vektorovém	vektorový	k2eAgInSc6d1	vektorový
prostoru	prostor	k1gInSc6	prostor
existuje	existovat	k5eAaImIp3nS	existovat
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
k	k	k7c3	k
<g/>
}	}	kIx)	}
vektorů	vektor	k1gInPc2	vektor
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
lineárně	lineárně	k6eAd1	lineárně
nezávislé	závislý	k2eNgFnPc1d1	nezávislá
<g/>
,	,	kIx,	,
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
každý	každý	k3xTgInSc4	každý
soubor	soubor	k1gInSc4	soubor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
+	+	kIx~	+
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
k	k	k7c3	k
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
vektorů	vektor	k1gInPc2	vektor
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
lineárně	lineárně	k6eAd1	lineárně
závislý	závislý	k2eAgInSc1d1	závislý
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
má	mít	k5eAaImIp3nS	mít
daný	daný	k2eAgInSc4d1	daný
vektorový	vektorový	k2eAgInSc4d1	vektorový
prostor	prostor	k1gInSc4	prostor
dimenzi	dimenze	k1gFnSc4	dimenze
rovnou	rovnou	k6eAd1	rovnou
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
k	k	k7c3	k
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
k	k	k7c3	k
<g/>
}	}	kIx)	}
najdu	najít	k5eAaPmIp1nS	najít
ve	v	k7c6	v
vektorovém	vektorový	k2eAgInSc6d1	vektorový
prostoru	prostor	k1gInSc6	prostor
lineárně	lineárně	k6eAd1	lineárně
nezávislý	závislý	k2eNgInSc1d1	nezávislý
soubor	soubor	k1gInSc1	soubor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
k	k	k7c3	k
<g/>
}	}	kIx)	}
vektorů	vektor	k1gInPc2	vektor
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
daný	daný	k2eAgInSc1d1	daný
vektorový	vektorový	k2eAgInSc1d1	vektorový
prostor	prostor	k1gInSc1	prostor
nekonečné	konečný	k2eNgFnSc2d1	nekonečná
dimenze	dimenze	k1gFnSc2	dimenze
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Báze	báze	k1gFnSc1	báze
(	(	kIx(	(
<g/>
lineární	lineární	k2eAgFnSc1d1	lineární
algebra	algebra	k1gFnSc1	algebra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
má	mít	k5eAaImIp3nS	mít
vektorový	vektorový	k2eAgInSc1d1	vektorový
prostor	prostor	k1gInSc1	prostor
dimenzi	dimenze	k1gFnSc3	dimenze
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
n	n	k0	n
<g/>
}	}	kIx)	}
,	,	kIx,	,
tak	tak	k8xC	tak
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
soubor	soubor	k1gInSc4	soubor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
n	n	k0	n
<g/>
}	}	kIx)	}
lineárně	lineárně	k6eAd1	lineárně
nezávislých	závislý	k2eNgInPc2d1	nezávislý
vektorů	vektor	k1gInPc2	vektor
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
všechny	všechen	k3xTgInPc1	všechen
vektory	vektor	k1gInPc1	vektor
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
vektorovém	vektorový	k2eAgInSc6d1	vektorový
prostoru	prostor	k1gInSc6	prostor
jsme	být	k5eAaImIp1nP	být
schopní	schopný	k2eAgMnPc1d1	schopný
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k9	jako
jistou	jistý	k2eAgFnSc4d1	jistá
lineární	lineární	k2eAgFnSc4d1	lineární
kombinaci	kombinace	k1gFnSc4	kombinace
těchto	tento	k3xDgFnPc2	tento
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
n	n	k0	n
<g/>
}	}	kIx)	}
vektorů	vektor	k1gInPc2	vektor
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
proto	proto	k8xC	proto
nazýváme	nazývat	k5eAaImIp1nP	nazývat
báze	báze	k1gFnPc4	báze
daného	daný	k2eAgInSc2d1	daný
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Posledně	posledně	k6eAd1	posledně
jmenovaná	jmenovaný	k2eAgFnSc1d1	jmenovaná
vlastnost	vlastnost	k1gFnSc1	vlastnost
plyne	plynout	k5eAaImIp3nS	plynout
z	z	k7c2	z
definice	definice	k1gFnSc2	definice
dimenze	dimenze	k1gFnSc2	dimenze
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Kdyby	kdyby	kYmCp3nS	kdyby
totiž	totiž	k9	totiž
existoval	existovat	k5eAaImAgInS	existovat
vektor	vektor	k1gInSc1	vektor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
nešlo	jít	k5eNaImAgNnS	jít
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k9	jako
lineární	lineární	k2eAgFnSc4d1	lineární
kombinaci	kombinace	k1gFnSc4	kombinace
výše	vysoce	k6eAd2	vysoce
zmíněných	zmíněný	k2eAgMnPc2d1	zmíněný
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
n	n	k0	n
<g/>
}	}	kIx)	}
vektorů	vektor	k1gInPc2	vektor
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
bychom	by	kYmCp1nP	by
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
soubor	soubor	k1gInSc4	soubor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
+	+	kIx~	+
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
n	n	k0	n
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
vektorů	vektor	k1gInPc2	vektor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
lineárně	lineárně	k6eAd1	lineárně
nezávislý	závislý	k2eNgInSc1d1	nezávislý
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
ve	v	k7c6	v
sporu	spor	k1gInSc6	spor
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
dimenze	dimenze	k1gFnSc1	dimenze
prostoru	prostor	k1gInSc2	prostor
rovna	roven	k2eAgFnSc1d1	rovna
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
n	n	k0	n
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vektorový	vektorový	k2eAgInSc4d1	vektorový
podprostor	podprostor	k1gInSc4	podprostor
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
řečeno	říct	k5eAaPmNgNnS	říct
výše	vysoce	k6eAd2	vysoce
<g/>
,	,	kIx,	,
podprostor	podprostor	k1gMnSc1	podprostor
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
je	být	k5eAaImIp3nS	být
taková	takový	k3xDgFnSc1	takový
jeho	jeho	k3xOp3gFnSc1	jeho
podmnožina	podmnožina	k1gFnSc1	podmnožina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
sama	sám	k3xTgFnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
vektorovým	vektorový	k2eAgInSc7d1	vektorový
prostorem	prostor	k1gInSc7	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Lineární	lineární	k2eAgInSc4d1	lineární
obal	obal	k1gInSc4	obal
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
speciální	speciální	k2eAgInSc4d1	speciální
případ	případ	k1gInSc4	případ
podprostoru	podprostor	k1gInSc2	podprostor
<g/>
.	.	kIx.	.
</s>
<s>
Mějme	mít	k5eAaImRp1nP	mít
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
k	k	k7c3	k
<g/>
}	}	kIx)	}
vektorů	vektor	k1gInPc2	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
,	,	kIx,	,
...	...	k?	...
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}}	}}	k?	}}
a	a	k8xC	a
uvažujme	uvažovat	k5eAaImRp1nP	uvažovat
všechny	všechen	k3xTgFnPc4	všechen
jejich	jejich	k3xOp3gFnPc4	jejich
možné	možný	k2eAgFnPc4d1	možná
lineární	lineární	k2eAgFnPc4d1	lineární
kombinace	kombinace	k1gFnPc4	kombinace
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
množinu	množina	k1gFnSc4	množina
všech	všecek	k3xTgFnPc2	všecek
těchto	tento	k3xDgFnPc2	tento
lineárních	lineární	k2eAgFnPc2d1	lineární
kombinací	kombinace	k1gFnPc2	kombinace
nazýváme	nazývat	k5eAaImIp1nP	nazývat
lineárním	lineární	k2eAgInSc7d1	lineární
obalem	obal	k1gInSc7	obal
vektorů	vektor	k1gInPc2	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
...	...	k?	...
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Že	že	k8xS	že
se	se	k3xPyFc4	se
skutečně	skutečně	k6eAd1	skutečně
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
podprostor	podprostor	k1gInSc4	podprostor
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
těžké	těžký	k2eAgNnSc1d1	těžké
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
.	.	kIx.	.
</s>
<s>
Uveďme	uvést	k5eAaPmRp1nP	uvést
si	se	k3xPyFc3	se
nyní	nyní	k6eAd1	nyní
příklady	příklad	k1gInPc4	příklad
nejčastěji	často	k6eAd3	často
používaných	používaný	k2eAgInPc2d1	používaný
vektorových	vektorový	k2eAgInPc2d1	vektorový
prostorů	prostor	k1gInPc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
numerické	numerický	k2eAgFnSc6d1	numerická
matematice	matematika	k1gFnSc6	matematika
nejčastěji	často	k6eAd3	často
používanými	používaný	k2eAgInPc7d1	používaný
vektorovými	vektorový	k2eAgInPc7d1	vektorový
prostory	prostor	k1gInPc7	prostor
jsou	být	k5eAaImIp3nP	být
ty	ten	k3xDgFnPc4	ten
konečnědimenzionální	konečnědimenzionální	k2eAgFnPc4d1	konečnědimenzionální
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
navíc	navíc	k6eAd1	navíc
definované	definovaný	k2eAgInPc1d1	definovaný
nad	nad	k7c7	nad
číselnými	číselný	k2eAgNnPc7d1	číselné
tělesy	těleso	k1gNnPc7	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc7d1	velká
výhodou	výhoda	k1gFnSc7	výhoda
prostorů	prostor	k1gInPc2	prostor
konečné	konečný	k2eAgFnSc2d1	konečná
dimenze	dimenze	k1gFnSc2	dimenze
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
lze	lze	k6eAd1	lze
snadno	snadno	k6eAd1	snadno
zavést	zavést	k5eAaPmF	zavést
bázi	báze	k1gFnSc4	báze
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
vektor	vektor	k1gInSc1	vektor
tak	tak	k9	tak
lze	lze	k6eAd1	lze
popsat	popsat	k5eAaPmF	popsat
pomocí	pomocí	k7c2	pomocí
jeho	jeho	k3xOp3gFnPc2	jeho
souřadnic	souřadnice	k1gFnPc2	souřadnice
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
bázi	báze	k1gFnSc6	báze
<g/>
.	.	kIx.	.
</s>
<s>
Souřadnice	souřadnice	k1gFnSc1	souřadnice
přitom	přitom	k6eAd1	přitom
tvoří	tvořit	k5eAaImIp3nS	tvořit
n-tice	nice	k1gFnPc4	n-tice
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
studiu	studio	k1gNnSc6	studio
libovolného	libovolný	k2eAgInSc2d1	libovolný
konečněrozměrného	konečněrozměrný	k2eAgInSc2d1	konečněrozměrný
prostoru	prostor	k1gInSc2	prostor
se	se	k3xPyFc4	se
tak	tak	k9	tak
stačí	stačit	k5eAaBmIp3nS	stačit
omezit	omezit	k5eAaPmF	omezit
na	na	k7c4	na
studium	studium	k1gNnSc4	studium
prostoru	prostor	k1gInSc2	prostor
n-tic	nic	k1gMnSc1	n-tic
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc1	jehož
vlastnosti	vlastnost	k1gFnPc1	vlastnost
si	se	k3xPyFc3	se
přiblížíme	přiblížit	k5eAaPmIp1nP	přiblížit
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
oddíle	oddíl	k1gInSc6	oddíl
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
pohledu	pohled	k1gInSc2	pohled
jsou	být	k5eAaImIp3nP	být
co	co	k9	co
do	do	k7c2	do
struktury	struktura	k1gFnSc2	struktura
mnohem	mnohem	k6eAd1	mnohem
bohatší	bohatý	k2eAgFnPc4d2	bohatší
prostory	prostora	k1gFnPc4	prostora
s	s	k7c7	s
nekonečnou	konečný	k2eNgFnSc7d1	nekonečná
dimenzí	dimenze	k1gFnSc7	dimenze
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
např.	např.	kA	např.
prostor	prostor	k1gInSc1	prostor
spojitých	spojitý	k2eAgFnPc2d1	spojitá
funkcí	funkce	k1gFnPc2	funkce
či	či	k8xC	či
prostor	prostora	k1gFnPc2	prostora
posloupností	posloupnost	k1gFnPc2	posloupnost
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
příklady	příklad	k1gInPc1	příklad
zde	zde	k6eAd1	zde
také	také	k9	také
uvádíme	uvádět	k5eAaImIp1nP	uvádět
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
příklady	příklad	k1gInPc1	příklad
vektorových	vektorový	k2eAgInPc2d1	vektorový
prostorů	prostor	k1gInPc2	prostor
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
i	i	k9	i
v	v	k7c6	v
oddíle	oddíl	k1gInSc6	oddíl
Vektorové	vektorový	k2eAgFnSc2d1	vektorová
prostory	prostora	k1gFnSc2	prostora
s	s	k7c7	s
dodatečnou	dodatečný	k2eAgFnSc7d1	dodatečná
strukturou	struktura	k1gFnSc7	struktura
níže	nízce	k6eAd2	nízce
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Aritmetický	aritmetický	k2eAgInSc4d1	aritmetický
vektor	vektor	k1gInSc4	vektor
<g/>
.	.	kIx.	.
</s>
<s>
Mějme	mít	k5eAaImRp1nP	mít
těleso	těleso	k1gNnSc4	těleso
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
}	}	kIx)	}
a	a	k8xC	a
jisté	jistý	k2eAgNnSc4d1	jisté
přirozené	přirozený	k2eAgNnSc4d1	přirozené
číslo	číslo	k1gNnSc4	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
n	n	k0	n
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Uvažujme	uvažovat	k5eAaImRp1nP	uvažovat
dále	daleko	k6eAd2	daleko
kartézský	kartézský	k2eAgInSc4d1	kartézský
součin	součin	k1gInSc4	součin
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
×	×	k?	×
...	...	k?	...
×	×	k?	×
T	T	kA	T
=	=	kIx~	=
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
\	\	kIx~	\
<g/>
times	times	k1gMnSc1	times
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
\	\	kIx~	\
<g/>
times	times	k1gInSc1	times
T	T	kA	T
<g/>
=	=	kIx~	=
<g/>
T	T	kA	T
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
,	,	kIx,	,
tj.	tj.	kA	tj.
prostor	prostor	k1gInSc1	prostor
uspořádaných	uspořádaný	k2eAgFnPc2d1	uspořádaná
n-tic	nice	k1gFnPc2	n-tice
prvků	prvek	k1gInPc2	prvek
z	z	k7c2	z
tělesa	těleso	k1gNnSc2	těleso
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
prostoru	prostor	k1gInSc6	prostor
si	se	k3xPyFc3	se
definujme	definovat	k5eAaBmRp1nP	definovat
operaci	operace	k1gFnSc4	operace
sčítání	sčítání	k1gNnSc4	sčítání
a	a	k8xC	a
operaci	operace	k1gFnSc4	operace
násobení	násobení	k1gNnSc2	násobení
prvkem	prvek	k1gInSc7	prvek
z	z	k7c2	z
tělesa	těleso	k1gNnSc2	těleso
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
Nechť	nechť	k9	nechť
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
...	...	k?	...
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
ldots	ldots	k1gInSc1	ldots
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
(	(	kIx(	(
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
...	...	k?	...
,	,	kIx,	,
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
n	n	k0	n
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
ldots	ldots	k1gInSc1	ldots
,	,	kIx,	,
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgFnPc4	dva
uspořádané	uspořádaný	k2eAgFnPc4d1	uspořádaná
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
n	n	k0	n
<g/>
}	}	kIx)	}
-tice	iec	k1gInSc2	-tiec
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc1	jejich
součet	součet	k1gInSc1	součet
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s hack="1">
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}}	}}}	k?	}}}
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
definován	definován	k2eAgInSc1d1	definován
<g />
.	.	kIx.	.
</s>
<s hack="1">
jako	jako	k8xC	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
,	,	kIx,	,
...	...	k?	...
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
tedy	tedy	k8xC	tedy
jako	jako	k9	jako
jiná	jiný	k2eAgFnSc1d1	jiná
uspořádaná	uspořádaný	k2eAgFnSc1d1	uspořádaná
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
n	n	k0	n
<g/>
}	}	kIx)	}
-tice	ice	k1gFnPc1	-tice
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc1	jejíž
složky	složka	k1gFnPc1	složka
jsou	být	k5eAaImIp3nP	být
rovny	roven	k2eAgFnPc1d1	rovna
součtům	součet	k1gInPc3	součet
složek	složka	k1gFnPc2	složka
dvou	dva	k4xCgFnPc6	dva
předešlých	předešlý	k2eAgFnPc2d1	předešlá
uspořádaných	uspořádaný	k2eAgFnPc2d1	uspořádaná
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
n	n	k0	n
<g/>
}	}	kIx)	}
-tic	ic	k1gFnSc4	-tic
<g/>
.	.	kIx.	.
</s>
<s>
Nechť	nechť	k9	nechť
dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
}	}	kIx)	}
prvek	prvek	k1gInSc1	prvek
tělesa	těleso	k1gNnSc2	těleso
<g/>
,	,	kIx,	,
násobek	násobek	k1gInSc1	násobek
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alph	k1gMnSc4	alph
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
pak	pak	k6eAd1	pak
definujeme	definovat	k5eAaBmIp1nP	definovat
jako	jako	k9	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
(	(	kIx(	(
α	α	k?	α
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
α	α	k?	α
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
...	...	k?	...
,	,	kIx,	,
α	α	k?	α
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
x_	x_	k?	x_
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
opět	opět	k6eAd1	opět
uspořádaná	uspořádaný	k2eAgFnSc1d1	uspořádaná
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
n	n	k0	n
<g/>
}	}	kIx)	}
-tice	ice	k1gFnSc2	-tice
<g/>
.	.	kIx.	.
</s>
<s>
Množina	množina	k1gFnSc1	množina
všech	všecek	k3xTgFnPc2	všecek
uspořádaných	uspořádaný	k2eAgFnPc2d1	uspořádaná
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
n	n	k0	n
<g/>
}	}	kIx)	}
-tic	ic	k6eAd1	-tic
s	s	k7c7	s
tělesem	těleso	k1gNnSc7	těleso
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
}	}	kIx)	}
a	a	k8xC	a
s	s	k7c7	s
výše	vysoce	k6eAd2	vysoce
definovanými	definovaný	k2eAgFnPc7d1	definovaná
operacemi	operace	k1gFnPc7	operace
sčítání	sčítání	k1gNnSc2	sčítání
a	a	k8xC	a
násobení	násobení	k1gNnSc2	násobení
je	být	k5eAaImIp3nS	být
vektorový	vektorový	k2eAgInSc1d1	vektorový
prostor	prostor	k1gInSc1	prostor
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
lze	lze	k6eAd1	lze
snadno	snadno	k6eAd1	snadno
dokázat	dokázat	k5eAaPmF	dokázat
z	z	k7c2	z
definice	definice	k1gFnSc2	definice
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nazýváme	nazývat	k5eAaImIp1nP	nazývat
ho	on	k3xPp3gInSc4	on
aritmetickým	aritmetický	k2eAgInSc7d1	aritmetický
vektorovým	vektorový	k2eAgInSc7d1	vektorový
prostorem	prostor	k1gInSc7	prostor
dimenze	dimenze	k1gFnSc2	dimenze
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
n	n	k0	n
<g/>
}	}	kIx)	}
nad	nad	k7c7	nad
tělesem	těleso	k1gNnSc7	těleso
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
n	n	k0	n
<g/>
}	}	kIx)	}
-rozměrným	ozměrný	k2eAgInSc7d1	-rozměrný
aritmetickým	aritmetický	k2eAgInSc7d1	aritmetický
vektorovým	vektorový	k2eAgInSc7d1	vektorový
prostorem	prostor	k1gInSc7	prostor
nad	nad	k7c7	nad
tělesem	těleso	k1gNnSc7	těleso
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc4	jeho
prvky	prvek	k1gInPc4	prvek
pak	pak	k6eAd1	pak
nazýváme	nazývat	k5eAaImIp1nP	nazývat
aritmetické	aritmetický	k2eAgInPc4d1	aritmetický
vektory	vektor	k1gInPc4	vektor
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
vektorů	vektor	k1gInPc2	vektor
používán	používat	k5eAaImNgInS	používat
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
přívlastek	přívlastek	k1gInSc4	přívlastek
aritmetický	aritmetický	k2eAgInSc4d1	aritmetický
vynechává	vynechávat	k5eAaImIp3nS	vynechávat
a	a	k8xC	a
hovoří	hovořit	k5eAaImIp3nS	hovořit
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
o	o	k7c6	o
vektorech	vektor	k1gInPc6	vektor
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
s	s	k7c7	s
frází	fráze	k1gFnSc7	fráze
<g/>
:	:	kIx,	:
vektorový	vektorový	k2eAgInSc1d1	vektorový
prostor	prostor	k1gInSc1	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
s	s	k7c7	s
přirozeně	přirozeně	k6eAd1	přirozeně
definovanými	definovaný	k2eAgFnPc7d1	definovaná
aritmetickými	aritmetický	k2eAgFnPc7d1	aritmetická
operacemi	operace	k1gFnPc7	operace
či	či	k8xC	či
vektorový	vektorový	k2eAgInSc1d1	vektorový
prostor	prostor	k1gInSc1	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
s	s	k7c7	s
přirozeně	přirozeně	k6eAd1	přirozeně
definovanými	definovaný	k2eAgFnPc7d1	definovaná
operacemi	operace	k1gFnPc7	operace
sčítání	sčítání	k1gNnSc2	sčítání
a	a	k8xC	a
násobení	násobení	k1gNnSc2	násobení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
myslí	myslet	k5eAaImIp3nS	myslet
právě	právě	k9	právě
výše	vysoce	k6eAd2	vysoce
zavedený	zavedený	k2eAgInSc4d1	zavedený
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dva	dva	k4xCgInPc1	dva
vektory	vektor	k1gInPc1	vektor
sčítají	sčítat	k5eAaImIp3nP	sčítat
a	a	k8xC	a
násobí	násobit	k5eAaImIp3nS	násobit
číslem	číslo	k1gNnSc7	číslo
po	po	k7c6	po
složkách	složka	k1gFnPc6	složka
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
totiž	totiž	k9	totiž
definovat	definovat	k5eAaBmF	definovat
i	i	k9	i
jiné	jiný	k2eAgFnPc4d1	jiná
operace	operace	k1gFnPc4	operace
sčítání	sčítání	k1gNnSc2	sčítání
a	a	k8xC	a
násobení	násobení	k1gNnSc2	násobení
číslem	číslo	k1gNnSc7	číslo
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yQgInPc6	který
by	by	kYmCp3nS	by
prostor	prostora	k1gFnPc2	prostora
uspořádaných	uspořádaný	k2eAgFnPc2d1	uspořádaná
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
n	n	k0	n
<g/>
}	}	kIx)	}
-tic	ic	k1gInSc1	-tic
též	též	k9	též
tvořil	tvořit	k5eAaImAgInS	tvořit
vektorový	vektorový	k2eAgInSc1d1	vektorový
prostor	prostor	k1gInSc1	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
bychom	by	kYmCp1nP	by
mu	on	k3xPp3gNnSc3	on
už	už	k9	už
ale	ale	k9	ale
neříkali	říkat	k5eNaImAgMnP	říkat
aritmetický	aritmetický	k2eAgInSc4d1	aritmetický
vektorový	vektorový	k2eAgInSc4d1	vektorový
prostor	prostor	k1gInSc4	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
vždy	vždy	k6eAd1	vždy
se	se	k3xPyFc4	se
za	za	k7c4	za
těleso	těleso	k1gNnSc4	těleso
bere	brát	k5eAaImIp3nS	brát
množina	množina	k1gFnSc1	množina
reálných	reálný	k2eAgFnPc2d1	reálná
či	či	k8xC	či
komplexních	komplexní	k2eAgFnPc2d1	komplexní
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
dostáváme	dostávat	k5eAaImIp1nP	dostávat
tedy	tedy	k9	tedy
prostory	prostora	k1gFnPc1	prostora
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
či	či	k8xC	či
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
C	C	kA	C
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Prvky	prvek	k1gInPc1	prvek
těchto	tento	k3xDgInPc2	tento
prostorů	prostor	k1gInPc2	prostor
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
značí	značit	k5eAaImIp3nS	značit
jako	jako	k8xC	jako
sloupce	sloupec	k1gInPc4	sloupec
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⋮	⋮	k?	⋮
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
begin	begin	k1gInSc1	begin
<g/>
{	{	kIx(	{
<g/>
pmatrix	pmatrix	k1gInSc1	pmatrix
<g/>
}	}	kIx)	}
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}	}	kIx)	}
<g/>
\\	\\	k?	\\
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\\\	\\\	k?	\\\
<g/>
vdots	vdots	k1gInSc1	vdots
\\	\\	k?	\\
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
end	end	k?	end
<g/>
{	{	kIx(	{
<g/>
pmatrix	pmatrix	k1gInSc1	pmatrix
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
n	n	k0	n
<g/>
}	}	kIx)	}
udává	udávat	k5eAaImIp3nS	udávat
počet	počet	k1gInSc4	počet
složek	složka	k1gFnPc2	složka
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
zapisovaným	zapisovaný	k2eAgMnPc3d1	zapisovaný
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
n	n	k0	n
<g/>
}	}	kIx)	}
-ticím	icí	k1gMnPc3	-ticí
říkáme	říkat	k5eAaImIp1nP	říkat
sloupcové	sloupcový	k2eAgInPc1d1	sloupcový
vektory	vektor	k1gInPc1	vektor
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
se	se	k3xPyFc4	se
ale	ale	k9	ale
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
s	s	k7c7	s
vektory	vektor	k1gInPc7	vektor
psanými	psaný	k2eAgInPc7d1	psaný
do	do	k7c2	do
řádku	řádek	k1gInSc2	řádek
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
...	...	k?	...
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
které	který	k3yIgNnSc1	který
nazýváme	nazývat	k5eAaImIp1nP	nazývat
řádkové	řádkový	k2eAgInPc1d1	řádkový
vektory	vektor	k1gInPc1	vektor
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
se	s	k7c7	s
sloupcovými	sloupcový	k2eAgInPc7d1	sloupcový
vektory	vektor	k1gInPc7	vektor
viz	vidět	k5eAaImRp2nS	vidět
Příklad	příklad	k1gInSc1	příklad
1	[number]	k4	1
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Lineární	lineární	k2eAgFnSc1d1	lineární
kombinace	kombinace	k1gFnSc1	kombinace
<g/>
,	,	kIx,	,
Příklad	příklad	k1gInSc1	příklad
1	[number]	k4	1
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Lineární	lineární	k2eAgFnSc4d1	lineární
nezávislost	nezávislost	k1gFnSc4	nezávislost
či	či	k8xC	či
Příklad	příklad	k1gInSc1	příklad
1	[number]	k4	1
a	a	k8xC	a
Příklad	příklad	k1gInSc1	příklad
2	[number]	k4	2
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Lineární	lineární	k2eAgInSc4d1	lineární
obal	obal	k1gInSc4	obal
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
pracujeme	pracovat	k5eAaImIp1nP	pracovat
pouze	pouze	k6eAd1	pouze
se	s	k7c7	s
samotnými	samotný	k2eAgInPc7d1	samotný
aritmetickými	aritmetický	k2eAgInPc7d1	aritmetický
vektory	vektor	k1gInPc7	vektor
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
jedno	jeden	k4xCgNnSc1	jeden
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
používáme	používat	k5eAaImIp1nP	používat
řádkový	řádkový	k2eAgInSc1d1	řádkový
či	či	k8xC	či
sloupcový	sloupcový	k2eAgInSc1d1	sloupcový
zápis	zápis	k1gInSc1	zápis
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
ale	ale	k9	ale
začne	začít	k5eAaPmIp3nS	začít
být	být	k5eAaImF	být
patrný	patrný	k2eAgInSc1d1	patrný
<g/>
,	,	kIx,	,
budeme	být	k5eAaImBp1nP	být
<g/>
-li	i	k?	-li
chtít	chtít	k5eAaImF	chtít
těmito	tento	k3xDgFnPc7	tento
vektory	vektor	k1gInPc1	vektor
násobit	násobit	k5eAaImF	násobit
matici	matice	k1gFnSc4	matice
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
viz	vidět	k5eAaImRp2nS	vidět
články	článek	k1gInPc4	článek
Sloupcový	sloupcový	k2eAgInSc4d1	sloupcový
vektor	vektor	k1gInSc4	vektor
a	a	k8xC	a
Řádkový	řádkový	k2eAgInSc4d1	řádkový
vektor	vektor	k1gInSc4	vektor
<g/>
.	.	kIx.	.
</s>
<s>
Prostor	prostor	k1gInSc1	prostor
uspořádaných	uspořádaný	k2eAgFnPc2d1	uspořádaná
n-tic	nice	k1gFnPc2	n-tice
má	mít	k5eAaImIp3nS	mít
dimenzi	dimenze	k1gFnSc4	dimenze
rovnou	rovnou	k6eAd1	rovnou
n.	n.	k?	n.
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
složky	složka	k1gFnPc1	složka
obecného	obecný	k2eAgInSc2d1	obecný
n-složkového	nložkový	k2eAgInSc2d1	n-složkový
aritmetického	aritmetický	k2eAgInSc2d1	aritmetický
vektoru	vektor	k1gInSc2	vektor
jsou	být	k5eAaImIp3nP	být
totiž	totiž	k9	totiž
navzájem	navzájem	k6eAd1	navzájem
nezávislé	závislý	k2eNgNnSc1d1	nezávislé
a	a	k8xC	a
k	k	k7c3	k
určení	určení	k1gNnSc3	určení
každého	každý	k3xTgInSc2	každý
vektoru	vektor	k1gInSc2	vektor
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
vektorovém	vektorový	k2eAgInSc6d1	vektorový
prostoru	prostor	k1gInSc6	prostor
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
třeba	třeba	k6eAd1	třeba
právě	právě	k6eAd1	právě
n	n	k0	n
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
bylo	být	k5eAaImAgNnS	být
ukázáno	ukázat	k5eAaPmNgNnS	ukázat
v	v	k7c6	v
Motivaci	motivace	k1gFnSc6	motivace
<g/>
,	,	kIx,	,
prostor	prostor	k1gInSc4	prostor
šipek	šipka	k1gFnPc2	šipka
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
prostoru	prostora	k1gFnSc4	prostora
dvousložkových	dvousložkový	k2eAgInPc2d1	dvousložkový
aritmetických	aritmetický	k2eAgInPc2d1	aritmetický
vektorů	vektor	k1gInPc2	vektor
<g/>
.	.	kIx.	.
</s>
<s>
Přesněji	přesně	k6eAd2	přesně
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
námi	my	k3xPp1nPc7	my
rozebíraný	rozebíraný	k2eAgInSc1d1	rozebíraný
případ	případ	k1gInSc1	případ
je	být	k5eAaImIp3nS	být
případ	případ	k1gInSc4	případ
aritmetického	aritmetický	k2eAgInSc2d1	aritmetický
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
definovaného	definovaný	k2eAgNnSc2d1	definované
nad	nad	k7c7	nad
tělesem	těleso	k1gNnSc7	těleso
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
}	}	kIx)	}
a	a	k8xC	a
s	s	k7c7	s
přirozeně	přirozeně	k6eAd1	přirozeně
zavedenými	zavedený	k2eAgFnPc7d1	zavedená
aritmetickými	aritmetický	k2eAgFnPc7d1	aritmetická
operacemi	operace	k1gFnPc7	operace
<g/>
.	.	kIx.	.
</s>
<s>
Prostor	prostor	k1gInSc1	prostor
šipek	šipka	k1gFnPc2	šipka
ve	v	k7c6	v
trojrozměrném	trojrozměrný	k2eAgInSc6d1	trojrozměrný
prostoru	prostor	k1gInSc6	prostor
by	by	kYmCp3nS	by
odpovídal	odpovídat	k5eAaImAgInS	odpovídat
množině	množina	k1gFnSc3	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Analogicky	analogicky	k6eAd1	analogicky
pak	pak	k6eAd1	pak
prostor	prostor	k1gInSc1	prostor
šipek	šipka	k1gFnPc2	šipka
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
k	k	k7c3	k
<g/>
}	}	kIx)	}
-rozměrném	ozměrný	k2eAgInSc6d1	-rozměrný
prostoru	prostor	k1gInSc6	prostor
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
množině	množina	k1gFnSc3	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}}	}}	k?	}}
nad	nad	k7c7	nad
tělesem	těleso	k1gNnSc7	těleso
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
}	}	kIx)	}
s	s	k7c7	s
přirozeně	přirozeně	k6eAd1	přirozeně
definovanými	definovaný	k2eAgFnPc7d1	definovaná
operacemi	operace	k1gFnPc7	operace
sčítání	sčítání	k1gNnSc2	sčítání
a	a	k8xC	a
násobení	násobení	k1gNnSc2	násobení
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Matice	matice	k1gFnSc2	matice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
předchozím	předchozí	k2eAgInSc6d1	předchozí
oddílu	oddíl	k1gInSc6	oddíl
jsme	být	k5eAaImIp1nP	být
zkoumali	zkoumat	k5eAaImAgMnP	zkoumat
prostor	prostor	k1gInSc4	prostor
uspořádaných	uspořádaný	k2eAgFnPc2d1	uspořádaná
n-tic	nice	k1gFnPc2	n-tice
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
prvků	prvek	k1gInPc2	prvek
množiny	množina	k1gFnSc2	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Podívejme	podívat	k5eAaPmRp1nP	podívat
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
na	na	k7c4	na
lehce	lehko	k6eAd1	lehko
obecnější	obecní	k2eAgFnSc4d2	obecní
množinu	množina	k1gFnSc4	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
n	n	k0	n
,	,	kIx,	,
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
≡	≡	k?	≡
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
n	n	k0	n
×	×	k?	×
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
,	,	kIx,	,
<g/>
m	m	kA	m
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
equiv	equivit	k5eAaPmRp2nS	equivit
T	T	kA	T
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
times	times	k1gInSc4	times
m	m	kA	m
<g/>
}}	}}	k?	}}
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
jako	jako	k9	jako
kartézský	kartézský	k2eAgInSc1d1	kartézský
součin	součin	k1gInSc1	součin
n	n	k0	n
krát	krát	k6eAd1	krát
m	m	kA	m
množin	množina	k1gFnPc2	množina
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
každá	každý	k3xTgFnSc1	každý
je	být	k5eAaImIp3nS	být
rovna	roven	k2eAgFnSc1d1	rovna
tělesu	těleso	k1gNnSc3	těleso
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
prakticky	prakticky	k6eAd1	prakticky
o	o	k7c4	o
množinu	množina	k1gFnSc4	množina
uspořádaných	uspořádaný	k2eAgFnPc2d1	uspořádaná
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
k	k	k7c3	k
<g/>
}	}	kIx)	}
-tic	ic	k1gFnSc1	-tic
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
k	k	k7c3	k
=	=	kIx~	=
n	n	k0	n
⋅	⋅	k?	⋅
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
k	k	k7c3	k
<g/>
=	=	kIx~	=
<g/>
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k2eAgMnSc1d1	cdot
m	m	kA	m
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
aritmetickým	aritmetický	k2eAgInPc3d1	aritmetický
vektorům	vektor	k1gInPc3	vektor
ale	ale	k8xC	ale
uděláme	udělat	k5eAaPmIp1nP	udělat
jednu	jeden	k4xCgFnSc4	jeden
věc	věc	k1gFnSc4	věc
navíc	navíc	k6eAd1	navíc
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
složky	složka	k1gFnPc4	složka
prvku	prvek	k1gInSc2	prvek
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
n	n	k0	n
,	,	kIx,	,
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
,	,	kIx,	,
<g/>
m	m	kA	m
<g/>
}}	}}	k?	}}
totiž	totiž	k9	totiž
seřadíme	seřadit	k5eAaPmIp1nP	seřadit
do	do	k7c2	do
obdélníku	obdélník	k1gInSc2	obdélník
o	o	k7c6	o
rozměrech	rozměr	k1gInPc6	rozměr
n	n	k0	n
krát	krát	k6eAd1	krát
m	m	kA	m
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
níže	nízce	k6eAd2	nízce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
:	:	kIx,	:
∈	∈	k?	∈
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
n	n	k0	n
,	,	kIx,	,
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⇔	⇔	k?	⇔
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
11	[number]	k4	11
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
12	[number]	k4	12
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
...	...	k?	...
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
1	[number]	k4	1
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
21	[number]	k4	21
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
22	[number]	k4	22
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
...	...	k?	...
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
2	[number]	k4	2
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⋮	⋮	k?	⋮
:	:	kIx,	:
:	:	kIx,	:
⋮	⋮	k?	⋮
:	:	kIx,	:
:	:	kIx,	:
⋱	⋱	k?	⋱
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
⋮	⋮	k?	⋮
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
...	...	k?	...
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
A	A	kA	A
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
in	in	k?	in
T	T	kA	T
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
,	,	kIx,	,
<g/>
m	m	kA	m
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
quad	quad	k1gMnSc1	quad
\	\	kIx~	\
<g/>
Leftrightarrow	Leftrightarrow	k1gMnSc1	Leftrightarrow
\	\	kIx~	\
<g/>
quad	quad	k1gMnSc1	quad
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
A	A	kA	A
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
begin	begin	k2eAgInSc4d1	begin
<g/>
{	{	kIx(	{
<g/>
pmatrix	pmatrix	k1gInSc4	pmatrix
<g/>
}	}	kIx)	}
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
11	[number]	k4	11
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
12	[number]	k4	12
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
&	&	k?	&
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
m	m	kA	m
<g/>
}	}	kIx)	}
<g/>
\\	\\	k?	\\
<g/>
a_	a_	k?	a_
<g/>
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
21	[number]	k4	21
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
22	[number]	k4	22
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
&	&	k?	&
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
m	m	kA	m
<g/>
}	}	kIx)	}
<g/>
\\\	\\\	k?	\\\
<g/>
vdots	vdotsit	k5eAaPmRp2nS	vdotsit
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
vdots	vdots	k1gInSc1	vdots
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
ddots	ddots	k1gInSc1	ddots
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s>
vdots	vdots	k1gInSc1	vdots
\\	\\	k?	\\
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
&	&	k?	&
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
nm	nm	k?	nm
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
end	end	k?	end
<g/>
{	{	kIx(	{
<g/>
pmatrix	pmatrix	k1gInSc1	pmatrix
<g/>
}}	}}	k?	}}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Takovýmto	takovýto	k3xDgInPc3	takovýto
objektům	objekt	k1gInPc3	objekt
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
matice	matice	k1gFnSc1	matice
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
označovat	označovat	k5eAaImF	označovat
matice	matice	k1gFnPc4	matice
velkými	velký	k2eAgInPc7d1	velký
tučnými	tučný	k2eAgNnPc7d1	tučné
či	či	k8xC	či
konturovými	konturový	k2eAgNnPc7d1	konturové
písmeny	písmeno	k1gNnPc7	písmeno
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
vyznačeno	vyznačit	k5eAaPmNgNnS	vyznačit
výše	vysoce	k6eAd2	vysoce
<g/>
.	.	kIx.	.
</s>
<s>
Mohli	moct	k5eAaImAgMnP	moct
jsme	být	k5eAaImIp1nP	být
ale	ale	k8xC	ale
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
místo	místo	k1gNnSc4	místo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
A	A	kA	A
<g/>
}	}	kIx)	}
}	}	kIx)	}
psát	psát	k5eAaImF	psát
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
značení	značení	k1gNnSc4	značení
<g/>
.	.	kIx.	.
</s>
<s>
Máme	mít	k5eAaImIp1nP	mít
tedy	tedy	k9	tedy
množinu	množina	k1gFnSc4	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
n	n	k0	n
,	,	kIx,	,
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
,	,	kIx,	,
<g/>
m	m	kA	m
<g/>
}}	}}	k?	}}
,	,	kIx,	,
těleso	těleso	k1gNnSc4	těleso
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Zbývá	zbývat	k5eAaImIp3nS	zbývat
nám	my	k3xPp1nPc3	my
tedy	tedy	k9	tedy
definovat	definovat	k5eAaBmF	definovat
operaci	operace	k1gFnSc4	operace
sčítání	sčítání	k1gNnSc2	sčítání
a	a	k8xC	a
operaci	operace	k1gFnSc4	operace
násobení	násobení	k1gNnSc2	násobení
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc4	dva
definujeme	definovat	k5eAaBmIp1nP	definovat
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
aritmetických	aritmetický	k2eAgInPc2d1	aritmetický
vektorů	vektor	k1gInPc2	vektor
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
11	[number]	k4	11
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
12	[number]	k4	12
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
...	...	k?	...
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s hack="1">
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
21	[number]	k4	21
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
22	[number]	k4	22
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
...	...	k?	...
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
2	[number]	k4	2
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⋮	⋮	k?	⋮
:	:	kIx,	:
:	:	kIx,	:
⋮	⋮	k?	⋮
:	:	kIx,	:
:	:	kIx,	:
⋱	⋱	k?	⋱
:	:	kIx,	:
:	:	kIx,	:
⋮	⋮	k?	⋮
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
...	...	k?	...
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
11	[number]	k4	11
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
12	[number]	k4	12
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
...	...	k?	...
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
1	[number]	k4	1
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
21	[number]	k4	21
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
22	[number]	k4	22
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
...	...	k?	...
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
2	[number]	k4	2
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
⋮	⋮	k?	⋮
:	:	kIx,	:
:	:	kIx,	:
⋮	⋮	k?	⋮
:	:	kIx,	:
:	:	kIx,	:
⋱	⋱	k?	⋱
:	:	kIx,	:
:	:	kIx,	:
⋮	⋮	k?	⋮
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
n	n	k0	n
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
n	n	k0	n
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
...	...	k?	...
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
n	n	k0	n
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
11	[number]	k4	11
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
11	[number]	k4	11
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
12	[number]	k4	12
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
12	[number]	k4	12
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
...	...	k?	...
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
1	[number]	k4	1
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
b	b	k?	b
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
1	[number]	k4	1
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
21	[number]	k4	21
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
21	[number]	k4	21
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
22	[number]	k4	22
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
22	[number]	k4	22
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
...	...	k?	...
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
2	[number]	k4	2
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
2	[number]	k4	2
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⋮	⋮	k?	⋮
:	:	kIx,	:
:	:	kIx,	:
⋮	⋮	k?	⋮
:	:	kIx,	:
:	:	kIx,	:
⋱	⋱	k?	⋱
:	:	kIx,	:
:	:	kIx,	:
⋮	⋮	k?	⋮
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
n	n	k0	n
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
n	n	k0	n
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
...	...	k?	...
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
n	n	k0	n
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
A	A	kA	A
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
11	[number]	k4	11
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
12	[number]	k4	12
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
...	...	k?	...
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
1	[number]	k4	1
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
21	[number]	k4	21
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
22	[number]	k4	22
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
...	...	k?	...
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
α	α	k?	α
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
2	[number]	k4	2
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⋮	⋮	k?	⋮
:	:	kIx,	:
:	:	kIx,	:
⋮	⋮	k?	⋮
:	:	kIx,	:
:	:	kIx,	:
⋱	⋱	k?	⋱
:	:	kIx,	:
:	:	kIx,	:
⋮	⋮	k?	⋮
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
...	...	k?	...
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
A	A	kA	A
<g/>
}	}	kIx)	}
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
B	B	kA	B
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
begin	begin	k1gInSc1	begin
<g/>
{	{	kIx(	{
<g/>
pmatrix	pmatrix	k1gInSc1	pmatrix
<g/>
}	}	kIx)	}
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
11	[number]	k4	11
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
12	[number]	k4	12
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
&	&	k?	&
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
m	m	kA	m
<g/>
}	}	kIx)	}
<g/>
\\	\\	k?	\\
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
21	[number]	k4	21
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
22	[number]	k4	22
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
&	&	k?	&
<g/>
a_	a_	k?	a_
<g />
.	.	kIx.	.
</s>
<s>
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
m	m	kA	m
<g/>
}	}	kIx)	}
<g/>
\\\	\\\	k?	\\\
<g/>
vdots	vdotsit	k5eAaPmRp2nS	vdotsit
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
vdots	vdots	k1gInSc1	vdots
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
ddots	ddots	k1gInSc1	ddots
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
vdots	vdots	k1gInSc1	vdots
\\	\\	k?	\\
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
&	&	k?	&
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
nm	nm	k?	nm
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
end	end	k?	end
<g/>
{	{	kIx(	{
<g/>
pmatrix	pmatrix	k1gInSc1	pmatrix
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
begin	begin	k1gInSc1	begin
<g/>
{	{	kIx(	{
<g/>
pmatrix	pmatrix	k1gInSc1	pmatrix
<g/>
}	}	kIx)	}
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
11	[number]	k4	11
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
<g />
.	.	kIx.	.
</s>
<s>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
12	[number]	k4	12
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
&	&	k?	&
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
m	m	kA	m
<g/>
}	}	kIx)	}
<g/>
\\	\\	k?	\\
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
21	[number]	k4	21
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
22	[number]	k4	22
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
<g />
.	.	kIx.	.
</s>
<s>
&	&	k?	&
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
m	m	kA	m
<g/>
}	}	kIx)	}
<g/>
\\\	\\\	k?	\\\
<g/>
vdots	vdotsit	k5eAaPmRp2nS	vdotsit
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
vdots	vdots	k1gInSc1	vdots
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
ddots	ddots	k1gInSc1	ddots
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
vdots	vdots	k1gInSc1	vdots
\\	\\	k?	\\
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
&	&	k?	&
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
nm	nm	k?	nm
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
end	end	k?	end
<g/>
{	{	kIx(	{
<g/>
pmatrix	pmatrix	k1gInSc1	pmatrix
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
begin	begin	k1gInSc1	begin
<g/>
{	{	kIx(	{
<g/>
pmatrix	pmatrix	k1gInSc1	pmatrix
<g/>
}	}	kIx)	}
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
11	[number]	k4	11
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
+	+	kIx~	+
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
11	[number]	k4	11
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
12	[number]	k4	12
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
12	[number]	k4	12
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
&	&	k?	&
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
m	m	kA	m
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
<g />
.	.	kIx.	.
</s>
<s>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
m	m	kA	m
<g/>
}	}	kIx)	}
<g/>
\\	\\	k?	\\
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
21	[number]	k4	21
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
21	[number]	k4	21
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
22	[number]	k4	22
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
22	[number]	k4	22
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s>
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
&	&	k?	&
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
m	m	kA	m
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
m	m	kA	m
<g/>
}	}	kIx)	}
<g/>
\\\	\\\	k?	\\\
<g/>
vdots	vdotsit	k5eAaPmRp2nS	vdotsit
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
vdots	vdots	k1gInSc1	vdots
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
ddots	ddots	k1gInSc1	ddots
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
vdots	vdots	k1gInSc1	vdots
\\	\\	k?	\\
<g/>
a_	a_	k?	a_
<g />
.	.	kIx.	.
</s>
<s>
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
<g />
.	.	kIx.	.
</s>
<s>
&	&	k?	&
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
nm	nm	k?	nm
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
nm	nm	k?	nm
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
end	end	k?	end
<g/>
{	{	kIx(	{
<g/>
pmatrix	pmatrix	k1gInSc1	pmatrix
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
qquad	qquad	k1gInSc1	qquad
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
A	A	kA	A
<g/>
}	}	kIx)	}
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s>
\	\	kIx~	\
<g/>
begin	begin	k2eAgInSc4d1	begin
<g/>
{	{	kIx(	{
<g/>
pmatrix	pmatrix	k1gInSc4	pmatrix
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
11	[number]	k4	11
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
12	[number]	k4	12
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
<g />
.	.	kIx.	.
</s>
<s>
m	m	kA	m
<g/>
}	}	kIx)	}
<g/>
\\\	\\\	k?	\\\
<g/>
alpha	alpha	k1gFnSc1	alpha
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
21	[number]	k4	21
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
22	[number]	k4	22
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
m	m	kA	m
<g/>
}	}	kIx)	}
<g/>
\\\	\\\	k?	\\\
<g/>
<g />
.	.	kIx.	.
</s>
<s>
vdots	vdots	k1gInSc1	vdots
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
vdots	vdots	k1gInSc1	vdots
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
ddots	ddots	k1gInSc1	ddots
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
vdots	vdots	k1gInSc1	vdots
\\\	\\\	k?	\\\
<g/>
alpha	alpha	k1gFnSc1	alpha
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
nm	nm	k?	nm
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
end	end	k?	end
<g/>
{	{	kIx(	{
<g/>
pmatrix	pmatrix	k1gInSc1	pmatrix
<g/>
}}	}}	k?	}}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
sčítání	sčítání	k1gNnSc2	sčítání
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
násobení	násobení	k1gNnSc1	násobení
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
sčítání	sčítání	k1gNnSc1	sčítání
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
násobení	násobení	k1gNnSc4	násobení
po	po	k7c6	po
složkách	složka	k1gFnPc6	složka
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
klasický	klasický	k2eAgInSc4d1	klasický
způsob	způsob	k1gInSc4	způsob
zavedení	zavedení	k1gNnSc4	zavedení
těchto	tento	k3xDgFnPc2	tento
operací	operace	k1gFnPc2	operace
<g/>
,	,	kIx,	,
kterým	který	k3yQgFnPc3	který
tak	tak	k9	tak
občas	občas	k6eAd1	občas
říkáme	říkat	k5eAaImIp1nP	říkat
"	"	kIx"	"
<g/>
přirozeně	přirozeně	k6eAd1	přirozeně
definované	definovaný	k2eAgFnPc1d1	definovaná
aritmetické	aritmetický	k2eAgFnPc1d1	aritmetická
operace	operace	k1gFnPc1	operace
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
takto	takto	k6eAd1	takto
definovanými	definovaný	k2eAgFnPc7d1	definovaná
operacemi	operace	k1gFnPc7	operace
lze	lze	k6eAd1	lze
snadno	snadno	k6eAd1	snadno
ukázat	ukázat	k5eAaPmF	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
množina	množina	k1gFnSc1	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
n	n	k0	n
,	,	kIx,	,
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
,	,	kIx,	,
<g/>
m	m	kA	m
<g/>
}}	}}	k?	}}
s	s	k7c7	s
tělesem	těleso	k1gNnSc7	těleso
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
}	}	kIx)	}
tvoří	tvořit	k5eAaImIp3nS	tvořit
vektorový	vektorový	k2eAgInSc1d1	vektorový
prostor	prostor	k1gInSc1	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
za	za	k7c4	za
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
}	}	kIx)	}
bere	brát	k5eAaImIp3nS	brát
množina	množina	k1gFnSc1	množina
reálných	reálný	k2eAgFnPc2d1	reálná
či	či	k8xC	či
komplexních	komplexní	k2eAgFnPc2d1	komplexní
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsme	být	k5eAaImIp1nP	být
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
složky	složka	k1gFnPc4	složka
vektoru	vektor	k1gInSc2	vektor
alias	alias	k9	alias
matice	matice	k1gFnSc1	matice
uspořádali	uspořádat	k5eAaPmAgMnP	uspořádat
do	do	k7c2	do
obdélníku	obdélník	k1gInSc2	obdélník
ještě	ještě	k6eAd1	ještě
nic	nic	k3yNnSc1	nic
neznamená	znamenat	k5eNaImIp3nS	znamenat
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
totiž	totiž	k9	totiž
pracujeme	pracovat	k5eAaImIp1nP	pracovat
jako	jako	k9	jako
s	s	k7c7	s
aritmetickými	aritmetický	k2eAgInPc7d1	aritmetický
vektory	vektor	k1gInPc7	vektor
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíl	rozdíl	k1gInSc1	rozdíl
nastává	nastávat	k5eAaImIp3nS	nastávat
teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
matice	matice	k1gFnPc4	matice
můžeme	moct	k5eAaImIp1nP	moct
definovat	definovat	k5eAaBmF	definovat
i	i	k9	i
jejich	jejich	k3xOp3gNnSc4	jejich
vzájemné	vzájemný	k2eAgNnSc4d1	vzájemné
násobení	násobení	k1gNnSc4	násobení
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
tedy	tedy	k9	tedy
násobit	násobit	k5eAaImF	násobit
spolu	spolu	k6eAd1	spolu
dva	dva	k4xCgInPc4	dva
vektory	vektor	k1gInPc4	vektor
(	(	kIx(	(
<g/>
matice	matice	k1gFnSc1	matice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
obecném	obecný	k2eAgInSc6d1	obecný
případě	případ	k1gInSc6	případ
můžeme	moct	k5eAaImIp1nP	moct
pouze	pouze	k6eAd1	pouze
vektor	vektor	k1gInSc4	vektor
násobit	násobit	k5eAaImF	násobit
prvkem	prvek	k1gInSc7	prvek
z	z	k7c2	z
tělesa	těleso	k1gNnSc2	těleso
(	(	kIx(	(
<g/>
číslem	číslo	k1gNnSc7	číslo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Možnost	možnost	k1gFnSc1	možnost
násobit	násobit	k5eAaImF	násobit
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
matice	matice	k1gFnSc1	matice
je	on	k3xPp3gFnPc4	on
ale	ale	k9	ale
mimo	mimo	k7c4	mimo
rámec	rámec	k1gInSc4	rámec
definice	definice	k1gFnSc2	definice
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnPc4	jeho
zavedení	zavedení	k1gNnPc4	zavedení
nutná	nutný	k2eAgNnPc4d1	nutné
<g/>
.	.	kIx.	.
</s>
<s>
Čtenáře	čtenář	k1gMnPc4	čtenář
proto	proto	k8xC	proto
pro	pro	k7c4	pro
podrobnosti	podrobnost	k1gFnPc4	podrobnost
odkážeme	odkázat	k5eAaPmIp1nP	odkázat
na	na	k7c4	na
článek	článek	k1gInSc4	článek
Matice	matice	k1gFnSc2	matice
<g/>
.	.	kIx.	.
</s>
<s>
Analogicky	analogicky	k6eAd1	analogicky
jako	jako	k9	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
aritmetických	aritmetický	k2eAgInPc2d1	aritmetický
vektorů	vektor	k1gInPc2	vektor
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
dimenze	dimenze	k1gFnSc1	dimenze
prostoru	prostor	k1gInSc2	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
n	n	k0	n
,	,	kIx,	,
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
,	,	kIx,	,
<g/>
m	m	kA	m
<g/>
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
⋅	⋅	k?	⋅
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k2eAgMnSc1d1	cdot
m	m	kA	m
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Lineární	lineární	k2eAgInSc4d1	lineární
operátor	operátor	k1gInSc4	operátor
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
zabývali	zabývat	k5eAaImAgMnP	zabývat
vektorovými	vektorový	k2eAgInPc7d1	vektorový
prostory	prostor	k1gInPc7	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
V	V	kA	V
,	,	kIx,	,
T	T	kA	T
,	,	kIx,	,
⊕	⊕	k?	⊕
,	,	kIx,	,
⊙	⊙	k?	⊙
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
(	(	kIx(	(
<g/>
V	V	kA	V
<g/>
,	,	kIx,	,
<g/>
T	T	kA	T
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
oplus	oplus	k1gInSc1	oplus
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
odot	odot	k1gMnSc1	odot
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Uvažujme	uvažovat	k5eAaImRp1nP	uvažovat
nyní	nyní	k6eAd1	nyní
zobrazení	zobrazení	k1gNnSc4	zobrazení
množiny	množina	k1gFnSc2	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	V	kA	V
<g/>
}	}	kIx)	}
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Jinými	jiný	k2eAgNnPc7d1	jiné
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
uvažujme	uvažovat	k5eAaImRp1nP	uvažovat
zobrazení	zobrazení	k1gNnSc4	zobrazení
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
V	v	k7c6	v
→	→	k?	→
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
L	L	kA	L
<g/>
:	:	kIx,	:
<g/>
V	V	kA	V
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc4	ten
V	V	kA	V
<g/>
}	}	kIx)	}
,	,	kIx,	,
které	který	k3yRgNnSc1	který
vezme	vzít	k5eAaPmIp3nS	vzít
vektor	vektor	k1gInSc1	vektor
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	V	kA	V
<g/>
}	}	kIx)	}
a	a	k8xC	a
vrátí	vrátit	k5eAaPmIp3nS	vrátit
obecně	obecně	k6eAd1	obecně
nějaký	nějaký	k3yIgInSc1	nějaký
jiný	jiný	k2eAgInSc1d1	jiný
vektor	vektor	k1gInSc1	vektor
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	V	kA	V
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vlastnosti	vlastnost	k1gFnSc6	vlastnost
tohoto	tento	k3xDgNnSc2	tento
zobrazení	zobrazení	k1gNnSc2	zobrazení
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
L	L	kA	L
<g/>
}	}	kIx)	}
naklademe	naklást	k5eAaPmIp1nP	naklást
dvě	dva	k4xCgFnPc4	dva
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
∀	∀	k?	∀
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
V	V	kA	V
)	)	kIx)	)
(	(	kIx(	(
∀	∀	k?	∀
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
V	V	kA	V
)	)	kIx)	)
(	(	kIx(	(
L	L	kA	L
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
L	L	kA	L
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
+	+	kIx~	+
L	L	kA	L
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
)	)	kIx)	)
)	)	kIx)	)
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
forall	forall	k1gInSc1	forall
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
V	V	kA	V
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
forall	forall	k1gInSc1	forall
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
V	V	kA	V
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
L	L	kA	L
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
+	+	kIx~	+
<g/>
L	L	kA	L
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
))	))	k?	))
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
∀	∀	k?	∀
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
V	V	kA	V
)	)	kIx)	)
(	(	kIx(	(
∀	∀	k?	∀
α	α	k?	α
∈	∈	k?	∈
T	T	kA	T
)	)	kIx)	)
(	(	kIx(	(
L	L	kA	L
(	(	kIx(	(
α	α	k?	α
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
α	α	k?	α
L	L	kA	L
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
)	)	kIx)	)
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
forall	forall	k1gInSc1	forall
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
<g />
.	.	kIx.	.
</s>
<s hack="1">
V	v	k7c4	v
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
forall	forall	k1gMnSc1	forall
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
\	\	kIx~	\
<g/>
in	in	k?	in
T	T	kA	T
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
L	L	kA	L
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
L	L	kA	L
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
))	))	k?	))
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
První	první	k4xOgFnSc4	první
podmínku	podmínka	k1gFnSc4	podmínka
lze	lze	k6eAd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
slovy	slovo	k1gNnPc7	slovo
"	"	kIx"	"
<g/>
obraz	obraz	k1gInSc1	obraz
součtu	součet	k1gInSc2	součet
je	být	k5eAaImIp3nS	být
součet	součet	k1gInSc1	součet
obrazů	obraz	k1gInPc2	obraz
<g/>
"	"	kIx"	"
a	a	k8xC	a
matematicky	matematicky	k6eAd1	matematicky
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
aditivita	aditivita	k1gFnSc1	aditivita
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc4	druhý
podmínku	podmínka	k1gFnSc4	podmínka
pak	pak	k6eAd1	pak
můžeme	moct	k5eAaImIp1nP	moct
popsat	popsat	k5eAaPmF	popsat
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
obraz	obraz	k1gInSc1	obraz
násobku	násobek	k1gInSc2	násobek
je	být	k5eAaImIp3nS	být
násobek	násobek	k1gInSc1	násobek
obrazu	obraz	k1gInSc2	obraz
<g/>
"	"	kIx"	"
a	a	k8xC	a
matematicky	matematicky	k6eAd1	matematicky
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
říká	říkat	k5eAaImIp3nS	říkat
homogenita	homogenita	k1gFnSc1	homogenita
<g/>
.	.	kIx.	.
</s>
<s>
Zobrazení	zobrazení	k1gNnSc1	zobrazení
splňujícímu	splňující	k2eAgInSc3d1	splňující
výše	vysoce	k6eAd2	vysoce
uvedené	uvedený	k2eAgFnPc4d1	uvedená
podmínky	podmínka	k1gFnPc4	podmínka
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
lineární	lineární	k2eAgInSc1d1	lineární
operátor	operátor	k1gInSc1	operátor
<g/>
.	.	kIx.	.
</s>
<s>
Nechť	nechť	k9	nechť
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
vektorový	vektorový	k2eAgInSc1d1	vektorový
prostor	prostor	k1gInSc1	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
</s>
<s>
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	v	k7c6	v
<g/>
}	}	kIx)	}
konečné	konečný	k2eAgFnSc2d1	konečná
dimenze	dimenze	k1gFnSc2	dimenze
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
pro	pro	k7c4	pro
konkrétnost	konkrétnost	k1gFnSc4	konkrétnost
brát	brát	k5eAaImF	brát
aritmetický	aritmetický	k2eAgInSc4d1	aritmetický
vektorový	vektorový	k2eAgInSc4d1	vektorový
prostor	prostor	k1gInSc4	prostor
dimenze	dimenze	k1gFnSc2	dimenze
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
n	n	k0	n
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
lineární	lineární	k2eAgInSc1d1	lineární
operátor	operátor	k1gInSc1	operátor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
L	L	kA	L
<g/>
}	}	kIx)	}
působící	působící	k2eAgInSc1d1	působící
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
prostoru	prostor	k1gInSc6	prostor
bere	brát	k5eAaImIp3nS	brát
aritmetické	aritmetický	k2eAgInPc4d1	aritmetický
vektory	vektor	k1gInPc4	vektor
a	a	k8xC	a
vrací	vracet	k5eAaImIp3nS	vracet
jiné	jiný	k2eAgInPc4d1	jiný
aritmetické	aritmetický	k2eAgInPc4d1	aritmetický
vektory	vektor	k1gInPc4	vektor
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
splňuje	splňovat	k5eAaImIp3nS	splňovat
vlastnost	vlastnost	k1gFnSc1	vlastnost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
(	(	kIx(	(
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
L	L	kA	L
(	(	kIx(	(
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
+	+	kIx~	+
L	L	kA	L
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
α	α	k?	α
L	L	kA	L
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s hack="1">
+	+	kIx~	+
L	L	kA	L
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L	L	kA	L
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
L	L	kA	L
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s>
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
L	L	kA	L
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
L	L	kA	L
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
+	+	kIx~	+
<g/>
L	L	kA	L
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Uvažujme	uvažovat	k5eAaImRp1nP	uvažovat
nějaké	nějaký	k3yIgNnSc4	nějaký
dva	dva	k4xCgInPc4	dva
lineární	lineární	k2eAgInPc4d1	lineární
operátory	operátor	k1gInPc4	operátor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	A	kA	A
,	,	kIx,	,
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
A	A	kA	A
<g/>
,	,	kIx,	,
<g/>
B	B	kA	B
<g/>
}	}	kIx)	}
působící	působící	k2eAgFnSc2d1	působící
na	na	k7c6	na
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	V	kA	V
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
běžných	běžný	k2eAgFnPc2d1	běžná
funkcí	funkce	k1gFnPc2	funkce
bychom	by	kYmCp1nP	by
i	i	k9	i
nyní	nyní	k6eAd1	nyní
chtěli	chtít	k5eAaImAgMnP	chtít
tyto	tento	k3xDgInPc1	tento
dva	dva	k4xCgInPc1	dva
lineární	lineární	k2eAgInPc1d1	lineární
operátory	operátor	k1gInPc1	operátor
umět	umět	k5eAaImF	umět
sečíst	sečíst	k5eAaPmF	sečíst
<g/>
.	.	kIx.	.
</s>
<s>
Chtěli	chtít	k5eAaImAgMnP	chtít
bychom	by	kYmCp1nP	by
si	se	k3xPyFc3	se
tedy	tedy	k9	tedy
zavést	zavést	k5eAaPmF	zavést
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
to	ten	k3xDgNnSc1	ten
přesně	přesně	k6eAd1	přesně
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
když	když	k8xS	když
operátor	operátor	k1gInSc4	operátor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
A	a	k9	a
<g/>
}	}	kIx)	}
sečteme	sečíst	k5eAaPmIp1nP	sečíst
s	s	k7c7	s
operátorem	operátor	k1gInSc7	operátor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
B	B	kA	B
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
by	by	kYmCp3nS	by
mnohého	mnohé	k1gNnSc2	mnohé
napadlo	napadnout	k5eAaPmAgNnS	napadnout
<g/>
,	,	kIx,	,
definujeme	definovat	k5eAaBmIp1nP	definovat
si	se	k3xPyFc3	se
součet	součet	k1gInSc4	součet
dvou	dva	k4xCgMnPc2	dva
lineárních	lineární	k2eAgMnPc2d1	lineární
operátorů	operátor	k1gMnPc2	operátor
následovně	následovně	k6eAd1	následovně
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
A	A	kA	A
+	+	kIx~	+
B	B	kA	B
)	)	kIx)	)
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
A	a	k9	a
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
+	+	kIx~	+
B	B	kA	B
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
+	+	kIx~	+
<g/>
B	B	kA	B
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
A	a	k9	a
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
B	B	kA	B
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
V	V	kA	V
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
libovolný	libovolný	k2eAgInSc4d1	libovolný
vektor	vektor	k1gInSc4	vektor
z	z	k7c2	z
prostoru	prostor	k1gInSc2	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	V	kA	V
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
levé	levý	k2eAgFnSc6d1	levá
straně	strana	k1gFnSc6	strana
poslední	poslední	k2eAgFnSc2d1	poslední
rovnosti	rovnost	k1gFnSc2	rovnost
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
jediný	jediný	k2eAgInSc1d1	jediný
lineární	lineární	k2eAgInSc1d1	lineární
operátor	operátor	k1gInSc1	operátor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
jsme	být	k5eAaImIp1nP	být
si	se	k3xPyFc3	se
označili	označit	k5eAaPmAgMnP	označit
jako	jako	k9	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	A	kA	A
+	+	kIx~	+
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
A	a	k9	a
<g/>
+	+	kIx~	+
<g/>
B	B	kA	B
<g/>
}	}	kIx)	}
a	a	k8xC	a
nazýváme	nazývat	k5eAaImIp1nP	nazývat
ho	on	k3xPp3gInSc4	on
součet	součet	k1gInSc4	součet
lineárních	lineární	k2eAgMnPc2d1	lineární
operátorů	operátor	k1gMnPc2	operátor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
A	a	k9	a
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
B	B	kA	B
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Výrazem	výraz	k1gInSc7	výraz
výše	výše	k1gFnSc2	výše
jsme	být	k5eAaImIp1nP	být
tak	tak	k6eAd1	tak
definovali	definovat	k5eAaBmAgMnP	definovat
hodnotu	hodnota	k1gFnSc4	hodnota
tohoto	tento	k3xDgInSc2	tento
lineárního	lineární	k2eAgInSc2d1	lineární
operátoru	operátor	k1gInSc2	operátor
pomocí	pomocí	k7c2	pomocí
hodnot	hodnota	k1gFnPc2	hodnota
operátorů	operátor	k1gMnPc2	operátor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
A	a	k9	a
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
B	B	kA	B
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
si	se	k3xPyFc3	se
definujme	definovat	k5eAaBmRp1nP	definovat
i	i	k9	i
násobek	násobek	k1gInSc4	násobek
lineárního	lineární	k2eAgInSc2d1	lineární
operátoru	operátor	k1gInSc2	operátor
prvkem	prvek	k1gInSc7	prvek
z	z	k7c2	z
tělesa	těleso	k1gNnSc2	těleso
jako	jako	k8xC	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
α	α	k?	α
A	A	kA	A
)	)	kIx)	)
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
α	α	k?	α
A	A	kA	A
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
A	a	k9	a
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
A	a	k9	a
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
kde	kde	k6eAd1	kde
opět	opět	k6eAd1	opět
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
V	V	kA	V
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
libovolný	libovolný	k2eAgInSc4d1	libovolný
vektor	vektor	k1gInSc4	vektor
z	z	k7c2	z
prostoru	prostor	k1gInSc2	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	V	kA	V
<g/>
}	}	kIx)	}
a	a	k8xC	a
na	na	k7c6	na
levé	levý	k2eAgFnSc6d1	levá
straně	strana	k1gFnSc6	strana
rovnosti	rovnost	k1gFnSc2	rovnost
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
operátor	operátor	k1gInSc4	operátor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
jsme	být	k5eAaImIp1nP	být
si	se	k3xPyFc3	se
označili	označit	k5eAaPmAgMnP	označit
jako	jako	k9	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
A	A	kA	A
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alph	k1gMnSc4	alph
A	A	kA	A
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
hodnotu	hodnota	k1gFnSc4	hodnota
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
vektor	vektor	k1gInSc4	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
jsme	být	k5eAaImIp1nP	být
pak	pak	k6eAd1	pak
definovali	definovat	k5eAaBmAgMnP	definovat
výrazem	výraz	k1gInSc7	výraz
na	na	k7c6	na
pravé	pravý	k2eAgFnSc6d1	pravá
straně	strana	k1gFnSc6	strana
rovnosti	rovnost	k1gFnSc2	rovnost
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
zdůrazněme	zdůraznit	k5eAaPmRp1nP	zdůraznit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nyní	nyní	k6eAd1	nyní
nesčítáme	sčítat	k5eNaImIp1nP	sčítat
vektory	vektor	k1gInPc4	vektor
a	a	k8xC	a
nenásobíme	násobit	k5eNaImIp1nP	násobit
vektory	vektor	k1gInPc4	vektor
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	V	kA	V
<g/>
}	}	kIx)	}
,	,	kIx,	,
ale	ale	k8xC	ale
samotné	samotný	k2eAgMnPc4d1	samotný
lineární	lineární	k2eAgMnPc4d1	lineární
operátory	operátor	k1gMnPc4	operátor
<g/>
.	.	kIx.	.
</s>
<s>
Označme	označit	k5eAaPmRp1nP	označit
množinu	množina	k1gFnSc4	množina
všech	všecek	k3xTgMnPc2	všecek
lineárních	lineární	k2eAgMnPc2d1	lineární
operátorů	operátor	k1gMnPc2	operátor
působících	působící	k2eAgMnPc2d1	působící
na	na	k7c6	na
prostoru	prostor	k1gInSc6	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	v	k7c6	v
<g/>
}	}	kIx)	}
jako	jako	k9	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
V	V	kA	V
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathcal	mathcat	k5eAaPmAgInS	mathcat
{	{	kIx(	{
<g/>
L	L	kA	L
<g/>
}}	}}	k?	}}
<g/>
(	(	kIx(	(
<g/>
V	V	kA	V
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
této	tento	k3xDgFnSc6	tento
množině	množina	k1gFnSc6	množina
lze	lze	k6eAd1	lze
ukázat	ukázat	k5eAaPmF	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
tělesem	těleso	k1gNnSc7	těleso
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
}	}	kIx)	}
a	a	k8xC	a
právě	právě	k6eAd1	právě
zavedenými	zavedený	k2eAgFnPc7d1	zavedená
operacemi	operace	k1gFnPc7	operace
sčítání	sčítání	k1gNnSc2	sčítání
a	a	k8xC	a
násobení	násobení	k1gNnSc2	násobení
tvoří	tvořit	k5eAaImIp3nS	tvořit
vektorový	vektorový	k2eAgInSc1d1	vektorový
prostor	prostor	k1gInSc1	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Máme	mít	k5eAaImIp1nP	mít
tedy	tedy	k9	tedy
vektorový	vektorový	k2eAgInSc4d1	vektorový
prostor	prostor	k1gInSc4	prostor
lineárních	lineární	k2eAgInPc2d1	lineární
operátorů	operátor	k1gInPc2	operátor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
V	V	kA	V
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathcal	mathcat	k5eAaPmAgInS	mathcat
{	{	kIx(	{
<g/>
L	L	kA	L
<g/>
}}	}}	k?	}}
<g/>
(	(	kIx(	(
<g/>
V	V	kA	V
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
každý	každý	k3xTgInSc1	každý
působí	působit	k5eAaImIp3nP	působit
na	na	k7c6	na
dalším	další	k2eAgInSc6d1	další
vektorovém	vektorový	k2eAgInSc6d1	vektorový
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
prostoru	prostor	k1gInSc2	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	v	k7c6	v
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
Lineární	lineární	k2eAgInPc1d1	lineární
operátory	operátor	k1gInPc1	operátor
jsou	být	k5eAaImIp3nP	být
navíc	navíc	k6eAd1	navíc
jen	jen	k9	jen
speciálním	speciální	k2eAgInSc7d1	speciální
případem	případ	k1gInSc7	případ
obecnějšího	obecní	k2eAgInSc2d2	obecní
druhu	druh	k1gInSc2	druh
zobrazení	zobrazení	k1gNnSc2	zobrazení
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
nazýváme	nazývat	k5eAaImIp1nP	nazývat
lineární	lineární	k2eAgNnSc4d1	lineární
zobrazení	zobrazení	k1gNnSc4	zobrazení
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
mají	mít	k5eAaImIp3nP	mít
stejné	stejný	k2eAgFnPc1d1	stejná
vlastnosti	vlastnost	k1gFnPc1	vlastnost
jako	jako	k8xC	jako
lineární	lineární	k2eAgInPc1d1	lineární
operátory	operátor	k1gInPc1	operátor
až	až	k9	až
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vraceli	vracet	k5eAaImAgMnP	vracet
své	svůj	k3xOyFgFnPc4	svůj
hodnoty	hodnota	k1gFnPc4	hodnota
do	do	k7c2	do
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	V	kA	V
<g/>
}	}	kIx)	}
,	,	kIx,	,
tak	tak	k6eAd1	tak
je	on	k3xPp3gInPc4	on
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
ještě	ještě	k6eAd1	ještě
dalšího	další	k2eAgInSc2d1	další
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
označme	označit	k5eAaPmRp1nP	označit
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
W	W	kA	W
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
W	W	kA	W
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Lineární	lineární	k2eAgNnSc1d1	lineární
zobrazení	zobrazení	k1gNnSc1	zobrazení
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
zobrazení	zobrazení	k1gNnSc4	zobrazení
z	z	k7c2	z
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	V	kA	V
<g/>
}	}	kIx)	}
do	do	k7c2	do
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
W	W	kA	W
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
W	W	kA	W
<g/>
}	}	kIx)	}
,	,	kIx,	,
které	který	k3yQgNnSc1	který
splňuje	splňovat	k5eAaImIp3nS	splňovat
podmínku	podmínka	k1gFnSc4	podmínka
aditivity	aditivita	k1gFnSc2	aditivita
a	a	k8xC	a
homogenity	homogenita	k1gFnSc2	homogenita
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tato	tento	k3xDgNnPc1	tento
zobrazení	zobrazení	k1gNnPc1	zobrazení
můžeme	moct	k5eAaImIp1nP	moct
sčítat	sčítat	k5eAaImF	sčítat
a	a	k8xC	a
násobit	násobit	k5eAaImF	násobit
prvkem	prvek	k1gInSc7	prvek
z	z	k7c2	z
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
pro	pro	k7c4	pro
lineární	lineární	k2eAgMnPc4d1	lineární
operátory	operátor	k1gMnPc4	operátor
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
i	i	k9	i
u	u	k7c2	u
těchto	tento	k3xDgNnPc2	tento
zobrazení	zobrazení	k1gNnPc2	zobrazení
dalo	dát	k5eAaPmAgNnS	dát
ukázat	ukázat	k5eAaPmF	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tvoří	tvořit	k5eAaImIp3nP	tvořit
vektorový	vektorový	k2eAgInSc4d1	vektorový
prostor	prostor	k1gInSc4	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
vektorový	vektorový	k2eAgInSc4d1	vektorový
prostor	prostor	k1gInSc4	prostor
zobrazení	zobrazení	k1gNnSc2	zobrazení
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
berou	brát	k5eAaImIp3nP	brát
vektory	vektor	k1gInPc4	vektor
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	V	kA	V
<g/>
}	}	kIx)	}
a	a	k8xC	a
vrací	vracet	k5eAaImIp3nS	vracet
hodnoty	hodnota	k1gFnPc4	hodnota
do	do	k7c2	do
jiného	jiný	k2eAgInSc2d1	jiný
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
W	W	kA	W
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
W	W	kA	W
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Dimenze	dimenze	k1gFnSc1	dimenze
prostoru	prostor	k1gInSc2	prostor
všech	všecek	k3xTgInPc2	všecek
lineárních	lineární	k2eAgInPc2d1	lineární
operátorů	operátor	k1gInPc2	operátor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
V	V	kA	V
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathcal	mathcat	k5eAaPmAgInS	mathcat
{	{	kIx(	{
<g/>
L	L	kA	L
<g/>
}}	}}	k?	}}
<g/>
(	(	kIx(	(
<g/>
V	V	kA	V
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c4	na
dimenzi	dimenze	k1gFnSc4	dimenze
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	V	kA	V
<g/>
}	}	kIx)	}
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgNnSc6	který
tyto	tento	k3xDgInPc4	tento
operátory	operátor	k1gInPc4	operátor
působí	působit	k5eAaImIp3nP	působit
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
dimenze	dimenze	k1gFnSc1	dimenze
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	v	k7c4	v
<g/>
}	}	kIx)	}
nekonečná	konečný	k2eNgFnSc1d1	nekonečná
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
nekonečná	konečný	k2eNgFnSc1d1	nekonečná
i	i	k8xC	i
dimenze	dimenze	k1gFnSc1	dimenze
prostoru	prostor	k1gInSc2	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
V	V	kA	V
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathcal	mathcat	k5eAaPmAgInS	mathcat
{	{	kIx(	{
<g/>
L	L	kA	L
<g/>
}}	}}	k?	}}
<g/>
(	(	kIx(	(
<g/>
V	V	kA	V
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
dimenze	dimenze	k1gFnSc1	dimenze
prostoru	prostor	k1gInSc2	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	v	k7c6	v
<g/>
}	}	kIx)	}
konečná	konečný	k2eAgNnPc1d1	konečné
a	a	k8xC	a
rovná	rovný	k2eAgNnPc1d1	rovné
jistému	jistý	k2eAgNnSc3d1	jisté
přirozenému	přirozený	k2eAgNnSc3d1	přirozené
číslu	číslo	k1gNnSc3	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
n	n	k0	n
<g/>
}	}	kIx)	}
,	,	kIx,	,
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
<g />
.	.	kIx.	.
</s>
<s hack="1">
dimenze	dimenze	k1gFnSc1	dimenze
prostoru	prostor	k1gInSc2	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
V	V	kA	V
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathcal	mathcat	k5eAaPmAgInS	mathcat
{	{	kIx(	{
<g/>
L	L	kA	L
<g/>
}}	}}	k?	}}
<g/>
(	(	kIx(	(
<g/>
V	V	kA	V
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
taky	taky	k6eAd1	taky
konečná	konečný	k2eAgFnSc1d1	konečná
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
rovna	roven	k2eAgFnSc1d1	rovna
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
n	n	k0	n
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Máme	mít	k5eAaImIp1nP	mít
<g/>
-li	i	k?	-li
totiž	totiž	k9	totiž
vektor	vektor	k1gInSc4	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
z	z	k7c2	z
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	v	k7c6	v
<g/>
}	}	kIx)	}
dimenze	dimenze	k1gFnPc4	dimenze
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
n	n	k0	n
<g/>
}	}	kIx)	}
,	,	kIx,	,
tak	tak	k6eAd1	tak
jsme	být	k5eAaImIp1nP	být
tento	tento	k3xDgInSc4	tento
vektor	vektor	k1gInSc4	vektor
schopni	schopen	k2eAgMnPc1d1	schopen
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k8xS	jako
lineární	lineární	k2eAgFnSc4d1	lineární
kombinaci	kombinace	k1gFnSc4	kombinace
vektorů	vektor	k1gInPc2	vektor
báze	báze	k1gFnSc2	báze
<g/>
.	.	kIx.	.
</s>
<s>
Označme	označit	k5eAaPmRp1nP	označit
si	se	k3xPyFc3	se
tyto	tento	k3xDgInPc4	tento
vektory	vektor	k1gInPc4	vektor
báze	báze	k1gFnSc2	báze
jako	jako	k8xS	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
...	...	k?	...
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
pak	pak	k6eAd1	pak
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
i	i	k9	i
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
i	i	k8xC	i
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
jsou	být	k5eAaImIp3nP	být
souřadnice	souřadnice	k1gFnPc4	souřadnice
vektoru	vektor	k1gInSc2	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
ve	v	k7c6	v
zvolené	zvolený	k2eAgFnSc6d1	zvolená
bázi	báze	k1gFnSc6	báze
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
zapůsobíme	zapůsobit	k5eAaPmIp1nP	zapůsobit
na	na	k7c4	na
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
lineárním	lineární	k2eAgInSc7d1	lineární
operátorem	operátor	k1gInSc7	operátor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
L	L	kA	L
<g/>
}	}	kIx)	}
,	,	kIx,	,
tak	tak	k6eAd1	tak
z	z	k7c2	z
definice	definice	k1gFnSc2	definice
vlastností	vlastnost	k1gFnPc2	vlastnost
tohoto	tento	k3xDgInSc2	tento
operátoru	operátor	k1gInSc2	operátor
plyne	plynout	k5eAaImIp3nS	plynout
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
L	L	kA	L
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
i	i	k9	i
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
i	i	k9	i
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
i	i	k9	i
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
i	i	k8xC	i
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
i	i	k9	i
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
l	l	kA	l
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L	L	kA	L
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
L	L	kA	L
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
big	big	k?	big
(	(	kIx(	(
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
big	big	k?	big
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s>
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
L	L	kA	L
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
l	l	kA	l
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
kde	kde	k6eAd1	kde
jsme	být	k5eAaImIp1nP	být
označili	označit	k5eAaPmAgMnP	označit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
l	l	kA	l
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
L	L	kA	L
(	(	kIx(	(
:	:	kIx,	:
e	e	k0	e
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
l	l	kA	l
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
L	L	kA	L
<g/>
(	(	kIx(	(
<g/>
e_	e_	k?	e_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Vektory	vektor	k1gInPc1	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
l	l	kA	l
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
l	l	kA	l
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
ale	ale	k8xC	ale
zase	zase	k9	zase
leží	ležet	k5eAaImIp3nS	ležet
ve	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	V	kA	V
<g/>
}	}	kIx)	}
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
je	on	k3xPp3gFnPc4	on
tedy	tedy	k9	tedy
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
v	v	k7c6	v
bázi	báze	k1gFnSc6	báze
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
<g />
.	.	kIx.	.
</s>
<s hack="1">
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
...	...	k?	...
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
l	l	kA	l
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
j	j	k?	j
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
β	β	k?	β
:	:	kIx,	:
i	i	k9	i
j	j	k?	j
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
→	→	k?	→
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
j	j	k?	j
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
l	l	kA	l
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
ij	ij	k?	ij
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}}	}}	k?	}}
:	:	kIx,	:
pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
i	i	k9	i
∈	∈	k?	∈
{	{	kIx(	{
1	[number]	k4	1
,	,	kIx,	,
...	...	k?	...
,	,	kIx,	,
n	n	k0	n
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
i	i	k9	i
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
,	,	kIx,	,
<g/>
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
tak	tak	k9	tak
můžeme	moct	k5eAaImIp1nP	moct
psát	psát	k5eAaImF	psát
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
i	i	k9	i
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
j	j	k?	j
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
β	β	k?	β
:	:	kIx,	:
i	i	k9	i
<g />
.	.	kIx.	.
</s>
<s hack="1">
j	j	k?	j
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
e	e	k0	e
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
j	j	k?	j
:	:	kIx,	:
:	:	kIx,	:
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L	L	kA	L
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
<g />
.	.	kIx.	.
</s>
<s>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
ij	ij	k?	ij
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
e	e	k0	e
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Koeficienty	koeficient	k1gInPc1	koeficient
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
β	β	k?	β
:	:	kIx,	:
i	i	k9	i
j	j	k?	j
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
beta	beta	k1gNnSc1	beta
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
ij	ij	k?	ij
<g/>
}}	}}	k?	}}
popisují	popisovat	k5eAaImIp3nP	popisovat
působení	působení	k1gNnSc4	působení
lineárního	lineární	k2eAgInSc2d1	lineární
operátoru	operátor	k1gInSc2	operátor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
L	L	kA	L
<g/>
}	}	kIx)	}
na	na	k7c4	na
vektory	vektor	k1gInPc4	vektor
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	V	kA	V
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Těchto	tento	k3xDgInPc2	tento
koeficientů	koeficient	k1gInPc2	koeficient
je	být	k5eAaImIp3nS	být
zjevně	zjevně	k6eAd1	zjevně
dohromady	dohromady	k6eAd1	dohromady
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
n	n	k0	n
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Máme	mít	k5eAaImIp1nP	mít
tedy	tedy	k9	tedy
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
n	n	k0	n
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
nichž	jenž	k3xRgFnPc2	jenž
můžeme	moct	k5eAaImIp1nP	moct
</s>
<s>
popsat	popsat	k5eAaPmF	popsat
libovolný	libovolný	k2eAgInSc4d1	libovolný
operátor	operátor	k1gInSc4	operátor
působící	působící	k2eAgNnSc1d1	působící
na	na	k7c6	na
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	V	kA	V
<g/>
}	}	kIx)	}
a	a	k8xC	a
dimenze	dimenze	k1gFnSc1	dimenze
prostoru	prostor	k1gInSc2	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
V	V	kA	V
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathcal	mathcat	k5eAaPmAgInS	mathcat
{	{	kIx(	{
<g/>
L	L	kA	L
<g/>
}}	}}	k?	}}
<g/>
(	(	kIx(	(
<g/>
V	V	kA	V
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
rovna	roven	k2eAgFnSc1d1	rovna
tomuto	tento	k3xDgNnSc3	tento
číslu	číslo	k1gNnSc3	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Posloupnost	posloupnost	k1gFnSc4	posloupnost
<g/>
.	.	kIx.	.
</s>
<s>
Jistým	jistý	k2eAgNnSc7d1	jisté
zobecněním	zobecnění	k1gNnSc7	zobecnění
aritmetických	aritmetický	k2eAgInPc2d1	aritmetický
vektorů	vektor	k1gInPc2	vektor
na	na	k7c4	na
nekonečnou	konečný	k2eNgFnSc4d1	nekonečná
dimenzi	dimenze	k1gFnSc4	dimenze
jsou	být	k5eAaImIp3nP	být
posloupnosti	posloupnost	k1gFnPc1	posloupnost
prvků	prvek	k1gInPc2	prvek
z	z	k7c2	z
tělesa	těleso	k1gNnSc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
uspořádaných	uspořádaný	k2eAgFnPc2d1	uspořádaná
n-tic	nice	k1gFnPc2	n-tice
nyní	nyní	k6eAd1	nyní
bereme	brát	k5eAaImIp1nP	brát
posloupnosti	posloupnost	k1gFnPc1	posloupnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
složek	složka	k1gFnPc2	složka
nekonečně	konečně	k6eNd1	konečně
mnoho	mnoho	k4c1	mnoho
<g/>
.	.	kIx.	.
</s>
<s>
Operace	operace	k1gFnSc1	operace
sčítání	sčítání	k1gNnSc2	sčítání
a	a	k8xC	a
násobení	násobení	k1gNnSc2	násobení
prvkem	prvek	k1gInSc7	prvek
z	z	k7c2	z
tělesa	těleso	k1gNnSc2	těleso
můžeme	moct	k5eAaImIp1nP	moct
zavést	zavést	k5eAaPmF	zavést
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
pro	pro	k7c4	pro
aritmetické	aritmetický	k2eAgInPc4d1	aritmetický
vektory	vektor	k1gInPc4	vektor
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
bychom	by	kYmCp1nP	by
i	i	k9	i
ověřili	ověřit	k5eAaPmAgMnP	ověřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
množina	množina	k1gFnSc1	množina
všech	všecek	k3xTgFnPc2	všecek
posloupností	posloupnost	k1gFnPc2	posloupnost
prvků	prvek	k1gInPc2	prvek
daného	daný	k2eAgNnSc2d1	dané
tělesa	těleso	k1gNnSc2	těleso
tvoří	tvořit	k5eAaImIp3nS	tvořit
vektorový	vektorový	k2eAgInSc1d1	vektorový
prostor	prostor	k1gInSc1	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Nulovým	nulový	k2eAgInSc7d1	nulový
vektorem	vektor	k1gInSc7	vektor
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
posloupnost	posloupnost	k1gFnSc4	posloupnost
nul	nula	k1gFnPc2	nula
<g/>
,	,	kIx,	,
opačný	opačný	k2eAgInSc4d1	opačný
vektorem	vektor	k1gInSc7	vektor
k	k	k7c3	k
dané	daný	k2eAgFnSc3d1	daná
posloupnosti	posloupnost	k1gFnSc3	posloupnost
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
posloupnost	posloupnost	k1gFnSc1	posloupnost
opačných	opačný	k2eAgInPc2d1	opačný
prvků	prvek	k1gInPc2	prvek
atd.	atd.	kA	atd.
Více	hodně	k6eAd2	hodně
nás	my	k3xPp1nPc4	my
ale	ale	k9	ale
zajímají	zajímat	k5eAaImIp3nP	zajímat
posloupnosti	posloupnost	k1gFnPc1	posloupnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
konvergují	konvergovat	k5eAaImIp3nP	konvergovat
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
tyto	tento	k3xDgFnPc4	tento
posloupnosti	posloupnost	k1gFnPc4	posloupnost
vůbec	vůbec	k9	vůbec
tvořily	tvořit	k5eAaImAgFnP	tvořit
vektorový	vektorový	k2eAgInSc4d1	vektorový
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
ale	ale	k8xC	ale
nejprve	nejprve	k6eAd1	nejprve
musíme	muset	k5eAaImIp1nP	muset
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
součet	součet	k1gInSc1	součet
konvergentních	konvergentní	k2eAgFnPc2d1	konvergentní
posloupností	posloupnost	k1gFnPc2	posloupnost
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
konvergentní	konvergentní	k2eAgFnSc4d1	konvergentní
posloupnost	posloupnost	k1gFnSc4	posloupnost
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
pro	pro	k7c4	pro
násobek	násobek	k1gInSc4	násobek
<g/>
.	.	kIx.	.
</s>
<s>
Omezíme	omezit	k5eAaPmIp1nP	omezit
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
na	na	k7c4	na
těleso	těleso	k1gNnSc4	těleso
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
a	a	k8xC	a
využijeme	využít	k5eAaPmIp1nP	využít
vlastností	vlastnost	k1gFnSc7	vlastnost
limity	limita	k1gFnSc2	limita
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
k	k	k7c3	k
→	→	k?	→
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
k	k	k7c3	k
→	→	k?	→
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
+	+	kIx~	+
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
k	k	k7c3	k
→	→	k?	→
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
k	k	k7c3	k
→	→	k?	→
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
α	α	k?	α
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
α	α	k?	α
:	:	kIx,	:
lim	limo	k1gNnPc2	limo
:	:	kIx,	:
k	k	k7c3	k
→	→	k?	→
∞	∞	k?	∞
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
<g/>
infty	inft	k1gInPc4	inft
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g />
.	.	kIx.	.
</s>
<s>
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
<g/>
infty	inft	k1gInPc4	inft
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
<g/>
infty	inft	k1gInPc4	inft
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s>
k	k	k7c3	k
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
quad	quad	k1gInSc1	quad
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
<g/>
infty	inft	k1gInPc4	inft
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
\	\	kIx~	\
<g/>
lim	limo	k1gNnPc2	limo
<g />
.	.	kIx.	.
</s>
<s hack="1">
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc4	ten
\	\	kIx~	\
<g/>
infty	inft	k1gInPc4	inft
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
vidíme	vidět	k5eAaImIp1nP	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
součet	součet	k1gInSc4	součet
dvou	dva	k4xCgFnPc2	dva
konvergentních	konvergentní	k2eAgFnPc2d1	konvergentní
posloupností	posloupnost	k1gFnPc2	posloupnost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s hack="1">
,	,	kIx,	,
(	(	kIx(	(
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
k	k	k7c3	k
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
(	(	kIx(	(
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
k	k	k7c3	k
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
konvergentní	konvergentní	k2eAgFnSc4d1	konvergentní
posloupnost	posloupnost	k1gFnSc4	posloupnost
a	a	k8xC	a
totéž	týž	k3xTgNnSc4	týž
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
∈	∈	k?	∈
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
\	\	kIx~	\
<g/>
in	in	k?	in
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
}	}	kIx)	}
násobek	násobek	k1gInSc4	násobek
konvergentní	konvergentní	k2eAgFnSc2d1	konvergentní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
<g/>
.	.	kIx.	.
</s>
<s>
Množina	množina	k1gFnSc1	množina
reálných	reálný	k2eAgFnPc2d1	reálná
konvergentních	konvergentní	k2eAgFnPc2d1	konvergentní
posloupností	posloupnost	k1gFnPc2	posloupnost
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
na	na	k7c4	na
součet	součet	k1gInSc4	součet
svých	svůj	k3xOyFgInPc2	svůj
prvků	prvek	k1gInPc2	prvek
a	a	k8xC	a
na	na	k7c4	na
násobení	násobení	k1gNnSc4	násobení
svých	svůj	k3xOyFgInPc2	svůj
prvků	prvek	k1gInPc2	prvek
číslem	číslo	k1gNnSc7	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
množina	množina	k1gFnSc1	množina
tvoří	tvořit	k5eAaImIp3nS	tvořit
podprostor	podprostor	k1gInSc4	podprostor
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
všech	všecek	k3xTgFnPc2	všecek
posloupností	posloupnost	k1gFnPc2	posloupnost
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
Příklad	příklad	k1gInSc1	příklad
3	[number]	k4	3
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vektorový	vektorový	k2eAgInSc4d1	vektorový
podprostor	podprostor	k1gInSc4	podprostor
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
jsou	být	k5eAaImIp3nP	být
kvůli	kvůli	k7c3	kvůli
konvergenci	konvergence	k1gFnSc3	konvergence
na	na	k7c4	na
prvky	prvek	k1gInPc4	prvek
posloupnosti	posloupnost	k1gFnSc2	posloupnost
nakladena	nakladen	k2eAgNnPc4d1	nakladeno
jistá	jistý	k2eAgNnPc4d1	jisté
omezení	omezení	k1gNnPc4	omezení
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
prostor	prostor	k1gInSc1	prostor
konvergentních	konvergentní	k2eAgFnPc2d1	konvergentní
posloupností	posloupnost	k1gFnPc2	posloupnost
nekonečnou	konečný	k2eNgFnSc4d1	nekonečná
dimenzi	dimenze	k1gFnSc4	dimenze
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
o	o	k7c4	o
nekonečnědimezionální	konečnědimezionální	k2eNgInSc4d1	konečnědimezionální
podprostor	podprostor	k1gInSc4	podprostor
prostoru	prostor	k1gInSc2	prostor
všech	všecek	k3xTgFnPc2	všecek
posloupností	posloupnost	k1gFnPc2	posloupnost
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Spojitá	spojitý	k2eAgFnSc1d1	spojitá
funkce	funkce	k1gFnSc1	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Uvažujme	uvažovat	k5eAaImRp1nP	uvažovat
nyní	nyní	k6eAd1	nyní
množinu	množina	k1gFnSc4	množina
všech	všecek	k3xTgFnPc2	všecek
spojitých	spojitý	k2eAgFnPc2d1	spojitá
reálných	reálný	k2eAgFnPc2d1	reálná
funkcí	funkce	k1gFnPc2	funkce
jedné	jeden	k4xCgFnSc2	jeden
reálné	reálný	k2eAgFnSc2d1	reálná
proměnné	proměnná	k1gFnSc2	proměnná
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
si	se	k3xPyFc3	se
vezměme	vzít	k5eAaPmRp1nP	vzít
těleso	těleso	k1gNnSc4	těleso
reálných	reálný	k2eAgNnPc2d1	reálné
čísel	číslo	k1gNnPc2	číslo
a	a	k8xC	a
definujme	definovat	k5eAaBmRp1nP	definovat
si	se	k3xPyFc3	se
operace	operace	k1gFnPc4	operace
sčítání	sčítání	k1gNnSc4	sčítání
dvou	dva	k4xCgFnPc2	dva
funkcí	funkce	k1gFnPc2	funkce
a	a	k8xC	a
násobení	násobení	k1gNnSc2	násobení
funkce	funkce	k1gFnSc2	funkce
číslem	číslo	k1gNnSc7	číslo
bodově	bodově	k6eAd1	bodově
<g/>
:	:	kIx,	:
pro	pro	k7c4	pro
libovolné	libovolný	k2eAgFnPc4d1	libovolná
dvě	dva	k4xCgFnPc4	dva
funkce	funkce	k1gFnPc4	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
,	,	kIx,	,
g	g	kA	g
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
f	f	k?	f
<g/>
,	,	kIx,	,
<g/>
g	g	kA	g
<g/>
}	}	kIx)	}
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s hack="1">
číslo	číslo	k1gNnSc1	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
∈	∈	k?	∈
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
\	\	kIx~	\
<g/>
in	in	k?	in
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
}	}	kIx)	}
mějme	mít	k5eAaImRp1nP	mít
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
f	f	k?	f
+	+	kIx~	+
g	g	kA	g
)	)	kIx)	)
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
=	=	kIx~	=
f	f	k?	f
<g />
.	.	kIx.	.
</s>
<s hack="1">
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
+	+	kIx~	+
g	g	kA	g
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
,	,	kIx,	,
:	:	kIx,	:
(	(	kIx(	(
α	α	k?	α
f	f	k?	f
)	)	kIx)	)
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
=	=	kIx~	=
α	α	k?	α
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
f	f	k?	f
<g/>
+	+	kIx~	+
<g/>
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
f	f	k?	f
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
g	g	kA	g
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
quad	quad	k6eAd1	quad
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
f	f	k?	f
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
x	x	k?	x
<g/>
}	}	kIx)	}
probíhá	probíhat	k5eAaImIp3nS	probíhat
reálnou	reálný	k2eAgFnSc4d1	reálná
osu	osa	k1gFnSc4	osa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
matematické	matematický	k2eAgFnSc6d1	matematická
analýze	analýza	k1gFnSc6	analýza
se	se	k3xPyFc4	se
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
součet	součet	k1gInSc1	součet
dvou	dva	k4xCgFnPc2	dva
spojitých	spojitý	k2eAgFnPc2d1	spojitá
funkcí	funkce	k1gFnPc2	funkce
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
spojitá	spojitý	k2eAgFnSc1d1	spojitá
funkce	funkce	k1gFnSc1	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
násobek	násobek	k1gInSc1	násobek
spojité	spojitý	k2eAgFnSc2d1	spojitá
funkce	funkce	k1gFnSc2	funkce
je	být	k5eAaImIp3nS	být
spojitá	spojitý	k2eAgFnSc1d1	spojitá
funkce	funkce	k1gFnSc1	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Množina	množina	k1gFnSc1	množina
všech	všecek	k3xTgFnPc2	všecek
spojitých	spojitý	k2eAgFnPc2d1	spojitá
reálných	reálný	k2eAgFnPc2d1	reálná
funkcí	funkce	k1gFnPc2	funkce
reálné	reálný	k2eAgFnSc2d1	reálná
proměnné	proměnná	k1gFnSc2	proměnná
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
na	na	k7c4	na
sčítání	sčítání	k1gNnSc4	sčítání
funkcí	funkce	k1gFnPc2	funkce
a	a	k8xC	a
násobení	násobení	k1gNnPc2	násobení
funkcí	funkce	k1gFnPc2	funkce
číslem	číslo	k1gNnSc7	číslo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
nutný	nutný	k2eAgInSc1d1	nutný
předpoklad	předpoklad	k1gInSc1	předpoklad
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
vektorovým	vektorový	k2eAgInSc7d1	vektorový
prostorem	prostor	k1gInSc7	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Ověřením	ověření	k1gNnSc7	ověření
axiomů	axiom	k1gInPc2	axiom
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
skutečně	skutečně	k6eAd1	skutečně
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
množina	množina	k1gFnSc1	množina
tvoří	tvořit	k5eAaImIp3nS	tvořit
vektorový	vektorový	k2eAgInSc4d1	vektorový
prostor	prostor	k1gInSc4	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
prostor	prostor	k1gInSc1	prostor
přitom	přitom	k6eAd1	přitom
představuje	představovat	k5eAaImIp3nS	představovat
podprostor	podprostor	k1gInSc4	podprostor
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
všech	všecek	k3xTgFnPc2	všecek
reálných	reálný	k2eAgFnPc2d1	reálná
funkcí	funkce	k1gFnPc2	funkce
reálné	reálný	k2eAgFnSc2d1	reálná
proměnné	proměnná	k1gFnSc2	proměnná
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
Příklad	příklad	k1gInSc1	příklad
2	[number]	k4	2
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Vektorový	vektorový	k2eAgInSc4d1	vektorový
podprostor	podprostor	k1gInSc4	podprostor
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
též	též	k9	též
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
vektorový	vektorový	k2eAgInSc1d1	vektorový
prostor	prostor	k1gInSc1	prostor
uvažovaných	uvažovaný	k2eAgFnPc2d1	uvažovaná
spojitých	spojitý	k2eAgFnPc2d1	spojitá
funkcí	funkce	k1gFnPc2	funkce
je	být	k5eAaImIp3nS	být
nekonečné	konečný	k2eNgFnPc4d1	nekonečná
dimenze	dimenze	k1gFnPc4	dimenze
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Polynom	polynom	k1gInSc1	polynom
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Motivaci	motivace	k1gFnSc6	motivace
jsme	být	k5eAaImIp1nP	být
použili	použít	k5eAaPmAgMnP	použít
prostor	prostor	k1gInSc4	prostor
všech	všecek	k3xTgInPc2	všecek
polynomů	polynom	k1gInPc2	polynom
(	(	kIx(	(
<g/>
všech	všecek	k3xTgInPc2	všecek
číselných	číselný	k2eAgInPc2d1	číselný
polynomů	polynom	k1gInPc2	polynom
jedné	jeden	k4xCgFnSc2	jeden
reálné	reálný	k2eAgFnSc2d1	reálná
proměnné	proměnná	k1gFnSc2	proměnná
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
se	se	k3xPyFc4	se
abstrakcí	abstrakce	k1gFnSc7	abstrakce
jeho	jeho	k3xOp3gFnPc2	jeho
vlastností	vlastnost	k1gFnPc2	vlastnost
dobrali	dobrat	k5eAaPmAgMnP	dobrat
pojmu	pojem	k1gInSc3	pojem
vektorový	vektorový	k2eAgInSc1d1	vektorový
prostor	prostor	k1gInSc1	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
těžké	těžký	k2eAgNnSc1d1	těžké
ověřit	ověřit	k5eAaPmF	ověřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
množina	množina	k1gFnSc1	množina
skutečně	skutečně	k6eAd1	skutečně
splňuje	splňovat	k5eAaImIp3nS	splňovat
všechny	všechen	k3xTgInPc4	všechen
axiomy	axiom	k1gInPc4	axiom
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
součet	součet	k1gInSc1	součet
a	a	k8xC	a
násobek	násobek	k1gInSc1	násobek
polynomů	polynom	k1gInPc2	polynom
je	být	k5eAaImIp3nS	být
definován	definovat	k5eAaBmNgInS	definovat
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
ostatní	ostatní	k2eAgFnPc4d1	ostatní
spojité	spojitý	k2eAgFnPc4d1	spojitá
funkce	funkce	k1gFnPc4	funkce
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
předchozí	předchozí	k2eAgFnSc2d1	předchozí
příklad	příklad	k1gInSc4	příklad
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
v	v	k7c6	v
oddíle	oddíl	k1gInSc6	oddíl
Dimenze	dimenze	k1gFnSc2	dimenze
výše	výše	k1gFnSc2	výše
bylo	být	k5eAaImAgNnS	být
naznačeno	naznačit	k5eAaPmNgNnS	naznačit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vektorový	vektorový	k2eAgInSc1d1	vektorový
prostor	prostor	k1gInSc1	prostor
všech	všecek	k3xTgInPc2	všecek
polynomů	polynom	k1gInPc2	polynom
je	být	k5eAaImIp3nS	být
nekonečněrozměrný	konečněrozměrný	k2eNgInSc1d1	konečněrozměrný
<g/>
,	,	kIx,	,
značíme	značit	k5eAaImIp1nP	značit
ho	on	k3xPp3gNnSc4	on
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathcal	mathcat	k5eAaPmAgInS	mathcat
{	{	kIx(	{
<g/>
P	P	kA	P
<g/>
}}}	}}}	k?	}}}
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
ale	ale	k8xC	ale
najít	najít	k5eAaPmF	najít
jistou	jistý	k2eAgFnSc4d1	jistá
podmnožinu	podmnožina	k1gFnSc4	podmnožina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
bude	být	k5eAaImBp3nS	být
tvořit	tvořit	k5eAaImF	tvořit
konečněrozměrný	konečněrozměrný	k2eAgInSc1d1	konečněrozměrný
vektorový	vektorový	k2eAgInSc1d1	vektorový
podprostor	podprostor	k1gInSc1	podprostor
<g/>
.	.	kIx.	.
</s>
<s>
Máme	mít	k5eAaImIp1nP	mít
na	na	k7c6	na
mysli	mysl	k1gFnSc6	mysl
konkrétně	konkrétně	k6eAd1	konkrétně
množinu	množina	k1gFnSc4	množina
všech	všecek	k3xTgInPc2	všecek
polynomů	polynom	k1gInPc2	polynom
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
stupeň	stupeň	k1gInSc1	stupeň
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgInSc1d2	menší
nebo	nebo	k8xC	nebo
roven	roven	k2eAgInSc1d1	roven
jistému	jistý	k2eAgNnSc3d1	jisté
zadanému	zadaný	k2eAgNnSc3d1	zadané
přirozenému	přirozený	k2eAgNnSc3d1	přirozené
číslu	číslo	k1gNnSc3	číslo
n.	n.	k?	n.
Je	být	k5eAaImIp3nS	být
snadné	snadný	k2eAgNnSc1d1	snadné
si	se	k3xPyFc3	se
rozmyslet	rozmyslet	k5eAaPmF	rozmyslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
součtem	součet	k1gInSc7	součet
polynomů	polynom	k1gInPc2	polynom
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
stupeň	stupeň	k1gInSc1	stupeň
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgInSc1d2	menší
než	než	k8xS	než
n	n	k0	n
opět	opět	k6eAd1	opět
dostanu	dostat	k5eAaPmIp1nS	dostat
polynom	polynom	k1gInSc1	polynom
se	s	k7c7	s
stupněm	stupeň	k1gInSc7	stupeň
nepřevyšujícím	převyšující	k2eNgInSc7d1	nepřevyšující
n.	n.	k?	n.
Podobně	podobně	k6eAd1	podobně
pro	pro	k7c4	pro
násobek	násobek	k1gInSc4	násobek
polynomu	polynom	k1gInSc2	polynom
číslem	číslo	k1gNnSc7	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Označme	označit	k5eAaPmRp1nP	označit
si	se	k3xPyFc3	se
množinu	množina	k1gFnSc4	množina
těchto	tento	k3xDgInPc2	tento
polynomů	polynom	k1gInPc2	polynom
jako	jako	k8xC	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
+	+	kIx~	+
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathcal	mathcat	k5eAaPmAgInS	mathcat
{	{	kIx(	{
<g/>
P	P	kA	P
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
množiny	množina	k1gFnSc2	množina
zahrnujeme	zahrnovat	k5eAaImIp1nP	zahrnovat
i	i	k9	i
nulový	nulový	k2eAgInSc4d1	nulový
polynom	polynom	k1gInSc4	polynom
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
stupeň	stupeň	k1gInSc1	stupeň
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
nedefinuje	definovat	k5eNaBmIp3nS	definovat
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
polynom	polynom	k1gInSc4	polynom
stupně	stupeň	k1gInSc2	stupeň
n	n	k0	n
popsán	popsat	k5eAaPmNgInS	popsat
n	n	k0	n
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
koeficienty	koeficient	k1gInPc4	koeficient
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgInSc1	jeden
koeficient	koeficient	k1gInSc1	koeficient
u	u	k7c2	u
každé	každý	k3xTgFnSc2	každý
mocniny	mocnina	k1gFnSc2	mocnina
nezávisle	závisle	k6eNd1	závisle
proměnné	proměnná	k1gFnSc2	proměnná
plus	plus	k6eAd1	plus
absolutní	absolutní	k2eAgInSc1d1	absolutní
člen	člen	k1gInSc1	člen
<g/>
,	,	kIx,	,
u	u	k7c2	u
něhož	jenž	k3xRgMnSc4	jenž
žádná	žádný	k3yNgFnSc1	žádný
mocnina	mocnina	k1gFnSc1	mocnina
není	být	k5eNaImIp3nS	být
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dimenze	dimenze	k1gFnSc1	dimenze
prostoru	prostor	k1gInSc2	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
P	P	kA	P
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
+	+	kIx~	+
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathcal	mathcat	k5eAaPmAgInS	mathcat
{	{	kIx(	{
<g/>
P	P	kA	P
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
rovna	roven	k2eAgFnSc1d1	rovna
n	n	k0	n
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
užitečné	užitečný	k2eAgNnSc1d1	užitečné
vybavit	vybavit	k5eAaPmF	vybavit
vektorový	vektorový	k2eAgInSc4d1	vektorový
prostor	prostor	k1gInSc4	prostor
dodatečnou	dodatečný	k2eAgFnSc7d1	dodatečná
strukturou	struktura	k1gFnSc7	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
např.	např.	kA	např.
zavedení	zavedení	k1gNnSc4	zavedení
pojmu	pojem	k1gInSc2	pojem
délky	délka	k1gFnSc2	délka
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
vektorový	vektorový	k2eAgInSc1d1	vektorový
prostor	prostor	k1gInSc1	prostor
stane	stanout	k5eAaPmIp3nS	stanout
navíc	navíc	k6eAd1	navíc
metrickým	metrický	k2eAgInSc7d1	metrický
prostorem	prostor	k1gInSc7	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Formálně	formálně	k6eAd1	formálně
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
pojem	pojem	k1gInSc1	pojem
zavádí	zavádět	k5eAaImIp3nS	zavádět
jako	jako	k9	jako
jisté	jistý	k2eAgNnSc1d1	jisté
zobrazení	zobrazení	k1gNnSc1	zobrazení
z	z	k7c2	z
množiny	množina	k1gFnSc2	množina
vektorů	vektor	k1gInPc2	vektor
do	do	k7c2	do
množiny	množina	k1gFnSc2	množina
kladných	kladný	k2eAgNnPc2d1	kladné
(	(	kIx(	(
<g/>
reálných	reálný	k2eAgNnPc2d1	reálné
<g/>
)	)	kIx)	)
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgNnSc3	tento
zobrazení	zobrazení	k1gNnSc3	zobrazení
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
norma	norma	k1gFnSc1	norma
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
splňovat	splňovat	k5eAaImF	splňovat
jisté	jistý	k2eAgFnPc4d1	jistá
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
oddíl	oddíl	k1gInSc1	oddíl
Normované	normovaný	k2eAgFnSc2d1	normovaná
vektorové	vektorový	k2eAgFnSc2d1	vektorová
prostory	prostora	k1gFnSc2	prostora
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
můžeme	moct	k5eAaImIp1nP	moct
zavést	zavést	k5eAaPmF	zavést
na	na	k7c6	na
vektorovém	vektorový	k2eAgInSc6d1	vektorový
prostoru	prostor	k1gInSc6	prostor
skalární	skalární	k2eAgInSc1d1	skalární
součin	součin	k1gInSc1	součin
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
nám	my	k3xPp1nPc3	my
mj.	mj.	kA	mj.
umožní	umožnit	k5eAaPmIp3nS	umožnit
měřit	měřit	k5eAaImF	měřit
úhly	úhel	k1gInPc4	úhel
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
vektory	vektor	k1gInPc7	vektor
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
Prostory	prostor	k1gInPc7	prostor
se	s	k7c7	s
skalárním	skalární	k2eAgInSc7d1	skalární
součinem	součin	k1gInSc7	součin
níže	nízce	k6eAd2	nízce
<g/>
.	.	kIx.	.
</s>
<s>
Pokročilejším	pokročilý	k2eAgInSc7d2	pokročilejší
příkladem	příklad	k1gInSc7	příklad
zavedení	zavedení	k1gNnSc2	zavedení
dodatečné	dodatečný	k2eAgFnSc2d1	dodatečná
struktury	struktura	k1gFnSc2	struktura
na	na	k7c6	na
vektorovém	vektorový	k2eAgInSc6d1	vektorový
prostoru	prostor	k1gInSc6	prostor
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
zabývá	zabývat	k5eAaImIp3nS	zabývat
oddíl	oddíl	k1gInSc1	oddíl
Topologický	topologický	k2eAgInSc1d1	topologický
vektorový	vektorový	k2eAgInSc1d1	vektorový
prostor	prostor	k1gInSc1	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Normovaný	normovaný	k2eAgInSc1d1	normovaný
lineární	lineární	k2eAgInSc1d1	lineární
prostor	prostor	k1gInSc1	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Uvažujme	uvažovat	k5eAaImRp1nP	uvažovat
v	v	k7c6	v
dalším	další	k1gNnSc6	další
vektorový	vektorový	k2eAgInSc4d1	vektorový
prostor	prostor	k1gInSc4	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	v	k7c4	v
<g/>
}	}	kIx)	}
nad	nad	k7c7	nad
číselným	číselný	k2eAgNnSc7d1	číselné
tělesem	těleso	k1gNnSc7	těleso
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
bylo	být	k5eAaImAgNnS	být
zmíněno	zmínit	k5eAaPmNgNnS	zmínit
v	v	k7c6	v
úvodu	úvod	k1gInSc6	úvod
nadřazené	nadřazený	k2eAgFnSc2d1	nadřazená
sekce	sekce	k1gFnSc2	sekce
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
důvodů	důvod	k1gInPc2	důvod
velmi	velmi	k6eAd1	velmi
užitečné	užitečný	k2eAgNnSc1d1	užitečné
zavést	zavést	k5eAaPmF	zavést
ve	v	k7c6	v
vektorovém	vektorový	k2eAgInSc6d1	vektorový
prostoru	prostor	k1gInSc6	prostor
pojem	pojem	k1gInSc1	pojem
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Matematicky	matematicky	k6eAd1	matematicky
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
realizován	realizován	k2eAgInSc1d1	realizován
pomocí	pomocí	k7c2	pomocí
pomocného	pomocný	k2eAgNnSc2d1	pomocné
zobrazení	zobrazení	k1gNnSc2	zobrazení
zvaného	zvaný	k2eAgInSc2d1	zvaný
norma	norma	k1gFnSc1	norma
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
vektorový	vektorový	k2eAgInSc1d1	vektorový
prostor	prostor	k1gInSc1	prostor
do	do	k7c2	do
množiny	množina	k1gFnSc2	množina
nezáporných	záporný	k2eNgNnPc2d1	nezáporné
(	(	kIx(	(
<g/>
reálných	reálný	k2eAgNnPc2d1	reálné
<g/>
)	)	kIx)	)
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Každému	každý	k3xTgInSc3	každý
vektoru	vektor	k1gInSc3	vektor
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
jednoznačně	jednoznačně	k6eAd1	jednoznačně
přiřazeno	přiřazen	k2eAgNnSc1d1	přiřazeno
nezáporné	záporný	k2eNgNnSc1d1	nezáporné
číslo	číslo	k1gNnSc1	číslo
-	-	kIx~	-
jeho	jeho	k3xOp3gFnSc1	jeho
"	"	kIx"	"
<g/>
délka	délka	k1gFnSc1	délka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
přirozené	přirozený	k2eAgNnSc1d1	přirozené
požadovat	požadovat	k5eAaImF	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
měl	mít	k5eAaImAgInS	mít
nulový	nulový	k2eAgInSc1d1	nulový
vektor	vektor	k1gInSc1	vektor
nulovou	nulový	k2eAgFnSc4d1	nulová
délku	délka	k1gFnSc4	délka
a	a	k8xC	a
žádný	žádný	k3yNgInSc4	žádný
jiný	jiný	k2eAgInSc4d1	jiný
vektor	vektor	k1gInSc4	vektor
nenulovou	nulový	k2eNgFnSc4d1	nenulová
délku	délka	k1gFnSc4	délka
neměl	mít	k5eNaImAgMnS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Neboli	neboli	k8xC	neboli
<g/>
,	,	kIx,	,
nulovou	nulový	k2eAgFnSc4d1	nulová
délku	délka	k1gFnSc4	délka
má	mít	k5eAaImIp3nS	mít
právě	právě	k9	právě
jen	jen	k9	jen
nulový	nulový	k2eAgInSc4d1	nulový
vektor	vektor	k1gInSc4	vektor
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
požadujeme	požadovat	k5eAaImIp1nP	požadovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
prodloužíme	prodloužit	k5eAaPmIp1nP	prodloužit
<g/>
-li	i	k?	-li
daný	daný	k2eAgInSc1d1	daný
vektor	vektor	k1gInSc1	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
}	}	kIx)	}
-krát	rát	k1gMnSc1	-krát
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
délka	délka	k1gFnSc1	délka
vzroste	vzrůst	k5eAaPmIp3nS	vzrůst
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
}	}	kIx)	}
-krát	rát	k1gInSc1	-krát
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
}	}	kIx)	}
kladné	kladný	k2eAgInPc1d1	kladný
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
bychom	by	kYmCp1nP	by
brali	brát	k5eAaImAgMnP	brát
absolutní	absolutní	k2eAgFnSc4d1	absolutní
hodnotu	hodnota	k1gFnSc4	hodnota
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
|	|	kIx~	|
<g/>
}	}	kIx)	}
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
norma	norma	k1gFnSc1	norma
zobecněním	zobecnění	k1gNnSc7	zobecnění
pojmu	pojem	k1gInSc2	pojem
absolutní	absolutní	k2eAgFnSc2d1	absolutní
hodnoty	hodnota	k1gFnSc2	hodnota
na	na	k7c6	na
reálných	reálný	k2eAgInPc6d1	reálný
číslech	číslo	k1gNnPc6	číslo
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
požadujeme	požadovat	k5eAaImIp1nP	požadovat
splnění	splnění	k1gNnSc4	splnění
trojúhelníkové	trojúhelníkový	k2eAgFnSc2d1	trojúhelníková
nerovnosti	nerovnost	k1gFnSc2	nerovnost
<g/>
.	.	kIx.	.
</s>
<s>
Že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
též	též	k9	též
o	o	k7c4	o
přirozený	přirozený	k2eAgInSc4d1	přirozený
požadavek	požadavek	k1gInSc4	požadavek
je	být	k5eAaImIp3nS	být
názorně	názorně	k6eAd1	názorně
vidět	vidět	k5eAaImF	vidět
z	z	k7c2	z
příkladu	příklad	k1gInSc2	příklad
šipek	šipka	k1gFnPc2	šipka
v	v	k7c6	v
rovině	rovina	k1gFnSc6	rovina
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
Motivace	motivace	k1gFnSc1	motivace
a	a	k8xC	a
obrázek	obrázek	k1gInSc1	obrázek
Obr	obr	k1gMnSc1	obr
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
součet	součet	k1gInSc1	součet
délek	délka	k1gFnPc2	délka
šipek	šipka	k1gFnPc2	šipka
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
alespoň	alespoň	k9	alespoň
tak	tak	k6eAd1	tak
velký	velký	k2eAgInSc1d1	velký
jako	jako	k8xC	jako
délka	délka	k1gFnSc1	délka
šipky	šipka	k1gFnSc2	šipka
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Jinak	jinak	k6eAd1	jinak
bychom	by	kYmCp1nP	by
nemohli	moct	k5eNaImAgMnP	moct
sestrojit	sestrojit	k5eAaPmF	sestrojit
trojúhelník	trojúhelník	k1gInSc4	trojúhelník
o	o	k7c6	o
stranách	strana	k1gFnPc6	strana
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}}	}}	k?	}}
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Přeformulujeme	přeformulovat	k5eAaPmIp1nP	přeformulovat
<g/>
-li	i	k?	-li
právě	právě	k9	právě
uvedené	uvedený	k2eAgInPc4d1	uvedený
požadavky	požadavek	k1gInPc4	požadavek
do	do	k7c2	do
matematické	matematický	k2eAgFnSc2d1	matematická
podoby	podoba	k1gFnSc2	podoba
<g/>
,	,	kIx,	,
dostáváme	dostávat	k5eAaImIp1nP	dostávat
matematickou	matematický	k2eAgFnSc4d1	matematická
definici	definice	k1gFnSc4	definice
normy	norma	k1gFnSc2	norma
<g/>
:	:	kIx,	:
Norma	Norma	k1gFnSc1	Norma
<g/>
,	,	kIx,	,
značíme	značit	k5eAaImIp1nP	značit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
⋅	⋅	k?	⋅
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zobrazení	zobrazení	k1gNnSc1	zobrazení
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	v	k7c6	v
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
nad	nad	k7c7	nad
tělesem	těleso	k1gNnSc7	těleso
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
}	}	kIx)	}
)	)	kIx)	)
do	do	k7c2	do
nezáporných	záporný	k2eNgFnPc2d1	nezáporná
<g />
.	.	kIx.	.
</s>
<s hack="1">
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
⋅	⋅	k?	⋅
∥	∥	k?	∥
:	:	kIx,	:
V	v	k7c6	v
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
:	:	kIx,	:
<g/>
V	v	k7c6	v
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gInSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
}}	}}	k?	}}
,	,	kIx,	,
splňující	splňující	k2eAgInPc4d1	splňující
následující	následující	k2eAgInPc4d1	následující
tři	tři	k4xCgInPc4	tři
požadavky	požadavek	k1gInPc4	požadavek
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
∀	∀	k?	∀
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
V	V	kA	V
)	)	kIx)	)
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s hack="1">
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
=	=	kIx~	=
0	[number]	k4	0
⇔	⇔	k?	⇔
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
forall	forall	k1gInSc1	forall
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
V	V	kA	V
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
\	\	kIx~	\
<g/>
Leftrightarrow	Leftrightarrow	k1gFnSc1	Leftrightarrow
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
∀	∀	k?	∀
α	α	k?	α
∈	∈	k?	∈
T	T	kA	T
)	)	kIx)	)
(	(	kIx(	(
∥	∥	k?	∥
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
=	=	kIx~	=
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
∥	∥	k?	∥
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
)	)	kIx)	)
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
forall	forall	k1gMnSc1	forall
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
\	\	kIx~	\
<g/>
in	in	k?	in
T	T	kA	T
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
=	=	kIx~	=
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
∀	∀	k?	∀
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s hack="1">
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
V	V	kA	V
)	)	kIx)	)
(	(	kIx(	(
∀	∀	k?	∀
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
V	V	kA	V
)	)	kIx)	)
(	(	kIx(	(
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
≤	≤	k?	≤
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
∥	∥	k?	∥
+	+	kIx~	+
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
)	)	kIx)	)
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
forall	forall	k1gInSc1	forall
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
V	V	kA	V
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
forall	forall	k1gInSc1	forall
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
V	V	kA	V
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Zavedením	zavedení	k1gNnSc7	zavedení
normy	norma	k1gFnSc2	norma
na	na	k7c6	na
vektorovém	vektorový	k2eAgInSc6d1	vektorový
prostoru	prostor	k1gInSc6	prostor
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc4	tento
stává	stávat	k5eAaImIp3nS	stávat
metrickým	metrický	k2eAgInSc7d1	metrický
prostorem	prostor	k1gInSc7	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Norma	Norma	k1gFnSc1	Norma
totiž	totiž	k9	totiž
vyhovuje	vyhovovat	k5eAaImIp3nS	vyhovovat
definičním	definiční	k2eAgFnPc3d1	definiční
podmínkám	podmínka	k1gFnPc3	podmínka
metriky	metrika	k1gFnSc2	metrika
<g/>
.	.	kIx.	.
</s>
<s>
Níže	nízce	k6eAd2	nízce
si	se	k3xPyFc3	se
uvedeme	uvést	k5eAaPmIp1nP	uvést
pár	pár	k4xCyI	pár
příkladů	příklad	k1gInPc2	příklad
normovaných	normovaný	k2eAgInPc2d1	normovaný
vektorových	vektorový	k2eAgInPc2d1	vektorový
prostorů	prostor	k1gInPc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
příklady	příklad	k1gInPc1	příklad
normovaných	normovaný	k2eAgInPc2d1	normovaný
prostorů	prostor	k1gInPc2	prostor
lze	lze	k6eAd1	lze
přitom	přitom	k6eAd1	přitom
nalézt	nalézt	k5eAaPmF	nalézt
v	v	k7c6	v
sekci	sekce	k1gFnSc6	sekce
Prostory	prostora	k1gFnSc2	prostora
se	s	k7c7	s
skalárním	skalární	k2eAgInSc7d1	skalární
součinem	součin	k1gInSc7	součin
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
skalární	skalární	k2eAgInSc1d1	skalární
součin	součin	k1gInSc1	součin
indukuje	indukovat	k5eAaBmIp3nS	indukovat
normu	norma	k1gFnSc4	norma
a	a	k8xC	a
každý	každý	k3xTgInSc4	každý
prostor	prostor	k1gInSc4	prostor
se	s	k7c7	s
skalárním	skalární	k2eAgInSc7d1	skalární
součinem	součin	k1gInSc7	součin
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
automaticky	automaticky	k6eAd1	automaticky
i	i	k9	i
normovaným	normovaný	k2eAgInSc7d1	normovaný
vektorovým	vektorový	k2eAgInSc7d1	vektorový
prostorem	prostor	k1gInSc7	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Aritmetický	aritmetický	k2eAgInSc4d1	aritmetický
vektor	vektor	k1gInSc4	vektor
<g/>
.	.	kIx.	.
</s>
<s>
Vraťme	vrátit	k5eAaPmRp1nP	vrátit
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
k	k	k7c3	k
vektorovým	vektorový	k2eAgInPc3d1	vektorový
prostorům	prostor	k1gInPc3	prostor
aritmetických	aritmetický	k2eAgMnPc2d1	aritmetický
vektorů	vektor	k1gInPc2	vektor
a	a	k8xC	a
ukažme	ukázat	k5eAaPmRp1nP	ukázat
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
lze	lze	k6eAd1	lze
na	na	k7c6	na
těchto	tento	k3xDgInPc6	tento
prostorech	prostor	k1gInPc6	prostor
zavést	zavést	k5eAaPmF	zavést
normu	norma	k1gFnSc4	norma
<g/>
.	.	kIx.	.
</s>
<s>
Uvažujme	uvažovat	k5eAaImRp1nP	uvažovat
číselné	číselný	k2eAgNnSc4d1	číselné
těleso	těleso	k1gNnSc4	těleso
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
}	}	kIx)	}
a	a	k8xC	a
aritmetický	aritmetický	k2eAgInSc4d1	aritmetický
prostor	prostor	k1gInSc4	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
mathbb	mathbba	k1gFnPc2	mathbba
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
,	,	kIx,	,
popř.	popř.	kA	popř.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
C	C	kA	C
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
≤	≤	k?	≤
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
1	[number]	k4	1
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
pevně	pevně	k6eAd1	pevně
dané	daný	k2eAgNnSc4d1	dané
přirozené	přirozený	k2eAgNnSc4d1	přirozené
číslo	číslo	k1gNnSc4	číslo
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
výše	výše	k1gFnSc2	výše
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vektory	vektor	k1gInPc1	vektor
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
uspořádané	uspořádaný	k2eAgFnPc4d1	uspořádaná
n-tice	nice	k1gFnPc4	n-tice
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
...	...	k?	...
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
definici	definice	k1gFnSc4	definice
normy	norma	k1gFnSc2	norma
nebyl	být	k5eNaImAgInS	být
nijak	nijak	k6eAd1	nijak
specifikován	specifikovat	k5eAaBmNgInS	specifikovat
explicitní	explicitní	k2eAgInSc1d1	explicitní
tvar	tvar	k1gInSc1	tvar
tohoto	tento	k3xDgNnSc2	tento
zobrazení	zobrazení	k1gNnSc2	zobrazení
a	a	k8xC	a
možností	možnost	k1gFnPc2	možnost
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
zvolit	zvolit	k5eAaPmF	zvolit
normu	norma	k1gFnSc4	norma
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
mnoho	mnoho	k4c1	mnoho
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
však	však	k9	však
často	často	k6eAd1	často
vymezuje	vymezovat	k5eAaImIp3nS	vymezovat
jistá	jistý	k2eAgFnSc1d1	jistá
třída	třída	k1gFnSc1	třída
norem	norma	k1gFnPc2	norma
<g/>
,	,	kIx,	,
kterým	který	k3yRgFnPc3	který
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
říká	říkat	k5eAaImIp3nS	říkat
p-normy	porma	k1gFnPc4	p-norma
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
definice	definice	k1gFnSc1	definice
zní	znět	k5eAaImIp3nS	znět
takto	takto	k6eAd1	takto
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
i	i	k9	i
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
popřípadě	popřípadě	k6eAd1	popřípadě
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
max	max	kA	max
:	:	kIx,	:
1	[number]	k4	1
≤	≤	k?	≤
i	i	k8xC	i
≤	≤	k?	≤
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Big	Big	k1gFnSc1	Big
(	(	kIx(	(
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s>
i	i	k9	i
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Big	Big	k1gFnSc1	Big
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s>
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
quad	quad	k6eAd1	quad
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
popřípadě	popřípadě	k6eAd1	popřípadě
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
quad	quad	k6eAd1	quad
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s>
<g/>
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
max	max	kA	max
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
i	i	k8xC	i
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
⋅	⋅	k?	⋅
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}}	}}	k?	}}
,	,	kIx,	,
popř.	popř.	kA	popř.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
⋅	⋅	k?	⋅
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc1	infta
}}	}}	k?	}}
označuje	označovat	k5eAaImIp3nS	označovat
danou	daný	k2eAgFnSc4d1	daná
p-normu	porma	k1gFnSc4	p-norma
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
=	=	kIx~	=
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
p	p	k?	p
<g/>
=	=	kIx~	=
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
dostáváme	dostávat	k5eAaImIp1nP	dostávat
klasickou	klasický	k2eAgFnSc4d1	klasická
Euklidovu	Euklidův	k2eAgFnSc4d1	Euklidova
normu	norma	k1gFnSc4	norma
vektoru	vektor	k1gInSc2	vektor
<g/>
.	.	kIx.	.
</s>
<s>
Dokázat	dokázat	k5eAaPmF	dokázat
první	první	k4xOgFnSc4	první
a	a	k8xC	a
druhou	druhý	k4xOgFnSc4	druhý
definiční	definiční	k2eAgFnSc4d1	definiční
vlastnost	vlastnost	k1gFnSc4	vlastnost
normy	norma	k1gFnSc2	norma
pro	pro	k7c4	pro
právě	právě	k6eAd1	právě
zavedená	zavedený	k2eAgNnPc4d1	zavedené
zobrazení	zobrazení	k1gNnPc4	zobrazení
je	být	k5eAaImIp3nS	být
snadné	snadný	k2eAgNnSc1d1	snadné
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
důkaz	důkaz	k1gInSc4	důkaz
trojúhelníkové	trojúhelníkový	k2eAgFnSc2d1	trojúhelníková
nerovnosti	nerovnost	k1gFnSc2	nerovnost
lze	lze	k6eAd1	lze
pak	pak	k6eAd1	pak
použít	použít	k5eAaPmF	použít
Minkowského	Minkowského	k2eAgFnPc4d1	Minkowského
nerovnosti	nerovnost	k1gFnPc4	nerovnost
<g/>
.	.	kIx.	.
</s>
<s>
Máme	mít	k5eAaImIp1nP	mít
tak	tak	k9	tak
nyní	nyní	k6eAd1	nyní
vektorový	vektorový	k2eAgInSc1d1	vektorový
prostor	prostor	k1gInSc1	prostor
aritmetických	aritmetický	k2eAgInPc2d1	aritmetický
vektorů	vektor	k1gInPc2	vektor
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
definován	definovat	k5eAaBmNgInS	definovat
jednoparametrický	jednoparametrický	k2eAgInSc1d1	jednoparametrický
systém	systém	k1gInSc1	systém
norem	norma	k1gFnPc2	norma
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
s	s	k7c7	s
vektory	vektor	k1gInPc7	vektor
a	a	k8xC	a
pro	pro	k7c4	pro
měření	měření	k1gNnSc4	měření
jejich	jejich	k3xOp3gFnPc2	jejich
délek	délka	k1gFnPc2	délka
si	se	k3xPyFc3	se
vždy	vždy	k6eAd1	vždy
zvolíme	zvolit	k5eAaPmIp1nP	zvolit
tu	ten	k3xDgFnSc4	ten
normu	norma	k1gFnSc4	norma
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
kontextu	kontext	k1gInSc6	kontext
nejlépe	dobře	k6eAd3	dobře
pracuje	pracovat	k5eAaImIp3nS	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Matice	matice	k1gFnSc2	matice
<g/>
.	.	kIx.	.
</s>
<s>
Výše	vysoce	k6eAd2	vysoce
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
zmínili	zmínit	k5eAaPmAgMnP	zmínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
matice	matice	k1gFnPc1	matice
mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
podobnou	podobný	k2eAgFnSc4d1	podobná
strukturu	struktura	k1gFnSc4	struktura
jako	jako	k8xS	jako
aritmetické	aritmetický	k2eAgInPc4d1	aritmetický
vektory	vektor	k1gInPc4	vektor
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
na	na	k7c6	na
těchto	tento	k3xDgInPc6	tento
vektorech	vektor	k1gInPc6	vektor
bychom	by	kYmCp1nP	by
analogicky	analogicky	k6eAd1	analogicky
mohli	moct	k5eAaImAgMnP	moct
zavést	zavést	k5eAaPmF	zavést
normu	norma	k1gFnSc4	norma
i	i	k9	i
na	na	k7c6	na
maticích	matice	k1gFnPc6	matice
obecných	obecný	k2eAgInPc2d1	obecný
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
aritmetickým	aritmetický	k2eAgNnSc7d1	aritmetické
vektorů	vektor	k1gInPc2	vektor
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
složky	složka	k1gFnPc1	složka
matic	matice	k1gFnPc2	matice
rozloženy	rozložit	k5eAaPmNgInP	rozložit
do	do	k7c2	do
obdélníku	obdélník	k1gInSc2	obdélník
<g/>
,	,	kIx,	,
kterážto	kterážto	k?	kterážto
vlastnost	vlastnost	k1gFnSc1	vlastnost
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
definovat	definovat	k5eAaBmF	definovat
násobek	násobek	k1gInSc4	násobek
dvou	dva	k4xCgInPc2	dva
matic	matice	k1gFnPc2	matice
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
matice	matice	k1gFnPc1	matice
přitom	přitom	k6eAd1	přitom
musí	muset	k5eAaImIp3nP	muset
splňovat	splňovat	k5eAaImF	splňovat
jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
požadavky	požadavek	k1gInPc1	požadavek
na	na	k7c4	na
své	svůj	k3xOyFgInPc4	svůj
rozměry	rozměr	k1gInPc4	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Násobit	násobit	k5eAaImF	násobit
tedy	tedy	k9	tedy
nelze	lze	k6eNd1	lze
jakékoliv	jakýkoliv	k3yIgFnPc4	jakýkoliv
dvě	dva	k4xCgFnPc4	dva
matice	matice	k1gFnPc4	matice
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
ale	ale	k9	ale
omezíme	omezit	k5eAaPmIp1nP	omezit
jen	jen	k9	jen
na	na	k7c6	na
čtvercové	čtvercový	k2eAgFnSc6d1	čtvercová
matice	matika	k1gFnSc6	matika
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
ty	ten	k3xDgMnPc4	ten
mající	mající	k2eAgInSc1d1	mající
stejný	stejný	k2eAgInSc1d1	stejný
počet	počet	k1gInSc1	počet
řádků	řádek	k1gInPc2	řádek
a	a	k8xC	a
sloupců	sloupec	k1gInPc2	sloupec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k9	tak
toto	tento	k3xDgNnSc4	tento
omezení	omezení	k1gNnSc4	omezení
odpadá	odpadat	k5eAaImIp3nS	odpadat
<g/>
.	.	kIx.	.
</s>
<s>
Uvažujme	uvažovat	k5eAaImRp1nP	uvažovat
nyní	nyní	k6eAd1	nyní
tedy	tedy	k9	tedy
prostor	prostor	k1gInSc4	prostor
všech	všecek	k3xTgFnPc2	všecek
čtvercových	čtvercový	k2eAgFnPc2d1	čtvercová
matic	matice	k1gFnPc2	matice
řádu	řád	k1gInSc2	řád
n	n	k0	n
a	a	k8xC	a
za	za	k7c4	za
těleso	těleso	k1gNnSc4	těleso
vezměme	vzít	k5eAaPmRp1nP	vzít
množinu	množina	k1gFnSc4	množina
reálných	reálný	k2eAgNnPc2d1	reálné
či	či	k8xC	či
komplexních	komplexní	k2eAgNnPc2d1	komplexní
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
prostoru	prostor	k1gInSc6	prostor
lze	lze	k6eAd1	lze
také	také	k9	také
zavést	zavést	k5eAaPmF	zavést
normu	norma	k1gFnSc4	norma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
matic	matice	k1gFnPc2	matice
se	se	k3xPyFc4	se
ale	ale	k9	ale
kromě	kromě	k7c2	kromě
tří	tři	k4xCgInPc2	tři
požadavků	požadavek	k1gInPc2	požadavek
v	v	k7c4	v
definici	definice	k1gFnSc4	definice
normy	norma	k1gFnSc2	norma
požaduje	požadovat	k5eAaImIp3nS	požadovat
ještě	ještě	k9	ještě
jedna	jeden	k4xCgFnSc1	jeden
vlastnost	vlastnost	k1gFnSc1	vlastnost
související	související	k2eAgFnSc1d1	související
s	s	k7c7	s
násobením	násobení	k1gNnSc7	násobení
matic	matice	k1gFnPc2	matice
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
sice	sice	k8xC	sice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
∀	∀	k?	∀
:	:	kIx,	:
A	A	kA	A
:	:	kIx,	:
∈	∈	k?	∈
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
n	n	k0	n
,	,	kIx,	,
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
(	(	kIx(	(
∀	∀	k?	∀
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
∈	∈	k?	∈
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
n	n	k0	n
,	,	kIx,	,
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
(	(	kIx(	(
∥	∥	k?	∥
:	:	kIx,	:
A	A	kA	A
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
∥	∥	k?	∥
≤	≤	k?	≤
∥	∥	k?	∥
:	:	kIx,	:
A	A	kA	A
:	:	kIx,	:
∥	∥	k?	∥
∥	∥	k?	∥
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
∥	∥	k?	∥
)	)	kIx)	)
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
forall	forall	k1gMnSc1	forall
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
A	A	kA	A
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
in	in	k?	in
T	T	kA	T
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
,	,	kIx,	,
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
forall	forall	k1gMnSc1	forall
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
B	B	kA	B
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s>
\	\	kIx~	\
<g/>
in	in	k?	in
T	T	kA	T
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
,	,	kIx,	,
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
A	A	kA	A
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
B	B	kA	B
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
A	A	kA	A
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
B	B	kA	B
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
množina	množina	k1gFnSc1	množina
reálných	reálný	k2eAgFnPc2d1	reálná
či	či	k8xC	či
komplexních	komplexní	k2eAgFnPc2d1	komplexní
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Norma	Norma	k1gFnSc1	Norma
na	na	k7c6	na
prostoru	prostor	k1gInSc6	prostor
matic	matice	k1gFnPc2	matice
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
zobrazení	zobrazení	k1gNnSc1	zobrazení
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
matici	matice	k1gFnSc4	matice
přiřadí	přiřadit	k5eAaPmIp3nP	přiřadit
nezáporné	záporný	k2eNgNnSc4d1	nezáporné
číslo	číslo	k1gNnSc4	číslo
a	a	k8xC	a
splňuje	splňovat	k5eAaImIp3nS	splňovat
přitom	přitom	k6eAd1	přitom
čtyři	čtyři	k4xCgInPc4	čtyři
požadavky	požadavek	k1gInPc4	požadavek
zmíněné	zmíněný	k2eAgFnSc2d1	zmíněná
výše	výše	k1gFnSc2	výše
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
bychom	by	kYmCp1nP	by
uvažovali	uvažovat	k5eAaImAgMnP	uvažovat
prostor	prostor	k1gInSc4	prostor
matic	matice	k1gFnPc2	matice
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
obdélníkové	obdélníkový	k2eAgInPc1d1	obdélníkový
a	a	k8xC	a
které	který	k3yIgInPc1	který
tedy	tedy	k9	tedy
nelze	lze	k6eNd1	lze
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
násobit	násobit	k5eAaImF	násobit
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
požadavek	požadavek	k1gInSc1	požadavek
odpadá	odpadat	k5eAaImIp3nS	odpadat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nemá	mít	k5eNaImIp3nS	mít
smysl	smysl	k1gInSc4	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Norem	Norma	k1gFnPc2	Norma
na	na	k7c6	na
prostoru	prostor	k1gInSc6	prostor
matic	matice	k1gFnPc2	matice
existuje	existovat	k5eAaImIp3nS	existovat
nekonečně	konečně	k6eNd1	konečně
mnoho	mnoho	k4c1	mnoho
<g/>
,	,	kIx,	,
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
se	se	k3xPyFc4	se
jich	on	k3xPp3gFnPc2	on
však	však	k9	však
používá	používat	k5eAaImIp3nS	používat
jen	jen	k9	jen
pár	pár	k4xCyI	pár
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
Euklidova	Euklidův	k2eAgFnSc1d1	Euklidova
norma	norma	k1gFnSc1	norma
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc3	který
se	se	k3xPyFc4	se
též	též	k9	též
říká	říkat	k5eAaImIp3nS	říkat
Frobeniova	Frobeniův	k2eAgFnSc1d1	Frobeniova
norma	norma	k1gFnSc1	norma
či	či	k8xC	či
Hilbert-Schmidtova	Hilbert-Schmidtův	k2eAgFnSc1d1	Hilbert-Schmidtův
norma	norma	k1gFnSc1	norma
a	a	k8xC	a
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
libovolnou	libovolný	k2eAgFnSc4d1	libovolná
matici	matice	k1gFnSc4	matice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
:	:	kIx,	:
∈	∈	k?	∈
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
n	n	k0	n
,	,	kIx,	,
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
mathbb	mathbb	k1gInSc1	mathbb
{	{	kIx(	{
<g/>
A	A	kA	A
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
in	in	k?	in
T	T	kA	T
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
,	,	kIx,	,
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
definována	definovat	k5eAaBmNgFnS	definovat
následovně	následovně	k6eAd1	následovně
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
A	A	kA	A
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
i	i	k9	i
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
j	j	k?	j
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
i	i	k9	i
j	j	k?	j
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
A	A	kA	A
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s>
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
ij	ij	k?	ij
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}}}	}}}	k?	}}}
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
i	i	k9	i
j	j	k?	j
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
ij	ij	k?	ij
<g/>
}}	}}	k?	}}
jsou	být	k5eAaImIp3nP	být
prvky	prvek	k1gInPc1	prvek
matice	matice	k1gFnSc2	matice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
A	A	kA	A
<g/>
}	}	kIx)	}
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
vidno	vidno	k1gNnSc1	vidno
<g/>
,	,	kIx,	,
Euklidova	Euklidův	k2eAgFnSc1d1	Euklidova
norma	norma	k1gFnSc1	norma
na	na	k7c6	na
prostoru	prostor	k1gInSc6	prostor
matic	matice	k1gFnPc2	matice
je	být	k5eAaImIp3nS	být
přímým	přímý	k2eAgNnSc7d1	přímé
zobecněním	zobecnění	k1gNnSc7	zobecnění
Euklidovy	Euklidův	k2eAgFnSc2d1	Euklidova
normy	norma	k1gFnSc2	norma
na	na	k7c6	na
prostorech	prostor	k1gInPc6	prostor
aritmetických	aritmetický	k2eAgInPc2d1	aritmetický
vektorů	vektor	k1gInPc2	vektor
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Lineární	lineární	k2eAgInSc4d1	lineární
operátor	operátor	k1gInSc4	operátor
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xC	jak
jsme	být	k5eAaImIp1nP	být
si	se	k3xPyFc3	se
výše	vysoce	k6eAd2	vysoce
ukázali	ukázat	k5eAaPmAgMnP	ukázat
<g/>
,	,	kIx,	,
lineární	lineární	k2eAgInSc1d1	lineární
operátor	operátor	k1gInSc4	operátor
definovaný	definovaný	k2eAgInSc4d1	definovaný
na	na	k7c6	na
vektorovém	vektorový	k2eAgInSc6d1	vektorový
prostoru	prostor	k1gInSc6	prostor
dimenze	dimenze	k1gFnSc2	dimenze
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
n	n	k0	n
<g/>
}	}	kIx)	}
lze	lze	k6eAd1	lze
popsat	popsat	k5eAaPmF	popsat
pomocí	pomocí	k7c2	pomocí
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
n	n	k0	n
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
čísel	číslo	k1gNnPc2	číslo
(	(	kIx(	(
<g/>
uvažujeme	uvažovat	k5eAaImIp1nP	uvažovat
<g/>
-li	i	k?	-li
prostory	prostor	k1gInPc1	prostor
nad	nad	k7c7	nad
číselnými	číselný	k2eAgNnPc7d1	číselné
tělesy	těleso	k1gNnPc7	těleso
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
čísla	číslo	k1gNnPc1	číslo
přitom	přitom	k6eAd1	přitom
můžeme	moct	k5eAaImIp1nP	moct
uspořádat	uspořádat	k5eAaPmF	uspořádat
do	do	k7c2	do
čtvercové	čtvercový	k2eAgFnSc2d1	čtvercová
matice	matice	k1gFnSc2	matice
řádu	řád	k1gInSc2	řád
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
n	n	k0	n
<g/>
}	}	kIx)	}
,	,	kIx,	,
čímž	což	k3yRnSc7	což
dostáváme	dostávat	k5eAaImIp1nP	dostávat
matici	matice	k1gFnSc4	matice
lineárního	lineární	k2eAgInSc2d1	lineární
operátoru	operátor	k1gInSc2	operátor
<g/>
.	.	kIx.	.
</s>
<s>
Každému	každý	k3xTgInSc3	každý
lineárnímu	lineární	k2eAgInSc3d1	lineární
operátoru	operátor	k1gInSc3	operátor
tak	tak	k6eAd1	tak
přísluší	příslušet	k5eAaImIp3nS	příslušet
jistá	jistý	k2eAgFnSc1d1	jistá
matice	matice	k1gFnSc1	matice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vektorový	vektorový	k2eAgInSc4d1	vektorový
prostor	prostor	k1gInSc4	prostor
lineárních	lineární	k2eAgInPc2d1	lineární
operátorů	operátor	k1gInPc2	operátor
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
můžeme	moct	k5eAaImIp1nP	moct
dívat	dívat	k5eAaImF	dívat
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
na	na	k7c4	na
prostor	prostor	k1gInSc4	prostor
matic	matice	k1gFnPc2	matice
a	a	k8xC	a
obdobným	obdobný	k2eAgInSc7d1	obdobný
způsobem	způsob	k1gInSc7	způsob
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
zavést	zavést	k5eAaPmF	zavést
normu	norma	k1gFnSc4	norma
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
předchozí	předchozí	k2eAgFnSc2d1	předchozí
příklad	příklad	k1gInSc4	příklad
<g/>
.	.	kIx.	.
</s>
<s>
Násobení	násobení	k1gNnSc1	násobení
matic	matice	k1gFnPc2	matice
pak	pak	k6eAd1	pak
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
skládání	skládání	k1gNnSc4	skládání
lineárních	lineární	k2eAgInPc2d1	lineární
operátorů	operátor	k1gInPc2	operátor
<g/>
.	.	kIx.	.
</s>
<s>
Zaměřme	zaměřit	k5eAaPmRp1nP	zaměřit
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
na	na	k7c4	na
lineární	lineární	k2eAgInPc4d1	lineární
operátory	operátor	k1gInPc4	operátor
definované	definovaný	k2eAgInPc4d1	definovaný
na	na	k7c6	na
normovaném	normovaný	k2eAgInSc6d1	normovaný
vektorovém	vektorový	k2eAgInSc6d1	vektorový
prostoru	prostor	k1gInSc6	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	v	k7c6	v
<g/>
}	}	kIx)	}
nekonečné	konečný	k2eNgFnSc2d1	nekonečná
dimenze	dimenze	k1gFnSc2	dimenze
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
operátory	operátor	k1gInPc1	operátor
už	už	k6eAd1	už
nelze	lze	k6eNd1	lze
popsat	popsat	k5eAaPmF	popsat
maticí	matice	k1gFnSc7	matice
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
by	by	kYmCp3nS	by
totiž	totiž	k9	totiž
musela	muset	k5eAaImAgFnS	muset
mít	mít	k5eAaImF	mít
nekonečně	konečně	k6eNd1	konečně
mnoho	mnoho	k4c4	mnoho
řádků	řádek	k1gInPc2	řádek
a	a	k8xC	a
sloupců	sloupec	k1gInPc2	sloupec
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
všemi	všecek	k3xTgInPc7	všecek
operátory	operátor	k1gInPc7	operátor
však	však	k8xC	však
můžeme	moct	k5eAaImIp1nP	moct
vymezit	vymezit	k5eAaPmF	vymezit
podmnožinu	podmnožina	k1gFnSc4	podmnožina
tvořenou	tvořený	k2eAgFnSc7d1	tvořená
spojitými	spojitý	k2eAgInPc7d1	spojitý
operátory	operátor	k1gInPc7	operátor
<g/>
,	,	kIx,	,
kterým	který	k3yQgFnPc3	který
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
říká	říkat	k5eAaImIp3nS	říkat
omezené	omezený	k2eAgInPc4d1	omezený
operátory	operátor	k1gInPc4	operátor
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
omezený	omezený	k2eAgInSc4d1	omezený
operátor	operátor	k1gInSc4	operátor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
L	L	kA	L
<g/>
}	}	kIx)	}
existuje	existovat	k5eAaImIp3nS	existovat
kladné	kladný	k2eAgNnSc1d1	kladné
číslo	číslo	k1gNnSc1	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
C_	C_	k1gFnSc6	C_
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
L	L	kA	L
<g/>
}}	}}	k?	}}
takové	takový	k3xDgNnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
libovolný	libovolný	k2eAgInSc4d1	libovolný
vektor	vektor	k1gInSc4	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
V	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}	}	kIx)	}
platí	platit	k5eAaImIp3nS	platit
nerovnost	nerovnost	k1gFnSc1	nerovnost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
L	L	kA	L
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
∥	∥	k?	∥
≤	≤	k?	≤
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
:	:	kIx,	:
⋅	⋅	k?	⋅
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
L	L	kA	L
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
C_	C_	k1gMnSc1	C_
<g/>
{	{	kIx(	{
<g/>
L	L	kA	L
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Obraz	obraz	k1gInSc1	obraz
vektoru	vektor	k1gInSc2	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
při	při	k7c6	při
zobrazení	zobrazení	k1gNnSc6	zobrazení
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
L	L	kA	L
<g/>
}	}	kIx)	}
má	mít	k5eAaImIp3nS	mít
tedy	tedy	k9	tedy
normu	norma	k1gFnSc4	norma
nejvýše	nejvýše	k6eAd1	nejvýše
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
C_	C_	k1gMnSc1	C_
<g/>
{	{	kIx(	{
<g/>
L	L	kA	L
<g/>
}}	}}	k?	}}
-krát	rát	k1gInSc1	-krát
větší	veliký	k2eAgInSc1d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
norma	norma	k1gFnSc1	norma
původního	původní	k2eAgInSc2d1	původní
vektoru	vektor	k1gInSc2	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
tedy	tedy	k9	tedy
uvažujeme	uvažovat	k5eAaImIp1nP	uvažovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
operátor	operátor	k1gInSc4	operátor
definovaný	definovaný	k2eAgInSc4d1	definovaný
na	na	k7c6	na
normovaném	normovaný	k2eAgInSc6d1	normovaný
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
do	do	k7c2	do
něhož	jenž	k3xRgInSc2	jenž
spadají	spadat	k5eAaPmIp3nP	spadat
vektory	vektor	k1gInPc4	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
.	.	kIx.	.
</s>
<s>
Chceme	chtít	k5eAaImIp1nP	chtít
nyní	nyní	k6eAd1	nyní
definovat	definovat	k5eAaBmF	definovat
normu	norma	k1gFnSc4	norma
samotného	samotný	k2eAgNnSc2d1	samotné
lineárního	lineární	k2eAgNnSc2d1	lineární
zobrazení	zobrazení	k1gNnSc2	zobrazení
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
právě	právě	k6eAd1	právě
uvedené	uvedený	k2eAgFnSc2d1	uvedená
definice	definice	k1gFnSc2	definice
je	být	k5eAaImIp3nS	být
lineární	lineární	k2eAgInSc1d1	lineární
operátor	operátor	k1gInSc1	operátor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
L	L	kA	L
<g/>
}	}	kIx)	}
omezený	omezený	k2eAgInSc1d1	omezený
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
existuje	existovat	k5eAaImIp3nS	existovat
alespoň	alespoň	k9	alespoň
jedno	jeden	k4xCgNnSc1	jeden
číslo	číslo	k1gNnSc1	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
C_	C_	k1gMnSc1	C_
<g/>
{	{	kIx(	{
<g/>
L	L	kA	L
<g/>
}}	}}	k?	}}
s	s	k7c7	s
vlastností	vlastnost	k1gFnSc7	vlastnost
výše	výše	k1gFnSc2	výše
<g/>
.	.	kIx.	.
</s>
<s>
Takových	takový	k3xDgNnPc2	takový
čísel	číslo	k1gNnPc2	číslo
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
jich	on	k3xPp3gInPc2	on
být	být	k5eAaImF	být
dokonce	dokonce	k9	dokonce
nekonečně	konečně	k6eNd1	konečně
mnoho	mnoho	k4c1	mnoho
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
budeme	být	k5eAaImBp1nP	být
uvažovat	uvažovat	k5eAaImF	uvažovat
infimum	infimum	k1gInSc4	infimum
množiny	množina	k1gFnSc2	množina
všech	všecek	k3xTgFnPc2	všecek
těchto	tento	k3xDgNnPc2	tento
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
dostaneme	dostat	k5eAaPmIp1nP	dostat
opět	opět	k6eAd1	opět
číslo	číslo	k1gNnSc4	číslo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
splňuje	splňovat	k5eAaImIp3nS	splňovat
danou	daný	k2eAgFnSc4d1	daná
vlastnost	vlastnost	k1gFnSc4	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
číslo	číslo	k1gNnSc4	číslo
nazýváme	nazývat	k5eAaImIp1nP	nazývat
norma	norma	k1gFnSc1	norma
omezeného	omezený	k2eAgInSc2d1	omezený
lineárního	lineární	k2eAgInSc2d1	lineární
operátoru	operátor	k1gInSc2	operátor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
L	L	kA	L
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
symbolech	symbol	k1gInPc6	symbol
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
L	L	kA	L
∥	∥	k?	∥
=	=	kIx~	=
inf	inf	k?	inf
{	{	kIx(	{
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
:	:	kIx,	:
}	}	kIx)	}
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
L	L	kA	L
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
inf	inf	k?	inf
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
C_	C_	k1gMnSc1	C_
<g/>
{	{	kIx(	{
<g/>
L	L	kA	L
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Ačkoli	ačkoli	k8xS	ačkoli
jsme	být	k5eAaImIp1nP	být
právě	právě	k6eAd1	právě
zavedli	zavést	k5eAaPmAgMnP	zavést
objekt	objekt	k1gInSc4	objekt
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
jsme	být	k5eAaImIp1nP	být
pojmenovali	pojmenovat	k5eAaPmAgMnP	pojmenovat
norma	norma	k1gFnSc1	norma
<g/>
,	,	kIx,	,
neověřili	ověřit	k5eNaPmAgMnP	ověřit
jsme	být	k5eAaImIp1nP	být
dosud	dosud	k6eAd1	dosud
<g/>
,	,	kIx,	,
že	že	k8xS	že
skutečně	skutečně	k6eAd1	skutečně
splňuje	splňovat	k5eAaImIp3nS	splňovat
definiční	definiční	k2eAgFnPc4d1	definiční
podmínky	podmínka	k1gFnPc4	podmínka
normy	norma	k1gFnSc2	norma
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
potřeba	potřeba	k6eAd1	potřeba
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
zavedené	zavedený	k2eAgNnSc4d1	zavedené
zobrazení	zobrazení	k1gNnSc4	zobrazení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
omezenému	omezený	k2eAgInSc3d1	omezený
lineárnímu	lineární	k2eAgInSc3d1	lineární
operátoru	operátor	k1gInSc3	operátor
přiřazuje	přiřazovat	k5eAaImIp3nS	přiřazovat
infimum	infimum	k1gNnSc1	infimum
jisté	jistý	k2eAgFnSc2d1	jistá
množiny	množina	k1gFnSc2	množina
<g/>
,	,	kIx,	,
skutečně	skutečně	k6eAd1	skutečně
normou	norma	k1gFnSc7	norma
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
těžké	těžký	k2eAgNnSc1d1	těžké
nahlédnout	nahlédnout	k5eAaPmF	nahlédnout
<g/>
,	,	kIx,	,
vyjdeme	vyjít	k5eAaPmIp1nP	vyjít
<g/>
-li	i	k?	-li
z	z	k7c2	z
vlastností	vlastnost	k1gFnPc2	vlastnost
normy	norma	k1gFnSc2	norma
na	na	k7c6	na
vektorovém	vektorový	k2eAgInSc6d1	vektorový
prostoru	prostor	k1gInSc6	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	V	kA	V
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
rovnou	rovnou	k6eAd1	rovnou
splněn	splnit	k5eAaPmNgInS	splnit
i	i	k9	i
vztah	vztah	k1gInSc1	vztah
analogický	analogický	k2eAgInSc1d1	analogický
čtvrtému	čtvrtý	k4xOgInSc3	čtvrtý
požadavku	požadavek	k1gInSc2	požadavek
na	na	k7c4	na
normu	norma	k1gFnSc4	norma
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
matic	matice	k1gFnPc2	matice
<g/>
.	.	kIx.	.
</s>
<s>
Sice	sice	k8xC	sice
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
(	(	kIx(	(
A	A	kA	A
B	B	kA	B
)	)	kIx)	)
∥	∥	k?	∥
≤	≤	k?	≤
∥	∥	k?	∥
A	a	k8xC	a
∥	∥	k?	∥
⋅	⋅	k?	⋅
∥	∥	k?	∥
B	B	kA	B
∥	∥	k?	∥
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
(	(	kIx(	(
<g/>
AB	AB	kA	AB
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
A	a	k9	a
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
B	B	kA	B
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	A	kA	A
,	,	kIx,	,
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
A	A	kA	A
<g/>
,	,	kIx,	,
<g/>
B	B	kA	B
<g/>
}	}	kIx)	}
jsou	být	k5eAaImIp3nP	být
omezené	omezený	k2eAgInPc1d1	omezený
lineární	lineární	k2eAgInPc1d1	lineární
operátory	operátor	k1gInPc1	operátor
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
AB	AB	kA	AB
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gNnSc4	jejich
složení	složení	k1gNnSc4	složení
<g/>
.	.	kIx.	.
</s>
<s>
Máme	mít	k5eAaImIp1nP	mít
<g/>
-li	i	k?	-li
již	již	k6eAd1	již
dokázané	dokázaný	k2eAgFnPc1d1	dokázaná
vlastnosti	vlastnost	k1gFnPc1	vlastnost
normy	norma	k1gFnSc2	norma
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
právě	právě	k6eAd1	právě
uvedenou	uvedený	k2eAgFnSc4d1	uvedená
nerovnost	nerovnost	k1gFnSc4	nerovnost
odvodit	odvodit	k5eAaPmF	odvodit
následovně	následovně	k6eAd1	následovně
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
(	(	kIx(	(
A	A	kA	A
B	B	kA	B
)	)	kIx)	)
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
∥	∥	k?	∥
=	=	kIx~	=
∥	∥	k?	∥
A	A	kA	A
(	(	kIx(	(
B	B	kA	B
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
)	)	kIx)	)
∥	∥	k?	∥
<g />
.	.	kIx.	.
</s>
<s hack="1">
≤	≤	k?	≤
∥	∥	k?	∥
A	a	k8xC	a
∥	∥	k?	∥
⋅	⋅	k?	⋅
∥	∥	k?	∥
B	B	kA	B
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
∥	∥	k?	∥
≤	≤	k?	≤
∥	∥	k?	∥
A	a	k8xC	a
∥	∥	k?	∥
⋅	⋅	k?	⋅
∥	∥	k?	∥
B	B	kA	B
∥	∥	k?	∥
⋅	⋅	k?	⋅
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
(	(	kIx(	(
<g/>
AB	AB	kA	AB
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
A	A	kA	A
<g/>
(	(	kIx(	(
<g/>
B	B	kA	B
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
))	))	k?	))
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
A	a	k9	a
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
B	B	kA	B
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
A	a	k9	a
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
B	B	kA	B
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
|	|	kIx~	|
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
kde	kde	k6eAd1	kde
jsme	být	k5eAaImIp1nP	být
v	v	k7c6	v
první	první	k4xOgFnSc6	první
rovnosti	rovnost	k1gFnSc6	rovnost
využili	využít	k5eAaPmAgMnP	využít
vlastností	vlastnost	k1gFnPc2	vlastnost
skládání	skládání	k1gNnSc4	skládání
zobrazení	zobrazení	k1gNnSc2	zobrazení
a	a	k8xC	a
pak	pak	k6eAd1	pak
po	po	k7c6	po
řadě	řada	k1gFnSc6	řada
definice	definice	k1gFnSc2	definice
normy	norma	k1gFnSc2	norma
pro	pro	k7c4	pro
operátor	operátor	k1gInSc4	operátor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
A	a	k9	a
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
B	B	kA	B
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
vidíme	vidět	k5eAaImIp1nP	vidět
<g/>
,	,	kIx,	,
číslo	číslo	k1gNnSc1	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
A	a	k8xC	a
∥	∥	k?	∥
⋅	⋅	k?	⋅
∥	∥	k?	∥
B	B	kA	B
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
A	a	k9	a
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
B	B	kA	B
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
je	být	k5eAaImIp3nS	být
příkladem	příklad	k1gInSc7	příklad
čísla	číslo	k1gNnSc2	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
C_	C_	k1gMnSc1	C_
<g/>
{	{	kIx(	{
<g/>
L	L	kA	L
<g/>
}}	}}	k?	}}
z	z	k7c2	z
definice	definice	k1gFnSc2	definice
výše	výše	k1gFnSc2	výše
<g/>
,	,	kIx,	,
když	když	k8xS	když
položíme	položit	k5eAaPmIp1nP	položit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
=	=	kIx~	=
A	A	kA	A
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
L	L	kA	L
<g/>
=	=	kIx~	=
<g/>
AB	AB	kA	AB
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
norma	norma	k1gFnSc1	norma
operátoru	operátor	k1gInSc2	operátor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
AB	AB	kA	AB
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
definice	definice	k1gFnSc2	definice
infimum	infimum	k1gInSc1	infimum
všech	všecek	k3xTgNnPc2	všecek
čísel	číslo	k1gNnPc2	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
C_	C_	k1gMnSc6	C_
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
L	L	kA	L
<g/>
}}	}}	k?	}}
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutně	nutně	k6eAd1	nutně
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
A	a	k8xC	a
B	B	kA	B
∥	∥	k?	∥
≤	≤	k?	≤
∥	∥	k?	∥
A	a	k8xC	a
∥	∥	k?	∥
⋅	⋅	k?	⋅
∥	∥	k?	∥
B	B	kA	B
∥	∥	k?	∥
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
AB	AB	kA	AB
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
A	a	k9	a
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
B	B	kA	B
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
.	.	kIx.	.
</s>
<s>
Vektorový	vektorový	k2eAgInSc4d1	vektorový
prostor	prostor	k1gInSc4	prostor
omezených	omezený	k2eAgInPc2d1	omezený
lineárních	lineární	k2eAgInPc2d1	lineární
operátorů	operátor	k1gInPc2	operátor
definovaných	definovaný	k2eAgInPc2d1	definovaný
na	na	k7c6	na
nekonečněrozměrném	konečněrozměrný	k2eNgInSc6d1	konečněrozměrný
normovaném	normovaný	k2eAgInSc6d1	normovaný
vektorovém	vektorový	k2eAgInSc6d1	vektorový
prostoru	prostor	k1gInSc6	prostor
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
také	také	k9	také
nekonečněrozměrný	konečněrozměrný	k2eNgInSc1d1	konečněrozměrný
normovaný	normovaný	k2eAgInSc1d1	normovaný
vektorový	vektorový	k2eAgInSc1d1	vektorový
prostor	prostor	k1gInSc1	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
lp	lp	k?	lp
prostor	prostor	k1gInSc1	prostor
(	(	kIx(	(
<g/>
posloupnosti	posloupnost	k1gFnPc1	posloupnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výše	vysoce	k6eAd2	vysoce
jsme	být	k5eAaImIp1nP	být
se	se	k3xPyFc4	se
zmínili	zmínit	k5eAaPmAgMnP	zmínit
o	o	k7c6	o
prostoru	prostor	k1gInSc6	prostor
konvergentních	konvergentní	k2eAgFnPc2d1	konvergentní
posloupností	posloupnost	k1gFnPc2	posloupnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
množině	množina	k1gFnSc6	množina
můžeme	moct	k5eAaImIp1nP	moct
dále	daleko	k6eAd2	daleko
rozlišovat	rozlišovat	k5eAaImF	rozlišovat
podmnožiny	podmnožina	k1gFnPc4	podmnožina
číselných	číselný	k2eAgFnPc2d1	číselná
posloupností	posloupnost	k1gFnPc2	posloupnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
konvergují	konvergovat	k5eAaImIp3nP	konvergovat
"	"	kIx"	"
<g/>
různě	různě	k6eAd1	různě
rychle	rychle	k6eAd1	rychle
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Matematicky	matematicky	k6eAd1	matematicky
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
fakt	fakt	k1gInSc1	fakt
zachycen	zachytit	k5eAaPmNgInS	zachytit
v	v	k7c4	v
definici	definice	k1gFnSc4	definice
lp	lp	k?	lp
prostorů	prostor	k1gInPc2	prostor
(	(	kIx(	(
<g/>
l	l	kA	l
zde	zde	k6eAd1	zde
označuje	označovat	k5eAaImIp3nS	označovat
malé	malý	k2eAgNnSc1d1	malé
písmeno	písmeno	k1gNnSc1	písmeno
L	L	kA	L
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Definice	definice	k1gFnSc1	definice
těchto	tento	k3xDgInPc2	tento
prostorů	prostor	k1gInPc2	prostor
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
přímočaře	přímočaro	k6eAd1	přímočaro
zavést	zavést	k5eAaPmF	zavést
normu	norma	k1gFnSc4	norma
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
příkladů	příklad	k1gInPc2	příklad
normovaných	normovaný	k2eAgInPc2d1	normovaný
vektorových	vektorový	k2eAgInPc2d1	vektorový
prostorů	prostor	k1gInPc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Norma	Norma	k1gFnSc1	Norma
je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
zavedena	zavést	k5eAaPmNgFnS	zavést
v	v	k7c6	v
analogii	analogie	k1gFnSc6	analogie
na	na	k7c4	na
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
p	p	k?	p
<g/>
}	}	kIx)	}
-normy	orma	k1gFnSc2	-norma
aritmetických	aritmetický	k2eAgInPc2d1	aritmetický
vektorů	vektor	k1gInPc2	vektor
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
naznačeno	naznačit	k5eAaPmNgNnS	naznačit
v	v	k7c6	v
předchozím	předchozí	k2eAgInSc6d1	předchozí
příkladě	příklad	k1gInSc6	příklad
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
dané	daný	k2eAgNnSc4d1	dané
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
≥	≥	k?	≥
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
p	p	k?	p
<g/>
\	\	kIx~	\
<g/>
geq	geq	k?	geq
1	[number]	k4	1
<g/>
}	}	kIx)	}
definuje	definovat	k5eAaBmIp3nS	definovat
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
l	l	kA	l
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
<	<	kIx(	<
+	+	kIx~	+
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
l	l	kA	l
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Big	Big	k1gFnSc1	Big
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
<g />
.	.	kIx.	.
</s>
<s>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
C	C	kA	C
<g/>
}	}	kIx)	}
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Big	Big	k1gFnSc1	Big
|	|	kIx~	|
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
infty	inft	k1gInPc4	inft
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
<g/>
<	<	kIx(	<
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Big	Big	k1gFnSc1	Big
\	\	kIx~	\
<g/>
}}	}}	k?	}}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
dodefinovává	dodefinovávat	k5eAaImIp3nS	dodefinovávat
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
l	l	kA	l
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
sup	sup	k1gMnSc1	sup
:	:	kIx,	:
1	[number]	k4	1
≤	≤	k?	≤
n	n	k0	n
<	<	kIx(	<
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
<	<	kIx(	<
+	+	kIx~	+
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
l	l	kA	l
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Big	Big	k1gFnSc1	Big
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s>
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
C	C	kA	C
<g/>
}	}	kIx)	}
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Big	Big	k1gFnSc1	Big
|	|	kIx~	|
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
sup	sup	k1gMnSc1	sup
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
n	n	k0	n
<g/>
<	<	kIx(	<
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnSc2	infta
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
<	<	kIx(	<
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Big	Big	k1gFnSc1	Big
\	\	kIx~	\
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
sup	sup	k1gMnSc1	sup
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
sup	supit	k5eAaImRp2nS	supit
}	}	kIx)	}
označuje	označovat	k5eAaImIp3nS	označovat
supremum	supremum	k1gInSc1	supremum
všech	všecek	k3xTgInPc2	všecek
prvků	prvek	k1gInPc2	prvek
posloupnosti	posloupnost	k1gFnSc2	posloupnost
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
těchto	tento	k3xDgFnPc6	tento
množinách	množina	k1gFnPc6	množina
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
díky	díky	k7c3	díky
Minkowského	Minkowského	k2eAgFnSc3d1	Minkowského
nerovnosti	nerovnost	k1gFnSc3	nerovnost
ukázat	ukázat	k5eAaPmF	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
uzavřené	uzavřený	k2eAgFnPc1d1	uzavřená
na	na	k7c4	na
součet	součet	k1gInSc4	součet
dvou	dva	k4xCgFnPc2	dva
svých	svůj	k3xOyFgFnPc2	svůj
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Součtem	součet	k1gInSc7	součet
dvou	dva	k4xCgFnPc2	dva
řad	řada	k1gFnPc2	řada
z	z	k7c2	z
dané	daný	k2eAgFnSc2d1	daná
množiny	množina	k1gFnSc2	množina
dostaneme	dostat	k5eAaPmIp1nP	dostat
opět	opět	k6eAd1	opět
prvek	prvek	k1gInSc4	prvek
dané	daný	k2eAgFnSc2d1	daná
množiny	množina	k1gFnSc2	množina
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
smysl	smysl	k1gInSc4	smysl
hovořit	hovořit	k5eAaImF	hovořit
o	o	k7c4	o
zavedení	zavedení	k1gNnSc4	zavedení
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Ověření	ověření	k1gNnSc1	ověření
axiomů	axiom	k1gInPc2	axiom
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
již	již	k6eAd1	již
rutinní	rutinní	k2eAgFnSc4d1	rutinní
záležitost	záležitost	k1gFnSc4	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Normu	Norma	k1gFnSc4	Norma
lze	lze	k6eAd1	lze
v	v	k7c6	v
takovýchto	takovýto	k3xDgInPc6	takovýto
prostorech	prostor	k1gInPc6	prostor
definovat	definovat	k5eAaBmF	definovat
jako	jako	k8xS	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
(	(	kIx(	(
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
popřípadě	popřípadě	k6eAd1	popřípadě
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
(	(	kIx(	(
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
sup	sup	k1gMnSc1	sup
:	:	kIx,	:
1	[number]	k4	1
≤	≤	k?	≤
n	n	k0	n
<	<	kIx(	<
<g />
.	.	kIx.	.
</s>
<s hack="1">
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
(	(	kIx(	(
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Big	Big	k1gFnSc1	Big
(	(	kIx(	(
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Big	Big	k1gFnSc1	Big
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc4	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
quad	quad	k6eAd1	quad
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
popřípadě	popřípadě	k6eAd1	popřípadě
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
quad	quad	k6eAd1	quad
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
(	(	kIx(	(
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sup	sup	k1gMnSc1	sup
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
n	n	k0	n
<g/>
<	<	kIx(	<
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
K	k	k7c3	k
ověření	ověření	k1gNnSc3	ověření
<g/>
,	,	kIx,	,
že	že	k8xS	že
takto	takto	k6eAd1	takto
definované	definovaný	k2eAgNnSc1d1	definované
zobrazení	zobrazení	k1gNnSc1	zobrazení
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
číselné	číselný	k2eAgFnPc1d1	číselná
posloupnosti	posloupnost	k1gFnPc1	posloupnost
přiřadí	přiřadit	k5eAaPmIp3nP	přiřadit
nezáporné	záporný	k2eNgNnSc4d1	nezáporné
číslo	číslo	k1gNnSc4	číslo
<g/>
,	,	kIx,	,
splňuje	splňovat	k5eAaImIp3nS	splňovat
trojúhelníkovou	trojúhelníkový	k2eAgFnSc4d1	trojúhelníková
nerovnost	nerovnost	k1gFnSc4	nerovnost
přitom	přitom	k6eAd1	přitom
opět	opět	k6eAd1	opět
můžeme	moct	k5eAaImIp1nP	moct
s	s	k7c7	s
úspěchem	úspěch	k1gInSc7	úspěch
použít	použít	k5eAaPmF	použít
Minkowského	Minkowského	k2eAgFnPc4d1	Minkowského
nerovnosti	nerovnost	k1gFnPc4	nerovnost
<g/>
.	.	kIx.	.
</s>
<s>
Dostali	dostat	k5eAaPmAgMnP	dostat
jsme	být	k5eAaImIp1nP	být
tak	tak	k6eAd1	tak
rovnou	rovnou	k6eAd1	rovnou
jednoparametrický	jednoparametrický	k2eAgInSc1d1	jednoparametrický
systém	systém	k1gInSc1	systém
normovaných	normovaný	k2eAgInPc2d1	normovaný
prostorů	prostor	k1gInPc2	prostor
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
hodnotu	hodnota	k1gFnSc4	hodnota
parametru	parametr	k1gInSc2	parametr
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
∈	∈	k?	∈
⟨	⟨	k?	⟨
1	[number]	k4	1
,	,	kIx,	,
∞	∞	k?	∞
⟩	⟩	k?	⟩
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
p	p	k?	p
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
\	\	kIx~	\
<g/>
langle	langle	k1gInSc1	langle
1	[number]	k4	1
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc4	infta
\	\	kIx~	\
<g/>
rangle	rangle	k6eAd1	rangle
}	}	kIx)	}
máme	mít	k5eAaImIp1nP	mít
daný	daný	k2eAgInSc4d1	daný
lp	lp	k?	lp
prostor	prostor	k1gInSc4	prostor
s	s	k7c7	s
danou	daný	k2eAgFnSc7d1	daná
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
p	p	k?	p
<g/>
}	}	kIx)	}
normou	norma	k1gFnSc7	norma
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Lp	Lp	k1gFnSc2	Lp
prostor	prostora	k1gFnPc2	prostora
<g/>
.	.	kIx.	.
</s>
<s>
Analogii	analogie	k1gFnSc4	analogie
<g/>
,	,	kIx,	,
či	či	k8xC	či
spíše	spíše	k9	spíše
zobecnění	zobecnění	k1gNnSc1	zobecnění
<g/>
,	,	kIx,	,
lp	lp	k?	lp
prostorů	prostor	k1gInPc2	prostor
představují	představovat	k5eAaImIp3nP	představovat
Lp	Lp	k1gMnPc3	Lp
prostory	prostor	k1gInPc1	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
oproti	oproti	k7c3	oproti
předchozímu	předchozí	k2eAgInSc3d1	předchozí
příkladu	příklad	k1gInSc3	příklad
neuvažují	uvažovat	k5eNaImIp3nP	uvažovat
číselné	číselný	k2eAgFnPc4d1	číselná
posloupnosti	posloupnost	k1gFnPc4	posloupnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
měřitelné	měřitelný	k2eAgFnPc4d1	měřitelná
funkce	funkce	k1gFnPc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
definičních	definiční	k2eAgFnPc6d1	definiční
podmínkách	podmínka	k1gFnPc6	podmínka
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
místo	místo	k7c2	místo
sum	suma	k1gFnPc2	suma
objevují	objevovat	k5eAaImIp3nP	objevovat
integrály	integrál	k1gInPc1	integrál
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
nechť	nechť	k9	nechť
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
X	X	kA	X
,	,	kIx,	,
Σ	Σ	k?	Σ
,	,	kIx,	,
μ	μ	k?	μ
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
Sigma	sigma	k1gNnSc2	sigma
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
)	)	kIx)	)
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
prostor	prostor	k1gInSc4	prostor
s	s	k7c7	s
mírou	míra	k1gFnSc7	míra
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
≥	≥	k?	≥
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
p	p	k?	p
<g/>
\	\	kIx~	\
<g/>
geq	geq	k?	geq
1	[number]	k4	1
<g/>
}	}	kIx)	}
definujeme	definovat	k5eAaBmIp1nP	definovat
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
μ	μ	k?	μ
<	<	kIx(	<
+	+	kIx~	+
∞	∞	k?	∞
,	,	kIx,	,
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
měřitelná	měřitelný	k2eAgFnSc1d1	měřitelná
funkce	funkce	k1gFnSc1	funkce
na	na	k7c4	na
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L	L	kA	L
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Big	Big	k1gFnSc1	Big
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
f	f	k?	f
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Big	Big	k1gFnSc1	Big
|	|	kIx~	|
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
int	int	k?	int
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
|	|	kIx~	|
<g/>
f	f	k?	f
<g/>
|	|	kIx~	|
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
<	<	kIx(	<
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc4	infta
,	,	kIx,	,
<g/>
\	\	kIx~	\
f	f	k?	f
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
je	být	k5eAaImIp3nS	být
měřitelná	měřitelný	k2eAgFnSc1d1	měřitelná
<g />
.	.	kIx.	.
</s>
<s hack="1">
funkce	funkce	k1gFnSc1	funkce
na	na	k7c4	na
}}	}}	k?	}}
<g/>
X	X	kA	X
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Big	Big	k1gFnSc1	Big
\	\	kIx~	\
<g/>
}}	}}	k?	}}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
dodefinovává	dodefinovávat	k5eAaImIp3nS	dodefinovávat
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
L	L	kA	L
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
esssup	esssup	k1gInSc1	esssup
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
x	x	k?	x
∈	∈	k?	∈
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
<	<	kIx(	<
+	+	kIx~	+
∞	∞	k?	∞
,	,	kIx,	,
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
je	být	k5eAaImIp3nS	být
měřitelná	měřitelný	k2eAgFnSc1d1	měřitelná
funkce	funkce	k1gFnSc1	funkce
na	na	k7c4	na
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L	L	kA	L
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Big	Big	k1gFnSc1	Big
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
f	f	k?	f
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Big	Big	k1gFnSc1	Big
|	|	kIx~	|
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
textrm	textrm	k1gInSc1	textrm
{	{	kIx(	{
<g/>
esssup	esssup	k1gInSc1	esssup
<g/>
<g />
.	.	kIx.	.
</s>
<s>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
X	X	kA	X
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
|	|	kIx~	|
<g/>
<	<	kIx(	<
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc4	infta
,	,	kIx,	,
<g/>
\	\	kIx~	\
f	f	k?	f
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
je	být	k5eAaImIp3nS	být
měřitelná	měřitelný	k2eAgFnSc1d1	měřitelná
funkce	funkce	k1gFnSc1	funkce
na	na	k7c4	na
}}	}}	k?	}}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
X	X	kA	X
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Big	Big	k1gFnSc1	Big
\	\	kIx~	\
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
esssup	esssup	k1gInSc1	esssup
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
textrm	textrm	k1gInSc1	textrm
{	{	kIx(	{
<g/>
esssup	esssup	k1gInSc1	esssup
<g/>
}}}	}}}	k?	}}}
označuje	označovat	k5eAaImIp3nS	označovat
esenciální	esenciální	k2eAgNnSc1d1	esenciální
supremum	supremum	k1gNnSc1	supremum
funkce	funkce	k1gFnSc2	funkce
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
f	f	k?	f
<g/>
}	}	kIx)	}
na	na	k7c6	na
množině	množina	k1gFnSc6	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
X	X	kA	X
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Analogicky	analogicky	k6eAd1	analogicky
jako	jako	k9	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
lp	lp	k?	lp
prostorů	prostor	k1gInPc2	prostor
bychom	by	kYmCp1nP	by
pomocí	pomocí	k7c2	pomocí
integrální	integrální	k2eAgFnSc2d1	integrální
podoby	podoba	k1gFnSc2	podoba
Minkowského	Minkowského	k2eAgFnSc2d1	Minkowského
nerovnosti	nerovnost	k1gFnSc2	nerovnost
ověřili	ověřit	k5eAaPmAgMnP	ověřit
uzavřenost	uzavřenost	k1gFnSc4	uzavřenost
daných	daný	k2eAgFnPc2d1	daná
množin	množina	k1gFnPc2	množina
na	na	k7c4	na
součet	součet	k1gInSc4	součet
dvou	dva	k4xCgFnPc2	dva
funkcí	funkce	k1gFnPc2	funkce
a	a	k8xC	a
platnost	platnost	k1gFnSc1	platnost
axiomů	axiom	k1gInPc2	axiom
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
uvažujeme	uvažovat	k5eAaImIp1nP	uvažovat
klasické	klasický	k2eAgNnSc4d1	klasické
sčítání	sčítání	k1gNnSc4	sčítání
funkcí	funkce	k1gFnPc2	funkce
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
násobení	násobení	k1gNnSc1	násobení
číslem	číslo	k1gNnSc7	číslo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Definice	definice	k1gFnSc1	definice
daných	daný	k2eAgFnPc2d1	daná
množin	množina	k1gFnPc2	množina
nás	my	k3xPp1nPc4	my
opět	opět	k6eAd1	opět
přímočaře	přímočaro	k6eAd1	přímočaro
vede	vést	k5eAaImIp3nS	vést
na	na	k7c4	na
definici	definice	k1gFnSc4	definice
normy	norma	k1gFnSc2	norma
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
f	f	k?	f
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
μ	μ	k?	μ
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
popřípadě	popřípadě	k6eAd1	popřípadě
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
f	f	k?	f
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
esssup	esssup	k1gInSc1	esssup
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
∈	∈	k?	∈
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
f	f	k?	f
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
f	f	k?	f
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Big	Big	k1gFnSc1	Big
(	(	kIx(	(
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
int	int	k?	int
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
f	f	k?	f
<g/>
|	|	kIx~	|
<g />
.	.	kIx.	.
</s>
<s>
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Big	Big	k1gFnSc1	Big
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
p	p	k?	p
<g/>
<g />
.	.	kIx.	.
</s>
<s>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
quad	quad	k6eAd1	quad
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
popřípadě	popřípadě	k6eAd1	popřípadě
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
quad	quad	k6eAd1	quad
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
f	f	k?	f
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
textrm	textrm	k1gInSc1	textrm
{	{	kIx(	{
<g/>
esssup	esssup	k1gInSc1	esssup
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
X	X	kA	X
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
|	|	kIx~	|
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Při	při	k7c6	při
ověřování	ověřování	k1gNnSc6	ověřování
trojúhelníkové	trojúhelníkový	k2eAgFnSc2d1	trojúhelníková
nerovnosti	nerovnost	k1gFnSc2	nerovnost
bychom	by	kYmCp1nP	by
přitom	přitom	k6eAd1	přitom
opět	opět	k6eAd1	opět
využili	využít	k5eAaPmAgMnP	využít
integrální	integrální	k2eAgFnPc4d1	integrální
podoby	podoba	k1gFnPc4	podoba
Minkowského	Minkowského	k2eAgFnSc2d1	Minkowského
nerovnosti	nerovnost	k1gFnSc2	nerovnost
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
∈	∈	k?	∈
⟨	⟨	k?	⟨
1	[number]	k4	1
,	,	kIx,	,
∞	∞	k?	∞
⟩	⟩	k?	⟩
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
p	p	k?	p
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
\	\	kIx~	\
<g/>
langle	langle	k1gInSc1	langle
1	[number]	k4	1
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnPc4	infta
\	\	kIx~	\
<g/>
rangle	rangle	k6eAd1	rangle
}	}	kIx)	}
tak	tak	k6eAd1	tak
máme	mít	k5eAaImIp1nP	mít
daný	daný	k2eAgInSc1d1	daný
Lp	Lp	k1gFnSc7	Lp
prostor	prostora	k1gFnPc2	prostora
s	s	k7c7	s
danou	daný	k2eAgFnSc7d1	daná
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
p	p	k?	p
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
p	p	k?	p
<g/>
}	}	kIx)	}
normou	norma	k1gFnSc7	norma
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Prostor	prostora	k1gFnPc2	prostora
se	s	k7c7	s
skalárním	skalární	k2eAgInSc7d1	skalární
součinem	součin	k1gInSc7	součin
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
příkladem	příklad	k1gInSc7	příklad
zobrazení	zobrazení	k1gNnSc2	zobrazení
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
můžeme	moct	k5eAaImIp1nP	moct
dodatečně	dodatečně	k6eAd1	dodatečně
vybavit	vybavit	k5eAaPmF	vybavit
vektorový	vektorový	k2eAgInSc4d1	vektorový
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
skalární	skalární	k2eAgInSc1d1	skalární
součin	součin	k1gInSc1	součin
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
zobrazení	zobrazení	k1gNnSc1	zobrazení
nám	my	k3xPp1nPc3	my
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
například	například	k6eAd1	například
zavést	zavést	k5eAaPmF	zavést
úhly	úhel	k1gInPc1	úhel
mezi	mezi	k7c4	mezi
vektory	vektor	k1gInPc4	vektor
či	či	k8xC	či
ortogonalitu	ortogonalita	k1gFnSc4	ortogonalita
<g/>
.	.	kIx.	.
</s>
<s>
Shrňme	shrnout	k5eAaPmRp1nP	shrnout
si	se	k3xPyFc3	se
v	v	k7c6	v
krátkosti	krátkost	k1gFnSc6	krátkost
definici	definice	k1gFnSc4	definice
skalárního	skalární	k2eAgInSc2d1	skalární
součinu	součin	k1gInSc2	součin
na	na	k7c6	na
vektorovém	vektorový	k2eAgInSc6d1	vektorový
prostoru	prostor	k1gInSc6	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	v	k7c6	v
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
nad	nad	k7c7	nad
číselným	číselný	k2eAgNnSc7d1	číselné
tělesem	těleso	k1gNnSc7	těleso
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}	}	kIx)	}
)	)	kIx)	)
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
Skalární	skalární	k2eAgInSc1d1	skalární
součin	součin	k1gInSc1	součin
<g/>
,	,	kIx,	,
značíme	značit	k5eAaImIp1nP	značit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
⋅	⋅	k?	⋅
,	,	kIx,	,
⋅	⋅	k?	⋅
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zobrazení	zobrazení	k1gNnSc1	zobrazení
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
(	(	kIx(	(
⋅	⋅	k?	⋅
,	,	kIx,	,
⋅	⋅	k?	⋅
)	)	kIx)	)
:	:	kIx,	:
V	V	kA	V
×	×	k?	×
V	v	k7c6	v
→	→	k?	→
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
)	)	kIx)	)
<g/>
:	:	kIx,	:
<g/>
V	V	kA	V
<g/>
\	\	kIx~	\
<g/>
times	times	k1gMnSc1	times
V	v	k7c6	v
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc4	ten
T	T	kA	T
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
,	,	kIx,	,
které	který	k3yIgFnSc6	který
každé	každý	k3xTgFnSc6	každý
dvojici	dvojice	k1gFnSc6	dvojice
vektorů	vektor	k1gInPc2	vektor
přiřadí	přiřadit	k5eAaPmIp3nS	přiřadit
(	(	kIx(	(
<g/>
obecně	obecně	k6eAd1	obecně
komplexní	komplexní	k2eAgNnSc1d1	komplexní
<g/>
)	)	kIx)	)
číslo	číslo	k1gNnSc1	číslo
a	a	k8xC	a
splňuje	splňovat	k5eAaImIp3nS	splňovat
následující	následující	k2eAgFnPc4d1	následující
čtyři	čtyři	k4xCgFnPc4	čtyři
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
∀	∀	k?	∀
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
V	V	kA	V
)	)	kIx)	)
(	(	kIx(	(
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
≥	≥	k?	≥
0	[number]	k4	0
)	)	kIx)	)
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
forall	forall	k1gInSc1	forall
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
V	V	kA	V
<g/>
)	)	kIx)	)
<g/>
((	((	k?	((
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
geq	geq	k?	geq
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
∀	∀	k?	∀
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
V	V	kA	V
)	)	kIx)	)
(	(	kIx(	(
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
0	[number]	k4	0
⇔	⇔	k?	⇔
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
forall	forall	k1gInSc1	forall
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
V	V	kA	V
<g/>
)	)	kIx)	)
<g/>
((	((	k?	((
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
Leftrightarrow	Leftrightarrow	k1gFnSc1	Leftrightarrow
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
∀	∀	k?	∀
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
V	V	kA	V
)	)	kIx)	)
(	(	kIx(	(
∀	∀	k?	∀
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
V	V	kA	V
)	)	kIx)	)
(	(	kIx(	(
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
̄	̄	k?	̄
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
forall	forall	k1gInSc1	forall
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
V	V	kA	V
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
forall	forall	k1gInSc1	forall
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
V	V	kA	V
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
((	((	k?	((
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overline	overlin	k1gInSc5	overlin
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
∀	∀	k?	∀
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
V	V	kA	V
)	)	kIx)	)
(	(	kIx(	(
∀	∀	k?	∀
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
<g />
.	.	kIx.	.
</s>
<s hack="1">
V	v	k7c4	v
)	)	kIx)	)
(	(	kIx(	(
∀	∀	k?	∀
α	α	k?	α
∈	∈	k?	∈
T	T	kA	T
)	)	kIx)	)
(	(	kIx(	(
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
α	α	k?	α
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
+	+	kIx~	+
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
z	z	k7c2	z
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
)	)	kIx)	)
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
forall	forall	k1gInSc1	forall
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
V	V	kA	V
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
forall	forall	k1gInSc1	forall
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
V	V	kA	V
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
forall	forall	k1gMnSc1	forall
\	\	kIx~	\
<g/>
alpha	alpha	k1gMnSc1	alpha
\	\	kIx~	\
<g/>
in	in	k?	in
T	T	kA	T
<g/>
)	)	kIx)	)
<g/>
((	((	k?	((
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s>
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
z	z	k7c2	z
<g/>
}}	}}	k?	}}
<g/>
))	))	k?	))
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
kde	kde	k6eAd1	kde
pruh	pruh	k1gInSc1	pruh
nad	nad	k7c7	nad
číslem	číslo	k1gNnSc7	číslo
označuje	označovat	k5eAaImIp3nS	označovat
jeho	jeho	k3xOp3gNnSc1	jeho
komplexní	komplexní	k2eAgNnSc1d1	komplexní
sdružení	sdružení	k1gNnSc1	sdružení
<g/>
.	.	kIx.	.
</s>
<s>
Jinými	jiný	k2eAgNnPc7d1	jiné
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
skalární	skalární	k2eAgInSc1d1	skalární
součin	součin	k1gInSc1	součin
je	být	k5eAaImIp3nS	být
pozitivní	pozitivní	k2eAgFnSc1d1	pozitivní
nedegenerovaná	degenerovaný	k2eNgFnSc1d1	degenerovaný
sesquilineární	sesquilineární	k2eAgFnSc1d1	sesquilineární
forma	forma	k1gFnSc1	forma
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
dvou	dva	k4xCgInPc6	dva
vektorech	vektor	k1gInPc6	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}}	}}}	k?	}}}
pak	pak	k6eAd1	pak
říkáme	říkat	k5eAaImIp1nP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
kolmé	kolmý	k2eAgNnSc1d1	kolmé
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
ortogonální	ortogonální	k2eAgInPc1d1	ortogonální
<g/>
,	,	kIx,	,
právě	právě	k6eAd1	právě
když	když	k8xS	když
jejich	jejich	k3xOp3gInSc1	jejich
skalární	skalární	k2eAgInSc1d1	skalární
součin	součin	k1gInSc1	součin
je	být	k5eAaImIp3nS	být
nula	nula	k1gFnSc1	nula
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Máme	mít	k5eAaImIp1nP	mít
<g/>
-li	i	k?	-li
navíc	navíc	k6eAd1	navíc
zadaný	zadaný	k2eAgInSc4d1	zadaný
skalární	skalární	k2eAgInSc4d1	skalární
součin	součin	k1gInSc4	součin
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jsme	být	k5eAaImIp1nP	být
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
pomocí	pomoc	k1gFnSc7	pomoc
schopni	schopen	k2eAgMnPc1d1	schopen
zavést	zavést	k5eAaPmF	zavést
i	i	k8xC	i
normu	norma	k1gFnSc4	norma
<g/>
.	.	kIx.	.
</s>
<s>
Norma	Norma	k1gFnSc1	Norma
se	se	k3xPyFc4	se
pomocí	pomocí	k7c2	pomocí
skalárního	skalární	k2eAgInSc2d1	skalární
součinu	součin	k1gInSc2	součin
přitom	přitom	k6eAd1	přitom
zavádí	zavádět	k5eAaImIp3nS	zavádět
kanonicky	kanonicky	k6eAd1	kanonicky
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
máme	mít	k5eAaImIp1nP	mít
<g/>
-li	i	k?	-li
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
⋅	⋅	k?	⋅
,	,	kIx,	,
⋅	⋅	k?	⋅
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
)	)	kIx)	)
<g/>
}	}	kIx)	}
skalární	skalární	k2eAgInSc1d1	skalární
součin	součin	k1gInSc1	součin
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
tak	tak	k6eAd1	tak
můžeme	moct	k5eAaImIp1nP	moct
definovat	definovat	k5eAaBmF	definovat
zobrazení	zobrazení	k1gNnSc4	zobrazení
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
⋅	⋅	k?	⋅
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
}	}	kIx)	}
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
∥	∥	k?	∥
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
vektor	vektor	k1gInSc4	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
V	V	kA	V
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
požadavek	požadavek	k1gInSc1	požadavek
v	v	k7c4	v
definici	definice	k1gFnSc4	definice
skalárního	skalární	k2eAgInSc2d1	skalární
součinu	součin	k1gInSc2	součin
nám	my	k3xPp1nPc3	my
přitom	přitom	k6eAd1	přitom
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
odmocninou	odmocnina	k1gFnSc7	odmocnina
objeví	objevit	k5eAaPmIp3nP	objevit
jen	jen	k9	jen
nezáporná	záporný	k2eNgNnPc1d1	nezáporné
čísla	číslo	k1gNnPc1	číslo
a	a	k8xC	a
odmocnina	odmocnina	k1gFnSc1	odmocnina
tak	tak	k6eAd1	tak
má	mít	k5eAaImIp3nS	mít
vždy	vždy	k6eAd1	vždy
v	v	k7c6	v
reálných	reálný	k2eAgNnPc6d1	reálné
číslech	číslo	k1gNnPc6	číslo
smysl	smysl	k1gInSc4	smysl
<g/>
.	.	kIx.	.
</s>
<s>
Ověřme	ověřit	k5eAaPmRp1nP	ověřit
nyní	nyní	k6eAd1	nyní
<g/>
,	,	kIx,	,
že	že	k8xS	že
právě	právě	k9	právě
zavedené	zavedený	k2eAgNnSc1d1	zavedené
zobrazení	zobrazení	k1gNnSc1	zobrazení
je	být	k5eAaImIp3nS	být
skutečně	skutečně	k6eAd1	skutečně
norma	norma	k1gFnSc1	norma
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
požadavek	požadavek	k1gInSc1	požadavek
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
v	v	k7c6	v
oddíle	oddíl	k1gInSc6	oddíl
Normované	normovaný	k2eAgFnSc2d1	normovaná
vektorové	vektorový	k2eAgFnSc2d1	vektorová
prostory	prostora	k1gFnSc2	prostora
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
snadné	snadný	k2eAgNnSc1d1	snadné
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
.	.	kIx.	.
</s>
<s>
Zaměřme	zaměřit	k5eAaPmRp1nP	zaměřit
se	se	k3xPyFc4	se
na	na	k7c4	na
požadavek	požadavek	k1gInSc4	požadavek
třetí	třetí	k4xOgInSc4	třetí
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
trojúhelníkovou	trojúhelníkový	k2eAgFnSc4d1	trojúhelníková
nerovnost	nerovnost	k1gFnSc4	nerovnost
<g/>
.	.	kIx.	.
</s>
<s>
Využijeme	využít	k5eAaPmIp1nP	využít
<g/>
-li	i	k?	-li
vlastností	vlastnost	k1gFnSc7	vlastnost
skalárního	skalární	k2eAgInSc2d1	skalární
součinu	součin	k1gInSc2	součin
<g/>
,	,	kIx,	,
obdržíme	obdržet	k5eAaPmIp1nP	obdržet
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
+	+	kIx~	+
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s hack="1">
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
+	+	kIx~	+
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
+	+	kIx~	+
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
(	(	kIx(	(
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
̄	̄	k?	̄
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
2	[number]	k4	2
:	:	kIx,	:
Re	re	k9	re
:	:	kIx,	:
{	{	kIx(	{
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
}	}	kIx)	}
+	+	kIx~	+
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g />
.	.	kIx.	.
</s>
<s>
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{{	{{	k?	{{
<g/>
\	\	kIx~	\
<g/>
big	big	k?	big
(	(	kIx(	(
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s>
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
big	big	k?	big
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
overline	overlin	k1gInSc5	overlin
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s>
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
big	big	k?	big
(	(	kIx(	(
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
big	big	k?	big
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
Re	re	k9	re
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Re	re	k9	re
:	:	kIx,	:
{	{	kIx(	{
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
Re	re	k9	re
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
}}	}}	k?	}}
označuje	označovat	k5eAaImIp3nS	označovat
reálnou	reálný	k2eAgFnSc4d1	reálná
část	část	k1gFnSc4	část
čísla	číslo	k1gNnSc2	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
můžeme	moct	k5eAaImIp1nP	moct
seshora	seshora	k6eAd1	seshora
odhadnout	odhadnout	k5eAaPmF	odhadnout
absolutní	absolutní	k2eAgFnSc7d1	absolutní
hodnotou	hodnota	k1gFnSc7	hodnota
téhož	týž	k3xTgNnSc2	týž
čísla	číslo	k1gNnSc2	číslo
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
Re	re	k9	re
:	:	kIx,	:
{	{	kIx(	{
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
}	}	kIx)	}
≤	≤	k?	≤
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
text	text	k1gInSc1	text
<g/>
{	{	kIx(	{
<g/>
Re	re	k9	re
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
|	|	kIx~	|
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
|	|	kIx~	|
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Absolutní	absolutní	k2eAgFnSc4d1	absolutní
hodnotu	hodnota	k1gFnSc4	hodnota
skalárního	skalární	k2eAgInSc2d1	skalární
součinu	součin	k1gInSc2	součin
můžeme	moct	k5eAaImIp1nP	moct
navíc	navíc	k6eAd1	navíc
díky	díky	k7c3	díky
Schwarzově	Schwarzův	k2eAgFnSc3d1	Schwarzova
nerovnosti	nerovnost	k1gFnSc3	nerovnost
odhadnout	odhadnout	k5eAaPmF	odhadnout
jako	jako	k8xS	jako
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
≤	≤	k?	≤
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
<g />
.	.	kIx.	.
</s>
<s hack="1">
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
|	|	kIx~	|
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
tedy	tedy	k9	tedy
dostáváme	dostávat	k5eAaImIp1nP	dostávat
výraz	výraz	k1gInSc4	výraz
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
≤	≤	k?	≤
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
2	[number]	k4	2
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s hack="1">
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
+	+	kIx~	+
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
+	+	kIx~	+
∥	∥	k?	∥
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
+	+	kIx~	+
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g />
.	.	kIx.	.
</s>
<s>
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g />
.	.	kIx.	.
</s>
<s>
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s>
<g/>
sqrt	sqrt	k1gInSc1	sqrt
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
jenž	jenž	k3xRgInSc1	jenž
jsme	být	k5eAaImIp1nP	být
měli	mít	k5eAaImAgMnP	mít
dokázat	dokázat	k5eAaPmF	dokázat
<g/>
.	.	kIx.	.
</s>
<s>
Zobrazení	zobrazení	k1gNnSc1	zobrazení
definované	definovaný	k2eAgNnSc1d1	definované
pomocí	pomocí	k7c2	pomocí
skalárního	skalární	k2eAgInSc2d1	skalární
součinu	součin	k1gInSc2	součin
způsobem	způsob	k1gInSc7	způsob
výše	výše	k1gFnSc1	výše
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
skutečně	skutečně	k6eAd1	skutečně
norma	norma	k1gFnSc1	norma
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
vidno	vidno	k6eAd1	vidno
<g/>
,	,	kIx,	,
každý	každý	k3xTgInSc4	každý
prostor	prostor	k1gInSc4	prostor
se	s	k7c7	s
skalárním	skalární	k2eAgInSc7d1	skalární
součinem	součin	k1gInSc7	součin
je	být	k5eAaImIp3nS	být
současně	současně	k6eAd1	současně
i	i	k9	i
normovaným	normovaný	k2eAgInSc7d1	normovaný
prostorem	prostor	k1gInSc7	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
to	ten	k3xDgNnSc1	ten
však	však	k9	však
platit	platit	k5eAaImF	platit
nemusí	muset	k5eNaImIp3nS	muset
<g/>
.	.	kIx.	.
</s>
<s>
Níže	nízce	k6eAd2	nízce
si	se	k3xPyFc3	se
nicméně	nicméně	k8xC	nicméně
uvedeme	uvést	k5eAaPmIp1nP	uvést
příklady	příklad	k1gInPc1	příklad
těch	ten	k3xDgInPc2	ten
normovaných	normovaný	k2eAgInPc2d1	normovaný
prostorů	prostor	k1gInPc2	prostor
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgInPc6	který
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
dá	dát	k5eAaPmIp3nS	dát
zavést	zavést	k5eAaPmF	zavést
i	i	k9	i
skalární	skalární	k2eAgInSc1d1	skalární
součin	součin	k1gInSc1	součin
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Eukleidův	Eukleidův	k2eAgInSc4d1	Eukleidův
prostor	prostor	k1gInSc4	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Vraťme	vrátit	k5eAaPmRp1nP	vrátit
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
k	k	k7c3	k
našemu	náš	k3xOp1gInSc3	náš
oblíbenému	oblíbený	k2eAgInSc3d1	oblíbený
vektorovému	vektorový	k2eAgInSc3d1	vektorový
prostoru	prostor	k1gInSc3	prostor
aritmetických	aritmetický	k2eAgInPc2d1	aritmetický
vektorů	vektor	k1gInPc2	vektor
definovanému	definovaný	k2eAgInSc3d1	definovaný
nad	nad	k7c7	nad
číselným	číselný	k2eAgNnSc7d1	číselné
tělesem	těleso	k1gNnSc7	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
konkrétnost	konkrétnost	k1gFnSc4	konkrétnost
uvažujme	uvažovat	k5eAaImRp1nP	uvažovat
prostor	prostor	k1gInSc4	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
C	C	kA	C
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
nad	nad	k7c7	nad
tělesem	těleso	k1gNnSc7	těleso
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
C	C	kA	C
<g/>
}	}	kIx)	}
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
normy	norma	k1gFnSc2	norma
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
v	v	k7c6	v
případě	případ	k1gInSc6	případ
skalárního	skalární	k2eAgInSc2d1	skalární
součinu	součin	k1gInSc2	součin
je	být	k5eAaImIp3nS	být
více	hodně	k6eAd2	hodně
možností	možnost	k1gFnSc7	možnost
jak	jak	k8xC	jak
toto	tento	k3xDgNnSc4	tento
zobrazení	zobrazení	k1gNnSc4	zobrazení
zavést	zavést	k5eAaPmF	zavést
<g/>
.	.	kIx.	.
</s>
<s>
My	my	k3xPp1nPc1	my
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
zmíníme	zmínit	k5eAaPmIp1nP	zmínit
o	o	k7c6	o
nejčastěji	často	k6eAd3	často
užívaném	užívaný	k2eAgInSc6d1	užívaný
součinu	součin	k1gInSc6	součin
<g/>
,	,	kIx,	,
standardním	standardní	k2eAgInSc6d1	standardní
skalárním	skalární	k2eAgInSc6d1	skalární
součinu	součin	k1gInSc6	součin
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
je	být	k5eAaImIp3nS	být
dán	dát	k5eAaPmNgInS	dát
vztahem	vztah	k1gInSc7	vztah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
i	i	k9	i
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
̄	̄	k?	̄
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
i	i	k8xC	i
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
bar	bar	k1gInSc1	bar
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
kde	kde	k6eAd1	kde
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
...	...	k?	...
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
(	(	kIx(	(
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
...	...	k?	...
,	,	kIx,	,
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
,	,	kIx,	,
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
jsou	být	k5eAaImIp3nP	být
libovolné	libovolný	k2eAgInPc1d1	libovolný
dva	dva	k4xCgInPc1	dva
vektory	vektor	k1gInPc1	vektor
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
C	C	kA	C
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Pruh	pruh	k1gInSc1	pruh
nad	nad	k7c7	nad
složkou	složka	k1gFnSc7	složka
vektoru	vektor	k1gInSc2	vektor
označuje	označovat	k5eAaImIp3nS	označovat
komplexní	komplexní	k2eAgNnSc1d1	komplexní
sdružení	sdružení	k1gNnSc1	sdružení
daného	daný	k2eAgNnSc2d1	dané
čísla	číslo	k1gNnSc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
musíme	muset	k5eAaImIp1nP	muset
ověřit	ověřit	k5eAaPmF	ověřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
toto	tento	k3xDgNnSc4	tento
zobrazení	zobrazení	k1gNnSc4	zobrazení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
dvěma	dva	k4xCgFnPc7	dva
vektorům	vektor	k1gInPc3	vektor
přiřadí	přiřadit	k5eAaPmIp3nS	přiřadit
obecně	obecně	k6eAd1	obecně
komplexní	komplexní	k2eAgNnSc4d1	komplexní
číslo	číslo	k1gNnSc4	číslo
<g/>
,	,	kIx,	,
skutečně	skutečně	k6eAd1	skutečně
splňuje	splňovat	k5eAaImIp3nS	splňovat
axiomy	axiom	k1gInPc4	axiom
skalárního	skalární	k2eAgInSc2d1	skalární
součinu	součin	k1gInSc2	součin
a	a	k8xC	a
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
si	se	k3xPyFc3	se
tedy	tedy	k9	tedy
zaslouží	zasloužit	k5eAaPmIp3nS	zasloužit
<g/>
.	.	kIx.	.
</s>
<s>
Ověření	ověření	k1gNnSc1	ověření
všech	všecek	k3xTgFnPc2	všecek
náležitostí	náležitost	k1gFnPc2	náležitost
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
snadné	snadný	k2eAgNnSc1d1	snadné
<g/>
,	,	kIx,	,
máme	mít	k5eAaImIp1nP	mít
tak	tak	k6eAd1	tak
aritmetický	aritmetický	k2eAgInSc4d1	aritmetický
vektorový	vektorový	k2eAgInSc4d1	vektorový
prostor	prostor	k1gInSc4	prostor
vybavený	vybavený	k2eAgInSc4d1	vybavený
standardním	standardní	k2eAgInSc7d1	standardní
skalárním	skalární	k2eAgInSc7d1	skalární
součinem	součin	k1gInSc7	součin
<g/>
.	.	kIx.	.
</s>
<s>
Zaveďme	zavést	k5eAaPmRp1nP	zavést
nyní	nyní	k6eAd1	nyní
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
tohoto	tento	k3xDgInSc2	tento
skalárního	skalární	k2eAgInSc2d1	skalární
součinu	součin	k1gInSc2	součin
normu	norma	k1gFnSc4	norma
na	na	k7c6	na
prostoru	prostor	k1gInSc6	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
C	C	kA	C
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
postupem	postup	k1gInSc7	postup
zmíněným	zmíněný	k2eAgInSc7d1	zmíněný
v	v	k7c6	v
úvodu	úvod	k1gInSc6	úvod
nadřazené	nadřazený	k2eAgFnSc2d1	nadřazená
sekce	sekce	k1gFnSc2	sekce
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
libovolný	libovolný	k2eAgInSc4d1	libovolný
vektor	vektor	k1gInSc4	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
...	...	k?	...
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
tedy	tedy	k8xC	tedy
dostáváme	dostávat	k5eAaImIp1nP	dostávat
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
i	i	k9	i
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
̄	̄	k?	̄
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
i	i	k9	i
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{	{	kIx(	{
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
bar	bar	k1gInSc1	bar
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}}	}}}	k?	}}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Jak	jak	k6eAd1	jak
vidíme	vidět	k5eAaImIp1nP	vidět
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
norma	norma	k1gFnSc1	norma
je	být	k5eAaImIp3nS	být
rovna	roven	k2eAgFnSc1d1	rovna
Euklidově	Euklidův	k2eAgFnSc3d1	Euklidova
normě	norma	k1gFnSc3	norma
definované	definovaný	k2eAgFnPc1d1	definovaná
v	v	k7c6	v
oddíle	oddíl	k1gInSc6	oddíl
Aritmetické	aritmetický	k2eAgFnSc2d1	aritmetická
prostory	prostora	k1gFnSc2	prostora
<g/>
.	.	kIx.	.
</s>
<s>
Standardní	standardní	k2eAgInSc1d1	standardní
skalární	skalární	k2eAgInSc1d1	skalární
součin	součin	k1gInSc1	součin
tedy	tedy	k9	tedy
indukuje	indukovat	k5eAaBmIp3nS	indukovat
Euklidovu	Euklidův	k2eAgFnSc4d1	Euklidova
normu	norma	k1gFnSc4	norma
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
⋅	⋅	k?	⋅
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
místo	místo	k7c2	místo
komplexních	komplexní	k2eAgNnPc2d1	komplexní
čísel	číslo	k1gNnPc2	číslo
uvažujeme	uvažovat	k5eAaImIp1nP	uvažovat
všude	všude	k6eAd1	všude
reálná	reálný	k2eAgNnPc1d1	reálné
čísla	číslo	k1gNnPc1	číslo
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
vektorový	vektorový	k2eAgInSc1d1	vektorový
prostor	prostor	k1gInSc1	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
<g />
.	.	kIx.	.
</s>
<s hack="1">
nad	nad	k7c7	nad
tělesem	těleso	k1gNnSc7	těleso
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
}	}	kIx)	}
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
standardní	standardní	k2eAgInSc1d1	standardní
skalární	skalární	k2eAgInSc1d1	skalární
součin	součin	k1gInSc1	součin
redukuje	redukovat	k5eAaBmIp3nS	redukovat
do	do	k7c2	do
tvaru	tvar	k1gInSc2	tvar
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
i	i	k9	i
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
i	i	k8xC	i
<g/>
}	}	kIx)	}
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
...	...	k?	...
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
(	(	kIx(	(
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
...	...	k?	...
,	,	kIx,	,
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
,	,	kIx,	,
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
jsou	být	k5eAaImIp3nP	být
libovolné	libovolný	k2eAgInPc1d1	libovolný
dva	dva	k4xCgInPc1	dva
vektory	vektor	k1gInPc1	vektor
z	z	k7c2	z
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tento	tento	k3xDgInSc1	tento
skalární	skalární	k2eAgInSc1d1	skalární
součin	součin	k1gInSc1	součin
samozřejmě	samozřejmě	k6eAd1	samozřejmě
indukuje	indukovat	k5eAaBmIp3nS	indukovat
Euklidovu	Euklidův	k2eAgFnSc4d1	Euklidova
normu	norma	k1gFnSc4	norma
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
na	na	k7c6	na
prostoru	prostor	k1gInSc6	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Vektorovému	vektorový	k2eAgInSc3d1	vektorový
prostoru	prostor	k1gInSc3	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
nad	nad	k7c7	nad
tělesem	těleso	k1gNnSc7	těleso
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
}	}	kIx)	}
s	s	k7c7	s
výše	vysoce	k6eAd2	vysoce
definovaným	definovaný	k2eAgInSc7d1	definovaný
skalárním	skalární	k2eAgInSc7d1	skalární
součinem	součin	k1gInSc7	součin
a	a	k8xC	a
odpovídající	odpovídající	k2eAgFnSc7d1	odpovídající
normou	norma	k1gFnSc7	norma
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
Eukleidův	Eukleidův	k2eAgInSc1d1	Eukleidův
prostor	prostor	k1gInSc1	prostor
dimenze	dimenze	k1gFnSc2	dimenze
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
n	n	k0	n
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
lp	lp	k?	lp
prostor	prostor	k1gInSc1	prostor
(	(	kIx(	(
<g/>
posloupnosti	posloupnost	k1gFnPc1	posloupnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Uvažujme	uvažovat	k5eAaImRp1nP	uvažovat
nyní	nyní	k6eAd1	nyní
l	l	kA	l
<g/>
2	[number]	k4	2
prostor	prostora	k1gFnPc2	prostora
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
lp	lp	k?	lp
prostor	prostor	k1gInSc4	prostor
definovaný	definovaný	k2eAgInSc4d1	definovaný
výše	vysoce	k6eAd2	vysoce
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
položíme	položit	k5eAaPmIp1nP	položit
p	p	k?	p
=	=	kIx~	=
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
prostor	prostor	k1gInSc1	prostor
tvoří	tvořit	k5eAaImIp3nS	tvořit
konvergentní	konvergentní	k2eAgFnSc2d1	konvergentní
posloupnosti	posloupnost	k1gFnSc2	posloupnost
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
(	(	kIx(	(
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
_	_	kIx~	_
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}}	}}	k?	}}
komplexních	komplexní	k2eAgNnPc2d1	komplexní
čísel	číslo	k1gNnPc2	číslo
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgInPc4	který
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
<	<	kIx(	<
+	+	kIx~	+
∞	∞	k?	∞
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
<	<	kIx(	<
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
posloupnosti	posloupnost	k1gFnSc6	posloupnost
jistým	jistý	k2eAgNnSc7d1	jisté
zobecněním	zobecnění	k1gNnSc7	zobecnění
aritmetických	aritmetický	k2eAgInPc2d1	aritmetický
vektorů	vektor	k1gInPc2	vektor
<g/>
,	,	kIx,	,
zaveďme	zavést	k5eAaPmRp1nP	zavést
v	v	k7c6	v
analogii	analogie	k1gFnSc6	analogie
s	s	k7c7	s
předchozím	předchozí	k2eAgInSc7d1	předchozí
příkladem	příklad	k1gInSc7	příklad
zobrazení	zobrazení	k1gNnSc2	zobrazení
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
(	(	kIx(	(
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
)	)	kIx)	)
,	,	kIx,	,
(	(	kIx(	(
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
̄	̄	k?	̄
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
<g />
.	.	kIx.	.
</s>
<s hack="1">
((	((	k?	((
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
(	(	kIx(	(
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
))	))	k?	))
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	infta	k1gFnSc2	infta
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
bar	bar	k1gInSc1	bar
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
(	(	kIx(	(
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
(	(	kIx(	(
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
jsou	být	k5eAaImIp3nP	být
libovolné	libovolný	k2eAgFnPc1d1	libovolná
dvě	dva	k4xCgFnPc1	dva
posloupnosti	posloupnost	k1gFnPc1	posloupnost
z	z	k7c2	z
prostoru	prostor	k1gInSc2	prostor
l	l	kA	l
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tomto	tento	k3xDgNnSc6	tento
zobrazení	zobrazení	k1gNnSc6	zobrazení
bychom	by	kYmCp1nP	by
chtěli	chtít	k5eAaImAgMnP	chtít
opět	opět	k6eAd1	opět
ukázat	ukázat	k5eAaPmF	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
skalární	skalární	k2eAgInSc4d1	skalární
součin	součin	k1gInSc4	součin
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
vlastnosti	vlastnost	k1gFnPc1	vlastnost
skalárního	skalární	k2eAgInSc2d1	skalární
součinu	součin	k1gInSc2	součin
bychom	by	kYmCp1nP	by
ověřovali	ověřovat	k5eAaImAgMnP	ověřovat
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
u	u	k7c2	u
aritmetických	aritmetický	k2eAgInPc2d1	aritmetický
vektorů	vektor	k1gInPc2	vektor
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
ale	ale	k8xC	ale
navíc	navíc	k6eAd1	navíc
ještě	ještě	k9	ještě
potřebujeme	potřebovat	k5eAaImIp1nP	potřebovat
vědět	vědět	k5eAaImF	vědět
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
řada	řada	k1gFnSc1	řada
<g/>
,	,	kIx,	,
vystupující	vystupující	k2eAgFnSc1d1	vystupující
v	v	k7c6	v
definici	definice	k1gFnSc6	definice
zobrazení	zobrazení	k1gNnSc2	zobrazení
výše	výše	k1gFnSc2	výše
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
konečný	konečný	k2eAgInSc1d1	konečný
součet	součet	k1gInSc1	součet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
lze	lze	k6eAd1	lze
užít	užít	k5eAaPmF	užít
Hölderovy	Hölderův	k2eAgFnPc4d1	Hölderův
nerovnosti	nerovnost	k1gFnPc4	nerovnost
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
≤	≤	k?	≤
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s>
<g/>
leq	leq	k?	leq
\	\	kIx~	\
<g/>
left	left	k1gMnSc1	left
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
<g />
.	.	kIx.	.
</s>
<s>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
vybíráme	vybírat	k5eAaImIp1nP	vybírat
posloupnosti	posloupnost	k1gFnPc4	posloupnost
s	s	k7c7	s
prostoru	prostor	k1gInSc6	prostor
l	l	kA	l
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
jsou	být	k5eAaImIp3nP	být
řady	řada	k1gFnPc1	řada
na	na	k7c6	na
pravé	pravý	k2eAgFnSc6d1	pravá
straně	strana	k1gFnSc6	strana
nerovnosti	nerovnost	k1gFnSc3	nerovnost
konečné	konečná	k1gFnSc3	konečná
a	a	k8xC	a
číslo	číslo	k1gNnSc1	číslo
na	na	k7c6	na
pravé	pravý	k2eAgFnSc6d1	pravá
straně	strana	k1gFnSc6	strana
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
konečné	konečný	k2eAgNnSc1d1	konečné
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
plyne	plynout	k5eAaImIp3nS	plynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
konečná	konečný	k2eAgFnSc1d1	konečná
i	i	k8xC	i
řada	řada	k1gFnSc1	řada
na	na	k7c6	na
levé	levý	k2eAgFnSc6d1	levá
straně	strana	k1gFnSc6	strana
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
řadu	řada	k1gFnSc4	řada
ale	ale	k8xC	ale
zjevně	zjevně	k6eAd1	zjevně
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
̄	̄	k?	̄
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
≤	≤	k?	≤
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∞	∞	k?	∞
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
a	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
bar	bar	k1gInSc1	bar
{	{	kIx(	{
<g/>
a	a	k8xC	a
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
b_	b_	k?	b_
<g/>
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
b_	b_	k?	b_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Máme	mít	k5eAaImIp1nP	mít
tak	tak	k6eAd1	tak
ověřeno	ověřen	k2eAgNnSc1d1	ověřeno
<g/>
,	,	kIx,	,
že	že	k8xS	že
zobrazení	zobrazení	k1gNnSc1	zobrazení
výše	výše	k1gFnSc2	výše
je	být	k5eAaImIp3nS	být
skalární	skalární	k2eAgInSc1d1	skalární
součin	součin	k1gInSc1	součin
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
každé	každý	k3xTgNnSc4	každý
dvojici	dvojice	k1gFnSc4	dvojice
posloupností	posloupnost	k1gFnPc2	posloupnost
z	z	k7c2	z
prostoru	prostor	k1gInSc2	prostor
l	l	kA	l
<g/>
2	[number]	k4	2
přiřazuje	přiřazovat	k5eAaImIp3nS	přiřazovat
(	(	kIx(	(
<g/>
konečné	konečný	k2eAgNnSc4d1	konečné
<g/>
)	)	kIx)	)
číslo	číslo	k1gNnSc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
v	v	k7c6	v
předchozím	předchozí	k2eAgInSc6d1	předchozí
příkladu	příklad	k1gInSc6	příklad
bychom	by	kYmCp1nP	by
i	i	k9	i
nyní	nyní	k6eAd1	nyní
ověřili	ověřit	k5eAaPmAgMnP	ověřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
skalární	skalární	k2eAgInSc1d1	skalární
součin	součin	k1gInSc1	součin
definuje	definovat	k5eAaBmIp3nS	definovat
normu	norma	k1gFnSc4	norma
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
totožná	totožný	k2eAgFnSc1d1	totožná
s	s	k7c7	s
normou	norma	k1gFnSc7	norma
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
⋅	⋅	k?	⋅
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
definovanou	definovaný	k2eAgFnSc4d1	definovaná
v	v	k7c6	v
oddíle	oddíl	k1gInSc6	oddíl
lp	lp	k?	lp
prostory	prostora	k1gFnSc2	prostora
<g/>
,	,	kIx,	,
když	když	k8xS	když
položíme	položit	k5eAaPmIp1nP	položit
p	p	k?	p
<g/>
=	=	kIx~	=
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Lp	Lp	k1gFnSc2	Lp
prostor	prostora	k1gFnPc2	prostora
<g/>
.	.	kIx.	.
</s>
<s>
Přejděme	přejít	k5eAaPmRp1nP	přejít
nyní	nyní	k6eAd1	nyní
od	od	k7c2	od
lp	lp	k?	lp
prostorů	prostor	k1gInPc2	prostor
číselných	číselný	k2eAgFnPc2d1	číselná
posloupností	posloupnost	k1gFnPc2	posloupnost
k	k	k7c3	k
Lp	Lp	k1gFnPc3	Lp
prostorům	prostor	k1gInPc3	prostor
měřitelných	měřitelný	k2eAgFnPc2d1	měřitelná
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Uvažujme	uvažovat	k5eAaImRp1nP	uvažovat
konkrétně	konkrétně	k6eAd1	konkrétně
L2	L2	k1gFnSc4	L2
prostor	prostora	k1gFnPc2	prostora
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
množinu	množina	k1gFnSc4	množina
měřitelných	měřitelný	k2eAgFnPc2d1	měřitelná
funkcí	funkce	k1gFnPc2	funkce
definovaných	definovaný	k2eAgFnPc2d1	definovaná
na	na	k7c6	na
prostoru	prostor	k1gInSc6	prostor
s	s	k7c7	s
mírou	míra	k1gFnSc7	míra
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
X	X	kA	X
,	,	kIx,	,
Σ	Σ	k?	Σ
,	,	kIx,	,
μ	μ	k?	μ
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
(	(	kIx(	(
<g/>
X	X	kA	X
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
Sigma	sigma	k1gNnSc2	sigma
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
které	který	k3yRgFnPc1	který
splňují	splňovat	k5eAaImIp3nP	splňovat
<g />
.	.	kIx.	.
</s>
<s hack="1">
vztah	vztah	k1gInSc1	vztah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
f	f	k?	f
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
μ	μ	k?	μ
<	<	kIx(	<
+	+	kIx~	+
∞	∞	k?	∞
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
int	int	k?	int
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
f	f	k?	f
<g/>
|	|	kIx~	|
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
<	<	kIx(	<
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
infty	inft	k1gInPc1	inft
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
I	i	k9	i
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
prostoru	prostor	k1gInSc6	prostor
chceme	chtít	k5eAaImIp1nP	chtít
zavést	zavést	k5eAaPmF	zavést
skalární	skalární	k2eAgInSc4d1	skalární
součin	součin	k1gInSc4	součin
<g/>
.	.	kIx.	.
</s>
<s>
Vyjdeme	vyjít	k5eAaPmIp1nP	vyjít
<g/>
-li	i	k?	-li
z	z	k7c2	z
výrazu	výraz	k1gInSc2	výraz
pro	pro	k7c4	pro
skalární	skalární	k2eAgInSc4d1	skalární
součin	součin	k1gInSc4	součin
posloupností	posloupnost	k1gFnPc2	posloupnost
v	v	k7c6	v
předchozím	předchozí	k2eAgInSc6d1	předchozí
příkladu	příklad	k1gInSc6	příklad
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sumu	suma	k1gFnSc4	suma
zaměníme	zaměnit	k5eAaPmIp1nP	zaměnit
za	za	k7c4	za
integrál	integrál	k1gInSc4	integrál
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
obdržíme	obdržet	k5eAaPmIp1nP	obdržet
definiční	definiční	k2eAgInSc4d1	definiční
vztah	vztah	k1gInSc4	vztah
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
f	f	k?	f
,	,	kIx,	,
g	g	kA	g
)	)	kIx)	)
=	=	kIx~	=
:	:	kIx,	:
∫	∫	k?	∫
:	:	kIx,	:
X	X	kA	X
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
̄	̄	k?	̄
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
g	g	kA	g
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
d	d	k?	d
:	:	kIx,	:
μ	μ	k?	μ
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
f	f	k?	f
<g/>
,	,	kIx,	,
<g/>
g	g	kA	g
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
int	int	k?	int
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
X	X	kA	X
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
bar	bar	k1gInSc1	bar
{	{	kIx(	{
<g/>
f	f	k?	f
<g/>
}}	}}	k?	}}
<g/>
g	g	kA	g
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
f	f	k?	f
,	,	kIx,	,
g	g	kA	g
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
f	f	k?	f
<g/>
,	,	kIx,	,
<g/>
g	g	kA	g
<g/>
}	}	kIx)	}
jsou	být	k5eAaImIp3nP	být
libovolné	libovolný	k2eAgFnPc4d1	libovolná
(	(	kIx(	(
<g/>
komplexní	komplexní	k2eAgFnPc4d1	komplexní
<g/>
)	)	kIx)	)
funkce	funkce	k1gFnPc4	funkce
z	z	k7c2	z
L	L	kA	L
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Analogicky	analogicky	k6eAd1	analogicky
jako	jako	k9	jako
pro	pro	k7c4	pro
posloupnosti	posloupnost	k1gFnPc4	posloupnost
bychom	by	kYmCp1nP	by
i	i	k9	i
zde	zde	k6eAd1	zde
ověřili	ověřit	k5eAaPmAgMnP	ověřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zadaný	zadaný	k2eAgInSc1d1	zadaný
vztah	vztah	k1gInSc1	vztah
definuje	definovat	k5eAaBmIp3nS	definovat
skalární	skalární	k2eAgInSc1d1	skalární
součin	součin	k1gInSc1	součin
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
bychom	by	kYmCp1nP	by
pro	pro	k7c4	pro
ověřování	ověřování	k1gNnSc4	ověřování
trojúhelníkové	trojúhelníkový	k2eAgFnSc2d1	trojúhelníková
nerovnosti	nerovnost	k1gFnSc2	nerovnost
využili	využít	k5eAaPmAgMnP	využít
Hölderovy	Hölderův	k2eAgFnPc4d1	Hölderův
nerovnosti	nerovnost	k1gFnPc4	nerovnost
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
v	v	k7c6	v
integrálním	integrální	k2eAgInSc6d1	integrální
tvaru	tvar	k1gInSc6	tvar
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
posloupností	posloupnost	k1gFnPc2	posloupnost
bychom	by	kYmCp1nP	by
i	i	k9	i
nyní	nyní	k6eAd1	nyní
ukázali	ukázat	k5eAaPmAgMnP	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
takto	takto	k6eAd1	takto
definovaný	definovaný	k2eAgInSc1d1	definovaný
skalární	skalární	k2eAgInSc1d1	skalární
součin	součin	k1gInSc1	součin
indukuje	indukovat	k5eAaBmIp3nS	indukovat
normu	norma	k1gFnSc4	norma
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
totožná	totožný	k2eAgFnSc1d1	totožná
s	s	k7c7	s
normou	norma	k1gFnSc7	norma
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
⋅	⋅	k?	⋅
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
objevující	objevující	k2eAgMnSc1d1	objevující
se	se	k3xPyFc4	se
v	v	k7c6	v
oddíle	oddíl	k1gInSc6	oddíl
Lp	Lp	k1gFnSc2	Lp
prostory	prostora	k1gFnSc2	prostora
<g/>
,	,	kIx,	,
když	když	k8xS	když
položíme	položit	k5eAaPmIp1nP	položit
p	p	k?	p
<g/>
=	=	kIx~	=
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Vektorovému	vektorový	k2eAgInSc3d1	vektorový
prostoru	prostor	k1gInSc3	prostor
L2	L2	k1gFnSc2	L2
vybavenému	vybavený	k2eAgInSc3d1	vybavený
skalárním	skalární	k2eAgInSc7d1	skalární
součinem	součin	k1gInSc7	součin
definovaným	definovaný	k2eAgInSc7d1	definovaný
výše	vysoce	k6eAd2	vysoce
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
prostor	prostor	k1gInSc1	prostor
kvadraticky	kvadraticky	k6eAd1	kvadraticky
integrabilních	integrabilní	k2eAgFnPc2d1	integrabilní
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
prostor	prostor	k1gInSc1	prostor
hraje	hrát	k5eAaImIp3nS	hrát
zvlášť	zvlášť	k6eAd1	zvlášť
důležitou	důležitý	k2eAgFnSc4d1	důležitá
roli	role	k1gFnSc4	role
v	v	k7c6	v
kvantové	kvantový	k2eAgFnSc6d1	kvantová
mechanice	mechanika	k1gFnSc6	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgFnPc4	všechen
vlnové	vlnový	k2eAgFnPc4d1	vlnová
funkce	funkce	k1gFnPc4	funkce
popisující	popisující	k2eAgInSc1d1	popisující
stav	stav	k1gInSc1	stav
kvantového	kvantový	k2eAgInSc2d1	kvantový
systému	systém	k1gInSc2	systém
totiž	totiž	k9	totiž
musejí	muset	k5eAaImIp3nP	muset
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
Bornovým	Bornův	k2eAgInSc7d1	Bornův
postulátem	postulát	k1gInSc7	postulát
patřit	patřit	k5eAaImF	patřit
do	do	k7c2	do
tohoto	tento	k3xDgInSc2	tento
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Topologický	topologický	k2eAgInSc1d1	topologický
vektorový	vektorový	k2eAgInSc1d1	vektorový
prostor	prostor	k1gInSc1	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
jsme	být	k5eAaImIp1nP	být
si	se	k3xPyFc3	se
uváděli	uvádět	k5eAaImAgMnP	uvádět
příklady	příklad	k1gInPc4	příklad
vektorových	vektorový	k2eAgMnPc2d1	vektorový
prostorů	prostor	k1gInPc2	prostor
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgInPc3	jenž
byla	být	k5eAaImAgFnS	být
dodatečná	dodatečný	k2eAgFnSc1d1	dodatečná
struktura	struktura	k1gFnSc1	struktura
dodána	dodat	k5eAaPmNgFnS	dodat
pomocí	pomocí	k7c2	pomocí
jistých	jistý	k2eAgNnPc2d1	jisté
zobrazení	zobrazení	k1gNnPc2	zobrazení
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
vektorům	vektor	k1gInPc3	vektor
přiřazovala	přiřazovat	k5eAaImAgNnP	přiřazovat
čísla	číslo	k1gNnPc1	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vektorový	vektorový	k2eAgInSc4d1	vektorový
prostor	prostor	k1gInSc4	prostor
však	však	k9	však
můžeme	moct	k5eAaImIp1nP	moct
nahlížet	nahlížet	k5eAaImF	nahlížet
i	i	k9	i
z	z	k7c2	z
topologického	topologický	k2eAgNnSc2d1	topologické
hlediska	hledisko	k1gNnSc2	hledisko
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
ho	on	k3xPp3gInSc4	on
totiž	totiž	k9	totiž
současně	současně	k6eAd1	současně
chápat	chápat	k5eAaImF	chápat
jako	jako	k9	jako
topologický	topologický	k2eAgInSc4d1	topologický
prostor	prostor	k1gInSc4	prostor
s	s	k7c7	s
jistou	jistý	k2eAgFnSc7d1	jistá
topologií	topologie	k1gFnSc7	topologie
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
příklad	příklad	k1gInSc4	příklad
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
topologie	topologie	k1gFnSc1	topologie
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
spojena	spojen	k2eAgFnSc1d1	spojena
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
lineární	lineární	k2eAgFnSc7d1	lineární
strukturou	struktura	k1gFnSc7	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
spojením	spojení	k1gNnSc7	spojení
máme	mít	k5eAaImIp1nP	mít
na	na	k7c6	na
mysli	mysl	k1gFnSc6	mysl
situaci	situace	k1gFnSc6	situace
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc2	jenž
jsou	být	k5eAaImIp3nP	být
operace	operace	k1gFnPc1	operace
sčítání	sčítání	k1gNnSc2	sčítání
vektorů	vektor	k1gInPc2	vektor
a	a	k8xC	a
násobení	násobení	k1gNnSc4	násobení
vektoru	vektor	k1gInSc2	vektor
prvkem	prvek	k1gInSc7	prvek
z	z	k7c2	z
tělesa	těleso	k1gNnSc2	těleso
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
topologii	topologie	k1gFnSc6	topologie
spojitými	spojitý	k2eAgNnPc7d1	spojité
zobrazeními	zobrazení	k1gNnPc7	zobrazení
<g/>
.	.	kIx.	.
</s>
<s>
Dospíváme	dospívat	k5eAaImIp1nP	dospívat
tak	tak	k6eAd1	tak
k	k	k7c3	k
objektu	objekt	k1gInSc3	objekt
nazvanému	nazvaný	k2eAgMnSc3d1	nazvaný
topologický	topologický	k2eAgInSc4d1	topologický
vektorový	vektorový	k2eAgInSc4d1	vektorový
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
matematickou	matematický	k2eAgFnSc4d1	matematická
definici	definice	k1gFnSc4	definice
uvádíme	uvádět	k5eAaImIp1nP	uvádět
v	v	k7c6	v
následujícím	následující	k2eAgNnSc6d1	následující
<g/>
.	.	kIx.	.
</s>
<s>
Vektorový	vektorový	k2eAgInSc4d1	vektorový
prostor	prostor	k1gInSc4	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	v	k7c4	v
<g/>
}	}	kIx)	}
nad	nad	k7c7	nad
tělesem	těleso	k1gNnSc7	těleso
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
}	}	kIx)	}
vybavený	vybavený	k2eAgInSc1d1	vybavený
topologií	topologie	k1gFnSc7	topologie
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
τ	τ	k?	τ
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
tau	tau	k1gNnPc6	tau
}	}	kIx)	}
tvoří	tvořit	k5eAaImIp3nS	tvořit
topologický	topologický	k2eAgInSc1d1	topologický
vektorový	vektorový	k2eAgInSc1d1	vektorový
prostor	prostor	k1gInSc1	prostor
<g/>
,	,	kIx,	,
právě	právě	k9	právě
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
splněny	splnit	k5eAaPmNgFnP	splnit
tři	tři	k4xCgFnPc1	tři
podmínky	podmínka	k1gFnPc1	podmínka
<g/>
:	:	kIx,	:
Sčítání	sčítání	k1gNnSc1	sčítání
vektorů	vektor	k1gInPc2	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⊕	⊕	k?	⊕
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
oplus	oplus	k1gMnSc1	oplus
}	}	kIx)	}
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
chápané	chápaný	k2eAgInPc1d1	chápaný
jako	jako	k8xS	jako
zobrazení	zobrazení	k1gNnSc6	zobrazení
topologických	topologický	k2eAgInPc2d1	topologický
prostorů	prostor	k1gInPc2	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⊕	⊕	k?	⊕
:	:	kIx,	:
(	(	kIx(	(
V	V	kA	V
×	×	k?	×
V	V	kA	V
,	,	kIx,	,
:	:	kIx,	:
τ	τ	k?	τ
'	'	kIx"	'
:	:	kIx,	:
)	)	kIx)	)
→	→	k?	→
(	(	kIx(	(
V	V	kA	V
,	,	kIx,	,
τ	τ	k?	τ
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
oplus	oplus	k1gInSc4	oplus
:	:	kIx,	:
<g/>
(	(	kIx(	(
<g/>
V	v	k7c4	v
<g/>
\	\	kIx~	\
<g/>
times	times	k1gInSc4	times
V	V	kA	V
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
tau	tau	k1gNnSc2	tau
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
(	(	kIx(	(
<g/>
V	V	kA	V
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
tau	tau	k1gNnSc2	tau
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
spojité	spojitý	k2eAgNnSc4d1	spojité
zobrazení	zobrazení	k1gNnSc4	zobrazení
<g/>
.	.	kIx.	.
</s>
<s>
Násobení	násobení	k1gNnSc2	násobení
prvkem	prvek	k1gInSc7	prvek
z	z	k7c2	z
tělesa	těleso	k1gNnSc2	těleso
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⊙	⊙	k?	⊙
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
odot	odotum	k1gNnPc2	odotum
}	}	kIx)	}
,	,	kIx,	,
chápané	chápaný	k2eAgNnSc4d1	chápané
jako	jako	k8xC	jako
zobrazení	zobrazení	k1gNnSc4	zobrazení
topologických	topologický	k2eAgInPc2d1	topologický
prostorů	prostor	k1gInPc2	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⊙	⊙	k?	⊙
:	:	kIx,	:
(	(	kIx(	(
T	T	kA	T
×	×	k?	×
V	V	kA	V
,	,	kIx,	,
:	:	kIx,	:
τ	τ	k?	τ
''	''	k?	''
:	:	kIx,	:
)	)	kIx)	)
→	→	k?	→
(	(	kIx(	(
V	V	kA	V
,	,	kIx,	,
τ	τ	k?	τ
<g />
.	.	kIx.	.
</s>
<s hack="1">
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
odot	odot	k5eAaPmF	odot
:	:	kIx,	:
<g/>
(	(	kIx(	(
<g/>
T	T	kA	T
<g/>
\	\	kIx~	\
<g/>
times	times	k1gInSc1	times
V	V	kA	V
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
tau	tau	k1gNnSc2	tau
''	''	k?	''
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
(	(	kIx(	(
<g/>
V	V	kA	V
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
tau	tau	k1gNnSc2	tau
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
spojité	spojitý	k2eAgNnSc4d1	spojité
zobrazení	zobrazení	k1gNnSc4	zobrazení
<g/>
.	.	kIx.	.
</s>
<s>
Prostor	prostor	k1gInSc1	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
V	V	kA	V
,	,	kIx,	,
τ	τ	k?	τ
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
(	(	kIx(	(
<g/>
V	V	kA	V
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
tau	tau	k1gNnSc2	tau
)	)	kIx)	)
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
Hausdorffův	Hausdorffův	k2eAgMnSc1d1	Hausdorffův
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládáme	předpokládat	k5eAaImIp1nP	předpokládat
přitom	přitom	k6eAd1	přitom
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
tělese	těleso	k1gNnSc6	těleso
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
též	též	k9	též
zavedena	zaveden	k2eAgFnSc1d1	zavedena
jistá	jistý	k2eAgFnSc1d1	jistá
topologie	topologie	k1gFnSc1	topologie
<g/>
.	.	kIx.	.
</s>
<s>
Topologie	topologie	k1gFnSc1	topologie
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
τ	τ	k?	τ
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
tau	tau	k1gNnSc3	tau
'	'	kIx"	'
<g/>
}	}	kIx)	}
v	v	k7c6	v
první	první	k4xOgFnSc6	první
podmínce	podmínka	k1gFnSc6	podmínka
představuje	představovat	k5eAaImIp3nS	představovat
součinovou	součinový	k2eAgFnSc4d1	součinová
topologii	topologie	k1gFnSc4	topologie
na	na	k7c6	na
kartézském	kartézský	k2eAgInSc6d1	kartézský
součinu	součin	k1gInSc6	součin
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
×	×	k?	×
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	v	k7c4	v
<g/>
\	\	kIx~	\
<g/>
times	times	k1gInSc4	times
V	V	kA	V
<g/>
}	}	kIx)	}
,	,	kIx,	,
topologie	topologie	k1gFnSc1	topologie
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
τ	τ	k?	τ
''	''	k?	''
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
tau	tau	k1gNnSc3	tau
''	''	k?	''
<g/>
}	}	kIx)	}
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
podmínce	podmínka	k1gFnSc6	podmínka
pak	pak	k6eAd1	pak
součinovou	součinový	k2eAgFnSc4d1	součinová
topologii	topologie	k1gFnSc4	topologie
na	na	k7c6	na
kartézském	kartézský	k2eAgInSc6d1	kartézský
součinu	součin	k1gInSc6	součin
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
×	×	k?	×
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
\	\	kIx~	\
<g/>
times	times	k1gMnSc1	times
V	V	kA	V
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
požadavek	požadavek	k1gInSc1	požadavek
pak	pak	k6eAd1	pak
nakládá	nakládat	k5eAaImIp3nS	nakládat
omezení	omezení	k1gNnSc4	omezení
na	na	k7c4	na
vzhled	vzhled	k1gInSc4	vzhled
topologie	topologie	k1gFnSc2	topologie
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
τ	τ	k?	τ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
tau	tau	k1gNnSc3	tau
}	}	kIx)	}
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	V	kA	V
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Sice	sice	k8xC	sice
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
každé	každý	k3xTgInPc4	každý
dva	dva	k4xCgInPc4	dva
vektory	vektor	k1gInPc4	vektor
existují	existovat	k5eAaImIp3nP	existovat
jejich	jejich	k3xOp3gNnPc4	jejich
okolí	okolí	k1gNnPc4	okolí
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
jsou	být	k5eAaImIp3nP	být
navzájem	navzájem	k6eAd1	navzájem
disjunktní	disjunktní	k2eAgFnPc1d1	disjunktní
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
každý	každý	k3xTgInSc1	každý
vektorový	vektorový	k2eAgInSc1d1	vektorový
prostor	prostor	k1gInSc1	prostor
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
definována	definován	k2eAgFnSc1d1	definována
topologie	topologie	k1gFnSc1	topologie
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
nutně	nutně	k6eAd1	nutně
topologickým	topologický	k2eAgInSc7d1	topologický
vektorovým	vektorový	k2eAgInSc7d1	vektorový
prostorem	prostor	k1gInSc7	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Jednoduchým	jednoduchý	k2eAgInSc7d1	jednoduchý
příkladem	příklad	k1gInSc7	příklad
topologického	topologický	k2eAgInSc2d1	topologický
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
je	být	k5eAaImIp3nS	být
prostor	prostor	k1gInSc1	prostor
aritmetických	aritmetický	k2eAgInPc2d1	aritmetický
vektorů	vektor	k1gInPc2	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	v	k7c6	v
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
za	za	k7c4	za
topologii	topologie	k1gFnSc4	topologie
vezmeme	vzít	k5eAaPmIp1nP	vzít
topologii	topologie	k1gFnSc4	topologie
indukovanou	indukovaný	k2eAgFnSc7d1	indukovaná
euklidovskou	euklidovský	k2eAgFnSc7d1	euklidovská
normou	norma	k1gFnSc7	norma
<g/>
.	.	kIx.	.
</s>
<s>
Jinými	jiný	k2eAgNnPc7d1	jiné
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
okolími	okolí	k1gNnPc7	okolí
daného	daný	k2eAgInSc2d1	daný
vektoru	vektor	k1gInSc2	vektor
jsou	být	k5eAaImIp3nP	být
koule	koule	k1gFnPc1	koule
o	o	k7c6	o
jistém	jistý	k2eAgInSc6d1	jistý
poloměru	poloměr	k1gInSc6	poloměr
mající	mající	k2eAgMnPc1d1	mající
svůj	svůj	k3xOyFgInSc4	svůj
střed	střed	k1gInSc4	střed
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
vektoru	vektor	k1gInSc6	vektor
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
například	například	k6eAd1	například
(	(	kIx(	(
<g/>
otevřená	otevřený	k2eAgFnSc1d1	otevřená
<g/>
)	)	kIx)	)
koule	koule	k1gFnSc1	koule
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
B_	B_	k1gMnSc1	B_
<g/>
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
o	o	k7c6	o
(	(	kIx(	(
<g/>
kladném	kladný	k2eAgInSc6d1	kladný
<g/>
)	)	kIx)	)
poloměru	poloměr	k1gInSc6	poloměr
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
r	r	kA	r
<g/>
}	}	kIx)	}
se	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s hack="1">
středem	středem	k7c2	středem
ve	v	k7c6	v
vektoru	vektor	k1gInSc6	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
(	(	kIx(	(
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
...	...	k?	...
,	,	kIx,	,
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}	}	kIx)	}
má	mít	k5eAaImIp3nS	mít
množinový	množinový	k2eAgInSc4d1	množinový
tvar	tvar	k1gInSc4	tvar
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
=	=	kIx~	=
{	{	kIx(	{
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
∥	∥	k?	∥
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
<	<	kIx(	<
r	r	kA	r
}	}	kIx)	}
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
(	(	kIx(	(
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
y	y	k?	y
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
...	...	k?	...
,	,	kIx,	,
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
∈	∈	k?	∈
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∑	∑	k?	∑
:	:	kIx,	:
i	i	k9	i
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
y	y	k?	y
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
x	x	k?	x
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<	<	kIx(	<
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
:	:	kIx,	:
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
B_	B_	k1gMnSc1	B_
<g/>
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
{{	{{	k?	{{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g />
.	.	kIx.	.
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
<g />
.	.	kIx.	.
</s>
<s>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
<	<	kIx(	<
<g/>
r	r	kA	r
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Bigg	Bigg	k1gInSc1	Bigg
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
y_	y_	k?	y_
<g/>
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
ldots	ldots	k1gInSc1	ldots
,	,	kIx,	,
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
<g />
.	.	kIx.	.
</s>
<s>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Bigg	Bigg	k1gInSc1	Bigg
|	|	kIx~	|
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sqrt	sqrt	k5eAaPmF	sqrt
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
-x_	_	k?	-x_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
\	\	kIx~	\
<	<	kIx(	<
<g/>
\	\	kIx~	\
r	r	kA	r
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Bigg	Bigg	k1gInSc1	Bigg
\	\	kIx~	\
<g/>
}}	}}	k?	}}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Topologii	topologie	k1gFnSc4	topologie
pak	pak	k6eAd1	pak
sestrojíme	sestrojit	k5eAaPmIp1nP	sestrojit
jako	jako	k9	jako
sjednocení	sjednocení	k1gNnSc4	sjednocení
všech	všecek	k3xTgFnPc2	všecek
možných	možný	k2eAgFnPc2d1	možná
koulí	koule	k1gFnPc2	koule
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
koulí	koulet	k5eAaImIp3nP	koulet
o	o	k7c6	o
<g />
.	.	kIx.	.
</s>
<s hack="1">
všech	všecek	k3xTgNnPc2	všecek
možných	možný	k2eAgNnPc2d1	možné
(	(	kIx(	(
<g/>
nenulových	nulový	k2eNgInPc6d1	nenulový
<g/>
)	)	kIx)	)
poloměrech	poloměr	k1gInPc6	poloměr
se	se	k3xPyFc4	se
středy	středa	k1gFnPc1	středa
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
možných	možný	k2eAgInPc6d1	možný
vektorech	vektor	k1gInPc6	vektor
prostoru	prostora	k1gFnSc4	prostora
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nim	on	k3xPp3gInPc3	on
ještě	ještě	k6eAd1	ještě
musíme	muset	k5eAaImIp1nP	muset
do	do	k7c2	do
topologie	topologie	k1gFnSc2	topologie
přihodit	přihodit	k5eAaPmF	přihodit
všechny	všechen	k3xTgInPc4	všechen
možné	možný	k2eAgInPc4d1	možný
průniky	průnik	k1gInPc4	průnik
konečně	konečně	k9	konečně
mnoha	mnoho	k4c2	mnoho
libovolných	libovolný	k2eAgFnPc2d1	libovolná
koulí	koule	k1gFnPc2	koule
<g/>
,	,	kIx,	,
prázdnou	prázdný	k2eAgFnSc4d1	prázdná
množinu	množina	k1gFnSc4	množina
a	a	k8xC	a
celou	celý	k2eAgFnSc4d1	celá
množinu	množina	k1gFnSc4	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
těleso	těleso	k1gNnSc4	těleso
bereme	brát	k5eAaImIp1nP	brát
reálnou	reálný	k2eAgFnSc4d1	reálná
osu	osa	k1gFnSc4	osa
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
}	}	kIx)	}
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
topologii	topologie	k1gFnSc4	topologie
sestrojíme	sestrojit	k5eAaPmIp1nP	sestrojit
analogicky	analogicky	k6eAd1	analogicky
případu	případ	k1gInSc2	případ
výše	vysoce	k6eAd2	vysoce
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
položíme	položit	k5eAaPmIp1nP	položit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
nám	my	k3xPp1nPc3	my
otevřená	otevřený	k2eAgFnSc1d1	otevřená
koule	koule	k1gFnSc1	koule
redukuje	redukovat	k5eAaBmIp3nS	redukovat
na	na	k7c4	na
otevřený	otevřený	k2eAgInSc4d1	otevřený
interval	interval	k1gInSc4	interval
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
x	x	k?	x
)	)	kIx)	)
=	=	kIx~	=
{	{	kIx(	{
y	y	k?	y
∈	∈	k?	∈
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
y	y	k?	y
-	-	kIx~	-
x	x	k?	x
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
<	<	kIx(	<
r	r	kA	r
}	}	kIx)	}
=	=	kIx~	=
(	(	kIx(	(
x	x	k?	x
<g />
.	.	kIx.	.
</s>
<s hack="1">
-	-	kIx~	-
r	r	kA	r
,	,	kIx,	,
x	x	k?	x
+	+	kIx~	+
r	r	kA	r
)	)	kIx)	)
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
B_	B_	k1gMnSc1	B_
<g/>
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
mathbb	mathbb	k1gInSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
||	||	k?	||
<g/>
y-x	y	k?	y-x
<g/>
|	|	kIx~	|
<g/>
<	<	kIx(	<
<g/>
r	r	kA	r
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
x-r	x	k?	x-r
<g/>
,	,	kIx,	,
<g/>
x	x	k?	x
<g/>
+	+	kIx~	+
<g/>
r	r	kA	r
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Součinová	součinový	k2eAgFnSc1d1	součinová
topologie	topologie	k1gFnSc1	topologie
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
τ	τ	k?	τ
''	''	k?	''
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
tau	tau	k1gNnSc3	tau
''	''	k?	''
<g/>
}	}	kIx)	}
pro	pro	k7c4	pro
kartézský	kartézský	k2eAgInSc4d1	kartézský
součin	součin	k1gInSc4	součin
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
×	×	k?	×
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
\	\	kIx~	\
<g/>
times	times	k1gMnSc1	times
V	V	kA	V
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
tvořena	tvořit	k5eAaImNgFnS	tvořit
kartézskými	kartézský	k2eAgInPc7d1	kartézský
<g />
.	.	kIx.	.
</s>
<s hack="1">
součiny	součin	k1gInPc4	součin
koulí	koulet	k5eAaImIp3nS	koulet
z	z	k7c2	z
prostorů	prostor	k1gInPc2	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
=	=	kIx~	=
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	v	k7c6	v
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc7	jejich
konečnými	konečný	k2eAgInPc7d1	konečný
průniky	průnik	k1gInPc7	průnik
a	a	k8xC	a
libovolnými	libovolný	k2eAgNnPc7d1	libovolné
sjednoceními	sjednocení	k1gNnPc7	sjednocení
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
navíc	navíc	k6eAd1	navíc
vezmeme	vzít	k5eAaPmIp1nP	vzít
ještě	ještě	k6eAd1	ještě
prázdnou	prázdný	k2eAgFnSc4d1	prázdná
množinu	množina	k1gFnSc4	množina
a	a	k8xC	a
celou	celý	k2eAgFnSc4d1	celá
množinu	množina	k1gFnSc4	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
×	×	k?	×
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
\	\	kIx~	\
<g/>
times	times	k1gMnSc1	times
V	V	kA	V
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
pro	pro	k7c4	pro
topologii	topologie	k1gFnSc4	topologie
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
τ	τ	k?	τ
'	'	kIx"	'
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
tau	tau	k1gNnSc3	tau
'	'	kIx"	'
<g/>
}	}	kIx)	}
na	na	k7c6	na
kartézském	kartézský	k2eAgInSc6d1	kartézský
součinu	součin	k1gInSc6	součin
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
×	×	k?	×
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	v	k7c4	v
<g/>
\	\	kIx~	\
<g/>
times	times	k1gInSc4	times
V	V	kA	V
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Ukažme	ukázat	k5eAaPmRp1nP	ukázat
nejprve	nejprve	k6eAd1	nejprve
spojitost	spojitost	k1gFnSc4	spojitost
součtu	součet	k1gInSc2	součet
dvou	dva	k4xCgInPc2	dva
aritmetických	aritmetický	k2eAgInPc2d1	aritmetický
vektorů	vektor	k1gInPc2	vektor
v	v	k7c6	v
námi	my	k3xPp1nPc7	my
zavedené	zavedený	k2eAgFnSc3d1	zavedená
topologii	topologie	k1gFnSc3	topologie
<g/>
.	.	kIx.	.
</s>
<s>
Naším	náš	k3xOp1gInSc7	náš
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
ověřit	ověřit	k5eAaPmF	ověřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
kterékoliv	kterýkoliv	k3yIgInPc4	kterýkoliv
dva	dva	k4xCgInPc4	dva
vektory	vektor	k1gInPc4	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
0	[number]	k4	0
<g/>
}}	}}	k?	}}
a	a	k8xC	a
kterýkoli	kterýkoli	k3yIgInSc4	kterýkoli
kladný	kladný	k2eAgInSc4d1	kladný
poloměr	poloměr	k1gInSc4	poloměr
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
r	r	kA	r
<g/>
}	}	kIx)	}
leží	ležet	k5eAaImIp3nS	ležet
vektor	vektor	k1gInSc4	vektor
tvaru	tvar	k1gInSc2	tvar
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}}	}}}	k?	}}}
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
U	u	k7c2	u
=	=	kIx~	=
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
U	u	k7c2	u
<g/>
=	=	kIx~	=
<g/>
B_	B_	k1gFnSc2	B_
<g/>
{	{	kIx(	{
<g/>
r	r	kA	r
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
ε	ε	k?	ε
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
B_	B_	k1gFnSc1	B_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
~	~	kIx~	~
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
B_	B_	k1gFnSc1	B_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
tilde	tilde	k6eAd1	tilde
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
}}	}}	k?	}}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
pro	pro	k7c4	pro
jisté	jistý	k2eAgInPc4d1	jistý
poloměry	poloměr	k1gInPc4	poloměr
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc4	varepsilon
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
~	~	kIx~	~
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
tilde	tilde	k6eAd1	tilde
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
}}}	}}}	k?	}}}
.	.	kIx.	.
</s>
<s>
Neboli	neboli	k8xC	neboli
chceme	chtít	k5eAaImIp1nP	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
platilo	platit	k5eAaImAgNnS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
-	-	kIx~	-
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
<g />
.	.	kIx.	.
</s>
<s hack="1">
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
<	<	kIx(	<
r	r	kA	r
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
<	<	kIx(	<
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
r	r	kA	r
<g/>
}	}	kIx)	}
najdeme	najít	k5eAaPmIp1nP	najít
odpovídající	odpovídající	k2eAgFnSc4d1	odpovídající
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc4	varepsilon
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
ε	ε	k?	ε
~	~	kIx~	~
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
tilde	tilde	k6eAd1	tilde
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
}}}	}}}	k?	}}}
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
splněna	splnit	k5eAaPmNgFnS	splnit
tato	tento	k3xDgFnSc1	tento
podmínka	podmínka	k1gFnSc1	podmínka
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
můžeme	moct	k5eAaImIp1nP	moct
uzavřít	uzavřít	k5eAaPmF	uzavřít
<g/>
,	,	kIx,	,
že	že	k8xS	že
sčítání	sčítání	k1gNnSc1	sčítání
vektorů	vektor	k1gInPc2	vektor
je	být	k5eAaImIp3nS	být
spojité	spojitý	k2eAgNnSc1d1	spojité
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s hack="1">
neboť	neboť	k8xC	neboť
pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
okolí	okolí	k1gNnSc4	okolí
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
U	u	k7c2	u
<g/>
}	}	kIx)	}
součtu	součet	k1gInSc2	součet
jsme	být	k5eAaImIp1nP	být
našli	najít	k5eAaPmAgMnP	najít
odpovídající	odpovídající	k2eAgNnSc4d1	odpovídající
okolí	okolí	k1gNnSc4	okolí
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
ε	ε	k?	ε
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
×	×	k?	×
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
~	~	kIx~	~
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
B_	B_	k1gMnPc6	B_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s>
times	times	k1gMnSc1	times
B_	B_	k1gMnSc1	B_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
tilde	tilde	k6eAd1	tilde
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
}}	}}	k?	}}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
v	v	k7c6	v
součinové	součinový	k2eAgFnSc6d1	součinová
topologii	topologie	k1gFnSc6	topologie
množiny	množina	k1gFnSc2	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
×	×	k?	×
V	V	kA	V
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	v	k7c4	v
<g/>
\	\	kIx~	\
<g/>
times	times	k1gInSc4	times
V	V	kA	V
<g/>
}	}	kIx)	}
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
v	v	k7c6	v
definici	definice	k1gFnSc6	definice
spojitosti	spojitost	k1gFnSc6	spojitost
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
tím	ten	k3xDgInSc7	ten
účelem	účel	k1gInSc7	účel
však	však	k9	však
stačí	stačit	k5eAaBmIp3nS	stačit
položit	položit	k5eAaPmF	položit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
~	~	kIx~	~
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc4	varepsilon
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
tilde	tilde	k6eAd1	tilde
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}}	}}}	k?	}}}
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
měli	mít	k5eAaImAgMnP	mít
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
-	-	kIx~	-
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
∥	∥	k?	∥
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
+	+	kIx~	+
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
≤	≤	k?	≤
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
<	<	kIx(	<
ε	ε	k?	ε
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
~	~	kIx~	~
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
2	[number]	k4	2
r	r	kA	r
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
<	<	kIx(	<
r	r	kA	r
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
-	-	kIx~	-
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
<g />
.	.	kIx.	.
</s>
<s>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g />
.	.	kIx.	.
</s>
<s>
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
<g />
.	.	kIx.	.
</s>
<s>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
<	<	kIx(	<
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
tilde	tilde	k6eAd1	tilde
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}}	}}	k?	}}
<g/>
<	<	kIx(	<
<g/>
r.	r.	kA	r.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Odhadli	odhadnout	k5eAaPmAgMnP	odhadnout
jsme	být	k5eAaImIp1nP	být
tedy	tedy	k9	tedy
patřičnou	patřičný	k2eAgFnSc4d1	patřičná
normu	norma	k1gFnSc4	norma
jak	jak	k8xS	jak
jsme	být	k5eAaImIp1nP	být
měli	mít	k5eAaImAgMnP	mít
a	a	k8xC	a
ověřili	ověřit	k5eAaPmAgMnP	ověřit
jsme	být	k5eAaImIp1nP	být
tak	tak	k9	tak
spojitost	spojitost	k1gFnSc4	spojitost
sčítání	sčítání	k1gNnSc2	sčítání
vektorů	vektor	k1gInPc2	vektor
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
nyní	nyní	k6eAd1	nyní
ověřme	ověřit	k5eAaPmRp1nP	ověřit
spojitost	spojitost	k1gFnSc4	spojitost
násobení	násobení	k1gNnSc2	násobení
vektoru	vektor	k1gInSc2	vektor
číslem	číslo	k1gNnSc7	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Chceme	chtít	k5eAaImIp1nP	chtít
ukázat	ukázat	k5eAaPmF	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
násobek	násobek	k1gInSc4	násobek
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
čísla	číslo	k1gNnSc2	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
}}	}}	k?	}}
a	a	k8xC	a
vektoru	vektor	k1gInSc2	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
<g />
.	.	kIx.	.
</s>
<s hack="1">
a	a	k8xC	a
pro	pro	k7c4	pro
každé	každý	k3xTgNnSc4	každý
jeho	jeho	k3xOp3gNnSc4	jeho
okolí	okolí	k1gNnSc4	okolí
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
U	u	k7c2	u
=	=	kIx~	=
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
U	u	k7c2	u
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
=	=	kIx~	=
<g/>
B	B	kA	B
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
alpha	alph	k1gMnSc2	alph
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
najdeme	najít	k5eAaPmIp1nP	najít
okolí	okolí	k1gNnSc4	okolí
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
B	B	kA	B
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
čísla	číslo	k1gNnPc4	číslo
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
a	a	k8xC	a
okolí	okolí	k1gNnSc2	okolí
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
B	B	kA	B
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
vektoru	vektor	k1gInSc2	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
ať	ať	k9	ať
vynásobím	vynásobit	k5eAaPmIp1nS	vynásobit
libovolné	libovolný	k2eAgNnSc4d1	libovolné
číslo	číslo	k1gNnSc4	číslo
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
B	B	kA	B
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
s	s	k7c7	s
libovolným	libovolný	k2eAgInSc7d1	libovolný
vektorem	vektor	k1gInSc7	vektor
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
B	B	kA	B
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
,	,	kIx,	,
tak	tak	k6eAd1	tak
dostanu	dostat	k5eAaPmIp1nS	dostat
opět	opět	k6eAd1	opět
vektor	vektor	k1gInSc4	vektor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
U	u	k7c2	u
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Jinými	jiný	k2eAgNnPc7d1	jiné
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
mějme	mít	k5eAaImRp1nP	mít
kouli	koule	k1gFnSc4	koule
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
U	u	k7c2	u
=	=	kIx~	=
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
U	u	k7c2	u
<g/>
=	=	kIx~	=
<g/>
B_	B_	k1gFnSc2	B_
<g/>
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
se	s	k7c7	s
středem	střed	k1gInSc7	střed
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
a	a	k8xC	a
poloměrem	poloměr	k1gInSc7	poloměr
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
r	r	kA	r
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Chceme	chtít	k5eAaImIp1nP	chtít
najít	najít	k5eAaPmF	najít
poloměr	poloměr	k1gInSc4	poloměr
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
}	}	kIx)	}
koule	koule	k1gFnSc1	koule
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
ε	ε	k?	ε
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
B_	B_	k1gMnPc6	B_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
se	s	k7c7	s
středem	střed	k1gInSc7	střed
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
a	a	k8xC	a
poloměr	poloměr	k1gInSc4	poloměr
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
~	~	kIx~	~
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
tilde	tilde	k6eAd1	tilde
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
}}}	}}}	k?	}}}
koule	koule	k1gFnSc1	koule
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
~	~	kIx~	~
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
B_	B_	k1gMnPc6	B_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
tilde	tilde	k6eAd1	tilde
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
}}	}}	k?	}}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
se	s	k7c7	s
<g />
.	.	kIx.	.
</s>
<s hack="1">
středem	středem	k7c2	středem
v	v	k7c6	v
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}}	}}	k?	}}
tak	tak	k6eAd1	tak
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
libovolný	libovolný	k2eAgInSc1d1	libovolný
vektor	vektor	k1gInSc1	vektor
tvaru	tvar	k1gInSc2	tvar
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
ležel	ležet	k5eAaImAgMnS	ležet
v	v	k7c6	v
množině	množina	k1gFnSc6	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
U	u	k7c2	u
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
U	u	k7c2	u
<g/>
}	}	kIx)	}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
∈	∈	k?	∈
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
ε	ε	k?	ε
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
\	\	kIx~	\
<g/>
in	in	k?	in
B_	B_	k1gFnSc1	B_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
∈	∈	k?	∈
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
~	~	kIx~	~
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
B_	B_	k1gFnSc1	B_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
tilde	tilde	k6eAd1	tilde
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
}}	}}	k?	}}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
použitím	použití	k1gNnSc7	použití
vlastností	vlastnost	k1gFnPc2	vlastnost
normy	norma	k1gFnSc2	norma
můžeme	moct	k5eAaImIp1nP	moct
odhadnout	odhadnout	k5eAaPmF	odhadnout
seshora	seshora	k6eAd1	seshora
výraz	výraz	k1gInSc4	výraz
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
následovně	následovně	k6eAd1	následovně
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
α	α	k?	α
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
∥	∥	k?	∥
(	(	kIx(	(
α	α	k?	α
-	-	kIx~	-
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
+	+	kIx~	+
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
0	[number]	k4	0
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
+	+	kIx~	+
(	(	kIx(	(
α	α	k?	α
-	-	kIx~	-
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
≤	≤	k?	≤
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
α	α	k?	α
-	-	kIx~	-
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
2	[number]	k4	2
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
-	-	kIx~	-
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
α	α	k?	α
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
≤	≤	k?	≤
:	:	kIx,	:
ε	ε	k?	ε
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
~	~	kIx~	~
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
~	~	kIx~	~
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
ε	ε	k?	ε
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s>
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
leq	leq	k?	leq
\	\	kIx~	\
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g />
.	.	kIx.	.
</s>
<s>
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
leq	leq	k?	leq
\	\	kIx~	\
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
tilde	tilde	k6eAd1	tilde
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
|	|	kIx~	|
<g/>
<g />
.	.	kIx.	.
</s>
<s>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
tilde	tilde	k6eAd1	tilde
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
kde	kde	k6eAd1	kde
jsme	být	k5eAaImIp1nP	být
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
nerovnosti	nerovnost	k1gFnSc6	nerovnost
využili	využít	k5eAaPmAgMnP	využít
definic	definice	k1gFnPc2	definice
příslušných	příslušný	k2eAgNnPc2d1	příslušné
okolí	okolí	k1gNnPc2	okolí
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
jsou	být	k5eAaImIp3nP	být
specifikována	specifikován	k2eAgFnSc1d1	specifikována
výše	výše	k1gFnSc1	výše
<g/>
.	.	kIx.	.
</s>
<s>
Diskutujme	diskutovat	k5eAaImRp1nP	diskutovat
nyní	nyní	k6eAd1	nyní
dva	dva	k4xCgInPc4	dva
případy	případ	k1gInPc4	případ
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
prvé	prvý	k4xOgFnPc4	prvý
<g/>
,	,	kIx,	,
když	když	k8xS	když
platí	platit	k5eAaImIp3nS	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
≥	≥	k?	≥
:	:	kIx,	:
6	[number]	k4	6
∥	∥	k?	∥
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
r	r	kA	r
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
geq	geq	k?	geq
\	\	kIx~	\
6	[number]	k4	6
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
r	r	kA	r
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
poloměr	poloměr	k1gInSc4	poloměr
okolí	okolí	k1gNnSc2	okolí
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
U	u	k7c2	u
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
stačí	stačit	k5eAaBmIp3nS	stačit
položit	položit	k5eAaPmF	položit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
=	=	kIx~	=
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc4	varepsilon
=	=	kIx~	=
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
~	~	kIx~	~
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
</s>
<s>
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
tilde	tilde	k6eAd1	tilde
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g />
.	.	kIx.	.
</s>
<s hack="1">
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
~	~	kIx~	~
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
~	~	kIx~	~
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
ε	ε	k?	ε
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
3	[number]	k4	3
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
≤	≤	k?	≤
6	[number]	k4	6
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
≤	≤	k?	≤
r	r	kA	r
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc4	varepsilon
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
tilde	tilde	k6eAd1	tilde
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
tilde	tilde	k6eAd1	tilde
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g />
.	.	kIx.	.
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc4	varepsilon
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
3	[number]	k4	3
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
6	[number]	k4	6
<g/>
|	|	kIx~	|
<g/>
<g />
.	.	kIx.	.
</s>
<s>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
<g />
.	.	kIx.	.
</s>
<s hack="1">
r.	r.	kA	r.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Ukázali	ukázat	k5eAaPmAgMnP	ukázat
jsme	být	k5eAaImIp1nP	být
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
≥	≥	k?	≥
:	:	kIx,	:
6	[number]	k4	6
∥	∥	k?	∥
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
r	r	kA	r
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
geq	geq	k?	geq
\	\	kIx~	\
6	[number]	k4	6
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
,	,	kIx,	,
tak	tak	k6eAd1	tak
jsme	být	k5eAaImIp1nP	být
našli	najít	k5eAaPmAgMnP	najít
poloměry	poloměr	k1gInPc7	poloměr
okolí	okolí	k1gNnSc1	okolí
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
ε	ε	k?	ε
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
B_	B_	k1gMnSc6	B_
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
a	a	k8xC	a
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
~	~	kIx~	~
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
B_	B_	k1gMnPc6	B_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
tilde	tilde	k6eAd1	tilde
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
}}	}}	k?	}}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyhovují	vyhovovat	k5eAaImIp3nP	vyhovovat
definici	definice	k1gFnSc4	definice
spojitosti	spojitost	k1gFnSc2	spojitost
násobení	násobení	k1gNnSc4	násobení
vektoru	vektor	k1gInSc2	vektor
číslem	číslo	k1gNnSc7	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Podívejme	podívat	k5eAaPmRp1nP	podívat
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
na	na	k7c4	na
případ	případ	k1gInSc4	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
<	<	kIx(	<
:	:	kIx,	:
6	[number]	k4	6
∥	∥	k?	∥
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
<g />
.	.	kIx.	.
</s>
<s hack="1">
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
r	r	kA	r
<g/>
\	\	kIx~	\
<	<	kIx(	<
<g/>
\	\	kIx~	\
6	[number]	k4	6
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
můžeme	moct	k5eAaImIp1nP	moct
položit	položit	k5eAaPmF	položit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
6	[number]	k4	6
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
~	~	kIx~	~
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
6	[number]	k4	6
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc4	varepsilon
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s>
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
quad	quad	k6eAd1	quad
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
tilde	tilde	k6eAd1	tilde
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
}}	}}	k?	}}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
}	}	kIx)	}
:	:	kIx,	:
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
r	r	kA	r
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
poloměr	poloměr	k1gInSc4	poloměr
okolí	okolí	k1gNnSc2	okolí
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
U	u	k7c2	u
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
U	u	k7c2	u
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Dostáváme	dostávat	k5eAaImIp1nP	dostávat
tak	tak	k6eAd1	tak
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
~	~	kIx~	~
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
~	~	kIx~	~
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
ε	ε	k?	ε
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
36	[number]	k4	36
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
|	|	kIx~	|
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
6	[number]	k4	6
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
6	[number]	k4	6
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
6	[number]	k4	6
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
6	[number]	k4	6
∥	∥	k?	∥
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc4	varepsilon
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
tilde	tilde	k6eAd1	tilde
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
tilde	tilde	k6eAd1	tilde
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g />
.	.	kIx.	.
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc4	varepsilon
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
<g />
.	.	kIx.	.
</s>
<s>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
{	{	kIx(	{
<g/>
36	[number]	k4	36
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
}}	}}	k?	}}
<g/>
<g />
.	.	kIx.	.
</s>
<s>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Bigg	Bigg	k1gInSc1	Bigg
(	(	kIx(	(
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g />
.	.	kIx.	.
</s>
<s>
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
Bigg	Bigga	k1gFnPc2	Bigga
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
.	.	kIx.	.
<g/>
}	}	kIx)	}
:	:	kIx,	:
Protože	protože	k8xS	protože
řešíme	řešit	k5eAaImIp1nP	řešit
případ	případ	k1gInSc4	případ
pro	pro	k7c4	pro
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
<	<	kIx(	<
:	:	kIx,	:
6	[number]	k4	6
∥	∥	k?	∥
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
r	r	kA	r
<g/>
\	\	kIx~	\
<	<	kIx(	<
<g/>
\	\	kIx~	\
6	[number]	k4	6
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
první	první	k4xOgInSc4	první
člen	člen	k1gInSc4	člen
v	v	k7c6	v
závorce	závorka	k1gFnSc6	závorka
odhadnou	odhadnout	k5eAaPmIp3nP	odhadnout
seshora	seshora	k6eAd1	seshora
jedničkou	jednička	k1gFnSc7	jednička
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
dostali	dostat	k5eAaPmAgMnP	dostat
výraz	výraz	k1gInSc4	výraz
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
6	[number]	k4	6
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
6	[number]	k4	6
∥	∥	k?	∥
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
<	<	kIx(	<
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
6	[number]	k4	6
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
1	[number]	k4	1
+	+	kIx~	+
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
<	<	kIx(	<
r	r	kA	r
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Bigg	Bigg	k1gInSc1	Bigg
(	(	kIx(	(
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
<g />
.	.	kIx.	.
</s>
<s>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}	}}}	k?	}}}
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
<g />
.	.	kIx.	.
</s>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Bigg	Bigg	k1gInSc1	Bigg
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
<	<	kIx(	<
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Bigg	Bigg	k1gInSc1	Bigg
(	(	kIx(	(
<g/>
}	}	kIx)	}
<g/>
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
Bigg	Bigg	k1gInSc1	Bigg
)	)	kIx)	)
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gInSc1	frac
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
<g/>
<	<	kIx(	<
<g/>
r.	r.	kA	r.
<g/>
}	}	kIx)	}
:	:	kIx,	:
V	v	k7c6	v
případě	případ	k1gInSc6	případ
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
<	<	kIx(	<
:	:	kIx,	:
6	[number]	k4	6
∥	∥	k?	∥
:	:	kIx,	:
α	α	k?	α
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
0	[number]	k4	0
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
∥	∥	k?	∥
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
r	r	kA	r
<g/>
\	\	kIx~	\
<	<	kIx(	<
<g/>
\	\	kIx~	\
6	[number]	k4	6
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
\	\	kIx~	\
<g/>
alpha	alpha	k1gFnSc1	alpha
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
|	|	kIx~	|
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
jsme	být	k5eAaImIp1nP	být
tedy	tedy	k9	tedy
též	též	k9	též
našli	najít	k5eAaPmAgMnP	najít
poloměry	poloměr	k1gInPc4	poloměr
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
ε	ε	k?	ε
~	~	kIx~	~
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
tilde	tilde	k6eAd1	tilde
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc4	varepsilon
}}}	}}}	k?	}}}
daných	daný	k2eAgFnPc2d1	daná
okolí	okolí	k1gNnSc4	okolí
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
splněna	splněn	k2eAgFnSc1d1	splněna
podmínka	podmínka	k1gFnSc1	podmínka
spojitosti	spojitost	k1gFnSc2	spojitost
<g/>
.	.	kIx.	.
</s>
<s>
Ověřili	ověřit	k5eAaPmAgMnP	ověřit
jsme	být	k5eAaImIp1nP	být
tak	tak	k9	tak
platnost	platnost	k1gFnSc1	platnost
druhé	druhý	k4xOgFnSc2	druhý
definiční	definiční	k2eAgFnSc2d1	definiční
podmínky	podmínka	k1gFnSc2	podmínka
topologického	topologický	k2eAgInSc2d1	topologický
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Prostor	prostor	k1gInSc1	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	v	k7c6	v
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
je	být	k5eAaImIp3nS	být
zajisté	zajisté	k9	zajisté
Hausdorffův	Hausdorffův	k2eAgMnSc1d1	Hausdorffův
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
<g />
.	.	kIx.	.
</s>
<s hack="1">
pro	pro	k7c4	pro
každé	každý	k3xTgInPc4	každý
dva	dva	k4xCgInPc4	dva
vektory	vektor	k1gInPc4	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}}	}}}	k?	}}}
jsme	být	k5eAaImIp1nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
zjistit	zjistit	k5eAaPmF	zjistit
jejich	jejich	k3xOp3gFnSc4	jejich
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
pomocí	pomocí	k7c2	pomocí
Euklidovy	Euklidův	k2eAgFnSc2d1	Euklidova
normy	norma	k1gFnSc2	norma
<g/>
,	,	kIx,	,
označme	označit	k5eAaPmRp1nP	označit
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
d	d	k?	d
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
pak	pak	k6eAd1	pak
vezmu	vzít	k5eAaPmIp1nS	vzít
kouli	koule	k1gFnSc4	koule
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
B_	B_	k1gMnSc1	B_
<g/>
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
o	o	k7c6	o
poloměru	poloměr	k1gInSc6	poloměr
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
r	r	kA	r
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
d	d	k?	d
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
r	r	kA	r
<g/>
=	=	kIx~	=
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
d	d	k?	d
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
3	[number]	k4	3
<g/>
}}}	}}}	k?	}}}
a	a	k8xC	a
kouli	koule	k1gFnSc4	koule
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
B	B	kA	B
:	:	kIx,	:
r	r	kA	r
:	:	kIx,	:
:	:	kIx,	:
(	(	kIx(	(
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
)	)	kIx)	)
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
B_	B_	k1gMnSc1	B_
<g/>
{	{	kIx(	{
<g/>
r	r	kA	r
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}	}}	k?	}}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
o	o	k7c6	o
témže	týž	k3xTgInSc6	týž
poloměru	poloměr	k1gInSc6	poloměr
<g/>
,	,	kIx,	,
tak	tak	k9	tak
tyto	tento	k3xDgFnPc1	tento
dvě	dva	k4xCgFnPc1	dva
koule	koule	k1gFnPc1	koule
tvoří	tvořit	k5eAaImIp3nP	tvořit
okolí	okolí	k1gNnSc4	okolí
vektoru	vektor	k1gInSc2	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
x	x	k?	x
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}}	}}}	k?	}}}
a	a	k8xC	a
vektoru	vektor	k1gInSc2	vektor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
y	y	k?	y
→	→	k?	→
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
y	y	k?	y
<g/>
}}}	}}}	k?	}}}
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
přitom	přitom	k6eAd1	přitom
disjunktní	disjunktní	k2eAgFnPc1d1	disjunktní
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
splněn	splnit	k5eAaPmNgInS	splnit
i	i	k9	i
třetí	třetí	k4xOgInSc1	třetí
požadavek	požadavek	k1gInSc1	požadavek
a	a	k8xC	a
můžeme	moct	k5eAaImIp1nP	moct
uzavřít	uzavřít	k5eAaPmF	uzavřít
<g/>
,	,	kIx,	,
že	že	k8xS	že
prostor	prostor	k1gInSc1	prostor
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
V	V	kA	V
=	=	kIx~	=
:	:	kIx,	:
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
V	v	k7c6	v
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
^	^	kIx~	^
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
nad	nad	k7c7	nad
tělesem	těleso	k1gNnSc7	těleso
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
T	T	kA	T
=	=	kIx~	=
:	:	kIx,	:
R	R	kA	R
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
scriptstyle	scriptstyl	k1gInSc5	scriptstyl
T	T	kA	T
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
}	}	kIx)	}
s	s	k7c7	s
přirozeně	přirozeně	k6eAd1	přirozeně
zavedenou	zavedený	k2eAgFnSc7d1	zavedená
topologií	topologie	k1gFnSc7	topologie
je	být	k5eAaImIp3nS	být
topologickým	topologický	k2eAgInSc7d1	topologický
vektorovým	vektorový	k2eAgInSc7d1	vektorový
prostorem	prostor	k1gInSc7	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Zdaleka	zdaleka	k6eAd1	zdaleka
nejčastěji	často	k6eAd3	často
používanými	používaný	k2eAgInPc7d1	používaný
vektorovými	vektorový	k2eAgInPc7d1	vektorový
prostory	prostor	k1gInPc7	prostor
jsou	být	k5eAaImIp3nP	být
ty	ten	k3xDgFnPc1	ten
definované	definovaný	k2eAgFnPc1d1	definovaná
nad	nad	k7c7	nad
tělesem	těleso	k1gNnSc7	těleso
reálných	reálný	k2eAgFnPc2d1	reálná
či	či	k8xC	či
komplexních	komplexní	k2eAgFnPc2d1	komplexní
čísel	číslo	k1gNnPc2	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
však	však	k9	však
uvažovat	uvažovat	k5eAaImF	uvažovat
i	i	k9	i
jiná	jiný	k2eAgNnPc4d1	jiné
než	než	k8xS	než
tato	tento	k3xDgNnPc1	tento
tělesa	těleso	k1gNnPc1	těleso
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
ani	ani	k9	ani
nemusí	muset	k5eNaImIp3nP	muset
mít	mít	k5eAaImF	mít
nekonečnou	konečný	k2eNgFnSc4d1	nekonečná
mohutnost	mohutnost	k1gFnSc4	mohutnost
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
případem	případ	k1gInSc7	případ
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
je	být	k5eAaImIp3nS	být
vektorový	vektorový	k2eAgInSc1d1	vektorový
prostor	prostor	k1gInSc1	prostor
definovaný	definovaný	k2eAgInSc1d1	definovaný
nad	nad	k7c7	nad
konečným	konečný	k2eAgNnSc7d1	konečné
tělesem	těleso	k1gNnSc7	těleso
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
před	před	k7c7	před
zavedením	zavedení	k1gNnSc7	zavedení
pojmu	pojem	k1gInSc2	pojem
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgInS	objevit
koncept	koncept	k1gInSc1	koncept
vektoru	vektor	k1gInSc2	vektor
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
pomocí	pomoc	k1gFnSc7	pomoc
popisuje	popisovat	k5eAaImIp3nS	popisovat
působení	působení	k1gNnSc1	působení
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
momentů	moment	k1gInPc2	moment
sil	síla	k1gFnPc2	síla
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
veličin	veličina	k1gFnPc2	veličina
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
jejichž	jejichž	k3xOyRp3gNnSc4	jejichž
určení	určení	k1gNnSc4	určení
je	být	k5eAaImIp3nS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
nejen	nejen	k6eAd1	nejen
jejich	jejich	k3xOp3gFnSc4	jejich
velikost	velikost	k1gFnSc4	velikost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
směr	směr	k1gInSc4	směr
působení	působení	k1gNnSc2	působení
<g/>
.	.	kIx.	.
</s>
<s>
Vektor	vektor	k1gInSc1	vektor
samotný	samotný	k2eAgInSc1d1	samotný
pak	pak	k6eAd1	pak
obvykle	obvykle	k6eAd1	obvykle
vyjadřujeme	vyjadřovat	k5eAaImIp1nP	vyjadřovat
jako	jako	k9	jako
šipku	šipka	k1gFnSc4	šipka
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
směr	směr	k1gInSc4	směr
udává	udávat	k5eAaImIp3nS	udávat
směr	směr	k1gInSc1	směr
působení	působení	k1gNnSc2	působení
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
délka	délka	k1gFnSc1	délka
velikost	velikost	k1gFnSc1	velikost
působící	působící	k2eAgFnSc2d1	působící
veličiny	veličina	k1gFnSc2	veličina
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
se	se	k3xPyFc4	se
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
tématu	téma	k1gNnSc6	téma
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
článek	článek	k1gInSc1	článek
Vektor	vektor	k1gInSc1	vektor
<g/>
.	.	kIx.	.
</s>
<s>
Jistým	jistý	k2eAgNnSc7d1	jisté
zobecněním	zobecnění	k1gNnSc7	zobecnění
vektoru	vektor	k1gInSc2	vektor
je	být	k5eAaImIp3nS	být
pojem	pojem	k1gInSc1	pojem
tenzoru	tenzor	k1gInSc2	tenzor
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc4	ten
lze	lze	k6eAd1	lze
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
pomocí	pomocí	k7c2	pomocí
dvourozměrného	dvourozměrný	k2eAgInSc2d1	dvourozměrný
lineárního	lineární	k2eAgInSc2d1	lineární
objektu	objekt	k1gInSc2	objekt
-	-	kIx~	-
matice	matice	k1gFnSc1	matice
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
např.	např.	kA	např.
tenzor	tenzor	k1gInSc4	tenzor
momentu	moment	k1gInSc2	moment
setrvačnosti	setrvačnost	k1gFnSc2	setrvačnost
<g/>
,	,	kIx,	,
tenzor	tenzor	k1gInSc1	tenzor
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
pole	pole	k1gNnSc2	pole
atd.	atd.	kA	atd.
S	s	k7c7	s
tenzory	tenzor	k1gInPc7	tenzor
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
setkat	setkat	k5eAaPmF	setkat
kromě	kromě	k7c2	kromě
mechaniky	mechanika	k1gFnSc2	mechanika
např.	např.	kA	např.
i	i	k8xC	i
v	v	k7c6	v
obecné	obecný	k2eAgFnSc6d1	obecná
teorii	teorie	k1gFnSc6	teorie
relativity	relativita	k1gFnSc2	relativita
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
dalším	další	k2eAgInSc7d1	další
příkladem	příklad	k1gInSc7	příklad
využití	využití	k1gNnSc2	využití
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
hojně	hojně	k6eAd1	hojně
setkat	setkat	k5eAaPmF	setkat
v	v	k7c6	v
kvantové	kvantový	k2eAgFnSc6d1	kvantová
mechanice	mechanika	k1gFnSc6	mechanika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
s	s	k7c7	s
jeho	jeho	k3xOp3gFnSc7	jeho
pomocí	pomoc	k1gFnSc7	pomoc
popisuje	popisovat	k5eAaImIp3nS	popisovat
stav	stav	k1gInSc1	stav
částice	částice	k1gFnSc2	částice
či	či	k8xC	či
jiného	jiný	k2eAgInSc2d1	jiný
fyzikálního	fyzikální	k2eAgInSc2d1	fyzikální
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
velkého	velký	k2eAgInSc2d1	velký
významu	význam	k1gInSc2	význam
nabývají	nabývat	k5eAaImIp3nP	nabývat
tzv.	tzv.	kA	tzv.
Lp	Lp	k1gFnSc4	Lp
prostory	prostora	k1gFnSc2	prostora
integrabilních	integrabilní	k2eAgFnPc2d1	integrabilní
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Fyzikální	fyzikální	k2eAgInSc1d1	fyzikální
stav	stav	k1gInSc1	stav
systému	systém	k1gInSc2	systém
lze	lze	k6eAd1	lze
popsat	popsat	k5eAaPmF	popsat
jako	jako	k9	jako
komplexní	komplexní	k2eAgFnSc4d1	komplexní
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
lze	lze	k6eAd1	lze
opět	opět	k6eAd1	opět
chápat	chápat	k5eAaImF	chápat
jako	jako	k8xS	jako
vektor	vektor	k1gInSc4	vektor
<g/>
.	.	kIx.	.
</s>
<s>
Fyzikální	fyzikální	k2eAgFnPc1d1	fyzikální
veličiny	veličina	k1gFnPc1	veličina
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
axiomy	axiom	k1gInPc7	axiom
kvantové	kvantový	k2eAgFnSc2d1	kvantová
mechaniky	mechanika	k1gFnSc2	mechanika
vyjádřeny	vyjádřen	k2eAgInPc1d1	vyjádřen
jako	jako	k8xS	jako
lineární	lineární	k2eAgInPc1d1	lineární
operátory	operátor	k1gInPc1	operátor
působící	působící	k2eAgInPc1d1	působící
na	na	k7c6	na
těchto	tento	k3xDgInPc6	tento
vektorech	vektor	k1gInPc6	vektor
<g/>
.	.	kIx.	.
</s>
<s>
Značná	značný	k2eAgFnSc1d1	značná
obliba	obliba	k1gFnSc1	obliba
lineárních	lineární	k2eAgInPc2d1	lineární
objektů	objekt	k1gInPc2	objekt
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
vyvěrá	vyvěrat	k5eAaImIp3nS	vyvěrat
z	z	k7c2	z
faktu	fakt	k1gInSc2	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
k	k	k7c3	k
popisu	popis	k1gInSc3	popis
přírodních	přírodní	k2eAgInPc2d1	přírodní
procesů	proces	k1gInPc2	proces
se	se	k3xPyFc4	se
dosti	dosti	k6eAd1	dosti
často	často	k6eAd1	často
používají	používat	k5eAaImIp3nP	používat
rovnice	rovnice	k1gFnPc1	rovnice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
obtížně	obtížně	k6eAd1	obtížně
řešitelné	řešitelný	k2eAgFnPc1d1	řešitelná
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
namísto	namísto	k7c2	namísto
s	s	k7c7	s
přesným	přesný	k2eAgNnSc7d1	přesné
analytickým	analytický	k2eAgNnSc7d1	analytické
řešením	řešení	k1gNnSc7	řešení
spokojíme	spokojit	k5eAaPmIp1nP	spokojit
s	s	k7c7	s
alespoň	alespoň	k9	alespoň
přibližným	přibližný	k2eAgNnSc7d1	přibližné
řešením	řešení	k1gNnSc7	řešení
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
nám	my	k3xPp1nPc3	my
kvůli	kvůli	k7c3	kvůli
obtížnosti	obtížnost	k1gFnSc3	obtížnost
úlohy	úloha	k1gFnSc2	úloha
ani	ani	k8xC	ani
nic	nic	k3yNnSc1	nic
jiného	jiný	k2eAgNnSc2d1	jiné
nezbývá	zbývat	k5eNaImIp3nS	zbývat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
nejjednodušším	jednoduchý	k2eAgInSc7d3	nejjednodušší
možným	možný	k2eAgInSc7d1	možný
způsobem	způsob	k1gInSc7	způsob
je	být	k5eAaImIp3nS	být
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
řešením	řešení	k1gNnSc7	řešení
je	být	k5eAaImIp3nS	být
lineární	lineární	k2eAgInSc1d1	lineární
objekt	objekt	k1gInSc1	objekt
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc4	ten
dosadíme	dosadit	k5eAaPmIp1nP	dosadit
do	do	k7c2	do
rovnice	rovnice	k1gFnSc2	rovnice
popisující	popisující	k2eAgInSc4d1	popisující
přírodní	přírodní	k2eAgInSc4d1	přírodní
proces	proces	k1gInSc4	proces
a	a	k8xC	a
snažíme	snažit	k5eAaImIp1nP	snažit
se	se	k3xPyFc4	se
najít	najít	k5eAaPmF	najít
takový	takový	k3xDgInSc1	takový
tvar	tvar	k1gInSc1	tvar
lineárního	lineární	k2eAgInSc2d1	lineární
objektu	objekt	k1gInSc2	objekt
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
odchylka	odchylka	k1gFnSc1	odchylka
od	od	k7c2	od
přesného	přesný	k2eAgNnSc2d1	přesné
řešení	řešení	k1gNnSc2	řešení
minimální	minimální	k2eAgFnSc1d1	minimální
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
nalezení	nalezení	k1gNnSc2	nalezení
přibližného	přibližný	k2eAgNnSc2d1	přibližné
řešení	řešení	k1gNnSc2	řešení
funguje	fungovat	k5eAaImIp3nS	fungovat
překvapivě	překvapivě	k6eAd1	překvapivě
často	často	k6eAd1	často
<g/>
.	.	kIx.	.
</s>
<s>
Lineární	lineární	k2eAgInPc1d1	lineární
objekty	objekt	k1gInPc1	objekt
mají	mít	k5eAaImIp3nP	mít
tu	ten	k3xDgFnSc4	ten
výhodu	výhoda	k1gFnSc4	výhoda
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
snadno	snadno	k6eAd1	snadno
pracuje	pracovat	k5eAaImIp3nS	pracovat
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gFnPc4	on
vybudována	vybudován	k2eAgFnSc1d1	vybudována
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
matematická	matematický	k2eAgFnSc1d1	matematická
teorie	teorie	k1gFnSc1	teorie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kvantové	kvantový	k2eAgFnSc6d1	kvantová
mechanice	mechanika	k1gFnSc6	mechanika
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
lineární	lineární	k2eAgFnSc1d1	lineární
struktura	struktura	k1gFnSc1	struktura
fyzikálního	fyzikální	k2eAgInSc2d1	fyzikální
světa	svět	k1gInSc2	svět
rovnou	rovnou	k6eAd1	rovnou
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
ukotvena	ukotven	k2eAgFnSc1d1	ukotvena
v	v	k7c6	v
axiomech	axiom	k1gInPc6	axiom
kvantové	kvantový	k2eAgFnSc2d1	kvantová
mechaniky	mechanika	k1gFnSc2	mechanika
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
fyziky	fyzika	k1gFnSc2	fyzika
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
pojem	pojem	k1gInSc1	pojem
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
či	či	k8xC	či
obecněji	obecně	k6eAd2	obecně
lineární	lineární	k2eAgFnSc2d1	lineární
algebry	algebra	k1gFnSc2	algebra
jako	jako	k8xS	jako
celku	celek	k1gInSc2	celek
nezastupitelné	zastupitelný	k2eNgNnSc4d1	nezastupitelné
místo	místo	k1gNnSc4	místo
třeba	třeba	k6eAd1	třeba
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
numerické	numerický	k2eAgFnSc2d1	numerická
matematiky	matematika	k1gFnSc2	matematika
či	či	k8xC	či
informatiky	informatika	k1gFnSc2	informatika
<g/>
.	.	kIx.	.
</s>
<s>
Lineární	lineární	k2eAgFnSc1d1	lineární
algebra	algebra	k1gFnSc1	algebra
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
rámec	rámec	k1gInSc4	rámec
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
různé	různý	k2eAgInPc4d1	různý
výpočetní	výpočetní	k2eAgInPc4d1	výpočetní
problémy	problém	k1gInPc4	problém
formulovat	formulovat	k5eAaImF	formulovat
elegantním	elegantní	k2eAgInSc7d1	elegantní
a	a	k8xC	a
přehledným	přehledný	k2eAgInSc7d1	přehledný
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
teorie	teorie	k1gFnSc1	teorie
matic	matice	k1gFnPc2	matice
nalézá	nalézat	k5eAaImIp3nS	nalézat
v	v	k7c6	v
numerické	numerický	k2eAgFnSc6d1	numerická
matematice	matematika	k1gFnSc6	matematika
široké	široký	k2eAgNnSc4d1	široké
uplatnění	uplatnění	k1gNnSc4	uplatnění
<g/>
.	.	kIx.	.
</s>
<s>
Koncept	koncept	k1gInSc1	koncept
vektorového	vektorový	k2eAgInSc2d1	vektorový
prostoru	prostor	k1gInSc2	prostor
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
nalézt	nalézt	k5eAaBmF	nalézt
i	i	k9	i
v	v	k7c6	v
teoretičtějších	teoretický	k2eAgFnPc6d2	teoretičtější
partiích	partie	k1gFnPc6	partie
matematiky	matematika	k1gFnSc2	matematika
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
za	za	k7c4	za
všechny	všechen	k3xTgFnPc4	všechen
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
tečný	tečný	k2eAgInSc4d1	tečný
prostor	prostor	k1gInSc4	prostor
zavedený	zavedený	k2eAgInSc4d1	zavedený
na	na	k7c6	na
fibrovaném	fibrovaný	k2eAgInSc6d1	fibrovaný
prostoru	prostor	k1gInSc6	prostor
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
struktura	struktura	k1gFnSc1	struktura
studovaná	studovaný	k2eAgFnSc1d1	studovaná
odvětvím	odvětvit	k5eAaPmIp1nS	odvětvit
diferenciální	diferenciální	k2eAgFnSc1d1	diferenciální
geometrie	geometrie	k1gFnSc1	geometrie
<g/>
.	.	kIx.	.
</s>
