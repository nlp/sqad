<s>
Norek	norek	k1gMnSc1	norek
evropský	evropský	k2eAgMnSc1d1	evropský
(	(	kIx(	(
<g/>
Mustela	Mustela	k1gFnSc1	Mustela
lutreola	lutreola	k1gFnSc1	lutreola
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velký	velký	k2eAgMnSc1d1	velký
zástupce	zástupce	k1gMnSc1	zástupce
čeledi	čeleď	k1gFnSc2	čeleď
lasicovitých	lasicovitý	k2eAgInPc2d1	lasicovitý
obývající	obývající	k2eAgInPc4d1	obývající
evropské	evropský	k2eAgInPc4d1	evropský
lesy	les	k1gInPc4	les
v	v	k7c6	v
části	část	k1gFnSc6	část
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
,	,	kIx,	,
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
,	,	kIx,	,
Estonska	Estonsko	k1gNnSc2	Estonsko
a	a	k8xC	a
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
