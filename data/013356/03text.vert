<p>
<s>
Norek	norek	k1gMnSc1	norek
evropský	evropský	k2eAgMnSc1d1	evropský
(	(	kIx(	(
<g/>
Mustela	Mustela	k1gFnSc1	Mustela
lutreola	lutreola	k1gFnSc1	lutreola
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velký	velký	k2eAgMnSc1d1	velký
zástupce	zástupce	k1gMnSc1	zástupce
čeledi	čeleď	k1gFnSc2	čeleď
lasicovitých	lasicovitý	k2eAgInPc2d1	lasicovitý
obývající	obývající	k2eAgInPc4d1	obývající
evropské	evropský	k2eAgInPc4d1	evropský
lesy	les	k1gInPc4	les
v	v	k7c6	v
části	část	k1gFnSc6	část
Španělska	Španělsko	k1gNnSc2	Španělsko
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
,	,	kIx,	,
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
,	,	kIx,	,
Estonska	Estonsko	k1gNnSc2	Estonsko
a	a	k8xC	a
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
vyhynul	vyhynout	k5eAaPmAgInS	vyhynout
už	už	k6eAd1	už
koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Dříve	dříve	k6eAd2	dříve
žil	žít	k5eAaImAgMnS	žít
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
území	území	k1gNnSc2	území
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
byl	být	k5eAaImAgMnS	být
poslední	poslední	k2eAgMnSc1d1	poslední
norek	norek	k1gMnSc1	norek
zastřelen	zastřelit	k5eAaPmNgMnS	zastřelit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
u	u	k7c2	u
Jindřichova	Jindřichův	k2eAgInSc2d1	Jindřichův
Hradce	Hradec	k1gInSc2	Hradec
a	a	k8xC	a
uložen	uložit	k5eAaPmNgMnS	uložit
do	do	k7c2	do
sbírek	sbírka	k1gFnPc2	sbírka
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Mnohde	mnohde	k6eAd1	mnohde
jej	on	k3xPp3gMnSc4	on
nahradil	nahradit	k5eAaPmAgMnS	nahradit
norek	norek	k1gMnSc1	norek
americký	americký	k2eAgMnSc1d1	americký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stavba	stavba	k1gFnSc1	stavba
těla	tělo	k1gNnSc2	tělo
==	==	k?	==
</s>
</p>
<p>
<s>
Má	mít	k5eAaImIp3nS	mít
čokoládově	čokoládově	k6eAd1	čokoládově
hnědou	hnědý	k2eAgFnSc4d1	hnědá
srst	srst	k1gFnSc4	srst
s	s	k7c7	s
bílou	bílý	k2eAgFnSc7d1	bílá
tlamou	tlama	k1gFnSc7	tlama
a	a	k8xC	a
čenichem	čenich	k1gInSc7	čenich
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
váží	vážit	k5eAaImIp3nS	vážit
kolem	kolem	k7c2	kolem
900	[number]	k4	900
g	g	kA	g
a	a	k8xC	a
měří	měřit	k5eAaImIp3nS	měřit
28	[number]	k4	28
<g/>
–	–	k?	–
<g/>
43	[number]	k4	43
cm	cm	kA	cm
<g/>
,	,	kIx,	,
samice	samice	k1gFnSc1	samice
měří	měřit	k5eAaImIp3nS	měřit
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
cm	cm	kA	cm
a	a	k8xC	a
váží	vážit	k5eAaImIp3nS	vážit
kolem	kolem	k7c2	kolem
600	[number]	k4	600
g.	g.	k?	g.
</s>
</p>
<p>
<s>
==	==	k?	==
Způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
==	==	k?	==
</s>
</p>
<p>
<s>
Norek	norek	k1gMnSc1	norek
evropský	evropský	k2eAgMnSc1d1	evropský
je	být	k5eAaImIp3nS	být
samotář	samotář	k1gMnSc1	samotář
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
hloubí	hloubit	k5eAaImIp3nS	hloubit
nory	nora	k1gFnPc4	nora
poblíž	poblíž	k7c2	poblíž
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
a	a	k8xC	a
své	svůj	k3xOyFgNnSc4	svůj
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
velké	velký	k2eAgInPc1d1	velký
i	i	k9	i
4	[number]	k4	4
km	km	kA	km
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
značkuje	značkovat	k5eAaImIp3nS	značkovat
pomocí	pomocí	k7c2	pomocí
pachů	pach	k1gInPc2	pach
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
typický	typický	k2eAgMnSc1d1	typický
masožravec	masožravec	k1gMnSc1	masožravec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
loví	lovit	k5eAaImIp3nS	lovit
ryby	ryba	k1gFnPc4	ryba
<g/>
,	,	kIx,	,
králíky	králík	k1gMnPc4	králík
<g/>
,	,	kIx,	,
zajíce	zajíc	k1gMnPc4	zajíc
a	a	k8xC	a
vodní	vodní	k2eAgMnPc4d1	vodní
ptáky	pták	k1gMnPc4	pták
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
==	==	k?	==
</s>
</p>
<p>
<s>
Během	během	k7c2	během
období	období	k1gNnSc2	období
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
trvá	trvat	k5eAaImIp3nS	trvat
od	od	k7c2	od
února	únor	k1gInSc2	únor
do	do	k7c2	do
března	březen	k1gInSc2	březen
<g/>
,	,	kIx,	,
samci	samec	k1gMnPc1	samec
opouštějí	opouštět	k5eAaImIp3nP	opouštět
svá	svůj	k3xOyFgNnPc4	svůj
doupata	doupě	k1gNnPc4	doupě
a	a	k8xC	a
vydávají	vydávat	k5eAaPmIp3nP	vydávat
se	se	k3xPyFc4	se
na	na	k7c4	na
mnohdy	mnohdy	k6eAd1	mnohdy
dlouhé	dlouhý	k2eAgFnPc4d1	dlouhá
výpravy	výprava	k1gFnPc4	výprava
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterých	který	k3yIgFnPc6	který
hledají	hledat	k5eAaImIp3nP	hledat
partnerku	partnerka	k1gFnSc4	partnerka
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
i	i	k8xC	i
samice	samice	k1gFnPc1	samice
se	se	k3xPyFc4	se
páří	pářit	k5eAaImIp3nP	pářit
hned	hned	k6eAd1	hned
s	s	k7c7	s
několika	několik	k4yIc7	několik
zástupci	zástupce	k1gMnPc7	zástupce
opačného	opačný	k2eAgNnSc2d1	opačné
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
rodí	rodit	k5eAaImIp3nP	rodit
4	[number]	k4	4
až	až	k9	až
6	[number]	k4	6
holých	holý	k2eAgNnPc2d1	holé
a	a	k8xC	a
slepých	slepý	k2eAgNnPc2d1	slepé
mláďat	mládě	k1gNnPc2	mládě
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
jsou	být	k5eAaImIp3nP	být
odstavena	odstaven	k2eAgNnPc1d1	odstaveno
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
8	[number]	k4	8
až	až	k9	až
10	[number]	k4	10
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
European	European	k1gMnSc1	European
Mink	mink	k1gMnSc1	mink
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
norek	norek	k1gMnSc1	norek
evropský	evropský	k2eAgMnSc1d1	evropský
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Mustela	Mustel	k1gMnSc2	Mustel
lutreola	lutreola	k1gFnSc1	lutreola
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Fotografie	fotografie	k1gFnSc1	fotografie
norka	norek	k1gMnSc2	norek
evropského	evropský	k2eAgMnSc2d1	evropský
na	na	k7c4	na
Bio-Foto	Bio-Fota	k1gFnSc5	Bio-Fota
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
Norek	norek	k1gMnSc1	norek
v	v	k7c6	v
ZOO	zoo	k1gFnSc6	zoo
Děčín	Děčín	k1gInSc1	Děčín
</s>
</p>
