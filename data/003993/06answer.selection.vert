<s>
Čert	čert	k1gMnSc1	čert
(	(	kIx(	(
<g/>
čort	čort	k1gMnSc1	čort
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
slovanských	slovanský	k2eAgInPc6d1	slovanský
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
staročesky	staročesky	k6eAd1	staročesky
črt	črt	k1gInSc1	črt
<g/>
,	,	kIx,	,
psáno	psán	k2eAgNnSc1d1	psáno
jako	jako	k9	jako
czrt	czrt	k1gInSc4	czrt
<g/>
,	,	kIx,	,
czrrt	czrrt	k1gInSc4	czrrt
či	či	k8xC	či
czirt	czirt	k1gInSc4	czirt
-	-	kIx~	-
srovnej	srovnat	k5eAaPmRp2nS	srovnat
"	"	kIx"	"
<g/>
čírtě	čírtě	k1gNnSc4	čírtě
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
démon	démon	k1gMnSc1	démon
pocházející	pocházející	k2eAgMnSc1d1	pocházející
z	z	k7c2	z
předkřesťanské	předkřesťanský	k2eAgFnSc2d1	předkřesťanská
slovanské	slovanský	k2eAgFnSc2d1	Slovanská
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
.	.	kIx.	.
</s>
