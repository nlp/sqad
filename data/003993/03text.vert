<s>
Čert	čert	k1gMnSc1	čert
(	(	kIx(	(
<g/>
čort	čort	k1gMnSc1	čort
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
slovanských	slovanský	k2eAgInPc6d1	slovanský
jazycích	jazyk	k1gInPc6	jazyk
<g/>
,	,	kIx,	,
staročesky	staročesky	k6eAd1	staročesky
črt	črt	k1gInSc1	črt
<g/>
,	,	kIx,	,
psáno	psán	k2eAgNnSc1d1	psáno
jako	jako	k9	jako
czrt	czrt	k1gInSc4	czrt
<g/>
,	,	kIx,	,
czrrt	czrrt	k1gInSc4	czrrt
či	či	k8xC	či
czirt	czirt	k1gInSc4	czirt
-	-	kIx~	-
srovnej	srovnat	k5eAaPmRp2nS	srovnat
"	"	kIx"	"
<g/>
čírtě	čírtě	k1gNnSc4	čírtě
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
démon	démon	k1gMnSc1	démon
pocházející	pocházející	k2eAgMnSc1d1	pocházející
z	z	k7c2	z
předkřesťanské	předkřesťanský	k2eAgFnSc2d1	předkřesťanská
slovanské	slovanský	k2eAgFnSc2d1	Slovanská
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
křesťanství	křesťanství	k1gNnSc2	křesťanství
byl	být	k5eAaImAgInS	být
u	u	k7c2	u
Slovanů	Slovan	k1gMnPc2	Slovan
jednak	jednak	k8xC	jednak
ztotožňován	ztotožňován	k2eAgMnSc1d1	ztotožňován
s	s	k7c7	s
ďáblem	ďábel	k1gMnSc7	ďábel
jako	jako	k8xC	jako
vládcem	vládce	k1gMnSc7	vládce
pekla	peklo	k1gNnSc2	peklo
a	a	k8xC	a
jednak	jednak	k8xC	jednak
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
mytologicko-pohádková	mytologickoohádkový	k2eAgFnSc1d1	mytologicko-pohádkový
polymorfní	polymorfní	k2eAgFnSc1d1	polymorfní
(	(	kIx(	(
<g/>
ale	ale	k8xC	ale
nejčastěji	často	k6eAd3	často
tvarem	tvar	k1gInSc7	tvar
Panovi	Pan	k1gMnSc3	Pan
podobná	podobný	k2eAgFnSc1d1	podobná
<g/>
)	)	kIx)	)
postava	postava	k1gFnSc1	postava
zastupující	zastupující	k2eAgMnPc4d1	zastupující
různé	různý	k2eAgMnPc4d1	různý
staré	starý	k2eAgMnPc4d1	starý
slovanské	slovanský	k2eAgMnPc4d1	slovanský
démony	démon	k1gMnPc4	démon
a	a	k8xC	a
bůžky	bůžek	k1gMnPc4	bůžek
v	v	k7c6	v
křesťanském	křesťanský	k2eAgInSc6d1	křesťanský
folklóru	folklór	k1gInSc6	folklór
<g/>
,	,	kIx,	,
fungující	fungující	k2eAgNnSc4d1	fungující
jako	jako	k8xC	jako
ztělesnění	ztělesnění	k1gNnSc4	ztělesnění
zla	zlo	k1gNnSc2	zlo
a	a	k8xC	a
obyčejně	obyčejně	k6eAd1	obyčejně
bydlící	bydlící	k2eAgMnPc1d1	bydlící
v	v	k7c6	v
pekle	peklo	k1gNnSc6	peklo
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
obvykle	obvykle	k6eAd1	obvykle
ne	ne	k9	ne
přímo	přímo	k6eAd1	přímo
jako	jako	k8xC	jako
vládce	vládce	k1gMnSc1	vládce
pekla	peklo	k1gNnSc2	peklo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podoba	podoba	k1gFnSc1	podoba
čerta	čert	k1gMnSc2	čert
zřejmě	zřejmě	k6eAd1	zřejmě
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
různých	různý	k2eAgFnPc2d1	různá
bytostí	bytost	k1gFnPc2	bytost
<g/>
,	,	kIx,	,
především	především	k9	především
z	z	k7c2	z
antické	antický	k2eAgFnSc2d1	antická
mytologie	mytologie	k1gFnSc2	mytologie
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhled	k1gInSc7	vzhled
i	i	k8xC	i
povahou	povaha	k1gFnSc7	povaha
se	se	k3xPyFc4	se
nejvíce	nejvíce	k6eAd1	nejvíce
podobá	podobat	k5eAaImIp3nS	podobat
satyrům	satyr	k1gMnPc3	satyr
a	a	k8xC	a
Panovi	Pan	k1gMnSc3	Pan
(	(	kIx(	(
<g/>
Faunovi	Faun	k1gMnSc3	Faun
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bohu	bůh	k1gMnSc3	bůh
pastvin	pastvina	k1gFnPc2	pastvina
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
čert	čert	k1gMnSc1	čert
andělem	anděl	k1gMnSc7	anděl
<g/>
,	,	kIx,	,
svrženým	svržený	k2eAgMnSc7d1	svržený
z	z	k7c2	z
nebes	nebesa	k1gNnPc2	nebesa
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
jeho	jeho	k3xOp3gInSc4	jeho
původ	původ	k1gInSc4	původ
odvozovat	odvozovat	k5eAaImF	odvozovat
i	i	k9	i
od	od	k7c2	od
Titánů	Titán	k1gMnPc2	Titán
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
poraženi	porazit	k5eAaPmNgMnP	porazit
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
s	s	k7c7	s
řeckými	řecký	k2eAgMnPc7d1	řecký
bohy	bůh	k1gMnPc7	bůh
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
od	od	k7c2	od
Héfaista	Héfaist	k1gMnSc2	Héfaist
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
Zeus	Zeus	k1gInSc1	Zeus
svrhl	svrhnout	k5eAaPmAgInS	svrhnout
z	z	k7c2	z
Olympu	Olymp	k1gInSc2	Olymp
-	-	kIx~	-
Hefaistos	Hefaistos	k1gInSc1	Hefaistos
byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
nevzhledný	vzhledný	k2eNgInSc1d1	nevzhledný
a	a	k8xC	a
kulhal	kulhat	k5eAaImAgInS	kulhat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
slovanské	slovanský	k2eAgFnSc6d1	Slovanská
mytologii	mytologie	k1gFnSc6	mytologie
se	se	k3xPyFc4	se
objevovaly	objevovat	k5eAaImAgFnP	objevovat
podobné	podobný	k2eAgFnPc1d1	podobná
bytosti	bytost	k1gFnPc1	bytost
<g/>
,	,	kIx,	,
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
ďas	ďas	k1gMnSc1	ďas
nebo	nebo	k8xC	nebo
také	také	k9	také
běs	běs	k1gInSc1	běs
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
českém	český	k2eAgInSc6d1	český
folklóru	folklór	k1gInSc6	folklór
je	být	k5eAaImIp3nS	být
čert	čert	k1gMnSc1	čert
pohádková	pohádkový	k2eAgFnSc1d1	pohádková
bytost	bytost	k1gFnSc1	bytost
žijící	žijící	k2eAgFnSc1d1	žijící
v	v	k7c6	v
pekle	peklo	k1gNnSc6	peklo
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rohy	roh	k1gInPc4	roh
(	(	kIx(	(
<g/>
spíše	spíše	k9	spíše
nenápadné	nápadný	k2eNgInPc4d1	nenápadný
růžky	růžek	k1gInPc4	růžek
<g/>
,	,	kIx,	,
skryté	skrytý	k2eAgInPc1d1	skrytý
v	v	k7c6	v
kudrnatých	kudrnatý	k2eAgInPc6d1	kudrnatý
vlasech	vlas	k1gInPc6	vlas
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
často	často	k6eAd1	často
pod	pod	k7c7	pod
kloboukem	klobouk	k1gInSc7	klobouk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ocas	ocas	k1gInSc4	ocas
<g/>
,	,	kIx,	,
kopyto	kopyto	k1gNnSc4	kopyto
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
přičmoudlá	přičmoudlý	k2eAgFnSc1d1	přičmoudlá
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Peklo	peklo	k1gNnSc1	peklo
opouští	opouštět	k5eAaImIp3nS	opouštět
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
získala	získat	k5eAaPmAgFnS	získat
duše	duše	k1gFnSc1	duše
hříšníků	hříšník	k1gMnPc2	hříšník
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
je	on	k3xPp3gMnPc4	on
snaží	snažit	k5eAaImIp3nP	snažit
získat	získat	k5eAaPmF	získat
formou	forma	k1gFnSc7	forma
úpisu	úpis	k1gInSc2	úpis
<g/>
.	.	kIx.	.
</s>
<s>
Nabídne	nabídnout	k5eAaPmIp3nS	nabídnout
člověku	člověk	k1gMnSc3	člověk
na	na	k7c4	na
určitou	určitý	k2eAgFnSc4d1	určitá
dobu	doba	k1gFnSc4	doba
služby	služba	k1gFnSc2	služba
nebo	nebo	k8xC	nebo
výhody	výhoda	k1gFnPc4	výhoda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ten	ten	k3xDgMnSc1	ten
mu	on	k3xPp3gNnSc3	on
musí	muset	k5eAaImIp3nP	muset
vlastní	vlastní	k2eAgFnSc7d1	vlastní
krví	krev	k1gFnSc7	krev
upsat	upsat	k5eAaPmF	upsat
svou	svůj	k3xOyFgFnSc4	svůj
duši	duše	k1gFnSc4	duše
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
určená	určený	k2eAgFnSc1d1	určená
lhůta	lhůta	k1gFnSc1	lhůta
vyprší	vypršet	k5eAaPmIp3nS	vypršet
<g/>
,	,	kIx,	,
čert	čert	k1gMnSc1	čert
přijde	přijít	k5eAaPmIp3nS	přijít
k	k	k7c3	k
člověku	člověk	k1gMnSc3	člověk
a	a	k8xC	a
odnese	odnést	k5eAaPmIp3nS	odnést
jeho	jeho	k3xOp3gFnSc4	jeho
duši	duše	k1gFnSc4	duše
do	do	k7c2	do
pekla	peklo	k1gNnSc2	peklo
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
také	také	k9	také
odnáší	odnášet	k5eAaImIp3nS	odnášet
duše	duše	k1gFnPc4	duše
zlých	zlý	k2eAgMnPc2d1	zlý
a	a	k8xC	a
hříšných	hříšný	k2eAgMnPc2d1	hříšný
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
budou	být	k5eAaImBp3nP	být
duše	duše	k1gFnPc1	duše
na	na	k7c4	na
věky	věk	k1gInPc4	věk
věků	věk	k1gInPc2	věk
trýzněny	trýzněn	k2eAgInPc1d1	trýzněn
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pekle	peklo	k1gNnSc6	peklo
jsou	být	k5eAaImIp3nP	být
podle	podle	k7c2	podle
pohádkových	pohádkový	k2eAgFnPc2d1	pohádková
tradic	tradice	k1gFnPc2	tradice
duše	duše	k1gFnSc2	duše
váženy	vážit	k5eAaImNgInP	vážit
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jen	jen	k9	jen
pro	pro	k7c4	pro
jistotu	jistota	k1gFnSc4	jistota
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nedošlo	dojít	k5eNaPmAgNnS	dojít
k	k	k7c3	k
nějaké	nějaký	k3yIgFnSc3	nějaký
chybě	chyba	k1gFnSc3	chyba
<g/>
;	;	kIx,	;
tu	tu	k6eAd1	tu
ovšem	ovšem	k9	ovšem
nelze	lze	k6eNd1	lze
spolehlivě	spolehlivě	k6eAd1	spolehlivě
vyloučit	vyloučit	k5eAaPmF	vyloučit
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
pohádek	pohádka	k1gFnPc2	pohádka
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
záměně	záměna	k1gFnSc6	záměna
zlé	zlý	k2eAgFnSc2d1	zlá
duše	duše	k1gFnSc2	duše
za	za	k7c4	za
dobrou	dobrá	k1gFnSc4	dobrá
založena	založen	k2eAgFnSc1d1	založena
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
čistým	čistý	k2eAgFnPc3d1	čistá
duším	duše	k1gFnPc3	duše
se	se	k3xPyFc4	se
opustit	opustit	k5eAaPmF	opustit
peklo	peklo	k1gNnSc4	peklo
<g/>
,	,	kIx,	,
i	i	k9	i
přes	přes	k7c4	přes
odpor	odpor	k1gInSc4	odpor
pekelných	pekelný	k2eAgFnPc2d1	pekelná
mocností	mocnost	k1gFnPc2	mocnost
<g/>
,	,	kIx,	,
vždy	vždy	k6eAd1	vždy
podaří	podařit	k5eAaPmIp3nS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
pohádková	pohádkový	k2eAgFnSc1d1	pohádková
zápletka	zápletka	k1gFnSc1	zápletka
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
Honza	Honza	k1gMnSc1	Honza
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
třeba	třeba	k9	třeba
krejčí	krejčí	k1gMnPc4	krejčí
<g/>
)	)	kIx)	)
čerta	čert	k1gMnSc4	čert
ošidí	ošidit	k5eAaPmIp3nS	ošidit
<g/>
,	,	kIx,	,
či	či	k8xC	či
spíše	spíše	k9	spíše
napálí	napálit	k5eAaPmIp3nS	napálit
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
těchto	tento	k3xDgFnPc2	tento
pohádek	pohádka	k1gFnPc2	pohádka
jsou	být	k5eAaImIp3nP	být
čerti	čert	k1gMnPc1	čert
bytosti	bytost	k1gFnSc2	bytost
mdlého	mdlý	k2eAgInSc2d1	mdlý
rozumu	rozum	k1gInSc2	rozum
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
nikdy	nikdy	k6eAd1	nikdy
se	se	k3xPyFc4	se
nevyplatí	vyplatit	k5eNaPmIp3nS	vyplatit
podceňovat	podceňovat	k5eAaImF	podceňovat
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Čert	čert	k1gMnSc1	čert
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
letu	let	k1gInSc2	let
a	a	k8xC	a
okamžitého	okamžitý	k2eAgNnSc2d1	okamžité
přemísťování	přemísťování	k1gNnSc2	přemísťování
se	se	k3xPyFc4	se
z	z	k7c2	z
místa	místo	k1gNnSc2	místo
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Kvalita	kvalita	k1gFnSc1	kvalita
čertových	čertový	k2eAgNnPc2d1	čertový
kouzel	kouzlo	k1gNnPc2	kouzlo
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
čerta	čert	k1gMnSc2	čert
–	–	k?	–
například	například	k6eAd1	například
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
pekle	peklo	k1gNnSc6	peklo
panuje	panovat	k5eAaImIp3nS	panovat
určitá	určitý	k2eAgFnSc1d1	určitá
hierarchie	hierarchie	k1gFnSc1	hierarchie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nejnižším	nízký	k2eAgInSc6d3	nejnižší
postu	post	k1gInSc6	post
jsou	být	k5eAaImIp3nP	být
plesniví	plesnivý	k2eAgMnPc1d1	plesnivý
čerti	čert	k1gMnPc1	čert
a	a	k8xC	a
pak	pak	k6eAd1	pak
to	ten	k3xDgNnSc1	ten
stoupá	stoupat	k5eAaImIp3nS	stoupat
vzhůru	vzhůru	k6eAd1	vzhůru
až	až	k9	až
k	k	k7c3	k
Luciferovi	Lucifer	k1gMnSc3	Lucifer
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
čerty	čert	k1gMnPc7	čert
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
setkat	setkat	k5eAaPmF	setkat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
pohádkách	pohádka	k1gFnPc6	pohádka
<g/>
;	;	kIx,	;
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
pověsti	pověst	k1gFnSc6	pověst
o	o	k7c6	o
Faustově	Faustův	k2eAgInSc6d1	Faustův
domě	dům	k1gInSc6	dům
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
;	;	kIx,	;
v	v	k7c6	v
lidové	lidový	k2eAgFnSc6d1	lidová
tradici	tradice	k1gFnSc6	tradice
sv.	sv.	kA	sv.
Mikuláše	mikuláš	k1gInSc2	mikuláš
atd.	atd.	kA	atd.
Synonymem	synonymum	k1gNnSc7	synonymum
pro	pro	k7c4	pro
výraz	výraz	k1gInSc4	výraz
čert	čert	k1gMnSc1	čert
je	být	k5eAaImIp3nS	být
slovo	slovo	k1gNnSc4	slovo
ďábel	ďábel	k1gMnSc1	ďábel
<g/>
,	,	kIx,	,
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
kulturním	kulturní	k2eAgNnSc6d1	kulturní
prostředí	prostředí	k1gNnSc6	prostředí
jde	jít	k5eAaImIp3nS	jít
však	však	k9	však
o	o	k7c4	o
významově	významově	k6eAd1	významově
poněkud	poněkud	k6eAd1	poněkud
odlišný	odlišný	k2eAgInSc1d1	odlišný
pojem	pojem	k1gInSc1	pojem
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
kterým	který	k3yIgMnSc7	který
si	se	k3xPyFc3	se
lidé	člověk	k1gMnPc1	člověk
často	často	k6eAd1	často
představí	představit	k5eAaPmIp3nP	představit
jinou	jiný	k2eAgFnSc4d1	jiná
bytost	bytost	k1gFnSc4	bytost
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
zdali	zdali	k8xS	zdali
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
čert	čert	k1gMnSc1	čert
záporná	záporný	k2eAgFnSc1d1	záporná
bytost	bytost	k1gFnSc1	bytost
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ďábla	ďábel	k1gMnSc2	ďábel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zlé	zlý	k2eAgMnPc4d1	zlý
lidi	člověk	k1gMnPc4	člověk
trestá	trestat	k5eAaImIp3nS	trestat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ty	ten	k3xDgInPc4	ten
dobré	dobrý	k2eAgInPc4d1	dobrý
odměňuje	odměňovat	k5eAaImIp3nS	odměňovat
zlatem	zlato	k1gNnSc7	zlato
či	či	k8xC	či
kouzelnými	kouzelný	k2eAgInPc7d1	kouzelný
předměty	předmět	k1gInPc7	předmět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
pohádkách	pohádka	k1gFnPc6	pohádka
je	být	k5eAaImIp3nS	být
čert	čert	k1gMnSc1	čert
spíš	spíš	k9	spíš
komický	komický	k2eAgMnSc1d1	komický
a	a	k8xC	a
přihlouplý	přihlouplý	k2eAgMnSc1d1	přihlouplý
než	než	k8xS	než
děsivý	děsivý	k2eAgMnSc1d1	děsivý
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ďábel	ďábel	k1gMnSc1	ďábel
<g/>
.	.	kIx.	.
</s>
<s>
Čert	čert	k1gMnSc1	čert
se	se	k3xPyFc4	se
v	v	k7c6	v
křesťanské	křesťanský	k2eAgFnSc6d1	křesťanská
teologii	teologie	k1gFnSc6	teologie
nevyskytuje	vyskytovat	k5eNaImIp3nS	vyskytovat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
křesťanských	křesťanský	k2eAgInPc2d1	křesťanský
principů	princip	k1gInPc2	princip
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
reálně	reálně	k6eAd1	reálně
existující	existující	k2eAgFnSc3d1	existující
duchovní	duchovní	k2eAgFnSc3d1	duchovní
netělesné	tělesný	k2eNgFnSc3d1	netělesná
bytosti	bytost	k1gFnSc3	bytost
ďábli	ďábel	k1gMnPc1	ďábel
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc7	jejich
působením	působení	k1gNnSc7	působení
se	se	k3xPyFc4	se
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
a	a	k8xC	a
prohlubuje	prohlubovat	k5eAaImIp3nS	prohlubovat
zlo	zlo	k1gNnSc4	zlo
jako	jako	k8xC	jako
takové	takový	k3xDgFnPc4	takový
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
původci	původce	k1gMnPc1	původce
i	i	k8xC	i
mnoha	mnoho	k4c2	mnoho
chorob	choroba	k1gFnPc2	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
přivolává	přivolávat	k5eAaImIp3nS	přivolávat
působení	působení	k1gNnSc4	působení
ďábla	ďábel	k1gMnSc2	ďábel
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
sám	sám	k3xTgMnSc1	sám
zlo	zlo	k1gNnSc4	zlo
páchá	páchat	k5eAaImIp3nS	páchat
<g/>
,	,	kIx,	,
neplatí	platit	k5eNaImIp3nS	platit
zde	zde	k6eAd1	zde
však	však	k9	však
přímá	přímý	k2eAgFnSc1d1	přímá
úměra	úměra	k1gFnSc1	úměra
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
principy	princip	k1gInPc1	princip
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
formě	forma	k1gFnSc6	forma
přijímány	přijímat	k5eAaImNgInP	přijímat
jen	jen	k6eAd1	jen
některými	některý	k3yIgFnPc7	některý
církvemi	církev	k1gFnPc7	církev
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
církev	církev	k1gFnSc1	církev
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
pro	pro	k7c4	pro
vyhánění	vyhánění	k1gNnSc4	vyhánění
zlých	zlý	k2eAgMnPc2d1	zlý
duchů	duch	k1gMnPc2	duch
a	a	k8xC	a
ďábelského	ďábelský	k2eAgInSc2d1	ďábelský
vlivu	vliv	k1gInSc2	vliv
oficiálně	oficiálně	k6eAd1	oficiálně
ustanoven	ustanoven	k2eAgInSc1d1	ustanoven
institut	institut	k1gInSc1	institut
exorcisty	exorcista	k1gMnSc2	exorcista
<g/>
.	.	kIx.	.
</s>
<s>
Upisování	upisování	k1gNnSc1	upisování
duše	duše	k1gFnSc2	duše
ďáblu	ďábel	k1gMnSc3	ďábel
patří	patřit	k5eAaImIp3nP	patřit
spíše	spíše	k9	spíše
do	do	k7c2	do
folklóru	folklór	k1gInSc2	folklór
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ďábla	ďábel	k1gMnSc4	ďábel
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
označení	označení	k1gNnSc1	označení
démon	démon	k1gMnSc1	démon
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
démon	démon	k1gMnSc1	démon
nemusí	muset	k5eNaImIp3nS	muset
existovat	existovat	k5eAaImF	existovat
reálně	reálně	k6eAd1	reálně
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
existence	existence	k1gFnSc1	existence
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
jen	jen	k9	jen
odvozena	odvodit	k5eAaPmNgFnS	odvodit
od	od	k7c2	od
lidské	lidský	k2eAgFnSc2d1	lidská
mysli	mysl	k1gFnSc2	mysl
<g/>
.	.	kIx.	.
</s>
<s>
Čort	Čort	k1gMnSc1	Čort
neboli	neboli	k8xC	neboli
čert	čert	k1gMnSc1	čert
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
stvoření	stvoření	k1gNnSc1	stvoření
ze	z	k7c2	z
slovanské	slovanský	k2eAgFnSc2d1	Slovanská
mytologie	mytologie	k1gFnSc2	mytologie
nebo	nebo	k8xC	nebo
přesněji	přesně	k6eAd2	přesně
démonologie	démonologie	k1gFnSc1	démonologie
<g/>
.	.	kIx.	.
</s>
<s>
Čort	Čort	k1gInSc1	Čort
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
ze	z	k7c2	z
vrcholně	vrcholně	k6eAd1	vrcholně
zlého	zlý	k2eAgMnSc2d1	zlý
démona	démon	k1gMnSc2	démon
s	s	k7c7	s
rohy	roh	k1gInPc7	roh
<g/>
,	,	kIx,	,
kopyty	kopyto	k1gNnPc7	kopyto
<g/>
,	,	kIx,	,
ocasem	ocas	k1gInSc7	ocas
a	a	k8xC	a
prasečím	prasečí	k2eAgInSc7d1	prasečí
obličejem	obličej	k1gInSc7	obličej
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
mytologie	mytologie	k1gFnSc2	mytologie
je	být	k5eAaImIp3nS	být
synem	syn	k1gMnSc7	syn
Černoboga	Černobog	k1gMnSc2	Černobog
a	a	k8xC	a
Mary	Mary	k1gFnSc2	Mary
(	(	kIx(	(
<g/>
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
nejspíše	nejspíše	k9	nejspíše
litevská	litevský	k2eAgFnSc1d1	Litevská
bohyně	bohyně	k1gFnSc1	bohyně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
jako	jako	k8xC	jako
haspyda	haspyda	k1gFnSc1	haspyda
<g/>
,	,	kIx,	,
didko	didko	k1gNnSc1	didko
<g/>
,	,	kIx,	,
irod	irod	k6eAd1	irod
a	a	k8xC	a
kutsyi	kutsyi	k6eAd1	kutsyi
<g/>
.	.	kIx.	.
</s>
<s>
Tysjača	Tysjača	k1gMnSc1	Tysjača
čertej	čertat	k5eAaPmRp2nS	čertat
–	–	k?	–
Tisíce	tisíc	k4xCgInPc1	tisíc
čertů	čert	k1gMnPc2	čert
<g/>
.	.	kIx.	.
</s>
<s>
Čort	Čort	k1gInSc1	Čort
poberi	pober	k1gFnSc2	pober
<g/>
!	!	kIx.	!
</s>
<s>
–	–	k?	–
"	"	kIx"	"
<g/>
Čert	čert	k1gMnSc1	čert
to	ten	k3xDgNnSc1	ten
vem	vem	k?	vem
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
jako	jako	k9	jako
nadávka	nadávka	k1gFnSc1	nadávka
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Čort	Čort	k1gMnSc1	Čort
poputal	poputat	k5eAaPmAgMnS	poputat
–	–	k?	–
Poblázněný	poblázněný	k2eAgInSc4d1	poblázněný
čertem	čert	k1gMnSc7	čert
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
čertjam	čertjam	k6eAd1	čertjam
–	–	k?	–
K	k	k7c3	k
čertům	čert	k1gMnPc3	čert
<g/>
,	,	kIx,	,
míněno	míněn	k2eAgNnSc4d1	míněno
do	do	k7c2	do
pekel	peklo	k1gNnPc2	peklo
<g/>
.	.	kIx.	.
</s>
<s>
Hrátky	hrátky	k1gFnPc4	hrátky
s	s	k7c7	s
čertem	čert	k1gMnSc7	čert
<g/>
,	,	kIx,	,
pohádková	pohádkový	k2eAgFnSc1d1	pohádková
divadelní	divadelní	k2eAgFnSc1d1	divadelní
hra	hra	k1gFnSc1	hra
Jana	Jan	k1gMnSc2	Jan
Drdy	Drda	k1gMnSc2	Drda
1946	[number]	k4	1946
<g/>
;	;	kIx,	;
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Josefa	Josef	k1gMnSc2	Josef
Macha	Mach	k1gMnSc2	Mach
podle	podle	k7c2	podle
Drdovy	Drdův	k2eAgFnSc2d1	Drdova
hry	hra	k1gFnSc2	hra
1956	[number]	k4	1956
Kam	kam	k6eAd1	kam
čert	čert	k1gMnSc1	čert
nemůže	moct	k5eNaImIp3nS	moct
<g/>
,	,	kIx,	,
filmová	filmový	k2eAgFnSc1d1	filmová
komedie	komedie	k1gFnSc1	komedie
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Podskalského	podskalský	k2eAgMnSc2d1	podskalský
s	s	k7c7	s
Janou	Jana	k1gFnSc7	Jana
Hlaváčovou	Hlaváčová	k1gFnSc7	Hlaváčová
a	a	k8xC	a
Miroslavem	Miroslav	k1gMnSc7	Miroslav
Horníčkem	Horníček	k1gMnSc7	Horníček
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
Dalskabáty	Dalskabáta	k1gFnSc2	Dalskabáta
<g/>
,	,	kIx,	,
hříšná	hříšný	k2eAgFnSc1d1	hříšná
ves	ves	k1gFnSc1	ves
aneb	aneb	k?	aneb
<g />
.	.	kIx.	.
</s>
<s>
Zapomenutý	zapomenutý	k2eAgMnSc1d1	zapomenutý
čert	čert	k1gMnSc1	čert
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgFnSc1d1	divadelní
komedie	komedie	k1gFnSc1	komedie
Jana	Jan	k1gMnSc2	Jan
Drdy	Drda	k1gMnSc2	Drda
o	o	k7c6	o
8	[number]	k4	8
obrazech	obraz	k1gInPc6	obraz
-	-	kIx~	-
polepšený	polepšený	k2eAgMnSc1d1	polepšený
čert	čert	k1gMnSc1	čert
pomůže	pomoct	k5eAaPmIp3nS	pomoct
odhalit	odhalit	k5eAaPmF	odhalit
ďábla	ďábel	k1gMnSc4	ďábel
převlečeného	převlečený	k2eAgMnSc2d1	převlečený
za	za	k7c4	za
faráře	farář	k1gMnSc4	farář
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
a	a	k8xC	a
stejnojmenný	stejnojmenný	k2eAgInSc1d1	stejnojmenný
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
natočený	natočený	k2eAgInSc1d1	natočený
podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
hry	hra	k1gFnSc2	hra
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
S	s	k7c7	s
čerty	čert	k1gMnPc7	čert
nejsou	být	k5eNaImIp3nP	být
žerty	žert	k1gInPc1	žert
<g/>
,	,	kIx,	,
filmová	filmový	k2eAgFnSc1d1	filmová
pohádka	pohádka	k1gFnSc1	pohádka
Hynka	Hynek	k1gMnSc2	Hynek
Bočana	bočan	k1gMnSc2	bočan
(	(	kIx(	(
<g/>
1984	[number]	k4	1984
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Čert	čert	k1gMnSc1	čert
ví	vědět	k5eAaImIp3nS	vědět
proč	proč	k6eAd1	proč
<g/>
,	,	kIx,	,
film	film	k1gInSc4	film
Romana	Roman	k1gMnSc2	Roman
Vávry	Vávra	k1gMnSc2	Vávra
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Čertík	čertík	k1gMnSc1	čertík
Bertík	Bertík	k1gMnSc1	Bertík
<g/>
,	,	kIx,	,
pohádková	pohádkový	k2eAgFnSc1d1	pohádková
postavička	postavička	k1gFnSc1	postavička
z	z	k7c2	z
televizního	televizní	k2eAgInSc2d1	televizní
pořadu	pořad	k1gInSc2	pořad
Štěpánky	Štěpánka	k1gFnSc2	Štěpánka
Haničincové	Haničincový	k2eAgFnPc1d1	Haničincová
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
Čert	čert	k1gMnSc1	čert
nespí	spát	k5eNaImIp3nS	spát
<g/>
,	,	kIx,	,
československý	československý	k2eAgInSc1d1	československý
povidkový	povidkový	k2eAgInSc1d1	povidkový
hraný	hraný	k2eAgInSc1d1	hraný
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
Čertův	čertův	k2eAgMnSc1d1	čertův
švagr	švagr	k1gMnSc1	švagr
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgInSc1d1	televizní
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
režisérky	režisérka	k1gFnSc2	režisérka
Vlasty	Vlasta	k1gFnSc2	Vlasta
Janečkové	Janečkové	k2eAgFnPc4d1	Janečkové
Hrátky	hrátky	k1gFnPc4	hrátky
s	s	k7c7	s
čertem	čert	k1gMnSc7	čert
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
filmová	filmový	k2eAgFnSc1d1	filmová
pohádka	pohádka	k1gFnSc1	pohádka
režiséra	režisér	k1gMnSc2	režisér
Josefa	Josef	k1gMnSc2	Josef
Macha	Mach	k1gMnSc2	Mach
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
</s>
<s>
Čert	čert	k1gMnSc1	čert
a	a	k8xC	a
Káča	Káča	k1gFnSc1	Káča
<g/>
,	,	kIx,	,
pohádka	pohádka	k1gFnSc1	pohádka
české	český	k2eAgFnSc2d1	Česká
spisovatelky	spisovatelka	k1gFnSc2	spisovatelka
Boženy	Božena	k1gFnSc2	Božena
Němcové	Němcová	k1gFnSc2	Němcová
Kam	kam	k6eAd1	kam
čert	čert	k1gMnSc1	čert
nemůže	moct	k5eNaImIp3nS	moct
<g/>
,	,	kIx,	,
nastrčí	nastrčit	k5eAaPmIp3nS	nastrčit
ženskou	ženská	k1gFnSc4	ženská
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
pořekadlo	pořekadlo	k1gNnSc1	pořekadlo
<g/>
)	)	kIx)	)
Čiň	činit	k5eAaImRp2nS	činit
čertu	čert	k1gMnSc3	čert
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
peklem	peklo	k1gNnSc7	peklo
se	se	k3xPyFc4	se
Ti	ty	k3xPp2nSc3	ty
odmění	odměnit	k5eAaPmIp3nS	odměnit
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
přísloví	přísloví	k1gNnSc1	přísloví
<g/>
)	)	kIx)	)
Tumáš	Tumáš	k1gMnSc5	Tumáš
čerte	čert	k1gMnSc5	čert
kropáč	kropáč	k1gInSc4	kropáč
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
frazeologizmus	frazeologizmus	k1gInSc4	frazeologizmus
<g/>
)	)	kIx)	)
Berou	brát	k5eAaImIp3nP	brát
mě	já	k3xPp1nSc4	já
všichni	všechen	k3xTgMnPc1	všechen
čerti	čert	k1gMnPc1	čert
(	(	kIx(	(
<g/>
frazeologismus	frazeologismus	k1gInSc1	frazeologismus
<g/>
)	)	kIx)	)
Vem	Vem	k?	Vem
tě	ty	k3xPp2nSc4	ty
čert	čert	k1gMnSc1	čert
<g/>
!	!	kIx.	!
</s>
<s>
Táhni	táhnout	k5eAaImRp2nS	táhnout
k	k	k7c3	k
čertu	čert	k1gMnSc3	čert
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
frazeologismus	frazeologismus	k1gInSc4	frazeologismus
<g/>
)	)	kIx)	)
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
po	po	k7c6	po
čertech	čert	k1gMnPc6	čert
...	...	k?	...
<g/>
(	(	kIx(	(
<g/>
nějaké	nějaký	k3yIgInPc1	nějaký
-	-	kIx~	-
pěkné	pěkný	k2eAgInPc1d1	pěkný
<g/>
,	,	kIx,	,
drahé	drahý	k2eAgInPc1d1	drahý
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
frazeologismus	frazeologismus	k1gInSc1	frazeologismus
<g/>
)	)	kIx)	)
Čert	čert	k1gMnSc1	čert
ví	vědět	k5eAaImIp3nS	vědět
proč	proč	k6eAd1	proč
(	(	kIx(	(
<g/>
frazeologismus	frazeologismus	k1gInSc1	frazeologismus
<g/>
)	)	kIx)	)
Šijou	šít	k5eAaImIp3nP	šít
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
<g/>
(	(	kIx(	(
<g/>
m	m	kA	m
<g/>
)	)	kIx)	)
všichni	všechen	k3xTgMnPc1	všechen
čerti	čert	k1gMnPc1	čert
(	(	kIx(	(
<g/>
frazeologismus	frazeologismus	k1gInSc1	frazeologismus
<g/>
)	)	kIx)	)
To	to	k9	to
mi	já	k3xPp1nSc3	já
byl	být	k5eAaImAgMnS	být
čert	čert	k1gMnSc1	čert
dlužen	dlužen	k2eAgMnSc1d1	dlužen
(	(	kIx(	(
<g/>
frazeologismus	frazeologismus	k1gInSc1	frazeologismus
<g/>
)	)	kIx)	)
</s>
