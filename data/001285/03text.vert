<s>
Antimon	antimon	k1gInSc1	antimon
<g/>
,	,	kIx,	,
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Sb	sb	kA	sb
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Stibium	stibium	k1gNnSc1	stibium
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
polokovy	polokův	k2eAgFnPc4d1	polokův
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgFnPc1d1	známa
lidstvu	lidstvo	k1gNnSc3	lidstvo
již	již	k9	již
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
různých	různý	k2eAgFnPc2d1	různá
slitin	slitina	k1gFnPc2	slitina
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
elektronických	elektronický	k2eAgInPc2d1	elektronický
prvků	prvek	k1gInPc2	prvek
<g/>
,	,	kIx,	,
barviv	barvivo	k1gNnPc2	barvivo
a	a	k8xC	a
keramických	keramický	k2eAgInPc2d1	keramický
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Antimon	antimon	k1gInSc1	antimon
je	být	k5eAaImIp3nS	být
stříbrolesklý	stříbrolesklý	k2eAgInSc1d1	stříbrolesklý
kovový	kovový	k2eAgInSc1d1	kovový
až	až	k8xS	až
polokovový	polokovový	k2eAgInSc1d1	polokovový
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
již	již	k6eAd1	již
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
mocenstvích	mocenství	k1gNnPc6	mocenství
Sb-	Sb-	k1gFnSc7	Sb-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
Sb	sb	kA	sb
<g/>
+	+	kIx~	+
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
Sb	sb	kA	sb
<g/>
+	+	kIx~	+
<g/>
4	[number]	k4	4
a	a	k8xC	a
Sb	sb	kA	sb
<g/>
+	+	kIx~	+
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Antimon	antimon	k1gInSc1	antimon
stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
elektrochemické	elektrochemický	k2eAgFnSc6d1	elektrochemická
řadě	řada	k1gFnSc6	řada
napětí	napětí	k1gNnSc2	napětí
kovů	kov	k1gInPc2	kov
až	až	k9	až
za	za	k7c7	za
vodíkem	vodík	k1gInSc7	vodík
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
pouze	pouze	k6eAd1	pouze
působením	působení	k1gNnSc7	působení
silných	silný	k2eAgFnPc2d1	silná
minerálních	minerální	k2eAgFnPc2d1	minerální
oxidačních	oxidační	k2eAgFnPc2d1	oxidační
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
,	,	kIx,	,
vůči	vůči	k7c3	vůči
kterým	který	k3yIgFnPc3	který
není	být	k5eNaImIp3nS	být
antimon	antimon	k1gInSc1	antimon
příliš	příliš	k6eAd1	příliš
odolný	odolný	k2eAgInSc1d1	odolný
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
také	také	k9	také
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
především	především	k9	především
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
chlorovodíkové	chlorovodíkový	k2eAgFnSc6d1	chlorovodíková
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
i	i	k8xC	i
malých	malý	k2eAgFnPc2d1	malá
množství	množství	k1gNnSc4	množství
oxidačních	oxidační	k2eAgNnPc2d1	oxidační
činidel	činidlo	k1gNnPc2	činidlo
(	(	kIx(	(
<g/>
HNO	HNO	kA	HNO
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ochotně	ochotně	k6eAd1	ochotně
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
halogeny	halogen	k1gInPc7	halogen
a	a	k8xC	a
sulfanem	sulfan	k1gInSc7	sulfan
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
tepla	teplo	k1gNnSc2	teplo
se	se	k3xPyFc4	se
slučuje	slučovat	k5eAaImIp3nS	slučovat
se	s	k7c7	s
sírou	síra	k1gFnSc7	síra
<g/>
,	,	kIx,	,
fosforem	fosfor	k1gInSc7	fosfor
<g/>
,	,	kIx,	,
arsenem	arsen	k1gInSc7	arsen
a	a	k8xC	a
dalšími	další	k2eAgInPc7d1	další
prvky	prvek	k1gInPc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zahřívání	zahřívání	k1gNnSc6	zahřívání
s	s	k7c7	s
oxidačními	oxidační	k2eAgNnPc7d1	oxidační
činidly	činidlo	k1gNnPc7	činidlo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
dusičnany	dusičnan	k1gInPc1	dusičnan
<g/>
,	,	kIx,	,
chlorečnany	chlorečnan	k1gInPc1	chlorečnan
<g/>
)	)	kIx)	)
vybuchuje	vybuchovat	k5eAaImIp3nS	vybuchovat
práškový	práškový	k2eAgInSc4d1	práškový
antimon	antimon	k1gInSc4	antimon
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
solí	sůl	k1gFnPc2	sůl
kyseliny	kyselina	k1gFnSc2	kyselina
antimoničné	antimoničný	k2eAgFnSc2d1	antimoničný
<g/>
.	.	kIx.	.
</s>
<s>
Kovový	kovový	k2eAgInSc1d1	kovový
antimon	antimon	k1gInSc1	antimon
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
několika	několik	k4yIc6	několik
alotropních	alotropní	k2eAgFnPc6d1	alotropní
modifikacích	modifikace	k1gFnPc6	modifikace
<g/>
:	:	kIx,	:
modro-bílý	modroílý	k2eAgInSc4d1	modro-bílý
kovový	kovový	k2eAgInSc4d1	kovový
antimon	antimon	k1gInSc4	antimon
a	a	k8xC	a
nestálé	stálý	k2eNgFnPc4d1	nestálá
nekovové	kovový	k2eNgFnPc4d1	nekovová
formy	forma	k1gFnPc4	forma
žlutého	žlutý	k2eAgMnSc2d1	žlutý
a	a	k8xC	a
černého	černý	k2eAgInSc2d1	černý
antimonu	antimon	k1gInSc2	antimon
<g/>
.	.	kIx.	.
</s>
<s>
Kovový	kovový	k2eAgInSc1d1	kovový
neboli	neboli	k8xC	neboli
šedý	šedý	k2eAgInSc1d1	šedý
antimon	antimon	k1gInSc1	antimon
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
tvrdý	tvrdý	k2eAgMnSc1d1	tvrdý
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
křehký	křehký	k2eAgInSc1d1	křehký
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
je	být	k5eAaImIp3nS	být
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
teplot	teplota	k1gFnPc2	teplota
neomezeně	omezeně	k6eNd1	omezeně
stálý	stálý	k2eAgInSc1d1	stálý
<g/>
,	,	kIx,	,
za	za	k7c2	za
zvýšené	zvýšený	k2eAgFnSc2d1	zvýšená
teploty	teplota	k1gFnSc2	teplota
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
oxidu	oxid	k1gInSc2	oxid
antimonitého	antimonitý	k2eAgInSc2d1	antimonitý
Sb	sb	kA	sb
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Žlutý	žlutý	k2eAgInSc4d1	žlutý
antimon	antimon	k1gInSc4	antimon
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
zaváděním	zavádění	k1gNnSc7	zavádění
kyslíku	kyslík	k1gInSc2	kyslík
do	do	k7c2	do
kapalného	kapalný	k2eAgInSc2d1	kapalný
antimonovodíku	antimonovodík	k1gInSc2	antimonovodík
při	při	k7c6	při
-90	-90	k4	-90
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
modifikacím	modifikace	k1gFnPc3	modifikace
žlutého	žlutý	k2eAgInSc2d1	žlutý
arsenu	arsen	k1gInSc2	arsen
a	a	k8xC	a
bílého	bílý	k2eAgInSc2d1	bílý
fosforu	fosfor	k1gInSc2	fosfor
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
-80	-80	k4	-80
°	°	k?	°
<g/>
C	C	kA	C
černá	černat	k5eAaImIp3nS	černat
a	a	k8xC	a
přechází	přecházet	k5eAaImIp3nS	přecházet
na	na	k7c4	na
modifikaci	modifikace	k1gFnSc4	modifikace
černého	černý	k2eAgInSc2d1	černý
antimonu	antimon	k1gInSc2	antimon
<g/>
.	.	kIx.	.
</s>
<s>
Černý	černý	k2eAgInSc1d1	černý
antimon	antimon	k1gInSc1	antimon
vzniká	vznikat	k5eAaImIp3nS	vznikat
buď	buď	k8xC	buď
ze	z	k7c2	z
žlutého	žlutý	k2eAgMnSc2d1	žlutý
nebo	nebo	k8xC	nebo
působením	působení	k1gNnSc7	působení
vzduchu	vzduch	k1gInSc2	vzduch
na	na	k7c4	na
kapalný	kapalný	k2eAgInSc4d1	kapalný
antimonovodík	antimonovodík	k1gInSc4	antimonovodík
při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
vyšších	vysoký	k2eAgFnPc2d2	vyšší
než	než	k8xS	než
-80	-80	k4	-80
°	°	k?	°
<g/>
C.	C.	kA	C.
Černý	černý	k2eAgInSc1d1	černý
antimon	antimon	k1gInSc1	antimon
je	být	k5eAaImIp3nS	být
reaktivnější	reaktivní	k2eAgInSc1d2	reaktivnější
než	než	k8xS	než
kovový	kovový	k2eAgInSc1d1	kovový
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
obyčejné	obyčejný	k2eAgFnPc4d1	obyčejná
teploty	teplota	k1gFnPc4	teplota
se	s	k7c7	s
vzduchem	vzduch	k1gInSc7	vzduch
oxiduje	oxidovat	k5eAaBmIp3nS	oxidovat
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
dokonce	dokonce	k9	dokonce
vznítit	vznítit	k5eAaPmF	vznítit
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
zahřán	zahřán	k1gInSc1	zahřán
za	za	k7c2	za
nepřístupu	nepřístup	k1gInSc2	nepřístup
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
přechází	přecházet	k5eAaImIp3nS	přecházet
na	na	k7c4	na
kovovou	kovový	k2eAgFnSc4d1	kovová
modifikaci	modifikace	k1gFnSc4	modifikace
<g/>
.	.	kIx.	.
</s>
<s>
Antimon	antimon	k1gInSc1	antimon
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
poměrně	poměrně	k6eAd1	poměrně
vzácným	vzácný	k2eAgInSc7d1	vzácný
prvkem	prvek	k1gInSc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
činí	činit	k5eAaImIp3nS	činit
pouze	pouze	k6eAd1	pouze
0,2	[number]	k4	0,2
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
ppm	ppm	k?	ppm
(	(	kIx(	(
<g/>
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
činí	činit	k5eAaImIp3nS	činit
jeho	jeho	k3xOp3gFnSc1	jeho
koncentrace	koncentrace	k1gFnSc1	koncentrace
pouze	pouze	k6eAd1	pouze
0,3	[number]	k4	0,3
mikrogramy	mikrogram	k1gInPc4	mikrogram
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
litru	litr	k1gInSc6	litr
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
antimonu	antimon	k1gInSc2	antimon
přibližně	přibližně	k6eAd1	přibližně
300	[number]	k4	300
miliard	miliarda	k4xCgFnPc2	miliarda
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
rudou	ruda	k1gFnSc7	ruda
antimonu	antimon	k1gInSc2	antimon
je	být	k5eAaImIp3nS	být
antimonit	antimonit	k1gInSc1	antimonit
<g/>
,	,	kIx,	,
chemicky	chemicky	k6eAd1	chemicky
sulfid	sulfid	k1gInSc1	sulfid
antimonitý	antimonitý	k2eAgInSc1d1	antimonitý
Sb	sb	kA	sb
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
minerály	minerál	k1gInPc7	minerál
antimonu	antimon	k1gInSc2	antimon
je	být	k5eAaImIp3nS	být
ullmannit	ullmannit	k1gInSc1	ullmannit
o	o	k7c4	o
složení	složení	k1gNnSc4	složení
NiSbS	NiSbS	k1gFnSc2	NiSbS
<g/>
,	,	kIx,	,
breithauptit	breithauptit	k5eAaPmF	breithauptit
NiSb	NiSb	k1gInSc4	NiSb
<g/>
,	,	kIx,	,
dyskrazit	dyskrazit	k5eAaPmF	dyskrazit
Ag	Ag	k1gFnSc7	Ag
<g/>
3	[number]	k4	3
<g/>
Sb	sb	kA	sb
<g/>
,	,	kIx,	,
pyrargyrit	pyrargyrit	k1gInSc4	pyrargyrit
Ag	Ag	k1gFnPc2	Ag
<g/>
3	[number]	k4	3
<g/>
SbS	SbS	k1gFnPc2	SbS
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
boulangerit	boulangerit	k1gInSc1	boulangerit
5	[number]	k4	5
<g/>
PbS	PbS	k1gFnSc2	PbS
<g/>
.2	.2	k4	.2
Sb	sb	kA	sb
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
jamesonit	jamesonit	k1gInSc1	jamesonit
2	[number]	k4	2
PbS	PbS	k1gFnPc2	PbS
<g/>
.	.	kIx.	.
<g/>
Sb	sb	kA	sb
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
3	[number]	k4	3
a	a	k8xC	a
například	například	k6eAd1	například
stefanit	stefanit	k1gInSc1	stefanit
5	[number]	k4	5
Ag	Ag	k1gFnSc2	Ag
<g/>
2	[number]	k4	2
<g/>
S.	S.	kA	S.
<g/>
Sb	sb	kA	sb
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Vzácně	vzácně	k6eAd1	vzácně
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
s	s	k7c7	s
elementárním	elementární	k2eAgInSc7d1	elementární
<g/>
,	,	kIx,	,
kovovým	kovový	k2eAgInSc7d1	kovový
antimonem	antimon	k1gInSc7	antimon
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
je	být	k5eAaImIp3nS	být
také	také	k9	také
přítomen	přítomen	k2eAgMnSc1d1	přítomen
jako	jako	k8xC	jako
příměs	příměs	k1gFnSc1	příměs
v	v	k7c6	v
rudách	ruda	k1gFnPc6	ruda
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
mědi	měď	k1gFnSc2	měď
a	a	k8xC	a
olova	olovo	k1gNnSc2	olovo
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnPc4d3	veliký
zásoby	zásoba	k1gFnPc4	zásoba
antimonu	antimon	k1gInSc2	antimon
na	na	k7c6	na
světě	svět	k1gInSc6	svět
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
v	v	k7c6	v
Tádžikistánu	Tádžikistán	k1gInSc6	Tádžikistán
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslově	průmyslově	k6eAd1	průmyslově
se	se	k3xPyFc4	se
antimon	antimon	k1gInSc1	antimon
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
pražně-redukčním	pražněedukční	k2eAgInSc7d1	pražně-redukční
pochodem	pochod	k1gInSc7	pochod
svých	svůj	k3xOyFgFnPc2	svůj
sulfidických	sulfidický	k2eAgFnPc2d1	sulfidická
rud	ruda	k1gFnPc2	ruda
za	za	k7c2	za
přístupu	přístup	k1gInSc2	přístup
vzduchu	vzduch	k1gInSc2	vzduch
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
oxidů	oxid	k1gInPc2	oxid
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
redukují	redukovat	k5eAaBmIp3nP	redukovat
žárově	žárově	k6eAd1	žárově
uhlím	uhlí	k1gNnSc7	uhlí
(	(	kIx(	(
<g/>
koksem	koks	k1gInSc7	koks
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
5	[number]	k4	5
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c6	o
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
3	[number]	k4	3
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
O	O	kA	O
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gMnSc1	mathsf
{	{	kIx(	{
<g/>
Sb_	Sb_	k1gMnSc1	Sb_
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
S_	S_	k1gFnSc2	S_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
5	[number]	k4	5
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
O_	O_	k1gMnSc1	O_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
Sb_	Sb_	k1gMnSc6	Sb_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
O_	O_	k1gFnSc2	O_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
+	+	kIx~	+
<g/>
3	[number]	k4	3
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
SO_	SO_	k1gMnSc1	SO_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}}}	}}}}	k?	}}}}
:	:	kIx,	:
Sulfid	sulfid	k1gInSc1	sulfid
antimonitý	antimonitý	k2eAgInSc1d1	antimonitý
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
oxidu	oxid	k1gInSc2	oxid
antimoničitého	antimoničitý	k2eAgInSc2d1	antimoničitý
a	a	k8xC	a
oxidu	oxid	k1gInSc2	oxid
siřičitého	siřičitý	k2eAgInSc2d1	siřičitý
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
O	o	k7c6	o
:	:	kIx,	:
4	[number]	k4	4
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
<g />
.	.	kIx.	.
</s>
<s hack="1">
4	[number]	k4	4
:	:	kIx,	:
C	C	kA	C
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
S	s	k7c7	s
b	b	k?	b
+	+	kIx~	+
4	[number]	k4	4
:	:	kIx,	:
C	C	kA	C
O	O	kA	O
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gMnSc1	mathsf
{	{	kIx(	{
<g/>
Sb_	Sb_	k1gMnSc1	Sb_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
O_	O_	k1gFnSc2	O_
<g/>
{	{	kIx(	{
<g/>
4	[number]	k4	4
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
4	[number]	k4	4
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
C	C	kA	C
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
Sb	sb	kA	sb
<g/>
+	+	kIx~	+
<g/>
4	[number]	k4	4
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
CO	co	k3yInSc1	co
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
Oxid	oxid	k1gInSc1	oxid
antimoničitý	antimoničitý	k2eAgInSc1d1	antimoničitý
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
uhlíkem	uhlík	k1gInSc7	uhlík
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
antimonu	antimon	k1gInSc2	antimon
a	a	k8xC	a
oxidu	oxid	k1gInSc2	oxid
uhelnatého	uhelnatý	k2eAgInSc2d1	uhelnatý
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
možný	možný	k2eAgInSc1d1	možný
způsob	způsob	k1gInSc1	způsob
výroby	výroba	k1gFnSc2	výroba
antimonu	antimon	k1gInSc2	antimon
je	být	k5eAaImIp3nS	být
srážecím	srážecí	k2eAgInSc7d1	srážecí
pochodem	pochod	k1gInSc7	pochod
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
spolu	spolu	k6eAd1	spolu
reaguje	reagovat	k5eAaBmIp3nS	reagovat
antimonit	antimonit	k1gInSc4	antimonit
a	a	k8xC	a
železo	železo	k1gNnSc4	železo
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
antimonit	antimonit	k1gInSc1	antimonit
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
hlušiny	hlušina	k1gFnSc2	hlušina
<g/>
.	.	kIx.	.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
b	b	k?	b
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
S	s	k7c7	s
:	:	kIx,	:
3	[number]	k4	3
:	:	kIx,	:
:	:	kIx,	:
+	+	kIx~	+
3	[number]	k4	3
:	:	kIx,	:
F	F	kA	F
e	e	k0	e
:	:	kIx,	:
→	→	k?	→
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
S	s	k7c7	s
b	b	k?	b
+	+	kIx~	+
3	[number]	k4	3
:	:	kIx,	:
F	F	kA	F
<g />
.	.	kIx.	.
</s>
<s hack="1">
e	e	k0	e
S	s	k7c7	s
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathsf	mathsf	k1gMnSc1	mathsf
{	{	kIx(	{
<g/>
Sb_	Sb_	k1gMnSc1	Sb_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
S_	S_	k1gFnSc2	S_
<g/>
{	{	kIx(	{
<g/>
3	[number]	k4	3
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
3	[number]	k4	3
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
Fe	Fe	k1gMnSc1	Fe
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
Sb	sb	kA	sb
<g/>
+	+	kIx~	+
<g/>
3	[number]	k4	3
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
FeS	fes	k1gNnSc1	fes
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
Sulfid	sulfid	k1gInSc1	sulfid
antimonitý	antimonitý	k2eAgInSc1d1	antimonitý
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
železem	železo	k1gNnSc7	železo
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
antimonu	antimon	k1gInSc2	antimon
a	a	k8xC	a
sulfidu	sulfid	k1gInSc2	sulfid
železnatého	železnatý	k2eAgInSc2d1	železnatý
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
čistý	čistý	k2eAgInSc4d1	čistý
antimon	antimon	k1gInSc4	antimon
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
z	z	k7c2	z
roztoků	roztok	k1gInPc2	roztok
nebo	nebo	k8xC	nebo
tavenin	tavenina	k1gFnPc2	tavenina
elektrolyticky	elektrolyticky	k6eAd1	elektrolyticky
<g/>
.	.	kIx.	.
</s>
<s>
Významné	významný	k2eAgNnSc1d1	významné
uplatnění	uplatnění	k1gNnSc1	uplatnění
nalézá	nalézat	k5eAaImIp3nS	nalézat
antimon	antimon	k1gInSc4	antimon
jako	jako	k8xS	jako
složky	složka	k1gFnPc4	složka
různých	různý	k2eAgFnPc2d1	různá
slitin	slitina	k1gFnPc2	slitina
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
nich	on	k3xPp3gMnPc6	on
však	však	k9	však
tvoří	tvořit	k5eAaImIp3nP	tvořit
pouze	pouze	k6eAd1	pouze
minoritní	minoritní	k2eAgFnSc4d1	minoritní
součást	součást	k1gFnSc4	součást
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pouze	pouze	k6eAd1	pouze
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
vlastnosti	vlastnost	k1gFnPc4	vlastnost
základní	základní	k2eAgFnSc2d1	základní
slitiny	slitina	k1gFnSc2	slitina
-	-	kIx~	-
např.	např.	kA	např.
zvýšení	zvýšení	k1gNnPc4	zvýšení
mechanické	mechanický	k2eAgFnSc2d1	mechanická
pevnosti	pevnost	k1gFnSc2	pevnost
a	a	k8xC	a
odolnosti	odolnost	k1gFnSc2	odolnost
proti	proti	k7c3	proti
chemickým	chemický	k2eAgInPc3d1	chemický
vlivům	vliv	k1gInPc3	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
významné	významný	k2eAgNnSc1d1	významné
použití	použití	k1gNnSc1	použití
je	být	k5eAaImIp3nS	být
využití	využití	k1gNnSc4	využití
sulfidu	sulfid	k1gInSc2	sulfid
antimoničného	antimoničný	k2eAgInSc2d1	antimoničný
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
kaučuku	kaučuk	k1gInSc2	kaučuk
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
typy	typ	k1gInPc1	typ
olověných	olověný	k2eAgInPc2d1	olověný
akumulátorů	akumulátor	k1gInPc2	akumulátor
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
vyráběny	vyrábět	k5eAaImNgInP	vyrábět
ze	z	k7c2	z
slitin	slitina	k1gFnPc2	slitina
olova	olovo	k1gNnSc2	olovo
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
kovy	kov	k1gInPc7	kov
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
výroba	výroba	k1gFnSc1	výroba
pozitivních	pozitivní	k2eAgFnPc2d1	pozitivní
desek	deska	k1gFnPc2	deska
ze	z	k7c2	z
slitiny	slitina	k1gFnSc2	slitina
olova	olovo	k1gNnSc2	olovo
<g/>
,	,	kIx,	,
antimonu	antimon	k1gInSc2	antimon
a	a	k8xC	a
selenu	selen	k1gInSc2	selen
značně	značně	k6eAd1	značně
prodlužuje	prodlužovat	k5eAaImIp3nS	prodlužovat
životnost	životnost	k1gFnSc1	životnost
akumulátoru	akumulátor	k1gInSc2	akumulátor
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
vyšší	vysoký	k2eAgFnSc2d2	vyšší
mechanické	mechanický	k2eAgFnSc2d1	mechanická
pevnosti	pevnost	k1gFnSc2	pevnost
této	tento	k3xDgFnSc2	tento
slitiny	slitina	k1gFnSc2	slitina
<g/>
.	.	kIx.	.
</s>
<s>
Významný	významný	k2eAgMnSc1d1	významný
je	být	k5eAaImIp3nS	být
podíl	podíl	k1gInSc1	podíl
antimonu	antimon	k1gInSc2	antimon
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
pájek	pájka	k1gFnPc2	pájka
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
olova	olovo	k1gNnSc2	olovo
a	a	k8xC	a
cínu	cín	k1gInSc2	cín
<g/>
.	.	kIx.	.
</s>
<s>
Přídavky	přídavek	k1gInPc1	přídavek
antimonu	antimon	k1gInSc2	antimon
<g/>
,	,	kIx,	,
kadmia	kadmium	k1gNnSc2	kadmium
a	a	k8xC	a
stříbra	stříbro	k1gNnSc2	stříbro
získávají	získávat	k5eAaImIp3nP	získávat
tyto	tento	k3xDgFnPc4	tento
pájky	pájka	k1gFnPc4	pájka
lepší	dobrý	k2eAgFnSc4d2	lepší
vodivost	vodivost	k1gFnSc4	vodivost
<g/>
,	,	kIx,	,
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
se	se	k3xPyFc4	se
pevnost	pevnost	k1gFnSc1	pevnost
sváru	svár	k1gInSc2	svár
i	i	k9	i
když	když	k8xS	když
za	za	k7c4	za
cenu	cena	k1gFnSc4	cena
zvýšení	zvýšení	k1gNnSc2	zvýšení
bodu	bod	k1gInSc2	bod
tání	tání	k1gNnSc2	tání
slitiny	slitina	k1gFnSc2	slitina
<g/>
.	.	kIx.	.
</s>
<s>
Slitina	slitina	k1gFnSc1	slitina
o	o	k7c6	o
přibližném	přibližný	k2eAgNnSc6d1	přibližné
složení	složení	k1gNnSc6	složení
75	[number]	k4	75
%	%	kIx~	%
olova	olovo	k1gNnSc2	olovo
<g/>
,	,	kIx,	,
15	[number]	k4	15
%	%	kIx~	%
cínu	cín	k1gInSc2	cín
a	a	k8xC	a
10	[number]	k4	10
%	%	kIx~	%
antimonu	antimon	k1gInSc2	antimon
-	-	kIx~	-
liteřina	liteřina	k1gFnSc1	liteřina
byla	být	k5eAaImAgFnS	být
po	po	k7c4	po
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
základním	základní	k2eAgInSc7d1	základní
materiálem	materiál	k1gInSc7	materiál
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
tiskařských	tiskařský	k2eAgFnPc2d1	tiskařská
liter	litera	k1gFnPc2	litera
-	-	kIx~	-
forem	forum	k1gNnSc7	forum
sazby	sazba	k1gFnSc2	sazba
tisku	tisk	k1gInSc2	tisk
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
slitinu	slitina	k1gFnSc4	slitina
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
proslulý	proslulý	k2eAgMnSc1d1	proslulý
zlatník	zlatník	k1gMnSc1	zlatník
Johannes	Johannes	k1gMnSc1	Johannes
Gutenberg	Gutenberg	k1gMnSc1	Gutenberg
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
knihtisku	knihtisk	k1gInSc2	knihtisk
a	a	k8xC	a
výrobce	výrobce	k1gMnSc1	výrobce
světoznámé	světoznámý	k2eAgFnSc2d1	světoznámá
Gutenbergovy	Gutenbergův	k2eAgFnSc2d1	Gutenbergova
bible	bible	k1gFnSc2	bible
<g/>
.	.	kIx.	.
</s>
<s>
Ložiskový	ložiskový	k2eAgInSc1d1	ložiskový
kov	kov	k1gInSc1	kov
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
cín	cín	k1gInSc1	cín
<g/>
,	,	kIx,	,
olovo	olovo	k1gNnSc1	olovo
<g/>
,	,	kIx,	,
měď	měď	k1gFnSc1	měď
a	a	k8xC	a
antimon	antimon	k1gInSc1	antimon
<g/>
.	.	kIx.	.
</s>
<s>
Rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
se	se	k3xPyFc4	se
dva	dva	k4xCgInPc1	dva
typy	typ	k1gInPc1	typ
ložiskových	ložiskový	k2eAgInPc2d1	ložiskový
kovů	kov	k1gInPc2	kov
-	-	kIx~	-
bílé	bílý	k2eAgFnPc1d1	bílá
a	a	k8xC	a
červené	červený	k2eAgFnPc1d1	červená
<g/>
.	.	kIx.	.
</s>
<s>
Bílé	bílé	k1gNnSc4	bílé
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
90	[number]	k4	90
%	%	kIx~	%
cínu	cín	k1gInSc2	cín
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
%	%	kIx~	%
antimonu	antimon	k1gInSc2	antimon
a	a	k8xC	a
několik	několik	k4yIc1	několik
procent	procento	k1gNnPc2	procento
mědi	měď	k1gFnSc2	měď
<g/>
.	.	kIx.	.
</s>
<s>
Červené	Červená	k1gFnPc1	Červená
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
75	[number]	k4	75
<g/>
-	-	kIx~	-
<g/>
90	[number]	k4	90
%	%	kIx~	%
mědi	měď	k1gFnSc2	měď
<g/>
,	,	kIx,	,
10	[number]	k4	10
%	%	kIx~	%
cínu	cín	k1gInSc2	cín
a	a	k8xC	a
menší	malý	k2eAgNnSc4d2	menší
množství	množství	k1gNnSc4	množství
zinku	zinek	k1gInSc2	zinek
<g/>
,	,	kIx,	,
olova	olovo	k1gNnSc2	olovo
a	a	k8xC	a
antimonu	antimon	k1gInSc2	antimon
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
především	především	k9	především
vysokou	vysoký	k2eAgFnSc7d1	vysoká
odolností	odolnost	k1gFnSc7	odolnost
proti	proti	k7c3	proti
otěru	otěr	k1gInSc3	otěr
i	i	k9	i
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
měkké	měkký	k2eAgFnPc1d1	měkká
-	-	kIx~	-
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
kluzných	kluzný	k2eAgNnPc2d1	kluzné
ložisek	ložisko	k1gNnPc2	ložisko
pro	pro	k7c4	pro
automobilový	automobilový	k2eAgInSc4d1	automobilový
průmysl	průmysl	k1gInSc4	průmysl
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
aplikace	aplikace	k1gFnPc4	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Známá	známý	k2eAgFnSc1d1	známá
soška	soška	k1gFnSc1	soška
filmového	filmový	k2eAgMnSc2d1	filmový
Oskara	Oskar	k1gMnSc2	Oskar
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1930	[number]	k4	1930
vyráběna	vyrábět	k5eAaImNgFnS	vyrábět
ze	z	k7c2	z
slitiny	slitina	k1gFnSc2	slitina
cínu	cín	k1gInSc2	cín
<g/>
,	,	kIx,	,
mědi	měď	k1gFnSc2	měď
a	a	k8xC	a
antimonu	antimon	k1gInSc2	antimon
<g/>
,	,	kIx,	,
pokrytá	pokrytý	k2eAgFnSc1d1	pokrytá
vrstvou	vrstva	k1gFnSc7	vrstva
niklu	nikl	k1gInSc2	nikl
<g/>
,	,	kIx,	,
stříbra	stříbro	k1gNnSc2	stříbro
a	a	k8xC	a
konečně	konečně	k6eAd1	konečně
čistého	čistý	k2eAgInSc2d1	čistý
24	[number]	k4	24
<g/>
karátového	karátový	k2eAgNnSc2d1	karátové
zlata	zlato	k1gNnSc2	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Vzácně	vzácně	k6eAd1	vzácně
je	být	k5eAaImIp3nS	být
antimon	antimon	k1gInSc1	antimon
legován	legován	k2eAgInSc1d1	legován
do	do	k7c2	do
dentálních	dentální	k2eAgFnPc2d1	dentální
slitin	slitina	k1gFnPc2	slitina
<g/>
,	,	kIx,	,
používaných	používaný	k2eAgInPc2d1	používaný
v	v	k7c6	v
zubním	zubní	k2eAgNnSc6d1	zubní
lékařství	lékařství	k1gNnSc6	lékařství
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
kovy	kov	k1gInPc7	kov
dentálních	dentální	k2eAgFnPc2d1	dentální
slitin	slitina	k1gFnPc2	slitina
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
antimonu	antimon	k1gInSc2	antimon
jsou	být	k5eAaImIp3nP	být
palladium	palladium	k1gNnSc4	palladium
a	a	k8xC	a
stříbro	stříbro	k1gNnSc4	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Přídavek	přídavek	k1gInSc1	přídavek
definovaného	definovaný	k2eAgNnSc2d1	definované
množství	množství	k1gNnSc2	množství
atomů	atom	k1gInPc2	atom
antimonu	antimon	k1gInSc2	antimon
do	do	k7c2	do
krystalu	krystal	k1gInSc2	krystal
superčistého	superčistý	k2eAgInSc2d1	superčistý
křemíku	křemík	k1gInSc2	křemík
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
polovodič	polovodič	k1gInSc1	polovodič
typu	typ	k1gInSc2	typ
N	N	kA	N
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
komponent	komponenta	k1gFnPc2	komponenta
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
základních	základní	k2eAgFnPc2d1	základní
součástí	součást	k1gFnPc2	součást
současné	současný	k2eAgFnSc2d1	současná
elektroniky	elektronika	k1gFnSc2	elektronika
-	-	kIx~	-
diod	dioda	k1gFnPc2	dioda
a	a	k8xC	a
tranzistorů	tranzistor	k1gInPc2	tranzistor
<g/>
.	.	kIx.	.
</s>
<s>
Optické	optický	k2eAgInPc1d1	optický
disky	disk	k1gInPc1	disk
(	(	kIx(	(
<g/>
CD	CD	kA	CD
<g/>
,	,	kIx,	,
DVD	DVD	kA	DVD
<g/>
,	,	kIx,	,
Blu-ray	Bluaa	k1gFnPc1	Blu-raa
<g/>
)	)	kIx)	)
s	s	k7c7	s
možností	možnost	k1gFnSc7	možnost
vícenásobného	vícenásobný	k2eAgInSc2d1	vícenásobný
zápisu	zápis	k1gInSc2	zápis
používají	používat	k5eAaImIp3nP	používat
pro	pro	k7c4	pro
záznam	záznam	k1gInSc4	záznam
dat	datum	k1gNnPc2	datum
vrstvy	vrstva	k1gFnSc2	vrstva
nejčastěji	často	k6eAd3	často
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
slitin	slitina	k1gFnPc2	slitina
germanium-antimon-tellur	germaniumntimonellura	k1gFnPc2	germanium-antimon-tellura
nebo	nebo	k8xC	nebo
stříbro-indium-antimon-tellur	stříbrondiumntimonellura	k1gFnPc2	stříbro-indium-antimon-tellura
<g/>
.	.	kIx.	.
</s>
<s>
Záznam	záznam	k1gInSc1	záznam
spočívá	spočívat	k5eAaImIp3nS	spočívat
ve	v	k7c6	v
změně	změna	k1gFnSc6	změna
struktury	struktura	k1gFnSc2	struktura
materiálu	materiál	k1gInSc2	materiál
z	z	k7c2	z
krystalické	krystalický	k2eAgFnSc2d1	krystalická
do	do	k7c2	do
amorfní	amorfní	k2eAgFnSc2d1	amorfní
formy	forma	k1gFnSc2	forma
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
obě	dva	k4xCgFnPc1	dva
formy	forma	k1gFnPc1	forma
mají	mít	k5eAaImIp3nP	mít
významně	významně	k6eAd1	významně
odlišné	odlišný	k2eAgFnPc1d1	odlišná
optické	optický	k2eAgFnPc1d1	optická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Zahřeje	zahřát	k5eAaPmIp3nS	zahřát
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
hmota	hmota	k1gFnSc1	hmota
laserem	laser	k1gInSc7	laser
nad	nad	k7c4	nad
určitou	určitý	k2eAgFnSc4d1	určitá
teplotu	teplota	k1gFnSc4	teplota
(	(	kIx(	(
<g/>
teplota	teplota	k1gFnSc1	teplota
krystalizace	krystalizace	k1gFnSc2	krystalizace
<g/>
)	)	kIx)	)
a	a	k8xC	a
poté	poté	k6eAd1	poté
ochladí	ochladit	k5eAaPmIp3nS	ochladit
<g/>
,	,	kIx,	,
získává	získávat	k5eAaImIp3nS	získávat
krystalickou	krystalický	k2eAgFnSc4d1	krystalická
strukturu	struktura	k1gFnSc4	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
však	však	k9	však
zahřáta	zahřát	k5eAaPmNgFnS	zahřát
nad	nad	k7c4	nad
teplotu	teplota	k1gFnSc4	teplota
tání	tání	k1gNnSc2	tání
a	a	k8xC	a
poté	poté	k6eAd1	poté
prudce	prudko	k6eAd1	prudko
ochlazena	ochlazen	k2eAgFnSc1d1	ochlazena
<g/>
,	,	kIx,	,
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
amorfního	amorfní	k2eAgInSc2d1	amorfní
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
neuspořádaného	uspořádaný	k2eNgInSc2d1	neuspořádaný
<g/>
)	)	kIx)	)
stavu	stav	k1gInSc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vodíkem	vodík	k1gInSc7	vodík
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
antimon	antimon	k1gInSc1	antimon
plynný	plynný	k2eAgInSc1d1	plynný
antimonovodík	antimonovodík	k1gInSc1	antimonovodík
neboli	neboli	k8xC	neboli
stiban	stiban	k1gInSc1	stiban
SbH	SbH	k1gFnSc2	SbH
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zapáchající	zapáchající	k2eAgMnSc1d1	zapáchající
<g/>
,	,	kIx,	,
bezbarvý	bezbarvý	k2eAgMnSc1d1	bezbarvý
a	a	k8xC	a
snadno	snadno	k6eAd1	snadno
zápalný	zápalný	k2eAgInSc1d1	zápalný
plyn	plyn	k1gInSc1	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
působením	působení	k1gNnSc7	působení
vodíku	vodík	k1gInSc2	vodík
ve	v	k7c6	v
stavu	stav	k1gInSc6	stav
zrodu	zrod	k1gInSc2	zrod
na	na	k7c4	na
rozpustné	rozpustný	k2eAgFnPc4d1	rozpustná
sloučeniny	sloučenina	k1gFnPc4	sloučenina
antimonu	antimon	k1gInSc2	antimon
<g/>
.	.	kIx.	.
</s>
<s>
Hořením	hoření	k1gNnSc7	hoření
stibanu	stiban	k1gInSc2	stiban
vzniká	vznikat	k5eAaImIp3nS	vznikat
oxid	oxid	k1gInSc1	oxid
antimonitý	antimonitý	k2eAgInSc1d1	antimonitý
Sb	sb	kA	sb
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Sulfidy	sulfid	k1gInPc1	sulfid
antimonu	antimon	k1gInSc2	antimon
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
nerozpustné	rozpustný	k2eNgFnSc2d1	nerozpustná
sloučeniny	sloučenina	k1gFnSc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Sulfid	sulfid	k1gInSc1	sulfid
antimonitý	antimonitý	k2eAgInSc1d1	antimonitý
Sb	sb	kA	sb
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
po	po	k7c4	po
vysrážení	vysrážení	k1gNnSc4	vysrážení
ze	z	k7c2	z
studeného	studený	k2eAgInSc2d1	studený
roztoku	roztok	k1gInSc2	roztok
oranžovočervený	oranžovočervený	k2eAgInSc1d1	oranžovočervený
<g/>
,	,	kIx,	,
po	po	k7c4	po
vysrážení	vysrážení	k1gNnSc4	vysrážení
z	z	k7c2	z
teplejších	teplý	k2eAgInPc2d2	teplejší
roztoků	roztok	k1gInPc2	roztok
nebo	nebo	k8xC	nebo
při	při	k7c6	při
zahřívání	zahřívání	k1gNnSc6	zahřívání
oranžovočerveného	oranžovočervený	k2eAgInSc2d1	oranžovočervený
sulfidu	sulfid	k1gInSc2	sulfid
<g/>
,	,	kIx,	,
přechází	přecházet	k5eAaImIp3nS	přecházet
na	na	k7c4	na
šedočernou	šedočerný	k2eAgFnSc4d1	šedočerná
modifikaci	modifikace	k1gFnSc4	modifikace
<g/>
.	.	kIx.	.
</s>
<s>
Sulfid	sulfid	k1gInSc1	sulfid
antimonitý	antimonitý	k2eAgInSc1d1	antimonitý
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
sloučenin	sloučenina	k1gFnPc2	sloučenina
používaných	používaný	k2eAgFnPc2d1	používaná
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
zápalek	zápalka	k1gFnPc2	zápalka
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc7d1	základní
rudou	ruda	k1gFnSc7	ruda
používanou	používaný	k2eAgFnSc7d1	používaná
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
antimonu	antimon	k1gInSc2	antimon
<g/>
.	.	kIx.	.
</s>
<s>
Sulfid	sulfid	k1gInSc1	sulfid
antimoničný	antimoničný	k2eAgInSc1d1	antimoničný
Sb	sb	kA	sb
<g/>
2	[number]	k4	2
<g/>
S	s	k7c7	s
<g/>
5	[number]	k4	5
je	být	k5eAaImIp3nS	být
oranžový	oranžový	k2eAgInSc1d1	oranžový
prášek	prášek	k1gInSc1	prášek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
k	k	k7c3	k
vulkanizaci	vulkanizace	k1gFnSc3	vulkanizace
kaučuku	kaučuk	k1gInSc2	kaučuk
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
halogenidy	halogenid	k1gInPc1	halogenid
lze	lze	k6eAd1	lze
(	(	kIx(	(
<g/>
až	až	k9	až
na	na	k7c4	na
chlorid	chlorid	k1gInSc4	chlorid
antimoničitý	antimoničitý	k2eAgInSc4d1	antimoničitý
SbCl	SbCl	k1gInSc4	SbCl
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
připravit	připravit	k5eAaPmF	připravit
přímým	přímý	k2eAgNnSc7d1	přímé
sloučením	sloučení	k1gNnSc7	sloučení
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Halogenidy	Halogenid	k1gInPc1	Halogenid
antimonu	antimon	k1gInSc2	antimon
snadno	snadno	k6eAd1	snadno
hydrolyzují	hydrolyzovat	k5eAaBmIp3nP	hydrolyzovat
i	i	k9	i
vzdušnou	vzdušný	k2eAgFnSc7d1	vzdušná
vlhkostí	vlhkost	k1gFnSc7	vlhkost
<g/>
,	,	kIx,	,
nejstálejší	stálý	k2eAgMnSc1d3	nejstálejší
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
jsou	být	k5eAaImIp3nP	být
fluoridy	fluorid	k1gInPc1	fluorid
<g/>
.	.	kIx.	.
</s>
<s>
Fluorid	fluorid	k1gInSc4	fluorid
antimonitý	antimonitý	k2eAgInSc4d1	antimonitý
SbF	SbF	k1gFnSc7	SbF
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
rozplývající	rozplývající	k2eAgMnSc1d1	rozplývající
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
podvojné	podvojný	k2eAgFnPc4d1	podvojná
soli	sůl	k1gFnPc4	sůl
s	s	k7c7	s
chloridy	chlorid	k1gInPc7	chlorid
a	a	k8xC	a
sírany	síran	k1gInPc7	síran
alkalických	alkalický	k2eAgInPc2d1	alkalický
kovů	kov	k1gInPc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Fluorid	fluorid	k1gInSc4	fluorid
antimoničný	antimoničný	k2eAgInSc4d1	antimoničný
SbF	SbF	k1gFnSc7	SbF
<g/>
5	[number]	k4	5
je	být	k5eAaImIp3nS	být
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
olejovitá	olejovitý	k2eAgFnSc1d1	olejovitá
kapalina	kapalina	k1gFnSc1	kapalina
<g/>
.	.	kIx.	.
</s>
<s>
Snadno	snadno	k6eAd1	snadno
tvoří	tvořit	k5eAaImIp3nP	tvořit
podvojné	podvojný	k2eAgFnPc1d1	podvojná
soli	sůl	k1gFnPc1	sůl
a	a	k8xC	a
adiční	adiční	k2eAgFnPc1d1	adiční
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
antimonitý	antimonitý	k2eAgInSc4d1	antimonitý
SbCl	SbCl	k1gInSc4	SbCl
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
měkká	měkký	k2eAgFnSc1d1	měkká
<g/>
,	,	kIx,	,
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
<g/>
,	,	kIx,	,
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
dýmající	dýmající	k2eAgFnSc1d1	dýmající
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
moření	moření	k1gNnSc3	moření
kovů	kov	k1gInPc2	kov
a	a	k8xC	a
v	v	k7c6	v
medicíně	medicína	k1gFnSc6	medicína
pro	pro	k7c4	pro
leptání	leptání	k1gNnSc4	leptání
<g/>
.	.	kIx.	.
</s>
<s>
Snadno	snadno	k6eAd1	snadno
tvoří	tvořit	k5eAaImIp3nP	tvořit
adiční	adiční	k2eAgFnPc1d1	adiční
sloučeniny	sloučenina	k1gFnPc1	sloučenina
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
podvojné	podvojný	k2eAgFnPc1d1	podvojná
soli	sůl	k1gFnPc1	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
antimoničitý	antimoničitý	k2eAgInSc4d1	antimoničitý
SbCl	SbCl	k1gInSc4	SbCl
<g/>
4	[number]	k4	4
je	být	k5eAaImIp3nS	být
tmavohnědá	tmavohnědý	k2eAgFnSc1d1	tmavohnědá
kapalina	kapalina	k1gFnSc1	kapalina
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
nelze	lze	k6eNd1	lze
připravit	připravit	k5eAaPmF	připravit
v	v	k7c6	v
čistém	čistý	k2eAgInSc6d1	čistý
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
rovnováze	rovnováha	k1gFnSc6	rovnováha
s	s	k7c7	s
chloridem	chlorid	k1gInSc7	chlorid
antimonitým	antimonitý	k2eAgInSc7d1	antimonitý
a	a	k8xC	a
antimoničným	antimoničný	k2eAgInSc7d1	antimoničný
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
antimoničitý	antimoničitý	k2eAgInSc1d1	antimoničitý
tvoří	tvořit	k5eAaImIp3nS	tvořit
komplexní	komplexní	k2eAgFnPc4d1	komplexní
soli	sůl	k1gFnPc4	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
antimoničný	antimoničný	k2eAgInSc4d1	antimoničný
SbCl	SbCl	k1gInSc4	SbCl
<g/>
5	[number]	k4	5
je	být	k5eAaImIp3nS	být
nažloutlá	nažloutlý	k2eAgFnSc1d1	nažloutlá
olejovitá	olejovitý	k2eAgFnSc1d1	olejovitá
kapalina	kapalina	k1gFnSc1	kapalina
<g/>
.	.	kIx.	.
</s>
<s>
Nalézá	nalézat	k5eAaImIp3nS	nalézat
uplatnění	uplatnění	k1gNnSc4	uplatnění
především	především	k9	především
v	v	k7c6	v
organické	organický	k2eAgFnSc6d1	organická
syntéze	syntéza	k1gFnSc6	syntéza
jako	jako	k8xC	jako
chlorační	chlorační	k2eAgNnSc1d1	chlorační
činidlo	činidlo	k1gNnSc1	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
Bromid	bromid	k1gInSc1	bromid
antimonitý	antimonitý	k2eAgInSc4d1	antimonitý
SbBr	SbBr	k1gInSc4	SbBr
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Snadno	snadno	k6eAd1	snadno
tvoří	tvořit	k5eAaImIp3nP	tvořit
podvojné	podvojný	k2eAgFnPc1d1	podvojná
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Bromid	bromid	k1gInSc1	bromid
antimoničný	antimoničný	k2eAgInSc4d1	antimoničný
SbBr	SbBr	k1gInSc4	SbBr
<g/>
5	[number]	k4	5
neexistuje	existovat	k5eNaImIp3nS	existovat
volný	volný	k2eAgInSc1d1	volný
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
svých	svůj	k3xOyFgFnPc2	svůj
podvojných	podvojný	k2eAgFnPc2d1	podvojná
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Jodid	jodid	k1gInSc4	jodid
antimonitý	antimonitý	k2eAgInSc4d1	antimonitý
SbI	SbI	k1gFnSc7	SbI
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
rubínově	rubínově	k6eAd1	rubínově
červená	červený	k2eAgFnSc1d1	červená
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
tvořící	tvořící	k2eAgFnPc1d1	tvořící
snadno	snadno	k6eAd1	snadno
adiční	adiční	k2eAgFnPc1d1	adiční
sloučeniny	sloučenina	k1gFnPc1	sloučenina
s	s	k7c7	s
jodem	jod	k1gInSc7	jod
<g/>
.	.	kIx.	.
</s>
<s>
Jodid	jodid	k1gInSc4	jodid
antimoničný	antimoničný	k2eAgInSc4d1	antimoničný
SbI	SbI	k1gFnSc7	SbI
<g/>
5	[number]	k4	5
je	být	k5eAaImIp3nS	být
temně	temně	k6eAd1	temně
hnědá	hnědý	k2eAgFnSc1d1	hnědá
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Oxidy	oxid	k1gInPc1	oxid
antimonu	antimon	k1gInSc2	antimon
slouží	sloužit	k5eAaImIp3nP	sloužit
při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
různých	různý	k2eAgInPc2d1	různý
barevných	barevný	k2eAgInPc2d1	barevný
pigmentů	pigment	k1gInPc2	pigment
a	a	k8xC	a
barvení	barvení	k1gNnSc2	barvení
keramiky	keramika	k1gFnSc2	keramika
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
antimonitý	antimonitý	k2eAgInSc1d1	antimonitý
Sb	sb	kA	sb
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
popř.	popř.	kA	popř.
Sb	sb	kA	sb
<g/>
4	[number]	k4	4
<g/>
O	o	k7c4	o
<g/>
6	[number]	k4	6
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
práškovitá	práškovitý	k2eAgFnSc1d1	práškovitá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
při	při	k7c6	při
větším	veliký	k2eAgNnSc6d2	veliký
zahřívání	zahřívání	k1gNnSc6	zahřívání
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c4	v
žlutou	žlutý	k2eAgFnSc4d1	žlutá
modifikaci	modifikace	k1gFnSc4	modifikace
a	a	k8xC	a
při	při	k7c6	při
ochlazení	ochlazení	k1gNnSc3	ochlazení
zpět	zpět	k6eAd1	zpět
v	v	k7c4	v
bílou	bílý	k2eAgFnSc4d1	bílá
modifikaci	modifikace	k1gFnSc4	modifikace
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
antimonitý	antimonitý	k2eAgInSc1d1	antimonitý
je	být	k5eAaImIp3nS	být
amfoterní	amfoterní	k2eAgInSc1d1	amfoterní
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
antimoničitý	antimoničitý	k2eAgInSc1d1	antimoničitý
Sb	sb	kA	sb
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
4	[number]	k4	4
lépe	dobře	k6eAd2	dobře
oxid	oxid	k1gInSc1	oxid
antimonito-antimoničný	antimonitontimoničný	k2eAgInSc1d1	antimonito-antimoničný
je	být	k5eAaImIp3nS	být
bílý	bílý	k2eAgInSc1d1	bílý
prášek	prášek	k1gInSc1	prášek
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
zahřívání	zahřívání	k1gNnSc6	zahřívání
oxidu	oxid	k1gInSc2	oxid
antimonitého	antimonitý	k2eAgInSc2d1	antimonitý
nebo	nebo	k8xC	nebo
antimoničitého	antimoničitý	k2eAgInSc2d1	antimoničitý
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
800-900	[number]	k4	800-900
°	°	k?	°
<g/>
C.	C.	kA	C.
Oxid	oxid	k1gInSc4	oxid
antimoničný	antimoničný	k2eAgInSc1d1	antimoničný
Sb	sb	kA	sb
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
5	[number]	k4	5
je	být	k5eAaImIp3nS	být
nažloutlý	nažloutlý	k2eAgInSc1d1	nažloutlý
prášek	prášek	k1gInSc1	prášek
<g/>
.	.	kIx.	.
</s>
<s>
Získává	získávat	k5eAaImIp3nS	získávat
se	se	k3xPyFc4	se
oxidací	oxidace	k1gFnSc7	oxidace
oxidu	oxid	k1gInSc2	oxid
antimoničitého	antimoničitý	k2eAgInSc2d1	antimoničitý
<g/>
.	.	kIx.	.
</s>
<s>
Vinan	Vinan	k1gInSc1	Vinan
antimonylo-draselný	antimonyloraselný	k2eAgInSc1d1	antimonylo-draselný
byl	být	k5eAaImAgInS	být
znám	znám	k2eAgInSc1d1	znám
již	již	k6eAd1	již
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
K	k	k7c3	k
<g/>
[	[	kIx(	[
<g/>
C	C	kA	C
<g/>
4	[number]	k4	4
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
6	[number]	k4	6
<g/>
Sb	sb	kA	sb
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
]	]	kIx)	]
<g/>
.1	.1	k4	.1
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
jako	jako	k9	jako
dávivý	dávivý	k2eAgInSc4d1	dávivý
kámen	kámen	k1gInSc4	kámen
neboli	neboli	k8xC	neboli
tartarus	tartarus	k1gMnSc1	tartarus
emeticus	emeticus	k1gMnSc1	emeticus
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
sloučenina	sloučenina	k1gFnSc1	sloučenina
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
po	po	k7c6	po
požití	požití	k1gNnSc6	požití
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
zvracení	zvracení	k1gNnSc1	zvracení
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
všechny	všechen	k3xTgFnPc4	všechen
rozpustné	rozpustný	k2eAgFnPc4d1	rozpustná
soli	sůl	k1gFnPc4	sůl
antimonu	antimon	k1gInSc2	antimon
jedovatý	jedovatý	k2eAgMnSc1d1	jedovatý
<g/>
.	.	kIx.	.
</s>
<s>
Síran	síran	k1gInSc1	síran
antimonitý	antimonitý	k2eAgInSc1d1	antimonitý
Sb	sb	kA	sb
<g/>
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
SO	So	kA	So
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
bezbarvá	bezbarvý	k2eAgFnSc1d1	bezbarvá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Získává	získávat	k5eAaImIp3nS	získávat
se	se	k3xPyFc4	se
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
antimonu	antimon	k1gInSc2	antimon
<g/>
,	,	kIx,	,
oxid	oxid	k1gInSc1	oxid
antimonitého	antimonitý	k2eAgMnSc2d1	antimonitý
nebo	nebo	k8xC	nebo
sulfid	sulfid	k1gInSc1	sulfid
antimonitého	antimonitý	k2eAgInSc2d1	antimonitý
v	v	k7c6	v
horké	horký	k2eAgFnSc6d1	horká
koncentrované	koncentrovaný	k2eAgFnSc6d1	koncentrovaná
kyselině	kyselina	k1gFnSc6	kyselina
sírové	sírový	k2eAgFnSc6d1	sírová
<g/>
.	.	kIx.	.
</s>
<s>
Dusičnan	dusičnan	k1gInSc4	dusičnan
antimonitý	antimonitý	k2eAgInSc4d1	antimonitý
Sb	sb	kA	sb
<g/>
(	(	kIx(	(
<g/>
NO	no	k9	no
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
reakcí	reakce	k1gFnSc7	reakce
oxidu	oxid	k1gInSc2	oxid
antimonitého	antimonitý	k2eAgNnSc2d1	antimonitý
s	s	k7c7	s
dýmavou	dýmavý	k2eAgFnSc7d1	dýmavá
kyselinou	kyselina	k1gFnSc7	kyselina
dusičnou	dusičný	k2eAgFnSc7d1	dusičná
<g/>
.	.	kIx.	.
</s>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
<g/>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc4	vydání
1961	[number]	k4	1961
Galerie	galerie	k1gFnSc2	galerie
antimon	antimon	k1gInSc4	antimon
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc4	Commons
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
antimon	antimon	k1gInSc1	antimon
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
antimon	antimon	k1gInSc1	antimon
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
