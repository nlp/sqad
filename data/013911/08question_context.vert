<s>
Po	po	k7c6
absolvování	absolvování	k1gNnSc6
oboru	obor	k1gInSc2
Strojírenská	strojírenský	k2eAgFnSc1d1
technologie	technologie	k1gFnSc1
na	na	k7c6
Střední	střední	k2eAgFnSc6d1
průmyslové	průmyslový	k2eAgFnSc6d1
škole	škola	k1gFnSc6
na	na	k7c6
Třebešíně	Třebešína	k1gFnSc6
zahájil	zahájit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1994	#num#	k4
vysokoškolská	vysokoškolský	k2eAgNnPc1d1
studia	studio	k1gNnPc1
na	na	k7c6
Vysoké	vysoký	k2eAgFnSc6d1
škole	škola	k1gFnSc6
ekonomické	ekonomický	k2eAgFnSc6d1
<g/>
,	,	kIx,
kde	kde	k6eAd1
si	se	k3xPyFc3
jako	jako	k8xC,k8xS
svůj	svůj	k3xOyFgInSc4
obor	obor	k1gInSc4
vybral	vybrat	k5eAaPmAgMnS
statistiku	statistika	k1gFnSc4
a	a	k8xC
ekonometrii	ekonometrie	k1gFnSc4
<g/>
.	.	kIx.
</s>