<s>
Zbyněk	Zbyněk	k1gMnSc1
Passer	Passer	k1gMnSc1
</s>
<s>
Zbyněk	Zbyněk	k1gMnSc1
Passer	Passer	k1gMnSc1
</s>
<s>
předseda	předseda	k1gMnSc1
České	český	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
přátel	přítel	k1gMnPc2
Izraele	Izrael	k1gInSc2
Úřadující	úřadující	k2eAgMnSc1d1
</s>
<s>
Ve	v	k7c4
funkci	funkce	k1gFnSc4
od	od	k7c2
<g/>
:	:	kIx,
<g/>
prosince	prosinec	k1gInSc2
2016	#num#	k4
</s>
<s>
zastupitel	zastupitel	k1gMnSc1
místní	místní	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha-Koloděje	Praha-Koloděje	k1gFnSc2
<g/>
(	(	kIx(
<g/>
v	v	k7c6
letech	léto	k1gNnPc6
2005-2012	2005-2012	k4
také	také	k9
místostarosta	místostarosta	k1gMnSc1
MČ	MČ	kA
<g/>
)	)	kIx)
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1998	#num#	k4
–	–	k?
11	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2016	#num#	k4
</s>
<s>
starosta	starosta	k1gMnSc1
místní	místní	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha-Koloděje	Praha-Koloděje	k1gFnSc2
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
2012	#num#	k4
–	–	k?
2015	#num#	k4
Předchůdce	předchůdce	k1gMnSc2
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1
Kovář	Kovář	k1gMnSc1
Nástupce	nástupce	k1gMnSc1
</s>
<s>
Angela	Angela	k1gFnSc1
Morávková	Morávková	k1gFnSc1
</s>
<s>
Stranická	stranický	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
Členství	členství	k1gNnSc2
</s>
<s>
nezávislýPro	závislýPro	k6eNd1
Prahu	Praha	k1gFnSc4
(	(	kIx(
<g/>
od	od	k7c2
2013	#num#	k4
do	do	k7c2
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1974	#num#	k4
(	(	kIx(
<g/>
46	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
PrahaČeskoslovensko	PrahaČeskoslovensko	k1gNnSc1
Československo	Československo	k1gNnSc1
Národnost	národnost	k1gFnSc4
</s>
<s>
česká	český	k2eAgFnSc1d1
Choť	choť	k1gFnSc1
</s>
<s>
Mgr.	Mgr.	kA
Marie	Marie	k1gFnSc1
Passerová	Passerová	k1gFnSc1
Vztahy	vztah	k1gInPc1
</s>
<s>
Radim	Radim	k1gMnSc1
Passer	Passer	k1gMnSc1
(	(	kIx(
<g/>
bratr	bratr	k1gMnSc1
<g/>
)	)	kIx)
<g/>
Petra	Petra	k1gFnSc1
Venturová	Venturový	k2eAgFnSc1d1
(	(	kIx(
<g/>
sestřenice	sestřenice	k1gFnSc1
<g/>
)	)	kIx)
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Radim	Radim	k1gMnSc1
Passer	Passer	k1gMnSc1
(	(	kIx(
<g/>
sourozenec	sourozenec	k1gMnSc1
<g/>
)	)	kIx)
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
9	#num#	k4
-	-	kIx~
Koloděje	Koloděje	k1gFnPc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
VŠE	všechen	k3xTgNnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
Náboženství	náboženství	k1gNnSc2
</s>
<s>
římskokatolické	římskokatolický	k2eAgFnSc3d1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Zbyněk	Zbyněk	k1gMnSc1
Passer	Passer	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
12	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1974	#num#	k4
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
bývalý	bývalý	k2eAgMnSc1d1
český	český	k2eAgMnSc1d1
komunální	komunální	k2eAgMnSc1d1
politik	politik	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
současnosti	současnost	k1gFnSc6
investor	investor	k1gMnSc1
a	a	k8xC
podnikatel	podnikatel	k1gMnSc1
židovského	židovský	k2eAgInSc2d1
původu	původ	k1gInSc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
od	od	k7c2
ledna	leden	k1gInSc2
2014	#num#	k4
výkonný	výkonný	k2eAgMnSc1d1
místopředseda	místopředseda	k1gMnSc1
politického	politický	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
Pro	pro	k7c4
Prahu	Praha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
dlouholetým	dlouholetý	k2eAgMnSc7d1
zastupitelem	zastupitel	k1gMnSc7
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Praha-Koloděje	Praha-Koloděje	k1gFnSc2
<g/>
,	,	kIx,
v	v	k7c6
letech	let	k1gInPc6
2012	#num#	k4
<g/>
–	–	k?
<g/>
2015	#num#	k4
zde	zde	k6eAd1
byl	být	k5eAaImAgInS
starostou	starosta	k1gMnSc7
a	a	k8xC
v	v	k7c6
letech	let	k1gInPc6
2005	#num#	k4
<g/>
–	–	k?
<g/>
2012	#num#	k4
místostarostou	místostarosta	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Životopis	životopis	k1gInSc1
</s>
<s>
Po	po	k7c6
absolvování	absolvování	k1gNnSc6
oboru	obor	k1gInSc2
Strojírenská	strojírenský	k2eAgFnSc1d1
technologie	technologie	k1gFnSc1
na	na	k7c6
Střední	střední	k2eAgFnSc6d1
průmyslové	průmyslový	k2eAgFnSc6d1
škole	škola	k1gFnSc6
na	na	k7c6
Třebešíně	Třebešína	k1gFnSc6
zahájil	zahájit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1994	#num#	k4
vysokoškolská	vysokoškolský	k2eAgNnPc1d1
studia	studio	k1gNnPc1
na	na	k7c6
Vysoké	vysoký	k2eAgFnSc6d1
škole	škola	k1gFnSc6
ekonomické	ekonomický	k2eAgFnSc6d1
<g/>
,	,	kIx,
kde	kde	k6eAd1
si	se	k3xPyFc3
jako	jako	k8xC,k8xS
svůj	svůj	k3xOyFgInSc4
obor	obor	k1gInSc4
vybral	vybrat	k5eAaPmAgMnS
statistiku	statistika	k1gFnSc4
a	a	k8xC
ekonometrii	ekonometrie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ještě	ještě	k6eAd1
během	během	k7c2
studií	studio	k1gNnPc2
na	na	k7k6
VŠE	VŠE	kA
(	(	kIx(
<g/>
kterou	který	k3yRgFnSc4
na	na	k7c6
základě	základ	k1gInSc6
své	svůj	k3xOyFgFnSc2
pracovní	pracovní	k2eAgFnSc2d1
vytíženosti	vytíženost	k1gFnSc2
nedokončil	dokončit	k5eNaPmAgInS
<g/>
)	)	kIx)
pracoval	pracovat	k5eAaImAgInS
na	na	k7c6
obchodních	obchodní	k2eAgFnPc6d1
a	a	k8xC
marketingových	marketingový	k2eAgFnPc6d1
pozicích	pozice	k1gFnPc6
v	v	k7c6
rodinných	rodinný	k2eAgFnPc6d1
společnostech	společnost	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1997	#num#	k4
pracoval	pracovat	k5eAaImAgMnS
v	v	k7c6
mezinárodním	mezinárodní	k2eAgInSc6d1
obchodě	obchod	k1gInSc6
pro	pro	k7c4
společnost	společnost	k1gFnSc4
Ideal	Ideal	k1gMnSc1
Lux	Lux	k1gMnSc1
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1999	#num#	k4
jako	jako	k8xC,k8xS
jednatel	jednatel	k1gMnSc1
společnosti	společnost	k1gFnSc2
CRS	CRS	kA
Network	network	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
2002	#num#	k4
<g/>
–	–	k?
<g/>
2007	#num#	k4
byl	být	k5eAaImAgMnS
členem	člen	k1gMnSc7
představenstva	představenstvo	k1gNnSc2
společnosti	společnost	k1gFnSc2
Passerinvest	Passerinvest	k1gMnSc1
Group	Group	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
nadále	nadále	k6eAd1
působí	působit	k5eAaImIp3nS
jako	jako	k9
člen	člen	k1gMnSc1
dozorčí	dozorčí	k2eAgFnSc2d1
rady	rada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
podniká	podnikat	k5eAaImIp3nS
mimo	mimo	k7c4
jiné	jiné	k1gNnSc4
jako	jako	k8xS,k8xC
řídící	řídící	k2eAgMnSc1d1
partner	partner	k1gMnSc1
v	v	k7c6
oblastech	oblast	k1gFnPc6
investic	investice	k1gFnPc2
do	do	k7c2
drahých	drahý	k2eAgInPc2d1
kovů	kov	k1gInPc2
(	(	kIx(
<g/>
[	[	kIx(
<g/>
GmbH	GmbH	k1gFnSc1
<g/>
)	)	kIx)
<g/>
]	]	kIx)
<g/>
,	,	kIx,
médií	médium	k1gNnPc2
(	(	kIx(
<g/>
Svobodné	svobodný	k2eAgNnSc1d1
Universum	universum	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
edicí	edice	k1gFnPc2
vzácných	vzácný	k2eAgInPc2d1
tisků	tisk	k1gInPc2
pod	pod	k7c7
značkou	značka	k1gFnSc7
Heritage	Heritag	k1gInSc2
Colections	Colections	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Koncem	koncem	k7c2
roku	rok	k1gInSc2
2016	#num#	k4
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
předsedou	předseda	k1gMnSc7
České	český	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
přátel	přítel	k1gMnPc2
Izraele	Izrael	k1gInSc2
<g/>
,	,	kIx,
členské	členský	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
European	European	k1gMnSc1
Alliance	Alliance	k1gFnSc2
for	forum	k1gNnPc2
Israel	Israel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Passer	Passer	k1gMnSc1
byl	být	k5eAaImAgMnS
v	v	k7c6
letech	léto	k1gNnPc6
2006	#num#	k4
-	-	kIx~
2011	#num#	k4
úspěšným	úspěšný	k2eAgMnSc7d1
motoristickým	motoristický	k2eAgMnSc7d1
závodníkem	závodník	k1gMnSc7
v	v	k7c4
rally	rall	k1gInPc4
<g/>
,	,	kIx,
závodech	závod	k1gInPc6
do	do	k7c2
vrchu	vrch	k1gInSc6
<g/>
,	,	kIx,
sprintech	sprint	k1gInPc6
i	i	k8xC
vytrvalostních	vytrvalostní	k2eAgInPc6d1
závodech	závod	k1gInPc6
na	na	k7c6
okruzích	okruh	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
celkem	celkem	k6eAd1
jedenaosmdesáti	jedenaosmdesát	k4xCc2
startů	start	k1gInPc2
nasbíral	nasbírat	k5eAaPmAgMnS
padesát	padesát	k4xCc4
pódiových	pódiový	k2eAgNnPc2d1
umístění	umístění	k1gNnPc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
mezinárodním	mezinárodní	k2eAgMnSc7d1
mistrem	mistr	k1gMnSc7
ČR	ČR	kA
v	v	k7c6
divizi	divize	k1gFnSc6
3	#num#	k4
Národní	národní	k2eAgFnSc2d1
formule	formule	k1gFnSc2
s	s	k7c7
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
rekordním	rekordní	k2eAgInSc7d1
počtem	počet	k1gInSc7
bodů	bod	k1gInPc2
v	v	k7c6
sezoně	sezona	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Politická	politický	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
Zbyněk	Zbyněk	k1gMnSc1
Passer	Passer	k1gMnSc1
svou	svůj	k3xOyFgFnSc4
komunálně	komunálně	k6eAd1
politickou	politický	k2eAgFnSc4d1
kariéru	kariéra	k1gFnSc4
zahájil	zahájit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1998	#num#	k4
až	až	k9
2005	#num#	k4
působil	působit	k5eAaImAgInS
jako	jako	k9
předseda	předseda	k1gMnSc1
kontrolního	kontrolní	k2eAgInSc2d1
výboru	výbor	k1gInSc2
a	a	k8xC
zastupitel	zastupitel	k1gMnSc1
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
do	do	k7c2
roku	rok	k1gInSc2
2012	#num#	k4
jako	jako	k8xC,k8xS
zástupce	zástupce	k1gMnSc1
starosty	starosta	k1gMnSc2
a	a	k8xC
poté	poté	k6eAd1
do	do	k7c2
července	červenec	k1gInSc2
2015	#num#	k4
jako	jako	k8xS,k8xC
starosta	starosta	k1gMnSc1
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Koloděje	koloděj	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
komunálních	komunální	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
neúspěšně	úspěšně	k6eNd1
kandidoval	kandidovat	k5eAaImAgMnS
za	za	k7c4
politické	politický	k2eAgNnSc4d1
hnutí	hnutí	k1gNnSc4
Pro	pro	k7c4
Prahu	Praha	k1gFnSc4
do	do	k7c2
zastupitelstva	zastupitelstvo	k1gNnSc2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xC,k8xS
lídr	lídr	k1gMnSc1
kandidátky	kandidátka	k1gFnSc2
byl	být	k5eAaImAgMnS
i	i	k9
kandidátem	kandidát	k1gMnSc7
na	na	k7c4
post	post	k1gInSc4
pražského	pražský	k2eAgMnSc2d1
primátora	primátor	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Hnutí	hnutí	k1gNnSc1
ve	v	k7c6
volbách	volba	k1gFnPc6
získalo	získat	k5eAaPmAgNnS
zhruba	zhruba	k6eAd1
2,5	2,5	k4
procenta	procento	k1gNnPc4
hlasů	hlas	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
kraje	kraj	k1gInSc2
roku	rok	k1gInSc2
2015	#num#	k4
Passer	Passer	k1gMnSc1
rezignoval	rezignovat	k5eAaBmAgMnS
na	na	k7c4
funkci	funkce	k1gFnSc4
výkonného	výkonný	k2eAgMnSc2d1
místopředsedy	místopředseda	k1gMnSc2
Pro	pro	k7c4
Prahu	Praha	k1gFnSc4
a	a	k8xC
následujícího	následující	k2eAgInSc2d1
roku	rok	k1gInSc2
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
členství	členství	k1gNnSc2
zaniklo	zaniknout	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Passer	Passer	k1gMnSc1
byl	být	k5eAaImAgMnS
v	v	k7c6
mimořádných	mimořádný	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
znovuzvolen	znovuzvolit	k5eAaPmNgInS
do	do	k7c2
zastupitelstva	zastupitelstvo	k1gNnSc2
městské	městský	k2eAgFnSc2d1
části	část	k1gFnSc2
Koloděje	koloděj	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
mandát	mandát	k1gInSc4
zastupitele	zastupitel	k1gMnSc2
rezignoval	rezignovat	k5eAaBmAgMnS
v	v	k7c6
únoru	únor	k1gInSc6
2016	#num#	k4
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
ukončil	ukončit	k5eAaPmAgMnS
tak	tak	k9
svoji	svůj	k3xOyFgFnSc4
politickou	politický	k2eAgFnSc4d1
kariéru	kariéra	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Rodina	rodina	k1gFnSc1
</s>
<s>
Zbyněk	Zbyněk	k1gMnSc1
Passer	Passer	k1gMnSc1
je	být	k5eAaImIp3nS
bratr	bratr	k1gMnSc1
českého	český	k2eAgMnSc2d1
miliardáře	miliardář	k1gMnSc2
Radima	Radim	k1gMnSc2
Passera	Passer	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Passerovi	Passerův	k2eAgMnPc5d1
jsou	být	k5eAaImIp3nP
etnickým	etnický	k2eAgInSc7d1
a	a	k8xC
náboženským	náboženský	k2eAgInSc7d1
původem	původ	k1gInSc7
židovská	židovský	k2eAgFnSc1d1
rodina	rodina	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
přišla	přijít	k5eAaPmAgFnS
do	do	k7c2
Čech	Čechy	k1gFnPc2
na	na	k7c6
konci	konec	k1gInSc6
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInSc1
první	první	k4xOgInSc1
předek	předek	k1gInSc1
žijící	žijící	k2eAgMnSc1d1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnPc4
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
dohledat	dohledat	k5eAaPmF
v	v	k7c6
archivech	archiv	k1gInPc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
jmenoval	jmenovat	k5eAaBmAgMnS,k5eAaImAgMnS
Abraham	Abraham	k1gMnSc1
Passer	Passer	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následoval	následovat	k5eAaImAgMnS
Jákob	Jákob	k1gMnSc1
a	a	k8xC
po	po	k7c6
něm	on	k3xPp3gNnSc6
Max	Max	k1gMnSc1
Passer	Passer	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
se	se	k3xPyFc4
oženil	oženit	k5eAaPmAgMnS
s	s	k7c7
českou	český	k2eAgFnSc7d1
křesťankou	křesťanka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc1
první	první	k4xOgMnSc1
syn	syn	k1gMnSc1
<g/>
,	,	kIx,
dědeček	dědeček	k1gMnSc1
Zbyňka	Zbyněk	k1gMnSc2
Passera	Passer	k1gMnSc2
a	a	k8xC
Radima	Radim	k1gMnSc2
Passera	Passer	k1gMnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
jmenoval	jmenovat	k5eAaImAgMnS,k5eAaBmAgMnS
Maxmilián	Maxmilián	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
prvorozený	prvorozený	k2eAgMnSc1d1
syn	syn	k1gMnSc1
v	v	k7c6
řadě	řada	k1gFnSc6
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gMnSc1
otec	otec	k1gMnSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
jmenoval	jmenovat	k5eAaBmAgMnS,k5eAaImAgMnS
Maximus	Maximus	k1gMnSc1
Passer	Passer	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
zemřel	zemřít	k5eAaPmAgMnS
roku	rok	k1gInSc2
2003	#num#	k4
<g/>
..	..	k?
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Bratrancem	bratranec	k1gMnSc7
Maximuse	Maximuse	k1gFnSc2
Passera	Passero	k1gNnSc2
je	být	k5eAaImIp3nS
režisér	režisér	k1gMnSc1
Ivan	Ivan	k1gMnSc1
Passer	Passer	k1gMnSc1
a	a	k8xC
Eva	Eva	k1gFnSc1
Límanová	Límanová	k1gFnSc1
<g/>
,	,	kIx,
rozená	rozený	k2eAgFnSc1d1
Passerová	Passerová	k1gFnSc1
<g/>
,	,	kIx,
bývalá	bývalý	k2eAgFnSc1d1
redaktorka	redaktorka	k1gFnSc1
Hlasu	hlas	k1gInSc2
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratrem	bratr	k1gMnSc7
Maximuse	Maximuse	k1gFnSc2
Passera	Passero	k1gNnSc2
je	být	k5eAaImIp3nS
Jan	Jan	k1gMnSc1
Passer	Passer	k1gMnSc1
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
šéftrenér	šéftrenér	k1gMnSc1
české	český	k2eAgFnSc2d1
plavecké	plavecký	k2eAgFnSc2d1
reprezentace	reprezentace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
syn	syn	k1gMnSc1
Jan	Jan	k1gMnSc1
M.	M.	kA
Passer	Passer	k1gMnSc1
<g/>
,	,	kIx,
bratranec	bratranec	k1gMnSc1
Zbyňka	Zbyněk	k1gMnSc2
a	a	k8xC
Radima	Radim	k1gMnSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
soudcem	soudce	k1gMnSc7
Nejvyššího	vysoký	k2eAgInSc2d3
správního	správní	k2eAgInSc2d1
soudu	soud	k1gInSc2
ČR	ČR	kA
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
2016	#num#	k4
je	být	k5eAaImIp3nS
soudcem	soudce	k1gMnSc7
Soudního	soudní	k2eAgNnSc2d1
dvoru	dvůr	k1gInSc3
EU	EU	kA
<g/>
.	.	kIx.
</s>
<s>
Zbyněk	Zbyněk	k1gMnSc1
Passer	Passer	k1gMnSc1
je	být	k5eAaImIp3nS
římskokatolického	římskokatolický	k2eAgNnSc2d1
vyznání	vyznání	k1gNnSc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
jeho	jeho	k3xOp3gMnSc1
dědeček	dědeček	k1gMnSc1
<g/>
,	,	kIx,
matka	matka	k1gFnSc1
a	a	k8xC
otec	otec	k1gMnSc1
<g/>
,	,	kIx,
naopak	naopak	k6eAd1
jeho	jeho	k3xOp3gMnSc1
bratr	bratr	k1gMnSc1
Radim	Radim	k1gMnSc1
je	být	k5eAaImIp3nS
členem	člen	k1gInSc7
Církve	církev	k1gFnSc2
adventistů	adventista	k1gMnPc2
sedmého	sedmý	k4xOgInSc2
dne	den	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
příležitostně	příležitostně	k6eAd1
působí	působit	k5eAaImIp3nS
i	i	k9
jako	jako	k9
kazatel	kazatel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbyněk	Zbyněk	k1gMnSc1
je	být	k5eAaImIp3nS
ženatý	ženatý	k2eAgInSc1d1
od	od	k7c2
roku	rok	k1gInSc2
1997	#num#	k4
<g/>
,	,	kIx,
se	s	k7c7
ženou	žena	k1gFnSc7
Marií	Maria	k1gFnPc2
vychovávají	vychovávat	k5eAaImIp3nP
pět	pět	k4xCc4
dětí	dítě	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
https://www.ereading.cz/nakladatele/data/ebooks/1574_preview.pdf	https://www.ereading.cz/nakladatele/data/ebooks/1574_preview.pdf	k1gInSc1
<g/>
↑	↑	k?
http://euro.e15.cz/archiv/mezi-bohem-a-politikou-1091390	http://euro.e15.cz/archiv/mezi-bohem-a-politikou-1091390	k4
<g/>
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
zastupitelstev	zastupitelstvo	k1gNnPc2
obcí	obec	k1gFnPc2
10	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
-	-	kIx~
11	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
,	,	kIx,
Jmenné	jmenný	k2eAgInPc4d1
seznamy	seznam	k1gInPc4
<g/>
,	,	kIx,
Výběr	výběr	k1gInSc4
<g/>
:	:	kIx,
všichni	všechen	k3xTgMnPc1
platní	platný	k2eAgMnPc1d1
kandidáti	kandidát	k1gMnPc1
dle	dle	k7c2
poř	poř	k?
<g/>
.	.	kIx.
čísla	číslo	k1gNnPc1
<g/>
,	,	kIx,
Zastupitelstvo	zastupitelstvo	k1gNnSc1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
Kraj	kraj	k1gInSc1
<g/>
:	:	kIx,
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Obec	obec	k1gFnSc1
<g/>
:	:	kIx,
Praha	Praha	k1gFnSc1
hl.	hl.	k?
<g/>
m.	m.	k?
<g/>
,	,	kIx,
Kandidátní	kandidátní	k2eAgFnSc1d1
listina	listina	k1gFnSc1
<g/>
:	:	kIx,
Strana	strana	k1gFnSc1
svobodných	svobodný	k2eAgMnPc2d1
občanů	občan	k1gMnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2014	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Komunální	komunální	k2eAgFnSc2d1
volby	volba	k1gFnSc2
Praha-Koloděje	Praha-Koloděje	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zápis	zápis	k1gInSc1
z	z	k7c2
jednání	jednání	k1gNnSc2
ZMČ	ZMČ	kA
Praha-Koloděje	Praha-Koloděje	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MČ	MČ	kA
Praha-Koloděje	Praha-Koloděje	k1gFnPc2
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
PASSER	PASSER	kA
<g/>
,	,	kIx,
Radim	Radim	k1gMnSc1
<g/>
.	.	kIx.
3	#num#	k4
a	a	k8xC
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
Roku	rok	k1gInSc2
-	-	kIx~
Aneb	Aneb	k?
americký	americký	k2eAgInSc4d1
sen	sen	k1gInSc4
v	v	k7c6
česku	česk	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
:	:	kIx,
Maranatha	Maranatha	k1gMnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
250	#num#	k4
s.	s.	k?
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
239	#num#	k4
<g/>
-	-	kIx~
<g/>
5727	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Očekávání	očekávání	k1gNnSc2
<g/>
,	,	kIx,
s.	s.	k?
13	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
https://www.facebook.com/notes/pro-prahu/rozhovor-se-zby%C5%88kem-passerem-pro-mf-dnes/849597305069371/	https://www.facebook.com/notes/pro-prahu/rozhovor-se-zby%C5%88kem-passerem-pro-mf-dnes/849597305069371/	k4
<g/>
↑	↑	k?
http://www.parlamentnilisty.cz/arena/monitor/Umrel-mu-chlapecek-uveril-v-Boha-Miliardar-Passer-hovori-o-Babisovi-a-svem-zasahu-do-politiky-322520	http://www.parlamentnilisty.cz/arena/monitor/Umrel-mu-chlapecek-uveril-v-Boha-Miliardar-Passer-hovori-o-Babisovi-a-svem-zasahu-do-politiky-322520	k4
</s>
