<s>
Olovo	olovo	k1gNnSc4	olovo
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
<g/>
:	:	kIx,	:
Pb	Pb	k1gFnSc1	Pb
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
Plumbum	Plumbum	k1gInSc1	Plumbum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
těžký	těžký	k2eAgInSc1d1	těžký
toxický	toxický	k2eAgInSc1d1	toxický
kov	kov	k1gInSc1	kov
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgInSc1d1	znám
lidstvu	lidstvo	k1gNnSc3	lidstvo
již	již	k9	již
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
nízký	nízký	k2eAgInSc1d1	nízký
bod	bod	k1gInSc1	bod
tání	tání	k1gNnSc2	tání
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
kujný	kujný	k2eAgInSc1d1	kujný
i	i	k9	i
při	při	k7c6	při
pokojové	pokojový	k2eAgFnSc6d1	pokojová
teplotě	teplota	k1gFnSc6	teplota
a	a	k8xC	a
odolný	odolný	k2eAgInSc1d1	odolný
vůči	vůči	k7c3	vůči
korozi	koroze	k1gFnSc3	koroze
<g/>
.	.	kIx.	.
</s>
<s>
Nízkotavitelný	Nízkotavitelný	k2eAgMnSc1d1	Nízkotavitelný
<g/>
,	,	kIx,	,
měkký	měkký	k2eAgMnSc1d1	měkký
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
těžký	těžký	k2eAgInSc1d1	těžký
<g/>
,	,	kIx,	,
toxický	toxický	k2eAgInSc1d1	toxický
kov	kov	k1gInSc1	kov
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc1d1	používaný
člověkem	člověk	k1gMnSc7	člověk
již	již	k6eAd1	již
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
mocenství	mocenství	k1gNnSc6	mocenství
<g/>
:	:	kIx,	:
Pb	Pb	k1gFnSc1	Pb
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
a	a	k8xC	a
Pb	Pb	k1gFnSc1	Pb
<g/>
4	[number]	k4	4
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
teplot	teplota	k1gFnPc2	teplota
pod	pod	k7c4	pod
7,196	[number]	k4	7,196
K	k	k7c3	k
je	být	k5eAaImIp3nS	být
supravodičem	supravodič	k1gInSc7	supravodič
I.	I.	kA	I.
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
podmínek	podmínka	k1gFnPc2	podmínka
je	být	k5eAaImIp3nS	být
olovo	olovo	k1gNnSc1	olovo
odolné	odolný	k2eAgNnSc1d1	odolné
a	a	k8xC	a
neomezeně	omezeně	k6eNd1	omezeně
stálé	stálý	k2eAgFnSc2d1	stálá
vůči	vůči	k7c3	vůči
atmosférickým	atmosférický	k2eAgInPc3d1	atmosférický
vlivům	vliv	k1gInPc3	vliv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kompaktním	kompaktní	k2eAgInSc6d1	kompaktní
stavu	stav	k1gInSc6	stav
se	se	k3xPyFc4	se
na	na	k7c6	na
vlhkém	vlhký	k2eAgInSc6d1	vlhký
vzduchu	vzduch	k1gInSc6	vzduch
příliš	příliš	k6eAd1	příliš
nemění	měnit	k5eNaImIp3nS	měnit
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
zvolna	zvolna	k6eAd1	zvolna
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
lesk	lesk	k1gInSc4	lesk
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
šedobílá	šedobílý	k2eAgFnSc1d1	šedobílá
vrstva	vrstva	k1gFnSc1	vrstva
Oxidů	oxid	k1gInPc2	oxid
<g/>
,	,	kIx,	,
hydroxidů	hydroxid	k1gInPc2	hydroxid
a	a	k8xC	a
uhličitanů	uhličitan	k1gInPc2	uhličitan
<g/>
.	.	kIx.	.
</s>
<s>
Dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
především	především	k9	především
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
dusičné	dusičný	k2eAgNnSc1d1	dusičné
<g/>
,	,	kIx,	,
koncentrovaná	koncentrovaný	k2eAgFnSc1d1	koncentrovaná
kyselina	kyselina	k1gFnSc1	kyselina
sírová	sírový	k2eAgFnSc1d1	sírová
jej	on	k3xPp3gMnSc4	on
naopak	naopak	k6eAd1	naopak
pasivuje	pasivovat	k5eAaBmIp3nS	pasivovat
a	a	k8xC	a
olovo	olovo	k1gNnSc1	olovo
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
nereaguje	reagovat	k5eNaBmIp3nS	reagovat
<g/>
.	.	kIx.	.
</s>
<s>
Olovo	olovo	k1gNnSc1	olovo
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
pohlcuje	pohlcovat	k5eAaImIp3nS	pohlcovat
rentgenové	rentgenový	k2eAgNnSc4d1	rentgenové
záření	záření	k1gNnSc4	záření
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
proto	proto	k8xC	proto
k	k	k7c3	k
odstínění	odstínění	k1gNnSc3	odstínění
zdrojů	zdroj	k1gInPc2	zdroj
tohoto	tento	k3xDgNnSc2	tento
záření	záření	k1gNnSc1	záření
v	v	k7c6	v
chemických	chemický	k2eAgFnPc6d1	chemická
a	a	k8xC	a
fyzikálních	fyzikální	k2eAgFnPc6d1	fyzikální
aparaturách	aparatura	k1gFnPc6	aparatura
a	a	k8xC	a
především	především	k6eAd1	především
v	v	k7c6	v
lékařství	lékařství	k1gNnSc6	lékařství
při	při	k7c6	při
ochraně	ochrana	k1gFnSc6	ochrana
obsluhy	obsluha	k1gFnSc2	obsluha
běžných	běžný	k2eAgInPc2d1	běžný
medicinálních	medicinální	k2eAgInPc2d1	medicinální
rentgenů	rentgen	k1gInPc2	rentgen
<g/>
.	.	kIx.	.
</s>
<s>
Polotloušťka	Polotloušťka	k1gFnSc1	Polotloušťka
olova	olovo	k1gNnSc2	olovo
je	být	k5eAaImIp3nS	být
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c4	na
energii	energie	k1gFnSc4	energie
a	a	k8xC	a
typu	typa	k1gFnSc4	typa
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
pro	pro	k7c4	pro
stínění	stínění	k1gNnSc4	stínění
beta	beta	k1gNnSc2	beta
záření	záření	k1gNnSc2	záření
je	být	k5eAaImIp3nS	být
olovo	olovo	k1gNnSc1	olovo
naprosto	naprosto	k6eAd1	naprosto
nevhodné	vhodný	k2eNgNnSc1d1	nevhodné
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
silného	silný	k2eAgNnSc2d1	silné
druhotného	druhotný	k2eAgNnSc2d1	druhotné
záření	záření	k1gNnSc2	záření
které	který	k3yRgNnSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
při	při	k7c6	při
interakci	interakce	k1gFnSc6	interakce
beta	beta	k1gNnSc1	beta
záření	záření	k1gNnSc2	záření
s	s	k7c7	s
atomy	atom	k1gInPc7	atom
olova	olovo	k1gNnSc2	olovo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
slitiny	slitina	k1gFnPc1	slitina
s	s	k7c7	s
cínem	cín	k1gInSc7	cín
<g/>
,	,	kIx,	,
antimonem	antimon	k1gInSc7	antimon
nebo	nebo	k8xC	nebo
stříbrem	stříbro	k1gNnSc7	stříbro
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
výborné	výborný	k2eAgFnPc4d1	výborná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
při	při	k7c6	při
tavném	tavný	k2eAgNnSc6d1	tavné
spojování	spojování	k1gNnSc6	spojování
kovových	kovový	k2eAgInPc2d1	kovový
předmětů	předmět	k1gInPc2	předmět
pájením	pájení	k1gNnSc7	pájení
a	a	k8xC	a
jako	jako	k9	jako
pájky	pájka	k1gFnPc1	pájka
jsou	být	k5eAaImIp3nP	být
doposud	doposud	k6eAd1	doposud
široce	široko	k6eAd1	široko
používány	používat	k5eAaImNgFnP	používat
<g/>
.	.	kIx.	.
</s>
<s>
Olovo	olovo	k1gNnSc1	olovo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
zastoupeno	zastoupit	k5eAaPmNgNnS	zastoupit
poměrně	poměrně	k6eAd1	poměrně
řídce	řídce	k6eAd1	řídce
<g/>
,	,	kIx,	,
průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
činí	činit	k5eAaImIp3nS	činit
pouze	pouze	k6eAd1	pouze
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
ppm	ppm	k?	ppm
(	(	kIx(	(
<g/>
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
však	však	k9	však
jeho	jeho	k3xOp3gInSc1	jeho
obsah	obsah	k1gInSc1	obsah
větší	veliký	k2eAgInSc1d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
by	by	kYmCp3nS	by
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
očekávat	očekávat	k5eAaImF	očekávat
podle	podle	k7c2	podle
jeho	on	k3xPp3gNnSc2	on
umístění	umístění	k1gNnSc2	umístění
v	v	k7c6	v
periodické	periodický	k2eAgFnSc6d1	periodická
tabulce	tabulka	k1gFnSc6	tabulka
prvků	prvek	k1gInPc2	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
fakt	fakt	k1gInSc4	fakt
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
izotopy	izotop	k1gInPc1	izotop
olova	olovo	k1gNnSc2	olovo
jsou	být	k5eAaImIp3nP	být
konečným	konečný	k2eAgInSc7d1	konečný
produktem	produkt	k1gInSc7	produkt
radioaktivních	radioaktivní	k2eAgFnPc2d1	radioaktivní
rozpadových	rozpadový	k2eAgFnPc2d1	rozpadová
řad	řada	k1gFnPc2	řada
uranu	uran	k1gInSc2	uran
a	a	k8xC	a
thoria	thorium	k1gNnSc2	thorium
a	a	k8xC	a
obsah	obsah	k1gInSc1	obsah
olova	olovo	k1gNnSc2	olovo
se	se	k3xPyFc4	se
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
postupně	postupně	k6eAd1	postupně
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
činí	činit	k5eAaImIp3nS	činit
jeho	jeho	k3xOp3gFnSc1	jeho
koncentrace	koncentrace	k1gFnSc1	koncentrace
pouze	pouze	k6eAd1	pouze
0,03	[number]	k4	0,03
mikrogramu	mikrogram	k1gInSc2	mikrogram
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
litru	litr	k1gInSc6	litr
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
olova	olovo	k1gNnSc2	olovo
přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Elementární	elementární	k2eAgNnSc1d1	elementární
olovo	olovo	k1gNnSc1	olovo
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
vzácně	vzácně	k6eAd1	vzácně
<g/>
.	.	kIx.	.
</s>
<s>
Nejběžnějším	běžný	k2eAgInSc7d3	nejběžnější
minerálem	minerál	k1gInSc7	minerál
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
olověnou	olověný	k2eAgFnSc7d1	olověná
rudou	ruda	k1gFnSc7	ruda
je	být	k5eAaImIp3nS	být
sulfid	sulfid	k1gInSc1	sulfid
olovnatý	olovnatý	k2eAgInSc1d1	olovnatý
<g/>
,	,	kIx,	,
galenit	galenit	k1gInSc1	galenit
PbS	PbS	k1gFnSc2	PbS
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
méně	málo	k6eAd2	málo
běžnými	běžný	k2eAgFnPc7d1	běžná
minerály	minerál	k1gInPc1	minerál
olova	olovo	k1gNnSc2	olovo
jsou	být	k5eAaImIp3nP	být
cerusit	cerusit	k1gInSc4	cerusit
<g/>
,	,	kIx,	,
uhličitan	uhličitan	k1gInSc4	uhličitan
olovnatý	olovnatý	k2eAgInSc4d1	olovnatý
PbCO	PbCO	k1gFnSc7	PbCO
<g/>
3	[number]	k4	3
a	a	k8xC	a
anglesit	anglesit	k1gInSc4	anglesit
<g/>
,	,	kIx,	,
síran	síran	k1gInSc4	síran
olovnatý	olovnatý	k2eAgInSc4d1	olovnatý
PbSO	PbSO	k1gFnSc7	PbSO
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
olovo	olovo	k1gNnSc1	olovo
často	často	k6eAd1	často
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
doprovodný	doprovodný	k2eAgInSc1d1	doprovodný
prvek	prvek	k1gInSc1	prvek
v	v	k7c6	v
rudách	ruda	k1gFnPc6	ruda
zinku	zinek	k1gInSc2	zinek
a	a	k8xC	a
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
získávání	získávání	k1gNnSc6	získávání
olova	olovo	k1gNnSc2	olovo
z	z	k7c2	z
rudy	ruda	k1gFnSc2	ruda
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
hornina	hornina	k1gFnSc1	hornina
jemně	jemně	k6eAd1	jemně
namleta	namlet	k2eAgFnSc1d1	namleta
a	a	k8xC	a
flotací	flotace	k1gFnSc7	flotace
oddělena	oddělen	k2eAgFnSc1d1	oddělena
složka	složka	k1gFnSc1	složka
s	s	k7c7	s
vysokým	vysoký	k2eAgNnSc7d1	vysoké
zastoupením	zastoupení	k1gNnSc7	zastoupení
kovu	kov	k1gInSc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
pražení	pražení	k1gNnSc1	pražení
rudy	ruda	k1gFnSc2	ruda
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
převede	převést	k5eAaPmIp3nS	převést
přítomné	přítomný	k2eAgInPc1d1	přítomný
sulfidy	sulfid	k1gInPc1	sulfid
olova	olovo	k1gNnSc2	olovo
na	na	k7c4	na
oxidy	oxid	k1gInPc4	oxid
<g/>
.	.	kIx.	.
2	[number]	k4	2
PbS	PbS	k1gFnPc2	PbS
+	+	kIx~	+
3	[number]	k4	3
O2	O2	k1gFnSc2	O2
→	→	k?	→
2	[number]	k4	2
PbO	PbO	k1gFnPc2	PbO
+	+	kIx~	+
2	[number]	k4	2
SO2	SO2	k1gFnPc2	SO2
Olovo	olovo	k1gNnSc4	olovo
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
z	z	k7c2	z
praženého	pražený	k2eAgInSc2d1	pražený
koncentrátu	koncentrát	k1gInSc2	koncentrát
rud	ruda	k1gFnPc2	ruda
získává	získávat	k5eAaImIp3nS	získávat
běžnou	běžný	k2eAgFnSc7d1	běžná
žárovou	žárový	k2eAgFnSc7d1	Žárová
redukcí	redukce	k1gFnSc7	redukce
elementárním	elementární	k2eAgInSc7d1	elementární
uhlíkem	uhlík	k1gInSc7	uhlík
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
koks	koks	k1gInSc1	koks
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
PbO	PbO	k?	PbO
+	+	kIx~	+
C	C	kA	C
→	→	k?	→
Pb	Pb	k1gMnSc7	Pb
+	+	kIx~	+
CO	co	k8xS	co
Olovo	olovo	k1gNnSc4	olovo
začali	začít	k5eAaPmAgMnP	začít
lidé	člověk	k1gMnPc1	člověk
používat	používat	k5eAaImF	používat
již	již	k6eAd1	již
v	v	k7c6	v
dávnověku	dávnověk	k1gInSc6	dávnověk
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jeho	jeho	k3xOp3gFnPc1	jeho
rudy	ruda	k1gFnPc1	ruda
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
dobře	dobře	k6eAd1	dobře
dostupné	dostupný	k2eAgFnPc1d1	dostupná
<g/>
.	.	kIx.	.
</s>
<s>
Kdy	kdy	k6eAd1	kdy
a	a	k8xC	a
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
olovo	olovo	k1gNnSc1	olovo
získáno	získán	k2eAgNnSc1d1	získáno
poprvé	poprvé	k6eAd1	poprvé
není	být	k5eNaImIp3nS	být
dosud	dosud	k6eAd1	dosud
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgInSc1d3	nejstarší
dochovaný	dochovaný	k2eAgInSc1d1	dochovaný
předmět	předmět	k1gInSc1	předmět
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
období	období	k1gNnSc2	období
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
3000	[number]	k4	3000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
2000	[number]	k4	2000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
nalezen	nalézt	k5eAaBmNgInS	nalézt
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
co	co	k3yRnSc4	co
největší	veliký	k2eAgNnSc4d3	veliký
omezení	omezení	k1gNnSc4	omezení
využívání	využívání	k1gNnSc2	využívání
olova	olovo	k1gNnSc2	olovo
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc2	jeho
slitin	slitina	k1gFnPc2	slitina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
předmětů	předmět	k1gInPc2	předmět
praktického	praktický	k2eAgNnSc2d1	praktické
použití	použití	k1gNnSc2	použití
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
prokázané	prokázaný	k2eAgFnSc3d1	prokázaná
toxicitě	toxicita	k1gFnSc3	toxicita
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
ještě	ještě	k9	ještě
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
bylo	být	k5eAaImAgNnS	být
olovo	olovo	k1gNnSc1	olovo
velmi	velmi	k6eAd1	velmi
běžně	běžně	k6eAd1	běžně
užívaným	užívaný	k2eAgInSc7d1	užívaný
kovem	kov	k1gInSc7	kov
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
zpracovatelů	zpracovatel	k1gMnPc2	zpracovatel
olova	olovo	k1gNnSc2	olovo
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
současné	současný	k2eAgFnSc2d1	současná
doby	doba	k1gFnSc2	doba
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
vyrábějící	vyrábějící	k2eAgInPc1d1	vyrábějící
elektrické	elektrický	k2eAgInPc1d1	elektrický
akumulátory	akumulátor	k1gInPc1	akumulátor
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
svoji	svůj	k3xOyFgFnSc4	svůj
vysokou	vysoký	k2eAgFnSc4d1	vysoká
hmotnost	hmotnost	k1gFnSc4	hmotnost
a	a	k8xC	a
obsah	obsah	k1gInSc4	obsah
vysoce	vysoce	k6eAd1	vysoce
žíravé	žíravý	k2eAgFnSc2d1	žíravá
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
jsou	být	k5eAaImIp3nP	být
technické	technický	k2eAgInPc4d1	technický
parametry	parametr	k1gInPc4	parametr
olověných	olověný	k2eAgInPc2d1	olověný
akumulátorů	akumulátor	k1gInPc2	akumulátor
natolik	natolik	k6eAd1	natolik
vhodné	vhodný	k2eAgNnSc1d1	vhodné
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
vybavení	vybavení	k1gNnSc6	vybavení
automobilů	automobil	k1gInPc2	automobil
mají	mít	k5eAaImIp3nP	mít
stále	stále	k6eAd1	stále
většinové	většinový	k2eAgNnSc4d1	většinové
zastoupení	zastoupení	k1gNnSc4	zastoupení
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
hlavní	hlavní	k2eAgFnSc7d1	hlavní
výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
odolnost	odolnost	k1gFnSc4	odolnost
vůči	vůči	k7c3	vůči
otřesům	otřes	k1gInPc3	otřes
a	a	k8xC	a
vysokému	vysoký	k2eAgNnSc3d1	vysoké
proudovému	proudový	k2eAgNnSc3d1	proudové
zatížení	zatížení	k1gNnSc3	zatížení
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
vhodné	vhodný	k2eAgInPc1d1	vhodný
pro	pro	k7c4	pro
vozidla	vozidlo	k1gNnPc4	vozidlo
jako	jako	k8xC	jako
startovací	startovací	k2eAgInPc4d1	startovací
i	i	k8xC	i
trakční	trakční	k2eAgInPc4d1	trakční
zdroje	zdroj	k1gInPc4	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tyto	tento	k3xDgInPc4	tento
účely	účel	k1gInPc4	účel
je	být	k5eAaImIp3nS	být
využívána	využívat	k5eAaImNgFnS	využívat
přibližně	přibližně	k6eAd1	přibližně
polovina	polovina	k1gFnSc1	polovina
světové	světový	k2eAgFnSc2d1	světová
produkce	produkce	k1gFnSc2	produkce
olova	olovo	k1gNnSc2	olovo
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
recyklace	recyklace	k1gFnSc1	recyklace
je	být	k5eAaImIp3nS	být
také	také	k9	také
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgInPc2d3	nejvýznamnější
zdrojů	zdroj	k1gInPc2	zdroj
tohoto	tento	k3xDgInSc2	tento
kovu	kov	k1gInSc2	kov
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
bylo	být	k5eAaImAgNnS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
vyrobit	vyrobit	k5eAaPmF	vyrobit
skleněné	skleněný	k2eAgFnPc4d1	skleněná
tabule	tabule	k1gFnPc4	tabule
o	o	k7c6	o
větších	veliký	k2eAgInPc6d2	veliký
rozměrech	rozměr	k1gInPc6	rozměr
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
okna	okno	k1gNnPc1	okno
zhotovovala	zhotovovat	k5eAaImAgNnP	zhotovovat
z	z	k7c2	z
malých	malý	k2eAgFnPc2d1	malá
skleněných	skleněný	k2eAgFnPc2d1	skleněná
tabulek	tabulka	k1gFnPc2	tabulka
seskládaných	seskládaný	k2eAgInPc2d1	seskládaný
do	do	k7c2	do
olověných	olověný	k2eAgNnPc2d1	olověné
H	H	kA	H
profilů	profil	k1gInPc2	profil
letovaných	letovaný	k2eAgFnPc2d1	letovaná
cínem	cín	k1gInSc7	cín
(	(	kIx(	(
<g/>
takzvané	takzvaný	k2eAgFnPc4d1	takzvaná
vitráže	vitráž	k1gFnPc4	vitráž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Dodnes	dodnes	k6eAd1	dodnes
tato	tento	k3xDgNnPc1	tento
okna	okno	k1gNnPc1	okno
můžeme	moct	k5eAaImIp1nP	moct
vidět	vidět	k5eAaImF	vidět
ve	v	k7c6	v
starých	starý	k2eAgFnPc6d1	stará
katedrálách	katedrála	k1gFnPc6	katedrála
a	a	k8xC	a
středověkých	středověký	k2eAgInPc6d1	středověký
hradech	hrad	k1gInPc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
odolnosti	odolnost	k1gFnSc2	odolnost
olova	olovo	k1gNnSc2	olovo
vůči	vůči	k7c3	vůči
korozi	koroze	k1gFnSc3	koroze
vodou	voda	k1gFnSc7	voda
bylo	být	k5eAaImAgNnS	být
využíváno	využívat	k5eAaImNgNnS	využívat
ke	k	k7c3	k
konstrukci	konstrukce	k1gFnSc3	konstrukce
části	část	k1gFnSc2	část
vodovodních	vodovodní	k2eAgInPc2d1	vodovodní
rozvodů	rozvod	k1gInPc2	rozvod
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
objektech	objekt	k1gInPc6	objekt
<g/>
)	)	kIx)	)
z	z	k7c2	z
olověných	olověný	k2eAgFnPc2d1	olověná
trubek	trubka	k1gFnPc2	trubka
s	s	k7c7	s
cínovou	cínový	k2eAgFnSc7d1	cínová
vložkou	vložka	k1gFnSc7	vložka
silnou	silný	k2eAgFnSc7d1	silná
cca	cca	kA	cca
0.5	[number]	k4	0.5
mm	mm	kA	mm
(	(	kIx(	(
<g/>
poznají	poznat	k5eAaPmIp3nP	poznat
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
podélných	podélný	k2eAgInPc2d1	podélný
výstupků	výstupek	k1gInPc2	výstupek
<g/>
)	)	kIx)	)
a	a	k8xC	a
odpadních	odpadní	k2eAgInPc2d1	odpadní
rozvodů	rozvod	k1gInPc2	rozvod
v	v	k7c6	v
domácnostech	domácnost	k1gFnPc6	domácnost
a	a	k8xC	a
chemických	chemický	k2eAgFnPc6d1	chemická
laboratořích	laboratoř	k1gFnPc6	laboratoř
(	(	kIx(	(
<g/>
dodnes	dodnes	k6eAd1	dodnes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
řada	řada	k1gFnSc1	řada
těchto	tento	k3xDgFnPc2	tento
instalací	instalace	k1gFnPc2	instalace
plně	plně	k6eAd1	plně
funkčních	funkční	k2eAgFnPc2d1	funkční
<g/>
.	.	kIx.	.
</s>
<s>
Konstrukce	konstrukce	k1gFnSc1	konstrukce
velkoobjemových	velkoobjemový	k2eAgFnPc2d1	velkoobjemová
nádob	nádoba	k1gFnPc2	nádoba
na	na	k7c4	na
uchovávání	uchovávání	k1gNnSc4	uchovávání
koncentrované	koncentrovaný	k2eAgFnSc2d1	koncentrovaná
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
využívá	využívat	k5eAaImIp3nS	využívat
faktu	fakt	k1gInSc2	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
olovo	olovo	k1gNnSc1	olovo
je	být	k5eAaImIp3nS	být
vůči	vůči	k7c3	vůči
působení	působení	k1gNnSc3	působení
této	tento	k3xDgFnSc2	tento
mimořádně	mimořádně	k6eAd1	mimořádně
silné	silný	k2eAgFnSc2d1	silná
kyseliny	kyselina	k1gFnSc2	kyselina
vysoce	vysoce	k6eAd1	vysoce
rezistentní	rezistentní	k2eAgMnSc1d1	rezistentní
<g/>
.	.	kIx.	.
</s>
<s>
Olovo	olovo	k1gNnSc1	olovo
přitom	přitom	k6eAd1	přitom
slouží	sloužit	k5eAaImIp3nS	sloužit
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
pokrytí	pokrytí	k1gNnSc4	pokrytí
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
stěn	stěna	k1gFnPc2	stěna
ocelových	ocelový	k2eAgFnPc2d1	ocelová
nádrží	nádrž	k1gFnPc2	nádrž
<g/>
,	,	kIx,	,
samotné	samotný	k2eAgNnSc1d1	samotné
olovo	olovo	k1gNnSc1	olovo
by	by	kYmCp3nS	by
nemělo	mít	k5eNaImAgNnS	mít
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
mechanickou	mechanický	k2eAgFnSc4d1	mechanická
pevnost	pevnost	k1gFnSc4	pevnost
a	a	k8xC	a
odolnost	odolnost	k1gFnSc4	odolnost
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stavebnictví	stavebnictví	k1gNnSc6	stavebnictví
se	se	k3xPyFc4	se
využívalo	využívat	k5eAaImAgNnS	využívat
olovo	olovo	k1gNnSc1	olovo
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
jako	jako	k8xC	jako
střešní	střešní	k2eAgFnSc1d1	střešní
krytina	krytina	k1gFnSc1	krytina
nebo	nebo	k8xC	nebo
jako	jako	k9	jako
materiál	materiál	k1gInSc4	materiál
pro	pro	k7c4	pro
zalévání	zalévání	k1gNnSc4	zalévání
spojovacích	spojovací	k2eAgFnPc2d1	spojovací
spon	spona	k1gFnPc2	spona
<g/>
,	,	kIx,	,
čepů	čep	k1gInPc2	čep
a	a	k8xC	a
jiných	jiný	k2eAgInPc2d1	jiný
železných	železný	k2eAgInPc2d1	železný
prvků	prvek	k1gInPc2	prvek
u	u	k7c2	u
kamenných	kamenný	k2eAgFnPc2d1	kamenná
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
parních	parní	k2eAgInPc2d1	parní
kotlů	kotel	k1gInPc2	kotel
se	se	k3xPyFc4	se
používalo	používat	k5eAaImAgNnS	používat
olovo	olovo	k1gNnSc1	olovo
jako	jako	k8xC	jako
těsnění	těsnění	k1gNnSc1	těsnění
vymývacích	vymývací	k2eAgNnPc2d1	vymývací
víček	víčko	k1gNnPc2	víčko
a	a	k8xC	a
náplň	náplň	k1gFnSc1	náplň
olovníků	olovník	k1gInPc2	olovník
<g/>
.	.	kIx.	.
</s>
<s>
Olovo	olovo	k1gNnSc1	olovo
velmi	velmi	k6eAd1	velmi
účinně	účinně	k6eAd1	účinně
pohlcuje	pohlcovat	k5eAaImIp3nS	pohlcovat
rentgenové	rentgenový	k2eAgNnSc1d1	rentgenové
záření	záření	k1gNnSc1	záření
a	a	k8xC	a
gama	gama	k1gNnSc1	gama
paprsky	paprsek	k1gInPc1	paprsek
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
proto	proto	k8xC	proto
jako	jako	k8xC	jako
ochrana	ochrana	k1gFnSc1	ochrana
na	na	k7c6	na
pracovištích	pracoviště	k1gNnPc6	pracoviště
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
vysoce	vysoce	k6eAd1	vysoce
energetickým	energetický	k2eAgNnSc7d1	energetické
elektromagnetickým	elektromagnetický	k2eAgNnSc7d1	elektromagnetické
zářením	záření	k1gNnSc7	záření
pracuje	pracovat	k5eAaImIp3nS	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Přidáním	přidání	k1gNnSc7	přidání
olova	olovo	k1gNnSc2	olovo
do	do	k7c2	do
skla	sklo	k1gNnSc2	sklo
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
značně	značně	k6eAd1	značně
jeho	jeho	k3xOp3gInSc1	jeho
index	index	k1gInSc1	index
lomu	lom	k1gInSc2	lom
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
snižuje	snižovat	k5eAaImIp3nS	snižovat
jeho	jeho	k3xOp3gFnSc1	jeho
tvrdost	tvrdost	k1gFnSc1	tvrdost
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
snáze	snadno	k6eAd2	snadno
brousí	brousit	k5eAaImIp3nP	brousit
a	a	k8xC	a
leští	leštit	k5eAaImIp3nP	leštit
<g/>
.	.	kIx.	.
</s>
<s>
Olovnaté	olovnatý	k2eAgNnSc1d1	olovnaté
sklo	sklo	k1gNnSc1	sklo
je	být	k5eAaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
výhradní	výhradní	k2eAgFnSc7d1	výhradní
surovinou	surovina	k1gFnSc7	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
broušených	broušený	k2eAgInPc2d1	broušený
ověsů	ověs	k1gInPc2	ověs
skleněných	skleněný	k2eAgInPc2d1	skleněný
lustrů	lustr	k1gInPc2	lustr
i	i	k8xC	i
řady	řada	k1gFnSc2	řada
dekorativních	dekorativní	k2eAgInPc2d1	dekorativní
skleněných	skleněný	k2eAgInPc2d1	skleněný
předmětů	předmět	k1gInPc2	předmět
(	(	kIx(	(
<g/>
vázy	váza	k1gFnPc1	váza
<g/>
,	,	kIx,	,
popelníky	popelník	k1gInPc1	popelník
<g/>
,	,	kIx,	,
těžítka	těžítko	k1gNnPc1	těžítko
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Olovo	olovo	k1gNnSc1	olovo
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
převažujícím	převažující	k2eAgInSc7d1	převažující
materiálem	materiál	k1gInSc7	materiál
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
střeliva	střelivo	k1gNnSc2	střelivo
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
vysokou	vysoký	k2eAgFnSc4d1	vysoká
specifickou	specifický	k2eAgFnSc4d1	specifická
hmotnost	hmotnost	k1gFnSc4	hmotnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
olověné	olověný	k2eAgFnSc3d1	olověná
střele	střela	k1gFnSc3	střela
vysokou	vysoký	k2eAgFnSc4d1	vysoká
průraznost	průraznost	k1gFnSc4	průraznost
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
nábojů	náboj	k1gInPc2	náboj
do	do	k7c2	do
lehkých	lehký	k2eAgFnPc2d1	lehká
palných	palný	k2eAgFnPc2d1	palná
zbraní	zbraň	k1gFnPc2	zbraň
(	(	kIx(	(
<g/>
pistole	pistol	k1gFnPc1	pistol
<g/>
,	,	kIx,	,
revolvery	revolver	k1gInPc1	revolver
<g/>
,	,	kIx,	,
pušky	puška	k1gFnPc1	puška
<g/>
,	,	kIx,	,
samopaly	samopal	k1gInPc1	samopal
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
olověného	olověný	k2eAgNnSc2d1	olověné
jádra	jádro	k1gNnSc2	jádro
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
kryto	krýt	k5eAaImNgNnS	krýt
ocelovým	ocelový	k2eAgInSc7d1	ocelový
nebo	nebo	k8xC	nebo
měděným	měděný	k2eAgInSc7d1	měděný
pláštěm	plášť	k1gInSc7	plášť
<g/>
.	.	kIx.	.
</s>
<s>
Střelivo	střelivo	k1gNnSc1	střelivo
pro	pro	k7c4	pro
brokové	brokový	k2eAgFnPc4d1	broková
zbraně	zbraň	k1gFnPc4	zbraň
tvoří	tvořit	k5eAaImIp3nP	tvořit
obvykle	obvykle	k6eAd1	obvykle
broky	brok	k1gInPc1	brok
z	z	k7c2	z
čistého	čistý	k2eAgNnSc2d1	čisté
olova	olovo	k1gNnSc2	olovo
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
slitiny	slitina	k1gFnPc4	slitina
olova	olovo	k1gNnSc2	olovo
s	s	k7c7	s
antimonem	antimon	k1gInSc7	antimon
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
slitin	slitina	k1gFnPc2	slitina
olova	olovo	k1gNnSc2	olovo
jsou	být	k5eAaImIp3nP	být
rozhodně	rozhodně	k6eAd1	rozhodně
nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
pájky	pájka	k1gFnPc1	pájka
<g/>
.	.	kIx.	.
</s>
<s>
Nejobvyklejší	obvyklý	k2eAgFnPc1d3	nejobvyklejší
pájky	pájka	k1gFnPc1	pájka
jsou	být	k5eAaImIp3nP	být
slitiny	slitina	k1gFnPc4	slitina
olova	olovo	k1gNnSc2	olovo
s	s	k7c7	s
cínem	cín	k1gInSc7	cín
<g/>
,	,	kIx,	,
používané	používaný	k2eAgInPc4d1	používaný
pro	pro	k7c4	pro
pájení	pájení	k1gNnSc4	pájení
jednoduchých	jednoduchý	k2eAgInPc2d1	jednoduchý
elektrických	elektrický	k2eAgInPc2d1	elektrický
obvodů	obvod	k1gInPc2	obvod
nebo	nebo	k8xC	nebo
instalatérské	instalatérský	k2eAgFnSc2d1	instalatérská
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Bod	bod	k1gInSc1	bod
tání	tání	k1gNnSc2	tání
těchto	tento	k3xDgFnPc2	tento
pájek	pájka	k1gFnPc2	pájka
je	být	k5eAaImIp3nS	být
dán	dát	k5eAaPmNgInS	dát
poměrem	poměr	k1gInSc7	poměr
obou	dva	k4xCgInPc2	dva
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	se	k3xPyFc4	se
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
250	[number]	k4	250
<g/>
-	-	kIx~	-
<g/>
400	[number]	k4	400
°	°	k?	°
<g/>
C.	C.	kA	C.
Pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
bodu	bod	k1gInSc2	bod
tání	tání	k1gNnSc2	tání
a	a	k8xC	a
pevnosti	pevnost	k1gFnSc2	pevnost
sváru	svár	k1gInSc2	svár
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
slitiny	slitina	k1gFnPc1	slitina
cínu	cín	k1gInSc2	cín
<g/>
,	,	kIx,	,
olova	olovo	k1gNnSc2	olovo
<g/>
,	,	kIx,	,
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
kadmia	kadmium	k1gNnSc2	kadmium
a	a	k8xC	a
antimonu	antimon	k1gInSc2	antimon
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
<g/>
,	,	kIx,	,
vyžadující	vyžadující	k2eAgFnSc4d1	vyžadující
zvlášť	zvlášť	k6eAd1	zvlášť
velkou	velký	k2eAgFnSc4d1	velká
tvrdost	tvrdost	k1gFnSc4	tvrdost
spoje	spoj	k1gInSc2	spoj
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
přidává	přidávat	k5eAaImIp3nS	přidávat
i	i	k9	i
fosfor	fosfor	k1gInSc4	fosfor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
však	však	k9	však
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
křehkost	křehkost	k1gFnSc4	křehkost
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
ekologického	ekologický	k2eAgNnSc2d1	ekologické
hlediska	hledisko	k1gNnSc2	hledisko
zvyšován	zvyšován	k2eAgInSc4d1	zvyšován
tlak	tlak	k1gInSc4	tlak
na	na	k7c4	na
odstranění	odstranění	k1gNnSc4	odstranění
toxických	toxický	k2eAgInPc2d1	toxický
těžkých	těžký	k2eAgInPc2d1	těžký
kovů	kov	k1gInPc2	kov
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
olovo	olovo	k1gNnSc1	olovo
a	a	k8xC	a
kadmium	kadmium	k1gNnSc1	kadmium
z	z	k7c2	z
elektronických	elektronický	k2eAgInPc2d1	elektronický
produktů	produkt	k1gInPc2	produkt
každodenního	každodenní	k2eAgNnSc2d1	každodenní
použití	použití	k1gNnSc2	použití
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
roste	růst	k5eAaImIp3nS	růst
poptávka	poptávka	k1gFnSc1	poptávka
po	po	k7c6	po
pájkách	pájka	k1gFnPc6	pájka
složených	složený	k2eAgInPc2d1	složený
pouze	pouze	k6eAd1	pouze
ze	z	k7c2	z
stříbra	stříbro	k1gNnSc2	stříbro
a	a	k8xC	a
cínu	cín	k1gInSc2	cín
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
jejich	jejich	k3xOp3gFnSc4	jejich
vyšší	vysoký	k2eAgFnSc4d2	vyšší
cenu	cena	k1gFnSc4	cena
a	a	k8xC	a
nižší	nízký	k2eAgFnSc4d2	nižší
kvalitu	kvalita	k1gFnSc4	kvalita
a	a	k8xC	a
životnost	životnost	k1gFnSc4	životnost
spoje	spoj	k1gFnSc2	spoj
-	-	kIx~	-
bezolovnaté	bezolovnatý	k2eAgFnPc1d1	bezolovnatá
pájky	pájka	k1gFnPc1	pájka
jsou	být	k5eAaImIp3nP	být
křehčí	křehký	k2eAgFnPc1d2	křehčí
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
výrazně	výrazně	k6eAd1	výrazně
náchylnější	náchylný	k2eAgFnPc1d2	náchylnější
ke	k	k7c3	k
vznikům	vznik	k1gInPc3	vznik
tzv.	tzv.	kA	tzv.
studených	studený	k2eAgInPc2d1	studený
spojů	spoj	k1gInPc2	spoj
(	(	kIx(	(
<g/>
otřesy	otřes	k1gInPc1	otřes
<g/>
,	,	kIx,	,
tepelná	tepelný	k2eAgFnSc1d1	tepelná
dilatace	dilatace	k1gFnSc1	dilatace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
jsou	být	k5eAaImIp3nP	být
dříve	dříve	k6eAd2	dříve
či	či	k8xC	či
později	pozdě	k6eAd2	pozdě
degradovány	degradován	k2eAgInPc1d1	degradován
tzv.	tzv.	kA	tzv.
cínovým	cínový	k2eAgInSc7d1	cínový
morem	mor	k1gInSc7	mor
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
vznik	vznik	k1gInSc1	vznik
olovo	olovo	k1gNnSc1	olovo
významně	významně	k6eAd1	významně
potlačuje	potlačovat	k5eAaImIp3nS	potlačovat
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
olověné	olověný	k2eAgFnPc1d1	olověná
pájky	pájka	k1gFnPc1	pájka
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
používají	používat	k5eAaImIp3nP	používat
při	při	k7c6	při
opravách	oprava	k1gFnPc6	oprava
elektroniky	elektronika	k1gFnSc2	elektronika
<g/>
.	.	kIx.	.
</s>
<s>
Ložisková	ložiskový	k2eAgFnSc1d1	ložisková
kompozice	kompozice	k1gFnSc1	kompozice
je	být	k5eAaImIp3nS	být
slitina	slitina	k1gFnSc1	slitina
s	s	k7c7	s
přibližným	přibližný	k2eAgNnSc7d1	přibližné
složením	složení	k1gNnSc7	složení
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
90	[number]	k4	90
%	%	kIx~	%
Sn	Sn	k1gFnSc1	Sn
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
navíc	navíc	k6eAd1	navíc
měď	měď	k1gFnSc1	měď
<g/>
,	,	kIx,	,
olovo	olovo	k1gNnSc1	olovo
a	a	k8xC	a
antimon	antimon	k1gInSc1	antimon
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
především	především	k9	především
vysokou	vysoký	k2eAgFnSc7d1	vysoká
odolností	odolnost	k1gFnSc7	odolnost
proti	proti	k7c3	proti
otěru	otěr	k1gInSc3	otěr
i	i	k9	i
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
měkké	měkký	k2eAgFnPc1d1	měkká
-	-	kIx~	-
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
kluzných	kluzný	k2eAgNnPc2d1	kluzné
ložisek	ložisko	k1gNnPc2	ložisko
pro	pro	k7c4	pro
automobilový	automobilový	k2eAgInSc4d1	automobilový
průmysl	průmysl	k1gInSc4	průmysl
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
aplikace	aplikace	k1gFnPc4	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
nedávnou	dávný	k2eNgFnSc7d1	nedávná
dobou	doba	k1gFnSc7	doba
byla	být	k5eAaImAgFnS	být
hojně	hojně	k6eAd1	hojně
užívanou	užívaný	k2eAgFnSc7d1	užívaná
slitinou	slitina	k1gFnSc7	slitina
liteřina	liteřina	k1gFnSc1	liteřina
<g/>
,	,	kIx,	,
směs	směs	k1gFnSc1	směs
olova	olovo	k1gNnSc2	olovo
<g/>
,	,	kIx,	,
cínu	cín	k1gInSc2	cín
a	a	k8xC	a
antimonu	antimon	k1gInSc2	antimon
<g/>
.	.	kIx.	.
</s>
<s>
Odlévala	odlévat	k5eAaImAgFnS	odlévat
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
písmena	písmeno	k1gNnPc4	písmeno
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
tiskárnách	tiskárna	k1gFnPc6	tiskárna
skládala	skládat	k5eAaImAgFnS	skládat
do	do	k7c2	do
stránek	stránka	k1gFnPc2	stránka
a	a	k8xC	a
sloužila	sloužit	k5eAaImAgFnS	sloužit
k	k	k7c3	k
tisku	tisk	k1gInSc3	tisk
knih	kniha	k1gFnPc2	kniha
<g/>
,	,	kIx,	,
novin	novina	k1gFnPc2	novina
a	a	k8xC	a
časopisů	časopis	k1gInPc2	časopis
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vytištění	vytištění	k1gNnSc6	vytištění
potřebného	potřebný	k2eAgInSc2d1	potřebný
textu	text	k1gInSc2	text
se	se	k3xPyFc4	se
stránka	stránka	k1gFnSc1	stránka
rozmetala	rozmetat	k5eAaImAgFnS	rozmetat
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
odlity	odlit	k2eAgFnPc1d1	odlita
nové	nový	k2eAgFnPc1d1	nová
litery	litera	k1gFnPc1	litera
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
typ	typ	k1gInSc1	typ
tisku	tisk	k1gInSc2	tisk
překonán	překonat	k5eAaPmNgInS	překonat
a	a	k8xC	a
opuštěn	opustit	k5eAaPmNgInS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Olovo	olovo	k1gNnSc1	olovo
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
sloučeniny	sloučenina	k1gFnPc4	sloučenina
s	s	k7c7	s
mocenstvím	mocenství	k1gNnSc7	mocenství
Pb	Pb	k1gFnSc1	Pb
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
a	a	k8xC	a
Pb	Pb	k1gFnSc1	Pb
<g/>
4	[number]	k4	4
<g/>
+	+	kIx~	+
<g/>
.	.	kIx.	.
</s>
<s>
Nejstálejší	stálý	k2eAgInPc1d3	nejstálejší
jsou	být	k5eAaImIp3nP	být
přitom	přitom	k6eAd1	přitom
sloučeniny	sloučenina	k1gFnPc4	sloučenina
dvojmocného	dvojmocný	k2eAgNnSc2d1	dvojmocné
olova	olovo	k1gNnSc2	olovo
<g/>
,	,	kIx,	,
čtyřmocné	čtyřmocný	k2eAgNnSc1d1	čtyřmocný
olovo	olovo	k1gNnSc1	olovo
je	být	k5eAaImIp3nS	být
vesměs	vesměs	k6eAd1	vesměs
oxidačním	oxidační	k2eAgNnSc7d1	oxidační
činidlem	činidlo	k1gNnSc7	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
řady	řada	k1gFnSc2	řada
sloučenin	sloučenina	k1gFnPc2	sloučenina
mají	mít	k5eAaImIp3nP	mít
největší	veliký	k2eAgInSc4d3	veliký
praktický	praktický	k2eAgInSc4d1	praktický
význam	význam	k1gInSc4	význam
<g/>
:	:	kIx,	:
Oxid	oxid	k1gInSc4	oxid
olovnatý	olovnatý	k2eAgInSc4d1	olovnatý
<g/>
,	,	kIx,	,
PbO	PbO	k1gFnSc1	PbO
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
barevných	barevný	k2eAgFnPc6d1	barevná
formách	forma	k1gFnPc6	forma
-	-	kIx~	-
červená	červenat	k5eAaImIp3nS	červenat
tetragonální	tetragonální	k2eAgMnSc1d1	tetragonální
(	(	kIx(	(
<g/>
starší	starý	k2eAgInSc1d2	starší
název	název	k1gInSc1	název
olovnatý	olovnatý	k2eAgInSc4d1	olovnatý
klejt	klejt	k1gInSc4	klejt
<g/>
)	)	kIx)	)
a	a	k8xC	a
forma	forma	k1gFnSc1	forma
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
olovnatý	olovnatý	k2eAgInSc1d1	olovnatý
lze	lze	k6eAd1	lze
nejsnáze	snadno	k6eAd3	snadno
připravit	připravit	k5eAaPmF	připravit
přímou	přímý	k2eAgFnSc7d1	přímá
oxidací	oxidace	k1gFnSc7	oxidace
roztaveného	roztavený	k2eAgNnSc2d1	roztavené
olova	olovo	k1gNnSc2	olovo
vzdušným	vzdušný	k2eAgInSc7d1	vzdušný
kyslíkem	kyslík	k1gInSc7	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
uplatnění	uplatnění	k1gNnSc1	uplatnění
nalézá	nalézat	k5eAaImIp3nS	nalézat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
těžkého	těžký	k2eAgNnSc2d1	těžké
olovnatého	olovnatý	k2eAgNnSc2d1	olovnaté
skla	sklo	k1gNnSc2	sklo
<g/>
,	,	kIx,	,
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
indexem	index	k1gInSc7	index
lomu	lom	k1gInSc2	lom
a	a	k8xC	a
leskem	lesk	k1gInSc7	lesk
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc4d1	další
uplatnění	uplatnění	k1gNnSc4	uplatnění
nalézá	nalézat	k5eAaImIp3nS	nalézat
tato	tento	k3xDgFnSc1	tento
látka	látka	k1gFnSc1	látka
jako	jako	k8xS	jako
složka	složka	k1gFnSc1	složka
keramických	keramický	k2eAgFnPc2d1	keramická
glazur	glazura	k1gFnPc2	glazura
a	a	k8xC	a
emailů	email	k1gInPc2	email
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
olovnato	olovnato	k6eAd1	olovnato
-	-	kIx~	-
olovičitý	olovičitý	k2eAgInSc1d1	olovičitý
<g/>
,	,	kIx,	,
suřík	suřík	k1gInSc1	suřík
<g/>
,	,	kIx,	,
Pb	Pb	k1gFnSc1	Pb
<g/>
3	[number]	k4	3
<g/>
O	o	k7c4	o
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
složený	složený	k2eAgInSc1d1	složený
oxid	oxid	k1gInSc1	oxid
2	[number]	k4	2
<g/>
PbO	PbO	k1gFnPc2	PbO
+	+	kIx~	+
PbO	PbO	k1gFnPc2	PbO
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
nalézá	nalézat	k5eAaImIp3nS	nalézat
využití	využití	k1gNnSc1	využití
jako	jako	k8xS	jako
nerozpustný	rozpustný	k2eNgInSc1d1	nerozpustný
červený	červený	k2eAgInSc1d1	červený
pigment	pigment	k1gInSc1	pigment
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
antikorozních	antikorozní	k2eAgInPc2d1	antikorozní
nátěrů	nátěr	k1gInPc2	nátěr
železných	železný	k2eAgFnPc2d1	železná
a	a	k8xC	a
ocelových	ocelový	k2eAgFnPc2d1	ocelová
konstrukcí	konstrukce	k1gFnPc2	konstrukce
a	a	k8xC	a
jako	jako	k9	jako
součást	součást	k1gFnSc4	součást
keramických	keramický	k2eAgFnPc2d1	keramická
glazur	glazura	k1gFnPc2	glazura
<g/>
.	.	kIx.	.
</s>
<s>
Uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
se	se	k3xPyFc4	se
i	i	k9	i
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
syntetického	syntetický	k2eAgInSc2d1	syntetický
kaučuku	kaučuk	k1gInSc2	kaučuk
jako	jako	k8xC	jako
aktivátor	aktivátor	k1gInSc4	aktivátor
vulkanizace	vulkanizace	k1gFnSc2	vulkanizace
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
olovičitý	olovičitý	k2eAgInSc1d1	olovičitý
<g/>
,	,	kIx,	,
PbO	PbO	k1gFnSc1	PbO
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
hnědá	hnědý	k2eAgFnSc1d1	hnědá
látka	látka	k1gFnSc1	látka
s	s	k7c7	s
oxidačními	oxidační	k2eAgFnPc7d1	oxidační
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
využití	využití	k1gNnSc1	využití
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
zápalek	zápalka	k1gFnPc2	zápalka
je	být	k5eAaImIp3nS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
faktu	fakt	k1gInSc6	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnPc4	jeho
směsi	směs	k1gFnPc4	směs
se	s	k7c7	s
snadno	snadno	k6eAd1	snadno
zápalnými	zápalný	k2eAgFnPc7d1	zápalná
látkami	látka	k1gFnPc7	látka
(	(	kIx(	(
<g/>
fosfor	fosfor	k1gInSc1	fosfor
<g/>
,	,	kIx,	,
síra	síra	k1gFnSc1	síra
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
samovolně	samovolně	k6eAd1	samovolně
vzněcují	vzněcovat	k5eAaImIp3nP	vzněcovat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlastnost	vlastnost	k1gFnSc1	vlastnost
se	se	k3xPyFc4	se
uplatní	uplatnit	k5eAaPmIp3nS	uplatnit
i	i	k9	i
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
pyrotechnických	pyrotechnický	k2eAgInPc2d1	pyrotechnický
materiálů	materiál	k1gInPc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Sulfid	sulfid	k1gInSc1	sulfid
olovnatý	olovnatý	k2eAgInSc1d1	olovnatý
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
sloučenina	sloučenina	k1gFnSc1	sloučenina
kovového	kovový	k2eAgInSc2d1	kovový
lesku	lesk	k1gInSc2	lesk
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
štěpná	štěpný	k2eAgFnSc1d1	štěpná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
setkáváme	setkávat	k5eAaImIp1nP	setkávat
jako	jako	k9	jako
s	s	k7c7	s
minerálem	minerál	k1gInSc7	minerál
a	a	k8xC	a
olověnou	olověný	k2eAgFnSc7d1	olověná
rudou	ruda	k1gFnSc7	ruda
galenitem	galenit	k1gInSc7	galenit
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
čistý	čistý	k2eAgMnSc1d1	čistý
PbS	PbS	k1gMnSc1	PbS
je	být	k5eAaImIp3nS	být
citlivým	citlivý	k2eAgInSc7d1	citlivý
detektorem	detektor	k1gInSc7	detektor
infračerveného	infračervený	k2eAgNnSc2d1	infračervené
záření	záření	k1gNnSc2	záření
a	a	k8xC	a
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
fotoelektrickou	fotoelektrický	k2eAgFnSc4d1	fotoelektrická
vodivost	vodivost	k1gFnSc4	vodivost
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
se	se	k3xPyFc4	se
chová	chovat	k5eAaImIp3nS	chovat
i	i	k9	i
selenid	selenid	k1gInSc1	selenid
olovnatý	olovnatý	k2eAgInSc1d1	olovnatý
a	a	k8xC	a
telurid	telurid	k1gInSc1	telurid
olovnatý	olovnatý	k2eAgInSc4d1	olovnatý
PbSe	PbSe	k1gInSc4	PbSe
a	a	k8xC	a
PbTe	PbTe	k1gFnSc4	PbTe
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlastnost	vlastnost	k1gFnSc1	vlastnost
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
např.	např.	kA	např.
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
fotografických	fotografický	k2eAgInPc2d1	fotografický
expozimetrů	expozimetr	k1gInPc2	expozimetr
a	a	k8xC	a
fotočlánků	fotočlánek	k1gInPc2	fotočlánek
<g/>
.	.	kIx.	.
</s>
<s>
Uhličitan	uhličitan	k1gInSc4	uhličitan
olovnatý	olovnatý	k2eAgInSc4d1	olovnatý
PbCO	PbCO	k1gFnSc7	PbCO
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
zahřátím	zahřátí	k1gNnSc7	zahřátí
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
barviva	barvivo	k1gNnSc2	barvivo
-	-	kIx~	-
olovnaté	olovnatý	k2eAgFnSc2d1	olovnatá
běloby	běloba	k1gFnSc2	běloba
-	-	kIx~	-
Pb	Pb	k1gFnSc1	Pb
<g/>
3	[number]	k4	3
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
CO	co	k6eAd1	co
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
malířský	malířský	k2eAgInSc1d1	malířský
pigment	pigment	k1gInSc1	pigment
má	mít	k5eAaImIp3nS	mít
výbornou	výborný	k2eAgFnSc4d1	výborná
krycí	krycí	k2eAgFnSc4d1	krycí
schopnost	schopnost	k1gFnSc4	schopnost
(	(	kIx(	(
<g/>
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
fázi	fáze	k1gFnSc6	fáze
techniky	technika	k1gFnSc2	technika
-	-	kIx~	-
podmalbě	podmalba	k1gFnSc6	podmalba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jím	jíst	k5eAaImIp1nS	jíst
malíř	malíř	k1gMnSc1	malíř
vykreslí	vykreslit	k5eAaPmIp3nS	vykreslit
světlá	světlý	k2eAgNnPc4d1	světlé
místa	místo	k1gNnPc4	místo
<g/>
)	)	kIx)	)
a	a	k8xC	a
smíšen	smíšen	k2eAgInSc1d1	smíšen
s	s	k7c7	s
olejovitými	olejovitý	k2eAgFnPc7d1	olejovitá
látkami	látka	k1gFnPc7	látka
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k8xS	jako
malířská	malířský	k2eAgFnSc1d1	malířská
barva	barva	k1gFnSc1	barva
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
tohoto	tento	k3xDgInSc2	tento
pigmentu	pigment	k1gInSc2	pigment
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
jeho	jeho	k3xOp3gFnSc2	jeho
toxicity	toxicita	k1gFnSc2	toxicita
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
které	který	k3yQgFnSc3	který
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nP	muset
dbát	dbát	k5eAaImF	dbát
zvýšené	zvýšený	k2eAgFnSc3d1	zvýšená
opatrnosti	opatrnost	k1gFnSc3	opatrnost
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
přítomnosti	přítomnost	k1gFnSc6	přítomnost
sirovodíku	sirovodík	k1gInSc2	sirovodík
tmavne	tmavnout	k5eAaImIp3nS	tmavnout
vznikem	vznik	k1gInSc7	vznik
sulfidu	sulfid	k1gInSc2	sulfid
olovnatého	olovnatý	k2eAgInSc2d1	olovnatý
PbS	PbS	k1gFnPc7	PbS
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejkvalitnější	kvalitní	k2eAgInSc4d3	nejkvalitnější
druh	druh	k1gInSc4	druh
olovnaté	olovnatý	k2eAgFnSc2d1	olovnatá
běloby	běloba	k1gFnSc2	běloba
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
běloba	běloba	k1gFnSc1	běloba
kremžská	kremžský	k2eAgFnSc1d1	kremžská
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
velmi	velmi	k6eAd1	velmi
významným	významný	k2eAgInPc3d1	významný
malířským	malířský	k2eAgInPc3d1	malířský
pigmentům	pigment	k1gInPc3	pigment
patří	patřit	k5eAaImIp3nS	patřit
žlutý	žlutý	k2eAgInSc4d1	žlutý
chroman	chroman	k1gInSc4	chroman
olovnatý	olovnatý	k2eAgInSc4d1	olovnatý
PbCrO	PbCrO	k1gFnSc7	PbCrO
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xC	jako
chromová	chromový	k2eAgFnSc1d1	chromová
žluť	žluť	k1gFnSc1	žluť
<g/>
.	.	kIx.	.
</s>
<s>
Síran	síran	k1gInSc1	síran
olovnatý	olovnatý	k2eAgInSc1d1	olovnatý
<g/>
,	,	kIx,	,
PbSO	PbSO	k1gFnSc1	PbSO
<g/>
4	[number]	k4	4
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
obtížně	obtížně	k6eAd1	obtížně
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
sloučenina	sloučenina	k1gFnSc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Krystaly	krystal	k1gInPc1	krystal
čistého	čistý	k2eAgInSc2d1	čistý
síranu	síran	k1gInSc2	síran
olovnatého	olovnatý	k2eAgInSc2d1	olovnatý
jsou	být	k5eAaImIp3nP	být
čiré	čirý	k2eAgNnSc4d1	čiré
jako	jako	k8xC	jako
sklo	sklo	k1gNnSc4	sklo
(	(	kIx(	(
<g/>
označují	označovat	k5eAaImIp3nP	označovat
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
jako	jako	k8xC	jako
olovnaté	olovnatý	k2eAgNnSc1d1	olovnaté
sklo	sklo	k1gNnSc1	sklo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chemických	chemický	k2eAgFnPc6d1	chemická
výrobách	výroba	k1gFnPc6	výroba
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
používá	používat	k5eAaImIp3nS	používat
přídavek	přídavek	k1gInSc1	přídavek
síranových	síranový	k2eAgInPc2d1	síranový
iontů	ion	k1gInPc2	ion
k	k	k7c3	k
roztoku	roztok	k1gInSc3	roztok
pro	pro	k7c4	pro
odstraňování	odstraňování	k1gNnSc4	odstraňování
toxických	toxický	k2eAgInPc2d1	toxický
iontů	ion	k1gInPc2	ion
olova	olovo	k1gNnSc2	olovo
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
také	také	k9	také
používal	používat	k5eAaImAgInS	používat
jako	jako	k9	jako
pigment	pigment	k1gInSc1	pigment
do	do	k7c2	do
barev	barva	k1gFnPc2	barva
a	a	k8xC	a
zejména	zejména	k9	zejména
do	do	k7c2	do
líčidel	líčidlo	k1gNnPc2	líčidlo
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
olověná	olověný	k2eAgFnSc1d1	olověná
běloba	běloba	k1gFnSc1	běloba
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dusičnan	dusičnan	k1gInSc1	dusičnan
olovnatý	olovnatý	k2eAgInSc1d1	olovnatý
<g/>
,	,	kIx,	,
Pb	Pb	k1gFnPc4	Pb
<g/>
(	(	kIx(	(
<g/>
NO	no	k9	no
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejlépe	dobře	k6eAd3	dobře
rozpustných	rozpustný	k2eAgFnPc2d1	rozpustná
sloučenin	sloučenina	k1gFnPc2	sloučenina
olova	olovo	k1gNnSc2	olovo
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
velmi	velmi	k6eAd1	velmi
snadno	snadno	k6eAd1	snadno
přímo	přímo	k6eAd1	přímo
reakcí	reakce	k1gFnSc7	reakce
elementárního	elementární	k2eAgNnSc2d1	elementární
olova	olovo	k1gNnSc2	olovo
s	s	k7c7	s
kyselinou	kyselina	k1gFnSc7	kyselina
dusičnou	dusičný	k2eAgFnSc7d1	dusičná
za	za	k7c2	za
intenzivního	intenzivní	k2eAgInSc2d1	intenzivní
vývoje	vývoj	k1gInSc2	vývoj
oxidů	oxid	k1gInPc2	oxid
dusíku	dusík	k1gInSc2	dusík
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
často	často	k6eAd1	často
jako	jako	k9	jako
výchozí	výchozí	k2eAgFnSc1d1	výchozí
látka	látka	k1gFnSc1	látka
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
jiných	jiný	k2eAgFnPc2d1	jiná
sloučenin	sloučenina	k1gFnPc2	sloučenina
olova	olovo	k1gNnSc2	olovo
<g/>
.	.	kIx.	.
</s>
<s>
Tetraethylolovo	tetraethylolovo	k1gNnSc1	tetraethylolovo
<g/>
,	,	kIx,	,
Pb	Pb	k1gFnSc1	Pb
<g/>
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
H	H	kA	H
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
organokovová	organokovový	k2eAgFnSc1d1	organokovová
sloučenina	sloučenina	k1gFnSc1	sloučenina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
přidávala	přidávat	k5eAaImAgFnS	přidávat
do	do	k7c2	do
benzínu	benzín	k1gInSc2	benzín
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zpomalovala	zpomalovat	k5eAaImAgFnS	zpomalovat
rychlost	rychlost	k1gFnSc1	rychlost
jeho	on	k3xPp3gNnSc2	on
hoření	hoření	k1gNnSc2	hoření
a	a	k8xC	a
zvyšovala	zvyšovat	k5eAaImAgFnS	zvyšovat
oktanové	oktanový	k2eAgNnSc4d1	oktanové
číslo	číslo	k1gNnSc4	číslo
paliva	palivo	k1gNnSc2	palivo
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
usazené	usazený	k2eAgNnSc1d1	usazené
olovo	olovo	k1gNnSc1	olovo
sloužilo	sloužit	k5eAaImAgNnS	sloužit
jako	jako	k9	jako
mazadlo	mazadlo	k1gNnSc1	mazadlo
sedel	sedlo	k1gNnPc2	sedlo
ventilů	ventil	k1gInPc2	ventil
spalovacích	spalovací	k2eAgInPc2d1	spalovací
motorů	motor	k1gInPc2	motor
a	a	k8xC	a
utěsňovalo	utěsňovat	k5eAaImAgNnS	utěsňovat
spalovací	spalovací	k2eAgInSc4d1	spalovací
prostor	prostor	k1gInSc4	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
typy	typ	k1gInPc4	typ
spalovacích	spalovací	k2eAgInPc2d1	spalovací
motorů	motor	k1gInPc2	motor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
musí	muset	k5eAaImIp3nP	muset
využívat	využívat	k5eAaPmF	využívat
tento	tento	k3xDgInSc4	tento
typ	typ	k1gInSc4	typ
paliva	palivo	k1gNnSc2	palivo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
tato	tento	k3xDgFnSc1	tento
příměs	příměs	k1gFnSc1	příměs
nahrazována	nahrazován	k2eAgFnSc1d1	nahrazována
organokovovými	organokovový	k2eAgFnPc7d1	organokovová
sloučeninami	sloučenina	k1gFnPc7	sloučenina
manganu	mangan	k1gInSc2	mangan
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vyspělých	vyspělý	k2eAgFnPc6d1	vyspělá
zemích	zem	k1gFnPc6	zem
byly	být	k5eAaImAgFnP	být
zavedeny	zaveden	k2eAgInPc4d1	zaveden
trojcestné	trojcestný	k2eAgInPc4d1	trojcestný
katalyzátory	katalyzátor	k1gInPc4	katalyzátor
výfukových	výfukový	k2eAgInPc2d1	výfukový
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
zavedení	zavedení	k1gNnSc3	zavedení
bezolovnatých	bezolovnatý	k2eAgInPc2d1	bezolovnatý
benzínů	benzín	k1gInPc2	benzín
<g/>
.	.	kIx.	.
</s>
<s>
Olovo	olovo	k1gNnSc1	olovo
<g/>
,	,	kIx,	,
vyskytující	vyskytující	k2eAgMnSc1d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
stabilních	stabilní	k2eAgInPc2d1	stabilní
izotopů	izotop	k1gInPc2	izotop
<g/>
:	:	kIx,	:
204	[number]	k4	204
<g/>
Pb	Pb	k1gFnPc2	Pb
<g/>
,	,	kIx,	,
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
přibližně	přibližně	k6eAd1	přibližně
1,4	[number]	k4	1,4
%	%	kIx~	%
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
zástupcem	zástupce	k1gMnSc7	zástupce
olova	olovo	k1gNnSc2	olovo
vzniklého	vzniklý	k2eAgNnSc2d1	vzniklé
mimo	mimo	k7c4	mimo
radioaktivní	radioaktivní	k2eAgFnPc4d1	radioaktivní
rozpadové	rozpadový	k2eAgFnPc4d1	rozpadová
řady	řada	k1gFnPc4	řada
<g/>
.	.	kIx.	.
206	[number]	k4	206
<g/>
Pb	Pb	k1gFnPc2	Pb
<g/>
,	,	kIx,	,
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
přibližně	přibližně	k6eAd1	přibližně
24,1	[number]	k4	24,1
%	%	kIx~	%
které	který	k3yQgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
<g />
.	.	kIx.	.
</s>
<s>
jako	jako	k8xC	jako
finální	finální	k2eAgInSc1d1	finální
produkt	produkt	k1gInSc1	produkt
rozpadu	rozpad	k1gInSc2	rozpad
uranu	uran	k1gInSc2	uran
238U	[number]	k4	238U
(	(	kIx(	(
<g/>
Uran-radiová	Uranadiový	k2eAgFnSc1d1	Uran-radiový
rozpadová	rozpadový	k2eAgFnSc1d1	rozpadová
řada	řada	k1gFnSc1	řada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
207	[number]	k4	207
<g/>
Pb	Pb	k1gFnPc2	Pb
<g/>
,	,	kIx,	,
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
přibližně	přibližně	k6eAd1	přibližně
22,1	[number]	k4	22,1
%	%	kIx~	%
které	který	k3yQgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
jako	jako	k8xC	jako
finální	finální	k2eAgInSc1d1	finální
produkt	produkt	k1gInSc1	produkt
rozpadu	rozpad	k1gInSc2	rozpad
uranu	uran	k1gInSc2	uran
235U	[number]	k4	235U
(	(	kIx(	(
<g/>
Aktiniová	aktiniový	k2eAgFnSc1d1	aktiniová
rozpadová	rozpadový	k2eAgFnSc1d1	rozpadová
řada	řada	k1gFnSc1	řada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
208	[number]	k4	208
<g/>
Pb	Pb	k1gFnPc2	Pb
<g/>
,	,	kIx,	,
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
přibližně	přibližně	k6eAd1	přibližně
52,4	[number]	k4	52,4
%	%	kIx~	%
které	který	k3yIgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
jako	jako	k8xC	jako
finální	finální	k2eAgInSc1d1	finální
produkt	produkt	k1gInSc1	produkt
rozpadu	rozpad	k1gInSc2	rozpad
thoria	thorium	k1gNnSc2	thorium
232	[number]	k4	232
<g/>
Th	Th	k1gFnPc2	Th
(	(	kIx(	(
<g/>
Thoriová	thoriový	k2eAgFnSc1d1	thoriová
rozpadová	rozpadový	k2eAgFnSc1d1	rozpadová
řada	řada	k1gFnSc1	řada
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Olovo	olovo	k1gNnSc1	olovo
<g/>
,	,	kIx,	,
vyskytující	vyskytující	k2eAgMnSc1d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
rudách	ruda	k1gFnPc6	ruda
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
tedy	tedy	k9	tedy
odlišný	odlišný	k2eAgInSc1d1	odlišný
vzájemný	vzájemný	k2eAgInSc1d1	vzájemný
poměr	poměr	k1gInSc1	poměr
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
izotopů	izotop	k1gInPc2	izotop
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
původu	původ	k1gInSc6	původ
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
skutečnosti	skutečnost	k1gFnSc3	skutečnost
lze	lze	k6eAd1	lze
v	v	k7c6	v
jistých	jistý	k2eAgInPc6d1	jistý
případech	případ	k1gInPc6	případ
využít	využít	k5eAaPmF	využít
k	k	k7c3	k
vysledování	vysledování	k1gNnSc3	vysledování
původu	původ	k1gInSc2	původ
olova	olovo	k1gNnSc2	olovo
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
archeologické	archeologický	k2eAgInPc1d1	archeologický
vzorky	vzorek	k1gInPc7	vzorek
<g/>
)	)	kIx)	)
metodou	metoda	k1gFnSc7	metoda
hmotnostní	hmotnostní	k2eAgFnSc2d1	hmotnostní
spektrometrie	spektrometrie	k1gFnSc2	spektrometrie
<g/>
.	.	kIx.	.
</s>
<s>
Uvedená	uvedený	k2eAgFnSc1d1	uvedená
technika	technika	k1gFnSc1	technika
určí	určit	k5eAaPmIp3nS	určit
velmi	velmi	k6eAd1	velmi
přesně	přesně	k6eAd1	přesně
vzájemné	vzájemný	k2eAgNnSc4d1	vzájemné
zastoupení	zastoupení	k1gNnSc4	zastoupení
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
izotopů	izotop	k1gInPc2	izotop
olova	olovo	k1gNnSc2	olovo
a	a	k8xC	a
porovnáním	porovnání	k1gNnSc7	porovnání
s	s	k7c7	s
tabelovanými	tabelovaný	k2eAgFnPc7d1	tabelovaná
hodnotami	hodnota	k1gFnPc7	hodnota
pro	pro	k7c4	pro
známé	známý	k1gMnPc4	známý
starověké	starověký	k2eAgFnSc2d1	starověká
lokality	lokalita	k1gFnSc2	lokalita
těžby	těžba	k1gFnSc2	těžba
olověných	olověný	k2eAgFnPc2d1	olověná
rud	ruda	k1gFnPc2	ruda
lze	lze	k6eAd1	lze
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
mírou	míra	k1gFnSc7	míra
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
určit	určit	k5eAaPmF	určit
původ	původ	k1gInSc4	původ
vyšetřovaného	vyšetřovaný	k2eAgInSc2d1	vyšetřovaný
olověného	olověný	k2eAgInSc2d1	olověný
předmětu	předmět	k1gInSc2	předmět
nebo	nebo	k8xC	nebo
i	i	k9	i
předmětu	předmět	k1gInSc2	předmět
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
olovnatý	olovnatý	k2eAgInSc1d1	olovnatý
pigment	pigment	k1gInSc1	pigment
<g/>
.	.	kIx.	.
</s>
<s>
Podobné	podobný	k2eAgFnPc1d1	podobná
studie	studie	k1gFnPc1	studie
se	se	k3xPyFc4	se
nemusí	muset	k5eNaImIp3nP	muset
omezovat	omezovat	k5eAaImF	omezovat
vždy	vždy	k6eAd1	vždy
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
archeologické	archeologický	k2eAgInPc4d1	archeologický
vzorky	vzorek	k1gInPc4	vzorek
-	-	kIx~	-
existují	existovat	k5eAaImIp3nP	existovat
výzkumy	výzkum	k1gInPc1	výzkum
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
snažily	snažit	k5eAaImAgFnP	snažit
vytipovat	vytipovat	k5eAaPmF	vytipovat
základní	základní	k2eAgInSc4d1	základní
zdroj	zdroj	k1gInSc4	zdroj
emisního	emisní	k2eAgNnSc2d1	emisní
olova	olovo	k1gNnSc2	olovo
ze	z	k7c2	z
spalovacích	spalovací	k2eAgInPc2d1	spalovací
motorů	motor	k1gInPc2	motor
pro	pro	k7c4	pro
určitou	určitý	k2eAgFnSc4d1	určitá
lokalitu	lokalita	k1gFnSc4	lokalita
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
prací	práce	k1gFnPc2	práce
pochází	pocházet	k5eAaImIp3nS	pocházet
např.	např.	kA	např.
většina	většina	k1gFnSc1	většina
emisního	emisní	k2eAgNnSc2d1	emisní
olova	olovo	k1gNnSc2	olovo
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Vídně	Vídeň	k1gFnSc2	Vídeň
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Izotop	izotop	k1gInSc1	izotop
210	[number]	k4	210
<g/>
Pb	Pb	k1gFnPc2	Pb
(	(	kIx(	(
<g/>
s	s	k7c7	s
poločasem	poločas	k1gInSc7	poločas
rozpadu	rozpad	k1gInSc2	rozpad
přibližně	přibližně	k6eAd1	přibližně
22	[number]	k4	22
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
neustále	neustále	k6eAd1	neustále
vzniká	vznikat	k5eAaImIp3nS	vznikat
rozpadem	rozpad	k1gInSc7	rozpad
222	[number]	k4	222
<g/>
Rn	Rn	k1gMnPc2	Rn
uvolňovaného	uvolňovaný	k2eAgInSc2d1	uvolňovaný
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
olovo	olovo	k1gNnSc1	olovo
běžně	běžně	k6eAd1	běžně
dostupné	dostupný	k2eAgNnSc1d1	dostupné
není	být	k5eNaImIp3nS	být
prosté	prostý	k2eAgNnSc1d1	prosté
tohoto	tento	k3xDgInSc2	tento
izotopu	izotop	k1gInSc2	izotop
<g/>
.	.	kIx.	.
</s>
<s>
Olovo	olovo	k1gNnSc1	olovo
patří	patřit	k5eAaImIp3nS	patřit
zcela	zcela	k6eAd1	zcela
jasně	jasně	k6eAd1	jasně
mezi	mezi	k7c4	mezi
toxické	toxický	k2eAgInPc4d1	toxický
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
historického	historický	k2eAgNnSc2d1	historické
hlediska	hledisko	k1gNnSc2	hledisko
je	být	k5eAaImIp3nS	být
právě	právě	k6eAd1	právě
nadměrné	nadměrný	k2eAgNnSc4d1	nadměrné
užívání	užívání	k1gNnSc4	užívání
olova	olovo	k1gNnSc2	olovo
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
faktorů	faktor	k1gInPc2	faktor
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
přispěl	přispět	k5eAaPmAgInS	přispět
k	k	k7c3	k
zániku	zánik	k1gInSc3	zánik
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
v	v	k7c6	v
období	období	k1gNnSc6	období
kolem	kolem	k7c2	kolem
změny	změna	k1gFnSc2	změna
letopočtu	letopočet	k1gInSc2	letopočet
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
mělo	mít	k5eAaImAgNnS	mít
používání	používání	k1gNnSc1	používání
octanu	octan	k1gInSc2	octan
olovnatého	olovnatý	k2eAgInSc2d1	olovnatý
jako	jako	k8xS	jako
sladidla	sladidlo	k1gNnSc2	sladidlo
<g/>
.	.	kIx.	.
</s>
<s>
Toxicita	toxicita	k1gFnSc1	toxicita
olova	olovo	k1gNnSc2	olovo
je	být	k5eAaImIp3nS	být
zvláště	zvláště	k6eAd1	zvláště
významná	významný	k2eAgFnSc1d1	významná
pro	pro	k7c4	pro
dětský	dětský	k2eAgInSc4d1	dětský
organismus	organismus	k1gInSc4	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Trvalá	trvalý	k2eAgFnSc1d1	trvalá
expozice	expozice	k1gFnSc1	expozice
dětského	dětský	k2eAgInSc2d1	dětský
organismu	organismus	k1gInSc2	organismus
i	i	k8xC	i
nízkými	nízký	k2eAgFnPc7d1	nízká
dávkami	dávka	k1gFnPc7	dávka
olova	olovo	k1gNnSc2	olovo
je	být	k5eAaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
zpomalení	zpomalení	k1gNnSc1	zpomalení
duševního	duševní	k2eAgInSc2d1	duševní
vývoje	vývoj	k1gInSc2	vývoj
a	a	k8xC	a
nepříznivých	příznivý	k2eNgFnPc2d1	nepříznivá
změn	změna	k1gFnPc2	změna
v	v	k7c6	v
chování	chování	k1gNnSc6	chování
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
olovo	olovo	k1gNnSc1	olovo
zakázáno	zakázat	k5eAaPmNgNnS	zakázat
používat	používat	k5eAaImF	používat
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
používání	používání	k1gNnSc3	používání
olova	olovo	k1gNnSc2	olovo
v	v	k7c6	v
rozvodech	rozvod	k1gInPc6	rozvod
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
aditiva	aditivum	k1gNnPc1	aditivum
v	v	k7c6	v
benzínu	benzín	k1gInSc6	benzín
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc3	jeho
ostatnímu	ostatní	k2eAgNnSc3d1	ostatní
využití	využití	k1gNnSc3	využití
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
všudypřítomným	všudypřítomný	k2eAgInSc7d1	všudypřítomný
kontaminantem	kontaminant	k1gInSc7	kontaminant
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Olovo	olovo	k1gNnSc1	olovo
se	se	k3xPyFc4	se
po	po	k7c6	po
vniknutí	vniknutí	k1gNnSc6	vniknutí
do	do	k7c2	do
organismu	organismus	k1gInSc2	organismus
ukládá	ukládat	k5eAaImIp3nS	ukládat
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
kostech	kost	k1gFnPc6	kost
a	a	k8xC	a
v	v	k7c6	v
určitém	určitý	k2eAgNnSc6d1	určité
množství	množství	k1gNnSc6	množství
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
.	.	kIx.	.
</s>
<s>
Typickými	typický	k2eAgInPc7d1	typický
příznaky	příznak	k1gInPc7	příznak
otravy	otrava	k1gFnSc2	otrava
olovem	olovo	k1gNnSc7	olovo
jsou	být	k5eAaImIp3nP	být
bledost	bledost	k1gFnSc1	bledost
obličeje	obličej	k1gInSc2	obličej
a	a	k8xC	a
rtů	ret	k1gInPc2	ret
<g/>
,	,	kIx,	,
zácpa	zácpa	k1gFnSc1	zácpa
a	a	k8xC	a
nechuť	nechuť	k1gFnSc1	nechuť
k	k	k7c3	k
jídlu	jídlo	k1gNnSc3	jídlo
<g/>
,	,	kIx,	,
kolika	kolika	k1gFnSc1	kolika
<g/>
,	,	kIx,	,
anémie	anémie	k1gFnSc1	anémie
<g/>
,	,	kIx,	,
bolesti	bolest	k1gFnPc1	bolest
hlavy	hlava	k1gFnSc2	hlava
<g/>
,	,	kIx,	,
křeče	křeč	k1gFnSc2	křeč
<g/>
,	,	kIx,	,
chronická	chronický	k2eAgFnSc1d1	chronická
nefritida	nefritida	k1gFnSc1	nefritida
<g/>
,	,	kIx,	,
poškození	poškození	k1gNnSc1	poškození
mozku	mozek	k1gInSc2	mozek
a	a	k8xC	a
poruchy	porucha	k1gFnSc2	porucha
centrálního	centrální	k2eAgInSc2d1	centrální
mozkového	mozkový	k2eAgInSc2d1	mozkový
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Léčení	léčení	k1gNnSc1	léčení
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tvorbě	tvorba	k1gFnSc6	tvorba
komplexu	komplex	k1gInSc2	komplex
a	a	k8xC	a
maskování	maskování	k1gNnSc1	maskování
Pb	Pb	k1gFnSc6	Pb
silným	silný	k2eAgNnSc7d1	silné
chelatačním	chelatační	k2eAgNnSc7d1	chelatační
činidlem	činidlo	k1gNnSc7	činidlo
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
stopy	stopa	k1gFnPc4	stopa
olova	olovo	k1gNnSc2	olovo
v	v	k7c6	v
okolním	okolní	k2eAgNnSc6d1	okolní
prostředí	prostředí	k1gNnSc6	prostředí
a	a	k8xC	a
potravě	potrava	k1gFnSc3	potrava
mohou	moct	k5eAaImIp3nP	moct
vést	vést	k5eAaImF	vést
při	při	k7c6	při
trvalém	trvalý	k2eAgInSc6d1	trvalý
přísunu	přísun	k1gInSc6	přísun
do	do	k7c2	do
organismu	organismus	k1gInSc2	organismus
k	k	k7c3	k
následným	následný	k2eAgNnPc3d1	následné
těžkým	těžký	k2eAgNnPc3d1	těžké
onemocněním	onemocnění	k1gNnPc3	onemocnění
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
olovo	olovo	k1gNnSc1	olovo
se	se	k3xPyFc4	se
v	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
kumuluje	kumulovat	k5eAaImIp3nS	kumulovat
a	a	k8xC	a
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
se	se	k3xPyFc4	se
jen	jen	k9	jen
obtížně	obtížně	k6eAd1	obtížně
<g/>
.	.	kIx.	.
</s>
<s>
Těžké	těžký	k2eAgInPc1d1	těžký
kovy	kov	k1gInPc1	kov
jako	jako	k8xS	jako
olovo	olovo	k1gNnSc1	olovo
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgInPc1d1	schopný
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
putovat	putovat	k5eAaImF	putovat
na	na	k7c4	na
velké	velký	k2eAgFnPc4d1	velká
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
<g/>
,	,	kIx,	,
kontaminují	kontaminovat	k5eAaBmIp3nP	kontaminovat
půdu	půda	k1gFnSc4	půda
i	i	k9	i
tisíce	tisíc	k4xCgInPc4	tisíc
kilometrů	kilometr	k1gInPc2	kilometr
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
zdroje	zdroj	k1gInSc2	zdroj
znečištění	znečištěný	k2eAgMnPc1d1	znečištěný
olovem	olovo	k1gNnSc7	olovo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodních	vodní	k2eAgFnPc6d1	vodní
nádržích	nádrž	k1gFnPc6	nádrž
a	a	k8xC	a
řekách	řeka	k1gFnPc6	řeka
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
kumulaci	kumulace	k1gFnSc3	kumulace
olova	olovo	k1gNnSc2	olovo
v	v	k7c6	v
sedimentech	sediment	k1gInPc6	sediment
a	a	k8xC	a
tvorbě	tvorba	k1gFnSc3	tvorba
methylderivátů	methylderivát	k1gInPc2	methylderivát
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
drastickému	drastický	k2eAgNnSc3d1	drastické
omezení	omezení	k1gNnSc3	omezení
obsahu	obsah	k1gInSc2	obsah
olova	olovo	k1gNnSc2	olovo
v	v	k7c6	v
autobenzínech	autobenzín	k1gInPc6	autobenzín
se	se	k3xPyFc4	se
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
významně	významně	k6eAd1	významně
podařilo	podařit	k5eAaPmAgNnS	podařit
zmenšit	zmenšit	k5eAaPmF	zmenšit
rozsah	rozsah	k1gInSc4	rozsah
oblastí	oblast	k1gFnPc2	oblast
kriticky	kriticky	k6eAd1	kriticky
zatížených	zatížený	k2eAgFnPc2d1	zatížená
olovem	olovo	k1gNnSc7	olovo
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
bylo	být	k5eAaImAgNnS	být
ohroženo	ohrozit	k5eAaPmNgNnS	ohrozit
přes	přes	k7c4	přes
70	[number]	k4	70
<g/>
%	%	kIx~	%
sledovaných	sledovaný	k2eAgInPc2d1	sledovaný
ekosystémů	ekosystém	k1gInPc2	ekosystém
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
jen	jen	k9	jen
8	[number]	k4	8
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgInSc1d1	americký
výzkum	výzkum	k1gInSc1	výzkum
konstatoval	konstatovat	k5eAaBmAgInS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tisíce	tisíc	k4xCgInSc2	tisíc
tun	tuna	k1gFnPc2	tuna
olova	olovo	k1gNnSc2	olovo
se	se	k3xPyFc4	se
ročně	ročně	k6eAd1	ročně
do	do	k7c2	do
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
dostává	dostávat	k5eAaImIp3nS	dostávat
vinou	vinou	k7c2	vinou
rybářů	rybář	k1gMnPc2	rybář
a	a	k8xC	a
lovců	lovec	k1gMnPc2	lovec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
používají	používat	k5eAaImIp3nP	používat
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
zálibu	záliba	k1gFnSc4	záliba
olověné	olověný	k2eAgInPc4d1	olověný
broky	brok	k1gInPc4	brok
a	a	k8xC	a
olůvka	olůvko	k1gNnPc4	olůvko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
otravou	otrava	k1gFnSc7	otrava
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	let	k1gInPc6	let
2.4	[number]	k4	2.4
mil	míle	k1gFnPc2	míle
ptáků	pták	k1gMnPc2	pták
<g/>
/	/	kIx~	/
<g/>
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
-	-	kIx~	-
8000	[number]	k4	8000
kachen	kachna	k1gFnPc2	kachna
<g/>
/	/	kIx~	/
<g/>
rok	rok	k1gInSc1	rok
(	(	kIx(	(
<g/>
Anas	Anas	k1gInSc1	Anas
platyrhynchos	platyrhynchos	k1gInSc1	platyrhynchos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
1500	[number]	k4	1500
mrtvých	mrtvý	k1gMnPc2	mrtvý
labutí	labuť	k1gFnPc2	labuť
(	(	kIx(	(
<g/>
Cygnus	Cygnus	k1gMnSc1	Cygnus
olor	olor	k1gMnSc1	olor
<g/>
)	)	kIx)	)
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1981	[number]	k4	1981
<g/>
-	-	kIx~	-
<g/>
1984	[number]	k4	1984
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
60	[number]	k4	60
%	%	kIx~	%
otravou	otrava	k1gFnSc7	otrava
olovem	olovo	k1gNnSc7	olovo
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jeho	jeho	k3xOp3gFnSc3	jeho
nebezpečnosti	nebezpečnost	k1gFnSc3	nebezpečnost
je	být	k5eAaImIp3nS	být
omezeno	omezen	k2eAgNnSc1d1	omezeno
jeho	jeho	k3xOp3gNnSc4	jeho
používání	používání	k1gNnSc4	používání
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
elektronických	elektronický	k2eAgInPc6d1	elektronický
a	a	k8xC	a
elektrických	elektrický	k2eAgNnPc6d1	elektrické
zařízeních	zařízení	k1gNnPc6	zařízení
tzv.	tzv.	kA	tzv.
směrnicí	směrnice	k1gFnSc7	směrnice
RoHS	RoHS	k1gFnSc7	RoHS
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
rtutí	rtuť	k1gFnSc7	rtuť
<g/>
,	,	kIx,	,
kadmiem	kadmium	k1gNnSc7	kadmium
a	a	k8xC	a
dalším	další	k2eAgMnSc7d1	další
látkami	látka	k1gFnPc7	látka
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Bencko	Bencko	k1gNnSc1	Bencko
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Cikrt	Cikrt	k1gInSc1	Cikrt
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Lener	Lenra	k1gFnPc2	Lenra
<g/>
:	:	kIx,	:
Toxické	toxický	k2eAgInPc1d1	toxický
kovy	kov	k1gInPc1	kov
v	v	k7c6	v
životním	životní	k2eAgNnSc6d1	životní
a	a	k8xC	a
pracovním	pracovní	k2eAgNnSc6d1	pracovní
prostředí	prostředí	k1gNnSc6	prostředí
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
Grada	Grada	k1gFnSc1	Grada
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7169-150-X	[number]	k4	80-7169-150-X
Miroslav	Miroslava	k1gFnPc2	Miroslava
Šuta	šuta	k1gFnSc1	šuta
<g/>
:	:	kIx,	:
Účinky	účinek	k1gInPc1	účinek
výfukových	výfukový	k2eAgInPc2d1	výfukový
plynů	plyn	k1gInPc2	plyn
z	z	k7c2	z
automobilů	automobil	k1gInPc2	automobil
na	na	k7c4	na
lidské	lidský	k2eAgNnSc4d1	lidské
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
a	a	k8xC	a
Slovenský	slovenský	k2eAgInSc1d1	slovenský
dopravní	dopravní	k2eAgInSc1d1	dopravní
klub	klub	k1gInSc1	klub
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-901339-4-0	[number]	k4	80-901339-4-0
Cotton	Cotton	k1gInSc4	Cotton
F.	F.	kA	F.
<g/>
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc1	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1961	[number]	k4	1961
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwooda	k1gFnPc2	Greenwooda
<g/>
,	,	kIx,	,
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc1	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
olovo	olovo	k1gNnSc1	olovo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
olovo	olovo	k1gNnSc4	olovo
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
