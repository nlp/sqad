<s desamb="1">
Dle	dle	k7c2
současné	současný	k2eAgFnSc2d1
systematiky	systematika	k1gFnSc2
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
dinosaury	dinosaurus	k1gMnPc4
také	také	k9
ptáci	pták	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
jsou	být	k5eAaImIp3nP
jejich	jejich	k3xOp3gFnSc7
jedinou	jediný	k2eAgFnSc7d1
dosud	dosud	k6eAd1
žijící	žijící	k2eAgFnSc7d1
skupinou	skupina	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Dinosauři	dinosaurus	k1gMnPc1
se	se	k3xPyFc4
objevili	objevit	k5eAaPmAgMnP
ve	v	k7c6
středním	střední	k2eAgInSc6d1
nebo	nebo	k8xC
na	na	k7c6
počátku	počátek	k1gInSc6
svrchního	svrchní	k2eAgInSc2d1
triasu	trias	k1gInSc2
(	(	kIx(
<g/>
po	po	k7c6
permském	permský	k2eAgNnSc6d1
vymírání	vymírání	k1gNnSc6
<g/>
)	)	kIx)
asi	asi	k9
před	před	k7c7
250	#num#	k4
až	až	k8xS
235	#num#	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
<g/>
<g />
.	.	kIx.
</s>