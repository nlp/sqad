<s>
Dinosauři	dinosaurus	k1gMnPc1
</s>
<s>
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Dinosaurus	dinosaurus	k1gMnSc1
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
DinosauřiStratigrafický	DinosauřiStratigrafický	k2eAgInSc1d1
výskyt	výskyt	k1gInSc1
<g/>
:	:	kIx,
Střední	střední	k2eAgInSc1d1
trias	trias	k1gInSc1
až	až	k9
současnost	současnost	k1gFnSc4
(	(	kIx(
<g/>
ptáci	pták	k1gMnPc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
asi	asi	k9
před	před	k7c7
245	#num#	k4
<g/>
–	–	k?
<g/>
0	#num#	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Montáž	montáž	k1gFnSc1
fosilií	fosilie	k1gFnPc2
šesti	šest	k4xCc2
různých	různý	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
<g/>
,	,	kIx,
zachycující	zachycující	k2eAgFnSc1d1
různorodost	různorodost	k1gFnSc1
této	tento	k3xDgFnSc2
skupiny	skupina	k1gFnSc2
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordata	k1gFnSc1
<g/>
)	)	kIx)
Podkmen	podkmen	k1gInSc1
</s>
<s>
obratlovci	obratlovec	k1gMnPc1
(	(	kIx(
<g/>
Vertebrata	Vertebrat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
sauropsidi	sauropsid	k1gMnPc1
(	(	kIx(
<g/>
Sauropsida	Sauropsida	k1gFnSc1
<g/>
)	)	kIx)
Podtřída	podtřída	k1gFnSc1
</s>
<s>
diapsidi	diapsid	k1gMnPc1
(	(	kIx(
<g/>
Diapsida	Diapsida	k1gFnSc1
<g/>
)	)	kIx)
Nadřád	nadřád	k1gInSc1
</s>
<s>
dinosauři	dinosaurus	k1gMnPc1
(	(	kIx(
<g/>
Dinosauria	Dinosaurium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
Owen	Owen	k1gNnSc1
<g/>
,	,	kIx,
1842	#num#	k4
Podřazené	podřazený	k2eAgInPc1d1
taxony	taxon	k1gInPc1
</s>
<s>
Ornithoscelida	Ornithoscelida	k1gFnSc1
–	–	k?
ornitoscelidi	ornitoscelid	k1gMnPc5
</s>
<s>
Theropoda	Theropoda	k1gFnSc1
–	–	k?
teropodi	teropod	k1gMnPc5
</s>
<s>
†	†	k?
Ornithischia	Ornithischia	k1gFnSc1
–	–	k?
ptakopánví	ptakopánvit	k5eAaPmIp3nS,k5eAaImIp3nS
</s>
<s>
†	†	k?
Saurischia	Saurischia	k1gFnSc1
–	–	k?
plazopánví	plazopánvit	k5eAaPmIp3nS,k5eAaImIp3nS
</s>
<s>
†	†	k?
Sauropodomorpha	Sauropodomorpha	k1gFnSc1
–	–	k?
sauropodomorfové	sauropodomorfový	k2eAgFnPc4d1
</s>
<s>
†	†	k?
Herrerasauridae	Herrerasauridae	k1gFnPc2
–	–	k?
hererasauridi	hererasaurid	k1gMnPc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dinosauři	dinosaurus	k1gMnPc1
(	(	kIx(
<g/>
z	z	k7c2
řec.	řec.	k?
δ	δ	k?
–	–	k?
deinos	deinos	k1gInSc1
+	+	kIx~
σ	σ	k?
–	–	k?
sauros	saurosa	k1gFnPc2
=	=	kIx~
„	„	k?
<g/>
strašný	strašný	k2eAgMnSc1d1
ještěr	ještěr	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
překládáno	překládán	k2eAgNnSc1d1
„	„	k?
<g/>
hrozný	hrozný	k2eAgMnSc1d1
plaz	plaz	k1gMnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
rozmanitou	rozmanitý	k2eAgFnSc7d1
skupinou	skupina	k1gFnSc7
obratlovců	obratlovec	k1gMnPc2
řazenou	řazený	k2eAgFnSc7d1
mezi	mezi	k7c4
plazy	plaz	k1gMnPc4
(	(	kIx(
<g/>
Reptilia	Reptilia	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
dominovala	dominovat	k5eAaImAgFnS
fauně	fauna	k1gFnSc3
na	na	k7c6
pevninách	pevnina	k1gFnPc6
této	tento	k3xDgFnSc2
planety	planeta	k1gFnSc2
přes	přes	k7c4
134	#num#	k4
milionů	milion	k4xCgInPc2
let	léto	k1gNnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
období	období	k1gNnSc6
druhohor	druhohory	k1gFnPc2
(	(	kIx(
<g/>
zejména	zejména	k9
v	v	k7c6
jurské	jurský	k2eAgFnSc6d1
a	a	k8xC
křídové	křídový	k2eAgFnSc6d1
periodě	perioda	k1gFnSc6
<g/>
,	,	kIx,
asi	asi	k9
před	před	k7c7
201	#num#	k4
až	až	k8xS
66	#num#	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dle	dle	k7c2
současné	současný	k2eAgFnSc2d1
systematiky	systematika	k1gFnSc2
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
dinosaury	dinosaurus	k1gMnPc4
také	také	k9
ptáci	pták	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
jsou	být	k5eAaImIp3nP
jejich	jejich	k3xOp3gFnSc7
jedinou	jediný	k2eAgFnSc7d1
dosud	dosud	k6eAd1
žijící	žijící	k2eAgFnSc7d1
skupinou	skupina	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Dinosauři	dinosaurus	k1gMnPc1
se	se	k3xPyFc4
objevili	objevit	k5eAaPmAgMnP
ve	v	k7c6
středním	střední	k2eAgInSc6d1
nebo	nebo	k8xC
na	na	k7c6
počátku	počátek	k1gInSc6
svrchního	svrchní	k2eAgInSc2d1
triasu	trias	k1gInSc2
(	(	kIx(
<g/>
po	po	k7c6
permském	permský	k2eAgNnSc6d1
vymírání	vymírání	k1gNnSc6
<g/>
)	)	kIx)
asi	asi	k9
před	před	k7c7
250	#num#	k4
až	až	k8xS
235	#num#	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
vyhynuli	vyhynout	k5eAaPmAgMnP
(	(	kIx(
<g/>
kromě	kromě	k7c2
jedné	jeden	k4xCgFnSc2
větve	větev	k1gFnSc2
–	–	k?
ptáků	pták	k1gMnPc2
<g/>
)	)	kIx)
před	před	k7c7
66	#num#	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
v	v	k7c6
rámci	rámec	k1gInSc6
vymírání	vymírání	k1gNnSc2
na	na	k7c6
konci	konec	k1gInSc6
křídy	křída	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Jejich	jejich	k3xOp3gFnSc1
evoluční	evoluční	k2eAgFnSc1d1
radiace	radiace	k1gFnSc1
byla	být	k5eAaImAgFnS
velmi	velmi	k6eAd1
rychlá	rychlý	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Evoluce	evoluce	k1gFnSc1
</s>
<s>
Sesterskou	sesterský	k2eAgFnSc7d1
skupinou	skupina	k1gFnSc7
dinosaurů	dinosaurus	k1gMnPc2
je	být	k5eAaImIp3nS
čeleď	čeleď	k1gFnSc4
Silesauridae	Silesauridae	k1gNnSc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gNnSc1
zástupci	zástupce	k1gMnPc1
prokazatelně	prokazatelně	k6eAd1
existovali	existovat	k5eAaImAgMnP
již	již	k6eAd1
v	v	k7c6
období	období	k1gNnSc6
středního	střední	k2eAgInSc2d1
triasu	trias	k1gInSc2
(	(	kIx(
<g/>
geologický	geologický	k2eAgInSc1d1
věk	věk	k1gInSc1
anis	anisa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
asi	asi	k9
před	před	k7c7
247	#num#	k4
až	až	k8xS
242	#num#	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Protože	protože	k8xS
s	s	k7c7
dinosaury	dinosaurus	k1gMnPc7
měli	mít	k5eAaImAgMnP
společného	společný	k2eAgMnSc4d1
vývojového	vývojový	k2eAgMnSc4d1
předka	předek	k1gMnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
<g />
.	.	kIx.
</s>
<s hack="1">
prakticky	prakticky	k6eAd1
jisté	jistý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
nejstarší	starý	k2eAgMnPc1d3
dinosauři	dinosaurus	k1gMnPc1
museli	muset	k5eAaImAgMnP
existovat	existovat	k5eAaImF
již	již	k9
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Fosilie	fosilie	k1gFnSc1
nejstarších	starý	k2eAgFnPc2d3
forem	forma	k1gFnPc2
diapsidních	diapsidní	k2eAgMnPc2d1
plazů	plaz	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
by	by	kYmCp3nP
již	již	k6eAd1
mohli	moct	k5eAaImAgMnP
být	být	k5eAaImF
dinosaury	dinosaurus	k1gMnPc4
v	v	k7c6
pravém	pravý	k2eAgInSc6d1
smyslu	smysl	k1gInSc6
<g/>
,	,	kIx,
pocházejí	pocházet	k5eAaImIp3nP
z	z	k7c2
doby	doba	k1gFnSc2
před	před	k7c7
asi	asi	k9
245	#num#	k4
až	až	k8xS
235	#num#	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
nejstarší	starý	k2eAgFnPc1d3
prokazatelné	prokazatelný	k2eAgFnPc1d1
formy	forma	k1gFnPc1
pak	pak	k6eAd1
byly	být	k5eAaImAgFnP
objeveny	objevit	k5eAaPmNgFnP
v	v	k7c6
sedimentech	sediment	k1gInPc6
geologického	geologický	k2eAgNnSc2d1
souvrství	souvrství	k1gNnSc2
Santa	Sant	k1gMnSc2
Maria	Mario	k1gMnSc2
na	na	k7c6
území	území	k1gNnSc6
Brazílie	Brazílie	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
se	s	k7c7
stářím	stáří	k1gNnSc7
233,2	233,2	k4
milionu	milion	k4xCgInSc2
let	léto	k1gNnPc2
-	-	kIx~
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
například	například	k6eAd1
o	o	k7c4
rody	rod	k1gInPc4
Buriolestes	Buriolestesa	k1gFnPc2
<g/>
,	,	kIx,
Gnathovorax	Gnathovorax	k1gInSc1
nebo	nebo	k8xC
Saturnalia	Saturnalia	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
12	#num#	k4
<g/>
]	]	kIx)
První	první	k4xOgMnPc1
dinosauři	dinosaurus	k1gMnPc1
se	se	k3xPyFc4
pravděpodobně	pravděpodobně	k6eAd1
vyvinuli	vyvinout	k5eAaPmAgMnP
na	na	k7c6
pevninách	pevnina	k1gFnPc6
jižního	jižní	k2eAgInSc2d1
superkontinentu	superkontinent	k1gInSc2
Gondwany	Gondwana	k1gFnSc2
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
téměř	téměř	k6eAd1
s	s	k7c7
jistotou	jistota	k1gFnSc7
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
na	na	k7c6
jihu	jih	k1gInSc6
dnešní	dnešní	k2eAgFnSc2d1
Jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
k	k	k7c3
jejich	jejich	k3xOp3gInSc3
evolučnímu	evoluční	k2eAgInSc3d1
rozkvětu	rozkvět	k1gInSc3
došlo	dojít	k5eAaPmAgNnS
poprvé	poprvé	k6eAd1
v	v	k7c6
době	doba	k1gFnSc6
před	před	k7c7
asi	asi	k9
<g />
.	.	kIx.
</s>
<s hack="1">
234	#num#	k4
až	až	k9
232	#num#	k4
miliony	milion	k4xCgInPc1
let	léto	k1gNnPc2
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
obdobím	období	k1gNnSc7
výrazného	výrazný	k2eAgNnSc2d1
zvýšení	zvýšení	k1gNnSc2
vlhkosti	vlhkost	k1gFnSc2
klimatu	klima	k1gNnSc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
Karnská	Karnský	k2eAgFnSc1d1
pluviální	pluviální	k2eAgFnSc1d1
epizoda	epizoda	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Tato	tento	k3xDgFnSc1
událost	událost	k1gFnSc1
zřejmě	zřejmě	k6eAd1
<g />
.	.	kIx.
</s>
<s hack="1">
způsobila	způsobit	k5eAaPmAgFnS
rychlé	rychlý	k2eAgNnSc4d1
vystřídání	vystřídání	k1gNnSc4
vlhkého	vlhký	k2eAgNnSc2d1
a	a	k8xC
suchého	suchý	k2eAgNnSc2d1
klimatu	klima	k1gNnSc2
s	s	k7c7
vysokými	vysoký	k2eAgFnPc7d1
teplotami	teplota	k1gFnPc7
<g/>
,	,	kIx,
kterému	který	k3yRgInSc3,k3yQgInSc3,k3yIgInSc3
se	se	k3xPyFc4
nejlépe	dobře	k6eAd3
přizpůsobili	přizpůsobit	k5eAaPmAgMnP
právě	právě	k9
tehdejší	tehdejší	k2eAgMnPc1d1
vývojově	vývojově	k6eAd1
primitivní	primitivní	k2eAgMnPc1d1
dinosauři	dinosaurus	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Vznik	vznik	k1gInSc1
dinosaurů	dinosaurus	k1gMnPc2
na	na	k7c6
území	území	k1gNnSc6
současné	současný	k2eAgFnSc2d1
Jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
je	být	k5eAaImIp3nS
ale	ale	k8xC
některými	některý	k3yIgMnPc7
badateli	badatel	k1gMnPc7
zpochybňováno	zpochybňován	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Evoluce	evoluce	k1gFnSc1
dinosaurů	dinosaurus	k1gMnPc2
byla	být	k5eAaImAgFnS
relativně	relativně	k6eAd1
rychlá	rychlý	k2eAgFnSc1d1
a	a	k8xC
k	k	k7c3
výrazným	výrazný	k2eAgFnPc3d1
morfologickým	morfologický	k2eAgFnPc3d1
a	a	k8xC
anatomickým	anatomický	k2eAgFnPc3d1
změnám	změna	k1gFnPc3
v	v	k7c6
rámci	rámec	k1gInSc6
jednotlivých	jednotlivý	k2eAgFnPc2d1
vývojových	vývojový	k2eAgFnPc2d1
linií	linie	k1gFnPc2
docházelo	docházet	k5eAaImAgNnS
poměrně	poměrně	k6eAd1
často	často	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dinosauři	dinosaurus	k1gMnPc1
se	se	k3xPyFc4
objevili	objevit	k5eAaPmAgMnP
v	v	k7c6
prostředí	prostředí	k1gNnSc6
"	"	kIx"
<g/>
zaplněného	zaplněný	k2eAgInSc2d1
ekoprostoru	ekoprostor	k1gInSc2
<g/>
"	"	kIx"
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
dominovaly	dominovat	k5eAaImAgFnP
jiné	jiný	k2eAgFnPc1d1
formy	forma	k1gFnPc1
obratlovců	obratlovec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejpočetnějšími	početní	k2eAgMnPc7d3
zástupci	zástupce	k1gMnPc7
dinosaurů	dinosaurus	k1gMnPc2
v	v	k7c6
prvních	první	k4xOgInPc6
milionech	milion	k4xCgInPc6
let	léto	k1gNnPc2
jejich	jejich	k3xOp3gInSc2
známého	známý	k2eAgInSc2d1
vývoje	vývoj	k1gInSc2
byli	být	k5eAaImAgMnP
sauropodomorfové	sauropodomorf	k1gMnPc1
a	a	k8xC
hererasauři	hererasaur	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Značný	značný	k2eAgInSc4d1
vliv	vliv	k1gInSc4
na	na	k7c4
šíření	šíření	k1gNnSc4
a	a	k8xC
vývoj	vývoj	k1gInSc4
dinosaurů	dinosaurus	k1gMnPc2
měla	mít	k5eAaImAgFnS
v	v	k7c6
počátcích	počátek	k1gInPc6
jejich	jejich	k3xOp3gFnSc2
evoluce	evoluce	k1gFnSc2
úroveň	úroveň	k1gFnSc1
hladiny	hladina	k1gFnSc2
moří	moře	k1gNnPc2
a	a	k8xC
oceánů	oceán	k1gInPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
v	v	k7c6
období	období	k1gNnSc6
pozdního	pozdní	k2eAgInSc2d1
triasu	trias	k1gInSc2
výrazně	výrazně	k6eAd1
kolísala	kolísat	k5eAaImAgFnS
a	a	k8xC
střídavě	střídavě	k6eAd1
zaplavovala	zaplavovat	k5eAaImAgFnS
a	a	k8xC
odhalovala	odhalovat	k5eAaImAgFnS
pevninské	pevninský	k2eAgInPc4d1
mosty	most	k1gInPc4
a	a	k8xC
další	další	k2eAgNnSc4d1
spojení	spojení	k1gNnSc4
s	s	k7c7
jinými	jiný	k2eAgFnPc7d1
oblastmi	oblast	k1gFnPc7
potenciálního	potenciální	k2eAgInSc2d1
výskytu	výskyt	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Velký	velký	k2eAgInSc4d1
význam	význam	k1gInSc4
měli	mít	k5eAaImAgMnP
býložraví	býložravý	k2eAgMnPc1d1
dinosauři	dinosaurus	k1gMnPc1
pro	pro	k7c4
šíření	šíření	k1gNnSc4
semen	semeno	k1gNnPc2
tehdejších	tehdejší	k2eAgFnPc2d1
rostlin	rostlina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
ukázaly	ukázat	k5eAaPmAgFnP
některé	některý	k3yIgFnPc1
studie	studie	k1gFnPc1
<g/>
,	,	kIx,
díky	díky	k7c3
dinosaurům	dinosaurus	k1gMnPc3
se	se	k3xPyFc4
mohly	moct	k5eAaImAgFnP
mnohé	mnohý	k2eAgInPc4d1
druhy	druh	k1gInPc4
rostlin	rostlina	k1gFnPc2
s	s	k7c7
pevnými	pevný	k2eAgNnPc7d1
semeny	semeno	k1gNnPc7
(	(	kIx(
<g/>
odolávajícími	odolávající	k2eAgFnPc7d1
trávicímu	trávicí	k2eAgInSc3d1
traktu	trakt	k1gInSc3
býložravých	býložravý	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
<g/>
)	)	kIx)
šířit	šířit	k5eAaImF
až	až	k9
o	o	k7c4
desítky	desítka	k1gFnPc4
kilometrů	kilometr	k1gInPc2
za	za	k7c4
den	den	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Definice	definice	k1gFnSc1
a	a	k8xC
význam	význam	k1gInSc1
</s>
<s>
Veškeré	veškerý	k3xTgFnPc4
informace	informace	k1gFnPc4
o	o	k7c6
druhohorních	druhohorní	k2eAgMnPc6d1
dinosaurech	dinosaurus	k1gMnPc6
dnes	dnes	k6eAd1
máme	mít	k5eAaImIp1nP
k	k	k7c3
dispozici	dispozice	k1gFnSc3
pouze	pouze	k6eAd1
ze	z	k7c2
zachovaných	zachovaný	k2eAgFnPc2d1
fosilií	fosilie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ptáci	pták	k1gMnPc1
jsou	být	k5eAaImIp3nP
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
pokračovatelé	pokračovatel	k1gMnPc1
evoluční	evoluční	k2eAgFnSc2d1
linie	linie	k1gFnSc2
plazopánvých	plazopánvý	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
(	(	kIx(
<g/>
teropodů	teropod	k1gMnPc2
<g/>
,	,	kIx,
což	což	k3yRnSc4,k3yQnSc4
byli	být	k5eAaImAgMnP
obvykle	obvykle	k6eAd1
masožraví	masožravý	k2eAgMnPc1d1
dinosauři	dinosaurus	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
chodili	chodit	k5eAaImAgMnP
po	po	k7c6
dvou	dva	k4xCgNnPc6
a	a	k8xC
vykazovali	vykazovat	k5eAaImAgMnP
ptačí	ptačí	k2eAgInPc4d1
anatomické	anatomický	k2eAgInPc4d1
znaky	znak	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
například	například	k6eAd1
o	o	k7c4
duté	dutý	k2eAgFnPc4d1
kosti	kost	k1gFnPc4
<g/>
,	,	kIx,
plicní	plicní	k2eAgInPc4d1
vaky	vak	k1gInPc4
<g/>
,	,	kIx,
esovité	esovitý	k2eAgInPc4d1
krky	krk	k1gInPc4
a	a	k8xC
často	často	k6eAd1
i	i	k9
peří	peřit	k5eAaImIp3nP
jako	jako	k9
tělesný	tělesný	k2eAgInSc4d1
pokryv	pokryv	k1gInSc4
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
v	v	k7c6
posledních	poslední	k2eAgNnPc6d1
dvaceti	dvacet	k4xCc6
letech	léto	k1gNnPc6
dokázaly	dokázat	k5eAaPmAgFnP
intenzivní	intenzivní	k2eAgInPc4d1
výzkumy	výzkum	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
některých	některý	k3yIgMnPc2
paleontologů	paleontolog	k1gMnPc2
(	(	kIx(
<g/>
Robert	Robert	k1gMnSc1
T.	T.	kA
Bakker	Bakker	k1gMnSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
Ostrom	Ostrom	k1gInSc1
<g/>
,	,	kIx,
Jacques	Jacques	k1gMnSc1
Gauthier	Gauthier	k1gMnSc1
atd.	atd.	kA
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
pak	pak	k6eAd1
ptáci	pták	k1gMnPc1
přímo	přímo	k6eAd1
malí	malý	k2eAgMnPc1d1
<g/>
,	,	kIx,
agilní	agilní	k2eAgMnPc1d1
dinosauři	dinosaurus	k1gMnPc1
(	(	kIx(
<g/>
důkazem	důkaz	k1gInSc7
pro	pro	k7c4
tuto	tento	k3xDgFnSc4
teorii	teorie	k1gFnSc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
objev	objev	k1gInSc1
opeřených	opeřený	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
Liao-ning	Liao-ning	k1gInSc4
i	i	k8xC
jinde	jinde	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
navrhli	navrhnout	k5eAaPmAgMnP
vyčlenit	vyčlenit	k5eAaPmF
dinosaury	dinosaurus	k1gMnPc7
úplně	úplně	k6eAd1
ze	z	k7c2
třídy	třída	k1gFnSc2
Reptilia	Reptilius	k1gMnSc2
(	(	kIx(
<g/>
plazi	plaz	k1gMnPc1
<g/>
)	)	kIx)
a	a	k8xC
zahrnout	zahrnout	k5eAaPmF
je	on	k3xPp3gInPc4
společně	společně	k6eAd1
s	s	k7c7
ptáky	pták	k1gMnPc7
do	do	k7c2
nové	nový	k2eAgFnSc2d1
třídy	třída	k1gFnSc2
Dinosauria	Dinosaurium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každopádně	každopádně	k6eAd1
jde	jít	k5eAaImIp3nS
však	však	k9
o	o	k7c4
monofyletickou	monofyletický	k2eAgFnSc4d1
skupinu	skupina	k1gFnSc4
se	s	k7c7
společným	společný	k2eAgInSc7d1
předkem	předek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dinosauři	dinosaurus	k1gMnPc1
jsou	být	k5eAaImIp3nP
každopádně	každopádně	k6eAd1
přirozenou	přirozený	k2eAgFnSc7d1
skupinou	skupina	k1gFnSc7
vývojově	vývojově	k6eAd1
velmi	velmi	k6eAd1
úspěšných	úspěšný	k2eAgMnPc2d1
obratlovců	obratlovec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dnes	dnes	k6eAd1
již	již	k6eAd1
je	být	k5eAaImIp3nS
však	však	k9
důležitější	důležitý	k2eAgFnSc1d2
kladistika	kladistika	k1gFnSc1
nežli	nežli	k8xS
přesné	přesný	k2eAgNnSc1d1
určování	určování	k1gNnSc1
vnějších	vnější	k2eAgFnPc2d1
taxonových	taxonův	k2eAgFnPc2d1
kategorií	kategorie	k1gFnPc2
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
touto	tento	k3xDgFnSc7
problematikou	problematika	k1gFnSc7
zabývá	zabývat	k5eAaImIp3nS
jen	jen	k9
málokdo	málokdo	k3yInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
dinosaura	dinosaurus	k1gMnSc2
je	být	k5eAaImIp3nS
podle	podle	k7c2
definice	definice	k1gFnSc2
považován	považován	k2eAgMnSc1d1
jakýkoliv	jakýkoliv	k3yIgMnSc1
živočich	živočich	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
je	být	k5eAaImIp3nS
přímým	přímý	k2eAgMnSc7d1
potomkem	potomek	k1gMnSc7
posledního	poslední	k2eAgNnSc2d1
společného	společný	k2eAgMnSc4d1
předka	předek	k1gMnSc4
iguanodona	iguanodon	k1gMnSc4
a	a	k8xC
megalosaura	megalosaur	k1gMnSc4
(	(	kIx(
<g/>
někdy	někdy	k6eAd1
se	se	k3xPyFc4
také	také	k9
pro	pro	k7c4
tento	tento	k3xDgInSc4
účel	účel	k1gInSc4
„	„	k?
<g/>
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
“	“	k?
známějších	známý	k2eAgInPc2d2
rodů	rod	k1gInPc2
Tyrannosaurus	Tyrannosaurus	k1gInSc1
a	a	k8xC
Triceratops	Triceratops	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Evoluční	evoluční	k2eAgInSc1d1
význam	význam	k1gInSc1
</s>
<s>
Dinosauři	dinosaurus	k1gMnPc1
patří	patřit	k5eAaImIp3nP
mezi	mezi	k7c4
nejúspěšnější	úspěšný	k2eAgFnPc4d3
skupiny	skupina	k1gFnPc4
suchozemských	suchozemský	k2eAgMnPc2d1
obratlovců	obratlovec	k1gMnPc2
v	v	k7c6
dějinách	dějiny	k1gFnPc6
života	život	k1gInSc2
na	na	k7c6
Zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zcela	zcela	k6eAd1
dominovali	dominovat	k5eAaImAgMnP
suchozemským	suchozemský	k2eAgMnPc3d1
ekosystémům	ekosystém	k1gInPc3
v	v	k7c6
období	období	k1gNnSc6
jury	jura	k1gFnSc2
a	a	k8xC
křídy	křída	k1gFnSc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
celkem	celkem	k6eAd1
asi	asi	k9
134	#num#	k4
milionů	milion	k4xCgInPc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
jejich	jejich	k3xOp3gFnSc2
existence	existence	k1gFnSc2
nedosáhl	dosáhnout	k5eNaPmAgMnS
žádný	žádný	k3yNgMnSc1
jiný	jiný	k2eAgMnSc1d1
suchozemský	suchozemský	k2eAgMnSc1d1
živočich	živočich	k1gMnSc1
(	(	kIx(
<g/>
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
sladkovodních	sladkovodní	k2eAgMnPc2d1
krokodýlů	krokodýl	k1gMnPc2
a	a	k8xC
ptakoještěrů	ptakoještěr	k1gMnPc2
<g/>
)	)	kIx)
rozměrů	rozměr	k1gInPc2
přesahujících	přesahující	k2eAgInPc2d1
velikost	velikost	k1gFnSc4
jezevce	jezevec	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
je	být	k5eAaImIp3nS
však	však	k9
pravdou	pravda	k1gFnSc7
<g/>
,	,	kIx,
že	že	k8xS
jakkoliv	jakkoliv	k6eAd1
byli	být	k5eAaImAgMnP
dinosauři	dinosaurus	k1gMnPc1
v	v	k7c6
druhohorních	druhohorní	k2eAgInPc6d1
terestrických	terestrický	k2eAgInPc6d1
ekosystémech	ekosystém	k1gInPc6
významní	významný	k2eAgMnPc1d1
<g/>
,	,	kIx,
nebyli	být	k5eNaImAgMnP
zdaleka	zdaleka	k6eAd1
jedinými	jediný	k2eAgInPc7d1
početnými	početný	k2eAgInPc7d1
a	a	k8xC
diverzifikovanými	diverzifikovaný	k2eAgInPc7d1
organismy	organismus	k1gInPc7
tohoto	tento	k3xDgNnSc2
období	období	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmi	velmi	k6eAd1
hojní	hojnit	k5eAaImIp3nP
byli	být	k5eAaImAgMnP
i	i	k9
mnozí	mnohý	k2eAgMnPc1d1
další	další	k2eAgMnPc1d1
plazi	plaz	k1gMnPc1
<g/>
,	,	kIx,
kostnaté	kostnatý	k2eAgFnPc1d1
ryby	ryba	k1gFnPc1
<g/>
,	,	kIx,
hmyz	hmyz	k1gInSc1
apod.	apod.	kA
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
nicméně	nicméně	k8xC
pravděpodobné	pravděpodobný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
pokud	pokud	k8xS
by	by	kYmCp3nP
dinosauři	dinosaurus	k1gMnPc1
na	na	k7c6
konci	konec	k1gInSc6
křídy	křída	k1gFnSc2
před	před	k7c7
66	#num#	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
nevyhynuli	vyhynout	k5eNaPmAgMnP
<g/>
,	,	kIx,
savci	savec	k1gMnPc1
by	by	kYmCp3nP
se	se	k3xPyFc4
evolučně	evolučně	k6eAd1
rozvíjeli	rozvíjet	k5eAaImAgMnP
pomaleji	pomale	k6eAd2
a	a	k8xC
člověk	člověk	k1gMnSc1
by	by	kYmCp3nS
se	se	k3xPyFc4
možná	možná	k9
objevil	objevit	k5eAaPmAgInS
až	až	k9
mnohem	mnohem	k6eAd1
později	pozdě	k6eAd2
(	(	kIx(
<g/>
pokud	pokud	k8xS
vůbec	vůbec	k9
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
Přesto	přesto	k8xC
jsou	být	k5eAaImIp3nP
dosud	dosud	k6eAd1
dinosauři	dinosaurus	k1gMnPc1
v	v	k7c6
mnoha	mnoho	k4c6
ohledech	ohled	k1gInPc6
vnímáni	vnímat	k5eAaImNgMnP
jako	jako	k9
symbol	symbol	k1gInSc1
čehosi	cosi	k3yInSc2
zpátečnického	zpátečnický	k2eAgMnSc4d1
a	a	k8xC
neschopného	schopný	k2eNgMnSc4d1
změny	změna	k1gFnPc4
(	(	kIx(
<g/>
například	například	k6eAd1
pojem	pojem	k1gInSc4
„	„	k?
<g/>
politický	politický	k2eAgMnSc1d1
dinosaurus	dinosaurus	k1gMnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takový	takový	k3xDgInSc1
pohled	pohled	k1gInSc4
je	být	k5eAaImIp3nS
však	však	k9
nesprávný	správný	k2eNgMnSc1d1
a	a	k8xC
zcela	zcela	k6eAd1
absurdní	absurdní	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Anatomie	anatomie	k1gFnSc1
</s>
<s>
Carcharodontosaurus	Carcharodontosaurus	k1gMnSc1
<g/>
,	,	kIx,
jeden	jeden	k4xCgMnSc1
z	z	k7c2
obřích	obří	k2eAgMnPc2d1
teropodních	teropodní	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Dinosauři	dinosaurus	k1gMnPc1
patří	patřit	k5eAaImIp3nP
mezi	mezi	k7c4
diapsidní	diapsidní	k2eAgMnPc4d1
živočichy	živočich	k1gMnPc4
–	–	k?
mají	mít	k5eAaImIp3nP
dva	dva	k4xCgInPc1
spánkové	spánkový	k2eAgInPc1d1
otvory	otvor	k1gInPc1
na	na	k7c4
upevnění	upevnění	k1gNnSc4
žvýkacích	žvýkací	k2eAgInPc2d1
svalů	sval	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Dalším	další	k2eAgInSc7d1
typickým	typický	k2eAgInSc7d1
znakem	znak	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
odlišuje	odlišovat	k5eAaImIp3nS
dinosaury	dinosaurus	k1gMnPc4
od	od	k7c2
většiny	většina	k1gFnSc2
ostatních	ostatní	k2eAgMnPc2d1
plazů	plaz	k1gMnPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
postavení	postavení	k1gNnSc1
končetin	končetina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
Specialitou	specialita	k1gFnSc7
je	být	k5eAaImIp3nS
tzv.	tzv.	kA
čtvrtý	čtvrtý	k4xOgInSc4
trochanter	trochanter	k1gInSc4
<g/>
,	,	kIx,
sloužící	sloužící	k2eAgMnSc1d1
k	k	k7c3
upnutí	upnutí	k1gNnSc3
mohutných	mohutný	k2eAgInPc2d1
kaudofemorálních	kaudofemorální	k2eAgInPc2d1
svalů	sval	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ty	k3xPp2nSc1
pomáhaly	pomáhat	k5eAaImAgFnP
při	při	k7c6
pohybu	pohyb	k1gInSc6
zadních	zadní	k2eAgFnPc2d1
končetin	končetina	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
stahovaly	stahovat	k5eAaImAgFnP
dozadu	dozadu	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
U	u	k7c2
současných	současný	k2eAgMnPc2d1
plazů	plaz	k1gMnPc2
jsou	být	k5eAaImIp3nP
končetiny	končetina	k1gFnPc4
postaveny	postaven	k2eAgFnPc4d1
směrem	směr	k1gInSc7
do	do	k7c2
stran	strana	k1gFnPc2
(	(	kIx(
<g/>
např.	např.	kA
krokodýli	krokodýl	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
dinosauří	dinosauří	k2eAgFnPc1d1
končetiny	končetina	k1gFnPc1
směřují	směřovat	k5eAaImIp3nP
přímo	přímo	k6eAd1
pod	pod	k7c4
tělo	tělo	k1gNnSc4
(	(	kIx(
<g/>
jako	jako	k8xS,k8xC
u	u	k7c2
savců	savec	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přední	přední	k2eAgFnPc4d1
končetiny	končetina	k1gFnPc4
byly	být	k5eAaImAgFnP
obvykle	obvykle	k6eAd1
kratší	krátký	k2eAgFnPc1d2
než	než	k8xS
zadní	zadní	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc1d1
významný	významný	k2eAgInSc1d1
znak	znak	k1gInSc1
je	být	k5eAaImIp3nS
zvláštní	zvláštní	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
zadních	zadní	k2eAgFnPc2d1
končetin	končetina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stehenní	stehenní	k2eAgFnSc1d1
kost	kost	k1gFnSc1
byla	být	k5eAaImAgFnS
opatřena	opatřit	k5eAaPmNgFnS
kulovitou	kulovitý	k2eAgFnSc7d1
hlavicí	hlavice	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
zapadala	zapadat	k5eAaPmAgFnS,k5eAaImAgFnS
do	do	k7c2
pánevní	pánevní	k2eAgFnSc2d1
jamky	jamka	k1gFnSc2
a	a	k8xC
umožňovala	umožňovat	k5eAaImAgFnS
svislé	svislý	k2eAgNnSc4d1
umístění	umístění	k1gNnSc4
končetiny	končetina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lýtková	lýtkový	k2eAgFnSc1d1
kost	kost	k1gFnSc1
(	(	kIx(
<g/>
fibula	fibula	k1gFnSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
na	na	k7c6
vnější	vnější	k2eAgFnSc6d1
straně	strana	k1gFnSc6
mnohem	mnohem	k6eAd1
tenčí	tenký	k2eAgFnSc1d2
než	než	k8xS
holenní	holenní	k2eAgFnSc1d1
kost	kost	k1gFnSc1
(	(	kIx(
<g/>
tibia	tibia	k1gFnSc1
<g/>
)	)	kIx)
na	na	k7c6
straně	strana	k1gFnSc6
vnitřní	vnitřní	k2eAgFnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kyčelní	kyčelní	k2eAgFnSc1d1
jamka	jamka	k1gFnSc1
(	(	kIx(
<g/>
acetabulum	acetabulum	k1gInSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
charakteristicky	charakteristicky	k6eAd1
perforovaná	perforovaný	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
Speciální	speciální	k2eAgFnSc1d1
anatomická	anatomický	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
kyčelního	kyčelní	k2eAgInSc2d1
kloubu	kloub	k1gInSc2
umožnila	umožnit	k5eAaPmAgFnS
sauropodům	sauropod	k1gMnPc3
i	i	k8xC
teropodům	teropod	k1gMnPc3
dosáhnout	dosáhnout	k5eAaPmF
gigantických	gigantický	k2eAgInPc2d1
rozměrů	rozměr	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
Histologickým	histologický	k2eAgInSc7d1
rozborem	rozbor	k1gInSc7
fosilních	fosilní	k2eAgFnPc2d1
kostí	kost	k1gFnPc2
bylo	být	k5eAaImAgNnS
zjištěno	zjistit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
také	také	k9
dinosauři	dinosaurus	k1gMnPc1
měli	mít	k5eAaImAgMnP
<g />
.	.	kIx.
</s>
<s hack="1">
meziobratlové	meziobratlový	k2eAgFnPc4d1
ploténky	ploténka	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
osifikaci	osifikace	k1gFnSc3
křížových	křížový	k2eAgFnPc2d1
(	(	kIx(
<g/>
sakrálních	sakrální	k2eAgFnPc2d1
<g/>
)	)	kIx)
kostí	kost	k1gFnPc2
docházelo	docházet	k5eAaImAgNnS
u	u	k7c2
dinosaurů	dinosaurus	k1gMnPc2
už	už	k6eAd1
v	v	k7c6
počátcích	počátek	k1gInPc6
jejich	jejich	k3xOp3gInSc2
evolučního	evoluční	k2eAgInSc2d1
vývoje	vývoj	k1gInSc2
<g/>
,	,	kIx,
jak	jak	k6eAd1
ukázaly	ukázat	k5eAaPmAgInP
objevy	objev	k1gInPc4
z	z	k7c2
Brazílie	Brazílie	k1gFnSc2
(	(	kIx(
<g/>
pozdně	pozdně	k6eAd1
triasové	triasový	k2eAgNnSc1d1
souvrství	souvrství	k1gNnSc1
Candelária	Candelárium	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ocasy	ocas	k1gInPc1
dinosaurů	dinosaurus	k1gMnPc2
měly	mít	k5eAaImAgInP
velkou	velký	k2eAgFnSc4d1
tvarovou	tvarový	k2eAgFnSc4d1
i	i	k8xC
velikostní	velikostní	k2eAgFnSc4d1
variabilitu	variabilita	k1gFnSc4
<g/>
,	,	kIx,
od	od	k7c2
relativně	relativně	k6eAd1
krátkých	krátký	k2eAgInPc2d1
až	až	k9
zakrnělých	zakrnělý	k2eAgInPc2d1
(	(	kIx(
<g/>
a	a	k8xC
proměněných	proměněný	k2eAgInPc2d1
v	v	k7c4
synsakrum	synsakrum	k1gInSc4
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
u	u	k7c2
některých	některý	k3yIgInPc2
oviraptorosaurů	oviraptorosaur	k1gInPc2
<g/>
)	)	kIx)
až	až	k9
po	po	k7c4
extrémně	extrémně	k6eAd1
dlouhé	dlouhý	k2eAgInPc4d1
u	u	k7c2
některých	některý	k3yIgInPc2
sauropodů	sauropod	k1gInPc2
nebo	nebo	k8xC
uzpůsobené	uzpůsobený	k2eAgFnSc6d1
obranné	obranný	k2eAgFnSc6d1
funkci	funkce	k1gFnSc6
(	(	kIx(
<g/>
ankylosauři	ankylosaur	k1gMnPc1
<g/>
,	,	kIx,
stegosauři	stegosaur	k1gMnPc1
<g/>
,	,	kIx,
někteří	některý	k3yIgMnPc1
sauropodi	sauropod	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravděpodobně	pravděpodobně	k6eAd1
ale	ale	k8xC
neexistuje	existovat	k5eNaImIp3nS
přímý	přímý	k2eAgInSc4d1
vztah	vztah	k1gInSc4
mezi	mezi	k7c7
délkou	délka	k1gFnSc7
ocasu	ocas	k1gInSc2
a	a	k8xC
přední	přední	k2eAgFnSc7d1
částí	část	k1gFnSc7
těla	tělo	k1gNnSc2
ve	v	k7c6
smyslu	smysl	k1gInSc6
vyvažování	vyvažování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
různých	různý	k2eAgInPc2d1
skupiny	skupina	k1gFnPc1
dinosaurů	dinosaurus	k1gMnPc2
se	se	k3xPyFc4
ocasy	ocas	k1gInPc7
a	a	k8xC
jejich	jejich	k3xOp3gNnSc7
anatomická	anatomický	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
vyvíjely	vyvíjet	k5eAaImAgFnP
nezávisle	závisle	k6eNd1
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
především	především	k9
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c4
jejich	jejich	k3xOp3gFnSc4
funkci	funkce	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dinosauři	dinosaurus	k1gMnPc1
měli	mít	k5eAaImAgMnP
zuby	zub	k1gInPc4
zasazeny	zasazen	k2eAgInPc4d1
v	v	k7c6
jamkách	jamka	k1gFnPc6
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k9
krokodýli	krokodýl	k1gMnPc1
<g/>
,	,	kIx,
u	u	k7c2
plazů	plaz	k1gMnPc2
vyrůstají	vyrůstat	k5eAaImIp3nP
zuby	zub	k1gInPc1
přímo	přímo	k6eAd1
z	z	k7c2
čelistní	čelistní	k2eAgFnSc2d1
kosti	kost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dinosauří	dinosauří	k2eAgFnSc1d1
páteř	páteř	k1gFnSc1
byla	být	k5eAaImAgFnS
tvořena	tvořit	k5eAaImNgFnS
různým	různý	k2eAgInSc7d1
počtem	počet	k1gInSc7
obratlů	obratel	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
byly	být	k5eAaImAgInP
mnohdy	mnohdy	k6eAd1
duté	dutý	k2eAgInPc1d1
a	a	k8xC
vyplněné	vyplněný	k2eAgInPc1d1
vzdušnými	vzdušný	k2eAgInPc7d1
vaky	vak	k1gInPc7
(	(	kIx(
<g/>
plazopánví	plazopánev	k1gFnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žebra	žebro	k1gNnPc1
byla	být	k5eAaImAgFnS
připojena	připojit	k5eAaPmNgFnS
dvěma	dva	k4xCgFnPc7
kloubními	kloubní	k2eAgFnPc7d1
hlavicemi	hlavice	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Stejně	stejně	k6eAd1
jako	jako	k9
ostatní	ostatní	k2eAgMnPc1d1
plazi	plaz	k1gMnPc1
byli	být	k5eAaImAgMnP
dinosauři	dinosaurus	k1gMnPc1
vejcorodí	vejcorodý	k2eAgMnPc1d1
(	(	kIx(
<g/>
přestože	přestože	k8xS
se	se	k3xPyFc4
již	již	k6eAd1
objevily	objevit	k5eAaPmAgFnP
teorie	teorie	k1gFnPc1
o	o	k7c6
živorodosti	živorodost	k1gFnSc6
některých	některý	k3yIgInPc2
druhů	druh	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gNnPc1
vejce	vejce	k1gNnPc1
však	však	k9
byla	být	k5eAaImAgNnP
původně	původně	k6eAd1
kožovitá	kožovitý	k2eAgNnPc1d1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
ale	ale	k8xC
později	pozdě	k6eAd2
měla	mít	k5eAaImAgFnS
pevnou	pevný	k2eAgFnSc4d1
skořápku	skořápka	k1gFnSc4
inkrustovanou	inkrustovaný	k2eAgFnSc4d1
minerálními	minerální	k2eAgFnPc7d1
látkami	látka	k1gFnPc7
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k9
vejce	vejce	k1gNnSc1
dnešních	dnešní	k2eAgMnPc2d1
ptáků	pták	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Výzkum	výzkum	k1gInSc1
L.	L.	kA
Witmera	Witmera	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
prokázal	prokázat	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
většina	většina	k1gFnSc1
dinosaurů	dinosaurus	k1gMnPc2
měla	mít	k5eAaImAgFnS
zřejmě	zřejmě	k6eAd1
odlehčené	odlehčený	k2eAgFnPc4d1
lebky	lebka	k1gFnPc4
a	a	k8xC
ty	ten	k3xDgFnPc1
byly	být	k5eAaImAgFnP
do	do	k7c2
značné	značný	k2eAgFnSc2d1
míry	míra	k1gFnSc2
vyplněné	vyplněný	k2eAgInPc1d1
vzdušnými	vzdušný	k2eAgFnPc7d1
dutinami	dutina	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
Výzkum	výzkum	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
2018	#num#	k4
dokládá	dokládat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
výrazné	výrazný	k2eAgFnPc4d1
změny	změna	k1gFnPc4
na	na	k7c6
pánevních	pánevní	k2eAgFnPc6d1
kostech	kost	k1gFnPc6
některých	některý	k3yIgFnPc2
skupin	skupina	k1gFnPc2
dinosaurů	dinosaurus	k1gMnPc2
(	(	kIx(
<g/>
například	například	k6eAd1
maniraptorů	maniraptor	k1gInPc2
a	a	k8xC
ptakopánvých	ptakopánvý	k2eAgFnPc6d1
<g/>
)	)	kIx)
souvisejí	souviset	k5eAaImIp3nP
zejména	zejména	k9
s	s	k7c7
dýchacím	dýchací	k2eAgInSc7d1
systémem	systém	k1gInSc7
a	a	k8xC
nikoliv	nikoliv	k9
s	s	k7c7
přechodem	přechod	k1gInSc7
k	k	k7c3
herbivorii	herbivorie	k1gFnSc3
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
mnozí	mnohý	k2eAgMnPc1d1
paleontologové	paleontolog	k1gMnPc1
domnívali	domnívat	k5eAaImAgMnP
dříve	dříve	k6eAd2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Peří	peřit	k5eAaImIp3nS
</s>
<s>
Kůže	kůže	k1gFnSc1
zejména	zejména	k9
teropodních	teropodní	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
byla	být	k5eAaImAgFnS
často	často	k6eAd1
pokrytá	pokrytý	k2eAgFnSc1d1
peřím	peřit	k5eAaImIp1nS
nebo	nebo	k8xC
proto-peřím	proto-peřit	k5eAaPmIp1nS
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
ti	ten	k3xDgMnPc1
dinosauři	dinosaurus	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
v	v	k7c6
evoluci	evoluce	k1gFnSc6
o	o	k7c6
peří	peří	k1gNnSc6
přišli	přijít	k5eAaPmAgMnP
<g/>
,	,	kIx,
měli	mít	k5eAaImAgMnP
</s>
<s>
tlustou	tlustý	k2eAgFnSc4d1
kůži	kůže	k1gFnSc4
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
dnešní	dnešní	k2eAgMnPc1d1
sloni	slon	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
některých	některý	k3yIgMnPc2
druhů	druh	k1gInPc2
<g/>
,	,	kIx,
zejména	zejména	k9
velkých	velký	k2eAgMnPc2d1
býložravců	býložravec	k1gMnPc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
vyvinuly	vyvinout	k5eAaPmAgInP
mohutné	mohutný	k2eAgInPc1d1
kostěné	kostěný	k2eAgInPc1d1
pláty	plát	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existovali	existovat	k5eAaImAgMnP
také	také	k9
četní	četný	k2eAgMnPc1d1
opeření	opeřený	k2eAgMnPc1d1
dinosauři	dinosaurus	k1gMnPc1
<g/>
,	,	kIx,
ze	z	k7c2
kterých	který	k3yIgFnPc2,k3yRgFnPc2,k3yQgFnPc2
se	se	k3xPyFc4
později	pozdě	k6eAd2
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
během	během	k7c2
jurského	jurský	k2eAgNnSc2d1
období	období	k1gNnSc2
<g/>
,	,	kIx,
vyvinuli	vyvinout	k5eAaPmAgMnP
praví	pravý	k2eAgMnPc1d1
ptáci	pták	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
známe	znát	k5eAaImIp1nP
kolem	kolem	k7c2
dvaceti	dvacet	k4xCc2
druhů	druh	k1gInPc2
malých	malý	k2eAgInPc2d1
dravých	dravý	k2eAgInPc2d1
teropodů	teropod	k1gInPc2
s	s	k7c7
pernatým	pernatý	k2eAgInSc7d1
integumentem	integument	k1gInSc7
(	(	kIx(
<g/>
pokryvem	pokryv	k1gInSc7
těla	tělo	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
téměř	téměř	k6eAd1
všechny	všechen	k3xTgInPc4
pocházejí	pocházet	k5eAaImIp3nP
z	z	k7c2
provincie	provincie	k1gFnSc2
Liao-ning	Liao-ning	k1gInSc1
v	v	k7c6
Číně	Čína	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
se	se	k3xPyFc4
nedokonalé	dokonalý	k2eNgNnSc1d1
proto-peří	proto-peří	k1gNnSc1
objevilo	objevit	k5eAaPmAgNnS
i	i	k9
u	u	k7c2
některých	některý	k3yIgMnPc2
býložravých	býložravý	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Peří	peří	k1gNnSc1
známe	znát	k5eAaImIp1nP
jak	jak	k6eAd1
u	u	k7c2
plazopánvých	plazopánvý	k2eAgInPc2d1
<g/>
,	,	kIx,
ale	ale	k8xC
protopeří	protopeřit	k5eAaPmIp3nS
i	i	k9
u	u	k7c2
ptakopánvých	ptakopánvý	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jelikož	jelikož	k8xS
nejmladší	mladý	k2eAgMnSc1d3
spoječný	spoječný	k2eAgMnSc1d1
předchůdce	předchůdce	k1gMnSc1
plazopánvých	plazopánvý	k2eAgFnPc2d1
a	a	k8xC
ptakopánvých	ptakopánvý	k2eAgFnPc2d1
žil	žíla	k1gFnPc2
ve	v	k7c6
stejné	stejný	k2eAgFnSc6d1
době	doba	k1gFnSc6
jako	jako	k8xS,k8xC
první	první	k4xOgMnPc1
dinosauři	dinosaurus	k1gMnPc1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
pravděpodobné	pravděpodobný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
první	první	k4xOgMnPc1
dinosauři	dinosaurus	k1gMnPc1
byli	být	k5eAaImAgMnP
opeření	opeření	k1gNnSc4
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
peří	peří	k1gNnSc1
je	být	k5eAaImIp3nS
pravděpodobně	pravděpodobně	k6eAd1
znakem	znak	k1gInSc7
dinosaurů	dinosaurus	k1gMnPc2
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
srst	srst	k1gFnSc1
je	být	k5eAaImIp3nS
znakem	znak	k1gInSc7
savců	savec	k1gMnPc2
a	a	k8xC
podobně	podobně	k6eAd1
jako	jako	k9
někteří	některý	k3yIgMnPc1
savci	savec	k1gMnPc1
ztratili	ztratit	k5eAaPmAgMnP
srst	srst	k1gFnSc4
<g/>
,	,	kIx,
tak	tak	k8xC,k8xS
někteří	některý	k3yIgMnPc1
dinosauři	dinosaurus	k1gMnPc1
ztratili	ztratit	k5eAaPmAgMnP
peří	peří	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Možná	možná	k6eAd1
se	se	k3xPyFc4
tedy	tedy	k9
u	u	k7c2
předka	předek	k1gMnSc2
všech	všecek	k3xTgMnPc2
dinosaurů	dinosaurus	k1gMnPc2
objevilo	objevit	k5eAaPmAgNnS
ještě	ještě	k6eAd1
v	v	k7c6
triasu	trias	k1gInSc6
nebo	nebo	k8xC
se	se	k3xPyFc4
u	u	k7c2
dinosarů	dinosar	k1gInPc2
peří	peří	k1gNnSc2
objevilo	objevit	k5eAaPmAgNnS
několikrát	několikrát	k6eAd1
nezávisle	závisle	k6eNd1
na	na	k7c6
sobě	sebe	k3xPyFc6
<g/>
,	,	kIx,
první	první	k4xOgFnSc1
verze	verze	k1gFnSc1
je	být	k5eAaImIp3nS
však	však	k9
mnohem	mnohem	k6eAd1
pravděpodobnější	pravděpodobný	k2eAgMnSc1d2
<g/>
.	.	kIx.
</s>
<s>
Ačkoliv	ačkoliv	k8xS
současní	současný	k2eAgMnPc1d1
ptáci	pták	k1gMnPc1
jsou	být	k5eAaImIp3nP
přímými	přímý	k2eAgMnPc7d1
potomky	potomek	k1gMnPc7
teropodních	teropodní	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
(	(	kIx(
<g/>
resp.	resp.	kA
jsou	být	k5eAaImIp3nP
sami	sám	k3xTgMnPc1
přežívajícími	přežívající	k2eAgMnPc7d1
teropodními	teropodní	k2eAgMnPc7d1
dinosaury	dinosaurus	k1gMnPc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ani	ani	k8xC
v	v	k7c6
případě	případ	k1gInSc6
druhotně	druhotně	k6eAd1
nelétavých	létavý	k2eNgInPc2d1
druhů	druh	k1gInPc2
jejich	jejich	k3xOp3gInSc1
mozky	mozek	k1gInPc1
nejsou	být	k5eNaImIp3nP
neurofyziologickým	neurofyziologický	k2eAgInSc7d1
ekvivalentem	ekvivalent	k1gInSc7
mozků	mozek	k1gInPc2
druhohorních	druhohorní	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
současnosti	současnost	k1gFnSc6
se	se	k3xPyFc4
již	již	k6eAd1
paleontologové	paleontolog	k1gMnPc1
shodují	shodovat	k5eAaImIp3nP
na	na	k7c6
závěru	závěr	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
ptáci	pták	k1gMnPc1
vznikli	vzniknout	k5eAaPmAgMnP
v	v	k7c6
průběhu	průběh	k1gInSc6
jurské	jurský	k2eAgFnSc2d1
periody	perioda	k1gFnSc2
z	z	k7c2
opeřených	opeřený	k2eAgMnPc2d1
teropodních	teropodní	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozměry	rozměra	k1gFnPc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Velikost	velikost	k1gFnSc1
dinosaurů	dinosaurus	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Lebka	lebka	k1gFnSc1
tyranosaura	tyranosaur	k1gMnSc2
<g/>
,	,	kIx,
jednoho	jeden	k4xCgMnSc4
z	z	k7c2
nejpopulárnějších	populární	k2eAgMnPc2d3
druhohorních	druhohorní	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Dinosauři	dinosaurus	k1gMnPc1
ze	z	k7c2
skupiny	skupina	k1gFnSc2
Sauropoda	Sauropoda	k1gFnSc1
představovali	představovat	k5eAaImAgMnP
největší	veliký	k2eAgMnPc4d3
suchozemské	suchozemský	k2eAgMnPc4d1
živočichy	živočich	k1gMnPc4
všech	všecek	k3xTgFnPc2
dob	doba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
Důvody	důvod	k1gInPc4
jejich	jejich	k3xOp3gInSc2
gigantismu	gigantismus	k1gInSc2
zřejmě	zřejmě	k6eAd1
souvisejí	souviset	k5eAaImIp3nP
především	především	k9
s	s	k7c7
jejich	jejich	k3xOp3gInSc7
ontogenetickým	ontogenetický	k2eAgInSc7d1
růstem	růst	k1gInSc7
<g/>
,	,	kIx,
způsobem	způsob	k1gInSc7
reprodukce	reprodukce	k1gFnSc2
a	a	k8xC
fyziologií	fyziologie	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
Někteří	některý	k3yIgMnPc1
snad	snad	k9
mohli	moct	k5eAaImAgMnP
dosáhnout	dosáhnout	k5eAaPmF
délky	délka	k1gFnPc4
až	až	k6eAd1
kolem	kolem	k7c2
40	#num#	k4
metrů	metr	k1gInPc2
(	(	kIx(
<g/>
avšak	avšak	k8xC
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
nejisté	jistý	k2eNgInPc4d1
fosilní	fosilní	k2eAgInPc4d1
objevy	objev	k1gInPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tedy	tedy	k8xC
zhruba	zhruba	k6eAd1
o	o	k7c4
10	#num#	k4
metrů	metr	k1gInPc2
více	hodně	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
dosahují	dosahovat	k5eAaImIp3nP
největší	veliký	k2eAgFnPc4d3
velryby	velryba	k1gFnPc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
a	a	k8xC
hmotnosti	hmotnost	k1gFnSc2
až	až	k9
přes	přes	k7c4
100	#num#	k4
tun	tuna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výrazně	výrazně	k6eAd1
těžší	těžký	k2eAgNnSc1d2
je	být	k5eAaImIp3nS
pouze	pouze	k6eAd1
jediný	jediný	k2eAgInSc1d1
žijící	žijící	k2eAgInSc1d1
druh	druh	k1gInSc1
kytovce	kytovec	k1gMnSc2
–	–	k?
plejtvák	plejtvák	k1gMnSc1
obrovský	obrovský	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zatím	zatím	k6eAd1
největším	veliký	k2eAgInSc7d3
relativně	relativně	k6eAd1
dobře	dobře	k6eAd1
známým	známý	k2eAgMnSc7d1
dinosaurem	dinosaurus	k1gMnSc7
je	být	k5eAaImIp3nS
rod	rod	k1gInSc1
Argentinosaurus	Argentinosaurus	k1gInSc1
<g/>
,	,	kIx,
u	u	k7c2
něhož	jenž	k3xRgInSc2
se	se	k3xPyFc4
předpokládá	předpokládat	k5eAaImIp3nS
hmotnost	hmotnost	k1gFnSc1
zhruba	zhruba	k6eAd1
70	#num#	k4
až	až	k9
96	#num#	k4
tun	tuna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
Vážil	vážit	k5eAaImAgInS
tedy	tedy	k9
asi	asi	k9
tolik	tolik	k4xDc1,k4yIc1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
15	#num#	k4
dospělých	dospělý	k2eAgMnPc2d1
slonů	slon	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Dosud	dosud	k6eAd1
nejpřesnější	přesný	k2eAgMnSc1d3
dosud	dosud	k6eAd1
získaný	získaný	k2eAgInSc1d1
odhad	odhad	k1gInSc1
hmotnosti	hmotnost	k1gFnSc2
tohoto	tento	k3xDgMnSc2
obřího	obří	k2eAgMnSc2d1
sauropoda	sauropod	k1gMnSc2
činí	činit	k5eAaImIp3nS
asi	asi	k9
85	#num#	k4
000	#num#	k4
kg	kg	kA
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
tedy	tedy	k9
těžší	těžký	k2eAgMnSc1d2
než	než	k8xS
například	například	k6eAd1
dopravní	dopravní	k2eAgFnSc1d1
letadlo	letadlo	k1gNnSc4
Boeing	boeing	k1gInSc1
737	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
Podobně	podobně	k6eAd1
velký	velký	k2eAgMnSc1d1
byl	být	k5eAaImAgInS
také	také	k9
Puertasaurus	Puertasaurus	k1gInSc1
nebo	nebo	k8xC
Patagotitan	Patagotitan	k1gInSc1
<g/>
,	,	kIx,
rovněž	rovněž	k9
z	z	k7c2
Argentiny	Argentina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Některé	některý	k3yIgFnPc1
izolované	izolovaný	k2eAgFnPc1d1
obrovské	obrovský	k2eAgFnPc1d1
fosilní	fosilní	k2eAgFnPc1d1
kosti	kost	k1gFnPc1
naznačují	naznačovat	k5eAaImIp3nP
existenci	existence	k1gFnSc4
ještě	ještě	k6eAd1
větších	veliký	k2eAgMnPc2d2
dinosaurů	dinosaurus	k1gMnPc2
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
např.	např.	kA
Bruhathkayosaurus	Bruhathkayosaurus	k1gInSc1
z	z	k7c2
Indie	Indie	k1gFnSc2
<g/>
,	,	kIx,
původní	původní	k2eAgInSc1d1
materiál	materiál	k1gInSc1
však	však	k9
již	již	k6eAd1
není	být	k5eNaImIp3nS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
roku	rok	k1gInSc3
2016	#num#	k4
bylo	být	k5eAaImAgNnS
známo	znám	k2eAgNnSc1d1
asi	asi	k9
osm	osm	k4xCc1
sauropodních	sauropodní	k2eAgInPc2d1
rodů	rod	k1gInPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnSc1
hmotnost	hmotnost	k1gFnSc1
podle	podle	k7c2
odhadů	odhad	k1gInPc2
<g />
.	.	kIx.
</s>
<s hack="1">
přesahovala	přesahovat	k5eAaImAgFnS
60	#num#	k4
metrických	metrický	k2eAgFnPc2d1
tun	tuna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
však	však	k9
existovali	existovat	k5eAaImAgMnP
i	i	k9
velmi	velmi	k6eAd1
drobní	drobný	k2eAgMnPc1d1
dinosauři	dinosaurus	k1gMnPc1
(	(	kIx(
<g/>
např.	např.	kA
dromeosauridi	dromeosaurid	k1gMnPc1
Microraptor	Microraptor	k1gMnSc1
nebo	nebo	k8xC
Epidendrosaurus	Epidendrosaurus	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
je	být	k5eAaImIp3nS
však	však	k9
často	často	k6eAd1
řazen	řadit	k5eAaImNgInS
k	k	k7c3
Avialae	Avialae	k1gFnSc3
<g/>
,	,	kIx,
tedy	tedy	k8xC
již	již	k9
k	k	k7c3
„	„	k?
<g/>
ptačím	ptačí	k2eAgMnPc3d1
dinosaurům	dinosaurus	k1gMnPc3
<g/>
“	“	k?
do	do	k7c2
čeledi	čeleď	k1gFnSc2
Scansoriopterygidae	Scansoriopterygida	k1gFnSc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
validita	validita	k1gFnSc1
je	být	k5eAaImIp3nS
diskutabilní	diskutabilní	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
dosahovali	dosahovat	k5eAaImAgMnP
velikostí	velikost	k1gFnSc7
jen	jen	k6eAd1
v	v	k7c6
řádu	řád	k1gInSc6
desítek	desítka	k1gFnPc2
centimetrů	centimetr	k1gInPc2
a	a	k8xC
stovek	stovka	k1gFnPc2
gramů	gram	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejmenším	malý	k2eAgInSc7d3
známým	známý	k2eAgInSc7d1
druhem	druh	k1gInSc7
teropodního	teropodní	k2eAgMnSc2d1
dinosaura	dinosaurus	k1gMnSc2
je	být	k5eAaImIp3nS
podle	podle	k7c2
moderní	moderní	k2eAgFnSc2d1
systematiky	systematika	k1gFnSc2
kolibřík	kolibřík	k1gMnSc1
kalypta	kalypta	k1gMnSc1
nejmenší	malý	k2eAgMnSc1d3
(	(	kIx(
<g/>
Mellisuga	Mellisuga	k1gFnSc1
helenae	helenae	k1gFnSc1
<g/>
)	)	kIx)
s	s	k7c7
průměrnou	průměrný	k2eAgFnSc7d1
délkou	délka	k1gFnSc7
těla	tělo	k1gNnSc2
kolem	kolem	k7c2
6	#num#	k4
centimetrů	centimetr	k1gInPc2
a	a	k8xC
hmotností	hmotnost	k1gFnPc2
kolem	kolem	k7c2
2	#num#	k4
gramů	gram	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nejdelší	dlouhý	k2eAgMnPc1d3
dinosauři	dinosaurus	k1gMnPc1
</s>
<s>
Z	z	k7c2
důvodu	důvod	k1gInSc2
nekompletnosti	nekompletnost	k1gFnSc2
fosilního	fosilní	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
a	a	k8xC
skutečnosti	skutečnost	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
nikdy	nikdy	k6eAd1
nenalezneme	naleznout	k5eNaPmIp1nP,k5eNaBmIp1nP
skutečného	skutečný	k2eAgMnSc4d1
rekordmana	rekordman	k1gMnSc4
<g/>
,	,	kIx,
nelze	lze	k6eNd1
brát	brát	k5eAaImF
tabulky	tabulka	k1gFnPc4
rekordů	rekord	k1gInPc2
zcela	zcela	k6eAd1
vážně	vážně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
zde	zde	k6eAd1
nicméně	nicméně	k8xC
indicie	indicie	k1gFnSc1
(	(	kIx(
<g/>
znalost	znalost	k1gFnSc1
blízkých	blízký	k2eAgMnPc2d1
příbuzných	příbuzný	k1gMnPc2
<g/>
,	,	kIx,
biomechanické	biomechanický	k2eAgFnPc1d1
studie	studie	k1gFnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
nám	my	k3xPp1nPc3
dovolují	dovolovat	k5eAaImIp3nP
byť	byť	k8xS
přibližně	přibližně	k6eAd1
určit	určit	k5eAaPmF
délku	délka	k1gFnSc4
<g/>
,	,	kIx,
výšku	výška	k1gFnSc4
či	či	k8xC
hmotnost	hmotnost	k1gFnSc4
zvířete	zvíře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dle	dle	k7c2
těchto	tento	k3xDgFnPc2
indicií	indicie	k1gFnPc2
by	by	kYmCp3nP
pak	pak	k9
k	k	k7c3
nejdelším	dlouhý	k2eAgMnPc3d3
dinosaurům	dinosaurus	k1gMnPc3
mohl	moct	k5eAaImAgMnS
patřit	patřit	k5eAaImF
severoamerický	severoamerický	k2eAgInSc4d1
rod	rod	k1gInSc4
Maarapunisaurus	Maarapunisaurus	k1gMnSc1
fragillimus	fragillimus	k1gMnSc1
s	s	k7c7
délkou	délka	k1gFnSc7
kolem	kolem	k7c2
32	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
dalším	další	k2eAgMnPc3d1
obrům	obr	k1gMnPc3
patřily	patřit	k5eAaImAgInP
rody	rod	k1gInPc1
Bruhathkayosaurus	Bruhathkayosaurus	k1gInSc1
s	s	k7c7
odhadovanou	odhadovaný	k2eAgFnSc7d1
délkou	délka	k1gFnSc7
až	až	k6eAd1
kolem	kolem	k7c2
44	#num#	k4
metrů	metr	k1gInPc2
(	(	kIx(
<g/>
u	u	k7c2
tohoto	tento	k3xDgInSc2
rodu	rod	k1gInSc2
je	být	k5eAaImIp3nS
však	však	k9
možné	možný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
zkamenělý	zkamenělý	k2eAgInSc4d1
kmen	kmen	k1gInSc4
stromu	strom	k1gInSc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
Puertasaurus	Puertasaurus	k1gInSc1
s	s	k7c7
35	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
–	–	k?
<g/>
40	#num#	k4
m	m	kA
<g/>
,	,	kIx,
Argentinosaurus	Argentinosaurus	k1gInSc1
s	s	k7c7
30	#num#	k4
<g/>
–	–	k?
<g/>
37	#num#	k4
m	m	kA
<g/>
,	,	kIx,
Turiasaurus	Turiasaurus	k1gInSc1
30	#num#	k4
<g/>
–	–	k?
<g/>
39	#num#	k4
m	m	kA
(	(	kIx(
<g/>
dosud	dosud	k6eAd1
největší	veliký	k2eAgMnSc1d3
suchozemský	suchozemský	k2eAgMnSc1d1
živočich	živočich	k1gMnSc1
<g/>
,	,	kIx,
jaký	jaký	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
kdy	kdy	k6eAd1
byl	být	k5eAaImAgMnS
na	na	k7c6
evropském	evropský	k2eAgInSc6d1
kontinentu	kontinent	k1gInSc6
nalezen	naleznout	k5eAaPmNgMnS,k5eAaBmNgMnS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Supersaurus	Supersaurus	k1gInSc1
s	s	k7c7
35	#num#	k4
m	m	kA
<g/>
–	–	k?
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
40	#num#	k4
m	m	kA
<g/>
,	,	kIx,
Sauroposeidon	Sauroposeidon	k1gInSc1
s	s	k7c7
29	#num#	k4
<g/>
–	–	k?
<g/>
34	#num#	k4
m	m	kA
(	(	kIx(
<g/>
s	s	k7c7
výškou	výška	k1gFnSc7
snad	snad	k9
18	#num#	k4
metrů	metr	k1gInPc2
nejvyšší	vysoký	k2eAgMnSc1d3
dosud	dosud	k6eAd1
známý	známý	k2eAgMnSc1d1
dinosaurus	dinosaurus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
„	„	k?
<g/>
Seismosaurus	Seismosaurus	k1gInSc1
<g/>
“	“	k?
s	s	k7c7
33	#num#	k4
<g/>
–	–	k?
<g/>
35	#num#	k4
m	m	kA
<g/>
,	,	kIx,
Paralititan	Paralititan	k1gInSc1
s	s	k7c7
26	#num#	k4
<g/>
–	–	k?
<g/>
35	#num#	k4
m	m	kA
<g/>
,	,	kIx,
Antarctosaurus	Antarctosaurus	k1gInSc1
s	s	k7c7
až	až	k6eAd1
40	#num#	k4
m	m	kA
nebo	nebo	k8xC
Argyrosaurus	Argyrosaurus	k1gInSc1
s	s	k7c7
18	#num#	k4
<g/>
–	–	k?
<g/>
30	#num#	k4
m.	m.	k?
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Největší	veliký	k2eAgMnPc1d3
draví	dravý	k2eAgMnPc1d1
dinosauři	dinosaurus	k1gMnPc1
</s>
<s>
Model	model	k1gInSc4
druhu	druh	k1gInSc2
Giganotosaurus	Giganotosaurus	k1gInSc1
carolinii	carolinium	k1gNnPc7
</s>
<s>
Největší	veliký	k2eAgMnPc1d3
draví	dravý	k2eAgMnPc1d1
dinosauři	dinosaurus	k1gMnPc1
(	(	kIx(
<g/>
teropodi	teropod	k1gMnPc1
<g/>
)	)	kIx)
představují	představovat	k5eAaImIp3nP
společně	společně	k6eAd1
s	s	k7c7
některými	některý	k3yIgMnPc7
zástupci	zástupce	k1gMnPc7
krokodýlů	krokodýl	k1gMnPc2
zároveň	zároveň	k6eAd1
největší	veliký	k2eAgMnPc4d3
suchozemské	suchozemský	k2eAgMnPc4d1
dravce	dravec	k1gMnPc4
všech	všecek	k3xTgFnPc2
dob	doba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dlouhou	dlouhý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
držel	držet	k5eAaImAgInS
primát	primát	k1gInSc1
rekordního	rekordní	k2eAgMnSc2d1
predátora	predátor	k1gMnSc2
známý	známý	k2eAgInSc1d1
severoamerický	severoamerický	k2eAgInSc1d1
Tyrannosaurus	Tyrannosaurus	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
minulého	minulý	k2eAgNnSc2d1
století	století	k1gNnSc2
ho	on	k3xPp3gMnSc4
však	však	k9
překonal	překonat	k5eAaPmAgMnS
argentinský	argentinský	k2eAgMnSc1d1
obr	obr	k1gMnSc1
Giganotosaurus	Giganotosaurus	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
měřil	měřit	k5eAaImAgInS
na	na	k7c4
délku	délka	k1gFnSc4
kolem	kolem	k7c2
13	#num#	k4
metrů	metr	k1gInPc2
a	a	k8xC
vážil	vážit	k5eAaImAgInS
snad	snad	k9
až	až	k9
kolem	kolem	k7c2
8	#num#	k4
tun	tuna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ani	ani	k8xC
tento	tento	k3xDgInSc1
teropod	teropod	k1gInSc1
však	však	k9
zřejmě	zřejmě	k6eAd1
nebyl	být	k5eNaImAgInS
největší	veliký	k2eAgInSc1d3
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
potvrzují	potvrzovat	k5eAaImIp3nP
nedávné	dávný	k2eNgInPc1d1
nálezy	nález	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
je	být	k5eAaImIp3nS
držitelem	držitel	k1gMnSc7
velikostního	velikostní	k2eAgInSc2d1
primátu	primát	k1gInSc2
mezi	mezi	k7c7
teropody	teropod	k1gMnPc7
Spinosaurus	Spinosaurus	k1gInSc4
<g/>
,	,	kIx,
vědecky	vědecky	k6eAd1
popsaný	popsaný	k2eAgInSc1d1
již	již	k9
roku	rok	k1gInSc2
1915	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
nejdelším	dlouhý	k2eAgMnPc3d3
teropodům	teropod	k1gMnPc3
patřil	patřit	k5eAaImAgInS
severoafrický	severoafrický	k2eAgInSc4d1
rod	rod	k1gInSc4
Spinosaurus	Spinosaurus	k1gMnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc4
se	s	k7c7
svými	svůj	k3xOyFgInPc7
16	#num#	k4
<g/>
–	–	k?
<g/>
18	#num#	k4
metry	metr	k1gInPc7
neměl	mít	k5eNaImAgMnS
v	v	k7c6
dospělosti	dospělost	k1gFnSc6
žádného	žádný	k3yNgMnSc2
přirozeného	přirozený	k2eAgMnSc2d1
nepřítele	nepřítel	k1gMnSc2
(	(	kIx(
<g/>
nepočítáme	počítat	k5eNaImIp1nP
<g/>
-li	-li	k?
příslušníky	příslušník	k1gMnPc4
stejného	stejný	k2eAgInSc2d1
druhu	druh	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
Šlo	jít	k5eAaImAgNnS
však	však	k9
o	o	k7c4
velmi	velmi	k6eAd1
specializovanou	specializovaný	k2eAgFnSc4d1
formu	forma	k1gFnSc4
dravého	dravý	k2eAgMnSc2d1
dinosaura	dinosaurus	k1gMnSc2
<g/>
,	,	kIx,
uzpůsobenou	uzpůsobený	k2eAgFnSc4d1
na	na	k7c4
lov	lov	k1gInSc4
ryb	ryba	k1gFnPc2
ve	v	k7c6
vodním	vodní	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
závěsu	závěs	k1gInSc6
za	za	k7c7
ním	on	k3xPp3gMnSc7
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
rod	rod	k1gInSc1
Giganotosaurus	Giganotosaurus	k1gInSc1
s	s	k7c7
13,5	13,5	k4
<g/>
–	–	k?
<g/>
13,7	13,7	k4
m	m	kA
<g/>
,	,	kIx,
Tyrannotitan	Tyrannotitan	k1gInSc1
s	s	k7c7
13,4	13,4	k4
m	m	kA
<g/>
,	,	kIx,
Deltadromeus	Deltadromeus	k1gInSc1
s	s	k7c7
13,3	13,3	k4
m	m	kA
<g/>
,	,	kIx,
Mapusaurus	Mapusaurus	k1gInSc1
s	s	k7c7
12,8	12,8	k4
m	m	kA
<g/>
,	,	kIx,
Tyrannosaurus	Tyrannosaurus	k1gInSc1
s	s	k7c7
12,5	12,5	k4
m	m	kA
nebo	nebo	k8xC
Carcharodontosaurus	Carcharodontosaurus	k1gInSc1
s	s	k7c7
11,1	11,1	k4
<g/>
–	–	k?
<g/>
13,5	13,5	k4
m.	m.	k?
Tyto	tento	k3xDgInPc4
údaje	údaj	k1gInPc4
jsou	být	k5eAaImIp3nP
však	však	k9
pouze	pouze	k6eAd1
přibližné	přibližný	k2eAgNnSc1d1
a	a	k8xC
v	v	k7c6
budoucnu	budoucno	k1gNnSc6
nejspíš	nejspíš	k9
dojde	dojít	k5eAaPmIp3nS
k	k	k7c3
jejich	jejich	k3xOp3gFnSc3
revizi	revize	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Inteligence	inteligence	k1gFnSc1
dinosaurů	dinosaurus	k1gMnPc2
</s>
<s>
Obecně	obecně	k6eAd1
byli	být	k5eAaImAgMnP
dinosauři	dinosaurus	k1gMnPc1
v	v	k7c6
porovnání	porovnání	k1gNnSc6
se	s	k7c7
současnými	současný	k2eAgMnPc7d1
savci	savec	k1gMnPc7
a	a	k8xC
ptáky	pták	k1gMnPc7
relativně	relativně	k6eAd1
málo	málo	k6eAd1
inteligentními	inteligentní	k2eAgMnPc7d1
tvory	tvor	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
ale	ale	k8xC
významné	významný	k2eAgFnPc4d1
výjimky	výjimka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
jednoho	jeden	k4xCgMnSc4
z	z	k7c2
nejinteligentnějších	inteligentní	k2eAgMnPc2d3
známých	známý	k2eAgMnPc2d1
druhohorních	druhohorní	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
je	být	k5eAaImIp3nS
obecně	obecně	k6eAd1
považován	považován	k2eAgInSc1d1
menší	malý	k2eAgInSc1d2
deinonychosaurní	deinonychosaurní	k2eAgInSc1d1
teropod	teropod	k1gInSc1
druhu	druh	k1gInSc2
Troodon	Troodon	k1gMnSc1
formosus	formosus	k1gMnSc1
ze	z	k7c2
svrchní	svrchní	k2eAgFnSc2d1
křídy	křída	k1gFnSc2
USA	USA	kA
a	a	k8xC
Kanady	Kanada	k1gFnSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
encefalizační	encefalizační	k2eAgInSc1d1
kvocient	kvocient	k1gInSc1
(	(	kIx(
<g/>
EQ	EQ	kA
–	–	k?
vyjadřující	vyjadřující	k2eAgFnSc4d1
poměrnou	poměrný	k2eAgFnSc4d1
velikost	velikost	k1gFnSc4
mozku	mozek	k1gInSc2
k	k	k7c3
tělu	tělo	k1gNnSc3
oproti	oproti	k7c3
stejně	stejně	k6eAd1
těžkému	těžký	k2eAgMnSc3d1
krokodýlovi	krokodýl	k1gMnSc3
<g/>
)	)	kIx)
činil	činit	k5eAaImAgInS
až	až	k9
6,5	6,5	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Troodon	Troodon	k1gInSc1
byl	být	k5eAaImAgInS
tak	tak	k9
zřejmě	zřejmě	k6eAd1
stejně	stejně	k6eAd1
inteligentní	inteligentní	k2eAgFnPc1d1
jako	jako	k8xC,k8xS
dnešní	dnešní	k2eAgFnPc1d1
šelmy	šelma	k1gFnPc1
a	a	k8xC
někteří	některý	k3yIgMnPc1
ptáci	pták	k1gMnPc1
(	(	kIx(
<g/>
a	a	k8xC
mnohem	mnohem	k6eAd1
inteligentnější	inteligentní	k2eAgMnPc1d2
než	než	k8xS
dnešní	dnešní	k2eAgMnPc1d1
plazi	plaz	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objevily	objevit	k5eAaPmAgInP
se	se	k3xPyFc4
také	také	k9
hypotetické	hypotetický	k2eAgFnPc1d1
rekonstrukce	rekonstrukce	k1gFnPc1
možného	možný	k2eAgNnSc2d1
vzezření	vzezření	k1gNnSc2
vysoce	vysoce	k6eAd1
inteligentních	inteligentní	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
dinosauroidů	dinosauroid	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
by	by	kYmCp3nP
podle	podle	k7c2
názoru	názor	k1gInSc2
některých	některý	k3yIgMnPc2
vědců	vědec	k1gMnPc2
(	(	kIx(
<g/>
např.	např.	kA
Dale	Dale	k1gFnSc1
Russell	Russell	k1gMnSc1
<g/>
)	)	kIx)
mohli	moct	k5eAaImAgMnP
v	v	k7c6
případě	případ	k1gInSc6
přežití	přežití	k1gNnSc2
do	do	k7c2
kenozoika	kenozoikum	k1gNnSc2
vzniknout	vzniknout	k5eAaPmF
postupným	postupný	k2eAgInSc7d1
vývojem	vývoj	k1gInSc7
z	z	k7c2
některých	některý	k3yIgInPc2
troodontidů	troodontid	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
tyto	tento	k3xDgInPc4
názory	názor	k1gInPc4
však	však	k9
chybí	chybit	k5eAaPmIp3nP,k5eAaImIp3nP
jakékoliv	jakýkoliv	k3yIgInPc1
fosilní	fosilní	k2eAgInPc1d1
doklady	doklad	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nedávno	nedávno	k6eAd1
byl	být	k5eAaImAgInS
však	však	k9
změřen	změřen	k2eAgInSc1d1
objem	objem	k1gInSc1
mozkovny	mozkovna	k1gFnSc2
jiného	jiný	k2eAgMnSc2d1
malého	malý	k2eAgMnSc2d1
svrchnokřídového	svrchnokřídový	k2eAgMnSc2d1
teropoda	teropod	k1gMnSc2
druhu	druh	k1gInSc2
Bambiraptor	Bambiraptor	k1gMnSc1
feinbergi	feinberg	k1gFnSc2
(	(	kIx(
<g/>
objeven	objeven	k2eAgMnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1995	#num#	k4
v	v	k7c6
Montaně	Montana	k1gFnSc6
<g/>
,	,	kIx,
USA	USA	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledek	výsledek	k1gInSc1
odpovídá	odpovídat	k5eAaImIp3nS
téměř	téměř	k6eAd1
dvojnásobku	dvojnásobek	k1gInSc3
hodnoty	hodnota	k1gFnSc2
u	u	k7c2
troodonta	troodont	k1gInSc2
(	(	kIx(
<g/>
EQ	EQ	kA
12,5	12,5	k4
až	až	k9
13,8	13,8	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
bylo	být	k5eAaImAgNnS
zjištěno	zjistit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
tento	tento	k3xDgMnSc1
dinosaurus	dinosaurus	k1gMnSc1
používal	používat	k5eAaImAgMnS
„	„	k?
<g/>
vratiprst	vratiprst	k1gInSc4
<g/>
“	“	k?
a	a	k8xC
mohl	moct	k5eAaImAgInS
tedy	tedy	k9
možná	možná	k9
manipulovat	manipulovat	k5eAaImF
s	s	k7c7
předměty	předmět	k1gInPc7
(	(	kIx(
<g/>
na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
však	však	k9
šlo	jít	k5eAaImAgNnS
ještě	ještě	k9
o	o	k7c4
mládě	mládě	k1gNnSc4
<g/>
,	,	kIx,
u	u	k7c2
kterého	který	k3yQgInSc2,k3yIgInSc2,k3yRgInSc2
je	být	k5eAaImIp3nS
mozkovna	mozkovna	k1gFnSc1
relativně	relativně	k6eAd1
větší	veliký	k2eAgFnSc1d2
v	v	k7c6
poměru	poměr	k1gInSc6
k	k	k7c3
tělu	tělo	k1gNnSc3
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Porovnáváme	porovnávat	k5eAaImIp1nP
<g/>
-li	-li	k?
EQ	EQ	kA
neptačích	ptačí	k2eNgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
s	s	k7c7
ptáky	pták	k1gMnPc7
<g/>
,	,	kIx,
vycházejí	vycházet	k5eAaImIp3nP
nám	my	k3xPp1nPc3
obvykle	obvykle	k6eAd1
hodnoty	hodnota	k1gFnPc1
v	v	k7c6
širokém	široký	k2eAgNnSc6d1
rozpětí	rozpětí	k1gNnSc6
0,05	0,05	k4
až	až	k9
1,4	1,4	k4
<g/>
.	.	kIx.
</s>
<s>
Vůbec	vůbec	k9
nejinteligentnějšími	inteligentní	k2eAgMnPc7d3
dinosaury	dinosaurus	k1gMnPc7
jsou	být	k5eAaImIp3nP
zřejmě	zřejmě	k6eAd1
dnešní	dnešní	k2eAgFnPc1d1
vrány	vrána	k1gFnPc1
(	(	kIx(
<g/>
konkrétně	konkrétně	k6eAd1
vrána	vrána	k1gFnSc1
novokaledonská	novokaledonský	k2eAgFnSc1d1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
pozná	poznat	k5eAaPmIp3nS
v	v	k7c6
zrcadle	zrcadlo	k1gNnSc6
a	a	k8xC
používá	používat	k5eAaImIp3nS
jednoduché	jednoduchý	k2eAgInPc4d1
nástroje	nástroj	k1gInPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
dinosaurů	dinosaurus	k1gMnPc2
(	(	kIx(
<g/>
především	především	k6eAd1
býložravých	býložravý	k2eAgInPc2d1
<g/>
)	)	kIx)
však	však	k9
zřejmě	zřejmě	k6eAd1
příliš	příliš	k6eAd1
vysoké	vysoký	k2eAgFnSc2d1
inteligence	inteligence	k1gFnSc2
nedosahovala	dosahovat	k5eNaImAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
Jinou	jiný	k2eAgFnSc7d1
otázkou	otázka	k1gFnSc7
je	být	k5eAaImIp3nS
také	také	k9
komunikace	komunikace	k1gFnSc1
dinosaurů	dinosaurus	k1gMnPc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
mohla	moct	k5eAaImAgFnS
nabývat	nabývat	k5eAaImF
mnoha	mnoho	k4c2
různých	různý	k2eAgFnPc2d1
podob	podoba	k1gFnPc2
–	–	k?
vokalizace	vokalizace	k1gFnSc2
<g/>
,	,	kIx,
zvuk	zvuk	k1gInSc1
produkovaný	produkovaný	k2eAgInSc1d1
chrastěním	chrastění	k1gNnPc3
tělesného	tělesný	k2eAgNnSc2d1
„	„	k?
<g/>
brnění	brnění	k1gNnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
dupáním	dupání	k1gNnSc7
<g/>
,	,	kIx,
máváním	mávání	k1gNnSc7
opeřenými	opeřený	k2eAgFnPc7d1
končetinami	končetina	k1gFnPc7
a	a	k8xC
mnoho	mnoho	k4c1
dalších	další	k2eAgInPc2d1
způsobů	způsob	k1gInPc2
dorozumívání	dorozumívání	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mozek	mozek	k1gInSc1
dinosaurů	dinosaurus	k1gMnPc2
se	se	k3xPyFc4
vyvíjel	vyvíjet	k5eAaImAgInS
a	a	k8xC
rostl	růst	k5eAaImAgInS
rovnoměrným	rovnoměrný	k2eAgNnSc7d1
tempem	tempo	k1gNnSc7
a	a	k8xC
tvarově	tvarově	k6eAd1
se	se	k3xPyFc4
příliš	příliš	k6eAd1
neodlišoval	odlišovat	k5eNaImAgInS
u	u	k7c2
malých	malý	k2eAgNnPc2d1
mláďat	mládě	k1gNnPc2
a	a	k8xC
dospělců	dospělec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobný	podobný	k2eAgInSc4d1
vzorec	vzorec	k1gInSc4
nacházíme	nacházet	k5eAaImIp1nP
i	i	k9
u	u	k7c2
dnešních	dnešní	k2eAgMnPc2d1
ptáků	pták	k1gMnPc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
u	u	k7c2
krokodýlovitých	krokodýlovitý	k2eAgMnPc2d1
plazů	plaz	k1gMnPc2
je	být	k5eAaImIp3nS
zcela	zcela	k6eAd1
odlišný	odlišný	k2eAgInSc1d1
(	(	kIx(
<g/>
tvar	tvar	k1gInSc1
mozku	mozek	k1gInSc2
prochází	procházet	k5eAaImIp3nS
v	v	k7c6
průběhu	průběh	k1gInSc6
ontogeneze	ontogeneze	k1gFnSc2
významnými	významný	k2eAgFnPc7d1
změnami	změna	k1gFnPc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zbarvení	zbarvení	k1gNnSc1
dinosaurů	dinosaurus	k1gMnPc2
</s>
<s>
Celkový	celkový	k2eAgInSc1d1
vzhled	vzhled	k1gInSc1
hraje	hrát	k5eAaImIp3nS
v	v	k7c6
životě	život	k1gInSc6
zvířete	zvíře	k1gNnSc2
velkou	velký	k2eAgFnSc4d1
roli	role	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
nejen	nejen	k6eAd1
důležitou	důležitý	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
sexuálního	sexuální	k2eAgInSc2d1
výběru	výběr	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
často	často	k6eAd1
slouží	sloužit	k5eAaImIp3nS
i	i	k9
jako	jako	k8xC,k8xS
ochrana	ochrana	k1gFnSc1
proti	proti	k7c3
predátorům	predátor	k1gMnPc3
<g/>
,	,	kIx,
po	po	k7c6
případě	případ	k1gInSc6
jako	jako	k8xC,k8xS
maskovací	maskovací	k2eAgInSc1d1
úbor	úbor	k1gInSc1
lovců	lovec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dinosauři	dinosaurus	k1gMnPc1
byli	být	k5eAaImAgMnP
zpočátku	zpočátku	k6eAd1
představováni	představován	k2eAgMnPc1d1
jako	jako	k8xC,k8xS
šedí	šedý	k2eAgMnPc1d1
tvorové	tvor	k1gMnPc1
bez	bez	k7c2
jakýchkoli	jakýkoli	k3yIgInPc2
výrazných	výrazný	k2eAgInPc2d1
barevných	barevný	k2eAgInPc2d1
znaků	znak	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
není	být	k5eNaImIp3nS
důvod	důvod	k1gInSc1
tento	tento	k3xDgInSc4
názor	názor	k1gInSc4
považovat	považovat	k5eAaImF
za	za	k7c4
prokazatelný	prokazatelný	k2eAgInSc4d1
fakt	fakt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
jako	jako	k9
i	i	k9
dnešní	dnešní	k2eAgFnSc1d1
fauna	fauna	k1gFnSc1
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
zcela	zcela	k6eAd1
jistě	jistě	k6eAd1
i	i	k9
dinosauři	dinosaurus	k1gMnPc1
barevnými	barevný	k2eAgMnPc7d1
živočichy	živočich	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
ke	k	k7c3
skutečnosti	skutečnost	k1gFnSc3
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
nikdy	nikdy	k6eAd1
nedozvíme	dozvědět	k5eNaPmIp1nP
<g/>
,	,	kIx,
jakými	jaký	k3yQgFnPc7,k3yIgFnPc7,k3yRgFnPc7
barvami	barva	k1gFnPc7
tito	tento	k3xDgMnPc1
diapsidní	diapsidní	k2eAgMnPc1d1
plazi	plaz	k1gMnPc1
oplývali	oplývat	k5eAaImAgMnP
<g/>
,	,	kIx,
lze	lze	k6eAd1
na	na	k7c4
toto	tento	k3xDgNnSc4
téma	téma	k1gNnSc4
pouze	pouze	k6eAd1
spekulovat	spekulovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Různorodost	různorodost	k1gFnSc1
zbarvení	zbarvení	k1gNnSc2
záležela	záležet	k5eAaImAgFnS
na	na	k7c6
způsobu	způsob	k1gInSc6
života	život	k1gInSc2
daného	daný	k2eAgInSc2d1
druhu	druh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvíře	zvíře	k1gNnSc1
žijící	žijící	k2eAgFnSc1d1
v	v	k7c6
lesích	les	k1gInPc6
se	se	k3xPyFc4
z	z	k7c2
důvodu	důvod	k1gInSc2
maskování	maskování	k1gNnSc1
jistě	jistě	k9
lišilo	lišit	k5eAaImAgNnS
od	od	k7c2
tvorů	tvor	k1gMnPc2
obývajících	obývající	k2eAgMnPc2d1
volná	volný	k2eAgNnPc4d1
prostranství	prostranství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pestrost	pestrost	k1gFnSc1
byla	být	k5eAaImAgFnS
určitě	určitě	k6eAd1
výhodou	výhoda	k1gFnSc7
i	i	k8xC
během	běh	k1gInSc7
námluv	námluva	k1gFnPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
si	se	k3xPyFc3
samice	samice	k1gFnPc1
vybraly	vybrat	k5eAaPmAgFnP
jedince	jedinec	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
jim	on	k3xPp3gMnPc3
nejvíce	hodně	k6eAd3,k6eAd1
imponovali	imponovat	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
přítomnosti	přítomnost	k1gFnSc3
peří	peřit	k5eAaImIp3nS
u	u	k7c2
mnoha	mnoho	k4c2
druhů	druh	k1gInPc2
dinosaurů	dinosaurus	k1gMnPc2
je	být	k5eAaImIp3nS
předpoklad	předpoklad	k1gInSc1
zbarvení	zbarvení	k1gNnSc1
velmi	velmi	k6eAd1
pravděpodobný	pravděpodobný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počátkem	počátkem	k7c2
roku	rok	k1gInSc2
2010	#num#	k4
bylo	být	k5eAaImAgNnS
vědci	vědec	k1gMnSc6
oznámeno	oznámit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
byly	být	k5eAaImAgFnP
objeveny	objeven	k2eAgFnPc4d1
molekuly	molekula	k1gFnPc4
pigmentů	pigment	k1gInPc2
(	(	kIx(
<g/>
původních	původní	k2eAgNnPc2d1
barviv	barvivo	k1gNnPc2
<g/>
)	)	kIx)
v	v	k7c6
primitivním	primitivní	k2eAgNnSc6d1
peří	peří	k1gNnSc6
neptačích	ptačí	k2eNgMnPc2d1
teropodních	teropodní	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
rodu	rod	k1gInSc2
Sinosauropteryx	Sinosauropteryx	k1gInSc1
<g/>
,	,	kIx,
Anchiornis	Anchiornis	k1gInSc1
<g/>
,	,	kIx,
Caudipteryx	Caudipteryx	k1gInSc1
a	a	k8xC
snad	snad	k9
i	i	k9
některých	některý	k3yIgFnPc2
dalších	další	k2eAgFnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
historicky	historicky	k6eAd1
vůbec	vůbec	k9
první	první	k4xOgFnSc4
příležitost	příležitost	k1gFnSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
identifikována	identifikován	k2eAgFnSc1d1
barva	barva	k1gFnSc1
neptačího	ptačí	k2eNgMnSc2d1
dinosaura	dinosaurus	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
již	již	k6eAd1
známe	znát	k5eAaImIp1nP
barvu	barva	k1gFnSc4
u	u	k7c2
šesti	šest	k4xCc2
druhů	druh	k1gInPc2
druhohorních	druhohorní	k2eAgInPc2d1
teropodů	teropod	k1gInPc2
–	–	k?
Sinosauropteryx	Sinosauropteryx	k1gInSc4
<g/>
,	,	kIx,
Anchiornis	Anchiornis	k1gFnSc4
<g/>
,	,	kIx,
Microraptor	Microraptor	k1gInSc4
<g/>
,	,	kIx,
Sinornithosaurus	Sinornithosaurus	k1gInSc4
<g/>
,	,	kIx,
Archaeopteryx	Archaeopteryx	k1gInSc4
a	a	k8xC
Confuciusornis	Confuciusornis	k1gFnSc4
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Hnízdo	hnízdo	k1gNnSc4
s	s	k7c7
fosilizovanými	fosilizovaný	k2eAgNnPc7d1
vajíčky	vajíčko	k1gNnPc7
kachnozobého	kachnozobý	k2eAgMnSc2d1
dinosaura	dinosaurus	k1gMnSc2
druhu	druh	k1gInSc2
Maiasaura	Maiasaura	k1gFnSc1
peeblesorum	peeblesorum	k1gInSc4
v	v	k7c4
Museum	museum	k1gNnSc4
of	of	k?
the	the	k?
Rockies	Rockies	k1gMnSc1
<g/>
,	,	kIx,
Bozeman	Bozeman	k1gMnSc1
<g/>
,	,	kIx,
Montana	Montana	k1gFnSc1
</s>
<s>
Reprodukční	reprodukční	k2eAgFnSc1d1
biologie	biologie	k1gFnSc1
dinosaurů	dinosaurus	k1gMnPc2
</s>
<s>
Dinosauři	dinosaurus	k1gMnPc1
byli	být	k5eAaImAgMnP
často	často	k6eAd1
společenskými	společenský	k2eAgMnPc7d1
tvory	tvor	k1gMnPc7
<g/>
,	,	kIx,
žijícími	žijící	k2eAgMnPc7d1
trvale	trvale	k6eAd1
ve	v	k7c6
skupinách	skupina	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
Podobně	podobně	k6eAd1
jako	jako	k9
všichni	všechen	k3xTgMnPc1
dnešní	dnešní	k2eAgMnPc1d1
plazi	plaz	k1gMnPc1
a	a	k8xC
ptáci	pták	k1gMnPc1
se	se	k3xPyFc4
rozmnožovali	rozmnožovat	k5eAaImAgMnP
výhradně	výhradně	k6eAd1
vejci	vejce	k1gNnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
Četné	četný	k2eAgInPc4d1
objevy	objev	k1gInPc4
vajíček	vajíčko	k1gNnPc2
i	i	k8xC
celých	celý	k2eAgNnPc2d1
hnízd	hnízdo	k1gNnPc2
nebo	nebo	k8xC
hnízdních	hnízdní	k2eAgFnPc2d1
kolonií	kolonie	k1gFnPc2
byly	být	k5eAaImAgFnP
od	od	k7c2
20	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc4
minulého	minulý	k2eAgNnSc2d1
století	století	k1gNnSc2
postupně	postupně	k6eAd1
učiněny	učiněn	k2eAgFnPc1d1
prakticky	prakticky	k6eAd1
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
(	(	kIx(
<g/>
především	především	k9
Mongolsko	Mongolsko	k1gNnSc1
<g/>
,	,	kIx,
USA	USA	kA
<g/>
,	,	kIx,
Čína	Čína	k1gFnSc1
a	a	k8xC
Argentina	Argentina	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgNnSc4
dinosauří	dinosauří	k2eAgNnSc4d1
vejce	vejce	k1gNnSc4
byly	být	k5eAaImAgFnP
objeveny	objevit	k5eAaPmNgFnP
na	na	k7c6
jihu	jih	k1gInSc6
Francie	Francie	k1gFnSc2
již	již	k6eAd1
roku	rok	k1gInSc2
1859	#num#	k4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
dinosauří	dinosauří	k2eAgFnPc1d1
ale	ale	k8xC
byly	být	k5eAaImAgFnP
rozeznány	rozeznat	k5eAaPmNgFnP
až	až	k6eAd1
později	pozdě	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objevitel	objevitel	k1gMnSc1
<g/>
,	,	kIx,
kněz	kněz	k1gMnSc1
a	a	k8xC
amatérský	amatérský	k2eAgMnSc1d1
přírodovědec	přírodovědec	k1gMnSc1
Jean-Jacques	Jean-Jacques	k1gMnSc1
Pouech	Pouech	k1gInSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
považoval	považovat	k5eAaImAgMnS
za	za	k7c4
fosilie	fosilie	k1gFnPc4
ptačích	ptačí	k2eAgNnPc2d1
vajíček	vajíčko	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
Aktivní	aktivní	k2eAgFnSc1d1
rodičovská	rodičovský	k2eAgFnSc1d1
péče	péče	k1gFnSc1
již	již	k6eAd1
byla	být	k5eAaImAgFnS
prokázána	prokázat	k5eAaPmNgFnS
alespoň	alespoň	k9
u	u	k7c2
některých	některý	k3yIgInPc2
druhů	druh	k1gInPc2
dinosaurů	dinosaurus	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
Dinosauří	dinosauří	k2eAgNnPc4d1
vejce	vejce	k1gNnPc4
měla	mít	k5eAaImAgFnS
většinou	většinou	k6eAd1
pevnou	pevný	k2eAgFnSc4d1
skořápku	skořápka	k1gFnSc4
<g/>
,	,	kIx,
často	často	k6eAd1
zdobenou	zdobený	k2eAgFnSc7d1
různými	různý	k2eAgInPc7d1
vystupujícími	vystupující	k2eAgInPc7d1
útvary	útvar	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
známe	znát	k5eAaImIp1nP
dinosauří	dinosauří	k2eAgNnPc4d1
vejce	vejce	k1gNnPc4
z	z	k7c2
více	hodně	k6eAd2
než	než	k8xS
200	#num#	k4
lokalit	lokalita	k1gFnPc2
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
Některé	některý	k3yIgInPc4
objevy	objev	k1gInPc4
nicméně	nicméně	k8xC
naznačují	naznačovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
například	například	k6eAd1
u	u	k7c2
nejstarších	starý	k2eAgMnPc2d3
dinosaurů	dinosaurus	k1gMnPc2
mohla	moct	k5eAaImAgFnS
být	být	k5eAaImF
vajíčka	vajíčko	k1gNnPc4
měkká	měkký	k2eAgNnPc4d1
a	a	k8xC
kožnatá	kožnatý	k2eAgNnPc4d1
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k8xC,k8xS
u	u	k7c2
ještěrů	ještěr	k1gMnPc2
a	a	k8xC
hadů	had	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
Tato	tento	k3xDgFnSc1
interpretace	interpretace	k1gFnSc1
však	však	k9
bývá	bývat	k5eAaImIp3nS
některými	některý	k3yIgFnPc7
odbornými	odborný	k2eAgFnPc7d1
pracemi	práce	k1gFnPc7
zpochybňována	zpochybňovat	k5eAaImNgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dodnes	dodnes	k6eAd1
však	však	k9
neexistuje	existovat	k5eNaImIp3nS
relevantní	relevantní	k2eAgFnSc1d1
metoda	metoda	k1gFnSc1
k	k	k7c3
určení	určení	k1gNnSc3
pohlaví	pohlaví	k1gNnSc2
u	u	k7c2
těchto	tento	k3xDgMnPc2
vyhynulých	vyhynulý	k2eAgMnPc2d1
tvorů	tvor	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouze	pouze	k6eAd1
v	v	k7c6
případě	případ	k1gInSc6
některých	některý	k3yIgFnPc2
skupin	skupina	k1gFnPc2
na	na	k7c4
toto	tento	k3xDgNnSc4
rozlišení	rozlišení	k1gNnSc4
ukazují	ukazovat	k5eAaImIp3nP
například	například	k6eAd1
různé	různý	k2eAgInPc1d1
imponující	imponující	k2eAgInPc1d1
útvary	útvar	k1gInPc1
na	na	k7c6
lebkách	lebka	k1gFnPc6
(	(	kIx(
<g/>
hadrosauridi	hadrosaurid	k1gMnPc1
<g/>
,	,	kIx,
ceratopsidi	ceratopsid	k1gMnPc1
<g/>
,	,	kIx,
pachycefalosauridi	pachycefalosaurid	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
Existují	existovat	k5eAaImIp3nP
sice	sice	k8xC
domněnky	domněnka	k1gFnPc1
<g/>
,	,	kIx,
že	že	k8xS
například	například	k6eAd1
velcí	velký	k2eAgMnPc1d1
sauropodi	sauropod	k1gMnPc1
mohli	moct	k5eAaImAgMnP
rodit	rodit	k5eAaImF
živá	živý	k2eAgNnPc4d1
mláďata	mládě	k1gNnPc4
(	(	kIx(
<g/>
viviparie	viviparie	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ty	ten	k3xDgFnPc1
jsou	být	k5eAaImIp3nP
však	však	k9
dnes	dnes	k6eAd1
již	již	k6eAd1
prakticky	prakticky	k6eAd1
zcela	zcela	k6eAd1
vyvráceny	vyvrácen	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
O	o	k7c6
pohlavních	pohlavní	k2eAgInPc6d1
orgánech	orgán	k1gInPc6
dinosaurů	dinosaurus	k1gMnPc2
máme	mít	k5eAaImIp1nP
zatím	zatím	k6eAd1
jen	jen	k9
nepřímé	přímý	k2eNgInPc4d1
fosilní	fosilní	k2eAgInPc4d1
doklady	doklad	k1gInPc4
<g/>
,	,	kIx,
proto	proto	k8xC
si	se	k3xPyFc3
v	v	k7c6
jejich	jejich	k3xOp3gFnSc6
rekonstrukci	rekonstrukce	k1gFnSc6
musíme	muset	k5eAaImIp1nP
pomáhat	pomáhat	k5eAaImF
srovnáváním	srovnávání	k1gNnSc7
se	s	k7c7
současnými	současný	k2eAgMnPc7d1
živočichy	živočich	k1gMnPc7
(	(	kIx(
<g/>
zejména	zejména	k9
nejbližšími	blízký	k2eAgFnPc7d3
žijícími	žijící	k2eAgFnPc7d1
příbuznými	příbuzná	k1gFnPc7
dinosaurů	dinosaurus	k1gMnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
ptáky	pták	k1gMnPc4
a	a	k8xC
krokodýly	krokodýl	k1gMnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
Podoba	podoba	k1gFnSc1
kloaky	kloaka	k1gFnSc2
(	(	kIx(
<g/>
tělního	tělní	k2eAgInSc2d1
vývodu	vývod	k1gInSc2
<g/>
)	)	kIx)
u	u	k7c2
druhohorních	druhohorní	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
byla	být	k5eAaImAgFnS
poprvé	poprvé	k6eAd1
popsána	popsat	k5eAaPmNgFnS
až	až	k9
díky	díky	k7c3
výborně	výborně	k6eAd1
zachovanému	zachovaný	k2eAgInSc3d1
exempláři	exemplář	k1gInSc3
rodu	rod	k1gInSc2
Psittacosaurus	Psittacosaurus	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc1
exemplář	exemplář	k1gInSc1
dochoval	dochovat	k5eAaPmAgInS
přibližnou	přibližný	k2eAgFnSc4d1
podobu	podoba	k1gFnSc4
vnější	vnější	k2eAgFnSc2d1
části	část	k1gFnSc2
kloaky	kloaka	k1gFnSc2
s	s	k7c7
její	její	k3xOp3gFnSc7
protáhlou	protáhlý	k2eAgFnSc7d1
stavbou	stavba	k1gFnSc7
<g/>
,	,	kIx,
nezachoval	zachovat	k5eNaPmAgMnS
ale	ale	k8xC
žádné	žádný	k3yNgFnPc4
informace	informace	k1gFnPc4
o	o	k7c6
její	její	k3xOp3gFnSc6
vnitřní	vnitřní	k2eAgFnSc6d1
struktuře	struktura	k1gFnSc6
<g/>
,	,	kIx,
ani	ani	k8xC
o	o	k7c4
pohlaví	pohlaví	k1gNnSc4
jejího	její	k3xOp3gMnSc2
původce	původce	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ukazuje	ukazovat	k5eAaImIp3nS
se	se	k3xPyFc4
také	také	k9
<g/>
,	,	kIx,
že	že	k8xS
teropodní	teropodní	k2eAgMnPc1d1
dinosauři	dinosaurus	k1gMnPc1
mohli	moct	k5eAaImAgMnP
provozovat	provozovat	k5eAaImF
jakési	jakýsi	k3yIgInPc4
námluvní	námluvní	k2eAgInPc4d1
rituály	rituál	k1gInPc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
ryli	rýt	k5eAaImAgMnP
do	do	k7c2
země	zem	k1gFnSc2
drápy	dráp	k1gInPc1
na	na	k7c6
zadních	zadní	k2eAgFnPc6d1
končetinách	končetina	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stopy	stop	k1gInPc4
po	po	k7c6
těchto	tento	k3xDgFnPc6
aktivitách	aktivita	k1gFnPc6
jsou	být	k5eAaImIp3nP
dnes	dnes	k6eAd1
objevovány	objevován	k2eAgInPc1d1
na	na	k7c4
území	území	k1gNnSc4
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
a	a	k8xC
Číny	Čína	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
Výzkum	výzkum	k1gInSc1
inkubace	inkubace	k1gFnSc2
u	u	k7c2
současných	současný	k2eAgMnPc2d1
plazů	plaz	k1gMnPc2
ukazuje	ukazovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
polárních	polární	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
nebylo	být	k5eNaImAgNnS
možné	možný	k2eAgNnSc1d1
udržet	udržet	k5eAaPmF
dostatečnou	dostatečný	k2eAgFnSc4d1
teplotu	teplota	k1gFnSc4
vajíček	vajíčko	k1gNnPc2
pouze	pouze	k6eAd1
nahrnutou	nahrnutý	k2eAgFnSc7d1
zeminou	zemina	k1gFnSc7
či	či	k8xC
jiným	jiné	k1gNnSc7
<g />
.	.	kIx.
</s>
<s hack="1">
materiálem	materiál	k1gInSc7
<g/>
,	,	kIx,
ale	ale	k8xC
bylo	být	k5eAaImAgNnS
nezbytné	nezbytný	k2eAgNnSc1d1,k2eNgNnSc1d1
vajíčka	vajíčko	k1gNnPc1
zahřívat	zahřívat	k5eAaImF
vlastním	vlastní	k2eAgNnSc7d1
tělesným	tělesný	k2eAgNnSc7d1
teplem	teplo	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
Rituály	rituál	k1gInPc1
hnízdění	hnízdění	k1gNnSc2
a	a	k8xC
inkubace	inkubace	k1gFnSc2
vajec	vejce	k1gNnPc2
u	u	k7c2
mnoha	mnoho	k4c3
druhů	druh	k1gInPc2
dinosaurů	dinosaurus	k1gMnPc2
byly	být	k5eAaImAgFnP
zřejmě	zřejmě	k6eAd1
podobné	podobný	k2eAgNnSc4d1
chování	chování	k1gNnSc4
současných	současný	k2eAgMnPc2d1
hnízdících	hnízdící	k2eAgMnPc2d1
ptáků	pták	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
Dinosauři	dinosaurus	k1gMnPc1
svá	svůj	k3xOyFgNnPc4
hnízda	hnízdo	k1gNnPc4
pravděpodobně	pravděpodobně	k6eAd1
chránili	chránit	k5eAaImAgMnP
před	před	k7c7
potravními	potravní	k2eAgInPc7d1
<g />
.	.	kIx.
</s>
<s hack="1">
oportunisty	oportunista	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
na	na	k7c6
nich	on	k3xPp3gInPc6
chtěli	chtít	k5eAaImAgMnP
přiživit	přiživit	k5eAaPmF
(	(	kIx(
<g/>
ještěři	ještěr	k1gMnPc1
<g/>
,	,	kIx,
jiní	jiný	k2eAgMnPc1d1
draví	dravý	k2eAgMnPc1d1
a	a	k8xC
všežraví	všežravý	k2eAgMnPc1d1
dinosauři	dinosaurus	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
81	#num#	k4
<g/>
]	]	kIx)
Oviraptorosauři	Oviraptorosauř	k1gFnSc6
nám	my	k3xPp1nPc3
zanechali	zanechat	k5eAaPmAgMnP
také	také	k9
největší	veliký	k2eAgNnPc1d3
známá	známý	k2eAgNnPc1d1
fosilní	fosilní	k2eAgNnPc1d1
vajíčka	vajíčko	k1gNnPc1
všech	všecek	k3xTgFnPc2
dob	doba	k1gFnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
největší	veliký	k2eAgInPc1d3
exempláře	exemplář	k1gInPc1
<g />
.	.	kIx.
</s>
<s hack="1">
čínského	čínský	k2eAgInSc2d1
oodruhu	oodruh	k1gInSc2
Macroelongatoolithus	Macroelongatoolithus	k1gInSc1
xixianensis	xixianensis	k1gFnSc2
měří	měřit	k5eAaImIp3nS
až	až	k9
61	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
Naopak	naopak	k6eAd1
nejmenší	malý	k2eAgNnPc4d3
známá	známý	k2eAgNnPc4d1
vajíčka	vajíčko	k1gNnPc4
druhohorních	druhohorní	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
(	(	kIx(
<g/>
oorod	oorod	k1gInSc1
Himeoolithus	Himeoolithus	k1gInSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgNnP
objevena	objevit	k5eAaPmNgNnP
na	na	k7c6
území	území	k1gNnSc6
Japonska	Japonsko	k1gNnSc2
a	a	k8xC
jejich	jejich	k3xOp3gFnSc1
odhadovaná	odhadovaný	k2eAgFnSc1d1
hmotnost	hmotnost	k1gFnSc1
je	být	k5eAaImIp3nS
9,9	9,9	k4
gramu	gram	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
U	u	k7c2
lépe	dobře	k6eAd2
známých	známý	k2eAgInPc2d1
druhů	druh	k1gInPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
obří	obří	k2eAgInSc4d1
teropod	teropod	k1gInSc4
Tyrannosaurus	Tyrannosaurus	k1gMnSc1
rex	rex	k?
<g/>
,	,	kIx,
u	u	k7c2
něhož	jenž	k3xRgInSc2
známe	znát	k5eAaImIp1nP
několik	několik	k4yIc4
desítek	desítka	k1gFnPc2
kosterních	kosterní	k2eAgInPc2d1
exemplářů	exemplář	k1gInPc2
<g/>
,	,	kIx,
někteří	některý	k3yIgMnPc1
paleontologové	paleontolog	k1gMnPc1
předpokládají	předpokládat	k5eAaImIp3nP
pohlavní	pohlavní	k2eAgInSc4d1
dimorfismus	dimorfismus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mohutnější	mohutný	k2eAgFnPc4d2
kostry	kostra	k1gFnPc4
pak	pak	k6eAd1
obvykle	obvykle	k6eAd1
označují	označovat	k5eAaImIp3nP
za	za	k7c4
samičí	samičí	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
ale	ale	k8xC
máme	mít	k5eAaImIp1nP
k	k	k7c3
podobným	podobný	k2eAgInPc3d1
úsudkům	úsudek	k1gInPc3
příliš	příliš	k6eAd1
málo	málo	k6eAd1
fosilního	fosilní	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
ukázal	ukázat	k5eAaPmAgMnS
rozsáhlý	rozsáhlý	k2eAgInSc4d1
výzkum	výzkum	k1gInSc4
koster	kostra	k1gFnPc2
recentních	recentní	k2eAgMnPc2d1
gaviálů	gaviál	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
Rozlišit	rozlišit	k5eAaPmF
pohlaví	pohlaví	k1gNnSc4
dinosaurů	dinosaurus	k1gMnPc2
je	být	k5eAaImIp3nS
na	na	k7c6
základě	základ	k1gInSc6
samotného	samotný	k2eAgInSc2d1
fosilního	fosilní	k2eAgInSc2d1
záznamu	záznam	k1gInSc2
velmi	velmi	k6eAd1
obtížné	obtížný	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nově	nově	k6eAd1
by	by	kYmCp3nP
s	s	k7c7
tímto	tento	k3xDgInSc7
problémem	problém	k1gInSc7
mohla	moct	k5eAaImAgFnS
pomoci	pomoct	k5eAaPmF
statistická	statistický	k2eAgFnSc1d1
metoda	metoda	k1gFnSc1
za	za	k7c4
využití	využití	k1gNnSc4
přesného	přesný	k2eAgNnSc2d1
měření	měření	k1gNnSc2
a	a	k8xC
počítačových	počítačový	k2eAgInPc2d1
programů	program	k1gInPc2
pro	pro	k7c4
analýzu	analýza	k1gFnSc4
morfometrie	morfometrie	k1gFnSc2
většího	veliký	k2eAgInSc2d2
vzorku	vzorek	k1gInSc2
jedinců	jedinec	k1gMnPc2
v	v	k7c6
populacích	populace	k1gFnPc6
různých	různý	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
dinosaurů	dinosaurus	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dinosauři	dinosaurus	k1gMnPc1
se	se	k3xPyFc4
v	v	k7c6
mnoha	mnoho	k4c6
ohledech	ohled	k1gInPc6
podobali	podobat	k5eAaImAgMnP
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
reprodukci	reprodukce	k1gFnSc6
a	a	k8xC
hnízdním	hnízdní	k2eAgNnSc6d1
chování	chování	k1gNnSc6
současným	současný	k2eAgMnPc3d1
ptákům	pták	k1gMnPc3
<g/>
,	,	kIx,
v	v	k7c6
některých	některý	k3yIgInPc6
ohledech	ohled	k1gInPc6
ale	ale	k8xC
zase	zase	k9
nikoliv	nikoliv	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gNnSc7
reprodukční	reprodukční	k2eAgNnSc1d1
chování	chování	k1gNnSc1
bylo	být	k5eAaImAgNnS
komplexní	komplexní	k2eAgNnSc1d1
a	a	k8xC
zahrnovalo	zahrnovat	k5eAaImAgNnS
odvozené	odvozený	k2eAgInPc4d1
i	i	k8xC
primitivní	primitivní	k2eAgInPc4d1
prvky	prvek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
dokládají	dokládat	k5eAaImIp3nP
unikátní	unikátní	k2eAgInPc1d1
nálezy	nález	k1gInPc1
fosilních	fosilní	k2eAgFnPc2d1
koster	kostra	k1gFnPc2
dospělců	dospělec	k1gMnPc2
i	i	k8xC
embryí	embryo	k1gNnPc2
z	z	k7c2
hnízdišť	hnízdiště	k1gNnPc2
především	především	k9
z	z	k7c2
území	území	k1gNnSc2
současné	současný	k2eAgFnSc2d1
Číny	Čína	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sexuální	sexuální	k2eAgInSc1d1
dimorfismus	dimorfismus	k1gInSc1
(	(	kIx(
<g/>
pohlavní	pohlavní	k2eAgFnSc1d1
dvojtvárnost	dvojtvárnost	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tedy	tedy	k8xC
rozdílný	rozdílný	k2eAgInSc1d1
vzhled	vzhled	k1gInSc1
a	a	k8xC
velikost	velikost	k1gFnSc1
obou	dva	k4xCgFnPc2
pohlaví	pohlaví	k1gNnSc2
stejného	stejný	k2eAgInSc2d1
druhu	druh	k1gInSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
nepochybně	pochybně	k6eNd1
vyskytoval	vyskytovat	k5eAaImAgInS
i	i	k9
u	u	k7c2
dinosaurů	dinosaurus	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnPc7
míra	míra	k1gFnSc1
a	a	k8xC
význam	význam	k1gInSc1
jsou	být	k5eAaImIp3nP
ale	ale	k8xC
stále	stále	k6eAd1
předmětem	předmět	k1gInSc7
intenzivních	intenzivní	k2eAgFnPc2d1
debat	debata	k1gFnPc2
mezi	mezi	k7c7
paleontology	paleontolog	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgNnSc7d1
vodítkem	vodítko	k1gNnSc7
jsou	být	k5eAaImIp3nP
přitom	přitom	k6eAd1
srovnávací	srovnávací	k2eAgFnPc1d1
morfologické	morfologický	k2eAgFnPc1d1
studie	studie	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
ale	ale	k8xC
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
poněkud	poněkud	k6eAd1
zavádějící	zavádějící	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Fyziologie	fyziologie	k1gFnSc1
dinosaurů	dinosaurus	k1gMnPc2
</s>
<s>
Již	již	k6eAd1
od	od	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
dinosauři	dinosaurus	k1gMnPc1
měli	mít	k5eAaImAgMnP
rychlý	rychlý	k2eAgInSc4d1
metabolismus	metabolismus	k1gInSc4
<g/>
,	,	kIx,
podobný	podobný	k2eAgInSc4d1
metabolismu	metabolismus	k1gInSc3
dnešních	dnešní	k2eAgMnPc2d1
ptáků	pták	k1gMnPc2
<g/>
,	,	kIx,
nikoliv	nikoliv	k9
současných	současný	k2eAgMnPc2d1
plazů	plaz	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
také	také	k9
vysoce	vysoce	k6eAd1
pravděpodobné	pravděpodobný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
minimálně	minimálně	k6eAd1
někteří	některý	k3yIgMnPc1
(	(	kIx(
<g/>
pravděpodobně	pravděpodobně	k6eAd1
všichni	všechen	k3xTgMnPc1
teropodi	teropod	k1gMnPc1
<g/>
)	)	kIx)
dinosauři	dinosaurus	k1gMnPc1
byli	být	k5eAaImAgMnP
teplokrevní	teplokrevný	k2eAgMnPc1d1
a	a	k8xC
jejich	jejich	k3xOp3gFnSc1
tělesná	tělesný	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
tak	tak	k6eAd1
nebyla	být	k5eNaImAgFnS
závislá	závislý	k2eAgFnSc1d1
na	na	k7c6
teplotě	teplota	k1gFnSc6
okolního	okolní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
(	(	kIx(
<g/>
podobně	podobně	k6eAd1
jako	jako	k8xC,k8xS
u	u	k7c2
dnešních	dnešní	k2eAgMnPc2d1
ptáků	pták	k1gMnPc2
a	a	k8xC
savců	savec	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
89	#num#	k4
<g/>
]	]	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
[	[	kIx(
<g/>
90	#num#	k4
<g/>
]	]	kIx)
To	ten	k3xDgNnSc1
odpovídá	odpovídat	k5eAaImIp3nS
novějšímu	nový	k2eAgNnSc3d2
pojetí	pojetí	k1gNnSc3
dinosaurů	dinosaurus	k1gMnPc2
coby	coby	k?
rychlých	rychlý	k2eAgMnPc2d1
a	a	k8xC
aktivních	aktivní	k2eAgMnPc2d1
tvorů	tvor	k1gMnPc2
<g/>
,	,	kIx,
žijících	žijící	k2eAgFnPc2d1
například	například	k6eAd1
i	i	k9
v	v	k7c6
polárních	polární	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
a	a	k8xC
migrujících	migrující	k2eAgMnPc2d1
na	na	k7c6
velké	velký	k2eAgFnSc6d1
vzdálenosti	vzdálenost	k1gFnSc6
(	(	kIx(
<g/>
pro	pro	k7c4
předpokládané	předpokládaný	k2eAgFnPc4d1
migrace	migrace	k1gFnPc4
na	na	k7c4
vzdálenost	vzdálenost	k1gFnSc4
až	až	k6eAd1
několika	několik	k4yIc2
tisíc	tisíc	k4xCgInPc2
kilometrů	kilometr	k1gInPc2
však	však	k9
dosud	dosud	k6eAd1
chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
přímé	přímý	k2eAgInPc4d1
fosilní	fosilní	k2eAgInPc4d1
důkazy	důkaz	k1gInPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
91	#num#	k4
<g/>
]	]	kIx)
O	o	k7c6
potenciální	potenciální	k2eAgFnSc6d1
teplokrevnosti	teplokrevnost	k1gFnSc6
teropodních	teropodní	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
by	by	kYmCp3nS
mohl	moct	k5eAaImAgMnS
svědčit	svědčit	k5eAaImF
také	také	k9
poměr	poměr	k1gInSc4
počtu	počet	k1gInSc2
dravých	dravý	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
a	a	k8xC
jejich	jejich	k3xOp3gFnSc2
kořisti	kořist	k1gFnSc2
na	na	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
paleontologických	paleontologický	k2eAgFnPc6d1
lokalitách	lokalita	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
zhruba	zhruba	k6eAd1
kolem	kolem	k7c2
2	#num#	k4
až	až	k9
5	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
odpovídá	odpovídat	k5eAaImIp3nS
mnohem	mnohem	k6eAd1
lépe	dobře	k6eAd2
teplokrevným	teplokrevný	k2eAgMnPc3d1
savcům	savec	k1gMnPc3
než	než	k8xS
studenokrevným	studenokrevný	k2eAgInPc3d1
plazům	plaz	k1gInPc3
(	(	kIx(
<g/>
kde	kde	k6eAd1
je	být	k5eAaImIp3nS
poměr	poměr	k1gInSc1
mnohem	mnohem	k6eAd1
vyšší	vysoký	k2eAgMnSc1d2
a	a	k8xC
dosahuje	dosahovat	k5eAaImIp3nS
zhruba	zhruba	k6eAd1
15	#num#	k4
<g/>
–	–	k?
<g/>
20	#num#	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
Objevy	objev	k1gInPc1
fosilií	fosilie	k1gFnPc2
dinosaurů	dinosaurus	k1gMnPc2
v	v	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
oblastech	oblast	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
v	v	k7c6
době	doba	k1gFnSc6
druhohor	druhohory	k1gFnPc2
ležely	ležet	k5eAaImAgFnP
za	za	k7c7
polárním	polární	k2eAgInSc7d1
kruhem	kruh	k1gInSc7
<g/>
,	,	kIx,
rovněž	rovněž	k9
poukazují	poukazovat	k5eAaImIp3nP
na	na	k7c4
jejich	jejich	k3xOp3gFnSc4
pravděpodobnou	pravděpodobný	k2eAgFnSc4d1
teplokrevnost	teplokrevnost	k1gFnSc4
a	a	k8xC
vysoký	vysoký	k2eAgInSc4d1
stupeň	stupeň	k1gInSc4
metabolické	metabolický	k2eAgFnSc2d1
výměny	výměna	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
se	se	k3xPyFc4
však	však	k9
objevila	objevit	k5eAaPmAgFnS
studie	studie	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
v	v	k7c6
dinosaurech	dinosaurus	k1gMnPc6
spatřuje	spatřovat	k5eAaImIp3nS
spíše	spíše	k9
tzv.	tzv.	kA
mezotermy	mezoterm	k1gInPc4
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
94	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
95	#num#	k4
<g/>
]	]	kIx)
Otázka	otázka	k1gFnSc1
endotermie	endotermie	k1gFnSc2
dinosaurů	dinosaurus	k1gMnPc2
nicméně	nicméně	k8xC
zůstává	zůstávat	k5eAaImIp3nS
nevyřešena	vyřešit	k5eNaPmNgFnS
i	i	k9
ve	v	k7c6
druhém	druhý	k4xOgNnSc6
desetiletí	desetiletí	k1gNnSc6
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
96	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
některých	některý	k3yIgNnPc2
studií	studio	k1gNnPc2
se	se	k3xPyFc4
teplokrevnost	teplokrevnost	k1gFnSc1
obješvila	obješvit	k5eAaPmAgFnS,k5eAaImAgFnS
spolu	spolu	k6eAd1
s	s	k7c7
miniaturizací	miniaturizace	k1gFnSc7
(	(	kIx(
<g/>
zmenšováním	zmenšování	k1gNnSc7
tělesných	tělesný	k2eAgInPc2d1
rozměrů	rozměr	k1gInPc2
<g/>
)	)	kIx)
u	u	k7c2
teropodních	teropodní	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
v	v	k7c6
průběhu	průběh	k1gInSc6
spodní	spodní	k2eAgFnSc2d1
až	až	k8xS
střední	střední	k2eAgFnSc2d1
jury	jura	k1gFnSc2
(	(	kIx(
<g/>
asi	asi	k9
před	před	k7c7
180	#num#	k4
až	až	k8xS
170	#num#	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
<g/>
)	)	kIx)
a	a	k8xC
ptáci	pták	k1gMnPc1
ji	on	k3xPp3gFnSc4
získali	získat	k5eAaPmAgMnP
coby	coby	k?
odvozený	odvozený	k2eAgInSc1d1
fyziologický	fyziologický	k2eAgInSc1d1
znak	znak	k1gInSc1
<g/>
,	,	kIx,
zděděný	zděděný	k2eAgInSc1d1
přímo	přímo	k6eAd1
od	od	k7c2
svých	svůj	k3xOyFgInPc2
dinosauřích	dinosauří	k2eAgInPc2d1
předků	předek	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jisté	jistý	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
dinosauři	dinosaurus	k1gMnPc1
tvořili	tvořit	k5eAaImAgMnP
v	v	k7c6
období	období	k1gNnSc6
jury	jura	k1gFnSc2
a	a	k8xC
křídy	křída	k1gFnSc2
pevninskou	pevninský	k2eAgFnSc4d1
megafaunu	megafauna	k1gFnSc4
<g/>
,	,	kIx,
charakteristickou	charakteristický	k2eAgFnSc7d1
fyziologií	fyziologie	k1gFnSc7
odlišnou	odlišný	k2eAgFnSc7d1
od	od	k7c2
fyziologie	fyziologie	k1gFnSc2
prakticky	prakticky	k6eAd1
všech	všecek	k3xTgMnPc2
současných	současný	k2eAgMnPc2d1
živočichů	živočich	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
98	#num#	k4
<g/>
]	]	kIx)
Mnozí	mnohý	k2eAgMnPc1d1
dinosauři	dinosaurus	k1gMnPc1
měli	mít	k5eAaImAgMnP
systém	systém	k1gInSc4
tzv.	tzv.	kA
vzdušných	vzdušný	k2eAgInPc2d1
vaků	vak	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
se	se	k3xPyFc4
jim	on	k3xPp3gMnPc3
rozšiřovaly	rozšiřovat	k5eAaImAgFnP
až	až	k9
do	do	k7c2
kostních	kostní	k2eAgFnPc2d1
dutin	dutina	k1gFnPc2
a	a	k8xC
umožňovaly	umožňovat	k5eAaImAgFnP
jim	on	k3xPp3gMnPc3
efektivnější	efektivní	k2eAgNnSc1d2
dýchání	dýchání	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
respirační	respirační	k2eAgInSc1d1
systém	systém	k1gInSc1
po	po	k7c6
teropodních	teropodní	k2eAgMnPc6d1
dinosaurech	dinosaurus	k1gMnPc6
zdědili	zdědit	k5eAaPmAgMnP
i	i	k9
jejich	jejich	k3xOp3gMnPc1
potomci	potomek	k1gMnPc1
ptáci	pták	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
99	#num#	k4
<g/>
]	]	kIx)
Jazyk	jazyk	k1gInSc1
dinosaurů	dinosaurus	k1gMnPc2
byl	být	k5eAaImAgInS
fixovaný	fixovaný	k2eAgMnSc1d1
k	k	k7c3
čelistnímu	čelistní	k2eAgInSc3d1
dnu	den	k1gInSc3
a	a	k8xC
nevyčníval	vyčnívat	k5eNaImAgMnS
z	z	k7c2
ústní	ústní	k2eAgFnSc2d1
dutiny	dutina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
100	#num#	k4
<g/>
]	]	kIx)
Dýchací	dýchací	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
dinosaurů	dinosaurus	k1gMnPc2
byla	být	k5eAaImAgFnS
v	v	k7c6
každém	každý	k3xTgInSc6
případě	případ	k1gInSc6
vysoce	vysoce	k6eAd1
výkonná	výkonný	k2eAgFnSc1d1
a	a	k8xC
zřejmě	zřejmě	k6eAd1
byla	být	k5eAaImAgFnS
i	i	k9
jedním	jeden	k4xCgInSc7
z	z	k7c2
hlavních	hlavní	k2eAgInPc2d1
faktorů	faktor	k1gInPc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
jim	on	k3xPp3gMnPc3
zajistil	zajistit	k5eAaPmAgInS
jejich	jejich	k3xOp3gInSc1
dlouhodobý	dlouhodobý	k2eAgInSc1d1
evoluční	evoluční	k2eAgInSc1d1
úspěch	úspěch	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
101	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
102	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Velcí	velký	k2eAgMnPc1d1
dinosauři	dinosaurus	k1gMnPc1
měli	mít	k5eAaImAgMnP
nepochybně	pochybně	k6eNd1
problém	problém	k1gInSc4
s	s	k7c7
udržováním	udržování	k1gNnSc7
stále	stále	k6eAd1
tělesné	tělesný	k2eAgFnSc2d1
teploty	teplota	k1gFnSc2
<g/>
,	,	kIx,
zejména	zejména	k9
pak	pak	k6eAd1
s	s	k7c7
přehříváním	přehřívání	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
jejich	jejich	k3xOp3gInSc1
mozek	mozek	k1gInSc1
fungoval	fungovat	k5eAaImAgInS
ve	v	k7c6
stálé	stálý	k2eAgFnSc6d1
ideální	ideální	k2eAgFnSc6d1
teplotě	teplota	k1gFnSc6
<g/>
,	,	kIx,
museli	muset	k5eAaImAgMnP
být	být	k5eAaImF
tito	tento	k3xDgMnPc1
dinosauři	dinosaurus	k1gMnPc1
vybaveni	vybavit	k5eAaPmNgMnP
neurovaskulárním	urovaskulární	k2eNgInSc7d1
systémem	systém	k1gInSc7
cévních	cévní	k2eAgInPc2d1
svazků	svazek	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
jejich	jejich	k3xOp3gInPc1
mozky	mozek	k1gInPc1
ochlazovaly	ochlazovat	k5eAaImAgInP
a	a	k8xC
udržovaly	udržovat	k5eAaImAgInP
dlouhodobě	dlouhodobě	k6eAd1
v	v	k7c6
optimální	optimální	k2eAgFnSc6d1
teplotě	teplota	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
platilo	platit	k5eAaImAgNnS
přinejmenším	přinejmenším	k6eAd1
pro	pro	k7c4
obří	obří	k2eAgMnPc4d1
sauropody	sauropod	k1gMnPc4
<g/>
,	,	kIx,
teropody	teropod	k1gMnPc4
i	i	k8xC
ornitopody	ornitopod	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
103	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Víme	vědět	k5eAaImIp1nP
také	také	k9
<g/>
,	,	kIx,
že	že	k8xS
neptačí	ptačí	k2eNgMnPc1d1
dinosauři	dinosaurus	k1gMnPc1
rostli	růst	k5eAaImAgMnP
relativně	relativně	k6eAd1
rychle	rychle	k6eAd1
(	(	kIx(
<g/>
dospívali	dospívat	k5eAaImAgMnP
obvykle	obvykle	k6eAd1
do	do	k7c2
10	#num#	k4
<g/>
.	.	kIx.
roku	rok	k1gInSc2
života	život	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
nedožívali	dožívat	k5eNaImAgMnP
se	se	k3xPyFc4
příliš	příliš	k6eAd1
vysokého	vysoký	k2eAgInSc2d1
věku	věk	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
104	#num#	k4
<g/>
]	]	kIx)
Četné	četný	k2eAgInPc4d1
objevy	objev	k1gInPc4
patologií	patologie	k1gFnPc2
na	na	k7c6
kostech	kost	k1gFnPc6
ukazují	ukazovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
mnozí	mnohý	k2eAgMnPc1d1
dinosauři	dinosaurus	k1gMnPc1
se	se	k3xPyFc4
utkávali	utkávat	k5eAaImAgMnP
v	v	k7c6
agresivních	agresivní	k2eAgInPc6d1
vnitrodruhových	vnitrodruhový	k2eAgInPc6d1
soubojích	souboj	k1gInPc6
(	(	kIx(
<g/>
například	například	k6eAd1
rohatí	rohatý	k2eAgMnPc1d1
dinosauři	dinosaurus	k1gMnPc1
<g/>
,	,	kIx,
tyranosauridi	tyranosaurid	k1gMnPc1
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
105	#num#	k4
<g/>
]	]	kIx)
Obecně	obecně	k6eAd1
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
říci	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
dinosauři	dinosaurus	k1gMnPc1
velmi	velmi	k6eAd1
rychle	rychle	k6eAd1
rostli	růst	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
některých	některý	k3yIgMnPc2
hadrosauridů	hadrosaurid	k1gInPc2
činil	činit	k5eAaImAgInS
rozdíl	rozdíl	k1gInSc1
v	v	k7c6
hmotnosti	hmotnost	k1gFnSc6
u	u	k7c2
čerstvě	čerstvě	k6eAd1
vylíhlého	vylíhlý	k2eAgNnSc2d1
mláděte	mládě	k1gNnSc2
a	a	k8xC
plně	plně	k6eAd1
dorostlého	dorostlý	k2eAgMnSc2d1
dospělce	dospělec	k1gMnSc2
asi	asi	k9
1	#num#	k4
ku	k	k7c3
16	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
malí	malý	k2eAgMnPc1d1
dinosauři	dinosaurus	k1gMnPc1
však	však	k9
díky	díky	k7c3
rapidnímu	rapidní	k2eAgInSc3d1
růstu	růst	k1gInSc3
rychle	rychle	k6eAd1
tento	tento	k3xDgInSc4
rozdíl	rozdíl	k1gInSc4
doháněli	dohánět	k5eAaImAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
106	#num#	k4
<g/>
]	]	kIx)
Vnitřní	vnitřní	k2eAgFnSc1d1
stavba	stavba	k1gFnSc1
kostí	kost	k1gFnPc2
dinosaurů	dinosaurus	k1gMnPc2
se	se	k3xPyFc4
nicméně	nicméně	k8xC
lišila	lišit	k5eAaImAgFnS
od	od	k7c2
stavby	stavba	k1gFnSc2
kostí	kost	k1gFnPc2
savců	savec	k1gMnPc2
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
povrchové	povrchový	k2eAgFnPc1d1
jsou	být	k5eAaImIp3nP
si	se	k3xPyFc3
relativně	relativně	k6eAd1
značně	značně	k6eAd1
podobné	podobný	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
107	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Druhohorní	druhohorní	k2eAgMnPc1d1
dinosauři	dinosaurus	k1gMnPc1
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
dožívali	dožívat	k5eAaImAgMnP
spíše	spíše	k9
nižšího	nízký	k2eAgInSc2d2
věku	věk	k1gInSc2
<g/>
,	,	kIx,
například	například	k6eAd1
populární	populární	k2eAgInSc4d1
teropod	teropod	k1gInSc4
druhu	druh	k1gInSc2
Tyrannosaurus	Tyrannosaurus	k1gInSc1
rex	rex	k?
pravděpodobně	pravděpodobně	k6eAd1
jen	jen	k9
vzácně	vzácně	k6eAd1
překračoval	překračovat	k5eAaImAgInS
30	#num#	k4
<g/>
.	.	kIx.
rok	rok	k1gInSc1
života	život	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
108	#num#	k4
<g/>
]	]	kIx)
Nejstarším	starý	k2eAgInSc7d3
doložitelným	doložitelný	k2eAgInSc7d1
věkem	věk	k1gInSc7
dinosaura	dinosaurus	k1gMnSc2
je	být	k5eAaImIp3nS
55	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
zjištěných	zjištěný	k2eAgInPc2d1
díky	díky	k7c3
počtu	počet	k1gInSc2
a	a	k8xC
tvaru	tvar	k1gInSc2
přírůstků	přírůstek	k1gInPc2
na	na	k7c6
průřezu	průřez	k1gInSc6
fosilních	fosilní	k2eAgFnPc2d1
kostí	kost	k1gFnPc2
afrického	africký	k2eAgMnSc2d1
sauropoda	sauropod	k1gMnSc2
druhu	druh	k1gInSc2
Janenschia	Janenschius	k1gMnSc4
robusta	robust	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgMnSc1
pozdně	pozdně	k6eAd1
jurský	jurský	k2eAgMnSc1d1
dlouhokrký	dlouhokrký	k2eAgMnSc1d1
dinosaurus	dinosaurus	k1gMnSc1
zahynul	zahynout	k5eAaPmAgMnS
přibližně	přibližně	k6eAd1
v	v	k7c6
55	#num#	k4
letech	léto	k1gNnPc6
věku	věk	k1gInSc2
<g/>
,	,	kIx,
u	u	k7c2
několika	několik	k4yIc2
dalších	další	k2eAgMnPc2d1
sauropodů	sauropod	k1gMnPc2
(	(	kIx(
<g/>
Mamenchisaurus	Mamenchisaurus	k1gMnSc1
<g/>
,	,	kIx,
Apatosaurus	Apatosaurus	k1gMnSc1
<g/>
,	,	kIx,
Camarasaurus	Camarasaurus	k1gMnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
zase	zase	k9
zjištěn	zjištěn	k2eAgInSc1d1
věk	věk	k1gInSc1
přes	přes	k7c4
40	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
109	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Četné	četný	k2eAgFnPc1d1
známky	známka	k1gFnPc1
zranění	zranění	k1gNnPc2
a	a	k8xC
nemocí	nemoc	k1gFnPc2
na	na	k7c6
kostech	kost	k1gFnPc6
některých	některý	k3yIgMnPc2
dinosaurů	dinosaurus	k1gMnPc2
svědčí	svědčit	k5eAaImIp3nS
také	také	k9
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
tito	tento	k3xDgMnPc1
pravěcí	pravěký	k2eAgMnPc1d1
tvorové	tvor	k1gMnPc1
trpěli	trpět	k5eAaImAgMnP
četnými	četný	k2eAgFnPc7d1
chorobami	choroba	k1gFnPc7
včetně	včetně	k7c2
nádorových	nádorový	k2eAgNnPc2d1
onemocnění	onemocnění	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
110	#num#	k4
<g/>
]	]	kIx)
Byly	být	k5eAaImAgInP
objeveny	objevit	k5eAaPmNgInP
fosilní	fosilní	k2eAgInPc1d1
otisky	otisk	k1gInPc1
těl	tělo	k1gNnPc2
parazitů	parazit	k1gMnPc2
<g/>
,	,	kIx,
mezi	mezi	k7c4
jejichž	jejichž	k3xOyRp3gMnPc4
hostitele	hostitel	k1gMnPc4
nejspíš	nejspíš	k9
patřili	patřit	k5eAaImAgMnP
ptakoještěři	ptakoještěr	k1gMnPc1
a	a	k8xC
opeření	opeřený	k2eAgMnPc1d1
dinosauři	dinosaurus	k1gMnPc1
(	(	kIx(
<g/>
například	například	k6eAd1
velké	velký	k2eAgFnPc1d1
blechy	blecha	k1gFnPc1
<g/>
,	,	kIx,
vši	veš	k1gFnPc1
a	a	k8xC
klíšťata	klíště	k1gNnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
111	#num#	k4
<g/>
]	]	kIx)
Velkými	velký	k2eAgMnPc7d1
parazity	parazit	k1gMnPc7
dinosaurů	dinosaurus	k1gMnPc2
byly	být	k5eAaImAgFnP
například	například	k6eAd1
jurské	jurský	k2eAgFnPc1d1
a	a	k8xC
křídové	křídový	k2eAgFnPc1d1
blechy	blecha	k1gFnPc1
rodu	rod	k1gInSc2
Pseudopulex	Pseudopulex	k1gInSc1
<g/>
,	,	kIx,
dosahující	dosahující	k2eAgFnSc2d1
délky	délka	k1gFnSc2
i	i	k9
přes	přes	k7c4
2	#num#	k4
centimetry	centimetr	k1gInPc4
(	(	kIx(
<g/>
zhruba	zhruba	k6eAd1
desetinásobek	desetinásobek	k1gInSc4
velikosti	velikost	k1gFnSc2
dnešních	dnešní	k2eAgFnPc2d1
blech	blecha	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
týče	týkat	k5eAaImIp3nS
rychlosti	rychlost	k1gFnSc2
pohybu	pohyb	k1gInSc2
<g/>
,	,	kIx,
většina	většina	k1gFnSc1
velkých	velký	k2eAgMnPc2d1
čtyřnohých	čtyřnohý	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
zřejmě	zřejmě	k6eAd1
vysokých	vysoký	k2eAgFnPc2d1
rychlostí	rychlost	k1gFnPc2
v	v	k7c6
běhu	běh	k1gInSc6
nedosahovala	dosahovat	k5eNaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
menší	malý	k2eAgInPc4d2
a	a	k8xC
středně	středně	k6eAd1
velcí	velký	k2eAgMnPc1d1
dinosauři	dinosaurus	k1gMnPc1
však	však	k9
byli	být	k5eAaImAgMnP
schopni	schopen	k2eAgMnPc1d1
dosáhnout	dosáhnout	k5eAaPmF
pozoruhodné	pozoruhodný	k2eAgFnPc4d1
rychlosti	rychlost	k1gFnPc4
kolem	kolem	k7c2
40	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h	h	k?
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
ukázaly	ukázat	k5eAaPmAgInP
zkamenělé	zkamenělý	k2eAgInPc1d1
otisky	otisk	k1gInPc1
sérií	série	k1gFnPc2
jejich	jejich	k3xOp3gFnPc2
stop	stopa	k1gFnPc2
<g/>
[	[	kIx(
<g/>
112	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
série	série	k1gFnSc1
fosilních	fosilní	k2eAgInPc2d1
otisků	otisk	k1gInPc2
stop	stopa	k1gFnPc2
z	z	k7c2
pozdní	pozdní	k2eAgFnSc2d1
křídy	křída	k1gFnSc2
Polska	Polsko	k1gNnSc2
dokonce	dokonce	k9
poukazuje	poukazovat	k5eAaImIp3nS
na	na	k7c4
menšího	malý	k2eAgMnSc4d2
dromeosaurida	dromeosaurid	k1gMnSc4
<g/>
,	,	kIx,
běžícího	běžící	k2eAgMnSc4d1
rychlostí	rychlost	k1gFnSc7
kolem	kolem	k7c2
50	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
<g/>
[	[	kIx(
<g/>
113	#num#	k4
<g/>
]	]	kIx)
Vůbec	vůbec	k9
nejrychlejší	rychlý	k2eAgMnPc1d3
byli	být	k5eAaImAgMnP
zřejmě	zřejmě	k6eAd1
tzv.	tzv.	kA
pštrosí	pštrosí	k2eAgMnPc1d1
dinosauři	dinosaurus	k1gMnPc1
(	(	kIx(
<g/>
ornitomimosauři	ornitomimosaur	k1gMnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
schopní	schopný	k2eAgMnPc1d1
v	v	k7c6
běhu	běh	k1gInSc6
dosáhnout	dosáhnout	k5eAaPmF
rychlosti	rychlost	k1gFnPc4
kolem	kolem	k7c2
60	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
Díky	díky	k7c3
fosilním	fosilní	k2eAgFnPc3d1
stopám	stopa	k1gFnPc3
také	také	k9
přibližně	přibližně	k6eAd1
víme	vědět	k5eAaImIp1nP
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
někteří	některý	k3yIgMnPc1
dinosauři	dinosaurus	k1gMnPc1
dokázali	dokázat	k5eAaPmAgMnP
zvedat	zvedat	k5eAaImF
ze	z	k7c2
země	zem	k1gFnSc2
a	a	k8xC
pohybovat	pohybovat	k5eAaImF
se	se	k3xPyFc4
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
životním	životní	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
114	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
O	o	k7c6
anatomii	anatomie	k1gFnSc6
dinosaurů	dinosaurus	k1gMnPc2
se	se	k3xPyFc4
dozvídáme	dozvídat	k5eAaImIp1nP
jen	jen	k9
nepřímo	přímo	k6eNd1
<g/>
,	,	kIx,
z	z	k7c2
výzkumu	výzkum	k1gInSc2
jejich	jejich	k3xOp3gFnPc2
koster	kostra	k1gFnPc2
a	a	k8xC
otisků	otisk	k1gInPc2
měkkých	měkký	k2eAgFnPc2d1
tkání	tkáň	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obrovské	obrovský	k2eAgFnPc4d1
byly	být	k5eAaImAgInP
například	například	k6eAd1
vnitřní	vnitřní	k2eAgInPc4d1
tělesné	tělesný	k2eAgInPc4d1
orgány	orgán	k1gInPc4
obřích	obří	k2eAgInPc2d1
sauropodů	sauropod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
odhadů	odhad	k1gInPc2
vážilo	vážit	k5eAaImAgNnS
například	například	k6eAd1
jen	jen	k6eAd1
srdce	srdce	k1gNnSc4
brachiosaurida	brachiosaurida	k1gFnSc1
rodu	rod	k1gInSc2
Giraffatitan	Giraffatitan	k1gInSc4
asi	asi	k9
200	#num#	k4
kilogramů	kilogram	k1gInPc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
jeho	jeho	k3xOp3gInSc1
žaludek	žaludek	k1gInSc1
mohl	moct	k5eAaImAgInS
být	být	k5eAaImF
těžký	těžký	k2eAgInSc1d1
až	až	k9
2500	#num#	k4
kilogramů	kilogram	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srdce	srdce	k1gNnSc1
pracovalo	pracovat	k5eAaImAgNnS
tempem	tempo	k1gNnSc7
asi	asi	k9
14	#num#	k4
až	až	k9
17	#num#	k4
úderů	úder	k1gInPc2
za	za	k7c4
minutu	minuta	k1gFnSc4
a	a	k8xC
dinosaurus	dinosaurus	k1gMnSc1
se	se	k3xPyFc4
nadechl	nadechnout	k5eAaPmAgMnS
přibližně	přibližně	k6eAd1
3,5	3,5	k4
<g/>
krát	krát	k6eAd1
za	za	k7c4
minutu	minuta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jeho	jeho	k3xOp3gNnSc6
těle	tělo	k1gNnSc6
bylo	být	k5eAaImAgNnS
podle	podle	k7c2
odhadů	odhad	k1gInPc2
také	také	k9
asi	asi	k9
3000	#num#	k4
litrů	litr	k1gInPc2
krve	krev	k1gFnSc2
<g/>
,	,	kIx,
tedy	tedy	k9
asi	asi	k9
600	#num#	k4
<g/>
krát	krát	k6eAd1
více	hodně	k6eAd2
než	než	k8xS
u	u	k7c2
dospělého	dospělý	k2eAgMnSc2d1
člověka	člověk	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
115	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Některé	některý	k3yIgFnPc1
vědecké	vědecký	k2eAgFnPc1d1
studie	studie	k1gFnPc1
se	se	k3xPyFc4
zabývají	zabývat	k5eAaImIp3nP
také	také	k9
zajímavými	zajímavý	k2eAgFnPc7d1
a	a	k8xC
obtížně	obtížně	k6eAd1
interpretovatelnými	interpretovatelný	k2eAgNnPc7d1
tématy	téma	k1gNnPc7
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
odolnost	odolnost	k1gFnSc4
různých	různý	k2eAgInPc2d1
druhů	druh	k1gInPc2
dinosaurů	dinosaurus	k1gMnPc2
vůči	vůči	k7c3
bolesti	bolest	k1gFnSc3
<g/>
,	,	kIx,
reakce	reakce	k1gFnPc1
na	na	k7c4
ni	on	k3xPp3gFnSc4
a	a	k8xC
chování	chování	k1gNnSc4
spojené	spojený	k2eAgNnSc4d1
s	s	k7c7
předcházením	předcházení	k1gNnSc7
zranění	zranění	k1gNnSc2
či	či	k8xC
s	s	k7c7
reakcemi	reakce	k1gFnPc7
na	na	k7c4
utržené	utržený	k2eAgNnSc4d1
zranění	zranění	k1gNnSc4
(	(	kIx(
<g/>
v	v	k7c6
rámci	rámec	k1gInSc6
jedinců	jedinec	k1gMnPc2
i	i	k8xC
společenstvev	společenstvva	k1gFnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
dinosauřích	dinosauří	k2eAgFnPc2d1
hnízdních	hnízdní	k2eAgFnPc2d1
kolonií	kolonie	k1gFnPc2
<g/>
,	,	kIx,
stád	stádo	k1gNnPc2
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
116	#num#	k4
<g/>
]	]	kIx)
Diskutovanou	diskutovaný	k2eAgFnSc7d1
otázkou	otázka	k1gFnSc7
je	být	k5eAaImIp3nS
také	také	k9
zvuk	zvuk	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc4,k3yIgInSc4,k3yQgInSc4
dinosauři	dinosaurus	k1gMnPc1
vydávali	vydávat	k5eAaPmAgMnP,k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rekonstrukce	rekonstrukce	k1gFnSc1
hlasu	hlas	k1gInSc2
dinosaurů	dinosaurus	k1gMnPc2
byla	být	k5eAaImAgFnS
provedena	provést	k5eAaPmNgFnS
například	například	k6eAd1
u	u	k7c2
kachnozobého	kachnozobý	k2eAgMnSc2d1
dinosaura	dinosaurus	k1gMnSc2
rodu	rod	k1gInSc2
Parasaurolophus	Parasaurolophus	k1gMnSc1
<g/>
[	[	kIx(
<g/>
117	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
teropoda	teropoda	k1gMnSc1
rodu	rod	k1gInSc2
Tyrannosaurus	Tyrannosaurus	k1gInSc1
(	(	kIx(
<g/>
ovšem	ovšem	k9
pouze	pouze	k6eAd1
hypotetická	hypotetický	k2eAgFnSc1d1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
118	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Termofyziologii	Termofyziologie	k1gFnSc4
a	a	k8xC
metabolickou	metabolický	k2eAgFnSc4d1
termoregulaci	termoregulace	k1gFnSc4
u	u	k7c2
druhohorních	druhohorní	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
zjišťují	zjišťovat	k5eAaImIp3nP
vědci	vědec	k1gMnPc1
i	i	k9
s	s	k7c7
pomocí	pomoc	k1gFnSc7
geochemického	geochemický	k2eAgInSc2d1
rozboru	rozbor	k1gInSc2
fosilizovaných	fosilizovaný	k2eAgFnPc2d1
vaječných	vaječný	k2eAgFnPc2d1
skořápek	skořápka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
119	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
O	o	k7c6
rychlém	rychlý	k2eAgInSc6d1
metabolismu	metabolismus	k1gInSc6
a	a	k8xC
výkonné	výkonný	k2eAgFnSc6d1
respiraci	respirace	k1gFnSc6
druhohorních	druhohorní	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
svědčí	svědčit	k5eAaImIp3nS
také	také	k9
schopnost	schopnost	k1gFnSc1
velmi	velmi	k6eAd1
rychlého	rychlý	k2eAgInSc2d1
běhu	běh	k1gInSc2
přinejmenším	přinejmenším	k6eAd1
u	u	k7c2
některých	některý	k3yIgFnPc2
skupin	skupina	k1gFnPc2
(	(	kIx(
<g/>
menší	malý	k2eAgMnPc1d2
teropodi	teropod	k1gMnPc1
a	a	k8xC
ornitopodi	ornitopod	k1gMnPc1
<g/>
,	,	kIx,
mláďata	mládě	k1gNnPc1
a	a	k8xC
subadultní	subadultní	k2eAgInPc1d1
exempláře	exemplář	k1gInPc1
velkých	velký	k2eAgInPc2d1
teropodů	teropod	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svědčí	svědčit	k5eAaImIp3nS
o	o	k7c6
ní	on	k3xPp3gFnSc6
například	například	k6eAd1
také	také	k6eAd1
dochované	dochovaný	k2eAgFnPc1d1
ichnofosilie	ichnofosilie	k1gFnPc1
(	(	kIx(
<g/>
zkamenělé	zkamenělý	k2eAgInPc1d1
otisky	otisk	k1gInPc1
stop	stopa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
umožňují	umožňovat	k5eAaImIp3nP
vypočítat	vypočítat	k5eAaPmF
přibližnou	přibližný	k2eAgFnSc4d1
rychlost	rychlost	k1gFnSc4
pohybu	pohyb	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
dvou	dva	k4xCgFnPc2
sérií	série	k1gFnPc2
fosilních	fosilní	k2eAgFnPc2d1
stop	stopa	k1gFnPc2
z	z	k7c2
Polska	Polsko	k1gNnSc2
a	a	k8xC
Utahu	Utah	k1gInSc2
v	v	k7c6
USA	USA	kA
byla	být	k5eAaImAgFnS
vypočítána	vypočítat	k5eAaPmNgFnS
rychlost	rychlost	k1gFnSc1
běhu	běh	k1gInSc2
jejich	jejich	k3xOp3gMnPc2
původců	původce	k1gMnPc2
až	až	k9
na	na	k7c4
50	#num#	k4
km	km	kA
<g/>
/	/	kIx~
<g/>
h.	h.	k?
<g/>
[	[	kIx(
<g/>
120	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ekologie	ekologie	k1gFnSc1
dinosaurů	dinosaurus	k1gMnPc2
</s>
<s>
Převážná	převážný	k2eAgFnSc1d1
většina	většina	k1gFnSc1
druhohorních	druhohorní	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
obývala	obývat	k5eAaImAgFnS
pouze	pouze	k6eAd1
suchozemské	suchozemský	k2eAgInPc4d1
ekosystémy	ekosystém	k1gInPc4
<g/>
,	,	kIx,
mající	mající	k2eAgInPc4d1
charakter	charakter	k1gInSc4
pralesů	prales	k1gInPc2
<g/>
,	,	kIx,
záplavových	záplavový	k2eAgFnPc2d1
nížin	nížina	k1gFnPc2
a	a	k8xC
říčních	říční	k2eAgFnPc2d1
delt	delta	k1gFnPc2
<g/>
,	,	kIx,
výjimečně	výjimečně	k6eAd1
snad	snad	k9
i	i	k9
polopouští	polopoušť	k1gFnPc2
a	a	k8xC
náhorních	náhorní	k2eAgFnPc2d1
plošin	plošina	k1gFnPc2
(	(	kIx(
<g/>
o	o	k7c6
těch	ten	k3xDgInPc6
se	se	k3xPyFc4
nám	my	k3xPp1nPc3
však	však	k9
nedochovalo	dochovat	k5eNaPmAgNnS
mnoho	mnoho	k4c4
informací	informace	k1gFnPc2
vzhledem	vzhledem	k7c3
k	k	k7c3
malé	malý	k2eAgFnSc3d1
pravděpodobnosti	pravděpodobnost	k1gFnSc3
dochování	dochování	k1gNnSc2
zkamenělin	zkamenělina	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Někteří	některý	k3yIgMnPc1
dinosauři	dinosaurus	k1gMnPc1
byli	být	k5eAaImAgMnP
adaptovaní	adaptovaný	k2eAgMnPc1d1
na	na	k7c4
život	život	k1gInSc4
v	v	k7c6
dlouhodobém	dlouhodobý	k2eAgNnSc6d1
suchu	sucho	k1gNnSc6
<g/>
,	,	kIx,
jiní	jiný	k2eAgMnPc1d1
zase	zase	k9
v	v	k7c6
polární	polární	k2eAgFnSc6d1
zimě	zima	k1gFnSc6
(	(	kIx(
<g/>
jak	jak	k8xC,k8xS
ukazují	ukazovat	k5eAaImIp3nP
objevy	objev	k1gInPc4
z	z	k7c2
Antarktidy	Antarktida	k1gFnSc2
<g/>
,	,	kIx,
jihu	jih	k1gInSc3
Austrálie	Austrálie	k1gFnSc2
nebo	nebo	k8xC
Aljašky	Aljaška	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc1
opeřené	opeřený	k2eAgInPc1d1
druhy	druh	k1gInPc1
se	se	k3xPyFc4
dokázaly	dokázat	k5eAaPmAgInP
pohybovat	pohybovat	k5eAaImF
klouzavým	klouzavý	k2eAgInSc7d1
letem	let	k1gInSc7
mezi	mezi	k7c7
stromy	strom	k1gInPc7
(	(	kIx(
<g/>
například	například	k6eAd1
čínský	čínský	k2eAgInSc1d1
Microraptor	Microraptor	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jiné	jiný	k2eAgFnPc1d1
formy	forma	k1gFnPc1
zase	zase	k9
byly	být	k5eAaImAgFnP
uzpůsobeny	uzpůsobit	k5eAaPmNgInP
částečnému	částečný	k2eAgInSc3d1
nebo	nebo	k8xC
i	i	k8xC
dlouhodobému	dlouhodobý	k2eAgInSc3d1
životu	život	k1gInSc3
ve	v	k7c6
vodě	voda	k1gFnSc6
(	(	kIx(
<g/>
například	například	k6eAd1
teropodi	teropod	k1gMnPc1
spinosauridi	spinosaurid	k1gMnPc1
nebo	nebo	k8xC
rody	rod	k1gInPc1
Halszkaraptor	Halszkaraptor	k1gInSc1
a	a	k8xC
Koreaceratops	Koreaceratops	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
121	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
122	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zajímavé	zajímavý	k2eAgFnPc1d1
adaptace	adaptace	k1gFnPc1
pro	pro	k7c4
hrabavý	hrabavý	k2eAgInSc4d1
způsob	způsob	k1gInSc4
života	život	k1gInSc2
byly	být	k5eAaImAgInP
objeveny	objevit	k5eAaPmNgInP
u	u	k7c2
několika	několik	k4yIc2
druhů	druh	k1gInPc2
dinosaurů	dinosaurus	k1gMnPc2
<g/>
,	,	kIx,
zejména	zejména	k9
u	u	k7c2
malého	malý	k2eAgMnSc2d1
ornitopoda	ornitopod	k1gMnSc2
druhu	druh	k1gInSc2
Oryctodromeus	Oryctodromeus	k1gMnSc1
cubicularis	cubicularis	k1gFnSc1
z	z	k7c2
americké	americký	k2eAgFnSc2d1
Montany	Montana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgMnSc1
malý	malý	k2eAgMnSc1d1
býložravec	býložravec	k1gMnSc1
<g/>
,	,	kIx,
žijící	žijící	k2eAgMnSc1d1
v	v	k7c6
době	doba	k1gFnSc6
před	před	k7c7
95	#num#	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
<g/>
,	,	kIx,
zřejmě	zřejmě	k6eAd1
aktivně	aktivně	k6eAd1
vyhrabával	vyhrabávat	k5eAaImAgMnS
podzemní	podzemní	k2eAgFnPc4d1
nory	nora	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobné	podobný	k2eAgInPc1d1
útvary	útvar	k1gInPc1
byly	být	k5eAaImAgInP
objeveny	objevit	k5eAaPmNgInP
například	například	k6eAd1
také	také	k9
v	v	k7c6
Austrálii	Austrálie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dosud	dosud	k6eAd1
ale	ale	k8xC
nevíme	vědět	k5eNaImIp1nP
<g/>
,	,	kIx,
kolik	kolik	k4yRc4,k4yQc4,k4yIc4
druhů	druh	k1gInPc2
dinosaurů	dinosaurus	k1gMnPc2
a	a	k8xC
z	z	k7c2
jakých	jaký	k3yQgFnPc2,k3yIgFnPc2,k3yRgFnPc2
taxonomických	taxonomický	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
podobnou	podobný	k2eAgFnSc4d1
schopnost	schopnost	k1gFnSc4
vykazovalo	vykazovat	k5eAaImAgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
123	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
U	u	k7c2
býložravých	býložravý	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
se	se	k3xPyFc4
evoluční	evoluční	k2eAgFnSc1d1
úprava	úprava	k1gFnSc1
čelistního	čelistní	k2eAgInSc2d1
aparátu	aparát	k1gInSc2
pro	pro	k7c4
mechanické	mechanický	k2eAgNnSc4d1
zpracování	zpracování	k1gNnSc4
potravy	potrava	k1gFnSc2
rozdělila	rozdělit	k5eAaPmAgFnS
do	do	k7c2
dvou	dva	k4xCgFnPc2
základních	základní	k2eAgFnPc2d1
linií	linie	k1gFnPc2
–	–	k?
u	u	k7c2
jedné	jeden	k4xCgFnSc2
vznikl	vzniknout	k5eAaPmAgInS
kousací	kousací	k2eAgInSc1d1
aparát	aparát	k1gInSc1
podobný	podobný	k2eAgInSc1d1
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yIgInSc4,k3yRgInSc4
známe	znát	k5eAaImIp1nP
u	u	k7c2
savců	savec	k1gMnPc2
<g/>
,	,	kIx,
a	a	k8xC
ve	v	k7c6
druhém	druhý	k4xOgInSc6
případě	případ	k1gInSc6
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
více	hodně	k6eAd2
odpovídá	odpovídat	k5eAaImIp3nS
dnešním	dnešní	k2eAgMnPc3d1
ptákům	pták	k1gMnPc3
(	(	kIx(
<g/>
a	a	k8xC
čelistem	čelist	k1gFnPc3
přeměněným	přeměněný	k2eAgFnPc3d1
v	v	k7c6
zobák	zobák	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
124	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jaké	jaký	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
pachy	pach	k1gInPc1
dinosauři	dinosaurus	k1gMnPc1
vydávali	vydávat	k5eAaPmAgMnP,k5eAaImAgMnP
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
stále	stále	k6eAd1
velkou	velký	k2eAgFnSc7d1
otázkou	otázka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravděpodobně	pravděpodobně	k6eAd1
se	se	k3xPyFc4
některé	některý	k3yIgInPc1
druhy	druh	k1gInPc1
mohly	moct	k5eAaImAgInP
dorozumívat	dorozumívat	k5eAaImF
pachy	pach	k1gInPc7
<g/>
,	,	kIx,
produkovanými	produkovaný	k2eAgFnPc7d1
pachovými	pachový	k2eAgFnPc7d1
žlázami	žláza	k1gFnPc7
v	v	k7c6
kůži	kůže	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
tomto	tento	k3xDgInSc6
aspektu	aspekt	k1gInSc6
dinosauří	dinosauří	k2eAgFnSc2d1
biologie	biologie	k1gFnSc2
však	však	k9
nemáme	mít	k5eNaImIp1nP
žádné	žádný	k3yNgInPc4
pevné	pevný	k2eAgInPc4d1
fosilní	fosilní	k2eAgInPc4d1
doklady	doklad	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
125	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dinosauři	dinosaurus	k1gMnPc1
žili	žít	k5eAaImAgMnP
ve	v	k7c6
světě	svět	k1gInSc6
mírně	mírně	k6eAd1
odlišném	odlišný	k2eAgInSc6d1
od	od	k7c2
našeho	náš	k3xOp1gInSc2
–	–	k?
čím	co	k3yRnSc7,k3yInSc7,k3yQnSc7
starší	starší	k1gMnPc1
období	období	k1gNnSc2
bereme	brát	k5eAaImIp1nP
v	v	k7c4
potaz	potaz	k1gInSc4
<g/>
,	,	kIx,
tím	ten	k3xDgNnSc7
více	hodně	k6eAd2
odlišná	odlišný	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
tehdejší	tehdejší	k2eAgFnSc1d1
fauna	fauna	k1gFnSc1
i	i	k8xC
flóra	flóra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
konci	konec	k1gInSc3
křídové	křídový	k2eAgFnSc2d1
periody	perioda	k1gFnSc2
již	již	k6eAd1
vegetace	vegetace	k1gFnSc1
připomínala	připomínat	k5eAaImAgFnS
současnou	současný	k2eAgFnSc7d1
mnohem	mnohem	k6eAd1
více	hodně	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
v	v	k7c6
období	období	k1gNnSc6
triasu	trias	k1gInSc2
o	o	k7c4
150	#num#	k4
milionů	milion	k4xCgInPc2
let	léto	k1gNnPc2
dříve	dříve	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
délka	délka	k1gFnSc1
dní	den	k1gInPc2
se	se	k3xPyFc4
tehdy	tehdy	k6eAd1
lišila	lišit	k5eAaImAgFnS
od	od	k7c2
současných	současný	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
v	v	k7c6
období	období	k1gNnSc6
svrchní	svrchní	k2eAgFnSc2d1
křídy	křída	k1gFnSc2
(	(	kIx(
<g/>
asi	asi	k9
před	před	k7c7
75	#num#	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
délka	délka	k1gFnSc1
dne	den	k1gInSc2
o	o	k7c4
půl	půl	k1xP
hodiny	hodina	k1gFnSc2
kratší	krátký	k2eAgInPc1d2
než	než	k8xS
dnes	dnes	k6eAd1
(	(	kIx(
<g/>
činila	činit	k5eAaImAgFnS
asi	asi	k9
23,5	23,5	k4
hodiny	hodina	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
protože	protože	k8xS
Země	země	k1gFnSc1
se	se	k3xPyFc4
tehdy	tehdy	k6eAd1
ještě	ještě	k6eAd1
o	o	k7c4
trochu	trocha	k1gFnSc4
rychleji	rychle	k6eAd2
otáčela	otáčet	k5eAaImAgFnS
kolem	kolem	k7c2
své	svůj	k3xOyFgFnSc2
osy	osa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dinosauří	dinosauří	k2eAgInSc1d1
rok	rok	k1gInSc1
měl	mít	k5eAaImAgInS
tedy	tedy	k9
více	hodně	k6eAd2
(	(	kIx(
<g/>
konkrétně	konkrétně	k6eAd1
372	#num#	k4
<g/>
)	)	kIx)
dní	den	k1gInPc2
<g/>
,	,	kIx,
oproti	oproti	k7c3
dnešním	dnešní	k2eAgInPc3d1
365,25	365,25	k4
dne	den	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
126	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
výzkumu	výzkum	k1gInSc2
</s>
<s>
Předvědecké	předvědecký	k2eAgNnSc1d1
období	období	k1gNnSc1
</s>
<s>
Rohatý	rohatý	k2eAgMnSc1d1
dinosaurus	dinosaurus	k1gMnSc1
rodu	rod	k1gInSc2
Achelousaurus	Achelousaurus	k1gMnSc1
</s>
<s>
Zkameněliny	zkamenělina	k1gFnPc1
dinosaurů	dinosaurus	k1gMnPc2
v	v	k7c6
podobě	podoba	k1gFnSc6
fosilních	fosilní	k2eAgFnPc2d1
skořápek	skořápka	k1gFnPc2
vajec	vejce	k1gNnPc2
byly	být	k5eAaImAgFnP
v	v	k7c6
Mongolsku	Mongolsko	k1gNnSc6
možná	možná	k9
využívány	využívat	k5eAaImNgInP,k5eAaPmNgInP
k	k	k7c3
výrobě	výroba	k1gFnSc3
náhrdelníků	náhrdelník	k1gInPc2
již	již	k6eAd1
ke	k	k7c3
konci	konec	k1gInSc3
období	období	k1gNnSc2
paleolitu	paleolit	k1gInSc2
<g/>
[	[	kIx(
<g/>
127	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fosilní	fosilní	k2eAgFnPc1d1
kosti	kost	k1gFnPc1
dinosaurů	dinosaurus	k1gMnPc2
již	již	k6eAd1
prokazatelně	prokazatelně	k6eAd1
znali	znát	k5eAaImAgMnP
také	také	k9
staří	starý	k2eAgMnPc1d1
Číňané	Číňan	k1gMnPc1
kolem	kolem	k7c2
roku	rok	k1gInSc2
300	#num#	k4
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
máme	mít	k5eAaImIp1nP
první	první	k4xOgInSc4
písemný	písemný	k2eAgInSc4d1
záznam	záznam	k1gInSc4
o	o	k7c4
jejich	jejich	k3xOp3gNnSc4
objevení	objevení	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
128	#num#	k4
<g/>
]	]	kIx)
Lze	lze	k6eAd1
však	však	k9
předpokládat	předpokládat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
místní	místní	k2eAgMnPc1d1
obyvatelé	obyvatel	k1gMnPc1
znali	znát	k5eAaImAgMnP
již	již	k6eAd1
před	před	k7c7
mnoha	mnoho	k4c7
tisíciletími	tisíciletí	k1gNnPc7
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
o	o	k7c6
jejich	jejich	k3xOp3gFnSc6
pravé	pravý	k2eAgFnSc6d1
podstatě	podstata	k1gFnSc6
neměli	mít	k5eNaImAgMnP
ještě	ještě	k9
správnou	správný	k2eAgFnSc4d1
představu	představa	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Považovali	považovat	k5eAaImAgMnP
je	on	k3xPp3gMnPc4
proto	proto	k8xC
často	často	k6eAd1
za	za	k7c4
zkamenělé	zkamenělý	k2eAgInPc4d1
květy	květ	k1gInPc4
nebo	nebo	k8xC
otisky	otisk	k1gInPc4
stop	stopa	k1gFnPc2
různých	různý	k2eAgMnPc2d1
bájných	bájný	k2eAgMnPc2d1
tvorů	tvor	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
129	#num#	k4
<g/>
]	]	kIx)
Významným	významný	k2eAgInSc7d1
příkladem	příklad	k1gInSc7
je	být	k5eAaImIp3nS
"	"	kIx"
<g/>
Lotosová	lotosový	k2eAgFnSc1d1
pevnost	pevnost	k1gFnSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
200	#num#	k4
metrů	metr	k1gInPc2
dlouhý	dlouhý	k2eAgInSc4d1
vertikální	vertikální	k2eAgInSc4d1
zářez	zářez	k1gInSc4
do	do	k7c2
pískovcové	pískovcový	k2eAgFnSc2d1
skály	skála	k1gFnSc2
<g/>
,	,	kIx,
obsahující	obsahující	k2eAgInPc1d1
fosilní	fosilní	k2eAgInPc1d1
otisky	otisk	k1gInPc1
stop	stop	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
z	z	k7c2
období	období	k1gNnSc2
rané	raný	k2eAgFnSc2d1
křídy	křída	k1gFnSc2
(	(	kIx(
<g/>
ichnorod	ichnorod	k1gInSc1
Caririchnium	Caririchnium	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stopy	stopa	k1gFnPc1
ptakopánvých	ptakopánvý	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
připomínají	připomínat	k5eAaImIp3nP
květy	květ	k1gInPc1
lotosu	lotos	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
dalo	dát	k5eAaPmAgNnS
této	tento	k3xDgFnSc6
lokalitě	lokalita	k1gFnSc6
její	její	k3xOp3gInSc4
název	název	k1gInSc4
i	i	k8xC
pověst	pověst	k1gFnSc4
magického	magický	k2eAgNnSc2d1
místa	místo	k1gNnSc2
s	s	k7c7
kouzelnou	kouzelný	k2eAgFnSc7d1
mocí	moc	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lokalita	lokalita	k1gFnSc1
je	být	k5eAaImIp3nS
přitom	přitom	k6eAd1
trvale	trvale	k6eAd1
osídlena	osídlit	k5eAaPmNgFnS
minimálně	minimálně	k6eAd1
od	od	k7c2
poloviny	polovina	k1gFnSc2
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
a	a	k8xC
sloužila	sloužit	k5eAaImAgFnS
původně	původně	k6eAd1
jako	jako	k8xS,k8xC
pevnost	pevnost	k1gFnSc1
s	s	k7c7
posádkou	posádka	k1gFnSc7
<g/>
,	,	kIx,
chránící	chránící	k2eAgFnSc1d1
tuto	tento	k3xDgFnSc4
část	část	k1gFnSc4
Číny	Čína	k1gFnSc2
před	před	k7c7
vpády	vpád	k1gInPc7
mongolských	mongolský	k2eAgInPc2d1
kmenů	kmen	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
130	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
některých	některý	k3yIgFnPc6
oblastech	oblast	k1gFnPc6
Číny	Čína	k1gFnSc2
byly	být	k5eAaImAgInP
(	(	kIx(
<g/>
a	a	k8xC
dosud	dosud	k6eAd1
jsou	být	k5eAaImIp3nP
<g/>
)	)	kIx)
tyto	tento	k3xDgFnPc4
fosilie	fosilie	k1gFnPc4
tak	tak	k6eAd1
četné	četný	k2eAgFnPc1d1
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
místní	místní	k2eAgMnPc1d1
mudrci	mudrc	k1gMnPc1
a	a	k8xC
lékaři	lékař	k1gMnPc1
roztloukali	roztloukat	k5eAaImAgMnP
na	na	k7c4
prach	prach	k1gInSc4
a	a	k8xC
používali	používat	k5eAaImAgMnP
údajně	údajně	k6eAd1
jako	jako	k8xS,k8xC
prostředek	prostředek	k1gInSc1
na	na	k7c4
mužskou	mužský	k2eAgFnSc4d1
potenci	potence	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
bylo	být	k5eAaImAgNnS
navíc	navíc	k6eAd1
zjištěno	zjistit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
mnozí	mnohý	k2eAgMnPc1d1
venkované	venkovan	k1gMnPc1
takto	takto	k6eAd1
používají	používat	k5eAaImIp3nP
fosilie	fosilie	k1gFnPc1
dinosaurů	dinosaurus	k1gMnPc2
i	i	k8xC
v	v	k7c6
současnosti	současnost	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
131	#num#	k4
<g/>
]	]	kIx)
Představa	představa	k1gFnSc1
o	o	k7c4
množství	množství	k1gNnSc4
zničených	zničený	k2eAgInPc2d1
cenných	cenný	k2eAgInPc2d1
pozůstatků	pozůstatek	k1gInPc2
pravěku	pravěk	k1gInSc2
je	být	k5eAaImIp3nS
potom	potom	k6eAd1
téměř	téměř	k6eAd1
děsivá	děsivý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zkameněliny	zkamenělina	k1gFnPc1
dinosaurů	dinosaurus	k1gMnPc2
byly	být	k5eAaImAgFnP
na	na	k7c6
mnoha	mnoho	k4c6
místech	místo	k1gNnPc6
světa	svět	k1gInSc2
známé	známý	k2eAgNnSc1d1
již	již	k6eAd1
v	v	k7c6
předvědeckém	předvědecký	k2eAgNnSc6d1
období	období	k1gNnSc6
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
ale	ale	k9
byly	být	k5eAaImAgInP
považovány	považován	k2eAgInPc1d1
za	za	k7c4
pozůstatky	pozůstatek	k1gInPc4
legendárních	legendární	k2eAgMnPc2d1
králů	král	k1gMnPc2
<g/>
,	,	kIx,
božstev	božstvo	k1gNnPc2
<g/>
,	,	kIx,
obrů	obr	k1gMnPc2
nebo	nebo	k8xC
draků	drak	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
132	#num#	k4
<g/>
]	]	kIx)
Mnohé	mnohý	k2eAgInPc4d1
domnělé	domnělý	k2eAgInPc4d1
pozůstatky	pozůstatek	k1gInPc4
"	"	kIx"
<g/>
draků	drak	k1gInPc2
<g/>
"	"	kIx"
byly	být	k5eAaImAgInP
spíše	spíše	k9
fosiliemi	fosilie	k1gFnPc7
velkých	velký	k2eAgMnPc2d1
pleistocénních	pleistocénní	k2eAgMnPc2d1
savců	savec	k1gMnPc2
než	než	k8xS
zkamenělinami	zkamenělina	k1gFnPc7
druhohorních	druhohorní	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
133	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podobná	podobný	k2eAgNnPc1d1
božstva	božstvo	k1gNnPc1
<g/>
,	,	kIx,
založená	založený	k2eAgFnSc1d1
na	na	k7c6
objevech	objev	k1gInPc6
dinosauřích	dinosauří	k2eAgFnPc2d1
zkamenělin	zkamenělina	k1gFnPc2
<g/>
,	,	kIx,
však	však	k9
uctívali	uctívat	k5eAaImAgMnP
například	například	k6eAd1
také	také	k9
Tibeťané	Tibeťan	k1gMnPc1
a	a	k8xC
Skytové	Skyt	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
134	#num#	k4
<g/>
]	]	kIx)
Staří	starý	k2eAgMnPc1d1
Číňané	Číňan	k1gMnPc1
věřili	věřit	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
velké	velký	k2eAgFnPc1d1
kosti	kost	k1gFnPc1
patřily	patřit	k5eAaImAgFnP
drakům	drak	k1gMnPc3
a	a	k8xC
dali	dát	k5eAaPmAgMnP
jim	on	k3xPp3gMnPc3
jméno	jméno	k1gNnSc1
konglong	konglonga	k1gFnPc2
(	(	kIx(
<g/>
„	„	k?
<g/>
hrozný	hrozný	k2eAgInSc4d1
drak	drak	k1gInSc4
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k9
původní	původní	k2eAgMnPc1d1
obyvatelé	obyvatel	k1gMnPc1
Severní	severní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
se	se	k3xPyFc4
již	již	k6eAd1
před	před	k7c7
mnoha	mnoho	k4c7
stovkami	stovka	k1gFnPc7
(	(	kIx(
<g/>
a	a	k8xC
možná	možná	k9
tisíci	tisíc	k4xCgInPc7
<g/>
)	)	kIx)
let	let	k1gInSc4
setkávali	setkávat	k5eAaImAgMnP
s	s	k7c7
fosilními	fosilní	k2eAgFnPc7d1
kostmi	kost	k1gFnPc7
velkých	velký	k2eAgNnPc2d1
vymřelých	vymřelý	k2eAgNnPc2d1
zvířat	zvíře	k1gNnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
kostí	kost	k1gFnPc2
dinosaurů	dinosaurus	k1gMnPc2
(	(	kIx(
<g/>
především	především	k9
na	na	k7c6
severozápadě	severozápad	k1gInSc6
USA	USA	kA
a	a	k8xC
na	na	k7c6
západě	západ	k1gInSc6
Kanady	Kanada	k1gFnSc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
135	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Evropě	Evropa	k1gFnSc6
byly	být	k5eAaImAgFnP
velké	velký	k2eAgFnPc1d1
kosti	kost	k1gFnPc1
dinosaurů	dinosaurus	k1gMnPc2
(	(	kIx(
<g/>
ale	ale	k8xC
i	i	k9
mamutů	mamut	k1gMnPc2
<g/>
,	,	kIx,
jeskynních	jeskynní	k2eAgMnPc2d1
medvědů	medvěd	k1gMnPc2
apod.	apod.	kA
<g/>
)	)	kIx)
považovány	považován	k2eAgInPc1d1
za	za	k7c2
pozůstatky	pozůstatek	k1gInPc1
zvířat	zvíře	k1gNnPc2
žijících	žijící	k2eAgFnPc2d1
před	před	k7c7
biblickou	biblický	k2eAgFnSc7d1
potopou	potopa	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Portugalsku	Portugalsko	k1gNnSc6
byly	být	k5eAaImAgInP
od	od	k7c2
13	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
stopy	stopa	k1gFnSc2
sauropodního	sauropodní	k2eAgMnSc2d1
dinosaura	dinosaurus	k1gMnSc2
na	na	k7c6
útesech	útes	k1gInPc6
mysu	mys	k1gInSc2
Cabo	Cabo	k1gNnSc1
Espichel	Espichel	k1gInSc1
považovány	považován	k2eAgFnPc1d1
za	za	k7c4
stopy	stopa	k1gFnPc4
oslíka	oslík	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
vezl	vézt	k5eAaImAgMnS
na	na	k7c6
hřbetě	hřbet	k1gInSc6
panenku	panenka	k1gFnSc4
Marii	Maria	k1gFnSc3
s	s	k7c7
malým	malý	k1gMnSc7
Ježíšem	Ježíš	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
136	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
137	#num#	k4
<g/>
]	]	kIx)
Dinosauří	dinosauří	k2eAgFnPc1d1
stopy	stopa	k1gFnPc1
byly	být	k5eAaImAgFnP
nicméně	nicméně	k8xC
dlouhodobě	dlouhodobě	k6eAd1
známé	známý	k2eAgNnSc1d1
a	a	k8xC
dokonce	dokonce	k9
i	i	k9
nábožensky	nábožensky	k6eAd1
uctívané	uctívaný	k2eAgInPc1d1
již	již	k6eAd1
před	před	k7c7
staletími	staletí	k1gNnPc7
<g/>
[	[	kIx(
<g/>
138	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
dokládá	dokládat	k5eAaImIp3nS
také	také	k9
příklad	příklad	k1gInSc4
z	z	k7c2
polských	polský	k2eAgFnPc2d1
Svatokřížských	Svatokřížský	k2eAgFnPc2d1
hor	hora	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byly	být	k5eAaImAgFnP
vedle	vedle	k7c2
stop	stopa	k1gFnPc2
raně	raně	k6eAd1
jurských	jurský	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
(	(	kIx(
<g/>
ichnodruh	ichnodruh	k1gMnSc1
Moyenisauropus	Moyenisauropus	k1gMnSc1
karaszevskii	karaszevskie	k1gFnSc4
<g/>
)	)	kIx)
tesány	tesán	k2eAgInPc1d1
do	do	k7c2
kamene	kámen	k1gInSc2
petroglyfy	petroglyf	k1gMnPc4
<g/>
,	,	kIx,
které	který	k3yIgMnPc4,k3yQgMnPc4,k3yRgMnPc4
možná	možná	k9
měly	mít	k5eAaImAgFnP
zobrazovat	zobrazovat	k5eAaImF
domnělou	domnělý	k2eAgFnSc4d1
podobu	podoba	k1gFnSc4
jejich	jejich	k3xOp3gMnSc2
původce	původce	k1gMnSc2
(	(	kIx(
<g/>
a	a	k8xC
mohly	moct	k5eAaImAgFnP
být	být	k5eAaImF
také	také	k9
místem	místo	k1gNnSc7
jakýchsi	jakýsi	k3yIgInPc2
magických	magický	k2eAgInPc2d1
okultních	okultní	k2eAgInPc2d1
rituálů	rituál	k1gInPc2
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
139	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
některých	některý	k3yIgInPc2
dobových	dobový	k2eAgInPc2d1
pramenů	pramen	k1gInPc2
(	(	kIx(
<g/>
např.	např.	kA
tzv.	tzv.	kA
Pseudo-Arostotelés	Pseudo-Arostotelésa	k1gFnPc2
<g/>
)	)	kIx)
mohli	moct	k5eAaImAgMnP
fosilní	fosilní	k2eAgFnPc4d1
dinosauří	dinosauří	k2eAgFnPc4d1
stopy	stopa	k1gFnPc4
(	(	kIx(
<g/>
označované	označovaný	k2eAgFnPc1d1
za	za	k7c2
stopy	stopa	k1gFnSc2
mytického	mytický	k2eAgMnSc4d1
Hérakla	Hérakles	k1gMnSc4
<g/>
)	)	kIx)
znát	znát	k5eAaImF
také	také	k9
obyvatelé	obyvatel	k1gMnPc1
území	území	k1gNnSc2
jižní	jižní	k2eAgFnSc2d1
Itálie	Itálie	k1gFnSc2
v	v	k7c6
dobách	doba	k1gFnPc6
klasické	klasický	k2eAgFnSc2d1
antiky	antika	k1gFnSc2
již	již	k6eAd1
před	před	k7c7
více	hodně	k6eAd2
než	než	k8xS
dvěma	dva	k4xCgNnPc7
tisíciletími	tisíciletí	k1gNnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
140	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Fosilie	fosilie	k1gFnPc1
dinosaurů	dinosaurus	k1gMnPc2
byly	být	k5eAaImAgFnP
vědomě	vědomě	k6eAd1
či	či	k8xC
nevědomě	vědomě	k6eNd1
využívány	využívat	k5eAaPmNgInP,k5eAaImNgInP
také	také	k9
jako	jako	k9
stavební	stavební	k2eAgInSc4d1
materiál	materiál	k1gInSc4
a	a	k8xC
můžeme	moct	k5eAaImIp1nP
je	on	k3xPp3gFnPc4
dnes	dnes	k6eAd1
vzácně	vzácně	k6eAd1
nalézt	nalézt	k5eAaPmF,k5eAaBmF
například	například	k6eAd1
ve	v	k7c6
zdech	zeď	k1gFnPc6
různých	různý	k2eAgFnPc2d1
prehistorických	prehistorický	k2eAgFnPc2d1
i	i	k8xC
historických	historický	k2eAgFnPc2d1
budov	budova	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
141	#num#	k4
<g/>
]	]	kIx)
První	první	k4xOgFnSc1
kvalitní	kvalitní	k2eAgFnSc1d1
písemná	písemný	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
dinosauří	dinosauří	k2eAgFnSc6d1
fosílii	fosílie	k1gFnSc6
však	však	k9
pochází	pocházet	k5eAaImIp3nS
až	až	k9
z	z	k7c2
raně	raně	k6eAd1
novověké	novověký	k2eAgFnSc2d1
Anglie	Anglie	k1gFnSc2
a	a	k8xC
datuje	datovat	k5eAaImIp3nS
se	se	k3xPyFc4
k	k	k7c3
roku	rok	k1gInSc3
1676	#num#	k4
(	(	kIx(
<g/>
jiný	jiný	k2eAgInSc1d1
údaj	údaj	k1gInSc1
hovoří	hovořit	k5eAaImIp3nS
o	o	k7c6
roku	rok	k1gInSc6
1677	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jde	jít	k5eAaImIp3nS
však	však	k9
zatím	zatím	k6eAd1
jen	jen	k9
o	o	k7c6
zobrazení	zobrazení	k1gNnSc6
bez	bez	k7c2
odborného	odborný	k2eAgInSc2d1
popisu	popis	k1gInSc2
(	(	kIx(
<g/>
autorem	autor	k1gMnSc7
byl	být	k5eAaImAgMnS
tehdejší	tehdejší	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
Ashmolean	Ashmolean	k1gMnSc1
museum	museum	k1gNnSc1
Robert	Robert	k1gMnSc1
Plot	plot	k1gInSc1
a	a	k8xC
jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
část	část	k1gFnSc4
stehenní	stehenní	k2eAgFnSc2d1
kosti	kost	k1gFnSc2
teropoda	teropod	k1gMnSc2
megalosaura	megalosaur	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1763	#num#	k4
popsal	popsat	k5eAaPmAgMnS
tuto	tento	k3xDgFnSc4
fosilii	fosilie	k1gFnSc4
anglický	anglický	k2eAgMnSc1d1
přírodovědec	přírodovědec	k1gMnSc1
Richard	Richard	k1gMnSc1
Brookes	Brookes	k1gMnSc1
jako	jako	k8xS,k8xC
Scrotum	Scrotum	k1gNnSc1
humanum	humanum	k1gNnSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
šourek	šourek	k1gInSc1
lidský	lidský	k2eAgInSc1d1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
na	na	k7c6
základě	základ	k1gInSc6
její	její	k3xOp3gFnSc2
povrchní	povrchní	k2eAgFnSc2d1
podobnosti	podobnost	k1gFnSc2
s	s	k7c7
mužskými	mužský	k2eAgNnPc7d1
varlaty	varle	k1gNnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
142	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Prvním	první	k4xOgInSc7
vědeckým	vědecký	k2eAgInSc7d1
názvem	název	k1gInSc7
<g/>
,	,	kIx,
přisouzeným	přisouzený	k2eAgInSc7d1
fosilii	fosilie	k1gFnSc6
dinosaura	dinosaurus	k1gMnSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
taxon	taxon	k1gInSc1
Rutellum	Rutellum	k1gInSc1
excavatum	excavatum	k1gNnSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
použil	použít	k5eAaPmAgMnS
Plotův	Plotův	k2eAgMnSc1d1
nástupce	nástupce	k1gMnSc1
ve	v	k7c6
funkci	funkce	k1gFnSc6
ředitele	ředitel	k1gMnSc2
muzea	muzeum	k1gNnSc2
<g/>
,	,	kIx,
Edward	Edward	k1gMnSc1
Lhuyd	Lhuyd	k1gMnSc1
roku	rok	k1gInSc2
1699	#num#	k4
k	k	k7c3
popisu	popis	k1gInSc3
zubu	zub	k1gInSc2
sauropoda	sauropod	k1gMnSc4
cetiosaura	cetiosaur	k1gMnSc4
(	(	kIx(
<g/>
o	o	k7c6
skutečném	skutečný	k2eAgNnSc6d1
zvířeti	zvíře	k1gNnSc6
však	však	k9
neměl	mít	k5eNaImAgInS
správnou	správný	k2eAgFnSc4d1
představu	představa	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Již	již	k6eAd1
kolem	kolem	k7c2
roku	rok	k1gInSc2
1778	#num#	k4
zkoumal	zkoumat	k5eAaImAgMnS
dinosauří	dinosauří	k2eAgFnPc4d1
fosilie	fosilie	k1gFnPc4
francouzský	francouzský	k2eAgMnSc1d1
učenec	učenec	k1gMnSc1
a	a	k8xC
kněz	kněz	k1gMnSc1
Charles	Charles	k1gMnSc1
Bacheley	Bachelea	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravou	pravý	k2eAgFnSc4d1
povahu	povaha	k1gFnSc4
jejich	jejich	k3xOp3gFnPc2
fosilií	fosilie	k1gFnPc2
ještě	ještě	k6eAd1
nerozpoznal	rozpoznat	k5eNaPmAgMnS
<g/>
,	,	kIx,
ale	ale	k8xC
domníval	domnívat	k5eAaImAgMnS
se	se	k3xPyFc4
již	již	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
mohlo	moct	k5eAaImAgNnS
jednat	jednat	k5eAaImF
o	o	k7c4
dnes	dnes	k6eAd1
již	již	k6eAd1
nežijící	žijící	k2eNgMnPc4d1
živočichy	živočich	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
143	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vědecké	vědecký	k2eAgNnSc1d1
období	období	k1gNnSc1
</s>
<s>
Prvním	první	k4xOgMnSc7
opravdu	opravdu	k6eAd1
vědecky	vědecky	k6eAd1
popsaným	popsaný	k2eAgMnSc7d1
dinosaurem	dinosaurus	k1gMnSc7
byl	být	k5eAaImAgMnS
tak	tak	k6eAd1
opět	opět	k6eAd1
Megalosaurus	Megalosaurus	k1gInSc1
<g/>
,	,	kIx,
popsaný	popsaný	k2eAgInSc1d1
roku	rok	k1gInSc2
1824	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
144	#num#	k4
<g/>
]	]	kIx)
avšak	avšak	k8xC
o	o	k7c4
dinosaury	dinosaurus	k1gMnPc4
propukl	propuknout	k5eAaPmAgInS
zájem	zájem	k1gInSc1
teprve	teprve	k6eAd1
po	po	k7c6
objevu	objev	k1gInSc6
jiného	jiný	k2eAgMnSc2d1
dinosaura	dinosaurus	k1gMnSc2
<g/>
,	,	kIx,
ornitopoda	ornitopod	k1gMnSc2
iguanodona	iguanodon	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
květnu	květen	k1gInSc6
roku	rok	k1gInSc2
1822	#num#	k4
nalezl	nalézt	k5eAaBmAgMnS,k5eAaPmAgMnS
doktor	doktor	k1gMnSc1
a	a	k8xC
nadšený	nadšený	k2eAgMnSc1d1
paleontolog	paleontolog	k1gMnSc1
amatér	amatér	k1gMnSc1
<g/>
,	,	kIx,
Gideon	Gideon	k1gMnSc1
Mantell	Mantell	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
oblasti	oblast	k1gFnSc6
Tilgate	Tilgat	k1gInSc5
Forest	Forest	k1gMnSc1
v	v	k7c6
jihoanglickém	jihoanglický	k2eAgNnSc6d1
hrabství	hrabství	k1gNnSc6
Sussex	Sussex	k1gInSc1
kus	kus	k6eAd1
pískovce	pískovec	k1gInPc1
se	s	k7c7
zkamenělým	zkamenělý	k2eAgInSc7d1
zubem	zub	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zub	zub	k1gInSc1
jej	on	k3xPp3gMnSc4
nadchl	nadchnout	k5eAaPmAgInS
a	a	k8xC
tak	tak	k6eAd1
pátral	pátrat	k5eAaImAgMnS
po	po	k7c6
dalších	další	k2eAgInPc6d1
pozůstatcích	pozůstatek	k1gInPc6
záhadného	záhadný	k2eAgMnSc4d1
živočicha	živočich	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měl	mít	k5eAaImAgMnS
štěstí	štěstí	k1gNnSc4
a	a	k8xC
objevil	objevit	k5eAaPmAgMnS
další	další	k2eAgFnPc4d1
fosílie	fosílie	k1gFnPc4
patřící	patřící	k2eAgInSc1d1
stejnému	stejný	k2eAgInSc3d1
druhu	druh	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doktor	doktor	k1gMnSc1
Mantell	Mantell	k1gMnSc1
dospěl	dochvít	k5eAaPmAgMnS
k	k	k7c3
závěru	závěr	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
obrovitého	obrovitý	k2eAgMnSc4d1
plaza	plaz	k1gMnSc4
a	a	k8xC
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
víc	hodně	k6eAd2
<g/>
,	,	kIx,
z	z	k7c2
tvaru	tvar	k1gInSc2
a	a	k8xC
opotřebování	opotřebování	k1gNnSc6
zubů	zub	k1gInPc2
usoudil	usoudit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
to	ten	k3xDgNnSc1
byl	být	k5eAaImAgMnS
býložravec	býložravec	k1gMnSc1
(	(	kIx(
<g/>
býložravý	býložravý	k2eAgMnSc1d1
plaz	plaz	k1gMnSc1
–	–	k?
to	ten	k3xDgNnSc4
byla	být	k5eAaImAgFnS
pro	pro	k7c4
tehdejší	tehdejší	k2eAgMnPc4d1
biology	biolog	k1gMnPc4
věc	věc	k1gFnSc1
nepředstavitelná	představitelný	k2eNgFnSc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Nález	nález	k1gInSc1
doktora	doktor	k1gMnSc2
Mantella	Mantell	k1gMnSc2
nebyl	být	k5eNaImAgInS
přijat	přijmout	k5eAaPmNgInS
vědci	vědec	k1gMnPc7
z	z	k7c2
Geologické	geologický	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
v	v	k7c6
Londýně	Londýn	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvrdili	tvrdit	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
zub	zub	k1gInSc4
ryby	ryba	k1gFnSc2
<g/>
,	,	kIx,
případně	případně	k6eAd1
savce	savec	k1gMnPc4
z	z	k7c2
diluvia	diluvium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
dokonce	dokonce	k9
varován	varovat	k5eAaImNgMnS
profesorem	profesor	k1gMnSc7
oxfordské	oxfordský	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
<g/>
,	,	kIx,
Williamem	William	k1gInSc7
Bucklandem	Buckland	k1gInSc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
netvrdil	tvrdit	k5eNaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
nálezy	nález	k1gInPc1
pocházejí	pocházet	k5eAaImIp3nP
z	z	k7c2
pískovcových	pískovcový	k2eAgInPc2d1
lomů	lom	k1gInPc2
oblasti	oblast	k1gFnSc2
Tilgate	Tilgat	k1gInSc5
Forest	Forest	k1gFnSc4
<g/>
,	,	kIx,
protože	protože	k8xS
„	„	k?
<g/>
v	v	k7c6
žádném	žádný	k3yNgInSc6
případě	případ	k1gInSc6
nemohou	moct	k5eNaImIp3nP
být	být	k5eAaImF
tak	tak	k6eAd1
staré	starý	k2eAgNnSc4d1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Eoraptor	Eoraptor	k1gInSc1
lunensis	lunensis	k1gFnSc2
</s>
<s>
Sám	sám	k3xTgInSc1
Buckland	Buckland	k1gInSc1
popsal	popsat	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1824	#num#	k4
z	z	k7c2
fosilních	fosilní	k2eAgInPc2d1
pozůstatků	pozůstatek	k1gInPc2
jež	jenž	k3xRgInPc4
měl	mít	k5eAaImAgMnS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
(	(	kIx(
<g/>
část	část	k1gFnSc1
čelisti	čelist	k1gFnSc2
<g/>
,	,	kIx,
pánve	pánev	k1gFnSc2
<g/>
,	,	kIx,
lopatky	lopatka	k1gFnSc2
zadní	zadní	k2eAgFnSc2d1
nohy	noha	k1gFnSc2
a	a	k8xC
několik	několik	k4yIc4
obratlů	obratel	k1gInPc2
<g/>
)	)	kIx)
megalosaura	megalosaura	k1gFnSc1
(	(	kIx(
<g/>
použil	použít	k5eAaPmAgInS
pouze	pouze	k6eAd1
rodové	rodový	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Popsal	popsat	k5eAaPmAgMnS
jej	on	k3xPp3gMnSc4
jako	jako	k8xS,k8xC
obrovitého	obrovitý	k2eAgMnSc2d1
dravého	dravý	k2eAgMnSc2d1
ještěra	ještěr	k1gMnSc2
<g/>
,	,	kIx,
přestože	přestože	k8xS
si	se	k3xPyFc3
všiml	všimnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
zuby	zub	k1gInPc1
jsou	být	k5eAaImIp3nP
<g/>
,	,	kIx,
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
u	u	k7c2
krokodýlů	krokodýl	k1gMnPc2
<g/>
,	,	kIx,
zasazeny	zasazen	k2eAgFnPc1d1
v	v	k7c6
jamkách	jamka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Megalosaurus	Megalosaurus	k1gInSc1
byl	být	k5eAaImAgInS
bez	bez	k7c2
problémů	problém	k1gInPc2
akceptován	akceptovat	k5eAaBmNgInS
vědeckou	vědecký	k2eAgFnSc7d1
veřejností	veřejnost	k1gFnSc7
(	(	kIx(
<g/>
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
prvním	první	k4xOgMnSc6
formálně	formálně	k6eAd1
popsaným	popsaný	k2eAgMnSc7d1
dinosaurem	dinosaurus	k1gMnSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Ale	ale	k9
Gideon	Gideon	k1gMnSc1
Mantell	Mantell	k1gMnSc1
svůj	svůj	k3xOyFgInSc4
boj	boj	k1gInSc4
nevzdával	vzdávat	k5eNaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydal	vydat	k5eAaPmAgMnS
se	se	k3xPyFc4
do	do	k7c2
Londýna	Londýn	k1gInSc2
<g/>
,	,	kIx,
aby	aby	k9
nalezené	nalezený	k2eAgInPc4d1
zuby	zub	k1gInPc4
porovnal	porovnat	k5eAaPmAgMnS
se	s	k7c7
zuby	zub	k1gInPc7
v	v	k7c6
muzejních	muzejní	k2eAgFnPc6d1
sbírkách	sbírka	k1gFnPc6
plazů	plaz	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nenalezl	nalézt	k5eNaBmAgMnS,k5eNaPmAgMnS
ovšem	ovšem	k9
nic	nic	k3yNnSc1
podobného	podobný	k2eAgNnSc2d1
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
měl	mít	k5eAaImAgMnS
v	v	k7c6
ruce	ruka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Shodou	shoda	k1gFnSc7
náhod	náhoda	k1gFnPc2
se	se	k3xPyFc4
v	v	k7c6
muzeu	muzeum	k1gNnSc6
potkal	potkat	k5eAaPmAgInS
se	s	k7c7
Samuelem	Samuel	k1gMnSc7
Stutchburym	Stutchburym	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
zabýval	zabývat	k5eAaImAgInS
studiem	studio	k1gNnSc7
leguánů	leguán	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fosilní	fosilní	k2eAgInSc1d1
zub	zub	k1gInSc1
byl	být	k5eAaImAgInS
nápadně	nápadně	k6eAd1
podobný	podobný	k2eAgInSc1d1
zubům	zub	k1gInPc3
leguána	leguán	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
byl	být	k5eAaImAgInS
pro	pro	k7c4
Mantella	Mantell	k1gMnSc4
konečný	konečný	k2eAgInSc1d1
impuls	impuls	k1gInSc1
a	a	k8xC
tak	tak	k9
roku	rok	k1gInSc2
1825	#num#	k4
popsal	popsat	k5eAaPmAgMnS
býložravého	býložravý	k2eAgMnSc4d1
plaza	plaz	k1gMnSc4
pod	pod	k7c7
jménem	jméno	k1gNnSc7
Iguanodon	Iguanodona	k1gFnPc2
(	(	kIx(
<g/>
„	„	k?
<g/>
leguání	leguánět	k5eAaImIp3nS
zub	zub	k1gInSc4
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tentokrát	tentokrát	k6eAd1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
už	už	k6eAd1
dostalo	dostat	k5eAaPmAgNnS
patřičného	patřičný	k2eAgNnSc2d1
uznání	uznání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1826	#num#	k4
byly	být	k5eAaImAgFnP
mimochodem	mimochodem	k9
fosilie	fosilie	k1gFnPc1
dinosaurů	dinosaurus	k1gMnPc2
objeveny	objevit	k5eAaPmNgFnP
poprvé	poprvé	k6eAd1
ve	v	k7c6
Francii	Francie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
145	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
roku	rok	k1gInSc2
1852	#num#	k4
(	(	kIx(
<g/>
kdy	kdy	k6eAd1
umírá	umírat	k5eAaImIp3nS
G.	G.	kA
Mantell	Mantell	k1gInSc1
<g/>
)	)	kIx)
bylo	být	k5eAaImAgNnS
známo	znám	k2eAgNnSc1d1
již	již	k9
devět	devět	k4xCc1
druhů	druh	k1gInPc2
dinosaurů	dinosaurus	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
výrazně	výrazně	k6eAd1
odlišovali	odlišovat	k5eAaImAgMnP
od	od	k7c2
současných	současný	k2eAgMnPc2d1
plazů	plaz	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
Sir	sir	k1gMnSc1
Richard	Richard	k1gMnSc1
Owen	Owen	k1gMnSc1
<g/>
,	,	kIx,
první	první	k4xOgMnSc1
ředitel	ředitel	k1gMnSc1
Britského	britský	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
přírodních	přírodní	k2eAgFnPc2d1
věd	věda	k1gFnPc2
<g/>
,	,	kIx,
navrhl	navrhnout	k5eAaPmAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
byli	být	k5eAaImAgMnP
zařazeni	zařadit	k5eAaPmNgMnP
do	do	k7c2
samostatné	samostatný	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
nazvané	nazvaný	k2eAgFnSc2d1
Dinosauria	Dinosaurium	k1gNnPc4
(	(	kIx(
<g/>
„	„	k?
<g/>
strašný	strašný	k2eAgMnSc1d1
ještěr	ještěr	k1gMnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sám	sám	k3xTgMnSc1
Mantell	Mantell	k1gMnSc1
již	již	k6eAd1
zkoumal	zkoumat	k5eAaImAgMnS
i	i	k9
stavbu	stavba	k1gFnSc4
fosilních	fosilní	k2eAgFnPc2d1
kostí	kost	k1gFnPc2
(	(	kIx(
<g/>
konkrétně	konkrétně	k6eAd1
u	u	k7c2
vývojově	vývojově	k6eAd1
primitivního	primitivní	k2eAgMnSc2d1
stegosaura	stegosaur	k1gMnSc2
rodu	rod	k1gInSc2
Regnosaurus	Regnosaurus	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
tak	tak	k9
jedním	jeden	k4xCgMnSc7
ze	z	k7c2
zakladatelů	zakladatel	k1gMnPc2
vědního	vědní	k2eAgInSc2d1
oboru	obor	k1gInSc2
paleohistologie	paleohistologie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
146	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vědecké	vědecký	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
Dinosauria	Dinosaurium	k1gNnSc2
bylo	být	k5eAaImAgNnS
poprvé	poprvé	k6eAd1
publikováno	publikován	k2eAgNnSc1d1
Richardem	Richard	k1gMnSc7
Owenem	Owen	k1gMnSc7
v	v	k7c6
dubnu	duben	k1gInSc6
roku	rok	k1gInSc2
1842	#num#	k4
<g/>
,	,	kIx,
když	když	k8xS
v	v	k7c6
Londýně	Londýn	k1gInSc6
publikoval	publikovat	k5eAaBmAgMnS
svoji	svůj	k3xOyFgFnSc4
druhou	druhý	k4xOgFnSc4
odbornou	odborný	k2eAgFnSc4d1
zprávu	zpráva	k1gFnSc4
o	o	k7c6
"	"	kIx"
<g/>
britských	britský	k2eAgInPc6d1
fosilních	fosilní	k2eAgInPc6d1
plazech	plaz	k1gInPc6
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústně	ústně	k6eAd1
toto	tento	k3xDgNnSc4
jméno	jméno	k1gNnSc4
zmínil	zmínit	k5eAaPmAgInS
již	již	k6eAd1
při	při	k7c6
jedné	jeden	k4xCgFnSc6
ze	z	k7c2
svých	svůj	k3xOyFgFnPc2
přednášek	přednáška	k1gFnPc2
o	o	k7c4
rok	rok	k1gInSc4
dříve	dříve	k6eAd2
<g/>
,	,	kIx,
ale	ale	k8xC
oficiálně	oficiálně	k6eAd1
platí	platit	k5eAaImIp3nS
právě	právě	k9
až	až	k9
od	od	k7c2
doby	doba	k1gFnSc2
písemné	písemný	k2eAgFnSc2d1
publikace	publikace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
147	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dinosauři	dinosaurus	k1gMnPc1
se	se	k3xPyFc4
jako	jako	k8xS,k8xC
skupina	skupina	k1gFnSc1
dostávají	dostávat	k5eAaImIp3nP
do	do	k7c2
obecného	obecný	k2eAgNnSc2d1
povědomí	povědomí	k1gNnSc2
laické	laický	k2eAgFnSc2d1
veřejnosti	veřejnost	k1gFnSc2
až	až	k9
po	po	k7c6
slavných	slavný	k2eAgInPc6d1
objevech	objev	k1gInPc6
na	na	k7c6
území	území	k1gNnSc6
Severní	severní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
,	,	kIx,
především	především	k9
v	v	k7c6
době	doba	k1gFnSc6
tzv.	tzv.	kA
války	válka	k1gFnSc2
o	o	k7c6
kosti	kost	k1gFnSc6
<g/>
,	,	kIx,
odehrávající	odehrávající	k2eAgMnSc1d1
se	se	k3xPyFc4
v	v	k7c6
poslední	poslední	k2eAgFnSc6d1
třetině	třetina	k1gFnSc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velkou	velký	k2eAgFnSc7d1
obrodou	obroda	k1gFnSc7
prošel	projít	k5eAaPmAgInS
pohled	pohled	k1gInSc1
na	na	k7c4
dinosaury	dinosaurus	k1gMnPc4
v	v	k7c6
období	období	k1gNnSc6
tzv.	tzv.	kA
Dinosauří	dinosauří	k2eAgFnSc2d1
renesance	renesance	k1gFnSc2
<g/>
,	,	kIx,
malé	malý	k2eAgFnSc2d1
vědecké	vědecký	k2eAgFnSc2d1
revoluce	revoluce	k1gFnSc2
na	na	k7c6
přelomu	přelom	k1gInSc6
60	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
70	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
148	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
149	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1960	#num#	k4
byly	být	k5eAaImAgFnP
na	na	k7c6
mrazivých	mrazivý	k2eAgInPc6d1
Špicberkách	Špicberky	k1gInPc6
objeveny	objevit	k5eAaPmNgInP
první	první	k4xOgInPc1
fosilní	fosilní	k2eAgInPc1d1
otisky	otisk	k1gInPc1
stop	stop	k2eAgMnPc2d1
ornitopodních	ornitopodní	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
staré	stará	k1gFnSc2
kolem	kolem	k7c2
125	#num#	k4
milionů	milion	k4xCgInPc2
let	léto	k1gNnPc2
(	(	kIx(
<g/>
raná	raný	k2eAgFnSc1d1
křída	křída	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
další	další	k2eAgInPc1d1
pak	pak	k6eAd1
byly	být	k5eAaImAgInP
odkryty	odkrýt	k5eAaPmNgInP
v	v	k7c6
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
a	a	k8xC
na	na	k7c6
začátku	začátek	k1gInSc6
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
objevy	objev	k1gInPc4
se	se	k3xPyFc4
významně	významně	k6eAd1
podílely	podílet	k5eAaImAgInP
na	na	k7c6
změně	změna	k1gFnSc6
pohledu	pohled	k1gInSc2
na	na	k7c4
dinosaury	dinosaurus	k1gMnPc4
v	v	k7c6
poslední	poslední	k2eAgFnSc6d1
třetině	třetina	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
150	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
jsou	být	k5eAaImIp3nP
již	již	k9
fosilie	fosilie	k1gFnPc1
dinosaurů	dinosaurus	k1gMnPc2
známé	známá	k1gFnSc2
prakticky	prakticky	k6eAd1
ze	z	k7c2
všech	všecek	k3xTgInPc2
kontinentů	kontinent	k1gInPc2
světa	svět	k1gInSc2
<g/>
,	,	kIx,
například	například	k6eAd1
i	i	k9
ze	z	k7c2
zaledněné	zaledněný	k2eAgFnSc2d1
Antarktidy	Antarktida	k1gFnSc2
(	(	kIx(
<g/>
například	například	k6eAd1
Ostrov	ostrov	k1gInSc1
Jamese	Jamese	k1gFnSc2
Rosse	Rosse	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
151	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1
doba	doba	k1gFnSc1
</s>
<s>
Zhruba	zhruba	k6eAd1
od	od	k7c2
poloviny	polovina	k1gFnSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
rozvíjí	rozvíjet	k5eAaImIp3nS
jako	jako	k9
nový	nový	k2eAgInSc4d1
obor	obor	k1gInSc4
paleohistologie	paleohistologie	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
umožnila	umožnit	k5eAaPmAgFnS
nový	nový	k2eAgInSc4d1
a	a	k8xC
přesnější	přesný	k2eAgInSc4d2
výzkum	výzkum	k1gInSc4
dinosaurů	dinosaurus	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
152	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
posledních	poslední	k2eAgNnPc6d1
desetiletích	desetiletí	k1gNnPc6
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
výraznému	výrazný	k2eAgInSc3d1
rozkvětu	rozkvět	k1gInSc3
a	a	k8xC
zintenzivnění	zintenzivnění	k1gNnSc4
ve	v	k7c6
výzkumu	výzkum	k1gInSc6
dinosaurů	dinosaurus	k1gMnPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
jak	jak	k6eAd1
vlivem	vliv	k1gInSc7
využívání	využívání	k1gNnSc2
nových	nový	k2eAgFnPc2d1
technologií	technologie	k1gFnPc2
a	a	k8xC
postupů	postup	k1gInPc2
výzkumu	výzkum	k1gInSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
i	i	k8xC
objevováním	objevování	k1gNnSc7
nových	nový	k2eAgFnPc2d1
paleontologických	paleontologický	k2eAgFnPc2d1
lokalit	lokalita	k1gFnPc2
nebo	nebo	k8xC
třeba	třeba	k6eAd1
zapojením	zapojení	k1gNnSc7
většího	veliký	k2eAgNnSc2d2
množství	množství	k1gNnSc2
výzkumných	výzkumný	k2eAgFnPc2d1
kapacit	kapacita	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
153	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
současnosti	současnost	k1gFnSc6
probíhá	probíhat	k5eAaImIp3nS
tzv.	tzv.	kA
Zlatá	zlatý	k2eAgFnSc1d1
éra	éra	k1gFnSc1
dinosauří	dinosauří	k2eAgFnSc2d1
paleontologie	paleontologie	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jsou	být	k5eAaImIp3nP
objevovány	objevován	k2eAgFnPc4d1
desítky	desítka	k1gFnPc4
nových	nový	k2eAgInPc2d1
druhů	druh	k1gInPc2
dinosaurů	dinosaurus	k1gMnPc2
každým	každý	k3xTgInSc7
rokem	rok	k1gInSc7
a	a	k8xC
informací	informace	k1gFnSc7
o	o	k7c6
této	tento	k3xDgFnSc6
pravěké	pravěký	k2eAgFnSc6d1
skupině	skupina	k1gFnSc6
živočichů	živočich	k1gMnPc2
takřka	takřka	k6eAd1
raketovým	raketový	k2eAgNnSc7d1
tempem	tempo	k1gNnSc7
přibývá	přibývat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
jen	jen	k9
za	za	k7c4
rok	rok	k1gInSc4
2019	#num#	k4
bylo	být	k5eAaImAgNnS
popsáno	popsat	k5eAaPmNgNnS
rovných	rovný	k2eAgInPc2d1
50	#num#	k4
nových	nový	k2eAgInPc2d1
druhů	druh	k1gInPc2
druhohorních	druhohorní	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
(	(	kIx(
<g/>
z	z	k7c2
toho	ten	k3xDgNnSc2
49	#num#	k4
nových	nový	k2eAgInPc2d1
rodů	rod	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tedy	tedy	k8xC
zhruba	zhruba	k6eAd1
jeden	jeden	k4xCgInSc1
nový	nový	k2eAgInSc1d1
druh	druh	k1gInSc1
každý	každý	k3xTgInSc4
týden	týden	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
154	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Klasifikace	klasifikace	k1gFnSc1
</s>
<s>
Počet	počet	k1gInSc1
dinosaurů	dinosaurus	k1gMnPc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Seznam	seznam	k1gInSc1
dinosaurů	dinosaurus	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Dinosauři	dinosaurus	k1gMnPc1
jsou	být	k5eAaImIp3nP
často	často	k6eAd1
označování	označování	k1gNnSc1
jako	jako	k8xS,k8xC
pravěcí	pravěký	k2eAgMnPc1d1
(	(	kIx(
<g/>
vele	velo	k1gNnSc6
<g/>
)	)	kIx)
<g/>
ještěři	ještěr	k1gMnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
mají	mít	k5eAaImIp3nP
k	k	k7c3
ještěrům	ještěr	k1gMnPc3
vývojově	vývojově	k6eAd1
i	i	k9
fyziologicky	fyziologicky	k6eAd1
daleko	daleko	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dinosauři	dinosaurus	k1gMnPc1
byli	být	k5eAaImAgMnP
mnohem	mnohem	k6eAd1
vyspělejší	vyspělý	k2eAgFnSc7d2
skupinou	skupina	k1gFnSc7
živočichů	živočich	k1gMnPc2
<g/>
,	,	kIx,
než	než	k8xS
jsou	být	k5eAaImIp3nP
zástupci	zástupce	k1gMnPc1
kladu	klad	k1gInSc2
Lepidosauria	Lepidosaurium	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
155	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
roku	rok	k1gInSc3
2010	#num#	k4
je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
již	již	k9
přes	přes	k7c4
700	#num#	k4
platných	platný	k2eAgInPc2d1
rodů	rod	k1gInPc2
(	(	kIx(
<g/>
a	a	k8xC
kolem	kolem	k7c2
1	#num#	k4
500	#num#	k4
druhů	druh	k1gInPc2
<g/>
)	)	kIx)
dinosaurů	dinosaurus	k1gMnPc2
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
je	být	k5eAaImIp3nS
jisté	jistý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
zdaleka	zdaleka	k6eAd1
nejde	jít	k5eNaImIp3nS
o	o	k7c4
konečná	konečný	k2eAgNnPc4d1
čísla	číslo	k1gNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
156	#num#	k4
<g/>
]	]	kIx)
Většina	většina	k1gFnSc1
odhadů	odhad	k1gInPc2
skutečného	skutečný	k2eAgInSc2d1
počtu	počet	k1gInSc2
dinosaurů	dinosaurus	k1gMnPc2
se	se	k3xPyFc4
v	v	k7c6
současnosti	současnost	k1gFnSc6
pohybuje	pohybovat	k5eAaImIp3nS
kolem	kolem	k7c2
2	#num#	k4
000	#num#	k4
až	až	k9
20	#num#	k4
000	#num#	k4
druhů	druh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
byla	být	k5eAaImAgFnS
však	však	k9
publikována	publikován	k2eAgFnSc1d1
studie	studie	k1gFnSc1
<g/>
,	,	kIx,
podle	podle	k7c2
které	který	k3yIgFnSc2,k3yQgFnSc2,k3yRgFnSc2
známe	znát	k5eAaImIp1nP
dnes	dnes	k6eAd1
527	#num#	k4
platných	platný	k2eAgInPc2d1
rodů	rod	k1gInPc2
a	a	k8xC
celkem	celkem	k6eAd1
jich	on	k3xPp3gMnPc2
existovalo	existovat	k5eAaImAgNnS
asi	asi	k9
1	#num#	k4
844	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
odhad	odhad	k1gInSc1
je	být	k5eAaImIp3nS
však	však	k9
velmi	velmi	k6eAd1
pochybný	pochybný	k2eAgMnSc1d1
(	(	kIx(
<g/>
nemluvě	nemluva	k1gFnSc3
o	o	k7c6
zarážející	zarážející	k2eAgFnSc6d1
„	„	k?
<g/>
přesnosti	přesnost	k1gFnSc6
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
odhadů	odhad	k1gInPc2
většiny	většina	k1gFnSc2
odborníků	odborník	k1gMnPc2
totiž	totiž	k9
rozhodně	rozhodně	k6eAd1
neznáme	neznat	k5eAaImIp1nP,k5eNaImIp1nP
více	hodně	k6eAd2
než	než	k8xS
pětinu	pětina	k1gFnSc4
všech	všecek	k3xTgInPc2
kdysi	kdysi	k6eAd1
existujících	existující	k2eAgInPc2d1
rodů	rod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajímavé	zajímavý	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
počty	počet	k1gInPc1
Američana	Američan	k1gMnSc2
George	Georg	k1gFnSc2
Olshevského	Olshevský	k2eAgInSc2d1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
uvádí	uvádět	k5eAaImIp3nS
až	až	k9
220	#num#	k4
000	#num#	k4
druhů	druh	k1gInPc2
(	(	kIx(
<g/>
nikoli	nikoli	k9
rodů	rod	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Olshevsky	Olshevsko	k1gNnPc7
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
každých	každý	k3xTgInPc2
4,5	4,5	k4
milionu	milion	k4xCgInSc2
let	léto	k1gNnPc2
se	se	k3xPyFc4
v	v	k7c6
průměru	průměr	k1gInSc6
vymění	vyměnit	k5eAaPmIp3nS
jedna	jeden	k4xCgFnSc1
megafauna	megafauna	k1gFnSc1
s	s	k7c7
počtem	počet	k1gInSc7
6	#num#	k4
000	#num#	k4
druhů	druh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
165	#num#	k4
milionů	milion	k4xCgInPc2
let	léto	k1gNnPc2
by	by	kYmCp3nS
se	se	k3xPyFc4
tedy	tedy	k9
vystřídalo	vystřídat	k5eAaPmAgNnS
celkem	celkem	k6eAd1
asi	asi	k9
220	#num#	k4
000	#num#	k4
druhů	druh	k1gInPc2
(	(	kIx(
<g/>
165	#num#	k4
000	#num#	k4
000	#num#	k4
/	/	kIx~
4	#num#	k4
500	#num#	k4
000	#num#	k4
=	=	kIx~
37	#num#	k4
*	*	kIx~
6	#num#	k4
000	#num#	k4
=	=	kIx~
220	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
však	však	k9
jen	jen	k9
o	o	k7c4
spekulace	spekulace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
nijak	nijak	k6eAd1
nedají	dát	k5eNaPmIp3nP
dokázat	dokázat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společně	společně	k6eAd1
s	s	k7c7
ptáky	pták	k1gMnPc7
(	(	kIx(
<g/>
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
patří	patřit	k5eAaImIp3nP
mezi	mezi	k7c7
dinosaury	dinosaurus	k1gMnPc7
do	do	k7c2
podřádu	podřád	k1gInSc2
Theropoda	Theropoda	k1gFnSc1
<g/>
)	)	kIx)
roste	růst	k5eAaImIp3nS
celkový	celkový	k2eAgInSc1d1
počet	počet	k1gInSc1
známých	známý	k2eAgInPc2d1
rodů	rod	k1gInPc2
na	na	k7c4
zhruba	zhruba	k6eAd1
10	#num#	k4
000	#num#	k4
(	(	kIx(
<g/>
z	z	k7c2
toho	ten	k3xDgNnSc2
9	#num#	k4
600	#num#	k4
tvoří	tvořit	k5eAaImIp3nP
dnešní	dnešní	k2eAgMnPc1d1
ptáci	pták	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
statisíce	statisíce	k1gInPc1
rodů	rod	k1gInPc2
by	by	kYmCp3nP
pak	pak	k6eAd1
tvořili	tvořit	k5eAaImAgMnP
fosilní	fosilní	k2eAgMnPc1d1
ptáci	pták	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novější	nový	k2eAgFnPc4d2
studie	studie	k1gFnPc4
z	z	k7c2
roku	rok	k1gInSc2
2016	#num#	k4
uvádí	uvádět	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
celkem	celkem	k6eAd1
mohlo	moct	k5eAaImAgNnS
v	v	k7c6
průběhu	průběh	k1gInSc6
druhohor	druhohory	k1gFnPc2
existovat	existovat	k5eAaImF
asi	asi	k9
1500	#num#	k4
<g/>
–	–	k?
<g/>
2500	#num#	k4
druhů	druh	k1gInPc2
neptačích	ptačí	k2eNgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
157	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
červenci	červenec	k1gInSc3
roku	rok	k1gInSc2
2019	#num#	k4
jich	on	k3xPp3gInPc2
je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
přibližně	přibližně	k6eAd1
1350	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
158	#num#	k4
<g/>
]	]	kIx)
Ukazuje	ukazovat	k5eAaImIp3nS
<g />
.	.	kIx.
</s>
<s hack="1">
se	se	k3xPyFc4
však	však	k9
<g/>
,	,	kIx,
že	že	k8xS
odhady	odhad	k1gInPc1
dinosauří	dinosauří	k2eAgFnSc2d1
biodiverzity	biodiverzita	k1gFnSc2
významně	významně	k6eAd1
snižuje	snižovat	k5eAaImIp3nS
výběrovost	výběrovost	k1gFnSc1
objevů	objev	k1gInPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
z	z	k7c2
určitých	určitý	k2eAgNnPc2d1
období	období	k1gNnPc2
a	a	k8xC
regionů	region	k1gInPc2
nemáme	mít	k5eNaImIp1nP
dostatečně	dostatečně	k6eAd1
kompletní	kompletní	k2eAgInSc4d1
fosilní	fosilní	k2eAgInSc4d1
záznam	záznam	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
159	#num#	k4
<g/>
]	]	kIx)
Odhady	odhad	k1gInPc1
druhové	druhový	k2eAgFnSc2d1
rozmanitosti	rozmanitost	k1gFnSc2
dinosaurů	dinosaurus	k1gMnPc2
však	však	k9
může	moct	k5eAaImIp3nS
silně	silně	k6eAd1
ovlivňovat	ovlivňovat	k5eAaImF
nejistota	nejistota	k1gFnSc1
ohledně	ohledně	k7c2
tvarové	tvarový	k2eAgFnSc2d1
variace	variace	k1gFnSc2
jednotlivých	jednotlivý	k2eAgFnPc2d1
částí	část	k1gFnPc2
kostry	kostra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnohé	mnohý	k2eAgInPc1d1
domněle	domněle	k6eAd1
odlišné	odlišný	k2eAgInPc1d1
rody	rod	k1gInPc1
a	a	k8xC
druhy	druh	k1gInPc1
totiž	totiž	k9
mohou	moct	k5eAaImIp3nP
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
představovat	představovat	k5eAaImF
jen	jen	k9
odlišně	odlišně	k6eAd1
disponované	disponovaný	k2eAgNnSc1d1
(	(	kIx(
<g/>
morfologicky	morfologicky	k6eAd1
variabilní	variabilní	k2eAgInSc1d1
<g/>
)	)	kIx)
jedince	jedinko	k6eAd1
stejného	stejný	k2eAgInSc2d1
druhu	druh	k1gInSc2
či	či	k8xC
rodu	rod	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
160	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dinosauři	dinosaurus	k1gMnPc1
byli	být	k5eAaImAgMnP
rozšíření	rozšíření	k1gNnSc4
prakticky	prakticky	k6eAd1
na	na	k7c6
všech	všecek	k3xTgInPc6
kontinentech	kontinent	k1gInPc6
druhohorní	druhohorní	k2eAgFnSc2d1
éry	éra	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc2
fosilie	fosilie	k1gFnSc2
dnes	dnes	k6eAd1
známe	znát	k5eAaImIp1nP
z	z	k7c2
oblastí	oblast	k1gFnPc2
Špicberk	Špicberky	k1gFnPc2
<g/>
,	,	kIx,
Antarktidy	Antarktida	k1gFnSc2
nebo	nebo	k8xC
i	i	k8xC
Tibetu	Tibet	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
161	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podle	podle	k7c2
údajů	údaj	k1gInPc2
skotského	skotský	k2eAgMnSc2d1
paleontologa	paleontolog	k1gMnSc2
Michaela	Michael	k1gMnSc2
Bentona	Benton	k1gMnSc2
bylo	být	k5eAaImAgNnS
k	k	k7c3
září	září	k1gNnSc3
2008	#num#	k4
popsáno	popsat	k5eAaPmNgNnS
již	již	k9
1	#num#	k4
047	#num#	k4
druhů	druh	k1gInPc2
dinosaurů	dinosaurus	k1gMnPc2
<g/>
,	,	kIx,
pouze	pouze	k6eAd1
asi	asi	k9
500	#num#	k4
z	z	k7c2
nich	on	k3xPp3gMnPc2
je	být	k5eAaImIp3nS
však	však	k9
považováno	považován	k2eAgNnSc1d1
za	za	k7c4
platné	platný	k2eAgInPc4d1
(	(	kIx(
<g/>
validní	validní	k2eAgInPc4d1
<g/>
)	)	kIx)
taxony	taxon	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
162	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rozvoji	rozvoj	k1gInSc3
dinosaurů	dinosaurus	k1gMnPc2
napomohlo	napomoct	k5eAaPmAgNnS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
že	že	k8xS
přečkali	přečkat	k5eAaPmAgMnP
hromadné	hromadný	k2eAgNnSc4d1
vymírání	vymírání	k1gNnSc4
na	na	k7c6
konci	konec	k1gInSc6
triasu	trias	k1gInSc2
před	před	k7c7
201	#num#	k4
miliony	milion	k4xCgInPc7
lety	léto	k1gNnPc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
vymřeli	vymřít	k5eAaPmAgMnP
hlavní	hlavní	k2eAgMnPc1d1
konkurenti	konkurent	k1gMnPc1
dinosaurů	dinosaurus	k1gMnPc2
v	v	k7c6
triasu	trias	k1gInSc6
–	–	k?
„	„	k?
<g/>
krurotarsánští	krurotarsánský	k2eAgMnPc1d1
archosauři	archosaur	k1gMnPc1
<g/>
“	“	k?
(	(	kIx(
<g/>
archosauři	archosauř	k1gMnSc3
skupiny	skupina	k1gFnSc2
Crurotarsi	Crurotarse	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
163	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Podle	podle	k7c2
analýzy	analýza	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
2015	#num#	k4
z	z	k7c2
dosavadních	dosavadní	k2eAgInPc2d1
nálezů	nález	k1gInPc2
za	za	k7c2
posledních	poslední	k2eAgNnPc2d1
200	#num#	k4
let	léto	k1gNnPc2
nelze	lze	k6eNd1
přesněji	přesně	k6eAd2
určit	určit	k5eAaPmF
celkový	celkový	k2eAgInSc4d1
počet	počet	k1gInSc4
druhů	druh	k1gInPc2
dinosaurů	dinosaurus	k1gMnPc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
kdy	kdy	k6eAd1
žil	žít	k5eAaImAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
164	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
165	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
21	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
je	být	k5eAaImIp3nS
objevováno	objevovat	k5eAaImNgNnS
nejvíce	nejvíce	k6eAd1,k6eAd3
nových	nový	k2eAgInPc2d1
druhů	druh	k1gInPc2
dinosaurů	dinosaurus	k1gMnPc2
v	v	k7c4
historii	historie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zhruba	zhruba	k6eAd1
za	za	k7c4
prvních	první	k4xOgInPc2
20	#num#	k4
let	let	k1gInSc4
tohoto	tento	k3xDgNnSc2
století	století	k1gNnSc2
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
bylo	být	k5eAaImAgNnS
objeveno	objevit	k5eAaPmNgNnS
více	hodně	k6eAd2
než	než	k8xS
63	#num#	k4
%	%	kIx~
všech	všecek	k3xTgInPc2
známých	známý	k2eAgInPc2d1
druhů	druh	k1gInPc2
dinosaurů	dinosaurus	k1gMnPc2
(	(	kIx(
<g/>
674	#num#	k4
z	z	k7c2
1067	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
k	k	k7c3
roku	rok	k1gInSc3
2019	#num#	k4
od	od	k7c2
doby	doba	k1gFnSc2
prvního	první	k4xOgMnSc2
formálního	formální	k2eAgMnSc2d1
popisu	popis	k1gInSc6
dinosaura	dinosaurus	k1gMnSc2
(	(	kIx(
<g/>
Megalosaurus	Megalosaurus	k1gMnSc1
<g/>
,	,	kIx,
1824	#num#	k4
<g/>
)	)	kIx)
uplynulo	uplynout	k5eAaPmAgNnS
již	již	k9
195	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
desetiletí	desetiletí	k1gNnSc4
2010	#num#	k4
až	až	k8xS
2019	#num#	k4
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
k	k	k7c3
červenci	červenec	k1gInSc3
2019	#num#	k4
celkem	celek	k1gInSc7
404	#num#	k4
druhů	druh	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
představuje	představovat	k5eAaImIp3nS
téměř	téměř	k6eAd1
38	#num#	k4
%	%	kIx~
z	z	k7c2
celkového	celkový	k2eAgInSc2d1
počtu	počet	k1gInSc2
všech	všecek	k3xTgInPc2
dosud	dosud	k6eAd1
objevených	objevený	k2eAgInPc2d1
druhů	druh	k1gInPc2
druhohorních	druhohorní	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
166	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Státy	stát	k1gInPc1
a	a	k8xC
kontinenty	kontinent	k1gInPc1
s	s	k7c7
nejvíce	hodně	k6eAd3,k6eAd1
dinosauřími	dinosauří	k2eAgFnPc7d1
fosiliemi	fosilie	k1gFnPc7
</s>
<s>
Celkový	celkový	k2eAgInSc1d1
počet	počet	k1gInSc1
známých	známý	k2eAgInPc2d1
platných	platný	k2eAgInPc2d1
druhů	druh	k1gInPc2
dinosaurů	dinosaurus	k1gMnPc2
činí	činit	k5eAaImIp3nS
k	k	k7c3
červenci	červenec	k1gInSc3
2020	#num#	k4
zhruba	zhruba	k6eAd1
1380	#num#	k4
až	až	k9
1390	#num#	k4
(	(	kIx(
<g/>
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
vědecké	vědecký	k2eAgFnSc6d1
platnosti	platnost	k1gFnSc6
několika	několik	k4yIc2
sporných	sporný	k2eAgInPc2d1
taxonů	taxon	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Státem	stát	k1gInSc7
s	s	k7c7
nejvíce	hodně	k6eAd3,k6eAd1
platnými	platný	k2eAgInPc7d1
dinosauřími	dinosauří	k2eAgInPc7d1
druhy	druh	k1gInPc7
je	být	k5eAaImIp3nS
Čína	Čína	k1gFnSc1
s	s	k7c7
308	#num#	k4
druhy	druh	k1gInPc7
(	(	kIx(
<g/>
22,37	22,37	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dále	daleko	k6eAd2
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
americké	americký	k2eAgInPc4d1
s	s	k7c7
280	#num#	k4
druhy	druh	k1gInPc7
(	(	kIx(
<g/>
20,33	20,33	k4
%	%	kIx~
<g/>
)	)	kIx)
a	a	k8xC
na	na	k7c6
třetím	třetí	k4xOgNnSc6
místě	místo	k1gNnSc6
Argentina	Argentina	k1gFnSc1
se	s	k7c7
147	#num#	k4
druhy	druh	k1gInPc7
(	(	kIx(
<g/>
10,68	10,68	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následuje	následovat	k5eAaImIp3nS
Mongolsko	Mongolsko	k1gNnSc4
s	s	k7c7
94	#num#	k4
druhy	druh	k1gInPc7
(	(	kIx(
<g/>
6,83	6,83	k4
%	%	kIx~
<g/>
)	)	kIx)
a	a	k8xC
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
s	s	k7c7
84	#num#	k4
druhy	druh	k1gInPc7
(	(	kIx(
<g/>
6,10	6,10	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kanada	Kanada	k1gFnSc1
má	mít	k5eAaImIp3nS
ze	z	k7c2
svého	svůj	k3xOyFgNnSc2
území	území	k1gNnSc2
popsáno	popsat	k5eAaPmNgNnS
79	#num#	k4
druhů	druh	k1gInPc2
(	(	kIx(
<g/>
5,74	5,74	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k1gNnSc1
v	v	k7c6
pořadí	pořadí	k1gNnSc6
je	být	k5eAaImIp3nS
Brazílie	Brazílie	k1gFnSc1
s	s	k7c7
31	#num#	k4
druhy	druh	k1gInPc7
(	(	kIx(
<g/>
2,25	2,25	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Jihoafrická	jihoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
s	s	k7c7
29	#num#	k4
druhy	druh	k1gInPc7
(	(	kIx(
<g/>
2,11	2,11	k4
%	%	kIx~
<g/>
)	)	kIx)
a	a	k8xC
Indie	Indie	k1gFnSc1
se	s	k7c7
Španělskem	Španělsko	k1gNnSc7
s	s	k7c7
27	#num#	k4
druhy	druh	k1gInPc7
(	(	kIx(
<g/>
1,96	1,96	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Francie	Francie	k1gFnSc1
má	mít	k5eAaImIp3nS
26	#num#	k4
druhů	druh	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
odpovídá	odpovídat	k5eAaImIp3nS
1,89	1,89	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
rámci	rámec	k1gInSc6
kontinentů	kontinent	k1gInPc2
dominuje	dominovat	k5eAaImIp3nS
Asie	Asie	k1gFnSc1
s	s	k7c7
500	#num#	k4
druhy	druh	k1gInPc7
(	(	kIx(
<g/>
36,3	36,3	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
následuje	následovat	k5eAaImIp3nS
Severní	severní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
s	s	k7c7
362	#num#	k4
druhy	druh	k1gInPc7
(	(	kIx(
<g/>
26,3	26,3	k4
%	%	kIx~
<g/>
)	)	kIx)
a	a	k8xC
dále	daleko	k6eAd2
Evropa	Evropa	k1gFnSc1
s	s	k7c7
207	#num#	k4
druhy	druh	k1gInPc7
(	(	kIx(
<g/>
15,2	15,2	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jižní	jižní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
má	mít	k5eAaImIp3nS
druhů	druh	k1gInPc2
185	#num#	k4
(	(	kIx(
<g/>
13,1	13,1	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Afrika	Afrika	k1gFnSc1
96	#num#	k4
(	(	kIx(
<g/>
7,0	7,0	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Austrálie	Austrálie	k1gFnSc1
23	#num#	k4
(	(	kIx(
<g/>
1,7	1,7	k4
%	%	kIx~
<g/>
)	)	kIx)
a	a	k8xC
Antarktida	Antarktida	k1gFnSc1
zatím	zatím	k6eAd1
6	#num#	k4
(	(	kIx(
<g/>
0,4	0,4	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
poměru	poměr	k1gInSc6
ke	k	k7c3
své	svůj	k3xOyFgFnSc3
rozloze	rozloha	k1gFnSc3
však	však	k9
má	mít	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgFnSc4d3
hustotu	hustota	k1gFnSc4
objevů	objev	k1gInPc2
Evropa	Evropa	k1gFnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
tento	tento	k3xDgInSc1
průměr	průměr	k1gInSc1
činí	činit	k5eAaImIp3nS
1	#num#	k4
druh	druh	k1gInSc1
na	na	k7c4
49	#num#	k4
197	#num#	k4
km²	km²	k?
(	(	kIx(
<g/>
z	z	k7c2
jednotlivých	jednotlivý	k2eAgInPc2d1
států	stát	k1gInPc2
pak	pak	k6eAd1
dominuje	dominovat	k5eAaImIp3nS
Velká	velký	k2eAgFnSc1d1
Británie	Británie	k1gFnSc1
s	s	k7c7
1	#num#	k4
druhem	druh	k1gInSc7
na	na	k7c4
2887	#num#	k4
km²	km²	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
v	v	k7c6
Rusku	Rusko	k1gNnSc6
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
jen	jen	k9
1	#num#	k4
druh	druh	k1gInSc1
na	na	k7c4
víc	hodně	k6eAd2
než	než	k8xS
1,4	1,4	k4
milionu	milion	k4xCgInSc2
km²	km²	k?
a	a	k8xC
v	v	k7c6
Antarktidě	Antarktida	k1gFnSc6
dokonce	dokonce	k9
1	#num#	k4
na	na	k7c4
2,3	2,3	k4
milionu	milion	k4xCgInSc2
km²	km²	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
poměru	poměr	k1gInSc6
mezi	mezi	k7c7
plazopánvými	plazopánvý	k2eAgMnPc7d1
a	a	k8xC
ptakopánvými	ptakopánvý	k2eAgMnPc7d1
dinosaury	dinosaurus	k1gMnPc7
převažují	převažovat	k5eAaImIp3nP
početně	početně	k6eAd1
první	první	k4xOgMnPc1
jmenovaní	jmenovaný	k1gMnPc1
859	#num#	k4
:	:	kIx,
520	#num#	k4
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
odpovídá	odpovídat	k5eAaImIp3nS
poměru	poměra	k1gFnSc4
62,29	62,29	k4
%	%	kIx~
ku	k	k7c3
37,71	37,71	k4
%	%	kIx~
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvíce	hodně	k6eAd3,k6eAd1
bylo	být	k5eAaImAgNnS
popsáno	popsat	k5eAaPmNgNnS
teropodních	teropodní	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
(	(	kIx(
<g/>
486	#num#	k4
–	–	k?
35,2	35,2	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
dále	daleko	k6eAd2
sauropodomorfů	sauropodomorf	k1gInPc2
(	(	kIx(
<g/>
373	#num#	k4
–	–	k?
27,0	27,0	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ornitopodů	ornitopod	k1gInPc2
(	(	kIx(
<g/>
216	#num#	k4
–	–	k?
15,7	15,7	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rohatých	rohatý	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
(	(	kIx(
<g/>
114	#num#	k4
–	–	k?
8,3	8,3	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ankylosaurů	ankylosaur	k1gInPc2
(	(	kIx(
<g/>
95	#num#	k4
–	–	k?
6,9	6,9	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
stegosaurů	stegosaur	k1gInPc2
(	(	kIx(
<g/>
28	#num#	k4
–	–	k?
2,0	2,0	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pachycefalosaurů	pachycefalosaur	k1gInPc2
(	(	kIx(
<g/>
22	#num#	k4
–	–	k?
1,6	1,6	k4
%	%	kIx~
<g/>
)	)	kIx)
a	a	k8xC
heterodontosaurů	heterodontosaur	k1gInPc2
(	(	kIx(
<g/>
8	#num#	k4
–	–	k?
0,6	0,6	k4
%	%	kIx~
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbylých	zbylý	k2eAgInPc2d1
37	#num#	k4
druhů	druh	k1gInPc2
nelze	lze	k6eNd1
přesně	přesně	k6eAd1
taxonomicky	taxonomicky	k6eAd1
zařadit	zařadit	k5eAaPmF
<g/>
,	,	kIx,
většinou	většinou	k6eAd1
se	se	k3xPyFc4
však	však	k9
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
vývojově	vývojově	k6eAd1
pokročilé	pokročilý	k2eAgMnPc4d1
ptakopánvé	ptakopánvý	k2eAgMnPc4d1
dinosaury	dinosaurus	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
167	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Již	již	k6eAd1
v	v	k7c6
červnu	červen	k1gInSc6
roku	rok	k1gInSc2
2019	#num#	k4
bylo	být	k5eAaImAgNnS
z	z	k7c2
Číny	Čína	k1gFnSc2
popsáno	popsat	k5eAaPmNgNnS
rovných	rovný	k2eAgMnPc2d1
300	#num#	k4
platně	platně	k6eAd1
popsaných	popsaný	k2eAgInPc2d1
druhů	druh	k1gInPc2
dinosaurů	dinosaurus	k1gMnPc2
a	a	k8xC
zůstává	zůstávat	k5eAaImIp3nS
tak	tak	k6eAd1
nejbohatší	bohatý	k2eAgFnSc7d3
zemí	zem	k1gFnSc7
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
se	se	k3xPyFc4
fosilních	fosilní	k2eAgInPc2d1
objevů	objev	k1gInPc2
této	tento	k3xDgFnSc2
skupiny	skupina	k1gFnSc2
týká	týkat	k5eAaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
168	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Systém	systém	k1gInSc1
</s>
<s>
PlazopánvíPtakopánví	PlazopánvíPtakopánvit	k5eAaPmIp3nS,k5eAaImIp3nS
</s>
<s>
Dinosauři	dinosaurus	k1gMnPc1
patří	patřit	k5eAaImIp3nP
do	do	k7c2
kladů	klad	k1gInPc2
Dinosauromorpha	Dinosauromorph	k1gMnSc2
<g/>
,	,	kIx,
Dinosauriformes	Dinosauriformes	k1gInSc4
a	a	k8xC
Dracohors	Dracohors	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nadřád	nadřád	k1gInSc4
dinosauři	dinosaurus	k1gMnPc1
(	(	kIx(
<g/>
Dinosauria	Dinosaurium	k1gNnPc1
Owen	Owena	k1gFnPc2
<g/>
,	,	kIx,
1842	#num#	k4
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
zřejmě	zřejmě	k6eAd1
přirozenou	přirozený	k2eAgFnSc7d1
monofyletickou	monofyletický	k2eAgFnSc7d1
skupinou	skupina	k1gFnSc7
<g/>
,	,	kIx,
sdílející	sdílející	k2eAgFnSc7d1
společného	společný	k2eAgMnSc4d1
předka	předek	k1gMnSc4
a	a	k8xC
tradičně	tradičně	k6eAd1
se	se	k3xPyFc4
dělí	dělit	k5eAaImIp3nS
na	na	k7c4
dva	dva	k4xCgInPc4
morfologicky	morfologicky	k6eAd1
dobře	dobře	k6eAd1
vymezené	vymezený	k2eAgInPc4d1
řády	řád	k1gInPc4
–	–	k?
Saurischia	Saurischia	k1gFnSc1
(	(	kIx(
<g/>
=	=	kIx~
plazopánví	plazopánví	k1gNnSc1
<g/>
;	;	kIx,
Seeley	Seeley	k1gInPc1
<g/>
,	,	kIx,
1887	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Ornithischia	Ornithischia	k1gFnSc1
(	(	kIx(
<g/>
=	=	kIx~
ptakopánví	ptakopánví	k1gNnSc1
<g/>
;	;	kIx,
Seeley	Seeley	k1gInPc1
<g/>
,	,	kIx,
1888	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
těchto	tento	k3xDgInPc2
dvou	dva	k4xCgInPc2
řádů	řád	k1gInPc2
jsou	být	k5eAaImIp3nP
dinosauři	dinosaurus	k1gMnPc1
řazeni	řazen	k2eAgMnPc1d1
na	na	k7c6
základě	základ	k1gInSc6
stavby	stavba	k1gFnSc2
pánve	pánev	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
169	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Řád	řád	k1gInSc1
Saurischia	Saurischium	k1gNnSc2
zahrnuje	zahrnovat	k5eAaImIp3nS
dva	dva	k4xCgInPc4
podřády	podřád	k1gInPc4
–	–	k?
Theropoda	Theropoda	k1gFnSc1
<g/>
,	,	kIx,
mezi	mezi	k7c4
které	který	k3yIgInPc4,k3yRgInPc4,k3yQgInPc4
patří	patřit	k5eAaImIp3nP
všichni	všechen	k3xTgMnPc1
masožraví	masožravý	k2eAgMnPc1d1
dinosauři	dinosaurus	k1gMnPc1
<g/>
;	;	kIx,
a	a	k8xC
Sauropodomorpha	Sauropodomorpha	k1gFnSc1
<g/>
,	,	kIx,
mezi	mezi	k7c4
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
patří	patřit	k5eAaImIp3nP
sauropodi	sauropod	k1gMnPc1
(	(	kIx(
<g/>
Sauropoda	Sauropoda	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
největší	veliký	k2eAgMnPc1d3
suchozemští	suchozemský	k2eAgMnPc1d1
živočichové	živočich	k1gMnPc1
všech	všecek	k3xTgFnPc2
dob	doba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Řád	řád	k1gInSc1
Ornithischia	Ornithischium	k1gNnSc2
zahrnuje	zahrnovat	k5eAaImIp3nS
dva	dva	k4xCgInPc4
podřády	podřád	k1gInPc4
–	–	k?
Thyreophora	Thyreophora	k1gFnSc1
<g/>
,	,	kIx,
mezi	mezi	k7c4
které	který	k3yQgInPc4,k3yIgInPc4,k3yRgInPc4
patří	patřit	k5eAaImIp3nP
všichni	všechen	k3xTgMnPc1
obrnění	obrněný	k2eAgMnPc1d1
dinosauři	dinosaurus	k1gMnPc1
a	a	k8xC
Cerapoda	Cerapoda	k1gFnSc1
<g/>
,	,	kIx,
mezi	mezi	k7c4
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
patří	patřit	k5eAaImIp3nS
heterodontosauriformové	heterodontosauriformový	k2eAgNnSc1d1
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc1
vývoj	vývoj	k1gInSc1
kulminoval	kulminovat	k5eAaImAgInS
pestrou	pestrý	k2eAgFnSc7d1
faunou	fauna	k1gFnSc7
svrchní	svrchní	k2eAgFnSc2d1
křídy	křída	k1gFnSc2
a	a	k8xC
ornitopodi	ornitopod	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
zahrnovali	zahrnovat	k5eAaImAgMnP
veleúspěšné	veleúspěšný	k2eAgFnPc4d1
hadrosauroidy	hadrosauroida	k1gFnPc4
a	a	k8xC
iguanodontidy	iguanodontida	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
však	však	k9
publikovala	publikovat	k5eAaBmAgFnS
trojice	trojice	k1gFnSc1
paleontologů	paleontolog	k1gMnPc2
nový	nový	k2eAgInSc4d1
pohled	pohled	k1gInSc4
na	na	k7c4
systematiku	systematika	k1gFnSc4
dinosaurů	dinosaurus	k1gMnPc2
<g/>
,	,	kIx,
ze	z	k7c2
kterého	který	k3yRgInSc2,k3yQgInSc2,k3yIgInSc2
by	by	kYmCp3nS
vyplývalo	vyplývat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
tradiční	tradiční	k2eAgNnSc1d1
dělení	dělení	k1gNnSc1
je	být	k5eAaImIp3nS
zcela	zcela	k6eAd1
nesprávné	správný	k2eNgNnSc1d1
a	a	k8xC
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
jsou	být	k5eAaImIp3nP
teropodi	teropod	k1gMnPc1
blíže	blízce	k6eAd2
příbuzní	příbuzný	k2eAgMnPc1d1
ptakopánvým	ptakopánvý	k2eAgMnPc3d1
dinosaurům	dinosaurus	k1gMnPc3
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
mezi	mezi	k7c4
plazopánvé	plazopánvá	k1gFnPc4
by	by	kYmCp3nS
již	již	k9
byli	být	k5eAaImAgMnP
řazeni	řazen	k2eAgMnPc1d1
pouze	pouze	k6eAd1
sauropodomorfové	sauropodomorf	k1gMnPc1
a	a	k8xC
herrerasauridi	herrerasaurid	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
170	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
171	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
172	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
173	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
novější	nový	k2eAgFnSc2d2
systematiky	systematika	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
však	však	k9
není	být	k5eNaImIp3nS
plně	plně	k6eAd1
uznávána	uznáván	k2eAgFnSc1d1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
dinosauři	dinosaurus	k1gMnPc1
děleni	dělit	k5eAaImNgMnP
takto	takto	k6eAd1
<g/>
:	:	kIx,
</s>
<s>
Dinosauria	Dinosaurium	k1gNnPc1
–	–	k?
dinosauři	dinosaurus	k1gMnPc1
</s>
<s>
Ornithoscelida	Ornithoscelida	k1gFnSc1
–	–	k?
ornitoscelidi	ornitoscelid	k1gMnPc5
</s>
<s>
Theropoda	Theropoda	k1gFnSc1
–	–	k?
teropodi	teropod	k1gMnPc5
</s>
<s>
Ornithischia	Ornithischia	k1gFnSc1
–	–	k?
ptakopánví	ptakopánvět	k5eAaImIp3nS,k5eAaPmIp3nS
</s>
<s>
Saurischia	Saurischia	k1gFnSc1
–	–	k?
plazopánví	plazopánev	k1gFnPc2
(	(	kIx(
<g/>
v	v	k7c6
upravené	upravený	k2eAgFnSc6d1
podobě	podoba	k1gFnSc6
<g/>
)	)	kIx)
</s>
<s>
Sauropodomorpha	Sauropodomorpha	k1gFnSc1
–	–	k?
sauropodomorfové	sauropodomorfový	k2eAgFnPc4d1
</s>
<s>
Herrerasauridae	Herrerasauridae	k1gFnSc1
–	–	k?
hererasauridi	hererasaurid	k1gMnPc1
</s>
<s>
Jde	jít	k5eAaImIp3nS
o	o	k7c4
převratné	převratný	k2eAgFnPc4d1
změny	změna	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
do	do	k7c2
podvědomí	podvědomí	k1gNnSc2
dostavají	dostavat	k5eAaImIp3nP,k5eAaPmIp3nP
jen	jen	k9
zvolna	zvolna	k6eAd1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
jim	on	k3xPp3gMnPc3
ale	ale	k9
přizpůsoben	přizpůsobit	k5eAaPmNgInS
i	i	k9
níže	nízce	k6eAd2
uvedený	uvedený	k2eAgInSc1d1
systém	systém	k1gInSc1
známých	známý	k2eAgInPc2d1
taxonů	taxon	k1gInPc2
dinosaurů	dinosaurus	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
(	(	kIx(
<g/>
Znamení	znamení	k1gNnSc2
kříže	kříž	k1gInSc2
(	(	kIx(
<g/>
†	†	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
použito	použít	k5eAaPmNgNnS
u	u	k7c2
taxonů	taxon	k1gInPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnPc1
jedinci	jedinec	k1gMnPc1
už	už	k6eAd1
zcela	zcela	k6eAd1
vymřeli	vymřít	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
)	)	kIx)
</s>
<s>
†	†	k?
<g/>
Řád	řád	k1gInSc1
Saurischia	Saurischia	k1gFnSc1
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
†	†	k?
<g/>
Podřád	podřád	k1gInSc1
Herrerasauria	Herrerasaurium	k1gNnSc2
</s>
<s>
†	†	k?
<g/>
Podřád	podřád	k1gInSc1
Sauropodomorpha	Sauropodomorpha	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Saturnalia	Saturnalia	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Čeleď	čeleď	k1gFnSc1
Plateosauridae	Plateosauridae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Infrařád	Infrařáda	k1gFnPc2
Sauropoda	Sauropoda	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
(	(	kIx(
<g/>
nedefinováno	definován	k2eNgNnSc4d1
<g/>
)	)	kIx)
Vulcanodontidae	Vulcanodontidae	k1gNnSc4
</s>
<s>
†	†	k?
<g/>
(	(	kIx(
<g/>
nedefinováno	definován	k2eNgNnSc1d1
<g/>
)	)	kIx)
Eusauropoda	Eusauropoda	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
(	(	kIx(
<g/>
nedefinováno	definován	k2eNgNnSc1d1
<g/>
)	)	kIx)
Turiasauria	Turiasaurium	k1gNnPc5
</s>
<s>
†	†	k?
<g/>
(	(	kIx(
<g/>
nedefinováno	definován	k2eNgNnSc1d1
<g/>
)	)	kIx)
Neosauropoda	Neosauropoda	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Nadčeleď	nadčeleď	k1gFnSc1
Diplodocoidea	Diplodocoidea	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
(	(	kIx(
<g/>
nedefinováno	definován	k2eNgNnSc1d1
<g/>
)	)	kIx)
Flagellicaudata	Flagellicaudata	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Čeleď	čeleď	k1gFnSc1
Diplodocidae	Diplodocidae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Čeleď	čeleď	k1gFnSc1
Dicraeosauridae	Dicraeosauridae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
(	(	kIx(
<g/>
nedefinováno	definován	k2eNgNnSc1d1
<g/>
)	)	kIx)
Macronaria	Macronarium	k1gNnSc2
</s>
<s>
†	†	k?
<g/>
(	(	kIx(
<g/>
nedefinováno	definovat	k5eNaBmNgNnS
<g/>
)	)	kIx)
Titanosauriformes	Titanosauriformes	k1gInSc1
</s>
<s>
†	†	k?
<g/>
Čeleď	čeleď	k1gFnSc1
Brachiosauridae	Brachiosauridae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
(	(	kIx(
<g/>
nedefinováno	definován	k2eNgNnSc1d1
<g/>
)	)	kIx)
Somphospondyli	Somphospondyli	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
(	(	kIx(
<g/>
nedefinováno	definován	k2eNgNnSc1d1
<g/>
)	)	kIx)
Titanosauria	Titanosaurium	k1gNnSc2
</s>
<s>
†	†	k?
<g/>
(	(	kIx(
<g/>
nedefinováno	definován	k2eNgNnSc1d1
<g/>
)	)	kIx)
Lithostrotia	Lithostrotia	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Čeleď	čeleď	k1gFnSc1
Saltasauridae	Saltasauridae	k1gFnSc1
</s>
<s>
Řád	řád	k1gInSc1
Ornithoscelida	Ornithoscelida	k1gFnSc1
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
†	†	k?
<g/>
Podřád	podřád	k1gInSc1
Ornithischia	Ornithischia	k1gFnSc1
</s>
<s>
</s>
<s desamb="1">
<g/>
†	†	k?
<g/>
Pisanosaurus	Pisanosaurus	k1gMnSc1
</s>
<s>
†	†	k?
<g/>
Čeleď	čeleď	k1gFnSc1
Heterodontosauridae	Heterodontosauridae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
(	(	kIx(
<g/>
nedefinováno	definován	k2eNgNnSc1d1
<g/>
)	)	kIx)
Genasauria	Genasaurium	k1gNnPc4
</s>
<s>
†	†	k?
<g/>
Podřád	podřád	k1gInSc1
Thyreophora	Thyreophora	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Infrařád	Infrařáda	k1gFnPc2
Stegosauria	Stegosaurium	k1gNnPc5
</s>
<s>
†	†	k?
<g/>
Čeleď	čeleď	k1gFnSc1
Stegosauridae	Stegosauridae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Infrařád	Infrařáda	k1gFnPc2
Ankylosauria	Ankylosaurium	k1gNnPc1
</s>
<s>
†	†	k?
<g/>
Čeleď	čeleď	k1gFnSc1
Nodosauridae	Nodosauridae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Podčeleď	podčeleď	k1gFnSc1
Nodosaurinae	Nodosaurinae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Čeleď	čeleď	k1gFnSc1
Ankylosauridae	Ankylosauridae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Podčeleď	podčeleď	k1gFnSc1
Ankylosaurinae	Ankylosaurinae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Podřád	podřád	k1gInSc1
Cerapoda	Cerapoda	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Čeleď	čeleď	k1gFnSc1
Heterodontosauridae	Heterodontosauridae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
(	(	kIx(
<g/>
nedefinováno	definován	k2eNgNnSc1d1
<g/>
)	)	kIx)
Marginocephalia	Marginocephalia	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Infrařád	Infrařáda	k1gFnPc2
Pachycephalosauria	Pachycephalosaurium	k1gNnSc2
</s>
<s>
†	†	k?
<g/>
Infrařád	Infrařáda	k1gFnPc2
Ceratopsia	Ceratopsia	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Čeleď	čeleď	k1gFnSc1
Psittacosauridae	Psittacosauridae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
(	(	kIx(
<g/>
nedefinováno	definován	k2eNgNnSc1d1
<g/>
)	)	kIx)
Neoceratopsia	Neoceratopsia	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Čeleď	čeleď	k1gFnSc1
Leptoceratopsidae	Leptoceratopsidae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Čeleď	čeleď	k1gFnSc1
Bagaceratopidae	Bagaceratopidae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
(	(	kIx(
<g/>
nedefinováno	definován	k2eNgNnSc1d1
<g/>
)	)	kIx)
Coronosauria	Coronosaurium	k1gNnPc1
</s>
<s>
†	†	k?
<g/>
Čeleď	čeleď	k1gFnSc1
Protoceratopsidae	Protoceratopsidae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Nadčeleď	nadčeleď	k1gFnSc1
Ceratopsoidea	Ceratopsoidea	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Čeleď	čeleď	k1gFnSc1
Ceratopsidae	Ceratopsidae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Podčeleď	podčeleď	k1gFnSc1
Centrosaurinae	Centrosaurinae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Podčeleď	podčeleď	k1gFnSc1
Ceratopsinae	Ceratopsinae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Infrařád	Infrařáda	k1gFnPc2
Ornithopoda	Ornithopoda	k1gMnSc1
</s>
<s>
†	†	k?
<g/>
Čeleď	čeleď	k1gFnSc1
Hypsilophodontidae	Hypsilophodontidae	k1gFnSc1
(	(	kIx(
<g/>
parafylycká	parafylycký	k2eAgFnSc1d1
<g/>
)	)	kIx)
</s>
<s>
†	†	k?
<g/>
Čeleď	čeleď	k1gFnSc1
Rhabdodontidae	Rhabdodontidae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
(	(	kIx(
<g/>
nedefinováno	definován	k2eNgNnSc1d1
<g/>
)	)	kIx)
Iguanodontia	Iguanodontia	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
(	(	kIx(
<g/>
nedefinováno	definován	k2eNgNnSc1d1
<g/>
)	)	kIx)
Dryomorpha	Dryomorpha	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Čeleď	čeleď	k1gFnSc1
Dryosauridae	Dryosauridae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
(	(	kIx(
<g/>
nedefinováno	definován	k2eNgNnSc1d1
<g/>
)	)	kIx)
Ankylopollexia	Ankylopollexia	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Čeleď	čeleď	k1gFnSc1
Camptosauridae	Camptosauridae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
(	(	kIx(
<g/>
nedefinováno	definovat	k5eNaBmNgNnS
<g/>
)	)	kIx)
Hadrosauriformes	Hadrosauriformes	k1gInSc1
</s>
<s>
†	†	k?
<g/>
Nadčeleď	nadčeleď	k1gFnSc1
Hadrosauroidea	Hadrosauroidea	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Čeleď	čeleď	k1gFnSc1
Hadrosauridae	Hadrosauridae	k1gFnSc1
</s>
<s>
Podřád	podřád	k1gInSc1
Theropoda	Theropod	k1gMnSc2
</s>
<s>
</s>
<s desamb="1">
<g/>
†	†	k?
<g/>
Eoraptor	Eoraptor	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
nedefinováno	definován	k2eNgNnSc1d1
<g/>
)	)	kIx)
Eusaurischia	Eusaurischia	k1gFnSc1
</s>
<s>
Podřád	podřád	k1gInSc1
Theropoda	Theropod	k1gMnSc2
</s>
<s>
(	(	kIx(
<g/>
nedefinováno	definován	k2eNgNnSc1d1
<g/>
)	)	kIx)
Neotheropoda	Neotheropoda	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Nadčeleď	nadčeleď	k1gFnSc1
Coelophysoidea	Coelophysoidea	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
nedefinováno	definován	k2eNgNnSc1d1
<g/>
)	)	kIx)
Averostra	Averostra	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Infrařád	Infrařáda	k1gFnPc2
Ceratosauria	Ceratosaurium	k1gNnPc4
</s>
<s>
†	†	k?
<g/>
Nadčeleď	nadčeleď	k1gFnSc1
Abelisauroidea	Abelisauroidea	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Čeleď	čeleď	k1gFnSc1
Abelisauridae	Abelisauridae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Podčeleď	podčeleď	k1gFnSc1
Carnotaurinae	Carnotaurinae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Čeleď	čeleď	k1gFnSc1
Noasauridae	Noasauridae	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
nedefinováno	definován	k2eNgNnSc4d1
<g/>
)	)	kIx)
Tetanurae	Tetanurae	k1gNnSc4
</s>
<s>
rody	rod	k1gInPc1
Dilophosaurus	Dilophosaurus	k1gInSc1
a	a	k8xC
Cryolophosaurus	Cryolophosaurus	k1gInSc1
</s>
<s>
†	†	k?
<g/>
Nadčeleď	nadčeleď	k1gFnSc1
Megalosauroidea	Megalosauroidea	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Čeleď	čeleď	k1gFnSc1
Megalosauridae	Megalosauridae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Podčeleď	podčeleď	k1gFnSc1
Megalosaurinae	Megalosaurinae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Podčeleď	podčeleď	k1gFnSc1
Eustreptospondylinae	Eustreptospondylinae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Čeleď	čeleď	k1gFnSc1
Spinosauridae	Spinosauridae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Podčeleď	podčeleď	k1gFnSc1
Spinosaurinae	Spinosaurinae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Podčeleď	podčeleď	k1gFnSc1
Baryonychinae	Baryonychinae	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
nedefinováno	definován	k2eNgNnSc1d1
<g/>
)	)	kIx)
Avetheropoda	Avetheropoda	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Infrařád	Infrařáda	k1gFnPc2
Carnosauria	Carnosaurium	k1gNnSc2
</s>
<s>
†	†	k?
<g/>
Nadčeleď	nadčeleď	k1gFnSc1
Allosauroidea	Allosauroidea	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Čeleď	čeleď	k1gFnSc1
Allosauridae	Allosauridae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Čeleď	čeleď	k1gFnSc1
Sinraptoridae	Sinraptoridae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Čeleď	čeleď	k1gFnSc1
Carcharodontosauridae	Carcharodontosauridae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Podčeleď	podčeleď	k1gFnSc1
Carcharodontosaurinae	Carcharodontosaurinae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Tribus	Tribus	k1gMnSc1
Giganotosaurini	Giganotosaurin	k1gMnPc1
</s>
<s>
Infrařád	Infrařáda	k1gFnPc2
Coelurosauria	Coelurosaurium	k1gNnSc2
</s>
<s>
†	†	k?
<g/>
CompsognathidaeLebka	CompsognathidaeLebka	k1gFnSc1
a	a	k8xC
část	část	k1gFnSc1
kostry	kostra	k1gFnSc2
druhu	druh	k1gInSc2
Tyrannosaurus	Tyrannosaurus	k1gInSc1
rex	rex	k?
</s>
<s>
†	†	k?
<g/>
Nadčeleď	nadčeleď	k1gFnSc1
Tyrannosauroidea	Tyrannosauroidea	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Čeleď	čeleď	k1gFnSc1
Tyrannosauridae	Tyrannosauridae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Podčeleď	podčeleď	k1gFnSc1
Albertosaurinae	Albertosaurinae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Podčeleď	podčeleď	k1gFnSc1
Tyrannosaurinae	Tyrannosaurinae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
(	(	kIx(
<g/>
nedefinováno	definován	k2eNgNnSc1d1
<g/>
)	)	kIx)
Ornithomimosauria	Ornithomimosaurium	k1gNnPc1
</s>
<s>
†	†	k?
<g/>
Čeleď	čeleď	k1gFnSc1
Ornithomimidae	Ornithomimidae	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
nedefinováno	definován	k2eNgNnSc1d1
<g/>
)	)	kIx)
Maniraptora	Maniraptor	k1gMnSc4
</s>
<s>
†	†	k?
<g/>
(	(	kIx(
<g/>
nedefinováno	definován	k2eNgNnSc1d1
<g/>
)	)	kIx)
Oviraptorosauria	Oviraptorosaurium	k1gNnPc4
</s>
<s>
†	†	k?
<g/>
Nadčeleď	nadčeleď	k1gFnSc1
Caenagnathoidea	Caenagnathoidea	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Čeleď	čeleď	k1gFnSc1
Caenagnathidae	Caenagnathidae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Čeleď	čeleď	k1gFnSc1
Oviraptoridae	Oviraptoridae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Podčeleď	podčeleď	k1gFnSc1
Oviraptorinae	Oviraptorinae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Podčeleď	podčeleď	k1gFnSc1
Ingeniinae	Ingeniinae	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
nedefinováno	definován	k2eNgNnSc4d1
<g/>
)	)	kIx)
Therizinosauria	Therizinosaurium	k1gNnSc2
</s>
<s>
†	†	k?
<g/>
Nadčeleď	nadčeleď	k1gFnSc1
Therizinosauroidea	Therizinosauroidea	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Čeleď	čeleď	k1gFnSc1
Therizinosauridae	Therizinosauridae	k1gFnSc1
</s>
<s>
(	(	kIx(
<g/>
nedefinováno	definován	k2eNgNnSc1d1
<g/>
)	)	kIx)
Eumaniraptora	Eumaniraptor	k1gMnSc4
(	(	kIx(
<g/>
Paraves	Paraves	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
(	(	kIx(
<g/>
nedefinováno	definován	k2eNgNnSc4d1
<g/>
)	)	kIx)
Deinonychosauria	Deinonychosaurium	k1gNnPc1
</s>
<s>
†	†	k?
<g/>
Čeleď	čeleď	k1gFnSc1
Troodontidae	Troodontidae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Čeleď	čeleď	k1gFnSc1
Dromaeosauridae	Dromaeosauridae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
(	(	kIx(
<g/>
nedefinováno	definován	k2eNgNnSc1d1
<g/>
)	)	kIx)
Microraptoria	Microraptorium	k1gNnPc5
</s>
<s>
†	†	k?
<g/>
Podčeleď	podčeleď	k1gFnSc1
Dromaeosaurinae	Dromaeosaurinae	k1gFnSc1
</s>
<s>
†	†	k?
<g/>
Podčeleď	podčeleď	k1gFnSc1
Velociraptorinae	Velociraptorinae	k1gFnSc1
</s>
<s>
klad	klad	k1gInSc1
Avialae	Aviala	k1gFnSc2
(	(	kIx(
<g/>
např.	např.	kA
ptáci	pták	k1gMnPc1
<g/>
)	)	kIx)
</s>
<s>
Paleontologie	paleontologie	k1gFnSc1
</s>
<s>
Unikátní	unikátní	k2eAgInPc4d1
objevy	objev	k1gInPc4
</s>
<s>
Mezi	mezi	k7c4
unikátní	unikátní	k2eAgInPc4d1
objevy	objev	k1gInPc4
dinosaurů	dinosaurus	k1gMnPc2
patří	patřit	k5eAaImIp3nS
především	především	k9
tzv.	tzv.	kA
„	„	k?
<g/>
dinosauří	dinosauří	k2eAgFnSc2d1
mumie	mumie	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
tedy	tedy	k8xC
fosilizované	fosilizovaný	k2eAgInPc1d1
pozůstatky	pozůstatek	k1gInPc1
neptačích	ptačí	k2eNgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
se	s	k7c7
zachovanými	zachovaný	k2eAgInPc7d1
otisky	otisk	k1gInPc7
měkkých	měkký	k2eAgFnPc2d1
tkání	tkáň	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nejproslulejší	proslulý	k2eAgMnPc4d3
patří	patřit	k5eAaImIp3nP
tzv.	tzv.	kA
opeření	opeřený	k2eAgMnPc1d1
dinosauři	dinosaurus	k1gMnPc1
z	z	k7c2
Číny	Čína	k1gFnSc2
nebo	nebo	k8xC
kachnozobí	kachnozobý	k2eAgMnPc1d1
dinosauři	dinosaurus	k1gMnPc1
„	„	k?
<g/>
Leonardo	Leonardo	k1gMnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
Brachylophosaurus	Brachylophosaurus	k1gMnSc1
<g/>
)	)	kIx)
a	a	k8xC
„	„	k?
<g/>
Dakota	Dakota	k1gFnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
Edmontosaurus	Edmontosaurus	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
u	u	k7c2
nichž	jenž	k3xRgFnPc2
se	se	k3xPyFc4
dochovala	dochovat	k5eAaPmAgFnS
i	i	k9
část	část	k1gFnSc1
kůže	kůže	k1gFnSc2
<g/>
,	,	kIx,
svaloviny	svalovina	k1gFnSc2
<g/>
,	,	kIx,
vaziva	vazivo	k1gNnSc2
<g/>
,	,	kIx,
vnitřních	vnitřní	k2eAgInPc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
orgánů	orgán	k1gInPc2
apod.	apod.	kA
<g/>
[	[	kIx(
<g/>
174	#num#	k4
<g/>
]	]	kIx)
Ukazuje	ukazovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
k	k	k7c3
zachování	zachování	k1gNnSc3
podobných	podobný	k2eAgFnPc2d1
struktur	struktura	k1gFnPc2
přispívá	přispívat	k5eAaImIp3nS
velké	velký	k2eAgNnSc4d1
množství	množství	k1gNnSc4
různých	různý	k2eAgInPc2d1
faktorů	faktor	k1gInPc2
a	a	k8xC
biologických	biologický	k2eAgInPc2d1
i	i	k8xC
geologických	geologický	k2eAgInPc2d1
procesů	proces	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
175	#num#	k4
<g/>
]	]	kIx)
Možnost	možnost	k1gFnSc1
dochování	dochování	k1gNnSc2
původního	původní	k2eAgInSc2d1
organického	organický	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
v	v	k7c6
dinosauřích	dinosauří	k2eAgFnPc6d1
fosilních	fosilní	k2eAgFnPc6d1
kostech	kost	k1gFnPc6
je	být	k5eAaImIp3nS
však	však	k9
dosud	dosud	k6eAd1
zpochybňována	zpochybňován	k2eAgFnSc1d1
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
s	s	k7c7
poukazem	poukaz	k1gInSc7
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
že	že	k8xS
předchozí	předchozí	k2eAgInPc1d1
výsledky	výsledek	k1gInPc1
mohly	moct	k5eAaImAgInP
být	být	k5eAaImF
zkresleny	zkreslit	k5eAaPmNgInP
objevem	objev	k1gInSc7
recentních	recentní	k2eAgFnPc2d1
organických	organický	k2eAgFnPc2d1
molekul	molekula	k1gFnPc2
<g/>
,	,	kIx,
náležejících	náležející	k2eAgFnPc2d1
mikrobiomu	mikrobiom	k1gInSc6
v	v	k7c6
kostech	kost	k1gFnPc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
176	#num#	k4
<g/>
]	]	kIx)
Jednou	jednou	k6eAd1
z	z	k7c2
cest	cesta	k1gFnPc2
pro	pro	k7c4
zachování	zachování	k1gNnSc4
měkkých	měkký	k2eAgFnPc2d1
tkání	tkáň	k1gFnPc2
ve	v	k7c6
fosilním	fosilní	k2eAgInSc6d1
stavu	stav	k1gInSc6
by	by	kYmCp3nS
přitom	přitom	k6eAd1
mohla	moct	k5eAaImAgFnS
být	být	k5eAaImF
přeměna	přeměna	k1gFnSc1
proteinů	protein	k1gInPc2
do	do	k7c2
polymerů	polymer	k1gInPc2
v	v	k7c6
oxidativních	oxidativní	k2eAgNnPc6d1
prostředích	prostředí	k1gNnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
177	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
178	#num#	k4
<g/>
]	]	kIx)
Mnohé	mnohý	k2eAgInPc1d1
případy	případ	k1gInPc1
domnělých	domnělý	k2eAgFnPc2d1
„	„	k?
<g/>
měkkých	měkký	k2eAgFnPc2d1
tkání	tkáň	k1gFnPc2
<g/>
“	“	k?
dinosaurů	dinosaurus	k1gMnPc2
však	však	k8xC
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
způsobeny	způsobit	k5eAaPmNgInP
přítomností	přítomnost	k1gFnSc7
mikrobiálního	mikrobiální	k2eAgInSc2d1
filmu	film	k1gInSc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
bakteriální	bakteriální	k2eAgFnPc1d1
mikroflóry	mikroflóra	k1gFnPc1
ve	v	k7c6
fosiliích	fosilie	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
179	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
průběhu	průběh	k1gInSc6
historie	historie	k1gFnSc2
bylo	být	k5eAaImAgNnS
objeveno	objevit	k5eAaPmNgNnS
také	také	k9
množství	množství	k1gNnSc1
lokalit	lokalita	k1gFnPc2
s	s	k7c7
hromadnými	hromadný	k2eAgInPc7d1
nálezy	nález	k1gInPc7
koster	kostra	k1gFnPc2
dinosaurů	dinosaurus	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Může	moct	k5eAaImIp3nS
se	se	k3xPyFc4
jednat	jednat	k5eAaImF
o	o	k7c4
pohřebiště	pohřebiště	k1gNnSc4
<g/>
,	,	kIx,
vzniklá	vzniklý	k2eAgFnSc1d1
vinou	vinou	k7c2
přírodní	přírodní	k2eAgFnSc2d1
živelné	živelný	k2eAgFnSc2d1
katastrofy	katastrofa	k1gFnSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
o	o	k7c4
náhodná	náhodný	k2eAgNnPc4d1
nahromadění	nahromadění	k1gNnPc4
koster	kostra	k1gFnPc2
například	například	k6eAd1
v	v	k7c6
říčních	říční	k2eAgFnPc6d1
deltách	delta	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobné	podobný	k2eAgInPc1d1
objevy	objev	k1gInPc1
pocházejí	pocházet	k5eAaImIp3nP
například	například	k6eAd1
z	z	k7c2
USA	USA	kA
<g/>
,	,	kIx,
Kanady	Kanada	k1gFnSc2
<g/>
,	,	kIx,
Belgie	Belgie	k1gFnSc2
<g/>
,	,	kIx,
Švýcarska	Švýcarsko	k1gNnSc2
<g/>
,	,	kIx,
Číny	Čína	k1gFnSc2
i	i	k8xC
mnoha	mnoho	k4c2
jiných	jiný	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
180	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zajímavé	zajímavý	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
také	také	k9
objevy	objev	k1gInPc4
fosilních	fosilní	k2eAgInPc2d1
otisků	otisk	k1gInPc2
stop	stopa	k1gFnPc2
dinosaurů	dinosaurus	k1gMnPc2
<g/>
,	,	kIx,
které	který	k3yQgMnPc4,k3yRgMnPc4,k3yIgMnPc4
již	již	k6eAd1
známe	znát	k5eAaImIp1nP
z	z	k7c2
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnSc2
rozsáhlé	rozsáhlý	k2eAgFnSc2d1
lokality	lokalita	k1gFnSc2
mohou	moct	k5eAaImIp3nP
vytvářet	vytvářet	k5eAaImF
přerušovaný	přerušovaný	k2eAgInSc4d1
obří	obří	k2eAgInSc4d1
systém	systém	k1gInSc4
tzv.	tzv.	kA
mega-tracksites	mega-tracksites	k1gInSc1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
tomu	ten	k3xDgNnSc3
je	být	k5eAaImIp3nS
například	například	k6eAd1
v	v	k7c6
jihoamerické	jihoamerický	k2eAgFnSc6d1
Bolívii	Bolívie	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
má	mít	k5eAaImIp3nS
podobná	podobný	k2eAgFnSc1d1
multi-lokalita	multi-lokalita	k1gFnSc1
rozlohu	rozloha	k1gFnSc4
přes	přes	k7c4
100	#num#	k4
000	#num#	k4
km	km	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
181	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
mnoha	mnoho	k4c6
případech	případ	k1gInPc6
byly	být	k5eAaImAgFnP
objeveny	objevit	k5eAaPmNgFnP
také	také	k9
stopy	stopa	k1gFnPc1
infekcí	infekce	k1gFnPc2
a	a	k8xC
kosterních	kosterní	k2eAgFnPc2d1
patologií	patologie	k1gFnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
kostních	kostní	k2eAgInPc2d1
nádorů	nádor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
u	u	k7c2
jedince	jedinec	k1gMnSc2
rohatého	rohatý	k1gMnSc2
dinosaura	dinosaurus	k1gMnSc2
rodu	rod	k1gInSc2
Centrosaurus	Centrosaurus	k1gMnSc1
z	z	k7c2
kanadské	kanadský	k2eAgFnSc2d1
Alberty	Alberta	k1gFnSc2
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
odhalena	odhalen	k2eAgFnSc1d1
zhoubná	zhoubný	k2eAgFnSc1d1
choroba	choroba	k1gFnSc1
v	v	k7c6
podobě	podoba	k1gFnSc6
tumoru	tumor	k1gInSc2
osteosarkomu	osteosarkom	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
182	#num#	k4
<g/>
]	]	kIx)
Objeveny	objeven	k2eAgFnPc1d1
byly	být	k5eAaImAgFnP
také	také	k9
stopy	stopa	k1gFnPc1
po	po	k7c6
zánětu	zánět	k1gInSc6
kostní	kostní	k2eAgFnSc2d1
dřeně	dřeň	k1gFnSc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
osteomyelitidě	osteomyelitida	k1gFnSc3
u	u	k7c2
druhohorních	druhohorní	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
(	(	kIx(
<g/>
a	a	k8xC
to	ten	k3xDgNnSc1
například	například	k6eAd1
u	u	k7c2
obřího	obří	k2eAgMnSc2d1
jedince	jedinec	k1gMnSc2
tyranosaura	tyranosaur	k1gMnSc2
"	"	kIx"
<g/>
Sue	Sue	k1gMnSc1
<g/>
"	"	kIx"
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
183	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
České	český	k2eAgInPc1d1
nálezy	nález	k1gInPc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Nálezy	nález	k1gInPc1
dinosaurů	dinosaurus	k1gMnPc2
na	na	k7c6
území	území	k1gNnSc6
Česka	Česko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc1
kost	kost	k1gFnSc1
českého	český	k2eAgMnSc2d1
dinosaura	dinosaurus	k1gMnSc2
(	(	kIx(
<g/>
Burianosaurus	Burianosaurus	k1gInSc1
augustai	augusta	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
objevená	objevený	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
u	u	k7c2
Kutné	kutný	k2eAgFnSc2d1
Hory	hora	k1gFnSc2
</s>
<s>
Dinosauři	dinosaurus	k1gMnPc1
byli	být	k5eAaImAgMnP
běžnou	běžný	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
druhohorních	druhohorní	k2eAgInPc2d1
ekosystémů	ekosystém	k1gInPc2
také	také	k9
na	na	k7c6
území	území	k1gNnSc6
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
184	#num#	k4
<g/>
]	]	kIx)
První	první	k4xOgFnPc1
fosilie	fosilie	k1gFnPc1
přisuzované	přisuzovaný	k2eAgFnPc1d1
dinosaurům	dinosaurus	k1gMnPc3
byly	být	k5eAaImAgFnP
na	na	k7c6
našem	náš	k3xOp1gNnSc6
území	území	k1gNnSc6
objeveny	objevit	k5eAaPmNgInP
již	již	k6eAd1
koncem	koncem	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
popsáni	popsán	k2eAgMnPc1d1
profesorem	profesor	k1gMnSc7
Antonínem	Antonín	k1gMnSc7
Fričem	Frič	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vesměs	vesměs	k6eAd1
se	se	k3xPyFc4
však	však	k9
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
pozůstatky	pozůstatek	k1gInPc4
jiných	jiný	k2eAgMnPc2d1
tvorů	tvor	k1gMnPc2
nebo	nebo	k8xC
nelze	lze	k6eNd1
přesně	přesně	k6eAd1
jejich	jejich	k3xOp3gInSc4
původ	původ	k1gInSc4
rozlišit	rozlišit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
185	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
byl	být	k5eAaImAgInS
objeven	objevit	k5eAaPmNgMnS
vůbec	vůbec	k9
první	první	k4xOgMnSc1
nepochybný	pochybný	k2eNgMnSc1d1
český	český	k2eAgMnSc1d1
dinosaurus	dinosaurus	k1gMnSc1
známý	známý	k2eAgMnSc1d1
podle	podle	k7c2
kosterních	kosterní	k2eAgInPc2d1
pozůstatků	pozůstatek	k1gInPc2
na	na	k7c6
Kutnohorsku	Kutnohorsko	k1gNnSc6
(	(	kIx(
<g/>
předtím	předtím	k6eAd1
byla	být	k5eAaImAgFnS
u	u	k7c2
nás	my	k3xPp1nPc2
objevena	objeven	k2eAgFnSc1d1
pouze	pouze	k6eAd1
fosilní	fosilní	k2eAgFnSc1d1
tříprstá	tříprstý	k2eAgFnSc1d1
stopa	stopa	k1gFnSc1
malého	malý	k2eAgMnSc2d1
dravého	dravý	k2eAgMnSc2d1
dinosaura	dinosaurus	k1gMnSc2
z	z	k7c2
lomu	lom	k1gInSc2
„	„	k?
<g/>
U	u	k7c2
Devíti	devět	k4xCc2
Křížů	kříž	k1gInPc2
<g/>
“	“	k?
nedaleko	nedaleko	k7c2
Červeného	Červeného	k2eAgInSc2d1
Kostelce	Kostelec	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
186	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
byly	být	k5eAaImAgInP
na	na	k7c6
stejném	stejný	k2eAgNnSc6d1
místě	místo	k1gNnSc6
objeveny	objeven	k2eAgFnPc1d1
další	další	k2eAgFnPc1d1
kosti	kost	k1gFnPc1
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
články	článek	k1gInPc4
prstů	prst	k1gInPc2
nohy	noha	k1gFnSc2
tohoto	tento	k3xDgMnSc2
ornitopodního	ornitopodní	k2eAgMnSc2d1
dinosaura	dinosaurus	k1gMnSc2
pravděpodobně	pravděpodobně	k6eAd1
příbuzného	příbuzný	k2eAgInSc2d1
rodu	rod	k1gInSc2
Rhabdodon	Rhabdodona	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
roku	rok	k1gInSc2
2007	#num#	k4
bylo	být	k5eAaImAgNnS
oznámeno	oznámit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
údajně	údajně	k6eAd1
našly	najít	k5eAaPmAgInP
další	další	k2eAgInPc1d1
otisky	otisk	k1gInPc1
stop	stopa	k1gFnPc2
malého	malý	k2eAgMnSc2d1
teropoda	teropod	k1gMnSc2
<g/>
,	,	kIx,
patřící	patřící	k2eAgFnSc1d1
pravděpodobně	pravděpodobně	k6eAd1
stejnému	stejný	k2eAgInSc3d1
druhu	druh	k1gInSc3
jako	jako	k8xC,k8xS
ta	ten	k3xDgFnSc1
předchozí	předchozí	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
přibyl	přibýt	k5eAaPmAgInS
další	další	k2eAgInSc1d1
objev	objev	k1gInSc1
<g/>
,	,	kIx,
stopy	stopa	k1gFnPc1
ptakopánvého	ptakopánvý	k2eAgMnSc2d1
dinosaura	dinosaurus	k1gMnSc2
ze	z	k7c2
stejného	stejný	k2eAgInSc2d1
lomu	lom	k1gInSc2
(	(	kIx(
<g/>
pískovcová	pískovcový	k2eAgFnSc1d1
deska	deska	k1gFnSc1
v	v	k7c6
Pražské	pražský	k2eAgFnSc6d1
botanické	botanický	k2eAgFnSc6d1
zahradě	zahrada	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
187	#num#	k4
<g/>
]	]	kIx)
<g/>
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
se	se	k3xPyFc4
na	na	k7c6
internetu	internet	k1gInSc6
objevila	objevit	k5eAaPmAgFnS
recesistická	recesistický	k2eAgFnSc1d1
zpráva	zpráva	k1gFnSc1
o	o	k7c6
objevu	objev	k1gInSc6
velkého	velký	k2eAgMnSc2d1
teropodního	teropodní	k2eAgMnSc2d1
dinosaura	dinosaurus	k1gMnSc2
jménem	jméno	k1gNnSc7
Torvosaurus	Torvosaurus	k1gInSc1
orliciensis	orliciensis	k1gInSc1
v	v	k7c6
Orlických	orlický	k2eAgFnPc6d1
horách	hora	k1gFnPc6
(	(	kIx(
<g/>
nedaleko	nedaleko	k7c2
obce	obec	k1gFnSc2
Kounov	Kounov	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
pak	pak	k6eAd1
přibyl	přibýt	k5eAaPmAgInS
další	další	k2eAgInSc1d1
fiktivní	fiktivní	k2eAgInSc1d1
rod	rod	k1gInSc1
<g/>
,	,	kIx,
ceratopsid	ceratopsid	k1gInSc1
Hlukyceratops	Hlukyceratops	k1gInSc1
kounoviensis	kounoviensis	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
188	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
popsal	popsat	k5eAaPmAgMnS
Daniel	Daniel	k1gMnSc1
Madzia	Madzius	k1gMnSc2
fosilní	fosilní	k2eAgInSc1d1
zub	zub	k1gInSc1
ze	z	k7c2
sbírek	sbírka	k1gFnPc2
PřF	PřF	k1gFnSc2
MU	MU	kA
v	v	k7c6
Brně	Brno	k1gNnSc6
jako	jako	k9
první	první	k4xOgInSc4
zub	zub	k1gInSc4
teropdního	teropdní	k2eAgMnSc2d1
dinosaura	dinosaurus	k1gMnSc2
<g/>
,	,	kIx,
známého	známý	k2eAgMnSc2d1
z	z	k7c2
našeho	náš	k3xOp1gNnSc2
území	území	k1gNnSc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
Moravská	moravský	k2eAgFnSc1d1
tetanura	tetanura	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
asi	asi	k9
5	#num#	k4
metrů	metr	k1gInPc2
dlouhý	dlouhý	k2eAgMnSc1d1
dravý	dravý	k2eAgMnSc1d1
dinosaurus	dinosaurus	k1gMnSc1
žil	žít	k5eAaImAgMnS
v	v	k7c6
období	období	k1gNnSc6
pozdní	pozdní	k2eAgFnSc2d1
jury	jura	k1gFnSc2
(	(	kIx(
<g/>
asi	asi	k9
před	před	k7c7
160	#num#	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
<g/>
)	)	kIx)
na	na	k7c6
území	území	k1gNnSc6
dnešní	dnešní	k2eAgFnSc2d1
Jižní	jižní	k2eAgFnSc2d1
Moravy	Morava	k1gFnSc2
(	(	kIx(
<g/>
lokalita	lokalita	k1gFnSc1
Švédské	švédský	k2eAgFnSc2d1
šance	šance	k1gFnSc2
nedaleko	nedaleko	k7c2
Brna	Brno	k1gNnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
189	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
září	září	k1gNnSc6
2017	#num#	k4
publikoval	publikovat	k5eAaBmAgMnS
paleontolog	paleontolog	k1gMnSc1
Daniel	Daniel	k1gMnSc1
Madzia	Madzia	k1gFnSc1
s	s	k7c7
kolegy	kolega	k1gMnPc7
vědeckou	vědecký	k2eAgFnSc4d1
studii	studie	k1gFnSc4
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yRgFnSc6,k3yQgFnSc6,k3yIgFnSc6
stanovil	stanovit	k5eAaPmAgInS
prvního	první	k4xOgMnSc2
českého	český	k2eAgMnSc2d1
dinosaura	dinosaurus	k1gMnSc2
s	s	k7c7
oficiálním	oficiální	k2eAgNnSc7d1
vědeckým	vědecký	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
–	–	k?
Burianosaurus	Burianosaurus	k1gInSc4
augustai	augusta	k1gFnSc2
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
známého	známý	k2eAgInSc2d1
jako	jako	k8xC,k8xS
„	„	k?
<g/>
ornitopod	ornitopoda	k1gFnPc2
od	od	k7c2
Kutné	kutný	k2eAgFnSc2d1
Hory	hora	k1gFnSc2
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
190	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Replika	replika	k1gFnSc1
otisku	otisk	k1gInSc2
tříprsté	tříprstý	k2eAgFnSc2d1
stopy	stopa	k1gFnSc2
českého	český	k2eAgMnSc2d1
dinosaura	dinosaurus	k1gMnSc2
(	(	kIx(
<g/>
či	či	k8xC
dinosauromorfa	dinosauromorf	k1gMnSc2
<g/>
)	)	kIx)
od	od	k7c2
Červeného	Červeného	k2eAgInSc2d1
Kostelce	Kostelec	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
objevena	objevit	k5eAaPmNgFnS
na	na	k7c6
pískovcové	pískovcový	k2eAgFnSc6d1
desce	deska	k1gFnSc6
a	a	k8xC
má	mít	k5eAaImIp3nS
stáří	stáří	k1gNnSc4
kolem	kolem	k7c2
215	#num#	k4
milionů	milion	k4xCgInPc2
let	léto	k1gNnPc2
(	(	kIx(
<g/>
pozdní	pozdní	k2eAgInSc1d1
trias	trias	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Délka	délka	k1gFnSc1
stopy	stopa	k1gFnSc2
činí	činit	k5eAaImIp3nS
14	#num#	k4
centimetrů	centimetr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Vyhynutí	vyhynutí	k1gNnSc1
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Vymírání	vymírání	k1gNnSc2
na	na	k7c6
konci	konec	k1gInSc6
křídy	křída	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Otázka	otázka	k1gFnSc1
masového	masový	k2eAgNnSc2d1
vymírání	vymírání	k1gNnSc2
na	na	k7c6
konci	konec	k1gInSc6
druhohor	druhohory	k1gFnPc2
je	být	k5eAaImIp3nS
téma	téma	k1gNnSc1
<g/>
,	,	kIx,
ke	k	k7c3
kterému	který	k3yIgInSc3,k3yRgInSc3,k3yQgInSc3
se	se	k3xPyFc4
vědci	vědec	k1gMnPc1
často	často	k6eAd1
vracejí	vracet	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teorií	teorie	k1gFnPc2
na	na	k7c4
téma	téma	k1gNnSc4
<g/>
,	,	kIx,
proč	proč	k6eAd1
dinosauři	dinosaurus	k1gMnPc1
vymřeli	vymřít	k5eAaPmAgMnP
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgNnSc1
jsou	být	k5eAaImIp3nP
podpořeny	podpořen	k2eAgInPc1d1
plnohodnotnými	plnohodnotný	k2eAgInPc7d1
důkazy	důkaz	k1gInPc7
<g/>
,	,	kIx,
jiné	jiný	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
spíše	spíše	k9
úsměvné	úsměvný	k2eAgFnPc1d1
a	a	k8xC
zakládají	zakládat	k5eAaImIp3nP
se	se	k3xPyFc4
na	na	k7c6
pouhých	pouhý	k2eAgFnPc6d1
spekulacích	spekulace	k1gFnPc6
nebo	nebo	k8xC
vyložených	vyložený	k2eAgInPc6d1
nesmyslech	nesmysl	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
191	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
takovým	takový	k3xDgFnPc3
může	moct	k5eAaImIp3nS
patřit	patřit	k5eAaImF
myšlenka	myšlenka	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
za	za	k7c4
vyhynutí	vyhynutí	k1gNnSc4
dinosaurů	dinosaurus	k1gMnPc2
je	být	k5eAaImIp3nS
odpovědná	odpovědný	k2eAgFnSc1d1
nějaká	nějaký	k3yIgFnSc1
zvláštní	zvláštní	k2eAgFnSc1d1
nemoc	nemoc	k1gFnSc1
<g/>
,	,	kIx,
téměř	téměř	k6eAd1
pandemie	pandemie	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
vyhubila	vyhubit	k5eAaPmAgFnS
veliké	veliký	k2eAgNnSc4d1
množství	množství	k1gNnSc4
tehdejší	tehdejší	k2eAgFnSc2d1
fauny	fauna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vědci	vědec	k1gMnPc1
však	však	k9
zatím	zatím	k6eAd1
nemají	mít	k5eNaImIp3nP
žádné	žádný	k3yNgInPc4
důkazy	důkaz	k1gInPc4
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
v	v	k7c6
minulosti	minulost	k1gFnSc6
něco	něco	k3yInSc1
takového	takový	k3xDgMnSc4
přihodilo	přihodit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
sice	sice	k8xC
nalezeny	nalezen	k2eAgInPc1d1
pozůstatky	pozůstatek	k1gInPc1
nemocných	mocný	k2eNgMnPc2d1,k2eAgMnPc2d1
jedinců	jedinec	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
na	na	k7c4
svoji	svůj	k3xOyFgFnSc4
nemoc	nemoc	k1gFnSc4
pravděpodobně	pravděpodobně	k6eAd1
zemřeli	zemřít	k5eAaPmAgMnP
<g/>
,	,	kIx,
avšak	avšak	k8xC
často	často	k6eAd1
se	se	k3xPyFc4
doba	doba	k1gFnSc1
úmrtí	úmrtí	k1gNnSc2
neshoduje	shodovat	k5eNaImIp3nS
s	s	k7c7
dobou	doba	k1gFnSc7
vyhynutí	vyhynutí	k1gNnSc2
zbylých	zbylý	k2eAgInPc2d1
druhů	druh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgFnSc1d1
<g/>
,	,	kIx,
ne	ne	k9
příliš	příliš	k6eAd1
populární	populární	k2eAgFnSc7d1
a	a	k8xC
uznávanou	uznávaný	k2eAgFnSc7d1
teorií	teorie	k1gFnSc7
(	(	kIx(
<g/>
či	či	k8xC
opět	opět	k6eAd1
raději	rád	k6eAd2
spekulací	spekulace	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
ta	ten	k3xDgFnSc1
o	o	k7c6
„	„	k?
<g/>
samozničení	samozničení	k1gNnSc6
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
během	během	k7c2
dlouhých	dlouhý	k2eAgInPc2d1
miliónů	milión	k4xCgInPc2
let	léto	k1gNnPc2
evoluce	evoluce	k1gFnSc2
se	se	k3xPyFc4
dinosauři	dinosaurus	k1gMnPc1
vyvinuli	vyvinout	k5eAaPmAgMnP
do	do	k7c2
takových	takový	k3xDgInPc2
tvarů	tvar	k1gInPc2
<g/>
,	,	kIx,
až	až	k6eAd1
nebyli	být	k5eNaImAgMnP
schopni	schopen	k2eAgMnPc1d1
dál	daleko	k6eAd2
přežít	přežít	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jenže	jenže	k8xC
to	ten	k3xDgNnSc1
nijak	nijak	k6eAd1
neodpovídá	odpovídat	k5eNaImIp3nS
přírodnímu	přírodní	k2eAgInSc3d1
výběru	výběr	k1gInSc3
<g/>
,	,	kIx,
dle	dle	k7c2
něhož	jenž	k3xRgInSc2
přežije	přežít	k5eAaPmIp3nS
ten	ten	k3xDgInSc4
nejsilnější	silný	k2eAgInSc4d3
<g/>
,	,	kIx,
nejlépe	dobře	k6eAd3
přizpůsobený	přizpůsobený	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vědci	vědec	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
zastávali	zastávat	k5eAaImAgMnP
<g/>
/	/	kIx~
<g/>
zastávají	zastávat	k5eAaImIp3nP
tuto	tento	k3xDgFnSc4
teorii	teorie	k1gFnSc4
<g/>
,	,	kIx,
poukazují	poukazovat	k5eAaImIp3nP
<g/>
,	,	kIx,
jakožto	jakožto	k8xS
důkaz	důkaz	k1gInSc1
<g/>
,	,	kIx,
na	na	k7c4
skupiny	skupina	k1gFnPc4
pachycefalosaurů	pachycefalosaur	k1gMnPc2
<g/>
,	,	kIx,
ankylosaurů	ankylosaur	k1gMnPc2
<g/>
,	,	kIx,
ceratopsidů	ceratopsid	k1gMnPc2
a	a	k8xC
jiných	jiný	k2eAgMnPc2d1
<g/>
,	,	kIx,
bizarně	bizarně	k6eAd1
vyhlížejících	vyhlížející	k2eAgMnPc2d1
živočichů	živočich	k1gMnPc2
z	z	k7c2
konce	konec	k1gInSc2
křídového	křídový	k2eAgNnSc2d1
období	období	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Tato	tento	k3xDgFnSc1
teorie	teorie	k1gFnSc1
se	se	k3xPyFc4
nezdá	zdát	k5eNaImIp3nS,k5eNaPmIp3nS
být	být	k5eAaImF
příliš	příliš	k6eAd1
pravděpodobná	pravděpodobný	k2eAgFnSc1d1
už	už	k9
jen	jen	k9
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
dinosauři	dinosaurus	k1gMnPc1
nebyli	být	k5eNaImAgMnP
jedinými	jediný	k2eAgMnPc7d1
tvory	tvor	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
tehdy	tehdy	k6eAd1
vymizeli	vymizet	k5eAaPmAgMnP
(	(	kIx(
<g/>
ptakoještěři	ptakoještěr	k1gMnPc1
<g/>
,	,	kIx,
mořští	mořský	k2eAgMnPc1d1
plazi	plaz	k1gMnPc1
<g/>
,	,	kIx,
…	…	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
zdaleka	zdaleka	k6eAd1
nemuseli	muset	k5eNaImAgMnP
vypadat	vypadat	k5eAaPmF,k5eAaImF
až	až	k9
tak	tak	k6eAd1
bizarně	bizarně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ba	ba	k9
naopak	naopak	k6eAd1
je	být	k5eAaImIp3nS
jasné	jasný	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
tyto	tento	k3xDgFnPc1
„	„	k?
<g/>
bizarnosti	bizarnost	k1gFnPc1
<g/>
“	“	k?
byly	být	k5eAaImAgInP
veledůležitým	veledůležitý	k2eAgInSc7d1
aspektem	aspekt	k1gInSc7
pohlavního	pohlavní	k2eAgInSc2d1
výběru	výběr	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
dokazuje	dokazovat	k5eAaImIp3nS
skutečnost	skutečnost	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
během	během	k7c2
vývoje	vývoj	k1gInSc2
docházelo	docházet	k5eAaImAgNnS
ke	k	k7c3
zvětšování	zvětšování	k1gNnSc3
a	a	k8xC
vývoji	vývoj	k1gInSc3
těchto	tento	k3xDgFnPc2
„	„	k?
<g/>
ozdob	ozdoba	k1gFnPc2
<g/>
“	“	k?
<g/>
,	,	kIx,
a	a	k8xC
nikoli	nikoli	k9
ke	k	k7c3
ztrátě	ztráta	k1gFnSc3
<g/>
,	,	kIx,
popřípadě	popřípadě	k6eAd1
redukci	redukce	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
stejné	stejný	k2eAgFnSc6d1
době	doba	k1gFnSc6
navíc	navíc	k6eAd1
vyhynula	vyhynout	k5eAaPmAgFnS
i	i	k9
většina	většina	k1gFnSc1
dosud	dosud	k6eAd1
rostoucích	rostoucí	k2eAgInPc2d1
druhů	druh	k1gInPc2
cykasů	cykas	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objevují	objevovat	k5eAaImIp3nP
se	se	k3xPyFc4
také	také	k9
jiné	jiný	k2eAgFnPc1d1
hypotézy	hypotéza	k1gFnPc1
<g/>
,	,	kIx,
například	například	k6eAd1
i	i	k9
taková	takový	k3xDgFnSc1
<g/>
,	,	kIx,
že	že	k8xS
za	za	k7c4
vyhynutí	vyhynutí	k1gNnSc4
dinosaurů	dinosaurus	k1gMnPc2
může	moct	k5eAaImIp3nS
nedostatek	nedostatek	k1gInSc1
vitamínu	vitamín	k1gInSc2
D	D	kA
u	u	k7c2
dinosauřích	dinosauří	k2eAgNnPc2d1
embryí	embryo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
192	#num#	k4
<g/>
]	]	kIx)
Podobné	podobný	k2eAgFnPc1d1
myšlenky	myšlenka	k1gFnPc1
jsou	být	k5eAaImIp3nP
ale	ale	k9
velmi	velmi	k6eAd1
nepravděpodobné	pravděpodobný	k2eNgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Paleontolog	paleontolog	k1gMnSc1
Robert	Robert	k1gMnSc1
T.	T.	kA
Bakker	Bakker	k1gInSc1
se	se	k3xPyFc4
domnívá	domnívat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
hlavní	hlavní	k2eAgFnSc7d1
příčinou	příčina	k1gFnSc7
vyhynutí	vyhynutí	k1gNnSc2
dinosaurů	dinosaurus	k1gMnPc2
byly	být	k5eAaImAgFnP
pandemie	pandemie	k1gFnPc1
nemocí	nemoc	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
mezi	mezi	k7c7
dinosauřími	dinosauří	k2eAgFnPc7d1
populacemi	populace	k1gFnPc7
rozšířily	rozšířit	k5eAaPmAgFnP
spolu	spolu	k6eAd1
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
na	na	k7c6
konci	konec	k1gInSc6
křídy	křída	k1gFnSc2
vytvořily	vytvořit	k5eAaPmAgInP
pevninské	pevninský	k2eAgInPc1d1
mosty	most	k1gInPc1
mezi	mezi	k7c7
východní	východní	k2eAgFnSc7d1
Asií	Asie	k1gFnSc7
a	a	k8xC
Severní	severní	k2eAgFnSc7d1
Amerikou	Amerika	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
193	#num#	k4
<g/>
]	]	kIx)
Bakker	Bakkero	k1gNnPc2
sice	sice	k8xC
uznává	uznávat	k5eAaImIp3nS
i	i	k9
realitu	realita	k1gFnSc4
dopadu	dopad	k1gInSc2
velkého	velký	k2eAgInSc2d1
asteroidu	asteroid	k1gInSc2
<g/>
,	,	kIx,
nevidí	vidět	k5eNaImIp3nS
v	v	k7c6
něm	on	k3xPp3gMnSc6
však	však	k9
hlavní	hlavní	k2eAgFnSc4d1
příčinu	příčina	k1gFnSc4
vymírání	vymírání	k1gNnSc2
na	na	k7c6
konci	konec	k1gInSc6
křídy	křída	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
hypotéza	hypotéza	k1gFnSc1
však	však	k9
nevysvětluje	vysvětlovat	k5eNaImIp3nS
vyhynutí	vyhynutí	k1gNnSc2
mnoha	mnoho	k4c2
jiných	jiný	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
organismů	organismus	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
194	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Snad	snad	k9
nejvíc	nejvíc	k6eAd1,k6eAd3
uznávanou	uznávaný	k2eAgFnSc7d1
a	a	k8xC
rozšířenou	rozšířený	k2eAgFnSc7d1
teorií	teorie	k1gFnSc7
je	být	k5eAaImIp3nS
ta	ten	k3xDgFnSc1
o	o	k7c6
katastrofě	katastrofa	k1gFnSc6
způsobené	způsobený	k2eAgFnSc6d1
dopadem	dopad	k1gInSc7
vesmírného	vesmírný	k2eAgNnSc2d1
tělesa	těleso	k1gNnSc2
na	na	k7c6
Zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
teorie	teorie	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
osmdesátých	osmdesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
dvacátého	dvacátý	k4xOgNnSc2
století	století	k1gNnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
dva	dva	k4xCgMnPc1
američtí	americký	k2eAgMnPc1d1
vědci	vědec	k1gMnPc1
<g/>
,	,	kIx,
a	a	k8xC
Walter	Walter	k1gMnSc1
Alvarez	Alvarez	k1gMnSc1
a	a	k8xC
jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
Luis	Luisa	k1gFnPc2
Walter	Walter	k1gMnSc1
Alvarez	Alvarez	k1gMnSc1
<g/>
,	,	kIx,
poukázali	poukázat	k5eAaPmAgMnP
na	na	k7c4
neobvykle	obvykle	k6eNd1
vysoké	vysoký	k2eAgNnSc4d1
množství	množství	k1gNnSc4
iridia	iridium	k1gNnSc2
nacházejícího	nacházející	k2eAgInSc2d1
se	se	k3xPyFc4
v	v	k7c6
sedimentech	sediment	k1gInPc6
z	z	k7c2
rozhraní	rozhraní	k1gNnSc2
K-Pg	K-Pga	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
jejich	jejich	k3xOp3gFnSc2
teorie	teorie	k1gFnSc2
dopadl	dopadnout	k5eAaPmAgInS
na	na	k7c4
Zemi	zem	k1gFnSc4
zhruba	zhruba	k6eAd1
před	před	k7c4
65	#num#	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
let	léto	k1gNnPc2
(	(	kIx(
<g/>
některé	některý	k3yIgInPc1
údaje	údaj	k1gInPc1
však	však	k9
hovoří	hovořit	k5eAaImIp3nP
spíše	spíše	k9
o	o	k7c6
66	#num#	k4
milionech	milion	k4xCgInPc6
let	léto	k1gNnPc2
<g/>
)	)	kIx)
gigantický	gigantický	k2eAgInSc1d1
asteroid	asteroid	k1gInSc1
o	o	k7c6
průměru	průměr	k1gInSc6
přesahujícím	přesahující	k2eAgInSc6d1
10	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
teorie	teorie	k1gFnPc4
měla	mít	k5eAaImAgFnS
však	však	k9
jeden	jeden	k4xCgInSc4
drobný	drobný	k2eAgInSc4d1
„	„	k?
<g/>
nedostatek	nedostatek	k1gInSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
by	by	kYmCp3nP
na	na	k7c6
Zemi	zem	k1gFnSc6
spadl	spadnout	k5eAaPmAgInS
asteroid	asteroid	k1gInSc4
(	(	kIx(
<g/>
a	a	k8xC
způsobil	způsobit	k5eAaPmAgMnS
takovou	takový	k3xDgFnSc4
katastrofu	katastrofa	k1gFnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
někde	někde	k6eAd1
by	by	kYmCp3nS
přece	přece	k9
musel	muset	k5eAaImAgMnS
být	být	k5eAaImF
nějaký	nějaký	k3yIgInSc4
kráter	kráter	k1gInSc4
nebo	nebo	k8xC
alespoň	alespoň	k9
pozůstatky	pozůstatek	k1gInPc1
po	po	k7c4
něm.	něm.	k?
Důkaz	důkaz	k1gInSc4
přišel	přijít	k5eAaPmAgMnS
v	v	k7c6
devadesátých	devadesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
mexického	mexický	k2eAgInSc2d1
poloostrova	poloostrov	k1gInSc2
Yucatán	Yucatán	k1gMnSc1
objevili	objevit	k5eAaPmAgMnP
geologové	geolog	k1gMnPc1
pozůstatky	pozůstatek	k1gInPc1
po	po	k7c6
obrovském	obrovský	k2eAgInSc6d1
kráteru	kráter	k1gInSc6
(	(	kIx(
<g/>
Chicxulub	Chicxulub	k1gMnSc1
<g/>
)	)	kIx)
o	o	k7c6
průměru	průměr	k1gInSc6
až	až	k9
200	#num#	k4
km	km	kA
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
odpovídal	odpovídat	k5eAaImAgMnS
i	i	k8xC
přibližným	přibližný	k2eAgNnSc7d1
geologickým	geologický	k2eAgNnSc7d1
stářím	stáří	k1gNnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
195	#num#	k4
<g/>
]	]	kIx)
Další	další	k2eAgInPc1d1
výzkumy	výzkum	k1gInPc1
ukazují	ukazovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
globální	globální	k2eAgNnSc4d1
podnebí	podnebí	k1gNnSc4
se	se	k3xPyFc4
po	po	k7c6
dopadu	dopad	k1gInSc6
drasticky	drasticky	k6eAd1
ochladilo	ochladit	k5eAaPmAgNnS
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
mohlo	moct	k5eAaImAgNnS
k	k	k7c3
vyhynutí	vyhynutí	k1gNnSc3
dinosaurů	dinosaurus	k1gMnPc2
významně	významně	k6eAd1
přispět	přispět	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
196	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lebka	lebka	k1gFnSc1
triceratopse	triceratops	k1gMnSc2
<g/>
,	,	kIx,
jednoho	jeden	k4xCgMnSc4
z	z	k7c2
posledních	poslední	k2eAgMnPc2d1
neptačích	ptačí	k2eNgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1
příčinou	příčina	k1gFnSc7
katastrofy	katastrofa	k1gFnSc2
byla	být	k5eAaImAgFnS
zřejmě	zřejmě	k6eAd1
kombinace	kombinace	k1gFnSc1
událostí	událost	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
proběhly	proběhnout	k5eAaPmAgInP
v	v	k7c6
relativně	relativně	k6eAd1
krátkém	krátký	k2eAgInSc6d1
<g/>
,	,	kIx,
a	a	k8xC
jak	jak	k8xC,k8xS
se	se	k3xPyFc4
zdá	zdát	k5eAaPmIp3nS,k5eAaImIp3nS
<g/>
,	,	kIx,
geologicky	geologicky	k6eAd1
velice	velice	k6eAd1
aktivním	aktivní	k2eAgInSc6d1
časovém	časový	k2eAgInSc6d1
úseku	úsek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příčin	příčina	k1gFnPc2
bylo	být	k5eAaImAgNnS
hned	hned	k6eAd1
několik	několik	k4yIc1
a	a	k8xC
šlo	jít	k5eAaImAgNnS
o	o	k7c4
složitější	složitý	k2eAgInPc4d2
komplex	komplex	k1gInSc4
jevů	jev	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dominantní	dominantní	k2eAgFnSc4d1
roli	role	k1gFnSc4
měl	mít	k5eAaImAgInS
však	však	k9
pravděpodobně	pravděpodobně	k6eAd1
dopad	dopad	k1gInSc4
planetky	planetka	k1gFnSc2
Chicxulub	Chicxuluba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
197	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
198	#num#	k4
<g/>
]	]	kIx)
Hlavním	hlavní	k2eAgInSc7d1
mechanismem	mechanismus	k1gInSc7
vymírání	vymírání	k1gNnSc2
mohlo	moct	k5eAaImAgNnS
být	být	k5eAaImF
drastické	drastický	k2eAgNnSc4d1
ochlazení	ochlazení	k1gNnSc4
trvající	trvající	k2eAgInPc4d1
roky	rok	k1gInPc4
až	až	k8xS
desetiletí	desetiletí	k1gNnPc4
po	po	k7c6
dopadu	dopad	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
průměrná	průměrný	k2eAgFnSc1d1
teplota	teplota	k1gFnSc1
na	na	k7c6
planetě	planeta	k1gFnSc6
snížila	snížit	k5eAaPmAgFnS
ze	z	k7c2
zhruba	zhruba	k6eAd1
25	#num#	k4
°	°	k?
<g/>
C	C	kA
<g />
.	.	kIx.
</s>
<s hack="1">
na	na	k7c4
mrazivých	mrazivý	k2eAgNnPc2d1
−	−	k?
°	°	k?
<g/>
C.	C.	kA
<g/>
[	[	kIx(
<g/>
199	#num#	k4
<g/>
]	]	kIx)
Ukazuje	ukazovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
dinosauři	dinosaurus	k1gMnPc1
rozhodně	rozhodně	k6eAd1
nebyli	být	k5eNaImAgMnP
na	na	k7c6
ústupu	ústup	k1gInSc6
a	a	k8xC
nevymírali	vymírat	k5eNaImAgMnP
již	již	k6eAd1
dlouho	dlouho	k6eAd1
před	před	k7c7
touto	tento	k3xDgFnSc7
událostí	událost	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
200	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
201	#num#	k4
<g/>
]	]	kIx)
Dříve	dříve	k6eAd2
se	se	k3xPyFc4
přitom	přitom	k6eAd1
objevovaly	objevovat	k5eAaImAgFnP
domněnky	domněnka	k1gFnPc1
<g/>
,	,	kIx,
že	že	k8xS
dinosauři	dinosaurus	k1gMnPc1
jako	jako	k8xC,k8xS
skupina	skupina	k1gFnSc1
byli	být	k5eAaImAgMnP
na	na	k7c6
ústupu	ústup	k1gInSc6
již	již	k6eAd1
relativně	relativně	k6eAd1
dlouho	dlouho	k6eAd1
před	před	k7c7
hranicí	hranice	k1gFnSc7
K-Pg	K-Pg	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
202	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
203	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Některé	některý	k3yIgFnPc1
odborné	odborný	k2eAgFnPc1d1
práce	práce	k1gFnPc1
postulují	postulovat	k5eAaImIp3nP
možnost	možnost	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
neptačí	ptačí	k2eNgMnPc1d1
dinosauři	dinosaurus	k1gMnPc1
mohli	moct	k5eAaImAgMnP
přežít	přežít	k5eAaPmF
až	až	k9
desítky	desítka	k1gFnPc4
nebo	nebo	k8xC
i	i	k8xC
stovky	stovka	k1gFnPc4
tisíc	tisíc	k4xCgInPc2
let	léto	k1gNnPc2
do	do	k7c2
paleogénu	paleogén	k1gInSc2
(	(	kIx(
<g/>
obecně	obecně	k6eAd1
však	však	k9
nejsou	být	k5eNaImIp3nP
přijímané	přijímaný	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
204	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
byla	být	k5eAaImAgFnS
publikována	publikován	k2eAgFnSc1d1
studie	studie	k1gFnSc1
<g/>
,	,	kIx,
zpřesňující	zpřesňující	k2eAgInPc1d1
dosavadní	dosavadní	k2eAgInPc1d1
odhady	odhad	k1gInPc1
doby	doba	k1gFnSc2
vymření	vymření	k1gNnSc2
dinosaurů	dinosaurus	k1gMnPc2
(	(	kIx(
<g/>
katastrofu	katastrofa	k1gFnSc4
K-Pg	K-Pga	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vylepšená	vylepšený	k2eAgFnSc1d1
datovací	datovací	k2eAgFnSc1d1
metoda	metoda	k1gFnSc1
argon-argon	argon-argona	k1gFnPc2
s	s	k7c7
odchylkou	odchylka	k1gFnSc7
pouhých	pouhý	k2eAgInPc2d1
0,25	0,25	k4
%	%	kIx~
určila	určit	k5eAaPmAgFnS
dobu	doba	k1gFnSc4
předělu	předěl	k1gInSc2
druhohor	druhohory	k1gFnPc2
a	a	k8xC
třetihor	třetihory	k1gFnPc2
na	na	k7c4
65	#num#	k4
950	#num#	k4
000	#num#	k4
let	léto	k1gNnPc2
(	(	kIx(
<g/>
±	±	k?
40	#num#	k4
000	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
205	#num#	k4
<g/>
]	]	kIx)
Ještě	ještě	k6eAd1
novější	nový	k2eAgInPc4d2
údaje	údaj	k1gInPc4
z	z	k7c2
roku	rok	k1gInSc2
2013	#num#	k4
a	a	k8xC
2014	#num#	k4
hovoří	hovořit	k5eAaImIp3nP
o	o	k7c6
66,04	66,04	k4
až	až	k9
66,23	66,23	k4
milionu	milion	k4xCgInSc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Občas	občas	k6eAd1
se	se	k3xPyFc4
také	také	k9
objevují	objevovat	k5eAaImIp3nP
vědecky	vědecky	k6eAd1
nepodložené	podložený	k2eNgFnPc1d1
spekulace	spekulace	k1gFnPc1
o	o	k7c6
možném	možný	k2eAgInSc6d1
dalším	další	k2eAgInSc6d1
vývoji	vývoj	k1gInSc6
dinosaurů	dinosaurus	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
některých	některý	k3yIgMnPc2
vědců	vědec	k1gMnPc2
by	by	kYmCp3nP
se	se	k3xPyFc4
někteří	některý	k3yIgMnPc1
teropodní	teropodní	k2eAgMnPc1d1
dinosauři	dinosaurus	k1gMnPc1
změnili	změnit	k5eAaPmAgMnP
v	v	k7c6
inteligentní	inteligentní	k2eAgFnSc6d1
bytosti	bytost	k1gFnSc6
<g/>
,	,	kIx,
tvořící	tvořící	k2eAgFnSc4d1
kulturu	kultura	k1gFnSc4
a	a	k8xC
vlastní	vlastní	k2eAgFnSc3d1
civilizaci	civilizace	k1gFnSc3
–	–	k?
tzv.	tzv.	kA
dinosauroidy	dinosauroida	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
206	#num#	k4
<g/>
]	]	kIx)
Pro	pro	k7c4
tyto	tento	k3xDgFnPc4
spekulace	spekulace	k1gFnPc4
je	být	k5eAaImIp3nS
podkladem	podklad	k1gInSc7
relativně	relativně	k6eAd1
velká	velký	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
dinosaurů	dinosaurus	k1gMnPc2
čeledí	čeleď	k1gFnPc2
dromaeosauridae	dromaeosaurida	k1gInSc2
a	a	k8xC
troodontidae	troodontida	k1gInSc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgMnPc2
by	by	kYmCp3nP
se	se	k3xPyFc4
(	(	kIx(
<g/>
nebýt	být	k5eNaImF
oné	onen	k3xDgFnSc2
katastrofy	katastrofa	k1gFnSc2
<g/>
)	)	kIx)
mohl	moct	k5eAaImAgMnS
v	v	k7c6
dalším	další	k2eAgInSc6d1
vývoji	vývoj	k1gInSc6
takový	takový	k3xDgInSc4
dinosauroid	dinosauroid	k1gInSc4
vyvinout	vyvinout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
ale	ale	k9
o	o	k7c4
pouhé	pouhý	k2eAgFnPc4d1
nepodložené	podložený	k2eNgFnPc4d1
spekulace	spekulace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
207	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Otázka	otázka	k1gFnSc1
třetihorních	třetihorní	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
</s>
<s>
Již	již	k6eAd1
dlouho	dlouho	k6eAd1
vědci	vědec	k1gMnPc1
spekulují	spekulovat	k5eAaImIp3nP
o	o	k7c6
možném	možný	k2eAgNnSc6d1
přežití	přežití	k1gNnSc6
dinosaurů	dinosaurus	k1gMnPc2
do	do	k7c2
paleocénu	paleocén	k1gInSc2
(	(	kIx(
<g/>
nejstaršího	starý	k2eAgNnSc2d3
období	období	k1gNnSc2
třetihor	třetihory	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
studie	studie	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
2007	#num#	k4
(	(	kIx(
<g/>
Fassett	Fassetta	k1gFnPc2
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
pak	pak	k6eAd1
skutečně	skutečně	k6eAd1
neptačí	ptačí	k2eNgMnPc1d1
dinosauři	dinosaurus	k1gMnPc1
kritickou	kritický	k2eAgFnSc4d1
hranici	hranice	k1gFnSc4
K-Pg	K-Pga	k1gFnPc2
přežili	přežít	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alespoň	alespoň	k9
na	na	k7c4
území	území	k1gNnSc4
dnešního	dnešní	k2eAgNnSc2d1
Nového	Nového	k2eAgNnSc2d1
Mexika	Mexiko	k1gNnSc2
a	a	k8xC
Colorada	Colorado	k1gNnSc2
v	v	k7c6
USA	USA	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
jejich	jejich	k3xOp3gFnPc1
zkameněliny	zkamenělina	k1gFnPc1
pravděpodobně	pravděpodobně	k6eAd1
nacházely	nacházet	k5eAaImAgFnP
ještě	ještě	k9
před	před	k7c7
64,4	64,4	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
(	(	kIx(
<g/>
a	a	k8xC
přežili	přežít	k5eAaPmAgMnP
tak	tak	k6eAd1
minimálně	minimálně	k6eAd1
1,1	1,1	k4
milionu	milion	k4xCgInSc2
let	léto	k1gNnPc2
po	po	k7c6
skončení	skončení	k1gNnSc6
druhohor	druhohory	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
jiných	jiný	k2eAgMnPc2d1
paleontologů	paleontolog	k1gMnPc2
však	však	k9
tyto	tento	k3xDgInPc1
údaje	údaj	k1gInPc1
nejsou	být	k5eNaImIp3nP
pravdivé	pravdivý	k2eAgInPc1d1
a	a	k8xC
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
křídové	křídový	k2eAgFnPc4d1
fosílie	fosílie	k1gFnPc4
druhotně	druhotně	k6eAd1
uložené	uložený	k2eAgInPc1d1
již	již	k6eAd1
v	v	k7c6
třetihorním	třetihorní	k2eAgNnSc6d1
období	období	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přežití	přežití	k1gNnSc1
alespoň	alespoň	k9
některých	některý	k3yIgMnPc2
neptačích	ptačí	k2eNgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
do	do	k7c2
paleocénu	paleocén	k1gInSc2
po	po	k7c4
dobu	doba	k1gFnSc4
alespoň	alespoň	k9
několika	několik	k4yIc2
set	sto	k4xCgNnPc2
tisíc	tisíc	k4xCgInSc1
let	léto	k1gNnPc2
je	být	k5eAaImIp3nS
nicméně	nicméně	k8xC
velmi	velmi	k6eAd1
pravděpodobné	pravděpodobný	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
208	#num#	k4
<g/>
]	]	kIx)
Nálezy	nález	k1gInPc7
z	z	k7c2
Montany	Montana	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
měly	mít	k5eAaImAgFnP
tomuto	tento	k3xDgInSc3
scénáři	scénář	k1gInSc3
nasvědčovat	nasvědčovat	k5eAaImF
<g/>
,	,	kIx,
ale	ale	k8xC
dnes	dnes	k6eAd1
nejsou	být	k5eNaImIp3nP
obecně	obecně	k6eAd1
uznávány	uznáván	k2eAgFnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
209	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dlouho	dlouho	k6eAd1
se	se	k3xPyFc4
předpokládalo	předpokládat	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
žádní	žádný	k3yNgMnPc1
dinosauři	dinosaurus	k1gMnPc1
nedokázali	dokázat	k5eNaPmAgMnP
hrabat	hrabat	k5eAaImF
nory	nora	k1gFnPc4
a	a	k8xC
dlouhodobě	dlouhodobě	k6eAd1
je	být	k5eAaImIp3nS
obývat	obývat	k5eAaImF
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
bylo	být	k5eAaImAgNnS
pokládáno	pokládat	k5eAaImNgNnS
za	za	k7c4
jedno	jeden	k4xCgNnSc4
z	z	k7c2
vysvětlení	vysvětlení	k1gNnSc2
jejich	jejich	k3xOp3gNnSc2
vyhynutí	vyhynutí	k1gNnSc2
(	(	kIx(
<g/>
zachránit	zachránit	k5eAaPmF
se	se	k3xPyFc4
měli	mít	k5eAaImAgMnP
jen	jen	k9
savci	savec	k1gMnPc1
a	a	k8xC
plazi	plaz	k1gMnPc1
schopní	schopný	k2eAgMnPc1d1
„	„	k?
<g/>
norování	norování	k1gNnSc6
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
však	však	k9
byl	být	k5eAaImAgMnS
objeven	objeven	k2eAgMnSc1d1
malý	malý	k2eAgMnSc1d1
dinosaurus	dinosaurus	k1gMnSc1
druhu	druh	k1gInSc2
Oryctodromeus	Oryctodromeus	k1gInSc1
cubicularis	cubicularis	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
prokazatelně	prokazatelně	k6eAd1
dokázal	dokázat	k5eAaPmAgInS
hrabat	hrabat	k5eAaImF
nory	nora	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takových	takový	k3xDgInPc2
druhů	druh	k1gInPc2
dinosaurů	dinosaurus	k1gMnPc2
zřejmě	zřejmě	k6eAd1
i	i	k9
na	na	k7c6
konci	konec	k1gInSc6
křídy	křída	k1gFnSc2
mohlo	moct	k5eAaImAgNnS
být	být	k5eAaImF
více	hodně	k6eAd2
<g/>
,	,	kIx,
takže	takže	k8xS
možnost	možnost	k1gFnSc4
jejich	jejich	k3xOp3gNnSc2
přežití	přežití	k1gNnSc2
do	do	k7c2
starších	starý	k2eAgFnPc2d2
třetihor	třetihory	k1gFnPc2
se	se	k3xPyFc4
tak	tak	k9
alespoň	alespoň	k9
v	v	k7c6
teoretické	teoretický	k2eAgFnSc6d1
rovině	rovina	k1gFnSc6
zvýšila	zvýšit	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
210	#num#	k4
<g/>
]	]	kIx)
Podobné	podobný	k2eAgInPc4d1
objevy	objev	k1gInPc4
přicházejí	přicházet	k5eAaImIp3nP
také	také	k9
z	z	k7c2
Austrálie	Austrálie	k1gFnSc2
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
jde	jít	k5eAaImIp3nS
o	o	k7c4
dílo	dílo	k1gNnSc4
jiných	jiný	k2eAgInPc2d1
malých	malý	k2eAgInPc2d1
ornitopodů	ornitopod	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
211	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
<g/>
,	,	kIx,
dinosauři	dinosaurus	k1gMnPc1
vlastně	vlastně	k9
zcela	zcela	k6eAd1
nevymřeli	vymřít	k5eNaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
skupina	skupina	k1gFnSc1
teropodů	teropod	k1gMnPc2
<g/>
,	,	kIx,
ptáci	pták	k1gMnPc1
<g/>
,	,	kIx,
žijí	žít	k5eAaImIp3nP
na	na	k7c4
Zemi	zem	k1gFnSc4
dodnes	dodnes	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgFnSc7d1
nesmyslnou	smyslný	k2eNgFnSc7d1
teorií	teorie	k1gFnSc7
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
dinosauři	dinosaurus	k1gMnPc1
přežili	přežít	k5eAaPmAgMnP
až	až	k9
dodnes	dodnes	k6eAd1
jako	jako	k9
takoví	takový	k3xDgMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
teorii	teorie	k1gFnSc4
zdánlivě	zdánlivě	k6eAd1
podporují	podporovat	k5eAaImIp3nP
určitá	určitý	k2eAgNnPc4d1
svědectví	svědectví	k1gNnPc4
o	o	k7c6
údajných	údajný	k2eAgNnPc6d1
pozorováních	pozorování	k1gNnPc6
podobných	podobný	k2eAgMnPc2d1
tvorů	tvor	k1gMnPc2
z	z	k7c2
Afriky	Afrika	k1gFnSc2
a	a	k8xC
Jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
212	#num#	k4
<g/>
]	]	kIx)
Tato	tento	k3xDgFnSc1
teorie	teorie	k1gFnSc1
není	být	k5eNaImIp3nS
mezi	mezi	k7c7
vědci	vědec	k1gMnPc7
příliš	příliš	k6eAd1
oblíbená	oblíbený	k2eAgFnSc1d1
a	a	k8xC
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
spekulaci	spekulace	k1gFnSc4
bez	bez	k7c2
nezvratných	zvratný	k2eNgInPc2d1
důkazů	důkaz	k1gInPc2
<g/>
,	,	kIx,
spadající	spadající	k2eAgMnSc1d1
spíše	spíše	k9
do	do	k7c2
oboru	obor	k1gInSc2
kryptozoologie	kryptozoologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existují	existovat	k5eAaImIp3nP
nicméně	nicméně	k8xC
podložené	podložený	k2eAgFnPc1d1
spekulace	spekulace	k1gFnPc1
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
by	by	kYmCp3nP
se	se	k3xPyFc4
dinosauři	dinosaurus	k1gMnPc1
mohli	moct	k5eAaImAgMnP
vyvíjet	vyvíjet	k5eAaImF
dál	daleko	k6eAd2
<g/>
,	,	kIx,
kdyby	kdyby	kYmCp3nS
na	na	k7c6
konci	konec	k1gInSc6
křídy	křída	k1gFnSc2
nevyhynuli	vyhynout	k5eNaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
213	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odraz	odraz	k1gInSc1
v	v	k7c6
kultuře	kultura	k1gFnSc6
</s>
<s>
Dinosauři	dinosaurus	k1gMnPc1
v	v	k7c6
klasické	klasický	k2eAgFnSc6d1
literatuře	literatura	k1gFnSc6
</s>
<s>
První	první	k4xOgFnSc1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
dinosaurech	dinosaurus	k1gMnPc6
v	v	k7c6
klasické	klasický	k2eAgFnSc6d1
literatuře	literatura	k1gFnSc6
se	se	k3xPyFc4
týká	týkat	k5eAaImIp3nS
nikoliv	nikoliv	k9
překvapivě	překvapivě	k6eAd1
teropoda	teropoda	k1gFnSc1
megalosaura	megalosaura	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgMnSc1
dravý	dravý	k2eAgMnSc1d1
dinosaurus	dinosaurus	k1gMnSc1
se	se	k3xPyFc4
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1852	#num#	k4
objevil	objevit	k5eAaPmAgMnS
jako	jako	k9
element	element	k1gInSc4
pro	pro	k7c4
dokreslení	dokreslení	k1gNnSc4
temné	temný	k2eAgFnSc2d1
a	a	k8xC
nepříznivé	příznivý	k2eNgFnSc2d1
atmosféry	atmosféra	k1gFnSc2
v	v	k7c6
díle	dílo	k1gNnSc6
britského	britský	k2eAgMnSc2d1
spisovatele	spisovatel	k1gMnSc2
Charlese	Charles	k1gMnSc2
Dickense	Dickens	k1gMnSc2
Ponurý	ponurý	k2eAgInSc4d1
dům	dům	k1gInSc4
(	(	kIx(
<g/>
v	v	k7c4
orig	orig	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bleak	Bleak	k1gMnSc1
House	house	k1gNnSc1
<g/>
,	,	kIx,
1852	#num#	k4
<g/>
–	–	k?
<g/>
1853	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
214	#num#	k4
<g/>
]	]	kIx)
Dinosauři	dinosaurus	k1gMnPc1
bývají	bývat	k5eAaImIp3nP
často	často	k6eAd1
zobrazováni	zobrazován	k2eAgMnPc1d1
také	také	k9
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
vesmírem	vesmír	k1gInSc7
a	a	k8xC
astronomií	astronomie	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
215	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
první	první	k4xOgFnSc6
polovině	polovina	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
například	například	k6eAd1
objevily	objevit	k5eAaPmAgFnP
dnes	dnes	k6eAd1
již	již	k6eAd1
<g />
.	.	kIx.
</s>
<s hack="1">
zcela	zcela	k6eAd1
vyvrácené	vyvrácený	k2eAgFnPc4d1
hypotézy	hypotéza	k1gFnPc4
o	o	k7c6
možné	možný	k2eAgFnSc6d1
existenci	existence	k1gFnSc6
dinosaurů	dinosaurus	k1gMnPc2
na	na	k7c6
(	(	kIx(
<g/>
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
povrchově	povrchově	k6eAd1
nezmapované	zmapovaný	k2eNgFnSc6d1
<g/>
)	)	kIx)
sousední	sousední	k2eAgFnSc6d1
planetě	planeta	k1gFnSc6
Venuši	Venuše	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
216	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
také	také	k9
dobře	dobře	k6eAd1
známé	známý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
dinosauří	dinosauří	k2eAgFnPc1d1
fosilie	fosilie	k1gFnPc1
sehrály	sehrát	k5eAaPmAgFnP
významnou	významný	k2eAgFnSc4d1
úlohu	úloha	k1gFnSc4
v	v	k7c6
mytologii	mytologie	k1gFnSc6
a	a	k8xC
legendách	legenda	k1gFnPc6
různých	různý	k2eAgInPc2d1
národů	národ	k1gInPc2
dlouho	dlouho	k6eAd1
před	před	k7c7
vznikem	vznik	k1gInSc7
vědecké	vědecký	k2eAgFnSc2d1
paleontologie	paleontologie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
217	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Scéna	scéna	k1gFnSc1
ze	z	k7c2
svrchní	svrchní	k2eAgFnSc2d1
jury	jura	k1gFnSc2
severního	severní	k2eAgNnSc2d1
Německa	Německo	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
popředí	popředí	k1gNnSc6
„	„	k?
<g/>
trpasličí	trpasličí	k2eAgInSc1d1
<g/>
“	“	k?
sauropod	sauropod	k1gInSc1
Europasaurus	Europasaurus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Dinosauři	dinosaurus	k1gMnPc1
v	v	k7c6
populární	populární	k2eAgFnSc6d1
kultuře	kultura	k1gFnSc6
</s>
<s>
Viz	vidět	k5eAaImRp2nS
také	také	k9
články	článek	k1gInPc4
Dinosauři	dinosaurus	k1gMnPc1
v	v	k7c6
populární	populární	k2eAgFnSc6d1
kultuře	kultura	k1gFnSc6
<g/>
,	,	kIx,
Tyrannosaurus	Tyrannosaurus	k1gInSc1
v	v	k7c6
populární	populární	k2eAgFnSc6d1
kultuře	kultura	k1gFnSc6
a	a	k8xC
Stegosaurus	Stegosaurus	k1gInSc4
v	v	k7c6
populární	populární	k2eAgFnSc6d1
kultuře	kultura	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Dinosauři	dinosaurus	k1gMnPc1
jsou	být	k5eAaImIp3nP
předmětem	předmět	k1gInSc7
zájmu	zájem	k1gInSc2
a	a	k8xC
obliby	obliba	k1gFnSc2
značného	značný	k2eAgNnSc2d1
množství	množství	k1gNnSc2
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
218	#num#	k4
<g/>
]	]	kIx)
Nejvíce	hodně	k6eAd3,k6eAd1
jimi	on	k3xPp3gFnPc7
bývají	bývat	k5eAaImIp3nP
fascinovány	fascinován	k2eAgFnPc4d1
děti	dítě	k1gFnPc4
<g/>
,	,	kIx,
u	u	k7c2
mnohých	mnohé	k1gNnPc2
z	z	k7c2
nich	on	k3xPp3gMnPc2
se	se	k3xPyFc4
dinosauři	dinosaurus	k1gMnPc1
stanou	stanout	k5eAaPmIp3nP
na	na	k7c4
delší	dlouhý	k2eAgFnSc4d2
dobu	doba	k1gFnSc4
intenzivním	intenzivní	k2eAgInSc7d1
zájem	zájem	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Psychologové	psycholog	k1gMnPc1
dokonce	dokonce	k9
popisují	popisovat	k5eAaImIp3nP
cosi	cosi	k3yInSc4
jako	jako	k8xS,k8xC
„	„	k?
<g/>
dinosauří	dinosauří	k2eAgNnSc1d1
období	období	k1gNnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
jakožto	jakožto	k8xS
vývojovou	vývojový	k2eAgFnSc4d1
fázi	fáze	k1gFnSc4
psychiky	psychika	k1gFnSc2
mnoha	mnoho	k4c2
dětí	dítě	k1gFnPc2
zhruba	zhruba	k6eAd1
mezi	mezi	k7c7
2	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
6	#num#	k4
<g/>
.	.	kIx.
rokem	rok	k1gInSc7
věku	věk	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
219	#num#	k4
<g/>
]	]	kIx)
První	první	k4xOgNnSc1
vzedmutí	vzedmutí	k1gNnSc1
popularity	popularita	k1gFnSc2
dinosaurů	dinosaurus	k1gMnPc2
nastalo	nastat	k5eAaPmAgNnS
zřejmě	zřejmě	k6eAd1
v	v	k7c6
50	#num#	k4
<g/>
.	.	kIx.
letech	let	k1gInPc6
19	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byly	být	k5eAaImAgInP
v	v	k7c6
Seydenhamu	Seydenham	k1gInSc6
nedaleko	nedaleko	k7c2
Londýna	Londýn	k1gInSc2
umístěny	umístit	k5eAaPmNgInP
velmi	velmi	k6eAd1
nepřesné	přesný	k2eNgInPc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
impozantní	impozantní	k2eAgInPc1d1
betonové	betonový	k2eAgInPc1d1
modely	model	k1gInPc1
dinosaurů	dinosaurus	k1gMnPc2
v	v	k7c6
životní	životní	k2eAgFnSc6d1
velikosti	velikost	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
220	#num#	k4
<g/>
]	]	kIx)
Obecný	obecný	k2eAgInSc1d1
pohled	pohled	k1gInSc1
na	na	k7c4
dinosaury	dinosaurus	k1gMnPc4
(	(	kIx(
<g/>
i	i	k9
na	na	k7c6
jejich	jejich	k3xOp3gNnSc6
vzezření	vzezření	k1gNnSc6
a	a	k8xC
ekologii	ekologie	k1gFnSc6
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
stále	stále	k6eAd1
u	u	k7c2
většiny	většina	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
populace	populace	k1gFnSc2
poněkud	poněkud	k6eAd1
zastaralý	zastaralý	k2eAgInSc1d1
a	a	k8xC
nerespektuje	respektovat	k5eNaImIp3nS
nové	nový	k2eAgInPc4d1
vědecké	vědecký	k2eAgInPc4d1
poznatky	poznatek	k1gInPc4
o	o	k7c6
nich	on	k3xPp3gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
221	#num#	k4
<g/>
]	]	kIx)
Setkáváme	setkávat	k5eAaImIp1nP
se	se	k3xPyFc4
tak	tak	k9
s	s	k7c7
nesprávnými	správný	k2eNgInPc7d1
údaji	údaj	k1gInPc7
v	v	k7c6
hračkářstvích	hračkářství	k1gNnPc6
<g/>
,	,	kIx,
filmech	film	k1gInPc6
<g/>
,	,	kIx,
starších	starý	k2eAgFnPc6d2
nebo	nebo	k8xC
dětských	dětský	k2eAgFnPc6d1
knihách	kniha	k1gFnPc6
<g/>
,	,	kIx,
nebo	nebo	k8xC
běžných	běžný	k2eAgFnPc6d1
diskuzích	diskuze	k1gFnPc6
(	(	kIx(
<g/>
kdy	kdy	k6eAd1
jsou	být	k5eAaImIp3nP
například	například	k6eAd1
s	s	k7c7
dinosaury	dinosaurus	k1gMnPc7
zaměňováni	zaměňován	k2eAgMnPc1d1
jiní	jiný	k2eAgMnPc1d1
pravěcí	pravěký	k2eAgMnPc1d1
obratlovci	obratlovec	k1gMnPc1
jako	jako	k8xS,k8xC
ptakoještěři	ptakoještěr	k1gMnPc1
<g/>
,	,	kIx,
mosasauři	mosasaur	k1gMnPc1
<g/>
,	,	kIx,
pelykosauři	pelykosaur	k1gMnPc1
<g/>
,	,	kIx,
plesiosauři	plesiosaur	k1gMnPc1
atd.	atd.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
222	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Největší	veliký	k2eAgInSc1d3
posun	posun	k1gInSc1
ve	v	k7c6
vnímání	vnímání	k1gNnSc6
dinosaurů	dinosaurus	k1gMnPc2
nastal	nastat	k5eAaPmAgInS
v	v	k7c6
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
začíná	začínat	k5eAaImIp3nS
období	období	k1gNnSc4
tzv.	tzv.	kA
dinosauří	dinosauří	k2eAgFnSc2d1
renesance	renesance	k1gFnSc2
<g/>
[	[	kIx(
<g/>
223	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dinosauři	dinosaurus	k1gMnPc1
se	se	k3xPyFc4
prakticky	prakticky	k6eAd1
od	od	k7c2
počátku	počátek	k1gInSc2
dějin	dějiny	k1gFnPc2
kinematografie	kinematografie	k1gFnSc2
na	na	k7c6
počátku	počátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
stali	stát	k5eAaPmAgMnP
její	její	k3xOp3gFnSc7
nedílnou	dílný	k2eNgFnSc7d1
součástí	součást	k1gFnSc7
a	a	k8xC
jsou	být	k5eAaImIp3nP
jí	on	k3xPp3gFnSc7
stále	stále	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
224	#num#	k4
<g/>
]	]	kIx)
Pochopitelný	pochopitelný	k2eAgInSc1d1
je	být	k5eAaImIp3nS
také	také	k9
komerční	komerční	k2eAgInSc1d1
potenciál	potenciál	k1gInSc1
dinosaurů	dinosaurus	k1gMnPc2
<g/>
,	,	kIx,
ať	ať	k8xS,k8xC
již	již	k6eAd1
v	v	k7c6
podobě	podoba	k1gFnSc6
prodeje	prodej	k1gInSc2
skutečných	skutečný	k2eAgFnPc2d1
fosilií	fosilie	k1gFnPc2
(	(	kIx(
<g/>
často	často	k6eAd1
na	na	k7c6
černém	černý	k2eAgInSc6d1
trhu	trh	k1gInSc6
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
nebo	nebo	k8xC
například	například	k6eAd1
prodej	prodej	k1gInSc1
replik	replika	k1gFnPc2
koster	kostra	k1gFnPc2
velkých	velký	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
225	#num#	k4
<g/>
]	]	kIx)
Zajímavá	zajímavý	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
také	také	k9
historie	historie	k1gFnSc1
zobrazování	zobrazování	k1gNnSc2
dinosaurů	dinosaurus	k1gMnPc2
(	(	kIx(
<g/>
„	„	k?
<g/>
paleoart	paleoart	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
tradice	tradice	k1gFnSc1
sahá	sahat	k5eAaImIp3nS
až	až	k9
do	do	k7c2
první	první	k4xOgFnSc2
třetiny	třetina	k1gFnSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
a	a	k8xC
v	v	k7c6
níž	jenž	k3xRgFnSc6
se	se	k3xPyFc4
udál	udát	k5eAaPmAgMnS
<g />
.	.	kIx.
</s>
<s hack="1">
velmi	velmi	k6eAd1
výrazný	výrazný	k2eAgInSc1d1
posun	posun	k1gInSc1
zejména	zejména	k9
v	v	k7c6
posledních	poslední	k2eAgNnPc6d1
desetiletích	desetiletí	k1gNnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
226	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c4
dlouhou	dlouhý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
byli	být	k5eAaImAgMnP
dinosauři	dinosaurus	k1gMnPc1
zobrazováni	zobrazován	k2eAgMnPc1d1
jako	jako	k8xC,k8xS
strach	strach	k1gInSc4
nahánějící	nahánějící	k2eAgNnPc4d1
monstra	monstrum	k1gNnPc4
z	z	k7c2
legend	legenda	k1gFnPc2
a	a	k8xC
mýtů	mýtus	k1gInPc2
<g/>
,	,	kIx,
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
však	však	k9
byl	být	k5eAaImAgInS
jejich	jejich	k3xOp3gInSc4
vzhled	vzhled	k1gInSc4
a	a	k8xC
chování	chování	k1gNnSc4
mnohem	mnohem	k6eAd1
podobnější	podobný	k2eAgNnSc4d2
skutečným	skutečný	k2eAgInPc3d1
tvorům	tvor	k1gMnPc3
ze	z	k7c2
současnosti	současnost	k1gFnSc2
(	(	kIx(
<g/>
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
velcí	velký	k2eAgMnPc1d1
ptáci	pták	k1gMnPc1
a	a	k8xC
plazi	plaz	k1gMnPc1
<g/>
,	,	kIx,
případně	případně	k6eAd1
i	i	k9
savci	savec	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
227	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
228	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Patrně	patrně	k6eAd1
nejznámějším	známý	k2eAgInSc7d3
a	a	k8xC
nejpopulárnějším	populární	k2eAgInSc7d3
druhem	druh	k1gInSc7
dinosaura	dinosaurus	k1gMnSc2
a	a	k8xC
snad	snad	k9
i	i	k9
pravěkého	pravěký	k2eAgMnSc2d1
živočicha	živočich	k1gMnSc2
vůbec	vůbec	k9
je	být	k5eAaImIp3nS
obří	obří	k2eAgInSc4d1
teropod	teropod	k1gInSc4
Tyrannosaurus	Tyrannosaurus	k1gMnSc1
rex	rex	k?
<g/>
,	,	kIx,
formálně	formálně	k6eAd1
popsaný	popsaný	k2eAgInSc1d1
roku	rok	k1gInSc3
1905	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
natrvalo	natrvalo	k6eAd1
součástí	součást	k1gFnSc7
populární	populární	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
a	a	k8xC
objevil	objevit	k5eAaPmAgInS
se	se	k3xPyFc4
v	v	k7c6
mnoha	mnoho	k4c6
filmech	film	k1gInPc6
<g/>
,	,	kIx,
na	na	k7c6
nesčetných	sčetný	k2eNgFnPc6d1
malbách	malba	k1gFnPc6
a	a	k8xC
ilustracích	ilustrace	k1gFnPc6
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gFnPc1
kosterní	kosterní	k2eAgFnPc1d1
repliky	replika	k1gFnPc1
a	a	k8xC
modely	model	k1gInPc1
se	se	k3xPyFc4
objevily	objevit	k5eAaPmAgInP
ve	v	k7c6
stovkách	stovka	k1gFnPc6
muzeí	muzeum	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
229	#num#	k4
<g/>
]	]	kIx)
Největší	veliký	k2eAgInSc1d3
model	model	k1gInSc1
dinosaura	dinosaurus	k1gMnSc2
<g/>
,	,	kIx,
rovněž	rovněž	k9
tyranosaura	tyranosaura	k1gFnSc1
<g/>
,	,	kIx,
stojí	stát	k5eAaImIp3nS
od	od	k7c2
podzimu	podzim	k1gInSc2
roku	rok	k1gInSc2
2000	#num#	k4
v	v	k7c6
kanadském	kanadský	k2eAgInSc6d1
Drumhelleru	Drumheller	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c6
zpodobnění	zpodobnění	k1gNnSc6
slavného	slavný	k2eAgMnSc2d1
teropoda	teropod	k1gMnSc2
v	v	k7c6
nadživotní	nadživotní	k2eAgFnSc6d1
velikosti	velikost	k1gFnSc6
<g/>
,	,	kIx,
s	s	k7c7
výškou	výška	k1gFnSc7
26	#num#	k4
metrů	metr	k1gInPc2
a	a	k8xC
hmotností	hmotnost	k1gFnSc7
téměř	téměř	k6eAd1
66	#num#	k4
tun	tuna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
230	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mezi	mezi	k7c4
známé	známý	k2eAgMnPc4d1
popularizátory	popularizátor	k1gMnPc4
tematiky	tematika	k1gFnSc2
dinosaurů	dinosaurus	k1gMnPc2
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
patří	patřit	k5eAaImIp3nS
dlouhodobě	dlouhodobě	k6eAd1
například	například	k6eAd1
Jaroslav	Jaroslav	k1gMnSc1
Mareš	Mareš	k1gMnSc1
(	(	kIx(
<g/>
1937	#num#	k4
<g/>
-	-	kIx~
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
231	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
Vladimír	Vladimír	k1gMnSc1
Socha	Socha	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
232	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
233	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pohled	pohled	k1gInSc1
na	na	k7c4
modely	model	k1gInPc4
dinosaurů	dinosaurus	k1gMnPc2
v	v	k7c6
JuraParku	JuraPark	k1gInSc6
Krasiejów	Krasiejów	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobné	podobný	k2eAgInPc1d1
modely	model	k1gInPc1
se	se	k3xPyFc4
v	v	k7c6
poslední	poslední	k2eAgFnSc6d1
době	doba	k1gFnSc6
těší	těšit	k5eAaImIp3nS
velké	velký	k2eAgFnSc3d1
popularitě	popularita	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Klonování	klonování	k1gNnPc4
a	a	k8xC
měkké	měkký	k2eAgFnPc4d1
tkáně	tkáň	k1gFnPc4
dinosaurů	dinosaurus	k1gMnPc2
</s>
<s>
Klonovat	klonovat	k5eAaImF
dinosaury	dinosaurus	k1gMnPc4
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
to	ten	k3xDgNnSc1
bylo	být	k5eAaImAgNnS
zobrazeno	zobrazit	k5eAaPmNgNnS
v	v	k7c6
Jurském	jurský	k2eAgInSc6d1
parku	park	k1gInSc6
věda	věda	k1gFnSc1
zatím	zatím	k6eAd1
nedokáže	dokázat	k5eNaPmIp3nS
<g/>
,	,	kIx,
a	a	k8xC
zřejmě	zřejmě	k6eAd1
tomu	ten	k3xDgNnSc3
tak	tak	k9
bude	být	k5eAaImBp3nS
i	i	k9
v	v	k7c6
budoucnu	budoucno	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
234	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
byl	být	k5eAaImAgInS
však	však	k9
zahájen	zahájen	k2eAgInSc1d1
projekt	projekt	k1gInSc1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
amerického	americký	k2eAgMnSc2d1
paleontologa	paleontolog	k1gMnSc2
Jacka	Jacek	k1gMnSc2
Hornera	Horner	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
si	se	k3xPyFc3
klade	klást	k5eAaImIp3nS
za	za	k7c4
cíl	cíl	k1gInSc4
pozměnit	pozměnit	k5eAaPmF
genom	genom	k1gInSc4
současného	současný	k2eAgNnSc2d1
kuřete	kuře	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
by	by	kYmCp3nP
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
možné	možný	k2eAgNnSc1d1
vytvořit	vytvořit	k5eAaPmF
živého	živý	k2eAgMnSc4d1
kura	kur	k1gMnSc4
s	s	k7c7
dosud	dosud	k6eAd1
zachovanými	zachovaný	k2eAgInPc7d1
archaickými	archaický	k2eAgInPc7d1
znaky	znak	k1gInPc7
dinosauřích	dinosauří	k2eAgInPc2d1
předků	předek	k1gInPc2
(	(	kIx(
<g/>
ocas	ocas	k1gInSc1
<g/>
,	,	kIx,
ozubené	ozubený	k2eAgFnPc1d1
čelisti	čelist	k1gFnPc1
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
235	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
236	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
současnosti	současnost	k1gFnSc6
jsou	být	k5eAaImIp3nP
také	také	k9
některé	některý	k3yIgFnPc1
fosílie	fosílie	k1gFnPc1
podrobovány	podrobován	k2eAgFnPc1d1
výzkumu	výzkum	k1gInSc2
a	a	k8xC
pátrání	pátrání	k1gNnSc2
po	po	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
původních	původní	k2eAgInPc6d1
proteinech	protein	k1gInPc6
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
již	již	k6eAd1
byly	být	k5eAaImAgInP
údajně	údajně	k6eAd1
skutečně	skutečně	k6eAd1
izolovány	izolovat	k5eAaBmNgFnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
237	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
současnosti	současnost	k1gFnSc6
se	se	k3xPyFc4
touto	tento	k3xDgFnSc7
problematikou	problematika	k1gFnSc7
zabývá	zabývat	k5eAaImIp3nS
především	především	k9
americká	americký	k2eAgFnSc1d1
biochemička	biochemička	k1gFnSc1
Mary	Mary	k1gFnSc2
Higby	Higba	k1gFnSc2
Schweitzerová	Schweitzerová	k1gFnSc1
<g/>
,	,	kIx,
spolupracující	spolupracující	k2eAgMnPc1d1
již	již	k6eAd1
od	od	k7c2
90	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
s	s	k7c7
Jackem	Jacek	k1gMnSc7
Hornerem	Horner	k1gMnSc7
<g/>
[	[	kIx(
<g/>
238	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
podobě	podoba	k1gFnSc6
genomu	genom	k1gInSc2
(	(	kIx(
<g/>
jeho	jeho	k3xOp3gFnSc2
přibližné	přibližný	k2eAgFnSc2d1
délce	délka	k1gFnSc3
a	a	k8xC
struktuře	struktura	k1gFnSc3
<g/>
)	)	kIx)
druhohorních	druhohorní	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
již	již	k6eAd1
mají	mít	k5eAaImIp3nP
vědci	vědec	k1gMnPc1
díky	díky	k7c3
porovnávání	porovnávání	k1gNnSc3
s	s	k7c7
genomem	genom	k1gInSc7
ptáků	pták	k1gMnPc2
a	a	k8xC
krokodýlů	krokodýl	k1gMnPc2
určitou	určitý	k2eAgFnSc4d1
představu	představa	k1gFnSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
však	však	k9
neumožňuje	umožňovat	k5eNaImIp3nS
jejich	jejich	k3xOp3gNnPc4
klonování	klonování	k1gNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
239	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
240	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Některé	některý	k3yIgInPc1
novější	nový	k2eAgInPc1d2
výzkumy	výzkum	k1gInPc1
přicházejí	přicházet	k5eAaImIp3nP
se	s	k7c7
závěrem	závěr	k1gInSc7
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
dinosauřích	dinosauří	k2eAgFnPc6d1
zkamenělinách	zkamenělina	k1gFnPc6
se	se	k3xPyFc4
původní	původní	k2eAgInPc1d1
proteiny	protein	k1gInPc1
nemohly	moct	k5eNaImAgInP
dochovat	dochovat	k5eAaPmF
(	(	kIx(
<g/>
ale	ale	k8xC
vyskytují	vyskytovat	k5eAaImIp3nP
se	se	k3xPyFc4
v	v	k7c6
nich	on	k3xPp3gFnPc6
kolonie	kolonie	k1gFnSc1
recentních	recentní	k2eAgInPc2d1
mikroorganismů	mikroorganismus	k1gInPc2
či	či	k8xC
klamná	klamný	k2eAgFnSc1d1
stopa	stopa	k1gFnSc1
po	po	k7c6
biomolekulách	biomolekula	k1gFnPc6
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
241	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
naopak	naopak	k6eAd1
s	s	k7c7
tvrzením	tvrzení	k1gNnSc7
<g/>
,	,	kIx,
že	že	k8xS
stopy	stopa	k1gFnPc4
původních	původní	k2eAgFnPc2d1
organických	organický	k2eAgFnPc2d1
molekul	molekula	k1gFnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
a	a	k8xC
bílkovin	bílkovina	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
kolagen	kolagen	k1gInSc1
<g/>
)	)	kIx)
se	se	k3xPyFc4
skutečně	skutečně	k6eAd1
mohou	moct	k5eAaImIp3nP
za	za	k7c2
určitých	určitý	k2eAgFnPc2d1
podmínek	podmínka	k1gFnPc2
dochovat	dochovat	k5eAaPmF
do	do	k7c2
současnosti	současnost	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
242	#num#	k4
<g/>
]	]	kIx)
Některé	některý	k3yIgInPc1
novější	nový	k2eAgInPc1d2
objevy	objev	k1gInPc1
skutečně	skutečně	k6eAd1
naznačují	naznačovat	k5eAaImIp3nP
možnost	možnost	k1gFnSc4
dochování	dochování	k1gNnSc2
původních	původní	k2eAgFnPc2d1
biomolekul	biomolekula	k1gFnPc2
(	(	kIx(
<g/>
proteinů	protein	k1gInPc2
<g/>
,	,	kIx,
chromozomů	chromozom	k1gInPc2
a	a	k8xC
DNA-markerů	DNA-marker	k1gInPc2
<g/>
)	)	kIx)
ve	v	k7c6
fosiliích	fosilie	k1gFnPc6
některých	některý	k3yIgMnPc2
druhohorních	druhohorní	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
ve	v	k7c6
skvěle	skvěle	k6eAd1
dochovaném	dochovaný	k2eAgInSc6d1
fosilním	fosilní	k2eAgInSc6d1
exempláři	exemplář	k1gInSc6
chrupavčité	chrupavčitý	k2eAgFnSc2d1
tkáně	tkáň	k1gFnSc2
mláděte	mládě	k1gNnSc2
kachnozobého	kachnozobý	k2eAgMnSc2d1
dinosaura	dinosaurus	k1gMnSc2
druhu	druh	k1gInSc2
Hypacrosaurus	Hypacrosaurus	k1gMnSc1
stebingeri	stebinger	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
243	#num#	k4
<g/>
]	]	kIx)
Zatím	zatím	k6eAd1
není	být	k5eNaImIp3nS
zcela	zcela	k6eAd1
jisté	jistý	k2eAgNnSc1d1
<g/>
,	,	kIx,
nakolik	nakolik	k6eAd1
významné	významný	k2eAgInPc1d1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
tyto	tento	k3xDgInPc4
objevy	objev	k1gInPc4
pro	pro	k7c4
budoucí	budoucí	k2eAgInSc4d1
výzkum	výzkum	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
244	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Inteligentní	inteligentní	k2eAgMnPc1d1
dinosauři	dinosaurus	k1gMnPc1
</s>
<s>
Hojně	hojně	k6eAd1
se	se	k3xPyFc4
již	již	k6eAd1
několik	několik	k4yIc4
desetiletí	desetiletí	k1gNnPc2
spekuluje	spekulovat	k5eAaImIp3nS
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
zda	zda	k8xS
dinosauři	dinosaurus	k1gMnPc1
mohli	moct	k5eAaImAgMnP
(	(	kIx(
<g/>
v	v	k7c6
případě	případ	k1gInSc6
že	že	k8xS
by	by	kYmCp3nP
nevyhynuli	vyhynout	k5eNaPmAgMnP
na	na	k7c6
konci	konec	k1gInSc6
křídy	křída	k1gFnSc2
<g/>
)	)	kIx)
vytvořit	vytvořit	k5eAaPmF
civilizaci	civilizace	k1gFnSc4
<g/>
,	,	kIx,
v	v	k7c6
mnoha	mnoho	k4c6
ohledech	ohled	k1gInPc6
podobnou	podobný	k2eAgFnSc4d1
té	ten	k3xDgFnSc2
lidské	lidský	k2eAgFnSc2d1
<g/>
[	[	kIx(
<g/>
245	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objevují	objevovat	k5eAaImIp3nP
se	se	k3xPyFc4
také	také	k9
modely	model	k1gInPc1
hypotetických	hypotetický	k2eAgMnPc2d1
inteligentních	inteligentní	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
–	–	k?
tzv.	tzv.	kA
dinosauroidů	dinosauroid	k1gInPc2
<g/>
[	[	kIx(
<g/>
246	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejslavnější	slavný	k2eAgFnSc1d3
je	být	k5eAaImIp3nS
nejspíš	nejspíš	k9
zpodobnění	zpodobnění	k1gNnSc4
od	od	k7c2
kanadského	kanadský	k2eAgMnSc2d1
paleontologa	paleontolog	k1gMnSc2
Dalea	Daleus	k1gMnSc2
Russella	Russell	k1gMnSc2
a	a	k8xC
výtvarníka	výtvarník	k1gMnSc2
Rona	Ron	k1gMnSc2
Séguina	Séguin	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1982	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
247	#num#	k4
<g/>
]	]	kIx)
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
však	však	k9
pouze	pouze	k6eAd1
o	o	k7c4
fiktivní	fiktivní	k2eAgNnSc4d1
ztvárnění	ztvárnění	k1gNnSc4
„	„	k?
<g/>
dinosauřího	dinosauří	k2eAgMnSc4d1
humanoida	humanoid	k1gMnSc4
<g/>
“	“	k?
bez	bez	k7c2
jakéhokoliv	jakýkoliv	k3yIgInSc2
vědeckého	vědecký	k2eAgInSc2d1
základu	základ	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
248	#num#	k4
<g/>
]	]	kIx)
Podobnou	podobný	k2eAgFnSc7d1
fikcí	fikce	k1gFnSc7
jsou	být	k5eAaImIp3nP
také	také	k9
úvahy	úvaha	k1gFnPc1
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
by	by	kYmCp3nS
vypadal	vypadat	k5eAaPmAgInS,k5eAaImAgInS
svět	svět	k1gInSc1
sdílený	sdílený	k2eAgInSc1d1
dinosaury	dinosaurus	k1gMnPc7
i	i	k8xC
lidmi	člověk	k1gMnPc7
a	a	k8xC
zda	zda	k8xS
by	by	kYmCp3nP
například	například	k6eAd1
některé	některý	k3yIgInPc1
druhy	druh	k1gInPc1
„	„	k?
<g/>
neptačích	ptačí	k2eNgInPc2d1
<g/>
“	“	k?
dinosaurů	dinosaurus	k1gMnPc2
byly	být	k5eAaImAgFnP
v	v	k7c6
současnosti	současnost	k1gFnSc6
domestikovatelné	domestikovatelný	k2eAgNnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
249	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
250	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ptáci	pták	k1gMnPc1
coby	coby	k?
přežívající	přežívající	k2eAgMnPc1d1
dinosauři	dinosaurus	k1gMnPc1
</s>
<s>
Od	od	k7c2
konce	konec	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
je	být	k5eAaImIp3nS
i	i	k9
širokou	široký	k2eAgFnSc7d1
veřejností	veřejnost	k1gFnSc7
postupně	postupně	k6eAd1
akceptován	akceptován	k2eAgInSc1d1
vědecky	vědecky	k6eAd1
potvrzený	potvrzený	k2eAgInSc1d1
fakt	fakt	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
ptáci	pták	k1gMnPc1
jsou	být	k5eAaImIp3nP
poslední	poslední	k2eAgFnSc7d1
přežívající	přežívající	k2eAgFnSc7d1
skupinou	skupina	k1gFnSc7
teropodních	teropodní	k2eAgMnPc2d1
dinosaurů	dinosaurus	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
víme	vědět	k5eAaImIp1nP
<g/>
,	,	kIx,
že	že	k8xS
první	první	k4xOgMnPc1
ptáci	pták	k1gMnPc1
vznikli	vzniknout	k5eAaPmAgMnP
z	z	k7c2
dinosauřích	dinosauří	k2eAgMnPc2d1
předků	předek	k1gMnPc2
v	v	k7c6
období	období	k1gNnSc6
jury	jura	k1gFnSc2
(	(	kIx(
<g/>
asi	asi	k9
před	před	k7c7
160	#num#	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
<g/>
)	)	kIx)
a	a	k8xC
jako	jako	k9
jediná	jediný	k2eAgFnSc1d1
(	(	kIx(
<g/>
vysoce	vysoce	k6eAd1
specializovaná	specializovaný	k2eAgFnSc1d1
<g/>
)	)	kIx)
skupina	skupina	k1gFnSc1
maniraptorů	maniraptor	k1gInPc2
přežili	přežít	k5eAaPmAgMnP
vymírání	vymírání	k1gNnSc4
na	na	k7c6
konci	konec	k1gInSc6
křídy	křída	k1gFnSc2
před	před	k7c7
66	#num#	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
populární	populární	k2eAgFnSc6d1
kultuře	kultura	k1gFnSc6
se	se	k3xPyFc4
tato	tento	k3xDgFnSc1
myšlenka	myšlenka	k1gFnSc1
těší	těšit	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
značné	značný	k2eAgFnSc3d1
oblibě	obliba	k1gFnSc3
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
ne	ne	k9
všichni	všechen	k3xTgMnPc1
tradicionalisté	tradicionalista	k1gMnPc1
z	z	k7c2
řad	řada	k1gFnPc2
biologů	biolog	k1gMnPc2
<g/>
,	,	kIx,
ornitologů	ornitolog	k1gMnPc2
nebo	nebo	k8xC
paleontologů	paleontolog	k1gMnPc2
jsou	být	k5eAaImIp3nP
jí	on	k3xPp3gFnSc7
zcela	zcela	k6eAd1
nakloněni	naklonit	k5eAaPmNgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
251	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
252	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dinosauři	dinosaurus	k1gMnPc1
jako	jako	k8xS,k8xC
komodita	komodita	k1gFnSc1
</s>
<s>
Od	od	k7c2
konce	konec	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
začíná	začínat	k5eAaImIp3nS
šířit	šířit	k5eAaImF
pašování	pašování	k1gNnSc1
fosilií	fosilie	k1gFnPc2
a	a	k8xC
nelegální	legální	k2eNgInSc4d1
vývoz	vývoz	k1gInSc4
cenných	cenný	k2eAgInPc2d1
fosilních	fosilní	k2eAgInPc2d1
exemplářů	exemplář	k1gInPc2
ze	z	k7c2
zemí	zem	k1gFnPc2
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
Mongolsko	Mongolsko	k1gNnSc1
a	a	k8xC
Čína	Čína	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Legálně	legálně	k6eAd1
jsou	být	k5eAaImIp3nP
pak	pak	k6eAd1
vydražovány	vydražovat	k5eAaImNgInP
také	také	k9
kvalitní	kvalitní	k2eAgFnPc1d1
a	a	k8xC
kompletní	kompletní	k2eAgFnPc1d1
kostry	kostra	k1gFnPc1
dinosaurů	dinosaurus	k1gMnPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc4,k3yRgInPc4,k3yQgInPc4
vykopávají	vykopávat	k5eAaImIp3nP
komerční	komerční	k2eAgMnPc1d1
sběratelé	sběratel	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paleontologové	paleontolog	k1gMnPc1
na	na	k7c4
tyto	tento	k3xDgFnPc4
praktiky	praktika	k1gFnPc4
nemají	mít	k5eNaImIp3nP
příliš	příliš	k6eAd1
pozitivní	pozitivní	k2eAgInSc4d1
náhled	náhled	k1gInSc4
<g/>
,	,	kIx,
protože	protože	k8xS
cenné	cenný	k2eAgInPc1d1
exempláře	exemplář	k1gInPc1
dinosaurů	dinosaurus	k1gMnPc2
i	i	k8xC
jiných	jiný	k2eAgMnPc2d1
pravěkých	pravěký	k2eAgMnPc2d1
živočichů	živočich	k1gMnPc2
jsou	být	k5eAaImIp3nP
pak	pak	k6eAd1
obvykle	obvykle	k6eAd1
pro	pro	k7c4
vědu	věda	k1gFnSc4
zcela	zcela	k6eAd1
ztraceny	ztratit	k5eAaPmNgFnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
253	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Některé	některý	k3yIgInPc1
dobře	dobře	k6eAd1
zachované	zachovaný	k2eAgInPc1d1
objevy	objev	k1gInPc1
dinosauřích	dinosauří	k2eAgFnPc2d1
koster	kostra	k1gFnPc2
jsou	být	k5eAaImIp3nP
vykopány	vykopán	k2eAgFnPc1d1
komerčními	komerční	k2eAgMnPc7d1
sběrateli	sběratel	k1gMnPc7
a	a	k8xC
následně	následně	k6eAd1
prodávány	prodávat	k5eAaImNgInP
či	či	k8xC
vydraženy	vydražit	k5eAaPmNgInP
za	za	k7c4
značné	značný	k2eAgFnPc4d1
finanční	finanční	k2eAgFnPc4d1
částky	částka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
příklad	příklad	k1gInSc4
dvou	dva	k4xCgInPc2
tyranosauřích	tyranosauří	k2eAgInPc2d1
exemplářů	exemplář	k1gInPc2
<g/>
,	,	kIx,
jednoho	jeden	k4xCgMnSc4
s	s	k7c7
přezdívkou	přezdívka	k1gFnSc7
"	"	kIx"
<g/>
Sue	Sue	k1gFnSc7
<g/>
"	"	kIx"
(	(	kIx(
<g/>
v	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
za	za	k7c4
8,4	8,4	k4
milionu	milion	k4xCgInSc2
dolarů	dolar	k1gInPc2
<g/>
)	)	kIx)
a	a	k8xC
dalšího	další	k2eAgMnSc2d1
s	s	k7c7
přezdívkou	přezdívka	k1gFnSc7
"	"	kIx"
<g/>
Stan	stan	k1gInSc1
<g/>
"	"	kIx"
(	(	kIx(
<g/>
v	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
za	za	k7c4
rekordních	rekordní	k2eAgInPc2d1
31,8	31,8	k4
milionu	milion	k4xCgInSc2
dolarů	dolar	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
254	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
https://dinosaurusblog.com/2012/07/15/jak-dlouho-existovali-neptaci-dinosauri/	https://dinosaurusblog.com/2012/07/15/jak-dlouho-existovali-neptaci-dinosauri/	k4
<g/>
↑	↑	k?
https://dinosaurusblog.com/2009/03/31/776571-jsou-ptaci-dinosauri/	https://dinosaurusblog.com/2009/03/31/776571-jsou-ptaci-dinosauri/	k4
<g/>
↑	↑	k?
http://phys.org/news/2016-11-biggest-dinosaur-tree-emerged-million.html	http://phys.org/news/2016-11-biggest-dinosaur-tree-emerged-million.html	k1gMnSc1
-	-	kIx~
Biggest	Biggest	k1gMnSc1
map	mapa	k1gFnPc2
of	of	k?
dinosaur	dinosaur	k1gMnSc1
tree	tree	k6eAd1
yet	yet	k?
suggests	suggests	k1gInSc4
they	thea	k1gFnSc2
emerged	emerged	k1gInSc4
20	#num#	k4
million	million	k1gInSc1
years	years	k6eAd1
earlier	earlier	k1gInSc1
than	thana	k1gFnPc2
thought	thought	k1gMnSc1
<g/>
↑	↑	k?
Max	Max	k1gMnSc1
C.	C.	kA
Langer	Langer	k1gMnSc1
<g/>
,	,	kIx,
Jahandar	Jahandar	k1gMnSc1
Ramezani	Ramezan	k1gMnPc1
&	&	k?
Átila	Átila	k1gFnSc1
A.S.	A.S.	k1gFnSc1
Da	Da	k1gFnSc1
Rosa	Rosa	k1gFnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U-Pb	U-Pb	k1gMnSc1
age	age	k?
constraints	constraints	k6eAd1
on	on	k3xPp3gMnSc1
dinosaur	dinosaur	k1gMnSc1
rise	ris	k1gFnSc2
from	from	k1gMnSc1
south	south	k1gMnSc1
Brazil	Brazil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gondwana	Gondwana	k1gFnSc1
Research	Researcha	k1gFnPc2
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1016/j.gr.2018.01.005	https://doi.org/10.1016/j.gr.2018.01.005	k4
<g/>
↑	↑	k?
Júlio	Júlio	k1gMnSc1
Cesar	Cesar	k1gMnSc1
de	de	k?
Almeida	Almeida	k1gFnSc1
Marsola	Marsola	k1gFnSc1
&	&	k?
Max	Max	k1gMnSc1
Cardoso	Cardosa	k1gFnSc5
Langer	Langer	k1gMnSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dinosaur	Dinosaura	k1gFnPc2
Origins	Originsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reference	reference	k1gFnSc1
Module	modul	k1gInSc5
in	in	k?
Earth	Earth	k1gInSc4
Systems	Systemsa	k1gFnPc2
and	and	k?
Environmental	Environmental	k1gMnSc1
Sciences	Sciences	k1gMnSc1
2019	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1016/B978-0-12-409548-9.11846-9	https://doi.org/10.1016/B978-0-12-409548-9.11846-9	k4
<g/>
↑	↑	k?
https://www.osel.cz/11476-dinosaurum-se-darilo-do-posledni-chvile.html	https://www.osel.cz/11476-dinosaurum-se-darilo-do-posledni-chvile.html	k1gMnSc1
<g/>
↑	↑	k?
Ciara	Ciara	k1gMnSc1
O	o	k7c6
<g/>
’	’	k?
<g/>
Donovan	Donovan	k1gMnSc1
<g/>
,	,	kIx,
Andrew	Andrew	k1gMnSc1
Meade	Mead	k1gInSc5
&	&	k?
Chris	Chris	k1gFnSc2
Venditti	Venditť	k1gFnSc2
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dinosaurs	Dinosaurs	k1gInSc1
reveal	reveal	k1gInSc4
the	the	k?
geographical	geographicat	k5eAaPmAgMnS
signature	signatur	k1gMnSc5
of	of	k?
an	an	k?
evolutionary	evolutionara	k1gFnSc2
radiation	radiation	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
Ecology	Ecolog	k1gInPc1
&	&	k?
Evolution	Evolution	k1gInSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.103	10.103	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
41559	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
17	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
454	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
↑	↑	k?
https://phys.org/news/2018-02-dinosaurs-successful-good.html	https://phys.org/news/2018-02-dinosaurs-successful-good.html	k1gMnSc1
<g/>
↑	↑	k?
Rozhovor	rozhovor	k1gInSc1
o	o	k7c6
prvních	první	k4xOgInPc6
dinosaurech	dinosaurus	k1gMnPc6
v	v	k7c6
rozhlasovém	rozhlasový	k2eAgInSc6d1
pořadu	pořad	k1gInSc6
Meteor	meteor	k1gInSc1
<g/>
,	,	kIx,
čas	čas	k1gInSc1
33	#num#	k4
<g/>
:	:	kIx,
<g/>
11	#num#	k4
min	mina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
https://www.osel.cz/11265-kdo-byl-prvnim-dinosaurem.html	https://www.osel.cz/11265-kdo-byl-prvnim-dinosaurem.html	k1gMnSc1
<g/>
↑	↑	k?
SOCHA	Socha	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Půlstoletí	půlstoletí	k1gNnSc1
králičího	králičí	k2eAgMnSc2d1
krokodýla	krokodýl	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSEL	osel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
https://www.osel.cz/10943-saturnalia-skutecny-vanocni-dinosaurus.html	https://www.osel.cz/10943-saturnalia-skutecny-vanocni-dinosaurus.html	k1gMnSc1
<g/>
↑	↑	k?
Júlio	Júlio	k1gMnSc1
C.	C.	kA
A.	A.	kA
Marsola	Marsola	k1gFnSc1
<g/>
;	;	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Increases	Increases	k1gInSc1
in	in	k?
sampling	sampling	k1gInSc1
support	support	k1gInSc1
the	the	k?
southern	southern	k1gInSc1
Gondwanan	Gondwanan	k1gInSc1
hypothesis	hypothesis	k1gFnPc2
for	forum	k1gNnPc2
the	the	k?
origin	origin	k1gMnSc1
of	of	k?
dinosaurs	dinosaurs	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Palaeontology	Palaeontolog	k1gMnPc4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://onlinelibrary.wiley.com/doi/full/10.1111/pala.12411	https://onlinelibrary.wiley.com/doi/full/10.1111/pala.12411	k4
<g/>
↑	↑	k?
Michael	Michael	k1gMnSc1
S.	S.	kA
Y.	Y.	kA
Lee	Lea	k1gFnSc6
<g/>
;	;	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dynamic	Dynamic	k1gMnSc1
biogeographic	biogeographic	k1gMnSc1
models	models	k6eAd1
and	and	k?
dinosaur	dinosaura	k1gFnPc2
origins	origins	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Earth	Earth	k1gMnSc1
and	and	k?
Environmental	Environmental	k1gMnSc1
Science	Science	k1gFnSc2
Transactions	Transactionsa	k1gFnPc2
of	of	k?
The	The	k1gMnPc2
Royal	Royal	k1gMnSc1
Society	societa	k1gFnSc2
of	of	k?
Edinburgh	Edinburgh	k1gInSc1
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1017/S1755691018000920	https://doi.org/10.1017/S1755691018000920	k4
<g/>
↑	↑	k?
Massimo	Massima	k1gFnSc5
Bernardi	Bernard	k1gMnPc1
<g/>
,	,	kIx,
Piero	Piero	k1gNnSc1
Gianolla	Gianolla	k1gMnSc1
<g/>
,	,	kIx,
Fabio	Fabio	k1gMnSc1
Massimo	Massima	k1gFnSc5
Petti	Petti	k1gNnSc1
<g/>
,	,	kIx,
Paolo	Paolo	k1gNnSc1
Mietto	Miett	k2eAgNnSc1d1
&	&	k?
Michael	Michael	k1gMnSc1
J.	J.	kA
Benton	Benton	k1gInSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dinosaur	Dinosaura	k1gFnPc2
diversification	diversification	k1gInSc1
linked	linked	k1gMnSc1
with	with	k1gMnSc1
the	the	k?
Carnian	Carnian	k1gMnSc1
Pluvial	Pluvial	k1gMnSc1
Episode	Episod	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
Communications	Communicationsa	k1gFnPc2
9	#num#	k4
<g/>
,	,	kIx,
Article	Article	k1gFnSc1
number	number	k1gMnSc1
<g/>
:	:	kIx,
1499	#num#	k4
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.103	10.103	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
41467	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
18	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3996	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
↑	↑	k?
Michael	Michael	k1gMnSc1
J.	J.	kA
Benton	Benton	k1gInSc1
<g/>
,	,	kIx,
Massimo	Massima	k1gFnSc5
Bernardi	Bernard	k1gMnPc1
and	and	k?
Cormac	Cormac	k1gFnSc1
Kinsella	Kinsella	k1gFnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Carnian	Carnian	k1gMnSc1
Pluvial	Pluvial	k1gMnSc1
Episode	Episod	k1gInSc5
and	and	k?
the	the	k?
origin	origin	k1gInSc1
of	of	k?
dinosaurs	dinosaurs	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
the	the	k?
Geological	Geological	k1gFnSc2
Society	societa	k1gFnSc2
(	(	kIx(
<g/>
advance	advance	k1gFnSc1
online	onlinout	k5eAaPmIp3nS
publication	publication	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1144/jgs2018-049	https://doi.org/10.1144/jgs2018-049	k4
<g/>
↑	↑	k?
https://paleonerdish.wordpress.com/2018/04/20/a-brief-introduction-to-the-carnian-pluvial-episode/	https://paleonerdish.wordpress.com/2018/04/20/a-brief-introduction-to-the-carnian-pluvial-episode/	k4
<g/>
↑	↑	k?
https://www.nature.com/magazine-assets/d41586-019-03699-7/d41586-019-03699-7.pdf	https://www.nature.com/magazine-assets/d41586-019-03699-7/d41586-019-03699-7.pdf	k1gMnSc1
<g/>
↑	↑	k?
SOCHA	Socha	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Karnská	Karnský	k2eAgFnSc1d1
pluviální	pluviální	k2eAgFnSc1d1
epizoda	epizoda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
DinosaurusBlog	DinosaurusBlog	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
21	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Matthew	Matthew	k1gMnSc1
G.	G.	kA
Baron	baron	k1gMnSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Difficulties	Difficulties	k1gMnSc1
with	with	k1gMnSc1
the	the	k?
origin	origin	k1gMnSc1
of	of	k?
dinosaurs	dinosaurs	k1gInSc1
<g/>
:	:	kIx,
a	a	k8xC
comment	comment	k1gMnSc1
on	on	k3xPp3gMnSc1
the	the	k?
current	current	k1gInSc1
debate	debat	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Palaeovertebrata	Palaeovertebrata	k1gFnSc1
<g/>
,	,	kIx,
43	#num#	k4
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
-e	-e	k?
<g/>
3	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
10.185	10.185	k4
<g/>
63	#num#	k4
<g/>
/	/	kIx~
<g/>
pv	pv	k?
<g/>
.43	.43	k4
<g/>
.1	.1	k4
<g/>
.	.	kIx.
<g/>
e	e	k0
<g/>
3	#num#	k4
<g/>
↑	↑	k?
Nicholas	Nicholas	k1gMnSc1
M.	M.	kA
A.	A.	kA
Crouch	Crouch	k1gMnSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Extinction	Extinction	k1gInSc1
rates	rates	k1gInSc4
of	of	k?
non-avian	non-avian	k1gInSc1
dinosaur	dinosaur	k1gMnSc1
species	species	k1gFnPc2
are	ar	k1gInSc5
uncorrelated	uncorrelated	k1gInSc1
with	with	k1gInSc1
the	the	k?
rate	rate	k1gInSc1
of	of	k?
evolution	evolution	k1gInSc1
of	of	k?
phylogenetically	phylogeneticalla	k1gFnSc2
informative	informativ	k1gInSc5
characters	characters	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biology	biolog	k1gMnPc7
Letters	Lettersa	k1gFnPc2
<g/>
,	,	kIx,
16	#num#	k4
<g/>
(	(	kIx(
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
20200231	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
http://dx.doi.org/10.1098/rsbl.2020.0231	http://dx.doi.org/10.1098/rsbl.2020.0231	k4
<g/>
↑	↑	k?
Fernando	Fernanda	k1gFnSc5
E.	E.	kA
Novas	Novas	k1gMnSc1
<g/>
,	,	kIx,
Federico	Federico	k1gMnSc1
L.	L.	kA
Agnolin	Agnolin	k1gInSc1
<g/>
,	,	kIx,
Martín	Martín	k1gMnSc1
D.	D.	kA
Ezcurra	Ezcurra	k1gMnSc1
<g/>
,	,	kIx,
Rodrigo	Rodrigo	k1gMnSc1
T.	T.	kA
Müller	Müller	k1gMnSc1
<g/>
,	,	kIx,
Agustì	Agustì	k1gMnSc1
Martinelli	Martinell	k1gMnSc3
&	&	k?
Max	Max	k1gMnSc1
Langer	Langer	k1gMnSc1
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Review	Review	k1gFnSc1
of	of	k?
the	the	k?
fossil	fossit	k5eAaImAgMnS,k5eAaPmAgMnS
record	record	k1gMnSc1
of	of	k?
early	earl	k1gMnPc4
dinosaurs	dinosaurs	k6eAd1
from	from	k6eAd1
South	South	k1gMnSc1
America	America	k1gMnSc1
<g/>
,	,	kIx,
and	and	k?
its	its	k?
phylogenetic	phylogenetice	k1gFnPc2
implications	implications	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gMnSc1
of	of	k?
South	South	k1gMnSc1
American	American	k1gMnSc1
Earth	Earth	k1gMnSc1
Sciences	Sciences	k1gMnSc1
<g/>
.	.	kIx.
103341	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1016/j.jsames.2021.103341	https://doi.org/10.1016/j.jsames.2021.103341	k4
<g/>
↑	↑	k?
Tore	Tor	k1gMnSc5
G.	G.	kA
Klausen	Klausen	k1gInSc1
<g/>
,	,	kIx,
Niall	Niall	k1gMnSc1
W.	W.	kA
Paterson	Paterson	k1gMnSc1
&	&	k?
Michael	Michael	k1gMnSc1
J.	J.	kA
Benton	Benton	k1gInSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geological	Geological	k1gFnSc1
control	controla	k1gFnPc2
on	on	k3xPp3gMnSc1
dinosaurs	dinosaurs	k6eAd1
<g/>
’	’	k?
rise	risat	k5eAaPmIp3nS
to	ten	k3xDgNnSc1
dominance	dominance	k1gFnSc1
<g/>
:	:	kIx,
Late	lat	k1gInSc5
Triassic	Triassice	k1gFnPc2
ecosystem	ecosyst	k1gInSc7
stress	stress	k1gInSc1
by	by	kYmCp3nS
relative	relativ	k1gInSc5
sea	sea	k?
level	level	k1gInSc4
change	change	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Terra	Terra	k1gFnSc1
Nova	nova	k1gFnSc1
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1111/ter.12480	https://doi.org/10.1111/ter.12480	k4
<g/>
↑	↑	k?
George	George	k1gNnPc2
L.	L.	kA
W.	W.	kA
Perry	Perra	k1gMnSc2
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
How	How	k1gFnSc1
far	fara	k1gFnPc2
might	might	k2eAgInSc1d1
plant-eating	plant-eating	k1gInSc1
dinosaurs	dinosaurs	k6eAd1
have	havat	k5eAaPmIp3nS
moved	moved	k1gInSc4
seeds	seedsa	k1gFnPc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
Biology	biolog	k1gMnPc4
Letters	Lettersa	k1gFnPc2
<g/>
.	.	kIx.
17	#num#	k4
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
20200689	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1098/rsbl.2020.0689	https://doi.org/10.1098/rsbl.2020.0689	k4
<g/>
↑	↑	k?
https://phys.org/news/2021-01-giant-dinosaurs-seeds-prehistoric-world.html	https://phys.org/news/2021-01-giant-dinosaurs-seeds-prehistoric-world.html	k1gInSc1
<g/>
↑	↑	k?
https://dinosaurusblog.com/2011/01/25/879663-co-je-to-ten-dinosaurus/	https://dinosaurusblog.com/2011/01/25/879663-co-je-to-ten-dinosaurus/	k4
<g/>
↑	↑	k?
Paul	Paul	k1gMnSc1
C.	C.	kA
Sereno	Seren	k2eAgNnSc1d1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnPc2
evolution	evolution	k1gInSc4
of	of	k?
dinosaurs	dinosaurs	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Science	Science	k1gFnSc1
<g/>
,	,	kIx,
284	#num#	k4
<g/>
:	:	kIx,
2137	#num#	k4
<g/>
–	–	k?
<g/>
2146	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
https://dinomuseum.ca/2019/07/30/when-dinosaurs-ruled-the-earth/	https://dinomuseum.ca/2019/07/30/when-dinosaurs-ruled-the-earth/	k4
<g/>
↑	↑	k?
https://www.idnes.cz/technet/veda/dinosaurus-planetka-chixculub-pad-vymreni-dinosauru-michael-benton-krida.A201127_093422_veda_mla	https://www.idnes.cz/technet/veda/dinosaurus-planetka-chixculub-pad-vymreni-dinosauru-michael-benton-krida.A201127_093422_veda_mla	k1gMnSc1
<g/>
↑	↑	k?
SOCHA	Socha	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proč	proč	k6eAd1
na	na	k7c6
dinosaurech	dinosaurus	k1gMnPc6
záleží	záležet	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSEL	osel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
V.	V.	kA
R.	R.	kA
Allen	Allen	k1gMnSc1
<g/>
,	,	kIx,
B.	B.	kA
M.	M.	kA
Kilbourne	Kilbourn	k1gInSc5
and	and	k?
J.	J.	kA
R.	R.	kA
Hutchinson	Hutchinson	k1gMnSc1
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnPc2
evolution	evolution	k1gInSc4
of	of	k?
pelvic	pelvice	k1gInPc2
limb	limb	k1gInSc4
muscle	muscle	k1gFnSc2
moment	moment	k1gInSc1
arms	arms	k1gInSc1
in	in	k?
bird-line	bird-lin	k1gInSc5
archosaurs	archosaursa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Science	Science	k1gFnSc1
Advances	Advancesa	k1gFnPc2
<g/>
.	.	kIx.
7	#num#	k4
(	(	kIx(
<g/>
12	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
eabe	eabe	k1gInSc1
<g/>
2778	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
10.112	10.112	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
sciadv	sciadva	k1gFnPc2
<g/>
.	.	kIx.
<g/>
abe	abe	k?
<g/>
2778	#num#	k4
<g/>
↑	↑	k?
W.	W.	kA
Scott	Scott	k1gInSc1
Persons	Persons	k1gInSc1
IV	IV	kA
&	&	k?
Philip	Philip	k1gInSc1
J.	J.	kA
Currie	Currie	k1gFnSc2
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
anatomical	anatomicat	k5eAaPmAgMnS
and	and	k?
functional	functionat	k5eAaPmAgInS,k5eAaImAgInS
evolution	evolution	k1gInSc1
of	of	k?
the	the	k?
femoral	femorat	k5eAaImAgInS,k5eAaPmAgInS
fourth	fourth	k1gInSc1
trochanter	trochanter	k1gInSc1
in	in	k?
ornithischian	ornithischian	k1gInSc1
dinosaurs	dinosaurs	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Anatomical	Anatomical	k1gMnSc1
Record	Record	k1gMnSc1
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1002/ar.24094	https://doi.org/10.1002/ar.24094	k4
<g/>
↑	↑	k?
Shiro	Shiro	k1gNnSc1
Egawa	Egawa	k1gFnSc1
<g/>
;	;	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Morphogenetic	Morphogenetice	k1gFnPc2
mechanism	mechanisma	k1gFnPc2
of	of	k?
the	the	k?
acquisition	acquisition	k1gInSc1
of	of	k?
the	the	k?
dinosaur-type	dinosaur-typ	k1gInSc5
acetabulum	acetabulum	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Royal	Royal	k1gInSc4
Society	societa	k1gFnSc2
Open	Open	k1gNnSc4
Science	Scienec	k1gInSc2
5	#num#	k4
<g/>
(	(	kIx(
<g/>
10	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
180604	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
10.109	10.109	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
rsos	rsos	k6eAd1
<g/>
.180604	.180604	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Henry	Henry	k1gMnSc1
P.	P.	kA
Tsai	Tsa	k1gFnPc1
<g/>
,	,	kIx,
Kevin	Kevin	k2eAgInSc1d1
M.	M.	kA
Middleton	Middleton	k1gInSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
R.	R.	kA
Hutchinson	Hutchinson	k1gMnSc1
&	&	k?
Casey	Casea	k1gFnSc2
M.	M.	kA
Holliday	Hollidaa	k1gFnSc2
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
More	mor	k1gInSc5
than	thana	k1gFnPc2
one	one	k?
way	way	k?
to	ten	k3xDgNnSc4
be	be	k?
a	a	k8xC
giant	giant	k1gInSc1
<g/>
:	:	kIx,
Convergence	Convergence	k1gFnSc1
and	and	k?
disparity	disparita	k1gFnSc2
in	in	k?
the	the	k?
hip	hip	k0
joints	joints	k6eAd1
of	of	k?
saurischian	saurischian	k1gInSc1
dinosaurs	dinosaurs	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evolution	Evolution	k1gInSc1
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1111/evo.14017	https://doi.org/10.1111/evo.14017	k4
<g/>
↑	↑	k?
Tanja	Tanja	k1gMnSc1
Wintrich	Wintrich	k1gMnSc1
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
Scaal	Scaal	k1gMnSc1
<g/>
,	,	kIx,
Christine	Christin	k1gInSc5
Böhmer	Böhmer	k1gMnSc1
<g/>
,	,	kIx,
Rico	Rico	k1gMnSc1
Schellhorn	Schellhorn	k1gMnSc1
<g/>
,	,	kIx,
Ilja	Ilja	k1gMnSc1
Kogan	Kogan	k1gMnSc1
<g/>
,	,	kIx,
Aaron	Aaron	k1gMnSc1
van	vana	k1gFnPc2
der	drát	k5eAaImRp2nS
Reest	Reest	k1gInSc1
&	&	k?
P.	P.	kA
Martin	Martin	k1gMnSc1
Sander	Sandra	k1gFnPc2
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Palaeontological	Palaeontological	k1gFnSc1
evidence	evidence	k1gFnSc1
reveals	reveals	k1gInSc4
convergent	convergent	k1gInSc1
evolution	evolution	k1gInSc1
of	of	k?
intervertebral	intervertebrat	k5eAaPmAgInS,k5eAaImAgInS
joint	joint	k1gInSc1
types	types	k1gMnSc1
in	in	k?
amniotes	amniotes	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Scientific	Scientifice	k1gInPc2
Reports	Reportsa	k1gFnPc2
<g/>
,	,	kIx,
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Article	Article	k1gNnSc1
number	numbra	k1gFnPc2
<g/>
:	:	kIx,
14106	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1038/s41598-020-70751-2	https://doi.org/10.1038/s41598-020-70751-2	k4
<g/>
↑	↑	k?
Débora	Débora	k1gFnSc1
Moro	mora	k1gFnSc5
<g/>
,	,	kIx,
Leonardo	Leonardo	k1gMnSc1
Kerber	Kerber	k1gMnSc1
<g/>
,	,	kIx,
Rodrigo	Rodrigo	k1gMnSc1
T.	T.	kA
Müller	Müller	k1gMnSc1
&	&	k?
Flávio	Flávio	k1gMnSc1
A.	A.	kA
Pretto	Pretto	k1gNnSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sacral	Sacral	k1gFnPc2
co‐	co‐	k1gInSc4
in	in	k?
dinosaurs	dinosaurs	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
oldest	oldest	k1gMnSc1
record	record	k1gMnSc1
of	of	k?
fused	fused	k1gMnSc1
sacral	sacrat	k5eAaPmAgMnS,k5eAaImAgMnS
vertebrae	vertebrae	k1gNnSc4
in	in	k?
Dinosauria	Dinosaurium	k1gNnSc2
and	and	k?
the	the	k?
diversity	diversit	k1gInPc1
of	of	k?
sacral	sacrat	k5eAaPmAgInS,k5eAaImAgInS
co‐	co‐	k1gInSc1
patterns	patterns	k1gInSc1
in	in	k?
the	the	k?
group	group	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
Anatomy	anatom	k1gMnPc7
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1111/joa.13356	https://doi.org/10.1111/joa.13356	k4
<g/>
↑	↑	k?
David	David	k1gMnSc1
W.	W.	kA
E.	E.	kA
Hone	hon	k1gInSc5
<g/>
,	,	kIx,
W.	W.	kA
Scott	Scott	k2eAgInSc1d1
Persons	Persons	k1gInSc1
&	&	k?
Steven	Stevna	k1gFnPc2
C.	C.	kA
Le	Le	k1gMnSc1
Comber	Comber	k1gMnSc1
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gFnSc1
data	datum	k1gNnSc2
on	on	k3xPp3gMnSc1
tail	tainout	k5eAaPmAgInS
lengths	lengths	k1gInSc4
and	and	k?
variation	variation	k1gInSc1
along	along	k1gMnSc1
the	the	k?
caudal	caudat	k5eAaImAgMnS,k5eAaPmAgMnS
series	series	k1gMnSc1
in	in	k?
the	the	k?
non-avialan	non-avialan	k1gInSc1
dinosaurs	dinosaurs	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
PeerJ	PeerJ	k1gFnSc1
<g/>
.	.	kIx.
9	#num#	k4
<g/>
:	:	kIx,
e	e	k0
<g/>
10721	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.7717/peerj.10721	https://doi.org/10.7717/peerj.10721	k4
<g/>
↑	↑	k?
https://archosaurmusings.wordpress.com/2021/02/15/dinosaur-tails-redux/	https://archosaurmusings.wordpress.com/2021/02/15/dinosaur-tails-redux/	k4
<g/>
↑	↑	k?
https://www.osel.cz/11237-prvni-dinosauri-kladli-mekka-vejce.html	https://www.osel.cz/11237-prvni-dinosauri-kladli-mekka-vejce.html	k1gMnSc1
-	-	kIx~
První	první	k4xOgMnPc1
dinosauři	dinosaurus	k1gMnPc1
kladli	klást	k5eAaImAgMnP
měkká	měkký	k2eAgNnPc4d1
vejce	vejce	k1gNnPc4
<g/>
↑	↑	k?
https://dinosaurusblog.com/2008/12/14/750099-tri-kulata-paleontologicka-vyroci/	https://dinosaurusblog.com/2008/12/14/750099-tri-kulata-paleontologicka-vyroci/	k4
<g/>
↑	↑	k?
Loredana	Loredana	k1gFnSc1
Macaluso	Macalusa	k1gFnSc5
&	&	k?
Emanuel	Emanuel	k1gMnSc1
Tschopp	Tschopp	k1gMnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evolutionary	Evolutionara	k1gFnSc2
changes	changesa	k1gFnPc2
in	in	k?
pubic	pubic	k1gMnSc1
orientation	orientation	k1gInSc1
in	in	k?
dinosaurs	dinosaurs	k1gInSc1
are	ar	k1gInSc5
more	mor	k1gInSc5
strongly	strongla	k1gFnSc2
correlated	correlated	k1gMnSc1
with	with	k1gMnSc1
the	the	k?
ventilation	ventilation	k1gInSc1
system	syst	k1gInSc7
than	than	k1gNnSc1
with	with	k1gInSc4
herbivory	herbivora	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Palaeontology	Palaeontolog	k1gMnPc4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1111/pala.12362	https://doi.org/10.1111/pala.12362	k4
<g/>
↑	↑	k?
Daniel	Daniel	k1gMnSc1
T.	T.	kA
Ksepka	Ksepka	k1gFnSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Feathered	Feathered	k1gInSc1
dinosaurs	dinosaurs	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Current	Current	k1gInSc1
Biology	biolog	k1gMnPc7
<g/>
,	,	kIx,
30	#num#	k4
<g/>
(	(	kIx(
<g/>
22	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
R	R	kA
<g/>
1347	#num#	k4
<g/>
-R	-R	k?
<g/>
1353	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1016/j.cub.2020.10.007	https://doi.org/10.1016/j.cub.2020.10.007	k4
<g/>
↑	↑	k?
Maria	Maria	k1gFnSc1
Eugenia	Eugenium	k1gNnSc2
Leone	Leo	k1gMnSc5
Gold	Gold	k1gMnSc1
and	and	k?
Akinobu	Akinoba	k1gFnSc4
Watanabe	Watanab	k1gInSc5
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Flightless	Flightless	k1gInSc1
birds	birds	k6eAd1
are	ar	k1gInSc5
not	nota	k1gFnPc2
neuroanatomical	uroanatomicat	k5eNaPmAgInS
analogs	analogs	k1gInSc1
of	of	k?
non-avian	non-avian	k1gInSc1
dinosaurs	dinosaurs	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
BMC	BMC	kA
Evolutionary	Evolutionara	k1gFnSc2
Biology	biolog	k1gMnPc4
18	#num#	k4
<g/>
:	:	kIx,
190	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1186/s12862-018-1312-0	https://doi.org/10.1186/s12862-018-1312-0	k4
<g/>
↑	↑	k?
Federico	Federico	k1gMnSc1
L.	L.	kA
Agnolin	Agnolin	k1gInSc1
<g/>
,	,	kIx,
Matias	Matias	k1gMnSc1
J.	J.	kA
Motta	motto	k1gNnSc2
<g/>
,	,	kIx,
Federico	Federico	k6eAd1
Brissón	Brissón	k1gInSc1
Egli	Egli	k1gNnSc2
<g/>
,	,	kIx,
Gastón	Gastón	k1gMnSc1
Lo	Lo	k1gFnSc2
Coco	Coco	k1gMnSc1
and	and	k?
Fernando	Fernanda	k1gFnSc5
E.	E.	kA
Novas	Novasa	k1gFnPc2
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paravian	Paravian	k1gInSc1
Phylogeny	Phylogen	k1gInPc1
and	and	k?
the	the	k?
Dinosaur-Bird	Dinosaur-Bird	k1gInSc1
Transition	Transition	k1gInSc1
<g/>
:	:	kIx,
An	An	k1gMnSc1
Overview	Overview	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Frontiers	Frontiers	k1gInSc1
in	in	k?
Earth	Earth	k1gInSc1
Science	Science	k1gFnSc1
6	#num#	k4
<g/>
:	:	kIx,
<g/>
252	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.3389/feart.2018.00252	https://doi.org/10.3389/feart.2018.00252	k4
<g/>
↑	↑	k?
Rozhovor	rozhovor	k1gInSc1
o	o	k7c6
největších	veliký	k2eAgMnPc6d3
dinosaurech	dinosaurus	k1gMnPc6
v	v	k7c6
pořadu	pořad	k1gInSc6
Meteor	meteor	k1gInSc1
<g/>
,	,	kIx,
čas	čas	k1gInSc1
16	#num#	k4
<g/>
:	:	kIx,
<g/>
25	#num#	k4
min	mina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
http://www.pravek.info/zajimavosti/proc-byli-sauropodni-dinosauri-tak-velci/	http://www.pravek.info/zajimavosti/proc-byli-sauropodni-dinosauri-tak-velci/	k?
<g/>
↑	↑	k?
https://dinosaurusblog.com/2011/05/24/893631-nejvetsi-obratlovec-vsech-dob/	https://dinosaurusblog.com/2011/05/24/893631-nejvetsi-obratlovec-vsech-dob/	k4
<g/>
↑	↑	k?
Roger	Rogero	k1gNnPc2
B.	B.	kA
J.	J.	kA
Benson	Benson	k1gMnSc1
<g/>
,	,	kIx,
Gene	gen	k1gInSc5
Hunt	hunt	k1gInSc1
<g/>
,	,	kIx,
Matthew	Matthew	k1gMnSc1
T.	T.	kA
Carrano	Carrana	k1gFnSc5
&	&	k?
Nicolás	Nicolás	k1gInSc1
Campione	Campion	k1gInSc5
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cope	cop	k1gInSc5
<g/>
'	'	kIx"
<g/>
s	s	k7c7
rule	rula	k1gFnSc6
and	and	k?
the	the	k?
adaptive	adaptiv	k1gInSc5
landscape	landscapat	k5eAaPmIp3nS
of	of	k?
dinosaur	dinosaur	k1gMnSc1
body	bod	k1gInPc4
size	size	k6eAd1
evolution	evolution	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Palaeontology	Palaeontolog	k1gMnPc4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
10.111	10.111	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
pala	pal	k1gInSc2
<g/>
.12329	.12329	k4
<g/>
↑	↑	k?
SOCHA	Socha	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Argentinosaurus	Argentinosaurus	k1gInSc1
byl	být	k5eAaImAgInS
těžší	těžký	k2eAgMnSc1d2
než	než	k8xS
Boeing	boeing	k1gInSc1
737	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSEL	osel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
http://paleoking.blogspot.com/2012/01/giant-that-never-was-all-your.html	http://paleoking.blogspot.com/2012/01/giant-that-never-was-all-your.html	k1gInSc1
<g/>
↑	↑	k?
http://dinosaurusblog.com/2016/09/12/tezsi-nez-deset-slonu/	http://dinosaurusblog.com/2016/09/12/tezsi-nez-deset-slonu/	k4
<g/>
↑	↑	k?
Článek	článek	k1gInSc1
na	na	k7c6
webu	web	k1gInSc6
Pravěk	pravěk	k1gInSc1
<g/>
.	.	kIx.
<g/>
info	info	k6eAd1
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
http://www.pravek.info/zajimavosti/zahada-jmenem-bruhathkayosaurus/	http://www.pravek.info/zajimavosti/zahada-jmenem-bruhathkayosaurus/	k?
<g/>
↑	↑	k?
https://veda.instory.cz/1352-giganticky-dinosaurus-ktery-byl-popsan-v-roce-1989-byl-tezky-jako-petadvacet-slonu.html	https://veda.instory.cz/1352-giganticky-dinosaurus-ktery-byl-popsan-v-roce-1989-byl-tezky-jako-petadvacet-slonu.html	k1gInSc1
<g/>
↑	↑	k?
http://www.osel.cz/11034-nejvyssi-zivocich-vsech-dob.html	http://www.osel.cz/11034-nejvyssi-zivocich-vsech-dob.html	k1gInSc1
<g/>
↑	↑	k?
https://dinosaurusblog.com/2012/07/18/takze-ktery-byl-ten-nejvetsi-z-nejvetsich/	https://dinosaurusblog.com/2012/07/18/takze-ktery-byl-ten-nejvetsi-z-nejvetsich/	k4
<g/>
↑	↑	k?
https://dinosaurusblog.com/2017/02/20/jak-velky-byl-spinosaurus/	https://dinosaurusblog.com/2017/02/20/jak-velky-byl-spinosaurus/	k4
<g/>
↑	↑	k?
Záznam	záznam	k1gInSc1
přednášky	přednáška	k1gFnSc2
V.	V.	kA
Sochy	Socha	k1gMnSc2
o	o	k7c6
možnosti	možnost	k1gFnSc6
vzniku	vznik	k1gInSc2
hypotetické	hypotetický	k2eAgFnSc2d1
civilizace	civilizace	k1gFnSc2
dinosauroidů	dinosauroid	k1gInPc2
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
https://dinosaurusblog.com/2007/03/02/495299-byl-t-rex-chytrejsi-nez-vrana/	https://dinosaurusblog.com/2007/03/02/495299-byl-t-rex-chytrejsi-nez-vrana/	k4
<g/>
</s>
<s>
↑	↑	k?
https://dinomuseum.ca/2019/06/17/the-calls-of-the-past-how-dinosaurs-might-have-communicated/	https://dinomuseum.ca/2019/06/17/the-calls-of-the-past-how-dinosaurs-might-have-communicated/	k4
<g/>
↑	↑	k?
Krishna	Krishn	k1gInSc2
Hu	hu	k0
<g/>
,	,	kIx,
J.	J.	kA
Logan	Logan	k1gMnSc1
King	King	k1gMnSc1
<g/>
,	,	kIx,
Cheyenne	Cheyenn	k1gInSc5
A.	A.	kA
Romick	Romick	k1gMnSc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
L.	L.	kA
Dufeau	Dufeaa	k1gMnSc4
<g/>
,	,	kIx,
Lawrence	Lawrenec	k1gMnSc2
M.	M.	kA
Witmer	Witmer	k1gMnSc1
<g/>
,	,	kIx,
Thomas	Thomas	k1gMnSc1
L.	L.	kA
Stubbs	Stubbs	k1gInSc1
<g/>
,	,	kIx,
Emily	Emil	k1gMnPc4
J.	J.	kA
Rayfield	Rayfieldo	k1gNnPc2
&	&	k?
Michael	Michael	k1gMnSc1
J.	J.	kA
Benton	Benton	k1gInSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ontogenetic	Ontogenetice	k1gFnPc2
endocranial	endocranial	k1gInSc4
shape	shapat	k5eAaPmIp3nS
change	change	k1gFnSc1
in	in	k?
alligators	alligators	k1gInSc1
and	and	k?
ostriches	ostriches	k1gInSc1
and	and	k?
implications	implications	k6eAd1
for	forum	k1gNnPc2
the	the	k?
development	development	k1gMnSc1
of	of	k?
the	the	k?
non‐	non‐	k1gInSc1
dinosaur	dinosaura	k1gFnPc2
endocranium	endocranium	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Anatomical	Anatomical	k1gMnSc1
Record	Record	k1gMnSc1
(	(	kIx(
<g/>
advance	advance	k1gFnSc1
online	onlinout	k5eAaPmIp3nS
publication	publication	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1002/ar.24579	https://doi.org/10.1002/ar.24579	k4
<g/>
↑	↑	k?
https://dinosaurusblog.com/2015/03/03/dejiny-vyzkumu-dinosaurich-barev/	https://dinosaurusblog.com/2015/03/03/dejiny-vyzkumu-dinosaurich-barev/	k4
<g/>
↑	↑	k?
Marco	Marco	k1gMnSc1
Romano	Romano	k1gMnSc1
&	&	k?
James	James	k1gMnSc1
Farlow	Farlow	k1gMnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bacteria	Bacterium	k1gNnSc2
meet	meeta	k1gFnPc2
the	the	k?
"	"	kIx"
<g/>
titans	titans	k1gInSc1
<g/>
"	"	kIx"
<g/>
:	:	kIx,
horizontal	horizontat	k5eAaImAgMnS,k5eAaPmAgMnS
transfer	transfer	k1gInSc4
of	of	k?
symbiotic	symbiotice	k1gFnPc2
microbiota	microbiota	k1gFnSc1
as	as	k1gNnSc2
a	a	k8xC
possible	possible	k6eAd1
driving	driving	k1gInSc1
factor	factor	k1gInSc1
of	of	k?
sociality	socialita	k1gFnSc2
in	in	k?
dinosaurs	dinosaurs	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bollettino	Bollettina	k1gFnSc5
della	dell	k1gMnSc2
Societa	societa	k1gFnSc1
Paleontologica	Paleontologica	k1gFnSc1
Italiana	Italiana	k1gFnSc1
57	#num#	k4
<g/>
:	:	kIx,
75	#num#	k4
<g/>
-	-	kIx~
<g/>
79	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.443	10.443	k4
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
BSPI	BSPI	kA
<g/>
.2018	.2018	k4
<g/>
.05	.05	k4
<g/>
↑	↑	k?
https://dinomuseum.ca/2019/11/04/all-about-baby-dinosaurs/	https://dinomuseum.ca/2019/11/04/all-about-baby-dinosaurs/	k4
<g/>
↑	↑	k?
Socha	Socha	k1gMnSc1
<g/>
,	,	kIx,
V.	V.	kA
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravěcí	pravěký	k2eAgMnPc1d1
vládci	vládce	k1gMnPc1
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kazda	Kazda	k1gMnSc1
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
88316	#num#	k4
<g/>
-	-	kIx~
<g/>
75	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
str	str	kA
<g/>
.	.	kIx.
170	#num#	k4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
http://www.nhm.ac.uk/discover/were-dinosaurs-good-parents.html	http://www.nhm.ac.uk/discover/were-dinosaurs-good-parents.html	k1gInSc1
<g/>
↑	↑	k?
https://dinosaurusblog.com/2009/06/02/790787-dinosauri-vejce/	https://dinosaurusblog.com/2009/06/02/790787-dinosauri-vejce/	k4
<g/>
↑	↑	k?
https://dinosaurusblog.com/2020/06/25/prvni-dinosauri-kladli-mekka-vejce/	https://dinosaurusblog.com/2020/06/25/prvni-dinosauri-kladli-mekka-vejce/	k4
<g/>
↑	↑	k?
Lucas	Lucas	k1gMnSc1
J.	J.	kA
Legendre	Legendr	k1gInSc5
<g/>
,	,	kIx,
David	David	k1gMnSc1
Rubilar-Rogers	Rubilar-Rogersa	k1gFnPc2
<g/>
,	,	kIx,
Alexander	Alexandra	k1gFnPc2
O.	O.	kA
Vargas	Vargas	k1gInSc1
&	&	k?
Julia	Julius	k1gMnSc2
A.	A.	kA
Clarke	Clark	k1gMnSc2
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
first	first	k1gMnSc1
dinosaur	dinosaur	k1gMnSc1
egg	egg	k?
remains	remains	k1gInSc1
a	a	k8xC
mystery	myster	k1gInPc1
<g/>
.	.	kIx.
bioRxiv	bioRxiv	k6eAd1
2	#num#	k4
<g/>
020.12.10.406	020.12.10.406	k4
<g/>
678	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1101/2020.12.10.406678	https://doi.org/10.1101/2020.12.10.406678	k4
<g/>
↑	↑	k?
V.	V.	kA
Socha	Socha	k1gMnSc1
v	v	k7c6
pořadu	pořad	k1gInSc6
ČRo	ČRo	k1gFnSc1
Meteor	meteor	k1gInSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
,	,	kIx,
čas	čas	k1gInSc1
16	#num#	k4
<g/>
:	:	kIx,
<g/>
37	#num#	k4
min	mina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
↑	↑	k?
https://dinosaurusblog.com/2009/02/27/769708-dinosaurak-nebo-dinosaurice/	https://dinosaurusblog.com/2009/02/27/769708-dinosaurak-nebo-dinosaurice/	k4
<g/>
↑	↑	k?
https://dinosaurusblog.com/2014/06/09/rodili-dinosauri-ziva-mladata/	https://dinosaurusblog.com/2014/06/09/rodili-dinosauri-ziva-mladata/	k4
<g/>
↑	↑	k?
http://dinosaurusblog.com/2016/08/25/tyrannosaurus-sex/	http://dinosaurusblog.com/2016/08/25/tyrannosaurus-sex/	k4
<g/>
↑	↑	k?
Jakob	Jakob	k1gMnSc1
Vinther	Vinthra	k1gFnPc2
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
Nicholls	Nichollsa	k1gFnPc2
&	&	k?
Diane	Dian	k1gInSc5
A.	A.	kA
Kelly	Kell	k1gInPc1
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
cloacal	cloacat	k5eAaPmAgInS
opening	opening	k1gInSc1
in	in	k?
a	a	k8xC
non-avian	non-avian	k1gMnSc1
dinosaur	dinosaur	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Current	Current	k1gInSc1
Biology	biolog	k1gMnPc4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1016/j.cub.2020.12.039	https://doi.org/10.1016/j.cub.2020.12.039	k4
<g/>
↑	↑	k?
SOCHA	Socha	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc1
rekonstrukce	rekonstrukce	k1gFnSc1
dinosauří	dinosauří	k2eAgFnSc2d1
kloaky	kloaka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSEL	osel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
https://blogs.scientificamerican.com/laelaps/how-dinosaurs-strutted-their-stuff/	https://blogs.scientificamerican.com/laelaps/how-dinosaurs-strutted-their-stuff/	k?
<g/>
↑	↑	k?
Lockley	Locklea	k1gFnSc2
<g/>
,	,	kIx,
M.	M.	kA
<g/>
,	,	kIx,
Houck	Houck	k1gMnSc1
<g/>
,	,	kIx,
K.	K.	kA
<g/>
,	,	kIx,
Matthews	Matthews	k1gInSc1
<g/>
,	,	kIx,
N.	N.	kA
<g/>
,	,	kIx,
McCrea	McCreum	k1gNnSc2
<g/>
,	,	kIx,
R.	R.	kA
<g/>
,	,	kIx,
Xing	Xing	k1gMnSc1
<g/>
,	,	kIx,
L.	L.	kA
<g/>
,	,	kIx,
Tsukui	Tsukui	k1gNnSc2
<g/>
,	,	kIx,
L.	L.	kA
<g/>
,	,	kIx,
Ramezani	Ramezaň	k1gFnSc3
<g/>
,	,	kIx,
J.	J.	kA
<g/>
,	,	kIx,
Breithaupt	Breithaupt	k1gMnSc1
<g/>
,	,	kIx,
B.	B.	kA
<g/>
,	,	kIx,
Cart	Cart	k1gMnSc1
<g/>
,	,	kIx,
K.	K.	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
<g/>
,	,	kIx,
Buckley	Bucklea	k1gFnSc2
<g/>
,	,	kIx,
L.	L.	kA
<g/>
,	,	kIx,
Hadden	Haddna	k1gFnPc2
<g/>
,	,	kIx,
G.	G.	kA
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gFnSc1
theropod	theropoda	k1gFnPc2
display	displaa	k1gMnSc2
arena	areno	k1gNnSc2
sites	sites	k1gMnSc1
in	in	k?
the	the	k?
Cretaceous	Cretaceous	k1gMnSc1
of	of	k?
North	North	k1gMnSc1
America	America	k1gMnSc1
<g/>
:	:	kIx,
clues	clues	k1gMnSc1
to	ten	k3xDgNnSc4
distributions	distributions	k6eAd1
in	in	k?
space	space	k1gMnSc1
and	and	k?
time	time	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cretaceous	Cretaceous	k1gMnSc1
Research	Research	k1gMnSc1
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
j.	j.	k?
<g/>
cretres	cretres	k1gInSc1
<g/>
.2017	.2017	k4
<g/>
.09	.09	k4
<g/>
.009	.009	k4
<g/>
↑	↑	k?
Kohei	Kohei	k1gNnSc2
Tanaka	Tanak	k1gMnSc2
<g/>
,	,	kIx,
Darla	Darl	k1gMnSc2
K.	K.	kA
Zelenitsky	Zelenitsky	k1gMnSc2
<g/>
,	,	kIx,
François	François	k1gInSc1
Therrien	Therrien	k1gInSc1
&	&	k?
Yoshitsugu	Yoshitsug	k1gInSc2
Kobayashi	Kobayash	k1gFnSc2
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nest	Nest	k1gInSc1
substrate	substrat	k1gInSc5
reflects	reflects	k1gInSc1
incubation	incubation	k1gInSc1
style	styl	k1gInSc5
in	in	k?
extant	extant	k1gInSc1
archosaurs	archosaurs	k6eAd1
with	with	k1gInSc1
implications	implicationsa	k1gFnPc2
for	forum	k1gNnPc2
dinosaur	dinosaur	k1gMnSc1
nesting	nesting	k1gInSc4
habits	habitsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Scientific	Scientifice	k1gInPc2
Reports	Reports	k1gInSc1
8	#num#	k4
<g/>
,	,	kIx,
Article	Article	k1gFnSc1
number	number	k1gMnSc1
<g/>
:	:	kIx,
3170	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.103	10.103	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
s	s	k7c7
<g/>
41598	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
18	#num#	k4
<g/>
-	-	kIx~
<g/>
21386	#num#	k4
<g/>
-x	-x	k?
<g/>
↑	↑	k?
Kohei	Kohei	k1gNnSc2
Tanaka	Tanak	k1gMnSc2
<g/>
;	;	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Transition	Transition	k1gInSc1
in	in	k?
Nesting	Nesting	k1gInSc1
Methods	Methods	k1gInSc1
and	and	k?
Behaviors	Behaviors	k1gInSc1
from	from	k1gMnSc1
Non-Avian	Non-Avian	k1gMnSc1
Dinosaurs	Dinosaurs	k1gInSc4
to	ten	k3xDgNnSc4
Birds	Birds	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japanese	Japanese	k1gFnSc1
Journal	Journal	k1gFnSc1
of	of	k?
Ornithology	Ornitholog	k1gMnPc4
67	#num#	k4
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
25	#num#	k4
<g/>
-	-	kIx~
<g/>
40	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.3838/jjo.67.25	https://doi.org/10.3838/jjo.67.25	k4
<g/>
↑	↑	k?
https://www.nature.com/articles/d41586-019-02174-7	https://www.nature.com/articles/d41586-019-02174-7	k4
<g/>
↑	↑	k?
https://www.abicko.cz/clanek/precti-si-priroda/25711/dinosauri-jesle-rodice-s-metrovymi-drapy.html	https://www.abicko.cz/clanek/precti-si-priroda/25711/dinosauri-jesle-rodice-s-metrovymi-drapy.html	k1gMnSc1
<g/>
↑	↑	k?
http://www.pravek.info/zajimavosti/nejvetsi-vejce-vsech-dob/	http://www.pravek.info/zajimavosti/nejvetsi-vejce-vsech-dob/	k?
<g/>
↑	↑	k?
Kohei	Kohei	k1gNnSc2
Tanaka	Tanak	k1gMnSc2
<g/>
,	,	kIx,
Darla	Darl	k1gMnSc2
K.	K.	kA
Zelenitsky	Zelenitsky	k1gMnSc2
<g/>
,	,	kIx,
François	François	k1gInSc1
Therrien	Therrien	k1gInSc1
<g/>
,	,	kIx,
Tadahiro	Tadahiro	k1gNnSc1
Ikeda	Ikeda	k1gFnSc1
<g/>
,	,	kIx,
Katsuhiro	Katsuhiro	k1gNnSc1
Kubota	Kubota	k1gFnSc1
<g/>
,	,	kIx,
Haruo	Haruo	k1gNnSc1
Saegusa	Saegus	k1gMnSc2
<g/>
,	,	kIx,
Tomonori	Tomonori	k1gNnSc1
Tanaka	Tanak	k1gMnSc2
&	&	k?
Kenji	Kenje	k1gFnSc4
Ikuno	Ikuno	k6eAd1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Exceptionally	Exceptionalla	k1gFnSc2
small	smalla	k1gFnPc2
theropod	theropoda	k1gFnPc2
eggs	eggs	k1gInSc1
from	from	k1gMnSc1
the	the	k?
Lower	Lower	k1gMnSc1
Cretaceous	Cretaceous	k1gMnSc1
Ohyamashimo	Ohyamashima	k1gFnSc5
Formation	Formation	k1gInSc1
of	of	k?
Tamba	Tamba	k1gFnSc1
<g/>
,	,	kIx,
Hyogo	Hyoga	k1gMnSc5
Prefecture	Prefectur	k1gMnSc5
<g/>
,	,	kIx,
Japan	japan	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cretaceous	Cretaceous	k1gMnSc1
Research	Research	k1gMnSc1
<g/>
.	.	kIx.
104519	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1016/j.cretres.2020.104519	https://doi.org/10.1016/j.cretres.2020.104519	k4
<g/>
↑	↑	k?
https://dinosaurusblog.com/2020/07/27/objevena-nejmensi-dinosauri-vejce/	https://dinosaurusblog.com/2020/07/27/objevena-nejmensi-dinosauri-vejce/	k4
<g/>
↑	↑	k?
David	David	k1gMnSc1
Hone	hon	k1gInSc5
<g/>
,	,	kIx,
Jordan	Jordan	k1gMnSc1
C.	C.	kA
Mallon	Mallon	k1gInSc1
<g/>
,	,	kIx,
Patrick	Patrick	k1gMnSc1
Hennessey	Hennessea	k1gFnSc2
&	&	k?
Lawrence	Lawrence	k1gFnSc2
M.	M.	kA
Witmer	Witmer	k1gMnSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ontogeny	Ontogen	k1gInPc7
of	of	k?
a	a	k8xC
sexually	sexualla	k1gFnSc2
selected	selected	k1gMnSc1
structure	structur	k1gMnSc5
in	in	k?
an	an	k?
extant	extant	k1gInSc1
archosaur	archosaur	k1gMnSc1
Gavialis	Gavialis	k1gFnPc2
gangeticus	gangeticus	k1gMnSc1
(	(	kIx(
<g/>
Pseudosuchia	Pseudosuchia	k1gFnSc1
<g/>
:	:	kIx,
Crocodylia	Crocodylia	k1gFnSc1
<g/>
)	)	kIx)
with	with	k1gInSc1
implications	implicationsa	k1gFnPc2
for	forum	k1gNnPc2
sexual	sexual	k1gMnSc1
dimorphism	dimorphism	k1gMnSc1
in	in	k?
dinosaurs	dinosaurs	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
PeerJ	PeerJ	k1gFnPc2
<g/>
,	,	kIx,
8	#num#	k4
<g/>
:	:	kIx,
<g/>
e	e	k0
<g/>
9134	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.7717/peerj.9134	https://doi.org/10.7717/peerj.9134	k4
<g/>
↑	↑	k?
https://www.sciencedaily.com/releases/2020/08/200826200704.htm	https://www.sciencedaily.com/releases/2020/08/200826200704.htm	k1gMnSc1
<g/>
↑	↑	k?
Shundong	Shundong	k1gMnSc1
Bi	Bi	k1gMnSc1
<g/>
,	,	kIx,
Romain	Romain	k1gMnSc1
Amiot	Amiot	k1gMnSc1
<g/>
,	,	kIx,
Claire	Clair	k1gMnSc5
Peyre	Peyr	k1gMnSc5
de	de	k?
Fabrè	Fabrè	k1gMnSc1
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
Pittman	Pittman	k1gMnSc1
<g/>
,	,	kIx,
Matthew	Matthew	k1gMnSc1
C.	C.	kA
Lamanna	Lamanno	k1gNnSc2
<g/>
,	,	kIx,
Yilun	Yilun	k1gMnSc1
Yu	Yu	k1gMnSc1
<g/>
,	,	kIx,
Congyu	Congy	k2eAgFnSc4d1
Yu	Yu	k1gFnSc4
<g/>
,	,	kIx,
Tzuruei	Tzurue	k1gMnPc1
Yang	Yang	k1gMnSc1
<g/>
,	,	kIx,
Shukang	Shukang	k1gMnSc1
Zhang	Zhang	k1gMnSc1
<g/>
,	,	kIx,
Qi	Qi	k1gMnSc1
Zhao	Zhao	k1gMnSc1
&	&	k?
Xing	Xing	k1gMnSc1
Xu	Xu	k1gMnSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
An	An	k1gFnSc1
oviraptorid	oviraptorida	k1gFnPc2
preserved	preserved	k1gMnSc1
atop	atop	k1gMnSc1
an	an	k?
embryo-bearing	embryo-bearing	k1gInSc1
egg	egg	k?
clutch	clutch	k1gInSc1
sheds	sheds	k6eAd1
light	light	k2eAgMnSc1d1
on	on	k3xPp3gMnSc1
the	the	k?
reproductive	reproductiv	k1gInSc5
biology	biolog	k1gMnPc4
of	of	k?
non-avialan	non-avialan	k1gInSc4
theropod	theropoda	k1gFnPc2
dinosaurs	dinosaurs	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Science	Scienec	k1gInSc2
Bulletin	bulletin	k1gInSc1
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1016/j.scib.2020.12.018	https://doi.org/10.1016/j.scib.2020.12.018	k4
<g/>
↑	↑	k?
Ryosuke	Ryosuke	k1gFnPc2
Motani	Motan	k1gMnPc1
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sex	sex	k1gInSc1
estimation	estimation	k1gInSc4
from	from	k6eAd1
morphology	morpholog	k1gMnPc7
in	in	k?
living	living	k1gInSc1
animals	animals	k1gInSc1
and	and	k?
dinosaurs	dinosaurs	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zoological	Zoological	k1gMnSc1
Journal	Journal	k1gMnSc1
of	of	k?
the	the	k?
Linnean	Linnean	k1gInSc4
Society	societa	k1gFnSc2
<g/>
.	.	kIx.
zlaa	zlaa	k6eAd1
<g/>
181	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1093/zoolinnean/zlaa181	https://doi.org/10.1093/zoolinnean/zlaa181	k4
<g/>
↑	↑	k?
https://dinosaurusblog.com/2009/03/27/775683-byli-dinosauri-teplokrevni/	https://dinosaurusblog.com/2009/03/27/775683-byli-dinosauri-teplokrevni/	k4
<g/>
↑	↑	k?
https://dinosaurusblog.com/2016/11/29/byli-tyranosauri-teplokrevni/	https://dinosaurusblog.com/2016/11/29/byli-tyranosauri-teplokrevni/	k4
<g/>
↑	↑	k?
https://dinomuseum.ca/2019/05/21/dinosaurs-in-the-frost/	https://dinomuseum.ca/2019/05/21/dinosaurs-in-the-frost/	k4
<g/>
↑	↑	k?
Bakker	Bakker	k1gMnSc1
<g/>
,	,	kIx,
R.	R.	kA
T.	T.	kA
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Dinosaur	Dinosaur	k1gMnSc1
Heresies	Heresies	k1gMnSc1
<g/>
,	,	kIx,
Zebra	zebra	k1gFnSc1
Books	Books	k1gInSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
(	(	kIx(
<g/>
str	str	kA
<g/>
.	.	kIx.
381	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Alexei	Alexe	k1gFnSc2
B.	B.	kA
Herman	Herman	k1gMnSc1
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
A.	A.	kA
Spicer	Spicer	k1gMnSc1
&	&	k?
Teresa	Teresa	k1gFnSc1
E.	E.	kA
V.	V.	kA
Spicer	Spicer	k1gMnSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Environmental	Environmental	k1gMnSc1
Constraints	Constraints	k1gInSc4
on	on	k3xPp3gMnSc1
Terrestrial	Terrestrial	k1gMnSc1
Vertebrate	Vertebrat	k1gInSc5
Behaviour	Behaviour	k1gMnSc1
and	and	k?
Reproduction	Reproduction	k1gInSc1
in	in	k?
the	the	k?
High	High	k1gInSc1
Arctic	Arctice	k1gFnPc2
of	of	k?
the	the	k?
Late	lat	k1gInSc5
Cretaceous	Cretaceous	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Palaeogeography	Palaeogeograph	k1gMnPc7
<g/>
,	,	kIx,
Palaeoclimatology	Palaeoclimatolog	k1gMnPc7
<g/>
,	,	kIx,
Palaeoecology	Palaeoecolog	k1gMnPc7
<g/>
,	,	kIx,
441	#num#	k4
<g/>
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
317	#num#	k4
<g/>
-	-	kIx~
<g/>
338	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
j.	j.	k?
<g/>
palaeo	palaeo	k6eAd1
<g/>
.2015	.2015	k4
<g/>
.09	.09	k4
<g/>
.041	.041	k4
<g/>
↑	↑	k?
https://dinosaurusblog.com/2014/06/19/meli-dinosauri-teplou-krev/	https://dinosaurusblog.com/2014/06/19/meli-dinosauri-teplou-krev/	k4
<g/>
↑	↑	k?
http://technet.idnes.cz/dinosauri-telesna-teplota-jursky-svet-dy5-/veda.aspx?c=A150625_144229_veda_mla	http://technet.idnes.cz/dinosauri-telesna-teplota-jursky-svet-dy5-/veda.aspx?c=A150625_144229_veda_mla	k1gMnSc1
<g/>
↑	↑	k?
http://www.osel.cz/9456-dinosaury-nevyhubila-studenokrevnost.html	http://www.osel.cz/9456-dinosaury-nevyhubila-studenokrevnost.html	k1gMnSc1
<g/>
↑	↑	k?
Enrico	Enrico	k1gMnSc1
L.	L.	kA
Rezende	Rezend	k1gMnSc5
<g/>
,	,	kIx,
Leonardo	Leonardo	k1gMnSc5
D.	D.	kA
Bacigalupe	Bacigalup	k1gInSc5
<g/>
,	,	kIx,
Roberto	Roberta	k1gFnSc5
F.	F.	kA
Nespolo	Nespola	k1gFnSc5
and	and	k?
Francisco	Francisco	k6eAd1
Bozinovic	Bozinovice	k1gFnPc2
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Shrinking	Shrinking	k1gInSc1
dinosaurs	dinosaurs	k1gInSc4
and	and	k?
the	the	k?
evolution	evolution	k1gInSc1
of	of	k?
endothermy	endotherma	k1gFnSc2
in	in	k?
birds	birds	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Science	Science	k1gFnSc1
Advances	Advancesa	k1gFnPc2
<g/>
,	,	kIx,
6	#num#	k4
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
eaaw	eaaw	k?
<g/>
4486	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
10.112	10.112	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
sciadv	sciadva	k1gFnPc2
<g/>
.	.	kIx.
<g/>
aaw	aaw	k?
<g/>
4486	#num#	k4
<g/>
↑	↑	k?
Roger	Rogero	k1gNnPc2
B.	B.	kA
J.	J.	kA
Benson	Benson	k1gMnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dinosaur	Dinosaura	k1gFnPc2
Macroevolution	Macroevolution	k1gInSc4
and	and	k?
Macroecology	Macroecolog	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Annual	Annual	k1gMnSc1
Review	Review	k1gMnSc1
of	of	k?
Ecology	Ecologa	k1gFnSc2
<g/>
,	,	kIx,
Evolution	Evolution	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
Systematics	Systematics	k1gInSc1
49	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1146/annurev-ecolsys-110617-062231	https://doi.org/10.1146/annurev-ecolsys-110617-062231	k4
<g/>
↑	↑	k?
Markus	Markus	k1gMnSc1
Lambertz	Lambertz	k1gMnSc1
<g/>
,	,	kIx,
Filippo	Filippa	k1gFnSc5
Bertozzo	Bertozza	k1gFnSc5
&	&	k?
P.	P.	kA
Martin	Martin	k1gMnSc1
Sander	Sandra	k1gFnPc2
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bone	bon	k1gInSc5
histological	histologicat	k5eAaPmAgInS
correlates	correlates	k1gMnSc1
for	forum	k1gNnPc2
air	air	k?
sacs	sacs	k6eAd1
and	and	k?
their	their	k1gInSc1
implications	implicationsa	k1gFnPc2
for	forum	k1gNnPc2
understanding	understanding	k1gInSc1
the	the	k?
origin	origin	k1gInSc1
of	of	k?
the	the	k?
dinosaurian	dinosaurian	k1gInSc1
respiratory	respirator	k1gInPc4
system	systo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Biology	biolog	k1gMnPc4
Letters	Letters	k1gInSc1
14	#num#	k4
<g/>
:	:	kIx,
20170514	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
10.109	10.109	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
rsbl	rsbnout	k5eAaPmAgInS
<g/>
.2017	.2017	k4
<g/>
.0514	.0514	k4
<g/>
↑	↑	k?
Zhiheng	Zhiheng	k1gMnSc1
Li	li	k9
<g/>
,	,	kIx,
Zhonghe	Zhonghe	k1gNnSc1
Zhou	Zhous	k1gInSc2
&	&	k?
Julia	Julius	k1gMnSc2
A.	A.	kA
Clarke	Clark	k1gMnSc2
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Convergent	Convergent	k1gInSc1
evolution	evolution	k1gInSc4
of	of	k?
a	a	k8xC
mobile	mobile	k1gNnSc4
bony	bona	k1gFnSc2
tongue	tongu	k1gInSc2
in	in	k?
flighted	flighted	k1gInSc1
dinosaurs	dinosaurs	k1gInSc1
and	and	k?
pterosaurs	pterosaurs	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
PLoS	plos	k1gMnSc1
ONE	ONE	kA
13	#num#	k4
<g/>
(	(	kIx(
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
e	e	k0
<g/>
0	#num#	k4
<g/>
198078	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1371/journal.pone.0198078	https://doi.org/10.1371/journal.pone.0198078	k4
<g/>
↑	↑	k?
Robert	Robert	k1gMnSc1
J.	J.	kA
Brocklehurst	Brocklehurst	k1gMnSc1
<g/>
,	,	kIx,
Emma	Emma	k1gFnSc1
R.	R.	kA
Schachner	Schachner	k1gInSc1
&	&	k?
William	William	k1gInSc1
I.	I.	kA
Sellers	Sellers	k1gInSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vertebral	Vertebral	k1gFnSc1
morphometrics	morphometricsa	k1gFnPc2
and	and	k?
lung	lung	k1gMnSc1
structure	structur	k1gMnSc5
in	in	k?
non-avian	non-avian	k1gInSc4
dinosaurs	dinosaursa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Royal	Royal	k1gInSc4
Society	societa	k1gFnSc2
Open	Open	k1gNnSc4
Science	Scienec	k1gInSc2
5	#num#	k4
<g/>
:	:	kIx,
180983	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
10.109	10.109	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
rsos	rsos	k6eAd1
<g/>
.180983	.180983	k4
<g/>
↑	↑	k?
https://blogs.scientificamerican.com/laelaps/dinosaurs-had-bird-like-lungs/	https://blogs.scientificamerican.com/laelaps/dinosaurs-had-bird-like-lungs/	k?
<g/>
↑	↑	k?
Wm	Wm	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruger	Ruger	k1gInSc1
Porter	porter	k1gInSc4
&	&	k?
Lawrence	Lawrenec	k1gInSc2
M.	M.	kA
Witmer	Witmer	k1gMnSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vascular	Vascular	k1gInSc1
Patterns	Patterns	k1gInSc4
in	in	k?
the	the	k?
Heads	Heads	k1gInSc1
of	of	k?
Dinosaurs	Dinosaurs	k1gInSc1
<g/>
:	:	kIx,
Evidence	evidence	k1gFnPc1
for	forum	k1gNnPc2
Blood	Blooda	k1gFnPc2
Vessels	Vesselsa	k1gFnPc2
<g/>
,	,	kIx,
Sites	Sitesa	k1gFnPc2
of	of	k?
Thermal	Thermal	k1gInSc1
Exchange	Exchange	k1gInSc1
<g/>
,	,	kIx,
and	and	k?
Their	Their	k1gInSc1
Role	role	k1gFnSc1
in	in	k?
Physiological	Physiological	k1gFnSc2
Thermoregulatory	Thermoregulator	k1gInPc1
Strategies	Strategiesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Anatomical	Anatomical	k1gMnSc1
Record	Record	k1gMnSc1
(	(	kIx(
<g/>
advance	advance	k1gFnSc1
online	onlinout	k5eAaPmIp3nS
publication	publication	k1gInSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1002/ar.24234	https://doi.org/10.1002/ar.24234	k4
<g/>
↑	↑	k?
https://dinosaurusblog.com/2009/04/03/777177-jak-rychle-dinosauri-rostli/	https://dinosaurusblog.com/2009/04/03/777177-jak-rychle-dinosauri-rostli/	k4
<g/>
↑	↑	k?
http://www.eartharchives.org/articles/dinosaur-fossils-show-that-they-were-bruisers/	http://www.eartharchives.org/articles/dinosaur-fossils-show-that-they-were-bruisers/	k?
<g/>
↑	↑	k?
Bakker	Bakker	k1gInSc1
<g/>
,	,	kIx,
R.	R.	kA
T.	T.	kA
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Dinosaur	Dinosaur	k1gMnSc1
Heresies	Heresies	k1gMnSc1
<g/>
,	,	kIx,
Zebra	zebra	k1gFnSc1
Books	Books	k1gInSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
(	(	kIx(
<g/>
str	str	kA
<g/>
.	.	kIx.
357	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Trevor	Trevor	k1gMnSc1
G.	G.	kA
Aguirre	Aguirr	k1gInSc5
<g/>
,	,	kIx,
Aniket	Aniket	k1gInSc4
Ingrole	Ingrole	k1gFnSc2
<g/>
,	,	kIx,
Luca	Luca	k1gMnSc1
Fuller	Fuller	k1gMnSc1
<g/>
,	,	kIx,
Tim	Tim	k?
W.	W.	kA
Seek	Seek	k1gInSc1
<g/>
,	,	kIx,
Anthony	Anthon	k1gInPc1
R.	R.	kA
Fiorillo	Fiorillo	k1gNnSc1
<g/>
,	,	kIx,
Joseph	Joseph	k1gInSc1
J.	J.	kA
W.	W.	kA
Sertich	Sertich	k1gMnSc1
&	&	k?
Seth	Seth	k1gMnSc1
W.	W.	kA
Donahue	Donahue	k1gFnSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Differing	Differing	k1gInSc1
trabecular	trabecular	k1gInSc4
bone	bon	k1gInSc5
architecture	architectur	k1gMnSc5
in	in	k?
dinosaurs	dinosaurs	k1gInSc1
and	and	k?
mammals	mammals	k1gInSc1
contribute	contribut	k1gInSc5
to	ten	k3xDgNnSc1
stiffness	stiffness	k1gInSc1
and	and	k?
limits	limits	k1gInSc1
on	on	k3xPp3gMnSc1
bone	bon	k1gInSc5
strain	strain	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
PLoS	plos	k1gMnSc1
ONE	ONE	kA
<g/>
,	,	kIx,
15	#num#	k4
<g/>
(	(	kIx(
<g/>
8	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
e	e	k0
<g/>
0	#num#	k4
<g/>
237042	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1371/journal.pone.0237042	https://doi.org/10.1371/journal.pone.0237042	k4
<g/>
↑	↑	k?
Erickson	Ericksona	k1gFnPc2
<g/>
,	,	kIx,
Gregory	Gregor	k1gMnPc4
M.	M.	kA
<g/>
;	;	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Gigantism	Gigantism	k1gInSc1
and	and	k?
comparative	comparativ	k1gInSc5
life-history	life-histor	k1gInPc1
parameters	parameters	k1gInSc1
of	of	k?
tyrannosaurid	tyrannosaurid	k1gInSc1
dinosaurs	dinosaurs	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
<g/>
.	.	kIx.
430	#num#	k4
(	(	kIx(
<g/>
7001	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
772	#num#	k4
<g/>
–	–	k?
<g/>
775	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.103	10.103	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
nature	natur	k1gMnSc5
<g/>
0	#num#	k4
<g/>
2699	#num#	k4
<g/>
↑	↑	k?
https://dinosaurusblog.com/2019/09/23/petapadesatiny-nejstarsiho-dinosaura/	https://dinosaurusblog.com/2019/09/23/petapadesatiny-nejstarsiho-dinosaura/	k4
<g/>
↑	↑	k?
https://dinosaurusblog.com/2009/04/10/778654-take-dinosauri-umirali-na-rakovinu/	https://dinosaurusblog.com/2009/04/10/778654-take-dinosauri-umirali-na-rakovinu/	k4
<g/>
↑	↑	k?
http://dinosaurusblog.com/2016/04/19/giganticke-blechy-v-dinosaurim-peri/	http://dinosaurusblog.com/2016/04/19/giganticke-blechy-v-dinosaurim-peri/	k4
<g/>
↑	↑	k?
http://www.osel.cz/index.php?clanek=8105	http://www.osel.cz/index.php?clanek=8105	k4
<g/>
↑	↑	k?
https://www.osel.cz/11560-pekelne-rychly-polsky-raptor.html	https://www.osel.cz/11560-pekelne-rychly-polsky-raptor.html	k1gMnSc1
<g/>
↑	↑	k?
https://www.idnes.cz/technet/veda/tyranosaurus-vyvoj-historie-dinosaurus-stopa-stopy.A210212_141152_veda_vse	https://www.idnes.cz/technet/veda/tyranosaurus-vyvoj-historie-dinosaurus-stopa-stopy.A210212_141152_veda_vse	k1gFnSc2
<g/>
↑	↑	k?
http://dinosaurusblog.com/2016/07/04/jak-velke-vnitrni-organy-meli-obri-sauropodi/	http://dinosaurusblog.com/2016/07/04/jak-velke-vnitrni-organy-meli-obri-sauropodi/	k4
<g/>
↑	↑	k?
Les	les	k1gInSc1
Hearn	Hearn	k1gInSc1
and	and	k?
Amanda	Amanda	k1gFnSc1
C.	C.	kA
de	de	k?
C.	C.	kA
Williams	Williams	k1gInSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pain	Pain	k1gInSc1
in	in	k?
dinosaurs	dinosaurs	k1gInSc1
<g/>
:	:	kIx,
what	what	k1gInSc1
is	is	k?
the	the	k?
evidence	evidence	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
Philosophical	Philosophical	k1gFnSc1
Transactions	Transactionsa	k1gFnPc2
of	of	k?
the	the	k?
Royal	Royal	k1gMnSc1
Society	societa	k1gFnSc2
B	B	kA
<g/>
:	:	kIx,
Biological	Biological	k1gMnSc1
Sciences	Sciences	k1gMnSc1
374	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1098/rstb.2019.0370	https://doi.org/10.1098/rstb.2019.0370	k4
<g/>
↑	↑	k?
http://www.osel.cz/9592-krasne-parasaurolofovo-buceni.html	http://www.osel.cz/9592-krasne-parasaurolofovo-buceni.html	k1gMnSc1
<g/>
↑	↑	k?
https://www.idnes.cz/technet/veda/jak-znel-t-rex.A190927_100835_veda_mla	https://www.idnes.cz/technet/veda/jak-znel-t-rex.A190927_100835_veda_mla	k6eAd1
<g/>
↑	↑	k?
Robin	Robina	k1gFnPc2
R.	R.	kA
Dawson	Dawson	k1gMnSc1
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
J.	J.	kA
Field	Field	k1gMnSc1
<g/>
,	,	kIx,
Pincelli	Pincell	k1gMnPc1
M.	M.	kA
Hull	Hull	k1gMnSc1
<g/>
,	,	kIx,
Darla	Darla	k1gMnSc1
K.	K.	kA
Zelenitsky	Zelenitsky	k1gMnSc1
<g/>
,	,	kIx,
François	François	k1gFnSc1
Therrien	Therrina	k1gFnPc2
and	and	k?
Hagit	Hagit	k1gMnSc1
P.	P.	kA
Affek	Affek	k1gMnSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Eggshell	Eggshell	k1gInSc1
geochemistry	geochemistr	k1gMnPc7
reveals	reveals	k6eAd1
ancestral	ancestrat	k5eAaPmAgMnS,k5eAaImAgMnS
metabolic	metabolice	k1gFnPc2
thermoregulation	thermoregulation	k1gInSc4
in	in	k?
Dinosauria	Dinosaurium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Science	Science	k1gFnSc1
Advances	Advancesa	k1gFnPc2
<g/>
,	,	kIx,
6	#num#	k4
<g/>
(	(	kIx(
<g/>
7	#num#	k4
<g/>
)	)	kIx)
eaax	eaax	k1gInSc1
<g/>
9361	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
10.112	10.112	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
sciadv	sciadva	k1gFnPc2
<g/>
.	.	kIx.
<g/>
aax	aax	k?
<g/>
9361	#num#	k4
<g/>
↑	↑	k?
SOCHA	Socha	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pekelně	pekelně	k6eAd1
rychlý	rychlý	k2eAgInSc1d1
polský	polský	k2eAgInSc1d1
raptor	raptor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSEL	osel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
20	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
http://www.osel.cz/10848-umeli-dinosauri-plavat.html	http://www.osel.cz/10848-umeli-dinosauri-plavat.html	k1gInSc1
<g/>
↑	↑	k?
https://dinomuseum.ca/2020/07/31/swimming-dinosaurs/	https://dinomuseum.ca/2020/07/31/swimming-dinosaurs/	k4
<g/>
↑	↑	k?
Rozhovor	rozhovor	k1gInSc1
pro	pro	k7c4
rozhlasový	rozhlasový	k2eAgInSc4d1
pořad	pořad	k1gInSc4
Meteor	meteor	k1gInSc1
(	(	kIx(
<g/>
čas	čas	k1gInSc1
17	#num#	k4
<g/>
:	:	kIx,
<g/>
53	#num#	k4
minut	minuta	k1gFnPc2
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Roger	Rogero	k1gNnPc2
B.	B.	kA
J.	J.	kA
Benson	Benson	k1gMnSc1
&	&	k?
Paul	Paul	k1gMnSc1
M.	M.	kA
Barrett	Barrett	k1gMnSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evolution	Evolution	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Two	Two	k1gMnSc1
Faces	Faces	k1gMnSc1
of	of	k?
Plant-Eating	Plant-Eating	k1gInSc1
Dinosaurs	Dinosaurs	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Current	Current	k1gInSc1
Biology	biolog	k1gMnPc7
<g/>
,	,	kIx,
30	#num#	k4
<g/>
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
R	R	kA
<g/>
14	#num#	k4
<g/>
-R	-R	k?
<g/>
16	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1016/j.cub.2019.11.035.	https://doi.org/10.1016/j.cub.2019.11.035.	k4
doi	doi	k?
<g/>
:	:	kIx,
https://www.cell.com/current-biology/fulltext/S0960-9822(19	https://www.cell.com/current-biology/fulltext/S0960-9822(19	k4
<g/>
)	)	kIx)
<g/>
31505	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
↑	↑	k?
http://www.osel.cz/11044-jak-moc-pachl-tyrannosaurus-rex.html	http://www.osel.cz/11044-jak-moc-pachl-tyrannosaurus-rex.html	k1gMnSc1
<g/>
↑	↑	k?
SOCHA	Socha	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dinosauří	dinosauří	k2eAgInSc1d1
rok	rok	k1gInSc1
měl	mít	k5eAaImAgInS
372	#num#	k4
dnů	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSEL	osel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
18	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
https://dinosaurusblog.com/2013/07/17/dinosauri-zkameneliny-vyuzivali-lide-jiz-v-paleolitu/	https://dinosaurusblog.com/2013/07/17/dinosauri-zkameneliny-vyuzivali-lide-jiz-v-paleolitu/	k4
<g/>
↑	↑	k?
SOCHA	Socha	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
stopách	stopa	k1gFnPc6
Fénixe	fénix	k1gMnSc2
vstříc	vstříc	k7c3
dinosaurům	dinosaurus	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSEL	osel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Xing	Xing	k1gInSc1
<g/>
,	,	kIx,
L.	L.	kA
D.	D.	kA
<g/>
;	;	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stone	ston	k1gInSc5
flowers	flowers	k1gInSc4
explained	explained	k1gInSc1
as	as	k1gNnSc1
dinosaur	dinosaura	k1gFnPc2
undertracks	undertracksa	k1gFnPc2
<g/>
:	:	kIx,
unusual	unusuat	k5eAaPmAgMnS,k5eAaImAgMnS,k5eAaBmAgMnS
ichnites	ichnites	k1gMnSc1
from	from	k1gMnSc1
the	the	k?
Lower	Lower	k1gMnSc1
Cretaceous	Cretaceous	k1gMnSc1
Jiaguan	Jiaguan	k1gMnSc1
Formation	Formation	k1gInSc1
<g/>
,	,	kIx,
Qijiang	Qijiang	k1gMnSc1
District	District	k1gMnSc1
<g/>
,	,	kIx,
Chongqing	Chongqing	k1gInSc1
<g/>
,	,	kIx,
China	China	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geological	Geological	k1gFnSc1
Bulletin	bulletin	k1gInSc1
of	of	k?
China	China	k1gFnSc1
34	#num#	k4
<g/>
(	(	kIx(
<g/>
5	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
885	#num#	k4
<g/>
–	–	k?
<g/>
890	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
SOCHA	Socha	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dinosauři	dinosaurus	k1gMnPc1
z	z	k7c2
Lotosové	lotosový	k2eAgFnSc2d1
pevnosti	pevnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSEL	osel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
https://dinosaurusblog.com/2009/06/25/795085-byli-dinosauri-objeveni-jiz-ve-staroveku/	https://dinosaurusblog.com/2009/06/25/795085-byli-dinosauri-objeveni-jiz-ve-staroveku/	k4
<g/>
↑	↑	k?
https://www.abicko.cz/clanek/precti-si-priroda/26070/dinosauri-nebo-draci-co-si-ve-stredoveku-mysleli-o-zkamenelinach.html	https://www.abicko.cz/clanek/precti-si-priroda/26070/dinosauri-nebo-draci-co-si-ve-stredoveku-mysleli-o-zkamenelinach.html	k1gMnSc1
<g/>
↑	↑	k?
SOCHA	Socha	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dinosauři	dinosaurus	k1gMnPc1
nejsou	být	k5eNaImIp3nP
draci	drak	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSEL	osel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
https://dinosaurusblog.com/2011/01/18/879011-mocne-dinosauri-bozstvo/	https://dinosaurusblog.com/2011/01/18/879011-mocne-dinosauri-bozstvo/	k4
<g/>
↑	↑	k?
http://dinosaurusblog.com/2015/11/02/indiani-a-dinosauri/	http://dinosaurusblog.com/2015/11/02/indiani-a-dinosauri/	k4
<g/>
↑	↑	k?
https://dinosaurusblog.com/2009/10/16/816850-nejstarsi-evropske-zpodobneni-dinosauri-fosilie/	https://dinosaurusblog.com/2009/10/16/816850-nejstarsi-evropske-zpodobneni-dinosauri-fosilie/	k4
<g/>
↑	↑	k?
http://dinosaurusblog.com/2017/02/28/giganticky-oslik-z-portugalska/	http://dinosaurusblog.com/2017/02/28/giganticky-oslik-z-portugalska/	k4
<g/>
↑	↑	k?
http://www.osel.cz/index.php?clanek=8153	http://www.osel.cz/index.php?clanek=8153	k4
<g/>
↑	↑	k?
http://www.osel.cz/10936-dinosauri-certi-z-polska.html	http://www.osel.cz/10936-dinosauri-certi-z-polska.html	k1gMnSc1
<g/>
↑	↑	k?
SOCHA	Socha	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znal	znát	k5eAaImAgMnS
Aristotelés	Aristotelés	k1gInSc4
dinosauří	dinosauří	k2eAgFnSc2d1
fosilie	fosilie	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSEL	osel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
18	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Brianna	Briann	k1gInSc2
A.	A.	kA
Santucci	Santucce	k1gFnSc4
<g/>
,	,	kIx,
Carol	Carol	k1gInSc4
A.	A.	kA
Moneymaker	Moneymaker	k1gInSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
F.	F.	kA
Lisco	Lisco	k1gMnSc1
and	and	k?
Vincent	Vincent	k1gMnSc1
L.	L.	kA
Santucci	Santucce	k1gFnSc6
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
An	An	k1gFnSc1
overview	overview	k?
of	of	k?
paleontological	paleontologicat	k5eAaPmAgMnS
resources	resources	k1gMnSc1
preserved	preserved	k1gMnSc1
within	within	k1gMnSc1
prehistoric	prehistoric	k1gMnSc1
and	and	k?
historic	historic	k1gMnSc1
structures	structures	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
In	In	k1gMnSc1
<g/>
:	:	kIx,
Lucas	Lucas	k1gMnSc1
<g/>
,	,	kIx,
S.	S.	kA
G.	G.	kA
<g/>
,	,	kIx,
Hunt	hunt	k1gInSc1
<g/>
,	,	kIx,
A.	A.	kA
P.	P.	kA
&	&	k?
Lichtig	Lichtig	k1gInSc1
<g/>
,	,	kIx,
A.	A.	kA
J.	J.	kA
<g/>
,	,	kIx,
2021	#num#	k4
<g/>
,	,	kIx,
Fossil	Fossil	k1gFnSc1
Record	Record	k1gInSc1
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
Mexico	Mexico	k1gMnSc1
Museum	museum	k1gNnSc1
of	of	k?
Natural	Natural	k?
History	Histor	k1gInPc4
and	and	k?
Science	Science	k1gFnSc1
Bulletin	bulletin	k1gInSc1
<g/>
,	,	kIx,
82	#num#	k4
<g/>
:	:	kIx,
347	#num#	k4
<g/>
-	-	kIx~
<g/>
356	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
http://www.pravek.info/zajimavosti/sourek-lidsky-aneb-pikantni-vstup-dinosauru-do-dejin/	http://www.pravek.info/zajimavosti/sourek-lidsky-aneb-pikantni-vstup-dinosauru-do-dejin/	k?
<g/>
↑	↑	k?
Arnaud	Arnaud	k1gMnSc1
Brignon	Brignon	k1gMnSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Les	les	k1gInSc1
premiè	premiè	k1gMnSc1
découvertes	découvertes	k1gMnSc1
de	de	k?
vertébrés	vertébrés	k1gInSc1
jurassiques	jurassiques	k1gMnSc1
aux	aux	k?
Vaches	Vaches	k1gMnSc1
Noires	Noires	k1gMnSc1
(	(	kIx(
<g/>
Calvados	Calvados	k1gMnSc1
<g/>
,	,	kIx,
France	Franc	k1gMnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
early	earl	k1gMnPc4
discoveries	discoveriesa	k1gFnPc2
of	of	k?
Jurassic	Jurassic	k1gMnSc1
vertebrate	vertebrat	k1gInSc5
remains	remainsa	k1gFnPc2
in	in	k?
the	the	k?
Vaches	Vaches	k1gMnSc1
Noires	Noires	k1gMnSc1
(	(	kIx(
<g/>
Normandy	Normandy	k?
<g/>
,	,	kIx,
France	Franc	k1gMnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Actes	Actes	k1gInSc1
du	du	k?
1	#num#	k4
<g/>
er	er	k?
Colloque	Colloqu	k1gMnSc4
Paléontologie	Paléontologie	k1gFnSc2
et	et	k?
Archéologie	Archéologie	k1gFnSc2
en	en	k?
Normandie	Normandie	k1gFnSc2
de	de	k?
Villers-sur-Mer	Villers-sur-Mer	k1gInSc1
5-6	5-6	k4
octobre	octobr	k1gInSc5
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bulletin	bulletin	k1gInSc1
de	de	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
Association	Association	k1gInSc1
paléontologique	paléontologiqu	k1gFnSc2
de	de	k?
Villers-sur-Mer	Villers-sur-Mer	k1gMnSc1
<g/>
:	:	kIx,
2020	#num#	k4
<g/>
:	:	kIx,
7	#num#	k4
<g/>
-	-	kIx~
<g/>
40	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
https://dinosaurusblog.com/2009/02/20/768006-185-let-dinosauri-paleontologie/	https://dinosaurusblog.com/2009/02/20/768006-185-let-dinosauri-paleontologie/	k4
<g/>
↑	↑	k?
Arnaud	Arnaud	k1gMnSc1
Brignon	Brignon	k1gMnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nouvelles	Nouvelles	k1gMnSc1
données	données	k1gMnSc1
historiques	historiques	k1gMnSc1
sur	sur	k?
les	les	k1gInSc1
premiers	premiers	k1gInSc1
dinosaures	dinosaures	k1gInSc1
trouvés	trouvés	k1gInSc1
en	en	k?
France	Franc	k1gMnSc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
New	New	k1gMnSc1
historical	historicat	k5eAaPmAgMnS
data	datum	k1gNnPc4
on	on	k3xPp3gMnSc1
the	the	k?
first	first	k5eAaPmF
dinosaurs	dinosaurs	k6eAd1
found	found	k1gInSc1
in	in	k?
France	Franc	k1gMnSc2
<g/>
.	.	kIx.
<g/>
]	]	kIx)
Bulletin	bulletin	k1gInSc1
de	de	k?
la	la	k1gNnSc7
Société	Sociétý	k2eAgFnSc2d1
Géologique	Géologiqu	k1gFnSc2
de	de	k?
France	Franc	k1gMnSc2
189	#num#	k4
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
4	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1051/bsgf/2018003	https://doi.org/10.1051/bsgf/2018003	k4
<g/>
↑	↑	k?
https://paleonerdish.wordpress.com/2020/01/31/from-mantell-to-de-ricqles-a-brief-history-of-paleohistology/	https://paleonerdish.wordpress.com/2020/01/31/from-mantell-to-de-ricqles-a-brief-history-of-paleohistology/	k4
<g/>
↑	↑	k?
Socha	Socha	k1gMnSc1
<g/>
,	,	kIx,
V.	V.	kA
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravěcí	pravěký	k2eAgMnPc1d1
vládci	vládce	k1gMnPc1
Evropy	Evropa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kazda	Kazda	k1gMnSc1
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
88316	#num#	k4
<g/>
-	-	kIx~
<g/>
75	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
str	str	kA
<g/>
.	.	kIx.
24	#num#	k4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
http://dinosaurusblog.com/2015/10/27/dinosauri-renesance/	http://dinosaurusblog.com/2015/10/27/dinosauri-renesance/	k4
<g/>
↑	↑	k?
http://www.pravek.info/zajimavosti/dinosauri-renesance/	http://www.pravek.info/zajimavosti/dinosauri-renesance/	k?
<g/>
↑	↑	k?
SOCHA	Socha	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dinosauři	dinosaurus	k1gMnPc1
ze	z	k7c2
Špicberků	Špicberky	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSEL	osel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Matthew	Matthew	k1gMnSc1
C.	C.	kA
Lamanna	Lamanna	k1gFnSc1
<g/>
;	;	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Late	lat	k1gInSc5
Cretaceous	Cretaceous	k1gInSc1
non-avian	non-avian	k1gMnSc1
dinosaurs	dinosaursa	k1gFnPc2
from	from	k1gMnSc1
the	the	k?
James	James	k1gMnSc1
Ross	Rossa	k1gFnPc2
Basin	Basin	k1gMnSc1
<g/>
,	,	kIx,
Antarctica	Antarctica	k1gMnSc1
<g/>
:	:	kIx,
description	description	k1gInSc1
of	of	k?
new	new	k?
material	material	k1gMnSc1
<g/>
,	,	kIx,
updated	updated	k1gMnSc1
synthesis	synthesis	k1gFnSc2
<g/>
,	,	kIx,
biostratigraphy	biostratigrapha	k1gFnSc2
<g/>
,	,	kIx,
and	and	k?
paleobiogeography	paleobiogeographa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Advances	Advances	k1gInSc1
in	in	k?
Polar	Polar	k1gInSc1
Science	Science	k1gFnSc1
30	#num#	k4
<g/>
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
10.136	10.136	k4
<g/>
79	#num#	k4
<g/>
/	/	kIx~
<g/>
j.	j.	k?
<g/>
advps	advps	k1gInSc1
<g/>
.2019	.2019	k4
<g/>
.0007	.0007	k4
<g/>
↑	↑	k?
Alida	Alida	k1gMnSc1
M.	M.	kA
Bailleul	Bailleul	k1gInSc1
<g/>
,	,	kIx,
Jingmai	Jingmai	k1gNnPc1
O	o	k7c4
<g/>
'	'	kIx"
<g/>
Connor	Connor	k1gMnSc1
&	&	k?
Mary	Mary	k1gFnSc2
H.	H.	kA
Schweitzer	Schweitzer	k1gMnSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dinosaur	Dinosaur	k1gMnSc1
paleohistology	paleohistolog	k1gMnPc4
<g/>
:	:	kIx,
review	review	k?
<g/>
,	,	kIx,
trends	trends	k1gInSc1
and	and	k?
new	new	k?
avenues	avenues	k1gInSc1
of	of	k?
investigation	investigation	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
PeerJ	PeerJ	k1gFnSc1
7	#num#	k4
<g/>
:	:	kIx,
<g/>
e	e	k0
<g/>
7764	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.7717/peerj.7764	https://doi.org/10.7717/peerj.7764	k4
<g/>
↑	↑	k?
Článek	článek	k1gInSc1
o	o	k7c6
nových	nový	k2eAgInPc6d1
objevech	objev	k1gInPc6
v	v	k7c6
posledních	poslední	k2eAgNnPc6d1
50	#num#	k4
letech	léto	k1gNnPc6
na	na	k7c6
webu	web	k1gInSc6
Pravěk	pravěk	k1gInSc1
<g/>
.	.	kIx.
<g/>
info	info	k6eAd1
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
http://www.osel.cz/10957-dinosauri-statistika-za-rok-2019.html	http://www.osel.cz/10957-dinosauri-statistika-za-rok-2019.html	k1gMnSc1
<g/>
↑	↑	k?
Bakker	Bakker	k1gMnSc1
<g/>
,	,	kIx,
R.	R.	kA
T.	T.	kA
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Dinosaur	Dinosaur	k1gMnSc1
Heresies	Heresies	k1gMnSc1
<g/>
,	,	kIx,
Zebra	zebra	k1gFnSc1
Books	Books	k1gInSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
22	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
https://dinosaurusblog.com/2008/02/20/658986-velka-dinosauri-statistika/	https://dinosaurusblog.com/2008/02/20/658986-velka-dinosauri-statistika/	k4
<g/>
↑	↑	k?
http://dinosaurusblog.com/2016/03/30/vetsinu-dinosauru-jeste-nezname/	http://dinosaurusblog.com/2016/03/30/vetsinu-dinosauru-jeste-nezname/	k4
<g/>
↑	↑	k?
https://dinosaurusblog.com/2019/07/01/dinosauri-statistika-roku-2019/	https://dinosaurusblog.com/2019/07/01/dinosauri-statistika-roku-2019/	k4
-	-	kIx~
Dinosauří	dinosauří	k2eAgFnSc1d1
statistika	statistika	k1gFnSc1
roku	rok	k1gInSc2
2019	#num#	k4
<g/>
↑	↑	k?
Jonathan	Jonathan	k1gMnSc1
P.	P.	kA
Tennant	Tennant	k1gMnSc1
<g/>
,	,	kIx,
Alfio	Alfio	k1gMnSc1
Alessandro	Alessandra	k1gFnSc5
Chiarenza	Chiarenz	k1gMnSc4
&	&	k?
Matthew	Matthew	k1gMnSc1
Baron	baron	k1gMnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
How	How	k1gFnSc1
has	hasit	k5eAaImRp2nS
our	our	k?
knowledge	knowledg	k1gFnSc2
of	of	k?
dinosaur	dinosaur	k1gMnSc1
diversity	diversit	k1gInPc7
through	through	k1gMnSc1
geologic	geologic	k1gMnSc1
time	timat	k5eAaPmIp3nS
changed	changed	k1gMnSc1
through	through	k1gMnSc1
research	research	k1gInSc4
history	histor	k1gInPc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
PeerJ	PeerJ	k1gFnSc1
6	#num#	k4
<g/>
:	:	kIx,
<g/>
e	e	k0
<g/>
4417	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.7717/peerj.4417	https://doi.org/10.7717/peerj.4417	k4
<g/>
↑	↑	k?
https://dinomuseum.ca/2019/12/17/how-many-kind-of-dinosaurs-were-there/	https://dinomuseum.ca/2019/12/17/how-many-kind-of-dinosaurs-were-there/	k4
<g/>
↑	↑	k?
Lida	Lido	k1gNnSc2
Xing	Xinga	k1gFnPc2
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
G.	G.	kA
Lockley	Locklea	k1gFnPc1
<g/>
,	,	kIx,
Hendrik	Hendrik	k1gMnSc1
Klein	Klein	k1gMnSc1
<g/>
,	,	kIx,
W.	W.	kA
Scott	Scott	k2eAgInSc1d1
Persons	Persons	k1gInSc1
IV	IV	kA
<g/>
,	,	kIx,
Mengyuan	Mengyuan	k1gMnSc1
Wei	Wei	k1gMnSc1
<g/>
,	,	kIx,
Chen	Chen	k1gMnSc1
Li	li	k8xS
&	&	k?
Miaoyan	Miaoyan	k1gMnSc1
Wang	Wang	k1gMnSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
first	first	k1gMnSc1
record	record	k1gMnSc1
of	of	k?
Cretaceous	Cretaceous	k1gMnSc1
non-avian	non-avian	k1gMnSc1
dinosaur	dinosaur	k1gMnSc1
tracks	tracksa	k1gFnPc2
from	from	k1gMnSc1
the	the	k?
Qinghai-Tibet	Qinghai-Tibet	k1gMnSc1
Plateau	Plateaus	k1gInSc2
<g/>
,	,	kIx,
China	China	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cretaceous	Cretaceous	k1gMnSc1
Research	Research	k1gMnSc1
<g/>
,	,	kIx,
104549	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1016/j.cretres.2020.104549	https://doi.org/10.1016/j.cretres.2020.104549	k4
<g/>
↑	↑	k?
http://news.bbc.co.uk/2/hi/science/nature/7620621.stm	http://news.bbc.co.uk/2/hi/science/nature/7620621.stma	k1gFnPc2
<g/>
↑	↑	k?
Štěstí	štěstí	k1gNnSc1
a	a	k8xC
smůla	smůla	k1gFnSc1
dinosaurů	dinosaurus	k1gMnPc2
<g/>
.	.	kIx.
–	–	k?
Lidové	lidový	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
<g/>
,	,	kIx,
12	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2008	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
http://phys.org/news/2015-08-good-bad-fossil-dinosaurs.html	http://phys.org/news/2015-08-good-bad-fossil-dinosaurs.html	k1gInSc1
-	-	kIx~
Just	just	k6eAd1
how	how	k?
good	good	k1gInSc1
(	(	kIx(
<g/>
or	or	k?
bad	bad	k?
<g/>
)	)	kIx)
is	is	k?
the	the	k?
fossil	fossit	k5eAaImAgInS,k5eAaPmAgInS
record	record	k1gInSc1
of	of	k?
dinosaurs	dinosaurs	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
↑	↑	k?
Michael	Michael	k1gMnSc1
J.	J.	kA
Benton	Benton	k1gInSc1
(	(	kIx(
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Palaeodiversity	Palaeodiversit	k1gInPc7
and	and	k?
formation	formation	k1gInSc1
counts	counts	k1gInSc1
<g/>
:	:	kIx,
redundancy	redundanca	k1gFnPc1
or	or	k?
bias	bias	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Palaeontology	Palaeontolog	k1gMnPc7
<g/>
.	.	kIx.
58	#num#	k4
<g/>
(	(	kIx(
<g/>
6	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
1003	#num#	k4
<g/>
–	–	k?
<g/>
1029	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
10.111	10.111	k4
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
pala	pal	k1gInSc2
<g/>
.12191	.12191	k4
<g/>
↑	↑	k?
http://www.osel.cz/10675-vetsina-dinosauru-byla-objevena-v-poslednich-dvaceti-letech.html	http://www.osel.cz/10675-vetsina-dinosauru-byla-objevena-v-poslednich-dvaceti-letech.html	k1gMnSc1
<g/>
↑	↑	k?
SOCHA	Socha	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dinosauří	dinosauří	k2eAgFnSc1d1
statistika	statistika	k1gFnSc1
roku	rok	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSEL	osel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SOCHA	Socha	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čína	Čína	k1gFnSc1
bude	být	k5eAaImBp3nS
mít	mít	k5eAaImF
brzy	brzy	k6eAd1
300	#num#	k4
dinosauřích	dinosauří	k2eAgInPc2d1
druhů	druh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSEL	osel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Bakker	Bakker	k1gInSc1
<g/>
,	,	kIx,
R.	R.	kA
T.	T.	kA
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Dinosaur	Dinosaur	k1gMnSc1
Heresies	Heresies	k1gMnSc1
<g/>
,	,	kIx,
Zebra	zebra	k1gFnSc1
Books	Books	k1gInSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
(	(	kIx(
<g/>
str	str	kA
<g/>
.	.	kIx.
457	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Baron	baron	k1gMnSc1
<g/>
,	,	kIx,
Matthew	Matthew	k1gMnSc1
G.	G.	kA
<g/>
;	;	kIx,
noram	noram	k1gInSc1
<g/>
,	,	kIx,
Norman	Norman	k1gMnSc1
B.	B.	kA
<g/>
;	;	kIx,
Barrett	Barrett	k1gMnSc1
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
M.	M.	kA
(	(	kIx(
<g/>
22	#num#	k4
March	March	k1gInSc1
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
"	"	kIx"
<g/>
A	a	k9
new	new	k?
hypothesis	hypothesis	k1gFnPc2
of	of	k?
dinosaur	dinosaur	k1gMnSc1
relationships	relationships	k6eAd1
and	and	k?
early	earl	k1gMnPc4
dinosaur	dinosaur	k1gMnSc1
evolution	evolution	k1gInSc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
543	#num#	k4
<g/>
:	:	kIx,
501	#num#	k4
<g/>
–	–	k?
<g/>
506	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
<g/>
10.103	10.103	k4
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
nature	natur	k1gMnSc5
<g/>
21700	#num#	k4
<g/>
↑	↑	k?
SOCHA	Socha	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemětřesení	zemětřesení	k1gNnSc1
v	v	k7c6
dinosauří	dinosauří	k2eAgFnSc6d1
systematice	systematika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSEL	osel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
https://www.csmonitor.com/Science/2017/1102/Is-the-dinosaur-family-tree-becoming-a-dinosaur	https://www.csmonitor.com/Science/2017/1102/Is-the-dinosaur-family-tree-becoming-a-dinosaur	k1gMnSc1
<g/>
↑	↑	k?
https://www.earthmagazine.org/article/redefining-dinosaurs-paleontologists-are-shaking-dinosaur-family-tree-its-roots	https://www.earthmagazine.org/article/redefining-dinosaurs-paleontologists-are-shaking-dinosaur-family-tree-its-roots	k1gInSc1
<g/>
↑	↑	k?
https://dinosaurusblog.com/2009/05/12/786688-tutanchamon-doby-kridove/	https://dinosaurusblog.com/2009/05/12/786688-tutanchamon-doby-kridove/	k4
<g/>
↑	↑	k?
Luke	Luke	k1gFnPc2
A.	A.	kA
Parry	Parra	k1gFnSc2
<g/>
,	,	kIx,
Fiann	Fiann	k1gMnSc1
Smithwick	Smithwick	k1gMnSc1
<g/>
,	,	kIx,
Klara	Klara	k1gMnSc1
K.	K.	kA
Nordén	Nordén	k1gInSc1
<g/>
,	,	kIx,
Evan	Evan	k1gMnSc1
T.	T.	kA
Saitta	Saitta	k1gMnSc1
<g/>
,	,	kIx,
Jesus	Jesus	k1gMnSc1
Lozano-Fernandez	Lozano-Fernandez	k1gMnSc1
<g/>
,	,	kIx,
Alastair	Alastair	k1gMnSc1
R.	R.	kA
Tanner	Tanner	k1gMnSc1
<g/>
,	,	kIx,
Jean-Bernard	Jean-Bernard	k1gMnSc1
Caron	Caron	k1gMnSc1
<g/>
,	,	kIx,
Gregory	Gregor	k1gMnPc4
D.	D.	kA
Edgecombe	Edgecomb	k1gInSc5
<g/>
,	,	kIx,
Derek	Derek	k1gMnSc1
E.	E.	kA
G.	G.	kA
Briggs	Briggsa	k1gFnPc2
and	and	k?
Jakob	Jakob	k1gMnSc1
Vinther	Vinthra	k1gFnPc2
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soft-Bodied	Soft-Bodied	k1gInSc1
Fossils	Fossils	k1gInSc4
Are	ar	k1gInSc5
Not	nota	k1gFnPc2
Simply	Simply	k1gFnPc2
Rotten	Rotten	k2eAgInSc1d1
Carcasses	Carcasses	k1gInSc1
–	–	k?
Toward	Towarda	k1gFnPc2
a	a	k8xC
Holistic	Holistice	k1gFnPc2
Understanding	Understanding	k1gInSc4
of	of	k?
Exceptional	Exceptional	k1gFnSc2
Fossil	Fossil	k1gFnSc2
Preservation	Preservation	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bioessays	Bioessays	k1gInSc1
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
10.100	10.100	k4
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
bies	bies	k6eAd1
<g/>
.201700167	.201700167	k4
<g/>
↑	↑	k?
Evan	Evan	k1gMnSc1
Thomas	Thomas	k1gMnSc1
Saitta	Saitta	k1gMnSc1
<g/>
;	;	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Life	Lif	k1gInSc2
Inside	Insid	k1gInSc5
A	a	k9
Dinosaur	Dinosaur	k1gMnSc1
Bone	bon	k1gInSc5
<g/>
:	:	kIx,
A	a	k9
Thriving	Thriving	k1gInSc1
Microbiome	Microbiom	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
BioRxiv	BioRxiva	k1gFnPc2
400176	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1101/400176	https://doi.org/10.1101/400176	k4
<g/>
↑	↑	k?
Jasmina	Jasmin	k2eAgFnSc1d1
Wiemann	Wiemann	k1gInSc1
<g/>
,	,	kIx,
Matteo	Matteo	k6eAd1
Fabbri	Fabbr	k1gMnPc1
<g/>
,	,	kIx,
Tzu-Ruei	Tzu-Rue	k1gMnPc1
Yang	Yang	k1gMnSc1
<g/>
,	,	kIx,
Koen	Koen	k1gMnSc1
Stein	Stein	k1gMnSc1
<g/>
,	,	kIx,
P.	P.	kA
Martin	Martin	k1gInSc4
Sander	Sandra	k1gFnPc2
<g/>
,	,	kIx,
Mark	Mark	k1gMnSc1
A.	A.	kA
Norell	Norell	k1gMnSc1
&	&	k?
Derek	Derek	k1gMnSc1
E.	E.	kA
G.	G.	kA
Briggs	Briggsa	k1gFnPc2
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fossilization	Fossilization	k1gInSc1
transforms	transforms	k6eAd1
vertebrate	vertebrat	k1gInSc5
hard	hard	k1gInSc4
tissue	tissu	k1gInPc1
proteins	proteins	k6eAd1
into	into	k6eAd1
N-heterocyclic	N-heterocyclice	k1gFnPc2
polymers	polymers	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nature	Natur	k1gMnSc5
Communications	Communicationsa	k1gFnPc2
9	#num#	k4
<g/>
,	,	kIx,
Article	Article	k1gFnSc1
number	number	k1gMnSc1
<g/>
:	:	kIx,
4741	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1038/s41467-018-07013-3	https://doi.org/10.1038/s41467-018-07013-3	k4
<g/>
↑	↑	k?
Mary	Mary	k1gFnSc1
Higby	Higba	k1gFnSc2
Schweitzer	Schweitzra	k1gFnPc2
<g/>
,	,	kIx,
Elena	Elena	k1gFnSc1
R.	R.	kA
Schroeter	Schroetra	k1gFnPc2
<g/>
,	,	kIx,
Timothy	Timotha	k1gFnSc2
P.	P.	kA
Cleland	Cleland	k1gInSc1
&	&	k?
Wenxia	Wenxia	k1gFnSc1
Zheng	Zheng	k1gMnSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Paleoproteomics	Paleoproteomics	k1gInSc1
of	of	k?
Mesozoic	Mesozoic	k1gMnSc1
dinosaurs	dinosaurs	k6eAd1
and	and	k?
other	other	k1gInSc1
Mesozoic	Mesozoic	k1gMnSc1
fossils	fossils	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
PROTEOMICS	PROTEOMICS	kA
(	(	kIx(
<g/>
advance	advanec	k1gMnSc2
online	onlinout	k5eAaPmIp3nS
publication	publication	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1002/pmic.201800251	https://doi.org/10.1002/pmic.201800251	k4
<g/>
↑	↑	k?
Evan	Evan	k1gMnSc1
T.	T.	kA
Saitta	Saitta	k1gMnSc1
<g/>
;	;	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cretaceous	Cretaceous	k1gMnSc1
dinosaur	dinosaur	k1gMnSc1
bone	bon	k1gInSc5
contains	contains	k1gInSc1
recent	recent	k1gMnSc1
organic	organice	k1gFnPc2
material	material	k1gMnSc1
and	and	k?
provides	provides	k1gMnSc1
an	an	k?
environment	environment	k1gMnSc1
conducive	conducivat	k5eAaPmIp3nS
to	ten	k3xDgNnSc4
microbial	microbial	k1gMnSc1
communities	communities	k1gMnSc1
<g/>
.	.	kIx.
eLife	eLif	k1gInSc5
8	#num#	k4
<g/>
:	:	kIx,
e	e	k0
<g/>
46205	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
10.755	10.755	k4
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
eLife	eLif	k1gInSc5
<g/>
.46205	.46205	k4
<g/>
↑	↑	k?
https://www.abicko.cz/clanek/precti-si-priroda/25254/dinosauri-pohrebiste-mista-davne-zkazy.html	https://www.abicko.cz/clanek/precti-si-priroda/25254/dinosauri-pohrebiste-mista-davne-zkazy.html	k1gMnSc1
<g/>
↑	↑	k?
Ch	Ch	kA
<g/>
.	.	kIx.
A.	A.	kA
Meyer	Meyer	k1gMnSc1
<g/>
,	,	kIx,
D.	D.	kA
Marty	Marta	k1gFnSc2
<g/>
,	,	kIx,
B.	B.	kA
Thüring	Thüring	k1gInSc1
<g/>
,	,	kIx,
S.	S.	kA
Thüring	Thüring	k1gInSc1
&	&	k?
M.	M.	kA
Belvedere	Belveder	k1gInSc5
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnSc1
Late	lat	k1gInSc5
Cretaceous	Cretaceous	k1gMnSc1
dinosaur	dinosaura	k1gFnPc2
track	track	k1gMnSc1
record	record	k1gMnSc1
of	of	k?
Bolivia	Bolivia	k1gFnSc1
–	–	k?
Review	Review	k1gFnSc2
and	and	k?
perspective	perspectiv	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gMnSc1
of	of	k?
South	South	k1gMnSc1
American	American	k1gMnSc1
Earth	Earth	k1gMnSc1
Sciences	Sciences	k1gMnSc1
<g/>
.	.	kIx.
102992	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1016/j.jsames.2020.102992	https://doi.org/10.1016/j.jsames.2020.102992	k4
<g/>
↑	↑	k?
SOCHA	Socha	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objeven	objeven	k2eAgInSc4d1
první	první	k4xOgInSc4
případ	případ	k1gInSc4
zhoubné	zhoubný	k2eAgFnSc2d1
rakoviny	rakovina	k1gFnSc2
u	u	k7c2
dinosaura	dinosaurus	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSEL	osel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
https://techfocus.cz/veda-vesmir/2855-krale-dinosauru-trapil-zanet-kostni-drene-podle-vedcu-se-slavna-sue-dozila-33-let.html	https://techfocus.cz/veda-vesmir/2855-krale-dinosauru-trapil-zanet-kostni-drene-podle-vedcu-se-slavna-sue-dozila-33-let.html	k1gInSc1
<g/>
↑	↑	k?
https://dinosaurusblog.com/2009/05/15/787311-evropsti-dinosauri/	https://dinosaurusblog.com/2009/05/15/787311-evropsti-dinosauri/	k4
<g/>
↑	↑	k?
Kompletní	kompletní	k2eAgInSc1d1
záznam	záznam	k1gInSc1
přednášky	přednáška	k1gFnSc2
V.	V.	kA
Sochy	Socha	k1gMnSc2
o	o	k7c6
českých	český	k2eAgInPc6d1
objevech	objev	k1gInPc6
druhohorních	druhohorní	k2eAgMnPc2d1
plazů	plaz	k1gMnPc2
<g/>
,	,	kIx,
19	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
↑	↑	k?
https://dinosaurusblog.com/2009/11/03/819828-informacni-karta-jedineho-ceskeho-teropoda/	https://dinosaurusblog.com/2009/11/03/819828-informacni-karta-jedineho-ceskeho-teropoda/	k4
<g/>
↑	↑	k?
https://dinosaurusblog.com/2011/04/26/890720-ceskych-dinosauru-bylo-mnohem-vic/	https://dinosaurusblog.com/2011/04/26/890720-ceskych-dinosauru-bylo-mnohem-vic/	k4
<g/>
↑	↑	k?
http://dinosaurusblog.com/2016/06/16/torvosaurus-orliciensis-a-hlukyceratops-kounoviensis/	http://dinosaurusblog.com/2016/06/16/torvosaurus-orliciensis-a-hlukyceratops-kounoviensis/	k4
<g/>
↑	↑	k?
https://www.osel.cz/11194-jak-asi-vypadal-teropod-od-brna.html	https://www.osel.cz/11194-jak-asi-vypadal-teropod-od-brna.html	k1gMnSc1
<g/>
↑	↑	k?
https://technet.idnes.cz/cesko-kosti-dinosaurus-burianosaurus-augustai-f57-/veda.aspx?c=A170924_170849_veda_mla	https://technet.idnes.cz/cesko-kosti-dinosaurus-burianosaurus-augustai-f57-/veda.aspx?c=A170924_170849_veda_mla	k1gMnSc1
<g/>
↑	↑	k?
https://veda.instory.cz/1462-kuriozni-hypotezy-o-pricinach-vyhynuti-dinosauru-agresivni-housenky-toxicke-rostliny-i-smrtici-vlna-nudy.html	https://veda.instory.cz/1462-kuriozni-hypotezy-o-pricinach-vyhynuti-dinosauru-agresivni-housenky-toxicke-rostliny-i-smrtici-vlna-nudy.html	k1gMnSc1
<g/>
↑	↑	k?
D.	D.	kA
R.	R.	kA
Fraser	Fraser	k1gMnSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Why	Why	k1gFnSc1
did	did	k?
the	the	k?
dinosaurs	dinosaurs	k6eAd1
become	becom	k1gInSc5
extinct	extinctum	k1gNnPc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
Could	Could	k1gInSc1
cholecalciferol	cholecalciferol	k1gInSc4
(	(	kIx(
<g/>
vitamin	vitamin	k1gInSc4
D	D	kA
<g/>
3	#num#	k4
<g/>
)	)	kIx)
deficiency	deficienca	k1gFnSc2
be	be	k?
the	the	k?
answer	answer	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
Nutritional	Nutritional	k1gFnSc1
Science	Science	k1gFnSc1
8	#num#	k4
<g/>
:	:	kIx,
d	d	k?
<g/>
9	#num#	k4
<g/>
:	:	kIx,
1	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1017/jns.2019.7	https://doi.org/10.1017/jns.2019.7	k4
<g/>
↑	↑	k?
SOCHA	Socha	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyhubila	vyhubit	k5eAaPmAgFnS
dinosaury	dinosaurus	k1gMnPc4
pandemie	pandemie	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSEL	osel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Bakker	Bakker	k1gInSc1
<g/>
,	,	kIx,
R.	R.	kA
T.	T.	kA
(	(	kIx(
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Dinosaur	Dinosaur	k1gMnSc1
Heresies	Heresies	k1gMnSc1
<g/>
,	,	kIx,
Zebra	zebra	k1gFnSc1
Books	Books	k1gInSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
(	(	kIx(
<g/>
str	str	kA
<g/>
.	.	kIx.
425	#num#	k4
-	-	kIx~
444	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
https://dinosaurusblog.com/2015/09/29/dejiny-zkoumani-zaniku-dinosauru/	https://dinosaurusblog.com/2015/09/29/dejiny-zkoumani-zaniku-dinosauru/	k4
<g/>
↑	↑	k?
https://techfocus.cz/veda-vesmir/2628-posledni-dinosauri-vymirali-ve-svete-plnem-mrazu-a-temnoty.html	https://techfocus.cz/veda-vesmir/2628-posledni-dinosauri-vymirali-ve-svete-plnem-mrazu-a-temnoty.html	k1gMnSc1
<g/>
↑	↑	k?
Shelby	Shelba	k1gFnSc2
L.	L.	kA
Lyons	Lyons	k1gInSc1
<g/>
,	,	kIx,
Allison	Allison	k1gMnSc1
T.	T.	kA
Karp	Karp	k1gMnSc1
<g/>
,	,	kIx,
Timothy	Timotha	k1gMnSc2
J.	J.	kA
Bralower	Bralower	k1gInSc1
<g/>
,	,	kIx,
Kliti	Kliti	k1gNnPc1
Grice	Grice	k1gFnSc1
<g/>
,	,	kIx,
Bettina	Bettina	k1gFnSc1
Schaefer	Schaefer	k1gMnSc1
<g/>
,	,	kIx,
Sean	Sean	k1gMnSc1
P.	P.	kA
S.	S.	kA
Gulick	Gulicka	k1gFnPc2
<g/>
,	,	kIx,
Joanna	Joanno	k1gNnSc2
V.	V.	kA
Morgan	morgan	k1gMnSc1
<g/>
,	,	kIx,
and	and	k?
Katherine	Katherin	k1gInSc5
H.	H.	kA
Freeman	Freeman	k1gMnSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Organic	Organice	k1gFnPc2
matter	matter	k1gMnSc1
from	from	k1gMnSc1
the	the	k?
Chicxulub	Chicxulub	k1gMnSc1
crater	crater	k1gMnSc1
exacerbated	exacerbated	k1gMnSc1
the	the	k?
K-Pg	K-Pg	k1gMnSc1
impact	impact	k1gMnSc1
winter	winter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proceedings	Proceedings	k1gInSc1
of	of	k?
the	the	k?
National	National	k1gMnSc1
Academy	Academa	k1gFnSc2
of	of	k?
Sciences	Sciences	k1gInSc1
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1073/pnas.2004596117	https://doi.org/10.1073/pnas.2004596117	k4
<g/>
↑	↑	k?
https://phys.org/news/2020-09-evidence-ejected-chicxulub-crater-impact.html	https://phys.org/news/2020-09-evidence-ejected-chicxulub-crater-impact.html	k1gMnSc1
<g/>
↑	↑	k?
SOCHA	Socha	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dinosaury	dinosaurus	k1gMnPc7
vyhubila	vyhubit	k5eAaPmAgFnS
desetiletí	desetiletí	k1gNnSc4
mrazu	mráz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSEL	osel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
https://dinosaurusblog.com/2020/11/23/dinosaurum-se-darilo-do-posledni-chvile/	https://dinosaurusblog.com/2020/11/23/dinosaurum-se-darilo-do-posledni-chvile/	k4
<g/>
↑	↑	k?
Joseph	Joseph	k1gMnSc1
A.	A.	kA
Bonsor	Bonsor	k1gMnSc1
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
M.	M.	kA
Barrett	Barrett	k1gMnSc1
<g/>
,	,	kIx,
Thomas	Thomas	k1gMnSc1
J.	J.	kA
Raven	Raven	k1gInSc1
and	and	k?
Natalie	Natalie	k1gFnSc1
Cooper	Cooper	k1gMnSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dinosaur	Dinosaura	k1gFnPc2
diversification	diversification	k1gInSc1
rates	rates	k1gMnSc1
were	were	k1gFnPc2
not	nota	k1gFnPc2
in	in	k?
decline	declin	k1gInSc5
prior	prior	k1gMnSc1
to	ten	k3xDgNnSc1
the	the	k?
K-Pg	K-Pg	k1gInSc1
boundary	boundara	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Royal	Royal	k1gInSc1
Society	societa	k1gFnSc2
Open	Opena	k1gFnPc2
Science	Science	k1gFnSc2
<g/>
,	,	kIx,
7	#num#	k4
<g/>
(	(	kIx(
<g/>
11	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
201195	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1098/rsos.201195	https://doi.org/10.1098/rsos.201195	k4
<g/>
↑	↑	k?
http://phys.org/news/2016-04-dinosaurs-decline-asteroid-apocalypse.html	http://phys.org/news/2016-04-dinosaurs-decline-asteroid-apocalypse.html	k1gInSc1
-	-	kIx~
Dinosaurs	Dinosaurs	k1gInSc1
'	'	kIx"
<g/>
already	alreada	k1gFnPc1
in	in	k?
decline	declin	k1gInSc5
<g/>
'	'	kIx"
before	befor	k1gInSc5
asteroid	asteroid	k1gInSc4
apocalypse	apocalyps	k1gMnSc2
<g/>
↑	↑	k?
SOCHA	Socha	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vymírali	vymírat	k5eAaImAgMnP
dinosauři	dinosaurus	k1gMnPc1
již	již	k6eAd1
dlouho	dlouho	k6eAd1
před	před	k7c7
koncem	konec	k1gInSc7
křídy	křída	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSEL	osel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
13	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
https://dinosaurusblog.com/2015/02/03/zili-v-montane-tretihorni-dinosauri/	https://dinosaurusblog.com/2015/02/03/zili-v-montane-tretihorni-dinosauri/	k4
<g/>
↑	↑	k?
http://palaeoblog.blogspot.com/search?q=65%2C95+million	http://palaeoblog.blogspot.com/search?q=65%2C95+million	k1gInSc1
<g/>
↑	↑	k?
SOCHA	Socha	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kde	kde	k6eAd1
se	se	k3xPyFc4
vzali	vzít	k5eAaPmAgMnP
hypotetičtí	hypotetický	k2eAgMnPc1d1
superchytří	superchytřit	k5eAaPmIp3nP
dinosauři	dinosaurus	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSEL	osel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
24	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
https://www.stoplusjednicka.cz/alternativni-historie-mohla-existovat-dinosauri-civilizace	https://www.stoplusjednicka.cz/alternativni-historie-mohla-existovat-dinosauri-civilizace	k1gFnSc1
<g/>
↑	↑	k?
https://dinosaurusblog.com/2009/05/01/784379-tretihorni-neptaci-dinosauri/	https://dinosaurusblog.com/2009/05/01/784379-tretihorni-neptaci-dinosauri/	k4
<g/>
↑	↑	k?
SOCHA	Socha	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žili	žít	k5eAaImAgMnP
v	v	k7c6
Montaně	Montana	k1gFnSc6
třetihorní	třetihorní	k2eAgMnSc1d1
dinosauři	dinosaurus	k1gMnPc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSEL	osel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
https://dinosaurusblog.com/2007/03/21/506983-norujici-dinosaurus/	https://dinosaurusblog.com/2007/03/21/506983-norujici-dinosaurus/	k4
<g/>
↑	↑	k?
https://dinosaurusblog.com/2009/07/20/799230-tak-prece-norovali/	https://dinosaurusblog.com/2009/07/20/799230-tak-prece-norovali/	k4
<g/>
↑	↑	k?
https://www.idnes.cz/technet/veda/mohou-jeste-nekde-zit-dinosauri.A210206_074644_veda_mla	https://www.idnes.cz/technet/veda/mohou-jeste-nekde-zit-dinosauri.A210206_074644_veda_mla	k1gMnSc1
<g/>
↑	↑	k?
SOCHA	Socha	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
kdyby	kdyby	kYmCp3nP
dinosauři	dinosaurus	k1gMnPc1
nevyhynuli	vyhynout	k5eNaPmAgMnP
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSEL	osel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
https://dinosaurusblog.com/2009/10/14/816483-dalsi-vyroci-jurskeho-parku-a-dickensovo-prvenstvi/	https://dinosaurusblog.com/2009/10/14/816483-dalsi-vyroci-jurskeho-parku-a-dickensovo-prvenstvi/	k4
<g/>
↑	↑	k?
https://motherboard.vice.com/en_us/article/nek7ed/dinosaurs-in-space-history-fiction	https://motherboard.vice.com/en_us/article/nek7ed/dinosaurs-in-space-history-fiction	k1gInSc1
<g/>
↑	↑	k?
https://www.stoplusjednicka.cz/lekce-z-dejin-astronomie-jak-dlouho-zili-dinosauri-na-venusi	https://www.stoplusjednicka.cz/lekce-z-dejin-astronomie-jak-dlouho-zili-dinosauri-na-venuse	k1gFnSc4
<g/>
↑	↑	k?
https://veda.instory.cz/1344-temna-strana-vedy-o-praveku.html	https://veda.instory.cz/1344-temna-strana-vedy-o-praveku.html	k1gMnSc1
<g/>
↑	↑	k?
http://www.pravek.info/novinky/proc-jsou-dinosauri-duleziti/	http://www.pravek.info/novinky/proc-jsou-dinosauri-duleziti/	k?
<g/>
↑	↑	k?
SOCHA	Socha	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proč	proč	k6eAd1
podporovat	podporovat	k5eAaImF
dětský	dětský	k2eAgInSc4d1
zájem	zájem	k1gInSc4
o	o	k7c4
dinosaury	dinosaurus	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DinosaurusBlog	DinosaurusBlog	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
21	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://tetzoo.com/blog/2018/12/11/up-close-and-personal-crystal-palace-dinosaurs	http://tetzoo.com/blog/2018/12/11/up-close-and-personal-crystal-palace-dinosaurs	k1gInSc1
<g/>
↑	↑	k?
https://dinomuseum.ca/2019/08/27/the-life-appearance-of-dinosaurs-and-common-myths-surrounding-it/	https://dinomuseum.ca/2019/08/27/the-life-appearance-of-dinosaurs-and-common-myths-surrounding-it/	k4
<g/>
↑	↑	k?
https://dinosaurusblog.com/2009/06/23/794681-nejvetsi-omyly-o-dinosaurech/	https://dinosaurusblog.com/2009/06/23/794681-nejvetsi-omyly-o-dinosaurech/	k4
<g/>
↑	↑	k?
http://dinosaurusblog.com/2015/10/27/dinosauri-renesance/	http://dinosaurusblog.com/2015/10/27/dinosauri-renesance/	k4
<g/>
↑	↑	k?
http://www.denofgeek.com/us/movies/jurassic-world/246782/from-king-kong-to-jurassic-world-the-evolution-of-dinosaur-movies	http://www.denofgeek.com/us/movies/jurassic-world/246782/from-king-kong-to-jurassic-world-the-evolution-of-dinosaur-movies	k1gMnSc1
<g/>
↑	↑	k?
SOCHA	Socha	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chcete	chtít	k5eAaImIp2nP
si	se	k3xPyFc3
koupit	koupit	k5eAaPmF
tyranosaura	tyranosaur	k1gMnSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSEL	osel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
23	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
https://99percentinvisible.org/episode/welcome-to-jurassic-art/	https://99percentinvisible.org/episode/welcome-to-jurassic-art/	k4
<g/>
↑	↑	k?
http://www.poppalaeo.com/blogposts/2019/6/19/mark-witton-depicting-prehistoric-animals-as-monsters-how-why-and-so-what	http://www.poppalaeo.com/blogposts/2019/6/19/mark-witton-depicting-prehistoric-animals-as-monsters-how-why-and-so-what	k5eAaImF,k5eAaBmF,k5eAaPmF
<g/>
↑	↑	k?
https://www.nationalgeographic.com/family/reimagining-dinosaurs	https://www.nationalgeographic.com/family/reimagining-dinosaurs	k6eAd1
<g/>
↑	↑	k?
SOCHA	Socha	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
115	#num#	k4
let	léto	k1gNnPc2
dinosauří	dinosauří	k2eAgFnSc2d1
legendy	legenda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSEL	osel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
29	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
https://calgaryherald.com/news/looking-her-absolute-best-worlds-largest-dinosaur-in-drumheller-undergoing-300k-restoration	https://calgaryherald.com/news/looking-her-absolute-best-worlds-largest-dinosaur-in-drumheller-undergoing-300k-restoration	k1gInSc1
<g/>
↑	↑	k?
https://dinosaurusblog.com/2021/05/07/vzpominka-na-jaroslava-marese/	https://dinosaurusblog.com/2021/05/07/vzpominka-na-jaroslava-marese/	k4
<g/>
↑	↑	k?
https://www.databazeknih.cz/zivotopis/vladimir-socha-6559	https://www.databazeknih.cz/zivotopis/vladimir-socha-6559	k4
<g/>
↑	↑	k?
https://www.citarny.cz/knihy-lide/o-knihach-a-lidech/spisovatele-knihy/socha-vladimir-knihy-dinosauri	https://www.citarny.cz/knihy-lide/o-knihach-a-lidech/spisovatele-knihy/socha-vladimir-knihy-dinosauri	k1gNnSc6
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.zshorakhk.cz	www.zshorakhk.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
https://dinosaurusblog.com/2009/03/07/771302-prvni-vazny-pokus-o-jursky-park/	https://dinosaurusblog.com/2009/03/07/771302-prvni-vazny-pokus-o-jursky-park/	k4
<g/>
↑	↑	k?
http://www.wired.com/magazine/2011/09/ff_chickensaurus/all/1	http://www.wired.com/magazine/2011/09/ff_chickensaurus/all/1	k4
<g/>
↑	↑	k?
https://dinosaurusblog.com/2012/11/05/dalsi-podpora-pro-dinosauri-proteiny/	https://dinosaurusblog.com/2012/11/05/dalsi-podpora-pro-dinosauri-proteiny/	k4
<g/>
↑	↑	k?
https://dinosaurusblog.com/2015/02/17/jak-svet-poznal-tehotnou-tyranosaurici/	https://dinosaurusblog.com/2015/02/17/jak-svet-poznal-tehotnou-tyranosaurici/	k4
<g/>
↑	↑	k?
Darren	Darrna	k1gFnPc2
K.	K.	kA
Griffin	Griffin	k1gInSc1
<g/>
,	,	kIx,
Denis	Denisa	k1gFnPc2
M.	M.	kA
Larkin	Larkin	k1gMnSc1
&	&	k?
Rebecca	Rebecca	k1gMnSc1
E.	E.	kA
O	O	kA
<g/>
'	'	kIx"
<g/>
Connor	Connor	k1gMnSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jurassic	Jurassic	k1gMnSc1
Park	park	k1gInSc1
<g/>
:	:	kIx,
What	What	k2eAgMnSc1d1
Did	Did	k1gMnSc1
the	the	k?
Genomes	Genomes	k1gMnSc1
of	of	k?
Dinosaurs	Dinosaurs	k1gInSc1
Look	Look	k1gMnSc1
Like	Like	k1gFnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
In	In	k1gMnSc1
<g/>
:	:	kIx,
Kraus	Kraus	k1gMnSc1
R.	R.	kA
(	(	kIx(
<g/>
eds	eds	k?
<g/>
)	)	kIx)
Avian	Avian	k1gInSc1
Genomics	Genomics	k1gInSc1
in	in	k?
Ecology	Ecologa	k1gFnSc2
and	and	k?
Evolution	Evolution	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Springer	Springer	k1gMnSc1
<g/>
,	,	kIx,
Cham	Cham	k1gMnSc1
<g/>
:	:	kIx,
331	#num#	k4
<g/>
-	-	kIx~
<g/>
348	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1007/978-3-030-16477-5_11	https://doi.org/10.1007/978-3-030-16477-5_11	k4
<g/>
↑	↑	k?
http://www.osel.cz/10749-jak-vypadal-dinosauri-genom.html	http://www.osel.cz/10749-jak-vypadal-dinosauri-genom.html	k1gMnSc1
<g/>
↑	↑	k?
Alleon	Alleon	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
<g/>
;	;	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pushing	Pushing	k1gInSc1
Raman	Raman	k1gInSc4
spectroscopy	spectroscopa	k1gFnSc2
over	over	k1gInSc1
the	the	k?
edge	edge	k1gInSc1
<g/>
:	:	kIx,
purported	purported	k1gInSc1
signatures	signatures	k1gInSc1
of	of	k?
organic	organice	k1gFnPc2
molecules	molecules	k1gMnSc1
in	in	k?
fossil	fossit	k5eAaPmAgMnS,k5eAaImAgMnS
animals	animals	k6eAd1
are	ar	k1gInSc5
instrumental	instrumental	k1gMnSc1
artefacts	artefactsit	k5eAaPmRp2nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
BioEssays	BioEssays	k1gInSc1
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1002/bies.202000295	https://doi.org/10.1002/bies.202000295	k4
<g/>
↑	↑	k?
http://www.osel.cz/10891-co-je-noveho-u-dinosaurich-mekkych-tkani.html	http://www.osel.cz/10891-co-je-noveho-u-dinosaurich-mekkych-tkani.html	k1gMnSc1
<g/>
↑	↑	k?
Alida	Alida	k1gMnSc1
M.	M.	kA
Bailleul	Bailleul	k1gInSc1
<g/>
,	,	kIx,
Wenxia	Wenxia	k1gFnSc1
Zheng	Zheng	k1gMnSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
R.	R.	kA
Horner	Horner	k1gMnSc1
<g/>
,	,	kIx,
Brian	Brian	k1gMnSc1
K.	K.	kA
Hall	Hall	k1gMnSc1
<g/>
,	,	kIx,
Casey	Casea	k1gMnSc2
M.	M.	kA
Holliday	Hollidaa	k1gMnSc2
&	&	k?
Mary	Mary	k1gFnSc2
H.	H.	kA
Schweitzer	Schweitzer	k1gMnSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evidence	evidence	k1gFnSc1
of	of	k?
proteins	proteins	k1gInSc1
<g/>
,	,	kIx,
chromosomes	chromosomes	k1gInSc1
and	and	k?
chemical	chemicat	k5eAaPmAgInS
markers	markers	k1gInSc1
of	of	k?
DNA	DNA	kA
in	in	k?
exceptionally	exceptionalla	k1gFnSc2
preserved	preserved	k1gMnSc1
dinosaur	dinosaur	k1gMnSc1
cartilage	cartilagat	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
National	National	k1gFnSc1
Science	Science	k1gFnSc1
Review	Review	k1gFnSc1
<g/>
,	,	kIx,
nwz	nwz	k?
<g/>
206	#num#	k4
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1093/nsr/nwz206	https://doi.org/10.1093/nsr/nwz206	k4
<g/>
↑	↑	k?
https://dvojka.rozhlas.cz/meteor-o-slunecnim-kouzle-zralocim-oku-a-zdrogovane-libusi-8294754	https://dvojka.rozhlas.cz/meteor-o-slunecnim-kouzle-zralocim-oku-a-zdrogovane-libusi-8294754	k4
<g/>
↑	↑	k?
http://www.osel.cz/8310-mohli-dinosauri-vytvorit-civilizaci.html	http://www.osel.cz/8310-mohli-dinosauri-vytvorit-civilizaci.html	k1gMnSc1
<g/>
↑	↑	k?
http://www.osel.cz/7838-kde-se-vzali-hypoteticti-superchytri-dinosauri.html	http://www.osel.cz/7838-kde-se-vzali-hypoteticti-superchytri-dinosauri.html	k1gInSc1
<g/>
↑	↑	k?
https://dinosaurusblog.com/2021/02/01/usvit-dinosauroidu/	https://dinosaurusblog.com/2021/02/01/usvit-dinosauroidu/	k4
<g/>
↑	↑	k?
https://veda.instory.cz/1418-hypotezy-o-vysoke-inteligenci-nekterych-dinosauru-i-mozne-civilizaci-vytvorene-dinosaury.html	https://veda.instory.cz/1418-hypotezy-o-vysoke-inteligenci-nekterych-dinosauru-i-mozne-civilizaci-vytvorene-dinosaury.html	k1gMnSc1
<g/>
↑	↑	k?
http://tetzoo.com/blog/2018/8/16/could-we-domesticate-non-bird-dinosaurs	http://tetzoo.com/blog/2018/8/16/could-we-domesticate-non-bird-dinosaurs	k1gInSc1
<g/>
↑	↑	k?
https://www.sciencefocus.com/nature/what-if-the-dinosaurs-had-survived/	https://www.sciencefocus.com/nature/what-if-the-dinosaurs-had-survived/	k?
<g/>
↑	↑	k?
https://www.audubon.org/news/ask-kenn-kaufman-which-birds-are-most-their-dinosaur-ancestors	https://www.audubon.org/news/ask-kenn-kaufman-which-birds-are-most-their-dinosaur-ancestors	k6eAd1
<g/>
↑	↑	k?
SOCHA	Socha	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žijí	žít	k5eAaImIp3nP
dinosauři	dinosaurus	k1gMnPc1
všude	všude	k6eAd1
kolem	kolem	k7c2
nás	my	k3xPp1nPc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSEL	osel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
SOCHA	Socha	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dinosauří	dinosauří	k2eAgInSc4d1
byznys	byznys	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OSEL	osel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
https://www.idnes.cz/technet/veda/nejdrazsi-fosilie-tyranosaurus-t-rex-stan.A201012_151325_veda_mla	https://www.idnes.cz/technet/veda/nejdrazsi-fosilie-tyranosaurus-t-rex-stan.A201012_151325_veda_mla	k6eAd1
</s>
<s>
George	George	k6eAd1
Olshevsky	Olshevsko	k1gNnPc7
o	o	k7c6
dosud	dosud	k6eAd1
neobjevených	objevený	k2eNgFnPc6d1
dinosaurech	dinosaurus	k1gMnPc6
</s>
<s>
Wild	Wild	k1gInSc1
Prehistory	Prehistor	k1gInPc1
<g/>
:	:	kIx,
O	o	k7c4
zbarvení	zbarvení	k1gNnSc4
dinosaurů	dinosaurus	k1gMnPc2
</s>
<s>
The	The	k?
Biggest	Biggest	k1gInSc1
Carnivore	Carnivor	k1gInSc5
<g/>
:	:	kIx,
Dinosaur	Dinosaur	k1gMnSc1
History	Histor	k1gMnPc7
Rewritten	Rewritten	k2eAgInSc1d1
</s>
<s>
Rozměry	rozměra	k1gFnPc1
teropodů	teropod	k1gInPc2
<g/>
,	,	kIx,
k	k	k7c3
roku	rok	k1gInSc3
2003	#num#	k4
<g/>
,	,	kIx,
Mickey	Mickea	k1gFnPc1
Mortimer	Mortimer	k1gMnSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Bakker	Bakker	k1gMnSc1
<g/>
,	,	kIx,
R.	R.	kA
T.	T.	kA
<g/>
:	:	kIx,
Červený	Červený	k1gMnSc1
raptor	raptor	k1gMnSc1
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
Dobrovský	Dobrovský	k1gMnSc1
BETA	beta	k1gNnPc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2000	#num#	k4
</s>
<s>
Mareš	Mareš	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
<g/>
:	:	kIx,
Gladiátoři	gladiátor	k1gMnPc1
druhohor	druhohory	k1gFnPc2
<g/>
,	,	kIx,
Filip	Filip	k1gMnSc1
Trend	trend	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2001	#num#	k4
</s>
<s>
Mareš	Mareš	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
<g/>
:	:	kIx,
Záhada	záhada	k1gFnSc1
dinosaurů	dinosaurus	k1gMnPc2
<g/>
,	,	kIx,
Svoboda-Libertas	Svoboda-Libertasa	k1gFnPc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1993	#num#	k4
</s>
<s>
Socha	Socha	k1gMnSc1
<g/>
,	,	kIx,
V.	V.	kA
<g/>
:	:	kIx,
Encyklopedie	encyklopedie	k1gFnSc1
dinosaurů	dinosaurus	k1gMnPc2
ve	v	k7c6
světle	světlo	k1gNnSc6
nejnovějších	nový	k2eAgInPc2d3
objevů	objev	k1gInPc2
<g/>
,	,	kIx,
Libri	Libri	k1gNnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2010	#num#	k4
</s>
<s>
Socha	Socha	k1gMnSc1
<g/>
,	,	kIx,
V.	V.	kA
<g/>
:	:	kIx,
Úžasný	úžasný	k2eAgInSc1d1
svět	svět	k1gInSc1
dinosaurů	dinosaurus	k1gMnPc2
<g/>
,	,	kIx,
Triton	triton	k1gMnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2009	#num#	k4
</s>
<s>
Socha	Socha	k1gMnSc1
<g/>
,	,	kIx,
V.	V.	kA
<g/>
:	:	kIx,
Neznámí	známý	k2eNgMnPc1d1
dinosauři	dinosaurus	k1gMnPc1
<g/>
,	,	kIx,
Mladá	mladý	k2eAgFnSc1d1
fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2015	#num#	k4
</s>
<s>
Socha	Socha	k1gMnSc1
<g/>
,	,	kIx,
V.	V.	kA
<g/>
:	:	kIx,
Dinosauři	dinosaurus	k1gMnPc1
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
,	,	kIx,
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2017	#num#	k4
</s>
<s>
Zahraniční	zahraniční	k2eAgInPc1d1
tituly	titul	k1gInPc1
</s>
<s>
Holtz	Holtz	k1gMnSc1
<g/>
,	,	kIx,
T.	T.	kA
R.	R.	kA
<g/>
:	:	kIx,
Dinosaurs	Dinosaurs	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Most	most	k1gInSc4
Complete	Comple	k1gNnSc2
<g/>
,	,	kIx,
Up-to-Date	Up-to-Dat	k1gInSc5
Encyclopedia	Encyclopedium	k1gNnPc1
for	forum	k1gNnPc2
Dinosaur	Dinosaura	k1gFnPc2
Lovers	Loversa	k1gFnPc2
of	of	k?
All	All	k1gFnSc2
Ages	Agesa	k1gFnPc2
<g/>
,	,	kIx,
Random	Random	k1gInSc4
House	house	k1gNnSc4
<g/>
,	,	kIx,
2007	#num#	k4
</s>
<s>
Long	Long	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
A.	A.	kA
<g/>
:	:	kIx,
Feathered	Feathered	k1gInSc1
Dinosaurs	Dinosaurs	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Origin	Origin	k1gMnSc1
of	of	k?
Birds	Birds	k1gInSc1
<g/>
,	,	kIx,
Oxford	Oxford	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2008	#num#	k4
</s>
<s>
Paul	Paul	k1gMnSc1
<g/>
,	,	kIx,
G.	G.	kA
S.	S.	kA
<g/>
:	:	kIx,
Predatory	Predator	k1gInPc1
Dinosaurs	Dinosaursa	k1gFnPc2
of	of	k?
the	the	k?
World	World	k1gMnSc1
<g/>
,	,	kIx,
Simon	Simon	k1gMnSc1
&	&	k?
Schuster	Schuster	k1gMnSc1
<g/>
,	,	kIx,
1988	#num#	k4
</s>
<s>
Paul	Paul	k1gMnSc1
<g/>
,	,	kIx,
G.	G.	kA
S.	S.	kA
<g/>
:	:	kIx,
The	The	k1gFnPc2
Princeton	Princeton	k1gInSc1
Field	Field	k1gMnSc1
Guide	Guid	k1gInSc5
to	ten	k3xDgNnSc1
Dinosaurs	Dinosaurs	k1gInSc1
<g/>
,	,	kIx,
Princeton	Princeton	k1gInSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
2010	#num#	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Opeření	opeřený	k2eAgMnPc1d1
dinosauři	dinosaurus	k1gMnPc1
</s>
<s>
Seznam	seznam	k1gInSc1
dinosaurů	dinosaurus	k1gMnPc2
</s>
<s>
Velikost	velikost	k1gFnSc1
dinosaurů	dinosaurus	k1gMnPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
dinosauři	dinosaurus	k1gMnPc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
dinosaurus	dinosaurus	k1gMnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Taxon	taxon	k1gInSc1
Dinosauria	Dinosaurium	k1gNnSc2
ve	v	k7c6
Wikidruzích	Wikidruh	k1gInPc6
</s>
<s>
České	český	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
Web	web	k1gInSc1
DinosaurusBlog	DinosaurusBloga	k1gFnPc2
</s>
<s>
Web	web	k1gInSc1
Pravěk	pravěk	k1gInSc1
<g/>
.	.	kIx.
<g/>
info	info	k6eAd1
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
DinoPark	DinoPark	k1gInSc1
Czech	Czecha	k1gFnPc2
Republic	Republice	k1gFnPc2
</s>
<s>
Česká	český	k2eAgNnPc1d1
videa	video	k1gNnPc1
</s>
<s>
Přednáška	přednáška	k1gFnSc1
o	o	k7c4
vyhynutí	vyhynutí	k1gNnSc4
dinosaurů	dinosaurus	k1gMnPc2
<g/>
;	;	kIx,
Mgr.	Mgr.	kA
Vladimír	Vladimír	k1gMnSc1
Socha	Socha	k1gMnSc1
<g/>
,	,	kIx,
Hvězdárna	hvězdárna	k1gFnSc1
a	a	k8xC
planetárium	planetárium	k1gNnSc1
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
27	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
</s>
<s>
Články	článek	k1gInPc1
</s>
<s>
Článek	článek	k1gInSc1
o	o	k7c6
předvědeckých	předvědecký	k2eAgInPc6d1
objevech	objev	k1gInPc6
dinosaurů	dinosaurus	k1gMnPc2
na	na	k7c6
webu	web	k1gInSc6
Osel	osel	k1gMnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Článek	článek	k1gInSc1
o	o	k7c4
dinosauří	dinosauří	k2eAgFnSc4d1
inteligenci	inteligence	k1gFnSc4
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Článek	článek	k1gInSc1
o	o	k7c6
rychlosti	rychlost	k1gFnSc6
dinosauřího	dinosauří	k2eAgInSc2d1
pohybu	pohyb	k1gInSc2
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Článek	článek	k1gInSc1
o	o	k7c6
„	„	k?
<g/>
klonování	klonování	k1gNnSc6
<g/>
“	“	k?
dinosaurů	dinosaurus	k1gMnPc2
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Článek	článek	k1gInSc1
o	o	k7c6
obecných	obecný	k2eAgInPc6d1
dinosauřích	dinosauří	k2eAgInPc6d1
rekordech	rekord	k1gInPc6
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Článek	článek	k1gInSc1
Nejčastěji	často	k6eAd3
kladené	kladený	k2eAgInPc4d1
dotazy	dotaz	k1gInPc4
o	o	k7c6
dinosaurech	dinosaurus	k1gMnPc6
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Článek	článek	k1gInSc1
Vše	všechen	k3xTgNnSc4
o	o	k7c6
dinosaurech	dinosaurus	k1gMnPc6
na	na	k7c6
webu	web	k1gInSc6
DinosaurusBlog	DinosaurusBloga	k1gFnPc2
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Článek	článek	k1gInSc1
Fakta	faktum	k1gNnSc2
o	o	k7c6
dinosaurech	dinosaurus	k1gMnPc6
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Zahraniční	zahraniční	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
</s>
<s>
V	v	k7c6
angličtině	angličtina	k1gFnSc6
</s>
<s>
Mikko	Mikko	k1gNnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Phylogeny	Phylogen	k1gInPc7
Archive	archiv	k1gInSc5
<g/>
:	:	kIx,
Dinosauria	Dinosaurium	k1gNnSc2
–	–	k?
fylogeneze	fylogeneze	k1gFnSc2
dinosaurů	dinosaurus	k1gMnPc2
</s>
<s>
DinoData	DinoData	k1gFnSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
–	–	k?
databáze	databáze	k1gFnSc2
dinosaurů	dinosaurus	k1gMnPc2
</s>
<s>
Dinosaur	Dinosaur	k1gMnSc1
Mailing	Mailing	k1gInSc4
List	list	k1gInSc1
</s>
<s>
Dinosauria	Dinosaurium	k1gNnPc1
On-Line	On-Lin	k1gMnSc5
</s>
<s>
Dinosaurs	Dinosaurs	k1gInSc1
Dinosaur	Dinosaura	k1gFnPc2
Facts	Factsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Dinosaur	Dinosaur	k1gMnSc1
World	World	k1gMnSc1
–	–	k?
plakáty	plakát	k1gInPc1
a	a	k8xC
ilustrace	ilustrace	k1gFnPc1
</s>
<s>
Web	web	k1gInSc1
Thescelosaurus	Thescelosaurus	k1gInSc1
<g/>
!	!	kIx.
</s>
<s>
Numerical	Numericat	k5eAaPmAgMnS
study	stud	k1gInPc1
of	of	k?
dinosaur	dinosaura	k1gFnPc2
evolution	evolution	k1gInSc4
–	–	k?
Supertree	Supertre	k1gInSc2
<g/>
,	,	kIx,
zatím	zatím	k6eAd1
nejnovější	nový	k2eAgFnSc7d3
ALi	ALi	k1gFnSc7
fylogenetický	fylogenetický	k2eAgInSc1d1
strom	strom	k1gInSc1
z	z	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
</s>
<s>
V	v	k7c6
němčině	němčina	k1gFnSc6
</s>
<s>
Dinosaurier-interesse	Dinosaurier-interesse	k6eAd1
</s>
<s>
Dinosaurier	Dinosaurier	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
</s>
<s>
Dinosaurier-Spuren	Dinosaurier-Spurna	k1gFnPc2
</s>
<s>
Dinos	Dinos	k1gInSc1
in	in	k?
den	den	k1gInSc4
Medien	Medien	k2eAgInSc4d1
</s>
<s>
dinosaurier-web	dinosaurier-wba	k1gFnPc2
<g/>
.	.	kIx.
<g/>
de	de	k?
</s>
<s>
Jurassic	Jurassic	k1gMnSc1
Harz	Harz	k1gMnSc1
</s>
<s>
Dinosaurierpark	Dinosaurierpark	k1gInSc1
Münchehagen	Münchehagen	k1gInSc1
</s>
<s>
Sauriermuseum	Sauriermuseum	k1gNnSc1
Aathal	Aathal	k1gMnSc1
bei	bei	k?
Zürich	Zürich	k1gMnSc1
</s>
<s>
V	v	k7c6
polštině	polština	k1gFnSc6
</s>
<s>
Dinozaury	Dinozaura	k1gFnPc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
!	!	kIx.
</s>
<s desamb="1">
–	–	k?
největší	veliký	k2eAgFnSc1d3
polská	polský	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
o	o	k7c6
dinosaurech	dinosaurus	k1gMnPc6
</s>
<s>
Polskie	Polskie	k1gFnSc1
Forum	forum	k1gNnSc1
Sympatyków	Sympatyków	k1gFnSc2
Paleontologii	paleontologie	k1gFnSc3
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Dinosauři	dinosaurus	k1gMnPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4012362-5	4012362-5	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
601	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85038094	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85038094	#num#	k4
</s>
