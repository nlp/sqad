<s>
Dinosauři	dinosaurus	k1gMnPc1
(	(	kIx(
<g/>
z	z	k7c2
řec.	řec.	k?
δ	δ	k?
–	–	k?
deinos	deinos	k1gInSc1
+	+	kIx~
σ	σ	k?
–	–	k?
sauros	saurosa	k1gFnPc2
=	=	kIx~
„	„	k?
<g/>
strašný	strašný	k2eAgMnSc1d1
ještěr	ještěr	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
překládáno	překládán	k2eAgNnSc1d1
„	„	k?
<g/>
hrozný	hrozný	k2eAgMnSc1d1
plaz	plaz	k1gMnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
jsou	být	k5eAaImIp3nP
rozmanitou	rozmanitý	k2eAgFnSc7d1
skupinou	skupina	k1gFnSc7
obratlovců	obratlovec	k1gMnPc2
řazenou	řazený	k2eAgFnSc7d1
mezi	mezi	k7c4
plazy	plaz	k1gMnPc4
(	(	kIx(
<g/>
Reptilia	Reptilia	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
dominovala	dominovat	k5eAaImAgFnS
fauně	fauna	k1gFnSc3
na	na	k7c6
pevninách	pevnina	k1gFnPc6
této	tento	k3xDgFnSc2
planety	planeta	k1gFnSc2
přes	přes	k7c4
134	#num#	k4
milionů	milion	k4xCgInPc2
let	léto	k1gNnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
období	období	k1gNnSc6
druhohor	druhohory	k1gFnPc2
(	(	kIx(
<g/>
zejména	zejména	k9
v	v	k7c6
jurské	jurský	k2eAgFnSc6d1
a	a	k8xC
křídové	křídový	k2eAgFnSc6d1
periodě	perioda	k1gFnSc6
<g/>
,	,	kIx,
asi	asi	k9
před	před	k7c7
201	#num#	k4
až	až	k8xS
66	#num#	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>