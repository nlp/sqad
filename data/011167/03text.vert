<p>
<s>
Mart	Marta	k1gFnPc2	Marta
Siimann	Siimanna	k1gFnPc2	Siimanna
(	(	kIx(	(
<g/>
*	*	kIx~	*
21	[number]	k4	21
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
estonský	estonský	k2eAgMnSc1d1	estonský
politik	politik	k1gMnSc1	politik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1997-1999	[number]	k4	1997-1999
byl	být	k5eAaImAgMnS	být
premiérem	premiér	k1gMnSc7	premiér
Estonska	Estonsko	k1gNnSc2	Estonsko
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
je	být	k5eAaImIp3nS	být
předsedou	předseda	k1gMnSc7	předseda
Estonského	estonský	k2eAgInSc2d1	estonský
olympijského	olympijský	k2eAgInSc2d1	olympijský
výboru	výbor	k1gInSc2	výbor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1989-1992	[number]	k4	1989-1992
byl	být	k5eAaImAgMnS	být
ředitel	ředitel	k1gMnSc1	ředitel
estonské	estonský	k2eAgFnSc2d1	Estonská
státní	státní	k2eAgFnSc2d1	státní
televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
</s>
<s>
Poslancem	poslanec	k1gMnSc7	poslanec
estonského	estonský	k2eAgInSc2d1	estonský
parlamentu	parlament	k1gInSc2	parlament
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1995	[number]	k4	1995
<g/>
–	–	k?	–
<g/>
1997	[number]	k4	1997
a	a	k8xC	a
1999	[number]	k4	1999
<g/>
-	-	kIx~	-
<g/>
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
představitelem	představitel	k1gMnSc7	představitel
liberálně	liberálně	k6eAd1	liberálně
orientované	orientovaný	k2eAgFnSc2d1	orientovaná
Estonské	estonský	k2eAgFnSc2d1	Estonská
koaliční	koaliční	k2eAgFnSc2d1	koaliční
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
Eesti	Eesti	k1gNnSc1	Eesti
Koonderakond	Koonderakonda	k1gFnPc2	Koonderakonda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mart	Marta	k1gFnPc2	Marta
Siimann	Siimann	k1gNnSc4	Siimann
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
estonského	estonský	k2eAgInSc2d1	estonský
olympijského	olympijský	k2eAgInSc2d1	olympijský
výboru	výbor	k1gInSc2	výbor
</s>
</p>
