<s>
Mogadišo	Mogadišo	k1gNnSc1	Mogadišo
(	(	kIx(	(
<g/>
somálsky	somálsky	k6eAd1	somálsky
Muqdisho	Muqdis	k1gMnSc2	Muqdis
<g/>
,	,	kIx,	,
hovorově	hovorově	k6eAd1	hovorově
často	často	k6eAd1	často
Xamar	Xamara	k1gFnPc2	Xamara
(	(	kIx(	(
<g/>
vyslov	vyslovit	k5eAaPmRp2nS	vyslovit
hamar	hamar	k1gInSc1	hamar
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
arabsky	arabsky	k6eAd1	arabsky
م	م	k?	م
<g/>
,	,	kIx,	,
Maqadíšú	Maqadíšú	k1gFnSc1	Maqadíšú
<g/>
;	;	kIx,	;
italsky	italsky	k6eAd1	italsky
Mogadiscio	Mogadiscio	k6eAd1	Mogadiscio
<g/>
;	;	kIx,	;
anglicky	anglicky	k6eAd1	anglicky
Mogadishu	Mogadish	k1gInSc3	Mogadish
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
a	a	k8xC	a
největší	veliký	k2eAgInSc1d3	veliký
přístav	přístav	k1gInSc1	přístav
Somálské	somálský	k2eAgFnSc2d1	Somálská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
