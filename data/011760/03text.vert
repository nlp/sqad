<p>
<s>
Mogadišo	Mogadišo	k1gNnSc1	Mogadišo
(	(	kIx(	(
<g/>
somálsky	somálsky	k6eAd1	somálsky
Muqdisho	Muqdis	k1gMnSc2	Muqdis
<g/>
,	,	kIx,	,
hovorově	hovorově	k6eAd1	hovorově
často	často	k6eAd1	často
Xamar	Xamara	k1gFnPc2	Xamara
(	(	kIx(	(
<g/>
vyslov	vyslovit	k5eAaPmRp2nS	vyslovit
hamar	hamar	k1gInSc1	hamar
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
arabsky	arabsky	k6eAd1	arabsky
م	م	k?	م
<g/>
,	,	kIx,	,
Maqadíšú	Maqadíšú	k1gFnSc1	Maqadíšú
<g/>
;	;	kIx,	;
italsky	italsky	k6eAd1	italsky
Mogadiscio	Mogadiscio	k6eAd1	Mogadiscio
<g/>
;	;	kIx,	;
anglicky	anglicky	k6eAd1	anglicky
Mogadishu	Mogadish	k1gInSc3	Mogadish
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgNnSc1d3	veliký
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
a	a	k8xC	a
největší	veliký	k2eAgInSc1d3	veliký
přístav	přístav	k1gInSc1	přístav
Somálské	somálský	k2eAgFnSc2d1	Somálská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Mogadišo	Mogadišo	k1gNnSc1	Mogadišo
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
regionu	region	k1gInSc6	region
Banadir	Banadir	k1gInSc1	Banadir
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Indického	indický	k2eAgInSc2d1	indický
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
v	v	k7c6	v
zeměpisné	zeměpisný	k2eAgFnSc6d1	zeměpisná
poloze	poloha	k1gFnSc6	poloha
2	[number]	k4	2
<g/>
°	°	k?	°
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
́	́	k?	́
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
a	a	k8xC	a
45	[number]	k4	45
<g/>
°	°	k?	°
<g/>
21	[number]	k4	21
<g/>
́	́	k?	́
<g/>
východní	východní	k2eAgFnSc2d1	východní
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
je	být	k5eAaImIp3nS	být
17	[number]	k4	17
m.	m.	k?	m.
Nejnižším	nízký	k2eAgInSc7d3	nejnižší
bodem	bod	k1gInSc7	bod
je	být	k5eAaImIp3nS	být
hladina	hladina	k1gFnSc1	hladina
Indického	indický	k2eAgInSc2d1	indický
oceánu	oceán	k1gInSc2	oceán
a	a	k8xC	a
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
<g/>
,	,	kIx,	,
Villa	Villa	k1gFnSc1	Villa
Somalia	Somalia	k1gFnSc1	Somalia
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
120	[number]	k4	120
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
rok	rok	k1gInSc4	rok
téměř	téměř	k6eAd1	téměř
nemění	měnit	k5eNaImIp3nS	měnit
<g/>
,	,	kIx,	,
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
25	[number]	k4	25
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
o	o	k7c4	o
zhruba	zhruba	k6eAd1	zhruba
2	[number]	k4	2
°	°	k?	°
<g/>
C	C	kA	C
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
dešťových	dešťový	k2eAgFnPc2d1	dešťová
srážek	srážka	k1gFnPc2	srážka
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
téměř	téměř	k6eAd1	téměř
neprší	pršet	k5eNaImIp3nS	pršet
<g/>
,	,	kIx,	,
nejvíce	hodně	k6eAd3	hodně
naprší	napršet	k5eAaPmIp3nS	napršet
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
kolem	kolem	k7c2	kolem
80	[number]	k4	80
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
sleduje	sledovat	k5eAaImIp3nS	sledovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1953	[number]	k4	1953
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ve	v	k7c6	v
městě	město	k1gNnSc6	město
žilo	žít	k5eAaImAgNnS	žít
63	[number]	k4	63
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgNnSc1d1	poslední
sčítání	sčítání	k1gNnSc1	sčítání
bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ve	v	k7c6	v
městě	město	k1gNnSc6	město
žilo	žít	k5eAaImAgNnS	žít
982	[number]	k4	982
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
napjaté	napjatý	k2eAgFnSc3d1	napjatá
situaci	situace	k1gFnSc3	situace
<g/>
,	,	kIx,	,
způsobené	způsobený	k2eAgInPc1d1	způsobený
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
občanskou	občanský	k2eAgFnSc7d1	občanská
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
určit	určit	k5eAaPmF	určit
přesný	přesný	k2eAgInSc4d1	přesný
počet	počet	k1gInSc4	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgInPc1d1	současný
odhady	odhad	k1gInPc1	odhad
kolísají	kolísat	k5eAaImIp3nP	kolísat
mezi	mezi	k7c7	mezi
1	[number]	k4	1
500	[number]	k4	500
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
000	[number]	k4	000
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
30	[number]	k4	30
km	km	kA	km
severně	severně	k6eAd1	severně
od	od	k7c2	od
města	město	k1gNnSc2	město
teče	téct	k5eAaImIp3nS	téct
největší	veliký	k2eAgFnSc1d3	veliký
řeka	řeka	k1gFnSc1	řeka
Somálska	Somálsko	k1gNnSc2	Somálsko
Šebeli	Šebel	k1gInSc6	Šebel
(	(	kIx(	(
<g/>
Webi	Webi	k1gNnSc1	Webi
Shebeelle	Shebeelle	k1gFnSc2	Shebeelle
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
sídlily	sídlit	k5eAaImAgFnP	sídlit
nejvyšší	vysoký	k2eAgFnPc1d3	nejvyšší
státní	státní	k2eAgFnPc1d1	státní
orgány	orgány	k1gFnPc1	orgány
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
jednokomorové	jednokomorový	k2eAgNnSc1d1	jednokomorové
národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
(	(	kIx(	(
<g/>
Galaha	Galaha	k1gFnSc1	Galaha
Shabciga	Shabciga	k1gFnSc1	Shabciga
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
revoluce	revoluce	k1gFnSc2	revoluce
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
je	být	k5eAaImIp3nS	být
však	však	k9	však
činnost	činnost	k1gFnSc1	činnost
vlády	vláda	k1gFnSc2	vláda
ochromena	ochromen	k2eAgFnSc1d1	ochromena
a	a	k8xC	a
země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
chaosu	chaos	k1gInSc6	chaos
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
také	také	k9	také
sídlí	sídlet	k5eAaImIp3nS	sídlet
Východoafrické	východoafrický	k2eAgNnSc1d1	Východoafrické
islámské	islámský	k2eAgNnSc1d1	islámské
shromáždění	shromáždění	k1gNnSc1	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
části	část	k1gFnPc4	část
patří	patřit	k5eAaImIp3nS	patřit
Hammanwein	Hammanwein	k2eAgInSc1d1	Hammanwein
(	(	kIx(	(
<g/>
staré	starý	k2eAgNnSc1d1	staré
město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tržiště	tržiště	k1gNnSc4	tržiště
Bakara	Bakara	k1gFnSc1	Bakara
a	a	k8xC	a
dřívější	dřívější	k2eAgNnSc1d1	dřívější
známé	známý	k2eAgNnSc1d1	známé
letovisko	letovisko	k1gNnSc1	letovisko
Gezira	Gezira	k1gMnSc1	Gezira
Beach	Beach	k1gMnSc1	Beach
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Středověk	středověk	k1gInSc1	středověk
===	===	k?	===
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
800	[number]	k4	800
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k9	už
roku	rok	k1gInSc2	rok
900	[number]	k4	900
ho	on	k3xPp3gInSc4	on
kolonizovali	kolonizovat	k5eAaBmAgMnP	kolonizovat
muslimové	muslim	k1gMnPc1	muslim
z	z	k7c2	z
Arabského	arabský	k2eAgInSc2d1	arabský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
arabských	arabský	k2eAgMnPc2d1	arabský
cestovatelů	cestovatel	k1gMnPc2	cestovatel
kteří	který	k3yQgMnPc1	který
navštívili	navštívit	k5eAaPmAgMnP	navštívit
Somálsko	Somálsko	k1gNnSc4	Somálsko
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
letech	let	k1gInPc6	let
890	[number]	k4	890
<g/>
–	–	k?	–
<g/>
93	[number]	k4	93
Íbn	Íbn	k1gMnSc2	Íbn
al-Jákub	al-Jákub	k1gInSc4	al-Jákub
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
napsal	napsat	k5eAaPmAgInS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Mogadišu	Mogadišo	k1gNnSc6	Mogadišo
byl	být	k5eAaImAgInS	být
silně	silně	k6eAd1	silně
rozvinutý	rozvinutý	k2eAgInSc1d1	rozvinutý
obchod	obchod	k1gInSc1	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
velmi	velmi	k6eAd1	velmi
prosperovalo	prosperovat	k5eAaImAgNnS	prosperovat
z	z	k7c2	z
vnitrozemského	vnitrozemský	k2eAgInSc2d1	vnitrozemský
obchodu	obchod	k1gInSc2	obchod
se	s	k7c7	s
svahilskými	svahilský	k2eAgInPc7d1	svahilský
městskými	městský	k2eAgInPc7d1	městský
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
námořního	námořní	k2eAgInSc2d1	námořní
obchodu	obchod	k1gInSc2	obchod
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
arabským	arabský	k2eAgMnSc7d1	arabský
návštěvníkem	návštěvník	k1gMnSc7	návštěvník
Mogadišo	Mogadišo	k1gNnSc1	Mogadišo
byl	být	k5eAaImAgInS	být
Ibn	Ibn	k1gMnSc4	Ibn
Battuta	Battut	k1gMnSc4	Battut
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
město	město	k1gNnSc4	město
navštívil	navštívit	k5eAaPmAgMnS	navštívit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1332	[number]	k4	1332
<g/>
-	-	kIx~	-
<g/>
33	[number]	k4	33
<g/>
,	,	kIx,	,
a	a	k8xC	a
považoval	považovat	k5eAaImAgMnS	považovat
ho	on	k3xPp3gNnSc4	on
za	za	k7c4	za
nejvýznamnější	významný	k2eAgInSc4d3	nejvýznamnější
přístav	přístav	k1gInSc4	přístav
ve	v	k7c6	v
Východní	východní	k2eAgFnSc6d1	východní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1499	[number]	k4	1499
se	se	k3xPyFc4	se
u	u	k7c2	u
města	město	k1gNnSc2	město
objevila	objevit	k5eAaPmAgFnS	objevit
portugalská	portugalský	k2eAgFnSc1d1	portugalská
flotila	flotila	k1gFnSc1	flotila
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Vasco	Vasco	k6eAd1	Vasco
da	da	k?	da
Gamy	game	k1gInPc4	game
a	a	k8xC	a
zasypala	zasypat	k5eAaPmAgFnS	zasypat
město	město	k1gNnSc4	město
dělostřeleckou	dělostřelecký	k2eAgFnSc7d1	dělostřelecká
palbou	palba	k1gFnSc7	palba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Koloniální	koloniální	k2eAgFnSc1d1	koloniální
éra	éra	k1gFnSc1	éra
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
17	[number]	k4	17
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
město	město	k1gNnSc1	město
stalo	stát	k5eAaPmAgNnS	stát
dějištěm	dějiště	k1gNnSc7	dějiště
bitev	bitva	k1gFnPc2	bitva
mezi	mezi	k7c7	mezi
Portugalci	Portugalec	k1gMnPc7	Portugalec
a	a	k8xC	a
Turky	Turek	k1gMnPc7	Turek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
bojů	boj	k1gInPc2	boj
vyšli	vyjít	k5eAaPmAgMnP	vyjít
vítězně	vítězně	k6eAd1	vítězně
Osmanští	osmanský	k2eAgMnPc1d1	osmanský
Turci	Turek	k1gMnPc1	Turek
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
na	na	k7c4	na
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
město	město	k1gNnSc4	město
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1871	[number]	k4	1871
se	se	k3xPyFc4	se
Mogadišu	Mogadišo	k1gNnSc6	Mogadišo
dostalo	dostat	k5eAaPmAgNnS	dostat
pod	pod	k7c4	pod
moc	moc	k1gFnSc4	moc
sultána	sultán	k1gMnSc2	sultán
ze	z	k7c2	z
Zanzibaru	Zanzibar	k1gInSc2	Zanzibar
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zde	zde	k6eAd1	zde
nechal	nechat	k5eAaPmAgInS	nechat
vystavět	vystavět	k5eAaPmF	vystavět
palác	palác	k1gInSc1	palác
Haresa	Haresa	k1gFnSc1	Haresa
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
měl	mít	k5eAaImAgMnS	mít
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
sídlo	sídlo	k1gNnSc4	sídlo
správce	správce	k1gMnSc1	správce
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
paláci	palác	k1gInSc6	palác
Haresa	Haresa	k1gFnSc1	Haresa
muzeum	muzeum	k1gNnSc1	muzeum
a	a	k8xC	a
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1889	[number]	k4	1889
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
pronajato	pronajmout	k5eAaPmNgNnS	pronajmout
Italům	Ital	k1gMnPc3	Ital
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ho	on	k3xPp3gMnSc4	on
od	od	k7c2	od
zanzibarského	zanzibarský	k2eAgMnSc2d1	zanzibarský
sultána	sultán	k1gMnSc2	sultán
koupili	koupit	k5eAaPmAgMnP	koupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
<g/>
,	,	kIx,	,
a	a	k8xC	a
ustanovili	ustanovit	k5eAaPmAgMnP	ustanovit
ho	on	k3xPp3gMnSc4	on
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Italského	italský	k2eAgNnSc2d1	italské
Somálska	Somálsko	k1gNnSc2	Somálsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
se	se	k3xPyFc4	se
město	město	k1gNnSc4	město
po	po	k7c6	po
tvrdém	tvrdý	k2eAgInSc6d1	tvrdý
odporu	odpor	k1gInSc6	odpor
dostalo	dostat	k5eAaPmAgNnS	dostat
pod	pod	k7c4	pod
úplnou	úplný	k2eAgFnSc4d1	úplná
kontrolu	kontrola	k1gFnSc4	kontrola
Italů	Ital	k1gMnPc2	Ital
<g/>
.	.	kIx.	.
</s>
<s>
Fašistický	fašistický	k2eAgInSc1d1	fašistický
režim	režim	k1gInSc1	režim
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
plánoval	plánovat	k5eAaImAgMnS	plánovat
ve	v	k7c6	v
Východní	východní	k2eAgFnSc6d1	východní
Africe	Afrika	k1gFnSc6	Afrika
vybudovat	vybudovat	k5eAaPmF	vybudovat
velkou	velký	k2eAgFnSc4d1	velká
říši	říše	k1gFnSc4	říše
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1940	[number]	k4	1940
Italové	Ital	k1gMnPc1	Ital
napadli	napadnout	k5eAaPmAgMnP	napadnout
a	a	k8xC	a
obsadili	obsadit	k5eAaPmAgMnP	obsadit
britskou	britský	k2eAgFnSc4d1	britská
část	část	k1gFnSc4	část
Somálska	Somálsko	k1gNnSc2	Somálsko
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
Britské	britský	k2eAgNnSc1d1	Britské
Somálsko	Somálsko	k1gNnSc1	Somálsko
<g/>
,	,	kIx,	,
dnešní	dnešní	k2eAgInSc1d1	dnešní
Somaliland	Somaliland	k1gInSc1	Somaliland
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
však	však	k9	však
Britové	Brit	k1gMnPc1	Brit
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
generála	generál	k1gMnSc2	generál
Williama	William	k1gMnSc2	William
Platta	Platt	k1gMnSc2	Platt
na	na	k7c4	na
Italy	Ital	k1gMnPc4	Ital
zaútočili	zaútočit	k5eAaPmAgMnP	zaútočit
a	a	k8xC	a
dobyli	dobýt	k5eAaPmAgMnP	dobýt
ztracené	ztracený	k2eAgNnSc4d1	ztracené
území	území	k1gNnSc4	území
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Britové	Brit	k1gMnPc1	Brit
nakonec	nakonec	k6eAd1	nakonec
zaútočili	zaútočit	k5eAaPmAgMnP	zaútočit
i	i	k9	i
proti	proti	k7c3	proti
Italskému	italský	k2eAgNnSc3d1	italské
Somálsku	Somálsko	k1gNnSc3	Somálsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1941	[number]	k4	1941
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Mogadišu	Mogadišo	k1gNnSc3	Mogadišo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1950	[number]	k4	1950
se	se	k3xPyFc4	se
Mogadišu	Mogadišo	k1gNnSc3	Mogadišo
stalo	stát	k5eAaPmAgNnS	stát
administrativním	administrativní	k2eAgNnSc7d1	administrativní
střediskem	středisko	k1gNnSc7	středisko
Somálska	Somálsko	k1gNnSc2	Somálsko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
pod	pod	k7c7	pod
správou	správa	k1gFnSc7	správa
OSN	OSN	kA	OSN
pověřenou	pověřený	k2eAgFnSc7d1	pověřená
Itálií	Itálie	k1gFnSc7	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1956	[number]	k4	1956
získalo	získat	k5eAaPmAgNnS	získat
Somálsko	Somálsko	k1gNnSc1	Somálsko
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
samosprávu	samospráva	k1gFnSc4	samospráva
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1960	[number]	k4	1960
úplnou	úplný	k2eAgFnSc4d1	úplná
nezávislost	nezávislost	k1gFnSc4	nezávislost
na	na	k7c6	na
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
Mogadišu	Mogadišo	k1gNnSc3	Mogadišo
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kolaps	kolaps	k1gInSc1	kolaps
centrální	centrální	k2eAgFnSc2d1	centrální
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
povstalecké	povstalecký	k2eAgFnPc1d1	povstalecká
milice	milice	k1gFnPc1	milice
dobyly	dobýt	k5eAaPmAgFnP	dobýt
Mogadišu	Mogadišo	k1gNnSc6	Mogadišo
<g/>
,	,	kIx,	,
přinutily	přinutit	k5eAaPmAgInP	přinutit
tak	tak	k6eAd1	tak
prezidenta	prezident	k1gMnSc4	prezident
Berreho	Berre	k1gMnSc4	Berre
k	k	k7c3	k
úniku	únik	k1gInSc3	únik
do	do	k7c2	do
nigerijského	nigerijský	k2eAgInSc2d1	nigerijský
Lagosu	Lagos	k1gInSc2	Lagos
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
diktátorský	diktátorský	k2eAgInSc1d1	diktátorský
režim	režim	k1gInSc1	režim
byl	být	k5eAaImAgInS	být
svržen	svrhnout	k5eAaPmNgInS	svrhnout
<g/>
.	.	kIx.	.
</s>
<s>
Vítězné	vítězný	k2eAgInPc1d1	vítězný
klany	klan	k1gInPc1	klan
se	se	k3xPyFc4	se
však	však	k9	však
již	již	k6eAd1	již
nedokázaly	dokázat	k5eNaPmAgFnP	dokázat
shodnout	shodnout	k5eAaPmF	shodnout
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgMnS	mít
stát	stát	k5eAaImF	stát
Barreho	Barre	k1gMnSc4	Barre
nástupcem	nástupce	k1gMnSc7	nástupce
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
klany	klan	k1gInPc1	klan
navrhovaly	navrhovat	k5eAaImAgInP	navrhovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
se	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
měl	mít	k5eAaImAgMnS	mít
stát	stát	k5eAaPmF	stát
Ali	Ali	k1gFnSc4	Ali
Mahdi	Mahd	k1gMnPc1	Mahd
Muhammad	Muhammad	k1gInSc4	Muhammad
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc1d1	jiná
zase	zase	k9	zase
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
jím	on	k3xPp3gNnSc7	on
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
vůdce	vůdce	k1gMnSc4	vůdce
nejsilnější	silný	k2eAgFnSc2d3	nejsilnější
skupiny	skupina	k1gFnSc2	skupina
klanů	klan	k1gInPc2	klan
<g/>
,	,	kIx,	,
Mohammed	Mohammed	k1gMnSc1	Mohammed
Farrah	Farraha	k1gFnPc2	Farraha
Aidid	Aidida	k1gFnPc2	Aidida
<g/>
.	.	kIx.	.
</s>
<s>
Začala	začít	k5eAaPmAgFnS	začít
tak	tak	k9	tak
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
mezi	mezi	k7c7	mezi
znepřátelenými	znepřátelený	k2eAgFnPc7d1	znepřátelená
frakcemi	frakce	k1gFnPc7	frakce
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
usilovala	usilovat	k5eAaImAgFnS	usilovat
o	o	k7c4	o
vládu	vláda	k1gFnSc4	vláda
nad	nad	k7c7	nad
Somálskem	Somálsko	k1gNnSc7	Somálsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
mít	mít	k5eAaImF	mít
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
Mogadišo	Mogadišo	k1gNnSc4	Mogadišo
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
město	město	k1gNnSc4	město
se	se	k3xPyFc4	se
v	v	k7c6	v
letech	let	k1gInPc6	let
1991	[number]	k4	1991
<g/>
–	–	k?	–
<g/>
92	[number]	k4	92
sváděly	svádět	k5eAaImAgInP	svádět
těžké	těžký	k2eAgInPc1d1	těžký
boje	boj	k1gInPc1	boj
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
však	však	k9	však
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
desetitisícům	desetitisíce	k1gInPc3	desetitisíce
mrtvých	mrtvý	k1gMnPc2	mrtvý
a	a	k8xC	a
devastaci	devastace	k1gFnSc4	devastace
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
zemi	zem	k1gFnSc6	zem
sužovalo	sužovat	k5eAaImAgNnS	sužovat
velké	velký	k2eAgNnSc1d1	velké
sucho	sucho	k1gNnSc1	sucho
a	a	k8xC	a
hladomor	hladomor	k1gInSc1	hladomor
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
a	a	k8xC	a
OSN	OSN	kA	OSN
k	k	k7c3	k
vyslání	vyslání	k1gNnSc3	vyslání
mírových	mírový	k2eAgFnPc2d1	mírová
sil	síla	k1gFnPc2	síla
do	do	k7c2	do
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1992	[number]	k4	1992
bylo	být	k5eAaImAgNnS	být
poblíž	poblíž	k7c2	poblíž
Mogadiša	Mogadišo	k1gNnSc2	Mogadišo
vysazeno	vysadit	k5eAaPmNgNnS	vysadit
20	[number]	k4	20
000	[number]	k4	000
příslušníků	příslušník	k1gMnPc2	příslušník
námořní	námořní	k2eAgFnSc2d1	námořní
pěchoty	pěchota	k1gFnSc2	pěchota
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
této	tento	k3xDgFnSc2	tento
akce	akce	k1gFnSc2	akce
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgFnSc2d1	zvaná
Obnovená	obnovený	k2eAgFnSc1d1	obnovená
naděje	naděje	k1gFnPc1	naděje
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zajmout	zajmout	k5eAaPmF	zajmout
Mohammeda	Mohammed	k1gMnSc4	Mohammed
Farraha	Farrah	k1gMnSc4	Farrah
Aidida	Aidid	k1gMnSc4	Aidid
<g/>
.	.	kIx.	.
</s>
<s>
Aididova	Aididův	k2eAgFnSc1d1	Aididův
milice	milice	k1gFnSc1	milice
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
1993	[number]	k4	1993
napadla	napadnout	k5eAaPmAgFnS	napadnout
a	a	k8xC	a
pobila	pobít	k5eAaPmAgFnS	pobít
v	v	k7c6	v
Mogadišu	Mogadišo	k1gNnSc6	Mogadišo
24	[number]	k4	24
pákistánských	pákistánský	k2eAgMnPc2d1	pákistánský
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1993	[number]	k4	1993
se	se	k3xPyFc4	se
elitní	elitní	k2eAgFnSc1d1	elitní
americká	americký	k2eAgFnSc1d1	americká
jednotka	jednotka	k1gFnSc1	jednotka
(	(	kIx(	(
<g/>
Task	Task	k1gInSc1	Task
Force	force	k1gFnSc2	force
Ranger	Rangra	k1gFnPc2	Rangra
<g/>
,	,	kIx,	,
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
jednotek	jednotka	k1gFnPc2	jednotka
75	[number]	k4	75
<g/>
th	th	k?	th
Ranger	Rangero	k1gNnPc2	Rangero
Regiment	regiment	k1gInSc1	regiment
a	a	k8xC	a
1	[number]	k4	1
<g/>
st	st	kA	st
Special	Special	k1gMnSc1	Special
Forces	Forces	k1gMnSc1	Forces
Operational	Operational	k1gMnSc1	Operational
Detachment-Delta	Detachment-Delta	k1gMnSc1	Detachment-Delta
<g/>
)	)	kIx)	)
vydaly	vydat	k5eAaPmAgFnP	vydat
do	do	k7c2	do
Mogadiša	Mogadišo	k1gNnSc2	Mogadišo
<g/>
,	,	kIx,	,
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
zajmout	zajmout	k5eAaPmF	zajmout
dva	dva	k4xCgInPc4	dva
vysoké	vysoký	k2eAgMnPc4d1	vysoký
Aididovy	Aididův	k2eAgMnPc4d1	Aididův
poradce	poradce	k1gMnPc4	poradce
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
bitvy	bitva	k1gFnSc2	bitva
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
zasaženo	zasáhnout	k5eAaPmNgNnS	zasáhnout
5	[number]	k4	5
vrtulníků	vrtulník	k1gInPc2	vrtulník
UH-60	UH-60	k1gFnSc2	UH-60
Black	Black	k1gMnSc1	Black
Hawk	Hawk	k1gMnSc1	Hawk
(	(	kIx(	(
<g/>
dva	dva	k4xCgMnPc1	dva
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
zřítily	zřítit	k5eAaPmAgFnP	zřítit
do	do	k7c2	do
města	město	k1gNnSc2	město
a	a	k8xC	a
zbývající	zbývající	k2eAgFnPc1d1	zbývající
byly	být	k5eAaImAgFnP	být
pouze	pouze	k6eAd1	pouze
poškozeny	poškodit	k5eAaPmNgFnP	poškodit
<g/>
,	,	kIx,	,
a	a	k8xC	a
přistály	přistát	k5eAaPmAgFnP	přistát
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
v	v	k7c6	v
Mogadišu	Mogadišo	k1gNnSc6	Mogadišo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Americké	americký	k2eAgFnPc1d1	americká
jednotky	jednotka	k1gFnPc1	jednotka
ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
vydaly	vydat	k5eAaPmAgFnP	vydat
k	k	k7c3	k
místům	místo	k1gNnPc3	místo
havárie	havárie	k1gFnSc2	havárie
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
musely	muset	k5eAaImAgFnP	muset
čelit	čelit	k5eAaImF	čelit
útokům	útok	k1gInPc3	útok
Somálců	Somálec	k1gMnPc2	Somálec
<g/>
.	.	kIx.	.
</s>
<s>
Dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
na	na	k7c4	na
určené	určený	k2eAgNnSc4d1	určené
místo	místo	k1gNnSc4	místo
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
jen	jen	k9	jen
pár	pár	k4xCyI	pár
jednotkám	jednotka	k1gFnPc3	jednotka
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
ve	v	k7c6	v
městě	město	k1gNnSc6	město
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
obklíčeno	obklíčit	k5eAaPmNgNnS	obklíčit
přes	přes	k7c4	přes
sto	sto	k4xCgNnSc4	sto
Američanů	Američan	k1gMnPc2	Američan
<g/>
,	,	kIx,	,
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Obklíčené	obklíčený	k2eAgFnSc2d1	obklíčená
jednotky	jednotka	k1gFnSc2	jednotka
musela	muset	k5eAaImAgFnS	muset
vyprostit	vyprostit	k5eAaPmF	vyprostit
10	[number]	k4	10
<g/>
.	.	kIx.	.
horská	horský	k2eAgFnSc1d1	horská
divize	divize	k1gFnSc1	divize
<g/>
.	.	kIx.	.
</s>
<s>
Akce	akce	k1gFnSc1	akce
skončila	skončit	k5eAaPmAgFnS	skončit
smrtí	smrt	k1gFnSc7	smrt
osmnácti	osmnáct	k4xCc2	osmnáct
Američanů	Američan	k1gMnPc2	Američan
a	a	k8xC	a
jednoho	jeden	k4xCgMnSc2	jeden
Malajce	Malajec	k1gMnSc2	Malajec
<g/>
.	.	kIx.	.
</s>
<s>
Somálců	Somálec	k1gMnPc2	Somálec
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
něco	něco	k6eAd1	něco
kolem	kolem	k6eAd1	kolem
tisíce	tisíc	k4xCgInPc4	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
Americké	americký	k2eAgFnPc1d1	americká
jednotky	jednotka	k1gFnPc1	jednotka
se	se	k3xPyFc4	se
ze	z	k7c2	z
Somálska	Somálsko	k1gNnSc2	Somálsko
stáhly	stáhnout	k5eAaPmAgFnP	stáhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
zemi	zem	k1gFnSc6	zem
opustily	opustit	k5eAaPmAgInP	opustit
také	také	k9	také
dozorčí	dozorčí	k1gFnSc1	dozorčí
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Boje	boj	k1gInPc1	boj
mezi	mezi	k7c7	mezi
znepřátelenými	znepřátelený	k2eAgInPc7d1	znepřátelený
klany	klan	k1gInPc7	klan
v	v	k7c6	v
Mogadišu	Mogadišo	k1gNnSc6	Mogadišo
sílily	sílit	k5eAaImAgFnP	sílit
<g/>
.	.	kIx.	.
</s>
<s>
Mohammed	Mohammed	k1gMnSc1	Mohammed
Farrah	Farrah	k1gMnSc1	Farrah
Aidid	Aidida	k1gFnPc2	Aidida
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
prezidentem	prezident	k1gMnSc7	prezident
Somálska	Somálsko	k1gNnSc2	Somálsko
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
měl	mít	k5eAaImAgMnS	mít
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
jen	jen	k6eAd1	jen
Mogadišu	Mogadišo	k1gNnSc3	Mogadišo
a	a	k8xC	a
okolí	okolí	k1gNnSc3	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
na	na	k7c4	na
následky	následek	k1gInPc4	následek
zranění	zranění	k1gNnSc2	zranění
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
utrpěl	utrpět	k5eAaPmAgMnS	utrpět
při	při	k7c6	při
bojích	boj	k1gInPc6	boj
v	v	k7c6	v
Mogadišu	Mogadišo	k1gNnSc6	Mogadišo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mogadišu	Mogadišo	k1gNnSc6	Mogadišo
dnes	dnes	k6eAd1	dnes
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Druhá	druhý	k4xOgFnSc1	druhý
bitva	bitva	k1gFnSc1	bitva
v	v	k7c4	v
Mogadišo	Mogadišo	k1gNnSc4	Mogadišo
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
v	v	k7c6	v
Somálsku	Somálsko	k1gNnSc6	Somálsko
objevilo	objevit	k5eAaPmAgNnS	objevit
hnutí	hnutí	k1gNnSc1	hnutí
s	s	k7c7	s
názvem	název	k1gInSc7	název
Svaz	svaz	k1gInSc1	svaz
islámských	islámský	k2eAgInPc2d1	islámský
soudů	soud	k1gInPc2	soud
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
postupně	postupně	k6eAd1	postupně
stávat	stávat	k5eAaImF	stávat
nejmocnější	mocný	k2eAgFnSc7d3	nejmocnější
silou	síla	k1gFnSc7	síla
v	v	k7c6	v
Somálsku	Somálsko	k1gNnSc6	Somálsko
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2006	[number]	k4	2006
vypukl	vypuknout	k5eAaPmAgInS	vypuknout
v	v	k7c6	v
Mogadišu	Mogadišo	k1gNnSc6	Mogadišo
boj	boj	k1gInSc4	boj
mezi	mezi	k7c7	mezi
Svazem	svaz	k1gInSc7	svaz
a	a	k8xC	a
Spojenectvím	spojenectví	k1gNnSc7	spojenectví
pro	pro	k7c4	pro
obnovu	obnova	k1gFnSc4	obnova
míru	míra	k1gFnSc4	míra
a	a	k8xC	a
proti	proti	k7c3	proti
terorismu	terorismus	k1gInSc3	terorismus
<g/>
.	.	kIx.	.
</s>
<s>
Nejtvrdší	tvrdý	k2eAgInPc1d3	nejtvrdší
boje	boj	k1gInPc1	boj
se	se	k3xPyFc4	se
odehrály	odehrát	k5eAaPmAgInP	odehrát
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Sii-Sii	Sii-Sie	k1gFnSc4	Sii-Sie
v	v	k7c4	v
severní	severní	k2eAgFnPc4d1	severní
části	část	k1gFnPc4	část
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
soboty	sobota	k1gFnSc2	sobota
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
bylo	být	k5eAaImAgNnS	být
při	při	k7c6	při
pouličních	pouliční	k2eAgInPc6d1	pouliční
bojích	boj	k1gInPc6	boj
hlášeno	hlásit	k5eAaImNgNnS	hlásit
144	[number]	k4	144
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
strany	strana	k1gFnPc1	strana
se	s	k7c7	s
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
dohodly	dohodnout	k5eAaPmAgFnP	dohodnout
na	na	k7c6	na
příměří	příměří	k1gNnSc6	příměří
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
už	už	k6eAd1	už
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
se	se	k3xPyFc4	se
boje	boj	k1gInPc1	boj
znovu	znovu	k6eAd1	znovu
rozhořely	rozhořet	k5eAaPmAgInP	rozhořet
<g/>
.	.	kIx.	.
</s>
<s>
Svaz	svaz	k1gInSc1	svaz
islámských	islámský	k2eAgInPc2d1	islámský
soudů	soud	k1gInPc2	soud
postupně	postupně	k6eAd1	postupně
získával	získávat	k5eAaImAgInS	získávat
převahu	převaha	k1gFnSc4	převaha
<g/>
,	,	kIx,	,
až	až	k9	až
se	s	k7c7	s
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
Spojenectví	spojenectví	k1gNnSc2	spojenectví
pro	pro	k7c4	pro
obnovu	obnova	k1gFnSc4	obnova
míru	míra	k1gFnSc4	míra
a	a	k8xC	a
proti	proti	k7c3	proti
terorismu	terorismus	k1gInSc3	terorismus
vzdalo	vzdát	k5eAaPmAgNnS	vzdát
a	a	k8xC	a
Mogadišu	Mogadišo	k1gNnSc3	Mogadišo
definitivně	definitivně	k6eAd1	definitivně
ovládly	ovládnout	k5eAaPmAgFnP	ovládnout
islámské	islámský	k2eAgFnPc1d1	islámská
milice	milice	k1gFnPc1	milice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
co	co	k9	co
Mogadišu	Mogadišo	k1gNnSc3	Mogadišo
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
islamisté	islamista	k1gMnPc1	islamista
ze	z	k7c2	z
Svazu	svaz	k1gInSc2	svaz
islámských	islámský	k2eAgInPc2d1	islámský
soudů	soud	k1gInPc2	soud
<g/>
,	,	kIx,	,
nastal	nastat	k5eAaPmAgInS	nastat
ve	v	k7c6	v
městě	město	k1gNnSc6	město
klid	klid	k1gInSc1	klid
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
Mogadiša	Mogadišo	k1gNnSc2	Mogadišo
byli	být	k5eAaImAgMnP	být
zpočátku	zpočátku	k6eAd1	zpočátku
nadšeni	nadchnout	k5eAaPmNgMnP	nadchnout
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gNnSc1	jejich
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
islamistů	islamista	k1gMnPc2	islamista
<g/>
,	,	kIx,	,
konečně	konečně	k6eAd1	konečně
si	se	k3xPyFc3	se
po	po	k7c6	po
letech	léto	k1gNnPc6	léto
bojů	boj	k1gInPc2	boj
mohli	moct	k5eAaImAgMnP	moct
oddychnout	oddychnout	k5eAaPmF	oddychnout
<g/>
.	.	kIx.	.
</s>
<s>
Islamisté	islamista	k1gMnPc1	islamista
však	však	k9	však
ve	v	k7c6	v
městě	město	k1gNnSc6	město
začali	začít	k5eAaPmAgMnP	začít
prosazovat	prosazovat	k5eAaImF	prosazovat
islámské	islámský	k2eAgNnSc4d1	islámské
právo	právo	k1gNnSc4	právo
šaría	šaría	k6eAd1	šaría
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
u	u	k7c2	u
místních	místní	k2eAgMnPc2d1	místní
obyvatel	obyvatel	k1gMnPc2	obyvatel
projevilo	projevit	k5eAaPmAgNnS	projevit
s	s	k7c7	s
jistou	jistý	k2eAgFnSc7d1	jistá
dávkou	dávka	k1gFnSc7	dávka
znepokojenosti	znepokojenost	k1gFnSc2	znepokojenost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Útěk	útěk	k1gInSc1	útěk
z	z	k7c2	z
Mogadiša	Mogadišo	k1gNnSc2	Mogadišo
===	===	k?	===
</s>
</p>
<p>
<s>
Začátkem	začátkem	k7c2	začátkem
prosince	prosinec	k1gInSc2	prosinec
2006	[number]	k4	2006
začala	začít	k5eAaPmAgFnS	začít
ofenzíva	ofenzíva	k1gFnSc1	ofenzíva
Islámských	islámský	k2eAgInPc2d1	islámský
soudů	soud	k1gInPc2	soud
proti	proti	k7c3	proti
přechodné	přechodný	k2eAgFnSc3d1	přechodná
somálské	somálský	k2eAgFnSc3d1	Somálská
vládě	vláda	k1gFnSc3	vláda
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Baidoa	Baido	k1gInSc2	Baido
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
se	se	k3xPyFc4	se
do	do	k7c2	do
bojů	boj	k1gInPc2	boj
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
somálské	somálský	k2eAgFnSc2d1	Somálská
vlády	vláda	k1gFnSc2	vláda
oficiálně	oficiálně	k6eAd1	oficiálně
zapojila	zapojit	k5eAaPmAgFnS	zapojit
také	také	k9	také
etiopská	etiopský	k2eAgFnSc1d1	etiopská
armáda	armáda	k1gFnSc1	armáda
(	(	kIx(	(
<g/>
už	už	k6eAd1	už
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2006	[number]	k4	2006
některé	některý	k3yIgInPc1	některý
zdroje	zdroj	k1gInPc1	zdroj
informovaly	informovat	k5eAaBmAgInP	informovat
o	o	k7c6	o
umístění	umístění	k1gNnSc6	umístění
etiopských	etiopský	k2eAgNnPc2d1	etiopské
vojsk	vojsko	k1gNnPc2	vojsko
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
města	město	k1gNnSc2	město
Baidoa	Baido	k1gInSc2	Baido
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nebýt	být	k5eNaImF	být
vstupu	vstup	k1gInSc2	vstup
etiopské	etiopský	k2eAgFnSc2d1	etiopská
armády	armáda	k1gFnSc2	armáda
do	do	k7c2	do
bojů	boj	k1gInPc2	boj
<g/>
,	,	kIx,	,
somálská	somálský	k2eAgFnSc1d1	Somálská
vláda	vláda	k1gFnSc1	vláda
by	by	kYmCp3nS	by
patrně	patrně	k6eAd1	patrně
byla	být	k5eAaImAgFnS	být
přinucena	přinucen	k2eAgFnSc1d1	přinucena
kapitulovat	kapitulovat	k5eAaBmF	kapitulovat
<g/>
.	.	kIx.	.
</s>
<s>
Etiopané	Etiopaný	k2eAgNnSc1d1	Etiopané
nasadili	nasadit	k5eAaPmAgMnP	nasadit
proti	proti	k7c3	proti
islamistům	islamista	k1gMnPc3	islamista
tanky	tank	k1gInPc1	tank
a	a	k8xC	a
letectvo	letectvo	k1gNnSc4	letectvo
a	a	k8xC	a
přinutili	přinutit	k5eAaPmAgMnP	přinutit
je	on	k3xPp3gNnSc4	on
k	k	k7c3	k
ústupu	ústup	k1gInSc3	ústup
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
somálské	somálský	k2eAgFnSc2d1	Somálská
vládní	vládní	k2eAgFnSc2d1	vládní
jednotky	jednotka	k1gFnSc2	jednotka
společně	společně	k6eAd1	společně
s	s	k7c7	s
etiopskou	etiopský	k2eAgFnSc7d1	etiopská
vládou	vláda	k1gFnSc7	vláda
dobyly	dobýt	k5eAaPmAgInP	dobýt
město	město	k1gNnSc4	město
Jowhar	Jowhara	k1gFnPc2	Jowhara
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
90	[number]	k4	90
km	km	kA	km
od	od	k7c2	od
Mogadiša	Mogadišo	k1gNnSc2	Mogadišo
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
přimělo	přimět	k5eAaPmAgNnS	přimět
islamisty	islamista	k1gMnSc2	islamista
k	k	k7c3	k
úniku	únik	k1gInSc3	únik
ze	z	k7c2	z
somálské	somálský	k2eAgFnSc2d1	Somálská
metropole	metropol	k1gFnSc2	metropol
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
,	,	kIx,	,
k	k	k7c3	k
přístavu	přístav	k1gInSc3	přístav
Kismaayo	Kismaayo	k6eAd1	Kismaayo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Mogadišu	Mogadišo	k1gNnSc6	Mogadišo
byly	být	k5eAaImAgFnP	být
vládní	vládní	k2eAgFnPc1d1	vládní
jednotky	jednotka	k1gFnPc1	jednotka
nadšeně	nadšeně	k6eAd1	nadšeně
vítány	vítat	k5eAaImNgFnP	vítat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úniku	únik	k1gInSc6	únik
Islámských	islámský	k2eAgInPc2d1	islámský
soudů	soud	k1gInPc2	soud
se	se	k3xPyFc4	se
však	však	k9	však
ve	v	k7c6	v
městě	město	k1gNnSc6	město
opět	opět	k6eAd1	opět
začalo	začít	k5eAaPmAgNnS	začít
šířit	šířit	k5eAaImF	šířit
násilí	násilí	k1gNnSc4	násilí
<g/>
.	.	kIx.	.
</s>
<s>
Opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
klany	klan	k1gInPc1	klan
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
předtím	předtím	k6eAd1	předtím
většinou	většinou	k6eAd1	většinou
přidaly	přidat	k5eAaPmAgFnP	přidat
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
islamistů	islamista	k1gMnPc2	islamista
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
přijímaly	přijímat	k5eAaImAgFnP	přijímat
zpět	zpět	k6eAd1	zpět
své	svůj	k3xOyFgMnPc4	svůj
dřívější	dřívější	k2eAgMnPc4d1	dřívější
bojovníky	bojovník	k1gMnPc4	bojovník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Somálská	somálský	k2eAgFnSc1d1	Somálská
vláda	vláda	k1gFnSc1	vláda
dosadila	dosadit	k5eAaPmAgFnS	dosadit
na	na	k7c4	na
post	post	k1gInSc4	post
starosty	starosta	k1gMnSc2	starosta
Mahmouda	Mahmoud	k1gMnSc2	Mahmoud
Hassana	Hassan	k1gMnSc2	Hassan
Aliho	Ali	k1gMnSc2	Ali
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
na	na	k7c6	na
stejném	stejný	k2eAgInSc6d1	stejný
postu	post	k1gInSc6	post
působil	působit	k5eAaImAgMnS	působit
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
radikálů	radikál	k1gMnPc2	radikál
<g/>
.	.	kIx.	.
</s>
<s>
Premiér	premiér	k1gMnSc1	premiér
Ali	Ali	k1gMnSc1	Ali
Mohamed	Mohamed	k1gMnSc1	Mohamed
Ghedi	Ghed	k1gMnPc1	Ghed
oznámil	oznámit	k5eAaPmAgInS	oznámit
záměr	záměr	k1gInSc1	záměr
přesunout	přesunout	k5eAaPmF	přesunout
sídlo	sídlo	k1gNnSc4	sídlo
vlády	vláda	k1gFnSc2	vláda
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Mogadiša	Mogadišo	k1gNnSc2	Mogadišo
<g/>
.	.	kIx.	.
</s>
<s>
Totéž	týž	k3xTgNnSc4	týž
učinil	učinit	k5eAaPmAgMnS	učinit
i	i	k9	i
prezident	prezident	k1gMnSc1	prezident
Yusuf	Yusuf	k1gMnSc1	Yusuf
Ahmed	Ahmed	k1gMnSc1	Ahmed
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
se	se	k3xPyFc4	se
představitelé	představitel	k1gMnPc1	představitel
klanů	klan	k1gInPc2	klan
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
se	s	k7c7	s
somálskou	somálský	k2eAgFnSc7d1	Somálská
vládou	vláda	k1gFnSc7	vláda
o	o	k7c4	o
připojení	připojení	k1gNnSc4	připojení
klanů	klan	k1gInPc2	klan
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
somálské	somálský	k2eAgFnSc2d1	Somálská
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
a	a	k8xC	a
ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
Mogadišo	Mogadišo	k1gNnSc1	Mogadišo
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
komerční	komerční	k2eAgNnSc1d1	komerční
a	a	k8xC	a
finanční	finanční	k2eAgNnSc1d1	finanční
centrum	centrum	k1gNnSc1	centrum
Somálska	Somálsko	k1gNnSc2	Somálsko
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářství	hospodářství	k1gNnSc1	hospodářství
se	s	k7c7	s
<g/>
,	,	kIx,	,
po	po	k7c6	po
dlouhých	dlouhý	k2eAgInPc6d1	dlouhý
bojích	boj	k1gInPc6	boj
<g/>
,	,	kIx,	,
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
poněkud	poněkud	k6eAd1	poněkud
zlepšilo	zlepšit	k5eAaPmAgNnS	zlepšit
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
nulový	nulový	k2eAgInSc1d1	nulový
vliv	vliv	k1gInSc1	vliv
vlády	vláda	k1gFnSc2	vláda
ve	v	k7c6	v
městě	město	k1gNnSc6	město
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
volný	volný	k2eAgInSc1d1	volný
obchod	obchod	k1gInSc1	obchod
bez	bez	k7c2	bez
daní	daň	k1gFnPc2	daň
nebo	nebo	k8xC	nebo
regulačních	regulační	k2eAgInPc2d1	regulační
poplatků	poplatek	k1gInPc2	poplatek
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zdejších	zdejší	k2eAgNnPc2d1	zdejší
průmyslových	průmyslový	k2eAgNnPc2d1	průmyslové
odvětví	odvětví	k1gNnPc2	odvětví
je	být	k5eAaImIp3nS	být
nejvýznamnější	významný	k2eAgMnSc1d3	nejvýznamnější
petrochemické	petrochemický	k2eAgInPc4d1	petrochemický
<g/>
,	,	kIx,	,
potravinářské	potravinářský	k2eAgInPc4d1	potravinářský
<g/>
,	,	kIx,	,
textilní	textilní	k2eAgInPc4d1	textilní
<g/>
,	,	kIx,	,
kožedělné	kožedělný	k2eAgInPc4d1	kožedělný
a	a	k8xC	a
loďařské	loďařský	k2eAgInPc4d1	loďařský
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
trh	trh	k1gInSc1	trh
nabízí	nabízet	k5eAaImIp3nS	nabízet
širokou	široký	k2eAgFnSc4d1	široká
nabídku	nabídka	k1gFnSc4	nabídka
zboží	zboží	k1gNnSc2	zboží
<g/>
,	,	kIx,	,
od	od	k7c2	od
jídla	jídlo	k1gNnSc2	jídlo
až	až	k9	až
po	po	k7c4	po
elektroniku	elektronika	k1gFnSc4	elektronika
<g/>
.	.	kIx.	.
</s>
<s>
Silnice	silnice	k1gFnPc1	silnice
jsou	být	k5eAaImIp3nP	být
nekvalitní	kvalitní	k2eNgInPc4d1	nekvalitní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
několika	několik	k4yIc2	několik
dalšími	další	k2eAgFnPc7d1	další
somálskými	somálský	k2eAgFnPc7d1	Somálská
městy	město	k1gNnPc7	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Mogadišu	Mogadišo	k1gNnSc6	Mogadišo
je	být	k5eAaImIp3nS	být
také	také	k9	také
přístav	přístav	k1gInSc1	přístav
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
však	však	k9	však
není	být	k5eNaImIp3nS	být
mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
obchodem	obchod	k1gInSc7	obchod
příliš	příliš	k6eAd1	příliš
využíván	využívat	k5eAaImNgInS	využívat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
somálských	somálský	k2eAgFnPc6d1	Somálská
vodách	voda	k1gFnPc6	voda
řádí	řádit	k5eAaImIp3nP	řádit
piráti	pirát	k1gMnPc1	pirát
<g/>
.	.	kIx.	.
</s>
<s>
Letiště	letiště	k1gNnSc1	letiště
v	v	k7c6	v
Mogadišu	Mogadišo	k1gNnSc6	Mogadišo
je	být	k5eAaImIp3nS	být
také	také	k9	také
nefunkční	funkční	k2eNgMnSc1d1	nefunkční
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
intenzivním	intenzivní	k2eAgInPc3d1	intenzivní
bojům	boj	k1gInPc3	boj
velmi	velmi	k6eAd1	velmi
poničené	poničený	k2eAgInPc4d1	poničený
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přes	přes	k7c4	přes
nezákonnost	nezákonnost	k1gFnSc4	nezákonnost
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
možná	možná	k9	možná
díky	díky	k7c3	díky
ní	on	k3xPp3gFnSc2	on
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mogadišo	Mogadišo	k1gNnSc1	Mogadišo
vede	vést	k5eAaImIp3nS	vést
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Africe	Afrika	k1gFnSc6	Afrika
v	v	k7c6	v
přístupu	přístup	k1gInSc6	přístup
k	k	k7c3	k
telekomunikacím	telekomunikace	k1gFnPc3	telekomunikace
a	a	k8xC	a
internetu	internet	k1gInSc6	internet
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgFnSc1d1	moderní
komunikační	komunikační	k2eAgFnSc1d1	komunikační
síť	síť	k1gFnSc1	síť
je	být	k5eAaImIp3nS	být
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
a	a	k8xC	a
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
místní	místní	k2eAgInPc4d1	místní
mobilní	mobilní	k2eAgInPc4d1	mobilní
telefonní	telefonní	k2eAgInPc4d1	telefonní
systémy	systém	k1gInPc4	systém
s	s	k7c7	s
mezinárodními	mezinárodní	k2eAgInPc7d1	mezinárodní
kontakty	kontakt	k1gInPc7	kontakt
přes	přes	k7c4	přes
satelit	satelit	k1gInSc4	satelit
<g/>
.	.	kIx.	.
</s>
<s>
Nepřítomnost	nepřítomnost	k1gFnSc1	nepřítomnost
daní	daň	k1gFnPc2	daň
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
nízkým	nízký	k2eAgFnPc3d1	nízká
cenám	cena	k1gFnPc3	cena
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
nejnižší	nízký	k2eAgFnSc4d3	nejnižší
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
četné	četný	k2eAgFnPc1d1	četná
internetové	internetový	k2eAgFnPc1d1	internetová
kavárny	kavárna	k1gFnPc1	kavárna
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
také	také	k9	také
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc4	několik
rozhlasových	rozhlasový	k2eAgFnPc2d1	rozhlasová
stanic	stanice	k1gFnPc2	stanice
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc4	dva
televizní	televizní	k2eAgInPc4d1	televizní
vysílače	vysílač	k1gInPc4	vysílač
a	a	k8xC	a
poskytovatele	poskytovatel	k1gMnPc4	poskytovatel
internetu	internet	k1gInSc2	internet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Ankara	Ankara	k1gFnSc1	Ankara
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mogadišo	Mogadišo	k1gNnSc4	Mogadišo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Mogadišo	Mogadišo	k1gNnSc4	Mogadišo
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
