<s>
Mléko	mléko	k1gNnSc1	mléko
je	být	k5eAaImIp3nS	být
produkt	produkt	k1gInSc4	produkt
mléčných	mléčný	k2eAgFnPc2d1	mléčná
žláz	žláza	k1gFnPc2	žláza
(	(	kIx(	(
<g/>
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
<g/>
)	)	kIx)	)
samic	samice	k1gFnPc2	samice
savců	savec	k1gMnPc2	savec
<g/>
.	.	kIx.	.
</s>
<s>
Mléko	mléko	k1gNnSc1	mléko
je	být	k5eAaImIp3nS	být
základním	základní	k2eAgInSc7d1	základní
zdrojem	zdroj	k1gInSc7	zdroj
výživy	výživa	k1gFnSc2	výživa
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
mláďata	mládě	k1gNnPc4	mládě
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
mleziva	mlezivo	k1gNnPc1	mlezivo
<g/>
"	"	kIx"	"
získávají	získávat	k5eAaImIp3nP	získávat
potřebné	potřebný	k2eAgFnPc4d1	potřebná
protilátky	protilátka	k1gFnPc4	protilátka
a	a	k8xC	a
vitamíny	vitamín	k1gInPc4	vitamín
pro	pro	k7c4	pro
upevnění	upevnění	k1gNnSc4	upevnění
své	svůj	k3xOyFgFnSc2	svůj
imunity	imunita	k1gFnSc2	imunita
<g/>
.	.	kIx.	.
</s>
<s>
Savci	savec	k1gMnPc1	savec
konzumují	konzumovat	k5eAaBmIp3nP	konzumovat
mléko	mléko	k1gNnSc4	mléko
až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
nejsou	být	k5eNaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
trávit	trávit	k5eAaImF	trávit
pevnou	pevný	k2eAgFnSc4d1	pevná
stravu	strava	k1gFnSc4	strava
(	(	kIx(	(
<g/>
píce	píce	k1gFnSc1	píce
<g/>
,	,	kIx,	,
maso	maso	k1gNnSc1	maso
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
zpravidla	zpravidla	k6eAd1	zpravidla
o	o	k7c4	o
schopnost	schopnost	k1gFnSc4	schopnost
zužitkovat	zužitkovat	k5eAaPmF	zužitkovat
bílkoviny	bílkovina	k1gFnPc4	bílkovina
z	z	k7c2	z
mléka	mléko	k1gNnSc2	mléko
přicházejí	přicházet	k5eAaImIp3nP	přicházet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
výživě	výživa	k1gFnSc6	výživa
se	se	k3xPyFc4	se
označením	označení	k1gNnSc7	označení
"	"	kIx"	"
<g/>
mléko	mléko	k1gNnSc1	mléko
<g/>
"	"	kIx"	"
většinou	většinou	k6eAd1	většinou
rozumí	rozumět	k5eAaImIp3nS	rozumět
kravské	kravský	k2eAgNnSc1d1	kravské
mléko	mléko	k1gNnSc1	mléko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
konzumováno	konzumovat	k5eAaBmNgNnS	konzumovat
nejčastěji	často	k6eAd3	často
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
mlékem	mléko	k1gNnSc7	mléko
se	se	k3xPyFc4	se
setkáváme	setkávat	k5eAaImIp1nP	setkávat
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
podobách	podoba	k1gFnPc6	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
to	ten	k3xDgNnSc1	ten
být	být	k5eAaImF	být
čerstvé	čerstvý	k2eAgNnSc1d1	čerstvé
mléko	mléko	k1gNnSc1	mléko
<g/>
,	,	kIx,	,
zkyslé	zkyslý	k2eAgNnSc1d1	zkyslé
mléko	mléko	k1gNnSc1	mléko
<g/>
,	,	kIx,	,
sušené	sušený	k2eAgNnSc1d1	sušené
mléko	mléko	k1gNnSc1	mléko
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
Rostlinnými	rostlinný	k2eAgFnPc7d1	rostlinná
náhražkami	náhražka	k1gFnPc7	náhražka
zvířecího	zvířecí	k2eAgInSc2d1	zvířecí
mléka	mléko	k1gNnSc2	mléko
jsou	být	k5eAaImIp3nP	být
sojové	sojový	k2eAgNnSc4d1	sojové
mléko	mléko	k1gNnSc4	mléko
<g/>
,	,	kIx,	,
rýžové	rýžový	k2eAgNnSc1d1	rýžové
mléko	mléko	k1gNnSc1	mléko
<g/>
,	,	kIx,	,
mandlové	mandlový	k2eAgNnSc1d1	mandlové
mléko	mléko	k1gNnSc1	mléko
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
<g/>
,	,	kIx,	,
ty	ten	k3xDgInPc4	ten
však	však	k9	však
kromě	kromě	k7c2	kromě
barvy	barva	k1gFnSc2	barva
s	s	k7c7	s
(	(	kIx(	(
<g/>
kravským	kravský	k2eAgMnSc7d1	kravský
<g/>
)	)	kIx)	)
mlékem	mléko	k1gNnSc7	mléko
nemají	mít	k5eNaImIp3nP	mít
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
<g/>
.	.	kIx.	.
</s>
<s>
Mlezivo	mlezivo	k1gNnSc1	mlezivo
(	(	kIx(	(
<g/>
Colostrum	Colostrum	k1gNnSc1	Colostrum
<g/>
)	)	kIx)	)
Více	hodně	k6eAd2	hodně
<g/>
:	:	kIx,	:
Mlezivo	mlezivo	k1gNnSc1	mlezivo
Mlezivo	mlezivo	k1gNnSc1	mlezivo
neboli	neboli	k8xC	neboli
kolostrum	kolostrum	k1gNnSc1	kolostrum
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
nezralé	zralý	k2eNgNnSc1d1	nezralé
mléko	mléko	k1gNnSc1	mléko
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
první	první	k4xOgInSc1	první
výměšek	výměšek	k1gInSc1	výměšek
mléčné	mléčný	k2eAgFnSc2d1	mléčná
žlázy	žláza	k1gFnSc2	žláza
samice	samice	k1gFnSc2	samice
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
až	až	k8xS	až
dnů	den	k1gInPc2	den
po	po	k7c6	po
porodu	porod	k1gInSc6	porod
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
po	po	k7c4	po
kterou	který	k3yRgFnSc4	který
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc1	tento
mléko	mléko	k1gNnSc1	mléko
vyměšováno	vyměšován	k2eAgNnSc1d1	vyměšován
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
druhu	druh	k1gInSc6	druh
či	či	k8xC	či
plemeni	plemeno	k1gNnSc6	plemeno
zvířat	zvíře	k1gNnPc2	zvíře
(	(	kIx(	(
<g/>
člověka	člověk	k1gMnSc2	člověk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
zvýšeným	zvýšený	k2eAgInSc7d1	zvýšený
obsahem	obsah	k1gInSc7	obsah
imunoglobulinů	imunoglobulin	k1gInPc2	imunoglobulin
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yQgMnPc2	který
mládě	mládě	k1gNnSc1	mládě
získává	získávat	k5eAaImIp3nS	získávat
protilátky	protilátka	k1gFnPc4	protilátka
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
pasivní	pasivní	k2eAgFnSc1d1	pasivní
imunita	imunita	k1gFnSc1	imunita
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
i	i	k9	i
zvýšený	zvýšený	k2eAgInSc4d1	zvýšený
obsah	obsah	k1gInSc4	obsah
minerálních	minerální	k2eAgFnPc2d1	minerální
látek	látka	k1gFnPc2	látka
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
hořčíku	hořčík	k1gInSc2	hořčík
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
příznivě	příznivě	k6eAd1	příznivě
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
vyplavení	vyplavení	k1gNnSc4	vyplavení
střevní	střevní	k2eAgFnSc2d1	střevní
smolky	smolka	k1gFnSc2	smolka
<g/>
,	,	kIx,	,
a	a	k8xC	a
vitamínů	vitamín	k1gInPc2	vitamín
<g/>
.	.	kIx.	.
</s>
<s>
Lidské	lidský	k2eAgNnSc1d1	lidské
mateřské	mateřský	k2eAgNnSc1d1	mateřské
mléko	mléko	k1gNnSc1	mléko
je	být	k5eAaImIp3nS	být
podáváno	podáván	k2eAgNnSc1d1	podáváno
kojencům	kojenec	k1gMnPc3	kojenec
během	během	k7c2	během
kojení	kojení	k1gNnSc2	kojení
<g/>
,	,	kIx,	,
buď	buď	k8xC	buď
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
prsu	prs	k1gInSc2	prs
nebo	nebo	k8xC	nebo
vytlačené	vytlačený	k2eAgInPc4d1	vytlačený
a	a	k8xC	a
uložené	uložený	k2eAgInPc4d1	uložený
do	do	k7c2	do
zásoby	zásoba	k1gFnSc2	zásoba
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
mlezivo	mlezivo	k1gNnSc1	mlezivo
přenáší	přenášet	k5eAaImIp3nS	přenášet
z	z	k7c2	z
matky	matka	k1gFnSc2	matka
nejen	nejen	k6eAd1	nejen
živiny	živina	k1gFnSc2	živina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
protilátky	protilátka	k1gFnPc4	protilátka
<g/>
.	.	kIx.	.
</s>
<s>
Lidské	lidský	k2eAgNnSc1d1	lidské
mléko	mléko	k1gNnSc1	mléko
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
řídké	řídký	k2eAgNnSc1d1	řídké
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
vysoký	vysoký	k2eAgInSc4d1	vysoký
obsah	obsah	k1gInSc4	obsah
laktózy	laktóza	k1gFnSc2	laktóza
(	(	kIx(	(
<g/>
primární	primární	k2eAgInSc1d1	primární
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
cukr	cukr	k1gInSc4	cukr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kravské	kravský	k2eAgNnSc1d1	kravské
mléko	mléko	k1gNnSc1	mléko
Mléko	mléko	k1gNnSc4	mléko
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
svým	svůj	k3xOyFgInSc7	svůj
obsahem	obsah	k1gInSc7	obsah
tuku	tuk	k1gInSc2	tuk
<g/>
,	,	kIx,	,
bílkovin	bílkovina	k1gFnPc2	bílkovina
a	a	k8xC	a
ostatních	ostatní	k2eAgFnPc2d1	ostatní
látek	látka	k1gFnPc2	látka
podle	podle	k7c2	podle
plemenné	plemenný	k2eAgFnSc2d1	plemenná
příslušnosti	příslušnost	k1gFnSc2	příslušnost
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Kravské	kravský	k2eAgNnSc1d1	kravské
mléko	mléko	k1gNnSc1	mléko
má	mít	k5eAaImIp3nS	mít
nižší	nízký	k2eAgInSc4d2	nižší
obsah	obsah	k1gInSc4	obsah
cukru	cukr	k1gInSc2	cukr
(	(	kIx(	(
<g/>
4,5	[number]	k4	4,5
<g/>
–	–	k?	–
<g/>
5	[number]	k4	5
%	%	kIx~	%
laktózy	laktóza	k1gFnSc2	laktóza
<g/>
)	)	kIx)	)
a	a	k8xC	a
vyšší	vysoký	k2eAgInSc1d2	vyšší
obsah	obsah	k1gInSc1	obsah
bílkovin	bílkovina	k1gFnPc2	bílkovina
(	(	kIx(	(
<g/>
3,2	[number]	k4	3,2
<g/>
–	–	k?	–
<g/>
3,6	[number]	k4	3,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
asi	asi	k9	asi
3,5	[number]	k4	3,5
%	%	kIx~	%
<g/>
–	–	k?	–
<g/>
6,5	[number]	k4	6,5
%	%	kIx~	%
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
mléčného	mléčný	k2eAgInSc2d1	mléčný
tuku	tuk	k1gInSc2	tuk
<g/>
,	,	kIx,	,
8,5	[number]	k4	8,5
%	%	kIx~	%
<g/>
–	–	k?	–
<g/>
9,0	[number]	k4	9,0
%	%	kIx~	%
tukuprosté	tukuprostý	k2eAgFnSc2d1	tukuprostá
sušiny	sušina	k1gFnSc2	sušina
a	a	k8xC	a
asi	asi	k9	asi
88	[number]	k4	88
%	%	kIx~	%
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
hlavní	hlavní	k2eAgInSc1d1	hlavní
protein	protein	k1gInSc1	protein
(	(	kIx(	(
<g/>
80	[number]	k4	80
%	%	kIx~	%
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
sýrovina	sýrovina	k1gFnSc1	sýrovina
(	(	kIx(	(
<g/>
kasein	kasein	k1gInSc1	kasein
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
syrovátka	syrovátka	k1gFnSc1	syrovátka
tvoří	tvořit	k5eAaImIp3nS	tvořit
většinu	většina	k1gFnSc4	většina
zbytku	zbytek	k1gInSc2	zbytek
<g/>
.	.	kIx.	.
</s>
<s>
Kozí	kozí	k2eAgNnSc1d1	kozí
mléko	mléko	k1gNnSc1	mléko
Ovčí	ovčí	k2eAgNnSc1d1	ovčí
mléko	mléko	k1gNnSc1	mléko
Kobylí	kobylí	k2eAgNnSc1d1	kobylí
mléko	mléko	k1gNnSc1	mléko
Lamí	lamí	k2eAgNnSc1d1	lamí
mléko	mléko	k1gNnSc1	mléko
Velbloudí	velbloudí	k2eAgNnSc1d1	velbloudí
mléko	mléko	k1gNnSc4	mléko
Buvolí	buvolí	k2eAgNnSc4d1	buvolí
mléko	mléko	k1gNnSc4	mléko
Savčí	savčí	k2eAgMnPc1d1	savčí
kojenci	kojenec	k1gMnPc1	kojenec
produkují	produkovat	k5eAaImIp3nP	produkovat
enzym	enzym	k1gInSc4	enzym
laktázu	laktáza	k1gFnSc4	laktáza
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
štěpí	štěpit	k5eAaImIp3nS	štěpit
mléčný	mléčný	k2eAgInSc4d1	mléčný
cukr	cukr	k1gInSc4	cukr
laktózu	laktóza	k1gFnSc4	laktóza
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
to	ten	k3xDgNnSc1	ten
funguje	fungovat	k5eAaImIp3nS	fungovat
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
někteří	některý	k3yIgMnPc1	některý
dospělí	dospělý	k2eAgMnPc1d1	dospělý
lidští	lidský	k2eAgMnPc1d1	lidský
jedinci	jedinec	k1gMnPc1	jedinec
konzumují	konzumovat	k5eAaBmIp3nP	konzumovat
mléko	mléko	k1gNnSc4	mléko
jiných	jiný	k2eAgNnPc2d1	jiné
zvířat	zvíře	k1gNnPc2	zvíře
(	(	kIx(	(
<g/>
kravské	kravský	k2eAgFnPc1d1	kravská
<g/>
,	,	kIx,	,
kozí	kozí	k2eAgFnPc1d1	kozí
<g/>
,	,	kIx,	,
ovčí	ovčí	k2eAgFnPc1d1	ovčí
<g/>
,	,	kIx,	,
koňské	koňský	k2eAgFnPc1d1	koňská
<g/>
,	,	kIx,	,
lamí	lamí	k2eAgFnPc1d1	lamí
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tvorba	tvorba	k1gFnSc1	tvorba
laktázy	laktáza	k1gFnSc2	laktáza
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
v	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
klesá	klesat	k5eAaImIp3nS	klesat
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
etnického	etnický	k2eAgInSc2d1	etnický
původu	původ	k1gInSc2	původ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
míry	míra	k1gFnSc2	míra
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
cukr	cukr	k1gInSc1	cukr
laktóza	laktóza	k1gFnSc1	laktóza
stane	stanout	k5eAaPmIp3nS	stanout
nestravitelný	stravitelný	k2eNgInSc1d1	nestravitelný
<g/>
.	.	kIx.	.
</s>
<s>
Dotyčným	dotyčný	k2eAgMnSc7d1	dotyčný
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
určité	určitý	k2eAgFnSc2d1	určitá
dávky	dávka	k1gFnSc2	dávka
z	z	k7c2	z
mléka	mléko	k1gNnSc2	mléko
špatně	špatně	k6eAd1	špatně
nebo	nebo	k8xC	nebo
trpí	trpět	k5eAaImIp3nP	trpět
nadměrnou	nadměrný	k2eAgFnSc7d1	nadměrná
plynatostí	plynatost	k1gFnSc7	plynatost
a	a	k8xC	a
průjmy	průjem	k1gInPc7	průjem
<g/>
,	,	kIx,	,
trpí	trpět	k5eAaImIp3nS	trpět
zejména	zejména	k9	zejména
intolerancí	intolerance	k1gFnSc7	intolerance
laktózy	laktóza	k1gFnSc2	laktóza
–	–	k?	–
velikost	velikost	k1gFnSc1	velikost
dávky	dávka	k1gFnSc2	dávka
pak	pak	k6eAd1	pak
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
podílu	podíl	k1gInSc6	podíl
zkonzumovaného	zkonzumovaný	k2eAgNnSc2d1	zkonzumované
mléka	mléko	k1gNnSc2	mléko
na	na	k7c6	na
celé	celý	k2eAgFnSc6d1	celá
stravě	strava	k1gFnSc6	strava
a	a	k8xC	a
osobních	osobní	k2eAgFnPc6d1	osobní
dispozicích	dispozice	k1gFnPc6	dispozice
<g/>
.	.	kIx.	.
</s>
<s>
Mléko	mléko	k1gNnSc1	mléko
se	se	k3xPyFc4	se
nejvíce	nejvíce	k6eAd1	nejvíce
konzumuje	konzumovat	k5eAaBmIp3nS	konzumovat
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
pak	pak	k6eAd1	pak
méně	málo	k6eAd2	málo
a	a	k8xC	a
u	u	k7c2	u
obyvatel	obyvatel	k1gMnPc2	obyvatel
Afriky	Afrika	k1gFnSc2	Afrika
je	být	k5eAaImIp3nS	být
schopnost	schopnost	k1gFnSc1	schopnost
trávit	trávit	k5eAaImF	trávit
mléko	mléko	k1gNnSc4	mléko
nejmenší	malý	k2eAgNnSc4d3	nejmenší
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nemohou	moct	k5eNaImIp3nP	moct
trávit	trávit	k5eAaImF	trávit
mléko	mléko	k1gNnSc4	mléko
s	s	k7c7	s
obsahem	obsah	k1gInSc7	obsah
laktózy	laktóza	k1gFnSc2	laktóza
mají	mít	k5eAaImIp3nP	mít
tzv.	tzv.	kA	tzv.
laktózovou	laktózový	k2eAgFnSc4d1	laktózová
intoleranci	intolerance	k1gFnSc4	intolerance
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
původnímu	původní	k2eAgInSc3d1	původní
účelu	účel	k1gInSc3	účel
mléka	mléko	k1gNnSc2	mléko
není	být	k5eNaImIp3nS	být
třeba	třeba	k6eAd1	třeba
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
onemocnění	onemocnění	k1gNnSc4	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Intolerance	intolerance	k1gFnSc1	intolerance
na	na	k7c4	na
laktózu	laktóza	k1gFnSc4	laktóza
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
objevit	objevit	k5eAaPmF	objevit
i	i	k9	i
kdykoliv	kdykoliv	k6eAd1	kdykoliv
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
věku	věk	k1gInSc2	věk
v	v	k7c6	v
případě	případ	k1gInSc6	případ
vysazení	vysazení	k1gNnSc2	vysazení
mléka	mléko	k1gNnSc2	mléko
na	na	k7c4	na
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Mléko	mléko	k1gNnSc1	mléko
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
lidskou	lidský	k2eAgFnSc4d1	lidská
výživu	výživa	k1gFnSc4	výživa
upravuje	upravovat	k5eAaImIp3nS	upravovat
klasickou	klasický	k2eAgFnSc7d1	klasická
pasterizací	pasterizace	k1gFnSc7	pasterizace
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
mléka	mléko	k1gNnSc2	mléko
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
zahřátí	zahřátí	k1gNnSc4	zahřátí
na	na	k7c4	na
cca	cca	kA	cca
61,5	[number]	k4	61,5
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
mléko	mléko	k1gNnSc1	mléko
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
vhodné	vhodný	k2eAgNnSc1d1	vhodné
pro	pro	k7c4	pro
krátkodobé	krátkodobý	k2eAgNnSc4d1	krátkodobé
skladování	skladování	k1gNnSc4	skladování
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
ultravysokým	ultravysoký	k2eAgInSc7d1	ultravysoký
záhřevem	záhřev	k1gInSc7	záhřev
UHT	UHT	kA	UHT
(	(	kIx(	(
<g/>
částečná	částečný	k2eAgFnSc1d1	částečná
chemická	chemický	k2eAgFnSc1d1	chemická
změna	změna	k1gFnSc1	změna
<g/>
,	,	kIx,	,
při	při	k7c6	při
aseptickém	aseptický	k2eAgNnSc6d1	aseptické
balení	balení	k1gNnSc6	balení
možnost	možnost	k1gFnSc4	možnost
dlouhodobého	dlouhodobý	k2eAgNnSc2d1	dlouhodobé
uchovávání	uchovávání	k1gNnSc2	uchovávání
při	při	k7c6	při
pokojové	pokojový	k2eAgFnSc6d1	pokojová
teplotě	teplota	k1gFnSc6	teplota
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
prodloužení	prodloužení	k1gNnSc2	prodloužení
trvanlivosti	trvanlivost	k1gFnSc2	trvanlivost
dochází	docházet	k5eAaImIp3nS	docházet
i	i	k9	i
k	k	k7c3	k
ničení	ničení	k1gNnSc3	ničení
patogenů	patogen	k1gInPc2	patogen
(	(	kIx(	(
<g/>
proti	proti	k7c3	proti
nemocem	nemoc	k1gFnPc3	nemoc
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mléko	mléko	k1gNnSc1	mléko
se	se	k3xPyFc4	se
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
na	na	k7c4	na
mléčné	mléčný	k2eAgInPc4d1	mléčný
výrobky	výrobek	k1gInPc4	výrobek
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
smetana	smetana	k1gFnSc1	smetana
<g/>
,	,	kIx,	,
máslo	máslo	k1gNnSc1	máslo
<g/>
,	,	kIx,	,
jogurt	jogurt	k1gInSc1	jogurt
<g/>
,	,	kIx,	,
sýr	sýr	k1gInSc1	sýr
<g/>
,	,	kIx,	,
syrovátka	syrovátka	k1gFnSc1	syrovátka
<g/>
,	,	kIx,	,
laktóza	laktóza	k1gFnSc1	laktóza
<g/>
,	,	kIx,	,
sušené	sušený	k2eAgNnSc1d1	sušené
mléko	mléko	k1gNnSc1	mléko
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
jiných	jiný	k2eAgFnPc2d1	jiná
potravinových	potravinový	k2eAgFnPc2d1	potravinová
přísad	přísada	k1gFnPc2	přísada
a	a	k8xC	a
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
produktů	produkt	k1gInPc2	produkt
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
mléku	mléko	k1gNnSc6	mléko
je	být	k5eAaImIp3nS	být
rozšířena	rozšířen	k2eAgFnSc1d1	rozšířena
pověra	pověra	k1gFnSc1	pověra
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
konzumace	konzumace	k1gFnSc1	konzumace
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
sekreci	sekrece	k1gFnSc4	sekrece
hlenu	hlen	k1gInSc2	hlen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
dotčená	dotčený	k2eAgFnSc1d1	dotčená
osoba	osoba	k1gFnSc1	osoba
není	být	k5eNaImIp3nS	být
alergická	alergický	k2eAgFnSc1d1	alergická
na	na	k7c4	na
mléčné	mléčný	k2eAgInPc4d1	mléčný
produkty	produkt	k1gInPc4	produkt
<g/>
,	,	kIx,	,
vědecké	vědecký	k2eAgFnPc4d1	vědecká
studie	studie	k1gFnPc4	studie
dokazují	dokazovat	k5eAaImIp3nP	dokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
efekt	efekt	k1gInSc1	efekt
je	být	k5eAaImIp3nS	být
smyšlený	smyšlený	k2eAgInSc1d1	smyšlený
<g/>
.	.	kIx.	.
mateřské	mateřský	k2eAgNnSc1d1	mateřské
mléko	mléko	k1gNnSc1	mléko
kravské	kravský	k2eAgNnSc1d1	kravské
mléko	mléko	k1gNnSc1	mléko
kozí	kozí	k2eAgNnSc1d1	kozí
mléko	mléko	k1gNnSc1	mléko
kokosové	kokosový	k2eAgNnSc1d1	kokosové
mléko	mléko	k1gNnSc1	mléko
sójové	sójový	k2eAgNnSc1d1	sójové
mléko	mléko	k1gNnSc1	mléko
rýžové	rýžový	k2eAgNnSc1d1	rýžové
mléko	mléko	k1gNnSc1	mléko
mandlové	mandlový	k2eAgNnSc1d1	mandlové
mléko	mléko	k1gNnSc1	mléko
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
mléko	mléko	k1gNnSc4	mléko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
mléko	mléko	k1gNnSc4	mléko
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
www.mlieko.sk	www.mlieko.sk	k1gInSc1	www.mlieko.sk
</s>
