<s>
Savci	savec	k1gMnPc1
konzumují	konzumovat	k5eAaBmIp3nP
mléko	mléko	k1gNnSc4
až	až	k9
do	do	k7c2
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
dokud	dokud	k8xS
nejsou	být	k5eNaImIp3nP
schopni	schopen	k2eAgMnPc1d1
trávit	trávit	k5eAaImF
pevnou	pevný	k2eAgFnSc4d1
stravu	strava	k1gFnSc4
(	(	kIx(
<g/>
píce	píce	k1gFnSc1
<g/>
,	,	kIx,
maso	maso	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
dospělosti	dospělost	k1gFnSc6
zpravidla	zpravidla	k6eAd1
o	o	k7c4
schopnost	schopnost	k1gFnSc4
zužitkovat	zužitkovat	k5eAaPmF
bílkoviny	bílkovina	k1gFnPc4
z	z	k7c2
mléka	mléko	k1gNnSc2
přicházejí	přicházet	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>