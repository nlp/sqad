<s>
Mnoho	mnoho	k4c1	mnoho
sloučenin	sloučenina	k1gFnPc2	sloučenina
astatu	astat	k1gInSc2	astat
bylo	být	k5eAaImAgNnS	být
připraveno	připravit	k5eAaPmNgNnS	připravit
a	a	k8xC	a
studováno	studovat	k5eAaImNgNnS	studovat
v	v	k7c6	v
mikroskopickém	mikroskopický	k2eAgNnSc6d1	mikroskopické
množství	množství	k1gNnSc6	množství
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
rozpadají	rozpadat	k5eAaImIp3nP	rozpadat
vlivem	vliv	k1gInSc7	vliv
radioaktivity	radioaktivita	k1gFnSc2	radioaktivita
<g/>
.	.	kIx.	.
</s>
