<s>
Archangelsk	Archangelsk	k1gInSc1	Archangelsk
(	(	kIx(	(
<g/>
А	А	k?	А
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
přístav	přístav	k1gInSc1	přístav
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
severu	sever	k1gInSc6	sever
evropské	evropský	k2eAgFnSc2d1	Evropská
části	část	k1gFnSc2	část
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
administrativní	administrativní	k2eAgNnSc1d1	administrativní
centrum	centrum	k1gNnSc1	centrum
Archangelské	archangelský	k2eAgFnSc2d1	Archangelská
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
při	při	k7c6	při
ústí	ústí	k1gNnSc6	ústí
řeky	řeka	k1gFnSc2	řeka
Severní	severní	k2eAgFnSc2d1	severní
Dviny	Dvina	k1gFnSc2	Dvina
do	do	k7c2	do
Dvinského	Dvinský	k2eAgInSc2d1	Dvinský
zálivu	záliv	k1gInSc2	záliv
Bílého	bílý	k2eAgNnSc2d1	bílé
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
348	[number]	k4	348
716	[number]	k4	716
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
sčítání	sčítání	k1gNnSc1	sčítání
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc7d3	nejstarší
zmínkou	zmínka	k1gFnSc7	zmínka
o	o	k7c6	o
místním	místní	k2eAgNnSc6d1	místní
osídlení	osídlení	k1gNnSc6	osídlení
je	být	k5eAaImIp3nS	být
záznam	záznam	k1gInSc1	záznam
z	z	k7c2	z
kroniky	kronika	k1gFnSc2	kronika
z	z	k7c2	z
r.	r.	kA	r.
1419	[number]	k4	1419
o	o	k7c6	o
klášteru	klášter	k1gInSc3	klášter
archanděla	archanděl	k1gMnSc2	archanděl
Michala	Michal	k1gMnSc2	Michal
<g/>
,	,	kIx,	,
ležícím	ležící	k2eAgNnSc6d1	ležící
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
Severní	severní	k2eAgFnSc2d1	severní
Dviny	Dvina	k1gFnSc2	Dvina
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
pověsti	pověst	k1gFnPc1	pověst
kladou	klást	k5eAaImIp3nP	klást
vznik	vznik	k1gInSc4	vznik
tohoto	tento	k3xDgInSc2	tento
kláštera	klášter	k1gInSc2	klášter
do	do	k7c2	do
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Anglický	anglický	k2eAgMnSc1d1	anglický
mořeplavec	mořeplavec	k1gMnSc1	mořeplavec
Richard	Richard	k1gMnSc1	Richard
Chancellor	Chancellor	k1gMnSc1	Chancellor
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
r.	r.	kA	r.
1533	[number]	k4	1533
připlul	připlout	k5eAaPmAgMnS	připlout
do	do	k7c2	do
Severodvinsku	Severodvinsko	k1gNnSc3	Severodvinsko
<g/>
,	,	kIx,	,
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Archangelska	Archangelsko	k1gNnSc2	Archangelsko
trhovou	trhový	k2eAgFnSc4d1	trhová
osadu	osada	k1gFnSc4	osada
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1583	[number]	k4	1583
vydal	vydat	k5eAaPmAgMnS	vydat
car	car	k1gMnSc1	car
Ivan	Ivan	k1gMnSc1	Ivan
Hrozný	hrozný	k2eAgInSc4d1	hrozný
příkaz	příkaz	k1gInSc4	příkaz
k	k	k7c3	k
vybudování	vybudování	k1gNnSc3	vybudování
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
pak	pak	k6eAd1	pak
bylo	být	k5eAaImAgNnS	být
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
skutečně	skutečně	k6eAd1	skutečně
založeno	založit	k5eAaPmNgNnS	založit
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
úspěšně	úspěšně	k6eAd1	úspěšně
rozvíjelo	rozvíjet	k5eAaImAgNnS	rozvíjet
a	a	k8xC	a
až	až	k6eAd1	až
do	do	k7c2	do
založení	založení	k1gNnSc2	založení
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
bylo	být	k5eAaImAgNnS	být
nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
ruským	ruský	k2eAgInSc7d1	ruský
námořním	námořní	k2eAgInSc7d1	námořní
přístavem	přístav	k1gInSc7	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
poté	poté	k6eAd1	poté
však	však	k9	však
jeho	on	k3xPp3gInSc4	on
rozvoj	rozvoj	k1gInSc4	rozvoj
neustal	ustat	k5eNaPmAgMnS	ustat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1916	[number]	k4	1916
až	až	k9	až
2004	[number]	k4	2004
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
provozována	provozován	k2eAgFnSc1d1	provozována
nejsevernější	severní	k2eAgFnSc1d3	nejsevernější
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
udává	udávat	k5eAaImIp3nS	udávat
následující	následující	k2eAgInSc1d1	následující
graf	graf	k1gInSc1	graf
<g/>
.	.	kIx.	.
</s>
<s>
Archangelsk	Archangelsk	k1gInSc1	Archangelsk
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgInPc2d3	nejdůležitější
dopravních	dopravní	k2eAgInPc2d1	dopravní
uzlů	uzel	k1gInPc2	uzel
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
tři	tři	k4xCgInPc1	tři
přístavy	přístav	k1gInPc1	přístav
<g/>
,	,	kIx,	,
dva	dva	k4xCgMnPc1	dva
námořní	námořní	k2eAgMnPc1d1	námořní
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
říční	říční	k2eAgMnSc1d1	říční
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc1	jejichž
výkon	výkon	k1gInSc4	výkon
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
r.	r.	kA	r.
2010	[number]	k4	2010
4,5	[number]	k4	4,5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
t.	t.	k?	t.
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
končí	končit	k5eAaImIp3nS	končit
Severní	severní	k2eAgFnSc1d1	severní
železnice	železnice	k1gFnSc1	železnice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ho	on	k3xPp3gMnSc4	on
spojuje	spojovat	k5eAaImIp3nS	spojovat
s	s	k7c7	s
ruskou	ruský	k2eAgFnSc7d1	ruská
železniční	železniční	k2eAgFnSc7d1	železniční
sítí	síť	k1gFnSc7	síť
<g/>
,	,	kIx,	,
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
čtyři	čtyři	k4xCgNnPc4	čtyři
nádraží	nádraží	k1gNnPc4	nádraží
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgNnSc7d1	jediné
silničním	silniční	k2eAgNnSc7d1	silniční
spojením	spojení	k1gNnSc7	spojení
je	být	k5eAaImIp3nS	být
federální	federální	k2eAgFnSc1d1	federální
silnice	silnice	k1gFnSc1	silnice
M8	M8	k1gFnSc2	M8
Cholmogory	Cholmogora	k1gFnSc2	Cholmogora
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vede	vést	k5eAaImIp3nS	vést
z	z	k7c2	z
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
Veřejná	veřejný	k2eAgFnSc1d1	veřejná
městská	městský	k2eAgFnSc1d1	městská
doprava	doprava	k1gFnSc1	doprava
je	být	k5eAaImIp3nS	být
provozována	provozován	k2eAgFnSc1d1	provozována
autobusy	autobus	k1gInPc7	autobus
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	nedaleko	k7c2	nedaleko
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
dvě	dva	k4xCgNnPc1	dva
civilní	civilní	k2eAgNnPc1d1	civilní
letiště	letiště	k1gNnPc1	letiště
<g/>
:	:	kIx,	:
větší	veliký	k2eAgFnSc1d2	veliký
Talagi	Talag	k1gFnPc1	Talag
(	(	kIx(	(
<g/>
IATA	IATA	kA	IATA
<g/>
:	:	kIx,	:
ARH	ARH	kA	ARH
<g/>
,	,	kIx,	,
ICAO	ICAO	kA	ICAO
<g/>
:	:	kIx,	:
ULAA	ULAA	kA	ULAA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
menší	malý	k2eAgNnSc1d2	menší
Vaskovo	Vaskův	k2eAgNnSc1d1	Vaskovo
(	(	kIx(	(
<g/>
ICAO	ICAO	kA	ICAO
<g/>
:	:	kIx,	:
ULAH	ULAH	kA	ULAH
<g/>
)	)	kIx)	)
a	a	k8xC	a
vojenská	vojenský	k2eAgFnSc1d1	vojenská
letecká	letecký	k2eAgFnSc1d1	letecká
základna	základna	k1gFnSc1	základna
Lachta	Lachta	k1gFnSc1	Lachta
<g/>
.	.	kIx.	.
180	[number]	k4	180
km	km	kA	km
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Archangelska	Archangelsk	k1gInSc2	Archangelsk
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
kosmodrom	kosmodrom	k1gInSc1	kosmodrom
Pleseck	Plesecka	k1gFnPc2	Plesecka
<g/>
.	.	kIx.	.
</s>
