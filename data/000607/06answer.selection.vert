<s>
Archangelsk	Archangelsk	k1gInSc1	Archangelsk
(	(	kIx(	(
<g/>
А	А	k?	А
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
přístav	přístav	k1gInSc1	přístav
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
severu	sever	k1gInSc6	sever
evropské	evropský	k2eAgFnSc2d1	Evropská
části	část	k1gFnSc2	část
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
administrativní	administrativní	k2eAgNnSc1d1	administrativní
centrum	centrum	k1gNnSc1	centrum
Archangelské	archangelský	k2eAgFnSc2d1	Archangelská
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
