<s>
Byl	být	k5eAaImAgInS	být
založen	založen	k2eAgInSc1d1	založen
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
Ivem	Ivo	k1gMnSc7	Ivo
Lukačovičem	Lukačovič	k1gMnSc7	Lukačovič
<g/>
,	,	kIx,	,
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
českých	český	k2eAgInPc2d1	český
internetových	internetový	k2eAgInPc2d1	internetový
katalogů	katalog	k1gInPc2	katalog
a	a	k8xC	a
vyhledávačů	vyhledávač	k1gMnPc2	vyhledávač
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
