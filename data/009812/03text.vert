<p>
<s>
Arizona	Arizona	k1gFnSc1	Arizona
(	(	kIx(	(
<g/>
anglická	anglický	k2eAgFnSc1d1	anglická
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
ɛ	ɛ	k?	ɛ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
nebo	nebo	k8xC	nebo
[	[	kIx(	[
<g/>
æ	æ	k?	æ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
State	status	k1gInSc5	status
of	of	k?	of
Arizona	Arizona	k1gFnSc1	Arizona
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k1gInSc4	stát
nacházející	nacházející	k2eAgInSc4d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
horských	horský	k2eAgInPc2d1	horský
států	stát	k1gInPc2	stát
v	v	k7c6	v
západním	západní	k2eAgInSc6d1	západní
regionu	region	k1gInSc6	region
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Arizona	Arizona	k1gFnSc1	Arizona
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
států	stát	k1gInPc2	stát
Čtyř	čtyři	k4xCgInPc2	čtyři
rohů	roh	k1gInPc2	roh
<g/>
,	,	kIx,	,
hraničí	hraničit	k5eAaImIp3nS	hraničit
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Novým	nový	k2eAgNnSc7d1	nové
Mexikem	Mexiko	k1gNnSc7	Mexiko
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
Utahem	Utah	k1gInSc7	Utah
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
s	s	k7c7	s
Nevadou	Nevada	k1gFnSc7	Nevada
a	a	k8xC	a
Kalifornií	Kalifornie	k1gFnSc7	Kalifornie
a	a	k8xC	a
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
mexickými	mexický	k2eAgInPc7d1	mexický
státy	stát	k1gInPc7	stát
Baja	Baj	k1gInSc2	Baj
California	Californium	k1gNnSc2	Californium
a	a	k8xC	a
Sonora	sonora	k1gFnSc1	sonora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rozlohou	rozloha	k1gFnSc7	rozloha
295	[number]	k4	295
234	[number]	k4	234
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
Arizona	Arizona	k1gFnSc1	Arizona
šestým	šestý	k4xOgInSc7	šestý
největším	veliký	k2eAgInSc7d3	veliký
státem	stát	k1gInSc7	stát
USA	USA	kA	USA
<g/>
,	,	kIx,	,
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
6,9	[number]	k4	6,9
milionů	milion	k4xCgInPc2	milion
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
14	[number]	k4	14
<g/>
.	.	kIx.	.
nejlidnatějším	lidnatý	k2eAgInSc7d3	nejlidnatější
státem	stát	k1gInSc7	stát
a	a	k8xC	a
s	s	k7c7	s
hodnotou	hodnota	k1gFnSc7	hodnota
hustoty	hustota	k1gFnSc2	hustota
zalidnění	zalidnění	k1gNnSc2	zalidnění
24	[number]	k4	24
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c6	na
km2	km2	k4	km2
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
33	[number]	k4	33
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
a	a	k8xC	a
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Phoenix	Phoenix	k1gInSc1	Phoenix
se	s	k7c7	s
1,5	[number]	k4	1,5
milionem	milion	k4xCgInSc7	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
největšími	veliký	k2eAgMnPc7d3	veliký
městy	město	k1gNnPc7	město
jsou	být	k5eAaImIp3nP	být
Tucson	Tucson	k1gNnSc1	Tucson
(	(	kIx(	(
<g/>
530	[number]	k4	530
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mesa	Mesa	k1gMnSc1	Mesa
(	(	kIx(	(
<g/>
470	[number]	k4	470
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Chandler	Chandler	k1gMnSc1	Chandler
(	(	kIx(	(
<g/>
260	[number]	k4	260
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gilbert	Gilbert	k1gMnSc1	Gilbert
(	(	kIx(	(
<g/>
250	[number]	k4	250
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Glendale	Glendala	k1gFnSc6	Glendala
(	(	kIx(	(
<g/>
240	[number]	k4	240
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
Scottsdale	Scottsdala	k1gFnSc6	Scottsdala
(	(	kIx(	(
<g/>
240	[number]	k4	240
tisíc	tisíc	k4xCgInSc4	tisíc
obyv	obyvum	k1gNnPc2	obyvum
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
vrchol	vrchol	k1gInSc1	vrchol
Humphreys	Humphreysa	k1gFnPc2	Humphreysa
Peak	Peak	k1gInSc4	Peak
s	s	k7c7	s
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
3852	[number]	k4	3852
m	m	kA	m
v	v	k7c6	v
pohoří	pohoří	k1gNnSc6	pohoří
San	San	k1gFnSc2	San
Francisco	Francisco	k6eAd1	Francisco
Peaks	Peaks	k1gInSc4	Peaks
<g/>
.	.	kIx.	.
</s>
<s>
Největšími	veliký	k2eAgInPc7d3	veliký
toky	tok	k1gInPc7	tok
řeky	řeka	k1gFnSc2	řeka
Colorado	Colorado	k1gNnSc1	Colorado
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
tvoří	tvořit	k5eAaImIp3nP	tvořit
hranici	hranice	k1gFnSc4	hranice
s	s	k7c7	s
Kalifornií	Kalifornie	k1gFnSc7	Kalifornie
a	a	k8xC	a
část	část	k1gFnSc1	část
hranice	hranice	k1gFnSc1	hranice
s	s	k7c7	s
Nevadou	Nevada	k1gFnSc7	Nevada
<g/>
,	,	kIx,	,
a	a	k8xC	a
Gila	Gila	k1gFnSc1	Gila
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Arizony	Arizona	k1gFnSc2	Arizona
se	se	k3xPyFc4	se
první	první	k4xOgMnPc1	první
španělští	španělský	k2eAgMnPc1d1	španělský
průzkumníci	průzkumník	k1gMnPc1	průzkumník
objevili	objevit	k5eAaPmAgMnP	objevit
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
30	[number]	k4	30
<g/>
.	.	kIx.	.
a	a	k8xC	a
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jezuitští	jezuitský	k2eAgMnPc1d1	jezuitský
misionáři	misionář	k1gMnPc1	misionář
dorazili	dorazit	k5eAaPmAgMnP	dorazit
do	do	k7c2	do
regionu	region	k1gInSc2	region
ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
bylo	být	k5eAaImAgNnS	být
součástí	součást	k1gFnSc7	součást
místokrálovství	místokrálovství	k1gNnSc2	místokrálovství
Nové	Nové	k2eAgNnSc1d1	Nové
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
provincie	provincie	k1gFnSc2	provincie
Horní	horní	k2eAgFnSc2d1	horní
Kalifornie	Kalifornie	k1gFnSc2	Kalifornie
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1821	[number]	k4	1821
stala	stát	k5eAaPmAgFnS	stát
částí	část	k1gFnSc7	část
samostatného	samostatný	k2eAgNnSc2d1	samostatné
Mexika	Mexiko	k1gNnSc2	Mexiko
a	a	k8xC	a
od	od	k7c2	od
něj	on	k3xPp3gMnSc2	on
získaly	získat	k5eAaPmAgFnP	získat
region	region	k1gInSc4	region
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1848	[number]	k4	1848
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
výsledku	výsledek	k1gInSc2	výsledek
mexicko-americké	mexickomerický	k2eAgFnSc2d1	mexicko-americká
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
se	se	k3xPyFc4	se
oblast	oblast	k1gFnSc1	oblast
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
nově	nově	k6eAd1	nově
založeného	založený	k2eAgNnSc2d1	založené
novomexického	novomexický	k2eAgNnSc2d1	novomexický
teritoria	teritorium	k1gNnSc2	teritorium
<g/>
,	,	kIx,	,
nejjižnější	jižní	k2eAgFnSc4d3	nejjižnější
část	část	k1gFnSc4	část
regionu	region	k1gInSc2	region
pořídily	pořídit	k5eAaPmAgFnP	pořídit
USA	USA	kA	USA
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
tzv.	tzv.	kA	tzv.
Gadsdenovou	Gadsdenový	k2eAgFnSc7d1	Gadsdenový
koupí	koupě	k1gFnSc7	koupě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1863	[number]	k4	1863
bylo	být	k5eAaImAgNnS	být
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
vlastní	vlastní	k2eAgNnSc1d1	vlastní
arizonské	arizonský	k2eAgNnSc1d1	arizonské
teritorium	teritorium	k1gNnSc1	teritorium
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
získalo	získat	k5eAaPmAgNnS	získat
svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
ze	z	k7c2	z
starého	starý	k2eAgNnSc2d1	staré
španělského	španělský	k2eAgNnSc2d1	španělské
jména	jméno	k1gNnSc2	jméno
Arizonac	Arizonac	k1gFnSc1	Arizonac
(	(	kIx(	(
<g/>
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
z	z	k7c2	z
o	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
odhamského	odhamský	k2eAgInSc2d1	odhamský
výrazu	výraz	k1gInSc2	výraz
"	"	kIx"	"
<g/>
alĭ	alĭ	k?	alĭ
ṣ	ṣ	k?	ṣ
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
malý	malý	k2eAgInSc4d1	malý
pramen	pramen	k1gInSc4	pramen
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
označena	označen	k2eAgFnSc1d1	označena
oblast	oblast	k1gFnSc1	oblast
v	v	k7c6	v
Sonoře	sonora	k1gFnSc6	sonora
<g/>
.	.	kIx.	.
</s>
<s>
Arizona	Arizona	k1gFnSc1	Arizona
se	s	k7c7	s
14	[number]	k4	14
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
poslední	poslední	k2eAgNnSc4d1	poslední
území	území	k1gNnSc4	území
kontinentálních	kontinentální	k2eAgInPc2d1	kontinentální
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
stala	stát	k5eAaPmAgFnS	stát
48	[number]	k4	48
<g/>
.	.	kIx.	.
státem	stát	k1gInSc7	stát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Arizona	Arizona	k1gFnSc1	Arizona
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
svým	svůj	k3xOyFgNnSc7	svůj
pouštním	pouštní	k2eAgNnSc7d1	pouštní
klimatem	klima	k1gNnSc7	klima
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
horkými	horký	k2eAgNnPc7d1	horké
léty	léto	k1gNnPc7	léto
a	a	k8xC	a
mírnými	mírný	k2eAgFnPc7d1	mírná
zimami	zima	k1gFnPc7	zima
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
polohách	poloha	k1gFnPc6	poloha
jsou	být	k5eAaImIp3nP	být
borovicové	borovicový	k2eAgInPc1d1	borovicový
lesy	les	k1gInPc1	les
a	a	k8xC	a
horské	horský	k2eAgFnPc1d1	horská
louky	louka	k1gFnPc1	louka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
kontrastují	kontrastovat	k5eAaImIp3nP	kontrastovat
s	s	k7c7	s
pouštěmi	poušť	k1gFnPc7	poušť
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
státě	stát	k1gInSc6	stát
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mnoho	mnoho	k4c1	mnoho
národních	národní	k2eAgInPc2d1	národní
parků	park	k1gInPc2	park
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Grand	grand	k1gMnSc1	grand
Canyonu	Canyona	k1gFnSc4	Canyona
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lesů	les	k1gInPc2	les
<g/>
,	,	kIx,	,
monumentů	monument	k1gInPc2	monument
a	a	k8xC	a
indiánských	indiánský	k2eAgFnPc2d1	indiánská
rezervací	rezervace	k1gFnPc2	rezervace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Arizona	Arizona	k1gFnSc1	Arizona
je	být	k5eAaImIp3nS	být
šestý	šestý	k4xOgInSc1	šestý
největší	veliký	k2eAgInSc1d3	veliký
stát	stát	k1gInSc1	stát
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
15	[number]	k4	15
%	%	kIx~	%
z	z	k7c2	z
celkové	celkový	k2eAgFnSc2d1	celková
rozlohy	rozloha	k1gFnSc2	rozloha
(	(	kIx(	(
<g/>
306	[number]	k4	306
000	[number]	k4	000
km2	km2	k4	km2
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
soukromém	soukromý	k2eAgNnSc6d1	soukromé
vlastnictví	vlastnictví	k1gNnSc6	vlastnictví
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc4	zbytek
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
veřejné	veřejný	k2eAgInPc1d1	veřejný
lesy	les	k1gInPc1	les
a	a	k8xC	a
parky	park	k1gInPc1	park
<g/>
,	,	kIx,	,
rekreační	rekreační	k2eAgInPc1d1	rekreační
zóny	zóna	k1gFnPc4	zóna
a	a	k8xC	a
americké	americký	k2eAgFnPc4d1	americká
přírodní	přírodní	k2eAgFnPc4d1	přírodní
rezervace	rezervace	k1gFnPc4	rezervace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Arizona	Arizona	k1gFnSc1	Arizona
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
pro	pro	k7c4	pro
své	svůj	k3xOyFgFnPc4	svůj
pouště	poušť	k1gFnPc4	poušť
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
suchomilných	suchomilný	k2eAgFnPc2d1	suchomilná
rostlin	rostlina	k1gFnPc2	rostlina
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
kaktusy	kaktus	k1gInPc1	kaktus
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
svým	svůj	k3xOyFgNnSc7	svůj
klimatem	klima	k1gNnSc7	klima
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
horkými	horký	k2eAgNnPc7d1	horké
léty	léto	k1gNnPc7	léto
a	a	k8xC	a
mírnými	mírný	k2eAgFnPc7d1	mírná
zimami	zima	k1gFnPc7	zima
<g/>
.	.	kIx.	.
</s>
<s>
Méně	málo	k6eAd2	málo
známé	známý	k2eAgFnPc1d1	známá
jsou	být	k5eAaImIp3nP	být
už	už	k9	už
borové	borový	k2eAgInPc1d1	borový
lesy	les	k1gInPc1	les
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
státu	stát	k1gInSc2	stát
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
známé	známý	k2eAgNnSc1d1	známé
jako	jako	k8xS	jako
Colorado	Colorado	k1gNnSc1	Colorado
Plateau	Plateaus	k1gInSc2	Plateaus
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
ostře	ostro	k6eAd1	ostro
kontrastují	kontrastovat	k5eAaImIp3nP	kontrastovat
s	s	k7c7	s
pouštěmi	poušť	k1gFnPc7	poušť
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
atrakcí	atrakce	k1gFnSc7	atrakce
státu	stát	k1gInSc2	stát
Arizona	Arizona	k1gFnSc1	Arizona
je	být	k5eAaImIp3nS	být
Grand	grand	k1gMnSc1	grand
Canyon	Canyon	k1gMnSc1	Canyon
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
řekou	řeka	k1gFnSc7	řeka
Colorado	Colorado	k1gNnSc1	Colorado
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
446	[number]	k4	446
km	km	kA	km
dlouhý	dlouhý	k2eAgMnSc1d1	dlouhý
<g/>
,	,	kIx,	,
v	v	k7c6	v
nejužší	úzký	k2eAgFnSc6d3	nejužší
části	část	k1gFnSc6	část
6	[number]	k4	6
km	km	kA	km
a	a	k8xC	a
v	v	k7c6	v
nejširší	široký	k2eAgFnSc6d3	nejširší
části	část	k1gFnSc6	část
29	[number]	k4	29
km	km	kA	km
široký	široký	k2eAgMnSc1d1	široký
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
2	[number]	k4	2
km	km	kA	km
hluboký	hluboký	k2eAgInSc4d1	hluboký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Arizoně	Arizona	k1gFnSc6	Arizona
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejzachovalejších	zachovalý	k2eAgInPc2d3	nejzachovalejší
kráterů	kráter	k1gInPc2	kráter
vzniklých	vzniklý	k2eAgInPc2d1	vzniklý
dopadem	dopad	k1gInSc7	dopad
meteoritu	meteorit	k1gInSc2	meteorit
<g/>
.	.	kIx.	.
</s>
<s>
Meteor	meteor	k1gInSc1	meteor
Crater	Crater	k1gInSc1	Crater
(	(	kIx(	(
<g/>
též	též	k9	též
Barringerův	Barringerův	k2eAgInSc1d1	Barringerův
kráter	kráter	k1gInSc1	kráter
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
uprostřed	uprostřed	k7c2	uprostřed
plošiny	plošina	k1gFnSc2	plošina
Colorado	Colorado	k1gNnSc1	Colorado
Plateau	Plateaus	k1gInSc2	Plateaus
<g/>
,	,	kIx,	,
asi	asi	k9	asi
40	[number]	k4	40
km	km	kA	km
západně	západně	k6eAd1	západně
od	od	k7c2	od
města	město	k1gNnSc2	město
Winslow	Winslow	k1gFnSc2	Winslow
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
vyvýšený	vyvýšený	k2eAgInSc1d1	vyvýšený
okraj	okraj	k1gInSc1	okraj
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
46	[number]	k4	46
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
samotný	samotný	k2eAgInSc1d1	samotný
kráter	kráter	k1gInSc1	kráter
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
174	[number]	k4	174
metrů	metr	k1gInPc2	metr
hluboký	hluboký	k2eAgMnSc1d1	hluboký
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
asi	asi	k9	asi
1200	[number]	k4	1200
metrů	metr	k1gInPc2	metr
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klimatické	klimatický	k2eAgFnPc1d1	klimatická
podmínky	podmínka	k1gFnPc1	podmínka
==	==	k?	==
</s>
</p>
<p>
<s>
Kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Arizona	Arizona	k1gFnSc1	Arizona
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc4d1	velký
stát	stát	k1gInSc4	stát
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
velké	velký	k2eAgInPc4d1	velký
rozdíly	rozdíl	k1gInPc4	rozdíl
v	v	k7c6	v
nadmořských	nadmořský	k2eAgFnPc6d1	nadmořská
výškách	výška	k1gFnPc6	výška
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
rozdílné	rozdílný	k2eAgFnPc1d1	rozdílná
klimatické	klimatický	k2eAgFnPc4d1	klimatická
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nižších	nízký	k2eAgFnPc6d2	nižší
nadmořských	nadmořský	k2eAgFnPc6d1	nadmořská
výškách	výška	k1gFnPc6	výška
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
pouště	poušť	k1gFnPc4	poušť
s	s	k7c7	s
horkými	horký	k2eAgNnPc7d1	horké
léty	léto	k1gNnPc7	léto
a	a	k8xC	a
mírnými	mírný	k2eAgFnPc7d1	mírná
zimami	zima	k1gFnPc7	zima
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
pozdního	pozdní	k2eAgInSc2d1	pozdní
podzimu	podzim	k1gInSc2	podzim
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
jara	jaro	k1gNnSc2	jaro
tam	tam	k6eAd1	tam
bývá	bývat	k5eAaImIp3nS	bývat
mírné	mírný	k2eAgNnSc1d1	mírné
podnebí	podnebí	k1gNnSc1	podnebí
s	s	k7c7	s
průměrným	průměrný	k2eAgInSc7d1	průměrný
minimálními	minimální	k2eAgFnPc7d1	minimální
teplotami	teplota	k1gFnPc7	teplota
kolem	kolem	k7c2	kolem
15	[number]	k4	15
°	°	k?	°
<g/>
C.	C.	kA	C.
Od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
do	do	k7c2	do
února	únor	k1gInSc2	únor
můžou	můžou	k?	můžou
být	být	k5eAaImF	být
až	až	k9	až
4	[number]	k4	4
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
občasné	občasný	k2eAgInPc1d1	občasný
mrazy	mráz	k1gInPc1	mráz
nejsou	být	k5eNaImIp3nP	být
neobvyklé	obvyklý	k2eNgInPc1d1	neobvyklý
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
února	únor	k1gInSc2	únor
začnou	začít	k5eAaPmIp3nP	začít
teploty	teplota	k1gFnPc1	teplota
stoupat	stoupat	k5eAaImF	stoupat
a	a	k8xC	a
ve	v	k7c6	v
dne	den	k1gInSc2	den
je	být	k5eAaImIp3nS	být
teplo	teplo	k1gNnSc4	teplo
a	a	k8xC	a
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
velmi	velmi	k6eAd1	velmi
chladno	chladno	k6eAd1	chladno
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
května	květen	k1gInSc2	květen
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
se	se	k3xPyFc4	se
teploty	teplota	k1gFnPc1	teplota
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
od	od	k7c2	od
32	[number]	k4	32
°	°	k?	°
<g/>
C	C	kA	C
do	do	k7c2	do
48	[number]	k4	48
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
teploty	teplota	k1gFnPc1	teplota
občas	občas	k6eAd1	občas
vystoupí	vystoupit	k5eAaPmIp3nP	vystoupit
nad	nad	k7c7	nad
50	[number]	k4	50
°	°	k?	°
<g/>
C.	C.	kA	C.
Díky	díky	k7c3	díky
velmi	velmi	k6eAd1	velmi
suchému	suchý	k2eAgNnSc3d1	suché
klimatu	klima	k1gNnSc3	klima
jsou	být	k5eAaImIp3nP	být
velké	velký	k2eAgInPc1d1	velký
rozdíly	rozdíl	k1gInPc1	rozdíl
mez	mez	k1gFnSc4	mez
denními	denní	k2eAgFnPc7d1	denní
a	a	k8xC	a
nočními	noční	k2eAgFnPc7d1	noční
teplotami	teplota	k1gFnPc7	teplota
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
kolem	kolem	k7c2	kolem
28	[number]	k4	28
°	°	k?	°
<g/>
C	C	kA	C
v	v	k7c6	v
letních	letní	k2eAgInPc6d1	letní
měsících	měsíc	k1gInPc6	měsíc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
státu	stát	k1gInSc2	stát
jsou	být	k5eAaImIp3nP	být
plošiny	plošina	k1gFnPc1	plošina
s	s	k7c7	s
chladnějším	chladný	k2eAgNnSc7d2	chladnější
podnebím	podnebí	k1gNnSc7	podnebí
s	s	k7c7	s
chladnými	chladný	k2eAgFnPc7d1	chladná
zimami	zima	k1gFnPc7	zima
a	a	k8xC	a
mírnými	mírný	k2eAgNnPc7d1	mírné
léty	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
díky	díky	k7c3	díky
chladnému	chladný	k2eAgInSc3d1	chladný
větru	vítr	k1gInSc3	vítr
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
klesají	klesat	k5eAaImIp3nP	klesat
teploty	teplota	k1gFnPc1	teplota
až	až	k9	až
na	na	k7c4	na
-20	-20	k4	-20
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
Srážky	srážka	k1gFnPc1	srážka
v	v	k7c6	v
Arizoně	Arizona	k1gFnSc6	Arizona
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
kolem	kolem	k7c2	kolem
322	[number]	k4	322
mm	mm	kA	mm
a	a	k8xC	a
přichází	přicházet	k5eAaImIp3nS	přicházet
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
obdobích	období	k1gNnPc6	období
dešťů	dešť	k1gInPc2	dešť
<g/>
.	.	kIx.	.
</s>
<s>
Studená	studený	k2eAgFnSc1d1	studená
fronta	fronta	k1gFnSc1	fronta
přichází	přicházet	k5eAaImIp3nS	přicházet
od	od	k7c2	od
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
zimy	zima	k1gFnSc2	zima
a	a	k8xC	a
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
jsou	být	k5eAaImIp3nP	být
monzunové	monzunový	k2eAgInPc1d1	monzunový
deště	dešť	k1gInPc1	dešť
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
monzunových	monzunový	k2eAgInPc2d1	monzunový
dešťů	dešť	k1gInPc2	dešť
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
a	a	k8xC	a
srpnu	srpen	k1gInSc6	srpen
<g/>
,	,	kIx,	,
přináší	přinášet	k5eAaImIp3nP	přinášet
bouřky	bouřka	k1gFnPc1	bouřka
a	a	k8xC	a
vítr	vítr	k1gInSc1	vítr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tornáda	tornádo	k1gNnPc1	tornádo
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Arizoně	Arizona	k1gFnSc6	Arizona
poměrně	poměrně	k6eAd1	poměrně
vzácná	vzácný	k2eAgFnSc1d1	vzácná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
státu	stát	k1gInSc2	stát
Phoenixu	Phoenix	k1gInSc2	Phoenix
se	se	k3xPyFc4	se
po	po	k7c4	po
většinu	většina	k1gFnSc4	většina
roku	rok	k1gInSc2	rok
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
teploty	teplota	k1gFnPc1	teplota
kolem	kolem	k7c2	kolem
35	[number]	k4	35
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Arizony	Arizona	k1gFnSc2	Arizona
jako	jako	k8xS	jako
první	první	k4xOgMnSc1	první
Evropan	Evropan	k1gMnSc1	Evropan
pronikl	proniknout	k5eAaPmAgMnS	proniknout
v	v	k7c6	v
letech	let	k1gInPc6	let
1540	[number]	k4	1540
<g/>
–	–	k?	–
<g/>
1542	[number]	k4	1542
španělský	španělský	k2eAgInSc4d1	španělský
conquistador	conquistador	k1gMnSc1	conquistador
Coronado	Coronada	k1gFnSc5	Coronada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1821	[number]	k4	1821
se	se	k3xPyFc4	se
řídce	řídce	k6eAd1	řídce
osídlená	osídlený	k2eAgFnSc1d1	osídlená
španělská	španělský	k2eAgFnSc1d1	španělská
provincie	provincie	k1gFnSc1	provincie
Horní	horní	k2eAgFnSc1d1	horní
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
stala	stát	k5eAaPmAgFnS	stát
součástí	součást	k1gFnSc7	součást
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
zbavilo	zbavit	k5eAaPmAgNnS	zbavit
španělské	španělský	k2eAgFnPc4d1	španělská
nadvlády	nadvláda	k1gFnPc4	nadvláda
<g/>
.	.	kIx.	.
</s>
<s>
Mexická	mexický	k2eAgFnSc1d1	mexická
provincie	provincie	k1gFnSc1	provincie
přešla	přejít	k5eAaPmAgFnS	přejít
pod	pod	k7c4	pod
svrchovanost	svrchovanost	k1gFnSc4	svrchovanost
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
roku	rok	k1gInSc2	rok
1848	[number]	k4	1848
po	po	k7c4	po
americko	americko	k6eAd1	americko
<g/>
–	–	k?	–
<g/>
mexické	mexický	k2eAgInPc4d1	mexický
válce	válec	k1gInPc4	válec
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgNnSc4	který
Mexiko	Mexiko	k1gNnSc1	Mexiko
přišlo	přijít	k5eAaPmAgNnS	přijít
o	o	k7c4	o
polovinu	polovina	k1gFnSc4	polovina
svého	svůj	k3xOyFgNnSc2	svůj
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
6	[number]	k4	6
392	[number]	k4	392
017	[number]	k4	017
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
20	[number]	k4	20
%	%	kIx~	%
více	hodně	k6eAd2	hodně
než	než	k8xS	než
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
přírůstek	přírůstek	k1gInSc1	přírůstek
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
jak	jak	k6eAd1	jak
přirozený	přirozený	k2eAgInSc1d1	přirozený
přírůstek	přírůstek	k1gInSc1	přírůstek
<g/>
,	,	kIx,	,
tak	tak	k9	tak
imigranty	imigrant	k1gMnPc4	imigrant
<g/>
.	.	kIx.	.
</s>
<s>
Téměř	téměř	k6eAd1	téměř
tři	tři	k4xCgFnPc4	tři
čtvrtiny	čtvrtina	k1gFnPc1	čtvrtina
obyvatel	obyvatel	k1gMnPc2	obyvatel
starších	starý	k2eAgNnPc2d2	starší
pěti	pět	k4xCc2	pět
let	léto	k1gNnPc2	léto
mluví	mluvit	k5eAaImIp3nS	mluvit
doma	doma	k6eAd1	doma
pouze	pouze	k6eAd1	pouze
anglicky	anglicky	k6eAd1	anglicky
a	a	k8xC	a
asi	asi	k9	asi
20	[number]	k4	20
%	%	kIx~	%
španělsky	španělsky	k6eAd1	španělsky
<g/>
.	.	kIx.	.
</s>
<s>
Navažština	Navažština	k1gFnSc1	Navažština
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgInSc1	třetí
nejpoužívanější	používaný	k2eAgInSc1d3	nejpoužívanější
jazyk	jazyk	k1gInSc1	jazyk
s	s	k7c7	s
necelými	celý	k2eNgInPc7d1	necelý
dvěma	dva	k4xCgInPc7	dva
procenty	procent	k1gInPc7	procent
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
Arizony	Arizona	k1gFnSc2	Arizona
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
konzervativní	konzervativní	k2eAgFnSc1d1	konzervativní
<g/>
,	,	kIx,	,
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
se	se	k3xPyFc4	se
kloní	klonit	k5eAaImIp3nS	klonit
k	k	k7c3	k
republikánské	republikánský	k2eAgFnSc3d1	republikánská
straně	strana	k1gFnSc3	strana
<g/>
,	,	kIx,	,
velký	velký	k2eAgInSc1d1	velký
vliv	vliv	k1gInSc1	vliv
má	mít	k5eAaImIp3nS	mít
zde	zde	k6eAd1	zde
křesťanská	křesťanský	k2eAgFnSc1d1	křesťanská
tradice	tradice	k1gFnSc1	tradice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rasové	rasový	k2eAgNnSc4d1	rasové
složení	složení	k1gNnSc4	složení
===	===	k?	===
</s>
</p>
<p>
<s>
73,0	[number]	k4	73,0
%	%	kIx~	%
Bílí	bílý	k2eAgMnPc1d1	bílý
Američané	Američan	k1gMnPc1	Američan
(	(	kIx(	(
<g/>
nehispánští	hispánský	k2eNgMnPc1d1	hispánský
běloši	běloch	k1gMnPc1	běloch
57,8	[number]	k4	57,8
%	%	kIx~	%
+	+	kIx~	+
běloši	běloch	k1gMnPc1	běloch
hispánského	hispánský	k2eAgInSc2d1	hispánský
původu	původ	k1gInSc2	původ
15,2	[number]	k4	15,2
%	%	kIx~	%
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
4,1	[number]	k4	4,1
%	%	kIx~	%
Afroameričané	Afroameričan	k1gMnPc1	Afroameričan
</s>
</p>
<p>
<s>
4,6	[number]	k4	4,6
%	%	kIx~	%
Američtí	americký	k2eAgMnPc1d1	americký
indiáni	indián	k1gMnPc1	indián
</s>
</p>
<p>
<s>
2,8	[number]	k4	2,8
%	%	kIx~	%
Asijští	asijský	k2eAgMnPc1d1	asijský
Američané	Američan	k1gMnPc1	Američan
</s>
</p>
<p>
<s>
0,2	[number]	k4	0,2
%	%	kIx~	%
Pacifičtí	pacifický	k2eAgMnPc1d1	pacifický
ostrované	ostrovan	k1gMnPc1	ostrovan
</s>
</p>
<p>
<s>
11,9	[number]	k4	11,9
%	%	kIx~	%
Jiná	jiný	k2eAgFnSc1d1	jiná
rasa	rasa	k1gFnSc1	rasa
</s>
</p>
<p>
<s>
3,4	[number]	k4	3,4
%	%	kIx~	%
Dvě	dva	k4xCgNnPc1	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
rasObyvatelé	rasObyvatel	k1gMnPc1	rasObyvatel
hispánského	hispánský	k2eAgInSc2d1	hispánský
nebo	nebo	k8xC	nebo
latinskoamerického	latinskoamerický	k2eAgInSc2d1	latinskoamerický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
rasu	rasa	k1gFnSc4	rasa
<g/>
,	,	kIx,	,
tvořili	tvořit	k5eAaImAgMnP	tvořit
29,6	[number]	k4	29,6
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
<g/>
Nejpočetnější	početní	k2eAgFnSc7d3	nejpočetnější
menšinou	menšina	k1gFnSc7	menšina
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Arizona	Arizona	k1gFnSc1	Arizona
jsou	být	k5eAaImIp3nP	být
Mexičané	Mexičan	k1gMnPc1	Mexičan
(	(	kIx(	(
<g/>
asi	asi	k9	asi
21	[number]	k4	21
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
Němci	Němec	k1gMnPc1	Němec
<g/>
,	,	kIx,	,
Britové	Brit	k1gMnPc1	Brit
a	a	k8xC	a
Irové	Ir	k1gMnPc1	Ir
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
území	území	k1gNnPc1	území
poblíž	poblíž	k7c2	poblíž
mexických	mexický	k2eAgFnPc2d1	mexická
hranic	hranice	k1gFnPc2	hranice
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
okres	okres	k1gInSc1	okres
Santa	Santo	k1gNnSc2	Santo
Cruz	Cruza	k1gFnPc2	Cruza
County	Counta	k1gFnSc2	Counta
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
osídlena	osídlit	k5eAaPmNgNnP	osídlit
převážně	převážně	k6eAd1	převážně
přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
z	z	k7c2	z
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
části	část	k1gFnSc6	část
státu	stát	k1gInSc2	stát
žije	žít	k5eAaImIp3nS	žít
mnoho	mnoho	k4c1	mnoho
indiánů	indián	k1gMnPc2	indián
<g/>
.	.	kIx.	.
</s>
<s>
Afroameričanů	Afroameričan	k1gMnPc2	Afroameričan
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Arizoně	Arizona	k1gFnSc6	Arizona
poměrně	poměrně	k6eAd1	poměrně
málo	málo	k6eAd1	málo
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
Phoenixu	Phoenix	k1gInSc6	Phoenix
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
zdvojnásobil	zdvojnásobit	k5eAaPmAgMnS	zdvojnásobit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
Církevní	církevní	k2eAgFnSc1d1	církevní
příslušnost	příslušnost	k1gFnSc1	příslušnost
obyvatel	obyvatel	k1gMnPc2	obyvatel
Arizony	Arizona	k1gFnSc2	Arizona
je	být	k5eAaImIp3nS	být
následující	následující	k2eAgInSc1d1	následující
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
křesťané	křesťan	k1gMnPc1	křesťan
80	[number]	k4	80
%	%	kIx~	%
</s>
</p>
<p>
<s>
protestanti	protestant	k1gMnPc1	protestant
42	[number]	k4	42
%	%	kIx~	%
</s>
</p>
<p>
<s>
baptisté	baptista	k1gMnPc1	baptista
9	[number]	k4	9
%	%	kIx~	%
</s>
</p>
<p>
<s>
metodisté	metodista	k1gMnPc1	metodista
5	[number]	k4	5
%	%	kIx~	%
</s>
</p>
<p>
<s>
luteráni	luterán	k1gMnPc1	luterán
4	[number]	k4	4
%	%	kIx~	%
</s>
</p>
<p>
<s>
ostatní	ostatní	k2eAgMnPc1d1	ostatní
protestanti	protestant	k1gMnPc1	protestant
24	[number]	k4	24
%	%	kIx~	%
</s>
</p>
<p>
<s>
římští	římský	k2eAgMnPc1d1	římský
katolíci	katolík	k1gMnPc1	katolík
31	[number]	k4	31
%	%	kIx~	%
</s>
</p>
<p>
<s>
mormoni	mormon	k1gMnPc1	mormon
6	[number]	k4	6
%	%	kIx~	%
</s>
</p>
<p>
<s>
ostatní	ostatní	k2eAgMnPc1d1	ostatní
křesťané	křesťan	k1gMnPc1	křesťan
1	[number]	k4	1
%	%	kIx~	%
</s>
</p>
<p>
<s>
jiná	jiná	k1gFnSc1	jiná
náboženství	náboženství	k1gNnPc2	náboženství
2	[number]	k4	2
%	%	kIx~	%
</s>
</p>
<p>
<s>
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnPc2	vyznání
18	[number]	k4	18
%	%	kIx~	%
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Mottem	motto	k1gNnSc7	motto
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
Ditat	Ditat	k2eAgInSc1d1	Ditat
Deus	Deus	k1gInSc1	Deus
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
květinou	květina	k1gFnSc7	květina
květ	květ	k1gInSc4	květ
kaktusu	kaktus	k1gInSc2	kaktus
saguaro	saguara	k1gFnSc5	saguara
<g/>
,	,	kIx,	,
stromem	strom	k1gInSc7	strom
parkinsonie	parkinsonie	k1gFnSc2	parkinsonie
pichlavá	pichlavý	k2eAgNnPc1d1	pichlavé
<g/>
,	,	kIx,	,
ptákem	pták	k1gMnSc7	pták
střízlík	střízlík	k1gMnSc1	střízlík
kaktusový	kaktusový	k2eAgMnSc1d1	kaktusový
a	a	k8xC	a
písní	píseň	k1gFnPc2	píseň
Arizona	Arizona	k1gFnSc1	Arizona
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Arizona	Arizona	k1gFnSc1	Arizona
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Arizona	Arizona	k1gFnSc1	Arizona
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
státu	stát	k1gInSc2	stát
Arizona	Arizona	k1gFnSc1	Arizona
</s>
</p>
