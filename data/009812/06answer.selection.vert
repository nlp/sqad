<s>
Arizona	Arizona	k1gFnSc1	Arizona
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
svým	svůj	k3xOyFgNnSc7	svůj
pouštním	pouštní	k2eAgNnSc7d1	pouštní
klimatem	klima	k1gNnSc7	klima
<g/>
,	,	kIx,	,
výjimečně	výjimečně	k6eAd1	výjimečně
horkými	horký	k2eAgNnPc7d1	horké
léty	léto	k1gNnPc7	léto
a	a	k8xC	a
mírnými	mírný	k2eAgFnPc7d1	mírná
zimami	zima	k1gFnPc7	zima
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
polohách	poloha	k1gFnPc6	poloha
jsou	být	k5eAaImIp3nP	být
borovicové	borovicový	k2eAgInPc1d1	borovicový
lesy	les	k1gInPc1	les
a	a	k8xC	a
horské	horský	k2eAgFnPc1d1	horská
louky	louka	k1gFnPc1	louka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
kontrastují	kontrastovat	k5eAaImIp3nP	kontrastovat
s	s	k7c7	s
pouštěmi	poušť	k1gFnPc7	poušť
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
<g/>
.	.	kIx.	.
</s>
