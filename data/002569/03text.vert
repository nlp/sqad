<s>
Xuân	Xuân	k1gMnSc1	Xuân
Diệ	Diệ	k1gMnSc1	Diệ
<g/>
,	,	kIx,	,
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
Ngô	Ngô	k1gMnSc1	Ngô
Xuân	Xuân	k1gMnSc1	Xuân
Diệ	Diệ	k1gMnSc1	Diệ
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1916	[number]	k4	1916
provincie	provincie	k1gFnSc2	provincie
Bì	Bì	k1gFnSc1	Bì
Đị	Đị	k1gFnSc1	Đị
-	-	kIx~	-
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1985	[number]	k4	1985
Hanoj	Hanoj	k1gFnSc1	Hanoj
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
vietnamských	vietnamský	k2eAgMnPc2d1	vietnamský
básníků	básník	k1gMnPc2	básník
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Užíval	užívat	k5eAaImAgMnS	užívat
též	též	k6eAd1	též
pseudonymu	pseudonym	k1gInSc2	pseudonym
Trao	Trao	k1gMnSc1	Trao
Nha	Nha	k1gMnSc1	Nha
(	(	kIx(	(
<g/>
viet	viet	k1gMnSc1	viet
<g/>
.	.	kIx.	.
</s>
<s>
Trả	Trả	k?	Trả
Nha	Nha	k1gFnSc1	Nha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Napsal	napsat	k5eAaBmAgInS	napsat
kolem	kolem	k7c2	kolem
450	[number]	k4	450
převážně	převážně	k6eAd1	převážně
milostných	milostný	k2eAgFnPc2d1	milostná
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
několik	několik	k4yIc4	několik
povídek	povídka	k1gFnPc2	povídka
<g/>
,	,	kIx,	,
eseje	esej	k1gFnSc2	esej
a	a	k8xC	a
literární	literární	k2eAgFnSc2d1	literární
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
také	také	k9	také
překládal	překládat	k5eAaImAgMnS	překládat
poesii	poesie	k1gFnSc4	poesie
(	(	kIx(	(
<g/>
mj.	mj.	kA	mj.
Nazı	Nazı	k1gMnSc1	Nazı
Hikmet	Hikmet	k1gMnSc1	Hikmet
<g/>
,	,	kIx,	,
Nicolás	Nicolás	k1gInSc1	Nicolás
Guillén	Guillén	k1gInSc1	Guillén
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
učitelem	učitel	k1gMnSc7	učitel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1943	[number]	k4	1943
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
Xuan	Xuan	k1gInSc4	Xuan
Zieu	Zieus	k1gInSc2	Zieus
zemědělské	zemědělský	k2eAgNnSc4d1	zemědělské
inženýrství	inženýrství	k1gNnSc4	inženýrství
<g/>
,	,	kIx,	,
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgMnS	připojit
k	k	k7c3	k
národně	národně	k6eAd1	národně
osvobozeneckému	osvobozenecký	k2eAgNnSc3d1	osvobozenecké
hnutí	hnutí	k1gNnSc3	hnutí
Viet	Viet	k2eAgInSc4d1	Viet
Minh	Minh	k1gInSc4	Minh
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
znám	znám	k2eAgInSc1d1	znám
především	především	k9	především
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
milostnou	milostný	k2eAgFnSc4d1	milostná
poesii	poesie	k1gFnSc4	poesie
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc2	jeho
básnické	básnický	k2eAgFnSc2d1	básnická
sbírky	sbírka	k1gFnSc2	sbírka
Poesie	poesie	k1gFnSc2	poesie
(	(	kIx(	(
<g/>
viet	viet	k1gMnSc1	viet
<g/>
.	.	kIx.	.
</s>
<s>
Thơ	Thơ	k?	Thơ
thơ	thơ	k?	thơ
<g/>
,	,	kIx,	,
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
a	a	k8xC	a
Vůně	vůně	k1gFnSc1	vůně
na	na	k7c6	na
křídlech	křídlo	k1gNnPc6	křídlo
větru	vítr	k1gInSc2	vítr
(	(	kIx(	(
<g/>
viet	viet	k1gInSc1	viet
<g/>
.	.	kIx.	.
</s>
<s>
Gử	Gử	k?	Gử
hư	hư	k?	hư
cho	cho	k0	cho
gió	gió	k?	gió
<g/>
,	,	kIx,	,
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
67	[number]	k4	67
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
oslavující	oslavující	k2eAgNnSc4d1	oslavující
mládí	mládí	k1gNnSc4	mládí
<g/>
,	,	kIx,	,
lásku	láska	k1gFnSc4	láska
(	(	kIx(	(
<g/>
z	z	k7c2	z
části	část	k1gFnSc2	část
homosexuální	homosexuální	k2eAgFnSc2d1	homosexuální
<g/>
)	)	kIx)	)
a	a	k8xC	a
přírodu	příroda	k1gFnSc4	příroda
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
známé	známý	k2eAgFnPc1d1	známá
<g/>
,	,	kIx,	,
čtou	číst	k5eAaImIp3nP	číst
se	se	k3xPyFc4	se
na	na	k7c6	na
vietnamských	vietnamský	k2eAgFnPc6d1	vietnamská
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
"	"	kIx"	"
<g/>
největšího	veliký	k2eAgMnSc4d3	veliký
z	z	k7c2	z
nových	nový	k2eAgMnPc2d1	nový
básníků	básník	k1gMnPc2	básník
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
krále	král	k1gMnSc2	král
milostných	milostný	k2eAgFnPc2d1	milostná
básní	báseň	k1gFnPc2	báseň
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Hanoji	Hanoj	k1gFnSc6	Hanoj
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
něm	on	k3xPp3gInSc6	on
pojmenována	pojmenován	k2eAgFnSc1d1	pojmenována
ulice	ulice	k1gFnSc1	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Ukázky	ukázka	k1gFnPc1	ukázka
z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
-	-	kIx~	-
Xuân	Xuân	k1gNnSc1	Xuân
Diệ	Diệ	k1gFnSc2	Diệ
na	na	k7c4	na
www.klubhanoi.cz	www.klubhanoi.cz	k1gInSc4	www.klubhanoi.cz
</s>
