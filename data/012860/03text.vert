<p>
<s>
Styren	styren	k1gInSc1	styren
(	(	kIx(	(
<g/>
fenylethen	fenylethen	k1gInSc1	fenylethen
<g/>
,	,	kIx,	,
vinylbenzen	vinylbenzen	k2eAgInSc1d1	vinylbenzen
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
významná	významný	k2eAgFnSc1d1	významná
aromatická	aromatický	k2eAgFnSc1d1	aromatická
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
chemikálie	chemikálie	k1gFnSc1	chemikálie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Za	za	k7c2	za
běžných	běžný	k2eAgFnPc2d1	běžná
podmínek	podmínka	k1gFnPc2	podmínka
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
bezbarvou	bezbarvý	k2eAgFnSc4d1	bezbarvá
až	až	k8xS	až
nažloutlou	nažloutlý	k2eAgFnSc4d1	nažloutlá
kapalinu	kapalina	k1gFnSc4	kapalina
pronikavě	pronikavě	k6eAd1	pronikavě
nasládlého	nasládlý	k2eAgInSc2d1	nasládlý
zápachu	zápach	k1gInSc2	zápach
<g/>
.	.	kIx.	.
</s>
<s>
Snadno	snadno	k6eAd1	snadno
těká	těkat	k5eAaImIp3nS	těkat
<g/>
,	,	kIx,	,
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
se	se	k3xPyFc4	se
oxiduje	oxidovat	k5eAaBmIp3nS	oxidovat
a	a	k8xC	a
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
se	se	k3xPyFc4	se
peroxidy	peroxid	k1gInPc7	peroxid
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
působí	působit	k5eAaImIp3nP	působit
jako	jako	k9	jako
katalyzátor	katalyzátor	k1gInSc4	katalyzátor
polymerizace	polymerizace	k1gFnSc2	polymerizace
společně	společně	k6eAd1	společně
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
parametry	parametr	k1gInPc7	parametr
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
tlak	tlak	k1gInSc1	tlak
<g/>
,	,	kIx,	,
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
světlo	světlo	k1gNnSc1	světlo
a	a	k8xC	a
silné	silný	k2eAgFnPc1d1	silná
kyseliny	kyselina	k1gFnPc1	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
styren	styren	k1gInSc1	styren
stabilizuje	stabilizovat	k5eAaBmIp3nS	stabilizovat
přídavkem	přídavek	k1gInSc7	přídavek
inhibitorů	inhibitor	k1gInPc2	inhibitor
<g/>
,	,	kIx,	,
např.	např.	kA	např.
hydrochinonem	hydrochinon	k1gInSc7	hydrochinon
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
volně	volně	k6eAd1	volně
nenalézá	nalézat	k5eNaImIp3nS	nalézat
<g/>
,	,	kIx,	,
metabolicky	metabolicky	k6eAd1	metabolicky
však	však	k9	však
vzniká	vznikat	k5eAaImIp3nS	vznikat
jako	jako	k9	jako
produkt	produkt	k1gInSc4	produkt
rozkladu	rozklad	k1gInSc2	rozklad
kyseliny	kyselina	k1gFnSc2	kyselina
skořicové	skořicový	k2eAgFnSc2d1	skořicová
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
špatně	špatně	k6eAd1	špatně
rozpustný	rozpustný	k2eAgMnSc1d1	rozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
v	v	k7c6	v
organických	organický	k2eAgNnPc6d1	organické
rozpouštědlech	rozpouštědlo	k1gNnPc6	rozpouštědlo
(	(	kIx(	(
<g/>
alkoholy	alkohol	k1gInPc1	alkohol
<g/>
,	,	kIx,	,
aceton	aceton	k1gInSc1	aceton
a	a	k8xC	a
sirouhlík	sirouhlík	k1gInSc1	sirouhlík
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
využíván	využívat	k5eAaPmNgInS	využívat
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
barviv	barvivo	k1gNnPc2	barvivo
<g/>
,	,	kIx,	,
plastů	plast	k1gInPc2	plast
<g/>
,	,	kIx,	,
v	v	k7c6	v
gumárenském	gumárenský	k2eAgInSc6d1	gumárenský
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
výskyt	výskyt	k1gInSc1	výskyt
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
je	být	k5eAaImIp3nS	být
především	především	k9	především
důsledkem	důsledek	k1gInSc7	důsledek
antropogenní	antropogenní	k2eAgFnSc2d1	antropogenní
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgNnSc1d3	nejvýznamnější
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
využití	využití	k1gNnSc1	využití
k	k	k7c3	k
polymeraci	polymerace	k1gFnSc3	polymerace
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
výroby	výroba	k1gFnSc2	výroba
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
nejběžnějších	běžný	k2eAgMnPc2d3	Nejběžnější
plastů	plast	k1gInPc2	plast
–	–	k?	–
polystyrenu	polystyren	k1gInSc6	polystyren
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k8xC	ale
využívá	využívat	k5eAaPmIp3nS	využívat
se	se	k3xPyFc4	se
i	i	k9	i
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
lepidel	lepidlo	k1gNnPc2	lepidlo
<g/>
,	,	kIx,	,
fotografických	fotografický	k2eAgInPc2d1	fotografický
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
inkoustů	inkoust	k1gInPc2	inkoust
<g/>
,	,	kIx,	,
automobilových	automobilový	k2eAgFnPc2d1	automobilová
součástek	součástka	k1gFnPc2	součástka
<g/>
,	,	kIx,	,
obalových	obalový	k2eAgInPc2d1	obalový
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
plastového	plastový	k2eAgNnSc2d1	plastové
nádobí	nádobí	k1gNnSc2	nádobí
a	a	k8xC	a
řady	řada	k1gFnSc2	řada
dalšího	další	k2eAgNnSc2d1	další
spotřebního	spotřební	k2eAgNnSc2d1	spotřební
zboží	zboží	k1gNnSc2	zboží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Toxicita	toxicita	k1gFnSc1	toxicita
==	==	k?	==
</s>
</p>
<p>
<s>
Styren	styren	k1gInSc1	styren
má	mít	k5eAaImIp3nS	mít
narkotické	narkotický	k2eAgNnSc4d1	narkotické
a	a	k8xC	a
lokálně	lokálně	k6eAd1	lokálně
dráždivé	dráždivý	k2eAgInPc4d1	dráždivý
účinky	účinek	k1gInPc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Koncentrace	koncentrace	k1gFnSc1	koncentrace
70	[number]	k4	70
ppm	ppm	k?	ppm
ještě	ještě	k9	ještě
nevyvolává	vyvolávat	k5eNaImIp3nS	vyvolávat
dráždění	dráždění	k1gNnSc1	dráždění
<g/>
,	,	kIx,	,
koncentrace	koncentrace	k1gFnSc1	koncentrace
100	[number]	k4	100
ppm	ppm	k?	ppm
je	být	k5eAaImIp3nS	být
snesitelná	snesitelný	k2eAgFnSc1d1	snesitelná
<g/>
,	,	kIx,	,
koncentrace	koncentrace	k1gFnSc1	koncentrace
200	[number]	k4	200
ppm	ppm	k?	ppm
až	až	k9	až
400	[number]	k4	400
ppm	ppm	k?	ppm
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
nepříjemný	příjemný	k2eNgInSc4d1	nepříjemný
pocit	pocit	k1gInSc4	pocit
zápachu	zápach	k1gInSc2	zápach
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c4	nad
koncentraci	koncentrace	k1gFnSc4	koncentrace
400	[number]	k4	400
ppm	ppm	k?	ppm
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
dráždivý	dráždivý	k2eAgInSc1d1	dráždivý
účinek	účinek	k1gInSc1	účinek
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
800	[number]	k4	800
ppm	ppm	k?	ppm
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
narkotizační	narkotizační	k2eAgInSc1d1	narkotizační
účinek	účinek	k1gInSc1	účinek
a	a	k8xC	a
při	při	k7c6	při
koncentraci	koncentrace	k1gFnSc6	koncentrace
nad	nad	k7c7	nad
1	[number]	k4	1
300	[number]	k4	300
ppm	ppm	k?	ppm
je	být	k5eAaImIp3nS	být
již	již	k9	již
pobyt	pobyt	k1gInSc1	pobyt
nesnesitelný	snesitelný	k2eNgInSc1d1	nesnesitelný
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
vyloučeny	vyloučen	k2eAgInPc1d1	vyloučen
pozdní	pozdní	k2eAgInPc1d1	pozdní
účinky	účinek	k1gInPc1	účinek
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
edém	edém	k1gInSc1	edém
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
požití	požití	k1gNnSc6	požití
je	být	k5eAaImIp3nS	být
nepatrně	nepatrně	k6eAd1	nepatrně
jedovatější	jedovatý	k2eAgMnSc1d2	jedovatější
než	než	k8xS	než
benzen	benzen	k1gInSc1	benzen
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
styku	styk	k1gInSc6	styk
s	s	k7c7	s
kůží	kůže	k1gFnSc7	kůže
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
jejímu	její	k3xOp3gNnSc3	její
vysušení	vysušení	k1gNnSc3	vysušení
a	a	k8xC	a
dráždění	dráždění	k1gNnSc3	dráždění
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
styku	styk	k1gInSc6	styk
s	s	k7c7	s
rohovkou	rohovka	k1gFnSc7	rohovka
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
dráždění	dráždění	k1gNnSc3	dráždění
až	až	k8xS	až
trvalému	trvalý	k2eAgNnSc3d1	trvalé
poškození	poškození	k1gNnSc3	poškození
<g/>
.	.	kIx.	.
</s>
<s>
Chronická	chronický	k2eAgFnSc1d1	chronická
expozice	expozice	k1gFnSc1	expozice
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
pseudoneurastenickými	pseudoneurastenický	k2eAgFnPc7d1	pseudoneurastenický
poruchami	porucha	k1gFnPc7	porucha
<g/>
,	,	kIx,	,
změnami	změna	k1gFnPc7	změna
v	v	k7c6	v
jaterních	jaterní	k2eAgFnPc6d1	jaterní
funkcích	funkce	k1gFnPc6	funkce
a	a	k8xC	a
poklesem	pokles	k1gInSc7	pokles
krevního	krevní	k2eAgInSc2d1	krevní
tlaku	tlak	k1gInSc2	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
dalšími	další	k2eAgInPc7d1	další
následky	následek	k1gInPc7	následek
chronické	chronický	k2eAgFnSc2d1	chronická
expozice	expozice	k1gFnSc2	expozice
patří	patřit	k5eAaImIp3nP	patřit
hepatotoxické	hepatotoxický	k2eAgInPc1d1	hepatotoxický
účinky	účinek	k1gInPc1	účinek
a	a	k8xC	a
atrofie	atrofie	k1gFnPc1	atrofie
sliznice	sliznice	k1gFnSc2	sliznice
horních	horní	k2eAgFnPc2d1	horní
cest	cesta	k1gFnPc2	cesta
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
test	test	k1gInSc1	test
expozice	expozice	k1gFnSc2	expozice
styrenu	styren	k1gInSc2	styren
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
použít	použít	k5eAaPmF	použít
analýzu	analýza	k1gFnSc4	analýza
kyseliny	kyselina	k1gFnSc2	kyselina
mandlové	mandlový	k2eAgFnSc2d1	Mandlová
v	v	k7c6	v
moči	moč	k1gFnSc6	moč
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
agentura	agentura	k1gFnSc1	agentura
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
rakoviny	rakovina	k1gFnSc2	rakovina
(	(	kIx(	(
<g/>
IARC	IARC	kA	IARC
<g/>
)	)	kIx)	)
řadí	řadit	k5eAaImIp3nS	řadit
styren	styren	k1gInSc1	styren
mezi	mezi	k7c7	mezi
karcinogeny	karcinogen	k1gInPc7	karcinogen
skupiny	skupina	k1gFnSc2	skupina
2B	[number]	k4	2B
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
studií	studio	k1gNnPc2	studio
u	u	k7c2	u
pracovníků	pracovník	k1gMnPc2	pracovník
vystavených	vystavená	k1gFnPc2	vystavená
styrenu	styren	k1gInSc2	styren
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
plastů	plast	k1gInPc2	plast
nalezlo	nalézt	k5eAaBmAgNnS	nalézt
zvýšený	zvýšený	k2eAgInSc4d1	zvýšený
počet	počet	k1gInSc4	počet
chromozomálních	chromozomální	k2eAgFnPc2d1	chromozomální
aberací	aberace	k1gFnPc2	aberace
v	v	k7c6	v
periferních	periferní	k2eAgInPc6d1	periferní
lymfocytech	lymfocyt	k1gInPc6	lymfocyt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
V	v	k7c6	v
životním	životní	k2eAgNnSc6d1	životní
prostředí	prostředí	k1gNnSc6	prostředí
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
uvolnění	uvolnění	k1gNnSc6	uvolnění
do	do	k7c2	do
ovzduší	ovzduší	k1gNnSc2	ovzduší
se	se	k3xPyFc4	se
styren	styren	k1gInSc1	styren
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c4	v
benzaldehyd	benzaldehyd	k1gInSc4	benzaldehyd
a	a	k8xC	a
formaldehyd	formaldehyd	k1gInSc4	formaldehyd
<g/>
.	.	kIx.	.
</s>
<s>
Poločas	poločas	k1gInSc1	poločas
setrvání	setrvání	k1gNnSc2	setrvání
v	v	k7c6	v
ovzduší	ovzduší	k1gNnSc6	ovzduší
je	být	k5eAaImIp3nS	být
kolem	kolem	k7c2	kolem
dvou	dva	k4xCgFnPc2	dva
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
městském	městský	k2eAgNnSc6d1	Městské
prostředí	prostředí	k1gNnSc6	prostředí
se	se	k3xPyFc4	se
styren	styren	k1gInSc1	styren
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
koncentracích	koncentrace	k1gFnPc6	koncentrace
okolo	okolo	k7c2	okolo
0,3	[number]	k4	0,3
μ	μ	k?	μ
<g/>
/	/	kIx~	/
<g/>
m3	m3	k4	m3
<g/>
,	,	kIx,	,
ve	v	k7c6	v
znečištěném	znečištěný	k2eAgNnSc6d1	znečištěné
ovzduší	ovzduší	k1gNnSc6	ovzduší
až	až	k9	až
20	[number]	k4	20
μ	μ	k?	μ
<g/>
/	/	kIx~	/
<g/>
m3	m3	k4	m3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Styren	styren	k1gInSc1	styren
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Styren	styren	k1gInSc1	styren
charakteristika	charakteristikon	k1gNnSc2	charakteristikon
na	na	k7c6	na
stránce	stránka	k1gFnSc6	stránka
IRZ	IRZ	kA	IRZ
</s>
</p>
<p>
<s>
Bezpečnostní	bezpečnostní	k2eAgInSc1d1	bezpečnostní
list	list	k1gInSc1	list
styrenu	styren	k1gInSc2	styren
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
firmy	firma	k1gFnSc2	firma
Unipetrol	Unipetrol	k1gInSc1	Unipetrol
</s>
</p>
