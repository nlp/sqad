<s>
První	první	k4xOgNnSc1	první
album	album	k1gNnSc1	album
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
s	s	k7c7	s
názvem	název	k1gInSc7	název
Rocka	Rocek	k1gMnSc2	Rocek
Rolla	Roll	k1gMnSc2	Roll
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
a	a	k8xC	a
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
vydání	vydání	k1gNnSc2	vydání
alba	album	k1gNnSc2	album
Stained	Stained	k1gMnSc1	Stained
Class	Class	k1gInSc1	Class
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
Judas	Judasa	k1gFnPc2	Judasa
Priest	Priest	k1gFnSc1	Priest
stále	stále	k6eAd1	stále
drží	držet	k5eAaImIp3nS	držet
na	na	k7c6	na
metalové	metalový	k2eAgFnSc6d1	metalová
špičce	špička	k1gFnSc6	špička
<g/>
.	.	kIx.	.
</s>
