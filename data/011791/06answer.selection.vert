<s>
Chrobák	chrobák	k1gMnSc1	chrobák
révový	révový	k2eAgMnSc1d1	révový
(	(	kIx(	(
<g/>
Lethrus	Lethrus	k1gMnSc1	Lethrus
apterus	apterus	k1gMnSc1	apterus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc1	druh
nelétavého	létavý	k2eNgMnSc2d1	nelétavý
brouka	brouk	k1gMnSc2	brouk
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Lethrus	Lethrus	k1gInSc1	Lethrus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
zařazen	zařadit	k5eAaPmNgInS	zařadit
do	do	k7c2	do
čeledě	čeleď	k1gFnSc2	čeleď
chrobákovitých	chrobákovitý	k2eAgMnPc2d1	chrobákovitý
<g/>
.	.	kIx.	.
</s>
