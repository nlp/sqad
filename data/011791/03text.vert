<p>
<s>
Chrobák	chrobák	k1gMnSc1	chrobák
révový	révový	k2eAgMnSc1d1	révový
(	(	kIx(	(
<g/>
Lethrus	Lethrus	k1gMnSc1	Lethrus
apterus	apterus	k1gMnSc1	apterus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc1	druh
nelétavého	létavý	k2eNgMnSc2d1	nelétavý
brouka	brouk	k1gMnSc2	brouk
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Lethrus	Lethrus	k1gInSc1	Lethrus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
zařazen	zařadit	k5eAaPmNgInS	zařadit
do	do	k7c2	do
čeledě	čeleď	k1gFnSc2	čeleď
chrobákovitých	chrobákovitý	k2eAgMnPc2d1	chrobákovitý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Druh	druh	k1gInSc1	druh
je	být	k5eAaImIp3nS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
od	od	k7c2	od
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
jihovýchodním	jihovýchodní	k2eAgInSc7d1	jihovýchodní
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Balkán	Balkán	k1gInSc4	Balkán
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
jižní	jižní	k2eAgFnSc4d1	jižní
Ukrajinu	Ukrajina	k1gFnSc4	Ukrajina
a	a	k8xC	a
do	do	k7c2	do
jižního	jižní	k2eAgNnSc2d1	jižní
evropského	evropský	k2eAgNnSc2d1	Evropské
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
kdysi	kdysi	k6eAd1	kdysi
sahala	sahat	k5eAaImAgFnS	sahat
severozápadní	severozápadní	k2eAgFnPc4d1	severozápadní
hranice	hranice	k1gFnPc4	hranice
jeho	jeho	k3xOp3gNnSc4	jeho
rozšíření	rozšíření	k1gNnSc4	rozšíření
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
zde	zde	k6eAd1	zde
již	již	k6eAd1	již
nežije	žít	k5eNaImIp3nS	žít
<g/>
.	.	kIx.	.
</s>
<s>
Nejbližší	blízký	k2eAgNnSc1d3	nejbližší
známé	známý	k2eAgNnSc1d1	známé
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
chrobák	chrobák	k1gMnSc1	chrobák
révový	révový	k2eAgMnSc1d1	révový
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
jihozápadním	jihozápadní	k2eAgNnSc6d1	jihozápadní
Slovensku	Slovensko	k1gNnSc6	Slovensko
poblíž	poblíž	k7c2	poblíž
Nového	Nového	k2eAgInSc2d1	Nového
Mesta	Mest	k1gInSc2	Mest
nad	nad	k7c4	nad
Váhom	Váhom	k1gInSc4	Váhom
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
brouci	brouk	k1gMnPc1	brouk
ztratili	ztratit	k5eAaPmAgMnP	ztratit
schopnost	schopnost	k1gFnSc4	schopnost
létat	létat	k5eAaImF	létat
a	a	k8xC	a
nemohou	moct	k5eNaImIp3nP	moct
se	se	k3xPyFc4	se
snadno	snadno	k6eAd1	snadno
šířit	šířit	k5eAaImF	šířit
do	do	k7c2	do
větších	veliký	k2eAgFnPc2d2	veliký
vzdálenosti	vzdálenost	k1gFnSc3	vzdálenost
<g/>
.	.	kIx.	.
</s>
<s>
Žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
koloniích	kolonie	k1gFnPc6	kolonie
na	na	k7c6	na
osluněných	osluněný	k2eAgFnPc6d1	osluněná
stráních	stráň	k1gFnPc6	stráň
na	na	k7c6	na
místech	místo	k1gNnPc6	místo
s	s	k7c7	s
řídce	řídce	k6eAd1	řídce
zapojeným	zapojený	k2eAgInSc7d1	zapojený
rostlinným	rostlinný	k2eAgInSc7d1	rostlinný
pokryvem	pokryv	k1gInSc7	pokryv
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
jim	on	k3xPp3gMnPc3	on
vyhovují	vyhovovat	k5eAaImIp3nP	vyhovovat
suchá	suchý	k2eAgNnPc1d1	suché
místa	místo	k1gNnPc1	místo
stepního	stepní	k2eAgInSc2d1	stepní
charakteru	charakter	k1gInSc2	charakter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Chrobák	chrobák	k1gMnSc1	chrobák
révový	révový	k2eAgMnSc1d1	révový
je	být	k5eAaImIp3nS	být
15	[number]	k4	15
až	až	k9	až
25	[number]	k4	25
mm	mm	kA	mm
velký	velký	k2eAgMnSc1d1	velký
brouk	brouk	k1gMnSc1	brouk
oválného	oválný	k2eAgMnSc2d1	oválný
a	a	k8xC	a
robustního	robustní	k2eAgNnSc2d1	robustní
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
zbarvený	zbarvený	k2eAgMnSc1d1	zbarvený
je	být	k5eAaImIp3nS	být
tmavě	tmavě	k6eAd1	tmavě
až	až	k9	až
černě	černě	k6eAd1	černě
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
mírně	mírně	k6eAd1	mírně
lesklý	lesklý	k2eAgInSc1d1	lesklý
<g/>
.	.	kIx.	.
</s>
<s>
Mimořádně	mimořádně	k6eAd1	mimořádně
mohutná	mohutný	k2eAgFnSc1d1	mohutná
hlava	hlava	k1gFnSc1	hlava
se	s	k7c7	s
silnými	silný	k2eAgNnPc7d1	silné
kusadly	kusadla	k1gNnPc7	kusadla
je	být	k5eAaImIp3nS	být
vpředu	vpředu	k7c2	vpředu
širší	široký	k2eAgFnSc2d2	širší
než	než	k8xS	než
vzadu	vzadu	k6eAd1	vzadu
<g/>
.	.	kIx.	.
</s>
<s>
Srostlé	srostlý	k2eAgFnPc1d1	srostlá
krovky	krovka	k1gFnPc1	krovka
trojúhelníkovitého	trojúhelníkovitý	k2eAgInSc2d1	trojúhelníkovitý
tvaru	tvar	k1gInSc2	tvar
jsou	být	k5eAaImIp3nP	být
stejně	stejně	k6eAd1	stejně
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
jako	jako	k8xC	jako
štít	štít	k1gInSc1	štít
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
má	mít	k5eAaImIp3nS	mít
kusadla	kusadla	k1gNnPc4	kusadla
rozvětvená	rozvětvený	k2eAgNnPc4d1	rozvětvené
a	a	k8xC	a
na	na	k7c6	na
mandibulách	mandibula	k1gFnPc6	mandibula
má	mít	k5eAaImIp3nS	mít
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
výrůstky	výrůstek	k1gInPc4	výrůstek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životní	životní	k2eAgInSc1d1	životní
cyklus	cyklus	k1gInSc1	cyklus
==	==	k?	==
</s>
</p>
<p>
<s>
Počátkem	počátkem	k7c2	počátkem
jara	jaro	k1gNnSc2	jaro
vylézají	vylézat	k5eAaImIp3nP	vylézat
z	z	k7c2	z
půdy	půda	k1gFnSc2	půda
vylíhnutá	vylíhnutý	k2eAgFnSc1d1	vylíhnutá
imaga	imaga	k1gFnSc1	imaga
a	a	k8xC	a
budují	budovat	k5eAaImIp3nP	budovat
si	se	k3xPyFc3	se
okolo	okolo	k7c2	okolo
10	[number]	k4	10
cm	cm	kA	cm
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
nory	nora	k1gFnSc2	nora
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
začátkem	začátek	k1gInSc7	začátek
doby	doba	k1gFnSc2	doba
páření	páření	k1gNnSc2	páření
samci	samec	k1gMnSc3	samec
vyhledávají	vyhledávat	k5eAaImIp3nP	vyhledávat
nory	nora	k1gFnPc1	nora
samic	samice	k1gFnPc2	samice
a	a	k8xC	a
stěhují	stěhovat	k5eAaImIp3nP	stěhovat
se	se	k3xPyFc4	se
k	k	k7c3	k
nim	on	k3xPp3gMnPc3	on
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
samec	samec	k1gMnSc1	samec
snaží	snažit	k5eAaImIp3nS	snažit
dostat	dostat	k5eAaPmF	dostat
k	k	k7c3	k
samici	samice	k1gFnSc3	samice
<g/>
,	,	kIx,	,
u	u	k7c2	u
které	který	k3yIgFnSc2	který
již	již	k6eAd1	již
samec	samec	k1gMnSc1	samec
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
a	a	k8xC	a
tehdy	tehdy	k6eAd1	tehdy
dochází	docházet	k5eAaImIp3nS	docházet
mezi	mezi	k7c7	mezi
samci	samec	k1gMnPc1	samec
k	k	k7c3	k
souboji	souboj	k1gInSc3	souboj
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
uvnitř	uvnitř	k6eAd1	uvnitř
zatarasí	zatarasit	k5eAaPmIp3nS	zatarasit
vchod	vchod	k1gInSc4	vchod
hlavou	hlava	k1gFnSc7	hlava
a	a	k8xC	a
štítem	štít	k1gInSc7	štít
<g/>
,	,	kIx,	,
přetlačují	přetlačovat	k5eAaImIp3nP	přetlačovat
se	se	k3xPyFc4	se
a	a	k8xC	a
dochází	docházet	k5eAaImIp3nS	docházet
i	i	k9	i
na	na	k7c4	na
boj	boj	k1gInSc4	boj
kusadly	kusadla	k1gNnPc7	kusadla
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
spáření	spáření	k1gNnSc6	spáření
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
noře	nora	k1gFnSc6	nora
<g/>
,	,	kIx,	,
začnou	začít	k5eAaPmIp3nP	začít
společně	společně	k6eAd1	společně
budovat	budovat	k5eAaImF	budovat
štolu	štola	k1gFnSc4	štola
vedoucí	vedoucí	k2eAgFnSc4d1	vedoucí
nejprve	nejprve	k6eAd1	nejprve
20	[number]	k4	20
až	až	k9	až
30	[number]	k4	30
cm	cm	kA	cm
šikmo	šikmo	k6eAd1	šikmo
a	a	k8xC	a
pak	pak	k6eAd1	pak
50	[number]	k4	50
až	až	k9	až
60	[number]	k4	60
cm	cm	kA	cm
kolmo	kolmo	k6eAd1	kolmo
dolů	dolů	k6eAd1	dolů
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
svislé	svislý	k2eAgFnSc2d1	svislá
části	část	k1gFnSc2	část
odbočuje	odbočovat	k5eAaImIp3nS	odbočovat
6	[number]	k4	6
až	až	k9	až
11	[number]	k4	11
krátkých	krátké	k1gNnPc2	krátké
chodbiček	chodbička	k1gFnPc2	chodbička
zakončených	zakončený	k2eAgFnPc2d1	zakončená
komůrkami	komůrka	k1gFnPc7	komůrka
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
kusadly	kusadla	k1gNnPc7	kusadla
ukrajuje	ukrajovat	k5eAaImIp3nS	ukrajovat
mladé	mladý	k2eAgInPc4d1	mladý
listy	list	k1gInPc4	list
a	a	k8xC	a
květy	květ	k1gInPc4	květ
rostlin	rostlina	k1gFnPc2	rostlina
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vinné	vinný	k2eAgFnPc4d1	vinná
révy	réva	k1gFnPc4	réva
<g/>
,	,	kIx,	,
odtud	odtud	k6eAd1	odtud
druhové	druhový	k2eAgNnSc4d1	druhové
jméno	jméno	k1gNnSc4	jméno
"	"	kIx"	"
<g/>
révový	révový	k2eAgMnSc1d1	révový
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
nosí	nosit	k5eAaImIp3nP	nosit
je	on	k3xPp3gFnPc4	on
do	do	k7c2	do
nory	nora	k1gFnSc2	nora
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	on	k3xPp3gInPc4	on
pěchuje	pěchovat	k5eAaImIp3nS	pěchovat
do	do	k7c2	do
komůrek	komůrka	k1gFnPc2	komůrka
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gMnSc1	samec
mezitím	mezitím	k6eAd1	mezitím
stráží	strážit	k5eAaImIp3nS	strážit
ústí	ústí	k1gNnSc4	ústí
nory	nora	k1gFnSc2	nora
před	před	k7c7	před
zabráním	zabránit	k5eAaPmIp1nS	zabránit
jiným	jiný	k2eAgInSc7d1	jiný
párem	pár	k1gInSc7	pár
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
naplnění	naplnění	k1gNnSc6	naplnění
komůrek	komůrka	k1gFnPc2	komůrka
uloží	uložit	k5eAaPmIp3nS	uložit
samice	samice	k1gFnSc1	samice
do	do	k7c2	do
každé	každý	k3xTgFnSc2	každý
6	[number]	k4	6
mm	mm	kA	mm
velké	velký	k2eAgNnSc1d1	velké
vajíčko	vajíčko	k1gNnSc1	vajíčko
a	a	k8xC	a
zazátkuje	zazátkovat	k5eAaPmIp3nS	zazátkovat
hlínou	hlína	k1gFnSc7	hlína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c4	za
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
se	se	k3xPyFc4	se
vylíhnou	vylíhnout	k5eAaPmIp3nP	vylíhnout
larvy	larva	k1gFnPc1	larva
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nP	živit
připravenými	připravený	k2eAgFnPc7d1	připravená
a	a	k8xC	a
nakvašenými	nakvašený	k2eAgFnPc7d1	nakvašená
rostlinami	rostlina	k1gFnPc7	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
měsíce	měsíc	k1gInSc2	měsíc
projdou	projít	k5eAaPmIp3nP	projít
třemi	tři	k4xCgInPc7	tři
instary	instar	k1gInPc7	instar
a	a	k8xC	a
zakuklí	zakuklit	k5eAaPmIp3nP	zakuklit
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Trvá	trvat	k5eAaImIp3nS	trvat
asi	asi	k9	asi
čtvrt	čtvrt	k1gFnSc4	čtvrt
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
larvy	larva	k1gFnPc1	larva
v	v	k7c6	v
kuklách	kukla	k1gFnPc6	kukla
promění	proměnit	k5eAaPmIp3nS	proměnit
v	v	k7c4	v
dospělce	dospělec	k1gMnPc4	dospělec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
jara	jaro	k1gNnSc2	jaro
z	z	k7c2	z
kukel	kukla	k1gFnPc2	kukla
vyhrabou	vyhrabat	k5eAaPmIp3nP	vyhrabat
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ohrožení	ohrožení	k1gNnSc1	ohrožení
==	==	k?	==
</s>
</p>
<p>
<s>
Počátkem	počátkem	k7c2	počátkem
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
žil	žít	k5eAaImAgMnS	žít
chrobák	chrobák	k1gMnSc1	chrobák
révový	révový	k2eAgMnSc1d1	révový
i	i	k9	i
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
vyhynulý	vyhynulý	k2eAgInSc4d1	vyhynulý
druh	druh	k1gInSc4	druh
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
byl	být	k5eAaImAgInS	být
vyhuben	vyhubit	k5eAaPmNgInS	vyhubit
zarůstáním	zarůstání	k1gNnSc7	zarůstání
vhodných	vhodný	k2eAgInPc2d1	vhodný
terénů	terén	k1gInPc2	terén
náletovými	náletový	k2eAgFnPc7d1	náletová
dřevinami	dřevina	k1gFnPc7	dřevina
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gFnSc7	jejich
přeměnou	přeměna	k1gFnSc7	přeměna
na	na	k7c6	na
intenzivně	intenzivně	k6eAd1	intenzivně
obdělávanou	obdělávaný	k2eAgFnSc7d1	obdělávaná
ornou	orný	k2eAgFnSc7d1	orná
půdou	půda	k1gFnSc7	půda
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
plošné	plošný	k2eAgNnSc1d1	plošné
používání	používání	k1gNnSc1	používání
hnojiv	hnojivo	k1gNnPc2	hnojivo
a	a	k8xC	a
insekticidů	insekticid	k1gInPc2	insekticid
proti	proti	k7c3	proti
hmyzu	hmyz	k1gInSc3	hmyz
přispělo	přispět	k5eAaPmAgNnS	přispět
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
vymizení	vymizení	k1gNnSc3	vymizení
z	z	k7c2	z
české	český	k2eAgFnSc2d1	Česká
přírody	příroda	k1gFnSc2	příroda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
chrobák	chrobák	k1gMnSc1	chrobák
révový	révový	k2eAgMnSc1d1	révový
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Chrobák	chrobák	k1gMnSc1	chrobák
révový	révový	k2eAgMnSc1d1	révový
-	-	kIx~	-
foto	foto	k1gNnSc1	foto
dvou	dva	k4xCgMnPc2	dva
soupeřů	soupeř	k1gMnPc2	soupeř
</s>
</p>
