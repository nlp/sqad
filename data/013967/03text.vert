<s>
Organizace	organizace	k1gFnSc1
islámské	islámský	k2eAgFnSc2d1
spolupráce	spolupráce	k1gFnSc2
</s>
<s>
Organizace	organizace	k1gFnSc1
islámské	islámský	k2eAgFnSc2d1
spolupráce	spolupráce	k1gFnSc2
</s>
<s>
členské	členský	k2eAgInPc1d1
státy	stát	k1gInPc1
pozorovatelé	pozorovatel	k1gMnPc1
pozastavené	pozastavený	k2eAgNnSc4d1
členství	členství	k1gNnSc4
Zkratka	zkratka	k1gFnSc1
</s>
<s>
OIC	OIC	kA
<g/>
/	/	kIx~
<g/>
OCI	OCI	kA
Vznik	vznik	k1gInSc1
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1969	#num#	k4
Typ	typa	k1gFnPc2
</s>
<s>
mezinárodní	mezinárodní	k2eAgNnSc4d1
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Džidda	Džidda	k1gFnSc1
<g/>
,	,	kIx,
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
21	#num#	k4
<g/>
°	°	k?
<g/>
30	#num#	k4
<g/>
′	′	k?
<g/>
36,2	36,2	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
39	#num#	k4
<g/>
°	°	k?
<g/>
10	#num#	k4
<g/>
′	′	k?
<g/>
57,3	57,3	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Úřední	úřední	k2eAgInSc4d1
jazyk	jazyk	k1gInSc4
</s>
<s>
arabština	arabština	k1gFnSc1
<g/>
,	,	kIx,
angličtina	angličtina	k1gFnSc1
<g/>
,	,	kIx,
francouzština	francouzština	k1gFnSc1
Členové	člen	k1gMnPc1
</s>
<s>
57	#num#	k4
zemí	zem	k1gFnPc2
Generální	generální	k2eAgMnSc1d1
tajemník	tajemník	k1gMnSc1
</s>
<s>
Ekmeleddin	Ekmeleddin	k2eAgMnSc1d1
İ	İ	k5eAaPmIp1nS
Lídr	lídr	k1gMnSc1
</s>
<s>
Yousef	Yousef	k1gInSc1
Al-Othaimeen	Al-Othaimena	k1gFnPc2
(	(	kIx(
<g/>
od	od	k7c2
2016	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Iyad	Iyad	k1gInSc1
bin	bin	k?
Amin	amin	k1gInSc1
Madani	Madaň	k1gFnSc3
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
–	–	k?
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Ekmeleddin	Ekmeleddin	k2eAgMnSc1d1
İ	İ	k5eAaPmIp1nS
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.oic-oci.org	www.oic-oci.org	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Organizace	organizace	k1gFnSc1
islámské	islámský	k2eAgFnSc2d1
spolupráce	spolupráce	k1gFnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
Organisation	Organisation	k1gInSc1
of	of	k?
Islamic	Islamice	k1gInPc2
Cooperation	Cooperation	k1gInSc4
–	–	k?
OIC	OIC	kA
<g/>
,	,	kIx,
francouzsky	francouzsky	k6eAd1
Organisation	Organisation	k1gInSc1
de	de	k?
la	la	k1gNnSc7
Coopération	Coopération	k1gInSc1
Islamique	Islamique	k1gInSc1
–	–	k?
OCI	OCI	kA
<g/>
,	,	kIx,
arabsky	arabsky	k6eAd1
م	م	k?
ا	ا	k?
ا	ا	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
sestávající	sestávající	k2eAgFnSc1d1
z	z	k7c2
57	#num#	k4
členských	členský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
snahou	snaha	k1gFnSc7
je	být	k5eAaImIp3nS
být	být	k5eAaImF
kolektivním	kolektivní	k2eAgInSc7d1
hlasem	hlas	k1gInSc7
islámského	islámský	k2eAgInSc2d1
světa	svět	k1gInSc2
(	(	kIx(
<g/>
Umma	Umma	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
chránit	chránit	k5eAaImF
zájmy	zájem	k1gInPc4
muslimů	muslim	k1gMnPc2
a	a	k8xC
zajistit	zajistit	k5eAaPmF
jejich	jejich	k3xOp3gInSc4
rozvoj	rozvoj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
největší	veliký	k2eAgFnSc4d3
mezinárodní	mezinárodní	k2eAgFnSc4d1
organizaci	organizace	k1gFnSc4
mimo	mimo	k7c4
Organizaci	organizace	k1gFnSc4
spojených	spojený	k2eAgInPc2d1
národů	národ	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Jejími	její	k3xOp3gMnPc7
oficiálními	oficiální	k2eAgMnPc7d1
jazyky	jazyk	k1gMnPc7
jsou	být	k5eAaImIp3nP
arabština	arabština	k1gFnSc1
<g/>
,	,	kIx,
angličtina	angličtina	k1gFnSc1
a	a	k8xC
francouzština	francouzština	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c4
28	#num#	k4
<g/>
.	.	kIx.
červnem	červen	k1gInSc7
2011	#num#	k4
se	se	k3xPyFc4
nazývala	nazývat	k5eAaImAgFnS
Organizace	organizace	k1gFnSc1
islámské	islámský	k2eAgFnSc2d1
konference	konference	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Členské	členský	k2eAgFnPc1d1
země	zem	k1gFnPc1
</s>
<s>
Organizace	organizace	k1gFnSc1
islámské	islámský	k2eAgFnSc2d1
spolupráce	spolupráce	k1gFnSc2
má	mít	k5eAaImIp3nS
57	#num#	k4
členů	člen	k1gMnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgFnPc2
všechny	všechen	k3xTgFnPc1
kromě	kromě	k7c2
Palestiny	Palestina	k1gFnSc2
jsou	být	k5eAaImIp3nP
OSN	OSN	kA
uznány	uznat	k5eAaPmNgInP
jako	jako	k9
státy	stát	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
z	z	k7c2
nich	on	k3xPp3gInPc2
<g/>
,	,	kIx,
hlavně	hlavně	k6eAd1
v	v	k7c6
západní	západní	k2eAgFnSc6d1
Africe	Afrika	k1gFnSc6
<g/>
,	,	kIx,
nejsou	být	k5eNaImIp3nP
státy	stát	k1gInPc1
s	s	k7c7
muslimskou	muslimský	k2eAgFnSc7d1
většinou	většina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
státy	stát	k1gInPc4
s	s	k7c7
významnou	významný	k2eAgFnSc7d1
muslimskou	muslimský	k2eAgFnSc7d1
menšinou	menšina	k1gFnSc7
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
např.	např.	kA
Rusko	Rusko	k1gNnSc1
a	a	k8xC
Thajsko	Thajsko	k1gNnSc1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
pozorovatelskými	pozorovatelský	k2eAgInPc7d1
státy	stát	k1gInPc7
organizace	organizace	k1gFnSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
např.	např.	kA
Indie	Indie	k1gFnSc2
a	a	k8xC
Etiopie	Etiopie	k1gFnSc2
členy	člen	k1gInPc4
nejsou	být	k5eNaImIp3nP
<g/>
.	.	kIx.
</s>
<s>
Členský	členský	k2eAgInSc1d1
stát	stát	k1gInSc1
</s>
<s>
Vstup	vstup	k1gInSc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
Afghánistán	Afghánistán	k1gInSc1
Afghánistán	Afghánistán	k1gInSc1
</s>
<s>
1969	#num#	k4
</s>
<s>
Členství	členství	k1gNnSc1
přerušeno	přerušen	k2eAgNnSc1d1
v	v	k7c6
období	období	k1gNnSc6
1980	#num#	k4
–	–	k?
březen	březen	k1gInSc1
1989	#num#	k4
</s>
<s>
Alžírsko	Alžírsko	k1gNnSc1
Alžírsko	Alžírsko	k1gNnSc1
</s>
<s>
1969	#num#	k4
</s>
<s>
Čad	Čad	k1gInSc1
Čad	Čad	k1gInSc1
</s>
<s>
1969	#num#	k4
</s>
<s>
Egypt	Egypt	k1gInSc1
Egypt	Egypt	k1gInSc1
</s>
<s>
1969	#num#	k4
</s>
<s>
Členství	členství	k1gNnSc1
přerušeno	přerušen	k2eAgNnSc1d1
v	v	k7c6
období	období	k1gNnSc6
květen	květno	k1gNnPc2
1979	#num#	k4
–	–	k?
březen	březen	k1gInSc1
1984	#num#	k4
</s>
<s>
Guinea	Guinea	k1gFnSc1
Guinea	guinea	k1gFnSc2
</s>
<s>
1969	#num#	k4
</s>
<s>
Indonésie	Indonésie	k1gFnSc1
Indonésie	Indonésie	k1gFnSc1
</s>
<s>
1969	#num#	k4
</s>
<s>
Írán	Írán	k1gInSc1
Írán	Írán	k1gInSc1
</s>
<s>
1969	#num#	k4
</s>
<s>
Jordánsko	Jordánsko	k1gNnSc1
Jordánsko	Jordánsko	k1gNnSc1
</s>
<s>
1969	#num#	k4
</s>
<s>
Kuvajt	Kuvajt	k1gInSc1
Kuvajt	Kuvajt	k1gInSc1
</s>
<s>
1969	#num#	k4
</s>
<s>
Libanon	Libanon	k1gInSc1
Libanon	Libanon	k1gInSc1
</s>
<s>
1969	#num#	k4
</s>
<s>
Libye	Libye	k1gFnSc1
Libye	Libye	k1gFnSc1
</s>
<s>
1969	#num#	k4
</s>
<s>
Malajsie	Malajsie	k1gFnSc1
Malajsie	Malajsie	k1gFnSc2
</s>
<s>
1969	#num#	k4
</s>
<s>
Mali	Mali	k1gNnSc1
Mali	Mali	k1gNnSc2
</s>
<s>
1969	#num#	k4
</s>
<s>
Mauritánie	Mauritánie	k1gFnSc1
Mauritánie	Mauritánie	k1gFnSc2
</s>
<s>
1969	#num#	k4
</s>
<s>
Maroko	Maroko	k1gNnSc1
Maroko	Maroko	k1gNnSc1
</s>
<s>
1969	#num#	k4
</s>
<s>
Niger	Niger	k1gInSc1
Niger	Niger	k1gInSc1
</s>
<s>
1969	#num#	k4
</s>
<s>
Pákistán	Pákistán	k1gInSc1
Pákistán	Pákistán	k1gInSc1
</s>
<s>
1969	#num#	k4
</s>
<s>
Blokuje	blokovat	k5eAaImIp3nS
členství	členství	k1gNnSc4
Indie	Indie	k1gFnSc2
</s>
<s>
Palestinská	palestinský	k2eAgFnSc1d1
autonomie	autonomie	k1gFnSc1
Palestinská	palestinský	k2eAgFnSc1d1
autonomie	autonomie	k1gFnSc2
<g/>
[	[	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1969	#num#	k4
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
</s>
<s>
1969	#num#	k4
</s>
<s>
Senegal	Senegal	k1gInSc1
Senegal	Senegal	k1gInSc1
</s>
<s>
1969	#num#	k4
</s>
<s>
Súdán	Súdán	k1gInSc1
Súdán	Súdán	k1gInSc1
</s>
<s>
1969	#num#	k4
</s>
<s>
Somálsko	Somálsko	k1gNnSc1
Somálsko	Somálsko	k1gNnSc1
</s>
<s>
1969	#num#	k4
</s>
<s>
Tunisko	Tunisko	k1gNnSc1
Tunisko	Tunisko	k1gNnSc1
</s>
<s>
1969	#num#	k4
</s>
<s>
Turecko	Turecko	k1gNnSc1
Turecko	Turecko	k1gNnSc1
</s>
<s>
1969	#num#	k4
</s>
<s>
Jemen	Jemen	k1gInSc1
Jemen	Jemen	k1gInSc1
</s>
<s>
1969	#num#	k4
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1990	#num#	k4
Jemenská	jemenský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
spojena	spojit	k5eAaPmNgFnS
s	s	k7c7
Jižním	jižní	k2eAgInSc7d1
Jemenem	Jemen	k1gInSc7
</s>
<s>
Bahrajn	Bahrajn	k1gMnSc1
Bahrajn	Bahrajn	k1gMnSc1
</s>
<s>
1970	#num#	k4
</s>
<s>
Omán	Omán	k1gInSc1
Omán	Omán	k1gInSc1
</s>
<s>
1970	#num#	k4
</s>
<s>
Katar	katar	k1gMnSc1
Katar	katar	k1gMnSc1
</s>
<s>
1970	#num#	k4
</s>
<s>
Sýrie	Sýrie	k1gFnSc1
Sýrie	Sýrie	k1gFnSc1
</s>
<s>
1970	#num#	k4
</s>
<s>
Členství	členství	k1gNnSc1
pozastaveno	pozastaven	k2eAgNnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
arabské	arabský	k2eAgInPc1d1
emiráty	emirát	k1gInPc1
Spojené	spojený	k2eAgInPc1d1
arabské	arabský	k2eAgInPc1d1
emiráty	emirát	k1gInPc1
</s>
<s>
1970	#num#	k4
</s>
<s>
Sierra	Sierra	k1gFnSc1
Leone	Leo	k1gMnSc5
Sierra	Sierra	k1gFnSc1
Leone	Leo	k1gMnSc5
</s>
<s>
1972	#num#	k4
</s>
<s>
Bangladéš	Bangladéš	k1gInSc1
Bangladéš	Bangladéš	k1gInSc1
</s>
<s>
1974	#num#	k4
</s>
<s>
Gabon	Gabon	k1gMnSc1
Gabon	Gabon	k1gMnSc1
</s>
<s>
1974	#num#	k4
</s>
<s>
Gambie	Gambie	k1gFnSc1
Gambie	Gambie	k1gFnSc2
</s>
<s>
1974	#num#	k4
</s>
<s>
Guinea-Bissau	Guinea-Bissa	k2eAgFnSc4d1
Guinea-Bissau	Guinea-Bissaa	k1gFnSc4
</s>
<s>
1974	#num#	k4
</s>
<s>
Uganda	Uganda	k1gFnSc1
Uganda	Uganda	k1gFnSc1
</s>
<s>
1974	#num#	k4
</s>
<s>
Burkina	Burkina	k1gMnSc1
Faso	Faso	k1gMnSc1
Burkina	Burkina	k1gMnSc1
Faso	Faso	k1gMnSc1
</s>
<s>
1975	#num#	k4
</s>
<s>
Kamerun	Kamerun	k1gInSc1
Kamerun	Kamerun	k1gInSc1
</s>
<s>
1975	#num#	k4
</s>
<s>
Komory	komora	k1gFnPc1
Komory	komora	k1gFnSc2
</s>
<s>
1976	#num#	k4
</s>
<s>
Irák	Irák	k1gInSc1
Irák	Irák	k1gInSc1
</s>
<s>
1976	#num#	k4
</s>
<s>
Maledivy	Maledivy	k1gFnPc1
Maledivy	Maledivy	k1gFnPc1
</s>
<s>
1976	#num#	k4
</s>
<s>
Džibutsko	Džibutsko	k6eAd1
Džibutsko	Džibutsko	k1gNnSc1
</s>
<s>
1978	#num#	k4
</s>
<s>
Benin	Benin	k2eAgInSc1d1
Benin	Benin	k1gInSc1
</s>
<s>
1982	#num#	k4
</s>
<s>
Brunej	Brunat	k5eAaImRp2nS,k5eAaPmRp2nS
Brunej	Brunej	k1gMnSc5
</s>
<s>
1984	#num#	k4
</s>
<s>
Nigérie	Nigérie	k1gFnSc1
Nigérie	Nigérie	k1gFnSc1
</s>
<s>
1986	#num#	k4
</s>
<s>
Ázerbájdžán	Ázerbájdžán	k1gInSc1
Ázerbájdžán	Ázerbájdžán	k1gInSc1
</s>
<s>
1991	#num#	k4
</s>
<s>
Albánie	Albánie	k1gFnSc1
Albánie	Albánie	k1gFnSc2
</s>
<s>
1992	#num#	k4
</s>
<s>
Kyrgyzstán	Kyrgyzstán	k2eAgInSc1d1
Kyrgyzstán	Kyrgyzstán	k1gInSc1
</s>
<s>
1992	#num#	k4
</s>
<s>
Tádžikistán	Tádžikistán	k1gInSc1
Tádžikistán	Tádžikistán	k1gInSc1
</s>
<s>
1992	#num#	k4
</s>
<s>
Turkmenistán	Turkmenistán	k1gInSc1
Turkmenistán	Turkmenistán	k1gInSc1
</s>
<s>
1992	#num#	k4
</s>
<s>
Mosambik	Mosambik	k1gInSc1
Mosambik	Mosambik	k1gInSc1
</s>
<s>
1994	#num#	k4
</s>
<s>
Kazachstán	Kazachstán	k1gInSc1
Kazachstán	Kazachstán	k1gInSc1
</s>
<s>
1995	#num#	k4
</s>
<s>
Uzbekistán	Uzbekistán	k1gInSc1
Uzbekistán	Uzbekistán	k1gInSc1
</s>
<s>
1995	#num#	k4
</s>
<s>
Surinam	Surinam	k6eAd1
Surinam	Surinam	k1gInSc1
</s>
<s>
1996	#num#	k4
</s>
<s>
Togo	Togo	k1gNnSc1
Togo	Togo	k1gNnSc1
</s>
<s>
1997	#num#	k4
</s>
<s>
Guyana	Guyana	k1gFnSc1
Guyana	Guyana	k1gFnSc1
</s>
<s>
1998	#num#	k4
</s>
<s>
Pobřeží	pobřeží	k1gNnSc1
slonoviny	slonovina	k1gFnSc2
Pobřeží	pobřeží	k1gNnSc2
slonoviny	slonovina	k1gFnSc2
</s>
<s>
2001	#num#	k4
</s>
<s>
Bývalé	bývalý	k2eAgFnPc1d1
členské	členský	k2eAgFnPc1d1
země	zem	k1gFnPc1
</s>
<s>
Zanzibar	Zanzibar	k1gInSc1
Zanzibar	Zanzibar	k1gInSc1
</s>
<s>
1993	#num#	k4
</s>
<s>
Vystoupil	vystoupit	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
</s>
<s>
Pozorující	pozorující	k2eAgInPc1d1
státy	stát	k1gInPc1
</s>
<s>
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
</s>
<s>
1994	#num#	k4
</s>
<s>
Středoafrická	středoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Středoafrická	středoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
1996	#num#	k4
</s>
<s>
Severní	severní	k2eAgInSc1d1
Kypr	Kypr	k1gInSc1
Severní	severní	k2eAgInSc1d1
Kypr	Kypr	k1gInSc1
jako	jako	k8xC,k8xS
„	„	k?
<g/>
Turecký	turecký	k2eAgInSc1d1
kyperský	kyperský	k2eAgInSc1d1
stát	stát	k1gInSc1
<g/>
“	“	k?
</s>
<s>
1979	#num#	k4
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Thajsko	Thajsko	k1gNnSc1
Thajsko	Thajsko	k1gNnSc1
</s>
<s>
1998	#num#	k4
</s>
<s>
Rusko	Rusko	k1gNnSc1
Rusko	Rusko	k1gNnSc1
</s>
<s>
2005	#num#	k4
</s>
<s>
Pozorovatelské	pozorovatelský	k2eAgFnPc1d1
mezinárodní	mezinárodní	k2eAgFnPc1d1
organizace	organizace	k1gFnPc1
</s>
<s>
Liga	liga	k1gFnSc1
arabských	arabský	k2eAgInPc2d1
států	stát	k1gInPc2
</s>
<s>
1975	#num#	k4
</s>
<s>
Organizace	organizace	k1gFnSc1
spojených	spojený	k2eAgInPc2d1
národů	národ	k1gInPc2
</s>
<s>
1976	#num#	k4
</s>
<s>
Hnutí	hnutí	k1gNnSc1
nezúčastněných	zúčastněný	k2eNgFnPc2d1
zemí	zem	k1gFnPc2
</s>
<s>
1977	#num#	k4
</s>
<s>
Africká	africký	k2eAgFnSc1d1
Unie	unie	k1gFnSc1
</s>
<s>
1977	#num#	k4
</s>
<s>
Do	do	k7c2
roku	rok	k1gInSc2
2002	#num#	k4
jako	jako	k8xS,k8xC
Organizace	organizace	k1gFnSc1
africké	africký	k2eAgFnSc2d1
jednoty	jednota	k1gFnSc2
</s>
<s>
Organizace	organizace	k1gFnSc1
ekonomické	ekonomický	k2eAgFnSc2d1
spolupráce	spolupráce	k1gFnSc2
</s>
<s>
1995	#num#	k4
</s>
<s>
Demokracie	demokracie	k1gFnSc1
</s>
<s>
Index	index	k1gInSc1
demokracie	demokracie	k1gFnSc2
za	za	k7c4
rok	rok	k1gInSc4
2010	#num#	k4
sestavovaný	sestavovaný	k2eAgInSc4d1
společností	společnost	k1gFnSc7
Economist	Economist	k1gMnSc1
Intelligence	Intelligence	k1gFnSc2
Unit	Unit	k1gMnSc1
žádnou	žádný	k3yNgFnSc4
zemi	zem	k1gFnSc4
OIC	OIC	kA
neoznačil	označit	k5eNaPmAgMnS
za	za	k7c4
úplnou	úplný	k2eAgFnSc4d1
demokracii	demokracie	k1gFnSc4
a	a	k8xC
jen	jen	k9
3	#num#	k4
země	zem	k1gFnSc2
označil	označit	k5eAaPmAgMnS
za	za	k7c2
nefunkční	funkční	k2eNgFnSc2d1
demokracie	demokracie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbytek	zbytek	k1gInSc1
byl	být	k5eAaImAgInS
zařazen	zařadit	k5eAaPmNgInS
buď	buď	k8xC
mezi	mezi	k7c4
autoritativní	autoritativní	k2eAgInPc4d1
režimy	režim	k1gInPc4
nebo	nebo	k8xC
hybridní	hybridní	k2eAgInPc4d1
režimy	režim	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Svobody	svoboda	k1gFnPc1
</s>
<s>
Ve	v	k7c6
výroční	výroční	k2eAgFnSc6d1
zprávě	zpráva	k1gFnSc6
organizace	organizace	k1gFnSc2
Freedom	Freedom	k1gInSc1
House	house	k1gNnSc1
za	za	k7c4
rok	rok	k1gInSc4
2010	#num#	k4
byly	být	k5eAaImAgFnP
tři	tři	k4xCgFnPc4
státy	stát	k1gInPc7
OIC	OIC	kA
označený	označený	k2eAgInSc4d1
za	za	k7c2
svobodné	svobodný	k2eAgFnSc2d1
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
se	se	k3xPyFc4
týče	týkat	k5eAaImIp3nS
politických	politický	k2eAgNnPc2d1
a	a	k8xC
občanských	občanský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Reportéři	reportér	k1gMnPc1
bez	bez	k7c2
hranic	hranice	k1gFnPc2
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
Indexu	index	k1gInSc6
svobody	svoboda	k1gFnSc2
tisku	tisk	k1gInSc2
za	za	k7c4
rok	rok	k1gInSc4
2010	#num#	k4
označili	označit	k5eAaPmAgMnP
situaci	situace	k1gFnSc4
v	v	k7c6
Mali	Mali	k1gNnSc6
a	a	k8xC
Surinamu	Surinam	k1gInSc6
za	za	k7c4
uspokojivou	uspokojivý	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatní	ostatní	k2eAgInPc1d1
státy	stát	k1gInPc1
patří	patřit	k5eAaImIp3nP
do	do	k7c2
rozmezí	rozmezí	k1gNnSc2
význačné	význačný	k2eAgInPc4d1
problémy	problém	k1gInPc4
až	až	k9
velmi	velmi	k6eAd1
vážná	vážný	k2eAgFnSc1d1
situace	situace	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
V	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
Palestinská	palestinský	k2eAgFnSc1d1
autonomie	autonomie	k1gFnSc1
vystřídala	vystřídat	k5eAaPmAgFnS
Organizaci	organizace	k1gFnSc4
pro	pro	k7c4
osvobození	osvobození	k1gNnSc4
Palestiny	Palestina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Organisation	Organisation	k1gInSc1
of	of	k?
Islamic	Islamice	k1gInPc2
Cooperation	Cooperation	k1gInSc4
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Special	Special	k1gInSc1
Envoy	Envoa	k1gMnSc2
to	ten	k3xDgNnSc1
the	the	k?
Organization	Organization	k1gInSc1
of	of	k?
the	the	k?
Islamic	Islamic	k1gMnSc1
Conference	Conferenec	k1gMnSc2
(	(	kIx(
<g/>
OIC	OIC	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
US	US	kA
Department	department	k1gInSc1
of	of	k?
State	status	k1gInSc5
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
OIC	OIC	kA
changes	changes	k1gInSc4
name	name	k1gFnSc7
<g/>
,	,	kIx,
emblem	embl	k1gInSc7
Archivováno	archivován	k2eAgNnSc4d1
23	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
Pakistan	Pakistan	k1gInSc1
Observer	Observer	k1gInSc1
<g/>
↑	↑	k?
OIC	OIC	kA
member	member	k1gMnSc1
states	states	k1gMnSc1
<g/>
.	.	kIx.
www.oic-oci.org	www.oic-oci.org	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
OIC	OIC	kA
observers	observers	k1gInSc1
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
↑	↑	k?
Democracy	Democracy	k1gInPc1
Index	index	k1gInSc1
2010	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Economist	Economist	k1gInSc1
Intelligence	Intelligence	k1gFnSc2
Unit	Unita	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Freedom	Freedom	k1gInSc1
in	in	k?
the	the	k?
World	World	k1gInSc1
2011	#num#	k4
<g/>
:	:	kIx,
Table	tablo	k1gNnSc6
of	of	k?
Independent	independent	k1gMnSc1
Countries	Countries	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Freedom	Freedom	k1gInSc4
House	house	k1gNnSc1
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Freedom	Freedom	k1gInSc1
of	of	k?
the	the	k?
Press	Press	k1gInSc1
Worldwide	Worldwid	k1gInSc5
in	in	k?
2011	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reporters	Reporters	k1gInSc1
Without	Without	k1gMnSc1
Borders	Borders	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Press	Press	k1gInSc1
Freedom	Freedom	k1gInSc1
Index	index	k1gInSc1
2010	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reporters	Reporters	k1gInSc1
Without	Without	k1gMnSc1
Borders	Borders	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Organizace	organizace	k1gFnSc1
islámské	islámský	k2eAgFnSc2d1
spolupráce	spolupráce	k1gFnSc2
Členové	člen	k1gMnPc1
</s>
<s>
Afghánistán	Afghánistán	k1gInSc1
•	•	k?
Albánie	Albánie	k1gFnSc2
•	•	k?
Alžírsko	Alžírsko	k1gNnSc4
•	•	k?
Ázerbájdžán	Ázerbájdžán	k1gInSc1
•	•	k?
Bahrajn	Bahrajn	k1gInSc1
•	•	k?
Bangladéš	Bangladéš	k1gInSc1
•	•	k?
Benin	Benin	k1gMnSc1
•	•	k?
Burkina	Burkina	k1gMnSc1
Faso	Faso	k1gMnSc1
•	•	k?
Brunej	Brunej	k1gMnSc1
•	•	k?
Čad	Čad	k1gInSc1
•	•	k?
Džibutsko	Džibutsko	k1gNnSc4
•	•	k?
Egypt	Egypt	k1gInSc1
•	•	k?
Gabon	Gabon	k1gInSc1
•	•	k?
Gambie	Gambie	k1gFnSc2
•	•	k?
Guinea	guinea	k1gFnSc2
•	•	k?
Guinea-Bissau	Guinea-Bissaa	k1gFnSc4
•	•	k?
Guyana	Guyana	k1gFnSc1
•	•	k?
Indonésie	Indonésie	k1gFnSc1
•	•	k?
Írán	Írán	k1gInSc1
•	•	k?
Irák	Irák	k1gInSc1
•	•	k?
Jemen	Jemen	k1gInSc1
•	•	k?
Jordánsko	Jordánsko	k1gNnSc4
•	•	k?
Kamerun	Kamerun	k1gInSc1
•	•	k?
Katar	katar	k1gInSc1
•	•	k?
Kazachstán	Kazachstán	k1gInSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Komory	komora	k1gFnSc2
•	•	k?
Kuvajt	Kuvajt	k1gInSc1
•	•	k?
Kyrgyzstán	Kyrgyzstán	k1gInSc1
•	•	k?
Libanon	Libanon	k1gInSc1
•	•	k?
Libye	Libye	k1gFnSc1
•	•	k?
Maledivy	Maledivy	k1gFnPc4
•	•	k?
Malajsie	Malajsie	k1gFnSc1
•	•	k?
Mali	Mali	k1gNnPc2
•	•	k?
Mauritánie	Mauritánie	k1gFnSc2
•	•	k?
Maroko	Maroko	k1gNnSc4
•	•	k?
Mosambik	Mosambik	k1gInSc1
•	•	k?
Niger	Niger	k1gInSc1
•	•	k?
Nigérie	Nigérie	k1gFnSc2
•	•	k?
Omán	Omán	k1gInSc1
•	•	k?
Pákistán	Pákistán	k1gInSc1
•	•	k?
Palestina	Palestina	k1gFnSc1
•	•	k?
Pobřeží	pobřeží	k1gNnSc1
slonoviny	slonovina	k1gFnSc2
•	•	k?
Saúdská	saúdský	k2eAgFnSc1d1
Arábie	Arábie	k1gFnSc1
•	•	k?
Senegal	Senegal	k1gInSc1
•	•	k?
Sierra	Sierra	k1gFnSc1
Leone	Leo	k1gMnSc5
•	•	k?
Somálsko	Somálsko	k1gNnSc4
•	•	k?
Spojené	spojený	k2eAgInPc4d1
arabské	arabský	k2eAgInPc4d1
emiráty	emirát	k1gInPc4
•	•	k?
Súdán	Súdán	k1gInSc1
•	•	k?
Surinam	Surinam	k1gInSc1
•	•	k?
Tádžikistán	Tádžikistán	k1gInSc1
•	•	k?
Turecko	Turecko	k1gNnSc1
•	•	k?
Tunisko	Tunisko	k1gNnSc1
•	•	k?
Togo	Togo	k1gNnSc4
•	•	k?
Turkmenistán	Turkmenistán	k1gInSc1
•	•	k?
Uganda	Uganda	k1gFnSc1
•	•	k?
Uzbekistán	Uzbekistán	k1gInSc4
Pozastavené	pozastavený	k2eAgNnSc4d1
členství	členství	k1gNnSc4
</s>
<s>
Sýrie	Sýrie	k1gFnSc1
Pozorovatelé	pozorovatel	k1gMnPc1
</s>
<s>
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
•	•	k?
Rusko	Rusko	k1gNnSc1
•	•	k?
Středoafrická	středoafrický	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
Severní	severní	k2eAgFnSc1d1
Kypr	Kypr	k1gInSc1
•	•	k?
Thajsko	Thajsko	k1gNnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Islám	islám	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kn	kn	k?
<g/>
20020321711	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
2081073-8	2081073-8	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0004	#num#	k4
0648	#num#	k4
136X	136X	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
nb	nb	k?
<g/>
2012022645	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
302216036	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-nb	lccn-nb	k1gInSc1
<g/>
2012022645	#num#	k4
</s>
