<p>
<s>
Žlutá	žlutý	k2eAgFnSc1d1	žlutá
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
barev	barva	k1gFnPc2	barva
barevného	barevný	k2eAgNnSc2d1	barevné
spektra	spektrum	k1gNnSc2	spektrum
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
barva	barva	k1gFnSc1	barva
monochromatického	monochromatický	k2eAgNnSc2d1	monochromatické
světla	světlo	k1gNnSc2	světlo
o	o	k7c6	o
vlnové	vlnový	k2eAgFnSc6d1	vlnová
délce	délka	k1gFnSc6	délka
mezi	mezi	k7c4	mezi
565	[number]	k4	565
nm	nm	k?	nm
a	a	k8xC	a
590	[number]	k4	590
nm	nm	k?	nm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Žlutá	žlutý	k2eAgFnSc1d1	žlutá
barva	barva	k1gFnSc1	barva
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
základní	základní	k2eAgFnPc4d1	základní
barvy	barva	k1gFnPc4	barva
při	při	k7c6	při
subtraktivním	subtraktivní	k2eAgNnSc6d1	subtraktivní
míchání	míchání	k1gNnSc6	míchání
používaném	používaný	k2eAgNnSc6d1	používané
při	při	k7c6	při
barevném	barevný	k2eAgInSc6d1	barevný
tisku	tisk	k1gInSc6	tisk
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
CMYK	CMYK	kA	CMYK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejím	její	k3xOp3gInSc7	její
doplňkem	doplněk	k1gInSc7	doplněk
je	být	k5eAaImIp3nS	být
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Někteří	některý	k3yIgMnPc1	některý
umělci	umělec	k1gMnPc1	umělec
uznávají	uznávat	k5eAaImIp3nP	uznávat
tradiční	tradiční	k2eAgFnSc4d1	tradiční
teorii	teorie	k1gFnSc4	teorie
barev	barva	k1gFnPc2	barva
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
které	který	k3yIgFnSc2	který
je	být	k5eAaImIp3nS	být
doplňkem	doplněk	k1gInSc7	doplněk
žluté	žlutý	k2eAgFnSc2d1	žlutá
barvy	barva	k1gFnSc2	barva
nachová	nachovat	k5eAaBmIp3nS	nachovat
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc1	použití
a	a	k8xC	a
symbolika	symbolika	k1gFnSc1	symbolika
žluté	žlutý	k2eAgFnSc2d1	žlutá
barvy	barva	k1gFnSc2	barva
==	==	k?	==
</s>
</p>
<p>
<s>
Žlutá	žlutý	k2eAgFnSc1d1	žlutá
je	být	k5eAaImIp3nS	být
velice	velice	k6eAd1	velice
výrazná	výrazný	k2eAgFnSc1d1	výrazná
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
maximum	maximum	k1gNnSc1	maximum
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
žluté	žlutý	k2eAgFnSc2d1	žlutá
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
lidské	lidský	k2eAgNnSc1d1	lidské
oko	oko	k1gNnSc1	oko
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
žlutou	žlutý	k2eAgFnSc4d1	žlutá
barvu	barva	k1gFnSc4	barva
velice	velice	k6eAd1	velice
citlivé	citlivý	k2eAgInPc1d1	citlivý
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
žlutá	žlutat	k5eAaImIp3nS	žlutat
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
upoutání	upoutání	k1gNnSc4	upoutání
pozornosti	pozornost	k1gFnSc2	pozornost
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
varování	varování	k1gNnSc1	varování
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Žlutá	žlutat	k5eAaImIp3nS	žlutat
je	on	k3xPp3gNnPc4	on
(	(	kIx(	(
<g/>
též	též	k9	též
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
)	)	kIx)	)
barvou	barva	k1gFnSc7	barva
signální	signální	k2eAgFnSc7d1	signální
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
účinku	účinek	k1gInSc2	účinek
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
v	v	k7c6	v
kontrastu	kontrast	k1gInSc6	kontrast
s	s	k7c7	s
černou	černý	k2eAgFnSc7d1	černá
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
semaforu	semafor	k1gInSc6	semafor
znamená	znamenat	k5eAaImIp3nS	znamenat
žlutá	žlutý	k2eAgFnSc1d1	žlutá
barva	barva	k1gFnSc1	barva
"	"	kIx"	"
<g/>
pozor	pozor	k1gInSc1	pozor
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
signalizuje	signalizovat	k5eAaImIp3nS	signalizovat
brzkou	brzký	k2eAgFnSc4d1	brzká
změnu	změna	k1gFnSc4	změna
signálu	signál	k1gInSc2	signál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
sportu	sport	k1gInSc6	sport
žlutá	žlutý	k2eAgFnSc1d1	žlutá
barva	barva	k1gFnSc1	barva
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
varování	varování	k1gNnSc4	varování
(	(	kIx(	(
<g/>
žlutá	žlutý	k2eAgFnSc1d1	žlutá
karta	karta	k1gFnSc1	karta
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
automobilových	automobilový	k2eAgInPc6d1	automobilový
závodech	závod	k1gInPc6	závod
signalizuje	signalizovat	k5eAaImIp3nS	signalizovat
žlutá	žlutý	k2eAgFnSc1d1	žlutá
vlajka	vlajka	k1gFnSc1	vlajka
výstrahu	výstraha	k1gFnSc4	výstraha
<g/>
,	,	kIx,	,
auta	auto	k1gNnPc1	auto
se	se	k3xPyFc4	se
nesmí	smět	k5eNaImIp3nP	smět
předjíždět	předjíždět	k5eAaImF	předjíždět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vysoce	vysoce	k6eAd1	vysoce
viditelné	viditelný	k2eAgFnPc1d1	viditelná
žluté	žlutý	k2eAgFnPc1d1	žlutá
barvy	barva	k1gFnPc1	barva
jsou	být	k5eAaImIp3nP	být
používány	používat	k5eAaImNgFnP	používat
pro	pro	k7c4	pro
silniční	silniční	k2eAgNnSc4d1	silniční
konstrukční	konstrukční	k2eAgNnSc4d1	konstrukční
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
,	,	kIx,	,
žlutým	žlutý	k2eAgInSc7d1	žlutý
majáčkem	majáček	k1gInSc7	majáček
jsou	být	k5eAaImIp3nP	být
označena	označit	k5eAaPmNgNnP	označit
vozidla	vozidlo	k1gNnPc1	vozidlo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
ohrozit	ohrozit	k5eAaPmF	ohrozit
okolní	okolní	k2eAgInSc4d1	okolní
provoz	provoz	k1gInSc4	provoz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
a	a	k8xC	a
USA	USA	kA	USA
jsou	být	k5eAaImIp3nP	být
školní	školní	k2eAgInPc1d1	školní
autobusy	autobus	k1gInPc1	autobus
natřeny	natřen	k2eAgInPc1d1	natřen
na	na	k7c4	na
žluto	žluto	k1gNnSc4	žluto
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
viditelnosti	viditelnost	k1gFnSc2	viditelnost
a	a	k8xC	a
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Žlutá	žlutat	k5eAaImIp3nS	žlutat
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
často	často	k6eAd1	často
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
zlatou	zlatý	k2eAgFnSc4d1	zlatá
a	a	k8xC	a
jako	jako	k9	jako
taková	takový	k3xDgFnSc1	takový
je	být	k5eAaImIp3nS	být
symbolem	symbol	k1gInSc7	symbol
věčnosti	věčnost	k1gFnSc2	věčnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jako	jako	k9	jako
"	"	kIx"	"
<g/>
žlutá	žlutý	k2eAgFnSc1d1	žlutá
rasa	rasa	k1gFnSc1	rasa
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
asiaté	asiat	k1gMnPc1	asiat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
USA	USA	kA	USA
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byli	být	k5eAaImAgMnP	být
imigranti	imigrant	k1gMnPc1	imigrant
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
východoasijských	východoasijský	k2eAgFnPc2d1	východoasijská
zemí	zem	k1gFnPc2	zem
hanlivě	hanlivě	k6eAd1	hanlivě
nazývání	nazývání	k1gNnSc2	nazývání
"	"	kIx"	"
<g/>
Žlutá	žlutý	k2eAgFnSc1d1	žlutá
hrozba	hrozba	k1gFnSc1	hrozba
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
tradičním	tradiční	k2eAgNnSc6d1	tradiční
západním	západní	k2eAgNnSc6d1	západní
umění	umění	k1gNnSc6	umění
je	být	k5eAaImIp3nS	být
žlutá	žlutý	k2eAgFnSc1d1	žlutá
barvou	barva	k1gFnSc7	barva
bohatství	bohatství	k1gNnSc2	bohatství
<g/>
,	,	kIx,	,
otcovství	otcovství	k1gNnSc2	otcovství
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
závisti	závist	k1gFnSc2	závist
<g/>
,	,	kIx,	,
Jidáše	jidáš	k1gInSc2	jidáš
<g/>
,	,	kIx,	,
Židů	Žid	k1gMnPc2	Žid
a	a	k8xC	a
synagogy	synagoga	k1gFnSc2	synagoga
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Žlutá	žlutý	k2eAgFnSc1d1	žlutá
barva	barva	k1gFnSc1	barva
byl	být	k5eAaImAgInS	být
symbol	symbol	k1gInSc1	symbol
čínských	čínský	k2eAgMnPc2d1	čínský
císařů	císař	k1gMnPc2	císař
a	a	k8xC	a
čínské	čínský	k2eAgFnSc2d1	čínská
monarchie	monarchie	k1gFnSc2	monarchie
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
to	ten	k3xDgNnSc1	ten
také	také	k9	také
barva	barva	k1gFnSc1	barva
Nové	Nové	k2eAgFnSc2d1	Nové
Strany	strana	k1gFnSc2	strana
v	v	k7c6	v
Čínské	čínský	k2eAgFnSc6d1	čínská
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tužky	tužka	k1gFnPc1	tužka
mají	mít	k5eAaImIp3nP	mít
žlutou	žlutý	k2eAgFnSc4d1	žlutá
barvu	barva	k1gFnSc4	barva
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
spojitosti	spojitost	k1gFnSc3	spojitost
s	s	k7c7	s
Čínou	Čína	k1gFnSc7	Čína
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
grafit	grafit	k1gInSc1	grafit
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
tužky	tužka	k1gFnSc2	tužka
s	s	k7c7	s
čínským	čínský	k2eAgInSc7d1	čínský
grafitem	grafit	k1gInSc7	grafit
se	se	k3xPyFc4	se
barvily	barvit	k5eAaImAgFnP	barvit
na	na	k7c4	na
žluto	žluto	k1gNnSc4	žluto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
jsou	být	k5eAaImIp3nP	být
běžně	běžně	k6eAd1	běžně
žlutá	žlutý	k2eAgNnPc4d1	žluté
taxi	taxi	k1gNnPc4	taxi
<g/>
.	.	kIx.	.
</s>
<s>
Počátek	počátek	k1gInSc1	počátek
této	tento	k3xDgFnSc2	tento
praxe	praxe	k1gFnSc2	praxe
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Harry	Harra	k1gFnSc2	Harra
N.	N.	kA	N.
Allen	Allen	k1gMnSc1	Allen
natřel	natřít	k5eAaPmAgMnS	natřít
své	svůj	k3xOyFgNnSc4	svůj
taxi	taxi	k1gNnSc4	taxi
na	na	k7c4	na
žluto	žluto	k1gNnSc4	žluto
<g/>
,	,	kIx,	,
když	když	k8xS	když
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
žlutá	žlutý	k2eAgFnSc1d1	žlutá
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
nejlépe	dobře	k6eAd3	dobře
viditelnou	viditelný	k2eAgFnSc7d1	viditelná
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Žlutá	žlutat	k5eAaImIp3nS	žlutat
je	být	k5eAaImIp3nS	být
mezi	mezi	k7c7	mezi
mezinárodními	mezinárodní	k2eAgFnPc7d1	mezinárodní
politickými	politický	k2eAgFnPc7d1	politická
organizacemi	organizace	k1gFnPc7	organizace
barva	barva	k1gFnSc1	barva
liberálů	liberál	k1gMnPc2	liberál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Žlutého	žlutý	k2eAgInSc2d1	žlutý
papíru	papír	k1gInSc2	papír
užívají	užívat	k5eAaImIp3nP	užívat
Zlaté	zlatý	k1gInPc1	zlatý
stránky	stránka	k1gFnSc2	stránka
–	–	k?	–
součást	součást	k1gFnSc4	součást
telefonního	telefonní	k2eAgInSc2d1	telefonní
seznamu	seznam	k1gInSc2	seznam
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
tříděn	třídit	k5eAaImNgInS	třídit
podle	podle	k7c2	podle
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dočasné	dočasný	k2eAgFnPc1d1	dočasná
vodorovné	vodorovný	k2eAgFnPc1d1	vodorovná
dopravní	dopravní	k2eAgFnPc1d1	dopravní
značky	značka	k1gFnPc1	značka
jsou	být	k5eAaImIp3nP	být
provedeny	provést	k5eAaPmNgFnP	provést
žlutou	žlutý	k2eAgFnSc7d1	žlutá
barvou	barva	k1gFnSc7	barva
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
trvalých	trvalá	k1gFnPc2	trvalá
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
bílé	bílý	k2eAgFnPc1d1	bílá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
barevném	barevný	k2eAgNnSc6d1	barevné
značení	značení	k1gNnSc6	značení
odporů	odpor	k1gInPc2	odpor
znamená	znamenat	k5eAaImIp3nS	znamenat
žlutá	žlutý	k2eAgFnSc1d1	žlutá
barva	barva	k1gFnSc1	barva
číslici	číslice	k1gFnSc4	číslice
4	[number]	k4	4
</s>
</p>
<p>
<s>
Žlutá	žlutý	k2eAgFnSc1d1	žlutá
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
barva	barva	k1gFnSc1	barva
pro	pro	k7c4	pro
moč	moč	k1gFnSc4	moč
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
žlutá	žlutat	k5eAaImIp3nS	žlutat
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
žlutý	žlutý	k2eAgMnSc1d1	žlutý
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
