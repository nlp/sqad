<s>
Obec	obec	k1gFnSc1	obec
Kvasice	Kvasice	k1gFnSc2	Kvasice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
okrese	okres	k1gInSc6	okres
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
Zlínský	zlínský	k2eAgInSc1d1	zlínský
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přes	přes	k7c4	přes
2000	[number]	k4	2000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
obci	obec	k1gFnSc6	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1141	[number]	k4	1141
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
dni	den	k1gInSc3	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2005	[number]	k4	2005
bylo	být	k5eAaImAgNnS	být
obci	obec	k1gFnSc3	obec
tehdejším	tehdejší	k2eAgMnSc7d1	tehdejší
předsedou	předseda	k1gMnSc7	předseda
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Lubomírem	Lubomír	k1gMnSc7	Lubomír
Zaorálkem	Zaorálek	k1gInSc7	Zaorálek
uděleno	udělen	k2eAgNnSc4d1	uděleno
právo	právo	k1gNnSc4	právo
k	k	k7c3	k
používání	používání	k1gNnSc3	používání
vlajky	vlajka	k1gFnSc2	vlajka
obce	obec	k1gFnSc2	obec
(	(	kIx(	(
<g/>
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
číslo	číslo	k1gNnSc4	číslo
62	[number]	k4	62
Výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
vědu	věda	k1gFnSc4	věda
<g/>
,	,	kIx,	,
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
,	,	kIx,	,
kulturu	kultura	k1gFnSc4	kultura
<g/>
,	,	kIx,	,
mládež	mládež	k1gFnSc4	mládež
a	a	k8xC	a
tělovýchovu	tělovýchova	k1gFnSc4	tělovýchova
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
Parlamentu	parlament	k1gInSc2	parlament
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
(	(	kIx(	(
<g/>
mající	mající	k2eAgInSc1d1	mající
poměr	poměr	k1gInSc1	poměr
šířky	šířka	k1gFnSc2	šířka
ku	k	k7c3	k
délce	délka	k1gFnSc3	délka
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
list	list	k1gInSc1	list
tvořený	tvořený	k2eAgInSc1d1	tvořený
třemi	tři	k4xCgInPc7	tři
vodorovnými	vodorovný	k2eAgInPc7d1	vodorovný
pruhy	pruh	k1gInPc7	pruh
(	(	kIx(	(
<g/>
červeným	červený	k2eAgInSc7d1	červený
<g/>
,	,	kIx,	,
bílým	bílý	k2eAgNnSc7d1	bílé
a	a	k8xC	a
červeným	červená	k1gFnPc3	červená
<g/>
)	)	kIx)	)
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
žerďové	žerďové	k2eAgFnSc6d1	žerďové
části	část	k1gFnSc6	část
horního	horní	k2eAgInSc2d1	horní
pruhu	pruh	k1gInSc2	pruh
je	být	k5eAaImIp3nS	být
zobrazena	zobrazen	k2eAgFnSc1d1	zobrazena
bílá	bílý	k2eAgFnSc1d1	bílá
zavinutá	zavinutý	k2eAgFnSc1d1	zavinutá
střela	střela	k1gFnSc1	střela
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
byla	být	k5eAaImAgFnS	být
slavnostně	slavnostně	k6eAd1	slavnostně
vysvěcena	vysvětit	k5eAaPmNgFnS	vysvětit
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
znakem	znak	k1gInSc7	znak
obce	obec	k1gFnSc2	obec
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
kvasické	kvasický	k2eAgFnSc2d1	kvasický
pouti	pouť	k1gFnSc2	pouť
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
12	[number]	k4	12
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Bílý	bílý	k2eAgInSc1d1	bílý
pruh	pruh	k1gInSc1	pruh
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
části	část	k1gFnSc6	část
vlajky	vlajka	k1gFnSc2	vlajka
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
řeku	řeka	k1gFnSc4	řeka
Moravu	Morava	k1gFnSc4	Morava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
katastrem	katastr	k1gInSc7	katastr
Kvasic	Kvasice	k1gInPc2	Kvasice
protéká	protékat	k5eAaImIp3nS	protékat
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
zavinutá	zavinutý	k2eAgFnSc1d1	zavinutá
střela	střela	k1gFnSc1	střela
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
odřivous	odřivous	k1gMnSc1	odřivous
<g/>
)	)	kIx)	)
na	na	k7c6	na
červeném	červený	k2eAgNnSc6d1	červené
poli	pole	k1gNnSc6	pole
je	být	k5eAaImIp3nS	být
odvozena	odvodit	k5eAaPmNgFnS	odvodit
ze	z	k7c2	z
znaku	znak	k1gInSc2	znak
a	a	k8xC	a
patřila	patřit	k5eAaImAgFnS	patřit
dříve	dříve	k6eAd2	dříve
šlechtickému	šlechtický	k2eAgInSc3d1	šlechtický
rodu	rod	k1gInSc3	rod
Benešovců	Benešovec	k1gInPc2	Benešovec
z	z	k7c2	z
Benešova	Benešov	k1gInSc2	Benešov
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgNnSc2	který
pochází	pocházet	k5eAaImIp3nP	pocházet
první	první	k4xOgMnSc1	první
písemně	písemně	k6eAd1	písemně
doložený	doložený	k2eAgMnSc1d1	doložený
majitel	majitel	k1gMnSc1	majitel
obce	obec	k1gFnSc2	obec
Ondřej	Ondřej	k1gMnSc1	Ondřej
z	z	k7c2	z
Benešova	Benešov	k1gInSc2	Benešov
a	a	k8xC	a
Kvasic	Kvasice	k1gInPc2	Kvasice
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
znak	znak	k1gInSc1	znak
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
obec	obec	k1gFnSc1	obec
používá	používat	k5eAaImIp3nS	používat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1360	[number]	k4	1360
<g/>
,	,	kIx,	,
podobá	podobat	k5eAaImIp3nS	podobat
znakům	znak	k1gInPc3	znak
obcí	obec	k1gFnSc7	obec
Kravaře	kravař	k1gMnSc2	kravař
a	a	k8xC	a
Bílovec	Bílovec	k1gInSc4	Bílovec
(	(	kIx(	(
<g/>
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
rovněž	rovněž	k9	rovněž
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
majetkem	majetek	k1gInSc7	majetek
spřízněných	spřízněný	k2eAgInPc2d1	spřízněný
rodů	rod	k1gInPc2	rod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
předcházely	předcházet	k5eAaImAgInP	předcházet
svěcení	svěcení	k1gNnSc4	svěcení
obecních	obecní	k2eAgInPc2d1	obecní
symbolů	symbol	k1gInPc2	symbol
několikaleté	několikaletý	k2eAgInPc4d1	několikaletý
spory	spor	k1gInPc4	spor
s	s	k7c7	s
heraldiky	heraldik	k1gMnPc7	heraldik
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
nutnosti	nutnost	k1gFnSc2	nutnost
jeho	jeho	k3xOp3gFnSc2	jeho
změny	změna	k1gFnSc2	změna
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Kvasicích	kvasicí	k2eAgFnPc6d1	kvasicí
<g/>
.	.	kIx.	.
</s>
<s>
Zámek	zámek	k1gInSc1	zámek
Kvasice	Kvasic	k1gMnSc2	Kvasic
Kostel	kostel	k1gInSc4	kostel
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
a	a	k8xC	a
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
-	-	kIx~	-
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Antoše	Antoš	k1gMnSc2	Antoš
Dohnala	Dohnal	k1gMnSc2	Dohnal
Kaple	kaple	k1gFnSc2	kaple
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Bolestné	bolestný	k2eAgFnSc2d1	bolestná
Kaple	kaple	k1gFnSc2	kaple
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Pomocné	pomocný	k2eAgFnSc2d1	pomocná
Socha	socha	k1gFnSc1	socha
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
Fara	fara	k1gFnSc1	fara
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Antoše	Antoš	k1gMnSc2	Antoš
Dohnala	Dohnal	k1gMnSc2	Dohnal
Na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
vede	vést	k5eAaImIp3nS	vést
lipovo-jírovcovou	lipovoírovcův	k2eAgFnSc7d1	lipovo-jírovcův
alejí	alej	k1gFnSc7	alej
stará	starat	k5eAaImIp3nS	starat
barokní	barokní	k2eAgFnSc1d1	barokní
Křížová	křížový	k2eAgFnSc1d1	křížová
cesta	cesta	k1gFnSc1	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Spáčil	Spáčil	k1gMnSc1	Spáčil
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1899	[number]	k4	1899
Kvasice	Kvasice	k1gFnSc1	Kvasice
-	-	kIx~	-
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1978	[number]	k4	1978
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
<g/>
)	)	kIx)	)
-	-	kIx~	-
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
středoškolský	středoškolský	k2eAgMnSc1d1	středoškolský
profesor	profesor	k1gMnSc1	profesor
<g/>
.	.	kIx.	.
</s>
<s>
Leopold	Leopold	k1gMnSc1	Leopold
Josef	Josef	k1gMnSc1	Josef
Hansmann	Hansmann	k1gMnSc1	Hansmann
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1824	[number]	k4	1824
Kvasice	Kvasice	k1gFnSc1	Kvasice
-	-	kIx~	-
7	[number]	k4	7
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1863	[number]	k4	1863
<g/>
)	)	kIx)	)
-	-	kIx~	-
redaktor	redaktor	k1gMnSc1	redaktor
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Marcel	Marcel	k1gMnSc1	Marcel
Krasický	Krasický	k2eAgMnSc1d1	Krasický
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1898	[number]	k4	1898
Kvasice	Kvasice	k1gFnSc2	Kvasice
-	-	kIx~	-
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1968	[number]	k4	1968
Přerov	Přerov	k1gInSc1	Přerov
<g/>
)	)	kIx)	)
-	-	kIx~	-
akademický	akademický	k2eAgMnSc1d1	akademický
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
<g/>
,	,	kIx,	,
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
<g/>
,	,	kIx,	,
výtvarný	výtvarný	k2eAgMnSc1d1	výtvarný
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
.	.	kIx.	.
</s>
<s>
Maximilian	Maximilian	k1gInSc1	Maximilian
von	von	k1gInSc1	von
Proskowetz	Proskowetz	k1gInSc1	Proskowetz
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1851	[number]	k4	1851
Kvasice	Kvasice	k1gFnSc1	Kvasice
-	-	kIx~	-
19	[number]	k4	19
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1898	[number]	k4	1898
Fort	Fort	k?	Fort
Wayne	Wayn	k1gMnSc5	Wayn
<g/>
,	,	kIx,	,
Indiana	Indiana	k1gFnSc1	Indiana
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
-	-	kIx~	-
agronom	agronom	k1gMnSc1	agronom
a	a	k8xC	a
diplomat	diplomat	k1gMnSc1	diplomat
<g/>
.	.	kIx.	.
</s>
<s>
Friedrich	Friedrich	k1gMnSc1	Friedrich
von	von	k1gInSc1	von
Thun	Thun	k1gMnSc1	Thun
(	(	kIx(	(
<g/>
*	*	kIx~	*
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1942	[number]	k4	1942
Kvasice	Kvasice	k1gFnSc1	Kvasice
<g/>
)	)	kIx)	)
-	-	kIx~	-
herec	herec	k1gMnSc1	herec
<g/>
.	.	kIx.	.
</s>
<s>
Michal	Michal	k1gMnSc1	Michal
Pavlata	Pavle	k1gNnPc4	Pavle
(	(	kIx(	(
<g/>
*	*	kIx~	*
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1945	[number]	k4	1945
Kvasice	Kvasice	k1gFnSc1	Kvasice
<g/>
)	)	kIx)	)
-	-	kIx~	-
herec	herec	k1gMnSc1	herec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
katastru	katastr	k1gInSc6	katastr
obce	obec	k1gFnSc2	obec
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
tato	tento	k3xDgNnPc1	tento
chráněná	chráněný	k2eAgNnPc1d1	chráněné
území	území	k1gNnPc1	území
<g/>
:	:	kIx,	:
Evropsky	evropsky	k6eAd1	evropsky
významné	významný	k2eAgFnPc1d1	významná
lokality	lokalita	k1gFnPc1	lokalita
<g/>
:	:	kIx,	:
Chřiby	Chřiby	k1gInPc1	Chřiby
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kvasice	Kvasice	k1gFnSc2	Kvasice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Katastrální	katastrální	k2eAgFnSc1d1	katastrální
mapa	mapa	k1gFnSc1	mapa
obce	obec	k1gFnSc2	obec
Kvasice	Kvasice	k1gFnSc2	Kvasice
na	na	k7c6	na
webu	web	k1gInSc6	web
ČÚZK	ČÚZK	kA	ČÚZK
Seznam	seznam	k1gInSc1	seznam
děl	dít	k5eAaImAgInS	dít
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Kvasice	Kvasice	k1gFnSc1	Kvasice
</s>
