<p>
<s>
Sherwood	Sherwood	k1gInSc1	Sherwood
Anderson	Anderson	k1gMnSc1	Anderson
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1876	[number]	k4	1876
<g/>
,	,	kIx,	,
Camden	Camdno	k1gNnPc2	Camdno
<g/>
,	,	kIx,	,
Ohio	Ohio	k1gNnSc1	Ohio
<g/>
,	,	kIx,	,
USA	USA	kA	USA
–	–	k?	–
8	[number]	k4	8
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
Panama	Panama	k1gFnSc1	Panama
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
(	(	kIx(	(
<g/>
výběr	výběr	k1gInSc1	výběr
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Windy	Wind	k1gInPc1	Wind
McPherson	McPhersona	k1gFnPc2	McPhersona
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Son	son	k1gInSc1	son
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Syn	syn	k1gMnSc1	syn
Windyho	Windy	k1gMnSc2	Windy
McPersona	McPerson	k1gMnSc2	McPerson
<g/>
,	,	kIx,	,
1916	[number]	k4	1916
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Marching	Marching	k1gInSc1	Marching
Men	Men	k1gFnSc2	Men
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Muži	muž	k1gMnPc1	muž
na	na	k7c6	na
pochodu	pochod	k1gInSc6	pochod
<g/>
,	,	kIx,	,
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Winesburg	Winesburg	k1gInSc1	Winesburg
<g/>
,	,	kIx,	,
Ohio	Ohio	k1gNnSc1	Ohio
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Městečko	městečko	k1gNnSc1	městečko
v	v	k7c6	v
Ohiu	Ohio	k1gNnSc6	Ohio
<g/>
,	,	kIx,	,
1919	[number]	k4	1919
<g/>
,	,	kIx,	,
povídky	povídka	k1gFnPc1	povídka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Poor	Poor	k1gMnSc1	Poor
White	Whit	k1gInSc5	Whit
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Bílý	bílý	k2eAgMnSc1d1	bílý
nuzák	nuzák	k1gMnSc1	nuzák
<g/>
,	,	kIx,	,
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Triumph	Triumph	k1gInSc1	Triumph
of	of	k?	of
the	the	k?	the
Egg	Egg	k1gFnSc2	Egg
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Triumf	triumf	k1gInSc1	triumf
vejce	vejce	k1gNnSc2	vejce
<g/>
,	,	kIx,	,
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
povídky	povídka	k1gFnPc1	povídka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Many	mana	k1gFnPc1	mana
Marriages	Marriagesa	k1gFnPc2	Marriagesa
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Mnohá	mnohý	k2eAgNnPc4d1	mnohé
manželství	manželství	k1gNnPc4	manželství
<g/>
,	,	kIx,	,
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Horses	Horses	k1gMnSc1	Horses
and	and	k?	and
Men	Men	k1gMnSc1	Men
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Koně	kůň	k1gMnPc1	kůň
a	a	k8xC	a
lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
povídky	povídka	k1gFnPc1	povídka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
A	a	k9	a
Story-Teller	Story-Teller	k1gInSc1	Story-Teller
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Story	story	k1gFnSc7	story
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Vypravěčův	vypravěčův	k2eAgInSc1d1	vypravěčův
příběh	příběh	k1gInSc1	příběh
<g/>
,	,	kIx,	,
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
s	s	k7c7	s
autobiografickými	autobiografický	k2eAgInPc7d1	autobiografický
prvky	prvek	k1gInPc7	prvek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dark	Dark	k1gMnSc1	Dark
Laughter	Laughter	k1gMnSc1	Laughter
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Temný	temný	k2eAgInSc1d1	temný
smích	smích	k1gInSc1	smích
<g/>
,	,	kIx,	,
1925	[number]	k4	1925
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tar	Tar	k?	Tar
<g/>
:	:	kIx,	:
A	a	k9	a
Midwest	Midwest	k1gInSc1	Midwest
Childhood	Childhooda	k1gFnPc2	Childhooda
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Tar	Tar	k1gFnSc1	Tar
<g/>
,	,	kIx,	,
středozápadní	středozápadní	k2eAgNnSc1d1	středozápadní
dětství	dětství	k1gNnSc1	dětství
<g/>
,	,	kIx,	,
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
s	s	k7c7	s
autobiografickými	autobiografický	k2eAgInPc7d1	autobiografický
prvky	prvek	k1gInPc7	prvek
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sherwood	Sherwood	k1gInSc1	Sherwood
Anderson	Anderson	k1gMnSc1	Anderson
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Notebook	notebook	k1gInSc1	notebook
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Zápisník	zápisník	k1gInSc1	zápisník
<g/>
,	,	kIx,	,
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
paměti	paměť	k1gFnSc6	paměť
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hello	Hello	k1gNnSc1	Hello
Towns	Townsa	k1gFnPc2	Townsa
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
,	,	kIx,	,
povídky	povídka	k1gFnPc1	povídka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Beyond	Beyond	k1gMnSc1	Beyond
Desire	Desir	k1gInSc5	Desir
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Víc	hodně	k6eAd2	hodně
než	než	k8xS	než
touha	touha	k1gFnSc1	touha
<g/>
,	,	kIx,	,
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Death	Death	k1gInSc1	Death
in	in	k?	in
the	the	k?	the
Woods	Woods	k1gInSc1	Woods
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
Smrt	smrt	k1gFnSc1	smrt
v	v	k7c6	v
lesích	les	k1gInPc6	les
<g/>
,	,	kIx,	,
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
povídky	povídka	k1gFnPc1	povídka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Česky	česky	k6eAd1	česky
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
===	===	k?	===
</s>
</p>
<p>
<s>
Smutní	smutný	k2eAgMnPc1d1	smutný
trubači	trubač	k1gMnPc1	trubač
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
povídky	povídka	k1gFnPc1	povídka
<g/>
,	,	kIx,	,
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Jan	Jan	k1gMnSc1	Jan
Čep	Čep	k1gMnSc1	Čep
<g/>
,	,	kIx,	,
Dobré	dobrý	k2eAgNnSc4d1	dobré
dílo	dílo	k1gNnSc4	dílo
svazek	svazek	k1gInSc1	svazek
90	[number]	k4	90
<g/>
,	,	kIx,	,
Marta	Marta	k1gFnSc1	Marta
Florianová	Florianová	k1gFnSc1	Florianová
<g/>
,	,	kIx,	,
Stará	starý	k2eAgFnSc1d1	stará
Říše	říše	k1gFnSc1	říše
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
<g/>
,	,	kIx,	,
1927	[number]	k4	1927
</s>
</p>
<p>
<s>
Temný	temný	k2eAgInSc1d1	temný
smích	smích	k1gInSc1	smích
<g/>
,	,	kIx,	,
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
přeložili	přeložit	k5eAaPmAgMnP	přeložit
Jarmila	Jarmila	k1gFnSc1	Jarmila
Fastrová	Fastrová	k1gFnSc1	Fastrová
a	a	k8xC	a
Aloys	Aloysa	k1gFnPc2	Aloysa
Skoumal	Skoumal	k1gMnSc1	Skoumal
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Petr	Petr	k1gMnSc1	Petr
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1927	[number]	k4	1927
</s>
</p>
<p>
<s>
Městečko	městečko	k1gNnSc1	městečko
v	v	k7c6	v
Ohiu	Ohio	k1gNnSc6	Ohio
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Eva	Eva	k1gFnSc1	Eva
Kondrysová	Kondrysová	k1gFnSc1	Kondrysová
<g/>
,	,	kIx,	,
SNKLHU	SNKLHU	kA	SNKLHU
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1958	[number]	k4	1958
</s>
</p>
<p>
<s>
Smrt	smrt	k1gFnSc1	smrt
v	v	k7c6	v
lese	les	k1gInSc6	les
(	(	kIx(	(
<g/>
výbor	výbor	k1gInSc1	výbor
12	[number]	k4	12
povídek	povídka	k1gFnPc2	povídka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Eva	Eva	k1gFnSc1	Eva
Kondrysová	Kondrysová	k1gFnSc1	Kondrysová
a	a	k8xC	a
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Schejbal	Schejbal	k1gMnSc1	Schejbal
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1964	[number]	k4	1964
</s>
</p>
<p>
<s>
Městečko	městečko	k1gNnSc1	městečko
v	v	k7c6	v
Ohiu	Ohio	k1gNnSc6	Ohio
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Eva	Eva	k1gFnSc1	Eva
Kondrysová	Kondrysová	k1gFnSc1	Kondrysová
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Světová	světový	k2eAgFnSc1d1	světová
četba	četba	k1gFnSc1	četba
<g/>
,	,	kIx,	,
svazek	svazek	k1gInSc1	svazek
469	[number]	k4	469
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1976	[number]	k4	1976
</s>
</p>
<p>
<s>
Městečko	městečko	k1gNnSc1	městečko
v	v	k7c6	v
Ohiu	Ohio	k1gNnSc6	Ohio
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
Eva	Eva	k1gFnSc1	Eva
Kondrysová	Kondrysová	k1gFnSc1	Kondrysová
<g/>
,	,	kIx,	,
Ivo	Ivo	k1gMnSc1	Ivo
Železný	Železný	k1gMnSc1	Železný
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1995	[number]	k4	1995
ISBN	ISBN	kA	ISBN
80-237-2120-8	[number]	k4	80-237-2120-8
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Sherwood	Sherwooda	k1gFnPc2	Sherwooda
Anderson	Anderson	k1gMnSc1	Anderson
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Sherwood	Sherwood	k1gInSc1	Sherwood
Anderson	Anderson	k1gMnSc1	Anderson
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
šifra	šifra	k1gFnSc1	šifra
zv	zv	k?	zv
(	(	kIx(	(
<g/>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Vančura	Vančura	k1gMnSc1	Vančura
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
heslo	heslo	k1gNnSc1	heslo
Sherwood	Sherwood	k1gInSc1	Sherwood
Anderson	Anderson	k1gMnSc1	Anderson
in	in	k?	in
<g/>
:	:	kIx,	:
Slovník	slovník	k1gInSc1	slovník
spisovatelů	spisovatel	k1gMnPc2	spisovatel
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1979	[number]	k4	1979
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Hilský	Hilský	k1gMnSc1	Hilský
<g/>
:	:	kIx,	:
Kniha	kniha	k1gFnSc1	kniha
mládí	mládí	k1gNnSc2	mládí
<g/>
,	,	kIx,	,
samoty	samota	k1gFnSc2	samota
a	a	k8xC	a
lásky	láska	k1gFnSc2	láska
<g/>
,	,	kIx,	,
předmluva	předmluva	k1gFnSc1	předmluva
ke	k	k7c3	k
knize	kniha	k1gFnSc3	kniha
Městečko	městečko	k1gNnSc1	městečko
v	v	k7c6	v
Ohiu	Ohio	k1gNnSc6	Ohio
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc4	odeon
<g/>
,	,	kIx,	,
Světová	světový	k2eAgFnSc1d1	světová
četba	četba	k1gFnSc1	četba
<g/>
,	,	kIx,	,
svazek	svazek	k1gInSc1	svazek
469	[number]	k4	469
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1976	[number]	k4	1976
</s>
</p>
