<p>
<s>
Austrálie	Austrálie	k1gFnSc1	Austrálie
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Australia	Australius	k1gMnSc2	Australius
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
světadíl	světadíl	k1gInSc1	světadíl
s	s	k7c7	s
nejmenší	malý	k2eAgFnSc7d3	nejmenší
rozlohou	rozloha	k1gFnSc7	rozloha
a	a	k8xC	a
nejmenší	malý	k2eAgFnSc7d3	nejmenší
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
nadmořskou	nadmořský	k2eAgFnSc7d1	nadmořská
výškou	výška	k1gFnSc7	výška
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
stát	stát	k1gInSc1	stát
Austrálie	Austrálie	k1gFnSc2	Austrálie
zabírá	zabírat	k5eAaImIp3nS	zabírat
většinu	většina	k1gFnSc4	většina
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
chybně	chybně	k6eAd1	chybně
uváděno	uvádět	k5eAaImNgNnS	uvádět
celý	celý	k2eAgInSc1d1	celý
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnPc1	jeho
hranice	hranice	k1gFnPc1	hranice
jsou	být	k5eAaImIp3nP	být
definovány	definovat	k5eAaBmNgFnP	definovat
kontinentální	kontinentální	k2eAgFnSc7d1	kontinentální
plošinou	plošina	k1gFnSc7	plošina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pokrývá	pokrývat	k5eAaImIp3nS	pokrývat
2,5	[number]	k4	2,5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
km2	km2	k4	km2
včetně	včetně	k7c2	včetně
Arafurského	Arafurský	k2eAgNnSc2d1	Arafurské
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
Korálového	korálový	k2eAgNnSc2d1	korálové
moře	moře	k1gNnSc2	moře
s	s	k7c7	s
Velkým	velký	k2eAgInSc7d1	velký
bariérovým	bariérový	k2eAgInSc7d1	bariérový
útesem	útes	k1gInSc7	útes
<g/>
,	,	kIx,	,
s	s	k7c7	s
hloubkou	hloubka	k1gFnSc7	hloubka
kolem	kolem	k7c2	kolem
50	[number]	k4	50
m.	m.	k?	m.
Toto	tento	k3xDgNnSc1	tento
moře	moře	k1gNnSc1	moře
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejdůležitějších	důležitý	k2eAgInPc2d3	nejdůležitější
mořských	mořský	k2eAgInPc2d1	mořský
ekosystémů	ekosystém	k1gInPc2	ekosystém
a	a	k8xC	a
chráněným	chráněný	k2eAgInSc7d1	chráněný
životním	životní	k2eAgInSc7d1	životní
prostorem	prostor	k1gInSc7	prostor
pro	pro	k7c4	pro
1500	[number]	k4	1500
druhů	druh	k1gInPc2	druh
ryb	ryba	k1gFnPc2	ryba
<g/>
,	,	kIx,	,
4000	[number]	k4	4000
druhů	druh	k1gInPc2	druh
měkkýšů	měkkýš	k1gMnPc2	měkkýš
a	a	k8xC	a
400	[number]	k4	400
druhů	druh	k1gInPc2	druh
korálů	korál	k1gInPc2	korál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Australský	australský	k2eAgInSc1d1	australský
kontinent	kontinent	k1gInSc1	kontinent
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgFnSc2d1	Nové
Guineje	Guinea	k1gFnSc2	Guinea
a	a	k8xC	a
přilehlých	přilehlý	k2eAgInPc2d1	přilehlý
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
tři	tři	k4xCgInPc1	tři
státy	stát	k1gInPc1	stát
<g/>
:	:	kIx,	:
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
,	,	kIx,	,
Papua-Nová	Papua-Nový	k2eAgFnSc1d1	Papua-Nová
Guinea	Guinea	k1gFnSc1	Guinea
a	a	k8xC	a
Indonésie	Indonésie	k1gFnSc1	Indonésie
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
provincie	provincie	k1gFnSc1	provincie
Papua	Papu	k2eAgFnSc1d1	Papua
a	a	k8xC	a
Západní	západní	k2eAgFnSc1d1	západní
Papua	Papua	k1gFnSc1	Papua
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
není	být	k5eNaImIp3nS	být
součástí	součást	k1gFnSc7	součást
australského	australský	k2eAgInSc2d1	australský
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
nenachází	nacházet	k5eNaImIp3nS	nacházet
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
kontinentálním	kontinentální	k2eAgInSc6d1	kontinentální
šelfu	šelf	k1gInSc6	šelf
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
však	však	k9	však
částí	část	k1gFnSc7	část
ponořeného	ponořený	k2eAgInSc2d1	ponořený
kontinentu	kontinent	k1gInSc2	kontinent
Zélandie	Zélandie	k1gFnSc2	Zélandie
<g/>
.	.	kIx.	.
</s>
<s>
Zélandie	Zélandie	k1gFnSc1	Zélandie
a	a	k8xC	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
Austrálií	Austrálie	k1gFnSc7	Austrálie
tvoří	tvořit	k5eAaImIp3nS	tvořit
širší	široký	k2eAgInSc4d2	širší
region	region	k1gInSc4	region
Australasie	Australasie	k1gFnSc2	Australasie
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
Oceánie	Oceánie	k1gFnSc2	Oceánie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poloha	poloha	k1gFnSc1	poloha
==	==	k?	==
</s>
</p>
<p>
<s>
Austrálie	Austrálie	k1gFnSc1	Austrálie
leží	ležet	k5eAaImIp3nS	ležet
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
kontinenty	kontinent	k1gInPc7	kontinent
leží	ležet	k5eAaImIp3nS	ležet
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc4	jejichž
přiřazení	přiřazení	k1gNnSc4	přiřazení
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
či	či	k8xC	či
onomu	onen	k3xDgInSc3	onen
světadílu	světadíl	k1gInSc3	světadíl
není	být	k5eNaImIp3nS	být
jednotné	jednotný	k2eAgNnSc1d1	jednotné
<g/>
.	.	kIx.	.
</s>
<s>
Budeme	být	k5eAaImBp1nP	být
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
držet	držet	k5eAaImF	držet
přístupu	přístup	k1gInSc3	přístup
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
Austrálii	Austrálie	k1gFnSc4	Austrálie
pohromadě	pohromadě	k6eAd1	pohromadě
s	s	k7c7	s
Oceánií	Oceánie	k1gFnSc7	Oceánie
<g/>
,	,	kIx,	,
přibudou	přibýt	k5eAaPmIp3nP	přibýt
k	k	k7c3	k
ostrovům	ostrov	k1gInPc3	ostrov
australského	australský	k2eAgInSc2d1	australský
šelfu	šelf	k1gInSc2	šelf
ještě	ještě	k9	ještě
desítky	desítka	k1gFnPc4	desítka
dalších	další	k2eAgNnPc2d1	další
souostroví	souostroví	k1gNnPc2	souostroví
tisíce	tisíc	k4xCgInSc2	tisíc
kilometrů	kilometr	k1gInPc2	kilometr
daleko	daleko	k6eAd1	daleko
v	v	k7c6	v
Tichém	tichý	k2eAgInSc6d1	tichý
oceánu	oceán	k1gInSc6	oceán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejzazší	zadní	k2eAgInPc1d3	nejzazší
body	bod	k1gInPc1	bod
na	na	k7c6	na
pevnině	pevnina	k1gFnSc6	pevnina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Nejsevernější	severní	k2eAgInSc1d3	nejsevernější
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
Cape	capat	k5eAaImIp3nS	capat
York	York	k1gInSc1	York
na	na	k7c6	na
stejnojmenném	stejnojmenný	k2eAgInSc6d1	stejnojmenný
poloostrově	poloostrov	k1gInSc6	poloostrov
v	v	k7c6	v
Queenslandu	Queensland	k1gInSc6	Queensland
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
°	°	k?	°
41	[number]	k4	41
<g/>
'	'	kIx"	'
j.š.	j.š.	k?	j.š.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejjižnější	jižní	k2eAgInSc1d3	nejjižnější
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
South	South	k1gMnSc1	South
East	East	k1gMnSc1	East
Cape	capat	k5eAaImIp3nS	capat
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Victoria	Victorium	k1gNnSc2	Victorium
(	(	kIx(	(
<g/>
39	[number]	k4	39
<g/>
°	°	k?	°
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
'	'	kIx"	'
j.š.	j.š.	k?	j.š.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejzápadnější	západní	k2eAgInSc1d3	nejzápadnější
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
Steep	Steep	k1gInSc1	Steep
Point	pointa	k1gFnPc2	pointa
v	v	k7c6	v
Západní	západní	k2eAgFnSc6d1	západní
Austrálii	Austrálie	k1gFnSc6	Austrálie
(	(	kIx(	(
<g/>
113	[number]	k4	113
<g/>
°	°	k?	°
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
'	'	kIx"	'
v.d.	v.d.	k?	v.d.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejvýchodnější	východní	k2eAgInSc1d3	nejvýchodnější
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
Cape	capat	k5eAaImIp3nS	capat
Byron	Byron	k1gMnSc1	Byron
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Jižním	jižní	k2eAgInSc6d1	jižní
Walesu	Wales	k1gInSc6	Wales
(	(	kIx(	(
<g/>
153	[number]	k4	153
<g/>
°	°	k?	°
39	[number]	k4	39
<g/>
'	'	kIx"	'
v.d.	v.d.	k?	v.d.
<g/>
)	)	kIx)	)
<g/>
Nejzazší	zadní	k2eAgInPc4d3	nejzazší
body	bod	k1gInPc4	bod
včetně	včetně	k7c2	včetně
šelfových	šelfový	k2eAgInPc2d1	šelfový
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Nejsevernější	severní	k2eAgInSc1d3	nejsevernější
bod	bod	k1gInSc1	bod
Nové	Nové	k2eAgFnSc2d1	Nové
Guineje	Guinea	k1gFnSc2	Guinea
leží	ležet	k5eAaImIp3nP	ležet
poblíž	poblíž	k6eAd1	poblíž
Warmandi	Warmand	k1gMnPc1	Warmand
v	v	k7c6	v
indonéské	indonéský	k2eAgFnSc6d1	Indonéská
části	část	k1gFnSc6	část
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
°	°	k?	°
21	[number]	k4	21
<g/>
'	'	kIx"	'
j.š.	j.š.	k?	j.š.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejsevernější	severní	k2eAgInSc1d3	nejsevernější
bod	bod	k1gInSc1	bod
souostroví	souostroví	k1gNnSc1	souostroví
Moluky	Moluky	k1gFnPc1	Moluky
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
také	také	k9	také
přiřazuje	přiřazovat	k5eAaImIp3nS	přiřazovat
k	k	k7c3	k
Austrálii	Austrálie	k1gFnSc3	Austrálie
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
Morotai	Morota	k1gFnSc2	Morota
a	a	k8xC	a
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
se	se	k3xPyFc4	se
Tanjung	Tanjung	k1gInSc1	Tanjung
Sopi	Sopi	k1gNnSc2	Sopi
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
°	°	k?	°
39	[number]	k4	39
<g/>
'	'	kIx"	'
s.	s.	k?	s.
<g/>
š.	š.	k?	š.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejjižnější	jižní	k2eAgInSc1d3	nejjižnější
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
South	South	k1gMnSc1	South
East	East	k1gMnSc1	East
Cape	capat	k5eAaImIp3nS	capat
na	na	k7c6	na
Tasmánii	Tasmánie	k1gFnSc6	Tasmánie
(	(	kIx(	(
<g/>
43	[number]	k4	43
<g/>
°	°	k?	°
39	[number]	k4	39
<g/>
'	'	kIx"	'
j.š.	j.š.	k?	j.š.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejzápadnější	západní	k2eAgInSc1d3	nejzápadnější
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
Dirk	Dirk	k1gInSc1	Dirk
Hartog	Hartog	k1gInSc1	Hartog
Island	Island	k1gInSc1	Island
v	v	k7c6	v
Západní	západní	k2eAgFnSc6d1	západní
Austrálii	Austrálie	k1gFnSc6	Austrálie
(	(	kIx(	(
<g/>
112	[number]	k4	112
<g/>
°	°	k?	°
55	[number]	k4	55
<g/>
'	'	kIx"	'
v.d.	v.d.	k?	v.d.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejvýchodnější	východní	k2eAgInSc1d3	nejvýchodnější
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
Cape	capat	k5eAaImIp3nS	capat
Byron	Byron	k1gMnSc1	Byron
v	v	k7c6	v
Novém	nový	k2eAgInSc6d1	nový
Jižním	jižní	k2eAgInSc6d1	jižní
Walesu	Wales	k1gInSc6	Wales
(	(	kIx(	(
<g/>
153	[number]	k4	153
<g/>
°	°	k?	°
39	[number]	k4	39
<g/>
'	'	kIx"	'
v.d.	v.d.	k?	v.d.
<g/>
)	)	kIx)	)
<g/>
Nejzazší	zadní	k2eAgInPc1d3	nejzazší
body	bod	k1gInPc1	bod
Oceánie	Oceánie	k1gFnSc2	Oceánie
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Nejsevernější	severní	k2eAgInSc4d3	nejsevernější
bod	bod	k1gInSc4	bod
<g/>
:	:	kIx,	:
Kure	kur	k1gMnSc5	kur
Island	Island	k1gInSc1	Island
<g/>
,	,	kIx,	,
Midway	Midway	k1gInPc1	Midway
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
°	°	k?	°
25	[number]	k4	25
<g/>
'	'	kIx"	'
s.	s.	k?	s.
<g/>
š.	š.	k?	š.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejjižnější	jižní	k2eAgInSc1d3	nejjižnější
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
Campbell	Campbell	k1gMnSc1	Campbell
(	(	kIx(	(
<g/>
52	[number]	k4	52
<g/>
°	°	k?	°
30	[number]	k4	30
<g/>
'	'	kIx"	'
j.š.	j.š.	k?	j.š.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejzápadnější	západní	k2eAgInSc1d3	nejzápadnější
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
ostrov	ostrov	k1gInSc1	ostrov
Misool	Misoola	k1gFnPc2	Misoola
v	v	k7c6	v
indonéské	indonéský	k2eAgFnSc6d1	Indonéská
provincii	provincie	k1gFnSc6	provincie
Papua	Papua	k1gMnSc1	Papua
(	(	kIx(	(
<g/>
129	[number]	k4	129
<g/>
°	°	k?	°
43	[number]	k4	43
<g/>
'	'	kIx"	'
v.d.	v.d.	k?	v.d.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejvýchodnější	východní	k2eAgInSc1d3	nejvýchodnější
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
Sala	Sala	k1gMnSc1	Sala
y	y	k?	y
Gómez	Gómez	k1gMnSc1	Gómez
(	(	kIx(	(
<g/>
105	[number]	k4	105
<g/>
°	°	k?	°
28	[number]	k4	28
<g/>
'	'	kIx"	'
z.d.	z.d.	k?	z.d.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Během	během	k7c2	během
posledních	poslední	k2eAgNnPc2d1	poslední
10	[number]	k4	10
000	[number]	k4	000
let	léto	k1gNnPc2	léto
zatopila	zatopit	k5eAaPmAgFnS	zatopit
vzrůstající	vzrůstající	k2eAgFnSc4d1	vzrůstající
úroveň	úroveň	k1gFnSc4	úroveň
moří	moře	k1gNnPc2	moře
nížiny	nížina	k1gFnSc2	nížina
a	a	k8xC	a
oddělila	oddělit	k5eAaPmAgFnS	oddělit
dnešní	dnešní	k2eAgFnSc1d1	dnešní
nízko	nízko	k6eAd1	nízko
položenou	položený	k2eAgFnSc4d1	položená
polovyprahlou	polovyprahlý	k2eAgFnSc4d1	polovyprahlá
pevninu	pevnina	k1gFnSc4	pevnina
a	a	k8xC	a
dva	dva	k4xCgInPc1	dva
hornaté	hornatý	k2eAgInPc1d1	hornatý
ostrovy	ostrov	k1gInPc1	ostrov
Novou	nový	k2eAgFnSc4d1	nová
Guineu	Guinea	k1gFnSc4	Guinea
a	a	k8xC	a
Tasmánii	Tasmánie	k1gFnSc4	Tasmánie
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
z	z	k7c2	z
tektonického	tektonický	k2eAgNnSc2d1	tektonické
hlediska	hledisko	k1gNnSc2	hledisko
k	k	k7c3	k
Austrálii	Austrálie	k1gFnSc3	Austrálie
také	také	k9	také
náležejí	náležet	k5eAaImIp3nP	náležet
<g/>
.	.	kIx.	.
70	[number]	k4	70
%	%	kIx~	%
jeho	jeho	k3xOp3gNnSc2	jeho
území	území	k1gNnSc2	území
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
pouště	poušť	k1gFnPc1	poušť
<g/>
,	,	kIx,	,
polopouště	polopoušť	k1gFnPc1	polopoušť
a	a	k8xC	a
suché	suchý	k2eAgFnPc1d1	suchá
stepi	step	k1gFnPc1	step
<g/>
.	.	kIx.	.
</s>
<s>
Malé	Malé	k2eAgNnSc4d1	Malé
území	území	k1gNnSc4	území
na	na	k7c6	na
severu	sever	k1gInSc6	sever
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
tropům	trop	k1gInPc3	trop
s	s	k7c7	s
průměrnými	průměrný	k2eAgFnPc7d1	průměrná
ročními	roční	k2eAgFnPc7d1	roční
srážkami	srážka	k1gFnPc7	srážka
2000	[number]	k4	2000
mm	mm	kA	mm
a	a	k8xC	a
místy	místy	k6eAd1	místy
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
zbytky	zbytek	k1gInPc1	zbytek
tropického	tropický	k2eAgInSc2d1	tropický
deštného	deštný	k2eAgInSc2d1	deštný
pralesa	prales	k1gInSc2	prales
<g/>
.	.	kIx.	.
</s>
<s>
Lidské	lidský	k2eAgNnSc1d1	lidské
osídlení	osídlení	k1gNnSc1	osídlení
se	se	k3xPyFc4	se
soustředí	soustředit	k5eAaPmIp3nS	soustředit
zejména	zejména	k9	zejména
na	na	k7c4	na
severní	severní	k2eAgNnSc4d1	severní
až	až	k8xS	až
jihovýchodní	jihovýchodní	k2eAgNnSc4d1	jihovýchodní
pobřeží	pobřeží	k1gNnSc4	pobřeží
a	a	k8xC	a
přilehlé	přilehlý	k2eAgInPc4d1	přilehlý
ostrovy	ostrov	k1gInPc4	ostrov
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
Tasmánii	Tasmánie	k1gFnSc4	Tasmánie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgMnSc1d1	západní
a	a	k8xC	a
jihozápadní	jihozápadní	k2eAgNnSc1d1	jihozápadní
pobřeží	pobřeží	k1gNnSc1	pobřeží
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
osídleno	osídlit	k5eAaPmNgNnS	osídlit
jako	jako	k9	jako
první	první	k4xOgNnSc1	první
<g/>
,	,	kIx,	,
a	a	k8xC	a
střed	střed	k1gInSc4	střed
kontinentu	kontinent	k1gInSc2	kontinent
mají	mít	k5eAaImIp3nP	mít
mnohem	mnohem	k6eAd1	mnohem
horší	zlý	k2eAgFnPc4d2	horší
podmínky	podmínka	k1gFnPc4	podmínka
k	k	k7c3	k
životu	život	k1gInSc3	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Flóra	Flóra	k1gFnSc1	Flóra
==	==	k?	==
</s>
</p>
<p>
<s>
Austrálie	Austrálie	k1gFnSc1	Austrálie
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
svébytným	svébytný	k2eAgNnSc7d1	svébytné
rostlinstvem	rostlinstvo	k1gNnSc7	rostlinstvo
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
známé	známý	k2eAgFnPc4d1	známá
rostliny	rostlina	k1gFnPc4	rostlina
australského	australský	k2eAgInSc2d1	australský
původu	původ	k1gInSc2	původ
patří	patřit	k5eAaImIp3nS	patřit
především	především	k9	především
blahovičník	blahovičník	k1gInSc1	blahovičník
(	(	kIx(	(
<g/>
eukalyptus	eukalyptus	k1gInSc1	eukalyptus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
bariérový	bariérový	k2eAgInSc1d1	bariérový
útes	útes	k1gInSc1	útes
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
2000	[number]	k4	2000
kilometrů	kilometr	k1gInPc2	kilometr
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
korálový	korálový	k2eAgInSc4d1	korálový
útes	útes	k1gInSc4	útes
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
vytvářel	vytvářet	k5eAaImAgInS	vytvářet
miliony	milion	k4xCgInPc4	milion
let	léto	k1gNnPc2	léto
a	a	k8xC	a
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
asi	asi	k9	asi
350	[number]	k4	350
druhů	druh	k1gInPc2	druh
korálů	korál	k1gInPc2	korál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fauna	fauna	k1gFnSc1	fauna
==	==	k?	==
</s>
</p>
<p>
<s>
Zvířecí	zvířecí	k2eAgInPc1d1	zvířecí
druhy	druh	k1gInPc1	druh
patřící	patřící	k2eAgInPc1d1	patřící
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
klokanovití	klokanovitý	k2eAgMnPc1d1	klokanovitý
<g/>
,	,	kIx,	,
rodu	rod	k1gInSc2	rod
ježura	ježura	k1gFnSc1	ježura
nebo	nebo	k8xC	nebo
kasuár	kasuár	k1gMnSc1	kasuár
jsou	být	k5eAaImIp3nP	být
australskými	australský	k2eAgInPc7d1	australský
endemity	endemit	k1gInPc7	endemit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Původní	původní	k2eAgMnPc1d1	původní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
náleží	náležet	k5eAaImIp3nP	náležet
k	k	k7c3	k
australoidní	australoidní	k2eAgFnSc3d1	australoidní
rase	rasa	k1gFnSc3	rasa
a	a	k8xC	a
na	na	k7c4	na
australský	australský	k2eAgInSc4d1	australský
kontinent	kontinent	k1gInSc4	kontinent
přišli	přijít	k5eAaPmAgMnP	přijít
již	již	k6eAd1	již
před	před	k7c7	před
50	[number]	k4	50
000	[number]	k4	000
lety	let	k1gInPc7	let
během	během	k7c2	během
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
expanzí	expanze	k1gFnPc2	expanze
lidstva	lidstvo	k1gNnSc2	lidstvo
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
australská	australský	k2eAgFnSc1d1	australská
pevnina	pevnina	k1gFnSc1	pevnina
a	a	k8xC	a
Nová	nový	k2eAgFnSc1d1	nová
Guinea	Guinea	k1gFnSc1	Guinea
ještě	ještě	k9	ještě
díky	díky	k7c3	díky
nízké	nízký	k2eAgFnSc3d1	nízká
hladině	hladina	k1gFnSc3	hladina
moří	moře	k1gNnPc2	moře
tvořily	tvořit	k5eAaImAgInP	tvořit
jedinou	jediný	k2eAgFnSc4d1	jediná
pevninu	pevnina	k1gFnSc4	pevnina
zvanou	zvaný	k2eAgFnSc4d1	zvaná
Sahul	Sahula	k1gFnPc2	Sahula
<g/>
.	.	kIx.	.
</s>
<s>
Australoidé	Australoidý	k2eAgNnSc1d1	Australoidý
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
Melanésany	Melanésan	k1gMnPc4	Melanésan
a	a	k8xC	a
Austrálce	Austrálec	k1gMnPc4	Austrálec
<g/>
.	.	kIx.	.
</s>
<s>
Melanésané	Melanésan	k1gMnPc1	Melanésan
obývají	obývat	k5eAaImIp3nP	obývat
Novou	nový	k2eAgFnSc4d1	nová
Guineu	Guinea	k1gFnSc4	Guinea
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
Papuánci	Papuánec	k1gMnPc1	Papuánec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
přilehlé	přilehlý	k2eAgInPc1d1	přilehlý
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
již	již	k6eAd1	již
technicky	technicky	k6eAd1	technicky
nepatří	patřit	k5eNaImIp3nS	patřit
k	k	k7c3	k
australskému	australský	k2eAgInSc3d1	australský
kontinentu	kontinent	k1gInSc3	kontinent
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
k	k	k7c3	k
Oceánii	Oceánie	k1gFnSc3	Oceánie
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Šalamounovy	Šalamounův	k2eAgInPc1d1	Šalamounův
ostrovy	ostrov	k1gInPc1	ostrov
a	a	k8xC	a
Nová	nový	k2eAgFnSc1d1	nová
Kaledonie	Kaledonie	k1gFnSc1	Kaledonie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
podskupinu	podskupina	k1gFnSc4	podskupina
Austrálců	Austrálec	k1gMnPc2	Austrálec
lze	lze	k6eAd1	lze
označit	označit	k5eAaPmF	označit
vymřelé	vymřelý	k2eAgMnPc4d1	vymřelý
Tasmánce	Tasmánec	k1gMnPc4	Tasmánec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
ale	ale	k8xC	ale
byli	být	k5eAaImAgMnP	být
od	od	k7c2	od
ostatních	ostatní	k2eAgMnPc2d1	ostatní
Austrálců	Austrálec	k1gMnPc2	Austrálec
izolováni	izolovat	k5eAaBmNgMnP	izolovat
stoupající	stoupající	k2eAgFnSc7d1	stoupající
hladinou	hladina	k1gFnSc7	hladina
oceánu	oceán	k1gInSc2	oceán
ve	v	k7c4	v
stejnou	stejný	k2eAgFnSc4d1	stejná
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Papuánci	Papuánec	k1gMnPc1	Papuánec
<g/>
.	.	kIx.	.
</s>
<s>
Nedávno	nedávno	k6eAd1	nedávno
bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Australoidé	Australoidý	k2eAgNnSc1d1	Australoidý
se	se	k3xPyFc4	se
křížili	křížit	k5eAaImAgMnP	křížit
s	s	k7c7	s
vymřelými	vymřelý	k2eAgMnPc7d1	vymřelý
Denisovany	Denisovan	k1gMnPc7	Denisovan
<g/>
,	,	kIx,	,
druhem	druh	k1gMnSc7	druh
člověka	člověk	k1gMnSc2	člověk
podobným	podobný	k2eAgMnPc3d1	podobný
Neandrtálcům	neandrtálec	k1gMnPc3	neandrtálec
<g/>
.	.	kIx.	.
</s>
<s>
Podíl	podíl	k1gInSc1	podíl
denisovanských	denisovanský	k2eAgInPc2d1	denisovanský
genů	gen	k1gInPc2	gen
na	na	k7c6	na
jejich	jejich	k3xOp3gInSc6	jejich
genomu	genom	k1gInSc6	genom
činí	činit	k5eAaImIp3nS	činit
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Australoidé	Australoidý	k2eAgNnSc4d1	Australoidý
nikdy	nikdy	k6eAd1	nikdy
nedosáhli	dosáhnout	k5eNaPmAgMnP	dosáhnout
přílišné	přílišný	k2eAgFnSc3d1	přílišná
civilizační	civilizační	k2eAgFnSc3d1	civilizační
vyspělosti	vyspělost	k1gFnSc3	vyspělost
<g/>
.	.	kIx.	.
</s>
<s>
Austrálci	Austrálec	k1gMnPc1	Austrálec
nikdy	nikdy	k6eAd1	nikdy
nepraktikovali	praktikovat	k5eNaImAgMnP	praktikovat
zemědělství	zemědělství	k1gNnSc2	zemědělství
a	a	k8xC	a
spoléhali	spoléhat	k5eAaImAgMnP	spoléhat
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
lov	lov	k1gInSc4	lov
a	a	k8xC	a
sběr	sběr	k1gInSc4	sběr
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Melanésané	Melanésan	k1gMnPc1	Melanésan
znali	znát	k5eAaImAgMnP	znát
zemědělství	zemědělství	k1gNnSc4	zemědělství
tropických	tropický	k2eAgFnPc2d1	tropická
plodin	plodina	k1gFnPc2	plodina
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
ho	on	k3xPp3gMnSc4	on
také	také	k9	také
doplňovali	doplňovat	k5eAaImAgMnP	doplňovat
lovem	lov	k1gInSc7	lov
a	a	k8xC	a
sběrem	sběr	k1gInSc7	sběr
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
nízkou	nízký	k2eAgFnSc4d1	nízká
hustotu	hustota	k1gFnSc4	hustota
osídlení	osídlení	k1gNnSc2	osídlení
a	a	k8xC	a
rychlé	rychlý	k2eAgNnSc4d1	rychlé
nahrazení	nahrazení	k1gNnSc4	nahrazení
Austrálců	Austrálec	k1gMnPc2	Austrálec
Evropany	Evropan	k1gMnPc7	Evropan
dorazivšími	dorazivší	k2eAgMnPc7d1	dorazivší
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
indonéské	indonéský	k2eAgFnSc6d1	Indonéská
části	část	k1gFnSc6	část
Nové	Nové	k2eAgFnSc2d1	Nové
Guineje	Guinea	k1gFnSc2	Guinea
(	(	kIx(	(
<g/>
provincie	provincie	k1gFnSc1	provincie
Papua	Papu	k2eAgFnSc1d1	Papua
a	a	k8xC	a
Západní	západní	k2eAgFnSc1d1	západní
Papua	Papua	k1gFnSc1	Papua
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
Papuánci	Papuánec	k1gMnPc1	Papuánec
nahrazováni	nahrazován	k2eAgMnPc1d1	nahrazován
osadníky	osadník	k1gMnPc4	osadník
z	z	k7c2	z
asijských	asijský	k2eAgInPc2d1	asijský
ostrovů	ostrov	k1gInPc2	ostrov
(	(	kIx(	(
<g/>
především	především	k9	především
Jávy	Jáva	k1gFnSc2	Jáva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nezávislé	závislý	k2eNgFnSc6d1	nezávislá
zemi	zem	k1gFnSc6	zem
Papui	Papui	k1gNnSc2	Papui
Nové	Nové	k2eAgFnSc6d1	Nové
Guineji	Guinea	k1gFnSc6	Guinea
skladba	skladba	k1gFnSc1	skladba
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
stále	stále	k6eAd1	stále
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přírodní	přírodní	k2eAgNnPc4d1	přírodní
specifika	specifikon	k1gNnPc4	specifikon
==	==	k?	==
</s>
</p>
<p>
<s>
Austrálie	Austrálie	k1gFnSc1	Austrálie
byla	být	k5eAaImAgFnS	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
oddělena	oddělit	k5eAaPmNgFnS	oddělit
od	od	k7c2	od
zbytku	zbytek	k1gInSc2	zbytek
pevninských	pevninský	k2eAgFnPc2d1	pevninská
formací	formace	k1gFnPc2	formace
a	a	k8xC	a
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
mohla	moct	k5eAaImAgFnS	moct
v	v	k7c6	v
izolaci	izolace	k1gFnSc6	izolace
vyvinout	vyvinout	k5eAaPmF	vyvinout
unikátní	unikátní	k2eAgFnSc1d1	unikátní
flóra	flóra	k1gFnSc1	flóra
a	a	k8xC	a
fauna	fauna	k1gFnSc1	fauna
<g/>
.	.	kIx.	.
</s>
<s>
Austrálie	Austrálie	k1gFnSc1	Austrálie
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
jako	jako	k8xC	jako
kontinent	kontinent	k1gInSc4	kontinent
vačnatců	vačnatec	k1gMnPc2	vačnatec
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
ti	ten	k3xDgMnPc1	ten
od	od	k7c2	od
objevení	objevení	k1gNnSc2	objevení
kontinentu	kontinent	k1gInSc2	kontinent
Evropany	Evropan	k1gMnPc4	Evropan
trpí	trpět	k5eAaImIp3nS	trpět
celou	celý	k2eAgFnSc7d1	celá
sérií	série	k1gFnSc7	série
ekologických	ekologický	k2eAgFnPc2d1	ekologická
invazí	invaze	k1gFnPc2	invaze
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
některé	některý	k3yIgNnSc1	některý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
již	již	k6eAd1	již
zlikvidovaly	zlikvidovat	k5eAaPmAgInP	zlikvidovat
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgMnSc1d3	veliký
dravý	dravý	k2eAgMnSc1d1	dravý
vačnatec	vačnatec	k1gMnSc1	vačnatec
vakovlk	vakovlk	k1gMnSc1	vakovlk
v	v	k7c6	v
době	doba	k1gFnSc6	doba
příchodu	příchod	k1gInSc2	příchod
lidí	člověk	k1gMnPc2	člověk
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
cílenému	cílený	k2eAgInSc3d1	cílený
honu	hon	k1gInSc3	hon
a	a	k8xC	a
konkurenci	konkurence	k1gFnSc3	konkurence
dinga	dingo	k1gMnSc2	dingo
<g/>
.	.	kIx.	.
</s>
<s>
Drobní	drobný	k2eAgMnPc1d1	drobný
vačnatci	vačnatec	k1gMnPc1	vačnatec
trpí	trpět	k5eAaImIp3nP	trpět
díky	díky	k7c3	díky
systematickému	systematický	k2eAgInSc3d1	systematický
lovu	lov	k1gInSc3	lov
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
dinga	dingo	k1gMnSc2	dingo
<g/>
,	,	kIx,	,
lišek	liška	k1gFnPc2	liška
a	a	k8xC	a
koček	kočka	k1gFnPc2	kočka
či	či	k8xC	či
kvůli	kvůli	k7c3	kvůli
devastaci	devastace	k1gFnSc3	devastace
prostředí	prostředí	k1gNnSc2	prostředí
způsobované	způsobovaný	k2eAgFnPc4d1	způsobovaná
velbloudy	velbloud	k1gMnPc7	velbloud
<g/>
,	,	kIx,	,
vodními	vodní	k2eAgMnPc7d1	vodní
buvoly	buvol	k1gMnPc7	buvol
<g/>
,	,	kIx,	,
myšmi	myš	k1gFnPc7	myš
<g/>
,	,	kIx,	,
krysami	krysa	k1gFnPc7	krysa
<g/>
,	,	kIx,	,
potkany	potkan	k1gMnPc7	potkan
<g/>
,	,	kIx,	,
ropuchami	ropucha	k1gFnPc7	ropucha
a	a	k8xC	a
králíky	králík	k1gMnPc7	králík
<g/>
.	.	kIx.	.
</s>
<s>
Přírodě	příroda	k1gFnSc3	příroda
také	také	k9	také
nesvědčí	svědčit	k5eNaImIp3nS	svědčit
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
chov	chov	k1gInSc4	chov
dobytka	dobytek	k1gInSc2	dobytek
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
ovcí	ovce	k1gFnPc2	ovce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgInPc1d1	základní
údaje	údaj	k1gInPc1	údaj
==	==	k?	==
</s>
</p>
<p>
<s>
Rozloha	rozloha	k1gFnSc1	rozloha
<g/>
:	:	kIx,	:
7	[number]	k4	7
682	[number]	k4	682
300	[number]	k4	300
kilometrů	kilometr	k1gInPc2	kilometr
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
</s>
</p>
<p>
<s>
Nejvyšší	vysoký	k2eAgInPc4d3	Nejvyšší
body	bod	k1gInPc4	bod
<g/>
:	:	kIx,	:
Mt	Mt	k1gFnSc1	Mt
Kosciusko	Kosciusko	k1gNnSc4	Kosciusko
(	(	kIx(	(
<g/>
pevninská	pevninský	k2eAgFnSc1d1	pevninská
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
2228	[number]	k4	2228
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Puncak	Puncak	k1gMnSc1	Puncak
Jaya	Jaya	k1gMnSc1	Jaya
(	(	kIx(	(
<g/>
celkem	celkem	k6eAd1	celkem
<g/>
,	,	kIx,	,
4	[number]	k4	4
884	[number]	k4	884
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nejdelší	dlouhý	k2eAgFnSc1d3	nejdelší
nevysychající	vysychající	k2eNgFnSc1d1	nevysychající
řeka	řeka	k1gFnSc1	řeka
<g/>
:	:	kIx,	:
Murray	Murray	k1gInPc1	Murray
<g/>
(	(	kIx(	(
<g/>
2589	[number]	k4	2589
km	km	kA	km
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
jezero	jezero	k1gNnSc1	jezero
<g/>
:	:	kIx,	:
Eyrovo	Eyrův	k2eAgNnSc1d1	Eyrův
jezero	jezero	k1gNnSc1	jezero
(	(	kIx(	(
<g/>
9583	[number]	k4	9583
kilometrů	kilometr	k1gInPc2	kilometr
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Státy	stát	k1gInPc1	stát
==	==	k?	==
</s>
</p>
<p>
<s>
Austrálie	Austrálie	k1gFnSc1	Austrálie
</s>
</p>
<p>
<s>
Papua-Nová	Papua-Nový	k2eAgFnSc1d1	Papua-Nová
Guinea	Guinea	k1gFnSc1	Guinea
</s>
</p>
<p>
<s>
Indonésie	Indonésie	k1gFnSc1	Indonésie
–	–	k?	–
Irian	Irian	k1gMnSc1	Irian
Jaya	Jaya	k1gMnSc1	Jaya
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Austrálie	Austrálie	k1gFnSc2	Austrálie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Austrálie	Austrálie	k1gFnSc2	Austrálie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
