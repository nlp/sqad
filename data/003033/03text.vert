<s>
Synantropie	Synantropie	k1gFnSc1	Synantropie
je	být	k5eAaImIp3nS	být
odborný	odborný	k2eAgInSc4d1	odborný
biologický	biologický	k2eAgInSc4d1	biologický
pojem	pojem	k1gInSc4	pojem
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jím	on	k3xPp3gNnSc7	on
vyjadřováno	vyjadřován	k2eAgNnSc1d1	vyjadřováno
soužití	soužití	k1gNnSc1	soužití
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
živočichů	živočich	k1gMnPc2	živočich
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
člověka	člověk	k1gMnSc2	člověk
respektive	respektive	k9	respektive
v	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
jeho	jeho	k3xOp3gNnPc2	jeho
obydlí	obydlí	k1gNnPc2	obydlí
<g/>
.	.	kIx.	.
</s>
<s>
Synantropní	Synantropný	k2eAgMnPc1d1	Synantropný
živočichové	živočich	k1gMnPc1	živočich
tedy	tedy	k9	tedy
obvykle	obvykle	k6eAd1	obvykle
žijí	žít	k5eAaImIp3nP	žít
například	například	k6eAd1	například
uprostřed	uprostřed	k7c2	uprostřed
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
vesnic	vesnice	k1gFnPc2	vesnice
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
lidských	lidský	k2eAgNnPc2d1	lidské
sídel	sídlo	k1gNnPc2	sídlo
společně	společně	k6eAd1	společně
s	s	k7c7	s
člověkem	člověk	k1gMnSc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Totéž	týž	k3xTgNnSc1	týž
platí	platit	k5eAaImIp3nS	platit
i	i	k9	i
pro	pro	k7c4	pro
flóru	flóra	k1gFnSc4	flóra
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
synantropní	synantropní	k2eAgInPc1d1	synantropní
druhy	druh	k1gInPc1	druh
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Krysa	krysa	k1gFnSc1	krysa
Potkan	potkan	k1gMnSc1	potkan
Vrabec	Vrabec	k1gMnSc1	Vrabec
domácí	domácí	k1gMnSc1	domácí
Hrdlička	Hrdlička	k1gMnSc1	Hrdlička
zahradní	zahradní	k2eAgFnSc4d1	zahradní
Myš	myš	k1gFnSc4	myš
domácí	domácí	k2eAgFnSc1d1	domácí
PETRÁČKOVÁ	Petráčková	k1gFnSc1	Petráčková
<g/>
,	,	kIx,	,
Věra	Věra	k1gFnSc1	Věra
<g/>
;	;	kIx,	;
KRAUS	Kraus	k1gMnSc1	Kraus
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Akademický	akademický	k2eAgInSc1d1	akademický
slovník	slovník	k1gInSc1	slovník
cizích	cizí	k2eAgNnPc2d1	cizí
slov	slovo	k1gNnPc2	slovo
A-	A-	k1gMnPc2	A-
<g/>
Ž.	Ž.	kA	Ž.
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
834	[number]	k4	834
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
607	[number]	k4	607
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
