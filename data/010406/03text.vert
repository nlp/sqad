<p>
<s>
Obrazárna	obrazárna	k1gFnSc1	obrazárna
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
Národní	národní	k2eAgFnSc6d1	národní
galerii	galerie	k1gFnSc6	galerie
druhou	druhý	k4xOgFnSc7	druhý
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
sbírkou	sbírka	k1gFnSc7	sbírka
obrazů	obraz	k1gInPc2	obraz
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vystavena	vystavit	k5eAaPmNgFnS	vystavit
v	v	k7c6	v
Arcibiskupském	arcibiskupský	k2eAgInSc6d1	arcibiskupský
zámku	zámek	k1gInSc6	zámek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
součástí	součást	k1gFnPc2	součást
světového	světový	k2eAgNnSc2d1	světové
přírodního	přírodní	k2eAgNnSc2d1	přírodní
a	a	k8xC	a
kulturního	kulturní	k2eAgNnSc2d1	kulturní
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
obrazárny	obrazárna	k1gFnSc2	obrazárna
==	==	k?	==
</s>
</p>
<p>
<s>
U	u	k7c2	u
zrodu	zrod	k1gInSc2	zrod
sbírky	sbírka	k1gFnSc2	sbírka
stál	stát	k5eAaImAgMnS	stát
olomoucký	olomoucký	k2eAgMnSc1d1	olomoucký
biskup	biskup	k1gMnSc1	biskup
Karel	Karel	k1gMnSc1	Karel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Lichtenstein	Lichtenstein	k1gMnSc1	Lichtenstein
<g/>
.	.	kIx.	.
</s>
<s>
Stávající	stávající	k2eAgFnSc4d1	stávající
skupinu	skupina	k1gFnSc4	skupina
obrazů	obraz	k1gInPc2	obraz
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
výrazně	výrazně	k6eAd1	výrazně
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
nejen	nejen	k6eAd1	nejen
nákupy	nákup	k1gInPc1	nákup
významných	významný	k2eAgNnPc2d1	významné
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
nákupem	nákup	k1gInSc7	nákup
celých	celý	k2eAgFnPc2d1	celá
sbírek	sbírka	k1gFnPc2	sbírka
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
i	i	k9	i
velmi	velmi	k6eAd1	velmi
významnou	významný	k2eAgFnSc4d1	významná
sbírku	sbírka	k1gFnSc4	sbírka
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dvou	dva	k4xCgNnPc2	dva
set	sto	k4xCgNnPc2	sto
obrazů	obraz	k1gInPc2	obraz
a	a	k8xC	a
grafik	grafika	k1gFnPc2	grafika
od	od	k7c2	od
bratrů	bratr	k1gMnPc2	bratr
Franze	Franze	k1gFnSc2	Franze
a	a	k8xC	a
Bernarda	Bernard	k1gMnSc4	Bernard
von	von	k1gInSc4	von
Imstenraed	Imstenraed	k1gMnSc1	Imstenraed
z	z	k7c2	z
Kolína	Kolín	k1gInSc2	Kolín
nad	nad	k7c7	nad
Rýnem	Rýn	k1gInSc7	Rýn
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
sbírka	sbírka	k1gFnSc1	sbírka
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
díla	dílo	k1gNnSc2	dílo
italských	italský	k2eAgMnPc2d1	italský
<g/>
,	,	kIx,	,
holandských	holandský	k2eAgMnPc2d1	holandský
<g/>
,	,	kIx,	,
vlámských	vlámský	k2eAgMnPc2d1	vlámský
i	i	k8xC	i
německých	německý	k2eAgMnPc2d1	německý
malířů	malíř	k1gMnPc2	malíř
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
olomouckých	olomoucký	k2eAgMnPc2d1	olomoucký
biskupů	biskup	k1gMnPc2	biskup
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Troyera	Troyer	k1gMnSc2	Troyer
<g/>
,	,	kIx,	,
Leopolda	Leopold	k1gMnSc2	Leopold
Egkha	Egkh	k1gMnSc2	Egkh
a	a	k8xC	a
Maxmiliána	Maxmilián	k1gMnSc2	Maxmilián
z	z	k7c2	z
Hamiltonu	Hamilton	k1gInSc2	Hamilton
působil	působit	k5eAaImAgMnS	působit
jako	jako	k8xC	jako
správce	správce	k1gMnSc1	správce
sbírky	sbírka	k1gFnSc2	sbírka
obrazů	obraz	k1gInPc2	obraz
jejich	jejich	k3xOp3gFnSc7	jejich
dvorní	dvorní	k2eAgMnSc1d1	dvorní
malíř	malíř	k1gMnSc1	malíř
Karel	Karel	k1gMnSc1	Karel
Josef	Josef	k1gMnSc1	Josef
Adolf	Adolf	k1gMnSc1	Adolf
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
hraběte	hrabě	k1gMnSc2	hrabě
Colloredo-Waldsee	Colloredo-Waldse	k1gMnSc2	Colloredo-Waldse
při	při	k7c6	při
úpravách	úprava	k1gFnPc6	úprava
interiérů	interiér	k1gInPc2	interiér
Trůnního	trůnní	k2eAgInSc2d1	trůnní
sálu	sál	k1gInSc2	sál
a	a	k8xC	a
Malé	Malé	k2eAgFnSc2d1	Malé
jídelny	jídelna	k1gFnSc2	jídelna
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
poškození	poškození	k1gNnSc3	poškození
několika	několik	k4yIc7	několik
děl	dělo	k1gNnPc2	dělo
úpravami	úprava	k1gFnPc7	úprava
do	do	k7c2	do
tak	tak	k6eAd1	tak
zvaných	zvaný	k2eAgFnPc2d1	zvaná
paneláží	paneláž	k1gFnPc2	paneláž
<g/>
.	.	kIx.	.
</s>
<s>
Úprava	úprava	k1gFnSc1	úprava
spočívala	spočívat	k5eAaImAgFnS	spočívat
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
formátů	formát	k1gInPc2	formát
domalováním	domalování	k1gNnSc7	domalování
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
oříznutím	oříznutí	k1gNnSc7	oříznutí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c4	o
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
arcibiskupa	arcibiskup	k1gMnSc2	arcibiskup
Kohna	Kohn	k1gMnSc2	Kohn
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
sbírka	sbírka	k1gFnSc1	sbírka
restaurována	restaurován	k2eAgFnSc1d1	restaurována
vídeňskými	vídeňský	k2eAgMnPc7d1	vídeňský
odborníky	odborník	k1gMnPc7	odborník
a	a	k8xC	a
zpřístupněna	zpřístupněn	k2eAgFnSc1d1	zpřístupněna
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
obrazů	obraz	k1gInPc2	obraz
je	být	k5eAaImIp3nS	být
vystavena	vystavit	k5eAaPmNgFnS	vystavit
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
historických	historický	k2eAgInPc6d1	historický
interiérech	interiér	k1gInPc6	interiér
zámku	zámek	k1gInSc2	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Podstatnější	podstatný	k2eAgFnSc1d2	podstatnější
část	část	k1gFnSc1	část
sbírky	sbírka	k1gFnSc2	sbírka
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
samostatné	samostatný	k2eAgFnSc2d1	samostatná
prohlídky	prohlídka	k1gFnSc2	prohlídka
obrazárny	obrazárna	k1gFnSc2	obrazárna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významná	významný	k2eAgNnPc1d1	významné
díla	dílo	k1gNnPc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Nejcennějším	cenný	k2eAgInSc7d3	nejcennější
obrazem	obraz	k1gInSc7	obraz
sbírky	sbírka	k1gFnSc2	sbírka
je	být	k5eAaImIp3nS	být
obraz	obraz	k1gInSc1	obraz
známého	známý	k2eAgMnSc2d1	známý
benátského	benátský	k2eAgMnSc2d1	benátský
malíře	malíř	k1gMnSc2	malíř
Tiziana	Tizian	k1gMnSc4	Tizian
Vecellia	Vecellius	k1gMnSc4	Vecellius
zvaný	zvaný	k2eAgMnSc1d1	zvaný
Apollo	Apollo	k1gMnSc1	Apollo
a	a	k8xC	a
Marsyas	Marsyas	k1gMnSc1	Marsyas
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
obraze	obraz	k1gInSc6	obraz
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
antický	antický	k2eAgInSc1d1	antický
mýtus	mýtus	k1gInSc1	mýtus
o	o	k7c6	o
potrestání	potrestání	k1gNnSc6	potrestání
satyra	satyr	k1gMnSc2	satyr
Marsya	Marsyus	k1gMnSc2	Marsyus
po	po	k7c6	po
hudebním	hudební	k2eAgInSc6d1	hudební
souboji	souboj	k1gInSc6	souboj
s	s	k7c7	s
bohem	bůh	k1gMnSc7	bůh
Apollonem	Apollo	k1gMnSc7	Apollo
<g/>
.	.	kIx.	.
</s>
<s>
Král	Král	k1gMnSc1	Král
Midas	Midas	k1gMnSc1	Midas
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
rozhodnout	rozhodnout	k5eAaPmF	rozhodnout
o	o	k7c6	o
vítězi	vítěz	k1gMnSc6	vítěz
souboje	souboj	k1gInSc2	souboj
na	na	k7c6	na
obrazu	obraz	k1gInSc6	obraz
nese	nést	k5eAaImIp3nS	nést
Tizianovu	Tizianův	k2eAgFnSc4d1	Tizianova
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
dílo	dílo	k1gNnSc1	dílo
bývá	bývat	k5eAaImIp3nS	bývat
při	při	k7c6	při
významných	významný	k2eAgFnPc6d1	významná
příležitostech	příležitost	k1gFnPc6	příležitost
zapůjčováno	zapůjčován	k2eAgNnSc1d1	zapůjčováno
do	do	k7c2	do
světových	světový	k2eAgFnPc2d1	světová
galerií	galerie	k1gFnPc2	galerie
<g/>
.	.	kIx.	.
</s>
<s>
Obraz	obraz	k1gInSc1	obraz
byl	být	k5eAaImAgInS	být
vystaven	vystavit	k5eAaPmNgInS	vystavit
například	například	k6eAd1	například
na	na	k7c6	na
výstavách	výstava	k1gFnPc6	výstava
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
Washingtonu	Washington	k1gInSc6	Washington
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
sbírce	sbírka	k1gFnSc6	sbírka
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
díla	dílo	k1gNnPc1	dílo
nizozemského	nizozemský	k2eAgNnSc2d1	Nizozemské
malířství	malířství	k1gNnSc2	malířství
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
například	například	k6eAd1	například
Jan	Jan	k1gMnSc1	Jan
Brueghel	Brueghel	k1gMnSc1	Brueghel
starší	starší	k1gMnSc1	starší
<g/>
:	:	kIx,	:
Selská	selský	k2eAgFnSc1d1	selská
rvačka	rvačka	k1gFnSc1	rvačka
(	(	kIx(	(
<g/>
ca	ca	kA	ca
1610	[number]	k4	1610
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Anthonis	Anthonis	k1gInSc1	Anthonis
van	van	k1gInSc1	van
Dyck	Dyck	k1gInSc4	Dyck
<g/>
:	:	kIx,	:
Anglický	anglický	k2eAgMnSc1d1	anglický
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
I.	I.	kA	I.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
Henrietta	Henrietta	k1gFnSc1	Henrietta
Marie	Marie	k1gFnSc1	Marie
(	(	kIx(	(
<g/>
1632	[number]	k4	1632
<g/>
–	–	k?	–
<g/>
1634	[number]	k4	1634
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
Lucas	Lucas	k1gMnSc1	Lucas
Cranach	Cranach	k1gMnSc1	Cranach
starší	starší	k1gMnSc1	starší
<g/>
:	:	kIx,	:
Stětí	Stěť	k1gFnSc7	Stěť
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
a	a	k8xC	a
Stětí	Stětí	k1gNnSc1	Stětí
sv.	sv.	kA	sv.
Kateřiny	Kateřina	k1gFnSc2	Kateřina
(	(	kIx(	(
<g/>
ca	ca	kA	ca
1515	[number]	k4	1515
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Annibale	Annibal	k1gInSc6	Annibal
Carracci	Carracce	k1gFnSc3	Carracce
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
1588	[number]	k4	1588
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Latona	Latona	k1gFnSc1	Latona
a	a	k8xC	a
sedláci	sedlák	k1gMnPc1	sedlák
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnPc1d1	další
významní	významný	k2eAgMnPc1d1	významný
malíři	malíř	k1gMnPc1	malíř
zastoupení	zastoupení	k1gNnSc2	zastoupení
v	v	k7c6	v
obrazárně	obrazárna	k1gFnSc6	obrazárna
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
Paolo	Paolo	k1gNnSc1	Paolo
Veronese	Veronese	k1gFnSc1	Veronese
(	(	kIx(	(
<g/>
užívající	užívající	k2eAgMnPc1d1	užívající
i	i	k8xC	i
jmen	jméno	k1gNnPc2	jméno
P.	P.	kA	P.
Cagliari	Cagliar	k1gFnSc2	Cagliar
nebo	nebo	k8xC	nebo
P.	P.	kA	P.
Veronese	Veronese	k1gFnSc1	Veronese
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hans	Hans	k1gMnSc1	Hans
von	von	k1gInSc1	von
Aachen	Aachno	k1gNnPc2	Aachno
<g/>
,	,	kIx,	,
P.	P.	kA	P.
Coeck	Coeck	k1gInSc1	Coeck
van	van	k1gInSc1	van
Aelst	Aelst	k1gInSc4	Aelst
,	,	kIx,	,
Jacopo	Jacopa	k1gFnSc5	Jacopa
Bassano	Bassana	k1gFnSc5	Bassana
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
byl	být	k5eAaImAgMnS	být
z	z	k7c2	z
podnětu	podnět	k1gInSc2	podnět
prof.	prof.	kA	prof.
Ivo	Ivo	k1gMnSc1	Ivo
Hlobila	Hlobila	k1gMnSc1	Hlobila
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
zámku	zámek	k1gInSc2	zámek
znovuobjeven	znovuobjeven	k2eAgInSc4d1	znovuobjeven
velkoformátový	velkoformátový	k2eAgInSc4d1	velkoformátový
obraz	obraz	k1gInSc4	obraz
Zavraždění	zavraždění	k1gNnSc2	zavraždění
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
vídeňského	vídeňský	k2eAgMnSc2d1	vídeňský
malíře	malíř	k1gMnSc2	malíř
Antona	Anton	k1gMnSc2	Anton
Pettera	Petter	k1gMnSc2	Petter
namalovaný	namalovaný	k2eAgInSc4d1	namalovaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1844	[number]	k4	1844
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
který	který	k3yRgMnSc1	který
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
moment	moment	k1gInSc4	moment
zavraždění	zavraždění	k1gNnPc2	zavraždění
knížete	kníže	k1gMnSc2	kníže
Václava	Václav	k1gMnSc2	Václav
jeho	jeho	k3xOp3gMnSc7	jeho
bratrem	bratr	k1gMnSc7	bratr
Boleslavem	Boleslav	k1gMnSc7	Boleslav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
rozměry	rozměr	k1gInPc7	rozměr
8,5	[number]	k4	8,5
na	na	k7c4	na
5,1	[number]	k4	5,1
metru	metr	k1gInSc2	metr
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
k	k	k7c3	k
největším	veliký	k2eAgInPc3d3	veliký
obrazům	obraz	k1gInPc3	obraz
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
PEŘINKA	peřinka	k1gFnSc1	peřinka
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Vácslav	Vácslav	k1gMnSc1	Vácslav
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
města	město	k1gNnSc2	město
Kroměříže	Kroměříž	k1gFnPc1	Kroměříž
<g/>
.	.	kIx.	.
</s>
<s>
Díl	díl	k1gInSc1	díl
I.	I.	kA	I.
<g/>
,	,	kIx,	,
Obsahující	obsahující	k2eAgFnSc4d1	obsahující
dobu	doba	k1gFnSc4	doba
po	po	k7c4	po
rok	rok	k1gInSc4	rok
1620	[number]	k4	1620
/	/	kIx~	/
dle	dle	k7c2	dle
původních	původní	k2eAgInPc2d1	původní
pramenů	pramen	k1gInPc2	pramen
vypravuje	vypravovat	k5eAaImIp3nS	vypravovat
Frant	Frant	k1gInSc1	Frant
<g/>
.	.	kIx.	.
</s>
<s>
Vácsl	Vácsl	k1gInSc1	Vácsl
<g/>
.	.	kIx.	.
</s>
<s>
Peřinka	peřinka	k1gFnSc1	peřinka
<g/>
.	.	kIx.	.
</s>
<s>
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
<g/>
:	:	kIx,	:
Obecní	obecní	k2eAgFnSc1d1	obecní
rada	rada	k1gFnSc1	rada
<g/>
,	,	kIx,	,
1913	[number]	k4	1913
<g/>
.	.	kIx.	.
544	[number]	k4	544
s.	s.	k?	s.
</s>
</p>
<p>
<s>
PEŘINKA	peřinka	k1gFnSc1	peřinka
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Vácslav	Vácslav	k1gMnSc1	Vácslav
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
města	město	k1gNnSc2	město
Kroměříže	Kroměříž	k1gFnSc2	Kroměříž
:	:	kIx,	:
(	(	kIx(	(
<g/>
dějiny	dějiny	k1gFnPc1	dějiny
let	léto	k1gNnPc2	léto
1619	[number]	k4	1619
<g/>
–	–	k?	–
<g/>
1695	[number]	k4	1695
<g/>
)	)	kIx)	)
;	;	kIx,	;
Díl	díl	k1gInSc4	díl
druhý	druhý	k4xOgInSc4	druhý
;	;	kIx,	;
část	část	k1gFnSc1	část
I.	I.	kA	I.
<g/>
,	,	kIx,	,
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
za	za	k7c2	za
války	válka	k1gFnSc2	válka
třicetileté	třicetiletý	k2eAgNnSc1d1	třicetileté
;	;	kIx,	;
část	část	k1gFnSc1	část
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
život	život	k1gInSc1	život
–	–	k?	–
Biskup	biskup	k1gMnSc1	biskup
Karel	Karel	k1gMnSc1	Karel
z	z	k7c2	z
Lichtenštejna	Lichtenštejn	k1gInSc2	Lichtenštejn
/	/	kIx~	/
dle	dle	k7c2	dle
původních	původní	k2eAgInPc2d1	původní
pramenů	pramen	k1gInPc2	pramen
vypravuje	vypravovat	k5eAaImIp3nS	vypravovat
František	František	k1gMnSc1	František
Vácslav	Vácslav	k1gMnSc1	Vácslav
Peřinka	peřinka	k1gFnSc1	peřinka
<g/>
.	.	kIx.	.
</s>
<s>
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
<g/>
:	:	kIx,	:
Národní	národní	k2eAgInSc1d1	národní
výbor	výbor	k1gInSc1	výbor
<g/>
,	,	kIx,	,
1947	[number]	k4	1947
<g/>
.	.	kIx.	.
1072	[number]	k4	1072
s.	s.	k?	s.
</s>
</p>
<p>
<s>
BREITENBACHER	BREITENBACHER	kA	BREITENBACHER
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
arcibiskupské	arcibiskupský	k2eAgFnSc2d1	arcibiskupská
obrazárny	obrazárna	k1gFnSc2	obrazárna
v	v	k7c6	v
Kroměříži	Kroměříž	k1gFnSc6	Kroměříž
:	:	kIx,	:
archivní	archivní	k2eAgFnSc2d1	archivní
studie	studie	k1gFnSc2	studie
<g/>
.	.	kIx.	.
</s>
<s>
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
<g/>
:	:	kIx,	:
Leopold	Leopold	k1gMnSc1	Leopold
Prečan	Prečan	k1gMnSc1	Prečan
<g/>
,	,	kIx,	,
1925	[number]	k4	1925
<g/>
–	–	k?	–
<g/>
1927	[number]	k4	1927
<g/>
.	.	kIx.	.
2	[number]	k4	2
svazky	svazek	k1gInPc4	svazek
(	(	kIx(	(
<g/>
186	[number]	k4	186
s.	s.	k?	s.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Obrazárna	obrazárna	k1gFnSc1	obrazárna
</s>
</p>
<p>
<s>
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
</s>
</p>
<p>
<s>
Arcibiskupský	arcibiskupský	k2eAgInSc1d1	arcibiskupský
zámek	zámek	k1gInSc1	zámek
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
</s>
</p>
<p>
<s>
Kroměřížská	kroměřížský	k2eAgFnSc1d1	Kroměřížská
arcibiskupská	arcibiskupský	k2eAgFnSc1d1	arcibiskupská
zámecká	zámecký	k2eAgFnSc1d1	zámecká
knihovna	knihovna	k1gFnSc1	knihovna
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Arcibiskupský	arcibiskupský	k2eAgInSc1d1	arcibiskupský
zámek	zámek	k1gInSc1	zámek
a	a	k8xC	a
zahrady	zahrada	k1gFnSc2	zahrada
Kroměříž	Kroměříž	k1gFnSc1	Kroměříž
<g/>
,	,	kIx,	,
obrazárna	obrazárna	k1gFnSc1	obrazárna
</s>
</p>
