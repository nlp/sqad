<s>
Sicílie	Sicílie	k1gFnSc1	Sicílie
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
Sicilia	Sicilius	k1gMnSc2	Sicilius
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
autonomní	autonomní	k2eAgInSc1d1	autonomní
region	region	k1gInSc1	region
Itálie	Itálie	k1gFnSc2	Itálie
na	na	k7c6	na
stejnojmenném	stejnojmenný	k2eAgInSc6d1	stejnojmenný
největším	veliký	k2eAgInSc6d3	veliký
ostrově	ostrov	k1gInSc6	ostrov
ve	v	k7c6	v
Středozemním	středozemní	k2eAgNnSc6d1	středozemní
moři	moře	k1gNnSc6	moře
Sicílii	Sicílie	k1gFnSc4	Sicílie
a	a	k8xC	a
okolních	okolní	k2eAgInPc6d1	okolní
malých	malý	k2eAgInPc6d1	malý
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
sjednocením	sjednocení	k1gNnSc7	sjednocení
Itálie	Itálie	k1gFnSc2	Itálie
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
oblast	oblast	k1gFnSc4	oblast
součástí	součást	k1gFnPc2	součást
Království	království	k1gNnSc2	království
obojí	oboj	k1gFnPc2	oboj
Sicílie	Sicílie	k1gFnSc2	Sicílie
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
25	[number]	k4	25
703	[number]	k4	703
km2	km2	k4	km2
<g/>
,	,	kIx,	,
5	[number]	k4	5
miliónů	milión	k4xCgInPc2	milión
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
na	na	k7c4	na
9	[number]	k4	9
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Palermo	Palermo	k1gNnSc1	Palermo
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
italštiny	italština	k1gFnSc2	italština
se	se	k3xPyFc4	se
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
mluví	mluvit	k5eAaImIp3nS	mluvit
i	i	k9	i
sicilštinou	sicilština	k1gFnSc7	sicilština
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Sicílie	Sicílie	k1gFnSc2	Sicílie
(	(	kIx(	(
<g/>
ostrov	ostrov	k1gInSc1	ostrov
<g/>
)	)	kIx)	)
<g/>
#	#	kIx~	#
<g/>
Dějiny	dějiny	k1gFnPc1	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Autonomní	autonomní	k2eAgInSc1d1	autonomní
region	region	k1gInSc1	region
byl	být	k5eAaImAgInS	být
ustaven	ustavit	k5eAaPmNgInS	ustavit
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1946	[number]	k4	1946
po	po	k7c6	po
přeměně	přeměna	k1gFnSc6	přeměna
Itálie	Itálie	k1gFnSc2	Itálie
z	z	k7c2	z
království	království	k1gNnSc2	království
na	na	k7c4	na
republiku	republika	k1gFnSc4	republika
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
status	status	k1gInSc1	status
byl	být	k5eAaImAgInS	být
revidován	revidovat	k5eAaImNgInS	revidovat
26	[number]	k4	26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
italských	italský	k2eAgInPc2d1	italský
autonomních	autonomní	k2eAgInPc2d1	autonomní
regionů	region	k1gInPc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
italské	italský	k2eAgFnPc1d1	italská
správní	správní	k2eAgFnPc1d1	správní
reformy	reforma	k1gFnPc1	reforma
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1950	[number]	k4	1950
a	a	k8xC	a
1984	[number]	k4	1984
přinesly	přinést	k5eAaPmAgFnP	přinést
pomoc	pomoc	k1gFnSc4	pomoc
sicilské	sicilský	k2eAgFnSc3d1	sicilská
ekonomice	ekonomika	k1gFnSc3	ekonomika
především	především	k9	především
díky	díky	k7c3	díky
důležitým	důležitý	k2eAgFnPc3d1	důležitá
investicím	investice	k1gFnPc3	investice
do	do	k7c2	do
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
podpořily	podpořit	k5eAaPmAgFnP	podpořit
výstavbu	výstavba	k1gFnSc4	výstavba
dálnic	dálnice	k1gFnPc2	dálnice
a	a	k8xC	a
letišť	letiště	k1gNnPc2	letiště
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Sicílie	Sicílie	k1gFnSc2	Sicílie
(	(	kIx(	(
<g/>
ostrov	ostrov	k1gInSc1	ostrov
<g/>
)	)	kIx)	)
<g/>
#	#	kIx~	#
<g/>
Geografie	geografie	k1gFnSc1	geografie
<g/>
.	.	kIx.	.
</s>
<s>
Autonomní	autonomní	k2eAgInSc1d1	autonomní
region	region	k1gInSc1	region
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
hlavní	hlavní	k2eAgInSc1d1	hlavní
ostrov	ostrov	k1gInSc1	ostrov
Sicílie	Sicílie	k1gFnSc2	Sicílie
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
98,9	[number]	k4	98,9
%	%	kIx~	%
jeho	jeho	k3xOp3gFnSc2	jeho
rozlohy	rozloha	k1gFnSc2	rozloha
<g/>
,	,	kIx,	,
a	a	k8xC	a
okolní	okolní	k2eAgInPc4d1	okolní
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Oblast	oblast	k1gFnSc1	oblast
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
ze	z	k7c2	z
seizmicky	seizmicky	k6eAd1	seizmicky
nejaktivnějších	aktivní	k2eAgMnPc2d3	nejaktivnější
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
je	být	k5eAaImIp3nS	být
Etna	Etna	k1gFnSc1	Etna
na	na	k7c6	na
hlavním	hlavní	k2eAgInSc6d1	hlavní
ostrově	ostrov	k1gInSc6	ostrov
(	(	kIx(	(
<g/>
3	[number]	k4	3
323	[number]	k4	323
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
činnou	činný	k2eAgFnSc7d1	činná
sopkou	sopka	k1gFnSc7	sopka
evropského	evropský	k2eAgInSc2d1	evropský
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Sopečného	sopečný	k2eAgInSc2d1	sopečný
původu	původ	k1gInSc2	původ
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
Liparské	Liparský	k2eAgNnSc1d1	Liparské
ostrovyv	ostrovyv	k1gInSc4	ostrovyv
Tyrhénském	tyrhénský	k2eAgNnSc6d1	Tyrhénské
moři	moře	k1gNnSc6	moře
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
metropolitnímu	metropolitní	k2eAgNnSc3d1	metropolitní
městu	město	k1gNnSc3	město
Messina	Messina	k1gFnSc1	Messina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
sedmi	sedm	k4xCc2	sedm
ostrovů	ostrov	k1gInPc2	ostrov
jsou	být	k5eAaImIp3nP	být
dosud	dosud	k6eAd1	dosud
aktivními	aktivní	k2eAgFnPc7d1	aktivní
sopkami	sopka	k1gFnPc7	sopka
Stromboli	Strombole	k1gFnSc3	Strombole
a	a	k8xC	a
Vulcano	Vulcana	k1gFnSc5	Vulcana
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
ostrovů	ostrov	k1gInPc2	ostrov
Sicílie	Sicílie	k1gFnSc2	Sicílie
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
autonomnímu	autonomní	k2eAgInSc3d1	autonomní
regionu	region	k1gInSc3	region
náleží	náležet	k5eAaImIp3nP	náležet
kromě	kromě	k7c2	kromě
hlavního	hlavní	k2eAgInSc2d1	hlavní
ostrova	ostrov	k1gInSc2	ostrov
také	také	k9	také
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Liparské	Liparský	k2eAgInPc4d1	Liparský
ostrovy	ostrov	k1gInPc4	ostrov
a	a	k8xC	a
ostrov	ostrov	k1gInSc4	ostrov
Ustica	Ustic	k1gInSc2	Ustic
<g/>
,	,	kIx,	,
na	na	k7c6	na
západě	západ	k1gInSc6	západ
Egadské	Egadský	k2eAgFnPc1d1	Egadský
a	a	k8xC	a
Stagnonské	Stagnonský	k2eAgInPc1d1	Stagnonský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
ostrov	ostrov	k1gInSc1	ostrov
Pantelleria	Pantellerium	k1gNnSc2	Pantellerium
a	a	k8xC	a
Pelagické	pelagický	k2eAgInPc4d1	pelagický
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Přehled	přehled	k1gInSc1	přehled
ostrovů	ostrov	k1gInPc2	ostrov
autonomního	autonomní	k2eAgInSc2d1	autonomní
regionu	region	k1gInSc2	region
Sicílie	Sicílie	k1gFnSc2	Sicílie
větších	veliký	k2eAgNnPc2d2	veliký
než	než	k8xS	než
2	[number]	k4	2
km2	km2	k4	km2
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
tabulka	tabulka	k1gFnSc1	tabulka
<g/>
:	:	kIx,	:
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
se	se	k3xPyFc4	se
region	region	k1gInSc1	region
skládal	skládat	k5eAaImAgInS	skládat
z	z	k7c2	z
9	[number]	k4	9
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
na	na	k7c4	na
zasedání	zasedání	k1gNnSc4	zasedání
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
autonomního	autonomní	k2eAgInSc2d1	autonomní
regionu	region	k1gInSc2	region
Sicílie	Sicílie	k1gFnSc2	Sicílie
byla	být	k5eAaImAgFnS	být
schválena	schválen	k2eAgFnSc1d1	schválena
reforma	reforma	k1gFnSc1	reforma
správního	správní	k2eAgNnSc2d1	správní
uspořádání	uspořádání	k1gNnSc2	uspořádání
tohoto	tento	k3xDgInSc2	tento
regionu	region	k1gInSc2	region
<g/>
,	,	kIx,	,
nově	nově	k6eAd1	nově
určující	určující	k2eAgNnSc1d1	určující
postavení	postavení	k1gNnSc1	postavení
center	centrum	k1gNnPc2	centrum
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
celků	celek	k1gInPc2	celek
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
tří	tři	k4xCgMnPc2	tři
tzv.	tzv.	kA	tzv.
metropolitních	metropolitní	k2eAgNnPc2d1	metropolitní
měst	město	k1gNnPc2	město
-	-	kIx~	-
Palerma	Palermo	k1gNnPc4	Palermo
<g/>
,	,	kIx,	,
Catanie	Catanie	k1gFnPc4	Catanie
a	a	k8xC	a
Messiny	Messina	k1gFnPc4	Messina
<g/>
.	.	kIx.	.
</s>
<s>
Zbývající	zbývající	k2eAgFnPc1d1	zbývající
provincie	provincie	k1gFnPc1	provincie
byly	být	k5eAaImAgFnP	být
přetransformovány	přetransformovat	k5eAaPmNgFnP	přetransformovat
do	do	k7c2	do
tzv.	tzv.	kA	tzv.
volných	volný	k2eAgInPc2d1	volný
sdružení	sdružení	k1gNnSc4	sdružení
obcí	obec	k1gFnPc2	obec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
pracuje	pracovat	k5eAaImIp3nS	pracovat
12	[number]	k4	12
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejrozšířenější	rozšířený	k2eAgFnPc4d3	nejrozšířenější
plodiny	plodina	k1gFnPc4	plodina
pěstované	pěstovaný	k2eAgFnPc4d1	pěstovaná
na	na	k7c6	na
sicilském	sicilský	k2eAgNnSc6d1	sicilské
území	území	k1gNnSc6	území
patří	patřit	k5eAaImIp3nS	patřit
oliva	oliva	k1gFnSc1	oliva
<g/>
,	,	kIx,	,
mandle	mandle	k1gFnSc1	mandle
<g/>
,	,	kIx,	,
vinná	vinný	k2eAgFnSc1d1	vinná
réva	réva	k1gFnSc1	réva
<g/>
,	,	kIx,	,
citrusy	citrus	k1gInPc1	citrus
a	a	k8xC	a
pistácie	pistácie	k1gFnPc1	pistácie
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
zde	zde	k6eAd1	zde
nalézt	nalézt	k5eAaBmF	nalézt
i	i	k9	i
aleje	alej	k1gFnPc4	alej
korkových	korkový	k2eAgInPc2d1	korkový
dubů	dub	k1gInPc2	dub
<g/>
.	.	kIx.	.
</s>
<s>
Plemenářství	plemenářství	k1gNnSc1	plemenářství
a	a	k8xC	a
chov	chov	k1gInSc1	chov
zvířat	zvíře	k1gNnPc2	zvíře
nehraje	hrát	k5eNaImIp3nS	hrát
v	v	k7c6	v
sicilském	sicilský	k2eAgNnSc6d1	sicilské
hospodářství	hospodářství	k1gNnSc6	hospodářství
velkou	velký	k2eAgFnSc4d1	velká
úlohu	úloha	k1gFnSc4	úloha
<g/>
.	.	kIx.	.
</s>
<s>
Zmínit	zmínit	k5eAaPmF	zmínit
lze	lze	k6eAd1	lze
snad	snad	k9	snad
odchov	odchov	k1gInSc1	odchov
ryb	ryba	k1gFnPc2	ryba
v	v	k7c6	v
sádkách	sádka	k1gFnPc6	sádka
a	a	k8xC	a
řídce	řídce	k6eAd1	řídce
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgInSc1d1	vyskytující
chov	chov	k1gInSc1	chov
koz	koza	k1gFnPc2	koza
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
hornatých	hornatý	k2eAgFnPc6d1	hornatá
oblastech	oblast	k1gFnPc6	oblast
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sektoru	sektor	k1gInSc6	sektor
obchodu	obchod	k1gInSc2	obchod
a	a	k8xC	a
služeb	služba	k1gFnPc2	služba
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
zaměstnáno	zaměstnat	k5eAaPmNgNnS	zaměstnat
68	[number]	k4	68
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Průmysl	průmysl	k1gInSc1	průmysl
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
20	[number]	k4	20
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c6	na
industriálně	industriálně	k6eAd1	industriálně
rozvinutější	rozvinutý	k2eAgFnSc6d2	rozvinutější
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Messina	Messina	k1gFnSc1	Messina
<g/>
,	,	kIx,	,
Syrakusy	Syrakusy	k1gFnPc1	Syrakusy
<g/>
,	,	kIx,	,
Caltanissetta	Caltanissetto	k1gNnPc1	Caltanissetto
a	a	k8xC	a
Catania	Catanium	k1gNnPc1	Catanium
a	a	k8xC	a
na	na	k7c4	na
oblasti	oblast	k1gFnPc4	oblast
s	s	k7c7	s
tradičním	tradiční	k2eAgNnSc7d1	tradiční
a	a	k8xC	a
méně	málo	k6eAd2	málo
technicky	technicky	k6eAd1	technicky
rozvinutým	rozvinutý	k2eAgInSc7d1	rozvinutý
průmyslem	průmysl	k1gInSc7	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozvinutých	rozvinutý	k2eAgFnPc6d1	rozvinutá
oblastech	oblast	k1gFnPc6	oblast
najdeme	najít	k5eAaPmIp1nP	najít
převážně	převážně	k6eAd1	převážně
průmysl	průmysl	k1gInSc4	průmysl
petrochemický	petrochemický	k2eAgInSc4d1	petrochemický
<g/>
,	,	kIx,	,
strojírenský	strojírenský	k2eAgInSc4d1	strojírenský
a	a	k8xC	a
elektrotechnický	elektrotechnický	k2eAgInSc4d1	elektrotechnický
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ostatních	ostatní	k2eAgFnPc6d1	ostatní
oblastech	oblast	k1gFnPc6	oblast
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
technologicky	technologicky	k6eAd1	technologicky
méně	málo	k6eAd2	málo
náročnou	náročný	k2eAgFnSc4d1	náročná
výrobu	výroba	k1gFnSc4	výroba
olivového	olivový	k2eAgInSc2d1	olivový
oleje	olej	k1gInSc2	olej
<g/>
,	,	kIx,	,
těstovin	těstovina	k1gFnPc2	těstovina
nebo	nebo	k8xC	nebo
vína	víno	k1gNnSc2	víno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
můžeme	moct	k5eAaImIp1nP	moct
registrovat	registrovat	k5eAaBmF	registrovat
také	také	k9	také
těžbu	těžba	k1gFnSc4	těžba
nerostných	nerostný	k2eAgFnPc2d1	nerostná
surovin	surovina	k1gFnPc2	surovina
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
kamenná	kamenný	k2eAgFnSc1d1	kamenná
sůl	sůl	k1gFnSc1	sůl
nebo	nebo	k8xC	nebo
methan	methan	k1gInSc1	methan
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
ostrov	ostrov	k1gInSc1	ostrov
ve	v	k7c6	v
Středozemí	středozemí	k1gNnSc6	středozemí
byl	být	k5eAaImAgMnS	být
během	během	k7c2	během
podmaňovaní	podmaňovaný	k2eAgMnPc1d1	podmaňovaný
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
a	a	k8xC	a
středověku	středověk	k1gInSc6	středověk
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
velkým	velký	k2eAgInSc7d1	velký
množství	množství	k1gNnSc4	množství
kultur	kultura	k1gFnPc2	kultura
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
řeckou	řecký	k2eAgFnSc4d1	řecká
<g/>
,	,	kIx,	,
římskou	římský	k2eAgFnSc4d1	římská
<g/>
,	,	kIx,	,
aragonskou	aragonský	k2eAgFnSc4d1	Aragonská
<g/>
,	,	kIx,	,
normanskou	normanský	k2eAgFnSc4d1	normanská
<g/>
,	,	kIx,	,
arabskou	arabský	k2eAgFnSc4d1	arabská
ad	ad	k7c4	ad
<g/>
.	.	kIx.	.
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
i	i	k9	i
na	na	k7c4	na
gastronomii	gastronomie	k1gFnSc4	gastronomie
<g/>
.	.	kIx.	.
</s>
<s>
Sicilská	sicilský	k2eAgFnSc1d1	sicilská
kuchyně	kuchyně	k1gFnSc1	kuchyně
je	být	k5eAaImIp3nS	být
však	však	k9	však
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
i	i	k8xC	i
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
a	a	k8xC	a
africkou	africký	k2eAgFnSc7d1	africká
kuchyní	kuchyně	k1gFnSc7	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
Víno	víno	k1gNnSc1	víno
Malvázie	Malvázie	k1gFnSc2	Malvázie
z	z	k7c2	z
Liparských	Liparský	k2eAgInPc2d1	Liparský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
vůně	vůně	k1gFnSc2	vůně
mandlovníků	mandlovník	k1gInPc2	mandlovník
<g/>
,	,	kIx,	,
pistáciových	pistáciový	k2eAgInPc2d1	pistáciový
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
olivových	olivový	k2eAgInPc2d1	olivový
hájů	háj	k1gInPc2	háj
<g/>
,	,	kIx,	,
citrusovníků	citrusovník	k1gInPc2	citrusovník
<g/>
,	,	kIx,	,
pínií	pínie	k1gFnSc7	pínie
se	se	k3xPyFc4	se
snůbí	snůbit	k5eAaPmIp3nS	snůbit
s	s	k7c7	s
kulinářskou	kulinářský	k2eAgFnSc7d1	kulinářská
jednoduchostí	jednoduchost	k1gFnSc7	jednoduchost
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
kousek	kousek	k1gInSc1	kousek
sicilského	sicilský	k2eAgNnSc2d1	sicilské
jídla	jídlo	k1gNnSc2	jídlo
je	být	k5eAaImIp3nS	být
zážitkem	zážitek	k1gInSc7	zážitek
<g/>
.	.	kIx.	.
</s>
<s>
Pestrá	pestrý	k2eAgNnPc1d1	pestré
jídla	jídlo	k1gNnPc1	jídlo
jsou	být	k5eAaImIp3nP	být
dochucené	dochucený	k2eAgInPc1d1	dochucený
vlastní	vlastní	k2eAgFnSc7d1	vlastní
solí	sůl	k1gFnSc7	sůl
z	z	k7c2	z
Marsaly	Marsaly	k1gFnSc2	Marsaly
a	a	k8xC	a
využitím	využití	k1gNnSc7	využití
bylinek	bylinka	k1gFnPc2	bylinka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
základ	základ	k1gInSc4	základ
sicilské	sicilský	k2eAgFnSc2d1	sicilská
kuchyně	kuchyně	k1gFnSc2	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
sicilské	sicilský	k2eAgFnSc3d1	sicilská
kuchyni	kuchyně	k1gFnSc3	kuchyně
neodmyslitelně	odmyslitelně	k6eNd1	odmyslitelně
patří	patřit	k5eAaImIp3nP	patřit
i	i	k9	i
mořské	mořský	k2eAgFnPc1d1	mořská
ryby	ryba	k1gFnPc1	ryba
a	a	k8xC	a
vynikající	vynikající	k2eAgInPc1d1	vynikající
sladké	sladký	k2eAgInPc1d1	sladký
zákusky	zákusek	k1gInPc1	zákusek
<g/>
.	.	kIx.	.
</s>
<s>
Palermo	Palermo	k1gNnSc1	Palermo
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc4	město
ležící	ležící	k2eAgNnSc4d1	ležící
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
založili	založit	k5eAaPmAgMnP	založit
Kartáginci	Kartáginec	k1gMnPc1	Kartáginec
<g/>
.	.	kIx.	.
</s>
<s>
Architektonicky	architektonicky	k6eAd1	architektonicky
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
různorodé	různorodý	k2eAgNnSc1d1	různorodé
díky	díky	k7c3	díky
častým	častý	k2eAgFnPc3d1	častá
změnám	změna	k1gFnPc3	změna
na	na	k7c6	na
sicilském	sicilský	k2eAgInSc6d1	sicilský
trůně	trůn	k1gInSc6	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
nejzajímavější	zajímavý	k2eAgFnPc4d3	nejzajímavější
památky	památka	k1gFnPc4	památka
můžeme	moct	k5eAaImIp1nP	moct
jmenovat	jmenovat	k5eAaImF	jmenovat
Normanský	normanský	k2eAgInSc4d1	normanský
palác	palác	k1gInSc4	palác
<g/>
,	,	kIx,	,
Teatro	Teatro	k1gNnSc4	Teatro
Maximo	Maxima	k1gFnSc5	Maxima
nebo	nebo	k8xC	nebo
kostel	kostel	k1gInSc4	kostel
San	San	k1gFnSc2	San
Cataldo	Cataldo	k1gNnSc4	Cataldo
<g/>
.	.	kIx.	.
</s>
<s>
Taormina	Taormin	k2eAgFnSc1d1	Taormina
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
také	také	k9	také
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
sicilské	sicilský	k2eAgFnSc2d1	sicilská
Monte	Mont	k1gMnSc5	Mont
Carlo	Carla	k1gMnSc5	Carla
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejoblíbenějších	oblíbený	k2eAgNnPc2d3	nejoblíbenější
sicilských	sicilský	k2eAgNnPc2d1	sicilské
letovisek	letovisko	k1gNnPc2	letovisko
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Taorminy	Taormin	k2eAgFnSc2d1	Taormina
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejkrásnějších	krásný	k2eAgInPc2d3	nejkrásnější
výhledů	výhled	k1gInPc2	výhled
na	na	k7c4	na
Etnu	Etna	k1gFnSc4	Etna
z	z	k7c2	z
celé	celý	k2eAgFnSc2d1	celá
Sicílie	Sicílie	k1gFnSc2	Sicílie
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejznámějším	známý	k2eAgFnPc3d3	nejznámější
památkám	památka	k1gFnPc3	památka
patří	patřit	k5eAaImIp3nS	patřit
řecký	řecký	k2eAgInSc4d1	řecký
amfiteátr	amfiteátr	k1gInSc4	amfiteátr
z	z	k7c2	z
3	[number]	k4	3
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
či	či	k8xC	či
Palazzo	Palazza	k1gFnSc5	Palazza
Santo	Santa	k1gFnSc5	Santa
Stefano	Stefana	k1gFnSc5	Stefana
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Taormina	Taormin	k2eAgInSc2d1	Taormin
bylo	být	k5eAaImAgNnS	být
údajně	údajně	k6eAd1	údajně
založeno	založit	k5eAaPmNgNnS	založit
obyvateli	obyvatel	k1gMnPc7	obyvatel
řeckého	řecký	k2eAgInSc2d1	řecký
ostrova	ostrov	k1gInSc2	ostrov
Naxos	Naxos	k1gInSc1	Naxos
<g/>
.	.	kIx.	.
</s>
<s>
Selinunte	Selinunt	k1gMnSc5	Selinunt
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgNnPc1d1	ležící
na	na	k7c6	na
západě	západ	k1gInSc6	západ
ostrova	ostrov	k1gInSc2	ostrov
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
opuštěné	opuštěný	k2eAgNnSc1d1	opuštěné
starořecké	starořecký	k2eAgNnSc1d1	starořecké
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
Nalezneme	nalézt	k5eAaBmIp1nP	nalézt
zde	zde	k6eAd1	zde
zbytky	zbytek	k1gInPc4	zbytek
akropole	akropole	k1gFnSc2	akropole
a	a	k8xC	a
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
chrámový	chrámový	k2eAgInSc4d1	chrámový
komplex	komplex	k1gInSc4	komplex
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
řeckými	řecký	k2eAgMnPc7d1	řecký
osadníky	osadník	k1gMnPc7	osadník
z	z	k7c2	z
města	město	k1gNnSc2	město
jménem	jméno	k1gNnSc7	jméno
Megara	Megar	k1gMnSc2	Megar
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
730	[number]	k4	730
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Agrigento	Agrigento	k1gNnSc1	Agrigento
leží	ležet	k5eAaImIp3nS	ležet
nedaleko	nedaleko	k7c2	nedaleko
již	již	k6eAd1	již
zmiňovaného	zmiňovaný	k2eAgNnSc2d1	zmiňované
Selinunte	Selinunt	k1gInSc5	Selinunt
<g/>
.	.	kIx.	.
</s>
<s>
Původem	původ	k1gInSc7	původ
řecké	řecký	k2eAgNnSc1d1	řecké
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
koncem	koncem	k7c2	koncem
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
osadníky	osadník	k1gMnPc4	osadník
z	z	k7c2	z
řecké	řecký	k2eAgInPc4d1	řecký
Gely	gel	k1gInPc4	gel
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgFnSc7d3	nejznámější
památkou	památka	k1gFnSc7	památka
je	být	k5eAaImIp3nS	být
Chrám	chrám	k1gInSc1	chrám
svornosti	svornost	k1gFnSc2	svornost
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
takzvaném	takzvaný	k2eAgNnSc6d1	takzvané
Údolí	údolí	k1gNnSc6	údolí
chrámů	chrám	k1gInPc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Sicilská	sicilský	k2eAgFnSc1d1	sicilská
sopka	sopka	k1gFnSc1	sopka
Etna	Etna	k1gFnSc1	Etna
se	se	k3xPyFc4	se
vypíná	vypínat	k5eAaImIp3nS	vypínat
do	do	k7c2	do
výše	výše	k1gFnSc2	výše
3	[number]	k4	3
323	[number]	k4	323
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
činných	činný	k2eAgFnPc2d1	činná
sopek	sopka	k1gFnPc2	sopka
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
tou	ten	k3xDgFnSc7	ten
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
evropskou	evropský	k2eAgFnSc7d1	Evropská
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
je	být	k5eAaImIp3nS	být
oblast	oblast	k1gFnSc1	oblast
Etny	Etna	k1gFnSc2	Etna
zapsána	zapsat	k5eAaPmNgFnS	zapsat
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
přírodních	přírodní	k2eAgFnPc2d1	přírodní
památek	památka	k1gFnPc2	památka
Světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Hora	hora	k1gFnSc1	hora
Eryx	Eryx	k1gInSc1	Eryx
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xC	jako
Monte	Mont	k1gInSc5	Mont
St.	st.	kA	st.
Giuliano	Giuliana	k1gFnSc5	Giuliana
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
ostrova	ostrov	k1gInSc2	ostrov
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
750	[number]	k4	750
metrů	metr	k1gInPc2	metr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
nádherný	nádherný	k2eAgInSc1d1	nádherný
výhled	výhled	k1gInSc4	výhled
na	na	k7c4	na
blízké	blízký	k2eAgNnSc4d1	blízké
okolí	okolí	k1gNnSc4	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
vrcholu	vrchol	k1gInSc6	vrchol
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
malebné	malebný	k2eAgNnSc4d1	malebné
městečko	městečko	k1gNnSc4	městečko
Erice	Erika	k1gFnSc3	Erika
<g/>
.	.	kIx.	.
</s>
