<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
Tanzanie	Tanzanie	k1gFnSc2	Tanzanie
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
celého	celý	k2eAgInSc2d1	celý
Afrického	africký	k2eAgInSc2d1	africký
kontinentu	kontinent	k1gInSc2	kontinent
je	být	k5eAaImIp3nS	být
vrchol	vrchol	k1gInSc1	vrchol
Uhuru	Uhur	k1gInSc2	Uhur
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
vrchol	vrchol	k1gInSc4	vrchol
svobody	svoboda	k1gFnSc2	svoboda
<g/>
)	)	kIx)	)
na	na	k7c6	na
vulkánu	vulkán	k1gInSc6	vulkán
Kibo	Kibo	k6eAd1	Kibo
s	s	k7c7	s
výškou	výška	k1gFnSc7	výška
5895	[number]	k4	5895
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Vrchol	vrchol	k1gInSc1	vrchol
přitahuje	přitahovat	k5eAaImIp3nS	přitahovat
denně	denně	k6eAd1	denně
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
58	[number]	k4	58
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
