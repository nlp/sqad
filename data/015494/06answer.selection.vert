<s desamb="1">
Zkratka	zkratka	k1gFnSc1
tohoto	tento	k3xDgInSc2
titulu	titul	k1gInSc2
je	být	k5eAaImIp3nS
Ing.	ing.	kA
(	(	kIx(
<g/>
dříve	dříve	k6eAd2
se	se	k3xPyFc4
užívala	užívat	k5eAaImAgFnS
i	i	k9
zkratka	zkratka	k1gFnSc1
ing.	ing.	kA
nebo	nebo	k8xC
inž	inž	k?
<g/>
.	.	kIx.
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
oblasti	oblast	k1gFnSc6
architektury	architektura	k1gFnSc2
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
varianta	varianta	k1gFnSc1
<g/>
,	,	kIx,
resp.	resp.	kA
akademický	akademický	k2eAgInSc1d1
titul	titul	k1gInSc1
<g/>
,	,	kIx,
inženýr	inženýr	k1gMnSc1
architekt	architekt	k1gMnSc1
ve	v	k7c6
zkratce	zkratka	k1gFnSc6
Ing.	ing.	kA
arch	arch	k1gInSc1
<g/>
.	.	kIx.
(	(	kIx(
<g/>
z	z	k7c2
lat.	lat.	k?
ingerum	ingerum	k1gInSc1
architectus	architectus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
obě	dva	k4xCgFnPc1
zkratky	zkratka	k1gFnPc1
titulů	titul	k1gInPc2
se	se	k3xPyFc4
případně	případně	k6eAd1
umísťují	umísťovat	k5eAaImIp3nP
před	před	k7c4
jméno	jméno	k1gNnSc4
<g/>
.	.	kIx.
</s>