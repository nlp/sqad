<s>
Jaký	jaký	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
akademický	akademický	k2eAgInSc4d1
titul	titul	k1gInSc4
získává	získávat	k5eAaImIp3nS
absolvent	absolvent	k1gMnSc1
vysoké	vysoký	k2eAgFnSc2d1
školy	škola	k1gFnSc2
v	v	k7c6
magisterském	magisterský	k2eAgInSc6d1
studijním	studijní	k2eAgInSc6d1
programu	program	k1gInSc6
v	v	k7c6
oblasti	oblast	k1gFnSc6
technických	technický	k2eAgFnPc2d1
věd	věda	k1gFnPc2
a	a	k8xC
technologií	technologie	k1gFnPc2
<g/>
,	,	kIx,
ekonomie	ekonomie	k1gFnSc2
<g/>
,	,	kIx,
zemědělství	zemědělství	k1gNnSc2
<g/>
,	,	kIx,
lesnictví	lesnictví	k1gNnSc2
nebo	nebo	k8xC
vojenství	vojenství	k1gNnSc2
<g/>
?	?	kIx.
</s>