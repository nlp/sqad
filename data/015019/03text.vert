<s>
Swiss	Swiss	k1gInSc1
Indoors	Indoors	k1gInSc1
2014	#num#	k4
–	–	k?
mužská	mužský	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
</s>
<s>
Swiss	Swiss	k1gInSc1
Indoors	Indoors	k1gInSc1
2014	#num#	k4
Vítězové	vítěz	k1gMnPc1
<g/>
:	:	kIx,
</s>
<s>
Vasek	Vasek	k1gInSc4
Pospisil	Pospisil	k1gMnPc2
Nenad	Nenad	k1gInSc1
Zimonjić	Zimonjić	k1gFnPc2
Finalisté	finalista	k1gMnPc1
<g/>
:	:	kIx,
</s>
<s>
Marin	Marina	k1gFnPc2
Draganja	Draganj	k1gInSc2
Henri	Henr	k1gFnSc2
Kontinen	Kontinna	k1gFnPc2
Výsledek	výsledek	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
(	(	kIx(
<g/>
15	#num#	k4
<g/>
–	–	k?
<g/>
13	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
[	[	kIx(
<g/>
10	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Soutěže	soutěž	k1gFnSc2
</s>
<s>
mužská	mužský	k2eAgFnSc1d1
dvouhra	dvouhra	k1gFnSc1
•	•	k?
mužská	mužský	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
</s>
<s>
<	<	kIx(
2013	#num#	k4
</s>
<s>
2015	#num#	k4
>	>	kIx)
</s>
<s>
Do	do	k7c2
soutěže	soutěž	k1gFnSc2
mužské	mužský	k2eAgFnSc2d1
čtyřhry	čtyřhra	k1gFnSc2
na	na	k7c6
tenisovém	tenisový	k2eAgInSc6d1
turnaji	turnaj	k1gInSc6
Swiss	Swiss	k1gInSc1
Indoors	Indoors	k1gInSc1
2014	#num#	k4
nastoupilo	nastoupit	k5eAaPmAgNnS
šestnáct	šestnáct	k4xCc4
dvojic	dvojice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obhájcem	obhájce	k1gMnSc7
titulu	titul	k1gInSc2
byl	být	k5eAaImAgMnS
pár	pár	k4xCyI
Treat	Treat	k2eAgInSc4d1
Conrad	Conrad	k1gInSc4
Huey	Huea	k1gFnSc2
a	a	k8xC
Dominic	Dominice	k1gFnPc2
Inglot	Inglot	k1gMnSc1
<g/>
,	,	kIx,
jehož	jenž	k3xRgMnSc4,k3xOyRp3gMnSc4
členové	člen	k1gMnPc1
nestartovali	startovat	k5eNaBmAgMnP
společně	společně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Huey	Huey	k1gInPc7
se	se	k3xPyFc4
rozhodl	rozhodnout	k5eAaPmAgInS
zúčastnit	zúčastnit	k5eAaPmF
paralelně	paralelně	k6eAd1
probíhajícího	probíhající	k2eAgInSc2d1
turnaje	turnaj	k1gInSc2
Valencia	Valencia	k1gFnSc1
Open	Open	k1gNnSc1
500	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inglot	Inglot	k1gInSc1
spolu	spolu	k6eAd1
s	s	k7c7
Florinem	Florin	k1gInSc7
Mergeou	Mergea	k1gFnSc7
v	v	k7c6
semifinále	semifinále	k1gNnSc6
nestačil	stačit	k5eNaBmAgInS
na	na	k7c4
Pospisila	Pospisila	k1gFnSc4
se	s	k7c7
Zimonjićem	Zimonjić	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Soutěž	soutěž	k1gFnSc1
čtyřhry	čtyřhra	k1gFnSc2
vyhrála	vyhrát	k5eAaPmAgFnS
druhá	druhý	k4xOgFnSc1
nasazená	nasazený	k2eAgFnSc1d1
kanadsko-srbská	kanadsko-srbský	k2eAgFnSc1d1
dvojice	dvojice	k1gFnSc1
Vasek	Vasek	k1gMnSc1
Pospisil	Pospisil	k1gMnSc1
a	a	k8xC
Nenad	Nenad	k1gInSc1
Zimonjić	Zimonjić	k1gMnPc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gMnPc1
členové	člen	k1gMnPc1
ve	v	k7c6
finále	finále	k1gNnSc6
zdolali	zdolat	k5eAaPmAgMnP
chorvatsko-finský	chorvatsko-finský	k2eAgInSc4d1
pár	pár	k1gInSc4
Marin	Marina	k1gFnPc2
Draganja	Draganj	k1gInSc2
a	a	k8xC
Henri	Henr	k1gFnSc2
Kontinen	Kontinna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
rovnocenném	rovnocenný	k2eAgNnSc6d1
rozdělení	rozdělení	k1gNnSc6
prvních	první	k4xOgFnPc2
dvou	dva	k4xCgFnPc2
setů	set	k1gInPc2
7	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
a	a	k8xC
1	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
,	,	kIx,
o	o	k7c6
vítězích	vítěz	k1gMnPc6
rozhodl	rozhodnout	k5eAaPmAgInS
až	až	k6eAd1
supertiebreak	supertiebreak	k6eAd1
poměrem	poměr	k1gInSc7
míčů	míč	k1gInPc2
[	[	kIx(
<g/>
10	#num#	k4
<g/>
–	–	k?
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
sadu	sada	k1gFnSc4
rozhodl	rozhodnout	k5eAaPmAgInS
dlouhý	dlouhý	k2eAgInSc1d1
tiebreak	tiebreak	k1gInSc1
<g/>
,	,	kIx,
když	když	k8xS
v	v	k7c6
něm	on	k3xPp3gMnSc6
bylo	být	k5eAaImAgNnS
odehráno	odehrát	k5eAaPmNgNnS
28	#num#	k4
míčů	míč	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Nasazení	nasazení	k1gNnSc1
párů	pár	k1gInPc2
</s>
<s>
Ivan	Ivan	k1gMnSc1
Dodig	Dodig	k1gMnSc1
/	/	kIx~
Marcelo	Marcela	k1gFnSc5
Melo	Melo	k?
(	(	kIx(
<g/>
semifinále	semifinále	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Vasek	Vasek	k1gMnSc1
Pospisil	Pospisil	k1gMnSc1
/	/	kIx~
Nenad	Nenad	k1gInSc1
Zimonjić	Zimonjić	k1gMnSc2
(	(	kIx(
<g/>
vítězové	vítěz	k1gMnPc1
<g/>
)	)	kIx)
</s>
<s>
Rohan	Rohan	k1gMnSc1
Bopanna	Bopann	k1gMnSc2
/	/	kIx~
Daniel	Daniel	k1gMnSc1
Nestor	Nestor	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Łukasz	Łukasz	k1gMnSc1
Kubot	Kubot	k1gMnSc1
/	/	kIx~
Robert	Robert	k1gMnSc1
Lindstedt	Lindstedt	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Pavouk	pavouk	k1gMnSc1
</s>
<s>
Legenda	legenda	k1gFnSc1
</s>
<s>
Q	Q	kA
–	–	k?
kvalifikant	kvalifikant	k1gMnSc1
</s>
<s>
WC	WC	kA
–	–	k?
divoká	divoký	k2eAgFnSc1d1
karta	karta	k1gFnSc1
</s>
<s>
LL	LL	kA
–	–	k?
šťastný	šťastný	k2eAgMnSc1d1
poražený	poražený	k2eAgMnSc1d1
</s>
<s>
Alt	Alt	kA
–	–	k?
náhradník	náhradník	k1gMnSc1
</s>
<s>
SE	s	k7c7
–	–	k?
zvláštní	zvláštní	k2eAgFnSc1d1
výjimka	výjimka	k1gFnSc1
</s>
<s>
PR	pr	k0
–	–	k?
žebříčková	žebříčkový	k2eAgFnSc1d1
ochrana	ochrana	k1gFnSc1
</s>
<s>
w	w	k?
<g/>
/	/	kIx~
<g/>
o	o	k7c6
–	–	k?
bez	bez	k7c2
boje	boj	k1gInSc2
</s>
<s>
r	r	kA
–	–	k?
skreč	skreč	k1gInSc1
</s>
<s>
d	d	k?
–	–	k?
diskvalifikace	diskvalifikace	k1gFnSc1
</s>
<s>
První	první	k4xOgNnSc1
kolo	kolo	k1gNnSc1
</s>
<s>
Čtvrtfinále	čtvrtfinále	k1gNnSc1
</s>
<s>
Semifinále	semifinále	k1gNnSc1
</s>
<s>
Finále	finále	k1gNnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
I	i	k9
Dodig	Dodig	k1gMnSc1
M	M	kA
Melo	Melo	k?
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
M	M	kA
Fyrstenberg	Fyrstenberg	k1gInSc1
S	s	k7c7
Johnson	Johnson	k1gMnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
I	i	k9
Dodig	Dodig	k1gMnSc1
M	M	kA
Melo	Melo	k?
</s>
<s>
711	#num#	k4
</s>
<s>
64	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
WC	WC	kA
</s>
<s>
M	M	kA
Chiudinelli	Chiudinell	k1gMnPc1
M	M	kA
Lammer	Lammer	k1gMnSc1
</s>
<s>
4	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
A	a	k9
Begemann	Begemann	k1gMnSc1
J	J	kA
Knowle	Knowle	k1gFnSc1
</s>
<s>
69	#num#	k4
</s>
<s>
77	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
A	a	k9
Begemann	Begemann	k1gMnSc1
J	J	kA
Knowle	Knowle	k1gFnSc1
</s>
<s>
6	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1	#num#	k4
</s>
<s>
I	i	k9
Dodig	Dodig	k1gMnSc1
M	M	kA
Melo	Melo	k?
</s>
<s>
3	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
3	#num#	k4
</s>
<s>
R	R	kA
Bopanna	Bopanno	k1gNnSc2
D	D	kA
Nestor	Nestor	k1gMnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
64	#num#	k4
</s>
<s>
M	M	kA
Draganja	Draganja	k1gMnSc1
H	H	kA
Kontinen	Kontinen	k1gInSc1
</s>
<s>
6	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
M	M	kA
Draganja	Draganja	k1gMnSc1
H	H	kA
Kontinen	Kontinen	k1gInSc1
</s>
<s>
6	#num#	k4
</s>
<s>
77	#num#	k4
</s>
<s>
M	M	kA
Draganja	Draganja	k1gMnSc1
H	H	kA
Kontinen	Kontinen	k1gInSc1
</s>
<s>
6	#num#	k4
</s>
<s>
77	#num#	k4
</s>
<s>
Q	Q	kA
</s>
<s>
C	C	kA
Fleming	Fleming	k1gInSc1
J	J	kA
Marray	Marraa	k1gMnSc2
</s>
<s>
6	#num#	k4
</s>
<s>
65	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
B	B	kA
Becker	Becker	k1gMnSc1
D	D	kA
Thiem	Thiem	k1gInSc1
</s>
<s>
3	#num#	k4
</s>
<s>
64	#num#	k4
</s>
<s>
B	B	kA
Becker	Becker	k1gMnSc1
D	D	kA
Thiem	Thiem	k1gInSc1
</s>
<s>
3	#num#	k4
</s>
<s>
77	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
M	M	kA
Draganja	Draganja	k1gMnSc1
H	H	kA
Kontinen	Kontinen	k1gInSc1
</s>
<s>
613	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
D	D	kA
Inglot	Inglot	k1gMnSc1
F	F	kA
Mergea	Mergea	k1gMnSc1
</s>
<s>
77	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
V	v	k7c6
Pospisil	Pospisil	k1gFnSc6
N	N	kA
Zimonjić	Zimonjić	k1gMnSc2
</s>
<s>
715	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
S	s	k7c7
Lipsky	lipsky	k6eAd1
P	P	kA
Oswald	Oswalda	k1gFnPc2
</s>
<s>
63	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
D	D	kA
Inglot	Inglot	k1gMnSc1
F	F	kA
Mergea	Mergea	k1gMnSc1
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
S	s	k7c7
González	Gonzáleza	k1gFnPc2
L	L	kA
Rosol	Rosol	k1gMnSc1
</s>
<s>
6	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
S	s	k7c7
González	Gonzáleza	k1gFnPc2
L	L	kA
Rosol	Rosol	k1gMnSc1
</s>
<s>
3	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
Ł	Ł	kA
Kubot	Kubot	k1gMnSc1
R	R	kA
Lindstedt	Lindstedt	k1gMnSc1
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
D	D	kA
Inglot	Inglot	k1gMnSc1
F	F	kA
Mergea	Mergea	k1gMnSc1
</s>
<s>
66	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
J	J	kA
Murray	Murra	k2eAgFnPc1d1
J	J	kA
Peers	Peers	k1gInSc4
</s>
<s>
7	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2	#num#	k4
</s>
<s>
V	v	k7c6
Pospisil	Pospisil	k1gFnSc6
N	N	kA
Zimonjić	Zimonjić	k1gMnSc2
</s>
<s>
78	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
M	M	kA
Matkowski	Matkowski	k1gNnPc2
L	L	kA
Paes	Paes	k1gInSc4
</s>
<s>
5	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
M	M	kA
Matkowski	Matkowski	k1gNnPc2
L	L	kA
Paes	Paes	k1gInSc4
</s>
<s>
710	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
WC	WC	kA
</s>
<s>
S	s	k7c7
Ehrat	Ehrat	k2eAgInSc4d1
H	H	kA
Laaksonen	Laaksonen	k1gInSc4
</s>
<s>
4	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
V	v	k7c6
Pospisil	Pospisil	k1gFnSc6
N	N	kA
Zimonjić	Zimonjić	k1gMnSc2
</s>
<s>
68	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2	#num#	k4
</s>
<s>
V	v	k7c6
Pospisil	Pospisil	k1gFnSc6
N	N	kA
Zimonjić	Zimonjić	k1gMnSc2
</s>
<s>
6	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
2014	#num#	k4
Swiss	Swissa	k1gFnPc2
Indoors	Indoorsa	k1gFnPc2
–	–	k?
Doubles	Doubles	k1gMnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Mužská	mužský	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
Swiss	Swiss	k1gInSc4
Indoors	Indoors	k1gInSc1
2014	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ATP	atp	kA
Tour	Tour	k1gMnSc1
<g/>
,	,	kIx,
Inc	Inc	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Kvalifikace	kvalifikace	k1gFnSc1
mužské	mužský	k2eAgFnSc2d1
čtyřhry	čtyřhra	k1gFnSc2
Swiss	Swiss	k1gInSc1
Indoors	Indoors	k1gInSc1
2014	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ATP	atp	kA
Tour	Tour	k1gMnSc1
<g/>
,	,	kIx,
Inc	Inc	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Tenis	tenis	k1gInSc1
</s>
