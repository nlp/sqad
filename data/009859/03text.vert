<p>
<s>
Plzeň	Plzeň	k1gFnSc1	Plzeň
(	(	kIx(	(
<g/>
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
a	a	k8xC	a
dalších	další	k2eAgInPc6d1	další
jazycích	jazyk	k1gInPc6	jazyk
Pilsen	Pilsen	k2eAgMnSc1d1	Pilsen
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
statutární	statutární	k2eAgMnSc1d1	statutární
město	město	k1gNnSc4	město
na	na	k7c6	na
západě	západ	k1gInSc6	západ
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
metropole	metropol	k1gFnSc2	metropol
Plzeňského	plzeňský	k2eAgInSc2d1	plzeňský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
řek	řeka	k1gFnPc2	řeka
Mže	Mže	k1gFnSc2	Mže
<g/>
,	,	kIx,	,
Radbuza	Radbuza	k1gFnSc1	Radbuza
<g/>
,	,	kIx,	,
Úhlava	Úhlava	k1gFnSc1	Úhlava
a	a	k8xC	a
Úslava	Úslava	k1gFnSc1	Úslava
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
vzniká	vznikat	k5eAaImIp3nS	vznikat
řeka	řeka	k1gFnSc1	řeka
Berounka	Berounka	k1gFnSc1	Berounka
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
172	[number]	k4	172
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
čtvrtým	čtvrtý	k4xOgMnSc7	čtvrtý
největším	veliký	k2eAgMnSc7d3	veliký
městem	město	k1gNnSc7	město
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Plzeň	Plzeň	k1gFnSc1	Plzeň
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k9	jako
průmyslové	průmyslový	k2eAgNnSc1d1	průmyslové
a	a	k8xC	a
pivovarnické	pivovarnický	k2eAgNnSc1d1	pivovarnické
město	město	k1gNnSc1	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozsáhlých	rozsáhlý	k2eAgFnPc6d1	rozsáhlá
továrních	tovární	k2eAgFnPc6d1	tovární
halách	hala	k1gFnPc6	hala
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Škodovky	škodovka	k1gFnSc2	škodovka
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
dopravní	dopravní	k2eAgInPc1d1	dopravní
prostředky	prostředek	k1gInPc1	prostředek
a	a	k8xC	a
průmyslové	průmyslový	k2eAgInPc1d1	průmyslový
stroje	stroj	k1gInPc1	stroj
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
krajské	krajský	k2eAgFnSc2d1	krajská
metropole	metropol	k1gFnSc2	metropol
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
vaří	vařit	k5eAaImIp3nS	vařit
známá	známý	k2eAgNnPc4d1	známé
piva	pivo	k1gNnPc4	pivo
Prazdroj	prazdroj	k1gInSc1	prazdroj
a	a	k8xC	a
Gambrinus	gambrinus	k1gInSc1	gambrinus
<g/>
.	.	kIx.	.
</s>
<s>
Spodně	spodně	k6eAd1	spodně
kvašený	kvašený	k2eAgInSc1d1	kvašený
světlý	světlý	k2eAgInSc1d1	světlý
ležák	ležák	k1gInSc1	ležák
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
pivo	pivo	k1gNnSc1	pivo
plzeňského	plzeňský	k2eAgInSc2d1	plzeňský
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
celosvětově	celosvětově	k6eAd1	celosvětově
pak	pak	k6eAd1	pak
jako	jako	k8xS	jako
Pilsner	Pilsner	k1gInSc4	Pilsner
nebo	nebo	k8xC	nebo
Pils	Pils	k1gInSc4	Pils
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
německého	německý	k2eAgNnSc2d1	německé
jména	jméno	k1gNnSc2	jméno
města	město	k1gNnSc2	město
Pilsen	Pilsen	k1gInSc1	Pilsen
<g/>
.	.	kIx.	.
</s>
<s>
Plzeň	Plzeň	k1gFnSc1	Plzeň
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
podstatný	podstatný	k2eAgInSc4d1	podstatný
kulturní	kulturní	k2eAgInSc4d1	kulturní
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
početné	početný	k2eAgInPc4d1	početný
kulturní	kulturní	k2eAgInPc4d1	kulturní
domy	dům	k1gInPc4	dům
a	a	k8xC	a
divadla	divadlo	k1gNnPc4	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
sídlí	sídlet	k5eAaImIp3nS	sídlet
také	také	k9	také
biskupství	biskupství	k1gNnSc4	biskupství
plzeňské	plzeňský	k2eAgFnSc2d1	Plzeňská
diecéze	diecéze	k1gFnSc2	diecéze
<g/>
.	.	kIx.	.
</s>
<s>
Vysoké	vysoká	k1gFnPc1	vysoká
školství	školství	k1gNnSc2	školství
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
Západočeská	západočeský	k2eAgFnSc1d1	Západočeská
univerzita	univerzita	k1gFnSc1	univerzita
a	a	k8xC	a
Lékařská	lékařský	k2eAgFnSc1d1	lékařská
fakulta	fakulta	k1gFnSc1	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
byla	být	k5eAaImAgFnS	být
Plzeň	Plzeň	k1gFnSc1	Plzeň
(	(	kIx(	(
<g/>
společně	společně	k6eAd1	společně
s	s	k7c7	s
belgickým	belgický	k2eAgInSc7d1	belgický
Monsem	Mons	k1gInSc7	Mons
<g/>
)	)	kIx)	)
Evropským	evropský	k2eAgNnSc7d1	Evropské
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Současné	současný	k2eAgNnSc1d1	současné
historické	historický	k2eAgNnSc1d1	historické
jádro	jádro	k1gNnSc1	jádro
města	město	k1gNnSc2	město
má	mít	k5eAaImIp3nS	mít
pravidelnou	pravidelný	k2eAgFnSc4d1	pravidelná
síť	síť	k1gFnSc4	síť
ulic	ulice	k1gFnPc2	ulice
s	s	k7c7	s
obdélníkovým	obdélníkový	k2eAgNnSc7d1	obdélníkové
náměstím	náměstí	k1gNnSc7	náměstí
Republiky	republika	k1gFnSc2	republika
uprostřed	uprostřed	k7c2	uprostřed
(	(	kIx(	(
<g/>
139	[number]	k4	139
×	×	k?	×
193	[number]	k4	193
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
vévodí	vévodit	k5eAaImIp3nS	vévodit
katedrála	katedrál	k1gMnSc4	katedrál
sv.	sv.	kA	sv.
Bartoloměje	Bartoloměj	k1gMnSc4	Bartoloměj
s	s	k7c7	s
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
kostelní	kostelní	k2eAgFnSc7d1	kostelní
věží	věž	k1gFnSc7	věž
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Středověk	středověk	k1gInSc1	středověk
===	===	k?	===
</s>
</p>
<p>
<s>
První	první	k4xOgFnPc1	první
zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
Plzni	Plzeň	k1gFnSc6	Plzeň
(	(	kIx(	(
<g/>
dnešním	dnešní	k2eAgInSc6d1	dnešní
Starém	starý	k2eAgInSc6d1	starý
Plzenci	Plzenec	k1gInSc6	Plzenec
<g/>
)	)	kIx)	)
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
976	[number]	k4	976
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
u	u	k7c2	u
tohoto	tento	k3xDgNnSc2	tento
přemyslovského	přemyslovský	k2eAgNnSc2d1	přemyslovské
hradiště	hradiště	k1gNnSc2	hradiště
kníže	kníže	k1gMnSc1	kníže
Boleslav	Boleslav	k1gMnSc1	Boleslav
II	II	kA	II
<g/>
.	.	kIx.	.
porazil	porazit	k5eAaPmAgMnS	porazit
vojsko	vojsko	k1gNnSc4	vojsko
německého	německý	k2eAgMnSc2d1	německý
krále	král	k1gMnSc2	král
Oty	Ota	k1gMnSc2	Ota
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podhradí	podhradí	k1gNnSc6	podhradí
postupně	postupně	k6eAd1	postupně
vyrostlo	vyrůst	k5eAaPmAgNnS	vyrůst
městské	městský	k2eAgNnSc1d1	Městské
sídlo	sídlo	k1gNnSc1	sídlo
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
kostelů	kostel	k1gInPc2	kostel
a	a	k8xC	a
živým	živý	k2eAgInSc7d1	živý
obchodním	obchodní	k2eAgInSc7d1	obchodní
ruchem	ruch	k1gInSc7	ruch
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
nynější	nynější	k2eAgFnSc2d1	nynější
polohy	poloha	k1gFnSc2	poloha
(	(	kIx(	(
<g/>
v	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
sousedství	sousedství	k1gNnSc6	sousedství
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
vsi	ves	k1gFnSc2	ves
Malice	Malice	k1gFnSc2	Malice
<g/>
)	)	kIx)	)
přenesl	přenést	k5eAaPmAgInS	přenést
město	město	k1gNnSc4	město
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Nová	nový	k2eAgFnSc1d1	nová
Plzeň	Plzeň	k1gFnSc1	Plzeň
král	král	k1gMnSc1	král
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
roku	rok	k1gInSc2	rok
1295	[number]	k4	1295
jako	jako	k8xS	jako
důležitou	důležitý	k2eAgFnSc4d1	důležitá
obchodní	obchodní	k2eAgFnSc4d1	obchodní
křižovatku	křižovatka	k1gFnSc4	křižovatka
západních	západní	k2eAgFnPc2d1	západní
Čech	Čechy	k1gFnPc2	Čechy
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
do	do	k7c2	do
Bavorska	Bavorsko	k1gNnSc2	Bavorsko
<g/>
.	.	kIx.	.
</s>
<s>
Vyřešila	vyřešit	k5eAaPmAgFnS	vyřešit
se	se	k3xPyFc4	se
tím	ten	k3xDgInSc7	ten
dodávka	dodávka	k1gFnSc1	dodávka
vody	voda	k1gFnPc1	voda
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
místo	místo	k1gNnSc1	místo
leželo	ležet	k5eAaImAgNnS	ležet
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
řek	řeka	k1gFnPc2	řeka
Mže	Mže	k1gFnSc2	Mže
a	a	k8xC	a
Radbuzy	Radbuza	k1gFnSc2	Radbuza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
výhodné	výhodný	k2eAgFnSc3d1	výhodná
poloze	poloha	k1gFnSc3	poloha
na	na	k7c6	na
křižovatce	křižovatka	k1gFnSc6	křižovatka
obchodních	obchodní	k2eAgFnPc2d1	obchodní
cest	cesta	k1gFnPc2	cesta
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
z	z	k7c2	z
německých	německý	k2eAgFnPc2d1	německá
zemí	zem	k1gFnPc2	zem
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
Plzeň	Plzeň	k1gFnSc1	Plzeň
stala	stát	k5eAaPmAgFnS	stát
třetím	třetí	k4xOgNnSc7	třetí
největším	veliký	k2eAgNnSc7d3	veliký
a	a	k8xC	a
nejdůležitějším	důležitý	k2eAgNnSc7d3	nejdůležitější
městem	město	k1gNnSc7	město
po	po	k7c6	po
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
Kutné	kutný	k2eAgFnSc6d1	Kutná
Hoře	hora	k1gFnSc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
byly	být	k5eAaImAgInP	být
také	také	k6eAd1	také
vystavěny	vystavěn	k2eAgInPc1d1	vystavěn
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Bartoloměje	Bartoloměj	k1gMnSc2	Bartoloměj
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
náměstí	náměstí	k1gNnSc2	náměstí
<g/>
,	,	kIx,	,
františkánský	františkánský	k2eAgInSc1d1	františkánský
klášter	klášter	k1gInSc1	klášter
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
při	při	k7c6	při
hradbách	hradba	k1gFnPc6	hradba
<g/>
,	,	kIx,	,
nedochovaný	dochovaný	k2eNgInSc1d1	nedochovaný
dominikánský	dominikánský	k2eAgInSc1d1	dominikánský
klášter	klášter	k1gInSc1	klášter
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
<g/>
,	,	kIx,	,
špitální	špitální	k2eAgInSc1d1	špitální
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Maří	mařit	k5eAaImIp3nS	mařit
Magdalény	Magdaléna	k1gFnPc4	Magdaléna
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1320	[number]	k4	1320
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
samém	samý	k3xTgInSc6	samý
počátku	počátek	k1gInSc6	počátek
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
měli	mít	k5eAaImAgMnP	mít
husité	husita	k1gMnPc1	husita
díky	díky	k7c3	díky
radikálnímu	radikální	k2eAgMnSc3d1	radikální
knězi	kněz	k1gMnSc3	kněz
Václavu	Václav	k1gMnSc3	Václav
Korandovi	Korand	k1gMnSc3	Korand
ve	v	k7c6	v
městě	město	k1gNnSc6	město
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1420	[number]	k4	1420
ale	ale	k9	ale
musel	muset	k5eAaImAgMnS	muset
Koranda	Korando	k1gNnSc2	Korando
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Žižkou	Žižka	k1gMnSc7	Žižka
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
Tábora	Tábor	k1gInSc2	Tábor
a	a	k8xC	a
Plzeň	Plzeň	k1gFnSc1	Plzeň
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
baštou	bašta	k1gFnSc7	bašta
katolické	katolický	k2eAgFnSc2d1	katolická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Třikrát	třikrát	k6eAd1	třikrát
byla	být	k5eAaImAgFnS	být
neúspěšně	úspěšně	k6eNd1	úspěšně
obléhána	obléhán	k2eAgFnSc1d1	obléhána
<g/>
,	,	kIx,	,
nejprve	nejprve	k6eAd1	nejprve
Janem	Jan	k1gMnSc7	Jan
Žižkou	Žižka	k1gMnSc7	Žižka
a	a	k8xC	a
poté	poté	k6eAd1	poté
dvakrát	dvakrát	k6eAd1	dvakrát
Prokopem	Prokop	k1gMnSc7	Prokop
Holým	Holý	k1gMnSc7	Holý
<g/>
,	,	kIx,	,
a	a	k8xC	a
podílela	podílet	k5eAaImAgFnS	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
odporu	odpor	k1gInSc6	odpor
proti	proti	k7c3	proti
Jiřímu	Jiří	k1gMnSc3	Jiří
z	z	k7c2	z
Poděbrad	Poděbrady	k1gInPc2	Poděbrady
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1467	[number]	k4	1467
sídlila	sídlit	k5eAaImAgFnS	sídlit
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
pražská	pražský	k2eAgFnSc1d1	Pražská
kapitula	kapitula	k1gFnSc1	kapitula
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
letech	let	k1gInPc6	let
1431	[number]	k4	1431
<g/>
–	–	k?	–
<g/>
1561	[number]	k4	1561
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
orgánem	orgán	k1gInSc7	orgán
římsko-katolické	římskoatolický	k2eAgFnSc2d1	římsko-katolická
církve	církev	k1gFnSc2	církev
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Administrátor	administrátor	k1gMnSc1	administrátor
arcibiskupství	arcibiskupství	k1gNnSc2	arcibiskupství
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
Hilarius	Hilarius	k1gInSc4	Hilarius
Litoměřický	litoměřický	k2eAgInSc4d1	litoměřický
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
kromě	kromě	k7c2	kromě
spisů	spis	k1gInPc2	spis
polemizujících	polemizující	k2eAgInPc2d1	polemizující
s	s	k7c7	s
kališníky	kališník	k1gMnPc7	kališník
sepsal	sepsat	k5eAaPmAgMnS	sepsat
i	i	k9	i
latinskou	latinský	k2eAgFnSc4d1	Latinská
Historii	historie	k1gFnSc4	historie
města	město	k1gNnSc2	město
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Plzeň	Plzeň	k1gFnSc1	Plzeň
je	být	k5eAaImIp3nS	být
také	také	k9	také
kolébkou	kolébka	k1gFnSc7	kolébka
českého	český	k2eAgInSc2d1	český
knihtisku	knihtisk	k1gInSc2	knihtisk
<g/>
,	,	kIx,	,
nejstarším	starý	k2eAgInSc7d3	nejstarší
tiskem	tisk	k1gInSc7	tisk
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
Statuta	statuta	k1gNnPc4	statuta
Arnošta	Arnošt	k1gMnSc4	Arnošt
z	z	k7c2	z
Pardubic	Pardubice	k1gInPc2	Pardubice
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1476	[number]	k4	1476
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
dříve	dříve	k6eAd2	dříve
za	za	k7c2	za
něj	on	k3xPp3gNnSc2	on
byla	být	k5eAaImAgFnS	být
považována	považován	k2eAgFnSc1d1	považována
kniha	kniha	k1gFnSc1	kniha
Kronika	kronika	k1gFnSc1	kronika
trojánská	trojánský	k2eAgFnSc1d1	Trojánská
už	už	k6eAd1	už
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1468	[number]	k4	1468
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
tiskárna	tiskárna	k1gFnSc1	tiskárna
fungovala	fungovat	k5eAaImAgFnS	fungovat
na	na	k7c6	na
rohu	roh	k1gInSc6	roh
dnešních	dnešní	k2eAgFnPc2d1	dnešní
ulic	ulice	k1gFnPc2	ulice
Smetanova	Smetanův	k2eAgFnSc1d1	Smetanova
a	a	k8xC	a
Bezručova	Bezručův	k2eAgFnSc1d1	Bezručova
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1533	[number]	k4	1533
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Raný	raný	k2eAgInSc1d1	raný
novověk	novověk	k1gInSc1	novověk
===	===	k?	===
</s>
</p>
<p>
<s>
Počátkem	počátkem	k7c2	počátkem
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
výrazně	výrazně	k6eAd1	výrazně
poškozeno	poškodit	k5eAaPmNgNnS	poškodit
požáry	požár	k1gInPc4	požár
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
roku	rok	k1gInSc2	rok
1507	[number]	k4	1507
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
shořely	shořet	k5eAaPmAgInP	shořet
dvě	dva	k4xCgFnPc4	dva
třetiny	třetina	k1gFnPc4	třetina
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Loupeživý	loupeživý	k2eAgMnSc1d1	loupeživý
rytíř	rytíř	k1gMnSc1	rytíř
Bavůrek	Bavůrka	k1gFnPc2	Bavůrka
ze	z	k7c2	z
Švamberka	Švamberka	k1gFnSc1	Švamberka
byl	být	k5eAaImAgInS	být
popraven	popraven	k2eAgMnSc1d1	popraven
na	na	k7c6	na
plzeňském	plzeňský	k2eAgNnSc6d1	plzeňské
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vzbudilo	vzbudit	k5eAaPmAgNnS	vzbudit
řadu	řada	k1gFnSc4	řada
protestů	protest	k1gInPc2	protest
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
devět	devět	k4xCc4	devět
měsíců	měsíc	k1gInPc2	měsíc
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1599	[number]	k4	1599
a	a	k8xC	a
1600	[number]	k4	1600
(	(	kIx(	(
<g/>
od	od	k7c2	od
září	září	k1gNnSc2	září
až	až	k9	až
do	do	k7c2	do
června	červen	k1gInSc2	červen
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Plzeň	Plzeň	k1gFnSc4	Plzeň
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Svaté	svatý	k2eAgFnSc2d1	svatá
říše	říš	k1gFnSc2	říš
římské	římský	k2eAgFnSc2d1	římská
<g/>
,	,	kIx,	,
když	když	k8xS	když
sem	sem	k6eAd1	sem
císař	císař	k1gMnSc1	císař
Rudolf	Rudolf	k1gMnSc1	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
před	před	k7c7	před
morovou	morový	k2eAgFnSc7d1	morová
epidemií	epidemie	k1gFnSc7	epidemie
<g/>
.	.	kIx.	.
</s>
<s>
Sídlil	sídlit	k5eAaImAgInS	sídlit
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
Císařském	císařský	k2eAgInSc6d1	císařský
domě	dům	k1gInSc6	dům
vedle	vedle	k7c2	vedle
dnešní	dnešní	k2eAgFnSc2d1	dnešní
budovy	budova	k1gFnSc2	budova
plzeňské	plzeňský	k2eAgFnSc2d1	Plzeňská
radnice	radnice	k1gFnSc2	radnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1618	[number]	k4	1618
byla	být	k5eAaImAgFnS	být
Plzeň	Plzeň	k1gFnSc1	Plzeň
poprvé	poprvé	k6eAd1	poprvé
dobyta	dobyt	k2eAgFnSc1d1	dobyta
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
vojskem	vojsko	k1gNnSc7	vojsko
českých	český	k2eAgMnPc2d1	český
stavů	stav	k1gInPc2	stav
pod	pod	k7c7	pod
velením	velení	k1gNnSc7	velení
Petra	Petr	k1gMnSc2	Petr
Arnošta	Arnošt	k1gMnSc2	Arnošt
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Mansfelda	Mansfelda	k1gFnSc1	Mansfelda
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
hospodářství	hospodářství	k1gNnSc2	hospodářství
i	i	k8xC	i
kultura	kultura	k1gFnSc1	kultura
upadaly	upadat	k5eAaPmAgFnP	upadat
<g/>
.	.	kIx.	.
</s>
<s>
Švédská	švédský	k2eAgNnPc1d1	švédské
obléhání	obléhání	k1gNnPc1	obléhání
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1637	[number]	k4	1637
i	i	k8xC	i
1648	[number]	k4	1648
byla	být	k5eAaImAgFnS	být
neúspěšná	úspěšný	k2eNgFnSc1d1	neúspěšná
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1683	[number]	k4	1683
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
dekretem	dekret	k1gInSc7	dekret
Leopolda	Leopold	k1gMnSc2	Leopold
I.	I.	kA	I.
založen	založit	k5eAaPmNgInS	založit
35	[number]	k4	35
<g/>
.	.	kIx.	.
pěší	pěší	k2eAgInSc1d1	pěší
pluk	pluk	k1gInSc1	pluk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1695	[number]	k4	1695
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
popraven	popraven	k2eAgMnSc1d1	popraven
Jan	Jan	k1gMnSc1	Jan
Sladký	Sladký	k1gMnSc1	Sladký
Kozina	kozina	k1gFnSc1	kozina
<g/>
,	,	kIx,	,
vůdce	vůdce	k1gMnSc1	vůdce
chodského	chodský	k2eAgNnSc2d1	Chodské
povstání	povstání	k1gNnSc2	povstání
proti	proti	k7c3	proti
vrchnosti	vrchnost	k1gFnSc3	vrchnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1799	[number]	k4	1799
<g/>
,	,	kIx,	,
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
napoleonských	napoleonský	k2eAgFnPc2d1	napoleonská
válek	válka	k1gFnPc2	válka
<g/>
,	,	kIx,	,
prošla	projít	k5eAaPmAgFnS	projít
městem	město	k1gNnSc7	město
ruská	ruský	k2eAgNnPc4d1	ruské
vojska	vojsko	k1gNnPc4	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dnech	den	k1gInPc6	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
<g/>
–	–	k?	–
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
zde	zde	k6eAd1	zde
pobýval	pobývat	k5eAaImAgMnS	pobývat
maršál	maršál	k1gMnSc1	maršál
Alexandr	Alexandr	k1gMnSc1	Alexandr
Vasiljevič	Vasiljevič	k1gMnSc1	Vasiljevič
Suvorov	Suvorovo	k1gNnPc2	Suvorovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Průmyslový	průmyslový	k2eAgInSc1d1	průmyslový
rozvoj	rozvoj	k1gInSc1	rozvoj
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
nechal	nechat	k5eAaPmAgMnS	nechat
purkmistr	purkmistr	k1gMnSc1	purkmistr
Martin	Martin	k1gMnSc1	Martin
Kopecký	Kopecký	k1gMnSc1	Kopecký
(	(	kIx(	(
<g/>
1828	[number]	k4	1828
<g/>
–	–	k?	–
<g/>
1850	[number]	k4	1850
<g/>
)	)	kIx)	)
zbourat	zbourat	k5eAaPmF	zbourat
hradby	hradba	k1gFnPc4	hradba
a	a	k8xC	a
na	na	k7c6	na
jejich	jejich	k3xOp3gInSc6	jejich
místě	místo	k1gNnSc6	místo
vybudovat	vybudovat	k5eAaPmF	vybudovat
kolem	kolem	k7c2	kolem
starého	starý	k2eAgNnSc2d1	staré
města	město	k1gNnSc2	město
sady	sada	k1gFnSc2	sada
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
známé	známý	k2eAgInPc1d1	známý
jako	jako	k8xS	jako
Sady	sad	k1gInPc1	sad
Pětatřicátníků	pětatřicátník	k1gMnPc2	pětatřicátník
<g/>
,	,	kIx,	,
Smetanovy	Smetanův	k2eAgFnPc1d1	Smetanova
<g/>
,	,	kIx,	,
Kopeckého	Kopeckého	k2eAgFnPc1d1	Kopeckého
<g/>
,	,	kIx,	,
Šafaříkovy	Šafaříkův	k2eAgFnPc1d1	Šafaříkova
<g/>
,	,	kIx,	,
Křižíkovy	Křižíkův	k2eAgFnPc1d1	Křižíkova
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1832	[number]	k4	1832
pak	pak	k6eAd1	pak
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
první	první	k4xOgNnSc1	první
kamenné	kamenný	k2eAgNnSc1d1	kamenné
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1839	[number]	k4	1839
bylo	být	k5eAaImAgNnS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
Měšťanského	měšťanský	k2eAgInSc2d1	měšťanský
pivovaru	pivovar	k1gInSc2	pivovar
(	(	kIx(	(
<g/>
Bürgerliches	Bürgerliches	k1gMnSc1	Bürgerliches
Brauhaus	Brauhaus	k1gMnSc1	Brauhaus
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Prazdroj	prazdroj	k1gInSc1	prazdroj
<g/>
)	)	kIx)	)
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1842	[number]	k4	1842
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
bavorský	bavorský	k2eAgMnSc1d1	bavorský
sládek	sládek	k1gMnSc1	sládek
Josef	Josef	k1gMnSc1	Josef
Groll	Groll	k1gMnSc1	Groll
uvařil	uvařit	k5eAaPmAgMnS	uvařit
první	první	k4xOgFnSc4	první
várku	várka	k1gFnSc4	várka
piva	pivo	k1gNnSc2	pivo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1859	[number]	k4	1859
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
hrabě	hrabě	k1gMnSc1	hrabě
Valdštejn	Valdštejn	k1gMnSc1	Valdštejn
pobočku	pobočka	k1gFnSc4	pobočka
svých	svůj	k3xOyFgFnPc2	svůj
sléváren	slévárna	k1gFnPc2	slévárna
a	a	k8xC	a
strojíren	strojírna	k1gFnPc2	strojírna
<g/>
.	.	kIx.	.
</s>
<s>
Továrna	továrna	k1gFnSc1	továrna
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1869	[number]	k4	1869
odkoupil	odkoupit	k5eAaPmAgMnS	odkoupit
její	její	k3xOp3gMnSc1	její
vrchní	vrchní	k2eAgMnSc1d1	vrchní
inženýr	inženýr	k1gMnSc1	inženýr
Emil	Emil	k1gMnSc1	Emil
Škoda	Škoda	k1gMnSc1	Škoda
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
základem	základ	k1gInSc7	základ
budoucích	budoucí	k2eAgInPc2d1	budoucí
Škodových	Škodových	k2eAgInPc2d1	Škodových
závodů	závod	k1gInPc2	závod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1869	[number]	k4	1869
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
První	první	k4xOgInSc1	první
plzeňský	plzeňský	k2eAgInSc1d1	plzeňský
akciový	akciový	k2eAgInSc1d1	akciový
pivovar	pivovar	k1gInSc1	pivovar
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
Gambrinus	gambrinus	k1gInSc1	gambrinus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1882	[number]	k4	1882
zde	zde	k6eAd1	zde
získal	získat	k5eAaPmAgMnS	získat
František	František	k1gMnSc1	František
Křižík	Křižík	k1gMnSc1	Křižík
patent	patent	k1gInSc4	patent
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
vynález	vynález	k1gInSc4	vynález
obloukové	obloukový	k2eAgFnSc2d1	oblouková
lampy	lampa	k1gFnSc2	lampa
a	a	k8xC	a
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
realizoval	realizovat	k5eAaBmAgMnS	realizovat
zakázky	zakázka	k1gFnPc4	zakázka
na	na	k7c6	na
modernizaci	modernizace	k1gFnSc6	modernizace
veřejného	veřejný	k2eAgNnSc2d1	veřejné
osvětlení	osvětlení	k1gNnSc2	osvětlení
a	a	k8xC	a
na	na	k7c4	na
vybudování	vybudování	k1gNnSc4	vybudování
pouliční	pouliční	k2eAgFnSc2d1	pouliční
elektrické	elektrický	k2eAgFnSc2d1	elektrická
dráhy	dráha	k1gFnSc2	dráha
(	(	kIx(	(
<g/>
zprovozněna	zprovozněn	k2eAgFnSc1d1	zprovozněna
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Plzeň	Plzeň	k1gFnSc1	Plzeň
stala	stát	k5eAaPmAgFnS	stát
kolébkou	kolébka	k1gFnSc7	kolébka
české	český	k2eAgFnSc2d1	Česká
secese	secese	k1gFnSc2	secese
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Už	už	k6eAd1	už
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
Plzeň	Plzeň	k1gFnSc1	Plzeň
druhým	druhý	k4xOgNnSc7	druhý
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
třetím	třetí	k4xOgMnSc6	třetí
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
všech	všecek	k3xTgFnPc2	všecek
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
už	už	k6eAd1	už
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
čtvrtým	čtvrtá	k1gFnPc3	čtvrtá
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
předstižena	předstihnout	k5eAaPmNgFnS	předstihnout
Ostravou	Ostrava	k1gFnSc7	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
západočeská	západočeský	k2eAgFnSc1d1	Západočeská
metropole	metropole	k1gFnSc1	metropole
nicméně	nicméně	k8xC	nicméně
prosperovala	prosperovat	k5eAaImAgFnS	prosperovat
díky	díky	k7c3	díky
rozvoji	rozvoj	k1gInSc3	rozvoj
strojírenských	strojírenský	k2eAgInPc2d1	strojírenský
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
průmyslových	průmyslový	k2eAgInPc2d1	průmyslový
závodů	závod	k1gInPc2	závod
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
meziválečného	meziválečný	k2eAgNnSc2d1	meziválečné
období	období	k1gNnSc2	období
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozšiřování	rozšiřování	k1gNnSc3	rozšiřování
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
byly	být	k5eAaImAgFnP	být
připojeny	připojen	k2eAgFnPc1d1	připojena
obce	obec	k1gFnPc1	obec
Doubravka	Doubravka	k1gFnSc1	Doubravka
<g/>
,	,	kIx,	,
Doudlevce	Doudlevce	k1gMnPc4	Doudlevce
<g/>
,	,	kIx,	,
Lobzy	Lobza	k1gFnPc4	Lobza
a	a	k8xC	a
Skvrňany	Skvrňan	k1gMnPc4	Skvrňan
a	a	k8xC	a
vznikla	vzniknout	k5eAaPmAgNnP	vzniknout
tak	tak	k6eAd1	tak
"	"	kIx"	"
<g/>
Velká	velký	k2eAgFnSc1d1	velká
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc4	jejíž
počet	počet	k1gInSc4	počet
obyvatel	obyvatel	k1gMnSc1	obyvatel
překročil	překročit	k5eAaPmAgMnS	překročit
stotisícovou	stotisícový	k2eAgFnSc4d1	stotisícová
hranici	hranice	k1gFnSc4	hranice
(	(	kIx(	(
<g/>
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
vlastní	vlastní	k2eAgFnSc1d1	vlastní
Plzeň	Plzeň	k1gFnSc1	Plzeň
členila	členit	k5eAaImAgFnS	členit
na	na	k7c4	na
Vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
Město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
východní	východní	k2eAgFnSc4d1	východní
Pražské	pražský	k2eAgNnSc1d1	Pražské
Předměstí	předměstí	k1gNnSc1	předměstí
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgNnSc1d1	jižní
Říšské	říšský	k2eAgNnSc1d1	říšské
Předměstí	předměstí	k1gNnSc1	předměstí
a	a	k8xC	a
severní	severní	k2eAgNnSc1d1	severní
Saské	saský	k2eAgNnSc1d1	Saské
Předměstí	předměstí	k1gNnSc1	předměstí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Starostou	Starosta	k1gMnSc7	Starosta
města	město	k1gNnSc2	město
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
letech	let	k1gInPc6	let
1919	[number]	k4	1919
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
Luděk	Luděk	k1gMnSc1	Luděk
Pik	Pik	k1gMnSc1	Pik
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
byl	být	k5eAaImAgInS	být
vybudován	vybudován	k2eAgInSc1d1	vybudován
památník	památník	k1gInSc1	památník
Národního	národní	k2eAgNnSc2d1	národní
osvobození	osvobození	k1gNnSc2	osvobození
s	s	k7c7	s
dominující	dominující	k2eAgFnSc7d1	dominující
bronzovou	bronzový	k2eAgFnSc7d1	bronzová
sochou	socha	k1gFnSc7	socha
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
slavnostně	slavnostně	k6eAd1	slavnostně
odhalen	odhalit	k5eAaPmNgInS	odhalit
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
dík	dík	k7c3	dík
prezidentovi	prezident	k1gMnSc3	prezident
za	za	k7c4	za
založení	založení	k1gNnSc4	založení
a	a	k8xC	a
desetileté	desetiletý	k2eAgNnSc4d1	desetileté
fungování	fungování	k1gNnSc4	fungování
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1938	[number]	k4	1938
Československá	československý	k2eAgFnSc1d1	Československá
republika	republika	k1gFnSc1	republika
odstoupila	odstoupit	k5eAaPmAgFnS	odstoupit
svá	svůj	k3xOyFgNnPc4	svůj
pohraniční	pohraniční	k2eAgNnPc4d1	pohraniční
území	území	k1gNnSc6	území
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Většinově	většinově	k6eAd1	většinově
česká	český	k2eAgFnSc1d1	Česká
Plzeň	Plzeň	k1gFnSc1	Plzeň
sice	sice	k8xC	sice
zůstala	zůstat	k5eAaPmAgFnS	zůstat
na	na	k7c6	na
území	území	k1gNnSc6	území
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
však	však	k9	však
městem	město	k1gNnSc7	město
na	na	k7c6	na
samotné	samotný	k2eAgFnSc6d1	samotná
hranici	hranice	k1gFnSc6	hranice
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
stran	strana	k1gFnPc2	strana
obklopena	obklopit	k5eAaPmNgFnS	obklopit
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Obce	obec	k1gFnPc1	obec
severozápadním	severozápadní	k2eAgInSc7d1	severozápadní
a	a	k8xC	a
jihozápadním	jihozápadní	k2eAgInSc7d1	jihozápadní
směrem	směr	k1gInSc7	směr
již	již	k6eAd1	již
patřily	patřit	k5eAaImAgFnP	patřit
k	k	k7c3	k
Říši	říš	k1gFnSc3	říš
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
dnešní	dnešní	k2eAgFnSc2d1	dnešní
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Plzeň-Litice	Plzeň-Litice	k1gFnSc1	Plzeň-Litice
a	a	k8xC	a
Plzeň-Lhota	Plzeň-Lhota	k1gFnSc1	Plzeň-Lhota
<g/>
.	.	kIx.	.
</s>
<s>
Vládním	vládní	k2eAgNnSc7d1	vládní
nařízením	nařízení	k1gNnSc7	nařízení
byla	být	k5eAaImAgFnS	být
Plzeň	Plzeň	k1gFnSc1	Plzeň
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
květnu	květen	k1gInSc6	květen
1942	[number]	k4	1942
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
o	o	k7c4	o
obce	obec	k1gFnPc4	obec
Bolevec	Bolevec	k1gInSc4	Bolevec
<g/>
,	,	kIx,	,
Božkov	Božkov	k1gInSc1	Božkov
<g/>
,	,	kIx,	,
Bukovec	Bukovec	k1gInSc1	Bukovec
<g/>
,	,	kIx,	,
Černice	černice	k1gFnSc1	černice
<g/>
,	,	kIx,	,
Hradiště	Hradiště	k1gNnSc1	Hradiště
<g/>
,	,	kIx,	,
Koterov	Koterov	k1gInSc1	Koterov
<g/>
,	,	kIx,	,
Radobyčice	Radobyčice	k1gFnSc1	Radobyčice
<g/>
,	,	kIx,	,
Újezd	Újezd	k1gInSc1	Újezd
a	a	k8xC	a
o	o	k7c4	o
územní	územní	k2eAgInPc4d1	územní
zbytky	zbytek	k1gInPc4	zbytek
obce	obec	k1gFnSc2	obec
Litic	Litice	k1gFnPc2	Litice
(	(	kIx(	(
<g/>
obec	obec	k1gFnSc1	obec
začleněna	začleněn	k2eAgFnSc1d1	začleněna
do	do	k7c2	do
Říše	říš	k1gFnSc2	říš
<g/>
)	)	kIx)	)
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	s	k7c7	s
statutárním	statutární	k2eAgNnSc7d1	statutární
městem	město	k1gNnSc7	město
se	s	k7c7	s
130	[number]	k4	130
tisíci	tisíc	k4xCgInPc7	tisíc
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Škodovy	Škodův	k2eAgInPc1d1	Škodův
závody	závod	k1gInPc1	závod
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
významnou	významný	k2eAgFnSc7d1	významná
zbrojovkou	zbrojovka	k1gFnSc7	zbrojovka
zásobující	zásobující	k2eAgFnSc4d1	zásobující
německou	německý	k2eAgFnSc4d1	německá
armádu	armáda	k1gFnSc4	armáda
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
Plzeň	Plzeň	k1gFnSc4	Plzeň
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
unikala	unikat	k5eAaImAgFnS	unikat
bombardování	bombardování	k1gNnSc2	bombardování
<g/>
,	,	kIx,	,
k	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
velkému	velký	k2eAgInSc3d1	velký
náletu	nálet	k1gInSc3	nálet
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
20	[number]	k4	20
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1944	[number]	k4	1944
<g/>
,	,	kIx,	,
při	při	k7c6	při
něm	on	k3xPp3gInSc6	on
byl	být	k5eAaImAgInS	být
ovšem	ovšem	k9	ovšem
nejvíce	hodně	k6eAd3	hodně
zdemolován	zdemolovat	k5eAaPmNgInS	zdemolovat
pivovarský	pivovarský	k2eAgInSc1d1	pivovarský
komplex	komplex	k1gInSc1	komplex
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
Prazdroj	prazdroj	k1gInSc1	prazdroj
(	(	kIx(	(
<g/>
18	[number]	k4	18
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Škodovy	Škodův	k2eAgInPc1d1	Škodův
závody	závod	k1gInPc1	závod
byly	být	k5eAaImAgInP	být
bombardováním	bombardování	k1gNnSc7	bombardování
významně	významně	k6eAd1	významně
poškozeny	poškodit	k5eAaPmNgFnP	poškodit
teprve	teprve	k6eAd1	teprve
na	na	k7c6	na
samém	samý	k3xTgInSc6	samý
sklonku	sklonek	k1gInSc6	sklonek
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
náletem	nálet	k1gInSc7	nálet
25	[number]	k4	25
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
bylo	být	k5eAaImAgNnS	být
za	za	k7c4	za
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
války	válka	k1gFnSc2	válka
náletů	nálet	k1gInPc2	nálet
jedenáct	jedenáct	k4xCc1	jedenáct
<g/>
,	,	kIx,	,
vyžádaly	vyžádat	k5eAaPmAgInP	vyžádat
si	se	k3xPyFc3	se
926	[number]	k4	926
obětí	oběť	k1gFnPc2	oběť
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Spojenci	spojenec	k1gMnPc1	spojenec
před	před	k7c4	před
nálety	nálet	k1gInPc4	nálet
varovali	varovat	k5eAaImAgMnP	varovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
stanice	stanice	k1gFnSc2	stanice
BBC	BBC	kA	BBC
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
minimalizovali	minimalizovat	k5eAaBmAgMnP	minimalizovat
civilní	civilní	k2eAgFnPc4d1	civilní
oběti	oběť	k1gFnPc4	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Útoky	útok	k1gInPc1	útok
způsobily	způsobit	k5eAaPmAgInP	způsobit
citelné	citelný	k2eAgInPc4d1	citelný
zásahy	zásah	k1gInPc4	zásah
nacistům	nacista	k1gMnPc3	nacista
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
ale	ale	k9	ale
i	i	k9	i
na	na	k7c4	na
omyly	omyl	k1gInPc4	omyl
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
civilní	civilní	k2eAgFnPc1d1	civilní
oběti	oběť	k1gFnPc1	oběť
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
vděčným	vděčný	k2eAgNnSc7d1	vděčné
soustem	sousto	k1gNnSc7	sousto
propagandy	propaganda	k1gFnSc2	propaganda
–	–	k?	–
té	ten	k3xDgFnSc3	ten
nacistické	nacistický	k2eAgFnSc3d1	nacistická
<g/>
,	,	kIx,	,
i	i	k9	i
pozdější	pozdní	k2eAgFnSc2d2	pozdější
komunistické	komunistický	k2eAgFnSc2d1	komunistická
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Poválečná	poválečný	k2eAgFnSc1d1	poválečná
éra	éra	k1gFnSc1	éra
===	===	k?	===
</s>
</p>
<p>
<s>
Plzeň	Plzeň	k1gFnSc1	Plzeň
a	a	k8xC	a
západní	západní	k2eAgFnPc1d1	západní
Čechy	Čechy	k1gFnPc1	Čechy
byly	být	k5eAaImAgFnP	být
osvobozeny	osvobodit	k5eAaPmNgFnP	osvobodit
americkými	americký	k2eAgFnPc7d1	americká
jednotkami	jednotka	k1gFnPc7	jednotka
vedenými	vedený	k2eAgFnPc7d1	vedená
generálem	generál	k1gMnSc7	generál
Pattonem	Patton	k1gInSc7	Patton
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
americko-sovětské	americkoovětský	k2eAgFnSc2d1	americko-sovětská
dohody	dohoda	k1gFnSc2	dohoda
Patton	Patton	k1gInSc1	Patton
už	už	k6eAd1	už
nesměl	smět	k5eNaImAgInS	smět
pokračovat	pokračovat	k5eAaImF	pokračovat
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Slavný	slavný	k2eAgInSc1d1	slavný
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc1	jeho
výrok	výrok	k1gInSc1	výrok
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
už	už	k6eAd1	už
<g/>
,	,	kIx,	,
tak	tak	k9	tak
to	ten	k3xDgNnSc4	ten
Američané	Američan	k1gMnPc1	Američan
"	"	kIx"	"
<g/>
mají	mít	k5eAaImIp3nP	mít
vzít	vzít	k5eAaPmF	vzít
až	až	k9	až
do	do	k7c2	do
Moskvy	Moskva	k1gFnSc2	Moskva
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
na	na	k7c4	na
osvobození	osvobození	k1gNnSc4	osvobození
Plzně	Plzeň	k1gFnSc2	Plzeň
upomíná	upomínat	k5eAaImIp3nS	upomínat
Patton	Patton	k1gInSc1	Patton
Memorial	Memorial	k1gInSc1	Memorial
Pilsen	Pilsen	k1gInSc1	Pilsen
–	–	k?	–
Památník	památník	k1gInSc4	památník
americké	americký	k2eAgFnSc2d1	americká
armády	armáda	k1gFnSc2	armáda
1945	[number]	k4	1945
provedený	provedený	k2eAgMnSc1d1	provedený
jako	jako	k8xC	jako
stálé	stálý	k2eAgNnSc1d1	stálé
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
únoru	únor	k1gInSc6	únor
1948	[number]	k4	1948
při	při	k7c6	při
oslavách	oslava	k1gFnPc6	oslava
výročí	výročí	k1gNnSc2	výročí
konce	konec	k1gInSc2	konec
války	válka	k1gFnSc2	válka
nepadalo	padat	k5eNaImAgNnS	padat
o	o	k7c6	o
Američanech	Američan	k1gMnPc6	Američan
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
ani	ani	k8xC	ani
slovo	slovo	k1gNnSc4	slovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1953	[number]	k4	1953
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
k	k	k7c3	k
prvním	první	k4xOgMnPc3	první
protikomunistickým	protikomunistický	k2eAgMnPc3d1	protikomunistický
masovým	masový	k2eAgMnPc3d1	masový
nepokojům	nepokoj	k1gInPc3	nepokoj
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
tehdy	tehdy	k6eAd1	tehdy
probíhající	probíhající	k2eAgFnSc7d1	probíhající
měnovou	měnový	k2eAgFnSc7d1	měnová
reformou	reforma	k1gFnSc7	reforma
se	s	k7c7	s
střelbou	střelba	k1gFnSc7	střelba
do	do	k7c2	do
demonstrujících	demonstrující	k2eAgMnPc2d1	demonstrující
<g/>
.	.	kIx.	.
</s>
<s>
Odvetou	odveta	k1gFnSc7	odveta
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
bylo	být	k5eAaImAgNnS	být
kromě	kromě	k7c2	kromě
represí	represe	k1gFnPc2	represe
proti	proti	k7c3	proti
demonstrantům	demonstrant	k1gMnPc3	demonstrant
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
dělníkům	dělník	k1gMnPc3	dělník
Škodovky	škodovka	k1gFnSc2	škodovka
<g/>
,	,	kIx,	,
také	také	k9	také
zbourání	zbourání	k1gNnSc4	zbourání
Masarykova	Masarykův	k2eAgInSc2d1	Masarykův
pomníku	pomník	k1gInSc2	pomník
<g/>
,	,	kIx,	,
symbolu	symbol	k1gInSc2	symbol
nežádoucí	žádoucí	k2eNgFnPc1d1	nežádoucí
demokracie	demokracie	k1gFnPc1	demokracie
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
první	první	k4xOgInSc4	první
lidový	lidový	k2eAgInSc4d1	lidový
protest	protest	k1gInSc4	protest
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
sovětském	sovětský	k2eAgInSc6d1	sovětský
bloku	blok	k1gInSc6	blok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
s	s	k7c7	s
masivním	masivní	k2eAgInSc7d1	masivní
rozvojem	rozvoj	k1gInSc7	rozvoj
bytové	bytový	k2eAgFnSc2d1	bytová
výstavby	výstavba	k1gFnSc2	výstavba
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
budování	budování	k1gNnSc4	budování
sídlišť	sídliště	k1gNnPc2	sídliště
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
areál	areál	k1gInSc1	areál
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
na	na	k7c6	na
Slovanech	Slovan	k1gInPc6	Slovan
<g/>
,	,	kIx,	,
následovala	následovat	k5eAaImAgFnS	následovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1961	[number]	k4	1961
Doubravka	Doubravka	k1gFnSc1	Doubravka
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
pak	pak	k6eAd1	pak
Bory	bor	k1gInPc4	bor
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1969	[number]	k4	1969
se	se	k3xPyFc4	se
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
T.	T.	kA	T.
G.	G.	kA	G.
Masaryka	Masaryk	k1gMnSc2	Masaryk
upálil	upálit	k5eAaPmAgMnS	upálit
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
sovětské	sovětský	k2eAgFnSc3d1	sovětská
okupaci	okupace	k1gFnSc3	okupace
mladý	mladý	k2eAgMnSc1d1	mladý
pivovarský	pivovarský	k2eAgMnSc1d1	pivovarský
dělník	dělník	k1gMnSc1	dělník
Josef	Josef	k1gMnSc1	Josef
Hlavatý	Hlavatý	k1gMnSc1	Hlavatý
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
se	se	k3xPyFc4	se
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
přesunula	přesunout	k5eAaPmAgFnS	přesunout
na	na	k7c4	na
sever	sever	k1gInSc4	sever
<g/>
,	,	kIx,	,
budovala	budovat	k5eAaImAgNnP	budovat
se	se	k3xPyFc4	se
rozsáhlá	rozsáhlý	k2eAgNnPc1d1	rozsáhlé
sídliště	sídliště	k1gNnPc1	sídliště
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Bolevce	Bolevce	k1gMnSc2	Bolevce
a	a	k8xC	a
Lochotína	Lochotín	k1gMnSc2	Lochotín
<g/>
,	,	kIx,	,
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
sametovou	sametový	k2eAgFnSc7d1	sametová
revolucí	revoluce	k1gFnSc7	revoluce
se	se	k3xPyFc4	se
výstavba	výstavba	k1gFnSc1	výstavba
přesouvala	přesouvat	k5eAaImAgFnS	přesouvat
postupně	postupně	k6eAd1	postupně
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
západ	západ	k1gInSc4	západ
k	k	k7c3	k
oblasti	oblast	k1gFnSc3	oblast
Vinice	vinice	k1gFnSc2	vinice
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomuto	tento	k3xDgInSc3	tento
rychlému	rychlý	k2eAgInSc3d1	rychlý
rozvoji	rozvoj	k1gInSc3	rozvoj
překročilo	překročit	k5eAaPmAgNnS	překročit
město	město	k1gNnSc1	město
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
hranici	hranice	k1gFnSc6	hranice
150	[number]	k4	150
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgFnP	být
připojeny	připojit	k5eAaPmNgFnP	připojit
i	i	k9	i
obce	obec	k1gFnPc1	obec
Černice	černice	k1gFnPc1	černice
<g/>
,	,	kIx,	,
Radobyčice	Radobyčice	k1gFnPc1	Radobyčice
<g/>
,	,	kIx,	,
Koterov	Koterov	k1gInSc1	Koterov
<g/>
,	,	kIx,	,
Červený	červený	k2eAgInSc1d1	červený
Hrádek	hrádek	k1gInSc1	hrádek
<g/>
,	,	kIx,	,
Křimice	Křimika	k1gFnSc3	Křimika
a	a	k8xC	a
Radčice	Radčice	k1gFnSc1	Radčice
a	a	k8xC	a
Plzeň	Plzeň	k1gFnSc1	Plzeň
získala	získat	k5eAaPmAgFnS	získat
hranice	hranice	k1gFnPc4	hranice
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgFnPc4	jaký
měla	mít	k5eAaImAgFnS	mít
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
Plzni	Plzeň	k1gFnSc3	Plzeň
postavena	postaven	k2eAgFnSc1d1	postavena
i	i	k9	i
dálnice	dálnice	k1gFnSc1	dálnice
D	D	kA	D
<g/>
5	[number]	k4	5
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
spojila	spojit	k5eAaPmAgFnS	spojit
Prahu	Praha	k1gFnSc4	Praha
s	s	k7c7	s
bývalým	bývalý	k2eAgNnSc7d1	bývalé
Západním	západní	k2eAgNnSc7d1	západní
Německem	Německo	k1gNnSc7	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Dokončení	dokončení	k1gNnSc1	dokončení
této	tento	k3xDgFnSc2	tento
důležité	důležitý	k2eAgFnSc2d1	důležitá
dopravní	dopravní	k2eAgFnSc2d1	dopravní
tepny	tepna	k1gFnSc2	tepna
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
částečně	částečně	k6eAd1	částečně
vyřešila	vyřešit	k5eAaPmAgFnS	vyřešit
tehdy	tehdy	k6eAd1	tehdy
katastrofální	katastrofální	k2eAgFnSc4d1	katastrofální
dopravní	dopravní	k2eAgFnSc4d1	dopravní
situaci	situace	k1gFnSc4	situace
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
však	však	k9	však
kvůli	kvůli	k7c3	kvůli
komplikacím	komplikace	k1gFnPc3	komplikace
protáhlo	protáhnout	k5eAaPmAgNnS	protáhnout
až	až	k9	až
do	do	k7c2	do
prvního	první	k4xOgNnSc2	první
desetiletí	desetiletí	k1gNnSc2	desetiletí
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Evropské	evropský	k2eAgNnSc1d1	Evropské
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
kultury	kultura	k1gFnSc2	kultura
2015	[number]	k4	2015
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2010	[number]	k4	2010
vyhlásili	vyhlásit	k5eAaPmAgMnP	vyhlásit
zástupci	zástupce	k1gMnPc1	zástupce
mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
poroty	porota	k1gFnSc2	porota
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
město	město	k1gNnSc1	město
Plzeň	Plzeň	k1gFnSc4	Plzeň
za	za	k7c4	za
vítěze	vítěz	k1gMnSc4	vítěz
soutěže	soutěž	k1gFnSc2	soutěž
o	o	k7c4	o
titul	titul	k1gInSc4	titul
Evropské	evropský	k2eAgNnSc4d1	Evropské
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
kultury	kultura	k1gFnSc2	kultura
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
dostala	dostat	k5eAaPmAgFnS	dostat
Plzeň	Plzeň	k1gFnSc1	Plzeň
přednost	přednost	k1gFnSc1	přednost
před	před	k7c7	před
Ostravou	Ostrava	k1gFnSc7	Ostrava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
projektu	projekt	k1gInSc2	projekt
bylo	být	k5eAaImAgNnS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
Nové	Nové	k2eAgNnSc1d1	Nové
divadlo	divadlo	k1gNnSc1	divadlo
a	a	k8xC	a
ve	v	k7c6	v
Štruncových	Štruncových	k2eAgInPc6d1	Štruncových
sadech	sad	k1gInPc6	sad
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
fotbalového	fotbalový	k2eAgInSc2d1	fotbalový
stadiónu	stadión	k1gInSc2	stadión
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
sportovně-relaxační	sportovněelaxační	k2eAgNnSc1d1	sportovně-relaxační
centrum	centrum	k1gNnSc1	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Světovaru	Světovar	k1gInSc6	Světovar
měl	mít	k5eAaImAgInS	mít
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
bývalého	bývalý	k2eAgInSc2d1	bývalý
pivovaru	pivovar	k1gInSc2	pivovar
vzniknout	vzniknout	k5eAaPmF	vzniknout
komplex	komplex	k1gInSc4	komplex
budov	budova	k1gFnPc2	budova
s	s	k7c7	s
ateliéry	ateliér	k1gInPc7	ateliér
a	a	k8xC	a
byty	byt	k1gInPc7	byt
pro	pro	k7c4	pro
umělce	umělec	k1gMnPc4	umělec
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
projekt	projekt	k1gInSc1	projekt
byl	být	k5eAaImAgInS	být
ale	ale	k8xC	ale
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
místo	místo	k1gNnSc1	místo
zaujalo	zaujmout	k5eAaPmAgNnS	zaujmout
bývalé	bývalý	k2eAgNnSc1d1	bývalé
depo	depo	k1gNnSc1	depo
dopravních	dopravní	k2eAgInPc2d1	dopravní
podniků	podnik	k1gInPc2	podnik
<g/>
,	,	kIx,	,
nově	nově	k6eAd1	nově
DEPO	depo	k1gNnSc1	depo
<g/>
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
tzv.	tzv.	kA	tzv.
kreativní	kreativní	k2eAgFnSc1d1	kreativní
zóna	zóna	k1gFnSc1	zóna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
DEPO2015	DEPO2015	k1gFnSc6	DEPO2015
se	se	k3xPyFc4	se
pořádají	pořádat	k5eAaImIp3nP	pořádat
koncerty	koncert	k1gInPc1	koncert
<g/>
,	,	kIx,	,
výstavy	výstava	k1gFnPc1	výstava
<g/>
,	,	kIx,	,
přednášky	přednáška	k1gFnPc1	přednáška
<g/>
,	,	kIx,	,
konference	konference	k1gFnPc1	konference
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
součástí	součást	k1gFnSc7	součást
je	on	k3xPp3gNnPc4	on
otevřená	otevřený	k2eAgNnPc4d1	otevřené
díla	dílo	k1gNnPc4	dílo
Makerspace	Makerspace	k1gFnSc1	Makerspace
DEPO	depo	k1gNnSc1	depo
<g/>
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
otevřená	otevřený	k2eAgFnSc1d1	otevřená
kancelář	kancelář	k1gFnSc1	kancelář
Coworking	Coworking	k1gInSc1	Coworking
<g/>
,	,	kIx,	,
vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
program	program	k1gInSc1	program
pro	pro	k7c4	pro
kreativní	kreativní	k2eAgInPc4d1	kreativní
průmysly	průmysl	k1gInPc4	průmysl
<g/>
,	,	kIx,	,
program	program	k1gInSc1	program
umělců	umělec	k1gMnPc2	umělec
na	na	k7c6	na
rezidenci	rezidence	k1gFnSc6	rezidence
Open	Openo	k1gNnPc2	Openo
A.i.	A.i.	k1gMnPc2	A.i.
<g/>
R.	R.	kA	R.
či	či	k8xC	či
komunitní	komunitní	k2eAgFnSc1d1	komunitní
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
také	také	k9	také
festival	festival	k1gInSc1	festival
Rock	rock	k1gInSc1	rock
for	forum	k1gNnPc2	forum
People	People	k1gMnSc5	People
Europe	Europ	k1gMnSc5	Europ
<g/>
,	,	kIx,	,
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
později	pozdě	k6eAd2	pozdě
tu	tu	k6eAd1	tu
končilo	končit	k5eAaImAgNnS	končit
divadelní	divadelní	k2eAgNnSc1d1	divadelní
představení	představení	k1gNnSc1	představení
Obří	obří	k2eAgFnSc2d1	obří
loutky	loutka	k1gFnSc2	loutka
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
podle	podle	k7c2	podle
organizátorů	organizátor	k1gMnPc2	organizátor
vidělo	vidět	k5eAaImAgNnS	vidět
100	[number]	k4	100
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Koncem	koncem	k7c2	koncem
dubna	duben	k1gInSc2	duben
2014	[number]	k4	2014
získal	získat	k5eAaPmAgInS	získat
projekt	projekt	k1gInSc1	projekt
Cenu	cena	k1gFnSc4	cena
Meliny	Melin	k2eAgFnSc2d1	Melina
Mercouri	Mercour	k1gFnSc2	Mercour
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
monitorovací	monitorovací	k2eAgInSc1d1	monitorovací
a	a	k8xC	a
poradní	poradní	k2eAgInSc1d1	poradní
výbor	výbor	k1gInSc1	výbor
pro	pro	k7c4	pro
Evropská	evropský	k2eAgNnPc4d1	Evropské
hlavní	hlavní	k2eAgNnPc4d1	hlavní
města	město	k1gNnPc4	město
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Podmínkou	podmínka	k1gFnSc7	podmínka
bylo	být	k5eAaImAgNnS	být
naplnění	naplnění	k1gNnSc1	naplnění
předem	předem	k6eAd1	předem
daných	daný	k2eAgNnPc2d1	dané
kritérií	kritérion	k1gNnPc2	kritérion
připravenosti	připravenost	k1gFnSc2	připravenost
<g/>
.	.	kIx.	.
</s>
<s>
Plzeň	Plzeň	k1gFnSc1	Plzeň
2015	[number]	k4	2015
tak	tak	k8xS	tak
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
projekt	projekt	k1gInSc4	projekt
získala	získat	k5eAaPmAgFnS	získat
dotaci	dotace	k1gFnSc4	dotace
1,5	[number]	k4	1,5
milionu	milion	k4xCgInSc2	milion
eur	euro	k1gNnPc2	euro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
organizace	organizace	k1gFnSc2	organizace
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
–	–	k?	–
<g/>
Turismus	turismus	k1gInSc4	turismus
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
město	město	k1gNnSc1	město
za	za	k7c4	za
"	"	kIx"	"
<g/>
kulturní	kulturní	k2eAgInSc4d1	kulturní
<g/>
"	"	kIx"	"
rok	rok	k1gInSc4	rok
2015	[number]	k4	2015
necelých	celý	k2eNgInPc2d1	necelý
3,4	[number]	k4	3,4
milionu	milion	k4xCgInSc2	milion
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2,8	[number]	k4	2,8
milionu	milion	k4xCgInSc2	milion
bylo	být	k5eAaImAgNnS	být
jednodenních	jednodenní	k2eAgNnPc2d1	jednodenní
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
540	[number]	k4	540
tisíc	tisíc	k4xCgInSc4	tisíc
turistů	turist	k1gMnPc2	turist
zde	zde	k6eAd1	zde
strávilo	strávit	k5eAaPmAgNnS	strávit
minimálně	minimálně	k6eAd1	minimálně
jednu	jeden	k4xCgFnSc4	jeden
noc	noc	k1gFnSc4	noc
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtinu	čtvrtina	k1gFnSc4	čtvrtina
jednodenních	jednodenní	k2eAgMnPc2d1	jednodenní
návštěvníků	návštěvník	k1gMnPc2	návštěvník
tvořili	tvořit	k5eAaImAgMnP	tvořit
hosté	host	k1gMnPc1	host
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
celá	celý	k2eAgFnSc1d1	celá
polovina	polovina	k1gFnSc1	polovina
přijela	přijet	k5eAaPmAgFnS	přijet
z	z	k7c2	z
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Čísla	číslo	k1gNnPc1	číslo
ukázala	ukázat	k5eAaPmAgNnP	ukázat
analýza	analýza	k1gFnSc1	analýza
signálních	signální	k2eAgNnPc2d1	signální
dat	datum	k1gNnPc2	datum
mobilních	mobilní	k2eAgInPc2d1	mobilní
operátorů	operátor	k1gInPc2	operátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Plzně	Plzeň	k1gFnSc2	Plzeň
byl	být	k5eAaImAgInS	být
díky	díky	k7c3	díky
rostoucímu	rostoucí	k2eAgInSc3d1	rostoucí
průmyslu	průmysl	k1gInSc3	průmysl
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
poměrně	poměrně	k6eAd1	poměrně
výrazný	výrazný	k2eAgInSc1d1	výrazný
<g/>
.	.	kIx.	.
</s>
<s>
Největšího	veliký	k2eAgInSc2d3	veliký
počtu	počet	k1gInSc2	počet
<g/>
,	,	kIx,	,
175	[number]	k4	175
229	[number]	k4	229
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
ale	ale	k8xC	ale
vlivem	vliv	k1gInSc7	vliv
suburbanizace	suburbanizace	k1gFnSc2	suburbanizace
jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
stále	stále	k6eAd1	stále
pomalu	pomalu	k6eAd1	pomalu
klesal	klesat	k5eAaImAgInS	klesat
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
obratu	obrat	k1gInSc3	obrat
a	a	k8xC	a
např.	např.	kA	např.
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2016	[number]	k4	2016
měla	mít	k5eAaImAgFnS	mít
Plzeň	Plzeň	k1gFnSc1	Plzeň
169	[number]	k4	169
858	[number]	k4	858
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
města	město	k1gNnSc2	město
také	také	k9	také
dojíždí	dojíždět	k5eAaImIp3nS	dojíždět
necelých	celý	k2eNgInPc2d1	necelý
40	[number]	k4	40
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
za	za	k7c2	za
prací	práce	k1gFnPc2	práce
i	i	k9	i
za	za	k7c7	za
studiem	studio	k1gNnSc7	studio
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
vlastní	vlastní	k2eAgFnSc6d1	vlastní
metropolitní	metropolitní	k2eAgFnSc6d1	metropolitní
oblasti	oblast	k1gFnSc6	oblast
žije	žít	k5eAaImIp3nS	žít
přes	přes	k7c4	přes
300	[number]	k4	300
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Složení	složení	k1gNnSc1	složení
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnPc2	sčítání
1921	[number]	k4	1921
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c4	v
3	[number]	k4	3
284	[number]	k4	284
domech	dům	k1gInPc6	dům
88	[number]	k4	88
416	[number]	k4	416
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
bylo	být	k5eAaImAgNnS	být
43	[number]	k4	43
057	[number]	k4	057
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
79	[number]	k4	79
166	[number]	k4	166
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
československé	československý	k2eAgFnSc3d1	Československá
národnosti	národnost	k1gFnSc3	národnost
<g/>
,	,	kIx,	,
6	[number]	k4	6
757	[number]	k4	757
k	k	k7c3	k
německé	německý	k2eAgFnSc3d1	německá
a	a	k8xC	a
701	[number]	k4	701
k	k	k7c3	k
židovské	židovská	k1gFnSc3	židovská
<g/>
.	.	kIx.	.
</s>
<s>
Žilo	žít	k5eAaImAgNnS	žít
zde	zde	k6eAd1	zde
52	[number]	k4	52
514	[number]	k4	514
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
5	[number]	k4	5
763	[number]	k4	763
evangelíků	evangelík	k1gMnPc2	evangelík
<g/>
,	,	kIx,	,
4	[number]	k4	4
427	[number]	k4	427
příslušníků	příslušník	k1gMnPc2	příslušník
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
a	a	k8xC	a
3	[number]	k4	3
094	[number]	k4	094
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnPc2	sčítání
1930	[number]	k4	1930
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c4	v
6	[number]	k4	6
451	[number]	k4	451
domech	dům	k1gInPc6	dům
114	[number]	k4	114
704	[number]	k4	704
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
105	[number]	k4	105
731	[number]	k4	731
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
československé	československý	k2eAgFnSc3d1	Československá
národnosti	národnost	k1gFnSc3	národnost
a	a	k8xC	a
6	[number]	k4	6
782	[number]	k4	782
k	k	k7c3	k
německé	německý	k2eAgFnSc2d1	německá
<g/>
.	.	kIx.	.
</s>
<s>
Žilo	žít	k5eAaImAgNnS	žít
zde	zde	k6eAd1	zde
61	[number]	k4	61
344	[number]	k4	344
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
10	[number]	k4	10
891	[number]	k4	891
evangelíků	evangelík	k1gMnPc2	evangelík
<g/>
,	,	kIx,	,
6	[number]	k4	6
803	[number]	k4	803
příslušníků	příslušník	k1gMnPc2	příslušník
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
a	a	k8xC	a
2	[number]	k4	2
773	[number]	k4	773
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
rok	rok	k1gInSc1	rok
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
přesně	přesně	k6eAd1	přesně
119	[number]	k4	119
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
hlásí	hlásit	k5eAaImIp3nS	hlásit
k	k	k7c3	k
české	český	k2eAgFnSc3d1	Česká
národnosti	národnost	k1gFnSc3	národnost
<g/>
.	.	kIx.	.
</s>
<s>
Nejpočetnějšími	početní	k2eAgFnPc7d3	nejpočetnější
menšinami	menšina	k1gFnPc7	menšina
jsou	být	k5eAaImIp3nP	být
slovenská	slovenský	k2eAgFnSc1d1	slovenská
(	(	kIx(	(
<g/>
3	[number]	k4	3
0	[number]	k4	0
<g/>
86	[number]	k4	86
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ukrajinská	ukrajinský	k2eAgFnSc1d1	ukrajinská
(	(	kIx(	(
<g/>
2	[number]	k4	2
0	[number]	k4	0
<g/>
80	[number]	k4	80
<g/>
)	)	kIx)	)
a	a	k8xC	a
vietnamská	vietnamský	k2eAgFnSc1d1	vietnamská
(	(	kIx(	(
<g/>
965	[number]	k4	965
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Správa	správa	k1gFnSc1	správa
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Územní	územní	k2eAgNnSc1d1	územní
členění	členění	k1gNnSc1	členění
===	===	k?	===
</s>
</p>
<p>
<s>
Současnou	současný	k2eAgFnSc4d1	současná
rozlohu	rozloha	k1gFnSc4	rozloha
má	mít	k5eAaImIp3nS	mít
Plzeň	Plzeň	k1gFnSc1	Plzeň
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
připojily	připojit	k5eAaPmAgFnP	připojit
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
samostatné	samostatný	k2eAgFnSc2d1	samostatná
obce	obec	k1gFnSc2	obec
Lhota	Lhota	k1gFnSc1	Lhota
a	a	k8xC	a
Malesice	Malesice	k1gFnPc1	Malesice
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
zároveň	zároveň	k6eAd1	zároveň
staly	stát	k5eAaPmAgFnP	stát
novými	nový	k2eAgInPc7d1	nový
městskými	městský	k2eAgInPc7d1	městský
obvody	obvod	k1gInPc7	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
město	město	k1gNnSc4	město
Plzeň	Plzeň	k1gFnSc1	Plzeň
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
deseti	deset	k4xCc2	deset
městských	městský	k2eAgInPc2d1	městský
obvodů	obvod	k1gInPc2	obvod
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc4d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
jsou	být	k5eAaImIp3nP	být
obvody	obvod	k1gInPc1	obvod
1	[number]	k4	1
až	až	k8xS	až
4	[number]	k4	4
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
skládají	skládat	k5eAaImIp3nP	skládat
z	z	k7c2	z
několika	několik	k4yIc2	několik
původních	původní	k2eAgFnPc2d1	původní
městských	městský	k2eAgFnPc2d1	městská
čtvrtí	čtvrt	k1gFnPc2	čtvrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Plzeň	Plzeň	k1gFnSc1	Plzeň
1	[number]	k4	1
<g/>
:	:	kIx,	:
katastrální	katastrální	k2eAgFnSc1d1	katastrální
území	území	k1gNnSc4	území
Bolevec	Bolevec	k1gInSc1	Bolevec
a	a	k8xC	a
část	část	k1gFnSc1	část
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
Plzeň	Plzeň	k1gFnSc1	Plzeň
</s>
</p>
<p>
<s>
Plzeň	Plzeň	k1gFnSc1	Plzeň
2	[number]	k4	2
<g/>
-Slovany	-Slovan	k1gMnPc4	-Slovan
<g/>
:	:	kIx,	:
katastrální	katastrální	k2eAgFnSc1d1	katastrální
území	území	k1gNnSc4	území
Božkov	Božkov	k1gInSc1	Božkov
<g/>
,	,	kIx,	,
Bručná	Bručné	k1gNnPc1	Bručné
<g/>
,	,	kIx,	,
Hradiště	Hradiště	k1gNnSc1	Hradiště
u	u	k7c2	u
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
,	,	kIx,	,
Koterov	Koterov	k1gInSc4	Koterov
a	a	k8xC	a
část	část	k1gFnSc4	část
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
Plzeň	Plzeň	k1gFnSc1	Plzeň
</s>
</p>
<p>
<s>
Plzeň	Plzeň	k1gFnSc1	Plzeň
3	[number]	k4	3
<g/>
:	:	kIx,	:
katastrální	katastrální	k2eAgNnPc1d1	katastrální
území	území	k1gNnPc1	území
Doudlevce	Doudlevce	k1gMnSc2	Doudlevce
<g/>
,	,	kIx,	,
Radobyčice	Radobyčice	k1gFnSc2	Radobyčice
<g/>
,	,	kIx,	,
Skvrňany	Skvrňan	k1gMnPc4	Skvrňan
<g/>
,	,	kIx,	,
Valcha	valcha	k1gFnSc1	valcha
a	a	k8xC	a
část	část	k1gFnSc1	část
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
Plzeň	Plzeň	k1gFnSc1	Plzeň
</s>
</p>
<p>
<s>
Plzeň	Plzeň	k1gFnSc1	Plzeň
4	[number]	k4	4
<g/>
:	:	kIx,	:
katastrální	katastrální	k2eAgFnSc1d1	katastrální
území	území	k1gNnSc4	území
Bukovec	Bukovec	k1gInSc1	Bukovec
<g/>
,	,	kIx,	,
Červený	červený	k2eAgInSc1d1	červený
Hrádek	hrádek	k1gInSc1	hrádek
u	u	k7c2	u
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
,	,	kIx,	,
Doubravka	Doubravka	k1gFnSc1	Doubravka
<g/>
,	,	kIx,	,
Lobzy	Lobz	k1gInPc1	Lobz
<g/>
,	,	kIx,	,
Plzeň	Plzeň	k1gFnSc1	Plzeň
4	[number]	k4	4
a	a	k8xC	a
Újezd	Újezd	k1gInSc1	Újezd
</s>
</p>
<p>
<s>
Plzeň	Plzeň	k1gFnSc1	Plzeň
5	[number]	k4	5
<g/>
-Křimice	-Křimika	k1gFnSc6	-Křimika
<g/>
:	:	kIx,	:
větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
Křimice	Křimika	k1gFnSc3	Křimika
a	a	k8xC	a
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
Radčice	Radčice	k1gFnSc2	Radčice
u	u	k7c2	u
Plzně	Plzeň	k1gFnSc2	Plzeň
</s>
</p>
<p>
<s>
Plzeň	Plzeň	k1gFnSc1	Plzeň
6	[number]	k4	6
<g/>
-Litice	-Litice	k1gFnSc1	-Litice
<g/>
:	:	kIx,	:
katastrální	katastrální	k2eAgNnSc1d1	katastrální
území	území	k1gNnSc1	území
Litice	Litice	k1gFnSc2	Litice
u	u	k7c2	u
Plzně	Plzeň	k1gFnSc2	Plzeň
</s>
</p>
<p>
<s>
Plzeň	Plzeň	k1gFnSc1	Plzeň
7	[number]	k4	7
<g/>
-Radčice	-Radčice	k1gFnSc1	-Radčice
<g/>
:	:	kIx,	:
větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
Radčice	Radčice	k1gFnSc2	Radčice
u	u	k7c2	u
Plzně	Plzeň	k1gFnSc2	Plzeň
a	a	k8xC	a
malá	malý	k2eAgFnSc1d1	malá
část	část	k1gFnSc1	část
katastrálního	katastrální	k2eAgNnSc2d1	katastrální
území	území	k1gNnSc2	území
Křimice	Křimika	k1gFnSc3	Křimika
</s>
</p>
<p>
<s>
Plzeň	Plzeň	k1gFnSc1	Plzeň
8	[number]	k4	8
<g/>
-Černice	-Černice	k1gFnSc1	-Černice
<g/>
:	:	kIx,	:
katastrální	katastrální	k2eAgNnSc1d1	katastrální
území	území	k1gNnSc1	území
Černice	černice	k1gFnSc2	černice
</s>
</p>
<p>
<s>
Plzeň	Plzeň	k1gFnSc1	Plzeň
9	[number]	k4	9
<g/>
-Malesice	-Malesice	k1gFnSc1	-Malesice
<g/>
:	:	kIx,	:
katastrální	katastrální	k2eAgNnSc1d1	katastrální
území	území	k1gNnSc1	území
Malesice	Malesice	k1gFnSc2	Malesice
a	a	k8xC	a
Dolní	dolní	k2eAgInSc1d1	dolní
Vlkýš	Vlkýš	k1gInSc1	Vlkýš
</s>
</p>
<p>
<s>
Plzeň	Plzeň	k1gFnSc1	Plzeň
10	[number]	k4	10
<g/>
-Lhota	-Lhota	k1gFnSc1	-Lhota
<g/>
:	:	kIx,	:
katastrální	katastrální	k2eAgNnSc4d1	katastrální
území	území	k1gNnSc4	území
Lhota	Lhota	k1gFnSc1	Lhota
u	u	k7c2	u
DobřanPevět	DobřanPevět	k1gInSc4	DobřanPevět
městských	městský	k2eAgInPc2d1	městský
obvodů	obvod	k1gInPc2	obvod
při	při	k7c6	při
volbách	volba	k1gFnPc6	volba
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
tvoří	tvořit	k5eAaImIp3nS	tvořit
volební	volební	k2eAgInSc1d1	volební
obvod	obvod	k1gInSc1	obvod
č.	č.	k?	č.
9	[number]	k4	9
<g/>
,	,	kIx,	,
jen	jen	k6eAd1	jen
obvod	obvod	k1gInSc1	obvod
Plzeň	Plzeň	k1gFnSc1	Plzeň
2	[number]	k4	2
<g/>
-Slovany	-Slovan	k1gMnPc7	-Slovan
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
obcemi	obec	k1gFnPc7	obec
spadá	spadat	k5eAaImIp3nS	spadat
do	do	k7c2	do
volebního	volební	k2eAgInSc2d1	volební
obvodu	obvod	k1gInSc2	obvod
č.	č.	k?	č.
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Soudní	soudní	k2eAgInSc1d1	soudní
obvod	obvod	k1gInSc1	obvod
Okresního	okresní	k2eAgInSc2d1	okresní
soudu	soud	k1gInSc2	soud
Plzeň-město	Plzeňěsta	k1gMnSc5	Plzeň-města
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
pouze	pouze	k6eAd1	pouze
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
okres	okres	k1gInSc1	okres
Plzeň-město	Plzeňěsta	k1gMnSc5	Plzeň-města
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
další	další	k2eAgFnPc4d1	další
obce	obec	k1gFnPc4	obec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Instituce	instituce	k1gFnSc2	instituce
===	===	k?	===
</s>
</p>
<p>
<s>
Plzeň	Plzeň	k1gFnSc1	Plzeň
je	být	k5eAaImIp3nS	být
centrem	centrum	k1gNnSc7	centrum
Plzeňského	plzeňský	k2eAgInSc2d1	plzeňský
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
zde	zde	k6eAd1	zde
kromě	kromě	k7c2	kromě
různých	různý	k2eAgFnPc2d1	různá
krajských	krajský	k2eAgFnPc2d1	krajská
institucí	instituce	k1gFnPc2	instituce
sídlí	sídlet	k5eAaImIp3nS	sídlet
především	především	k9	především
hejtman	hejtman	k1gMnSc1	hejtman
s	s	k7c7	s
krajskou	krajský	k2eAgFnSc7d1	krajská
radou	rada	k1gFnSc7	rada
<g/>
,	,	kIx,	,
zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
kraje	kraj	k1gInSc2	kraj
i	i	k8xC	i
krajský	krajský	k2eAgInSc1d1	krajský
úřad	úřad	k1gInSc1	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
statutární	statutární	k2eAgNnSc1d1	statutární
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
řízeno	řídit	k5eAaImNgNnS	řídit
magistrátem	magistrát	k1gInSc7	magistrát
<g/>
,	,	kIx,	,
občané	občan	k1gMnPc1	občan
města	město	k1gNnSc2	město
každé	každý	k3xTgInPc4	každý
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
volí	volit	k5eAaImIp3nS	volit
47	[number]	k4	47
<g/>
členné	členný	k2eAgNnSc1d1	členné
zastupitelstvo	zastupitelstvo	k1gNnSc1	zastupitelstvo
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgInSc2	jenž
poté	poté	k6eAd1	poté
vychází	vycházet	k5eAaImIp3nS	vycházet
rada	rada	k1gFnSc1	rada
města	město	k1gNnSc2	město
a	a	k8xC	a
primátor	primátor	k1gMnSc1	primátor
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
deseti	deset	k4xCc2	deset
městských	městský	k2eAgInPc2d1	městský
obvodů	obvod	k1gInPc2	obvod
má	mít	k5eAaImIp3nS	mít
vlastní	vlastní	k2eAgNnSc4d1	vlastní
zastupitelstvo	zastupitelstvo	k1gNnSc4	zastupitelstvo
<g/>
,	,	kIx,	,
radu	rada	k1gFnSc4	rada
a	a	k8xC	a
místního	místní	k2eAgMnSc4d1	místní
starostu	starosta	k1gMnSc4	starosta
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
primátorem	primátor	k1gMnSc7	primátor
je	být	k5eAaImIp3nS	být
Martin	Martin	k1gMnSc1	Martin
Baxa	Baxa	k1gMnSc1	Baxa
a	a	k8xC	a
radniční	radniční	k2eAgFnSc4d1	radniční
koalici	koalice	k1gFnSc4	koalice
tvoří	tvořit	k5eAaImIp3nS	tvořit
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
ANO	ano	k9	ano
<g/>
,	,	kIx,	,
TOP	topit	k5eAaImRp2nS	topit
09	[number]	k4	09
a	a	k8xC	a
ČSSD	ČSSD	kA	ČSSD
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Soudnictví	soudnictví	k1gNnPc1	soudnictví
zde	zde	k6eAd1	zde
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
hned	hned	k6eAd1	hned
tři	tři	k4xCgInPc1	tři
okresní	okresní	k2eAgInPc1d1	okresní
soudy	soud	k1gInPc1	soud
<g/>
,	,	kIx,	,
Plzeň-město	Plzeňěsta	k1gFnSc5	Plzeň-města
<g/>
,	,	kIx,	,
Plzeň-jih	Plzeňih	k1gMnSc1	Plzeň-jih
a	a	k8xC	a
Plzeň-sever	Plzeňever	k1gInSc1	Plzeň-sever
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
odvolacím	odvolací	k2eAgInSc7d1	odvolací
soudem	soud	k1gInSc7	soud
je	být	k5eAaImIp3nS	být
Krajský	krajský	k2eAgInSc1d1	krajský
soud	soud	k1gInSc1	soud
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgInPc4	všechen
s	s	k7c7	s
příslušnými	příslušný	k2eAgInPc7d1	příslušný
státními	státní	k2eAgInPc7d1	státní
zastupitelstvími	zastupitelství	k1gNnPc7	zastupitelství
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
celý	celý	k2eAgInSc4d1	celý
obvod	obvod	k1gInSc4	obvod
krajského	krajský	k2eAgInSc2d1	krajský
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
celý	celý	k2eAgInSc1d1	celý
původní	původní	k2eAgInSc1d1	původní
Západočeský	západočeský	k2eAgInSc1d1	západočeský
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
zřízena	zřízen	k2eAgFnSc1d1	zřízena
notářská	notářský	k2eAgFnSc1d1	notářská
komora	komora	k1gFnSc1	komora
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
zde	zde	k6eAd1	zde
dále	daleko	k6eAd2	daleko
krajské	krajský	k2eAgNnSc1d1	krajské
a	a	k8xC	a
městské	městský	k2eAgNnSc1d1	Městské
ředitelství	ředitelství	k1gNnSc1	ředitelství
Policie	policie	k1gFnSc2	policie
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
i	i	k8xC	i
vlastní	vlastní	k2eAgFnSc1d1	vlastní
městská	městský	k2eAgFnSc1d1	městská
policie	policie	k1gFnSc1	policie
<g/>
.	.	kIx.	.
</s>
<s>
Plzeň	Plzeň	k1gFnSc1	Plzeň
se	se	k3xPyFc4	se
také	také	k6eAd1	také
proslavila	proslavit	k5eAaPmAgFnS	proslavit
věznicí	věznice	k1gFnSc7	věznice
označovanou	označovaný	k2eAgFnSc7d1	označovaná
jako	jako	k8xS	jako
věznice	věznice	k1gFnSc1	věznice
Bory	bor	k1gInPc4	bor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
sídlí	sídlet	k5eAaImIp3nS	sídlet
biskup	biskup	k1gMnSc1	biskup
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
diecéze	diecéze	k1gFnSc2	diecéze
<g/>
,	,	kIx,	,
Plzeň	Plzeň	k1gFnSc1	Plzeň
je	být	k5eAaImIp3nS	být
též	též	k9	též
sídlem	sídlo	k1gNnSc7	sídlo
biskupa	biskup	k1gMnSc2	biskup
diecéze	diecéze	k1gFnSc2	diecéze
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
a	a	k8xC	a
západočeského	západočeský	k2eAgInSc2d1	západočeský
seniorátu	seniorát	k1gInSc2	seniorát
Českobratrské	českobratrský	k2eAgFnSc2d1	Českobratrská
církve	církev	k1gFnSc2	církev
evangelické	evangelický	k2eAgFnSc2d1	evangelická
<g/>
.	.	kIx.	.
</s>
<s>
Lékařskou	lékařský	k2eAgFnSc4d1	lékařská
péči	péče	k1gFnSc4	péče
o	o	k7c4	o
obyvatele	obyvatel	k1gMnPc4	obyvatel
nejen	nejen	k6eAd1	nejen
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
jejího	její	k3xOp3gNnSc2	její
širokého	široký	k2eAgNnSc2d1	široké
okolí	okolí	k1gNnSc2	okolí
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
především	především	k9	především
Fakultní	fakultní	k2eAgFnSc1d1	fakultní
nemocnice	nemocnice	k1gFnSc1	nemocnice
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
pracoviště	pracoviště	k1gNnSc4	pracoviště
na	na	k7c6	na
Borech	bor	k1gInPc6	bor
a	a	k8xC	a
v	v	k7c6	v
Lochotíně	Lochotína	k1gFnSc6	Lochotína
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
např.	např.	kA	např.
nachází	nacházet	k5eAaImIp3nS	nacházet
Zoologická	zoologický	k2eAgFnSc1d1	zoologická
a	a	k8xC	a
botanická	botanický	k2eAgFnSc1d1	botanická
zahrada	zahrada	k1gFnSc1	zahrada
města	město	k1gNnSc2	město
Plzně	Plzeň	k1gFnSc2	Plzeň
nebo	nebo	k8xC	nebo
Arboretum	arboretum	k1gNnSc4	arboretum
Sofronka	Sofronka	k1gFnSc1	Sofronka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
==	==	k?	==
</s>
</p>
<p>
<s>
Historické	historický	k2eAgNnSc1d1	historické
jádro	jádro	k1gNnSc1	jádro
města	město	k1gNnSc2	město
a	a	k8xC	a
okolní	okolní	k2eAgFnSc2d1	okolní
ulice	ulice	k1gFnSc2	ulice
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
chráněny	chránit	k5eAaImNgFnP	chránit
jako	jako	k8xC	jako
městská	městský	k2eAgFnSc1d1	městská
památková	památkový	k2eAgFnSc1d1	památková
rezervace	rezervace	k1gFnSc1	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
několik	několik	k4yIc1	několik
vesnických	vesnický	k2eAgFnPc2d1	vesnická
památkových	památkový	k2eAgFnPc2d1	památková
rezervací	rezervace	k1gFnPc2	rezervace
a	a	k8xC	a
také	také	k9	také
městských	městský	k2eAgFnPc2d1	městská
a	a	k8xC	a
vesnických	vesnický	k2eAgFnPc2d1	vesnická
památkových	památkový	k2eAgFnPc2d1	památková
zón	zóna	k1gFnPc2	zóna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dominantou	dominanta	k1gFnSc7	dominanta
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
gotická	gotický	k2eAgFnSc1d1	gotická
katedrála	katedrála	k1gFnSc1	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Bartoloměje	Bartoloměj	k1gMnSc2	Bartoloměj
<g/>
,	,	kIx,	,
budovaná	budovaný	k2eAgFnSc1d1	budovaná
od	od	k7c2	od
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
102	[number]	k4	102
metrů	metr	k1gInPc2	metr
vysoká	vysoký	k2eAgFnSc1d1	vysoká
věž	věž	k1gFnSc1	věž
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
kostelní	kostelní	k2eAgFnSc7d1	kostelní
věží	věž	k1gFnSc7	věž
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
vyhlídku	vyhlídka	k1gFnSc4	vyhlídka
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
62	[number]	k4	62
metrů	metr	k1gInPc2	metr
vede	vést	k5eAaImIp3nS	vést
301	[number]	k4	301
schodů	schod	k1gInPc2	schod
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
farní	farní	k2eAgInSc1d1	farní
kostel	kostel	k1gInSc1	kostel
je	být	k5eAaImIp3nS	být
katedrálou	katedrála	k1gFnSc7	katedrála
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
zřízeno	zřídit	k5eAaPmNgNnS	zřídit
biskupství	biskupství	k1gNnSc1	biskupství
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
významné	významný	k2eAgFnPc1d1	významná
církevní	církevní	k2eAgFnPc1d1	církevní
památky	památka	k1gFnPc1	památka
jsou	být	k5eAaImIp3nP	být
bývalý	bývalý	k2eAgInSc4d1	bývalý
františkánský	františkánský	k2eAgInSc4d1	františkánský
klášter	klášter	k1gInSc4	klášter
s	s	k7c7	s
kostelem	kostel	k1gInSc7	kostel
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
P.	P.	kA	P.
Marie	Maria	k1gFnSc2	Maria
a	a	k8xC	a
barokní	barokní	k2eAgFnSc1d1	barokní
budova	budova	k1gFnSc1	budova
kláštera	klášter	k1gInSc2	klášter
dominikánek	dominikánka	k1gFnPc2	dominikánka
s	s	k7c7	s
kostelem	kostel	k1gInSc7	kostel
sv.	sv.	kA	sv.
Anny	Anna	k1gFnSc2	Anna
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1711	[number]	k4	1711
<g/>
–	–	k?	–
<g/>
1714	[number]	k4	1714
<g/>
,	,	kIx,	,
přestavěná	přestavěný	k2eAgFnSc1d1	přestavěná
1805	[number]	k4	1805
<g/>
–	–	k?	–
<g/>
1807	[number]	k4	1807
na	na	k7c4	na
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
umístěna	umístit	k5eAaPmNgFnS	umístit
Studijní	studijní	k2eAgFnSc1d1	studijní
a	a	k8xC	a
vědecká	vědecký	k2eAgFnSc1d1	vědecká
knihovna	knihovna	k1gFnSc1	knihovna
Plzeňského	plzeňský	k2eAgInSc2d1	plzeňský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
městskou	městský	k2eAgFnSc7d1	městská
zástavbou	zástavba	k1gFnSc7	zástavba
také	také	k9	také
ční	čnět	k5eAaImIp3nP	čnět
věže	věž	k1gFnPc1	věž
kostela	kostel	k1gInSc2	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
na	na	k7c6	na
Jižním	jižní	k2eAgNnSc6d1	jižní
Předměstí	předměstí	k1gNnSc6	předměstí
<g/>
,	,	kIx,	,
k	k	k7c3	k
nejstarším	starý	k2eAgInPc3d3	nejstarší
kostelům	kostel	k1gInPc3	kostel
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
pak	pak	k6eAd1	pak
patří	patřit	k5eAaImIp3nS	patřit
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Jiří	Jiří	k1gMnSc2	Jiří
na	na	k7c6	na
soutoku	soutok	k1gInSc6	soutok
Úslavy	Úslava	k1gFnSc2	Úslava
a	a	k8xC	a
Berounky	Berounka	k1gFnSc2	Berounka
v	v	k7c6	v
Plzni-Doubravce	Plzni-Doubravka	k1gFnSc6	Plzni-Doubravka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Před	před	k7c7	před
druhou	druhý	k4xOgFnSc7	druhý
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
žila	žít	k5eAaImAgFnS	žít
početná	početný	k2eAgFnSc1d1	početná
židovská	židovský	k2eAgFnSc1d1	židovská
komunita	komunita	k1gFnSc1	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Nepřehlédnutelná	přehlédnutelný	k2eNgFnSc1d1	nepřehlédnutelná
je	být	k5eAaImIp3nS	být
Velká	velký	k2eAgFnSc1d1	velká
synagoga	synagoga	k1gFnSc1	synagoga
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
druhou	druhý	k4xOgFnSc7	druhý
největší	veliký	k2eAgFnSc7d3	veliký
synagogou	synagoga	k1gFnSc7	synagoga
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
památky	památka	k1gFnPc4	památka
židovské	židovský	k2eAgFnSc2d1	židovská
kultury	kultura	k1gFnSc2	kultura
patří	patřit	k5eAaImIp3nS	patřit
Stará	starý	k2eAgFnSc1d1	stará
synagoga	synagoga	k1gFnSc1	synagoga
s	s	k7c7	s
pozůstatky	pozůstatek	k1gInPc7	pozůstatek
pomocné	pomocný	k2eAgFnSc2d1	pomocná
synagogy	synagoga	k1gFnSc2	synagoga
nebo	nebo	k8xC	nebo
Nový	nový	k2eAgInSc4d1	nový
židovský	židovský	k2eAgInSc4d1	židovský
hřbitov	hřbitov	k1gInSc4	hřbitov
s	s	k7c7	s
památníkem	památník	k1gInSc7	památník
obětem	oběť	k1gFnPc3	oběť
holocaustu	holocaust	k1gInSc2	holocaust
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
se	se	k3xPyFc4	se
dochoval	dochovat	k5eAaPmAgInS	dochovat
cenný	cenný	k2eAgInSc1d1	cenný
soubor	soubor	k1gInSc1	soubor
měšťanských	měšťanský	k2eAgInPc2d1	měšťanský
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Ozdobou	ozdoba	k1gFnSc7	ozdoba
centrálního	centrální	k2eAgNnSc2d1	centrální
náměstí	náměstí	k1gNnSc2	náměstí
Republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
renesanční	renesanční	k2eAgFnSc1d1	renesanční
plzeňská	plzeňský	k2eAgFnSc1d1	Plzeňská
radnice	radnice	k1gFnSc1	radnice
ze	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
katedrály	katedrála	k1gFnSc2	katedrála
nachází	nacházet	k5eAaImIp3nS	nacházet
také	také	k9	také
třeba	třeba	k6eAd1	třeba
biskupská	biskupský	k2eAgFnSc1d1	biskupská
rezidence	rezidence	k1gFnSc1	rezidence
nebo	nebo	k8xC	nebo
mariánský	mariánský	k2eAgInSc1d1	mariánský
sloup	sloup	k1gInSc1	sloup
a	a	k8xC	a
tři	tři	k4xCgFnPc1	tři
novodobé	novodobý	k2eAgFnPc1d1	novodobá
kašny	kašna	k1gFnPc1	kašna
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgFnPc4d1	významná
památky	památka	k1gFnPc4	památka
dále	daleko	k6eAd2	daleko
patří	patřit	k5eAaImIp3nS	patřit
Měšťanská	měšťanský	k2eAgFnSc1d1	měšťanská
beseda	beseda	k1gFnSc1	beseda
<g/>
,	,	kIx,	,
budova	budova	k1gFnSc1	budova
Velkého	velký	k2eAgNnSc2d1	velké
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
masné	masný	k2eAgInPc4d1	masný
krámy	krám	k1gInPc4	krám
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
sídlí	sídlet	k5eAaImIp3nS	sídlet
Západočeská	západočeský	k2eAgFnSc1d1	Západočeská
galerie	galerie	k1gFnSc1	galerie
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
plzeňské	plzeňský	k2eAgNnSc4d1	plzeňské
historické	historický	k2eAgNnSc4d1	historické
podzemí	podzemí	k1gNnSc4	podzemí
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
také	také	k9	také
řada	řada	k1gFnSc1	řada
domů	dům	k1gInPc2	dům
zdobených	zdobený	k2eAgInPc2d1	zdobený
freskami	freska	k1gFnPc7	freska
a	a	k8xC	a
sgrafity	sgrafito	k1gNnPc7	sgrafito
malíře	malíř	k1gMnSc2	malíř
Mikoláše	Mikoláš	k1gMnSc2	Mikoláš
Alše	Aleš	k1gMnSc2	Aleš
<g/>
,	,	kIx,	,
např.	např.	kA	např.
U	u	k7c2	u
Bílého	bílý	k2eAgMnSc2d1	bílý
jednorožce	jednorožec	k1gMnSc2	jednorožec
nebo	nebo	k8xC	nebo
U	u	k7c2	u
Červeného	Červeného	k2eAgNnSc2d1	Červeného
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Známé	známý	k2eAgFnPc1d1	známá
jsou	být	k5eAaImIp3nP	být
též	též	k9	též
bytové	bytový	k2eAgInPc4d1	bytový
interiéry	interiér	k1gInPc4	interiér
od	od	k7c2	od
architekta	architekt	k1gMnSc2	architekt
Adolfa	Adolf	k1gMnSc2	Adolf
Loose	Loos	k1gMnSc2	Loos
<g/>
,	,	kIx,	,
zajímavým	zajímavý	k2eAgInSc7d1	zajímavý
dokladem	doklad	k1gInSc7	doklad
moderního	moderní	k2eAgInSc2d1	moderní
rozvoje	rozvoj	k1gInSc2	rozvoj
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
i	i	k9	i
tzv.	tzv.	kA	tzv.
Mrakodrap	mrakodrap	k1gInSc1	mrakodrap
<g/>
.	.	kIx.	.
</s>
<s>
Hodnotná	hodnotný	k2eAgFnSc1d1	hodnotná
vesnická	vesnický	k2eAgFnSc1d1	vesnická
architektura	architektura	k1gFnSc1	architektura
se	se	k3xPyFc4	se
dochovala	dochovat	k5eAaPmAgFnS	dochovat
např.	např.	kA	např.
v	v	k7c6	v
Božkově	Božkův	k2eAgNnSc6d1	Božkovo
<g/>
,	,	kIx,	,
Koterově	Koterův	k2eAgNnSc6d1	Koterův
<g/>
,	,	kIx,	,
Černicích	černice	k1gFnPc6	černice
nebo	nebo	k8xC	nebo
Radobyčicích	Radobyčice	k1gFnPc6	Radobyčice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgFnPc4d1	významná
technické	technický	k2eAgFnPc4d1	technická
památky	památka	k1gFnPc4	památka
na	na	k7c6	na
území	území	k1gNnSc6	území
Plzně	Plzeň	k1gFnSc2	Plzeň
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Tyršův	Tyršův	k2eAgInSc1d1	Tyršův
most	most	k1gInSc1	most
–	–	k?	–
první	první	k4xOgInSc4	první
zcela	zcela	k6eAd1	zcela
svařovaný	svařovaný	k2eAgInSc4d1	svařovaný
obloukový	obloukový	k2eAgInSc4d1	obloukový
most	most	k1gInSc4	most
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
vodárenská	vodárenský	k2eAgFnSc1d1	vodárenská
věž	věž	k1gFnSc1	věž
v	v	k7c6	v
Pražské	pražský	k2eAgFnSc6d1	Pražská
ulici	ulice	k1gFnSc6	ulice
se	s	k7c7	s
zbytky	zbytek	k1gInPc7	zbytek
původního	původní	k2eAgNnSc2d1	původní
zařízení	zařízení	k1gNnSc2	zařízení
nebo	nebo	k8xC	nebo
zbytky	zbytek	k1gInPc7	zbytek
opevnění	opevnění	k1gNnSc2	opevnění
po	po	k7c6	po
obvodu	obvod	k1gInSc6	obvod
historického	historický	k2eAgNnSc2d1	historické
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Unikátní	unikátní	k2eAgFnSc7d1	unikátní
technickou	technický	k2eAgFnSc7d1	technická
památkou	památka	k1gFnSc7	památka
je	být	k5eAaImIp3nS	být
též	též	k9	též
Kolomazná	kolomazný	k2eAgFnSc1d1	Kolomazná
pec	pec	k1gFnSc1	pec
v	v	k7c6	v
lese	les	k1gInSc6	les
v	v	k7c6	v
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Bolevec	Bolevec	k1gMnSc1	Bolevec
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejzachovalejších	zachovalý	k2eAgFnPc2d3	nejzachovalejší
pecí	pec	k1gFnPc2	pec
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc1	několik
památných	památný	k2eAgInPc2d1	památný
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
menších	malý	k2eAgNnPc2d2	menší
chráněných	chráněný	k2eAgNnPc2d1	chráněné
území	území	k1gNnPc2	území
<g/>
:	:	kIx,	:
duby	dub	k1gInPc7	dub
u	u	k7c2	u
Velkého	velký	k2eAgInSc2d1	velký
rybníka	rybník	k1gInSc2	rybník
<g/>
,	,	kIx,	,
Koterovská	Koterovský	k2eAgFnSc1d1	Koterovská
lípa	lípa	k1gFnSc1	lípa
<g/>
,	,	kIx,	,
Körnerův	Körnerův	k2eAgInSc1d1	Körnerův
dub	dub	k1gInSc1	dub
<g/>
,	,	kIx,	,
lípy	lípa	k1gFnPc1	lípa
u	u	k7c2	u
Mže	Mže	k1gFnSc2	Mže
<g/>
,	,	kIx,	,
smrk	smrk	k1gInSc1	smrk
–	–	k?	–
Troják	troják	k1gInSc1	troják
v	v	k7c6	v
Lánech	lán	k1gInPc6	lán
a	a	k8xC	a
alej	alej	k1gFnSc1	alej
Kilometrovka	Kilometrovka	k1gFnSc1	Kilometrovka
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
památkou	památka	k1gFnSc7	památka
je	být	k5eAaImIp3nS	být
pozdně	pozdně	k6eAd1	pozdně
středověká	středověký	k2eAgFnSc1d1	středověká
bolevecká	bolevecký	k2eAgFnSc1d1	bolevecká
rybniční	rybniční	k2eAgFnSc1d1	rybniční
soustava	soustava	k1gFnSc1	soustava
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
okraji	okraj	k1gInSc6	okraj
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Známý	známý	k2eAgInSc1d1	známý
je	být	k5eAaImIp3nS	být
též	též	k9	též
Památník	památník	k1gInSc1	památník
obětem	oběť	k1gFnPc3	oběť
zla	zlo	k1gNnSc2	zlo
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
meditační	meditační	k2eAgFnSc1d1	meditační
zahrada	zahrada	k1gFnSc1	zahrada
v	v	k7c6	v
Plzni-Doudlevcích	Plzni-Doudlevek	k1gInPc6	Plzni-Doudlevek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Divadla	divadlo	k1gNnSc2	divadlo
===	===	k?	===
</s>
</p>
<p>
<s>
Zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
prvních	první	k4xOgNnPc6	první
divadelních	divadelní	k2eAgNnPc6d1	divadelní
představeních	představení	k1gNnPc6	představení
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
,	,	kIx,	,
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
<g/>
,	,	kIx,	,
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
přelomu	přelom	k1gInSc2	přelom
18	[number]	k4	18
<g/>
.	.	kIx.	.
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
představení	představení	k1gNnSc1	představení
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
je	být	k5eAaImIp3nS	být
doloženo	doložen	k2eAgNnSc1d1	doloženo
roku	rok	k1gInSc2	rok
1818	[number]	k4	1818
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
divadlo	divadlo	k1gNnSc1	divadlo
zahájilo	zahájit	k5eAaPmAgNnS	zahájit
provoz	provoz	k1gInSc4	provoz
roku	rok	k1gInSc2	rok
1832	[number]	k4	1832
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Největším	veliký	k2eAgMnSc7d3	veliký
a	a	k8xC	a
nejvýznamnějším	významný	k2eAgNnSc7d3	nejvýznamnější
divadlem	divadlo	k1gNnSc7	divadlo
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
je	být	k5eAaImIp3nS	být
městské	městský	k2eAgNnSc1d1	Městské
Divadlo	divadlo	k1gNnSc1	divadlo
Josefa	Josef	k1gMnSc2	Josef
Kajetána	Kajetán	k1gMnSc2	Kajetán
Tyla	Tyl	k1gMnSc2	Tyl
<g/>
.	.	kIx.	.
</s>
<s>
Divadlo	divadlo	k1gNnSc1	divadlo
je	být	k5eAaImIp3nS	být
tvořeno	tvořit	k5eAaImNgNnS	tvořit
pěti	pět	k4xCc7	pět
uměleckými	umělecký	k2eAgInPc7d1	umělecký
soubory	soubor	k1gInPc7	soubor
–	–	k?	–
opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
činohra	činohra	k1gFnSc1	činohra
<g/>
,	,	kIx,	,
muzikál	muzikál	k1gInSc1	muzikál
<g/>
,	,	kIx,	,
balet	balet	k1gInSc1	balet
a	a	k8xC	a
orchestr	orchestr	k1gInSc1	orchestr
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
stálé	stálý	k2eAgFnPc4d1	stálá
scény	scéna	k1gFnPc4	scéna
<g/>
:	:	kIx,	:
Velké	velký	k2eAgNnSc4d1	velké
divadlo	divadlo	k1gNnSc4	divadlo
v	v	k7c6	v
reprezentativní	reprezentativní	k2eAgFnSc6d1	reprezentativní
novorenesanční	novorenesanční	k2eAgFnSc6d1	novorenesanční
budově	budova	k1gFnSc6	budova
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1897	[number]	k4	1897
<g/>
–	–	k?	–
<g/>
1902	[number]	k4	1902
a	a	k8xC	a
Nové	Nové	k2eAgNnSc4d1	Nové
divadlo	divadlo	k1gNnSc4	divadlo
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
budově	budova	k1gFnSc6	budova
dostavěné	dostavěný	k2eAgFnSc2d1	dostavěná
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
má	mít	k5eAaImIp3nS	mít
velkou	velký	k2eAgFnSc4d1	velká
tradici	tradice	k1gFnSc4	tradice
loutkové	loutkový	k2eAgNnSc1d1	loutkové
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
působili	působit	k5eAaImAgMnP	působit
zde	zde	k6eAd1	zde
známí	známý	k2eAgMnPc1d1	známý
loutkáři	loutkář	k1gMnPc1	loutkář
Gustav	Gustav	k1gMnSc1	Gustav
Nosek	Nosek	k1gMnSc1	Nosek
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Skupa	skupa	k1gFnSc1	skupa
a	a	k8xC	a
Jiří	Jiří	k1gMnSc1	Jiří
Trnka	Trnka	k1gMnSc1	Trnka
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
zde	zde	k6eAd1	zde
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
loutky	loutka	k1gFnPc1	loutka
Spejbla	Spejbl	k1gMnSc2	Spejbl
a	a	k8xC	a
Hurvínka	Hurvínek	k1gMnSc2	Hurvínek
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
tradici	tradice	k1gFnSc4	tradice
dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
navazuje	navazovat	k5eAaImIp3nS	navazovat
Divadlo	divadlo	k1gNnSc1	divadlo
Alfa	alfa	k1gNnSc2	alfa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
menším	malý	k2eAgNnPc3d2	menší
divadlům	divadlo	k1gNnPc3	divadlo
a	a	k8xC	a
alternativě	alternativa	k1gFnSc3	alternativa
plzeňské	plzeňský	k2eAgFnSc2d1	Plzeňská
divadelní	divadelní	k2eAgFnSc2d1	divadelní
kultury	kultura	k1gFnSc2	kultura
patří	patřit	k5eAaImIp3nS	patřit
Divadlo	divadlo	k1gNnSc4	divadlo
Dialog	dialog	k1gInSc1	dialog
s	s	k7c7	s
dvanácti	dvanáct	k4xCc7	dvanáct
soubory	soubor	k1gInPc7	soubor
<g/>
.	.	kIx.	.
</s>
<s>
Komorní	komorní	k2eAgFnSc7d1	komorní
scénou	scéna	k1gFnSc7	scéna
je	být	k5eAaImIp3nS	být
Divadélko	divadélko	k1gNnSc1	divadélko
JoNáš	Jonáš	k1gMnSc1	Jonáš
v	v	k7c6	v
Měšťanské	měšťanský	k2eAgFnSc6d1	měšťanská
besedě	beseda	k1gFnSc6	beseda
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
souborů	soubor	k1gInPc2	soubor
je	být	k5eAaImIp3nS	být
amatérský	amatérský	k2eAgInSc1d1	amatérský
Divadelní	divadelní	k2eAgInSc1d1	divadelní
spolek	spolek	k1gInSc1	spolek
Jezírko	jezírko	k1gNnSc1	jezírko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kina	kino	k1gNnSc2	kino
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
dobách	doba	k1gFnPc6	doba
komunismu	komunismus	k1gInSc2	komunismus
měla	mít	k5eAaImAgFnS	mít
téměř	téměř	k6eAd1	téměř
každá	každý	k3xTgFnSc1	každý
plzeňská	plzeňský	k2eAgFnSc1d1	Plzeňská
část	část	k1gFnSc1	část
svoje	svůj	k3xOyFgNnSc4	svůj
kino	kino	k1gNnSc4	kino
<g/>
,	,	kIx,	,
další	další	k2eAgNnPc1d1	další
kina	kino	k1gNnPc1	kino
pak	pak	k6eAd1	pak
působila	působit	k5eAaImAgNnP	působit
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
–	–	k?	–
například	například	k6eAd1	například
kino	kino	k1gNnSc4	kino
Hvězda	hvězda	k1gFnSc1	hvězda
v	v	k7c6	v
Pražské	pražský	k2eAgFnSc6d1	Pražská
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
kino	kino	k1gNnSc4	kino
Moskva	Moskva	k1gFnSc1	Moskva
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
Elektra	Elektr	k1gMnSc2	Elektr
<g/>
)	)	kIx)	)
na	na	k7c6	na
Americké	americký	k2eAgFnSc6d1	americká
třídě	třída	k1gFnSc6	třída
<g/>
,	,	kIx,	,
kino	kino	k1gNnSc1	kino
Eden	Eden	k1gInSc1	Eden
v	v	k7c6	v
Rejskově	rejskův	k2eAgFnSc6d1	Rejskova
ulici	ulice	k1gFnSc6	ulice
za	za	k7c7	za
hlavním	hlavní	k2eAgNnSc7d1	hlavní
vlakovým	vlakový	k2eAgNnSc7d1	vlakové
nádražím	nádraží	k1gNnSc7	nádraží
<g/>
,	,	kIx,	,
kino	kino	k1gNnSc4	kino
Mír	Míra	k1gFnPc2	Míra
na	na	k7c6	na
Lochotíně	Lochotína	k1gFnSc6	Lochotína
či	či	k8xC	či
kino	kino	k1gNnSc4	kino
Leningrad	Leningrad	k1gInSc1	Leningrad
v	v	k7c6	v
Doubravce	Doubravka	k1gFnSc6	Doubravka
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
tato	tento	k3xDgNnPc1	tento
kina	kino	k1gNnPc1	kino
ale	ale	k9	ale
po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
dříve	dříve	k6eAd2	dříve
či	či	k8xC	či
později	pozdě	k6eAd2	pozdě
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
mají	mít	k5eAaImIp3nP	mít
filmoví	filmový	k2eAgMnPc1d1	filmový
diváci	divák	k1gMnPc1	divák
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
dvě	dva	k4xCgNnPc1	dva
multikina	multikino	k1gNnPc1	multikino
<g/>
,	,	kIx,	,
Cinema	Cinema	k1gNnSc1	Cinema
City	City	k1gFnSc2	City
v	v	k7c6	v
obchodním	obchodní	k2eAgInSc6d1	obchodní
centru	centr	k1gInSc6	centr
Plaza	plaz	k1gMnSc2	plaz
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
bývalého	bývalý	k2eAgNnSc2d1	bývalé
výstaviště	výstaviště	k1gNnSc2	výstaviště
a	a	k8xC	a
Cinestar	Cinestara	k1gFnPc2	Cinestara
v	v	k7c6	v
obchodním	obchodní	k2eAgInSc6d1	obchodní
centru	centr	k1gInSc6	centr
Olympia	Olympia	k1gFnSc1	Olympia
u	u	k7c2	u
výpadovky	výpadovka	k1gFnSc2	výpadovka
na	na	k7c4	na
Nepomuk	Nepomuk	k1gInSc4	Nepomuk
<g/>
.	.	kIx.	.
</s>
<s>
Pravidelná	pravidelný	k2eAgNnPc1d1	pravidelné
představení	představení	k1gNnPc1	představení
se	se	k3xPyFc4	se
také	také	k9	také
konají	konat	k5eAaImIp3nP	konat
v	v	k7c6	v
Kině	kino	k1gNnSc6	kino
Beseda	beseda	k1gFnSc1	beseda
působícím	působící	k2eAgInSc7d1	působící
v	v	k7c6	v
historické	historický	k2eAgFnSc6d1	historická
budově	budova	k1gFnSc6	budova
Měšťanské	měšťanský	k2eAgFnSc2d1	měšťanská
besedy	beseda	k1gFnSc2	beseda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letních	letní	k2eAgInPc6d1	letní
měsících	měsíc	k1gInPc6	měsíc
jsou	být	k5eAaImIp3nP	být
pořádána	pořádán	k2eAgNnPc4d1	pořádáno
představení	představení	k1gNnPc4	představení
v	v	k7c6	v
lochotínském	lochotínský	k2eAgInSc6d1	lochotínský
amfiteátru	amfiteátr	k1gInSc6	amfiteátr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rozhlas	rozhlas	k1gInSc1	rozhlas
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
sídlí	sídlet	k5eAaImIp3nS	sídlet
Český	český	k2eAgInSc1d1	český
rozhlas	rozhlas	k1gInSc1	rozhlas
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
,	,	kIx,	,
regionální	regionální	k2eAgNnSc1d1	regionální
studio	studio	k1gNnSc1	studio
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
památkové	památkový	k2eAgFnSc3d1	památková
chráněné	chráněný	k2eAgFnSc3d1	chráněná
budově	budova	k1gFnSc3	budova
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
Míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
speciálně	speciálně	k6eAd1	speciálně
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
rozhlasového	rozhlasový	k2eAgNnSc2d1	rozhlasové
vysílání	vysílání	k1gNnSc2	vysílání
postavena	postaven	k2eAgFnSc1d1	postavena
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Muzea	muzeum	k1gNnSc2	muzeum
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Kopeckého	Kopeckého	k2eAgInPc6d1	Kopeckého
sadech	sad	k1gInPc6	sad
sídlí	sídlet	k5eAaImIp3nS	sídlet
Západočeské	západočeský	k2eAgNnSc1d1	Západočeské
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
budově	budova	k1gFnSc6	budova
v	v	k7c6	v
novorenesančním	novorenesanční	k2eAgInSc6d1	novorenesanční
stylu	styl	k1gInSc6	styl
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1896	[number]	k4	1896
<g/>
–	–	k?	–
<g/>
1899	[number]	k4	1899
lze	lze	k6eAd1	lze
navštívit	navštívit	k5eAaPmF	navštívit
zejména	zejména	k9	zejména
expozici	expozice	k1gFnSc4	expozice
městské	městský	k2eAgFnSc2d1	městská
zbrojnice	zbrojnice	k1gFnSc2	zbrojnice
<g/>
,	,	kIx,	,
míšeňského	míšeňský	k2eAgInSc2d1	míšeňský
porcelánu	porcelán	k1gInSc2	porcelán
a	a	k8xC	a
výstavy	výstava	k1gFnSc2	výstava
přibližující	přibližující	k2eAgFnSc4d1	přibližující
historii	historie	k1gFnSc4	historie
regionu	region	k1gInSc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Pobočkami	pobočka	k1gFnPc7	pobočka
jsou	být	k5eAaImIp3nP	být
Národopisné	národopisný	k2eAgNnSc1d1	Národopisné
muzeum	muzeum	k1gNnSc1	muzeum
Plzeňska	Plzeňsko	k1gNnSc2	Plzeňsko
<g/>
,	,	kIx,	,
Muzeum	muzeum	k1gNnSc1	muzeum
církevního	církevní	k2eAgNnSc2d1	církevní
umění	umění	k1gNnSc2	umění
plzeňské	plzeňský	k2eAgFnSc2d1	Plzeňská
diecéze	diecéze	k1gFnSc2	diecéze
ve	v	k7c6	v
františkánském	františkánský	k2eAgInSc6d1	františkánský
klášteře	klášter	k1gInSc6	klášter
a	a	k8xC	a
Muzeum	muzeum	k1gNnSc1	muzeum
loutek	loutka	k1gFnPc2	loutka
<g/>
.	.	kIx.	.
</s>
<s>
Plzeňský	plzeňský	k2eAgInSc1d1	plzeňský
Prazdroj	prazdroj	k1gInSc1	prazdroj
provozuje	provozovat	k5eAaImIp3nS	provozovat
vlastní	vlastní	k2eAgNnSc4d1	vlastní
Pivovarské	pivovarský	k2eAgNnSc4d1	Pivovarské
muzeum	muzeum	k1gNnSc4	muzeum
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgNnSc2	který
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vstoupit	vstoupit	k5eAaPmF	vstoupit
i	i	k9	i
do	do	k7c2	do
prohlídkového	prohlídkový	k2eAgInSc2d1	prohlídkový
okruhu	okruh	k1gInSc2	okruh
plzeňského	plzeňský	k2eAgNnSc2d1	plzeňské
historického	historický	k2eAgNnSc2d1	historické
podzemí	podzemí	k1gNnSc2	podzemí
<g/>
.	.	kIx.	.
</s>
<s>
Osvobození	osvobození	k1gNnSc1	osvobození
Plzně	Plzeň	k1gFnSc2	Plzeň
americkou	americký	k2eAgFnSc7d1	americká
armádou	armáda	k1gFnSc7	armáda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
je	být	k5eAaImIp3nS	být
věnováno	věnován	k2eAgNnSc1d1	věnováno
muzeum	muzeum	k1gNnSc1	muzeum
Patton	Patton	k1gInSc1	Patton
Memorial	Memorial	k1gMnSc1	Memorial
Pilsen	Pilsen	k1gInSc1	Pilsen
<g/>
.	.	kIx.	.
</s>
<s>
Tematické	tematický	k2eAgFnPc4d1	tematická
expozice	expozice	k1gFnPc4	expozice
věnované	věnovaný	k2eAgFnSc3d1	věnovaná
vědě	věda	k1gFnSc3	věda
a	a	k8xC	a
technice	technika	k1gFnSc3	technika
nabízí	nabízet	k5eAaImIp3nS	nabízet
Techmania	Techmanium	k1gNnSc2	Techmanium
Science	Science	k1gFnSc2	Science
Center	centrum	k1gNnPc2	centrum
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
bývalé	bývalý	k2eAgFnSc2d1	bývalá
Škodovky	škodovka	k1gFnSc2	škodovka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
nově	nově	k6eAd1	nově
opravené	opravený	k2eAgFnSc6d1	opravená
Staré	Staré	k2eAgFnSc6d1	Staré
synagoze	synagoga	k1gFnSc6	synagoga
je	být	k5eAaImIp3nS	být
stálá	stálý	k2eAgFnSc1d1	stálá
expozice	expozice	k1gFnSc1	expozice
"	"	kIx"	"
<g/>
Židovské	židovský	k2eAgFnPc4d1	židovská
tradice	tradice	k1gFnPc4	tradice
a	a	k8xC	a
zvyky	zvyk	k1gInPc4	zvyk
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Galerie	galerie	k1gFnSc1	galerie
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
sídlí	sídlet	k5eAaImIp3nS	sídlet
Západočeská	západočeský	k2eAgFnSc1d1	Západočeská
galerie	galerie	k1gFnSc1	galerie
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
,	,	kIx,	,
Galerie	galerie	k1gFnSc1	galerie
města	město	k1gNnSc2	město
Plzně	Plzeň	k1gFnSc2	Plzeň
a	a	k8xC	a
univerzitní	univerzitní	k2eAgFnSc2d1	univerzitní
Galerie	galerie	k1gFnSc2	galerie
Ladislava	Ladislav	k1gMnSc2	Ladislav
Sutnara	Sutnar	k1gMnSc2	Sutnar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Knihovny	knihovna	k1gFnSc2	knihovna
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1876	[number]	k4	1876
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
Knihovna	knihovna	k1gFnSc1	knihovna
města	město	k1gNnSc2	město
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
dnes	dnes	k6eAd1	dnes
kromě	kromě	k7c2	kromě
ústřední	ústřední	k2eAgFnSc2d1	ústřední
budovy	budova	k1gFnSc2	budova
9	[number]	k4	9
knihoven	knihovna	k1gFnPc2	knihovna
s	s	k7c7	s
celotýdenním	celotýdenní	k2eAgInSc7d1	celotýdenní
provozem	provoz	k1gInSc7	provoz
a	a	k8xC	a
12	[number]	k4	12
malých	malý	k2eAgFnPc2d1	malá
poboček	pobočka	k1gFnPc2	pobočka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
sídlí	sídlet	k5eAaImIp3nS	sídlet
Studijní	studijní	k2eAgFnSc1d1	studijní
a	a	k8xC	a
vědecká	vědecký	k2eAgFnSc1d1	vědecká
knihovna	knihovna	k1gFnSc1	knihovna
Plzeňského	plzeňský	k2eAgInSc2d1	plzeňský
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
knihovny	knihovna	k1gFnPc4	knihovna
patří	patřit	k5eAaImIp3nS	patřit
Univerzitní	univerzitní	k2eAgFnSc1d1	univerzitní
knihovna	knihovna	k1gFnSc1	knihovna
Západočeské	západočeský	k2eAgFnSc2d1	Západočeská
univerzity	univerzita	k1gFnSc2	univerzita
a	a	k8xC	a
knihovna	knihovna	k1gFnSc1	knihovna
Západočeského	západočeský	k2eAgNnSc2d1	Západočeské
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Významné	významný	k2eAgFnPc1d1	významná
kulturní	kulturní	k2eAgFnPc1d1	kulturní
akce	akce	k1gFnPc1	akce
===	===	k?	===
</s>
</p>
<p>
<s>
Smetanovské	smetanovský	k2eAgInPc1d1	smetanovský
dny	den	k1gInPc1	den
–	–	k?	–
festival	festival	k1gInSc4	festival
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
spojený	spojený	k2eAgInSc1d1	spojený
s	s	k7c7	s
pořádáním	pořádání	k1gNnSc7	pořádání
koncertů	koncert	k1gInPc2	koncert
<g/>
,	,	kIx,	,
divadelních	divadelní	k2eAgNnPc2d1	divadelní
představení	představení	k1gNnPc2	představení
<g/>
,	,	kIx,	,
výstav	výstava	k1gFnPc2	výstava
a	a	k8xC	a
sympózií	sympózium	k1gNnPc2	sympózium
<g/>
;	;	kIx,	;
obvyklým	obvyklý	k2eAgInSc7d1	obvyklý
termínem	termín	k1gInSc7	termín
konání	konání	k1gNnSc2	konání
je	být	k5eAaImIp3nS	být
březen	březen	k1gInSc1	březen
</s>
</p>
<p>
<s>
Finále	finála	k1gFnSc3	finála
Plzeň	Plzeň	k1gFnSc1	Plzeň
–	–	k?	–
filmový	filmový	k2eAgInSc1d1	filmový
festival	festival	k1gInSc1	festival
českých	český	k2eAgInPc2d1	český
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
koná	konat	k5eAaImIp3nS	konat
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
koncem	koncem	k7c2	koncem
dubna	duben	k1gInSc2	duben
</s>
</p>
<p>
<s>
Plzeňský	plzeňský	k2eAgInSc1d1	plzeňský
Majáles	majáles	k1gInSc1	majáles
–	–	k?	–
studentské	studentský	k2eAgFnPc1d1	studentská
oslavy	oslava	k1gFnPc1	oslava
<g/>
,	,	kIx,	,
tradičně	tradičně	k6eAd1	tradičně
pořádané	pořádaný	k2eAgFnPc1d1	pořádaná
koncem	koncem	k7c2	koncem
dubna	duben	k1gInSc2	duben
</s>
</p>
<p>
<s>
Metalfest	Metalfest	k1gMnSc1	Metalfest
Open	Open	k1gMnSc1	Open
Air	Air	k1gFnSc2	Air
–	–	k?	–
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
metalový	metalový	k2eAgInSc4d1	metalový
festival	festival	k1gInSc4	festival
konající	konající	k2eAgInSc4d1	konající
se	se	k3xPyFc4	se
v	v	k7c6	v
lochotínském	lochotínský	k2eAgInSc6d1	lochotínský
amfiteátru	amfiteátr	k1gInSc6	amfiteátr
každoročně	každoročně	k6eAd1	každoročně
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
května	květen	k1gInSc2	květen
a	a	k8xC	a
června	červen	k1gInSc2	červen
</s>
</p>
<p>
<s>
Slavnosti	slavnost	k1gFnPc1	slavnost
svobody	svoboda	k1gFnSc2	svoboda
–	–	k?	–
slavnosti	slavnost	k1gFnSc2	slavnost
osvobození	osvobození	k1gNnSc2	osvobození
Plzně	Plzeň	k1gFnSc2	Plzeň
americkou	americký	k2eAgFnSc7d1	americká
armádou	armáda	k1gFnSc7	armáda
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
konané	konaný	k2eAgFnSc2d1	konaná
začátkem	začátkem	k7c2	začátkem
května	květen	k1gInSc2	květen
<g/>
;	;	kIx,	;
tradiční	tradiční	k2eAgFnSc7d1	tradiční
součástí	součást	k1gFnSc7	součást
slavností	slavnost	k1gFnPc2	slavnost
je	být	k5eAaImIp3nS	být
konvoj	konvoj	k1gInSc4	konvoj
historických	historický	k2eAgNnPc2d1	historické
vozidel	vozidlo	k1gNnPc2	vozidlo
(	(	kIx(	(
<g/>
Convoy	Convoa	k1gFnPc1	Convoa
of	of	k?	of
Liberty	Libert	k1gInPc4	Libert
<g/>
)	)	kIx)	)
s	s	k7c7	s
účastí	účast	k1gFnSc7	účast
amerických	americký	k2eAgMnPc2d1	americký
veteránů	veterán	k1gMnPc2	veterán
</s>
</p>
<p>
<s>
Skupova	Skupův	k2eAgFnSc1d1	Skupova
Plzeň	Plzeň	k1gFnSc1	Plzeň
–	–	k?	–
festival	festival	k1gInSc1	festival
loutkového	loutkový	k2eAgNnSc2d1	loutkové
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
koná	konat	k5eAaImIp3nS	konat
se	se	k3xPyFc4	se
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
festival	festival	k1gInSc1	festival
Divadlo	divadlo	k1gNnSc1	divadlo
–	–	k?	–
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
divadelní	divadelní	k2eAgInSc4d1	divadelní
festival	festival	k1gInSc4	festival
<g/>
,	,	kIx,	,
koná	konat	k5eAaImIp3nS	konat
se	se	k3xPyFc4	se
v	v	k7c6	v
září	září	k1gNnSc6	září
</s>
</p>
<p>
<s>
Pilsner	Pilsner	k1gMnSc1	Pilsner
Fest	fest	k6eAd1	fest
–	–	k?	–
slavnosti	slavnost	k1gFnSc2	slavnost
plzeňského	plzeňský	k2eAgNnSc2d1	plzeňské
piva	pivo	k1gNnSc2	pivo
a	a	k8xC	a
pivovarnictví	pivovarnictví	k1gNnSc2	pivovarnictví
<g/>
,	,	kIx,	,
konají	konat	k5eAaImIp3nP	konat
se	se	k3xPyFc4	se
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
</s>
</p>
<p>
<s>
Jazz	jazz	k1gInSc1	jazz
bez	bez	k7c2	bez
hranic	hranice	k1gFnPc2	hranice
–	–	k?	–
mezinárodní	mezinárodní	k2eAgInSc4d1	mezinárodní
festival	festival	k1gInSc4	festival
jazzu	jazz	k1gInSc2	jazz
<g/>
,	,	kIx,	,
koná	konat	k5eAaImIp3nS	konat
se	se	k3xPyFc4	se
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
</s>
</p>
<p>
<s>
==	==	k?	==
Školství	školství	k1gNnSc2	školství
==	==	k?	==
</s>
</p>
<p>
<s>
Plzeň	Plzeň	k1gFnSc1	Plzeň
je	být	k5eAaImIp3nS	být
krajské	krajský	k2eAgNnSc4d1	krajské
město	město	k1gNnSc4	město
se	s	k7c7	s
170	[number]	k4	170
tisíci	tisíc	k4xCgInPc7	tisíc
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
,	,	kIx,	,
čemuž	což	k3yRnSc3	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
i	i	k9	i
množství	množství	k1gNnSc1	množství
středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
vyšších	vysoký	k2eAgFnPc2d2	vyšší
a	a	k8xC	a
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
za	za	k7c7	za
nimiž	jenž	k3xRgMnPc7	jenž
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
sjíždějí	sjíždět	k5eAaImIp3nP	sjíždět
studenti	student	k1gMnPc1	student
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
kraje	kraj	k1gInSc2	kraj
i	i	k8xC	i
dalších	další	k2eAgNnPc2d1	další
míst	místo	k1gNnPc2	místo
Česka	Česko	k1gNnSc2	Česko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Střední	střední	k2eAgNnSc1d1	střední
školství	školství	k1gNnSc1	školství
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
řada	řada	k1gFnSc1	řada
středních	střední	k2eAgFnPc2d1	střední
odborných	odborný	k2eAgFnPc2d1	odborná
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
učilišť	učiliště	k1gNnPc2	učiliště
<g/>
,	,	kIx,	,
jakož	jakož	k8xC	jakož
i	i	k9	i
6	[number]	k4	6
gymnázií	gymnázium	k1gNnPc2	gymnázium
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Veřejné	veřejný	k2eAgFnPc1d1	veřejná
školy	škola	k1gFnPc1	škola
</s>
</p>
<p>
<s>
Gymnázium	gymnázium	k1gNnSc1	gymnázium
Plzeň	Plzeň	k1gFnSc1	Plzeň
(	(	kIx(	(
<g/>
Mikulášské	mikulášský	k2eAgNnSc1d1	Mikulášské
gymnázium	gymnázium	k1gNnSc1	gymnázium
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Masarykovo	Masarykův	k2eAgNnSc1d1	Masarykovo
gymnázium	gymnázium	k1gNnSc1	gymnázium
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Gymnázium	gymnázium	k1gNnSc1	gymnázium
Luďka	Luděk	k1gMnSc2	Luděk
Pika	Pik	k1gMnSc2	Pik
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sportovní	sportovní	k2eAgNnSc1d1	sportovní
gymnázium	gymnázium	k1gNnSc1	gymnázium
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SOŠ	SOŠ	kA	SOŠ
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
užitého	užitý	k2eAgNnSc2d1	užité
umění	umění	k1gNnSc2	umění
a	a	k8xC	a
designu	design	k1gInSc2	design
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Integrovaná	integrovaný	k2eAgFnSc1d1	integrovaná
střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
živnostenská	živnostenský	k2eAgFnSc1d1	Živnostenská
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
výroby	výroba	k1gFnSc2	výroba
a	a	k8xC	a
služeb	služba	k1gFnPc2	služba
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
informatiky	informatika	k1gFnSc2	informatika
a	a	k8xC	a
finančních	finanční	k2eAgFnPc2d1	finanční
služeb	služba	k1gFnPc2	služba
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SOU	sou	k1gInSc4	sou
elektrotechnické	elektrotechnický	k2eAgFnPc1d1	elektrotechnická
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Konzervatoř	konzervatoř	k1gFnSc1	konzervatoř
Plzeň	Plzeň	k1gFnSc1	Plzeň
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Obchodní	obchodní	k2eAgFnSc1d1	obchodní
akademie	akademie	k1gFnSc1	akademie
Plzeň	Plzeň	k1gFnSc1	Plzeň
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hotelová	hotelový	k2eAgFnSc1d1	hotelová
škola	škola	k1gFnSc1	škola
Plzeň	Plzeň	k1gFnSc1	Plzeň
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SOU	sou	k1gInSc1	sou
stavební	stavební	k2eAgInSc1d1	stavební
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SPŠ	SPŠ	kA	SPŠ
elektrotechnická	elektrotechnický	k2eAgFnSc1d1	elektrotechnická
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SPŠ	SPŠ	kA	SPŠ
stavební	stavební	k2eAgInSc1d1	stavební
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SPŠ	SPŠ	kA	SPŠ
dopravní	dopravní	k2eAgInSc1d1	dopravní
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SPŠ	SPŠ	kA	SPŠ
strojnická	strojnický	k2eAgFnSc1d1	strojnická
a	a	k8xC	a
SOŠ	SOŠ	kA	SOŠ
profesora	profesor	k1gMnSc2	profesor
Švejcara	Švejcar	k1gMnSc2	Švejcar
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Církevní	církevní	k2eAgFnSc1d1	církevní
škola	škola	k1gFnSc1	škola
</s>
</p>
<p>
<s>
Církevní	církevní	k2eAgNnSc1d1	církevní
gymnázium	gymnázium	k1gNnSc1	gymnázium
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Soukromé	soukromý	k2eAgFnPc1d1	soukromá
školy	škola	k1gFnPc1	škola
</s>
</p>
<p>
<s>
Gymnázium	gymnázium	k1gNnSc1	gymnázium
Františka	František	k1gMnSc2	František
Křižíka	Křižík	k1gMnSc2	Křižík
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Soukromá	soukromý	k2eAgFnSc1d1	soukromá
střední	střední	k2eAgFnSc1d1	střední
uměleckoprůmyslová	uměleckoprůmyslový	k2eAgFnSc1d1	uměleckoprůmyslová
škola	škola	k1gFnSc1	škola
Zámeček	zámeček	k1gInSc1	zámeček
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Plzeňská	plzeňský	k2eAgFnSc1d1	Plzeňská
obchodní	obchodní	k2eAgFnSc1d1	obchodní
akademie	akademie	k1gFnSc1	akademie
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hotelová	hotelový	k2eAgFnSc1d1	hotelová
škola	škola	k1gFnSc1	škola
Plzeň	Plzeň	k1gFnSc1	Plzeň
–	–	k?	–
Akademie	akademie	k1gFnSc2	akademie
hotelnictví	hotelnictví	k1gNnSc2	hotelnictví
a	a	k8xC	a
cestovního	cestovní	k2eAgInSc2d1	cestovní
ruchu	ruch	k1gInSc2	ruch
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bezpečnostně	bezpečnostně	k6eAd1	bezpečnostně
právní	právní	k2eAgFnSc1d1	právní
akademie	akademie	k1gFnSc1	akademie
Plzeň	Plzeň	k1gFnSc1	Plzeň
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Sportovní	sportovní	k2eAgFnSc1d1	sportovní
a	a	k8xC	a
podnikatelská	podnikatelský	k2eAgFnSc1d1	podnikatelská
střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Vyšší	vysoký	k2eAgNnSc4d2	vyšší
odborné	odborný	k2eAgNnSc4d1	odborné
školství	školství	k1gNnSc4	školství
===	===	k?	===
</s>
</p>
<p>
<s>
VOŠ	VOŠ	kA	VOŠ
a	a	k8xC	a
SPŠ	SPŠ	kA	SPŠ
elektrotechnická	elektrotechnický	k2eAgFnSc1d1	elektrotechnická
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Střední	střední	k2eAgFnSc1d1	střední
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
VOŠ	VOŠ	kA	VOŠ
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
<g/>
,	,	kIx,	,
managementu	management	k1gInSc2	management
a	a	k8xC	a
veřejnosprávních	veřejnosprávní	k2eAgNnPc2d1	veřejnosprávní
studií	studio	k1gNnPc2	studio
<g/>
,	,	kIx,	,
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o	o	k7c4	o
(	(	kIx(	(
<g/>
web	web	k1gInSc4	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Vysoké	vysoký	k2eAgNnSc1d1	vysoké
školství	školství	k1gNnSc1	školství
===	===	k?	===
</s>
</p>
<p>
<s>
Západočeská	západočeský	k2eAgFnSc1d1	Západočeská
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Lékařská	lékařský	k2eAgFnSc1d1	lékařská
fakulta	fakulta	k1gFnSc1	fakulta
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgInPc1d1	Karlův
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Metropolitní	metropolitní	k2eAgFnSc1d1	metropolitní
univerzita	univerzita	k1gFnSc1	univerzita
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Plzeň	Plzeň	k1gFnSc1	Plzeň
je	být	k5eAaImIp3nS	být
důležitou	důležitý	k2eAgFnSc7d1	důležitá
dopravní	dopravní	k2eAgFnSc7d1	dopravní
křižovatkou	křižovatka	k1gFnSc7	křižovatka
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
význam	význam	k1gInSc1	význam
má	mít	k5eAaImIp3nS	mít
dnes	dnes	k6eAd1	dnes
dálnice	dálnice	k1gFnSc1	dálnice
D5	D5	k1gFnSc2	D5
mezi	mezi	k7c7	mezi
Prahou	Praha	k1gFnSc7	Praha
a	a	k8xC	a
Norimberkem	Norimberk	k1gInSc7	Norimberk
s	s	k7c7	s
dálničním	dálniční	k2eAgInSc7d1	dálniční
obchvatem	obchvat	k1gInSc7	obchvat
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc1d1	další
důležité	důležitý	k2eAgFnPc1d1	důležitá
silnice	silnice	k1gFnPc1	silnice
vedou	vést	k5eAaImIp3nP	vést
do	do	k7c2	do
Strakonic	Strakonice	k1gFnPc2	Strakonice
<g/>
,	,	kIx,	,
do	do	k7c2	do
Karlových	Karlův	k2eAgInPc2d1	Karlův
Varů	Vary	k1gInPc2	Vary
a	a	k8xC	a
Stříbra	stříbro	k1gNnSc2	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlavního	hlavní	k2eAgNnSc2d1	hlavní
nádraží	nádraží	k1gNnSc2	nádraží
vedou	vést	k5eAaImIp3nP	vést
železniční	železniční	k2eAgFnPc1d1	železniční
tratě	trať	k1gFnPc1	trať
na	na	k7c4	na
Rokycany	Rokycany	k1gInPc4	Rokycany
–	–	k?	–
Prahu	práh	k1gInSc2	práh
<g/>
,	,	kIx,	,
Mladotice	Mladotice	k1gFnSc1	Mladotice
–	–	k?	–
Žatec	Žatec	k1gInSc1	Žatec
<g/>
,	,	kIx,	,
Mariánské	mariánský	k2eAgFnPc1d1	Mariánská
Lázně	lázeň	k1gFnPc1	lázeň
–	–	k?	–
Cheb	Cheb	k1gInSc1	Cheb
–	–	k?	–
Schirnding	Schirnding	k1gInSc4	Schirnding
a	a	k8xC	a
Nürnberg	Nürnberg	k1gInSc4	Nürnberg
<g/>
,	,	kIx,	,
Nýřany	Nýřana	k1gFnPc4	Nýřana
–	–	k?	–
Domažlice	Domažlice	k1gFnPc4	Domažlice
–	–	k?	–
Cham	Cham	k1gMnSc1	Cham
a	a	k8xC	a
Regensburg	Regensburg	k1gMnSc1	Regensburg
<g/>
,	,	kIx,	,
na	na	k7c4	na
Klatovy	Klatovy	k1gInPc4	Klatovy
a	a	k8xC	a
na	na	k7c4	na
Nepomuk	Nepomuk	k1gInSc4	Nepomuk
–	–	k?	–
Strakonice	Strakonice	k1gFnPc4	Strakonice
–	–	k?	–
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Samozřejmostí	samozřejmost	k1gFnSc7	samozřejmost
je	být	k5eAaImIp3nS	být
i	i	k9	i
hustá	hustý	k2eAgFnSc1d1	hustá
síť	síť	k1gFnSc1	síť
městské	městský	k2eAgFnSc2d1	městská
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
s	s	k7c7	s
tramvajemi	tramvaj	k1gFnPc7	tramvaj
<g/>
,	,	kIx,	,
trolejbusy	trolejbus	k1gInPc1	trolejbus
a	a	k8xC	a
autobusy	autobus	k1gInPc1	autobus
<g/>
.	.	kIx.	.
</s>
<s>
Tramvajovou	tramvajový	k2eAgFnSc4d1	tramvajová
dopravu	doprava	k1gFnSc4	doprava
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
tři	tři	k4xCgFnPc4	tři
linky	linka	k1gFnPc4	linka
<g/>
.	.	kIx.	.
</s>
<s>
Linka	linka	k1gFnSc1	linka
č.	č.	k?	č.
1	[number]	k4	1
spojuje	spojovat	k5eAaImIp3nS	spojovat
Bolevec	Bolevec	k1gInSc4	Bolevec
a	a	k8xC	a
Slovany	Slovan	k1gInPc4	Slovan
<g/>
,	,	kIx,	,
linka	linka	k1gFnSc1	linka
č.	č.	k?	č.
2	[number]	k4	2
Světovar	Světovar	k1gInSc4	Světovar
a	a	k8xC	a
Skvrňany	Skvrňan	k1gMnPc4	Skvrňan
a	a	k8xC	a
nejvytíženější	vytížený	k2eAgFnSc1d3	nejvytíženější
linka	linka	k1gFnSc1	linka
č.	č.	k?	č.
4	[number]	k4	4
Košutku	Košutek	k1gInSc2	Košutek
a	a	k8xC	a
Bory	bor	k1gInPc4	bor
<g/>
.	.	kIx.	.
</s>
<s>
Trolejbusová	trolejbusový	k2eAgFnSc1d1	trolejbusová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
síť	síť	k1gFnSc4	síť
tramvají	tramvaj	k1gFnPc2	tramvaj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
je	být	k5eAaImIp3nS	být
celkem	celkem	k6eAd1	celkem
9	[number]	k4	9
trolejbusových	trolejbusový	k2eAgFnPc2d1	trolejbusová
linek	linka	k1gFnPc2	linka
<g/>
.	.	kIx.	.
</s>
<s>
Městské	městský	k2eAgInPc1d1	městský
autobusy	autobus	k1gInPc1	autobus
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
hlavně	hlavně	k9	hlavně
spojení	spojení	k1gNnSc4	spojení
velmi	velmi	k6eAd1	velmi
vzdálených	vzdálený	k2eAgFnPc2d1	vzdálená
částí	část	k1gFnPc2	část
města	město	k1gNnSc2	město
a	a	k8xC	a
jako	jako	k9	jako
příměstské	příměstský	k2eAgInPc1d1	příměstský
spoje	spoj	k1gInPc1	spoj
zajíždí	zajíždět	k5eAaImIp3nP	zajíždět
i	i	k9	i
do	do	k7c2	do
některých	některý	k3yIgFnPc2	některý
obcí	obec	k1gFnPc2	obec
za	za	k7c7	za
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Veřejné	veřejný	k2eAgNnSc4d1	veřejné
vnitrostátní	vnitrostátní	k2eAgNnSc4d1	vnitrostátní
a	a	k8xC	a
neveřejné	veřejný	k2eNgNnSc4d1	neveřejné
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
letiště	letiště	k1gNnSc4	letiště
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
11	[number]	k4	11
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
,	,	kIx,	,
v	v	k7c6	v
nedaleké	daleký	k2eNgFnSc6d1	nedaleká
obci	obec	k1gFnSc6	obec
Líně	líně	k6eAd1	líně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sport	sport	k1gInSc1	sport
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
působí	působit	k5eAaImIp3nS	působit
fotbalový	fotbalový	k2eAgInSc1d1	fotbalový
klub	klub	k1gInSc1	klub
FC	FC	kA	FC
Viktoria	Viktoria	k1gFnSc1	Viktoria
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
fotbalové	fotbalový	k2eAgFnSc6d1	fotbalová
soutěži	soutěž	k1gFnSc6	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
zde	zde	k6eAd1	zde
působí	působit	k5eAaImIp3nS	působit
hokejový	hokejový	k2eAgInSc1d1	hokejový
klub	klub	k1gInSc1	klub
HC	HC	kA	HC
Škoda	škoda	k1gFnSc1	škoda
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
hraje	hrát	k5eAaImIp3nS	hrát
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
hokejovou	hokejový	k2eAgFnSc4d1	hokejová
soutěž	soutěž	k1gFnSc4	soutěž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Osobnosti	osobnost	k1gFnPc1	osobnost
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Rodáci	rodák	k1gMnPc5	rodák
===	===	k?	===
</s>
</p>
<p>
<s>
Oskar	Oskar	k1gMnSc1	Oskar
Baum	Baum	k1gMnSc1	Baum
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
–	–	k?	–
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
kritik	kritik	k1gMnSc1	kritik
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Beneš	Beneš	k1gMnSc1	Beneš
(	(	kIx(	(
<g/>
*	*	kIx~	*
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotograf	fotograf	k1gMnSc1	fotograf
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
kardinál	kardinál	k1gMnSc1	kardinál
Beran	Beran	k1gMnSc1	Beran
(	(	kIx(	(
<g/>
1888	[number]	k4	1888
<g/>
–	–	k?	–
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
pražský	pražský	k2eAgMnSc1d1	pražský
</s>
</p>
<p>
<s>
Hugo	Hugo	k1gMnSc1	Hugo
Boettinger	Boettinger	k1gMnSc1	Boettinger
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
–	–	k?	–
<g/>
1934	[number]	k4	1934
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
</s>
</p>
<p>
<s>
Blanka	Blanka	k1gFnSc1	Blanka
Bohdanová	Bohdanová	k1gFnSc1	Bohdanová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Felix	Felix	k1gMnSc1	Felix
le	le	k?	le
Breux	Breux	k1gInSc1	Breux
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1974	[number]	k4	1974
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
</s>
</p>
<p>
<s>
Rudolf	Rudolf	k1gMnSc1	Rudolf
Cortés	Cortésa	k1gFnPc2	Cortésa
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
–	–	k?	–
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
</s>
</p>
<p>
<s>
Ben	Ben	k1gInSc1	Ben
Cristovao	Cristovao	k6eAd1	Cristovao
(	(	kIx(	(
<g/>
*	*	kIx~	*
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Čech	Čech	k1gMnSc1	Čech
(	(	kIx(	(
<g/>
*	*	kIx~	*
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalový	fotbalový	k2eAgMnSc1d1	fotbalový
brankář	brankář	k1gMnSc1	brankář
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Čechura	Čechura	k1gMnSc1	Čechura
(	(	kIx(	(
<g/>
*	*	kIx~	*
1952	[number]	k4	1952
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
<g/>
,	,	kIx,	,
archivář	archivář	k1gMnSc1	archivář
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Černý	Černý	k1gMnSc1	Černý
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
–	–	k?	–
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
egyptolog	egyptolog	k1gMnSc1	egyptolog
</s>
</p>
<p>
<s>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Ebermann	Ebermann	k1gMnSc1	Ebermann
(	(	kIx(	(
<g/>
*	*	kIx~	*
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
<g/>
,	,	kIx,	,
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
1977	[number]	k4	1977
(	(	kIx(	(
<g/>
narozen	narozen	k2eAgMnSc1d1	narozen
ve	v	k7c6	v
Vochově	Vochova	k1gFnSc6	Vochova
u	u	k7c2	u
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kateřina	Kateřina	k1gFnSc1	Kateřina
Emmons	Emmonsa	k1gFnPc2	Emmonsa
(	(	kIx(	(
<g/>
*	*	kIx~	*
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
olympijská	olympijský	k2eAgFnSc1d1	olympijská
medailistka	medailistka	k1gFnSc1	medailistka
ve	v	k7c6	v
střelbě	střelba	k1gFnSc6	střelba
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Francouz	Francouz	k1gMnSc1	Francouz
(	(	kIx(	(
<g/>
*	*	kIx~	*
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
</s>
</p>
<p>
<s>
Jiřina	Jiřina	k1gFnSc1	Jiřina
Fuchsová	Fuchsová	k1gFnSc1	Fuchsová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
</s>
</p>
<p>
<s>
Gertrud	Gertrud	k1gMnSc1	Gertrud
Fussenegger	Fussenegger	k1gMnSc1	Fussenegger
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Gott	Gott	k1gMnSc1	Gott
(	(	kIx(	(
<g/>
*	*	kIx~	*
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpěvák	zpěvák	k1gMnSc1	zpěvák
</s>
</p>
<p>
<s>
Peter	Peter	k1gMnSc1	Peter
Grünberg	Grünberg	k1gMnSc1	Grünberg
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
–	–	k?	–
<g/>
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fyzik	fyzik	k1gMnSc1	fyzik
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Harant	Harant	k?	Harant
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
–	–	k?	–
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
matematik	matematik	k1gMnSc1	matematik
a	a	k8xC	a
geometr	geometr	k1gMnSc1	geometr
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
ČVUT	ČVUT	kA	ČVUT
</s>
</p>
<p>
<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Horníček	Horníček	k1gMnSc1	Horníček
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
</s>
</p>
<p>
<s>
Luboš	Luboš	k1gMnSc1	Luboš
Hruška	Hruška	k1gMnSc1	Hruška
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politický	politický	k2eAgMnSc1d1	politický
vězeň	vězeň	k1gMnSc1	vězeň
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
a	a	k8xC	a
tvůrce	tvůrce	k1gMnSc1	tvůrce
Meditační	meditační	k2eAgFnSc2d1	meditační
zahrady	zahrada	k1gFnSc2	zahrada
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Chochola	Chochola	k1gFnSc1	Chochola
(	(	kIx(	(
<g/>
1893	[number]	k4	1893
<g/>
–	–	k?	–
<g/>
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
<g/>
,	,	kIx,	,
urbanista	urbanista	k1gMnSc1	urbanista
<g/>
,	,	kIx,	,
nábytkář	nábytkář	k1gMnSc1	nábytkář
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Janovický	janovický	k2eAgMnSc1d1	janovický
(	(	kIx(	(
<g/>
*	*	kIx~	*
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
</s>
</p>
<p>
<s>
Filip	Filip	k1gMnSc1	Filip
Jícha	jícha	k1gFnSc1	jícha
(	(	kIx(	(
<g/>
*	*	kIx~	*
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
házenkář	házenkář	k1gMnSc1	házenkář
<g/>
,	,	kIx,	,
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
házenkář	házenkář	k1gMnSc1	házenkář
světa	svět	k1gInSc2	svět
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
dvojnásobný	dvojnásobný	k2eAgMnSc1d1	dvojnásobný
vítěz	vítěz	k1gMnSc1	vítěz
házenkářské	házenkářský	k2eAgFnSc2d1	házenkářská
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
(	(	kIx(	(
<g/>
s	s	k7c7	s
THW	THW	kA	THW
Kiel	Kiel	k1gInSc1	Kiel
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Milan	Milan	k1gMnSc1	Milan
Kajkl	Kajkl	k1gInSc1	Kajkl
(	(	kIx(	(
<g/>
1950	[number]	k4	1950
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
<g/>
,	,	kIx,	,
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
1976	[number]	k4	1976
a	a	k8xC	a
1977	[number]	k4	1977
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Hugo	Hugo	k1gMnSc1	Hugo
Kepka	Kepka	k1gMnSc1	Kepka
(	(	kIx(	(
<g/>
1869	[number]	k4	1869
<g/>
–	–	k?	–
<g/>
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
</s>
</p>
<p>
<s>
Milan	Milan	k1gMnSc1	Milan
Knížák	Knížák	k1gMnSc1	Knížák
(	(	kIx(	(
<g/>
*	*	kIx~	*
1940	[number]	k4	1940
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výtvarník	výtvarník	k1gMnSc1	výtvarník
</s>
</p>
<p>
<s>
Bohumil	Bohumil	k1gMnSc1	Bohumil
Konečný	Konečný	k1gMnSc1	Konečný
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
kreslíř	kreslíř	k1gMnSc1	kreslíř
<g/>
,	,	kIx,	,
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Kovářík	kovářík	k1gMnSc1	kovářík
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
–	–	k?	–
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Kovařík	Kovařík	k1gMnSc1	Kovařík
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
–	–	k?	–
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
akademický	akademický	k2eAgMnSc1d1	akademický
malíř	malíř	k1gMnSc1	malíř
</s>
</p>
<p>
<s>
Jana	Jana	k1gFnSc1	Jana
Kovaříková	Kovaříková	k1gFnSc1	Kovaříková
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
–	–	k?	–
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Milan	Milan	k1gMnSc1	Milan
Kraft	Kraft	k1gMnSc1	Kraft
(	(	kIx(	(
<g/>
*	*	kIx~	*
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Krásný	krásný	k2eAgMnSc1d1	krásný
(	(	kIx(	(
<g/>
1865	[number]	k4	1865
<g/>
–	–	k?	–
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Kreuzmann	Kreuzmann	k1gMnSc1	Kreuzmann
starší	starší	k1gMnSc1	starší
(	(	kIx(	(
<g/>
1895	[number]	k4	1895
<g/>
–	–	k?	–
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Kučera	Kučera	k1gMnSc1	Kučera
(	(	kIx(	(
<g/>
*	*	kIx~	*
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Langmajer	Langmajer	k1gMnSc1	Langmajer
(	(	kIx(	(
<g/>
*	*	kIx~	*
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
držitel	držitel	k1gMnSc1	držitel
Ceny	cena	k1gFnSc2	cena
Thálie	Thálie	k1gFnSc2	Thálie
pro	pro	k7c4	pro
umělce	umělec	k1gMnPc4	umělec
do	do	k7c2	do
33	[number]	k4	33
let	léto	k1gNnPc2	léto
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
</s>
</p>
<p>
<s>
Gottfried	Gottfried	k1gMnSc1	Gottfried
Lindauer	Lindauer	k1gMnSc1	Lindauer
(	(	kIx(	(
<g/>
Bohumír	Bohumír	k1gMnSc1	Bohumír
Lindauer	Lindauer	k1gMnSc1	Lindauer
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1839	[number]	k4	1839
<g/>
–	–	k?	–
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
novozélandský	novozélandský	k2eAgMnSc1d1	novozélandský
malíř	malíř	k1gMnSc1	malíř
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Mandl	mandl	k1gInSc1	mandl
(	(	kIx(	(
<g/>
1874	[number]	k4	1874
<g/>
–	–	k?	–
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
</s>
</p>
<p>
<s>
Matouš	Matouš	k1gMnSc1	Matouš
Mandl	mandl	k1gInSc1	mandl
(	(	kIx(	(
<g/>
1865	[number]	k4	1865
<g/>
–	–	k?	–
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
a	a	k8xC	a
purkmistr	purkmistr	k1gMnSc1	purkmistr
města	město	k1gNnSc2	město
</s>
</p>
<p>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Matzner	Matzner	k1gMnSc1	Matzner
(	(	kIx(	(
<g/>
1944	[number]	k4	1944
<g/>
–	–	k?	–
<g/>
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
publicista	publicista	k1gMnSc1	publicista
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
producent	producent	k1gMnSc1	producent
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
dramaturg	dramaturg	k1gMnSc1	dramaturg
</s>
</p>
<p>
<s>
Leo	Leo	k1gMnSc1	Leo
Meisl	Meisl	k1gInSc1	Meisl
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
–	–	k?	–
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Pech	Pech	k1gMnSc1	Pech
mladší	mladý	k2eAgMnSc1d2	mladší
(	(	kIx(	(
<g/>
*	*	kIx~	*
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rallyeový	rallyeový	k2eAgMnSc1d1	rallyeový
jezdec	jezdec	k1gMnSc1	jezdec
<g/>
,	,	kIx,	,
pětinásobný	pětinásobný	k2eAgMnSc1d1	pětinásobný
mistr	mistr	k1gMnSc1	mistr
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
dvojnásobný	dvojnásobný	k2eAgMnSc1d1	dvojnásobný
mistr	mistr	k1gMnSc1	mistr
Slovenska	Slovensko	k1gNnSc2	Slovensko
</s>
</p>
<p>
<s>
Jindřich	Jindřich	k1gMnSc1	Jindřich
Plachta	plachta	k1gFnSc1	plachta
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
<g/>
–	–	k?	–
<g/>
1951	[number]	k4	1951
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
a	a	k8xC	a
komik	komik	k1gMnSc1	komik
</s>
</p>
<p>
<s>
Ignác	Ignác	k1gMnSc1	Ignác
František	František	k1gMnSc1	František
Platzer	Platzer	k1gMnSc1	Platzer
(	(	kIx(	(
<g/>
1717	[number]	k4	1717
<g/>
–	–	k?	–
<g/>
1787	[number]	k4	1787
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sochař	sochař	k1gMnSc1	sochař
a	a	k8xC	a
řezbář	řezbář	k1gMnSc1	řezbář
</s>
</p>
<p>
<s>
Marie	Marie	k1gFnSc1	Marie
Rosůlková	Rosůlková	k1gFnSc1	Rosůlková
(	(	kIx(	(
<g/>
1901	[number]	k4	1901
<g/>
–	–	k?	–
<g/>
1993	[number]	k4	1993
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Samiec	Samiec	k1gMnSc1	Samiec
(	(	kIx(	(
<g/>
*	*	kIx~	*
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
akordeonista	akordeonista	k1gMnSc1	akordeonista
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Skála	Skála	k1gMnSc1	Skála
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
první	první	k4xOgFnSc2	první
záchytné	záchytný	k2eAgFnSc2d1	záchytná
stanice	stanice	k1gFnSc2	stanice
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Stelzer	Stelzer	k1gMnSc1	Stelzer
(	(	kIx(	(
<g/>
1815	[number]	k4	1815
<g/>
–	–	k?	–
<g/>
1894	[number]	k4	1894
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plzeňský	plzeňský	k2eAgMnSc1d1	plzeňský
stavitel	stavitel	k1gMnSc1	stavitel
(	(	kIx(	(
<g/>
Měšťanský	měšťanský	k2eAgInSc1d1	měšťanský
pivovar	pivovar	k1gInSc1	pivovar
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
synagoga	synagoga	k1gFnSc1	synagoga
<g/>
,	,	kIx,	,
Saský	saský	k2eAgInSc1d1	saský
<g/>
/	/	kIx~	/
<g/>
Rooseveltův	Rooseveltův	k2eAgInSc1d1	Rooseveltův
most	most	k1gInSc1	most
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Straka	Straka	k1gMnSc1	Straka
(	(	kIx(	(
<g/>
*	*	kIx~	*
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
<g/>
,	,	kIx,	,
olympijský	olympijský	k2eAgMnSc1d1	olympijský
vítěz	vítěz	k1gMnSc1	vítěz
1998	[number]	k4	1998
a	a	k8xC	a
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
2005	[number]	k4	2005
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Suchý	Suchý	k1gMnSc1	Suchý
(	(	kIx(	(
<g/>
*	*	kIx~	*
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
divadelník	divadelník	k1gMnSc1	divadelník
<g/>
,	,	kIx,	,
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
,	,	kIx,	,
textař	textař	k1gMnSc1	textař
a	a	k8xC	a
básník	básník	k1gMnSc1	básník
</s>
</p>
<p>
<s>
Ladislav	Ladislav	k1gMnSc1	Ladislav
Sutnar	Sutnar	k1gMnSc1	Sutnar
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
–	–	k?	–
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
designér	designér	k1gMnSc1	designér
</s>
</p>
<p>
<s>
Petr	Petr	k1gMnSc1	Petr
Sýkora	Sýkora	k1gMnSc1	Sýkora
(	(	kIx(	(
<g/>
*	*	kIx~	*
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hokejista	hokejista	k1gMnSc1	hokejista
<g/>
,	,	kIx,	,
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
1999	[number]	k4	1999
a	a	k8xC	a
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
vítěz	vítěz	k1gMnSc1	vítěz
Stanley	Stanlea	k1gFnSc2	Stanlea
Cupu	cup	k1gInSc2	cup
1999	[number]	k4	1999
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
a	a	k8xC	a
2008	[number]	k4	2008
<g/>
/	/	kIx~	/
<g/>
2009	[number]	k4	2009
</s>
</p>
<p>
<s>
Fritz	Fritz	k1gInSc1	Fritz
von	von	k1gInSc1	von
Scholz	Scholz	k1gInSc1	Scholz
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
–	–	k?	–
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
generálporučík	generálporučík	k1gMnSc1	generálporučík
Waffen-SS	Waffen-SS	k1gMnSc1	Waffen-SS
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Soukup	Soukup	k1gMnSc1	Soukup
(	(	kIx(	(
<g/>
*	*	kIx~	*
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
</s>
</p>
<p>
<s>
Eva	Eva	k1gFnSc1	Eva
Šenková	Šenková	k1gFnSc1	Šenková
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
a	a	k8xC	a
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
</s>
</p>
<p>
<s>
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Šíp	Šíp	k1gMnSc1	Šíp
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
–	–	k?	–
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sochař	sochař	k1gMnSc1	sochař
</s>
</p>
<p>
<s>
Emil	Emil	k1gMnSc1	Emil
Škoda	Škoda	k1gMnSc1	Škoda
(	(	kIx(	(
<g/>
1839	[number]	k4	1839
<g/>
–	–	k?	–
<g/>
1900	[number]	k4	1900
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
podniku	podnik	k1gInSc2	podnik
Škoda	Škoda	k1gMnSc1	Škoda
</s>
</p>
<p>
<s>
Růžena	Růžena	k1gFnSc1	Růžena
Šlemrová	Šlemrová	k1gFnSc1	Šlemrová
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
–	–	k?	–
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Šrom	Šrom	k1gMnSc1	Šrom
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
–	–	k?	–
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
publicista	publicista	k1gMnSc1	publicista
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Trávníček	Trávníček	k1gMnSc1	Trávníček
(	(	kIx(	(
<g/>
*	*	kIx~	*
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
horolezec	horolezec	k1gMnSc1	horolezec
a	a	k8xC	a
cestovatel	cestovatel	k1gMnSc1	cestovatel
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Trnka	Trnka	k1gMnSc1	Trnka
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
–	–	k?	–
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
výtvarník	výtvarník	k1gMnSc1	výtvarník
<g/>
,	,	kIx,	,
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
animovaných	animovaný	k2eAgInPc2d1	animovaný
filmů	film	k1gInPc2	film
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Trojan	Trojan	k1gMnSc1	Trojan
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
<g/>
–	–	k?	–
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
pedagog	pedagog	k1gMnSc1	pedagog
</s>
</p>
<p>
<s>
Ota	Ota	k1gMnSc1	Ota
Ulč	Ulč	k1gMnSc1	Ulč
(	(	kIx(	(
<g/>
*	*	kIx~	*
1930	[number]	k4	1930
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
politologie	politologie	k1gFnSc2	politologie
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Větrovec	Větrovec	k1gMnSc1	Větrovec
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
–	–	k?	–
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vogel	Vogel	k1gMnSc1	Vogel
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
–	–	k?	–
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dirigent	dirigent	k1gMnSc1	dirigent
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Votlučka	Votlučka	k1gMnSc1	Votlučka
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
–	–	k?	–
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
<g/>
,	,	kIx,	,
ilustrátor	ilustrátor	k1gMnSc1	ilustrátor
</s>
</p>
<p>
<s>
Marek	Marek	k1gMnSc1	Marek
Wollner	Wollner	k1gMnSc1	Wollner
(	(	kIx(	(
<g/>
*	*	kIx~	*
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
</s>
</p>
<p>
<s>
Miroslav	Miroslav	k1gMnSc1	Miroslav
Zikmund	Zikmund	k1gMnSc1	Zikmund
(	(	kIx(	(
<g/>
*	*	kIx~	*
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cestovatel	cestovatel	k1gMnSc1	cestovatel
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
</s>
</p>
<p>
<s>
===	===	k?	===
Studenti	student	k1gMnPc1	student
===	===	k?	===
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Linda	Linda	k1gFnSc1	Linda
(	(	kIx(	(
<g/>
1789	[number]	k4	1789
<g/>
–	–	k?	–
<g/>
1834	[number]	k4	1834
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pravděpodobný	pravděpodobný	k2eAgMnSc1d1	pravděpodobný
autor	autor	k1gMnSc1	autor
Rukopisu	rukopis	k1gInSc2	rukopis
královédvorského	královédvorský	k2eAgNnSc2d1	královédvorské
a	a	k8xC	a
Zelenohorského	zelenohorský	k2eAgNnSc2d1	zelenohorský
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Nedvěd	Nedvěd	k1gMnSc1	Nedvěd
(	(	kIx(	(
<g/>
*	*	kIx~	*
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotbalista	fotbalista	k1gMnSc1	fotbalista
</s>
</p>
<p>
<s>
Bedřich	Bedřich	k1gMnSc1	Bedřich
Smetana	Smetana	k1gMnSc1	Smetana
(	(	kIx(	(
<g/>
1824	[number]	k4	1824
<g/>
–	–	k?	–
<g/>
1884	[number]	k4	1884
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
</s>
</p>
<p>
<s>
===	===	k?	===
Ostatní	ostatní	k2eAgFnPc1d1	ostatní
osobnosti	osobnost	k1gFnPc1	osobnost
Plzně	Plzeň	k1gFnSc2	Plzeň
===	===	k?	===
</s>
</p>
<p>
<s>
Jakub	Jakub	k1gMnSc1	Jakub
Auguston	Auguston	k1gInSc1	Auguston
(	(	kIx(	(
<g/>
1668	[number]	k4	1668
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
–	–	k?	–
<g/>
1735	[number]	k4	1735
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
barokní	barokní	k2eAgMnSc1d1	barokní
architekt	architekt	k1gMnSc1	architekt
a	a	k8xC	a
stavitel	stavitel	k1gMnSc1	stavitel
</s>
</p>
<p>
<s>
Martin	Martin	k1gMnSc1	Martin
Kopecký	Kopecký	k1gMnSc1	Kopecký
(	(	kIx(	(
<g/>
1777	[number]	k4	1777
<g/>
–	–	k?	–
<g/>
1854	[number]	k4	1854
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
purkmistr	purkmistr	k1gMnSc1	purkmistr
královského	královský	k2eAgNnSc2d1	královské
města	město	k1gNnSc2	město
Plzně	Plzeň	k1gFnSc2	Plzeň
v	v	k7c6	v
letech	let	k1gInPc6	let
1828	[number]	k4	1828
<g/>
–	–	k?	–
<g/>
1850	[number]	k4	1850
</s>
</p>
<p>
<s>
Antonín	Antonín	k1gMnSc1	Antonín
Kotěra	Kotěra	k1gFnSc1	Kotěra
(	(	kIx(	(
<g/>
1844	[number]	k4	1844
<g/>
–	–	k?	–
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
a	a	k8xC	a
v	v	k7c6	v
letech	let	k1gInPc6	let
1886	[number]	k4	1886
<g/>
–	–	k?	–
<g/>
1907	[number]	k4	1907
ředitel	ředitel	k1gMnSc1	ředitel
první	první	k4xOgFnSc2	první
české	český	k2eAgFnSc2d1	Česká
obchodní	obchodní	k2eAgFnSc2d1	obchodní
školy	škola	k1gFnSc2	škola
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Křižík	Křižík	k1gMnSc1	Křižík
(	(	kIx(	(
<g/>
1847	[number]	k4	1847
<g/>
–	–	k?	–
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vynálezce	vynálezce	k1gMnSc1	vynálezce
a	a	k8xC	a
podnikatel	podnikatel	k1gMnSc1	podnikatel
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Mutinský	Mutinský	k2eAgMnSc1d1	Mutinský
(	(	kIx(	(
<g/>
*	*	kIx~	*
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filmař	filmař	k1gMnSc1	filmař
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
a	a	k8xC	a
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
grafického	grafický	k2eAgNnSc2d1	grafické
řešení	řešení	k1gNnSc2	řešení
na	na	k7c6	na
pomníku	pomník	k1gInSc6	pomník
Díky	díky	k7c3	díky
<g/>
,	,	kIx,	,
Ameriko	Amerika	k1gFnSc5	Amerika
<g/>
!	!	kIx.	!
</s>
<s>
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
</s>
</p>
<p>
<s>
Helena	Helena	k1gFnSc1	Helena
Růžičková	Růžičková	k1gFnSc1	Růžičková
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Schwarz	Schwarz	k1gMnSc1	Schwarz
(	(	kIx(	(
<g/>
1840	[number]	k4	1840
<g/>
–	–	k?	–
<g/>
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
úředník	úředník	k1gMnSc1	úředník
<g/>
,	,	kIx,	,
propagátor	propagátor	k1gMnSc1	propagátor
české	český	k2eAgFnSc2d1	Česká
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
publikací	publikace	k1gFnPc2	publikace
o	o	k7c6	o
veřejné	veřejný	k2eAgFnSc6d1	veřejná
správě	správa	k1gFnSc6	správa
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Skupa	skupa	k1gFnSc1	skupa
(	(	kIx(	(
<g/>
1892	[number]	k4	1892
<g/>
–	–	k?	–
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
Spejbla	Spejbl	k1gMnSc2	Spejbl
a	a	k8xC	a
Hurvínka	Hurvínek	k1gMnSc2	Hurvínek
</s>
</p>
<p>
<s>
Radomír	Radomír	k1gMnSc1	Radomír
Šimůnek	Šimůnek	k1gMnSc1	Šimůnek
starší	starší	k1gMnSc1	starší
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cyklokrosař	cyklokrosař	k1gMnSc1	cyklokrosař
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Kajetán	Kajetán	k1gMnSc1	Kajetán
Tyl	Tyl	k1gMnSc1	Tyl
(	(	kIx(	(
<g/>
1808	[number]	k4	1808
<g/>
–	–	k?	–
<g/>
1856	[number]	k4	1856
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
divadelník	divadelník	k1gMnSc1	divadelník
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
překladatelNa	překladatelNa	k6eAd1	překladatelNa
plzeňském	plzeňský	k2eAgInSc6d1	plzeňský
Ústředním	ústřední	k2eAgInSc6d1	ústřední
hřbitově	hřbitov	k1gInSc6	hřbitov
jsou	být	k5eAaImIp3nP	být
pochováni	pochován	k2eAgMnPc1d1	pochován
například	například	k6eAd1	například
Jiří	Jiří	k1gMnSc1	Jiří
Trnka	Trnka	k1gMnSc1	Trnka
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Skupa	skupa	k1gFnSc1	skupa
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Klostermann	Klostermann	k1gMnSc1	Klostermann
či	či	k8xC	či
Augustin	Augustin	k1gMnSc1	Augustin
Němejc	Němejc	k1gFnSc1	Němejc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dřívějších	dřívější	k2eAgFnPc6d1	dřívější
dobách	doba	k1gFnPc6	doba
se	se	k3xPyFc4	se
však	však	k9	však
plzeňská	plzeňský	k2eAgFnSc1d1	Plzeňská
elita	elita	k1gFnSc1	elita
nechávala	nechávat	k5eAaImAgFnS	nechávat
pohřbívat	pohřbívat	k5eAaImF	pohřbívat
také	také	k9	také
na	na	k7c6	na
Mikulášském	mikulášský	k2eAgInSc6d1	mikulášský
hřbitově	hřbitov	k1gInSc6	hřbitov
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byli	být	k5eAaImAgMnP	být
pohřbeni	pohřbít	k5eAaPmNgMnP	pohřbít
Emil	Emil	k1gMnSc1	Emil
Škoda	Škoda	k1gMnSc1	Škoda
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Kajetán	Kajetán	k1gMnSc1	Kajetán
Tyl	Tyl	k1gMnSc1	Tyl
nebo	nebo	k8xC	nebo
purkmistr	purkmistr	k1gMnSc1	purkmistr
Martin	Martin	k1gMnSc1	Martin
Kopecký	Kopecký	k1gMnSc1	Kopecký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Plzeň	Plzeň	k1gFnSc1	Plzeň
má	mít	k5eAaImIp3nS	mít
následující	následující	k2eAgNnPc4d1	následující
partnerská	partnerský	k2eAgNnPc4d1	partnerské
města	město	k1gNnPc4	město
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Limoges	Limoges	k1gInSc1	Limoges
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
</s>
</p>
<p>
<s>
Lutych	Lutych	k1gInSc1	Lutych
<g/>
,	,	kIx,	,
Belgie	Belgie	k1gFnSc1	Belgie
</s>
</p>
<p>
<s>
Řezno	Řezno	k1gNnSc1	Řezno
(	(	kIx(	(
<g/>
Regensburg	Regensburg	k1gInSc1	Regensburg
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
</s>
</p>
<p>
<s>
Birmingham	Birmingham	k1gInSc1	Birmingham
<g/>
,	,	kIx,	,
USA	USA	kA	USA
</s>
</p>
<p>
<s>
Takasaki	Takasaki	k1gNnSc1	Takasaki
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
</s>
</p>
<p>
<s>
Winterthur	Winterthur	k1gMnSc1	Winterthur
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
</s>
</p>
<p>
<s>
Žilina	Žilina	k1gFnSc1	Žilina
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc1	Slovensko
</s>
</p>
<p>
<s>
Reykjavík	Reykjavík	k1gInSc1	Reykjavík
<g/>
,	,	kIx,	,
Island	Island	k1gInSc1	Island
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
HRUŠKA	Hruška	k1gMnSc1	Hruška
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
pamětní	pamětní	k2eAgFnSc2d1	pamětní
král	král	k1gMnSc1	král
<g/>
.	.	kIx.	.
krajského	krajský	k2eAgNnSc2d1	krajské
města	město	k1gNnSc2	město
Plzně	Plzeň	k1gFnSc2	Plzeň
od	od	k7c2	od
roku	rok	k1gInSc2	rok
775	[number]	k4	775
až	až	k9	až
1870	[number]	k4	1870
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nákladem	náklad	k1gInSc7	náklad
dědiců	dědic	k1gMnPc2	dědic
Hruškových	Hruškových	k2eAgInPc1d1	Hruškových
<g/>
,	,	kIx,	,
1883	[number]	k4	1883
<g/>
.	.	kIx.	.
1125	[number]	k4	1125
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kolektiv	kolektiv	k1gInSc1	kolektiv
autorů	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
města	město	k1gNnSc2	město
Plzně	Plzeň	k1gFnSc2	Plzeň
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
:	:	kIx,	:
Statutární	statutární	k2eAgNnSc1d1	statutární
město	město	k1gNnSc1	město
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
2016	[number]	k4	2016
a	a	k8xC	a
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
3	[number]	k4	3
svazky	svazek	k1gInPc4	svazek
(	(	kIx(	(
<g/>
884	[number]	k4	884
<g/>
,	,	kIx,	,
913	[number]	k4	913
a	a	k8xC	a
158	[number]	k4	158
s.	s.	k?	s.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87911	[number]	k4	87911
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kolektiv	kolektiv	k1gInSc1	kolektiv
autorů	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Průvodce	průvodce	k1gMnSc4	průvodce
architekturou	architektura	k1gFnSc7	architektura
města	město	k1gNnSc2	město
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
:	:	kIx,	:
Nava	Nava	k?	Nava
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
335	[number]	k4	335
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7211	[number]	k4	7211
<g/>
-	-	kIx~	-
<g/>
433	[number]	k4	433
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Plzeň	Plzeň	k1gFnSc1	Plzeň
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Plzeň	Plzeň	k1gFnSc1	Plzeň
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Plzeň	Plzeň	k1gFnSc1	Plzeň
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Plzeň	Plzeň	k1gFnSc1	Plzeň
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Plzeň	Plzeň	k1gFnSc1	Plzeň
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Plzeň	Plzeň	k1gFnSc1	Plzeň
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Mapový	mapový	k2eAgInSc1d1	mapový
portál	portál	k1gInSc1	portál
města	město	k1gNnSc2	město
Plzně	Plzeň	k1gFnSc2	Plzeň
</s>
</p>
<p>
<s>
Neoficiální	oficiální	k2eNgFnPc4d1	neoficiální
stránky	stránka	k1gFnPc4	stránka
o	o	k7c6	o
Plzni	Plzeň	k1gFnSc6	Plzeň
</s>
</p>
<p>
<s>
RegionPlzen	RegionPlzen	k2eAgInSc1d1	RegionPlzen
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
zpravodajský	zpravodajský	k2eAgInSc4d1	zpravodajský
portál	portál	k1gInSc4	portál
</s>
</p>
<p>
<s>
Virtuální	virtuální	k2eAgFnPc1d1	virtuální
prohlídky	prohlídka	k1gFnPc1	prohlídka
města	město	k1gNnSc2	město
Plzeň	Plzeň	k1gFnSc1	Plzeň
</s>
</p>
<p>
<s>
Plzeň	Plzeň	k1gFnSc1	Plzeň
2015	[number]	k4	2015
hlavní	hlavní	k2eAgFnSc7d1	hlavní
město	město	k1gNnSc4	město
kultury	kultura	k1gFnSc2	kultura
</s>
</p>
<p>
<s>
Pilsna	Pilsen	k2eAgFnSc1d1	Pilsen
Digitalis	Digitalis	k1gFnSc1	Digitalis
–	–	k?	–
historické	historický	k2eAgFnPc4d1	historická
noviny	novina	k1gFnPc4	novina
<g/>
,	,	kIx,	,
časopisy	časopis	k1gInPc4	časopis
<g/>
,	,	kIx,	,
ročenky	ročenka	k1gFnPc4	ročenka
<g/>
,	,	kIx,	,
monografie	monografie	k1gFnPc4	monografie
<g/>
,	,	kIx,	,
fotografie	fotografia	k1gFnPc4	fotografia
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgInPc4d1	divadelní
plakáty	plakát	k1gInPc4	plakát
<g/>
,	,	kIx,	,
plány	plán	k1gInPc1	plán
města	město	k1gNnSc2	město
i	i	k8xC	i
mapy	mapa	k1gFnPc4	mapa
vztahující	vztahující	k2eAgFnPc4d1	vztahující
se	se	k3xPyFc4	se
k	k	k7c3	k
Plzni	Plzeň	k1gFnSc3	Plzeň
volně	volně	k6eAd1	volně
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
</s>
</p>
