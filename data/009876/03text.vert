<p>
<s>
Chata	chata	k1gFnSc1	chata
Vrátna	Vrátn	k1gInSc2	Vrátn
je	být	k5eAaImIp3nS	být
horská	horský	k2eAgFnSc1d1	horská
chata	chata	k1gFnSc1	chata
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
Vrátné	vrátné	k2eAgFnSc2d1	vrátné
doliny	dolina	k1gFnSc2	dolina
v	v	k7c6	v
Kriváňské	kriváňský	k2eAgFnSc6d1	Kriváňská
Malé	Malé	k2eAgFnSc6d1	Malé
Fatře	Fatra	k1gFnSc6	Fatra
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
úpatí	úpatí	k1gNnSc6	úpatí
Velkého	velký	k2eAgInSc2d1	velký
Kriváně	Kriváň	k1gInSc2	Kriváň
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
740	[number]	k4	740
m.	m.	k?	m.
Přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
chatě	chata	k1gFnSc6	chata
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
dolní	dolní	k2eAgFnSc1d1	dolní
stanice	stanice	k1gFnSc1	stanice
nové	nový	k2eAgFnSc2d1	nová
kabinkové	kabinkový	k2eAgFnSc2d1	kabinková
lanovky	lanovka	k1gFnSc2	lanovka
na	na	k7c6	na
Snilovské	Snilovský	k2eAgFnSc6d1	Snilovský
sedlo-Chleb	sedlo-Chlba	k1gFnPc2	sedlo-Chlba
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
nacházela	nacházet	k5eAaImAgFnS	nacházet
stanice	stanice	k1gFnSc1	stanice
sedačkové	sedačkový	k2eAgFnSc2d1	sedačková
lanovky	lanovka	k1gFnSc2	lanovka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
začala	začít	k5eAaPmAgFnS	začít
jezdit	jezdit	k5eAaImF	jezdit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1950	[number]	k4	1950
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dokončením	dokončení	k1gNnSc7	dokončení
chaty	chata	k1gFnSc2	chata
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Turistika	turistika	k1gFnSc1	turistika
==	==	k?	==
</s>
</p>
<p>
<s>
Chata	chata	k1gFnSc1	chata
je	být	k5eAaImIp3nS	být
přístupná	přístupný	k2eAgFnSc1d1	přístupná
autobusovou	autobusový	k2eAgFnSc7d1	autobusová
dopravou	doprava	k1gFnSc7	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
chaty	chata	k1gFnSc2	chata
vedou	vést	k5eAaImIp3nP	vést
dva	dva	k4xCgInPc1	dva
turistické	turistický	k2eAgFnSc2d1	turistická
stezky	stezka	k1gFnPc1	stezka
<g/>
.	.	kIx.	.
</s>
<s>
Žlutě	žlutě	k6eAd1	žlutě
označený	označený	k2eAgInSc1d1	označený
směřuje	směřovat	k5eAaImIp3nS	směřovat
na	na	k7c4	na
chatu	chata	k1gFnSc4	chata
na	na	k7c6	na
Grúni	Grúni	k?	Grúni
a	a	k8xC	a
zeleně	zeleně	k6eAd1	zeleně
označený	označený	k2eAgInSc1d1	označený
na	na	k7c4	na
Snilovské	Snilovský	k2eAgNnSc4d1	Snilovské
sedlo	sedlo	k1gNnSc4	sedlo
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
hlavní	hlavní	k2eAgInSc4d1	hlavní
hřeben	hřeben	k1gInSc4	hřeben
pohoří	pohoří	k1gNnSc2	pohoří
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	nedaleko	k7c2	nedaleko
chaty	chata	k1gFnSc2	chata
je	být	k5eAaImIp3nS	být
Symbolický	symbolický	k2eAgInSc4d1	symbolický
hřbitov	hřbitov	k1gInSc4	hřbitov
obětí	oběť	k1gFnPc2	oběť
Malé	Malé	k2eAgFnSc2d1	Malé
Fatry	Fatra	k1gFnSc2	Fatra
věnovaný	věnovaný	k2eAgInSc1d1	věnovaný
obětem	oběť	k1gFnPc3	oběť
hor.	hor.	k?	hor.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Chata	chata	k1gFnSc1	chata
Vrátna	Vrátna	k1gFnSc1	Vrátna
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
