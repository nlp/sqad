<s>
Ukrajinská	ukrajinský	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
</s>
<s>
Ukrajinská	ukrajinský	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
</s>
<s>
Oblíbená	oblíbený	k2eAgFnSc1d1
ukrajinská	ukrajinský	k2eAgFnSc1d1
jídlaZákladní	jídlaZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Rozšíření	rozšíření	k1gNnSc1
</s>
<s>
Ukrajina	Ukrajina	k1gFnSc1
Ukrajina	Ukrajina	k1gFnSc1
Typ	typ	k1gInSc1
</s>
<s>
národní	národní	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
Ingredience	ingredience	k1gFnSc2
Suroviny	surovina	k1gFnSc2
</s>
<s>
zvěřina	zvěřina	k1gFnSc1
<g/>
,	,	kIx,
ryby	ryba	k1gFnPc1
<g/>
,	,	kIx,
obiloviny	obilovina	k1gFnPc1
<g/>
,	,	kIx,
brambory	brambora	k1gFnPc1
<g/>
,	,	kIx,
zelí	zelí	k1gNnSc1
<g/>
,	,	kIx,
okurky	okurek	k1gInPc1
<g/>
,	,	kIx,
cibule	cibule	k1gFnPc1
<g/>
,	,	kIx,
houby	houba	k1gFnPc1
<g/>
,	,	kIx,
ovoce	ovoce	k1gNnSc1
<g/>
,	,	kIx,
kořenová	kořenový	k2eAgFnSc1d1
zelenina	zelenina	k1gFnSc1
<g/>
,	,	kIx,
kysaná	kysaný	k2eAgFnSc1d1
smetana	smetana	k1gFnSc1
Dochucovadla	Dochucovadlo	k1gNnSc2
</s>
<s>
kopr	kopr	k1gInSc1
<g/>
,	,	kIx,
česnek	česnek	k1gInSc1
<g/>
,	,	kIx,
med	med	k1gInSc1
<g/>
,	,	kIx,
křen	křen	k1gInSc1
<g/>
,	,	kIx,
kmín	kmín	k1gInSc1
Pokrmy	pokrm	k1gInPc7
Jídla	jídlo	k1gNnSc2
</s>
<s>
Boršč	boršč	k1gInSc1
<g/>
,	,	kIx,
Soljanka	soljanka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Varenyky	Varenyko	k1gNnPc7
<g/>
,	,	kIx,
Pirižky	Pirižek	k1gInPc7
<g/>
,	,	kIx,
Šašlik	šašlik	k1gInSc1
<g/>
,	,	kIx,
Mlinci	Mlinek	k1gMnPc1
<g/>
,	,	kIx,
Varňje	Varňj	k1gFnPc1
Nápoje	nápoj	k1gInSc2
</s>
<s>
Čaj	čaj	k1gInSc1
<g/>
,	,	kIx,
Kvas	kvas	k1gInSc1
<g/>
,	,	kIx,
Rjažka	Rjažka	k1gFnSc1
<g/>
,	,	kIx,
Vodka	vodka	k1gFnSc1
<g/>
,	,	kIx,
Medovucha	Medovucha	k1gFnSc1
</s>
<s>
Ukrajinská	ukrajinský	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
rozmanitá	rozmanitý	k2eAgFnSc1d1
a	a	k8xC
má	mít	k5eAaImIp3nS
prvky	prvek	k1gInPc4
různých	různý	k2eAgFnPc2d1
evropských	evropský	k2eAgFnPc2d1
kuchyní	kuchyně	k1gFnPc2
<g/>
,	,	kIx,
zvláště	zvláště	k6eAd1
ruské	ruský	k2eAgFnSc2d1
<g/>
,	,	kIx,
polské	polský	k2eAgFnSc2d1
<g/>
,	,	kIx,
německé	německý	k2eAgFnSc2d1
<g/>
,	,	kIx,
maďarské	maďarský	k2eAgFnSc2d1
a	a	k8xC
turecké	turecký	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdůležitější	důležitý	k2eAgFnSc7d3
je	být	k5eAaImIp3nS
ale	ale	k9
staroslovanský	staroslovanský	k2eAgInSc4d1
vliv	vliv	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Ukrajinská	ukrajinský	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
ovlivnila	ovlivnit	k5eAaPmAgFnS
i	i	k9
národní	národní	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
sousedních	sousední	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
polské	polský	k2eAgFnSc2d1
a	a	k8xC
ruské	ruský	k2eAgFnSc2d1
kuchyně	kuchyně	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvky	prvek	k1gInPc7
ukrajinské	ukrajinský	k2eAgFnSc2d1
kuchyně	kuchyně	k1gFnSc2
jsou	být	k5eAaImIp3nP
známé	známý	k2eAgInPc1d1
také	také	k9
v	v	k7c6
severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
díky	díky	k7c3
ukrajinským	ukrajinský	k2eAgMnPc3d1
imigrantům	imigrant	k1gMnPc3
do	do	k7c2
USA	USA	kA
a	a	k8xC
Kanady	Kanada	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ukrajinské	ukrajinský	k2eAgFnPc4d1
tradice	tradice	k1gFnPc4
vaření	vaření	k1gNnSc2
jsou	být	k5eAaImIp3nP
udržovány	udržovat	k5eAaImNgFnP
v	v	k7c6
mnoha	mnoho	k4c6
restauracích	restaurace	k1gFnPc6
na	na	k7c6
Ukrajině	Ukrajina	k1gFnSc6
<g/>
,	,	kIx,
Rusku	Rusko	k1gNnSc6
a	a	k8xC
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
také	také	k9
v	v	k7c6
Evropě	Evropa	k1gFnSc6
a	a	k8xC
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Zvláštností	zvláštnost	k1gFnSc7
ukrajinské	ukrajinský	k2eAgFnSc2d1
kuchyně	kuchyně	k1gFnSc2
je	být	k5eAaImIp3nS
využívání	využívání	k1gNnSc1
převážně	převážně	k6eAd1
domácích	domácí	k2eAgFnPc2d1
potravin	potravina	k1gFnPc2
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
brambory	brambora	k1gFnPc1
<g/>
,	,	kIx,
maso	maso	k1gNnSc1
<g/>
,	,	kIx,
zvěřina	zvěřina	k1gFnSc1
<g/>
,	,	kIx,
ovoce	ovoce	k1gNnSc1
<g/>
,	,	kIx,
houby	houba	k1gFnPc1
<g/>
,	,	kIx,
lesní	lesní	k2eAgInPc1d1
plody	plod	k1gInPc1
a	a	k8xC
bylinky	bylinka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nejznámější	známý	k2eAgNnPc4d3
jídla	jídlo	k1gNnPc4
patří	patřit	k5eAaImIp3nS
boršč	boršč	k1gInSc1
<g/>
,	,	kIx,
soljanka	soljanka	k1gFnSc1
a	a	k8xC
varenyky	varenyky	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
nápoji	nápoj	k1gInPc7
je	být	k5eAaImIp3nS
oblíbená	oblíbený	k2eAgFnSc1d1
vodka	vodka	k1gFnSc1
nebo	nebo	k8xC
kvas	kvas	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Suroviny	surovina	k1gFnPc1
</s>
<s>
Ukrajinská	ukrajinský	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
je	být	k5eAaImIp3nS
tradičně	tradičně	k6eAd1
založena	založit	k5eAaPmNgFnS
na	na	k7c4
místní	místní	k2eAgFnSc4d1
produkci	produkce	k1gFnSc4
a	a	k8xC
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
si	se	k3xPyFc3
lidé	člověk	k1gMnPc1
vypěstují	vypěstovat	k5eAaPmIp3nP
na	na	k7c6
zahrádkách	zahrádka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
oblíbené	oblíbený	k2eAgFnPc4d1
potraviny	potravina	k1gFnPc4
na	na	k7c6
Ukrajině	Ukrajina	k1gFnSc6
patří	patřit	k5eAaImIp3nS
zvěřina	zvěřina	k1gFnSc1
a	a	k8xC
ryby	ryba	k1gFnPc1
z	z	k7c2
místních	místní	k2eAgInPc2d1
lesů	les	k1gInPc2
a	a	k8xC
řek	řeka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Specialitou	specialita	k1gFnSc7
je	být	k5eAaImIp3nS
černý	černý	k2eAgInSc1d1
a	a	k8xC
červený	červený	k2eAgInSc1d1
kaviár	kaviár	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základními	základní	k2eAgFnPc7d1
surovinami	surovina	k1gFnPc7
jsou	být	k5eAaImIp3nP
brambory	brambora	k1gFnPc1
<g/>
,	,	kIx,
obilí	obilí	k1gNnSc1
<g/>
,	,	kIx,
červená	červený	k2eAgFnSc1d1
řepa	řepa	k1gFnSc1
a	a	k8xC
zelí	zelí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čerstvé	čerstvý	k2eAgNnSc1d1
ovoce	ovoce	k1gNnSc1
<g/>
,	,	kIx,
zelenina	zelenina	k1gFnSc1
<g/>
,	,	kIx,
houby	houba	k1gFnPc1
a	a	k8xC
lesní	lesní	k2eAgInPc1d1
plody	plod	k1gInPc1
doplňují	doplňovat	k5eAaImIp3nP
jídelníček	jídelníček	k1gInSc4
ve	v	k7c6
vegetačním	vegetační	k2eAgNnSc6d1
období	období	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
zimě	zima	k1gFnSc6
se	se	k3xPyFc4
pak	pak	k6eAd1
konzumují	konzumovat	k5eAaBmIp3nP
kompoty	kompot	k1gInPc1
<g/>
,	,	kIx,
nakládané	nakládaný	k2eAgInPc1d1
okurky	okurek	k1gInPc1
a	a	k8xC
další	další	k2eAgFnPc1d1
konzervované	konzervovaný	k2eAgFnPc1d1
zeleniny	zelenina	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
velké	velký	k2eAgFnSc6d1
oblibě	obliba	k1gFnSc6
jsou	být	k5eAaImIp3nP
slunečnicová	slunečnicový	k2eAgNnPc1d1
semínka	semínko	k1gNnPc1
<g/>
,	,	kIx,
která	který	k3yIgNnPc1,k3yRgNnPc1,k3yQgNnPc1
jsou	být	k5eAaImIp3nP
k	k	k7c3
dostání	dostání	k1gNnSc3
po	po	k7c4
celý	celý	k2eAgInSc4d1
rok	rok	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pak	pak	k6eAd1
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
zmrzlina	zmrzlina	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
si	se	k3xPyFc3
dopřávají	dopřávat	k5eAaImIp3nP
i	i	k9
v	v	k7c6
zimě	zima	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Chléb	chléb	k1gInSc1
je	být	k5eAaImIp3nS
nepostradatelnou	postradatelný	k2eNgFnSc7d1
součástí	součást	k1gFnSc7
ukrajinských	ukrajinský	k2eAgNnPc2d1
jídel	jídlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyrábí	vyrábět	k5eAaImIp3nS
se	se	k3xPyFc4
mnoho	mnoho	k4c1
druhů	druh	k1gInPc2
chleba	chléb	k1gInSc2
od	od	k7c2
tmavého	tmavý	k2eAgInSc2d1
<g/>
,	,	kIx,
světlého	světlý	k2eAgInSc2d1
chleba	chléb	k1gInSc2
až	až	k9
po	po	k7c4
speciální	speciální	k2eAgInPc4d1
chleby	chléb	k1gInPc4
pro	pro	k7c4
různé	různý	k2eAgFnPc4d1
příležitosti	příležitost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejtradičnější	tradiční	k2eAgInSc4d3
je	být	k5eAaImIp3nS
tmavý	tmavý	k2eAgInSc4d1
chléb	chléb	k1gInSc4
(	(	kIx(
<g/>
čorni	čornit	k5eAaPmRp2nS,k5eAaImRp2nS
chleb	chlebit	k5eAaImRp2nS
<g/>
)	)	kIx)
vyráběný	vyráběný	k2eAgInSc1d1
z	z	k7c2
pohanky	pohanka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
příloha	příloha	k1gFnSc1
k	k	k7c3
jídlu	jídlo	k1gNnSc3
se	se	k3xPyFc4
často	často	k6eAd1
konzumuje	konzumovat	k5eAaBmIp3nS
kukuřičná	kukuřičný	k2eAgFnSc1d1
kaše	kaše	k1gFnSc1
nazývaná	nazývaný	k2eAgFnSc1d1
mamalyha	mamalyha	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
podobná	podobný	k2eAgFnSc1d1
italské	italský	k2eAgFnSc3d1
polentě	polenta	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oblíbené	oblíbený	k2eAgInPc1d1
jsou	být	k5eAaImIp3nP
i	i	k9
sýry	sýr	k1gInPc7
<g/>
,	,	kIx,
nejpoužívanějším	používaný	k2eAgMnSc7d3
je	být	k5eAaImIp3nS
ovčí	ovčí	k2eAgInSc1d1
sýr	sýr	k1gInSc1
podobný	podobný	k2eAgInSc1d1
naší	náš	k3xOp1gFnSc6
brynze	brynza	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boršč	boršč	k1gInSc1
<g/>
,	,	kIx,
polévka	polévka	k1gFnSc1
</s>
<s>
Pokrmy	pokrm	k1gInPc1
</s>
<s>
Polévky	polévka	k1gFnPc1
</s>
<s>
Soljanka	soljanka	k1gFnSc1
<g/>
,	,	kIx,
polévkaBoršč	polévkaBoršč	k1gFnSc1
<g/>
,	,	kIx,
národní	národní	k2eAgNnSc1d1
jídlo	jídlo	k1gNnSc1
<g/>
,	,	kIx,
vývar	vývar	k1gInSc1
z	z	k7c2
vepřového	vepřový	k2eAgNnSc2d1
masa	maso	k1gNnSc2
<g/>
,	,	kIx,
dále	daleko	k6eAd2
se	se	k3xPyFc4
do	do	k7c2
něj	on	k3xPp3gInSc2
přidává	přidávat	k5eAaImIp3nS
červená	červený	k2eAgFnSc1d1
řepa	řepa	k1gFnSc1
<g/>
,	,	kIx,
cibule	cibule	k1gFnSc1
<g/>
,	,	kIx,
mrkev	mrkev	k1gFnSc1
<g/>
,	,	kIx,
rajčata	rajče	k1gNnPc1
<g/>
,	,	kIx,
brambory	brambor	k1gInPc1
<g/>
,	,	kIx,
hlávkové	hlávkový	k2eAgNnSc1d1
zelí	zelí	k1gNnSc1
<g/>
,	,	kIx,
kopr	kopr	k1gInSc1
a	a	k8xC
smetana	smetana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
polévce	polévka	k1gFnSc3
se	se	k3xPyFc4
často	často	k6eAd1
podávají	podávat	k5eAaImIp3nP
pampušky	pampuška	k1gFnPc1
<g/>
,	,	kIx,
v	v	k7c6
oleji	olej	k1gInSc6
smažené	smažený	k2eAgInPc1d1
bochánky	bochánek	k1gInPc1
potřené	potřený	k2eAgInPc1d1
česnekem	česnek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Soljanka	soljanka	k1gFnSc1
<g/>
,	,	kIx,
národní	národní	k2eAgNnSc1d1
jídlo	jídlo	k1gNnSc1
<g/>
,	,	kIx,
hutná	hutný	k2eAgFnSc1d1
polévka	polévka	k1gFnSc1
s	s	k7c7
kusy	kus	k1gInPc7
masa	maso	k1gNnSc2
nebo	nebo	k8xC
ryby	ryba	k1gFnPc1
se	s	k7c7
zeleninou	zelenina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přísadami	přísada	k1gFnPc7
jsou	být	k5eAaImIp3nP
rajčata	rajče	k1gNnPc4
<g/>
,	,	kIx,
olivy	oliva	k1gFnPc1
<g/>
,	,	kIx,
citron	citron	k1gInSc1
<g/>
,	,	kIx,
nakládané	nakládaný	k2eAgFnPc1d1
houby	houba	k1gFnPc1
<g/>
,	,	kIx,
okurkový	okurkový	k2eAgInSc1d1
nálev	nálev	k1gInSc1
<g/>
,	,	kIx,
kopr	kopr	k1gInSc1
a	a	k8xC
smetana	smetana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Juška	Juška	k1gFnSc1
<g/>
,	,	kIx,
rybí	rybí	k2eAgFnSc1d1
polévka	polévka	k1gFnSc1
z	z	k7c2
nejrůznějších	různý	k2eAgInPc2d3
druhů	druh	k1gInPc2
ryb	ryba	k1gFnPc2
(	(	kIx(
<g/>
sumec	sumec	k1gMnSc1
<g/>
,	,	kIx,
štika	štika	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
obsahuje	obsahovat	k5eAaImIp3nS
kořenovou	kořenový	k2eAgFnSc4d1
zeleninu	zelenina	k1gFnSc4
a	a	k8xC
koření	koření	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Okroška	Okroška	k1gFnSc1
<g/>
,	,	kIx,
zeleninová	zeleninový	k2eAgFnSc1d1
polévka	polévka	k1gFnSc1
připravovaná	připravovaný	k2eAgFnSc1d1
z	z	k7c2
kvasu	kvas	k1gInSc2
<g/>
,	,	kIx,
přidává	přidávat	k5eAaImIp3nS
se	se	k3xPyFc4
vařené	vařený	k2eAgNnSc1d1
vejce	vejce	k1gNnSc1
a	a	k8xC
smetana	smetana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okroška	Okroška	k1gFnSc1
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
zeleninová	zeleninový	k2eAgFnSc1d1
<g/>
,	,	kIx,
masová	masový	k2eAgFnSc1d1
nebo	nebo	k8xC
rybí	rybí	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konzumuje	konzumovat	k5eAaBmIp3nS
se	se	k3xPyFc4
studená	studený	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Rosolnyk	Rosolnyk	k1gInSc1
<g/>
,	,	kIx,
kyselá	kyselý	k2eAgFnSc1d1
polévka	polévka	k1gFnSc1
z	z	k7c2
okurkového	okurkový	k2eAgInSc2d1
nálevu	nálev	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
také	také	k9
drůbeží	drůbeží	k2eAgInPc4d1
droby	drob	k1gInPc4
<g/>
,	,	kIx,
ledvinky	ledvinka	k1gFnPc4
<g/>
,	,	kIx,
zeleninu	zelenina	k1gFnSc4
<g/>
,	,	kIx,
brambory	brambora	k1gFnPc4
nebo	nebo	k8xC
ječmen	ječmen	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opět	opět	k6eAd1
bývá	bývat	k5eAaImIp3nS
politá	politý	k2eAgFnSc1d1
smetanou	smetana	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Kapustňak	Kapustňak	k1gInSc1
<g/>
,	,	kIx,
zelňačka	zelňačka	k1gFnSc1
s	s	k7c7
vepřovým	vepřový	k2eAgNnSc7d1
masem	maso	k1gNnSc7
<g/>
,	,	kIx,
vepřovým	vepřový	k2eAgInSc7d1
špekem	špek	k1gInSc7
(	(	kIx(
<g/>
sala	sala	k6eAd1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Servíruje	servírovat	k5eAaBmIp3nS
se	se	k3xPyFc4
se	s	k7c7
zakysanou	zakysaný	k2eAgFnSc7d1
smetanou	smetana	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Varenyky	Varenyka	k1gFnPc1
<g/>
,	,	kIx,
plněné	plněný	k2eAgFnPc1d1
taštičky	taštička	k1gFnPc1
se	s	k7c7
smetanou	smetaný	k2eAgFnSc7d1
</s>
<s>
Hlavní	hlavní	k2eAgNnPc1d1
jídla	jídlo	k1gNnPc1
</s>
<s>
Varenyky	Varenyka	k1gFnPc1
<g/>
,	,	kIx,
národní	národní	k2eAgNnSc1d1
jídlo	jídlo	k1gNnSc1
<g/>
,	,	kIx,
plněné	plněný	k2eAgFnPc1d1
těstovinové	těstovinový	k2eAgFnPc1d1
taštičky	taštička	k1gFnPc1
ve	v	k7c6
tvaru	tvar	k1gInSc6
čtverce	čtverec	k1gInSc2
nebo	nebo	k8xC
půlměsíce	půlměsíc	k1gInPc4
podobné	podobný	k2eAgInPc4d1
italským	italský	k2eAgFnPc3d1
raviolám	raviola	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Taštičky	taštička	k1gFnSc2
jsou	být	k5eAaImIp3nP
naplněné	naplněný	k2eAgNnSc1d1
mletým	mletý	k2eAgNnSc7d1
masem	maso	k1gNnSc7
<g/>
,	,	kIx,
zelím	zelí	k1gNnSc7
<g/>
,	,	kIx,
tvarohem	tvaroh	k1gInSc7
nebo	nebo	k8xC
ovocem	ovoce	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podávají	podávat	k5eAaImIp3nP
se	se	k3xPyFc4
polité	politý	k2eAgInPc1d1
máslem	máslo	k1gNnSc7
s	s	k7c7
kysanou	kysaný	k2eAgFnSc7d1
smetanou	smetana	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Pirižky	Pirižka	k1gFnPc1
<g/>
,	,	kIx,
národní	národní	k2eAgNnSc1d1
jídlo	jídlo	k1gNnSc1
<g/>
,	,	kIx,
smažené	smažený	k2eAgInPc1d1
bochánky	bochánek	k1gInPc1
z	z	k7c2
kynutého	kynutý	k2eAgNnSc2d1
těsta	těsto	k1gNnSc2
plněného	plněný	k2eAgInSc2d1
masem	maso	k1gNnSc7
<g/>
,	,	kIx,
vejci	vejce	k1gNnPc7
<g/>
,	,	kIx,
bramborami	brambora	k1gFnPc7
<g/>
,	,	kIx,
zelím	zelí	k1gNnSc7
<g/>
,	,	kIx,
houbami	houba	k1gFnPc7
<g/>
,	,	kIx,
tvarohem	tvaroh	k1gInSc7
a	a	k8xC
dalšími	další	k2eAgFnPc7d1
náplněmi	náplň	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Čebureky	Čeburek	k1gInPc1
<g/>
,	,	kIx,
v	v	k7c6
oleji	olej	k1gInSc6
smažené	smažený	k2eAgFnSc2d1
závitky	závitka	k1gFnSc2
ve	v	k7c6
tvaru	tvar	k1gInSc6
půlměsíce	půlměsíc	k1gInSc2
plněné	plněný	k2eAgNnSc1d1
mletým	mletý	k2eAgNnSc7d1
jehněčím	jehněčí	k2eAgNnSc7d1
masem	maso	k1gNnSc7
<g/>
,	,	kIx,
cibulí	cibule	k1gFnSc7
<g/>
,	,	kIx,
petrželkou	petrželka	k1gFnSc7
a	a	k8xC
pepřem	pepř	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Pirohy	piroh	k1gInPc1
<g/>
,	,	kIx,
smažené	smažený	k2eAgInPc1d1
bochánky	bochánek	k1gInPc1
s	s	k7c7
cibulkouHolubci	cibulkouHolubec	k1gInPc7
<g/>
,	,	kIx,
závitky	závitek	k1gInPc1
plněné	plněný	k2eAgInPc1d1
směsí	směs	k1gFnPc2
kořeněného	kořeněný	k2eAgNnSc2d1
masa	maso	k1gNnSc2
a	a	k8xC
rýže	rýže	k1gFnSc2
v	v	k7c6
rajčatové	rajčatový	k2eAgFnSc6d1
omáčce	omáčka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Kručenyky	Kručenyk	k1gInPc1
<g/>
,	,	kIx,
hovězí	hovězí	k2eAgInPc1d1
nebo	nebo	k8xC
vepřové	vepřový	k2eAgInPc1d1
závitky	závitek	k1gInPc1
s	s	k7c7
nejrůznějšími	různý	k2eAgFnPc7d3
náplněmi	náplň	k1gFnPc7
(	(	kIx(
<g/>
švestkami	švestka	k1gFnPc7
<g/>
,	,	kIx,
slaninou	slanina	k1gFnSc7
a	a	k8xC
špenátem	špenát	k1gInSc7
<g/>
,	,	kIx,
popřípadě	popřípadě	k6eAd1
houbami	houba	k1gFnPc7
<g/>
,	,	kIx,
cibulí	cibule	k1gFnSc7
<g/>
,	,	kIx,
vejci	vejce	k1gNnSc6
<g/>
,	,	kIx,
sýrem	sýr	k1gInSc7
<g/>
)	)	kIx)
</s>
<s>
Šašlik	šašlik	k1gInSc1
<g/>
,	,	kIx,
předem	předem	k6eAd1
marinované	marinovaný	k2eAgInPc4d1
kousky	kousek	k1gInPc4
masa	maso	k1gNnSc2
grilované	grilovaný	k2eAgNnSc4d1
pak	pak	k6eAd1
na	na	k7c4
jehlici	jehlice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Kotleta	kotleta	k1gFnSc1
po	po	k7c6
kiivski	kiivski	k1gNnSc6
<g/>
,	,	kIx,
kyjevský	kyjevský	k2eAgInSc1d1
kotlet	kotlet	k1gInSc1
<g/>
,	,	kIx,
kuřecí	kuřecí	k2eAgNnPc1d1
prsíčka	prsíčka	k1gNnPc1
zjemněná	zjemněný	k2eAgFnSc1d1
máslovou	máslový	k2eAgFnSc7d1
nádivkou	nádivka	k1gFnSc7
<g/>
,	,	kIx,
obalená	obalený	k2eAgFnSc1d1
v	v	k7c6
trojobalu	trojobal	k1gInSc6
<g/>
,	,	kIx,
smažená	smažený	k2eAgFnSc1d1
a	a	k8xC
podávaná	podávaný	k2eAgFnSc1d1
většinou	většinou	k6eAd1
s	s	k7c7
bramborovou	bramborový	k2eAgFnSc7d1
kaší	kaše	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Sičenyky	Sičenyka	k1gFnPc1
<g/>
,	,	kIx,
placičky	placička	k1gFnPc1
z	z	k7c2
mletého	mletý	k2eAgNnSc2d1
masa	maso	k1gNnSc2
smíšeného	smíšený	k2eAgNnSc2d1
s	s	k7c7
vejcem	vejce	k1gNnSc7
<g/>
,	,	kIx,
cibulí	cibule	k1gFnSc7
<g/>
,	,	kIx,
chlebovými	chlebový	k2eAgInPc7d1
drobky	drobek	k1gInPc7
a	a	k8xC
mlékem	mléko	k1gNnSc7
smažené	smažený	k2eAgFnSc2d1
v	v	k7c6
oleji	olej	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Deruby	Derub	k1gInPc1
<g/>
,	,	kIx,
bramboráky	bramborák	k1gInPc1
se	se	k3xPyFc4
zeleninouCholoděc	zeleninouCholoděc	k6eAd1
(	(	kIx(
<g/>
aspik	aspik	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
telecí	telecí	k2eAgFnSc1d1
<g/>
,	,	kIx,
hovězí	hovězí	k2eAgFnSc1d1
nebo	nebo	k8xC
vepřová	vepřový	k2eAgFnSc1d1
huspenina	huspenina	k1gFnSc1
se	s	k7c7
zeleninou	zelenina	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Servíruje	servírovat	k5eAaBmIp3nS
se	se	k3xPyFc4
s	s	k7c7
ostrým	ostrý	k2eAgInSc7d1
křenem	křen	k1gInSc7
<g/>
,	,	kIx,
případně	případně	k6eAd1
hořčicí	hořčice	k1gFnSc7
<g/>
,	,	kIx,
česnekem	česnek	k1gInSc7
či	či	k8xC
smetanou	smetana	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Deruny	Derun	k1gInPc1
<g/>
,	,	kIx,
bramboráky	bramborák	k1gInPc1
s	s	k7c7
kysanou	kysaný	k2eAgFnSc7d1
smetanou	smetana	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Mamalyha	Mamalyha	k1gFnSc1
<g/>
,	,	kIx,
kaša	kaša	k1gNnSc1
<g/>
,	,	kIx,
národní	národní	k2eAgNnSc1d1
jídlo	jídlo	k1gNnSc1
<g/>
,	,	kIx,
kaše	kaše	k1gFnPc1
z	z	k7c2
různých	různý	k2eAgInPc2d1
druhů	druh	k1gInPc2
obilí	obilí	k1gNnSc2
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
však	však	k9
z	z	k7c2
pohanky	pohanka	k1gFnSc2
nebo	nebo	k8xC
kukuřice	kukuřice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ochucuje	ochucovat	k5eAaImIp3nS
se	s	k7c7
škvarky	škvarek	k1gInPc7
a	a	k8xC
cibulí	cibule	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jí	jíst	k5eAaImIp3nS
se	se	k3xPyFc4
k	k	k7c3
snídani	snídaně	k1gFnSc3
nebo	nebo	k8xC
jako	jako	k8xC,k8xS
příloha	příloha	k1gFnSc1
k	k	k7c3
masu	maso	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Pampušky	Pampušek	k1gInPc1
<g/>
,	,	kIx,
smažené	smažený	k2eAgInPc1d1
kynuté	kynutý	k2eAgInPc1d1
bochánky	bochánek	k1gInPc1
<g/>
,	,	kIx,
podobné	podobný	k2eAgInPc1d1
našim	náš	k3xOp1gFnPc3
koblihám	kobliha	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potírají	potírat	k5eAaImIp3nP
se	se	k3xPyFc4
česnekem	česnek	k1gInSc7
a	a	k8xC
podávají	podávat	k5eAaImIp3nP
se	se	k3xPyFc4
k	k	k7c3
boršči	boršč	k1gInSc3
nebo	nebo	k8xC
jušce	juška	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mohou	moct	k5eAaImIp3nP
se	se	k3xPyFc4
podávat	podávat	k5eAaImF
i	i	k9
sladké	sladký	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Mlynci	Mlynec	k1gInPc7
<g/>
,	,	kIx,
palačinky	palačinka	k1gFnPc4
plněné	plněný	k2eAgFnPc4d1
masem	maso	k1gNnSc7
<g/>
,	,	kIx,
sýrem	sýr	k1gInSc7
<g/>
,	,	kIx,
kaviárem	kaviár	k1gInSc7
nebo	nebo	k8xC
houbami	houba	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podávají	podávat	k5eAaImIp3nP
se	se	k3xPyFc4
se	s	k7c7
zakysanou	zakysaný	k2eAgFnSc7d1
Vinegret	Vinegret	k1gInSc4
<g/>
,	,	kIx,
salátsmetanou	salátsmetaný	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Připravují	připravovat	k5eAaImIp3nP
se	se	k3xPyFc4
i	i	k9
sladké	sladký	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s>
Salo	Salo	k6eAd1
<g/>
,	,	kIx,
vepřový	vepřový	k2eAgInSc4d1
špek	špek	k1gInSc4
nebo	nebo	k8xC
sádlo	sádlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konzumuje	konzumovat	k5eAaBmIp3nS
se	se	k3xPyFc4
s	s	k7c7
chlebem	chléb	k1gInSc7
a	a	k8xC
rozetřeným	rozetřený	k2eAgInSc7d1
česnekem	česnek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Salo	Salo	k1gMnSc1
se	se	k3xPyFc4
často	často	k6eAd1
konzumuje	konzumovat	k5eAaBmIp3nS
při	při	k7c6
pití	pití	k1gNnSc6
vodky	vodka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Saláty	salát	k1gInPc1
</s>
<s>
Salát	salát	k1gInSc1
olivje	olivj	k1gInSc2
(	(	kIx(
<g/>
olivier	olivier	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
salát	salát	k1gInSc4
z	z	k7c2
vařených	vařený	k2eAgFnPc2d1
brambor	brambora	k1gFnPc2
<g/>
,	,	kIx,
cibule	cibule	k1gFnSc2
<g/>
,	,	kIx,
kysaných	kysaný	k2eAgFnPc2d1
okurek	okurka	k1gFnPc2
<g/>
,	,	kIx,
hrášku	hrášek	k1gInSc2
<g/>
,	,	kIx,
vajec	vejce	k1gNnPc2
a	a	k8xC
kousků	kousek	k1gInPc2
masa	maso	k1gNnSc2
zalitý	zalitý	k2eAgInSc4d1
majonézou	majonéza	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Připomíná	připomínat	k5eAaImIp3nS
náš	náš	k3xOp1gInSc4
bramborový	bramborový	k2eAgInSc4d1
salát	salát	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Vinegret	Vinegret	k1gMnSc1
(	(	kIx(
<g/>
viněgret	viněgret	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
salát	salát	k1gInSc4
z	z	k7c2
červené	červený	k2eAgFnSc2d1
řepy	řepa	k1gFnSc2
<g/>
,	,	kIx,
brambor	brambora	k1gFnPc2
<g/>
,	,	kIx,
mrkve	mrkev	k1gFnSc2
<g/>
,	,	kIx,
cibule	cibule	k1gFnSc2
a	a	k8xC
kysaného	kysaný	k2eAgNnSc2d1
zelí	zelí	k1gNnSc2
nebo	nebo	k8xC
kysanéMlynci	kysanéMlynec	k1gInPc7
<g/>
,	,	kIx,
palačinky	palačinka	k1gFnPc4
okurky	okurka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vše	všechen	k3xTgNnSc1
nakrájené	nakrájený	k2eAgNnSc1d1
na	na	k7c4
kostky	kostka	k1gFnPc4
<g/>
,	,	kIx,
dochucené	dochucený	k2eAgMnPc4d1
a	a	k8xC
zalité	zalitý	k2eAgMnPc4d1
slunečnicovým	slunečnicový	k2eAgInSc7d1
olejem	olej	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Sladká	sladký	k2eAgNnPc1d1
jídla	jídlo	k1gNnPc1
</s>
<s>
Bubliky	Bublika	k1gFnPc1
<g/>
,	,	kIx,
kroužky	kroužek	k1gInPc1
z	z	k7c2
kynutého	kynutý	k2eAgNnSc2d1
pšeničného	pšeničný	k2eAgNnSc2d1
těsta	těsto	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
se	se	k3xPyFc4
krátce	krátce	k6eAd1
vaří	vařit	k5eAaImIp3nS
a	a	k8xC
poté	poté	k6eAd1
pečou	péct	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
těsta	těsto	k1gNnSc2
se	se	k3xPyFc4
často	často	k6eAd1
přidávají	přidávat	k5eAaImIp3nP
semínka	semínko	k1gNnPc4
máku	mák	k1gInSc2
nebo	nebo	k8xC
jiná	jiný	k2eAgNnPc4d1
ochucovadla	ochucovadlo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bubliky	Bublik	k1gMnPc7
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
jedí	jíst	k5eAaImIp3nP
k	k	k7c3
čaji	čaj	k1gInSc3
nebo	nebo	k8xC
kávě	káva	k1gFnSc3
<g/>
,	,	kIx,
proto	proto	k8xC
bývá	bývat	k5eAaImIp3nS
těsto	těsto	k1gNnSc4
o	o	k7c4
málo	málo	k1gNnSc4
sladší	sladký	k2eAgFnSc1d2
než	než	k8xS
je	být	k5eAaImIp3nS
tomu	ten	k3xDgMnSc3
u	u	k7c2
pečiva	pečivo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
pekařstvích	pekařství	k1gNnPc6
nebo	nebo	k8xC
na	na	k7c6
trzích	trh	k1gInPc6
se	se	k3xPyFc4
bubliky	bublika	k1gFnPc1
často	často	k6eAd1
zavěšují	zavěšovat	k5eAaImIp3nP
na	na	k7c4
šňůru	šňůra	k1gFnSc4
po	po	k7c6
12	#num#	k4
kusech	kus	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Pampušky	Pampušek	k1gInPc1
<g/>
,	,	kIx,
smažené	smažený	k2eAgInPc1d1
kynuté	kynutý	k2eAgInPc1d1
bochánky	bochánek	k1gInPc1
<g/>
,	,	kIx,
podobné	podobný	k2eAgInPc1d1
našim	náš	k3xOp1gFnPc3
koblihám	kobliha	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
plněné	plněný	k2eAgInPc1d1
ovocem	ovoce	k1gNnSc7
<g/>
,	,	kIx,
vareňjem	vareňj	k1gInSc7
nebo	nebo	k8xC
mákem	mák	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
posypané	posypaný	k2eAgInPc1d1
cukrem	cukr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Mlynci	Mlynec	k1gInPc7
<g/>
,	,	kIx,
palačinky	palačinka	k1gFnPc4
plněné	plněný	k2eAgFnPc4d1
tvarohem	tvaroh	k1gInSc7
<g/>
,	,	kIx,
marmeládou	marmeláda	k1gFnSc7
nebo	nebo	k8xC
ovocem	ovoce	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podávají	podávat	k5eAaImIp3nP
se	se	k3xPyFc4
se	s	k7c7
zakysanouKvas	zakysanouKvas	k1gInSc1
<g/>
,	,	kIx,
národní	národní	k2eAgFnSc4d1
nápojsmetanou	nápojsmetaný	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Syrnyky	Syrnyk	k1gInPc4
<g/>
,	,	kIx,
tvarohové	tvarohový	k2eAgInPc4d1
lívance	lívanec	k1gInPc4
<g/>
,	,	kIx,
natřené	natřený	k2eAgFnSc2d1
džemem	džem	k1gInSc7
nebo	nebo	k8xC
varuje	varovat	k5eAaImIp3nS
a	a	k8xC
polité	politý	k2eAgNnSc4d1
smetanou	smetana	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Vareňje	Vareňje	k1gFnSc1
<g/>
,	,	kIx,
zavařenina	zavařenina	k1gFnSc1
s	s	k7c7
kousky	kousek	k1gInPc7
ovoce	ovoce	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dělá	dělat	k5eAaImIp3nS
se	se	k3xPyFc4
z	z	k7c2
meruněk	meruňka	k1gFnPc2
<g/>
,	,	kIx,
třešní	třešeň	k1gFnPc2
<g/>
,	,	kIx,
černého	černý	k2eAgInSc2d1
rybízu	rybíz	k1gInSc2
<g/>
,	,	kIx,
jablek	jablko	k1gNnPc2
<g/>
,	,	kIx,
hrušek	hruška	k1gFnPc2
<g/>
,	,	kIx,
kdoulí	kdoule	k1gFnPc2
<g/>
,	,	kIx,
jahod	jahoda	k1gFnPc2
<g/>
,	,	kIx,
malin	malina	k1gFnPc2
<g/>
,	,	kIx,
pomerančů	pomeranč	k1gInPc2
nebo	nebo	k8xC
citrónů	citrón	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
palačinky	palačinka	k1gFnPc4
a	a	k8xC
další	další	k2eAgNnSc4d1
pečivo	pečivo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Nápoje	nápoj	k1gInPc1
</s>
<s>
Čaj	čaj	k1gInSc4
<g/>
,	,	kIx,
oblíbený	oblíbený	k2eAgInSc4d1
nápoj	nápoj	k1gInSc4
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
slazený	slazený	k2eAgInSc1d1
černý	černý	k2eAgInSc1d1
čaj	čaj	k1gInSc1
s	s	k7c7
citrónem	citrón	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Často	často	k6eAd1
se	se	k3xPyFc4
také	také	k9
sladí	sladit	k5eAaPmIp3nS,k5eAaImIp3nS
vareňje	vareňje	k1gFnSc1
nebo	nebo	k8xC
Prodej	prodej	k1gInSc1
kvasu	kvas	k1gInSc2
v	v	k7c6
Kyjevěmedem	Kyjevěmed	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vaří	vařit	k5eAaImIp3nS
se	se	k3xPyFc4
i	i	k9
nejrůznější	různý	k2eAgInPc1d3
bylinkové	bylinkový	k2eAgInPc1d1
čaje	čaj	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Rjažanka	Rjažanka	k1gFnSc1
<g/>
,	,	kIx,
kysaný	kysaný	k2eAgInSc1d1
mléčný	mléčný	k2eAgInSc4d1
nápoj	nápoj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobný	podobný	k2eAgInSc4d1
kefíru	kefír	k1gInSc3
<g/>
.	.	kIx.
</s>
<s>
Kvas	kvas	k1gInSc4
<g/>
,	,	kIx,
pěnivý	pěnivý	k2eAgInSc4d1
sladkokyselý	sladkokyselý	k2eAgInSc4d1
slabě	slabě	k6eAd1
alkoholický	alkoholický	k2eAgInSc4d1
nápoj	nápoj	k1gInSc4
vyráběný	vyráběný	k2eAgInSc4d1
z	z	k7c2
kvasnic	kvasnice	k1gFnPc2
<g/>
,	,	kIx,
cukru	cukr	k1gInSc2
a	a	k8xC
chleba	chléb	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Může	moct	k5eAaImIp3nS
být	být	k5eAaImF
ochucený	ochucený	k2eAgInSc4d1
jahodami	jahoda	k1gFnPc7
<g/>
,	,	kIx,
malinami	malina	k1gFnPc7
<g/>
,	,	kIx,
třešněmi	třešeň	k1gFnPc7
<g/>
,	,	kIx,
rozinkami	rozinka	k1gFnPc7
<g/>
,	,	kIx,
citronem	citron	k1gInSc7
<g/>
,	,	kIx,
zázvorem	zázvor	k1gInSc7
nebo	nebo	k8xC
mátou	máta	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
vysoký	vysoký	k2eAgInSc1d1
obsah	obsah	k1gInSc1
vitamínu	vitamín	k1gInSc2
B.	B.	kA
</s>
<s>
Pivo	pivo	k1gNnSc1
<g/>
,	,	kIx,
oblíbený	oblíbený	k2eAgInSc4d1
nápoj	nápoj	k1gInSc4
<g/>
,	,	kIx,
místní	místní	k2eAgFnPc4d1
značky	značka	k1gFnPc4
Oboloň	Oboloň	k1gFnSc2
<g/>
,	,	kIx,
Černihivske	Černihivske	k1gNnSc2
<g/>
,	,	kIx,
Lvivske	Lvivske	k1gNnSc2
<g/>
,	,	kIx,
Rogan	Rogan	k1gMnSc1
<g/>
,	,	kIx,
Slavutyč	Slavutyč	k1gMnSc1
</s>
<s>
Víno	víno	k1gNnSc1
<g/>
,	,	kIx,
nejčastěji	často	k6eAd3
sladká	sladký	k2eAgFnSc1d1
dezertní	dezertní	k2eAgFnSc1d1
vína	vína	k1gFnSc1
podobná	podobný	k2eAgFnSc1d1
portskému	portské	k1gNnSc3
nebo	nebo	k8xC
sherry	sherry	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Horilka	horilka	k1gFnSc1
(	(	kIx(
<g/>
vodka	vodka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
národní	národní	k2eAgInSc4d1
nápoj	nápoj	k1gInSc4
<g/>
,	,	kIx,
destiluje	destilovat	k5eAaImIp3nS
se	se	k3xPyFc4
z	z	k7c2
obilí	obilí	k1gNnSc2
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
z	z	k7c2
pšenice	pšenice	k1gFnSc2
či	či	k8xC
žita	žito	k1gNnSc2
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
ale	ale	k9
také	také	k9
z	z	k7c2
brambor	brambora	k1gFnPc2
<g/>
,	,	kIx,
medu	med	k1gInSc2
či	či	k8xC
cukrové	cukrový	k2eAgFnSc2d1
řepy	řepa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obsahuje	obsahovat	k5eAaImIp3nS
40	#num#	k4
%	%	kIx~
alkoholu	alkohol	k1gInSc2
a	a	k8xC
nejznámější	známý	k2eAgFnSc7d3
značkou	značka	k1gFnSc7
je	být	k5eAaImIp3nS
Nemiroff	Nemiroff	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podomácku	podomácku	k6eAd1
vyrobená	vyrobený	k2eAgFnSc1d1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
samohonka	samohonka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
až	až	k9
80	#num#	k4
%	%	kIx~
alkoholu	alkohol	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Medovucha	Medovucha	k1gFnSc1
<g/>
,	,	kIx,
národní	národní	k2eAgInSc4d1
nápoj	nápoj	k1gInSc4
<g/>
,	,	kIx,
fermentovaný	fermentovaný	k2eAgInSc4d1
nápoj	nápoj	k1gInSc4
z	z	k7c2
medu	med	k1gInSc2
<g/>
,	,	kIx,
vody	voda	k1gFnSc2
a	a	k8xC
kvasnic	kvasnice	k1gFnPc2
s	s	k7c7
nízkým	nízký	k2eAgInSc7d1
obsahem	obsah	k1gInSc7
alkoholu	alkohol	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
ukrajinská	ukrajinský	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
https://www.mundo.cz/ukrajina/kuchyne	https://www.mundo.cz/ukrajina/kuchynout	k5eAaPmIp3nS
</s>
<s>
https://www.labuznik.cz/recept/kuchyne/ukrajinska/	https://www.labuznik.cz/recept/kuchyne/ukrajinska/	k?
</s>
<s>
https://www.cestujlevne.com/pruvodce/ukrajina/jidlo	https://www.cestujlevne.com/pruvodce/ukrajina/jidlo	k1gNnSc1
</s>
<s>
https://www.cestolino.cz/pruvodce/ukrajina/mistni-kuchyne/	https://www.cestolino.cz/pruvodce/ukrajina/mistni-kuchyne/	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Evropská	evropský	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
Nezávislé	závislý	k2eNgFnSc2d1
státy	stát	k1gInPc1
</s>
<s>
Albánie	Albánie	k1gFnSc1
•	•	k?
Andorra	Andorra	k1gFnSc1
•	•	k?
Arménie	Arménie	k1gFnSc2
•	•	k?
Ázerbájdžán	Ázerbájdžán	k1gInSc1
•	•	k?
Belgie	Belgie	k1gFnSc1
•	•	k?
Bělorusko	Bělorusko	k1gNnSc1
•	•	k?
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
•	•	k?
Bulharsko	Bulharsko	k1gNnSc1
•	•	k?
Černá	černat	k5eAaImIp3nS
Hora	hora	k1gFnSc1
•	•	k?
Česko	Česko	k1gNnSc1
•	•	k?
Dánsko	Dánsko	k1gNnSc1
•	•	k?
Estonsko	Estonsko	k1gNnSc1
•	•	k?
Finsko	Finsko	k1gNnSc1
•	•	k?
Francie	Francie	k1gFnSc2
•	•	k?
Gruzie	Gruzie	k1gFnSc2
•	•	k?
Chorvatsko	Chorvatsko	k1gNnSc1
•	•	k?
Irsko	Irsko	k1gNnSc4
•	•	k?
Island	Island	k1gInSc1
•	•	k?
Itálie	Itálie	k1gFnSc2
•	•	k?
Kazachstán	Kazachstán	k1gInSc1
•	•	k?
Kypr	Kypr	k1gInSc1
•	•	k?
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1
•	•	k?
Litva	Litva	k1gFnSc1
•	•	k?
Lotyšsko	Lotyšsko	k1gNnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Lucembursko	Lucembursko	k1gNnSc1
•	•	k?
Maďarsko	Maďarsko	k1gNnSc1
•	•	k?
Malta	Malta	k1gFnSc1
•	•	k?
Moldavsko	Moldavsko	k1gNnSc1
•	•	k?
Monako	Monako	k1gNnSc1
•	•	k?
Německo	Německo	k1gNnSc1
•	•	k?
Nizozemsko	Nizozemsko	k1gNnSc1
•	•	k?
Norsko	Norsko	k1gNnSc1
•	•	k?
Polsko	Polsko	k1gNnSc1
•	•	k?
Portugalsko	Portugalsko	k1gNnSc1
•	•	k?
Rakousko	Rakousko	k1gNnSc1
•	•	k?
Rumunsko	Rumunsko	k1gNnSc1
•	•	k?
Rusko	Rusko	k1gNnSc1
•	•	k?
Řecko	Řecko	k1gNnSc1
•	•	k?
San	San	k1gMnSc1
Marino	Marina	k1gFnSc5
•	•	k?
Severní	severní	k2eAgFnSc1d1
Makedonie	Makedonie	k1gFnSc1
•	•	k?
Slovensko	Slovensko	k1gNnSc1
•	•	k?
Slovinsko	Slovinsko	k1gNnSc1
•	•	k?
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
•	•	k?
Srbsko	Srbsko	k1gNnSc1
•	•	k?
Španělsko	Španělsko	k1gNnSc1
•	•	k?
Švédsko	Švédsko	k1gNnSc1
•	•	k?
Švýcarsko	Švýcarsko	k1gNnSc1
•	•	k?
Turecko	Turecko	k1gNnSc1
•	•	k?
Ukrajina	Ukrajina	k1gFnSc1
•	•	k?
Vatikán	Vatikán	k1gInSc1
Teritoria	teritorium	k1gNnSc2
<g/>
,	,	kIx,
kolonie	kolonie	k1gFnSc2
a	a	k8xC
zámořská	zámořský	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Alandy	Aland	k1gInPc1
•	•	k?
Faerské	Faerský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
Gibraltar	Gibraltar	k1gInSc1
•	•	k?
Grónsko	Grónsko	k1gNnSc1
•	•	k?
Kosovo	Kosův	k2eAgNnSc1d1
•	•	k?
Man	Man	k1gMnSc1
•	•	k?
Normanské	normanský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
•	•	k?
Špicberky	Špicberky	k1gInPc4
Další	další	k2eAgFnSc1d1
regionální	regionální	k2eAgFnSc1d1
kuchyně	kuchyně	k1gFnSc1
</s>
<s>
Alpy	Alpy	k1gFnPc1
•	•	k?
Andalusie	Andalusie	k1gFnSc2
•	•	k?
Anglie	Anglie	k1gFnSc2
•	•	k?
Baleáry	Baleáry	k1gFnPc4
•	•	k?
Balkán	Balkán	k1gInSc1
•	•	k?
Baskicko	Baskicko	k1gNnSc4
•	•	k?
Baškortostán	Baškortostán	k1gInSc1
•	•	k?
Bavorsko	Bavorsko	k1gNnSc1
•	•	k?
Bretaň	Bretaň	k1gFnSc1
•	•	k?
Cornwall	Cornwall	k1gInSc1
•	•	k?
Extremadura	Extremadura	k1gFnSc1
•	•	k?
Galicie	Galicie	k1gFnSc1
•	•	k?
Gaskoňsko	Gaskoňsko	k1gNnSc1
•	•	k?
Jižní	jižní	k2eAgFnPc1d1
Čechy	Čechy	k1gFnPc1
•	•	k?
Katalánsko	Katalánsko	k1gNnSc1
•	•	k?
Komijská	Komijský	k2eAgFnSc1d1
republika	republika	k1gFnSc1
•	•	k?
Korsika	Korsika	k1gFnSc1
•	•	k?
Krym	Krym	k1gInSc1
•	•	k?
Laponsko	Laponsko	k1gNnSc1
•	•	k?
Livonsko	Livonsko	k1gNnSc1
•	•	k?
Lužice	Lužice	k1gFnSc1
•	•	k?
Morava	Morava	k1gFnSc1
•	•	k?
Mordvinsko	Mordvinsko	k1gNnSc1
•	•	k?
Provensálsko	Provensálsko	k1gNnSc1
•	•	k?
Sardinie	Sardinie	k1gFnSc1
•	•	k?
Sasko	Sasko	k1gNnSc1
•	•	k?
Severní	severní	k2eAgNnSc4d1
Irsko	Irsko	k1gNnSc4
•	•	k?
Sicílie	Sicílie	k1gFnSc1
•	•	k?
Skotsko	Skotsko	k1gNnSc1
•	•	k?
Slezsko	Slezsko	k1gNnSc4
•	•	k?
Tatarstán	Tatarstán	k1gInSc1
•	•	k?
Toskánsko	Toskánsko	k1gNnSc1
•	•	k?
Udmurtsko	Udmurtsko	k1gNnSc4
•	•	k?
Wales	Wales	k1gInSc1
Města	město	k1gNnSc2
</s>
<s>
Barcelona	Barcelona	k1gFnSc1
•	•	k?
Berlín	Berlín	k1gInSc1
•	•	k?
Lyon	Lyon	k1gInSc1
•	•	k?
Madrid	Madrid	k1gInSc1
•	•	k?
Neapol	Neapol	k1gFnSc1
•	•	k?
Paříž	Paříž	k1gFnSc1
•	•	k?
Praha	Praha	k1gFnSc1
•	•	k?
Řím	Řím	k1gInSc1
•	•	k?
Vídeň	Vídeň	k1gFnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Gastronomie	gastronomie	k1gFnSc1
</s>
