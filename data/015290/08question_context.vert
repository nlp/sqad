<s>
Ideologie	ideologie	k1gFnSc1
je	být	k5eAaImIp3nS
propracovaná	propracovaný	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
názorů	názor	k1gInPc2
<g/>
,	,	kIx,
postojů	postoj	k1gInPc2
<g/>
,	,	kIx,
hodnot	hodnota	k1gFnPc2
a	a	k8xC
idejí	idea	k1gFnPc2
s	s	k7c7
apologetickou	apologetický	k2eAgFnSc7d1
nebo	nebo	k8xC
ofenzivní	ofenzivní	k2eAgFnSc7d1
funkcí	funkce	k1gFnSc7
založenou	založený	k2eAgFnSc7d1
na	na	k7c4
formulování	formulování	k1gNnSc4
politických	politický	k2eAgInPc2d1
<g/>
,	,	kIx,
hospodářských	hospodářský	k2eAgInPc2d1
<g/>
,	,	kIx,
světonázorových	světonázorový	k2eAgInPc2d1
a	a	k8xC
<g/>
/	/	kIx~
<g/>
nebo	nebo	k8xC
podobných	podobný	k2eAgInPc2d1
zájmů	zájem	k1gInPc2
určité	určitý	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
<g/>
.	.	kIx.
</s>