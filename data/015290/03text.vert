<s>
Ideologie	ideologie	k1gFnSc1
</s>
<s>
Ideologie	ideologie	k1gFnSc1
je	být	k5eAaImIp3nS
propracovaná	propracovaný	k2eAgFnSc1d1
soustava	soustava	k1gFnSc1
názorů	názor	k1gInPc2
<g/>
,	,	kIx,
postojů	postoj	k1gInPc2
<g/>
,	,	kIx,
hodnot	hodnota	k1gFnPc2
a	a	k8xC
idejí	idea	k1gFnPc2
s	s	k7c7
apologetickou	apologetický	k2eAgFnSc7d1
nebo	nebo	k8xC
ofenzivní	ofenzivní	k2eAgFnSc7d1
funkcí	funkce	k1gFnSc7
založenou	založený	k2eAgFnSc7d1
na	na	k7c4
formulování	formulování	k1gNnSc4
politických	politický	k2eAgInPc2d1
<g/>
,	,	kIx,
hospodářských	hospodářský	k2eAgInPc2d1
<g/>
,	,	kIx,
světonázorových	světonázorový	k2eAgInPc2d1
a	a	k8xC
<g/>
/	/	kIx~
<g/>
nebo	nebo	k8xC
podobných	podobný	k2eAgInPc2d1
zájmů	zájem	k1gInPc2
určité	určitý	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
politické	politický	k2eAgFnSc6d1
a	a	k8xC
společenské	společenský	k2eAgFnSc6d1
praxi	praxe	k1gFnSc6
se	se	k3xPyFc4
ideologie	ideologie	k1gFnSc1
vládnoucí	vládnoucí	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
projevuje	projevovat	k5eAaImIp3nS
např.	např.	kA
ve	v	k7c6
formě	forma	k1gFnSc6
filosofie	filosofie	k1gFnSc2
<g/>
,	,	kIx,
práva	právo	k1gNnSc2
či	či	k8xC
morálky	morálka	k1gFnSc2
<g/>
,	,	kIx,
obecně	obecně	k6eAd1
se	se	k3xPyFc4
skrze	skrze	k?
svou	svůj	k3xOyFgFnSc4
subjektivitu	subjektivita	k1gFnSc4
snaží	snažit	k5eAaImIp3nS
o	o	k7c4
formulaci	formulace	k1gFnSc4
celkového	celkový	k2eAgInSc2d1
výkladu	výklad	k1gInSc2
společnosti	společnost	k1gFnSc2
a	a	k8xC
člověka	člověk	k1gMnSc2
jako	jako	k8xC,k8xS
takového	takový	k3xDgMnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
angloamerickém	angloamerický	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
je	být	k5eAaImIp3nS
obvyklé	obvyklý	k2eAgNnSc1d1
i	i	k9
užívání	užívání	k1gNnSc1
termínu	termín	k1gInSc2
ideologie	ideologie	k1gFnSc2
pro	pro	k7c4
neutrální	neutrální	k2eAgNnSc4d1
označování	označování	k1gNnSc4
ucelené	ucelený	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
myšlenek	myšlenka	k1gFnPc2
a	a	k8xC
výpovědí	výpověď	k1gFnPc2
<g/>
,	,	kIx,
případně	případně	k6eAd1
"	"	kIx"
<g/>
pojem	pojem	k1gInSc4
označující	označující	k2eAgFnSc4d1
soustavu	soustava	k1gFnSc4
symbolických	symbolický	k2eAgInPc2d1
výrazů	výraz	k1gInPc2
s	s	k7c7
mobilizační	mobilizační	k2eAgFnSc7d1
funkcí	funkce	k1gFnSc7
<g/>
,	,	kIx,
legitimizující	legitimizující	k2eAgFnSc1d1
určité	určitý	k2eAgNnSc4d1
jednání	jednání	k1gNnSc4
<g/>
"	"	kIx"
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
evropském	evropský	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
má	mít	k5eAaImIp3nS
termín	termín	k1gInSc1
častěji	často	k6eAd2
pejorativní	pejorativní	k2eAgNnSc4d1
vyznění	vyznění	k1gNnSc4
a	a	k8xC
vyjadřuje	vyjadřovat	k5eAaImIp3nS
též	též	k9
uzavřenost	uzavřenost	k1gFnSc4
<g/>
,	,	kIx,
jednostrannost	jednostrannost	k1gFnSc4
<g/>
,	,	kIx,
iracionální	iracionální	k2eAgInPc4d1
a	a	k8xC
propagandistické	propagandistický	k2eAgInPc4d1
rysy	rys	k1gInPc4
takové	takový	k3xDgFnSc2
soustavy	soustava	k1gFnSc2
myšlenek	myšlenka	k1gFnPc2
a	a	k8xC
výpovědí	výpověď	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Často	často	k6eAd1
se	se	k3xPyFc4
pojmem	pojem	k1gInSc7
ideologie	ideologie	k1gFnSc2
označují	označovat	k5eAaImIp3nP
pouze	pouze	k6eAd1
politické	politický	k2eAgFnSc2d1
ideologie	ideologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Funkce	funkce	k1gFnSc1
ideologie	ideologie	k1gFnSc2
</s>
<s>
Ideologie	ideologie	k1gFnSc1
má	mít	k5eAaImIp3nS
podle	podle	k7c2
amerického	americký	k2eAgMnSc2d1
politologa	politolog	k1gMnSc2
Andrewa	Andrewus	k1gMnSc2
Heywooda	Heywood	k1gMnSc2
více	hodně	k6eAd2
funkcí	funkce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgFnSc4
je	být	k5eAaImIp3nS
vysvětlovací	vysvětlovací	k2eAgFnSc1d1
funkce	funkce	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
pomáhá	pomáhat	k5eAaImIp3nS
vysvětlovat	vysvětlovat	k5eAaImF
politické	politický	k2eAgInPc4d1
jevy	jev	k1gInPc4
a	a	k8xC
události	událost	k1gFnPc4
<g/>
,	,	kIx,
hodnotící	hodnotící	k2eAgFnSc1d1
funkce	funkce	k1gFnSc1
nám	my	k3xPp1nPc3
poskytuje	poskytovat	k5eAaImIp3nS
hodnotový	hodnotový	k2eAgInSc4d1
systém	systém	k1gInSc4
a	a	k8xC
jeho	jeho	k3xOp3gNnPc4
kritéria	kritérion	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
orientační	orientační	k2eAgFnSc3d1
funkci	funkce	k1gFnSc3
se	se	k3xPyFc4
můžeme	moct	k5eAaImIp1nP
snáze	snadno	k6eAd2
připojit	připojit	k5eAaPmF
k	k	k7c3
nějaké	nějaký	k3yIgFnSc3
sociální	sociální	k2eAgFnSc3d1
skupině	skupina	k1gFnSc3
a	a	k8xC
funkce	funkce	k1gFnSc1
programová	programový	k2eAgFnSc1d1
nám	my	k3xPp1nPc3
nabízí	nabízet	k5eAaImIp3nS
základní	základní	k2eAgInPc4d1
rysy	rys	k1gInPc4
politického	politický	k2eAgInSc2d1
programu	program	k1gInSc2
a	a	k8xC
politické	politický	k2eAgInPc4d1
cíle	cíl	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
pojmu	pojem	k1gInSc2
ideologie	ideologie	k1gFnSc2
a	a	k8xC
její	její	k3xOp3gNnSc1
chápání	chápání	k1gNnSc1
jednotlivými	jednotlivý	k2eAgMnPc7d1
mysliteli	myslitel	k1gMnPc7
</s>
<s>
Za	za	k7c2
tvůrce	tvůrce	k1gMnSc2
pojmu	pojem	k1gInSc2
ideologie	ideologie	k1gFnSc2
je	být	k5eAaImIp3nS
označován	označován	k2eAgMnSc1d1
francouzský	francouzský	k2eAgMnSc1d1
osvícenecký	osvícenecký	k2eAgMnSc1d1
aristokrat	aristokrat	k1gMnSc1
a	a	k8xC
filosof	filosof	k1gMnSc1
Antoine	Antoin	k1gInSc5
Destutt	Destuttum	k1gNnPc2
de	de	k?
Tracy	Traca	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
se	se	k3xPyFc4
za	za	k7c4
Direktoria	direktorium	k1gNnPc4
podílel	podílet	k5eAaImAgMnS
na	na	k7c6
vzniku	vznik	k1gInSc6
Národního	národní	k2eAgInSc2d1
institutu	institut	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
později	pozdě	k6eAd2
působil	působit	k5eAaImAgMnS
v	v	k7c6
Sekci	sekce	k1gFnSc6
morálních	morální	k2eAgFnPc2d1
a	a	k8xC
politických	politický	k2eAgFnPc2d1
věd	věda	k1gFnPc2
na	na	k7c4
Oddělení	oddělení	k1gNnSc4
analýzy	analýza	k1gFnSc2
vjemů	vjem	k1gInPc2
a	a	k8xC
idejí	idea	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Slovo	slovo	k1gNnSc1
„	„	k?
<g/>
idéologie	idéologie	k1gFnSc2
<g/>
“	“	k?
bylo	být	k5eAaImAgNnS
poprvé	poprvé	k6eAd1
použito	použít	k5eAaPmNgNnS
na	na	k7c6
přednášce	přednáška	k1gFnSc6
20	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1796	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
kdy	kdy	k6eAd1
de	de	k?
Tracy	Traca	k1gFnSc2
hledal	hledat	k5eAaImAgInS
příhodný	příhodný	k2eAgInSc1d1
název	název	k1gInSc1
pro	pro	k7c4
svou	svůj	k3xOyFgFnSc4
vědu	věda	k1gFnSc4
o	o	k7c6
idejích	idea	k1gFnPc6
<g/>
,	,	kIx,
„	„	k?
<g/>
Science	Science	k1gFnSc2
des	des	k1gNnSc2
idées	idées	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
zbavenou	zbavený	k2eAgFnSc4d1
veškerých	veškerý	k3xTgFnPc2
metafyzických	metafyzický	k2eAgFnPc2d1
a	a	k8xC
náboženských	náboženský	k2eAgFnPc2d1
konotací	konotace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Destutt	Destutt	k1gInSc1
de	de	k?
Tracy	Traca	k1gFnSc2
tento	tento	k3xDgInSc1
termín	termín	k1gInSc1
poprvé	poprvé	k6eAd1
použil	použít	k5eAaPmAgInS
pro	pro	k7c4
označení	označení	k1gNnSc4
systematicky	systematicky	k6eAd1
kritických	kritický	k2eAgNnPc2d1
a	a	k8xC
terapeutických	terapeutický	k2eAgNnPc2d1
studií	studio	k1gNnPc2
sensualistických	sensualistický	k2eAgInPc2d1
základů	základ	k1gInPc2
idejí	idea	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moderní	moderní	k2eAgInPc1d1
pojetí	pojetí	k1gNnSc4
pojmu	pojem	k1gInSc2
však	však	k9
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
posměšné	posměšný	k2eAgNnSc1d1
na	na	k7c6
základě	základ	k1gInSc6
vnitřní	vnitřní	k2eAgFnSc2d1
souvislosti	souvislost	k1gFnSc2
přenesené	přenesený	k2eAgFnSc2d1
aplikace	aplikace	k1gFnSc2
na	na	k7c4
všechny	všechen	k3xTgInPc4
systémy	systém	k1gInPc4
idejí	idea	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
nelogicky	logicky	k6eNd1
nadřazují	nadřazovat	k5eAaImIp3nP
a	a	k8xC
zároveň	zároveň	k6eAd1
s	s	k7c7
sebou	se	k3xPyFc7
nesou	nést	k5eAaImIp3nP
předpoklad	předpoklad	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
tato	tento	k3xDgFnSc1
absence	absence	k1gFnSc1
realismu	realismus	k1gInSc2
má	mít	k5eAaImIp3nS
svůj	svůj	k3xOyFgInSc4
zdroj	zdroj	k1gInSc4
v	v	k7c6
sebezaslepení	sebezaslepení	k1gNnSc6
a	a	k8xC
sebezaujetí	sebezaujetí	k1gNnSc6
ideologa	ideolog	k1gMnSc2
jako	jako	k8xC,k8xS
reprezentanta	reprezentant	k1gMnSc2
ideologie	ideologie	k1gFnSc2
<g/>
,	,	kIx,
vlastní	vlastní	k2eAgFnSc7d1
tedy	tedy	k9
než	než	k8xS
propagandistickou	propagandistický	k2eAgFnSc4d1
funkci	funkce	k1gFnSc4
<g/>
,	,	kIx,
byť	byť	k8xS
to	ten	k3xDgNnSc1
původně	původně	k6eAd1
proklamátory	proklamátor	k1gInPc1
idejí	idea	k1gFnPc2
nebylo	být	k5eNaImAgNnS
zamýšleno	zamýšlet	k5eAaImNgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
O	o	k7c4
pejorativní	pejorativní	k2eAgInSc4d1
význam	význam	k1gInSc4
slova	slovo	k1gNnSc2
ideologie	ideologie	k1gFnSc2
se	se	k3xPyFc4
zasloužil	zasloužit	k5eAaPmAgMnS
Napoleon	Napoleon	k1gMnSc1
Bonaparte	bonapart	k1gInSc5
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
zpočátku	zpočátku	k6eAd1
podporoval	podporovat	k5eAaImAgInS
institut	institut	k1gInSc1
a	a	k8xC
roku	rok	k1gInSc2
1797	#num#	k4
se	se	k3xPyFc4
dokonce	dokonce	k9
stal	stát	k5eAaPmAgMnS
jeho	jeho	k3xOp3gMnSc7
čestným	čestný	k2eAgMnSc7d1
členem	člen	k1gMnSc7
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
ale	ale	k8xC
později	pozdě	k6eAd2
se	se	k3xPyFc4
od	od	k7c2
Destutta	Destutt	k1gMnSc2
de	de	k?
Tracyho	Tracy	k1gMnSc2
distancoval	distancovat	k5eAaBmAgMnS
<g/>
,	,	kIx,
protože	protože	k8xS
jeho	jeho	k3xOp3gFnSc1
škola	škola	k1gFnSc1
údajně	údajně	k6eAd1
nedokázala	dokázat	k5eNaPmAgFnS
pochopit	pochopit	k5eAaPmF
praktickou	praktický	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yInSc1,k3yQnSc1
se	se	k3xPyFc4
Napoleon	Napoleon	k1gMnSc1
ve	v	k7c6
Francii	Francie	k1gFnSc6
ujal	ujmout	k5eAaPmAgInS
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
došlo	dojít	k5eAaPmAgNnS
mezi	mezi	k7c7
ním	on	k3xPp3gNnSc7
a	a	k8xC
institutem	institut	k1gInSc7
k	k	k7c3
názorovému	názorový	k2eAgNnSc3d1
střetnutí	střetnutí	k1gNnSc3
<g/>
,	,	kIx,
neboť	neboť	k8xC
institut	institut	k1gInSc4
„	„	k?
<g/>
obhajoval	obhajovat	k5eAaImAgMnS
demokratické	demokratický	k2eAgInPc4d1
výsledky	výsledek	k1gInPc4
revoluce	revoluce	k1gFnSc2
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
postupně	postupně	k6eAd1
hrází	hráz	k1gFnSc7
proti	proti	k7c3
absolutistickým	absolutistický	k2eAgFnPc3d1
snahám	snaha	k1gFnPc3
Napoleona	Napoleon	k1gMnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Napoleon	Napoleon	k1gMnSc1
dokonce	dokonce	k9
obvinil	obvinit	k5eAaPmAgMnS
Ideology	ideolog	k1gMnPc4
z	z	k7c2
neúspěšného	úspěšný	k2eNgNnSc2d1
tažení	tažení	k1gNnSc2
do	do	k7c2
Ruska	Rusko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
de	de	k?
Tracy	Traca	k1gFnSc2
slova	slovo	k1gNnSc2
ideolog	ideolog	k1gMnSc1
(	(	kIx(
<g/>
„	„	k?
<g/>
idéologue	idéologue	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
nikdy	nikdy	k6eAd1
nepoužil	použít	k5eNaPmAgMnS
<g/>
,	,	kIx,
neboť	neboť	k8xC
člověk	člověk	k1gMnSc1
zabývající	zabývající	k2eAgFnSc2d1
se	se	k3xPyFc4
ideologií	ideologie	k1gFnSc7
byl	být	k5eAaImAgInS
podle	podle	k7c2
něj	on	k3xPp3gMnSc2
ideologista	ideologist	k1gMnSc2
(	(	kIx(
<g/>
„	„	k?
<g/>
idéologiste	idéologist	k1gMnSc5
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
redaktoři	redaktor	k1gMnPc1
Francouzského	francouzský	k2eAgInSc2d1
akademického	akademický	k2eAgInSc2d1
slovníku	slovník	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
1835	#num#	k4
s	s	k7c7
oběma	dva	k4xCgInPc7
výrazy	výraz	k1gInPc7
</s>
<s>
pracovali	pracovat	k5eAaImAgMnP
jako	jako	k8xS,k8xC
se	s	k7c7
synonymy	synonymum	k1gNnPc7
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
upřednostnili	upřednostnit	k5eAaPmAgMnP
politické	politický	k2eAgFnSc3d1
„	„	k?
<g/>
ideolog	ideolog	k1gMnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
kterým	který	k3yIgMnPc3,k3yQgMnPc3,k3yRgMnPc3
Napoleon	napoleon	k1gInSc1
označoval	označovat	k5eAaImAgInS
své	svůj	k3xOyFgMnPc4
liberální	liberální	k2eAgMnPc4d1
odpůrce	odpůrce	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Z	z	k7c2
tohoto	tento	k3xDgNnSc2
rétorického	rétorický	k2eAgNnSc2d1
pozměnění	pozměnění	k1gNnSc2
významu	význam	k1gInSc2
pochází	pocházet	k5eAaImIp3nS
marxistický	marxistický	k2eAgInSc4d1
vývoj	vývoj	k1gInSc4
ideologie	ideologie	k1gFnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
znevážení	znevážení	k1gNnSc3
věr	věra	k1gFnPc2
<g/>
,	,	kIx,
teorie	teorie	k1gFnSc2
a	a	k8xC
praxe	praxe	k1gFnSc2
odpůrců	odpůrce	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dodnes	dodnes	k6eAd1
je	být	k5eAaImIp3nS
ideologie	ideologie	k1gFnSc1
chápána	chápat	k5eAaImNgFnS
jako	jako	k8xC,k8xS
pojem	pojem	k1gInSc1
užívající	užívající	k2eAgFnSc2d1
se	se	k3xPyFc4
pro	pro	k7c4
hru	hra	k1gFnSc4
moci	moc	k1gFnSc2
a	a	k8xC
resistence	resistence	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
nutně	nutně	k6eAd1
nemusí	muset	k5eNaImIp3nP
být	být	k5eAaImF
jen	jen	k9
politické	politický	k2eAgFnPc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Moderní	moderní	k2eAgFnSc4d1
podobu	podoba	k1gFnSc4
pojem	pojem	k1gInSc4
ideologie	ideologie	k1gFnSc2
nabyl	nabýt	k5eAaPmAgMnS
až	až	k6eAd1
v	v	k7c6
marxismu	marxismus	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Karla	Karel	k1gMnSc2
Marxe	Marx	k1gMnSc2
a	a	k8xC
Friedricha	Friedrich	k1gMnSc2
Engelse	Engels	k1gMnSc2
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
falešné	falešný	k2eAgNnSc4d1
vědomí	vědomí	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
zakrývá	zakrývat	k5eAaImIp3nS
skutečnost	skutečnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uchopení	uchopení	k1gNnSc1
tohoto	tento	k3xDgInSc2
pojmu	pojem	k1gInSc2
se	se	k3xPyFc4
však	však	k9
v	v	k7c6
průběhu	průběh	k1gInSc6
vývoje	vývoj	k1gInSc2
marxismu	marxismus	k1gInSc2
lišilo	lišit	k5eAaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kupříkladu	kupříkladu	k6eAd1
Lenin	Lenin	k2eAgMnSc1d1
se	se	k3xPyFc4
shoduje	shodovat	k5eAaImIp3nS
s	s	k7c7
ostatními	ostatní	k2eAgMnPc7d1
radikálními	radikální	k2eAgMnPc7d1
marxisty	marxista	k1gMnPc7
na	na	k7c4
existenci	existence	k1gFnSc4
dvou	dva	k4xCgInPc2
typů	typ	k1gInPc2
ideologií	ideologie	k1gFnPc2
<g/>
:	:	kIx,
negativní	negativní	k2eAgFnSc1d1
sloužící	sloužící	k1gFnSc1
buržoazii	buržoazie	k1gFnSc4
a	a	k8xC
pozitivní	pozitivní	k2eAgFnSc4d1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
revolučním	revoluční	k2eAgInSc7d1
nástrojem	nástroj	k1gInSc7
pro	pro	k7c4
dělnickou	dělnický	k2eAgFnSc4d1
třídu	třída	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Marx	Marx	k1gMnSc1
naopak	naopak	k6eAd1
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
existuje	existovat	k5eAaImIp3nS
více	hodně	k6eAd2
druhů	druh	k1gInPc2
ideologie	ideologie	k1gFnSc2
<g/>
,	,	kIx,
neboť	neboť	k8xC
každá	každý	k3xTgFnSc1
třída	třída	k1gFnSc1
si	se	k3xPyFc3
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
svou	svůj	k3xOyFgFnSc4
vlastní	vlastní	k2eAgFnSc4d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
Marxe	Marx	k1gMnSc2
vycházel	vycházet	k5eAaImAgInS
i	i	k9
sociolog	sociolog	k1gMnSc1
Karl	Karl	k1gMnSc1
Mannheim	Mannheim	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
výzkumem	výzkum	k1gInSc7
pojmu	pojem	k1gInSc2
zabýval	zabývat	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
nemarxistickém	marxistický	k2eNgNnSc6d1
užití	užití	k1gNnSc6
termínu	termín	k1gInSc2
představují	představovat	k5eAaImIp3nP
ideologie	ideologie	k1gFnPc1
doktríny	doktrína	k1gFnSc2
vyhlašované	vyhlašovaný	k2eAgFnPc1d1
představiteli	představitel	k1gMnPc7
stran	strana	k1gFnPc2
<g/>
,	,	kIx,
prvně	prvně	k?
užité	užitý	k2eAgNnSc4d1
na	na	k7c6
přelomu	přelom	k1gInSc6
19	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Právě	právě	k6eAd1
na	na	k7c6
těchto	tento	k3xDgFnPc6
staví	stavit	k5eAaPmIp3nS,k5eAaBmIp3nS,k5eAaImIp3nS
své	svůj	k3xOyFgFnSc2
teorie	teorie	k1gFnSc2
Karl	Karl	k1gMnSc1
Mannheim	Mannheim	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
Termín	termín	k1gInSc1
ideologie	ideologie	k1gFnSc2
má	mít	k5eAaImIp3nS
pejorativní	pejorativní	k2eAgInSc1d1
nádech	nádech	k1gInSc1
pro	pro	k7c4
některé	některý	k3yIgMnPc4
nemarxistické	marxistický	k2eNgMnPc4d1
autory	autor	k1gMnPc4
v	v	k7c6
těch	ten	k3xDgInPc6
případech	případ	k1gInPc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
stranické	stranický	k2eAgFnSc2d1
doktríny	doktrína	k1gFnSc2
obsahují	obsahovat	k5eAaImIp3nP
monopolistické	monopolistický	k2eAgFnPc1d1
<g/>
,	,	kIx,
propracované	propracovaný	k2eAgFnPc1d1
a	a	k8xC
uzavřené	uzavřený	k2eAgFnPc1d1
ideologie	ideologie	k1gFnPc1
<g/>
,	,	kIx,
jímž	jenž	k3xRgInSc7
je	být	k5eAaImIp3nS
následně	následně	k6eAd1
dodán	dodán	k2eAgInSc1d1
tento	tento	k3xDgInSc1
hanlivý	hanlivý	k2eAgInSc1d1
podtext	podtext	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Marxisté	marxista	k1gMnPc1
i	i	k8xC
nemarxisté	nemarxista	k1gMnPc1
se	se	k3xPyFc4
shodují	shodovat	k5eAaImIp3nP
na	na	k7c4
tezi	teze	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
ve	v	k7c6
všeobecném	všeobecný	k2eAgNnSc6d1
negativním	negativní	k2eAgNnSc6d1
slova	slovo	k1gNnPc1
smyslu	smysl	k1gInSc6
pojem	pojem	k1gInSc1
ideologie	ideologie	k1gFnSc2
znemožňuje	znemožňovat	k5eAaImIp3nS
racionální	racionální	k2eAgFnSc4d1
argumentaci	argumentace	k1gFnSc4
<g/>
,	,	kIx,
nejedná	jednat	k5eNaImIp3nS
se	se	k3xPyFc4
tak	tak	k9
pouze	pouze	k6eAd1
o	o	k7c4
doktrínu	doktrína	k1gFnSc4
bez	bez	k7c2
logických	logický	k2eAgInPc2d1
rozumových	rozumový	k2eAgInPc2d1
podkladů	podklad	k1gInPc2
<g/>
,	,	kIx,
</s>
<s>
ale	ale	k8xC
také	také	k9
o	o	k7c4
systém	systém	k1gInSc4
devastující	devastující	k2eAgInPc4d1
příhodné	příhodný	k2eAgInPc4d1
protiargumenty	protiargument	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dle	dle	k7c2
Mannheima	Mannheim	k1gMnSc2
nejsou	být	k5eNaImIp3nP
ideologie	ideologie	k1gFnPc1
schémata	schéma	k1gNnPc1
sloužící	sloužící	k2eAgNnPc1d1
k	k	k7c3
vytýkání	vytýkání	k1gNnSc3
kvalit	kvalita	k1gFnPc2
vědeckých	vědecký	k2eAgInPc2d1
konstruktů	konstrukt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Domníval	domnívat	k5eAaImAgMnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
ideologie	ideologie	k1gFnSc1
představují	představovat	k5eAaImIp3nP
pyšný	pyšný	k2eAgInSc4d1
kulturní	kulturní	k2eAgInSc4d1
útvar	útvar	k1gInSc4
poskytující	poskytující	k2eAgInPc4d1
směrodatné	směrodatný	k2eAgInPc4d1
výklady	výklad	k1gInPc4
světa	svět	k1gInSc2
a	a	k8xC
jejich	jejich	k3xOp3gFnSc2
funkce	funkce	k1gFnSc2
je	být	k5eAaImIp3nS
nenahraditelná	nahraditelný	k2eNgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Další	další	k2eAgFnSc1d1
odbornější	odborný	k2eAgFnSc1d2
verze	verze	k1gFnSc1
přístupů	přístup	k1gInPc2
k	k	k7c3
ideologii	ideologie	k1gFnSc3
jsou	být	k5eAaImIp3nP
pestrou	pestrý	k2eAgFnSc7d1
škálou	škála	k1gFnSc7
politického	politický	k2eAgNnSc2d1
uvažování	uvažování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neomezují	omezovat	k5eNaImIp3nP
se	se	k3xPyFc4
jen	jen	k9
na	na	k7c4
třídění	třídění	k1gNnSc4
odpovědí	odpověď	k1gFnPc2
k	k	k7c3
jednotlivým	jednotlivý	k2eAgInPc3d1
postojům	postoj	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
empirická	empirický	k2eAgFnSc1d1
práce	práce	k1gFnSc1
se	se	k3xPyFc4
zabývá	zabývat	k5eAaImIp3nS
otázkou	otázka	k1gFnSc7
nastolenou	nastolený	k2eAgFnSc7d1
antropologickými	antropologický	k2eAgInPc7d1
přístupy	přístup	k1gInPc7
<g/>
,	,	kIx,
totiž	totiž	k9
je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
ideologie	ideologie	k1gFnSc1
prostě	prostě	k6eAd1
jakýmkoli	jakýkoli	k3yIgInSc7
druhem	druh	k1gInSc7
symbolické	symbolický	k2eAgFnSc2d1
soustavy	soustava	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
může	moct	k5eAaImIp3nS
lidi	člověk	k1gMnPc4
vybavit	vybavit	k5eAaPmF
politickou	politický	k2eAgFnSc7d1
identitou	identita	k1gFnSc7
a	a	k8xC
řídit	řídit	k5eAaImF
jejich	jejich	k3xOp3gInPc4
politické	politický	k2eAgInPc4d1
postoje	postoj	k1gInPc4
<g/>
,	,	kIx,
nebo	nebo	k8xC
zda	zda	k8xS
má	mít	k5eAaImIp3nS
zvláštní	zvláštní	k2eAgInPc4d1
rozlišitelné	rozlišitelný	k2eAgInPc4d1
rysy	rys	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sociologové	sociolog	k1gMnPc1
od	od	k7c2
sebe	sebe	k3xPyFc4
oddělují	oddělovat	k5eAaImIp3nP
dva	dva	k4xCgInPc1
typy	typ	k1gInPc1
vzniku	vznik	k1gInSc2
a	a	k8xC
vývoje	vývoj	k1gInSc2
pojmu	pojem	k1gInSc2
ideologie	ideologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeden	jeden	k4xCgInSc1
označují	označovat	k5eAaImIp3nP
jako	jako	k8xS,k8xC
hodnotící	hodnotící	k2eAgInSc1d1
a	a	k8xC
druhý	druhý	k4xOgInSc1
jako	jako	k9
nehodnotící	hodnotící	k2eNgInSc1d1
<g/>
,	,	kIx,
avšak	avšak	k8xC
oba	dva	k4xCgInPc1
způsoby	způsob	k1gInPc1
v	v	k7c6
detailu	detail	k1gInSc6
hodnotí	hodnotit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Minimálně	minimálně	k6eAd1
ve	v	k7c6
smyslu	smysl	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
ani	ani	k8xC
jeden	jeden	k4xCgInSc1
neuznává	uznávat	k5eNaImIp3nS
platnost	platnost	k1gFnSc4
idejí	idea	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
podle	podle	k7c2
nich	on	k3xPp3gMnPc2
ideologické	ideologický	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odlišné	odlišný	k2eAgInPc1d1
je	být	k5eAaImIp3nS
pouze	pouze	k6eAd1
chápání	chápání	k1gNnSc1
ideologie	ideologie	k1gFnSc2
jako	jako	k8xS,k8xC
politické	politický	k2eAgFnSc2d1
nebo	nebo	k8xC
sociální	sociální	k2eAgFnSc2d1
filosofické	filosofický	k2eAgFnSc2d1
myšlenky	myšlenka	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
teprve	teprve	k6eAd1
uspořádána	uspořádán	k2eAgFnSc1d1
a	a	k8xC
diskutována	diskutován	k2eAgFnSc1d1
<g/>
,	,	kIx,
takto	takto	k6eAd1
užívají	užívat	k5eAaImIp3nP
termín	termín	k1gInSc4
především	především	k9
angličtí	anglický	k2eAgMnPc1d1
akademičtí	akademický	k2eAgMnPc1d1
autoři	autor	k1gMnPc1
politické	politický	k2eAgFnSc2d1
literatury	literatura	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c4
teoretická	teoretický	k2eAgNnPc4d1
rozpracování	rozpracování	k1gNnSc4
termínu	termín	k1gInSc2
ideologie	ideologie	k1gFnSc2
byli	být	k5eAaImAgMnP
v	v	k7c6
minulosti	minulost	k1gFnSc6
bezesporu	bezesporu	k9
nejproduktivnější	produktivní	k2eAgMnPc1d3
marxisté	marxista	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
se	se	k3xPyFc4
snažili	snažit	k5eAaImAgMnP
pomocí	pomocí	k7c2
kritického	kritický	k2eAgNnSc2d1
ideologického	ideologický	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
vysvětlit	vysvětlit	k5eAaPmF
<g/>
,	,	kIx,
z	z	k7c2
jakých	jaký	k3yRgInPc2,k3yQgInPc2,k3yIgInPc2
důvodů	důvod	k1gInPc2
dělníci	dělník	k1gMnPc1
nepokračovali	pokračovat	k5eNaImAgMnP
</s>
<s>
v	v	k7c6
šíření	šíření	k1gNnSc6
revolučních	revoluční	k2eAgFnPc2d1
nálad	nálada	k1gFnPc2
prosazovaných	prosazovaný	k2eAgFnPc2d1
Marxem	Marx	k1gMnSc7
jako	jako	k8xS,k8xC
antitezí	antiteze	k1gFnSc7
k	k	k7c3
dominantním	dominantní	k2eAgFnPc3d1
ideologiím	ideologie	k1gFnPc3
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc1
se	se	k3xPyFc4
sám	sám	k3xTgMnSc1
Marx	Marx	k1gMnSc1
rozhodl	rozhodnout	k5eAaPmAgMnS
poodkrýt	poodkrýt	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
marxismus	marxismus	k1gInSc1
ve	v	k7c4
finále	finále	k1gNnSc4
ideologii	ideologie	k1gFnSc3
de	de	k?
facto	facto	k1gNnSc4
ořezal	ořezat	k5eAaPmAgInS
natolik	natolik	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
rovna	roven	k2eAgFnSc1d1
jakémukoli	jakýkoli	k3yIgInSc3
popisu	popis	k1gInSc3
pozornosti	pozornost	k1gFnSc2
společenského	společenský	k2eAgMnSc2d1
činitele	činitel	k1gMnSc2
<g/>
,	,	kIx,
o	o	k7c6
kterém	který	k3yQgInSc6,k3yIgInSc6,k3yRgInSc6
předpokládáme	předpokládat	k5eAaImIp1nP
<g/>
,	,	kIx,
že	že	k8xS
má	mít	k5eAaImIp3nS
jedinečný	jedinečný	k2eAgInSc1d1
zájem	zájem	k1gInSc1
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
ekonomický	ekonomický	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jednotlivá	jednotlivý	k2eAgNnPc4d1
pojetí	pojetí	k1gNnPc4
ideologie	ideologie	k1gFnSc2
</s>
<s>
Pojetí	pojetí	k1gNnSc1
ideologie	ideologie	k1gFnSc2
dle	dle	k7c2
Karla	Karel	k1gMnSc2
Marxe	Marx	k1gMnSc2
</s>
<s>
Pojem	pojem	k1gInSc1
ideologie	ideologie	k1gFnSc2
u	u	k7c2
Marxe	Marx	k1gMnSc2
se	se	k3xPyFc4
od	od	k7c2
obecného	obecný	k2eAgInSc2d1
pojmu	pojem	k1gInSc2
velmi	velmi	k6eAd1
liší	lišit	k5eAaImIp3nP
<g/>
,	,	kIx,
ale	ale	k8xC
právě	právě	k9
s	s	k7c7
jeho	jeho	k3xOp3gFnSc7
osobou	osoba	k1gFnSc7
získává	získávat	k5eAaImIp3nS
pojem	pojem	k1gInSc1
větší	veliký	k2eAgInSc1d2
význam	význam	k1gInSc4
<g/>
,	,	kIx,
než	než	k8xS
měl	mít	k5eAaImAgMnS
doposud	doposud	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slovo	slovo	k1gNnSc4
ideologie	ideologie	k1gFnSc2
použil	použít	k5eAaPmAgMnS
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
raném	raný	k2eAgNnSc6d1
díle	dílo	k1gNnSc6
Německá	německý	k2eAgFnSc1d1
ideologie	ideologie	k1gFnSc1
(	(	kIx(
<g/>
1846	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
napsal	napsat	k5eAaPmAgInS,k5eAaBmAgInS
spolu	spolu	k6eAd1
s	s	k7c7
Friedrichem	Friedrich	k1gMnSc7
Engelsem	Engels	k1gMnSc7
<g/>
.	.	kIx.
v	v	k7c6
tomto	tento	k3xDgInSc6
díle	díl	k1gInSc6
je	být	k5eAaImIp3nS
ideologie	ideologie	k1gFnSc1
chápána	chápán	k2eAgFnSc1d1
coby	coby	k?
„	„	k?
<g/>
Myšlenky	myšlenka	k1gFnSc2
vládnoucí	vládnoucí	k2eAgFnSc2d1
třídy	třída	k1gFnSc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
v	v	k7c6
každé	každý	k3xTgFnSc6
epoše	epocha	k1gFnSc6
myšlenkami	myšlenka	k1gFnPc7
vládnoucími	vládnoucí	k2eAgFnPc7d1
<g/>
,	,	kIx,
tj.	tj.	kA
třída	třída	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
vládnoucí	vládnoucí	k2eAgFnSc7d1
materiální	materiální	k2eAgFnSc7d1
mocí	moc	k1gFnSc7
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
současně	současně	k6eAd1
její	její	k3xOp3gFnSc7
vládnoucí	vládnoucí	k2eAgFnSc7d1
duchovní	duchovní	k2eAgFnSc7d1
mocí	moc	k1gFnSc7
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Marx	Marx	k1gMnSc1
je	být	k5eAaImIp3nS
toho	ten	k3xDgInSc2
názoru	názor	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
v	v	k7c6
ideologii	ideologie	k1gFnSc6
šíří	šířit	k5eAaImIp3nS
nepravdivé	pravdivý	k2eNgNnSc1d1
</s>
<s>
vidění	vidění	k1gNnSc4
světa	svět	k1gInSc2
<g/>
,	,	kIx,
dochází	docházet	k5eAaImIp3nS
zde	zde	k6eAd1
tak	tak	k6eAd1
k	k	k7c3
mystifikaci	mystifikace	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
jev	jev	k1gInSc4
později	pozdě	k6eAd2
nazval	nazvat	k5eAaBmAgMnS,k5eAaPmAgMnS
Friedrich	Friedrich	k1gMnSc1
Engels	Engels	k1gMnSc1
jako	jako	k8xS,k8xC
falešné	falešný	k2eAgNnSc1d1
vědomí	vědomí	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dle	dle	k7c2
Marxe	Marx	k1gMnSc2
je	být	k5eAaImIp3nS
ideologie	ideologie	k1gFnSc1
spjata	spjat	k2eAgFnSc1d1
s	s	k7c7
třídním	třídní	k2eAgInSc7d1
systémem	systém	k1gInSc7
a	a	k8xC
je	být	k5eAaImIp3nS
projevem	projev	k1gInSc7
moci	moc	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
nerovnoměrně	rovnoměrně	k6eNd1
rozdělena	rozdělit	k5eAaPmNgFnS
mezi	mezi	k7c4
společenské	společenský	k2eAgFnPc4d1
vrstvy	vrstva	k1gFnPc4
(	(	kIx(
<g/>
třídy	třída	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Marx	Marx	k1gMnSc1
také	také	k9
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
ideologie	ideologie	k1gFnSc1
je	být	k5eAaImIp3nS
jen	jen	k9
dočasný	dočasný	k2eAgInSc1d1
jev	jev	k1gInSc1
<g/>
,	,	kIx,
protože	protože	k8xS
proletariát	proletariát	k1gInSc1
žádnou	žádný	k3yNgFnSc4
ideologii	ideologie	k1gFnSc4
nepotřebuje	potřebovat	k5eNaImIp3nS
<g/>
,	,	kIx,
protože	protože	k8xS
je	být	k5eAaImIp3nS
jedinou	jediný	k2eAgFnSc7d1
třídou	třída	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
nepotřebuje	potřebovat	k5eNaImIp3nS
iluze	iluze	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pojetí	pojetí	k1gNnSc1
ideologie	ideologie	k1gFnSc2
podle	podle	k7c2
hnutí	hnutí	k1gNnSc2
navazujícího	navazující	k2eAgNnSc2d1
na	na	k7c4
myšlenky	myšlenka	k1gFnPc4
marxismu	marxismus	k1gInSc2
</s>
<s>
Ideologie	ideologie	k1gFnSc1
už	už	k6eAd1
nepředstavuje	představovat	k5eNaImIp3nS
jen	jen	k9
nepravdu	nepravda	k1gFnSc4
(	(	kIx(
<g/>
mystifikaci	mystifikace	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Termín	termín	k1gInSc1
je	být	k5eAaImIp3nS
zbaven	zbaven	k2eAgMnSc1d1
hanlivých	hanlivý	k2eAgFnPc2d1
a	a	k8xC
negativních	negativní	k2eAgFnPc2d1
konotací	konotace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lenin	Lenin	k1gMnSc1
například	například	k6eAd1
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
díle	dílo	k1gNnSc6
Co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
dělat	dělat	k5eAaImF
<g/>
?	?	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
1902	#num#	k4
<g/>
)	)	kIx)
hovoří	hovořit	k5eAaImIp3nS
o	o	k7c6
proletářských	proletářský	k2eAgFnPc6d1
idejích	idea	k1gFnPc6
jako	jako	k8xC,k8xS
o	o	k7c6
„	„	k?
<g/>
socialistické	socialistický	k2eAgFnSc3d1
ideologii	ideologie	k1gFnSc3
<g/>
“	“	k?
či	či	k8xC
„	„	k?
<g/>
marxistické	marxistický	k2eAgFnSc3d1
ideologii	ideologie	k1gFnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pojetí	pojetí	k1gNnSc1
ideologie	ideologie	k1gFnSc2
dle	dle	k7c2
Antonia	Antonio	k1gMnSc2
Gramsciho	Gramsci	k1gMnSc2
</s>
<s>
Italský	italský	k2eAgMnSc1d1
politik	politik	k1gMnSc1
a	a	k8xC
marxistický	marxistický	k2eAgMnSc1d1
filosof	filosof	k1gMnSc1
Antonio	Antonio	k1gMnSc1
Gramsci	Gramsek	k1gMnPc1
došel	dojít	k5eAaPmAgInS
při	při	k7c6
rozvíjení	rozvíjení	k1gNnSc6
marxistické	marxistický	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
ideologie	ideologie	k1gFnSc2
nejdále	daleko	k6eAd3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
třídní	třídní	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
se	se	k3xPyFc4
objevuje	objevovat	k5eAaImIp3nS
v	v	k7c6
kapitalismu	kapitalismus	k1gInSc6
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
udržován	udržován	k2eAgMnSc1d1
jen	jen	k9
nerovnou	rovný	k2eNgFnSc7d1
ekonomikou	ekonomika	k1gFnSc7
a	a	k8xC
politickou	politický	k2eAgFnSc7d1
mocí	moc	k1gFnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
vládou	vláda	k1gFnSc7
buržoazních	buržoazní	k2eAgFnPc2d1
idejí	idea	k1gFnPc2
a	a	k8xC
teorií	teorie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zastával	zastávat	k5eAaImAgMnS
také	také	k9
názor	názor	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
ideologie	ideologie	k1gFnSc1
objevuje	objevovat	k5eAaImIp3nS
ve	v	k7c6
všech	všecek	k3xTgFnPc6
vrstvách	vrstva	k1gFnPc6
ve	v	k7c6
společnosti	společnost	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mannheimovo	Mannheimův	k2eAgNnSc1d1
pojetí	pojetí	k1gNnSc1
ideologie	ideologie	k1gFnSc2
</s>
<s>
Maďarský	maďarský	k2eAgMnSc1d1
sociolog	sociolog	k1gMnSc1
Karl	Karl	k1gMnSc1
Mannheim	Mannheim	k1gInSc4
reprezentuje	reprezentovat	k5eAaImIp3nS
směr	směr	k1gInSc4
v	v	k7c6
sociologickém	sociologický	k2eAgNnSc6d1
myšlení	myšlení	k1gNnSc6
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
můžeme	moct	k5eAaImIp1nP
označit	označit	k5eAaPmF
za	za	k7c4
„	„	k?
<g/>
kritiku	kritika	k1gFnSc4
ideologií	ideologie	k1gFnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Směr	směr	k1gInSc1
kritizuje	kritizovat	k5eAaImIp3nS
hlavně	hlavně	k9
ideologický	ideologický	k2eAgInSc1d1
a	a	k8xC
utopický	utopický	k2eAgInSc1d1
způsob	způsob	k1gInSc1
myšlení	myšlení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mannheim	Mannheim	k1gInSc1
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
Marxova	Marxův	k2eAgNnSc2d1
pojetí	pojetí	k1gNnSc2
ideologie	ideologie	k1gFnSc2
jako	jako	k8xC,k8xS
falešného	falešný	k2eAgNnSc2d1
vědomí	vědomí	k1gNnSc2
a	a	k8xC
jako	jako	k9
procesu	proces	k1gInSc2
tvorby	tvorba	k1gFnSc2
idejí	idea	k1gFnPc2
z	z	k7c2
idejí	idea	k1gFnPc2
samotných	samotný	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s>
Považuje	považovat	k5eAaImIp3nS
naši	náš	k3xOp1gFnSc4
dobu	doba	k1gFnSc4
za	za	k7c4
dobu	doba	k1gFnSc4
konfliktu	konflikt	k1gInSc2
mezi	mezi	k7c7
ideologiemi	ideologie	k1gFnPc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
má	mít	k5eAaImIp3nS
ničivý	ničivý	k2eAgInSc4d1
dopad	dopad	k1gInSc4
na	na	k7c4
lidstvo	lidstvo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
zastává	zastávat	k5eAaImIp3nS
názor	názor	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
současnému	současný	k2eAgInSc3d1
světu	svět	k1gInSc3
a	a	k8xC
dějinám	dějiny	k1gFnPc3
lze	lze	k6eAd1
porozumět	porozumět	k5eAaPmF
jen	jen	k9
skrze	skrze	k?
porozumění	porozumění	k1gNnSc1
podstatě	podstata	k1gFnSc6
ideologických	ideologický	k2eAgInPc2d1
systémů	systém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
Mannheima	Mannheima	k1gNnSc4
představuje	představovat	k5eAaImIp3nS
ideologie	ideologie	k1gFnSc1
soubor	soubor	k1gInSc4
idejí	idea	k1gFnSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
jsou	být	k5eAaImIp3nP
vztahy	vztah	k1gInPc1
idejí	idea	k1gFnPc2
k	k	k7c3
jejich	jejich	k3xOp3gMnPc3
nositelům	nositel	k1gMnPc3
<g/>
,	,	kIx,
k	k	k7c3
jejich	jejich	k3xOp3gInPc3
zájmům	zájem	k1gInPc3
a	a	k8xC
sociálním	sociální	k2eAgFnPc3d1
pozicím	pozice	k1gFnPc3
důležitější	důležitý	k2eAgInSc4d2
než	než	k8xS
samotný	samotný	k2eAgInSc4d1
obsah	obsah	k1gInSc4
idejí	idea	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pojem	pojem	k1gInSc1
ideologie	ideologie	k1gFnSc2
je	být	k5eAaImIp3nS
víceznačný	víceznačný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Můžeme	moct	k5eAaImIp1nP
oddělit	oddělit	k5eAaPmF
dva	dva	k4xCgInPc4
významy	význam	k1gInPc4
pojmu	pojem	k1gInSc2
„	„	k?
<g/>
ideologie	ideologie	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
první	první	k4xOgMnSc1
lze	lze	k6eAd1
označit	označit	k5eAaPmF
za	za	k7c4
dílčí	dílčí	k2eAgInSc4d1
a	a	k8xC
druhý	druhý	k4xOgMnSc1
za	za	k7c4
totální	totální	k2eAgInSc4d1
pojem	pojem	k1gInSc4
ideologie	ideologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Dílčí	dílčí	k2eAgInSc1d1
pojem	pojem	k1gInSc1
vypovídá	vypovídat	k5eAaImIp3nS,k5eAaPmIp3nS
pouze	pouze	k6eAd1
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
určitým	určitý	k2eAgFnPc3d1
„	„	k?
<g/>
představám	představa	k1gFnPc3
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
idejím	idea	k1gFnPc3
<g/>
“	“	k?
nechceme	chtít	k5eNaImIp1nP
věřit	věřit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgFnPc1
ideje	idea	k1gFnPc1
totiž	totiž	k9
pokládáme	pokládat	k5eAaImIp1nP
za	za	k7c4
zastření	zastření	k1gNnSc4
faktického	faktický	k2eAgInSc2d1
stavu	stav	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
není	být	k5eNaImIp3nS
vhod	vhod	k6eAd1
tvůrci	tvůrce	k1gMnPc1
těchto	tento	k3xDgFnPc2
představ	představa	k1gFnPc2
(	(	kIx(
<g/>
idejí	idea	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c4
tento	tento	k3xDgInSc4
pojem	pojem	k1gInSc4
můžeme	moct	k5eAaImIp1nP
zahrnout	zahrnout	k5eAaPmF
vše	všechen	k3xTgNnSc4
od	od	k7c2
vědomé	vědomý	k2eAgFnSc2d1
lži	lež	k1gFnSc2
až	až	k9
po	po	k7c6
klamání	klamání	k1gNnSc6
sebe	sebe	k3xPyFc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
ideologii	ideologie	k1gFnSc4
označuje	označovat	k5eAaImIp3nS
jen	jen	k9
část	část	k1gFnSc1
tvrzení	tvrzení	k1gNnSc2
určité	určitý	k2eAgFnSc2d1
osoby	osoba	k1gFnSc2
a	a	k8xC
to	ten	k3xDgNnSc1
pouze	pouze	k6eAd1
vzhledem	vzhledem	k7c3
k	k	k7c3
obsahovosti	obsahovost	k1gFnSc3
výpovědi	výpověď	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osoba	osoba	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
zastírá	zastírat	k5eAaImIp3nS
fakta	faktum	k1gNnPc4
<g/>
,	,	kIx,
a	a	k8xC
její	její	k3xOp3gMnSc1
potenciální	potenciální	k2eAgMnSc1d1
odhalitel	odhalitel	k1gMnSc1
jsou	být	k5eAaImIp3nP
na	na	k7c6
stejné	stejný	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lži	lež	k1gFnSc6
a	a	k8xC
zastírání	zastírání	k1gNnSc6
faktů	fakt	k1gInPc2
určité	určitý	k2eAgFnSc2d1
osoby	osoba	k1gFnSc2
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
ještě	ještě	k9
u	u	k7c2
dílčího	dílčí	k2eAgInSc2d1
pojmu	pojem	k1gInSc2
odhaleny	odhalen	k2eAgInPc1d1
<g/>
,	,	kIx,
nejde	jít	k5eNaImIp3nS
to	ten	k3xDgNnSc4
o	o	k7c4
radikální	radikální	k2eAgNnSc4d1
podezření	podezření	k1gNnSc4
z	z	k7c2
ideologie	ideologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Funkcionalizování	Funkcionalizování	k1gNnSc1
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
jen	jen	k9
v	v	k7c6
psychologické	psychologický	k2eAgFnSc6d1
rovině	rovina	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
k	k	k7c3
zastírání	zastírání	k1gNnSc3
pravdy	pravda	k1gFnSc2
vedou	vést	k5eAaImIp3nP
osobu	osoba	k1gFnSc4
určité	určitý	k2eAgInPc4d1
zájmy	zájem	k1gInPc4
<g/>
,	,	kIx,
kterých	který	k3yIgInPc2,k3yRgInPc2,k3yQgInPc2
chce	chtít	k5eAaImIp3nS
dosáhnout	dosáhnout	k5eAaPmF
<g/>
,	,	kIx,
proto	proto	k8xC
pracuje	pracovat	k5eAaImIp3nS
partikulární	partikulární	k2eAgInSc1d1
pojem	pojem	k1gInSc1
ideologie	ideologie	k1gFnSc2
především	především	k6eAd1
s	s	k7c7
psychologií	psychologie	k1gFnSc7
zájmů	zájem	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
O	o	k7c4
totální	totální	k2eAgMnPc4d1
(	(	kIx(
<g/>
radikální	radikální	k2eAgMnPc4d1
<g/>
)	)	kIx)
pojem	pojem	k1gInSc1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
<g/>
,	,	kIx,
pokud	pokud	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
ideologii	ideologie	k1gFnSc4
období	období	k1gNnSc2
nebo	nebo	k8xC
určité	určitý	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
(	(	kIx(
<g/>
např.	např.	kA
třídy	třída	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
tím	ten	k3xDgNnSc7
myšlena	myšlen	k2eAgFnSc1d1
zvláštnost	zvláštnost	k1gFnSc1
a	a	k8xC
povaha	povaha	k1gFnSc1
totální	totální	k2eAgFnSc2d1
struktury	struktura	k1gFnSc2
vědomí	vědomí	k1gNnSc2
tohoto	tento	k3xDgNnSc2
období	období	k1gNnSc2
nebo	nebo	k8xC
skupiny	skupina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Totální	totální	k2eAgFnSc1d1
ideologie	ideologie	k1gFnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
celý	celý	k2eAgInSc4d1
myšlenkový	myšlenkový	k2eAgInSc4d1
systém	systém	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
charakterizuje	charakterizovat	k5eAaBmIp3nS
určitou	určitý	k2eAgFnSc4d1
skupinu	skupina	k1gFnSc4
nebo	nebo	k8xC
dobu	doba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
se	se	k3xPyFc4
zde	zde	k6eAd1
funkcionalizuje	funkcionalizovat	k5eAaImIp3nS,k5eAaBmIp3nS,k5eAaPmIp3nS
celá	celý	k2eAgFnSc1d1
teoretická	teoretický	k2eAgFnSc1d1
rovina	rovina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
pojetí	pojetí	k1gNnSc1
ideologie	ideologie	k1gFnSc2
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nS
pracovat	pracovat	k5eAaImF
hlavně	hlavně	k9
s	s	k7c7
formalizovaným	formalizovaný	k2eAgInSc7d1
a	a	k8xC
objektivním	objektivní	k2eAgInSc7d1
pojmem	pojem	k1gInSc7
funkce	funkce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tady	tady	k6eAd1
se	se	k3xPyFc4
často	často	k6eAd1
jedna	jeden	k4xCgFnSc1
strana	strana	k1gFnSc1
snaží	snažit	k5eAaImIp3nS
zničit	zničit	k5eAaPmF
druhou	druhý	k4xOgFnSc7
a	a	k8xC
to	ten	k3xDgNnSc1
jak	jak	k6eAd1
její	její	k3xOp3gInPc4
obsahy	obsah	k1gInPc4
a	a	k8xC
stanoviska	stanovisko	k1gNnPc4
tak	tak	k9
i	i	k8xC
její	její	k3xOp3gFnSc4
duchovní	duchovní	k2eAgFnSc4d1
základnu	základna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
tomuto	tento	k3xDgNnSc3
extrémnímu	extrémní	k2eAgNnSc3d1
vyhrocení	vyhrocení	k1gNnSc3
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
strany	strana	k1gFnPc1
destruují	destruovat	k5eAaImIp3nP
i	i	k9
duchovní	duchovní	k2eAgFnPc1d1
podstaty	podstata	k1gFnPc1
opozice	opozice	k1gFnSc2
<g/>
,	,	kIx,
mohlo	moct	k5eAaImAgNnS
dojít	dojít	k5eAaPmF
až	až	k9
v	v	k7c6
moderní	moderní	k2eAgFnSc6d1
společnosti	společnost	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
vytvořily	vytvořit	k5eAaPmAgFnP
rozhodující	rozhodující	k2eAgFnPc1d1
sociální	sociální	k2eAgFnPc1d1
polarity	polarita	k1gFnPc1
nesené	nesený	k2eAgFnPc1d1
odlišnou	odlišný	k2eAgFnSc7d1
světovou	světový	k2eAgFnSc7d1
vůlí	vůle	k1gFnSc7
</s>
<s>
O	o	k7c4
podezření	podezření	k1gNnSc4
z	z	k7c2
ideologie	ideologie	k1gFnSc2
můžeme	moct	k5eAaImIp1nP
podle	podle	k7c2
Mannheima	Mannheimum	k1gNnSc2
mluvit	mluvit	k5eAaImF
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
přirozená	přirozený	k2eAgFnSc1d1
lidská	lidský	k2eAgFnSc1d1
nedůvěra	nedůvěra	k1gFnSc1
se	se	k3xPyFc4
stává	stávat	k5eAaImIp3nS
metodickou	metodický	k2eAgFnSc4d1
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
nepřátelské	přátelský	k2eNgInPc1d1
názory	názor	k1gInPc1
neberou	brát	k5eNaImIp3nP
jen	jen	k9
jako	jako	k9
vylhané	vylhaný	k2eAgNnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
objevuje	objevovat	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
nich	on	k3xPp3gFnPc6
neschopnost	neschopnost	k1gFnSc1
pravdy	pravda	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
vysvětluje	vysvětlovat	k5eAaImIp3nS
jako	jako	k9
funkce	funkce	k1gFnSc1
sociálního	sociální	k2eAgNnSc2d1
zakotvení	zakotvení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Partikulární	partikulární	k2eAgFnSc1d1
ideologie	ideologie	k1gFnSc1
se	se	k3xPyFc4
často	často	k6eAd1
transformuje	transformovat	k5eAaBmIp3nS
v	v	k7c4
totální	totální	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poukazování	poukazování	k1gNnSc1
na	na	k7c4
chyby	chyba	k1gFnPc4
druhé	druhý	k4xOgFnSc2
strany	strana	k1gFnSc2
přechází	přecházet	k5eAaImIp3nS
ve	v	k7c4
snahu	snaha	k1gFnSc4
podrobit	podrobit	k5eAaPmF
sociologické	sociologický	k2eAgFnSc3d1
kritice	kritika	k1gFnSc3
celé	celá	k1gFnSc2
její	její	k3xOp3gNnSc4
myšlení	myšlení	k1gNnSc4
a	a	k8xC
vědomí	vědomí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Ideologie	ideologie	k1gFnSc1
v	v	k7c6
době	doba	k1gFnSc6
totalitních	totalitní	k2eAgInPc2d1
režimů	režim	k1gInPc2
a	a	k8xC
během	během	k7c2
studené	studený	k2eAgFnSc2d1
války	válka	k1gFnSc2
</s>
<s>
V	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
bylo	být	k5eAaImAgNnS
upozorňováno	upozorňovat	k5eAaImNgNnS
hlavně	hlavně	k9
na	na	k7c4
úlohu	úloha	k1gFnSc4
ideologie	ideologie	k1gFnSc2
na	na	k7c4
režimy	režim	k1gInPc4
v	v	k7c6
Itálii	Itálie	k1gFnSc6
(	(	kIx(
<g/>
fašismus	fašismus	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
Německu	Německo	k1gNnSc6
(	(	kIx(
<g/>
nacismus	nacismus	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Rusku	Ruska	k1gFnSc4
(	(	kIx(
<g/>
komunismus	komunismus	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavními	hlavní	k2eAgMnPc7d1
osobami	osoba	k1gFnPc7
tohoto	tento	k3xDgNnSc2
období	období	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
se	se	k3xPyFc4
vývojem	vývoj	k1gInSc7
ideologie	ideologie	k1gFnSc2
zabývali	zabývat	k5eAaImAgMnP
<g/>
,	,	kIx,
byli	být	k5eAaImAgMnP
Karl	Karl	k1gMnSc1
Popper	Popper	k1gMnSc1
<g/>
,	,	kIx,
Hannah	Hannah	k1gMnSc1
Arendtová	Arendtová	k1gFnSc1
<g/>
,	,	kIx,
J.	J.	kA
L.	L.	kA
Talmon	Talmon	k1gMnSc1
a	a	k8xC
Bernard	Bernard	k1gMnSc1
Crick	Crick	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
ideologiích	ideologie	k1gFnPc6
typu	typ	k1gInSc2
komunismu	komunismus	k1gInSc2
nebo	nebo	k8xC
fašismu	fašismus	k1gInSc2
tvrdili	tvrdit	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
ideologie	ideologie	k1gFnPc1
„	„	k?
<g/>
uzavřené	uzavřený	k2eAgNnSc1d1
<g/>
“	“	k?
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
si	se	k3xPyFc3
nárokují	nárokovat	k5eAaImIp3nP
monopol	monopol	k1gInSc4
na	na	k7c4
pravdu	pravda	k1gFnSc4
a	a	k8xC
odmítají	odmítat	k5eAaImIp3nP
tolerovat	tolerovat	k5eAaImF
myšlenky	myšlenka	k1gFnPc1
a	a	k8xC
názory	názor	k1gInPc1
s	s	k7c7
nimi	on	k3xPp3gInPc7
neslučitelné	slučitelný	k2eNgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ideologie	ideologie	k1gFnPc1
jsou	být	k5eAaImIp3nP
tak	tak	k9
„	„	k?
<g/>
sekulární	sekulární	k2eAgNnSc1d1
náboženství	náboženství	k1gNnSc1
<g/>
“	“	k?
<g/>
:	:	kIx,
mají	mít	k5eAaImIp3nP
totalizující	totalizující	k2eAgFnSc4d1
povahu	povaha	k1gFnSc4
a	a	k8xC
slouží	sloužit	k5eAaImIp3nS
jako	jako	k8xS,k8xC
nástroje	nástroj	k1gInPc4
sociální	sociální	k2eAgFnSc2d1
kontroly	kontrola	k1gFnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
zajišťují	zajišťovat	k5eAaImIp3nP
servilitu	servilita	k1gFnSc4
a	a	k8xC
poslušnost	poslušnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Poppera	Popper	k1gMnSc2
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
například	například	k6eAd1
liberalismus	liberalismus	k1gInSc4
ideologií	ideologie	k1gFnPc2
„	„	k?
<g/>
otevřenou	otevřený	k2eAgFnSc7d1
<g/>
“	“	k?
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
založen	založit	k5eAaPmNgInS
na	na	k7c6
svobodě	svoboda	k1gFnSc6
<g/>
,	,	kIx,
toleranci	tolerance	k1gFnSc6
a	a	k8xC
různosti	různost	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pojem	pojem	k1gInSc1
ideologie	ideologie	k1gFnSc2
od	od	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Pojem	pojem	k1gInSc1
je	být	k5eAaImIp3nS
nově	nově	k6eAd1
definován	definován	k2eAgInSc1d1
a	a	k8xC
to	ten	k3xDgNnSc1
pro	pro	k7c4
potřeby	potřeba	k1gFnPc4
běžné	běžný	k2eAgFnSc2d1
společenské	společenský	k2eAgFnSc2d1
a	a	k8xC
politické	politický	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
<g/>
,	,	kIx,
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
tak	tak	k9
neutrálním	neutrální	k2eAgMnSc7d1
a	a	k8xC
s	s	k7c7
objektivním	objektivní	k2eAgInSc7d1
obsahem	obsah	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
zbaven	zbavit	k5eAaPmNgMnS
politické	politický	k2eAgFnSc2d1
zátěže	zátěž	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
s	s	k7c7
ním	on	k3xPp3gMnSc7
byla	být	k5eAaImAgFnS
v	v	k7c6
minulosti	minulost	k1gFnSc6
spojována	spojovat	k5eAaImNgFnS
<g/>
.	.	kIx.
</s>
<s>
Vnímání	vnímání	k1gNnSc1
ideologie	ideologie	k1gFnSc2
napříč	napříč	k7c7
politickým	politický	k2eAgNnSc7d1
spektrem	spektrum	k1gNnSc7
</s>
<s>
Podle	podle	k7c2
A.	A.	kA
Heywooda	Heywooda	k1gFnSc1
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
liberálové	liberál	k1gMnPc1
na	na	k7c6
ideologii	ideologie	k1gFnSc6
pohlížejí	pohlížet	k5eAaImIp3nP
jako	jako	k9
na	na	k7c4
oficiálně	oficiálně	k6eAd1
sankcionovaný	sankcionovaný	k2eAgInSc4d1
systém	systém	k1gInSc4
názorů	názor	k1gInPc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
si	se	k3xPyFc3
nárokuje	nárokovat	k5eAaImIp3nS
monopol	monopol	k1gInSc4
na	na	k7c4
pravdu	pravda	k1gFnSc4
<g/>
,	,	kIx,
často	často	k6eAd1
za	za	k7c2
pomoci	pomoc	k1gFnSc2
pochybného	pochybný	k2eAgNnSc2d1
tvrzení	tvrzení	k1gNnSc2
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
vědecký	vědecký	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ideologie	ideologie	k1gFnSc1
má	mít	k5eAaImIp3nS
tudíž	tudíž	k8xC
utlačovatelskou	utlačovatelský	k2eAgFnSc4d1
až	až	k6eAd1
totalitní	totalitní	k2eAgFnSc4d1
povahu	povaha	k1gFnSc4
<g/>
,	,	kIx,
markantními	markantní	k2eAgInPc7d1
příklady	příklad	k1gInPc7
ideologie	ideologie	k1gFnSc2
jsou	být	k5eAaImIp3nP
komunismus	komunismus	k1gInSc4
a	a	k8xC
fašismus	fašismus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
konzervativců	konzervativec	k1gMnPc2
je	být	k5eAaImIp3nS
ideologie	ideologie	k1gFnSc1
projev	projev	k1gInSc4
arogance	arogance	k1gFnSc2
racionalismu	racionalismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ideologie	ideologie	k1gFnPc1
jsou	být	k5eAaImIp3nP
propracované	propracovaný	k2eAgInPc4d1
myšlenkové	myšlenkový	k2eAgInPc4d1
systémy	systém	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
jsou	být	k5eAaImIp3nP
nebezpečné	bezpečný	k2eNgInPc1d1
či	či	k8xC
nespolehlivé	spolehlivý	k2eNgInPc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
jsou	být	k5eAaImIp3nP
odtržené	odtržený	k2eAgInPc1d1
od	od	k7c2
reality	realita	k1gFnSc2
<g/>
,	,	kIx,
kladou	klást	k5eAaImIp3nP
si	se	k3xPyFc3
tak	tak	k6eAd1
nedosažitelné	dosažitelný	k2eNgInPc1d1
cíle	cíl	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Socialisté	socialist	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
následují	následovat	k5eAaImIp3nP
Marxe	Marx	k1gMnPc4
<g/>
,	,	kIx,
v	v	k7c6
ideologii	ideologie	k1gFnSc6
viděli	vidět	k5eAaImAgMnP
soubor	soubor	k1gInSc4
myšlenek	myšlenka	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
zakrývají	zakrývat	k5eAaImIp3nP
rozpory	rozpor	k1gInPc4
třídní	třídní	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Fašisté	fašista	k1gMnPc1
se	se	k3xPyFc4
často	často	k6eAd1
se	se	k3xPyFc4
od	od	k7c2
ideologie	ideologie	k1gFnSc2
odvracejí	odvracet	k5eAaImIp3nP
a	a	k8xC
reprezentují	reprezentovat	k5eAaImIp3nP
ji	on	k3xPp3gFnSc4
jako	jako	k8xS,k8xC
„	„	k?
<g/>
světový	světový	k2eAgInSc1d1
názor	názor	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
Weltanschauung	Weltanschauung	k1gMnSc1
<g/>
)	)	kIx)
než	než	k8xS
jako	jako	k9
nějakou	nějaký	k3yIgFnSc4
systematickou	systematický	k2eAgFnSc4d1
filosofii	filosofie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Ekologové	ekolog	k1gMnPc1
zastávají	zastávat	k5eAaImIp3nP
názor	názor	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
ideologie	ideologie	k1gFnSc1
negativně	negativně	k6eAd1
poznamenaná	poznamenaný	k2eAgFnSc1d1
svým	svůj	k3xOyFgNnSc7
napojením	napojení	k1gNnSc7
na	na	k7c4
arogantní	arogantní	k2eAgInSc4d1
humanismus	humanismus	k1gInSc4
a	a	k8xC
na	na	k7c4
ekonomiku	ekonomika	k1gFnSc4
orientovanou	orientovaný	k2eAgFnSc4d1
na	na	k7c4
růst	růst	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Náboženští	náboženský	k2eAgMnPc1d1
fundamentalisté	fundamentalista	k1gMnPc1
přistupují	přistupovat	k5eAaImIp3nP
k	k	k7c3
hlavním	hlavní	k2eAgInPc3d1
náboženským	náboženský	k2eAgInPc3d1
textům	text	k1gInPc3
jako	jako	k8xS,k8xC
k	k	k7c3
ideologii	ideologie	k1gFnSc3
a	a	k8xC
to	ten	k3xDgNnSc1
z	z	k7c2
důvodu	důvod	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
jsou	být	k5eAaImIp3nP
zjeveným	zjevený	k2eAgNnSc7d1
slovem	slovo	k1gNnSc7
božím	boží	k2eAgNnSc7d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odmítají	odmítat	k5eAaImIp3nP
tak	tak	k6eAd1
sekulární	sekulární	k2eAgFnPc1d1
teorie	teorie	k1gFnPc1
<g/>
,	,	kIx,
nejsou	být	k5eNaImIp3nP
totiž	totiž	k9
opřeny	opřen	k2eAgInPc1d1
o	o	k7c4
náboženské	náboženský	k2eAgInPc4d1
principy	princip	k1gInPc4
a	a	k8xC
postrádají	postrádat	k5eAaImIp3nP
tak	tak	k6eAd1
mravní	mravní	k2eAgFnSc4d1
podstatu	podstata	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Jakub	Jakub	k1gMnSc1
Rákosník	rákosník	k1gMnSc1
<g/>
:	:	kIx,
Právo	právo	k1gNnSc1
v	v	k7c6
bezpráví	bezpráví	k1gNnSc6
<g/>
,	,	kIx,
recenze	recenze	k1gFnSc1
knihy	kniha	k1gFnSc2
Komunistické	komunistický	k2eAgNnSc4d1
právo	právo	k1gNnSc4
v	v	k7c6
Československu	Československo	k1gNnSc6
<g/>
↑	↑	k?
Jan	Jan	k1gMnSc1
Jandourek	Jandourek	k1gMnSc1
<g/>
:	:	kIx,
Sociologický	sociologický	k2eAgInSc1d1
slovník	slovník	k1gInSc1
<g/>
,	,	kIx,
Portál	portál	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heslo	heslo	k1gNnSc4
ideologie	ideologie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
HEYWOOD	HEYWOOD	kA
<g/>
,	,	kIx,
A.	A.	kA
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Politologie	politologie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Eurolex	Eurolex	k1gInSc1
Bohemia	bohemia	k1gFnSc1
<g/>
.	.	kIx.
s.	s.	k?
29	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86861	#num#	k4
<g/>
-	-	kIx~
<g/>
71	#num#	k4
<g/>
-	-	kIx~
<g/>
6.1	6.1	k4
2	#num#	k4
ŠARADÍN	ŠARADÍN	kA
<g/>
,	,	kIx,
P.	P.	kA
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historické	historický	k2eAgFnPc4d1
proměny	proměna	k1gFnPc4
pojmu	pojem	k1gInSc2
ideologie	ideologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Centrum	centrum	k1gNnSc1
pro	pro	k7c4
studium	studium	k1gNnSc4
demokracie	demokracie	k1gFnSc2
a	a	k8xC
kultury	kultura	k1gFnSc2
<g/>
.	.	kIx.
s.	s.	k?
20	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
859	#num#	k4
<g/>
-	-	kIx~
<g/>
5994	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ŠARADÍN	ŠARADÍN	kA
<g/>
,	,	kIx,
P.	P.	kA
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historické	historický	k2eAgFnPc4d1
proměny	proměna	k1gFnPc4
pojmu	pojem	k1gInSc2
ideologie	ideologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Centrum	centrum	k1gNnSc1
pro	pro	k7c4
studium	studium	k1gNnSc4
demokracie	demokracie	k1gFnSc2
a	a	k8xC
kultury	kultura	k1gFnSc2
<g/>
.	.	kIx.
s.	s.	k?
21	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
859	#num#	k4
<g/>
-	-	kIx~
<g/>
5994	#num#	k4
<g/>
-	-	kIx~
<g/>
1.1	1.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
MILLER	Miller	k1gMnSc1
<g/>
,	,	kIx,
D.	D.	kA
<g/>
;	;	kIx,
COLEMANOVÁ	COLEMANOVÁ	kA
<g/>
,	,	kIx,
J.	J.	kA
<g/>
;	;	kIx,
CONNOLY	CONNOLY	kA
<g/>
,	,	kIx,
W.	W.	kA
<g/>
;	;	kIx,
RYAN	RYAN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
A.	A.	kA
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Blackwellova	Blackwellův	k2eAgFnSc1d1
Encyklopedie	encyklopedie	k1gFnSc1
Politického	politický	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ideologie	ideologie	k1gFnSc1
<g/>
.	.	kIx.
s.	s.	k?
190	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Proglas	Proglas	k1gInSc1
<g/>
,	,	kIx,
Jota	jota	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85617	#num#	k4
<g/>
-	-	kIx~
<g/>
47	#num#	k4
<g/>
-	-	kIx~
<g/>
1.1	1.1	k4
2	#num#	k4
3	#num#	k4
ŠARADÍN	ŠARADÍN	kA
<g/>
,	,	kIx,
P.	P.	kA
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historické	historický	k2eAgFnPc4d1
proměny	proměna	k1gFnPc4
pojmu	pojem	k1gInSc2
ideologie	ideologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Centrum	centrum	k1gNnSc1
pro	pro	k7c4
studium	studium	k1gNnSc4
demokracie	demokracie	k1gFnSc2
a	a	k8xC
kultury	kultura	k1gFnSc2
<g/>
.	.	kIx.
s.	s.	k?
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
859	#num#	k4
<g/>
-	-	kIx~
<g/>
5994	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ŠARADÍN	ŠARADÍN	kA
<g/>
,	,	kIx,
P.	P.	kA
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historické	historický	k2eAgFnPc4d1
proměny	proměna	k1gFnPc4
pojmu	pojem	k1gInSc2
ideologie	ideologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Centrum	centrum	k1gNnSc1
pro	pro	k7c4
studium	studium	k1gNnSc4
demokracie	demokracie	k1gFnSc2
a	a	k8xC
kultury	kultura	k1gFnSc2
<g/>
.	.	kIx.
s.	s.	k?
31	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
859	#num#	k4
<g/>
-	-	kIx~
<g/>
5994	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
ŠARADÍN	ŠARADÍN	kA
<g/>
,	,	kIx,
P.	P.	kA
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historické	historický	k2eAgFnPc4d1
proměny	proměna	k1gFnPc4
pojmu	pojem	k1gInSc2
ideologie	ideologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Centrum	centrum	k1gNnSc1
pro	pro	k7c4
studium	studium	k1gNnSc4
demokracie	demokracie	k1gFnSc2
a	a	k8xC
kultury	kultura	k1gFnSc2
<g/>
.	.	kIx.
s.	s.	k?
32	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
859	#num#	k4
<g/>
-	-	kIx~
<g/>
5994	#num#	k4
<g/>
-	-	kIx~
<g/>
1.1	1.1	k4
2	#num#	k4
3	#num#	k4
MILLER	Miller	k1gMnSc1
<g/>
,	,	kIx,
D.	D.	kA
<g/>
;	;	kIx,
COLEMANOVÁ	COLEMANOVÁ	kA
<g/>
,	,	kIx,
J.	J.	kA
<g/>
;	;	kIx,
CONNOLY	CONNOLY	kA
<g/>
,	,	kIx,
W.	W.	kA
<g/>
;	;	kIx,
RYAN	RYAN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
A.	A.	kA
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Blackwellova	Blackwellův	k2eAgFnSc1d1
Encyklopedie	encyklopedie	k1gFnSc1
Politického	politický	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ideologie	ideologie	k1gFnSc1
<g/>
.	.	kIx.
s.	s.	k?
191	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Proglas	Proglas	k1gInSc1
<g/>
,	,	kIx,
Jota	jota	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85617	#num#	k4
<g/>
-	-	kIx~
<g/>
47	#num#	k4
<g/>
-	-	kIx~
<g/>
1.1	1.1	k4
2	#num#	k4
MILLER	Miller	k1gMnSc1
<g/>
,	,	kIx,
D.	D.	kA
<g/>
;	;	kIx,
COLEMANOVÁ	COLEMANOVÁ	kA
<g/>
,	,	kIx,
J.	J.	kA
<g/>
;	;	kIx,
CONNOLY	CONNOLY	kA
<g/>
,	,	kIx,
W.	W.	kA
<g/>
;	;	kIx,
RYAN	RYAN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
A.	A.	kA
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Blackwellova	Blackwellův	k2eAgFnSc1d1
Encyklopedie	encyklopedie	k1gFnSc1
Politického	politický	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ideologie	ideologie	k1gFnSc1
<g/>
.	.	kIx.
s.	s.	k?
192	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Proglas	Proglas	k1gInSc1
<g/>
,	,	kIx,
Jota	jota	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85617	#num#	k4
<g/>
-	-	kIx~
<g/>
47	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
HEYWOOD	HEYWOOD	kA
<g/>
,	,	kIx,
A.	A.	kA
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Politologie	politologie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Eurolex	Eurolex	k1gInSc1
Bohemia	bohemia	k1gFnSc1
<g/>
.	.	kIx.
s.	s.	k?
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86861	#num#	k4
<g/>
-	-	kIx~
<g/>
71	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
↑	↑	k?
HEYWOOD	HEYWOOD	kA
<g/>
,	,	kIx,
A.	A.	kA
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Politologie	politologie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Eurolex	Eurolex	k1gInSc1
Bohemia	bohemia	k1gFnSc1
<g/>
.	.	kIx.
s.	s.	k?
24	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86861	#num#	k4
<g/>
-	-	kIx~
<g/>
71	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
HEYWOOD	HEYWOOD	kA
<g/>
,	,	kIx,
A.	A.	kA
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Politologie	politologie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Eurolex	Eurolex	k1gInSc1
Bohemia	bohemia	k1gFnSc1
<g/>
.	.	kIx.
s.	s.	k?
25	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86861	#num#	k4
<g/>
-	-	kIx~
<g/>
71	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
HEYWOOD	HEYWOOD	kA
<g/>
,	,	kIx,
A.	A.	kA
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Politologie	politologie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Eurolex	Eurolex	k1gInSc1
Bohemia	bohemia	k1gFnSc1
<g/>
.	.	kIx.
s.	s.	k?
25	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86861	#num#	k4
<g/>
-	-	kIx~
<g/>
71	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
HEYWOOD	HEYWOOD	kA
<g/>
,	,	kIx,
A.	A.	kA
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Politologie	politologie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Eurolex	Eurolex	k1gInSc1
Bohemia	bohemia	k1gFnSc1
<g/>
.	.	kIx.
s.	s.	k?
26	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86861	#num#	k4
<g/>
-	-	kIx~
<g/>
71	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
HEYWOOD	HEYWOOD	kA
<g/>
,	,	kIx,
A.	A.	kA
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Politologie	politologie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Eurolex	Eurolex	k1gInSc1
Bohemia	bohemia	k1gFnSc1
<g/>
.	.	kIx.
s.	s.	k?
27	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86861	#num#	k4
<g/>
-	-	kIx~
<g/>
71	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
HEYWOOD	HEYWOOD	kA
<g/>
,	,	kIx,
A.	A.	kA
(	(	kIx(
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Politologie	politologie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Eurolex	Eurolex	k1gInSc1
Bohemia	bohemia	k1gFnSc1
<g/>
.	.	kIx.
s.	s.	k?
31	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86861	#num#	k4
<g/>
-	-	kIx~
<g/>
71	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Politická	politický	k2eAgFnSc1d1
ideologie	ideologie	k1gFnSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
ŠARADÍN	ŠARADÍN	kA
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historické	historický	k2eAgFnPc4d1
proměny	proměna	k1gFnPc4
pojmu	pojem	k1gInSc2
ideologie	ideologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
Centrum	centrum	k1gNnSc1
pro	pro	k7c4
studium	studium	k1gNnSc4
demokracie	demokracie	k1gFnSc2
a	a	k8xC
kultury	kultura	k1gFnSc2
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
.	.	kIx.
121	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85959	#num#	k4
<g/>
-	-	kIx~
<g/>
94	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
HEYWOOD	HEYWOOD	kA
<g/>
,	,	kIx,
Andrew	Andrew	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Politologie	politologie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Eurolex	Eurolex	k1gInSc1
Bohemia	bohemia	k1gFnSc1
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
482	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
86432	#num#	k4
<g/>
-	-	kIx~
<g/>
95	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
MANNHEIM	Mannheim	k1gInSc1
<g/>
,	,	kIx,
Karl	Karl	k1gMnSc1
(	(	kIx(
<g/>
1991	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ideologie	ideologie	k1gFnSc2
a	a	k8xC
utopie	utopie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bratislava	Bratislava	k1gFnSc1
<g/>
:	:	kIx,
Archa	archa	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7115	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
22	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
MILLER	Miller	k1gMnSc1
<g/>
,	,	kIx,
D.	D.	kA
<g/>
;	;	kIx,
COLEMANOVÁ	COLEMANOVÁ	kA
<g/>
,	,	kIx,
J.	J.	kA
<g/>
;	;	kIx,
CONNOLY	CONNOLY	kA
<g/>
,	,	kIx,
W.	W.	kA
<g/>
;	;	kIx,
RYAN	RYAN	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
A.	A.	kA
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Blackwellova	Blackwellův	k2eAgFnSc1d1
Encyklopedie	encyklopedie	k1gFnSc1
Politického	politický	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ideologie	ideologie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
:	:	kIx,
<g/>
Proglas	Proglas	k1gInSc1
<g/>
,	,	kIx,
Jota	jota	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85617	#num#	k4
<g/>
-	-	kIx~
<g/>
47	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
ideologie	ideologie	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
ideologie	ideologie	k1gFnSc2
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Téma	téma	k1gNnSc1
Ideologie	ideologie	k1gFnSc2
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
Reartikulace	Reartikulace	k1gFnSc1
konceptů	koncept	k1gInPc2
ideologie	ideologie	k1gFnSc2
a	a	k8xC
hegemonie	hegemonie	k1gFnSc2
v	v	k7c6
rámci	rámec	k1gInSc6
mediálních	mediální	k2eAgFnPc2d1
studií	studie	k1gFnPc2
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
Beránek	Beránek	k1gMnSc1
<g/>
,	,	kIx,
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
2010	#num#	k4
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4026486-5	4026486-5	k4
</s>
