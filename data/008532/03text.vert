<p>
<s>
Den	den	k1gInSc1	den
boje	boj	k1gInSc2	boj
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
a	a	k8xC	a
demokracii	demokracie	k1gFnSc4	demokracie
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
:	:	kIx,	:
Deň	Deň	k1gMnSc1	Deň
boja	boja	k1gMnSc1	boja
za	za	k7c4	za
slobodu	sloboda	k1gFnSc4	sloboda
a	a	k8xC	a
demokraciu	demokracium	k1gNnSc6	demokracium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
státní	státní	k2eAgInSc4d1	státní
svátek	svátek	k1gInSc4	svátek
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
státní	státní	k2eAgInSc4d1	státní
svátek	svátek	k1gInSc4	svátek
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
každoročně	každoročně	k6eAd1	každoročně
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
daný	daný	k2eAgInSc1d1	daný
dle	dle	k7c2	dle
§	§	k?	§
1	[number]	k4	1
zákona	zákon	k1gInSc2	zákon
číslo	číslo	k1gNnSc4	číslo
245	[number]	k4	245
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Svátek	svátek	k1gInSc1	svátek
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
jako	jako	k8xC	jako
připomenutí	připomenutí	k1gNnSc1	připomenutí
dvou	dva	k4xCgFnPc2	dva
událostí	událost	k1gFnPc2	událost
moderních	moderní	k2eAgFnPc2d1	moderní
českých	český	k2eAgFnPc2d1	Česká
dějin	dějiny	k1gFnPc2	dějiny
spojených	spojený	k2eAgFnPc2d1	spojená
se	s	k7c7	s
studenty	student	k1gMnPc7	student
českých	český	k2eAgFnPc2d1	Česká
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
1939	[number]	k4	1939
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
demonstrace	demonstrace	k1gFnPc4	demonstrace
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
a	a	k8xC	a
nepokoje	nepokoj	k1gInPc4	nepokoj
spojené	spojený	k2eAgInPc4d1	spojený
s	s	k7c7	s
pohřbem	pohřeb	k1gInSc7	pohřeb
studenta	student	k1gMnSc2	student
Jana	Jan	k1gMnSc2	Jan
Opletala	Opletal	k1gMnSc2	Opletal
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
vedení	vedení	k1gNnSc1	vedení
Nacistického	nacistický	k2eAgNnSc2d1	nacistické
Německa	Německo	k1gNnSc2	Německo
o	o	k7c4	o
uzavření	uzavření	k1gNnSc4	uzavření
českých	český	k2eAgFnPc2d1	Česká
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
na	na	k7c4	na
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
provedly	provést	k5eAaPmAgFnP	provést
nacistické	nacistický	k2eAgFnPc1d1	nacistická
bezpečností	bezpečnost	k1gFnSc7	bezpečnost
složky	složka	k1gFnSc2	složka
razie	razie	k1gFnSc2	razie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Brně	Brno	k1gNnSc6	Brno
a	a	k8xC	a
Příbrami	Příbram	k1gFnSc6	Příbram
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
dopadnout	dopadnout	k5eAaPmF	dopadnout
vedoucí	vedoucí	k1gFnSc2	vedoucí
studentských	studentská	k1gFnPc2	studentská
organizaci	organizace	k1gFnSc4	organizace
a	a	k8xC	a
internovat	internovat	k5eAaBmF	internovat
ostatní	ostatní	k2eAgMnPc4d1	ostatní
studenty	student	k1gMnPc4	student
<g/>
.	.	kIx.	.
</s>
<s>
Devět	devět	k4xCc1	devět
představitelů	představitel	k1gMnPc2	představitel
studentských	studentský	k2eAgMnPc2d1	studentský
vůdců	vůdce	k1gMnPc2	vůdce
(	(	kIx(	(
<g/>
Josef	Josef	k1gMnSc1	Josef
Adamec	Adamec	k1gMnSc1	Adamec
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Černý	Černý	k1gMnSc1	Černý
<g/>
,	,	kIx,	,
Marek	Marek	k1gMnSc1	Marek
Frauwirth	Frauwirth	k1gMnSc1	Frauwirth
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Klíma	Klíma	k1gMnSc1	Klíma
<g/>
,	,	kIx,	,
Bedřich	Bedřich	k1gMnSc1	Bedřich
Koula	Koula	k1gMnSc1	Koula
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Matoušek	Matoušek	k1gMnSc1	Matoušek
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Skorkovský	Skorkovský	k2eAgMnSc1d1	Skorkovský
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Šaffránek	Šaffránek	k1gMnSc1	Šaffránek
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
Weinert	Weinert	k1gMnSc1	Weinert
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
bez	bez	k7c2	bez
soudu	soud	k1gInSc2	soud
popraveno	popraven	k2eAgNnSc1d1	popraveno
v	v	k7c6	v
Ruzyňských	ruzyňský	k2eAgFnPc6d1	Ruzyňská
kasárnách	kasárny	k1gFnPc6	kasárny
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
1200	[number]	k4	1200
zatčených	zatčený	k2eAgMnPc2d1	zatčený
studentů	student	k1gMnPc2	student
bylo	být	k5eAaImAgNnS	být
poté	poté	k6eAd1	poté
převezeno	převézt	k5eAaPmNgNnS	převézt
do	do	k7c2	do
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
Sachsenhausen-Oranienburg	Sachsenhausen-Oranienburg	k1gInSc1	Sachsenhausen-Oranienburg
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
jich	on	k3xPp3gInPc2	on
byla	být	k5eAaImAgFnS	být
většina	většina	k1gFnSc1	většina
propuštěna	propustit	k5eAaPmNgFnS	propustit
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc4	zbytek
pak	pak	k6eAd1	pak
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1943	[number]	k4	1943
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgMnPc2	tento
1200	[number]	k4	1200
studentů	student	k1gMnPc2	student
nepřežilo	přežít	k5eNaPmAgNnS	přežít
útrapy	útrapa	k1gFnPc4	útrapa
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
táborů	tábor	k1gInPc2	tábor
35	[number]	k4	35
<g/>
.	.	kIx.	.
<g/>
Ústřední	ústřední	k2eAgInSc1d1	ústřední
svaz	svaz	k1gInSc1	svaz
československého	československý	k2eAgNnSc2d1	Československé
studentstva	studentstvo	k1gNnSc2	studentstvo
vyvíjel	vyvíjet	k5eAaImAgMnS	vyvíjet
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
československé	československý	k2eAgFnSc2d1	Československá
exilové	exilový	k2eAgFnSc2d1	exilová
vlády	vláda	k1gFnSc2	vláda
aktivní	aktivní	k2eAgFnSc4d1	aktivní
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gNnSc7	jejíž
vyvrcholením	vyvrcholení	k1gNnSc7	vyvrcholení
byla	být	k5eAaImAgFnS	být
právě	právě	k6eAd1	právě
londýnská	londýnský	k2eAgFnSc1d1	londýnská
schůze	schůze	k1gFnSc1	schůze
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
studentské	studentský	k2eAgFnSc2d1	studentská
rady	rada	k1gFnSc2	rada
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
přijala	přijmout	k5eAaPmAgFnS	přijmout
takzvané	takzvaný	k2eAgNnSc4d1	takzvané
Prohlášení	prohlášení	k1gNnSc4	prohlášení
spojeneckých	spojenecký	k2eAgMnPc2d1	spojenecký
studentů	student	k1gMnPc2	student
k	k	k7c3	k
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
Mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
dnem	den	k1gInSc7	den
studentstva	studentstvo	k1gNnSc2	studentstvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
1989	[number]	k4	1989
===	===	k?	===
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
událost	událost	k1gFnSc1	událost
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
připomínka	připomínka	k1gFnSc1	připomínka
50	[number]	k4	50
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
nacistických	nacistický	k2eAgFnPc2d1	nacistická
represí	represe	k1gFnPc2	represe
přerostla	přerůst	k5eAaPmAgFnS	přerůst
v	v	k7c6	v
demonstraci	demonstrace	k1gFnSc6	demonstrace
proti	proti	k7c3	proti
komunistickému	komunistický	k2eAgInSc3d1	komunistický
režimu	režim	k1gInSc3	režim
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
se	se	k3xPyFc4	se
na	na	k7c6	na
Albertově	Albertův	k2eAgFnSc6d1	Albertova
sešli	sejít	k5eAaPmAgMnP	sejít
studenti	student	k1gMnPc1	student
pražských	pražský	k2eAgFnPc2d1	Pražská
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
:	:	kIx,	:
<g/>
40	[number]	k4	40
se	se	k3xPyFc4	se
na	na	k7c6	na
Albertově	Albertův	k2eAgFnSc6d1	Albertova
nacházelo	nacházet	k5eAaImAgNnS	nacházet
přibližně	přibližně	k6eAd1	přibližně
500	[number]	k4	500
až	až	k9	až
600	[number]	k4	600
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gInSc1	jejich
počet	počet	k1gInSc1	počet
poměrně	poměrně	k6eAd1	poměrně
rychle	rychle	k6eAd1	rychle
narůstal	narůstat	k5eAaImAgInS	narůstat
nově	nově	k6eAd1	nově
příchozími	příchozí	k1gMnPc7	příchozí
<g/>
.	.	kIx.	.
</s>
<s>
Manifestace	manifestace	k1gFnSc1	manifestace
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
zpěvem	zpěv	k1gInSc7	zpěv
písně	píseň	k1gFnSc2	píseň
Gaudeamus	Gaudeamus	k1gInSc1	Gaudeamus
igitur	igitura	k1gFnPc2	igitura
a	a	k8xC	a
projevem	projev	k1gInSc7	projev
Martina	Martin	k1gMnSc2	Martin
Klímy	Klíma	k1gMnSc2	Klíma
z	z	k7c2	z
uskupení	uskupení	k1gNnSc2	uskupení
Nezávislých	závislý	k2eNgMnPc2d1	nezávislý
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgFnSc1d1	státní
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
odhadovala	odhadovat	k5eAaImAgFnS	odhadovat
počet	počet	k1gInSc4	počet
účastníků	účastník	k1gMnPc2	účastník
až	až	k6eAd1	až
na	na	k7c4	na
15	[number]	k4	15
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
další	další	k2eAgInSc1d1	další
zdroj	zdroj	k1gInSc1	zdroj
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
davu	dav	k1gInSc6	dav
až	až	k9	až
50	[number]	k4	50
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
40	[number]	k4	40
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
část	část	k1gFnSc1	část
manifestace	manifestace	k1gFnSc2	manifestace
ukončena	ukončit	k5eAaPmNgFnS	ukončit
a	a	k8xC	a
pořadatelé	pořadatel	k1gMnPc1	pořadatel
vyzvali	vyzvat	k5eAaPmAgMnP	vyzvat
k	k	k7c3	k
pochodu	pochod	k1gInSc3	pochod
na	na	k7c4	na
Vyšehrad	Vyšehrad	k1gInSc4	Vyšehrad
ke	k	k7c3	k
hrobu	hrob	k1gInSc3	hrob
Karla	Karel	k1gMnSc2	Karel
Hynka	Hynek	k1gMnSc2	Hynek
Máchy	Mácha	k1gMnSc2	Mácha
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
lidí	člověk	k1gMnPc2	člověk
chtěla	chtít	k5eAaImAgFnS	chtít
ale	ale	k8xC	ale
směřovat	směřovat	k5eAaImF	směřovat
na	na	k7c4	na
Václavské	václavský	k2eAgNnSc4d1	Václavské
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
měli	mít	k5eAaImAgMnP	mít
původně	původně	k6eAd1	původně
demonstrující	demonstrující	k2eAgMnPc1d1	demonstrující
dojít	dojít	k5eAaPmF	dojít
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
hlásal	hlásat	k5eAaImAgInS	hlásat
leták	leták	k1gInSc1	leták
vytištěný	vytištěný	k2eAgInSc1d1	vytištěný
k	k	k7c3	k
manifestaci	manifestace	k1gFnSc3	manifestace
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
odhodlanými	odhodlaný	k2eAgMnPc7d1	odhodlaný
byl	být	k5eAaImAgMnS	být
i	i	k9	i
poručík	poručík	k1gMnSc1	poručík
StB	StB	k1gMnSc1	StB
Ludvík	Ludvík	k1gMnSc1	Ludvík
Zifčák	Zifčák	k1gMnSc1	Zifčák
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
později	pozdě	k6eAd2	pozdě
pokládán	pokládat	k5eAaImNgInS	pokládat
za	za	k7c4	za
mrtvého	mrtvý	k2eAgMnSc4d1	mrtvý
studenta	student	k1gMnSc4	student
Martina	Martin	k1gMnSc4	Martin
Šmída	Šmíd	k1gMnSc4	Šmíd
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Vyšehrad	Vyšehrad	k1gInSc4	Vyšehrad
postupně	postupně	k6eAd1	postupně
dorazil	dorazit	k5eAaPmAgInS	dorazit
dav	dav	k1gInSc1	dav
přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
zcela	zcela	k6eAd1	zcela
zaplnili	zaplnit	k5eAaPmAgMnP	zaplnit
prostranství	prostranství	k1gNnSc4	prostranství
před	před	k7c7	před
kostelem	kostel	k1gInSc7	kostel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
18	[number]	k4	18
<g/>
:	:	kIx,	:
<g/>
15	[number]	k4	15
byla	být	k5eAaImAgFnS	být
demonstrace	demonstrace	k1gFnSc1	demonstrace
oficiálně	oficiálně	k6eAd1	oficiálně
ukončena	ukončit	k5eAaPmNgFnS	ukončit
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
oficiální	oficiální	k2eAgFnSc2d1	oficiální
části	část	k1gFnSc2	část
demonstrace	demonstrace	k1gFnSc2	demonstrace
se	se	k3xPyFc4	se
dav	dav	k1gInSc1	dav
neplánovaně	plánovaně	k6eNd1	plánovaně
vydal	vydat	k5eAaPmAgInS	vydat
do	do	k7c2	do
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
5	[number]	k4	5
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Karlovo	Karlův	k2eAgNnSc4d1	Karlovo
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
19	[number]	k4	19
<g/>
:	:	kIx,	:
<g/>
12	[number]	k4	12
přišel	přijít	k5eAaPmAgInS	přijít
rozkaz	rozkaz	k1gInSc1	rozkaz
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
dav	dav	k1gInSc1	dav
na	na	k7c6	na
vhodném	vhodný	k2eAgNnSc6d1	vhodné
místě	místo	k1gNnSc6	místo
zablokovat	zablokovat	k5eAaPmF	zablokovat
<g/>
.	.	kIx.	.
</s>
<s>
Policejní	policejní	k2eAgInSc1d1	policejní
kordon	kordon	k1gInSc1	kordon
zatarasil	zatarasit	k5eAaPmAgInS	zatarasit
Most	most	k1gInSc1	most
1	[number]	k4	1
<g/>
.	.	kIx.	.
máje	máj	k1gInSc2	máj
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
zabránil	zabránit	k5eAaPmAgMnS	zabránit
davu	dav	k1gInSc3	dav
odbočit	odbočit	k5eAaPmF	odbočit
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Pražský	pražský	k2eAgInSc4d1	pražský
hrad	hrad	k1gInSc4	hrad
<g/>
,	,	kIx,	,
demonstrující	demonstrující	k2eAgMnPc1d1	demonstrující
zahnuli	zahnout	k5eAaPmAgMnP	zahnout
na	na	k7c4	na
Národní	národní	k2eAgFnSc4d1	národní
třídu	třída	k1gFnSc4	třída
a	a	k8xC	a
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Václavské	václavský	k2eAgNnSc4d1	Václavské
náměstí	náměstí	k1gNnSc4	náměstí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
<g/>
:	:	kIx,	:
<g/>
25	[number]	k4	25
byla	být	k5eAaImAgFnS	být
kordonem	kordon	k1gInSc7	kordon
přehrazena	přehrazen	k2eAgFnSc1d1	přehrazena
Národní	národní	k2eAgFnSc1d1	národní
třída	třída	k1gFnSc1	třída
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
Perštýna	Perštýna	k1gFnSc1	Perštýna
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
čelo	čelo	k1gNnSc1	čelo
demonstrace	demonstrace	k1gFnSc2	demonstrace
zastaveno	zastavit	k5eAaPmNgNnS	zastavit
<g/>
,	,	kIx,	,
účastníci	účastník	k1gMnPc1	účastník
si	se	k3xPyFc3	se
sedli	sednout	k5eAaPmAgMnP	sednout
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
před	před	k7c4	před
pořádkové	pořádkový	k2eAgFnPc4d1	pořádková
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Dívky	dívka	k1gFnPc1	dívka
začaly	začít	k5eAaPmAgFnP	začít
spontánně	spontánně	k6eAd1	spontánně
zasunovat	zasunovat	k5eAaImF	zasunovat
za	za	k7c4	za
štíty	štít	k1gInPc4	štít
příslušníků	příslušník	k1gMnPc2	příslušník
pohotovostního	pohotovostní	k2eAgInSc2d1	pohotovostní
pluku	pluk	k1gInSc2	pluk
květiny	květina	k1gFnSc2	květina
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
policejní	policejní	k2eAgMnPc1d1	policejní
velitelé	velitel	k1gMnPc1	velitel
obávali	obávat	k5eAaImAgMnP	obávat
opakování	opakování	k1gNnSc4	opakování
předchozí	předchozí	k2eAgFnSc2d1	předchozí
situace	situace	k1gFnSc2	situace
z	z	k7c2	z
Vyšehradské	vyšehradský	k2eAgFnSc2d1	Vyšehradská
ulice	ulice	k1gFnSc2	ulice
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
dav	dav	k1gInSc1	dav
uzávěru	uzávěr	k1gInSc2	uzávěr
obešel	obejít	k5eAaPmAgInS	obejít
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
o	o	k7c4	o
čtvrt	čtvrt	k1xP	čtvrt
hodiny	hodina	k1gFnSc2	hodina
později	pozdě	k6eAd2	pozdě
k	k	k7c3	k
uzavření	uzavření	k1gNnSc3	uzavření
ulic	ulice	k1gFnPc2	ulice
Mikulandské	Mikulandský	k2eAgFnSc2d1	Mikulandská
a	a	k8xC	a
Voršilské	voršilský	k2eAgFnSc2d1	Voršilská
<g/>
,	,	kIx,	,
a	a	k8xC	a
k	k	k7c3	k
zablokování	zablokování	k1gNnSc3	zablokování
ústupu	ústup	k1gInSc2	ústup
Národní	národní	k2eAgFnSc7d1	národní
třídou	třída	k1gFnSc7	třída
zpět	zpět	k6eAd1	zpět
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
Národnímu	národní	k2eAgNnSc3d1	národní
divadlu	divadlo	k1gNnSc3	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
000	[number]	k4	000
demonstrantů	demonstrant	k1gMnPc2	demonstrant
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
uzavřeno	uzavřít	k5eAaPmNgNnS	uzavřít
mezi	mezi	k7c4	mezi
dva	dva	k4xCgInPc4	dva
policejní	policejní	k2eAgInPc4d1	policejní
kordony	kordon	k1gInPc4	kordon
<g/>
.	.	kIx.	.
</s>
<s>
Demonstrující	demonstrující	k2eAgMnPc1d1	demonstrující
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
v	v	k7c6	v
pokojné	pokojný	k2eAgFnSc6d1	pokojná
a	a	k8xC	a
nenásilné	násilný	k2eNgFnSc6d1	nenásilná
demonstraci	demonstrace	k1gFnSc6	demonstrace
za	za	k7c4	za
provolávání	provolávání	k1gNnSc4	provolávání
hesel	heslo	k1gNnPc2	heslo
jako	jako	k8xC	jako
Máme	mít	k5eAaImIp1nP	mít
holé	holý	k2eAgFnPc4d1	holá
ruce	ruka	k1gFnPc4	ruka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
části	část	k1gFnSc6	část
demonstrace	demonstrace	k1gFnSc2	demonstrace
byl	být	k5eAaImAgInS	být
ještě	ještě	k6eAd1	ještě
demonstrantům	demonstrant	k1gMnPc3	demonstrant
umožňován	umožňovat	k5eAaImNgInS	umožňovat
jednotlivě	jednotlivě	k6eAd1	jednotlivě
volný	volný	k2eAgInSc4d1	volný
odchod	odchod	k1gInSc4	odchod
<g/>
.	.	kIx.	.
<g/>
Přibližně	přibližně	k6eAd1	přibližně
ve	v	k7c4	v
čtvrt	čtvrt	k1gFnSc4	čtvrt
na	na	k7c4	na
devět	devět	k4xCc4	devět
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
již	již	k6eAd1	již
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
obklíčení	obklíčení	k1gNnSc4	obklíčení
opustit	opustit	k5eAaPmF	opustit
a	a	k8xC	a
policejní	policejní	k2eAgInSc1d1	policejní
kordon	kordon	k1gInSc1	kordon
postupující	postupující	k2eAgInSc1d1	postupující
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
začal	začít	k5eAaPmAgInS	začít
prostor	prostor	k1gInSc4	prostor
zahušťovat	zahušťovat	k5eAaImF	zahušťovat
<g/>
.	.	kIx.	.
</s>
<s>
Pohotovostní	pohotovostní	k2eAgInSc1d1	pohotovostní
pluk	pluk	k1gInSc1	pluk
veřejné	veřejný	k2eAgFnSc2d1	veřejná
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
a	a	k8xC	a
Odbor	odbor	k1gInSc1	odbor
zvláštního	zvláštní	k2eAgNnSc2d1	zvláštní
určení	určení	k1gNnSc2	určení
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
červené	červený	k2eAgInPc1d1	červený
barety	baret	k1gInPc1	baret
<g/>
)	)	kIx)	)
následně	následně	k6eAd1	následně
začaly	začít	k5eAaPmAgFnP	začít
demonstrující	demonstrující	k2eAgInPc1d1	demonstrující
surově	surově	k6eAd1	surově
bít	bít	k5eAaImF	bít
obušky	obušek	k1gInPc4	obušek
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
byli	být	k5eAaImAgMnP	být
účastníci	účastník	k1gMnPc1	účastník
demonstrace	demonstrace	k1gFnSc2	demonstrace
vyzývání	vyzývání	k1gNnSc2	vyzývání
k	k	k7c3	k
rozchodu	rozchod	k1gInSc3	rozchod
<g/>
,	,	kIx,	,
jediné	jediný	k2eAgFnPc4d1	jediná
únikové	únikový	k2eAgFnPc4d1	úniková
cesty	cesta	k1gFnPc4	cesta
vedly	vést	k5eAaImAgInP	vést
skrz	skrz	k7c4	skrz
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
uličky	ulička	k1gFnSc2	ulička
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
demonstranti	demonstrant	k1gMnPc1	demonstrant
brutálně	brutálně	k6eAd1	brutálně
biti	bít	k5eAaImNgMnP	bít
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
21	[number]	k4	21
<g/>
:	:	kIx,	:
<g/>
20	[number]	k4	20
byla	být	k5eAaImAgFnS	být
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
demonstrace	demonstrace	k1gFnSc2	demonstrace
násilně	násilně	k6eAd1	násilně
rozptýlena	rozptýlen	k2eAgFnSc1d1	rozptýlena
<g/>
;	;	kIx,	;
někteří	některý	k3yIgMnPc1	některý
účastníci	účastník	k1gMnPc1	účastník
byli	být	k5eAaImAgMnP	být
následně	následně	k6eAd1	následně
zatčeni	zatknout	k5eAaPmNgMnP	zatknout
a	a	k8xC	a
naloženi	naložit	k5eAaPmNgMnP	naložit
do	do	k7c2	do
připravených	připravený	k2eAgInPc2d1	připravený
autobusů	autobus	k1gInPc2	autobus
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
po	po	k7c6	po
rozehnání	rozehnání	k1gNnSc6	rozehnání
demonstrace	demonstrace	k1gFnSc1	demonstrace
docházelo	docházet	k5eAaImAgNnS	docházet
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
pořádkových	pořádkový	k2eAgFnPc2d1	pořádková
jednotek	jednotka	k1gFnPc2	jednotka
k	k	k7c3	k
napadání	napadání	k1gNnSc3	napadání
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
či	či	k8xC	či
skupinek	skupinka	k1gFnPc2	skupinka
přihlížejících	přihlížející	k2eAgInPc2d1	přihlížející
<g/>
.	.	kIx.	.
</s>
<s>
Nezávislá	závislý	k2eNgFnSc1d1	nezávislá
lékařská	lékařský	k2eAgFnSc1d1	lékařská
komise	komise	k1gFnSc1	komise
později	pozdě	k6eAd2	pozdě
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
568	[number]	k4	568
lidí	člověk	k1gMnPc2	člověk
bylo	být	k5eAaImAgNnS	být
během	během	k7c2	během
zásahu	zásah	k1gInSc2	zásah
zraněno	zranit	k5eAaPmNgNnS	zranit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Slavení	slavení	k1gNnSc2	slavení
svátku	svátek	k1gInSc2	svátek
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Spor	spor	k1gInSc1	spor
o	o	k7c4	o
název	název	k1gInSc4	název
svátku	svátek	k1gInSc2	svátek
===	===	k?	===
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2016	[number]	k4	2016
schválila	schválit	k5eAaPmAgFnS	schválit
Poslanecká	poslanecký	k2eAgFnSc1d1	Poslanecká
sněmovna	sněmovna	k1gFnSc1	sněmovna
přejmenování	přejmenování	k1gNnSc2	přejmenování
státního	státní	k2eAgInSc2d1	státní
svátku	svátek	k1gInSc2	svátek
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
tento	tento	k3xDgInSc1	tento
návrh	návrh	k1gInSc1	návrh
úspěšně	úspěšně	k6eAd1	úspěšně
prošel	projít	k5eAaPmAgInS	projít
legislativním	legislativní	k2eAgInSc7d1	legislativní
procesem	proces	k1gInSc7	proces
<g/>
,	,	kIx,	,
nesl	nést	k5eAaImAgMnS	nést
by	by	kYmCp3nS	by
svátek	svátek	k1gInSc4	svátek
nově	nova	k1gFnSc3	nova
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
den	den	k1gInSc1	den
studentstva	studentstvo	k1gNnSc2	studentstvo
a	a	k8xC	a
den	den	k1gInSc1	den
boje	boj	k1gInSc2	boj
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
a	a	k8xC	a
demokracii	demokracie	k1gFnSc4	demokracie
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
výrazněji	výrazně	k6eAd2	výrazně
odkazovat	odkazovat	k5eAaImF	odkazovat
na	na	k7c6	na
události	událost	k1gFnSc6	událost
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
krok	krok	k1gInSc1	krok
byl	být	k5eAaImAgInS	být
kritizován	kritizovat	k5eAaImNgInS	kritizovat
některými	některý	k3yIgMnPc7	některý
pravicovými	pravicový	k2eAgMnPc7d1	pravicový
opozičními	opoziční	k2eAgMnPc7d1	opoziční
poslanci	poslanec	k1gMnPc7	poslanec
a	a	k8xC	a
historiky	historik	k1gMnPc7	historik
<g/>
.	.	kIx.	.
</s>
<s>
Poslanecký	poslanecký	k2eAgInSc1d1	poslanecký
návrh	návrh	k1gInSc1	návrh
zákona	zákon	k1gInSc2	zákon
byl	být	k5eAaImAgInS	být
zamítnut	zamítnout	k5eAaPmNgInS	zamítnout
Senátem	senát	k1gInSc7	senát
a	a	k8xC	a
poslanci	poslanec	k1gMnPc1	poslanec
nedokázali	dokázat	k5eNaPmAgMnP	dokázat
jeho	jeho	k3xOp3gNnSc4	jeho
veto	veto	k1gNnSc4	veto
přehlasovat	přehlasovat	k5eAaBmF	přehlasovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zneužití	zneužití	k1gNnSc3	zneužití
výročí	výročí	k1gNnSc3	výročí
===	===	k?	===
</s>
</p>
<p>
<s>
Tohoto	tento	k3xDgInSc2	tento
svátku	svátek	k1gInSc2	svátek
pravidelně	pravidelně	k6eAd1	pravidelně
využívají	využívat	k5eAaPmIp3nP	využívat
k	k	k7c3	k
demonstracím	demonstrace	k1gFnPc3	demonstrace
a	a	k8xC	a
sebepropagaci	sebepropagace	k1gFnSc6	sebepropagace
příslušníci	příslušník	k1gMnPc1	příslušník
krajní	krajní	k2eAgFnSc2d1	krajní
pravice	pravice	k1gFnSc2	pravice
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
dochází	docházet	k5eAaImIp3nS	docházet
i	i	k9	i
ke	k	k7c3	k
střetům	střet	k1gInPc3	střet
<g/>
,	,	kIx,	,
např.	např.	kA	např.
s	s	k7c7	s
anarchisty	anarchista	k1gMnPc7	anarchista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Sametová	sametový	k2eAgFnSc1d1	sametová
revoluce	revoluce	k1gFnSc1	revoluce
</s>
</p>
<p>
<s>
Uzavření	uzavření	k1gNnSc1	uzavření
českých	český	k2eAgFnPc2d1	Česká
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1939	[number]	k4	1939
</s>
</p>
<p>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
den	den	k1gInSc1	den
studentstva	studentstvo	k1gNnSc2	studentstvo
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInPc1d1	státní
svátky	svátek	k1gInPc1	svátek
Slovenska	Slovensko	k1gNnSc2	Slovensko
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Den	dna	k1gFnPc2	dna
boje	boj	k1gInSc2	boj
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
a	a	k8xC	a
demokracii	demokracie	k1gFnSc4	demokracie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
Studenti	student	k1gMnPc1	student
si	se	k3xPyFc3	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
připomněli	připomnět	k5eAaPmAgMnP	připomnět
jak	jak	k8xC	jak
listopad	listopad	k1gInSc4	listopad
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
den	den	k1gInSc1	den
studentstva	studentstvo	k1gNnSc2	studentstvo
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
Zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
245	[number]	k4	245
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
ze	z	k7c2	z
dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
o	o	k7c6	o
státních	státní	k2eAgInPc6d1	státní
svátcích	svátek	k1gInPc6	svátek
<g/>
,	,	kIx,	,
o	o	k7c6	o
ostatních	ostatní	k2eAgInPc6d1	ostatní
svátcích	svátek	k1gInPc6	svátek
<g/>
,	,	kIx,	,
o	o	k7c6	o
významných	významný	k2eAgInPc6d1	významný
dnech	den	k1gInPc6	den
a	a	k8xC	a
o	o	k7c6	o
dnech	den	k1gInPc6	den
pracovního	pracovní	k2eAgInSc2d1	pracovní
klidu	klid	k1gInSc2	klid
(	(	kIx(	(
<g/>
změna	změna	k1gFnSc1	změna
101	[number]	k4	101
<g/>
/	/	kIx~	/
<g/>
2004	[number]	k4	2004
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ZUNA	zuna	k1gFnSc1	zuna
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
studentů	student	k1gMnPc2	student
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
1939	[number]	k4	1939
a	a	k8xC	a
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
-	-	kIx~	-
Slavné	slavný	k2eAgInPc4d1	slavný
dny	den	k1gInPc4	den
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Stream	Stream	k1gInSc1	Stream
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2010-11-17	[number]	k4	2010-11-17
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠUBERT	Šubert	k1gMnSc1	Šubert
<g/>
,	,	kIx,	,
Eduard	Eduard	k1gMnSc1	Eduard
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc1	listopad
-	-	kIx~	-
Den	den	k1gInSc1	den
boje	boj	k1gInSc2	boj
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
a	a	k8xC	a
demokracii	demokracie	k1gFnSc4	demokracie
-	-	kIx~	-
Na	na	k7c4	na
ubrousek	ubrousek	k1gInSc4	ubrousek
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2016-11-15	[number]	k4	2016-11-15
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
-	-	kIx~	-
<g/>
17	[number]	k4	17
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
