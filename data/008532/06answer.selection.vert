<s>
Den	den	k1gInSc1	den
boje	boj	k1gInSc2	boj
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
a	a	k8xC	a
demokracii	demokracie	k1gFnSc4	demokracie
(	(	kIx(	(
<g/>
slovensky	slovensky	k6eAd1	slovensky
<g/>
:	:	kIx,	:
Deň	Deň	k1gMnSc1	Deň
boja	boja	k1gMnSc1	boja
za	za	k7c4	za
slobodu	sloboda	k1gFnSc4	sloboda
a	a	k8xC	a
demokraciu	demokracium	k1gNnSc6	demokracium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
státní	státní	k2eAgInSc4d1	státní
svátek	svátek	k1gInSc4	svátek
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
státní	státní	k2eAgInSc4d1	státní
svátek	svátek	k1gInSc4	svátek
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
slaví	slavit	k5eAaImIp3nS	slavit
každoročně	každoročně	k6eAd1	každoročně
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
