<p>
<s>
Zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
je	být	k5eAaImIp3nS	být
přírodní	přírodní	k2eAgInSc4d1	přírodní
hořlavý	hořlavý	k2eAgInSc4d1	hořlavý
plyn	plyn	k1gInSc4	plyn
využívaný	využívaný	k2eAgInSc4d1	využívaný
jako	jako	k8xS	jako
významné	významný	k2eAgNnSc4d1	významné
plynné	plynný	k2eAgNnSc4d1	plynné
fosilní	fosilní	k2eAgNnSc4d1	fosilní
palivo	palivo	k1gNnSc4	palivo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
hlavní	hlavní	k2eAgFnSc7d1	hlavní
složkou	složka	k1gFnSc7	složka
je	být	k5eAaImIp3nS	být
methan	methan	k1gInSc1	methan
<g/>
.	.	kIx.	.
</s>
<s>
Zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
se	se	k3xPyFc4	se
těží	těžet	k5eAaImIp3nS	těžet
z	z	k7c2	z
porézních	porézní	k2eAgFnPc2d1	porézní
sedimentárních	sedimentární	k2eAgFnPc2d1	sedimentární
hornin	hornina	k1gFnPc2	hornina
uzavřených	uzavřený	k2eAgFnPc2d1	uzavřená
ve	v	k7c6	v
strukturních	strukturní	k2eAgFnPc6d1	strukturní
pastech	past	k1gFnPc6	past
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
ropa	ropa	k1gFnSc1	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
buď	buď	k8xC	buď
samostatně	samostatně	k6eAd1	samostatně
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
ropou	ropa	k1gFnSc7	ropa
nebo	nebo	k8xC	nebo
černým	černý	k2eAgNnSc7d1	černé
uhlím	uhlí	k1gNnSc7	uhlí
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
také	také	k9	také
jako	jako	k9	jako
zdroj	zdroj	k1gInSc4	zdroj
vodíku	vodík	k1gInSc2	vodík
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
dusíkatých	dusíkatý	k2eAgNnPc2d1	dusíkaté
hnojiv	hnojivo	k1gNnPc2	hnojivo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
především	především	k9	především
methan	methan	k1gInSc1	methan
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
ostatními	ostatní	k2eAgNnPc7d1	ostatní
fosilními	fosilní	k2eAgNnPc7d1	fosilní
palivy	palivo	k1gNnPc7	palivo
při	při	k7c6	při
spalování	spalování	k1gNnSc6	spalování
nejmenší	malý	k2eAgInSc4d3	nejmenší
podíl	podíl	k1gInSc4	podíl
CO2	CO2	k1gFnPc2	CO2
na	na	k7c4	na
jednotku	jednotka	k1gFnSc4	jednotka
uvolněné	uvolněný	k2eAgFnSc2d1	uvolněná
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
je	být	k5eAaImIp3nS	být
methan	methan	k1gInSc4	methan
mnohem	mnohem	k6eAd1	mnohem
účinnější	účinný	k2eAgInSc4d2	účinnější
skleníkový	skleníkový	k2eAgInSc4d1	skleníkový
plyn	plyn	k1gInSc4	plyn
než	než	k8xS	než
CO2	CO2	k1gFnPc4	CO2
a	a	k8xC	a
úniky	únik	k1gInPc4	únik
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
logistickém	logistický	k2eAgInSc6d1	logistický
řetezci	řetezec	k1gInSc6	řetezec
od	od	k7c2	od
těžby	těžba	k1gFnSc2	těžba
po	po	k7c4	po
spotřebu	spotřeba	k1gFnSc4	spotřeba
jsou	být	k5eAaImIp3nP	být
značné	značný	k2eAgInPc1d1	značný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
novějších	nový	k2eAgInPc6d2	novější
výzkumech	výzkum	k1gInPc6	výzkum
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
užívání	užívání	k1gNnSc1	užívání
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
místo	místo	k7c2	místo
jiných	jiný	k2eAgNnPc2d1	jiné
fosilních	fosilní	k2eAgNnPc2d1	fosilní
paliv	palivo	k1gNnPc2	palivo
hodnoceno	hodnotit	k5eAaImNgNnS	hodnotit
negativně	negativně	k6eAd1	negativně
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vozidlech	vozidlo	k1gNnPc6	vozidlo
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
ve	v	k7c6	v
stlačené	stlačený	k2eAgFnSc6d1	stlačená
(	(	kIx(	(
<g/>
CNG	CNG	kA	CNG
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
zkapalněné	zkapalněný	k2eAgFnSc6d1	zkapalněná
podobě	podoba	k1gFnSc6	podoba
(	(	kIx(	(
<g/>
LNG	LNG	kA	LNG
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
je	být	k5eAaImIp3nS	být
bez	bez	k7c2	bez
zápachu	zápach	k1gInSc2	zápach
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
odorizuje	odorizovat	k5eAaBmIp3nS	odorizovat
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
přidávají	přidávat	k5eAaImIp3nP	přidávat
se	se	k3xPyFc4	se
do	do	k7c2	do
něj	on	k3xPp3gNnSc2	on
páchnoucí	páchnoucí	k2eAgInPc1d1	páchnoucí
plyny	plyn	k1gInPc1	plyn
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ethylmerkaptan	ethylmerkaptan	k1gInSc1	ethylmerkaptan
<g/>
)	)	kIx)	)
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
možno	možno	k6eAd1	možno
čichem	čich	k1gInSc7	čich
zjistit	zjistit	k5eAaPmF	zjistit
koncentraci	koncentrace	k1gFnSc4	koncentrace
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
větší	veliký	k2eAgFnSc2d2	veliký
než	než	k8xS	než
1	[number]	k4	1
procento	procento	k1gNnSc4	procento
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
je	být	k5eAaImIp3nS	být
využíván	využívat	k5eAaImNgInS	využívat
jako	jako	k9	jako
zdroj	zdroj	k1gInSc1	zdroj
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
také	také	k9	také
jako	jako	k9	jako
surovina	surovina	k1gFnSc1	surovina
pro	pro	k7c4	pro
chemický	chemický	k2eAgInSc4d1	chemický
a	a	k8xC	a
palivový	palivový	k2eAgInSc4d1	palivový
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fyzikální	fyzikální	k2eAgFnPc1d1	fyzikální
charakteristiky	charakteristika	k1gFnPc1	charakteristika
==	==	k?	==
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgFnPc1	tento
charakteristiky	charakteristika	k1gFnPc1	charakteristika
jsou	být	k5eAaImIp3nP	být
průměrné	průměrný	k2eAgFnPc1d1	průměrná
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
složení	složení	k1gNnSc2	složení
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
nalezištích	naleziště	k1gNnPc6	naleziště
liší	lišit	k5eAaImIp3nS	lišit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hustota	hustota	k1gFnSc1	hustota
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
suchý	suchý	k2eAgMnSc1d1	suchý
plynný	plynný	k2eAgMnSc1d1	plynný
<g/>
:	:	kIx,	:
0,7	[number]	k4	0,7
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
<g/>
3	[number]	k4	3
(	(	kIx(	(
<g/>
lehčí	lehký	k2eAgMnSc1d2	lehčí
než	než	k8xS	než
vzduch	vzduch	k1gInSc1	vzduch
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
kapalný	kapalný	k2eAgMnSc1d1	kapalný
<g/>
:	:	kIx,	:
400	[number]	k4	400
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
<g/>
3	[number]	k4	3
</s>
</p>
<p>
<s>
Molární	molární	k2eAgFnSc1d1	molární
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
:	:	kIx,	:
M	M	kA	M
=	=	kIx~	=
0,0164	[number]	k4	0,0164
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
mol	mol	k1gMnSc1	mol
</s>
</p>
<p>
<s>
Zápalná	zápalný	k2eAgFnSc1d1	zápalná
teplota	teplota	k1gFnSc1	teplota
<g/>
:	:	kIx,	:
t	t	k?	t
=	=	kIx~	=
650	[number]	k4	650
°	°	k?	°
<g/>
C	C	kA	C
</s>
</p>
<p>
<s>
Teplota	teplota	k1gFnSc1	teplota
plamene	plamen	k1gInSc2	plamen
1957	[number]	k4	1957
°	°	k?	°
<g/>
C	C	kA	C
</s>
</p>
<p>
<s>
Výhřevnost	výhřevnost	k1gFnSc1	výhřevnost
<g/>
:	:	kIx,	:
16	[number]	k4	16
<g/>
–	–	k?	–
<g/>
34	[number]	k4	34
MJ	mj	kA	mj
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
<g/>
3	[number]	k4	3
(	(	kIx(	(
<g/>
plynný	plynný	k2eAgInSc1d1	plynný
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Oktanové	oktanový	k2eAgNnSc1d1	oktanové
číslo	číslo	k1gNnSc1	číslo
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
ve	v	k7c6	v
spalovacích	spalovací	k2eAgInPc6d1	spalovací
motorech	motor	k1gInPc6	motor
<g/>
:	:	kIx,	:
120	[number]	k4	120
<g/>
–	–	k?	–
<g/>
130	[number]	k4	130
</s>
</p>
<p>
<s>
Dolní	dolní	k2eAgFnSc1d1	dolní
mez	mez	k1gFnSc1	mez
výbušnosti	výbušnost	k1gFnSc2	výbušnost
<g/>
:	:	kIx,	:
4,3	[number]	k4	4,3
<g/>
%	%	kIx~	%
</s>
</p>
<p>
<s>
Horní	horní	k2eAgFnSc1d1	horní
mez	mez	k1gFnSc1	mez
výbušnosti	výbušnost	k1gFnSc2	výbušnost
<g/>
:	:	kIx,	:
15	[number]	k4	15
<g/>
%	%	kIx~	%
</s>
</p>
<p>
<s>
==	==	k?	==
Složení	složení	k1gNnSc1	složení
==	==	k?	==
</s>
</p>
<p>
<s>
Zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
je	být	k5eAaImIp3nS	být
směsí	směs	k1gFnSc7	směs
plynných	plynný	k2eAgInPc2d1	plynný
alkanů	alkan	k1gInPc2	alkan
methanu	methan	k1gInSc2	methan
(	(	kIx(	(
<g/>
CH	Ch	kA	Ch
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ethanu	ethan	k1gInSc2	ethan
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
H	H	kA	H
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
propanu	propan	k1gInSc2	propan
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
3	[number]	k4	3
<g/>
H	H	kA	H
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
a	a	k8xC	a
butanu	butan	k1gInSc2	butan
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
4	[number]	k4	4
<g/>
H	H	kA	H
<g/>
10	[number]	k4	10
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Typické	typický	k2eAgNnSc1d1	typické
složení	složení	k1gNnSc1	složení
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
:	:	kIx,	:
</s>
</p>
<p>
<s>
Složení	složení	k1gNnSc1	složení
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgNnSc2	který
ložiska	ložisko	k1gNnSc2	ložisko
se	se	k3xPyFc4	se
těží	těžet	k5eAaImIp3nS	těžet
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInPc1d1	následující
údaje	údaj	k1gInPc1	údaj
o	o	k7c6	o
složení	složení	k1gNnSc6	složení
byly	být	k5eAaImAgInP	být
převzaty	převzít	k5eAaPmNgInP	převzít
z	z	k7c2	z
a.	a.	k?	a.
Hodnoty	hodnota	k1gFnPc1	hodnota
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
molárních	molární	k2eAgNnPc6d1	molární
procentech	procento	k1gNnPc6	procento
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
==	==	k?	==
</s>
</p>
<p>
<s>
Zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
třemi	tři	k4xCgInPc7	tři
způsoby	způsob	k1gInPc7	způsob
<g/>
:	:	kIx,	:
biogenicky	biogenicky	k6eAd1	biogenicky
bakteriálním	bakteriální	k2eAgInSc7d1	bakteriální
rozkladem	rozklad	k1gInSc7	rozklad
organické	organický	k2eAgFnSc2d1	organická
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
termogenicky	termogenicky	k6eAd1	termogenicky
společně	společně	k6eAd1	společně
s	s	k7c7	s
ropou	ropa	k1gFnSc7	ropa
nebo	nebo	k8xC	nebo
anorganickou	anorganický	k2eAgFnSc7d1	anorganická
cestou	cesta	k1gFnSc7	cesta
během	během	k7c2	během
tuhnutí	tuhnutí	k1gNnSc2	tuhnutí
magmatu	magma	k1gNnSc2	magma
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Anorganicky	anorganicky	k6eAd1	anorganicky
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
uhlovodíky	uhlovodík	k1gInPc1	uhlovodík
byly	být	k5eAaImAgInP	být
popsány	popsat	k5eAaPmNgInP	popsat
z	z	k7c2	z
oceánských	oceánský	k2eAgInPc2d1	oceánský
hřbetů	hřbet	k1gInPc2	hřbet
v	v	k7c6	v
Tichém	tichý	k2eAgInSc6d1	tichý
a	a	k8xC	a
Atlantském	atlantský	k2eAgInSc6d1	atlantský
oceánu	oceán	k1gInSc6	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnPc1	jejich
množství	množství	k1gNnPc1	množství
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
malá	malý	k2eAgNnPc1d1	malé
a	a	k8xC	a
nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
tak	tak	k6eAd1	tak
nepřístupných	přístupný	k2eNgFnPc6d1	nepřístupná
hloubkách	hloubka	k1gFnPc6	hloubka
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
pevniny	pevnina	k1gFnSc2	pevnina
<g/>
,	,	kIx,	,
že	že	k8xS	že
nemají	mít	k5eNaImIp3nP	mít
komerční	komerční	k2eAgInSc4d1	komerční
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Biogeneze	biogeneze	k1gFnSc1	biogeneze
probíhá	probíhat	k5eAaImIp3nS	probíhat
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
mělkých	mělký	k2eAgFnPc6d1	mělká
částech	část	k1gFnPc6	část
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
a	a	k8xC	a
jejími	její	k3xOp3gInPc7	její
produkty	produkt	k1gInPc7	produkt
jsou	být	k5eAaImIp3nP	být
pouze	pouze	k6eAd1	pouze
plynné	plynný	k2eAgInPc1d1	plynný
uhlovodíky	uhlovodík	k1gInPc1	uhlovodík
(	(	kIx(	(
<g/>
metan	metan	k1gInSc1	metan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
přeměně	přeměna	k1gFnSc6	přeměna
organické	organický	k2eAgFnSc2d1	organická
hmoty	hmota	k1gFnSc2	hmota
drobnými	drobný	k2eAgInPc7d1	drobný
mikroorganismy	mikroorganismus	k1gInPc7	mikroorganismus
na	na	k7c4	na
metan	metan	k1gInSc4	metan
<g/>
.	.	kIx.	.
</s>
<s>
Methanogeny	Methanogen	k1gInPc1	Methanogen
<g/>
,	,	kIx,	,
drobné	drobný	k2eAgInPc1d1	drobný
<g/>
,	,	kIx,	,
metan	metan	k1gInSc4	metan
produkující	produkující	k2eAgInPc1d1	produkující
mikroorganismy	mikroorganismus	k1gInPc1	mikroorganismus
<g/>
,	,	kIx,	,
chemicky	chemicky	k6eAd1	chemicky
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
organickou	organický	k2eAgFnSc4d1	organická
hmotu	hmota	k1gFnSc4	hmota
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
metanu	metan	k1gInSc2	metan
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
mikroorganismy	mikroorganismus	k1gInPc1	mikroorganismus
se	se	k3xPyFc4	se
běžně	běžně	k6eAd1	běžně
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
blízko	blízko	k7c2	blízko
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
bez	bez	k7c2	bez
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
mikroorganismy	mikroorganismus	k1gInPc1	mikroorganismus
také	také	k9	také
žijí	žít	k5eAaImIp3nP	žít
ve	v	k7c6	v
střevech	střevo	k1gNnPc6	střevo
většiny	většina	k1gFnSc2	většina
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Tvorba	tvorba	k1gFnSc1	tvorba
metanu	metan	k1gInSc2	metan
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
probíhá	probíhat	k5eAaImIp3nS	probíhat
jen	jen	k9	jen
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
povrchu	povrch	k1gInSc2	povrch
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
tohoto	tento	k3xDgInSc2	tento
metanu	metan	k1gInSc2	metan
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
ztracena	ztratit	k5eAaPmNgFnS	ztratit
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
určitých	určitý	k2eAgFnPc2d1	určitá
okolností	okolnost	k1gFnPc2	okolnost
však	však	k9	však
může	moct	k5eAaImIp3nS	moct
tento	tento	k3xDgInSc4	tento
metan	metan	k1gInSc4	metan
být	být	k5eAaImF	být
zachycen	zachytit	k5eAaPmNgMnS	zachytit
v	v	k7c6	v
podpovrchových	podpovrchový	k2eAgFnPc6d1	podpovrchová
strukturních	strukturní	k2eAgFnPc6d1	strukturní
pastích	past	k1gFnPc6	past
a	a	k8xC	a
vytěžen	vytěžit	k5eAaPmNgInS	vytěžit
jako	jako	k9	jako
zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Příklad	příklad	k1gInSc1	příklad
biogenního	biogenní	k2eAgInSc2d1	biogenní
metanu	metan	k1gInSc2	metan
je	být	k5eAaImIp3nS	být
skládkový	skládkový	k2eAgInSc4d1	skládkový
plyn	plyn	k1gInSc4	plyn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
pokračující	pokračující	k2eAgFnSc6d1	pokračující
subsidenci	subsidence	k1gFnSc6	subsidence
sedimentární	sedimentární	k2eAgFnSc2d1	sedimentární
pánve	pánev	k1gFnSc2	pánev
biogeneze	biogeneze	k1gFnSc2	biogeneze
ustává	ustávat	k5eAaImIp3nS	ustávat
a	a	k8xC	a
začnou	začít	k5eAaPmIp3nP	začít
probíhat	probíhat	k5eAaImF	probíhat
procesy	proces	k1gInPc1	proces
termogenické	termogenický	k2eAgInPc1d1	termogenický
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
termogenické	termogenický	k2eAgFnSc6d1	termogenický
přeměně	přeměna	k1gFnSc6	přeměna
organické	organický	k2eAgFnSc2d1	organická
hmoty	hmota	k1gFnSc2	hmota
vznikají	vznikat	k5eAaImIp3nP	vznikat
plynné	plynný	k2eAgInPc1d1	plynný
uhlovodíky	uhlovodík	k1gInPc1	uhlovodík
(	(	kIx(	(
<g/>
zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tekuté	tekutý	k2eAgInPc1d1	tekutý
uhlovodíky	uhlovodík	k1gInPc1	uhlovodík
(	(	kIx(	(
<g/>
ropa	ropa	k1gFnSc1	ropa
<g/>
)	)	kIx)	)
i	i	k9	i
pevné	pevný	k2eAgInPc4d1	pevný
uhlovodíky	uhlovodík	k1gInPc4	uhlovodík
(	(	kIx(	(
<g/>
asfalt	asfalt	k1gInSc4	asfalt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Organický	organický	k2eAgInSc1d1	organický
materiál	materiál	k1gInSc1	materiál
se	se	k3xPyFc4	se
vlivem	vliv	k1gInSc7	vliv
tepla	teplo	k1gNnSc2	teplo
a	a	k8xC	a
tlaku	tlak	k1gInSc2	tlak
přemění	přeměnit	k5eAaPmIp3nS	přeměnit
nejprve	nejprve	k6eAd1	nejprve
na	na	k7c4	na
kerogen	kerogen	k1gInSc4	kerogen
a	a	k8xC	a
pak	pak	k6eAd1	pak
na	na	k7c4	na
ropu	ropa	k1gFnSc4	ropa
a	a	k8xC	a
zemní	zemní	k2eAgInSc4d1	zemní
plyn	plyn	k1gInSc4	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Ropa	ropa	k1gFnSc1	ropa
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
tvořit	tvořit	k5eAaImF	tvořit
při	při	k7c6	při
cca	cca	kA	cca
60	[number]	k4	60
stupních	stupeň	k1gInPc6	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
termogenickým	termogenický	k2eAgInSc7d1	termogenický
rozpadem	rozpad	k1gInSc7	rozpad
(	(	kIx(	(
<g/>
krakováním	krakování	k1gNnPc3	krakování
<g/>
)	)	kIx)	)
kerogenu	kerogen	k1gInSc3	kerogen
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
proces	proces	k1gInSc1	proces
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
až	až	k9	až
do	do	k7c2	do
cca	cca	kA	cca
120	[number]	k4	120
stupňů	stupeň	k1gInPc2	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
cca	cca	kA	cca
100	[number]	k4	100
stupních	stupeň	k1gInPc6	stupeň
začíná	začínat	k5eAaImIp3nS	začínat
tvorba	tvorba	k1gFnSc1	tvorba
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
zhruba	zhruba	k6eAd1	zhruba
do	do	k7c2	do
200	[number]	k4	200
stupňů	stupeň	k1gInPc2	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
<g/>
.	.	kIx.	.
</s>
<s>
Teplotnímu	teplotní	k2eAgInSc3d1	teplotní
intervalu	interval	k1gInSc3	interval
tvorby	tvorba	k1gFnSc2	tvorba
ropy	ropa	k1gFnSc2	ropa
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
ropné	ropný	k2eAgNnSc1d1	ropné
okno	okno	k1gNnSc1	okno
(	(	kIx(	(
<g/>
60	[number]	k4	60
<g/>
-	-	kIx~	-
<g/>
120	[number]	k4	120
stupňů	stupeň	k1gInPc2	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teplotnímu	teplotní	k2eAgInSc3d1	teplotní
intervalu	interval	k1gInSc3	interval
tvorby	tvorba	k1gFnSc2	tvorba
plynu	plyn	k1gInSc2	plyn
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
plynové	plynový	k2eAgNnSc4d1	plynové
okno	okno	k1gNnSc4	okno
(	(	kIx(	(
<g/>
100	[number]	k4	100
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
+	+	kIx~	+
stupňů	stupeň	k1gInPc2	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tepelného	tepelný	k2eAgInSc2d1	tepelný
toku	tok	k1gInSc2	tok
v	v	k7c4	v
daně	daň	k1gFnPc4	daň
sedimentární	sedimentární	k2eAgFnSc3d1	sedimentární
pánvi	pánev	k1gFnSc3	pánev
se	se	k3xPyFc4	se
hloubka	hloubka	k1gFnSc1	hloubka
ropného	ropný	k2eAgNnSc2d1	ropné
okna	okno	k1gNnSc2	okno
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c4	mezi
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
km	km	kA	km
a	a	k8xC	a
hloubka	hloubka	k1gFnSc1	hloubka
plynového	plynový	k2eAgNnSc2d1	plynové
okna	okno	k1gNnSc2	okno
mezi	mezi	k7c7	mezi
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
teplotní	teplotní	k2eAgFnPc4d1	teplotní
podmínky	podmínka	k1gFnPc4	podmínka
v	v	k7c6	v
pánvi	pánev	k1gFnSc6	pánev
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgFnPc1d1	různá
zdrojové	zdrojový	k2eAgFnPc1d1	zdrojová
horniny	hornina	k1gFnPc1	hornina
náchylné	náchylný	k2eAgFnPc1d1	náchylná
k	k	k7c3	k
vytváření	vytváření	k1gNnSc3	vytváření
různých	různý	k2eAgInPc2d1	různý
typu	typ	k1gInSc2	typ
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
zdrojové	zdrojový	k2eAgFnPc1d1	zdrojová
horniny	hornina	k1gFnPc1	hornina
(	(	kIx(	(
<g/>
mořské	mořský	k2eAgFnPc1d1	mořská
břidlice	břidlice	k1gFnPc1	břidlice
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
obsahem	obsah	k1gInSc7	obsah
organického	organický	k2eAgInSc2d1	organický
materiálu	materiál	k1gInSc2	materiál
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
schopné	schopný	k2eAgFnPc1d1	schopná
produkce	produkce	k1gFnPc1	produkce
kapalných	kapalný	k2eAgInPc2d1	kapalný
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
(	(	kIx(	(
<g/>
ropy	ropa	k1gFnSc2	ropa
<g/>
)	)	kIx)	)
i	i	k8xC	i
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Produkce	produkce	k1gFnSc1	produkce
probíhá	probíhat	k5eAaImIp3nS	probíhat
postupně	postupně	k6eAd1	postupně
podle	podle	k7c2	podle
zahřívání	zahřívání	k1gNnSc2	zahřívání
zdrojové	zdrojový	k2eAgFnSc2d1	zdrojová
horniny	hornina	k1gFnSc2	hornina
při	při	k7c6	při
subsidenci	subsidence	k1gFnSc6	subsidence
a	a	k8xC	a
procházením	procházení	k1gNnSc7	procházení
nejprve	nejprve	k6eAd1	nejprve
ropným	ropný	k2eAgNnSc7d1	ropné
oknem	okno	k1gNnSc7	okno
a	a	k8xC	a
pak	pak	k6eAd1	pak
plynovým	plynový	k2eAgNnSc7d1	plynové
oknem	okno	k1gNnSc7	okno
<g/>
.	.	kIx.	.
</s>
<s>
Uhlí	uhlí	k1gNnSc1	uhlí
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
zdrojovou	zdrojový	k2eAgFnSc4d1	zdrojová
horninu	hornina	k1gFnSc4	hornina
náchylnou	náchylný	k2eAgFnSc4d1	náchylná
ke	k	k7c3	k
tvoření	tvoření	k1gNnSc3	tvoření
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
určitých	určitý	k2eAgInPc6d1	určitý
případech	případ	k1gInPc6	případ
(	(	kIx(	(
<g/>
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
typu	typ	k1gInSc6	typ
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
tvořit	tvořit	k5eAaImF	tvořit
i	i	k9	i
ropu	ropa	k1gFnSc4	ropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
co	co	k9	co
zdrojová	zdrojový	k2eAgFnSc1d1	zdrojová
hornina	hornina	k1gFnSc1	hornina
dosáhne	dosáhnout	k5eAaPmIp3nS	dosáhnout
zralosti	zralost	k1gFnSc3	zralost
<g/>
,	,	kIx,	,
nastane	nastat	k5eAaPmIp3nS	nastat
migrace	migrace	k1gFnSc1	migrace
<g/>
.	.	kIx.	.
</s>
<s>
Plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
společně	společně	k6eAd1	společně
s	s	k7c7	s
ropou	ropa	k1gFnSc7	ropa
<g/>
,	,	kIx,	,
migrují	migrovat	k5eAaImIp3nP	migrovat
buď	buď	k8xC	buď
podél	podél	k7c2	podél
geologických	geologický	k2eAgInPc2d1	geologický
zlomů	zlom	k1gInPc2	zlom
(	(	kIx(	(
<g/>
vertikální	vertikální	k2eAgFnSc1d1	vertikální
migrace	migrace	k1gFnSc1	migrace
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
podél	podél	k7c2	podél
porézních	porézní	k2eAgFnPc2d1	porézní
sedimentárních	sedimentární	k2eAgFnPc2d1	sedimentární
vrstev	vrstva	k1gFnPc2	vrstva
(	(	kIx(	(
<g/>
laterální	laterální	k2eAgFnSc1d1	laterální
migrace	migrace	k1gFnSc1	migrace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Typické	typický	k2eAgInPc1d1	typický
příklady	příklad	k1gInPc1	příklad
nosných	nosný	k2eAgFnPc2d1	nosná
vrstev	vrstva	k1gFnPc2	vrstva
při	při	k7c6	při
laterální	laterální	k2eAgFnSc6d1	laterální
migraci	migrace	k1gFnSc6	migrace
jsou	být	k5eAaImIp3nP	být
porézní	porézní	k2eAgInPc1d1	porézní
pískovce	pískovec	k1gInPc1	pískovec
<g/>
,	,	kIx,	,
některé	některý	k3yIgInPc4	některý
vápence	vápenec	k1gInPc4	vápenec
nebo	nebo	k8xC	nebo
i	i	k9	i
zvětralé	zvětralý	k2eAgFnSc2d1	zvětralá
vyvřelé	vyvřelý	k2eAgFnSc2d1	vyvřelá
horniny	hornina	k1gFnSc2	hornina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
fáze	fáze	k1gFnSc1	fáze
je	být	k5eAaImIp3nS	být
zachycení	zachycení	k1gNnSc4	zachycení
migrující	migrující	k2eAgFnSc2d1	migrující
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
plynu	plyn	k1gInSc2	plyn
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
ropné	ropný	k2eAgFnPc4d1	ropná
pasti	past	k1gFnPc4	past
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vzniká	vznikat	k5eAaImIp3nS	vznikat
jejich	jejich	k3xOp3gNnPc4	jejich
současná	současný	k2eAgNnPc4d1	současné
naleziště	naleziště	k1gNnPc4	naleziště
<g/>
.	.	kIx.	.
</s>
<s>
Ropná	ropný	k2eAgFnSc1d1	ropná
past	past	k1gFnSc1	past
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
porézních	porézní	k2eAgFnPc2d1	porézní
hornin	hornina	k1gFnPc2	hornina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
nadloží	nadloží	k1gNnSc6	nadloží
a	a	k8xC	a
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
utěsněné	utěsněný	k2eAgFnSc2d1	utěsněná
horninami	hornina	k1gFnPc7	hornina
nepropustnými	propustný	k2eNgFnPc7d1	nepropustná
<g/>
.	.	kIx.	.
</s>
<s>
Ropné	ropný	k2eAgFnPc1d1	ropná
pasti	past	k1gFnPc1	past
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
jako	jako	k9	jako
geologické	geologický	k2eAgFnPc1d1	geologická
struktury	struktura	k1gFnPc1	struktura
(	(	kIx(	(
<g/>
např.	např.	kA	např.
antiklinály	antiklinála	k1gFnSc2	antiklinála
<g/>
,	,	kIx,	,
zlomové	zlomový	k2eAgFnSc2d1	zlomová
struktury	struktura	k1gFnSc2	struktura
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
stratigraficky	stratigraficky	k6eAd1	stratigraficky
(	(	kIx(	(
<g/>
např.	např.	kA	např.
vyklinováním	vyklinování	k1gNnSc7	vyklinování
pískovce	pískovec	k1gInSc2	pískovec
a	a	k8xC	a
faciálním	faciální	k2eAgInSc7d1	faciální
přechodem	přechod	k1gInSc7	přechod
do	do	k7c2	do
nepropustných	propustný	k2eNgFnPc2d1	nepropustná
břidlic	břidlice	k1gFnPc2	břidlice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Těsnící	těsnící	k2eAgFnPc1d1	těsnící
horniny	hornina	k1gFnPc1	hornina
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
břidlice	břidlice	k1gFnSc2	břidlice
s	s	k7c7	s
vysokým	vysoký	k2eAgInSc7d1	vysoký
obsahem	obsah	k1gInSc7	obsah
jílu	jíl	k1gInSc2	jíl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
vyvřelé	vyvřelý	k2eAgFnSc2d1	vyvřelá
horniny	hornina	k1gFnSc2	hornina
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nejsou	být	k5eNaImIp3nP	být
zvětralé	zvětralý	k2eAgFnPc1d1	zvětralá
<g/>
.	.	kIx.	.
</s>
<s>
Srdcem	srdce	k1gNnSc7	srdce
ropné	ropný	k2eAgFnSc2d1	ropná
pasti	past	k1gFnSc2	past
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
vlastní	vlastní	k2eAgFnSc1d1	vlastní
porézní	porézní	k2eAgFnSc1d1	porézní
hornina	hornina	k1gFnSc1	hornina
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
kolektorová	kolektorový	k2eAgFnSc1d1	kolektorová
hornina	hornina	k1gFnSc1	hornina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
migrující	migrující	k2eAgFnSc1d1	migrující
ropa	ropa	k1gFnSc1	ropa
a	a	k8xC	a
plyn	plyn	k1gInSc1	plyn
nahromadí	nahromadit	k5eAaPmIp3nS	nahromadit
<g/>
.	.	kIx.	.
</s>
<s>
Typické	typický	k2eAgFnPc1d1	typická
kolektorové	kolektorový	k2eAgFnPc1d1	kolektorová
horniny	hornina	k1gFnPc1	hornina
jsou	být	k5eAaImIp3nP	být
porézní	porézní	k2eAgInPc4d1	porézní
pískovce	pískovec	k1gInPc4	pískovec
nebo	nebo	k8xC	nebo
vápence	vápenec	k1gInPc4	vápenec
korálových	korálový	k2eAgInPc2d1	korálový
útesu	útes	k1gInSc6	útes
<g/>
.	.	kIx.	.
</s>
<s>
Klíčovým	klíčový	k2eAgInSc7d1	klíčový
parametrem	parametr	k1gInSc7	parametr
kolektorových	kolektorový	k2eAgFnPc2d1	kolektorová
hornin	hornina	k1gFnPc2	hornina
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc1	jejich
porozita	porozita	k1gFnSc1	porozita
a	a	k8xC	a
propustnost	propustnost	k1gFnSc1	propustnost
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
porozita	porozita	k1gFnSc1	porozita
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
od	od	k7c2	od
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
35	[number]	k4	35
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
a	a	k8xC	a
typická	typický	k2eAgFnSc1d1	typická
propustnost	propustnost	k1gFnSc1	propustnost
od	od	k7c2	od
100	[number]	k4	100
Millidarcy	Millidarca	k1gFnSc2	Millidarca
(	(	kIx(	(
<g/>
mD	mD	k?	mD
<g/>
)	)	kIx)	)
do	do	k7c2	do
několika	několik	k4yIc2	několik
Darcy	Darcum	k1gNnPc7	Darcum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
kolektoru	kolektor	k1gInSc2	kolektor
se	se	k3xPyFc4	se
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
tekutiny	tekutina	k1gFnPc1	tekutina
rozdělí	rozdělit	k5eAaPmIp3nP	rozdělit
podle	podle	k7c2	podle
relativních	relativní	k2eAgFnPc2d1	relativní
hustot	hustota	k1gFnPc2	hustota
<g/>
.	.	kIx.	.
</s>
<s>
Plyn	plyn	k1gInSc1	plyn
se	se	k3xPyFc4	se
nahromadí	nahromadit	k5eAaPmIp3nS	nahromadit
v	v	k7c6	v
nejmělčích	mělký	k2eAgFnPc6d3	nejmělčí
částech	část	k1gFnPc6	část
ropné	ropný	k2eAgFnSc2d1	ropná
pasti	past	k1gFnSc2	past
<g/>
,	,	kIx,	,
ropa	ropa	k1gFnSc1	ropa
níže	níže	k1gFnSc1	níže
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
nejníže	nízce	k6eAd3	nízce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zdrojové	zdrojový	k2eAgFnSc2d1	zdrojová
horniny	hornina	k1gFnSc2	hornina
<g/>
,	,	kIx,	,
maturace	maturace	k1gFnSc2	maturace
<g/>
,	,	kIx,	,
migrace	migrace	k1gFnSc2	migrace
<g/>
,	,	kIx,	,
ropné	ropný	k2eAgFnSc2d1	ropná
pasti	past	k1gFnSc2	past
<g/>
,	,	kIx,	,
kolektorové	kolektorový	k2eAgFnSc2d1	kolektorová
a	a	k8xC	a
těsnicí	těsnicí	k2eAgFnSc2d1	těsnicí
horniny	hornina	k1gFnSc2	hornina
jsou	být	k5eAaImIp3nP	být
souhrnně	souhrnně	k6eAd1	souhrnně
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
ropným	ropný	k2eAgInSc7d1	ropný
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
mohlo	moct	k5eAaImAgNnS	moct
naleziště	naleziště	k1gNnSc4	naleziště
vzniknout	vzniknout	k5eAaPmF	vzniknout
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
v	v	k7c6	v
dané	daný	k2eAgFnSc6d1	daná
sedimentární	sedimentární	k2eAgFnSc6d1	sedimentární
pánvi	pánev	k1gFnSc6	pánev
existovat	existovat	k5eAaImF	existovat
všechny	všechen	k3xTgInPc4	všechen
jeho	jeho	k3xOp3gInPc4	jeho
elementy	element	k1gInPc4	element
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
musí	muset	k5eAaImIp3nS	muset
jejich	jejich	k3xOp3gFnSc1	jejich
tvorba	tvorba	k1gFnSc1	tvorba
proběhnout	proběhnout	k5eAaPmF	proběhnout
ve	v	k7c6	v
správné	správný	k2eAgFnSc6d1	správná
sekvenci	sekvence	k1gFnSc6	sekvence
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
tvorba	tvorba	k1gFnSc1	tvorba
ropných	ropný	k2eAgFnPc2d1	ropná
pasti	past	k1gFnSc6	past
musí	muset	k5eAaImIp3nP	muset
proběhnou	proběhnout	k5eAaPmIp3nP	proběhnout
před	před	k7c4	před
migraci	migrace	k1gFnSc4	migrace
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
ropa	ropa	k1gFnSc1	ropa
a	a	k8xC	a
plyn	plyn	k1gInSc1	plyn
budou	být	k5eAaImBp3nP	být
migrovat	migrovat	k5eAaImF	migrovat
až	až	k9	až
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
a	a	k8xC	a
uniknou	uniknout	k5eAaPmIp3nP	uniknout
do	do	k7c2	do
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
elementů	element	k1gInPc2	element
ropného	ropný	k2eAgInSc2d1	ropný
systému	systém	k1gInSc2	systém
je	být	k5eAaImIp3nS	být
základem	základ	k1gInSc7	základ
moderního	moderní	k2eAgInSc2d1	moderní
průzkumu	průzkum	k1gInSc2	průzkum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průzkum	průzkum	k1gInSc1	průzkum
==	==	k?	==
</s>
</p>
<p>
<s>
Moderní	moderní	k2eAgInSc1d1	moderní
průzkum	průzkum	k1gInSc1	průzkum
je	být	k5eAaImIp3nS	být
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
stejných	stejný	k2eAgInPc6d1	stejný
principech	princip	k1gInPc6	princip
jako	jako	k8xC	jako
průzkum	průzkum	k1gInSc1	průzkum
na	na	k7c4	na
ropu	ropa	k1gFnSc4	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
těsná	těsný	k2eAgFnSc1d1	těsná
integrace	integrace	k1gFnSc1	integrace
mnoha	mnoho	k4c2	mnoho
technických	technický	k2eAgFnPc2d1	technická
disciplín	disciplína	k1gFnPc2	disciplína
<g/>
:	:	kIx,	:
geologie	geologie	k1gFnSc2	geologie
<g/>
,	,	kIx,	,
geofyziky	geofyzika	k1gFnSc2	geofyzika
<g/>
,	,	kIx,	,
paleontologie	paleontologie	k1gFnSc2	paleontologie
<g/>
,	,	kIx,	,
geochemie	geochemie	k1gFnSc2	geochemie
<g/>
,	,	kIx,	,
petrofyziky	petrofyzika	k1gFnSc2	petrofyzika
<g/>
,	,	kIx,	,
ropného	ropný	k2eAgNnSc2d1	ropné
inženýrství	inženýrství	k1gNnSc2	inženýrství
a	a	k8xC	a
ekonomie	ekonomie	k1gFnSc2	ekonomie
<g/>
.	.	kIx.	.
</s>
<s>
Práce	práce	k1gFnSc1	práce
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
integrovaných	integrovaný	k2eAgInPc2d1	integrovaný
týmu	tým	k1gInSc3	tým
<g/>
,	,	kIx,	,
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
nichž	jenž	k3xRgFnPc2	jenž
uvedené	uvedený	k2eAgFnPc1d1	uvedená
disciplíny	disciplína	k1gFnPc1	disciplína
těsně	těsně	k6eAd1	těsně
spolupracují	spolupracovat	k5eAaImIp3nP	spolupracovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Základem	základ	k1gInSc7	základ
je	být	k5eAaImIp3nS	být
vždy	vždy	k6eAd1	vždy
dobrá	dobrý	k2eAgFnSc1d1	dobrá
znalost	znalost	k1gFnSc1	znalost
podpovrchové	podpovrchový	k2eAgFnSc2d1	podpovrchová
strukturní	strukturní	k2eAgFnSc2d1	strukturní
stavby	stavba	k1gFnSc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
vybuduje	vybudovat	k5eAaPmIp3nS	vybudovat
na	na	k7c6	na
základě	základ	k1gInSc6	základ
analýzy	analýza	k1gFnSc2	analýza
seismických	seismický	k2eAgInPc2d1	seismický
profilů	profil	k1gInPc2	profil
ve	v	k7c6	v
2D	[number]	k4	2D
nebo	nebo	k8xC	nebo
3D	[number]	k4	3D
modelu	model	k1gInSc2	model
a	a	k8xC	a
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
i	i	k9	i
ze	z	k7c2	z
satelitních	satelitní	k2eAgInPc2d1	satelitní
snímků	snímek	k1gInPc2	snímek
nebo	nebo	k8xC	nebo
z	z	k7c2	z
terénní	terénní	k2eAgFnSc2d1	terénní
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
Geochemická	geochemický	k2eAgFnSc1d1	geochemická
<g/>
,	,	kIx,	,
geologická	geologický	k2eAgFnSc1d1	geologická
a	a	k8xC	a
paleontologická	paleontologický	k2eAgNnPc4d1	paleontologické
data	datum	k1gNnPc4	datum
ze	z	k7c2	z
starších	starý	k2eAgInPc2d2	starší
vrtů	vrt	k1gInPc2	vrt
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
integrovat	integrovat	k5eAaBmF	integrovat
do	do	k7c2	do
vznikajícího	vznikající	k2eAgInSc2d1	vznikající
modelu	model	k1gInSc2	model
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
co	co	k9	co
nejpřesnější	přesný	k2eAgInSc1d3	nejpřesnější
obrázek	obrázek	k1gInSc1	obrázek
podpovrchových	podpovrchový	k2eAgFnPc2d1	podpovrchová
struktur	struktura	k1gFnPc2	struktura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Geochemik	geochemik	k1gMnSc1	geochemik
provede	provést	k5eAaPmIp3nS	provést
analýzu	analýza	k1gFnSc4	analýza
zdrojových	zdrojový	k2eAgFnPc2d1	zdrojová
hornin	hornina	k1gFnPc2	hornina
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
existují	existovat	k5eAaImIp3nP	existovat
vzorky	vzorek	k1gInPc4	vzorek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
analýzu	analýza	k1gFnSc4	analýza
vzorků	vzorek	k1gInPc2	vzorek
ropy	ropa	k1gFnSc2	ropa
buď	buď	k8xC	buď
z	z	k7c2	z
existujících	existující	k2eAgInPc2d1	existující
vrtů	vrt	k1gInPc2	vrt
nebo	nebo	k8xC	nebo
z	z	k7c2	z
míst	místo	k1gNnPc2	místo
kde	kde	k6eAd1	kde
ropa	ropa	k1gFnSc1	ropa
prosakuje	prosakovat	k5eAaImIp3nS	prosakovat
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
potvrdí	potvrdit	k5eAaPmIp3nS	potvrdit
věk	věk	k1gInSc1	věk
a	a	k8xC	a
typ	typ	k1gInSc1	typ
zdrojové	zdrojový	k2eAgFnSc2d1	zdrojová
horniny	hornina	k1gFnSc2	hornina
<g/>
,	,	kIx,	,
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
stupeň	stupeň	k1gInSc1	stupeň
termální	termální	k2eAgFnSc2d1	termální
maturace	maturace	k1gFnSc2	maturace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Paleontolog	paleontolog	k1gMnSc1	paleontolog
provede	provést	k5eAaPmIp3nS	provést
analýzu	analýza	k1gFnSc4	analýza
podpovrchových	podpovrchový	k2eAgInPc2d1	podpovrchový
vzorků	vzorek	k1gInPc2	vzorek
z	z	k7c2	z
vrtů	vrt	k1gInPc2	vrt
a	a	k8xC	a
povrchových	povrchový	k2eAgInPc2d1	povrchový
vzorků	vzorek	k1gInPc2	vzorek
z	z	k7c2	z
výchozu	výchoz	k1gInSc2	výchoz
<g/>
.	.	kIx.	.
</s>
<s>
Určí	určit	k5eAaPmIp3nS	určit
věk	věk	k1gInSc1	věk
hornin	hornina	k1gFnPc2	hornina
ve	v	k7c6	v
studované	studovaný	k2eAgFnSc6d1	studovaná
oblasti	oblast	k1gFnSc6	oblast
a	a	k8xC	a
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
i	i	k9	i
jejich	jejich	k3xOp3gInSc4	jejich
sedimentární	sedimentární	k2eAgInSc4d1	sedimentární
původ	původ	k1gInSc4	původ
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
studia	studio	k1gNnSc2	studio
fosilních	fosilní	k2eAgNnPc2d1	fosilní
společenstev	společenstvo	k1gNnPc2	společenstvo
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
určit	určit	k5eAaPmF	určit
v	v	k7c6	v
jakém	jaký	k3yQgNnSc6	jaký
prostředí	prostředí	k1gNnSc6	prostředí
byla	být	k5eAaImAgFnS	být
ta	ten	k3xDgFnSc1	ten
která	který	k3yIgFnSc1	který
hornina	hornina	k1gFnSc1	hornina
uložena	uložit	k5eAaPmNgFnS	uložit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
v	v	k7c6	v
mělkém	mělký	k2eAgNnSc6d1	mělké
moři	moře	k1gNnSc6	moře
nebo	nebo	k8xC	nebo
v	v	k7c6	v
hluboké	hluboký	k2eAgFnSc6d1	hluboká
vodě	voda	k1gFnSc6	voda
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
Geofyzik	geofyzik	k1gMnSc1	geofyzik
a	a	k8xC	a
geolog	geolog	k1gMnSc1	geolog
integrují	integrovat	k5eAaBmIp3nP	integrovat
výsledky	výsledek	k1gInPc7	výsledek
paleontologické	paleontologický	k2eAgFnSc2d1	paleontologická
analýzy	analýza	k1gFnSc2	analýza
dohromady	dohromady	k6eAd1	dohromady
se	s	k7c7	s
seismickými	seismický	k2eAgInPc7d1	seismický
profily	profil	k1gInPc7	profil
<g/>
,	,	kIx,	,
profily	profil	k1gInPc1	profil
interpretují	interpretovat	k5eAaBmIp3nP	interpretovat
a	a	k8xC	a
společně	společně	k6eAd1	společně
zmapují	zmapovat	k5eAaPmIp3nP	zmapovat
oblast	oblast	k1gFnSc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Připraví	připravit	k5eAaPmIp3nS	připravit
strukturní	strukturní	k2eAgFnPc4d1	strukturní
podpovrchové	podpovrchový	k2eAgFnPc4d1	podpovrchová
mapy	mapa	k1gFnPc4	mapa
klíčových	klíčový	k2eAgInPc2d1	klíčový
horizontů	horizont	k1gInPc2	horizont
a	a	k8xC	a
identifikují	identifikovat	k5eAaBmIp3nP	identifikovat
možné	možný	k2eAgInPc1d1	možný
prospekty	prospekt	k1gInPc1	prospekt
k	k	k7c3	k
vrtání	vrtání	k1gNnSc3	vrtání
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
úkolem	úkol	k1gInSc7	úkol
geofyzika	geofyzika	k1gFnSc1	geofyzika
je	být	k5eAaImIp3nS	být
získat	získat	k5eAaPmF	získat
ze	z	k7c2	z
seismických	seismický	k2eAgInPc2d1	seismický
profilů	profil	k1gInPc2	profil
kvantitativní	kvantitativní	k2eAgFnSc2d1	kvantitativní
informace	informace	k1gFnSc2	informace
o	o	k7c6	o
porozitě	porozita	k1gFnSc6	porozita
a	a	k8xC	a
kapalinovém	kapalinový	k2eAgInSc6d1	kapalinový
obsahu	obsah	k1gInSc6	obsah
studovaných	studovaný	k2eAgInPc2d1	studovaný
horizontů	horizont	k1gInPc2	horizont
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
potom	potom	k6eAd1	potom
společně	společně	k6eAd1	společně
s	s	k7c7	s
ropným	ropný	k2eAgMnSc7d1	ropný
inženýrem	inženýr	k1gMnSc7	inženýr
a	a	k8xC	a
ekonomem	ekonom	k1gMnSc7	ekonom
odhadne	odhadnout	k5eAaPmIp3nS	odhadnout
možné	možný	k2eAgFnPc4d1	možná
rezervy	rezerva	k1gFnPc4	rezerva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jakýkoliv	jakýkoliv	k3yIgInSc1	jakýkoliv
průzkum	průzkum	k1gInSc1	průzkum
neodvratně	odvratně	k6eNd1	odvratně
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
element	element	k1gInSc4	element
rizika	riziko	k1gNnSc2	riziko
<g/>
.	.	kIx.	.
</s>
<s>
Typický	typický	k2eAgInSc1d1	typický
průzkumný	průzkumný	k2eAgInSc1d1	průzkumný
vrt	vrt	k1gInSc1	vrt
má	mít	k5eAaImIp3nS	mít
naději	naděje	k1gFnSc4	naděje
na	na	k7c4	na
úspěch	úspěch	k1gInSc4	úspěch
mezi	mezi	k7c7	mezi
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
40	[number]	k4	40
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Tým	tým	k1gInSc1	tým
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
toto	tento	k3xDgNnSc1	tento
riziko	riziko	k1gNnSc1	riziko
odhadnout	odhadnout	k5eAaPmF	odhadnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Průzkumné	průzkumný	k2eAgInPc1d1	průzkumný
vrty	vrt	k1gInPc1	vrt
jsou	být	k5eAaImIp3nP	být
prvním	první	k4xOgNnSc7	první
stadiem	stadion	k1gNnSc7	stadion
hledání	hledání	k1gNnSc2	hledání
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
je	být	k5eAaImIp3nS	být
nové	nový	k2eAgNnSc1d1	nové
ložisko	ložisko	k1gNnSc1	ložisko
objeveno	objeven	k2eAgNnSc1d1	objeveno
<g/>
,	,	kIx,	,
druhým	druhý	k4xOgNnSc7	druhý
stadiem	stadion	k1gNnSc7	stadion
jsou	být	k5eAaImIp3nP	být
podpůrné	podpůrný	k2eAgInPc1d1	podpůrný
vrty	vrt	k1gInPc1	vrt
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
přesně	přesně	k6eAd1	přesně
vymezit	vymezit	k5eAaPmF	vymezit
ložisko	ložisko	k1gNnSc4	ložisko
a	a	k8xC	a
získat	získat	k5eAaPmF	získat
co	co	k9	co
nejvíce	nejvíce	k6eAd1	nejvíce
parametrů	parametr	k1gInPc2	parametr
o	o	k7c6	o
kolektorových	kolektorový	k2eAgFnPc6d1	kolektorová
horninách	hornina	k1gFnPc6	hornina
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
informace	informace	k1gFnPc4	informace
tým	tým	k1gInSc1	tým
použije	použít	k5eAaPmIp3nS	použít
v	v	k7c6	v
modelování	modelování	k1gNnSc6	modelování
kolektorů	kolektor	k1gInPc2	kolektor
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
se	se	k3xPyFc4	se
připraví	připravit	k5eAaPmIp3nP	připravit
3D	[number]	k4	3D
statický	statický	k2eAgInSc4d1	statický
model	model	k1gInSc4	model
kolektorů	kolektor	k1gMnPc2	kolektor
že	že	k8xS	že
všech	všecek	k3xTgNnPc2	všecek
dostupných	dostupný	k2eAgNnPc2d1	dostupné
geologických	geologický	k2eAgNnPc2d1	geologické
a	a	k8xC	a
geofyzikálních	geofyzikální	k2eAgNnPc2d1	geofyzikální
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
potom	potom	k6eAd1	potom
podrobí	podrobit	k5eAaPmIp3nS	podrobit
dynamické	dynamický	k2eAgFnSc3d1	dynamická
simulaci	simulace	k1gFnSc3	simulace
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
předpovídá	předpovídat	k5eAaImIp3nS	předpovídat
tok	tok	k1gInSc1	tok
tekutin	tekutina	k1gFnPc2	tekutina
(	(	kIx(	(
<g/>
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
plynu	plyn	k1gInSc2	plyn
a	a	k8xC	a
vody	voda	k1gFnSc2	voda
<g/>
)	)	kIx)	)
skrz	skrz	k7c4	skrz
kolektor	kolektor	k1gInSc4	kolektor
během	během	k7c2	během
produkce	produkce	k1gFnSc2	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
této	tento	k3xDgFnSc2	tento
dynamické	dynamický	k2eAgFnSc2d1	dynamická
simulace	simulace	k1gFnSc2	simulace
tým	tým	k1gInSc1	tým
potom	potom	k6eAd1	potom
navrhne	navrhnout	k5eAaPmIp3nS	navrhnout
optimální	optimální	k2eAgNnSc4d1	optimální
umístění	umístění	k1gNnSc4	umístění
těžebních	těžební	k2eAgInPc2d1	těžební
vrtů	vrt	k1gInPc2	vrt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
stádiu	stádium	k1gNnSc6	stádium
tým	tým	k1gInSc4	tým
těžebních	těžební	k2eAgMnPc2d1	těžební
inženýrů	inženýr	k1gMnPc2	inženýr
navrhne	navrhnout	k5eAaPmIp3nS	navrhnout
povrchová	povrchový	k2eAgNnPc4d1	povrchové
těžební	těžební	k2eAgNnPc4d1	těžební
zařízení	zařízení	k1gNnPc4	zařízení
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
této	tento	k3xDgFnSc2	tento
práce	práce	k1gFnSc2	práce
ekonomové	ekonom	k1gMnPc1	ekonom
přepracují	přepracovat	k5eAaPmIp3nP	přepracovat
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
model	model	k1gInSc4	model
peněžního	peněžní	k2eAgInSc2d1	peněžní
toku	tok	k1gInSc2	tok
během	během	k7c2	během
života	život	k1gInSc2	život
ropného	ropný	k2eAgNnSc2d1	ropné
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
toho	ten	k3xDgMnSc4	ten
management	management	k1gInSc1	management
rozhodně	rozhodně	k6eAd1	rozhodně
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
do	do	k7c2	do
projektu	projekt	k1gInSc2	projekt
dále	daleko	k6eAd2	daleko
investovat	investovat	k5eAaBmF	investovat
a	a	k8xC	a
přikročit	přikročit	k5eAaPmF	přikročit
k	k	k7c3	k
těžbě	těžba	k1gFnSc3	těžba
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
kladné	kladný	k2eAgNnSc1d1	kladné
<g/>
,	,	kIx,	,
tým	tým	k1gInSc1	tým
těžebních	těžební	k2eAgMnPc2d1	těžební
inženýrů	inženýr	k1gMnPc2	inženýr
postaví	postavit	k5eAaPmIp3nS	postavit
povrchová	povrchový	k2eAgFnSc1d1	povrchová
těžební	těžební	k2eAgNnSc4d1	těžební
zařízení	zařízení	k1gNnSc4	zařízení
a	a	k8xC	a
vrtní	vrtní	k2eAgMnPc1d1	vrtní
inženýři	inženýr	k1gMnPc1	inženýr
vyvrtají	vyvrtat	k5eAaPmIp3nP	vyvrtat
těžební	těžební	k2eAgInPc4d1	těžební
vrty	vrt	k1gInPc4	vrt
<g/>
.	.	kIx.	.
</s>
<s>
Průzkumné	průzkumný	k2eAgInPc1d1	průzkumný
vrty	vrt	k1gInPc1	vrt
se	se	k3xPyFc4	se
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
nehodí	hodit	k5eNaPmIp3nS	hodit
pro	pro	k7c4	pro
produkci	produkce	k1gFnSc4	produkce
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
hned	hned	k6eAd1	hned
po	po	k7c6	po
vyvrtání	vyvrtání	k1gNnSc6	vyvrtání
a	a	k8xC	a
ukončení	ukončení	k1gNnSc6	ukončení
karotážních	karotážní	k2eAgNnPc2d1	karotážní
měření	měření	k1gNnPc2	měření
zacementovány	zacementován	k2eAgFnPc1d1	zacementován
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Těžba	těžba	k1gFnSc1	těžba
==	==	k?	==
</s>
</p>
<p>
<s>
Tradiční	tradiční	k2eAgNnPc1d1	tradiční
ložiska	ložisko	k1gNnPc1	ložisko
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
jsou	být	k5eAaImIp3nP	být
strukturní	strukturní	k2eAgFnPc1d1	strukturní
pasti	past	k1gFnPc1	past
sestávající	sestávající	k2eAgFnPc1d1	sestávající
z	z	k7c2	z
porézní	porézní	k2eAgFnSc2d1	porézní
kolektorové	kolektorový	k2eAgFnSc2d1	kolektorová
horniny	hornina	k1gFnSc2	hornina
obklopené	obklopený	k2eAgFnSc2d1	obklopená
horninami	hornina	k1gFnPc7	hornina
nepropustnými	propustný	k2eNgFnPc7d1	nepropustná
(	(	kIx(	(
<g/>
břidlice	břidlice	k1gFnSc1	břidlice
<g/>
,	,	kIx,	,
sůl	sůl	k1gFnSc1	sůl
<g/>
,	,	kIx,	,
vyvřeliny	vyvřelina	k1gFnPc1	vyvřelina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Plyn	plyn	k1gInSc1	plyn
se	se	k3xPyFc4	se
nahromadí	nahromadit	k5eAaPmIp3nS	nahromadit
v	v	k7c6	v
nejvyšší	vysoký	k2eAgFnSc6d3	nejvyšší
části	část	k1gFnSc6	část
pasti	past	k1gFnSc2	past
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
navrtán	navrtán	k2eAgMnSc1d1	navrtán
a	a	k8xC	a
vytěžen	vytěžen	k2eAgMnSc1d1	vytěžen
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
dvoufázové	dvoufázový	k2eAgNnSc4d1	dvoufázové
ložisko	ložisko	k1gNnSc4	ložisko
(	(	kIx(	(
<g/>
plyn	plyn	k1gInSc1	plyn
a	a	k8xC	a
voda	voda	k1gFnSc1	voda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
optimální	optimální	k2eAgFnSc1d1	optimální
strategie	strategie	k1gFnSc1	strategie
je	být	k5eAaImIp3nS	být
navrtat	navrtat	k5eAaPmF	navrtat
klasickým	klasický	k2eAgInSc7d1	klasický
vertikálním	vertikální	k2eAgInSc7d1	vertikální
vrtem	vrt	k1gInSc7	vrt
samý	samý	k3xTgInSc4	samý
vrchol	vrchol	k1gInSc4	vrchol
struktury	struktura	k1gFnSc2	struktura
co	co	k9	co
nejdále	daleko	k6eAd3	daleko
od	od	k7c2	od
rozhraní	rozhraní	k1gNnSc2	rozhraní
plyn-voda	plynod	k1gMnSc2	plyn-vod
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
co	co	k3yInSc1	co
nejdéle	dlouho	k6eAd3	dlouho
zabránilo	zabránit	k5eAaPmAgNnS	zabránit
přítoků	přítok	k1gInPc2	přítok
vody	voda	k1gFnSc2	voda
do	do	k7c2	do
vrtu	vrt	k1gInSc2	vrt
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
třífázové	třífázový	k2eAgNnSc4d1	třífázové
ložisko	ložisko	k1gNnSc4	ložisko
(	(	kIx(	(
<g/>
plyn-ropa-voda	plynopaoda	k1gFnSc1	plyn-ropa-voda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ropa	ropa	k1gFnSc1	ropa
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
vytěžit	vytěžit	k5eAaPmF	vytěžit
nejdříve	dříve	k6eAd3	dříve
a	a	k8xC	a
teprve	teprve	k6eAd1	teprve
poté	poté	k6eAd1	poté
přistoupit	přistoupit	k5eAaPmF	přistoupit
k	k	k7c3	k
těžbě	těžba	k1gFnSc3	těžba
plynové	plynový	k2eAgFnSc2d1	plynová
čepičky	čepička	k1gFnSc2	čepička
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
plynová	plynový	k2eAgFnSc1d1	plynová
čepička	čepička	k1gFnSc1	čepička
vytěžila	vytěžit	k5eAaPmAgFnS	vytěžit
nejdříve	dříve	k6eAd3	dříve
v	v	k7c6	v
kolektoru	kolektor	k1gInSc6	kolektor
by	by	kYmCp3nS	by
poklesl	poklesnout	k5eAaPmAgInS	poklesnout
tlak	tlak	k1gInSc1	tlak
a	a	k8xC	a
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
viskozita	viskozita	k1gFnSc1	viskozita
zbývající	zbývající	k2eAgFnSc2d1	zbývající
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
nutně	nutně	k6eAd1	nutně
za	za	k7c4	za
následek	následek	k1gInSc4	následek
drastické	drastický	k2eAgNnSc1d1	drastické
snížení	snížení	k1gNnSc1	snížení
jejich	jejich	k3xOp3gFnPc2	jejich
vytěžitelných	vytěžitelný	k2eAgFnPc2d1	vytěžitelná
zásob	zásoba	k1gFnPc2	zásoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nekonvenční	konvenční	k2eNgNnPc4d1	nekonvenční
ložiska	ložisko	k1gNnPc4	ložisko
plynu	plyn	k1gInSc2	plyn
je	být	k5eAaImIp3nS	být
kolektivní	kolektivní	k2eAgInSc4d1	kolektivní
název	název	k1gInSc4	název
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
zdroje	zdroj	k1gInPc4	zdroj
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
kromě	kromě	k7c2	kromě
tradičních	tradiční	k2eAgInPc2d1	tradiční
dvou-	dvou-	k?	dvou-
a	a	k8xC	a
třífázových	třífázový	k2eAgInPc2d1	třífázový
kolektorů	kolektor	k1gInPc2	kolektor
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
těžbu	těžba	k1gFnSc4	těžba
plynu	plyn	k1gInSc2	plyn
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
zdrojové	zdrojový	k2eAgFnSc2d1	zdrojová
horniny	hornina	k1gFnSc2	hornina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
plyn	plyn	k1gInSc1	plyn
tvoří	tvořit	k5eAaImIp3nS	tvořit
in-situ	init	k1gInSc2	in-sit
(	(	kIx(	(
<g/>
=	=	kIx~	=
<g/>
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
místě	místo	k1gNnSc6	místo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgInSc3	tento
typu	typ	k1gInSc3	typ
ložiska	ložisko	k1gNnSc2	ložisko
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
břidlicový	břidlicový	k2eAgInSc1d1	břidlicový
plyn	plyn	k1gInSc1	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Zdrojové	zdrojový	k2eAgFnPc1d1	zdrojová
horniny	hornina	k1gFnPc1	hornina
jsou	být	k5eAaImIp3nP	být
typicky	typicky	k6eAd1	typicky
břidlice	břidlice	k1gFnPc4	břidlice
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
nízkou	nízký	k2eAgFnSc7d1	nízká
propustností	propustnost	k1gFnSc7	propustnost
<g/>
.	.	kIx.	.
</s>
<s>
Tradiční	tradiční	k2eAgInPc1d1	tradiční
vertikální	vertikální	k2eAgInPc1d1	vertikální
vrty	vrt	k1gInPc1	vrt
se	se	k3xPyFc4	se
nedají	dát	k5eNaPmIp3nP	dát
použít	použít	k5eAaPmF	použít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
obnaží	obnažit	k5eAaPmIp3nS	obnažit
jen	jen	k9	jen
poměrně	poměrně	k6eAd1	poměrně
krátkou	krátký	k2eAgFnSc4d1	krátká
délku	délka	k1gFnSc4	délka
formace	formace	k1gFnSc2	formace
<g/>
.	.	kIx.	.
</s>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
vrty	vrt	k1gInPc1	vrt
ukloněné	ukloněný	k2eAgInPc1d1	ukloněný
nebo	nebo	k8xC	nebo
i	i	k9	i
horizontální	horizontální	k2eAgFnSc1d1	horizontální
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgFnPc6	který
délka	délka	k1gFnSc1	délka
horizontální	horizontální	k2eAgFnSc2d1	horizontální
sekce	sekce	k1gFnSc2	sekce
může	moct	k5eAaImIp3nS	moct
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
až	až	k6eAd1	až
několika	několik	k4yIc3	několik
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
obnažit	obnažit	k5eAaPmF	obnažit
co	co	k9	co
největší	veliký	k2eAgFnSc4d3	veliký
délku	délka	k1gFnSc4	délka
formace	formace	k1gFnSc2	formace
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
přistoupí	přistoupit	k5eAaPmIp3nP	přistoupit
k	k	k7c3	k
hydraulickému	hydraulický	k2eAgNnSc3d1	hydraulické
štěpení	štěpení	k1gNnSc3	štěpení
formace	formace	k1gFnSc2	formace
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
vytvořily	vytvořit	k5eAaPmAgInP	vytvořit
systémy	systém	k1gInPc1	systém
umělých	umělý	k2eAgFnPc2d1	umělá
puklin	puklina	k1gFnPc2	puklina
<g/>
,	,	kIx,	,
skrz	skrz	k6eAd1	skrz
které	který	k3yQgFnPc1	který
by	by	kYmCp3nP	by
do	do	k7c2	do
vrtů	vrt	k1gInPc2	vrt
mohl	moct	k5eAaImAgMnS	moct
začít	začít	k5eAaPmF	začít
proudit	proudit	k5eAaImF	proudit
obsah	obsah	k1gInSc4	obsah
formace	formace	k1gFnSc2	formace
(	(	kIx(	(
<g/>
plyn	plyn	k1gInSc1	plyn
a	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
nebo	nebo	k8xC	nebo
ropa	ropa	k1gFnSc1	ropa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hydraulické	hydraulický	k2eAgNnSc1d1	hydraulické
štěpení	štěpení	k1gNnSc1	štěpení
se	se	k3xPyFc4	se
u	u	k7c2	u
dlouhých	dlouhý	k2eAgInPc2d1	dlouhý
horizontálních	horizontální	k2eAgInPc2d1	horizontální
vrtů	vrt	k1gInPc2	vrt
provádí	provádět	k5eAaImIp3nS	provádět
v	v	k7c6	v
několika	několik	k4yIc6	několik
stádiích	stádium	k1gNnPc6	stádium
a	a	k8xC	a
nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
vícestupňové	vícestupňový	k2eAgNnSc1d1	vícestupňové
hydraulické	hydraulický	k2eAgNnSc1d1	hydraulické
štěpení	štěpení	k1gNnSc1	štěpení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
nekonvenčním	konvenční	k2eNgInSc7d1	nekonvenční
zdrojem	zdroj	k1gInSc7	zdroj
je	být	k5eAaImIp3nS	být
metan	metan	k1gInSc1	metan
uhelných	uhelný	k2eAgFnPc2d1	uhelná
slojí	sloj	k1gFnPc2	sloj
(	(	kIx(	(
<g/>
CBM	CBM	kA	CBM
<g/>
,	,	kIx,	,
Coalbed	Coalbed	k1gMnSc1	Coalbed
Methane	methan	k1gInSc5	methan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
metan	metan	k1gInSc4	metan
uložený	uložený	k2eAgInSc4d1	uložený
v	v	k7c6	v
uhlí	uhlí	k1gNnSc2	uhlí
procesem	proces	k1gInSc7	proces
zvaným	zvaný	k2eAgInSc7d1	zvaný
adsorpce	adsorpce	k1gFnSc2	adsorpce
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
typického	typický	k2eAgInSc2d1	typický
pískovce	pískovec	k1gInSc2	pískovec
nebo	nebo	k8xC	nebo
jiného	jiný	k2eAgNnSc2d1	jiné
tradičního	tradiční	k2eAgNnSc2d1	tradiční
ložiska	ložisko	k1gNnSc2	ložisko
<g/>
.	.	kIx.	.
</s>
<s>
Nazývá	nazývat	k5eAaImIp3nS	nazývat
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
sladký	sladký	k2eAgInSc1d1	sladký
plyn	plyn	k1gInSc1	plyn
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
málo	málo	k1gNnSc1	málo
sirovodíku	sirovodík	k1gInSc2	sirovodík
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
v	v	k7c6	v
téměř	téměř	k6eAd1	téměř
tekutém	tekutý	k2eAgInSc6d1	tekutý
stavu	stav	k1gInSc6	stav
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
tradičních	tradiční	k2eAgNnPc2d1	tradiční
ložisek	ložisko	k1gNnPc2	ložisko
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
velmi	velmi	k6eAd1	velmi
málo	málo	k4c1	málo
těžších	těžký	k2eAgInPc2d2	těžší
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
propan	propan	k1gInSc4	propan
nebo	nebo	k8xC	nebo
butan	butan	k1gInSc4	butan
<g/>
,	,	kIx,	,
a	a	k8xC	a
žádný	žádný	k3yNgInSc4	žádný
kondenzát	kondenzát	k1gInSc4	kondenzát
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
až	až	k9	až
několik	několik	k4yIc1	několik
procent	procento	k1gNnPc2	procento
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
uhelné	uhelný	k2eAgFnPc1d1	uhelná
sloje	sloj	k1gFnPc1	sloj
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
ložiska	ložisko	k1gNnPc1	ložisko
v	v	k7c4	v
Illawarra	Illawarro	k1gNnPc4	Illawarro
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
naopak	naopak	k6eAd1	naopak
málo	málo	k4c4	málo
metanu	metan	k1gInSc2	metan
a	a	k8xC	a
převládající	převládající	k2eAgInSc1d1	převládající
plyn	plyn	k1gInSc1	plyn
je	být	k5eAaImIp3nS	být
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
metanu	metan	k1gInSc2	metan
v	v	k7c6	v
uhelných	uhelný	k2eAgFnPc6d1	uhelná
slojích	sloj	k1gFnPc6	sloj
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
z	z	k7c2	z
podzemní	podzemní	k2eAgFnSc2d1	podzemní
těžby	těžba	k1gFnSc2	těžba
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
představuje	představovat	k5eAaImIp3nS	představovat
vážné	vážný	k2eAgNnSc4d1	vážné
bezpečnostní	bezpečnostní	k2eAgNnSc4d1	bezpečnostní
riziko	riziko	k1gNnSc4	riziko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Těžba	těžba	k1gFnSc1	těžba
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
principu	princip	k1gInSc6	princip
desorpce	desorpce	k1gFnSc2	desorpce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
podle	podle	k7c2	podle
křivky	křivka	k1gFnSc2	křivka
Langmuirovy	Langmuirův	k2eAgFnSc2d1	Langmuirova
adsorpční	adsorpční	k2eAgFnSc2d1	adsorpční
izotermy	izoterma	k1gFnSc2	izoterma
<g/>
.	.	kIx.	.
</s>
<s>
Adsorbovaný	adsorbovaný	k2eAgInSc1d1	adsorbovaný
metan	metan	k1gInSc1	metan
se	se	k3xPyFc4	se
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
sníží	snížit	k5eAaPmIp3nS	snížit
v	v	k7c6	v
uhelné	uhelný	k2eAgFnSc6d1	uhelná
sloji	sloj	k1gFnSc6	sloj
tlak	tlak	k1gInSc1	tlak
<g/>
.	.	kIx.	.
</s>
<s>
Metan	metan	k1gInSc1	metan
je	být	k5eAaImIp3nS	být
těžen	těžit	k5eAaImNgInS	těžit
vrtáním	vrtání	k1gNnSc7	vrtání
do	do	k7c2	do
uhelné	uhelný	k2eAgFnSc2d1	uhelná
sloje	sloj	k1gFnSc2	sloj
s	s	k7c7	s
následným	následný	k2eAgNnSc7d1	následné
čerpáním	čerpání	k1gNnSc7	čerpání
vody	voda	k1gFnSc2	voda
ze	z	k7c2	z
sloje	sloj	k1gFnSc2	sloj
<g/>
.	.	kIx.	.
</s>
<s>
Pokles	pokles	k1gInSc1	pokles
tlaku	tlak	k1gInSc2	tlak
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
desorpci	desorpce	k1gFnSc4	desorpce
metanu	metan	k1gInSc2	metan
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
průtok	průtok	k1gInSc1	průtok
v	v	k7c6	v
plynném	plynný	k2eAgNnSc6d1	plynné
skupenství	skupenství	k1gNnSc6	skupenství
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
tlakem	tlak	k1gInSc7	tlak
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
plynovodem	plynovod	k1gInSc7	plynovod
distribuován	distribuovat	k5eAaBmNgInS	distribuovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
nekonvenčním	konvenční	k2eNgInSc7d1	nekonvenční
zdrojem	zdroj	k1gInSc7	zdroj
jsou	být	k5eAaImIp3nP	být
zmrzlé	zmrzlý	k2eAgInPc1d1	zmrzlý
hydráty	hydrát	k1gInPc1	hydrát
metanu	metan	k1gInSc2	metan
pod	pod	k7c7	pod
mořským	mořský	k2eAgInSc7d1	mořský
dnem	den	k1gInSc7	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
FÍK	fík	k1gInSc1	fík
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Zemní	zemní	k2eAgInSc1d1	zemní
plyn	plyn	k1gInSc1	plyn
<g/>
:	:	kIx,	:
tabulky	tabulka	k1gFnPc1	tabulka
<g/>
,	,	kIx,	,
diagramy	diagram	k1gInPc1	diagram
<g/>
,	,	kIx,	,
rovnice	rovnice	k1gFnPc1	rovnice
<g/>
,	,	kIx,	,
výpočty	výpočet	k1gInPc1	výpočet
<g/>
,	,	kIx,	,
výpočtové	výpočtový	k2eAgNnSc1d1	výpočtové
pravítko	pravítko	k1gNnSc1	pravítko
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Agentura	agentura	k1gFnSc1	agentura
ČSTZ	ČSTZ	kA	ČSTZ
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-86028-22-4	[number]	k4	80-86028-22-4
</s>
</p>
<p>
<s>
Nils	Nils	k1gInSc1	Nils
G.	G.	kA	G.
Holm	Holm	k1gInSc1	Holm
a	a	k8xC	a
Jean	Jean	k1gMnSc1	Jean
Luc	Luc	k1gMnSc1	Luc
Charlou	Charla	k1gMnSc7	Charla
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
Initial	Initial	k1gMnSc1	Initial
indications	indicationsa	k1gFnPc2	indicationsa
of	of	k?	of
abiotic	abiotice	k1gFnPc2	abiotice
formation	formation	k1gInSc4	formation
of	of	k?	of
hydrocarbons	hydrocarbons	k1gInSc1	hydrocarbons
in	in	k?	in
the	the	k?	the
Rainbow	Rainbow	k1gMnSc1	Rainbow
ultramafic	ultramafic	k1gMnSc1	ultramafic
hydrothermal	hydrothermat	k5eAaBmAgMnS	hydrothermat
system	syst	k1gMnSc7	syst
<g/>
,	,	kIx,	,
Mid-Atlantic	Mid-Atlantice	k1gFnPc2	Mid-Atlantice
Ridge	Ridge	k1gFnPc2	Ridge
<g/>
;	;	kIx,	;
Earth	Eartha	k1gFnPc2	Eartha
and	and	k?	and
Planetary	Planetar	k1gInPc1	Planetar
Science	Science	k1gFnSc1	Science
Letters	Letters	k1gInSc1	Letters
<g/>
,	,	kIx,	,
vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
191	[number]	k4	191
<g/>
,	,	kIx,	,
cislo	cisnout	k5eAaImAgNnS	cisnout
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Ropný	ropný	k2eAgInSc1d1	ropný
vrchol	vrchol	k1gInSc1	vrchol
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
zemní	zemní	k2eAgInSc4d1	zemní
plyn	plyn	k1gInSc4	plyn
se	se	k3xPyFc4	se
zatím	zatím	k6eAd1	zatím
očekává	očekávat	k5eAaImIp3nS	očekávat
někdy	někdy	k6eAd1	někdy
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
2010	[number]	k4	2010
a	a	k8xC	a
2020	[number]	k4	2020
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Focul	Focul	k1gInSc1	Focul
viu	viu	k?	viu
(	(	kIx(	(
<g/>
přírodní	přírodní	k2eAgInSc1d1	přírodní
vývěr	vývěr	k1gInSc1	vývěr
plynu	plyn	k1gInSc2	plyn
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Plynárenství	plynárenství	k1gNnSc1	plynárenství
(	(	kIx(	(
<g/>
obor	obor	k1gInSc1	obor
zabývající	zabývající	k2eAgInSc1d1	zabývající
se	se	k3xPyFc4	se
těžbou	těžba	k1gFnSc7	těžba
a	a	k8xC	a
zpracováním	zpracování	k1gNnSc7	zpracování
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
zemní	zemní	k2eAgInSc4d1	zemní
plyn	plyn	k1gInSc4	plyn
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
zemní	zemnit	k5eAaImIp3nS	zemnit
plyn	plyn	k1gInSc4	plyn
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
