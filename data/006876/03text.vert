<s>
Kanton	Kanton	k1gInSc1	Kanton
Narbonne-Est	Narbonne-Est	k1gInSc1	Narbonne-Est
(	(	kIx(	(
<g/>
fr.	fr.	k?	fr.
Canton	Canton	k1gInSc1	Canton
de	de	k?	de
Narbonne-Est	Narbonne-Est	k1gInSc1	Narbonne-Est
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
francouzský	francouzský	k2eAgInSc4d1	francouzský
kanton	kanton	k1gInSc4	kanton
v	v	k7c6	v
departementu	departement	k1gInSc6	departement
Aude	Aud	k1gFnSc2	Aud
v	v	k7c6	v
regionu	region	k1gInSc6	region
Languedoc-Roussillon	Languedoc-Roussillon	k1gInSc1	Languedoc-Roussillon
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
ho	on	k3xPp3gInSc4	on
východní	východní	k2eAgFnSc1d1	východní
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
Narbonne	Narbonn	k1gMnSc5	Narbonn
<g/>
.	.	kIx.	.
</s>
