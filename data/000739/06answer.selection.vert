<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Německa	Německo	k1gNnSc2	Německo
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
a	a	k8xC	a
od	od	k7c2	od
sjednocení	sjednocení	k1gNnSc2	sjednocení
Německa	Německo	k1gNnSc2	Německo
(	(	kIx(	(
<g/>
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
obou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
města	město	k1gNnSc2	město
<g/>
)	)	kIx)	)
<g/>
Berlín	Berlín	k1gInSc1	Berlín
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
největším	veliký	k2eAgNnPc3d3	veliký
městům	město	k1gNnPc3	město
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
druhým	druhý	k4xOgNnSc7	druhý
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
