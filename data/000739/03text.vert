<s>
Berlín	Berlín	k1gInSc1	Berlín
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Berlin	berlina	k1gFnPc2	berlina
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
spolková	spolkový	k2eAgFnSc1d1	spolková
země	země	k1gFnSc1	země
Spolkové	spolkový	k2eAgFnSc2d1	spolková
republiky	republika	k1gFnSc2	republika
Německo	Německo	k1gNnSc1	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Německa	Německo	k1gNnSc2	Německo
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
a	a	k8xC	a
od	od	k7c2	od
sjednocení	sjednocení	k1gNnSc2	sjednocení
Německa	Německo	k1gNnSc2	Německo
(	(	kIx(	(
<g/>
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
obou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
města	město	k1gNnSc2	město
<g/>
)	)	kIx)	)
<g/>
Berlín	Berlín	k1gInSc1	Berlín
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
největším	veliký	k2eAgNnPc3d3	veliký
městům	město	k1gNnPc3	město
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
druhým	druhý	k4xOgNnSc7	druhý
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Berlín	Berlín	k1gInSc1	Berlín
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
70	[number]	k4	70
km	km	kA	km
západně	západně	k6eAd1	západně
od	od	k7c2	od
hranic	hranice	k1gFnPc2	hranice
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
obklopen	obklopit	k5eAaPmNgMnS	obklopit
spolkovou	spolkový	k2eAgFnSc7d1	spolková
zemí	zem	k1gFnSc7	zem
Braniborsko	Braniborsko	k1gNnSc4	Braniborsko
(	(	kIx(	(
<g/>
Brandenburg	Brandenburg	k1gInSc1	Brandenburg
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Městem	město	k1gNnSc7	město
protéká	protékat	k5eAaImIp3nS	protékat
řeka	řeka	k1gFnSc1	řeka
Spréva	Spréva	k1gFnSc1	Spréva
(	(	kIx(	(
<g/>
Spree	Spree	k1gInSc1	Spree
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgFnSc2d1	vodní
plochy	plocha	k1gFnSc2	plocha
(	(	kIx(	(
<g/>
především	především	k9	především
jezera	jezero	k1gNnSc2	jezero
<g/>
)	)	kIx)	)
tvoří	tvořit	k5eAaImIp3nS	tvořit
6,6	[number]	k4	6,6
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
plochy	plocha	k1gFnSc2	plocha
<g/>
;	;	kIx,	;
mimo	mimo	k6eAd1	mimo
mnoha	mnoho	k4c2	mnoho
parků	park	k1gInPc2	park
a	a	k8xC	a
obdobných	obdobný	k2eAgFnPc2d1	obdobná
zelených	zelený	k2eAgFnPc2d1	zelená
ploch	plocha	k1gFnPc2	plocha
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
lesní	lesní	k2eAgInPc4d1	lesní
porosty	porost	k1gInPc4	porost
(	(	kIx(	(
<g/>
17,9	[number]	k4	17,9
%	%	kIx~	%
celkové	celkový	k2eAgFnSc2d1	celková
plochy	plocha	k1gFnSc2	plocha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
výška	výška	k1gFnSc1	výška
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
:	:	kIx,	:
34	[number]	k4	34
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
Teufelsberg	Teufelsberg	k1gInSc1	Teufelsberg
<g/>
,	,	kIx,	,
115	[number]	k4	115
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
(	(	kIx(	(
<g/>
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
umělý	umělý	k2eAgInSc4d1	umělý
násyp	násyp	k1gInSc4	násyp
<g/>
,	,	kIx,	,
vzniklý	vzniklý	k2eAgInSc4d1	vzniklý
ze	z	k7c2	z
zbytků	zbytek	k1gInPc2	zbytek
stavebního	stavební	k2eAgInSc2d1	stavební
materiálu	materiál	k1gInSc2	materiál
vybombardovaných	vybombardovaný	k2eAgInPc2d1	vybombardovaný
domů	dům	k1gInPc2	dům
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
Berlín	Berlín	k1gInSc1	Berlín
<g/>
-	-	kIx~	-
<g/>
Praha	Praha	k1gFnSc1	Praha
činí	činit	k5eAaImIp3nS	činit
kolem	kolem	k7c2	kolem
350	[number]	k4	350
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
vlakem	vlak	k1gInSc7	vlak
trvá	trvat	k5eAaImIp3nS	trvat
necelých	celý	k2eNgInPc2d1	necelý
pět	pět	k4xCc4	pět
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
cesta	cesta	k1gFnSc1	cesta
letadlem	letadlo	k1gNnSc7	letadlo
kolem	kolem	k7c2	kolem
40	[number]	k4	40
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Berlína	Berlín	k1gInSc2	Berlín
a	a	k8xC	a
Rozdělení	rozdělení	k1gNnSc1	rozdělení
Berlína	Berlín	k1gInSc2	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnPc1	první
historicky	historicky	k6eAd1	historicky
doložené	doložená	k1gFnPc4	doložená
zmínění	zmínění	k1gNnSc2	zmínění
Berlína	Berlín	k1gInSc2	Berlín
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1244	[number]	k4	1244
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1451	[number]	k4	1451
se	se	k3xPyFc4	se
Berlín	Berlín	k1gInSc1	Berlín
stává	stávat	k5eAaImIp3nS	stávat
vládním	vládní	k2eAgNnSc7d1	vládní
městem	město	k1gNnSc7	město
braniborských	braniborský	k2eAgMnPc2d1	braniborský
markrabích	markrabí	k1gMnPc2	markrabí
a	a	k8xC	a
kurfiřtů	kurfiřt	k1gMnPc2	kurfiřt
<g/>
,	,	kIx,	,
1701	[number]	k4	1701
pak	pak	k6eAd1	pak
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
Pruského	pruský	k2eAgNnSc2d1	pruské
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
1871	[number]	k4	1871
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
nově	nově	k6eAd1	nově
založené	založený	k2eAgFnSc2d1	založená
Německé	německý	k2eAgFnSc2d1	německá
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Berlín	Berlín	k1gInSc1	Berlín
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
tolerantním	tolerantní	k2eAgNnSc7d1	tolerantní
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nalezlo	naleznout	k5eAaPmAgNnS	naleznout
útočiště	útočiště	k1gNnSc1	útočiště
mnoho	mnoho	k6eAd1	mnoho
pronásledovaných	pronásledovaný	k2eAgMnPc2d1	pronásledovaný
uprchlíků	uprchlík	k1gMnPc2	uprchlík
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
hugenoti	hugenot	k1gMnPc1	hugenot
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
(	(	kIx(	(
<g/>
po	po	k7c6	po
r.	r.	kA	r.
1685	[number]	k4	1685
<g/>
)	)	kIx)	)
a	a	k8xC	a
protestanti	protestant	k1gMnPc1	protestant
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
(	(	kIx(	(
<g/>
po	po	k7c6	po
r.	r.	kA	r.
1730	[number]	k4	1730
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnPc1	jejichž
někteří	některý	k3yIgMnPc1	některý
potomci	potomek	k1gMnPc1	potomek
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgMnPc1d1	žijící
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
České	český	k2eAgFnSc6d1	Česká
vesnici	vesnice	k1gFnSc6	vesnice
(	(	kIx(	(
<g/>
Böhmisches	Böhmisches	k1gInSc1	Böhmisches
Dorf	Dorf	k1gInSc1	Dorf
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
až	až	k9	až
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
ještě	ještě	k9	ještě
hovořili	hovořit	k5eAaImAgMnP	hovořit
česky	česky	k6eAd1	česky
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvacátých	dvacátý	k4xOgNnPc6	dvacátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
z	z	k7c2	z
Berlína	Berlín	k1gInSc2	Berlín
stává	stávat	k5eAaImIp3nS	stávat
evropská	evropský	k2eAgFnSc1d1	Evropská
metropole	metropole	k1gFnSc1	metropole
s	s	k7c7	s
bohatým	bohatý	k2eAgInSc7d1	bohatý
politickým	politický	k2eAgInSc7d1	politický
<g/>
,	,	kIx,	,
vědeckým	vědecký	k2eAgInSc7d1	vědecký
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
kulturním	kulturní	k2eAgInSc7d1	kulturní
životem	život	k1gInSc7	život
<g/>
.	.	kIx.	.
</s>
<s>
Tolerance	tolerance	k1gFnSc1	tolerance
končí	končit	k5eAaImIp3nS	končit
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
příchodem	příchod	k1gInSc7	příchod
Hitlera	Hitler	k1gMnSc2	Hitler
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
zničeno	zničen	k2eAgNnSc1d1	zničeno
(	(	kIx(	(
<g/>
např.	např.	kA	např.
40	[number]	k4	40
procent	procento	k1gNnPc2	procento
obytné	obytný	k2eAgFnSc2d1	obytná
plochy	plocha	k1gFnSc2	plocha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
Berlín	Berlín	k1gInSc1	Berlín
rozdělen	rozdělit	k5eAaPmNgInS	rozdělit
do	do	k7c2	do
čtyř	čtyři	k4xCgInPc2	čtyři
okupačních	okupační	k2eAgInPc2d1	okupační
sektorů	sektor	k1gInPc2	sektor
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
Berlín	Berlín	k1gInSc1	Berlín
dostal	dostat	k5eAaPmAgInS	dostat
do	do	k7c2	do
středu	střed	k1gInSc2	střed
konfliktů	konflikt	k1gInPc2	konflikt
mezi	mezi	k7c7	mezi
vítěznými	vítězný	k2eAgFnPc7d1	vítězná
mocnostmi	mocnost	k1gFnPc7	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
Berlínské	berlínský	k2eAgFnSc3d1	Berlínská
blokádě	blokáda	k1gFnSc3	blokáda
(	(	kIx(	(
<g/>
kdy	kdy	k6eAd1	kdy
veškeré	veškerý	k3xTgInPc4	veškerý
zásobování	zásobování	k1gNnSc1	zásobování
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
výlučně	výlučně	k6eAd1	výlučně
leteckým	letecký	k2eAgNnSc7d1	letecké
spojením	spojení	k1gNnSc7	spojení
<g/>
)	)	kIx)	)
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
k	k	k7c3	k
politickému	politický	k2eAgNnSc3d1	politické
rozdělení	rozdělení	k1gNnSc3	rozdělení
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
faktickému	faktický	k2eAgNnSc3d1	faktické
rozdělení	rozdělení	k1gNnSc3	rozdělení
dochází	docházet	k5eAaImIp3nP	docházet
stavbou	stavba	k1gFnSc7	stavba
Berlínské	berlínský	k2eAgFnSc2d1	Berlínská
zdi	zeď	k1gFnSc2	zeď
13	[number]	k4	13
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1961	[number]	k4	1961
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
Berlínské	berlínský	k2eAgFnSc2d1	Berlínská
zdi	zeď	k1gFnSc2	zeď
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
začíná	začínat	k5eAaImIp3nS	začínat
nová	nový	k2eAgFnSc1d1	nová
kapitola	kapitola	k1gFnSc1	kapitola
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Berlín	Berlín	k1gInSc1	Berlín
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
městem	město	k1gNnSc7	město
se	s	k7c7	s
zázemím	zázemí	k1gNnSc7	zázemí
a	a	k8xC	a
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
SRN	SRN	kA	SRN
<g/>
.	.	kIx.	.
</s>
<s>
Věková	věkový	k2eAgFnSc1d1	věková
struktura	struktura	k1gFnSc1	struktura
se	se	k3xPyFc4	se
po	po	k7c6	po
sjednocení	sjednocení	k1gNnSc6	sjednocení
Berlína	Berlín	k1gInSc2	Berlín
normalizovala	normalizovat	k5eAaBmAgFnS	normalizovat
<g/>
.	.	kIx.	.
</s>
<s>
Dřívější	dřívější	k2eAgFnSc1d1	dřívější
západní	západní	k2eAgFnSc1d1	západní
část	část	k1gFnSc1	část
města	město	k1gNnSc2	město
trpěla	trpět	k5eAaImAgFnS	trpět
svou	svůj	k3xOyFgFnSc7	svůj
polohou	poloha	k1gFnSc7	poloha
<g/>
:	:	kIx,	:
přes	přes	k7c4	přes
vysoký	vysoký	k2eAgInSc4d1	vysoký
počet	počet	k1gInSc4	počet
studentů	student	k1gMnPc2	student
bylo	být	k5eAaImAgNnS	být
přestárnutí	přestárnutí	k1gNnSc4	přestárnutí
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
aktuálním	aktuální	k2eAgInSc7d1	aktuální
problémem	problém	k1gInSc7	problém
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
omezenému	omezený	k2eAgInSc3d1	omezený
počtu	počet	k1gInSc3	počet
nových	nový	k2eAgNnPc2d1	nové
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
struktuře	struktura	k1gFnSc6	struktura
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
podle	podle	k7c2	podle
zaměstnání	zaměstnání	k1gNnSc2	zaměstnání
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
popředí	popředí	k1gNnSc6	popředí
výrazně	výrazně	k6eAd1	výrazně
sektor	sektor	k1gInSc1	sektor
služeb	služba	k1gFnPc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
situace	situace	k1gFnSc1	situace
je	být	k5eAaImIp3nS	být
charakterizována	charakterizovat	k5eAaBmNgFnS	charakterizovat
následujícími	následující	k2eAgFnPc7d1	následující
čísly	číslo	k1gNnPc7	číslo
(	(	kIx(	(
<g/>
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Již	již	k6eAd1	již
Západní	západní	k2eAgInSc1d1	západní
Berlín	Berlín	k1gInSc1	Berlín
se	se	k3xPyFc4	se
vyznačoval	vyznačovat	k5eAaImAgInS	vyznačovat
relativně	relativně	k6eAd1	relativně
vysokým	vysoký	k2eAgInSc7d1	vysoký
podílem	podíl	k1gInSc7	podíl
cizinců	cizinec	k1gMnPc2	cizinec
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
celkového	celkový	k2eAgInSc2d1	celkový
počtu	počet	k1gInSc2	počet
1,8	[number]	k4	1,8
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
ve	v	k7c6	v
městě	město	k1gNnSc6	město
bydlelo	bydlet	k5eAaImAgNnS	bydlet
např.	např.	kA	např.
128.000	[number]	k4	128.000
obyvatel	obyvatel	k1gMnPc2	obyvatel
turecké	turecký	k2eAgFnSc2d1	turecká
národnosti	národnost	k1gFnSc2	národnost
(	(	kIx(	(
<g/>
43,2	[number]	k4	43,2
%	%	kIx~	%
všech	všecek	k3xTgMnPc2	všecek
cizinců	cizinec	k1gMnPc2	cizinec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgFnSc1d1	dnešní
situace	situace	k1gFnSc1	situace
je	být	k5eAaImIp3nS	být
obdobná	obdobný	k2eAgFnSc1d1	obdobná
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
údajů	údaj	k1gInPc2	údaj
statistického	statistický	k2eAgInSc2d1	statistický
úřadu	úřad	k1gInSc2	úřad
činil	činit	k5eAaImAgInS	činit
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
ve	v	k7c6	v
městě	město	k1gNnSc6	město
hlášených	hlášený	k2eAgMnPc2d1	hlášený
občanů	občan	k1gMnPc2	občan
neněmecké	německý	k2eNgFnSc2d1	neněmecká
národnosti	národnost	k1gFnSc2	národnost
k	k	k7c3	k
30	[number]	k4	30
<g/>
.	.	kIx.	.
červnu	červen	k1gInSc3	červen
2006	[number]	k4	2006
463	[number]	k4	463
723	[number]	k4	723
(	(	kIx(	(
<g/>
13,9	[number]	k4	13,9
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
města	město	k1gNnSc2	město
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
183	[number]	k4	183
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
činí	činit	k5eAaImIp3nS	činit
3	[number]	k4	3
398	[number]	k4	398
205	[number]	k4	205
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
největším	veliký	k2eAgFnPc3d3	veliký
národnostním	národnostní	k2eAgFnPc3d1	národnostní
menšinám	menšina	k1gFnPc3	menšina
patří	patřit	k5eAaImIp3nS	patřit
turecká	turecký	k2eAgFnSc1d1	turecká
národnost	národnost	k1gFnSc1	národnost
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
příslušníci	příslušník	k1gMnPc1	příslušník
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
částečně	částečně	k6eAd1	částečně
již	již	k6eAd1	již
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
a	a	k8xC	a
čtvrté	čtvrtý	k4xOgFnSc3	čtvrtý
generaci	generace	k1gFnSc3	generace
<g/>
.	.	kIx.	.
</s>
<s>
Druhou	druhý	k4xOgFnSc4	druhý
největší	veliký	k2eAgFnSc4d3	veliký
národnostní	národnostní	k2eAgFnSc4d1	národnostní
skupinu	skupina	k1gFnSc4	skupina
tvoří	tvořit	k5eAaImIp3nP	tvořit
občané	občan	k1gMnPc1	občan
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
poté	poté	k6eAd1	poté
Srbska	Srbsko	k1gNnSc2	Srbsko
a	a	k8xC	a
Černé	Černé	k2eAgFnSc2d1	Černé
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Problematické	problematický	k2eAgNnSc1d1	problematické
je	být	k5eAaImIp3nS	být
posouzení	posouzení	k1gNnSc1	posouzení
počtu	počet	k1gInSc2	počet
a	a	k8xC	a
podílu	podíl	k1gInSc2	podíl
občanů	občan	k1gMnPc2	občan
arabské	arabský	k2eAgFnSc2d1	arabská
národnosti	národnost	k1gFnSc2	národnost
(	(	kIx(	(
<g/>
přes	přes	k7c4	přes
20	[number]	k4	20
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
4,6	[number]	k4	4,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
zejména	zejména	k9	zejména
o	o	k7c4	o
uprchlíky	uprchlík	k1gMnPc4	uprchlík
z	z	k7c2	z
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Libanonu	Libanon	k1gInSc6	Libanon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
největší	veliký	k2eAgFnSc4d3	veliký
skupinu	skupina	k1gFnSc4	skupina
tvoří	tvořit	k5eAaImIp3nS	tvořit
kategorie	kategorie	k1gFnSc1	kategorie
"	"	kIx"	"
<g/>
národnost	národnost	k1gFnSc1	národnost
nevyjasněna	vyjasněn	k2eNgFnSc1d1	nevyjasněna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Trend	trend	k1gInSc1	trend
dalšího	další	k2eAgInSc2d1	další
růstu	růst	k1gInSc2	růst
občanů	občan	k1gMnPc2	občan
neněmecké	německý	k2eNgFnSc2d1	neněmecká
národnosti	národnost	k1gFnSc2	národnost
se	se	k3xPyFc4	se
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
i	i	k9	i
nadále	nadále	k6eAd1	nadále
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc3	prosinec
2006	[number]	k4	2006
žilo	žít	k5eAaImAgNnS	žít
ve	v	k7c6	v
městě	město	k1gNnSc6	město
467	[number]	k4	467
683	[number]	k4	683
cizinců	cizinec	k1gMnPc2	cizinec
z	z	k7c2	z
184	[number]	k4	184
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
14	[number]	k4	14
procent	procento	k1gNnPc2	procento
přihlášených	přihlášený	k2eAgMnPc2d1	přihlášený
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
;	;	kIx,	;
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosincem	prosinec	k1gInSc7	prosinec
2005	[number]	k4	2005
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
přírůstek	přírůstek	k1gInSc1	přírůstek
o	o	k7c4	o
1,5	[number]	k4	1,5
procent	procento	k1gNnPc2	procento
<g/>
.	.	kIx.	.
</s>
<s>
Uvedená	uvedený	k2eAgNnPc4d1	uvedené
čísla	číslo	k1gNnPc4	číslo
je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
třeba	třeba	k6eAd1	třeba
brát	brát	k5eAaImF	brát
i	i	k9	i
v	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
ohledu	ohled	k1gInSc6	ohled
s	s	k7c7	s
rezervou	rezerva	k1gFnSc7	rezerva
<g/>
.	.	kIx.	.
</s>
<s>
Německo	Německo	k1gNnSc1	Německo
nerozlišuje	rozlišovat	k5eNaImIp3nS	rozlišovat
mezi	mezi	k7c7	mezi
národností	národnost	k1gFnSc7	národnost
a	a	k8xC	a
státní	státní	k2eAgFnSc7d1	státní
příslušností	příslušnost	k1gFnSc7	příslušnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
žije	žít	k5eAaImIp3nS	žít
např.	např.	kA	např.
i	i	k8xC	i
velká	velký	k2eAgFnSc1d1	velká
skupina	skupina	k1gFnSc1	skupina
tzv.	tzv.	kA	tzv.
vystěhovalců	vystěhovalec	k1gMnPc2	vystěhovalec
ze	z	k7c2	z
zemí	zem	k1gFnPc2	zem
bývalého	bývalý	k2eAgInSc2d1	bývalý
SSSR	SSSR	kA	SSSR
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
jakýmkoliv	jakýkoliv	k3yIgInSc7	jakýkoliv
způsobem	způsob	k1gInSc7	způsob
prokázali	prokázat	k5eAaPmAgMnP	prokázat
svůj	svůj	k3xOyFgInSc4	svůj
německý	německý	k2eAgInSc4d1	německý
původ	původ	k1gInSc4	původ
a	a	k8xC	a
získali	získat	k5eAaPmAgMnP	získat
tím	ten	k3xDgNnSc7	ten
německé	německý	k2eAgNnSc1d1	německé
občanství	občanství	k1gNnSc1	občanství
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
jejich	jejich	k3xOp3gFnPc1	jejich
znalosti	znalost	k1gFnPc1	znalost
němčiny	němčina	k1gFnSc2	němčina
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
mizivé	mizivý	k2eAgFnPc1d1	mizivá
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
ve	v	k7c6	v
výše	vysoce	k6eAd2	vysoce
uvedených	uvedený	k2eAgNnPc6d1	uvedené
číslech	číslo	k1gNnPc6	číslo
zachyceni	zachycen	k2eAgMnPc1d1	zachycen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
koncentrace	koncentrace	k1gFnSc1	koncentrace
obyvatel	obyvatel	k1gMnPc2	obyvatel
jiných	jiný	k2eAgFnPc2d1	jiná
národností	národnost	k1gFnPc2	národnost
do	do	k7c2	do
jistých	jistý	k2eAgInPc2d1	jistý
obvodů	obvod	k1gInPc2	obvod
nebo	nebo	k8xC	nebo
jejich	jejich	k3xOp3gFnPc2	jejich
částí	část	k1gFnPc2	část
má	mít	k5eAaImIp3nS	mít
tento	tento	k3xDgInSc1	tento
stav	stav	k1gInSc1	stav
mimo	mimo	k7c4	mimo
kulturní	kulturní	k2eAgFnPc4d1	kulturní
pestrosti	pestrost	k1gFnPc4	pestrost
i	i	k8xC	i
negativní	negativní	k2eAgFnPc4d1	negativní
stránky	stránka	k1gFnPc4	stránka
-	-	kIx~	-
základní	základní	k2eAgFnPc4d1	základní
školy	škola	k1gFnPc4	škola
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
podíl	podíl	k1gInSc1	podíl
cizinců	cizinec	k1gMnPc2	cizinec
činí	činit	k5eAaImIp3nS	činit
i	i	k9	i
přes	přes	k7c4	přes
90	[number]	k4	90
procent	procento	k1gNnPc2	procento
žáků	žák	k1gMnPc2	žák
<g/>
,	,	kIx,	,
nejsou	být	k5eNaImIp3nP	být
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
čtvrtích	čtvrt	k1gFnPc6	čtvrt
Berlína	Berlín	k1gInSc2	Berlín
výjimkou	výjimka	k1gFnSc7	výjimka
<g/>
.	.	kIx.	.
</s>
<s>
Integrační	integrační	k2eAgInPc1d1	integrační
modely	model	k1gInPc1	model
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
a	a	k8xC	a
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
zpočátku	zpočátku	k6eAd1	zpočátku
nadějné	nadějný	k2eAgFnPc1d1	nadějná
<g/>
,	,	kIx,	,
ztroskotaly	ztroskotat	k5eAaPmAgFnP	ztroskotat
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
hovořit	hovořit	k5eAaImF	hovořit
o	o	k7c6	o
uzavřených	uzavřený	k2eAgNnPc6d1	uzavřené
ghettech	ghetto	k1gNnPc6	ghetto
různých	různý	k2eAgFnPc2d1	různá
menšin	menšina	k1gFnPc2	menšina
cizinců	cizinec	k1gMnPc2	cizinec
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
o	o	k7c4	o
arabské	arabský	k2eAgNnSc4d1	arabské
resp.	resp.	kA	resp.
palestinské	palestinský	k2eAgFnSc6d1	palestinská
přistěhovalce	přistěhovalka	k1gFnSc6	přistěhovalka
či	či	k8xC	či
nositele	nositel	k1gMnPc4	nositel
asylu	asyl	k1gInSc2	asyl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
i	i	k9	i
o	o	k7c4	o
jistou	jistý	k2eAgFnSc4d1	jistá
část	část	k1gFnSc4	část
tureckého	turecký	k2eAgNnSc2d1	turecké
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
žije	žít	k5eAaImIp3nS	žít
mnohdy	mnohdy	k6eAd1	mnohdy
již	již	k6eAd1	již
ve	v	k7c6	v
třetí	třetí	k4xOgFnSc6	třetí
generaci	generace	k1gFnSc6	generace
(	(	kIx(	(
<g/>
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
integrována	integrovat	k5eAaBmNgFnS	integrovat
méně	málo	k6eAd2	málo
než	než	k8xS	než
ona	onen	k3xDgFnSc1	onen
druhá	druhý	k4xOgFnSc1	druhý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
existuje	existovat	k5eAaImIp3nS	existovat
i	i	k9	i
velká	velký	k2eAgFnSc1d1	velká
židovská	židovský	k2eAgFnSc1d1	židovská
obec	obec	k1gFnSc1	obec
<g/>
,	,	kIx,	,
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
11000	[number]	k4	11000
členy	člen	k1gInPc7	člen
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
údajů	údaj	k1gInPc2	údaj
pak	pak	k6eAd1	pak
13000	[number]	k4	13000
<g/>
)	)	kIx)	)
největší	veliký	k2eAgFnSc1d3	veliký
židovská	židovský	k2eAgFnSc1d1	židovská
obec	obec	k1gFnSc1	obec
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tom	ten	k3xDgInSc6	ten
je	být	k5eAaImIp3nS	být
obsaženo	obsáhnout	k5eAaPmNgNnS	obsáhnout
ovšem	ovšem	k9	ovšem
přibližně	přibližně	k6eAd1	přibližně
8000	[number]	k4	8000
vystěhovalců	vystěhovalec	k1gMnPc2	vystěhovalec
z	z	k7c2	z
bývalého	bývalý	k2eAgInSc2d1	bývalý
SSSR	SSSR	kA	SSSR
počátkem	počátkem	k7c2	počátkem
devadesátých	devadesátý	k4xOgNnPc2	devadesátý
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
méně	málo	k6eAd2	málo
než	než	k8xS	než
jedna	jeden	k4xCgFnSc1	jeden
třetina	třetina	k1gFnSc1	třetina
obce	obec	k1gFnSc2	obec
pochází	pocházet	k5eAaImIp3nS	pocházet
ještě	ještě	k6eAd1	ještě
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
dřívějšího	dřívější	k2eAgInSc2d1	dřívější
Západního	západní	k2eAgInSc2d1	západní
Berlína	Berlín	k1gInSc2	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
holocaustem	holocaust	k1gInSc7	holocaust
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
žilo	žít	k5eAaImAgNnS	žít
asi	asi	k9	asi
170	[number]	k4	170
000	[number]	k4	000
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
asi	asi	k9	asi
90	[number]	k4	90
000	[number]	k4	000
emigrovalo	emigrovat	k5eAaBmAgNnS	emigrovat
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zbylých	zbylý	k2eAgNnPc2d1	zbylé
jich	on	k3xPp3gFnPc2	on
jen	jen	k9	jen
asi	asi	k9	asi
800	[number]	k4	800
přežilo	přežít	k5eAaPmAgNnS	přežít
holocaust	holocaust	k1gInSc4	holocaust
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Politické	politický	k2eAgNnSc1d1	politické
zřízení	zřízení	k1gNnSc1	zřízení
a	a	k8xC	a
volby	volba	k1gFnPc1	volba
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Spolková	spolkový	k2eAgFnSc1d1	spolková
země	země	k1gFnSc1	země
Berlín	Berlín	k1gInSc1	Berlín
je	být	k5eAaImIp3nS	být
vedle	vedle	k7c2	vedle
Hamburku	Hamburk	k1gInSc2	Hamburk
a	a	k8xC	a
Brém	Brémy	k1gFnPc2	Brémy
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
městských	městský	k2eAgInPc2d1	městský
států	stát	k1gInPc2	stát
Spolkové	spolkový	k2eAgFnSc2d1	spolková
republiky	republika	k1gFnSc2	republika
Německo	Německo	k1gNnSc1	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
města	město	k1gNnSc2	město
stojí	stát	k5eAaImIp3nS	stát
primátor	primátor	k1gMnSc1	primátor
(	(	kIx(	(
<g/>
regierender	regierender	k1gMnSc1	regierender
Bürgermeister	Bürgermeister	k1gMnSc1	Bürgermeister
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
volený	volený	k2eAgInSc1d1	volený
poslaneckou	poslanecký	k2eAgFnSc7d1	Poslanecká
sněmovnou	sněmovna	k1gFnSc7	sněmovna
(	(	kIx(	(
<g/>
Abgeordnetenhaus	Abgeordnetenhaus	k1gInSc1	Abgeordnetenhaus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spolkových	spolkový	k2eAgInPc6d1	spolkový
orgánech	orgán	k1gInPc6	orgán
Německa	Německo	k1gNnSc2	Německo
je	být	k5eAaImIp3nS	být
Berlín	Berlín	k1gInSc1	Berlín
zastoupen	zastoupen	k2eAgInSc1d1	zastoupen
23	[number]	k4	23
mandáty	mandát	k1gInPc4	mandát
ve	v	k7c6	v
spolkovém	spolkový	k2eAgInSc6d1	spolkový
sněmu	sněm	k1gInSc6	sněm
a	a	k8xC	a
4	[number]	k4	4
mandáty	mandát	k1gInPc4	mandát
ve	v	k7c6	v
spolkové	spolkový	k2eAgFnSc6d1	spolková
radě	rada	k1gFnSc6	rada
<g/>
.	.	kIx.	.
</s>
<s>
Strany	strana	k1gFnPc1	strana
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
kandidují	kandidovat	k5eAaImIp3nP	kandidovat
při	při	k7c6	při
volbách	volba	k1gFnPc6	volba
<g/>
,	,	kIx,	,
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
až	až	k9	až
na	na	k7c4	na
malé	malý	k2eAgFnPc4d1	malá
výjimky	výjimka	k1gFnPc4	výjimka
stranám	strana	k1gFnPc3	strana
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
vládne	vládnout	k5eAaImIp3nS	vládnout
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
tzv.	tzv.	kA	tzv.
velké	velký	k2eAgFnSc2d1	velká
koalice	koalice	k1gFnSc2	koalice
SPD	SPD	kA	SPD
a	a	k8xC	a
CDU	CDU	kA	CDU
<g/>
.	.	kIx.	.
</s>
<s>
Berlín	Berlín	k1gInSc1	Berlín
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
obvodů	obvod	k1gInPc2	obvod
(	(	kIx(	(
<g/>
Bezirk	Bezirk	k1gInSc1	Bezirk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
sjednocení	sjednocení	k1gNnSc6	sjednocení
města	město	k1gNnSc2	město
měl	mít	k5eAaImAgInS	mít
Berlín	Berlín	k1gInSc1	Berlín
23	[number]	k4	23
obvodů	obvod	k1gInPc2	obvod
<g/>
,	,	kIx,	,
zákonem	zákon	k1gInSc7	zákon
o	o	k7c6	o
územní	územní	k2eAgFnSc6d1	územní
reformě	reforma	k1gFnSc6	reforma
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
byl	být	k5eAaImAgInS	být
jejich	jejich	k3xOp3gInSc4	jejich
počet	počet	k1gInSc4	počet
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
lednu	leden	k1gInSc6	leden
2001	[number]	k4	2001
snížen	snížit	k5eAaPmNgMnS	snížit
na	na	k7c4	na
12	[number]	k4	12
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
sloučení	sloučení	k1gNnSc3	sloučení
bývalých	bývalý	k2eAgInPc2d1	bývalý
západních	západní	k2eAgInPc2d1	západní
a	a	k8xC	a
východních	východní	k2eAgInPc2d1	východní
obvodů	obvod	k1gInPc2	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
obvody	obvod	k1gInPc1	obvod
(	(	kIx(	(
<g/>
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Již	již	k6eAd1	již
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
se	se	k3xPyFc4	se
uvažuje	uvažovat	k5eAaImIp3nS	uvažovat
o	o	k7c4	o
sloučení	sloučení	k1gNnSc4	sloučení
Berlína	Berlín	k1gInSc2	Berlín
s	s	k7c7	s
Braniborskem	Braniborsko	k1gNnSc7	Braniborsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
referendu	referendum	k1gNnSc6	referendum
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
však	však	k9	však
zamítnuto	zamítnut	k2eAgNnSc1d1	zamítnuto
(	(	kIx(	(
<g/>
především	především	k9	především
voliči	volič	k1gMnPc1	volič
v	v	k7c6	v
Braniborsku	Braniborsko	k1gNnSc6	Braniborsko
mj.	mj.	kA	mj.
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
velké	velký	k2eAgFnSc3d1	velká
zadluženosti	zadluženost	k1gFnSc3	zadluženost
Berlína	Berlín	k1gInSc2	Berlín
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
nového	nový	k2eAgNnSc2d1	nové
referenda	referendum	k1gNnSc2	referendum
není	být	k5eNaImIp3nS	být
zatím	zatím	k6eAd1	zatím
udán	udán	k2eAgInSc1d1	udán
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Finance	finance	k1gFnSc2	finance
a	a	k8xC	a
hospodářství	hospodářství	k1gNnSc2	hospodářství
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Sjednocený	sjednocený	k2eAgInSc1d1	sjednocený
Berlín	Berlín	k1gInSc1	Berlín
se	se	k3xPyFc4	se
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
ohledech	ohled	k1gInPc6	ohled
stal	stát	k5eAaPmAgMnS	stát
symbolem	symbol	k1gInSc7	symbol
nové	nový	k2eAgFnSc2d1	nová
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
skončení	skončení	k1gNnSc6	skončení
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
pozitivní	pozitivní	k2eAgInPc1d1	pozitivní
aspekty	aspekt	k1gInPc1	aspekt
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
netýkají	týkat	k5eNaImIp3nP	týkat
současného	současný	k2eAgInSc2d1	současný
stavu	stav	k1gInSc2	stav
hospodářství	hospodářství	k1gNnSc2	hospodářství
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
a	a	k8xC	a
již	již	k9	již
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
případě	případ	k1gInSc6	případ
její	její	k3xOp3gFnSc1	její
finanční	finanční	k2eAgFnSc1d1	finanční
situace	situace	k1gFnSc1	situace
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
Berlín	Berlín	k1gInSc1	Berlín
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
obětí	oběť	k1gFnSc7	oběť
své	svůj	k3xOyFgFnSc2	svůj
polohy	poloha	k1gFnSc2	poloha
a	a	k8xC	a
trpí	trpět	k5eAaImIp3nS	trpět
silnými	silný	k2eAgInPc7d1	silný
vlivy	vliv	k1gInPc7	vliv
hospodářsky	hospodářsky	k6eAd1	hospodářsky
zaostalých	zaostalý	k2eAgMnPc2d1	zaostalý
tzv.	tzv.	kA	tzv.
nových	nový	k2eAgFnPc2d1	nová
spolkových	spolkový	k2eAgFnPc2d1	spolková
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
seznamech	seznam	k1gInPc6	seznam
podprůměrně	podprůměrně	k6eAd1	podprůměrně
vyvinutých	vyvinutý	k2eAgInPc2d1	vyvinutý
regionů	region	k1gInPc2	region
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgInPc1d1	důležitý
parametry	parametr	k1gInPc1	parametr
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
vývoje	vývoj	k1gInSc2	vývoj
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
vysoce	vysoce	k6eAd1	vysoce
neuspokojivé	uspokojivý	k2eNgFnPc1d1	neuspokojivá
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
se	se	k3xPyFc4	se
přidružuje	přidružovat	k5eAaImIp3nS	přidružovat
i	i	k9	i
extrémně	extrémně	k6eAd1	extrémně
negativní	negativní	k2eAgFnSc1d1	negativní
situace	situace	k1gFnSc1	situace
financí	finance	k1gFnPc2	finance
<g/>
,	,	kIx,	,
vyplývající	vyplývající	k2eAgMnSc1d1	vyplývající
nejen	nejen	k6eAd1	nejen
z	z	k7c2	z
vnějších	vnější	k2eAgInPc2d1	vnější
faktorů	faktor	k1gInPc2	faktor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
částečně	částečně	k6eAd1	částečně
zaviněná	zaviněný	k2eAgFnSc1d1	zaviněná
i	i	k8xC	i
neschopností	neschopnost	k1gFnSc7	neschopnost
a	a	k8xC	a
korupcí	korupce	k1gFnSc7	korupce
místních	místní	k2eAgMnPc2d1	místní
politiků	politik	k1gMnPc2	politik
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Berlín	Berlín	k1gInSc1	Berlín
je	být	k5eAaImIp3nS	být
dobře	dobře	k6eAd1	dobře
napojen	napojen	k2eAgMnSc1d1	napojen
na	na	k7c4	na
celoněmeckou	celoněmecký	k2eAgFnSc4d1	celoněmecká
i	i	k8xC	i
evropskou	evropský	k2eAgFnSc4d1	Evropská
síť	síť	k1gFnSc4	síť
dálnic	dálnice	k1gFnPc2	dálnice
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
Berlína	Berlín	k1gInSc2	Berlín
vede	vést	k5eAaImIp3nS	vést
vnější	vnější	k2eAgNnSc1d1	vnější
<g/>
,	,	kIx,	,
zcela	zcela	k6eAd1	zcela
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
dálniční	dálniční	k2eAgInSc1d1	dálniční
okruh	okruh	k1gInSc1	okruh
<g/>
;	;	kIx,	;
vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
okruh	okruh	k1gInSc1	okruh
byl	být	k5eAaImAgInS	být
plánován	plánovat	k5eAaImNgInS	plánovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
blízké	blízký	k2eAgFnSc6d1	blízká
budoucnosti	budoucnost	k1gFnSc6	budoucnost
zřejmě	zřejmě	k6eAd1	zřejmě
realizován	realizovat	k5eAaBmNgInS	realizovat
nebude	být	k5eNaImBp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
železniční	železniční	k2eAgNnSc1d1	železniční
spojení	spojení	k1gNnSc1	spojení
je	být	k5eAaImIp3nS	být
uspokojivé	uspokojivý	k2eAgNnSc1d1	uspokojivé
(	(	kIx(	(
<g/>
z	z	k7c2	z
<g/>
/	/	kIx~	/
<g/>
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
denně	denně	k6eAd1	denně
šest	šest	k4xCc4	šest
rychlíků	rychlík	k1gInPc2	rychlík
Eurocity	Eurocit	k1gInPc4	Eurocit
v	v	k7c4	v
2	[number]	k4	2
<g/>
-hodinových	odinový	k2eAgInPc6d1	-hodinový
intervalech	interval	k1gInPc6	interval
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
berlínské	berlínský	k2eAgNnSc1d1	berlínské
nádraží	nádraží	k1gNnSc1	nádraží
je	být	k5eAaImIp3nS	být
Berlin	berlina	k1gFnPc2	berlina
Hauptbahnhof	Hauptbahnhof	k1gMnSc1	Hauptbahnhof
(	(	kIx(	(
<g/>
hlavní	hlavní	k2eAgNnSc1d1	hlavní
nádraží	nádraží	k1gNnSc1	nádraží
<g/>
)	)	kIx)	)
otevřené	otevřený	k2eAgNnSc1d1	otevřené
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
bývalého	bývalý	k2eAgInSc2d1	bývalý
Lehrter	Lehrter	k1gMnSc1	Lehrter
Bahnhof	Bahnhof	k1gInSc1	Bahnhof
<g/>
.	.	kIx.	.
</s>
<s>
Berlin	berlina	k1gFnPc2	berlina
Hauptbahnhof	Hauptbahnhof	k1gInSc4	Hauptbahnhof
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
největší	veliký	k2eAgNnSc4d3	veliký
nádraží	nádraží	k1gNnSc4	nádraží
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
významná	významný	k2eAgNnPc1d1	významné
nádraží	nádraží	k1gNnPc1	nádraží
jsou	být	k5eAaImIp3nP	být
Ostbahnhof	Ostbahnhof	k1gInSc4	Ostbahnhof
<g/>
,	,	kIx,	,
Spandau	Spandaa	k1gFnSc4	Spandaa
<g/>
,	,	kIx,	,
Südkreuz	Südkreuz	k1gInSc4	Südkreuz
a	a	k8xC	a
Gesundbrunnen	Gesundbrunnen	k1gInSc4	Gesundbrunnen
<g/>
.	.	kIx.	.
</s>
<s>
Berlín	Berlín	k1gInSc1	Berlín
momentálně	momentálně	k6eAd1	momentálně
disponuje	disponovat	k5eAaBmIp3nS	disponovat
třemi	tři	k4xCgNnPc7	tři
letišti	letiště	k1gNnSc6	letiště
<g/>
:	:	kIx,	:
Tempelhof	Tempelhof	k1gMnSc1	Tempelhof
<g/>
,	,	kIx,	,
Tegel	Tegel	k1gMnSc1	Tegel
a	a	k8xC	a
Schönefeld	Schönefeld	k1gMnSc1	Schönefeld
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc7d1	ležící
uvnitř	uvnitř	k7c2	uvnitř
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
Tempelhof	Tempelhof	k1gInSc1	Tempelhof
<g/>
,	,	kIx,	,
fungovalo	fungovat	k5eAaImAgNnS	fungovat
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
také	také	k9	také
Tegel	Tegel	k1gMnSc1	Tegel
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
uzavřeno	uzavřít	k5eAaPmNgNnS	uzavřít
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
východoberlínského	východoberlínský	k2eAgNnSc2d1	východoberlínský
a	a	k8xC	a
po	po	k7c4	po
sjednocení	sjednocení	k1gNnSc4	sjednocení
méně	málo	k6eAd2	málo
využívaného	využívaný	k2eAgNnSc2d1	využívané
<g/>
)	)	kIx)	)
letiště	letiště	k1gNnSc2	letiště
Schönefeld	Schönefeldo	k1gNnPc2	Schönefeldo
<g/>
,	,	kIx,	,
ležícího	ležící	k2eAgMnSc2d1	ležící
na	na	k7c4	na
jihovýchod	jihovýchod	k1gInSc4	jihovýchod
od	od	k7c2	od
Berlína	Berlín	k1gInSc2	Berlín
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
rozšířeno	rozšířit	k5eAaPmNgNnS	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
Síť	síť	k1gFnSc1	síť
městské	městský	k2eAgFnSc2d1	městská
hromadné	hromadný	k2eAgFnSc2d1	hromadná
dopravy	doprava	k1gFnSc2	doprava
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejhustším	hustý	k2eAgFnPc3d3	nejhustší
mezi	mezi	k7c7	mezi
evropskými	evropský	k2eAgNnPc7d1	Evropské
městy	město	k1gNnPc7	město
<g/>
;	;	kIx,	;
po	po	k7c6	po
sloučení	sloučení	k1gNnSc6	sloučení
obou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
města	město	k1gNnSc2	město
byla	být	k5eAaImAgFnS	být
propojena	propojit	k5eAaPmNgFnS	propojit
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
kombinaci	kombinace	k1gFnSc4	kombinace
metra	metro	k1gNnSc2	metro
<g/>
,	,	kIx,	,
městské	městský	k2eAgFnSc2d1	městská
železnice	železnice	k1gFnSc2	železnice
<g/>
,	,	kIx,	,
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
a	a	k8xC	a
autobusové	autobusový	k2eAgFnSc2d1	autobusová
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
si	se	k3xPyFc3	se
získala	získat	k5eAaPmAgFnS	získat
velkou	velká	k1gFnSc4	velká
oblibu	obliba	k1gFnSc4	obliba
nejen	nejen	k6eAd1	nejen
mezi	mezi	k7c7	mezi
turisty	turist	k1gMnPc7	turist
i	i	k8xC	i
doprava	doprava	k6eAd1	doprava
něčím	něčí	k3xOyIgMnPc3	něčí
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
připomíná	připomínat	k5eAaImIp3nS	připomínat
rikšu	rikša	k1gFnSc4	rikša
-	-	kIx~	-
jízdní	jízdní	k2eAgNnPc4d1	jízdní
kola	kolo	k1gNnPc4	kolo
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
pojmou	pojmout	k5eAaPmIp3nP	pojmout
mimo	mimo	k7c4	mimo
řidiče	řidič	k1gMnPc4	řidič
ještě	ještě	k9	ještě
dvě	dva	k4xCgFnPc1	dva
další	další	k2eAgFnPc1d1	další
osoby	osoba	k1gFnPc1	osoba
<g/>
;	;	kIx,	;
jezdí	jezdit	k5eAaImIp3nP	jezdit
zejména	zejména	k9	zejména
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
(	(	kIx(	(
<g/>
mimo	mimo	k6eAd1	mimo
zimních	zimní	k2eAgInPc2d1	zimní
měsíců	měsíc	k1gInPc2	měsíc
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Berlín	Berlín	k1gInSc1	Berlín
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
velkým	velký	k2eAgInSc7d1	velký
počtem	počet	k1gInSc7	počet
kulturních	kulturní	k2eAgNnPc2d1	kulturní
a	a	k8xC	a
vědeckých	vědecký	k2eAgNnPc2d1	vědecké
zařízení	zařízení	k1gNnPc2	zařízení
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
některé	některý	k3yIgFnPc1	některý
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
velké	velký	k2eAgFnPc1d1	velká
známosti	známost	k1gFnPc1	známost
a	a	k8xC	a
popularity	popularita	k1gFnPc1	popularita
i	i	k9	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Kulturní	kulturní	k2eAgFnSc1d1	kulturní
pestrost	pestrost	k1gFnSc1	pestrost
a	a	k8xC	a
tolerance	tolerance	k1gFnSc1	tolerance
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
traduje	tradovat	k5eAaImIp3nS	tradovat
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
Západního	západní	k2eAgInSc2d1	západní
Berlína	Berlín	k1gInSc2	Berlín
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
"	"	kIx"	"
<g/>
svobodným	svobodný	k2eAgMnSc7d1	svobodný
<g/>
"	"	kIx"	"
městem	město	k1gNnSc7	město
podle	podle	k7c2	podle
hesla	heslo	k1gNnSc2	heslo
"	"	kIx"	"
<g/>
pro	pro	k7c4	pro
každého	každý	k3xTgMnSc4	každý
něco	něco	k3yInSc1	něco
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
situace	situace	k1gFnSc1	situace
a	a	k8xC	a
výhledy	výhled	k1gInPc1	výhled
do	do	k7c2	do
budoucnosti	budoucnost	k1gFnSc2	budoucnost
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
silně	silně	k6eAd1	silně
ovlivněny	ovlivněn	k2eAgInPc1d1	ovlivněn
finanční	finanční	k2eAgFnSc7d1	finanční
krizí	krize	k1gFnSc7	krize
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
diskuse	diskuse	k1gFnPc1	diskuse
o	o	k7c4	o
zavření	zavření	k1gNnSc4	zavření
nebo	nebo	k8xC	nebo
sloučení	sloučení	k1gNnSc4	sloučení
oper	opera	k1gFnPc2	opera
<g/>
,	,	kIx,	,
o	o	k7c6	o
subvencích	subvence	k1gFnPc6	subvence
pro	pro	k7c4	pro
kulturní	kulturní	k2eAgFnSc4d1	kulturní
a	a	k8xC	a
vědecká	vědecký	k2eAgNnPc1d1	vědecké
zařízení	zařízení	k1gNnPc1	zařízení
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
kulturních	kulturní	k2eAgInPc2d1	kulturní
<g/>
,	,	kIx,	,
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
a	a	k8xC	a
vědeckých	vědecký	k2eAgNnPc2d1	vědecké
zařízení	zařízení	k1gNnPc2	zařízení
a	a	k8xC	a
událostí	událost	k1gFnPc2	událost
lze	lze	k6eAd1	lze
jmenovat	jmenovat	k5eAaImF	jmenovat
jen	jen	k9	jen
výběr	výběr	k1gInSc4	výběr
<g/>
:	:	kIx,	:
vedle	vedle	k7c2	vedle
nesčetných	sčetný	k2eNgNnPc2d1	nesčetné
divadel	divadlo	k1gNnPc2	divadlo
<g/>
,	,	kIx,	,
malých	malý	k2eAgFnPc2d1	malá
forem	forma	k1gFnPc2	forma
a	a	k8xC	a
kabaretů	kabaret	k1gInPc2	kabaret
celkem	celkem	k6eAd1	celkem
tři	tři	k4xCgFnPc1	tři
opery	opera	k1gFnPc1	opera
<g/>
:	:	kIx,	:
Státní	státní	k2eAgFnSc1d1	státní
opera	opera	k1gFnSc1	opera
<g/>
,	,	kIx,	,
Německá	německý	k2eAgFnSc1d1	německá
opera	opera	k1gFnSc1	opera
a	a	k8xC	a
Komická	komický	k2eAgFnSc1d1	komická
opera	opera	k1gFnSc1	opera
(	(	kIx(	(
<g/>
Staatsoper	Staatsoper	k1gInSc1	Staatsoper
<g/>
,	,	kIx,	,
Deutsche	Deutsche	k1gNnSc1	Deutsche
Oper	opera	k1gFnPc2	opera
<g/>
,	,	kIx,	,
Komische	Komische	k1gNnSc1	Komische
Oper	opera	k1gFnPc2	opera
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Friedrichstadt-Palast	Friedrichstadt-Palast	k1gInSc1	Friedrichstadt-Palast
<g/>
,	,	kIx,	,
divadlo	divadlo	k1gNnSc1	divadlo
s	s	k7c7	s
1895	[number]	k4	1895
místy	místo	k1gNnPc7	místo
a	a	k8xC	a
s	s	k7c7	s
největším	veliký	k2eAgNnSc7d3	veliký
jevištěm	jeviště	k1gNnSc7	jeviště
na	na	k7c6	na
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
2854	[number]	k4	2854
m	m	kA	m
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
obchodní	obchodní	k2eAgInSc4d1	obchodní
dům	dům	k1gInSc4	dům
Kaufhaus	Kaufhaus	k1gInSc1	Kaufhaus
des	des	k1gNnSc1	des
Westens	Westens	k1gInSc1	Westens
(	(	kIx(	(
<g/>
KaDeWe	KaDeWe	k1gNnSc1	KaDeWe
<g/>
)	)	kIx)	)
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
muzeí	muzeum	k1gNnPc2	muzeum
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
nejznámější	známý	k2eAgMnPc4d3	nejznámější
jsou	být	k5eAaImIp3nP	být
Pergamonské	Pergamonský	k2eAgNnSc1d1	Pergamonské
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
muzeum	muzeum	k1gNnSc1	muzeum
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
s	s	k7c7	s
originálem	originál	k1gInSc7	originál
sošky	soška	k1gFnSc2	soška
Nefertiti	Nefertiti	k1gFnSc2	Nefertiti
<g/>
)	)	kIx)	)
velice	velice	k6eAd1	velice
známá	známý	k2eAgFnSc1d1	známá
botanická	botanický	k2eAgFnSc1d1	botanická
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
dvě	dva	k4xCgFnPc4	dva
zoologické	zoologický	k2eAgFnPc4d1	zoologická
zahrady	zahrada	k1gFnPc4	zahrada
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
s	s	k7c7	s
velice	velice	k6eAd1	velice
známým	známý	k2eAgNnSc7d1	známé
akváriem	akvárium	k1gNnSc7	akvárium
Stará	starat	k5eAaImIp3nS	starat
a	a	k8xC	a
Nová	Nová	k1gFnSc1	Nová
Národní	národní	k2eAgFnSc2d1	národní
galerie	galerie	k1gFnSc2	galerie
Berlínský	berlínský	k2eAgInSc1d1	berlínský
filmový	filmový	k2eAgInSc1d1	filmový
festival	festival	k1gInSc1	festival
se	se	k3xPyFc4	se
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
každoročně	každoročně	k6eAd1	každoročně
pořádá	pořádat	k5eAaImIp3nS	pořádat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
a	a	k8xC	a
jako	jako	k9	jako
hlavní	hlavní	k2eAgFnSc4d1	hlavní
cenu	cena	k1gFnSc4	cena
zde	zde	k6eAd1	zde
porota	porota	k1gFnSc1	porota
uděluje	udělovat	k5eAaImIp3nS	udělovat
"	"	kIx"	"
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Zlatého	zlatý	k2eAgMnSc4d1	zlatý
medvěda	medvěd	k1gMnSc4	medvěd
<g/>
"	"	kIx"	"
Christopher	Christophra	k1gFnPc2	Christophra
street	street	k1gInSc1	street
day	day	k?	day
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
pochodů	pochod	k1gInPc2	pochod
za	za	k7c4	za
práva	právo	k1gNnPc4	právo
homosexuálů	homosexuál	k1gMnPc2	homosexuál
Karneval	karneval	k1gInSc1	karneval
kultur	kultura	k1gFnPc2	kultura
(	(	kIx(	(
<g/>
Karneval	karneval	k1gInSc1	karneval
der	drát	k5eAaImRp2nS	drát
Kulturen	Kulturno	k1gNnPc2	Kulturno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
velký	velký	k2eAgInSc1d1	velký
průvod	průvod	k1gInSc1	průvod
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
o	o	k7c6	o
letnicích	letnice	k1gFnPc6	letnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
během	během	k7c2	během
asi	asi	k9	asi
tří	tři	k4xCgInPc2	tři
dnů	den	k1gInPc2	den
prezentuje	prezentovat	k5eAaBmIp3nS	prezentovat
většina	většina	k1gFnSc1	většina
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
žijících	žijící	k2eAgFnPc2d1	žijící
národností	národnost	k1gFnPc2	národnost
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
2004	[number]	k4	2004
<g/>
:	:	kIx,	:
asi	asi	k9	asi
1,5	[number]	k4	1,5
milionu	milion	k4xCgInSc2	milion
účinkujících	účinkující	k1gFnPc2	účinkující
a	a	k8xC	a
návštěvníků	návštěvník	k1gMnPc2	návštěvník
<g/>
)	)	kIx)	)
Berlínská	berlínský	k2eAgFnSc1d1	Berlínská
filharmonie	filharmonie	k1gFnSc1	filharmonie
(	(	kIx(	(
<g/>
mezi	mezi	k7c4	mezi
dirigenty	dirigent	k1gMnPc4	dirigent
patřil	patřit	k5eAaImAgInS	patřit
Herbert	Herbert	k1gInSc1	Herbert
von	von	k1gInSc1	von
Karajan	Karajan	k1gInSc4	Karajan
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
jejím	její	k3xOp3gMnSc7	její
šéfdirigentem	šéfdirigent	k1gMnSc7	šéfdirigent
Sir	sir	k1gMnSc1	sir
Simon	Simon	k1gMnSc1	Simon
D.	D.	kA	D.
Rattle	Rattle	k1gFnPc1	Rattle
<g/>
,	,	kIx,	,
partner	partner	k1gMnSc1	partner
Magdaleny	Magdalena	k1gFnSc2	Magdalena
Kožené	kožený	k2eAgFnSc2d1	kožená
<g/>
)	)	kIx)	)
tři	tři	k4xCgFnPc1	tři
univerzity	univerzita	k1gFnPc1	univerzita
<g/>
:	:	kIx,	:
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
Humboldtova	Humboldtův	k2eAgFnSc1d1	Humboldtova
univerzita	univerzita	k1gFnSc1	univerzita
a	a	k8xC	a
Technická	technický	k2eAgFnSc1d1	technická
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
to	ten	k3xDgNnSc4	ten
další	další	k2eAgFnSc1d1	další
vědecká	vědecký	k2eAgFnSc1d1	vědecká
a	a	k8xC	a
výzkumná	výzkumný	k2eAgNnPc1d1	výzkumné
zařízení	zařízení	k1gNnPc1	zařízení
a	a	k8xC	a
instituce	instituce	k1gFnPc1	instituce
k	k	k7c3	k
dalším	další	k2eAgFnPc3d1	další
celosvětově	celosvětově	k6eAd1	celosvětově
známým	známý	k2eAgFnPc3d1	známá
událostem	událost	k1gFnPc3	událost
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
každoroční	každoroční	k2eAgInSc4d1	každoroční
maratonský	maratonský	k2eAgInSc4d1	maratonský
běh	běh	k1gInSc4	běh
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgFnPc2d1	další
pozoruhodností	pozoruhodnost	k1gFnPc2	pozoruhodnost
a	a	k8xC	a
památek	památka	k1gFnPc2	památka
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
jmenovat	jmenovat	k5eAaBmF	jmenovat
Říšský	říšský	k2eAgInSc1d1	říšský
sněm	sněm	k1gInSc1	sněm
(	(	kIx(	(
<g/>
Reichstag	Reichstag	k1gInSc1	Reichstag
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Braniborskou	braniborský	k2eAgFnSc4d1	Braniborská
bránu	brána	k1gFnSc4	brána
<g/>
,	,	kIx,	,
světoznámou	světoznámý	k2eAgFnSc4d1	světoznámá
třídu	třída	k1gFnSc4	třída
Unter	Untra	k1gFnPc2	Untra
den	den	k1gInSc4	den
Linden	Lindno	k1gNnPc2	Lindno
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
jako	jako	k9	jako
Karl-Marx-Allee	Karl-Marx-Allee	k1gFnSc1	Karl-Marx-Allee
<g/>
,	,	kIx,	,
zámek	zámek	k1gInSc1	zámek
Schloss	Schloss	k1gInSc1	Schloss
Charlottenburg	Charlottenburg	k1gInSc4	Charlottenburg
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgInSc4d1	bývalý
přechod	přechod	k1gInSc4	přechod
Checkpoint	Checkpoint	k1gMnSc1	Checkpoint
Charlie	Charlie	k1gMnSc1	Charlie
<g/>
,	,	kIx,	,
East	East	k2eAgInSc1d1	East
Side	Side	k1gInSc1	Side
Gallery	Galler	k1gInPc1	Galler
(	(	kIx(	(
<g/>
zbytek	zbytek	k1gInSc1	zbytek
Berlínské	berlínský	k2eAgFnSc2d1	Berlínská
<g />
.	.	kIx.	.
</s>
<s>
zdi	zeď	k1gFnPc1	zeď
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgFnSc1d1	televizní
věž	věž	k1gFnSc1	věž
Fernsehturm	Fernsehturm	k1gInSc1	Fernsehturm
<g/>
,	,	kIx,	,
chrám	chrám	k1gInSc1	chrám
Berliner	Berliner	k1gInSc1	Berliner
Dom	Dom	k?	Dom
a	a	k8xC	a
zříceninu	zřícenina	k1gFnSc4	zřícenina
kostela	kostel	k1gInSc2	kostel
Kaiser-Wilhelm-Gedächtniskirche	Kaiser-Wilhelm-Gedächtniskirche	k1gFnPc2	Kaiser-Wilhelm-Gedächtniskirche
<g/>
,	,	kIx,	,
Kurfürstendamm	Kurfürstendamma	k1gFnPc2	Kurfürstendamma
a	a	k8xC	a
Alexanderplatz	Alexanderplatza	k1gFnPc2	Alexanderplatza
(	(	kIx(	(
<g/>
bývalé	bývalý	k2eAgFnSc2d1	bývalá
"	"	kIx"	"
<g/>
západní	západní	k2eAgFnSc2d1	západní
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
východní	východní	k2eAgFnSc3d1	východní
<g/>
"	"	kIx"	"
centrum	centrum	k1gNnSc1	centrum
města	město	k1gNnSc2	město
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
celý	celý	k2eAgInSc4d1	celý
nový	nový	k2eAgInSc4d1	nový
komplex	komplex	k1gInSc4	komplex
na	na	k7c6	na
Postupimském	postupimský	k2eAgNnSc6d1	Postupimské
náměstí	náměstí	k1gNnSc6	náměstí
(	(	kIx(	(
<g/>
Potsdamer	Potsdamer	k1gMnSc1	Potsdamer
Platz	Platz	k1gMnSc1	Platz
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
nejbližším	blízký	k2eAgNnSc6d3	nejbližší
okolí	okolí	k1gNnSc6	okolí
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
v	v	k7c6	v
devadesátých	devadesátý	k4xOgNnPc6	devadesátý
letech	léto	k1gNnPc6	léto
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
více	hodně	k6eAd2	hodně
méně	málo	k6eAd2	málo
celá	celý	k2eAgFnSc1d1	celá
nová	nový	k2eAgFnSc1d1	nová
čtvrt	čtvrt	k1gFnSc1	čtvrt
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
pak	pak	k6eAd1	pak
i	i	k8xC	i
památky	památka	k1gFnPc1	památka
v	v	k7c6	v
blízké	blízký	k2eAgFnSc6d1	blízká
Postupimi	Postupim	k1gFnSc6	Postupim
<g/>
.	.	kIx.	.
</s>
<s>
Berlín	Berlín	k1gInSc1	Berlín
se	se	k3xPyFc4	se
chlubí	chlubit	k5eAaImIp3nS	chlubit
i	i	k9	i
rozsáhlými	rozsáhlý	k2eAgFnPc7d1	rozsáhlá
zelenými	zelený	k2eAgFnPc7d1	zelená
plochami	plocha	k1gFnPc7	plocha
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejznámějším	známý	k2eAgInPc3d3	nejznámější
patří	patřit	k5eAaImIp3nP	patřit
např.	např.	kA	např.
Tiergarten	Tiergarten	k2eAgInSc4d1	Tiergarten
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgInSc4d1	ležící
poměrně	poměrně	k6eAd1	poměrně
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
oblíbený	oblíbený	k2eAgInSc4d1	oblíbený
pro	pro	k7c4	pro
procházky	procházka	k1gFnPc4	procházka
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc4d1	známý
jako	jako	k9	jako
místo	místo	k7c2	místo
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
pořádá	pořádat	k5eAaImIp3nS	pořádat
Love	lov	k1gInSc5	lov
Parade	Parad	k1gInSc5	Parad
a	a	k8xC	a
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
na	na	k7c4	na
víkend	víkend	k1gInSc4	víkend
i	i	k8xC	i
opékat	opékat	k5eAaImF	opékat
selata	sele	k1gNnPc4	sele
a	a	k8xC	a
zejména	zejména	k9	zejména
-	-	kIx~	-
jak	jak	k8xS	jak
to	ten	k3xDgNnSc1	ten
činí	činit	k5eAaImIp3nP	činit
turečtí	turecký	k2eAgMnPc1d1	turecký
obyvatelé	obyvatel	k1gMnPc1	obyvatel
-	-	kIx~	-
jehňata	jehně	k1gNnPc4	jehně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
své	svůj	k3xOyFgFnSc2	svůj
minulosti	minulost	k1gFnSc2	minulost
má	mít	k5eAaImIp3nS	mít
Berlín	Berlín	k1gInSc1	Berlín
i	i	k8xC	i
mnoho	mnoho	k4c1	mnoho
památek	památka	k1gFnPc2	památka
<g/>
,	,	kIx,	,
souvisejících	související	k2eAgInPc2d1	související
s	s	k7c7	s
berlínskou	berlínský	k2eAgFnSc7d1	Berlínská
židovskou	židovský	k2eAgFnSc7d1	židovská
obcí	obec	k1gFnSc7	obec
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
těm	ten	k3xDgMnPc3	ten
nejznámějším	známý	k2eAgMnPc3d3	nejznámější
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Nová	nový	k2eAgFnSc1d1	nová
synagoga	synagoga	k1gFnSc1	synagoga
Židovské	židovský	k2eAgNnSc4d1	Židovské
muzeum	muzeum	k1gNnSc4	muzeum
Židovský	židovský	k2eAgInSc1d1	židovský
památník	památník	k1gInSc1	památník
Níže	nízce	k6eAd2	nízce
uvedená	uvedený	k2eAgFnSc1d1	uvedená
partnerství	partnerství	k1gNnSc2	partnerství
uzavřelo	uzavřít	k5eAaPmAgNnS	uzavřít
město	město	k1gNnSc1	město
Berlín	Berlín	k1gInSc1	Berlín
jako	jako	k8xC	jako
celek	celek	k1gInSc1	celek
(	(	kIx(	(
<g/>
stav	stav	k1gInSc1	stav
k	k	k7c3	k
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc3	prosinec
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
obvody	obvod	k1gInPc1	obvod
mají	mít	k5eAaImIp3nP	mít
další	další	k2eAgNnPc4d1	další
partnerská	partnerský	k2eAgNnPc4d1	partnerské
města	město	k1gNnPc4	město
<g/>
.	.	kIx.	.
</s>
<s>
PEŠEK	Pešek	k1gMnSc1	Pešek
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
aglomerace	aglomerace	k1gFnSc2	aglomerace
k	k	k7c3	k
velkoměstu	velkoměsto	k1gNnSc3	velkoměsto
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
a	a	k8xC	a
středoevropské	středoevropský	k2eAgFnPc1d1	středoevropská
metropole	metropol	k1gFnPc1	metropol
1850	[number]	k4	1850
<g/>
-	-	kIx~	-
<g/>
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Scriptorium	Scriptorium	k1gNnSc1	Scriptorium
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
319	[number]	k4	319
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86197	[number]	k4	86197
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Berlín	Berlín	k1gInSc1	Berlín
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Téma	téma	k1gFnSc1	téma
Berlín	Berlín	k1gInSc1	Berlín
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Slovníkové	slovníkový	k2eAgFnSc2d1	slovníková
heslo	heslo	k1gNnSc4	heslo
Berlín	Berlín	k1gInSc1	Berlín
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Oficiální	oficiální	k2eAgInSc4d1	oficiální
web	web	k1gInSc4	web
Berlína	Berlín	k1gInSc2	Berlín
Plán	plán	k1gInSc1	plán
Berlína	Berlín	k1gInSc2	Berlín
Podrobný	podrobný	k2eAgMnSc1d1	podrobný
průvodce	průvodce	k1gMnSc1	průvodce
Berlínem	Berlín	k1gInSc7	Berlín
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
</s>
