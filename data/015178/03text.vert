<s>
Pivovarské	pivovarský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
U	u	k7c2
Fleků	flek	k1gInPc2
</s>
<s>
Pivovarské	pivovarský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
U	u	k7c2
Fleků	flek	k1gInPc2
Pivovarské	pivovarský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
U	u	k7c2
Fleků	flek	k1gInPc2
Údaje	údaj	k1gInSc2
o	o	k7c6
muzeu	muzeum	k1gNnSc6
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Město	město	k1gNnSc1
</s>
<s>
Praha	Praha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Křemencova	Křemencův	k2eAgFnSc1d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2	#num#	k4
<g/>
-Nové	-Nová	k1gFnSc6
Město	město	k1gNnSc4
Původní	původní	k2eAgInSc1d1
účel	účel	k1gInSc1
budovy	budova	k1gFnSc2
</s>
<s>
pivovar	pivovar	k1gInSc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
4	#num#	k4
<g/>
′	′	k?
<g/>
44	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
25	#num#	k4
<g/>
′	′	k?
<g/>
0	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Webové	webový	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Pivovarské	pivovarský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
U	u	k7c2
Fleků	flek	k1gInPc2
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
Pivovaru	pivovar	k1gInSc2
u	u	k7c2
Fleků	flek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
jeden	jeden	k4xCgInSc1
z	z	k7c2
nejznámějších	známý	k2eAgInPc2d3
a	a	k8xC
nejstarších	starý	k2eAgInPc2d3
českých	český	k2eAgInPc2d1
pivovarů	pivovar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
Praze	Praha	k1gFnSc6
v	v	k7c6
městské	městský	k2eAgFnSc6d1
části	část	k1gFnSc6
Praha	Praha	k1gFnSc1
1	#num#	k4
na	na	k7c6
Novém	nový	k2eAgNnSc6d1
Městě	město	k1gNnSc6
v	v	k7c6
Křemencové	křemencový	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
pivovaru	pivovar	k1gInSc2
nabízí	nabízet	k5eAaImIp3nS
podnik	podnik	k1gInSc1
také	také	k9
restauraci	restaurace	k1gFnSc4
a	a	k8xC
Kabaret	kabaret	k1gInSc4
U	u	k7c2
Fleků	flek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
muzea	muzeum	k1gNnSc2
</s>
<s>
Muzeum	muzeum	k1gNnSc1
U	u	k7c2
Fleků	flek	k1gInPc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
areálu	areál	k1gInSc6
pivovaru	pivovar	k1gInSc2
a	a	k8xC
restaurace	restaurace	k1gFnSc2
U	u	k7c2
Fleků	flek	k1gInPc2
v	v	k7c6
budovách	budova	k1gFnPc6
bývalých	bývalý	k2eAgFnPc2d1
sladoven	sladovna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slad	slad	k1gInSc1
se	se	k3xPyFc4
zde	zde	k6eAd1
vyráběl	vyrábět	k5eAaImAgMnS
do	do	k7c2
roku	rok	k1gInSc2
1942	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
domě	dům	k1gInSc6
s	s	k7c7
pivovarem	pivovar	k1gInSc7
je	být	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1499	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
dům	dům	k1gInSc4
koupil	koupit	k5eAaPmAgMnS
sladovník	sladovník	k1gMnSc1
Vít	Vít	k1gMnSc1
Skřemenec	Skřemenec	k1gMnSc1
s	s	k7c7
manželkou	manželka	k1gFnSc7
a	a	k8xC
začal	začít	k5eAaPmAgInS
v	v	k7c6
něm	on	k3xPp3gNnSc6
provozovat	provozovat	k5eAaImF
sladovnické	sladovnický	k2eAgNnSc4d1
řemeslo	řemeslo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnešní	dnešní	k2eAgFnSc1d1
rodina	rodina	k1gFnSc1
Brtníkových	brtníkův	k2eAgMnPc2d1
koupila	koupit	k5eAaPmAgFnS
pivovar	pivovar	k1gInSc4
v	v	k7c6
roce	rok	k1gInSc6
1921	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pivovar	pivovar	k1gInSc1
<g/>
,	,	kIx,
sladovnu	sladovna	k1gFnSc4
a	a	k8xC
restauraci	restaurace	k1gFnSc4
provozovala	provozovat	k5eAaImAgFnS
do	do	k7c2
roku	rok	k1gInSc2
1949	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jim	on	k3xPp3gMnPc3
byly	být	k5eAaImAgInP
znárodněny	znárodnit	k5eAaPmNgInP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
poté	poté	k6eAd1
součástí	součást	k1gFnSc7
Pražských	pražský	k2eAgInPc2d1
pivovarů	pivovar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
byla	být	k5eAaImAgFnS
rodině	rodina	k1gFnSc3
Brtníků	brtník	k1gMnPc2
vrácena	vrácen	k2eAgFnSc1d1
restaurace	restaurace	k1gFnSc1
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
také	také	k9
pivovar	pivovar	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
areálu	areál	k1gInSc6
pivovaru	pivovar	k1gInSc2
je	být	k5eAaImIp3nS
pivovarnické	pivovarnický	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
rodina	rodina	k1gFnSc1
Brtníkových	brtníkův	k2eAgMnPc2d1
založila	založit	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Sbírkových	sbírkový	k2eAgInPc2d1
předmětů	předmět	k1gInPc2
je	být	k5eAaImIp3nS
v	v	k7c6
něm	on	k3xPp3gMnSc6
v	v	k7c6
současnosti	současnost	k1gFnSc6
několik	několik	k4yIc4
set	set	k1gInSc4
a	a	k8xC
řada	řada	k1gFnSc1
z	z	k7c2
nich	on	k3xPp3gMnPc2
je	být	k5eAaImIp3nS
unikátních	unikátní	k2eAgFnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
díky	díky	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
předměty	předmět	k1gInPc1
nacházející	nacházející	k2eAgInPc1d1
se	se	k3xPyFc4
v	v	k7c6
muzeu	muzeum	k1gNnSc6
nejsou	být	k5eNaImIp3nP
pouze	pouze	k6eAd1
předměty	předmět	k1gInPc1
z	z	k7c2
pivovaru	pivovar	k1gInSc2
u	u	k7c2
Fleků	flek	k1gInPc2
<g/>
,	,	kIx,
ale	ale	k8xC
pocházejí	pocházet	k5eAaImIp3nP
i	i	k9
z	z	k7c2
řady	řada	k1gFnSc2
pivovarů	pivovar	k1gInPc2
v	v	k7c6
Čechách	Čechy	k1gFnPc6
a	a	k8xC
na	na	k7c6
Moravě	Morava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Součástí	součást	k1gFnSc7
sbírky	sbírka	k1gFnSc2
jsou	být	k5eAaImIp3nP
také	také	k9
originály	originál	k1gInPc1
nebo	nebo	k8xC
kopie	kopie	k1gFnPc1
dobových	dobový	k2eAgFnPc2d1
fotografií	fotografia	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Muzeum	muzeum	k1gNnSc1
lze	lze	k6eAd1
navštívit	navštívit	k5eAaPmF
denně	denně	k6eAd1
v	v	k7c6
době	doba	k1gFnSc6
od	od	k7c2
10.00	10.00	k4
do	do	k7c2
16.00	16.00	k4
po	po	k7c6
předchozím	předchozí	k2eAgNnSc6d1
objednání	objednání	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Exkurze	exkurze	k1gFnPc1
jsou	být	k5eAaImIp3nP
nabízeny	nabízet	k5eAaImNgFnP
s	s	k7c7
průvodci	průvodce	k1gMnPc7
v	v	k7c6
několika	několik	k4yIc6
světových	světový	k2eAgInPc6d1
jazycích	jazyk	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
prohlídky	prohlídka	k1gFnSc2
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
fotografovat	fotografovat	k5eAaImF
nebo	nebo	k8xC
si	se	k3xPyFc3
dělat	dělat	k5eAaImF
filmový	filmový	k2eAgInSc4d1
nebo	nebo	k8xC
videozáznam	videozáznam	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgFnSc1
část	část	k1gFnSc1
exkurze	exkurze	k1gFnSc2
probíhá	probíhat	k5eAaImIp3nS
v	v	k7c6
bývalé	bývalý	k2eAgFnSc6d1
sladovně	sladovna	k1gFnSc6
ze	z	k7c2
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
druhá	druhý	k4xOgFnSc1
ve	v	k7c6
sladovně	sladovna	k1gFnSc6
z	z	k7c2
roku	rok	k1gInSc2
1889	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Prohlídková	prohlídkový	k2eAgFnSc1d1
trasa	trasa	k1gFnSc1
a	a	k8xC
exponáty	exponát	k1gInPc1
</s>
<s>
První	první	k4xOgFnSc1
sladovna	sladovna	k1gFnSc1
–	–	k?
začátek	začátek	k1gInSc4
návštěvnického	návštěvnický	k2eAgInSc2d1
okruhu	okruh	k1gInSc2
</s>
<s>
Prohlídka	prohlídka	k1gFnSc1
začíná	začínat	k5eAaImIp3nS
ve	v	k7c6
sladovně	sladovna	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Budova	budova	k1gFnSc1
má	mít	k5eAaImIp3nS
dvě	dva	k4xCgFnPc4
části	část	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Starší	starší	k1gMnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
pozadí	pozadí	k1gNnSc6
byla	být	k5eAaImAgFnS
vybudována	vybudovat	k5eAaPmNgFnS
na	na	k7c6
počátku	počátek	k1gInSc6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
a	a	k8xC
cenná	cenný	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
hlavní	hlavní	k2eAgFnSc1d1
místnost	místnost	k1gFnSc1
má	mít	k5eAaImIp3nS
krásný	krásný	k2eAgInSc4d1
renesanční	renesanční	k2eAgInSc4d1
valený	valený	k2eAgInSc4d1
strop	strop	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největší	veliký	k2eAgFnSc7d3
atrakcí	atrakce	k1gFnSc7
je	být	k5eAaImIp3nS
stříškový	stříškový	k2eAgInSc1d1
kouřový	kouřový	k2eAgInSc1d1
hvozd	hvozd	k1gInSc1
zvaný	zvaný	k2eAgInSc1d1
valach	valach	k1gInSc4
<g/>
,	,	kIx,
protože	protože	k8xS
připomíná	připomínat	k5eAaImIp3nS
hřbet	hřbet	k1gInSc4
koně	kůň	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
hvozdu	hvozd	k1gInSc6
se	se	k3xPyFc4
vykuřoval	vykuřovat	k5eAaImAgInS
(	(	kIx(
<g/>
jistý	jistý	k2eAgInSc1d1
způsob	způsob	k1gInSc1
sušení	sušení	k1gNnSc2
<g/>
)	)	kIx)
slad	slad	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
stříšce	stříška	k1gFnSc6
konstrukce	konstrukce	k1gFnSc2
valachu	valach	k1gInSc2
se	se	k3xPyFc4
navršil	navršit	k5eAaPmAgMnS
slad	slad	k1gInSc4
do	do	k7c2
maximální	maximální	k2eAgFnSc2d1
výše	výše	k1gFnSc2,k1gFnSc2wB
10	#num#	k4
cm	cm	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
pomocí	pomoc	k1gFnPc2
kouře	kouř	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
stoupal	stoupat	k5eAaImAgInS
ze	z	k7c2
sklepních	sklepní	k2eAgFnPc2d1
prostor	prostora	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
bylo	být	k5eAaImAgNnS
topeniště	topeniště	k1gNnSc1
<g/>
,	,	kIx,
se	se	k3xPyFc4
vyráběl	vyrábět	k5eAaImAgInS
tmavý	tmavý	k2eAgInSc1d1
slad	slad	k1gInSc1
<g/>
,	,	kIx,
kterému	který	k3yRgInSc3,k3yQgInSc3,k3yIgInSc3
říkají	říkat	k5eAaImIp3nP
barva	barva	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
slad	slad	k1gInSc4
byl	být	k5eAaImAgMnS
jedním	jeden	k4xCgMnSc7
ze	z	k7c2
čtyř	čtyři	k4xCgInPc2
druhů	druh	k1gInPc2
sladu	slad	k1gInSc2
<g/>
,	,	kIx,
ze	z	k7c2
kterých	který	k3yRgFnPc2,k3yIgFnPc2,k3yQgFnPc2
se	se	k3xPyFc4
dodnes	dodnes	k6eAd1
vaří	vařit	k5eAaImIp3nP
flekovské	flekovský	k2eAgNnSc4d1
pivo	pivo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
zhlédnutí	zhlédnutí	k1gNnSc3
je	být	k5eAaImIp3nS
i	i	k9
samotné	samotný	k2eAgNnSc1d1
topeniště	topeniště	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
zajímavé	zajímavý	k2eAgNnSc1d1
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
tvarem	tvar	k1gInSc7
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
menší	malý	k2eAgFnSc4d2
podlouhlou	podlouhlý	k2eAgFnSc4d1
chodbičku	chodbička	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tam	tam	k6eAd1
se	se	k3xPyFc4
vkládaly	vkládat	k5eAaImAgFnP
bukové	bukový	k2eAgFnPc1d1
klády	kláda	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kouř	kouř	k1gInSc1
způsobil	způsobit	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
strop	strop	k1gInSc1
je	být	k5eAaImIp3nS
od	od	k7c2
sazí	saze	k1gFnPc2
začerněný	začerněný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důsledkem	důsledek	k1gInSc7
byla	být	k5eAaImAgFnS
velmi	velmi	k6eAd1
vyčerpávající	vyčerpávající	k2eAgFnSc1d1
práce	práce	k1gFnSc1
tehdejších	tehdejší	k2eAgMnPc2d1
sladovníků	sladovník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Valach	valach	k1gInSc1
neboli	neboli	k8xC
kouřový	kouřový	k2eAgInSc1d1
hvozd	hvozd	k1gInSc1
byl	být	k5eAaImAgInS
zkonstruován	zkonstruovat	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1804	#num#	k4
podle	podle	k7c2
návrhu	návrh	k1gInSc2
legendárního	legendární	k2eAgMnSc2d1
inovátora	inovátor	k1gMnSc2
pivovarnictví	pivovarnictví	k1gNnSc2
F.	F.	kA
O.	O.	kA
Poupěte	poupě	k1gNnSc2
a	a	k8xC
sloužil	sloužit	k5eAaImAgMnS
do	do	k7c2
roku	rok	k1gInSc2
1942	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
to	ten	k3xDgNnSc1
poslední	poslední	k2eAgNnSc1d1
zařízení	zařízení	k1gNnSc1
tohoto	tento	k3xDgInSc2
druhu	druh	k1gInSc2
v	v	k7c6
českých	český	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
další	další	k2eAgFnSc6d1
části	část	k1gFnSc6
prohlídky	prohlídka	k1gFnSc2
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
vidět	vidět	k5eAaImF
např.	např.	kA
bronzový	bronzový	k2eAgInSc4d1
filtr	filtr	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
byl	být	k5eAaImAgInS
převezen	převézt	k5eAaPmNgMnS
z	z	k7c2
pivovaru	pivovar	k1gInSc2
v	v	k7c6
Dobříši	Dobříš	k1gFnSc6
nebo	nebo	k8xC
sprchový	sprchový	k2eAgInSc4d1
chladič	chladič	k1gInSc4
mladiny	mladina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaujme	zaujmout	k5eAaPmIp3nS
třeba	třeba	k6eAd1
i	i	k9
vzduchový	vzduchový	k2eAgInSc1d1
kompresor	kompresor	k1gInSc1
a	a	k8xC
směšovač	směšovač	k1gInSc1
piva	pivo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomocí	pomocí	k7c2
něho	on	k3xPp3gNnSc2
se	se	k3xPyFc4
směšovaly	směšovat	k5eAaImAgFnP
různé	různý	k2eAgFnPc1d1
várky	várka	k1gFnPc1
piva	pivo	k1gNnSc2
pro	pro	k7c4
dosažení	dosažení	k1gNnSc4
vyšší	vysoký	k2eAgFnSc2d2
kvality	kvalita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
zhlédnutí	zhlédnutí	k1gNnSc3
jsou	být	k5eAaImIp3nP
i	i	k9
nástroje	nástroj	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
sloužily	sloužit	k5eAaImAgInP
k	k	k7c3
promíchávání	promíchávání	k1gNnSc3
sladu	slad	k1gInSc2
na	na	k7c6
valachu	valach	k1gInSc6
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
mj.	mj.	kA
dřevěné	dřevěný	k2eAgFnPc4d1
lopaty	lopata	k1gFnPc4
a	a	k8xC
limpy	limpa	k1gFnPc4
<g/>
,	,	kIx,
dřevěné	dřevěný	k2eAgInPc4d1
nástroje	nástroj	k1gInPc4
sloužící	sloužící	k1gMnSc1
sladovníkovi	sladovník	k1gMnSc3
k	k	k7c3
ručnímu	ruční	k2eAgNnSc3d1
převracení	převracení	k1gNnSc3
sladu	slad	k1gInSc2
při	při	k7c6
sušení	sušení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Zajímavá	zajímavý	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
i	i	k9
sbírka	sbírka	k1gFnSc1
dobových	dobový	k2eAgFnPc2d1
fotografií	fotografia	k1gFnPc2
z	z	k7c2
počátku	počátek	k1gInSc2
dvacátého	dvacátý	k4xOgNnSc2
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ukazuje	ukazovat	k5eAaImIp3nS
třeba	třeba	k6eAd1
oblečení	oblečení	k1gNnSc1
servírky	servírka	k1gFnSc2
z	z	k7c2
doby	doba	k1gFnSc2
secese	secese	k1gFnSc2
z	z	k7c2
počátku	počátek	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
<g/>
,	,	kIx,
z	z	k7c2
roku	rok	k1gInSc2
1924	#num#	k4
a	a	k8xC
druhá	druhý	k4xOgFnSc1
z	z	k7c2
roku	rok	k1gInSc2
1931	#num#	k4
<g/>
,	,	kIx,
ukazují	ukazovat	k5eAaImIp3nP
pivovarskou	pivovarský	k2eAgFnSc4d1
chasu	chasa	k1gFnSc4
<g/>
,	,	kIx,
tedy	tedy	k9
pracovníky	pracovník	k1gMnPc4
pivovaru	pivovar	k1gInSc2
a	a	k8xC
restaurací	restaurace	k1gFnPc2
s	s	k7c7
majitelem	majitel	k1gMnSc7
Václavem	Václav	k1gMnSc7
Brtníkem	brtník	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgFnSc1d1
část	část	k1gFnSc1
prohlídky	prohlídka	k1gFnSc2
nabízí	nabízet	k5eAaImIp3nS
expozice	expozice	k1gFnSc1
bednářství	bednářství	k1gNnPc2
<g/>
,	,	kIx,
pocházející	pocházející	k2eAgMnPc1d1
nejspíše	nejspíše	k9
z	z	k7c2
období	období	k1gNnSc2
druhé	druhý	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
až	až	k9
první	první	k4xOgFnSc2
třetiny	třetina	k1gFnSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc4,k3yIgNnSc4,k3yQgNnSc4
k	k	k7c3
pivovaru	pivovar	k1gInSc3
neodbytně	odbytně	k6eNd1
patřily	patřit	k5eAaImAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lze	lze	k6eAd1
vidět	vidět	k5eAaImF
nástroje	nástroj	k1gInPc4
nezbytné	nezbytný	k2eAgInPc4d1,k2eNgInPc4d1
k	k	k7c3
výrobě	výroba	k1gFnSc3
sudů	sud	k1gInPc2
a	a	k8xC
ostatních	ostatní	k2eAgFnPc2d1
nádob	nádoba	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
používaly	používat	k5eAaImAgFnP
nejen	nejen	k6eAd1
v	v	k7c6
pivovaře	pivovar	k1gInSc6
a	a	k8xC
restauraci	restaurace	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
obecněji	obecně	k6eAd2
k	k	k7c3
životu	život	k1gInSc3
lidí	člověk	k1gMnPc2
v	v	k7c6
minulosti	minulost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
nástroje	nástroj	k1gInPc4
můžeme	moct	k5eAaImIp1nP
jmenovat	jmenovat	k5eAaBmF,k5eAaImF
hoblíky	hoblík	k1gInPc4
<g/>
,	,	kIx,
pořízy	poříz	k1gInPc4
<g/>
,	,	kIx,
modly	modla	k1gFnPc4
(	(	kIx(
<g/>
vyměřovací	vyměřovací	k2eAgFnPc4d1
lišty	lišta	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
sloužící	sloužící	k1gFnSc1
k	k	k7c3
vyměřování	vyměřování	k1gNnSc3
jednotlivých	jednotlivý	k2eAgInPc2d1
dílů	díl	k1gInPc2
sudů	sud	k1gInPc2
<g/>
,	,	kIx,
džberů	džber	k1gInPc2
aj.	aj.	kA
Zajímavá	zajímavý	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
i	i	k9
bednářská	bednářský	k2eAgFnSc1d1
lavice	lavice	k1gFnSc1
zvaná	zvaný	k2eAgFnSc1d1
dědek	dědek	k1gMnSc1
sloužící	sloužící	k2eAgMnSc1d1
k	k	k7c3
opracování	opracování	k1gNnSc3
dřeva	dřevo	k1gNnSc2
pořízem	poříz	k1gInSc7
a	a	k8xC
následné	následný	k2eAgNnSc1d1
sestavení	sestavení	k1gNnSc1
jednotlivých	jednotlivý	k2eAgFnPc2d1
částí	část	k1gFnPc2
pivních	pivní	k2eAgInPc2d1
sudů	sud	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
tam	tam	k6eAd1
i	i	k9
vypalovací	vypalovací	k2eAgNnPc1d1
železa	železo	k1gNnPc1
sloužící	sloužící	k2eAgNnPc1d1
k	k	k7c3
označení	označení	k1gNnSc3
<g/>
,	,	kIx,
ocejchování	ocejchování	k1gNnSc3
sudů	sud	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomocí	pomocí	k7c2
želez	železo	k1gNnPc2
se	se	k3xPyFc4
na	na	k7c4
ně	on	k3xPp3gMnPc4
vypalovaly	vypalovat	k5eAaImAgFnP
například	například	k6eAd1
jména	jméno	k1gNnPc1
pivovarů	pivovar	k1gInPc2
<g/>
,	,	kIx,
objem	objem	k1gInSc1
sudů	sud	k1gInPc2
<g/>
,	,	kIx,
označení	označení	k1gNnSc3
měst	město	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajímavé	zajímavý	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
bednáři	bednář	k1gMnPc1
byli	být	k5eAaImAgMnP
v	v	k7c6
pivovaru	pivovar	k1gInSc6
U	u	k7c2
Fleků	flek	k1gInPc2
ještě	ještě	k9
po	po	k7c6
druhé	druhý	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
a	a	k8xC
velmi	velmi	k6eAd1
pravděpodobně	pravděpodobně	k6eAd1
v	v	k7c6
šedesátých	šedesátý	k4xOgNnPc6
a	a	k8xC
sedmdesátých	sedmdesátý	k4xOgNnPc6
létech	léto	k1gNnPc6
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
přešlo	přejít	k5eAaPmAgNnS
na	na	k7c4
hliníkové	hliníkový	k2eAgNnSc4d1
a	a	k8xC
potom	potom	k6eAd1
nerezové	rezový	k2eNgInPc4d1
sudy	sud	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
se	se	k3xPyFc4
používají	používat	k5eAaImIp3nP
pivní	pivní	k2eAgInPc1d1
tanky	tank	k1gInPc1
<g/>
.	.	kIx.
</s>
<s>
Pivo	pivo	k1gNnSc1
však	však	k9
bylo	být	k5eAaImAgNnS
distribuováno	distribuovat	k5eAaBmNgNnS
i	i	k9
ve	v	k7c6
skleněných	skleněný	k2eAgFnPc6d1
lahvích	lahev	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ukázky	ukázka	k1gFnPc4
používaných	používaný	k2eAgNnPc2d1
skleněných	skleněný	k2eAgNnPc2d1
<g/>
,	,	kIx,
převážně	převážně	k6eAd1
litrových	litrový	k2eAgFnPc2d1
jsou	být	k5eAaImIp3nP
ze	z	k7c2
třicátých	třicátý	k4xOgNnPc2
let	léto	k1gNnPc2
dvacátého	dvacátý	k4xOgNnSc2
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc4
další	další	k2eAgInPc4d1
druhy	druh	k1gInPc4
lahví	lahev	k1gFnPc2
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
protektorátu	protektorát	k1gInSc2
Čechy	Čechy	k1gFnPc1
a	a	k8xC
Morava	Morava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
opatřeny	opatřen	k2eAgFnPc1d1
etiketami	etiketa	k1gFnPc7
a	a	k8xC
za	za	k7c2
protektorátu	protektorát	k1gInSc2
je	být	k5eAaImIp3nS
napřed	napřed	k6eAd1
uveden	uveden	k2eAgInSc1d1
název	název	k1gInSc1
německý	německý	k2eAgInSc1d1
a	a	k8xC
potom	potom	k6eAd1
český	český	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1
osobností	osobnost	k1gFnSc7
pivovaru	pivovar	k1gInSc2
byl	být	k5eAaImAgMnS
v	v	k7c6
první	první	k4xOgFnSc6
třetině	třetina	k1gFnSc6
dvacátého	dvacátý	k4xOgNnSc2
století	století	k1gNnSc2
Václav	Václav	k1gMnSc1
Brtník	brtník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pracoval	pracovat	k5eAaImAgMnS
v	v	k7c6
pivovaru	pivovar	k1gInSc6
U	u	k7c2
Fleků	flek	k1gInPc2
od	od	k7c2
roku	rok	k1gInSc2
1898	#num#	k4
<g/>
,	,	kIx,
po	po	k7c6
sedmi	sedm	k4xCc6
letech	léto	k1gNnPc6
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
sládkem	sládek	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1921	#num#	k4
<g/>
,	,	kIx,
po	po	k7c6
smrti	smrt	k1gFnSc6
vdovy	vdova	k1gFnSc2
po	po	k7c6
posledním	poslední	k2eAgMnSc6d1
majiteli	majitel	k1gMnSc6
<g/>
,	,	kIx,
celý	celý	k2eAgInSc4d1
pivovar	pivovar	k1gInSc4
a	a	k8xC
restauraci	restaurace	k1gFnSc4
koupil	koupit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc4
sedmdesáté	sedmdesátý	k4xOgFnPc4
narozeniny	narozeniny	k1gFnPc4
v	v	k7c6
roce	rok	k1gInSc6
1934	#num#	k4
připomínají	připomínat	k5eAaImIp3nP
ukázky	ukázka	k1gFnPc1
článku	článek	k1gInSc2
z	z	k7c2
tehdejšího	tehdejší	k2eAgInSc2d1
odborného	odborný	k2eAgInSc2d1
tisku	tisk	k1gInSc2
<g/>
,	,	kIx,
společné	společný	k2eAgFnSc2d1
fotografie	fotografia	k1gFnSc2
se	se	k3xPyFc4
zaměstnanci	zaměstnanec	k1gMnSc3
a	a	k8xC
také	také	k9
zpráva	zpráva	k1gFnSc1
o	o	k7c4
jeho	jeho	k3xOp3gNnSc4
úmrtí	úmrtí	k1gNnSc4
v	v	k7c6
roce	rok	k1gInSc6
1937	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
majitelem	majitel	k1gMnSc7
syn	syn	k1gMnSc1
František	František	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Druhá	druhý	k4xOgFnSc1
sladovna	sladovna	k1gFnSc1
–	–	k?
pokračování	pokračování	k1gNnSc1
návštěvnického	návštěvnický	k2eAgInSc2d1
okruhu	okruh	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Druhá	druhý	k4xOgFnSc1
část	část	k1gFnSc1
prohlídky	prohlídka	k1gFnSc2
pokračuje	pokračovat	k5eAaImIp3nS
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
sladovně	sladovna	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
budově	budova	k1gFnSc6
z	z	k7c2
roku	rok	k1gInSc2
1889	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
již	jenž	k3xRgFnSc4
fungovala	fungovat	k5eAaImAgFnS
na	na	k7c6
modernějším	moderní	k2eAgInSc6d2
výrobním	výrobní	k2eAgInSc6d1
principu	princip	k1gInSc6
než	než	k8xS
první	první	k4xOgInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
novější	nový	k2eAgFnSc6d2
sladovně	sladovna	k1gFnSc6
se	se	k3xPyFc4
slad	slad	k1gInSc1
vyráběl	vyrábět	k5eAaImAgInS
sušením	sušení	k1gNnSc7
horkým	horký	k2eAgInSc7d1
vzduchem	vzduch	k1gInSc7
<g/>
,	,	kIx,
tam	tam	k6eAd1
se	se	k3xPyFc4
již	již	k9
technologie	technologie	k1gFnSc1
vykuřování	vykuřování	k1gNnSc2
<g/>
,	,	kIx,
velmi	velmi	k6eAd1
pracná	pracný	k2eAgFnSc1d1
<g/>
,	,	kIx,
nepoužívala	používat	k5eNaImAgFnS
<g/>
.	.	kIx.
</s>
<s>
Budova	budova	k1gFnSc1
má	mít	k5eAaImIp3nS
dvě	dva	k4xCgNnPc4
patra	patro	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
přízemí	přízemí	k1gNnSc6
jsou	být	k5eAaImIp3nP
zajímavé	zajímavý	k2eAgInPc1d1
dobové	dobový	k2eAgInPc1d1
plány	plán	k1gInPc1
rekonstrukcí	rekonstrukce	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
významně	významně	k6eAd1
změnily	změnit	k5eAaPmAgFnP
dispozice	dispozice	k1gFnPc1
pivovaru	pivovar	k1gInSc2
a	a	k8xC
sladovny	sladovna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
ručně	ručně	k6eAd1
signované	signovaný	k2eAgFnPc1d1
kopie	kopie	k1gFnPc1
z	z	k7c2
konce	konec	k1gInSc2
19	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
počátku	počátek	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
probíhaly	probíhat	k5eAaImAgFnP
přestavby	přestavba	k1gFnPc1
a	a	k8xC
rekonstrukce	rekonstrukce	k1gFnSc1
jak	jak	k6eAd1
vnějších	vnější	k2eAgFnPc2d1
prostor	prostora	k1gFnPc2
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
restaurací	restaurace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Částečně	částečně	k6eAd1
již	již	k6eAd1
za	za	k7c2
přičinění	přičinění	k1gNnSc2
již	již	k6eAd1
zmíněného	zmíněný	k2eAgMnSc2d1
V.	V.	kA
Brtníka	brtník	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
schodech	schod	k1gInPc6
do	do	k7c2
vrchních	vrchní	k1gFnPc2
pater	patro	k1gNnPc2
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
zajímavá	zajímavý	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
Čech	Čechy	k1gFnPc2
a	a	k8xC
Moravy	Morava	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
zobrazuje	zobrazovat	k5eAaImIp3nS
výskyt	výskyt	k1gInSc4
pivovarů	pivovar	k1gInPc2
z	z	k7c2
roku	rok	k1gInSc2
1914	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
doby	doba	k1gFnSc2
Rakousko-Uherské	rakousko-uherský	k2eAgFnSc2d1
monarchie	monarchie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
vidět	vidět	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
zvláště	zvláště	k6eAd1
Čechy	Čech	k1gMnPc7
byly	být	k5eAaImAgFnP
plné	plný	k2eAgFnPc4d1
pivovarů	pivovar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaujmou	zaujmout	k5eAaPmIp3nP
i	i	k8xC
data	datum	k1gNnPc1
o	o	k7c6
počtu	počet	k1gInSc6
a	a	k8xC
výstavu	výstav	k1gInSc6
jednotlivých	jednotlivý	k2eAgInPc2d1
pivovarů	pivovar	k1gInPc2
těsně	těsně	k6eAd1
před	před	k7c7
první	první	k4xOgFnSc7
světovou	světový	k2eAgFnSc7d1
válkou	válka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajímavostí	zajímavost	k1gFnPc2
je	být	k5eAaImIp3nS
i	i	k9
tabulka	tabulka	k1gFnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
návštěvníci	návštěvník	k1gMnPc1
mohou	moct	k5eAaImIp3nP
seznámit	seznámit	k5eAaPmF
s	s	k7c7
počtem	počet	k1gInSc7
i	i	k8xC
výstavem	výstav	k1gInSc7
piva	pivo	k1gNnSc2
v	v	k7c6
jiných	jiná	k1gFnPc6
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
z	z	k7c2
roku	rok	k1gInSc2
1913	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
stěnách	stěna	k1gFnPc6
jsou	být	k5eAaImIp3nP
vystaveny	vystavit	k5eAaPmNgInP
i	i	k9
různé	různý	k2eAgInPc1d1
výuční	výuční	k2eAgInPc1d1
listy	list	k1gInPc1
sladovnického	sladovnický	k2eAgNnSc2d1
a	a	k8xC
pivovarského	pivovarský	k2eAgNnSc2d1
řemesla	řemeslo	k1gNnSc2
z	z	k7c2
první	první	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
dvacátého	dvacátý	k4xOgNnSc2
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Upoutají	upoutat	k5eAaPmIp3nP
především	především	k9
dobovou	dobový	k2eAgFnSc7d1
estetickou	estetický	k2eAgFnSc7d1
stránkou	stránka	k1gFnSc7
i	i	k8xC
dalšími	další	k2eAgFnPc7d1
raritami	rarita	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
druhém	druhý	k4xOgInSc6
patře	patro	k1gNnSc6
tak	tak	k6eAd1
kromě	kromě	k7c2
fotografií	fotografia	k1gFnPc2
rodiny	rodina	k1gFnSc2
V.	V.	kA
Brtníka	brtník	k1gMnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
i	i	k9
tablo	tablo	k1gNnSc1
se	s	k7c7
spolupracovníky	spolupracovník	k1gMnPc7
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
bylo	být	k5eAaImAgNnS
vytvořeno	vytvořit	k5eAaPmNgNnS
k	k	k7c3
jeho	jeho	k3xOp3gNnSc3
sedmdesátým	sedmdesátý	k4xOgFnPc3
narozeninám	narozeniny	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
nechybí	chybět	k5eNaImIp3nS,k5eNaPmIp3nS
ani	ani	k8xC
fotografie	fotografie	k1gFnSc1
s	s	k7c7
ukázkami	ukázka	k1gFnPc7
pivovarských	pivovarský	k2eAgFnPc2d1
povozů	povoz	k1gInPc2
tažených	tažený	k2eAgInPc2d1
koňmi	kůň	k1gMnPc7
<g/>
,	,	kIx,
kterými	který	k3yRgFnPc7,k3yQgFnPc7,k3yIgFnPc7
se	se	k3xPyFc4
rozváželo	rozvážet	k5eAaImAgNnS
pivo	pivo	k1gNnSc1
především	především	k6eAd1
sudové	sudová	k1gFnSc2
po	po	k7c6
hospodách	hospodách	k?
a	a	k8xC
restauracích	restaurace	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
vidění	vidění	k1gNnSc3
jsou	být	k5eAaImIp3nP
i	i	k9
plničky	plnička	k1gFnPc1
lahví	lahev	k1gFnPc2
<g/>
,	,	kIx,
kterými	který	k3yIgFnPc7,k3yQgFnPc7,k3yRgFnPc7
se	se	k3xPyFc4
rovněž	rovněž	k9
<g/>
,	,	kIx,
ale	ale	k8xC
v	v	k7c6
mnohem	mnohem	k6eAd1
menší	malý	k2eAgFnSc3d2
míře	míra	k1gFnSc3
distribuovalo	distribuovat	k5eAaBmAgNnS
pivo	pivo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Prohlídka	prohlídka	k1gFnSc1
pokračuje	pokračovat	k5eAaImIp3nS
kolem	kolem	k7c2
dvířek	dvířka	k1gNnPc2
původního	původní	k2eAgNnSc2d1
topeniště	topeniště	k1gNnSc2
a	a	k8xC
dále	daleko	k6eAd2
do	do	k7c2
horní	horní	k2eAgFnSc2d1
místnosti	místnost	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
sloužila	sloužit	k5eAaImAgFnS
k	k	k7c3
sušení	sušení	k1gNnSc3
skladu	sklad	k1gInSc2
horkým	horký	k2eAgInSc7d1
vzduchem	vzduch	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podlaha	podlaha	k1gFnSc1
je	být	k5eAaImIp3nS
tvořena	tvořit	k5eAaImNgFnS
kovovými	kovový	k2eAgInPc7d1
rošty	rošt	k1gInPc7
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
se	se	k3xPyFc4
slad	slad	k1gInSc1
rozprostřel	rozprostřít	k5eAaPmAgInS
a	a	k8xC
byl	být	k5eAaImAgInS
sušen	sušit	k5eAaImNgInS
horkým	horký	k2eAgInSc7d1
vzduchem	vzduch	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
stoupal	stoupat	k5eAaImAgInS
z	z	k7c2
topeniště	topeniště	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
místnosti	místnost	k1gFnSc6
jsou	být	k5eAaImIp3nP
vystaveny	vystavit	k5eAaPmNgInP
další	další	k2eAgInPc1d1
dobové	dobový	k2eAgInPc1d1
exponáty	exponát	k1gInPc1
jako	jako	k9
jsou	být	k5eAaImIp3nP
ruční	ruční	k2eAgFnPc1d1
plničky	plnička	k1gFnPc1
lahví	lahev	k1gFnPc2
jedno	jeden	k4xCgNnSc1
lahvové	lahvový	k2eAgInPc1d1
<g/>
,	,	kIx,
dále	daleko	k6eAd2
tzv.	tzv.	kA
rmutovačka	rmutovačka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zařízení	zařízení	k1gNnSc1
sloužící	sloužící	k2eAgNnSc1d1
ke	k	k7c3
zvýšení	zvýšení	k1gNnSc3
kvality	kvalita	k1gFnSc2
sladu	slad	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
historicky	historicky	k6eAd1
cenný	cenný	k2eAgInSc4d1
laboratorní	laboratorní	k2eAgInSc4d1
přístroj	přístroj	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c4
zmínku	zmínka	k1gFnSc4
stojí	stát	k5eAaImIp3nS
tzv.	tzv.	kA
volgemut	volgemut	k1gInSc1
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
jsou	být	k5eAaImIp3nP
jakési	jakýsi	k3yIgFnPc4
kovové	kovový	k2eAgFnPc4d1
hrábě	hrábě	k1gFnPc4
sloužící	sloužící	k2eAgFnPc1d1
k	k	k7c3
protočení	protočení	k1gNnSc3
a	a	k8xC
obracení	obracení	k1gNnSc3
klíčícího	klíčící	k2eAgNnSc2d1
obilného	obilný	k2eAgNnSc2d1
zrna	zrno	k1gNnSc2
a	a	k8xC
tím	ten	k3xDgNnSc7
i	i	k9
k	k	k7c3
jeho	jeho	k3xOp3gNnSc3
lepšímu	dobrý	k2eAgNnSc3d2
proschnutí	proschnutí	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
K	k	k7c3
vidění	vidění	k1gNnSc3
jsou	být	k5eAaImIp3nP
i	i	k9
ukázky	ukázka	k1gFnPc1
půllitrových	půllitrův	k2eAgMnPc2d1
a	a	k8xC
litrových	litrový	k2eAgFnPc2d1
lahví	lahev	k1gFnPc2
z	z	k7c2
různých	různý	k2eAgInPc2d1
pivovarů	pivovar	k1gInPc2
českých	český	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgNnSc1
jsou	být	k5eAaImIp3nP
embosované	embosovaný	k2eAgFnPc1d1
<g/>
,	,	kIx,
tedy	tedy	k8xC
s	s	k7c7
nápisem	nápis	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
vystupuje	vystupovat	k5eAaImIp3nS
ze	z	k7c2
skla	sklo	k1gNnSc2
láhve	láhev	k1gFnSc2
obsahující	obsahující	k2eAgInSc4d1
název	název	k1gInSc4
piva	pivo	k1gNnSc2
<g/>
,	,	kIx,
pivovaru	pivovar	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
jej	on	k3xPp3gInSc4
vyráběl	vyrábět	k5eAaImAgInS
atd.	atd.	kA
Zajímavostí	zajímavost	k1gFnSc7
je	být	k5eAaImIp3nS
i	i	k8xC
zátkovačka	zátkovačka	k1gFnSc1
k	k	k7c3
uzavíraní	uzavíraný	k2eAgMnPc1d1
lahví	lahev	k1gFnPc2
tehdejší	tehdejší	k2eAgFnSc7d1
novinkou	novinka	k1gFnSc7
<g/>
,	,	kIx,
korunkou	korunka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Vyrobený	vyrobený	k2eAgInSc1d1
slad	slad	k1gInSc1
se	se	k3xPyFc4
potom	potom	k6eAd1
ručně	ručně	k6eAd1
lopatami	lopata	k1gFnPc7
přesouval	přesouvat	k5eAaImAgInS
ke	k	k7c3
zmíněným	zmíněný	k2eAgNnPc3d1
dvířkům	dvířka	k1gNnPc3
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
se	se	k3xPyFc4
shazoval	shazovat	k5eAaImAgMnS
o	o	k7c4
patro	patro	k1gNnSc4
níže	nízce	k6eAd2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
dosušoval	dosušovat	k5eAaImAgMnS
a	a	k8xC
potom	potom	k6eAd1
se	se	k3xPyFc4
pytloval	pytlovat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
místnosti	místnost	k1gFnSc6
byla	být	k5eAaImAgFnS
podlaha	podlaha	k1gFnSc1
původně	původně	k6eAd1
pokryta	pokrýt	k5eAaPmNgFnS
konvovými	konvový	k2eAgInPc7d1
rošty	rošt	k1gInPc7
<g/>
,	,	kIx,
k	k	k7c3
již	již	k9
zmíněnému	zmíněný	k2eAgNnSc3d1
dosoušení	dosoušení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Unikátním	unikátní	k2eAgNnSc7d1
zařízením	zařízení	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
je	být	k5eAaImIp3nS
málokde	málokde	k6eAd1
k	k	k7c3
viděni	vidět	k5eAaImNgMnP
je	být	k5eAaImIp3nS
tzv.	tzv.	kA
odkličovačka	odkličovačka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
sloužila	sloužit	k5eAaImAgFnS
k	k	k7c3
odstraňování	odstraňování	k1gNnSc3
kořínků	kořínek	k1gInPc2
z	z	k7c2
naklíčeného	naklíčený	k2eAgInSc2d1
ječmene	ječmen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Kuriozitou	kuriozita	k1gFnSc7
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
keramický	keramický	k2eAgInSc1d1
30	#num#	k4
litrový	litrový	k2eAgInSc1d1
korbel	korbel	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
získal	získat	k5eAaPmAgInS
cenu	cena	k1gFnSc4
na	na	k7c6
festivalu	festival	k1gInSc6
kuriozit	kuriozita	k1gFnPc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
tradičně	tradičně	k6eAd1
pořádá	pořádat	k5eAaImIp3nS
v	v	k7c6
Pelhřimově	Pelhřimov	k1gInSc6
a	a	k8xC
má	mít	k5eAaImIp3nS
vazbu	vazba	k1gFnSc4
na	na	k7c4
Guinessovu	Guinessův	k2eAgFnSc4d1
knihu	kniha	k1gFnSc4
rekordů	rekord	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
závěrečné	závěrečný	k2eAgFnSc6d1
části	část	k1gFnSc6
muzea	muzeum	k1gNnSc2
jsou	být	k5eAaImIp3nP
instalovány	instalovat	k5eAaBmNgFnP
další	další	k2eAgFnPc1d1
plničky	plnička	k1gFnPc1
lahví	lahev	k1gFnPc2
<g/>
,	,	kIx,
narážecí	narážecí	k2eAgFnPc1d1
jehly	jehla	k1gFnPc1
do	do	k7c2
sudů	sud	k1gInPc2
<g/>
,	,	kIx,
kterými	který	k3yQgInPc7,k3yIgInPc7,k3yRgInPc7
se	se	k3xPyFc4
naráželo	narážet	k5eAaPmAgNnS,k5eAaImAgNnS
pivo	pivo	k1gNnSc1
k	k	k7c3
distribuci	distribuce	k1gFnSc3
do	do	k7c2
výčepních	výčepní	k1gFnPc2
zařízení	zařízení	k1gNnSc2
i	i	k9
ukázky	ukázka	k1gFnPc1
mj.	mj.	kA
i	i	k8xC
z	z	k7c2
doby	doba	k1gFnSc2
Československa	Československo	k1gNnSc2
let	léto	k1gNnPc2
1948	#num#	k4
<g/>
-	-	kIx~
<g/>
1989	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Vstupní	vstupní	k2eAgNnSc1d1
průčelí	průčelí	k1gNnSc1
roku	rok	k1gInSc2
2019	#num#	k4
</s>
<s>
Výčep	výčep	k1gInSc1
</s>
<s>
Zahrada	zahrada	k1gFnSc1
s	s	k7c7
kaštany	kaštan	k1gInPc7
</s>
<s>
Sál	sál	k1gInSc1
restaurace	restaurace	k1gFnSc2
</s>
<s>
Sál	sál	k1gInSc1
"	"	kIx"
<g/>
Akademická	akademický	k2eAgFnSc1d1
<g/>
"	"	kIx"
s	s	k7c7
malbou	malba	k1gFnSc7
Ládi	Láďa	k1gMnSc2
Nováka	Novák	k1gMnSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Vacl	Vacl	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
<g/>
:	:	kIx,
Sládek	Sládek	k1gMnSc1
Ivan	Ivan	k1gMnSc1
Chramosil	Chramosil	k1gMnSc1
ani	ani	k8xC
po	po	k7c6
44	#num#	k4
letech	léto	k1gNnPc6
v	v	k7c6
pivovarství	pivovarství	k1gNnSc6
neplánuje	plánovat	k5eNaImIp3nS
odchod	odchod	k1gInSc1
z	z	k7c2
oboru	obor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potravinářská	potravinářský	k2eAgFnSc1d1
revue	revue	k1gFnSc1
č.	č.	k?
2	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
,	,	kIx,
http://www.certum.cz/publikovali-jsme/528-3	http://www.certum.cz/publikovali-jsme/528-3	k4
</s>
<s>
Vacl	Vacl	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
<g/>
:	:	kIx,
Legendární	legendární	k2eAgMnSc1d1
sládek	sládek	k1gMnSc1
Ivan	Ivan	k1gMnSc1
Chramosil	Chramosil	k1gMnSc7
sedmdesátníkem	sedmdesátník	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvasny	Kvasny	k?
Prum	Prum	k1gMnSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
,	,	kIx,
62	#num#	k4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
115	#num#	k4
<g/>
-	-	kIx~
<g/>
116	#num#	k4
<g/>
.	.	kIx.
http://www.certum.cz/publikovali-jsme/529-3	http://www.certum.cz/publikovali-jsme/529-3	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Galerie	galerie	k1gFnSc1
Pivovarské	pivovarský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
U	u	k7c2
Fleků	flek	k1gInPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Pivovarské	pivovarský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
U	u	k7c2
Fleků	flek	k1gInPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
muzea	muzeum	k1gNnSc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
pivovaru	pivovar	k1gInSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Muzea	muzeum	k1gNnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
Všeobecná	všeobecný	k2eAgFnSc1d1
</s>
<s>
Národní	národní	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
(	(	kIx(
<g/>
Historické	historický	k2eAgInPc1d1
•	•	k?
zoologické	zoologický	k2eAgFnSc2d1
•	•	k?
mineralogické	mineralogický	k2eAgNnSc1d1
oddělení	oddělení	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Nová	nový	k2eAgFnSc1d1
budova	budova	k1gFnSc1
Národního	národní	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
•	•	k?
Muzeum	muzeum	k1gNnSc1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
•	•	k?
Uhříněveské	uhříněveský	k2eAgNnSc4d1
muzeum	muzeum	k1gNnSc4
Národopisná	národopisný	k2eAgFnSc1d1
<g/>
,	,	kIx,
etnologická	etnologický	k2eAgFnSc1d1
</s>
<s>
Národopisné	národopisný	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
(	(	kIx(
<g/>
Letohrádek	letohrádek	k1gInSc1
Kinských	Kinská	k1gFnPc2
<g/>
)	)	kIx)
•	•	k?
Náprstkovo	Náprstkův	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
asijských	asijský	k2eAgFnPc2d1
<g/>
,	,	kIx,
afrických	africký	k2eAgFnPc2d1
a	a	k8xC
amerických	americký	k2eAgFnPc2d1
kultur	kultura	k1gFnPc2
•	•	k?
Židovské	židovský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
•	•	k?
Pedagogické	pedagogický	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
J.	J.	kA
A.	A.	kA
Komenského	Komenský	k1gMnSc2
Vojenská	vojenský	k2eAgFnSc1d1
<g/>
,	,	kIx,
válečná	válečný	k2eAgFnSc1d1
a	a	k8xC
policejní	policejní	k2eAgFnSc1d1
</s>
<s>
Armádní	armádní	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
Žižkov	Žižkov	k1gInSc1
(	(	kIx(
<g/>
Vítkov	Vítkov	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Vojenské	vojenský	k2eAgNnSc1d1
historické	historický	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
•	•	k?
Expozice	expozice	k1gFnSc1
bitvy	bitva	k1gFnSc2
na	na	k7c6
Bílé	bílý	k2eAgFnSc6d1
hoře	hora	k1gFnSc6
(	(	kIx(
<g/>
Letohrádek	letohrádek	k1gInSc1
Hvězda	hvězda	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Policejní	policejní	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
•	•	k?
Národní	národní	k2eAgInSc1d1
památník	památník	k1gInSc1
hrdinů	hrdina	k1gMnPc2
heydrichiády	heydrichiáda	k1gFnSc2
Astronomická	astronomický	k2eAgNnPc1d1
<g/>
,	,	kIx,
přírodovědná	přírodovědný	k2eAgNnPc1d1
</s>
<s>
Hrdličkovo	Hrdličkův	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
člověka	člověk	k1gMnSc2
•	•	k?
Chlupáčovo	chlupáčův	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
historie	historie	k1gFnSc2
Země	zem	k1gFnSc2
•	•	k?
Keplerovo	Keplerův	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
•	•	k?
Mineralogické	mineralogický	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
•	•	k?
Muzeum	muzeum	k1gNnSc1
teraristiky	teraristika	k1gFnSc2
a	a	k8xC
výstava	výstava	k1gFnSc1
plazů	plaz	k1gMnPc2
•	•	k?
Zdravotnické	zdravotnický	k2eAgNnSc4d1
muzeum	muzeum	k1gNnSc4
•	•	k?
Zoo	zoo	k1gFnSc1
Praha	Praha	k1gFnSc1
Zemědělská	zemědělský	k2eAgFnSc1d1
<g/>
,	,	kIx,
gastronomická	gastronomický	k2eAgFnSc1d1
</s>
<s>
Zemědělské	zemědělský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
•	•	k?
Muzeum	muzeum	k1gNnSc1
gastronomie	gastronomie	k1gFnSc2
•	•	k?
Muzeum	muzeum	k1gNnSc1
čokolády	čokoláda	k1gFnSc2
•	•	k?
Muzeum	muzeum	k1gNnSc1
piva	pivo	k1gNnSc2
•	•	k?
Pivovarské	pivovarský	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
U	u	k7c2
Fleků	flek	k1gInPc2
Technická	technický	k2eAgFnSc1d1
<g/>
,	,	kIx,
dopravní	dopravní	k2eAgFnSc1d1
<g/>
,	,	kIx,
poštovní	poštovní	k2eAgFnSc1d1
</s>
<s>
Technické	technický	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
•	•	k?
Muzeum	muzeum	k1gNnSc1
MHD	MHD	kA
•	•	k?
Letecké	letecký	k2eAgNnSc4d1
muzeum	muzeum	k1gNnSc4
Kbely	Kbely	k1gInPc1
•	•	k?
Waldesovo	Waldesův	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
•	•	k?
Stará	starat	k5eAaImIp3nS
čistírna	čistírna	k1gFnSc1
odpadních	odpadní	k2eAgFnPc2d1
vod	voda	k1gFnPc2
(	(	kIx(
<g/>
Praha-Bubeneč	Praha-Bubeneč	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Muzeum	muzeum	k1gNnSc1
pražského	pražský	k2eAgNnSc2d1
vodárenství	vodárenství	k1gNnSc2
•	•	k?
Muzeum	muzeum	k1gNnSc1
v	v	k7c6
podskalské	podskalský	k2eAgFnSc6d1
celnici	celnice	k1gFnSc6
na	na	k7c6
Výtoni	výtoň	k1gFnSc6
•	•	k?
Poštovní	poštovní	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
•	•	k?
Muzeum	muzeum	k1gNnSc1
Karlova	Karlův	k2eAgInSc2d1
mostu	most	k1gInSc2
•	•	k?
Muzeum	muzeum	k1gNnSc1
PRE	PRE	kA
•	•	k?
Muzeum	muzeum	k1gNnSc1
historických	historický	k2eAgInPc2d1
nočníků	nočník	k1gInPc2
a	a	k8xC
toalet	toaleta	k1gFnPc2
Hudební	hudební	k2eAgFnSc1d1
<g/>
,	,	kIx,
literární	literární	k2eAgFnSc1d1
</s>
<s>
České	český	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
hudby	hudba	k1gFnSc2
(	(	kIx(
<g/>
Muzeum	muzeum	k1gNnSc1
Bedřicha	Bedřich	k1gMnSc2
Smetany	Smetana	k1gMnSc2
•	•	k?
Muzeum	muzeum	k1gNnSc1
Antonína	Antonín	k1gMnSc2
Dvořáka	Dvořák	k1gMnSc2
(	(	kIx(
<g/>
Vila	vít	k5eAaImAgFnS
Amerika	Amerika	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Památník	památník	k1gInSc1
národního	národní	k2eAgNnSc2d1
písemnictví	písemnictví	k1gNnSc2
•	•	k?
Muzeum	muzeum	k1gNnSc1
Franze	Franze	k1gFnSc1
Kafky	Kafka	k1gMnSc2
•	•	k?
Památník	památník	k1gInSc1
Jaroslava	Jaroslav	k1gMnSc2
Ježka	Ježek	k1gMnSc2
–	–	k?
Modrý	modrý	k2eAgInSc4d1
pokoj	pokoj	k1gInSc4
<g/>
)	)	kIx)
•	•	k?
Muzeum	muzeum	k1gNnSc1
W.	W.	kA
A.	A.	kA
Mozarta	Mozart	k1gMnSc2
a	a	k8xC
manželů	manžel	k1gMnPc2
Duškových	Duškových	k2eAgMnPc2d1
(	(	kIx(
<g/>
vila	vít	k5eAaImAgFnS
Bertramka	Bertramka	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Werichova	Werichův	k2eAgFnSc1d1
vila	vila	k1gFnSc1
Výtvarného	výtvarný	k2eAgNnSc2d1
umění	umění	k1gNnSc2
</s>
<s>
Národní	národní	k2eAgFnSc1d1
galerie	galerie	k1gFnSc1
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
Anežský	anežský	k2eAgInSc1d1
klášter	klášter	k1gInSc1
•	•	k?
Šternberský	šternberský	k2eAgInSc1d1
palác	palác	k1gInSc1
•	•	k?
Salmovský	Salmovský	k2eAgInSc1d1
palác	palác	k1gInSc1
•	•	k?
Palác	palác	k1gInSc1
Kinských	Kinských	k2eAgInSc1d1
•	•	k?
Veletržní	veletržní	k2eAgInSc1d1
palác	palác	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Uměleckoprůmyslové	uměleckoprůmyslový	k2eAgNnSc1d1
museum	museum	k1gNnSc1
•	•	k?
Muzeum	muzeum	k1gNnSc1
Alfonse	Alfons	k1gMnSc2
Muchy	Mucha	k1gMnSc2
•	•	k?
Museum	museum	k1gNnSc1
Kampa	Kampa	k1gFnSc1
•	•	k?
Galerie	galerie	k1gFnSc1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
(	(	kIx(
<g/>
Bílkova	Bílkův	k2eAgFnSc1d1
vila	vila	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Galerie	galerie	k1gFnSc1
Josefa	Josefa	k1gFnSc1
Sudka	sudka	k1gFnSc1
•	•	k?
Gorlice	Gorlice	k1gFnSc1
(	(	kIx(
<g/>
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Lapidárium	lapidárium	k1gNnSc1
Národního	národní	k2eAgNnSc2d1
muzea	muzeum	k1gNnSc2
•	•	k?
Budova	budova	k1gFnSc1
Spolku	spolek	k1gInSc2
výtvarných	výtvarný	k2eAgMnPc2d1
umělců	umělec	k1gMnPc2
Mánes	Mánes	k1gMnSc1
(	(	kIx(
<g/>
Galerie	galerie	k1gFnSc1
Mánes	Mánes	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Muzeum	muzeum	k1gNnSc1
Karla	Karel	k1gMnSc2
Zemana	Zeman	k1gMnSc2
Skanzen	skanzen	k1gInSc1
</s>
<s>
Zámecký	zámecký	k2eAgInSc1d1
areál	areál	k1gInSc1
Ctěnice	Ctěnice	k1gFnSc2
•	•	k?
Skanzen	skanzen	k1gInSc1
Řepora	Řepor	k1gMnSc2
•	•	k?
Vršovický	vršovický	k2eAgInSc4d1
skanzen	skanzen	k1gInSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Praha	Praha	k1gFnSc1
|	|	kIx~
Pivo	pivo	k1gNnSc1
</s>
