<s>
Airbus	airbus	k1gInSc1	airbus
A380	A380	k1gFnSc2	A380
je	být	k5eAaImIp3nS	být
dvoupatrový	dvoupatrový	k2eAgInSc1d1	dvoupatrový
čtyřmotorový	čtyřmotorový	k2eAgInSc1d1	čtyřmotorový
proudový	proudový	k2eAgInSc1d1	proudový
širokotrupý	širokotrupý	k2eAgInSc1d1	širokotrupý
letoun	letoun	k1gInSc1	letoun
vyráběný	vyráběný	k2eAgInSc1d1	vyráběný
evropskou	evropský	k2eAgFnSc7d1	Evropská
společností	společnost	k1gFnSc7	společnost
Airbus	airbus	k1gInSc1	airbus
S.	S.	kA	S.
<g/>
A.S.	A.S.	k1gMnPc2	A.S.
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgNnSc7d3	veliký
dopravním	dopravní	k2eAgNnSc7d1	dopravní
letadlem	letadlo	k1gNnSc7	letadlo
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
A380	A380	k1gFnSc1	A380
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
nákladní	nákladní	k2eAgInSc4d1	nákladní
Antonov	Antonov	k1gInSc4	Antonov
An-	An-	k1gFnPc4	An-
<g/>
225	[number]	k4	225
(	(	kIx(	(
<g/>
vyroben	vyrobit	k5eAaPmNgInS	vyrobit
jeden	jeden	k4xCgInSc1	jeden
kus	kus	k1gInSc1	kus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hydroplán	hydroplán	k1gInSc1	hydroplán
Hughes	Hughes	k1gMnSc1	Hughes
H-4	H-4	k1gMnSc1	H-4
Hercules	Hercules	k1gMnSc1	Hercules
(	(	kIx(	(
<g/>
prototyp	prototyp	k1gInSc1	prototyp
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzducholoď	vzducholoď	k1gFnSc4	vzducholoď
Hindenburg	Hindenburg	k1gInSc4	Hindenburg
a	a	k8xC	a
Boeing	boeing	k1gInSc4	boeing
747	[number]	k4	747
Dreamlifter	Dreamliftra	k1gFnPc2	Dreamliftra
(	(	kIx(	(
<g/>
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
An-	An-	k1gFnSc1	An-
<g/>
225	[number]	k4	225
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
červenci	červenec	k1gInSc3	červenec
2017	[number]	k4	2017
ho	on	k3xPp3gMnSc4	on
bylo	být	k5eAaImAgNnS	být
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
214	[number]	k4	214
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
širokotrupý	širokotrupý	k2eAgInSc4d1	širokotrupý
letoun	letoun	k1gInSc4	letoun
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
palubami	paluba	k1gFnPc7	paluba
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
pohon	pohon	k1gInSc1	pohon
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
čtyři	čtyři	k4xCgInPc4	čtyři
dvouproudové	dvouproudový	k2eAgInPc4d1	dvouproudový
motory	motor	k1gInPc4	motor
buď	buď	k8xC	buď
evropské	evropský	k2eAgInPc4d1	evropský
(	(	kIx(	(
<g/>
Rolls-Royce	Rolls-Royec	k1gInPc4	Rolls-Royec
Trent	Trent	k1gInSc1	Trent
900	[number]	k4	900
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
americké	americký	k2eAgFnPc1d1	americká
(	(	kIx(	(
<g/>
Engine	Engin	k1gInSc5	Engin
Alliance	Allianec	k1gMnPc4	Allianec
GP	GP	kA	GP
<g/>
7200	[number]	k4	7200
<g/>
)	)	kIx)	)
provenience	provenience	k1gFnSc2	provenience
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
konfiguraci	konfigurace	k1gFnSc6	konfigurace
pojme	pojmout	k5eAaPmIp3nS	pojmout
od	od	k7c2	od
525	[number]	k4	525
cestujících	cestující	k1gMnPc2	cestující
do	do	k7c2	do
855	[number]	k4	855
při	při	k7c6	při
verzi	verze	k1gFnSc6	verze
economy	econom	k1gInPc4	econom
<g/>
.	.	kIx.	.
</s>
<s>
Rozpětí	rozpětí	k1gNnSc2	rozpětí
jeho	jeho	k3xOp3gNnPc2	jeho
křídel	křídlo	k1gNnPc2	křídlo
je	být	k5eAaImIp3nS	být
79,8	[number]	k4	79,8
metru	metr	k1gInSc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
luxusní	luxusní	k2eAgFnSc6d1	luxusní
verzi	verze	k1gFnSc6	verze
letadla	letadlo	k1gNnSc2	letadlo
tzv	tzv	kA	tzv
business	business	k1gInSc1	business
jet	jet	k2eAgMnSc1d1	jet
mají	mít	k5eAaImIp3nP	mít
cestující	cestující	k1gMnPc1	cestující
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
vlastní	vlastní	k2eAgFnSc4d1	vlastní
postel	postel	k1gFnSc4	postel
<g/>
,	,	kIx,	,
bar	bar	k1gInSc4	bar
<g/>
,	,	kIx,	,
společenskou	společenský	k2eAgFnSc4d1	společenská
místnost	místnost	k1gFnSc4	místnost
nebo	nebo	k8xC	nebo
tělocvičnu	tělocvična	k1gFnSc4	tělocvična
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
letadla	letadlo	k1gNnSc2	letadlo
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
8,2	[number]	k4	8,2
miliardy	miliarda	k4xCgFnSc2	miliarda
Kč	Kč	kA	Kč
<g/>
.	.	kIx.	.
</s>
<s>
Airbus	airbus	k1gInSc1	airbus
investoval	investovat	k5eAaBmAgInS	investovat
do	do	k7c2	do
vývoje	vývoj	k1gInSc2	vývoj
okolo	okolo	k7c2	okolo
deseti	deset	k4xCc2	deset
miliard	miliarda	k4xCgFnPc2	miliarda
eur	euro	k1gNnPc2	euro
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
tyto	tento	k3xDgInPc4	tento
účely	účel	k1gInPc4	účel
musel	muset	k5eAaImAgInS	muset
nechat	nechat	k5eAaPmF	nechat
postavit	postavit	k5eAaPmF	postavit
novou	nový	k2eAgFnSc4d1	nová
dopravní	dopravní	k2eAgFnSc4d1	dopravní
loď	loď	k1gFnSc4	loď
a	a	k8xC	a
vybudovat	vybudovat	k5eAaPmF	vybudovat
další	další	k2eAgFnSc4d1	další
výrobní	výrobní	k2eAgFnSc4d1	výrobní
halu	hala	k1gFnSc4	hala
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
cenu	cena	k1gFnSc4	cena
získal	získat	k5eAaPmAgMnS	získat
větší	veliký	k2eAgInSc4d2	veliký
a	a	k8xC	a
tišší	tichý	k2eAgNnSc4d2	tišší
letadlo	letadlo	k1gNnSc4	letadlo
než	než	k8xS	než
Boeing	boeing	k1gInSc4	boeing
747	[number]	k4	747
s	s	k7c7	s
větším	veliký	k2eAgInSc7d2	veliký
doletem	dolet	k1gInSc7	dolet
a	a	k8xC	a
menší	malý	k2eAgFnSc7d2	menší
spotřebou	spotřeba	k1gFnSc7	spotřeba
paliva	palivo	k1gNnSc2	palivo
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
Boeing	boeing	k1gInSc4	boeing
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
výzvu	výzva	k1gFnSc4	výzva
odpověděla	odpovědět	k5eAaPmAgFnS	odpovědět
projektem	projekt	k1gInSc7	projekt
Boeing	boeing	k1gInSc4	boeing
747	[number]	k4	747
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
uvedení	uvedení	k1gNnSc2	uvedení
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
oproti	oproti	k7c3	oproti
předpokladu	předpoklad	k1gInSc2	předpoklad
nabral	nabrat	k5eAaPmAgInS	nabrat
zpoždění	zpoždění	k1gNnSc4	zpoždění
–	–	k?	–
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
listopadu	listopad	k1gInSc2	listopad
a	a	k8xC	a
prosince	prosinec	k1gInSc2	prosinec
2006	[number]	k4	2006
teprve	teprve	k6eAd1	teprve
končil	končit	k5eAaImAgInS	končit
proces	proces	k1gInSc1	proces
certifikace	certifikace	k1gFnSc2	certifikace
letounu	letoun	k1gInSc2	letoun
po	po	k7c6	po
150	[number]	k4	150
hodinách	hodina	k1gFnPc6	hodina
zkušebních	zkušební	k2eAgMnPc2d1	zkušební
letů	let	k1gInPc2	let
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2007	[number]	k4	2007
byl	být	k5eAaImAgInS	být
předán	předat	k5eAaPmNgInS	předat
první	první	k4xOgInSc1	první
letoun	letoun	k1gInSc1	letoun
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
MSN003	MSN003	k1gFnSc2	MSN003
ke	k	k7c3	k
komerčnímu	komerční	k2eAgNnSc3d1	komerční
užívání	užívání	k1gNnSc3	užívání
koncovému	koncový	k2eAgMnSc3d1	koncový
zákazníkovi	zákazník	k1gMnSc3	zákazník
<g/>
,	,	kIx,	,
společnosti	společnost	k1gFnSc3	společnost
Singapore	Singapor	k1gInSc5	Singapor
Airlines	Airlines	k1gMnSc1	Airlines
<g/>
.	.	kIx.	.
</s>
<s>
Airbus	airbus	k1gInSc1	airbus
oslavil	oslavit	k5eAaPmAgInS	oslavit
15	[number]	k4	15
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
pět	pět	k4xCc1	pět
let	léto	k1gNnPc2	léto
od	od	k7c2	od
dodání	dodání	k1gNnSc2	dodání
prvního	první	k4xOgMnSc2	první
stroje	stroj	k1gInSc2	stroj
A380	A380	k1gFnSc7	A380
společnosti	společnost	k1gFnSc2	společnost
Singapore	Singapor	k1gInSc5	Singapor
Airlines	Airlines	k1gInSc1	Airlines
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2017	[number]	k4	2017
Airbus	airbus	k1gInSc1	airbus
spustil	spustit	k5eAaPmAgInS	spustit
web	web	k1gInSc4	web
iflya	ifly	k1gInSc2	ifly
<g/>
380	[number]	k4	380
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
vyhledáváč	vyhledáváč	k1gInSc4	vyhledáváč
letenek	letenka	k1gFnPc2	letenka
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
letadlem	letadlo	k1gNnSc7	letadlo
Airbus	airbus	k1gInSc1	airbus
A	a	k8xC	a
<g/>
380	[number]	k4	380
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
musel	muset	k5eAaImAgInS	muset
Airbus	airbus	k1gInSc1	airbus
snížit	snížit	k5eAaPmF	snížit
produkci	produkce	k1gFnSc4	produkce
A380	A380	k1gFnSc2	A380
kvůli	kvůli	k7c3	kvůli
nízké	nízký	k2eAgFnSc3d1	nízká
poptávce	poptávka	k1gFnSc3	poptávka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2016	[number]	k4	2016
Airbus	airbus	k1gInSc1	airbus
oznámil	oznámit	k5eAaPmAgInS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
sníží	snížit	k5eAaPmIp3nS	snížit
produkci	produkce	k1gFnSc4	produkce
na	na	k7c4	na
12	[number]	k4	12
letounů	letoun	k1gInPc2	letoun
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
investice	investice	k1gFnSc1	investice
do	do	k7c2	do
vývoje	vývoj	k1gInSc2	vývoj
byla	být	k5eAaImAgFnS	být
25	[number]	k4	25
miliard	miliarda	k4xCgFnPc2	miliarda
Eur	euro	k1gNnPc2	euro
a	a	k8xC	a
Airbus	airbus	k1gInSc1	airbus
začíná	začínat	k5eAaImIp3nS	začínat
pochybovat	pochybovat	k5eAaImF	pochybovat
o	o	k7c6	o
její	její	k3xOp3gFnSc6	její
návratnosti	návratnost	k1gFnSc6	návratnost
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
počítalo	počítat	k5eAaImAgNnS	počítat
s	s	k7c7	s
prodejem	prodej	k1gInSc7	prodej
1200	[number]	k4	1200
letadel	letadlo	k1gNnPc2	letadlo
za	za	k7c4	za
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
9	[number]	k4	9
let	léto	k1gNnPc2	léto
od	od	k7c2	od
zařazení	zařazení	k1gNnSc2	zařazení
do	do	k7c2	do
komerčního	komerční	k2eAgInSc2d1	komerční
provozu	provoz	k1gInSc2	provoz
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2016	[number]	k4	2016
jich	on	k3xPp3gMnPc2	on
létalo	létat	k5eAaImAgNnS	létat
po	po	k7c6	po
světě	svět	k1gInSc6	svět
200	[number]	k4	200
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
dalších	další	k2eAgNnPc2d1	další
126	[number]	k4	126
měly	mít	k5eAaImAgFnP	mít
společnosti	společnost	k1gFnPc1	společnost
objednaných	objednaný	k2eAgMnPc2d1	objednaný
<g/>
.	.	kIx.	.
</s>
<s>
A380-800	A380-800	k4	A380-800
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc1d1	základní
verze	verze	k1gFnSc1	verze
Airbusu	airbus	k1gInSc2	airbus
A380	A380	k1gFnSc2	A380
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jediná	jediný	k2eAgFnSc1d1	jediná
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
létá	létat	k5eAaImIp3nS	létat
na	na	k7c6	na
světě	svět	k1gInSc6	svět
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
všechny	všechen	k3xTgFnPc1	všechen
ostatní	ostatní	k2eAgFnPc1d1	ostatní
navrhované	navrhovaný	k2eAgFnPc1d1	navrhovaná
verze	verze	k1gFnPc1	verze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
Airbus	airbus	k1gInSc4	airbus
plány	plán	k1gInPc4	plán
na	na	k7c6	na
prodloužené	prodloužený	k2eAgFnSc6d1	prodloužená
verzi	verze	k1gFnSc6	verze
A	a	k9	a
<g/>
380	[number]	k4	380
<g/>
-	-	kIx~	-
<g/>
900	[number]	k4	900
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
pojmula	pojmout	k5eAaPmAgFnS	pojmout
více	hodně	k6eAd2	hodně
cestujících	cestující	k1gMnPc2	cestující
než	než	k8xS	než
A	a	k9	a
<g/>
380	[number]	k4	380
<g/>
-	-	kIx~	-
<g/>
800	[number]	k4	800
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
standardní	standardní	k2eAgFnSc6d1	standardní
konfiguraci	konfigurace	k1gFnSc6	konfigurace
tří	tři	k4xCgFnPc2	tři
cestovních	cestovní	k2eAgFnPc2d1	cestovní
tříd	třída	k1gFnPc2	třída
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
kapacitu	kapacita	k1gFnSc4	kapacita
650	[number]	k4	650
cestujících	cestující	k1gMnPc2	cestující
<g/>
,	,	kIx,	,
s	s	k7c7	s
pouze	pouze	k6eAd1	pouze
ekonomickou	ekonomický	k2eAgFnSc7d1	ekonomická
třídou	třída	k1gFnSc7	třída
by	by	kYmCp3nS	by
pojmula	pojmout	k5eAaPmAgFnS	pojmout
900	[number]	k4	900
pasažérů	pasažér	k1gMnPc2	pasažér
<g/>
.	.	kIx.	.
</s>
<s>
Zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
tuto	tento	k3xDgFnSc4	tento
verzi	verze	k1gFnSc4	verze
potvrdily	potvrdit	k5eAaPmAgFnP	potvrdit
společnosti	společnost	k1gFnPc1	společnost
Emirates	Emirates	k1gInSc1	Emirates
<g/>
,	,	kIx,	,
Virgin	Virgin	k1gInSc1	Virgin
Atlantic	Atlantice	k1gFnPc2	Atlantice
<g/>
,	,	kIx,	,
Cathay	Cathaa	k1gFnPc1	Cathaa
Pacific	Pacifice	k1gInPc2	Pacifice
<g/>
,	,	kIx,	,
Air	Air	k1gMnSc2	Air
France	Franc	k1gMnSc2	Franc
<g/>
,	,	kIx,	,
KLM	KLM	kA	KLM
<g/>
,	,	kIx,	,
Lufthansa	Lufthansa	k1gFnSc1	Lufthansa
<g/>
,	,	kIx,	,
Kingfisher	Kingfishra	k1gFnPc2	Kingfishra
Airlines	Airlinesa	k1gFnPc2	Airlinesa
<g/>
,	,	kIx,	,
a	a	k8xC	a
ILFC	ILFC	kA	ILFC
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2010	[number]	k4	2010
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
program	program	k1gInSc1	program
je	být	k5eAaImIp3nS	být
pozastaven	pozastaven	k2eAgInSc1d1	pozastaven
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
se	se	k3xPyFc4	se
nestabilizuje	stabilizovat	k5eNaBmIp3nS	stabilizovat
výroba	výroba	k1gFnSc1	výroba
A	a	k9	a
<g/>
380	[number]	k4	380
<g/>
-	-	kIx~	-
<g/>
800	[number]	k4	800
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
projektu	projekt	k1gInSc2	projekt
A380	A380	k1gFnSc2	A380
Airbus	airbus	k1gInSc1	airbus
počítal	počítat	k5eAaImAgInS	počítat
s	s	k7c7	s
nákladní	nákladní	k2eAgFnSc7d1	nákladní
verzí	verze	k1gFnSc7	verze
A	a	k9	a
<g/>
380	[number]	k4	380
<g/>
F.	F.	kA	F.
Tento	tento	k3xDgInSc4	tento
letoun	letoun	k1gInSc4	letoun
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
třetí	třetí	k4xOgInSc4	třetí
největší	veliký	k2eAgInSc4d3	veliký
nákladní	nákladní	k2eAgInSc4d1	nákladní
letoun	letoun	k1gInSc4	letoun
světa	svět	k1gInSc2	svět
hned	hned	k6eAd1	hned
po	po	k7c4	po
Antonovu	Antonův	k2eAgFnSc4d1	Antonova
An-	An-	k1gFnSc4	An-
<g/>
255	[number]	k4	255
Mrija	Mrij	k1gInSc2	Mrij
a	a	k8xC	a
Boeingu	boeing	k1gInSc2	boeing
747	[number]	k4	747
Dramlifter	Dramliftra	k1gFnPc2	Dramliftra
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládáná	Předpokládáný	k2eAgFnSc1d1	Předpokládáná
nosnost	nosnost	k1gFnSc1	nosnost
byla	být	k5eAaImAgFnS	být
150	[number]	k4	150
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
by	by	kYmCp3nS	by
tak	tak	k6eAd1	tak
lepší	dobrý	k2eAgNnSc1d2	lepší
než	než	k8xS	než
u	u	k7c2	u
konkurenta	konkurent	k1gMnSc4	konkurent
Boeing	boeing	k1gInSc4	boeing
747	[number]	k4	747
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
F	F	kA	F
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
by	by	kYmCp3nS	by
ale	ale	k9	ale
byl	být	k5eAaImAgInS	být
jeho	jeho	k3xOp3gInSc1	jeho
provoz	provoz	k1gInSc1	provoz
dražší	drahý	k2eAgInSc1d2	dražší
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
Airbus	airbus	k1gInSc1	airbus
projekt	projekt	k1gInSc4	projekt
pozastavil	pozastavit	k5eAaPmAgInS	pozastavit
úplně	úplně	k6eAd1	úplně
<g/>
.	.	kIx.	.
</s>
<s>
Zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
tuto	tento	k3xDgFnSc4	tento
verzi	verze	k1gFnSc4	verze
měly	mít	k5eAaImAgFnP	mít
společnosti	společnost	k1gFnPc1	společnost
Emirates	Emirates	k1gInSc1	Emirates
<g/>
,	,	kIx,	,
ILFC	ILFC	kA	ILFC
<g/>
,	,	kIx,	,
FedEx	FedEx	k1gInSc1	FedEx
a	a	k8xC	a
UPS	UPS	kA	UPS
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
byl	být	k5eAaImAgInS	být
oznámen	oznámen	k2eAgInSc1d1	oznámen
vývoj	vývoj	k1gInSc1	vývoj
nové	nový	k2eAgFnSc2d1	nová
verze	verze	k1gFnSc2	verze
A	a	k9	a
<g/>
380	[number]	k4	380
<g/>
neo	neo	k?	neo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nS	by
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
A	a	k9	a
<g/>
320	[number]	k4	320
<g/>
neo	neo	k?	neo
a	a	k8xC	a
A	A	kA	A
<g/>
330	[number]	k4	330
<g/>
neo	neo	k?	neo
zefektivnila	zefektivnit	k5eAaPmAgFnS	zefektivnit
jeho	on	k3xPp3gInSc4	on
provoz	provoz	k1gInSc4	provoz
–	–	k?	–
měla	mít	k5eAaImAgFnS	mít
by	by	kYmCp3nP	by
větší	veliký	k2eAgFnSc4d2	veliký
kapacitu	kapacita	k1gFnSc4	kapacita
<g/>
,	,	kIx,	,
nová	nový	k2eAgNnPc4d1	nové
křídla	křídlo	k1gNnPc4	křídlo
a	a	k8xC	a
motory	motor	k1gInPc4	motor
<g/>
.	.	kIx.	.
</s>
<s>
Dodávky	dodávka	k1gFnPc1	dodávka
k	k	k7c3	k
zákazníkům	zákazník	k1gMnPc3	zákazník
byly	být	k5eAaImAgFnP	být
plánované	plánovaný	k2eAgInPc4d1	plánovaný
na	na	k7c4	na
roky	rok	k1gInPc4	rok
2020	[number]	k4	2020
až	až	k8xS	až
2021	[number]	k4	2021
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tuto	tento	k3xDgFnSc4	tento
verzi	verze	k1gFnSc4	verze
měla	mít	k5eAaImAgFnS	mít
zájem	zájem	k1gInSc4	zájem
hlavně	hlavně	k9	hlavně
společnost	společnost	k1gFnSc1	společnost
Emirates	Emiratesa	k1gFnPc2	Emiratesa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
plány	plán	k1gInPc1	plán
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
tohoto	tento	k3xDgInSc2	tento
modelu	model	k1gInSc2	model
selhaly	selhat	k5eAaPmAgFnP	selhat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2017	[number]	k4	2017
bylo	být	k5eAaImAgNnS	být
potvrzeno	potvrdit	k5eAaPmNgNnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nová	nový	k2eAgFnSc1d1	nová
verze	verze	k1gFnSc1	verze
A	a	k9	a
<g/>
380	[number]	k4	380
<g/>
neo	neo	k?	neo
nebude	být	k5eNaImBp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
letoun	letoun	k1gInSc1	letoun
nevyplatil	vyplatit	k5eNaPmAgInS	vyplatit
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
verzi	verze	k1gFnSc3	verze
žádný	žádný	k3yNgInSc1	žádný
obchodní	obchodní	k2eAgInSc1d1	obchodní
důvod	důvod	k1gInSc1	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pařížském	pařížský	k2eAgInSc6d1	pařížský
aerosalonu	aerosalon	k1gInSc6	aerosalon
2017	[number]	k4	2017
byla	být	k5eAaImAgFnS	být
představena	představit	k5eAaPmNgFnS	představit
nová	nový	k2eAgFnSc1d1	nová
verze	verze	k1gFnSc1	verze
A	a	k9	a
<g/>
380	[number]	k4	380
<g/>
plus	plus	k1gNnPc2	plus
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
úspornější	úsporný	k2eAgFnSc1d2	úspornější
o	o	k7c4	o
13	[number]	k4	13
%	%	kIx~	%
na	na	k7c4	na
jedno	jeden	k4xCgNnSc4	jeden
sedadlo	sedadlo	k1gNnSc4	sedadlo
<g/>
,	,	kIx,	,
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
efektivnějšího	efektivní	k2eAgNnSc2d2	efektivnější
využití	využití	k1gNnSc2	využití
prostoru	prostor	k1gInSc2	prostor
kabiny	kabina	k1gFnSc2	kabina
nabízí	nabízet	k5eAaImIp3nS	nabízet
o	o	k7c4	o
80	[number]	k4	80
sedadel	sedadlo	k1gNnPc2	sedadlo
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
nové	nový	k2eAgFnPc4d1	nová
winglety	wingleta	k1gFnPc4	wingleta
a	a	k8xC	a
upravená	upravený	k2eAgNnPc1d1	upravené
křídla	křídlo	k1gNnPc1	křídlo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
o	o	k7c6	o
4	[number]	k4	4
%	%	kIx~	%
zlepšují	zlepšovat	k5eAaImIp3nP	zlepšovat
efektivitu	efektivita	k1gFnSc4	efektivita
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
vzletová	vzletový	k2eAgFnSc1d1	vzletová
hmotnost	hmotnost	k1gFnSc1	hmotnost
byla	být	k5eAaImAgFnS	být
zvýšena	zvýšit	k5eAaPmNgFnS	zvýšit
o	o	k7c4	o
3	[number]	k4	3
tuny	tuna	k1gFnPc4	tuna
na	na	k7c4	na
celkových	celkový	k2eAgInPc2d1	celkový
578	[number]	k4	578
t.	t.	k?	t.
Zájem	zájem	k1gInSc1	zájem
projevila	projevit	k5eAaPmAgFnS	projevit
společnost	společnost	k1gFnSc4	společnost
Emirates	Emiratesa	k1gFnPc2	Emiratesa
<g/>
,	,	kIx,	,
na	na	k7c4	na
jejíž	jejíž	k3xOyRp3gInSc4	jejíž
požadavek	požadavek	k1gInSc4	požadavek
i	i	k8xC	i
tato	tento	k3xDgFnSc1	tento
verze	verze	k1gFnSc1	verze
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k8xC	a
<g/>
380	[number]	k4	380
<g/>
plus	plus	k1gNnPc6	plus
bude	být	k5eAaImBp3nS	být
dostupná	dostupný	k2eAgFnSc1d1	dostupná
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2020	[number]	k4	2020
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2010	[number]	k4	2010
explodoval	explodovat	k5eAaBmAgInS	explodovat
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgFnPc2	čtyři
motorů	motor	k1gInPc2	motor
tohoto	tento	k3xDgNnSc2	tento
letadla	letadlo	k1gNnSc2	letadlo
společnosti	společnost	k1gFnSc2	společnost
Qantas	Qantasa	k1gFnPc2	Qantasa
<g/>
.	.	kIx.	.
</s>
<s>
Příčinou	příčina	k1gFnSc7	příčina
havárie	havárie	k1gFnSc2	havárie
byla	být	k5eAaImAgFnS	být
prasklá	prasklý	k2eAgFnSc1d1	prasklá
trubice	trubice	k1gFnSc1	trubice
v	v	k7c6	v
motoru	motor	k1gInSc6	motor
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
následně	následně	k6eAd1	následně
vytékal	vytékat	k5eAaImAgInS	vytékat
olej	olej	k1gInSc1	olej
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Ten	ten	k3xDgMnSc1	ten
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
teploty	teplota	k1gFnPc4	teplota
přes	přes	k7c4	přes
1000	[number]	k4	1000
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
vznítil	vznítit	k5eAaPmAgInS	vznítit
<g/>
.	.	kIx.	.
</s>
<s>
Oheň	oheň	k1gInSc1	oheň
zničil	zničit	k5eAaPmAgInS	zničit
hnací	hnací	k2eAgFnSc4d1	hnací
hřídel	hřídel	k1gFnSc4	hřídel
motorů	motor	k1gInPc2	motor
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
nekontrolovatelné	kontrolovatelný	k2eNgNnSc1d1	nekontrolovatelné
roztočení	roztočení	k1gNnSc1	roztočení
kotouče	kotouč	k1gInSc2	kotouč
turbíny	turbína	k1gFnSc2	turbína
a	a	k8xC	a
následnou	následný	k2eAgFnSc4d1	následná
explozi	exploze	k1gFnSc4	exploze
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
protrhla	protrhnout	k5eAaPmAgFnS	protrhnout
křídlo	křídlo	k1gNnSc4	křídlo
a	a	k8xC	a
palivovou	palivový	k2eAgFnSc4d1	palivová
nádrž	nádrž	k1gFnSc4	nádrž
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yQgFnSc2	který
začalo	začít	k5eAaPmAgNnS	začít
unikat	unikat	k5eAaImF	unikat
palivo	palivo	k1gNnSc4	palivo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
bezpečném	bezpečný	k2eAgNnSc6d1	bezpečné
přistání	přistání	k1gNnSc6	přistání
v	v	k7c6	v
Singapuru	Singapur	k1gInSc6	Singapur
zde	zde	k6eAd1	zde
narazili	narazit	k5eAaPmAgMnP	narazit
na	na	k7c4	na
další	další	k2eAgInSc4d1	další
problém	problém	k1gInSc4	problém
<g/>
.	.	kIx.	.
</s>
<s>
Exploze	exploze	k1gFnSc1	exploze
také	také	k9	také
zničila	zničit	k5eAaPmAgFnS	zničit
kabely	kabela	k1gFnPc4	kabela
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k2eAgNnSc4d1	vedoucí
k	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
motoru	motor	k1gInSc3	motor
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
jej	on	k3xPp3gMnSc4	on
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
vypnout	vypnout	k5eAaPmF	vypnout
<g/>
;	;	kIx,	;
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
jej	on	k3xPp3gMnSc4	on
hasiči	hasič	k1gMnPc1	hasič
snažili	snažit	k5eAaImAgMnP	snažit
zastavit	zastavit	k5eAaPmF	zastavit
pomocí	pomocí	k7c2	pomocí
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Motory	motor	k1gInPc1	motor
jsou	být	k5eAaImIp3nP	být
stavěny	stavit	k5eAaImNgInP	stavit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vydržely	vydržet	k5eAaPmAgFnP	vydržet
obrovské	obrovský	k2eAgNnSc4d1	obrovské
množství	množství	k1gNnSc4	množství
vody	voda	k1gFnSc2	voda
při	při	k7c6	při
průletu	průlet	k1gInSc6	průlet
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
prudkém	prudký	k2eAgInSc6d1	prudký
dešti	dešť	k1gInSc6	dešť
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
hodiny	hodina	k1gFnPc1	hodina
po	po	k7c6	po
přistání	přistání	k1gNnSc6	přistání
se	se	k3xPyFc4	se
však	však	k9	však
konečně	konečně	k6eAd1	konečně
motor	motor	k1gInSc1	motor
za	za	k7c7	za
pomocí	pomoc	k1gFnSc7	pomoc
hasební	hasební	k2eAgFnSc2d1	hasební
pěny	pěna	k1gFnSc2	pěna
zastavil	zastavit	k5eAaPmAgMnS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
nebyl	být	k5eNaImAgMnS	být
zraněn	zranit	k5eAaPmNgMnS	zranit
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
mrtev	mrtev	k2eAgMnSc1d1	mrtev
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
prosinci	prosinec	k1gInSc3	prosinec
2016	[number]	k4	2016
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
prozovozu	prozovoz	k1gInSc6	prozovoz
200	[number]	k4	200
strojů	stroj	k1gInPc2	stroj
u	u	k7c2	u
13	[number]	k4	13
společností	společnost	k1gFnPc2	společnost
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
x	x	k?	x
Singapore	Singapor	k1gInSc5	Singapor
Airlines	Airlinesa	k1gFnPc2	Airlinesa
Od	od	k7c2	od
25	[number]	k4	25
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2007	[number]	k4	2007
87	[number]	k4	87
<g/>
x	x	k?	x
Emirates	Emiratesa	k1gFnPc2	Emiratesa
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2008	[number]	k4	2008
12	[number]	k4	12
<g/>
x	x	k?	x
Qantas	Qantasa	k1gFnPc2	Qantasa
Od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2008	[number]	k4	2008
10	[number]	k4	10
<g/>
x	x	k?	x
Air	Air	k1gMnSc4	Air
France	Franc	k1gMnSc4	Franc
Od	od	k7c2	od
<g />
.	.	kIx.	.
</s>
<s>
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2009	[number]	k4	2009
14	[number]	k4	14
<g/>
x	x	k?	x
Lufthansa	Lufthansa	k1gFnSc1	Lufthansa
Od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2010	[number]	k4	2010
10	[number]	k4	10
<g/>
x	x	k?	x
Korean	Korean	k1gMnSc1	Korean
Air	Air	k1gMnSc1	Air
Od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2011	[number]	k4	2011
5	[number]	k4	5
<g/>
x	x	k?	x
China	China	k1gFnSc1	China
Southern	Southern	k1gMnSc1	Southern
Airlines	Airlines	k1gMnSc1	Airlines
Od	od	k7c2	od
17	[number]	k4	17
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
6	[number]	k4	6
<g/>
x	x	k?	x
Malaysia	Malaysium	k1gNnSc2	Malaysium
Airlines	Airlinesa	k1gFnPc2	Airlinesa
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2012	[number]	k4	2012
6	[number]	k4	6
<g />
.	.	kIx.	.
</s>
<s>
<g/>
x	x	k?	x
Thai	Tha	k1gFnPc1	Tha
Airways	Airwaysa	k1gFnPc2	Airwaysa
Od	od	k7c2	od
6	[number]	k4	6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
x	x	k?	x
British	British	k1gInSc1	British
Airways	Airwaysa	k1gFnPc2	Airwaysa
Od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
x	x	k?	x
Asiana	Asiana	k1gFnSc1	Asiana
Airlines	Airlinesa	k1gFnPc2	Airlinesa
Od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2014	[number]	k4	2014
6	[number]	k4	6
<g/>
x	x	k?	x
Qatar	Qatar	k1gInSc1	Qatar
Airways	Airwaysa	k1gFnPc2	Airwaysa
Od	od	k7c2	od
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2014	[number]	k4	2014
8	[number]	k4	8
<g/>
x	x	k?	x
Etihad	Etihad	k1gInSc1	Etihad
Airways	Airwaysa	k1gFnPc2	Airwaysa
Od	od	k7c2	od
27	[number]	k4	27
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2014	[number]	k4	2014
</s>
