<s>
Airbus	airbus	k1gInSc1	airbus
A380	A380	k1gFnSc2	A380
je	být	k5eAaImIp3nS	být
dvoupatrový	dvoupatrový	k2eAgInSc1d1	dvoupatrový
čtyřmotorový	čtyřmotorový	k2eAgInSc1d1	čtyřmotorový
proudový	proudový	k2eAgInSc1d1	proudový
širokotrupý	širokotrupý	k2eAgInSc1d1	širokotrupý
letoun	letoun	k1gInSc1	letoun
vyráběný	vyráběný	k2eAgInSc1d1	vyráběný
evropskou	evropský	k2eAgFnSc7d1	Evropská
společností	společnost	k1gFnSc7	společnost
Airbus	airbus	k1gInSc1	airbus
S.	S.	kA	S.
<g/>
A.S.	A.S.	k1gMnPc2	A.S.
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
největším	veliký	k2eAgNnSc7d3	veliký
dopravním	dopravní	k2eAgNnSc7d1	dopravní
letadlem	letadlo	k1gNnSc7	letadlo
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
