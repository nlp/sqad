<s>
Boubel	boubel	k1gInSc1	boubel
(	(	kIx(	(
<g/>
též	též	k9	též
larvocysta	larvocysta	k1gFnSc1	larvocysta
<g/>
,	,	kIx,	,
uher	uher	k1gInSc1	uher
<g/>
,	,	kIx,	,
odborně	odborně	k6eAd1	odborně
metacestod	metacestod	k1gInSc1	metacestod
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
vývojové	vývojový	k2eAgNnSc4d1	vývojové
larvální	larvální	k2eAgNnSc4d1	larvální
stadium	stadium	k1gNnSc4	stadium
tasemnic	tasemnice	k1gFnPc2	tasemnice
v	v	k7c6	v
mezihostiteli	mezihostitel	k1gMnSc6	mezihostitel
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
dospělé	dospělý	k2eAgFnPc1d1	dospělá
tasemnice	tasemnice	k1gFnPc1	tasemnice
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
střevě	střevo	k1gNnSc6	střevo
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gMnSc4	jejich
lárvální	lárvální	k2eAgNnPc1d1	lárvální
stádia	stádium	k1gNnPc1	stádium
(	(	kIx(	(
<g/>
boubele	boubel	k1gInPc1	boubel
<g/>
)	)	kIx)	)
v	v	k7c6	v
mezihostitelích	mezihostitel	k1gMnPc6	mezihostitel
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
nacházet	nacházet	k5eAaImF	nacházet
v	v	k7c6	v
různých	různý	k2eAgInPc6d1	různý
orgánových	orgánový	k2eAgInPc6d1	orgánový
systémech	systém	k1gInPc6	systém
–	–	k?	–
svalovina	svalovina	k1gFnSc1	svalovina
<g/>
,	,	kIx,	,
játra	játra	k1gNnPc1	játra
<g/>
,	,	kIx,	,
smyslové	smyslový	k2eAgInPc1d1	smyslový
orgány	orgán	k1gInPc1	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Boubele	boubel	k1gInPc1	boubel
vznikají	vznikat	k5eAaImIp3nP	vznikat
pozřením	pozření	k1gNnPc3	pozření
vajíček	vajíčko	k1gNnPc2	vajíčko
tasemnic	tasemnice	k1gFnPc2	tasemnice
vnímavým	vnímavý	k2eAgMnSc7d1	vnímavý
mezihostitelem	mezihostitel	k1gMnSc7	mezihostitel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
trávicím	trávicí	k2eAgInSc6d1	trávicí
traktu	trakt	k1gInSc6	trakt
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
narušení	narušení	k1gNnSc3	narušení
obalové	obalový	k2eAgFnSc2d1	obalová
části	část	k1gFnSc2	část
vajíčka	vajíčko	k1gNnSc2	vajíčko
(	(	kIx(	(
<g/>
embryoforu	embryofor	k1gInSc2	embryofor
<g/>
)	)	kIx)	)
a	a	k8xC	a
k	k	k7c3	k
uvolnění	uvolnění	k1gNnSc3	uvolnění
larvy	larva	k1gFnSc2	larva
s	s	k7c7	s
háčky	háček	k1gInPc7	háček
zvané	zvaný	k2eAgFnPc1d1	zvaná
onkosféra	onkosféra	k1gFnSc1	onkosféra
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
penetruje	penetrovat	k5eAaImIp3nS	penetrovat
střevní	střevní	k2eAgFnSc4d1	střevní
stěnu	stěna	k1gFnSc4	stěna
a	a	k8xC	a
krevním	krevní	k2eAgNnSc7d1	krevní
obehěm	obehě	k1gNnSc7	obehě
je	být	k5eAaImIp3nS	být
přenesena	přenést	k5eAaPmNgFnS	přenést
do	do	k7c2	do
různých	různý	k2eAgInPc2d1	různý
orgánů	orgán	k1gInPc2	orgán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
uchytí	uchytit	k5eAaPmIp3nS	uchytit
se	s	k7c7	s
přemění	přeměnit	k5eAaPmIp3nP	přeměnit
v	v	k7c6	v
samotnou	samotný	k2eAgFnSc4d1	samotná
larvocystu-boubel	larvocystuoubet	k5eAaBmAgInS	larvocystu-boubet
<g/>
.	.	kIx.	.
</s>
<s>
Larvocysta	Larvocysta	k1gFnSc1	Larvocysta
poté	poté	k6eAd1	poté
roste	růst	k5eAaImIp3nS	růst
<g/>
,	,	kIx,	,
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
uvnitř	uvnitř	k6eAd1	uvnitř
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
zárodek	zárodek	k1gInSc1	zárodek
skolexu	skolex	k1gInSc2	skolex
tasemnice	tasemnice	k1gFnSc2	tasemnice
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
tasemnice	tasemnice	k1gFnSc2	tasemnice
rodu	rod	k1gInSc2	rod
Echinococcus	Echinococcus	k1gInSc1	Echinococcus
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
nepohlavnímu	pohlavní	k2eNgNnSc3d1	nepohlavní
rozmnožování	rozmnožování	k1gNnSc3	rozmnožování
a	a	k8xC	a
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
larvocystě	larvocysta	k1gFnSc6	larvocysta
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
mohou	moct	k5eAaImIp3nP	moct
vyvinout	vyvinout	k5eAaPmF	vyvinout
stovky	stovka	k1gFnPc4	stovka
až	až	k6eAd1	až
tisíce	tisíc	k4xCgInSc2	tisíc
zárodků	zárodek	k1gInPc2	zárodek
skolexů	skolex	k1gInPc2	skolex
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ostatních	ostatní	k2eAgFnPc2d1	ostatní
tasemnic	tasemnice	k1gFnPc2	tasemnice
se	se	k3xPyFc4	se
vyvine	vyvinout	k5eAaPmIp3nS	vyvinout
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgInSc4	jeden
skolex	skolex	k1gInSc4	skolex
<g/>
.	.	kIx.	.
</s>
<s>
Larvocysty	Larvocysta	k1gFnPc1	Larvocysta
nemigrují	migrovat	k5eNaImIp3nP	migrovat
tkáněmi	tkáň	k1gFnPc7	tkáň
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
aktivně	aktivně	k6eAd1	aktivně
neničí	ničit	k5eNaImIp3nP	ničit
okolní	okolní	k2eAgFnPc4d1	okolní
tkáně	tkáň	k1gFnPc4	tkáň
<g/>
.	.	kIx.	.
</s>
<s>
Larvocysty	Larvocysta	k1gFnPc1	Larvocysta
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
jen	jen	k9	jen
čekají	čekat	k5eAaImIp3nP	čekat
až	až	k6eAd1	až
je	být	k5eAaImIp3nS	být
mezihostitel	mezihostitel	k1gMnSc1	mezihostitel
pozřen	pozřít	k5eAaPmNgMnS	pozřít
konečným	konečný	k2eAgMnSc7d1	konečný
hostitelem	hostitel	k1gMnSc7	hostitel
a	a	k8xC	a
skolex	skolex	k1gInSc4	skolex
z	z	k7c2	z
boubele	boubel	k1gInSc2	boubel
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
uchytit	uchytit	k5eAaPmF	uchytit
ve	v	k7c6	v
střevě	střevo	k1gNnSc6	střevo
jako	jako	k8xC	jako
dospělá	dospělý	k2eAgFnSc1d1	dospělá
tasemnice	tasemnice	k1gFnSc1	tasemnice
<g/>
.	.	kIx.	.
</s>
<s>
Životnost	životnost	k1gFnSc1	životnost
larvocyst	larvocyst	k1gFnSc1	larvocyst
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
značná	značný	k2eAgFnSc1d1	značná
<g/>
,	,	kIx,	,
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
i	i	k9	i
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Larvocysty	Larvocysta	k1gFnPc1	Larvocysta
v	v	k7c6	v
orgánech	orgán	k1gInPc6	orgán
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
tkáně	tkáň	k1gFnPc4	tkáň
mechanicky	mechanicky	k6eAd1	mechanicky
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
ovlivňovat	ovlivňovat	k5eAaImF	ovlivňovat
i	i	k9	i
jejich	jejich	k3xOp3gFnPc4	jejich
funkce	funkce	k1gFnPc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Hostitelský	hostitelský	k2eAgInSc1d1	hostitelský
organismus	organismus	k1gInSc1	organismus
reaguje	reagovat	k5eAaBmIp3nS	reagovat
na	na	k7c4	na
přítomnost	přítomnost	k1gFnSc4	přítomnost
larvocyst	larvocyst	k5eAaPmF	larvocyst
imunitní	imunitní	k2eAgFnSc7d1	imunitní
reakcí	reakce	k1gFnSc7	reakce
<g/>
.	.	kIx.	.
hydatida	hydatida	k1gFnSc1	hydatida
–	–	k?	–
larva	larva	k1gFnSc1	larva
měchožila	měchožil	k1gMnSc2	měchožil
zhoubného	zhoubný	k2eAgMnSc2d1	zhoubný
(	(	kIx(	(
<g/>
Echinococcus	Echinococcus	k1gInSc1	Echinococcus
granulosus	granulosus	k1gInSc1	granulosus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
definitivní	definitivní	k2eAgMnSc1d1	definitivní
hostitel	hostitel	k1gMnSc1	hostitel
je	být	k5eAaImIp3nS	být
pes	pes	k1gMnSc1	pes
<g/>
,	,	kIx,	,
mezihostitel	mezihostitel	k1gMnSc1	mezihostitel
je	být	k5eAaImIp3nS	být
kopytník	kopytník	k1gInSc4	kopytník
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
člověk	člověk	k1gMnSc1	člověk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hydatida	hydatida	k1gFnSc1	hydatida
je	být	k5eAaImIp3nS	být
měchýřkovitý	měchýřkovitý	k2eAgInSc4d1	měchýřkovitý
útvar	útvar	k1gInSc4	útvar
vyplněný	vyplněný	k2eAgInSc4d1	vyplněný
skolexy	skolex	k1gInPc4	skolex
<g/>
,	,	kIx,	,
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
<g />
.	.	kIx.	.
</s>
<s>
může	moct	k5eAaImIp3nS	moct
dorůstat	dorůstat	k5eAaImF	dorůstat
až	až	k9	až
velikosti	velikost	k1gFnSc3	velikost
dětské	dětský	k2eAgFnSc2d1	dětská
hlavičky	hlavička	k1gFnSc2	hlavička
<g/>
;	;	kIx,	;
cysticerkus	cysticerkus	k1gInSc1	cysticerkus
–	–	k?	–
larva	larva	k1gFnSc1	larva
některých	některý	k3yIgFnPc2	některý
tasemnic	tasemnice	k1gFnPc2	tasemnice
rodu	rod	k1gInSc2	rod
Taenia	Taenium	k1gNnSc2	Taenium
<g/>
:	:	kIx,	:
cysticercus	cysticercus	k1gInSc1	cysticercus
bovis	bovis	k1gFnSc2	bovis
–	–	k?	–
larva	larva	k1gFnSc1	larva
tasemnice	tasemnice	k1gFnSc2	tasemnice
bezbranné	bezbranný	k2eAgFnSc2d1	bezbranná
(	(	kIx(	(
<g/>
Taenia	Taenium	k1gNnSc2	Taenium
saginata	saginata	k1gFnSc1	saginata
<g/>
)	)	kIx)	)
=	=	kIx~	=
"	"	kIx"	"
<g/>
uher	uher	k1gInSc4	uher
hovězí	hovězí	k2eAgInSc4d1	hovězí
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
definitivní	definitivní	k2eAgMnSc1d1	definitivní
hostitel	hostitel	k1gMnSc1	hostitel
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
mezihostitel	mezihostitel	k1gMnSc1	mezihostitel
je	být	k5eAaImIp3nS	být
hovězí	hovězí	k2eAgInSc4d1	hovězí
dobytek	dobytek	k1gInSc4	dobytek
<g/>
;	;	kIx,	;
<g />
.	.	kIx.	.
</s>
<s>
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
srdci	srdce	k1gNnSc6	srdce
a	a	k8xC	a
žvýkacích	žvýkací	k2eAgInPc6d1	žvýkací
svalech	sval	k1gInPc6	sval
skotu	skot	k1gInSc2	skot
<g/>
;	;	kIx,	;
přítomnost	přítomnost	k1gFnSc1	přítomnost
larev	larva	k1gFnPc2	larva
v	v	k7c6	v
mase	maso	k1gNnSc6	maso
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
též	též	k9	též
uhřivost	uhřivost	k1gFnSc1	uhřivost
cysticercus	cysticercus	k1gInSc1	cysticercus
cellulosae	cellulosae	k1gFnSc1	cellulosae
–	–	k?	–
larva	larva	k1gFnSc1	larva
tasemnice	tasemnice	k1gFnSc2	tasemnice
dlouhočlenné	dlouhočlenný	k2eAgFnSc2d1	dlouhočlenná
(	(	kIx(	(
<g/>
Taenia	Taenium	k1gNnSc2	Taenium
solium	solium	k1gNnSc4	solium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
definitivní	definitivní	k2eAgMnSc1d1	definitivní
hostitel	hostitel	k1gMnSc1	hostitel
je	být	k5eAaImIp3nS	být
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
mezihostitel	mezihostitel	k1gMnSc1	mezihostitel
je	být	k5eAaImIp3nS	být
prase	prase	k1gNnSc4	prase
<g/>
;	;	kIx,	;
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
ve	v	k7c6	v
svalovině	svalovina	k1gFnSc6	svalovina
jazyka	jazyk	k1gInSc2	jazyk
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
jícnu	jícen	k1gInSc2	jícen
<g/>
,	,	kIx,	,
srdce	srdce	k1gNnSc2	srdce
<g/>
,	,	kIx,	,
bránice	bránice	k1gFnSc2	bránice
a	a	k8xC	a
na	na	k7c6	na
vnitřní	vnitřní	k2eAgFnSc6d1	vnitřní
svalovině	svalovina	k1gFnSc6	svalovina
břišní	břišní	k2eAgInSc1d1	břišní
coenurus	coenurus	k1gInSc1	coenurus
–	–	k?	–
larva	larva	k1gFnSc1	larva
tasemnice	tasemnice	k1gFnSc2	tasemnice
vrtohlavé	vrtohlavý	k2eAgFnSc2d1	vrtohlavá
(	(	kIx(	(
<g/>
Taenia	Taenium	k1gNnPc1	Taenium
multiceps	multicepsa	k1gFnPc2	multicepsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
definitivní	definitivní	k2eAgMnSc1d1	definitivní
hostitel	hostitel	k1gMnSc1	hostitel
je	být	k5eAaImIp3nS	být
zástupce	zástupce	k1gMnSc1	zástupce
psovitých	psovití	k1gMnPc2	psovití
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
vlk	vlk	k1gMnSc1	vlk
<g/>
,	,	kIx,	,
mezihostitel	mezihostitel	k1gMnSc1	mezihostitel
je	být	k5eAaImIp3nS	být
ovce	ovce	k1gFnPc4	ovce
<g/>
;	;	kIx,	;
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
se	se	k3xPyFc4	se
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
tkáních	tkáň	k1gFnPc6	tkáň
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
hlavně	hlavně	k9	hlavně
pak	pak	k6eAd1	pak
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
utlačuje	utlačovat	k5eAaImIp3nS	utlačovat
mozkovou	mozkový	k2eAgFnSc4d1	mozková
tkáň	tkáň	k1gFnSc4	tkáň
a	a	k8xC	a
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
neurologické	neurologický	k2eAgInPc4d1	neurologický
příznaky	příznak	k1gInPc4	příznak
–	–	k?	–
"	"	kIx"	"
<g/>
vrtohlavá	vrtohlavý	k2eAgFnSc1d1	vrtohlavá
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
potácející	potácející	k2eAgFnSc1d1	potácející
se	s	k7c7	s
<g/>
,	,	kIx,	,
poskakující	poskakující	k2eAgNnSc1d1	poskakující
a	a	k8xC	a
nepřiměřeně	přiměřeně	k6eNd1	přiměřeně
reagující	reagující	k2eAgFnSc1d1	reagující
ovce	ovce	k1gFnSc1	ovce
<g/>
;	;	kIx,	;
alveokok	alveokok	k1gInSc1	alveokok
–	–	k?	–
larva	larva	k1gFnSc1	larva
měchožila	měchožil	k1gMnSc2	měchožil
bublinatého	bublinatý	k2eAgMnSc2d1	bublinatý
(	(	kIx(	(
<g/>
Echinococcus	Echinococcus	k1gInSc1	Echinococcus
multiocularis	multiocularis	k1gFnSc2	multiocularis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
definitivní	definitivní	k2eAgMnSc1d1	definitivní
hostitel	hostitel	k1gMnSc1	hostitel
je	být	k5eAaImIp3nS	být
liška	liška	k1gFnSc1	liška
<g/>
,	,	kIx,	,
mezihostitel	mezihostitel	k1gMnSc1	mezihostitel
je	být	k5eAaImIp3nS	být
hlodavec	hlodavec	k1gMnSc1	hlodavec
<g/>
;	;	kIx,	;
cysticerkoid	cysticerkoid	k1gInSc1	cysticerkoid
–	–	k?	–
larva	larva	k1gFnSc1	larva
tasemnic	tasemnice	k1gFnPc2	tasemnice
u	u	k7c2	u
hmyzu	hmyz	k1gInSc2	hmyz
a	a	k8xC	a
roztočů	roztoč	k1gMnPc2	roztoč
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Hymenolepis	Hymenolepis	k1gFnSc1	Hymenolepis
<g/>
)	)	kIx)	)
strobilocerkus	strobilocerkus	k1gInSc1	strobilocerkus
–	–	k?	–
ve	v	k7c6	v
skolexu	skolex	k1gInSc6	skolex
se	se	k3xPyFc4	se
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
jedna	jeden	k4xCgFnSc1	jeden
strobila	strobit	k5eAaPmAgFnS	strobit
tetrathyridium	tetrathyridium	k1gNnSc4	tetrathyridium
–	–	k?	–
za	za	k7c7	za
scolexem	scolex	k1gInSc7	scolex
ocáskovitý	ocáskovitý	k2eAgInSc1d1	ocáskovitý
přívěsek	přívěsek	k1gInSc1	přívěsek
<g/>
,	,	kIx,	,
nemá	mít	k5eNaImIp3nS	mít
články	článek	k1gInPc4	článek
tasemnice	tasemnice	k1gFnSc2	tasemnice
tasemnice	tasemnice	k1gFnSc2	tasemnice
dlouhočlenná	dlouhočlenný	k2eAgFnSc1d1	dlouhočlenná
měchožil	měchožil	k1gMnSc1	měchožil
zhoubný	zhoubný	k2eAgInSc1d1	zhoubný
</s>
