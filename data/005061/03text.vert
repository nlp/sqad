<s>
Java	Java	k1gFnSc1	Java
(	(	kIx(	(
<g/>
výslovnost	výslovnost	k1gFnSc1	výslovnost
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
IPA	IPA	kA	IPA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
objektově	objektově	k6eAd1	objektově
orientovaný	orientovaný	k2eAgInSc1d1	orientovaný
programovací	programovací	k2eAgInSc1d1	programovací
jazyk	jazyk	k1gInSc1	jazyk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
firma	firma	k1gFnSc1	firma
Sun	Sun	kA	Sun
Microsystems	Microsystems	k1gInSc4	Microsystems
a	a	k8xC	a
představila	představit	k5eAaPmAgFnS	představit
23	[number]	k4	23
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejpoužívanějších	používaný	k2eAgInPc2d3	nejpoužívanější
programovacích	programovací	k2eAgInPc2d1	programovací
jazyků	jazyk	k1gInPc2	jazyk
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
TIOBE	TIOBE	kA	TIOBE
indexu	index	k1gInSc2	index
je	být	k5eAaImIp3nS	být
Java	Java	k1gFnSc1	Java
nejpopulárnější	populární	k2eAgFnSc2d3	nejpopulárnější
programovací	programovací	k2eAgInSc4d1	programovací
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Díky	dík	k1gInPc1	dík
své	svůj	k3xOyFgFnSc2	svůj
přenositelnosti	přenositelnost	k1gFnSc2	přenositelnost
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
pro	pro	k7c4	pro
programy	program	k1gInPc4	program
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
různých	různý	k2eAgInPc6d1	různý
systémech	systém	k1gInPc6	systém
počínaje	počínaje	k7c7	počínaje
čipovými	čipový	k2eAgFnPc7d1	čipová
kartami	karta	k1gFnPc7	karta
(	(	kIx(	(
<g/>
platforma	platforma	k1gFnSc1	platforma
JavaCard	JavaCard	k1gMnSc1	JavaCard
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
mobilní	mobilní	k2eAgInPc4d1	mobilní
telefony	telefon	k1gInPc4	telefon
a	a	k8xC	a
různá	různý	k2eAgNnPc4d1	různé
zabudovaná	zabudovaný	k2eAgNnPc4d1	zabudované
zařízení	zařízení	k1gNnPc4	zařízení
(	(	kIx(	(
<g/>
platforma	platforma	k1gFnSc1	platforma
Java	Java	k1gMnSc1	Java
ME	ME	kA	ME
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aplikace	aplikace	k1gFnSc1	aplikace
pro	pro	k7c4	pro
desktopové	desktopový	k2eAgInPc4d1	desktopový
počítače	počítač	k1gInPc4	počítač
(	(	kIx(	(
<g/>
platforma	platforma	k1gFnSc1	platforma
Java	Java	k1gFnSc1	Java
SE	se	k3xPyFc4	se
<g/>
)	)	kIx)	)
až	až	k9	až
po	po	k7c4	po
rozsáhlé	rozsáhlý	k2eAgInPc4d1	rozsáhlý
distribuované	distribuovaný	k2eAgInPc4d1	distribuovaný
systémy	systém	k1gInPc4	systém
pracující	pracující	k2eAgInPc4d1	pracující
na	na	k7c6	na
řadě	řada	k1gFnSc6	řada
spolupracujících	spolupracující	k2eAgMnPc2d1	spolupracující
počítačů	počítač	k1gMnPc2	počítač
rozprostřené	rozprostřený	k2eAgInPc1d1	rozprostřený
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
platforma	platforma	k1gFnSc1	platforma
Java	Java	k1gMnSc1	Java
EE	EE	kA	EE
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
technologie	technologie	k1gFnPc1	technologie
se	se	k3xPyFc4	se
jako	jako	k9	jako
celek	celek	k1gInSc4	celek
nazývají	nazývat	k5eAaImIp3nP	nazývat
platforma	platforma	k1gFnSc1	platforma
Java	Java	k1gFnSc1	Java
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2007	[number]	k4	2007
Sun	sun	k1gInSc1	sun
uvolnil	uvolnit	k5eAaPmAgInS	uvolnit
zdrojové	zdrojový	k2eAgInPc4d1	zdrojový
kódy	kód	k1gInPc4	kód
Javy	Java	k1gFnSc2	Java
(	(	kIx(	(
<g/>
cca	cca	kA	cca
2,5	[number]	k4	2,5
miliónů	milión	k4xCgInPc2	milión
řádků	řádek	k1gInPc2	řádek
kódu	kód	k1gInSc2	kód
<g/>
)	)	kIx)	)
a	a	k8xC	a
Java	Java	k1gFnSc1	Java
bude	být	k5eAaImBp3nS	být
dále	daleko	k6eAd2	daleko
vyvíjena	vyvíjet	k5eAaImNgFnS	vyvíjet
jako	jako	k8xC	jako
open	open	k1gInSc1	open
source	sourec	k1gInSc2	sourec
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vývoji	vývoj	k1gInSc6	vývoj
jazyku	jazyk	k1gInSc6	jazyk
Java	Javus	k1gMnSc2	Javus
se	se	k3xPyFc4	se
vývojáři	vývojář	k1gMnPc7	vývojář
drželi	držet	k5eAaImAgMnP	držet
pěti	pět	k4xCc2	pět
hlavních	hlavní	k2eAgInPc2d1	hlavní
principů	princip	k1gInPc2	princip
<g/>
:	:	kIx,	:
Mělo	mít	k5eAaImAgNnS	mít
by	by	kYmCp3nP	by
to	ten	k3xDgNnSc1	ten
být	být	k5eAaImF	být
"	"	kIx"	"
<g/>
jednoduché	jednoduchý	k2eAgFnPc4d1	jednoduchá
<g/>
,	,	kIx,	,
objektově	objektově	k6eAd1	objektově
orientované	orientovaný	k2eAgNnSc1d1	orientované
a	a	k8xC	a
povědomé	povědomý	k2eAgNnSc1d1	povědomé
<g/>
"	"	kIx"	"
Mělo	mít	k5eAaImAgNnS	mít
by	by	kYmCp3nP	by
to	ten	k3xDgNnSc1	ten
být	být	k5eAaImF	být
"	"	kIx"	"
<g/>
robustní	robustní	k2eAgMnSc1d1	robustní
a	a	k8xC	a
bezpečné	bezpečný	k2eAgNnSc1d1	bezpečné
<g/>
"	"	kIx"	"
Mělo	mít	k5eAaImAgNnS	mít
by	by	kYmCp3nP	by
to	ten	k3xDgNnSc1	ten
být	být	k5eAaImF	být
"	"	kIx"	"
<g/>
nezávislé	závislý	k2eNgInPc4d1	nezávislý
na	na	k7c6	na
architektuře	architektura	k1gFnSc6	architektura
a	a	k8xC	a
přenositelné	přenositelný	k2eAgNnSc1d1	přenositelné
<g/>
"	"	kIx"	"
Mělo	mít	k5eAaImAgNnS	mít
by	by	kYmCp3nP	by
to	ten	k3xDgNnSc1	ten
být	být	k5eAaImF	být
"	"	kIx"	"
<g/>
výkonné	výkonný	k2eAgInPc4d1	výkonný
<g/>
"	"	kIx"	"
Mělo	mít	k5eAaImAgNnS	mít
by	by	kYmCp3nP	by
to	ten	k3xDgNnSc1	ten
být	být	k5eAaImF	být
"	"	kIx"	"
<g/>
interpretované	interpretovaný	k2eAgFnPc4d1	interpretovaná
<g/>
,	,	kIx,	,
vícevláknové	vícevláknový	k2eAgFnPc4d1	vícevláknová
a	a	k8xC	a
dynamické	dynamický	k2eAgFnPc4d1	dynamická
<g/>
"	"	kIx"	"
jednoduchý	jednoduchý	k2eAgInSc4d1	jednoduchý
–	–	k?	–
jeho	jeho	k3xOp3gFnPc4	jeho
syntaxe	syntax	k1gFnPc4	syntax
je	být	k5eAaImIp3nS	být
zjednodušenou	zjednodušený	k2eAgFnSc4d1	zjednodušená
(	(	kIx(	(
<g/>
a	a	k8xC	a
drobně	drobně	k6eAd1	drobně
upravenou	upravený	k2eAgFnSc7d1	upravená
<g/>
)	)	kIx)	)
verzí	verze	k1gFnSc7	verze
syntaxe	syntax	k1gFnSc2	syntax
jazyka	jazyk	k1gInSc2	jazyk
C	C	kA	C
a	a	k8xC	a
C	C	kA	C
<g/>
++	++	k?	++
<g/>
.	.	kIx.	.
</s>
<s>
Odpadla	odpadnout	k5eAaPmAgFnS	odpadnout
většina	většina	k1gFnSc1	většina
nízkoúrovňových	nízkoúrovňový	k2eAgFnPc2d1	nízkoúrovňová
konstrukcí	konstrukce	k1gFnPc2	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
jazyka	jazyk	k1gInSc2	jazyk
nejsou	být	k5eNaImIp3nP	být
například	například	k6eAd1	například
ukazatelé	ukazatel	k1gMnPc1	ukazatel
<g/>
,	,	kIx,	,
bezznaménkové	bezznamének	k1gMnPc1	bezznamének
číselné	číselný	k2eAgFnSc2d1	číselná
datové	datový	k2eAgFnSc2d1	datová
typy	typa	k1gFnSc2	typa
<g/>
,	,	kIx,	,
příkaz	příkaz	k1gInSc4	příkaz
goto	goto	k6eAd1	goto
nebo	nebo	k8xC	nebo
preprocesor	preprocesor	k1gInSc4	preprocesor
<g/>
.	.	kIx.	.
objektově	objektově	k6eAd1	objektově
orientovaný	orientovaný	k2eAgInSc1d1	orientovaný
–	–	k?	–
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
osmi	osm	k4xCc2	osm
primitivních	primitivní	k2eAgInPc2d1	primitivní
datových	datový	k2eAgInPc2d1	datový
typů	typ	k1gInPc2	typ
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgInPc1	všechen
ostatní	ostatní	k2eAgInPc1d1	ostatní
datové	datový	k2eAgInPc1d1	datový
typy	typ	k1gInPc1	typ
objektové	objektový	k2eAgInPc1d1	objektový
<g/>
.	.	kIx.	.
distribuovaný	distribuovaný	k2eAgInSc1d1	distribuovaný
–	–	k?	–
je	být	k5eAaImIp3nS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
aplikací	aplikace	k1gFnPc2	aplikace
v	v	k7c6	v
síti	síť	k1gFnSc6	síť
(	(	kIx(	(
<g/>
podporuje	podporovat	k5eAaImIp3nS	podporovat
různé	různý	k2eAgFnPc4d1	různá
úrovně	úroveň	k1gFnPc4	úroveň
síťového	síťový	k2eAgNnSc2d1	síťové
spojení	spojení	k1gNnSc2	spojení
<g/>
,	,	kIx,	,
práce	práce	k1gFnSc2	práce
se	s	k7c7	s
vzdálenými	vzdálený	k2eAgInPc7d1	vzdálený
soubory	soubor	k1gInPc7	soubor
<g/>
,	,	kIx,	,
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vytvářet	vytvářet	k5eAaImF	vytvářet
distribuované	distribuovaný	k2eAgFnPc4d1	distribuovaná
klientské	klientský	k2eAgFnPc4d1	klientská
aplikace	aplikace	k1gFnPc4	aplikace
a	a	k8xC	a
servery	server	k1gInPc4	server
<g/>
.	.	kIx.	.
interpretovaný	interpretovaný	k2eAgInSc1d1	interpretovaný
–	–	k?	–
místo	místo	k7c2	místo
skutečného	skutečný	k2eAgInSc2d1	skutečný
strojového	strojový	k2eAgInSc2d1	strojový
kódu	kód	k1gInSc2	kód
se	se	k3xPyFc4	se
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
pouze	pouze	k6eAd1	pouze
tzv.	tzv.	kA	tzv.
bajtkód	bajtkóda	k1gFnPc2	bajtkóda
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
formát	formát	k1gInSc1	formát
je	být	k5eAaImIp3nS	být
nezávislý	závislý	k2eNgInSc1d1	nezávislý
na	na	k7c6	na
architektuře	architektura	k1gFnSc6	architektura
počítače	počítač	k1gInSc2	počítač
nebo	nebo	k8xC	nebo
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
pak	pak	k6eAd1	pak
může	moct	k5eAaImIp3nS	moct
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
libovolném	libovolný	k2eAgInSc6d1	libovolný
počítači	počítač	k1gInSc6	počítač
nebo	nebo	k8xC	nebo
zařízení	zařízení	k1gNnSc6	zařízení
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
interpret	interpret	k1gMnSc1	interpret
Javy	Java	k1gFnPc4	Java
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
virtuální	virtuální	k2eAgInSc1d1	virtuální
stroj	stroj	k1gInSc1	stroj
Javy	Java	k1gFnSc2	Java
–	–	k?	–
Java	Java	k1gMnSc1	Java
Virtual	Virtual	k1gMnSc1	Virtual
Machine	Machin	k1gInSc5	Machin
(	(	kIx(	(
<g/>
JVM	JVM	kA	JVM
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdějších	pozdní	k2eAgFnPc6d2	pozdější
verzích	verze	k1gFnPc6	verze
Javy	Java	k1gFnSc2	Java
nebyl	být	k5eNaImAgInS	být
mezikód	mezikód	k1gInSc1	mezikód
přímo	přímo	k6eAd1	přímo
interpretován	interpretován	k2eAgInSc1d1	interpretován
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
před	před	k7c7	před
prvním	první	k4xOgInSc7	první
svým	svůj	k3xOyFgNnPc3	svůj
provedením	provedení	k1gNnPc3	provedení
dynamicky	dynamicky	k6eAd1	dynamicky
zkompilován	zkompilován	k2eAgMnSc1d1	zkompilován
do	do	k7c2	do
strojového	strojový	k2eAgInSc2d1	strojový
kódu	kód	k1gInSc2	kód
daného	daný	k2eAgInSc2d1	daný
počítače	počítač	k1gInSc2	počítač
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
just	just	k6eAd1	just
in	in	k?	in
time	time	k1gInSc1	time
compilation	compilation	k1gInSc1	compilation
–	–	k?	–
JIT	jit	k2eAgInSc1d1	jit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlastnost	vlastnost	k1gFnSc1	vlastnost
zásadním	zásadní	k2eAgInSc7d1	zásadní
způsobem	způsob	k1gInSc7	způsob
zrychlila	zrychlit	k5eAaPmAgFnS	zrychlit
provádění	provádění	k1gNnPc4	provádění
programů	program	k1gInPc2	program
v	v	k7c6	v
Javě	Java	k1gFnSc6	Java
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
výrazně	výrazně	k6eAd1	výrazně
zpomalila	zpomalit	k5eAaPmAgFnS	zpomalit
start	start	k1gInSc4	start
programů	program	k1gInPc2	program
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
převážně	převážně	k6eAd1	převážně
používají	používat	k5eAaImIp3nP	používat
technologie	technologie	k1gFnPc1	technologie
zvané	zvaný	k2eAgFnPc1d1	zvaná
HotSpot	HotSpot	k1gInSc4	HotSpot
compiler	compiler	k1gMnSc1	compiler
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
mezikód	mezikód	k1gInSc1	mezikód
zpočátku	zpočátku	k6eAd1	zpočátku
interpretují	interpretovat	k5eAaBmIp3nP	interpretovat
a	a	k8xC	a
na	na	k7c6	na
základě	základ	k1gInSc6	základ
statistik	statistika	k1gFnPc2	statistika
získaných	získaný	k2eAgFnPc2d1	získaná
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
interpretace	interpretace	k1gFnSc2	interpretace
později	pozdě	k6eAd2	pozdě
provedou	provést	k5eAaPmIp3nP	provést
překlad	překlad	k1gInSc4	překlad
často	často	k6eAd1	často
používaných	používaný	k2eAgFnPc2d1	používaná
částí	část	k1gFnPc2	část
do	do	k7c2	do
strojového	strojový	k2eAgInSc2d1	strojový
kódu	kód	k1gInSc2	kód
včetně	včetně	k7c2	včetně
dalších	další	k2eAgFnPc2d1	další
dynamických	dynamický	k2eAgFnPc2d1	dynamická
optimalizací	optimalizace	k1gFnPc2	optimalizace
(	(	kIx(	(
<g/>
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
např.	např.	kA	např.
inlining	inlining	k1gInSc1	inlining
krátkých	krátký	k2eAgFnPc2d1	krátká
metod	metoda	k1gFnPc2	metoda
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
platformy	platforma	k1gFnPc1	platforma
nabízejí	nabízet	k5eAaImIp3nP	nabízet
přímou	přímý	k2eAgFnSc4d1	přímá
hardwarovou	hardwarový	k2eAgFnSc4d1	hardwarová
podporu	podpora	k1gFnSc4	podpora
pro	pro	k7c4	pro
Javu	Java	k1gFnSc4	Java
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
též	též	k9	též
mikroprocesory	mikroprocesor	k1gInPc7	mikroprocesor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
dokáží	dokázat	k5eAaPmIp3nP	dokázat
spustit	spustit	k5eAaPmF	spustit
Javu	Java	k1gFnSc4	Java
hardwarově	hardwarově	k6eAd1	hardwarově
namísto	namísto	k7c2	namísto
softwarové	softwarový	k2eAgFnSc2d1	softwarová
emulace	emulace	k1gFnSc2	emulace
Java	Java	k1gMnSc1	Java
Virtual	Virtual	k1gMnSc1	Virtual
Machine	Machin	k1gInSc5	Machin
<g/>
.	.	kIx.	.
</s>
<s>
ARM	ARM	kA	ARM
procesory	procesor	k1gInPc1	procesor
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
přímou	přímý	k2eAgFnSc4d1	přímá
hardwarovou	hardwarový	k2eAgFnSc4d1	hardwarová
podporu	podpora	k1gFnSc4	podpora
pro	pro	k7c4	pro
spuštění	spuštění	k1gNnSc4	spuštění
binárního	binární	k2eAgInSc2d1	binární
kódu	kód	k1gInSc2	kód
Javy	Java	k1gFnSc2	Java
<g/>
.	.	kIx.	.
robustní	robustní	k2eAgFnSc1d1	robustní
–	–	k?	–
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
psaní	psaní	k1gNnSc4	psaní
vysoce	vysoce	k6eAd1	vysoce
spolehlivého	spolehlivý	k2eAgInSc2d1	spolehlivý
softwaru	software	k1gInSc2	software
–	–	k?	–
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
neumožňuje	umožňovat	k5eNaImIp3nS	umožňovat
některé	některý	k3yIgFnPc4	některý
programátorské	programátorský	k2eAgFnPc4d1	programátorská
konstrukce	konstrukce	k1gFnPc4	konstrukce
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
bývají	bývat	k5eAaImIp3nP	bývat
častou	častý	k2eAgFnSc7d1	častá
příčinou	příčina	k1gFnSc7	příčina
chyb	chyba	k1gFnPc2	chyba
(	(	kIx(	(
<g/>
např.	např.	kA	např.
správa	správa	k1gFnSc1	správa
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
příkaz	příkaz	k1gInSc4	příkaz
goto	goto	k1gNnSc4	goto
<g/>
,	,	kIx,	,
používání	používání	k1gNnSc4	používání
ukazatelů	ukazatel	k1gMnPc2	ukazatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
tzv.	tzv.	kA	tzv.
silnou	silný	k2eAgFnSc4d1	silná
typovou	typový	k2eAgFnSc4d1	typová
kontrolu	kontrola	k1gFnSc4	kontrola
–	–	k?	–
veškeré	veškerý	k3xTgFnPc4	veškerý
používané	používaný	k2eAgFnPc4d1	používaná
proměnné	proměnná	k1gFnPc4	proměnná
musí	muset	k5eAaImIp3nP	muset
mít	mít	k5eAaImF	mít
definovaný	definovaný	k2eAgInSc4d1	definovaný
svůj	svůj	k3xOyFgInSc4	svůj
datový	datový	k2eAgInSc4d1	datový
typ	typ	k1gInSc4	typ
<g/>
.	.	kIx.	.
generační	generační	k2eAgFnSc1d1	generační
správa	správa	k1gFnSc1	správa
paměti	paměť	k1gFnSc2	paměť
–	–	k?	–
správa	správa	k1gFnSc1	správa
paměti	paměť	k1gFnSc2	paměť
je	být	k5eAaImIp3nS	být
realizována	realizován	k2eAgNnPc1d1	realizováno
pomocí	pomocí	k7c2	pomocí
automatického	automatický	k2eAgNnSc2d1	automatické
garbage	garbage	k1gNnSc2	garbage
collectoru	collector	k1gInSc2	collector
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
automaticky	automaticky	k6eAd1	automaticky
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
již	již	k6eAd1	již
nepoužívané	používaný	k2eNgFnPc4d1	nepoužívaná
části	část	k1gFnPc4	část
paměti	paměť	k1gFnSc2	paměť
a	a	k8xC	a
uvolňuje	uvolňovat	k5eAaImIp3nS	uvolňovat
je	on	k3xPp3gMnPc4	on
pro	pro	k7c4	pro
další	další	k2eAgNnPc4d1	další
použití	použití	k1gNnPc4	použití
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
prvních	první	k4xOgFnPc6	první
verzích	verze	k1gFnPc6	verze
opět	opět	k6eAd1	opět
příčinou	příčina	k1gFnSc7	příčina
pomalejšího	pomalý	k2eAgInSc2d2	pomalejší
běhu	běh	k1gInSc2	běh
programů	program	k1gInPc2	program
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgFnPc6d1	poslední
verzích	verze	k1gFnPc6	verze
běhových	běhový	k2eAgNnPc2d1	běhové
prostředí	prostředí	k1gNnSc2	prostředí
je	být	k5eAaImIp3nS	být
díky	díky	k7c3	díky
novým	nový	k2eAgInPc3d1	nový
algoritmům	algoritmus	k1gInPc3	algoritmus
pro	pro	k7c4	pro
garbage	garbage	k1gFnSc4	garbage
collection	collection	k1gInSc1	collection
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
generační	generační	k2eAgFnSc6d1	generační
správě	správa	k1gFnSc6	správa
paměti	paměť	k1gFnSc2	paměť
(	(	kIx(	(
<g/>
paměť	paměť	k1gFnSc1	paměť
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
více	hodně	k6eAd2	hodně
částí	část	k1gFnPc2	část
<g/>
,	,	kIx,	,
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jiný	jiný	k2eAgInSc1d1	jiný
algoritmus	algoritmus	k1gInSc1	algoritmus
pro	pro	k7c4	pro
garbage	garbage	k1gFnSc4	garbage
collection	collection	k1gInSc1	collection
a	a	k8xC	a
objekty	objekt	k1gInPc1	objekt
jsou	být	k5eAaImIp3nP	být
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgFnPc7	tento
částmi	část	k1gFnPc7	část
přesunovány	přesunován	k2eAgInPc4d1	přesunován
podle	podle	k7c2	podle
délky	délka	k1gFnSc2	délka
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
)	)	kIx)	)
tento	tento	k3xDgMnSc1	tento
<g />
.	.	kIx.	.
</s>
<s>
problém	problém	k1gInSc1	problém
ze	z	k7c2	z
značné	značný	k2eAgFnSc2d1	značná
části	část	k1gFnSc2	část
eliminován	eliminován	k2eAgMnSc1d1	eliminován
<g/>
.	.	kIx.	.
bezpečný	bezpečný	k2eAgInSc1d1	bezpečný
–	–	k?	–
má	mít	k5eAaImIp3nS	mít
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
chrání	chránit	k5eAaImIp3nP	chránit
počítač	počítač	k1gInSc4	počítač
v	v	k7c6	v
síťovém	síťový	k2eAgNnSc6d1	síťové
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgNnSc6	který
je	být	k5eAaImIp3nS	být
program	program	k1gInSc1	program
zpracováván	zpracováván	k2eAgInSc1d1	zpracováván
<g/>
,	,	kIx,	,
před	před	k7c7	před
nebezpečnými	bezpečný	k2eNgFnPc7d1	nebezpečná
operacemi	operace	k1gFnPc7	operace
nebo	nebo	k8xC	nebo
napadením	napadení	k1gNnSc7	napadení
vlastního	vlastní	k2eAgInSc2d1	vlastní
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
nepřátelským	přátelský	k2eNgInSc7d1	nepřátelský
kódem	kód	k1gInSc7	kód
<g/>
.	.	kIx.	.
nezávislý	závislý	k2eNgMnSc1d1	nezávislý
na	na	k7c6	na
architektuře	architektura	k1gFnSc6	architektura
–	–	k?	–
vytvořená	vytvořený	k2eAgFnSc1d1	vytvořená
aplikace	aplikace	k1gFnSc1	aplikace
běží	běžet	k5eAaImIp3nS	běžet
na	na	k7c6	na
libovolném	libovolný	k2eAgInSc6d1	libovolný
operačním	operační	k2eAgInSc6d1	operační
systému	systém	k1gInSc6	systém
nebo	nebo	k8xC	nebo
libovolné	libovolný	k2eAgFnSc6d1	libovolná
architektuře	architektura	k1gFnSc6	architektura
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
spuštění	spuštění	k1gNnSc3	spuštění
programu	program	k1gInSc2	program
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
pouze	pouze	k6eAd1	pouze
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
dané	daný	k2eAgFnSc6d1	daná
platformě	platforma	k1gFnSc6	platforma
instalován	instalovat	k5eAaBmNgInS	instalovat
správný	správný	k2eAgInSc1d1	správný
virtuální	virtuální	k2eAgInSc1d1	virtuální
stroj	stroj	k1gInSc1	stroj
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
konkrétní	konkrétní	k2eAgFnSc2d1	konkrétní
platformy	platforma	k1gFnSc2	platforma
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
vzhled	vzhled	k1gInSc4	vzhled
a	a	k8xC	a
chování	chování	k1gNnSc4	chování
aplikace	aplikace	k1gFnSc2	aplikace
<g/>
.	.	kIx.	.
přenositelný	přenositelný	k2eAgInSc1d1	přenositelný
–	–	k?	–
vedle	vedle	k7c2	vedle
zmíněné	zmíněný	k2eAgFnSc2d1	zmíněná
nezávislosti	nezávislost	k1gFnSc2	nezávislost
na	na	k7c6	na
architektuře	architektura	k1gFnSc6	architektura
je	být	k5eAaImIp3nS	být
jazyk	jazyk	k1gInSc1	jazyk
nezávislý	závislý	k2eNgInSc1d1	nezávislý
i	i	k9	i
co	co	k9	co
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
vlastností	vlastnost	k1gFnSc7	vlastnost
základních	základní	k2eAgInPc2d1	základní
datových	datový	k2eAgInPc2d1	datový
typů	typ	k1gInPc2	typ
(	(	kIx(	(
<g/>
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
explicitně	explicitně	k6eAd1	explicitně
určena	určen	k2eAgFnSc1d1	určena
vlastnost	vlastnost	k1gFnSc1	vlastnost
a	a	k8xC	a
velikost	velikost	k1gFnSc1	velikost
každého	každý	k3xTgInSc2	každý
z	z	k7c2	z
primitivních	primitivní	k2eAgInPc2d1	primitivní
datových	datový	k2eAgInPc2d1	datový
typů	typ	k1gInPc2	typ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přenositelností	přenositelnost	k1gFnSc7	přenositelnost
se	se	k3xPyFc4	se
však	však	k9	však
myslí	myslet	k5eAaImIp3nS	myslet
pouze	pouze	k6eAd1	pouze
přenášení	přenášení	k1gNnSc4	přenášení
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
jedné	jeden	k4xCgFnSc2	jeden
platformy	platforma	k1gFnSc2	platforma
Javy	Java	k1gFnSc2	Java
(	(	kIx(	(
<g/>
např.	např.	kA	např.
J	J	kA	J
<g/>
2	[number]	k4	2
<g/>
SE	s	k7c7	s
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přenášení	přenášení	k1gNnSc6	přenášení
mezi	mezi	k7c7	mezi
platformami	platforma	k1gFnPc7	platforma
Javy	Java	k1gFnSc2	Java
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
dát	dát	k5eAaPmF	dát
pozor	pozor	k1gInSc4	pozor
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
platforma	platforma	k1gFnSc1	platforma
určená	určený	k2eAgFnSc1d1	určená
pro	pro	k7c4	pro
jednodušší	jednoduchý	k2eAgNnSc4d2	jednodušší
zařízení	zařízení	k1gNnSc4	zařízení
nemusí	muset	k5eNaImIp3nS	muset
podporovat	podporovat	k5eAaImF	podporovat
všechny	všechen	k3xTgFnPc4	všechen
funkce	funkce	k1gFnPc4	funkce
dostupné	dostupný	k2eAgFnPc4d1	dostupná
na	na	k7c6	na
platformě	platforma	k1gFnSc6	platforma
pro	pro	k7c4	pro
složitější	složitý	k2eAgNnSc4d2	složitější
zařízení	zařízení	k1gNnSc4	zařízení
a	a	k8xC	a
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
může	moct	k5eAaImIp3nS	moct
definovat	definovat	k5eAaBmF	definovat
některé	některý	k3yIgFnPc4	některý
vlastní	vlastní	k2eAgFnPc4d1	vlastní
třídy	třída	k1gFnPc4	třída
doplňující	doplňující	k2eAgFnSc4d1	doplňující
nějakou	nějaký	k3yIgFnSc4	nějaký
speciální	speciální	k2eAgFnSc4d1	speciální
funkčnost	funkčnost	k1gFnSc4	funkčnost
nebo	nebo	k8xC	nebo
nahrazující	nahrazující	k2eAgFnPc4d1	nahrazující
třídy	třída	k1gFnPc4	třída
vyšší	vysoký	k2eAgFnPc4d2	vyšší
platformy	platforma	k1gFnPc4	platforma
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
<g />
.	.	kIx.	.
</s>
<s>
pro	pro	k7c4	pro
nižší	nízký	k2eAgFnSc4d2	nižší
platformu	platforma	k1gFnSc4	platforma
příliš	příliš	k6eAd1	příliš
komplikované	komplikovaný	k2eAgNnSc1d1	komplikované
<g/>
.	.	kIx.	.
výkonný	výkonný	k2eAgInSc4d1	výkonný
–	–	k?	–
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
jazyk	jazyk	k1gInSc4	jazyk
interpretovaný	interpretovaný	k2eAgInSc4d1	interpretovaný
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
ztráta	ztráta	k1gFnSc1	ztráta
výkonu	výkon	k1gInSc2	výkon
významná	významný	k2eAgFnSc1d1	významná
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
překladače	překladač	k1gInPc1	překladač
mohou	moct	k5eAaImIp3nP	moct
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
režimu	režim	k1gInSc6	režim
"	"	kIx"	"
<g/>
just-in-time	justnimat	k5eAaPmIp3nS	just-in-timat
<g/>
"	"	kIx"	"
a	a	k8xC	a
do	do	k7c2	do
strojového	strojový	k2eAgInSc2d1	strojový
kódu	kód	k1gInSc2	kód
se	se	k3xPyFc4	se
překládá	překládat	k5eAaImIp3nS	překládat
jen	jen	k9	jen
ten	ten	k3xDgInSc4	ten
kód	kód	k1gInSc4	kód
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
opravdu	opravdu	k6eAd1	opravdu
zapotřebí	zapotřebí	k6eAd1	zapotřebí
<g/>
.	.	kIx.	.
víceúlohový	víceúlohový	k2eAgInSc1d1	víceúlohový
–	–	k?	–
podporuje	podporovat	k5eAaImIp3nS	podporovat
zpracování	zpracování	k1gNnSc4	zpracování
vícevláknových	vícevláknův	k2eAgFnPc2d1	vícevláknův
aplikací	aplikace	k1gFnPc2	aplikace
dynamický	dynamický	k2eAgInSc1d1	dynamický
–	–	k?	–
Java	Java	k1gFnSc1	Java
byla	být	k5eAaImAgFnS	být
navržena	navrhnout	k5eAaPmNgFnS	navrhnout
pro	pro	k7c4	pro
nasazení	nasazení	k1gNnSc4	nasazení
ve	v	k7c6	v
vyvíjejícím	vyvíjející	k2eAgMnSc6d1	vyvíjející
se	se	k3xPyFc4	se
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Knihovna	knihovna	k1gFnSc1	knihovna
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
dynamicky	dynamicky	k6eAd1	dynamicky
za	za	k7c2	za
chodu	chod	k1gInSc2	chod
rozšiřována	rozšiřovat	k5eAaImNgFnS	rozšiřovat
o	o	k7c4	o
nové	nový	k2eAgFnPc4d1	nová
třídy	třída	k1gFnPc4	třída
a	a	k8xC	a
funkce	funkce	k1gFnPc4	funkce
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k8xS	jak
z	z	k7c2	z
externích	externí	k2eAgInPc2d1	externí
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
vlastním	vlastní	k2eAgInSc7d1	vlastní
programem	program	k1gInSc7	program
<g/>
.	.	kIx.	.
elegantní	elegantní	k2eAgMnSc1d1	elegantní
–	–	k?	–
velice	velice	k6eAd1	velice
pěkně	pěkně	k6eAd1	pěkně
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
pracuje	pracovat	k5eAaImIp3nS	pracovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
snadno	snadno	k6eAd1	snadno
čitelný	čitelný	k2eAgMnSc1d1	čitelný
(	(	kIx(	(
<g/>
např.	např.	kA	např.
i	i	k9	i
pro	pro	k7c4	pro
publikaci	publikace	k1gFnSc4	publikace
algoritmů	algoritmus	k1gInPc2	algoritmus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přímo	přímo	k6eAd1	přímo
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
ošetření	ošetření	k1gNnSc4	ošetření
výjimek	výjimka	k1gFnPc2	výjimka
a	a	k8xC	a
typovou	typový	k2eAgFnSc4d1	typová
kontrolu	kontrola	k1gFnSc4	kontrola
<g/>
.	.	kIx.	.
1991	[number]	k4	1991
–	–	k?	–
James	James	k1gInSc1	James
Gosling	Gosling	k1gInSc1	Gosling
<g/>
,	,	kIx,	,
Bill	Bill	k1gMnSc1	Bill
Joy	Joy	k1gMnSc1	Joy
<g/>
,	,	kIx,	,
Mike	Mike	k1gFnSc1	Mike
Sheridan	Sheridan	k1gMnSc1	Sheridan
a	a	k8xC	a
Patrick	Patrick	k1gMnSc1	Patrick
Naughton	Naughton	k1gInSc4	Naughton
započali	započnout	k5eAaPmAgMnP	započnout
Stealth	Stealth	k1gMnSc1	Stealth
Project	Project	k1gMnSc1	Project
(	(	kIx(	(
<g/>
Tajný	tajný	k2eAgInSc1d1	tajný
projekt	projekt	k1gInSc1	projekt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
měl	mít	k5eAaImAgMnS	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
vytvořit	vytvořit	k5eAaPmF	vytvořit
systém	systém	k1gInSc4	systém
pro	pro	k7c4	pro
domácí	domácí	k2eAgInPc4d1	domácí
spotřebiče	spotřebič	k1gInPc4	spotřebič
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
přejmenoval	přejmenovat	k5eAaPmAgMnS	přejmenovat
na	na	k7c4	na
Green	Green	k2eAgInSc4d1	Green
Project	Project	k1gInSc4	Project
(	(	kIx(	(
<g/>
Zelený	zelený	k2eAgInSc4d1	zelený
projekt	projekt	k1gInSc4	projekt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
tým	tým	k1gInSc1	tým
vývojářů	vývojář	k1gMnPc2	vývojář
dostal	dostat	k5eAaPmAgInS	dostat
název	název	k1gInSc4	název
Green	Green	k2eAgInSc1d1	Green
Team	team	k1gInSc1	team
(	(	kIx(	(
<g/>
Zelený	zelený	k2eAgInSc1d1	zelený
tým	tým	k1gInSc1	tým
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vedoucím	vedoucí	k1gMnSc7	vedoucí
projektu	projekt	k1gInSc2	projekt
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
James	James	k1gMnSc1	James
Gosling	Gosling	k1gInSc4	Gosling
<g/>
.	.	kIx.	.
</s>
<s>
Green	Green	k2eAgInSc1d1	Green
Team	team	k1gInSc1	team
zpočátku	zpočátku	k6eAd1	zpočátku
používal	používat	k5eAaImAgInS	používat
programovací	programovací	k2eAgInSc1d1	programovací
jazyk	jazyk	k1gInSc1	jazyk
C	C	kA	C
<g/>
++	++	k?	++
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
ovšem	ovšem	k9	ovšem
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gInSc4	jejich
cíl	cíl	k1gInSc4	cíl
nebyl	být	k5eNaImAgInS	být
vhodný	vhodný	k2eAgInSc1d1	vhodný
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
James	James	k1gInSc1	James
Gosling	Gosling	k1gInSc1	Gosling
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
novém	nový	k2eAgInSc6d1	nový
jazyku	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
Oak	Oak	k1gFnSc4	Oak
(	(	kIx(	(
<g/>
dub	dub	k1gInSc1	dub
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
stromu	strom	k1gInSc2	strom
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
rostl	růst	k5eAaImAgInS	růst
před	před	k7c7	před
okny	okno	k1gNnPc7	okno
jeho	jeho	k3xOp3gFnSc2	jeho
kanceláře	kancelář	k1gFnSc2	kancelář
<g/>
.	.	kIx.	.
1995	[number]	k4	1995
–	–	k?	–
Přejmenování	přejmenování	k1gNnSc1	přejmenování
jazyka	jazyk	k1gInSc2	jazyk
z	z	k7c2	z
Oak	Oak	k1gFnSc2	Oak
na	na	k7c4	na
Java	Javus	k1gMnSc4	Javus
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
s	s	k7c7	s
názvem	název	k1gInSc7	název
Oak	Oak	k1gFnPc2	Oak
totiž	totiž	k9	totiž
již	již	k6eAd1	již
existoval	existovat	k5eAaImAgInS	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Java	Java	k6eAd1	Java
je	být	k5eAaImIp3nS	být
americké	americký	k2eAgNnSc1d1	americké
slangové	slangový	k2eAgNnSc1d1	slangové
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
kávu	káva	k1gFnSc4	káva
<g/>
.	.	kIx.	.
1996	[number]	k4	1996
–	–	k?	–
Java	Java	k1gMnSc1	Java
Development	Development	k1gMnSc1	Development
Kit	kit	k1gInSc1	kit
(	(	kIx(	(
<g/>
JDK	JDK	kA	JDK
<g/>
)	)	kIx)	)
1.0	[number]	k4	1.0
První	první	k4xOgFnSc7	první
oficiální	oficiální	k2eAgFnSc1d1	oficiální
verze	verze	k1gFnSc1	verze
vyšla	vyjít	k5eAaPmAgFnS	vyjít
26	[number]	k4	26
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvních	první	k4xOgFnPc6	první
verzích	verze	k1gFnPc6	verze
Javy	Java	k1gFnSc2	Java
a	a	k8xC	a
JDK	JDK	kA	JDK
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
použít	použít	k5eAaPmF	použít
klíčová	klíčový	k2eAgNnPc1d1	klíčové
slova	slovo	k1gNnPc1	slovo
private	privat	k1gInSc5	privat
a	a	k8xC	a
protected	protected	k1gInSc4	protected
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
dalšího	další	k2eAgInSc2d1	další
modifikátoru	modifikátor	k1gInSc2	modifikátor
přístupu	přístup	k1gInSc2	přístup
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
k	k	k7c3	k
omezení	omezení	k1gNnSc3	omezení
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
metodám	metoda	k1gFnPc3	metoda
a	a	k8xC	a
proměnným	proměnný	k2eAgFnPc3d1	proměnná
výhradně	výhradně	k6eAd1	výhradně
na	na	k7c4	na
podtřídy	podtřída	k1gFnPc4	podtřída
dané	daný	k2eAgFnSc2d1	daná
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
funkčnost	funkčnost	k1gFnSc1	funkčnost
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
jazyka	jazyk	k1gInSc2	jazyk
odebrána	odebrán	k2eAgFnSc1d1	odebrána
ve	v	k7c6	v
verzi	verze	k1gFnSc6	verze
1.0	[number]	k4	1.0
<g/>
.2	.2	k4	.2
<g/>
.	.	kIx.	.
1997	[number]	k4	1997
–	–	k?	–
JDK	JDK	kA	JDK
1.1	[number]	k4	1.1
Další	další	k2eAgFnSc1d1	další
významná	významný	k2eAgFnSc1d1	významná
verze	verze	k1gFnSc1	verze
JDK	JDK	kA	JDK
vyšla	vyjít	k5eAaPmAgFnS	vyjít
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jazyka	jazyk	k1gInSc2	jazyk
byla	být	k5eAaImAgFnS	být
přidána	přidat	k5eAaPmNgFnS	přidat
podpora	podpora	k1gFnSc1	podpora
vnořených	vnořený	k2eAgFnPc2d1	vnořená
tříd	třída	k1gFnPc2	třída
<g/>
,	,	kIx,	,
reflexe	reflexe	k1gFnSc1	reflexe
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
podpory	podpora	k1gFnSc2	podpora
modifikace	modifikace	k1gFnSc2	modifikace
za	za	k7c2	za
běhu	běh	k1gInSc2	běh
<g/>
)	)	kIx)	)
a	a	k8xC	a
Unicode	Unicod	k1gInSc5	Unicod
verze	verze	k1gFnPc1	verze
2.0	[number]	k4	2.0
<g/>
.	.	kIx.	.
</s>
<s>
JDK	JDK	kA	JDK
obsahovalo	obsahovat	k5eAaImAgNnS	obsahovat
následující	následující	k2eAgFnPc4d1	následující
novinky	novinka	k1gFnPc4	novinka
<g/>
:	:	kIx,	:
JavaBeans	JavaBeans	k1gInSc1	JavaBeans
<g/>
,	,	kIx,	,
JDBC	JDBC	kA	JDBC
<g/>
,	,	kIx,	,
Java	Java	k1gFnSc1	Java
RMI	RMI	kA	RMI
<g/>
,	,	kIx,	,
rozšíření	rozšíření	k1gNnSc2	rozšíření
AWT	AWT	kA	AWT
a	a	k8xC	a
JIT	jit	k2eAgInSc1d1	jit
kompilátor	kompilátor	k1gInSc1	kompilátor
pro	pro	k7c4	pro
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
<g/>
.	.	kIx.	.
1998	[number]	k4	1998
–	–	k?	–
J2SE	J2SE	k1gFnSc2	J2SE
1.2	[number]	k4	1.2
Verze	verze	k1gFnSc1	verze
s	s	k7c7	s
kódovým	kódový	k2eAgNnSc7d1	kódové
označením	označení	k1gNnSc7	označení
Playground	Playgrounda	k1gFnPc2	Playgrounda
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
8	[number]	k4	8
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenování	pojmenování	k1gNnSc1	pojmenování
se	se	k3xPyFc4	se
změnilo	změnit	k5eAaPmAgNnS	změnit
na	na	k7c4	na
Java	Javum	k1gNnPc4	Javum
2	[number]	k4	2
a	a	k8xC	a
název	název	k1gInSc1	název
verze	verze	k1gFnSc2	verze
J	J	kA	J
<g/>
2	[number]	k4	2
<g/>
SE	s	k7c7	s
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
SE	se	k3xPyFc4	se
z	z	k7c2	z
názvu	název	k1gInSc2	název
J2SE	J2SE	k1gFnSc2	J2SE
(	(	kIx(	(
<g/>
Java	Jav	k1gInSc2	Jav
2	[number]	k4	2
Platform	Platform	k1gInSc1	Platform
<g/>
,	,	kIx,	,
Standard	standard	k1gInSc1	standard
Edition	Edition	k1gInSc1	Edition
<g/>
)	)	kIx)	)
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
rozlišení	rozlišení	k1gNnSc3	rozlišení
základní	základní	k2eAgFnSc2d1	základní
platformy	platforma	k1gFnSc2	platforma
od	od	k7c2	od
J2EE	J2EE	k1gFnSc2	J2EE
(	(	kIx(	(
<g/>
Java	Jav	k1gInSc2	Jav
2	[number]	k4	2
Platform	Platform	k1gInSc1	Platform
<g/>
,	,	kIx,	,
Enterprise	Enterprise	k1gFnSc1	Enterprise
Edition	Edition	k1gInSc1	Edition
<g/>
)	)	kIx)	)
a	a	k8xC	a
J2ME	J2ME	k1gMnSc1	J2ME
(	(	kIx(	(
<g/>
Java	Java	k1gMnSc1	Java
2	[number]	k4	2
Platform	Platform	k1gInSc1	Platform
<g/>
,	,	kIx,	,
Micro	Micro	k1gNnSc1	Micro
Edition	Edition	k1gInSc1	Edition
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jazyka	jazyk	k1gInSc2	jazyk
přibylo	přibýt	k5eAaPmAgNnS	přibýt
klíčové	klíčový	k2eAgNnSc1d1	klíčové
slovo	slovo	k1gNnSc1	slovo
strictfp	strictfp	k1gInSc1	strictfp
<g/>
.	.	kIx.	.
</s>
<s>
Velikost	velikost	k1gFnSc1	velikost
platformy	platforma	k1gFnSc2	platforma
Java	Jav	k1gInSc2	Jav
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
verzi	verze	k1gFnSc6	verze
ztrojnásobila	ztrojnásobit	k5eAaPmAgFnS	ztrojnásobit
na	na	k7c4	na
1	[number]	k4	1
520	[number]	k4	520
tříd	třída	k1gFnPc2	třída
v	v	k7c6	v
59	[number]	k4	59
balíčcích	balíček	k1gInPc6	balíček
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
změny	změna	k1gFnPc1	změna
<g/>
:	:	kIx,	:
knihovna	knihovna	k1gFnSc1	knihovna
Swing	swing	k1gInSc4	swing
pro	pro	k7c4	pro
sestavení	sestavení	k1gNnSc4	sestavení
grafického	grafický	k2eAgNnSc2d1	grafické
rozhraní	rozhraní	k1gNnSc2	rozhraní
<g/>
,	,	kIx,	,
Java	Java	k1gFnSc1	Java
2D	[number]	k4	2D
API	API	kA	API
<g/>
,	,	kIx,	,
Java	Java	k1gFnSc1	Java
collections	collections	k1gInSc1	collections
framework	framework	k1gInSc1	framework
(	(	kIx(	(
<g/>
JSR	JSR	kA	JSR
166	[number]	k4	166
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Accessibility	Accessibilita	k1gFnPc4	Accessibilita
API	API	kA	API
<g/>
,	,	kIx,	,
JDBC	JDBC	kA	JDBC
2.0	[number]	k4	2.0
<g/>
,	,	kIx,	,
Java	Java	k1gMnSc1	Java
Plug-in	Plugn	k1gMnSc1	Plug-in
<g/>
,	,	kIx,	,
Java	Java	k1gMnSc1	Java
IDL	IDL	kA	IDL
–	–	k?	–
pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
technologie	technologie	k1gFnSc2	technologie
CORBA	CORBA	kA	CORBA
<g/>
,	,	kIx,	,
absolutní	absolutní	k2eAgFnSc1d1	absolutní
podpora	podpora	k1gFnSc1	podpora
Unicode	Unicod	k1gInSc5	Unicod
včetně	včetně	k7c2	včetně
psání	psání	k1gMnSc1	psání
v	v	k7c6	v
japonštině	japonština	k1gFnSc6	japonština
<g/>
,	,	kIx,	,
čínštině	čínština	k1gFnSc6	čínština
a	a	k8xC	a
korejském	korejský	k2eAgInSc6d1	korejský
jazyce	jazyk	k1gInSc6	jazyk
<g/>
,	,	kIx,	,
lepší	dobrý	k2eAgInSc1d2	lepší
výkon	výkon	k1gInSc1	výkon
JIT-kompilátoru	JITompilátor	k1gInSc2	JIT-kompilátor
<g/>
.	.	kIx.	.
2000	[number]	k4	2000
–	–	k?	–
J2SE	J2SE	k1gFnSc2	J2SE
1.3	[number]	k4	1.3
Verze	verze	k1gFnSc1	verze
s	s	k7c7	s
kódovým	kódový	k2eAgNnSc7d1	kódové
označením	označení	k1gNnSc7	označení
Kestrel	Kestrela	k1gFnPc2	Kestrela
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
8	[number]	k4	8
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jazyka	jazyk	k1gInSc2	jazyk
byly	být	k5eAaImAgFnP	být
přidány	přidat	k5eAaPmNgInP	přidat
syntetické	syntetický	k2eAgInPc1d1	syntetický
proxy	prox	k1gInPc1	prox
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
změny	změna	k1gFnPc1	změna
<g/>
:	:	kIx,	:
přidání	přidání	k1gNnPc1	přidání
JavaSound	JavaSounda	k1gFnPc2	JavaSounda
<g/>
,	,	kIx,	,
Java	Java	k1gFnSc1	Java
Naming	Naming	k1gInSc1	Naming
and	and	k?	and
Directory	Director	k1gInPc1	Director
Interface	interface	k1gInSc1	interface
(	(	kIx(	(
<g/>
JNDI	JNDI	kA	JNDI
<g/>
)	)	kIx)	)
–	–	k?	–
dříve	dříve	k6eAd2	dříve
dostupné	dostupný	k2eAgFnPc1d1	dostupná
jako	jako	k8xS	jako
rozšíření	rozšíření	k1gNnSc1	rozšíření
<g/>
,	,	kIx,	,
Java	Java	k1gFnSc1	Java
Platform	Platform	k1gInSc1	Platform
Debugger	Debugger	k1gInSc1	Debugger
Architecture	Architectur	k1gMnSc5	Architectur
(	(	kIx(	(
<g/>
JPDA	JPDA	kA	JPDA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
modifikace	modifikace	k1gFnSc1	modifikace
Java	Javum	k1gNnSc2	Javum
RMI	RMI	kA	RMI
pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
technologie	technologie	k1gFnSc2	technologie
CORBA	CORBA	kA	CORBA
a	a	k8xC	a
přidání	přidání	k1gNnSc2	přidání
HotSpot	HotSpota	k1gFnPc2	HotSpota
JVM	JVM	kA	JVM
<g/>
.	.	kIx.	.
2002	[number]	k4	2002
–	–	k?	–
J2SE	J2SE	k1gFnSc6	J2SE
1.4	[number]	k4	1.4
2004	[number]	k4	2004
–	–	k?	–
J2SE	J2SE	k1gFnPc2	J2SE
5.0	[number]	k4	5.0
Specifikovaná	specifikovaný	k2eAgFnSc1d1	specifikovaná
verze	verze	k1gFnSc1	verze
Java	Jav	k1gInSc2	Jav
5.0	[number]	k4	5.0
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byla	být	k5eAaImAgFnS	být
změněna	změnit	k5eAaPmNgFnS	změnit
oficiální	oficiální	k2eAgFnSc1d1	oficiální
indexace	indexace	k1gFnSc2	indexace
Javy	Java	k1gFnSc2	Java
<g/>
.	.	kIx.	.
</s>
<s>
Java	Jav	k1gInSc2	Jav
1.5	[number]	k4	1.5
se	se	k3xPyFc4	se
správně	správně	k6eAd1	správně
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Java	Java	k1gFnSc1	Java
5.0	[number]	k4	5.0
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
indexace	indexace	k1gFnSc1	indexace
přesto	přesto	k8xC	přesto
zůstala	zůstat	k5eAaPmAgFnS	zůstat
stejná	stejný	k2eAgFnSc1d1	stejná
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
x.	x.	k?	x.
Daná	daný	k2eAgFnSc1d1	daná
verze	verze	k1gFnSc1	verze
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
změn	změna	k1gFnPc2	změna
<g/>
:	:	kIx,	:
přidané	přidaný	k2eAgInPc1d1	přidaný
výčtové	výčtový	k2eAgInPc1d1	výčtový
typy	typ	k1gInPc1	typ
<g/>
,	,	kIx,	,
anotace	anotace	k1gFnPc1	anotace
<g/>
,	,	kIx,	,
generics	generics	k1gInSc1	generics
<g/>
,	,	kIx,	,
autoboxing	autoboxing	k1gInSc1	autoboxing
<g/>
/	/	kIx~	/
<g/>
anboxing	anboxing	k1gInSc1	anboxing
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
určitě	určitě	k6eAd1	určitě
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
cyklus	cyklus	k1gInSc1	cyklus
foreach	foreach	k1gInSc1	foreach
(	(	kIx(	(
<g/>
interátor	interátor	k1gInSc1	interátor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
určitě	určitě	k6eAd1	určitě
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Javadoc	Javadoc	k1gFnSc4	Javadoc
komentáře	komentář	k1gInSc2	komentář
<g/>
,	,	kIx,	,
povolen	povolen	k2eAgInSc1d1	povolen
import	import	k1gInSc1	import
statických	statický	k2eAgNnPc2d1	statické
polí	pole	k1gNnPc2	pole
a	a	k8xC	a
metod	metoda	k1gFnPc2	metoda
<g/>
,	,	kIx,	,
přidané	přidaný	k2eAgFnSc2d1	přidaná
metody	metoda	k1gFnSc2	metoda
s	s	k7c7	s
neurčitým	určitý	k2eNgInSc7d1	neurčitý
počtem	počet	k1gInSc7	počet
parametrů	parametr	k1gInPc2	parametr
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Výčtové	výčtový	k2eAgInPc1d1	výčtový
typy	typ	k1gInPc1	typ
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
enum	enum	k1gInSc1	enum
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
plnohodnotnou	plnohodnotný	k2eAgFnSc7d1	plnohodnotná
třídou	třída	k1gFnSc7	třída
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
že	že	k8xS	že
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
pole	pole	k1gNnPc4	pole
a	a	k8xC	a
metody	metoda	k1gFnPc4	metoda
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
dokonce	dokonce	k9	dokonce
skryté	skrytý	k2eAgInPc1d1	skrytý
i	i	k8xC	i
abstraktní	abstraktní	k2eAgInPc1d1	abstraktní
<g/>
.	.	kIx.	.
</s>
<s>
Anotace	anotace	k1gFnPc1	anotace
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
přidávat	přidávat	k5eAaImF	přidávat
do	do	k7c2	do
kódu	kód	k1gInSc2	kód
neovlivňující	ovlivňující	k2eNgNnPc1d1	neovlivňující
programová	programový	k2eAgNnPc1d1	programové
data	datum	k1gNnPc1	datum
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
například	například	k6eAd1	například
popisují	popisovat	k5eAaImIp3nP	popisovat
nějakou	nějaký	k3yIgFnSc4	nějaký
část	část	k1gFnSc4	část
kódu	kód	k1gInSc2	kód
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
účel	účel	k1gInSc1	účel
<g/>
.	.	kIx.	.
2006	[number]	k4	2006
–	–	k?	–
Java	Jav	k1gInSc2	Jav
SE	se	k3xPyFc4	se
6	[number]	k4	6
Java	Jav	k1gInSc2	Jav
6	[number]	k4	6
byla	být	k5eAaImAgFnS	být
zveřejněna	zveřejněn	k2eAgFnSc1d1	zveřejněna
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
indexace	indexace	k1gFnSc1	indexace
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgFnS	změnit
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
očekávané	očekávaný	k2eAgFnSc2d1	očekávaná
verze	verze	k1gFnSc2	verze
6.0	[number]	k4	6.0
byla	být	k5eAaImAgFnS	být
Java	Jav	k1gInSc2	Jav
6	[number]	k4	6
představena	představen	k2eAgFnSc1d1	představena
pod	pod	k7c7	pod
tímto	tento	k3xDgInSc7	tento
názvem	název	k1gInSc7	název
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgFnPc1d2	menší
změny	změna	k1gFnPc1	změna
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c4	v
Java	Javum	k1gNnPc4	Javum
5.0	[number]	k4	5.0
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
přidaly	přidat	k5eAaPmAgFnP	přidat
pomocí	pomocí	k7c2	pomocí
aktualizací	aktualizace	k1gFnPc2	aktualizace
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
–	–	k?	–
Java	Jav	k1gInSc2	Jav
SE	se	k3xPyFc4	se
7	[number]	k4	7
Java	Jav	k1gInSc2	Jav
7	[number]	k4	7
byla	být	k5eAaImAgFnS	být
zveřejněna	zveřejněn	k2eAgFnSc1d1	zveřejněna
28	[number]	k4	28
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Finální	finální	k2eAgFnPc1d1	finální
verze	verze	k1gFnPc1	verze
Java	Jav	k1gInSc2	Jav
Standard	standard	k1gInSc1	standard
Edition	Edition	k1gInSc1	Edition
7	[number]	k4	7
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
všechny	všechen	k3xTgFnPc4	všechen
naplánované	naplánovaný	k2eAgFnPc4d1	naplánovaná
změny	změna	k1gFnPc4	změna
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nP	objevit
v	v	k7c6	v
Java	Jav	k1gInSc2	Jav
Standard	standard	k1gInSc1	standard
Edition	Edition	k1gInSc1	Edition
8	[number]	k4	8
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
–	–	k?	–
Java	Jav	k1gInSc2	Jav
SE	se	k3xPyFc4	se
8	[number]	k4	8
Java	Jav	k1gInSc2	Jav
8	[number]	k4	8
byla	být	k5eAaImAgFnS	být
zveřejněna	zveřejněn	k2eAgFnSc1d1	zveřejněna
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Přinesla	přinést	k5eAaPmAgFnS	přinést
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
vybrané	vybraný	k2eAgInPc4d1	vybraný
prvky	prvek	k1gInPc4	prvek
z	z	k7c2	z
funkcionálního	funkcionální	k2eAgNnSc2d1	funkcionální
programování	programování	k1gNnSc2	programování
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
lambda	lambda	k1gNnSc4	lambda
funkce	funkce	k1gFnSc2	funkce
či	či	k8xC	či
proudové	proudový	k2eAgNnSc4d1	proudové
zpracování	zpracování	k1gNnSc4	zpracování
dat	datum	k1gNnPc2	datum
–	–	k?	–
viz	vidět	k5eAaImRp2nS	vidět
též	též	k9	též
programovací	programovací	k2eAgInSc1d1	programovací
model	model	k1gInSc1	model
MapReduce	MapReduce	k1gFnSc2	MapReduce
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
rozšíření	rozšíření	k1gNnSc1	rozšíření
Javy	Java	k1gFnSc2	Java
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
nových	nový	k2eAgInPc2d1	nový
syntaktických	syntaktický	k2eAgInPc2d1	syntaktický
prvků	prvek	k1gInPc2	prvek
i	i	k9	i
z	z	k7c2	z
nových	nový	k2eAgFnPc2d1	nová
instrukcí	instrukce	k1gFnPc2	instrukce
bajtkódu	bajtkódu	k6eAd1	bajtkódu
<g/>
.	.	kIx.	.
plán	plán	k1gInSc1	plán
<g/>
:	:	kIx,	:
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
21	[number]	k4	21
<g/>
.	.	kIx.	.
<g/>
září	září	k1gNnSc2	září
–	–	k?	–
Java	Javum	k1gNnSc2	Javum
SE	s	k7c7	s
9	[number]	k4	9
Aktuální	aktuální	k2eAgInSc4d1	aktuální
plán	plán	k1gInSc4	plán
pro	pro	k7c4	pro
Javu	Java	k1gFnSc4	Java
9	[number]	k4	9
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
například	například	k6eAd1	například
projekt	projekt	k1gInSc4	projekt
Jigsaw	Jigsaw	k1gFnSc2	Jigsaw
(	(	kIx(	(
<g/>
systém	systém	k1gInSc1	systém
modulů	modul	k1gInPc2	modul
<g/>
)	)	kIx)	)
či	či	k8xC	či
podporu	podpora	k1gFnSc4	podpora
pro	pro	k7c4	pro
reaktivní	reaktivní	k2eAgNnSc4d1	reaktivní
programování	programování	k1gNnSc4	programování
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
Oracle	Oracle	k1gFnSc1	Oracle
je	být	k5eAaImIp3nS	být
současným	současný	k2eAgMnSc7d1	současný
vlastníkem	vlastník	k1gMnSc7	vlastník
oficiální	oficiální	k2eAgFnSc2d1	oficiální
implementace	implementace	k1gFnSc2	implementace
Java	Jav	k2eAgFnSc1d1	Java
SE	se	k3xPyFc4	se
platformy	platforma	k1gFnSc2	platforma
<g/>
,	,	kIx,	,
po	po	k7c6	po
její	její	k3xOp3gFnSc6	její
akvizici	akvizice	k1gFnSc6	akvizice
firmy	firma	k1gFnSc2	firma
Sun	Sun	kA	Sun
Microsystems	Microsystems	k1gInSc4	Microsystems
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
implementace	implementace	k1gFnSc1	implementace
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c4	na
původní	původní	k2eAgFnSc4d1	původní
od	od	k7c2	od
Sunu	sun	k1gInSc2	sun
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
dostupná	dostupný	k2eAgFnSc1d1	dostupná
pro	pro	k7c4	pro
Mac	Mac	kA	Mac
OS	OS	kA	OS
X	X	kA	X
<g/>
,	,	kIx,	,
Windows	Windows	kA	Windows
a	a	k8xC	a
Solaris	Solaris	k1gFnSc2	Solaris
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Java	Java	k1gMnSc1	Java
postrádá	postrádat	k5eAaImIp3nS	postrádat
jakékoliv	jakýkoliv	k3yIgFnPc4	jakýkoliv
formální	formální	k2eAgFnPc4d1	formální
standardizace	standardizace	k1gFnPc4	standardizace
uznané	uznaný	k2eAgFnPc4d1	uznaná
Ecma	Ecm	k1gInSc2	Ecm
International	International	k1gFnPc7	International
<g/>
,	,	kIx,	,
ISO	ISO	kA	ISO
<g/>
/	/	kIx~	/
<g/>
IEC	IEC	kA	IEC
<g/>
,	,	kIx,	,
ANSI	ANSI	kA	ANSI
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jinými	jiný	k2eAgFnPc7d1	jiná
standardizačními	standardizační	k2eAgFnPc7d1	standardizační
organizacemi	organizace	k1gFnPc7	organizace
třetích	třetí	k4xOgFnPc2	třetí
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
implementace	implementace	k1gFnSc2	implementace
Oraclu	Oracl	k1gInSc2	Oracl
je	být	k5eAaImIp3nS	být
de	de	k?	de
facto	facto	k1gNnSc4	facto
standard	standard	k1gInSc4	standard
<g/>
.	.	kIx.	.
</s>
<s>
Implementace	implementace	k1gFnSc1	implementace
Oraclu	Oracl	k1gInSc2	Oracl
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
různých	různý	k2eAgFnPc2d1	různá
distribucí	distribuce	k1gFnPc2	distribuce
<g/>
:	:	kIx,	:
Java	Java	k1gFnSc1	Java
Runtime	runtime	k1gInSc1	runtime
Environment	Environment	k1gMnSc1	Environment
(	(	kIx(	(
<g/>
JRE	JRE	kA	JRE
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
části	část	k1gFnPc1	část
Java	Jav	k1gInSc2	Jav
SE	se	k3xPyFc4	se
platformy	platforma	k1gFnPc1	platforma
potřebné	potřebné	k1gNnSc4	potřebné
pro	pro	k7c4	pro
běh	běh	k1gInSc4	běh
programů	program	k1gInPc2	program
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
Java	Jav	k1gInSc2	Jav
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
určena	určit	k5eAaPmNgFnS	určit
pro	pro	k7c4	pro
koncové	koncový	k2eAgMnPc4d1	koncový
uživatele	uživatel	k1gMnPc4	uživatel
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
balíkem	balík	k1gInSc7	balík
je	být	k5eAaImIp3nS	být
Java	Java	k1gFnSc1	Java
Development	Development	k1gInSc4	Development
Kit	kit	k1gInSc1	kit
(	(	kIx(	(
<g/>
JDK	JDK	kA	JDK
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
pro	pro	k7c4	pro
softwarové	softwarový	k2eAgMnPc4d1	softwarový
vývojáře	vývojář	k1gMnPc4	vývojář
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
vývojové	vývojový	k2eAgInPc4d1	vývojový
nástroje	nástroj	k1gInPc4	nástroj
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
Java	Java	k1gMnSc1	Java
kompilátor	kompilátor	k1gMnSc1	kompilátor
<g/>
,	,	kIx,	,
Javadoc	Javadoc	k1gFnSc1	Javadoc
<g/>
,	,	kIx,	,
Jar	jar	k1gFnSc1	jar
a	a	k8xC	a
debugger	debugger	k1gInSc1	debugger
<g/>
.	.	kIx.	.
</s>
<s>
OpenJDK	OpenJDK	k?	OpenJDK
je	být	k5eAaImIp3nS	být
další	další	k2eAgFnSc7d1	další
významnou	významný	k2eAgFnSc7d1	významná
implementací	implementace	k1gFnSc7	implementace
Javy	Java	k1gFnSc2	Java
SE	se	k3xPyFc4	se
pod	pod	k7c7	pod
licencí	licence	k1gFnSc7	licence
GPL	GPL	kA	GPL
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
začala	začít	k5eAaPmAgFnS	začít
<g/>
,	,	kIx,	,
když	když	k8xS	když
Sun	Sun	kA	Sun
začal	začít	k5eAaPmAgInS	začít
vydávat	vydávat	k5eAaPmF	vydávat
zdrojový	zdrojový	k2eAgInSc1d1	zdrojový
kód	kód	k1gInSc1	kód
Javy	Java	k1gFnSc2	Java
pod	pod	k7c7	pod
licencí	licence	k1gFnSc7	licence
GPL	GPL	kA	GPL
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
Javy	Java	k1gFnSc2	Java
verze	verze	k1gFnSc2	verze
SE	s	k7c7	s
7	[number]	k4	7
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
OpenJDK	OpenJDK	k1gFnSc4	OpenJDK
oficiální	oficiální	k2eAgFnSc7d1	oficiální
referenční	referenční	k2eAgFnSc7d1	referenční
implementací	implementace	k1gFnSc7	implementace
Javy	Java	k1gFnSc2	Java
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
Javy	Java	k1gFnSc2	Java
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgInP	být
všechny	všechen	k3xTgFnPc4	všechen
její	její	k3xOp3gFnPc4	její
implementace	implementace	k1gFnPc4	implementace
kompatibilní	kompatibilní	k2eAgMnSc1d1	kompatibilní
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
v	v	k7c4	v
právní	právní	k2eAgInSc4d1	právní
spor	spor	k1gInSc4	spor
se	s	k7c7	s
společností	společnost	k1gFnSc7	společnost
Microsoft	Microsoft	kA	Microsoft
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
Sun	sun	k1gInSc4	sun
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
implementace	implementace	k1gFnSc1	implementace
Microsoftu	Microsoft	k1gInSc2	Microsoft
(	(	kIx(	(
<g/>
Microsoft	Microsoft	kA	Microsoft
Java	Java	k1gMnSc1	Java
Virtual	Virtual	k1gMnSc1	Virtual
Machine	Machin	k1gInSc5	Machin
<g/>
)	)	kIx)	)
nepodporují	podporovat	k5eNaImIp3nP	podporovat
RMI	RMI	kA	RMI
nebo	nebo	k8xC	nebo
JNI	JNI	kA	JNI
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
si	se	k3xPyFc3	se
Microsoft	Microsoft	kA	Microsoft
přidal	přidat	k5eAaPmAgMnS	přidat
funkce	funkce	k1gFnPc4	funkce
specifické	specifický	k2eAgFnPc4d1	specifická
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gFnSc4	jejich
vlastní	vlastní	k2eAgFnSc4d1	vlastní
platformu	platforma	k1gFnSc4	platforma
<g/>
.	.	kIx.	.
</s>
<s>
Sun	Sun	kA	Sun
podal	podat	k5eAaPmAgMnS	podat
žalobu	žaloba	k1gFnSc4	žaloba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
získal	získat	k5eAaPmAgMnS	získat
vyrovnání	vyrovnání	k1gNnSc3	vyrovnání
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
20	[number]	k4	20
miliónů	milión	k4xCgInPc2	milión
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
nařízení	nařízení	k1gNnSc1	nařízení
soudu	soud	k1gInSc2	soud
přikazující	přikazující	k2eAgFnSc2d1	přikazující
Microsoftu	Microsofta	k1gFnSc4	Microsofta
dodržování	dodržování	k1gNnSc2	dodržování
licenčních	licenční	k2eAgFnPc2d1	licenční
podmínek	podmínka	k1gFnPc2	podmínka
Sunu	sunout	k5eAaImIp1nS	sunout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
Microsoft	Microsoft	kA	Microsoft
nadále	nadále	k6eAd1	nadále
neprodává	prodávat	k5eNaImIp3nS	prodávat
žádné	žádný	k3yNgNnSc1	žádný
svoje	své	k1gNnSc1	své
Windows	Windows	kA	Windows
s	s	k7c7	s
Javou	Java	k1gFnSc7	Java
(	(	kIx(	(
<g/>
Windows	Windows	kA	Windows
XP	XP	kA	XP
SP	SP	kA	SP
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
Windows	Windows	kA	Windows
Server	server	k1gInSc4	server
2003	[number]	k4	2003
a	a	k8xC	a
novější	nový	k2eAgInSc4d2	novější
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Java	Java	k1gFnSc1	Java
používá	používat	k5eAaImIp3nS	používat
automatickou	automatický	k2eAgFnSc4d1	automatická
správu	správa	k1gFnSc4	správa
paměti	paměť	k1gFnSc2	paměť
pro	pro	k7c4	pro
řízení	řízení	k1gNnSc4	řízení
paměti	paměť	k1gFnSc2	paměť
v	v	k7c4	v
object	object	k2eAgInSc4d1	object
lifecycle	lifecycle	k1gInSc4	lifecycle
<g/>
.	.	kIx.	.
</s>
<s>
Programátor	programátor	k1gMnSc1	programátor
určuje	určovat	k5eAaImIp3nS	určovat
kdy	kdy	k6eAd1	kdy
bude	být	k5eAaImBp3nS	být
objekt	objekt	k1gInSc1	objekt
vytvořen	vytvořit	k5eAaPmNgInS	vytvořit
a	a	k8xC	a
Java	Java	k1gFnSc1	Java
runtime	runtime	k1gInSc1	runtime
zodpovídá	zodpovídat	k5eAaImIp3nS	zodpovídat
za	za	k7c4	za
obnovu	obnova	k1gFnSc4	obnova
paměti	paměť	k1gFnSc2	paměť
jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
objekty	objekt	k1gInPc1	objekt
přestanou	přestat	k5eAaPmIp3nP	přestat
používat	používat	k5eAaImF	používat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
nezůstanou	zůstat	k5eNaPmIp3nP	zůstat
žádné	žádný	k3yNgInPc4	žádný
odkazy	odkaz	k1gInPc4	odkaz
na	na	k7c4	na
objekt	objekt	k1gInSc4	objekt
<g/>
,	,	kIx,	,
nepřístupná	přístupný	k2eNgFnSc1d1	nepřístupná
paměť	paměť	k1gFnSc1	paměť
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
přístupnou	přístupný	k2eAgFnSc7d1	přístupná
pro	pro	k7c4	pro
garbage	garbage	k1gNnSc4	garbage
collector	collector	k1gMnSc1	collector
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ji	on	k3xPp3gFnSc4	on
automaticky	automaticky	k6eAd1	automaticky
uvolní	uvolnit	k5eAaPmIp3nS	uvolnit
<g/>
.	.	kIx.	.
</s>
<s>
Něco	něco	k3yInSc1	něco
podobné	podobný	k2eAgFnPc1d1	podobná
úniku	únik	k1gInSc3	únik
paměti	paměť	k1gFnSc2	paměť
pořad	pořad	k1gInSc1	pořad
může	moct	k5eAaImIp3nS	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
programátorův	programátorův	k2eAgInSc1d1	programátorův
kód	kód	k1gInSc1	kód
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
referenci	reference	k1gFnSc4	reference
na	na	k7c4	na
objekt	objekt	k1gInSc4	objekt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
potřeba	potřeba	k1gFnSc1	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
účelů	účel	k1gInPc2	účel
pro	pro	k7c4	pro
automatickou	automatický	k2eAgFnSc4d1	automatická
správu	správa	k1gFnSc4	správa
paměti	paměť	k1gFnSc2	paměť
Java	Javus	k1gMnSc2	Javus
bylo	být	k5eAaImAgNnS	být
zbavit	zbavit	k5eAaPmF	zbavit
programátora	programátor	k1gMnSc4	programátor
nutnosti	nutnost	k1gFnSc2	nutnost
lámat	lámat	k5eAaImF	lámat
si	se	k3xPyFc3	se
hlavu	hlava	k1gFnSc4	hlava
nad	nad	k7c7	nad
manuální	manuální	k2eAgFnSc7d1	manuální
správou	správa	k1gFnSc7	správa
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
programovacích	programovací	k2eAgInPc6d1	programovací
jazycích	jazyk	k1gInPc6	jazyk
je	být	k5eAaImIp3nS	být
paměť	paměť	k1gFnSc1	paměť
nutná	nutný	k2eAgFnSc1d1	nutná
pro	pro	k7c4	pro
vytvoření	vytvoření	k1gNnSc4	vytvoření
objektů	objekt	k1gInPc2	objekt
implicitně	implicitně	k6eAd1	implicitně
přidělena	přidělen	k2eAgFnSc1d1	přidělena
do	do	k7c2	do
zásobníku	zásobník	k1gInSc2	zásobník
<g/>
,	,	kIx,	,
anebo	anebo	k8xC	anebo
explicitně	explicitně	k6eAd1	explicitně
přidělena	přidělit	k5eAaPmNgFnS	přidělit
a	a	k8xC	a
uvolněna	uvolnit	k5eAaPmNgFnS	uvolnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
za	za	k7c4	za
správu	správa	k1gFnSc4	správa
paměti	paměť	k1gFnSc2	paměť
padá	padat	k5eAaImIp3nS	padat
na	na	k7c4	na
programátora	programátor	k1gMnSc4	programátor
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
program	program	k1gInSc1	program
objekt	objekt	k1gInSc1	objekt
neuvolní	uvolnit	k5eNaPmIp3nS	uvolnit
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
úniku	únik	k1gInSc3	únik
paměti	paměť	k1gFnSc2	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
program	program	k1gInSc1	program
pokusí	pokusit	k5eAaPmIp3nS	pokusit
uvolnit	uvolnit	k5eAaPmF	uvolnit
paměť	paměť	k1gFnSc4	paměť
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
jíž	jenž	k3xRgFnSc3	jenž
byla	být	k5eAaImAgFnS	být
uvolněna	uvolněn	k2eAgFnSc1d1	uvolněna
<g/>
,	,	kIx,	,
výsledek	výsledek	k1gInSc1	výsledek
není	být	k5eNaImIp3nS	být
definován	definovat	k5eAaBmNgInS	definovat
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
obtížné	obtížný	k2eAgNnSc1d1	obtížné
ho	on	k3xPp3gMnSc4	on
odhadnout	odhadnout	k5eAaPmF	odhadnout
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
program	program	k1gInSc1	program
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
stane	stanout	k5eAaPmIp3nS	stanout
nestabilní	stabilní	k2eNgFnSc1d1	nestabilní
a	a	k8xC	a
<g/>
/	/	kIx~	/
<g/>
nebo	nebo	k8xC	nebo
spadne	spadnout	k5eAaPmIp3nS	spadnout
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
částečně	částečně	k6eAd1	částečně
odstraněno	odstranit	k5eAaPmNgNnS	odstranit
použitím	použití	k1gNnSc7	použití
chytrých	chytrá	k1gFnPc2	chytrá
ukazatelů	ukazatel	k1gMnPc2	ukazatel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
také	také	k9	také
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
náročnost	náročnost	k1gFnSc1	náročnost
a	a	k8xC	a
složitost	složitost	k1gFnSc1	složitost
<g/>
.	.	kIx.	.
</s>
<s>
Syntaxe	syntaxe	k1gFnSc1	syntaxe
Javy	Java	k1gFnSc2	Java
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
odvozena	odvodit	k5eAaPmNgFnS	odvodit
z	z	k7c2	z
C	C	kA	C
<g/>
++	++	k?	++
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
C	C	kA	C
<g/>
++	++	k?	++
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
kombinuje	kombinovat	k5eAaImIp3nS	kombinovat
syntaxi	syntaxe	k1gFnSc4	syntaxe
pro	pro	k7c4	pro
strukturované	strukturovaný	k2eAgFnPc4d1	strukturovaná
<g/>
,	,	kIx,	,
generické	generický	k2eAgFnPc4d1	generická
<g/>
,	,	kIx,	,
a	a	k8xC	a
objektově-orientované	objektověrientovaný	k2eAgNnSc1d1	objektově-orientovaný
programování	programování	k1gNnSc1	programování
<g/>
,	,	kIx,	,
Java	Javum	k1gNnPc1	Javum
byla	být	k5eAaImAgNnP	být
postavena	postavit	k5eAaPmNgNnP	postavit
téměř	téměř	k6eAd1	téměř
výhradně	výhradně	k6eAd1	výhradně
jako	jako	k8xS	jako
objektově	objektově	k6eAd1	objektově
orientovaný	orientovaný	k2eAgInSc1d1	orientovaný
jazyk	jazyk	k1gInSc1	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Veškerý	veškerý	k3xTgInSc1	veškerý
kód	kód	k1gInSc1	kód
je	být	k5eAaImIp3nS	být
napsán	napsat	k5eAaPmNgInS	napsat
uvnitř	uvnitř	k7c2	uvnitř
třídy	třída	k1gFnSc2	třída
a	a	k8xC	a
všechno	všechen	k3xTgNnSc1	všechen
je	být	k5eAaImIp3nS	být
objekt	objekt	k1gInSc1	objekt
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
primitivních	primitivní	k2eAgInPc2d1	primitivní
datových	datový	k2eAgInPc2d1	datový
typů	typ	k1gInPc2	typ
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
celá	celý	k2eAgNnPc1d1	celé
čísla	číslo	k1gNnPc1	číslo
<g/>
,	,	kIx,	,
desetinná	desetinný	k2eAgNnPc1d1	desetinné
čísla	číslo	k1gNnPc1	číslo
<g/>
,	,	kIx,	,
logické	logický	k2eAgFnPc1d1	logická
hodnoty	hodnota	k1gFnPc1	hodnota
a	a	k8xC	a
znaky	znak	k1gInPc1	znak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
třídy	třída	k1gFnPc1	třída
z	z	k7c2	z
výkonnostních	výkonnostní	k2eAgInPc2d1	výkonnostní
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
C	C	kA	C
<g/>
++	++	k?	++
Java	Java	k1gMnSc1	Java
nepodporuje	podporovat	k5eNaImIp3nS	podporovat
přetěžování	přetěžování	k1gNnSc4	přetěžování
operátorů	operátor	k1gInPc2	operátor
nebo	nebo	k8xC	nebo
vícenásobnou	vícenásobný	k2eAgFnSc4d1	vícenásobná
dědičnost	dědičnost	k1gFnSc4	dědičnost
pro	pro	k7c4	pro
třídy	třída	k1gFnPc4	třída
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
zjednodušuje	zjednodušovat	k5eAaImIp3nS	zjednodušovat
jazyk	jazyk	k1gInSc1	jazyk
a	a	k8xC	a
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
při	při	k7c6	při
prevenci	prevence	k1gFnSc6	prevence
potenciálních	potenciální	k2eAgFnPc2d1	potenciální
chyb	chyba	k1gFnPc2	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Java	Java	k6eAd1	Java
používá	používat	k5eAaImIp3nS	používat
podobné	podobný	k2eAgFnPc4d1	podobná
komentovací	komentovací	k2eAgFnPc4d1	komentovací
metody	metoda	k1gFnPc4	metoda
jako	jako	k8xC	jako
C	C	kA	C
<g/>
++	++	k?	++
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
tři	tři	k4xCgInPc1	tři
různé	různý	k2eAgInPc1d1	různý
styly	styl	k1gInPc1	styl
komentářů	komentář	k1gInPc2	komentář
<g/>
:	:	kIx,	:
jednořádkový	jednořádkový	k2eAgInSc1d1	jednořádkový
uvozený	uvozený	k2eAgInSc1d1	uvozený
dvěma	dva	k4xCgNnPc7	dva
lomítky	lomítko	k1gNnPc7	lomítko
(	(	kIx(	(
<g/>
//	//	k?	//
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
víceřádkový	víceřádkový	k2eAgMnSc1d1	víceřádkový
uvozený	uvozený	k2eAgMnSc1d1	uvozený
/	/	kIx~	/
<g/>
*	*	kIx~	*
a	a	k8xC	a
uzavřený	uzavřený	k2eAgInSc4d1	uzavřený
s	s	k7c7	s
*	*	kIx~	*
<g/>
/	/	kIx~	/
a	a	k8xC	a
Javadoc	Javadoc	k1gInSc4	Javadoc
dokumentační	dokumentační	k2eAgInSc1d1	dokumentační
styl	styl	k1gInSc1	styl
komentování	komentování	k1gNnSc1	komentování
uvozený	uvozený	k2eAgMnSc1d1	uvozený
/	/	kIx~	/
<g/>
**	**	k?	**
a	a	k8xC	a
uzavřený	uzavřený	k2eAgInSc1d1	uzavřený
*	*	kIx~	*
<g/>
/	/	kIx~	/
<g/>
.	.	kIx.	.
</s>
<s>
Komentář	komentář	k1gInSc1	komentář
typu	typ	k1gInSc2	typ
Javadoc	Javadoc	k1gFnSc4	Javadoc
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
uživateli	uživatel	k1gMnSc3	uživatel
sestavit	sestavit	k5eAaPmF	sestavit
dokumentaci	dokumentace	k1gFnSc4	dokumentace
k	k	k7c3	k
programu	program	k1gInSc3	program
<g/>
.	.	kIx.	.
</s>
<s>
Ukázka	ukázka	k1gFnSc1	ukázka
použití	použití	k1gNnSc2	použití
komentářů	komentář	k1gInPc2	komentář
<g/>
:	:	kIx,	:
Tradiční	tradiční	k2eAgInSc1d1	tradiční
program	program	k1gInSc1	program
"	"	kIx"	"
<g/>
Hello	Hello	k1gNnSc1	Hello
world	worlda	k1gFnPc2	worlda
<g/>
"	"	kIx"	"
vypadá	vypadat	k5eAaPmIp3nS	vypadat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
Netradiční	tradiční	k2eNgFnSc1d1	netradiční
verze	verze	k1gFnSc1	verze
"	"	kIx"	"
<g/>
Hello	Hello	k1gNnSc1	Hello
world	worlda	k1gFnPc2	worlda
<g/>
"	"	kIx"	"
s	s	k7c7	s
malou	malý	k2eAgFnSc7d1	malá
ukázkou	ukázka	k1gFnSc7	ukázka
objektového	objektový	k2eAgInSc2d1	objektový
přístupu	přístup	k1gInSc2	přístup
<g/>
:	:	kIx,	:
Společnosti	společnost	k1gFnSc2	společnost
Google	Google	k1gInSc1	Google
a	a	k8xC	a
Android	android	k1gInSc1	android
si	se	k3xPyFc3	se
zvolily	zvolit	k5eAaPmAgFnP	zvolit
použití	použití	k1gNnSc4	použití
Javy	Java	k1gFnSc2	Java
jako	jako	k8xS	jako
klíčový	klíčový	k2eAgInSc4d1	klíčový
pilíř	pilíř	k1gInSc4	pilíř
při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
open	open	k1gNnSc4	open
source	source	k1gMnSc1	source
mobilního	mobilní	k2eAgInSc2d1	mobilní
operačního	operační	k2eAgInSc2d1	operační
systému	systém	k1gInSc2	systém
určeného	určený	k2eAgInSc2d1	určený
pro	pro	k7c4	pro
chytré	chytrý	k2eAgInPc4d1	chytrý
telefony	telefon	k1gInPc4	telefon
a	a	k8xC	a
tablety	tableta	k1gFnPc4	tableta
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Android	android	k1gInSc1	android
postaven	postavit	k5eAaPmNgInS	postavit
na	na	k7c6	na
Linuxovém	linuxový	k2eAgNnSc6d1	linuxové
jádře	jádro	k1gNnSc6	jádro
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
napsán	napsat	k5eAaBmNgInS	napsat
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
programovacím	programovací	k2eAgInSc6d1	programovací
jazyce	jazyk	k1gInSc6	jazyk
C	C	kA	C
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
SDK	SDK	kA	SDK
Androidu	android	k1gInSc2	android
používá	používat	k5eAaImIp3nS	používat
jazyk	jazyk	k1gInSc1	jazyk
Java	Jav	k1gInSc2	Jav
jako	jako	k8xC	jako
základ	základ	k1gInSc4	základ
pro	pro	k7c4	pro
svoje	svůj	k3xOyFgFnPc4	svůj
aplikace	aplikace	k1gFnPc4	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Java	Java	k6eAd1	Java
je	být	k5eAaImIp3nS	být
však	však	k9	však
používána	používán	k2eAgFnSc1d1	používána
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
syntaxi	syntaxe	k1gFnSc4	syntaxe
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
pro	pro	k7c4	pro
svoji	svůj	k3xOyFgFnSc4	svůj
knihovnu	knihovna	k1gFnSc4	knihovna
tříd	třída	k1gFnPc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Namísto	namísto	k7c2	namísto
vytváření	vytváření	k1gNnSc2	vytváření
instance	instance	k1gFnSc2	instance
třídy	třída	k1gFnSc2	třída
ze	z	k7c2	z
standardní	standardní	k2eAgFnSc1d1	standardní
Java	Java	k1gFnSc1	Java
Class	Classa	k1gFnPc2	Classa
Library	Librara	k1gFnSc2	Librara
Android	android	k1gInSc1	android
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
svoji	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
knihovní	knihovní	k2eAgFnSc4d1	knihovní
třídu	třída	k1gFnSc4	třída
<g/>
.	.	kIx.	.
</s>
<s>
Aplikace	aplikace	k1gFnSc1	aplikace
pro	pro	k7c4	pro
Android	android	k1gInSc4	android
jsou	být	k5eAaImIp3nP	být
kompilovány	kompilovat	k5eAaImNgInP	kompilovat
přes	přes	k7c4	přes
bajtkód	bajtkód	k1gInSc4	bajtkód
Javy	Java	k1gFnSc2	Java
jako	jako	k8xS	jako
mezistupeň	mezistupeň	k1gInSc1	mezistupeň
do	do	k7c2	do
formátu	formát	k1gInSc2	formát
Dalvik	Dalvik	k1gMnSc1	Dalvik
Executables	Executables	k1gMnSc1	Executables
(	(	kIx(	(
<g/>
přípona	přípona	k1gFnSc1	přípona
.	.	kIx.	.
<g/>
dex	dex	k?	dex
<g/>
)	)	kIx)	)
spustitelného	spustitelný	k2eAgInSc2d1	spustitelný
ve	v	k7c4	v
vlastní	vlastní	k2eAgInSc4d1	vlastní
Dalvik	Dalvik	k1gInSc4	Dalvik
Virtual	Virtual	k1gMnSc1	Virtual
Machine	Machin	k1gInSc5	Machin
Androidu	android	k1gInSc3	android
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
třídy	třída	k1gFnPc1	třída
v	v	k7c6	v
Dalvikově	Dalvikův	k2eAgFnSc6d1	Dalvikův
knihovně	knihovna	k1gFnSc6	knihovna
se	se	k3xPyFc4	se
podobají	podobat	k5eAaImIp3nP	podobat
jejím	její	k3xOp3gInPc3	její
protějškům	protějšek	k1gInPc3	protějšek
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
Java	Jav	k1gInSc2	Jav
<g/>
.	.	kIx.	.
</s>
<s>
Rozdíly	rozdíl	k1gInPc1	rozdíl
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
hlavním	hlavní	k2eAgInSc7d1	hlavní
bodem	bod	k1gInSc7	bod
sporu	spor	k1gInSc2	spor
mezi	mezi	k7c7	mezi
Sun	Sun	kA	Sun
<g/>
/	/	kIx~	/
<g/>
Oracle	Oracl	k1gMnSc2	Oracl
a	a	k8xC	a
Google	Googl	k1gMnSc2	Googl
<g/>
/	/	kIx~	/
<g/>
Android	android	k1gInSc1	android
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
soud	soud	k1gInSc1	soud
v	v	k7c4	v
San	San	k1gFnSc4	San
Francisku	Franciska	k1gFnSc4	Franciska
shledal	shledat	k5eAaPmAgMnS	shledat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
by	by	kYmCp3nP	by
API	API	kA	API
bylo	být	k5eAaImAgNnS	být
chráněno	chránit	k5eAaImNgNnS	chránit
copyrightem	copyright	k1gInSc7	copyright
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
by	by	kYmCp3nS	by
společnost	společnost	k1gFnSc1	společnost
Google	Google	k1gNnSc2	Google
porušila	porušit	k5eAaPmAgFnS	porušit
autorská	autorský	k2eAgNnPc4d1	autorské
práva	právo	k1gNnPc4	právo
společnosti	společnost	k1gFnSc2	společnost
Oracle	Oracle	k1gNnSc2	Oracle
použitím	použití	k1gNnSc7	použití
Javy	Java	k1gFnSc2	Java
v	v	k7c6	v
zařízeních	zařízení	k1gNnPc6	zařízení
s	s	k7c7	s
Androidem	android	k1gInSc7	android
<g/>
.	.	kIx.	.
</s>
<s>
Oracle	Oracle	k6eAd1	Oracle
vznesl	vznést	k5eAaPmAgMnS	vznést
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
postoji	postoj	k1gInSc6	postoj
otázky	otázka	k1gFnSc2	otázka
týkající	týkající	k2eAgFnSc2d1	týkající
se	se	k3xPyFc4	se
legálnosti	legálnost	k1gFnSc2	legálnost
používání	používání	k1gNnSc2	používání
Javy	Java	k1gFnSc2	Java
v	v	k7c6	v
Androidu	android	k1gInSc6	android
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
americký	americký	k2eAgMnSc1d1	americký
okresní	okresní	k2eAgMnSc1d1	okresní
soudce	soudce	k1gMnSc1	soudce
William	William	k1gInSc4	William
Haskell	Haskell	k1gInSc1	Haskell
Alsup	Alsup	k1gInSc1	Alsup
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
dne	den	k1gInSc2	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
že	že	k8xS	že
API	API	kA	API
nemůže	moct	k5eNaImIp3nS	moct
být	být	k5eAaImF	být
chráněno	chránit	k5eAaImNgNnS	chránit
autorskými	autorský	k2eAgNnPc7d1	autorské
právy	právo	k1gNnPc7	právo
<g/>
.	.	kIx.	.
</s>
<s>
Spor	spor	k1gInSc1	spor
dále	daleko	k6eAd2	daleko
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
vybraných	vybraný	k2eAgInPc2d1	vybraný
IDE	IDE	kA	IDE
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
v	v	k7c6	v
jazyce	jazyk	k1gInSc6	jazyk
Java	Jav	k1gInSc2	Jav
(	(	kIx(	(
<g/>
řazeno	řadit	k5eAaImNgNnS	řadit
abecedně	abecedně	k6eAd1	abecedně
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
BlueJ	BlueJ	k1gMnSc1	BlueJ
–	–	k?	–
multiplatformní	multiplatformní	k2eAgNnSc1d1	multiplatformní
vývojové	vývojový	k2eAgNnSc1d1	vývojové
prostředí	prostředí	k1gNnSc1	prostředí
(	(	kIx(	(
<g/>
volně	volně	k6eAd1	volně
šiřitelné	šiřitelný	k2eAgNnSc1d1	šiřitelné
<g/>
)	)	kIx)	)
Eclipse	Eclips	k1gMnSc4	Eclips
–	–	k?	–
multiplatformní	multiplatformní	k2eAgNnSc1d1	multiplatformní
vývojové	vývojový	k2eAgNnSc1d1	vývojové
prostředí	prostředí	k1gNnSc1	prostředí
pro	pro	k7c4	pro
programování	programování	k1gNnSc4	programování
v	v	k7c6	v
jazycích	jazyk	k1gInPc6	jazyk
Java	Javus	k1gMnSc2	Javus
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
++	++	k?	++
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
(	(	kIx(	(
<g/>
volně	volně	k6eAd1	volně
šiřitelné	šiřitelný	k2eAgNnSc1d1	šiřitelné
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
IntelliJ	IntelliJ	k?	IntelliJ
IDEA	idea	k1gFnSc1	idea
–	–	k?	–
vývojové	vývojový	k2eAgNnSc4d1	vývojové
prostředí	prostředí	k1gNnSc4	prostředí
pro	pro	k7c4	pro
programování	programování	k1gNnSc4	programování
v	v	k7c6	v
jazycích	jazyk	k1gInPc6	jazyk
Java	Javum	k1gNnSc2	Javum
<g/>
,	,	kIx,	,
Groovy	Groův	k2eAgFnSc2d1	Groův
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
(	(	kIx(	(
<g/>
komerční	komerční	k2eAgFnSc1d1	komerční
<g/>
)	)	kIx)	)
JDeveloper	JDeveloper	k1gMnSc1	JDeveloper
–	–	k?	–
vývojové	vývojový	k2eAgNnSc1d1	vývojové
prostředí	prostředí	k1gNnSc1	prostředí
firmy	firma	k1gFnSc2	firma
Oracle	Oracle	k1gFnSc2	Oracle
(	(	kIx(	(
<g/>
volně	volně	k6eAd1	volně
šiřitelné	šiřitelný	k2eAgInPc1d1	šiřitelný
<g/>
)	)	kIx)	)
NetBeans	NetBeans	k1gInSc1	NetBeans
–	–	k?	–
vývojové	vývojový	k2eAgNnSc4d1	vývojové
prostředí	prostředí	k1gNnSc4	prostředí
pro	pro	k7c4	pro
Javu	Java	k1gFnSc4	Java
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
/	/	kIx~	/
<g/>
C	C	kA	C
<g/>
++	++	k?	++
<g/>
,	,	kIx,	,
Groovy	Groův	k2eAgFnSc2d1	Groův
a	a	k8xC	a
PHP	PHP	kA	PHP
(	(	kIx(	(
<g/>
volně	volně	k6eAd1	volně
šiřitelné	šiřitelný	k2eAgNnSc1d1	šiřitelné
<g/>
)	)	kIx)	)
</s>
