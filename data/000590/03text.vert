<s>
Golfský	golfský	k2eAgInSc4d1	golfský
proud	proud	k1gInSc4	proud
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc1	jeho
severní	severní	k2eAgFnPc1d1	severní
větve	větev	k1gFnPc1	větev
-	-	kIx~	-
Irmingerův	Irmingerův	k2eAgInSc4d1	Irmingerův
<g/>
,	,	kIx,	,
Norský	norský	k2eAgInSc4d1	norský
a	a	k8xC	a
Severoatlantický	severoatlantický	k2eAgInSc4d1	severoatlantický
proud	proud	k1gInSc4	proud
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
silný	silný	k2eAgInSc1d1	silný
<g/>
,	,	kIx,	,
teplý	teplý	k2eAgInSc1d1	teplý
a	a	k8xC	a
poměrně	poměrně	k6eAd1	poměrně
rychlý	rychlý	k2eAgInSc1d1	rychlý
mořský	mořský	k2eAgInSc1d1	mořský
proud	proud	k1gInSc1	proud
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
Mexickém	mexický	k2eAgInSc6d1	mexický
zálivu	záliv	k1gInSc6	záliv
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
(	(	kIx(	(
<g/>
gulf	gulf	k1gInSc1	gulf
=	=	kIx~	=
anglicky	anglicky	k6eAd1	anglicky
záliv	záliv	k1gInSc1	záliv
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
Floridským	floridský	k2eAgInSc7d1	floridský
průlivem	průliv	k1gInSc7	průliv
<g/>
,	,	kIx,	,
sleduje	sledovat	k5eAaImIp3nS	sledovat
pobřeží	pobřeží	k1gNnSc1	pobřeží
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
k	k	k7c3	k
ostrovu	ostrov	k1gInSc3	ostrov
Newfoundland	Newfoundlanda	k1gFnPc2	Newfoundlanda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
odchyluje	odchylovat	k5eAaImIp3nS	odchylovat
od	od	k7c2	od
pevniny	pevnina	k1gFnSc2	pevnina
a	a	k8xC	a
pak	pak	k6eAd1	pak
přechází	přecházet	k5eAaImIp3nS	přecházet
přes	přes	k7c4	přes
Atlantský	atlantský	k2eAgInSc4d1	atlantský
oceán	oceán	k1gInSc4	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
na	na	k7c4	na
40	[number]	k4	40
<g/>
°	°	k?	°
s.	s.	k?	s.
š.	š.	k?	š.
a	a	k8xC	a
30	[number]	k4	30
<g/>
°	°	k?	°
z.	z.	k?	z.
d.	d.	k?	d.
se	se	k3xPyFc4	se
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
větve	větev	k1gFnPc4	větev
<g/>
:	:	kIx,	:
severní	severní	k2eAgInSc1d1	severní
proud	proud	k1gInSc1	proud
míří	mířit	k5eAaImIp3nS	mířit
k	k	k7c3	k
severní	severní	k2eAgFnSc3d1	severní
Evropě	Evropa	k1gFnSc3	Evropa
a	a	k8xC	a
jižní	jižní	k2eAgInPc1d1	jižní
se	se	k3xPyFc4	se
obrací	obracet	k5eAaImIp3nP	obracet
k	k	k7c3	k
západnímu	západní	k2eAgNnSc3d1	západní
pobřeží	pobřeží	k1gNnSc3	pobřeží
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Golfský	golfský	k2eAgInSc1d1	golfský
proud	proud	k1gInSc1	proud
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
100	[number]	k4	100
kilometrů	kilometr	k1gInPc2	kilometr
široký	široký	k2eAgMnSc1d1	široký
a	a	k8xC	a
800	[number]	k4	800
až	až	k9	až
1200	[number]	k4	1200
metrů	metr	k1gInPc2	metr
hluboký	hluboký	k2eAgInSc1d1	hluboký
<g/>
.	.	kIx.	.
</s>
<s>
Golfský	golfský	k2eAgInSc1d1	golfský
proud	proud	k1gInSc1	proud
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
podnebí	podnebí	k1gNnSc4	podnebí
východního	východní	k2eAgNnSc2d1	východní
pobřeží	pobřeží	k1gNnSc2	pobřeží
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
od	od	k7c2	od
Floridy	Florida	k1gFnSc2	Florida
po	po	k7c4	po
Newfoundland	Newfoundland	k1gInSc4	Newfoundland
a	a	k8xC	a
západního	západní	k2eAgNnSc2d1	západní
pobřeží	pobřeží	k1gNnSc2	pobřeží
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
větev	větev	k1gFnSc1	větev
Golfského	golfský	k2eAgInSc2d1	golfský
proudu	proud	k1gInSc2	proud
<g/>
,	,	kIx,	,
nazývaná	nazývaný	k2eAgFnSc1d1	nazývaná
Severoatlantický	severoatlantický	k2eAgInSc4d1	severoatlantický
proud	proud	k1gInSc4	proud
<g/>
,	,	kIx,	,
zmírňuje	zmírňovat	k5eAaImIp3nS	zmírňovat
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
zimy	zima	k1gFnSc2	zima
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
teplejší	teplý	k2eAgFnPc1d2	teplejší
než	než	k8xS	než
na	na	k7c6	na
jiných	jiný	k2eAgNnPc6d1	jiné
místech	místo	k1gNnPc6	místo
Země	zem	k1gFnSc2	zem
se	se	k3xPyFc4	se
stejnou	stejný	k2eAgFnSc4d1	stejná
zeměpisnou	zeměpisný	k2eAgFnSc4d1	zeměpisná
šířku	šířka	k1gFnSc4	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
činí	činit	k5eAaImIp3nS	činit
rozdíl	rozdíl	k1gInSc1	rozdíl
průměrných	průměrný	k2eAgFnPc2d1	průměrná
teplot	teplota	k1gFnPc2	teplota
mezi	mezi	k7c7	mezi
pobřežím	pobřeží	k1gNnSc7	pobřeží
Norska	Norsko	k1gNnSc2	Norsko
a	a	k8xC	a
severními	severní	k2eAgFnPc7d1	severní
částmi	část	k1gFnPc7	část
Kanady	Kanada	k1gFnSc2	Kanada
přibližně	přibližně	k6eAd1	přibližně
30	[number]	k4	30
°	°	k?	°
<g/>
C.	C.	kA	C.
Na	na	k7c4	na
Golfský	golfský	k2eAgInSc4d1	golfský
proud	proud	k1gInSc4	proud
má	mít	k5eAaImIp3nS	mít
vliv	vliv	k1gInSc1	vliv
stav	stav	k1gInSc4	stav
atmosféry	atmosféra	k1gFnSc2	atmosféra
a	a	k8xC	a
proudění	proudění	k1gNnSc2	proudění
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
tzv.	tzv.	kA	tzv.
severoatlantická	severoatlantický	k2eAgFnSc1d1	Severoatlantická
oscilace	oscilace	k1gFnSc1	oscilace
mění	měnit	k5eAaImIp3nS	měnit
rychlost	rychlost	k1gFnSc4	rychlost
proudu	proud	k1gInSc2	proud
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
v	v	k7c6	v
přímé	přímý	k2eAgFnSc6d1	přímá
i	i	k8xC	i
nepřímé	přímý	k2eNgFnSc6d1	nepřímá
interakci	interakce	k1gFnSc6	interakce
<g/>
.	.	kIx.	.
</s>
<s>
Golfský	golfský	k2eAgInSc1d1	golfský
proud	proud	k1gInSc1	proud
je	být	k5eAaImIp3nS	být
také	také	k9	také
součástí	součást	k1gFnSc7	součást
termohalinní	termohalinný	k2eAgMnPc1d1	termohalinný
cirkulace	cirkulace	k1gFnPc1	cirkulace
probíhající	probíhající	k2eAgFnPc1d1	probíhající
v	v	k7c6	v
oceánském	oceánský	k2eAgInSc6d1	oceánský
bazénu	bazén	k1gInSc6	bazén
<g/>
.	.	kIx.	.
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
Golfský	golfský	k2eAgInSc1d1	golfský
proud	proud	k1gInSc1	proud
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Golfský	golfský	k2eAgInSc4d1	golfský
proud	proud	k1gInSc4	proud
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Surface	Surface	k1gFnSc1	Surface
Currents	Currents	k1gInSc1	Currents
in	in	k?	in
the	the	k?	the
Atlantic	Atlantice	k1gFnPc2	Atlantice
Ocean	Oceany	k1gInPc2	Oceany
</s>
