<p>
<s>
Safír	safír	k1gInSc1	safír
(	(	kIx(	(
<g/>
název	název	k1gInSc1	název
z	z	k7c2	z
hebrejštiny	hebrejština	k1gFnSc2	hebrejština
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
monokrystal	monokrystal	k1gInSc4	monokrystal
oxidu	oxid	k1gInSc2	oxid
hlinitého	hlinitý	k2eAgInSc2d1	hlinitý
(	(	kIx(	(
<g/>
Al	ala	k1gFnPc2	ala
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
minerálu	minerál	k1gInSc2	minerál
zvaného	zvaný	k2eAgInSc2d1	zvaný
korund	korund	k1gInSc1	korund
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
buď	buď	k8xC	buď
jako	jako	k9	jako
přírodní	přírodní	k2eAgInSc4d1	přírodní
drahokam	drahokam	k1gInSc4	drahokam
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
uměle	uměle	k6eAd1	uměle
pro	pro	k7c4	pro
řadu	řada	k1gFnSc4	řada
rozličných	rozličný	k2eAgFnPc2d1	rozličná
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
tvrdost	tvrdost	k1gFnSc4	tvrdost
na	na	k7c4	na
sklíčka	sklíčko	k1gNnPc4	sklíčko
a	a	k8xC	a
ložiska	ložisko	k1gNnPc4	ložisko
hodinek	hodinka	k1gFnPc2	hodinka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zatímco	zatímco	k8xS	zatímco
korund	korund	k1gInSc1	korund
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
čistého	čistý	k2eAgInSc2d1	čistý
oxidu	oxid	k1gInSc2	oxid
hlinitého	hlinitý	k2eAgInSc2d1	hlinitý
<g/>
,	,	kIx,	,
safíry	safír	k1gInPc1	safír
vždy	vždy	k6eAd1	vždy
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
příměs	příměs	k1gFnSc4	příměs
jiných	jiný	k2eAgInPc2d1	jiný
prvků	prvek	k1gInPc2	prvek
(	(	kIx(	(
<g/>
železo	železo	k1gNnSc1	železo
<g/>
,	,	kIx,	,
chrom	chrom	k1gInSc1	chrom
<g/>
,	,	kIx,	,
měď	měď	k1gFnSc1	měď
<g/>
,	,	kIx,	,
hořčík	hořčík	k1gInSc1	hořčík
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
které	který	k3yRgFnPc1	který
mu	on	k3xPp3gMnSc3	on
dodávají	dodávat	k5eAaImIp3nP	dodávat
modrou	modrý	k2eAgFnSc4d1	modrá
<g/>
,	,	kIx,	,
červenou	červený	k2eAgFnSc4d1	červená
<g/>
,	,	kIx,	,
žlutou	žlutý	k2eAgFnSc4d1	žlutá
<g/>
,	,	kIx,	,
růžovou	růžový	k2eAgFnSc4d1	růžová
<g/>
,	,	kIx,	,
purpurovou	purpurový	k2eAgFnSc4d1	purpurová
<g/>
,	,	kIx,	,
oranžovou	oranžový	k2eAgFnSc4d1	oranžová
nebo	nebo	k8xC	nebo
zelenavou	zelenavý	k2eAgFnSc4d1	zelenavá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
safíry	safír	k1gInPc7	safír
se	se	k3xPyFc4	se
zařazují	zařazovat	k5eAaImIp3nP	zařazovat
všechny	všechen	k3xTgInPc1	všechen
drahokamy	drahokam	k1gInPc1	drahokam
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
odrůdy	odrůda	k1gFnPc4	odrůda
korundu	korund	k1gInSc2	korund
kromě	kromě	k7c2	kromě
červeného	červené	k1gNnSc2	červené
zvaného	zvaný	k2eAgNnSc2d1	zvané
též	též	k9	též
rubín	rubín	k1gInSc1	rubín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
drahokamu	drahokam	k1gInSc2	drahokam
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
a	a	k8xC	a
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
nejčastější	častý	k2eAgFnSc4d3	nejčastější
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Zni	znít	k5eAaImRp2nS	znít
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
SAPHIRUS	SAPHIRUS	kA	SAPHIRUS
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
modrý	modrý	k2eAgInSc1d1	modrý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přírodní	přírodní	k2eAgInPc1d1	přírodní
safíry	safír	k1gInPc1	safír
==	==	k?	==
</s>
</p>
<p>
<s>
Modré	modrý	k2eAgInPc1d1	modrý
safíry	safír	k1gInPc1	safír
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
širokým	široký	k2eAgNnSc7d1	široké
spektrem	spektrum	k1gNnSc7	spektrum
odstínů	odstín	k1gInPc2	odstín
<g/>
;	;	kIx,	;
modrá	modrý	k2eAgFnSc1d1	modrá
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
důsledkem	důsledek	k1gInSc7	důsledek
obsahu	obsah	k1gInSc2	obsah
titanu	titan	k1gInSc2	titan
a	a	k8xC	a
železa	železo	k1gNnSc2	železo
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
drahokamy	drahokam	k1gInPc1	drahokam
mají	mít	k5eAaImIp3nP	mít
barvy	barva	k1gFnPc1	barva
bledší	bledý	k2eAgFnSc1d2	bledší
a	a	k8xC	a
vzhled	vzhled	k1gInSc1	vzhled
je	být	k5eAaImIp3nS	být
šedavý	šedavý	k2eAgInSc1d1	šedavý
<g/>
.	.	kIx.	.
</s>
<s>
Přírodní	přírodní	k2eAgInPc1d1	přírodní
safíry	safír	k1gInPc1	safír
se	se	k3xPyFc4	se
často	často	k6eAd1	často
pro	pro	k7c4	pro
zvýraznění	zvýraznění	k1gNnSc4	zvýraznění
barev	barva	k1gFnPc2	barva
žíhají	žíhat	k5eAaImIp3nP	žíhat
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
provádí	provádět	k5eAaImIp3nS	provádět
ohřevem	ohřev	k1gInSc7	ohřev
na	na	k7c4	na
teplotu	teplota	k1gFnSc4	teplota
až	až	k9	až
1800	[number]	k4	1800
°	°	k?	°
<g/>
C	C	kA	C
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
několika	několik	k4yIc2	několik
hodin	hodina	k1gFnPc2	hodina
nebo	nebo	k8xC	nebo
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
s	s	k7c7	s
nižším	nízký	k2eAgInSc7d2	nižší
obsahem	obsah	k1gInSc7	obsah
dusíku	dusík	k1gInSc2	dusík
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
týdne	týden	k1gInSc2	týden
i	i	k9	i
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pozorování	pozorování	k1gNnSc6	pozorování
pod	pod	k7c7	pod
mikroskopem	mikroskop	k1gInSc7	mikroskop
je	být	k5eAaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
hedvábný	hedvábný	k2eAgInSc4d1	hedvábný
lesk	lesk	k1gInSc4	lesk
který	který	k3yIgInSc4	který
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
inkluze	inkluze	k1gFnPc1	inkluze
rutilu	rutil	k1gInSc2	rutil
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
jehlic	jehlice	k1gFnPc2	jehlice
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
jehlice	jehlice	k1gFnSc2	jehlice
celistvé	celistvý	k2eAgFnPc1d1	celistvá
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
známkou	známka	k1gFnSc7	známka
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
kámen	kámen	k1gInSc1	kámen
nebyl	být	k5eNaImAgInS	být
ohříván	ohřívat	k5eAaImNgInS	ohřívat
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
hedvábný	hedvábný	k2eAgInSc4d1	hedvábný
lesk	lesk	k1gInSc4	lesk
není	být	k5eNaImIp3nS	být
vidět	vidět	k5eAaImF	vidět
znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
kámen	kámen	k1gInSc1	kámen
byl	být	k5eAaImAgInS	být
přiměřeně	přiměřeně	k6eAd1	přiměřeně
zahřát	zahřát	k5eAaPmNgInS	zahřát
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
hedvábný	hedvábný	k2eAgInSc4d1	hedvábný
lesk	lesk	k1gInSc4	lesk
částečně	částečně	k6eAd1	částečně
narušen	narušen	k2eAgMnSc1d1	narušen
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
surový	surový	k2eAgInSc1d1	surový
kámen	kámen	k1gInSc1	kámen
byl	být	k5eAaImAgInS	být
ohříván	ohřívat	k5eAaImNgInS	ohřívat
nad	nad	k7c7	nad
dřevěným	dřevěný	k2eAgNnSc7d1	dřevěné
uhlím	uhlí	k1gNnSc7	uhlí
<g/>
;	;	kIx,	;
ohřev	ohřev	k1gInSc1	ohřev
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
20	[number]	k4	20
až	až	k9	až
30	[number]	k4	30
minut	minuta	k1gFnPc2	minuta
při	při	k7c6	při
teplotě	teplota	k1gFnSc6	teplota
1300	[number]	k4	1300
°	°	k?	°
<g/>
C	C	kA	C
pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
sytosti	sytost	k1gFnSc2	sytost
barev	barva	k1gFnPc2	barva
a	a	k8xC	a
odstranění	odstranění	k1gNnSc1	odstranění
šedých	šedý	k2eAgInPc2d1	šedý
nebo	nebo	k8xC	nebo
hnědých	hnědý	k2eAgInPc2d1	hnědý
odstínů	odstín	k1gInPc2	odstín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Safíry	safír	k1gInPc1	safír
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nP	dělit
na	na	k7c4	na
modré	modrý	k2eAgFnPc4d1	modrá
a	a	k8xC	a
fancy	fanc	k2eAgFnPc4d1	fanc
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
čili	čili	k8xC	čili
nemodré	modrý	k2eNgInPc1d1	modrý
odstíny	odstín	k1gInPc1	odstín
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýtečnější	výtečný	k2eAgFnPc4d3	výtečný
barvy	barva	k1gFnPc4	barva
mají	mít	k5eAaImIp3nP	mít
safíry	safír	k1gInPc1	safír
kášmírské	kášmírský	k2eAgInPc1d1	kášmírský
<g/>
,	,	kIx,	,
za	za	k7c7	za
nimi	on	k3xPp3gFnPc7	on
následují	následovat	k5eAaImIp3nP	následovat
barmské	barmský	k2eAgFnPc1d1	barmská
a	a	k8xC	a
srílanské	srílanský	k2eAgFnPc1d1	Srílanská
<g/>
,	,	kIx,	,
na	na	k7c6	na
ostatních	ostatní	k2eAgNnPc6d1	ostatní
nalezištích	naleziště	k1gNnPc6	naleziště
(	(	kIx(	(
<g/>
Austrálie	Austrálie	k1gFnSc1	Austrálie
<g/>
,	,	kIx,	,
Thajsko	Thajsko	k1gNnSc1	Thajsko
<g/>
,	,	kIx,	,
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
<g/>
,	,	kIx,	,
Laos	Laos	k1gInSc1	Laos
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
také	také	k9	také
krásné	krásný	k2eAgInPc4d1	krásný
modré	modrý	k2eAgInPc4d1	modrý
kameny	kámen	k1gInPc4	kámen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většinou	většina	k1gFnSc7	většina
jsou	být	k5eAaImIp3nP	být
podbarveny	podbarvit	k5eAaPmNgFnP	podbarvit
hnědou	hnědý	k2eAgFnSc7d1	hnědá
či	či	k8xC	či
zelenou	zelený	k2eAgFnSc7d1	zelená
barvou	barva	k1gFnSc7	barva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mimo	mimo	k6eAd1	mimo
modrý	modrý	k2eAgInSc1d1	modrý
safír	safír	k1gInSc1	safír
je	být	k5eAaImIp3nS	být
nejcennějším	cenný	k2eAgInSc7d3	nejcennější
safírem	safír	k1gInSc7	safír
padparádža	padparádža	k1gFnSc1	padparádža
<g/>
,	,	kIx,	,
padparascha	padparascha	k1gFnSc1	padparascha
safír	safír	k1gInSc1	safír
barvy	barva	k1gFnSc2	barva
lososové	lososový	k2eAgFnSc2d1	lososová
červeně	červeň	k1gFnSc2	červeň
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
zaměňuje	zaměňovat	k5eAaImIp3nS	zaměňovat
za	za	k7c4	za
oranžový	oranžový	k2eAgInSc4d1	oranžový
safír	safír	k1gInSc4	safír
a	a	k8xC	a
růžový	růžový	k2eAgInSc1d1	růžový
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
lest	lest	k1gFnSc1	lest
obchodníků	obchodník	k1gMnPc2	obchodník
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Správný	správný	k2eAgInSc1d1	správný
poměr	poměr	k1gInSc1	poměr
žluté	žlutý	k2eAgFnSc2d1	žlutá
a	a	k8xC	a
oranžové	oranžový	k2eAgFnSc2d1	oranžová
barvy	barva	k1gFnSc2	barva
je	být	k5eAaImIp3nS	být
zárukou	záruka	k1gFnSc7	záruka
oné	onen	k3xDgFnSc2	onen
výjimečné	výjimečný	k2eAgFnSc2d1	výjimečná
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Starověké	starověký	k2eAgMnPc4d1	starověký
lapidáře	lapidář	k1gMnPc4	lapidář
indické	indický	k2eAgMnPc4d1	indický
řadí	řadit	k5eAaImIp3nP	řadit
padparádžu	padparádzat	k5eAaPmIp1nS	padparádzat
(	(	kIx(	(
<g/>
zkomoleninu	zkomolenina	k1gFnSc4	zkomolenina
původního	původní	k2eAgInSc2d1	původní
pojmu	pojem	k1gInSc2	pojem
padmarága	padmarága	k1gFnSc1	padmarága
<g/>
,	,	kIx,	,
lotosové	lotosový	k2eAgFnSc2d1	Lotosová
touhy	touha	k1gFnSc2	touha
či	či	k8xC	či
padmarádža	padmarádža	k1gMnSc1	padmarádža
<g/>
,	,	kIx,	,
lotosový	lotosový	k2eAgMnSc1d1	lotosový
král	král	k1gMnSc1	král
<g/>
)	)	kIx)	)
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
rubínových	rubínový	k2eAgInPc2d1	rubínový
odstínů	odstín	k1gInPc2	odstín
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
růžové	růžový	k2eAgInPc1d1	růžový
safíry	safír	k1gInPc1	safír
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
bráhmanské	bráhmanský	k2eAgInPc4d1	bráhmanský
rubíny	rubín	k1gInPc4	rubín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kašmírské	kašmírský	k2eAgInPc1d1	kašmírský
safíry	safír	k1gInPc1	safír
sytě	sytě	k6eAd1	sytě
modré	modrý	k2eAgFnSc2d1	modrá
barvy	barva	k1gFnSc2	barva
a	a	k8xC	a
srílanské	srílanský	k2eAgFnSc2d1	Srílanská
padparádža	padparádžum	k1gNnSc2	padparádžum
kameny	kámen	k1gInPc1	kámen
jsou	být	k5eAaImIp3nP	být
špičkou	špička	k1gFnSc7	špička
safírové	safírový	k2eAgFnSc2d1	safírová
barevné	barevný	k2eAgFnSc2d1	barevná
škály	škála	k1gFnSc2	škála
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
závratných	závratný	k2eAgFnPc2d1	závratná
částek	částka	k1gFnPc2	částka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
využívají	využívat	k5eAaImIp3nP	využívat
mnozí	mnohý	k2eAgMnPc1d1	mnohý
světoví	světový	k2eAgMnPc1d1	světový
investoři	investor	k1gMnPc1	investor
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
barvami	barva	k1gFnPc7	barva
jsou	být	k5eAaImIp3nP	být
žluté	žlutý	k2eAgInPc1d1	žlutý
safíry	safír	k1gInPc1	safír
<g/>
,	,	kIx,	,
ceněné	ceněný	k2eAgInPc1d1	ceněný
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
jako	jako	k8xS	jako
kameny	kámen	k1gInPc7	kámen
osvícení	osvícení	k1gNnSc2	osvícení
<g/>
,	,	kIx,	,
oranžové	oranžový	k2eAgFnPc1d1	oranžová
<g/>
,	,	kIx,	,
růžové	růžový	k2eAgFnPc1d1	růžová
<g/>
,	,	kIx,	,
purpurové	purpurový	k2eAgFnPc1d1	purpurová
<g/>
,	,	kIx,	,
fialové	fialový	k2eAgFnPc1d1	fialová
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgFnPc1d1	zelená
a	a	k8xC	a
hnědé	hnědý	k2eAgInPc1d1	hnědý
safíry	safír	k1gInPc1	safír
<g/>
.	.	kIx.	.
</s>
<s>
Kašmírové	kašmírový	k2eAgInPc1d1	kašmírový
safíry	safír	k1gInPc1	safír
se	se	k3xPyFc4	se
těží	těžet	k5eAaImIp3nP	těžet
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
na	na	k7c6	na
Madagaskaru	Madagaskar	k1gInSc6	Madagaskar
<g/>
,	,	kIx,	,
Srí	Srí	k1gFnSc6	Srí
Lance	lance	k1gNnSc2	lance
a	a	k8xC	a
v	v	k7c6	v
Pákistánu	Pákistán	k1gInSc6	Pákistán
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
za	za	k7c4	za
Kašmírové	kašmírový	k2eAgInPc4d1	kašmírový
safíry	safír	k1gInPc4	safír
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
blížit	blížit	k5eAaImF	blížit
milionu	milion	k4xCgInSc2	milion
korun	koruna	k1gFnPc2	koruna
za	za	k7c4	za
karát	karát	k1gInSc4	karát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Safíry	safír	k1gInPc1	safír
africké	africký	k2eAgInPc1d1	africký
jsou	být	k5eAaImIp3nP	být
podkresleny	podkreslit	k5eAaPmNgInP	podkreslit
hnědým	hnědý	k2eAgInPc3d1	hnědý
a	a	k8xC	a
australské	australský	k2eAgFnSc2d1	australská
zelenkavým	zelenkavý	k2eAgNnSc7d1	zelenkavé
světlem	světlo	k1gNnSc7	světlo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
také	také	k9	také
pro	pro	k7c4	pro
východoasijské	východoasijský	k2eAgInPc4d1	východoasijský
kameny	kámen	k1gInPc4	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgInPc1	takový
nádechy	nádech	k1gInPc1	nádech
barev	barva	k1gFnPc2	barva
snižují	snižovat	k5eAaImIp3nP	snižovat
ceny	cena	k1gFnPc1	cena
kamenů	kámen	k1gInPc2	kámen
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
přírodní	přírodní	k2eAgInPc1d1	přírodní
nepálené	pálený	k2eNgInPc1d1	pálený
safíry	safír	k1gInPc1	safír
ze	z	k7c2	z
Srí	Srí	k1gFnSc2	Srí
Lanky	lanko	k1gNnPc7	lanko
nebo	nebo	k8xC	nebo
Barmy	Barma	k1gFnSc2	Barma
či	či	k8xC	či
Kašmíru	Kašmír	k1gInSc2	Kašmír
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
své	svůj	k3xOyFgInPc4	svůj
barevné	barevný	k2eAgInPc4d1	barevný
odstíny	odstín	k1gInPc4	odstín
nejvíce	nejvíce	k6eAd1	nejvíce
ceněny	ceněn	k2eAgInPc4d1	ceněn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Safíry	safír	k1gInPc1	safír
se	se	k3xPyFc4	se
napodobují	napodobovat	k5eAaImIp3nP	napodobovat
sklem	sklo	k1gNnSc7	sklo
<g/>
,	,	kIx,	,
syntetikou	syntetika	k1gFnSc7	syntetika
včetně	včetně	k7c2	včetně
té	ten	k3xDgFnSc2	ten
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
napodobuje	napodobovat	k5eAaImIp3nS	napodobovat
přírodní	přírodní	k2eAgInPc4d1	přírodní
kameny	kámen	k1gInPc4	kámen
dosti	dosti	k6eAd1	dosti
věrohodně	věrohodně	k6eAd1	věrohodně
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
nedostatečně	dostatečně	k6eNd1	dostatečně
zbarvené	zbarvený	k2eAgInPc1d1	zbarvený
kameny	kámen	k1gInPc1	kámen
ozařují	ozařovat	k5eAaImIp3nP	ozařovat
<g/>
,	,	kIx,	,
pálí	pálit	k5eAaImIp3nP	pálit
či	či	k8xC	či
procházejí	procházet	k5eAaImIp3nP	procházet
berryliovým	berryliový	k2eAgInSc7d1	berryliový
procesem	proces	k1gInSc7	proces
pálení	pálení	k1gNnSc2	pálení
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
měly	mít	k5eAaImAgFnP	mít
požadovanou	požadovaný	k2eAgFnSc4d1	požadovaná
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Takto	takto	k6eAd1	takto
se	se	k3xPyFc4	se
z	z	k7c2	z
Afriky	Afrika	k1gFnSc2	Afrika
dodávají	dodávat	k5eAaImIp3nP	dodávat
pálené	pálený	k2eAgFnPc1d1	pálená
padparádža	padparádža	k6eAd1	padparádža
kameny	kámen	k1gInPc7	kámen
napodobující	napodobující	k2eAgFnSc2d1	napodobující
pravé	pravá	k1gFnSc2	pravá
srílanské	srílanský	k2eAgFnSc6d1	Srílanská
a	a	k8xC	a
vietnamské	vietnamský	k2eAgFnSc6d1	vietnamská
padparádži	padparádž	k1gFnSc6	padparádž
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
směle	směle	k6eAd1	směle
konkurují	konkurovat	k5eAaImIp3nP	konkurovat
rubínům	rubín	k1gInPc3	rubín
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
pokládány	pokládán	k2eAgFnPc1d1	pokládána
za	za	k7c4	za
nejlepší	dobrý	k2eAgInPc4d3	nejlepší
rubíny	rubín	k1gInPc4	rubín
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Gemmologické	Gemmologický	k2eAgInPc1d1	Gemmologický
poznatky	poznatek	k1gInPc1	poznatek
kolem	kolem	k7c2	kolem
odlišování	odlišování	k1gNnSc2	odlišování
safírů	safír	k1gInPc2	safír
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
náročné	náročný	k2eAgInPc1d1	náročný
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
větší	veliký	k2eAgInPc1d2	veliký
safíry	safír	k1gInPc1	safír
stojí	stát	k5eAaImIp3nP	stát
od	od	k7c2	od
tisíců	tisíc	k4xCgInPc2	tisíc
po	po	k7c4	po
stamiliony	stamiliony	k1gInPc4	stamiliony
korun	koruna	k1gFnPc2	koruna
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
napodobeniny	napodobenina	k1gFnPc1	napodobenina
mohou	moct	k5eAaImIp3nP	moct
zmást	zmást	k5eAaPmF	zmást
i	i	k9	i
velké	velký	k2eAgMnPc4d1	velký
odborníky	odborník	k1gMnPc4	odborník
na	na	k7c4	na
přírodní	přírodní	k2eAgFnPc4d1	přírodní
vrostlice	vrostlice	k1gFnPc4	vrostlice
a	a	k8xC	a
inkluze	inkluze	k1gFnPc4	inkluze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Safíry	safír	k1gInPc1	safír
byly	být	k5eAaImAgInP	být
těženy	těžit	k5eAaImNgInP	těžit
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
na	na	k7c6	na
Srílance	Srílanka	k1gFnSc6	Srílanka
<g/>
,	,	kIx,	,
v	v	k7c6	v
Barmě	Barma	k1gFnSc6	Barma
a	a	k8xC	a
zdobí	zdobit	k5eAaImIp3nP	zdobit
mnoho	mnoho	k4c4	mnoho
význačných	význačný	k2eAgInPc2d1	význačný
klenotů	klenot	k1gInPc2	klenot
starověku	starověk	k1gInSc2	starověk
a	a	k8xC	a
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Safíry	safír	k1gInPc7	safír
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
používají	používat	k5eAaImIp3nP	používat
klasické	klasický	k2eAgInPc4d1	klasický
indické	indický	k2eAgInPc4d1	indický
nebo	nebo	k8xC	nebo
srílanské	srílanský	k2eAgInPc4d1	srílanský
názvy	název	k1gInPc4	název
safírů	safír	k1gInPc2	safír
–	–	k?	–
nílamani	nílaman	k1gMnPc1	nílaman
<g/>
,	,	kIx,	,
modrý	modrý	k2eAgInSc1d1	modrý
klenot	klenot	k1gInSc1	klenot
<g/>
,	,	kIx,	,
džalaníla	džalaníla	k1gFnSc1	džalaníla
<g/>
,	,	kIx,	,
safír	safír	k1gInSc1	safír
barvy	barva	k1gFnSc2	barva
namodralé	namodralý	k2eAgFnSc2d1	namodralá
světlé	světlý	k2eAgFnSc2d1	světlá
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
indraníla	indranílo	k1gNnSc2	indranílo
<g/>
,	,	kIx,	,
temně	temně	k6eAd1	temně
modrý	modrý	k2eAgInSc1d1	modrý
kášmíské	kášmíská	k1gFnSc3	kášmíská
modři	modř	k1gFnSc2	modř
<g/>
,	,	kIx,	,
padmarága	padmarága	k1gFnSc1	padmarága
-	-	kIx~	-
padparádža	padparádža	k1gFnSc1	padparádža
oranžovo-červené	oranžovo-červený	k2eAgFnSc2d1	oranžovo-červená
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
pušparága	pušparága	k1gFnSc1	pušparága
<g/>
,	,	kIx,	,
žlutý	žlutý	k2eAgInSc1d1	žlutý
safír	safír	k1gInSc1	safír
<g/>
,	,	kIx,	,
ratnapušparága	ratnapušparága	k1gFnSc1	ratnapušparága
<g/>
,	,	kIx,	,
zlatavého	zlatavý	k2eAgInSc2d1	zlatavý
odstínu	odstín	k1gInSc2	odstín
<g/>
,	,	kIx,	,
zlatý	zlatý	k1gInSc1	zlatý
golden	goldna	k1gFnPc2	goldna
safír	safír	k1gInSc1	safír
<g/>
,	,	kIx,	,
raktanílamani	raktanílaman	k1gMnPc1	raktanílaman
<g/>
,	,	kIx,	,
fialový	fialový	k2eAgInSc1d1	fialový
safír	safír	k1gInSc1	safír
<g/>
,	,	kIx,	,
raktagandhi	raktagandhi	k1gNnPc1	raktagandhi
<g/>
,	,	kIx,	,
purpurový	purpurový	k2eAgInSc1d1	purpurový
safír	safír	k1gInSc1	safír
<g/>
,	,	kIx,	,
pukharádža	pukharádža	k1gFnSc1	pukharádža
<g/>
,	,	kIx,	,
čirý	čirý	k2eAgInSc1d1	čirý
leukosafír	leukosafír	k1gInSc1	leukosafír
<g/>
,	,	kIx,	,
támranílamani	támranílaman	k1gMnPc1	támranílaman
<g/>
,	,	kIx,	,
oranžové	oranžový	k2eAgFnPc1d1	oranžová
barvy	barva	k1gFnPc1	barva
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
barvami	barva	k1gFnPc7	barva
jsou	být	k5eAaImIp3nP	být
hnědé	hnědý	k2eAgFnPc1d1	hnědá
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgFnPc1d1	zelená
<g/>
,	,	kIx,	,
černé	černý	k2eAgFnPc1d1	černá
a	a	k8xC	a
růžové	růžový	k2eAgFnPc1d1	růžová
<g/>
,	,	kIx,	,
rattu-nílamani	rattuílaman	k1gMnPc1	rattu-nílaman
safíry	safír	k1gInPc1	safír
<g/>
.	.	kIx.	.
</s>
<s>
Safíry	safír	k1gInPc1	safír
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
spinely	spinel	k1gInPc1	spinel
a	a	k8xC	a
turmalíny	turmalín	k1gInPc1	turmalín
<g/>
,	,	kIx,	,
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
celé	celý	k2eAgNnSc4d1	celé
barevné	barevný	k2eAgNnSc4d1	barevné
spektrum	spektrum	k1gNnSc4	spektrum
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
spojeny	spojit	k5eAaPmNgInP	spojit
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
legendami	legenda	k1gFnPc7	legenda
<g/>
,	,	kIx,	,
mystickými	mystický	k2eAgFnPc7d1	mystická
zkušenostmi	zkušenost	k1gFnPc7	zkušenost
<g/>
,	,	kIx,	,
léčebnými	léčebný	k2eAgFnPc7d1	léčebná
silami	síla	k1gFnPc7	síla
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
s	s	k7c7	s
dějinami	dějiny	k1gFnPc7	dějiny
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
skrze	skrze	k?	skrze
královské	královský	k2eAgInPc1d1	královský
a	a	k8xC	a
kultovní	kultovní	k2eAgInPc4d1	kultovní
klenoty	klenot	k1gInPc4	klenot
<g/>
.	.	kIx.	.
</s>
<s>
Safíry	safír	k1gInPc1	safír
modré	modrý	k2eAgFnSc2d1	modrá
barvy	barva	k1gFnSc2	barva
a	a	k8xC	a
padparádža	padparádžum	k1gNnSc2	padparádžum
losové	losový	k2eAgFnSc2d1	Losová
červeně	červeň	k1gFnSc2	červeň
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
<g/>
-li	i	k?	-li
větší	veliký	k2eAgFnSc4d2	veliký
karátovou	karátový	k2eAgFnSc4d1	karátová
váhu	váha	k1gFnSc4	váha
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
investiční	investiční	k2eAgInPc4d1	investiční
drahokamy	drahokam	k1gInPc4	drahokam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
kášmírské	kášmírský	k2eAgInPc1d1	kášmírský
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhém	druhý	k4xOgMnSc6	druhý
a	a	k8xC	a
třetím	třetí	k4xOgInSc6	třetí
barmské	barmský	k2eAgInPc4d1	barmský
a	a	k8xC	a
srílanské	srílanský	k2eAgInPc4d1	srílanský
safíry	safír	k1gInPc4	safír
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
90	[number]	k4	90
%	%	kIx~	%
modrých	modrý	k2eAgInPc2d1	modrý
safírů	safír	k1gInPc2	safír
je	být	k5eAaImIp3nS	být
tepelně	tepelně	k6eAd1	tepelně
upravováno	upravovat	k5eAaImNgNnS	upravovat
(	(	kIx(	(
<g/>
heated	heated	k1gInSc1	heated
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
lepší	lepší	k1gNnSc1	lepší
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
a	a	k8xC	a
tam	tam	k6eAd1	tam
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
u	u	k7c2	u
dalších	další	k2eAgFnPc2d1	další
barevných	barevný	k2eAgFnPc2d1	barevná
odrůd	odrůda	k1gFnPc2	odrůda
korundů	korund	k1gInPc2	korund
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
proces	proces	k1gInSc4	proces
doladění	doladění	k1gNnSc4	doladění
barvy	barva	k1gFnSc2	barva
kamene	kámen	k1gInSc2	kámen
je	být	k5eAaImIp3nS	být
gemologickou	gemologický	k2eAgFnSc7d1	gemologická
obcí	obec	k1gFnSc7	obec
tolerován	tolerovat	k5eAaImNgMnS	tolerovat
<g/>
.	.	kIx.	.
</s>
<s>
Treatment	Treatment	k1gInSc1	Treatment
s	s	k7c7	s
beryliovým	beryliový	k2eAgNnSc7d1	beryliový
a	a	k8xC	a
dalším	další	k2eAgNnSc7d1	další
chemickým	chemický	k2eAgNnSc7d1	chemické
tepelným	tepelný	k2eAgNnSc7d1	tepelné
doupravením	doupravení	k1gNnSc7	doupravení
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
zásadní	zásadní	k2eAgFnSc7d1	zásadní
změnou	změna	k1gFnSc7	změna
barvy	barva	k1gFnSc2	barva
<g/>
,	,	kIx,	,
vyvíjený	vyvíjený	k2eAgMnSc1d1	vyvíjený
thajci	thajce	k1gMnPc1	thajce
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nepokládá	pokládat	k5eNaImIp3nS	pokládat
za	za	k7c4	za
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
řešení	řešení	k1gNnSc4	řešení
zhodnocení	zhodnocení	k1gNnSc2	zhodnocení
drahých	drahý	k2eAgInPc2d1	drahý
korundů	korund	k1gInPc2	korund
<g/>
.	.	kIx.	.
</s>
<s>
Safíry	safír	k1gInPc1	safír
bez	bez	k7c2	bez
úpravy	úprava	k1gFnSc2	úprava
<g/>
,	,	kIx,	,
čisté	čistý	k2eAgInPc1d1	čistý
a	a	k8xC	a
veliké	veliký	k2eAgInPc1d1	veliký
<g/>
,	,	kIx,	,
kvalitně	kvalitně	k6eAd1	kvalitně
broušené	broušený	k2eAgFnPc1d1	broušená
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
dobrým	dobrý	k2eAgInSc7d1	dobrý
prostředkem	prostředek	k1gInSc7	prostředek
pro	pro	k7c4	pro
uložení	uložení	k1gNnSc4	uložení
investic	investice	k1gFnPc2	investice
<g/>
.	.	kIx.	.
</s>
<s>
Indové	Ind	k1gMnPc1	Ind
poučovali	poučovat	k5eAaImAgMnP	poučovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
žluté	žlutý	k2eAgInPc1d1	žlutý
safíry	safír	k1gInPc1	safír
posilují	posilovat	k5eAaImIp3nP	posilovat
mysl	mysl	k1gFnSc4	mysl
a	a	k8xC	a
optimismus	optimismus	k1gInSc4	optimismus
<g/>
,	,	kIx,	,
modré	modré	k1gNnSc1	modré
ochraňují	ochraňovat	k5eAaImIp3nP	ochraňovat
před	před	k7c7	před
zlými	zlý	k2eAgFnPc7d1	zlá
silami	síla	k1gFnPc7	síla
osudu	osud	k1gInSc2	osud
a	a	k8xC	a
padparádža	padparádža	k6eAd1	padparádža
daruje	darovat	k5eAaPmIp3nS	darovat
svobodu	svoboda	k1gFnSc4	svoboda
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
plnit	plnit	k5eAaImF	plnit
všechna	všechen	k3xTgNnPc4	všechen
rozvážná	rozvážný	k2eAgNnPc4d1	rozvážné
přání	přání	k1gNnPc4	přání
<g/>
.	.	kIx.	.
</s>
<s>
Viz	vidět	k5eAaImRp2nS	vidět
Brhat	Brhat	k1gInSc1	Brhat
samhitá	samhitat	k5eAaPmIp3nS	samhitat
od	od	k7c2	od
Varáha	Varáh	k1gMnSc2	Varáh
Mihiry	Mihira	k1gFnSc2	Mihira
a	a	k8xC	a
Manimálá	Manimálý	k2eAgFnSc1d1	Manimálý
od	od	k7c2	od
S.	S.	kA	S.
<g/>
M.	M.	kA	M.
Tagore	Tagor	k1gInSc5	Tagor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
safír	safír	k1gInSc1	safír
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
safír	safír	k1gInSc1	safír
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
