<s>
Pierre	Pierr	k1gMnSc5	Pierr
Curie	Curie	k1gMnSc5	Curie
(	(	kIx(	(
<g/>
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1859	[number]	k4	1859
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
-	-	kIx~	-
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
fyzik	fyzik	k1gMnSc1	fyzik
a	a	k8xC	a
chemik	chemik	k1gMnSc1	chemik
<g/>
,	,	kIx,	,
manžel	manžel	k1gMnSc1	manžel
Marie	Maria	k1gFnSc2	Maria
Curie-Skłodowské	Curie-Skłodowská	k1gFnSc2	Curie-Skłodowská
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
obdržel	obdržet	k5eAaPmAgInS	obdržet
společně	společně	k6eAd1	společně
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
a	a	k8xC	a
Henri	Henr	k1gFnSc6	Henr
Becquerelem	becquerel	k1gInSc7	becquerel
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
za	za	k7c4	za
výzkum	výzkum	k1gInSc4	výzkum
přirozené	přirozený	k2eAgFnSc2d1	přirozená
radioaktivity	radioaktivita	k1gFnSc2	radioaktivita
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
15	[number]	k4	15
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1859	[number]	k4	1859
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
Eugen	Eugen	k2eAgMnSc1d1	Eugen
Curie	Curie	k1gMnSc1	Curie
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
lékařem	lékař	k1gMnSc7	lékař
a	a	k8xC	a
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
Sophie-Claire	Sophie-Clair	k1gInSc5	Sophie-Clair
Depouilly	Depouilla	k1gFnPc4	Depouilla
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
dcerou	dcera	k1gFnSc7	dcera
továrníka	továrník	k1gMnSc2	továrník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
byl	být	k5eAaImAgMnS	být
vyučován	vyučovat	k5eAaImNgMnS	vyučovat
soukromým	soukromý	k2eAgMnSc7d1	soukromý
učitelem	učitel	k1gMnSc7	učitel
a	a	k8xC	a
již	již	k6eAd1	již
v	v	k7c6	v
16	[number]	k4	16
letech	léto	k1gNnPc6	léto
složil	složit	k5eAaPmAgMnS	složit
maturitní	maturitní	k2eAgFnSc4d1	maturitní
zkoušku	zkouška	k1gFnSc4	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
19	[number]	k4	19
letech	léto	k1gNnPc6	léto
získal	získat	k5eAaPmAgMnS	získat
univerzitní	univerzitní	k2eAgMnSc1d1	univerzitní
vzdělání	vzdělání	k1gNnSc4	vzdělání
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Mohl	moct	k5eAaImAgMnS	moct
tedy	tedy	k9	tedy
vyučovat	vyučovat	k5eAaImF	vyučovat
fyziku	fyzika	k1gFnSc4	fyzika
a	a	k8xC	a
chemii	chemie	k1gFnSc4	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1882	[number]	k4	1882
toto	tento	k3xDgNnSc1	tento
místo	místo	k6eAd1	místo
přijal	přijmout	k5eAaPmAgMnS	přijmout
<g/>
,	,	kIx,	,
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
asistent	asistent	k1gMnSc1	asistent
ve	v	k7c6	v
fyzikální	fyzikální	k2eAgFnSc6d1	fyzikální
laboratoři	laboratoř	k1gFnSc6	laboratoř
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1895	[number]	k4	1895
získal	získat	k5eAaPmAgInS	získat
doktorský	doktorský	k2eAgInSc1d1	doktorský
titul	titul	k1gInSc1	titul
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
profesorem	profesor	k1gMnSc7	profesor
fyziky	fyzika	k1gFnSc2	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
Skłodowskou	Skłodowska	k1gFnSc7	Skłodowska
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
učitele	učitel	k1gMnSc2	učitel
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
získal	získat	k5eAaPmAgInS	získat
místo	místo	k1gNnSc4	místo
profesora	profesor	k1gMnSc2	profesor
na	na	k7c6	na
fakultě	fakulta	k1gFnSc6	fakulta
přírodních	přírodní	k2eAgFnPc2d1	přírodní
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
odměněn	odměnit	k5eAaPmNgInS	odměnit
cenou	cena	k1gFnSc7	cena
Davy	Dav	k1gInPc1	Dav
Medal	Medal	k1gInSc4	Medal
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
mu	on	k3xPp3gMnSc3	on
udělila	udělit	k5eAaPmAgFnS	udělit
Královská	královský	k2eAgFnSc1d1	královská
společnost	společnost	k1gFnSc1	společnost
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
(	(	kIx(	(
<g/>
tuto	tento	k3xDgFnSc4	tento
cenu	cena	k1gFnSc4	cena
získal	získat	k5eAaPmAgMnS	získat
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1906	[number]	k4	1906
ráno	ráno	k6eAd1	ráno
<g/>
,	,	kIx,	,
když	když	k8xS	když
spěchal	spěchat	k5eAaImAgMnS	spěchat
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
pařížské	pařížský	k2eAgFnSc2d1	Pařížská
laboratoře	laboratoř	k1gFnSc2	laboratoř
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
smrtelně	smrtelně	k6eAd1	smrtelně
zraněn	zranit	k5eAaPmNgMnS	zranit
při	při	k7c6	při
dopravní	dopravní	k2eAgFnSc6d1	dopravní
nehodě	nehoda	k1gFnSc6	nehoda
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přecházení	přecházení	k1gNnSc6	přecházení
rue	rue	k?	rue
Dauphine	dauphin	k1gMnSc5	dauphin
byl	být	k5eAaImAgInS	být
sražen	sražen	k2eAgInSc4d1	sražen
drožkou	drožka	k1gFnSc7	drožka
jedoucí	jedoucí	k2eAgFnSc7d1	jedoucí
z	z	k7c2	z
Pont	Pont	k1gInSc4	Pont
Neuf	Neuf	k1gInSc1	Neuf
pod	pod	k7c4	pod
její	její	k3xOp3gNnPc4	její
kola	kolo	k1gNnPc4	kolo
a	a	k8xC	a
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
nehody	nehoda	k1gFnSc2	nehoda
<g/>
.	.	kIx.	.
</s>
<s>
Starší	starý	k2eAgFnSc1d2	starší
dcera	dcera	k1gFnSc1	dcera
Irè	Irè	k1gFnSc1	Irè
si	se	k3xPyFc3	se
vzala	vzít	k5eAaPmAgFnS	vzít
Frédérica	Frédéric	k2eAgFnSc1d1	Frédérica
Joliot-Curie	Joliot-Curie	k1gFnSc1	Joliot-Curie
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
získali	získat	k5eAaPmAgMnP	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
chemii	chemie	k1gFnSc4	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Mladší	mladý	k2eAgFnSc1d2	mladší
dcera	dcera	k1gFnSc1	dcera
Eva	Eva	k1gFnSc1	Eva
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c2	za
amerického	americký	k2eAgMnSc2d1	americký
diplomata	diplomat	k1gMnSc2	diplomat
H.	H.	kA	H.
R.	R.	kA	R.
Labouisse	Labouiss	k1gMnSc2	Labouiss
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
se	se	k3xPyFc4	se
zajímali	zajímat	k5eAaImAgMnP	zajímat
o	o	k7c4	o
sociální	sociální	k2eAgInPc4d1	sociální
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
a	a	k8xC	a
H.	H.	kA	H.
R.	R.	kA	R.
Labouisse	Labouisse	k1gFnSc1	Labouisse
jako	jako	k8xS	jako
předseda	předseda	k1gMnSc1	předseda
Dětského	dětský	k2eAgInSc2d1	dětský
fondu	fond	k1gInSc2	fond
OSN	OSN	kA	OSN
UNICEF	UNICEF	kA	UNICEF
převzal	převzít	k5eAaPmAgInS	převzít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
<s>
Eva	Eva	k1gFnSc1	Eva
Labouisse-Curie	Labouisse-Curie	k1gFnSc2	Labouisse-Curie
je	být	k5eAaImIp3nS	být
autorkou	autorka	k1gFnSc7	autorka
životopisu	životopis	k1gInSc2	životopis
své	svůj	k3xOyFgFnSc2	svůj
matky	matka	k1gFnSc2	matka
(	(	kIx(	(
<g/>
Madame	madame	k1gFnSc1	madame
Curie	Curie	k1gMnSc1	Curie
-	-	kIx~	-
Gallimard	Gallimard	k1gMnSc1	Gallimard
<g/>
,	,	kIx,	,
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přeloženého	přeložený	k2eAgInSc2d1	přeložený
do	do	k7c2	do
několika	několik	k4yIc2	několik
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
manželech	manžel	k1gMnPc6	manžel
Curiových	Curiový	k2eAgMnPc2d1	Curiový
bylo	být	k5eAaImAgNnS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
curium	curium	k1gNnSc1	curium
<g/>
,	,	kIx,	,
prvek	prvek	k1gInSc1	prvek
s	s	k7c7	s
atomovým	atomový	k2eAgNnSc7d1	atomové
číslem	číslo	k1gNnSc7	číslo
96	[number]	k4	96
a	a	k8xC	a
jednotka	jednotka	k1gFnSc1	jednotka
radioaktivity	radioaktivita	k1gFnSc2	radioaktivita
1	[number]	k4	1
curie	curie	k1gNnSc2	curie
(	(	kIx(	(
<g/>
1	[number]	k4	1
Ci	ci	k0	ci
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
definována	definovat	k5eAaBmNgFnS	definovat
jako	jako	k8xC	jako
počet	počet	k1gInSc1	počet
rozpadů	rozpad	k1gInPc2	rozpad
za	za	k7c2	za
1	[number]	k4	1
sekundu	sekund	k1gInSc2	sekund
1	[number]	k4	1
gramu	gram	k1gInSc2	gram
čistého	čistý	k2eAgNnSc2d1	čisté
radia	radio	k1gNnSc2	radio
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
jednotka	jednotka	k1gFnSc1	jednotka
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
jednotkou	jednotka	k1gFnSc7	jednotka
1	[number]	k4	1
becquerel	becquerel	k1gInSc1	becquerel
(	(	kIx(	(
<g/>
1	[number]	k4	1
Ci	ci	k0	ci
=	=	kIx~	=
3,7	[number]	k4	3,7
<g/>
×	×	k?	×
<g/>
1010	[number]	k4	1010
Bq	Bq	k1gFnPc2	Bq
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
studiu	studio	k1gNnSc3	studio
symetrie	symetrie	k1gFnSc2	symetrie
krystalů	krystal	k1gInPc2	krystal
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
objevil	objevit	k5eAaPmAgMnS	objevit
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
bratrem	bratr	k1gMnSc7	bratr
Paulem	Paul	k1gMnSc7	Paul
Jacquesem	Jacques	k1gMnSc7	Jacques
Curiem	curium	k1gNnSc7	curium
piezoelektrický	piezoelektrický	k2eAgInSc1d1	piezoelektrický
jev	jev	k1gInSc1	jev
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
obrátil	obrátit	k5eAaPmAgMnS	obrátit
svou	svůj	k3xOyFgFnSc4	svůj
pozornosti	pozornost	k1gFnSc3	pozornost
k	k	k7c3	k
magnetismu	magnetismus	k1gInSc3	magnetismus
<g/>
.	.	kIx.	.
</s>
<s>
Dokázal	dokázat	k5eAaPmAgMnS	dokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
magnetické	magnetický	k2eAgFnPc1d1	magnetická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
daného	daný	k2eAgInSc2d1	daný
materiálu	materiál	k1gInSc2	materiál
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
při	při	k7c6	při
určité	určitý	k2eAgFnSc6d1	určitá
teplotě	teplota	k1gFnSc6	teplota
-	-	kIx~	-
tato	tento	k3xDgFnSc1	tento
teplota	teplota	k1gFnSc1	teplota
se	se	k3xPyFc4	se
nyní	nyní	k6eAd1	nyní
nazývá	nazývat	k5eAaImIp3nS	nazývat
Curiova	Curiův	k2eAgFnSc1d1	Curiova
teplota	teplota	k1gFnSc1	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
radioaktivních	radioaktivní	k2eAgFnPc2d1	radioaktivní
látek	látka	k1gFnPc2	látka
prováděl	provádět	k5eAaImAgInS	provádět
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
<g/>
.	.	kIx.	.
</s>
<s>
Svých	svůj	k3xOyFgInPc2	svůj
objevů	objev	k1gInPc2	objev
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
v	v	k7c6	v
nepříliš	příliš	k6eNd1	příliš
vhodných	vhodný	k2eAgFnPc6d1	vhodná
podmínkách	podmínka	k1gFnPc6	podmínka
-	-	kIx~	-
měli	mít	k5eAaImAgMnP	mít
stěží	stěží	k6eAd1	stěží
dostačující	dostačující	k2eAgNnSc4d1	dostačující
laboratorní	laboratorní	k2eAgNnSc4d1	laboratorní
zařízení	zařízení	k1gNnSc4	zařízení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1898	[number]	k4	1898
oznámili	oznámit	k5eAaPmAgMnP	oznámit
objev	objev	k1gInSc4	objev
radia	radio	k1gNnSc2	radio
a	a	k8xC	a
polonia	polonium	k1gNnSc2	polonium
<g/>
,	,	kIx,	,
tyto	tento	k3xDgInPc4	tento
prvky	prvek	k1gInPc4	prvek
získali	získat	k5eAaPmAgMnP	získat
ze	z	k7c2	z
smolince	smolinec	k1gInSc2	smolinec
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
zabývali	zabývat	k5eAaImAgMnP	zabývat
výzkumem	výzkum	k1gInSc7	výzkum
radia	radio	k1gNnSc2	radio
a	a	k8xC	a
jeho	on	k3xPp3gInSc2	on
radioaktivního	radioaktivní	k2eAgInSc2d1	radioaktivní
rozpadu	rozpad	k1gInSc2	rozpad
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
práce	práce	k1gFnSc1	práce
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
základ	základ	k1gInSc4	základ
pro	pro	k7c4	pro
následující	následující	k2eAgInSc4d1	následující
výzkum	výzkum	k1gInSc4	výzkum
v	v	k7c6	v
jaderné	jaderný	k2eAgFnSc6d1	jaderná
fyzice	fyzika	k1gFnSc6	fyzika
a	a	k8xC	a
chemii	chemie	k1gFnSc6	chemie
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
oceněni	ocenit	k5eAaPmNgMnP	ocenit
Nobelovou	Nobelův	k2eAgFnSc7d1	Nobelova
cenou	cena	k1gFnSc7	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
za	za	k7c4	za
výzkum	výzkum	k1gInSc4	výzkum
radioaktivního	radioaktivní	k2eAgNnSc2d1	radioaktivní
záření	záření	k1gNnSc2	záření
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
objevil	objevit	k5eAaPmAgInS	objevit
Henri	Henre	k1gFnSc4	Henre
Becquerel	becquerel	k1gInSc1	becquerel
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
získal	získat	k5eAaPmAgInS	získat
Nobelovy	Nobelův	k2eAgFnPc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
společně	společně	k6eAd1	společně
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
<g/>
.	.	kIx.	.
</s>
