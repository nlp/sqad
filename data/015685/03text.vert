<s>
K	k	k7c3
</s>
<s>
K	k	k7c3
</s>
<s>
ZnakKNázev	ZnakKNázet	k5eAaPmDgInS
v	v	k7c6
UnicoduKódovánídechexUnicode	UnicoduKódovánídechexUnicod	k1gInSc5
<g/>
75	#num#	k4
<g/>
U	U	kA
<g/>
+	+	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
BUTF-	BUTF-	k1gFnPc2
<g/>
8754	#num#	k4
<g/>
bČíselná	bČíselný	k2eAgFnSc1d1
entitaKK	entitaKK	k?
</s>
<s>
ZnakkNázev	ZnakkNázet	k5eAaPmDgInS
v	v	k7c6
UnicoduKódovánídechexUnicode	UnicoduKódovánídechexUnicod	k1gInSc5
<g/>
107	#num#	k4
<g/>
U	U	kA
<g/>
+	+	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
BUTF-	BUTF-	k1gFnPc2
<g/>
81076	#num#	k4
<g/>
bČíselná	bČíselný	k2eAgFnSc1d1
entitakk	entitakk	k6eAd1
Hláskování	hláskování	k1gNnSc4
české	český	k2eAgFnSc2d1
</s>
<s>
Karel	Karel	k1gMnSc1
Hláskování	hláskování	k1gNnSc2
mezinárodní	mezinárodní	k2eAgFnSc2d1
</s>
<s>
Kilo	kilo	k1gNnSc1
Braille	Braille	k1gFnSc2
</s>
<s>
⠅	⠅	k?
Morseova	Morseův	k2eAgFnSc1d1
abeceda	abeceda	k1gFnSc1
</s>
<s>
–	–	k?
<g/>
⋅	⋅	k?
<g/>
–	–	k?
(	(	kIx(
<g/>
království	království	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Vizuální	vizuální	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
</s>
<s>
K	k	k7c3
je	být	k5eAaImIp3nS
11	#num#	k4
<g/>
.	.	kIx.
písmeno	písmeno	k1gNnSc4
latinské	latinský	k2eAgFnSc2d1
abecedy	abeceda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzniklo	vzniknout	k5eAaPmAgNnS
z	z	k7c2
písmene	písmeno	k1gNnSc2
κ	κ	k?
(	(	kIx(
<g/>
zvaného	zvaný	k2eAgNnSc2d1
kappa	kappa	k1gNnSc2
<g/>
)	)	kIx)
řecké	řecký	k2eAgFnSc2d1
abecedy	abeceda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
klasické	klasický	k2eAgFnSc6d1
latině	latina	k1gFnSc6
se	se	k3xPyFc4
toto	tento	k3xDgNnSc1
písmeno	písmeno	k1gNnSc1
používalo	používat	k5eAaImAgNnS
jen	jen	k9
zcela	zcela	k6eAd1
výjimečně	výjimečně	k6eAd1
(	(	kIx(
<g/>
psalo	psát	k5eAaImAgNnS
se	se	k3xPyFc4
C	C	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jen	jen	k9
jako	jako	k8xC,k8xS
určité	určitý	k2eAgFnPc1d1
zkratky	zkratka	k1gFnPc1
a	a	k8xC
ve	v	k7c6
variantě	varianta	k1gFnSc6
pravopisu	pravopis	k1gInSc2
názvů	název	k1gInPc2
Kalendae	Kalendae	k1gNnSc1
a	a	k8xC
Karthago	Karthago	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
češtině	čeština	k1gFnSc6
je	být	k5eAaImIp3nS
k	k	k7c3
předložka	předložka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
matematice	matematika	k1gFnSc6
je	být	k5eAaImIp3nS
k	k	k7c3
jedna	jeden	k4xCgFnSc1
z	z	k7c2
imaginárních	imaginární	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
kvaternionů	kvaternion	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
fyzice	fyzika	k1gFnSc6
</s>
<s>
k	k	k7c3
je	on	k3xPp3gFnPc4
označení	označení	k1gNnSc1
pro	pro	k7c4
Boltzmannovu	Boltzmannův	k2eAgFnSc4d1
konstantu	konstanta	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
k	k	k7c3
je	on	k3xPp3gFnPc4
označení	označení	k1gNnSc1
pro	pro	k7c4
konstantu	konstanta	k1gFnSc4
popisující	popisující	k2eAgFnSc1d1
tuhost	tuhost	k1gFnSc1
pružiny	pružina	k1gFnSc2
v	v	k7c6
Hookově	Hookův	k2eAgInSc6d1
zákonu	zákon	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
k	k	k7c3
je	být	k5eAaImIp3nS
koňská	koňský	k2eAgFnSc1d1
síla	síla	k1gFnSc1
<g/>
,	,	kIx,
starší	starý	k2eAgFnSc1d2
fyzikální	fyzikální	k2eAgFnSc1d1
jednotka	jednotka	k1gFnSc1
výkonu	výkon	k1gInSc2
mimo	mimo	k7c4
soustavu	soustava	k1gFnSc4
SI	si	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
K	K	kA
je	být	k5eAaImIp3nS
druh	druh	k1gInSc1
mezonů	mezon	k1gInPc2
–	–	k?
kaon	kaona	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
astronomii	astronomie	k1gFnSc6
je	být	k5eAaImIp3nS
K	k	k7c3
označení	označení	k1gNnSc4
jedné	jeden	k4xCgFnSc2
ze	z	k7c2
spektrálních	spektrální	k2eAgFnPc2d1
tříd	třída	k1gFnPc2
hvězd	hvězda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
soustavě	soustava	k1gFnSc6
SI	se	k3xPyFc3
</s>
<s>
K	K	kA
je	být	k5eAaImIp3nS
značka	značka	k1gFnSc1
jednotky	jednotka	k1gFnSc2
termodynamické	termodynamický	k2eAgFnSc2d1
teploty	teplota	k1gFnSc2
kelvin	kelvin	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
k	k	k7c3
je	on	k3xPp3gNnPc4
značka	značka	k1gFnSc1
předpony	předpona	k1gFnSc2
soustavy	soustava	k1gFnSc2
SI	si	k1gNnSc2
pro	pro	k7c4
1000	#num#	k4
<g/>
,	,	kIx,
kilo	kilo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
chemii	chemie	k1gFnSc6
</s>
<s>
K	K	kA
je	být	k5eAaImIp3nS
značka	značka	k1gFnSc1
draslíku	draslík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
k	k	k7c3
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc1
rovnovážné	rovnovážný	k2eAgFnSc2d1
konstanty	konstanta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
biochemii	biochemie	k1gFnSc6
je	být	k5eAaImIp3nS
K	k	k7c3
označení	označení	k1gNnSc3
pro	pro	k7c4
aminokyselinu	aminokyselina	k1gFnSc4
lysin	lysina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
výživě	výživa	k1gFnSc6
je	být	k5eAaImIp3nS
K	K	kA
vitamín	vitamín	k1gInSc1
–	–	k?
viz	vidět	k5eAaImRp2nS
vitamín	vitamín	k1gInSc1
K.	K.	kA
</s>
<s>
Na	na	k7c6
potravinách	potravina	k1gFnPc6
označuje	označovat	k5eAaImIp3nS
K	K	kA
v	v	k7c6
kroužku	kroužek	k1gInSc6
<g/>
,	,	kIx,
Ⓚ	Ⓚ	k?
<g/>
,	,	kIx,
košer	košer	k2eAgNnSc1d1
jídlo	jídlo	k1gNnSc1
certifikované	certifikovaný	k2eAgNnSc1d1
organizací	organizace	k1gFnSc7
Organized	Organized	k1gMnSc1
Kashrut	Kashrut	k1gMnSc1
Laboratories	Laboratories	k1gMnSc1
(	(	kIx(
<g/>
OK	oka	k1gFnPc2
Labs	Labs	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Různé	různý	k2eAgFnPc1d1
obměny	obměna	k1gFnPc1
písmene	písmeno	k1gNnSc2
K	K	kA
jsou	být	k5eAaImIp3nP
často	často	k6eAd1
součástí	součást	k1gFnSc7
značek	značka	k1gFnPc2
i	i	k8xC
dalších	další	k2eAgFnPc2d1
organizací	organizace	k1gFnPc2
dozorujících	dozorující	k2eAgFnPc2d1
kašrut	kašrut	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s>
K	K	kA
je	být	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgFnSc1d1
poznávací	poznávací	k2eAgFnSc1d1
značka	značka	k1gFnSc1
Kambodže	Kambodža	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
registrační	registrační	k2eAgFnSc6d1
značce	značka	k1gFnSc6
znamená	znamenat	k5eAaImIp3nS
K	K	kA
Karlovarský	karlovarský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
karetních	karetní	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
je	být	k5eAaImIp3nS
K	k	k7c3
označení	označení	k1gNnSc3
krále	král	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
šachu	šach	k1gInSc6
je	být	k5eAaImIp3nS
K	k	k7c3
označení	označení	k1gNnSc3
krále	král	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Za	za	k7c2
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2
k.	k.	k?
označovalo	označovat	k5eAaImAgNnS
úřady	úřad	k1gInPc4
uherské	uherský	k2eAgFnSc2d1
části	část	k1gFnSc2
monarchie	monarchie	k1gFnSc2
–	–	k?
viz	vidět	k5eAaImRp2nS
c.	c.	k?
k.	k.	k?
<g/>
.	.	kIx.
</s>
<s>
Zkratka	zkratka	k1gFnSc1
pro	pro	k7c4
protektorátní	protektorátní	k2eAgFnSc4d1
korunu	koruna	k1gFnSc4
v	v	k7c6
Protektorátu	protektorát	k1gInSc6
Čech	Čechy	k1gFnPc2
a	a	k8xC
Moravy	Morava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zkratka	zkratka	k1gFnSc1
pro	pro	k7c4
Československou	československý	k2eAgFnSc4d1
korunu	koruna	k1gFnSc4
za	za	k7c2
druhé	druhý	k4xOgFnSc2
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
radiokomunikaci	radiokomunikace	k1gFnSc6
je	být	k5eAaImIp3nS
K	k	k7c3
jedním	jeden	k4xCgInSc7
z	z	k7c2
prefixů	prefix	k1gInPc2
volacích	volací	k2eAgInPc2d1
znaků	znak	k1gInPc2
vyhrazených	vyhrazený	k2eAgInPc2d1
pro	pro	k7c4
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s>
Používá	používat	k5eAaImIp3nS
se	se	k3xPyFc4
též	též	k9
k	k	k7c3
vyjádření	vyjádření	k1gNnSc3
souhlasu	souhlas	k1gInSc2
<g/>
,	,	kIx,
k	k	k7c3
=	=	kIx~
ok	oko	k1gNnPc2
→	→	k?
vzniklo	vzniknout	k5eAaPmAgNnS
z	z	k7c2
anglického	anglický	k2eAgInSc2d1
ok	oka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
informatice	informatika	k1gFnSc6
a	a	k8xC
online	onlinout	k5eAaPmIp3nS
hrách	hrách	k1gInSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
jako	jako	k9
označení	označení	k1gNnSc1
řádu	řád	k1gInSc2
tisíců	tisíc	k4xCgInPc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
1	#num#	k4
<g/>
k	k	k7c3
=	=	kIx~
1000	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
arménském	arménský	k2eAgNnSc6d1
písmu	písmo	k1gNnSc6
písmenu	písmeno	k1gNnSc6
K	K	kA
odpovídá	odpovídat	k5eAaImIp3nS
písmeno	písmeno	k1gNnSc1
Կ	Կ	k?
(	(	kIx(
<g/>
կ	կ	k?
<g/>
)	)	kIx)
(	(	kIx(
<g/>
dle	dle	k7c2
české	český	k2eAgFnSc2d1
výslovnosti	výslovnost	k1gFnSc2
bez	bez	k7c2
přídechu	přídech	k1gInSc2
<g/>
)	)	kIx)
nebo	nebo	k8xC
písmeno	písmeno	k1gNnSc1
Ք	Ք	k?
(	(	kIx(
<g/>
ք	ք	k?
<g/>
)	)	kIx)
(	(	kIx(
<g/>
dle	dle	k7c2
anglické	anglický	k2eAgFnSc2d1
výslovnosti	výslovnost	k1gFnSc2
s	s	k7c7
přídechem	přídech	k1gInSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Novotný	Novotný	k1gMnSc1
<g/>
–	–	k?
<g/>
Pražák	Pražák	k1gMnSc1
<g/>
–	–	k?
<g/>
Sedláček	Sedláček	k1gMnSc1
<g/>
:	:	kIx,
Latinsko-český	latinsko-český	k2eAgInSc1d1
slovník	slovník	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Heslo	heslo	k1gNnSc1
K.	K.	kA
<g/>
↑	↑	k?
k	k	k7c3
na	na	k7c4
thefreedictionary	thefreedictionara	k1gFnPc4
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Galerie	galerie	k1gFnSc1
K	K	kA
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
k	k	k7c3
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc3
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
K	k	k7c3
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Latinka	latinka	k1gFnSc1
Základní	základní	k2eAgFnSc1d1
písmena	písmeno	k1gNnPc4
dle	dle	k7c2
ISO	ISO	kA
<g/>
*	*	kIx~
</s>
<s>
Aa	Aa	k?
</s>
<s>
Bb	Bb	k?
</s>
<s>
Cc	Cc	k?
</s>
<s>
Dd	Dd	k?
</s>
<s>
Ee	Ee	k?
</s>
<s>
Ff	ff	kA
</s>
<s>
Gg	Gg	k?
</s>
<s>
Hh	Hh	k?
</s>
<s>
Ii	Ii	k?
</s>
<s>
Jj	Jj	k?
</s>
<s>
Kk	Kk	k?
</s>
<s>
Ll	Ll	k?
</s>
<s>
Mm	mm	kA
</s>
<s>
Nn	Nn	k?
</s>
<s>
Oo	Oo	k?
</s>
<s>
Pp	Pp	k?
</s>
<s>
Qq	Qq	k?
</s>
<s>
Rr	Rr	k?
</s>
<s>
Ss	Ss	k?
</s>
<s>
Tt	Tt	k?
</s>
<s>
Uu	Uu	k?
</s>
<s>
Vv	Vv	k?
</s>
<s>
Ww	Ww	k?
</s>
<s>
Xx	Xx	k?
</s>
<s>
Yy	Yy	k?
</s>
<s>
Zz	Zz	k?
Další	další	k2eAgFnSc1d1
písmena	písmeno	k1gNnPc4
</s>
<s>
Ə	Ə	k?
</s>
<s>
Ǝ	Ǝ	k?
</s>
<s>
Ɛ	Ɛ	k?
</s>
<s>
Ɣ	Ɣ	k?
</s>
<s>
Ɩ	Ɩ	k?
</s>
<s>
Ɐ	Ɐ	k?
</s>
<s>
Ꝺ	Ꝺ	k?
</s>
<s>
Þ	Þ	k?
</s>
<s>
Ꞇ	Ꞇ	k?
</s>
<s>
ʇ	ʇ	k?
</s>
<s>
Ŋ	Ŋ	k?
</s>
<s>
Ɔ	Ɔ	k?
</s>
<s>
ſ	ſ	k?
</s>
<s>
Ƨ	Ƨ	k?
</s>
<s>
Ʋ	Ʋ	k?
</s>
<s>
Ʊ	Ʊ	k?
</s>
<s>
Ꝩ	Ꝩ	k?
</s>
<s>
Ꝡ	Ꝡ	k?
</s>
<s>
ẜ	ẜ	k?
</s>
<s>
Ꞅ	Ꞅ	k?
</s>
<s>
Ꝭ	Ꝭ	k?
</s>
<s>
Ʒ	Ʒ	k?
</s>
<s>
Ƹ	Ƹ	k?
</s>
<s>
Ȣ	Ȣ	k?
</s>
<s>
Ʌ	Ʌ	k?
</s>
<s>
Ꞁ	Ꞁ	k?
</s>
<s>
Ỽ	Ỽ	k?
</s>
<s>
Ỿ	Ỿ	k?
</s>
<s>
ʚ	ʚ	k?
</s>
<s>
Ƣ	Ƣ	k?
</s>
<s>
Κ	Κ	k?
</s>
<s>
Ƿ	Ƿ	k?
</s>
<s>
Ⅎ	Ⅎ	k?
</s>
<s>
Ꝼ	Ꝼ	k?
</s>
<s>
Ꝛ	Ꝛ	k?
</s>
<s>
Ʀ	Ʀ	k?
</s>
<s>
Ᵹ	Ᵹ	k?
</s>
<s>
Ꝿ	Ꝿ	k?
</s>
<s>
Ɋ	Ɋ	k?
</s>
<s>
Ь	Ь	k?
</s>
<s>
ʞ	ʞ	k?
</s>
<s>
Ȝ	Ȝ	k?
</s>
<s>
Ꝫ	Ꝫ	k?
</s>
<s>
Ꜭ	Ꜭ	k?
</s>
<s>
ɥ	ɥ	k?
</s>
<s>
Ꝝ	Ꝝ	k?
</s>
<s>
Ƽ	Ƽ	k?
</s>
<s>
Ꝯ	Ꝯ	k?
</s>
<s>
Ɂ	Ɂ	k?
</s>
<s>
Ƕ	Ƕ	k?
</s>
<s>
Ꜧ	Ꜧ	k?
Spřežky	spřežka	k1gFnPc1
a	a	k8xC
ligatury	ligatura	k1gFnPc1
</s>
<s>
Æ	Æ	k?
</s>
<s>
Œ	Œ	k?
</s>
<s>
ẞ	ẞ	k?
</s>
<s>
Ỻ	Ỻ	k?
</s>
<s>
Ǆ	Ǆ	k?
</s>
<s>
Ǳ	Ǳ	k?
</s>
<s>
Ǉ	Ǉ	k?
</s>
<s>
Ǌ	Ǌ	k?
</s>
<s>
ȸ	ȸ	k?
</s>
<s>
ȹ	ȹ	k?
</s>
<s>
Ĳ	Ĳ	k?
</s>
<s>
ch	ch	k0
</s>
<s>
cz	cz	k?
</s>
<s>
gb	gb	k?
</s>
<s>
gh	gh	k?
</s>
<s>
gy	gy	k?
</s>
<s>
ll	ll	k?
</s>
<s>
ly	ly	k?
</s>
<s>
nh	nh	k?
</s>
<s>
ny	ny	k?
</s>
<s>
rr	rr	k?
</s>
<s>
sh	sh	k?
</s>
<s>
sz	sz	k?
</s>
<s>
th	th	k?
Varianty	varianta	k1gFnSc2
písmena	písmeno	k1gNnSc2
K	k	k7c3
</s>
<s>
Ḱ	Ḱ	k?
</s>
<s>
K	k	k7c3
<g/>
̀	̀	k?
<g/>
k	k	k7c3
<g/>
̀	̀	k?
</s>
<s>
Ǩ	Ǩ	k?
</s>
<s>
Ƙ	Ƙ	k?
</s>
<s>
K	k	k7c3
<g/>
̄	̄	k?
<g/>
k	k	k7c3
<g/>
̄	̄	k?
</s>
<s>
K	k	k7c3
<g/>
̇	̇	k?
<g/>
k	k	k7c3
<g/>
̇	̇	k?
</s>
<s>
Ķ	Ķ	k?
</s>
<s>
Ḵ	Ḵ	k?
</s>
<s>
Ḳ	Ḳ	k?
</s>
<s>
K	k	k7c3
<g/>
̤	̤	k?
<g/>
k	k	k7c3
<g/>
̤	̤	k?
</s>
<s>
K	k	k7c3
<g/>
̦	̦	k?
<g/>
k	k	k7c3
<g/>
̦	̦	k?
</s>
<s>
Ḵ	Ḵ	k?
<g/>
̓	̓	k?
<g/>
ḵ	ḵ	k?
<g/>
̓	̓	k?
</s>
<s>
Ⱪ	Ⱪ	k?
</s>
<s>
Ꝁ	Ꝁ	k?
</s>
<s>
Ꝃ	Ꝃ	k?
</s>
<s>
Ꝅ	Ꝅ	k?
</s>
<s>
*	*	kIx~
tučně	tučně	k6eAd1
jsou	být	k5eAaImIp3nP
písmena	písmeno	k1gNnPc4
původní	původní	k2eAgFnSc2d1
latinské	latinský	k2eAgFnSc2d1
abecedy	abeceda	k1gFnSc2
</s>
<s>
Seznam	seznam	k1gInSc1
jazyků	jazyk	k1gInPc2
používajících	používající	k2eAgInPc2d1
latinku	latinka	k1gFnSc4
</s>
