<s>
Ángel	Ángel	k1gInSc1
Labruna	Labruna	k1gFnSc1
</s>
<s>
Ángel	Ángel	k1gInSc1
Labruna	Labruna	k1gFnSc1
Osobní	osobní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Datum	datum	k1gNnSc1
narození	narození	k1gNnSc2
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1918	#num#	k4
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Buenos	Buenos	k1gMnSc1
Aires	Aires	k1gMnSc1
Datum	datum	k1gInSc4
úmrtí	úmrtí	k1gNnSc2
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1983	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
64	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
a	a	k8xC
20	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1983	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
64	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
úmrtí	úmrtí	k1gNnSc2
</s>
<s>
Buenos	Buenos	k1gMnSc1
Aires	Aires	k1gMnSc1
Klubové	klubový	k2eAgFnPc4d1
informace	informace	k1gFnPc4
</s>
<s>
Konec	konec	k1gInSc1
hráčské	hráčský	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
Pozice	pozice	k1gFnSc2
</s>
<s>
útočník	útočník	k1gMnSc1
Úspěchy	úspěch	k1gInPc1
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
</s>
<s>
1946	#num#	k4
</s>
<s>
Argentina	Argentina	k1gFnSc1
</s>
<s>
1955	#num#	k4
</s>
<s>
Argentina	Argentina	k1gFnSc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
</s>
<s>
→	→	k?
Šipka	šipka	k1gFnSc1
znamená	znamenat	k5eAaImIp3nS
hostování	hostování	k1gNnSc4
hráče	hráč	k1gMnSc2
v	v	k7c6
daném	daný	k2eAgInSc6d1
klubu	klub	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Ángel	Ángel	k1gMnSc1
Amadeo	Amadeo	k1gMnSc1
Labruna	Labruna	k1gFnSc1
(	(	kIx(
<g/>
28	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1918	#num#	k4
<g/>
,	,	kIx,
Buenos	Buenos	k1gInSc1
Aires	Aires	k1gInSc1
–	–	k?
20	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1983	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
argentinský	argentinský	k2eAgMnSc1d1
fotbalista	fotbalista	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrával	hrávat	k5eAaImAgMnS
na	na	k7c6
pozici	pozice	k1gFnSc6
útočníka	útočník	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
dresu	dres	k1gInSc6
argentinské	argentinský	k2eAgFnSc2d1
reprezentace	reprezentace	k1gFnSc2
se	se	k3xPyFc4
zúčastnil	zúčastnit	k5eAaPmAgMnS
mistrovství	mistrovství	k1gNnSc2
světa	svět	k1gInSc2
ve	v	k7c6
Švédsku	Švédsko	k1gNnSc6
roku	rok	k1gInSc2
1958	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Má	mít	k5eAaImIp3nS
též	též	k9
dvě	dva	k4xCgFnPc4
zlaté	zlatý	k2eAgFnPc4d1
medaile	medaile	k1gFnPc4
z	z	k7c2
mistrovství	mistrovství	k1gNnSc2
Jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
(	(	kIx(
<g/>
Copa	Cop	k2eAgFnSc1d1
América	América	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
z	z	k7c2
let	léto	k1gNnPc2
1946	#num#	k4
a	a	k8xC
1955	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Celkem	celkem	k6eAd1
za	za	k7c4
národní	národní	k2eAgInSc4d1
tým	tým	k1gInSc4
odehrál	odehrát	k5eAaPmAgMnS
37	#num#	k4
zápasů	zápas	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
nichž	jenž	k3xRgInPc2
vstřelil	vstřelit	k5eAaPmAgMnS
17	#num#	k4
branek	branka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
S	s	k7c7
CA	ca	kA
River	River	k1gInSc1
Plate	plat	k1gInSc5
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
devětkrát	devětkrát	k6eAd1
mistrem	mistr	k1gMnSc7
Argentiny	Argentina	k1gFnSc2
(	(	kIx(
<g/>
1941	#num#	k4
<g/>
,	,	kIx,
1942	#num#	k4
<g/>
,	,	kIx,
1945	#num#	k4
<g/>
,	,	kIx,
1947	#num#	k4
<g/>
,	,	kIx,
1952	#num#	k4
<g/>
,	,	kIx,
1953	#num#	k4
<g/>
,	,	kIx,
1955	#num#	k4
<g/>
,	,	kIx,
1956	#num#	k4
<g/>
,	,	kIx,
1957	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vstřelil	vstřelit	k5eAaPmAgMnS
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
dresu	dres	k1gInSc6
293	#num#	k4
ligových	ligový	k2eAgInPc2d1
gólů	gól	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
do	do	k7c2
dnešních	dnešní	k2eAgInPc2d1
dnů	den	k1gInPc2
rekord	rekord	k1gInSc1
argentinské	argentinský	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
(	(	kIx(
<g/>
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
drží	držet	k5eAaImIp3nS
spolu	spolu	k6eAd1
s	s	k7c7
Arseniem	Arsenium	k1gNnSc7
Ericem	Erice	k1gMnSc7
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
vstřelil	vstřelit	k5eAaPmAgMnS
přesně	přesně	k6eAd1
stejný	stejný	k2eAgInSc4d1
počet	počet	k1gInSc4
branek	branka	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Brazilský	brazilský	k2eAgInSc1d1
časopis	časopis	k1gInSc1
Placar	Placar	k1gInSc1
ho	on	k3xPp3gMnSc4
vyhlásil	vyhlásit	k5eAaPmAgInS
25	#num#	k4
<g/>
.	.	kIx.
nejlepším	dobrý	k2eAgMnSc7d3
fotbalistou	fotbalista	k1gMnSc7
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Měl	mít	k5eAaImAgInS
přezdívku	přezdívka	k1gFnSc4
La	la	k1gNnSc2
Máquina	Máquino	k1gNnSc2
či	či	k8xC
El	Ela	k1gFnPc2
Feo	Feo	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Den	den	k1gInSc1
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
narození	narození	k1gNnSc2
<g/>
,	,	kIx,
28	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc6
<g/>
,	,	kIx,
slaví	slavit	k5eAaImIp3nP
fanoušci	fanoušek	k1gMnPc1
River	River	k1gInSc4
Plate	plat	k1gInSc5
každým	každý	k3xTgInSc7
rokem	rok	k1gInSc7
jako	jako	k9
Svátek	Svátek	k1gMnSc1
fanoušků	fanoušek	k1gMnPc2
River	River	k1gMnSc1
Plate	plat	k1gInSc5
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
skončení	skončení	k1gNnSc6
hráčské	hráčský	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
trenérem	trenér	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://www.transfermarkt.co.uk/en/ngel-labruna/profil/spieler_247103.html	http://www.transfermarkt.co.uk/en/ngel-labruna/profil/spieler_247103.html	k1gMnSc1
<g/>
↑	↑	k?
http://www.rsssf.com/tables/58full.html	http://www.rsssf.com/tables/58full.html	k1gMnSc1
<g/>
↑	↑	k?
http://www.rsssf.com/tables/46safull.html	http://www.rsssf.com/tables/46safull.html	k1gMnSc1
<g/>
↑	↑	k?
http://www.rsssf.com/tables/55safull.html	http://www.rsssf.com/tables/55safull.html	k1gMnSc1
<g/>
↑	↑	k?
http://www.rsssf.com/miscellaneous/labruna-intlg.html	http://www.rsssf.com/miscellaneous/labruna-intlg.html	k1gMnSc1
<g/>
↑	↑	k?
http://www.rsssf.com/tablesa/argtops-allt.html	http://www.rsssf.com/tablesa/argtops-allt.html	k1gInSc1
<g/>
↑	↑	k?
http://www.rsssf.com/miscellaneous/best-x-players-of-y.html#placar100	http://www.rsssf.com/miscellaneous/best-x-players-of-y.html#placar100	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Ángel	Ángela	k1gFnPc2
Labruna	Labruna	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Portrét	portrét	k1gInSc1
na	na	k7c4
World	World	k1gInSc4
Football	Football	k1gInSc4
Legends	Legendsa	k1gFnPc2
</s>
<s>
Portrét	portrét	k1gInSc1
na	na	k7c6
stránkách	stránka	k1gFnPc6
Argentina	Argentina	k1gFnSc1
<g/>
.	.	kIx.
<g/>
ar	ar	k1gInSc1
</s>
<s>
Heslo	heslo	k1gNnSc4
na	na	k7c4
Biografias	Biografias	k1gInSc4
<g/>
.	.	kIx.
<g/>
es	es	k1gNnSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Mistrovství	mistrovství	k1gNnSc1
Jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
1946	#num#	k4
–	–	k?
Argentina	Argentina	k1gFnSc1
Brankáři	brankář	k1gMnPc7
</s>
<s>
Ogando	Ogando	k6eAd1
•	•	k?
Vacca	Vacca	k1gMnSc1
Hráči	hráč	k1gMnPc1
</s>
<s>
Boyé	Boyé	k6eAd1
•	•	k?
de	de	k?
la	la	k1gNnSc2
Mata	mást	k5eAaImSgInS
•	•	k?
Fonda	Fonda	k1gFnSc1
•	•	k?
Labruna	Labruna	k1gFnSc1
•	•	k?
Loustau	Loustaus	k1gInSc2
•	•	k?
Marante	Marant	k1gMnSc5
•	•	k?
Martino	Martin	k2eAgNnSc1d1
•	•	k?
Méndez	Méndez	k1gInSc1
•	•	k?
Ongaro	Ongara	k1gFnSc5
•	•	k?
Pedernera	Pederner	k1gMnSc2
•	•	k?
Pescia	Pescius	k1gMnSc2
•	•	k?
Pontoni	Pontoň	k1gFnSc3
•	•	k?
Ramos	Ramos	k1gMnSc1
•	•	k?
Rodríguez	Rodríguez	k1gMnSc1
•	•	k?
Salomón	Salomón	k1gMnSc1
•	•	k?
Salvini	Salvin	k2eAgMnPc1d1
•	•	k?
Sobrero	Sobrero	k1gNnSc4
•	•	k?
Sosa	Sosa	k1gMnSc1
•	•	k?
Strembel	Strembel	k1gMnSc1
•	•	k?
Sued	Sued	k1gInSc1
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Stábile	Stábile	k6eAd1
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
Jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
1955	#num#	k4
–	–	k?
Argentina	Argentina	k1gFnSc1
Brankáři	brankář	k1gMnSc3
</s>
<s>
Marrapodi	Marrapod	k1gMnPc1
•	•	k?
Musimessi	Musimess	k1gMnSc5
Hráči	hráč	k1gMnSc5
</s>
<s>
Bagnatto	Bagnatto	k1gNnSc1
•	•	k?
Colman	Colman	k1gMnSc1
•	•	k?
Dellacha	Dellacha	k1gMnSc1
•	•	k?
Vairo	Vairo	k1gNnSc4
•	•	k?
Balay	Balaa	k1gFnSc2
•	•	k?
Gutiérrez	Gutiérrez	k1gMnSc1
•	•	k?
Leguía	Leguía	k1gMnSc1
•	•	k?
Lombardo	Lombardo	k1gMnSc1
•	•	k?
Mouriñ	Mouriñ	k1gMnSc1
•	•	k?
Sola	Sola	k1gMnSc1
•	•	k?
Bonelli	Bonelle	k1gFnSc3
•	•	k?
Borrello	Borrello	k1gNnSc4
•	•	k?
Cecconato	Cecconat	k2eAgNnSc1d1
•	•	k?
Conde	Cond	k1gInSc5
•	•	k?
Cruz	Cruz	k1gInSc1
•	•	k?
Cucchiaroni	Cucchiaroň	k1gFnSc3
•	•	k?
Grillo	Grillo	k1gNnSc4
•	•	k?
Labruna	Labruna	k1gFnSc1
•	•	k?
Micheli	Michel	k1gInSc6
•	•	k?
Vernazza	Vernazz	k1gMnSc4
Hlavní	hlavní	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
</s>
<s>
Stábile	Stábile	k6eAd1
</s>
<s>
Nejlepší	dobrý	k2eAgMnPc1d3
střelci	střelec	k1gMnPc1
argentinské	argentinský	k2eAgFnSc2d1
fotbalové	fotbalový	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
</s>
<s>
1891	#num#	k4
<g/>
:	:	kIx,
Archer	Archra	k1gFnPc2
</s>
<s>
1892	#num#	k4
</s>
<s>
1893	#num#	k4
<g/>
:	:	kIx,
Leslie	Leslie	k1gFnSc1
</s>
<s>
1894	#num#	k4
<g/>
:	:	kIx,
Gifford	Gifford	k1gInSc1
</s>
<s>
1895	#num#	k4
<g/>
:	:	kIx,
(	(	kIx(
<g/>
No	no	k9
records	records	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
1896	#num#	k4
<g/>
:	:	kIx,
Allen	Allen	k1gMnSc1
/	/	kIx~
Anderson	Anderson	k1gMnSc1
</s>
<s>
1897	#num#	k4
<g/>
:	:	kIx,
Stirling	Stirling	k1gInSc1
</s>
<s>
1898	#num#	k4
<g/>
:	:	kIx,
Allen	Allen	k1gMnSc1
</s>
<s>
1899	#num#	k4
<g/>
:	:	kIx,
Hooton	Hooton	k1gInSc1
</s>
<s>
1900	#num#	k4
<g/>
:	:	kIx,
Leonard	Leonard	k1gMnSc1
</s>
<s>
1901	#num#	k4
<g/>
:	:	kIx,
Dorning	Dorning	k1gInSc1
</s>
<s>
1902	#num#	k4
<g/>
:	:	kIx,
J.	J.	kA
Brown	Brown	k1gMnSc1
</s>
<s>
1903	#num#	k4
<g/>
:	:	kIx,
J.	J.	kA
Brown	Brown	k1gMnSc1
</s>
<s>
1904	#num#	k4
<g/>
:	:	kIx,
A.	A.	kA
Brown	Brown	k1gMnSc1
</s>
<s>
1905	#num#	k4
<g/>
:	:	kIx,
Gonzáles	Gonzáles	k1gMnSc1
/	/	kIx~
Lett	Lett	k1gMnSc1
</s>
<s>
1906	#num#	k4
<g/>
:	:	kIx,
E.	E.	kA
Brown	Brown	k1gMnSc1
/	/	kIx~
Hooton	Hooton	k1gInSc1
/	/	kIx~
Lawrie	Lawrie	k1gFnPc1
/	/	kIx~
Whaley	Whalea	k1gFnPc1
</s>
<s>
1907	#num#	k4
<g/>
:	:	kIx,
E.	E.	kA
Brown	Brown	k1gMnSc1
</s>
<s>
1908	#num#	k4
<g/>
:	:	kIx,
E.	E.	kA
Brown	Brown	k1gMnSc1
</s>
<s>
1909	#num#	k4
<g/>
:	:	kIx,
E.	E.	kA
Brown	Brown	k1gMnSc1
</s>
<s>
1910	#num#	k4
<g/>
:	:	kIx,
A.	A.	kA
W.	W.	kA
Hutton	Hutton	k1gInSc1
</s>
<s>
1911	#num#	k4
<g/>
:	:	kIx,
Malbrán	Malbrán	k2eAgMnSc1d1
/	/	kIx~
E.	E.	kA
Lett	Lett	k1gMnSc1
/	/	kIx~
Piaggio	Piaggio	k1gMnSc1
</s>
<s>
1912	#num#	k4
<g/>
:	:	kIx,
Ohaco	Ohaco	k6eAd1
</s>
<s>
1912	#num#	k4
FAF	FAF	kA
<g/>
:	:	kIx,
Colla	Colla	k1gMnSc1
</s>
<s>
1913	#num#	k4
<g/>
:	:	kIx,
Ohaco	Ohaco	k6eAd1
</s>
<s>
1913	#num#	k4
FAF	FAF	kA
<g/>
:	:	kIx,
Dannaher	Dannahra	k1gFnPc2
</s>
<s>
1914	#num#	k4
<g/>
:	:	kIx,
Ohaco	Ohaco	k6eAd1
</s>
<s>
1914	#num#	k4
FAF	FAF	kA
<g/>
:	:	kIx,
Carabelli	Carabell	k1gMnSc3
</s>
<s>
1915	#num#	k4
<g/>
:	:	kIx,
Ohaco	Ohaco	k6eAd1
</s>
<s>
1916	#num#	k4
<g/>
:	:	kIx,
Hiller	Hiller	k1gInSc1
</s>
<s>
1917	#num#	k4
<g/>
:	:	kIx,
Marcovecchio	Marcovecchio	k6eAd1
</s>
<s>
1918	#num#	k4
<g/>
:	:	kIx,
Zabaleta	Zabaleta	k1gFnSc1
</s>
<s>
1919	#num#	k4
<g/>
:	:	kIx,
Garasini	Garasin	k2eAgMnPc1d1
/	/	kIx~
Martín	Martín	k1gMnSc1
</s>
<s>
1919	#num#	k4
AAmF	AAmF	k1gMnSc1
<g/>
:	:	kIx,
Marcovecchio	Marcovecchio	k1gMnSc1
</s>
<s>
1920	#num#	k4
<g/>
:	:	kIx,
F.	F.	kA
Lucarelli	Lucarelle	k1gFnSc4
</s>
<s>
1920	#num#	k4
AAmF	AAmF	k1gMnSc1
<g/>
:	:	kIx,
Carreras	Carreras	k1gMnSc1
</s>
<s>
1921	#num#	k4
<g/>
:	:	kIx,
Dannaher	Dannahra	k1gFnPc2
</s>
<s>
1921	#num#	k4
AAmF	AAmF	k1gFnSc1
<g/>
:	:	kIx,
Zabaleta	Zabaleta	k1gFnSc1
</s>
<s>
1922	#num#	k4
<g/>
:	:	kIx,
Clarke	Clark	k1gInSc2
/	/	kIx~
Tarasconi	Tarascon	k1gMnPc1
</s>
<s>
1922	#num#	k4
AAmF	AAmF	k1gFnSc1
<g/>
:	:	kIx,
Seoane	Seoan	k1gInSc5
</s>
<s>
1923	#num#	k4
<g/>
:	:	kIx,
Tarasconi	Tarascoň	k1gFnSc6
</s>
<s>
1923	#num#	k4
AAmF	AAmF	k1gFnSc1
<g/>
:	:	kIx,
Barceló	Barceló	k1gFnSc1
</s>
<s>
1924	#num#	k4
<g/>
:	:	kIx,
Tarasconi	Tarascoň	k1gFnSc6
</s>
<s>
1924	#num#	k4
AAmF	AAmF	k1gFnSc1
<g/>
:	:	kIx,
R.	R.	kA
Lucarelli	Lucarelle	k1gFnSc6
/	/	kIx~
Ravaschino	Ravaschino	k1gNnSc1
</s>
<s>
1925	#num#	k4
<g/>
:	:	kIx,
Gaslini	Gaslin	k2eAgMnPc1d1
</s>
<s>
1925	#num#	k4
AAmF	AAmF	k1gFnSc1
<g/>
:	:	kIx,
Bellomo	Belloma	k1gFnSc5
</s>
<s>
1926	#num#	k4
<g/>
:	:	kIx,
Cherro	Cherro	k1gNnSc1
</s>
<s>
1926	#num#	k4
AAmF	AAmF	k1gFnSc1
<g/>
:	:	kIx,
Seoane	Seoan	k1gMnSc5
</s>
<s>
1927	#num#	k4
<g/>
:	:	kIx,
Tarasconi	Tarascoň	k1gFnSc6
</s>
<s>
1928	#num#	k4
<g/>
:	:	kIx,
Cherro	Cherro	k1gNnSc1
</s>
<s>
1929	#num#	k4
<g/>
:	:	kIx,
Giúdice	Giúdice	k1gFnSc1
</s>
<s>
1930	#num#	k4
<g/>
:	:	kIx,
Cherro	Cherro	k1gNnSc1
</s>
<s>
1931	#num#	k4
<g/>
:	:	kIx,
Zozaya	Zozay	k1gInSc2
</s>
<s>
1932	#num#	k4
<g/>
:	:	kIx,
B.	B.	kA
Ferreyra	Ferreyra	k1gMnSc1
</s>
<s>
1933	#num#	k4
<g/>
:	:	kIx,
Varallo	Varallo	k1gNnSc1
</s>
<s>
1934	#num#	k4
<g/>
:	:	kIx,
Barrera	Barrera	k1gFnSc1
</s>
<s>
1935	#num#	k4
<g/>
:	:	kIx,
Cosso	Cossa	k1gFnSc5
</s>
<s>
1936	#num#	k4
<g/>
:	:	kIx,
Barrera	Barrera	k1gFnSc1
</s>
<s>
1937	#num#	k4
<g/>
:	:	kIx,
Erico	Erico	k6eAd1
</s>
<s>
1938	#num#	k4
<g/>
:	:	kIx,
Erico	Erico	k6eAd1
</s>
<s>
1939	#num#	k4
<g/>
:	:	kIx,
Erico	Erico	k6eAd1
</s>
<s>
1940	#num#	k4
<g/>
:	:	kIx,
Benítez	Benítez	k1gMnSc1
Cáceres	Cáceres	k1gMnSc1
/	/	kIx~
Lángara	Lángara	k1gFnSc1
</s>
<s>
1941	#num#	k4
<g/>
:	:	kIx,
Canteli	Cantel	k1gInSc3
</s>
<s>
1942	#num#	k4
<g/>
:	:	kIx,
Martino	Martin	k2eAgNnSc1d1
</s>
<s>
1943	#num#	k4
<g/>
:	:	kIx,
Arrieta	Arrieta	k1gFnSc1
/	/	kIx~
Labruna	Labruna	k1gFnSc1
/	/	kIx~
Frutos	Frutos	k1gInSc1
</s>
<s>
1944	#num#	k4
<g/>
:	:	kIx,
Mellone	Mellon	k1gInSc5
</s>
<s>
1945	#num#	k4
<g/>
:	:	kIx,
Labruna	Labruna	k1gFnSc1
</s>
<s>
1946	#num#	k4
<g/>
:	:	kIx,
Boyé	Boyý	k2eAgFnSc2d1
</s>
<s>
1947	#num#	k4
<g/>
:	:	kIx,
Di	Di	k1gFnSc1
Stéfano	Stéfana	k1gFnSc5
</s>
<s>
1948	#num#	k4
<g/>
:	:	kIx,
Santos	Santos	k1gInSc1
</s>
<s>
1949	#num#	k4
<g/>
:	:	kIx,
Simes	Simes	k1gInSc1
/	/	kIx~
Pizzuti	Pizzuť	k1gFnPc1
</s>
<s>
1950	#num#	k4
<g/>
:	:	kIx,
Papa	papa	k1gMnSc1
</s>
<s>
1951	#num#	k4
<g/>
:	:	kIx,
Vernazza	Vernazza	k1gFnSc1
</s>
<s>
1952	#num#	k4
<g/>
:	:	kIx,
Ricagni	Ricageň	k1gFnSc6
</s>
<s>
1953	#num#	k4
<g/>
:	:	kIx,
Pizzuti	Pizzuť	k1gFnSc2
/	/	kIx~
Benavídez	Benavídez	k1gInSc1
</s>
<s>
1954	#num#	k4
<g/>
:	:	kIx,
Berni	Berni	k?
/	/	kIx~
Conde	Cond	k1gInSc5
/	/	kIx~
Borello	Borella	k1gMnSc5
</s>
<s>
1955	#num#	k4
<g/>
:	:	kIx,
Massei	Masse	k1gFnSc2
</s>
<s>
1956	#num#	k4
<g/>
:	:	kIx,
Castro	Castro	k1gNnSc1
/	/	kIx~
Grillo	Grillo	k1gNnSc1
</s>
<s>
1957	#num#	k4
<g/>
:	:	kIx,
Rob	roba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zárate	Zárat	k1gMnSc5
</s>
<s>
1958	#num#	k4
<g/>
:	:	kIx,
Sanfilippo	Sanfilippa	k1gFnSc5
</s>
<s>
1959	#num#	k4
<g/>
:	:	kIx,
Sanfilippo	Sanfilippa	k1gFnSc5
</s>
<s>
1960	#num#	k4
<g/>
:	:	kIx,
Sanfilippo	Sanfilippa	k1gFnSc5
</s>
<s>
1961	#num#	k4
<g/>
:	:	kIx,
Sanfilippo	Sanfilippa	k1gFnSc5
</s>
<s>
1962	#num#	k4
<g/>
:	:	kIx,
Artime	Artim	k1gInSc5
</s>
<s>
1963	#num#	k4
<g/>
:	:	kIx,
Artime	Artim	k1gMnSc5
</s>
<s>
1964	#num#	k4
<g/>
:	:	kIx,
Veira	Veir	k1gInSc2
</s>
<s>
1965	#num#	k4
<g/>
:	:	kIx,
Carone	Caron	k1gInSc5
</s>
<s>
1966	#num#	k4
<g/>
:	:	kIx,
Artime	Artim	k1gInSc5
</s>
<s>
Met	met	k1gInSc1
1967	#num#	k4
<g/>
:	:	kIx,
B.	B.	kA
Acosta	Acosta	k1gMnSc1
</s>
<s>
Nac	Nac	k?
1967	#num#	k4
<g/>
:	:	kIx,
Artime	Artim	k1gMnSc5
</s>
<s>
Met	met	k1gInSc1
1968	#num#	k4
<g/>
:	:	kIx,
Obberti	Obbert	k1gMnPc1
</s>
<s>
Nac	Nac	k?
1968	#num#	k4
<g/>
:	:	kIx,
Wehbe	Wehb	k1gInSc5
</s>
<s>
Met	met	k1gInSc1
1969	#num#	k4
<g/>
:	:	kIx,
Machado	Machada	k1gFnSc5
</s>
<s>
Nac	Nac	k?
1969	#num#	k4
<g/>
:	:	kIx,
Fischer	Fischer	k1gMnSc1
/	/	kIx~
Bulla	bulla	k1gFnSc1
</s>
<s>
Met	met	k1gInSc1
1970	#num#	k4
<g/>
:	:	kIx,
Más	Más	k1gFnSc1
</s>
<s>
Nac	Nac	k?
1970	#num#	k4
<g/>
:	:	kIx,
Bianchi	Bianch	k1gFnSc2
</s>
<s>
Met	met	k1gInSc1
1971	#num#	k4
<g/>
:	:	kIx,
Bianchi	Bianch	k1gFnSc2
</s>
<s>
Nac	Nac	k?
1971	#num#	k4
<g/>
:	:	kIx,
Obberti	Obbert	k1gMnPc1
/	/	kIx~
Luñ	Luñ	k1gMnSc1
</s>
<s>
Met	met	k1gInSc1
1972	#num#	k4
<g/>
:	:	kIx,
Brindisi	Brindisi	k1gNnSc2
</s>
<s>
Nac	Nac	k?
1972	#num#	k4
<g/>
:	:	kIx,
Morete	More	k1gNnSc2
</s>
<s>
Met	met	k1gInSc1
1973	#num#	k4
<g/>
:	:	kIx,
Más	Más	k1gMnPc1
/	/	kIx~
Curioni	Curion	k1gMnPc1
/	/	kIx~
Peñ	Peñ	k1gMnSc1
</s>
<s>
Nac	Nac	k?
1973	#num#	k4
<g/>
:	:	kIx,
Voglino	Voglin	k2eAgNnSc1d1
</s>
<s>
Met	met	k1gInSc1
1974	#num#	k4
<g/>
:	:	kIx,
Morete	More	k1gNnSc2
</s>
<s>
Nac	Nac	k?
1974	#num#	k4
<g/>
:	:	kIx,
Kempes	Kempes	k1gInSc1
</s>
<s>
Met	met	k1gInSc1
1975	#num#	k4
<g/>
:	:	kIx,
Scotta	Scott	k1gInSc2
</s>
<s>
Nac	Nac	k?
1975	#num#	k4
<g/>
:	:	kIx,
Scotta	Scott	k1gInSc2
</s>
<s>
Met	met	k1gInSc1
1976	#num#	k4
<g/>
:	:	kIx,
Kempes	Kempes	k1gInSc1
</s>
<s>
Nac	Nac	k?
1976	#num#	k4
<g/>
:	:	kIx,
Eresuma	Eresuma	k1gFnSc1
/	/	kIx~
Ludueñ	Ludueñ	k1gFnSc1
/	/	kIx~
Marchetti	Marchetti	k1gNnSc1
</s>
<s>
Met	met	k1gInSc1
1977	#num#	k4
<g/>
:	:	kIx,
Álvarez	Álvarez	k1gInSc1
</s>
<s>
Nac	Nac	k?
1977	#num#	k4
<g/>
:	:	kIx,
Letanu	Letan	k1gInSc2
</s>
<s>
Met	met	k1gInSc1
1978	#num#	k4
<g/>
:	:	kIx,
Maradona	Maradona	k1gFnSc1
/	/	kIx~
Andreuchi	Andreuchi	k1gNnSc1
</s>
<s>
Nac	Nac	k?
1978	#num#	k4
<g/>
:	:	kIx,
Reinaldi	Reinald	k1gMnPc1
</s>
<s>
Met	met	k1gInSc1
1979	#num#	k4
<g/>
:	:	kIx,
Maradona	Maradona	k1gFnSc1
/	/	kIx~
Fortunato	Fortunat	k2eAgNnSc1d1
</s>
<s>
Nac	Nac	k?
1979	#num#	k4
<g/>
:	:	kIx,
Maradona	Maradona	k1gFnSc1
</s>
<s>
Met	met	k1gInSc1
1980	#num#	k4
<g/>
:	:	kIx,
Maradona	Maradona	k1gFnSc1
</s>
<s>
Nac	Nac	k?
1980	#num#	k4
<g/>
:	:	kIx,
Maradona	Maradona	k1gFnSc1
</s>
<s>
Met	met	k1gInSc1
1981	#num#	k4
<g/>
:	:	kIx,
Chaparro	Chaparro	k1gNnSc1
</s>
<s>
Nac	Nac	k?
1981	#num#	k4
<g/>
:	:	kIx,
Bianchi	Bianch	k1gFnSc2
</s>
<s>
Nac	Nac	k?
1982	#num#	k4
<g/>
:	:	kIx,
Juárez	Juárez	k1gInSc1
</s>
<s>
Met	met	k1gInSc1
1982	#num#	k4
<g/>
:	:	kIx,
Morete	More	k1gNnSc2
</s>
<s>
Nac	Nac	k?
1983	#num#	k4
<g/>
:	:	kIx,
Husillos	Husillos	k1gInSc1
</s>
<s>
Met	met	k1gInSc1
1983	#num#	k4
<g/>
:	:	kIx,
Ramos	Ramos	k1gInSc1
</s>
<s>
Nac	Nac	k?
1984	#num#	k4
<g/>
:	:	kIx,
Pasculli	Pasculle	k1gFnSc6
</s>
<s>
Met	met	k1gInSc1
1984	#num#	k4
<g/>
:	:	kIx,
Francescoli	Francescole	k1gFnSc6
</s>
<s>
Nac	Nac	k?
1985	#num#	k4
<g/>
:	:	kIx,
Comas	Comas	k1gInSc1
</s>
<s>
1985	#num#	k4
<g/>
–	–	k?
<g/>
86	#num#	k4
<g/>
:	:	kIx,
Francescoli	Francescole	k1gFnSc6
</s>
<s>
1986	#num#	k4
<g/>
–	–	k?
<g/>
87	#num#	k4
<g/>
:	:	kIx,
Palma	palma	k1gFnSc1
</s>
<s>
1987	#num#	k4
<g/>
–	–	k?
<g/>
88	#num#	k4
<g/>
:	:	kIx,
J.	J.	kA
L.	L.	kA
Rodríguez	Rodríguez	k1gMnSc1
</s>
<s>
1988	#num#	k4
<g/>
–	–	k?
<g/>
89	#num#	k4
<g/>
:	:	kIx,
Dertycia	Dertycium	k1gNnSc2
/	/	kIx~
Gorosito	Gorosita	k1gFnSc5
</s>
<s>
1989	#num#	k4
<g/>
–	–	k?
<g/>
90	#num#	k4
<g/>
:	:	kIx,
Cozzoni	Cozzoň	k1gFnSc6
</s>
<s>
1990	#num#	k4
<g/>
–	–	k?
<g/>
91	#num#	k4
<g/>
:	:	kIx,
González	González	k1gMnSc1
</s>
<s>
Ap	ap	kA
1991	#num#	k4
<g/>
:	:	kIx,
Díaz	Díaz	k1gInSc1
</s>
<s>
Cl	Cl	k?
1992	#num#	k4
<g/>
:	:	kIx,
Scotto	Scotto	k1gNnSc1
/	/	kIx~
Latorre	Latorr	k1gMnSc5
</s>
<s>
Ap	ap	kA
1992	#num#	k4
<g/>
:	:	kIx,
A.	A.	kA
Acosta	Acosta	k1gMnSc1
</s>
<s>
Cl	Cl	k?
1993	#num#	k4
<g/>
:	:	kIx,
da	da	k?
Silva	Silva	k1gFnSc1
</s>
<s>
Ap	ap	kA
1993	#num#	k4
<g/>
:	:	kIx,
Martínez	Martínez	k1gInSc1
</s>
<s>
Cl	Cl	k?
1994	#num#	k4
<g/>
:	:	kIx,
Espina	Espino	k1gNnSc2
/	/	kIx~
Crespo	Crespa	k1gFnSc5
</s>
<s>
Ap	ap	kA
1994	#num#	k4
<g/>
:	:	kIx,
Francescoli	Francescole	k1gFnSc6
</s>
<s>
Cl	Cl	k?
1995	#num#	k4
<g/>
:	:	kIx,
Flores	Flores	k1gInSc1
</s>
<s>
Ap	ap	kA
1995	#num#	k4
<g/>
:	:	kIx,
Calderón	Calderón	k1gInSc1
</s>
<s>
Cl	Cl	k?
1996	#num#	k4
<g/>
:	:	kIx,
A.	A.	kA
López	López	k1gMnSc1
</s>
<s>
Ap	ap	kA
1996	#num#	k4
<g/>
:	:	kIx,
Reggi	Regg	k1gFnSc2
</s>
<s>
Cl	Cl	k?
1997	#num#	k4
<g/>
:	:	kIx,
Martínez	Martínez	k1gInSc1
</s>
<s>
Ap	ap	kA
1997	#num#	k4
<g/>
:	:	kIx,
da	da	k?
Silva	Silva	k1gFnSc1
</s>
<s>
Cl	Cl	k?
1998	#num#	k4
<g/>
:	:	kIx,
Sosa	Sos	k1gInSc2
</s>
<s>
Ap	ap	kA
1998	#num#	k4
<g/>
:	:	kIx,
Palermo	Palermo	k1gNnSc1
</s>
<s>
Cl	Cl	k?
1999	#num#	k4
<g/>
:	:	kIx,
Calderón	Calderón	k1gInSc1
</s>
<s>
Ap	ap	kA
1999	#num#	k4
<g/>
:	:	kIx,
Saviola	Saviola	k1gFnSc1
</s>
<s>
Cl	Cl	k?
2000	#num#	k4
<g/>
:	:	kIx,
Fuertes	Fuertes	k1gInSc1
</s>
<s>
Ap	ap	kA
2000	#num#	k4
<g/>
:	:	kIx,
Ángel	Ángel	k1gInSc1
</s>
<s>
Cl	Cl	k?
2001	#num#	k4
<g/>
:	:	kIx,
Romeo	Romeo	k1gMnSc1
</s>
<s>
Ap	ap	kA
2001	#num#	k4
<g/>
:	:	kIx,
Cardetti	Cardetť	k1gFnSc2
</s>
<s>
Cl	Cl	k?
2002	#num#	k4
<g/>
:	:	kIx,
Cavenaghi	Cavenagh	k1gFnSc2
</s>
<s>
Ap	ap	kA
2002	#num#	k4
<g/>
:	:	kIx,
Silvera	Silvera	k1gFnSc1
</s>
<s>
Cl	Cl	k?
2003	#num#	k4
<g/>
:	:	kIx,
Figueroa	Figuero	k1gInSc2
</s>
<s>
Ap	ap	kA
2003	#num#	k4
<g/>
:	:	kIx,
Farías	Farías	k1gInSc1
</s>
<s>
Cl	Cl	k?
2004	#num#	k4
<g/>
:	:	kIx,
Rolando	Rolanda	k1gFnSc5
Zárate	Zárat	k1gInSc5
</s>
<s>
Ap	ap	kA
2004	#num#	k4
<g/>
:	:	kIx,
L.	L.	kA
López	López	k1gMnSc1
</s>
<s>
Cl	Cl	k?
2005	#num#	k4
<g/>
:	:	kIx,
Pavone	Pavon	k1gInSc5
</s>
<s>
Ap	ap	kA
2005	#num#	k4
<g/>
:	:	kIx,
Cámpora	Cámpora	k1gFnSc1
</s>
<s>
Cl	Cl	k?
2006	#num#	k4
<g/>
:	:	kIx,
Vargas	Vargas	k1gInSc1
</s>
<s>
Ap	ap	kA
2006	#num#	k4
<g/>
:	:	kIx,
M.	M.	kA
Zárate	Zárat	k1gInSc5
/	/	kIx~
Palacio	Palacio	k6eAd1
</s>
<s>
Cl	Cl	k?
2007	#num#	k4
<g/>
:	:	kIx,
Palermo	Palermo	k1gNnSc1
</s>
<s>
Ap	ap	kA
2007	#num#	k4
<g/>
:	:	kIx,
Denis	Denisa	k1gFnPc2
</s>
<s>
Cl	Cl	k?
2008	#num#	k4
<g/>
:	:	kIx,
Cvitanich	Cvitanich	k1gInSc1
</s>
<s>
Ap	ap	kA
2008	#num#	k4
<g/>
:	:	kIx,
Sand	Sand	k1gInSc1
</s>
<s>
Cl	Cl	k?
2009	#num#	k4
<g/>
:	:	kIx,
Sand	Sand	k1gInSc1
</s>
<s>
Ap	ap	kA
2009	#num#	k4
<g/>
:	:	kIx,
Silva	Silva	k1gFnSc1
</s>
<s>
Cl	Cl	k?
2010	#num#	k4
<g/>
:	:	kIx,
Boselli	Boselle	k1gFnSc6
</s>
<s>
Ap	ap	kA
2010	#num#	k4
<g/>
:	:	kIx,
Stracqualursi	Stracqualurse	k1gFnSc6
/	/	kIx~
Silva	Silva	k1gFnSc1
</s>
<s>
Cl	Cl	k?
2011	#num#	k4
<g/>
:	:	kIx,
Cámpora	Cámpora	k1gFnSc1
/	/	kIx~
Gutiérrez	Gutiérrez	k1gInSc1
</s>
<s>
Ap	ap	kA
2011	#num#	k4
<g/>
:	:	kIx,
Ramírez	Ramírez	k1gInSc1
</s>
<s>
Cl	Cl	k?
2012	#num#	k4
<g/>
:	:	kIx,
Luna	luna	k1gFnSc1
</s>
<s>
In	In	k?
2012	#num#	k4
<g/>
:	:	kIx,
F.	F.	kA
Ferreyra	Ferreyra	k1gMnSc1
/	/	kIx~
Scocco	Scocco	k1gMnSc1
</s>
<s>
Fi	fi	k0
2013	#num#	k4
<g/>
:	:	kIx,
Gigliotti	Gigliotti	k1gNnSc2
/	/	kIx~
Scocco	Scocco	k6eAd1
</s>
<s>
In	In	k?
2013	#num#	k4
<g/>
:	:	kIx,
Pereyra	Pereyr	k1gInSc2
</s>
<s>
Fi	fi	k0
2014	#num#	k4
<g/>
:	:	kIx,
M.	M.	kA
Zárate	Zárat	k1gMnSc5
</s>
<s>
Tr	Tr	k?
2014	#num#	k4
<g/>
:	:	kIx,
Pratto	Pratto	k1gNnSc1
/	/	kIx~
M.	M.	kA
Rodríguez	Rodríguez	k1gInSc1
/	/	kIx~
Romero	Romero	k1gNnSc1
</s>
<s>
2015	#num#	k4
<g/>
:	:	kIx,
Ruben	ruben	k2eAgInSc1d1
</s>
<s>
2016	#num#	k4
<g/>
:	:	kIx,
Sand	Sand	k1gInSc1
</s>
<s>
2016	#num#	k4
<g/>
–	–	k?
<g/>
17	#num#	k4
<g/>
:	:	kIx,
Benedetto	Benedetto	k1gNnSc1
</s>
<s>
2017	#num#	k4
<g/>
–	–	k?
<g/>
18	#num#	k4
<g/>
:	:	kIx,
García	Garcí	k1gInSc2
</s>
<s>
2018	#num#	k4
<g/>
–	–	k?
<g/>
19	#num#	k4
<g/>
:	:	kIx,
L.	L.	kA
López	López	k1gMnSc1
</s>
<s>
2020	#num#	k4
<g/>
:	:	kIx,
Borré	Borrý	k2eAgNnSc1d1
/	/	kIx~
Romero	Romero	k1gNnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
</s>
