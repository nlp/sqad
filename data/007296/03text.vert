<s>
Lanová	lanový	k2eAgFnSc1d1	lanová
dráha	dráha	k1gFnSc1	dráha
(	(	kIx(	(
<g/>
lanovka	lanovka	k1gFnSc1	lanovka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dráha	dráha	k1gFnSc1	dráha
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
vozidla	vozidlo	k1gNnPc1	vozidlo
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
po	po	k7c6	po
šikmé	šikmý	k2eAgFnSc6d1	šikmá
nebo	nebo	k8xC	nebo
vodorovné	vodorovný	k2eAgFnSc6d1	vodorovná
trase	trasa	k1gFnSc6	trasa
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
poháněna	poháněn	k2eAgNnPc1d1	poháněno
pomocí	pomocí	k7c2	pomocí
tažných	tažný	k2eAgNnPc2d1	tažné
lan	lano	k1gNnPc2	lano
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
lanovek	lanovka	k1gFnPc2	lanovka
jsou	být	k5eAaImIp3nP	být
vozidla	vozidlo	k1gNnPc1	vozidlo
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
vozidla	vozidlo	k1gNnPc1	vozidlo
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
i	i	k9	i
sedačky	sedačka	k1gFnPc1	sedačka
<g/>
)	)	kIx)	)
zavěšena	zavěšen	k2eAgFnSc1d1	zavěšena
na	na	k7c6	na
nosných	nosný	k2eAgNnPc6d1	nosné
lanech	lano	k1gNnPc6	lano
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
dopravě	doprava	k1gFnSc3	doprava
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
třeba	třeba	k6eAd1	třeba
překonat	překonat	k5eAaPmF	překonat
velký	velký	k2eAgInSc4d1	velký
výškový	výškový	k2eAgInSc4d1	výškový
rozdíl	rozdíl	k1gInSc4	rozdíl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
často	často	k6eAd1	často
i	i	k8xC	i
lanovka	lanovka	k1gFnSc1	lanovka
sama	sám	k3xTgMnSc4	sám
je	být	k5eAaImIp3nS	být
turistickým	turistický	k2eAgNnSc7d1	turistické
lákadlem	lákadlo	k1gNnSc7	lákadlo
a	a	k8xC	a
jízda	jízda	k1gFnSc1	jízda
lanovkou	lanovka	k1gFnSc7	lanovka
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
nevšední	všední	k2eNgInSc4d1	nevšední
zážitek	zážitek	k1gInSc4	zážitek
<g/>
.	.	kIx.	.
</s>
<s>
Lanová	lanový	k2eAgFnSc1d1	lanová
dráha	dráha	k1gFnSc1	dráha
má	mít	k5eAaImIp3nS	mít
vždy	vždy	k6eAd1	vždy
nejméně	málo	k6eAd3	málo
dvě	dva	k4xCgFnPc4	dva
stanice	stanice	k1gFnPc4	stanice
<g/>
,	,	kIx,	,
horní	horní	k2eAgFnSc4d1	horní
a	a	k8xC	a
dolní	dolní	k2eAgFnSc4d1	dolní
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
lanové	lanový	k2eAgFnPc1d1	lanová
dráhy	dráha	k1gFnPc1	dráha
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
na	na	k7c6	na
trase	trasa	k1gFnSc6	trasa
jednu	jeden	k4xCgFnSc4	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
mezistanic	mezistanice	k1gFnPc2	mezistanice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
na	na	k7c4	na
lanové	lanový	k2eAgFnPc4d1	lanová
dráhy	dráha	k1gFnPc4	dráha
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
Zákon	zákon	k1gInSc1	zákon
o	o	k7c6	o
dráhách	dráha	k1gFnPc6	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Vagony	vagon	k1gInPc1	vagon
<g/>
,	,	kIx,	,
kabiny	kabina	k1gFnPc1	kabina
<g/>
,	,	kIx,	,
gondoly	gondola	k1gFnPc1	gondola
i	i	k8xC	i
tyče	tyč	k1gFnPc1	tyč
se	s	k7c7	s
sedačkami	sedačka	k1gFnPc7	sedačka
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
terminologického	terminologický	k2eAgNnSc2d1	terminologické
hlediska	hledisko	k1gNnSc2	hledisko
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
vozidla	vozidlo	k1gNnPc4	vozidlo
<g/>
.	.	kIx.	.
</s>
<s>
Lanové	lanový	k2eAgFnPc1d1	lanová
dráhy	dráha	k1gFnPc1	dráha
zároveň	zároveň	k6eAd1	zároveň
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
dopravních	dopravní	k2eAgInPc2d1	dopravní
prostředků	prostředek	k1gInPc2	prostředek
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
souhrnně	souhrnně	k6eAd1	souhrnně
označují	označovat	k5eAaImIp3nP	označovat
zvihadla	zvihadlo	k1gNnPc4	zvihadlo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
jízdy	jízda	k1gFnSc2	jízda
a	a	k8xC	a
vozidel	vozidlo	k1gNnPc2	vozidlo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
lanovka	lanovka	k1gFnSc1	lanovka
Visutá	visutý	k2eAgFnSc1d1	visutá
lanová	lanový	k2eAgFnSc1d1	lanová
dráha	dráha	k1gFnSc1	dráha
–	–	k?	–
vozidlo	vozidlo	k1gNnSc1	vozidlo
(	(	kIx(	(
<g/>
kabina	kabina	k1gFnSc1	kabina
nebo	nebo	k8xC	nebo
sedačka	sedačka	k1gFnSc1	sedačka
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nepohybuje	pohybovat	k5eNaImIp3nS	pohybovat
po	po	k7c6	po
zemi	zem	k1gFnSc6	zem
nebo	nebo	k8xC	nebo
kolejích	kolej	k1gFnPc6	kolej
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
během	během	k7c2	během
jízdy	jízda	k1gFnSc2	jízda
jen	jen	k6eAd1	jen
zavěšeno	zavěsit	k5eAaPmNgNnS	zavěsit
na	na	k7c6	na
lanech	lano	k1gNnPc6	lano
<g/>
.	.	kIx.	.
</s>
<s>
Kabinová	kabinový	k2eAgFnSc1d1	kabinová
–	–	k?	–
obvykle	obvykle	k6eAd1	obvykle
dvě	dva	k4xCgFnPc1	dva
zavěšené	zavěšený	k2eAgFnPc1d1	zavěšená
kabiny	kabina	k1gFnPc1	kabina
pohybující	pohybující	k2eAgFnPc1d1	pohybující
se	s	k7c7	s
vzduchem	vzduch	k1gInSc7	vzduch
protisměrně	protisměrně	k6eAd1	protisměrně
<g/>
.	.	kIx.	.
</s>
<s>
Kabinková	kabinkový	k2eAgFnSc1d1	kabinková
(	(	kIx(	(
<g/>
gondolová	gondolový	k2eAgFnSc1d1	gondolová
<g/>
)	)	kIx)	)
–	–	k?	–
na	na	k7c6	na
dopravním	dopravní	k2eAgInSc6d1	dopravní
<g/>
,	,	kIx,	,
event.	event.	k?	event.
tažném	tažný	k2eAgNnSc6d1	tažné
laně	lano	k1gNnSc6	lano
jsou	být	k5eAaImIp3nP	být
zavěšeny	zavěsit	k5eAaPmNgInP	zavěsit
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
kabinky	kabinka	k1gFnSc2	kabinka
pro	pro	k7c4	pro
2	[number]	k4	2
-	-	kIx~	-
16	[number]	k4	16
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Sedačková	sedačkový	k2eAgFnSc1d1	sedačková
–	–	k?	–
nosné	nosný	k2eAgNnSc1d1	nosné
lano	lano	k1gNnSc1	lano
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
tažným	tažný	k2eAgInSc7d1	tažný
<g/>
,	,	kIx,	,
na	na	k7c6	na
něm	on	k3xPp3gInSc6	on
jsou	být	k5eAaImIp3nP	být
zavěšeny	zavěsit	k5eAaPmNgInP	zavěsit
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
sedačky	sedačka	k1gFnSc2	sedačka
pro	pro	k7c4	pro
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
nebo	nebo	k8xC	nebo
8	[number]	k4	8
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
umožňovat	umožňovat	k5eAaImF	umožňovat
i	i	k9	i
přepravu	přeprava	k1gFnSc4	přeprava
lyžařů	lyžař	k1gMnPc2	lyžař
s	s	k7c7	s
lyžemi	lyže	k1gFnPc7	lyže
na	na	k7c6	na
nohou	noha	k1gFnPc6	noha
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Vozidlem	vozidlo	k1gNnSc7	vozidlo
<g/>
"	"	kIx"	"
této	tento	k3xDgFnSc2	tento
dráhy	dráha	k1gFnSc2	dráha
je	být	k5eAaImIp3nS	být
každý	každý	k3xTgInSc1	každý
závěs	závěs	k1gInSc1	závěs
se	s	k7c7	s
sedačkou	sedačka	k1gFnSc7	sedačka
<g/>
.	.	kIx.	.
</s>
<s>
Kombinovaná	kombinovaný	k2eAgFnSc1d1	kombinovaná
–	–	k?	–
na	na	k7c6	na
laně	lano	k1gNnSc6	lano
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
i	i	k9	i
kabinové	kabinový	k2eAgFnPc1d1	kabinová
i	i	k8xC	i
sedačkové	sedačkový	k2eAgFnPc1d1	sedačková
lanovky	lanovka	k1gFnPc1	lanovka
<g/>
.	.	kIx.	.
</s>
<s>
Pozemní	pozemní	k2eAgFnSc1d1	pozemní
lanová	lanový	k2eAgFnSc1d1	lanová
dráha	dráha	k1gFnSc1	dráha
–	–	k?	–
obvykle	obvykle	k6eAd1	obvykle
dva	dva	k4xCgInPc4	dva
vozy	vůz	k1gInPc4	vůz
pohybující	pohybující	k2eAgInPc4d1	pohybující
se	se	k3xPyFc4	se
po	po	k7c6	po
kolejích	kolej	k1gFnPc6	kolej
každý	každý	k3xTgInSc4	každý
v	v	k7c6	v
opačném	opačný	k2eAgInSc6d1	opačný
směru	směr	k1gInSc6	směr
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
druhů	druh	k1gInPc2	druh
lan	lano	k1gNnPc2	lano
se	se	k3xPyFc4	se
visuté	visutý	k2eAgFnPc1d1	visutá
lanovky	lanovka	k1gFnPc1	lanovka
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
na	na	k7c4	na
Jednolanové	jednolanový	k2eAgFnPc4d1	jednolanová
–	–	k?	–
funkce	funkce	k1gFnPc4	funkce
nosného	nosný	k2eAgMnSc2d1	nosný
a	a	k8xC	a
tažného	tažný	k2eAgNnSc2d1	tažné
lana	lano	k1gNnSc2	lano
je	být	k5eAaImIp3nS	být
sloučena	sloučit	k5eAaPmNgFnS	sloučit
do	do	k7c2	do
jediného	jediný	k2eAgNnSc2d1	jediné
dopravního	dopravní	k2eAgNnSc2d1	dopravní
lana	lano	k1gNnSc2	lano
Dvoulanové	dvoulanový	k2eAgFnSc2d1	dvoulanový
–	–	k?	–
vozidla	vozidlo	k1gNnPc1	vozidlo
jsou	být	k5eAaImIp3nP	být
tažena	tažen	k2eAgNnPc1d1	taženo
na	na	k7c6	na
jednom	jeden	k4xCgMnSc6	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
tažných	tažný	k2eAgNnPc6d1	tažné
lanech	lano	k1gNnPc6	lano
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
jsou	být	k5eAaImIp3nP	být
nesena	nesen	k2eAgNnPc1d1	neseno
jedním	jeden	k4xCgInSc7	jeden
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
nosnými	nosný	k2eAgNnPc7d1	nosné
lany	lano	k1gNnPc7	lano
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
oběhu	oběh	k1gInSc2	oběh
vozidel	vozidlo	k1gNnPc2	vozidlo
se	se	k3xPyFc4	se
lanovky	lanovka	k1gFnPc1	lanovka
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
na	na	k7c6	na
Oběžné	oběžný	k2eAgFnSc6d1	oběžná
–	–	k?	–
vozidla	vozidlo	k1gNnPc4	vozidlo
se	se	k3xPyFc4	se
na	na	k7c6	na
konci	konec	k1gInSc6	konec
otáčejí	otáčet	k5eAaImIp3nP	otáčet
do	do	k7c2	do
protisměru	protisměr	k1gInSc2	protisměr
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
jich	on	k3xPp3gMnPc2	on
větší	veliký	k2eAgFnSc1d2	veliký
počet	počet	k1gInSc1	počet
(	(	kIx(	(
<g/>
typicky	typicky	k6eAd1	typicky
u	u	k7c2	u
sedačkových	sedačkový	k2eAgFnPc2d1	sedačková
lanovek	lanovka	k1gFnPc2	lanovka
<g/>
)	)	kIx)	)
Kyvadlové	kyvadlový	k2eAgNnSc1d1	kyvadlové
–	–	k?	–
vozidla	vozidlo	k1gNnPc1	vozidlo
se	se	k3xPyFc4	se
neotáčejí	otáčet	k5eNaImIp3nP	otáčet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vracejí	vracet	k5eAaImIp3nP	vracet
se	se	k3xPyFc4	se
stejně	stejně	k6eAd1	stejně
orientovaná	orientovaný	k2eAgFnSc1d1	orientovaná
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
dráze	dráha	k1gFnSc6	dráha
dvě	dva	k4xCgNnPc4	dva
vozidla	vozidlo	k1gNnPc4	vozidlo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
se	se	k3xPyFc4	se
míjejí	míjet	k5eAaImIp3nP	míjet
uprostřed	uprostřed	k7c2	uprostřed
délky	délka	k1gFnSc2	délka
dráhy	dráha	k1gFnSc2	dráha
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
i	i	k9	i
lanovky	lanovka	k1gFnPc4	lanovka
jednovozidlové	jednovozidlový	k2eAgFnPc4d1	jednovozidlový
(	(	kIx(	(
<g/>
obdobné	obdobný	k2eAgFnPc4d1	obdobná
výtahu	výtah	k1gInSc2	výtah
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
způsobu	způsob	k1gInSc2	způsob
připojení	připojení	k1gNnSc2	připojení
vozů	vůz	k1gInPc2	vůz
na	na	k7c4	na
lano	lano	k1gNnSc4	lano
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
oběžné	oběžný	k2eAgFnPc1d1	oběžná
lanovky	lanovka	k1gFnPc1	lanovka
rozdělují	rozdělovat	k5eAaImIp3nP	rozdělovat
na	na	k7c4	na
Neodpojitelné	odpojitelný	k2eNgMnPc4d1	odpojitelný
-	-	kIx~	-
vozidla	vozidlo	k1gNnPc1	vozidlo
jsou	být	k5eAaImIp3nP	být
pevně	pevně	k6eAd1	pevně
uchycena	uchytit	k5eAaPmNgFnS	uchytit
na	na	k7c4	na
dopravní	dopravní	k2eAgNnSc4d1	dopravní
lano	lano	k1gNnSc4	lano
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stanicích	stanice	k1gFnPc6	stanice
se	se	k3xPyFc4	se
neodpojují	odpojovat	k5eNaImIp3nP	odpojovat
<g/>
.	.	kIx.	.
</s>
<s>
Odpojitelné	odpojitelný	k2eAgNnSc1d1	odpojitelné
-	-	kIx~	-
vozidla	vozidlo	k1gNnPc1	vozidlo
jsou	být	k5eAaImIp3nP	být
odpojitelně	odpojitelně	k6eAd1	odpojitelně
uchycena	uchytit	k5eAaPmNgNnP	uchytit
na	na	k7c4	na
dopravní	dopravní	k2eAgInPc4d1	dopravní
nebo	nebo	k8xC	nebo
tažné	tažný	k2eAgNnSc4d1	tažné
lano	lano	k1gNnSc4	lano
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stanicích	stanice	k1gFnPc6	stanice
se	se	k3xPyFc4	se
od	od	k7c2	od
lana	lano	k1gNnSc2	lano
odpojují	odpojovat	k5eAaImIp3nP	odpojovat
a	a	k8xC	a
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
se	se	k3xPyFc4	se
nižší	nízký	k2eAgFnSc7d2	nižší
(	(	kIx(	(
<g/>
staniční	staniční	k2eAgFnSc7d1	staniční
<g/>
)	)	kIx)	)
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
je	být	k5eAaImIp3nS	být
zajištěno	zajištěn	k2eAgNnSc1d1	zajištěno
komfortnější	komfortní	k2eAgNnSc1d2	komfortnější
a	a	k8xC	a
bezpečnější	bezpečný	k2eAgNnSc1d2	bezpečnější
nastupování	nastupování	k1gNnSc1	nastupování
a	a	k8xC	a
vystupování	vystupování	k1gNnSc1	vystupování
cestujících	cestující	k1gMnPc2	cestující
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
účelu	účel	k1gInSc2	účel
lze	lze	k6eAd1	lze
lanovky	lanovka	k1gFnPc1	lanovka
rozlišit	rozlišit	k5eAaPmF	rozlišit
na	na	k7c4	na
Osobní	osobní	k2eAgInPc4d1	osobní
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
turistickou	turistický	k2eAgFnSc7d1	turistická
<g/>
)	)	kIx)	)
Nákladní	nákladní	k2eAgFnSc7d1	nákladní
(	(	kIx(	(
<g/>
průmyslovou	průmyslový	k2eAgFnSc7d1	průmyslová
<g/>
,	,	kIx,	,
obslužnou	obslužný	k2eAgFnSc7d1	obslužná
<g/>
)	)	kIx)	)
Mezi	mezi	k7c4	mezi
největší	veliký	k2eAgMnPc4d3	veliký
výrobce	výrobce	k1gMnPc4	výrobce
lanovek	lanovka	k1gFnPc2	lanovka
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Doppelmayr	Doppelmayr	k1gMnSc1	Doppelmayr
Leitner	Leitner	k1gMnSc1	Leitner
Poma	Poma	k1gMnSc1	Poma
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
výtahu	výtah	k1gInSc2	výtah
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
kabina	kabina	k1gFnSc1	kabina
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
svisle	svisle	k6eAd1	svisle
<g/>
,	,	kIx,	,
u	u	k7c2	u
lanové	lanový	k2eAgFnSc2d1	lanová
dráhy	dráha	k1gFnSc2	dráha
obvykle	obvykle	k6eAd1	obvykle
stoupání	stoupání	k1gNnSc1	stoupání
nebývá	bývat	k5eNaImIp3nS	bývat
větší	veliký	k2eAgFnSc4d2	veliký
než	než	k8xS	než
45	[number]	k4	45
stupňů	stupeň	k1gInPc2	stupeň
(	(	kIx(	(
<g/>
1	[number]	k4	1
000	[number]	k4	000
promile	promile	k1gNnPc2	promile
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
zařízení	zařízení	k1gNnPc1	zařízení
však	však	k9	však
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
pomezí	pomezí	k1gNnSc6	pomezí
mezi	mezi	k7c7	mezi
lanovkou	lanovka	k1gFnSc7	lanovka
a	a	k8xC	a
výtahem	výtah	k1gInSc7	výtah
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
lanová	lanový	k2eAgFnSc1d1	lanová
dráha	dráha	k1gFnSc1	dráha
spojující	spojující	k2eAgFnPc4d1	spojující
dvě	dva	k4xCgFnPc4	dva
budovy	budova	k1gFnPc4	budova
pražského	pražský	k2eAgInSc2d1	pražský
hotelu	hotel	k1gInSc2	hotel
NH	NH	kA	NH
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
Mövenpick	Mövenpick	k1gInSc1	Mövenpick
<g/>
)	)	kIx)	)
na	na	k7c6	na
Smíchově	Smíchov	k1gInSc6	Smíchov
či	či	k8xC	či
šikmé	šikmý	k2eAgInPc4d1	šikmý
výtahy	výtah	k1gInPc4	výtah
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
lanové	lanový	k2eAgFnPc4d1	lanová
dráhy	dráha	k1gFnPc4	dráha
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
nepočítá	počítat	k5eNaImIp3nS	počítat
lyžařský	lyžařský	k2eAgInSc4d1	lyžařský
vlek	vlek	k1gInSc4	vlek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
podobný	podobný	k2eAgInSc1d1	podobný
sedačkové	sedačkový	k2eAgFnSc3d1	sedačková
lanovce	lanovka	k1gFnSc3	lanovka
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
místo	místo	k1gNnSc1	místo
závěsů	závěs	k1gInPc2	závěs
se	s	k7c7	s
sedačkami	sedačka	k1gFnPc7	sedačka
má	mít	k5eAaImIp3nS	mít
teleskopické	teleskopický	k2eAgFnPc4d1	teleskopická
tyče	tyč	k1gFnPc4	tyč
nebo	nebo	k8xC	nebo
snímatelné	snímatelný	k2eAgFnPc4d1	snímatelná
kotvy	kotva	k1gFnPc4	kotva
pro	pro	k7c4	pro
vlečení	vlečení	k1gNnSc4	vlečení
lyžařů	lyžař	k1gMnPc2	lyžař
a	a	k8xC	a
snowboardistů	snowboardista	k1gMnPc2	snowboardista
po	po	k7c6	po
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jiným	jiný	k2eAgInSc7d1	jiný
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
umožnit	umožnit	k5eAaPmF	umožnit
kolejovému	kolejový	k2eAgNnSc3d1	kolejové
vozidlu	vozidlo	k1gNnSc3	vozidlo
jízdu	jízda	k1gFnSc4	jízda
po	po	k7c6	po
strmější	strmý	k2eAgFnSc6d2	strmější
dráze	dráha	k1gFnSc6	dráha
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ozubnicová	ozubnicový	k2eAgFnSc1d1	ozubnicová
dráha	dráha	k1gFnSc1	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Visuté	visutý	k2eAgFnPc1d1	visutá
lanovky	lanovka	k1gFnPc1	lanovka
jsou	být	k5eAaImIp3nP	být
oproti	oproti	k7c3	oproti
jiným	jiný	k2eAgInPc3d1	jiný
druhům	druh	k1gInPc3	druh
dopravy	doprava	k1gFnSc2	doprava
šetrnější	šetrný	k2eAgFnSc4d2	šetrnější
k	k	k7c3	k
životnímu	životní	k2eAgNnSc3d1	životní
prostředí	prostředí	k1gNnSc3	prostředí
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jejich	jejich	k3xOp3gFnSc7	jejich
výstavbou	výstavba	k1gFnSc7	výstavba
jsou	být	k5eAaImIp3nP	být
dotčena	dotčen	k2eAgNnPc1d1	dotčeno
jen	jen	k8xS	jen
místa	místo	k1gNnPc1	místo
stanic	stanice	k1gFnPc2	stanice
a	a	k8xC	a
stožárů	stožár	k1gInPc2	stožár
<g/>
.	.	kIx.	.
</s>
<s>
Budování	budování	k1gNnSc1	budování
lanovek	lanovka	k1gFnPc2	lanovka
v	v	k7c6	v
chráněných	chráněný	k2eAgFnPc6d1	chráněná
přírodních	přírodní	k2eAgFnPc6d1	přírodní
oblastech	oblast	k1gFnPc6	oblast
však	však	k9	však
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
své	svůj	k3xOyFgMnPc4	svůj
odpůrce	odpůrce	k1gMnPc4	odpůrce
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
počet	počet	k1gInSc1	počet
návštěvníků	návštěvník	k1gMnPc2	návštěvník
dosud	dosud	k6eAd1	dosud
málo	málo	k4c4	málo
navštěvovaných	navštěvovaný	k2eAgNnPc2d1	navštěvované
míst	místo	k1gNnPc2	místo
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
mnohanásobně	mnohanásobně	k6eAd1	mnohanásobně
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
druhotnou	druhotný	k2eAgFnSc4d1	druhotná
zátěž	zátěž	k1gFnSc4	zátěž
pro	pro	k7c4	pro
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Pozemní	pozemní	k2eAgFnSc1d1	pozemní
lanová	lanový	k2eAgFnSc1d1	lanová
dráha	dráha	k1gFnSc1	dráha
Visutá	visutý	k2eAgFnSc1d1	visutá
lanová	lanový	k2eAgFnSc1d1	lanová
dráha	dráha	k1gFnSc1	dráha
Sedačková	sedačkový	k2eAgFnSc1d1	sedačková
lanová	lanový	k2eAgFnSc1d1	lanová
dráha	dráha	k1gFnSc1	dráha
Seznam	seznam	k1gInSc1	seznam
lanových	lanový	k2eAgFnPc2d1	lanová
drah	draha	k1gFnPc2	draha
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
Lyžařský	lyžařský	k2eAgInSc1d1	lyžařský
vlek	vlek	k1gInSc1	vlek
Výtah	výtah	k1gInSc4	výtah
Lyžařské	lyžařský	k2eAgNnSc4d1	lyžařské
středisko	středisko	k1gNnSc4	středisko
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
lanová	lanový	k2eAgFnSc1d1	lanová
dráha	dráha	k1gFnSc1	dráha
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Lokální	lokální	k2eAgFnSc1d1	lokální
šablona	šablona	k1gFnSc1	šablona
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
kategorii	kategorie	k1gFnSc4	kategorie
Commons	Commonsa	k1gFnPc2	Commonsa
než	než	k8xS	než
přiřazená	přiřazený	k2eAgFnSc1d1	přiřazená
položka	položka	k1gFnSc1	položka
Wikidat	Wikidat	k1gFnSc1	Wikidat
<g/>
:	:	kIx,	:
Lokální	lokální	k2eAgInSc1d1	lokální
odkaz	odkaz	k1gInSc1	odkaz
<g/>
:	:	kIx,	:
Cable	Cable	k1gFnSc1	Cable
transport	transport	k1gInSc1	transport
Wikidata	Wikidata	k1gFnSc1	Wikidata
<g/>
:	:	kIx,	:
Cableways	Cablewaysa	k1gFnPc2	Cablewaysa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
lanovka	lanovka	k1gFnSc1	lanovka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
10	[number]	k4	10
nej	nej	k?	nej
<g/>
...	...	k?	...
Lanové	lanový	k2eAgFnSc2d1	lanová
dráhy	dráha	k1gFnSc2	dráha
(	(	kIx(	(
<g/>
Marek	Marek	k1gMnSc1	Marek
Zouzalík	Zouzalík	k1gMnSc1	Zouzalík
<g/>
,	,	kIx,	,
21.1	[number]	k4	21.1
<g/>
.2005	.2005	k4	.2005
<g/>
)	)	kIx)	)
Stránky	stránka	k1gFnPc1	stránka
o	o	k7c6	o
všech	všecek	k3xTgFnPc6	všecek
lanových	lanový	k2eAgFnPc6d1	lanová
drahách	draha	k1gFnPc6	draha
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
Nej	Nej	k1gFnSc2	Nej
lanovky	lanovka	k1gFnSc2	lanovka
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
Roman	Roman	k1gMnSc1	Roman
Gric	Gric	k1gFnSc1	Gric
<g/>
,	,	kIx,	,
8.4	[number]	k4	8.4
<g/>
.2007	.2007	k4	.2007
<g/>
)	)	kIx)	)
</s>
