<s>
Lanová	lanový	k2eAgFnSc1d1	lanová
dráha	dráha	k1gFnSc1	dráha
(	(	kIx(	(
<g/>
lanovka	lanovka	k1gFnSc1	lanovka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
dráha	dráha	k1gFnSc1	dráha
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc4	jejíž
vozidla	vozidlo	k1gNnPc1	vozidlo
se	se	k3xPyFc4	se
pohybují	pohybovat	k5eAaImIp3nP	pohybovat
po	po	k7c6	po
šikmé	šikmý	k2eAgFnSc6d1	šikmá
nebo	nebo	k8xC	nebo
vodorovné	vodorovný	k2eAgFnSc6d1	vodorovná
trase	trasa	k1gFnSc6	trasa
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
poháněna	poháněn	k2eAgNnPc1d1	poháněno
pomocí	pomocí	k7c2	pomocí
tažných	tažný	k2eAgNnPc2d1	tažné
lan	lano	k1gNnPc2	lano
<g/>
.	.	kIx.	.
</s>
