<s>
Národní	národní	k2eAgFnSc1d1
bezpečnostní	bezpečnostní	k2eAgFnSc1d1
agentura	agentura	k1gFnSc1
(	(	kIx(
<g/>
The	The	k1gFnSc1
National	National	k1gFnSc2
Security	Securita	k1gFnSc2
Agency	Agenca	k1gFnSc2
<g/>
,	,	kIx,
zkratka	zkratka	k1gFnSc1
NSA	NSA	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vládní	vládní	k2eAgFnSc1d1
kryptologická	kryptologický	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
,	,	kIx,
spadající	spadající	k2eAgInSc1d1
pod	pod	k7c4
Ministerstvo	ministerstvo	k1gNnSc4
obrany	obrana	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
oficiálně	oficiálně	k6eAd1
vznikla	vzniknout	k5eAaPmAgFnS
4	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1952	#num#	k4
<g/>
.	.	kIx.
</s>