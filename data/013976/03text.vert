<s>
Národní	národní	k2eAgFnSc1d1
bezpečnostní	bezpečnostní	k2eAgFnSc1d1
agentura	agentura	k1gFnSc1
</s>
<s>
Na	na	k7c4
tento	tento	k3xDgInSc4
článek	článek	k1gInSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
NSA	NSA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
americké	americký	k2eAgFnSc6d1
kontrarozvědce	kontrarozvědka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
NSA	NSA	kA
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Národní	národní	k2eAgFnSc1d1
bezpečnostní	bezpečnostní	k2eAgFnSc1d1
agentura	agentura	k1gFnSc1
Pečeť	pečeť	k1gFnSc1
Národní	národní	k2eAgFnSc2d1
bezpečnostní	bezpečnostní	k2eAgFnSc2d1
agentury	agentura	k1gFnSc2
Sídlo	sídlo	k1gNnSc1
NSA	NSA	kA
ve	v	k7c6
Fort	Fort	k?
Meade	Mead	k1gInSc5
v	v	k7c6
Marylandu	Maryland	k1gInSc6
Zkratka	zkratka	k1gFnSc1
</s>
<s>
NSA	NSA	kA
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Armed	Armed	k1gInSc1
Forces	Forcesa	k1gFnPc2
Security	Securita	k1gFnSc2
Agency	Agenca	k1gFnSc2
Vznik	vznik	k1gInSc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1952	#num#	k4
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Fort	Fort	k?
George	George	k1gNnSc1
G.	G.	kA
Meade	Mead	k1gInSc5
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgFnSc2d1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
39	#num#	k4
<g/>
°	°	k?
<g/>
6	#num#	k4
<g/>
′	′	k?
<g/>
32,4	32,4	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
76	#num#	k4
<g/>
°	°	k?
<g/>
46	#num#	k4
<g/>
′	′	k?
<g/>
12	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
ředitel	ředitel	k1gMnSc1
NSA	NSA	kA
</s>
<s>
Paul	Paul	k1gMnSc1
M.	M.	kA
Nakasone	Nakason	k1gInSc5
Mateřská	mateřský	k2eAgFnSc1d1
organizace	organizace	k1gFnSc2
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1
obrany	obrana	k1gFnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
Rozpočet	rozpočet	k1gInSc4
</s>
<s>
tajné	tajný	k2eAgFnPc1d1
Oficiální	oficiální	k2eAgFnPc1d1
web	web	k1gInSc1
</s>
<s>
www.nsa.gov	www.nsa.gov	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Národní	národní	k2eAgFnSc1d1
bezpečnostní	bezpečnostní	k2eAgFnSc1d1
agentura	agentura	k1gFnSc1
(	(	kIx(
<g/>
The	The	k1gFnSc1
National	National	k1gFnSc2
Security	Securita	k1gFnSc2
Agency	Agenca	k1gFnSc2
<g/>
,	,	kIx,
zkratka	zkratka	k1gFnSc1
NSA	NSA	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vládní	vládní	k2eAgFnSc1d1
kryptologická	kryptologický	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
,	,	kIx,
spadající	spadající	k2eAgInSc1d1
pod	pod	k7c4
Ministerstvo	ministerstvo	k1gNnSc4
obrany	obrana	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
oficiálně	oficiálně	k6eAd1
vznikla	vzniknout	k5eAaPmAgFnS
4	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1952	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
odpovědná	odpovědný	k2eAgFnSc1d1
za	za	k7c4
sběr	sběr	k1gInSc4
a	a	k8xC
analýzu	analýza	k1gFnSc4
zahraniční	zahraniční	k2eAgFnSc2d1
komunikace	komunikace	k1gFnSc2
<g/>
,	,	kIx,
koordinuje	koordinovat	k5eAaBmIp3nS
<g/>
,	,	kIx,
řídí	řídit	k5eAaImIp3nS
a	a	k8xC
provádí	provádět	k5eAaImIp3nS
vysoce	vysoce	k6eAd1
specializované	specializovaný	k2eAgFnPc4d1
činnosti	činnost	k1gFnPc4
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc7
účelem	účel	k1gInSc7
je	být	k5eAaImIp3nS
získávání	získávání	k1gNnSc1
zpráv	zpráva	k1gFnPc2
zahraničních	zahraniční	k2eAgFnPc2d1
rozvědek	rozvědka	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
taktéž	taktéž	k?
odpovědná	odpovědný	k2eAgFnSc1d1
za	za	k7c4
ochranu	ochrana	k1gFnSc4
informačních	informační	k2eAgInPc2d1
systémů	systém	k1gInPc2
uvnitř	uvnitř	k7c2
vlády	vláda	k1gFnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
a	a	k8xC
její	její	k3xOp3gFnSc2
komunikace	komunikace	k1gFnSc2
s	s	k7c7
jinými	jiný	k2eAgFnPc7d1
agenturami	agentura	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výše	výše	k1gFnSc1
zmíněné	zmíněný	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
vyžadují	vyžadovat	k5eAaImIp3nP
značné	značný	k2eAgInPc4d1
prostředky	prostředek	k1gInPc4
pro	pro	k7c4
kryptoanalýzu	kryptoanalýza	k1gFnSc4
a	a	k8xC
kryptografii	kryptografie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Speciální	speciální	k2eAgFnSc7d1
částí	část	k1gFnSc7
NSA	NSA	kA
je	být	k5eAaImIp3nS
tzv.	tzv.	kA
U.	U.	kA
S.	S.	kA
Intelligence	Intelligence	k1gFnSc1
Community	Communita	k1gFnSc2
vedená	vedený	k2eAgFnSc1d1
ředitelem	ředitel	k1gMnSc7
národní	národní	k2eAgFnSc2d1
rozvědky	rozvědka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
května	květen	k1gInSc2
2018	#num#	k4
agenturu	agentura	k1gFnSc4
vede	vést	k5eAaImIp3nS
generál	generál	k1gMnSc1
Paul	Paul	k1gMnSc1
M.	M.	kA
Nakasone	Nakason	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Agentura	agentura	k1gFnSc1
má	mít	k5eAaImIp3nS
sídlo	sídlo	k1gNnSc4
ve	v	k7c6
Fort	Fort	k?
Meade	Mead	k1gInSc5
ve	v	k7c6
státě	stát	k1gInSc6
Maryland	Maryland	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1949	#num#	k4
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
Armed	Armed	k1gInSc1
Forces	Forces	k1gInSc4
Security	Securita	k1gFnSc2
Agency	Agenca	k1gFnSc2
(	(	kIx(
<g/>
AFSA	AFSA	kA
<g/>
)	)	kIx)
čili	čili	k8xC
předchůdkyně	předchůdkyně	k1gFnSc2
dnešní	dnešní	k2eAgFnSc2d1
NSA	NSA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
organizace	organizace	k1gFnPc4
byla	být	k5eAaImAgFnS
původně	původně	k6eAd1
zavedena	zavést	k5eAaPmNgFnS
v	v	k7c6
rámci	rámec	k1gInSc6
Ministerstva	ministerstvo	k1gNnSc2
obrany	obrana	k1gFnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Sboru	sbor	k1gInSc2
náčelníků	náčelník	k1gMnPc2
štábů	štáb	k1gInPc2
(	(	kIx(
<g/>
Joint	Joint	k1gInSc1
Chiefs	Chiefs	k1gInSc1
of	of	k?
Staff	Staff	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úkolem	úkol	k1gInSc7
AFSA	AFSA	kA
bylo	být	k5eAaImAgNnS
řízení	řízení	k1gNnSc1
komunikace	komunikace	k1gFnSc2
a	a	k8xC
výzvědných	výzvědný	k2eAgFnPc2d1
elektronických	elektronický	k2eAgFnPc2d1
aktivit	aktivita	k1gFnPc2
Vojenské	vojenský	k2eAgFnSc2d1
zpravodajské	zpravodajský	k2eAgFnSc2d1
služby	služba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
agentura	agentura	k1gFnSc1
ovšem	ovšem	k9
měla	mít	k5eAaImAgFnS
malou	malý	k2eAgFnSc4d1
moc	moc	k1gFnSc4
a	a	k8xC
nedostatek	nedostatek	k1gInSc4
centralizované	centralizovaný	k2eAgFnSc2d1
koordinace	koordinace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vytvoření	vytvoření	k1gNnSc1
NSA	NSA	kA
pramenilo	pramenit	k5eAaImAgNnS
ze	z	k7c2
zprávy	zpráva	k1gFnSc2
CIA	CIA	kA
dne	den	k1gInSc2
10	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1951	#num#	k4
odeslané	odeslaný	k2eAgFnSc2d1
ředitelem	ředitel	k1gMnSc7
Walterem	Walter	k1gMnSc7
Bedellem	Bedell	k1gMnSc7
Smithem	Smith	k1gInSc7
vedoucímu	vedoucí	k1gMnSc3
tajemníkovi	tajemník	k1gMnSc3
Jamesi	Jamese	k1gFnSc4
S.	S.	kA
Layovi	Laya	k1gMnSc3
na	na	k7c4
National	National	k1gFnSc4
Security	Securita	k1gFnSc2
Council	Council	k1gInSc1
(	(	kIx(
<g/>
NSC	NSC	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpráva	zpráva	k1gFnSc1
říkala	říkat	k5eAaImAgFnS
<g/>
,	,	kIx,
že	že	k8xS
kontrola	kontrola	k1gFnSc1
a	a	k8xC
koordinace	koordinace	k1gFnSc1
souboru	soubor	k1gInSc2
a	a	k8xC
zpracovávání	zpracovávání	k1gNnSc1
informací	informace	k1gFnPc2
komunikačními	komunikační	k2eAgFnPc7d1
výzvědnými	výzvědný	k2eAgFnPc7d1
službami	služba	k1gFnPc7
se	se	k3xPyFc4
ukázala	ukázat	k5eAaPmAgFnS
neúčinnou	účinný	k2eNgFnSc7d1
<g/>
,	,	kIx,
a	a	k8xC
doporučila	doporučit	k5eAaPmAgFnS
průzkum	průzkum	k1gInSc4
komunikačních	komunikační	k2eAgFnPc2d1
výzvědných	výzvědný	k2eAgFnPc2d1
aktivit	aktivita	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Návrh	návrh	k1gInSc1
byl	být	k5eAaImAgInS
přijat	přijat	k2eAgInSc1d1
dne	den	k1gInSc2
13	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1951	#num#	k4
a	a	k8xC
studie	studie	k1gFnSc1
byla	být	k5eAaImAgFnS
povolena	povolit	k5eAaPmNgFnS
28	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1951	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zpráva	zpráva	k1gFnSc1
(	(	kIx(
<g/>
pojmenovaná	pojmenovaný	k2eAgFnSc1d1
po	po	k7c6
předsedovi	předseda	k1gMnSc3
výboru	výbor	k1gInSc2
Herbertu	Herbert	k1gMnSc3
Brownellovi	Brownell	k1gMnSc3
a	a	k8xC
obecně	obecně	k6eAd1
známá	známý	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
„	„	k?
<g/>
Brownell	Brownell	k1gInSc1
Committee	Committee	k1gInSc1
Report	report	k1gInSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
dokončena	dokončen	k2eAgFnSc1d1
13	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1952	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zmapovala	zmapovat	k5eAaPmAgFnS
historii	historie	k1gFnSc4
komunikace	komunikace	k1gFnSc2
(	(	kIx(
<g/>
hlášení	hlášení	k1gNnSc2
<g/>
)	)	kIx)
tajných	tajný	k2eAgFnPc2d1
služeb	služba	k1gFnPc2
v	v	k7c6
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s>
Pravomoc	pravomoc	k1gFnSc1
</s>
<s>
NSA	NSA	kA
má	mít	k5eAaImIp3nS
některé	některý	k3yIgFnPc4
speciální	speciální	k2eAgFnPc4d1
pravomoci	pravomoc	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
nemá	mít	k5eNaImIp3nS
žádná	žádný	k3yNgFnSc1
z	z	k7c2
bezpečnostních	bezpečnostní	k2eAgFnPc2d1
složek	složka	k1gFnPc2
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zejména	zejména	k6eAd1
pak	pak	k6eAd1
odposlouchávat	odposlouchávat	k5eAaImF
telefonní	telefonní	k2eAgInPc4d1
hovory	hovor	k1gInPc4
<g/>
,	,	kIx,
číst	číst	k5eAaImF
e-maily	e-mail	k1gInPc4
a	a	k8xC
další	další	k2eAgFnPc4d1
bez	bez	k7c2
povolení	povolení	k1gNnSc2
soudu	soud	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Dále	daleko	k6eAd2
má	mít	k5eAaImIp3nS
NSA	NSA	kA
speciální	speciální	k2eAgInPc4d1
systémy	systém	k1gInPc4
jako	jako	k9
„	„	k?
<g/>
Velké	velký	k2eAgNnSc1d1
ucho	ucho	k1gNnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
poslouchá	poslouchat	k5eAaImIp3nS
na	na	k7c6
dnech	den	k1gInPc6
oceánu	oceán	k1gInSc2
a	a	k8xC
informuje	informovat	k5eAaBmIp3nS
tak	tak	k6eAd1
USA	USA	kA
o	o	k7c6
pohybu	pohyb	k1gInSc6
lodí	loď	k1gFnPc2
a	a	k8xC
ponorek	ponorka	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
další	další	k2eAgInPc1d1
systémy	systém	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
jsou	být	k5eAaImIp3nP
přísně	přísně	k6eAd1
utajeny	utajit	k5eAaPmNgFnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
NSA	NSA	kA
má	mít	k5eAaImIp3nS
svoji	svůj	k3xOyFgFnSc4
databanku	databanka	k1gFnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
jsou	být	k5eAaImIp3nP
uchovány	uchován	k2eAgFnPc1d1
všechny	všechen	k3xTgFnPc1
důležité	důležitý	k2eAgFnPc1d1
a	a	k8xC
tajné	tajný	k2eAgFnPc1d1
informace	informace	k1gFnPc1
(	(	kIx(
<g/>
deklarace	deklarace	k1gFnSc1
<g/>
,	,	kIx,
smlouvy	smlouva	k1gFnPc1
<g/>
,	,	kIx,
odpalovací	odpalovací	k2eAgInPc1d1
kódy	kód	k1gInPc1
<g/>
,	,	kIx,
plány	plán	k1gInPc1
ponorek	ponorka	k1gFnPc2
a	a	k8xC
zbraní	zbraň	k1gFnPc2
<g/>
,	,	kIx,
informace	informace	k1gFnPc1
o	o	k7c6
misích	mise	k1gFnPc6
USA	USA	kA
<g/>
…	…	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
NSA	NSA	kA
hraje	hrát	k5eAaImIp3nS
významnou	významný	k2eAgFnSc4d1
roli	role	k1gFnSc4
při	při	k7c6
potírání	potírání	k1gNnSc6
terorismu	terorismus	k1gInSc2
v	v	k7c6
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spekuluje	spekulovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
zabránila	zabránit	k5eAaPmAgFnS
již	již	k6eAd1
12	#num#	k4
teroristickým	teroristický	k2eAgInPc3d1
útokům	útok	k1gInPc3
<g/>
,	,	kIx,
ale	ale	k8xC
tahle	tenhle	k3xDgNnPc1
čísla	číslo	k1gNnPc1
nejsou	být	k5eNaImIp3nP
ověřena	ověřit	k5eAaPmNgNnP
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
(	(	kIx(
<g/>
NSA	NSA	kA
je	být	k5eAaImIp3nS
utajovaná	utajovaný	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
,	,	kIx,
takže	takže	k8xS
o	o	k7c6
svých	svůj	k3xOyFgFnPc6
aktivitách	aktivita	k1gFnPc6
mlčí	mlčet	k5eAaImIp3nP
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Aktivity	aktivita	k1gFnPc1
</s>
<s>
Globální	globální	k2eAgInSc1d1
sledovací	sledovací	k2eAgInSc1d1
program	program	k1gInSc1
PRISM	PRISM	kA
</s>
<s>
NSA	NSA	kA
je	být	k5eAaImIp3nS
nechvalně	chvalně	k6eNd1
proslulá	proslulý	k2eAgFnSc1d1
různými	různý	k2eAgInPc7d1
skandály	skandál	k1gInPc7
ohledně	ohledně	k7c2
mimosoudního	mimosoudní	k2eAgNnSc2d1
odposlouchávání	odposlouchávání	k1gNnSc2
a	a	k8xC
shromažďování	shromažďování	k1gNnSc6
dat	datum	k1gNnPc2
o	o	k7c6
občanech	občan	k1gMnPc6
USA	USA	kA
<g/>
,	,	kIx,
jakož	jakož	k8xC
i	i	k9
cizích	cizí	k2eAgInPc2d1
státních	státní	k2eAgInPc2d1
příslušnících	příslušník	k1gMnPc6
<g/>
.	.	kIx.
</s>
<s>
ECHELON	ECHELON	kA
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
Echelon	Echelon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
NSA	NSA	kA
provozuje	provozovat	k5eAaImIp3nS
tento	tento	k3xDgInSc4
projekt	projekt	k1gInSc4
společně	společně	k6eAd1
s	s	k7c7
tajnými	tajný	k2eAgFnPc7d1
službami	služba	k1gFnPc7
dalších	další	k2eAgInPc2d1
čtyř	čtyři	k4xCgInPc2
anglofonních	anglofonní	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
systém	systém	k1gInSc4
prostředků	prostředek	k1gInPc2
určených	určený	k2eAgInPc2d1
k	k	k7c3
zachycování	zachycování	k1gNnSc3
a	a	k8xC
zpracování	zpracování	k1gNnSc3
komunikace	komunikace	k1gFnSc2
vedené	vedený	k2eAgFnSc2d1
přes	přes	k7c4
komunikační	komunikační	k2eAgInPc4d1
satelity	satelit	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Šifrování	šifrování	k1gNnSc1
</s>
<s>
Jako	jako	k8xS,k8xC
certifikační	certifikační	k2eAgFnSc1d1
autorita	autorita	k1gFnSc1
na	na	k7c6
internetu	internet	k1gInSc6
jsou	být	k5eAaImIp3nP
dominantní	dominantní	k2eAgInSc4d1
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc2
společnosti	společnost	k1gFnSc2
mají	mít	k5eAaImIp3nP
podíl	podíl	k1gInSc4
na	na	k7c6
trhu	trh	k1gInSc6
přes	přes	k7c4
90	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
NSA	NSA	kA
tak	tak	k6eAd1
může	moct	k5eAaImIp3nS
odposlouchávat	odposlouchávat	k5eAaImF
většinu	většina	k1gFnSc4
komunikace	komunikace	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
NSA	NSA	kA
také	také	k9
uplácí	uplácet	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
šifrování	šifrování	k1gNnSc4
byla	být	k5eAaImAgFnS
úmyslně	úmyslně	k6eAd1
oslabena	oslabit	k5eAaPmNgFnS
a	a	k8xC
například	například	k6eAd1
instalována	instalován	k2eAgNnPc4d1
zadní	zadní	k2eAgNnPc4d1
vrátka	vrátka	k1gNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Databáze	databáze	k1gFnPc1
telefonních	telefonní	k2eAgInPc2d1
hovorů	hovor	k1gInPc2
</s>
<s>
Britský	britský	k2eAgInSc1d1
deník	deník	k1gInSc1
Guardian	Guardiana	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
zveřejnil	zveřejnit	k5eAaPmAgInS
tajný	tajný	k2eAgInSc1d1
soudní	soudní	k2eAgInSc1d1
příkaz	příkaz	k1gInSc1
<g/>
,	,	kIx,
kterým	který	k3yIgInSc7,k3yQgInSc7,k3yRgInSc7
Národní	národní	k2eAgFnSc1d1
bezpečnostní	bezpečnostní	k2eAgFnSc1d1
agentura	agentura	k1gFnSc1
(	(	kIx(
<g/>
NSA	NSA	kA
<g/>
)	)	kIx)
získala	získat	k5eAaPmAgFnS
povolení	povolení	k1gNnSc4
k	k	k7c3
přístupu	přístup	k1gInSc3
k	k	k7c3
datům	datum	k1gNnPc3
telekomunikační	telekomunikační	k2eAgFnSc3d1
společnosti	společnost	k1gFnSc3
Verizon	Verizona	k1gFnPc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnPc4
služby	služba	k1gFnPc4
využívá	využívat	k5eAaPmIp3nS,k5eAaImIp3nS
kolem	kolem	k7c2
100	#num#	k4
milionů	milion	k4xCgInPc2
klientů	klient	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Databáze	databáze	k1gFnSc1
zahrnovala	zahrnovat	k5eAaImAgFnS
vnitrostátní	vnitrostátní	k2eAgInPc4d1
i	i	k8xC
zahraniční	zahraniční	k2eAgInPc4d1
hovory	hovor	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
NSA	NSA	kA
sice	sice	k8xC
od	od	k7c2
Verizonu	Verizon	k1gInSc2
nezískala	získat	k5eNaPmAgFnS
ani	ani	k8xC
obsah	obsah	k1gInSc4
hovorů	hovor	k1gInPc2
<g/>
,	,	kIx,
ani	ani	k8xC
jména	jméno	k1gNnSc2
volajících	volající	k2eAgFnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Denně	denně	k6eAd1
však	však	k9
shromažďovala	shromažďovat	k5eAaImAgFnS
informace	informace	k1gFnPc4
o	o	k7c6
kontaktech	kontakt	k1gInPc6
<g/>
,	,	kIx,
místech	místo	k1gNnPc6
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
oba	dva	k4xCgMnPc1
účastníci	účastník	k1gMnPc1
hovoru	hovor	k1gInSc2
telefonovali	telefonovat	k5eAaImAgMnP
<g/>
,	,	kIx,
délce	délka	k1gFnSc6
jejich	jejich	k3xOp3gFnSc2
konverzace	konverzace	k1gFnSc2
a	a	k8xC
celkovém	celkový	k2eAgInSc6d1
času	čas	k1gInSc6
hovorů	hovor	k1gInPc2
za	za	k7c4
poslední	poslední	k2eAgInPc4d1
tři	tři	k4xCgInPc4
měsíce	měsíc	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tyto	tento	k3xDgFnPc1
snahy	snaha	k1gFnPc1
jsou	být	k5eAaImIp3nP
samozřejmě	samozřejmě	k6eAd1
dlouhodobého	dlouhodobý	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
<g/>
,	,	kIx,
s	s	k7c7
odposloucháváním	odposlouchávání	k1gNnSc7
začala	začít	k5eAaPmAgFnS
už	už	k6eAd1
vláda	vláda	k1gFnSc1
prezidenta	prezident	k1gMnSc2
Bushe	Bush	k1gMnSc2
po	po	k7c4
11	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úniky	únik	k1gInPc1
proběhly	proběhnout	k5eAaPmAgFnP
už	už	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Telefonní	telefonní	k2eAgFnPc1d1
karty	karta	k1gFnPc1
</s>
<s>
NSA	NSA	kA
získala	získat	k5eAaPmAgFnS
klíče	klíč	k1gInPc1
největšího	veliký	k2eAgMnSc2d3
výrobce	výrobce	k1gMnSc2
SIM	SIM	kA
karet	kareta	k1gFnPc2
a	a	k8xC
mohla	moct	k5eAaImAgFnS
tak	tak	k6eAd1
nezávisle	závisle	k6eNd1
odposlouchávat	odposlouchávat	k5eAaImF
hovory	hovor	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Platební	platební	k2eAgFnPc1d1
karty	karta	k1gFnPc1
</s>
<s>
Američtí	americký	k2eAgMnPc1d1
vydavatelé	vydavatel	k1gMnPc1
platebních	platební	k2eAgFnPc2d1
karet	kareta	k1gFnPc2
předávají	předávat	k5eAaImIp3nP
informace	informace	k1gFnPc4
o	o	k7c6
platbách	platba	k1gFnPc6
NSA	NSA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Přičemž	přičemž	k6eAd1
dominantními	dominantní	k2eAgMnPc7d1
vydavateli	vydavatel	k1gMnPc7
platebních	platební	k2eAgFnPc2d1
karet	kareta	k1gFnPc2
jsou	být	k5eAaImIp3nP
USA	USA	kA
a	a	k8xC
Čína	Čína	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
PRISM	PRISM	kA
</s>
<s>
Projekt	projekt	k1gInSc1
PRISM	PRISM	kA
zahrnuje	zahrnovat	k5eAaImIp3nS
analýzu	analýza	k1gFnSc4
dat	datum	k1gNnPc2
poskytnutých	poskytnutý	k2eAgNnPc2d1
firmami	firma	k1gFnPc7
Facebook	Facebook	k1gInSc1
<g/>
,	,	kIx,
Google	Google	k1gInSc1
<g/>
,	,	kIx,
Microsoft	Microsoft	kA
<g/>
,	,	kIx,
Yahoo	Yahoo	k6eAd1
a	a	k8xC
pěti	pět	k4xCc7
dalšími	další	k2eAgFnPc7d1
americkými	americký	k2eAgFnPc7d1
internetovými	internetový	k2eAgFnPc7d1
firmami	firma	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Informaci	informace	k1gFnSc6
přinesly	přinést	k5eAaPmAgInP
v	v	k7c6
červnu	červen	k1gInSc6
2013	#num#	k4
The	The	k1gMnPc2
Washington	Washington	k1gInSc4
Post	posta	k1gFnPc2
a	a	k8xC
The	The	k1gFnSc4
Guardian	Guardiana	k1gFnPc2
na	na	k7c6
základě	základ	k1gInSc6
tipu	tip	k1gInSc2
od	od	k7c2
Edwarda	Edward	k1gMnSc2
Snowdena	Snowden	k1gMnSc2
<g/>
,	,	kIx,
zaměstnance	zaměstnanec	k1gMnSc2
bezpečnostní	bezpečnostní	k2eAgFnSc2d1
agentury	agentura	k1gFnSc2
spolupracující	spolupracující	k2eAgFnSc2d1
s	s	k7c7
NSA	NSA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
způsobilo	způsobit	k5eAaPmAgNnS
menší	malý	k2eAgInSc4d2
skandál	skandál	k1gInSc4
<g/>
,	,	kIx,
americká	americký	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
sledování	sledování	k1gNnSc2
přiznala	přiznat	k5eAaPmAgFnS
<g/>
,	,	kIx,
všechny	všechen	k3xTgFnPc1
dotčené	dotčený	k2eAgFnPc1d1
společnosti	společnost	k1gFnPc1
ho	on	k3xPp3gNnSc4
naopak	naopak	k6eAd1
popřely	popřít	k5eAaPmAgInP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Datové	datový	k2eAgNnSc1d1
centrum	centrum	k1gNnSc1
v	v	k7c6
Utahu	Utah	k1gInSc6
</s>
<s>
V	v	k7c4
Camp	camp	k1gInSc4
Williams	Williamsa	k1gFnPc2
nedaleko	nedaleko	k7c2
Salt	salto	k1gNnPc2
Lake	Lak	k1gMnSc2
City	City	k1gFnSc2
se	se	k3xPyFc4
pro	pro	k7c4
NSA	NSA	kA
staví	stavit	k5eAaPmIp3nS,k5eAaImIp3nS,k5eAaBmIp3nS
obří	obří	k2eAgNnSc4d1
datové	datový	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
dokončeno	dokončit	k5eAaPmNgNnS
v	v	k7c6
září	září	k1gNnSc6
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
datacentrum	datacentrum	k1gNnSc1
má	mít	k5eAaImIp3nS
disponovat	disponovat	k5eAaBmF
kapacitou	kapacita	k1gFnSc7
až	až	k9
1	#num#	k4
jottabajt	jottabajta	k1gFnPc2
(	(	kIx(
<g/>
kvadrilion	kvadrilion	k4xCgInSc1
bajtů	bajt	k1gInPc2
<g/>
,	,	kIx,
1024	#num#	k4
)	)	kIx)
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
parametry	parametr	k1gInPc1
zahrnují	zahrnovat	k5eAaImIp3nP
rozlohu	rozloha	k1gFnSc4
1	#num#	k4
<g/>
–	–	k?
<g/>
1,5	1,5	k4
milionu	milion	k4xCgInSc3
čtverečních	čtvereční	k2eAgFnPc2d1
stop	stopa	k1gFnPc2
<g/>
,	,	kIx,
cenu	cena	k1gFnSc4
1,5	1,5	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
miliardy	miliarda	k4xCgFnSc2
USD	USD	kA
(	(	kIx(
<g/>
podle	podle	k7c2
odhadů	odhad	k1gInPc2
další	další	k2eAgFnPc4d1
dvě	dva	k4xCgFnPc4
miliardy	miliarda	k4xCgFnPc4
dolarů	dolar	k1gInPc2
padnou	padnout	k5eAaImIp3nP,k5eAaPmIp3nP
na	na	k7c4
údržbu	údržba	k1gFnSc4
<g/>
,	,	kIx,
hardware	hardware	k1gInSc4
a	a	k8xC
software	software	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
spotřeba	spotřeba	k1gFnSc1
energie	energie	k1gFnSc2
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
65	#num#	k4
megawattů	megawatt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účel	účel	k1gInSc1
je	být	k5eAaImIp3nS
tajný	tajný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Nejkontroverznější	kontroverzní	k2eAgFnPc1d3
aktivity	aktivita	k1gFnPc1
NSA	NSA	kA
</s>
<s>
Ředitel	ředitel	k1gMnSc1
Národní	národní	k2eAgFnSc2d1
zpravodajské	zpravodajský	k2eAgFnSc2d1
služby	služba	k1gFnSc2
James	James	k1gMnSc1
Clapper	Clapper	k1gMnSc1
6	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2013	#num#	k4
lhal	lhát	k5eAaImAgInS
před	před	k7c7
Kongresem	kongres	k1gInSc7
<g/>
,	,	kIx,
když	když	k8xS
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
NSA	NSA	kA
neshromažďuje	shromažďovat	k5eNaImIp3nS
údaje	údaj	k1gInPc4
o	o	k7c4
milionech	milion	k4xCgInPc6
Američanů	Američan	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Aféry	aféra	k1gFnPc1
sledovacích	sledovací	k2eAgFnPc2d1
aktivit	aktivita	k1gFnPc2
tajné	tajný	k2eAgFnSc2d1
služby	služba	k1gFnSc2
NSA	NSA	kA
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
denně	denně	k6eAd1
sbírá	sbírat	k5eAaImIp3nS
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
a	a	k8xC
uchovává	uchovávat	k5eAaImIp3nS
okolo	okolo	k7c2
200	#num#	k4
milionů	milion	k4xCgInPc2
SMS	SMS	kA
zpráv	zpráva	k1gFnPc2
</s>
<s>
sledovala	sledovat	k5eAaImAgFnS
téměř	téměř	k6eAd1
100	#num#	k4
000	#num#	k4
počítačů	počítač	k1gMnPc2
v	v	k7c6
cizích	cizí	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
</s>
<s>
každý	každý	k3xTgInSc4
den	den	k1gInSc4
zachytí	zachytit	k5eAaPmIp3nP
a	a	k8xC
uloží	uložit	k5eAaPmIp3nP
1,7	1,7	k4
miliardy	miliarda	k4xCgFnSc2
emailů	email	k1gInPc2
</s>
<s>
za	za	k7c4
jeden	jeden	k4xCgInSc4
měsíc	měsíc	k1gInSc4
zpracovává	zpracovávat	k5eAaImIp3nS
asi	asi	k9
181	#num#	k4
milionů	milion	k4xCgInPc2
nových	nový	k2eAgInPc2d1
záznamů	záznam	k1gInPc2
(	(	kIx(
<g/>
např.	např.	kA
emaily	email	k1gInPc1
<g/>
,	,	kIx,
audio	audio	k2eAgFnPc1d1
<g/>
,	,	kIx,
foto	foto	k1gNnSc1
a	a	k8xC
video	video	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Celosvětově	celosvětově	k6eAd1
používané	používaný	k2eAgInPc1d1
šifrovací	šifrovací	k2eAgInPc1d1
standardy	standard	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yIgInPc4,k3yRgInPc4
vydává	vydávat	k5eAaPmIp3nS,k5eAaImIp3nS
NIST	NIST	kA
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
prakticky	prakticky	k6eAd1
dílem	díl	k1gInSc7
NSA	NSA	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Pro	pro	k7c4
zadní	zadní	k2eAgNnPc4d1
vrátka	vrátka	k1gNnPc4
pak	pak	k8xC
komunikace	komunikace	k1gFnPc4
nemusí	muset	k5eNaImIp3nS
být	být	k5eAaImF
pro	pro	k7c4
NSA	NSA	kA
šifrovaná	šifrovaný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
tajném	tajný	k2eAgInSc6d1
programu	program	k1gInSc6
PRISM	PRISM	kA
se	se	k3xPyFc4
podílely	podílet	k5eAaImAgFnP
společnosti	společnost	k1gFnPc1
jako	jako	k8xS,k8xC
Microsoft	Microsoft	kA
<g/>
,	,	kIx,
Yahoo	Yahoo	k1gNnSc1
<g/>
,	,	kIx,
Google	Google	k1gNnSc1
<g/>
,	,	kIx,
Facebook	Facebook	k1gInSc1
<g/>
,	,	kIx,
YouTube	YouTub	k1gInSc5
a	a	k8xC
Apple	Apple	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sledována	sledován	k2eAgFnSc1d1
byla	být	k5eAaImAgFnS
komunikace	komunikace	k1gFnSc1
světových	světový	k2eAgMnPc2d1
politiků	politik	k1gMnPc2
včetně	včetně	k7c2
německé	německý	k2eAgFnSc2d1
kancléřky	kancléřka	k1gFnSc2
Angely	Angela	k1gFnSc2
Merkelové	Merkelová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
německého	německý	k2eAgInSc2d1
listu	list	k1gInSc2
Bild	Bild	k1gMnSc1
am	am	k?
Sonntag	Sonntag	k1gInSc1
americký	americký	k2eAgMnSc1d1
prezident	prezident	k1gMnSc1
Barack	Barack	k1gMnSc1
Obama	Obama	k?
nařídil	nařídit	k5eAaPmAgMnS
NSA	NSA	kA
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
o	o	k7c6
kancléřce	kancléřka	k1gFnSc6
vytvořila	vytvořit	k5eAaPmAgFnS
rozsáhlý	rozsáhlý	k2eAgInSc4d1
spis	spis	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
NSA	NSA	kA
má	mít	k5eAaImIp3nS
divizi	divize	k1gFnSc4
zvanou	zvaný	k2eAgFnSc4d1
Tailored	Tailored	k1gInSc4
Access	Access	k1gInSc1
Operations	Operations	k1gInSc1
(	(	kIx(
<g/>
TAO	TAO	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
provádí	provádět	k5eAaImIp3nS
hackerské	hackerský	k2eAgInPc4d1
útoky	útok	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Hackeři	hacker	k1gMnPc1
pod	pod	k7c7
názvem	název	k1gInSc7
Equation	Equation	k1gInSc1
Group	Group	k1gInSc4
možná	možná	k9
také	také	k9
pracují	pracovat	k5eAaImIp3nP
pro	pro	k7c4
NSA	NSA	kA
a	a	k8xC
podle	podle	k7c2
společnosti	společnost	k1gFnSc2
Kaspersky	Kaspersky	k1gFnSc2
stojí	stát	k5eAaImIp3nS
Equation	Equation	k1gInSc1
Group	Group	k1gInSc4
za	za	k7c4
nejméně	málo	k6eAd3
500	#num#	k4
infekcemi	infekce	k1gFnPc7
počítačů	počítač	k1gMnPc2
ve	v	k7c6
finančním	finanční	k2eAgNnSc6d1
<g/>
,	,	kIx,
energetickém	energetický	k2eAgNnSc6d1
<g/>
,	,	kIx,
vládním	vládní	k2eAgInSc6d1
i	i	k8xC
vojenském	vojenský	k2eAgInSc6d1
sektoru	sektor	k1gInSc6
ve	v	k7c6
42	#num#	k4
zemích	zem	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
Snowdena	Snowdeno	k1gNnSc2
hackeři	hacker	k1gMnPc1
z	z	k7c2
NSA	NSA	kA
způsobili	způsobit	k5eAaPmAgMnP
téměř	téměř	k6eAd1
kompletní	kompletní	k2eAgInSc4d1
internetový	internetový	k2eAgInSc4d1
výpadek	výpadek	k1gInSc4
v	v	k7c6
Sýrii	Sýrie	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
National	National	k1gFnSc2
Security	Securita	k1gFnSc2
Agency	Agenca	k1gFnSc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
https://w3techs.com/technologies/overview/ssl_certificate/all	https://w3techs.com/technologies/overview/ssl_certificate/all	k1gInSc1
-	-	kIx~
Usage	Usage	k1gFnSc1
of	of	k?
SSL	SSL	kA
certificate	certificat	k1gInSc5
authorities	authorities	k1gMnSc1
for	forum	k1gNnPc2
websites	websites	k1gInSc1
<g/>
↑	↑	k?
https://zonena.me/2013/09/how-the-nsa-is-breaking-ssl/	https://zonena.me/2013/09/how-the-nsa-is-breaking-ssl/	k4
-	-	kIx~
How	How	k1gFnSc1
the	the	k?
NSA	NSA	kA
is	is	k?
breaking	breaking	k1gInSc1
SSL	SSL	kA
<g/>
↑	↑	k?
https://www.caseyresearch.com/cryptohippies-response-to-the-nsas-attack-on-encryption/	https://www.caseyresearch.com/cryptohippies-response-to-the-nsas-attack-on-encryption/	k?
-	-	kIx~
Cryptohippie	Cryptohippie	k1gFnSc1
Responds	Respondsa	k1gFnPc2
to	ten	k3xDgNnSc1
the	the	k?
NSA	NSA	kA
<g/>
’	’	k?
<g/>
s	s	k7c7
Attack	Attack	k1gMnSc1
on	on	k3xPp3gMnSc1
Encryption	Encryption	k1gInSc4
<g/>
↑	↑	k?
https://gizmodo.com/nsa-paid-security-firm-10-million-bribe-to-keep-encryp-1487442397/amp	https://gizmodo.com/nsa-paid-security-firm-10-million-bribe-to-keep-encryp-1487442397/amp	k1gInSc1
-	-	kIx~
NSA	NSA	kA
Paid	Paid	k1gInSc1
a	a	k8xC
Huge	Huge	k1gInSc1
Security	Securita	k1gFnSc2
Firm	Firma	k1gFnPc2
$	$	kIx~
<g/>
10	#num#	k4
Million	Million	k1gInSc1
to	ten	k3xDgNnSc1
Keep	Keep	k1gInSc4
Encryption	Encryption	k1gInSc1
Weak	Weak	k1gInSc1
<g/>
1	#num#	k4
2	#num#	k4
USA	USA	kA
přiznaly	přiznat	k5eAaPmAgInP
šmírování	šmírování	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tajná	tajný	k2eAgNnPc4d1
NSA	NSA	kA
sbírá	sbírat	k5eAaImIp3nS
soukromá	soukromý	k2eAgNnPc4d1
data	datum	k1gNnPc4
z	z	k7c2
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
↑	↑	k?
https://www.lupa.cz/clanky/nsa-a-gchq-pry-hackly-vyrobce-sim-karet-a-ziskaly-bezpecnostni-klice/	https://www.lupa.cz/clanky/nsa-a-gchq-pry-hackly-vyrobce-sim-karet-a-ziskaly-bezpecnostni-klice/	k?
-	-	kIx~
NSA	NSA	kA
a	a	k8xC
GCHQ	GCHQ	kA
prý	prý	k9
hackly	hacknout	k5eAaPmAgInP
výrobce	výrobce	k1gMnSc1
SIM	sima	k1gFnPc2
karet	kareta	k1gFnPc2
a	a	k8xC
získaly	získat	k5eAaPmAgInP
bezpečnostní	bezpečnostní	k2eAgInPc1d1
klíče	klíč	k1gInPc1
<g/>
↑	↑	k?
http://business.time.com/2013/06/11/big-brother-is-watching-you-swipe-the-nsas-credit-card-data-grab/	http://business.time.com/2013/06/11/big-brother-is-watching-you-swipe-the-nsas-credit-card-data-grab/	k4
-	-	kIx~
Big	Big	k1gMnPc2
Brother	Brother	kA
Is	Is	k1gFnSc1
Watching	Watching	k1gInSc1
You	You	k1gMnSc5
Swipe	Swip	k1gMnSc5
<g/>
:	:	kIx,
The	The	k1gMnSc3
NSA	NSA	kA
<g/>
’	’	k?
<g/>
s	s	k7c7
Credit	Credit	k1gMnSc1
Card	Card	k1gMnSc1
Data	datum	k1gNnSc2
Grab	Grab	k1gMnSc1
<g/>
↑	↑	k?
https://nilsonreport.com/publication_newsletter_archive_issue.php?issue=1143	https://nilsonreport.com/publication_newsletter_archive_issue.php?issue=1143	k4
-	-	kIx~
150	#num#	k4
Largest	Largest	k1gMnSc1
Credit	Credit	k1gMnSc1
Card	Card	k1gMnSc1
Portfolios	Portfolios	k1gMnSc1
Worldwide	Worldwid	k1gInSc5
<g/>
,	,	kIx,
Nilson	Nilson	k1gInSc1
report	report	k1gInSc1
<g/>
,	,	kIx,
Issue	Issue	k1gInSc1
1143	#num#	k4
<g/>
,	,	kIx,
Nov	nov	k1gInSc1
2018	#num#	k4
<g/>
↑	↑	k?
Forbes	forbes	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Forbes	forbes	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
WIRED	WIRED	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
WIRED	WIRED	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Přehledně	přehledně	k6eAd1
<g/>
:	:	kIx,
Aféra	aféra	k1gFnSc1
sledovacích	sledovací	k2eAgFnPc2d1
aktivit	aktivita	k1gFnPc2
tajné	tajný	k2eAgFnSc2d1
služby	služba	k1gFnSc2
NSA	NSA	kA
–	–	k?
neplatný	platný	k2eNgInSc4d1
odkaz	odkaz	k1gInSc4
!	!	kIx.
</s>
<s desamb="1">
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
ještě	ještě	k6eAd1
horší	zlý	k2eAgMnSc1d2
<g/>
.	.	kIx.
</s>
<s desamb="1">
NSA	NSA	kA
sleduje	sledovat	k5eAaImIp3nS
„	„	k?
<g/>
prakticky	prakticky	k6eAd1
všechno	všechen	k3xTgNnSc4
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
děláte	dělat	k5eAaImIp2nP
na	na	k7c6
internetu	internet	k1gInSc6
<g/>
“	“	k?
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Technet	Technet	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2013	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Edward	Edward	k1gMnSc1
Snowden	Snowdna	k1gFnPc2
odhaluje	odhalovat	k5eAaImIp3nS
tajemství	tajemství	k1gNnSc1
NSA	NSA	kA
světu	svět	k1gInSc3
<g/>
"	"	kIx"
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
http://www.pcworld.com/article/2454380/overreliance-on-the-nsa-led-to-weak-crypto-standard-nist-advisers-find.html	http://www.pcworld.com/article/2454380/overreliance-on-the-nsa-led-to-weak-crypto-standard-nist-advisers-find.html	k1gInSc1
-	-	kIx~
Overreliance	Overrelianec	k1gMnSc2
on	on	k3xPp3gMnSc1
the	the	k?
NSA	NSA	kA
led	led	k1gInSc1
to	ten	k3xDgNnSc4
weak	weak	k6eAd1
crypto	crypto	k1gNnSc1
standard	standard	k1gInSc1
<g/>
,	,	kIx,
NIST	NIST	kA
advisers	advisers	k1gInSc1
find	find	k?
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Jak	jak	k8xC,k8xS
nás	my	k3xPp1nPc4
potichu	potichu	k6eAd1
sledují	sledovat	k5eAaImIp3nP
americké	americký	k2eAgInPc1d1
stroje	stroj	k1gInPc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reflex	reflex	k1gInSc1
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2013	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Obama	Obama	k?
o	o	k7c6
odposlouchávání	odposlouchávání	k1gNnSc6
Merkelové	Merkelová	k1gFnSc2
prý	prý	k9
věděl	vědět	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šéf	šéf	k1gMnSc1
NSA	NSA	kA
měl	mít	k5eAaImAgMnS
prezidenta	prezident	k1gMnSc4
osobně	osobně	k6eAd1
informovat	informovat	k5eAaBmF
<g/>
"	"	kIx"
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
27	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2013	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Secret	Secret	k1gInSc1
NSA	NSA	kA
hackers	hackers	k1gInSc1
from	from	k1gInSc1
TAO	TAO	kA
Office	Office	kA
have	have	k1gInSc1
been	been	k1gNnSc1
pwning	pwning	k1gInSc1
China	China	k1gFnSc1
for	forum	k1gNnPc2
nearly	nearla	k1gFnSc2
15	#num#	k4
years	yearsa	k1gFnPc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Computerworld	Computerworld	k1gInSc1
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2013	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Hackeři	hacker	k1gMnPc1
NSA	NSA	kA
umí	umět	k5eAaImIp3nP
doslova	doslova	k6eAd1
neuvěřitelné	uvěřitelný	k2eNgFnPc1d1
věci	věc	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakazí	nakazit	k5eAaPmIp3nS
i	i	k9
cédéčko	cédéčko	k?
v	v	k7c6
poště	pošta	k1gFnSc6
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Technet	Technet	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
23	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Flubbed	Flubbed	k1gMnSc1
NSA	NSA	kA
Hack	Hack	k1gMnSc1
Caused	Caused	k1gMnSc1
Massive	Massiev	k1gFnSc2
2012	#num#	k4
Syrian	Syrian	k1gInSc1
Internet	Internet	k1gInSc4
Blackout	Blackout	k1gMnSc1
<g/>
,	,	kIx,
Snowden	Snowdna	k1gFnPc2
Says	Saysa	k1gFnPc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
International	International	k1gFnPc2
Business	business	k1gInSc1
Times	Times	k1gMnSc1
<g/>
.	.	kIx.
13	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2013	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Five	Five	k1gFnSc1
Eyes	Eyesa	k1gFnPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Národní	národní	k2eAgFnSc1d1
bezpečnostní	bezpečnostní	k2eAgFnSc1d1
agentura	agentura	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
5228208-9	5228208-9	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2185	#num#	k4
2745	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79002506	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
151253538	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79002506	#num#	k4
</s>
