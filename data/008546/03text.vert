<p>
<s>
Obratník	obratník	k1gInSc1	obratník
Raka	rak	k1gMnSc2	rak
je	být	k5eAaImIp3nS	být
obratník	obratník	k1gInSc4	obratník
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c4	na
23	[number]	k4	23
<g/>
°	°	k?	°
26	[number]	k4	26
<g/>
'	'	kIx"	'
14.675	[number]	k4	14.675
<g/>
''	''	k?	''
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejsevernější	severní	k2eAgFnSc4d3	nejsevernější
rovnoběžku	rovnoběžka	k1gFnSc4	rovnoběžka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
Slunce	slunce	k1gNnSc1	slunce
v	v	k7c6	v
zenitu	zenit	k1gInSc6	zenit
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc2	tento
šířky	šířka	k1gFnSc2	šířka
Slunce	slunce	k1gNnSc2	slunce
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
pouze	pouze	k6eAd1	pouze
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
rok	rok	k1gInSc4	rok
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
době	doba	k1gFnSc6	doba
letního	letní	k2eAgInSc2d1	letní
slunovratu	slunovrat	k1gInSc2	slunovrat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
obratníku	obratník	k1gInSc2	obratník
Raka	rak	k1gMnSc2	rak
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
obdobně	obdobně	k6eAd1	obdobně
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
u	u	k7c2	u
obratníku	obratník	k1gInSc2	obratník
Kozoroha	Kozoroh	k1gMnSc2	Kozoroh
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
mnoho	mnoho	k4c4	mnoho
pouští	poušť	k1gFnPc2	poušť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Státy	stát	k1gInPc1	stát
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odraz	odraz	k1gInSc4	odraz
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
kniha	kniha	k1gFnSc1	kniha
Obratník	obratník	k1gInSc1	obratník
Raka	rak	k1gMnSc2	rak
amerického	americký	k2eAgMnSc2d1	americký
surrealistického	surrealistický	k2eAgMnSc2d1	surrealistický
spisovatele	spisovatel	k1gMnSc2	spisovatel
Henryho	Henry	k1gMnSc2	Henry
Millera	Miller	k1gMnSc2	Miller
</s>
</p>
<p>
<s>
hudební	hudební	k2eAgNnSc4d1	hudební
album	album	k1gNnSc4	album
Obratník	obratník	k1gInSc4	obratník
Raka	rak	k1gMnSc2	rak
české	český	k2eAgFnSc2d1	Česká
hudební	hudební	k2eAgFnSc2d1	hudební
skupiny	skupina	k1gFnSc2	skupina
Citron	citron	k1gInSc1	citron
</s>
</p>
<p>
<s>
kniha	kniha	k1gFnSc1	kniha
Ledová	ledový	k2eAgFnSc1d1	ledová
společnost	společnost	k1gFnSc1	společnost
<g/>
:	:	kIx,	:
Obratník	obratník	k1gInSc1	obratník
Raka	rak	k1gMnSc2	rak
francouzského	francouzský	k2eAgMnSc2d1	francouzský
spisovatele	spisovatel	k1gMnSc2	spisovatel
sci-fi	scii	k1gFnSc2	sci-fi
G.	G.	kA	G.
<g/>
-	-	kIx~	-
<g/>
J.	J.	kA	J.
Arnauda	Arnauda	k1gMnSc1	Arnauda
</s>
</p>
<p>
<s>
italský	italský	k2eAgInSc1d1	italský
film	film	k1gInSc1	film
Na	na	k7c6	na
obratníku	obratník	k1gInSc6	obratník
Raka	rak	k1gMnSc2	rak
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
v	v	k7c6	v
režii	režie	k1gFnSc6	režie
G.	G.	kA	G.
Lomiho	Lomi	k1gMnSc2	Lomi
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Obratník	obratník	k1gInSc1	obratník
Kozoroha	Kozoroh	k1gMnSc2	Kozoroh
</s>
</p>
<p>
<s>
Rovník	rovník	k1gInSc1	rovník
</s>
</p>
<p>
<s>
Tropický	tropický	k2eAgInSc1d1	tropický
pás	pás	k1gInSc1	pás
</s>
</p>
