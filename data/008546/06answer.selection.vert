<s>
Obratník	obratník	k1gInSc1	obratník
Raka	rak	k1gMnSc2	rak
je	být	k5eAaImIp3nS	být
obratník	obratník	k1gInSc4	obratník
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c4	na
23	[number]	k4	23
<g/>
°	°	k?	°
26	[number]	k4	26
<g/>
'	'	kIx"	'
14.675	[number]	k4	14.675
<g/>
''	''	k?	''
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
<g/>
.	.	kIx.	.
</s>
