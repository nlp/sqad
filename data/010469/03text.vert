<p>
<s>
Aritmeticko-geometrická	aritmetickoeometrický	k2eAgFnSc1d1	aritmeticko-geometrický
posloupnost	posloupnost	k1gFnSc1	posloupnost
je	být	k5eAaImIp3nS	být
posloupnost	posloupnost	k1gFnSc4	posloupnost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
součinem	součin	k1gInSc7	součin
aritmetické	aritmetický	k2eAgFnSc2d1	aritmetická
a	a	k8xC	a
geometrické	geometrický	k2eAgFnSc2d1	geometrická
posloupnosti	posloupnost	k1gFnSc2	posloupnost
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
posloupnost	posloupnost	k1gFnSc1	posloupnost
daná	daný	k2eAgFnSc1d1	daná
předpisem	předpis	k1gInSc7	předpis
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
<g/>
+	+	kIx~	+
<g/>
bn	bn	k?	bn
<g/>
)	)	kIx)	)
<g/>
q	q	k?	q
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
∈	∈	k?	∈
</s>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
q	q	k?	q
</s>
</p>
<p>
<s>
∈	∈	k?	∈
</s>
</p>
<p>
</p>
<p>
<s>
R	R	kA	R
</s>
</p>
<p>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
N	N	kA	N
<g/>
}	}	kIx)	}
,	,	kIx,	,
<g/>
a	a	k8xC	a
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
,	,	kIx,	,
<g/>
q	q	k?	q
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
\	\	kIx~	\
<g/>
mathbb	mathbb	k1gMnSc1	mathbb
{	{	kIx(	{
<g/>
R	R	kA	R
<g/>
}	}	kIx)	}
.	.	kIx.	.
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Příklad	příklad	k1gInSc1	příklad
==	==	k?	==
</s>
</p>
<p>
<s>
Příkladem	příklad	k1gInSc7	příklad
aritmeticko-geometrické	aritmetickoeometrický	k2eAgFnSc2d1	aritmeticko-geometrický
posloupnosti	posloupnost	k1gFnSc2	posloupnost
je	být	k5eAaImIp3nS	být
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
8	[number]	k4	8
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
24	[number]	k4	24
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
...	...	k?	...
</s>
</p>
<p>
<s>
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
a_	a_	k?	a_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
n	n	k0	n
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gInSc1	cdot
2	[number]	k4	2
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
2,8	[number]	k4	2,8
<g/>
,24	,24	k4	,24
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
dots	dots	k6eAd1	dots
.	.	kIx.	.
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Protože	protože	k8xS	protože
konstantní	konstantní	k2eAgFnSc1d1	konstantní
posloupnost	posloupnost	k1gFnSc1	posloupnost
(	(	kIx(	(
<g/>
samých	samý	k3xTgFnPc2	samý
jedniček	jednička	k1gFnPc2	jednička
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
aritmetická	aritmetický	k2eAgFnSc1d1	aritmetická
i	i	k8xC	i
geometrická	geometrický	k2eAgFnSc1d1	geometrická
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
aritmeticko-geometrická	aritmetickoeometrický	k2eAgFnSc1d1	aritmeticko-geometrický
posloupnost	posloupnost	k1gFnSc1	posloupnost
zobecněním	zobecnění	k1gNnSc7	zobecnění
obou	dva	k4xCgFnPc6	dva
těchto	tento	k3xDgInPc2	tento
elementárních	elementární	k2eAgInPc2d1	elementární
typů	typ	k1gInPc2	typ
posloupností	posloupnost	k1gFnPc2	posloupnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Posloupnost	posloupnost	k1gFnSc1	posloupnost
částečných	částečný	k2eAgInPc2d1	částečný
součtů	součet	k1gInPc2	součet
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
poměrně	poměrně	k6eAd1	poměrně
snadno	snadno	k6eAd1	snadno
podobným	podobný	k2eAgInSc7d1	podobný
postupem	postup	k1gInSc7	postup
jako	jako	k8xS	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
geometrické	geometrický	k2eAgFnSc2d1	geometrická
posloupnosti	posloupnost	k1gFnSc2	posloupnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc3	použití
==	==	k?	==
</s>
</p>
<p>
<s>
A.	A.	kA	A.
<g/>
-	-	kIx~	-
<g/>
g.	g.	k?	g.
posloupnosti	posloupnost	k1gFnSc2	posloupnost
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
jako	jako	k9	jako
řešení	řešení	k1gNnSc4	řešení
lineárních	lineární	k2eAgFnPc2d1	lineární
rekurentních	rekurentní	k2eAgFnPc2d1	rekurentní
rovnic	rovnice	k1gFnPc2	rovnice
2	[number]	k4	2
<g/>
.	.	kIx.	.
a	a	k8xC	a
vyššího	vysoký	k2eAgInSc2d2	vyšší
řádu	řád	k1gInSc2	řád
s	s	k7c7	s
konstantními	konstantní	k2eAgInPc7d1	konstantní
koeficienty	koeficient	k1gInPc7	koeficient
v	v	k7c6	v
případě	případ	k1gInSc6	případ
násobného	násobný	k2eAgInSc2d1	násobný
kořene	kořen	k1gInSc2	kořen
charakteristické	charakteristický	k2eAgFnSc2d1	charakteristická
rovnice	rovnice	k1gFnSc2	rovnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
např.	např.	kA	např.
ve	v	k7c6	v
financích	finance	k1gFnPc6	finance
při	při	k7c6	při
výpočtu	výpočet	k1gInSc6	výpočet
počáteční	počáteční	k2eAgFnPc1d1	počáteční
nebo	nebo	k8xC	nebo
koncové	koncový	k2eAgFnPc1d1	koncová
hodnoty	hodnota	k1gFnPc1	hodnota
aritmeticky	aritmeticky	k6eAd1	aritmeticky
rostoucích	rostoucí	k2eAgInPc2d1	rostoucí
nebo	nebo	k8xC	nebo
klesajících	klesající	k2eAgInPc2d1	klesající
důchodů	důchod	k1gInPc2	důchod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Aritmetická	aritmetický	k2eAgFnSc1d1	aritmetická
posloupnost	posloupnost	k1gFnSc1	posloupnost
</s>
</p>
<p>
<s>
Geometrická	geometrický	k2eAgFnSc1d1	geometrická
posloupnost	posloupnost	k1gFnSc1	posloupnost
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Calda	Calda	k1gMnSc1	Calda
Emil	Emil	k1gMnSc1	Emil
<g/>
.	.	kIx.	.
</s>
<s>
Posloupnosti	posloupnost	k1gFnPc1	posloupnost
a	a	k8xC	a
nekonečné	konečný	k2eNgFnPc1d1	nekonečná
řady	řada	k1gFnPc1	řada
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BERAN	Beran	k1gMnSc1	Beran
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
.	.	kIx.	.
</s>
<s>
Prověřte	prověřit	k5eAaPmRp2nP	prověřit
si	se	k3xPyFc3	se
své	svůj	k3xOyFgNnSc4	svůj
matematické	matematický	k2eAgNnSc4d1	matematické
nadání	nadání	k1gNnSc4	nadání
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
159	[number]	k4	159
s.	s.	k?	s.
</s>
</p>
