<p>
<s>
Lydický	Lydický	k2eAgInSc1d1	Lydický
modus	modus	k1gInSc1	modus
je	být	k5eAaImIp3nS	být
pojem	pojem	k1gInSc4	pojem
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
hudební	hudební	k2eAgFnSc2d1	hudební
nauky	nauka	k1gFnSc2	nauka
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
posloupnost	posloupnost	k1gFnSc4	posloupnost
tónů	tón	k1gInPc2	tón
diatonické	diatonický	k2eAgFnSc2d1	diatonická
durové	durový	k2eAgFnSc2d1	durová
stupnice	stupnice	k1gFnSc2	stupnice
zahrané	zahraný	k2eAgFnSc2d1	zahraná
od	od	k7c2	od
jejího	její	k3xOp3gInSc2	její
čtvrtého	čtvrtý	k4xOgInSc2	čtvrtý
stupně	stupeň	k1gInSc2	stupeň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Starořecký	starořecký	k2eAgInSc4d1	starořecký
a	a	k8xC	a
středověký	středověký	k2eAgInSc4d1	středověký
církevní	církevní	k2eAgInSc4d1	církevní
lydický	lydický	k2eAgInSc4d1	lydický
modus	modus	k1gInSc4	modus
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
antickém	antický	k2eAgNnSc6d1	antické
Řecku	Řecko	k1gNnSc6	Řecko
byla	být	k5eAaImAgFnS	být
pojmem	pojem	k1gInSc7	pojem
lydický	lydický	k2eAgInSc4d1	lydický
modus	modus	k1gInSc4	modus
označována	označován	k2eAgFnSc1d1	označována
sestupná	sestupný	k2eAgFnSc1d1	sestupná
posloupnost	posloupnost	k1gFnSc1	posloupnost
tónů	tón	k1gInPc2	tón
totožná	totožný	k2eAgFnSc1d1	totožná
s	s	k7c7	s
modem	modus	k1gInSc7	modus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xS	jako
jónský	jónský	k2eAgInSc1d1	jónský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počínaje	počínaje	k7c7	počínaje
evropským	evropský	k2eAgInSc7d1	evropský
středověkem	středověk	k1gInSc7	středověk
je	být	k5eAaImIp3nS	být
pojmem	pojem	k1gInSc7	pojem
lydický	lydický	k2eAgInSc4d1	lydický
modus	modus	k1gInSc4	modus
označován	označován	k2eAgInSc1d1	označován
modus	modus	k1gInSc1	modus
durové	durový	k2eAgFnSc2d1	durová
stupnice	stupnice	k1gFnSc2	stupnice
popsaný	popsaný	k2eAgInSc1d1	popsaný
v	v	k7c6	v
úvodním	úvodní	k2eAgInSc6d1	úvodní
odstavci	odstavec	k1gInSc6	odstavec
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
něm	on	k3xPp3gInSc6	on
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
celý	celý	k2eAgInSc4d1	celý
zbytek	zbytek	k1gInSc4	zbytek
tohoto	tento	k3xDgInSc2	tento
článku	článek	k1gInSc2	článek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc4	vlastnost
lydického	lydický	k2eAgInSc2d1	lydický
modu	modus	k1gInSc2	modus
==	==	k?	==
</s>
</p>
<p>
<s>
Lydický	Lydický	k2eAgInSc1d1	Lydický
modus	modus	k1gInSc1	modus
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
z	z	k7c2	z
durové	durový	k2eAgFnSc2d1	durová
stupnice	stupnice	k1gFnSc2	stupnice
jejím	její	k3xOp3gNnPc3	její
zahráním	zahrání	k1gNnPc3	zahrání
od	od	k7c2	od
čtvrtého	čtvrtý	k4xOgInSc2	čtvrtý
stupně	stupeň	k1gInSc2	stupeň
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
případě	případ	k1gInSc6	případ
C	C	kA	C
dur	dur	k1gNnSc6	dur
je	být	k5eAaImIp3nS	být
základním	základní	k2eAgInSc7d1	základní
tónem	tón	k1gInSc7	tón
lydického	lydický	k2eAgInSc2d1	lydický
modu	modus	k1gInSc2	modus
F	F	kA	F
a	a	k8xC	a
znění	znění	k1gNnSc6	znění
lydického	lydický	k2eAgInSc2d1	lydický
modu	modus	k1gInSc2	modus
<g/>
:	:	kIx,	:
f-g-a-h-c-d-	f-	k?	f-g-a-h-c-d-
<g/>
e.	e.	k?	e.
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
durový	durový	k2eAgInSc4d1	durový
modus	modus	k1gInSc4	modus
(	(	kIx(	(
<g/>
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
tercií	tercie	k1gFnSc7	tercie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
"	"	kIx"	"
<g/>
tvrdou	tvrdý	k2eAgFnSc7d1	tvrdá
<g/>
"	"	kIx"	"
velkou	velký	k2eAgFnSc7d1	velká
septimou	septima	k1gFnSc7	septima
a	a	k8xC	a
zvětšenou	zvětšený	k2eAgFnSc7d1	zvětšená
kvartou	kvarta	k1gFnSc7	kvarta
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
od	od	k7c2	od
běžné	běžný	k2eAgFnSc2d1	běžná
durové	durový	k2eAgFnSc2d1	durová
stupnice	stupnice	k1gFnSc2	stupnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
"	"	kIx"	"
<g/>
nejtvrdší	tvrdý	k2eAgFnPc4d3	nejtvrdší
<g/>
"	"	kIx"	"
z	z	k7c2	z
modů	modus	k1gInPc2	modus
durové	durový	k2eAgFnSc2d1	durová
stupnice	stupnice	k1gFnSc2	stupnice
<g/>
.	.	kIx.	.
</s>
<s>
Nejbližším	blízký	k2eAgInSc7d3	Nejbližší
měkčím	měkký	k2eAgInSc7d2	měkčí
modem	modus	k1gInSc7	modus
je	být	k5eAaImIp3nS	být
jónský	jónský	k2eAgInSc1d1	jónský
modus	modus	k1gInSc1	modus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
lydického	lydický	k2eAgNnSc2d1	lydický
liší	lišit	k5eAaImIp3nS	lišit
čistou	čistý	k2eAgFnSc7d1	čistá
kvartou	kvarta	k1gFnSc7	kvarta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
poslechu	poslech	k1gInSc3	poslech
<g/>
:	:	kIx,	:
C	C	kA	C
lydický	lydický	k2eAgInSc1d1	lydický
modus	modus	k1gInSc1	modus
</s>
</p>
<p>
<s>
===	===	k?	===
Intervalové	intervalový	k2eAgNnSc4d1	intervalové
složení	složení	k1gNnSc4	složení
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Složení	složení	k1gNnSc1	složení
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
tóninách	tónina	k1gFnPc6	tónina
==	==	k?	==
</s>
</p>
<p>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
složení	složení	k1gNnSc4	složení
lydyckého	lydycký	k2eAgInSc2d1	lydycký
módu	mód	k1gInSc2	mód
pro	pro	k7c4	pro
jednotlivé	jednotlivý	k2eAgFnPc4d1	jednotlivá
tóniny	tónina	k1gFnPc4	tónina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristické	charakteristický	k2eAgInPc1d1	charakteristický
akordy	akord	k1gInPc1	akord
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
lydický	lydický	k2eAgInSc4d1	lydický
mód	mód	k1gInSc4	mód
je	být	k5eAaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
durový	durový	k2eAgInSc1d1	durový
kvintakord	kvintakord	k1gInSc1	kvintakord
<g/>
,	,	kIx,	,
ze	z	k7c2	z
septakordů	septakord	k1gInPc2	septakord
pak	pak	k6eAd1	pak
velký	velký	k2eAgInSc4d1	velký
septakord	septakord	k1gInSc4	septakord
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kompletní	kompletní	k2eAgInSc1d1	kompletní
tónový	tónový	k2eAgInSc1d1	tónový
materiál	materiál	k1gInSc1	materiál
modu	modus	k1gInSc2	modus
je	být	k5eAaImIp3nS	být
vyjádřen	vyjádřit	k5eAaPmNgInS	vyjádřit
sedmizvukem	sedmizvuk	k1gInSc7	sedmizvuk
(	(	kIx(	(
<g/>
tercdecimovým	tercdecimový	k2eAgInSc7d1	tercdecimový
akordem	akord	k1gInSc7	akord
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
X	X	kA	X
</s>
</p>
<p>
</p>
<p>
<s>
13	[number]	k4	13
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
11	[number]	k4	11
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
7	[number]	k4	7
</s>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
j	j	k?	j
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
X	X	kA	X
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
13	[number]	k4	13
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
<g/>
+	+	kIx~	+
<g/>
/	/	kIx~	/
<g/>
7	[number]	k4	7
<g/>
maj	maj	k?	maj
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
X	X	kA	X
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgInSc4d1	základní
tón	tón	k1gInSc4	tón
lydického	lydický	k2eAgInSc2d1	lydický
modu	modus	k1gInSc2	modus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
charakteristické	charakteristický	k2eAgInPc4d1	charakteristický
akordy	akord	k1gInPc4	akord
lydického	lydický	k2eAgInSc2d1	lydický
modu	modus	k1gInSc2	modus
v	v	k7c6	v
tónině	tónina	k1gFnSc6	tónina
C	C	kA	C
dur	dur	k1gNnSc7	dur
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Použitelné	použitelný	k2eAgInPc1d1	použitelný
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
další	další	k2eAgInPc1d1	další
akordy	akord	k1gInPc1	akord
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
vzniknou	vzniknout	k5eAaPmIp3nP	vzniknout
vynecháním	vynechání	k1gNnSc7	vynechání
některých	některý	k3yIgInPc2	některý
intervalů	interval	k1gInPc2	interval
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
13	[number]	k4	13
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
11	[number]	k4	11
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
7	[number]	k4	7
</s>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
j	j	k?	j
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F	F	kA	F
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
13	[number]	k4	13
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
<g/>
+	+	kIx~	+
<g/>
/	/	kIx~	/
<g/>
7	[number]	k4	7
<g/>
maj	maj	k?	maj
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
6	[number]	k4	6
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
7	[number]	k4	7
</s>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
j	j	k?	j
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
6	[number]	k4	6
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
F	F	kA	F
</s>
</p>
<p>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
<s>
9	[number]	k4	9
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
F	F	kA	F
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
6	[number]	k4	6
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
F	F	kA	F
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
7	[number]	k4	7
<g/>
maj	maj	k?	maj
<g/>
/	/	kIx~	/
<g/>
6	[number]	k4	6
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
F	F	kA	F
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
add	add	k?	add
<g/>
9	[number]	k4	9
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Modus	modus	k1gInSc1	modus
</s>
</p>
<p>
<s>
Tónika	tónika	k1gFnSc1	tónika
</s>
</p>
<p>
<s>
Jónský	jónský	k2eAgInSc1d1	jónský
modus	modus	k1gInSc1	modus
</s>
</p>
<p>
<s>
Mixolydický	Mixolydický	k2eAgInSc1d1	Mixolydický
modus	modus	k1gInSc1	modus
</s>
</p>
<p>
<s>
Durová	durový	k2eAgFnSc1d1	durová
stupnice	stupnice	k1gFnSc1	stupnice
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lydický	Lydický	k2eAgInSc4d1	Lydický
modus	modus	k1gInSc4	modus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Kytarové	kytarový	k2eAgInPc1d1	kytarový
prstoklady	prstoklad	k1gInPc1	prstoklad
lydického	lydický	k2eAgInSc2d1	lydický
modu	modus	k1gInSc2	modus
</s>
</p>
