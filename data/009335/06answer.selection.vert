<s>
Pardubice	Pardubice	k1gInPc1	Pardubice
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
proslulé	proslulý	k2eAgNnSc4d1	proslulé
perníkem	perník	k1gInSc7	perník
<g/>
;	;	kIx,	;
tradiční	tradiční	k2eAgNnSc1d1	tradiční
spojení	spojení	k1gNnSc1	spojení
"	"	kIx"	"
<g/>
pardubický	pardubický	k2eAgInSc1d1	pardubický
perník	perník	k1gInSc1	perník
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
chráněné	chráněný	k2eAgNnSc1d1	chráněné
označení	označení	k1gNnSc1	označení
původu	původ	k1gInSc2	původ
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
