<s>
Inline	Inlinout	k5eAaPmIp3nS	Inlinout
brusle	brusle	k1gFnSc1	brusle
jsou	být	k5eAaImIp3nP	být
kolečkové	kolečkový	k2eAgFnPc4d1	kolečková
brusle	brusle	k1gFnPc4	brusle
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
kolečka	kolečko	k1gNnPc4	kolečko
umístěná	umístěný	k2eAgNnPc4d1	umístěné
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
řadě	řada	k1gFnSc6	řada
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
.	.	kIx.	.
</s>
<s>
Samotná	samotný	k2eAgFnSc1d1	samotná
brusle	brusle	k1gFnSc1	brusle
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
boty	bota	k1gFnSc2	bota
<g/>
,	,	kIx,	,
rámu	rám	k1gInSc2	rám
<g/>
,	,	kIx,	,
koleček	kolečko	k1gNnPc2	kolečko
a	a	k8xC	a
ložisek	ložisko	k1gNnPc2	ložisko
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
části	část	k1gFnPc1	část
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
podle	podle	k7c2	podle
způsobu	způsob	k1gInSc2	způsob
použití	použití	k1gNnSc2	použití
bruslí	brusle	k1gFnPc2	brusle
<g/>
.	.	kIx.	.
</s>
<s>
Inline	Inlinout	k5eAaPmIp3nS	Inlinout
bruslení	bruslení	k1gNnSc1	bruslení
je	být	k5eAaImIp3nS	být
mladý	mladý	k2eAgInSc4d1	mladý
sport	sport	k1gInSc4	sport
<g/>
,	,	kIx,	,
rozšířený	rozšířený	k2eAgInSc4d1	rozšířený
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nS	členit
do	do	k7c2	do
několika	několik	k4yIc2	několik
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
kategorie	kategorie	k1gFnSc1	kategorie
používá	používat	k5eAaImIp3nS	používat
specifický	specifický	k2eAgInSc1d1	specifický
typ	typ	k1gInSc1	typ
brusle	brusle	k1gFnSc2	brusle
<g/>
.	.	kIx.	.
</s>
<s>
Kondiční	kondiční	k2eAgFnSc3d1	kondiční
variantě	varianta	k1gFnSc3	varianta
(	(	kIx(	(
<g/>
fitness	fitness	k1gInSc1	fitness
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
zájemců	zájemce	k1gMnPc2	zájemce
si	se	k3xPyFc3	se
získává	získávat	k5eAaImIp3nS	získávat
i	i	k8xC	i
inline	inlinout	k5eAaPmIp3nS	inlinout
rychlobruslení	rychlobruslení	k1gNnSc1	rychlobruslení
<g/>
.	.	kIx.	.
</s>
<s>
Závody	závod	k1gInPc1	závod
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
variantách	varianta	k1gFnPc6	varianta
a	a	k8xC	a
délkách	délka	k1gFnPc6	délka
(	(	kIx(	(
<g/>
od	od	k7c2	od
200	[number]	k4	200
<g/>
m	m	kA	m
po	po	k7c4	po
maratony	maraton	k1gInPc4	maraton
a	a	k8xC	a
dálkové	dálkový	k2eAgInPc4d1	dálkový
závody	závod	k1gInPc4	závod
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
na	na	k7c6	na
dráze	dráha	k1gFnSc6	dráha
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
na	na	k7c6	na
silnici	silnice	k1gFnSc6	silnice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skateparcích	skateparek	k1gInPc6	skateparek
dovádějí	dovádět	k5eAaImIp3nP	dovádět
odvážní	odvážný	k2eAgMnPc1d1	odvážný
vyznavači	vyznavač	k1gMnPc1	vyznavač
aggressive	aggressiev	k1gFnSc2	aggressiev
bruslení	bruslený	k2eAgMnPc1d1	bruslený
na	na	k7c6	na
různých	různý	k2eAgFnPc6d1	různá
umělých	umělý	k2eAgFnPc6d1	umělá
překážkách	překážka	k1gFnPc6	překážka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
jsou	být	k5eAaImIp3nP	být
též	též	k6eAd1	též
rozšířené	rozšířený	k2eAgFnPc4d1	rozšířená
inline	inlinout	k5eAaPmIp3nS	inlinout
jízdy	jízda	k1gFnPc4	jízda
městy	město	k1gNnPc7	město
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Paříž	Paříž	k1gFnSc1	Paříž
<g/>
,	,	kIx,	,
Berlín	Berlín	k1gInSc1	Berlín
<g/>
,	,	kIx,	,
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
Bratislava	Bratislava	k1gFnSc1	Bratislava
atd.	atd.	kA	atd.
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc7d3	veliký
obdobnou	obdobný	k2eAgFnSc7d1	obdobná
akcí	akce	k1gFnSc7	akce
pravidelné	pravidelný	k2eAgNnSc1d1	pravidelné
noční	noční	k2eAgNnSc1d1	noční
bruslení	bruslení	k1gNnSc1	bruslení
v	v	k7c6	v
Opavě	Opava	k1gFnSc6	Opava
-	-	kIx~	-
Blade	Blad	k1gInSc5	Blad
Nights	Nightsa	k1gFnPc2	Nightsa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
každou	každý	k3xTgFnSc4	každý
sobotu	sobota	k1gFnSc4	sobota
od	od	k7c2	od
května	květen	k1gInSc2	květen
do	do	k7c2	do
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
počet	počet	k1gInSc1	počet
účastníků	účastník	k1gMnPc2	účastník
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
akci	akce	k1gFnSc6	akce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
ulicích	ulice	k1gFnPc6	ulice
města	město	k1gNnSc2	město
při	při	k7c6	při
zastavené	zastavený	k2eAgFnSc6d1	zastavená
dopravě	doprava	k1gFnSc6	doprava
<g/>
,	,	kIx,	,
bývá	bývat	k5eAaImIp3nS	bývat
okolo	okolo	k7c2	okolo
pěti	pět	k4xCc2	pět
set	sto	k4xCgNnPc2	sto
<g/>
.	.	kIx.	.
rychlobruslení	rychlobruslení	k1gNnSc2	rychlobruslení
<g/>
;	;	kIx,	;
Brusle	brusle	k1gFnPc1	brusle
jsou	být	k5eAaImIp3nP	být
typické	typický	k2eAgInPc1d1	typický
nízkou	nízký	k2eAgFnSc7d1	nízká
botou	bota	k1gFnSc7	bota
která	který	k3yIgNnPc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
plně	plně	k6eAd1	plně
využít	využít	k5eAaPmF	využít
potenciál	potenciál	k1gInSc4	potenciál
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
tvrdé	tvrdý	k2eAgFnPc4d1	tvrdá
boty	bota	k1gFnPc4	bota
podobné	podobný	k2eAgFnPc4d1	podobná
cyklistickým	cyklistický	k2eAgFnPc3d1	cyklistická
tretrám	tretra	k1gFnPc3	tretra
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
je	být	k5eAaImIp3nS	být
botu	bota	k1gFnSc4	bota
zapotřebí	zapotřebí	k6eAd1	zapotřebí
individuálně	individuálně	k6eAd1	individuálně
dotvarovat	dotvarovat	k5eAaPmF	dotvarovat
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
není	být	k5eNaImIp3nS	být
problém	problém	k1gInSc1	problém
v	v	k7c6	v
bruslích	brusle	k1gFnPc6	brusle
strávit	strávit	k5eAaPmF	strávit
i	i	k9	i
několik	několik	k4yIc4	několik
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
najet	najet	k5eAaPmF	najet
přes	přes	k7c4	přes
100	[number]	k4	100
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Skelet	skelet	k1gInSc1	skelet
boty	bota	k1gFnSc2	bota
je	být	k5eAaImIp3nS	být
vyráběn	vyrábět	k5eAaImNgInS	vyrábět
z	z	k7c2	z
plastu	plast	k1gInSc2	plast
<g/>
,	,	kIx,	,
laminátu	laminát	k1gInSc2	laminát
a	a	k8xC	a
nejlépe	dobře	k6eAd3	dobře
z	z	k7c2	z
karbonu	karbon	k1gInSc2	karbon
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInPc1d1	základní
skelety	skelet	k1gInPc1	skelet
jsou	být	k5eAaImIp3nP	být
bez	bez	k7c2	bez
možnosti	možnost	k1gFnSc2	možnost
tvarování	tvarování	k1gNnSc2	tvarování
na	na	k7c4	na
nohu	noha	k1gFnSc4	noha
<g/>
.	.	kIx.	.
</s>
<s>
Skelety	skelet	k1gInPc1	skelet
z	z	k7c2	z
karbonové	karbonový	k2eAgFnSc2d1	karbonová
tkaniny	tkanina	k1gFnSc2	tkanina
a	a	k8xC	a
nebo	nebo	k8xC	nebo
její	její	k3xOp3gFnPc1	její
směsi	směs	k1gFnPc1	směs
bývají	bývat	k5eAaImIp3nP	bývat
dodatečně	dodatečně	k6eAd1	dodatečně
individuálně	individuálně	k6eAd1	individuálně
tvarovatelné	tvarovatelný	k2eAgFnPc1d1	tvarovatelná
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
nohu	noha	k1gFnSc4	noha
závodníka	závodník	k1gMnSc2	závodník
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInPc4d3	Nejvyšší
modely	model	k1gInPc4	model
lze	lze	k6eAd1	lze
vyrábět	vyrábět	k5eAaImF	vyrábět
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
zakázku	zakázka	k1gFnSc4	zakázka
a	a	k8xC	a
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
dělá	dělat	k5eAaImIp3nS	dělat
odlite	odlit	k1gInSc5	odlit
nohy	noha	k1gFnPc1	noha
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
noha	noha	k1gFnSc1	noha
oskenuje	oskenovat	k5eAaImIp3nS	oskenovat
-	-	kIx~	-
BONT	BONT	kA	BONT
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřek	vnitřek	k1gInSc1	vnitřek
boty	bota	k1gFnSc2	bota
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
vyráběn	vyrábět	k5eAaImNgMnS	vyrábět
z	z	k7c2	z
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
botu	bota	k1gFnSc4	bota
je	být	k5eAaImIp3nS	být
připevněn	připevněn	k2eAgInSc1d1	připevněn
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
rám	rám	k1gInSc1	rám
(	(	kIx(	(
<g/>
též	též	k9	též
frame	framat	k5eAaPmIp3nS	framat
<g/>
,	,	kIx,	,
či	či	k8xC	či
šasi	šasi	k1gNnSc1	šasi
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
materiál	materiál	k1gInSc1	materiál
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
slitin	slitina	k1gFnPc2	slitina
hliníku	hliník	k1gInSc2	hliník
řady	řada	k1gFnSc2	řada
6000	[number]	k4	6000
používaných	používaný	k2eAgMnPc2d1	používaný
v	v	k7c6	v
automobilovém	automobilový	k2eAgInSc6d1	automobilový
průmyslu	průmysl	k1gInSc6	průmysl
nebo	nebo	k8xC	nebo
kvalitnější	kvalitní	k2eAgFnPc1d2	kvalitnější
řady	řada	k1gFnPc1	řada
7000	[number]	k4	7000
používaném	používaný	k2eAgNnSc6d1	používané
v	v	k7c6	v
leteckém	letecký	k2eAgNnSc6d1	letecké
průmyslu	průmysl	k1gInSc6	průmysl
AL	ala	k1gFnPc2	ala
7050	[number]	k4	7050
<g/>
,	,	kIx,	,
či	či	k8xC	či
karbon	karbon	k1gInSc1	karbon
<g/>
.	.	kIx.	.
</s>
<s>
Rámy	Ráma	k1gMnPc4	Ráma
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
botě	bota	k1gFnSc3	bota
posouvat	posouvat	k5eAaImF	posouvat
hlavně	hlavně	k9	hlavně
do	do	k7c2	do
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
správně	správně	k6eAd1	správně
tak	tak	k6eAd1	tak
nastavit	nastavit	k5eAaPmF	nastavit
usazení	usazení	k1gNnSc4	usazení
pod	pod	k7c4	pod
těžiště	těžiště	k1gNnSc4	těžiště
<g/>
.	.	kIx.	.
</s>
<s>
Rámy	rám	k1gInPc1	rám
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
délkou	délka	k1gFnSc7	délka
-	-	kIx~	-
od	od	k7c2	od
11.6	[number]	k4	11.6
<g/>
-	-	kIx~	-
<g/>
13.3	[number]	k4	13.3
palců	palec	k1gInPc2	palec
<g/>
,	,	kIx,	,
výškou	výška	k1gFnSc7	výška
a	a	k8xC	a
možností	možnost	k1gFnSc7	možnost
osazení	osazení	k1gNnSc2	osazení
koleček	koleček	k1gInSc1	koleček
<g/>
.	.	kIx.	.
</s>
<s>
Nejpoužívanější	používaný	k2eAgInPc1d3	nejpoužívanější
rámy	rám	k1gInPc1	rám
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
4	[number]	k4	4
kolečka	kolečko	k1gNnSc2	kolečko
do	do	k7c2	do
velikosti	velikost	k1gFnSc2	velikost
110	[number]	k4	110
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dnes	dnes	k6eAd1	dnes
špička	špička	k1gFnSc1	špička
jezdí	jezdit	k5eAaImIp3nS	jezdit
na	na	k7c6	na
kolečkách	koleček	k1gInPc6	koleček
velikosti	velikost	k1gFnSc2	velikost
125	[number]	k4	125
<g/>
mm	mm	kA	mm
-	-	kIx~	-
3	[number]	k4	3
x	x	k?	x
125	[number]	k4	125
<g/>
.	.	kIx.	.
</s>
<s>
Dětské	dětský	k2eAgInPc1d1	dětský
rámy	rám	k1gInPc1	rám
mají	mít	k5eAaImIp3nP	mít
kolečka	kolečko	k1gNnPc4	kolečko
80	[number]	k4	80
-	-	kIx~	-
100	[number]	k4	100
<g/>
mm	mm	kA	mm
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
pouze	pouze	k6eAd1	pouze
3	[number]	k4	3
kolečka	kolečko	k1gNnSc2	kolečko
velikosti	velikost	k1gFnSc2	velikost
100	[number]	k4	100
<g/>
-	-	kIx~	-
<g/>
110	[number]	k4	110
<g/>
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Běhouny	běhoun	k1gInPc4	běhoun
koleček	kolečko	k1gNnPc2	kolečko
jsou	být	k5eAaImIp3nP	být
vyráběny	vyrábět	k5eAaImNgInP	vyrábět
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
či	či	k8xC	či
více	hodně	k6eAd2	hodně
vrstev	vrstva	k1gFnPc2	vrstva
polyurethanu	polyurethanout	k5eAaPmIp1nS	polyurethanout
<g/>
,	,	kIx,	,
polymeru	polymer	k1gInSc3	polymer
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
pevnosti	pevnost	k1gFnSc3	pevnost
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
lehkosti	lehkost	k1gFnPc1	lehkost
<g/>
,	,	kIx,	,
ideální	ideální	k2eAgFnPc1d1	ideální
jízdní	jízdní	k2eAgFnPc1d1	jízdní
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Středy	středa	k1gFnPc1	středa
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
plastu	plast	k1gInSc2	plast
a	a	k8xC	a
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
ve	v	k7c6	v
velikosti	velikost	k1gFnSc6	velikost
125	[number]	k4	125
začaly	začít	k5eAaPmAgInP	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
i	i	k9	i
hliníkové	hliníkový	k2eAgInPc1d1	hliníkový
středy	střed	k1gInPc1	střed
-	-	kIx~	-
BONT	BONT	kA	BONT
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
používalo	používat	k5eAaImAgNnS	používat
5	[number]	k4	5
koleček	kolečko	k1gNnPc2	kolečko
menší	malý	k2eAgFnSc2d2	menší
velikosti	velikost	k1gFnSc2	velikost
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
84	[number]	k4	84
<g/>
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
delších	dlouhý	k2eAgInPc2d2	delší
závodů	závod	k1gInPc2	závod
(	(	kIx(	(
<g/>
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
Le	Le	k1gFnSc1	Le
Mans	Mans	k1gInSc1	Mans
na	na	k7c6	na
bruslích	brusle	k1gFnPc6	brusle
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
často	často	k6eAd1	často
používají	používat	k5eAaImIp3nP	používat
takzvané	takzvaný	k2eAgNnSc4d1	takzvané
marathon	marathon	k1gNnSc4	marathon
brusle	brusle	k1gFnSc2	brusle
(	(	kIx(	(
<g/>
také	také	k9	také
přezdívané	přezdívaný	k2eAgFnPc1d1	přezdívaná
maratonky	maratonka	k1gFnPc1	maratonka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
rychlobrusle	rychlobrusle	k6eAd1	rychlobrusle
s	s	k7c7	s
mírně	mírně	k6eAd1	mírně
zvýšeným	zvýšený	k2eAgInSc7d1	zvýšený
kotníkem	kotník	k1gInSc7	kotník
a	a	k8xC	a
více	hodně	k6eAd2	hodně
polstrovaná	polstrovaný	k2eAgFnSc1d1	polstrovaná
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
částečně	částečně	k6eAd1	částečně
omezuje	omezovat	k5eAaImIp3nS	omezovat
závodní	závodní	k2eAgFnSc4d1	závodní
techniku	technika	k1gFnSc4	technika
<g/>
,	,	kIx,	,
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
komfort	komfort	k1gInSc4	komfort
závodníků	závodník	k1gMnPc2	závodník
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
tak	tak	k6eAd1	tak
absolvovat	absolvovat	k5eAaPmF	absolvovat
i	i	k9	i
přes	přes	k7c4	přes
400	[number]	k4	400
<g/>
km	km	kA	km
za	za	k7c4	za
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
fitness	fitness	k6eAd1	fitness
<g/>
;	;	kIx,	;
Rekreační	rekreační	k2eAgMnSc1d1	rekreační
a	a	k8xC	a
fitness	fitness	k1gInSc1	fitness
kategorie	kategorie	k1gFnSc2	kategorie
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
splývají	splývat	k5eAaImIp3nP	splývat
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
především	především	k9	především
o	o	k7c4	o
pohodlné	pohodlný	k2eAgFnPc4d1	pohodlná
brusle	brusle	k1gFnPc4	brusle
na	na	k7c4	na
kondiční	kondiční	k2eAgFnSc4d1	kondiční
jízdu	jízda	k1gFnSc4	jízda
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
výrobci	výrobce	k1gMnPc1	výrobce
již	již	k6eAd1	již
používají	používat	k5eAaImIp3nP	používat
velká	velký	k2eAgNnPc4d1	velké
kolečka	kolečko	k1gNnPc4	kolečko
i	i	k9	i
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
kategorii	kategorie	k1gFnSc6	kategorie
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
výhodné	výhodný	k2eAgNnSc1d1	výhodné
pro	pro	k7c4	pro
překonávání	překonávání	k1gNnSc4	překonávání
drobných	drobný	k2eAgFnPc2d1	drobná
nerovností	nerovnost	k1gFnPc2	nerovnost
<g/>
.	.	kIx.	.
</s>
<s>
Brusle	brusle	k1gFnSc1	brusle
je	být	k5eAaImIp3nS	být
vybavena	vybavit	k5eAaPmNgFnS	vybavit
patní	patní	k2eAgFnSc7d1	patní
brzdou	brzda	k1gFnSc7	brzda
<g/>
.	.	kIx.	.
hokejové	hokejový	k2eAgFnPc4d1	hokejová
<g/>
;	;	kIx,	;
Tyto	tento	k3xDgFnPc1	tento
brusle	brusle	k1gFnPc1	brusle
vycházejí	vycházet	k5eAaImIp3nP	vycházet
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
lední	lední	k2eAgFnSc2d1	lední
varianty	varianta	k1gFnSc2	varianta
<g/>
,	,	kIx,	,
kolečka	kolečko	k1gNnPc4	kolečko
jsou	být	k5eAaImIp3nP	být
menší	malý	k2eAgInPc1d2	menší
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
mají	mít	k5eAaImIp3nP	mít
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
rockering	rockering	k1gInSc1	rockering
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
houpací	houpací	k2eAgInSc1d1	houpací
rám	rám	k1gInSc1	rám
<g/>
)	)	kIx)	)
který	který	k3yIgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
rychlé	rychlý	k2eAgFnPc4d1	rychlá
změny	změna	k1gFnPc4	změna
směru	směr	k1gInSc2	směr
<g/>
.	.	kIx.	.
freestyle	freestyl	k1gInSc5	freestyl
<g/>
;	;	kIx,	;
Tyto	tento	k3xDgFnPc1	tento
brusle	brusle	k1gFnPc1	brusle
jsou	být	k5eAaImIp3nP	být
typické	typický	k2eAgNnSc1d1	typické
krátkým	krátký	k2eAgInSc7d1	krátký
rámem	rám	k1gInSc7	rám
a	a	k8xC	a
menšími	malý	k2eAgInPc7d2	menší
kolečky	koleček	k1gInPc7	koleček
<g/>
.	.	kIx.	.
</s>
<s>
Brusle	brusle	k1gFnSc1	brusle
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
slalom	slalom	k1gInSc4	slalom
a	a	k8xC	a
ježdění	ježdění	k1gNnSc4	ježdění
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
zástavbě	zástavba	k1gFnSc6	zástavba
<g/>
.	.	kIx.	.
agresivní	agresivní	k2eAgMnSc1d1	agresivní
<g/>
;	;	kIx,	;
Brusle	brusle	k1gFnPc1	brusle
jsou	být	k5eAaImIp3nP	být
vhodné	vhodný	k2eAgFnPc1d1	vhodná
na	na	k7c4	na
skákání	skákání	k1gNnSc4	skákání
a	a	k8xC	a
provádění	provádění	k1gNnSc4	provádění
různých	různý	k2eAgInPc2d1	různý
akrobatických	akrobatický	k2eAgInPc2d1	akrobatický
triků	trik	k1gInPc2	trik
na	na	k7c6	na
překážkách	překážka	k1gFnPc6	překážka
ve	v	k7c6	v
skateparku	skatepark	k1gInSc6	skatepark
<g/>
,	,	kIx,	,
rampě	rampa	k1gFnSc6	rampa
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
co	co	k9	co
nabízí	nabízet	k5eAaImIp3nS	nabízet
městská	městský	k2eAgFnSc1d1	městská
výstavba	výstavba	k1gFnSc1	výstavba
<g/>
.	.	kIx.	.
</s>
<s>
Brusle	brusle	k1gFnPc1	brusle
se	se	k3xPyFc4	se
poznají	poznat	k5eAaPmIp3nP	poznat
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
nízkého	nízký	k2eAgInSc2d1	nízký
odolného	odolný	k2eAgInSc2d1	odolný
rámu	rám	k1gInSc2	rám
a	a	k8xC	a
menšího	malý	k2eAgInSc2d2	menší
průměru	průměr	k1gInSc2	průměr
koleček	koleček	k1gInSc1	koleček
<g/>
.	.	kIx.	.
</s>
<s>
Nízký	nízký	k2eAgInSc1d1	nízký
rám	rám	k1gInSc1	rám
z	z	k7c2	z
odolného	odolný	k2eAgInSc2d1	odolný
plastu	plast	k1gInSc2	plast
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
nejčastěji	často	k6eAd3	často
zástavbu	zástavba	k1gFnSc4	zástavba
dvou	dva	k4xCgNnPc2	dva
až	až	k9	až
čtyř	čtyři	k4xCgNnPc2	čtyři
koleček	kolečko	k1gNnPc2	kolečko
<g/>
.	.	kIx.	.
</s>
<s>
Rám	rám	k1gInSc1	rám
je	být	k5eAaImIp3nS	být
zesílený	zesílený	k2eAgInSc1d1	zesílený
aby	aby	kYmCp3nS	aby
vydržel	vydržet	k5eAaPmAgInS	vydržet
namáhání	namáhání	k1gNnSc4	namáhání
při	při	k7c6	při
dopadu	dopad	k1gInSc6	dopad
a	a	k8xC	a
odírání	odírání	k1gNnSc1	odírání
o	o	k7c4	o
překážky	překážka	k1gFnPc4	překážka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středu	střed	k1gInSc6	střed
je	být	k5eAaImIp3nS	být
tvarovaný	tvarovaný	k2eAgInSc1d1	tvarovaný
pro	pro	k7c4	pro
lepší	dobrý	k2eAgNnSc4d2	lepší
klouzání	klouzání	k1gNnSc4	klouzání
po	po	k7c6	po
zábradlí	zábradlí	k1gNnSc6	zábradlí
<g/>
.	.	kIx.	.
</s>
<s>
Kolečka	kolečko	k1gNnPc1	kolečko
malého	malý	k2eAgInSc2d1	malý
průměru	průměr	k1gInSc2	průměr
od	od	k7c2	od
55	[number]	k4	55
<g/>
mm	mm	kA	mm
do	do	k7c2	do
64	[number]	k4	64
<g/>
mm	mm	kA	mm
vyšší	vysoký	k2eAgFnSc2d2	vyšší
tvrdosti	tvrdost	k1gFnSc2	tvrdost
a	a	k8xC	a
tupého	tupý	k2eAgInSc2d1	tupý
profilu	profil	k1gInSc2	profil
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
pár	pár	k1gInSc1	pár
bývá	bývat	k5eAaImIp3nS	bývat
nahrazen	nahradit	k5eAaPmNgInS	nahradit
někdy	někdy	k6eAd1	někdy
kolečky	koleček	k1gInPc4	koleček
menšího	malý	k2eAgInSc2d2	menší
profilu	profil	k1gInSc2	profil
a	a	k8xC	a
jiného	jiný	k2eAgInSc2d1	jiný
tvrdšího	tvrdý	k2eAgInSc2d2	tvrdší
plastového	plastový	k2eAgInSc2d1	plastový
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Nižší	nízký	k2eAgFnSc1d2	nižší
bota	bota	k1gFnSc1	bota
<g/>
,	,	kIx,	,
celoplastové	celoplastový	k2eAgFnPc1d1	celoplastová
konstrukce	konstrukce	k1gFnPc1	konstrukce
s	s	k7c7	s
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
měkkou	měkký	k2eAgFnSc7d1	měkká
vložkou	vložka	k1gFnSc7	vložka
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
měkká	měkký	k2eAgFnSc1d1	měkká
bota	bota	k1gFnSc1	bota
opatřená	opatřený	k2eAgFnSc1d1	opatřená
jen	jen	k6eAd1	jen
zesílením	zesílení	k1gNnSc7	zesílení
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
kotníku	kotník	k1gInSc2	kotník
a	a	k8xC	a
špičky	špička	k1gFnSc2	špička
<g/>
.	.	kIx.	.
</s>
<s>
Bota	bota	k1gFnSc1	bota
je	být	k5eAaImIp3nS	být
opatřená	opatřený	k2eAgFnSc1d1	opatřená
zesílením	zesílení	k1gNnSc7	zesílení
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrozí	hrozit	k5eAaImIp3nS	hrozit
prodření	prodření	k1gNnSc4	prodření
o	o	k7c4	o
překážky	překážka	k1gFnPc4	překážka
<g/>
.	.	kIx.	.
</s>
<s>
Vázání	vázání	k1gNnSc1	vázání
je	být	k5eAaImIp3nS	být
používáno	používat	k5eAaImNgNnS	používat
jak	jak	k6eAd1	jak
na	na	k7c4	na
tkaničky	tkanička	k1gFnPc4	tkanička
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
přezkou	přezka	k1gFnSc7	přezka
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
vybavení	vybavení	k1gNnSc2	vybavení
bruslí	bruslit	k5eAaImIp3nS	bruslit
pro	pro	k7c4	pro
agressiv-inline	agressivnlin	k1gInSc5	agressiv-inlin
bývá	bývat	k5eAaImIp3nS	bývat
grindplate	grindplat	k1gMnSc5	grindplat
a	a	k8xC	a
soulplate	soulplat	k1gMnSc5	soulplat
<g/>
.	.	kIx.	.
</s>
<s>
Což	což	k9	což
jsou	být	k5eAaImIp3nP	být
výměnné	výměnný	k2eAgFnPc4d1	výměnná
ochranné	ochranný	k2eAgFnPc4d1	ochranná
součásti	součást	k1gFnPc4	součást
boty	bota	k1gFnSc2	bota
a	a	k8xC	a
rámu	rám	k1gInSc2	rám
lépe	dobře	k6eAd2	dobře
umožňující	umožňující	k2eAgFnSc1d1	umožňující
provádět	provádět	k5eAaImF	provádět
triky	trik	k1gInPc4	trik
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
spočívání	spočívání	k1gNnSc1	spočívání
v	v	k7c6	v
nějak	nějak	k6eAd1	nějak
provedeném	provedený	k2eAgInSc6d1	provedený
skluzu	skluz	k1gInSc6	skluz
po	po	k7c6	po
překážce	překážka	k1gFnSc6	překážka
<g/>
.	.	kIx.	.
</s>
<s>
Kterou	který	k3yRgFnSc4	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zábradlí	zábradlí	k1gNnSc4	zábradlí
,	,	kIx,	,
nebo	nebo	k8xC	nebo
hrana	hrana	k1gFnSc1	hrana
nějaké	nějaký	k3yIgFnSc2	nějaký
překážky	překážka	k1gFnSc2	překážka
<g/>
.	.	kIx.	.
speciální	speciální	k2eAgFnPc4d1	speciální
<g/>
;	;	kIx,	;
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
kategorie	kategorie	k1gFnSc2	kategorie
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nP	řadit
ostatní	ostatní	k2eAgFnPc1d1	ostatní
brusle	brusle	k1gFnPc1	brusle
pro	pro	k7c4	pro
méně	málo	k6eAd2	málo
rozšířené	rozšířený	k2eAgFnPc4d1	rozšířená
disciplíny	disciplína	k1gFnPc4	disciplína
jako	jako	k8xC	jako
sjezd	sjezd	k1gInSc4	sjezd
<g/>
,	,	kIx,	,
alpine	alpinout	k5eAaPmIp3nS	alpinout
slalom	slalom	k1gInSc1	slalom
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Brusle	brusle	k1gFnSc1	brusle
na	na	k7c4	na
inline	inlinout	k5eAaPmIp3nS	inlinout
slalom	slalom	k1gInSc1	slalom
mají	mít	k5eAaImIp3nP	mít
delší	dlouhý	k2eAgInSc4d2	delší
rám	rám	k1gInSc4	rám
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
je	být	k5eAaImIp3nS	být
připevněno	připevnit	k5eAaPmNgNnS	připevnit
5	[number]	k4	5
menších	malý	k2eAgFnPc2d2	menší
koleček	kolečko	k1gNnPc2	kolečko
velikosti	velikost	k1gFnSc2	velikost
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
84	[number]	k4	84
<g/>
mm	mm	kA	mm
<g/>
,	,	kIx,	,
bota	bota	k1gFnSc1	bota
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
,	,	kIx,	,
podobná	podobný	k2eAgFnSc1d1	podobná
botám	bota	k1gFnPc3	bota
na	na	k7c4	na
sjezd	sjezd	k1gInSc4	sjezd
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc4	několik
přezek	přezka	k1gFnPc2	přezka
pro	pro	k7c4	pro
maximální	maximální	k2eAgFnSc4d1	maximální
pevnost	pevnost	k1gFnSc4	pevnost
<g/>
.	.	kIx.	.
</s>
