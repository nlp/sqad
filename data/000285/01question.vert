<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
kolečkové	kolečkový	k2eAgFnPc1d1	kolečková
brusle	brusle	k1gFnPc1	brusle
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mají	mít	k5eAaImIp3nP	mít
kolečka	kolečko	k1gNnPc4	kolečko
umístěná	umístěný	k2eAgNnPc4d1	umístěné
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
řadě	řada	k1gFnSc6	řada
za	za	k7c7	za
sebou	se	k3xPyFc7	se
<g/>
?	?	kIx.	?
</s>
