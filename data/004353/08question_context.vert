<s>
Miloš	Miloš	k1gMnSc1	Miloš
Forman	Forman	k1gMnSc1	Forman
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
Jan	Jan	k1gMnSc1	Jan
Tomáš	Tomáš	k1gMnSc1	Tomáš
Forman	Forman	k1gMnSc1	Forman
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
*	*	kIx~	*
18	[number]	k4	18
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1932	[number]	k4	1932
Čáslav	Čáslav	k1gFnSc1	Čáslav
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
a	a	k8xC	a
herec	herec	k1gMnSc1	herec
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
žijící	žijící	k2eAgMnPc1d1	žijící
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
držitelem	držitel	k1gMnSc7	držitel
dvou	dva	k4xCgFnPc2	dva
Oscarů	Oscar	k1gInPc2	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
režii	režie	k1gFnSc4	režie
<g/>
,	,	kIx,	,
tří	tři	k4xCgInPc2	tři
Zlatých	zlatý	k2eAgInPc2d1	zlatý
glóbů	glóbus	k1gInPc2	glóbus
a	a	k8xC	a
ceny	cena	k1gFnSc2	cena
BAFTA	BAFTA	kA	BAFTA
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
kategorii	kategorie	k1gFnSc6	kategorie
<g/>
.	.	kIx.	.
</s>
