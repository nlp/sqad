<s>
Je	být	k5eAaImIp3nS	být
držitelem	držitel	k1gMnSc7	držitel
dvou	dva	k4xCgFnPc2	dva
Oscarů	Oscar	k1gInPc2	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
režii	režie	k1gFnSc4	režie
<g/>
,	,	kIx,	,
tří	tři	k4xCgInPc2	tři
Zlatých	zlatý	k2eAgInPc2d1	zlatý
glóbů	glóbus	k1gInPc2	glóbus
a	a	k8xC	a
ceny	cena	k1gFnSc2	cena
BAFTA	BAFTA	kA	BAFTA
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
kategorii	kategorie	k1gFnSc6	kategorie
<g/>
.	.	kIx.	.
</s>
