<s>
Český	český	k2eAgInSc1d1	český
svaz	svaz	k1gInSc1	svaz
tanečního	taneční	k2eAgInSc2d1	taneční
sportu	sport	k1gInSc2	sport
(	(	kIx(	(
<g/>
ČSTS	ČSTS	kA	ČSTS
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Czech	Czech	k1gInSc4	Czech
Dance	Danka	k1gFnSc3	Danka
Sport	sport	k1gInSc1	sport
Federation	Federation	k1gInSc1	Federation
<g/>
,	,	kIx,	,
CDSF	CDSF	kA	CDSF
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
občanské	občanský	k2eAgNnSc1d1	občanské
sdružení	sdružení	k1gNnSc1	sdružení
tanečníků	tanečník	k1gMnPc2	tanečník
a	a	k8xC	a
tanečních	taneční	k2eAgMnPc2d1	taneční
funkcionářů	funkcionář	k1gMnPc2	funkcionář
z	z	k7c2	z
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
