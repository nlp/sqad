<p>
<s>
Český	český	k2eAgInSc1d1	český
svaz	svaz	k1gInSc1	svaz
tanečního	taneční	k2eAgInSc2d1	taneční
sportu	sport	k1gInSc2	sport
(	(	kIx(	(
<g/>
ČSTS	ČSTS	kA	ČSTS
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Czech	Czech	k1gInSc4	Czech
Dance	Danka	k1gFnSc3	Danka
Sport	sport	k1gInSc1	sport
Federation	Federation	k1gInSc1	Federation
<g/>
,	,	kIx,	,
CDSF	CDSF	kA	CDSF
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
občanské	občanský	k2eAgNnSc1d1	občanské
sdružení	sdružení	k1gNnSc1	sdružení
tanečníků	tanečník	k1gMnPc2	tanečník
a	a	k8xC	a
tanečních	taneční	k2eAgMnPc2d1	taneční
funkcionářů	funkcionář	k1gMnPc2	funkcionář
z	z	k7c2	z
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nepolitickou	politický	k2eNgFnSc4d1	nepolitická
a	a	k8xC	a
neziskovou	ziskový	k2eNgFnSc4d1	nezisková
zájmovou	zájmový	k2eAgFnSc4d1	zájmová
společenskou	společenský	k2eAgFnSc4d1	společenská
organizaci	organizace	k1gFnSc4	organizace
respektive	respektive	k9	respektive
spolek	spolek	k1gInSc4	spolek
ustavený	ustavený	k2eAgInSc4d1	ustavený
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
platných	platný	k2eAgFnPc2d1	platná
právních	právní	k2eAgFnPc2d1	právní
norem	norma	k1gFnPc2	norma
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
předmětem	předmět	k1gInSc7	předmět
činnosti	činnost	k1gFnSc2	činnost
je	být	k5eAaImIp3nS	být
taneční	taneční	k2eAgInSc1d1	taneční
sport	sport	k1gInSc1	sport
prováděný	prováděný	k2eAgInSc1d1	prováděný
jak	jak	k6eAd1	jak
na	na	k7c4	na
rekreační	rekreační	k2eAgNnPc4d1	rekreační
tak	tak	k8xC	tak
i	i	k9	i
na	na	k7c6	na
výkonnostní	výkonnostní	k2eAgFnSc6d1	výkonnostní
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
sídlo	sídlo	k1gNnSc1	sídlo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Ústí	ústí	k1gNnSc6	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Činnost	činnost	k1gFnSc4	činnost
svazu	svaz	k1gInSc2	svaz
==	==	k?	==
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
činnost	činnost	k1gFnSc1	činnost
spadá	spadat	k5eAaImIp3nS	spadat
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
zájmové	zájmový	k2eAgFnSc2d1	zájmová
umělecké	umělecký	k2eAgFnSc2d1	umělecká
činnosti	činnost	k1gFnSc2	činnost
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
společenského	společenský	k2eAgInSc2d1	společenský
tance	tanec	k1gInSc2	tanec
<g/>
,	,	kIx,	,
tuto	tento	k3xDgFnSc4	tento
činnost	činnost	k1gFnSc4	činnost
dále	daleko	k6eAd2	daleko
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
a	a	k8xC	a
přetváří	přetvářet	k5eAaImIp3nS	přetvářet
na	na	k7c4	na
speciální	speciální	k2eAgNnPc4d1	speciální
sportovní	sportovní	k2eAgNnPc4d1	sportovní
odvětví	odvětví	k1gNnPc4	odvětví
-	-	kIx~	-
taneční	taneční	k2eAgInSc1d1	taneční
sport	sport	k1gInSc1	sport
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sportovní	sportovní	k2eAgFnSc6d1	sportovní
oblasti	oblast	k1gFnSc6	oblast
organizuje	organizovat	k5eAaBmIp3nS	organizovat
pravidelnou	pravidelný	k2eAgFnSc4d1	pravidelná
soutěžní	soutěžní	k2eAgFnSc4d1	soutěžní
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
provádí	provádět	k5eAaImIp3nS	provádět
jeho	jeho	k3xOp3gFnSc4	jeho
metodickou	metodický	k2eAgFnSc4d1	metodická
<g/>
,	,	kIx,	,
organizační	organizační	k2eAgFnSc4d1	organizační
i	i	k8xC	i
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
podporu	podpora	k1gFnSc4	podpora
<g/>
,	,	kIx,	,
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
vytvářet	vytvářet	k5eAaImF	vytvářet
vhodné	vhodný	k2eAgNnSc4d1	vhodné
prostředí	prostředí	k1gNnSc4	prostředí
pro	pro	k7c4	pro
masové	masový	k2eAgNnSc4d1	masové
provozování	provozování	k1gNnSc4	provozování
rekreačního	rekreační	k2eAgInSc2d1	rekreační
tanečního	taneční	k2eAgInSc2d1	taneční
sportu	sport	k1gInSc2	sport
<g/>
,	,	kIx,	,
usiluje	usilovat	k5eAaImIp3nS	usilovat
o	o	k7c4	o
dosažení	dosažení	k1gNnSc4	dosažení
vyšší	vysoký	k2eAgFnSc2d2	vyšší
sportovní	sportovní	k2eAgFnSc2d1	sportovní
a	a	k8xC	a
umělecké	umělecký	k2eAgFnSc2d1	umělecká
úrovně	úroveň	k1gFnSc2	úroveň
nejlepších	dobrý	k2eAgMnPc2d3	nejlepší
tanečních	taneční	k2eAgMnPc2d1	taneční
sportovců	sportovec	k1gMnPc2	sportovec
a	a	k8xC	a
i	i	k9	i
tanečních	taneční	k2eAgInPc2d1	taneční
kolektivů	kolektiv	k1gInPc2	kolektiv
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
zde	zde	k6eAd1	zde
o	o	k7c4	o
co	co	k3yRnSc4	co
nejširší	široký	k2eAgNnSc1d3	nejširší
zapojení	zapojení	k1gNnSc3	zapojení
zájemců	zájemce	k1gMnPc2	zájemce
o	o	k7c4	o
společenský	společenský	k2eAgInSc4d1	společenský
tanec	tanec	k1gInSc4	tanec
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
věkových	věkový	k2eAgFnPc2d1	věková
vrstev	vrstva	k1gFnPc2	vrstva
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
malé	malý	k2eAgMnPc4d1	malý
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
juniory	junior	k1gMnPc4	junior
tak	tak	k9	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
sport	sport	k1gInSc4	sport
vytvořena	vytvořen	k2eAgFnSc1d1	vytvořena
co	co	k9	co
nejširší	široký	k2eAgFnSc1d3	nejširší
sportovní	sportovní	k2eAgFnSc1d1	sportovní
personální	personální	k2eAgFnSc1d1	personální
základna	základna	k1gFnSc1	základna
<g/>
.	.	kIx.	.
</s>
<s>
Svaz	svaz	k1gInSc1	svaz
se	se	k3xPyFc4	se
také	také	k9	také
snaží	snažit	k5eAaImIp3nP	snažit
český	český	k2eAgInSc4d1	český
taneční	taneční	k2eAgInSc4d1	taneční
sport	sport	k1gInSc4	sport
pozdvihnout	pozdvihnout	k5eAaPmF	pozdvihnout
na	na	k7c4	na
srovnatelnou	srovnatelný	k2eAgFnSc4d1	srovnatelná
úroveň	úroveň	k1gFnSc4	úroveň
v	v	k7c6	v
širším	široký	k2eAgNnSc6d2	širší
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
měřítku	měřítko	k1gNnSc6	měřítko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Taneční	taneční	k2eAgInSc1d1	taneční
sport	sport	k1gInSc1	sport
</s>
</p>
<p>
<s>
Světová	světový	k2eAgFnSc1d1	světová
federace	federace	k1gFnSc1	federace
tanečního	taneční	k2eAgInSc2d1	taneční
sportu	sport	k1gInSc2	sport
(	(	kIx(	(
<g/>
WDSF	WDSF	kA	WDSF
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
ČSTS	ČSTS	kA	ČSTS
</s>
</p>
<p>
<s>
Informace	informace	k1gFnPc1	informace
o	o	k7c6	o
organizaci	organizace	k1gFnSc6	organizace
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Statutárního	statutární	k2eAgNnSc2d1	statutární
města	město	k1gNnSc2	město
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
</s>
</p>
