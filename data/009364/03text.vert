<p>
<s>
Dověcnosti	dověcnost	k1gFnPc4	dověcnost
je	být	k5eAaImIp3nS	být
první	první	k4xOgNnSc1	první
album	album	k1gNnSc1	album
skupiny	skupina	k1gFnSc2	skupina
Čechomor	Čechomora	k1gFnPc2	Čechomora
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
ho	on	k3xPp3gInSc4	on
vydala	vydat	k5eAaPmAgFnS	vydat
ještě	ještě	k6eAd1	ještě
pod	pod	k7c7	pod
prvním	první	k4xOgInSc7	první
názvem	název	k1gInSc7	název
1	[number]	k4	1
<g/>
.	.	kIx.	.
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
českomoravská	českomoravský	k2eAgFnSc1d1	Českomoravská
hudební	hudební	k2eAgFnSc1d1	hudební
společnost	společnost	k1gFnSc1	společnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
albu	album	k1gNnSc6	album
je	být	k5eAaImIp3nS	být
celkem	celkem	k6eAd1	celkem
18	[number]	k4	18
lidových	lidový	k2eAgFnPc2d1	lidová
písní	píseň	k1gFnPc2	píseň
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
skupina	skupina	k1gFnSc1	skupina
vybrala	vybrat	k5eAaPmAgFnS	vybrat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nebyly	být	k5eNaImAgFnP	být
notoricky	notoricky	k6eAd1	notoricky
známé	známý	k2eAgFnPc1d1	známá
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
společností	společnost	k1gFnPc2	společnost
Globus	globus	k1gInSc4	globus
Internacional	Internacional	k1gFnSc4	Internacional
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nahrání	nahrání	k1gNnSc6	nahrání
alba	album	k1gNnSc2	album
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
skupina	skupina	k1gFnSc1	skupina
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ukázka	ukázka	k1gFnSc1	ukázka
celého	celý	k2eAgNnSc2d1	celé
alba	album	k1gNnSc2	album
–	–	k?	–
Nižší	nízký	k2eAgFnSc1d2	nižší
kvalita	kvalita	k1gFnSc1	kvalita
<g/>
,	,	kIx,	,
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
kvalita	kvalita	k1gFnSc1	kvalita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Břenek	Břenek	k1gMnSc1	Břenek
–	–	k?	–
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
zpěv	zpěv	k1gInSc4	zpěv
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Černý	Černý	k1gMnSc1	Černý
–	–	k?	–
kytara	kytara	k1gFnSc1	kytara
a	a	k8xC	a
zpěv	zpěv	k1gInSc1	zpěv
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Michálek	Michálek	k1gMnSc1	Michálek
–	–	k?	–
harmonika	harmonika	k1gFnSc1	harmonika
</s>
</p>
<p>
<s>
Jiří	Jiří	k1gMnSc1	Jiří
Hodina	hodina	k1gFnSc1	hodina
–	–	k?	–
housle	housle	k1gFnPc4	housle
a	a	k8xC	a
zpěv	zpěv	k1gInSc4	zpěv
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
"	"	kIx"	"
<g/>
Olin	Olin	k2eAgMnSc1d1	Olin
<g/>
"	"	kIx"	"
Nejezchleba	Nejezchleb	k1gMnSc4	Nejezchleb
–	–	k?	–
violoncello	violoncello	k1gNnSc4	violoncello
a	a	k8xC	a
zpěv	zpěv	k1gInSc4	zpěv
</s>
</p>
<p>
<s>
Radek	Radek	k1gMnSc1	Radek
Pobořil	pobořit	k5eAaPmAgMnS	pobořit
–	–	k?	–
trubka	trubka	k1gFnSc1	trubka
<g/>
,	,	kIx,	,
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
jako	jako	k9	jako
host	host	k1gMnSc1	host
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc1	seznam
stop	stopa	k1gFnPc2	stopa
==	==	k?	==
</s>
</p>
<p>
<s>
Nedověcnosti	nedověcnost	k1gFnPc1	nedověcnost
(	(	kIx(	(
<g/>
ukázka	ukázka	k1gFnSc1	ukázka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rodičovská	rodičovský	k2eAgFnSc1d1	rodičovská
lítost	lítost	k1gFnSc1	lítost
(	(	kIx(	(
<g/>
ukázka	ukázka	k1gFnSc1	ukázka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Výmluva	výmluva	k1gFnSc1	výmluva
(	(	kIx(	(
<g/>
ukázka	ukázka	k1gFnSc1	ukázka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nectěná	ctěný	k2eNgFnSc1d1	ctěný
(	(	kIx(	(
<g/>
ukázka	ukázka	k1gFnSc1	ukázka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dudák	dudák	k1gInSc1	dudák
(	(	kIx(	(
<g/>
ukázka	ukázka	k1gFnSc1	ukázka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Odvod	odvod	k1gInSc1	odvod
(	(	kIx(	(
<g/>
ukázka	ukázka	k1gFnSc1	ukázka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Voják	voják	k1gMnSc1	voják
(	(	kIx(	(
<g/>
ukázka	ukázka	k1gFnSc1	ukázka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hosti	host	k1gMnPc1	host
(	(	kIx(	(
<g/>
ukázka	ukázka	k1gFnSc1	ukázka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tužba	tužba	k1gFnSc1	tužba
(	(	kIx(	(
<g/>
ukázka	ukázka	k1gFnSc1	ukázka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Žádba	Žádba	k1gFnSc1	Žádba
(	(	kIx(	(
<g/>
ukázka	ukázka	k1gFnSc1	ukázka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Otčenáš	otčenáš	k1gInSc1	otčenáš
(	(	kIx(	(
<g/>
ukázka	ukázka	k1gFnSc1	ukázka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Pití	pití	k1gNnSc1	pití
a	a	k8xC	a
jídlo	jídlo	k1gNnSc1	jídlo
(	(	kIx(	(
<g/>
ukázka	ukázka	k1gFnSc1	ukázka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Příčina	příčina	k1gFnSc1	příčina
pláče	pláč	k1gInSc2	pláč
(	(	kIx(	(
<g/>
ukázka	ukázka	k1gFnSc1	ukázka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Utopenec	utopenec	k1gMnSc1	utopenec
(	(	kIx(	(
<g/>
ukázka	ukázka	k1gFnSc1	ukázka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nepolepšený	polepšený	k2eNgInSc1d1	polepšený
(	(	kIx(	(
<g/>
ukázka	ukázka	k1gFnSc1	ukázka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Odpudivá	odpudivý	k2eAgFnSc1d1	odpudivá
(	(	kIx(	(
<g/>
ukázka	ukázka	k1gFnSc1	ukázka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Husy	husa	k1gFnPc1	husa
(	(	kIx(	(
<g/>
ukázka	ukázka	k1gFnSc1	ukázka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Žádba	Žádba	k1gFnSc1	Žádba
</s>
</p>
