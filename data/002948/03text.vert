<s>
V	v	k7c6	v
matematice	matematika	k1gFnSc6	matematika
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
sjednocení	sjednocení	k1gNnSc4	sjednocení
dvou	dva	k4xCgInPc2	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
množin	množina	k1gFnPc2	množina
označuje	označovat	k5eAaImIp3nS	označovat
taková	takový	k3xDgFnSc1	takový
množina	množina	k1gFnSc1	množina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
každý	každý	k3xTgInSc4	každý
prvek	prvek	k1gInSc4	prvek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
alespoň	alespoň	k9	alespoň
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
ze	z	k7c2	z
sjednocovaných	sjednocovaný	k2eAgFnPc2d1	sjednocovaná
množin	množina	k1gFnPc2	množina
<g/>
,	,	kIx,	,
a	a	k8xC	a
žádné	žádný	k3yNgInPc1	žádný
další	další	k2eAgInPc1d1	další
prvky	prvek	k1gInPc1	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Sjednocení	sjednocení	k1gNnSc1	sjednocení
množin	množina	k1gFnPc2	množina
A	A	kA	A
a	a	k8xC	a
B	B	kA	B
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
symbolem	symbol	k1gInSc7	symbol
A	A	kA	A
∪	∪	k?	∪
B.	B.	kA	B.
Pro	pro	k7c4	pro
všechna	všechen	k3xTgFnSc1	všechen
x	x	k?	x
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
x	x	k?	x
∈	∈	k?	∈
A	a	k8xC	a
∪	∪	k?	∪
B	B	kA	B
právě	právě	k6eAd1	právě
když	když	k8xS	když
x	x	k?	x
∈	∈	k?	∈
A	A	kA	A
nebo	nebo	k8xC	nebo
x	x	k?	x
∈	∈	k?	∈
B.	B.	kA	B.
(	(	kIx(	(
<g/>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
matematické	matematický	k2eAgNnSc4d1	matematické
nebo	nebo	k8xC	nebo
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
prvek	prvek	k1gInSc1	prvek
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
sjednocení	sjednocení	k1gNnSc2	sjednocení
i	i	k9	i
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
množinách	množina	k1gFnPc6	množina
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
V	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c6	o
sjednocení	sjednocení	k1gNnSc6	sjednocení
více	hodně	k6eAd2	hodně
množin	množina	k1gFnPc2	množina
<g/>
,	,	kIx,	,
je	on	k3xPp3gFnPc4	on
možno	možno	k6eAd1	možno
je	být	k5eAaImIp3nS	být
chápat	chápat	k5eAaImF	chápat
jako	jako	k9	jako
několik	několik	k4yIc4	několik
postupných	postupný	k2eAgNnPc2d1	postupné
sjednocení	sjednocení	k1gNnPc2	sjednocení
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
asociativita	asociativita	k1gFnSc1	asociativita
níže	níže	k1gFnSc1	níže
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
prvek	prvek	k1gInSc1	prvek
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
sjednocení	sjednocení	k1gNnSc1	sjednocení
právě	právě	k6eAd1	právě
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
<g/>
-li	i	k?	-li
prvkem	prvek	k1gInSc7	prvek
alespoň	alespoň	k9	alespoň
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
množin	množina	k1gFnPc2	množina
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
pro	pro	k7c4	pro
sjednocení	sjednocení	k1gNnSc4	sjednocení
tří	tři	k4xCgFnPc2	tři
množin	množina	k1gFnPc2	množina
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
x	x	k?	x
je	být	k5eAaImIp3nS	být
prvkem	prvek	k1gInSc7	prvek
A	A	kA	A
∪	∪	k?	∪
B	B	kA	B
∪	∪	k?	∪
C	C	kA	C
iff	iff	k?	iff
x	x	k?	x
∈	∈	k?	∈
A	A	kA	A
nebo	nebo	k8xC	nebo
x	x	k?	x
∈	∈	k?	∈
B	B	kA	B
nebo	nebo	k8xC	nebo
x	x	k?	x
∈	∈	k?	∈
C.	C.	kA	C.
Sjednocení	sjednocení	k1gNnSc1	sjednocení
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
n	n	k0	n
<g/>
}	}	kIx)	}
množin	množina	k1gFnPc2	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
:	:	kIx,	:
A	A	kA	A
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
,	,	kIx,	,
.	.	kIx.	.
.	.	kIx.	.
.	.	kIx.	.
,	,	kIx,	,
:	:	kIx,	:
A	a	k8xC	a
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A_	A_	k1gMnSc6	A_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
A_	A_	k1gMnSc1	A_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
...	...	k?	...
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
,	,	kIx,	,
<g/>
A_	A_	k1gFnSc1	A_
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}}	}}	k?	}}
lze	lze	k6eAd1	lze
zkráceně	zkráceně	k6eAd1	zkráceně
psát	psát	k5eAaImF	psát
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	A	kA	A
:	:	kIx,	:
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
∪	∪	k?	∪
:	:	kIx,	:
A	A	kA	A
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
∪	∪	k?	∪
.	.	kIx.	.
.	.	kIx.	.
.	.	kIx.	.
∪	∪	k?	∪
:	:	kIx,	:
A	A	kA	A
:	:	kIx,	:
2	[number]	k4	2
:	:	kIx,	:
:	:	kIx,	:
=	=	kIx~	=
:	:	kIx,	:
∪	∪	k?	∪
:	:	kIx,	:
i	i	k9	i
=	=	kIx~	=
1	[number]	k4	1
:	:	kIx,	:
:	:	kIx,	:
n	n	k0	n
:	:	kIx,	:
<g />
.	.	kIx.	.
</s>
<s hack="1">
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
:	:	kIx,	:
i	i	k9	i
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A_	A_	k1gMnSc6	A_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cup	cup	k1gInSc1	cup
A_	A_	k1gFnSc2	A_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cup	cup	k1gInSc1	cup
...	...	k?	...
<g/>
\	\	kIx~	\
<g/>
cup	cup	k1gInSc1	cup
A_	A_	k1gFnSc2	A_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g />
.	.	kIx.	.
</s>
<s hack="1">
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
cup	cup	k1gInSc1	cup
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
n	n	k0	n
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
A_	A_	k1gFnSc1	A_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}}	}}}	k?	}}}
:	:	kIx,	:
Příklad	příklad	k1gInSc1	příklad
<g/>
:	:	kIx,	:
Sjednocením	sjednocení	k1gNnSc7	sjednocení
množin	množina	k1gFnPc2	množina
{	{	kIx(	{
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
,	,	kIx,	,
9	[number]	k4	9
}	}	kIx)	}
a	a	k8xC	a
{	{	kIx(	{
3	[number]	k4	3
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
,	,	kIx,	,
9	[number]	k4	9
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
množina	množina	k1gFnSc1	množina
{	{	kIx(	{
1	[number]	k4	1
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
,	,	kIx,	,
8	[number]	k4	8
<g/>
,	,	kIx,	,
9	[number]	k4	9
}	}	kIx)	}
<g/>
.	.	kIx.	.
</s>
<s>
Sjednocením	sjednocení	k1gNnSc7	sjednocení
množiny	množina	k1gFnSc2	množina
všech	všecek	k3xTgNnPc2	všecek
prvočísel	prvočíslo	k1gNnPc2	prvočíslo
{	{	kIx(	{
2	[number]	k4	2
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
,	,	kIx,	,
5	[number]	k4	5
<g/>
,	,	kIx,	,
7	[number]	k4	7
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
,	,	kIx,	,
...	...	k?	...
}	}	kIx)	}
s	s	k7c7	s
množinou	množina	k1gFnSc7	množina
všech	všecek	k3xTgNnPc2	všecek
sudých	sudý	k2eAgNnPc2d1	sudé
kladných	kladný	k2eAgNnPc2d1	kladné
čísel	číslo	k1gNnPc2	číslo
{	{	kIx(	{
2	[number]	k4	2
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
,	,	kIx,	,
...	...	k?	...
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
množina	množina	k1gFnSc1	množina
<g/>
,	,	kIx,	,
jejímiž	jejíž	k3xOyRp3gInPc7	jejíž
prvky	prvek	k1gInPc7	prvek
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
čísla	číslo	k1gNnSc2	číslo
17	[number]	k4	17
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
,	,	kIx,	,
19	[number]	k4	19
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepatří	patřit	k5eNaImIp3nS	patřit
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
např.	např.	kA	např.
čísla	číslo	k1gNnSc2	číslo
9	[number]	k4	9
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
,	,	kIx,	,
27	[number]	k4	27
<g/>
,	,	kIx,	,
63	[number]	k4	63
<g/>
,	,	kIx,	,
121	[number]	k4	121
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
obecná	obecný	k2eAgFnSc1d1	obecná
definice	definice	k1gFnSc1	definice
sjednocení	sjednocení	k1gNnSc2	sjednocení
nebo	nebo	k8xC	nebo
také	také	k9	také
sumy	suma	k1gFnPc4	suma
pro	pro	k7c4	pro
libovolnou	libovolný	k2eAgFnSc4d1	libovolná
(	(	kIx(	(
<g/>
i	i	k8xC	i
nekonečnou	konečný	k2eNgFnSc4d1	nekonečná
množinu	množina	k1gFnSc4	množina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
z	z	k7c2	z
axiomu	axiom	k1gInSc2	axiom
sumy	suma	k1gFnSc2	suma
a	a	k8xC	a
jak	jak	k6eAd1	jak
je	být	k5eAaImIp3nS	být
používána	používán	k2eAgFnSc1d1	používána
v	v	k7c6	v
teorii	teorie	k1gFnSc6	teorie
množin	množina	k1gFnPc2	množina
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
⋃	⋃	k?	⋃
A	A	kA	A
=	=	kIx~	=
{	{	kIx(	{
x	x	k?	x
:	:	kIx,	:
(	(	kIx(	(
∃	∃	k?	∃
y	y	k?	y
)	)	kIx)	)
(	(	kIx(	(
x	x	k?	x
∈	∈	k?	∈
y	y	k?	y
∧	∧	k?	∧
y	y	k?	y
<g />
.	.	kIx.	.
</s>
<s hack="1">
∈	∈	k?	∈
A	A	kA	A
)	)	kIx)	)
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
bigcup	bigcupit	k5eAaPmRp2nS	bigcupit
A	A	kA	A
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
:	:	kIx,	:
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
exists	existsit	k5eAaPmRp2nS	existsit
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
y	y	k?	y
<g/>
\	\	kIx~	\
<g/>
land	land	k1gMnSc1	land
y	y	k?	y
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
A	A	kA	A
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
:	:	kIx,	:
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
definice	definice	k1gFnSc2	definice
dostávám	dostávat	k5eAaImIp1nS	dostávat
speciálně	speciálně	k6eAd1	speciálně
pro	pro	k7c4	pro
dvouprvkovou	dvouprvkový	k2eAgFnSc4d1	dvouprvková
množinu	množina	k1gFnSc4	množina
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
=	=	kIx~	=
{	{	kIx(	{
b	b	k?	b
,	,	kIx,	,
c	c	k0	c
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
,	,	kIx,	,
<g/>
c	c	k0	c
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
klasické	klasický	k2eAgNnSc4d1	klasické
dvoumnožinové	dvoumnožinový	k2eAgNnSc4d1	dvoumnožinový
sjednocení	sjednocení	k1gNnSc4	sjednocení
<g/>
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
b	b	k?	b
∪	∪	k?	∪
c	c	k0	c
=	=	kIx~	=
⋃	⋃	k?	⋃
A	A	kA	A
=	=	kIx~	=
⋃	⋃	k?	⋃
{	{	kIx(	{
b	b	k?	b
,	,	kIx,	,
c	c	k0	c
}	}	kIx)	}
=	=	kIx~	=
{	{	kIx(	{
x	x	k?	x
:	:	kIx,	:
x	x	k?	x
∈	∈	k?	∈
b	b	k?	b
∨	∨	k?	∨
x	x	k?	x
∈	∈	k?	∈
c	c	k0	c
}	}	kIx)	}
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
cup	cup	k1gInSc1	cup
c	c	k0	c
<g/>
<g />
.	.	kIx.	.
</s>
<s hack="1">
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
bigcup	bigcup	k1gInSc1	bigcup
A	A	kA	A
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
bigcup	bigcup	k1gInSc1	bigcup
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
b	b	k?	b
<g/>
,	,	kIx,	,
<g/>
c	c	k0	c
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
:	:	kIx,	:
<g/>
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
b	b	k?	b
<g/>
\	\	kIx~	\
<g/>
vee	vee	k?	vee
x	x	k?	x
<g/>
\	\	kIx~	\
<g/>
in	in	k?	in
c	c	k0	c
<g/>
\	\	kIx~	\
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
}	}	kIx)	}
:	:	kIx,	:
Operace	operace	k1gFnSc1	operace
sjednocení	sjednocení	k1gNnSc1	sjednocení
dvou	dva	k4xCgFnPc2	dva
množin	množina	k1gFnPc2	množina
(	(	kIx(	(
<g/>
jakožto	jakožto	k8xS	jakožto
binární	binární	k2eAgFnSc2d1	binární
operace	operace	k1gFnSc2	operace
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
asociativní	asociativní	k2eAgMnSc1d1	asociativní
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
(	(	kIx(	(
<g/>
A	A	kA	A
∪	∪	k?	∪
B	B	kA	B
<g/>
)	)	kIx)	)
∪	∪	k?	∪
C	C	kA	C
=	=	kIx~	=
A	A	kA	A
∪	∪	k?	∪
(	(	kIx(	(
<g/>
B	B	kA	B
∪	∪	k?	∪
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Současné	současný	k2eAgNnSc1d1	současné
sjednocení	sjednocení	k1gNnSc1	sjednocení
všech	všecek	k3xTgFnPc2	všecek
množin	množina	k1gFnPc2	množina
-	-	kIx~	-
A	a	k9	a
∪	∪	k?	∪
B	B	kA	B
∪	∪	k?	∪
C	C	kA	C
-	-	kIx~	-
je	být	k5eAaImIp3nS	být
oběma	dva	k4xCgMnPc7	dva
těmto	tento	k3xDgNnPc3	tento
výrazům	výraz	k1gInPc3	výraz
rovno	roven	k2eAgNnSc1d1	rovno
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
psát	psát	k5eAaImF	psát
sjednocení	sjednocení	k1gNnSc4	sjednocení
libovolného	libovolný	k2eAgNnSc2d1	libovolné
množství	množství	k1gNnSc2	množství
množin	množina	k1gFnPc2	množina
bez	bez	k7c2	bez
použití	použití	k1gNnSc2	použití
závorek	závorka	k1gFnPc2	závorka
<g/>
.	.	kIx.	.
</s>
<s>
Sjednocení	sjednocení	k1gNnSc1	sjednocení
je	být	k5eAaImIp3nS	být
komutativní	komutativní	k2eAgFnPc4d1	komutativní
operace	operace	k1gFnPc4	operace
<g/>
,	,	kIx,	,
platí	platit	k5eAaImIp3nS	platit
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
že	že	k8xS	že
A	a	k9	a
∪	∪	k?	∪
B	B	kA	B
=	=	kIx~	=
B	B	kA	B
∪	∪	k?	∪
A	A	kA	A
<g/>
,	,	kIx,	,
sjednocované	sjednocovaný	k2eAgFnSc2d1	sjednocovaná
množiny	množina	k1gFnSc2	množina
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
možno	možno	k6eAd1	možno
psát	psát	k5eAaImF	psát
v	v	k7c6	v
libovolném	libovolný	k2eAgNnSc6d1	libovolné
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
.	.	kIx.	.
</s>
<s>
Neutrálním	neutrální	k2eAgInSc7d1	neutrální
prvkem	prvek	k1gInSc7	prvek
pro	pro	k7c4	pro
operaci	operace	k1gFnSc4	operace
sjednocení	sjednocení	k1gNnSc2	sjednocení
je	být	k5eAaImIp3nS	být
prázdná	prázdný	k2eAgFnSc1d1	prázdná
množina	množina	k1gFnSc1	množina
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
A	a	k8xC	a
∪	∪	k?	∪
∅	∅	k?	∅
=	=	kIx~	=
A	a	k9	a
pro	pro	k7c4	pro
libovolné	libovolný	k2eAgNnSc4d1	libovolné
A.	A.	kA	A.
Prázdná	prázdný	k2eAgFnSc1d1	prázdná
množina	množina	k1gFnSc1	množina
se	se	k3xPyFc4	se
tak	tak	k9	tak
dá	dát	k5eAaPmIp3nS	dát
chápat	chápat	k5eAaImF	chápat
jako	jako	k8xC	jako
výsledek	výsledek	k1gInSc4	výsledek
sjednocení	sjednocení	k1gNnSc2	sjednocení
prázdné	prázdný	k2eAgFnSc2d1	prázdná
množiny	množina	k1gFnSc2	množina
množin	množina	k1gFnPc2	množina
<g/>
.	.	kIx.	.
</s>
<s>
Sjednocením	sjednocení	k1gNnSc7	sjednocení
s	s	k7c7	s
univerzální	univerzální	k2eAgFnSc7d1	univerzální
množinou	množina	k1gFnSc7	množina
získáme	získat	k5eAaPmIp1nP	získat
opět	opět	k6eAd1	opět
univerzální	univerzální	k2eAgFnSc4d1	univerzální
množinu	množina	k1gFnSc4	množina
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
∪	∪	k?	∪
I	I	kA	I
=	=	kIx~	=
I	I	kA	I
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g/>
\	\	kIx~	\
<g/>
cup	cup	k1gInSc1	cup
I	I	kA	I
<g/>
=	=	kIx~	=
<g/>
I	I	kA	I
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
definici	definice	k1gFnSc3	definice
sjednocení	sjednocení	k1gNnSc2	sjednocení
vyplývají	vyplývat	k5eAaImIp3nP	vyplývat
všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
skutečnosti	skutečnost	k1gFnPc1	skutečnost
z	z	k7c2	z
obdobných	obdobný	k2eAgFnPc2d1	obdobná
skutečností	skutečnost	k1gFnPc2	skutečnost
o	o	k7c6	o
logické	logický	k2eAgFnSc6d1	logická
spojce	spojka	k1gFnSc6	spojka
nebo	nebo	k8xC	nebo
<g/>
.	.	kIx.	.
</s>
<s>
Mohutnost	mohutnost	k1gFnSc1	mohutnost
sjednocení	sjednocení	k1gNnSc1	sjednocení
dvou	dva	k4xCgFnPc2	dva
množin	množina	k1gFnPc2	množina
je	být	k5eAaImIp3nS	být
přinejmenším	přinejmenším	k6eAd1	přinejmenším
rovna	roven	k2eAgFnSc1d1	rovna
mohutnosti	mohutnost	k1gFnSc2	mohutnost
větší	veliký	k2eAgFnSc1d2	veliký
z	z	k7c2	z
obou	dva	k4xCgFnPc2	dva
množin	množina	k1gFnPc2	množina
<g/>
,	,	kIx,	,
nejvýše	vysoce	k6eAd3	vysoce
pak	pak	k6eAd1	pak
součtu	součet	k1gInSc2	součet
obou	dva	k4xCgFnPc2	dva
mohutností	mohutnost	k1gFnPc2	mohutnost
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
konečné	konečný	k2eAgFnPc4d1	konečná
množiny	množina	k1gFnPc4	množina
platí	platit	k5eAaImIp3nS	platit
konkrétně	konkrétně	k6eAd1	konkrétně
<g/>
:	:	kIx,	:
|	|	kIx~	|
<g/>
A	a	k9	a
∪	∪	k?	∪
B	B	kA	B
<g/>
|	|	kIx~	|
=	=	kIx~	=
|	|	kIx~	|
<g/>
A	a	k9	a
<g/>
|	|	kIx~	|
+	+	kIx~	+
|	|	kIx~	|
<g/>
B	B	kA	B
<g/>
|	|	kIx~	|
-	-	kIx~	-
|	|	kIx~	|
<g/>
A	a	k9	a
∩	∩	k?	∩
B	B	kA	B
<g/>
|	|	kIx~	|
<g/>
.	.	kIx.	.
</s>
<s>
Sjednocení	sjednocení	k1gNnSc1	sjednocení
množin	množina	k1gFnPc2	množina
je	být	k5eAaImIp3nS	být
idempotentní	idempotentní	k2eAgMnSc1d1	idempotentní
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
platí	platit	k5eAaImIp3nP	platit
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
A	a	k9	a
∪	∪	k?	∪
A	A	kA	A
=	=	kIx~	=
A	A	kA	A
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
A	a	k9	a
<g/>
\	\	kIx~	\
<g/>
cup	cup	k1gInSc1	cup
A	A	kA	A
<g/>
=	=	kIx~	=
<g/>
A	A	kA	A
<g/>
}	}	kIx)	}
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
slovo	slovo	k1gNnSc1	slovo
sjednocení	sjednocení	k1gNnSc2	sjednocení
(	(	kIx(	(
<g/>
cizím	cizit	k5eAaImIp1nS	cizit
slovem	slovo	k1gNnSc7	slovo
unifikace	unifikace	k1gFnSc2	unifikace
<g/>
)	)	kIx)	)
zcela	zcela	k6eAd1	zcela
obecně	obecně	k6eAd1	obecně
představuje	představovat	k5eAaImIp3nS	představovat
vytváření	vytváření	k1gNnSc4	vytváření
jednoty	jednota	k1gFnSc2	jednota
<g/>
,	,	kIx,	,
jednotného	jednotný	k2eAgInSc2d1	jednotný
názoru	názor	k1gInSc2	názor
<g/>
,	,	kIx,	,
jednotného	jednotný	k2eAgNnSc2d1	jednotné
chování	chování	k1gNnSc2	chování
<g/>
,	,	kIx,	,
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
,	,	kIx,	,
vystupování	vystupování	k1gNnSc2	vystupování
apod.	apod.	kA	apod.
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
oborech	obor	k1gInPc6	obor
lidské	lidský	k2eAgFnSc2d1	lidská
činnosti	činnost	k1gFnSc2	činnost
a	a	k8xC	a
společenského	společenský	k2eAgInSc2d1	společenský
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Množinové	množinový	k2eAgFnSc2d1	množinová
operace	operace	k1gFnSc2	operace
Průnik	průnik	k1gInSc1	průnik
Rozdíl	rozdíl	k1gInSc1	rozdíl
množin	množina	k1gFnPc2	množina
Doplněk	doplněk	k1gInSc4	doplněk
množiny	množina	k1gFnSc2	množina
UNION	union	k1gInSc4	union
</s>
