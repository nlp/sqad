<s>
České	český	k2eAgNnSc1d1	české
slovo	slovo	k1gNnSc1	slovo
sjednocení	sjednocení	k1gNnSc2	sjednocení
(	(	kIx(	(
<g/>
cizím	cizit	k5eAaImIp1nS	cizit
slovem	slovo	k1gNnSc7	slovo
unifikace	unifikace	k1gFnSc2	unifikace
<g/>
)	)	kIx)	)
zcela	zcela	k6eAd1	zcela
obecně	obecně	k6eAd1	obecně
představuje	představovat	k5eAaImIp3nS	představovat
vytváření	vytváření	k1gNnSc4	vytváření
jednoty	jednota	k1gFnSc2	jednota
<g/>
,	,	kIx,	,
jednotného	jednotný	k2eAgInSc2d1	jednotný
názoru	názor	k1gInSc2	názor
<g/>
,	,	kIx,	,
jednotného	jednotný	k2eAgNnSc2d1	jednotné
chování	chování	k1gNnSc2	chování
<g/>
,	,	kIx,	,
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
,	,	kIx,	,
vystupování	vystupování	k1gNnSc2	vystupování
apod.	apod.	kA	apod.
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
oborech	obor	k1gInPc6	obor
lidské	lidský	k2eAgFnSc2d1	lidská
činnosti	činnost	k1gFnSc2	činnost
a	a	k8xC	a
společenského	společenský	k2eAgInSc2d1	společenský
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
