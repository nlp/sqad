<s>
Leží	ležet	k5eAaImIp3nS
na	na	k7c6
východním	východní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
Tel	tel	kA
Avivu	Aviv	k1gFnSc4
<g/>
,	,	kIx,
cca	cca	kA
4,5	4,5	k4
kilometru	kilometr	k1gInSc2
od	od	k7c2
pobřeží	pobřeží	k1gNnSc2
Středozemního	středozemní	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
okolo	okolo	k7c2
40	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>