<s>
Kfar	Kfar	k1gMnSc1
Šalem	šal	k1gInSc7
</s>
<s>
Mešita	mešita	k1gFnSc1
coby	coby	k?
pozůstatek	pozůstatek	k1gInSc4
původní	původní	k2eAgFnSc2d1
arabské	arabský	k2eAgFnSc2d1
vesnice	vesnice	k1gFnSc2
v	v	k7c4
Kfar	Kfar	k1gInSc4
Šalem	šal	k1gInSc7
</s>
<s>
Kfar	Kfar	k1gMnSc1
Šalem	Šalem	k1gInSc1
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
:	:	kIx,
כ	כ	k?
ש	ש	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
čtvrť	čtvrť	k1gFnSc1
ve	v	k7c6
východní	východní	k2eAgFnSc6d1
části	část	k1gFnSc6
Tel	tel	kA
Avivu	Aviv	k1gInSc4
v	v	k7c6
Izraeli	Izrael	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
správního	správní	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
Rova	Rov	k1gInSc2
9	#num#	k4
a	a	k8xC
samosprávné	samosprávný	k2eAgFnPc1d1
jednotky	jednotka	k1gFnPc1
Rova	Rov	k1gInSc2
Darom	Darom	k1gInSc1
Mizrach	Mizrach	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Leží	ležet	k5eAaImIp3nS
na	na	k7c6
východním	východní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
Tel	tel	kA
Avivu	Aviv	k1gFnSc4
<g/>
,	,	kIx,
cca	cca	kA
4,5	4,5	k4
kilometru	kilometr	k1gInSc2
od	od	k7c2
pobřeží	pobřeží	k1gNnSc2
Středozemního	středozemní	k2eAgNnSc2d1
moře	moře	k1gNnSc2
<g/>
,	,	kIx,
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
okolo	okolo	k7c2
40	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dopravní	dopravní	k2eAgFnSc7d1
osou	osa	k1gFnSc7
čtvrti	čtvrt	k1gFnSc2
je	být	k5eAaImIp3nS
silnice	silnice	k1gFnSc1
číslo	číslo	k1gNnSc1
461	#num#	k4
(	(	kIx(
<g/>
ulice	ulice	k1gFnSc1
Derech	Derech	k1gInSc4
Chajim	Chajim	k1gInSc1
Bar	bar	k1gInSc1
Lev	lev	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
severu	sever	k1gInSc6
s	s	k7c7
ní	on	k3xPp3gFnSc7
sousedí	sousedit	k5eAaImIp3nP
čtvrť	čtvrť	k1gFnSc4
Ramat	Ramat	k2eAgInSc4d1
ha-Tajasim	ha-Tajasim	k1gInSc4
<g/>
,	,	kIx,
na	na	k7c6
západě	západ	k1gInSc6
Neve	Nev	k1gFnSc2
Barbur	Barbura	k1gFnPc2
a	a	k8xC
Neve	Neve	k1gFnPc2
Kfir	Kfira	k1gFnPc2
<g/>
,	,	kIx,
na	na	k7c6
jihu	jih	k1gInSc6
Livne	Livn	k1gInSc5
a	a	k8xC
na	na	k7c6
východě	východ	k1gInSc6
Neve	Nev	k1gFnSc2
Eli	Eli	k1gFnSc2
<g/>
'	'	kIx"
<g/>
ezer	ezer	k1gInSc1
a	a	k8xC
Neve	Neve	k1gFnSc1
Chen	Chena	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Popis	popis	k1gInSc1
čtvrti	čtvrt	k1gFnSc2
</s>
<s>
Plocha	plocha	k1gFnSc1
čtvrti	čtvrt	k1gFnSc2
je	být	k5eAaImIp3nS
vymezena	vymezit	k5eAaPmNgFnS
na	na	k7c6
severu	sever	k1gInSc6
ulicí	ulice	k1gFnSc7
Ma	Ma	k1gFnSc2
<g/>
'	'	kIx"
<g/>
apilej	apilat	k5eAaPmRp2nS,k5eAaImRp2nS
Egoz	Egoz	k1gInSc4
a	a	k8xC
Derech	Derech	k1gInSc4
ha-Hagana	ha-Hagan	k1gMnSc2
<g/>
,	,	kIx,
na	na	k7c6
jihu	jih	k1gInSc6
třídou	třída	k1gFnSc7
Derech	Derech	k1gInSc4
Chajim	Chajim	k1gInSc1
Bar	bar	k1gInSc1
Lev	lev	k1gInSc1
a	a	k8xC
ulicí	ulice	k1gFnSc7
Machal	Machal	k1gMnSc1
<g/>
,	,	kIx,
na	na	k7c6
východě	východ	k1gInSc6
třídou	třída	k1gFnSc7
Cvija	Cvij	k1gInSc2
Lubetkin	Lubetkina	k1gFnPc2
a	a	k8xC
na	na	k7c6
západě	západ	k1gInSc6
ulicí	ulice	k1gFnSc7
Moše	mocha	k1gFnSc3
Brill	Brillum	k1gNnPc2
a	a	k8xC
ha-Rav	ha-Rava	k1gFnPc2
Alnekave	Alnekav	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zástavba	zástavba	k1gFnSc1
má	mít	k5eAaImIp3nS
charakter	charakter	k1gInSc4
volnějši	volnějši	k?
a	a	k8xC
nepravidelné	pravidelný	k2eNgFnSc2d1
výstavby	výstavba	k1gFnSc2
na	na	k7c6
rostlém	rostlý	k2eAgInSc6d1
půdorysu	půdorys	k1gInSc6
původní	původní	k2eAgFnSc2d1
arabské	arabský	k2eAgFnSc2d1
vesnice	vesnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
5765	#num#	k4
obyvatel	obyvatel	k1gMnPc2
(	(	kIx(
<g/>
údaj	údaj	k1gInSc4
společný	společný	k2eAgInSc4d1
pro	pro	k7c4
čtvrť	čtvrť	k1gFnSc4
Neve	Nev	k1gMnSc2
Eli	Eli	k1gMnSc2
<g/>
'	'	kIx"
<g/>
ezer	ezer	k1gInSc1
a	a	k8xC
východní	východní	k2eAgFnSc1d1
část	část	k1gFnSc1
čtvrtě	čtvrt	k1gFnSc2
Kfar	Kfar	k1gMnSc1
Šalem	šal	k1gInSc7
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
západní	západní	k2eAgFnSc6d1
části	část	k1gFnSc6
Kfar	Kfara	k1gFnPc2
Šalem	šal	k1gInSc7
společně	společně	k6eAd1
s	s	k7c7
čtvrtí	čtvrt	k1gFnSc7
Neve	Nev	k1gInSc2
Barbur	Barbura	k1gFnPc2
žilo	žít	k5eAaImAgNnS
v	v	k7c6
roce	rok	k1gInSc6
2007	#num#	k4
4403	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
místě	místo	k1gNnSc6
nynější	nynější	k2eAgFnSc2d1
čtvrtě	čtvrt	k1gFnSc2
Kfar	Kfar	k1gMnSc1
Šalem	šal	k1gInSc7
stávala	stávat	k5eAaImAgFnS
dříve	dříve	k6eAd2
velká	velký	k2eAgFnSc1d1
arabská	arabský	k2eAgFnSc1d1
vesnice	vesnice	k1gFnSc1
Salama	Salama	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jmenovala	jmenovat	k5eAaImAgFnS,k5eAaBmAgFnS
se	se	k3xPyFc4
podle	podle	k7c2
Salama	Salam	k1gMnSc2
Ibn	Ibn	k1gMnSc4
Hašima	Hašim	k1gMnSc4
<g/>
,	,	kIx,
druha	druh	k1gMnSc4
proroka	prorok	k1gMnSc4
Mohameda	Mohamed	k1gMnSc4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc1
hrobka	hrobka	k1gFnSc1
zde	zde	k6eAd1
stále	stále	k6eAd1
stojí	stát	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
byla	být	k5eAaImAgFnS
ve	v	k7c6
vesnici	vesnice	k1gFnSc6
založena	založen	k2eAgFnSc1d1
základní	základní	k2eAgFnSc1d1
chlapecká	chlapecký	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1936	#num#	k4
vznikla	vzniknout	k5eAaPmAgFnS
i	i	k9
dívčí	dívčí	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stála	stát	k5eAaImAgFnS
zde	zde	k6eAd1
mešita	mešita	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1931	#num#	k4
měla	mít	k5eAaImAgFnS
vesnice	vesnice	k1gFnSc1
3	#num#	k4
691	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dubnu	duben	k1gInSc6
1948	#num#	k4
<g/>
,	,	kIx,
na	na	k7c6
počátku	počátek	k1gInSc6
války	válka	k1gFnSc2
za	za	k7c4
nezávislost	nezávislost	k1gFnSc4
byla	být	k5eAaImAgFnS
ovládnuta	ovládnout	k5eAaPmNgFnS
židovskými	židovský	k2eAgFnPc7d1
silami	síla	k1gFnPc7
a	a	k8xC
arabské	arabský	k2eAgNnSc4d1
osídlení	osídlení	k1gNnSc4
zde	zde	k6eAd1
skončilo	skončit	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zástavba	zástavba	k1gFnSc1
byla	být	k5eAaImAgFnS
zčásti	zčásti	k6eAd1
zachována	zachovat	k5eAaPmNgFnS
a	a	k8xC
dodnes	dodnes	k6eAd1
je	být	k5eAaImIp3nS
v	v	k7c4
Kfar	Kfar	k1gInSc4
Šalem	šal	k1gInSc7
dochováno	dochován	k2eAgNnSc1d1
několik	několik	k4yIc1
původních	původní	k2eAgInPc2d1
arabských	arabský	k2eAgInPc2d1
domů	dům	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Kfar	Kfar	k1gInSc1
Shalem	Shalo	k1gNnSc7
<g/>
,	,	kIx,
Tel	tel	kA
Aviv	Aviv	k1gInSc1
<g/>
,	,	kIx,
Israel	Israel	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Google	Google	k1gNnSc1
Maps	Mapsa	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
,	,	kIx,
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
amudanan	amudanan	k1gInSc1
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
il	il	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
amudanan	amudanan	k1gInSc1
<g/>
.	.	kIx.
<g/>
co	co	k9
<g/>
.	.	kIx.
<g/>
il	il	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Quarters	Quarters	k1gInSc1
and	and	k?
Subquarters	Subquarters	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
tel-aviv	tel-avit	k5eAaPmDgInS
<g/>
.	.	kIx.
<g/>
gov	gov	k?
<g/>
.	.	kIx.
<g/>
il	il	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ש	ש	k?
ר	ר	k?
מ	מ	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
tel-aviv	tel-avit	k5eAaPmDgInS
<g/>
.	.	kIx.
<g/>
gov	gov	k?
<g/>
.	.	kIx.
<g/>
il	il	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Welcome	Welcom	k1gInSc5
To	ten	k3xDgNnSc1
Salama	Salama	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Palestine	Palestin	k1gInSc5
Remembered	Remembered	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
čtvrtí	čtvrtit	k5eAaImIp3nS
v	v	k7c6
Tel	tel	kA
Avivu	Aviv	k1gInSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Kfar	Kfar	k1gMnSc1
Šalem	šal	k1gInSc7
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Izrael	Izrael	k1gInSc1
</s>
