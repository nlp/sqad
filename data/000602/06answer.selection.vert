<s>
Alberto	Alberta	k1gFnSc5	Alberta
Moravia	Moravium	k1gNnPc1	Moravium
[	[	kIx(	[
<g/>
morávja	morávja	k1gFnSc1	morávja
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Alberto	Alberta	k1gFnSc5	Alberta
Píncherle	Píncherle	k1gFnSc5	Píncherle
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1907	[number]	k4	1907
Řím	Řím	k1gInSc1	Řím
-	-	kIx~	-
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1990	[number]	k4	1990
Řím	Řím	k1gInSc1	Řím
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
italský	italský	k2eAgMnSc1d1	italský
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
literární	literární	k2eAgMnSc1d1	literární
kritik	kritik	k1gMnSc1	kritik
<g/>
.	.	kIx.	.
</s>
