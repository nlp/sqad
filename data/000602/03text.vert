<s>
Alberto	Alberta	k1gFnSc5	Alberta
Moravia	Moravium	k1gNnPc1	Moravium
[	[	kIx(	[
<g/>
morávja	morávja	k1gFnSc1	morávja
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
vlastním	vlastní	k2eAgNnSc7d1	vlastní
jménem	jméno	k1gNnSc7	jméno
Alberto	Alberta	k1gFnSc5	Alberta
Píncherle	Píncherle	k1gFnSc5	Píncherle
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1907	[number]	k4	1907
Řím	Řím	k1gInSc1	Řím
-	-	kIx~	-
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1990	[number]	k4	1990
Řím	Řím	k1gInSc1	Řím
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
italský	italský	k2eAgMnSc1d1	italský
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
literární	literární	k2eAgMnSc1d1	literární
kritik	kritik	k1gMnSc1	kritik
<g/>
.	.	kIx.	.
</s>
<s>
Představitel	představitel	k1gMnSc1	představitel
italského	italský	k2eAgInSc2d1	italský
neorealismu	neorealismus	k1gInSc2	neorealismus
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
se	se	k3xPyFc4	se
u	u	k7c2	u
něho	on	k3xPp3gNnSc2	on
projevila	projevit	k5eAaPmAgFnS	projevit
kostní	kostní	k2eAgFnSc1d1	kostní
tuberkulóza	tuberkulóza	k1gFnSc1	tuberkulóza
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
léčil	léčit	k5eAaImAgMnS	léčit
v	v	k7c6	v
několika	několik	k4yIc6	několik
sanatoriích	sanatorium	k1gNnPc6	sanatorium
v	v	k7c6	v
Alpách	Alpy	k1gFnPc6	Alpy
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
vzdělání	vzdělání	k1gNnSc4	vzdělání
získal	získat	k5eAaPmAgMnS	získat
především	především	k9	především
od	od	k7c2	od
soukromých	soukromý	k2eAgMnPc2d1	soukromý
učitelů	učitel	k1gMnPc2	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
poměrně	poměrně	k6eAd1	poměrně
úspěšně	úspěšně	k6eAd1	úspěšně
bojoval	bojovat	k5eAaImAgMnS	bojovat
s	s	k7c7	s
tehdejší	tehdejší	k2eAgFnSc7d1	tehdejší
fašistickou	fašistický	k2eAgFnSc7d1	fašistická
cenzurou	cenzura	k1gFnSc7	cenzura
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
židovské	židovský	k2eAgFnSc2d1	židovská
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
téměř	téměř	k6eAd1	téměř
nemohl	moct	k5eNaImAgMnS	moct
publikovat	publikovat	k5eAaBmF	publikovat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
dokonce	dokonce	k9	dokonce
vydán	vydán	k2eAgInSc1d1	vydán
zatykač	zatykač	k1gInSc1	zatykač
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yRgNnSc6	který
se	se	k3xPyFc4	se
včas	včas	k6eAd1	včas
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
skrýval	skrývat	k5eAaImAgInS	skrývat
mezi	mezi	k7c7	mezi
dělníky	dělník	k1gMnPc7	dělník
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
poválečná	poválečný	k2eAgFnSc1d1	poválečná
literatura	literatura	k1gFnSc1	literatura
a	a	k8xC	a
názory	názor	k1gInPc1	názor
vedly	vést	k5eAaImAgInP	vést
ke	k	k7c3	k
sporům	spor	k1gInPc3	spor
s	s	k7c7	s
církví	církev	k1gFnSc7	církev
<g/>
.	.	kIx.	.
</s>
<s>
Katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
zařadila	zařadit	k5eAaPmAgFnS	zařadit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
jeho	jeho	k3xOp3gFnSc1	jeho
spisy	spis	k1gInPc4	spis
na	na	k7c4	na
Index	index	k1gInSc4	index
zakázaných	zakázaný	k2eAgFnPc2d1	zakázaná
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
poměrně	poměrně	k6eAd1	poměrně
hodně	hodně	k6eAd1	hodně
cestoval	cestovat	k5eAaImAgMnS	cestovat
<g/>
,	,	kIx,	,
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Anglii	Anglie	k1gFnSc4	Anglie
<g/>
,	,	kIx,	,
Francii	Francie	k1gFnSc4	Francie
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc4	Mexiko
<g/>
,	,	kIx,	,
Čínu	Čína	k1gFnSc4	Čína
<g/>
,	,	kIx,	,
ve	v	k7c6	v
třicátých	třicátý	k4xOgNnPc6	třicátý
letech	léto	k1gNnPc6	léto
navštívil	navštívit	k5eAaPmAgMnS	navštívit
i	i	k9	i
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
své	svůj	k3xOyFgInPc4	svůj
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
běžné	běžný	k2eAgInPc1d1	běžný
názory	názor	k1gInPc1	názor
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1959	[number]	k4	1959
<g/>
-	-	kIx~	-
<g/>
1962	[number]	k4	1962
předsedou	předseda	k1gMnSc7	předseda
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
PEN	PEN	kA	PEN
klubu	klub	k1gInSc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
Moravia	Moravia	k1gFnSc1	Moravia
se	se	k3xPyFc4	se
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
dílech	díl	k1gInPc6	díl
zabýval	zabývat	k5eAaImAgInS	zabývat
úpadkem	úpadek	k1gInSc7	úpadek
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
motivy	motiv	k1gInPc7	motiv
odcizení	odcizení	k1gNnSc2	odcizení
a	a	k8xC	a
krachem	krach	k1gInSc7	krach
lidských	lidský	k2eAgInPc2d1	lidský
vztahů	vztah	k1gInPc2	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
nejtypičtějším	typický	k2eAgNnSc7d3	nejtypičtější
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
nuda	nuda	k1gFnSc1	nuda
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
také	také	k9	také
antifašistickou	antifašistický	k2eAgFnSc7d1	Antifašistická
tematikou	tematika	k1gFnSc7	tematika
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
Moravia	Moravia	k1gFnSc1	Moravia
dostával	dostávat	k5eAaImAgInS	dostávat
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
k	k	k7c3	k
sexuální	sexuální	k2eAgFnSc3d1	sexuální
tematice	tematika	k1gFnSc3	tematika
<g/>
,	,	kIx,	,
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
díle	díl	k1gInSc6	díl
je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
ovlivnění	ovlivnění	k1gNnSc4	ovlivnění
neorealismem	neorealismus	k1gInSc7	neorealismus
a	a	k8xC	a
surrealismem	surrealismus	k1gInSc7	surrealismus
a	a	k8xC	a
S.	S.	kA	S.
Freudem	Freud	k1gInSc7	Freud
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
trend	trend	k1gInSc1	trend
se	se	k3xPyFc4	se
projevoval	projevovat	k5eAaImAgInS	projevovat
už	už	k9	už
v	v	k7c6	v
předválečných	předválečný	k2eAgNnPc6d1	předválečné
dílech	dílo	k1gNnPc6	dílo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
plně	plně	k6eAd1	plně
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
rozvinul	rozvinout	k5eAaPmAgInS	rozvinout
až	až	k6eAd1	až
na	na	k7c6	na
sklonku	sklonek	k1gInSc6	sklonek
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
dílech	dílo	k1gNnPc6	dílo
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
něho	on	k3xPp3gInSc4	on
sex	sex	k1gInSc4	sex
stal	stát	k5eAaPmAgInS	stát
základním	základní	k2eAgInSc7d1	základní
momentem	moment	k1gInSc7	moment
lidského	lidský	k2eAgInSc2d1	lidský
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
díla	dílo	k1gNnPc1	dílo
jsou	být	k5eAaImIp3nP	být
proto	proto	k8xC	proto
eroticky	eroticky	k6eAd1	eroticky
velmi	velmi	k6eAd1	velmi
otevřená	otevřený	k2eAgFnSc1d1	otevřená
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
románů	román	k1gInPc2	román
a	a	k8xC	a
povídek	povídka	k1gFnPc2	povídka
bylo	být	k5eAaImAgNnS	být
zfilmováno	zfilmovat	k5eAaPmNgNnS	zfilmovat
<g/>
.	.	kIx.	.
</s>
<s>
Zklamané	zklamaný	k2eAgFnPc1d1	zklamaná
naděje	naděje	k1gFnPc1	naděje
Marné	marný	k2eAgFnSc2d1	marná
ctižádosti	ctižádost	k1gFnSc2	ctižádost
(	(	kIx(	(
<g/>
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
Lhostejní	lhostejnět	k5eAaImIp3nS	lhostejnět
1929	[number]	k4	1929
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
kritickým	kritický	k2eAgInSc7d1	kritický
obrazem	obraz	k1gInSc7	obraz
úpadku	úpadek	k1gInSc2	úpadek
italské	italský	k2eAgFnSc2d1	italská
měšťanské	měšťanský	k2eAgFnSc2d1	měšťanská
rodiny	rodina	k1gFnSc2	rodina
Maškaráda	maškaráda	k1gFnSc1	maškaráda
(	(	kIx(	(
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
Agostino	Agostin	k2eAgNnSc1d1	Agostino
(	(	kIx(	(
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
Neposlušnost	neposlušnost	k1gFnSc1	neposlušnost
(	(	kIx(	(
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
Láska	láska	k1gFnSc1	láska
manželská	manželský	k2eAgFnSc1d1	manželská
(	(	kIx(	(
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
Římanka	Římanka	k1gFnSc1	Římanka
-	-	kIx~	-
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
román	román	k1gInSc4	román
o	o	k7c6	o
krásné	krásný	k2eAgFnSc6d1	krásná
Adrianě	Adriana	k1gFnSc6	Adriana
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
prostitutkou	prostitutka	k1gFnSc7	prostitutka
ze	z	k7c2	z
sociálních	sociální	k2eAgInPc2d1	sociální
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
uživila	uživit	k5eAaPmAgFnS	uživit
matku	matka	k1gFnSc4	matka
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
sní	snít	k5eAaImIp3nS	snít
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
hodně	hodně	k6eAd1	hodně
peněz	peníze	k1gInPc2	peníze
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
dítě	dítě	k1gNnSc4	dítě
s	s	k7c7	s
vrahem	vrah	k1gMnSc7	vrah
(	(	kIx(	(
<g/>
dítě	dítě	k1gNnSc4	dítě
prostitutky	prostitutka	k1gFnSc2	prostitutka
a	a	k8xC	a
vraha	vrah	k1gMnSc4	vrah
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
silné	silný	k2eAgNnSc1d1	silné
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInPc1	její
sny	sen	k1gInPc1	sen
se	se	k3xPyFc4	se
však	však	k9	však
nevyplní	vyplnit	k5eNaPmIp3nS	vyplnit
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
život	život	k1gInSc1	život
vyprchá	vyprchat	k5eAaPmIp3nS	vyprchat
<g/>
.	.	kIx.	.
</s>
<s>
Pohrdání	pohrdání	k1gNnSc1	pohrdání
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
Horalka	horalka	k1gFnSc1	horalka
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
Nešťastná	šťastný	k2eNgFnSc1d1	nešťastná
milenka	milenka	k1gFnSc1	milenka
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
Konformista	konformista	k1gMnSc1	konformista
Nuda	nuda	k1gFnSc1	nuda
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
-	-	kIx~	-
psychologický	psychologický	k2eAgInSc4d1	psychologický
román	román	k1gInSc4	román
o	o	k7c6	o
vztazích	vztah	k1gInPc6	vztah
malíře	malíř	k1gMnSc2	malíř
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
znuděn	znudit	k5eAaPmNgMnS	znudit
životem	život	k1gInSc7	život
Pozornost	pozornost	k1gFnSc1	pozornost
Já	já	k3xPp1nSc1	já
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
Cesta	cesta	k1gFnSc1	cesta
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g />
.	.	kIx.	.
</s>
<s>
Leopardí	leopardí	k2eAgFnSc1d1	leopardí
žena	žena	k1gFnSc1	žena
1934	[number]	k4	1934
-	-	kIx~	-
Přežít	přežít	k5eAaPmF	přežít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
smrt	smrt	k1gFnSc4	smrt
Voyeur	Voyeura	k1gFnPc2	Voyeura
aneb	aneb	k?	aneb
Muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
dívá	dívat	k5eAaImIp3nS	dívat
-	-	kIx~	-
1985	[number]	k4	1985
Epidemie	epidemie	k1gFnPc4	epidemie
Římské	římský	k2eAgFnSc2d1	římská
povídky	povídka	k1gFnSc2	povídka
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
Nové	Nové	k2eAgFnSc2d1	Nové
římské	římský	k2eAgFnSc2d1	římská
povídky	povídka	k1gFnSc2	povídka
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
Agostino	Agostin	k2eAgNnSc1d1	Agostino
(	(	kIx(	(
<g/>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
Automat	automat	k1gInSc1	automat
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
Beatrice	Beatrice	k1gFnSc2	Beatrice
Centi	Cenť	k1gFnSc2	Cenť
Život	život	k1gInSc1	život
je	být	k5eAaImIp3nS	být
hra	hra	k1gFnSc1	hra
Člověk	člověk	k1gMnSc1	člověk
jako	jako	k8xS	jako
cíl	cíl	k1gInSc1	cíl
Představa	představa	k1gFnSc1	představa
Indie	Indie	k1gFnSc1	Indie
Měsíc	měsíc	k1gInSc1	měsíc
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
</s>
