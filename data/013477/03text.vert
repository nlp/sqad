<s>
Oltář	Oltář	k1gInSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
liturgickém	liturgický	k2eAgInSc6d1
objektu	objekt	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
souhvězdí	souhvězdí	k1gNnSc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Oltář	Oltář	k1gInSc1
(	(	kIx(
<g/>
souhvězdí	souhvězdí	k1gNnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Gotický	gotický	k2eAgInSc1d1
křídlový	křídlový	k2eAgInSc1d1
oltář	oltář	k1gInSc1
v	v	k7c6
kapli	kaple	k1gFnSc6
Čtrnácti	čtrnáct	k4xCc2
svatých	svatý	k2eAgMnPc2d1
pomocníků	pomocník	k1gMnPc2
v	v	k7c6
poutním	poutní	k2eAgInSc6d1
kostele	kostel	k1gInSc6
Maria	Maria	k1gFnSc1
Elend	Elend	k1gInSc1
v	v	k7c6
rakouském	rakouský	k2eAgNnSc6d1
městě	město	k1gNnSc6
Sankt	Sankt	k1gInSc1
Jakob	Jakob	k1gMnSc1
im	im	k?
Rosental	Rosental	k1gMnSc1
<g/>
,	,	kIx,
Korutany	Korutany	k1gInPc1
</s>
<s>
Oltář	Oltář	k1gInSc1
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
מ	מ	kIx~
<g/>
ִ	ִ	kIx~
<g/>
ז	ז	kIx~
<g/>
ְ	ְ	kIx~
<g/>
ב	ב	kIx~
<g/>
ֵ	ֵ	kIx~
<g/>
ּ	ּ	kIx~
<g/>
ח	ח	kIx~
<g/>
ַ	ַ	kIx~
<g/>
,	,	kIx,
mizbeach	mizbeach	k1gInSc1
<g/>
,	,	kIx,
řecky	řecky	k6eAd1
β	β	kA
<g/>
,	,	kIx,
béma	béma	k1gFnSc1
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
ara	ara	k1gInSc1
nebo	nebo	k8xC
altaria	altarium	k1gNnPc1
<g/>
,	,	kIx,
v	v	k7c6
křesťanském	křesťanský	k2eAgInSc6d1
užití	užití	k1gNnSc2
altare	altare	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
místo	místo	k1gNnSc1
<g/>
,	,	kIx,
na	na	k7c6
němž	jenž	k3xRgInSc6
se	se	k3xPyFc4
v	v	k7c6
náboženství	náboženství	k1gNnSc6
přináší	přinášet	k5eAaImIp3nS
dar	dar	k1gInSc1
jako	jako	k8xS,k8xC
oběť	oběť	k1gFnSc1
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
bohu	bůh	k1gMnSc3
či	či	k8xC
božstvu	božstvo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Oltář	Oltář	k1gInSc1
ve	v	k7c6
starověku	starověk	k1gInSc6
</s>
<s>
Velikosti	velikost	k1gFnPc1
starověkých	starověký	k2eAgInPc2d1
oltářů	oltář	k1gInPc2
bylo	být	k5eAaImAgNnS
později	pozdě	k6eAd2
dosahováno	dosahovat	k5eAaImNgNnS
pouze	pouze	k6eAd1
zřídka	zřídka	k6eAd1
(	(	kIx(
<g/>
např.	např.	kA
oltář	oltář	k1gInSc1
o	o	k7c6
rozměrech	rozměr	k1gInPc6
198	#num#	k4
<g/>
×	×	k?
<g/>
23	#num#	k4
m	m	kA
v	v	k7c6
Syrakusách	Syrakusy	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oltáře	Oltář	k1gInSc2
stávaly	stávat	k5eAaImAgFnP
obvykle	obvykle	k6eAd1
na	na	k7c6
otevřených	otevřený	k2eAgInPc6d1
místech	místo	k1gNnPc6
<g/>
,	,	kIx,
často	často	k6eAd1
byly	být	k5eAaImAgFnP
určeny	určit	k5eAaPmNgFnP
k	k	k7c3
zápalné	zápalný	k2eAgFnSc3d1
oběti	oběť	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Známý	známý	k1gMnSc1
je	být	k5eAaImIp3nS
oltář	oltář	k1gInSc4
z	z	k7c2
Pergamu	Pergamos	k1gInSc2
z	z	k7c2
2	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
ozdobený	ozdobený	k2eAgInSc4d1
reliéfy	reliéf	k1gInPc4
o	o	k7c6
celkových	celkový	k2eAgInPc6d1
rozměrech	rozměr	k1gInPc6
36	#num#	k4
<g/>
×	×	k?
<g/>
34	#num#	k4
m	m	kA
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
nyní	nyní	k6eAd1
uložen	uložit	k5eAaPmNgInS
v	v	k7c6
berlínském	berlínský	k2eAgInSc6d1
Pergamonmuseu	Pergamonmuseus	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Etruskové	Etrusk	k1gMnPc1
a	a	k8xC
Římané	Říman	k1gMnPc1
budovali	budovat	k5eAaImAgMnP
oltáře	oltář	k1gInPc4
pro	pro	k7c4
svá	svůj	k3xOyFgNnPc4
osobní	osobní	k2eAgNnPc4d1
božstva	božstvo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těchto	tento	k3xDgInPc2
oltářů	oltář	k1gInPc2
se	se	k3xPyFc4
zachovalo	zachovat	k5eAaPmAgNnS
mnoho	mnoho	k4c1
tisíc	tisíc	k4xCgInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
často	často	k6eAd1
ozdobeny	ozdoben	k2eAgInPc1d1
nápisy	nápis	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
nich	on	k3xPp3gInPc6
se	se	k3xPyFc4
přinášela	přinášet	k5eAaImAgFnS
buď	buď	k8xC
zápalná	zápalný	k2eAgFnSc1d1
oběť	oběť	k1gFnSc1
(	(	kIx(
<g/>
např.	např.	kA
kadidlo	kadidlo	k1gNnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
se	se	k3xPyFc4
na	na	k7c4
něj	on	k3xPp3gMnSc4
pokládal	pokládat	k5eAaImAgInS
dar	dar	k1gInSc1
(	(	kIx(
<g/>
např.	např.	kA
se	se	k3xPyFc4
ulilo	ulejt	k5eAaPmAgNnS
víno	víno	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Oltář	Oltář	k1gInSc1
v	v	k7c6
judaismu	judaismus	k1gInSc6
</s>
<s>
V	v	k7c6
hebrejské	hebrejský	k2eAgFnSc6d1
bibli	bible	k1gFnSc6
se	se	k3xPyFc4
objevují	objevovat	k5eAaImIp3nP
popisy	popis	k1gInPc1
oltářů	oltář	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
byly	být	k5eAaImAgInP
obvykle	obvykle	k6eAd1
vybudovány	vybudovat	k5eAaPmNgInP
z	z	k7c2
hlíny	hlína	k1gFnSc2
nebo	nebo	k8xC
neotesaných	otesaný	k2eNgInPc2d1
kamenů	kámen	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Oltáře	Oltář	k1gInPc1
byly	být	k5eAaImAgInP
vztyčovány	vztyčovat	k5eAaImNgInP
na	na	k7c6
význačných	význačný	k2eAgNnPc6d1
místech	místo	k1gNnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Poprvé	poprvé	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
Bibli	bible	k1gFnSc6
objevuje	objevovat	k5eAaImIp3nS
oltář	oltář	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
vystavěl	vystavět	k5eAaPmAgMnS
Noe	Noe	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
dalšími	další	k2eAgFnPc7d1
postavami	postava	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
jej	on	k3xPp3gMnSc4
staví	stavit	k5eAaPmIp3nS,k5eAaBmIp3nS,k5eAaImIp3nS
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
např.	např.	kA
Abrahám	Abrahám	k1gMnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Izák	Izák	k1gMnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Jákob	Jákob	k1gMnSc1
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Mojžíš	Mojžíš	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Samotné	samotný	k2eAgNnSc1d1
hebrejské	hebrejský	k2eAgNnSc1d1
slovo	slovo	k1gNnSc1
pro	pro	k7c4
oltář	oltář	k1gInSc4
(	(	kIx(
<g/>
מ	מ	k?
<g/>
ִ	ִ	k?
<g/>
ז	ז	k?
<g/>
ְ	ְ	k?
<g/>
ב	ב	k?
<g/>
ֵ	ֵ	k?
<g/>
ּ	ּ	k?
<g/>
ח	ח	k?
<g/>
ַ	ַ	k?
<g/>
,	,	kIx,
<g/>
mizbeach	mizbeach	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
odvozeno	odvodit	k5eAaPmNgNnS
ze	z	k7c2
slovesného	slovesný	k2eAgInSc2d1
kořene	kořen	k1gInSc2
„	„	k?
<g/>
zajin-vejt-chet	zajin-vejt-chet	k1gMnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
ז	ז	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
tyto	tento	k3xDgInPc4
významy	význam	k1gInPc4
<g/>
:	:	kIx,
„	„	k?
<g/>
zabíjet	zabíjet	k5eAaImF
<g/>
,	,	kIx,
obětovat	obětovat	k5eAaBmF
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Židovská	židovský	k2eAgFnSc1d1
tradice	tradice	k1gFnSc1
však	však	k9
vnímá	vnímat	k5eAaImIp3nS
jednotlivá	jednotlivý	k2eAgNnPc4d1
písmena	písmeno	k1gNnPc4
slova	slovo	k1gNnSc2
mizbeach	mizbeacha	k1gFnPc2
jako	jako	k8xC,k8xS
akrostich	akrostichon	k1gNnPc2
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
písmeno	písmeno	k1gNnSc1
„	„	k?
<g/>
mem	mem	k?
<g/>
“	“	k?
(	(	kIx(
<g/>
מ	מ	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
počátečním	počáteční	k2eAgNnSc7d1
písmenem	písmeno	k1gNnSc7
pro	pro	k7c4
výraz	výraz	k1gInSc4
mechila	mechila	k1gFnSc1
(	(	kIx(
<g/>
מ	מ	k?
<g/>
ְ	ְ	k?
<g/>
ח	ח	k?
<g/>
ִ	ִ	k?
<g/>
י	י	k?
<g/>
ָ	ָ	k?
<g/>
ה	ה	k?
<g/>
,	,	kIx,
odpuštění	odpuštění	k1gNnSc4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
<g/>
,	,	kIx,
písmeno	písmeno	k1gNnSc1
„	„	k?
<g/>
zajin	zajin	k1gMnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
ז	ז	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
počátečním	počáteční	k2eAgNnSc7d1
písmenem	písmeno	k1gNnSc7
pro	pro	k7c4
výraz	výraz	k1gInSc4
zechut	zechut	k2eAgInSc4d1
(	(	kIx(
<g/>
ז	ז	k?
<g/>
ְ	ְ	k?
<g/>
כ	כ	k?
<g/>
ּ	ּ	k?
<g/>
ת	ת	k?
<g/>
,	,	kIx,
zásluha	zásluha	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
písmeno	písmeno	k1gNnSc1
„	„	k?
<g/>
bejt	bejt	k?
<g/>
“	“	k?
(	(	kIx(
<g/>
ב	ב	k?
<g/>
ּ	ּ	k?
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
je	být	k5eAaImIp3nS
počátečním	počáteční	k2eAgNnSc7d1
písmenem	písmeno	k1gNnSc7
výrazu	výraz	k1gInSc2
beracha	beracha	k1gFnSc1
(	(	kIx(
<g/>
ב	ב	k?
<g/>
ְ	ְ	k?
<g/>
ּ	ּ	k?
<g/>
ר	ר	k?
<g/>
ָ	ָ	k?
<g/>
כ	כ	k?
<g/>
ָ	ָ	k?
<g/>
ה	ה	k?
<g/>
,	,	kIx,
požehnání	požehnání	k1gNnSc4
<g/>
)	)	kIx)
a	a	k8xC
písmeno	písmeno	k1gNnSc1
„	„	k?
<g/>
chet	chet	k1gMnSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
ח	ח	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
počátečním	počáteční	k2eAgNnSc7d1
písmenem	písmeno	k1gNnSc7
pro	pro	k7c4
výraz	výraz	k1gInSc4
chajim	chajim	k1gInSc4
(	(	kIx(
<g/>
ח	ח	k?
<g/>
ַ	ַ	k?
<g/>
י	י	k?
<g/>
ִ	ִ	k?
<g/>
ּ	ּ	k?
<g/>
י	י	k?
<g/>
,	,	kIx,
život	život	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Oltář	Oltář	k1gInSc1
pro	pro	k7c4
zápalné	zápalný	k2eAgFnPc4d1
oběti	oběť	k1gFnPc4
</s>
<s>
Ve	v	k7c6
stanu	stan	k1gInSc6
setkávání	setkávání	k1gNnSc2
a	a	k8xC
později	pozdě	k6eAd2
v	v	k7c6
chrámu	chrám	k1gInSc6
byly	být	k5eAaImAgInP
umístěny	umístěn	k2eAgInPc1d1
dva	dva	k4xCgInPc1
oltáře	oltář	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgMnSc7
z	z	k7c2
nich	on	k3xPp3gMnPc2
byl	být	k5eAaImAgInS
oltář	oltář	k1gInSc1
pro	pro	k7c4
zápalné	zápalný	k2eAgFnPc4d1
oběti	oběť	k1gFnPc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
nazývaný	nazývaný	k2eAgMnSc1d1
také	také	k6eAd1
měděný	měděný	k2eAgInSc4d1
oltář	oltář	k1gInSc4
či	či	k8xC
stůl	stůl	k1gInSc4
Páně	páně	k2eAgInPc2d1
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
se	se	k3xPyFc4
nacházel	nacházet	k5eAaImAgMnS
v	v	k7c6
nádvoří	nádvoří	k1gNnSc6
svatyně	svatyně	k1gFnSc2
a	a	k8xC
je	být	k5eAaImIp3nS
popsán	popsat	k5eAaPmNgInS
v	v	k7c6
Ex	ex	k6eAd1
27	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
–	–	k?
<g/>
8	#num#	k4
(	(	kIx(
<g/>
Kral	Kral	k1gMnSc1
<g/>
,	,	kIx,
ČEP	Čep	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc1
půdorys	půdorys	k1gInSc4
byl	být	k5eAaImAgInS
čtvercový	čtvercový	k2eAgMnSc1d1
o	o	k7c6
délce	délka	k1gFnSc6
stran	stran	k7c2
5	#num#	k4
loktů	loket	k1gInPc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
výška	výška	k1gFnSc1
měřila	měřit	k5eAaImAgFnS
3	#num#	k4
lokty	loket	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Horní	horní	k2eAgFnSc1d1
část	část	k1gFnSc1
byla	být	k5eAaImAgFnS
ozdobena	ozdoben	k2eAgFnSc1d1
rohy	roh	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oltář	Oltář	k1gInSc1
byl	být	k5eAaImAgInS
zhotoven	zhotovit	k5eAaPmNgInS
ze	z	k7c2
dřeva	dřevo	k1gNnSc2
a	a	k8xC
obit	obit	k2eAgInSc1d1
měděnými	měděný	k2eAgInPc7d1
pláty	plát	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Šalomounově	Šalomounův	k2eAgInSc6d1
chrámu	chrám	k1gInSc6
měl	mít	k5eAaImAgInS
oltář	oltář	k1gInSc1
pro	pro	k7c4
zápalné	zápalný	k2eAgFnPc4d1
oběti	oběť	k1gFnPc4
větší	veliký	k2eAgFnSc2d2
rozměry	rozměra	k1gFnSc2
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
byl	být	k5eAaImAgInS
zhotoven	zhotovit	k5eAaPmNgInS
celý	celý	k2eAgInSc1d1
z	z	k7c2
mědi	měď	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
pokrývala	pokrývat	k5eAaImAgFnS
kámen	kámen	k1gInSc4
nebo	nebo	k8xC
zeminu	zemina	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oltář	Oltář	k1gInSc1
obnovil	obnovit	k5eAaPmAgInS
Ása	Ása	k1gFnSc4
<g/>
,	,	kIx,
odstranil	odstranit	k5eAaPmAgMnS
král	král	k1gMnSc1
Achaz	Achaz	k1gInSc1
a	a	k8xC
byl	být	k5eAaImAgInS
znovu	znovu	k6eAd1
očištěn	očistit	k5eAaPmNgInS
Chizkijášem	Chizkijáš	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zcela	zcela	k6eAd1
zničen	zničen	k2eAgInSc1d1
byl	být	k5eAaImAgInS
během	během	k7c2
dobytí	dobytí	k1gNnSc2
Jeruzaléma	Jeruzalém	k1gInSc2
Babylóňany	Babylóňan	k1gMnPc7
roku	rok	k1gInSc2
587	#num#	k4
<g/>
/	/	kIx~
<g/>
586	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
</s>
<s>
Po	po	k7c6
návratu	návrat	k1gInSc6
z	z	k7c2
babylonského	babylonský	k2eAgNnSc2d1
vyhnanství	vyhnanství	k1gNnSc2
byl	být	k5eAaImAgInS
oltář	oltář	k1gInSc1
znovu	znovu	k6eAd1
vystavěn	vystavět	k5eAaPmNgInS
na	na	k7c6
původním	původní	k2eAgNnSc6d1
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
vlády	vláda	k1gFnSc2
Antiocha	Antioch	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Epifana	Epifana	k1gFnSc1
byl	být	k5eAaImAgInS
oltář	oltář	k1gInSc1
znečištěn	znečistit	k5eAaPmNgInS
pohanskými	pohanský	k2eAgFnPc7d1
oběťmi	oběť	k1gFnPc7
<g/>
;	;	kIx,
Juda	judo	k1gNnPc1
Makabejský	makabejský	k2eAgInSc1d1
tento	tento	k3xDgInSc1
oltář	oltář	k1gInSc1
očistil	očistit	k5eAaPmAgInS
po	po	k7c4
dobytí	dobytí	k1gNnSc4
Jeruzaléma	Jeruzalém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pravděpodobně	pravděpodobně	k6eAd1
byl	být	k5eAaImAgInS
oltář	oltář	k1gInSc1
zrenovován	zrenovovat	k5eAaPmNgInS
během	během	k7c2
Héródovy	Héródův	k2eAgFnSc2d1
výstavby	výstavba	k1gFnSc2
chrámu	chrám	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
zničen	zničit	k5eAaPmNgInS
při	při	k7c6
dobytí	dobytí	k1gNnSc6
Jeruzaléma	Jeruzalém	k1gInSc2
Římany	Říman	k1gMnPc7
roku	rok	k1gInSc2
70	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Kadidlový	kadidlový	k2eAgInSc1d1
oltář	oltář	k1gInSc1
</s>
<s>
Druhým	druhý	k4xOgInSc7
oltářem	oltář	k1gInSc7
v	v	k7c6
chrámu	chrám	k1gInSc6
byl	být	k5eAaImAgInS
kadidlový	kadidlový	k2eAgInSc1d1
oltář	oltář	k1gInSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
nazývaný	nazývaný	k2eAgInSc1d1
také	také	k9
zlatý	zlatý	k2eAgInSc1d1
oltář	oltář	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
stál	stát	k5eAaImAgInS
před	před	k7c7
oponou	opona	k1gFnSc7
<g/>
,	,	kIx,
za	za	k7c7
níž	jenž	k3xRgFnSc7
ve	v	k7c4
velesvatyni	velesvatyně	k1gFnSc4
stála	stát	k5eAaImAgFnS
archa	archa	k1gFnSc1
úmluvy	úmluva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
tomto	tento	k3xDgInSc6
oltáři	oltář	k1gInSc6
kněží	kněz	k1gMnPc2
přinášeli	přinášet	k5eAaImAgMnP
ráno	ráno	k6eAd1
a	a	k8xC
večer	večer	k6eAd1
jako	jako	k9
oběť	oběť	k1gFnSc4
kadidlo	kadidlo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
oltář	oltář	k1gInSc4
byl	být	k5eAaImAgInS
malý	malý	k2eAgInSc1d1
stůl	stůl	k1gInSc1
z	z	k7c2
akáciového	akáciový	k2eAgNnSc2d1
dřeva	dřevo	k1gNnSc2
pokrytý	pokrytý	k2eAgInSc4d1
zlatem	zlato	k1gNnSc7
<g/>
;	;	kIx,
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
byl	být	k5eAaImAgInS
široký	široký	k2eAgInSc4d1
1	#num#	k4
loket	loket	k1gInSc4
<g/>
,	,	kIx,
vysoký	vysoký	k2eAgInSc4d1
2	#num#	k4
lokty	loket	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Podobné	podobný	k2eAgInPc4d1
rozměry	rozměr	k1gInPc4
měl	mít	k5eAaImAgInS
tento	tento	k3xDgInSc1
oltář	oltář	k1gInSc1
i	i	k9
v	v	k7c6
Šalomounově	Šalomounův	k2eAgInSc6d1
chrámu	chrám	k1gInSc6
<g/>
,	,	kIx,
avšak	avšak	k8xC
byl	být	k5eAaImAgInS
vyroben	vyrobit	k5eAaPmNgInS
z	z	k7c2
cedrového	cedrový	k2eAgNnSc2d1
dřeva	dřevo	k1gNnSc2
a	a	k8xC
byl	být	k5eAaImAgInS
taktéž	taktéž	k?
pokryt	pokrýt	k5eAaPmNgInS
zlatem	zlato	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
chrámu	chrám	k1gInSc6
obnoveném	obnovený	k2eAgInSc6d1
po	po	k7c6
babylonském	babylonský	k2eAgNnSc6d1
vyhnanství	vyhnanství	k1gNnSc6
byl	být	k5eAaImAgInS
znovu	znovu	k6eAd1
zřízen	zřídit	k5eAaPmNgInS
i	i	k8xC
kadidlový	kadidlový	k2eAgInSc1d1
oltář	oltář	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
odstranil	odstranit	k5eAaPmAgInS
až	až	k9
Antioch	Antioch	k1gInSc1
IV	IV	kA
<g/>
.	.	kIx.
a	a	k8xC
znovu	znovu	k6eAd1
zřídil	zřídit	k5eAaPmAgInS
Juda	judo	k1gNnPc4
Makabejský	makabejský	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
trofejemi	trofej	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
přinesl	přinést	k5eAaPmAgInS
z	z	k7c2
Jeruzaléma	Jeruzalém	k1gInSc2
po	po	k7c4
jeho	jeho	k3xOp3gNnSc4
dobytí	dobytí	k1gNnSc4
Titus	Titus	k1gInSc4
se	se	k3xPyFc4
však	však	k9
kadidlový	kadidlový	k2eAgInSc1d1
oltář	oltář	k1gInSc1
nenachází	nacházet	k5eNaImIp3nS
<g/>
,	,	kIx,
ačkoliv	ačkoliv	k8xS
se	se	k3xPyFc4
o	o	k7c6
něm	on	k3xPp3gInSc6
zmiňují	zmiňovat	k5eAaImIp3nP
pisatelé	pisatel	k1gMnPc1
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
tohoto	tento	k3xDgInSc2
oltáře	oltář	k1gInSc2
totiž	totiž	k9
měl	mít	k5eAaImAgInS
mít	mít	k5eAaImF
zjevení	zjevení	k1gNnSc4
anděla	anděl	k1gMnSc2
Zachariáš	Zachariáš	k1gMnSc2
<g/>
,	,	kIx,
když	když	k8xS
mu	on	k3xPp3gMnSc3
bylo	být	k5eAaImAgNnS
oznámeno	oznámit	k5eAaPmNgNnS
narození	narození	k1gNnSc1
Jana	Jan	k1gMnSc2
Křtitele	křtitel	k1gMnSc2
(	(	kIx(
<g/>
Lk	Lk	k1gFnSc1
1,11	1,11	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Oltář	Oltář	k1gInSc1
v	v	k7c6
křesťanství	křesťanství	k1gNnSc6
</s>
<s>
Nový	nový	k2eAgInSc1d1
zákon	zákon	k1gInSc1
používá	používat	k5eAaImIp3nS
slovo	slovo	k1gNnSc4
oltář	oltář	k1gInSc4
pro	pro	k7c4
vyjádření	vyjádření	k1gNnSc4
Ježíšovy	Ježíšův	k2eAgFnSc2d1
oběti	oběť	k1gFnSc2
na	na	k7c6
kříži	kříž	k1gInSc6
ve	v	k7c4
Sk	Sk	kA
14,13	14,13	k4
a	a	k8xC
Žd	Žd	k1gMnSc1
13,10	13,10	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
v	v	k7c6
křesťanství	křesťanství	k1gNnSc6
neexistuje	existovat	k5eNaImIp3nS
jiná	jiný	k2eAgFnSc1d1
oběť	oběť	k1gFnSc1
než	než	k8xS
oběť	oběť	k1gFnSc1
jediného	jediný	k2eAgMnSc2d1
kněze	kněz	k1gMnSc2
<g/>
,	,	kIx,
Ježíše	Ježíš	k1gMnSc2
Krista	Kristus	k1gMnSc2
<g/>
,	,	kIx,
nejsou	být	k5eNaImIp3nP
oltáře	oltář	k1gInPc4
křesťany	křesťan	k1gMnPc4
vnímány	vnímán	k2eAgMnPc4d1
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
v	v	k7c6
ostatních	ostatní	k2eAgNnPc6d1
náboženstvích	náboženství	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
oltář	oltář	k1gInSc4
také	také	k9
křesťané	křesťan	k1gMnPc1
v	v	k7c6
latině	latina	k1gFnSc6
neužívali	užívat	k5eNaImAgMnP
běžného	běžný	k2eAgMnSc4d1
slova	slovo	k1gNnPc1
ara	ara	k1gMnSc1
<g/>
,	,	kIx,
nýbrž	nýbrž	k8xC
zavedli	zavést	k5eAaPmAgMnP
nový	nový	k2eAgInSc4d1
název	název	k1gInSc4
altare	altar	k1gMnSc5
(	(	kIx(
<g/>
vyvýšené	vyvýšený	k2eAgNnSc4d1
místo	místo	k1gNnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oltář	Oltář	k1gInSc1
se	se	k3xPyFc4
v	v	k7c6
křesťanství	křesťanství	k1gNnSc6
používá	používat	k5eAaImIp3nS
jako	jako	k9
místo	místo	k1gNnSc1
<g/>
,	,	kIx,
na	na	k7c6
němž	jenž	k3xRgInSc6
se	se	k3xPyFc4
přinášejí	přinášet	k5eAaImIp3nP
dary	dar	k1gInPc1
chleba	chléb	k1gInSc2
a	a	k8xC
vína	víno	k1gNnSc2
při	při	k7c6
slavení	slavení	k1gNnSc6
eucharistie	eucharistie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
církvích	církev	k1gFnPc6
s	s	k7c7
vlastní	vlastní	k2eAgFnSc7d1
liturgickou	liturgický	k2eAgFnSc7d1
tradicí	tradice	k1gFnSc7
převládá	převládat	k5eAaImIp3nS
užívání	užívání	k1gNnSc1
kamenného	kamenný	k2eAgInSc2d1
oltáře	oltář	k1gInSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
však	však	k9
možné	možný	k2eAgNnSc1d1
se	se	k3xPyFc4
setkat	setkat	k5eAaPmF
i	i	k9
s	s	k7c7
oltáři	oltář	k1gInPc7
z	z	k7c2
jiného	jiný	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
a	a	k8xC
obvykle	obvykle	k6eAd1
jím	on	k3xPp3gInSc7
není	být	k5eNaImIp3nS
možné	možný	k2eAgNnSc1d1
hýbat	hýbat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnohé	mnohé	k1gNnSc1
katolické	katolický	k2eAgFnSc2d1
a	a	k8xC
pravoslavné	pravoslavný	k2eAgInPc4d1
oltáře	oltář	k1gInPc4
v	v	k7c6
sobě	sebe	k3xPyFc6
nesou	nést	k5eAaImIp3nP
ostatky	ostatek	k1gInPc1
svatých	svatá	k1gFnPc2
<g/>
;	;	kIx,
tento	tento	k3xDgInSc1
zvyk	zvyk	k1gInSc1
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
prvních	první	k4xOgNnPc2
křesťanských	křesťanský	k2eAgNnPc2d1
staletí	staletí	k1gNnPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
křesťané	křesťan	k1gMnPc1
mohli	moct	k5eAaImAgMnP
slavit	slavit	k5eAaImF
eucharistii	eucharistie	k1gFnSc4
na	na	k7c6
hrobech	hrob	k1gInPc6
mučedníků	mučedník	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Oltář	Oltář	k1gInSc1
v	v	k7c6
římskokatolické	římskokatolický	k2eAgFnSc6d1
církvi	církev	k1gFnSc6
</s>
<s>
Oltář	Oltář	k1gInSc1
se	se	k3xPyFc4
v	v	k7c6
katolické	katolický	k2eAgFnSc6d1
liturgické	liturgický	k2eAgFnSc6d1
tradici	tradice	k1gFnSc6
chápe	chápat	k5eAaImIp3nS
jako	jako	k9
symbol	symbol	k1gInSc1
Krista-	Krista-	k1gFnSc2
úhelného	úhelný	k2eAgInSc2d1
kamene	kámen	k1gInSc2
<g/>
,	,	kIx,
protože	protože	k8xS
představuje	představovat	k5eAaImIp3nS
místo	místo	k1gNnSc1
<g/>
,	,	kIx,
na	na	k7c6
němž	jenž	k3xRgInSc6
se	se	k3xPyFc4
zpřítomňuje	zpřítomňovat	k5eAaImIp3nS
jediná	jediný	k2eAgFnSc1d1
Ježíšova	Ježíšův	k2eAgFnSc1d1
oběť	oběť	k1gFnSc1
Otci	otec	k1gMnSc3
za	za	k7c4
spásu	spása	k1gFnSc4
světa	svět	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oltář	Oltář	k1gInSc4
jako	jako	k8xC,k8xS
místo	místo	k1gNnSc4
<g/>
,	,	kIx,
na	na	k7c6
němž	jenž	k3xRgInSc6
se	se	k3xPyFc4
slaví	slavit	k5eAaImIp3nS
oběť	oběť	k1gFnSc1
mše	mše	k1gFnSc2
svaté	svatý	k2eAgFnSc2d1
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
nazývaná	nazývaný	k2eAgFnSc1d1
eucharistie	eucharistie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Počet	počet	k1gInSc1
<g/>
,	,	kIx,
situace	situace	k1gFnSc1
a	a	k8xC
orientace	orientace	k1gFnSc1
-	-	kIx~
Je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
v	v	k7c6
chrámu	chrám	k1gInSc6
oltář	oltář	k1gInSc1
jen	jen	k9
jeden	jeden	k4xCgMnSc1
<g/>
,	,	kIx,
pak	pak	k6eAd1
stojí	stát	k5eAaImIp3nS
v	v	k7c6
presbytáři	presbytář	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
<g/>
-li	-li	k?
v	v	k7c6
chrámu	chrám	k1gInSc6
více	hodně	k6eAd2
oltářů	oltář	k1gInPc2
<g/>
,	,	kIx,
stojí	stát	k5eAaImIp3nS
v	v	k7c6
kaplích	kaple	k1gFnPc6
<g/>
,	,	kIx,
v	v	k7c6
chóru	chór	k1gInSc6
<g/>
,	,	kIx,
ve	v	k7c6
vícelodních	vícelodní	k2eAgInPc6d1
chrámech	chrám	k1gInPc6
také	také	k9
u	u	k7c2
sloupů	sloup	k1gInPc2
či	či	k8xC
pilířů	pilíř	k1gInPc2
mezilodní	mezilodní	k2eAgFnSc2d1
arkády	arkáda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Každý	každý	k3xTgInSc1
oltář	oltář	k1gInSc4
před	před	k7c7
jeho	jeho	k3xOp3gNnSc7
užíváním	užívání	k1gNnSc7
biskup	biskup	k1gMnSc1
žehná	žehnat	k5eAaImIp3nS
a	a	k8xC
pomaže	pomazat	k5eAaPmIp3nS
jej	on	k3xPp3gMnSc4
svěceným	svěcený	k2eAgInSc7d1
olejem	olej	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obřad	obřad	k1gInSc1
je	být	k5eAaImIp3nS
nutno	nutno	k6eAd1
opakovat	opakovat	k5eAaImF
<g/>
,	,	kIx,
pokud	pokud	k8xS
dojde	dojít	k5eAaPmIp3nS
k	k	k7c3
znesvěcení	znesvěcení	k1gNnSc3
(	(	kIx(
<g/>
pošpinění	pošpinění	k1gNnSc3
<g/>
)	)	kIx)
oltáře	oltář	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Až	až	k6eAd1
do	do	k7c2
liturgické	liturgický	k2eAgFnSc2d1
reformy	reforma	k1gFnSc2
Pavla	Pavel	k1gMnSc2
VI	VI	kA
<g/>
.	.	kIx.
na	na	k7c6
Druhém	druhý	k4xOgInSc6
vatikánském	vatikánský	k2eAgInSc6d1
koncilu	koncil	k1gInSc6
oltář	oltář	k1gInSc1
v	v	k7c6
katolickém	katolický	k2eAgInSc6d1
kostele	kostel	k1gInSc6
byl	být	k5eAaImAgInS
obvykle	obvykle	k6eAd1
přistavován	přistavovat	k5eAaImNgInS
k	k	k7c3
východní	východní	k2eAgFnSc3d1
zdi	zeď	k1gFnSc3
kostela	kostel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
starších	starý	k2eAgNnPc6d2
historických	historický	k2eAgNnPc6d1
obdobích	období	k1gNnPc6
k	k	k7c3
němu	on	k3xPp3gInSc3
vedly	vést	k5eAaImAgFnP
zpravidla	zpravidla	k6eAd1
tři	tři	k4xCgInPc4
stupně	stupeň	k1gInPc4
<g/>
,	,	kIx,
od	od	k7c2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
postačil	postačit	k5eAaPmAgMnS
jeden	jeden	k4xCgInSc4
schod	schod	k1gInSc4
nebo	nebo	k8xC
dřevěné	dřevěný	k2eAgNnSc4d1
plato	plato	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kněz	kněz	k1gMnSc1
i	i	k9
lid	lid	k1gInSc4
byli	být	k5eAaImAgMnP
ve	v	k7c6
většině	většina	k1gFnSc6
kostelů	kostel	k1gInPc2
otočeni	otočen	k2eAgMnPc1d1
směrem	směr	k1gInSc7
k	k	k7c3
východu	východ	k1gInSc3
či	či	k8xC
k	k	k7c3
oltářnímu	oltářní	k2eAgInSc3d1
kříži	kříž	k1gInSc3
(	(	kIx(
<g/>
symboly	symbol	k1gInPc1
Ježíše	Ježíš	k1gMnSc2
Krista	Kristus	k1gMnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Podoba	podoba	k1gFnSc1
a	a	k8xC
části	část	k1gFnSc2
-	-	kIx~
Základem	základ	k1gInSc7
je	být	k5eAaImIp3nS
oltářní	oltářní	k2eAgInSc1d1
stůl	stůl	k1gInSc1
(	(	kIx(
<g/>
menza	menza	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
původně	původně	k6eAd1
kamenný	kamenný	k2eAgInSc1d1
kvádr	kvádr	k1gInSc1
s	s	k7c7
dutinou	dutina	k1gFnSc7
nebo	nebo	k8xC
prohlubní	prohlubeň	k1gFnSc7
pro	pro	k7c4
relikvii	relikvie	k1gFnSc4
nebo	nebo	k8xC
pro	pro	k7c4
samostatnou	samostatný	k2eAgFnSc4d1
(	(	kIx(
<g/>
vyjímatelnou	vyjímatelný	k2eAgFnSc4d1
<g/>
)	)	kIx)
oltářní	oltářní	k2eAgFnSc4d1
desku	deska	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
tuto	tento	k3xDgFnSc4
relikvii	relikvie	k1gFnSc4
obsahuje	obsahovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
kryptách	krypta	k1gFnPc6
a	a	k8xC
kaplích	kaple	k1gFnPc6
může	moct	k5eAaImIp3nS
oltářní	oltářní	k2eAgFnSc4d1
menzu	menza	k1gFnSc4
nahrazovat	nahrazovat	k5eAaImF
náhrobní	náhrobní	k2eAgFnSc1d1
tumba	tumba	k1gFnSc1
světce	světec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Vybavení	vybavení	k1gNnSc1
oltáře	oltář	k1gInSc2
-	-	kIx~
Na	na	k7c4
oltářní	oltářní	k2eAgFnSc4d1
menzu	menza	k1gFnSc4
by	by	kYmCp3nS
se	se	k3xPyFc4
nemělo	mít	k5eNaImAgNnS
klást	klást	k5eAaImF
nic	nic	k3yNnSc1
<g/>
,	,	kIx,
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
nesouvisí	souviset	k5eNaImIp3nS
s	s	k7c7
funkcí	funkce	k1gFnSc7
oltáře	oltář	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obvykle	obvykle	k6eAd1
na	na	k7c6
něm	on	k3xPp3gInSc6
stojí	stát	k5eAaImIp3nS
dostatečně	dostatečně	k6eAd1
velký	velký	k2eAgInSc1d1
kříž	kříž	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
s	s	k7c7
vyobrazení	vyobrazení	k1gNnSc2
ukřižovaného	ukřižovaný	k2eAgMnSc4d1
Krista	Kristus	k1gMnSc4
a	a	k8xC
to	ten	k3xDgNnSc1
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
tzv.	tzv.	kA
korpus	korpus	k1gInSc1
je	být	k5eAaImIp3nS
obrácen	obrátit	k5eAaPmNgInS
směrem	směr	k1gInSc7
do	do	k7c2
oltáře	oltář	k1gInSc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
při	při	k7c6
celebraci	celebrace	k1gFnSc6
"	"	kIx"
<g/>
tváří	tvářet	k5eAaImIp3nS
k	k	k7c3
lidu	lid	k1gInSc3
<g/>
"	"	kIx"
směrem	směr	k1gInSc7
k	k	k7c3
celebrantovi	celebrant	k1gMnSc3
<g/>
,	,	kIx,
to	ten	k3xDgNnSc4
má	mít	k5eAaImIp3nS
viditelně	viditelně	k6eAd1
naznačovat	naznačovat	k5eAaImF
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
že	že	k8xS
mešní	mešní	k2eAgFnSc1d1
oběť	oběť	k1gFnSc1
je	být	k5eAaImIp3nS
nekrvavým	krvavý	k2eNgInSc7d1
znovu	znovu	k6eAd1
zpřítomněním	zpřítomnění	k1gNnSc7
krvavé	krvavý	k2eAgFnSc2d1
velkopáteční	velkopáteční	k2eAgFnSc2d1
výkupné	výkupný	k2eAgFnSc2d1
oběti	oběť	k1gFnSc2
Ježíše	Ježíš	k1gMnSc2
Krista	Kristus	k1gMnSc2
na	na	k7c6
kříži	kříž	k1gInSc6
a	a	k8xC
2-7	2-7	k4
svící	svíce	k1gFnPc2
ve	v	k7c6
svícnu	svícen	k1gInSc6
na	na	k7c6
znamení	znamení	k1gNnSc6
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
že	že	k8xS
Ježíš	Ježíš	k1gMnSc1
Kristus	Kristus	k1gMnSc1
<g/>
,	,	kIx,
v	v	k7c6
jehož	jehož	k3xOyRp3gInSc6
zastoupení	zastoupení	k1gNnSc6
(	(	kIx(
<g/>
in	in	k?
persona	persona	k1gFnSc1
Christi	Christ	k1gMnPc1
<g/>
)	)	kIx)
kněz	kněz	k1gMnSc1
mši	mše	k1gFnSc4
svatou	svatá	k1gFnSc4
na	na	k7c6
oltáři	oltář	k1gInSc6
slouží	sloužit	k5eAaImIp3nS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
Světlem	světlo	k1gNnSc7
světa	svět	k1gInSc2
a	a	k8xC
že	že	k8xS
jako	jako	k8xC,k8xS
se	se	k3xPyFc4
plamenem	plamen	k1gInSc7
stravuje	stravovat	k5eAaImIp3nS
svíce	svíce	k1gFnSc1
<g/>
,	,	kIx,
tak	tak	k6eAd1
i	i	k9
Ježíš	Ježíš	k1gMnSc1
Kristus	Kristus	k1gMnSc1
se	se	k3xPyFc4
vydal	vydat	k5eAaPmAgMnS
v	v	k7c4
oběť	oběť	k1gFnSc4
za	za	k7c4
lidstvo	lidstvo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oltář	Oltář	k1gInSc1
je	být	k5eAaImIp3nS
pravým	pravý	k2eAgInSc7d1
středem	střed	k1gInSc7
kostela	kostel	k1gInSc2
(	(	kIx(
<g/>
nikoli	nikoli	k9
matematickým	matematický	k2eAgMnPc3d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
místem	místo	k1gNnSc7
<g/>
,	,	kIx,
kolem	kolem	k7c2
něhož	jenž	k3xRgNnSc2
se	se	k3xPyFc4
křesťané	křesťan	k1gMnPc1
shromažďují	shromažďovat	k5eAaImIp3nP
ke	k	k7c3
společné	společný	k2eAgFnSc3d1
modlitbě	modlitba	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oltářní	oltářní	k2eAgFnSc1d1
menza	menza	k1gFnSc1
je	být	k5eAaImIp3nS
pokryta	pokrýt	k5eAaPmNgFnS
alepoň	aleponit	k5eAaPmRp2nS
jedním	jeden	k4xCgInSc7
oltářním	oltářní	k2eAgNnSc7d1
plátnem	plátno	k1gNnSc7
(	(	kIx(
<g/>
oltářní	oltářní	k2eAgFnSc7d1
pokrývkou	pokrývka	k1gFnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
nepoužívá	používat	k5eNaImIp3nS
pouze	pouze	k6eAd1
v	v	k7c6
průběhu	průběh	k1gInSc6
obřadů	obřad	k1gInPc2
Velkého	velký	k2eAgInSc2d1
pátku	pátek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
bohoslužby	bohoslužba	k1gFnSc2
na	na	k7c6
oltáři	oltář	k1gInSc6
může	moct	k5eAaImIp3nS
ležet	ležet	k5eAaImF
evangeliář	evangeliář	k1gInSc4
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
na	na	k7c4
něj	on	k3xPp3gMnSc4
přinášeny	přinášen	k2eAgInPc1d1
dary	dar	k1gInPc1
nekvašeného	kvašený	k2eNgInSc2d1
chleba	chléb	k1gInSc2
tzv.	tzv.	kA
hostie	hostie	k1gFnSc2
a	a	k8xC
vína	víno	k1gNnSc2
<g/>
,	,	kIx,
pod	pod	k7c4
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
se	se	k3xPyFc4
klade	klást	k5eAaImIp3nS
korporál	korporál	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svatostánek	svatostánek	k1gInSc1
s	s	k7c7
Nejsvětější	nejsvětější	k2eAgFnSc7d1
svátostí	svátost	k1gFnSc7
oltářní	oltářní	k2eAgFnSc7d1
<g/>
,	,	kIx,
tedy	tedy	k8xC
svatá	svatat	k5eAaImIp3nS
hostie-nekvašený	hostie-nekvašený	k2eAgInSc4d1
chléb	chléb	k1gInSc4
proměněný	proměněný	k2eAgInSc4d1
při	při	k7c6
slavení	slavení	k1gNnSc6
eucharistie	eucharistie	k1gFnSc2
v	v	k7c4
Tělo	tělo	k1gNnSc4
Ježíše	Ježíš	k1gMnSc2
Krista	Kristus	k1gMnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
podle	podle	k7c2
platných	platný	k2eAgInPc2d1
předpisů	předpis	k1gInPc2
použít	použít	k5eAaPmF
i	i	k9
při	při	k7c6
celebraci	celebrace	k1gFnSc6
tzv.	tzv.	kA
tváří	tvářet	k5eAaImIp3nS
k	k	k7c3
lidu	lid	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
běžně	běžně	k6eAd1
umožněna	umožnit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
1965	#num#	k4
dekretem	dekret	k1gInSc7
Inter	Inter	k1gInSc1
oecumenici	oecumenice	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
oltář	oltář	k1gInSc1
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
oddělen	oddělit	k5eAaPmNgInS
od	od	k7c2
retabula	retabulum	k1gNnSc2
a	a	k8xC
dá	dát	k5eAaPmIp3nS
se	se	k3xPyFc4
tak	tak	k9
volně	volně	k6eAd1
obcházet	obcházet	k5eAaImF
<g/>
,	,	kIx,
tento	tento	k3xDgInSc1
svatostánek	svatostánek	k1gInSc1
nesmí	smět	k5eNaImIp3nS
být	být	k5eAaImF
příliš	příliš	k6eAd1
velký	velký	k2eAgInSc1d1
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
tomu	ten	k3xDgNnSc3
bývalo	bývat	k5eAaImAgNnS
běžně	běžně	k6eAd1
dříve	dříve	k6eAd2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
při	při	k7c6
celebraci	celebrace	k1gFnSc6
orientované	orientovaný	k2eAgFnSc6d1
směrem	směr	k1gInSc7
k	k	k7c3
lidu	lid	k1gInSc3
věřícím	věřící	k1gMnSc7
zcela	zcela	k6eAd1
nezakrýval	zakrývat	k5eNaImAgMnS
pohled	pohled	k1gInSc4
na	na	k7c4
celebranta	celebrant	k1gMnSc4
a	a	k8xC
misál	misál	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
oltářní	oltářní	k2eAgFnSc6d1
menze	menza	k1gFnSc6
v	v	k7c6
historických	historický	k2eAgFnPc6d1
dobách	doba	k1gFnPc6
může	moct	k5eAaImIp3nS
stát	stát	k5eAaPmF,k5eAaImF
nástavec	nástavec	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nástavec	nástavec	k1gInSc1
se	se	k3xPyFc4
skládá	skládat	k5eAaImIp3nS
z	z	k7c2
predely	predela	k1gFnSc2
(	(	kIx(
<g/>
soklu	sokl	k1gInSc2
<g/>
,	,	kIx,
podnože	podnož	k1gInSc2
<g/>
)	)	kIx)
a	a	k8xC
retáblu	retábl	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
predelle	predella	k1gFnSc6
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
uprostřed	uprostřed	k6eAd1
vložen	vložit	k5eAaPmNgInS
svatostánek	svatostánek	k1gInSc4
s	s	k7c7
Nejsvětější	nejsvětější	k2eAgFnSc7d1
svátostí	svátost	k1gFnSc7
oltářní	oltářní	k2eAgMnPc1d1
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
tvoří	tvořit	k5eAaImIp3nS
svatá	svatý	k2eAgFnSc1d1
hostie	hostie	k1gFnSc1
viz	vidět	k5eAaImRp2nS
výše	vysoce	k6eAd2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svatostánek	svatostánek	k1gInSc1
mívá	mívat	k5eAaImIp3nS
podobu	podoba	k1gFnSc4
uzamykatelné	uzamykatelný	k2eAgFnSc2d1
schránky	schránka	k1gFnSc2
či	či	k8xC
skříňky	skříňka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Retábl	retábl	k1gInSc1
v	v	k7c6
době	doba	k1gFnSc6
středověku	středověk	k1gInSc2
a	a	k8xC
renesance	renesance	k1gFnSc1
tvořila	tvořit	k5eAaImAgFnS
zpravidla	zpravidla	k6eAd1
dřevěná	dřevěný	k2eAgFnSc1d1
oltářní	oltářní	k2eAgFnSc1d1
archa	archa	k1gFnSc1
<g/>
,	,	kIx,
vyzdobená	vyzdobený	k2eAgNnPc4d1
oltářním	oltářní	k2eAgInSc7d1
obrazem	obraz	k1gInSc7
či	či	k8xC
obrazy	obraz	k1gInPc7
<g/>
,	,	kIx,
v	v	k7c6
gotice	gotika	k1gFnSc6
často	často	k6eAd1
triptychem	triptychon	k1gNnSc7
nebo	nebo	k8xC
polyptychem	polyptych	k1gInSc7
<g/>
,	,	kIx,
nebo	nebo	k8xC
sochami	socha	k1gFnPc7
světců	světec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
době	doba	k1gFnSc6
barokní	barokní	k2eAgFnSc2d1
a	a	k8xC
pozdějších	pozdní	k2eAgMnPc2d2
bývá	bývat	k5eAaImIp3nS
v	v	k7c6
rámu	rám	k1gInSc6
nástavce	nástavec	k1gInSc2
vložen	vložit	k5eAaPmNgInS
jeden	jeden	k4xCgInSc1
hlavní	hlavní	k2eAgInSc1d1
oltářní	oltářní	k2eAgInSc1d1
obraz	obraz	k1gInSc1
mezi	mezi	k7c7
dvěma	dva	k4xCgFnPc7
sochami	socha	k1gFnPc7
svatých	svatý	k1gMnPc2
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
doplněn	doplnit	k5eAaPmNgInS
menším	malý	k2eAgInSc7d2
obrazem	obraz	k1gInSc7
v	v	k7c6
nástavci	nástavec	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
bazilikách	bazilika	k1gFnPc6
bývá	bývat	k5eAaImIp3nS
vrchol	vrchol	k1gInSc1
oltáře	oltář	k1gInSc2
ozdoben	ozdobit	k5eAaPmNgInS
korunou	koruna	k1gFnSc7
pod	pod	k7c7
baldachýnem	baldachýn	k1gInSc7
či	či	k8xC
samotným	samotný	k2eAgInSc7d1
baldachýnem	baldachýn	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvláštní	zvláštní	k2eAgInSc4d1
oltář	oltář	k1gInSc4
(	(	kIx(
<g/>
altare	altar	k1gMnSc5
privilegiatum	privilegiatum	k1gNnSc4
<g/>
)	)	kIx)
bývá	bývat	k5eAaImIp3nS
zasvěcen	zasvětit	k5eAaPmNgMnS
poutní	poutní	k2eAgMnSc1d1
ikoně	ikona	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
liturgické	liturgický	k2eAgFnSc6d1
reformě	reforma	k1gFnSc6
papeže	papež	k1gMnSc2
Pavla	Pavel	k1gMnSc2
VI	VI	kA
<g/>
.	.	kIx.
na	na	k7c6
Druhém	druhý	k4xOgInSc6
vatikánském	vatikánský	k2eAgInSc6d1
koncilu	koncil	k1gInSc6
je	být	k5eAaImIp3nS
předepsáno	předepsat	k5eAaPmNgNnS
oltáře	oltář	k1gInPc4
v	v	k7c6
nových	nový	k2eAgInPc6d1
katolických	katolický	k2eAgInPc6d1
kostelích	kostel	k1gInPc6
stavět	stavět	k5eAaImF
odděleně	odděleně	k6eAd1
od	od	k7c2
stěny	stěna	k1gFnSc2
viz	vidět	k5eAaImRp2nS
výše	výše	k1gFnPc4,k1gFnPc4wB
a	a	k8xC
obětní	obětní	k2eAgInSc4d1
stůl	stůl	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tím	ten	k3xDgNnSc7
je	být	k5eAaImIp3nS
prakticky	prakticky	k6eAd1
umožněno	umožnit	k5eAaPmNgNnS
celebrovat	celebrovat	k5eAaImF
tzv.	tzv.	kA
čelem	čelo	k1gNnSc7
k	k	k7c3
lidu	lid	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
ale	ale	k9
není	být	k5eNaImIp3nS
podmínkou	podmínka	k1gFnSc7
takového	takový	k3xDgInSc2
oltáře	oltář	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celebrace	Celebrace	k1gFnSc1
tzv.	tzv.	kA
zády	záda	k1gNnPc7
k	k	k7c3
lidu	lido	k1gNnSc3
je	být	k5eAaImIp3nS
legitimní	legitimní	k2eAgFnSc1d1
možnost	možnost	k1gFnSc1
slavení	slavení	k1gNnSc2
mše	mše	k1gFnSc2
svaté	svatý	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Není	být	k5eNaImIp3nS
již	již	k6eAd1
zdoben	zdoben	k2eAgInSc1d1
jako	jako	k8xS,k8xC
dříve	dříve	k6eAd2
obrazem	obraz	k1gInSc7
ani	ani	k8xC
sochou	socha	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svatostánek	svatostánek	k1gInSc1
nebývá	bývat	k5eNaImIp3nS
postaven	postavit	k5eAaPmNgInS
na	na	k7c6
oltáři	oltář	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
bývá	bývat	k5eAaImIp3nS
umístěn	umístit	k5eAaPmNgInS
samostatně	samostatně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
nestojí	stát	k5eNaImIp3nS
v	v	k7c6
presbytáři	presbytář	k1gInSc6
samostatně	samostatně	k6eAd1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
vložen	vložit	k5eAaPmNgInS
do	do	k7c2
stěny	stěna	k1gFnSc2
presbytáře	presbytář	k1gInSc2
či	či	k8xC
kaple	kaple	k1gFnSc2
uvnitř	uvnitř	k7c2
kostela	kostel	k1gInSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
jako	jako	k8xS,k8xC
tomu	ten	k3xDgNnSc3
někdy	někdy	k6eAd1
bývalo	bývat	k5eAaImAgNnS
již	již	k6eAd1
od	od	k7c2
středověku	středověk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oproti	oproti	k7c3
dřívější	dřívější	k2eAgFnSc3d1
mřížce	mřížka	k1gFnSc3
je	být	k5eAaImIp3nS
však	však	k9
kryt	krýt	k5eAaImNgInS
pevnými	pevný	k2eAgInPc7d1
neprůhlednými	průhledný	k2eNgInPc7d1
dvířky	dvířka	k1gNnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc1
předpisy	předpis	k1gInPc4
ovšem	ovšem	k9
oficiálně	oficiálně	k6eAd1
platí	platit	k5eAaImIp3nS
pouze	pouze	k6eAd1
pro	pro	k7c4
stavby	stavba	k1gFnPc4
nových	nový	k2eAgInPc2d1
kostelů	kostel	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
starých	starý	k2eAgInPc6d1
kostelích	kostel	k1gInPc6
se	se	k3xPyFc4
měl	mít	k5eAaImAgInS
uzpůsobit	uzpůsobit	k5eAaPmF
oltářní	oltářní	k2eAgInSc4d1
stůl	stůl	k1gInSc4
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
dal	dát	k5eAaPmAgInS
pohodlně	pohodlně	k6eAd1
obcházet	obcházet	k5eAaImF
<g/>
,	,	kIx,
a	a	k8xC
však	však	k9
pokud	pokud	k8xS
toto	tento	k3xDgNnSc1
nebylo	být	k5eNaImAgNnS
z	z	k7c2
historicko-estetického	historicko-estetický	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
možné	možný	k2eAgNnSc1d1
udělat	udělat	k5eAaPmF
<g/>
,	,	kIx,
musel	muset	k5eAaImAgMnS
se	se	k3xPyFc4
zachovat	zachovat	k5eAaPmF
původní	původní	k2eAgInSc4d1
oltář	oltář	k1gInSc4
a	a	k8xC
sloužit	sloužit	k5eAaImF
u	u	k7c2
něho	on	k3xPp3gMnSc2
zády	záda	k1gNnPc7
k	k	k7c3
lidu	lido	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dodnes	dodnes	k6eAd1
se	se	k3xPyFc4
ovšem	ovšem	k9
tyto	tento	k3xDgInPc1
stále	stále	k6eAd1
platné	platný	k2eAgInPc1d1
předpisy	předpis	k1gInPc1
z	z	k7c2
Říma	Řím	k1gInSc2
nedodržují	dodržovat	k5eNaImIp3nP
a	a	k8xC
i	i	k9
nově	nově	k6eAd1
se	se	k3xPyFc4
zavádí	zavádět	k5eAaImIp3nS
tvz	tvz	k?
<g/>
.	.	kIx.
obětní	obětní	k2eAgInPc4d1
stoly	stol	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
ve	v	k7c6
většině	většina	k1gFnSc6
případech	případ	k1gInPc6
ani	ani	k8xC
nesplňují	splňovat	k5eNaImIp3nP
předpisy	předpis	k1gInPc1
oltářních	oltářní	k2eAgInPc2d1
stolů	stol	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
zastánců	zastánce	k1gMnPc2
této	tento	k3xDgFnSc2
moderní	moderní	k2eAgFnSc2d1
vymoženosti	vymoženost	k1gFnSc2
se	se	k3xPyFc4
domnívá	domnívat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
pro	pro	k7c4
zřízení	zřízení	k1gNnSc4
obětního	obětní	k2eAgInSc2d1
stolu	stol	k1gInSc2
stačí	stačit	k5eAaBmIp3nS
pouze	pouze	k6eAd1
důvod	důvod	k1gInSc1
sloužení	sloužení	k1gNnSc2
tzv.	tzv.	kA
tváří	tvářet	k5eAaImIp3nS
k	k	k7c3
lidu	lid	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
interpretace	interpretace	k1gFnPc4
je	být	k5eAaImIp3nS
však	však	k9
mylná	mylný	k2eAgFnSc1d1
a	a	k8xC
důvod	důvod	k1gInSc1
tzv.	tzv.	kA
sloužit	sloužit	k5eAaImF
tváří	tvář	k1gFnSc7
k	k	k7c3
lidu	lid	k1gInSc3
je	být	k5eAaImIp3nS
chybně	chybně	k6eAd1
dávána	dáván	k2eAgFnSc1d1
nad	nad	k7c4
všechny	všechen	k3xTgInPc4
předpisy	předpis	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
jsou	být	k5eAaImIp3nP
tímto	tento	k3xDgInSc7
porušovány	porušován	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s>
Typy	typ	k1gInPc1
oltářů	oltář	k1gInPc2
</s>
<s>
stabilní	stabilní	k2eAgFnSc1d1
-	-	kIx~
je	být	k5eAaImIp3nS
popsán	popsat	k5eAaPmNgInS
výše	vysoce	k6eAd2
</s>
<s>
přenosný	přenosný	k2eAgInSc4d1
(	(	kIx(
<g/>
altare	altar	k1gMnSc5
portatile	portatile	k1gNnPc7
<g/>
)	)	kIx)
-	-	kIx~
lze	lze	k6eAd1
ho	on	k3xPp3gMnSc4
přemísťovat	přemísťovat	k5eAaImF
v	v	k7c6
rámci	rámec	k1gInSc6
kostela	kostel	k1gInSc2
<g/>
,	,	kIx,
kaplí	kaple	k1gFnSc7
či	či	k8xC
do	do	k7c2
kaple	kaple	k1gFnSc2
domácí	domácí	k2eAgFnSc2d1
</s>
<s>
cestovní	cestovní	k2eAgFnSc1d1
(	(	kIx(
<g/>
altare	altar	k1gMnSc5
viaticum	viaticum	k1gInSc4
<g/>
)	)	kIx)
-	-	kIx~
oltářní	oltářní	k2eAgFnSc1d1
deska	deska	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
bohoslužbám	bohoslužba	k1gFnPc3
na	na	k7c6
cestách	cesta	k1gFnPc6
<g/>
,	,	kIx,
například	například	k6eAd1
při	při	k7c6
válečných	válečný	k2eAgFnPc6d1
výpravách	výprava	k1gFnPc6
<g/>
;	;	kIx,
k	k	k7c3
jeho	jeho	k3xOp3gNnSc3
užívání	užívání	k1gNnSc3
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
oficiálního	oficiální	k2eAgNnSc2d1
církevního	církevní	k2eAgNnSc2d1
povolení	povolení	k1gNnSc2
</s>
<s>
Oltář	Oltář	k1gInSc1
ve	v	k7c6
východních	východní	k2eAgFnPc6d1
církvích	církev	k1gFnPc6
</s>
<s>
Ve	v	k7c6
východních	východní	k2eAgFnPc6d1
církvích	církev	k1gFnPc6
se	se	k3xPyFc4
slovem	slovem	k6eAd1
oltář	oltář	k1gInSc1
obvykle	obvykle	k6eAd1
označuje	označovat	k5eAaImIp3nS
celý	celý	k2eAgInSc4d1
prostor	prostor	k1gInSc4
za	za	k7c7
ikonostasem	ikonostas	k1gInSc7
(	(	kIx(
<g/>
tedy	tedy	k9
svatyně	svatyně	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
svatyni	svatyně	k1gFnSc6
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
samotný	samotný	k2eAgInSc1d1
oltář	oltář	k1gInSc1
<g/>
,	,	kIx,
svatý	svatý	k2eAgInSc1d1
stůl	stůl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oltář	Oltář	k1gInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
kostelích	kostel	k1gInPc6
východních	východní	k2eAgFnPc2d1
církví	církev	k1gFnPc2
obvykle	obvykle	k6eAd1
asi	asi	k9
metr	metr	k1gInSc1
vysoký	vysoký	k2eAgInSc1d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
vyroben	vyrobit	k5eAaPmNgInS
ze	z	k7c2
dřeva	dřevo	k1gNnSc2
nebo	nebo	k8xC
z	z	k7c2
kamene	kámen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozměr	rozměr	k1gInSc1
oltáře	oltář	k1gInSc2
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nS
<g/>
,	,	kIx,
avšak	avšak	k8xC
je	být	k5eAaImIp3nS
vždy	vždy	k6eAd1
čtvercový	čtvercový	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stůl	stůl	k1gInSc1
má	mít	k5eAaImIp3nS
pět	pět	k4xCc4
nohou	noha	k1gFnPc2
<g/>
;	;	kIx,
kromě	kromě	k7c2
obvyklých	obvyklý	k2eAgInPc2d1
čtyř	čtyři	k4xCgInPc2
prostřední	prostřední	k2eAgInPc1d1
sloupek	sloupek	k1gInSc1
podpírá	podpírat	k5eAaImIp3nS
oltářové	oltářový	k2eAgInPc4d1
ostatky	ostatek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
oltáři	oltář	k1gInSc6
je	být	k5eAaImIp3nS
rozloženo	rozložen	k2eAgNnSc1d1
lněné	lněný	k2eAgNnSc1d1
plátno	plátno	k1gNnSc1
přivázané	přivázaný	k2eAgNnSc1d1
k	k	k7c3
oltáři	oltář	k1gInSc3
<g/>
;	;	kIx,
toto	tento	k3xDgNnSc1
plátno	plátno	k1gNnSc1
se	se	k3xPyFc4
po	po	k7c6
posvěcení	posvěcení	k1gNnSc6
oltáře	oltář	k1gInSc2
nesundává	sundávat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
této	tento	k3xDgFnSc6
první	první	k4xOgFnSc6
přikrývce	přikrývka	k1gFnSc6
je	být	k5eAaImIp3nS
další	další	k2eAgFnSc1d1
zdobená	zdobený	k2eAgFnSc1d1
pokrývka	pokrývka	k1gFnSc1
<g/>
,	,	kIx,
často	často	k6eAd1
v	v	k7c6
příslušné	příslušný	k2eAgFnSc6d1
liturgické	liturgický	k2eAgFnSc6d1
barvě	barva	k1gFnSc6
podle	podle	k7c2
liturgického	liturgický	k2eAgNnSc2d1
období	období	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
oltáři	oltář	k1gInSc6
je	být	k5eAaImIp3nS
postaven	postavit	k5eAaPmNgInS
svatostánek	svatostánek	k1gInSc1
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
ve	v	k7c6
tvaru	tvar	k1gInSc6
kostela	kostel	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
se	se	k3xPyFc4
uchovává	uchovávat	k5eAaImIp3nS
svátost	svátost	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
roznášet	roznášet	k5eAaImF
nemocným	nemocný	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
oltáři	oltář	k1gInSc6
leží	ležet	k5eAaImIp3nS
evangeliář	evangeliář	k1gInSc1
a	a	k8xC
antimension	antimension	k1gInSc1
<g/>
,	,	kIx,
hedvábný	hedvábný	k2eAgInSc1d1
kus	kus	k1gInSc1
látky	látka	k1gFnSc2
s	s	k7c7
ikonou	ikona	k1gFnSc7
Krista	Kristus	k1gMnSc2
připraveného	připravený	k2eAgInSc2d1
k	k	k7c3
pohřbu	pohřeb	k1gInSc2
<g/>
,	,	kIx,
do	do	k7c2
něhož	jenž	k3xRgMnSc2
je	být	k5eAaImIp3nS
všit	všit	k2eAgInSc1d1
ostatek	ostatek	k1gInSc1
svatého	svatý	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Božská	božský	k2eAgFnSc1d1
liturgie	liturgie	k1gFnSc1
se	se	k3xPyFc4
slouží	sloužit	k5eAaImIp3nS
na	na	k7c6
antimensiu	antimensium	k1gNnSc6
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
je	být	k5eAaImIp3nS
oltář	oltář	k1gInSc1
posvěcen	posvěcen	k2eAgInSc1d1
a	a	k8xC
má	mít	k5eAaImIp3nS
vlastní	vlastní	k2eAgInPc4d1
ostatky	ostatek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Oltáře	Oltář	k1gInPc1
se	se	k3xPyFc4
smí	smět	k5eAaImIp3nP
dotknout	dotknout	k5eAaPmF
pouze	pouze	k6eAd1
muž	muž	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
přijal	přijmout	k5eAaPmAgMnS
svěcení	svěcení	k1gNnSc4
<g/>
,	,	kIx,
a	a	k8xC
nic	nic	k3yNnSc1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
nebylo	být	k5eNaImAgNnS
posvěceno	posvětit	k5eAaPmNgNnS
nebo	nebo	k8xC
nemá	mít	k5eNaImIp3nS
být	být	k5eAaImF
posvěceno	posvěcen	k2eAgNnSc1d1
<g/>
,	,	kIx,
na	na	k7c4
něj	on	k3xPp3gMnSc4
nesmí	smět	k5eNaImIp3nS
být	být	k5eAaImF
položeno	položit	k5eAaPmNgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
obvyklé	obvyklý	k2eAgNnSc1d1
žehnat	žehnat	k5eAaImF
ikony	ikona	k1gFnPc4
nebo	nebo	k8xC
dary	dar	k1gInPc4
jejich	jejich	k3xOp3gInPc7
položením	položení	k1gNnSc7
na	na	k7c4
oltář	oltář	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Oltáře	Oltář	k1gInSc2
se	se	k3xPyFc4
užívá	užívat	k5eAaImIp3nS
jako	jako	k9
místa	místo	k1gNnSc2
oběti	oběť	k1gFnSc2
při	při	k7c6
slavení	slavení	k1gNnSc6
eucharistie	eucharistie	k1gFnSc2
<g/>
,	,	kIx,
při	při	k7c6
níž	jenž	k3xRgFnSc6
se	se	k3xPyFc4
Bohu	bůh	k1gMnSc3
Otci	otec	k1gMnSc3
přináší	přinášet	k5eAaImIp3nS
chléb	chléb	k1gInSc4
a	a	k8xC
víno	víno	k1gNnSc4
a	a	k8xC
svolává	svolávat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
ně	on	k3xPp3gNnSc4
Duch	duch	k1gMnSc1
Svatý	svatý	k1gMnSc1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
v	v	k7c6
nich	on	k3xPp3gMnPc6
zpřítomnil	zpřítomnit	k5eAaPmAgMnS
Ježíše	Ježíš	k1gMnSc4
Krista	Kristus	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
oltáře	oltář	k1gInSc2
stojí	stát	k5eAaImIp3nS
také	také	k9
kněz	kněz	k1gMnSc1
předsedající	předsedající	k1gMnSc1
společné	společný	k2eAgFnSc3d1
modlitbě	modlitba	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Oltář	Oltář	k1gInSc1
a	a	k8xC
protestantská	protestantský	k2eAgFnSc1d1
reformace	reformace	k1gFnSc1
</s>
<s>
Pro	pro	k7c4
protestantské	protestantský	k2eAgMnPc4d1
reformátory	reformátor	k1gMnPc4
ztrácí	ztrácet	k5eAaImIp3nS
oltář	oltář	k1gInSc1
význam	význam	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důraz	důraz	k1gInSc1
na	na	k7c4
slyšení	slyšení	k1gNnSc4
Božího	boží	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
při	při	k7c6
bohoslužbě	bohoslužba	k1gFnSc6
staví	stavit	k5eAaBmIp3nS,k5eAaPmIp3nS,k5eAaImIp3nS
do	do	k7c2
větší	veliký	k2eAgFnSc2d2
pozornosti	pozornost	k1gFnSc2
kazatelnu	kazatelna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
luterských	luterský	k2eAgInPc6d1
kostelech	kostel	k1gInPc6
je	být	k5eAaImIp3nS
výkladu	výklad	k1gInSc2
Slova	slovo	k1gNnSc2
(	(	kIx(
<g/>
kázání	kázání	k1gNnSc1
<g/>
)	)	kIx)
věnována	věnován	k2eAgFnSc1d1
stejná	stejný	k2eAgFnSc1d1
pozornost	pozornost	k1gFnSc1
jako	jako	k9
večeři	večeře	k1gFnSc4
Páně	páně	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kazatelna	kazatelna	k1gFnSc1
proto	proto	k8xC
stojí	stát	k5eAaImIp3nS
v	v	k7c6
těsné	těsný	k2eAgFnSc6d1
blízkosti	blízkost	k1gFnSc6
oltáře	oltář	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
protestanty	protestant	k1gMnPc4
je	být	k5eAaImIp3nS
kazatelnový	kazatelnový	k2eAgInSc1d1
oltář	oltář	k1gInSc1
svatým	svatý	k2eAgNnSc7d1
místem	místo	k1gNnSc7
<g/>
,	,	kIx,
z	z	k7c2
něhož	jenž	k3xRgNnSc2
je	být	k5eAaImIp3nS
Kristus	Kristus	k1gMnSc1
věřícím	věřící	k2eAgInPc3d1
nablízku	nablízku	k6eAd1
při	při	k7c6
kázání	kázání	k1gNnSc6
<g/>
,	,	kIx,
modlitbě	modlitba	k1gFnSc6
<g/>
,	,	kIx,
večeři	večeře	k1gFnSc6
Páně	páně	k2eAgNnSc2d1
a	a	k8xC
požehnání	požehnání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
reformovaných	reformovaný	k2eAgFnPc6d1
církvích	církev	k1gFnPc6
<g/>
,	,	kIx,
u	u	k7c2
baptistů	baptista	k1gMnPc2
a	a	k8xC
u	u	k7c2
tzv.	tzv.	kA
svobodných	svobodný	k2eAgFnPc2d1
církví	církev	k1gFnPc2
neexistuje	existovat	k5eNaImIp3nS
oltář	oltář	k1gInSc1
vůbec	vůbec	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jeho	jeho	k3xOp3gNnSc6
místě	místo	k1gNnSc6
stojí	stát	k5eAaImIp3nS
hlásání	hlásání	k1gNnSc4
slova	slovo	k1gNnSc2
jako	jako	k9
střed	střed	k1gInSc4
bohoslužby	bohoslužba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
slavení	slavení	k1gNnSc3
večeře	večeře	k1gFnSc1
Páně	páně	k2eAgFnSc1d1
pak	pak	k6eAd1
slouží	sloužit	k5eAaImIp3nS
jakýkoli	jakýkoli	k3yIgInSc1
stolek	stolek	k1gInSc1
<g/>
,	,	kIx,
zvaný	zvaný	k2eAgInSc1d1
Stůl	stůl	k1gInSc1
Páně	páně	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Ex	ex	k6eAd1
20	#num#	k4
<g/>
,	,	kIx,
24	#num#	k4
<g/>
–	–	k?
<g/>
25	#num#	k4
(	(	kIx(
<g/>
Kral	Kral	k1gMnSc1
<g/>
,	,	kIx,
ČEP	Čep	k1gMnSc1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Gn	Gn	k1gFnSc1
22	#num#	k4
<g/>
,	,	kIx,
9	#num#	k4
(	(	kIx(
<g/>
Kral	Kral	k1gMnSc1
<g/>
,	,	kIx,
ČEP	Čep	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Ez	Ez	k1gFnSc1
6	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
(	(	kIx(
<g/>
Kral	Kral	k1gMnSc1
<g/>
,	,	kIx,
ČEP	Čep	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
2	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
Kr	Kr	k1gFnSc1
23	#num#	k4
<g/>
,	,	kIx,
12	#num#	k4
(	(	kIx(
<g/>
Kral	Kral	k1gMnSc1
<g/>
,	,	kIx,
ČEP	Čep	k1gMnSc1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Gn	Gn	k1gFnSc1
8	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
(	(	kIx(
<g/>
Kral	Kral	k1gMnSc1
<g/>
,	,	kIx,
ČEP	Čep	k1gMnSc1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Gn	Gn	k1gFnSc1
12	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
(	(	kIx(
<g/>
Kral	Kral	k1gMnSc1
<g/>
,	,	kIx,
ČEP	Čep	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Gn	Gn	k1gFnSc1
13	#num#	k4
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
4	#num#	k4
(	(	kIx(
<g/>
Kral	Kral	k1gMnSc1
<g/>
,	,	kIx,
ČEP	Čep	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Gn	Gn	k1gFnSc1
22	#num#	k4
<g/>
,	,	kIx,
9	#num#	k4
(	(	kIx(
<g/>
Kral	Kral	k1gMnSc1
<g/>
,	,	kIx,
ČEP	Čep	k1gMnSc1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Gn	Gn	k1gFnSc1
26	#num#	k4
<g/>
,	,	kIx,
25	#num#	k4
(	(	kIx(
<g/>
Kral	Kral	k1gMnSc1
<g/>
,	,	kIx,
ČEP	Čep	k1gMnSc1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Gn	Gn	k1gFnSc1
33	#num#	k4
<g/>
,	,	kIx,
20	#num#	k4
(	(	kIx(
<g/>
Kral	Kral	k1gMnSc1
<g/>
,	,	kIx,
ČEP	Čep	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Gn	Gn	k1gFnSc1
35	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
–	–	k?
<g/>
3	#num#	k4
(	(	kIx(
<g/>
Kral	Kral	k1gMnSc1
<g/>
,	,	kIx,
ČEP	Čep	k1gMnSc1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Ex	ex	k6eAd1
17	#num#	k4
<g/>
,	,	kIx,
15	#num#	k4
(	(	kIx(
<g/>
Kral	Kral	k1gMnSc1
<g/>
,	,	kIx,
ČEP	Čep	k1gMnSc1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
COHEN	COHEN	kA
<g/>
,	,	kIx,
Abraham	Abraham	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Talmud	talmud	k1gInSc1
(	(	kIx(
<g/>
pro	pro	k7c4
každého	každý	k3xTgMnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Sefer	Sefer	k1gMnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85924	#num#	k4
<g/>
-	-	kIx~
<g/>
49	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
450	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Ex	ex	k6eAd1
30	#num#	k4
<g/>
,	,	kIx,
28	#num#	k4
(	(	kIx(
<g/>
Kral	Kral	k1gMnSc1
<g/>
,	,	kIx,
ČEP	Čep	k1gMnSc1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Mal	málit	k5eAaImRp2nS
1	#num#	k4
<g/>
,	,	kIx,
7	#num#	k4
(	(	kIx(
<g/>
Kral	Kral	k1gMnSc1
<g/>
,	,	kIx,
ČEP	Čep	k1gMnSc1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
2	#num#	k4
<g/>
Kron	Kron	k1gMnSc1
4	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
(	(	kIx(
<g/>
Kral	Kral	k1gMnSc1
<g/>
,	,	kIx,
ČEP	Čep	k1gMnSc1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Ex	ex	k6eAd1
30	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
<g/>
–	–	k?
<g/>
10	#num#	k4
(	(	kIx(
<g/>
Kral	Kral	k1gMnSc1
<g/>
,	,	kIx,
ČEP	Čep	k1gMnSc1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Ex	ex	k6eAd1
37	#num#	k4
<g/>
,	,	kIx,
25	#num#	k4
<g/>
–	–	k?
<g/>
26	#num#	k4
(	(	kIx(
<g/>
Kral	Kral	k1gMnSc1
<g/>
,	,	kIx,
ČEP	Čep	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Joseph	Joseph	k1gMnSc1
Braun	Braun	k1gMnSc1
<g/>
,	,	kIx,
Der	drát	k5eAaImRp2nS
christliche	christliche	k1gInSc4
Altar	Altar	k1gInSc1
in	in	k?
seiner	seiner	k1gInSc1
geschichtlichen	geschichtlichen	k2eAgMnSc1d1
Entwicklung	Entwicklung	k1gMnSc1
<g/>
,	,	kIx,
2	#num#	k4
<g/>
díly	díl	k1gInPc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
:	:	kIx,
Arten	Artna	k1gFnPc2
<g/>
,	,	kIx,
Bestandteile	Bestandteila	k1gFnSc6
<g/>
,	,	kIx,
Altargrab	Altargraba	k1gFnPc2
<g/>
,	,	kIx,
Weihe	Weihe	k1gFnPc2
<g/>
,	,	kIx,
Symbolik	symbolika	k1gFnPc2
<g/>
;	;	kIx,
München	München	k1gInSc1
<g/>
,	,	kIx,
1924	#num#	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Ciboriový	Ciboriový	k2eAgInSc1d1
oltář	oltář	k1gInSc1
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1
oltář	oltář	k1gInSc1
</s>
<s>
Antependium	antependium	k1gNnSc1
</s>
<s>
Modlitba	modlitba	k1gFnSc1
</s>
<s>
Bohoslužba	bohoslužba	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
oltář	oltář	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Galerie	galerie	k1gFnSc1
oltář	oltář	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
oltář	oltář	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Kostelní	kostelní	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
Kostelní	kostelní	k2eAgFnSc2d1
typy	typa	k1gFnSc2
</s>
<s>
Bazilika	bazilika	k1gFnSc1
•	•	k?
Gotická	gotický	k2eAgFnSc1d1
katedrála	katedrála	k1gFnSc1
•	•	k?
Halový	halový	k2eAgInSc1d1
kostel	kostel	k1gInSc1
•	•	k?
Pseudobazilika	Pseudobazilika	k1gFnSc1
•	•	k?
Rotunda	rotunda	k1gFnSc1
Lodě	loď	k1gFnSc2
a	a	k8xC
jejich	jejich	k3xOp3gInPc1
druhy	druh	k1gInPc1
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1
loď	loď	k1gFnSc1
•	•	k?
Boční	boční	k2eAgFnSc1d1
loď	loď	k1gFnSc1
•	•	k?
Transept	transept	k1gInSc1
•	•	k?
Křížení	křížení	k1gNnSc2
•	•	k?
Dvojlodí	Dvojlodí	k1gNnSc2
•	•	k?
Trojlodí	trojlodí	k1gNnSc2
•	•	k?
Vícelodní	vícelodní	k2eAgInSc4d1
kostel	kostel	k1gInSc4
Prostory	prostora	k1gFnSc2
kostela	kostel	k1gInSc2
</s>
<s>
Apsida	apsida	k1gFnSc1
•	•	k?
Apsidová	apsidový	k2eAgFnSc1d1
kaple	kaple	k1gFnSc1
•	•	k?
Empora	empora	k1gFnSc1
•	•	k?
Chór	chór	k1gInSc1
•	•	k?
Chórový	chórový	k2eAgInSc1d1
ochoz	ochoz	k1gInSc1
•	•	k?
Kaple	kaple	k1gFnSc1
•	•	k?
Kněžiště	kněžiště	k1gNnSc2
•	•	k?
Kruchta	kruchta	k1gFnSc1
•	•	k?
Krypta	krypta	k1gFnSc1
•	•	k?
Sakristie	sakristie	k1gFnSc2
•	•	k?
Věž	věž	k1gFnSc4
Jiné	jiný	k2eAgInPc4d1
architektonické	architektonický	k2eAgInPc4d1
prvky	prvek	k1gInPc4
</s>
<s>
Ambon	ambon	k1gInSc1
•	•	k?
Baldachýn	baldachýn	k1gInSc1
•	•	k?
Fiála	fiála	k1gFnSc1
•	•	k?
Chrlič	chrlič	k1gInSc1
•	•	k?
Ikonostas	ikonostas	k1gInSc1
•	•	k?
Kazatelna	kazatelna	k1gFnSc1
•	•	k?
Klenba	klenba	k1gFnSc1
•	•	k?
Krab	krab	k1gInSc1
•	•	k?
Kružba	kružba	k1gFnSc1
•	•	k?
Křížová	Křížová	k1gFnSc1
kytka	kytka	k1gFnSc1
•	•	k?
Kupole	kupole	k1gFnSc1
•	•	k?
Lektorium	Lektorium	k1gNnSc1
•	•	k?
Lucerna	lucerna	k1gFnSc1
•	•	k?
Okulus	Okulus	k1gInSc1
•	•	k?
Oltář	Oltář	k1gInSc1
•	•	k?
Oltářní	oltářní	k2eAgFnSc1d1
mřížka	mřížka	k1gFnSc1
•	•	k?
Opěrný	opěrný	k2eAgInSc1d1
systém	systém	k1gInSc1
•	•	k?
Portál	portál	k1gInSc1
•	•	k?
Portikus	portikus	k1gInSc1
•	•	k?
Přípora	přípora	k1gFnSc1
•	•	k?
Rozeta	rozeta	k1gFnSc1
•	•	k?
Sanktusník	sanktusník	k1gInSc1
•	•	k?
Triforium	triforium	k1gNnSc4
•	•	k?
Tympanon	tympanon	k1gInSc1
•	•	k?
Vítězný	vítězný	k2eAgInSc1d1
oblouk	oblouk	k1gInSc1
•	•	k?
Vitráž	vitráž	k1gFnSc1
•	•	k?
Výklenek	výklenek	k1gInSc1
(	(	kIx(
<g/>
Nika	nika	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Žebro	žebro	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4001381-9	4001381-9	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85003895	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85003895	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Dřevo	dřevo	k1gNnSc1
a	a	k8xC
nábytek	nábytek	k1gInSc1
|	|	kIx~
Křesťanství	křesťanství	k1gNnSc1
</s>
