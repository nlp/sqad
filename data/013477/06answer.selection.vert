<s>
Oltář	Oltář	k1gInSc1
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
מ	מ	kIx~
<g/>
ִ	ִ	kIx~
<g/>
ז	ז	kIx~
<g/>
ְ	ְ	kIx~
<g/>
ב	ב	kIx~
<g/>
ֵ	ֵ	kIx~
<g/>
ּ	ּ	kIx~
<g/>
ח	ח	kIx~
<g/>
ַ	ַ	kIx~
<g/>
,	,	kIx,
mizbeach	mizbeach	k1gInSc1
<g/>
,	,	kIx,
řecky	řecky	k6eAd1
β	β	kA
<g/>
,	,	kIx,
béma	béma	k1gFnSc1
<g/>
,	,	kIx,
latinsky	latinsky	k6eAd1
ara	ara	k1gInSc1
nebo	nebo	k8xC
altaria	altarium	k1gNnPc1
<g/>
,	,	kIx,
v	v	k7c6
křesťanském	křesťanský	k2eAgInSc6d1
užití	užití	k1gNnSc2
altare	altare	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
místo	místo	k1gNnSc1
<g/>
,	,	kIx,
na	na	k7c6
němž	jenž	k3xRgInSc6
se	se	k3xPyFc4
v	v	k7c6
náboženství	náboženství	k1gNnSc6
přináší	přinášet	k5eAaImIp3nS
dar	dar	k1gInSc1
jako	jako	k8xS,k8xC
oběť	oběť	k1gFnSc1
<g/>
,	,	kIx,
obvykle	obvykle	k6eAd1
bohu	bůh	k1gMnSc3
či	či	k8xC
božstvu	božstvo	k1gNnSc6
<g/>
.	.	kIx.
</s>