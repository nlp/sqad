<s>
Pozitiv	pozitiv	k1gInSc1	pozitiv
(	(	kIx(	(
<g/>
z	z	k7c2	z
latinského	latinský	k2eAgNnSc2d1	latinské
ponere	ponrat	k5eAaPmIp3nS	ponrat
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
položit	položit	k5eAaPmF	položit
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
malé	malý	k2eAgInPc4d1	malý
přenosné	přenosný	k2eAgInPc4d1	přenosný
varhany	varhany	k1gInPc4	varhany
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
vybavené	vybavený	k2eAgInPc1d1	vybavený
pouze	pouze	k6eAd1	pouze
retnými	retný	k2eAgFnPc7d1	retný
píšťalami	píšťala	k1gFnPc7	píšťala
a	a	k8xC	a
jedním	jeden	k4xCgInSc7	jeden
manuálem	manuál	k1gInSc7	manuál
(	(	kIx(	(
<g/>
klaviaturou	klaviatura	k1gFnSc7	klaviatura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
