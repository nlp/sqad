<p>
<s>
Pozitiv	pozitiv	k1gInSc1	pozitiv
(	(	kIx(	(
<g/>
z	z	k7c2	z
latinského	latinský	k2eAgNnSc2d1	latinské
ponere	ponrat	k5eAaPmIp3nS	ponrat
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
položit	položit	k5eAaPmF	položit
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
malé	malý	k2eAgInPc4d1	malý
přenosné	přenosný	k2eAgInPc4d1	přenosný
varhany	varhany	k1gInPc4	varhany
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
vybavené	vybavený	k2eAgInPc1d1	vybavený
pouze	pouze	k6eAd1	pouze
retnými	retný	k2eAgFnPc7d1	retný
píšťalami	píšťala	k1gFnPc7	píšťala
a	a	k8xC	a
jedním	jeden	k4xCgInSc7	jeden
manuálem	manuál	k1gInSc7	manuál
(	(	kIx(	(
<g/>
klaviaturou	klaviatura	k1gFnSc7	klaviatura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
píšťaly	píšťala	k1gFnPc4	píšťala
jazykové	jazykový	k2eAgNnSc4d1	jazykové
a	a	k8xC	a
pedál	pedál	k1gInSc4	pedál
(	(	kIx(	(
<g/>
nožní	nožní	k2eAgFnSc1d1	nožní
klaviatura	klaviatura	k1gFnSc1	klaviatura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
termín	termín	k1gInSc1	termín
pozitiv	pozitiv	k1gInSc4	pozitiv
začal	začít	k5eAaPmAgInS	začít
používat	používat	k5eAaImF	používat
i	i	k9	i
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
části	část	k1gFnSc2	část
velkých	velký	k2eAgFnPc2d1	velká
kostelních	kostelní	k2eAgFnPc2d1	kostelní
varhan	varhany	k1gFnPc2	varhany
–	–	k?	–
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
menší	malý	k2eAgInSc4d2	menší
stroj	stroj	k1gInSc4	stroj
umístěný	umístěný	k2eAgInSc4d1	umístěný
na	na	k7c6	na
zábradlí	zábradlí	k1gNnSc6	zábradlí
kůru	kůr	k1gInSc2	kůr
nebo	nebo	k8xC	nebo
v	v	k7c6	v
přední	přední	k2eAgFnSc6d1	přední
části	část	k1gFnSc6	část
hlavní	hlavní	k2eAgFnSc2d1	hlavní
varhanní	varhanní	k2eAgFnSc2d1	varhanní
skříně	skříň	k1gFnSc2	skříň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přenosné	přenosný	k2eAgInPc1d1	přenosný
pozitivy	pozitiv	k1gInPc1	pozitiv
byly	být	k5eAaImAgInP	být
využívané	využívaný	k2eAgFnPc4d1	využívaná
především	především	k6eAd1	především
v	v	k7c6	v
menších	malý	k2eAgInPc6d2	menší
kostelech	kostel	k1gInPc6	kostel
<g/>
,	,	kIx,	,
kaplích	kaple	k1gFnPc6	kaple
<g/>
,	,	kIx,	,
sálech	sál	k1gInPc6	sál
a	a	k8xC	a
bohatých	bohatý	k2eAgFnPc6d1	bohatá
domácnostech	domácnost	k1gFnPc6	domácnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
kostelní	kostelní	k2eAgFnSc6d1	kostelní
hudbě	hudba	k1gFnSc6	hudba
fungují	fungovat	k5eAaImIp3nP	fungovat
jako	jako	k9	jako
doprovodný	doprovodný	k2eAgInSc4d1	doprovodný
nástroj	nástroj	k1gInSc4	nástroj
chórového	chórový	k2eAgInSc2d1	chórový
zpěvu	zpěv	k1gInSc2	zpěv
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
na	na	k7c4	na
ně	on	k3xPp3gFnPc4	on
hraje	hrát	k5eAaImIp3nS	hrát
stylem	styl	k1gInSc7	styl
basso	bassa	k1gFnSc5	bassa
continuo	continuo	k6eAd1	continuo
(	(	kIx(	(
<g/>
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
u	u	k7c2	u
cembala	cembalo	k1gNnSc2	cembalo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
světské	světský	k2eAgFnSc6d1	světská
hudbě	hudba	k1gFnSc6	hudba
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
buď	buď	k8xC	buď
jako	jako	k9	jako
sólové	sólový	k2eAgInPc4d1	sólový
nástroje	nástroj	k1gInPc4	nástroj
nebo	nebo	k8xC	nebo
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
hře	hra	k1gFnSc6	hra
na	na	k7c4	na
pozitiv	pozitiv	k1gInSc4	pozitiv
býval	bývat	k5eAaImAgInS	bývat
kromě	kromě	k7c2	kromě
varhaníka	varhaník	k1gMnSc4	varhaník
potřebný	potřebný	k2eAgInSc1d1	potřebný
také	také	k9	také
takzvaný	takzvaný	k2eAgInSc1d1	takzvaný
kalkant	kalkant	k?	kalkant
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obsluhoval	obsluhovat	k5eAaImAgInS	obsluhovat
měchy	měch	k1gInPc4	měch
zajišťující	zajišťující	k2eAgInSc4d1	zajišťující
potřebný	potřebný	k2eAgInSc4d1	potřebný
vzduch	vzduch	k1gInSc4	vzduch
do	do	k7c2	do
píšťal	píšťala	k1gFnPc2	píšťala
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
tuto	tento	k3xDgFnSc4	tento
funkci	funkce	k1gFnSc4	funkce
nahradilo	nahradit	k5eAaPmAgNnS	nahradit
tiché	tichý	k2eAgNnSc1d1	tiché
elektrické	elektrický	k2eAgNnSc1d1	elektrické
dmychadlo	dmychadlo	k1gNnSc1	dmychadlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tónový	tónový	k2eAgInSc1d1	tónový
rozsah	rozsah	k1gInSc1	rozsah
pozitivu	pozitiv	k1gInSc2	pozitiv
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
od	od	k7c2	od
velkého	velký	k2eAgNnSc2d1	velké
C	C	kA	C
do	do	k7c2	do
D	D	kA	D
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Pozytyw	Pozytyw	k1gFnSc2	Pozytyw
(	(	kIx(	(
<g/>
instrument	instrument	k1gInSc1	instrument
muzyczny	muzyczna	k1gFnSc2	muzyczna
<g/>
)	)	kIx)	)
na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
pozitiv	pozitivum	k1gNnPc2	pozitivum
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Článek	článek	k1gInSc1	článek
o	o	k7c6	o
opravě	oprava	k1gFnSc6	oprava
varhanního	varhanní	k2eAgInSc2d1	varhanní
pozitivu	pozitiv	k1gInSc2	pozitiv
v	v	k7c6	v
Lounské	lounský	k2eAgFnSc6d1	Lounská
farnosti	farnost	k1gFnSc6	farnost
</s>
</p>
