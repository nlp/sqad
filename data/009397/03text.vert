<p>
<s>
Na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
On	on	k3xPp3gInSc1	on
the	the	k?	the
Road	Road	k1gInSc1	Road
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
román	román	k1gInSc1	román
amerického	americký	k2eAgMnSc2d1	americký
spisovatele	spisovatel	k1gMnSc2	spisovatel
Jacka	Jacek	k1gMnSc2	Jacek
Kerouaca	Kerouacus	k1gMnSc2	Kerouacus
<g/>
.	.	kIx.	.
</s>
<s>
Kerouac	Kerouac	k6eAd1	Kerouac
jej	on	k3xPp3gMnSc4	on
napsal	napsat	k5eAaPmAgMnS	napsat
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kniha	kniha	k1gFnSc1	kniha
vyšla	vyjít	k5eAaPmAgFnS	vyjít
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
v	v	k7c6	v
nakladatelství	nakladatelství	k1gNnSc6	nakladatelství
Viking	Viking	k1gMnSc1	Viking
Press	Press	k1gInSc1	Press
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
Kerouacových	Kerouacův	k2eAgFnPc6d1	Kerouacova
autobiografických	autobiografický	k2eAgFnPc6d1	autobiografická
vzpomínkách	vzpomínka	k1gFnPc6	vzpomínka
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
psán	psát	k5eAaImNgInS	psát
stylem	styl	k1gInSc7	styl
proudu	proud	k1gInSc2	proud
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
Kerouac	Kerouac	k6eAd1	Kerouac
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
popisuje	popisovat	k5eAaImIp3nS	popisovat
zejména	zejména	k9	zejména
své	svůj	k3xOyFgNnSc4	svůj
cestování	cestování	k1gNnSc4	cestování
napříč	napříč	k7c7	napříč
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
se	se	k3xPyFc4	se
svými	svůj	k3xOyFgMnPc7	svůj
přáteli	přítel	k1gMnPc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnPc1	jejich
jména	jméno	k1gNnPc1	jméno
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
pozměněná	pozměněný	k2eAgFnSc1d1	pozměněná
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
předlohou	předloha	k1gFnSc7	předloha
pro	pro	k7c4	pro
hlavní	hlavní	k2eAgFnPc4d1	hlavní
postavy	postava	k1gFnPc4	postava
románu	román	k1gInSc2	román
byli	být	k5eAaImAgMnP	být
skuteční	skutečný	k2eAgMnPc1d1	skutečný
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jacka	Jacka	k1gMnSc1	Jacka
Kerouaca	Kerouaca	k1gMnSc1	Kerouaca
při	při	k7c6	při
tvorbě	tvorba	k1gFnSc6	tvorba
tohoto	tento	k3xDgNnSc2	tento
díla	dílo	k1gNnSc2	dílo
zásadně	zásadně	k6eAd1	zásadně
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
Neal	Neal	k1gInSc1	Neal
Cassady	Cassada	k1gFnSc2	Cassada
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
rozsáhlým	rozsáhlý	k2eAgInSc7d1	rozsáhlý
čtyřicetistránkovým	čtyřicetistránkový	k2eAgInSc7d1	čtyřicetistránkový
dopisem	dopis	k1gInSc7	dopis
(	(	kIx(	(
<g/>
známým	známý	k1gMnSc7	známý
jako	jako	k8xC	jako
dopis	dopis	k1gInSc4	dopis
o	o	k7c4	o
Joan	Joan	k1gNnSc4	Joan
Andersonové	Andersonová	k1gFnSc2	Andersonová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
de	de	k?	de
facto	facto	k1gNnSc4	facto
jedinou	jediný	k2eAgFnSc7d1	jediná
nepřerušovanou	přerušovaný	k2eNgFnSc7d1	nepřerušovaná
větou	věta	k1gFnSc7	věta
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
Kerouacovi	Kerouaec	k1gMnSc3	Kerouaec
inspirací	inspirace	k1gFnSc7	inspirace
k	k	k7c3	k
formě	forma	k1gFnSc3	forma
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
pro	pro	k7c4	pro
román	román	k1gInSc4	román
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
byl	být	k5eAaImAgMnS	být
propuštěn	propustit	k5eAaPmNgMnS	propustit
z	z	k7c2	z
nemocnice	nemocnice	k1gFnSc2	nemocnice
(	(	kIx(	(
<g/>
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
hospitalizován	hospitalizován	k2eAgInSc1d1	hospitalizován
s	s	k7c7	s
flebitidou	flebitida	k1gFnSc7	flebitida
–	–	k?	–
zánětem	zánět	k1gInSc7	zánět
žil	žíla	k1gFnPc2	žíla
<g/>
)	)	kIx)	)
koncem	koncem	k7c2	koncem
března	březen	k1gInSc2	březen
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
pustil	pustit	k5eAaPmAgMnS	pustit
se	se	k3xPyFc4	se
do	do	k7c2	do
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
psát	psát	k5eAaImF	psát
na	na	k7c4	na
pět	pět	k4xCc4	pět
metrů	metr	k1gInPc2	metr
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
tenké	tenký	k2eAgFnSc2d1	tenká
role	role	k1gFnSc2	role
japonského	japonský	k2eAgInSc2d1	japonský
kreslicího	kreslicí	k2eAgInSc2d1	kreslicí
papíru	papír	k1gInSc2	papír
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc4	jenž
našel	najít	k5eAaPmAgMnS	najít
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
a	a	k8xC	a
lepil	lepit	k5eAaImAgMnS	lepit
je	být	k5eAaImIp3nS	být
dohromady	dohromady	k6eAd1	dohromady
do	do	k7c2	do
velkých	velký	k2eAgInPc2d1	velký
pásů	pás	k1gInPc2	pás
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgMnS	pracovat
neúnavně	únavně	k6eNd1	únavně
po	po	k7c4	po
celé	celý	k2eAgInPc4d1	celý
tři	tři	k4xCgInPc4	tři
týdny	týden	k1gInPc4	týden
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
drog	droga	k1gFnPc2	droga
a	a	k8xC	a
v	v	k7c6	v
podnapilém	podnapilý	k2eAgInSc6d1	podnapilý
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
svůj	svůj	k3xOyFgInSc4	svůj
jediný	jediný	k2eAgInSc4d1	jediný
30	[number]	k4	30
m	m	kA	m
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
odstavec	odstavec	k1gInSc4	odstavec
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
vynořoval	vynořovat	k5eAaImAgInS	vynořovat
v	v	k7c6	v
paměti	paměť	k1gFnSc6	paměť
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
jeho	jeho	k3xOp3gInSc1	jeho
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
vymanit	vymanit	k5eAaPmF	vymanit
z	z	k7c2	z
dřívějších	dřívější	k2eAgNnPc2d1	dřívější
literárních	literární	k2eAgNnPc2d1	literární
omezení	omezení	k1gNnPc2	omezení
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc4	který
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
druh	druh	k1gInSc4	druh
lži	lež	k1gFnSc2	lež
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
dílo	dílo	k1gNnSc4	dílo
koncem	koncem	k7c2	koncem
dubna	duben	k1gInSc2	duben
dokončil	dokončit	k5eAaPmAgMnS	dokončit
<g/>
,	,	kIx,	,
opustil	opustit	k5eAaPmAgMnS	opustit
svou	svůj	k3xOyFgFnSc4	svůj
druhou	druhý	k4xOgFnSc4	druhý
manželku	manželka	k1gFnSc4	manželka
Joan	Joana	k1gFnPc2	Joana
Havertyovou	Havertyový	k2eAgFnSc4d1	Havertyový
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vyčerpaný	vyčerpaný	k2eAgInSc1d1	vyčerpaný
a	a	k8xC	a
přecitlivělý	přecitlivělý	k2eAgInSc1d1	přecitlivělý
a	a	k8xC	a
přesunul	přesunout	k5eAaPmAgInS	přesunout
se	se	k3xPyFc4	se
do	do	k7c2	do
domu	dům	k1gInSc2	dům
své	svůj	k3xOyFgFnSc2	svůj
sestry	sestra	k1gFnSc2	sestra
v	v	k7c4	v
Rocky	rock	k1gInPc4	rock
Mount	Mounta	k1gFnPc2	Mounta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
–	–	k?	–
pro	pro	k7c4	pro
uklidnění	uklidnění	k1gNnSc4	uklidnění
–	–	k?	–
věnoval	věnovat	k5eAaImAgMnS	věnovat
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
dny	den	k1gInPc4	den
četbě	četba	k1gFnSc3	četba
Dostojevského	Dostojevský	k2eAgMnSc2d1	Dostojevský
<g/>
,	,	kIx,	,
Prousta	Proust	k1gMnSc4	Proust
<g/>
,	,	kIx,	,
Lawrence	Lawrenec	k1gMnSc4	Lawrenec
<g/>
,	,	kIx,	,
Faulknera	Faulkner	k1gMnSc4	Faulkner
<g/>
,	,	kIx,	,
Flauberta	Flaubert	k1gMnSc4	Flaubert
<g/>
,	,	kIx,	,
Gorkého	Gorkij	k1gMnSc4	Gorkij
<g/>
,	,	kIx,	,
Whitmana	Whitman	k1gMnSc4	Whitman
<g/>
,	,	kIx,	,
Dickinsonové	Dickinsonové	k2eAgMnSc1d1	Dickinsonové
<g/>
,	,	kIx,	,
Yeatse	Yeatse	k1gFnSc1	Yeatse
<g/>
,	,	kIx,	,
Hawthorna	Hawthorna	k1gFnSc1	Hawthorna
<g/>
,	,	kIx,	,
Sandburga	Sandburga	k1gFnSc1	Sandburga
a	a	k8xC	a
Crana	Crana	k1gFnSc1	Crana
<g/>
.	.	kIx.	.
<g/>
Román	román	k1gInSc1	román
byl	být	k5eAaImAgInS	být
americkým	americký	k2eAgInSc7d1	americký
časopisem	časopis	k1gInSc7	časopis
Time	Tim	k1gInSc2	Tim
vybrán	vybrat	k5eAaPmNgInS	vybrat
mezi	mezi	k7c4	mezi
stovku	stovka	k1gFnSc4	stovka
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
anglicky	anglicky	k6eAd1	anglicky
psaných	psaný	k2eAgInPc2d1	psaný
románů	román	k1gInPc2	román
mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1923	[number]	k4	1923
až	až	k9	až
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Postavy	postava	k1gFnPc1	postava
==	==	k?	==
</s>
</p>
<p>
<s>
Následující	následující	k2eAgFnSc1d1	následující
tabulka	tabulka	k1gFnSc1	tabulka
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
postavy	postava	k1gFnPc4	postava
zachycené	zachycený	k2eAgFnPc4d1	zachycená
v	v	k7c6	v
románu	román	k1gInSc6	román
Na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
skutečné	skutečný	k2eAgFnSc2d1	skutečná
předlohy	předloha	k1gFnSc2	předloha
osobností	osobnost	k1gFnPc2	osobnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
Kerouac	Kerouac	k1gInSc1	Kerouac
nacházel	nacházet	k5eAaImAgInS	nacházet
mezi	mezi	k7c7	mezi
svými	svůj	k3xOyFgMnPc7	svůj
přáteli	přítel	k1gMnPc7	přítel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
On	on	k3xPp3gMnSc1	on
the	the	k?	the
Road	Roada	k1gFnPc2	Roada
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
MACURA	Macura	k1gMnSc1	Macura
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Slovník	slovník	k1gInSc4	slovník
světových	světový	k2eAgNnPc2d1	světové
literárních	literární	k2eAgNnPc2d1	literární
děl	dělo	k1gNnPc2	dělo
1	[number]	k4	1
<g/>
/	/	kIx~	/
A-	A-	k1gMnPc2	A-
<g/>
L.	L.	kA	L.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
207	[number]	k4	207
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
948	[number]	k4	948
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
475	[number]	k4	475
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TYTELL	TYTELL	kA	TYTELL
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
<g/>
.	.	kIx.	.
</s>
<s>
Nazí	nahý	k2eAgMnPc1d1	nahý
andělé	anděl	k1gMnPc1	anděl
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
:	:	kIx,	:
Votobia	Votobia	k1gFnSc1	Votobia
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7198	[number]	k4	7198
<g/>
-	-	kIx~	-
<g/>
104	[number]	k4	104
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
100	[number]	k4	100
nejdůležitějších	důležitý	k2eAgFnPc2d3	nejdůležitější
knih	kniha	k1gFnPc2	kniha
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
podle	podle	k7c2	podle
Le	Le	k1gFnSc2	Le
Monde	Mond	k1gMnSc5	Mond
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Žádné	žádný	k3yNgFnPc4	žádný
drogy	droga	k1gFnPc4	droga
<g/>
,	,	kIx,	,
jen	jen	k9	jen
spousta	spousta	k1gFnSc1	spousta
kávy	káva	k1gFnSc2	káva
<g/>
.	.	kIx.	.
</s>
<s>
Mýty	mýtus	k1gInPc1	mýtus
kolem	kolem	k6eAd1	kolem
Kerouakova	Kerouakův	k2eAgInSc2d1	Kerouakův
Na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
padly	padnout	k5eAaPmAgFnP	padnout
–	–	k?	–
kultura	kultura	k1gFnSc1	kultura
<g/>
.	.	kIx.	.
<g/>
iDNES	iDNES	k?	iDNES
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
11	[number]	k4	11
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
</s>
</p>
