<p>
<s>
Pontiac	Pontiac	k6eAd1	Pontiac
Phoenix	Phoenix	k1gInSc1	Phoenix
je	být	k5eAaImIp3nS	být
vůz	vůz	k1gInSc4	vůz
nižší	nízký	k2eAgFnSc2d2	nižší
střední	střední	k2eAgFnSc2d1	střední
třídy	třída	k1gFnSc2	třída
prodávaný	prodávaný	k2eAgInSc1d1	prodávaný
pod	pod	k7c7	pod
značkou	značka	k1gFnSc7	značka
Pontiac	Pontiac	k1gFnSc4	Pontiac
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Vozidlo	vozidlo	k1gNnSc1	vozidlo
se	se	k3xPyFc4	se
dočkalo	dočkat	k5eAaPmAgNnS	dočkat
dvou	dva	k4xCgFnPc2	dva
generací	generace	k1gFnPc2	generace
<g/>
,	,	kIx,	,
obě	dva	k4xCgFnPc1	dva
byly	být	k5eAaImAgFnP	být
založené	založený	k2eAgFnPc1d1	založená
na	na	k7c6	na
vozidlech	vozidlo	k1gNnPc6	vozidlo
Chevrolet	chevrolet	k1gInSc1	chevrolet
a	a	k8xC	a
platformě	platforma	k1gFnSc6	platforma
GM	GM	kA	GM
X.	X.	kA	X.
Automobil	automobil	k1gInSc1	automobil
je	být	k5eAaImIp3nS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
podle	podle	k7c2	podle
bájného	bájný	k2eAgMnSc2d1	bájný
ptáka	pták	k1gMnSc2	pták
fénixe	fénix	k1gMnSc2	fénix
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
První	první	k4xOgFnPc1	první
generace	generace	k1gFnPc1	generace
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
–	–	k?	–
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Vůz	vůz	k1gInSc1	vůz
Phoenix	Phoenix	k1gInSc1	Phoenix
s	s	k7c7	s
pohonem	pohon	k1gInSc7	pohon
zadních	zadní	k2eAgNnPc2d1	zadní
kol	kolo	k1gNnPc2	kolo
byl	být	k5eAaImAgInS	být
představen	představit	k5eAaPmNgInS	představit
pro	pro	k7c4	pro
modelový	modelový	k2eAgInSc4d1	modelový
rok	rok	k1gInSc4	rok
1977	[number]	k4	1977
pouze	pouze	k6eAd1	pouze
jako	jako	k8xS	jako
luxusní	luxusní	k2eAgFnSc1d1	luxusní
verze	verze	k1gFnSc1	verze
vozu	vůz	k1gInSc2	vůz
Pontiac	Pontiac	k1gFnSc1	Pontiac
Ventura	Ventura	kA	Ventura
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
Venturu	Ventura	k1gFnSc4	Ventura
zcela	zcela	k6eAd1	zcela
nahradil	nahradit	k5eAaPmAgMnS	nahradit
<g/>
.	.	kIx.	.
</s>
<s>
Phoenix	Phoenix	k1gInSc1	Phoenix
lišily	lišit	k5eAaImAgFnP	lišit
od	od	k7c2	od
Ventury	Ventura	k1gFnSc2	Ventura
jen	jen	k9	jen
drobné	drobný	k2eAgInPc1d1	drobný
detaily	detail	k1gInPc1	detail
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
maska	maska	k1gFnSc1	maska
chladiče	chladič	k1gInSc2	chladič
<g/>
,	,	kIx,	,
hranatá	hranatý	k2eAgNnPc4d1	hranaté
přední	přední	k2eAgNnPc4d1	přední
světla	světlo	k1gNnPc4	světlo
a	a	k8xC	a
žluté	žlutý	k2eAgInPc4d1	žlutý
zadní	zadní	k2eAgInPc4d1	zadní
blinkry	blinkr	k1gInPc4	blinkr
<g/>
.	.	kIx.	.
</s>
<s>
Phoenix	Phoenix	k1gInSc1	Phoenix
byl	být	k5eAaImAgInS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
jako	jako	k8xS	jako
dvoudveřové	dvoudveřový	k2eAgNnSc4d1	dvoudveřové
kupé	kupé	k1gNnSc4	kupé
<g/>
,	,	kIx,	,
čtyřdveřový	čtyřdveřový	k2eAgInSc4d1	čtyřdveřový
sedan	sedan	k1gInSc4	sedan
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
jako	jako	k8xS	jako
třídveřový	třídveřový	k2eAgInSc1d1	třídveřový
hatchback	hatchback	k1gInSc1	hatchback
<g/>
.	.	kIx.	.
</s>
<s>
Stupně	stupeň	k1gInPc1	stupeň
výbavy	výbava	k1gFnSc2	výbava
byly	být	k5eAaImAgInP	být
dva	dva	k4xCgInPc1	dva
<g/>
,	,	kIx,	,
základní	základní	k2eAgInPc1d1	základní
a	a	k8xC	a
luxusnější	luxusní	k2eAgInPc1d2	luxusnější
LJ	LJ	kA	LJ
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yIgInSc3	který
se	se	k3xPyFc4	se
dal	dát	k5eAaPmAgInS	dát
přikoupit	přikoupit	k5eAaPmF	přikoupit
sportovní	sportovní	k2eAgInSc1d1	sportovní
paket	paket	k1gInSc1	paket
SJ	SJ	kA	SJ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Motory	motor	k1gInPc1	motor
na	na	k7c4	na
výběr	výběr	k1gInSc4	výběr
zahrnovaly	zahrnovat	k5eAaImAgInP	zahrnovat
tehdy	tehdy	k6eAd1	tehdy
zbrusu	zbrusu	k6eAd1	zbrusu
nový	nový	k2eAgInSc4d1	nový
čtyřválec	čtyřválec	k1gInSc4	čtyřválec
Iron	iron	k1gInSc1	iron
Duke	Duke	k1gInSc1	Duke
s	s	k7c7	s
110	[number]	k4	110
koňskými	koňský	k2eAgFnPc7d1	koňská
silami	síla	k1gFnPc7	síla
(	(	kIx(	(
<g/>
82	[number]	k4	82
kW	kW	kA	kW
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
3,8	[number]	k4	3,8
<g/>
litrový	litrový	k2eAgInSc1d1	litrový
Buick	Buick	k1gInSc1	Buick
V6	V6	k1gFnSc2	V6
(	(	kIx(	(
<g/>
140	[number]	k4	140
koní	kůň	k1gMnPc2	kůň
<g/>
,	,	kIx,	,
104	[number]	k4	104
kW	kW	kA	kW
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pětilitrový	pětilitrový	k2eAgInSc1d1	pětilitrový
Chevrolet	chevrolet	k1gInSc1	chevrolet
LG3	LG3	k1gFnSc2	LG3
V8	V8	k1gFnPc2	V8
a	a	k8xC	a
5,7	[number]	k4	5,7
<g/>
litrový	litrový	k2eAgInSc4d1	litrový
Chevrolet	chevrolet	k1gInSc4	chevrolet
V	v	k7c4	v
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
Převodovky	převodovka	k1gFnPc4	převodovka
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
byly	být	k5eAaImAgInP	být
<g/>
:	:	kIx,	:
třístupňová	třístupňový	k2eAgFnSc1d1	třístupňová
manuální	manuální	k2eAgFnSc1d1	manuální
<g/>
,	,	kIx,	,
čtyřstupňová	čtyřstupňový	k2eAgFnSc1d1	čtyřstupňová
manuální	manuální	k2eAgFnSc1d1	manuální
a	a	k8xC	a
třístupňová	třístupňový	k2eAgFnSc1d1	třístupňová
automatická	automatický	k2eAgFnSc1d1	automatická
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhá	druhý	k4xOgFnSc1	druhý
generace	generace	k1gFnSc1	generace
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
–	–	k?	–
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
modelový	modelový	k2eAgInSc4d1	modelový
rok	rok	k1gInSc4	rok
1980	[number]	k4	1980
byl	být	k5eAaImAgInS	být
Phoenix	Phoenix	k1gInSc1	Phoenix
představen	představit	k5eAaPmNgInS	představit
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
generaci	generace	k1gFnSc6	generace
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
zmenšená	zmenšený	k2eAgFnSc1d1	zmenšená
a	a	k8xC	a
využívala	využívat	k5eAaImAgFnS	využívat
pohon	pohon	k1gInSc4	pohon
přední	přední	k2eAgFnSc2d1	přední
nápravy	náprava	k1gFnSc2	náprava
<g/>
.	.	kIx.	.
</s>
<s>
Vůz	vůz	k1gInSc1	vůz
byl	být	k5eAaImAgInS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
jako	jako	k8xC	jako
dvoudveřové	dvoudveřový	k2eAgNnSc4d1	dvoudveřové
kupé	kupé	k1gNnSc4	kupé
či	či	k8xC	či
pětidveřový	pětidveřový	k2eAgInSc4d1	pětidveřový
hatchback	hatchback	k1gInSc4	hatchback
<g/>
.	.	kIx.	.
</s>
<s>
Výbavy	výbava	k1gFnPc1	výbava
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
stejné	stejný	k2eAgFnPc1d1	stejná
jako	jako	k8xC	jako
u	u	k7c2	u
první	první	k4xOgFnSc2	první
generace	generace	k1gFnSc2	generace
<g/>
,	,	kIx,	,
základní	základní	k2eAgFnSc2d1	základní
a	a	k8xC	a
LJ	LJ	kA	LJ
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
doteď	doteď	k?	doteď
pouze	pouze	k6eAd1	pouze
sportovní	sportovní	k2eAgInSc4d1	sportovní
paket	paket	k1gInSc4	paket
SJ	SJ	kA	SJ
povýšil	povýšit	k5eAaPmAgMnS	povýšit
na	na	k7c4	na
úroveň	úroveň	k1gFnSc4	úroveň
plnohodnotné	plnohodnotný	k2eAgFnSc2d1	plnohodnotná
výbavy	výbava	k1gFnSc2	výbava
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
přibyla	přibýt	k5eAaPmAgFnS	přibýt
výbava	výbava	k1gFnSc1	výbava
PJ	PJ	kA	PJ
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
byly	být	k5eAaImAgFnP	být
výbavy	výbava	k1gFnPc1	výbava
LJ	LJ	kA	LJ
a	a	k8xC	a
SJ	SJ	kA	SJ
přejmenovány	přejmenovat	k5eAaPmNgInP	přejmenovat
na	na	k7c4	na
LE	LE	kA	LE
a	a	k8xC	a
SE	s	k7c7	s
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
byly	být	k5eAaImAgInP	být
dva	dva	k4xCgInPc1	dva
motory	motor	k1gInPc1	motor
<g/>
:	:	kIx,	:
2,5	[number]	k4	2,5
<g/>
litrový	litrový	k2eAgInSc4d1	litrový
čtyřválec	čtyřválec	k1gInSc4	čtyřválec
Iron	iron	k1gInSc1	iron
Duke	Duke	k1gInSc1	Duke
převzatý	převzatý	k2eAgInSc1d1	převzatý
z	z	k7c2	z
předchozí	předchozí	k2eAgFnSc2d1	předchozí
generace	generace	k1gFnSc2	generace
Phoenixe	Phoenixe	k1gFnSc2	Phoenixe
a	a	k8xC	a
nový	nový	k2eAgInSc4d1	nový
2,8	[number]	k4	2,8
<g/>
litrový	litrový	k2eAgInSc4d1	litrový
šestiválec	šestiválec	k1gInSc4	šestiválec
LE	LE	kA	LE
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
dodávaný	dodávaný	k2eAgInSc1d1	dodávaný
do	do	k7c2	do
výbavy	výbava	k1gFnSc2	výbava
SJ	SJ	kA	SJ
<g/>
/	/	kIx~	/
<g/>
SE	se	k3xPyFc4	se
standardně	standardně	k6eAd1	standardně
a	a	k8xC	a
do	do	k7c2	do
ostatních	ostatní	k2eAgFnPc2d1	ostatní
výbav	výbava	k1gFnPc2	výbava
za	za	k7c4	za
příplatek	příplatek	k1gInSc4	příplatek
<g/>
.	.	kIx.	.
</s>
<s>
Převodovky	převodovka	k1gFnPc1	převodovka
byly	být	k5eAaImAgFnP	být
na	na	k7c4	na
výběr	výběr	k1gInSc4	výběr
taktéž	taktéž	k?	taktéž
dvě	dva	k4xCgFnPc4	dva
<g/>
,	,	kIx,	,
čtyřstupňová	čtyřstupňový	k2eAgFnSc1d1	čtyřstupňová
manuální	manuální	k2eAgFnSc1d1	manuální
či	či	k8xC	či
trojstupňová	trojstupňový	k2eAgFnSc1d1	trojstupňová
automatická	automatický	k2eAgFnSc1d1	automatická
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vůz	vůz	k1gInSc1	vůz
společně	společně	k6eAd1	společně
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
příbuznými	příbuzný	k2eAgInPc7d1	příbuzný
vozy	vůz	k1gInPc7	vůz
koncernu	koncern	k1gInSc2	koncern
General	General	k1gFnPc4	General
Motors	Motorsa	k1gFnPc2	Motorsa
(	(	kIx(	(
<g/>
Chevrolet	chevrolet	k1gInSc1	chevrolet
Citation	Citation	k1gInSc1	Citation
<g/>
,	,	kIx,	,
Buick	Buick	k1gInSc1	Buick
Skylark	Skylark	k1gInSc1	Skylark
a	a	k8xC	a
Oldsmobile	Oldsmobila	k1gFnSc3	Oldsmobila
Omega	omega	k1gNnSc2	omega
<g/>
)	)	kIx)	)
propadl	propadnout	k5eAaPmAgMnS	propadnout
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
image	image	k1gFnSc1	image
vozu	vůz	k1gInSc2	vůz
Phoenix	Phoenix	k1gInSc4	Phoenix
silně	silně	k6eAd1	silně
utrpěla	utrpět	k5eAaPmAgFnS	utrpět
kvůli	kvůli	k7c3	kvůli
špatnému	špatný	k2eAgNnSc3d1	špatné
zpracování	zpracování	k1gNnSc3	zpracování
vozu	vůz	k1gInSc2	vůz
<g/>
,	,	kIx,	,
dvojím	dvojit	k5eAaImIp1nS	dvojit
hromadným	hromadný	k2eAgNnSc7d1	hromadné
svoláním	svolání	k1gNnSc7	svolání
k	k	k7c3	k
opravám	oprava	k1gFnPc3	oprava
a	a	k8xC	a
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
tendenci	tendence	k1gFnSc4	tendence
vozidla	vozidlo	k1gNnSc2	vozidlo
zablokovat	zablokovat	k5eAaPmF	zablokovat
zadní	zadní	k2eAgNnPc4d1	zadní
kola	kolo	k1gNnPc4	kolo
při	při	k7c6	při
brzdění	brzdění	k1gNnSc6	brzdění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K	k	k7c3	k
ukončení	ukončení	k1gNnSc3	ukončení
výroby	výroba	k1gFnSc2	výroba
modelu	model	k1gInSc2	model
Phoenix	Phoenix	k1gInSc4	Phoenix
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
a	a	k8xC	a
vůz	vůz	k1gInSc1	vůz
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
modelový	modelový	k2eAgInSc4d1	modelový
rok	rok	k1gInSc4	rok
1985	[number]	k4	1985
nahrazen	nahrazen	k2eAgInSc1d1	nahrazen
vozem	vůz	k1gInSc7	vůz
Pontiac	Pontiac	k1gFnSc1	Pontiac
Grand	grand	k1gMnSc1	grand
Am	Am	k1gMnSc1	Am
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Pontiac	Pontiac	k1gInSc1	Pontiac
Phoenix	Phoenix	k1gInSc1	Phoenix
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
