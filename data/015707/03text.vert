<s>
Palembang	Palembang	k1gMnSc1
P2	P2	k1gMnSc1
</s>
<s>
Palembang	Palembang	k1gInSc1
P2	P2	k1gFnSc2
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
3	#num#	k4
<g/>
°	°	k?
<g/>
17	#num#	k4
<g/>
′	′	k?
<g/>
50	#num#	k4
<g/>
″	″	k?
j.	j.	k?
š.	š.	k?
<g/>
,	,	kIx,
104	#num#	k4
<g/>
°	°	k?
<g/>
24	#num#	k4
<g/>
′	′	k?
<g/>
30	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Palembang	Palembang	k1gMnSc1
P2	P2	k1gMnSc1
či	či	k8xC
Palembang	Palembang	k1gMnSc1
II	II	kA
(	(	kIx(
<g/>
též	též	k9
jenom	jenom	k9
P	P	kA
<g/>
2	#num#	k4
<g/>
,	,	kIx,
P.	P.	kA
<g/>
2	#num#	k4
<g/>
,	,	kIx,
P	P	kA
II	II	kA
nebo	nebo	k8xC
Gloembang	Gloembang	k1gMnSc1
a	a	k8xC
Karangendah	Karangendah	k1gMnSc1
<g/>
)	)	kIx)
bylo	být	k5eAaImAgNnS
utajované	utajovaný	k2eAgNnSc1d1
vojenské	vojenský	k2eAgNnSc1d1
letiště	letiště	k1gNnSc1
vybudované	vybudovaný	k2eAgNnSc1d1
Nizozemci	Nizozemec	k1gMnPc7
asi	asi	k9
32	#num#	k4
mil	míle	k1gFnPc2
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
(	(	kIx(
<g/>
asi	asi	k9
51	#num#	k4
km	km	kA
<g/>
)	)	kIx)
jihozápadně	jihozápadně	k6eAd1
od	od	k7c2
dnešního	dnešní	k2eAgInSc2d1
Palembangu	Palembang	k1gInSc2
na	na	k7c6
Sumatře	Sumatra	k1gFnSc6
a	a	k8xC
používané	používaný	k2eAgInPc1d1
za	za	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gInSc7
poloha	poloha	k1gFnSc1
je	být	k5eAaImIp3nS
uváděna	uváděn	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
3	#num#	k4
<g/>
°	°	k?
<g/>
20	#num#	k4
<g/>
′	′	k?
j.	j.	k?
š.	š.	k?
<g/>
,	,	kIx,
104	#num#	k4
<g/>
°	°	k?
<g/>
24	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
3	#num#	k4
<g/>
°	°	k?
<g/>
17	#num#	k4
<g/>
′	′	k?
<g/>
50	#num#	k4
<g/>
″	″	k?
j.	j.	k?
š.	š.	k?
<g/>
,	,	kIx,
104	#num#	k4
<g/>
°	°	k?
<g/>
24	#num#	k4
<g/>
′	′	k?
<g/>
30	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
je	být	k5eAaImIp3nS
letiště	letiště	k1gNnSc4
mimo	mimo	k7c4
provoz	provoz	k1gInSc4
a	a	k8xC
areál	areál	k1gInSc4
bývalého	bývalý	k2eAgNnSc2d1
letiště	letiště	k1gNnSc2
využívá	využívat	k5eAaImIp3nS,k5eAaPmIp3nS
indonéská	indonéský	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výstavba	výstavba	k1gFnSc1
a	a	k8xC
využití	využití	k1gNnSc1
Spojenci	spojenec	k1gMnPc1
</s>
<s>
S	s	k7c7
narůstajícím	narůstající	k2eAgNnSc7d1
napětím	napětí	k1gNnSc7
na	na	k7c6
dálném	dálný	k2eAgInSc6d1
východě	východ	k1gInSc6
přistoupilo	přistoupit	k5eAaPmAgNnS
ML-KNIL	ML-KNIL	k1gFnSc1
(	(	kIx(
<g/>
Vojenské	vojenský	k2eAgNnSc1d1
letectvo	letectvo	k1gNnSc1
královské	královský	k2eAgNnSc1d1
nizozemsko	nizozemsko	k1gNnSc1
<g/>
–	–	k?
<g/>
indické	indický	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
<g/>
)	)	kIx)
v	v	k7c6
roce	rok	k1gInSc6
1941	#num#	k4
k	k	k7c3
budování	budování	k1gNnSc2
druhého	druhý	k4xOgNnSc2
letiště	letiště	k1gNnSc2
u	u	k7c2
Palembangu	Palembang	k1gInSc2
(	(	kIx(
<g/>
oním	onen	k3xDgNnSc7
prvním	první	k4xOgMnSc6
bylo	být	k5eAaImAgNnS
civilní	civilní	k2eAgInSc1d1
P	P	kA
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Nové	Nové	k2eAgNnSc1d1
letiště	letiště	k1gNnSc1
bylo	být	k5eAaImAgNnS
součástí	součást	k1gFnSc7
sítě	síť	k1gFnSc2
pěti	pět	k4xCc2
nových	nový	k2eAgFnPc2d1
skrytých	skrytý	k2eAgFnPc2d1
letišť	letiště	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
tvořila	tvořit	k5eAaImAgFnS
vnější	vnější	k2eAgFnSc4d1
obranu	obrana	k1gFnSc4
Jávy	Jáva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
čtyři	čtyři	k4xCgNnPc1
letiště	letiště	k1gNnPc1
byla	být	k5eAaImAgNnP
Singkawang	Singkawang	k1gInSc4
II	II	kA
a	a	k8xC
Samarinda	Samarinda	k1gFnSc1
II	II	kA
na	na	k7c6
Borneu	Borneo	k1gNnSc6
<g/>
,	,	kIx,
Kendari	Kendari	k1gNnSc6
II	II	kA
na	na	k7c6
Celebesu	Celebes	k1gInSc6
a	a	k8xC
Ambon	ambon	k1gInSc1
II	II	kA
na	na	k7c4
Ambonu	ambona	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
P2	P2	k4
bylo	být	k5eAaImAgNnS
budováno	budovat	k5eAaImNgNnS
asi	asi	k9
42	#num#	k4
mil	míle	k1gFnPc2
(	(	kIx(
<g/>
67,6	67,6	k4
km	km	kA
<g/>
)	)	kIx)
od	od	k7c2
tehdejšího	tehdejší	k2eAgInSc2d1
Palembangu	Palembang	k1gInSc2
na	na	k7c6
cestě	cesta	k1gFnSc6
do	do	k7c2
Prabumulih	Prabumuliha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cestou	cestou	k7c2
z	z	k7c2
Palembangu	Palembang	k1gInSc2
bylo	být	k5eAaImAgNnS
třeba	třeba	k6eAd1
přívozem	přívoz	k1gInSc7
překonat	překonat	k5eAaPmF
řeku	řeka	k1gFnSc4
Musi	Mus	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Letiště	letiště	k1gNnSc1
maximálně	maximálně	k6eAd1
využívalo	využívat	k5eAaImAgNnS,k5eAaPmAgNnS
terénu	terén	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
pouze	pouze	k6eAd1
vysušen	vysušen	k2eAgInSc1d1
a	a	k8xC
vyčištěn	vyčištěn	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
bylo	být	k5eAaImAgNnS
ukryto	ukrýt	k5eAaPmNgNnS
v	v	k7c6
džungli	džungle	k1gFnSc6
<g/>
,	,	kIx,
zůstalo	zůstat	k5eAaPmAgNnS
skryto	skrýt	k5eAaPmNgNnS
před	před	k7c7
pozorností	pozornost	k1gFnSc7
Japonců	Japonec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Již	již	k6eAd1
v	v	k7c6
prosinci	prosinec	k1gInSc6
1941	#num#	k4
začaly	začít	k5eAaPmAgFnP
ještě	ještě	k9
nedokončené	dokončený	k2eNgNnSc4d1
letiště	letiště	k1gNnSc4
používat	používat	k5eAaImF
Glenn	Glenn	k1gNnSc4
Martiny	Martina	k1gFnSc2
od	od	k7c2
2	#num#	k4
<g/>
-Vl	-Vl	k?
<g/>
.	.	kIx.
<g/>
G.I.	G.I.	k1gMnSc2
Ty	ty	k3xPp2nSc1
z	z	k7c2
letiště	letiště	k1gNnSc2
operovaly	operovat	k5eAaImAgFnP
až	až	k9
do	do	k7c2
26	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1942	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
2	#num#	k4
<g/>
-Vl	-Vl	k?
<g/>
.	.	kIx.
<g/>
G.I	G.I	k1gFnSc1
ustoupila	ustoupit	k5eAaPmAgFnS
na	na	k7c4
Jávu	Jáva	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rámci	rámec	k1gInSc6
společné	společný	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
před	před	k7c7
japonskou	japonský	k2eAgFnSc7d1
invazí	invaze	k1gFnSc7
operovaly	operovat	k5eAaImAgFnP
z	z	k7c2
letiště	letiště	k1gNnSc2
i	i	k8xC
stroje	stroj	k1gInSc2
RAF	raf	k0
a	a	k8xC
RAAF	RAAF	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
31	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1942	#num#	k4
se	se	k3xPyFc4
velení	velení	k1gNnSc4
na	na	k7c6
P2	P2	k1gFnSc6
ujal	ujmout	k5eAaPmAgMnS
Grp	Grp	k1gMnSc1
Cpt	Cpt	k1gMnSc1
(	(	kIx(
<g/>
kapitán	kapitán	k1gMnSc1
<g/>
)	)	kIx)
McCauley	McCauley	k1gInPc1
(	(	kIx(
<g/>
RAF	raf	k0
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
P2	P2	k1gFnSc2
operovaly	operovat	k5eAaImAgFnP
1	#num#	k4
<g/>
.	.	kIx.
squadrona	squadrona	k1gFnSc1
RAAF	RAAF	kA
a	a	k8xC
62	#num#	k4
<g/>
.	.	kIx.
squadrona	squadrona	k1gFnSc1
RAF	raf	k0
vybavené	vybavený	k2eAgInPc4d1
Lockheed	Lockheed	k1gInSc4
Hudsony	Hudsona	k1gFnSc2
a	a	k8xC
27	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
84	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
211	#num#	k4
<g/>
.	.	kIx.
squadrona	squadrona	k1gFnSc1
RAF	raf	k0
létající	létající	k2eAgFnPc4d1
s	s	k7c7
Bristol	Bristol	k1gInSc4
Blenheimy	Blenheimo	k1gNnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Obrana	obrana	k1gFnSc1
letiště	letiště	k1gNnSc2
byla	být	k5eAaImAgFnS
od	od	k7c2
konce	konec	k1gInSc2
ledna	leden	k1gInSc2
1942	#num#	k4
zajištěna	zajistit	k5eAaPmNgFnS
„	„	k?
<g/>
šesti	šest	k4xCc7
těžkými	těžký	k2eAgNnPc7d1
děly	dělo	k1gNnPc7
a	a	k8xC
šesti	šest	k4xCc7
Boforsy	Bofors	k1gInPc7
<g/>
“	“	k?
<g/>
,	,	kIx,
pro	pro	k7c4
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
byl	být	k5eAaImAgInS
ale	ale	k9
nedostatek	nedostatek	k1gInSc1
munice	munice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozemní	pozemní	k2eAgFnSc4d1
obranu	obrana	k1gFnSc4
zajišťovaly	zajišťovat	k5eAaImAgInP
dva	dva	k4xCgInPc1
nizozemské	nizozemský	k2eAgInPc1d1
obrněné	obrněný	k2eAgInPc1d1
automobily	automobil	k1gInPc1
a	a	k8xC
150	#num#	k4
domorodých	domorodý	k2eAgMnPc2d1
vojáků	voják	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
únoru	únor	k1gInSc6
1942	#num#	k4
bylo	být	k5eAaImAgNnS
P2	P2	k1gFnSc7
podřízeno	podřízen	k2eAgNnSc4d1
teritoriálnímu	teritoriální	k2eAgNnSc3d1
velitelství	velitelství	k1gNnSc3
KNIL	KNIL	kA
Jižní	jižní	k2eAgFnSc1d1
Sumatra	Sumatra	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Japonská	japonský	k2eAgFnSc1d1
okupace	okupace	k1gFnSc1
</s>
<s>
Když	když	k8xS
se	s	k7c7
14	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1942	#num#	k4
japonští	japonský	k2eAgMnPc1d1
výsadkáři	výsadkář	k1gMnPc1
zmocnili	zmocnit	k5eAaPmAgMnP
P1	P1	k1gFnSc4
a	a	k8xC
začali	začít	k5eAaPmAgMnP
obsazovat	obsazovat	k5eAaImF
Palembang	Palembang	k1gInSc4
<g/>
,	,	kIx,
zůstalo	zůstat	k5eAaPmAgNnS
P2	P2	k1gMnSc3
nepovšimnuto	povšimnut	k2eNgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
P2	P2	k1gFnPc6
ale	ale	k8xC
bylo	být	k5eAaImAgNnS
ohroženo	ohrozit	k5eAaPmNgNnS
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
16	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
začala	začít	k5eAaPmAgFnS
evakuace	evakuace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
když	když	k8xS
nad	nad	k7c7
letištěm	letiště	k1gNnSc7
často	často	k6eAd1
přelétávala	přelétávat	k5eAaImAgNnP
japonská	japonský	k2eAgNnPc1d1
letadla	letadlo	k1gNnPc1
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
jeho	jeho	k3xOp3gFnSc1
existence	existence	k1gFnSc1
Japonci	Japonec	k1gMnPc1
objevena	objevit	k5eAaPmNgFnS
teprve	teprve	k6eAd1
21	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Prvních	první	k4xOgInPc2
29	#num#	k4
rikkó	rikkó	k?
typu	typ	k1gInSc2
96	#num#	k4
(	(	kIx(
<g/>
G	G	kA
<g/>
3	#num#	k4
<g/>
M	M	kA
<g/>
)	)	kIx)
a	a	k8xC
transportní	transportní	k2eAgInSc1d1
letoun	letoun	k1gInSc1
od	od	k7c2
Genzan	Genzana	k1gFnPc2
kókútai	kókúta	k1gFnSc2
(	(	kIx(
<g/>
元	元	k?
~	~	kIx~
Genzanský	Genzanský	k2eAgInSc1d1
námořní	námořní	k2eAgInSc1d1
letecký	letecký	k2eAgInSc1d1
pluk	pluk	k1gInSc1
<g/>
/	/	kIx~
<g/>
skupina	skupina	k1gFnSc1
<g/>
)	)	kIx)
zde	zde	k6eAd1
přistálo	přistát	k5eAaPmAgNnS,k5eAaImAgNnS
24	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Japonci	Japonec	k1gMnPc1
označovali	označovat	k5eAaImAgMnP
P2	P2	k1gFnSc4
jako	jako	k8xS,k8xC
Gloembang	Gloembang	k1gInSc4
podle	podle	k7c2
nedaleké	daleký	k2eNgFnSc2d1
vesnice	vesnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
japonské	japonský	k2eAgFnSc2d1
okupace	okupace	k1gFnSc2
bylo	být	k5eAaImAgNnS
P2	P2	k1gFnSc4
domácí	domácí	k2eAgFnSc7d1
základnou	základna	k1gFnSc7
87	#num#	k4
<g/>
.	.	kIx.
sentai	sentai	k1gNnSc2
(	(	kIx(
<g/>
戦	戦	k?
~	~	kIx~
armádní	armádní	k2eAgInSc1d1
letecký	letecký	k2eAgInSc1d1
pluk	pluk	k1gInSc1
<g/>
/	/	kIx~
<g/>
skupina	skupina	k1gFnSc1
<g/>
)	)	kIx)
s	s	k7c7
Ki-	Ki-	k1gFnSc7
<g/>
44	#num#	k4
Šóki	Šóki	k1gNnSc2
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
chránila	chránit	k5eAaImAgFnS
ropné	ropný	k2eAgFnPc4d1
rafinérie	rafinérie	k1gFnPc4
v	v	k7c6
Plaju	Plaju	k1gFnSc6
a	a	k8xC
Sungei	Sunge	k1gFnSc6
Gerong	Gerong	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
87	#num#	k4
<g/>
.	.	kIx.
sentai	sentai	k6eAd1
z	z	k7c2
letiště	letiště	k1gNnSc2
operovaly	operovat	k5eAaImAgFnP
i	i	k9
stroje	stroj	k1gInSc2
21	#num#	k4
<g/>
.	.	kIx.
<g/>
,	,	kIx,
33	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
77	#num#	k4
<g/>
.	.	kIx.
sentai	sentai	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Koncem	koncem	k7c2
války	válka	k1gFnSc2
našli	najít	k5eAaPmAgMnP
Britové	Brit	k1gMnPc1
na	na	k7c6
P2	P2	k1gFnSc6
sedm	sedm	k4xCc4
Ki-	Ki-	k1gFnPc2
<g/>
45	#num#	k4
<g/>
,	,	kIx,
dvanáct	dvanáct	k4xCc4
Ki-	Ki-	k1gFnPc2
<g/>
46	#num#	k4
<g/>
,	,	kIx,
K-30	K-30	k1gFnSc1
a	a	k8xC
Ki-	Ki-	k1gFnSc1
<g/>
79	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
válce	válka	k1gFnSc6
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Dějiny	dějiny	k1gFnPc4
Indonésie	Indonésie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
vyhlášením	vyhlášení	k1gNnSc7
samostatnosti	samostatnost	k1gFnSc2
se	se	k3xPyFc4
P2	P2	k1gMnSc3
ocitlo	ocitnout	k5eAaPmAgNnS
na	na	k7c6
území	území	k1gNnSc6
nárokovaném	nárokovaný	k2eAgNnSc6d1
Indonésií	Indonésie	k1gFnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
zpočátku	zpočátku	k6eAd1
kontrolovaném	kontrolovaný	k2eAgMnSc6d1
Nizozemci	Nizozemec	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dne	den	k1gInSc2
7	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1947	#num#	k4
bylo	být	k5eAaImAgNnS
letiště	letiště	k1gNnSc1
dobyto	dobyt	k2eAgNnSc1d1
nizozemským	nizozemský	k2eAgInPc3d1
7	#num#	k4
Regiment	regiment	k1gInSc1
Stoottroepen	Stoottroepen	k2eAgInSc1d1
(	(	kIx(
<g/>
pěší	pěší	k2eAgInSc1d1
pluk	pluk	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
následovně	následovně	k6eAd1
používáno	používán	k2eAgNnSc1d1
Pipery	Piper	k1gInPc7
17	#num#	k4
VARWA	VARWA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dubnu	duben	k1gInSc6
1950	#num#	k4
bylo	být	k5eAaImAgNnS
P2	P2	k1gFnPc3
předáno	předat	k5eAaPmNgNnS
Indonésii	Indonésie	k1gFnSc6
a	a	k8xC
VPD	VPD	kA
byly	být	k5eAaImAgInP
využívány	využívat	k5eAaPmNgInP,k5eAaImNgInP
pravděpodobně	pravděpodobně	k6eAd1
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1975	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Sumatra	Sumatra	k1gFnSc1
and	and	k?
Java	Java	k1gFnSc1
1942	#num#	k4
<g/>
:	:	kIx,
Palembang	Palembang	k1gMnSc1
P2	P2	k1gMnSc1
airfield	airfield	k1gMnSc1
<g/>
,	,	kIx,
Sumatra	Sumatra	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
211	#num#	k4
<g/>
squadron	squadron	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2009-07-31	2009-07-31	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
HEYMAN	HEYMAN	kA
<g/>
,	,	kIx,
Jos	Jos	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ADF	ADF	kA
Serials	Serialsa	k1gFnPc2
Newsletter	Newsletter	k1gMnSc1
November	November	k1gMnSc1
-	-	kIx~
December	December	k1gInSc1
2006	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ADF	ADF	kA
Serial	Serial	k1gInSc1
Numbers	Numbers	k1gInSc1
<g/>
,	,	kIx,
prosinec	prosinec	k1gInSc1
2006	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Palembang	Palembanga	k1gFnPc2
P2	P2	k1gMnSc1
Airfield	Airfield	k1gMnSc1
Rediscovered	Rediscovered	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
JUNGSLAGER	JUNGSLAGER	kA
<g/>
,	,	kIx,
Gerard	Gerard	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Forlorn	Forlorn	k1gInSc1
Hope	Hope	k1gInSc4
<g/>
:	:	kIx,
The	The	k1gMnSc5
desperate	desperat	k1gMnSc5
battle	battle	k1gFnSc2
of	of	k?
the	the	k?
Dutch	Dutch	k1gInSc1
against	against	k1gInSc1
Japan	japan	k1gInSc4
<g/>
,	,	kIx,
December	December	k1gInSc4
1941	#num#	k4
<g/>
-March	-March	k1gInSc1
1942	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Amsterdam	Amsterdam	k1gInSc1
<g/>
:	:	kIx,
De	De	k?
Bataafsche	Bataafsch	k1gMnPc4
Leeuw	Leeuw	k1gFnSc2
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
906707660	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
II	II	kA
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
91	#num#	k4
až	až	k9
95	#num#	k4
<g/>
↑	↑	k?
Jungslager	Jungslager	k1gMnSc1
<g/>
,	,	kIx,
str	str	kA
<g/>
.	.	kIx.
138	#num#	k4
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
SHORES	SHORES	kA
<g/>
,	,	kIx,
Christopher	Christophra	k1gFnPc2
<g/>
;	;	kIx,
CULL	CULL	kA
<g/>
,	,	kIx,
Brian	Brian	k1gMnSc1
<g/>
;	;	kIx,
IZAVA	IZAVA	kA
<g/>
,	,	kIx,
Jasuho	Jasuha	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krvavá	krvavý	k2eAgFnSc1d1
jatka	jatka	k1gFnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plzeň	Plzeň	k1gFnSc1
<g/>
:	:	kIx,
Mustang	mustang	k1gMnSc1
<g/>
,	,	kIx,
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85831	#num#	k4
<g/>
-	-	kIx~
<g/>
73	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
HEYMAN	HEYMAN	kA
<g/>
,	,	kIx,
Jos	Jos	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ADF	ADF	kA
Serials	Serialsa	k1gFnPc2
Newsletter	Newsletter	k1gMnSc1
November	November	k1gMnSc1
-	-	kIx~
December	December	k1gInSc1
2006	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ADF	ADF	kA
Serial	Serial	k1gInSc1
Numbers	Numbers	k1gInSc1
<g/>
,	,	kIx,
prosinec	prosinec	k1gInSc1
2006	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Palembang	Palembanga	k1gFnPc2
P2	P2	k1gMnSc1
Airfield	Airfield	k1gMnSc1
Rediscovered	Rediscovered	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
KLEMEN	KLEMEN	kA
<g/>
,	,	kIx,
L.	L.	kA
Forgotten	Forgotten	k2eAgInSc1d1
Campaign	Campaign	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Dutch	Dutch	k1gMnSc1
East	East	k1gMnSc1
Indies	Indies	k1gMnSc1
Campaign	Campaign	k1gMnSc1
1941	#num#	k4
<g/>
-	-	kIx~
<g/>
1942	#num#	k4
<g/>
:	:	kIx,
The	The	k1gFnSc2
Japanese	Japanese	k1gFnSc2
Invasion	Invasion	k1gInSc1
of	of	k?
Sumatra	Sumatra	k1gFnSc1
Island	Island	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
dutcheastindies	dutcheastindies	k1gInSc1
<g/>
.	.	kIx.
<g/>
webs	webs	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2000	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgInPc1d1
také	také	k9
na	na	k7c4
<g/>
:	:	kIx,
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Sumatra	Sumatra	k1gFnSc1
and	and	k?
Java	Java	k1gFnSc1
1942	#num#	k4
<g/>
:	:	kIx,
Palembang	Palembang	k1gMnSc1
P2	P2	k1gMnSc1
airfield	airfield	k1gMnSc1
<g/>
,	,	kIx,
Sumatra	Sumatra	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
211	#num#	k4
<g/>
squadron	squadron	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2009-07-31	2009-07-31	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Letectví	letectví	k1gNnSc1
</s>
