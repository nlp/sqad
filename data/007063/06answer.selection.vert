<s>
Velký	velký	k2eAgInSc1d1	velký
požár	požár	k1gInSc1	požár
Londýna	Londýn	k1gInSc2	Londýn
se	se	k3xPyFc4	se
přehnal	přehnat	k5eAaPmAgMnS	přehnat
přes	přes	k7c4	přes
centrální	centrální	k2eAgFnPc4d1	centrální
části	část	k1gFnPc4	část
londýnské	londýnský	k2eAgFnSc2d1	londýnská
City	City	k1gFnSc2	City
<g/>
,	,	kIx,	,
řádil	řádit	k5eAaImAgMnS	řádit
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
do	do	k7c2	do
5	[number]	k4	5
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1666	[number]	k4	1666
a	a	k8xC	a
zničil	zničit	k5eAaPmAgInS	zničit
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
City	City	k1gFnSc2	City
uvnitř	uvnitř	k7c2	uvnitř
starých	starý	k2eAgFnPc2d1	stará
římských	římský	k2eAgFnPc2d1	římská
městských	městský	k2eAgFnPc2d1	městská
hradeb	hradba	k1gFnPc2	hradba
<g/>
.	.	kIx.	.
</s>
