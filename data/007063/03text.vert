<s>
Velký	velký	k2eAgInSc1d1	velký
požár	požár	k1gInSc1	požár
Londýna	Londýn	k1gInSc2	Londýn
se	se	k3xPyFc4	se
přehnal	přehnat	k5eAaPmAgMnS	přehnat
přes	přes	k7c4	přes
centrální	centrální	k2eAgFnPc4d1	centrální
části	část	k1gFnPc4	část
londýnské	londýnský	k2eAgFnSc2d1	londýnská
City	City	k1gFnSc2	City
<g/>
,	,	kIx,	,
řádil	řádit	k5eAaImAgMnS	řádit
od	od	k7c2	od
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
do	do	k7c2	do
5	[number]	k4	5
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1666	[number]	k4	1666
a	a	k8xC	a
zničil	zničit	k5eAaPmAgInS	zničit
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
City	City	k1gFnSc2	City
uvnitř	uvnitř	k7c2	uvnitř
starých	starý	k2eAgFnPc2d1	stará
římských	římský	k2eAgFnPc2d1	římská
městských	městský	k2eAgFnPc2d1	městská
hradeb	hradba	k1gFnPc2	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Požár	požár	k1gInSc1	požár
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
město	město	k1gNnSc4	město
v	v	k7c6	v
době	doba	k1gFnSc6	doba
doznívání	doznívání	k1gNnSc1	doznívání
jiné	jiný	k2eAgFnSc2d1	jiná
katastrofy	katastrofa	k1gFnSc2	katastrofa
<g/>
,	,	kIx,	,
velkého	velký	k2eAgInSc2d1	velký
londýnského	londýnský	k2eAgInSc2d1	londýnský
moru	mor	k1gInSc2	mor
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
město	město	k1gNnSc4	město
zasáhl	zasáhnout	k5eAaPmAgMnS	zasáhnout
o	o	k7c4	o
rok	rok	k1gInSc4	rok
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
požáru	požár	k1gInSc2	požár
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
přestavbě	přestavba	k1gFnSc3	přestavba
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
byla	být	k5eAaImAgFnS	být
Christopherem	Christopher	k1gInSc7	Christopher
Wrenem	Wreno	k1gNnSc7	Wreno
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
nová	nový	k2eAgFnSc1d1	nová
katedrála	katedrála	k1gFnSc1	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Pavla	Pavel	k1gMnSc2	Pavel
<g/>
.	.	kIx.	.
</s>
<s>
Předchozí	předchozí	k2eAgInPc4d1	předchozí
dva	dva	k4xCgInPc4	dva
požáry	požár	k1gInPc4	požár
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1133	[number]	k4	1133
a	a	k8xC	a
1212	[number]	k4	1212
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
způsobily	způsobit	k5eAaPmAgFnP	způsobit
velké	velký	k2eAgFnPc4d1	velká
škody	škoda	k1gFnPc4	škoda
na	na	k7c6	na
městských	městský	k2eAgFnPc6d1	městská
stavbách	stavba	k1gFnPc6	stavba
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
označovány	označován	k2eAgMnPc4d1	označován
stejným	stejný	k2eAgInSc7d1	stejný
přívlastkem	přívlastek	k1gInSc7	přívlastek
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
29	[number]	k4	29
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1940	[number]	k4	1940
nálet	nálet	k1gInSc1	nálet
německého	německý	k2eAgNnSc2d1	německé
letectva	letectvo	k1gNnSc2	letectvo
vyústil	vyústit	k5eAaPmAgInS	vyústit
v	v	k7c4	v
požár	požár	k1gInSc4	požár
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k9	jako
Druhý	druhý	k4xOgInSc1	druhý
velký	velký	k2eAgInSc1d1	velký
požár	požár	k1gInSc1	požár
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Požár	požár	k1gInSc1	požár
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1666	[number]	k4	1666
<g/>
,	,	kIx,	,
ráno	ráno	k6eAd1	ráno
v	v	k7c6	v
ulici	ulice	k1gFnSc6	ulice
Pudding	Pudding	k1gInSc4	Pudding
Lane	Lane	k1gNnSc2	Lane
<g/>
,	,	kIx,	,
v	v	k7c6	v
domě	dům	k1gInSc6	dům
Tomase	Tomasa	k1gFnSc3	Tomasa
Farrynora	Farrynor	k1gMnSc2	Farrynor
<g/>
,	,	kIx,	,
pekaře	pekař	k1gMnSc2	pekař
krále	král	k1gMnSc2	král
Karla	Karel	k1gMnSc2	Karel
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Farrinor	Farrinor	k1gMnSc1	Farrinor
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zapomněl	zapomnět	k5eAaImAgMnS	zapomnět
uhasit	uhasit	k5eAaPmF	uhasit
oheň	oheň	k1gInSc4	oheň
v	v	k7c6	v
peci	pec	k1gFnSc6	pec
<g/>
,	,	kIx,	,
když	když	k8xS	když
šel	jít	k5eAaImAgMnS	jít
večer	večer	k6eAd1	večer
spát	spát	k5eAaImF	spát
<g/>
,	,	kIx,	,
a	a	k8xC	a
krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
půlnoci	půlnoc	k1gFnSc6	půlnoc
jiskry	jiskra	k1gFnSc2	jiskra
z	z	k7c2	z
doutnajících	doutnající	k2eAgInPc2d1	doutnající
uhlíků	uhlík	k1gInPc2	uhlík
zapálily	zapálit	k5eAaPmAgFnP	zapálit
palivové	palivový	k2eAgNnSc4d1	palivové
dříví	dříví	k1gNnSc4	dříví
u	u	k7c2	u
pece	pec	k1gFnSc2	pec
<g/>
.	.	kIx.	.
</s>
<s>
Pekař	Pekař	k1gMnSc1	Pekař
i	i	k9	i
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
rodinou	rodina	k1gFnSc7	rodina
stihl	stihnout	k5eAaPmAgMnS	stihnout
uniknout	uniknout	k5eAaPmF	uniknout
z	z	k7c2	z
hořícího	hořící	k2eAgInSc2d1	hořící
domu	dům	k1gInSc2	dům
oknem	okno	k1gNnSc7	okno
v	v	k7c4	v
poschodí	poschodí	k1gNnSc4	poschodí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc4	jeho
hospodyni	hospodyně	k1gFnSc4	hospodyně
se	se	k3xPyFc4	se
únik	únik	k1gInSc4	únik
nezdařil	zdařit	k5eNaPmAgInS	zdařit
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
tak	tak	k6eAd1	tak
první	první	k4xOgInSc4	první
obětí	oběť	k1gFnPc2	oběť
požáru	požár	k1gInSc2	požár
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
hodiny	hodina	k1gFnSc2	hodina
byl	být	k5eAaImAgInS	být
informován	informovat	k5eAaBmNgMnS	informovat
starosta	starosta	k1gMnSc1	starosta
City	City	k1gFnSc2	City
sir	sir	k1gMnSc1	sir
Tomas	Tomas	k1gMnSc1	Tomas
Bloodworth	Bloodworth	k1gMnSc1	Bloodworth
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
však	však	k9	však
situaci	situace	k1gFnSc4	situace
podcenil	podcenit	k5eAaPmAgInS	podcenit
<g/>
.	.	kIx.	.
</s>
<s>
Živnou	živný	k2eAgFnSc4d1	živná
půdu	půda	k1gFnSc4	půda
pro	pro	k7c4	pro
obrovský	obrovský	k2eAgInSc4d1	obrovský
požár	požár	k1gInSc4	požár
připravilo	připravit	k5eAaPmAgNnS	připravit
dlouho	dlouho	k6eAd1	dlouho
trvající	trvající	k2eAgNnSc1d1	trvající
suché	suchý	k2eAgNnSc1d1	suché
a	a	k8xC	a
teplé	teplý	k2eAgNnSc1d1	teplé
počasí	počasí	k1gNnSc1	počasí
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgInSc3	jenž
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgInS	přidat
trvalý	trvalý	k2eAgInSc1d1	trvalý
suchý	suchý	k2eAgInSc1d1	suchý
severovýchodní	severovýchodní	k2eAgInSc1d1	severovýchodní
vítr	vítr	k1gInSc1	vítr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vanul	vanout	k5eAaImAgInS	vanout
bez	bez	k7c2	bez
přestání	přestání	k1gNnSc2	přestání
celé	celý	k2eAgInPc4d1	celý
týdny	týden	k1gInPc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
většiny	většina	k1gFnSc2	většina
domů	dům	k1gInPc2	dům
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
byly	být	k5eAaImAgInP	být
použity	použít	k5eAaPmNgInP	použít
hořlavé	hořlavý	k2eAgInPc1d1	hořlavý
materiály	materiál	k1gInPc1	materiál
(	(	kIx(	(
<g/>
dřevo	dřevo	k1gNnSc1	dřevo
a	a	k8xC	a
sláma	sláma	k1gFnSc1	sláma
<g/>
)	)	kIx)	)
a	a	k8xC	a
jiskry	jiskra	k1gFnPc1	jiskra
z	z	k7c2	z
hořícího	hořící	k2eAgInSc2d1	hořící
pekařova	pekařův	k2eAgInSc2d1	pekařův
domu	dům	k1gInSc2	dům
zapálily	zapálit	k5eAaPmAgInP	zapálit
sousední	sousední	k2eAgInPc1d1	sousední
domy	dům	k1gInPc1	dům
<g/>
.	.	kIx.	.
</s>
<s>
Podporován	podporován	k2eAgInSc1d1	podporován
východním	východní	k2eAgInSc7d1	východní
větrem	vítr	k1gInSc7	vítr
se	se	k3xPyFc4	se
požár	požár	k1gInSc1	požár
začal	začít	k5eAaPmAgInS	začít
šířit	šířit	k5eAaImF	šířit
Londýnem	Londýn	k1gInSc7	Londýn
a	a	k8xC	a
počáteční	počáteční	k2eAgFnPc1d1	počáteční
snahy	snaha	k1gFnPc1	snaha
zabránit	zabránit	k5eAaPmF	zabránit
jeho	jeho	k3xOp3gNnSc4	jeho
šíření	šíření	k1gNnSc4	šíření
zbořením	zboření	k1gNnSc7	zboření
pásu	pás	k1gInSc2	pás
domů	dům	k1gInPc2	dům
jako	jako	k8xC	jako
požární	požární	k2eAgFnPc1d1	požární
zábrany	zábrana	k1gFnPc1	zábrana
byly	být	k5eAaImAgFnP	být
neorganizované	organizovaný	k2eNgFnPc1d1	neorganizovaná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
úterý	úterý	k1gNnSc6	úterý
vítr	vítr	k1gInSc1	vítr
začal	začít	k5eAaPmAgInS	začít
polevovat	polevovat	k5eAaImF	polevovat
a	a	k8xC	a
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
požár	požár	k1gInSc1	požár
začal	začít	k5eAaPmAgInS	začít
slábnout	slábnout	k5eAaImF	slábnout
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
dopoledne	dopoledne	k6eAd1	dopoledne
už	už	k6eAd1	už
byl	být	k5eAaImAgInS	být
skoro	skoro	k6eAd1	skoro
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
navečer	navečer	k1gInSc4	navečer
plameny	plamen	k1gInPc1	plamen
znovu	znovu	k6eAd1	znovu
vyšlehly	vyšlehnout	k5eAaPmAgInP	vyšlehnout
u	u	k7c2	u
Temple	templ	k1gInSc5	templ
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
domy	dům	k1gInPc1	dům
byly	být	k5eAaImAgInP	být
odstřeleny	odstřelit	k5eAaPmNgInP	odstřelit
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgInS	být
oheň	oheň	k1gInSc4	oheň
překonán	překonán	k2eAgInSc1d1	překonán
<g/>
.	.	kIx.	.
</s>
<s>
Požár	požár	k1gInSc1	požár
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1666	[number]	k4	1666
byl	být	k5eAaImAgInS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
katastrof	katastrofa	k1gFnPc2	katastrofa
v	v	k7c6	v
dějinách	dějiny	k1gFnPc6	dějiny
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Zničil	zničit	k5eAaPmAgMnS	zničit
13	[number]	k4	13
200	[number]	k4	200
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
87	[number]	k4	87
chrámů	chrám	k1gInPc2	chrám
<g/>
,	,	kIx,	,
6	[number]	k4	6
kaplí	kaple	k1gFnPc2	kaple
a	a	k8xC	a
44	[number]	k4	44
cechovních	cechovní	k2eAgNnPc2d1	cechovní
sídel	sídlo	k1gNnPc2	sídlo
<g/>
,	,	kIx,	,
Královskou	královský	k2eAgFnSc4d1	královská
burzu	burza	k1gFnSc4	burza
<g/>
,	,	kIx,	,
Custom	Custom	k1gInSc4	Custom
House	house	k1gNnSc4	house
<g/>
,	,	kIx,	,
původní	původní	k2eAgFnSc4d1	původní
katedrálu	katedrála	k1gFnSc4	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Pavla	Pavel	k1gMnSc2	Pavel
<g/>
,	,	kIx,	,
Guidhall	Guidhall	k1gInSc1	Guidhall
<g/>
,	,	kIx,	,
Bridewell	Bridewell	k1gInSc1	Bridewell
Palace	Palace	k1gFnSc2	Palace
<g/>
,	,	kIx,	,
městské	městský	k2eAgNnSc1d1	Městské
vězení	vězení	k1gNnSc1	vězení
<g/>
,	,	kIx,	,
Session	Session	k1gInSc1	Session
House	house	k1gNnSc4	house
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgInPc4	čtyři
mosty	most	k1gInPc4	most
přes	přes	k7c4	přes
řeky	řek	k1gMnPc4	řek
Temži	Temže	k1gFnSc4	Temže
a	a	k8xC	a
Fleet	Fleet	k1gInSc4	Fleet
<g/>
,	,	kIx,	,
tři	tři	k4xCgFnPc1	tři
městské	městský	k2eAgFnPc1d1	městská
brány	brána	k1gFnPc1	brána
a	a	k8xC	a
domovy	domov	k1gInPc1	domov
100	[number]	k4	100
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
jedna	jeden	k4xCgFnSc1	jeden
šestina	šestina	k1gFnSc1	šestina
obyvatel	obyvatel	k1gMnPc2	obyvatel
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obětí	oběť	k1gFnPc2	oběť
požáru	požár	k1gInSc2	požár
není	být	k5eNaImIp3nS	být
přesně	přesně	k6eAd1	přesně
znám	znám	k2eAgMnSc1d1	znám
<g/>
.	.	kIx.	.
</s>
<s>
Tradičně	tradičně	k6eAd1	tradičně
se	se	k3xPyFc4	se
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
malý	malý	k2eAgInSc1d1	malý
počet	počet	k1gInSc1	počet
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
–	–	k?	–
<g/>
16	[number]	k4	16
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
některé	některý	k3yIgInPc1	některý
novější	nový	k2eAgInPc1d2	novější
prameny	pramen	k1gInPc1	pramen
spekulují	spekulovat	k5eAaImIp3nP	spekulovat
o	o	k7c6	o
možnosti	možnost	k1gFnSc6	možnost
až	až	k9	až
tisíců	tisíc	k4xCgInPc2	tisíc
obětí	oběť	k1gFnPc2	oběť
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
nadýchání	nadýchání	k1gNnSc2	nadýchání
kouře	kouř	k1gInSc2	kouř
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
velký	velký	k2eAgInSc1d1	velký
rozsah	rozsah	k1gInSc1	rozsah
škod	škoda	k1gFnPc2	škoda
způsobený	způsobený	k2eAgInSc1d1	způsobený
neúmyslně	úmyslně	k6eNd1	úmyslně
zaviněným	zaviněný	k2eAgInSc7d1	zaviněný
požárem	požár	k1gInSc7	požár
nebyl	být	k5eNaImAgInS	být
na	na	k7c6	na
světě	svět	k1gInSc6	svět
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
hradeb	hradba	k1gFnPc2	hradba
bylo	být	k5eAaImAgNnS	být
zničeno	zničit	k5eAaPmNgNnS	zničit
pět	pět	k4xCc1	pět
šestin	šestina	k1gFnPc2	šestina
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
1,3	[number]	k4	1,3
km2	km2	k4	km2
<g/>
)	)	kIx)	)
a	a	k8xC	a
vně	vně	k7c2	vně
hradeb	hradba	k1gFnPc2	hradba
asi	asi	k9	asi
jedna	jeden	k4xCgFnSc1	jeden
šestina	šestina	k1gFnSc1	šestina
(	(	kIx(	(
<g/>
255	[number]	k4	255
000	[number]	k4	000
m2	m2	k4	m2
<g/>
)	)	kIx)	)
plochy	plocha	k1gFnSc2	plocha
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zůstala	zůstat	k5eAaPmAgFnS	zůstat
uvnitř	uvnitř	k7c2	uvnitř
města	město	k1gNnSc2	město
nepoškozena	poškozen	k2eNgFnSc1d1	nepoškozena
<g/>
.	.	kIx.	.
</s>
<s>
Zůstaly	zůstat	k5eAaPmAgFnP	zůstat
stát	stát	k5eAaPmF	stát
jen	jen	k9	jen
ojedinělé	ojedinělý	k2eAgFnPc4d1	ojedinělá
stavby	stavba	k1gFnPc4	stavba
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Hodnota	hodnota	k1gFnSc1	hodnota
zničeného	zničený	k2eAgInSc2d1	zničený
majetku	majetek	k1gInSc2	majetek
byla	být	k5eAaImAgFnS	být
odhadnuta	odhadnut	k2eAgFnSc1d1	odhadnuta
asi	asi	k9	asi
na	na	k7c4	na
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
liber	libra	k1gFnPc2	libra
<g/>
.	.	kIx.	.
</s>
<s>
Zničeny	zničen	k2eAgFnPc1d1	zničena
byly	být	k5eAaImAgFnP	být
nejen	nejen	k6eAd1	nejen
domy	dům	k1gInPc4	dům
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jejich	jejich	k3xOp3gNnSc1	jejich
vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
vybavení	vybavení	k1gNnSc1	vybavení
včetně	včetně	k7c2	včetně
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
obrazů	obraz	k1gInPc2	obraz
a	a	k8xC	a
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Požár	požár	k1gInSc1	požár
měl	mít	k5eAaImAgInS	mít
ale	ale	k9	ale
i	i	k9	i
pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
efekt	efekt	k1gInSc1	efekt
–	–	k?	–
zastavil	zastavit	k5eAaPmAgInS	zastavit
šíření	šíření	k1gNnSc4	šíření
morové	morový	k2eAgFnSc2d1	morová
nákazy	nákaza	k1gFnSc2	nákaza
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
Londýn	Londýn	k1gInSc4	Londýn
předchozího	předchozí	k2eAgInSc2d1	předchozí
roku	rok	k1gInSc2	rok
a	a	k8xC	a
bývá	bývat	k5eAaImIp3nS	bývat
označována	označován	k2eAgFnSc1d1	označována
jako	jako	k8xS	jako
Velký	velký	k2eAgInSc1d1	velký
mor	mor	k1gInSc1	mor
a	a	k8xC	a
která	který	k3yQgFnSc1	který
dle	dle	k7c2	dle
soupisu	soupis	k1gInSc6	soupis
mrtvých	mrtvý	k1gMnPc2	mrtvý
způsobila	způsobit	k5eAaPmAgFnS	způsobit
smrt	smrt	k1gFnSc4	smrt
68	[number]	k4	68
590	[number]	k4	590
lidí	člověk	k1gMnPc2	člověk
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
některých	některý	k3yIgInPc2	některý
odhadů	odhad	k1gInPc2	odhad
však	však	k9	však
až	až	k9	až
100	[number]	k4	100
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zhotovením	zhotovení	k1gNnSc7	zhotovení
plánů	plán	k1gInPc2	plán
na	na	k7c4	na
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
města	město	k1gNnSc2	město
byl	být	k5eAaImAgMnS	být
pověřen	pověřit	k5eAaPmNgMnS	pověřit
Christopher	Christophra	k1gFnPc2	Christophra
Wren	Wreno	k1gNnPc2	Wreno
<g/>
.	.	kIx.	.
</s>
<s>
Navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
vybudovat	vybudovat	k5eAaPmF	vybudovat
hlavní	hlavní	k2eAgFnPc4d1	hlavní
třídy	třída	k1gFnPc4	třída
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
sever-jih	severih	k1gInSc4	sever-jih
a	a	k8xC	a
východ-západ	východápad	k1gInSc4	východ-západ
<g/>
,	,	kIx,	,
postavit	postavit	k5eAaPmF	postavit
kostely	kostel	k1gInPc1	kostel
na	na	k7c6	na
izolovaných	izolovaný	k2eAgNnPc6d1	izolované
<g/>
,	,	kIx,	,
významných	významný	k2eAgNnPc6d1	významné
místech	místo	k1gNnPc6	místo
<g/>
,	,	kIx,	,
soustředit	soustředit	k5eAaPmF	soustředit
veřejný	veřejný	k2eAgInSc4d1	veřejný
život	život	k1gInSc4	život
na	na	k7c6	na
nově	nově	k6eAd1	nově
postavených	postavený	k2eAgNnPc6d1	postavené
náměstích	náměstí	k1gNnPc6	náměstí
<g/>
,	,	kIx,	,
soustředit	soustředit	k5eAaPmF	soustředit
nejvýznamnější	významný	k2eAgFnPc4d3	nejvýznamnější
cechovní	cechovní	k2eAgNnPc4d1	cechovní
sídla	sídlo	k1gNnPc4	sídlo
do	do	k7c2	do
jedné	jeden	k4xCgFnSc2	jeden
skupiny	skupina	k1gFnSc2	skupina
budov	budova	k1gFnPc2	budova
připojených	připojený	k2eAgFnPc2d1	připojená
ke	k	k7c3	k
Guildhall	Guildhall	k1gInSc4	Guildhall
a	a	k8xC	a
postavit	postavit	k5eAaPmF	postavit
krásné	krásný	k2eAgNnSc4d1	krásné
nábřeží	nábřeží	k1gNnSc4	nábřeží
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
od	od	k7c2	od
Blackfairs	Blackfairsa	k1gFnPc2	Blackfairsa
až	až	k9	až
po	po	k7c4	po
Tower	Tower	k1gInSc4	Tower
<g/>
.	.	kIx.	.
</s>
<s>
Wren	Wren	k1gMnSc1	Wren
chtěl	chtít	k5eAaImAgMnS	chtít
vybudovat	vybudovat	k5eAaPmF	vybudovat
nový	nový	k2eAgInSc4d1	nový
systém	systém	k1gInSc4	systém
ulic	ulice	k1gFnPc2	ulice
s	s	k7c7	s
odstupňovanou	odstupňovaný	k2eAgFnSc7d1	odstupňovaná
šířkou	šířka	k1gFnSc7	šířka
–	–	k?	–
30	[number]	k4	30
<g/>
,	,	kIx,	,
60	[number]	k4	60
a	a	k8xC	a
90	[number]	k4	90
stop	stopa	k1gFnPc2	stopa
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnoho	mnoho	k4c1	mnoho
budov	budova	k1gFnPc2	budova
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
ušetřeno	ušetřit	k5eAaPmNgNnS	ušetřit
požáru	požár	k1gInSc2	požár
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
majitelé	majitel	k1gMnPc1	majitel
kladli	klást	k5eAaImAgMnP	klást
výrazný	výrazný	k2eAgInSc4d1	výrazný
odpor	odpor	k1gInSc4	odpor
proti	proti	k7c3	proti
jejich	jejich	k3xOp3gNnSc3	jejich
zboření	zboření	k1gNnSc1	zboření
<g/>
,	,	kIx,	,
Wrenovy	Wrenův	k2eAgInPc1d1	Wrenův
plány	plán	k1gInPc1	plán
se	se	k3xPyFc4	se
neuskutečnily	uskutečnit	k5eNaPmAgInP	uskutečnit
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
důvodem	důvod	k1gInSc7	důvod
středověkého	středověký	k2eAgInSc2d1	středověký
rázu	ráz	k1gInSc2	ráz
současného	současný	k2eAgInSc2d1	současný
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Wren	Wren	k1gMnSc1	Wren
byl	být	k5eAaImAgMnS	být
pověřen	pověřit	k5eAaPmNgMnS	pověřit
obnovou	obnova	k1gFnSc7	obnova
zničených	zničený	k2eAgInPc2d1	zničený
kostelů	kostel	k1gInPc2	kostel
a	a	k8xC	a
katedrály	katedrála	k1gFnSc2	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Pavla	Pavel	k1gMnSc2	Pavel
<g/>
,	,	kIx,	,
rekonstrukci	rekonstrukce	k1gFnSc3	rekonstrukce
městské	městský	k2eAgFnSc2d1	městská
zástavby	zástavba	k1gFnSc2	zástavba
řídil	řídit	k5eAaImAgMnS	řídit
Robert	Robert	k1gMnSc1	Robert
Hooke	Hook	k1gInSc2	Hook
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1667	[number]	k4	1667
Parlament	parlament	k1gInSc1	parlament
založil	založit	k5eAaPmAgInS	založit
fondy	fond	k1gInPc4	fond
pro	pro	k7c4	pro
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
města	město	k1gNnSc2	město
z	z	k7c2	z
výtěžku	výtěžek	k1gInSc2	výtěžek
nové	nový	k2eAgFnSc2d1	nová
daně	daň	k1gFnSc2	daň
z	z	k7c2	z
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
domy	dům	k1gInPc1	dům
byly	být	k5eAaImAgInP	být
stavěny	stavit	k5eAaImNgInP	stavit
z	z	k7c2	z
nehořlavých	hořlavý	k2eNgInPc2d1	nehořlavý
materiálů	materiál	k1gInPc2	materiál
(	(	kIx(	(
<g/>
cihel	cihla	k1gFnPc2	cihla
a	a	k8xC	a
kamene	kámen	k1gInSc2	kámen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
zlepšena	zlepšen	k2eAgFnSc1d1	zlepšena
kanalizace	kanalizace	k1gFnSc1	kanalizace
a	a	k8xC	a
zlepšila	zlepšit	k5eAaPmAgFnS	zlepšit
se	se	k3xPyFc4	se
dopravní	dopravní	k2eAgFnSc1d1	dopravní
dostupnost	dostupnost	k1gFnSc1	dostupnost
<g/>
.	.	kIx.	.
</s>
<s>
Památník	památník	k1gInSc1	památník
Velkého	velký	k2eAgInSc2d1	velký
požáru	požár	k1gInSc2	požár
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
označovaný	označovaný	k2eAgInSc1d1	označovaný
jednoduše	jednoduše	k6eAd1	jednoduše
jako	jako	k8xC	jako
Monument	monument	k1gInSc4	monument
<g/>
,	,	kIx,	,
navržený	navržený	k2eAgInSc4d1	navržený
Christopherem	Christopher	k1gMnSc7	Christopher
Wrenem	Wren	k1gMnSc7	Wren
a	a	k8xC	a
Robertem	Robert	k1gMnSc7	Robert
Hookem	Hooek	k1gMnSc7	Hooek
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
poblíž	poblíž	k7c2	poblíž
místa	místo	k1gNnSc2	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
požár	požár	k1gInSc1	požár
začal	začít	k5eAaPmAgInS	začít
–	–	k?	–
u	u	k7c2	u
severního	severní	k2eAgInSc2d1	severní
konce	konec	k1gInSc2	konec
London	London	k1gMnSc1	London
Bridge	Bridge	k1gNnPc2	Bridge
<g/>
.	.	kIx.	.
</s>
<s>
Nároží	nároží	k1gNnSc1	nároží
Giltspur	Giltspur	k1gMnSc1	Giltspur
Street	Street	k1gMnSc1	Street
a	a	k8xC	a
Cock	Cock	k1gMnSc1	Cock
Lane	Lan	k1gFnSc2	Lan
<g/>
,	,	kIx,	,
označované	označovaný	k2eAgFnSc2d1	označovaná
jako	jako	k9	jako
Pye	Pye	k1gFnSc7	Pye
Corner	Cornero	k1gNnPc2	Cornero
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
požár	požár	k1gInSc1	požár
skončil	skončit	k5eAaPmAgInS	skončit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
osazeno	osadit	k5eAaPmNgNnS	osadit
malou	malý	k2eAgFnSc7d1	malá
pozlacenou	pozlacený	k2eAgFnSc7d1	pozlacená
soškou	soška	k1gFnSc7	soška
nazývanou	nazývaný	k2eAgFnSc7d1	nazývaná
Fat	fatum	k1gNnPc2	fatum
Boy	boa	k1gFnSc2	boa
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
požárem	požár	k1gInSc7	požár
připravil	připravit	k5eAaPmAgMnS	připravit
grafik	grafik	k1gMnSc1	grafik
a	a	k8xC	a
rytec	rytec	k1gMnSc1	rytec
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
Václav	Václav	k1gMnSc1	Václav
Hollar	Hollar	k1gMnSc1	Hollar
řadu	řada	k1gFnSc4	řada
pohledů	pohled	k1gInPc2	pohled
na	na	k7c4	na
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
přestalo	přestat	k5eAaPmAgNnS	přestat
existovat	existovat	k5eAaImF	existovat
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
známa	znám	k2eAgFnSc1d1	známa
podoba	podoba	k1gFnSc1	podoba
středověkého	středověký	k2eAgNnSc2d1	středověké
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
podkladů	podklad	k1gInPc2	podklad
vycházelo	vycházet	k5eAaImAgNnS	vycházet
při	při	k7c6	při
jeho	jeho	k3xOp3gFnSc6	jeho
obnově	obnova	k1gFnSc6	obnova
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Velký	velký	k2eAgInSc4d1	velký
požár	požár	k1gInSc4	požár
Londýna	Londýn	k1gInSc2	Londýn
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
