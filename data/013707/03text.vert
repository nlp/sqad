<s>
Over	Over	k1gInSc1
Head	Heada	k1gFnPc2
Valve	Valev	k1gFnSc2
</s>
<s>
Rozvod	rozvod	k1gInSc1
OHV	OHV	kA
na	na	k7c6
motoru	motor	k1gInSc6
z	z	k7c2
motocyklu	motocykl	k1gInSc2
Norton	Norton	k1gInSc1
</s>
<s>
OHV	OHV	kA
(	(	kIx(
<g/>
Over	Over	k1gFnSc1
Head	Head	k1gFnSc1
Valve	Valev	k1gFnSc1
-	-	kIx~
v	v	k7c6
USA	USA	kA
místy	místy	k6eAd1
nazývaný	nazývaný	k2eAgInSc1d1
také	také	k9
jako	jako	k9
"	"	kIx"
<g/>
Pushrod	Pushrod	k1gInSc1
<g/>
"	"	kIx"
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
typ	typ	k1gInSc1
ventilového	ventilový	k2eAgInSc2d1
rozvodu	rozvod	k1gInSc2
pístového	pístový	k2eAgInSc2d1
motoru	motor	k1gInSc2
<g/>
,	,	kIx,
u	u	k7c2
kterého	který	k3yRgInSc2
jsou	být	k5eAaImIp3nP
ventily	ventil	k1gInPc1
umístěny	umístěn	k2eAgInPc1d1
v	v	k7c6
hlavě	hlava	k1gFnSc6
válců	válec	k1gInPc2
a	a	k8xC
vačkový	vačkový	k2eAgInSc1d1
hřídel	hřídel	k1gInSc1
je	být	k5eAaImIp3nS
umístěn	umístit	k5eAaPmNgInS
v	v	k7c6
bloku	blok	k1gInSc6
motoru	motor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Charakteristika	charakteristika	k1gFnSc1
</s>
<s>
Mezi	mezi	k7c7
vačkovým	vačkový	k2eAgInSc7d1
hřídelem	hřídel	k1gInSc7
a	a	k8xC
ventily	ventil	k1gInPc7
je	být	k5eAaImIp3nS
relativně	relativně	k6eAd1
velká	velký	k2eAgFnSc1d1
vzdálenost	vzdálenost	k1gFnSc1
(	(	kIx(
<g/>
přibližně	přibližně	k6eAd1
úměrná	úměrná	k1gFnSc1
zdvihu	zdvih	k1gInSc2
motoru	motor	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přenos	přenos	k1gInSc1
pohybu	pohyb	k1gInSc2
mezi	mezi	k7c7
vačkou	vačkou	k?
a	a	k8xC
ventilem	ventil	k1gInSc7
je	být	k5eAaImIp3nS
proto	proto	k6eAd1
realizován	realizovat	k5eAaBmNgInS
dalšími	další	k2eAgInPc7d1
prvky	prvek	k1gInPc7
mechanismu	mechanismus	k1gInSc2
<g/>
:	:	kIx,
zdvihátko	zdvihátko	k1gNnSc1
<g/>
,	,	kIx,
zdvihací	zdvihací	k2eAgFnSc1d1
tyčka	tyčka	k1gFnSc1
<g/>
,	,	kIx,
vahadla	vahadlo	k1gNnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozvod	rozvod	k1gInSc1
tak	tak	k6eAd1
obsahuje	obsahovat	k5eAaImIp3nS
velký	velký	k2eAgInSc1d1
počet	počet	k1gInSc1
součástek	součástka	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
nepříznivě	příznivě	k6eNd1
ovlivňují	ovlivňovat	k5eAaImIp3nP
jeho	jeho	k3xOp3gFnSc4
tuhost	tuhost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
porovnání	porovnání	k1gNnSc6
s	s	k7c7
rozvodem	rozvod	k1gInSc7
OHC	OHC	kA
větší	veliký	k2eAgInSc4d2
počet	počet	k1gInSc4
součástek	součástka	k1gFnPc2
a	a	k8xC
zároveň	zároveň	k6eAd1
větší	veliký	k2eAgFnSc1d2
hmota	hmota	k1gFnSc1
vykonává	vykonávat	k5eAaImIp3nS
nepříznivý	příznivý	k2eNgInSc4d1
vratný	vratný	k2eAgInSc4d1
pohyb	pohyb	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
se	se	k3xPyFc4
projevuje	projevovat	k5eAaImIp3nS
setrvačnými	setrvačný	k2eAgFnPc7d1
silami	síla	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
výhodou	výhoda	k1gFnSc7
rozvodu	rozvod	k1gInSc2
OHV	OHV	kA
je	být	k5eAaImIp3nS
jednodušší	jednoduchý	k2eAgNnSc1d2
řešení	řešení	k1gNnSc1
pohonu	pohon	k1gInSc2
vačkového	vačkový	k2eAgInSc2d1
hřídele	hřídel	k1gInSc2
<g/>
,	,	kIx,
díky	díky	k7c3
kterému	který	k3yRgInSc3,k3yQgInSc3,k3yIgInSc3
má	mít	k5eAaImIp3nS
tento	tento	k3xDgInSc1
rozvod	rozvod	k1gInSc1
vyšší	vysoký	k2eAgFnSc4d2
životnost	životnost	k1gFnSc4
a	a	k8xC
spolehlivost	spolehlivost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevýhody	nevýhoda	k1gFnPc4
rozvodu	rozvod	k1gInSc2
OHV	OHV	kA
převažují	převažovat	k5eAaImIp3nP
hlavně	hlavně	k9
u	u	k7c2
vysokootáčkových	vysokootáčkový	k2eAgInPc2d1
motorů	motor	k1gInPc2
<g/>
,	,	kIx,
proto	proto	k8xC
se	se	k3xPyFc4
u	u	k7c2
moderních	moderní	k2eAgInPc2d1
spalovacích	spalovací	k2eAgInPc2d1
motorů	motor	k1gInPc2
pro	pro	k7c4
pohon	pohon	k1gInSc4
automobilů	automobil	k1gInPc2
skoro	skoro	k6eAd1
nevyskytuje	vyskytovat	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Pohon	pohon	k1gInSc1
rozvodu	rozvod	k1gInSc2
</s>
<s>
Pohon	pohon	k1gInSc1
rozvodu	rozvod	k1gInSc2
je	být	k5eAaImIp3nS
vzhledem	vzhled	k1gInSc7
na	na	k7c4
výhodné	výhodný	k2eAgNnSc4d1
umístění	umístění	k1gNnSc4
vačkového	vačkový	k2eAgInSc2d1
hřídele	hřídel	k1gInSc2
blízko	blízko	k6eAd1
u	u	k7c2
klikovky	klikovka	k1gFnSc2
možné	možný	k2eAgFnSc2d1
realizovat	realizovat	k5eAaBmF
jednoduchým	jednoduchý	k2eAgNnSc7d1
ozubeným	ozubený	k2eAgNnSc7d1
soukolím	soukolí	k1gNnSc7
nebo	nebo	k8xC
ozubenými	ozubený	k2eAgNnPc7d1
koly	kolo	k1gNnPc7
se	se	k3xPyFc4
řetězem	řetěz	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Pohon	pohon	k1gInSc1
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
řešen	řešit	k5eAaImNgInS
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
umožnil	umožnit	k5eAaPmAgInS
přesné	přesný	k2eAgNnSc4d1
nastavení	nastavení	k1gNnSc4
vzájemné	vzájemný	k2eAgFnSc2d1
pozice	pozice	k1gFnSc2
klikového	klikový	k2eAgInSc2d1
a	a	k8xC
vačkového	vačkový	k2eAgInSc2d1
hřídele	hřídel	k1gInSc2
a	a	k8xC
aby	aby	kYmCp3nS
se	se	k3xPyFc4
během	během	k7c2
provozu	provoz	k1gInSc2
motoru	motor	k1gInSc2
toto	tento	k3xDgNnSc1
nastavení	nastavení	k1gNnSc1
nezměnilo	změnit	k5eNaPmAgNnS
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
ozubené	ozubený	k2eAgNnSc1d1
soukolí	soukolí	k1gNnSc1
splňuje	splňovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Protože	protože	k8xS
se	se	k3xPyFc4
rozvod	rozvod	k1gInSc1
OHV	OHV	kA
používá	používat	k5eAaImIp3nS
u	u	k7c2
čtyřdobých	čtyřdobý	k2eAgInPc2d1
motorů	motor	k1gInPc2
<g/>
,	,	kIx,
převodový	převodový	k2eAgInSc1d1
poměr	poměr	k1gInSc1
pohonu	pohon	k1gInSc2
je	být	k5eAaImIp3nS
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vačkový	vačkový	k2eAgInSc1d1
hřídel	hřídel	k1gInSc1
se	se	k3xPyFc4
otáčí	otáčet	k5eAaImIp3nS
2	#num#	k4
<g/>
×	×	k?
pomaleji	pomale	k6eAd2
než	než	k8xS
klikový	klikový	k2eAgInSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
čtyřdobý	čtyřdobý	k2eAgInSc1d1
motor	motor	k1gInSc1
vykoná	vykonat	k5eAaPmIp3nS
jeden	jeden	k4xCgInSc1
pracovní	pracovní	k2eAgInSc1d1
cyklus	cyklus	k1gInSc1
za	za	k7c4
dvě	dva	k4xCgFnPc4
otáčky	otáčka	k1gFnPc4
klikového	klikový	k2eAgInSc2d1
hřídele	hřídel	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Automobil	automobil	k1gInSc1
</s>
