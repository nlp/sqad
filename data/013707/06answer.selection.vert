<s>
OHV	OHV	kA
(	(	kIx(
<g/>
Over	Over	k1gFnSc1
Head	Head	k1gFnSc1
Valve	Valev	k1gFnSc1
-	-	kIx~
v	v	k7c6
USA	USA	kA
místy	místy	k6eAd1
nazývaný	nazývaný	k2eAgInSc1d1
také	také	k9
jako	jako	k9
"	"	kIx"
<g/>
Pushrod	Pushrod	k1gInSc1
<g/>
"	"	kIx"
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
typ	typ	k1gInSc1
ventilového	ventilový	k2eAgInSc2d1
rozvodu	rozvod	k1gInSc2
pístového	pístový	k2eAgInSc2d1
motoru	motor	k1gInSc2
<g/>
,	,	kIx,
u	u	k7c2
kterého	který	k3yRgInSc2
jsou	být	k5eAaImIp3nP
ventily	ventil	k1gInPc1
umístěny	umístěn	k2eAgInPc1d1
v	v	k7c6
hlavě	hlava	k1gFnSc6
válců	válec	k1gInPc2
a	a	k8xC
vačkový	vačkový	k2eAgInSc1d1
hřídel	hřídel	k1gInSc1
je	být	k5eAaImIp3nS
umístěn	umístit	k5eAaPmNgInS
v	v	k7c6
bloku	blok	k1gInSc6
motoru	motor	k1gInSc2
<g/>
.	.	kIx.
</s>