<s desamb="1">
Je	být	k5eAaImIp3nS
také	také	k9
autorem	autor	k1gMnSc7
obalu	obal	k1gInSc2
alba	album	k1gNnSc2
Off	Off	k1gFnSc1
the	the	k?
Wall	Wall	k1gInSc1
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
zpěváka	zpěvák	k1gMnSc2
Michaela	Michael	k1gMnSc2
Jacksona	Jackson	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Dále	daleko	k6eAd2
také	také	k9
pracoval	pracovat	k5eAaImAgInS
na	na	k7c6
různých	různý	k2eAgInPc6d1
filmových	filmový	k2eAgInPc6d1
projektech	projekt	k1gInPc6
(	(	kIx(
<g/>
například	například	k6eAd1
Dobyvatelé	dobyvatel	k1gMnPc1
ztracené	ztracený	k2eAgFnSc2d1
archy	archa	k1gFnSc2
a	a	k8xC
Jurský	jurský	k2eAgInSc1d1
park	park	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
reklamách	reklama	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Rovněž	rovněž	k9
se	se	k3xPyFc4
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
vyučování	vyučování	k1gNnSc3
designu	design	k1gInSc2
<g/>
,	,	kIx,
reklamy	reklama	k1gFnPc1
<g/>
,	,	kIx,
ilustrace	ilustrace	k1gFnPc1
a	a	k8xC
fotografie	fotografia	k1gFnPc1
na	na	k7c6
UCLA	UCLA	kA
a	a	k8xC
Otis	Otis	k1gInSc6
Art	Art	k1gInSc6
Institute	institut	k1gInSc6
<g/>
.	.	kIx.
</s>