<s>
Mike	Mike	k1gFnSc1
Salisbury	Salisbura	k1gFnSc2
(	(	kIx(
<g/>
designér	designér	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Mike	Mike	k1gNnSc1
Salisbury	Salisbura	k1gFnSc2
Narození	narození	k1gNnSc2
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
stoletíUtah	stoletíUtah	k1gInSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
designér	designér	k1gMnSc1
a	a	k8xC
fotograf	fotograf	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Tomuto	tento	k3xDgInSc3
článku	článek	k1gInSc3
chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
obrázky	obrázek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Víte	vědět	k5eAaImIp2nP
<g/>
-li	-li	k?
o	o	k7c6
nějakých	nějaký	k3yIgFnPc6
svobodně	svobodně	k6eAd1
šiřitelných	šiřitelný	k2eAgFnPc6d1
<g/>
,	,	kIx,
neváhejte	váhat	k5eNaImRp2nP
je	on	k3xPp3gMnPc4
načíst	načíst	k5eAaPmF,k5eAaBmF
a	a	k8xC
přidat	přidat	k5eAaPmF
do	do	k7c2
článku	článek	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
rychlejší	rychlý	k2eAgNnSc4d2
přidání	přidání	k1gNnSc4
obrázku	obrázek	k1gInSc2
můžete	moct	k5eAaImIp2nP
přidat	přidat	k5eAaPmF
žádost	žádost	k1gFnSc4
i	i	k9
sem	sem	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
WikiProjekt	WikiProjekt	k1gInSc1
Fotografování	fotografování	k1gNnSc2
</s>
<s>
Mike	Mike	k1gFnSc1
Salisbury	Salisbura	k1gFnSc2
je	být	k5eAaImIp3nS
americký	americký	k2eAgMnSc1d1
designér	designér	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
a	a	k8xC
dílo	dílo	k1gNnSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
v	v	k7c6
Utahu	Utah	k1gInSc6
a	a	k8xC
v	v	k7c6
dětství	dětství	k1gNnSc6
žil	žíla	k1gFnPc2
ještě	ještě	k9
například	například	k6eAd1
v	v	k7c6
Kalifornii	Kalifornie	k1gFnSc6
či	či	k8xC
na	na	k7c6
Havaji	Havaj	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
autorem	autor	k1gMnSc7
obalů	obal	k1gInPc2
hudebních	hudební	k2eAgNnPc2d1
alb	album	k1gNnPc2
mnoha	mnoho	k4c2
interpretů	interpret	k1gMnPc2
<g/>
,	,	kIx,
mezi	mezi	k7c4
něž	jenž	k3xRgFnPc4
patří	patřit	k5eAaImIp3nS
například	například	k6eAd1
George	George	k1gFnSc1
Harrison	Harrison	k1gMnSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
Cale	Cal	k1gInSc2
<g/>
,	,	kIx,
Randy	rand	k1gInPc1
Newman	Newman	k1gMnSc1
<g/>
,	,	kIx,
Ry	Ry	k1gMnSc1
Cooder	Cooder	k1gMnSc1
a	a	k8xC
Rickie	Rickie	k1gFnSc1
Lee	Lea	k1gFnSc3
Jones	Jones	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
také	také	k9
autorem	autor	k1gMnSc7
obalu	obal	k1gInSc2
alba	album	k1gNnSc2
Off	Off	k1gFnSc1
the	the	k?
Wall	Wall	k1gInSc1
(	(	kIx(
<g/>
1979	#num#	k4
<g/>
)	)	kIx)
zpěváka	zpěvák	k1gMnSc2
Michaela	Michael	k1gMnSc2
Jacksona	Jackson	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Dále	daleko	k6eAd2
také	také	k9
pracoval	pracovat	k5eAaImAgInS
na	na	k7c6
různých	různý	k2eAgInPc6d1
filmových	filmový	k2eAgInPc6d1
projektech	projekt	k1gInPc6
(	(	kIx(
<g/>
například	například	k6eAd1
Dobyvatelé	dobyvatel	k1gMnPc1
ztracené	ztracený	k2eAgFnSc2d1
archy	archa	k1gFnSc2
a	a	k8xC
Jurský	jurský	k2eAgInSc1d1
park	park	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
reklamách	reklama	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Rovněž	rovněž	k9
se	se	k3xPyFc4
věnoval	věnovat	k5eAaImAgMnS,k5eAaPmAgMnS
vyučování	vyučování	k1gNnSc3
designu	design	k1gInSc2
<g/>
,	,	kIx,
reklamy	reklama	k1gFnPc1
<g/>
,	,	kIx,
ilustrace	ilustrace	k1gFnPc1
a	a	k8xC
fotografie	fotografia	k1gFnPc1
na	na	k7c6
UCLA	UCLA	kA
a	a	k8xC
Otis	Otis	k1gInSc6
Art	Art	k1gInSc6
Institute	institut	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
BROWER	BROWER	kA
<g/>
,	,	kIx,
Steven	Steven	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mike	Mike	k1gNnSc7
Salisbury	Salisbura	k1gFnSc2
<g/>
,	,	kIx,
the	the	k?
Biggest	Biggest	k1gInSc1
Design	design	k1gInSc1
Multihyphenate	Multihyphenat	k1gInSc5
You	You	k1gFnPc1
Don	Don	k1gMnSc1
<g/>
’	’	k?
<g/>
t	t	k?
Know	Know	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
AIGA	AIGA	kA
<g/>
,	,	kIx,
2015-12-24	2015-12-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
DOOLEY	DOOLEY	kA
<g/>
,	,	kIx,
Michael	Michael	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
How	How	k1gFnPc2
Branding	Branding	k1gInSc1
Launched	Launched	k1gMnSc1
Michael	Michael	k1gMnSc1
Jackson	Jackson	k1gMnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Solo	Solo	k1gMnSc1
Career	Career	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Print	Printa	k1gFnPc2
<g/>
,	,	kIx,
2014-11-14	2014-11-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Success	Success	k1gInSc1
Ideas	Ideas	k1gMnSc1
from	from	k1gMnSc1
Delebrated	Delebrated	k1gMnSc1
Designer	Designer	k1gMnSc1
<g/>
,	,	kIx,
Mike	Mike	k1gFnSc1
Salisbury	Salisbura	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gFnPc2
Sherwood	Sherwooda	k1gFnPc2
Group	Group	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Mike	Mike	k1gNnSc7
Salisbury	Salisbura	k1gFnSc2
na	na	k7c4
Discogs	Discogs	k1gInSc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Umění	umění	k1gNnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
|	|	kIx~
Fotografie	fotografie	k1gFnSc1
|	|	kIx~
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
