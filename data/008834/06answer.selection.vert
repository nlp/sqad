<s>
Zbraslavský	zbraslavský	k2eAgInSc1d1	zbraslavský
klášter	klášter	k1gInSc1	klášter
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Aula	aula	k1gFnSc1	aula
Regia	Regia	k1gFnSc1	Regia
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
cisterciácký	cisterciácký	k2eAgInSc1d1	cisterciácký
klášter	klášter	k1gInSc1	klášter
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
pražské	pražský	k2eAgFnSc2d1	Pražská
čtvrti	čtvrt	k1gFnSc2	čtvrt
Zbraslav	Zbraslav	k1gFnSc1	Zbraslav
založený	založený	k2eAgInSc4d1	založený
králem	král	k1gMnSc7	král
Václavem	Václav	k1gMnSc7	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
nedaleko	nedaleko	k7c2	nedaleko
soutoku	soutok	k1gInSc2	soutok
řek	řeka	k1gFnPc2	řeka
Vltavy	Vltava	k1gFnSc2	Vltava
a	a	k8xC	a
Mže	Mže	k1gFnSc2	Mže
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Berounky	Berounka	k1gFnSc2	Berounka
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
jejího	její	k3xOp3gNnSc2	její
mrtvého	mrtvý	k2eAgNnSc2d1	mrtvé
ramene	rameno	k1gNnSc2	rameno
zvaného	zvaný	k2eAgMnSc4d1	zvaný
Krňák	Krňák	k1gMnSc1	Krňák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
