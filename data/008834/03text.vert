<p>
<s>
Zbraslavský	zbraslavský	k2eAgInSc1d1	zbraslavský
klášter	klášter	k1gInSc1	klášter
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
Aula	aula	k1gFnSc1	aula
Regia	Regia	k1gFnSc1	Regia
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
cisterciácký	cisterciácký	k2eAgInSc1d1	cisterciácký
klášter	klášter	k1gInSc1	klášter
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
pražské	pražský	k2eAgFnSc2d1	Pražská
čtvrti	čtvrt	k1gFnSc2	čtvrt
Zbraslav	Zbraslav	k1gFnSc1	Zbraslav
založený	založený	k2eAgInSc4d1	založený
králem	král	k1gMnSc7	král
Václavem	Václav	k1gMnSc7	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
nedaleko	nedaleko	k7c2	nedaleko
soutoku	soutok	k1gInSc2	soutok
řek	řeka	k1gFnPc2	řeka
Vltavy	Vltava	k1gFnSc2	Vltava
a	a	k8xC	a
Mže	Mže	k1gFnSc2	Mže
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Berounky	Berounka	k1gFnSc2	Berounka
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
jejího	její	k3xOp3gNnSc2	její
mrtvého	mrtvý	k2eAgNnSc2d1	mrtvé
ramene	rameno	k1gNnSc2	rameno
zvaného	zvaný	k2eAgMnSc4d1	zvaný
Krňák	Krňák	k1gMnSc1	Krňák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
gotický	gotický	k2eAgInSc1d1	gotický
konvent	konvent	k1gInSc1	konvent
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gFnSc6	jehož
půdě	půda	k1gFnSc6	půda
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
Zbraslavská	zbraslavský	k2eAgFnSc1d1	Zbraslavská
kronika	kronika	k1gFnSc1	kronika
<g/>
,	,	kIx,	,
časem	časem	k6eAd1	časem
prošel	projít	k5eAaPmAgInS	projít
mnoha	mnoho	k4c7	mnoho
proměnami	proměna	k1gFnPc7	proměna
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
velkolepého	velkolepý	k2eAgNnSc2d1	velkolepé
cisterciáckého	cisterciácký	k2eAgNnSc2d1	cisterciácké
opatství	opatství	k1gNnSc2	opatství
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
opati	opat	k1gMnPc1	opat
měli	mít	k5eAaImAgMnP	mít
významný	významný	k2eAgInSc4d1	významný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
politiku	politika	k1gFnSc4	politika
Českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
změnou	změna	k1gFnSc7	změna
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
dynastie	dynastie	k1gFnSc2	dynastie
a	a	k8xC	a
přesunem	přesun	k1gInSc7	přesun
královského	královský	k2eAgNnSc2d1	královské
pohřebiště	pohřebiště	k1gNnSc2	pohřebiště
do	do	k7c2	do
katedrály	katedrála	k1gFnSc2	katedrála
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc4	Vít
stal	stát	k5eAaPmAgMnS	stát
klášterem	klášter	k1gInSc7	klášter
druhořadého	druhořadý	k2eAgInSc2d1	druhořadý
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc4d1	velká
škody	škoda	k1gFnPc4	škoda
klášter	klášter	k1gInSc1	klášter
utrpěl	utrpět	k5eAaPmAgInS	utrpět
během	během	k7c2	během
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
nebo	nebo	k8xC	nebo
při	při	k7c6	při
útoku	útok	k1gInSc6	útok
švédských	švédský	k2eAgNnPc2d1	švédské
vojsk	vojsko	k1gNnPc2	vojsko
za	za	k7c2	za
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Barokní	barokní	k2eAgFnSc4d1	barokní
přestavbu	přestavba	k1gFnSc4	přestavba
zdevastovaného	zdevastovaný	k2eAgInSc2d1	zdevastovaný
klášterního	klášterní	k2eAgInSc2d1	klášterní
areálu	areál	k1gInSc2	areál
zastavil	zastavit	k5eAaPmAgMnS	zastavit
roku	rok	k1gInSc2	rok
1785	[number]	k4	1785
císařský	císařský	k2eAgInSc4d1	císařský
dekret	dekret	k1gInSc4	dekret
Josefa	Josef	k1gMnSc2	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
o	o	k7c6	o
rušení	rušení	k1gNnSc6	rušení
klášterů	klášter	k1gInPc2	klášter
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
se	se	k3xPyFc4	se
nezabývaly	zabývat	k5eNaImAgInP	zabývat
vyučováním	vyučování	k1gNnSc7	vyučování
<g/>
,	,	kIx,	,
pěstováním	pěstování	k1gNnSc7	pěstování
věd	věda	k1gFnPc2	věda
či	či	k8xC	či
péčí	péče	k1gFnPc2	péče
o	o	k7c4	o
nemocné	nemocný	k1gMnPc4	nemocný
<g/>
.	.	kIx.	.
</s>
<s>
Zbraslavský	zbraslavský	k2eAgInSc1d1	zbraslavský
klášter	klášter	k1gInSc1	klášter
byl	být	k5eAaImAgInS	být
společně	společně	k6eAd1	společně
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
osmi	osm	k4xCc7	osm
českými	český	k2eAgInPc7d1	český
a	a	k8xC	a
moravskými	moravský	k2eAgInPc7d1	moravský
cisterciáckými	cisterciácký	k2eAgInPc7d1	cisterciácký
kláštery	klášter	k1gInPc7	klášter
zrušen	zrušit	k5eAaPmNgInS	zrušit
a	a	k8xC	a
všechny	všechen	k3xTgFnPc1	všechen
nemovitosti	nemovitost	k1gFnPc1	nemovitost
přešly	přejít	k5eAaPmAgFnP	přejít
do	do	k7c2	do
majetku	majetek	k1gInSc2	majetek
náboženského	náboženský	k2eAgInSc2d1	náboženský
fondu	fond	k1gInSc2	fond
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
poté	poté	k6eAd1	poté
rozprodal	rozprodat	k5eAaPmAgMnS	rozprodat
<g/>
.	.	kIx.	.
</s>
<s>
Klášterní	klášterní	k2eAgFnPc1d1	klášterní
budovy	budova	k1gFnPc1	budova
byly	být	k5eAaImAgFnP	být
využívány	využívat	k5eAaImNgFnP	využívat
jako	jako	k8xS	jako
cukrovar	cukrovar	k1gInSc1	cukrovar
<g/>
,	,	kIx,	,
skladiště	skladiště	k1gNnSc1	skladiště
obilí	obilí	k1gNnSc1	obilí
<g/>
,	,	kIx,	,
výrobna	výrobna	k1gFnSc1	výrobna
chemikálií	chemikálie	k1gFnPc2	chemikálie
a	a	k8xC	a
pivovar	pivovar	k1gInSc1	pivovar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgMnS	být
klášter	klášter	k1gInSc4	klášter
díky	díky	k7c3	díky
Cyrilu	Cyril	k1gMnSc3	Cyril
Bartoňovi	Bartoň	k1gMnSc3	Bartoň
z	z	k7c2	z
Dobenína	Dobenín	k1gInSc2	Dobenín
zrekonstruován	zrekonstruovat	k5eAaPmNgInS	zrekonstruovat
a	a	k8xC	a
přestavěn	přestavět	k5eAaPmNgInS	přestavět
na	na	k7c4	na
zámecké	zámecký	k2eAgNnSc4d1	zámecké
sídlo	sídlo	k1gNnSc4	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
fungoval	fungovat	k5eAaImAgInS	fungovat
jako	jako	k8xS	jako
výstavní	výstavní	k2eAgInSc4d1	výstavní
prostor	prostor	k1gInSc4	prostor
sbírky	sbírka	k1gFnSc2	sbírka
asijského	asijský	k2eAgNnSc2d1	asijské
umění	umění	k1gNnSc2	umění
Národní	národní	k2eAgFnSc2d1	národní
galerie	galerie	k1gFnSc2	galerie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
restituci	restituce	k1gFnSc6	restituce
byl	být	k5eAaImAgInS	být
navrácen	navrácen	k2eAgInSc1d1	navrácen
Bartoňům	Bartoň	k1gMnPc3	Bartoň
z	z	k7c2	z
Dobenína	Dobenín	k1gInSc2	Dobenín
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
byl	být	k5eAaImAgInS	být
zařazen	zařadit	k5eAaPmNgInS	zařadit
na	na	k7c4	na
Seznam	seznam	k1gInSc4	seznam
národních	národní	k2eAgFnPc2d1	národní
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
plánuje	plánovat	k5eAaImIp3nS	plánovat
zřídit	zřídit	k5eAaPmF	zřídit
depozitář	depozitář	k1gInSc4	depozitář
knihovny	knihovna	k1gFnSc2	knihovna
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
se	s	k7c7	s
studovnou	studovna	k1gFnSc7	studovna
pro	pro	k7c4	pro
studenty	student	k1gMnPc4	student
a	a	k8xC	a
badatele	badatel	k1gMnPc4	badatel
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
zámeckého	zámecký	k2eAgInSc2d1	zámecký
areálu	areál	k1gInSc2	areál
je	být	k5eAaImIp3nS	být
i	i	k9	i
barokní	barokní	k2eAgInSc1d1	barokní
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Jakuba	Jakub	k1gMnSc2	Jakub
Většího	veliký	k2eAgMnSc2d2	veliký
s	s	k7c7	s
deskovým	deskový	k2eAgInSc7d1	deskový
obrazem	obraz	k1gInSc7	obraz
Madony	Madona	k1gFnSc2	Madona
ze	z	k7c2	z
Zbraslavi	Zbraslav	k1gFnSc2	Zbraslav
<g/>
,	,	kIx,	,
obrazy	obraz	k1gInPc1	obraz
Karla	Karel	k1gMnSc2	Karel
Škréty	Škréta	k1gMnSc2	Škréta
a	a	k8xC	a
Petra	Petr	k1gMnSc2	Petr
Brandla	Brandla	k1gFnSc2	Brandla
nebo	nebo	k8xC	nebo
ostatky	ostatek	k1gInPc1	ostatek
posledních	poslední	k2eAgMnPc2d1	poslední
přemyslovských	přemyslovský	k2eAgMnPc2d1	přemyslovský
králů	král	k1gMnPc2	král
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
pravidelných	pravidelný	k2eAgFnPc2d1	pravidelná
bohoslužeb	bohoslužba	k1gFnPc2	bohoslužba
běžně	běžně	k6eAd1	běžně
přístupný	přístupný	k2eAgInSc1d1	přístupný
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Cisterciáci	cisterciák	k1gMnPc5	cisterciák
==	==	k?	==
</s>
</p>
<p>
<s>
Cisterciácký	cisterciácký	k2eAgInSc1d1	cisterciácký
řád	řád	k1gInSc1	řád
hlásající	hlásající	k2eAgFnSc4d1	hlásající
askezi	askeze	k1gFnSc4	askeze
a	a	k8xC	a
manuální	manuální	k2eAgFnSc4d1	manuální
práci	práce	k1gFnSc4	práce
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
jako	jako	k9	jako
odpověď	odpověď	k1gFnSc4	odpověď
na	na	k7c4	na
rozmařilý	rozmařilý	k2eAgInSc4d1	rozmařilý
způsob	způsob	k1gInSc4	způsob
klášterního	klášterní	k2eAgInSc2d1	klášterní
života	život	k1gInSc2	život
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
11	[number]	k4	11
a	a	k8xC	a
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
neodpovídal	odpovídat	k5eNaImAgInS	odpovídat
původním	původní	k2eAgInSc7d1	původní
ideálům	ideál	k1gInPc3	ideál
mnišství	mnišství	k1gNnSc2	mnišství
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1098	[number]	k4	1098
odešlo	odejít	k5eAaPmAgNnS	odejít
několik	několik	k4yIc1	několik
zbožných	zbožný	k2eAgMnPc2d1	zbožný
mnichů	mnich	k1gMnPc2	mnich
z	z	k7c2	z
burgundského	burgundský	k2eAgInSc2d1	burgundský
benediktinského	benediktinský	k2eAgInSc2d1	benediktinský
kláštera	klášter	k1gInSc2	klášter
Molesme	Molesme	k1gFnSc2	Molesme
a	a	k8xC	a
na	na	k7c6	na
odlehlém	odlehlý	k2eAgInSc6d1	odlehlý
a	a	k8xC	a
opuštěném	opuštěný	k2eAgNnSc6d1	opuštěné
místě	místo	k1gNnSc6	místo
při	při	k7c6	při
staré	starý	k2eAgFnSc6d1	stará
římské	římský	k2eAgFnSc6d1	římská
silnici	silnice	k1gFnSc6	silnice
založili	založit	k5eAaPmAgMnP	založit
nový	nový	k2eAgInSc4d1	nový
klášter	klášter	k1gInSc4	klášter
podporující	podporující	k2eAgFnSc4d1	podporující
reformu	reforma	k1gFnSc4	reforma
mnišského	mnišský	k2eAgInSc2d1	mnišský
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jménem	jméno	k1gNnSc7	jméno
nového	nový	k2eAgInSc2d1	nový
kláštera	klášter	k1gInSc2	klášter
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
latinsky	latinsky	k6eAd1	latinsky
nazván	nazván	k2eAgInSc1d1	nazván
Cistercium	Cistercium	k1gNnSc4	Cistercium
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
poté	poté	k6eAd1	poté
označován	označovat	k5eAaImNgInS	označovat
celý	celý	k2eAgInSc1d1	celý
nový	nový	k2eAgInSc1d1	nový
řád	řád	k1gInSc1	řád
vycházející	vycházející	k2eAgInSc1d1	vycházející
z	z	k7c2	z
původní	původní	k2eAgFnSc2d1	původní
řehole	řehole	k1gFnSc2	řehole
sv.	sv.	kA	sv.
Benedikta	Benedikt	k1gMnSc2	Benedikt
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc7	jehož
heslem	heslo	k1gNnSc7	heslo
bylo	být	k5eAaImAgNnS	být
Ora	Ora	k1gFnPc4	Ora
et	et	k?	et
labora	labora	k1gFnSc1	labora
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
Modli	modlit	k5eAaImRp2nS	modlit
se	se	k3xPyFc4	se
a	a	k8xC	a
pracuj	pracovat	k5eAaImRp2nS	pracovat
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cisterciácký	cisterciácký	k2eAgInSc1d1	cisterciácký
řád	řád	k1gInSc1	řád
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
kladl	klást	k5eAaImAgInS	klást
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
intenzivní	intenzivní	k2eAgInSc4d1	intenzivní
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
Bohu	bůh	k1gMnSc3	bůh
a	a	k8xC	a
hlubokou	hluboký	k2eAgFnSc4d1	hluboká
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
zbožnost	zbožnost	k1gFnSc4	zbožnost
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
papežem	papež	k1gMnSc7	papež
uznán	uznán	k2eAgInSc1d1	uznán
roku	rok	k1gInSc2	rok
1119	[number]	k4	1119
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
šířit	šířit	k5eAaImF	šířit
i	i	k9	i
do	do	k7c2	do
nejodlehlejších	odlehlý	k2eAgFnPc2d3	nejodlehlejší
končin	končina	k1gFnPc2	končina
západokřesťanského	západokřesťanský	k2eAgInSc2d1	západokřesťanský
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Kláštery	klášter	k1gInPc1	klášter
nového	nový	k2eAgInSc2d1	nový
řádu	řád	k1gInSc2	řád
byly	být	k5eAaImAgFnP	být
zasvěceny	zasvěcen	k2eAgFnPc1d1	zasvěcena
Panně	Panna	k1gFnSc3	Panna
Marii	Maria	k1gFnSc3	Maria
a	a	k8xC	a
díky	díky	k7c3	díky
pevným	pevný	k2eAgFnPc3d1	pevná
filiačním	filiační	k2eAgFnPc3d1	filiační
vazbám	vazba	k1gFnPc3	vazba
dodržovaly	dodržovat	k5eAaImAgInP	dodržovat
stejné	stejný	k2eAgInPc4d1	stejný
zvyky	zvyk	k1gInPc4	zvyk
a	a	k8xC	a
předpisy	předpis	k1gInPc4	předpis
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
barvy	barva	k1gFnSc2	barva
hábitu	hábit	k1gInSc2	hábit
se	se	k3xPyFc4	se
cisterciákům	cisterciák	k1gMnPc3	cisterciák
říkalo	říkat	k5eAaImAgNnS	říkat
také	také	k9	také
bílí	bílý	k2eAgMnPc1d1	bílý
či	či	k8xC	či
šedí	šedý	k2eAgMnPc1d1	šedý
mniši	mnich	k1gMnPc1	mnich
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
zpočátku	zpočátku	k6eAd1	zpočátku
prosté	prostý	k2eAgFnSc6d1	prostá
reformní	reformní	k2eAgFnSc6d1	reformní
řeholi	řehole	k1gFnSc6	řehole
postupně	postupně	k6eAd1	postupně
našlo	najít	k5eAaPmAgNnS	najít
zalíbení	zalíbení	k1gNnSc4	zalíbení
mnoho	mnoho	k4c1	mnoho
movitých	movitý	k2eAgMnPc2d1	movitý
mecenášů	mecenáš	k1gMnPc2	mecenáš
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
zbožné	zbožný	k2eAgInPc1d1	zbožný
činy	čin	k1gInPc1	čin
byly	být	k5eAaImAgInP	být
motivovány	motivovat	k5eAaBmNgInP	motivovat
převážně	převážně	k6eAd1	převážně
starostí	starost	k1gFnPc2	starost
o	o	k7c4	o
spásu	spása	k1gFnSc4	spása
vlastní	vlastní	k2eAgFnSc2d1	vlastní
duše	duše	k1gFnSc2	duše
či	či	k8xC	či
duší	duše	k1gFnPc2	duše
svých	svůj	k3xOyFgInPc2	svůj
předků	předek	k1gInPc2	předek
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
mělo	mít	k5eAaImAgNnS	mít
zajistit	zajistit	k5eAaPmF	zajistit
obdarování	obdarování	k1gNnSc1	obdarování
kláštera	klášter	k1gInSc2	klášter
či	či	k8xC	či
pohřeb	pohřeb	k1gInSc4	pohřeb
na	na	k7c6	na
klášterní	klášterní	k2eAgFnSc6d1	klášterní
půdě	půda	k1gFnSc6	půda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgFnP	být
pravidelně	pravidelně	k6eAd1	pravidelně
slouženy	sloužen	k2eAgFnPc1d1	sloužena
mše	mše	k1gFnPc1	mše
a	a	k8xC	a
konány	konán	k2eAgFnPc1d1	konána
modlitby	modlitba	k1gFnPc1	modlitba
řeholníků	řeholník	k1gMnPc2	řeholník
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
se	se	k3xPyFc4	se
cisterciácké	cisterciácký	k2eAgInPc1d1	cisterciácký
kláštery	klášter	k1gInPc1	klášter
díky	díky	k7c3	díky
vlastní	vlastní	k2eAgFnSc3d1	vlastní
práci	práce	k1gFnSc3	práce
i	i	k8xC	i
podpoře	podpora	k1gFnSc3	podpora
světských	světský	k2eAgMnPc2d1	světský
fundátorů	fundátor	k1gMnPc2	fundátor
vypracovaly	vypracovat	k5eAaPmAgFnP	vypracovat
mezi	mezi	k7c4	mezi
nejbohatší	bohatý	k2eAgFnPc4d3	nejbohatší
středověké	středověký	k2eAgFnPc4d1	středověká
instituce	instituce	k1gFnPc4	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vladařů	vladař	k1gMnPc2	vladař
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgInPc7	jenž
svou	svůj	k3xOyFgFnSc7	svůj
náklonností	náklonnost	k1gFnSc7	náklonnost
k	k	k7c3	k
řádu	řád	k1gInSc3	řád
vynikal	vynikat	k5eAaImAgMnS	vynikat
především	především	k9	především
francouzský	francouzský	k2eAgMnSc1d1	francouzský
král	král	k1gMnSc1	král
Ludvík	Ludvík	k1gMnSc1	Ludvík
IX	IX	kA	IX
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
získali	získat	k5eAaPmAgMnP	získat
cisterciáci	cisterciák	k1gMnPc1	cisterciák
také	také	k9	také
sympatie	sympatie	k1gFnSc2	sympatie
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
měl	mít	k5eAaImAgInS	mít
prý	prý	k9	prý
"	"	kIx"	"
<g/>
rád	rád	k6eAd1	rád
zvláště	zvláště	k6eAd1	zvláště
a	a	k8xC	a
jmenovitě	jmenovitě	k6eAd1	jmenovitě
řád	řád	k1gInSc1	řád
cisterciácký	cisterciácký	k2eAgInSc1d1	cisterciácký
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
rozkvět	rozkvět	k1gInSc1	rozkvět
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
přišli	přijít	k5eAaPmAgMnP	přijít
bílí	bílý	k2eAgMnPc1d1	bílý
mniši	mnich	k1gMnPc1	mnich
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
příchod	příchod	k1gInSc1	příchod
byl	být	k5eAaImAgInS	být
zřejmě	zřejmě	k6eAd1	zřejmě
iniciován	iniciovat	k5eAaBmNgInS	iniciovat
olomouckým	olomoucký	k2eAgMnSc7d1	olomoucký
biskupem	biskup	k1gMnSc7	biskup
Jindřichem	Jindřich	k1gMnSc7	Jindřich
Zdíkem	Zdík	k1gMnSc7	Zdík
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
cisterciáckou	cisterciácký	k2eAgFnSc7d1	cisterciácká
fundací	fundace	k1gFnSc7	fundace
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
klášter	klášter	k1gInSc1	klášter
Sedlec	Sedlec	k1gInSc1	Sedlec
založený	založený	k2eAgInSc1d1	založený
roku	rok	k1gInSc2	rok
1142	[number]	k4	1142
šlechticem	šlechtic	k1gMnSc7	šlechtic
Miroslavem	Miroslav	k1gMnSc7	Miroslav
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
středověku	středověk	k1gInSc2	středověk
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
království	království	k1gNnSc6	království
založeno	založit	k5eAaPmNgNnS	založit
dalších	další	k2eAgInPc2d1	další
sedmnáct	sedmnáct	k4xCc4	sedmnáct
klášterů	klášter	k1gInPc2	klášter
cisterciáckého	cisterciácký	k2eAgInSc2d1	cisterciácký
řádu	řád	k1gInSc2	řád
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgMnPc7	jenž
byl	být	k5eAaImAgMnS	být
i	i	k9	i
klášter	klášter	k1gInSc4	klášter
na	na	k7c6	na
Zbraslavi	Zbraslav	k1gFnSc6	Zbraslav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
Zbraslavi	Zbraslav	k1gFnSc3	Zbraslav
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
pověst	pověst	k1gFnSc1	pověst
o	o	k7c6	o
potomkovi	potomek	k1gMnSc6	potomek
Bivoje	Bivoj	k1gMnSc2	Bivoj
a	a	k8xC	a
Kazi	Kazi	k1gFnSc2	Kazi
jménem	jméno	k1gNnSc7	jméno
Zbraslav	Zbraslav	k1gFnSc1	Zbraslav
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
založil	založit	k5eAaPmAgInS	založit
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
zbraslavské	zbraslavský	k2eAgFnSc2d1	Zbraslavská
lokality	lokalita	k1gFnSc2	lokalita
dvůr	dvůr	k1gInSc1	dvůr
a	a	k8xC	a
na	na	k7c6	na
smrtelném	smrtelný	k2eAgNnSc6d1	smrtelné
loži	lože	k1gNnSc6	lože
jej	on	k3xPp3gMnSc4	on
odkázal	odkázat	k5eAaPmAgInS	odkázat
benediktinskému	benediktinský	k2eAgInSc3d1	benediktinský
řádu	řád	k1gInSc3	řád
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Zbraslavi	Zbraslav	k1gFnSc6	Zbraslav
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1115	[number]	k4	1115
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
knížetem	kníže	k1gNnSc7wR	kníže
Vladislavem	Vladislav	k1gMnSc7	Vladislav
I.	I.	kA	I.
darována	darovat	k5eAaPmNgFnS	darovat
kladrubskému	kladrubský	k2eAgInSc3d1	kladrubský
klášteru	klášter	k1gInSc3	klášter
<g/>
,	,	kIx,	,
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
zde	zde	k6eAd1	zde
zřídil	zřídit	k5eAaPmAgMnS	zřídit
proboštství	proboštství	k1gNnSc4	proboštství
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
trvale	trvale	k6eAd1	trvale
žili	žít	k5eAaImAgMnP	žít
dva	dva	k4xCgMnPc1	dva
nebo	nebo	k8xC	nebo
tři	tři	k4xCgMnPc1	tři
mniši	mnich	k1gMnPc1	mnich
a	a	k8xC	a
občas	občas	k6eAd1	občas
zde	zde	k6eAd1	zde
pobýval	pobývat	k5eAaImAgMnS	pobývat
i	i	k9	i
kladrubský	kladrubský	k2eAgMnSc1d1	kladrubský
opat	opat	k1gMnSc1	opat
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
krátce	krátce	k6eAd1	krátce
patřila	patřit	k5eAaImAgFnS	patřit
do	do	k7c2	do
majetku	majetek	k1gInSc2	majetek
pražského	pražský	k2eAgMnSc2d1	pražský
biskupa	biskup	k1gMnSc2	biskup
Jana	Jan	k1gMnSc2	Jan
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1268	[number]	k4	1268
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
touze	touha	k1gFnSc6	touha
po	po	k7c6	po
místě	místo	k1gNnSc6	místo
vzdáleném	vzdálený	k2eAgNnSc6d1	vzdálené
kousek	kousek	k1gInSc1	kousek
od	od	k7c2	od
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
lovit	lovit	k5eAaImF	lovit
<g/>
,	,	kIx,	,
bavit	bavit	k5eAaImF	bavit
se	se	k3xPyFc4	se
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
být	být	k5eAaImF	být
po	po	k7c6	po
ruce	ruka	k1gFnSc6	ruka
<g/>
,	,	kIx,	,
provedl	provést	k5eAaPmAgInS	provést
s	s	k7c7	s
biskupem	biskup	k1gInSc7	biskup
směnu	směna	k1gFnSc4	směna
za	za	k7c4	za
Butovice	Butovice	k1gFnPc4	Butovice
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
královské	královský	k2eAgInPc4d1	královský
statky	statek	k1gInPc4	statek
a	a	k8xC	a
"	"	kIx"	"
<g/>
rozkázal	rozkázat	k5eAaPmAgInS	rozkázat
tam	tam	k6eAd1	tam
vystavět	vystavět	k5eAaPmF	vystavět
lovecký	lovecký	k2eAgInSc4d1	lovecký
dvůr	dvůr	k1gInSc4	dvůr
s	s	k7c7	s
věžemi	věž	k1gFnPc7	věž
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
pevnými	pevný	k2eAgFnPc7d1	pevná
zdmi	zeď	k1gFnPc7	zeď
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1278	[number]	k4	1278
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Moravském	moravský	k2eAgNnSc6d1	Moravské
poli	pole	k1gNnSc6	pole
dvorec	dvorec	k1gInSc1	dvorec
zdědil	zdědit	k5eAaPmAgInS	zdědit
i	i	k9	i
s	s	k7c7	s
vinicemi	vinice	k1gFnPc7	vinice
osázenými	osázený	k2eAgFnPc7d1	osázená
révou	réva	k1gFnSc7	réva
z	z	k7c2	z
Rakous	Rakousy	k1gInPc2	Rakousy
Přemyslův	Přemyslův	k2eAgMnSc1d1	Přemyslův
syn	syn	k1gMnSc1	syn
Václav	Václav	k1gMnSc1	Václav
a	a	k8xC	a
také	také	k9	také
on	on	k3xPp3gMnSc1	on
po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
Braniborska	Braniborsko	k1gNnSc2	Braniborsko
"	"	kIx"	"
<g/>
velmi	velmi	k6eAd1	velmi
často	často	k6eAd1	často
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
na	na	k7c4	na
lov	lov	k1gInSc4	lov
svrchu	svrchu	k6eAd1	svrchu
řečené	řečený	k2eAgNnSc4d1	řečené
místo	místo	k1gNnSc4	místo
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
pány	pan	k1gMnPc7	pan
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
se	se	k3xPyFc4	se
jako	jako	k9	jako
nový	nový	k2eAgMnSc1d1	nový
majitel	majitel	k1gMnSc1	majitel
pro	pro	k7c4	pro
založení	založení	k1gNnSc4	založení
kláštera	klášter	k1gInSc2	klášter
"	"	kIx"	"
<g/>
ke	k	k7c3	k
cti	čest	k1gFnSc3	čest
Boha	bůh	k1gMnSc2	bůh
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
matky	matka	k1gFnSc2	matka
Marie	Maria	k1gFnSc2	Maria
<g/>
,	,	kIx,	,
blahoslavené	blahoslavený	k2eAgFnSc2d1	blahoslavená
Panny	Panna	k1gFnSc2	Panna
<g/>
"	"	kIx"	"
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
roku	rok	k1gInSc2	rok
1291	[number]	k4	1291
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
dle	dle	k7c2	dle
zpráv	zpráva	k1gFnPc2	zpráva
cisterciáckých	cisterciácký	k2eAgMnPc2d1	cisterciácký
letopisců	letopisec	k1gMnPc2	letopisec
údajně	údajně	k6eAd1	údajně
pln	pln	k2eAgInSc1d1	pln
výčitek	výčitek	k1gInSc1	výčitek
za	za	k7c4	za
popravu	poprava	k1gFnSc4	poprava
otčíma	otčím	k1gMnSc2	otčím
Záviše	Záviš	k1gMnSc2	Záviš
z	z	k7c2	z
Falkenštejna	Falkenštejn	k1gInSc2	Falkenštejn
rozmlouval	rozmlouvat	k5eAaImAgMnS	rozmlouvat
o	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
plánu	plán	k1gInSc6	plán
se	s	k7c7	s
sedleckým	sedlecký	k2eAgMnSc7d1	sedlecký
opatem	opat	k1gMnSc7	opat
Heidenreichem	Heidenreich	k1gMnSc7	Heidenreich
a	a	k8xC	a
nabídl	nabídnout	k5eAaPmAgInS	nabídnout
mu	on	k3xPp3gMnSc3	on
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
nové	nový	k2eAgFnSc2d1	nová
fundace	fundace	k1gFnSc2	fundace
ujal	ujmout	k5eAaPmAgMnS	ujmout
<g/>
.	.	kIx.	.
</s>
<s>
Cisterciáčtí	cisterciácký	k2eAgMnPc1d1	cisterciácký
opati	opat	k1gMnPc1	opat
poté	poté	k6eAd1	poté
čekali	čekat	k5eAaImAgMnP	čekat
na	na	k7c4	na
konečné	konečný	k2eAgNnSc4d1	konečné
královo	králův	k2eAgNnSc4d1	královo
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
až	až	k9	až
po	po	k7c6	po
třetím	třetí	k4xOgNnSc6	třetí
upomenutí	upomenutí	k1gNnSc6	upomenutí
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
společně	společně	k6eAd1	společně
s	s	k7c7	s
králem	král	k1gMnSc7	král
začít	začít	k5eAaPmF	začít
vybírat	vybírat	k5eAaImF	vybírat
lokality	lokalita	k1gFnPc4	lokalita
vhodné	vhodný	k2eAgFnPc1d1	vhodná
k	k	k7c3	k
založení	založení	k1gNnSc3	založení
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Vybrali	vybrat	k5eAaPmAgMnP	vybrat
si	se	k3xPyFc3	se
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
stál	stát	k5eAaImAgInS	stát
oblíbený	oblíbený	k2eAgInSc1d1	oblíbený
lovecký	lovecký	k2eAgInSc1d1	lovecký
hrádek	hrádek	k1gInSc1	hrádek
králova	králův	k2eAgMnSc2d1	králův
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
panovníka	panovník	k1gMnSc4	panovník
příliš	příliš	k6eAd1	příliš
nepotěšilo	potěšit	k5eNaPmAgNnS	potěšit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Inspirací	inspirace	k1gFnSc7	inspirace
při	při	k7c6	při
založení	založení	k1gNnSc6	založení
byl	být	k5eAaImAgMnS	být
snad	snad	k9	snad
Václavovi	Václav	k1gMnSc3	Václav
francouzský	francouzský	k2eAgInSc4d1	francouzský
klášter	klášter	k1gInSc4	klášter
Royaumont	Royaumonta	k1gFnPc2	Royaumonta
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
sv.	sv.	kA	sv.
Ludvíkem	Ludvík	k1gMnSc7	Ludvík
či	či	k8xC	či
pohřebiště	pohřebiště	k1gNnSc1	pohřebiště
francouzských	francouzský	k2eAgMnPc2d1	francouzský
králů	král	k1gMnPc2	král
v	v	k7c6	v
Saint-Denis	Saint-Denis	k1gFnSc6	Saint-Denis
<g/>
.	.	kIx.	.
</s>
<s>
Odpovídala	odpovídat	k5eAaImAgFnS	odpovídat
by	by	kYmCp3nS	by
tomu	ten	k3xDgNnSc3	ten
podoba	podoba	k1gFnSc1	podoba
konventního	konventní	k2eAgInSc2d1	konventní
chrámu	chrám	k1gInSc2	chrám
<g/>
,	,	kIx,	,
tolik	tolik	k6eAd1	tolik
vzdálená	vzdálený	k2eAgFnSc1d1	vzdálená
od	od	k7c2	od
původních	původní	k2eAgFnPc2d1	původní
střízlivých	střízlivý	k2eAgFnPc2d1	střízlivá
představ	představa	k1gFnPc2	představa
zakladatelů	zakladatel	k1gMnPc2	zakladatel
řádu	řád	k1gInSc2	řád
o	o	k7c6	o
vzhledu	vzhled	k1gInSc6	vzhled
božího	boží	k2eAgInSc2d1	boží
stánku	stánek	k1gInSc2	stánek
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
králův	králův	k2eAgInSc4d1	králův
úmysl	úmysl	k1gInSc4	úmysl
přesunout	přesunout	k5eAaPmF	přesunout
na	na	k7c4	na
Zbraslav	Zbraslav	k1gFnSc4	Zbraslav
funkci	funkce	k1gFnSc4	funkce
přemyslovské	přemyslovský	k2eAgFnSc2d1	Přemyslovská
nekropole	nekropole	k1gFnSc2	nekropole
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nový	nový	k2eAgInSc1d1	nový
dvanáctičlenný	dvanáctičlenný	k2eAgInSc1d1	dvanáctičlenný
konvent	konvent	k1gInSc1	konvent
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
budoucím	budoucí	k2eAgMnSc7d1	budoucí
opatem	opat	k1gMnSc7	opat
Konrádem	Konrád	k1gMnSc7	Konrád
přišel	přijít	k5eAaPmAgMnS	přijít
ze	z	k7c2	z
Sedlce	Sedlec	k1gInSc2	Sedlec
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1292	[number]	k4	1292
<g/>
.	.	kIx.	.
</s>
<s>
Ubytoval	ubytovat	k5eAaPmAgMnS	ubytovat
se	se	k3xPyFc4	se
prozatímně	prozatímně	k6eAd1	prozatímně
v	v	k7c6	v
královském	královský	k2eAgInSc6d1	královský
dvorci	dvorec	k1gInSc6	dvorec
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgNnSc3	jenž
patřila	patřit	k5eAaImAgFnS	patřit
kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Jakuba	Jakub	k1gMnSc2	Jakub
Většího	veliký	k2eAgMnSc2d2	veliký
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1305	[number]	k4	1305
konaly	konat	k5eAaImAgFnP	konat
bohoslužby	bohoslužba	k1gFnPc1	bohoslužba
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
10	[number]	k4	10
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1292	[number]	k4	1292
pochází	pocházet	k5eAaImIp3nS	pocházet
první	první	k4xOgFnSc1	první
zakládací	zakládací	k2eAgFnSc1d1	zakládací
listina	listina	k1gFnSc1	listina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
píše	psát	k5eAaImIp3nS	psát
o	o	k7c6	o
králově	králův	k2eAgFnSc6d1	králova
nejistotě	nejistota	k1gFnSc6	nejistota
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
svou	svůj	k3xOyFgFnSc7	svůj
vládou	vláda	k1gFnSc7	vláda
zalíbí	zalíbit	k5eAaPmIp3nS	zalíbit
Bohu	bůh	k1gMnSc3	bůh
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
naději	naděje	k1gFnSc4	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
dobrými	dobrý	k2eAgInPc7d1	dobrý
skutky	skutek	k1gInPc7	skutek
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
získat	získat	k5eAaPmF	získat
odpuštění	odpuštění	k1gNnSc4	odpuštění
již	již	k6eAd1	již
spáchaných	spáchaný	k2eAgInPc2d1	spáchaný
hříchů	hřích	k1gInPc2	hřích
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
ochranu	ochrana	k1gFnSc4	ochrana
před	před	k7c7	před
hříchy	hřích	k1gInPc7	hřích
budoucnosti	budoucnost	k1gFnSc2	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Píše	psát	k5eAaImIp3nS	psát
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
<g/>
,	,	kIx,	,
že	že	k8xS	že
panovník	panovník	k1gMnSc1	panovník
touží	toužit	k5eAaImIp3nS	toužit
odčinit	odčinit	k5eAaPmF	odčinit
své	svůj	k3xOyFgInPc4	svůj
mladické	mladický	k2eAgInPc4d1	mladický
hříchy	hřích	k1gInPc4	hřích
<g/>
,	,	kIx,	,
a	a	k8xC	a
dočkat	dočkat	k5eAaPmF	dočkat
se	se	k3xPyFc4	se
tak	tak	k9	tak
přijetí	přijetí	k1gNnSc1	přijetí
mezi	mezi	k7c7	mezi
zástupy	zástup	k1gInPc7	zástup
blažených	blažený	k2eAgMnPc2d1	blažený
ve	v	k7c6	v
společenství	společenství	k1gNnSc6	společenství
nebeské	nebeský	k2eAgFnSc2d1	nebeská
síně	síň	k1gFnSc2	síň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
zakládací	zakládací	k2eAgFnSc6d1	zakládací
listině	listina	k1gFnSc6	listina
z	z	k7c2	z
21	[number]	k4	21
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1304	[number]	k4	1304
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
rozšířit	rozšířit	k5eAaPmF	rozšířit
původní	původní	k2eAgNnSc4d1	původní
nadání	nadání	k1gNnSc4	nadání
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
vyzrálý	vyzrálý	k2eAgMnSc1d1	vyzrálý
král	král	k1gMnSc1	král
nejistotou	nejistota	k1gFnSc7	nejistota
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
vládě	vláda	k1gFnSc6	vláda
netrpěl	trpět	k5eNaImAgMnS	trpět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
toužil	toužit	k5eAaImAgMnS	toužit
po	po	k7c6	po
zprostředkování	zprostředkování	k1gNnSc6	zprostředkování
a	a	k8xC	a
modlitbách	modlitba	k1gFnPc6	modlitba
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
líbí	líbit	k5eAaImIp3nP	líbit
Bohu	bůh	k1gMnSc3	bůh
a	a	k8xC	a
jimž	jenž	k3xRgMnPc3	jenž
on	on	k3xPp3gMnSc1	on
svěřuje	svěřovat	k5eAaImIp3nS	svěřovat
své	svůj	k3xOyFgFnPc4	svůj
prosby	prosba	k1gFnPc4	prosba
<g/>
.	.	kIx.	.
</s>
<s>
Zakladatel	zakladatel	k1gMnSc1	zakladatel
věnoval	věnovat	k5eAaPmAgMnS	věnovat
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
klášteru	klášter	k1gInSc3	klášter
zlatý	zlatý	k2eAgInSc1d1	zlatý
kříž	kříž	k1gInSc1	kříž
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
monstrancí	monstrance	k1gFnPc2	monstrance
a	a	k8xC	a
finance	finance	k1gFnPc4	finance
na	na	k7c4	na
zakoupení	zakoupení	k1gNnSc4	zakoupení
základu	základ	k1gInSc2	základ
klášterní	klášterní	k2eAgFnSc2d1	klášterní
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
;	;	kIx,	;
knihy	kniha	k1gFnSc2	kniha
mniši	mnich	k1gMnPc1	mnich
pořídili	pořídit	k5eAaPmAgMnP	pořídit
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
při	při	k7c6	při
návratu	návrat	k1gInSc6	návrat
ze	z	k7c2	z
zasedání	zasedání	k1gNnSc2	zasedání
generální	generální	k2eAgFnSc2d1	generální
kapituly	kapitula	k1gFnSc2	kapitula
<g/>
.	.	kIx.	.
</s>
<s>
Svou	svůj	k3xOyFgFnSc4	svůj
žádost	žádost	k1gFnSc4	žádost
o	o	k7c6	o
schválení	schválení	k1gNnSc6	schválení
nové	nový	k2eAgFnSc2d1	nová
filiace	filiace	k1gFnSc2	filiace
král	král	k1gMnSc1	král
podpořil	podpořit	k5eAaPmAgInS	podpořit
štědrým	štědrý	k2eAgInSc7d1	štědrý
darem	dar	k1gInSc7	dar
<g/>
,	,	kIx,	,
vedení	vedení	k1gNnSc6	vedení
řádu	řád	k1gInSc2	řád
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
souhlasně	souhlasně	k6eAd1	souhlasně
vyslovilo	vyslovit	k5eAaPmAgNnS	vyslovit
pro	pro	k7c4	pro
napojení	napojení	k1gNnSc4	napojení
nového	nový	k2eAgInSc2d1	nový
kláštera	klášter	k1gInSc2	klášter
na	na	k7c4	na
filiační	filiační	k2eAgFnSc4d1	filiační
řadu	řada	k1gFnSc4	řada
kláštera	klášter	k1gInSc2	klášter
v	v	k7c4	v
Citeaux	Citeaux	k1gInSc4	Citeaux
a	a	k8xC	a
také	také	k9	také
splnilo	splnit	k5eAaPmAgNnS	splnit
fundátorovo	fundátorův	k2eAgNnSc1d1	fundátorův
přání	přání	k1gNnSc1	přání
a	a	k8xC	a
zahrnulo	zahrnout	k5eAaPmAgNnS	zahrnout
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
do	do	k7c2	do
řádových	řádový	k2eAgFnPc2d1	řádová
modliteb	modlitba	k1gFnPc2	modlitba
<g/>
.	.	kIx.	.
<g/>
Se	s	k7c7	s
stavbou	stavba	k1gFnSc7	stavba
kláštera	klášter	k1gInSc2	klášter
se	se	k3xPyFc4	se
započalo	započnout	k5eAaPmAgNnS	započnout
zřejmě	zřejmě	k6eAd1	zřejmě
již	již	k6eAd1	již
během	během	k7c2	během
roku	rok	k1gInSc2	rok
1292	[number]	k4	1292
a	a	k8xC	a
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
konventu	konvent	k1gInSc2	konvent
byly	být	k5eAaImAgFnP	být
upraveny	upravit	k5eAaPmNgFnP	upravit
i	i	k9	i
stávající	stávající	k2eAgFnPc1d1	stávající
budovy	budova	k1gFnPc1	budova
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
konventní	konventní	k2eAgInSc1d1	konventní
chrám	chrám	k1gInSc1	chrám
zasvěcený	zasvěcený	k2eAgInSc1d1	zasvěcený
Panně	Panna	k1gFnSc3	Panna
Marii	Maria	k1gFnSc3	Maria
a	a	k8xC	a
sv.	sv.	kA	sv.
Václavovi	Václav	k1gMnSc3	Václav
zmíněn	zmínit	k5eAaPmNgInS	zmínit
v	v	k7c6	v
odpustkové	odpustkový	k2eAgFnSc6d1	odpustková
listině	listina	k1gFnSc6	listina
bamberského	bamberský	k2eAgInSc2d1	bamberský
biskupa	biskup	k1gInSc2	biskup
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1294	[number]	k4	1294
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
základní	základní	k2eAgInSc1d1	základní
kámen	kámen	k1gInSc1	kámen
byl	být	k5eAaImAgInS	být
dle	dle	k7c2	dle
kroniky	kronika	k1gFnSc2	kronika
za	za	k7c2	za
asistence	asistence	k1gFnSc2	asistence
mnoha	mnoho	k4c2	mnoho
mocných	mocní	k1gMnPc2	mocní
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
položen	položit	k5eAaPmNgInS	položit
až	až	k9	až
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1297	[number]	k4	1297
<g/>
,	,	kIx,	,
den	den	k1gInSc4	den
po	po	k7c6	po
Václavově	Václavův	k2eAgFnSc6d1	Václavova
dlouho	dlouho	k6eAd1	dlouho
plánované	plánovaný	k2eAgFnSc6d1	plánovaná
korunovaci	korunovace	k1gFnSc6	korunovace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
odsloužena	odsloužit	k5eAaPmNgFnS	odsloužit
v	v	k7c6	v
základech	základ	k1gInPc6	základ
nového	nový	k2eAgInSc2d1	nový
chrámu	chrám	k1gInSc2	chrám
slavnostní	slavnostní	k2eAgFnSc2d1	slavnostní
mše	mše	k1gFnSc2	mše
a	a	k8xC	a
král	král	k1gMnSc1	král
zde	zde	k6eAd1	zde
pasoval	pasovat	k5eAaBmAgMnS	pasovat
240	[number]	k4	240
rytířů	rytíř	k1gMnPc2	rytíř
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
vydali	vydat	k5eAaPmAgMnP	vydat
zúčastnění	zúčastněný	k2eAgMnPc1d1	zúčastněný
biskupové	biskup	k1gMnPc1	biskup
a	a	k8xC	a
arcibiskupové	arcibiskup	k1gMnPc1	arcibiskup
magdeburský	magdeburský	k2eAgMnSc1d1	magdeburský
a	a	k8xC	a
mohučský	mohučský	k2eAgMnSc1d1	mohučský
odpustkové	odpustkový	k2eAgFnPc1d1	odpustková
listiny	listina	k1gFnPc1	listina
se	s	k7c7	s
čtyřicetidenními	čtyřicetidenní	k2eAgInPc7d1	čtyřicetidenní
odpustky	odpustek	k1gInPc7	odpustek
pro	pro	k7c4	pro
každého	každý	k3xTgMnSc4	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
klášter	klášter	k1gInSc4	klášter
navštíví	navštívit	k5eAaPmIp3nS	navštívit
či	či	k8xC	či
jej	on	k3xPp3gMnSc4	on
obdaruje	obdarovat	k5eAaPmIp3nS	obdarovat
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
rychle	rychle	k6eAd1	rychle
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1305	[number]	k4	1305
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
téměř	téměř	k6eAd1	téměř
celý	celý	k2eAgInSc1d1	celý
chrám	chrám	k1gInSc1	chrám
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
konventních	konventní	k2eAgFnPc2d1	konventní
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
královský	královský	k2eAgMnSc1d1	královský
a	a	k8xC	a
také	také	k9	také
opatský	opatský	k2eAgInSc4d1	opatský
příbytek	příbytek	k1gInSc4	příbytek
<g/>
.	.	kIx.	.
<g/>
Ironií	ironie	k1gFnSc7	ironie
osudu	osud	k1gInSc2	osud
byl	být	k5eAaImAgMnS	být
právě	právě	k6eAd1	právě
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
prvním	první	k4xOgMnSc6	první
z	z	k7c2	z
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
byl	být	k5eAaImAgInS	být
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
sotva	sotva	k6eAd1	sotva
dostavěném	dostavěný	k2eAgInSc6d1	dostavěný
chrámu	chrám	k1gInSc6	chrám
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1305	[number]	k4	1305
bylo	být	k5eAaImAgNnS	být
Václavovo	Václavův	k2eAgNnSc1d1	Václavovo
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
nemocí	nemoc	k1gFnSc7	nemoc
zubožené	zubožený	k2eAgNnSc4d1	zubožené
tělo	tělo	k1gNnSc4	tělo
v	v	k7c6	v
královském	královský	k2eAgInSc6d1	královský
šatu	šat	k1gInSc6	šat
uloženo	uložen	k2eAgNnSc1d1	uloženo
k	k	k7c3	k
poslednímu	poslední	k2eAgInSc3d1	poslední
odpočinku	odpočinek	k1gInSc3	odpočinek
a	a	k8xC	a
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
královského	královský	k2eAgInSc2d1	královský
pohřbu	pohřeb	k1gInSc2	pohřeb
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
slouženy	sloužen	k2eAgFnPc4d1	sloužena
první	první	k4xOgFnPc4	první
bohoslužby	bohoslužba	k1gFnPc4	bohoslužba
<g/>
.	.	kIx.	.
</s>
<s>
Kapitula	kapitula	k1gFnSc1	kapitula
v	v	k7c4	v
Citeaux	Citeaux	k1gInSc4	Citeaux
nařídila	nařídit	k5eAaPmAgFnS	nařídit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
klášterech	klášter	k1gInPc6	klášter
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
v	v	k7c6	v
Uhrách	Uhry	k1gFnPc6	Uhry
slouženy	sloužen	k2eAgFnPc1d1	sloužena
zádušní	zádušní	k2eAgFnPc1d1	zádušní
mše	mše	k1gFnPc1	mše
za	za	k7c2	za
zesnulého	zesnulý	k2eAgMnSc2d1	zesnulý
krále	král	k1gMnSc2	král
a	a	k8xC	a
zbraslavský	zbraslavský	k2eAgMnSc1d1	zbraslavský
opat	opat	k1gMnSc1	opat
Ota	Ota	k1gMnSc1	Ota
začal	začít	k5eAaPmAgMnS	začít
psát	psát	k5eAaImF	psát
na	na	k7c4	na
oslavu	oslava	k1gFnSc4	oslava
velkého	velký	k2eAgMnSc2d1	velký
příznivce	příznivec	k1gMnSc2	příznivec
první	první	k4xOgFnSc4	první
kapitolu	kapitola	k1gFnSc4	kapitola
Zbraslavské	zbraslavský	k2eAgFnSc2d1	Zbraslavská
kroniky	kronika	k1gFnSc2	kronika
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
měla	mít	k5eAaImAgFnS	mít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
sloužit	sloužit	k5eAaImF	sloužit
k	k	k7c3	k
nastolení	nastolení	k1gNnSc3	nastolení
otázky	otázka	k1gFnSc2	otázka
případného	případný	k2eAgInSc2d1	případný
kanonizačního	kanonizační	k2eAgInSc2d1	kanonizační
procesu	proces	k1gInSc2	proces
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
Václavově	Václavův	k2eAgFnSc6d1	Václavova
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
osiřelého	osiřelý	k2eAgInSc2d1	osiřelý
trůnu	trůn	k1gInSc2	trůn
ujal	ujmout	k5eAaPmAgMnS	ujmout
mladičký	mladičký	k2eAgMnSc1d1	mladičký
následník	následník	k1gMnSc1	následník
Václav	Václav	k1gMnSc1	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
dle	dle	k7c2	dle
klášterní	klášterní	k2eAgFnSc2d1	klášterní
kroniky	kronika	k1gFnSc2	kronika
mrhal	mrhat	k5eAaImAgMnS	mrhat
svým	svůj	k3xOyFgNnSc7	svůj
nadáním	nadání	k1gNnSc7	nadání
a	a	k8xC	a
vůbec	vůbec	k9	vůbec
vedl	vést	k5eAaImAgMnS	vést
rozmařilý	rozmařilý	k2eAgInSc4d1	rozmařilý
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Zabralo	zabrat	k5eAaPmAgNnS	zabrat
na	na	k7c4	na
něj	on	k3xPp3gInSc4	on
údajně	údajně	k6eAd1	údajně
až	až	k9	až
pokárání	pokárání	k1gNnSc2	pokárání
opatem	opat	k1gMnSc7	opat
Konrádem	Konrád	k1gMnSc7	Konrád
přímo	přímo	k6eAd1	přímo
u	u	k7c2	u
otcova	otcův	k2eAgInSc2d1	otcův
hrobu	hrob	k1gInSc2	hrob
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1306	[number]	k4	1306
byl	být	k5eAaImAgMnS	být
Václav	Václav	k1gMnSc1	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
na	na	k7c6	na
válečném	válečný	k2eAgNnSc6d1	válečné
tažení	tažení	k1gNnSc6	tažení
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
a	a	k8xC	a
království	království	k1gNnSc4	království
zachvátila	zachvátit	k5eAaPmAgFnS	zachvátit
na	na	k7c4	na
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
nejistota	nejistota	k1gFnSc1	nejistota
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
častými	častý	k2eAgFnPc7d1	častá
změnami	změna	k1gFnPc7	změna
na	na	k7c6	na
královském	královský	k2eAgInSc6d1	královský
trůnu	trůn	k1gInSc6	trůn
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
podepsala	podepsat	k5eAaPmAgFnS	podepsat
i	i	k9	i
na	na	k7c6	na
rozvoji	rozvoj	k1gInSc6	rozvoj
konventu	konvent	k1gInSc2	konvent
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1307	[number]	k4	1307
byla	být	k5eAaImAgFnS	být
zastavena	zastaven	k2eAgFnSc1d1	zastavena
stavba	stavba	k1gFnSc1	stavba
téměř	téměř	k6eAd1	téměř
dokončeného	dokončený	k2eAgInSc2d1	dokončený
chrámu	chrám	k1gInSc2	chrám
a	a	k8xC	a
znovu	znovu	k6eAd1	znovu
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
budovat	budovat	k5eAaImF	budovat
až	až	k6eAd1	až
roku	rok	k1gInSc2	rok
1329	[number]	k4	1329
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Eliška	Eliška	k1gFnSc1	Eliška
Přemyslovna	Přemyslovna	k1gFnSc1	Přemyslovna
<g/>
,	,	kIx,	,
štědrá	štědrý	k2eAgFnSc1d1	štědrá
mecenáška	mecenáška	k1gFnSc1	mecenáška
zbraslavského	zbraslavský	k2eAgInSc2d1	zbraslavský
pokladu	poklad	k1gInSc2	poklad
<g/>
,	,	kIx,	,
nechala	nechat	k5eAaPmAgFnS	nechat
přistavět	přistavět	k5eAaPmF	přistavět
věnec	věnec	k1gInSc4	věnec
devíti	devět	k4xCc2	devět
kaplí	kaple	k1gFnPc2	kaple
<g/>
,	,	kIx,	,
za	za	k7c4	za
což	což	k3yRnSc4	což
jí	jíst	k5eAaImIp3nS	jíst
Petr	Petr	k1gMnSc1	Petr
Žitavský	žitavský	k2eAgMnSc1d1	žitavský
začal	začít	k5eAaPmAgMnS	začít
zvát	zvát	k5eAaImF	zvát
druhou	druhý	k4xOgFnSc7	druhý
zakladatelkou	zakladatelka	k1gFnSc7	zakladatelka
a	a	k8xC	a
"	"	kIx"	"
<g/>
matkou	matka	k1gFnSc7	matka
<g/>
"	"	kIx"	"
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
<g/>
Refektář	refektář	k1gInSc1	refektář
byl	být	k5eAaImAgInS	být
dostavěn	dostavěn	k2eAgInSc1d1	dostavěn
roku	rok	k1gInSc2	rok
1327	[number]	k4	1327
a	a	k8xC	a
vodovod	vodovod	k1gInSc1	vodovod
s	s	k7c7	s
umývárnou	umývárna	k1gFnSc7	umývárna
roku	rok	k1gInSc2	rok
1333	[number]	k4	1333
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
v	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
i	i	k9	i
ambit	ambit	k1gInSc1	ambit
se	s	k7c7	s
studničním	studniční	k2eAgNnSc7d1	studniční
stavením	stavení	k1gNnSc7	stavení
při	při	k7c6	při
rajském	rajský	k2eAgInSc6d1	rajský
dvoře	dvůr	k1gInSc6	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Dobové	dobový	k2eAgInPc4d1	dobový
prameny	pramen	k1gInPc4	pramen
hovoří	hovořit	k5eAaImIp3nS	hovořit
také	také	k9	také
o	o	k7c6	o
nemocnici	nemocnice	k1gFnSc6	nemocnice
s	s	k7c7	s
kaplí	kaple	k1gFnSc7	kaple
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1311	[number]	k4	1311
<g/>
,	,	kIx,	,
a	a	k8xC	a
kapli	kaple	k1gFnSc4	kaple
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
v	v	k7c6	v
klášterní	klášterní	k2eAgFnSc6d1	klášterní
bráně	brána	k1gFnSc6	brána
zbořené	zbořený	k2eAgFnSc2d1	zbořená
roku	rok	k1gInSc2	rok
1785	[number]	k4	1785
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
1331	[number]	k4	1331
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgInSc1d1	lucemburský
osvobodil	osvobodit	k5eAaPmAgInS	osvobodit
klášter	klášter	k1gInSc1	klášter
od	od	k7c2	od
placení	placení	k1gNnSc2	placení
dávek	dávka	k1gFnPc2	dávka
a	a	k8xC	a
poplatků	poplatek	k1gInPc2	poplatek
pro	pro	k7c4	pro
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
oslavu	oslava	k1gFnSc4	oslava
královské	královský	k2eAgFnSc2d1	královská
fundace	fundace	k1gFnSc2	fundace
a	a	k8xC	a
královského	královský	k2eAgNnSc2d1	královské
pohřebiště	pohřebiště	k1gNnSc2	pohřebiště
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
Zbraslav	Zbraslav	k1gFnSc1	Zbraslav
ztratila	ztratit	k5eAaPmAgFnS	ztratit
na	na	k7c6	na
politickém	politický	k2eAgInSc6d1	politický
významu	význam	k1gInSc6	význam
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
opatské	opatský	k2eAgFnPc1d1	opatská
pečeti	pečeť	k1gFnPc1	pečeť
s	s	k7c7	s
českým	český	k2eAgInSc7d1	český
královským	královský	k2eAgInSc7d1	královský
lvem	lev	k1gInSc7	lev
přetrvaly	přetrvat	k5eAaPmAgInP	přetrvat
až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dočasné	dočasný	k2eAgFnSc3d1	dočasná
stabilizaci	stabilizace	k1gFnSc3	stabilizace
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
situace	situace	k1gFnSc2	situace
došlo	dojít	k5eAaPmAgNnS	dojít
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
klášter	klášter	k1gInSc1	klášter
znovu	znovu	k6eAd1	znovu
prosperoval	prosperovat	k5eAaImAgInS	prosperovat
<g/>
.	.	kIx.	.
</s>
<s>
Stav	stav	k1gInSc1	stav
se	se	k3xPyFc4	se
zhoršil	zhoršit	k5eAaPmAgInS	zhoršit
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
zbraslavští	zbraslavský	k2eAgMnPc1d1	zbraslavský
mniši	mnich	k1gMnPc1	mnich
zadlužili	zadlužit	k5eAaPmAgMnP	zadlužit
a	a	k8xC	a
byli	být	k5eAaImAgMnP	být
nuceni	nutit	k5eAaImNgMnP	nutit
zastavovat	zastavovat	k5eAaImF	zastavovat
a	a	k8xC	a
odprodávat	odprodávat	k5eAaImF	odprodávat
své	svůj	k3xOyFgNnSc4	svůj
zboží	zboží	k1gNnSc4	zboží
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Úpadek	úpadek	k1gInSc1	úpadek
a	a	k8xC	a
zánik	zánik	k1gInSc1	zánik
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1420	[number]	k4	1420
se	se	k3xPyFc4	se
na	na	k7c6	na
Zbraslavi	Zbraslav	k1gFnSc6	Zbraslav
usídlil	usídlit	k5eAaPmAgMnS	usídlit
král	král	k1gMnSc1	král
Zikmund	Zikmund	k1gMnSc1	Zikmund
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
vojáky	voják	k1gMnPc7	voják
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
jal	jmout	k5eAaPmAgMnS	jmout
obléhat	obléhat	k5eAaImF	obléhat
Prahu	Praha	k1gFnSc4	Praha
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1420	[number]	k4	1420
vnikly	vniknout	k5eAaPmAgFnP	vniknout
do	do	k7c2	do
kláštera	klášter	k1gInSc2	klášter
houfy	houf	k1gInPc4	houf
husitů	husita	k1gMnPc2	husita
a	a	k8xC	a
pražanů	pražan	k1gMnPc2	pražan
vedených	vedený	k2eAgFnPc2d1	vedená
knězem	kněz	k1gMnSc7	kněz
Václavem	Václav	k1gMnSc7	Václav
Korandou	Koranda	k1gFnSc7	Koranda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dle	dle	k7c2	dle
svědectví	svědectví	k1gNnSc2	svědectví
dobových	dobový	k2eAgFnPc2d1	dobová
kronik	kronika	k1gFnPc2	kronika
dav	dav	k1gInSc1	dav
opilý	opilý	k2eAgInSc1d1	opilý
klášterním	klášterní	k2eAgNnSc7d1	klášterní
pivem	pivo	k1gNnSc7	pivo
a	a	k8xC	a
vínem	víno	k1gNnSc7	víno
zneuctil	zneuctít	k5eAaPmAgInS	zneuctít
královské	královský	k2eAgNnSc4d1	královské
pohřebiště	pohřebiště	k1gNnSc4	pohřebiště
a	a	k8xC	a
manipuloval	manipulovat	k5eAaImAgMnS	manipulovat
s	s	k7c7	s
pozůstatky	pozůstatek	k1gInPc7	pozůstatek
<g/>
.	.	kIx.	.
</s>
<s>
Tělo	tělo	k1gNnSc1	tělo
Václava	Václav	k1gMnSc2	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
bylo	být	k5eAaImAgNnS	být
údajně	údajně	k6eAd1	údajně
vytaženo	vytáhnout	k5eAaPmNgNnS	vytáhnout
z	z	k7c2	z
rakve	rakev	k1gFnSc2	rakev
<g/>
,	,	kIx,	,
ověnčeno	ověnčen	k2eAgNnSc4d1	ověnčeno
senem	seno	k1gNnSc7	seno
a	a	k8xC	a
napájeno	napájet	k5eAaImNgNnS	napájet
pivním	pivní	k2eAgInSc7d1	pivní
mokem	mok	k1gInSc7	mok
<g/>
.	.	kIx.	.
</s>
<s>
Klášter	klášter	k1gInSc1	klášter
byl	být	k5eAaImAgInS	být
vydrancován	vydrancovat	k5eAaPmNgInS	vydrancovat
a	a	k8xC	a
Síň	síň	k1gFnSc1	síň
královská	královský	k2eAgFnSc1d1	královská
podlehla	podlehnout	k5eAaPmAgFnS	podlehnout
plamenům	plamen	k1gInPc3	plamen
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yQnSc4	co
nestrávil	strávit	k5eNaPmAgInS	strávit
oheň	oheň	k1gInSc1	oheň
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
si	se	k3xPyFc3	se
drancující	drancující	k2eAgMnPc1d1	drancující
odnesli	odnést	k5eAaPmAgMnP	odnést
<g/>
.	.	kIx.	.
</s>
<s>
Prozíraví	prozíravý	k2eAgMnPc1d1	prozíravý
cisterciáci	cisterciák	k1gMnPc1	cisterciák
sice	sice	k8xC	sice
roku	rok	k1gInSc2	rok
1419	[number]	k4	1419
ukryli	ukrýt	k5eAaPmAgMnP	ukrýt
klášterní	klášterní	k2eAgFnPc4d1	klášterní
cennosti	cennost	k1gFnPc4	cennost
a	a	k8xC	a
knihy	kniha	k1gFnPc4	kniha
na	na	k7c6	na
Karlštejně	Karlštejn	k1gInSc6	Karlštejn
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Zikmund	Zikmund	k1gMnSc1	Zikmund
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
také	také	k9	také
hradní	hradní	k2eAgMnSc1d1	hradní
purkrabí	purkrabí	k1gMnSc1	purkrabí
cenné	cenný	k2eAgFnSc2d1	cenná
věci	věc	k1gFnSc2	věc
zabavil	zabavit	k5eAaPmAgInS	zabavit
a	a	k8xC	a
prodal	prodat	k5eAaPmAgInS	prodat
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1429	[number]	k4	1429
byl	být	k5eAaImAgInS	být
klášter	klášter	k1gInSc1	klášter
zřejmě	zřejmě	k6eAd1	zřejmě
znovu	znovu	k6eAd1	znovu
poškozen	poškodit	k5eAaPmNgInS	poškodit
bitvou	bitva	k1gFnSc7	bitva
<g/>
.	.	kIx.	.
<g/>
Mniši	mnich	k1gMnPc1	mnich
se	se	k3xPyFc4	se
ve	v	k7c6	v
čtyřicátých	čtyřicátý	k4xOgNnPc6	čtyřicátý
letech	léto	k1gNnPc6	léto
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
na	na	k7c4	na
Zbraslav	Zbraslav	k1gFnSc4	Zbraslav
vrátili	vrátit	k5eAaPmAgMnP	vrátit
<g/>
,	,	kIx,	,
postavili	postavit	k5eAaPmAgMnP	postavit
si	se	k3xPyFc3	se
nové	nový	k2eAgFnPc4d1	nová
budovy	budova	k1gFnPc4	budova
a	a	k8xC	a
pokusili	pokusit	k5eAaPmAgMnP	pokusit
se	se	k3xPyFc4	se
opatství	opatství	k1gNnSc4	opatství
obnovit	obnovit	k5eAaPmF	obnovit
<g/>
.	.	kIx.	.
</s>
<s>
Klášterní	klášterní	k2eAgInSc1d1	klášterní
majetek	majetek	k1gInSc1	majetek
však	však	k9	však
byl	být	k5eAaImAgInS	být
rozchvácen	rozchvácen	k2eAgInSc1d1	rozchvácen
a	a	k8xC	a
některá	některý	k3yIgFnSc1	některý
klášterní	klášterní	k2eAgFnSc1d1	klášterní
zboží	zboží	k1gNnSc2	zboží
již	již	k6eAd1	již
měla	mít	k5eAaImAgFnS	mít
nové	nový	k2eAgMnPc4d1	nový
majitele	majitel	k1gMnPc4	majitel
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1445	[number]	k4	1445
zastavili	zastavit	k5eAaPmAgMnP	zastavit
nějaké	nějaký	k3yIgFnPc4	nějaký
polnosti	polnost	k1gFnPc4	polnost
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
"	"	kIx"	"
<g/>
klášter	klášter	k1gInSc1	klášter
náš	náš	k3xOp1gInSc1	náš
<g/>
...	...	k?	...
válkami	válka	k1gFnPc7	válka
zkažen	zkazit	k5eAaPmNgInS	zkazit
<g/>
,	,	kIx,	,
zbořen	zbořit	k5eAaPmNgInS	zbořit
i	i	k8xC	i
vypálen	vypálen	k2eAgInSc1d1	vypálen
<g/>
...	...	k?	...
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
poznenáhlu	poznenáhlu	k6eAd1	poznenáhlu
opraven	opravna	k1gFnPc2	opravna
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
svou	svůj	k3xOyFgFnSc4	svůj
devastaci	devastace	k1gFnSc4	devastace
udělaly	udělat	k5eAaPmAgFnP	udělat
roku	rok	k1gInSc2	rok
1451	[number]	k4	1451
poničené	poničený	k2eAgFnSc2d1	poničená
budovy	budova	k1gFnSc2	budova
velký	velký	k2eAgInSc1d1	velký
dojem	dojem	k1gInSc1	dojem
na	na	k7c4	na
budoucího	budoucí	k2eAgMnSc4d1	budoucí
papeže	papež	k1gMnSc4	papež
Pia	Pius	k1gMnSc2	Pius
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
zaujaly	zaujmout	k5eAaPmAgFnP	zaujmout
desky	deska	k1gFnPc1	deska
s	s	k7c7	s
citáty	citát	k1gInPc7	citát
ze	z	k7c2	z
Starého	Starého	k2eAgInSc2d1	Starého
a	a	k8xC	a
Nového	Nového	k2eAgInSc2d1	Nového
zákona	zákon	k1gInSc2	zákon
umístěné	umístěný	k2eAgInPc1d1	umístěný
v	v	k7c6	v
křížové	křížový	k2eAgFnSc6d1	křížová
chodbě	chodba	k1gFnSc6	chodba
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
psány	psát	k5eAaImNgFnP	psát
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
písmena	písmeno	k1gNnPc1	písmeno
se	s	k7c7	s
vzdáleností	vzdálenost	k1gFnSc7	vzdálenost
od	od	k7c2	od
země	zem	k1gFnSc2	zem
zvětšovala	zvětšovat	k5eAaImAgFnS	zvětšovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
tak	tak	k6eAd1	tak
brán	brát	k5eAaImNgInS	brát
ohled	ohled	k1gInSc1	ohled
na	na	k7c4	na
případného	případný	k2eAgMnSc4d1	případný
čtenáře	čtenář	k1gMnSc4	čtenář
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
jagellonské	jagellonský	k2eAgFnSc2d1	Jagellonská
zřejmě	zřejmě	k6eAd1	zřejmě
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
stavebním	stavební	k2eAgFnPc3d1	stavební
úpravám	úprava	k1gFnPc3	úprava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jedinými	jediný	k2eAgNnPc7d1	jediné
svědectvími	svědectví	k1gNnPc7	svědectví
o	o	k7c6	o
vzhledu	vzhled	k1gInSc6	vzhled
konventu	konvent	k1gInSc2	konvent
v	v	k7c6	v
době	doba	k1gFnSc6	doba
následující	následující	k2eAgFnSc6d1	následující
je	být	k5eAaImIp3nS	být
veduta	veduta	k1gFnSc1	veduta
z	z	k7c2	z
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
namalovaná	namalovaný	k2eAgFnSc1d1	namalovaná
na	na	k7c4	na
pozadí	pozadí	k1gNnSc4	pozadí
epitafu	epitaf	k1gInSc2	epitaf
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
Václava	Václav	k1gMnSc4	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
její	její	k3xOp3gMnSc1	její
o	o	k7c4	o
sto	sto	k4xCgNnSc4	sto
let	léto	k1gNnPc2	léto
mladší	mladý	k2eAgFnSc1d2	mladší
varianta	varianta	k1gFnSc1	varianta
a	a	k8xC	a
kolorovaná	kolorovaný	k2eAgFnSc1d1	kolorovaná
kresba	kresba	k1gFnSc1	kresba
Pohledové	pohledový	k2eAgFnSc2d1	pohledová
mapy	mapa	k1gFnSc2	mapa
Prahy	Praha	k1gFnSc2	Praha
skotského	skotský	k1gInSc2	skotský
sochaře	sochař	k1gMnSc2	sochař
Daniela	Daniel	k1gMnSc2	Daniel
Altmana	Altman	k1gMnSc2	Altman
z	z	k7c2	z
Edinburgu	Edinburg	k1gInSc2	Edinburg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Třicetiletá	třicetiletý	k2eAgFnSc1d1	třicetiletá
válka	válka	k1gFnSc1	válka
způsobila	způsobit	k5eAaPmAgFnS	způsobit
klášteru	klášter	k1gInSc3	klášter
další	další	k2eAgFnSc2d1	další
těžké	těžký	k2eAgFnSc2d1	těžká
škody	škoda	k1gFnSc2	škoda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1611	[number]	k4	1611
a	a	k8xC	a
1612	[number]	k4	1612
byl	být	k5eAaImAgInS	být
opakovaně	opakovaně	k6eAd1	opakovaně
vydrancován	vydrancován	k2eAgInSc4d1	vydrancován
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1618	[number]	k4	1618
získal	získat	k5eAaPmAgInS	získat
větší	veliký	k2eAgFnSc4d2	veliký
část	část	k1gFnSc4	část
jeho	jeho	k3xOp3gInSc2	jeho
majetku	majetek	k1gInSc2	majetek
Jindřich	Jindřich	k1gMnSc1	Jindřich
Matyáš	Matyáš	k1gMnSc1	Matyáš
Thurn	Thurn	k1gMnSc1	Thurn
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1639	[number]	k4	1639
zde	zde	k6eAd1	zde
opět	opět	k6eAd1	opět
šlehaly	šlehat	k5eAaImAgInP	šlehat
plameny	plamen	k1gInPc4	plamen
vinou	vinou	k7c2	vinou
velitele	velitel	k1gMnSc2	velitel
švédského	švédský	k2eAgNnSc2d1	švédské
vojska	vojsko	k1gNnSc2	vojsko
a	a	k8xC	a
císařská	císařský	k2eAgFnSc1d1	císařská
armáda	armáda	k1gFnSc1	armáda
o	o	k7c4	o
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
devastaci	devastace	k1gFnSc3	devastace
klášterního	klášterní	k2eAgInSc2d1	klášterní
areálu	areál	k1gInSc2	areál
téměř	téměř	k6eAd1	téměř
dokončila	dokončit	k5eAaPmAgFnS	dokončit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klášter	klášter	k1gInSc1	klášter
v	v	k7c6	v
novověku	novověk	k1gInSc6	novověk
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
začal	začít	k5eAaPmAgInS	začít
klášter	klášter	k1gInSc1	klášter
znovu	znovu	k6eAd1	znovu
prosperovat	prosperovat	k5eAaImF	prosperovat
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1654	[number]	k4	1654
byl	být	k5eAaImAgInS	být
vysvěcen	vysvětit	k5eAaPmNgInS	vysvětit
nově	nově	k6eAd1	nově
postavený	postavený	k2eAgInSc1d1	postavený
opatský	opatský	k2eAgInSc1d1	opatský
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Jakuba	Jakub	k1gMnSc2	Jakub
a	a	k8xC	a
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
konečně	konečně	k6eAd1	konečně
chystala	chystat	k5eAaImAgFnS	chystat
obnova	obnova	k1gFnSc1	obnova
klášterních	klášterní	k2eAgFnPc2d1	klášterní
budov	budova	k1gFnPc2	budova
podle	podle	k7c2	podle
plánů	plán	k1gInPc2	plán
Jana	Jan	k1gMnSc2	Jan
Santiniho	Santini	k1gMnSc2	Santini
<g/>
.	.	kIx.	.
</s>
<s>
Stavby	stavba	k1gFnPc1	stavba
byly	být	k5eAaImAgFnP	být
ve	v	k7c6	v
špatném	špatný	k2eAgInSc6d1	špatný
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nezbývalo	zbývat	k5eNaImAgNnS	zbývat
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
strhnout	strhnout	k5eAaPmF	strhnout
a	a	k8xC	a
začít	začít	k5eAaPmF	začít
stavět	stavět	k5eAaImF	stavět
novou	nový	k2eAgFnSc4d1	nová
konventní	konventní	k2eAgFnSc4d1	konventní
trojkřídlou	trojkřídlý	k2eAgFnSc4d1	trojkřídlá
budovu	budova	k1gFnSc4	budova
v	v	k7c6	v
barokním	barokní	k2eAgInSc6d1	barokní
slohu	sloh	k1gInSc6	sloh
podle	podle	k7c2	podle
Santiniho	Santini	k1gMnSc2	Santini
návrhu	návrh	k1gInSc2	návrh
<g/>
,	,	kIx,	,
po	po	k7c6	po
jehož	jehož	k3xOyRp3gFnSc6	jehož
smrti	smrt	k1gFnSc6	smrt
převzal	převzít	k5eAaPmAgMnS	převzít
roku	rok	k1gInSc2	rok
1724	[number]	k4	1724
stavbu	stavba	k1gFnSc4	stavba
František	František	k1gMnSc1	František
Maxmilián	Maxmilián	k1gMnSc1	Maxmilián
Kaňka	Kaňka	k1gMnSc1	Kaňka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pramenů	pramen	k1gInPc2	pramen
se	se	k3xPyFc4	se
dozvídáme	dozvídat	k5eAaImIp1nP	dozvídat
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
příslibu	příslib	k1gInSc6	příslib
dokončit	dokončit	k5eAaPmF	dokončit
stavbu	stavba	k1gFnSc4	stavba
dle	dle	k7c2	dle
původních	původní	k2eAgFnPc2d1	původní
<g/>
,	,	kIx,	,
nepochybně	pochybně	k6eNd1	pochybně
Santiniho	Santini	k1gMnSc2	Santini
plánů	plán	k1gInPc2	plán
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dokončení	dokončení	k1gNnSc3	dokončení
velkolepého	velkolepý	k2eAgInSc2d1	velkolepý
projektu	projekt	k1gInSc2	projekt
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
klášter	klášter	k1gInSc1	klášter
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1785	[number]	k4	1785
císařem	císař	k1gMnSc7	císař
Josefem	Josef	k1gMnSc7	Josef
II	II	kA	II
<g/>
.	.	kIx.	.
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Klášterní	klášterní	k2eAgInPc4d1	klášterní
statky	statek	k1gInPc4	statek
získal	získat	k5eAaPmAgInS	získat
náboženský	náboženský	k2eAgInSc1d1	náboženský
fond	fond	k1gInSc1	fond
<g/>
,	,	kIx,	,
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Jakuba	Jakub	k1gMnSc2	Jakub
byl	být	k5eAaImAgInS	být
odsvěcen	odsvěcen	k2eAgMnSc1d1	odsvěcen
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
klenoty	klenot	k1gInPc1	klenot
byly	být	k5eAaImAgInP	být
rozprodány	rozprodat	k5eAaPmNgInP	rozprodat
ve	v	k7c6	v
veřejné	veřejný	k2eAgFnSc6d1	veřejná
dražbě	dražba	k1gFnSc6	dražba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přeměna	přeměna	k1gFnSc1	přeměna
na	na	k7c4	na
zámek	zámek	k1gInSc4	zámek
==	==	k?	==
</s>
</p>
<p>
<s>
Když	když	k8xS	když
koncem	koncem	k7c2	koncem
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
získal	získat	k5eAaPmAgMnS	získat
klášter	klášter	k1gInSc4	klášter
Josef	Josef	k1gMnSc1	Josef
ze	z	k7c2	z
Souvaigue	Souvaigu	k1gFnSc2	Souvaigu
<g/>
,	,	kIx,	,
nechal	nechat	k5eAaPmAgMnS	nechat
budovy	budova	k1gFnPc4	budova
přebudovat	přebudovat	k5eAaPmF	přebudovat
na	na	k7c4	na
cukrovar	cukrovar	k1gInSc4	cukrovar
<g/>
.	.	kIx.	.
</s>
<s>
Majitelé	majitel	k1gMnPc1	majitel
se	se	k3xPyFc4	se
střídali	střídat	k5eAaImAgMnP	střídat
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
byla	být	k5eAaImAgFnS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
také	také	k9	také
o	o	k7c4	o
chemikálie	chemikálie	k1gFnPc4	chemikálie
a	a	k8xC	a
pivo	pivo	k1gNnSc4	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
se	se	k3xPyFc4	se
koupí	koupě	k1gFnSc7	koupě
stal	stát	k5eAaPmAgMnS	stát
novým	nový	k2eAgMnSc7d1	nový
vlastníkem	vlastník	k1gMnSc7	vlastník
Cyril	Cyril	k1gMnSc1	Cyril
Bartoň	Bartoň	k1gMnSc1	Bartoň
z	z	k7c2	z
Dobenína	Dobenín	k1gInSc2	Dobenín
<g/>
,	,	kIx,	,
předek	předek	k1gMnSc1	předek
dnešních	dnešní	k2eAgMnPc2d1	dnešní
majitelů	majitel	k1gMnPc2	majitel
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nechal	nechat	k5eAaPmAgInS	nechat
klášter	klášter	k1gInSc4	klášter
přestavět	přestavět	k5eAaPmF	přestavět
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
Dušana	Dušan	k1gMnSc2	Dušan
Jurkoviče	Jurkovič	k1gMnSc2	Jurkovič
na	na	k7c4	na
zámek	zámek	k1gInSc4	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zásluhy	zásluha	k1gFnPc4	zásluha
o	o	k7c4	o
rozkvět	rozkvět	k1gInSc4	rozkvět
Zbraslavi	Zbraslav	k1gFnSc2	Zbraslav
byl	být	k5eAaImAgMnS	být
Cyril	Cyril	k1gMnSc1	Cyril
Bartoň	Bartoň	k1gMnSc1	Bartoň
roku	rok	k1gInSc2	rok
1927	[number]	k4	1927
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
čestným	čestný	k2eAgMnSc7d1	čestný
občanem	občan	k1gMnSc7	občan
Zbraslavi	Zbraslav	k1gFnSc2	Zbraslav
a	a	k8xC	a
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
památku	památka	k1gFnSc4	památka
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
nádvoří	nádvoří	k1gNnSc6	nádvoří
umístěna	umístěn	k2eAgFnSc1d1	umístěna
pamětní	pamětní	k2eAgFnSc1d1	pamětní
deska	deska	k1gFnSc1	deska
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
"	"	kIx"	"
<g/>
Co	co	k3yRnSc1	co
průmysl	průmysl	k1gInSc4	průmysl
zničil	zničit	k5eAaPmAgMnS	zničit
<g/>
,	,	kIx,	,
průmyslník	průmyslník	k1gMnSc1	průmyslník
Cyril	Cyril	k1gMnSc1	Cyril
Bartoň	Bartoň	k1gMnSc1	Bartoň
obnovil	obnovit	k5eAaPmAgMnS	obnovit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
byly	být	k5eAaImAgFnP	být
konventní	konventní	k2eAgFnPc1d1	konventní
budovy	budova	k1gFnPc1	budova
zapůjčeny	zapůjčit	k5eAaPmNgFnP	zapůjčit
jako	jako	k8xS	jako
depozitář	depozitář	k1gMnSc1	depozitář
Národní	národní	k2eAgFnSc4d1	národní
galerii	galerie	k1gFnSc4	galerie
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1939	[number]	k4	1939
až	až	k9	až
2009	[number]	k4	2009
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
stálá	stálý	k2eAgFnSc1d1	stálá
výstava	výstava	k1gFnSc1	výstava
asijského	asijský	k2eAgNnSc2d1	asijské
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Bartoňům	Bartoň	k1gMnPc3	Bartoň
z	z	k7c2	z
Dobenína	Dobenín	k1gInSc2	Dobenín
byl	být	k5eAaImAgInS	být
zámek	zámek	k1gInSc1	zámek
vrácen	vrátit	k5eAaPmNgInS	vrátit
v	v	k7c6	v
restituci	restituce	k1gFnSc6	restituce
a	a	k8xC	a
plánuje	plánovat	k5eAaImIp3nS	plánovat
se	se	k3xPyFc4	se
jeho	on	k3xPp3gNnSc2	on
využití	využití	k1gNnSc2	využití
jako	jako	k8xS	jako
depozitáře	depozitář	k1gInSc2	depozitář
knihovny	knihovna	k1gFnSc2	knihovna
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Architektura	architektura	k1gFnSc1	architektura
==	==	k?	==
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
kláštery	klášter	k1gInPc1	klášter
bílých	bílý	k2eAgMnPc2d1	bílý
bratří	bratr	k1gMnPc2	bratr
vycházely	vycházet	k5eAaImAgFnP	vycházet
z	z	k7c2	z
tradiční	tradiční	k2eAgFnSc2d1	tradiční
dispozice	dispozice	k1gFnSc2	dispozice
benediktinských	benediktinský	k2eAgNnPc2d1	benediktinské
opatství	opatství	k1gNnPc2	opatství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přímé	přímý	k2eAgFnSc6d1	přímá
opozici	opozice	k1gFnSc6	opozice
proti	proti	k7c3	proti
jejich	jejich	k3xOp3gFnSc3	jejich
nádheře	nádhera	k1gFnSc3	nádhera
zde	zde	k6eAd1	zde
však	však	k9	však
byla	být	k5eAaImAgFnS	být
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
prostotu	prostota	k1gFnSc4	prostota
prostoru	prostor	k1gInSc2	prostor
určeného	určený	k2eAgInSc2d1	určený
k	k	k7c3	k
modlitbě	modlitba	k1gFnSc3	modlitba
i	i	k8xC	i
práci	práce	k1gFnSc3	práce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
Bernard	Bernard	k1gMnSc1	Bernard
byl	být	k5eAaImAgMnS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nádhera	nádhera	k1gFnSc1	nádhera
odvádí	odvádět	k5eAaImIp3nS	odvádět
myšlenky	myšlenka	k1gFnPc4	myšlenka
od	od	k7c2	od
vnitřního	vnitřní	k2eAgInSc2d1	vnitřní
prožitku	prožitek	k1gInSc2	prožitek
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
první	první	k4xOgInPc1	první
řádové	řádový	k2eAgInPc1d1	řádový
kostely	kostel	k1gInPc1	kostel
<g/>
,	,	kIx,	,
stavěné	stavěný	k2eAgInPc1d1	stavěný
často	často	k6eAd1	často
na	na	k7c6	na
půdorysu	půdorys	k1gInSc6	půdorys
latinského	latinský	k2eAgInSc2d1	latinský
kříže	kříž	k1gInSc2	kříž
<g/>
,	,	kIx,	,
dodnes	dodnes	k6eAd1	dodnes
ohromují	ohromovat	k5eAaImIp3nP	ohromovat
čistotou	čistota	k1gFnSc7	čistota
slohu	sloh	k1gInSc2	sloh
a	a	k8xC	a
přítomností	přítomnost	k1gFnSc7	přítomnost
přirozeného	přirozený	k2eAgNnSc2d1	přirozené
světla	světlo	k1gNnSc2	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
nastala	nastat	k5eAaPmAgFnS	nastat
až	až	k9	až
s	s	k7c7	s
blížícím	blížící	k2eAgMnSc7d1	blížící
se	se	k3xPyFc4	se
13	[number]	k4	13
<g/>
.	.	kIx.	.
stoletím	století	k1gNnSc7	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
do	do	k7c2	do
staveb	stavba	k1gFnPc2	stavba
postupně	postupně	k6eAd1	postupně
začala	začít	k5eAaPmAgFnS	začít
vkrádat	vkrádat	k5eAaImF	vkrádat
zdobnost	zdobnost	k1gFnSc4	zdobnost
dekoru	dekor	k1gInSc2	dekor
hlavic	hlavice	k1gFnPc2	hlavice
<g/>
,	,	kIx,	,
svorníků	svorník	k1gInPc2	svorník
a	a	k8xC	a
tympanonů	tympanon	k1gInPc2	tympanon
a	a	k8xC	a
také	také	k9	také
se	se	k3xPyFc4	se
výrazněji	výrazně	k6eAd2	výrazně
prosadil	prosadit	k5eAaPmAgInS	prosadit
požadavek	požadavek	k1gInSc1	požadavek
mocných	mocný	k2eAgMnPc2d1	mocný
zakladatelů	zakladatel	k1gMnPc2	zakladatel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
kláštery	klášter	k1gInPc1	klášter
staly	stát	k5eAaPmAgInP	stát
jejich	jejich	k3xOp3gInSc6	jejich
pohřebišti	pohřebiště	k1gNnSc6	pohřebiště
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
cisterciáckým	cisterciácký	k2eAgFnPc3d1	cisterciácká
stavebním	stavební	k2eAgFnPc3d1	stavební
hutím	huť	k1gFnPc3	huť
se	se	k3xPyFc4	se
gotický	gotický	k2eAgInSc4d1	gotický
sloh	sloh	k1gInSc4	sloh
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
severní	severní	k2eAgMnSc1d1	severní
a	a	k8xC	a
střední	střední	k2eAgFnSc7d1	střední
Evropou	Evropa	k1gFnSc7	Evropa
<g/>
.	.	kIx.	.
<g/>
Konventní	konventní	k2eAgInSc1d1	konventní
chrám	chrám	k1gInSc1	chrám
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
na	na	k7c6	na
Zbraslavi	Zbraslav	k1gFnSc6	Zbraslav
sice	sice	k8xC	sice
zanikl	zaniknout	k5eAaPmAgInS	zaniknout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
dochované	dochovaný	k2eAgFnSc2d1	dochovaná
kopie	kopie	k1gFnSc2	kopie
barokního	barokní	k2eAgInSc2d1	barokní
plánku	plánek	k1gInSc2	plánek
zachycujícího	zachycující	k2eAgInSc2d1	zachycující
chrám	chrám	k1gInSc1	chrám
zřejmě	zřejmě	k6eAd1	zřejmě
v	v	k7c6	v
ideálním	ideální	k2eAgInSc6d1	ideální
stavu	stav	k1gInSc6	stav
po	po	k7c6	po
gotické	gotický	k2eAgFnSc6d1	gotická
dostavbě	dostavba	k1gFnSc6	dostavba
<g/>
,	,	kIx,	,
nalezené	nalezený	k2eAgFnSc6d1	nalezená
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
v	v	k7c6	v
báni	báně	k1gFnSc6	báně
kostela	kostel	k1gInSc2	kostel
v	v	k7c6	v
Horních	horní	k2eAgInPc6d1	horní
Mokropsech	Mokropsy	k1gInPc6	Mokropsy
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
získat	získat	k5eAaPmF	získat
představu	představa	k1gFnSc4	představa
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
vzhledu	vzhled	k1gInSc6	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
Rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
kostela	kostel	k1gInSc2	kostel
v	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
zčásti	zčásti	k6eAd1	zčásti
umožnily	umožnit	k5eAaPmAgInP	umožnit
archeologické	archeologický	k2eAgInPc1d1	archeologický
výzkumy	výzkum	k1gInPc1	výzkum
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1984	[number]	k4	1984
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
a	a	k8xC	a
geofyzikální	geofyzikální	k2eAgInSc1d1	geofyzikální
průzkum	průzkum	k1gInSc1	průzkum
základového	základový	k2eAgNnSc2d1	základové
zdiva	zdivo	k1gNnSc2	zdivo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
byly	být	k5eAaImAgInP	být
iniciovány	iniciovat	k5eAaBmNgInP	iniciovat
přestavbou	přestavba	k1gFnSc7	přestavba
inženýrských	inženýrský	k2eAgFnPc2d1	inženýrská
sítí	síť	k1gFnPc2	síť
zbraslavského	zbraslavský	k2eAgInSc2d1	zbraslavský
areálu	areál	k1gInSc2	areál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Prokázaly	prokázat	k5eAaPmAgInP	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
bazilika	bazilika	k1gFnSc1	bazilika
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
104	[number]	k4	104
metry	metr	k1gInPc4	metr
byla	být	k5eAaImAgFnS	být
až	až	k9	až
do	do	k7c2	do
postavení	postavení	k1gNnSc2	postavení
pražské	pražský	k2eAgFnSc2d1	Pražská
katedrály	katedrála	k1gFnSc2	katedrála
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
největším	veliký	k2eAgMnSc7d3	veliký
českým	český	k2eAgInSc7d1	český
středověkým	středověký	k2eAgInSc7d1	středověký
kostelem	kostel	k1gInSc7	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Chrám	chrám	k1gInSc1	chrám
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
na	na	k7c6	na
obvyklém	obvyklý	k2eAgInSc6d1	obvyklý
křížovém	křížový	k2eAgInSc6d1	křížový
půdorysu	půdorys	k1gInSc6	půdorys
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
druhým	druhý	k4xOgInSc7	druhý
českým	český	k2eAgInSc7d1	český
příkladem	příklad	k1gInSc7	příklad
pravoúhlého	pravoúhlý	k2eAgMnSc2d1	pravoúhlý
presbyteráře	presbyterář	k1gMnSc2	presbyterář
podle	podle	k7c2	podle
burgundského	burgundský	k2eAgInSc2d1	burgundský
vzoru	vzor	k1gInSc2	vzor
sídla	sídlo	k1gNnSc2	sídlo
generální	generální	k2eAgFnSc2d1	generální
kapituly	kapitula	k1gFnSc2	kapitula
v	v	k7c4	v
Citeaux	Citeaux	k1gInSc4	Citeaux
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
trojlodní	trojlodní	k2eAgFnSc4d1	trojlodní
baziliku	bazilika	k1gFnSc4	bazilika
s	s	k7c7	s
pravoúhlým	pravoúhlý	k2eAgInSc7d1	pravoúhlý
síňovým	síňový	k2eAgInSc7d1	síňový
chórem	chór	k1gInSc7	chór
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
střední	střední	k2eAgFnSc1d1	střední
část	část	k1gFnSc1	část
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
východě	východ	k1gInSc6	východ
a	a	k8xC	a
jihu	jih	k1gInSc6	jih
obklopena	obklopit	k5eAaPmNgFnS	obklopit
ochozem	ochoz	k1gInSc7	ochoz
<g/>
,	,	kIx,	,
do	do	k7c2	do
něhož	jenž	k3xRgInSc2	jenž
ústily	ústit	k5eAaImAgFnP	ústit
polygonální	polygonální	k2eAgFnPc1d1	polygonální
kaple	kaple	k1gFnPc1	kaple
zvenku	zvenku	k6eAd1	zvenku
vypadající	vypadající	k2eAgFnPc1d1	vypadající
jako	jako	k8xC	jako
monolitický	monolitický	k2eAgInSc1d1	monolitický
blok	blok	k1gInSc1	blok
obvodové	obvodový	k2eAgFnSc2d1	obvodová
zdi	zeď	k1gFnSc2	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
a	a	k8xC	a
severu	sever	k1gInSc6	sever
bylo	být	k5eAaImAgNnS	být
po	po	k7c6	po
třech	tři	k4xCgFnPc6	tři
kaplích	kaple	k1gFnPc6	kaple
<g/>
,	,	kIx,	,
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
východní	východní	k2eAgFnSc2d1	východní
kaple	kaple	k1gFnSc2	kaple
čtyři	čtyři	k4xCgMnPc4	čtyři
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgNnPc4	dva
ramena	rameno	k1gNnPc4	rameno
transeptu	transept	k1gInSc2	transept
tvořila	tvořit	k5eAaImAgFnS	tvořit
společně	společně	k6eAd1	společně
s	s	k7c7	s
presbytářem	presbytář	k1gInSc7	presbytář
rovnoramenný	rovnoramenný	k2eAgInSc1d1	rovnoramenný
kříž	kříž	k1gInSc1	kříž
<g/>
,	,	kIx,	,
chór	chór	k1gInSc1	chór
byl	být	k5eAaImAgInS	být
rozvinut	rozvinout	k5eAaPmNgInS	rozvinout
v	v	k7c4	v
monumentální	monumentální	k2eAgInSc4d1	monumentální
pravoúhlý	pravoúhlý	k2eAgInSc4d1	pravoúhlý
katedrální	katedrální	k2eAgInSc4d1	katedrální
závěr	závěr	k1gInSc4	závěr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jižním	jižní	k2eAgNnSc6d1	jižní
průčelí	průčelí	k1gNnSc6	průčelí
lodi	loď	k1gFnSc2	loď
byl	být	k5eAaImAgInS	být
portál	portál	k1gInSc1	portál
s	s	k7c7	s
archivoltou	archivolta	k1gFnSc7	archivolta
osázenou	osázený	k2eAgFnSc7d1	osázená
kraby	krab	k1gInPc1	krab
<g/>
,	,	kIx,	,
po	po	k7c6	po
stranách	strana	k1gFnPc6	strana
zdobený	zdobený	k2eAgInSc1d1	zdobený
fiálami	fiála	k1gFnPc7	fiála
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nad	nad	k7c7	nad
portálem	portál	k1gInSc7	portál
bylo	být	k5eAaImAgNnS	být
osazeno	osazen	k2eAgNnSc1d1	osazeno
okno	okno	k1gNnSc1	okno
a	a	k8xC	a
fiály	fiála	k1gFnPc1	fiála
se	se	k3xPyFc4	se
objevovaly	objevovat	k5eAaImAgFnP	objevovat
také	také	k9	také
na	na	k7c6	na
rohových	rohový	k2eAgInPc6d1	rohový
opěrných	opěrný	k2eAgInPc6d1	opěrný
pilířích	pilíř	k1gInPc6	pilíř
transeptu	transept	k1gInSc2	transept
<g/>
.	.	kIx.	.
</s>
<s>
Lomená	lomený	k2eAgNnPc1d1	lomené
okna	okno	k1gNnPc1	okno
baziliky	bazilika	k1gFnSc2	bazilika
se	se	k3xPyFc4	se
střídala	střídat	k5eAaImAgFnS	střídat
s	s	k7c7	s
opěrnými	opěrný	k2eAgInPc7d1	opěrný
pilíři	pilíř	k1gInPc7	pilíř
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
průčelí	průčelí	k1gNnSc6	průčelí
chrámu	chrám	k1gInSc2	chrám
byl	být	k5eAaImAgInS	být
opět	opět	k6eAd1	opět
portál	portál	k1gInSc1	portál
s	s	k7c7	s
vimperkem	vimperk	k1gInSc7	vimperk
a	a	k8xC	a
fiálami	fiála	k1gFnPc7	fiála
<g/>
.	.	kIx.	.
</s>
<s>
Rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
křížovou	křížový	k2eAgFnSc4d1	křížová
chodbu	chodba	k1gFnSc4	chodba
na	na	k7c6	na
severu	sever	k1gInSc6	sever
doplňoval	doplňovat	k5eAaImAgMnS	doplňovat
refektář	refektář	k1gInSc4	refektář
s	s	k7c7	s
pěti	pět	k4xCc7	pět
křížovými	křížový	k2eAgNnPc7d1	křížové
poli	pole	k1gNnPc7	pole
<g/>
.	.	kIx.	.
</s>
<s>
Nedochovaná	dochovaný	k2eNgFnSc1d1	nedochovaná
budova	budova	k1gFnSc1	budova
konventu	konvent	k1gInSc2	konvent
přiléhala	přiléhat	k5eAaImAgFnS	přiléhat
k	k	k7c3	k
severní	severní	k2eAgFnSc3d1	severní
straně	strana	k1gFnSc3	strana
kostela	kostel	k1gInSc2	kostel
a	a	k8xC	a
ze	z	k7c2	z
starších	starý	k2eAgFnPc2d2	starší
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
určovaly	určovat	k5eAaImAgInP	určovat
stavební	stavební	k2eAgFnSc4d1	stavební
podobu	podoba	k1gFnSc4	podoba
klášterního	klášterní	k2eAgInSc2d1	klášterní
areálu	areál	k1gInSc2	areál
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
přestavěn	přestavěn	k2eAgInSc1d1	přestavěn
patrový	patrový	k2eAgInSc1d1	patrový
opatský	opatský	k2eAgInSc1d1	opatský
příbytek	příbytek	k1gInSc1	příbytek
a	a	k8xC	a
prelatura	prelatura	k1gFnSc1	prelatura
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
vzhledu	vzhled	k1gInSc6	vzhled
panovníkova	panovníkův	k2eAgNnSc2d1	panovníkovo
sídla	sídlo	k1gNnSc2	sídlo
při	při	k7c6	při
pobytu	pobyt	k1gInSc6	pobyt
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
komorního	komorní	k2eAgInSc2d1	komorní
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zprávy	zpráva	k1gFnPc1	zpráva
nedochovaly	dochovat	k5eNaPmAgFnP	dochovat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
východu	východ	k1gInSc2	východ
ke	k	k7c3	k
kostelu	kostel	k1gInSc3	kostel
přiléhal	přiléhat	k5eAaImAgMnS	přiléhat
hřbitov	hřbitov	k1gInSc4	hřbitov
se	s	k7c7	s
sloupem	sloup	k1gInSc7	sloup
s	s	k7c7	s
věčným	věčný	k2eAgNnSc7d1	věčné
světlem	světlo	k1gNnSc7	světlo
zv	zv	k?	zv
<g/>
.	.	kIx.	.
</s>
<s>
Columna	Columna	k6eAd1	Columna
mortuorum	mortuorum	k1gNnSc4	mortuorum
<g/>
.	.	kIx.	.
<g/>
Svou	svůj	k3xOyFgFnSc4	svůj
téměř	téměř	k6eAd1	téměř
stometrovou	stometrový	k2eAgFnSc7d1	stometrová
délkou	délka	k1gFnSc7	délka
patřil	patřit	k5eAaImAgInS	patřit
chrám	chrám	k1gInSc1	chrám
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgFnPc4d3	veliký
stavby	stavba	k1gFnPc4	stavba
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
navázaly	navázat	k5eAaPmAgFnP	navázat
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
řádové	řádový	k2eAgInPc1d1	řádový
kostely	kostel	k1gInPc1	kostel
ve	v	k7c6	v
slezském	slezský	k2eAgInSc6d1	slezský
Kamenci	Kamenec	k1gInSc6	Kamenec
a	a	k8xC	a
Lubuši	Lubuše	k1gFnSc6	Lubuše
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
z	z	k7c2	z
lomového	lomový	k2eAgInSc2d1	lomový
vápence	vápenec	k1gInSc2	vápenec
a	a	k8xC	a
složitější	složitý	k2eAgInPc4d2	složitější
kamenické	kamenický	k2eAgInPc4d1	kamenický
kusy	kus	k1gInPc4	kus
z	z	k7c2	z
pískovce	pískovec	k1gInSc2	pískovec
<g/>
,	,	kIx,	,
opuky	opuka	k1gFnSc2	opuka
a	a	k8xC	a
terakoty	terakota	k1gFnSc2	terakota
<g/>
.	.	kIx.	.
</s>
<s>
Klášterní	klášterní	k2eAgFnPc1d1	klášterní
budovy	budova	k1gFnPc1	budova
byly	být	k5eAaImAgFnP	být
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
cihel	cihla	k1gFnPc2	cihla
<g/>
.	.	kIx.	.
<g/>
Dnešní	dnešní	k2eAgFnSc1d1	dnešní
podoba	podoba	k1gFnSc1	podoba
kláštera	klášter	k1gInSc2	klášter
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
barokní	barokní	k2eAgFnSc2d1	barokní
přestavby	přestavba	k1gFnSc2	přestavba
z	z	k7c2	z
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
křídle	křídlo	k1gNnSc6	křídlo
je	být	k5eAaImIp3nS	být
refektář	refektář	k1gInSc1	refektář
s	s	k7c7	s
freskovou	freskový	k2eAgFnSc7d1	fresková
výzdobou	výzdoba	k1gFnSc7	výzdoba
F.	F.	kA	F.
X.	X.	kA	X.
Palka	Palka	k1gMnSc1	Palka
a	a	k8xC	a
v	v	k7c6	v
patře	patro	k1gNnSc6	patro
je	být	k5eAaImIp3nS	být
Královský	královský	k2eAgInSc1d1	královský
sál	sál	k1gInSc1	sál
se	s	k7c7	s
štukovou	štukový	k2eAgFnSc7d1	štuková
výzdobou	výzdoba	k1gFnSc7	výzdoba
T.	T.	kA	T.
a	a	k8xC	a
M.	M.	kA	M.
Soldattiů	Soldatti	k1gMnPc2	Soldatti
a	a	k8xC	a
s	s	k7c7	s
freskami	freska	k1gFnPc7	freska
V.	V.	kA	V.
V.	V.	kA	V.
Reinera	Reiner	k1gMnSc2	Reiner
<g/>
.	.	kIx.	.
</s>
<s>
Zbytky	zbytek	k1gInPc1	zbytek
středověkého	středověký	k2eAgNnSc2d1	středověké
zdiva	zdivo	k1gNnSc2	zdivo
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
v	v	k7c6	v
západním	západní	k2eAgNnSc6d1	západní
a	a	k8xC	a
severním	severní	k2eAgNnSc6d1	severní
křídle	křídlo	k1gNnSc6	křídlo
bývalé	bývalý	k2eAgFnSc2d1	bývalá
prelatury	prelatura	k1gFnSc2	prelatura
a	a	k8xC	a
ve	v	k7c6	v
farním	farní	k2eAgInSc6d1	farní
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Jakuba	Jakub	k1gMnSc2	Jakub
Většího	veliký	k2eAgMnSc2d2	veliký
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
barokizován	barokizovat	k5eAaImNgInS	barokizovat
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Gotická	gotický	k2eAgFnSc1d1	gotická
je	být	k5eAaImIp3nS	být
také	také	k9	také
hranolová	hranolový	k2eAgFnSc1d1	hranolová
věž	věž	k1gFnSc1	věž
s	s	k7c7	s
portálem	portál	k1gInSc7	portál
<g/>
.	.	kIx.	.
</s>
<s>
Kněžiště	kněžiště	k1gNnSc1	kněžiště
chrámu	chrám	k1gInSc2	chrám
a	a	k8xC	a
hrobka	hrobka	k1gFnSc1	hrobka
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
stojí	stát	k5eAaImIp3nS	stát
socha	socha	k1gFnSc1	socha
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
zámku	zámek	k1gInSc2	zámek
je	být	k5eAaImIp3nS	být
bývalým	bývalý	k2eAgInSc7d1	bývalý
konventem	konvent	k1gInSc7	konvent
a	a	k8xC	a
zámecká	zámecký	k2eAgFnSc1d1	zámecká
věž	věž	k1gFnSc1	věž
je	být	k5eAaImIp3nS	být
vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
prelatury	prelatura	k1gFnSc2	prelatura
a	a	k8xC	a
do	do	k7c2	do
původně	původně	k6eAd1	původně
loveckého	lovecký	k2eAgInSc2d1	lovecký
dvorce	dvorec	k1gInSc2	dvorec
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Barokní	barokní	k2eAgFnSc1d1	barokní
přestavba	přestavba	k1gFnSc1	přestavba
kláštera	klášter	k1gInSc2	klášter
===	===	k?	===
</s>
</p>
<p>
<s>
O	o	k7c4	o
vrcholně	vrcholně	k6eAd1	vrcholně
barokní	barokní	k2eAgFnSc4d1	barokní
přestavbu	přestavba	k1gFnSc4	přestavba
kláštera	klášter	k1gInSc2	klášter
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
si	se	k3xPyFc3	se
klášter	klášter	k1gInSc1	klášter
uchoval	uchovat	k5eAaPmAgInS	uchovat
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
zasadili	zasadit	k5eAaPmAgMnP	zasadit
zejména	zejména	k9	zejména
tehdejší	tehdejší	k2eAgMnPc1d1	tehdejší
zbraslavští	zbraslavský	k2eAgMnPc1d1	zbraslavský
opati	opat	k1gMnPc1	opat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
především	především	k6eAd1	především
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Lochner	Lochner	k1gMnSc1	Lochner
(	(	kIx(	(
<g/>
1684	[number]	k4	1684
<g/>
–	–	k?	–
<g/>
1716	[number]	k4	1716
<g/>
)	)	kIx)	)
a	a	k8xC	a
Tomas	Tomas	k1gMnSc1	Tomas
Budecius	Budecius	k1gMnSc1	Budecius
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
1700	[number]	k4	1700
<g/>
–	–	k?	–
<g/>
32	[number]	k4	32
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
účelem	účel	k1gInSc7	účel
byl	být	k5eAaImAgInS	být
povolán	povolat	k5eAaPmNgInS	povolat
již	již	k9	již
zmíněný	zmíněný	k2eAgMnSc1d1	zmíněný
architekt	architekt	k1gMnSc1	architekt
J.	J.	kA	J.
B.	B.	kA	B.
Santini-Aichel	Santini-Aichel	k1gMnSc1	Santini-Aichel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
opírat	opírat	k5eAaImF	opírat
o	o	k7c4	o
písemný	písemný	k2eAgInSc4d1	písemný
materiál	materiál	k1gInSc4	materiál
přítomnosti	přítomnost	k1gFnSc2	přítomnost
a	a	k8xC	a
autorství	autorství	k1gNnSc2	autorství
Santiniho	Santini	k1gMnSc2	Santini
přestavby	přestavba	k1gFnSc2	přestavba
<g/>
,	,	kIx,	,
či	či	k8xC	či
spíše	spíše	k9	spíše
novostavby	novostavba	k1gFnSc2	novostavba
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
duchu	duch	k1gMnSc6	duch
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
běžné	běžný	k2eAgFnSc2d1	běžná
praxe	praxe	k1gFnSc2	praxe
barokní	barokní	k2eAgFnSc2d1	barokní
výstavby	výstavba	k1gFnSc2	výstavba
velkých	velký	k2eAgInPc2d1	velký
objektů	objekt	k1gInPc2	objekt
nebyla	být	k5eNaImAgFnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
stavební	stavební	k2eAgFnSc1d1	stavební
činnost	činnost	k1gFnSc1	činnost
na	na	k7c6	na
Zbraslavi	Zbraslav	k1gFnSc6	Zbraslav
zahájena	zahájit	k5eAaPmNgFnS	zahájit
naráz	naráz	k6eAd1	naráz
a	a	k8xC	a
v	v	k7c6	v
plném	plný	k2eAgInSc6d1	plný
rozsahu	rozsah	k1gInSc6	rozsah
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stavěna	stavěn	k2eAgFnSc1d1	stavěna
po	po	k7c6	po
menších	malý	k2eAgFnPc6d2	menší
částech	část	k1gFnPc6	část
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
později	pozdě	k6eAd2	pozdě
samostatně	samostatně	k6eAd1	samostatně
ukončeny	ukončit	k5eAaPmNgFnP	ukončit
a	a	k8xC	a
předány	předat	k5eAaPmNgFnP	předat
k	k	k7c3	k
užívání	užívání	k1gNnSc3	užívání
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
v	v	k7c6	v
Plasích	Plasy	k1gInPc6	Plasy
<g/>
.	.	kIx.	.
<g/>
Stavba	stavba	k1gFnSc1	stavba
začala	začít	k5eAaPmAgFnS	začít
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1705	[number]	k4	1705
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
fázi	fáze	k1gFnSc6	fáze
stavební	stavební	k2eAgFnSc2d1	stavební
činnosti	činnost	k1gFnSc2	činnost
na	na	k7c6	na
klášterním	klášterní	k2eAgInSc6d1	klášterní
komplexu	komplex	k1gInSc6	komplex
byla	být	k5eAaImAgFnS	být
prováděna	prováděn	k2eAgFnSc1d1	prováděna
spíše	spíše	k9	spíše
asanace	asanace	k1gFnSc1	asanace
středověkých	středověký	k2eAgFnPc2d1	středověká
klášterních	klášterní	k2eAgFnPc2d1	klášterní
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Projektem	projekt	k1gInSc7	projekt
nového	nový	k2eAgInSc2d1	nový
komplexu	komplex	k1gInSc2	komplex
se	se	k3xPyFc4	se
Santini	Santin	k2eAgMnPc1d1	Santin
zabýval	zabývat	k5eAaImAgInS	zabývat
zřejmě	zřejmě	k6eAd1	zřejmě
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
až	až	k9	až
s	s	k7c7	s
nástupem	nástup	k1gInSc7	nástup
dalšího	další	k2eAgMnSc2d1	další
opata	opat	k1gMnSc2	opat
Budecia	Budecium	k1gNnSc2	Budecium
roku	rok	k1gInSc2	rok
1716	[number]	k4	1716
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1723	[number]	k4	1723
byly	být	k5eAaImAgInP	být
zhruba	zhruba	k6eAd1	zhruba
hotovy	hotov	k2eAgInPc4d1	hotov
přízemí	přízemí	k1gNnSc3	přízemí
všech	všecek	k3xTgNnPc2	všecek
křídel	křídlo	k1gNnPc2	křídlo
<g/>
,	,	kIx,	,
zřejmě	zřejmě	k6eAd1	zřejmě
částečně	částečně	k6eAd1	částečně
i	i	k9	i
obvodové	obvodový	k2eAgFnSc3d1	obvodová
zdi	zeď	k1gFnSc3	zeď
patra	patro	k1gNnSc2	patro
a	a	k8xC	a
terasa	terasa	k1gFnSc1	terasa
vybíhající	vybíhající	k2eAgFnSc1d1	vybíhající
k	k	k7c3	k
řece	řeka	k1gFnSc3	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Santiniho	Santinize	k6eAd1	Santinize
koncepce	koncepce	k1gFnSc1	koncepce
uvažovala	uvažovat	k5eAaImAgFnS	uvažovat
uzavření	uzavření	k1gNnSc4	uzavření
pravoúhlého	pravoúhlý	k2eAgInSc2d1	pravoúhlý
dvoru	dvůr	k1gInSc6	dvůr
konventu	konvent	k1gInSc2	konvent
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
strany	strana	k1gFnSc2	strana
klášterním	klášterní	k2eAgInSc7d1	klášterní
chrámem	chrám	k1gInSc7	chrám
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
ani	ani	k8xC	ani
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
v	v	k7c6	v
Plasích	Plasy	k1gInPc6	Plasy
<g/>
,	,	kIx,	,
k	k	k7c3	k
realizaci	realizace	k1gFnSc3	realizace
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
poznatku	poznatek	k1gInSc2	poznatek
ze	z	k7c2	z
stavebně-historického	stavebněistorický	k2eAgInSc2d1	stavebně-historický
průzkumu	průzkum	k1gInSc2	průzkum
zapojil	zapojit	k5eAaPmAgInS	zapojit
Santini	Santin	k2eAgMnPc1d1	Santin
starší	starší	k1gMnPc1	starší
zástavbu	zástavba	k1gFnSc4	zástavba
kláštera	klášter	k1gInSc2	klášter
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
nové	nový	k2eAgFnSc2d1	nová
barokní	barokní	k2eAgFnSc2d1	barokní
kompozice	kompozice	k1gFnSc2	kompozice
<g/>
,	,	kIx,	,
především	především	k9	především
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
křídle	křídlo	k1gNnSc6	křídlo
využil	využít	k5eAaPmAgInS	využít
částečně	částečně	k6eAd1	částečně
románské	románský	k2eAgNnSc4d1	románské
zdivo	zdivo	k1gNnSc4	zdivo
z	z	k7c2	z
benediktinského	benediktinský	k2eAgNnSc2d1	benediktinské
proboštství	proboštství	k1gNnSc2	proboštství
a	a	k8xC	a
vrcholně	vrcholně	k6eAd1	vrcholně
gotické	gotický	k2eAgFnPc1d1	gotická
hmoty	hmota	k1gFnPc1	hmota
z	z	k7c2	z
cisterciáckého	cisterciácký	k2eAgNnSc2d1	cisterciácké
opatství	opatství	k1gNnSc2	opatství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Základním	základní	k2eAgInSc7d1	základní
půdorysem	půdorys	k1gInSc7	půdorys
konventu	konvent	k1gInSc2	konvent
je	být	k5eAaImIp3nS	být
trojkřídlá	trojkřídlý	k2eAgFnSc1d1	trojkřídlá
stavba	stavba	k1gFnSc1	stavba
na	na	k7c6	na
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
straně	strana	k1gFnSc6	strana
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
chrámem	chrám	k1gInSc7	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Vzájemnou	vzájemný	k2eAgFnSc4d1	vzájemná
půdorysnou	půdorysný	k2eAgFnSc4d1	půdorysná
návaznost	návaznost	k1gFnSc4	návaznost
chrámu	chrám	k1gInSc2	chrám
a	a	k8xC	a
konventu	konvent	k1gInSc2	konvent
"	"	kIx"	"
<g/>
pojal	pojmout	k5eAaPmAgMnS	pojmout
architekt	architekt	k1gMnSc1	architekt
jako	jako	k8xC	jako
vztah	vztah	k1gInSc1	vztah
gradovaný	gradovaný	k2eAgInSc1d1	gradovaný
objemově	objemově	k6eAd1	objemově
i	i	k9	i
obrysně	obrysně	k6eAd1	obrysně
<g/>
.	.	kIx.	.
</s>
<s>
Bohatý	bohatý	k2eAgInSc1d1	bohatý
pavilonovitě	pavilonovitě	k6eAd1	pavilonovitě
členěný	členěný	k2eAgInSc1d1	členěný
objekt	objekt	k1gInSc1	objekt
konventu	konvent	k1gInSc2	konvent
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
při	při	k7c6	při
dálkových	dálkový	k2eAgInPc6d1	dálkový
pohledech	pohled	k1gInPc6	pohled
od	od	k7c2	od
severu	sever	k1gInSc2	sever
rytmicky	rytmicky	k6eAd1	rytmicky
bohatou	bohatý	k2eAgFnSc7d1	bohatá
kompoziční	kompoziční	k2eAgFnSc7d1	kompoziční
podnoží	podnož	k1gFnSc7	podnož
monumentálního	monumentální	k2eAgInSc2d1	monumentální
konventního	konventní	k2eAgInSc2d1	konventní
chrámu	chrám	k1gInSc2	chrám
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
interiéru	interiér	k1gInSc6	interiér
konventu	konvent	k1gInSc2	konvent
je	být	k5eAaImIp3nS	být
originálně	originálně	k6eAd1	originálně
řešené	řešený	k2eAgNnSc1d1	řešené
trojtraktově	trojtraktově	k6eAd1	trojtraktově
založené	založený	k2eAgNnSc1d1	založené
přízemí	přízemí	k1gNnSc1	přízemí
<g/>
,	,	kIx,	,
dané	daný	k2eAgNnSc1d1	dané
vysunutím	vysunutí	k1gNnSc7	vysunutí
chodby	chodba	k1gFnSc2	chodba
ambitu	ambit	k1gInSc2	ambit
do	do	k7c2	do
nižšího	nízký	k2eAgInSc2d2	nižší
vnějšího	vnější	k2eAgInSc2d1	vnější
traktu	trakt	k1gInSc2	trakt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
obíhá	obíhat	k5eAaImIp3nS	obíhat
rajský	rajský	k2eAgInSc1d1	rajský
dvůr	dvůr	k1gInSc1	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
řešení	řešení	k1gNnSc1	řešení
přestavuje	přestavovat	k5eAaImIp3nS	přestavovat
návaznost	návaznost	k1gFnSc4	návaznost
na	na	k7c4	na
středověké	středověký	k2eAgFnPc4d1	středověká
stavební	stavební	k2eAgFnPc4d1	stavební
zvyklosti	zvyklost	k1gFnPc4	zvyklost
<g/>
,	,	kIx,	,
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
bylo	být	k5eAaImAgNnS	být
takto	takto	k6eAd1	takto
autorem	autor	k1gMnSc7	autor
užito	užít	k5eAaPmNgNnS	užít
záměrně	záměrně	k6eAd1	záměrně
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
tezi	teze	k1gFnSc3	teze
o	o	k7c6	o
záměrné	záměrný	k2eAgFnSc6d1	záměrná
středověké	středověký	k2eAgFnSc6d1	středověká
aluzi	aluze	k1gFnSc6	aluze
stavební	stavební	k2eAgFnSc2d1	stavební
kompozice	kompozice	k1gFnSc2	kompozice
by	by	kYmCp3nP	by
zřejmě	zřejmě	k6eAd1	zřejmě
odpovídaly	odpovídat	k5eAaImAgFnP	odpovídat
i	i	k9	i
morfologické	morfologický	k2eAgInPc4d1	morfologický
gotismy	gotismus	k1gInPc4	gotismus
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
pilířů	pilíř	k1gInPc2	pilíř
ambitu	ambit	k1gInSc2	ambit
<g/>
,	,	kIx,	,
trojlistých	trojlistý	k2eAgInPc2d1	trojlistý
záklenků	záklenek	k1gInPc2	záklenek
jeho	jeho	k3xOp3gNnPc2	jeho
oken	okno	k1gNnPc2	okno
i	i	k8xC	i
nevšední	všední	k2eNgFnSc2d1	nevšední
rytmizace	rytmizace	k1gFnSc2	rytmizace
kleneb	klenba	k1gFnPc2	klenba
střídání	střídání	k1gNnSc1	střídání
dvou	dva	k4xCgMnPc6	dva
druhu	druh	k1gInSc2	druh
konstrukci	konstrukce	k1gFnSc4	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
chodba	chodba	k1gFnSc1	chodba
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
proti	proti	k7c3	proti
vysokému	vysoký	k2eAgInSc3d1	vysoký
ochozu	ochoz	k1gInSc3	ochoz
při	při	k7c6	při
rajském	rajský	k2eAgInSc6d1	rajský
dvoře	dvůr	k1gInSc6	dvůr
dvoupodlažní	dvoupodlažní	k2eAgNnSc1d1	dvoupodlažní
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
spojuje	spojovat	k5eAaImIp3nS	spojovat
příčně	příčně	k6eAd1	příčně
severní	severní	k2eAgNnSc1d1	severní
křídlo	křídlo	k1gNnSc1	křídlo
s	s	k7c7	s
východním	východní	k2eAgNnSc7d1	východní
a	a	k8xC	a
západním	západní	k2eAgNnSc7d1	západní
ramenem	rameno	k1gNnSc7	rameno
s	s	k7c7	s
celami	cela	k1gFnPc7	cela
ve	v	k7c6	v
dvou	dva	k4xCgNnPc6	dva
poschodích	poschodí	k1gNnPc6	poschodí
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	k9	aby
podlaha	podlaha	k1gFnSc1	podlaha
prostřední	prostřední	k2eAgFnSc2d1	prostřední
chodby	chodba	k1gFnSc2	chodba
nezasahovala	zasahovat	k5eNaImAgNnP	zasahovat
do	do	k7c2	do
vysokých	vysoký	k2eAgFnPc2d1	vysoká
oken	okno	k1gNnPc2	okno
<g/>
,	,	kIx,	,
vyřízl	vyříznout	k5eAaPmAgMnS	vyříznout
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
Santini	Santin	k2eAgMnPc1d1	Santin
trojlisté	trojlistý	k2eAgInPc1d1	trojlistý
otvory	otvor	k1gInPc1	otvor
se	s	k7c7	s
zábradlím	zábradlí	k1gNnSc7	zábradlí
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
v	v	k7c6	v
podlahách	podlaha	k1gFnPc6	podlaha
empor	empora	k1gFnPc2	empora
želivského	želivský	k2eAgInSc2d1	želivský
klášterního	klášterní	k2eAgInSc2d1	klášterní
kostela	kostel	k1gInSc2	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
inovace	inovace	k1gFnSc1	inovace
mu	on	k3xPp3gMnSc3	on
zřejmě	zřejmě	k6eAd1	zřejmě
zajistila	zajistit	k5eAaPmAgFnS	zajistit
primát	primát	k1gInSc4	primát
v	v	k7c6	v
barokní	barokní	k2eAgFnSc6d1	barokní
evropské	evropský	k2eAgFnSc6d1	Evropská
architektuře	architektura	k1gFnSc6	architektura
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
obdobné	obdobný	k2eAgNnSc4d1	obdobné
řešení	řešení	k1gNnSc4	řešení
použil	použít	k5eAaPmAgInS	použít
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1718	[number]	k4	1718
v	v	k7c6	v
Turíně	Turín	k1gInSc6	Turín
ve	v	k7c6	v
vestibulu	vestibul	k1gInSc6	vestibul
paláce	palác	k1gInSc2	palác
Madama	Madamum	k1gNnSc2	Madamum
architekt	architekt	k1gMnSc1	architekt
Juvarra	Juvarra	k1gMnSc1	Juvarra
<g/>
.	.	kIx.	.
</s>
<s>
Interiér	interiér	k1gInSc1	interiér
ochozové	ochozový	k2eAgFnSc2d1	ochozová
chodby	chodba	k1gFnSc2	chodba
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
prostorových	prostorový	k2eAgFnPc2d1	prostorová
buněk	buňka	k1gFnPc2	buňka
zaklenutých	zaklenutý	k2eAgFnPc2d1	zaklenutá
českými	český	k2eAgFnPc7d1	Česká
plackami	placka	k1gFnPc7	placka
a	a	k8xC	a
kupolemi	kupole	k1gFnPc7	kupole
na	na	k7c6	na
pendativech	pendativ	k1gInPc6	pendativ
<g/>
.	.	kIx.	.
</s>
<s>
Pilíře	pilíř	k1gInPc1	pilíř
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
polosloupy	polosloup	k1gInPc4	polosloup
a	a	k8xC	a
podkládané	podkládaný	k2eAgInPc4d1	podkládaný
pilastry	pilastr	k1gInPc4	pilastr
s	s	k7c7	s
románskými	románský	k2eAgFnPc7d1	románská
hlavicemi	hlavice	k1gFnPc7	hlavice
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
s	s	k7c7	s
klenebními	klenební	k2eAgInPc7d1	klenební
pasy	pas	k1gInPc7	pas
zmnožené	zmnožený	k2eAgInPc1d1	zmnožený
triumfální	triumfální	k2eAgInPc1d1	triumfální
oblouky	oblouk	k1gInPc1	oblouk
<g/>
,	,	kIx,	,
připomínající	připomínající	k2eAgFnPc1d1	připomínající
gotické	gotický	k2eAgFnPc4d1	gotická
klenební	klenební	k2eAgFnPc4d1	klenební
konstrukce	konstrukce	k1gFnPc4	konstrukce
a	a	k8xC	a
zastírající	zastírající	k2eAgFnSc4d1	zastírající
prostorovou	prostorový	k2eAgFnSc4d1	prostorová
aditivnost	aditivnost	k1gFnSc4	aditivnost
<g/>
.	.	kIx.	.
<g/>
Dvoulodní	dvoulodní	k2eAgNnPc4d1	dvoulodní
provedení	provedení	k1gNnPc4	provedení
ambitu	ambit	k1gInSc2	ambit
v	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
křídle	křídlo	k1gNnSc6	křídlo
s	s	k7c7	s
vloženými	vložený	k2eAgFnPc7d1	vložená
probíranými	probíraný	k2eAgFnPc7d1	probíraná
emporami	empora	k1gFnPc7	empora
ve	v	k7c6	v
středním	střední	k2eAgInSc6d1	střední
traktu	trakt	k1gInSc6	trakt
a	a	k8xC	a
oboustranná	oboustranný	k2eAgFnSc1d1	oboustranná
orientace	orientace	k1gFnSc1	orientace
hlavního	hlavní	k2eAgNnSc2d1	hlavní
ramene	rameno	k1gNnSc2	rameno
ambitu	ambit	k1gInSc2	ambit
na	na	k7c4	na
prosvětlené	prosvětlený	k2eAgFnPc4d1	prosvětlená
sféry	sféra	k1gFnPc4	sféra
schodišť	schodiště	k1gNnPc2	schodiště
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
a	a	k8xC	a
západní	západní	k2eAgFnSc6d1	západní
straně	strana	k1gFnSc6	strana
je	být	k5eAaImIp3nS	být
projevem	projev	k1gInSc7	projev
mimořádného	mimořádný	k2eAgMnSc2d1	mimořádný
uměleckého	umělecký	k2eAgMnSc2d1	umělecký
génia	génius	k1gMnSc2	génius
a	a	k8xC	a
představuje	představovat	k5eAaImIp3nS	představovat
ukázku	ukázka	k1gFnSc4	ukázka
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
nejkrásnějších	krásný	k2eAgFnPc2d3	nejkrásnější
evropských	evropský	k2eAgFnPc2d1	Evropská
barokních	barokní	k2eAgFnPc2d1	barokní
komunikačních	komunikační	k2eAgFnPc2d1	komunikační
prostor	prostora	k1gFnPc2	prostora
<g/>
.	.	kIx.	.
</s>
<s>
Originálně	originálně	k6eAd1	originálně
řešeny	řešen	k2eAgFnPc1d1	řešena
byly	být	k5eAaImAgFnP	být
také	také	k9	také
slavnostní	slavnostní	k2eAgFnPc4d1	slavnostní
prostory	prostora	k1gFnPc4	prostora
i	i	k9	i
"	"	kIx"	"
<g/>
nová	nový	k2eAgFnSc1d1	nová
<g/>
"	"	kIx"	"
sakristie	sakristie	k1gFnSc1	sakristie
ve	v	k7c6	v
východním	východní	k2eAgNnSc6d1	východní
křídle	křídlo	k1gNnSc6	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
křídle	křídlo	k1gNnSc6	křídlo
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
nad	nad	k7c7	nad
velkým	velký	k2eAgInSc7d1	velký
portálem	portál	k1gInSc7	portál
vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
konventu	konvent	k1gInSc2	konvent
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
architektonickém	architektonický	k2eAgNnSc6d1	architektonické
pojetí	pojetí	k1gNnSc6	pojetí
můžeme	moct	k5eAaImIp1nP	moct
vidět	vidět	k5eAaImF	vidět
zřejmě	zřejmě	k6eAd1	zřejmě
nepochybnou	pochybný	k2eNgFnSc4d1	nepochybná
inspiraci	inspirace	k1gFnSc4	inspirace
vrcholně	vrcholně	k6eAd1	vrcholně
barokními	barokní	k2eAgFnPc7d1	barokní
kompozicemi	kompozice	k1gFnPc7	kompozice
<g/>
,	,	kIx,	,
např.	např.	kA	např.
palácovými	palácový	k2eAgInPc7d1	palácový
nástupy	nástup	k1gInPc7	nástup
Francesca	Francescus	k1gMnSc2	Francescus
Borrominiho	Borromini	k1gMnSc2	Borromini
<g/>
.	.	kIx.	.
</s>
<s>
Nástupní	nástupní	k2eAgFnSc1d1	nástupní
osa	osa	k1gFnSc1	osa
byla	být	k5eAaImAgFnS	být
působivě	působivě	k6eAd1	působivě
konfrontována	konfrontovat	k5eAaBmNgFnS	konfrontovat
s	s	k7c7	s
příčně	příčně	k6eAd1	příčně
orientovanými	orientovaný	k2eAgInPc7d1	orientovaný
prostory	prostor	k1gInPc7	prostor
v	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
traktech	trakt	k1gInPc6	trakt
a	a	k8xC	a
pohledově	pohledově	k6eAd1	pohledově
vyústěna	vyústit	k5eAaPmNgFnS	vyústit
do	do	k7c2	do
centrály	centrála	k1gFnSc2	centrála
dvora	dvůr	k1gInSc2	dvůr
<g/>
.	.	kIx.	.
<g/>
Typický	typický	k2eAgMnSc1d1	typický
Santiniho	Santini	k1gMnSc2	Santini
charakter	charakter	k1gInSc1	charakter
lze	lze	k6eAd1	lze
spatřovat	spatřovat	k5eAaImF	spatřovat
v	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
gotizujícím	gotizující	k2eAgNnSc6d1	gotizující
baroku	baroko	k1gNnSc6	baroko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Zbraslavském	zbraslavský	k2eAgInSc6d1	zbraslavský
konventu	konvent	k1gInSc6	konvent
jde	jít	k5eAaImIp3nS	jít
zejména	zejména	k9	zejména
o	o	k7c4	o
typická	typický	k2eAgNnPc4d1	typické
Santiniho	Santiniha	k1gFnSc5	Santiniha
arkádová	arkádový	k2eAgNnPc1d1	arkádové
okna	okno	k1gNnPc1	okno
s	s	k7c7	s
trojlistými	trojlistý	k2eAgInPc7d1	trojlistý
záklenky	záklenek	k1gInPc7	záklenek
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k1gNnSc1	další
Santiniho	Santini	k1gMnSc2	Santini
charakteristický	charakteristický	k2eAgInSc4d1	charakteristický
rys	rys	k1gInSc4	rys
vidíme	vidět	k5eAaImIp1nP	vidět
na	na	k7c6	na
příkladu	příklad	k1gInSc6	příklad
masivních	masivní	k2eAgInPc2d1	masivní
zdvojených	zdvojený	k2eAgInPc2d1	zdvojený
klenáků	klenák	k1gInPc2	klenák
a	a	k8xC	a
doprovodných	doprovodný	k2eAgInPc6d1	doprovodný
plošných	plošný	k2eAgInPc6d1	plošný
tvarech	tvar	k1gInPc6	tvar
s	s	k7c7	s
triglyfy	triglyf	k1gInPc7	triglyf
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
svědčí	svědčit	k5eAaImIp3nP	svědčit
o	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
malířském	malířský	k2eAgNnSc6d1	malířské
založení	založení	k1gNnSc6	založení
<g/>
.	.	kIx.	.
</s>
<s>
Analogické	analogický	k2eAgFnPc1d1	analogická
okenní	okenní	k2eAgFnPc1d1	okenní
kompozice	kompozice	k1gFnPc1	kompozice
refektáře	refektář	k1gInSc2	refektář
konventu	konvent	k1gInSc2	konvent
ve	v	k7c6	v
Zbraslavi	Zbraslav	k1gFnSc6	Zbraslav
tvořené	tvořený	k2eAgFnSc6d1	tvořená
obdélníkovými	obdélníkový	k2eAgInPc7d1	obdélníkový
a	a	k8xC	a
oválnými	oválný	k2eAgInPc7d1	oválný
tvary	tvar	k1gInPc7	tvar
s	s	k7c7	s
originálními	originální	k2eAgFnPc7d1	originální
šambránami	šambrána	k1gFnPc7	šambrána
lze	lze	k6eAd1	lze
spatřit	spatřit	k5eAaPmF	spatřit
v	v	k7c6	v
benediktinském	benediktinský	k2eAgInSc6d1	benediktinský
klášteře	klášter	k1gInSc6	klášter
v	v	k7c6	v
Melku	Melk	k1gInSc6	Melk
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
vnějších	vnější	k2eAgFnPc2d1	vnější
fasád	fasáda	k1gFnPc2	fasáda
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
povšimnutí	povšimnutí	k1gNnSc4	povšimnutí
průčelí	průčelí	k1gNnSc2	průčelí
k	k	k7c3	k
řece	řeka	k1gFnSc3	řeka
Vltavě	Vltava	k1gFnSc3	Vltava
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Santiniho	Santini	k1gMnSc4	Santini
zvlněný	zvlněný	k2eAgInSc1d1	zvlněný
půdorys	půdorys	k1gInSc1	půdorys
přízemí	přízemí	k1gNnSc2	přízemí
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c6	v
patře	patro	k1gNnSc6	patro
do	do	k7c2	do
klidnějších	klidný	k2eAgFnPc2d2	klidnější
přímých	přímý	k2eAgFnPc2d1	přímá
linií	linie	k1gFnPc2	linie
<g/>
.	.	kIx.	.
</s>
<s>
Severní	severní	k2eAgFnSc1d1	severní
strana	strana	k1gFnSc1	strana
konventu	konvent	k1gInSc2	konvent
hledí	hledět	k5eAaImIp3nS	hledět
k	k	k7c3	k
řece	řeka	k1gFnSc3	řeka
Berounce	Berounka	k1gFnSc3	Berounka
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
křídlo	křídlo	k1gNnSc1	křídlo
je	být	k5eAaImIp3nS	být
komponováno	komponován	k2eAgNnSc1d1	komponováno
dynamičtěji	dynamicky	k6eAd2	dynamicky
-	-	kIx~	-
konvexním	konvexní	k2eAgInSc7d1	konvexní
balkonem	balkon	k1gInSc7	balkon
před	před	k7c7	před
refektářem	refektář	k1gInSc7	refektář
<g/>
.	.	kIx.	.
</s>
<s>
Postranní	postranní	k2eAgInPc4d1	postranní
rizality	rizalit	k1gInPc4	rizalit
zakončují	zakončovat	k5eAaImIp3nP	zakončovat
oblé	oblý	k2eAgInPc4d1	oblý
rohy	roh	k1gInPc4	roh
s	s	k7c7	s
konkávním	konkávní	k2eAgInSc7d1	konkávní
koutem	kout	k1gInSc7	kout
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vybíhá	vybíhat	k5eAaImIp3nS	vybíhat
terasa	terasa	k1gFnSc1	terasa
s	s	k7c7	s
krytým	krytý	k2eAgInSc7d1	krytý
přístupem	přístup	k1gInSc7	přístup
k	k	k7c3	k
Berounce	Berounka	k1gFnSc3	Berounka
<g/>
.	.	kIx.	.
</s>
<s>
Sochy	socha	k1gFnPc1	socha
na	na	k7c6	na
štítě	štít	k1gInSc6	štít
refektáře	refektář	k1gInSc2	refektář
jsou	být	k5eAaImIp3nP	být
dílem	dílo	k1gNnSc7	dílo
Santiniho	Santini	k1gMnSc2	Santini
spolupracovníka	spolupracovník	k1gMnSc2	spolupracovník
Matěje	Matěj	k1gMnSc2	Matěj
Václava	Václav	k1gMnSc2	Václav
Jäckela	Jäckel	k1gMnSc2	Jäckel
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
už	už	k6eAd1	už
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
v	v	k7c6	v
Sedlci	Sedlec	k1gInSc6	Sedlec
a	a	k8xC	a
Žďáru	Žďár	k1gInSc6	Žďár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
a	a	k8xC	a
výtvarné	výtvarný	k2eAgNnSc1d1	výtvarné
umění	umění	k1gNnSc1	umění
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
klášterním	klášterní	k2eAgNnSc6d1	klášterní
ústraní	ústraní	k1gNnSc6	ústraní
na	na	k7c6	na
Zbraslavi	Zbraslav	k1gFnSc6	Zbraslav
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
několik	několik	k4yIc1	několik
významných	významný	k2eAgNnPc2d1	významné
literárních	literární	k2eAgNnPc2d1	literární
děl	dělo	k1gNnPc2	dělo
středověku	středověk	k1gInSc2	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
po	po	k7c6	po
skonu	skon	k1gInSc6	skon
štědrého	štědrý	k2eAgMnSc2d1	štědrý
donátora	donátor	k1gMnSc2	donátor
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
započal	započnout	k5eAaPmAgMnS	započnout
Ota	Ota	k1gMnSc1	Ota
Zbraslavský	zbraslavský	k2eAgMnSc1d1	zbraslavský
se	s	k7c7	s
sepisováním	sepisování	k1gNnSc7	sepisování
latinské	latinský	k2eAgFnSc2d1	Latinská
Zbraslavské	zbraslavský	k2eAgFnSc2d1	Zbraslavská
kroniky	kronika	k1gFnSc2	kronika
<g/>
,	,	kIx,	,
popisující	popisující	k2eAgNnPc4d1	popisující
období	období	k1gNnPc4	období
posledních	poslední	k2eAgMnPc2d1	poslední
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
a	a	k8xC	a
prvních	první	k4xOgInPc2	první
Lucemburků	Lucemburk	k1gInPc2	Lucemburk
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
trůně	trůn	k1gInSc6	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
Petr	Petr	k1gMnSc1	Petr
Žitavský	žitavský	k2eAgMnSc1d1	žitavský
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
popisu	popis	k1gInSc2	popis
dějinných	dějinný	k2eAgFnPc2d1	dějinná
událostí	událost	k1gFnPc2	událost
přesahujících	přesahující	k2eAgFnPc2d1	přesahující
i	i	k9	i
hranice	hranice	k1gFnSc1	hranice
království	království	k1gNnSc2	království
je	být	k5eAaImIp3nS	být
kronika	kronika	k1gFnSc1	kronika
také	také	k6eAd1	také
výkladem	výklad	k1gInSc7	výklad
ideálů	ideál	k1gInPc2	ideál
středověké	středověký	k2eAgFnSc2d1	středověká
sociální	sociální	k2eAgFnSc2d1	sociální
filosofie	filosofie	k1gFnSc2	filosofie
poskytujícím	poskytující	k2eAgInSc7d1	poskytující
návod	návod	k1gInSc4	návod
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
vládnout	vládnout	k5eAaImF	vládnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
obhajujícím	obhajující	k2eAgInSc7d1	obhajující
principy	princip	k1gInPc4	princip
cisterciáckého	cisterciácký	k2eAgInSc2d1	cisterciácký
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
pravděpodobné	pravděpodobný	k2eAgFnSc3d1	pravděpodobná
snaze	snaha	k1gFnSc3	snaha
obou	dva	k4xCgMnPc2	dva
autorů	autor	k1gMnPc2	autor
o	o	k7c6	o
kanonizaci	kanonizace	k1gFnSc6	kanonizace
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
brát	brát	k5eAaImF	brát
popis	popis	k1gInSc4	popis
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
osoby	osoba	k1gFnSc2	osoba
se	s	k7c7	s
značnou	značný	k2eAgFnSc7d1	značná
rezervou	rezerva	k1gFnSc7	rezerva
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
popis	popis	k1gInSc1	popis
vlády	vláda	k1gFnSc2	vláda
jeho	jeho	k3xOp3gMnSc2	jeho
syna	syn	k1gMnSc2	syn
<g/>
,	,	kIx,	,
u	u	k7c2	u
něhož	jenž	k3xRgNnSc2	jenž
kronika	kronika	k1gFnSc1	kronika
zvýrazňuje	zvýrazňovat	k5eAaImIp3nS	zvýrazňovat
spíše	spíše	k9	spíše
negativa	negativum	k1gNnPc4	negativum
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
více	hodně	k6eAd2	hodně
vynikla	vyniknout	k5eAaPmAgFnS	vyniknout
výjimečná	výjimečný	k2eAgFnSc1d1	výjimečná
osobnost	osobnost	k1gFnSc1	osobnost
jeho	on	k3xPp3gMnSc2	on
předchůdce	předchůdce	k1gMnSc2	předchůdce
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
trůnu	trůn	k1gInSc6	trůn
<g/>
.	.	kIx.	.
<g/>
Petr	Petr	k1gMnSc1	Petr
Žitavský	žitavský	k2eAgMnSc1d1	žitavský
byl	být	k5eAaImAgInS	být
také	také	k9	také
autorem	autor	k1gMnSc7	autor
nedochované	dochovaný	k2eNgFnSc2d1	nedochovaná
Knihy	kniha	k1gFnSc2	kniha
zbraslavských	zbraslavský	k2eAgNnPc2d1	Zbraslavské
tajemství	tajemství	k1gNnPc2	tajemství
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
popisovala	popisovat	k5eAaImAgFnS	popisovat
zázraky	zázrak	k1gInPc4	zázrak
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
se	se	k3xPyFc4	se
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
staly	stát	k5eAaPmAgFnP	stát
<g/>
,	,	kIx,	,
a	a	k8xC	a
dvou	dva	k4xCgFnPc2	dva
knih	kniha	k1gFnPc2	kniha
kázání	kázání	k1gNnSc2	kázání
<g/>
.	.	kIx.	.
</s>
<s>
Spis	spis	k1gInSc4	spis
Malogranatum	Malogranatum	k1gNnSc1	Malogranatum
<g/>
,	,	kIx,	,
sepsaný	sepsaný	k2eAgInSc1d1	sepsaný
neznámým	známý	k2eNgInSc7d1	neznámý
členem	člen	k1gInSc7	člen
zbraslavského	zbraslavský	k2eAgInSc2d1	zbraslavský
konventu	konvent	k1gInSc2	konvent
<g/>
,	,	kIx,	,
datovaný	datovaný	k2eAgInSc1d1	datovaný
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
a	a	k8xC	a
pojednávající	pojednávající	k2eAgFnPc4d1	pojednávající
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
dialogu	dialog	k1gInSc2	dialog
o	o	k7c6	o
duchovní	duchovní	k2eAgFnSc6d1	duchovní
cestě	cesta	k1gFnSc6	cesta
k	k	k7c3	k
náboženskému	náboženský	k2eAgNnSc3d1	náboženské
zdokonalení	zdokonalení	k1gNnSc3	zdokonalení
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dočkal	dočkat	k5eAaPmAgInS	dočkat
mnoha	mnoho	k4c2	mnoho
opisů	opis	k1gInPc2	opis
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
uměleckých	umělecký	k2eAgNnPc2d1	umělecké
děl	dělo	k1gNnPc2	dělo
se	se	k3xPyFc4	se
zachoval	zachovat	k5eAaPmAgInS	zachovat
deskový	deskový	k2eAgInSc1d1	deskový
obraz	obraz	k1gInSc1	obraz
Zbraslavské	zbraslavský	k2eAgFnSc2d1	Zbraslavská
madony	madona	k1gFnSc2	madona
neznámé	známý	k2eNgFnSc2d1	neznámá
datace	datace	k1gFnSc2	datace
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
kopie	kopie	k1gFnSc1	kopie
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
na	na	k7c6	na
oltáři	oltář	k1gInSc6	oltář
jižní	jižní	k2eAgFnSc2d1	jižní
lodi	loď	k1gFnSc2	loď
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Jakuba	Jakub	k1gMnSc2	Jakub
<g/>
,	,	kIx,	,
a	a	k8xC	a
socha	socha	k1gFnSc1	socha
Madony	Madona	k1gFnSc2	Madona
z	z	k7c2	z
Rouchovan	Rouchovan	k1gMnSc1	Rouchovan
dnes	dnes	k6eAd1	dnes
společně	společně	k6eAd1	společně
s	s	k7c7	s
originálem	originál	k1gInSc7	originál
Zbraslavské	zbraslavský	k2eAgFnSc2d1	Zbraslavská
madony	madona	k1gFnSc2	madona
přístupná	přístupný	k2eAgFnSc1d1	přístupná
veřejnosti	veřejnost	k1gFnPc1	veřejnost
v	v	k7c6	v
Anežském	anežský	k2eAgInSc6d1	anežský
klášteře	klášter	k1gInSc6	klášter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Zbraslavi	Zbraslav	k1gFnSc6	Zbraslav
v	v	k7c6	v
královské	královský	k2eAgFnSc6d1	královská
hrobce	hrobka	k1gFnSc6	hrobka
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
některé	některý	k3yIgFnPc1	některý
scény	scéna	k1gFnPc1	scéna
povídky	povídka	k1gFnSc2	povídka
Točník	Točník	k1gMnSc1	Točník
Václava	Václav	k1gMnSc2	Václav
Klimenta	Kliment	k1gMnSc2	Kliment
Klicpery	Klicpera	k1gFnSc2	Klicpera
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přemyslovská	přemyslovský	k2eAgFnSc1d1	Přemyslovská
nekropole	nekropole	k1gFnSc1	nekropole
==	==	k?	==
</s>
</p>
<p>
<s>
Cisterciáci	cisterciák	k1gMnPc1	cisterciák
zpočátku	zpočátku	k6eAd1	zpočátku
odmítali	odmítat	k5eAaImAgMnP	odmítat
pohřby	pohřeb	k1gInPc4	pohřeb
cizích	cizí	k2eAgFnPc2d1	cizí
osob	osoba	k1gFnPc2	osoba
kromě	kromě	k7c2	kromě
zemřelých	zemřelý	k2eAgMnPc2d1	zemřelý
klášterních	klášterní	k2eAgMnPc2d1	klášterní
hostů	host	k1gMnPc2	host
a	a	k8xC	a
až	až	k9	až
roku	rok	k1gInSc2	rok
1152	[number]	k4	1152
se	se	k3xPyFc4	se
v	v	k7c6	v
ustanovení	ustanovení	k1gNnSc6	ustanovení
generální	generální	k2eAgFnSc2d1	generální
kapituly	kapitula	k1gFnSc2	kapitula
objevila	objevit	k5eAaPmAgFnS	objevit
výjimka	výjimka	k1gFnSc1	výjimka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
přijmout	přijmout	k5eAaPmF	přijmout
k	k	k7c3	k
poslednímu	poslední	k2eAgInSc3d1	poslední
odpočinku	odpočinek	k1gInSc3	odpočinek
maximálně	maximálně	k6eAd1	maximálně
dva	dva	k4xCgMnPc4	dva
z	z	k7c2	z
přátel	přítel	k1gMnPc2	přítel
a	a	k8xC	a
familiářů	familiář	k1gMnPc2	familiář
kláštera	klášter	k1gInSc2	klášter
i	i	k8xC	i
s	s	k7c7	s
manželkami	manželka	k1gFnPc7	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
byli	být	k5eAaImAgMnP	být
cisterciáčtí	cisterciácký	k2eAgMnPc1d1	cisterciácký
mniši	mnich	k1gMnPc1	mnich
více	hodně	k6eAd2	hodně
naklonění	naklonění	k1gNnSc4	naklonění
pohřbívání	pohřbívání	k1gNnPc2	pohřbívání
svých	svůj	k3xOyFgMnPc2	svůj
dobrodinců	dobrodinec	k1gMnPc2	dobrodinec
<g/>
,	,	kIx,	,
na	na	k7c6	na
čemž	což	k3yRnSc6	což
měla	mít	k5eAaImAgFnS	mít
podíl	podíl	k1gInSc4	podíl
síla	síla	k1gFnSc1	síla
zvyklosti	zvyklost	k1gFnPc1	zvyklost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byly	být	k5eAaImAgInP	být
pohřby	pohřeb	k1gInPc1	pohřeb
běžné	běžný	k2eAgInPc1d1	běžný
v	v	k7c6	v
klášterech	klášter	k1gInPc6	klášter
benediktinské	benediktinský	k2eAgFnSc2d1	benediktinská
řehole	řehole	k1gFnSc2	řehole
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
tlak	tlak	k1gInSc1	tlak
vyvíjený	vyvíjený	k2eAgInSc1d1	vyvíjený
světskými	světský	k2eAgFnPc7d1	světská
osobami	osoba	k1gFnPc7	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byly	být	k5eAaImAgInP	být
již	již	k6eAd1	již
pohřby	pohřeb	k1gInPc1	pohřeb
členů	člen	k1gInPc2	člen
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
vrstvy	vrstva	k1gFnSc2	vrstva
častou	častý	k2eAgFnSc7d1	častá
záležitostí	záležitost	k1gFnSc7	záležitost
a	a	k8xC	a
cisterciácké	cisterciácký	k2eAgInPc1d1	cisterciácký
kláštery	klášter	k1gInPc1	klášter
byly	být	k5eAaImAgInP	být
upřednostňovány	upřednostňovat	k5eAaImNgInP	upřednostňovat
před	před	k7c7	před
kláštery	klášter	k1gInPc7	klášter
ostatních	ostatní	k2eAgInPc2d1	ostatní
řádů	řád	k1gInPc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
zřejmě	zřejmě	k6eAd1	zřejmě
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
víra	víra	k1gFnSc1	víra
ve	v	k7c4	v
větší	veliký	k2eAgFnSc4d2	veliký
účinnost	účinnost	k1gFnSc4	účinnost
modliteb	modlitba	k1gFnPc2	modlitba
pocházejících	pocházející	k2eAgFnPc2d1	pocházející
od	od	k7c2	od
tak	tak	k6eAd1	tak
zbožného	zbožný	k2eAgInSc2d1	zbožný
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Cisterciácké	cisterciácký	k2eAgInPc1d1	cisterciácký
kláštery	klášter	k1gInPc1	klášter
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
staly	stát	k5eAaPmAgFnP	stát
místy	místy	k6eAd1	místy
posledního	poslední	k2eAgInSc2d1	poslední
odpočinku	odpočinek	k1gInSc2	odpočinek
členů	člen	k1gMnPc2	člen
vládnoucích	vládnoucí	k2eAgInPc2d1	vládnoucí
rodů	rod	k1gInPc2	rod
Arpádovců	Arpádovec	k1gMnPc2	Arpádovec
<g/>
,	,	kIx,	,
Babenberků	Babenberka	k1gMnPc2	Babenberka
<g/>
,	,	kIx,	,
Piastovců	Piastovec	k1gMnPc2	Piastovec
<g/>
,	,	kIx,	,
Wittelsbachů	Wittelsbach	k1gMnPc2	Wittelsbach
<g/>
,	,	kIx,	,
Wettinů	Wettin	k1gMnPc2	Wettin
a	a	k8xC	a
také	také	k9	také
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Hroby	hrob	k1gInPc1	hrob
bývaly	bývat	k5eAaImAgInP	bývat
umístěny	umístit	k5eAaPmNgInP	umístit
na	na	k7c6	na
čestných	čestný	k2eAgNnPc6d1	čestné
místech	místo	k1gNnPc6	místo
klášterů	klášter	k1gInPc2	klášter
–	–	k?	–
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
ose	osa	k1gFnSc6	osa
kostela	kostel	k1gInSc2	kostel
před	před	k7c7	před
hlavním	hlavní	k2eAgInSc7d1	hlavní
oltářem	oltář	k1gInSc7	oltář
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
kapitulní	kapitulní	k2eAgFnSc6d1	kapitulní
síni	síň	k1gFnSc6	síň
<g/>
,	,	kIx,	,
v	v	k7c6	v
křížové	křížový	k2eAgFnSc6d1	křížová
chodbě	chodba	k1gFnSc6	chodba
či	či	k8xC	či
ve	v	k7c6	v
zvláštních	zvláštní	k2eAgFnPc6d1	zvláštní
kaplích	kaple	k1gFnPc6	kaple
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Zbraslavi	Zbraslav	k1gFnSc6	Zbraslav
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
pohřbívat	pohřbívat	k5eAaImF	pohřbívat
před	před	k7c7	před
dokončením	dokončení	k1gNnSc7	dokončení
východní	východní	k2eAgFnSc2d1	východní
částí	část	k1gFnPc2	část
chrámu	chrám	k1gInSc3	chrám
v	v	k7c6	v
původní	původní	k2eAgFnSc6d1	původní
kapli	kaple	k1gFnSc6	kaple
sv.	sv.	kA	sv.
Jakuba	Jakub	k1gMnSc2	Jakub
Většího	veliký	k2eAgMnSc2d2	veliký
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgFnP	být
pochovány	pochován	k2eAgFnPc1d1	pochována
předčasně	předčasně	k6eAd1	předčasně
zemřelé	zemřelý	k2eAgFnPc1d1	zemřelá
královské	královský	k2eAgFnPc1d1	královská
děti	dítě	k1gFnPc1	dítě
a	a	k8xC	a
srdce	srdce	k1gNnSc1	srdce
hrdé	hrdý	k2eAgFnSc2d1	hrdá
a	a	k8xC	a
ctižádostivé	ctižádostivý	k2eAgFnSc2d1	ctižádostivá
švábské	švábský	k2eAgFnSc2d1	Švábská
vévodkyně	vévodkyně	k1gFnSc2	vévodkyně
Anežky	Anežka	k1gFnSc2	Anežka
<g/>
.	.	kIx.	.
</s>
<s>
Královna	královna	k1gFnSc1	královna
Guta	Guta	k1gFnSc1	Guta
byla	být	k5eAaImAgFnS	být
prozatímně	prozatímně	k6eAd1	prozatímně
pochována	pochovat	k5eAaPmNgFnS	pochovat
na	na	k7c6	na
Pražském	pražský	k2eAgInSc6d1	pražský
hradě	hrad	k1gInSc6	hrad
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
časem	časem	k6eAd1	časem
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
společně	společně	k6eAd1	společně
s	s	k7c7	s
Přemyslem	Přemysl	k1gMnSc7	Přemysl
Otakarem	Otakar	k1gMnSc7	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
přemístěna	přemístit	k5eAaPmNgFnS	přemístit
na	na	k7c4	na
Zbraslav	Zbraslav	k1gFnSc4	Zbraslav
<g/>
.	.	kIx.	.
</s>
<s>
Což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Guty	Guta	k1gFnSc2	Guta
patrně	patrně	k6eAd1	patrně
stalo	stát	k5eAaPmAgNnS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
slavném	slavný	k2eAgInSc6d1	slavný
fundátorově	fundátorův	k2eAgInSc6d1	fundátorův
pohřbu	pohřeb	k1gInSc6	pohřeb
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
pohřbívat	pohřbívat	k5eAaImF	pohřbívat
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
roku	rok	k1gInSc2	rok
1320	[number]	k4	1320
byl	být	k5eAaImAgMnS	být
"	"	kIx"	"
<g/>
u	u	k7c2	u
nohou	noha	k1gFnPc2	noha
zakladatelových	zakladatelův	k2eAgFnPc2d1	zakladatelova
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
hrobě	hrob	k1gInSc6	hrob
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
a	a	k8xC	a
Gutou	Guta	k1gFnSc7	Guta
<g/>
,	,	kIx,	,
dětmi	dítě	k1gFnPc7	dítě
téhož	týž	k3xTgMnSc2	týž
zakladatele	zakladatel	k1gMnSc2	zakladatel
<g/>
"	"	kIx"	"
pohřben	pohřben	k2eAgMnSc1d1	pohřben
druhorozený	druhorozený	k2eAgMnSc1d1	druhorozený
syn	syn	k1gMnSc1	syn
Jana	Jan	k1gMnSc2	Jan
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
a	a	k8xC	a
Elišky	Eliška	k1gFnPc4	Eliška
Přemyslovny	Přemyslovna	k1gFnSc2	Přemyslovna
Otakar	Otakara	k1gFnPc2	Otakara
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
uprostřed	uprostřed	k7c2	uprostřed
chrámu	chrám	k1gInSc2	chrám
mezi	mezi	k7c7	mezi
sedadly	sedadlo	k1gNnPc7	sedadlo
mnichů	mnich	k1gMnPc2	mnich
pohřbena	pohřbít	k5eAaPmNgFnS	pohřbít
lehnická	lehnický	k2eAgFnSc1d1	lehnický
vévodkyně	vévodkyně	k1gFnSc1	vévodkyně
Markéta	Markéta	k1gFnSc1	Markéta
i	i	k9	i
s	s	k7c7	s
novorozeným	novorozený	k2eAgMnSc7d1	novorozený
synem	syn	k1gMnSc7	syn
Mikulášem	Mikuláš	k1gMnSc7	Mikuláš
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
ji	on	k3xPp3gFnSc4	on
její	její	k3xOp3gFnSc1	její
jmenovkyně	jmenovkyně	k1gFnSc1	jmenovkyně
<g/>
,	,	kIx,	,
dcera	dcera	k1gFnSc1	dcera
Jana	Jan	k1gMnSc2	Jan
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
pohřbena	pohřbít	k5eAaPmNgFnS	pohřbít
před	před	k7c7	před
stupni	stupeň	k1gInPc7	stupeň
hlavního	hlavní	k2eAgInSc2d1	hlavní
oltáře	oltář	k1gInSc2	oltář
"	"	kIx"	"
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
třemi	tři	k4xCgMnPc7	tři
dětmi	dítě	k1gFnPc7	dítě
královskými	královský	k2eAgInPc7d1	královský
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1326	[number]	k4	1326
zařídila	zařídit	k5eAaPmAgFnS	zařídit
Eliška	Eliška	k1gFnSc1	Eliška
Přemyslovna	Přemyslovna	k1gFnSc1	Přemyslovna
přesun	přesun	k1gInSc4	přesun
ostatků	ostatek	k1gInPc2	ostatek
svého	svůj	k3xOyFgMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
Václava	Václav	k1gMnSc2	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
z	z	k7c2	z
Olomouce	Olomouc	k1gFnSc2	Olomouc
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
ostatky	ostatek	k1gInPc1	ostatek
spočinuly	spočinout	k5eAaPmAgInP	spočinout
po	po	k7c6	po
straně	strana	k1gFnSc6	strana
otcova	otcův	k2eAgInSc2d1	otcův
hrobu	hrob	k1gInSc2	hrob
<g/>
.	.	kIx.	.
</s>
<s>
Eliška	Eliška	k1gFnSc1	Eliška
byla	být	k5eAaImAgFnS	být
uložena	uložit	k5eAaPmNgFnS	uložit
vedle	vedle	k7c2	vedle
svých	svůj	k3xOyFgInPc2	svůj
předků	předek	k1gInPc2	předek
"	"	kIx"	"
<g/>
dlouho	dlouho	k6eAd1	dlouho
trápená	trápený	k2eAgFnSc1d1	trápená
horečkami	horečka	k1gFnPc7	horečka
a	a	k8xC	a
plicní	plicní	k2eAgFnSc7d1	plicní
chorobou	choroba	k1gFnSc7	choroba
<g/>
"	"	kIx"	"
roku	rok	k1gInSc2	rok
1330	[number]	k4	1330
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
u	u	k7c2	u
matčina	matčin	k2eAgInSc2d1	matčin
hrobu	hrob	k1gInSc2	hrob
zastavil	zastavit	k5eAaPmAgInS	zastavit
při	při	k7c6	při
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
Karel	Karla	k1gFnPc2	Karla
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
přes	přes	k7c4	přes
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
trávil	trávit	k5eAaImAgMnS	trávit
za	za	k7c7	za
hranicemi	hranice	k1gFnPc7	hranice
českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
a	a	k8xC	a
touto	tento	k3xDgFnSc7	tento
návštěvou	návštěva	k1gFnSc7	návštěva
symbolicky	symbolicky	k6eAd1	symbolicky
navázal	navázat	k5eAaPmAgInS	navázat
na	na	k7c4	na
odkaz	odkaz	k1gInSc4	odkaz
svých	svůj	k3xOyFgInPc2	svůj
přemyslovských	přemyslovský	k2eAgInPc2d1	přemyslovský
předků	předek	k1gInPc2	předek
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1341	[number]	k4	1341
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
pohřbena	pohřben	k2eAgFnSc1d1	pohřbena
ovdovělá	ovdovělý	k2eAgFnSc1d1	ovdovělá
dolnobavorská	dolnobavorský	k2eAgFnSc1d1	dolnobavorská
vévodkyně	vévodkyně	k1gFnSc1	vévodkyně
Markéta	Markéta	k1gFnSc1	Markéta
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
roku	rok	k1gInSc2	rok
1386	[number]	k4	1386
královna	královna	k1gFnSc1	královna
Johana	Johana	k1gFnSc1	Johana
Bavorská	bavorský	k2eAgFnSc1d1	bavorská
a	a	k8xC	a
poslední	poslední	k2eAgInSc1d1	poslední
pohřeb	pohřeb	k1gInSc1	pohřeb
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
roku	rok	k1gInSc2	rok
1419	[number]	k4	1419
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
uložen	uložen	k2eAgMnSc1d1	uložen
Václav	Václav	k1gMnSc1	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
si	se	k3xPyFc3	se
sám	sám	k3xTgMnSc1	sám
vybral	vybrat	k5eAaPmAgMnS	vybrat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ostatky	ostatek	k1gInPc1	ostatek
Johany	Johana	k1gFnSc2	Johana
Bavorské	bavorský	k2eAgFnSc2d1	bavorská
a	a	k8xC	a
Václava	Václav	k1gMnSc2	Václav
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
byly	být	k5eAaImAgFnP	být
(	(	kIx(	(
<g/>
patrně	patrně	k6eAd1	patrně
<g/>
)	)	kIx)	)
později	pozdě	k6eAd2	pozdě
převezeny	převezen	k2eAgInPc4d1	převezen
a	a	k8xC	a
pohřbeny	pohřben	k2eAgInPc4d1	pohřben
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
Václava	Václav	k1gMnSc2	Václav
a	a	k8xC	a
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
trvání	trvání	k1gNnSc2	trvání
byl	být	k5eAaImAgInS	být
Zbraslavský	zbraslavský	k2eAgInSc1d1	zbraslavský
klášter	klášter	k1gInSc1	klášter
předním	přední	k2eAgNnSc7d1	přední
pohřebištěm	pohřebiště	k1gNnSc7	pohřebiště
dvou	dva	k4xCgFnPc2	dva
dynastií	dynastie	k1gFnPc2	dynastie
českých	český	k2eAgMnPc2d1	český
panovníků	panovník	k1gMnPc2	panovník
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
rodin	rodina	k1gFnPc2	rodina
<g/>
,	,	kIx,	,
které	který	k3yQgMnPc4	který
předčila	předčít	k5eAaPmAgFnS	předčít
až	až	k9	až
katedrála	katedrála	k1gFnSc1	katedrála
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
hroby	hrob	k1gInPc1	hrob
umístěné	umístěný	k2eAgInPc1d1	umístěný
v	v	k7c6	v
chóru	chór	k1gInSc6	chór
měly	mít	k5eAaImAgInP	mít
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
podlahy	podlaha	k1gFnSc2	podlaha
náhrobní	náhrobní	k2eAgFnSc2d1	náhrobní
desky	deska	k1gFnSc2	deska
a	a	k8xC	a
některé	některý	k3yIgInPc4	některý
zřejmě	zřejmě	k6eAd1	zřejmě
i	i	k9	i
umělecké	umělecký	k2eAgInPc4d1	umělecký
náhrobky	náhrobek	k1gInPc4	náhrobek
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
souboru	soubor	k1gInSc6	soubor
kosterního	kosterní	k2eAgInSc2d1	kosterní
materiálu	materiál	k1gInSc2	materiál
vyneseného	vynesený	k2eAgInSc2d1	vynesený
z	z	k7c2	z
královského	královský	k2eAgNnSc2d1	královské
pohřebiště	pohřebiště	k1gNnSc2	pohřebiště
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
roku	rok	k1gInSc2	rok
1671	[number]	k4	1671
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1824	[number]	k4	1824
byly	být	k5eAaImAgInP	být
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
zapsány	zapsán	k2eAgInPc1d1	zapsán
do	do	k7c2	do
farní	farní	k2eAgFnSc2d1	farní
pamětní	pamětní	k2eAgFnSc2d1	pamětní
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
lebky	lebka	k1gFnPc1	lebka
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
ukazovány	ukazován	k2eAgFnPc1d1	ukazována
jako	jako	k8xS	jako
lebky	lebka	k1gFnPc1	lebka
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Václava	Václav	k1gMnSc2	Václav
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
blíže	blízce	k6eAd2	blízce
neurčené	určený	k2eNgFnPc4d1	neurčená
královny	královna	k1gFnPc4	královna
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1875	[number]	k4	1875
byly	být	k5eAaImAgFnP	být
tři	tři	k4xCgFnPc1	tři
lebky	lebka	k1gFnPc1	lebka
uloženy	uložit	k5eAaPmNgFnP	uložit
do	do	k7c2	do
skřínky	skřínka	k1gFnSc2	skřínka
na	na	k7c6	na
stěně	stěna	k1gFnSc6	stěna
kostela	kostel	k1gInSc2	kostel
a	a	k8xC	a
ostatní	ostatní	k2eAgFnPc1d1	ostatní
kosti	kost	k1gFnPc1	kost
do	do	k7c2	do
přihrádek	přihrádka	k1gFnPc2	přihrádka
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Matiegka	Matiegka	k1gMnSc1	Matiegka
pak	pak	k6eAd1	pak
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
označil	označit	k5eAaPmAgMnS	označit
dochovaný	dochovaný	k2eAgInSc4d1	dochovaný
kosterní	kosterní	k2eAgInSc4d1	kosterní
materiál	materiál	k1gInSc4	materiál
za	za	k7c4	za
pozůstatky	pozůstatek	k1gInPc4	pozůstatek
sedmi	sedm	k4xCc2	sedm
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslník	průmyslník	k1gMnSc1	průmyslník
Cyril	Cyril	k1gMnSc1	Cyril
Bartoň	Bartoň	k1gMnSc1	Bartoň
přišel	přijít	k5eAaPmAgMnS	přijít
po	po	k7c6	po
vzniku	vznik	k1gInSc6	vznik
ČSR	ČSR	kA	ČSR
s	s	k7c7	s
nápadem	nápad	k1gInSc7	nápad
na	na	k7c4	na
pomník	pomník	k1gInSc4	pomník
přemyslovské	přemyslovský	k2eAgFnSc2d1	Přemyslovská
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
Josefa	Josef	k1gMnSc4	Josef
Václava	Václav	k1gMnSc4	Václav
Myslbeka	Myslbeek	k1gMnSc4	Myslbeek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
stačil	stačit	k5eAaBmAgInS	stačit
vytvořit	vytvořit	k5eAaPmF	vytvořit
pouze	pouze	k6eAd1	pouze
model	model	k1gInSc4	model
<g/>
,	,	kIx,	,
a	a	k8xC	a
poté	poté	k6eAd1	poté
byly	být	k5eAaImAgFnP	být
lebky	lebka	k1gFnPc1	lebka
vloženy	vložit	k5eAaPmNgFnP	vložit
do	do	k7c2	do
opukového	opukový	k2eAgInSc2d1	opukový
Památníku	památník	k1gInSc2	památník
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
od	od	k7c2	od
Jana	Jan	k1gMnSc2	Jan
Štursy	Štursa	k1gFnSc2	Štursa
<g/>
,	,	kIx,	,
z	z	k7c2	z
nějž	jenž	k3xRgInSc2	jenž
byly	být	k5eAaImAgInP	být
antropologem	antropolog	k1gMnSc7	antropolog
Emanuelem	Emanuel	k1gMnSc7	Emanuel
Vlčkem	vlček	k1gInSc7	vlček
identifikovány	identifikován	k2eAgInPc4d1	identifikován
ostatky	ostatek	k1gInPc4	ostatek
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Elišky	Eliška	k1gFnPc4	Eliška
Přemyslovny	Přemyslovna	k1gFnSc2	Přemyslovna
a	a	k8xC	a
zřejmě	zřejmě	k6eAd1	zřejmě
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
Eliščiných	Eliščin	k2eAgNnPc2d1	Eliščino
dvojčat	dvojče	k1gNnPc2	dvojče
–	–	k?	–
dcerky	dcerka	k1gFnSc2	dcerka
Elišky	Eliška	k1gFnSc2	Eliška
<g/>
.	.	kIx.	.
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1991	[number]	k4	1991
byly	být	k5eAaImAgInP	být
přemyslovské	přemyslovský	k2eAgInPc1d1	přemyslovský
ostatky	ostatek	k1gInPc1	ostatek
znovu	znovu	k6eAd1	znovu
slavnostně	slavnostně	k6eAd1	slavnostně
pohřbeny	pohřbít	k5eAaPmNgInP	pohřbít
do	do	k7c2	do
hrobu	hrob	k1gInSc2	hrob
v	v	k7c6	v
podlaze	podlaha	k1gFnSc6	podlaha
u	u	k7c2	u
vstupu	vstup	k1gInSc2	vstup
do	do	k7c2	do
presbyteria	presbyterium	k1gNnSc2	presbyterium
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Jakuba	Jakub	k1gMnSc2	Jakub
na	na	k7c6	na
Zbraslavi	Zbraslav	k1gFnSc6	Zbraslav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Madoně	Madona	k1gFnSc3	Madona
Zbraslavské	zbraslavský	k2eAgFnSc2d1	Zbraslavská
byla	být	k5eAaImAgFnS	být
zasvěcena	zasvěcen	k2eAgFnSc1d1	zasvěcena
43	[number]	k4	43
<g/>
.	.	kIx.	.
kaple	kaple	k1gFnSc2	kaple
Svaté	svatý	k2eAgFnSc2d1	svatá
cesty	cesta	k1gFnSc2	cesta
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
do	do	k7c2	do
Staré	Stará	k1gFnSc2	Stará
Boleslavi	Boleslaev	k1gFnSc3	Boleslaev
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
jezuity	jezuita	k1gMnPc7	jezuita
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1674	[number]	k4	1674
<g/>
–	–	k?	–
<g/>
1690	[number]	k4	1690
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Prameny	pramen	k1gInPc1	pramen
===	===	k?	===
</s>
</p>
<p>
<s>
Zbraslavská	zbraslavský	k2eAgFnSc1d1	Zbraslavská
kronika	kronika	k1gFnSc1	kronika
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Fiala	Fiala	k1gMnSc1	Fiala
<g/>
;	;	kIx,	;
překlad	překlad	k1gInSc1	překlad
František	františek	k1gInSc1	františek
Heřmanský	Heřmanský	k2eAgInSc1d1	Heřmanský
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Mertlík	Mertlík	k1gInSc1	Mertlík
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
597	[number]	k4	597
s.	s.	k?	s.
</s>
</p>
<p>
<s>
Decem	Dec	k1gInSc7	Dec
registra	registrum	k1gNnSc2	registrum
censuum	censuum	k1gInSc1	censuum
bohemica	bohemic	k1gInSc2	bohemic
compilata	compile	k1gNnPc1	compile
aetate	aetat	k1gInSc5	aetat
bellum	bellum	k1gNnSc1	bellum
husiticum	husiticum	k1gNnSc1	husiticum
praecedente	praecedent	k1gMnSc5	praecedent
<g/>
.	.	kIx.	.
</s>
<s>
Deset	deset	k4xCc1	deset
urbářů	urbář	k1gInPc2	urbář
českých	český	k2eAgInPc2d1	český
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
před	před	k7c7	před
válkami	válka	k1gFnPc7	válka
husitskými	husitský	k2eAgFnPc7d1	husitská
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Josef	Josef	k1gMnSc1	Josef
Emler	Emler	k1gMnSc1	Emler
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Královská	královský	k2eAgFnSc1d1	královská
česká	český	k2eAgFnSc1d1	Česká
společnost	společnost	k1gFnSc1	společnost
nauk	nauka	k1gFnPc2	nauka
<g/>
,	,	kIx,	,
1881	[number]	k4	1881
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Registrum	registrum	k1gNnSc1	registrum
monasterii	monasterie	k1gFnSc3	monasterie
Aulae	Aulae	k1gFnSc3	Aulae
Regiae	Regiae	k1gFnSc3	Regiae
de	de	k?	de
omnibus	omnibus	k1gInSc1	omnibus
proventibus	proventibus	k1gInSc1	proventibus
<g/>
,	,	kIx,	,
s.	s.	k?	s.
[	[	kIx(	[
<g/>
309	[number]	k4	309
<g/>
]	]	kIx)	]
-	-	kIx~	-
312	[number]	k4	312
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
lat	lat	k1gInSc1	lat
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Fontes	Fontes	k1gInSc1	Fontes
rerum	rerum	k1gNnSc1	rerum
Bohemicarum	Bohemicarum	k1gInSc1	Bohemicarum
<g/>
.	.	kIx.	.
</s>
<s>
Prameny	pramen	k1gInPc1	pramen
dějin	dějiny	k1gFnPc2	dějiny
českých	český	k2eAgFnPc2d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Josef	Josef	k1gMnSc1	Josef
Emler	Emler	k1gMnSc1	Emler
<g/>
.	.	kIx.	.
</s>
<s>
Svazek	svazek	k1gInSc1	svazek
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nadace	nadace	k1gFnSc1	nadace
Františka	František	k1gMnSc2	František
Palackého	Palacký	k1gMnSc2	Palacký
<g/>
,	,	kIx,	,
1884	[number]	k4	1884
<g/>
.	.	kIx.	.
</s>
<s>
XXVIII	XXVIII	kA	XXVIII
<g/>
,	,	kIx,	,
571	[number]	k4	571
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Petra	Petr	k1gMnSc2	Petr
Žitavského	žitavský	k2eAgInSc2d1	žitavský
Kronika	kronika	k1gFnSc1	kronika
zbraslavská	zbraslavský	k2eAgFnSc1d1	Zbraslavská
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
lat	lat	k1gInSc1	lat
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Listy	list	k1gInPc1	list
kláštera	klášter	k1gInSc2	klášter
Zbraslavského	zbraslavský	k2eAgInSc2d1	zbraslavský
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
Tadra	Tadra	k1gMnSc1	Tadra
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
akademie	akademie	k1gFnSc1	akademie
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
pro	pro	k7c4	pro
vědy	věda	k1gFnPc4	věda
<g/>
,	,	kIx,	,
slovesnost	slovesnost	k1gFnSc1	slovesnost
a	a	k8xC	a
umění	umění	k1gNnSc1	umění
<g/>
,	,	kIx,	,
1903	[number]	k4	1903
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Přepisy	přepis	k1gInPc1	přepis
listinného	listinný	k2eAgInSc2d1	listinný
materiálu	materiál	k1gInSc2	materiál
náležejícího	náležející	k2eAgInSc2d1	náležející
Zbraslavskému	zbraslavský	k2eAgInSc3d1	zbraslavský
klášteru	klášter	k1gInSc3	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Soubor	soubor	k1gInSc1	soubor
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
období	období	k1gNnSc4	období
od	od	k7c2	od
založení	založení	k1gNnSc2	založení
(	(	kIx(	(
<g/>
rok	rok	k1gInSc1	rok
1292	[number]	k4	1292
<g/>
)	)	kIx)	)
do	do	k7c2	do
zrušení	zrušení	k1gNnSc2	zrušení
kláštera	klášter	k1gInSc2	klášter
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1785	[number]	k4	1785
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ANTONÍN	Antonín	k1gMnSc1	Antonín
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
.	.	kIx.	.
</s>
<s>
Zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
politika	politika	k1gFnSc1	politika
krále	král	k1gMnSc2	král
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1283	[number]	k4	1283
<g/>
-	-	kIx~	-
<g/>
1300	[number]	k4	1300
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
Matice	matice	k1gFnSc1	matice
moravská	moravský	k2eAgFnSc1d1	Moravská
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
306	[number]	k4	306
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86488	[number]	k4	86488
<g/>
-	-	kIx~	-
<g/>
63	[number]	k4	63
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BENEŠOVSKÁ	benešovský	k2eAgFnSc1d1	Benešovská
<g/>
,	,	kIx,	,
Klára	Klára	k1gFnSc1	Klára
<g/>
.	.	kIx.	.
</s>
<s>
Aula	aula	k1gFnSc1	aula
Regia	Regia	k1gFnSc1	Regia
prè	prè	k?	prè
de	de	k?	de
Prague	Prague	k1gInSc1	Prague
et	et	k?	et
Mons	Mons	k1gInSc1	Mons
Regalis	Regalis	k1gFnSc1	Regalis
prè	prè	k?	prè
de	de	k?	de
Paris	Paris	k1gMnSc1	Paris
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
Cisterciáci	cisterciák	k1gMnPc1	cisterciák
ve	v	k7c6	v
středověkém	středověký	k2eAgInSc6d1	středověký
českém	český	k2eAgInSc6d1	český
státě	stát	k1gInSc6	stát
<g/>
,	,	kIx,	,
Sborník	sborník	k1gInSc1	sborník
z	z	k7c2	z
kolokvia	kolokvium	k1gNnSc2	kolokvium
v	v	k7c6	v
Kutné	kutný	k2eAgFnSc6d1	Kutná
Hoře	hora	k1gFnSc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Cîteaux	Cîteaux	k1gInSc1	Cîteaux
VZW	VZW	kA	VZW
<g/>
:	:	kIx,	:
Brecht	Brecht	k1gMnSc1	Brecht
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
231	[number]	k4	231
<g/>
–	–	k?	–
<g/>
245	[number]	k4	245
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BENEŠOVSKÁ	benešovský	k2eAgFnSc1d1	Benešovská
<g/>
,	,	kIx,	,
Klára	Klára	k1gFnSc1	Klára
<g/>
;	;	kIx,	;
JEČNÝ	ječný	k2eAgInSc1d1	ječný
<g/>
,	,	kIx,	,
Hubert	Hubert	k1gMnSc1	Hubert
<g/>
;	;	kIx,	;
STEHLÍKOVÁ	Stehlíková	k1gFnSc1	Stehlíková
<g/>
,	,	kIx,	,
Dana	Dana	k1gFnSc1	Dana
<g/>
;	;	kIx,	;
TRYML	TRYML	kA	TRYML
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgInPc1d1	Nové
prameny	pramen	k1gInPc1	pramen
k	k	k7c3	k
dějinám	dějiny	k1gFnPc3	dějiny
klášterního	klášterní	k2eAgInSc2d1	klášterní
kostela	kostel	k1gInSc2	kostel
cisterciáků	cisterciák	k1gMnPc2	cisterciák
na	na	k7c6	na
Zbraslavi	Zbraslav	k1gFnSc6	Zbraslav
<g/>
.	.	kIx.	.
</s>
<s>
Umění	umění	k1gNnSc1	umění
<g/>
.	.	kIx.	.
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
34	[number]	k4	34
<g/>
,	,	kIx,	,
s.	s.	k?	s.
400	[number]	k4	400
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BLÁHOVÁ	Bláhová	k1gFnSc1	Bláhová
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
<g/>
.	.	kIx.	.
</s>
<s>
Cisterciácké	cisterciácký	k2eAgNnSc4d1	cisterciácké
dějepisectví	dějepisectví	k1gNnSc4	dějepisectví
ve	v	k7c6	v
středověkých	středověký	k2eAgFnPc6d1	středověká
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Historický	historický	k2eAgInSc1d1	historický
obzor	obzor	k1gInSc1	obzor
<g/>
.	.	kIx.	.
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
,	,	kIx,	,
s.	s.	k?	s.
275	[number]	k4	275
<g/>
–	–	k?	–
<g/>
280	[number]	k4	280
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BOHÁČ	Boháč	k1gMnSc1	Boháč
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Středověké	středověký	k2eAgInPc1d1	středověký
kláštery	klášter	k1gInPc1	klášter
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
a	a	k8xC	a
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
v	v	k7c6	v
době	doba	k1gFnSc6	doba
předhusitské	předhusitský	k2eAgFnSc6d1	předhusitská
<g/>
.	.	kIx.	.
</s>
<s>
Historická	historický	k2eAgFnSc1d1	historická
geografie	geografie	k1gFnSc1	geografie
<g/>
.	.	kIx.	.
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ČECHURA	Čechura	k1gMnSc1	Čechura
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Cisterciácké	cisterciácký	k2eAgInPc1d1	cisterciácký
kláštery	klášter	k1gInPc1	klášter
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
v	v	k7c6	v
době	doba	k1gFnSc6	doba
předhusitské	předhusitský	k2eAgFnSc2d1	předhusitská
ve	v	k7c6	v
světle	světlo	k1gNnSc6	světlo
řádových	řádový	k2eAgNnPc2d1	řádové
akt	akta	k1gNnPc2	akta
<g/>
.	.	kIx.	.
</s>
<s>
PHS	PHS	kA	PHS
<g/>
.	.	kIx.	.
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
,	,	kIx,	,
s.	s.	k?	s.
35	[number]	k4	35
<g/>
–	–	k?	–
<g/>
72	[number]	k4	72
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DRAGOUN	dragoun	k1gMnSc1	dragoun
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
;	;	kIx,	;
STEHLÍKOVÁ	Stehlíková	k1gFnSc1	Stehlíková
<g/>
,	,	kIx,	,
Dana	Dana	k1gFnSc1	Dana
<g/>
.	.	kIx.	.
</s>
<s>
Archeologické	archeologický	k2eAgInPc1d1	archeologický
a	a	k8xC	a
stavebně-historické	stavebněistorický	k2eAgInPc1d1	stavebně-historický
výzkumy	výzkum	k1gInPc1	výzkum
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
prelatury	prelatura	k1gFnSc2	prelatura
zbraslavského	zbraslavský	k2eAgInSc2d1	zbraslavský
kláštera	klášter	k1gInSc2	klášter
v	v	k7c6	v
letech	let	k1gInPc6	let
1984	[number]	k4	1984
<g/>
–	–	k?	–
<g/>
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
TRYML	TRYML	kA	TRYML
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
<g/>
;	;	kIx,	;
DRAGOUN	dragoun	k1gMnSc1	dragoun
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
.	.	kIx.	.
</s>
<s>
Staletá	staletý	k2eAgFnSc1d1	staletá
Praha	Praha	k1gFnSc1	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Oswald	Oswald	k1gMnSc1	Oswald
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
Ročník	ročník	k1gInSc1	ročník
XXII	XXII	kA	XXII
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HORYNA	Horyna	k1gMnSc1	Horyna
<g/>
,	,	kIx,	,
Mojmír	Mojmír	k1gMnSc1	Mojmír
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Blažej	Blažej	k1gMnSc1	Blažej
Santini-Aichel	Santini-Aichel	k1gMnSc1	Santini-Aichel
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
487	[number]	k4	487
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7184	[number]	k4	7184
<g/>
-	-	kIx~	-
<g/>
664	[number]	k4	664
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CHARVÁTOVÁ	Charvátová	k1gFnSc1	Charvátová
<g/>
,	,	kIx,	,
Kateřina	Kateřina	k1gFnSc1	Kateřina
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
:	:	kIx,	:
král	král	k1gMnSc1	král
český	český	k2eAgMnSc1d1	český
a	a	k8xC	a
polský	polský	k2eAgMnSc1d1	polský
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
343	[number]	k4	343
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7021	[number]	k4	7021
<g/>
-	-	kIx~	-
<g/>
841	[number]	k4	841
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CHARVÁTOVÁ	Charvátová	k1gFnSc1	Charvátová
<g/>
,	,	kIx,	,
Kateřina	Kateřina	k1gFnSc1	Kateřina
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
cisterciáckého	cisterciácký	k2eAgInSc2d1	cisterciácký
řádu	řád	k1gInSc2	řád
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
1142	[number]	k4	1142
<g/>
–	–	k?	–
<g/>
1420	[number]	k4	1420
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
401	[number]	k4	401
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
246	[number]	k4	246
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
385	[number]	k4	385
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CHARVÁTOVÁ	Charvátová	k1gFnSc1	Charvátová
<g/>
,	,	kIx,	,
Kateřina	Kateřina	k1gFnSc1	Kateřina
<g/>
.	.	kIx.	.
</s>
<s>
Zbraslav	Zbraslav	k1gFnSc1	Zbraslav
–	–	k?	–
pohřebiště	pohřebiště	k1gNnSc2	pohřebiště
posledních	poslední	k2eAgInPc2d1	poslední
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
a	a	k8xC	a
současnost	současnost	k1gFnSc1	současnost
<g/>
.	.	kIx.	.
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
,	,	kIx,	,
s.	s.	k?	s.
57	[number]	k4	57
<g/>
–	–	k?	–
<g/>
58	[number]	k4	58
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CHARVÁTOVÁ	Charvátová	k1gFnSc1	Charvátová
<g/>
,	,	kIx,	,
Kateřina	Kateřina	k1gFnSc1	Kateřina
<g/>
.	.	kIx.	.
</s>
<s>
Chronicon	Chronicon	k1gInSc1	Chronicon
Aulae	Aula	k1gFnSc2	Aula
Regiae	Regia	k1gFnSc2	Regia
jako	jako	k8xC	jako
klášterní	klášterní	k2eAgFnSc1d1	klášterní
kronika	kronika	k1gFnSc1	kronika
<g/>
.	.	kIx.	.
</s>
<s>
Marginalia	Marginalia	k1gFnSc1	Marginalia
historica	historic	k1gInSc2	historic
V.	V.	kA	V.
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
,	,	kIx,	,
s.	s.	k?	s.
307	[number]	k4	307
<g/>
–	–	k?	–
<g/>
355	[number]	k4	355
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KUTHAN	KUTHAN	k?	KUTHAN
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Počátky	počátek	k1gInPc1	počátek
a	a	k8xC	a
rozmach	rozmach	k1gInSc1	rozmach
gotické	gotický	k2eAgFnSc2d1	gotická
architektury	architektura	k1gFnSc2	architektura
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
:	:	kIx,	:
k	k	k7c3	k
problematice	problematika	k1gFnSc3	problematika
cisterciácké	cisterciácký	k2eAgFnSc2d1	cisterciácká
stavební	stavební	k2eAgFnSc2d1	stavební
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
375	[number]	k4	375
s.	s.	k?	s.
</s>
</p>
<p>
<s>
KUTHAN	KUTHAN	k?	KUTHAN
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Cisterciácký	cisterciácký	k2eAgInSc1d1	cisterciácký
klášter	klášter	k1gInSc1	klášter
a	a	k8xC	a
pohřebiště	pohřebiště	k1gNnSc1	pohřebiště
posledních	poslední	k2eAgMnPc2d1	poslední
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
na	na	k7c6	na
Zbraslavi	Zbraslav	k1gFnSc6	Zbraslav
<g/>
.	.	kIx.	.
</s>
<s>
Pražský	pražský	k2eAgInSc1d1	pražský
sborník	sborník	k1gInSc1	sborník
historický	historický	k2eAgInSc1d1	historický
<g/>
.	.	kIx.	.
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
,	,	kIx,	,
s.	s.	k?	s.
81	[number]	k4	81
<g/>
–	–	k?	–
<g/>
100	[number]	k4	100
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
0	[number]	k4	0
<g/>
555	[number]	k4	555
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
238	[number]	k4	238
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KUTHAN	KUTHAN	k?	KUTHAN
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Cisterciácké	cisterciácký	k2eAgInPc1d1	cisterciácký
kláštery	klášter	k1gInPc1	klášter
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
husitské	husitský	k2eAgNnSc4d1	husitské
obrazoborectví	obrazoborectví	k1gNnSc4	obrazoborectví
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
:	:	kIx,	:
Řada	řada	k1gFnSc1	řada
historická	historický	k2eAgFnSc1d1	historická
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
</s>
<s>
CLXIII	CLXIII	kA	CLXIII
<g/>
.	.	kIx.	.
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
</s>
<s>
CLXIII	CLXIII	kA	CLXIII
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
s.	s.	k?	s.
81	[number]	k4	81
<g/>
–	–	k?	–
<g/>
89	[number]	k4	89
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOTRBA	kotrba	k1gFnSc1	kotrba
<g/>
,	,	kIx,	,
Viktor	Viktor	k1gMnSc1	Viktor
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
barokní	barokní	k2eAgFnSc1d1	barokní
gotika	gotika	k1gFnSc1	gotika
:	:	kIx,	:
dílo	dílo	k1gNnSc1	dílo
Jana	Jan	k1gMnSc2	Jan
Santiniho-Aichla	Santiniho-Aichla	k1gMnSc2	Santiniho-Aichla
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
196	[number]	k4	196
s.	s.	k?	s.
</s>
</p>
<p>
<s>
KUČA	KUČA	k?	KUČA
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Díl	díl	k1gInSc1	díl
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
O	O	kA	O
<g/>
/	/	kIx~	/
<g/>
Ž.	Ž.	kA	Ž.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Baset	Baset	k1gMnSc1	Baset
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Atlas	Atlas	k1gInSc1	Atlas
památek	památka	k1gFnPc2	památka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86223	[number]	k4	86223
<g/>
-	-	kIx~	-
<g/>
42	[number]	k4	42
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KUTHAN	KUTHAN	k?	KUTHAN
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Cisterciácké	cisterciácký	k2eAgInPc4d1	cisterciácký
kláštery	klášter	k1gInPc4	klášter
jako	jako	k8xS	jako
pohřební	pohřební	k2eAgNnPc4d1	pohřební
místa	místo	k1gNnPc4	místo
vladařských	vladařský	k2eAgInPc2d1	vladařský
rodů	rod	k1gInPc2	rod
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
In	In	k?	In
<g/>
:	:	kIx,	:
KUBELÍK	KUBELÍK	kA	KUBELÍK
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
;	;	kIx,	;
PAVLÍK	Pavlík	k1gMnSc1	Pavlík
<g/>
,	,	kIx,	,
Milan	Milan	k1gMnSc1	Milan
<g/>
;	;	kIx,	;
ŠTULC	ŠTULC	kA	ŠTULC
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Historická	historický	k2eAgFnSc1d1	historická
inspirace	inspirace	k1gFnSc1	inspirace
:	:	kIx,	:
Sborník	sborník	k1gInSc1	sborník
k	k	k7c3	k
poctě	pocta	k1gFnSc3	pocta
Dobroslava	Dobroslav	k1gMnSc2	Dobroslav
Líbala	Líbal	k1gMnSc2	Líbal
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Jalna	Jaln	k1gInSc2	Jaln
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86396	[number]	k4	86396
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
199	[number]	k4	199
<g/>
–	–	k?	–
<g/>
214	[number]	k4	214
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KUTHAN	KUTHAN	k?	KUTHAN
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Středoevropské	středoevropský	k2eAgInPc1d1	středoevropský
metamorfozy	metamorfoz	k1gInPc1	metamorfoz
pravoúhlého	pravoúhlý	k2eAgInSc2d1	pravoúhlý
chóru	chór	k1gInSc2	chór
:	:	kIx,	:
K	k	k7c3	k
interpretaci	interpretace	k1gFnSc3	interpretace
archeologického	archeologický	k2eAgInSc2d1	archeologický
výzkumu	výzkum	k1gInSc2	výzkum
na	na	k7c6	na
Zbraslavi	Zbraslav	k1gFnSc6	Zbraslav
<g/>
.	.	kIx.	.
</s>
<s>
Časopis	časopis	k1gInSc1	časopis
Národního	národní	k2eAgNnSc2d1	národní
muzea	muzeum	k1gNnSc2	muzeum
<g/>
.	.	kIx.	.
1987	[number]	k4	1987
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
156	[number]	k4	156
<g/>
,	,	kIx,	,
s.	s.	k?	s.
33	[number]	k4	33
<g/>
–	–	k?	–
<g/>
70	[number]	k4	70
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NEUMANN	Neumann	k1gMnSc1	Neumann
<g/>
,	,	kIx,	,
Emanuel	Emanuel	k1gMnSc1	Emanuel
<g/>
.	.	kIx.	.
</s>
<s>
Pamětihodnosti	pamětihodnost	k1gFnPc1	pamětihodnost
Zbraslavě	Zbraslav	k1gFnSc2	Zbraslav
:	:	kIx,	:
Stručný	stručný	k2eAgInSc1d1	stručný
nástin	nástin	k1gInSc1	nástin
historický	historický	k2eAgInSc1d1	historický
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
průvodcem	průvodce	k1gMnSc7	průvodce
<g/>
,	,	kIx,	,
všem	všecek	k3xTgMnPc3	všecek
návštěvníkům	návštěvník	k1gMnPc3	návštěvník
Zbraslavě	Zbraslav	k1gFnSc2	Zbraslav
potřebný	potřebný	k2eAgInSc1d1	potřebný
<g/>
.	.	kIx.	.
</s>
<s>
Zbraslav	Zbraslav	k1gFnSc1	Zbraslav
<g/>
:	:	kIx,	:
Vondruška	Vondruška	k1gMnSc1	Vondruška
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
po	po	k7c6	po
r.	r.	kA	r.
1926	[number]	k4	1926
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Stručné	stručný	k2eAgFnPc1d1	stručná
dějiny	dějiny	k1gFnPc1	dějiny
zaměřené	zaměřený	k2eAgFnPc1d1	zaměřená
především	především	k9	především
na	na	k7c4	na
osudy	osud	k1gInPc4	osud
zbraslavského	zbraslavský	k2eAgInSc2d1	zbraslavský
kláštera	klášter	k1gInSc2	klášter
doplňuje	doplňovat	k5eAaImIp3nS	doplňovat
průvodce	průvodce	k1gMnSc1	průvodce
upozorňující	upozorňující	k2eAgMnSc1d1	upozorňující
na	na	k7c6	na
další	další	k2eAgFnSc6d1	další
pamětihodnosti	pamětihodnost	k1gFnSc6	pamětihodnost
této	tento	k3xDgFnSc2	tento
dnešní	dnešní	k2eAgFnSc2d1	dnešní
pražské	pražský	k2eAgFnSc2d1	Pražská
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NOVOTNÝ	Novotný	k1gMnSc1	Novotný
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
<g/>
.	.	kIx.	.
</s>
<s>
Klášter	klášter	k1gInSc1	klášter
zbraslavský	zbraslavský	k2eAgInSc1d1	zbraslavský
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
1941	[number]	k4	1941
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
PROKOPOVÁ	Prokopová	k1gFnSc1	Prokopová
<g/>
,	,	kIx,	,
Irena	Irena	k1gFnSc1	Irena
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
založení	založení	k1gNnSc4	založení
Zbraslavského	zbraslavský	k2eAgInSc2d1	zbraslavský
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Historický	historický	k2eAgInSc1d1	historický
obzor	obzor	k1gInSc1	obzor
<g/>
.	.	kIx.	.
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
/	/	kIx~	/
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
s.	s.	k?	s.
146	[number]	k4	146
<g/>
–	–	k?	–
<g/>
158	[number]	k4	158
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1210	[number]	k4	1210
<g/>
-	-	kIx~	-
<g/>
6097	[number]	k4	6097
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SEDLÁK	Sedlák	k1gMnSc1	Sedlák
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Blažej	Blažej	k1gMnSc1	Blažej
Santini	Santin	k2eAgMnPc1d1	Santin
:	:	kIx,	:
Setkání	setkání	k1gNnSc1	setkání
baroku	barok	k1gInSc2	barok
s	s	k7c7	s
gotikou	gotika	k1gFnSc7	gotika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Vyšehrad	Vyšehrad	k1gInSc1	Vyšehrad
<g/>
,	,	kIx,	,
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
251	[number]	k4	251
s.	s.	k?	s.
</s>
</p>
<p>
<s>
SPĚVÁČEK	Spěváček	k1gMnSc1	Spěváček
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Petr	Petr	k1gMnSc1	Petr
Žitavský	žitavský	k2eAgMnSc1d1	žitavský
<g/>
,	,	kIx,	,
a	a	k8xC	a
počátky	počátek	k1gInPc1	počátek
lucemburské	lucemburský	k2eAgFnSc2d1	Lucemburská
dynastie	dynastie	k1gFnSc2	dynastie
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Mediaevalia	Mediaevalia	k1gFnSc1	Mediaevalia
historica	historica	k1gFnSc1	historica
Bohemica	Bohemica	k1gFnSc1	Bohemica
<g/>
.	.	kIx.	.
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
,	,	kIx,	,
s.	s.	k?	s.
177	[number]	k4	177
<g/>
–	–	k?	–
<g/>
196	[number]	k4	196
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ŠTĚDRONSKÝ	ŠTĚDRONSKÝ	kA	ŠTĚDRONSKÝ
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Zbraslav	Zbraslav	k1gFnSc1	Zbraslav
<g/>
,	,	kIx,	,
pohřebiště	pohřebiště	k1gNnPc1	pohřebiště
českých	český	k2eAgMnPc2d1	český
králů	král	k1gMnPc2	král
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Okresní	okresní	k2eAgNnSc1d1	okresní
sdružení	sdružení	k1gNnSc1	sdružení
katolické	katolický	k2eAgFnSc2d1	katolická
mládeže	mládež	k1gFnSc2	mládež
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VÁCHA	Vácha	k1gMnSc1	Vácha
<g/>
,	,	kIx,	,
Štěpán	Štěpán	k1gMnSc1	Štěpán
<g/>
.	.	kIx.	.
</s>
<s>
De	De	k?	De
regalibus	regalibus	k1gMnSc1	regalibus
honoribus	honoribus	k1gMnSc1	honoribus
in	in	k?	in
Aula	aula	k1gFnSc1	aula
Regia	Regia	k1gFnSc1	Regia
:	:	kIx,	:
Ikonografie	ikonografie	k1gFnSc1	ikonografie
Reinerových	Reinerův	k2eAgFnPc2d1	Reinerova
fresek	freska	k1gFnPc2	freska
v	v	k7c6	v
Královském	královský	k2eAgInSc6d1	královský
sále	sál	k1gInSc6	sál
a	a	k8xC	a
v	v	k7c6	v
prelatuře	prelatura	k1gFnSc6	prelatura
cisterciáckého	cisterciácký	k2eAgInSc2d1	cisterciácký
kláštera	klášter	k1gInSc2	klášter
na	na	k7c6	na
Zbraslavi	Zbraslav	k1gFnSc6	Zbraslav
<g/>
.	.	kIx.	.
</s>
<s>
Umění	umění	k1gNnSc1	umění
<g/>
.	.	kIx.	.
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
51	[number]	k4	51
<g/>
,	,	kIx,	,
s.	s.	k?	s.
13	[number]	k4	13
<g/>
–	–	k?	–
<g/>
29	[number]	k4	29
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VLČEK	Vlček	k1gMnSc1	Vlček
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
;	;	kIx,	;
SOMMER	Sommer	k1gMnSc1	Sommer
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
;	;	kIx,	;
FOLTÝN	Foltýn	k1gMnSc1	Foltýn
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
<g/>
.	.	kIx.	.
</s>
<s>
Encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
českých	český	k2eAgInPc2d1	český
klášterů	klášter	k1gInPc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
782	[number]	k4	782
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85983	[number]	k4	85983
<g/>
-	-	kIx~	-
<g/>
17	[number]	k4	17
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Zbraslavský	zbraslavský	k2eAgInSc4d1	zbraslavský
klášter	klášter	k1gInSc4	klášter
a	a	k8xC	a
zámek	zámek	k1gInSc4	zámek
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Mapa	mapa	k1gFnSc1	mapa
českých	český	k2eAgInPc2d1	český
a	a	k8xC	a
moravských	moravský	k2eAgInPc2d1	moravský
cisterciáckých	cisterciácký	k2eAgInPc2d1	cisterciácký
klášterů	klášter	k1gInPc2	klášter
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Cisterciácké	cisterciácký	k2eAgNnSc4d1	cisterciácké
opatství	opatství	k1gNnSc4	opatství
Vyšší	vysoký	k2eAgInSc1d2	vyšší
Brod	Brod	k1gInSc1	Brod
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
17	[number]	k4	17
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2007	[number]	k4	2007
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mapa	mapa	k1gFnSc1	mapa
rozšíření	rozšíření	k1gNnSc2	rozšíření
rané	raný	k2eAgFnSc2d1	raná
cisterciácké	cisterciácký	k2eAgFnSc2d1	cisterciácká
architektury	architektura	k1gFnSc2	architektura
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Cisterciácké	cisterciácký	k2eAgNnSc4d1	cisterciácké
opatství	opatství	k1gNnSc4	opatství
Vyšší	vysoký	k2eAgInSc1d2	vyšší
Brod	Brod	k1gInSc1	Brod
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
17	[number]	k4	17
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2007	[number]	k4	2007
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mapa	mapa	k1gFnSc1	mapa
nejvýznamnějších	významný	k2eAgInPc2d3	nejvýznamnější
cisterciáckých	cisterciácký	k2eAgInPc2d1	cisterciácký
klášterů	klášter	k1gInPc2	klášter
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Cisterciácké	cisterciácký	k2eAgNnSc4d1	cisterciácké
opatství	opatství	k1gNnSc4	opatství
Vyšší	vysoký	k2eAgInSc1d2	vyšší
Brod	Brod	k1gInSc1	Brod
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
17	[number]	k4	17
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2007	[number]	k4	2007
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
Petrus	Petrus	k1gMnSc1	Petrus
Zittaviensis	Zittaviensis	k1gFnSc2	Zittaviensis
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
Novotný	Novotný	k1gMnSc1	Novotný
<g/>
.	.	kIx.	.
</s>
<s>
Kronika	kronika	k1gFnSc1	kronika
Zbraslavská	zbraslavský	k2eAgFnSc1d1	Zbraslavská
[	[	kIx(	[
<g/>
PDF	PDF	kA	PDF
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Nadání	nadání	k1gNnSc1	nadání
F.	F.	kA	F.
Palackého	Palacký	k1gMnSc2	Palacký
<g/>
,	,	kIx,	,
1905	[number]	k4	1905
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
17	[number]	k4	17
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chrám	chrám	k1gInSc1	chrám
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
na	na	k7c6	na
Zbraslavi	Zbraslav	k1gFnSc6	Zbraslav
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
www.zbraslavhistorie.info	www.zbraslavhistorie.info	k1gNnSc1	www.zbraslavhistorie.info
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
17	[number]	k4	17
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
