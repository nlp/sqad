<s>
Frambézie	Frambézie	k1gFnPc4	Frambézie
je	být	k5eAaImIp3nS	být
přenosné	přenosný	k2eAgNnSc1d1	přenosné
kožní	kožní	k2eAgNnSc1d1	kožní
onemocnění	onemocnění	k1gNnSc1	onemocnění
bakteriálního	bakteriální	k2eAgInSc2d1	bakteriální
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
postihuje	postihovat	k5eAaImIp3nS	postihovat
zejména	zejména	k9	zejména
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
podařilo	podařit	k5eAaPmAgNnS	podařit
téměř	téměř	k6eAd1	téměř
vymýtit	vymýtit	k5eAaPmF	vymýtit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
znovu	znovu	k6eAd1	znovu
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
nejchudších	chudý	k2eAgFnPc6d3	nejchudší
oblastech	oblast	k1gFnPc6	oblast
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
trpí	trpět	k5eAaImIp3nP	trpět
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
frambézií	frambézie	k1gFnPc2	frambézie
na	na	k7c4	na
půl	půl	k1xP	půl
milionu	milion	k4xCgInSc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Situace	situace	k1gFnSc1	situace
byla	být	k5eAaImAgFnS	být
nejkritičtější	kritický	k2eAgFnSc1d3	nejkritičtější
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
s	s	k7c7	s
frambézií	frambézie	k1gFnSc7	frambézie
potýkalo	potýkat	k5eAaImAgNnS	potýkat
na	na	k7c4	na
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgFnSc1d1	světová
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
organizace	organizace	k1gFnSc1	organizace
(	(	kIx(	(
<g/>
WHO	WHO	kA	WHO
<g/>
)	)	kIx)	)
proto	proto	k8xC	proto
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
dětským	dětský	k2eAgInSc7d1	dětský
fondem	fond	k1gInSc7	fond
UNICEF	UNICEF	kA	UNICEF
zahájila	zahájit	k5eAaPmAgFnS	zahájit
masivní	masivní	k2eAgInSc4d1	masivní
program	program	k1gInSc4	program
kontroly	kontrola	k1gFnSc2	kontrola
a	a	k8xC	a
eliminace	eliminace	k1gFnSc2	eliminace
choroby	choroba	k1gFnSc2	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Program	program	k1gInSc1	program
fungoval	fungovat	k5eAaImAgInS	fungovat
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1952	[number]	k4	1952
a	a	k8xC	a
1964	[number]	k4	1964
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
díky	díky	k7c3	díky
němu	on	k3xPp3gMnSc3	on
vymýceno	vymýtit	k5eAaPmNgNnS	vymýtit
95	[number]	k4	95
%	%	kIx~	%
případů	případ	k1gInPc2	případ
frambézie	frambézie	k1gFnSc2	frambézie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
úspěchu	úspěch	k1gInSc6	úspěch
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
pozornost	pozornost	k1gFnSc1	pozornost
od	od	k7c2	od
nemoci	nemoc	k1gFnSc2	nemoc
odvrátila	odvrátit	k5eAaPmAgFnS	odvrátit
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
vrací	vracet	k5eAaImIp3nS	vracet
<g/>
.	.	kIx.	.
</s>
<s>
Frambézie	Frambézie	k1gFnSc1	Frambézie
se	se	k3xPyFc4	se
přenáší	přenášet	k5eAaImIp3nS	přenášet
kontaktem	kontakt	k1gInSc7	kontakt
s	s	k7c7	s
nakaženým	nakažený	k2eAgMnSc7d1	nakažený
<g/>
,	,	kIx,	,
především	především	k9	především
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
poranění	poranění	k1gNnSc2	poranění
kůže	kůže	k1gFnSc2	kůže
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
bakterie	bakterie	k1gFnSc1	bakterie
proniká	pronikat	k5eAaImIp3nS	pronikat
do	do	k7c2	do
organismu	organismus	k1gInSc2	organismus
dalšího	další	k2eAgMnSc2d1	další
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Postihuje	postihovat	k5eAaImIp3nS	postihovat
hlavně	hlavně	k9	hlavně
mladé	mladý	k2eAgMnPc4d1	mladý
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
děti	dítě	k1gFnPc1	dítě
do	do	k7c2	do
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
a	a	k8xC	a
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
hluboké	hluboký	k2eAgFnPc4d1	hluboká
rány	rána	k1gFnPc4	rána
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
těžké	těžký	k2eAgFnSc2d1	těžká
deformace	deformace	k1gFnSc2	deformace
kůže	kůže	k1gFnSc2	kůže
<g/>
,	,	kIx,	,
měkkých	měkký	k2eAgFnPc2d1	měkká
tkání	tkáň	k1gFnPc2	tkáň
<g/>
,	,	kIx,	,
kostí	kost	k1gFnPc2	kost
a	a	k8xC	a
chrupavek	chrupavka	k1gFnPc2	chrupavka
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Výskyt	výskyt	k1gInSc1	výskyt
frambézie	frambézie	k1gFnSc2	frambézie
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
naprosto	naprosto	k6eAd1	naprosto
neakceptovatelný	akceptovatelný	k2eNgMnSc1d1	neakceptovatelný
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
uvedl	uvést	k5eAaPmAgMnS	uvést
lékař	lékař	k1gMnSc1	lékař
Lorenzo	Lorenza	k1gFnSc5	Lorenza
Savioli	Saviole	k1gFnSc3	Saviole
z	z	k7c2	z
WHO	WHO	kA	WHO
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zdůraznil	zdůraznit	k5eAaPmAgMnS	zdůraznit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
existuje	existovat	k5eAaImIp3nS	existovat
levná	levný	k2eAgFnSc1d1	levná
a	a	k8xC	a
účinná	účinný	k2eAgFnSc1d1	účinná
léčba	léčba	k1gFnSc1	léčba
<g/>
.	.	kIx.	.
</s>
<s>
Frambézie	Frambézie	k1gFnPc1	Frambézie
se	se	k3xPyFc4	se
léčí	léčit	k5eAaImIp3nP	léčit
jedinou	jediný	k2eAgFnSc7d1	jediná
dávkou	dávka	k1gFnSc7	dávka
penicilinu	penicilin	k1gInSc2	penicilin
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Frambézie	Frambézie	k1gFnSc2	Frambézie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Léčba	léčba	k1gFnSc1	léčba
frambézie	frambézie	k1gFnSc1	frambézie
v	v	k7c6	v
komunitách	komunita	k1gFnPc6	komunita
Pygmejů	Pygmej	k1gMnPc2	Pygmej
v	v	k7c6	v
Kongu	Kongo	k1gNnSc6	Kongo
<g/>
,	,	kIx,	,
Lékaři	lékař	k1gMnPc1	lékař
bez	bez	k7c2	bez
hranic	hranice	k1gFnPc2	hranice
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
</s>
