<s>
Vášeň	vášeň	k1gFnSc1	vášeň
je	být	k5eAaImIp3nS	být
stupeň	stupeň	k1gInSc4	stupeň
emoce	emoce	k1gFnSc2	emoce
(	(	kIx(	(
<g/>
intenzivní	intenzivní	k2eAgFnSc1d1	intenzivní
<g/>
,	,	kIx,	,
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
emoce	emoce	k1gFnSc1	emoce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
míní	mínit	k5eAaImIp3nS	mínit
přilnutí	přilnutí	k1gNnPc4	přilnutí
k	k	k7c3	k
předmětu	předmět	k1gInSc3	předmět
vášně	vášeň	k1gFnSc2	vášeň
(	(	kIx(	(
<g/>
člověku	člověk	k1gMnSc6	člověk
<g/>
,	,	kIx,	,
myšlence	myšlenka	k1gFnSc6	myšlenka
<g/>
,	,	kIx,	,
věci	věc	k1gFnSc6	věc
<g/>
,	,	kIx,	,
popřípadě	popřípadě	k6eAd1	popřípadě
hodnotě	hodnota	k1gFnSc3	hodnota
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
velice	velice	k6eAd1	velice
výrazně	výrazně	k6eAd1	výrazně
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
myšlení	myšlení	k1gNnSc4	myšlení
a	a	k8xC	a
jednání	jednání	k1gNnSc4	jednání
jedince	jedinec	k1gMnSc2	jedinec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zdrojem	zdroj	k1gInSc7	zdroj
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
vytrvalosti	vytrvalost	k1gFnSc2	vytrvalost
a	a	k8xC	a
vůle	vůle	k1gFnSc2	vůle
<g/>
.	.	kIx.	.
</s>
<s>
Emoce	emoce	k1gFnSc1	emoce
Láska	láska	k1gFnSc1	láska
Nálada	nálada	k1gFnSc1	nálada
Afekt	afekt	k1gInSc4	afekt
Poruchy	porucha	k1gFnSc2	porucha
emotivity	emotivita	k1gFnSc2	emotivita
Touha	touha	k1gFnSc1	touha
Téma	téma	k1gFnSc1	téma
Vášeň	vášeň	k1gFnSc1	vášeň
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
