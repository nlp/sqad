<s>
Parmenidés	Parmenidés	k1gInSc1	Parmenidés
ostře	ostro	k6eAd1	ostro
rozlišil	rozlišit	k5eAaPmAgInS	rozlišit
jsoucí	jsoucí	k2eAgInSc1d1	jsoucí
a	a	k8xC	a
nejsoucí	nejsoucí	k2eAgInSc1d1	nejsoucí
a	a	k8xC	a
domníval	domnívat	k5eAaImAgInS	domnívat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
jest	být	k5eAaImIp3nS	být
jen	jen	k9	jen
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
je	být	k5eAaImIp3nS	být
vždycky	vždycky	k6eAd1	vždycky
a	a	k8xC	a
stále	stále	k6eAd1	stále
stejné	stejný	k2eAgNnSc1d1	stejné
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
věci	věc	k1gFnPc1	věc
pomíjivé	pomíjivý	k2eAgFnPc1d1	pomíjivá
a	a	k8xC	a
proměnlivé	proměnlivý	k2eAgFnPc1d1	proměnlivá
pokládal	pokládat	k5eAaImAgMnS	pokládat
za	za	k7c4	za
nejsoucí	jsoucí	k2eNgFnPc4d1	jsoucí
<g/>
,	,	kIx,	,
klamné	klamný	k2eAgFnPc4d1	klamná
<g/>
.	.	kIx.	.
</s>
