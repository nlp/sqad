<s>
Atatürkovy	Atatürkův	k2eAgFnPc1d1	Atatürkova
reformy	reforma	k1gFnPc1	reforma
začaly	začít	k5eAaPmAgFnP	začít
modernizací	modernizace	k1gFnSc7	modernizace
ústavního	ústavní	k2eAgNnSc2d1	ústavní
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
vč.	vč.	k?	vč.
přijetí	přijetí	k1gNnSc4	přijetí
nové	nový	k2eAgFnSc2d1	nová
turecké	turecký	k2eAgFnSc2d1	turecká
ústavy	ústava	k1gFnSc2	ústava
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nahradila	nahradit	k5eAaPmAgFnS	nahradit
ústavu	ústava	k1gFnSc4	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
a	a	k8xC	a
adaptací	adaptace	k1gFnSc7	adaptace
evropského	evropský	k2eAgNnSc2d1	Evropské
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
jurisprudence	jurisprudence	k1gFnSc2	jurisprudence
potřebám	potřeba	k1gFnPc3	potřeba
nové	nový	k2eAgFnSc2d1	nová
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
