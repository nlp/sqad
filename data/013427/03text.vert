<p>
<s>
Atatürkovy	Atatürkův	k2eAgFnPc1d1	Atatürkova
reformy	reforma	k1gFnPc1	reforma
(	(	kIx(	(
<g/>
turecky	turecky	k6eAd1	turecky
Atatürk	Atatürk	k1gInSc1	Atatürk
Devrimleri	Devrimler	k1gFnSc2	Devrimler
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
sérií	série	k1gFnSc7	série
politických	politický	k2eAgFnPc2d1	politická
<g/>
,	,	kIx,	,
právních	právní	k2eAgFnPc2d1	právní
<g/>
,	,	kIx,	,
kulturních	kulturní	k2eAgFnPc2d1	kulturní
<g/>
,	,	kIx,	,
sociálních	sociální	k2eAgFnPc2d1	sociální
a	a	k8xC	a
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
připraveny	připravit	k5eAaPmNgInP	připravit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
Turecká	turecký	k2eAgFnSc1d1	turecká
republika	republika	k1gFnSc1	republika
stala	stát	k5eAaPmAgFnS	stát
sekulárním	sekulární	k2eAgInSc7d1	sekulární
<g/>
,	,	kIx,	,
moderním	moderní	k2eAgInSc7d1	moderní
a	a	k8xC	a
národním	národní	k2eAgInSc7d1	národní
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
reformy	reforma	k1gFnPc1	reforma
byly	být	k5eAaImAgFnP	být
zavedeny	zaveden	k2eAgFnPc1d1	zavedena
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Mustafy	Mustaf	k1gMnPc7	Mustaf
Kemala	Kemal	k1gMnSc2	Kemal
Atatürka	Atatürek	k1gMnSc2	Atatürek
dle	dle	k7c2	dle
Kemalistické	Kemalistický	k2eAgFnSc2d1	Kemalistický
ideologie	ideologie	k1gFnSc2	ideologie
<g/>
.	.	kIx.	.
</s>
<s>
Středobodem	středobod	k1gInSc7	středobod
těchto	tento	k3xDgFnPc2	tento
reforem	reforma	k1gFnPc2	reforma
bylo	být	k5eAaImAgNnS	být
přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
turecká	turecký	k2eAgFnSc1d1	turecká
společnost	společnost	k1gFnSc1	společnost
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
muset	muset	k5eAaImF	muset
přiblížit	přiblížit	k5eAaPmF	přiblížit
západu	západ	k1gInSc3	západ
politicky	politicky	k6eAd1	politicky
i	i	k9	i
kulturně	kulturně	k6eAd1	kulturně
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
chce	chtít	k5eAaImIp3nS	chtít
modernizovat	modernizovat	k5eAaBmF	modernizovat
<g/>
.	.	kIx.	.
</s>
<s>
Atatürkovy	Atatürkův	k2eAgFnSc2d1	Atatürkova
politické	politický	k2eAgFnSc2d1	politická
reformy	reforma	k1gFnSc2	reforma
obsahovaly	obsahovat	k5eAaImAgFnP	obsahovat
mnoho	mnoho	k4c4	mnoho
fundamentálních	fundamentální	k2eAgFnPc2d1	fundamentální
institucionálních	institucionální	k2eAgFnPc2d1	institucionální
změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
znamenaly	znamenat	k5eAaImAgFnP	znamenat
konec	konec	k1gInSc4	konec
mnoha	mnoho	k4c2	mnoho
tradic	tradice	k1gFnPc2	tradice
<g/>
,	,	kIx,	,
a	a	k8xC	a
šetrně	šetrně	k6eAd1	šetrně
připravený	připravený	k2eAgInSc4d1	připravený
program	program	k1gInSc4	program
postupných	postupný	k2eAgFnPc2d1	postupná
změn	změna	k1gFnPc2	změna
byl	být	k5eAaImAgInS	být
implementován	implementován	k2eAgMnSc1d1	implementován
k	k	k7c3	k
rozpletení	rozpletení	k1gNnSc3	rozpletení
složitého	složitý	k2eAgInSc2d1	složitý
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
vyvíjel	vyvíjet	k5eAaImAgInS	vyvíjet
po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
<g/>
.	.	kIx.	.
</s>
<s>
Atatürkovy	Atatürkův	k2eAgFnPc1d1	Atatürkova
reformy	reforma	k1gFnPc1	reforma
začaly	začít	k5eAaPmAgFnP	začít
modernizací	modernizace	k1gFnSc7	modernizace
ústavního	ústavní	k2eAgNnSc2d1	ústavní
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
vč.	vč.	k?	vč.
přijetí	přijetí	k1gNnSc4	přijetí
nové	nový	k2eAgFnSc2d1	nová
turecké	turecký	k2eAgFnSc2d1	turecká
ústavy	ústava	k1gFnSc2	ústava
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nahradila	nahradit	k5eAaPmAgFnS	nahradit
ústavu	ústava	k1gFnSc4	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
a	a	k8xC	a
adaptací	adaptace	k1gFnSc7	adaptace
evropského	evropský	k2eAgNnSc2d1	Evropské
práva	právo	k1gNnSc2	právo
a	a	k8xC	a
jurisprudence	jurisprudence	k1gFnSc2	jurisprudence
potřebám	potřeba	k1gFnPc3	potřeba
nové	nový	k2eAgFnSc2d1	nová
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
kompletní	kompletní	k2eAgFnSc1d1	kompletní
sekularizace	sekularizace	k1gFnSc1	sekularizace
a	a	k8xC	a
modernizace	modernizace	k1gFnSc1	modernizace
administrativy	administrativa	k1gFnSc2	administrativa
<g/>
,	,	kIx,	,
se	s	k7c7	s
zaměřením	zaměření	k1gNnSc7	zaměření
především	především	k9	především
na	na	k7c4	na
vzdělávací	vzdělávací	k2eAgInSc4d1	vzdělávací
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Historicky	historicky	k6eAd1	historicky
Atatürkovy	Atatürkův	k2eAgFnPc1d1	Atatürkova
reformy	reforma	k1gFnPc1	reforma
nastupují	nastupovat	k5eAaImIp3nP	nastupovat
po	po	k7c6	po
období	období	k1gNnSc6	období
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
nazvaném	nazvaný	k2eAgNnSc6d1	nazvané
Tanzimat	Tanzimat	k1gInSc4	Tanzimat
-	-	kIx~	-
reorganizaci	reorganizace	k1gFnSc4	reorganizace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
začala	začít	k5eAaPmAgFnS	začít
roku	rok	k1gInSc2	rok
1839	[number]	k4	1839
a	a	k8xC	a
skončila	skončit	k5eAaPmAgFnS	skončit
První	první	k4xOgFnSc7	první
konstituční	konstituční	k2eAgFnSc7d1	konstituční
érou	éra	k1gFnSc7	éra
roku	rok	k1gInSc2	rok
1876	[number]	k4	1876
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
podobně	podobně	k6eAd1	podobně
výrazné	výrazný	k2eAgFnPc4d1	výrazná
reformní	reformní	k2eAgFnPc4d1	reformní
změny	změna	k1gFnPc4	změna
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
společenském	společenský	k2eAgNnSc6d1	společenské
<g/>
,	,	kIx,	,
ekonomickém	ekonomický	k2eAgNnSc6d1	ekonomické
i	i	k8xC	i
administrativním	administrativní	k2eAgNnSc6d1	administrativní
přišly	přijít	k5eAaPmAgFnP	přijít
s	s	k7c7	s
procesem	proces	k1gInSc7	proces
přístupu	přístup	k1gInSc2	přístup
Turecké	turecký	k2eAgFnSc2d1	turecká
republiky	republika	k1gFnSc2	republika
k	k	k7c3	k
Evropské	evropský	k2eAgFnSc3d1	Evropská
unii	unie	k1gFnSc3	unie
(	(	kIx(	(
<g/>
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Atatürk	Atatürk	k1gInSc1	Atatürk
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Reforms	Reformsa	k1gFnPc2	Reformsa
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Bein	Bein	k1gMnSc1	Bein
<g/>
,	,	kIx,	,
Amit	Amit	k1gMnSc1	Amit
<g/>
.	.	kIx.	.
</s>
<s>
Ottoman	Ottoman	k1gMnSc1	Ottoman
Ulema	ulema	k1gMnSc1	ulema
<g/>
,	,	kIx,	,
Turkish	Turkish	k1gMnSc1	Turkish
Republic	Republice	k1gFnPc2	Republice
<g/>
:	:	kIx,	:
Agents	Agents	k1gInSc1	Agents
of	of	k?	of
Change	change	k1gFnSc2	change
and	and	k?	and
Guardians	Guardians	k1gInSc1	Guardians
of	of	k?	of
Tradition	Tradition	k1gInSc1	Tradition
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Amazon	amazona	k1gFnPc2	amazona
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
Ergin	Ergin	k1gMnSc1	Ergin
<g/>
,	,	kIx,	,
Murat	Murat	k2eAgMnSc1d1	Murat
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Cultural	Cultural	k1gFnSc1	Cultural
encounters	encounters	k1gInSc1	encounters
in	in	k?	in
the	the	k?	the
social	social	k1gMnSc1	social
sciences	sciences	k1gMnSc1	sciences
and	and	k?	and
humanities	humanities	k1gMnSc1	humanities
<g/>
:	:	kIx,	:
western	western	k1gInSc1	western
émigré	émigrý	k2eAgFnSc2d1	émigrý
scholars	scholars	k1gInSc4	scholars
in	in	k?	in
Turkey	Turkea	k1gFnSc2	Turkea
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
History	Histor	k1gMnPc4	Histor
of	of	k?	of
the	the	k?	the
Human	Human	k1gMnSc1	Human
Sciences	Sciences	k1gMnSc1	Sciences
<g/>
,	,	kIx,	,
Feb	Feb	k1gFnSc1	Feb
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
22	[number]	k4	22
Issue	Issu	k1gInSc2	Issu
1	[number]	k4	1
<g/>
,	,	kIx,	,
pp	pp	k?	pp
105	[number]	k4	105
<g/>
–	–	k?	–
<g/>
130	[number]	k4	130
</s>
</p>
<p>
<s>
Hansen	Hansen	k1gInSc1	Hansen
<g/>
,	,	kIx,	,
Craig	Craig	k1gInSc1	Craig
C.	C.	kA	C.
"	"	kIx"	"
<g/>
Are	ar	k1gInSc5	ar
We	We	k1gMnSc6	We
Doing	Doing	k1gMnSc1	Doing
Theory	Theora	k1gFnSc2	Theora
Ethnocentrically	Ethnocentricalla	k1gFnSc2	Ethnocentricalla
<g/>
?	?	kIx.	?
</s>
<s>
A	a	k9	a
Comparison	Comparison	k1gMnSc1	Comparison
of	of	k?	of
Modernization	Modernization	k1gInSc1	Modernization
Theory	Theora	k1gFnSc2	Theora
and	and	k?	and
Kemalism	Kemalism	k1gInSc1	Kemalism
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
Journal	Journal	k1gFnSc1	Journal
of	of	k?	of
Developing	Developing	k1gInSc1	Developing
Societies	Societies	k1gInSc1	Societies
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
169796	[number]	k4	169796
<g/>
X	X	kA	X
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
Vol	vol	k6eAd1	vol
<g/>
.	.	kIx.	.
5	[number]	k4	5
Issue	Issu	k1gInSc2	Issu
2	[number]	k4	2
<g/>
,	,	kIx,	,
pp	pp	k?	pp
175	[number]	k4	175
<g/>
–	–	k?	–
<g/>
187	[number]	k4	187
</s>
</p>
<p>
<s>
Hanioğ	Hanioğ	k?	Hanioğ
<g/>
,	,	kIx,	,
M.	M.	kA	M.
Şükrü	Şükrü	k1gFnSc1	Şükrü
<g/>
.	.	kIx.	.
</s>
<s>
Atatürk	Atatürk	k1gInSc1	Atatürk
<g/>
:	:	kIx,	:
An	An	k1gFnPc1	An
intellectual	intellectual	k1gInSc4	intellectual
biography	biographa	k1gFnSc2	biographa
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Amazon	amazona	k1gFnPc2	amazona
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
Kazancı	Kazancı	k?	Kazancı
<g/>
,	,	kIx,	,
Ali	Ali	k1gMnSc1	Ali
and	and	k?	and
Ergün	Ergün	k1gMnSc1	Ergün
Özbudun	Özbudun	k1gMnSc1	Özbudun
<g/>
.	.	kIx.	.
</s>
<s>
Atatürk	Atatürk	k1gInSc1	Atatürk
<g/>
:	:	kIx,	:
Founder	Founder	k1gInSc1	Founder
of	of	k?	of
a	a	k8xC	a
Modern	Modern	k1gMnSc1	Modern
State	status	k1gInSc5	status
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
243	[number]	k4	243
<g/>
pp	pp	k?	pp
</s>
</p>
<p>
<s>
Ward	Ward	k1gMnSc1	Ward
<g/>
,	,	kIx,	,
Robert	Robert	k1gMnSc1	Robert
<g/>
,	,	kIx,	,
and	and	k?	and
Dankwart	Dankwart	k1gInSc1	Dankwart
Rustow	Rustow	k1gFnSc1	Rustow
<g/>
,	,	kIx,	,
eds	eds	k?	eds
<g/>
.	.	kIx.	.
</s>
<s>
Political	Politicat	k5eAaPmAgInS	Politicat
Modernization	Modernization	k1gInSc1	Modernization
in	in	k?	in
Japan	japan	k1gInSc1	japan
and	and	k?	and
Turkey	Turkea	k1gFnSc2	Turkea
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Yavuz	Yavuz	k1gMnSc1	Yavuz
<g/>
,	,	kIx,	,
M.	M.	kA	M.
Hakan	Hakan	k1gInSc1	Hakan
<g/>
.	.	kIx.	.
</s>
<s>
Islamic	Islamic	k1gMnSc1	Islamic
Political	Political	k1gMnSc1	Political
Identity	identita	k1gFnSc2	identita
in	in	k?	in
Turkey	Turkea	k1gFnSc2	Turkea
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Amazon	amazona	k1gFnPc2	amazona
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
Zurcher	Zurchra	k1gFnPc2	Zurchra
<g/>
,	,	kIx,	,
Erik	Erika	k1gFnPc2	Erika
<g/>
.	.	kIx.	.
</s>
<s>
Turkey	Turkea	k1gFnPc1	Turkea
<g/>
:	:	kIx,	:
A	a	k9	a
Modern	Modern	k1gInSc1	Modern
History	Histor	k1gInPc1	Histor
(	(	kIx(	(
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
Amazon	amazona	k1gFnPc2	amazona
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Atatürkovy	Atatürkův	k2eAgFnSc2d1	Atatürkova
reformy	reforma	k1gFnSc2	reforma
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
