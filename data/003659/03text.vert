<s>
Polyvinylchlorid	polyvinylchlorid	k1gInSc1	polyvinylchlorid
(	(	kIx(	(
<g/>
PVC	PVC	kA	PVC
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgFnSc7	třetí
nejpoužívanější	používaný	k2eAgFnSc7d3	nejpoužívanější
umělou	umělý	k2eAgFnSc7d1	umělá
hmotou	hmota	k1gFnSc7	hmota
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
po	po	k7c6	po
polyethylenu	polyethylen	k1gInSc6	polyethylen
a	a	k8xC	a
polypropylenu	polypropylen	k1gInSc6	polypropylen
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byl	být	k5eAaImAgInS	být
syntetizován	syntetizován	k2eAgInSc1d1	syntetizován
roku	rok	k1gInSc2	rok
1935	[number]	k4	1935
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
rozpustný	rozpustný	k2eAgMnSc1d1	rozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
v	v	k7c6	v
olejích	olej	k1gInPc6	olej
ani	ani	k8xC	ani
v	v	k7c6	v
koncentrovaných	koncentrovaný	k2eAgFnPc6d1	koncentrovaná
anorganických	anorganický	k2eAgFnPc6d1	anorganická
kyselinách	kyselina	k1gFnPc6	kyselina
a	a	k8xC	a
zásadách	zásada	k1gFnPc6	zásada
<g/>
.	.	kIx.	.
57	[number]	k4	57
%	%	kIx~	%
jeho	jeho	k3xOp3gFnSc2	jeho
molekulové	molekulový	k2eAgFnSc2d1	molekulová
hmotnosti	hmotnost	k1gFnSc2	hmotnost
tvoří	tvořit	k5eAaImIp3nS	tvořit
samotný	samotný	k2eAgInSc1d1	samotný
chlór	chlór	k1gInSc1	chlór
<g/>
,	,	kIx,	,
43	[number]	k4	43
%	%	kIx~	%
ethen	ethna	k1gFnPc2	ethna
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
surovinou	surovina	k1gFnSc7	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
PVC	PVC	kA	PVC
je	být	k5eAaImIp3nS	být
vinylchlorid	vinylchlorid	k1gInSc1	vinylchlorid
monomer	monomer	k1gInSc1	monomer
(	(	kIx(	(
<g/>
VCM	VCM	kA	VCM
–	–	k?	–
těkavý	těkavý	k2eAgMnSc1d1	těkavý
<g/>
,	,	kIx,	,
jemně	jemně	k6eAd1	jemně
nasládlý	nasládlý	k2eAgInSc1d1	nasládlý
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
teplota	teplota	k1gFnSc1	teplota
varu	var	k1gInSc2	var
-13,9	-13,9	k4	-13,9
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzniklý	vzniklý	k2eAgMnSc1d1	vzniklý
obvykle	obvykle	k6eAd1	obvykle
rozkladem	rozklad	k1gInSc7	rozklad
1,2	[number]	k4	1,2
<g/>
-dichlorethanu	ichlorethan	k1gInSc2	-dichlorethan
(	(	kIx(	(
<g/>
starší	starý	k2eAgInPc1d2	starší
postupy	postup	k1gInPc1	postup
vycházely	vycházet	k5eAaImAgFnP	vycházet
z	z	k7c2	z
acetylenu	acetylen	k1gInSc2	acetylen
a	a	k8xC	a
HCl	HCl	k1gFnSc2	HCl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
polymerací	polymerace	k1gFnSc7	polymerace
do	do	k7c2	do
řetězců	řetězec	k1gInPc2	řetězec
<g/>
,	,	kIx,	,
jen	jen	k9	jen
částečně	částečně	k6eAd1	částečně
se	se	k3xPyFc4	se
rozvětvujících	rozvětvující	k2eAgFnPc2d1	rozvětvující
<g/>
,	,	kIx,	,
vzniká	vznikat	k5eAaImIp3nS	vznikat
bílý	bílý	k2eAgInSc1d1	bílý
prášek	prášek	k1gInSc1	prášek
nebo	nebo	k8xC	nebo
granulát	granulát	k1gInSc1	granulát
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
polymer	polymer	k1gInSc1	polymer
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
mísí	mísit	k5eAaImIp3nP	mísit
s	s	k7c7	s
různými	různý	k2eAgFnPc7d1	různá
přísadami	přísada	k1gFnPc7	přísada
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zlepšují	zlepšovat	k5eAaImIp3nP	zlepšovat
jeho	jeho	k3xOp3gFnPc1	jeho
vlastnosti	vlastnost	k1gFnPc1	vlastnost
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
kterém	který	k3yRgInSc6	který
směru	směr	k1gInSc6	směr
podle	podle	k7c2	podle
cílového	cílový	k2eAgNnSc2d1	cílové
použití	použití	k1gNnSc2	použití
<g/>
.	.	kIx.	.
</s>
<s>
Těmito	tento	k3xDgFnPc7	tento
přísadami	přísada	k1gFnPc7	přísada
jsou	být	k5eAaImIp3nP	být
především	především	k6eAd1	především
plniva	plnivo	k1gNnSc2	plnivo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
křída	křída	k1gFnSc1	křída
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stabilizátory	stabilizátor	k1gInPc1	stabilizátor
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
tepelné	tepelný	k2eAgFnSc2d1	tepelná
stability	stabilita	k1gFnSc2	stabilita
a	a	k8xC	a
odolnosti	odolnost	k1gFnSc2	odolnost
vůči	vůči	k7c3	vůči
ultrafialovému	ultrafialový	k2eAgNnSc3d1	ultrafialové
i	i	k8xC	i
tepelnému	tepelný	k2eAgNnSc3d1	tepelné
záření	záření	k1gNnSc3	záření
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
změkčovadla	změkčovadlo	k1gNnSc2	změkčovadlo
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
lepší	dobrý	k2eAgFnSc4d2	lepší
manipulaci	manipulace	k1gFnSc4	manipulace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
maziva	mazivo	k1gNnSc2	mazivo
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
lepší	dobrý	k2eAgFnSc4d2	lepší
zpracovatelnost	zpracovatelnost	k1gFnSc4	zpracovatelnost
<g/>
)	)	kIx)	)
atd.	atd.	kA	atd.
Jako	jako	k8xC	jako
stabilizátory	stabilizátor	k1gInPc1	stabilizátor
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
používaly	používat	k5eAaImAgInP	používat
i	i	k9	i
těžké	těžký	k2eAgInPc1d1	těžký
kovy	kov	k1gInPc1	kov
(	(	kIx(	(
<g/>
kadmium	kadmium	k1gNnSc1	kadmium
<g/>
,	,	kIx,	,
olovo	olovo	k1gNnSc1	olovo
<g/>
)	)	kIx)	)
či	či	k8xC	či
organické	organický	k2eAgFnPc1d1	organická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
cínu	cín	k1gInSc2	cín
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
(	(	kIx(	(
<g/>
i	i	k9	i
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
evropské	evropský	k2eAgFnSc2d1	Evropská
politiky	politika	k1gFnSc2	politika
REACH	REACH	kA	REACH
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
většinou	většinou	k6eAd1	většinou
nahrazeny	nahradit	k5eAaPmNgInP	nahradit
přísadami	přísada	k1gFnPc7	přísada
na	na	k7c4	na
bázi	báze	k1gFnSc4	báze
ekologického	ekologický	k2eAgInSc2d1	ekologický
vápníku	vápník	k1gInSc2	vápník
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
změkčovadla	změkčovadlo	k1gNnPc1	změkčovadlo
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
používají	používat	k5eAaImIp3nP	používat
ftaláty	ftalát	k1gInPc1	ftalát
(	(	kIx(	(
<g/>
např.	např.	kA	např.
DEHP	DEHP	kA	DEHP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
značného	značný	k2eAgNnSc2d1	značné
rozšíření	rozšíření	k1gNnSc2	rozšíření
PVC	PVC	kA	PVC
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
snadná	snadný	k2eAgFnSc1d1	snadná
a	a	k8xC	a
tedy	tedy	k9	tedy
levná	levný	k2eAgFnSc1d1	levná
výroba	výroba	k1gFnSc1	výroba
jak	jak	k6eAd1	jak
základního	základní	k2eAgInSc2d1	základní
vinylchloridu	vinylchlorid	k1gInSc2	vinylchlorid
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
významné	významný	k2eAgFnPc4d1	významná
užitné	užitný	k2eAgFnPc4d1	užitná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
jeho	jeho	k3xOp3gInSc2	jeho
polymeru	polymer	k1gInSc2	polymer
-	-	kIx~	-
snadná	snadný	k2eAgFnSc1d1	snadná
zpracovatelnost	zpracovatelnost	k1gFnSc1	zpracovatelnost
prakticky	prakticky	k6eAd1	prakticky
všemi	všecek	k3xTgInPc7	všecek
základními	základní	k2eAgInPc7d1	základní
postupy	postup	k1gInPc7	postup
(	(	kIx(	(
<g/>
válcováním	válcování	k1gNnSc7	válcování
<g/>
,	,	kIx,	,
vytlačováním	vytlačování	k1gNnSc7	vytlačování
<g/>
,	,	kIx,	,
vstřikováním	vstřikování	k1gNnSc7	vstřikování
<g/>
,	,	kIx,	,
vyfukováním	vyfukování	k1gNnSc7	vyfukování
<g/>
,	,	kIx,	,
vakuovým	vakuový	k2eAgNnSc7d1	vakuové
tvarováním	tvarování	k1gNnSc7	tvarování
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
schopnost	schopnost	k1gFnSc4	schopnost
želatinace	želatinace	k1gFnSc2	želatinace
s	s	k7c7	s
různými	různý	k2eAgNnPc7d1	různé
změkčovadly	změkčovadlo	k1gNnPc7	změkčovadlo
<g/>
,	,	kIx,	,
značná	značný	k2eAgFnSc1d1	značná
chemická	chemický	k2eAgFnSc1d1	chemická
a	a	k8xC	a
biologická	biologický	k2eAgFnSc1d1	biologická
odolnost	odolnost	k1gFnSc1	odolnost
až	až	k8xS	až
netečnost	netečnost	k1gFnSc1	netečnost
<g/>
,	,	kIx,	,
dobrá	dobrý	k2eAgFnSc1d1	dobrá
tepelná	tepelný	k2eAgFnSc1d1	tepelná
odolnost	odolnost	k1gFnSc1	odolnost
<g/>
.	.	kIx.	.
</s>
<s>
Neměkčené	měkčený	k2eNgInPc1d1	neměkčený
PVC	PVC	kA	PVC
(	(	kIx(	(
<g/>
obecný	obecný	k2eAgInSc1d1	obecný
obchodní	obchodní	k2eAgInSc1d1	obchodní
název	název	k1gInSc1	název
novodur	novodur	k1gInSc1	novodur
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
pro	pro	k7c4	pro
"	"	kIx"	"
<g/>
tvrdé	tvrdý	k2eAgInPc4d1	tvrdý
<g/>
"	"	kIx"	"
výrobky	výrobek	k1gInPc4	výrobek
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yRgFnPc2	který
je	být	k5eAaImIp3nS	být
žádoucí	žádoucí	k2eAgFnSc1d1	žádoucí
tvarová	tvarový	k2eAgFnSc1d1	tvarová
stálost	stálost	k1gFnSc1	stálost
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
životnosti	životnost	k1gFnSc2	životnost
–	–	k?	–
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
trubky	trubka	k1gFnPc4	trubka
<g/>
,	,	kIx,	,
profily	profil	k1gInPc4	profil
<g/>
,	,	kIx,	,
desky	deska	k1gFnPc4	deska
<g/>
,	,	kIx,	,
nádoby	nádoba	k1gFnPc1	nádoba
apod.	apod.	kA	apod.
Měkčené	měkčený	k2eAgInPc1d1	měkčený
PVC	PVC	kA	PVC
(	(	kIx(	(
<g/>
obecný	obecný	k2eAgInSc4d1	obecný
<g />
.	.	kIx.	.
</s>
<s>
obchodní	obchodní	k2eAgInSc1d1	obchodní
název	název	k1gInSc1	název
novoplast	novoplast	k1gInSc1	novoplast
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
využíváno	využíván	k2eAgNnSc1d1	využíváno
pro	pro	k7c4	pro
výrobky	výrobek	k1gInPc4	výrobek
polotuhé	polotuhý	k2eAgInPc4d1	polotuhý
až	až	k9	až
elastické	elastický	k2eAgFnSc2d1	elastická
–	–	k?	–
fólie	fólie	k1gFnSc2	fólie
(	(	kIx(	(
<g/>
nesprávný	správný	k2eNgInSc1d1	nesprávný
obecný	obecný	k2eAgInSc1d1	obecný
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
igelit	igelit	k1gInSc1	igelit
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ochranné	ochranný	k2eAgFnSc2d1	ochranná
rukavice	rukavice	k1gFnSc2	rukavice
<g/>
,	,	kIx,	,
kabely	kabela	k1gFnSc2	kabela
<g/>
,	,	kIx,	,
podlahové	podlahový	k2eAgFnSc2d1	podlahová
krytiny	krytina	k1gFnSc2	krytina
(	(	kIx(	(
<g/>
nesprávný	správný	k2eNgInSc1d1	nesprávný
obecný	obecný	k2eAgInSc1d1	obecný
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
linoleum	linoleum	k1gNnSc1	linoleum
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hadice	hadice	k1gFnPc4	hadice
<g/>
,	,	kIx,	,
dýchací	dýchací	k2eAgFnPc4d1	dýchací
masky	maska	k1gFnPc4	maska
<g/>
,	,	kIx,	,
zdravotnické	zdravotnický	k2eAgInPc1d1	zdravotnický
vaky	vak	k1gInPc1	vak
apod.	apod.	kA	apod.
Více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovina	polovina	k1gFnSc1	polovina
z	z	k7c2	z
celosvětově	celosvětově	k6eAd1	celosvětově
vyráběného	vyráběný	k2eAgNnSc2d1	vyráběné
množství	množství	k1gNnSc2	množství
PVC	PVC	kA	PVC
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
ve	v	k7c6	v
stavebnictví	stavebnictví	k1gNnSc6	stavebnictví
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
masově	masově	k6eAd1	masově
nahrazuje	nahrazovat	k5eAaImIp3nS	nahrazovat
tradiční	tradiční	k2eAgInPc4d1	tradiční
stavební	stavební	k2eAgInPc4d1	stavební
materiály	materiál	k1gInPc4	materiál
jako	jako	k8xS	jako
dřevo	dřevo	k1gNnSc4	dřevo
<g/>
,	,	kIx,	,
beton	beton	k1gInSc4	beton
či	či	k8xC	či
hlínu	hlína	k1gFnSc4	hlína
<g/>
.	.	kIx.	.
</s>
<s>
PVC	PVC	kA	PVC
má	mít	k5eAaImIp3nS	mít
totiž	totiž	k9	totiž
prakticky	prakticky	k6eAd1	prakticky
ideální	ideální	k2eAgFnPc4d1	ideální
stavební	stavební	k2eAgFnPc4d1	stavební
vlastnosti	vlastnost	k1gFnPc4	vlastnost
jako	jako	k8xC	jako
snadno	snadno	k6eAd1	snadno
zpracovatelný	zpracovatelný	k2eAgMnSc1d1	zpracovatelný
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
odolný	odolný	k2eAgInSc1d1	odolný
materiál	materiál	k1gInSc1	materiál
pro	pro	k7c4	pro
odpadová	odpadový	k2eAgNnPc4d1	odpadové
potrubí	potrubí	k1gNnPc4	potrubí
<g/>
,	,	kIx,	,
okenní	okenní	k2eAgInPc4d1	okenní
a	a	k8xC	a
dveřní	dveřní	k2eAgInPc4d1	dveřní
rámy	rám	k1gInPc4	rám
<g/>
,	,	kIx,	,
podlahovou	podlahový	k2eAgFnSc4d1	podlahová
a	a	k8xC	a
střešní	střešní	k2eAgFnSc4d1	střešní
krytinu	krytina	k1gFnSc4	krytina
apod.	apod.	kA	apod.
Při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
může	moct	k5eAaImIp3nS	moct
do	do	k7c2	do
prostředí	prostředí	k1gNnSc2	prostředí
uniknout	uniknout	k5eAaPmF	uniknout
jak	jak	k8xC	jak
toxický	toxický	k2eAgInSc1d1	toxický
prvek	prvek	k1gInSc1	prvek
chlór	chlór	k1gInSc1	chlór
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
primární	primární	k2eAgInSc1d1	primární
Vinylchlorid	vinylchlorid	k1gInSc1	vinylchlorid
monomer	monomer	k1gInSc1	monomer
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
je	být	k5eAaImIp3nS	být
karcinogenem	karcinogen	k1gInSc7	karcinogen
vyvolávajícím	vyvolávající	k2eAgInSc7d1	vyvolávající
zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
typ	typ	k1gInSc1	typ
rakoviny	rakovina	k1gFnSc2	rakovina
jater	játra	k1gNnPc2	játra
(	(	kIx(	(
<g/>
angiosarkom	angiosarkom	k1gInSc1	angiosarkom
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výrobu	výroba	k1gFnSc4	výroba
polyvinylchloridu	polyvinylchlorid	k1gInSc2	polyvinylchlorid
doprovází	doprovázet	k5eAaImIp3nS	doprovázet
vznik	vznik	k1gInSc1	vznik
dioxinů	dioxin	k1gInPc2	dioxin
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c4	mezi
rizikové	rizikový	k2eAgFnPc4d1	riziková
toxické	toxický	k2eAgFnPc4d1	toxická
látky	látka	k1gFnPc4	látka
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
úniku	únik	k1gInSc2	únik
do	do	k7c2	do
prostředí	prostředí	k1gNnSc2	prostředí
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
podmínkách	podmínka	k1gFnPc6	podmínka
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
havárie	havárie	k1gFnPc4	havárie
ve	v	k7c6	v
Spolaně	Spolana	k1gFnSc6	Spolana
Neratovice	Neratovice	k1gFnSc2	Neratovice
<g/>
,	,	kIx,	,
jediné	jediný	k2eAgFnSc3d1	jediná
české	český	k2eAgFnSc3d1	Česká
továrně	továrna	k1gFnSc3	továrna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
polyvinylchlorid	polyvinylchlorid	k1gInSc4	polyvinylchlorid
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgInPc1d1	samotný
PVC	PVC	kA	PVC
výrobky	výrobek	k1gInPc1	výrobek
lze	lze	k6eAd1	lze
při	při	k7c6	při
správném	správný	k2eAgNnSc6d1	správné
zacházení	zacházení	k1gNnSc6	zacházení
považovat	považovat	k5eAaImF	považovat
za	za	k7c7	za
neškodné	škodný	k2eNgMnPc4d1	neškodný
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
každý	každý	k3xTgInSc1	každý
jiný	jiný	k2eAgInSc1d1	jiný
plastový	plastový	k2eAgInSc1d1	plastový
výrobek	výrobek	k1gInSc1	výrobek
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
některé	některý	k3yIgFnPc1	některý
přísady	přísada	k1gFnPc1	přísada
a	a	k8xC	a
změkčovadla	změkčovadlo	k1gNnPc1	změkčovadlo
mohou	moct	k5eAaImIp3nP	moct
z	z	k7c2	z
výrobků	výrobek	k1gInPc2	výrobek
uvolňovat	uvolňovat	k5eAaImF	uvolňovat
do	do	k7c2	do
prostředí	prostředí	k1gNnSc2	prostředí
a	a	k8xC	a
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
mohou	moct	k5eAaImIp3nP	moct
narušit	narušit	k5eAaPmF	narušit
například	například	k6eAd1	například
endokrinní	endokrinní	k2eAgFnSc4d1	endokrinní
soustavu	soustava	k1gFnSc4	soustava
<g/>
,	,	kIx,	,
totiž	totiž	k9	totiž
není	být	k5eNaImIp3nS	být
výsadou	výsada	k1gFnSc7	výsada
pouze	pouze	k6eAd1	pouze
PVC	PVC	kA	PVC
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
podmínkách	podmínka	k1gFnPc6	podmínka
–	–	k?	–
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
teplotě	teplota	k1gFnSc3	teplota
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Zásadním	zásadní	k2eAgInSc7d1	zásadní
problémem	problém	k1gInSc7	problém
PVC	PVC	kA	PVC
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
jeho	jeho	k3xOp3gFnPc4	jeho
požárně-technické	požárněechnický	k2eAgFnPc4d1	požárně-technický
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
není	být	k5eNaImIp3nS	být
nehořlavé	hořlavý	k2eNgNnSc1d1	nehořlavé
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pouze	pouze	k6eAd1	pouze
samozhášivé	samozhášivý	k2eAgInPc1d1	samozhášivý
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
nehoří	hořet	k5eNaImIp3nS	hořet
bez	bez	k7c2	bez
přímé	přímý	k2eAgFnSc2d1	přímá
vnější	vnější	k2eAgFnSc2d1	vnější
iniciace	iniciace	k1gFnSc2	iniciace
<g/>
,	,	kIx,	,
při	při	k7c6	při
odstranění	odstranění	k1gNnSc6	odstranění
vnějšího	vnější	k2eAgInSc2d1	vnější
plamene	plamen	k1gInSc2	plamen
samo	sám	k3xTgNnSc1	sám
uhasne	uhasnout	k5eAaPmIp3nS	uhasnout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
podmínkách	podmínka	k1gFnPc6	podmínka
požáru	požár	k1gInSc2	požár
však	však	k8xC	však
při	při	k7c6	při
jeho	jeho	k3xOp3gInSc6	jeho
tepelném	tepelný	k2eAgInSc6d1	tepelný
rozkladu	rozklad	k1gInSc6	rozklad
vzniká	vznikat	k5eAaImIp3nS	vznikat
chlorovodík	chlorovodík	k1gInSc1	chlorovodík
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
při	při	k7c6	při
koncentraci	koncentrace	k1gFnSc6	koncentrace
0,008	[number]	k4	0,008
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
l	l	kA	l
zdraví	zdravit	k5eAaImIp3nS	zdravit
nebezpečný	bezpečný	k2eNgInSc1d1	nebezpečný
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
reakci	reakce	k1gFnSc6	reakce
s	s	k7c7	s
vodní	vodní	k2eAgFnSc7d1	vodní
parou	para	k1gFnSc7	para
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
tvoří	tvořit	k5eAaImIp3nS	tvořit
koncentrovanou	koncentrovaný	k2eAgFnSc4d1	koncentrovaná
kyselinu	kyselina	k1gFnSc4	kyselina
chlorovodíkovou	chlorovodíkový	k2eAgFnSc4d1	chlorovodíková
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
působí	působit	k5eAaImIp3nS	působit
korozivně	korozivně	k6eAd1	korozivně
na	na	k7c4	na
kovové	kovový	k2eAgFnPc4d1	kovová
konstrukce	konstrukce	k1gFnPc4	konstrukce
a	a	k8xC	a
elektrické	elektrický	k2eAgInPc4d1	elektrický
obvody	obvod	k1gInPc4	obvod
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
důvodu	důvod	k1gInSc2	důvod
například	například	k6eAd1	například
musejí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
v	v	k7c6	v
prostorách	prostora	k1gFnPc6	prostora
se	s	k7c7	s
zvýšeným	zvýšený	k2eAgNnSc7d1	zvýšené
nebezpečím	nebezpečí	k1gNnSc7	nebezpečí
požáru	požár	k1gInSc2	požár
a	a	k8xC	a
ve	v	k7c6	v
shromaždištích	shromaždiště	k1gNnPc6	shromaždiště
většího	veliký	k2eAgNnSc2d2	veliký
množství	množství	k1gNnSc2	množství
osob	osoba	k1gFnPc2	osoba
použity	použít	k5eAaPmNgFnP	použít
elektrické	elektrický	k2eAgFnPc1d1	elektrická
kabely	kabela	k1gFnPc1	kabela
bez	bez	k7c2	bez
PVC	PVC	kA	PVC
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
v	v	k7c6	v
případě	případ	k1gInSc6	případ
požáru	požár	k1gInSc2	požár
neuvolňovaly	uvolňovat	k5eNaImAgInP	uvolňovat
nebezpečné	bezpečný	k2eNgInPc1d1	nebezpečný
plyny	plyn	k1gInPc1	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
<g/>
-li	i	k?	-li
k	k	k7c3	k
likvidaci	likvidace	k1gFnSc3	likvidace
PVC	PVC	kA	PVC
spalováním	spalování	k1gNnSc7	spalování
bez	bez	k7c2	bez
řádně	řádně	k6eAd1	řádně
přizpůsobené	přizpůsobený	k2eAgFnSc2d1	přizpůsobená
technologie	technologie	k1gFnSc2	technologie
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
součástí	součást	k1gFnPc2	součást
emisí	emise	k1gFnPc2	emise
i	i	k8xC	i
nežádoucí	žádoucí	k2eNgInPc4d1	nežádoucí
vedlejší	vedlejší	k2eAgInPc4d1	vedlejší
produkty	produkt	k1gInPc4	produkt
spalování	spalování	k1gNnSc2	spalování
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
toxické	toxický	k2eAgInPc1d1	toxický
<g/>
,	,	kIx,	,
karcinogenní	karcinogenní	k2eAgInPc1d1	karcinogenní
nebo	nebo	k8xC	nebo
jinak	jinak	k6eAd1	jinak
škodlivé	škodlivý	k2eAgFnPc1d1	škodlivá
látky	látka	k1gFnPc1	látka
jako	jako	k8xC	jako
chlorovodík	chlorovodík	k1gInSc1	chlorovodík
<g/>
,	,	kIx,	,
hexachlorbenzen	hexachlorbenzen	k2eAgInSc1d1	hexachlorbenzen
<g/>
,	,	kIx,	,
polychlorované	polychlorovaný	k2eAgInPc1d1	polychlorovaný
bifenyly	bifenyl	k1gInPc1	bifenyl
<g/>
,	,	kIx,	,
furany	furan	k1gInPc1	furan
a	a	k8xC	a
dioxiny	dioxin	k1gInPc1	dioxin
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
velmi	velmi	k6eAd1	velmi
přínosná	přínosný	k2eAgFnSc1d1	přínosná
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
úplná	úplný	k2eAgFnSc1d1	úplná
recyklovatelnost	recyklovatelnost	k1gFnSc1	recyklovatelnost
odpadů	odpad	k1gInPc2	odpad
<g/>
,	,	kIx,	,
vzniklých	vzniklý	k2eAgInPc2d1	vzniklý
z	z	k7c2	z
termoplastického	termoplastický	k2eAgInSc2d1	termoplastický
PVC	PVC	kA	PVC
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
poměrně	poměrně	k6eAd1	poměrně
snadno	snadno	k6eAd1	snadno
rozdrtit	rozdrtit	k5eAaPmF	rozdrtit
a	a	k8xC	a
po	po	k7c6	po
přetavení	přetavení	k1gNnSc6	přetavení
použít	použít	k5eAaPmF	použít
do	do	k7c2	do
nového	nový	k2eAgInSc2d1	nový
výrobku	výrobek	k1gInSc2	výrobek
<g/>
,	,	kIx,	,
čehož	což	k3yQnSc2	což
využívají	využívat	k5eAaPmIp3nP	využívat
například	například	k6eAd1	například
výrobci	výrobce	k1gMnPc1	výrobce
střešních	střešní	k2eAgFnPc2d1	střešní
krytin	krytina	k1gFnPc2	krytina
<g/>
,	,	kIx,	,
zahradních	zahradní	k2eAgInPc2d1	zahradní
prvků	prvek	k1gInPc2	prvek
apod.	apod.	kA	apod.
PVC	PVC	kA	PVC
výrobky	výrobek	k1gInPc1	výrobek
z	z	k7c2	z
recyklátu	recyklát	k1gInSc2	recyklát
jsou	být	k5eAaImIp3nP	být
totiž	totiž	k9	totiž
téměř	téměř	k6eAd1	téměř
o	o	k7c4	o
polovinu	polovina	k1gFnSc4	polovina
materiálově	materiálově	k6eAd1	materiálově
i	i	k9	i
energeticky	energeticky	k6eAd1	energeticky
méně	málo	k6eAd2	málo
náročné	náročný	k2eAgInPc1d1	náročný
než	než	k8xS	než
výrobky	výrobek	k1gInPc1	výrobek
z	z	k7c2	z
primárního	primární	k2eAgInSc2d1	primární
PVC	PVC	kA	PVC
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Polyvinylchlorid	polyvinylchlorid	k1gInSc1	polyvinylchlorid
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Stránky	stránka	k1gFnPc1	stránka
sdružení	sdružení	k1gNnSc2	sdružení
Arnika	arnika	k1gFnSc1	arnika
o	o	k7c6	o
PVC	PVC	kA	PVC
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
PVC	PVC	kA	PVC
Information	Information	k1gInSc1	Information
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Association	Association	k1gInSc4	Association
between	between	k1gInSc1	between
Asthma	Asthma	k1gFnSc1	Asthma
and	and	k?	and
Allergic	Allergic	k1gMnSc1	Allergic
Symptoms	Symptomsa	k1gFnPc2	Symptomsa
in	in	k?	in
Children	Childrna	k1gFnPc2	Childrna
and	and	k?	and
Phthalates	Phthalatesa	k1gFnPc2	Phthalatesa
in	in	k?	in
House	house	k1gNnSc1	house
Dust	Dust	k1gMnSc1	Dust
<g/>
:	:	kIx,	:
A	A	kA	A
Nested	Nested	k1gInSc1	Nested
Case-Control	Case-Control	k1gInSc1	Case-Control
Study	stud	k1gInPc1	stud
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Polyvinyl	Polyvinyl	k1gInSc1	Polyvinyl
Chloride	chlorid	k1gInSc5	chlorid
-	-	kIx~	-
General	General	k1gMnSc1	General
Info	Info	k1gMnSc1	Info
"	"	kIx"	"
<g/>
PVC	PVC	kA	PVC
–	–	k?	–
Toxic	Toxic	k1gMnSc1	Toxic
Plastic	Plastice	k1gFnPc2	Plastice
<g/>
"	"	kIx"	"
</s>
