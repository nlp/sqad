<s>
Základní	základní	k2eAgFnSc7d1	základní
surovinou	surovina	k1gFnSc7	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
PVC	PVC	kA	PVC
je	být	k5eAaImIp3nS	být
vinylchlorid	vinylchlorid	k1gInSc1	vinylchlorid
monomer	monomer	k1gInSc1	monomer
(	(	kIx(	(
<g/>
VCM	VCM	kA	VCM
–	–	k?	–
těkavý	těkavý	k2eAgMnSc1d1	těkavý
<g/>
,	,	kIx,	,
jemně	jemně	k6eAd1	jemně
nasládlý	nasládlý	k2eAgInSc1d1	nasládlý
plyn	plyn	k1gInSc1	plyn
<g/>
,	,	kIx,	,
teplota	teplota	k1gFnSc1	teplota
varu	var	k1gInSc2	var
-13,9	-13,9	k4	-13,9
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzniklý	vzniklý	k2eAgMnSc1d1	vzniklý
obvykle	obvykle	k6eAd1	obvykle
rozkladem	rozklad	k1gInSc7	rozklad
1,2	[number]	k4	1,2
<g/>
-dichlorethanu	ichlorethan	k1gInSc2	-dichlorethan
(	(	kIx(	(
<g/>
starší	starý	k2eAgInPc1d2	starší
postupy	postup	k1gInPc1	postup
vycházely	vycházet	k5eAaImAgFnP	vycházet
z	z	k7c2	z
acetylenu	acetylen	k1gInSc2	acetylen
a	a	k8xC	a
HCl	HCl	k1gFnSc2	HCl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
