<s>
Expreska	Expreska	k1gFnSc1	Expreska
<g/>
,	,	kIx,	,
celým	celý	k2eAgInSc7d1	celý
názvem	název	k1gInSc7	název
expresní	expresní	k2eAgFnSc1d1	expresní
smyčka	smyčka	k1gFnSc1	smyčka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
krátká	krátký	k2eAgFnSc1d1	krátká
sešitá	sešitý	k2eAgFnSc1d1	sešitá
popruhová	popruhový	k2eAgFnSc1d1	popruhová
(	(	kIx(	(
<g/>
plochá	plochý	k2eAgFnSc1d1	plochá
<g/>
)	)	kIx)	)
smyčka	smyčka	k1gFnSc1	smyčka
spojující	spojující	k2eAgFnSc4d1	spojující
dvojici	dvojice	k1gFnSc4	dvojice
karabin	karabina	k1gFnPc2	karabina
(	(	kIx(	(
<g/>
pojmem	pojem	k1gInSc7	pojem
expreska	expreska	k1gFnSc1	expreska
bývá	bývat	k5eAaImIp3nS	bývat
myšlen	myšlen	k2eAgInSc1d1	myšlen
také	také	k9	také
celý	celý	k2eAgInSc1d1	celý
komplet	komplet	k1gInSc1	komplet
obou	dva	k4xCgFnPc2	dva
karabin	karabina	k1gFnPc2	karabina
se	s	k7c7	s
smyčkou	smyčka	k1gFnSc7	smyčka
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
takový	takový	k3xDgInSc1	takový
komplet	komplet	k1gInSc1	komplet
označuje	označovat	k5eAaImIp3nS	označovat
delším	dlouhý	k2eAgInSc7d2	delší
názvem	název	k1gInSc7	název
expres	expres	k2eAgInSc4d1	expres
set	set	k1gInSc4	set
<g/>
,	,	kIx,	,
slangově	slangově	k6eAd1	slangově
i	i	k9	i
preska	preska	k1gFnSc1	preska
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
