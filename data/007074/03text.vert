<s>
Expreska	Expreska	k1gFnSc1	Expreska
<g/>
,	,	kIx,	,
celým	celý	k2eAgInSc7d1	celý
názvem	název	k1gInSc7	název
expresní	expresní	k2eAgFnSc1d1	expresní
smyčka	smyčka	k1gFnSc1	smyčka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
krátká	krátký	k2eAgFnSc1d1	krátká
sešitá	sešitý	k2eAgFnSc1d1	sešitá
popruhová	popruhový	k2eAgFnSc1d1	popruhová
(	(	kIx(	(
<g/>
plochá	plochý	k2eAgFnSc1d1	plochá
<g/>
)	)	kIx)	)
smyčka	smyčka	k1gFnSc1	smyčka
spojující	spojující	k2eAgFnSc4d1	spojující
dvojici	dvojice	k1gFnSc4	dvojice
karabin	karabina	k1gFnPc2	karabina
(	(	kIx(	(
<g/>
pojmem	pojem	k1gInSc7	pojem
expreska	expreska	k1gFnSc1	expreska
bývá	bývat	k5eAaImIp3nS	bývat
myšlen	myšlen	k2eAgInSc1d1	myšlen
také	také	k9	také
celý	celý	k2eAgInSc1d1	celý
komplet	komplet	k1gInSc1	komplet
obou	dva	k4xCgFnPc2	dva
karabin	karabina	k1gFnPc2	karabina
se	s	k7c7	s
smyčkou	smyčka	k1gFnSc7	smyčka
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
takový	takový	k3xDgInSc1	takový
komplet	komplet	k1gInSc1	komplet
označuje	označovat	k5eAaImIp3nS	označovat
delším	dlouhý	k2eAgInSc7d2	delší
názvem	název	k1gInSc7	název
expres	expres	k2eAgInSc4d1	expres
set	set	k1gInSc4	set
<g/>
,	,	kIx,	,
slangově	slangově	k6eAd1	slangově
i	i	k9	i
preska	preska	k1gFnSc1	preska
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
ke	k	k7c3	k
spojení	spojení	k1gNnSc3	spojení
lana	lano	k1gNnSc2	lano
a	a	k8xC	a
jistícího	jistící	k2eAgInSc2d1	jistící
bodu	bod	k1gInSc2	bod
při	při	k7c6	při
lezení	lezení	k1gNnSc6	lezení
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
jednoduché	jednoduchý	k2eAgFnSc2d1	jednoduchá
karabiny	karabina	k1gFnSc2	karabina
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
volnost	volnost	k1gFnSc1	volnost
pohybu	pohyb	k1gInSc2	pohyb
lana	lano	k1gNnSc2	lano
<g/>
,	,	kIx,	,
menší	malý	k2eAgNnSc1d2	menší
tření	tření	k1gNnSc1	tření
v	v	k7c6	v
jistícím	jistící	k2eAgInSc6d1	jistící
bodě	bod	k1gInSc6	bod
a	a	k8xC	a
oproti	oproti	k7c3	oproti
dvěma	dva	k4xCgFnPc3	dva
karabinám	karabina	k1gFnPc3	karabina
a	a	k8xC	a
samostatné	samostatný	k2eAgFnSc6d1	samostatná
smyčce	smyčka	k1gFnSc6	smyčka
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
zajištění	zajištění	k1gNnSc4	zajištění
při	při	k7c6	při
dosažení	dosažení	k1gNnSc6	dosažení
jistícího	jistící	k2eAgInSc2d1	jistící
bodu	bod	k1gInSc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
situacích	situace	k1gFnPc6	situace
snižuje	snižovat	k5eAaImIp3nS	snižovat
také	také	k9	také
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
samovolného	samovolný	k2eAgNnSc2d1	samovolné
otevření	otevření	k1gNnSc2	otevření
karabiny	karabina	k1gFnSc2	karabina
<g/>
.	.	kIx.	.
</s>
<s>
Nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
hmotnost	hmotnost	k1gFnSc1	hmotnost
oproti	oproti	k7c3	oproti
jediné	jediný	k2eAgFnSc3d1	jediná
karabině	karabina	k1gFnSc3	karabina
a	a	k8xC	a
také	také	k9	také
poměrně	poměrně	k6eAd1	poměrně
významné	významný	k2eAgNnSc4d1	významné
prodloužení	prodloužení	k1gNnSc4	prodloužení
délky	délka	k1gFnSc2	délka
případného	případný	k2eAgInSc2d1	případný
pádu	pád	k1gInSc2	pád
<g/>
.	.	kIx.	.
</s>
<s>
Minimální	minimální	k2eAgFnSc1d1	minimální
pevnost	pevnost	k1gFnSc1	pevnost
smyčky	smyčka	k1gFnSc2	smyčka
požadovaná	požadovaný	k2eAgFnSc1d1	požadovaná
normou	norma	k1gFnSc7	norma
je	být	k5eAaImIp3nS	být
22	[number]	k4	22
kN	kN	k?	kN
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sportovních	sportovní	k2eAgFnPc6d1	sportovní
cestách	cesta	k1gFnPc6	cesta
s	s	k7c7	s
velkým	velký	k2eAgInSc7d1	velký
počtem	počet	k1gInSc7	počet
jistících	jistící	k2eAgInPc2d1	jistící
bodů	bod	k1gInPc2	bod
je	být	k5eAaImIp3nS	být
standardní	standardní	k2eAgFnSc7d1	standardní
výbavou	výbava	k1gFnSc7	výbava
lezců	lezec	k1gMnPc2	lezec
a	a	k8xC	a
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
těžkých	těžký	k2eAgFnPc6d1	těžká
a	a	k8xC	a
převislých	převislý	k2eAgFnPc6d1	převislá
cestách	cesta	k1gFnPc6	cesta
ponechávají	ponechávat	k5eAaImIp3nP	ponechávat
často	často	k6eAd1	často
lezci	lezec	k1gMnPc1	lezec
expresky	expreska	k1gFnPc4	expreska
pro	pro	k7c4	pro
další	další	k2eAgNnSc4d1	další
opakování	opakování	k1gNnSc4	opakování
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
také	také	k9	také
PP	PP	kA	PP
styl	styl	k1gInSc1	styl
přelezu	přelézt	k5eAaPmIp1nS	přelézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Horolezectví	horolezectví	k1gNnSc2	horolezectví
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
expresce	expreska	k1gFnSc6	expreska
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
použity	použit	k2eAgFnPc1d1	použita
karabiny	karabina	k1gFnPc1	karabina
s	s	k7c7	s
různým	různý	k2eAgInSc7d1	různý
typem	typ	k1gInSc7	typ
zámku	zámek	k1gInSc2	zámek
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
pojistky	pojistka	k1gFnSc2	pojistka
nebo	nebo	k8xC	nebo
i	i	k9	i
s	s	k7c7	s
pojistkou	pojistka	k1gFnSc7	pojistka
<g/>
.	.	kIx.	.
</s>
<s>
Častá	častý	k2eAgFnSc1d1	častá
je	být	k5eAaImIp3nS	být
kombinace	kombinace	k1gFnSc1	kombinace
D	D	kA	D
karabin	karabina	k1gFnPc2	karabina
s	s	k7c7	s
rovným	rovný	k2eAgInSc7d1	rovný
zámkem	zámek	k1gInSc7	zámek
a	a	k8xC	a
dovnitř	dovnitř	k6eAd1	dovnitř
prohnutým	prohnutý	k2eAgInSc7d1	prohnutý
zámkem	zámek	k1gInSc7	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Karabina	karabina	k1gFnSc1	karabina
s	s	k7c7	s
rovným	rovný	k2eAgInSc7d1	rovný
zámkem	zámek	k1gInSc7	zámek
se	se	k3xPyFc4	se
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
případě	případ	k1gInSc6	případ
zapíná	zapínat	k5eAaImIp3nS	zapínat
do	do	k7c2	do
pevného	pevný	k2eAgInSc2d1	pevný
jistícího	jistící	k2eAgInSc2d1	jistící
bodu	bod	k1gInSc2	bod
a	a	k8xC	a
do	do	k7c2	do
karabiny	karabina	k1gFnSc2	karabina
s	s	k7c7	s
prohnutým	prohnutý	k2eAgInSc7d1	prohnutý
zámkem	zámek	k1gInSc7	zámek
se	se	k3xPyFc4	se
zapíná	zapínat	k5eAaImIp3nS	zapínat
lano	lano	k1gNnSc1	lano
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
prohnutí	prohnutí	k1gNnSc2	prohnutí
zámku	zámek	k1gInSc2	zámek
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
snaha	snaha	k1gFnSc1	snaha
usnadnit	usnadnit	k5eAaPmF	usnadnit
zapínání	zapínání	k1gNnSc4	zapínání
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
cvakání	cvakání	k1gNnSc1	cvakání
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
lana	lano	k1gNnPc4	lano
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
smyčky	smyčka	k1gFnSc2	smyčka
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
různá	různý	k2eAgNnPc4d1	různé
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
podle	podle	k7c2	podle
potřeby	potřeba	k1gFnSc2	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Kratší	krátký	k2eAgFnPc1d2	kratší
expresky	expreska	k1gFnPc1	expreska
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
u	u	k7c2	u
přímých	přímý	k2eAgFnPc2d1	přímá
cest	cesta	k1gFnPc2	cesta
a	a	k8xC	a
v	v	k7c6	v
případech	případ	k1gInPc6	případ
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
každý	každý	k3xTgInSc4	každý
centimetr	centimetr	k1gInSc4	centimetr
pádu	pád	k1gInSc2	pád
navíc	navíc	k6eAd1	navíc
může	moct	k5eAaImIp3nS	moct
vadit	vadit	k5eAaImF	vadit
<g/>
.	.	kIx.	.
</s>
<s>
Delší	dlouhý	k2eAgInPc1d2	delší
expresky	expresk	k1gInPc1	expresk
se	se	k3xPyFc4	se
s	s	k7c7	s
výhodou	výhoda	k1gFnSc7	výhoda
používají	používat	k5eAaImIp3nP	používat
například	například	k6eAd1	například
pod	pod	k7c7	pod
převisy	převis	k1gInPc7	převis
nebo	nebo	k8xC	nebo
v	v	k7c6	v
bodech	bod	k1gInPc6	bod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
směru	směr	k1gInSc2	směr
výstupu	výstup	k1gInSc2	výstup
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
co	co	k9	co
nejmenšímu	malý	k2eAgNnSc3d3	nejmenší
nárůstu	nárůst	k1gInSc3	nárůst
tření	tření	k1gNnPc2	tření
při	při	k7c6	při
průchodu	průchod	k1gInSc6	průchod
lana	lano	k1gNnSc2	lano
skrz	skrz	k7c4	skrz
jištění	jištění	k1gNnSc4	jištění
<g/>
.	.	kIx.	.
</s>
<s>
Spojovací	spojovací	k2eAgFnSc1d1	spojovací
smyčka	smyčka	k1gFnSc1	smyčka
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
klasických	klasický	k2eAgFnPc2d1	klasická
popruhových	popruhový	k2eAgFnPc2d1	popruhová
smyček	smyčka	k1gFnPc2	smyčka
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
tenčí	tenký	k2eAgFnPc1d2	tenčí
ploché	plochý	k2eAgFnPc1d1	plochá
smyčky	smyčka	k1gFnPc1	smyčka
z	z	k7c2	z
vysokopevnostního	vysokopevnostní	k2eAgInSc2d1	vysokopevnostní
polyetylénu	polyetylén	k1gInSc2	polyetylén
(	(	kIx(	(
<g/>
Spectra	Spectra	k1gFnSc1	Spectra
<g/>
,	,	kIx,	,
Dyneema	Dyneema	k1gFnSc1	Dyneema
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
bývá	bývat	k5eAaImIp3nS	bývat
poloha	poloha	k1gFnSc1	poloha
karabin	karabina	k1gFnPc2	karabina
v	v	k7c6	v
expresce	expreska	k1gFnSc6	expreska
zajištěna	zajistit	k5eAaPmNgFnS	zajistit
gumičkami	gumička	k1gFnPc7	gumička
<g/>
.	.	kIx.	.
</s>
<s>
Názory	názor	k1gInPc1	názor
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
zajišťovat	zajišťovat	k5eAaImF	zajišťovat
polohu	poloha	k1gFnSc4	poloha
obou	dva	k4xCgFnPc2	dva
karabin	karabina	k1gFnPc2	karabina
nebo	nebo	k8xC	nebo
pouze	pouze	k6eAd1	pouze
jednu	jeden	k4xCgFnSc4	jeden
karabinu	karabina	k1gFnSc4	karabina
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
různí	různý	k2eAgMnPc1d1	různý
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
různí	různit	k5eAaImIp3nP	různit
názory	názor	k1gInPc1	názor
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
lepší	dobrý	k2eAgNnSc1d2	lepší
<g/>
,	,	kIx,	,
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
zámky	zámek	k1gInPc1	zámek
obou	dva	k4xCgFnPc2	dva
karabin	karabina	k1gFnPc2	karabina
na	na	k7c4	na
stejnou	stejný	k2eAgFnSc4d1	stejná
stranu	strana	k1gFnSc4	strana
nebo	nebo	k8xC	nebo
každý	každý	k3xTgInSc4	každý
na	na	k7c4	na
jinou	jiný	k2eAgFnSc4d1	jiná
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
záleží	záležet	k5eAaImIp3nS	záležet
spíše	spíše	k9	spíše
na	na	k7c4	na
konkrétní	konkrétní	k2eAgFnSc4d1	konkrétní
situaci	situace	k1gFnSc4	situace
<g/>
.	.	kIx.	.
</s>
