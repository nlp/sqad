<s>
FAQ	FAQ	kA
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
anglického	anglický	k2eAgInSc2d1
výrazu	výraz	k1gInSc2
Frequently	Frequently	k1gMnSc1
Asked	Asked	k1gMnSc1
Questions	Questions	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
překladu	překlad	k1gInSc6
často	často	k6eAd1
kladené	kladený	k2eAgInPc4d1
dotazy	dotaz	k1gInPc4
<g/>
;	;	kIx,
občas	občas	k6eAd1
se	se	k3xPyFc4
—	—	k?
s	s	k7c7
mírně	mírně	k6eAd1
humorným	humorný	k2eAgInSc7d1
nádechem	nádech	k1gInSc7
—	—	k?
používá	používat	k5eAaImIp3nS
česká	český	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
ČKD	ČKD	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
dokument	dokument	k1gInSc4
obsahující	obsahující	k2eAgInSc4d1
seznam	seznam	k1gInSc4
otázek	otázka	k1gFnPc2
a	a	k8xC
odpovědí	odpověď	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
ohledně	ohledně	k7c2
nějaké	nějaký	k3yIgFnSc2
problematiky	problematika	k1gFnSc2
často	často	k6eAd1
vyskytují	vyskytovat	k5eAaImIp3nP
a	a	k8xC
jsou	být	k5eAaImIp3nP
pro	pro	k7c4
ni	on	k3xPp3gFnSc4
typické	typický	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>