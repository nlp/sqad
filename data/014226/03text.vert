<s>
FAQ	FAQ	kA
</s>
<s>
O	o	k7c6
jakostním	jakostní	k2eAgInSc6d1
stupni	stupeň	k1gInSc6
v	v	k7c6
zemědělství	zemědělství	k1gNnSc6
a	a	k8xC
potravinářství	potravinářství	k1gNnSc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Fair	fair	k2eAgFnSc2d1
average	averag	k1gFnSc2
quality	qualita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
FAQ	FAQ	kA
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
anglického	anglický	k2eAgInSc2d1
výrazu	výraz	k1gInSc2
Frequently	Frequently	k1gMnSc1
Asked	Asked	k1gMnSc1
Questions	Questions	k1gInSc4
<g/>
,	,	kIx,
v	v	k7c6
překladu	překlad	k1gInSc6
často	často	k6eAd1
kladené	kladený	k2eAgInPc4d1
dotazy	dotaz	k1gInPc4
<g/>
;	;	kIx,
občas	občas	k6eAd1
se	se	k3xPyFc4
—	—	k?
s	s	k7c7
mírně	mírně	k6eAd1
humorným	humorný	k2eAgInSc7d1
nádechem	nádech	k1gInSc7
—	—	k?
používá	používat	k5eAaImIp3nS
česká	český	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
ČKD	ČKD	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
dokument	dokument	k1gInSc4
obsahující	obsahující	k2eAgInSc4d1
seznam	seznam	k1gInSc4
otázek	otázka	k1gFnPc2
a	a	k8xC
odpovědí	odpověď	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
ohledně	ohledně	k7c2
nějaké	nějaký	k3yIgFnSc2
problematiky	problematika	k1gFnSc2
často	často	k6eAd1
vyskytují	vyskytovat	k5eAaImIp3nP
a	a	k8xC
jsou	být	k5eAaImIp3nP
pro	pro	k7c4
ni	on	k3xPp3gFnSc4
typické	typický	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Dokumenty	dokument	k1gInPc1
FAQ	FAQ	kA
vznikly	vzniknout	k5eAaPmAgFnP
na	na	k7c6
Internetu	Internet	k1gInSc6
<g/>
,	,	kIx,
konkrétně	konkrétně	k6eAd1
v	v	k7c6
Usenetových	Usenetový	k2eAgFnPc6d1
diskusních	diskusní	k2eAgFnPc6d1
skupinách	skupina	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
těchto	tento	k3xDgFnPc6
skupinách	skupina	k1gFnPc6
se	se	k3xPyFc4
často	často	k6eAd1
opakovaly	opakovat	k5eAaImAgFnP
stejné	stejný	k2eAgFnPc4d1
otázky	otázka	k1gFnPc4
začátečníků	začátečník	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yIgMnPc1,k3yQgMnPc1
znovu	znovu	k6eAd1
a	a	k8xC
znovu	znovu	k6eAd1
pokládali	pokládat	k5eAaImAgMnP
již	již	k6eAd1
mnohokrát	mnohokrát	k6eAd1
zodpovězené	zodpovězený	k2eAgInPc4d1
dotazy	dotaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
se	se	k3xPyFc4
takovým	takový	k3xDgFnPc3
opakovaným	opakovaný	k2eAgFnPc3d1
otázkám	otázka	k1gFnPc3
zabránilo	zabránit	k5eAaPmAgNnS
<g/>
,	,	kIx,
někteří	některý	k3yIgMnPc1
uživatelé	uživatel	k1gMnPc1
začali	začít	k5eAaPmAgMnP
vytvářet	vytvářet	k5eAaImF
seznamy	seznam	k1gInPc4
nejtypičtějších	typický	k2eAgFnPc2d3
otázek	otázka	k1gFnPc2
a	a	k8xC
odpovědí	odpověď	k1gFnPc2
na	na	k7c4
ně	on	k3xPp3gMnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yIgFnPc4,k3yRgFnPc4
do	do	k7c2
příslušné	příslušný	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
pravidelně	pravidelně	k6eAd1
zasílali	zasílat	k5eAaImAgMnP
v	v	k7c6
jediné	jediný	k2eAgFnSc6d1
shrnující	shrnující	k2eAgFnSc6d1
zprávě	zpráva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prostředí	prostředí	k1gNnSc6
diskusních	diskusní	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
se	se	k3xPyFc4
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
nezdvořilé	zdvořilý	k2eNgNnSc4d1
pokládat	pokládat	k5eAaImF
dotazy	dotaz	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
FAQ	FAQ	kA
pokryty	pokrýt	k5eAaPmNgFnP
<g/>
.	.	kIx.
</s>
<s>
Z	z	k7c2
těchto	tento	k3xDgFnPc2
pravidelně	pravidelně	k6eAd1
zasílaných	zasílaný	k2eAgFnPc2d1
zpráv	zpráva	k1gFnPc2
se	se	k3xPyFc4
FAQ	FAQ	kA
rozšířilo	rozšířit	k5eAaPmAgNnS
do	do	k7c2
mnoha	mnoho	k4c2
dalších	další	k2eAgFnPc2d1
forem	forma	k1gFnPc2
<g/>
,	,	kIx,
často	často	k6eAd1
se	se	k3xPyFc4
FAQ	FAQ	kA
dokumenty	dokument	k1gInPc4
objevují	objevovat	k5eAaImIp3nP
přímo	přímo	k6eAd1
v	v	k7c6
distribucích	distribuce	k1gFnPc6
software	software	k1gInSc1
jako	jako	k8xS,k8xC
součást	součást	k1gFnSc1
nápovědy	nápověda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nebo	nebo	k8xC
jako	jako	k9
robustní	robustní	k2eAgFnPc1d1
knowledge	knowledg	k1gFnPc1
base	basa	k1gFnSc6
řešení	řešení	k1gNnSc4
webových	webový	k2eAgFnPc2d1
stránek	stránka	k1gFnPc2
s	s	k7c7
prostorem	prostor	k1gInSc7
pro	pro	k7c4
vlastní	vlastní	k2eAgFnSc4d1
FAQ	FAQ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
pak	pak	k6eAd1
lze	lze	k6eAd1
různě	různě	k6eAd1
implementovat	implementovat	k5eAaImF
do	do	k7c2
webových	webový	k2eAgFnPc2d1
stránek	stránka	k1gFnPc2
i	i	k9
např.	např.	kA
za	za	k7c2
pomoci	pomoc	k1gFnSc2
vysouvacích	vysouvací	k2eAgInPc2d1
panelů	panel	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pojem	pojem	k1gInSc1
FAQ	FAQ	kA
se	se	k3xPyFc4
už	už	k6eAd1
objevuje	objevovat	k5eAaImIp3nS
i	i	k9
„	„	k?
<g/>
off-line	off-lin	k1gInSc5
<g/>
“	“	k?
<g/>
,	,	kIx,
např.	např.	kA
v	v	k7c6
některých	některý	k3yIgInPc6
návodech	návod	k1gInPc6
k	k	k7c3
elektronice	elektronika	k1gFnSc3
apod.	apod.	kA
</s>
<s>
Existují	existovat	k5eAaImIp3nP
tisíce	tisíc	k4xCgInPc1
dokumentů	dokument	k1gInPc2
FAQ	FAQ	kA
na	na	k7c4
různá	různý	k2eAgNnPc4d1
témata	téma	k1gNnPc4
<g/>
;	;	kIx,
na	na	k7c6
Internetu	Internet	k1gInSc6
existují	existovat	k5eAaImIp3nP
specializované	specializovaný	k2eAgInPc4d1
servery	server	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yRgInPc4,k3yIgInPc4,k3yQgInPc4
nabízejí	nabízet	k5eAaImIp3nP
sbírky	sbírka	k1gFnPc1
FAQ	FAQ	kA
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gNnSc4
fulltextové	fulltextový	k2eAgNnSc4d1
prohledávání	prohledávání	k1gNnSc4
apod.	apod.	kA
Někdy	někdy	k6eAd1
se	se	k3xPyFc4
označení	označení	k1gNnSc1
FAQ	FAQ	kA
používá	používat	k5eAaImIp3nS
pro	pro	k7c4
libovolnou	libovolný	k2eAgFnSc4d1
dokumentaci	dokumentace	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
jen	jen	k6eAd1
vzdáleně	vzdáleně	k6eAd1
týká	týkat	k5eAaImIp3nS
„	„	k?
<g/>
často	často	k6eAd1
kladených	kladený	k2eAgFnPc2d1
otázek	otázka	k1gFnPc2
<g/>
“	“	k?
<g/>
,	,	kIx,
příkladem	příklad	k1gInSc7
je	být	k5eAaImIp3nS
server	server	k1gInSc1
GameFAQs	GameFAQsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
„	„	k?
<g/>
FAQ	FAQ	kA
<g/>
“	“	k?
se	se	k3xPyFc4
objevil	objevit	k5eAaPmAgInS
až	až	k9
na	na	k7c6
Internetu	Internet	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
koncept	koncept	k1gInSc1
FAQ	FAQ	kA
je	být	k5eAaImIp3nS
relativně	relativně	k6eAd1
starý	starý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Např.	např.	kA
v	v	k7c6
roce	rok	k1gInSc6
1647	#num#	k4
vydal	vydat	k5eAaPmAgMnS
Matthew	Matthew	k1gMnSc1
Hopkins	Hopkins	k1gInSc4
knihu	kniha	k1gFnSc4
The	The	k1gMnPc2
Discovery	Discovera	k1gFnSc2
of	of	k?
Witches	Witches	k1gInSc1
(	(	kIx(
<g/>
Odhalování	odhalování	k1gNnSc1
čarodějnic	čarodějnice	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
držela	držet	k5eAaImAgFnS
formátu	formát	k1gInSc3
otázek	otázka	k1gFnPc2
a	a	k8xC
odpovědí	odpověď	k1gFnPc2
(	(	kIx(
<g/>
uváděném	uváděný	k2eAgInSc6d1
jako	jako	k8xS,k8xC
„	„	k?
<g/>
Některé	některý	k3yIgFnPc1
otázky	otázka	k1gFnPc1
zodpovězeny	zodpovědět	k5eAaPmNgFnP
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k6eAd1
klasických	klasický	k2eAgInPc2d1
katechismů	katechismus	k1gInPc2
má	mít	k5eAaImIp3nS
také	také	k9
takovou	takový	k3xDgFnSc4
strukturu	struktura	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
S.	S.	kA
<g/>
R.	R.	kA
<g/>
O.	O.	kA
<g/>
,	,	kIx,
Edgedesign	Edgedesign	k1gMnSc1
<g/>
.	.	kIx.
features	features	k1gMnSc1
<g/>
.	.	kIx.
<g/>
title	titla	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Get	Get	k1gMnSc1
Beyond	Beyond	k1gMnSc1
App	App	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
FAQ	FAQ	kA
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
FAQ	FAQ	kA
v	v	k7c6
České	český	k2eAgFnSc6d1
terminologické	terminologický	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
knihovnictví	knihovnictví	k1gNnSc2
a	a	k8xC
informační	informační	k2eAgFnSc2d1
vědy	věda	k1gFnSc2
(	(	kIx(
<g/>
TDKIV	TDKIV	kA
<g/>
)	)	kIx)
</s>
<s>
www.faqs.org	www.faqs.org	k1gInSc1
–	–	k?
Internet	Internet	k1gInSc1
FAQ	FAQ	kA
Archives	Archives	k1gInSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
FAQ	FAQ	kA
about	about	k2eAgInSc4d1
FAQs	FAQs	k1gInSc4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
www.gamefaqs.com	www.gamefaqs.com	k1gInSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
