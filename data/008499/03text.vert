<p>
<s>
Camp	camp	k1gInSc1	camp
Nou	Nou	k1gFnSc2	Nou
(	(	kIx(	(
<g/>
katalánsky	katalánsky	k6eAd1	katalánsky
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
nové	nový	k2eAgNnSc4d1	nové
hřiště	hřiště	k1gNnSc4	hřiště
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
psaný	psaný	k2eAgInSc1d1	psaný
i	i	k9	i
jako	jako	k8xC	jako
Nou	Nou	k1gFnSc1	Nou
Camp	camp	k1gInSc1	camp
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
fotbalový	fotbalový	k2eAgInSc4d1	fotbalový
stadion	stadion	k1gInSc4	stadion
ve	v	k7c6	v
španělské	španělský	k2eAgFnSc6d1	španělská
Barceloně	Barcelona	k1gFnSc6	Barcelona
<g/>
,	,	kIx,	,
domácí	domácí	k2eAgNnSc4d1	domácí
hřiště	hřiště	k1gNnSc4	hřiště
klubu	klub	k1gInSc2	klub
FC	FC	kA	FC
Barcelona	Barcelona	k1gFnSc1	Barcelona
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
a	a	k8xC	a
kapacita	kapacita	k1gFnSc1	kapacita
99	[number]	k4	99
500	[number]	k4	500
diváků	divák	k1gMnPc2	divák
ho	on	k3xPp3gMnSc4	on
činí	činit	k5eAaImIp3nS	činit
největším	veliký	k2eAgInSc7d3	veliký
fotbalovým	fotbalový	k2eAgInSc7d1	fotbalový
stadionem	stadion	k1gInSc7	stadion
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
největším	veliký	k2eAgNnSc7d3	veliký
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
regulí	regule	k1gFnPc2	regule
UEFA	UEFA	kA	UEFA
je	být	k5eAaImIp3nS	být
stadion	stadion	k1gInSc1	stadion
zařazen	zařadit	k5eAaPmNgInS	zařadit
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
bylo	být	k5eAaImAgNnS	být
jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
Estadi	Estad	k1gMnPc1	Estad
del	del	k?	del
FC	FC	kA	FC
Barcelona	Barcelona	k1gFnSc1	Barcelona
(	(	kIx(	(
<g/>
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Stadion	stadion	k1gInSc1	stadion
FC	FC	kA	FC
Barcelona	Barcelona	k1gFnSc1	Barcelona
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hlasováním	hlasování	k1gNnSc7	hlasování
členů	člen	k1gInPc2	člen
oficiálního	oficiální	k2eAgInSc2d1	oficiální
klubu	klub	k1gInSc2	klub
fanoušků	fanoušek	k1gMnPc2	fanoušek
byl	být	k5eAaImAgInS	být
však	však	k9	však
přejmenován	přejmenován	k2eAgInSc1d1	přejmenován
na	na	k7c4	na
často	často	k6eAd1	často
užívaný	užívaný	k2eAgInSc4d1	užívaný
název	název	k1gInSc4	název
Camp	camp	k1gInSc4	camp
Nou	Nou	k1gFnSc2	Nou
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Významné	významný	k2eAgInPc4d1	významný
zápasy	zápas	k1gInPc4	zápas
na	na	k7c4	na
Camp	camp	k1gInSc4	camp
Nou	Nou	k1gFnSc2	Nou
==	==	k?	==
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
×	×	k?	×
finále	finále	k1gNnSc6	finále
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
–	–	k?	–
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
5	[number]	k4	5
zápasů	zápas	k1gInPc2	zápas
včetně	včetně	k7c2	včetně
semifinále	semifinále	k1gNnSc2	semifinále
Mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
1982	[number]	k4	1982
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
x	x	k?	x
finalé	finalý	k2eAgFnSc2d1	finalý
rugbyové	rugbyové	k2eAgFnSc2d1	rugbyové
ligy	liga	k1gFnSc2	liga
Top	topit	k5eAaImRp2nS	topit
14	[number]	k4	14
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c4	na
celosezónní	celosezónní	k2eAgFnPc4d1	celosezónní
vstupenky	vstupenka	k1gFnPc4	vstupenka
se	se	k3xPyFc4	se
nečekají	čekat	k5eNaImIp3nP	čekat
žádné	žádný	k3yNgFnPc1	žádný
fronty	fronta	k1gFnPc1	fronta
<g/>
,	,	kIx,	,
jediný	jediný	k2eAgInSc1d1	jediný
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
je	být	k5eAaImIp3nS	být
získat	získat	k5eAaPmF	získat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zdědit	zdědit	k5eAaPmF	zdědit
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
či	či	k8xC	či
jinak	jinak	k6eAd1	jinak
získat	získat	k5eAaPmF	získat
od	od	k7c2	od
jiného	jiný	k2eAgMnSc2d1	jiný
majitele	majitel	k1gMnSc2	majitel
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
totiž	totiž	k9	totiž
vždy	vždy	k6eAd1	vždy
vyprodané	vyprodaný	k2eAgNnSc1d1	vyprodané
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
největších	veliký	k2eAgInPc2d3	veliký
evropských	evropský	k2eAgInPc2d1	evropský
stadionů	stadion	k1gInPc2	stadion
podle	podle	k7c2	podle
kapacity	kapacita	k1gFnSc2	kapacita
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Camp	camp	k1gInSc4	camp
Nou	Nou	k1gMnPc3	Nou
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
