<s>
Vlasovec	vlasovec	k1gMnSc1	vlasovec
mízní	mízní	k2eAgMnSc1d1	mízní
(	(	kIx(	(
<g/>
Wuchereria	Wuchererium	k1gNnPc4	Wuchererium
bancrofti	bancrofť	k1gFnSc2	bancrofť
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
parazitická	parazitický	k2eAgFnSc1d1	parazitická
hlístice	hlístice	k1gFnSc1	hlístice
patřící	patřící	k2eAgFnSc1d1	patřící
mezi	mezi	k7c4	mezi
filárie	filárie	k1gFnPc4	filárie
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
Onchocercidae	Onchocercida	k1gFnSc2	Onchocercida
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
cizopasník	cizopasník	k1gMnSc1	cizopasník
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
některých	některý	k3yIgMnPc2	některý
dalších	další	k2eAgMnPc2d1	další
primátů	primát	k1gMnPc2	primát
<g/>
.	.	kIx.	.
</s>
<s>
Přenašečem	přenašeč	k1gInSc7	přenašeč
vlasovce	vlasovec	k1gMnSc2	vlasovec
jsou	být	k5eAaImIp3nP	být
krevsající	krevsající	k2eAgMnPc1d1	krevsající
komáři	komár	k1gMnPc1	komár
z	z	k7c2	z
rodů	rod	k1gInPc2	rod
Aedes	Aedes	k1gInSc1	Aedes
<g/>
,	,	kIx,	,
Culex	Culex	k1gInSc1	Culex
<g/>
,	,	kIx,	,
Anopheles	Anopheles	k1gInSc1	Anopheles
či	či	k8xC	či
Mansonia	Mansonium	k1gNnSc2	Mansonium
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
sání	sání	k1gNnSc6	sání
se	se	k3xPyFc4	se
do	do	k7c2	do
krve	krev	k1gFnSc2	krev
definitivního	definitivní	k2eAgMnSc4d1	definitivní
hostitele	hostitel	k1gMnSc4	hostitel
(	(	kIx(	(
<g/>
např.	např.	kA	např.
člověka	člověk	k1gMnSc2	člověk
<g/>
)	)	kIx)	)
dostanou	dostat	k5eAaPmIp3nP	dostat
mikrofilárie	mikrofilárie	k1gFnPc1	mikrofilárie
<g/>
.	.	kIx.	.
</s>
<s>
Dospělec	dospělec	k1gMnSc1	dospělec
se	se	k3xPyFc4	se
vyvine	vyvinout	k5eAaPmIp3nS	vyvinout
a	a	k8xC	a
usadí	usadit	k5eAaPmIp3nS	usadit
v	v	k7c6	v
mízních	mízní	k2eAgFnPc6d1	mízní
uzlinách	uzlina	k1gFnPc6	uzlina
<g/>
.	.	kIx.	.
</s>
<s>
Ucpává	ucpávat	k5eAaImIp3nS	ucpávat
je	on	k3xPp3gNnSc4	on
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
zbytňování	zbytňování	k1gNnSc3	zbytňování
okolní	okolní	k2eAgFnSc2d1	okolní
tkáně	tkáň	k1gFnSc2	tkáň
uzliny	uzlina	k1gFnSc2	uzlina
-	-	kIx~	-
nemoc	nemoc	k1gFnSc1	nemoc
elefantiáza	elefantiáza	k1gFnSc1	elefantiáza
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
tím	ten	k3xDgNnSc7	ten
k	k	k7c3	k
obrovskému	obrovský	k2eAgNnSc3d1	obrovské
zvětšení	zvětšení	k1gNnSc3	zvětšení
některých	některý	k3yIgFnPc2	některý
částí	část	k1gFnPc2	část
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
například	například	k6eAd1	například
šourku	šourek	k1gInSc2	šourek
do	do	k7c2	do
velikosti	velikost	k1gFnSc2	velikost
pytle	pytel	k1gInSc2	pytel
brambor	brambora	k1gFnPc2	brambora
<g/>
,	,	kIx,	,
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
velikých	veliký	k2eAgInPc2d1	veliký
prsů	prs	k1gInPc2	prs
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
končetin	končetina	k1gFnPc2	končetina
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
sloní	sloní	k2eAgFnSc1d1	sloní
noha	noha	k1gFnSc1	noha
<g/>
,	,	kIx,	,
nesprávně	správně	k6eNd1	správně
elefantiáza	elefantiáza	k1gFnSc1	elefantiáza
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
velmi	velmi	k6eAd1	velmi
tencí	tenký	k2eAgMnPc1d1	tenký
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
až	až	k9	až
desítky	desítka	k1gFnPc1	desítka
centimetrů	centimetr	k1gInPc2	centimetr
dlouzí	dlouhý	k2eAgMnPc1d1	dlouhý
červi	červ	k1gMnPc1	červ
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
lymfatických	lymfatický	k2eAgFnPc6d1	lymfatická
cévách	céva	k1gFnPc6	céva
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
ucpat	ucpat	k5eAaPmF	ucpat
a	a	k8xC	a
způsobit	způsobit	k5eAaPmF	způsobit
vedle	vedle	k7c2	vedle
zánětů	zánět	k1gInPc2	zánět
tak	tak	k6eAd1	tak
typické	typický	k2eAgInPc1d1	typický
otoky	otok	k1gInPc1	otok
<g/>
,	,	kIx,	,
elefantiázy	elefantiáza	k1gFnPc1	elefantiáza
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nemoc	nemoc	k1gFnSc4	nemoc
je	být	k5eAaImIp3nS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
doba	doba	k1gFnSc1	doba
do	do	k7c2	do
propuknutí	propuknutí	k1gNnSc2	propuknutí
poměrně	poměrně	k6eAd1	poměrně
pestré	pestrý	k2eAgFnPc1d1	pestrá
klinické	klinický	k2eAgFnPc1d1	klinická
manifestace	manifestace	k1gFnPc1	manifestace
choroby	choroba	k1gFnSc2	choroba
i	i	k8xC	i
její	její	k3xOp3gInSc4	její
protrahovaný	protrahovaný	k2eAgInSc4d1	protrahovaný
průběh	průběh	k1gInSc4	průběh
<g/>
.	.	kIx.	.
</s>
<s>
Procento	procento	k1gNnSc1	procento
bezpříznakových	bezpříznakový	k2eAgInPc2d1	bezpříznakový
nosičů	nosič	k1gInPc2	nosič
trvale	trvale	k6eAd1	trvale
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
výskytu	výskyt	k1gInSc2	výskyt
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
vysoké	vysoký	k2eAgNnSc1d1	vysoké
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
subtropech	subtropy	k1gInPc6	subtropy
a	a	k8xC	a
tropech	trop	k1gInPc6	trop
rodí	rodit	k5eAaImIp3nS	rodit
oplodněná	oplodněný	k2eAgFnSc1d1	oplodněná
samička	samička	k1gFnSc1	samička
(	(	kIx(	(
<g/>
80	[number]	k4	80
<g/>
–	–	k?	–
<g/>
100	[number]	k4	100
mm	mm	kA	mm
x	x	k?	x
240	[number]	k4	240
<g/>
–	–	k?	–
<g/>
300	[number]	k4	300
μ	μ	k?	μ
<g/>
)	)	kIx)	)
této	tento	k3xDgFnSc2	tento
filárie	filárie	k1gFnSc2	filárie
v	v	k7c6	v
lymfatickém	lymfatický	k2eAgInSc6d1	lymfatický
systému	systém	k1gInSc6	systém
člověka	člověk	k1gMnSc2	člověk
právě	právě	k9	právě
takové	takový	k3xDgFnPc1	takový
larvy	larva	k1gFnPc1	larva
<g/>
,	,	kIx,	,
mikrofilárie	mikrofilárie	k1gFnPc1	mikrofilárie
(	(	kIx(	(
<g/>
230	[number]	k4	230
<g/>
–	–	k?	–
<g/>
290	[number]	k4	290
x	x	k?	x
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
μ	μ	k?	μ
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
bez	bez	k7c2	bez
nasátí	nasátí	k1gNnSc2	nasátí
komárem	komár	k1gMnSc7	komár
<g/>
/	/	kIx~	/
<g/>
mezihostitelem	mezihostitel	k1gMnSc7	mezihostitel
přibližně	přibližně	k6eAd1	přibližně
po	po	k7c6	po
deseti	deset	k4xCc6	deset
týdnech	týden	k1gInPc6	týden
hynou	hynout	k5eAaImIp3nP	hynout
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těle	tělo	k1gNnSc6	tělo
přenašeče	přenašeč	k1gInSc2	přenašeč
cirkulují	cirkulovat	k5eAaImIp3nP	cirkulovat
z	z	k7c2	z
žaludku	žaludek	k1gInSc2	žaludek
do	do	k7c2	do
hrudních	hrudní	k2eAgInPc2d1	hrudní
svalů	sval	k1gInPc2	sval
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
spodního	spodní	k2eAgInSc2d1	spodní
pysku	pysk	k1gInSc2	pysk
a	a	k8xC	a
právě	právě	k9	právě
odtud	odtud	k6eAd1	odtud
pak	pak	k6eAd1	pak
při	při	k7c6	při
dalším	další	k2eAgNnSc6d1	další
sání	sání	k1gNnSc6	sání
aktivně	aktivně	k6eAd1	aktivně
pronikají	pronikat	k5eAaImIp3nP	pronikat
na	na	k7c4	na
kůži	kůže	k1gFnSc4	kůže
hostitele	hostitel	k1gMnSc2	hostitel
a	a	k8xC	a
skrze	skrze	k?	skrze
ni	on	k3xPp3gFnSc4	on
do	do	k7c2	do
cévního	cévní	k2eAgNnSc2d1	cévní
řečiště	řečiště	k1gNnSc2	řečiště
<g/>
.	.	kIx.	.
</s>
<s>
Rostou	růst	k5eAaImIp3nP	růst
a	a	k8xC	a
dospívají	dospívat	k5eAaImIp3nP	dospívat
v	v	k7c6	v
lymfatických	lymfatický	k2eAgFnPc6d1	lymfatická
cestách	cesta	k1gFnPc6	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Záhadou	záhada	k1gFnSc7	záhada
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
maximální	maximální	k2eAgInSc4d1	maximální
výskyt	výskyt	k1gInSc4	výskyt
mikrofilárií	mikrofilárie	k1gFnPc2	mikrofilárie
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
v	v	k7c6	v
nočních	noční	k2eAgFnPc6d1	noční
hodinách	hodina	k1gFnPc6	hodina
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Microfilaria	Microfilarium	k1gNnSc2	Microfilarium
nocturna	nocturno	k1gNnSc2	nocturno
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ale	ale	k9	ale
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
maximu	maxima	k1gFnSc4	maxima
aktivity	aktivita	k1gFnSc2	aktivita
komárů	komár	k1gMnPc2	komár
<g/>
.	.	kIx.	.
</s>
<s>
Den	den	k1gInSc1	den
tráví	trávit	k5eAaImIp3nS	trávit
mikrofilárie	mikrofilárie	k1gFnPc4	mikrofilárie
v	v	k7c6	v
plicních	plicní	k2eAgFnPc6d1	plicní
kapilárách	kapilára	k1gFnPc6	kapilára
a	a	k8xC	a
vyšetření	vyšetření	k1gNnSc4	vyšetření
krve	krev	k1gFnSc2	krev
tedy	tedy	k9	tedy
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
falešně	falešně	k6eAd1	falešně
negativní	negativní	k2eAgMnSc1d1	negativní
(	(	kIx(	(
<g/>
cirkadiální	cirkadiální	k2eAgInSc1d1	cirkadiální
rytmus	rytmus	k1gInSc1	rytmus
se	se	k3xPyFc4	se
udrží	udržet	k5eAaPmIp3nS	udržet
i	i	k9	i
po	po	k7c6	po
změně	změna	k1gFnSc6	změna
časového	časový	k2eAgNnSc2d1	časové
pásma	pásmo	k1gNnSc2	pásmo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Léčí	léčit	k5eAaImIp3nS	léčit
se	se	k3xPyFc4	se
ivermectinem	ivermectin	k1gInSc7	ivermectin
<g/>
.	.	kIx.	.
</s>
<s>
VOLF	Volf	k1gMnSc1	Volf
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
;	;	kIx,	;
HORÁK	Horák	k1gMnSc1	Horák
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Paraziti	parazit	k1gMnPc1	parazit
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc2	jejich
biologie	biologie	k1gFnSc2	biologie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Triton	triton	k1gMnSc1	triton
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Vyd	Vyd	k1gMnSc1	Vyd
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7387	[number]	k4	7387
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
318	[number]	k4	318
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vlasovec	vlasovec	k1gMnSc1	vlasovec
mízní	mízní	k2eAgMnSc1d1	mízní
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Vlasovec	vlasovec	k1gMnSc1	vlasovec
mízní	mízní	k2eAgInSc4d1	mízní
na	na	k7c4	na
Biolibu	Bioliba	k1gFnSc4	Bioliba
</s>
