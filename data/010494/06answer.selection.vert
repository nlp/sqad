<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Sobotce	Sobotka	k1gFnSc6	Sobotka
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1885	[number]	k4	1885
se	se	k3xPyFc4	se
s	s	k7c7	s
rodiči	rodič	k1gMnPc7	rodič
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
do	do	k7c2	do
Písku	Písek	k1gInSc2	Písek
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
město	město	k1gNnSc1	město
si	se	k3xPyFc3	se
velmi	velmi	k6eAd1	velmi
oblíbil	oblíbit	k5eAaPmAgMnS	oblíbit
<g/>
,	,	kIx,	,
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
jeho	jeho	k3xOp3gFnPc2	jeho
divadelních	divadelní	k2eAgFnPc2d1	divadelní
her	hra	k1gFnPc2	hra
a	a	k8xC	a
románů	román	k1gInPc2	román
<g/>
.	.	kIx.	.
</s>
