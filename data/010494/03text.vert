<p>
<s>
Fráňa	Fráňa	k1gMnSc1	Fráňa
Šrámek	Šrámek	k1gMnSc1	Šrámek
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1877	[number]	k4	1877
Sobotka	Sobotka	k1gFnSc1	Sobotka
–	–	k?	–
1	[number]	k4	1
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1952	[number]	k4	1952
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
prozaik	prozaik	k1gMnSc1	prozaik
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
buřič	buřič	k1gMnSc1	buřič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Sobotce	Sobotka	k1gFnSc6	Sobotka
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1885	[number]	k4	1885
se	se	k3xPyFc4	se
s	s	k7c7	s
rodiči	rodič	k1gMnPc7	rodič
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
do	do	k7c2	do
Písku	Písek	k1gInSc2	Písek
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc1	tento
město	město	k1gNnSc1	město
si	se	k3xPyFc3	se
velmi	velmi	k6eAd1	velmi
oblíbil	oblíbit	k5eAaPmAgMnS	oblíbit
<g/>
,	,	kIx,	,
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
jeho	jeho	k3xOp3gFnPc2	jeho
divadelních	divadelní	k2eAgFnPc2d1	divadelní
her	hra	k1gFnPc2	hra
a	a	k8xC	a
románů	román	k1gInPc2	román
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
svých	svůj	k3xOyFgNnPc2	svůj
studentských	studentský	k2eAgNnPc2d1	studentské
let	léto	k1gNnPc2	léto
bydlel	bydlet	k5eAaImAgMnS	bydlet
v	v	k7c6	v
domě	dům	k1gInSc6	dům
U	u	k7c2	u
Koulí	koule	k1gFnPc2	koule
čp.	čp.	k?	čp.
31	[number]	k4	31
nedaleko	nedaleko	k7c2	nedaleko
Putimské	putimský	k2eAgFnSc2d1	Putimská
brány	brána	k1gFnSc2	brána
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1894	[number]	k4	1894
se	se	k3xPyFc4	se
odstěhoval	odstěhovat	k5eAaPmAgMnS	odstěhovat
do	do	k7c2	do
Roudnice	Roudnice	k1gFnSc2	Roudnice
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
maturity	maturita	k1gFnSc2	maturita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1903	[number]	k4	1903
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
na	na	k7c4	na
jednoroční	jednoroční	k2eAgFnSc4d1	jednoroční
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
službu	služba	k1gFnSc4	služba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
za	za	k7c4	za
trest	trest	k1gInSc4	trest
o	o	k7c4	o
rok	rok	k1gInSc4	rok
prodloužena	prodloužit	k5eAaPmNgFnS	prodloužit
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
již	již	k6eAd1	již
projevily	projevit	k5eAaPmAgInP	projevit
jeho	jeho	k3xOp3gInPc1	jeho
antimilitaristické	antimilitaristický	k2eAgInPc1d1	antimilitaristický
postoje	postoj	k1gInPc1	postoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
1904	[number]	k4	1904
vážně	vážně	k6eAd1	vážně
onemocněl	onemocnět	k5eAaPmAgInS	onemocnět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
vojenské	vojenský	k2eAgFnSc2d1	vojenská
služby	služba	k1gFnSc2	služba
začal	začít	k5eAaPmAgInS	začít
studovat	studovat	k5eAaImF	studovat
práva	právo	k1gNnPc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Studia	studio	k1gNnSc2	studio
ukončil	ukončit	k5eAaPmAgMnS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
věnovat	věnovat	k5eAaPmF	věnovat
literatuře	literatura	k1gFnSc3	literatura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dostal	dostat	k5eAaPmAgMnS	dostat
se	se	k3xPyFc4	se
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgInS	připojit
k	k	k7c3	k
časopisu	časopis	k1gInSc3	časopis
Nový	nový	k2eAgInSc1d1	nový
kult	kult	k1gInSc1	kult
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
vůdčí	vůdčí	k2eAgFnSc7d1	vůdčí
osobností	osobnost	k1gFnSc7	osobnost
byl	být	k5eAaImAgMnS	být
S.	S.	kA	S.
K.	K.	kA	K.
Neumann	Neumann	k1gMnSc1	Neumann
<g/>
.	.	kIx.	.
</s>
<s>
Redigoval	redigovat	k5eAaImAgInS	redigovat
časopis	časopis	k1gInSc1	časopis
Práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
byl	být	k5eAaImAgMnS	být
dvakrát	dvakrát	k6eAd1	dvakrát
vězněn	věznit	k5eAaImNgMnS	věznit
<g/>
,	,	kIx,	,
za	za	k7c4	za
účast	účast	k1gFnSc4	účast
na	na	k7c6	na
demonstracích	demonstrace	k1gFnPc6	demonstrace
a	a	k8xC	a
pro	pro	k7c4	pro
antimilitaristickou	antimilitaristický	k2eAgFnSc4d1	antimilitaristická
báseň	báseň	k1gFnSc4	báseň
Píšou	psát	k5eAaImIp3nP	psát
mi	já	k3xPp1nSc3	já
psaní	psaní	k1gNnSc3	psaní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Za	za	k7c2	za
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
narukoval	narukovat	k5eAaPmAgMnS	narukovat
hned	hned	k6eAd1	hned
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1914	[number]	k4	1914
na	na	k7c4	na
haličskou	haličský	k2eAgFnSc4d1	Haličská
frontu	fronta	k1gFnSc4	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Střet	střet	k1gInSc1	střet
s	s	k7c7	s
válečnou	válečný	k2eAgFnSc7d1	válečná
realitou	realita	k1gFnSc7	realita
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
drsný	drsný	k2eAgInSc1d1	drsný
<g/>
.	.	kIx.	.
</s>
<s>
Šrámkovy	Šrámkův	k2eAgInPc4d1	Šrámkův
bezprostřední	bezprostřední	k2eAgInPc4d1	bezprostřední
dojmy	dojem	k1gInPc4	dojem
odráží	odrážet	k5eAaImIp3nS	odrážet
nejen	nejen	k6eAd1	nejen
báseň	báseň	k1gFnSc1	báseň
s	s	k7c7	s
lakonickým	lakonický	k2eAgInSc7d1	lakonický
titulem	titul	k1gInSc7	titul
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
třeba	třeba	k6eAd1	třeba
také	také	k9	také
povídka	povídka	k1gFnSc1	povídka
První	první	k4xOgInSc4	první
akt	akt	k1gInSc4	akt
<g/>
.	.	kIx.	.
</s>
<s>
Už	už	k6eAd1	už
v	v	k7c6	v
září	září	k1gNnSc6	září
1914	[number]	k4	1914
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
ale	ale	k9	ale
podařilo	podařit	k5eAaPmAgNnS	podařit
válečné	válečný	k2eAgFnSc3d1	válečná
vřavě	vřava	k1gFnSc3	vřava
uniknout	uniknout	k5eAaPmF	uniknout
<g/>
,	,	kIx,	,
když	když	k8xS	když
byl	být	k5eAaImAgInS	být
kvůli	kvůli	k7c3	kvůli
revmatismu	revmatismus	k1gInSc3	revmatismus
hospitalizován	hospitalizován	k2eAgMnSc1d1	hospitalizován
v	v	k7c6	v
brněnské	brněnský	k2eAgFnSc6d1	brněnská
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
1915	[number]	k4	1915
ale	ale	k8xC	ale
znova	znova	k6eAd1	znova
narukoval	narukovat	k5eAaPmAgMnS	narukovat
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
na	na	k7c4	na
frontu	fronta	k1gFnSc4	fronta
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
Rumunska	Rumunsko	k1gNnSc2	Rumunsko
<g/>
.	.	kIx.	.
<g/>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
na	na	k7c4	na
protest	protest	k1gInSc4	protest
proti	proti	k7c3	proti
fašismu	fašismus	k1gInSc3	fašismus
nevycházel	vycházet	k5eNaImAgMnS	vycházet
z	z	k7c2	z
domu	dům	k1gInSc2	dům
<g/>
,	,	kIx,	,
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
protestu	protest	k1gInSc6	protest
však	však	k9	však
vědělo	vědět	k5eAaImAgNnS	vědět
málo	málo	k6eAd1	málo
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
národním	národní	k2eAgMnSc7d1	národní
umělcem	umělec	k1gMnSc7	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Pohřben	pohřben	k2eAgMnSc1d1	pohřben
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
rodném	rodný	k2eAgNnSc6d1	rodné
městě	město	k1gNnSc6	město
Sobotce	Sobotka	k1gFnSc3	Sobotka
na	na	k7c6	na
soboteckém	sobotecký	k2eAgInSc6d1	sobotecký
hřbitově	hřbitov	k1gInSc6	hřbitov
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterém	který	k3yQgInSc6	který
napsal	napsat	k5eAaBmAgInS	napsat
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
básní	báseň	k1gFnPc2	báseň
Sobotecký	Sobotecký	k2eAgInSc4d1	Sobotecký
hřbitov	hřbitov	k1gInSc4	hřbitov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
tvorba	tvorba	k1gFnSc1	tvorba
byla	být	k5eAaImAgFnS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
impresionismem	impresionismus	k1gInSc7	impresionismus
<g/>
,	,	kIx,	,
antimilitaristickými	antimilitaristický	k2eAgInPc7d1	antimilitaristický
až	až	k8xS	až
pacifistickými	pacifistický	k2eAgInPc7d1	pacifistický
postoji	postoj	k1gInPc7	postoj
<g/>
.	.	kIx.	.
</s>
<s>
F.	F.	kA	F.
Šrámek	šrámek	k1gInSc1	šrámek
se	se	k3xPyFc4	se
angažoval	angažovat	k5eAaBmAgInS	angažovat
v	v	k7c6	v
anarchistickém	anarchistický	k2eAgNnSc6d1	anarchistické
hnutí	hnutí	k1gNnSc6	hnutí
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
významným	významný	k2eAgMnSc7d1	významný
představitelem	představitel	k1gMnSc7	představitel
tzv.	tzv.	kA	tzv.
generace	generace	k1gFnPc1	generace
anarchistických	anarchistický	k2eAgMnPc2d1	anarchistický
buřičů	buřič	k1gMnPc2	buřič
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
jeho	jeho	k3xOp3gNnPc1	jeho
díla	dílo	k1gNnPc1	dílo
jsou	být	k5eAaImIp3nP	být
silně	silně	k6eAd1	silně
levicová	levicový	k2eAgFnSc1d1	levicová
<g/>
,	,	kIx,	,
ovlivněná	ovlivněný	k2eAgFnSc1d1	ovlivněná
anarchismem	anarchismus	k1gInSc7	anarchismus
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	on	k3xPp3gNnSc2	on
díla	dílo	k1gNnSc2	dílo
měla	mít	k5eAaImAgFnS	mít
značný	značný	k2eAgInSc4d1	značný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
mladou	mladý	k2eAgFnSc4d1	mladá
generaci	generace	k1gFnSc4	generace
mezi	mezi	k7c7	mezi
světovými	světový	k2eAgFnPc7d1	světová
válkami	válka	k1gFnPc7	válka
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
jeho	jeho	k3xOp3gFnPc1	jeho
básně	báseň	k1gFnPc1	báseň
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
revolučními	revoluční	k2eAgFnPc7d1	revoluční
písněmi	píseň	k1gFnPc7	píseň
proletářů	proletář	k1gMnPc2	proletář
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
těchto	tento	k3xDgNnPc2	tento
základních	základní	k2eAgNnPc2d1	základní
témat	téma	k1gNnPc2	téma
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
příroda	příroda	k1gFnSc1	příroda
</s>
</p>
<p>
<s>
odpor	odpor	k1gInSc4	odpor
k	k	k7c3	k
válce	válka	k1gFnSc3	válka
–	–	k?	–
svůj	svůj	k3xOyFgInSc4	svůj
vztah	vztah	k1gInSc4	vztah
k	k	k7c3	k
válce	válka	k1gFnSc3	válka
žene	hnát	k5eAaImIp3nS	hnát
až	až	k9	až
k	k	k7c3	k
odmítání	odmítání	k1gNnSc3	odmítání
válek	válka	k1gFnPc2	válka
obranných	obranný	k2eAgFnPc2d1	obranná
</s>
</p>
<p>
<s>
milostný	milostný	k2eAgInSc1d1	milostný
vztah	vztah	k1gInSc1	vztah
mladých	mladý	k2eAgMnPc2d1	mladý
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
neschopných	schopný	k2eNgMnPc2d1	neschopný
se	se	k3xPyFc4	se
podřídit	podřídit	k5eAaPmF	podřídit
konvencím	konvence	k1gFnPc3	konvence
</s>
</p>
<p>
<s>
===	===	k?	===
Básnické	básnický	k2eAgFnPc4d1	básnická
sbírky	sbírka	k1gFnPc4	sbírka
===	===	k?	===
</s>
</p>
<p>
<s>
Života	život	k1gInSc2	život
bído	bída	k1gFnSc5	bída
<g/>
,	,	kIx,	,
přec	přec	k9	přec
tě	ty	k3xPp2nSc4	ty
mám	mít	k5eAaImIp1nS	mít
rád	rád	k6eAd1	rád
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
–	–	k?	–
vyjádření	vyjádření	k1gNnSc4	vyjádření
odporu	odpor	k1gInSc2	odpor
k	k	k7c3	k
militarismu	militarismus	k1gInSc3	militarismus
a	a	k8xC	a
Rakousku-Uhersku	Rakousku-Uhersek	k1gInSc3	Rakousku-Uhersek
</s>
</p>
<p>
<s>
Modrý	modrý	k2eAgInSc1d1	modrý
a	a	k8xC	a
rudý	rudý	k2eAgMnSc1d1	rudý
(	(	kIx(	(
<g/>
1906	[number]	k4	1906
<g/>
)	)	kIx)	)
–	–	k?	–
antimilitarismus	antimilitarismus	k1gInSc1	antimilitarismus
<g/>
,	,	kIx,	,
vzpoura	vzpoura	k1gFnSc1	vzpoura
proti	proti	k7c3	proti
válce	válka	k1gFnSc3	válka
<g/>
,	,	kIx,	,
symbolika	symbolika	k1gFnSc1	symbolika
barev	barva	k1gFnPc2	barva
v	v	k7c6	v
názvu	název	k1gInSc6	název
(	(	kIx(	(
<g/>
modrá	modrý	k2eAgFnSc1d1	modrá
barva	barva	k1gFnSc1	barva
rakouské	rakouský	k2eAgFnSc2d1	rakouská
uniformy	uniforma	k1gFnSc2	uniforma
<g/>
,	,	kIx,	,
červená	červený	k2eAgFnSc1d1	červená
barvou	barva	k1gFnSc7	barva
socialismu	socialismus	k1gInSc2	socialismus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
báseň	báseň	k1gFnSc1	báseň
Raport	raport	k1gInSc1	raport
</s>
</p>
<p>
<s>
Splav	splav	k1gInSc1	splav
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
,	,	kIx,	,
1922	[number]	k4	1922
v	v	k7c6	v
rozšířené	rozšířený	k2eAgFnSc6d1	rozšířená
verzi	verze	k1gFnSc6	verze
s	s	k7c7	s
básněmi	báseň	k1gFnPc7	báseň
z	z	k7c2	z
války	válka	k1gFnSc2	válka
<g/>
)	)	kIx)	)
–	–	k?	–
milostná	milostný	k2eAgFnSc1d1	milostná
a	a	k8xC	a
přírodní	přírodní	k2eAgFnSc1d1	přírodní
lyrika	lyrika	k1gFnSc1	lyrika
s	s	k7c7	s
protiválečným	protiválečný	k2eAgInSc7d1	protiválečný
podtextem	podtext	k1gInSc7	podtext
<g/>
,	,	kIx,	,
žena	žena	k1gFnSc1	žena
a	a	k8xC	a
příroda	příroda	k1gFnSc1	příroda
splývají	splývat	k5eAaImIp3nP	splývat
do	do	k7c2	do
jednoho	jeden	k4xCgMnSc2	jeden
<g/>
,	,	kIx,	,
impresionistická	impresionistický	k2eAgFnSc1d1	impresionistická
lyrika	lyrika	k1gFnSc1	lyrika
<g/>
,	,	kIx,	,
senzitivní	senzitivní	k2eAgFnSc1d1	senzitivní
<g/>
,	,	kIx,	,
plná	plný	k2eAgFnSc1d1	plná
představ	představ	k1gInSc1	představ
<g/>
,	,	kIx,	,
harmonická	harmonický	k2eAgFnSc1d1	harmonická
<g/>
,	,	kIx,	,
milostná	milostný	k2eAgFnSc1d1	milostná
lyrika	lyrika	k1gFnSc1	lyrika
<g/>
,	,	kIx,	,
nejobyčejnější	obyčejný	k2eAgInPc4d3	nejobyčejnější
a	a	k8xC	a
nejpřirozenější	přirozený	k2eAgInPc4d3	nejpřirozenější
projevy	projev	k1gInPc4	projev
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
stejnojmenná	stejnojmenný	k2eAgFnSc1d1	stejnojmenná
báseň	báseň	k1gFnSc1	báseň
Splav	splav	k1gInSc1	splav
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nové	Nové	k2eAgFnPc1d1	Nové
básně	báseň	k1gFnPc1	báseň
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ještě	ještě	k6eAd1	ještě
zní	znět	k5eAaImIp3nS	znět
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
–	–	k?	–
zde	zde	k6eAd1	zde
již	již	k6eAd1	již
životní	životní	k2eAgFnSc2d1	životní
rezignace	rezignace	k1gFnSc2	rezignace
<g/>
,	,	kIx,	,
hořkost	hořkost	k1gFnSc1	hořkost
<g/>
,	,	kIx,	,
loučení	loučení	k1gNnSc1	loučení
</s>
</p>
<p>
<s>
Rány	Rána	k1gFnPc1	Rána
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
(	(	kIx(	(
<g/>
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
–	–	k?	–
vlastenecké	vlastenecký	k2eAgFnPc4d1	vlastenecká
básně	báseň	k1gFnPc4	báseň
<g/>
,	,	kIx,	,
reakce	reakce	k1gFnSc1	reakce
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
světovou	světový	k2eAgFnSc4d1	světová
válku	válka	k1gFnSc4	válka
a	a	k8xC	a
na	na	k7c4	na
osvobození	osvobození	k1gNnSc4	osvobození
republiky	republika	k1gFnSc2	republika
</s>
</p>
<p>
<s>
===	===	k?	===
Romány	román	k1gInPc4	román
===	===	k?	===
</s>
</p>
<p>
<s>
Stříbrný	stříbrný	k2eAgInSc1d1	stříbrný
vítr	vítr	k1gInSc1	vítr
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
)	)	kIx)	)
–	–	k?	–
román	román	k1gInSc1	román
o	o	k7c4	o
mládí	mládí	k1gNnSc4	mládí
a	a	k8xC	a
dospívání	dospívání	k1gNnSc4	dospívání
<g/>
,	,	kIx,	,
autobiografické	autobiografický	k2eAgInPc4d1	autobiografický
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
impresionistické	impresionistický	k2eAgNnSc1d1	impresionistické
líčení	líčení	k1gNnSc1	líčení
citové	citový	k2eAgFnSc2d1	citová
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
Jeník	Jeník	k1gMnSc1	Jeník
Ratkin	Ratkin	k1gMnSc1	Ratkin
se	se	k3xPyFc4	se
vzpírá	vzpírat	k5eAaImIp3nS	vzpírat
tyranskému	tyranský	k2eAgMnSc3d1	tyranský
otci	otec	k1gMnSc3	otec
<g/>
,	,	kIx,	,
nerozumí	rozumět	k5eNaImIp3nS	rozumět
světu	svět	k1gInSc3	svět
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
,	,	kIx,	,
získává	získávat	k5eAaImIp3nS	získávat
první	první	k4xOgFnPc4	první
milostné	milostný	k2eAgFnPc4d1	milostná
zkušenosti	zkušenost	k1gFnPc4	zkušenost
<g/>
,	,	kIx,	,
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
se	se	k3xPyFc4	se
do	do	k7c2	do
Aničky	Anička	k1gFnSc2	Anička
Karasové	Karasová	k1gFnSc2	Karasová
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
zklamání	zklamání	k1gNnSc1	zklamání
<g/>
,	,	kIx,	,
Jeník	Jeník	k1gMnSc1	Jeník
přes	přes	k7c4	přes
veškeré	veškerý	k3xTgInPc4	veškerý
konflikty	konflikt	k1gInPc4	konflikt
mládí	mládí	k1gNnSc2	mládí
poznává	poznávat	k5eAaImIp3nS	poznávat
smysl	smysl	k1gInSc4	smysl
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
připraven	připravit	k5eAaPmNgInS	připravit
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
čestně	čestně	k6eAd1	čestně
zápasit	zápasit	k5eAaImF	zápasit
a	a	k8xC	a
dokázat	dokázat	k5eAaPmF	dokázat
svůj	svůj	k3xOyFgInSc4	svůj
přerod	přerod	k1gInSc4	přerod
v	v	k7c4	v
muže	muž	k1gMnPc4	muž
<g/>
,	,	kIx,	,
druhé	druhý	k4xOgNnSc1	druhý
přepracované	přepracovaný	k2eAgNnSc1d1	přepracované
vydání	vydání	k1gNnSc1	vydání
doplněné	doplněná	k1gFnSc2	doplněná
autorovým	autorův	k2eAgInSc7d1	autorův
doslovem	doslov	k1gInSc7	doslov
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
–	–	k?	–
vyjádření	vyjádření	k1gNnSc4	vyjádření
citů	cit	k1gInPc2	cit
mladé	mladý	k2eAgFnSc2d1	mladá
generace	generace	k1gFnSc2	generace
(	(	kIx(	(
<g/>
zfilmován	zfilmován	k2eAgInSc1d1	zfilmován
Václavem	Václav	k1gMnSc7	Václav
Krškou	Krška	k1gMnSc7	Krška
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
román	román	k1gInSc1	román
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
ovlivněn	ovlivnit	k5eAaPmNgInS	ovlivnit
impresionismem	impresionismus	k1gInSc7	impresionismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tělo	tělo	k1gNnSc1	tělo
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
–	–	k?	–
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
působil	působit	k5eAaImAgMnS	působit
na	na	k7c4	na
mladou	mladý	k2eAgFnSc4d1	mladá
generaci	generace	k1gFnSc4	generace
<g/>
,	,	kIx,	,
u	u	k7c2	u
kritiky	kritika	k1gFnSc2	kritika
však	však	k9	však
vzbudil	vzbudit	k5eAaPmAgMnS	vzbudit
rozporné	rozporný	k2eAgFnPc4d1	rozporná
reakce	reakce	k1gFnPc4	reakce
</s>
</p>
<p>
<s>
Past	past	k1gFnSc1	past
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Křižovatky	křižovatka	k1gFnPc1	křižovatka
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
–	–	k?	–
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
pesimismus	pesimismus	k1gInSc4	pesimismus
<g/>
,	,	kIx,	,
radostnou	radostný	k2eAgFnSc4d1	radostná
víru	víra	k1gFnSc4	víra
v	v	k7c6	v
mládí	mládí	k1gNnSc6	mládí
zastoupila	zastoupit	k5eAaPmAgFnS	zastoupit
skepse	skepse	k1gFnSc1	skepse
<g/>
,	,	kIx,	,
láska	láska	k1gFnSc1	láska
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
zklamání	zklamání	k1gNnSc3	zklamání
</s>
</p>
<p>
<s>
===	===	k?	===
Povídkové	povídkový	k2eAgFnPc1d1	povídková
knihy	kniha	k1gFnPc1	kniha
===	===	k?	===
</s>
</p>
<p>
<s>
Sláva	Sláva	k1gFnSc1	Sláva
života	život	k1gInSc2	život
</s>
</p>
<p>
<s>
Žasnoucí	žasnoucí	k2eAgMnSc1d1	žasnoucí
voják	voják	k1gMnSc1	voják
–	–	k?	–
obyčejný	obyčejný	k2eAgMnSc1d1	obyčejný
člověk	člověk	k1gMnSc1	člověk
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
nic	nic	k3yNnSc1	nic
neznamená	znamenat	k5eNaImIp3nS	znamenat
</s>
</p>
<p>
<s>
Osika	osika	k1gFnSc1	osika
</s>
</p>
<p>
<s>
Klavír	klavír	k1gInSc4	klavír
a	a	k8xC	a
housle	housle	k1gFnPc4	housle
</s>
</p>
<p>
<s>
Prvních	první	k4xOgFnPc2	první
jednadvacet	jednadvacet	k4xCc1	jednadvacet
</s>
</p>
<p>
<s>
===	===	k?	===
Divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
===	===	k?	===
</s>
</p>
<p>
<s>
Červen	červen	k1gInSc1	červen
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
–	–	k?	–
impresionistické	impresionistický	k2eAgNnSc4d1	impresionistické
drama	drama	k1gNnSc4	drama
</s>
</p>
<p>
<s>
Luna	luna	k1gFnSc1	luna
</s>
</p>
<p>
<s>
Zvony	zvon	k1gInPc1	zvon
</s>
</p>
<p>
<s>
Léto	léto	k1gNnSc1	léto
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
)	)	kIx)	)
–	–	k?	–
impresionistické	impresionistický	k2eAgNnSc1d1	impresionistické
divadelní	divadelní	k2eAgNnSc1d1	divadelní
drama	drama	k1gNnSc1	drama
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
student	student	k1gMnSc1	student
se	se	k3xPyFc4	se
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
do	do	k7c2	do
vdané	vdaný	k2eAgFnSc2d1	vdaná
paničky	panička	k1gFnSc2	panička
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
jen	jen	k6eAd1	jen
předvádí	předvádět	k5eAaImIp3nS	předvádět
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
žena	žena	k1gFnSc1	žena
bez	bez	k7c2	bez
morálky	morálka	k1gFnSc2	morálka
a	a	k8xC	a
zábran	zábrana	k1gFnPc2	zábrana
<g/>
.	.	kIx.	.
</s>
<s>
Student	student	k1gMnSc1	student
zmoudří	zmoudřet	k5eAaPmIp3nS	zmoudřet
a	a	k8xC	a
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
se	se	k3xPyFc4	se
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
vrstevnice	vrstevnice	k1gFnSc2	vrstevnice
Stázie	Stázie	k1gFnSc2	Stázie
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
prožitek	prožitek	k1gInSc4	prožitek
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
nad	nad	k7c7	nad
řekou	řeka	k1gFnSc7	řeka
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
–	–	k?	–
setkání	setkání	k1gNnSc1	setkání
bývalých	bývalý	k2eAgMnPc2d1	bývalý
spolužáků	spolužák	k1gMnPc2	spolužák
ze	z	k7c2	z
studií	studie	k1gFnPc2	studie
<g/>
,	,	kIx,	,
zamyšlení	zamyšlení	k1gNnPc2	zamyšlení
nad	nad	k7c7	nad
ztracenými	ztracený	k2eAgFnPc7d1	ztracená
iluzemi	iluze	k1gFnPc7	iluze
mládí	mládí	k1gNnSc2	mládí
<g/>
,	,	kIx,	,
střet	střet	k1gInSc1	střet
starého	starý	k2eAgInSc2d1	starý
a	a	k8xC	a
nového	nový	k2eAgInSc2d1	nový
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
konflikt	konflikt	k1gInSc1	konflikt
mezi	mezi	k7c7	mezi
generacemi	generace	k1gFnPc7	generace
<g/>
,	,	kIx,	,
projev	projev	k1gInSc1	projev
českého	český	k2eAgInSc2d1	český
impresionismu	impresionismus	k1gInSc2	impresionismus
<g/>
,	,	kIx,	,
zfilmováno	zfilmován	k2eAgNnSc1d1	zfilmováno
Václavem	Václav	k1gMnSc7	Václav
Krškou	Krška	k1gMnSc7	Krška
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
</s>
</p>
<p>
<s>
Plačící	plačící	k2eAgMnSc1d1	plačící
satyr	satyr	k1gMnSc1	satyr
</s>
</p>
<p>
<s>
Ostrov	ostrov	k1gInSc1	ostrov
velké	velký	k2eAgFnSc2d1	velká
lásky	láska	k1gFnSc2	láska
</s>
</p>
<p>
<s>
Hagenbek	Hagenbek	k1gInSc1	Hagenbek
–	–	k?	–
satira	satira	k1gFnSc1	satira
na	na	k7c4	na
konec	konec	k1gInSc4	konec
Rakouska-Uherska	Rakouska-Uhersk	k1gInSc2	Rakouska-Uhersk
</s>
</p>
<p>
<s>
Dvě	dva	k4xCgNnPc4	dva
království	království	k1gNnPc4	království
–	–	k?	–
O	o	k7c6	o
dozrání	dozrání	k1gNnSc6	dozrání
Duby	Duba	k1gMnSc2	Duba
Ortové	Ortová	k1gFnSc2	Ortová
v	v	k7c4	v
ženu	žena	k1gFnSc4	žena
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
studentském	studentský	k2eAgInSc6d1	studentský
pokoji	pokoj	k1gInSc6	pokoj
<g/>
.	.	kIx.	.
</s>
<s>
Nedbá	nedbat	k5eAaImIp3nS	nedbat
výchovy	výchov	k1gInPc4	výchov
rodičů	rodič	k1gMnPc2	rodič
a	a	k8xC	a
vzdá	vzdát	k5eAaPmIp3nS	vzdát
se	se	k3xPyFc4	se
muži	muž	k1gMnSc3	muž
<g/>
,	,	kIx,	,
kterého	který	k3yRgInSc2	který
má	mít	k5eAaImIp3nS	mít
ráda	rád	k2eAgFnSc1d1	ráda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pravda	pravda	k9	pravda
</s>
</p>
<p>
<s>
==	==	k?	==
Filmové	filmový	k2eAgFnSc2d1	filmová
adaptace	adaptace	k1gFnSc2	adaptace
==	==	k?	==
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
dílem	dílo	k1gNnSc7	dílo
byl	být	k5eAaImAgInS	být
silně	silně	k6eAd1	silně
ovlivněn	ovlivněn	k2eAgMnSc1d1	ovlivněn
režisér	režisér	k1gMnSc1	režisér
Václav	Václav	k1gMnSc1	Václav
Krška	Krška	k1gMnSc1	Krška
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
úspěšné	úspěšný	k2eAgFnSc2d1	úspěšná
filmové	filmový	k2eAgFnSc2d1	filmová
adaptace	adaptace	k1gFnSc2	adaptace
divadelní	divadelní	k2eAgFnSc2d1	divadelní
hry	hra	k1gFnSc2	hra
Měsíc	měsíc	k1gInSc4	měsíc
nad	nad	k7c7	nad
řekou	řeka	k1gFnSc7	řeka
(	(	kIx(	(
<g/>
1953	[number]	k4	1953
<g/>
)	)	kIx)	)
a	a	k8xC	a
Stříbrný	stříbrný	k2eAgInSc1d1	stříbrný
vítr	vítr	k1gInSc1	vítr
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BURIÁNEK	Buriánek	k1gMnSc1	Buriánek
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
.	.	kIx.	.
</s>
<s>
Fráňa	Fráňa	k1gMnSc1	Fráňa
Šrámek	Šrámek	k1gMnSc1	Šrámek
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc6	vydání
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
1981	[number]	k4	1981
<g/>
.	.	kIx.	.
292	[number]	k4	292
s.	s.	k?	s.
cnb	cnb	k?	cnb
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
127037	[number]	k4	127037
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čeští	český	k2eAgMnPc1d1	český
spisovatelé	spisovatel	k1gMnPc1	spisovatel
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Československý	československý	k2eAgMnSc1d1	československý
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
281	[number]	k4	281
<g/>
–	–	k?	–
<g/>
285	[number]	k4	285
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Literatura	literatura	k1gFnSc1	literatura
od	od	k7c2	od
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
/	/	kIx~	/
hlavní	hlavní	k2eAgMnSc1d1	hlavní
redaktor	redaktor	k1gMnSc1	redaktor
Jan	Jan	k1gMnSc1	Jan
Mukařovský	Mukařovský	k1gMnSc1	Mukařovský
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Victoria	Victorium	k1gNnSc2	Victorium
Publishing	Publishing	k1gInSc1	Publishing
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
714	[number]	k4	714
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85865	[number]	k4	85865
<g/>
-	-	kIx~	-
<g/>
48	[number]	k4	48
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
139	[number]	k4	139
<g/>
–	–	k?	–
<g/>
152	[number]	k4	152
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
</s>
</p>
<p>
<s>
GÖTZ	GÖTZ	kA	GÖTZ
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
<g/>
,	,	kIx,	,
TETAUER	TETAUER	kA	TETAUER
<g/>
,	,	kIx,	,
Frank	Frank	k1gMnSc1	Frank
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
umění	umění	k1gNnSc1	umění
dramatické	dramatický	k2eAgNnSc1d1	dramatické
<g/>
,	,	kIx,	,
Část	část	k1gFnSc1	část
I.	I.	kA	I.
–	–	k?	–
činohra	činohra	k1gFnSc1	činohra
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Šolc	Šolc	k1gMnSc1	Šolc
a	a	k8xC	a
Šimáček	Šimáček	k1gMnSc1	Šimáček
<g/>
,	,	kIx,	,
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
230	[number]	k4	230
<g/>
–	–	k?	–
<g/>
233	[number]	k4	233
</s>
</p>
<p>
<s>
MERHAUT	MERHAUT	kA	MERHAUT
<g/>
,	,	kIx,	,
Luboš	Luboš	k1gMnSc1	Luboš
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Lexikon	lexikon	k1gInSc1	lexikon
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
:	:	kIx,	:
osobnosti	osobnost	k1gFnPc1	osobnost
<g/>
,	,	kIx,	,
díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
instituce	instituce	k1gFnPc1	instituce
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
/	/	kIx~	/
<g/>
I.	I.	kA	I.
S	s	k7c7	s
<g/>
–	–	k?	–
<g/>
T.	T.	kA	T.
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
1082	[number]	k4	1082
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
1670	[number]	k4	1670
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
712	[number]	k4	712
<g/>
–	–	k?	–
<g/>
718	[number]	k4	718
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Osobnosti	osobnost	k1gFnPc1	osobnost
–	–	k?	–
Česko	Česko	k1gNnSc1	Česko
:	:	kIx,	:
Ottův	Ottův	k2eAgInSc1d1	Ottův
slovník	slovník	k1gInSc1	slovník
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Ottovo	Ottův	k2eAgNnSc1d1	Ottovo
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
823	[number]	k4	823
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7360	[number]	k4	7360
<g/>
-	-	kIx~	-
<g/>
796	[number]	k4	796
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
702	[number]	k4	702
<g/>
–	–	k?	–
<g/>
703	[number]	k4	703
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TOMEŠ	Tomeš	k1gMnSc1	Tomeš
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgInSc1d1	český
biografický	biografický	k2eAgInSc1d1	biografický
slovník	slovník	k1gInSc1	slovník
XX	XX	kA	XX
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
:	:	kIx,	:
III	III	kA	III
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
:	:	kIx,	:
Q	Q	kA	Q
<g/>
–	–	k?	–
<g/>
Ž.	Ž.	kA	Ž.
Praha	Praha	k1gFnSc1	Praha
;	;	kIx,	;
Litomyšl	Litomyšl	k1gFnSc1	Litomyšl
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
;	;	kIx,	;
Petr	Petr	k1gMnSc1	Petr
Meissner	Meissner	k1gMnSc1	Meissner
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
587	[number]	k4	587
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7185	[number]	k4	7185
<g/>
-	-	kIx~	-
<g/>
247	[number]	k4	247
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
296	[number]	k4	296
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
českých	český	k2eAgMnPc2d1	český
spisovatelů	spisovatel	k1gMnPc2	spisovatel
</s>
</p>
<p>
<s>
Česká	český	k2eAgFnSc1d1	Česká
literatura	literatura	k1gFnSc1	literatura
v	v	k7c6	v
letech	let	k1gInPc6	let
1900	[number]	k4	1900
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
</s>
</p>
<p>
<s>
Impresionismus	impresionismus	k1gInSc1	impresionismus
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Fráňa	Fráňa	k1gFnSc1	Fráňa
Šrámek	šrámek	k1gInSc4	šrámek
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Fráňa	Fráňa	k1gMnSc1	Fráňa
Šrámek	Šrámek	k1gMnSc1	Šrámek
</s>
</p>
<p>
<s>
Osoba	osoba	k1gFnSc1	osoba
Fráňa	Fráňa	k1gMnSc1	Fráňa
Šrámek	Šrámek	k1gMnSc1	Šrámek
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
<p>
<s>
Wikilivres	Wikilivres	k1gMnSc1	Wikilivres
<g/>
:	:	kIx,	:
<g/>
Fráňa	Fráňa	k1gMnSc1	Fráňa
Šrámek	Šrámek	k1gMnSc1	Šrámek
<g/>
:	:	kIx,	:
díla	dílo	k1gNnPc4	dílo
na	na	k7c6	na
projektu	projekt	k1gInSc6	projekt
Wikilivres	Wikilivresa	k1gFnPc2	Wikilivresa
</s>
</p>
