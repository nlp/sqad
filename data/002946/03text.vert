<s>
Fuzzy	Fuzz	k1gInPc1	Fuzz
logika	logika	k1gFnSc1	logika
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
též	též	k9	též
mlhavá	mlhavý	k2eAgFnSc1d1	mlhavá
logika	logika	k1gFnSc1	logika
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
podobor	podobor	k1gInSc4	podobor
matematické	matematický	k2eAgFnSc2d1	matematická
logiky	logika	k1gFnSc2	logika
odvozený	odvozený	k2eAgMnSc1d1	odvozený
od	od	k7c2	od
teorie	teorie	k1gFnSc2	teorie
fuzzy	fuzza	k1gFnSc2	fuzza
množin	množina	k1gFnPc2	množina
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
logické	logický	k2eAgInPc1d1	logický
výroky	výrok	k1gInPc1	výrok
ohodnocují	ohodnocovat	k5eAaImIp3nP	ohodnocovat
mírou	míra	k1gFnSc7	míra
pravdivosti	pravdivost	k1gFnSc2	pravdivost
<g/>
.	.	kIx.	.
</s>
<s>
Liší	lišit	k5eAaImIp3nS	lišit
se	se	k3xPyFc4	se
tak	tak	k9	tak
od	od	k7c2	od
klasické	klasický	k2eAgFnSc2d1	klasická
výrokové	výrokový	k2eAgFnSc2d1	výroková
logiky	logika	k1gFnSc2	logika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
používá	používat	k5eAaImIp3nS	používat
pouze	pouze	k6eAd1	pouze
dvě	dva	k4xCgFnPc1	dva
logické	logický	k2eAgFnPc1d1	logická
hodnoty	hodnota	k1gFnPc1	hodnota
-	-	kIx~	-
pravdu	pravda	k1gFnSc4	pravda
a	a	k8xC	a
nepravdu	nepravda	k1gFnSc4	nepravda
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
zapisované	zapisovaný	k2eAgFnPc1d1	zapisovaná
jako	jako	k8xS	jako
1	[number]	k4	1
a	a	k8xC	a
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Fuzzy	Fuzza	k1gFnPc4	Fuzza
logika	logika	k1gFnSc1	logika
může	moct	k5eAaImIp3nS	moct
operovat	operovat	k5eAaImF	operovat
se	s	k7c7	s
všemi	všecek	k3xTgFnPc7	všecek
hodnotami	hodnota	k1gFnPc7	hodnota
z	z	k7c2	z
intervalu	interval	k1gInSc2	interval
<	<	kIx(	<
<g/>
0	[number]	k4	0
<g/>
;	;	kIx,	;
1	[number]	k4	1
<g/>
>	>	kIx)	>
<g/>
,	,	kIx,	,
kterých	který	k3yQgMnPc2	který
je	být	k5eAaImIp3nS	být
nekonečně	konečně	k6eNd1	konečně
mnoho	mnoho	k4c1	mnoho
<g/>
.	.	kIx.	.
</s>
<s>
Fuzzy	Fuzz	k1gInPc1	Fuzz
logika	logika	k1gFnSc1	logika
náleží	náležet	k5eAaImIp3nP	náležet
mezi	mezi	k7c4	mezi
vícehodnotové	vícehodnotový	k2eAgFnPc4d1	vícehodnotová
logiky	logika	k1gFnPc4	logika
<g/>
.	.	kIx.	.
</s>
<s>
Fuzzy	Fuzza	k1gFnPc4	Fuzza
logika	logika	k1gFnSc1	logika
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
pro	pro	k7c4	pro
řadu	řada	k1gFnSc4	řada
reálných	reálný	k2eAgFnPc2d1	reálná
rozhodovacích	rozhodovací	k2eAgFnPc2d1	rozhodovací
úloh	úloha	k1gFnPc2	úloha
vhodnější	vhodný	k2eAgFnSc1d2	vhodnější
než	než	k8xS	než
klasická	klasický	k2eAgFnSc1d1	klasická
logika	logika	k1gFnSc1	logika
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
usnadňuje	usnadňovat	k5eAaImIp3nS	usnadňovat
návrh	návrh	k1gInSc4	návrh
složitých	složitý	k2eAgInPc2d1	složitý
řídicích	řídicí	k2eAgInPc2d1	řídicí
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
anglického	anglický	k2eAgNnSc2d1	anglické
slova	slovo	k1gNnSc2	slovo
fuzzy	fuzza	k1gFnSc2	fuzza
-	-	kIx~	-
nejasný	jasný	k2eNgInSc1d1	nejasný
<g/>
,	,	kIx,	,
mlhavý	mlhavý	k2eAgInSc1d1	mlhavý
<g/>
,	,	kIx,	,
neostrý	ostrý	k2eNgInSc1d1	neostrý
(	(	kIx(	(
<g/>
potažmo	potažmo	k6eAd1	potažmo
neurčitý	určitý	k2eNgInSc1d1	neurčitý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Případně	případně	k6eAd1	případně
rovněž	rovněž	k9	rovněž
nepřesný	přesný	k2eNgMnSc1d1	nepřesný
<g/>
,	,	kIx,	,
zmatený	zmatený	k2eAgMnSc1d1	zmatený
(	(	kIx(	(
<g/>
konfúzní	konfúzní	k2eAgMnSc1d1	konfúzní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Fuzzy	Fuzz	k1gInPc4	Fuzz
logika	logika	k1gFnSc1	logika
byla	být	k5eAaImAgFnS	být
zavedena	zaveden	k2eAgFnSc1d1	zavedena
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
Lotfim	Lotfimo	k1gNnPc2	Lotfimo
Zadehem	Zadeh	k1gInSc7	Zadeh
z	z	k7c2	z
Kalifornské	kalifornský	k2eAgFnSc2d1	kalifornská
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c4	v
Berkeley	Berkele	k1gMnPc4	Berkele
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
z	z	k7c2	z
teorie	teorie	k1gFnSc2	teorie
fuzzy	fuzza	k1gFnSc2	fuzza
množin	množina	k1gFnPc2	množina
<g/>
,	,	kIx,	,
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
předmětem	předmět	k1gInSc7	předmět
zájmu	zájem	k1gInSc2	zájem
matematiků	matematik	k1gMnPc2	matematik
a	a	k8xC	a
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Motivace	motivace	k1gFnSc1	motivace
vzniku	vznik	k1gInSc2	vznik
fuzzy	fuzza	k1gFnSc2	fuzza
množin	množina	k1gFnPc2	množina
a	a	k8xC	a
návazně	návazně	k6eAd1	návazně
fuzzy	fuzza	k1gFnPc1	fuzza
logiky	logika	k1gFnSc2	logika
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vytvořit	vytvořit	k5eAaPmF	vytvořit
nástroj	nástroj	k1gInSc4	nástroj
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
mostem	most	k1gInSc7	most
mezi	mezi	k7c7	mezi
dvěma	dva	k4xCgInPc7	dva
typy	typ	k1gInPc7	typ
znalostí	znalost	k1gFnPc2	znalost
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgInPc7	jenž
je	být	k5eAaImIp3nS	být
propast	propast	k1gFnSc1	propast
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
vysvětlení	vysvětlení	k1gNnPc4	vysvětlení
si	se	k3xPyFc3	se
z	z	k7c2	z
hesla	heslo	k1gNnSc2	heslo
Vágnost	vágnost	k1gFnSc1	vágnost
uvedeného	uvedený	k2eAgInSc2d1	uvedený
zde	zde	k6eAd1	zde
na	na	k7c6	na
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
vypůjčíme	vypůjčit	k5eAaPmIp1nP	vypůjčit
odstavec	odstavec	k1gInSc4	odstavec
<g/>
:	:	kIx,	:
Mezi	mezi	k7c7	mezi
znalostmi	znalost	k1gFnPc7	znalost
získanými	získaný	k2eAgFnPc7d1	získaná
přirozeným	přirozený	k2eAgNnSc7d1	přirozené
poznáním	poznání	k1gNnSc7	poznání
a	a	k8xC	a
znalostmi	znalost	k1gFnPc7	znalost
získanými	získaný	k2eAgFnPc7d1	získaná
poznáním	poznání	k1gNnSc7	poznání
metodou	metoda	k1gFnSc7	metoda
exaktních	exaktní	k2eAgFnPc2d1	exaktní
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
kvalitativní	kvalitativní	k2eAgFnSc1d1	kvalitativní
propast	propast	k1gFnSc1	propast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvém	prvý	k4xOgInSc6	prvý
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
na	na	k7c4	na
svět	svět	k1gInSc4	svět
díváme	dívat	k5eAaImIp1nP	dívat
filtrem	filtr	k1gInSc7	filtr
vágnosti	vágnost	k1gFnSc2	vágnost
<g/>
,	,	kIx,	,
v	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
případě	případ	k1gInSc6	případ
filtrem	filtr	k1gInSc7	filtr
"	"	kIx"	"
<g/>
dírkovaným	dírkovaný	k2eAgInSc7d1	dírkovaný
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
dírkami	dírka	k1gFnPc7	dírka
"	"	kIx"	"
<g/>
vidíme	vidět	k5eAaImIp1nP	vidět
<g/>
"	"	kIx"	"
atributy	atribut	k1gInPc4	atribut
(	(	kIx(	(
<g/>
měřitelné	měřitelný	k2eAgFnPc4d1	měřitelná
veličiny	veličina	k1gFnPc4	veličina
a	a	k8xC	a
parametry	parametr	k1gInPc4	parametr
<g/>
)	)	kIx)	)
-	-	kIx~	-
elementární	elementární	k2eAgFnPc4d1	elementární
manifestace	manifestace	k1gFnPc4	manifestace
reálného	reálný	k2eAgInSc2d1	reálný
světa	svět	k1gInSc2	svět
a	a	k8xC	a
vztahy	vztah	k1gInPc4	vztah
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
<g/>
,	,	kIx,	,
a	a	k8xC	a
nic	nic	k3yNnSc4	nic
jiného	jiný	k2eAgMnSc4d1	jiný
<g/>
.	.	kIx.	.
</s>
<s>
Newton	Newton	k1gMnSc1	Newton
"	"	kIx"	"
<g/>
digitalizoval	digitalizovat	k5eAaImAgInS	digitalizovat
<g/>
"	"	kIx"	"
přirozený	přirozený	k2eAgInSc1d1	přirozený
vágní	vágní	k2eAgInSc1d1	vágní
pohled	pohled	k1gInSc1	pohled
člověka	člověk	k1gMnSc2	člověk
na	na	k7c4	na
reálný	reálný	k2eAgInSc4d1	reálný
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
</s>
<s>
Inherentně	inherentně	k6eAd1	inherentně
vágní	vágní	k2eAgFnSc2d1	vágní
znalosti	znalost	k1gFnSc2	znalost
získané	získaný	k2eAgFnSc2d1	získaná
přirozeným	přirozený	k2eAgNnSc7d1	přirozené
poznáním	poznání	k1gNnSc7	poznání
lze	lze	k6eAd1	lze
sdělovat	sdělovat	k5eAaImF	sdělovat
(	(	kIx(	(
<g/>
reprezentovat	reprezentovat	k5eAaImF	reprezentovat
<g/>
,	,	kIx,	,
popsat	popsat	k5eAaPmF	popsat
<g/>
)	)	kIx)	)
jen	jen	k9	jen
a	a	k8xC	a
jen	jen	k9	jen
neformálním	formální	k2eNgInSc7d1	neformální
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
přirozeným	přirozený	k2eAgInSc7d1	přirozený
<g/>
.	.	kIx.	.
</s>
<s>
Znalosti	znalost	k1gFnSc2	znalost
získané	získaný	k2eAgFnSc2d1	získaná
umělým	umělý	k2eAgNnSc7d1	umělé
poznáním	poznání	k1gNnSc7	poznání
lze	lze	k6eAd1	lze
reprezentovat	reprezentovat	k5eAaImF	reprezentovat
umělým	umělý	k2eAgInSc7d1	umělý
formálním	formální	k2eAgInSc7d1	formální
jazykem	jazyk	k1gInSc7	jazyk
(	(	kIx(	(
<g/>
matematika	matematika	k1gFnSc1	matematika
<g/>
,	,	kIx,	,
logika	logika	k1gFnSc1	logika
<g/>
,	,	kIx,	,
programovací	programovací	k2eAgInPc1d1	programovací
jazyky	jazyk	k1gInPc1	jazyk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
inherentně	inherentně	k6eAd1	inherentně
vágní	vágní	k2eAgInPc4d1	vágní
výroky	výrok	k1gInPc4	výrok
přirozeného	přirozený	k2eAgInSc2d1	přirozený
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
převést	převést	k5eAaPmF	převést
do	do	k7c2	do
formálního	formální	k2eAgInSc2d1	formální
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgInSc7	jenž
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
je	být	k5eAaImIp3nS	být
fuzzy	fuzza	k1gFnPc4	fuzza
logika	logika	k1gFnSc1	logika
a	a	k8xC	a
překonat	překonat	k5eAaPmF	překonat
tak	tak	k6eAd1	tak
onu	onen	k3xDgFnSc4	onen
výše	vysoce	k6eAd2	vysoce
uvedenou	uvedený	k2eAgFnSc4d1	uvedená
propast	propast	k1gFnSc4	propast
<g/>
.	.	kIx.	.
,	,	kIx,	,
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
vágnost	vágnost	k1gFnSc1	vágnost
přirozeného	přirozený	k2eAgInSc2d1	přirozený
jazyka	jazyk	k1gInSc2	jazyk
je	být	k5eAaImIp3nS	být
především	především	k6eAd1	především
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
pro	pro	k7c4	pro
příjemce	příjemce	k1gMnPc4	příjemce
utajená	utajený	k2eAgFnSc1d1	utajená
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
ji	on	k3xPp3gFnSc4	on
jen	jen	k9	jen
odhadovat	odhadovat	k5eAaImF	odhadovat
<g/>
)	)	kIx)	)
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
u	u	k7c2	u
kvantifikátorů	kvantifikátor	k1gInPc2	kvantifikátor
<g/>
,	,	kIx,	,
a	a	k8xC	a
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
vágnost	vágnost	k1gFnSc1	vágnost
umělého	umělý	k2eAgInSc2d1	umělý
formálního	formální	k2eAgInSc2d1	formální
jazyka	jazyk	k1gInSc2	jazyk
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
vždy	vždy	k6eAd1	vždy
nulová	nulový	k2eAgFnSc1d1	nulová
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
původní	původní	k2eAgFnSc4d1	původní
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
vágnost	vágnost	k1gFnSc4	vágnost
odstranit	odstranit	k5eAaPmF	odstranit
a	a	k8xC	a
převést	převést	k5eAaPmF	převést
ji	on	k3xPp3gFnSc4	on
na	na	k7c4	na
vnější	vnější	k2eAgFnSc4d1	vnější
vágnost	vágnost	k1gFnSc4	vágnost
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
je	být	k5eAaImIp3nS	být
umělý	umělý	k2eAgInSc1d1	umělý
formální	formální	k2eAgInSc1d1	formální
jazyk	jazyk	k1gInSc1	jazyk
fuzzy	fuzza	k1gFnSc2	fuzza
množin	množina	k1gFnPc2	množina
a	a	k8xC	a
fuzzy	fuzza	k1gFnSc2	fuzza
logiky	logika	k1gFnSc2	logika
schopen	schopen	k2eAgMnSc1d1	schopen
reprezentovat	reprezentovat	k5eAaImF	reprezentovat
<g/>
.	.	kIx.	.
</s>
<s>
Znamená	znamenat	k5eAaImIp3nS	znamenat
to	ten	k3xDgNnSc1	ten
vyzpovídat	vyzpovídat	k5eAaPmF	vyzpovídat
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
skupinu	skupina	k1gFnSc4	skupina
lidí	člověk	k1gMnPc2	člověk
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
shodli	shodnout	k5eAaPmAgMnP	shodnout
např.	např.	kA	např.
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
co	co	k9	co
pro	pro	k7c4	pro
ně	on	k3xPp3gFnPc4	on
fuzzy	fuzza	k1gFnPc4	fuzza
kvantitativně	kvantitativně	k6eAd1	kvantitativně
vyjádřeno	vyjádřit	k5eAaPmNgNnS	vyjádřit
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
příjemně	příjemně	k6eAd1	příjemně
teplá	teplý	k2eAgFnSc1d1	teplá
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
spíš	spíš	k9	spíš
vyšší	vysoký	k2eAgInSc4d2	vyšší
strom	strom	k1gInSc4	strom
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
nepříliš	příliš	k6eNd1	příliš
chytrý	chytrý	k2eAgMnSc1d1	chytrý
člověk	člověk	k1gMnSc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgNnSc1d1	původní
vágní	vágní	k2eAgNnSc1d1	vágní
chápání	chápání	k1gNnSc1	chápání
vyjádřené	vyjádřený	k2eAgNnSc1d1	vyjádřené
přirozeným	přirozený	k2eAgInSc7d1	přirozený
jazykem	jazyk	k1gInSc7	jazyk
se	se	k3xPyFc4	se
převádí	převádět	k5eAaImIp3nS	převádět
na	na	k7c4	na
fuzzy	fuzz	k1gInPc4	fuzz
hodnoty	hodnota	k1gFnSc2	hodnota
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
možno	možno	k6eAd1	možno
dávat	dávat	k5eAaImF	dávat
do	do	k7c2	do
souvislostí	souvislost	k1gFnPc2	souvislost
popsaných	popsaný	k2eAgFnPc2d1	popsaná
fuzzy	fuzz	k1gInPc4	fuzz
operacemi	operace	k1gFnPc7	operace
fuzzy	fuzz	k1gInPc4	fuzz
logiky	logika	k1gFnSc2	logika
<g/>
.	.	kIx.	.
</s>
<s>
Převod	převod	k1gInSc1	převod
z	z	k7c2	z
přirozeného	přirozený	k2eAgInSc2d1	přirozený
jazyka	jazyk	k1gInSc2	jazyk
do	do	k7c2	do
umělého	umělý	k2eAgInSc2d1	umělý
formálního	formální	k2eAgInSc2d1	formální
jazyka	jazyk	k1gInSc2	jazyk
fuzzy	fuzza	k1gFnSc2	fuzza
logiky	logika	k1gFnSc2	logika
je	být	k5eAaImIp3nS	být
vágní	vágní	k2eAgNnSc1d1	vágní
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
poznamenán	poznamenat	k5eAaPmNgInS	poznamenat
nejistotou	nejistota	k1gFnSc7	nejistota
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
významy	význam	k1gInPc4	význam
jazykových	jazykový	k2eAgFnPc2d1	jazyková
konstrukcí	konstrukce	k1gFnPc2	konstrukce
přirozeného	přirozený	k2eAgInSc2d1	přirozený
jazyka	jazyk	k1gInSc2	jazyk
jsou	být	k5eAaImIp3nP	být
každým	každý	k3xTgMnSc7	každý
člověkem	člověk	k1gMnSc7	člověk
přiřazovány	přiřazován	k2eAgFnPc1d1	přiřazován
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
emotivní	emotivní	k2eAgFnSc2d1	emotivní
<g/>
,	,	kIx,	,
subjektivní	subjektivní	k2eAgFnSc2d1	subjektivní
a	a	k8xC	a
vágní	vágní	k2eAgFnSc2d1	vágní
konotace	konotace	k1gFnSc2	konotace
<g/>
,	,	kIx,	,
měnící	měnící	k2eAgFnSc2d1	měnící
se	se	k3xPyFc4	se
od	od	k7c2	od
člověka	člověk	k1gMnSc2	člověk
k	k	k7c3	k
člověku	člověk	k1gMnSc3	člověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
každého	každý	k3xTgMnSc4	každý
i	i	k8xC	i
v	v	k7c6	v
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
</s>
<s>
Sebe	sebe	k3xPyFc4	sebe
sofistikovanější	sofistikovaný	k2eAgNnPc1d2	sofistikovanější
vyzpovídání	vyzpovídání	k1gNnPc1	vyzpovídání
respondentů	respondent	k1gMnPc2	respondent
nezaručí	zaručit	k5eNaPmIp3nP	zaručit
nulovou	nulový	k2eAgFnSc4d1	nulová
neurčitost	neurčitost	k1gFnSc4	neurčitost
onoho	onen	k3xDgInSc2	onen
převodu	převod	k1gInSc2	převod
z	z	k7c2	z
přirozeného	přirozený	k2eAgInSc2d1	přirozený
jazyka	jazyk	k1gInSc2	jazyk
do	do	k7c2	do
umělého	umělý	k2eAgInSc2d1	umělý
formálního	formální	k2eAgInSc2d1	formální
jazyka	jazyk	k1gInSc2	jazyk
fuzzy	fuzza	k1gFnSc2	fuzza
množin	množina	k1gFnPc2	množina
a	a	k8xC	a
fuzzy	fuzza	k1gFnSc2	fuzza
logiky	logika	k1gFnSc2	logika
<g/>
.	.	kIx.	.
</s>
<s>
Platí	platit	k5eAaImIp3nS	platit
zde	zde	k6eAd1	zde
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
jsme	být	k5eAaImIp1nP	být
řekli	říct	k5eAaPmAgMnP	říct
v	v	k7c6	v
heslech	heslo	k1gNnPc6	heslo
Wikipedie	Wikipedie	k1gFnPc1	Wikipedie
Vágnost	vágnost	k1gFnSc1	vágnost
a	a	k8xC	a
Exaktní	exaktní	k2eAgFnSc1d1	exaktní
věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
<g/>
:	:	kIx,	:
požadujeme	požadovat	k5eAaImIp1nP	požadovat
<g/>
-li	i	k?	-li
exaktní	exaktní	k2eAgInPc1d1	exaktní
poznatky	poznatek	k1gInPc1	poznatek
zapsatelné	zapsatelný	k2eAgNnSc1d1	zapsatelné
umělým	umělý	k2eAgInSc7d1	umělý
formálním	formální	k2eAgInSc7d1	formální
jazykem	jazyk	k1gInSc7	jazyk
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
začít	začít	k5eAaPmF	začít
exaktním	exaktní	k2eAgNnSc7d1	exaktní
Newtonovým	Newtonův	k2eAgNnSc7d1	Newtonovo
umělým	umělý	k2eAgNnSc7d1	umělé
poznáním	poznání	k1gNnSc7	poznání
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
inherentně	inherentně	k6eAd1	inherentně
vágní	vágní	k2eAgFnPc1d1	vágní
znalosti	znalost	k1gFnPc1	znalost
získané	získaný	k2eAgFnPc1d1	získaná
přirozeným	přirozený	k2eAgNnSc7d1	přirozené
lidským	lidský	k2eAgNnSc7d1	lidské
poznáním	poznání	k1gNnSc7	poznání
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
filtrem	filtr	k1gInSc7	filtr
poznáním	poznání	k1gNnSc7	poznání
vágnost	vágnost	k1gFnSc4	vágnost
<g/>
,	,	kIx,	,
dodatečně	dodatečně	k6eAd1	dodatečně
převést	převést	k5eAaPmF	převést
na	na	k7c4	na
exaktní	exaktní	k2eAgFnPc4d1	exaktní
znalosti	znalost	k1gFnPc4	znalost
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
zbavit	zbavit	k5eAaPmF	zbavit
je	on	k3xPp3gFnPc4	on
vnitřní	vnitřní	k2eAgFnPc4d1	vnitřní
vágnosti	vágnost	k1gFnPc4	vágnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
dodatečně	dodatečně	k6eAd1	dodatečně
zkvalitnit	zkvalitnit	k5eAaPmF	zkvalitnit
informaci	informace	k1gFnSc4	informace
<g/>
.	.	kIx.	.
</s>
<s>
Fuzzy	Fuzza	k1gFnPc4	Fuzza
logika	logika	k1gFnSc1	logika
postupně	postupně	k6eAd1	postupně
nalezla	naleznout	k5eAaPmAgFnS	naleznout
i	i	k9	i
jiná	jiný	k2eAgNnPc4d1	jiné
použití	použití	k1gNnPc4	použití
<g/>
,	,	kIx,	,
na	na	k7c4	na
příklad	příklad	k1gInSc4	příklad
v	v	k7c6	v
automatickém	automatický	k2eAgNnSc6d1	automatické
řízení	řízení	k1gNnSc6	řízení
<g/>
.	.	kIx.	.
</s>
<s>
Funkce	funkce	k1gFnSc1	funkce
příslušnosti	příslušnost	k1gFnSc2	příslušnost
ve	v	k7c4	v
fuzzy	fuzza	k1gFnPc4	fuzza
logice	logika	k1gFnSc3	logika
přiřazuje	přiřazovat	k5eAaImIp3nS	přiřazovat
příslušnost	příslušnost	k1gFnSc1	příslušnost
k	k	k7c3	k
množinám	množina	k1gFnPc3	množina
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
0	[number]	k4	0
do	do	k7c2	do
1	[number]	k4	1
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
obou	dva	k4xCgFnPc2	dva
hraničních	hraniční	k2eAgFnPc2d1	hraniční
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Fuzzy	Fuzz	k1gInPc1	Fuzz
logika	logika	k1gFnSc1	logika
tak	tak	k9	tak
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
matematicky	matematicky	k6eAd1	matematicky
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
pojmy	pojem	k1gInPc4	pojem
jako	jako	k9	jako
"	"	kIx"	"
<g/>
trochu	trochu	k6eAd1	trochu
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
dost	dost	k6eAd1	dost
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
hodně	hodně	k6eAd1	hodně
<g/>
"	"	kIx"	"
apod.	apod.	kA	apod.
</s>
<s>
Přesněji	přesně	k6eAd2	přesně
<g/>
,	,	kIx,	,
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
částečnou	částečný	k2eAgFnSc4d1	částečná
příslušnost	příslušnost	k1gFnSc4	příslušnost
k	k	k7c3	k
množině	množina	k1gFnSc3	množina
<g/>
.	.	kIx.	.
</s>
<s>
Fuzzy	Fuzza	k1gFnPc4	Fuzza
logika	logika	k1gFnSc1	logika
používá	používat	k5eAaImIp3nS	používat
stupeň	stupeň	k1gInSc4	stupeň
příslušnosti	příslušnost	k1gFnSc2	příslušnost
(	(	kIx(	(
<g/>
míru	mír	k1gInSc2	mír
pravdivosti	pravdivost	k1gFnSc2	pravdivost
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
matematický	matematický	k2eAgInSc1d1	matematický
model	model	k1gInSc1	model
vágnosti	vágnost	k1gFnSc2	vágnost
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
je	být	k5eAaImIp3nS	být
matematický	matematický	k2eAgInSc4d1	matematický
model	model	k1gInSc4	model
neznalosti	neznalost	k1gFnSc2	neznalost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
fuzzy	fuzza	k1gFnSc2	fuzza
logika	logika	k1gFnSc1	logika
může	moct	k5eAaImIp3nS	moct
modelovat	modelovat	k5eAaImF	modelovat
pouze	pouze	k6eAd1	pouze
sdělitelnou	sdělitelný	k2eAgFnSc4d1	sdělitelná
vnější	vnější	k2eAgFnSc4d1	vnější
vágnost	vágnost	k1gFnSc4	vágnost
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
vágnosti	vágnost	k1gFnSc2	vágnost
vyskytující	vyskytující	k2eAgFnSc6d1	vyskytující
se	se	k3xPyFc4	se
v	v	k7c6	v
konotaci	konotace	k1gFnSc6	konotace
(	(	kIx(	(
<g/>
vágní	vágní	k2eAgFnSc2d1	vágní
<g/>
,	,	kIx,	,
subjektivní	subjektivní	k2eAgMnSc1d1	subjektivní
a	a	k8xC	a
emocionálně	emocionálně	k6eAd1	emocionálně
zabarvené	zabarvený	k2eAgFnSc6d1	zabarvená
interpretaci	interpretace	k1gFnSc6	interpretace
<g/>
)	)	kIx)	)
jazykové	jazykový	k2eAgFnSc2d1	jazyková
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Fuzzy	Fuzza	k1gFnPc1	Fuzza
logika	logika	k1gFnSc1	logika
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
každý	každý	k3xTgInSc1	každý
formální	formální	k2eAgInSc1d1	formální
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
přísně	přísně	k6eAd1	přísně
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
exaktní	exaktní	k2eAgFnSc4d1	exaktní
interpretaci	interpretace	k1gFnSc4	interpretace
všech	všecek	k3xTgFnPc2	všecek
použitých	použitý	k2eAgFnPc2d1	použitá
jazykových	jazykový	k2eAgFnPc2d1	jazyková
konstrukcí	konstrukce	k1gFnPc2	konstrukce
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
nulovou	nulový	k2eAgFnSc4d1	nulová
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
vágnost	vágnost	k1gFnSc4	vágnost
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
tedy	tedy	k9	tedy
nulový	nulový	k2eAgInSc1d1	nulový
sémantický	sémantický	k2eAgInSc1d1	sémantický
diferenciál	diferenciál	k1gInSc1	diferenciál
této	tento	k3xDgFnSc2	tento
interpretace	interpretace	k1gFnSc2	interpretace
<g/>
.	.	kIx.	.
</s>
<s>
Stupeň	stupeň	k1gInSc1	stupeň
příslušnosti	příslušnost	k1gFnSc2	příslušnost
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
zaměňován	zaměňován	k2eAgInSc1d1	zaměňován
s	s	k7c7	s
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
pojmy	pojem	k1gInPc1	pojem
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
rozdílné	rozdílný	k2eAgInPc1d1	rozdílný
<g/>
.	.	kIx.	.
</s>
<s>
Fuzzy	Fuzza	k1gFnPc4	Fuzza
hodnota	hodnota	k1gFnSc1	hodnota
je	být	k5eAaImIp3nS	být
přiřazena	přiřadit	k5eAaPmNgFnS	přiřadit
funkcí	funkce	k1gFnSc7	funkce
příslušnosti	příslušnost	k1gFnSc2	příslušnost
k	k	k7c3	k
vágně	vágně	k6eAd1	vágně
definovaným	definovaný	k2eAgFnPc3d1	definovaná
množinám	množina	k1gFnPc3	množina
a	a	k8xC	a
nepředstavuje	představovat	k5eNaImIp3nS	představovat
pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
nějakého	nějaký	k3yIgInSc2	nějaký
jevu	jev	k1gInSc2	jev
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
u	u	k7c2	u
ní	on	k3xPp3gFnSc2	on
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
možnosti	možnost	k1gFnPc4	možnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mohou	moct	k5eAaImIp3nP	moct
nastat	nastat	k5eAaPmF	nastat
a	a	k8xC	a
možnosti	možnost	k1gFnPc1	možnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
nastanou	nastat	k5eAaPmIp3nP	nastat
<g/>
.	.	kIx.	.
</s>
<s>
Jinou	jiný	k2eAgFnSc7d1	jiná
vědní	vědní	k2eAgFnSc7d1	vědní
disciplínou	disciplína	k1gFnSc7	disciplína
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
využívat	využívat	k5eAaPmF	využívat
principů	princip	k1gInPc2	princip
fuzzy	fuzza	k1gFnSc2	fuzza
logiky	logika	k1gFnSc2	logika
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
kvantová	kvantový	k2eAgFnSc1d1	kvantová
fyzika	fyzika	k1gFnSc1	fyzika
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
též	též	k9	též
počítá	počítat	k5eAaImIp3nS	počítat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohou	moct	k5eAaImIp3nP	moct
existovat	existovat	k5eAaImF	existovat
i	i	k9	i
stavy	stav	k1gInPc1	stav
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yQgInPc2	který
je	být	k5eAaImIp3nS	být
výsledek	výsledek	k1gInSc1	výsledek
měření	měření	k1gNnSc2	měření
předpověditelný	předpověditelný	k2eAgInSc1d1	předpověditelný
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
30	[number]	k4	30
ml	ml	kA	ml
vody	voda	k1gFnSc2	voda
ve	v	k7c6	v
stomililitrové	stomililitrový	k2eAgFnSc6d1	stomililitrový
sklenici	sklenice	k1gFnSc6	sklenice
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
fuzzy	fuzz	k1gInPc7	fuzz
množinami	množina	k1gFnPc7	množina
<g/>
:	:	kIx,	:
Plná	plný	k2eAgFnSc1d1	plná
a	a	k8xC	a
Prázdná	prázdný	k2eAgFnSc1d1	prázdná
<g/>
.	.	kIx.	.
</s>
<s>
Naše	náš	k3xOp1gFnSc1	náš
částečně	částečně	k6eAd1	částečně
naplněná	naplněný	k2eAgFnSc1d1	naplněná
sklenice	sklenice	k1gFnSc1	sklenice
pak	pak	k6eAd1	pak
přísluší	příslušet	k5eAaImIp3nS	příslušet
z	z	k7c2	z
0,7	[number]	k4	0,7
k	k	k7c3	k
Prázdné	prázdná	k1gFnSc3	prázdná
a	a	k8xC	a
z	z	k7c2	z
0,3	[number]	k4	0,3
k	k	k7c3	k
Plné	plný	k2eAgFnSc2d1	plná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Fuzzy	Fuzza	k1gFnSc2	Fuzza
logic	logice	k1gFnPc2	logice
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Teorie	teorie	k1gFnSc1	teorie
fuzzy	fuzza	k1gFnSc2	fuzza
množin	množina	k1gFnPc2	množina
Amplituda	amplituda	k1gFnSc1	amplituda
pravděpodobnosti	pravděpodobnost	k1gFnSc2	pravděpodobnost
Schrödingerova	Schrödingerův	k2eAgFnSc1d1	Schrödingerova
kočka	kočka	k1gFnSc1	kočka
Predikátová	predikátový	k2eAgFnSc1d1	predikátová
logika	logika	k1gFnSc1	logika
Petr	Petr	k1gMnSc1	Petr
Hájek	Hájek	k1gMnSc1	Hájek
(	(	kIx(	(
<g/>
matematik	matematik	k1gMnSc1	matematik
<g/>
)	)	kIx)	)
Vágnost	vágnost	k1gFnSc1	vágnost
Exaktní	exaktní	k2eAgFnSc1d1	exaktní
věda	věda	k1gFnSc1	věda
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Fuzzy	Fuzza	k1gFnSc2	Fuzza
logika	logicus	k1gMnSc2	logicus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
