<p>
<s>
Primera	primera	k1gFnSc1	primera
División	División	k1gInSc1	División
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
profesionální	profesionální	k2eAgFnSc1d1	profesionální
argentinská	argentinský	k2eAgFnSc1d1	Argentinská
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
soutěž	soutěž	k1gFnSc1	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Účastní	účastnit	k5eAaImIp3nS	účastnit
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
20	[number]	k4	20
klubů	klub	k1gInPc2	klub
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
bojují	bojovat	k5eAaImIp3nP	bojovat
o	o	k7c4	o
titul	titul	k1gInSc4	titul
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
částech	část	k1gFnPc6	část
zvaných	zvaný	k2eAgInPc2d1	zvaný
Apertura	apertura	k1gFnSc1	apertura
a	a	k8xC	a
Clausura	Clausura	k1gFnSc1	Clausura
(	(	kIx(	(
<g/>
každá	každý	k3xTgFnSc1	každý
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
titul	titul	k1gInSc4	titul
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	Z	kA	Z
Primera	primera	k1gFnSc1	primera
División	División	k1gInSc1	División
se	se	k3xPyFc4	se
sestupuje	sestupovat	k5eAaImIp3nS	sestupovat
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
argentinské	argentinský	k2eAgFnSc2d1	Argentinská
ligy	liga	k1gFnSc2	liga
Primera	primera	k1gFnSc1	primera
B	B	kA	B
Nacional	Nacional	k1gFnSc1	Nacional
<g/>
.	.	kIx.	.
</s>
<s>
Sezóna	sezóna	k1gFnSc1	sezóna
probíhá	probíhat	k5eAaImIp3nS	probíhat
od	od	k7c2	od
srpna	srpen	k1gInSc2	srpen
do	do	k7c2	do
května	květen	k1gInSc2	květen
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Primera	primera	k1gFnSc1	primera
División	División	k1gInSc4	División
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	s	k7c7	s
profesionální	profesionální	k2eAgFnSc7d1	profesionální
soutěží	soutěž	k1gFnSc7	soutěž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
18	[number]	k4	18
amatérských	amatérský	k2eAgInPc2d1	amatérský
klubů	klub	k1gInPc2	klub
zformovalo	zformovat	k5eAaPmAgNnS	zformovat
profesionální	profesionální	k2eAgFnSc4d1	profesionální
ligu	liga	k1gFnSc4	liga
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
prošla	projít	k5eAaPmAgFnS	projít
několika	několik	k4yIc7	několik
změnami	změna	k1gFnPc7	změna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současném	současný	k2eAgInSc6d1	současný
formátu	formát	k1gInSc6	formát
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
od	od	k7c2	od
sezóny	sezóna	k1gFnSc2	sezóna
1991	[number]	k4	1991
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
profesionální	profesionální	k2eAgFnSc2d1	profesionální
éry	éra	k1gFnSc2	éra
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
argentinským	argentinský	k2eAgInSc7d1	argentinský
šampionem	šampion	k1gMnSc7	šampion
celkem	celek	k1gInSc7	celek
16	[number]	k4	16
klubů	klub	k1gInPc2	klub
<g/>
,	,	kIx,	,
jen	jen	k9	jen
4	[number]	k4	4
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
byly	být	k5eAaImAgFnP	být
mistry	mistr	k1gMnPc4	mistr
desetkrát	desetkrát	k6eAd1	desetkrát
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Nejúspěšnějším	úspěšný	k2eAgInSc7d3	nejúspěšnější
klubem	klub	k1gInSc7	klub
je	být	k5eAaImIp3nS	být
River	River	k1gInSc1	River
Plate	plat	k1gInSc5	plat
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
celkem	celek	k1gInSc7	celek
36	[number]	k4	36
titulů	titul	k1gInPc2	titul
(	(	kIx(	(
<g/>
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Formát	formát	k1gInSc1	formát
==	==	k?	==
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
se	se	k3xPyFc4	se
liga	liga	k1gFnSc1	liga
hraje	hrát	k5eAaImIp3nS	hrát
novým	nový	k2eAgInSc7d1	nový
formátem	formát	k1gInSc7	formát
(	(	kIx(	(
<g/>
respektive	respektive	k9	respektive
od	od	k7c2	od
r.	r.	kA	r.
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
byl	být	k5eAaImAgInS	být
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
jen	jen	k9	jen
jeden	jeden	k4xCgMnSc1	jeden
vítěz	vítěz	k1gMnSc1	vítěz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
20	[number]	k4	20
argentinských	argentinský	k2eAgMnPc2d1	argentinský
klubů	klub	k1gInPc2	klub
hraje	hrát	k5eAaImIp3nS	hrát
Primera	primera	k1gFnSc1	primera
División	División	k1gInSc1	División
ve	v	k7c6	v
dvou	dva	k4xCgFnPc6	dva
částech	část	k1gFnPc6	část
<g/>
:	:	kIx,	:
Apertura	apertura	k1gFnSc1	apertura
probíhá	probíhat	k5eAaImIp3nS	probíhat
od	od	k7c2	od
srpna	srpen	k1gInSc2	srpen
do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
a	a	k8xC	a
Clausura	Clausura	k1gFnSc1	Clausura
v	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roku	rok	k1gInSc6	rok
od	od	k7c2	od
února	únor	k1gInSc2	únor
do	do	k7c2	do
června	červen	k1gInSc2	červen
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
části	část	k1gFnPc1	část
mají	mít	k5eAaImIp3nP	mít
identický	identický	k2eAgInSc4d1	identický
herní	herní	k2eAgInSc4d1	herní
systém	systém	k1gInSc4	systém
(	(	kIx(	(
<g/>
19	[number]	k4	19
kol	kolo	k1gNnPc2	kolo
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
všichni	všechen	k3xTgMnPc1	všechen
soupeři	soupeř	k1gMnPc1	soupeř
střetnou	střetnout	k5eAaPmIp3nP	střetnout
jedenkrát	jedenkrát	k6eAd1	jedenkrát
každý	každý	k3xTgMnSc1	každý
s	s	k7c7	s
každým	každý	k3xTgMnSc7	každý
<g/>
,	,	kIx,	,
10	[number]	k4	10
utkání	utkání	k1gNnPc1	utkání
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
kole	kolo	k1gNnSc6	kolo
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c4	v
každé	každý	k3xTgNnSc4	každý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
korunován	korunován	k2eAgMnSc1d1	korunován
národní	národní	k2eAgMnSc1d1	národní
šampion	šampion	k1gMnSc1	šampion
<g/>
.	.	kIx.	.
</s>
<s>
Pojmenování	pojmenování	k1gNnSc1	pojmenování
obou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
lze	lze	k6eAd1	lze
volně	volně	k6eAd1	volně
přeložit	přeložit	k5eAaPmF	přeložit
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
otevírací	otevírací	k2eAgFnSc4d1	otevírací
část	část	k1gFnSc4	část
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
uzavírací	uzavírací	k2eAgFnSc1d1	uzavírací
část	část	k1gFnSc1	část
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
aktuální	aktuální	k2eAgInSc1d1	aktuální
formát	formát	k1gInSc1	formát
soutěže	soutěž	k1gFnSc2	soutěž
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sestup	sestup	k1gInSc4	sestup
==	==	k?	==
</s>
</p>
<p>
<s>
Sestup	sestup	k1gInSc1	sestup
je	být	k5eAaImIp3nS	být
postaven	postavit	k5eAaPmNgInS	postavit
na	na	k7c6	na
průměrném	průměrný	k2eAgNnSc6d1	průměrné
umístění	umístění	k1gNnSc6	umístění
klubů	klub	k1gInPc2	klub
během	během	k7c2	během
tří	tři	k4xCgFnPc2	tři
sezón	sezóna	k1gFnPc2	sezóna
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
každé	každý	k3xTgFnSc2	každý
sezóny	sezóna	k1gFnSc2	sezóna
(	(	kIx(	(
<g/>
nikoli	nikoli	k9	nikoli
pouze	pouze	k6eAd1	pouze
části	část	k1gFnSc2	část
<g/>
)	)	kIx)	)
2	[number]	k4	2
kluby	klub	k1gInPc4	klub
s	s	k7c7	s
nejhorším	zlý	k2eAgInSc7d3	Nejhorší
průměrem	průměr	k1gInSc7	průměr
umístění	umístění	k1gNnSc2	umístění
za	za	k7c4	za
3	[number]	k4	3
roky	rok	k1gInPc7	rok
sestupují	sestupovat	k5eAaImIp3nP	sestupovat
do	do	k7c2	do
nižší	nízký	k2eAgFnSc2d2	nižší
soutěže	soutěž	k1gFnSc2	soutěž
Primera	primera	k1gFnSc1	primera
B	B	kA	B
Nacional	Nacional	k1gFnPc1	Nacional
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
naopak	naopak	k6eAd1	naopak
postupují	postupovat	k5eAaImIp3nP	postupovat
celky	celek	k1gInPc1	celek
z	z	k7c2	z
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Kluby	klub	k1gInPc1	klub
na	na	k7c4	na
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
tabulce	tabulka	k1gFnSc6	tabulka
s	s	k7c7	s
tříletým	tříletý	k2eAgNnSc7d1	tříleté
průměrným	průměrný	k2eAgNnSc7d1	průměrné
umístěním	umístění	k1gNnSc7	umístění
se	se	k3xPyFc4	se
utkávají	utkávat	k5eAaImIp3nP	utkávat
v	v	k7c6	v
baráži	baráž	k1gFnSc6	baráž
se	s	k7c7	s
třetím	třetí	k4xOgInSc7	třetí
a	a	k8xC	a
čtvrtým	čtvrtý	k4xOgInSc7	čtvrtý
celkem	celek	k1gInSc7	celek
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
ligy	liga	k1gFnSc2	liga
<g/>
.	.	kIx.	.
</s>
<s>
Důsledkem	důsledek	k1gInSc7	důsledek
tohoto	tento	k3xDgInSc2	tento
systému	systém	k1gInSc2	systém
postupují	postupovat	k5eAaImIp3nP	postupovat
do	do	k7c2	do
první	první	k4xOgFnSc2	první
ligy	liga	k1gFnSc2	liga
vždy	vždy	k6eAd1	vždy
2	[number]	k4	2
až	až	k9	až
4	[number]	k4	4
celky	celek	k1gInPc4	celek
<g/>
.	.	kIx.	.
</s>
<s>
Nově	nově	k6eAd1	nově
postupujícím	postupující	k2eAgInPc3d1	postupující
týmům	tým	k1gInPc3	tým
se	se	k3xPyFc4	se
průměrují	průměrovat	k5eAaPmIp3nP	průměrovat
pouze	pouze	k6eAd1	pouze
umístění	umístění	k1gNnSc4	umístění
v	v	k7c6	v
sezónách	sezóna	k1gFnPc6	sezóna
Primery	primera	k1gFnSc2	primera
División	División	k1gInSc1	División
od	od	k7c2	od
jejich	jejich	k3xOp3gInSc2	jejich
postupu	postup	k1gInSc2	postup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
postupu	postup	k1gInSc2	postup
/	/	kIx~	/
sestupu	sestup	k1gInSc2	sestup
byl	být	k5eAaImAgMnS	být
ustanoven	ustanoven	k2eAgMnSc1d1	ustanoven
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Amatérská	amatérský	k2eAgFnSc1d1	amatérská
éra	éra	k1gFnSc1	éra
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
–	–	k?	–
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Argentina	Argentina	k1gFnSc1	Argentina
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
první	první	k4xOgFnSc2	první
zemí	zem	k1gFnPc2	zem
mimo	mimo	k7c4	mimo
Velkou	velký	k2eAgFnSc4d1	velká
Británii	Británie	k1gFnSc4	Británie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zformovala	zformovat	k5eAaPmAgFnS	zformovat
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
liga	liga	k1gFnSc1	liga
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
éry	éra	k1gFnSc2	éra
byl	být	k5eAaImAgInS	být
nejúspěšnějším	úspěšný	k2eAgInSc7d3	nejúspěšnější
klubem	klub	k1gInSc7	klub
Alumni	Alumni	k1gFnPc2	Alumni
Athletic	Athletice	k1gFnPc2	Athletice
Club	club	k1gInSc4	club
s	s	k7c7	s
deseti	deset	k4xCc7	deset
tituly	titul	k1gInPc7	titul
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgMnSc1	jeden
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
English	English	k1gInSc1	English
High	Higha	k1gFnPc2	Higha
School	Schoola	k1gFnPc2	Schoola
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Profesionální	profesionální	k2eAgFnSc1d1	profesionální
éra	éra	k1gFnSc1	éra
<g/>
:	:	kIx,	:
Systém	systém	k1gInSc1	systém
každý	každý	k3xTgInSc1	každý
s	s	k7c7	s
každým	každý	k3xTgInSc7	každý
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
–	–	k?	–
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Profesionalizace	profesionalizace	k1gFnSc1	profesionalizace
klubů	klub	k1gInPc2	klub
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
raných	raný	k2eAgNnPc6d1	rané
letech	léto	k1gNnPc6	léto
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
národní	národní	k2eAgFnSc6d1	národní
asociaci	asociace	k1gFnSc6	asociace
sdruženy	sdružit	k5eAaPmNgInP	sdružit
pouze	pouze	k6eAd1	pouze
kluby	klub	k1gInPc1	klub
z	z	k7c2	z
Buenos	Buenosa	k1gFnPc2	Buenosa
Aires	Airesa	k1gFnPc2	Airesa
<g/>
,	,	kIx,	,
z	z	k7c2	z
aglomerace	aglomerace	k1gFnSc2	aglomerace
Gran	Gran	k1gMnSc1	Gran
Buenos	Buenos	k1gMnSc1	Buenos
Aires	Aires	k1gMnSc1	Aires
a	a	k8xC	a
z	z	k7c2	z
La	la	k1gNnSc2	la
Plata	plato	k1gNnSc2	plato
<g/>
.	.	kIx.	.
</s>
<s>
Týmy	tým	k1gInPc1	tým
z	z	k7c2	z
Rosaria	rosarium	k1gNnSc2	rosarium
a	a	k8xC	a
argentinského	argentinský	k2eAgInSc2d1	argentinský
Santa	Sant	k1gInSc2	Sant
Fe	Fe	k1gMnPc2	Fe
se	se	k3xPyFc4	se
připojily	připojit	k5eAaPmAgInP	připojit
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
probíhala	probíhat	k5eAaImAgFnS	probíhat
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
dvoukolová	dvoukolový	k2eAgFnSc1d1	dvoukolová
soutěž	soutěž	k1gFnSc1	soutěž
každý	každý	k3xTgMnSc1	každý
s	s	k7c7	s
každým	každý	k3xTgNnSc7	každý
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
klub	klub	k1gInSc1	klub
s	s	k7c7	s
nejvíce	hodně	k6eAd3	hodně
body	bod	k1gInPc7	bod
získal	získat	k5eAaPmAgInS	získat
titul	titul	k1gInSc1	titul
<g/>
.	.	kIx.	.
</s>
<s>
Výjimkou	výjimka	k1gFnSc7	výjimka
byl	být	k5eAaImAgInS	být
rok	rok	k1gInSc1	rok
1936	[number]	k4	1936
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
ve	v	k7c6	v
finálovém	finálový	k2eAgNnSc6d1	finálové
utkání	utkání	k1gNnSc6	utkání
střetly	střetnout	k5eAaPmAgFnP	střetnout
vítěz	vítěz	k1gMnSc1	vítěz
Copa	Copum	k1gNnSc2	Copum
de	de	k?	de
Honor	Honor	k1gMnSc1	Honor
s	s	k7c7	s
vítězem	vítěz	k1gMnSc7	vítěz
Campeonato	Campeonat	k2eAgNnSc4d1	Campeonato
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
dominovaly	dominovat	k5eAaImAgInP	dominovat
tradiční	tradiční	k2eAgInPc1d1	tradiční
týmy	tým	k1gInPc1	tým
<g/>
:	:	kIx,	:
River	River	k1gInSc1	River
Plate	plat	k1gInSc5	plat
<g/>
,	,	kIx,	,
Boca	Bocum	k1gNnSc2	Bocum
Juniors	Juniorsa	k1gFnPc2	Juniorsa
<g/>
,	,	kIx,	,
Independiente	Independient	k1gMnSc5	Independient
<g/>
,	,	kIx,	,
Racing	Racing	k1gInSc1	Racing
Club	club	k1gInSc1	club
a	a	k8xC	a
San	San	k1gFnSc1	San
Lorenzo	Lorenza	k1gFnSc5	Lorenza
<g/>
.	.	kIx.	.
</s>
<s>
Žádný	žádný	k3yNgInSc1	žádný
jiný	jiný	k2eAgInSc1d1	jiný
tým	tým	k1gInSc1	tým
mimo	mimo	k7c4	mimo
tyto	tento	k3xDgInPc4	tento
zmíněné	zmíněný	k2eAgInPc4d1	zmíněný
kluby	klub	k1gInPc4	klub
během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
éry	éra	k1gFnSc2	éra
titul	titul	k1gInSc1	titul
nezískal	získat	k5eNaPmAgMnS	získat
<g/>
.	.	kIx.	.
</s>
<s>
Nejblíže	blízce	k6eAd3	blízce
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
klub	klub	k1gInSc4	klub
CA	ca	kA	ca
Banfield	Banfield	k1gInSc1	Banfield
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
<g/>
,	,	kIx,	,
když	když	k8xS	když
získal	získat	k5eAaPmAgMnS	získat
stejný	stejný	k2eAgInSc4d1	stejný
počet	počet	k1gInSc4	počet
bodů	bod	k1gInPc2	bod
jako	jako	k8xS	jako
Racing	Racing	k1gInSc1	Racing
Club	club	k1gInSc1	club
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
dvojzápasovém	dvojzápasový	k2eAgInSc6d1	dvojzápasový
play-off	playff	k1gInSc4	play-off
o	o	k7c4	o
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
však	však	k9	však
Banfield	Banfield	k1gMnSc1	Banfield
Racingu	Racing	k1gInSc2	Racing
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Profesionální	profesionální	k2eAgFnSc1d1	profesionální
éra	éra	k1gFnSc1	éra
<g/>
:	:	kIx,	:
Metropolitano	Metropolitana	k1gFnSc5	Metropolitana
a	a	k8xC	a
Nacional	Nacional	k1gMnSc3	Nacional
(	(	kIx(	(
<g/>
1967	[number]	k4	1967
–	–	k?	–
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
byl	být	k5eAaImAgInS	být
systém	systém	k1gInSc1	systém
nahrazen	nahradit	k5eAaPmNgInS	nahradit
dvěma	dva	k4xCgInPc7	dva
šampionáty	šampionát	k1gInPc7	šampionát
<g/>
:	:	kIx,	:
Metropolitano	Metropolitana	k1gFnSc5	Metropolitana
a	a	k8xC	a
Nacional	Nacional	k1gFnPc6	Nacional
<g/>
.	.	kIx.	.
</s>
<s>
Metropopolitana	Metropopolitana	k1gFnSc1	Metropopolitana
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgInP	moct
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
pouze	pouze	k6eAd1	pouze
kluby	klub	k1gInPc1	klub
z	z	k7c2	z
předešlé	předešlý	k2eAgFnSc2d1	předešlá
éry	éra	k1gFnSc2	éra
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
Nacional	Nacional	k1gFnSc4	Nacional
byl	být	k5eAaImAgInS	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
i	i	k8xC	i
týmům	tým	k1gInPc3	tým
z	z	k7c2	z
regionálních	regionální	k2eAgFnPc2d1	regionální
soutěží	soutěž	k1gFnPc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
změna	změna	k1gFnSc1	změna
umožnila	umožnit	k5eAaPmAgFnS	umožnit
menším	malý	k2eAgInPc3d2	menší
celkům	celek	k1gInPc3	celek
jako	jako	k8xS	jako
Estudiantes	Estudiantes	k1gMnSc1	Estudiantes
<g/>
,	,	kIx,	,
Vélez	Vélez	k1gMnSc1	Vélez
Sársfield	Sársfield	k1gMnSc1	Sársfield
<g/>
,	,	kIx,	,
Chacarita	Chacarita	k1gFnSc1	Chacarita
Juniors	Juniorsa	k1gFnPc2	Juniorsa
a	a	k8xC	a
dalším	další	k2eAgNnSc7d1	další
prolomit	prolomit	k5eAaPmF	prolomit
hegemonii	hegemonie	k1gFnSc3	hegemonie
pětice	pětice	k1gFnSc2	pětice
silných	silný	k2eAgInPc2d1	silný
klubů	klub	k1gInPc2	klub
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
doposud	doposud	k6eAd1	doposud
vládly	vládnout	k5eAaImAgInP	vládnout
lize	liga	k1gFnSc3	liga
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Profesionální	profesionální	k2eAgFnSc1d1	profesionální
éra	éra	k1gFnSc1	éra
<g/>
:	:	kIx,	:
Evropský	evropský	k2eAgInSc1d1	evropský
systém	systém	k1gInSc1	systém
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
–	–	k?	–
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
změnila	změnit	k5eAaPmAgFnS	změnit
struktura	struktura	k1gFnSc1	struktura
ligy	liga	k1gFnSc2	liga
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
evropský	evropský	k2eAgInSc1d1	evropský
model	model	k1gInSc1	model
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
dvou	dva	k4xCgInPc2	dva
šampionátů	šampionát	k1gInPc2	šampionát
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
ustanovila	ustanovit	k5eAaPmAgFnS	ustanovit
soutěž	soutěž	k1gFnSc1	soutěž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
hrál	hrát	k5eAaImAgMnS	hrát
dvakrát	dvakrát	k6eAd1	dvakrát
každý	každý	k3xTgMnSc1	každý
s	s	k7c7	s
každým	každý	k3xTgMnSc7	každý
a	a	k8xC	a
celek	celek	k1gInSc4	celek
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
získal	získat	k5eAaPmAgInS	získat
nejvíc	hodně	k6eAd3	hodně
bodů	bod	k1gInPc2	bod
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vítězem	vítěz	k1gMnSc7	vítěz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1988	[number]	k4	1988
<g/>
/	/	kIx~	/
<g/>
1989	[number]	k4	1989
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
udělovat	udělovat	k5eAaImF	udělovat
za	za	k7c4	za
vítězství	vítězství	k1gNnSc4	vítězství
3	[number]	k4	3
body	bod	k1gInPc7	bod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
remízy	remíza	k1gFnSc2	remíza
se	se	k3xPyFc4	se
kopaly	kopat	k5eAaImAgInP	kopat
pokutové	pokutový	k2eAgInPc1d1	pokutový
kopy	kop	k1gInPc1	kop
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc4	jejichž
vítěz	vítěz	k1gMnSc1	vítěz
obdržel	obdržet	k5eAaPmAgMnS	obdržet
2	[number]	k4	2
body	bod	k1gInPc4	bod
a	a	k8xC	a
poražený	poražený	k2eAgInSc4d1	poražený
jeden	jeden	k4xCgInSc4	jeden
bod	bod	k1gInSc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
systém	systém	k1gInSc1	systém
se	se	k3xPyFc4	se
však	však	k9	však
neuchytil	uchytit	k5eNaPmAgInS	uchytit
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
opuštěn	opustit	k5eAaPmNgInS	opustit
hned	hned	k6eAd1	hned
v	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
sezóně	sezóna	k1gFnSc6	sezóna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Profesionální	profesionální	k2eAgFnSc1d1	profesionální
éra	éra	k1gFnSc1	éra
<g/>
:	:	kIx,	:
Apertura	apertura	k1gFnSc1	apertura
a	a	k8xC	a
Clausura	Clausura	k1gFnSc1	Clausura
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
–	–	k?	–
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
O	o	k7c4	o
pět	pět	k4xCc4	pět
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
soutěž	soutěž	k1gFnSc1	soutěž
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgInPc2	dva
turnajů	turnaj	k1gInPc2	turnaj
–	–	k?	–
Apertury	apertura	k1gFnPc4	apertura
a	a	k8xC	a
Clausury	Clausur	k1gInPc4	Clausur
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
kapitola	kapitola	k1gFnSc1	kapitola
Formát	formát	k1gInSc1	formát
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
hráli	hrát	k5eAaImAgMnP	hrát
vítězové	vítěz	k1gMnPc1	vítěz
Apertury	apertura	k1gFnSc2	apertura
a	a	k8xC	a
Clausury	Clausur	k1gInPc4	Clausur
vzájemný	vzájemný	k2eAgInSc4d1	vzájemný
dvouzápas	dvouzápas	k1gInSc4	dvouzápas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
prohrál	prohrát	k5eAaPmAgMnS	prohrát
vítěz	vítěz	k1gMnSc1	vítěz
Clausury	Clausura	k1gFnSc2	Clausura
a	a	k8xC	a
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
klubů	klub	k1gInPc2	klub
Boca	Boca	k1gFnSc1	Boca
Juniors	Juniors	k1gInSc1	Juniors
s	s	k7c7	s
vítězem	vítěz	k1gMnSc7	vítěz
Apertury	apertura	k1gFnSc2	apertura
Newell	Newella	k1gFnPc2	Newella
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Old	Olda	k1gFnPc2	Olda
Boys	boy	k1gMnPc3	boy
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
během	během	k7c2	během
Clausury	Clausura	k1gFnSc2	Clausura
Boca	Bocum	k1gNnSc2	Bocum
Juniors	Juniorsa	k1gFnPc2	Juniorsa
ani	ani	k8xC	ani
jednou	jeden	k4xCgFnSc7	jeden
neprohrál	prohrát	k5eNaPmAgMnS	prohrát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
se	se	k3xPyFc4	se
zápas	zápas	k1gInSc1	zápas
mezi	mezi	k7c7	mezi
vítězi	vítěz	k1gMnPc7	vítěz
obou	dva	k4xCgInPc6	dva
částí	část	k1gFnPc2	část
také	také	k6eAd1	také
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
oba	dva	k4xCgMnPc1	dva
soupeři	soupeř	k1gMnPc1	soupeř
byli	být	k5eAaImAgMnP	být
uznáni	uznat	k5eAaPmNgMnP	uznat
jako	jako	k9	jako
mistři	mistr	k1gMnPc1	mistr
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
se	se	k3xPyFc4	se
zápas	zápas	k1gInSc1	zápas
mezi	mezi	k7c7	mezi
vítězem	vítěz	k1gMnSc7	vítěz
Apertury	apertura	k1gFnSc2	apertura
a	a	k8xC	a
vítězem	vítěz	k1gMnSc7	vítěz
Clausury	Clausura	k1gFnSc2	Clausura
nehraje	hrát	k5eNaImIp3nS	hrát
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
získávají	získávat	k5eAaImIp3nP	získávat
automaticky	automaticky	k6eAd1	automaticky
titul	titul	k1gInSc4	titul
v	v	k7c6	v
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
opanovali	opanovat	k5eAaPmAgMnP	opanovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
1995	[number]	k4	1995
<g/>
/	/	kIx~	/
<g/>
1996	[number]	k4	1996
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgNnP	změnit
pravidla	pravidlo	k1gNnPc1	pravidlo
<g/>
,	,	kIx,	,
za	za	k7c2	za
vítězství	vítězství	k1gNnSc2	vítězství
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
udělovat	udělovat	k5eAaImF	udělovat
3	[number]	k4	3
body	bod	k1gInPc4	bod
<g/>
,	,	kIx,	,
za	za	k7c4	za
remízu	remíza	k1gFnSc4	remíza
jeden	jeden	k4xCgInSc4	jeden
bod	bod	k1gInSc4	bod
a	a	k8xC	a
za	za	k7c4	za
prohru	prohra	k1gFnSc4	prohra
žádný	žádný	k3yNgMnSc1	žádný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Profesionální	profesionální	k2eAgFnSc1d1	profesionální
éra	éra	k1gFnSc1	éra
<g/>
:	:	kIx,	:
Torneo	Torneo	k1gMnSc1	Torneo
Inicial	Inicial	k1gMnSc1	Inicial
a	a	k8xC	a
Torneo	Torneo	k1gMnSc1	Torneo
Final	Final	k1gMnSc1	Final
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
–	–	k?	–
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
ročník	ročník	k1gInSc4	ročník
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
se	se	k3xPyFc4	se
Apertura	apertura	k1gFnSc1	apertura
přejmenovala	přejmenovat	k5eAaPmAgFnS	přejmenovat
na	na	k7c4	na
Torneo	Torneo	k1gNnSc4	Torneo
Inicial	Inicial	k1gInSc1	Inicial
a	a	k8xC	a
Clausura	Clausura	k1gFnSc1	Clausura
na	na	k7c4	na
Torneo	Torneo	k1gNnSc4	Torneo
Final	Final	k1gInSc4	Final
<g/>
.	.	kIx.	.
</s>
<s>
Systém	systém	k1gInSc1	systém
zůstal	zůstat	k5eAaPmAgInS	zůstat
téměř	téměř	k6eAd1	téměř
stejný	stejný	k2eAgInSc1d1	stejný
s	s	k7c7	s
tím	ten	k3xDgInSc7	ten
rozdílem	rozdíl	k1gInSc7	rozdíl
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
je	být	k5eAaImIp3nS	být
korunován	korunovat	k5eAaBmNgMnS	korunovat
pouze	pouze	k6eAd1	pouze
jeden	jeden	k4xCgMnSc1	jeden
vítěz	vítěz	k1gMnSc1	vítěz
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vzejde	vzejít	k5eAaPmIp3nS	vzejít
ze	z	k7c2	z
zápasu	zápas	k1gInSc2	zápas
mezi	mezi	k7c7	mezi
vítězem	vítěz	k1gMnSc7	vítěz
Torneo	Torneo	k1gMnSc1	Torneo
Inicial	Inicial	k1gMnSc1	Inicial
a	a	k8xC	a
Torneo	Torneo	k1gMnSc1	Torneo
Final	Final	k1gMnSc1	Final
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Mistrovské	mistrovský	k2eAgInPc1d1	mistrovský
tituly	titul	k1gInPc1	titul
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
1931	[number]	k4	1931
–	–	k?	–
1966	[number]	k4	1966
===	===	k?	===
</s>
</p>
<p>
<s>
1931	[number]	k4	1931
Boca	Boc	k1gInSc2	Boc
Juniors	Juniors	k1gInSc1	Juniors
</s>
</p>
<p>
<s>
1932	[number]	k4	1932
River	River	k1gInSc1	River
Plate	plat	k1gInSc5	plat
</s>
</p>
<p>
<s>
1933	[number]	k4	1933
San	San	k1gFnSc1	San
Lorenzo	Lorenza	k1gFnSc5	Lorenza
</s>
</p>
<p>
<s>
1934	[number]	k4	1934
Boca	Boc	k1gInSc2	Boc
Juniors	Juniors	k1gInSc1	Juniors
</s>
</p>
<p>
<s>
1935	[number]	k4	1935
Boca	Boc	k1gInSc2	Boc
Juniors	Juniors	k1gInSc1	Juniors
</s>
</p>
<p>
<s>
1936	[number]	k4	1936
River	River	k1gInSc1	River
Plate	plat	k1gInSc5	plat
</s>
</p>
<p>
<s>
1937	[number]	k4	1937
River	River	k1gInSc1	River
Plate	plat	k1gInSc5	plat
</s>
</p>
<p>
<s>
1938	[number]	k4	1938
Independiente	Independient	k1gInSc5	Independient
</s>
</p>
<p>
<s>
1939	[number]	k4	1939
Independiente	Independient	k1gMnSc5	Independient
</s>
</p>
<p>
<s>
1940	[number]	k4	1940
Boca	Boc	k1gInSc2	Boc
Juniors	Juniors	k1gInSc1	Juniors
</s>
</p>
<p>
<s>
1941	[number]	k4	1941
River	River	k1gInSc1	River
Plate	plat	k1gInSc5	plat
</s>
</p>
<p>
<s>
1942	[number]	k4	1942
River	River	k1gInSc1	River
Plate	plat	k1gInSc5	plat
</s>
</p>
<p>
<s>
1943	[number]	k4	1943
Boca	Boc	k1gInSc2	Boc
Juniors	Juniors	k1gInSc1	Juniors
</s>
</p>
<p>
<s>
1944	[number]	k4	1944
Boca	Boc	k1gInSc2	Boc
Juniors	Juniors	k1gInSc1	Juniors
</s>
</p>
<p>
<s>
1945	[number]	k4	1945
River	River	k1gInSc1	River
Plate	plat	k1gInSc5	plat
</s>
</p>
<p>
<s>
1946	[number]	k4	1946
San	San	k1gFnSc1	San
Lorenzo	Lorenza	k1gFnSc5	Lorenza
</s>
</p>
<p>
<s>
1947	[number]	k4	1947
River	River	k1gInSc1	River
Plate	plat	k1gInSc5	plat
</s>
</p>
<p>
<s>
1948	[number]	k4	1948
Independiente	Independient	k1gInSc5	Independient
</s>
</p>
<p>
<s>
1949	[number]	k4	1949
Racing	Racing	k1gInSc1	Racing
Club	club	k1gInSc4	club
de	de	k?	de
Avellaneda	Avellaned	k1gMnSc2	Avellaned
</s>
</p>
<p>
<s>
1950	[number]	k4	1950
Racing	Racing	k1gInSc1	Racing
Club	club	k1gInSc4	club
de	de	k?	de
Avellaneda	Avellaned	k1gMnSc2	Avellaned
</s>
</p>
<p>
<s>
1951	[number]	k4	1951
Racing	Racing	k1gInSc1	Racing
Club	club	k1gInSc4	club
de	de	k?	de
Avellaneda	Avellaned	k1gMnSc2	Avellaned
</s>
</p>
<p>
<s>
1952	[number]	k4	1952
River	River	k1gInSc1	River
Plate	plat	k1gInSc5	plat
</s>
</p>
<p>
<s>
1953	[number]	k4	1953
River	River	k1gInSc1	River
Plate	plat	k1gInSc5	plat
</s>
</p>
<p>
<s>
1954	[number]	k4	1954
Boca	Boc	k1gInSc2	Boc
Juniors	Juniors	k1gInSc1	Juniors
</s>
</p>
<p>
<s>
1955	[number]	k4	1955
River	River	k1gInSc1	River
Plate	plat	k1gInSc5	plat
</s>
</p>
<p>
<s>
1956	[number]	k4	1956
River	River	k1gInSc1	River
Plate	plat	k1gInSc5	plat
</s>
</p>
<p>
<s>
1957	[number]	k4	1957
River	River	k1gInSc1	River
Plate	plat	k1gInSc5	plat
</s>
</p>
<p>
<s>
1958	[number]	k4	1958
Racing	Racing	k1gInSc1	Racing
Club	club	k1gInSc4	club
de	de	k?	de
Avellaneda	Avellaned	k1gMnSc2	Avellaned
</s>
</p>
<p>
<s>
1959	[number]	k4	1959
San	San	k1gFnSc1	San
Lorenzo	Lorenza	k1gFnSc5	Lorenza
</s>
</p>
<p>
<s>
1960	[number]	k4	1960
Independiente	Independient	k1gMnSc5	Independient
</s>
</p>
<p>
<s>
1961	[number]	k4	1961
Racing	Racing	k1gInSc1	Racing
Club	club	k1gInSc4	club
de	de	k?	de
Avellaneda	Avellaned	k1gMnSc2	Avellaned
</s>
</p>
<p>
<s>
1962	[number]	k4	1962
Boca	Boc	k1gInSc2	Boc
Juniors	Juniors	k1gInSc1	Juniors
</s>
</p>
<p>
<s>
1963	[number]	k4	1963
Independiente	Independient	k1gInSc5	Independient
</s>
</p>
<p>
<s>
1964	[number]	k4	1964
Boca	Boc	k1gInSc2	Boc
Juniors	Juniors	k1gInSc1	Juniors
</s>
</p>
<p>
<s>
1965	[number]	k4	1965
Boca	Boc	k1gInSc2	Boc
Juniors	Juniors	k1gInSc1	Juniors
</s>
</p>
<p>
<s>
1966	[number]	k4	1966
Racing	Racing	k1gInSc1	Racing
Club	club	k1gInSc4	club
de	de	k?	de
Avellaneda	Avellaned	k1gMnSc2	Avellaned
</s>
</p>
<p>
<s>
===	===	k?	===
Metropolitano	Metropolitana	k1gFnSc5	Metropolitana
a	a	k8xC	a
Nacional	Nacional	k1gFnSc6	Nacional
1967	[number]	k4	1967
–	–	k?	–
1985	[number]	k4	1985
===	===	k?	===
</s>
</p>
<p>
<s>
1967	[number]	k4	1967
Metropolitano	Metropolitana	k1gFnSc5	Metropolitana
<g/>
:	:	kIx,	:
Estudiantes	Estudiantesa	k1gFnPc2	Estudiantesa
de	de	k?	de
La	la	k1gNnSc1	la
Plata	plato	k1gNnSc2	plato
</s>
</p>
<p>
<s>
1967	[number]	k4	1967
Nacional	Nacional	k1gFnSc1	Nacional
<g/>
:	:	kIx,	:
Independiente	Independient	k1gMnSc5	Independient
</s>
</p>
<p>
<s>
1968	[number]	k4	1968
Metropolitano	Metropolitana	k1gFnSc5	Metropolitana
<g/>
:	:	kIx,	:
San	San	k1gMnSc3	San
Lorenzo	Lorenza	k1gFnSc5	Lorenza
</s>
</p>
<p>
<s>
1968	[number]	k4	1968
Nacional	Nacional	k1gMnSc1	Nacional
<g/>
:	:	kIx,	:
Vélez	Vélez	k1gMnSc1	Vélez
Sársfield	Sársfield	k1gMnSc1	Sársfield
</s>
</p>
<p>
<s>
1969	[number]	k4	1969
Metropolitano	Metropolitana	k1gFnSc5	Metropolitana
<g/>
:	:	kIx,	:
Chacarita	Chacarita	k1gFnSc1	Chacarita
Juniors	Juniorsa	k1gFnPc2	Juniorsa
</s>
</p>
<p>
<s>
1969	[number]	k4	1969
Nacional	Nacional	k1gFnSc1	Nacional
<g/>
:	:	kIx,	:
Boca	Boca	k1gFnSc1	Boca
Juniors	Juniorsa	k1gFnPc2	Juniorsa
</s>
</p>
<p>
<s>
1970	[number]	k4	1970
Metropolitano	Metropolitana	k1gFnSc5	Metropolitana
<g/>
:	:	kIx,	:
Independiente	Independient	k1gMnSc5	Independient
</s>
</p>
<p>
<s>
1970	[number]	k4	1970
Nacional	Nacional	k1gFnSc1	Nacional
<g/>
:	:	kIx,	:
Boca	Boca	k1gFnSc1	Boca
Juniors	Juniorsa	k1gFnPc2	Juniorsa
</s>
</p>
<p>
<s>
1971	[number]	k4	1971
Metropolitano	Metropolitana	k1gFnSc5	Metropolitana
<g/>
:	:	kIx,	:
Independiente	Independient	k1gInSc5	Independient
</s>
</p>
<p>
<s>
1971	[number]	k4	1971
Nacional	Nacional	k1gMnSc1	Nacional
<g/>
:	:	kIx,	:
Rosario	Rosario	k1gMnSc1	Rosario
Central	Central	k1gMnSc1	Central
</s>
</p>
<p>
<s>
1972	[number]	k4	1972
Metropolitano	Metropolitana	k1gFnSc5	Metropolitana
<g/>
:	:	kIx,	:
San	San	k1gMnPc6	San
Lorenzo	Lorenza	k1gFnSc5	Lorenza
</s>
</p>
<p>
<s>
1972	[number]	k4	1972
Nacional	Nacional	k1gFnSc1	Nacional
<g/>
:	:	kIx,	:
San	San	k1gFnSc1	San
Lorenzo	Lorenza	k1gFnSc5	Lorenza
</s>
</p>
<p>
<s>
1973	[number]	k4	1973
Metropolitano	Metropolitana	k1gFnSc5	Metropolitana
<g/>
:	:	kIx,	:
CA	ca	kA	ca
Huracán	Huracán	k2eAgInSc1d1	Huracán
</s>
</p>
<p>
<s>
1973	[number]	k4	1973
Nacional	Nacional	k1gMnSc1	Nacional
<g/>
:	:	kIx,	:
Rosario	Rosario	k1gMnSc1	Rosario
Central	Central	k1gMnSc1	Central
</s>
</p>
<p>
<s>
1974	[number]	k4	1974
Metropolitano	Metropolitana	k1gFnSc5	Metropolitana
<g/>
:	:	kIx,	:
Newell	Newellum	k1gNnPc2	Newellum
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Old	Olda	k1gFnPc2	Olda
Boys	boy	k1gMnPc6	boy
</s>
</p>
<p>
<s>
1974	[number]	k4	1974
Nacional	Nacional	k1gFnSc1	Nacional
<g/>
:	:	kIx,	:
San	San	k1gFnSc1	San
Lorenzo	Lorenza	k1gFnSc5	Lorenza
</s>
</p>
<p>
<s>
1975	[number]	k4	1975
Metropolitano	Metropolitana	k1gFnSc5	Metropolitana
<g/>
:	:	kIx,	:
River	Rivra	k1gFnPc2	Rivra
Plate	plat	k1gInSc5	plat
</s>
</p>
<p>
<s>
1975	[number]	k4	1975
Nacional	Nacional	k1gFnSc1	Nacional
<g/>
:	:	kIx,	:
River	River	k1gInSc1	River
Plate	plat	k1gInSc5	plat
</s>
</p>
<p>
<s>
1976	[number]	k4	1976
Metropolitano	Metropolitana	k1gFnSc5	Metropolitana
<g/>
:	:	kIx,	:
Boca	Bocum	k1gNnPc1	Bocum
Juniors	Juniorsa	k1gFnPc2	Juniorsa
</s>
</p>
<p>
<s>
1976	[number]	k4	1976
Nacional	Nacional	k1gFnSc1	Nacional
<g/>
:	:	kIx,	:
Boca	Boca	k1gFnSc1	Boca
Juniors	Juniorsa	k1gFnPc2	Juniorsa
</s>
</p>
<p>
<s>
1977	[number]	k4	1977
Metropolitano	Metropolitana	k1gFnSc5	Metropolitana
<g/>
:	:	kIx,	:
River	River	k1gInSc4	River
Plate	plat	k1gInSc5	plat
</s>
</p>
<p>
<s>
1977	[number]	k4	1977
Nacional	Nacional	k1gFnSc1	Nacional
<g/>
:	:	kIx,	:
Independiente	Independient	k1gMnSc5	Independient
</s>
</p>
<p>
<s>
1978	[number]	k4	1978
Metropolitano	Metropolitana	k1gFnSc5	Metropolitana
<g/>
:	:	kIx,	:
Quilmes	Quilmes	k1gInSc4	Quilmes
AC	AC	kA	AC
</s>
</p>
<p>
<s>
1978	[number]	k4	1978
Nacional	Nacional	k1gFnSc1	Nacional
<g/>
:	:	kIx,	:
Independiente	Independient	k1gMnSc5	Independient
</s>
</p>
<p>
<s>
1979	[number]	k4	1979
Metropolitano	Metropolitana	k1gFnSc5	Metropolitana
<g/>
:	:	kIx,	:
River	Rivero	k1gNnPc2	Rivero
Plate	plat	k1gInSc5	plat
</s>
</p>
<p>
<s>
1979	[number]	k4	1979
Nacional	Nacional	k1gFnSc1	Nacional
<g/>
:	:	kIx,	:
River	River	k1gInSc1	River
Plate	plat	k1gInSc5	plat
</s>
</p>
<p>
<s>
1980	[number]	k4	1980
Metropolitano	Metropolitana	k1gFnSc5	Metropolitana
<g/>
:	:	kIx,	:
River	Rivra	k1gFnPc2	Rivra
Plate	plat	k1gInSc5	plat
</s>
</p>
<p>
<s>
1980	[number]	k4	1980
Nacional	Nacional	k1gMnSc1	Nacional
<g/>
:	:	kIx,	:
Rosario	Rosario	k1gMnSc1	Rosario
Central	Central	k1gMnSc1	Central
</s>
</p>
<p>
<s>
1981	[number]	k4	1981
Metropolitano	Metropolitana	k1gFnSc5	Metropolitana
<g/>
:	:	kIx,	:
Boca	Bocum	k1gNnPc1	Bocum
Juniors	Juniorsa	k1gFnPc2	Juniorsa
</s>
</p>
<p>
<s>
1981	[number]	k4	1981
Nacional	Nacional	k1gFnSc1	Nacional
<g/>
:	:	kIx,	:
River	River	k1gInSc1	River
Plate	plat	k1gInSc5	plat
</s>
</p>
<p>
<s>
1982	[number]	k4	1982
Metropolitano	Metropolitana	k1gFnSc5	Metropolitana
<g/>
:	:	kIx,	:
Ferro	Ferro	k1gNnPc1	Ferro
Carril	Carrila	k1gFnPc2	Carrila
Oeste	Oest	k1gMnSc5	Oest
</s>
</p>
<p>
<s>
1982	[number]	k4	1982
Nacional	Nacional	k1gFnSc1	Nacional
<g/>
:	:	kIx,	:
Estudiantes	Estudiantes	k1gInSc1	Estudiantes
de	de	k?	de
La	la	k1gNnSc1	la
Plata	plato	k1gNnSc2	plato
</s>
</p>
<p>
<s>
1983	[number]	k4	1983
Metropolitano	Metropolitana	k1gFnSc5	Metropolitana
<g/>
:	:	kIx,	:
Independiente	Independient	k1gMnSc5	Independient
</s>
</p>
<p>
<s>
1983	[number]	k4	1983
Nacional	Nacional	k1gFnSc1	Nacional
<g/>
:	:	kIx,	:
Estudiantes	Estudiantes	k1gInSc1	Estudiantes
de	de	k?	de
La	la	k1gNnSc1	la
Plata	plato	k1gNnSc2	plato
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
Metropolitano	Metropolitana	k1gFnSc5	Metropolitana
<g/>
:	:	kIx,	:
Argentinos	Argentinos	k1gInSc4	Argentinos
Juniors	Juniorsa	k1gFnPc2	Juniorsa
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
Nacional	Nacional	k1gFnSc1	Nacional
<g/>
:	:	kIx,	:
Ferro	Ferro	k1gNnSc1	Ferro
Carril	Carrila	k1gFnPc2	Carrila
Oeste	Oest	k1gMnSc5	Oest
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
Nacional	Nacional	k1gFnSc1	Nacional
<g/>
:	:	kIx,	:
Argentinos	Argentinos	k1gInSc1	Argentinos
Juniors	Juniorsa	k1gFnPc2	Juniorsa
</s>
</p>
<p>
<s>
===	===	k?	===
Argentinský	argentinský	k2eAgMnSc1d1	argentinský
šampion	šampion	k1gMnSc1	šampion
v	v	k7c6	v
letech	let	k1gInPc6	let
1986	[number]	k4	1986
–	–	k?	–
1991	[number]	k4	1991
===	===	k?	===
</s>
</p>
<p>
<s>
1985	[number]	k4	1985
<g/>
/	/	kIx~	/
<g/>
86	[number]	k4	86
River	Rivero	k1gNnPc2	Rivero
Plate	plat	k1gInSc5	plat
</s>
</p>
<p>
<s>
1986	[number]	k4	1986
<g/>
/	/	kIx~	/
<g/>
87	[number]	k4	87
Rosario	Rosario	k1gMnSc1	Rosario
Central	Central	k1gMnSc1	Central
</s>
</p>
<p>
<s>
1987	[number]	k4	1987
<g/>
/	/	kIx~	/
<g/>
88	[number]	k4	88
Newell	Newellum	k1gNnPc2	Newellum
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Old	Olda	k1gFnPc2	Olda
Boys	boy	k1gMnPc2	boy
</s>
</p>
<p>
<s>
1988	[number]	k4	1988
<g/>
/	/	kIx~	/
<g/>
89	[number]	k4	89
Independiente	Independient	k1gInSc5	Independient
</s>
</p>
<p>
<s>
1989	[number]	k4	1989
<g/>
/	/	kIx~	/
<g/>
90	[number]	k4	90
River	Rivero	k1gNnPc2	Rivero
Plate	plat	k1gInSc5	plat
</s>
</p>
<p>
<s>
1990	[number]	k4	1990
<g/>
/	/	kIx~	/
<g/>
91	[number]	k4	91
Newell	Newellum	k1gNnPc2	Newellum
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Old	Olda	k1gFnPc2	Olda
Boys	boy	k1gMnPc6	boy
</s>
</p>
<p>
<s>
===	===	k?	===
Apertura	apertura	k1gFnSc1	apertura
a	a	k8xC	a
Clausura	Clausura	k1gFnSc1	Clausura
v	v	k7c6	v
letech	let	k1gInPc6	let
1991	[number]	k4	1991
–	–	k?	–
2012	[number]	k4	2012
===	===	k?	===
</s>
</p>
<p>
<s>
1991	[number]	k4	1991
Apertura	apertura	k1gFnSc1	apertura
<g/>
:	:	kIx,	:
River	River	k1gInSc1	River
Plate	plat	k1gInSc5	plat
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
Clausura	Clausura	k1gFnSc1	Clausura
<g/>
:	:	kIx,	:
Newell	Newell	k1gInSc1	Newell
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Old	Olda	k1gFnPc2	Olda
Boys	boy	k1gMnPc6	boy
</s>
</p>
<p>
<s>
1992	[number]	k4	1992
Apertura	apertura	k1gFnSc1	apertura
<g/>
:	:	kIx,	:
Boca	Boca	k1gFnSc1	Boca
Juniors	Juniorsa	k1gFnPc2	Juniorsa
</s>
</p>
<p>
<s>
1993	[number]	k4	1993
Clausura	Clausura	k1gFnSc1	Clausura
<g/>
:	:	kIx,	:
Vélez	Vélez	k1gMnSc1	Vélez
Sársfield	Sársfield	k1gMnSc1	Sársfield
</s>
</p>
<p>
<s>
1993	[number]	k4	1993
Apertura	apertura	k1gFnSc1	apertura
<g/>
:	:	kIx,	:
River	River	k1gInSc1	River
Plate	plat	k1gInSc5	plat
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
Clausura	Clausura	k1gFnSc1	Clausura
<g/>
:	:	kIx,	:
Independiente	Independient	k1gInSc5	Independient
</s>
</p>
<p>
<s>
1994	[number]	k4	1994
Apertura	apertura	k1gFnSc1	apertura
<g/>
:	:	kIx,	:
River	River	k1gInSc1	River
Plate	plat	k1gInSc5	plat
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
Clausura	Clausura	k1gFnSc1	Clausura
<g/>
:	:	kIx,	:
San	San	k1gFnSc1	San
Lorenzo	Lorenza	k1gFnSc5	Lorenza
</s>
</p>
<p>
<s>
1995	[number]	k4	1995
Apertura	apertura	k1gFnSc1	apertura
<g/>
:	:	kIx,	:
Vélez	Vélez	k1gMnSc1	Vélez
Sársfield	Sársfield	k1gMnSc1	Sársfield
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
Clausura	Clausura	k1gFnSc1	Clausura
<g/>
:	:	kIx,	:
Vélez	Vélez	k1gMnSc1	Vélez
Sársfield	Sársfield	k1gMnSc1	Sársfield
</s>
</p>
<p>
<s>
1996	[number]	k4	1996
Apertura	apertura	k1gFnSc1	apertura
<g/>
:	:	kIx,	:
River	River	k1gInSc1	River
Plate	plat	k1gInSc5	plat
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
Clausura	Clausura	k1gFnSc1	Clausura
<g/>
:	:	kIx,	:
River	River	k1gInSc1	River
Plate	plat	k1gInSc5	plat
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
Apertura	apertura	k1gFnSc1	apertura
<g/>
:	:	kIx,	:
River	River	k1gInSc1	River
Plate	plat	k1gInSc5	plat
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
Clausura	Clausura	k1gFnSc1	Clausura
<g/>
:	:	kIx,	:
Vélez	Vélez	k1gMnSc1	Vélez
Sársfield	Sársfield	k1gMnSc1	Sársfield
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
Apertura	apertura	k1gFnSc1	apertura
<g/>
:	:	kIx,	:
Boca	Boca	k1gFnSc1	Boca
Juniors	Juniorsa	k1gFnPc2	Juniorsa
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
Clausura	Clausura	k1gFnSc1	Clausura
<g/>
:	:	kIx,	:
Boca	Boca	k1gFnSc1	Boca
Juniors	Juniorsa	k1gFnPc2	Juniorsa
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
Apertura	apertura	k1gFnSc1	apertura
<g/>
:	:	kIx,	:
River	River	k1gInSc1	River
Plate	plat	k1gInSc5	plat
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
Clausura	Clausura	k1gFnSc1	Clausura
<g/>
:	:	kIx,	:
River	River	k1gInSc1	River
Plate	plat	k1gInSc5	plat
</s>
</p>
<p>
<s>
2000	[number]	k4	2000
Apertura	apertura	k1gFnSc1	apertura
<g/>
:	:	kIx,	:
Boca	Boca	k1gFnSc1	Boca
Juniors	Juniorsa	k1gFnPc2	Juniorsa
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
Clausura	Clausura	k1gFnSc1	Clausura
<g/>
:	:	kIx,	:
San	San	k1gFnSc1	San
Lorenzo	Lorenza	k1gFnSc5	Lorenza
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
Apertura	apertura	k1gFnSc1	apertura
<g/>
:	:	kIx,	:
Racing	Racing	k1gInSc1	Racing
Club	club	k1gInSc1	club
de	de	k?	de
Avellaneda	Avellaned	k1gMnSc2	Avellaned
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
Clausura	Clausura	k1gFnSc1	Clausura
<g/>
:	:	kIx,	:
River	River	k1gInSc1	River
Plate	plat	k1gInSc5	plat
</s>
</p>
<p>
<s>
2002	[number]	k4	2002
Apertura	apertura	k1gFnSc1	apertura
<g/>
:	:	kIx,	:
Independiente	Independient	k1gInSc5	Independient
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
Clausura	Clausura	k1gFnSc1	Clausura
<g/>
:	:	kIx,	:
River	River	k1gInSc1	River
Plate	plat	k1gInSc5	plat
</s>
</p>
<p>
<s>
2003	[number]	k4	2003
Apertura	apertura	k1gFnSc1	apertura
<g/>
:	:	kIx,	:
Boca	Boca	k1gFnSc1	Boca
Juniors	Juniorsa	k1gFnPc2	Juniorsa
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
Clausura	Clausura	k1gFnSc1	Clausura
<g/>
:	:	kIx,	:
River	River	k1gInSc1	River
Plate	plat	k1gInSc5	plat
</s>
</p>
<p>
<s>
2004	[number]	k4	2004
Apertura	apertura	k1gFnSc1	apertura
<g/>
:	:	kIx,	:
Newell	Newell	k1gInSc1	Newell
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Old	Olda	k1gFnPc2	Olda
Boys	boy	k1gMnPc4	boy
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
Clausura	Clausura	k1gFnSc1	Clausura
<g/>
:	:	kIx,	:
Vélez	Vélez	k1gMnSc1	Vélez
Sársfield	Sársfield	k1gMnSc1	Sársfield
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
Apertura	apertura	k1gFnSc1	apertura
<g/>
:	:	kIx,	:
Boca	Boca	k1gFnSc1	Boca
Juniors	Juniorsa	k1gFnPc2	Juniorsa
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
Clausura	Clausura	k1gFnSc1	Clausura
<g/>
:	:	kIx,	:
Boca	Boca	k1gFnSc1	Boca
Juniors	Juniorsa	k1gFnPc2	Juniorsa
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
Apertura	apertura	k1gFnSc1	apertura
<g/>
:	:	kIx,	:
Estudiantes	Estudiantes	k1gInSc1	Estudiantes
de	de	k?	de
La	la	k1gNnSc1	la
Plata	plato	k1gNnSc2	plato
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
Clausura	Clausura	k1gFnSc1	Clausura
<g/>
:	:	kIx,	:
San	San	k1gFnSc1	San
Lorenzo	Lorenza	k1gFnSc5	Lorenza
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
Apertura	apertura	k1gFnSc1	apertura
<g/>
:	:	kIx,	:
CA	ca	kA	ca
Lanús	Lanús	k1gInSc1	Lanús
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
Clausura	Clausura	k1gFnSc1	Clausura
<g/>
:	:	kIx,	:
River	River	k1gInSc1	River
Plate	plat	k1gInSc5	plat
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
Apertura	apertura	k1gFnSc1	apertura
<g/>
:	:	kIx,	:
Boca	Boca	k1gFnSc1	Boca
Juniors	Juniorsa	k1gFnPc2	Juniorsa
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
Clausura	Clausura	k1gFnSc1	Clausura
<g/>
:	:	kIx,	:
Vélez	Vélez	k1gMnSc1	Vélez
Sársfield	Sársfield	k1gMnSc1	Sársfield
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
Apertura	apertura	k1gFnSc1	apertura
<g/>
:	:	kIx,	:
CA	ca	kA	ca
Banfield	Banfield	k1gMnSc1	Banfield
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
Clausura	Clausura	k1gFnSc1	Clausura
<g/>
:	:	kIx,	:
Argentinos	Argentinos	k1gInSc1	Argentinos
Juniors	Juniorsa	k1gFnPc2	Juniorsa
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
Apertura	apertura	k1gFnSc1	apertura
<g/>
:	:	kIx,	:
Estudiantes	Estudiantes	k1gInSc1	Estudiantes
de	de	k?	de
La	la	k1gNnSc1	la
Plata	plato	k1gNnSc2	plato
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
Clausura	Clausura	k1gFnSc1	Clausura
<g/>
:	:	kIx,	:
Vélez	Vélez	k1gMnSc1	Vélez
Sársfield	Sársfield	k1gMnSc1	Sársfield
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
Apertura	apertura	k1gFnSc1	apertura
<g/>
:	:	kIx,	:
Boca	Boca	k1gFnSc1	Boca
Juniors	Juniorsa	k1gFnPc2	Juniorsa
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
Clausura	Clausura	k1gFnSc1	Clausura
<g/>
:	:	kIx,	:
Arsenal	Arsenal	k1gFnSc1	Arsenal
FC	FC	kA	FC
(	(	kIx(	(
<g/>
Buenos	Buenos	k1gMnSc1	Buenos
Aires	Aires	k1gMnSc1	Aires
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Sezóny	sezóna	k1gFnSc2	sezóna
Inicial	Inicial	k1gMnSc1	Inicial
<g/>
/	/	kIx~	/
<g/>
Final	Final	k1gMnSc1	Final
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
–	–	k?	–
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
Inicial	Inicial	k1gMnSc1	Inicial
<g/>
:	:	kIx,	:
Vélez	Vélez	k1gMnSc1	Vélez
Sársfield	Sársfield	k1gMnSc1	Sársfield
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
Final	Final	k1gInSc1	Final
<g/>
:	:	kIx,	:
Newell	Newell	k1gInSc1	Newell
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Old	Olda	k1gFnPc2	Olda
Boys	boy	k1gMnPc6	boy
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
Superfinal	Superfinal	k1gMnSc1	Superfinal
<g/>
:	:	kIx,	:
Vélez	Vélez	k1gMnSc1	Vélez
Sársfield	Sársfield	k1gMnSc1	Sársfield
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
Inicial	Inicial	k1gInSc1	Inicial
<g/>
:	:	kIx,	:
San	San	k1gFnSc1	San
Lorenzo	Lorenza	k1gFnSc5	Lorenza
de	de	k?	de
Almagro	Almagro	k1gNnSc1	Almagro
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
Final	Final	k1gInSc1	Final
<g/>
:	:	kIx,	:
River	River	k1gInSc1	River
Plate	plat	k1gInSc5	plat
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
Transición	Transición	k1gInSc1	Transición
<g/>
:	:	kIx,	:
Racing	Racing	k1gInSc1	Racing
Club	club	k1gInSc1	club
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Argentine	Argentin	k1gInSc5	Argentin
Primera	primera	k1gFnSc1	primera
División	División	k1gInSc1	División
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Copa	Cop	k2eAgFnSc1d1	Copa
Argentina	Argentina	k1gFnSc1	Argentina
</s>
</p>
<p>
<s>
Supercopa	Supercopa	k1gFnSc1	Supercopa
Argentina	Argentina	k1gFnSc1	Argentina
</s>
</p>
