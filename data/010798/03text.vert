<p>
<s>
BORA	BORA	kA
-	-	kIx~
hansgrohe	hansgrohe	k1gFnSc1	hansgrohe
je	být	k5eAaImIp3nS
profesionální	profesionální	k2eAgFnSc1d1	profesionální
cyklistická	cyklistický	k2eAgFnSc1d1	cyklistická
stáj	stáj	k1gFnSc1	stáj
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
německém	německý	k2eAgNnSc6d1	německé
městě	město	k1gNnSc6	město
Raubling	Raubling	k1gInSc1	Raubling
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6	rok
2010	#num#	k4
pod	pod	k7c7
názvem	název	k1gInSc7	název
Team	team	k1gInSc1	team
NetApp	NetApp	k1gInSc4	NetApp
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6	rok
2015	#num#	k4
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
hlavním	hlavní	k2eAgMnSc7d1	hlavní
sponzorem	sponzor	k1gMnSc7	sponzor
firma	firma	k1gFnSc1	firma
BORA	BORA	kA
vyrábějící	vyrábějící	k2eAgFnSc1d1	vyrábějící
kuchyňská	kuchyňská	k1gFnSc1	kuchyňská
zařízení	zařízení	k1gNnSc2	zařízení
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6	rok
2016	#num#	k4
do	do	k7c2
stáje	stáj	k1gFnSc2	stáj
vstoupil	vstoupit	k5eAaPmAgMnS
výrobce	výrobce	k1gMnSc1	výrobce
sanitární	sanitární	k2eAgFnSc2d1	sanitární
techniky	technika	k1gFnSc2	technika
Hansgrohe	Hansgroh	k1gFnSc2	Hansgroh
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stáj	stáj	k1gFnSc1	stáj
používá	používat	k5eAaImIp3nS
kola	kolo	k1gNnSc2	kolo
americké	americký	k2eAgFnSc2d1	americká
firmy	firma	k1gFnSc2	firma
Specialized	Specialized	k1gMnSc1	Specialized
Bicycle	Bicycle	k1gFnSc2	Bicycle
Components	Components	k1gInSc1	Components
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
sezónu	sezóna	k1gFnSc4	sezóna
2017	#num#	k4
získala	získat	k5eAaPmAgFnS
licenci	licence	k1gFnSc4	licence
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
kategorie	kategorie	k1gFnSc2	kategorie
UCI	UCI	kA
WorldTeam	WorldTeam	k1gInSc1	WorldTeam
<g/>
.	.	kIx.
</s>
<s desamb="1">
Generálním	generální	k2eAgInSc7d1	generální
manažerem	manažer	k1gInSc7	manažer
je	být	k5eAaImIp3nS
Ralph	Ralph	k1gMnSc1	Ralph
Denk	Denk	k1gMnSc1	Denk
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
Bora-hansgrohe	Boraansgrohe	k1gNnSc4	Bora-hansgrohe
jezdí	jezdit	k5eAaImIp3nS
Jan	Jan	k1gMnSc1	Jan
Bárta	Bárta	k1gMnSc1	Bárta
<g/>
,	,	kIx,
Leopold	Leopold	k1gMnSc1	Leopold
König	König	k1gMnSc1	König
<g/>
,	,	kIx,
Shane	Shan	k1gInSc5	Shan
Archbold	Archbold	k1gInSc1	Archbold
<g/>
,	,	kIx,
Rafał	Rafał	k1gFnSc1	Rafał
Majka	Majka	k1gFnSc1	Majka
nebo	nebo	k8xC
Rüdiger	Rüdiger	k1gMnSc1	Rüdiger
Selig	Selig	k1gMnSc1	Selig
<g/>
,	,	kIx,
na	na	k7c6
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2016	#num#	k4
tým	tým	k1gInSc1	tým
posílil	posílit	k5eAaPmAgInS
dvounásobný	dvounásobný	k2eAgMnSc1d1	dvounásobný
mistr	mistr	k1gMnSc1	mistr
světa	svět	k1gInSc2	svět
Peter	Peter	k1gMnSc1	Peter
Sagan	Sagan	k1gMnSc1	Sagan
z	z	k7c2
Team	team	k1gInSc1	team
Tinkoff-Saxo	Tinkoff-Saxo	k1gNnSc1	Tinkoff-Saxo
<g/>
.	.	kIx.
</s>
</p>
<p>
<s>
==	==	k?
Historie	historie	k1gFnSc1	historie
názvu	název	k1gInSc2	název
týmu	tým	k1gInSc2	tým
==	==	k?
</s>
</p>
<p>
<s>
2010-2012	2010-2012	k4
Tým	tým	k1gInSc1	tým
NetApp	NetApp	k1gInSc4	NetApp
(	(	kIx(
<g/>
APP	APP	kA
<g/>
)	)	kIx)
</s>
</p>
<p>
<s>
2013-2014	2013-2014	k4
Tým	tým	k1gInSc1	tým
NetApp-Endura	NetApp-Endura	k1gFnSc1	NetApp-Endura
(	(	kIx(
<g/>
TNE	tnout	k5eAaBmIp3nS,k5eAaPmIp3nS
<g/>
)	)	kIx)
</s>
</p>
<p>
<s>
2015-2016	2015-2016	k4
Bora-Argon	Bora-Argon	k1gInSc1	Bora-Argon
18	#num#	k4
(	(	kIx(
<g/>
BOA	boa	k1gNnSc2	boa
<g/>
)	)	kIx)
</s>
</p>
<p>
<s>
2017	#num#	k4
<g/>
-Bora	-Bora	k1gFnSc1	-Bora
Hansgrohe	Hansgrohe	k1gFnSc1	Hansgrohe
(	(	kIx(
<g/>
BOH	BOH	kA
<g/>
)	)	kIx)
</s>
</p>
<p>
<s>
==	==	k?
Sezóna	sezóna	k1gFnSc1	sezóna
2019	#num#	k4
==	==	k?
</s>
</p>
<p>
<s>
==	==	k?
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?
</s>
</p>
<p>
<s>
https://www.bora-hansgrohe.com/	https://www.boraansgrohe.com/	k?
</s>
</p>
<p>
<s>
http://www.procyclingstats.com/team.php?id=1411	http://www.procyclingstats.com/team.php?id=1411	k4
</s>
</p>
