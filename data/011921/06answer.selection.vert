<s>
Krychle	krychle	k1gFnSc1	krychle
(	(	kIx(	(
<g/>
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
šestistěn	šestistěn	k2eAgInSc1d1	šestistěn
nebo	nebo	k8xC	nebo
také	také	k9	také
hexaedr	hexaedr	k1gInSc1	hexaedr
<g/>
)	)	kIx)	)
lidově	lidově	k6eAd1	lidově
zvaná	zvaný	k2eAgFnSc1d1	zvaná
též	též	k9	též
kostka	kostka	k1gFnSc1	kostka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
trojrozměrné	trojrozměrný	k2eAgNnSc1d1	trojrozměrné
těleso	těleso	k1gNnSc1	těleso
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnPc1	jehož
stěny	stěna	k1gFnPc1	stěna
tvoří	tvořit	k5eAaImIp3nP	tvořit
šest	šest	k4xCc4	šest
stejných	stejný	k2eAgInPc2d1	stejný
čtverců	čtverec	k1gInPc2	čtverec
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
osm	osm	k4xCc1	osm
rohů	roh	k1gInPc2	roh
a	a	k8xC	a
dvanáct	dvanáct	k4xCc1	dvanáct
hran	hrana	k1gFnPc2	hrana
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
