<s>
Povstalecká	povstalecký	k2eAgFnSc1d1
palba	palba	k1gFnSc1
z	z	k7c2
vesnice	vesnice	k1gFnSc2
a	a	k8xC
okolních	okolní	k2eAgInPc2d1
kopců	kopec	k1gInPc2
brzy	brzy	k6eAd1
vyřadila	vyřadit	k5eAaPmAgFnS
z	z	k7c2
boje	boj	k1gInSc2
vozidla	vozidlo	k1gNnSc2
HMMWV	HMMWV	kA
a	a	k8xC
i	i	k9
minomet	minomet	k1gInSc1
ráže	ráže	k1gFnSc2
120	[number]	k4
mm	mm	kA
<g/>
,	,	kIx,
povstalci	povstalec	k1gMnPc1
se	se	k3xPyFc4
probojovali	probojovat	k5eAaPmAgMnP
až	až	k9
k	k	k7c3
ochranným	ochranný	k2eAgInPc3d1
pískovým	pískový	k2eAgInPc3d1
valům	val	k1gInPc3
základny	základna	k1gFnSc2
<g/>
.	.	kIx.
</s>