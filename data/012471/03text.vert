<p>
<s>
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Wantu	Want	k1gInSc2	Want
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2008	[number]	k4	2008
v	v	k7c6	v
afghánské	afghánský	k2eAgFnSc6d1	afghánská
provincii	provincie	k1gFnSc6	provincie
Núristán	Núristán	k1gInSc4	Núristán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Brzy	brzy	k6eAd1	brzy
ráno	ráno	k6eAd1	ráno
ve	v	k7c6	v
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
místního	místní	k2eAgInSc2d1	místní
času	čas	k1gInSc2	čas
podniklo	podniknout	k5eAaPmAgNnS	podniknout
přibližně	přibližně	k6eAd1	přibližně
200	[number]	k4	200
mudžáhidů	mudžáhid	k1gMnPc2	mudžáhid
koordinovaný	koordinovaný	k2eAgInSc4d1	koordinovaný
útok	útok	k1gInSc4	útok
z	z	k7c2	z
několika	několik	k4yIc2	několik
směrů	směr	k1gInPc2	směr
na	na	k7c4	na
rozestavěnou	rozestavěný	k2eAgFnSc4d1	rozestavěná
americkou	americký	k2eAgFnSc4d1	americká
základnu	základna	k1gFnSc4	základna
Kahler	Kahler	k1gInSc1	Kahler
a	a	k8xC	a
pozorovací	pozorovací	k2eAgNnPc1d1	pozorovací
stanoviště	stanoviště	k1gNnPc1	stanoviště
Top	topit	k5eAaImRp2nS	topit
Side	Sid	k1gInSc2	Sid
poblíž	poblíž	k6eAd1	poblíž
vesnice	vesnice	k1gFnSc1	vesnice
Want	Wanta	k1gFnPc2	Wanta
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Waygal	Waygal	k1gInSc1	Waygal
<g/>
.	.	kIx.	.
</s>
<s>
Povstalecká	povstalecký	k2eAgFnSc1d1	povstalecká
palba	palba	k1gFnSc1	palba
z	z	k7c2	z
vesnice	vesnice	k1gFnSc2	vesnice
a	a	k8xC	a
okolních	okolní	k2eAgInPc2d1	okolní
kopců	kopec	k1gInPc2	kopec
brzy	brzy	k6eAd1	brzy
vyřadila	vyřadit	k5eAaPmAgFnS	vyřadit
z	z	k7c2	z
boje	boj	k1gInSc2	boj
vozidla	vozidlo	k1gNnSc2	vozidlo
HMMWV	HMMWV	kA	HMMWV
a	a	k8xC	a
i	i	k9	i
minomet	minomet	k1gInSc1	minomet
ráže	ráže	k1gFnSc2	ráže
120	[number]	k4	120
mm	mm	kA	mm
<g/>
,	,	kIx,	,
povstalci	povstalec	k1gMnPc1	povstalec
se	se	k3xPyFc4	se
probojovali	probojovat	k5eAaPmAgMnP	probojovat
až	až	k9	až
k	k	k7c3	k
ochranným	ochranný	k2eAgInPc3d1	ochranný
pískovým	pískový	k2eAgInPc3d1	pískový
valům	val	k1gInPc3	val
základny	základna	k1gFnSc2	základna
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
29	[number]	k4	29
zahájila	zahájit	k5eAaPmAgFnS	zahájit
dělostřelecká	dělostřelecký	k2eAgFnSc1d1	dělostřelecká
četa	četa	k1gFnSc1	četa
z	z	k7c2	z
Blessingu	Blessing	k1gInSc2	Blessing
palbu	palba	k1gFnSc4	palba
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
perimetru	perimetr	k1gInSc2	perimetr
základny	základna	k1gFnSc2	základna
<g/>
,	,	kIx,	,
celkem	celkem	k6eAd1	celkem
vypálila	vypálit	k5eAaPmAgFnS	vypálit
téměř	téměř	k6eAd1	téměř
stovku	stovka	k1gFnSc4	stovka
tříštivo-trhavých	tříštivorhavý	k2eAgInPc2d1	tříštivo-trhavý
granátů	granát	k1gInPc2	granát
ráže	ráže	k1gFnSc2	ráže
155	[number]	k4	155
mm	mm	kA	mm
<g/>
,	,	kIx,	,
palba	palba	k1gFnSc1	palba
z	z	k7c2	z
houfnic	houfnice	k1gFnPc2	houfnice
však	však	k9	však
nebyla	být	k5eNaImAgFnS	být
příliš	příliš	k6eAd1	příliš
účinná	účinný	k2eAgFnSc1d1	účinná
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
půl	půl	k1xP	půl
hodiny	hodina	k1gFnSc2	hodina
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
nad	nad	k7c7	nad
bojištěm	bojiště	k1gNnSc7	bojiště
objevil	objevit	k5eAaPmAgMnS	objevit
bombardér	bombardér	k1gMnSc1	bombardér
B-1B	B-1B	k1gMnSc1	B-1B
Lancer	Lancer	k1gMnSc1	Lancer
a	a	k8xC	a
následně	následně	k6eAd1	následně
i	i	k9	i
dva	dva	k4xCgInPc1	dva
bitevní	bitevní	k2eAgInPc1d1	bitevní
vrtulníky	vrtulník	k1gInPc1	vrtulník
AH-64	AH-64	k1gFnSc4	AH-64
Apache	Apach	k1gFnPc1	Apach
a	a	k8xC	a
letadla	letadlo	k1gNnPc1	letadlo
F-15E	F-15E	k1gMnSc2	F-15E
Strike	Strik	k1gMnSc2	Strik
Eagle	Eagl	k1gMnSc2	Eagl
a	a	k8xC	a
A-10	A-10	k1gMnPc1	A-10
Thunderbolt	Thunderbolta	k1gFnPc2	Thunderbolta
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Obránci	obránce	k1gMnPc1	obránce
základny	základna	k1gFnSc2	základna
začali	začít	k5eAaPmAgMnP	začít
získávat	získávat	k5eAaImF	získávat
převahu	převaha	k1gFnSc4	převaha
a	a	k8xC	a
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
posil	posila	k1gFnPc2	posila
s	s	k7c7	s
vozidly	vozidlo	k1gNnPc7	vozidlo
HMMWV	HMMWV	kA	HMMWV
bojová	bojový	k2eAgFnSc1d1	bojová
činnost	činnost	k1gFnSc1	činnost
nepřátel	nepřítel	k1gMnPc2	nepřítel
utichla	utichnout	k5eAaPmAgFnS	utichnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
vydal	vydat	k5eAaPmAgMnS	vydat
generálmajor	generálmajor	k1gMnSc1	generálmajor
J.	J.	kA	J.
Schlosser	Schlosser	k1gInSc1	Schlosser
rozkaz	rozkaz	k1gInSc1	rozkaz
ke	k	k7c3	k
stažení	stažení	k1gNnSc3	stažení
amerických	americký	k2eAgFnPc2d1	americká
jednotek	jednotka	k1gFnPc2	jednotka
ze	z	k7c2	z
základny	základna	k1gFnSc2	základna
Kahler	Kahler	k1gInSc1	Kahler
a	a	k8xC	a
z	z	k7c2	z
celého	celý	k2eAgNnSc2d1	celé
údolí	údolí	k1gNnSc2	údolí
Waygal	Waygal	k1gInSc1	Waygal
<g/>
.	.	kIx.	.
</s>
<s>
Protikoaliční	Protikoaliční	k2eAgMnPc1d1	Protikoaliční
bojovníci	bojovník	k1gMnPc1	bojovník
tak	tak	k6eAd1	tak
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
velkého	velký	k2eAgNnSc2d1	velké
strategického	strategický	k2eAgNnSc2d1	strategické
vítězství	vítězství	k1gNnSc2	vítězství
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
utrpěli	utrpět	k5eAaPmAgMnP	utrpět
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
velké	velký	k2eAgFnSc2d1	velká
ztráty	ztráta	k1gFnSc2	ztráta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
ROVENSKÝ	ROVENSKÝ	kA	ROVENSKÝ
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
<g/>
.	.	kIx.	.
</s>
<s>
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Wantu	Want	k1gInSc2	Want
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nebeští	nebeský	k2eAgMnPc1d1	nebeský
vojáci	voják	k1gMnPc1	voják
<g/>
"	"	kIx"	"
v	v	k7c6	v
krvavém	krvavý	k2eAgInSc6d1	krvavý
Nuristánu	Nuristán	k1gInSc6	Nuristán
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
I.	I.	kA	I.
<g/>
.	.	kIx.	.
</s>
<s>
ATM	ATM	kA	ATM
<g/>
.	.	kIx.	.
</s>
<s>
Leden	leden	k1gInSc1	leden
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
s.	s.	k?	s.
10	[number]	k4	10
až	až	k9	až
13	[number]	k4	13
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1802	[number]	k4	1802
<g/>
-	-	kIx~	-
<g/>
4823	[number]	k4	4823
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ROVENSKÝ	ROVENSKÝ	kA	ROVENSKÝ
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
<g/>
.	.	kIx.	.
</s>
<s>
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Wantu	Want	k1gInSc2	Want
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nebeští	nebeský	k2eAgMnPc1d1	nebeský
vojáci	voják	k1gMnPc1	voják
<g/>
"	"	kIx"	"
v	v	k7c6	v
krvavém	krvavý	k2eAgInSc6d1	krvavý
Nuristánu	Nuristán	k1gInSc6	Nuristán
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
II	II	kA	II
<g/>
..	..	k?	..
ATM	ATM	kA	ATM
<g/>
.	.	kIx.	.
</s>
<s>
Únor	únor	k1gInSc1	únor
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
s.	s.	k?	s.
4	[number]	k4	4
až	až	k9	až
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1802	[number]	k4	1802
<g/>
-	-	kIx~	-
<g/>
4823	[number]	k4	4823
<g/>
.	.	kIx.	.
</s>
</p>
