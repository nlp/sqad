<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Filípek	Filípek	k1gMnSc1	Filípek
(	(	kIx(	(
<g/>
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
Sviny	Svina	k1gFnPc1	Svina
u	u	k7c2	u
Veselí	veselí	k1gNnSc2	veselí
nad	nad	k7c7	nad
Lužnicí	Lužnice	k1gFnSc7	Lužnice
–	–	k?	–
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
patriot	patriot	k1gMnSc1	patriot
<g/>
,	,	kIx,	,
podnikatel	podnikatel	k1gMnSc1	podnikatel
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
mecenáš	mecenáš	k1gMnSc1	mecenáš
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Jan	Jan	k1gMnSc1	Jan
Filípek	Filípek	k1gMnSc1	Filípek
byl	být	k5eAaImAgMnS	být
pravnukem	pravnuk	k1gMnSc7	pravnuk
českého	český	k2eAgMnSc2d1	český
buditele	buditel	k1gMnSc2	buditel
<g/>
,	,	kIx,	,
novináře	novinář	k1gMnSc2	novinář
a	a	k8xC	a
spisovatele	spisovatel	k1gMnSc2	spisovatel
Václava	Václav	k1gMnSc2	Václav
Filípka	Filípek	k1gMnSc2	Filípek
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
studiích	studio	k1gNnPc6	studio
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
podniku	podnik	k1gInSc6	podnik
Kooperativa	kooperativa	k1gFnSc1	kooperativa
v	v	k7c6	v
obchodně-zemědělském	obchodněemědělský	k2eAgInSc6d1	obchodně-zemědělský
oboru	obor	k1gInSc6	obor
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
jako	jako	k9	jako
tajemník	tajemník	k1gMnSc1	tajemník
JUDr.	JUDr.	kA	JUDr.
Ladislava	Ladislav	k1gMnSc4	Ladislav
Feierabenda	Feierabend	k1gMnSc4	Feierabend
<g/>
,	,	kIx,	,
ministra	ministr	k1gMnSc4	ministr
československé	československý	k2eAgFnSc2d1	Československá
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
ČSR	ČSR	kA	ČSR
a	a	k8xC	a
exilové	exilový	k2eAgFnSc2d1	exilová
vlády	vláda	k1gFnSc2	vláda
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Filípek	Filípek	k1gMnSc1	Filípek
byl	být	k5eAaImAgMnS	být
za	za	k7c4	za
odbojovou	odbojový	k2eAgFnSc4d1	odbojová
činnost	činnost	k1gFnSc4	činnost
v	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
německé	německý	k2eAgFnSc2d1	německá
okupace	okupace	k1gFnSc2	okupace
ČSR	ČSR	kA	ČSR
zatčen	zatčen	k2eAgMnSc1d1	zatčen
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1941	[number]	k4	1941
až	až	k9	až
1945	[number]	k4	1945
vězněn	vězněn	k2eAgInSc4d1	vězněn
(	(	kIx(	(
<g/>
Pankrác	Pankrác	k1gFnSc1	Pankrác
<g/>
,	,	kIx,	,
Berlín	Berlín	k1gInSc1	Berlín
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1948	[number]	k4	1948
emigroval	emigrovat	k5eAaBmAgMnS	emigrovat
do	do	k7c2	do
Austrálie	Austrálie	k1gFnSc2	Austrálie
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
odešel	odejít	k5eAaPmAgMnS	odejít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
do	do	k7c2	do
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
podnikal	podnikat	k5eAaImAgMnS	podnikat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
aktivní	aktivní	k2eAgInSc1d1	aktivní
v	v	k7c6	v
čs	čs	kA	čs
<g/>
.	.	kIx.	.
krajanském	krajanský	k2eAgNnSc6d1	krajanské
hnutí	hnutí	k1gNnSc6	hnutí
<g/>
,	,	kIx,	,
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
a	a	k8xC	a
psal	psát	k5eAaImAgMnS	psát
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
řady	řada	k1gFnSc2	řada
knih	kniha	k1gFnPc2	kniha
svých	svůj	k3xOyFgFnPc2	svůj
pamětí	paměť	k1gFnPc2	paměť
a	a	k8xC	a
politických	politický	k2eAgFnPc2d1	politická
úvah	úvaha	k1gFnPc2	úvaha
–	–	k?	–
literatury	literatura	k1gFnSc2	literatura
faktu	fakt	k1gInSc2	fakt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Z	z	k7c2	z
díla	dílo	k1gNnSc2	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Zpod	zpod	k7c2	zpod
šibenice	šibenice	k1gFnSc2	šibenice
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
(	(	kIx(	(
<g/>
Ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
šibenice	šibenice	k1gFnSc2	šibenice
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
1982	[number]	k4	1982
česky	česky	k6eAd1	česky
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
anglicky	anglicky	k6eAd1	anglicky
<g/>
,	,	kIx,	,
v	v	k7c6	v
ČSFR	ČSFR	kA	ČSFR
–	–	k?	–
Ivo	Ivo	k1gMnSc1	Ivo
Železný	Železný	k1gMnSc1	Železný
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Odlesky	odlesk	k1gInPc1	odlesk
dějin	dějiny	k1gFnPc2	dějiny
československého	československý	k2eAgInSc2d1	československý
exilu	exil	k1gInSc2	exil
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
kniha	kniha	k1gFnSc1	kniha
popisuje	popisovat	k5eAaImIp3nS	popisovat
<g/>
,	,	kIx,	,
s	s	k7c7	s
jakými	jaký	k3yIgFnPc7	jaký
osobnostmi	osobnost	k1gFnPc7	osobnost
se	se	k3xPyFc4	se
za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
exilu	exil	k1gInSc6	exil
setkal	setkat	k5eAaPmAgMnS	setkat
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Rudolf	Rudolf	k1gMnSc1	Rudolf
Friml	Friml	k1gMnSc1	Friml
<g/>
,	,	kIx,	,
Rudolf	Rudolf	k1gMnSc1	Rudolf
Firkušný	Firkušný	k2eAgMnSc1d1	Firkušný
<g/>
,	,	kIx,	,
Jára	Jára	k1gMnSc1	Jára
Kohout	Kohout	k1gMnSc1	Kohout
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Voskovec	Voskovec	k1gMnSc1	Voskovec
<g/>
,	,	kIx,	,
Marie	Marie	k1gFnSc1	Marie
Provazníková	Provazníková	k1gFnSc1	Provazníková
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mnichov	Mnichov	k1gInSc1	Mnichov
1938	[number]	k4	1938
–	–	k?	–
Hra	hra	k1gFnSc1	hra
o	o	k7c4	o
Československo	Československo	k1gNnSc4	Československo
(	(	kIx(	(
<g/>
Grégr	Grégr	k1gMnSc1	Grégr
a	a	k8xC	a
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Jan	Jan	k1gMnSc1	Jan
Filípek	Filípek	k1gMnSc1	Filípek
</s>
</p>
<p>
<s>
J.	J.	kA	J.
Filípek	Filípek	k1gMnSc1	Filípek
</s>
</p>
<p>
<s>
JUDr.	JUDr.	kA	JUDr.
L.	L.	kA	L.
Feierabend	Feierabend	k1gMnSc1	Feierabend
</s>
</p>
