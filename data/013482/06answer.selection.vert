<s desamb="1">
Bitva	bitva	k1gFnSc1
probíhala	probíhat	k5eAaImAgFnS
na	na	k7c6
západní	západní	k2eAgFnSc6d1
frontě	fronta	k1gFnSc6
od	od	k7c2
července	červenec	k1gInSc2
do	do	k7c2
listopadu	listopad	k1gInSc2
1917	#num#	k4
a	a	k8xC
bojovalo	bojovat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
hřebeny	hřeben	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yRgInPc1
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
na	na	k7c6
jihu	jih	k1gInSc6
a	a	k8xC
východě	východ	k1gInSc6
od	od	k7c2
belgického	belgický	k2eAgNnSc2d1
města	město	k1gNnSc2
Ypry	Ypry	k1gInPc1
v	v	k7c6
Západních	západní	k2eAgInPc6d1
Flandrech	Flandry	k1gInPc6
<g/>
.	.	kIx.
</s>