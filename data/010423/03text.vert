<p>
<s>
Thomas	Thomas	k1gMnSc1	Thomas
Cowperthwait	Cowperthwait	k1gMnSc1	Cowperthwait
Eakins	Eakinsa	k1gFnPc2	Eakinsa
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1844	[number]	k4	1844
Filadelfie	Filadelfie	k1gFnSc2	Filadelfie
–	–	k?	–
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1916	[number]	k4	1916
Filadelfie	Filadelfie	k1gFnSc2	Filadelfie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
realistický	realistický	k2eAgMnSc1d1	realistický
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
fotograf	fotograf	k1gMnSc1	fotograf
<g/>
,	,	kIx,	,
sochař	sochař	k1gMnSc1	sochař
a	a	k8xC	a
výtvarný	výtvarný	k2eAgMnSc1d1	výtvarný
umělec	umělec	k1gMnSc1	umělec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
široce	široko	k6eAd1	široko
uznávaný	uznávaný	k2eAgMnSc1d1	uznávaný
jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
umělců	umělec	k1gMnPc2	umělec
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
historii	historie	k1gFnSc6	historie
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
a	a	k8xC	a
dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
Namaloval	namalovat	k5eAaPmAgMnS	namalovat
několik	několik	k4yIc4	několik
set	sto	k4xCgNnPc2	sto
portrétů	portrét	k1gInPc2	portrét
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
svých	svůj	k3xOyFgMnPc2	svůj
přátel	přítel	k1gMnPc2	přítel
<g/>
,	,	kIx,	,
rodinných	rodinný	k2eAgMnPc2d1	rodinný
příslušníků	příslušník	k1gMnPc2	příslušník
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
významných	významný	k2eAgFnPc2d1	významná
osobností	osobnost	k1gFnPc2	osobnost
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
vědy	věda	k1gFnSc2	věda
<g/>
,	,	kIx,	,
lékařství	lékařství	k1gNnSc2	lékařství
a	a	k8xC	a
duchovních	duchovní	k1gMnPc2	duchovní
<g/>
.	.	kIx.	.
</s>
<s>
Portréty	portrét	k1gInPc1	portrét
nabízí	nabízet	k5eAaImIp3nP	nabízet
přehled	přehled	k1gInSc4	přehled
o	o	k7c6	o
intelektuálním	intelektuální	k2eAgInSc6d1	intelektuální
životě	život	k1gInSc6	život
ve	v	k7c6	v
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
počátku	počátek	k1gInSc2	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Eakins	Eakins	k6eAd1	Eakins
namaloval	namalovat	k5eAaPmAgMnS	namalovat
řadu	řada	k1gFnSc4	řada
velkých	velký	k2eAgInPc2d1	velký
obrazů	obraz	k1gInPc2	obraz
<g/>
,	,	kIx,	,
kterými	který	k3yQgInPc7	který
přenesl	přenést	k5eAaPmAgInS	přenést
portrét	portrét	k1gInSc1	portrét
z	z	k7c2	z
ateliéru	ateliér	k1gInSc2	ateliér
do	do	k7c2	do
kanceláří	kancelář	k1gFnPc2	kancelář
<g/>
,	,	kIx,	,
ulic	ulice	k1gFnPc2	ulice
<g/>
,	,	kIx,	,
parků	park	k1gInPc2	park
a	a	k8xC	a
na	na	k7c4	na
břehy	břeh	k1gInPc4	břeh
řek	řek	k1gMnSc1	řek
svého	svůj	k3xOyFgNnSc2	svůj
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Aktivní	aktivní	k2eAgInSc1d1	aktivní
pohyb	pohyb	k1gInSc1	pohyb
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
mu	on	k3xPp3gMnSc3	on
umožnil	umožnit	k5eAaPmAgInS	umožnit
malovat	malovat	k5eAaImF	malovat
téma	téma	k1gNnSc4	téma
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
ho	on	k3xPp3gMnSc4	on
nejvíce	hodně	k6eAd3	hodně
inspirovalo	inspirovat	k5eAaBmAgNnS	inspirovat
<g/>
:	:	kIx,	:
nahé	nahý	k2eAgFnPc4d1	nahá
nebo	nebo	k8xC	nebo
lehce	lehko	k6eAd1	lehko
oděné	oděný	k2eAgFnPc4d1	oděná
postavy	postava	k1gFnPc4	postava
v	v	k7c6	v
pohybu	pohyb	k1gInSc6	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Maloval	malovat	k5eAaImAgMnS	malovat
subjekty	subjekt	k1gInPc7	subjekt
v	v	k7c6	v
plném	plný	k2eAgNnSc6d1	plné
slunečním	sluneční	k2eAgNnSc6d1	sluneční
světle	světlo	k1gNnSc6	světlo
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
toho	ten	k3xDgInSc2	ten
vytvářel	vytvářet	k5eAaImAgMnS	vytvářet
také	také	k6eAd1	také
obrazy	obraz	k1gInPc4	obraz
hlubokého	hluboký	k2eAgInSc2d1	hluboký
vesmíru	vesmír	k1gInSc2	vesmír
s	s	k7c7	s
využitím	využití	k1gNnSc7	využití
svého	svůj	k3xOyFgNnSc2	svůj
studia	studio	k1gNnSc2	studio
perspektivy	perspektiva	k1gFnSc2	perspektiva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Eakins	Eakins	k1gInSc1	Eakins
projevil	projevit	k5eAaPmAgInS	projevit
také	také	k9	také
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
nové	nový	k2eAgFnPc4d1	nová
technologie	technologie	k1gFnPc4	technologie
chronofotografie	chronofotografie	k1gFnSc2	chronofotografie
–	–	k?	–
oblasti	oblast	k1gFnSc2	oblast
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
je	být	k5eAaImIp3nS	být
nyní	nyní	k6eAd1	nyní
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
inovátora	inovátor	k1gMnSc4	inovátor
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
kontroverzní	kontroverzní	k2eAgFnSc4d1	kontroverzní
postavu	postava	k1gFnSc4	postava
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
práci	práce	k1gFnSc4	práce
byla	být	k5eAaImAgFnS	být
za	za	k7c2	za
jeho	on	k3xPp3gInSc2	on
života	život	k1gInSc2	život
věnována	věnovat	k5eAaImNgFnS	věnovat
malá	malý	k2eAgFnSc1d1	malá
míra	míra	k1gFnSc1	míra
oficiálního	oficiální	k2eAgNnSc2d1	oficiální
uznání	uznání	k1gNnSc2	uznání
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
smrti	smrt	k1gFnSc6	smrt
byl	být	k5eAaImAgInS	být
americkými	americký	k2eAgMnPc7d1	americký
historiky	historik	k1gMnPc7	historik
označen	označit	k5eAaPmNgMnS	označit
jako	jako	k9	jako
"	"	kIx"	"
<g/>
nejsilnější	silný	k2eAgFnSc1d3	nejsilnější
<g/>
,	,	kIx,	,
nejvíce	nejvíce	k6eAd1	nejvíce
hlubokomyslný	hlubokomyslný	k2eAgMnSc1d1	hlubokomyslný
realista	realista	k1gMnSc1	realista
devatenáctého	devatenáctý	k4xOgNnSc2	devatenáctý
a	a	k8xC	a
počátku	počátek	k1gInSc6	počátek
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
v	v	k7c6	v
americkém	americký	k2eAgNnSc6d1	americké
umění	umění	k1gNnSc6	umění
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1883	[number]	k4	1883
se	se	k3xPyFc4	se
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
Pennsylvánské	pennsylvánský	k2eAgFnSc2d1	Pennsylvánská
akademie	akademie	k1gFnSc2	akademie
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
ve	v	k7c6	v
Philadelphii	Philadelphia	k1gFnSc6	Philadelphia
zapsala	zapsat	k5eAaPmAgFnS	zapsat
malířka	malířka	k1gFnSc1	malířka
a	a	k8xC	a
fotografka	fotografka	k1gFnSc1	fotografka
Eva	Eva	k1gFnSc1	Eva
Watson-Schütze	Watson-Schütze	k1gFnSc1	Watson-Schütze
<g/>
.	.	kIx.	.
</s>
<s>
Studovala	studovat	k5eAaImAgFnS	studovat
u	u	k7c2	u
Thomase	Thomas	k1gMnSc2	Thomas
Eakinse	Eakins	k1gMnSc2	Eakins
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
zajímala	zajímat	k5eAaImAgFnS	zajímat
o	o	k7c4	o
akvarely	akvarel	k1gInPc4	akvarel
a	a	k8xC	a
olejomalby	olejomalba	k1gFnPc4	olejomalba
<g/>
,	,	kIx,	,
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
ještě	ještě	k6eAd1	ještě
zajímala	zajímat	k5eAaImAgFnS	zajímat
o	o	k7c4	o
Eakinsovo	Eakinsův	k2eAgNnSc4d1	Eakinsův
fotografování	fotografování	k1gNnSc4	fotografování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotografie	fotografia	k1gFnPc4	fotografia
==	==	k?	==
</s>
</p>
<p>
<s>
Eakins	Eakins	k6eAd1	Eakins
byl	být	k5eAaImAgInS	být
označen	označit	k5eAaPmNgMnS	označit
jako	jako	k9	jako
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
"	"	kIx"	"
<g/>
přinesl	přinést	k5eAaPmAgInS	přinést
fotoaparát	fotoaparát	k1gInSc1	fotoaparát
do	do	k7c2	do
amerického	americký	k2eAgNnSc2d1	americké
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
studia	studio	k1gNnSc2	studio
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
studia	studio	k1gNnSc2	studio
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
dostal	dostat	k5eAaPmAgInS	dostat
možnost	možnost	k1gFnSc4	možnost
využívat	využívat	k5eAaPmF	využívat
fotografie	fotografia	k1gFnPc4	fotografia
u	u	k7c2	u
francouzských	francouzský	k2eAgMnPc2d1	francouzský
realistů	realista	k1gMnPc2	realista
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
využití	využití	k1gNnSc1	využití
fotografie	fotografia	k1gFnSc2	fotografia
bylo	být	k5eAaImAgNnS	být
stále	stále	k6eAd1	stále
odsuzováno	odsuzovat	k5eAaImNgNnS	odsuzovat
jako	jako	k8xS	jako
zjednodušení	zjednodušení	k1gNnSc1	zjednodušení
tradicionalistů	tradicionalista	k1gMnPc2	tradicionalista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdních	pozdní	k2eAgNnPc6d1	pozdní
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
fotografickými	fotografický	k2eAgFnPc7d1	fotografická
studiemi	studie	k1gFnPc7	studie
pohybu	pohyb	k1gInSc2	pohyb
Angličana	Angličan	k1gMnSc2	Angličan
Eadwearda	Eadweard	k1gMnSc2	Eadweard
Muybridge	Muybridg	k1gMnSc2	Muybridg
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
s	s	k7c7	s
jeho	jeho	k3xOp3gFnPc7	jeho
sekvencemi	sekvence	k1gFnPc7	sekvence
koňů	kůň	k1gMnPc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
o	o	k7c4	o
využití	využití	k1gNnSc4	využití
fotosekvencí	fotosekvence	k1gFnPc2	fotosekvence
pohybu	pohyb	k1gInSc2	pohyb
také	také	k9	také
zajímat	zajímat	k5eAaImF	zajímat
<g/>
.	.	kIx.	.
</s>
<s>
Vytvářel	vytvářet	k5eAaImAgInS	vytvářet
své	svůj	k3xOyFgFnPc4	svůj
vlastní	vlastní	k2eAgFnPc4d1	vlastní
pohybové	pohybový	k2eAgFnPc4d1	pohybová
studie	studie	k1gFnPc4	studie
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
zahrnující	zahrnující	k2eAgFnSc4d1	zahrnující
nahou	nahý	k2eAgFnSc4d1	nahá
postavu	postava	k1gFnSc4	postava
<g/>
.	.	kIx.	.
</s>
<s>
Vynalezl	vynaleznout	k5eAaPmAgMnS	vynaleznout
dokonce	dokonce	k9	dokonce
svou	svůj	k3xOyFgFnSc4	svůj
vlastní	vlastní	k2eAgFnSc4d1	vlastní
techniku	technika	k1gFnSc4	technika
pro	pro	k7c4	pro
snímání	snímání	k1gNnSc4	snímání
pohybu	pohyb	k1gInSc2	pohyb
na	na	k7c4	na
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
Muybridge	Muybridge	k1gInSc1	Muybridge
opíral	opírat	k5eAaImAgInS	opírat
o	o	k7c4	o
systém	systém	k1gInSc4	systém
řady	řada	k1gFnSc2	řada
fotoaparátů	fotoaparát	k1gInPc2	fotoaparát
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
snímaly	snímat	k5eAaImAgFnP	snímat
posloupnost	posloupnost	k1gFnSc4	posloupnost
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
,	,	kIx,	,
Eakins	Eakinsa	k1gFnPc2	Eakinsa
raději	rád	k6eAd2	rád
používal	používat	k5eAaImAgMnS	používat
jednu	jeden	k4xCgFnSc4	jeden
kameru	kamera	k1gFnSc4	kamera
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nasnímala	nasnímat	k5eAaPmAgFnS	nasnímat
řadu	řada	k1gFnSc4	řada
expozic	expozice	k1gFnPc2	expozice
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
negativ	negativ	k1gInSc4	negativ
<g/>
.	.	kIx.	.
</s>
<s>
Dobrým	dobrý	k2eAgInSc7d1	dobrý
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc4	jeho
malovaný	malovaný	k2eAgInSc4d1	malovaný
obraz	obraz	k1gInSc4	obraz
Květnové	květnový	k2eAgNnSc4d1	květnové
ráno	ráno	k1gNnSc4	ráno
v	v	k7c6	v
parku	park	k1gInSc6	park
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgNnSc6	který
se	se	k3xPyFc4	se
významně	významně	k6eAd1	významně
spoléhal	spoléhat	k5eAaImAgInS	spoléhat
na	na	k7c4	na
pohybové	pohybový	k2eAgFnPc4d1	pohybová
studie	studie	k1gFnPc4	studie
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zachycují	zachycovat	k5eAaImIp3nP	zachycovat
skutečnou	skutečný	k2eAgFnSc4d1	skutečná
chůzi	chůze	k1gFnSc4	chůze
čtyř	čtyři	k4xCgMnPc2	čtyři
koní	kůň	k1gMnPc2	kůň
táhnoucích	táhnoucí	k2eAgMnPc2d1	táhnoucí
kočár	kočár	k1gInSc4	kočár
patrona	patron	k1gMnSc2	patron
Fairmana	Fairman	k1gMnSc2	Fairman
Rogerse	Rogerse	k1gFnSc2	Rogerse
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
Eakins	Eakins	k1gInSc1	Eakins
získal	získat	k5eAaPmAgInS	získat
kameru	kamera	k1gFnSc4	kamera
<g/>
,	,	kIx,	,
namaloval	namalovat	k5eAaPmAgMnS	namalovat
několik	několik	k4yIc4	několik
obrazů	obraz	k1gInPc2	obraz
jako	jako	k8xC	jako
např.	např.	kA	např.
Mending	Mending	k1gInSc1	Mending
the	the	k?	the
Net	Net	k1gFnSc2	Net
(	(	kIx(	(
<g/>
1881	[number]	k4	1881
<g/>
)	)	kIx)	)
a	a	k8xC	a
Arcadia	Arcadium	k1gNnSc2	Arcadium
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
)	)	kIx)	)
u	u	k7c2	u
kterých	který	k3yIgFnPc2	který
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
byly	být	k5eAaImAgInP	být
celé	celý	k2eAgInPc1d1	celý
nebo	nebo	k8xC	nebo
alespoň	alespoň	k9	alespoň
část	část	k1gFnSc1	část
odvozeny	odvozen	k2eAgFnPc1d1	odvozena
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
fotografií	fotografia	k1gFnPc2	fotografia
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
postavy	postava	k1gFnPc1	postava
se	se	k3xPyFc4	se
zdají	zdát	k5eAaImIp3nP	zdát
být	být	k5eAaImF	být
podrobně	podrobně	k6eAd1	podrobně
a	a	k8xC	a
přesně	přesně	k6eAd1	přesně
přeneseny	přenesen	k2eAgInPc1d1	přenesen
z	z	k7c2	z
fotografie	fotografia	k1gFnSc2	fotografia
pomocí	pomocí	k7c2	pomocí
některých	některý	k3yIgNnPc2	některý
zařízení	zařízení	k1gNnPc2	zařízení
jako	jako	k8xS	jako
například	například	k6eAd1	například
laterna	laterna	k1gFnSc1	laterna
magica	magica	k1gFnSc1	magica
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
Eakins	Eakins	k1gInSc1	Eakins
snažil	snažit	k5eAaImAgInS	snažit
zakrýt	zakrýt	k5eAaPmF	zakrýt
olejovou	olejový	k2eAgFnSc7d1	olejová
barvou	barva	k1gFnSc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Eakins	Eakins	k1gInSc1	Eakins
své	svůj	k3xOyFgFnSc2	svůj
metody	metoda	k1gFnSc2	metoda
uplatňoval	uplatňovat	k5eAaImAgInS	uplatňovat
důkladně	důkladně	k6eAd1	důkladně
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
o	o	k7c4	o
přesnost	přesnost	k1gFnSc4	přesnost
a	a	k8xC	a
realismus	realismus	k1gInSc4	realismus
<g/>
.	.	kIx.	.
</s>
<s>
Takzvané	takzvaný	k2eAgNnSc1d1	takzvané
"	"	kIx"	"
<g/>
Série	série	k1gFnSc1	série
aktů	akt	k1gInPc2	akt
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yIgFnPc6	který
začal	začít	k5eAaPmAgInS	začít
pracovat	pracovat	k5eAaImF	pracovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1883	[number]	k4	1883
<g/>
,	,	kIx,	,
obsahovaly	obsahovat	k5eAaImAgFnP	obsahovat
fotografie	fotografia	k1gFnPc4	fotografia
nahých	nahý	k2eAgMnPc2d1	nahý
studentů	student	k1gMnPc2	student
i	i	k8xC	i
profesionálních	profesionální	k2eAgMnPc2d1	profesionální
modelů	model	k1gInPc2	model
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
zabírány	zabírat	k5eAaImNgInP	zabírat
z	z	k7c2	z
různých	různý	k2eAgInPc2d1	různý
úhlů	úhel	k1gInPc2	úhel
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
daly	dát	k5eAaPmAgInP	dát
použít	použít	k5eAaPmF	použít
pro	pro	k7c4	pro
studium	studium	k1gNnSc4	studium
lidské	lidský	k2eAgFnSc2d1	lidská
anatomie	anatomie	k1gFnSc2	anatomie
<g/>
,	,	kIx,	,
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
často	často	k6eAd1	často
používány	používat	k5eAaImNgInP	používat
a	a	k8xC	a
vystavovány	vystavovat	k5eAaImNgInP	vystavovat
během	během	k7c2	během
studia	studio	k1gNnSc2	studio
ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
pořizoval	pořizovat	k5eAaImAgMnS	pořizovat
buď	buď	k8xC	buď
v	v	k7c6	v
interiéru	interiér	k1gInSc6	interiér
nebo	nebo	k8xC	nebo
venku	venku	k6eAd1	venku
portréty	portrét	k1gInPc4	portrét
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
své	svůj	k3xOyFgFnSc2	svůj
manželky	manželka	k1gFnSc2	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
provokativní	provokativní	k2eAgMnSc1d1	provokativní
<g/>
,	,	kIx,	,
a	a	k8xC	a
jediné	jediný	k2eAgNnSc1d1	jediné
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
kombinovaly	kombinovat	k5eAaImAgFnP	kombinovat
muže	muž	k1gMnSc2	muž
i	i	k8xC	i
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
fotografie	fotografia	k1gFnPc1	fotografia
nahého	nahý	k2eAgMnSc2d1	nahý
Eakinse	Eakins	k1gMnSc2	Eakins
a	a	k8xC	a
ženského	ženský	k2eAgInSc2d1	ženský
modelu	model	k1gInSc2	model
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
při	při	k7c6	při
fotografování	fotografování	k1gNnSc6	fotografování
byli	být	k5eAaImAgMnP	být
přítomni	přítomen	k2eAgMnPc1d1	přítomen
svědci	svědek	k1gMnPc1	svědek
a	a	k8xC	a
pomocníci	pomocník	k1gMnPc1	pomocník
a	a	k8xC	a
pózy	póza	k1gFnPc1	póza
většinou	většinou	k6eAd1	většinou
čerpaly	čerpat	k5eAaImAgFnP	čerpat
z	z	k7c2	z
tradice	tradice	k1gFnSc2	tradice
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
jenom	jenom	k9	jenom
pouhé	pouhý	k2eAgNnSc4d1	pouhé
množství	množství	k1gNnSc4	množství
fotografií	fotografia	k1gFnPc2	fotografia
a	a	k8xC	a
Eakinsovo	Eakinsův	k2eAgNnSc1d1	Eakinsův
zjevné	zjevný	k2eAgNnSc1d1	zjevné
zobrazení	zobrazení	k1gNnSc1	zobrazení
na	na	k7c6	na
nich	on	k3xPp3gMnPc6	on
ohrožovalo	ohrožovat	k5eAaImAgNnS	ohrožovat
jeho	jeho	k3xOp3gFnSc4	jeho
postavení	postavení	k1gNnSc1	postavení
na	na	k7c4	na
Akademii	akademie	k1gFnSc4	akademie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Amelia	Amelius	k1gMnSc2	Amelius
Van	van	k1gInSc1	van
Buren	Buren	k2eAgInSc1d1	Buren
==	==	k?	==
</s>
</p>
<p>
<s>
Talent	talent	k1gInSc4	talent
Amelie	Amelie	k1gFnSc2	Amelie
Van	van	k1gInSc1	van
Burenové	Burenová	k1gFnSc2	Burenová
umožnil	umožnit	k5eAaPmAgInS	umožnit
absolvovat	absolvovat	k5eAaPmF	absolvovat
lekce	lekce	k1gFnPc4	lekce
výtvarného	výtvarný	k2eAgNnSc2d1	výtvarné
umění	umění	k1gNnSc2	umění
přímo	přímo	k6eAd1	přímo
u	u	k7c2	u
Eakinse	Eakinse	k1gFnSc2	Eakinse
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
kontroverzních	kontroverzní	k2eAgFnPc2d1	kontroverzní
lekcí	lekce	k1gFnPc2	lekce
s	s	k7c7	s
nahými	nahý	k2eAgInPc7d1	nahý
modely	model	k1gInPc7	model
<g/>
,	,	kIx,	,
mužskými	mužský	k2eAgInPc7d1	mužský
i	i	k9	i
ženskými	ženská	k1gFnPc7	ženská
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1885-1886	[number]	k4	1885-1886
se	se	k3xPyFc4	se
spiklo	spiknout	k5eAaPmAgNnS	spiknout
několik	několik	k4yIc1	několik
bývalých	bývalý	k2eAgMnPc2d1	bývalý
Eakinsových	Eakinsův	k2eAgMnPc2d1	Eakinsův
studentů	student	k1gMnPc2	student
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
Thomase	Thomas	k1gMnSc2	Thomas
Pollocka	Pollocka	k1gFnSc1	Pollocka
Anshutze	Anshutze	k1gFnSc1	Anshutze
a	a	k8xC	a
Colina	Colina	k1gFnSc1	Colina
Campbella	Campbell	k1gMnSc2	Campbell
Coopera	Cooper	k1gMnSc2	Cooper
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
Eakins	Eakins	k1gInSc1	Eakins
byl	být	k5eAaImAgInS	být
vyhozen	vyhodit	k5eAaPmNgInS	vyhodit
z	z	k7c2	z
Pennsylvánské	pennsylvánský	k2eAgFnSc2d1	Pennsylvánská
akademie	akademie	k1gFnSc2	akademie
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Tvrdili	tvrdit	k5eAaImAgMnP	tvrdit
vyšetřovacímu	vyšetřovací	k2eAgInSc3d1	vyšetřovací
výboru	výbor	k1gInSc3	výbor
<g/>
,	,	kIx,	,
že	že	k8xS	že
Eakins	Eakins	k1gInSc1	Eakins
využíval	využívat	k5eAaImAgInS	využívat
studentky	studentka	k1gFnPc4	studentka
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
Van	vana	k1gFnPc2	vana
Burenové	Burenová	k1gFnSc2	Burenová
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
nahé	nahý	k2eAgInPc1d1	nahý
modely	model	k1gInPc1	model
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
velmi	velmi	k6eAd1	velmi
pobuřujícím	pobuřující	k2eAgNnPc3d1	pobuřující
obviněním	obvinění	k1gNnPc3	obvinění
bylo	být	k5eAaImAgNnS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Van	van	k1gInSc4	van
Burenová	Burenová	k1gFnSc1	Burenová
položila	položit	k5eAaPmAgFnS	položit
Eakinsovi	Eakins	k1gMnSc3	Eakins
otázku	otázka	k1gFnSc4	otázka
týkající	týkající	k2eAgNnSc4d1	týkající
se	se	k3xPyFc4	se
pánevních	pánevní	k2eAgInPc2d1	pánevní
pohybů	pohyb	k1gInPc2	pohyb
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Eakins	Eakins	k1gInSc4	Eakins
reagoval	reagovat	k5eAaBmAgMnS	reagovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
sundal	sundat	k5eAaPmAgMnS	sundat
své	svůj	k3xOyFgFnPc4	svůj
kalhoty	kalhoty	k1gFnPc4	kalhoty
a	a	k8xC	a
pohyby	pohyb	k1gInPc4	pohyb
demonstroval	demonstrovat	k5eAaBmAgMnS	demonstrovat
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
trval	trvat	k5eAaImAgInS	trvat
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
epizoda	epizoda	k1gFnSc1	epizoda
měla	mít	k5eAaImAgFnS	mít
zcela	zcela	k6eAd1	zcela
profesionální	profesionální	k2eAgInSc4d1	profesionální
charakter	charakter	k1gInSc4	charakter
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
zotavení	zotavení	k1gNnSc6	zotavení
z	z	k7c2	z
neurastenie	neurastenie	k1gFnSc2	neurastenie
se	se	k3xPyFc4	se
Van	vana	k1gFnPc2	vana
Buren	Burno	k1gNnPc2	Burno
vrátila	vrátit	k5eAaPmAgFnS	vrátit
do	do	k7c2	do
Filadelfie	Filadelfie	k1gFnSc2	Filadelfie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
u	u	k7c2	u
Eakinse	Eakinse	k1gFnSc2	Eakinse
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
už	už	k6eAd1	už
ne	ne	k9	ne
na	na	k7c6	na
Pennsylvánské	pennsylvánský	k2eAgFnSc6d1	Pennsylvánská
akademii	akademie	k1gFnSc6	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Van	van	k1gInSc1	van
Buren	Burna	k1gFnPc2	Burna
a	a	k8xC	a
Eakins	Eakinsa	k1gFnPc2	Eakinsa
zůstali	zůstat	k5eAaPmAgMnP	zůstat
v	v	k7c6	v
úzkém	úzký	k2eAgInSc6d1	úzký
kontaktu	kontakt	k1gInSc6	kontakt
ještě	ještě	k9	ještě
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
poté	poté	k6eAd1	poté
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc4	tři
nebo	nebo	k8xC	nebo
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
po	po	k7c6	po
propuštění	propuštění	k1gNnSc6	propuštění
z	z	k7c2	z
akademie	akademie	k1gFnSc2	akademie
Eakins	Eakins	k1gInSc1	Eakins
namaloval	namalovat	k5eAaPmAgInS	namalovat
Van	van	k1gInSc4	van
Buren	Buren	k2eAgInSc4d1	Buren
na	na	k7c6	na
obraze	obraz	k1gInSc6	obraz
Miss	miss	k1gFnSc1	miss
Amelia	Amelia	k1gFnSc1	Amelia
Van	van	k1gInSc4	van
Buren	Buren	k1gInSc1	Buren
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ukázky	ukázka	k1gFnPc1	ukázka
studií	studio	k1gNnPc2	studio
a	a	k8xC	a
maleb	malba	k1gFnPc2	malba
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
všech	všecek	k3xTgInPc6	všecek
<g/>
,	,	kIx,	,
asi	asi	k9	asi
osmi	osm	k4xCc2	osm
stech	sto	k4xCgNnPc6	sto
fotografiích	fotografia	k1gFnPc6	fotografia
připisovaných	připisovaný	k2eAgFnPc2d1	připisovaná
Eakinsovi	Eakinsův	k2eAgMnPc1d1	Eakinsův
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
skupině	skupina	k1gFnSc3	skupina
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
studie	studie	k1gFnSc2	studie
postavy	postava	k1gFnSc2	postava
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
oblečených	oblečený	k2eAgInPc2d1	oblečený
nebo	nebo	k8xC	nebo
nahých	nahý	k2eAgInPc2d1	nahý
nebo	nebo	k8xC	nebo
portréty	portrét	k1gInPc4	portrét
<g/>
.	.	kIx.	.
</s>
<s>
Žádný	žádný	k3yNgMnSc1	žádný
jiný	jiný	k2eAgMnSc1d1	jiný
americký	americký	k2eAgMnSc1d1	americký
umělec-fotograf	umělecotograf	k1gMnSc1	umělec-fotograf
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
kromě	kromě	k7c2	kromě
Eakinse	Eakinse	k1gFnSc2	Eakinse
nevytvořil	vytvořit	k5eNaPmAgInS	vytvořit
srovnatelný	srovnatelný	k2eAgInSc1d1	srovnatelný
počet	počet	k1gInSc1	počet
takových	takový	k3xDgInPc2	takový
podobných	podobný	k2eAgInPc2d1	podobný
snímků	snímek	k1gInPc2	snímek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Pionýři	pionýr	k1gMnPc1	pionýr
vysokorychlostní	vysokorychlostní	k2eAgFnSc2d1	vysokorychlostní
fotografie	fotografia	k1gFnSc2	fotografia
==	==	k?	==
</s>
</p>
<p>
<s>
Eadweard	Eadweard	k1gInSc1	Eadweard
Muybridge	Muybridg	k1gFnSc2	Muybridg
(	(	kIx(	(
<g/>
1830	[number]	k4	1830
<g/>
–	–	k?	–
<g/>
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
–	–	k?	–
průkopník	průkopník	k1gMnSc1	průkopník
chronofotografie	chronofotografie	k1gFnSc2	chronofotografie
</s>
</p>
<p>
<s>
Étienne-Jules	Étienne-Jules	k1gInSc1	Étienne-Jules
Marey	Marea	k1gFnSc2	Marea
(	(	kIx(	(
<g/>
1860	[number]	k4	1860
<g/>
–	–	k?	–
<g/>
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
–	–	k?	–
francouzský	francouzský	k2eAgMnSc1d1	francouzský
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
fyziolog	fyziolog	k1gMnSc1	fyziolog
a	a	k8xC	a
fotograf	fotograf	k1gMnSc1	fotograf
série	série	k1gFnSc2	série
Muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
sesedá	sesedat	k5eAaPmIp3nS	sesedat
z	z	k7c2	z
kola	kolo	k1gNnSc2	kolo
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
–	–	k?	–
<g/>
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
–	–	k?	–
vynálezce	vynálezce	k1gMnSc1	vynálezce
chronofotografické	chronofotografický	k2eAgFnSc2d1	chronofotografický
pušky	puška	k1gFnSc2	puška
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
August	August	k1gMnSc1	August
(	(	kIx(	(
<g/>
1862	[number]	k4	1862
<g/>
–	–	k?	–
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
a	a	k8xC	a
Luis	Luisa	k1gFnPc2	Luisa
(	(	kIx(	(
<g/>
1864	[number]	k4	1864
<g/>
–	–	k?	–
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
Lumiè	Lumiè	k1gMnPc1	Lumiè
vyvinuli	vyvinout	k5eAaPmAgMnP	vyvinout
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
kinematograf	kinematograf	k1gInSc1	kinematograf
(	(	kIx(	(
<g/>
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
pohyblivý	pohyblivý	k2eAgMnSc1d1	pohyblivý
zapisovač	zapisovač	k1gMnSc1	zapisovač
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Harold	Harold	k1gMnSc1	Harold
Eugene	Eugen	k1gInSc5	Eugen
Edgerton	Edgerton	k1gInSc1	Edgerton
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
–	–	k?	–
<g/>
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
–	–	k?	–
významný	významný	k2eAgMnSc1d1	významný
americký	americký	k2eAgMnSc1d1	americký
inženýr	inženýr	k1gMnSc1	inženýr
<g/>
,	,	kIx,	,
vynálezce	vynálezce	k1gMnSc1	vynálezce
stroboskopu	stroboskop	k1gInSc2	stroboskop
a	a	k8xC	a
pionýr	pionýr	k1gMnSc1	pionýr
vysokorychlostní	vysokorychlostní	k2eAgFnSc2d1	vysokorychlostní
chronofotografie	chronofotografie	k1gFnSc2	chronofotografie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ottomar	Ottomar	k1gMnSc1	Ottomar
Anschütz	Anschütz	k1gMnSc1	Anschütz
(	(	kIx(	(
<g/>
1846	[number]	k4	1846
<g/>
–	–	k?	–
<g/>
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
–	–	k?	–
německý	německý	k2eAgMnSc1d1	německý
vynálezce	vynálezce	k1gMnSc1	vynálezce
a	a	k8xC	a
chronofotograf	chronofotograf	k1gMnSc1	chronofotograf
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ernst	Ernst	k1gMnSc1	Ernst
Kohlrausch	Kohlrausch	k1gMnSc1	Kohlrausch
(	(	kIx(	(
<g/>
1850	[number]	k4	1850
<g/>
–	–	k?	–
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
–	–	k?	–
sportovní	sportovní	k2eAgMnSc1d1	sportovní
vědec	vědec	k1gMnSc1	vědec
a	a	k8xC	a
pionýr	pionýr	k1gMnSc1	pionýr
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Anton	Anton	k1gMnSc1	Anton
Giulio	Giulio	k1gMnSc1	Giulio
Bragaglia	Bragaglia	k1gFnSc1	Bragaglia
–	–	k?	–
pionýr	pionýr	k1gInSc4	pionýr
italské	italský	k2eAgFnSc2d1	italská
futuristické	futuristický	k2eAgFnSc2d1	futuristická
fotografie	fotografia	k1gFnSc2	fotografia
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jean-Martin	Jean-Martin	k2eAgInSc1d1	Jean-Martin
Charcot	Charcot	k1gInSc1	Charcot
(	(	kIx(	(
<g/>
1825	[number]	k4	1825
<g/>
–	–	k?	–
<g/>
1893	[number]	k4	1893
<g/>
)	)	kIx)	)
–	–	k?	–
pařížský	pařížský	k2eAgMnSc1d1	pařížský
neurolog	neurolog	k1gMnSc1	neurolog
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Albert	Albert	k1gMnSc1	Albert
Londe	Lond	k1gInSc5	Lond
(	(	kIx(	(
<g/>
1858	[number]	k4	1858
–	–	k?	–
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
–	–	k?	–
francouzský	francouzský	k2eAgInSc4d1	francouzský
lékařský	lékařský	k2eAgInSc4d1	lékařský
chronofotograf	chronofotograf	k1gInSc4	chronofotograf
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Adams	Adams	k1gInSc1	Adams
<g/>
,	,	kIx,	,
Henry	henry	k1gInSc1	henry
<g/>
:	:	kIx,	:
Eakins	Eakins	k1gInSc1	Eakins
Revealed	Revealed	k1gInSc1	Revealed
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Secret	Secret	k1gInSc1	Secret
Life	Life	k1gFnSc1	Life
of	of	k?	of
an	an	k?	an
American	American	k1gMnSc1	American
Artist	Artist	k1gMnSc1	Artist
<g/>
.	.	kIx.	.
</s>
<s>
Oxford	Oxford	k1gInSc1	Oxford
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
-	-	kIx~	-
<g/>
515668	[number]	k4	515668
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Berger	Berger	k1gMnSc1	Berger
<g/>
,	,	kIx,	,
Martin	Martin	k1gMnSc1	Martin
<g/>
:	:	kIx,	:
Man	Man	k1gMnSc1	Man
Made	Mad	k1gMnSc2	Mad
<g/>
:	:	kIx,	:
Thomas	Thomas	k1gMnSc1	Thomas
Eakins	Eakinsa	k1gFnPc2	Eakinsa
and	and	k?	and
the	the	k?	the
Construction	Construction	k1gInSc1	Construction
of	of	k?	of
Guilded	Guilded	k1gInSc1	Guilded
Age	Age	k1gMnSc1	Age
Manhood	Manhood	k1gInSc1	Manhood
<g/>
.	.	kIx.	.
</s>
<s>
University	universita	k1gFnPc1	universita
of	of	k?	of
California	Californium	k1gNnSc2	Californium
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
520	[number]	k4	520
<g/>
-	-	kIx~	-
<g/>
22209	[number]	k4	22209
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Canaday	Canadaa	k1gFnPc1	Canadaa
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
<g/>
:	:	kIx,	:
Thomas	Thomas	k1gMnSc1	Thomas
Eakins	Eakinsa	k1gFnPc2	Eakinsa
<g/>
;	;	kIx,	;
Familiar	Familiar	k1gInSc1	Familiar
truths	truths	k1gInSc1	truths
in	in	k?	in
clear	clear	k1gInSc1	clear
and	and	k?	and
beautiful	beautiful	k1gInSc1	beautiful
language	language	k1gFnSc1	language
<g/>
,	,	kIx,	,
Horizon	Horizon	k1gInSc1	Horizon
<g/>
.	.	kIx.	.
</s>
<s>
Volume	volum	k1gInSc5	volum
VI	VI	kA	VI
<g/>
,	,	kIx,	,
Number	Number	k1gInSc1	Number
4	[number]	k4	4
<g/>
,	,	kIx,	,
Autumn	Autumn	k1gInSc1	Autumn
1964	[number]	k4	1964
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Doyle	Doyle	k1gFnSc1	Doyle
<g/>
,	,	kIx,	,
Jennifer	Jennifer	k1gInSc1	Jennifer
<g/>
:	:	kIx,	:
Sex	sex	k1gInSc1	sex
<g/>
,	,	kIx,	,
Scandal	Scandal	k1gFnSc1	Scandal
<g/>
,	,	kIx,	,
and	and	k?	and
'	'	kIx"	'
<g/>
The	The	k1gMnSc1	The
Gross	Gross	k1gMnSc1	Gross
Clinic	Clinice	k1gFnPc2	Clinice
<g/>
.	.	kIx.	.
</s>
<s>
Representations	Representations	k1gInSc1	Representations
68	[number]	k4	68
(	(	kIx(	(
<g/>
Fall	Fall	k1gInSc1	Fall
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
33	[number]	k4	33
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Goodrich	Goodrich	k1gMnSc1	Goodrich
<g/>
,	,	kIx,	,
Lloyd	Lloyd	k1gMnSc1	Lloyd
<g/>
:	:	kIx,	:
Thomas	Thomas	k1gMnSc1	Thomas
Eakins	Eakinsa	k1gFnPc2	Eakinsa
<g/>
.	.	kIx.	.
</s>
<s>
Harvard	Harvard	k1gInSc1	Harvard
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0-674-88490-6	[number]	k4	0-674-88490-6
</s>
</p>
<p>
<s>
Homer	Homer	k1gInSc1	Homer
<g/>
,	,	kIx,	,
William	William	k1gInSc1	William
Innes	Innes	k1gInSc1	Innes
<g/>
:	:	kIx,	:
Thomas	Thomas	k1gMnSc1	Thomas
Eakins	Eakinsa	k1gFnPc2	Eakinsa
<g/>
:	:	kIx,	:
His	his	k1gNnSc1	his
Life	Lif	k1gInSc2	Lif
and	and	k?	and
Art	Art	k1gMnSc1	Art
<g/>
.	.	kIx.	.
</s>
<s>
Abbeville	Abbeville	k1gFnSc1	Abbeville
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
1-55859-281-4	[number]	k4	1-55859-281-4
</s>
</p>
<p>
<s>
Johns	Johns	k1gInSc1	Johns
<g/>
,	,	kIx,	,
Elizabeth	Elizabeth	k1gFnSc1	Elizabeth
<g/>
:	:	kIx,	:
Thomas	Thomas	k1gMnSc1	Thomas
Eakins	Eakinsa	k1gFnPc2	Eakinsa
<g/>
.	.	kIx.	.
</s>
<s>
Princeton	Princeton	k1gInSc1	Princeton
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0-691-00288-6	[number]	k4	0-691-00288-6
</s>
</p>
<p>
<s>
Kirkpatrick	Kirkpatrick	k1gInSc1	Kirkpatrick
<g/>
,	,	kIx,	,
Sidney	Sidney	k1gInPc1	Sidney
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Revenge	Reveng	k1gFnSc2	Reveng
of	of	k?	of
Thomas	Thomas	k1gMnSc1	Thomas
Eakins	Eakins	k1gInSc1	Eakins
<g/>
.	.	kIx.	.
</s>
<s>
Yale	Yale	k1gFnSc1	Yale
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
300	[number]	k4	300
<g/>
-	-	kIx~	-
<g/>
10855	[number]	k4	10855
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lubin	Lubin	k1gMnSc1	Lubin
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
<g/>
:	:	kIx,	:
Acts	Acts	k1gInSc1	Acts
of	of	k?	of
Portrayal	Portrayal	k1gInSc1	Portrayal
<g/>
:	:	kIx,	:
Eakins	Eakins	k1gInSc1	Eakins
<g/>
,	,	kIx,	,
Sargeant	Sargeant	k1gMnSc1	Sargeant
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
<g/>
.	.	kIx.	.
</s>
<s>
Yale	Yale	k1gFnSc1	Yale
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0-300-03213-7	[number]	k4	0-300-03213-7
</s>
</p>
<p>
<s>
Sewell	Sewell	k1gMnSc1	Sewell
<g/>
,	,	kIx,	,
Darrel	Darrel	k1gMnSc1	Darrel
<g/>
:	:	kIx,	:
Thomas	Thomas	k1gMnSc1	Thomas
Eakins	Eakinsa	k1gFnPc2	Eakinsa
<g/>
:	:	kIx,	:
Artist	Artist	k1gInSc1	Artist
of	of	k?	of
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
<g/>
.	.	kIx.	.
</s>
<s>
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Art	Art	k1gMnSc1	Art
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0-87633-047-2	[number]	k4	0-87633-047-2
</s>
</p>
<p>
<s>
Updike	Updike	k1gFnSc1	Updike
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
<g/>
:	:	kIx,	:
Still	Still	k1gInSc1	Still
Looking	Looking	k1gInSc4	Looking
<g/>
:	:	kIx,	:
Essays	Essays	k1gInSc4	Essays
on	on	k3xPp3gMnSc1	on
American	American	k1gMnSc1	American
Art	Art	k1gMnSc1	Art
<g/>
.	.	kIx.	.
</s>
<s>
Alfred	Alfred	k1gMnSc1	Alfred
A.	A.	kA	A.
Knopf	Knopf	k1gMnSc1	Knopf
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
1-4000-4418-9	[number]	k4	1-4000-4418-9
</s>
</p>
<p>
<s>
Weinberg	Weinberg	k1gMnSc1	Weinberg
<g/>
,	,	kIx,	,
H.	H.	kA	H.
Barbara	Barbara	k1gFnSc1	Barbara
<g/>
:	:	kIx,	:
Thomas	Thomas	k1gMnSc1	Thomas
Eakins	Eakinsa	k1gFnPc2	Eakinsa
and	and	k?	and
the	the	k?	the
Metropolitan	metropolitan	k1gInSc1	metropolitan
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Art	Art	k1gMnSc1	Art
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Metropolitan	metropolitan	k1gInSc1	metropolitan
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Art	Art	k1gMnSc1	Art
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Publication	Publication	k1gInSc1	Publication
no	no	k9	no
<g/>
:	:	kIx,	:
885-660	[number]	k4	885-660
</s>
</p>
<p>
<s>
Werbel	Werbel	k1gMnSc1	Werbel
<g/>
,	,	kIx,	,
Amy	Amy	k1gMnSc1	Amy
<g/>
:	:	kIx,	:
Thomas	Thomas	k1gMnSc1	Thomas
Eakins	Eakinsa	k1gFnPc2	Eakinsa
<g/>
:	:	kIx,	:
Art	Art	k1gMnSc5	Art
<g/>
,	,	kIx,	,
Medicine	Medicin	k1gMnSc5	Medicin
<g/>
,	,	kIx,	,
and	and	k?	and
Sexuality	sexualita	k1gFnSc2	sexualita
in	in	k?	in
Nineteenth-Century	Nineteenth-Centura	k1gFnSc2	Nineteenth-Centura
Philadelphia	Philadelphia	k1gFnSc1	Philadelphia
<g/>
.	.	kIx.	.
</s>
<s>
Yale	Yale	k1gFnSc1	Yale
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
300	[number]	k4	300
<g/>
-	-	kIx~	-
<g/>
11655	[number]	k4	11655
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
The	The	k?	The
Paris	Paris	k1gMnSc1	Paris
Letters	Lettersa	k1gFnPc2	Lettersa
of	of	k?	of
Thomas	Thomas	k1gMnSc1	Thomas
Eakins	Eakins	k1gInSc1	Eakins
<g/>
.	.	kIx.	.
</s>
<s>
Edited	Edited	k1gInSc1	Edited
by	by	kYmCp3nS	by
William	William	k1gInSc4	William
Innes	Innesa	k1gFnPc2	Innesa
Homer	Homra	k1gFnPc2	Homra
<g/>
.	.	kIx.	.
</s>
<s>
Princeton	Princeton	k1gInSc1	Princeton
<g/>
,	,	kIx,	,
PUP	pup	k1gInSc1	pup
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-0-691-13808-4	[number]	k4	978-0-691-13808-4
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Dům	dům	k1gInSc1	dům
Thomase	Thomas	k1gMnSc2	Thomas
Eakinse	Eakins	k1gMnSc2	Eakins
</s>
</p>
<p>
<s>
Chronologie	chronologie	k1gFnSc1	chronologie
fotografie	fotografia	k1gFnSc2	fotografia
</s>
</p>
<p>
<s>
Pionýři	pionýr	k1gMnPc1	pionýr
fotografické	fotografický	k2eAgFnSc2d1	fotografická
techniky	technika	k1gFnSc2	technika
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Thomas	Thomas	k1gMnSc1	Thomas
Eakins	Eakinsa	k1gFnPc2	Eakinsa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
www.ThomasEakins.org	www.ThomasEakins.org	k1gInSc1	www.ThomasEakins.org
148	[number]	k4	148
works	works	k6eAd1	works
by	by	k9	by
Thomas	Thomas	k1gMnSc1	Thomas
Eakins	Eakinsa	k1gFnPc2	Eakinsa
</s>
</p>
<p>
<s>
Thomas	Thomas	k1gMnSc1	Thomas
Eakins	Eakinsa	k1gFnPc2	Eakinsa
Exhibition	Exhibition	k1gInSc1	Exhibition
at	at	k?	at
The	The	k1gFnSc1	The
Metropolitan	metropolitan	k1gInSc1	metropolitan
Museum	museum	k1gNnSc1	museum
of	of	k?	of
Art	Art	k1gMnSc1	Art
</s>
</p>
<p>
<s>
Thomas	Thomas	k1gMnSc1	Thomas
Eakins	Eakinsa	k1gFnPc2	Eakinsa
letters	letters	k1gInSc4	letters
online	onlinout	k5eAaPmIp3nS	onlinout
at	at	k?	at
the	the	k?	the
Smithsonian	Smithsonian	k1gMnSc1	Smithsonian
Archives	Archives	k1gMnSc1	Archives
of	of	k?	of
American	American	k1gMnSc1	American
Art	Art	k1gMnSc1	Art
</s>
</p>
