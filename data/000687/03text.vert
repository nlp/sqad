<s>
János	János	k1gMnSc1	János
Áder	Áder	k1gMnSc1	Áder
(	(	kIx(	(
<g/>
*	*	kIx~	*
9	[number]	k4	9
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
Csorna	Csorna	k1gFnSc1	Csorna
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
maďarský	maďarský	k2eAgMnSc1d1	maďarský
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
europoslanec	europoslanec	k1gMnSc1	europoslanec
<g/>
,	,	kIx,	,
dlouholetý	dlouholetý	k2eAgMnSc1d1	dlouholetý
člen	člen	k1gMnSc1	člen
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
předseda	předseda	k1gMnSc1	předseda
pravicové	pravicový	k2eAgFnSc2d1	pravicová
strany	strana	k1gFnSc2	strana
Fidesz	Fidesza	k1gFnPc2	Fidesza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1998	[number]	k4	1998
<g/>
-	-	kIx~	-
<g/>
2002	[number]	k4	2002
byl	být	k5eAaImAgMnS	být
předsedou	předseda	k1gMnSc7	předseda
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
prezidentem	prezident	k1gMnSc7	prezident
Maďarska	Maďarsko	k1gNnSc2	Maďarsko
<g/>
.	.	kIx.	.
</s>
<s>
Úřadu	úřad	k1gInSc2	úřad
se	se	k3xPyFc4	se
oficiálně	oficiálně	k6eAd1	oficiálně
ujal	ujmout	k5eAaPmAgInS	ujmout
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
János	János	k1gMnSc1	János
Áder	Áder	k1gMnSc1	Áder
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Csorna	Csorno	k1gNnSc2	Csorno
v	v	k7c6	v
župě	župa	k1gFnSc6	župa
Győr-Moson-Sopron	Győr-Moson-Sopron	k1gInSc4	Győr-Moson-Sopron
na	na	k7c6	na
západě	západ	k1gInSc6	západ
tehdejší	tehdejší	k2eAgFnSc2d1	tehdejší
Maďarské	maďarský	k2eAgFnSc2d1	maďarská
lidové	lidový	k2eAgFnSc2d1	lidová
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
tamní	tamní	k2eAgFnSc4d1	tamní
základní	základní	k2eAgFnSc4d1	základní
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
na	na	k7c4	na
Révai	Réva	k1gFnPc4	Réva
Miklós	Miklós	k1gInSc4	Miklós
Gimnázium	Gimnázium	k1gNnSc1	Gimnázium
v	v	k7c6	v
Győru	Győr	k1gInSc6	Győr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
maturoval	maturovat	k5eAaBmAgMnS	maturovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
na	na	k7c4	na
Univerzitu	univerzita	k1gFnSc4	univerzita
Loránda	Loránda	k1gFnSc1	Loránda
Eötvöse	Eötvöse	k1gFnSc1	Eötvöse
fakultu	fakulta	k1gFnSc4	fakulta
státu	stát	k1gInSc2	stát
a	a	k8xC	a
práva	právo	k1gNnSc2	právo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
úspěšně	úspěšně	k6eAd1	úspěšně
promoval	promovat	k5eAaBmAgMnS	promovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1985	[number]	k4	1985
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
pracoval	pracovat	k5eAaImAgMnS	pracovat
ve	v	k7c6	v
výzkumném	výzkumný	k2eAgInSc6d1	výzkumný
sociologickém	sociologický	k2eAgInSc6d1	sociologický
ústavu	ústav	k1gInSc6	ústav
pro	pro	k7c4	pro
MTA	mta	k0	mta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
politice	politika	k1gFnSc6	politika
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
János	János	k1gMnSc1	János
Áder	Áder	k1gMnSc1	Áder
pohybovat	pohybovat	k5eAaImF	pohybovat
ihned	ihned	k6eAd1	ihned
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
pádu	pád	k1gInSc2	pád
komunismu	komunismus	k1gInSc2	komunismus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
se	se	k3xPyFc4	se
přidal	přidat	k5eAaPmAgMnS	přidat
k	k	k7c3	k
Fidesz	Fidesza	k1gFnPc2	Fidesza
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
László	László	k1gMnSc7	László
Kövérem	Kövér	k1gMnSc7	Kövér
a	a	k8xC	a
Viktorem	Viktor	k1gMnSc7	Viktor
Orbánem	Orbán	k1gMnSc7	Orbán
se	se	k3xPyFc4	se
jako	jako	k9	jako
zástupce	zástupce	k1gMnSc1	zástupce
této	tento	k3xDgFnSc2	tento
opoziční	opoziční	k2eAgFnSc2d1	opoziční
strany	strana	k1gFnSc2	strana
účastnil	účastnit	k5eAaImAgMnS	účastnit
jednání	jednání	k1gNnSc4	jednání
u	u	k7c2	u
Národního	národní	k2eAgInSc2d1	národní
kulatého	kulatý	k2eAgInSc2d1	kulatý
stolu	stol	k1gInSc2	stol
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
diskutovala	diskutovat	k5eAaImAgFnS	diskutovat
demokratická	demokratický	k2eAgFnSc1d1	demokratická
opozice	opozice	k1gFnSc1	opozice
s	s	k7c7	s
komunistickou	komunistický	k2eAgFnSc7d1	komunistická
MSZMP	MSZMP	kA	MSZMP
o	o	k7c6	o
přechodu	přechod	k1gInSc6	přechod
k	k	k7c3	k
demokratickému	demokratický	k2eAgNnSc3d1	demokratické
státnímu	státní	k2eAgNnSc3d1	státní
zřízení	zřízení	k1gNnSc3	zřízení
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
prvních	první	k4xOgFnPc2	první
svobodných	svobodný	k2eAgFnPc2d1	svobodná
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
byl	být	k5eAaImAgMnS	být
poslancem	poslanec	k1gMnSc7	poslanec
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
v	v	k7c6	v
každých	každý	k3xTgFnPc6	každý
následujících	následující	k2eAgFnPc6d1	následující
volbách	volba	k1gFnPc6	volba
byl	být	k5eAaImAgInS	být
znovuzvolen	znovuzvolit	k5eAaPmNgInS	znovuzvolit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pro	pro	k7c4	pro
Fidesz	Fidesza	k1gFnPc2	Fidesza
vítězných	vítězný	k2eAgFnPc6d1	vítězná
volbách	volba	k1gFnPc6	volba
1998	[number]	k4	1998
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
předsedou	předseda	k1gMnSc7	předseda
Maďarského	maďarský	k2eAgInSc2d1	maďarský
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
Zoltána	Zoltán	k1gMnSc2	Zoltán
Gála	Gála	k1gMnSc1	Gála
(	(	kIx(	(
<g/>
MSZP	MSZP	kA	MSZP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Áder	Áder	k1gInSc1	Áder
tuto	tento	k3xDgFnSc4	tento
funkci	funkce	k1gFnSc4	funkce
vykonával	vykonávat	k5eAaImAgInS	vykonávat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ho	on	k3xPp3gNnSc4	on
po	po	k7c6	po
méně	málo	k6eAd2	málo
úspěšných	úspěšný	k2eAgFnPc6d1	úspěšná
volbách	volba	k1gFnPc6	volba
nahradila	nahradit	k5eAaPmAgFnS	nahradit
Katalin	Katalin	k1gInSc4	Katalin
Szili	Szil	k1gMnPc1	Szil
(	(	kIx(	(
<g/>
MSZP	MSZP	kA	MSZP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rezignaci	rezignace	k1gFnSc6	rezignace
Zoltána	Zoltán	k1gMnSc2	Zoltán
Pokorniho	Pokorni	k1gMnSc2	Pokorni
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
převzal	převzít	k5eAaPmAgInS	převzít
Áder	Áder	k1gInSc4	Áder
funkci	funkce	k1gFnSc4	funkce
předsedy	předseda	k1gMnSc2	předseda
strany	strana	k1gFnSc2	strana
Fidesz-MPP	Fidesz-MPP	k1gFnSc2	Fidesz-MPP
<g/>
,	,	kIx,	,
funkci	funkce	k1gFnSc4	funkce
vykonával	vykonávat	k5eAaImAgInS	vykonávat
pouze	pouze	k6eAd1	pouze
do	do	k7c2	do
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
volbou	volba	k1gFnSc7	volba
předsedou	předseda	k1gMnSc7	předseda
strany	strana	k1gFnSc2	strana
opět	opět	k6eAd1	opět
stal	stát	k5eAaPmAgMnS	stát
Viktor	Viktor	k1gMnSc1	Viktor
Orbán	Orbán	k1gMnSc1	Orbán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
János	János	k1gMnSc1	János
Áder	Áder	k1gMnSc1	Áder
bude	být	k5eAaImBp3nS	být
kandidovat	kandidovat	k5eAaImF	kandidovat
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
stranu	strana	k1gFnSc4	strana
do	do	k7c2	do
Evropského	evropský	k2eAgInSc2d1	evropský
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
evropských	evropský	k2eAgFnPc6d1	Evropská
volbách	volba	k1gFnPc6	volba
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
<g/>
,	,	kIx,	,
Fidesz	Fidesz	k1gInSc1	Fidesz
získal	získat	k5eAaPmAgInS	získat
dokonce	dokonce	k9	dokonce
14	[number]	k4	14
křesel	křeslo	k1gNnPc2	křeslo
<g/>
,	,	kIx,	,
a	a	k8xC	a
Áder	Áder	k1gInSc1	Áder
má	mít	k5eAaImIp3nS	mít
tak	tak	k6eAd1	tak
mandát	mandát	k1gInSc4	mandát
europoslance	europoslance	k1gFnSc2	europoslance
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Prezidentské	prezidentský	k2eAgFnSc2d1	prezidentská
volby	volba	k1gFnSc2	volba
v	v	k7c6	v
Maďarsku	Maďarsko	k1gNnSc6	Maďarsko
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rezignaci	rezignace	k1gFnSc6	rezignace
prezidenta	prezident	k1gMnSc2	prezident
republiky	republika	k1gFnSc2	republika
Pála	Pála	k1gMnSc1	Pála
Schmitta	Schmitta	k1gMnSc1	Schmitta
kvůli	kvůli	k7c3	kvůli
jeho	jeho	k3xOp3gFnSc3	jeho
plagiátorské	plagiátorský	k2eAgFnSc3d1	plagiátorská
aféře	aféra	k1gFnSc3	aféra
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2012	[number]	k4	2012
se	se	k3xPyFc4	se
Áderovo	Áderův	k2eAgNnSc1d1	Áderův
jméno	jméno	k1gNnSc1	jméno
objevilo	objevit	k5eAaPmAgNnS	objevit
jako	jako	k9	jako
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
možných	možný	k2eAgMnPc2d1	možný
potenciálních	potenciální	k2eAgMnPc2d1	potenciální
kandidátů	kandidát	k1gMnPc2	kandidát
na	na	k7c4	na
post	post	k1gInSc4	post
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2012	[number]	k4	2012
premiér	premiér	k1gMnSc1	premiér
Viktor	Viktor	k1gMnSc1	Viktor
Orbán	Orbán	k1gMnSc1	Orbán
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Áder	Áder	k1gMnSc1	Áder
je	být	k5eAaImIp3nS	být
oficiálním	oficiální	k2eAgMnSc7d1	oficiální
kandidátem	kandidát	k1gMnSc7	kandidát
vládní	vládní	k2eAgFnSc2d1	vládní
strany	strana	k1gFnSc2	strana
Fidesz	Fidesza	k1gFnPc2	Fidesza
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
má	mít	k5eAaImIp3nS	mít
vládní	vládní	k2eAgFnSc1d1	vládní
koalice	koalice	k1gFnSc1	koalice
Fidesz	Fidesz	k1gInSc1	Fidesz
<g/>
–	–	k?	–
<g/>
KDNP	KDNP	kA	KDNP
od	od	k7c2	od
voleb	volba	k1gFnPc2	volba
2010	[number]	k4	2010
v	v	k7c6	v
parlamentu	parlament	k1gInSc6	parlament
dvoutřetinovou	dvoutřetinový	k2eAgFnSc4d1	dvoutřetinová
většinou	většina	k1gFnSc7	většina
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
zvolen	zvolit	k5eAaPmNgMnS	zvolit
prezidentem	prezident	k1gMnSc7	prezident
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Úřadu	úřad	k1gInSc2	úřad
se	se	k3xPyFc4	se
oficiálně	oficiálně	k6eAd1	oficiálně
ujal	ujmout	k5eAaPmAgInS	ujmout
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Áder	Áder	k1gInSc1	Áder
je	být	k5eAaImIp3nS	být
ženatý	ženatý	k2eAgInSc1d1	ženatý
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
Anita	Anita	k1gMnSc1	Anita
Herczegh	Herczegh	k1gMnSc1	Herczegh
pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k9	jako
soudkyně	soudkyně	k1gFnSc1	soudkyně
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc4	její
otec	otec	k1gMnSc1	otec
Géza	géz	k1gMnSc2	géz
Herczegh	Herczegh	k1gMnSc1	Herczegh
(	(	kIx(	(
<g/>
1928	[number]	k4	1928
<g/>
-	-	kIx~	-
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
ve	v	k7c6	v
Velkých	velký	k2eAgInPc6d1	velký
Kapušanech	Kapušan	k1gInPc6	Kapušan
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
Slovensku	Slovensko	k1gNnSc6	Slovensko
a	a	k8xC	a
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1993	[number]	k4	1993
<g/>
-	-	kIx~	-
<g/>
2003	[number]	k4	2003
byl	být	k5eAaImAgMnS	být
soudcem	soudce	k1gMnSc7	soudce
Mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
soudního	soudní	k2eAgInSc2d1	soudní
dvora	dvůr	k1gInSc2	dvůr
v	v	k7c6	v
Haagu	Haag	k1gInSc6	Haag
<g/>
.	.	kIx.	.
</s>
