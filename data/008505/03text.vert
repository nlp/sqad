<p>
<s>
Haskell	Haskell	k1gInSc1	Haskell
je	být	k5eAaImIp3nS	být
standardizovaný	standardizovaný	k2eAgInSc1d1	standardizovaný
funkcionální	funkcionální	k2eAgInSc4d1	funkcionální
programovací	programovací	k2eAgInSc4d1	programovací
jazyk	jazyk	k1gInSc4	jazyk
používající	používající	k2eAgNnSc4d1	používající
líné	líný	k2eAgNnSc4d1	líné
vyhodnocování	vyhodnocování	k1gNnSc4	vyhodnocování
<g/>
,	,	kIx,	,
pojmenovaný	pojmenovaný	k2eAgInSc4d1	pojmenovaný
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
logika	logicus	k1gMnSc2	logicus
Haskella	Haskell	k1gMnSc2	Haskell
Curryho	Curry	k1gMnSc2	Curry
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
<g/>
,	,	kIx,	,
především	především	k9	především
díky	díky	k7c3	díky
svým	svůj	k3xOyFgFnPc3	svůj
implementacím	implementace	k1gFnPc3	implementace
Hugs	Hugsa	k1gFnPc2	Hugsa
a	a	k8xC	a
GHC	GHC	kA	GHC
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Haskell	Haskell	k1gInSc1	Haskell
je	být	k5eAaImIp3nS	být
jazyk	jazyk	k1gInSc1	jazyk
dodržující	dodržující	k2eAgFnSc1d1	dodržující
referenční	referenční	k2eAgFnSc1d1	referenční
transparentnost	transparentnost	k1gFnSc1	transparentnost
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
zjednodušeně	zjednodušeně	k6eAd1	zjednodušeně
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tentýž	týž	k3xTgInSc1	týž
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
<g/>
)	)	kIx)	)
<g/>
výraz	výraz	k1gInSc1	výraz
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
jakémkoliv	jakýkoliv	k3yIgNnSc6	jakýkoliv
místě	místo	k1gNnSc6	místo
v	v	k7c6	v
programu	program	k1gInSc6	program
stejnou	stejný	k2eAgFnSc4d1	stejná
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
vlastnosti	vlastnost	k1gFnPc4	vlastnost
tohoto	tento	k3xDgInSc2	tento
jazyka	jazyk	k1gInSc2	jazyk
patří	patřit	k5eAaImIp3nS	patřit
přísné	přísný	k2eAgNnSc4d1	přísné
typování	typování	k1gNnSc4	typování
proměnných	proměnná	k1gFnPc2	proměnná
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
programátorovi	programátor	k1gMnSc3	programátor
může	moct	k5eAaImIp3nS	moct
usnadnit	usnadnit	k5eAaPmF	usnadnit
odhalování	odhalování	k1gNnSc1	odhalování
chyb	chyba	k1gFnPc2	chyba
v	v	k7c6	v
programu	program	k1gInSc6	program
<g/>
.	.	kIx.	.
</s>
<s>
Haskell	Haskell	k1gInSc1	Haskell
plně	plně	k6eAd1	plně
podporuje	podporovat	k5eAaImIp3nS	podporovat
práci	práce	k1gFnSc4	práce
se	s	k7c7	s
soubory	soubor	k1gInPc7	soubor
i	i	k8xC	i
standardními	standardní	k2eAgInPc7d1	standardní
vstupy	vstup	k1gInPc7	vstup
a	a	k8xC	a
výstupy	výstup	k1gInPc7	výstup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
na	na	k7c6	na
konci	konec	k1gInSc6	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
skupinou	skupina	k1gFnSc7	skupina
vědců	vědec	k1gMnPc2	vědec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
vytvořit	vytvořit	k5eAaPmF	vytvořit
otevřený	otevřený	k2eAgInSc4d1	otevřený
standard	standard	k1gInSc4	standard
funkcionálního	funkcionální	k2eAgInSc2d1	funkcionální
programovacího	programovací	k2eAgInSc2d1	programovací
jazyka	jazyk	k1gInSc2	jazyk
s	s	k7c7	s
moderními	moderní	k2eAgFnPc7d1	moderní
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
rozvíjen	rozvíjet	k5eAaImNgMnS	rozvíjet
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
akademické	akademický	k2eAgFnSc6d1	akademická
sféře	sféra	k1gFnSc6	sféra
(	(	kIx(	(
<g/>
motto	motto	k1gNnSc1	motto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
avoid	avoid	k1gInSc1	avoid
success	success	k1gInSc1	success
at	at	k?	at
all	all	k?	all
costs	costs	k1gInSc1	costs
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
"	"	kIx"	"
<g/>
vyhnout	vyhnout	k5eAaPmF	vyhnout
se	se	k3xPyFc4	se
úspěchu	úspěch	k1gInSc3	úspěch
za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
cenu	cena	k1gFnSc4	cena
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
začal	začít	k5eAaPmAgInS	začít
být	být	k5eAaImF	být
známější	známý	k2eAgInSc1d2	známější
i	i	k9	i
mimo	mimo	k7c4	mimo
ni	on	k3xPp3gFnSc4	on
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
díky	díky	k7c3	díky
online	onlinout	k5eAaPmIp3nS	onlinout
komunitě	komunita	k1gFnSc3	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc7d1	poslední
ustálenou	ustálený	k2eAgFnSc7d1	ustálená
verzí	verze	k1gFnSc7	verze
je	být	k5eAaImIp3nS	být
revize	revize	k1gFnSc1	revize
Haskell	Haskell	k1gInSc1	Haskell
98	[number]	k4	98
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
definuje	definovat	k5eAaBmIp3nS	definovat
minimální	minimální	k2eAgInSc1d1	minimální
a	a	k8xC	a
přenositelný	přenositelný	k2eAgInSc1d1	přenositelný
standard	standard	k1gInSc1	standard
jazyka	jazyk	k1gInSc2	jazyk
využitelný	využitelný	k2eAgInSc1d1	využitelný
k	k	k7c3	k
výuce	výuka	k1gFnSc3	výuka
nebo	nebo	k8xC	nebo
jako	jako	k8xC	jako
základ	základ	k1gInSc4	základ
dalších	další	k2eAgNnPc2d1	další
rozšíření	rozšíření	k1gNnPc2	rozšíření
<g/>
.	.	kIx.	.
</s>
<s>
Plánované	plánovaný	k2eAgNnSc1d1	plánované
zařazení	zařazení	k1gNnSc1	zařazení
některých	některý	k3yIgFnPc6	některý
rozšíření	rozšíření	k1gNnSc6	rozšíření
je	být	k5eAaImIp3nS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
jako	jako	k8xS	jako
Haskell	Haskell	k1gInSc1	Haskell
<g/>
'	'	kIx"	'
(	(	kIx(	(
<g/>
Haskell	Haskell	k1gInSc1	Haskell
Prime	prim	k1gInSc5	prim
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
v	v	k7c6	v
širším	široký	k2eAgNnSc6d2	širší
pojetí	pojetí	k1gNnSc6	pojetí
==	==	k?	==
</s>
</p>
<p>
<s>
Historie	historie	k1gFnSc1	historie
tohoto	tento	k3xDgInSc2	tento
jazyka	jazyk	k1gInSc2	jazyk
sahá	sahat	k5eAaImIp3nS	sahat
až	až	k9	až
do	do	k7c2	do
září	září	k1gNnSc2	září
roku	rok	k1gInSc2	rok
1987	[number]	k4	1987
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
konference	konference	k1gFnSc1	konference
o	o	k7c6	o
funkcionálním	funkcionální	k2eAgNnSc6d1	funkcionální
programování	programování	k1gNnSc6	programování
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Oregon	Oregon	k1gInSc1	Oregon
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
verze	verze	k1gFnSc1	verze
tohoto	tento	k3xDgInSc2	tento
jazyka	jazyk	k1gInSc2	jazyk
byla	být	k5eAaImAgFnS	být
oznámena	oznámit	k5eAaPmNgFnS	oznámit
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
(	(	kIx(	(
<g/>
version	version	k1gInSc1	version
1.0	[number]	k4	1.0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
přišly	přijít	k5eAaPmAgFnP	přijít
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1991	[number]	k4	1991
(	(	kIx(	(
<g/>
version	version	k1gInSc1	version
1.1	[number]	k4	1.1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1992	[number]	k4	1992
(	(	kIx(	(
<g/>
version	version	k1gInSc1	version
1.2	[number]	k4	1.2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1996	[number]	k4	1996
(	(	kIx(	(
<g/>
version	version	k1gInSc1	version
1.3	[number]	k4	1.3
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1997	[number]	k4	1997
(	(	kIx(	(
<g/>
version	version	k1gInSc1	version
<g/>
1.4	[number]	k4	1.4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
registroval	registrovat	k5eAaBmAgMnS	registrovat
John	John	k1gMnSc1	John
Peterson	Peterson	k1gMnSc1	Peterson
internetovou	internetový	k2eAgFnSc4d1	internetová
doménu	doména	k1gFnSc4	doména
haskell	haskella	k1gFnPc2	haskella
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
vrchol	vrchol	k1gInSc4	vrchol
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
Haskell	Haskell	k1gInSc1	Haskell
98	[number]	k4	98
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
také	také	k9	také
jeho	jeho	k3xOp3gFnSc1	jeho
poslední	poslední	k2eAgFnSc1d1	poslední
oficiální	oficiální	k2eAgFnSc1d1	oficiální
verze	verze	k1gFnSc1	verze
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
čistě	čistě	k6eAd1	čistě
funkcionální	funkcionální	k2eAgNnSc1d1	funkcionální
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Scheme	Schem	k1gInSc5	Schem
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
určený	určený	k2eAgInSc1d1	určený
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jméno	jméno	k1gNnSc1	jméno
nese	nést	k5eAaImIp3nS	nést
podle	podle	k7c2	podle
amerického	americký	k2eAgMnSc2d1	americký
logika	logicus	k1gMnSc2	logicus
(	(	kIx(	(
<g/>
matematika	matematik	k1gMnSc2	matematik
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
Haskell	Haskell	k1gMnSc1	Haskell
Brooks	Brooks	k1gInSc4	Brooks
Curry	Curra	k1gFnSc2	Curra
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
12	[number]	k4	12
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1900	[number]	k4	1900
a	a	k8xC	a
zemřel	zemřít	k5eAaPmAgMnS	zemřít
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1982	[number]	k4	1982
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Pennsylvania	Pennsylvanium	k1gNnSc2	Pennsylvanium
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
americký	americký	k2eAgMnSc1d1	americký
matematik	matematik	k1gMnSc1	matematik
studoval	studovat	k5eAaImAgMnS	studovat
ve	v	k7c6	v
dvacátých	dvacátý	k4xOgNnPc6	dvacátý
letech	léto	k1gNnPc6	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
na	na	k7c6	na
Harvardu	Harvard	k1gInSc6	Harvard
<g/>
.	.	kIx.	.
</s>
<s>
Zabýval	zabývat	k5eAaImAgInS	zabývat
se	se	k3xPyFc4	se
kombinační	kombinační	k2eAgFnSc7d1	kombinační
logikou	logika	k1gFnSc7	logika
a	a	k8xC	a
dal	dát	k5eAaPmAgInS	dát
základy	základ	k1gInPc4	základ
k	k	k7c3	k
funkcionálnímu	funkcionální	k2eAgNnSc3d1	funkcionální
programování	programování	k1gNnSc3	programování
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
nese	nést	k5eAaImIp3nS	nést
také	také	k9	také
jeho	jeho	k3xOp3gInSc1	jeho
název	název	k1gInSc1	název
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1944	[number]	k4	1944
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
laboratořích	laboratoř	k1gFnPc6	laboratoř
na	na	k7c4	na
Johns	Johns	k1gInSc4	Johns
Hopkins	Hopkins	k1gInSc4	Hopkins
University	universita	k1gFnSc2	universita
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
zabývat	zabývat	k5eAaImF	zabývat
ENIAC	ENIAC	kA	ENIAC
(	(	kIx(	(
<g/>
Electronic	Electronice	k1gFnPc2	Electronice
Numerical	Numerical	k1gFnSc2	Numerical
Integrator	Integrator	k1gMnSc1	Integrator
and	and	k?	and
Computer	computer	k1gInSc1	computer
<g/>
)	)	kIx)	)
–	–	k?	–
předchůdcem	předchůdce	k1gMnSc7	předchůdce
dnešních	dnešní	k2eAgInPc2d1	dnešní
počítačů	počítač	k1gInPc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1946	[number]	k4	1946
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
napsal	napsat	k5eAaPmAgInS	napsat
studie	studie	k1gFnPc4	studie
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
interpolací	interpolace	k1gFnSc7	interpolace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
přijímá	přijímat	k5eAaImIp3nS	přijímat
pozici	pozice	k1gFnSc4	pozice
profesora	profesor	k1gMnSc2	profesor
logiky	logika	k1gFnSc2	logika
v	v	k7c6	v
Amsterdamu	Amsterdam	k1gInSc6	Amsterdam
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
stráví	strávit	k5eAaPmIp3nP	strávit
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
a	a	k8xC	a
poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
na	na	k7c4	na
State	status	k1gInSc5	status
College	College	k1gNnPc6	College
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
později	pozdě	k6eAd2	pozdě
umírá	umírat	k5eAaImIp3nS	umírat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Datové	datový	k2eAgInPc1d1	datový
typy	typ	k1gInPc1	typ
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Čísla	číslo	k1gNnPc1	číslo
===	===	k?	===
</s>
</p>
<p>
<s>
Čísla	číslo	k1gNnPc1	číslo
se	se	k3xPyFc4	se
v	v	k7c6	v
Haskellu	Haskello	k1gNnSc6	Haskello
dají	dát	k5eAaPmIp3nP	dát
reprezentovat	reprezentovat	k5eAaImF	reprezentovat
mnoha	mnoho	k4c7	mnoho
datovými	datový	k2eAgInPc7d1	datový
typy	typ	k1gInPc7	typ
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
základní	základní	k2eAgNnSc4d1	základní
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Int	Int	k?	Int
–	–	k?	–
celé	celý	k2eAgNnSc4d1	celé
číslo	číslo	k1gNnSc4	číslo
omezeného	omezený	k2eAgInSc2d1	omezený
rozsahu	rozsah	k1gInSc2	rozsah
(	(	kIx(	(
<g/>
minimálně	minimálně	k6eAd1	minimálně
-2	-2	k4	-2
<g/>
^	^	kIx~	^
<g/>
29	[number]	k4	29
..	..	k?	..
2	[number]	k4	2
<g/>
^	^	kIx~	^
<g/>
29	[number]	k4	29
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Word	Word	kA	Word
–	–	k?	–
celé	celý	k2eAgNnSc4d1	celé
číslo	číslo	k1gNnSc4	číslo
bez	bez	k7c2	bez
znaménka	znaménko	k1gNnSc2	znaménko
se	s	k7c7	s
stejnou	stejný	k2eAgFnSc7d1	stejná
velikostí	velikost	k1gFnSc7	velikost
jako	jako	k8xS	jako
Int	Int	k1gFnSc7	Int
</s>
</p>
<p>
<s>
Integer	Integer	k1gMnSc1	Integer
–	–	k?	–
celé	celý	k2eAgNnSc1d1	celé
číslo	číslo	k1gNnSc1	číslo
neomezeného	omezený	k2eNgInSc2d1	neomezený
rozsahu	rozsah	k1gInSc2	rozsah
</s>
</p>
<p>
<s>
Double	double	k2eAgMnSc1d1	double
a	a	k8xC	a
Float	Float	k2eAgMnSc1d1	Float
–	–	k?	–
čísla	číslo	k1gNnSc2	číslo
s	s	k7c7	s
plovoucí	plovoucí	k2eAgFnSc7d1	plovoucí
desetinnou	desetinný	k2eAgFnSc7d1	desetinná
čárkou	čárka	k1gFnSc7	čárka
(	(	kIx(	(
<g/>
dvojitá	dvojitý	k2eAgFnSc1d1	dvojitá
resp.	resp.	kA	resp.
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
přesnost	přesnost	k1gFnSc1	přesnost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ratio	Ratio	k1gMnSc1	Ratio
a	a	k8xC	a
–	–	k?	–
racionální	racionální	k2eAgNnSc4d1	racionální
číslo	číslo	k1gNnSc4	číslo
s	s	k7c7	s
čitatelem	čitatel	k1gInSc7	čitatel
a	a	k8xC	a
jmenovatelem	jmenovatel	k1gInSc7	jmenovatel
celočíselného	celočíselný	k2eAgInSc2d1	celočíselný
typu	typ	k1gInSc2	typ
a	a	k8xC	a
</s>
</p>
<p>
<s>
===	===	k?	===
Znaky	znak	k1gInPc1	znak
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
reprezentaci	reprezentace	k1gFnSc4	reprezentace
znaků	znak	k1gInPc2	znak
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
typ	typ	k1gInSc1	typ
Char	Char	k1gInSc1	Char
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
ukládat	ukládat	k5eAaImF	ukládat
znaky	znak	k1gInPc4	znak
Unicode	Unicod	k1gMnSc5	Unicod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Booleany	Boolean	k1gInPc4	Boolean
===	===	k?	===
</s>
</p>
<p>
<s>
K	k	k7c3	k
ukládání	ukládání	k1gNnSc3	ukládání
pravdivostních	pravdivostní	k2eAgFnPc2d1	pravdivostní
hodnot	hodnota	k1gFnPc2	hodnota
slouží	sloužit	k5eAaImIp3nS	sloužit
typ	typ	k1gInSc1	typ
Bool	Bool	k1gInSc1	Bool
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
může	moct	k5eAaImIp3nS	moct
nabývat	nabývat	k5eAaImF	nabývat
dvou	dva	k4xCgFnPc2	dva
hodnot	hodnota	k1gFnPc2	hodnota
<g/>
:	:	kIx,	:
True	True	k1gFnSc1	True
a	a	k8xC	a
False	False	k1gFnSc1	False
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Seznamy	seznam	k1gInPc4	seznam
===	===	k?	===
</s>
</p>
<p>
<s>
Seznamy	seznam	k1gInPc1	seznam
jsou	být	k5eAaImIp3nP	být
v	v	k7c4	v
Haskellu	Haskella	k1gFnSc4	Haskella
nejčastěji	často	k6eAd3	často
používaným	používaný	k2eAgInSc7d1	používaný
složeným	složený	k2eAgInSc7d1	složený
datovým	datový	k2eAgInSc7d1	datový
typem	typ	k1gInSc7	typ
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
homogenní	homogenní	k2eAgMnPc1d1	homogenní
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohou	moct	k5eAaImIp3nP	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
hodnoty	hodnota	k1gFnPc4	hodnota
pouze	pouze	k6eAd1	pouze
jednoho	jeden	k4xCgInSc2	jeden
datového	datový	k2eAgInSc2d1	datový
typu	typ	k1gInSc2	typ
(	(	kIx(	(
<g/>
např.	např.	kA	např.
celé	celý	k2eAgNnSc1d1	celé
číslo	číslo	k1gNnSc1	číslo
Int	Int	k1gFnSc1	Int
<g/>
,	,	kIx,	,
pravdivostní	pravdivostní	k2eAgFnSc1d1	pravdivostní
hodnota	hodnota	k1gFnSc1	hodnota
Bool	Boola	k1gFnPc2	Boola
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
další	další	k2eAgInSc1d1	další
seznam	seznam	k1gInSc1	seznam
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Seznamy	seznam	k1gInPc1	seznam
se	se	k3xPyFc4	se
zapisují	zapisovat	k5eAaImIp3nP	zapisovat
do	do	k7c2	do
hranatých	hranatý	k2eAgFnPc2d1	hranatá
závorek	závorka	k1gFnPc2	závorka
a	a	k8xC	a
hodnoty	hodnota	k1gFnPc1	hodnota
se	se	k3xPyFc4	se
oddělují	oddělovat	k5eAaImIp3nP	oddělovat
čárkou	čárka	k1gFnSc7	čárka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
0,2	[number]	k4	0,2
<g/>
,4	,4	k4	,4
<g/>
,6	,6	k4	,6
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
t	t	k?	t
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
r	r	kA	r
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
i	i	k9	i
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
g	g	kA	g
<g/>
'	'	kIx"	'
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
True	Tru	k1gMnPc4	Tru
<g/>
,	,	kIx,	,
<g/>
False	Fals	k1gMnPc4	Fals
<g/>
,	,	kIx,	,
<g/>
True	Tru	k1gMnPc4	Tru
<g/>
,	,	kIx,	,
<g/>
False	Fals	k1gMnPc4	Fals
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
použít	použít	k5eAaPmF	použít
ekvivalentní	ekvivalentní	k2eAgInSc4d1	ekvivalentní
zápis	zápis	k1gInSc4	zápis
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
0	[number]	k4	0
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
[	[	kIx(	[
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
potřeba	potřeba	k6eAd1	potřeba
seznam	seznam	k1gInSc4	seznam
hodnot	hodnota	k1gFnPc2	hodnota
z	z	k7c2	z
určitého	určitý	k2eAgInSc2d1	určitý
rozsahu	rozsah	k1gInSc2	rozsah
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
použít	použít	k5eAaPmF	použít
tento	tento	k3xDgInSc4	tento
zápis	zápis	k1gInSc4	zápis
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
.5	.5	k4	.5
<g/>
]	]	kIx)	]
--	--	k?	--
ekvivalentní	ekvivalentní	k2eAgFnSc1d1	ekvivalentní
[	[	kIx(	[
<g/>
1,2	[number]	k4	1,2
<g/>
,3	,3	k4	,3
<g/>
,4	,4	k4	,4
<g/>
,5	,5	k4	,5
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
'	'	kIx"	'
<g/>
a	a	k8xC	a
<g/>
'	'	kIx"	'
<g/>
..	..	k?	..
<g/>
'	'	kIx"	'
<g/>
g	g	kA	g
<g/>
'	'	kIx"	'
<g/>
]	]	kIx)	]
--	--	k?	--
ekvivalentní	ekvivalentní	k2eAgFnSc1d1	ekvivalentní
"	"	kIx"	"
<g/>
abcdefg	abcdefg	k1gMnSc1	abcdefg
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
2,4	[number]	k4	2,4
<g/>
.	.	kIx.	.
<g/>
.12	.12	k4	.12
<g/>
]	]	kIx)	]
--	--	k?	--
ekvivalentní	ekvivalentní	k2eAgFnSc1d1	ekvivalentní
[	[	kIx(	[
<g/>
2,4	[number]	k4	2,4
<g/>
,6	,6	k4	,6
<g/>
,8	,8	k4	,8
<g/>
,10	,10	k4	,10
<g/>
,12	,12	k4	,12
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
9,6	[number]	k4	9,6
<g/>
..	..	k?	..
<g/>
(	(	kIx(	(
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
)	)	kIx)	)
<g/>
]	]	kIx)	]
--	--	k?	--
ekvivalentní	ekvivalentní	k2eAgFnSc1d1	ekvivalentní
[	[	kIx(	[
<g/>
9,6	[number]	k4	9,6
<g/>
,3	,3	k4	,3
<g/>
,0	,0	k4	,0
<g/>
,	,	kIx,	,
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
Haskell	Haskell	k1gInSc1	Haskell
rovněž	rovněž	k9	rovněž
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
použití	použití	k1gNnSc4	použití
nekonečných	konečný	k2eNgInPc2d1	nekonečný
seznamů	seznam	k1gInPc2	seznam
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
..	..	k?	..
<g/>
]	]	kIx)	]
--	--	k?	--
všechna	všechen	k3xTgNnPc1	všechen
přirozená	přirozený	k2eAgNnPc1d1	přirozené
čísla	číslo	k1gNnPc1	číslo
(	(	kIx(	(
<g/>
1,2	[number]	k4	1,2
<g/>
,3	,3	k4	,3
<g/>
,4	,4	k4	,4
<g/>
,5	,5	k4	,5
<g/>
,6	,6	k4	,6
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
1,3	[number]	k4	1,3
<g/>
..	..	k?	..
<g/>
]	]	kIx)	]
--	--	k?	--
lichá	lichý	k2eAgNnPc4d1	liché
kladná	kladný	k2eAgNnPc4d1	kladné
čísla	číslo	k1gNnPc4	číslo
(	(	kIx(	(
<g/>
1,3	[number]	k4	1,3
<g/>
,5	,5	k4	,5
<g/>
,7	,7	k4	,7
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
[	[	kIx(	[
<g/>
10,9	[number]	k4	10,9
<g/>
..	..	k?	..
<g/>
]	]	kIx)	]
--	--	k?	--
10,9	[number]	k4	10,9
<g/>
,8	,8	k4	,8
<g/>
,7	,7	k4	,7
<g/>
,6	,6	k4	,6
<g/>
,5	,5	k4	,5
<g/>
,4	,4	k4	,4
<g/>
...	...	k?	...
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
zápis	zápis	k1gInSc4	zápis
řetězců	řetězec	k1gInPc2	řetězec
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
syntaktický	syntaktický	k2eAgInSc4d1	syntaktický
cukr	cukr	k1gInSc4	cukr
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
string	string	k1gInSc4	string
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
řekl	říct	k5eAaPmAgMnS	říct
\	\	kIx~	\
<g/>
"	"	kIx"	"
<g/>
ahoj	ahoj	k0	ahoj
<g/>
\	\	kIx~	\
<g/>
""	""	k?	""
--	--	k?	--
[	[	kIx(	[
<g/>
'	'	kIx"	'
<g/>
ř	ř	k?	ř
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
e	e	k0	e
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
k	k	k7c3	k
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
'	'	kIx"	'
'	'	kIx"	'
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
"	"	kIx"	"
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
a	a	k8xC	a
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
h	h	k?	h
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
o	o	k7c4	o
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
j	j	k?	j
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
"	"	kIx"	"
<g/>
'	'	kIx"	'
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
====	====	k?	====
Funkce	funkce	k1gFnSc1	funkce
pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
se	s	k7c7	s
seznamy	seznam	k1gInPc7	seznam
====	====	k?	====
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
práci	práce	k1gFnSc4	práce
se	s	k7c7	s
seznamy	seznam	k1gInPc7	seznam
v	v	k7c6	v
Haskellu	Haskell	k1gInSc6	Haskell
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
použít	použít	k5eAaPmF	použít
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejužívanější	užívaný	k2eAgInPc4d3	nejužívanější
lze	lze	k6eAd1	lze
zařadit	zařadit	k5eAaPmF	zařadit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
map	mapa	k1gFnPc2	mapa
f	f	k?	f
list	list	k1gInSc1	list
aplikuje	aplikovat	k5eAaBmIp3nS	aplikovat
funkci	funkce	k1gFnSc4	funkce
f	f	k?	f
na	na	k7c4	na
každý	každý	k3xTgInSc4	každý
prvek	prvek	k1gInSc4	prvek
seznamu	seznam	k1gInSc2	seznam
list	list	k1gInSc1	list
</s>
</p>
<p>
<s>
filter	filter	k1gInSc1	filter
f	f	k?	f
list	list	k1gInSc1	list
vrací	vracet	k5eAaImIp3nS	vracet
seznam	seznam	k1gInSc1	seznam
všech	všecek	k3xTgInPc2	všecek
prvků	prvek	k1gInPc2	prvek
seznamu	seznam	k1gInSc2	seznam
list	lista	k1gFnPc2	lista
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yIgFnPc4	který
funkce	funkce	k1gFnPc4	funkce
f	f	k?	f
vrátila	vrátit	k5eAaPmAgFnS	vrátit
True	True	k1gFnSc4	True
</s>
</p>
<p>
<s>
head	head	k6eAd1	head
list	list	k1gInSc1	list
získá	získat	k5eAaPmIp3nS	získat
první	první	k4xOgInSc1	první
prvek	prvek	k1gInSc1	prvek
seznamu	seznam	k1gInSc2	seznam
</s>
</p>
<p>
<s>
tail	tail	k1gInSc1	tail
list	list	k1gInSc1	list
vezme	vzít	k5eAaPmIp3nS	vzít
první	první	k4xOgInSc1	první
prvek	prvek	k1gInSc1	prvek
seznamu	seznam	k1gInSc2	seznam
a	a	k8xC	a
vrátí	vrátit	k5eAaPmIp3nS	vrátit
zbytek	zbytek	k1gInSc4	zbytek
</s>
</p>
<p>
<s>
init	init	k2eAgInSc1d1	init
list	list	k1gInSc1	list
vrátí	vrátit	k5eAaPmIp3nS	vrátit
všechny	všechen	k3xTgInPc4	všechen
prvky	prvek	k1gInPc4	prvek
seznamu	seznam	k1gInSc2	seznam
kromě	kromě	k7c2	kromě
posledního	poslední	k2eAgInSc2d1	poslední
</s>
</p>
<p>
<s>
length	length	k1gInSc1	length
list	list	k1gInSc1	list
spočítá	spočítat	k5eAaPmIp3nS	spočítat
délku	délka	k1gFnSc4	délka
seznamu	seznam	k1gInSc2	seznam
</s>
</p>
<p>
<s>
reverse	reverse	k1gFnSc1	reverse
list	list	k1gInSc1	list
vrátí	vrátit	k5eAaPmIp3nS	vrátit
prvky	prvek	k1gInPc4	prvek
seznamu	seznam	k1gInSc2	seznam
list	list	k1gInSc1	list
v	v	k7c6	v
opačném	opačný	k2eAgNnSc6d1	opačné
pořadí	pořadí	k1gNnSc6	pořadí
</s>
</p>
<p>
<s>
minimum	minimum	k1gNnSc1	minimum
list	list	k1gInSc1	list
získá	získat	k5eAaPmIp3nS	získat
nejmenší	malý	k2eAgFnSc4d3	nejmenší
hodnotu	hodnota	k1gFnSc4	hodnota
ze	z	k7c2	z
seznamu	seznam	k1gInSc2	seznam
</s>
</p>
<p>
<s>
maximum	maximum	k1gNnSc1	maximum
list	list	k1gInSc1	list
získá	získat	k5eAaPmIp3nS	získat
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
hodnotu	hodnota	k1gFnSc4	hodnota
ze	z	k7c2	z
seznamu	seznam	k1gInSc2	seznam
</s>
</p>
<p>
<s>
drop	drop	k1gMnSc1	drop
n	n	k0	n
list	list	k1gInSc1	list
zahodí	zahodit	k5eAaPmIp3nP	zahodit
prvních	první	k4xOgNnPc6	první
n	n	k0	n
prvků	prvek	k1gInPc2	prvek
a	a	k8xC	a
vrátí	vrátit	k5eAaPmIp3nS	vrátit
zbytek	zbytek	k1gInSc4	zbytek
</s>
</p>
<p>
<s>
take	take	k6eAd1	take
n	n	k0	n
list	list	k1gInSc1	list
vrátí	vrátit	k5eAaPmIp3nP	vrátit
seznam	seznam	k1gInSc4	seznam
prvních	první	k4xOgFnPc2	první
n	n	k0	n
prvků	prvek	k1gInPc2	prvek
</s>
</p>
<p>
<s>
=====	=====	k?	=====
Příklady	příklad	k1gInPc1	příklad
=====	=====	k?	=====
</s>
</p>
<p>
<s>
map	mapa	k1gFnPc2	mapa
negate	negat	k1gInSc5	negat
[	[	kIx(	[
<g/>
1,2	[number]	k4	1,2
<g/>
,3	,3	k4	,3
<g/>
,4	,4	k4	,4
<g/>
]	]	kIx)	]
--	--	k?	--
[	[	kIx(	[
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
filter	filter	k1gMnSc1	filter
odd	odd	kA	odd
[	[	kIx(	[
<g/>
1,2	[number]	k4	1,2
<g/>
,3	,3	k4	,3
<g/>
,4	,4	k4	,4
<g/>
,5	,5	k4	,5
<g/>
,6	,6	k4	,6
<g/>
]	]	kIx)	]
--	--	k?	--
[	[	kIx(	[
<g/>
1,3	[number]	k4	1,3
<g/>
,5	,5	k4	,5
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
take	take	k1gInSc1	take
3	[number]	k4	3
[	[	kIx(	[
<g/>
1,3	[number]	k4	1,3
<g/>
.	.	kIx.	.
<g/>
.20	.20	k4	.20
<g/>
]	]	kIx)	]
--	--	k?	--
[	[	kIx(	[
<g/>
1,3	[number]	k4	1,3
<g/>
,5	,5	k4	,5
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
head	head	k1gInSc1	head
"	"	kIx"	"
<g/>
Hello	Hello	k1gNnSc1	Hello
<g/>
"	"	kIx"	"
--	--	k?	--
'	'	kIx"	'
<g/>
H	H	kA	H
<g/>
'	'	kIx"	'
</s>
</p>
<p>
<s>
tail	tail	k1gMnSc1	tail
"	"	kIx"	"
<g/>
world	world	k1gMnSc1	world
<g/>
"	"	kIx"	"
--	--	k?	--
"	"	kIx"	"
<g/>
orld	orld	k6eAd1	orld
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
init	init	k1gInSc1	init
[	[	kIx(	[
<g/>
True	True	k1gFnSc1	True
<g/>
,	,	kIx,	,
<g/>
True	True	k1gFnSc1	True
<g/>
,	,	kIx,	,
<g/>
False	False	k1gFnSc1	False
<g/>
,	,	kIx,	,
<g/>
True	True	k1gFnSc1	True
<g/>
,	,	kIx,	,
<g/>
False	False	k1gFnSc1	False
<g/>
]	]	kIx)	]
--	--	k?	--
[	[	kIx(	[
<g/>
True	True	k1gFnSc1	True
<g/>
,	,	kIx,	,
<g/>
True	True	k1gFnSc1	True
<g/>
,	,	kIx,	,
<g/>
False	False	k1gFnSc1	False
<g/>
,	,	kIx,	,
<g/>
True	True	k1gFnSc1	True
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
length	length	k1gInSc1	length
[	[	kIx(	[
<g/>
]	]	kIx)	]
--	--	k?	--
0	[number]	k4	0
</s>
</p>
<p>
<s>
reverse	reverse	k1gFnSc1	reverse
"	"	kIx"	"
<g/>
gnirts	gnirts	k1gInSc1	gnirts
<g/>
"	"	kIx"	"
--	--	k?	--
"	"	kIx"	"
<g/>
string	string	k1gInSc1	string
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
minimum	minimum	k1gNnSc1	minimum
[	[	kIx(	[
<g/>
4,2	[number]	k4	4,2
<g/>
,6	,6	k4	,6
<g/>
,4	,4	k4	,4
<g/>
,1	,1	k4	,1
<g/>
,9	,9	k4	,9
<g/>
,3	,3	k4	,3
<g/>
]	]	kIx)	]
--	--	k?	--
1	[number]	k4	1
</s>
</p>
<p>
<s>
maximum	maximum	k1gNnSc1	maximum
[	[	kIx(	[
<g/>
4,2	[number]	k4	4,2
<g/>
,6	,6	k4	,6
<g/>
,4	,4	k4	,4
<g/>
,1	,1	k4	,1
<g/>
,9	,9	k4	,9
<g/>
,3	,3	k4	,3
<g/>
]	]	kIx)	]
--	--	k?	--
9	[number]	k4	9
</s>
</p>
<p>
<s>
drop	drop	k1gMnSc1	drop
3	[number]	k4	3
[	[	kIx(	[
<g/>
1,3	[number]	k4	1,3
<g/>
.	.	kIx.	.
<g/>
.20	.20	k4	.20
<g/>
]	]	kIx)	]
--	--	k?	--
[	[	kIx(	[
<g/>
7,9	[number]	k4	7,9
<g/>
,11	,11	k4	,11
<g/>
,13	,13	k4	,13
<g/>
,15	,15	k4	,15
<g/>
,17	,17	k4	,17
<g/>
,19	,19	k4	,19
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
===	===	k?	===
N-tice	Nice	k1gInPc4	N-tice
(	(	kIx(	(
<g/>
tuples	tuples	k1gInSc4	tuples
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
N-tice	Nice	k1gFnSc1	N-tice
v	v	k7c6	v
Haskellu	Haskello	k1gNnSc6	Haskello
ukládají	ukládat	k5eAaImIp3nP	ukládat
několik	několik	k4yIc4	několik
hodnot	hodnota	k1gFnPc2	hodnota
různých	různý	k2eAgInPc2d1	různý
typů	typ	k1gInPc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Zapisují	zapisovat	k5eAaImIp3nP	zapisovat
se	se	k3xPyFc4	se
do	do	k7c2	do
závorek	závorka	k1gFnPc2	závorka
a	a	k8xC	a
prvky	prvek	k1gInPc1	prvek
se	se	k3xPyFc4	se
oddělují	oddělovat	k5eAaImIp3nP	oddělovat
čárkou	čárka	k1gFnSc7	čárka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
dva	dva	k4xCgInPc4	dva
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
--	--	k?	--
dvojice	dvojice	k1gFnSc1	dvojice
<g/>
,	,	kIx,	,
typ	typ	k1gInSc1	typ
(	(	kIx(	(
<g/>
Int	Int	k1gFnSc1	Int
<g/>
,	,	kIx,	,
String	String	k1gInSc1	String
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
wow	wow	k?	wow
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
False	False	k1gFnSc1	False
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
1,2	[number]	k4	1,2
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
--	--	k?	--
trojice	trojice	k1gFnSc1	trojice
<g/>
,	,	kIx,	,
typ	typ	k1gInSc1	typ
(	(	kIx(	(
<g/>
String	String	k1gInSc1	String
<g/>
,	,	kIx,	,
Bool	Bool	k1gInSc1	Bool
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
Int	Int	k1gFnSc1	Int
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
==	==	k?	==
</s>
</p>
<p>
<s>
Definice	definice	k1gFnSc1	definice
funkce	funkce	k1gFnSc2	funkce
faktoriálu	faktoriál	k1gInSc2	faktoriál
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Jiná	jiný	k2eAgFnSc1d1	jiná
definice	definice	k1gFnSc1	definice
faktoriálu	faktoriál	k1gInSc2	faktoriál
(	(	kIx(	(
<g/>
používá	používat	k5eAaImIp3nS	používat
funkci	funkce	k1gFnSc4	funkce
product	product	k5eAaPmF	product
ze	z	k7c2	z
standardní	standardní	k2eAgFnSc2d1	standardní
knihovny	knihovna	k1gFnSc2	knihovna
Haskellu	Haskell	k1gInSc2	Haskell
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Naivní	naivní	k2eAgFnSc1d1	naivní
implementace	implementace	k1gFnSc1	implementace
funkce	funkce	k1gFnSc1	funkce
vracející	vracející	k2eAgInSc4d1	vracející
n-tý	ný	k?	n-tý
prvek	prvek	k1gInSc4	prvek
Fibonacciho	Fibonacci	k1gMnSc2	Fibonacci
posloupnosti	posloupnost	k1gFnSc2	posloupnost
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Eratosthenovo	Eratosthenův	k2eAgNnSc1d1	Eratosthenovo
síto	síto	k1gNnSc1	síto
–	–	k?	–
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
implementace	implementace	k1gFnSc1	implementace
algoritmu	algoritmus	k1gInSc2	algoritmus
pro	pro	k7c4	pro
nalezení	nalezení	k1gNnPc4	nalezení
všech	všecek	k3xTgNnPc2	všecek
prvočísel	prvočíslo	k1gNnPc2	prvočíslo
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Elegantní	elegantní	k2eAgInSc1d1	elegantní
zápis	zápis	k1gInSc1	zápis
řadicího	řadicí	k2eAgInSc2d1	řadicí
algoritmu	algoritmus	k1gInSc2	algoritmus
quicksort	quicksort	k1gInSc1	quicksort
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Implementace	implementace	k1gFnSc2	implementace
==	==	k?	==
</s>
</p>
<p>
<s>
Následující	následující	k2eAgFnSc1d1	následující
implementace	implementace	k1gFnSc1	implementace
zcela	zcela	k6eAd1	zcela
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
téměř	téměř	k6eAd1	téměř
zcela	zcela	k6eAd1	zcela
<g/>
)	)	kIx)	)
vyhovují	vyhovovat	k5eAaImIp3nP	vyhovovat
standardu	standard	k1gInSc3	standard
Haskell	Haskellum	k1gNnPc2	Haskellum
98	[number]	k4	98
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
distribuovány	distribuovat	k5eAaBmNgInP	distribuovat
pod	pod	k7c4	pod
open	open	k1gInSc4	open
source	sourec	k1gInSc2	sourec
licencí	licence	k1gFnPc2	licence
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Glasgow	Glasgow	k1gNnSc1	Glasgow
Haskell	Haskell	k1gMnSc1	Haskell
Compiler	Compiler	k1gMnSc1	Compiler
(	(	kIx(	(
<g/>
GHC	GHC	kA	GHC
<g/>
)	)	kIx)	)
umí	umět	k5eAaImIp3nS	umět
překládat	překládat	k5eAaImF	překládat
zdrojové	zdrojový	k2eAgInPc4d1	zdrojový
kódy	kód	k1gInPc4	kód
Haskellu	Haskell	k1gInSc2	Haskell
do	do	k7c2	do
kódu	kód	k1gInSc2	kód
závislého	závislý	k2eAgInSc2d1	závislý
na	na	k7c6	na
architektuře	architektura	k1gFnSc6	architektura
počítače	počítač	k1gInSc2	počítač
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
do	do	k7c2	do
jazyka	jazyk	k1gInSc2	jazyk
C.	C.	kA	C.
</s>
</p>
<p>
<s>
Hugs	Hugs	k6eAd1	Hugs
je	být	k5eAaImIp3nS	být
interpret	interpret	k1gMnSc1	interpret
mezikódu	mezikód	k1gInSc2	mezikód
(	(	kIx(	(
<g/>
bajtkódu	bajtkódu	k6eAd1	bajtkódu
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nabízí	nabízet	k5eAaImIp3nS	nabízet
rychlý	rychlý	k2eAgInSc4d1	rychlý
překlad	překlad	k1gInSc4	překlad
programů	program	k1gInPc2	program
a	a	k8xC	a
snesitelnou	snesitelný	k2eAgFnSc4d1	snesitelná
rychlost	rychlost	k1gFnSc4	rychlost
spouštění	spouštění	k1gNnSc2	spouštění
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vhodný	vhodný	k2eAgInSc1d1	vhodný
především	především	k9	především
pro	pro	k7c4	pro
výukové	výukový	k2eAgInPc4d1	výukový
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
nhc	nhc	k?	nhc
<g/>
98	[number]	k4	98
je	být	k5eAaImIp3nS	být
dalším	další	k2eAgMnSc7d1	další
interpretem	interpret	k1gMnSc7	interpret
využívajícím	využívající	k2eAgFnPc3d1	využívající
mezikód	mezikód	k1gInSc1	mezikód
<g/>
,	,	kIx,	,
běh	běh	k1gInSc1	běh
programů	program	k1gInPc2	program
je	být	k5eAaImIp3nS	být
znatelně	znatelně	k6eAd1	znatelně
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
než	než	k8xS	než
u	u	k7c2	u
Hugsu	Hugs	k1gInSc2	Hugs
<g/>
.	.	kIx.	.
</s>
<s>
Nhc	Nhc	k?	Nhc
<g/>
98	[number]	k4	98
se	se	k3xPyFc4	se
zaměřuje	zaměřovat	k5eAaImIp3nS	zaměřovat
na	na	k7c4	na
hospodárnou	hospodárný	k2eAgFnSc4d1	hospodárná
správu	správa	k1gFnSc4	správa
paměti	paměť	k1gFnSc2	paměť
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
ideální	ideální	k2eAgFnSc1d1	ideální
pro	pro	k7c4	pro
využití	využití	k1gNnSc4	využití
na	na	k7c6	na
starších	starý	k2eAgInPc6d2	starší
počítačích	počítač	k1gInPc6	počítač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
THOMPSON	THOMPSON	kA	THOMPSON
<g/>
,	,	kIx,	,
S.	S.	kA	S.
Haskell	Haskell	k1gMnSc1	Haskell
The	The	k1gMnSc1	The
Craft	Craft	k1gMnSc1	Craft
of	of	k?	of
Functional	Functional	k1gMnSc4	Functional
Programming	Programming	k1gInSc1	Programming
<g/>
.	.	kIx.	.
</s>
<s>
Addison-Wesley	Addison-Wesley	k1gInPc1	Addison-Wesley
487	[number]	k4	487
s.	s.	k?	s.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Haskell	Haskella	k1gFnPc2	Haskella
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Domovská	domovský	k2eAgFnSc1d1	domovská
stránka	stránka	k1gFnSc1	stránka
Haskellu	Haskell	k1gInSc2	Haskell
</s>
</p>
<p>
<s>
Haskell	Haskell	k1gInSc1	Haskell
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
o	o	k7c6	o
Haskellu	Haskell	k1gInSc6	Haskell
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
</s>
</p>
<p>
<s>
Naucte-se	Nauctee	k1gFnSc1	Naucte-se
<g/>
.	.	kIx.	.
<g/>
Haskell	Haskell	k1gInSc1	Haskell
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
On-line	Onin	k1gInSc5	On-lin
kniha	kniha	k1gFnSc1	kniha
o	o	k7c4	o
programování	programování	k1gNnSc4	programování
v	v	k7c6	v
Haskellu	Haskell	k1gInSc6	Haskell
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
</s>
</p>
<p>
<s>
Seriál	seriál	k1gInSc1	seriál
Haskell	Haskell	k1gInSc1	Haskell
a	a	k8xC	a
funkcionální	funkcionální	k2eAgNnSc1d1	funkcionální
programování	programování	k1gNnSc1	programování
</s>
</p>
