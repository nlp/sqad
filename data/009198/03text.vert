<p>
<s>
Vývojové	vývojový	k2eAgNnSc1d1	vývojové
prostředí	prostředí	k1gNnSc1	prostředí
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
IDE	IDE	kA	IDE
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Integrated	Integrated	k1gMnSc1	Integrated
Development	Development	k1gMnSc1	Development
Environment	Environment	k1gMnSc1	Environment
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
software	software	k1gInSc1	software
usnadňující	usnadňující	k2eAgFnSc4d1	usnadňující
práci	práce	k1gFnSc4	práce
programátorů	programátor	k1gMnPc2	programátor
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
zaměřené	zaměřený	k2eAgNnSc4d1	zaměřené
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
programovací	programovací	k2eAgInSc4d1	programovací
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
editor	editor	k1gInSc4	editor
zdrojového	zdrojový	k2eAgInSc2d1	zdrojový
kódu	kód	k1gInSc2	kód
<g/>
,	,	kIx,	,
kompilátor	kompilátor	k1gInSc1	kompilátor
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
interpret	interpret	k1gMnSc1	interpret
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
také	také	k9	také
debugger	debugger	k1gMnSc1	debugger
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
systém	systém	k1gInSc4	systém
pro	pro	k7c4	pro
rychlý	rychlý	k2eAgInSc4d1	rychlý
vývoj	vývoj	k1gInSc4	vývoj
aplikací	aplikace	k1gFnPc2	aplikace
(	(	kIx(	(
<g/>
zvaný	zvaný	k2eAgInSc1d1	zvaný
RAD	rad	k1gInSc1	rad
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
vizuální	vizuální	k2eAgInSc4d1	vizuální
návrh	návrh	k1gInSc4	návrh
grafického	grafický	k2eAgNnSc2d1	grafické
uživatelského	uživatelský	k2eAgNnSc2d1	Uživatelské
rozhraní	rozhraní	k1gNnSc2	rozhraní
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
nástroj	nástroj	k1gInSc4	nástroj
pro	pro	k7c4	pro
objektově	objektově	k6eAd1	objektově
orientované	orientovaný	k2eAgNnSc4d1	orientované
programování	programování	k1gNnSc4	programování
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
také	také	k9	také
object	object	k2eAgInSc4d1	object
browser	browser	k1gInSc4	browser
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Souhrn	souhrn	k1gInSc1	souhrn
==	==	k?	==
</s>
</p>
<p>
<s>
Vývojová	vývojový	k2eAgNnPc1d1	vývojové
prostředí	prostředí	k1gNnPc1	prostředí
jsou	být	k5eAaImIp3nP	být
navržena	navrhnout	k5eAaPmNgNnP	navrhnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
produktivitu	produktivita	k1gFnSc4	produktivita
programátora	programátor	k1gMnSc2	programátor
pomocí	pomocí	k7c2	pomocí
provázaných	provázaný	k2eAgFnPc2d1	provázaná
komponent	komponenta	k1gFnPc2	komponenta
s	s	k7c7	s
podobným	podobný	k2eAgNnSc7d1	podobné
uživatelským	uživatelský	k2eAgNnSc7d1	Uživatelské
rozhraním	rozhraní	k1gNnSc7	rozhraní
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
programátor	programátor	k1gMnSc1	programátor
nemusí	muset	k5eNaImIp3nS	muset
tolikrát	tolikrát	k6eAd1	tolikrát
přepínat	přepínat	k5eAaImF	přepínat
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
módy	mód	k1gInPc7	mód
jako	jako	k8xC	jako
při	při	k7c6	při
použití	použití	k1gNnSc6	použití
individuálních	individuální	k2eAgInPc2d1	individuální
vývojových	vývojový	k2eAgInPc2d1	vývojový
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
protože	protože	k8xS	protože
IDE	IDE	kA	IDE
je	být	k5eAaImIp3nS	být
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
podstaty	podstata	k1gFnSc2	podstata
komplikovaným	komplikovaný	k2eAgInSc7d1	komplikovaný
softwarem	software	k1gInSc7	software
<g/>
,	,	kIx,	,
vysoká	vysoký	k2eAgFnSc1d1	vysoká
produktivita	produktivita	k1gFnSc1	produktivita
se	se	k3xPyFc4	se
projeví	projevit	k5eAaPmIp3nS	projevit
až	až	k9	až
po	po	k7c6	po
získání	získání	k1gNnSc6	získání
určitých	určitý	k2eAgFnPc2d1	určitá
zkušeností	zkušenost	k1gFnPc2	zkušenost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
je	být	k5eAaImIp3nS	být
IDE	IDE	kA	IDE
určeno	určit	k5eAaPmNgNnS	určit
pro	pro	k7c4	pro
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
programovací	programovací	k2eAgInSc4d1	programovací
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
aby	aby	kYmCp3nS	aby
poskytovalo	poskytovat	k5eAaImAgNnS	poskytovat
sadu	sada	k1gFnSc4	sada
vlastností	vlastnost	k1gFnPc2	vlastnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
co	co	k9	co
nejvíce	hodně	k6eAd3	hodně
přizpůsobují	přizpůsobovat	k5eAaImIp3nP	přizpůsobovat
programovacím	programovací	k2eAgNnPc3d1	programovací
paradigmatům	paradigma	k1gNnPc3	paradigma
daného	daný	k2eAgInSc2d1	daný
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
vícejazyková	vícejazykový	k2eAgNnPc4d1	vícejazykový
vývojová	vývojový	k2eAgNnPc4d1	vývojové
prostředí	prostředí	k1gNnPc4	prostředí
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Eclipse	Eclipse	k1gFnSc1	Eclipse
<g/>
,	,	kIx,	,
IntelliJ	IntelliJ	k1gFnSc1	IntelliJ
<g/>
,	,	kIx,	,
ActiveState	ActiveStat	k1gInSc5	ActiveStat
Komodo	komoda	k1gFnSc5	komoda
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnSc1d1	poslední
verze	verze	k1gFnSc1	verze
NetBeans	NetBeansa	k1gFnPc2	NetBeansa
<g/>
,	,	kIx,	,
Microsoft	Microsoft	kA	Microsoft
Visual	Visual	k1gInSc1	Visual
Studio	studio	k1gNnSc1	studio
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
IDE	IDE	kA	IDE
obvykle	obvykle	k6eAd1	obvykle
představují	představovat	k5eAaImIp3nP	představovat
samostatný	samostatný	k2eAgInSc4d1	samostatný
program	program	k1gInSc4	program
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
probíhá	probíhat	k5eAaImIp3nS	probíhat
celý	celý	k2eAgInSc4d1	celý
vývoj	vývoj	k1gInSc4	vývoj
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
program	program	k1gInSc1	program
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
mnoho	mnoho	k4c4	mnoho
nástrojů	nástroj	k1gInPc2	nástroj
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
<g/>
,	,	kIx,	,
úpravu	úprava	k1gFnSc4	úprava
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
a	a	k8xC	a
ladění	ladění	k1gNnSc4	ladění
softwaru	software	k1gInSc2	software
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
IDE	IDE	kA	IDE
je	být	k5eAaImIp3nS	být
shrnout	shrnout	k5eAaPmF	shrnout
schopnosti	schopnost	k1gFnPc4	schopnost
nástrojů	nástroj	k1gInPc2	nástroj
programovacího	programovací	k2eAgInSc2d1	programovací
jazyka	jazyk	k1gInSc2	jazyk
do	do	k7c2	do
takové	takový	k3xDgFnSc2	takový
ucelené	ucelený	k2eAgFnSc2d1	ucelená
podoby	podoba	k1gFnSc2	podoba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
teoreticky	teoreticky	k6eAd1	teoreticky
snižuje	snižovat	k5eAaImIp3nS	snižovat
čas	čas	k1gInSc4	čas
potřebný	potřebný	k2eAgInSc4d1	potřebný
k	k	k7c3	k
porozumění	porozumění	k1gNnSc3	porozumění
jazyku	jazyk	k1gInSc3	jazyk
a	a	k8xC	a
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
produktivitu	produktivita	k1gFnSc4	produktivita
vývojáře	vývojář	k1gMnSc2	vývojář
<g/>
.	.	kIx.	.
</s>
<s>
Těsná	těsný	k2eAgFnSc1d1	těsná
integrace	integrace	k1gFnSc1	integrace
činností	činnost	k1gFnPc2	činnost
může	moct	k5eAaImIp3nS	moct
také	také	k9	také
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
přispět	přispět	k5eAaPmF	přispět
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
produktivity	produktivita	k1gFnSc2	produktivita
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
zdrojový	zdrojový	k2eAgInSc1d1	zdrojový
kód	kód	k1gInSc1	kód
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
překládán	překládat	k5eAaImNgInS	překládat
ještě	ještě	k9	ještě
během	během	k7c2	během
psaní	psaní	k1gNnSc2	psaní
a	a	k8xC	a
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
navíc	navíc	k6eAd1	navíc
jsou	být	k5eAaImIp3nP	být
okamžitě	okamžitě	k6eAd1	okamžitě
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
zpětné	zpětný	k2eAgFnSc2d1	zpětná
informace	informace	k1gFnSc2	informace
o	o	k7c6	o
syntaktických	syntaktický	k2eAgFnPc6d1	syntaktická
chybách	chyba	k1gFnPc6	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
většina	většina	k1gFnSc1	většina
moderních	moderní	k2eAgInPc2d1	moderní
IDE	IDE	kA	IDE
je	být	k5eAaImIp3nS	být
grafická	grafický	k2eAgFnSc1d1	grafická
<g/>
,	,	kIx,	,
vývojová	vývojový	k2eAgFnSc1d1	vývojová
prostředí	prostředí	k1gNnSc4	prostředí
používaná	používaný	k2eAgNnPc4d1	používané
před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
operačních	operační	k2eAgInPc2d1	operační
systémů	systém	k1gInPc2	systém
Microsoft	Microsoft	kA	Microsoft
Windows	Windows	kA	Windows
byla	být	k5eAaImAgFnS	být
textová	textový	k2eAgFnSc1d1	textová
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
využívala	využívat	k5eAaPmAgFnS	využívat
funkčních	funkční	k2eAgFnPc2d1	funkční
i	i	k8xC	i
takzvaných	takzvaný	k2eAgFnPc2d1	takzvaná
horkých	horký	k2eAgFnPc2d1	horká
kláves	klávesa	k1gFnPc2	klávesa
k	k	k7c3	k
vykonání	vykonání	k1gNnSc3	vykonání
různých	různý	k2eAgInPc2d1	různý
úkolů	úkol	k1gInPc2	úkol
(	(	kIx(	(
<g/>
Turbo	turba	k1gFnSc5	turba
Pascal	pascal	k1gInSc1	pascal
je	být	k5eAaImIp3nS	být
častým	častý	k2eAgInSc7d1	častý
příkladem	příklad	k1gInSc7	příklad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protikladem	protiklad	k1gInSc7	protiklad
k	k	k7c3	k
vývoji	vývoj	k1gInSc3	vývoj
softwaru	software	k1gInSc2	software
v	v	k7c6	v
IDE	IDE	kA	IDE
je	být	k5eAaImIp3nS	být
psaní	psaní	k1gNnSc4	psaní
programů	program	k1gInPc2	program
pomocí	pomocí	k7c2	pomocí
samostatných	samostatný	k2eAgInPc2d1	samostatný
nástrojů	nástroj	k1gInPc2	nástroj
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
Vim	Vim	k?	Vim
<g/>
,	,	kIx,	,
GCC	GCC	kA	GCC
<g/>
,	,	kIx,	,
make	make	k1gNnSc2	make
a	a	k8xC	a
GDB	GDB	kA	GDB
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
IDE	IDE	kA	IDE
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
nezbytnými	zbytný	k2eNgFnPc7d1	zbytný
v	v	k7c6	v
době	doba	k1gFnSc6	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
programy	program	k1gInPc1	program
začaly	začít	k5eAaPmAgInP	začít
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
přes	přes	k7c4	přes
konzoli	konzole	k1gFnSc4	konzole
nebo	nebo	k8xC	nebo
terminál	terminál	k1gInSc4	terminál
<g/>
.	.	kIx.	.
</s>
<s>
Jazyk	jazyk	k1gInSc1	jazyk
Dartmouth	Dartmouth	k1gInSc1	Dartmouth
BASIC	Basic	kA	Basic
byl	být	k5eAaImAgInS	být
prvním	první	k4xOgInSc6	první
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
byl	být	k5eAaImAgInS	být
vyvinut	vyvinout	k5eAaPmNgInS	vyvinout
s	s	k7c7	s
IDE	IDE	kA	IDE
(	(	kIx(	(
<g/>
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
také	také	k6eAd1	také
prvním	první	k4xOgInPc3	první
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
pro	pro	k7c4	pro
použití	použití	k1gNnSc4	použití
na	na	k7c4	na
konzoli	konzole	k1gFnSc4	konzole
nebo	nebo	k8xC	nebo
terminálu	terminála	k1gFnSc4	terminála
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vývojové	vývojový	k2eAgNnSc1d1	vývojové
prostředí	prostředí	k1gNnSc1	prostředí
tohoto	tento	k3xDgInSc2	tento
jazyka	jazyk	k1gInSc2	jazyk
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
z	z	k7c2	z
Dartmouth	Dartmouth	k1gInSc4	Dartmouth
Time	Time	k1gFnSc3	Time
Sharing	Sharing	k1gInSc1	Sharing
System	Syst	k1gInSc7	Syst
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
na	na	k7c6	na
příkazech	příkaz	k1gInPc6	příkaz
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
nevypadalo	vypadat	k5eNaPmAgNnS	vypadat
jako	jako	k9	jako
dnes	dnes	k6eAd1	dnes
používaná	používaný	k2eAgNnPc1d1	používané
grafická	grafický	k2eAgNnPc1d1	grafické
IDE	IDE	kA	IDE
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
integrovalo	integrovat	k5eAaBmAgNnS	integrovat
editaci	editace	k1gFnSc4	editace
<g/>
,	,	kIx,	,
správu	správa	k1gFnSc4	správa
souborů	soubor	k1gInPc2	soubor
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc4	překlad
<g/>
,	,	kIx,	,
ladění	ladění	k1gNnSc4	ladění
a	a	k8xC	a
vykonávání	vykonávání	k1gNnSc4	vykonávání
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
moderní	moderní	k2eAgFnSc7d1	moderní
IDE	IDE	kA	IDE
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Maestro	maestro	k1gMnSc1	maestro
I	i	k9	i
je	být	k5eAaImIp3nS	být
produkt	produkt	k1gInSc1	produkt
firmy	firma	k1gFnSc2	firma
Softlab	Softlab	k1gMnSc1	Softlab
Munich	Munich	k1gMnSc1	Munich
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
integrovaným	integrovaný	k2eAgNnSc7d1	integrované
vývojovým	vývojový	k2eAgNnSc7d1	vývojové
prostředím	prostředí	k1gNnSc7	prostředí
pro	pro	k7c4	pro
software	software	k1gInSc4	software
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Maestro	maestro	k1gMnSc1	maestro
I	i	k9	i
byl	být	k5eAaImAgMnS	být
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
instalován	instalovat	k5eAaBmNgInS	instalovat
pro	pro	k7c4	pro
22	[number]	k4	22
000	[number]	k4	000
programátorů	programátor	k1gMnPc2	programátor
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
existovalo	existovat	k5eAaImAgNnS	existovat
například	například	k6eAd1	například
ve	v	k7c6	v
Spolkové	spolkový	k2eAgFnSc6d1	spolková
republice	republika	k1gFnSc6	republika
Německo	Německo	k1gNnSc4	Německo
šest	šest	k4xCc4	šest
tisíc	tisíc	k4xCgInSc4	tisíc
instalací	instalace	k1gFnPc2	instalace
<g/>
.	.	kIx.	.
</s>
<s>
Maestro	maestro	k1gMnSc1	maestro
I	i	k9	i
byl	být	k5eAaImAgMnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
světovou	světový	k2eAgFnSc7d1	světová
jedničkou	jednička	k1gFnSc7	jednička
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
oboru	obor	k1gInSc6	obor
v	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
a	a	k8xC	a
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
můžeme	moct	k5eAaImIp1nP	moct
najít	najít	k5eAaPmF	najít
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
posledních	poslední	k2eAgFnPc2d1	poslední
instalací	instalace	k1gFnPc2	instalace
Maestro	maestro	k1gMnSc1	maestro
I	i	k9	i
v	v	k7c4	v
Arlington	Arlington	k1gInSc4	Arlington
Museum	museum	k1gNnSc4	museum
of	of	k?	of
Information	Information	k1gInSc1	Information
Technology	technolog	k1gMnPc4	technolog
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgNnPc2	první
IDE	IDE	kA	IDE
s	s	k7c7	s
plug-in	plugn	k1gInSc1	plug-in
konceptem	koncept	k1gInSc7	koncept
byl	být	k5eAaImAgInS	být
software	software	k1gInSc1	software
s	s	k7c7	s
názvem	název	k1gInSc7	název
Softbench	Softbencha	k1gFnPc2	Softbencha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
časopis	časopis	k1gInSc1	časopis
Computerwoche	Computerwoche	k1gInSc4	Computerwoche
napsal	napsat	k5eAaPmAgInS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
používání	používání	k1gNnSc1	používání
IDE	IDE	kA	IDE
nebylo	být	k5eNaImAgNnS	být
vývojáři	vývojář	k1gMnPc7	vývojář
dobře	dobře	k6eAd1	dobře
přijato	přijmout	k5eAaPmNgNnS	přijmout
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
omezování	omezování	k1gNnSc2	omezování
jejich	jejich	k3xOp3gFnSc2	jejich
kreativity	kreativita	k1gFnSc2	kreativita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vizuální	vizuální	k2eAgNnSc1d1	vizuální
programování	programování	k1gNnSc1	programování
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
vizuální	vizuální	k2eAgNnSc4d1	vizuální
programování	programování	k1gNnSc4	programování
(	(	kIx(	(
<g/>
nezaměňovat	zaměňovat	k5eNaImF	zaměňovat
s	s	k7c7	s
Visual	Visual	k1gInSc4	Visual
Basic	Basic	kA	Basic
nebo	nebo	k8xC	nebo
Visual	Visual	k1gMnSc1	Visual
C	C	kA	C
<g/>
++	++	k?	++
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vizuální	vizuální	k2eAgFnSc1d1	vizuální
IDE	IDE	kA	IDE
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
uživateli	uživatel	k1gMnSc3	uživatel
vytvářet	vytvářet	k5eAaImF	vytvářet
nové	nový	k2eAgFnPc4d1	nová
aplikace	aplikace	k1gFnPc4	aplikace
přemístěním	přemístění	k1gNnSc7	přemístění
programovacích	programovací	k2eAgInPc2d1	programovací
stavebních	stavební	k2eAgInPc2d1	stavební
bloků	blok	k1gInPc2	blok
nebo	nebo	k8xC	nebo
uzlů	uzel	k1gInPc2	uzel
a	a	k8xC	a
vytvořením	vytvoření	k1gNnSc7	vytvoření
vývojových	vývojový	k2eAgInPc2d1	vývojový
diagramů	diagram	k1gInPc2	diagram
nebo	nebo	k8xC	nebo
blokových	blokový	k2eAgNnPc2d1	Blokové
schémat	schéma	k1gNnPc2	schéma
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
dále	daleko	k6eAd2	daleko
přeloženy	přeložit	k5eAaPmNgFnP	přeložit
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
diagramy	diagram	k1gInPc1	diagram
bývají	bývat	k5eAaImIp3nP	bývat
často	často	k6eAd1	často
založeny	založit	k5eAaPmNgInP	založit
na	na	k7c4	na
UML	UML	kA	UML
(	(	kIx(	(
<g/>
Unified	Unified	k1gInSc1	Unified
Modeling	Modeling	k1gInSc1	Modeling
Language	language	k1gFnSc1	language
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Toto	tento	k3xDgNnSc1	tento
rozhraní	rozhraní	k1gNnSc1	rozhraní
získalo	získat	k5eAaPmAgNnS	získat
oblibu	obliba	k1gFnSc4	obliba
s	s	k7c7	s
příchodem	příchod	k1gInSc7	příchod
systému	systém	k1gInSc2	systém
Lego	lego	k1gNnSc4	lego
Mindstorms	Mindstormsa	k1gFnPc2	Mindstormsa
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
aktivně	aktivně	k6eAd1	aktivně
používáno	používán	k2eAgNnSc1d1	používáno
řadou	řada	k1gFnSc7	řada
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
tak	tak	k9	tak
chtějí	chtít	k5eAaImIp3nP	chtít
těžit	těžit	k5eAaImF	těžit
ze	z	k7c2	z
schopností	schopnost	k1gFnPc2	schopnost
prohlížečů	prohlížeč	k1gMnPc2	prohlížeč
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
těch	ten	k3xDgNnPc2	ten
vytvořených	vytvořený	k2eAgNnPc2d1	vytvořené
v	v	k7c6	v
Mozille	Mozill	k1gInSc6	Mozill
<g/>
.	.	kIx.	.
</s>
<s>
KTechlab	KTechlab	k1gMnSc1	KTechlab
podporuje	podporovat	k5eAaImIp3nS	podporovat
programování	programování	k1gNnSc4	programování
pomocí	pomocí	k7c2	pomocí
vývojových	vývojový	k2eAgInPc2d1	vývojový
diagramů	diagram	k1gInPc2	diagram
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
populárním	populární	k2eAgNnSc7d1	populární
vývojovým	vývojový	k2eAgNnSc7d1	vývojové
prostředím	prostředí	k1gNnSc7	prostředí
a	a	k8xC	a
simulátorem	simulátor	k1gInSc7	simulátor
pro	pro	k7c4	pro
vývoj	vývoj	k1gInSc4	vývoj
aplikací	aplikace	k1gFnPc2	aplikace
pro	pro	k7c4	pro
mikropočítače	mikropočítač	k1gInPc4	mikropočítač
<g/>
.	.	kIx.	.
</s>
<s>
Vizuální	vizuální	k2eAgNnSc1d1	vizuální
programování	programování	k1gNnSc1	programování
se	se	k3xPyFc4	se
také	také	k9	také
zasloužilo	zasloužit	k5eAaPmAgNnS	zasloužit
o	o	k7c4	o
rozšíření	rozšíření	k1gNnSc4	rozšíření
distribuovaného	distribuovaný	k2eAgNnSc2d1	distribuované
programování	programování	k1gNnSc2	programování
<g/>
.	.	kIx.	.
</s>
<s>
Max	Max	k1gMnSc1	Max
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
softwarů	software	k1gInPc2	software
pro	pro	k7c4	pro
vizuální	vizuální	k2eAgNnSc4d1	vizuální
programování	programování	k1gNnSc4	programování
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
podle	podle	k7c2	podle
analogového	analogový	k2eAgInSc2d1	analogový
syntezátoru	syntezátor	k1gInSc2	syntezátor
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
používán	používat	k5eAaImNgMnS	používat
pro	pro	k7c4	pro
tvorbu	tvorba	k1gFnSc4	tvorba
hudebních	hudební	k2eAgFnPc2d1	hudební
aplikací	aplikace	k1gFnPc2	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
z	z	k7c2	z
průkopníků	průkopník	k1gMnPc2	průkopník
byl	být	k5eAaImAgMnS	být
Prograph	Prograph	k1gInSc4	Prograph
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
navržený	navržený	k2eAgInSc4d1	navržený
pro	pro	k7c4	pro
Macintosh	Macintosh	kA	Macintosh
a	a	k8xC	a
založený	založený	k2eAgInSc4d1	založený
na	na	k7c6	na
principu	princip	k1gInSc6	princip
datového	datový	k2eAgInSc2d1	datový
toku	tok	k1gInSc2	tok
<g/>
.	.	kIx.	.
</s>
<s>
Grafické	grafický	k2eAgNnSc1d1	grafické
vývojové	vývojový	k2eAgNnSc1d1	vývojové
prostředí	prostředí	k1gNnSc1	prostředí
Grape	Grape	k?	Grape
je	být	k5eAaImIp3nS	být
používáno	používat	k5eAaImNgNnS	používat
pro	pro	k7c4	pro
programování	programování	k1gNnSc4	programování
stavebnicových	stavebnicový	k2eAgInPc2d1	stavebnicový
robotů	robot	k1gInPc2	robot
qfix	qfix	k1gInSc4	qfix
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
přístup	přístup	k1gInSc1	přístup
je	být	k5eAaImIp3nS	být
také	také	k9	také
používán	používat	k5eAaImNgInS	používat
u	u	k7c2	u
specializovaných	specializovaný	k2eAgInPc2d1	specializovaný
softwarů	software	k1gInPc2	software
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
například	například	k6eAd1	například
Openlab	Openlaba	k1gFnPc2	Openlaba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
koncový	koncový	k2eAgMnSc1d1	koncový
uživatel	uživatel	k1gMnSc1	uživatel
požaduje	požadovat	k5eAaImIp3nS	požadovat
maximální	maximální	k2eAgFnSc4d1	maximální
flexibilitu	flexibilita	k1gFnSc4	flexibilita
bez	bez	k7c2	bez
nutnosti	nutnost	k1gFnSc2	nutnost
dlouhého	dlouhý	k2eAgNnSc2d1	dlouhé
osvojování	osvojování	k1gNnSc2	osvojování
znalostí	znalost	k1gFnPc2	znalost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
jiných	jiný	k1gMnPc2	jiný
programovacích	programovací	k2eAgInPc2d1	programovací
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vizuální	vizuální	k2eAgNnSc1d1	vizuální
programování	programování	k1gNnSc1	programování
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
open	openo	k1gNnPc2	openo
source	source	k1gMnSc1	source
prostředí	prostředí	k1gNnSc2	prostředí
Mindscript	Mindscript	k1gMnSc1	Mindscript
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
rozšířené	rozšířený	k2eAgFnPc4d1	rozšířená
funkce	funkce	k1gFnPc4	funkce
pro	pro	k7c4	pro
šifrování	šifrování	k1gNnSc4	šifrování
<g/>
,	,	kIx,	,
propojení	propojení	k1gNnSc4	propojení
s	s	k7c7	s
databázemi	databáze	k1gFnPc7	databáze
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
==	==	k?	==
Podpora	podpora	k1gFnSc1	podpora
programovacích	programovací	k2eAgInPc2d1	programovací
jazyků	jazyk	k1gInPc2	jazyk
==	==	k?	==
</s>
</p>
<p>
<s>
Některá	některý	k3yIgNnPc1	některý
IDE	IDE	kA	IDE
podporují	podporovat	k5eAaImIp3nP	podporovat
více	hodně	k6eAd2	hodně
jazyků	jazyk	k1gInPc2	jazyk
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Eclipse	Eclipse	k1gFnSc1	Eclipse
nebo	nebo	k8xC	nebo
Netbeans	Netbeans	k1gInSc1	Netbeans
<g/>
,	,	kIx,	,
oboje	oboj	k1gFnPc1	oboj
vytvořené	vytvořený	k2eAgFnPc1d1	vytvořená
v	v	k7c6	v
Javě	Jav	k1gInSc6	Jav
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
MonoDevelop	MonoDevelop	k1gInSc1	MonoDevelop
<g/>
,	,	kIx,	,
vytvořený	vytvořený	k2eAgInSc1d1	vytvořený
v	v	k7c6	v
C	C	kA	C
<g/>
#	#	kIx~	#
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podpora	podpora	k1gFnSc1	podpora
pro	pro	k7c4	pro
alternativní	alternativní	k2eAgInPc4d1	alternativní
jazyky	jazyk	k1gInPc4	jazyk
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
poskytována	poskytovat	k5eAaImNgFnS	poskytovat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
pluginů	plugin	k1gInPc2	plugin
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
instalovány	instalovat	k5eAaBmNgInP	instalovat
současně	současně	k6eAd1	současně
na	na	k7c6	na
stejném	stejný	k2eAgInSc6d1	stejný
IDE	IDE	kA	IDE
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Eclipse	Eclipse	k1gFnSc1	Eclipse
a	a	k8xC	a
Netbeans	Netbeans	k1gInSc1	Netbeans
mají	mít	k5eAaImIp3nP	mít
pluginy	plugin	k1gInPc1	plugin
pro	pro	k7c4	pro
C	C	kA	C
<g/>
/	/	kIx~	/
<g/>
C	C	kA	C
<g/>
++	++	k?	++
<g/>
,	,	kIx,	,
Python	Python	k1gMnSc1	Python
<g/>
,	,	kIx,	,
Ruby	rub	k1gInPc1	rub
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
PHP	PHP	kA	PHP
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
další	další	k2eAgInPc1d1	další
jazyky	jazyk	k1gInPc1	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příklady	příklad	k1gInPc1	příklad
==	==	k?	==
</s>
</p>
<p>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
několik	několik	k4yIc4	několik
příkladů	příklad	k1gInPc2	příklad
vývojových	vývojový	k2eAgFnPc2d1	vývojová
prostředí	prostředí	k1gNnSc4	prostředí
podle	podle	k7c2	podle
programovacích	programovací	k2eAgInPc2d1	programovací
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Object	Object	k2eAgInSc1d1	Object
Pascal	pascal	k1gInSc1	pascal
</s>
</p>
<p>
<s>
Delphi	Delphi	k6eAd1	Delphi
</s>
</p>
<p>
<s>
Kylix	Kylix	k1gInSc1	Kylix
</s>
</p>
<p>
<s>
Lazarus	Lazarus	k1gMnSc1	Lazarus
</s>
</p>
<p>
<s>
Free	Free	k6eAd1	Free
Pascal	pascal	k1gInSc1	pascal
</s>
</p>
<p>
<s>
Java	Java	k6eAd1	Java
</s>
</p>
<p>
<s>
NetBeans	NetBeans	k6eAd1	NetBeans
</s>
</p>
<p>
<s>
Eclipse	Eclipse	k6eAd1	Eclipse
</s>
</p>
<p>
<s>
IntelliJ	IntelliJ	k?	IntelliJ
IDEA	idea	k1gFnSc1	idea
</s>
</p>
<p>
<s>
jEdit	jEdit	k1gMnSc1	jEdit
</s>
</p>
<p>
<s>
JBuilder	JBuilder	k1gMnSc1	JBuilder
</s>
</p>
<p>
<s>
JDeveloper	JDeveloper	k1gMnSc1	JDeveloper
</s>
</p>
<p>
<s>
C	C	kA	C
a	a	k8xC	a
C	C	kA	C
<g/>
++	++	k?	++
</s>
</p>
<p>
<s>
Microsoft	Microsoft	kA	Microsoft
Visual	Visual	k1gInSc1	Visual
Studio	studio	k1gNnSc1	studio
</s>
</p>
<p>
<s>
NetBeans	NetBeans	k6eAd1	NetBeans
</s>
</p>
<p>
<s>
Code	Code	k1gFnSc1	Code
<g/>
::	::	k?	::
<g/>
Blocks	Blocks	k1gInSc1	Blocks
</s>
</p>
<p>
<s>
CLion	CLion	k1gMnSc1	CLion
</s>
</p>
<p>
<s>
Dev-C	Dev-C	k?	Dev-C
<g/>
++	++	k?	++
</s>
</p>
<p>
<s>
C	C	kA	C
<g/>
++	++	k?	++
<g/>
Builder	Builder	k1gMnSc1	Builder
</s>
</p>
<p>
<s>
Turbo	turba	k1gFnSc5	turba
C	C	kA	C
</s>
</p>
<p>
<s>
Anjuta	Anjut	k2eAgFnSc1d1	Anjuta
</s>
</p>
<p>
<s>
MinGW	MinGW	k?	MinGW
Developer	developer	k1gMnSc1	developer
Studio	studio	k1gNnSc1	studio
</s>
</p>
<p>
<s>
Sun	Sun	kA	Sun
C	C	kA	C
<g/>
++	++	k?	++
Forge	Forg	k1gMnSc2	Forg
</s>
</p>
<p>
<s>
Ultimate	Ultimat	k1gInSc5	Ultimat
<g/>
++	++	k?	++
</s>
</p>
<p>
<s>
Microsoft	Microsoft	kA	Microsoft
Visual	Visual	k1gMnSc1	Visual
C	C	kA	C
<g/>
++	++	k?	++
</s>
</p>
<p>
<s>
VisualAge	VisualAge	k1gFnSc1	VisualAge
C	C	kA	C
<g/>
++	++	k?	++
</s>
</p>
<p>
<s>
wxDev-C	wxDev-C	k?	wxDev-C
<g/>
++	++	k?	++
</s>
</p>
<p>
<s>
OpenLDev	OpenLDev	k1gFnSc1	OpenLDev
</s>
</p>
<p>
<s>
PHP	PHP	kA	PHP
</s>
</p>
<p>
<s>
NetBeans	NetBeans	k6eAd1	NetBeans
</s>
</p>
<p>
<s>
PHPStorm	PHPStorm	k1gInSc1	PHPStorm
</s>
</p>
<p>
<s>
Hapedit	Hapedit	k5eAaPmF	Hapedit
</s>
</p>
<p>
<s>
Zend	Zend	k6eAd1	Zend
Studio	studio	k1gNnSc1	studio
</s>
</p>
<p>
<s>
PSPad	PSPad	k6eAd1	PSPad
</s>
</p>
<p>
<s>
Notepad	Notepad	k1gInSc1	Notepad
<g/>
++	++	k?	++
</s>
</p>
<p>
<s>
specializované	specializovaný	k2eAgFnPc1d1	specializovaná
</s>
</p>
<p>
<s>
Carbide	Carbid	k1gMnSc5	Carbid
<g/>
.	.	kIx.	.
<g/>
c	c	k0	c
<g/>
++	++	k?	++
</s>
</p>
<p>
<s>
CodeWarrior	CodeWarrior	k1gMnSc1	CodeWarrior
</s>
</p>
<p>
<s>
DJGPP	DJGPP	kA	DJGPP
</s>
</p>
<p>
<s>
Dart	Dart	k2eAgInSc1d1	Dart
Editor	editor	k1gInSc1	editor
</s>
</p>
<p>
<s>
prostředí	prostředí	k1gNnSc4	prostředí
pro	pro	k7c4	pro
více	hodně	k6eAd2	hodně
jazyků	jazyk	k1gInPc2	jazyk
</s>
</p>
<p>
<s>
NetBeans	NetBeans	k6eAd1	NetBeans
</s>
</p>
<p>
<s>
Eclipse	Eclipse	k6eAd1	Eclipse
</s>
</p>
<p>
<s>
KDevelopNa	KDevelopNa	k6eAd1	KDevelopNa
svou	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
dobré	dobrý	k2eAgInPc4d1	dobrý
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
ale	ale	k9	ale
běžely	běžet	k5eAaImAgInP	běžet
pouze	pouze	k6eAd1	pouze
pod	pod	k7c7	pod
DOSem	DOS	k1gInSc7	DOS
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
programy	program	k1gInPc1	program
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
přeložené	přeložený	k2eAgInPc1d1	přeložený
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
zastaraly	zastarat	k5eAaPmAgInP	zastarat
<g/>
:	:	kIx,	:
Turbo	turba	k1gFnSc5	turba
Pascal	pascal	k1gInSc1	pascal
<g/>
,	,	kIx,	,
Turbo	turba	k1gFnSc5	turba
C	C	kA	C
<g/>
,	,	kIx,	,
Borland	Borland	kA	Borland
C.	C.	kA	C.
</s>
</p>
