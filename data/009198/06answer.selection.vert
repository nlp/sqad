<s>
Vývojové	vývojový	k2eAgNnSc1d1	vývojové
prostředí	prostředí	k1gNnSc1	prostředí
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
IDE	IDE	kA	IDE
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
Integrated	Integrated	k1gMnSc1	Integrated
Development	Development	k1gMnSc1	Development
Environment	Environment	k1gMnSc1	Environment
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
software	software	k1gInSc1	software
usnadňující	usnadňující	k2eAgFnSc4d1	usnadňující
práci	práce	k1gFnSc4	práce
programátorů	programátor	k1gMnPc2	programátor
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
zaměřené	zaměřený	k2eAgNnSc4d1	zaměřené
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
konkrétní	konkrétní	k2eAgInSc4d1	konkrétní
programovací	programovací	k2eAgInSc4d1	programovací
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
