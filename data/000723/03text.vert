<s>
Schindlerův	Schindlerův	k2eAgInSc1d1	Schindlerův
seznam	seznam	k1gInSc1	seznam
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc4d1	americký
dramatický	dramatický	k2eAgInSc4d1	dramatický
film	film	k1gInSc4	film
o	o	k7c6	o
Oskaru	Oskar	k1gMnSc6	Oskar
Schindlerovi	Schindler	k1gMnSc6	Schindler
<g/>
,	,	kIx,	,
německém	německý	k2eAgMnSc6d1	německý
obchodníkovi	obchodník	k1gMnSc6	obchodník
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zachránil	zachránit	k5eAaPmAgMnS	zachránit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
tisíc	tisíc	k4xCgInPc2	tisíc
polských	polský	k2eAgMnPc2d1	polský
Židů	Žid	k1gMnPc2	Žid
díky	díky	k7c3	díky
jeho	jeho	k3xOp3gFnSc3	jeho
továrně	továrna	k1gFnSc3	továrna
během	během	k7c2	během
holokaustu	holokaust	k1gInSc2	holokaust
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
režisérem	režisér	k1gMnSc7	režisér
Stevenem	Steven	k1gMnSc7	Steven
Spielbergerem	Spielberger	k1gMnSc7	Spielberger
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
knihy	kniha	k1gFnSc2	kniha
Schindlerova	Schindlerův	k2eAgFnSc1d1	Schindlerova
archa	archa	k1gFnSc1	archa
australského	australský	k2eAgMnSc2d1	australský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Thomase	Thomas	k1gMnSc2	Thomas
Keneallyho	Keneally	k1gMnSc2	Keneally
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
stala	stát	k5eAaPmAgFnS	stát
bestsellerem	bestseller	k1gInSc7	bestseller
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
postavami	postava	k1gFnPc7	postava
jsou	být	k5eAaImIp3nP	být
Liam	Liam	k1gMnSc1	Liam
Neeson	Neeson	k1gMnSc1	Neeson
jako	jako	k8xC	jako
Schindler	Schindler	k1gMnSc1	Schindler
<g/>
,	,	kIx,	,
Ralph	Ralph	k1gMnSc1	Ralph
Fiennes	Fiennes	k1gMnSc1	Fiennes
jako	jako	k8xC	jako
důstojník	důstojník	k1gMnSc1	důstojník
SS	SS	kA	SS
Amon	Amon	k1gMnSc1	Amon
Göth	Göth	k1gMnSc1	Göth
a	a	k8xC	a
Ben	Ben	k1gInSc1	Ben
Kingsley	Kingslea	k1gFnSc2	Kingslea
jako	jako	k8xC	jako
Schindlerův	Schindlerův	k2eAgInSc1d1	Schindlerův
židovský	židovský	k2eAgInSc1d1	židovský
účetní	účetní	k2eAgInSc1d1	účetní
Itzhak	Itzhak	k1gInSc1	Itzhak
Stern	sternum	k1gNnPc2	sternum
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozhovorů	rozhovor	k1gInPc2	rozhovor
s	s	k7c7	s
"	"	kIx"	"
<g/>
Schindlerovými	Schindlerův	k2eAgMnPc7d1	Schindlerův
židy	žid	k1gMnPc7	žid
<g/>
"	"	kIx"	"
a	a	k8xC	a
na	na	k7c6	na
základě	základ	k1gInSc6	základ
pečlivého	pečlivý	k2eAgNnSc2d1	pečlivé
studia	studio	k1gNnSc2	studio
historických	historický	k2eAgInPc2d1	historický
dokumentů	dokument	k1gInPc2	dokument
rekonstruuje	rekonstruovat	k5eAaBmIp3nS	rekonstruovat
Thomas	Thomas	k1gMnSc1	Thomas
Keneally	Kenealla	k1gFnSc2	Kenealla
události	událost	k1gFnSc2	událost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
zachránění	zachránění	k1gNnSc3	zachránění
více	hodně	k6eAd2	hodně
než	než	k8xS	než
tisíce	tisíc	k4xCgInPc1	tisíc
Židů	Žid	k1gMnPc2	Žid
v	v	k7c6	v
koncentračních	koncentrační	k2eAgInPc6d1	koncentrační
táborech	tábor	k1gInPc6	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Oskar	Oskar	k1gMnSc1	Oskar
Schindler	Schindler	k1gMnSc1	Schindler
byla	být	k5eAaImAgFnS	být
rozporuplná	rozporuplný	k2eAgFnSc1d1	rozporuplná
osoba	osoba	k1gFnSc1	osoba
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
autor	autor	k1gMnSc1	autor
ji	on	k3xPp3gFnSc4	on
nijak	nijak	k6eAd1	nijak
neidealizuje	idealizovat	k5eNaBmIp3nS	idealizovat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
vyprávění	vyprávění	k1gNnSc2	vyprávění
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
životě	život	k1gInSc6	život
vykresluje	vykreslovat	k5eAaImIp3nS	vykreslovat
obrovské	obrovský	k2eAgFnPc4d1	obrovská
zvůle	zvůle	k1gFnPc4	zvůle
holokaustu	holokaust	k1gInSc2	holokaust
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
rekonstruuje	rekonstruovat	k5eAaBmIp3nS	rekonstruovat
události	událost	k1gFnPc4	událost
velice	velice	k6eAd1	velice
podrobně	podrobně	k6eAd1	podrobně
a	a	k8xC	a
v	v	k7c6	v
přesných	přesný	k2eAgFnPc6d1	přesná
souvislostech	souvislost	k1gFnPc6	souvislost
<g/>
.	.	kIx.	.
</s>
<s>
Strhující	strhující	k2eAgNnSc1d1	strhující
vyprávění	vyprávění	k1gNnSc1	vyprávění
a	a	k8xC	a
především	především	k9	především
obrovská	obrovský	k2eAgFnSc1d1	obrovská
vypovídací	vypovídací	k2eAgFnSc1d1	vypovídací
hodnota	hodnota	k1gFnSc1	hodnota
dělá	dělat	k5eAaImIp3nS	dělat
ze	z	k7c2	z
Schindlerova	Schindlerův	k2eAgInSc2d1	Schindlerův
seznamu	seznam	k1gInSc2	seznam
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgFnPc2d3	nejlepší
knih	kniha	k1gFnPc2	kniha
s	s	k7c7	s
tematikou	tematika	k1gFnSc7	tematika
holocaustu	holocaust	k1gInSc2	holocaust
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Film	film	k1gInSc1	film
je	být	k5eAaImIp3nS	být
doprovázen	doprovázet	k5eAaImNgInS	doprovázet
hudbou	hudba	k1gFnSc7	hudba
Johna	John	k1gMnSc2	John
Williamse	Williams	k1gMnSc2	Williams
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
1939	[number]	k4	1939
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
do	do	k7c2	do
Krakova	Krakov	k1gInSc2	Krakov
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
Oskar	Oskar	k1gMnSc1	Oskar
Schindler	Schindler	k1gMnSc1	Schindler
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
NSDAP	NSDAP	kA	NSDAP
a	a	k8xC	a
neúspěšný	úspěšný	k2eNgMnSc1d1	neúspěšný
průmyslník	průmyslník	k1gMnSc1	průmyslník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
má	mít	k5eAaImIp3nS	mít
zálusk	zálusk	k1gInSc4	zálusk
na	na	k7c4	na
získání	získání	k1gNnSc4	získání
rychlého	rychlý	k2eAgNnSc2d1	rychlé
bohatství	bohatství	k1gNnSc2	bohatství
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
továrny	továrna	k1gFnSc2	továrna
na	na	k7c4	na
smaltované	smaltovaný	k2eAgNnSc4d1	smaltované
nádobí	nádobí	k1gNnSc4	nádobí
<g/>
.	.	kIx.	.
</s>
<s>
Uzavírá	uzavírat	k5eAaImIp3nS	uzavírat
dohodu	dohoda	k1gFnSc4	dohoda
s	s	k7c7	s
židovskými	židovský	k2eAgMnPc7d1	židovský
předáky	předák	k1gMnPc7	předák
na	na	k7c6	na
poskytnutí	poskytnutí	k1gNnSc6	poskytnutí
finanční	finanční	k2eAgFnSc2d1	finanční
pomoci	pomoc	k1gFnSc2	pomoc
na	na	k7c4	na
odkup	odkup	k1gInSc4	odkup
továrny	továrna	k1gFnSc2	továrna
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
právě	právě	k6eAd1	právě
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
nově	nově	k6eAd1	nově
vzniklého	vzniklý	k2eAgNnSc2d1	vzniklé
krakovského	krakovský	k2eAgNnSc2d1	Krakovské
ghetta	ghetto	k1gNnSc2	ghetto
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
továrně	továrna	k1gFnSc6	továrna
zaměstnává	zaměstnávat	k5eAaImIp3nS	zaměstnávat
Židy	Žid	k1gMnPc4	Žid
z	z	k7c2	z
ghetta	ghetto	k1gNnSc2	ghetto
a	a	k8xC	a
s	s	k7c7	s
jejich	jejich	k3xOp3gNnSc7	jejich
hledáním	hledání	k1gNnSc7	hledání
mu	on	k3xPp3gNnSc3	on
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
bývalý	bývalý	k2eAgMnSc1d1	bývalý
účetní	účetní	k1gMnSc1	účetní
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
Isaac	Isaac	k1gInSc4	Isaac
Stern	sternum	k1gNnPc2	sternum
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
v	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
zaměstnat	zaměstnat	k5eAaPmF	zaměstnat
co	co	k9	co
možná	možná	k9	možná
největší	veliký	k2eAgInSc1d3	veliký
počet	počet	k1gInSc1	počet
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Továrna	továrna	k1gFnSc1	továrna
prosperuje	prosperovat	k5eAaImIp3nS	prosperovat
a	a	k8xC	a
Schindler	Schindler	k1gMnSc1	Schindler
začíná	začínat	k5eAaImIp3nS	začínat
vydělávat	vydělávat	k5eAaImF	vydělávat
bohatství	bohatství	k1gNnSc4	bohatství
<g/>
.	.	kIx.	.
</s>
<s>
Novým	nový	k2eAgMnSc7d1	nový
správcem	správce	k1gMnSc7	správce
ghetta	ghetto	k1gNnSc2	ghetto
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
Amon	Amon	k1gMnSc1	Amon
Goeth	Goeth	k1gMnSc1	Goeth
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
má	mít	k5eAaImIp3nS	mít
odpor	odpor	k1gInSc4	odpor
nejen	nejen	k6eAd1	nejen
vůči	vůči	k7c3	vůči
Židům	Žid	k1gMnPc3	Žid
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
vůči	vůči	k7c3	vůči
životu	život	k1gInSc3	život
jako	jako	k8xC	jako
takovému	takový	k3xDgNnSc3	takový
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
správa	správa	k1gFnSc1	správa
Třetí	třetí	k4xOgFnSc2	třetí
říše	říš	k1gFnSc2	říš
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
přesunout	přesunout	k5eAaPmF	přesunout
všechny	všechen	k3xTgMnPc4	všechen
krakovské	krakovský	k2eAgMnPc4d1	krakovský
Židy	Žid	k1gMnPc4	Žid
do	do	k7c2	do
Osvětimi	Osvětim	k1gFnSc2	Osvětim
<g/>
.	.	kIx.	.
</s>
<s>
Schindler	Schindler	k1gMnSc1	Schindler
tomu	ten	k3xDgNnSc3	ten
chce	chtít	k5eAaImIp3nS	chtít
za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
cenu	cena	k1gFnSc4	cena
zabránit	zabránit	k5eAaPmF	zabránit
a	a	k8xC	a
zachránit	zachránit	k5eAaPmF	zachránit
co	co	k9	co
největší	veliký	k2eAgInSc4d3	veliký
počet	počet	k1gInSc4	počet
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Domlouvá	domlouvat	k5eAaImIp3nS	domlouvat
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
s	s	k7c7	s
Goethem	Goeth	k1gInSc7	Goeth
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
něj	on	k3xPp3gInSc2	on
Židy	Žid	k1gMnPc4	Žid
koupí	koupě	k1gFnPc2	koupě
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Isaacem	Isaace	k1gNnSc7	Isaace
Sternem	sternum	k1gNnSc7	sternum
sepisuje	sepisovat	k5eAaImIp3nS	sepisovat
seznam	seznam	k1gInSc1	seznam
Židů	Žid	k1gMnPc2	Žid
(	(	kIx(	(
<g/>
Schindlerův	Schindlerův	k2eAgInSc1d1	Schindlerův
seznam	seznam	k1gInSc1	seznam
<g/>
)	)	kIx)	)
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nS	objevit
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1200	[number]	k4	1200
jmen	jméno	k1gNnPc2	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Židé	Žid	k1gMnPc1	Žid
jsou	být	k5eAaImIp3nP	být
posléze	posléze	k6eAd1	posléze
transportováni	transportovat	k5eAaBmNgMnP	transportovat
do	do	k7c2	do
Schindlerovy	Schindlerův	k2eAgFnSc2d1	Schindlerova
továrny	továrna	k1gFnSc2	továrna
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
granátů	granát	k1gInPc2	granát
v	v	k7c6	v
Brněnci	Brněnec	k1gInSc6	Brněnec
(	(	kIx(	(
<g/>
Protektorát	protektorát	k1gInSc1	protektorát
Čechy	Čechy	k1gFnPc1	Čechy
a	a	k8xC	a
Morava	Morava	k1gFnSc1	Morava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přesunu	přesun	k1gInSc6	přesun
se	se	k3xPyFc4	se
omylem	omylem	k6eAd1	omylem
jeden	jeden	k4xCgInSc1	jeden
vlak	vlak	k1gInSc1	vlak
vypraví	vypravit	k5eAaPmIp3nS	vypravit
do	do	k7c2	do
Osvětimi	Osvětim	k1gFnSc2	Osvětim
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
to	ten	k3xDgNnSc4	ten
Schindler	Schindler	k1gMnSc1	Schindler
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
<g/>
,	,	kIx,	,
neprodleně	prodleně	k6eNd1	prodleně
tam	tam	k6eAd1	tam
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
<g/>
,	,	kIx,	,
riskuje	riskovat	k5eAaBmIp3nS	riskovat
vlastní	vlastní	k2eAgInSc4d1	vlastní
život	život	k1gInSc4	život
a	a	k8xC	a
úplatkem	úplatek	k1gInSc7	úplatek
vykupuje	vykupovat	k5eAaImIp3nS	vykupovat
ženy	žena	k1gFnPc4	žena
a	a	k8xC	a
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
tam	tam	k6eAd1	tam
byly	být	k5eAaImAgFnP	být
odvezeny	odvezen	k2eAgFnPc1d1	odvezena
<g/>
.	.	kIx.	.
</s>
<s>
Paradoxem	paradox	k1gInSc7	paradox
jeho	jeho	k3xOp3gFnSc2	jeho
nové	nový	k2eAgFnSc2d1	nová
továrny	továrna	k1gFnSc2	továrna
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
žádné	žádný	k3yNgInPc4	žádný
funkční	funkční	k2eAgInPc4d1	funkční
granáty	granát	k1gInPc4	granát
sama	sám	k3xTgFnSc1	sám
nikdy	nikdy	k6eAd1	nikdy
nevyrobí	vyrobit	k5eNaPmIp3nS	vyrobit
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nP	aby
továrnu	továrna	k1gFnSc4	továrna
nezavřeli	zavřít	k5eNaPmAgMnP	zavřít
a	a	k8xC	a
Židy	Žid	k1gMnPc7	Žid
neposlali	poslat	k5eNaPmAgMnP	poslat
do	do	k7c2	do
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
<g/>
,	,	kIx,	,
nakupuje	nakupovat	k5eAaBmIp3nS	nakupovat
granáty	granát	k1gInPc4	granát
jinde	jinde	k6eAd1	jinde
a	a	k8xC	a
vydává	vydávat	k5eAaPmIp3nS	vydávat
je	on	k3xPp3gNnSc4	on
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
že	že	k8xS	že
tak	tak	k9	tak
to	ten	k3xDgNnSc1	ten
nemůže	moct	k5eNaImIp3nS	moct
fungovat	fungovat	k5eAaImF	fungovat
do	do	k7c2	do
nekonečna	nekonečno	k1gNnSc2	nekonečno
<g/>
.	.	kIx.	.
</s>
<s>
Přichází	přicházet	k5eAaImIp3nS	přicházet
den	den	k1gInSc4	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Schindlerův	Schindlerův	k2eAgMnSc1d1	Schindlerův
účetní	účetní	k1gMnSc1	účetní
<g/>
,	,	kIx,	,
Stern	sternum	k1gNnPc2	sternum
<g/>
,	,	kIx,	,
sděluje	sdělovat	k5eAaImIp3nS	sdělovat
Schindlerovi	Schindler	k1gMnSc3	Schindler
<g/>
,	,	kIx,	,
že	že	k8xS	že
došly	dojít	k5eAaPmAgInP	dojít
peníze	peníz	k1gInPc1	peníz
<g/>
.	.	kIx.	.
</s>
<s>
Shodou	shoda	k1gFnSc7	shoda
okolností	okolnost	k1gFnPc2	okolnost
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
den	den	k1gInSc1	den
konce	konec	k1gInSc2	konec
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
kapitulace	kapitulace	k1gFnSc1	kapitulace
Německa	Německo	k1gNnSc2	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Němečtí	německý	k2eAgMnPc1d1	německý
vojáci	voják	k1gMnPc1	voják
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
příkaz	příkaz	k1gInSc4	příkaz
všechny	všechen	k3xTgMnPc4	všechen
Židy	Žid	k1gMnPc4	Žid
zastřelit	zastřelit	k5eAaPmF	zastřelit
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
nakonec	nakonec	k6eAd1	nakonec
neučiní	učinit	k5eNaPmIp3nP	učinit
a	a	k8xC	a
opustí	opustit	k5eAaPmIp3nP	opustit
továrnu	továrna	k1gFnSc4	továrna
<g/>
.	.	kIx.	.
</s>
<s>
Schindler	Schindler	k1gMnSc1	Schindler
jako	jako	k8xC	jako
člen	člen	k1gMnSc1	člen
NSDAP	NSDAP	kA	NSDAP
musí	muset	k5eAaImIp3nS	muset
uprchnout	uprchnout	k5eAaPmF	uprchnout
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
souzen	souzen	k2eAgMnSc1d1	souzen
jako	jako	k8xS	jako
válečný	válečný	k2eAgMnSc1d1	válečný
zločinec	zločinec	k1gMnSc1	zločinec
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Schindlerovi	Schindlerův	k2eAgMnPc1d1	Schindlerův
židé	žid	k1gMnPc1	žid
<g/>
"	"	kIx"	"
mu	on	k3xPp3gNnSc3	on
sepisují	sepisovat	k5eAaImIp3nP	sepisovat
dobrozdání	dobrozdání	k1gNnSc4	dobrozdání
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
popisují	popisovat	k5eAaImIp3nP	popisovat
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
zachránil	zachránit	k5eAaPmAgMnS	zachránit
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
se	se	k3xPyFc4	se
pod	pod	k7c4	pod
dopis	dopis	k1gInSc4	dopis
podepíší	podepsat	k5eAaPmIp3nP	podepsat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
památku	památka	k1gFnSc4	památka
mu	on	k3xPp3gMnSc3	on
dávají	dávat	k5eAaImIp3nP	dávat
také	také	k9	také
zlatý	zlatý	k2eAgInSc4d1	zlatý
prsten	prsten	k1gInSc4	prsten
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
odlejí	odlít	k5eAaPmIp3nP	odlít
ze	z	k7c2	z
zlatých	zlatý	k2eAgInPc2d1	zlatý
zubů	zub	k1gInPc2	zub
jednoho	jeden	k4xCgMnSc4	jeden
Žida	Žid	k1gMnSc4	Žid
<g/>
,	,	kIx,	,
na	na	k7c4	na
nějž	jenž	k3xRgInSc4	jenž
vyryjí	vyrýt	k5eAaPmIp3nP	vyrýt
úryvek	úryvek	k1gInSc4	úryvek
z	z	k7c2	z
talmudu	talmud	k1gInSc2	talmud
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
zachrání	zachránit	k5eAaPmIp3nS	zachránit
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
zachrání	zachránit	k5eAaPmIp3nP	zachránit
svět	svět	k1gInSc4	svět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Sanhedrin	sanhedrin	k1gInSc1	sanhedrin
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
velmi	velmi	k6eAd1	velmi
dojemné	dojemný	k2eAgFnSc6d1	dojemná
scéně	scéna	k1gFnSc6	scéna
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
Schindler	Schindler	k1gMnSc1	Schindler
uvědomuje	uvědomovat	k5eAaImIp3nS	uvědomovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mohl	moct	k5eAaImAgInS	moct
zachránit	zachránit	k5eAaPmF	zachránit
víc	hodně	k6eAd2	hodně
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
prodal	prodat	k5eAaPmAgMnS	prodat
svoje	svůj	k3xOyFgNnSc4	svůj
auto	auto	k1gNnSc4	auto
<g/>
,	,	kIx,	,
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc4	druhý
den	den	k1gInSc4	den
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
osvobozenecká	osvobozenecký	k2eAgFnSc1d1	osvobozenecká
Rudá	rudý	k2eAgFnSc1d1	rudá
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
Židy	Žid	k1gMnPc4	Žid
"	"	kIx"	"
<g/>
osvobodí	osvobodit	k5eAaPmIp3nS	osvobodit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
příběhu	příběh	k1gInSc2	příběh
je	být	k5eAaImIp3nS	být
situován	situovat	k5eAaBmNgInS	situovat
do	do	k7c2	do
dnešního	dnešní	k2eAgInSc2d1	dnešní
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
Oscar	Oscar	k1gMnSc1	Oscar
Schindler	Schindler	k1gMnSc1	Schindler
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
hrobu	hrob	k1gInSc3	hrob
přicházejí	přicházet	k5eAaImIp3nP	přicházet
přeživší	přeživší	k2eAgMnPc1d1	přeživší
Schindlerovi	Schindlerův	k2eAgMnPc1d1	Schindlerův
Židé	Žid	k1gMnPc1	Žid
i	i	k8xC	i
jejich	jejich	k3xOp3gMnPc1	jejich
potomci	potomek	k1gMnPc1	potomek
a	a	k8xC	a
jako	jako	k8xC	jako
symbol	symbol	k1gInSc4	symbol
úcty	úcta	k1gFnSc2	úcta
pokládají	pokládat	k5eAaImIp3nP	pokládat
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
hrob	hrob	k1gInSc4	hrob
oblázky	oblázek	k1gInPc1	oblázek
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
Thomase	Thomas	k1gMnSc2	Thomas
Keneallyho	Keneally	k1gMnSc2	Keneally
<g/>
,	,	kIx,	,
sepsaná	sepsaný	k2eAgFnSc1d1	sepsaná
podle	podle	k7c2	podle
výpovědí	výpověď	k1gFnPc2	výpověď
pamětníků	pamětník	k1gMnPc2	pamětník
(	(	kIx(	(
<g/>
především	především	k9	především
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
"	"	kIx"	"
<g/>
Schindlerových	Schindlerová	k1gFnPc2	Schindlerová
Židů	Žid	k1gMnPc2	Žid
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
inspirací	inspirace	k1gFnSc7	inspirace
pro	pro	k7c4	pro
natočení	natočení	k1gNnSc4	natočení
filmu	film	k1gInSc2	film
slavným	slavný	k2eAgMnSc7d1	slavný
režisérem	režisér	k1gMnSc7	režisér
židovského	židovský	k2eAgInSc2d1	židovský
původu	původ	k1gInSc2	původ
Stevenem	Steven	k1gMnSc7	Steven
Spielbergem	Spielberg	k1gMnSc7	Spielberg
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
film	film	k1gInSc4	film
dostal	dostat	k5eAaPmAgMnS	dostat
prvního	první	k4xOgMnSc4	první
režijního	režijní	k2eAgMnSc4d1	režijní
Oscara	Oscar	k1gMnSc4	Oscar
své	svůj	k3xOyFgFnSc2	svůj
kariéry	kariéra	k1gFnSc2	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Splnil	splnit	k5eAaPmAgMnS	splnit
tím	ten	k3xDgNnSc7	ten
"	"	kIx"	"
<g/>
povinnost	povinnost	k1gFnSc1	povinnost
<g/>
"	"	kIx"	"
vůči	vůči	k7c3	vůči
ostatním	ostatní	k2eAgMnPc3d1	ostatní
Židům	Žid	k1gMnPc3	Žid
a	a	k8xC	a
sám	sám	k3xTgMnSc1	sám
za	za	k7c2	za
tržby	tržba	k1gFnSc2	tržba
filmu	film	k1gInSc2	film
nevzal	vzít	k5eNaPmAgMnS	vzít
do	do	k7c2	do
vlastní	vlastní	k2eAgFnSc2d1	vlastní
kapsy	kapsa	k1gFnSc2	kapsa
ani	ani	k8xC	ani
dolar	dolar	k1gInSc4	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Spielbergův	Spielbergův	k2eAgInSc1d1	Spielbergův
film	film	k1gInSc1	film
si	se	k3xPyFc3	se
nehraje	hrát	k5eNaImIp3nS	hrát
na	na	k7c4	na
dokument	dokument	k1gInSc4	dokument
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
některými	některý	k3yIgMnPc7	některý
historiky	historik	k1gMnPc7	historik
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
a	a	k8xC	a
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
že	že	k8xS	že
především	především	k9	především
jeho	jeho	k3xOp3gFnSc1	jeho
druhá	druhý	k4xOgFnSc1	druhý
polovina	polovina	k1gFnSc1	polovina
překrucuje	překrucovat	k5eAaImIp3nS	překrucovat
historická	historický	k2eAgNnPc4d1	historické
fakta	faktum	k1gNnPc4	faktum
(	(	kIx(	(
<g/>
např.	např.	kA	např.
záchranná	záchranný	k2eAgFnSc1d1	záchranná
mise	mise	k1gFnSc1	mise
v	v	k7c6	v
Osvětimi	Osvětim	k1gFnSc6	Osvětim
<g/>
,	,	kIx,	,
nesmyslné	smyslný	k2eNgNnSc1d1	nesmyslné
tvrzení	tvrzení	k1gNnSc1	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
brněnecké	brněnecký	k2eAgFnSc6d1	brněnecký
továrně	továrna	k1gFnSc6	továrna
se	se	k3xPyFc4	se
žádný	žádný	k3yNgInSc1	žádný
válečný	válečný	k2eAgInSc1d1	válečný
materiál	materiál	k1gInSc1	materiál
nevyrobil	vyrobit	k5eNaPmAgInS	vyrobit
<g/>
,	,	kIx,	,
přecitlivělá	přecitlivělý	k2eAgFnSc1d1	přecitlivělá
scéna	scéna	k1gFnSc1	scéna
loučení	loučení	k1gNnSc2	loučení
s	s	k7c7	s
vězni	vězeň	k1gMnPc7	vězeň
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ovšem	ovšem	k9	ovšem
uměleckou	umělecký	k2eAgFnSc4d1	umělecká
kvalitu	kvalita	k1gFnSc4	kvalita
a	a	k8xC	a
výsledný	výsledný	k2eAgInSc1d1	výsledný
dojem	dojem	k1gInSc1	dojem
z	z	k7c2	z
filmu	film	k1gInSc2	film
nikterak	nikterak	k6eAd1	nikterak
nesnižuje	snižovat	k5eNaImIp3nS	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
Liam	Liam	k1gMnSc1	Liam
Neeson	Neeson	k1gMnSc1	Neeson
jako	jako	k8xS	jako
Oskar	Oskar	k1gMnSc1	Oskar
Schindler	Schindler	k1gMnSc1	Schindler
<g/>
,	,	kIx,	,
německý	německý	k2eAgMnSc1d1	německý
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zachránil	zachránit	k5eAaPmAgInS	zachránit
asi	asi	k9	asi
1100	[number]	k4	1100
Židů	Žid	k1gMnPc2	Žid
zaměstnávaje	zaměstnávat	k5eAaImSgInS	zaměstnávat
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
továrně	továrna	k1gFnSc6	továrna
<g/>
.	.	kIx.	.
</s>
<s>
Ben	Ben	k1gInSc1	Ben
Kingsley	Kingslea	k1gFnSc2	Kingslea
jako	jako	k8xC	jako
Itzhak	Itzhak	k1gInSc1	Itzhak
Stern	sternum	k1gNnPc2	sternum
<g/>
,	,	kIx,	,
Schindlerův	Schindlerův	k2eAgMnSc1d1	Schindlerův
účetní	účetní	k1gMnSc1	účetní
a	a	k8xC	a
obchodní	obchodní	k2eAgMnSc1d1	obchodní
partner	partner	k1gMnSc1	partner
<g/>
.	.	kIx.	.
</s>
<s>
Ralph	Ralph	k1gMnSc1	Ralph
Fiennes	Fiennes	k1gMnSc1	Fiennes
jako	jako	k8xC	jako
Amon	Amon	k1gMnSc1	Amon
Göth	Göth	k1gMnSc1	Göth
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgMnSc1d1	hlavní
odpůrce	odpůrce	k1gMnSc1	odpůrce
ve	v	k7c6	v
filmu	film	k1gInSc6	film
<g/>
,	,	kIx,	,
Göth	Göth	k1gMnSc1	Göth
je	být	k5eAaImIp3nS	být
důstojník	důstojník	k1gMnSc1	důstojník
SS	SS	kA	SS
odpovědný	odpovědný	k2eAgInSc1d1	odpovědný
za	za	k7c4	za
postavení	postavení	k1gNnSc4	postavení
a	a	k8xC	a
průběh	průběh	k1gInSc4	průběh
koncentračního	koncentrační	k2eAgInSc2d1	koncentrační
tábora	tábor	k1gInSc2	tábor
Płaszów	Płaszów	k1gMnSc1	Płaszów
a	a	k8xC	a
Schindlerův	Schindlerův	k2eAgMnSc1d1	Schindlerův
přítel	přítel	k1gMnSc1	přítel
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
filmu	film	k1gInSc2	film
jeho	jeho	k3xOp3gNnSc4	jeho
podezření	podezření	k1gNnSc4	podezření
vůči	vůči	k7c3	vůči
němu	on	k3xPp3gNnSc3	on
stále	stále	k6eAd1	stále
roste	růst	k5eAaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Embeth	Embeth	k1gInSc1	Embeth
Davidtz	Davidtza	k1gFnPc2	Davidtza
jako	jako	k8xS	jako
Helen	Helena	k1gFnPc2	Helena
Hirsch	Hirsch	k1gMnSc1	Hirsch
<g/>
,	,	kIx,	,
židovská	židovský	k2eAgFnSc1d1	židovská
dívka	dívka	k1gFnSc1	dívka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
Göthovi	Göth	k1gMnSc3	Göth
dělá	dělat	k5eAaImIp3nS	dělat
služku	služka	k1gFnSc4	služka
<g/>
.	.	kIx.	.
</s>
<s>
Caroline	Carolin	k1gInSc5	Carolin
Goodallová	Goodallový	k2eAgNnPc4d1	Goodallový
jako	jako	k8xC	jako
Emilie	Emilie	k1gFnSc2	Emilie
Schindler	Schindler	k1gMnSc1	Schindler
<g/>
,	,	kIx,	,
Schindlerova	Schindlerův	k2eAgFnSc1d1	Schindlerova
manželka	manželka	k1gFnSc1	manželka
<g/>
.	.	kIx.	.
</s>
<s>
Jonathan	Jonathan	k1gMnSc1	Jonathan
Sagall	Sagall	k1gMnSc1	Sagall
jako	jako	k8xS	jako
Poldek	Poldek	k1gMnSc1	Poldek
Pfefferberg	Pfefferberg	k1gMnSc1	Pfefferberg
<g/>
,	,	kIx,	,
mladý	mladý	k2eAgMnSc1d1	mladý
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
přežívá	přežívat	k5eAaImIp3nS	přežívat
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
ženou	žena	k1gFnSc7	žena
a	a	k8xC	a
prodává	prodávat	k5eAaImIp3nS	prodávat
Schindlerovi	Schindler	k1gMnSc3	Schindler
zboží	zboží	k1gNnSc4	zboží
pocházející	pocházející	k2eAgNnSc4d1	pocházející
z	z	k7c2	z
černého	černý	k2eAgInSc2d1	černý
trhu	trh	k1gInSc2	trh
<g/>
.	.	kIx.	.
</s>
<s>
Ezra	Ezra	k1gMnSc1	Ezra
Dagan	Dagan	k1gMnSc1	Dagan
jako	jako	k8xS	jako
Rabbi	Rabb	k1gMnSc6	Rabb
Lewartow	Lewartow	k1gMnSc6	Lewartow
Malgoscha	Malgoscha	k1gFnSc1	Malgoscha
Gebel	Gebela	k1gFnPc2	Gebela
jako	jako	k8xS	jako
Wiktoria	Wiktorium	k1gNnSc2	Wiktorium
Klonowska	Klonowsko	k1gNnSc2	Klonowsko
Shmuel	Shmuel	k1gMnSc1	Shmuel
Levy	Levy	k?	Levy
jako	jako	k8xC	jako
Wilek	Wilek	k1gMnSc1	Wilek
Chilowicz	Chilowicz	k1gMnSc1	Chilowicz
Mark	Mark	k1gMnSc1	Mark
Ivanir	Ivanir	k1gMnSc1	Ivanir
jako	jako	k8xC	jako
Marcel	Marcel	k1gMnSc1	Marcel
Goldberg	Goldberg	k1gMnSc1	Goldberg
Béatrice	Béatrika	k1gFnSc6	Béatrika
Macola	Macola	k1gFnSc1	Macola
jako	jako	k8xS	jako
Ingrid	Ingrid	k1gFnSc1	Ingrid
Andrzej	Andrzej	k1gMnSc1	Andrzej
Seweryn	Seweryn	k1gMnSc1	Seweryn
jako	jako	k8xS	jako
Julian	Julian	k1gMnSc1	Julian
Scherner	Scherner	k1gMnSc1	Scherner
Friedrich	Friedrich	k1gMnSc1	Friedrich
von	von	k1gInSc4	von
Thun	Thun	k1gMnSc1	Thun
jako	jako	k8xC	jako
Rolf	Rolf	k1gMnSc1	Rolf
Czurda	Czurdo	k1gNnSc2	Czurdo
Krzysztof	Krzysztof	k1gMnSc1	Krzysztof
Luft	Luft	k?	Luft
jako	jako	k8xS	jako
Herman	Herman	k1gMnSc1	Herman
Toffel	Toffel	k1gMnSc1	Toffel
Harry	Harra	k1gFnSc2	Harra
Nehring	Nehring	k1gInSc1	Nehring
jako	jako	k8xC	jako
Leo	Leo	k1gMnSc1	Leo
John	John	k1gMnSc1	John
Norbert	Norbert	k1gMnSc1	Norbert
Weisser	Weisser	k1gMnSc1	Weisser
jako	jako	k8xS	jako
Albert	Albert	k1gMnSc1	Albert
Hujar	Hujar	k1gMnSc1	Hujar
<g />
.	.	kIx.	.
</s>
<s>
Adi	Adi	k?	Adi
Nitzan	Nitzan	k1gMnSc1	Nitzan
jako	jako	k8xS	jako
Mila	Mila	k1gMnSc1	Mila
Pfefferberg	Pfefferberg	k1gMnSc1	Pfefferberg
Michael	Michael	k1gMnSc1	Michael
Schneider	Schneider	k1gMnSc1	Schneider
jako	jako	k8xS	jako
Juda	judo	k1gNnSc2	judo
Dresner	Dresner	k1gMnSc1	Dresner
Miri	Mir	k1gFnSc2	Mir
Fabian	Fabian	k1gMnSc1	Fabian
jako	jako	k8xC	jako
Chaja	Chaja	k1gFnSc1	Chaja
Dresner	Dresner	k1gInSc1	Dresner
Anna	Anna	k1gFnSc1	Anna
Mucha	Mucha	k1gMnSc1	Mucha
jako	jako	k8xC	jako
Danka	Danka	k1gFnSc1	Danka
Dresner	Dresner	k1gInSc4	Dresner
Ben	Ben	k1gInSc1	Ben
Darby	Darba	k1gFnSc2	Darba
jako	jako	k8xC	jako
Man	Man	k1gMnSc1	Man
in	in	k?	in
Grey	Grea	k1gFnSc2	Grea
Albert	Albert	k1gMnSc1	Albert
Misak	Misak	k1gMnSc1	Misak
jako	jako	k8xC	jako
Mordecai	Mordecai	k1gNnSc1	Mordecai
Wulkan	Wulkan	k1gMnSc1	Wulkan
Hans-Michael	Hans-Michael	k1gMnSc1	Hans-Michael
Rehberg	Rehberg	k1gMnSc1	Rehberg
jako	jako	k8xS	jako
Rudolf	Rudolf	k1gMnSc1	Rudolf
Hoess	Hoessa	k1gFnPc2	Hoessa
Daniel	Daniel	k1gMnSc1	Daniel
Del	Del	k1gMnSc1	Del
Ponte	Pont	k1gInSc5	Pont
jako	jako	k9	jako
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Josef	Josef	k1gMnSc1	Josef
Mengele	Mengel	k1gInSc2	Mengel
Podrobnější	podrobný	k2eAgFnPc1d2	podrobnější
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Schindlerův	Schindlerův	k2eAgInSc1d1	Schindlerův
seznam	seznam	k1gInSc1	seznam
(	(	kIx(	(
<g/>
soundtrack	soundtrack	k1gInSc1	soundtrack
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hudbu	hudba	k1gFnSc4	hudba
pro	pro	k7c4	pro
film	film	k1gInSc4	film
zkomponoval	zkomponovat	k5eAaPmAgMnS	zkomponovat
John	John	k1gMnSc1	John
Williams	Williamsa	k1gFnPc2	Williamsa
<g/>
.	.	kIx.	.
</s>
<s>
Skladatel	skladatel	k1gMnSc1	skladatel
byl	být	k5eAaImAgMnS	být
filmem	film	k1gInSc7	film
ohromen	ohromit	k5eAaPmNgMnS	ohromit
a	a	k8xC	a
vycítil	vycítit	k5eAaPmAgMnS	vycítit
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
příliš	příliš	k6eAd1	příliš
velká	velký	k2eAgFnSc1d1	velká
výzva	výzva	k1gFnSc1	výzva
<g/>
.	.	kIx.	.
</s>
<s>
Spielbergovi	Spielberg	k1gMnSc3	Spielberg
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Potřebujete	potřebovat	k5eAaImIp2nP	potřebovat
lepšího	dobrý	k2eAgMnSc4d2	lepší
komponistu	komponista	k1gMnSc4	komponista
než	než	k8xS	než
jsem	být	k5eAaImIp1nS	být
já	já	k3xPp1nSc1	já
pro	pro	k7c4	pro
tento	tento	k3xDgInSc4	tento
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
vím	vědět	k5eAaImIp1nS	vědět
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
všichni	všechen	k3xTgMnPc1	všechen
jsou	být	k5eAaImIp3nP	být
mrtví	mrtvý	k2eAgMnPc1d1	mrtvý
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
Úvodní	úvodní	k2eAgFnSc4d1	úvodní
písničku	písnička	k1gFnSc4	písnička
hrál	hrát	k5eAaImAgInS	hrát
Williams	Williams	k1gInSc1	Williams
na	na	k7c4	na
piáno	piáno	k?	piáno
a	a	k8xC	a
podle	podle	k7c2	podle
Spielbergova	Spielbergův	k2eAgInSc2d1	Spielbergův
návrhu	návrh	k1gInSc2	návrh
ji	on	k3xPp3gFnSc4	on
pak	pak	k9	pak
hrál	hrát	k5eAaImAgMnS	hrát
Itzhak	Itzhak	k1gMnSc1	Itzhak
Perlman	Perlman	k1gMnSc1	Perlman
na	na	k7c6	na
houslích	housle	k1gFnPc6	housle
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
scéně	scéna	k1gFnSc6	scéna
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
ghetto	ghetto	k1gNnSc1	ghetto
zlikvidováno	zlikvidován	k2eAgNnSc1d1	zlikvidováno
Nacisty	nacista	k1gMnPc7	nacista
<g/>
,	,	kIx,	,
zpívá	zpívat	k5eAaImIp3nS	zpívat
dětský	dětský	k2eAgInSc1d1	dětský
sbor	sbor	k1gInSc1	sbor
lidovou	lidový	k2eAgFnSc4d1	lidová
písničku	písnička	k1gFnSc4	písnička
Ojfn	Ojfn	k1gMnSc1	Ojfn
pripečik	pripečik	k1gMnSc1	pripečik
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
písnička	písnička	k1gFnSc1	písnička
byla	být	k5eAaImAgFnS	být
často	často	k6eAd1	často
Spielbergovou	Spielbergův	k2eAgFnSc7d1	Spielbergova
babičkou	babička	k1gFnSc7	babička
Becky	Becka	k1gFnSc2	Becka
zpívaná	zpívaná	k1gFnSc1	zpívaná
její	její	k3xOp3gNnSc4	její
vnoučatům	vnouče	k1gNnPc3	vnouče
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
byl	být	k5eAaImAgInS	být
film	film	k1gInSc1	film
natočen	natočit	k5eAaBmNgInS	natočit
zejména	zejména	k9	zejména
v	v	k7c6	v
černobílém	černobílý	k2eAgInSc6d1	černobílý
formátu	formát	k1gInSc6	formát
<g/>
,	,	kIx,	,
mohli	moct	k5eAaImAgMnP	moct
jsme	být	k5eAaImIp1nP	být
rozlišit	rozlišit	k5eAaPmF	rozlišit
červenou	červený	k2eAgFnSc4d1	červená
barvu	barva	k1gFnSc4	barva
u	u	k7c2	u
holčičky	holčička	k1gFnSc2	holčička
v	v	k7c6	v
kabátu	kabát	k1gInSc6	kabát
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
ve	v	k7c6	v
filmu	film	k1gInSc6	film
je	být	k5eAaImIp3nS	být
dívka	dívka	k1gFnSc1	dívka
viděna	vidět	k5eAaImNgFnS	vidět
mezi	mezi	k7c7	mezi
mrtvými	mrtvý	k1gMnPc7	mrtvý
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
jsme	být	k5eAaImIp1nP	být
poznali	poznat	k5eAaPmAgMnP	poznat
podle	podle	k7c2	podle
červeného	červený	k2eAgInSc2d1	červený
kabátu	kabát	k1gInSc2	kabát
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
měla	mít	k5eAaImAgFnS	mít
stále	stále	k6eAd1	stále
na	na	k7c6	na
sobě	sebe	k3xPyFc6	sebe
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
neúmyslné	úmyslný	k2eNgNnSc1d1	neúmyslné
<g/>
,	,	kIx,	,
tento	tento	k3xDgInSc1	tento
znak	znak	k1gInSc1	znak
je	být	k5eAaImIp3nS	být
náhodou	náhodou	k6eAd1	náhodou
velmi	velmi	k6eAd1	velmi
podobný	podobný	k2eAgInSc1d1	podobný
Romě	Roma	k1gFnSc3	Roma
Ligocké	Ligocký	k2eAgFnSc3d1	Ligocká
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Krakowském	Krakowský	k2eAgNnSc6d1	Krakowský
ghettu	ghetto	k1gNnSc6	ghetto
známá	známý	k2eAgFnSc1d1	známá
svým	svůj	k3xOyFgInSc7	svůj
červeným	červený	k2eAgInSc7d1	červený
kabátem	kabát	k1gInSc7	kabát
<g/>
.	.	kIx.	.
</s>
<s>
Ligocka	Ligocka	k1gFnSc1	Ligocka
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
jejích	její	k3xOp3gFnPc2	její
smyšlenému	smyšlený	k2eAgInSc3d1	smyšlený
protějšku	protějšek	k1gInSc3	protějšek
<g/>
,	,	kIx,	,
přežila	přežít	k5eAaPmAgFnS	přežít
holokaust	holokaust	k1gInSc4	holokaust
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
premiéře	premiéra	k1gFnSc6	premiéra
filmu	film	k1gInSc2	film
napsala	napsat	k5eAaBmAgFnS	napsat
a	a	k8xC	a
publikovala	publikovat	k5eAaBmAgFnS	publikovat
svůj	svůj	k3xOyFgInSc4	svůj
vlastní	vlastní	k2eAgInSc4d1	vlastní
příběh	příběh	k1gInSc4	příběh
<g/>
,	,	kIx,	,
Dívka	dívka	k1gFnSc1	dívka
v	v	k7c6	v
červeném	červený	k2eAgInSc6d1	červený
kabátě	kabát	k1gInSc6	kabát
<g/>
:	:	kIx,	:
Vzpomínky	vzpomínka	k1gFnSc2	vzpomínka
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
scéna	scéna	k1gFnSc1	scéna
byla	být	k5eAaImAgFnS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
Zeliga	Zelig	k1gMnSc2	Zelig
Burghuta	Burghut	k1gMnSc2	Burghut
<g/>
,	,	kIx,	,
muže	muž	k1gMnSc2	muž
který	který	k3yQgInSc1	který
přežil	přežít	k5eAaPmAgMnS	přežít
Plaszow	Plaszow	k1gFnSc4	Plaszow
(	(	kIx(	(
<g/>
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
tábory	tábor	k1gInPc4	tábor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byl	být	k5eAaImAgMnS	být
před	před	k7c7	před
natáčením	natáčení	k1gNnSc7	natáčení
filmu	film	k1gInSc2	film
dotazován	dotazovat	k5eAaImNgInS	dotazovat
Spielbergem	Spielberg	k1gMnSc7	Spielberg
<g/>
,	,	kIx,	,
Burghut	Burghut	k1gMnSc1	Burghut
vypravoval	vypravovat	k5eAaImAgMnS	vypravovat
o	o	k7c6	o
děvčátku	děvčátko	k1gNnSc6	děvčátko
<g/>
,	,	kIx,	,
ne	ne	k9	ne
starší	starý	k2eAgMnSc1d2	starší
čtyř	čtyři	k4xCgMnPc2	čtyři
let	let	k1gInSc4	let
nosící	nosící	k2eAgInSc4d1	nosící
růžový	růžový	k2eAgInSc4d1	růžový
kabát	kabát	k1gInSc4	kabát
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
zastřelena	zastřelit	k5eAaPmNgFnS	zastřelit
nacistickým	nacistický	k2eAgInSc7d1	nacistický
důstojníkem	důstojník	k1gMnSc7	důstojník
přímo	přímo	k6eAd1	přímo
před	před	k7c7	před
jeho	jeho	k3xOp3gNnPc7	jeho
očima	oko	k1gNnPc7	oko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
interview	interview	k1gNnSc6	interview
řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
něco	něco	k3yInSc1	něco
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
s	s	k7c7	s
vámi	vy	k3xPp2nPc7	vy
zůstane	zůstat	k5eAaPmIp3nS	zůstat
navždy	navždy	k6eAd1	navždy
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Podle	podle	k7c2	podle
Andyho	Andy	k1gMnSc2	Andy
Patrizia	Patrizius	k1gMnSc2	Patrizius
z	z	k7c2	z
IGN	IGN	kA	IGN
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
dívka	dívka	k1gFnSc1	dívka
v	v	k7c6	v
červeném	červený	k2eAgInSc6d1	červený
kabátě	kabát	k1gInSc6	kabát
signalizovat	signalizovat	k5eAaImF	signalizovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Schindler	Schindler	k1gMnSc1	Schindler
změnil	změnit	k5eAaPmAgInS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Andre	Andr	k1gMnSc5	Andr
Caron	Caron	k1gMnSc1	Caron
se	se	k3xPyFc4	se
zajímal	zajímat	k5eAaImAgMnS	zajímat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
byla	být	k5eAaImAgFnS	být
scéna	scéna	k1gFnSc1	scéna
natočena	natočen	k2eAgFnSc1d1	natočena
aby	aby	kYmCp3nS	aby
znázorňovala	znázorňovat	k5eAaImAgFnS	znázorňovat
nevinnost	nevinnost	k1gFnSc4	nevinnost
<g/>
,	,	kIx,	,
naději	naděje	k1gFnSc4	naděje
nebo	nebo	k8xC	nebo
červenou	červený	k2eAgFnSc4d1	červená
krev	krev	k1gFnSc4	krev
židovského	židovský	k2eAgInSc2d1	židovský
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
obětovaný	obětovaný	k2eAgInSc1d1	obětovaný
v	v	k7c6	v
hrůze	hrůza	k1gFnSc6	hrůza
holokaustu	holokaust	k1gInSc2	holokaust
<g/>
.	.	kIx.	.
</s>
<s>
Spielberg	Spielberg	k1gMnSc1	Spielberg
sám	sám	k3xTgMnSc1	sám
vysvětlil	vysvětlit	k5eAaPmAgMnS	vysvětlit
tuto	tento	k3xDgFnSc4	tento
scénu	scéna	k1gFnSc4	scéna
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
Začátek	začátek	k1gInSc1	začátek
uvádí	uvádět	k5eAaImIp3nS	uvádět
rodinu	rodina	k1gFnSc4	rodina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dodržuje	dodržovat	k5eAaImIp3nS	dodržovat
šabat	šabat	k1gInSc4	šabat
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
barva	barva	k1gFnSc1	barva
ze	z	k7c2	z
začátku	začátek	k1gInSc2	začátek
filmu	film	k1gInSc2	film
bledne	blednout	k5eAaImIp3nS	blednout
<g/>
,	,	kIx,	,
objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
kouř	kouř	k1gInSc1	kouř
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
hořící	hořící	k2eAgNnPc4d1	hořící
těla	tělo	k1gNnPc4	tělo
v	v	k7c6	v
Auschwitzu	Auschwitz	k1gInSc6	Auschwitz
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
pohledu	pohled	k1gInSc2	pohled
na	na	k7c4	na
svíčky	svíčka	k1gFnPc4	svíčka
získává	získávat	k5eAaImIp3nS	získávat
oheň	oheň	k1gInSc1	oheň
znovu	znovu	k6eAd1	znovu
vřelost	vřelost	k1gFnSc4	vřelost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Schindler	Schindler	k1gMnSc1	Schindler
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
svým	svůj	k3xOyFgMnPc3	svůj
dělníkům	dělník	k1gMnPc3	dělník
dodržovat	dodržovat	k5eAaImF	dodržovat
šabat	šabat	k1gInSc4	šabat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Spielberga	Spielberg	k1gMnSc2	Spielberg
to	ten	k3xDgNnSc1	ten
přestavovalo	přestavovat	k5eAaImAgNnS	přestavovat
"	"	kIx"	"
<g/>
spravedlivý	spravedlivý	k2eAgInSc1d1	spravedlivý
třpyt	třpyt	k1gInSc1	třpyt
barvy	barva	k1gFnSc2	barva
a	a	k8xC	a
jiskru	jiskra	k1gFnSc4	jiskra
naděje	naděje	k1gFnSc2	naděje
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
okamžiku	okamžik	k1gInSc6	okamžik
vidí	vidět	k5eAaImIp3nP	vidět
lidé	člověk	k1gMnPc1	člověk
jedoucí	jedoucí	k2eAgMnPc1d1	jedoucí
v	v	k7c6	v
transportech	transport	k1gInPc6	transport
do	do	k7c2	do
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
táborů	tábor	k1gInPc2	tábor
u	u	k7c2	u
trati	trať	k1gFnSc2	trať
chlapce	chlapec	k1gMnSc2	chlapec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
varoval	varovat	k5eAaImAgInS	varovat
výmluvným	výmluvný	k2eAgNnSc7d1	výmluvné
gestem	gesto	k1gNnSc7	gesto
(	(	kIx(	(
<g/>
rukou	ruka	k1gFnSc7	ruka
si	se	k3xPyFc3	se
jakoby	jakoby	k8xS	jakoby
podřízl	podříznout	k5eAaPmAgMnS	podříznout
krk	krk	k1gInSc4	krk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k9	co
on	on	k3xPp3gMnSc1	on
mohl	moct	k5eAaImAgMnS	moct
vědět	vědět	k5eAaImF	vědět
o	o	k7c6	o
židovské	židovský	k2eAgFnSc6d1	židovská
otázce	otázka	k1gFnSc6	otázka
<g/>
,	,	kIx,	,
asi	asi	k9	asi
zůstane	zůstat	k5eAaPmIp3nS	zůstat
záhadou	záhada	k1gFnSc7	záhada
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
postava	postava	k1gFnSc1	postava
chlapce	chlapec	k1gMnPc4	chlapec
je	být	k5eAaImIp3nS	být
autentická	autentický	k2eAgFnSc1d1	autentická
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
na	na	k7c6	na
základě	základ	k1gInSc6	základ
výpovědi	výpověď	k1gFnSc2	výpověď
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
přeživších	přeživší	k2eAgMnPc2d1	přeživší
(	(	kIx(	(
<g/>
Abrahama	Abraham	k1gMnSc2	Abraham
Bombay	Bombaa	k1gMnSc2	Bombaa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prezentované	prezentovaný	k2eAgFnPc1d1	prezentovaná
ve	v	k7c6	v
francouzském	francouzský	k2eAgInSc6d1	francouzský
dokumentárním	dokumentární	k2eAgInSc6d1	dokumentární
filmu	film	k1gInSc6	film
Shoah	Shoaha	k1gFnPc2	Shoaha
z	z	k7c2	z
poloviny	polovina	k1gFnSc2	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
v	v	k7c6	v
76	[number]	k4	76
<g/>
.	.	kIx.	.
minutě	minuta	k1gFnSc6	minuta
jeho	jeho	k3xOp3gInSc7	jeho
1	[number]	k4	1
<g/>
.	.	kIx.	.
části	část	k1gFnSc2	část
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zavlečení	zavlečení	k1gNnSc6	zavlečení
vlaku	vlak	k1gInSc2	vlak
se	s	k7c7	s
židovskými	židovský	k2eAgFnPc7d1	židovská
ženami	žena	k1gFnPc7	žena
do	do	k7c2	do
Osvětimi	Osvětim	k1gFnSc2	Osvětim
proběhne	proběhnout	k5eAaPmIp3nS	proběhnout
scéna	scéna	k1gFnSc1	scéna
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
s	s	k7c7	s
hrůzou	hrůza	k1gFnSc7	hrůza
očekávají	očekávat	k5eAaImIp3nP	očekávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ze	z	k7c2	z
sprch	sprcha	k1gFnPc2	sprcha
bude	být	k5eAaImBp3nS	být
puštěn	pustit	k5eAaPmNgInS	pustit
plyn	plyn	k1gInSc1	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
ale	ale	k9	ale
začne	začít	k5eAaPmIp3nS	začít
téci	téct	k5eAaImF	téct
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Identickou	identický	k2eAgFnSc4d1	identická
scénu	scéna	k1gFnSc4	scéna
ztvárnil	ztvárnit	k5eAaPmAgInS	ztvárnit
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
Juraj	Juraj	k1gMnSc1	Juraj
Herz	Herz	k1gMnSc1	Herz
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Zastihla	zastihnout	k5eAaPmAgFnS	zastihnout
mě	já	k3xPp1nSc4	já
noc	noc	k1gFnSc1	noc
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Schindlerův	Schindlerův	k2eAgInSc4d1	Schindlerův
seznam	seznam	k1gInSc4	seznam
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
filmu	film	k1gInSc2	film
Schindlerův	Schindlerův	k2eAgInSc1d1	Schindlerův
seznam	seznam	k1gInSc1	seznam
Schindlerův	Schindlerův	k2eAgInSc4d1	Schindlerův
seznam	seznam	k1gInSc4	seznam
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc3d1	filmová
databázi	databáze	k1gFnSc3	databáze
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Schindlerův	Schindlerův	k2eAgInSc1d1	Schindlerův
seznam	seznam	k1gInSc1	seznam
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Jak	jak	k6eAd1	jak
vypadají	vypadat	k5eAaImIp3nP	vypadat
místa	místo	k1gNnPc1	místo
filmování	filmování	k1gNnSc1	filmování
-	-	kIx~	-
prosinec	prosinec	k1gInSc1	prosinec
2013	[number]	k4	2013
</s>
