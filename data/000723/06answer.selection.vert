<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
režisérem	režisér	k1gMnSc7	režisér
Stevenem	Steven	k1gMnSc7	Steven
Spielbergerem	Spielberger	k1gMnSc7	Spielberger
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
knihy	kniha	k1gFnSc2	kniha
Schindlerova	Schindlerův	k2eAgFnSc1d1	Schindlerova
archa	archa	k1gFnSc1	archa
australského	australský	k2eAgMnSc2d1	australský
spisovatele	spisovatel	k1gMnSc2	spisovatel
Thomase	Thomas	k1gMnSc2	Thomas
Keneallyho	Keneally	k1gMnSc2	Keneally
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
stala	stát	k5eAaPmAgFnS	stát
bestsellerem	bestseller	k1gInSc7	bestseller
<g/>
.	.	kIx.	.
</s>
