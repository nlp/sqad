<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
házené	házená	k1gFnSc6	házená
o	o	k7c6	o
jedenácti	jedenáct	k4xCc6	jedenáct
hráčích	hráč	k1gMnPc6	hráč
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
ve	v	k7c6	v
dnech	den	k1gInPc6	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
–	–	k?	–
9	[number]	k4	9
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mistrovství	mistrovství	k1gNnSc1	mistrovství
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
osm	osm	k4xCc1	osm
mužstev	mužstvo	k1gNnPc2	mužstvo
<g/>
,	,	kIx,	,
rozdělených	rozdělený	k2eAgInPc2d1	rozdělený
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
čtyřčlenných	čtyřčlenný	k2eAgFnPc2d1	čtyřčlenná
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Vítězové	vítěz	k1gMnPc1	vítěz
skupin	skupina	k1gFnPc2	skupina
hráli	hrát	k5eAaImAgMnP	hrát
finále	finále	k1gNnSc4	finále
<g/>
,	,	kIx,	,
týmy	tým	k1gInPc4	tým
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
hrály	hrát	k5eAaImAgFnP	hrát
o	o	k7c4	o
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
a	a	k8xC	a
týmy	tým	k1gInPc4	tým
na	na	k7c6	na
třetím	třetí	k4xOgNnSc6	třetí
místě	místo	k1gNnSc6	místo
o	o	k7c4	o
páté	pátý	k4xOgNnSc4	pátý
místo	místo	k1gNnSc4	místo
a	a	k8xC	a
poslední	poslední	k2eAgFnSc4d1	poslední
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
hráli	hrát	k5eAaImAgMnP	hrát
o	o	k7c4	o
sedmé	sedmý	k4xOgNnSc4	sedmý
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výsledky	výsledek	k1gInPc1	výsledek
a	a	k8xC	a
tabulky	tabulka	k1gFnPc1	tabulka
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Finále	finále	k1gNnSc1	finále
===	===	k?	===
</s>
</p>
<p>
<s>
NDR	NDR	kA	NDR
-	-	kIx~	-
SRN	SRN	kA	SRN
<g/>
14	[number]	k4	14
<g/>
:	:	kIx,	:
<g/>
7	[number]	k4	7
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
:	:	kIx,	:
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
O	o	k7c4	o
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
===	===	k?	===
</s>
</p>
<p>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
-	-	kIx~	-
Polsko	Polsko	k1gNnSc1	Polsko
10	[number]	k4	10
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
</s>
</p>
<p>
<s>
===	===	k?	===
O	o	k7c4	o
5	[number]	k4	5
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
===	===	k?	===
</s>
</p>
<p>
<s>
Rakousko	Rakousko	k1gNnSc1	Rakousko
-	-	kIx~	-
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
22	[number]	k4	22
<g/>
:	:	kIx,	:
<g/>
19	[number]	k4	19
</s>
</p>
<p>
<s>
===	===	k?	===
O	o	k7c4	o
7	[number]	k4	7
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
===	===	k?	===
</s>
</p>
<p>
<s>
Izrael	Izrael	k1gInSc1	Izrael
-	-	kIx~	-
USA	USA	kA	USA
<g/>
16	[number]	k4	16
<g/>
:	:	kIx,	:
<g/>
5	[number]	k4	5
</s>
</p>
<p>
<s>
==	==	k?	==
Konečné	Konečné	k2eAgNnSc1d1	Konečné
pořadí	pořadí	k1gNnSc1	pořadí
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Soupisky	soupiska	k1gFnSc2	soupiska
==	==	k?	==
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
NDR	NDR	kA	NDR
</s>
</p>
<p>
<s>
Trenér	trenér	k1gMnSc1	trenér
<g/>
:	:	kIx,	:
Heinz	Heinz	k1gMnSc1	Heinz
Seiler	Seiler	k1gMnSc1	Seiler
a	a	k8xC	a
Herbert	Herbert	k1gMnSc1	Herbert
Dittrich	Dittrich	k1gMnSc1	Dittrich
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
SRN	SRN	kA	SRN
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
en	en	k?	en
<g/>
)	)	kIx)	)
IHF-Archív	IHF-Archív	k1gMnSc1	IHF-Archív
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
en	en	k?	en
<g/>
)	)	kIx)	)
Sportovní	sportovní	k2eAgFnPc1d1	sportovní
statistiky	statistika	k1gFnPc1	statistika
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
cs	cs	k?	cs
<g/>
)	)	kIx)	)
Achiv	Achiva	k1gFnPc2	Achiva
časopisů	časopis	k1gInPc2	časopis
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
