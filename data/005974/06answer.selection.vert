<s>
Hangul	Hangul	k1gInSc1
<g/>
,	,	kIx,
korejsky	korejsky	k6eAd1
<g/>
:	:	kIx,
한	한	k?
<g/>
,	,	kIx,
často	často	k6eAd1
přepisováno	přepisován	k2eAgNnSc1d1
jinak	jinak	k6eAd1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
hangŭ	hangŭ	k?
<g/>
"	"	kIx"
<g/>
,	,	kIx,
"	"	kIx"
<g/>
hangeul	hangeout	k5eAaPmAgMnS
<g/>
"	"	kIx"
<g/>
,	,	kIx,
"	"	kIx"
<g/>
hangyl	hangyl	k1gInSc1
<g/>
"	"	kIx"
<g/>
;	;	kIx,
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Koreji	Korea	k1gFnSc6
je	být	k5eAaImIp3nS
nazývaný	nazývaný	k2eAgInSc1d1
čosongul	čosongul	k1gInSc1
-	-	kIx~
조	조	k?
)	)	kIx)
je	být	k5eAaImIp3nS
korejské	korejský	k2eAgNnSc4d1
písmo	písmo	k1gNnSc4
<g/>
.	.	kIx.
</s>