<s>
Lékařská	lékařský	k2eAgFnSc1d1	lékařská
fakulta	fakulta	k1gFnSc1	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
(	(	kIx(	(
<g/>
LF	LF	kA	LF
MU	MU	kA	MU
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
devíti	devět	k4xCc2	devět
fakult	fakulta	k1gFnPc2	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
specializuje	specializovat	k5eAaBmIp3nS	specializovat
na	na	k7c4	na
výuku	výuka	k1gFnSc4	výuka
lékařských	lékařský	k2eAgInPc2d1	lékařský
oborů	obor	k1gInPc2	obor
<g/>
,	,	kIx,	,
zdravotnických	zdravotnický	k2eAgFnPc2d1	zdravotnická
specializací	specializace	k1gFnPc2	specializace
a	a	k8xC	a
vědecko-výzkumnou	vědeckoýzkumný	k2eAgFnSc4d1	vědecko-výzkumná
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
