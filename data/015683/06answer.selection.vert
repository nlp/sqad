<s>
Válka	válka	k1gFnSc1	válka
skončila	skončit	k5eAaPmAgFnS	skončit
uzavřením	uzavření	k1gNnSc7	uzavření
vestfálského	vestfálský	k2eAgInSc2d1	vestfálský
míru	mír	k1gInSc2	mír
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1648	[number]	k4	1648
<g/>
;	;	kIx,	;
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
míru	mír	k1gInSc2	mír
nejvíce	nejvíce	k6eAd1	nejvíce
získaly	získat	k5eAaPmAgInP	získat
protestantské	protestantský	k2eAgInPc1d1	protestantský
státy	stát	k1gInPc1	stát
a	a	k8xC	a
Francie	Francie	k1gFnSc1	Francie
<g/>
.	.	kIx.	.
</s>
