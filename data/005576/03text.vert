<s>
Seppuku	Seppuk	k1gMnSc3	Seppuk
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
"	"	kIx"	"
<g/>
plátkování	plátkování	k1gNnSc1	plátkování
žaludku	žaludek	k1gInSc2	žaludek
<g/>
"	"	kIx"	"
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
japonsky	japonsky	k6eAd1	japonsky
<g/>
:	:	kIx,	:
切	切	k?	切
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
japonská	japonský	k2eAgFnSc1d1	japonská
rituální	rituální	k2eAgFnSc1d1	rituální
sebevražda	sebevražda	k1gFnSc1	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
též	též	k9	též
známá	známý	k2eAgNnPc4d1	známé
jako	jako	k8xS	jako
harakiri	harakiri	k1gNnPc4	harakiri
(	(	kIx(	(
<g/>
腹	腹	k?	腹
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
"	"	kIx"	"
<g/>
řezání	řezání	k1gNnSc1	řezání
břicha	břicho	k1gNnSc2	břicho
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
tento	tento	k3xDgInSc4	tento
termín	termín	k1gInSc4	termín
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
stejnými	stejný	k2eAgInPc7d1	stejný
znaky	znak	k1gInPc7	znak
použitými	použitý	k2eAgInPc7d1	použitý
v	v	k7c6	v
obráceném	obrácený	k2eAgNnSc6d1	obrácené
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
pokládán	pokládán	k2eAgMnSc1d1	pokládán
za	za	k7c4	za
poněkud	poněkud	k6eAd1	poněkud
vulgární	vulgární	k2eAgInSc4d1	vulgární
a	a	k8xC	a
samotnými	samotný	k2eAgMnPc7d1	samotný
samuraji	samuraj	k1gMnPc7	samuraj
nebyl	být	k5eNaImAgMnS	být
nikdy	nikdy	k6eAd1	nikdy
používán	používat	k5eAaImNgMnS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Muži	muž	k1gMnPc1	muž
si	se	k3xPyFc3	se
v	v	k7c4	v
kleče	kleče	k6eAd1	kleče
prořízli	proříznout	k5eAaPmAgMnP	proříznout
břišní	břišní	k2eAgFnSc4d1	břišní
dutinu	dutina	k1gFnSc4	dutina
krátkým	krátký	k2eAgInSc7d1	krátký
samurajským	samurajský	k2eAgInSc7d1	samurajský
mečem	meč	k1gInSc7	meč
(	(	kIx(	(
<g/>
wakizaši	wakizasat	k5eAaPmIp1nSwK	wakizasat
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
překl	překl	k1gInSc1	překl
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
společník	společník	k1gMnSc1	společník
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
vodorovně	vodorovně	k6eAd1	vodorovně
zleva	zleva	k6eAd1	zleva
doprava	doprava	k6eAd1	doprava
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
mohl	moct	k5eAaImAgInS	moct
následovat	následovat	k5eAaImF	následovat
ještě	ještě	k6eAd1	ještě
svislý	svislý	k2eAgInSc1d1	svislý
řez	řez	k1gInSc1	řez
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
jim	on	k3xPp3gMnPc3	on
pobočník	pobočník	k1gMnSc1	pobočník
usekl	useknout	k5eAaPmAgInS	useknout
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tak	tak	k6eAd1	tak
ukončil	ukončit	k5eAaPmAgInS	ukončit
jejich	jejich	k3xOp3gNnSc4	jejich
utrpení	utrpení	k1gNnSc4	utrpení
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
bojovníci	bojovník	k1gMnPc1	bojovník
ale	ale	k8xC	ale
dali	dát	k5eAaPmAgMnP	dát
před	před	k7c7	před
stětím	stětí	k1gNnSc7	stětí
přednost	přednost	k1gFnSc1	přednost
vlastnoručnímu	vlastnoruční	k2eAgNnSc3d1	vlastnoruční
probodnutí	probodnutí	k1gNnSc3	probodnutí
hrdla	hrdlo	k1gNnSc2	hrdlo
či	či	k8xC	či
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Samotný	samotný	k2eAgInSc1d1	samotný
rituál	rituál	k1gInSc1	rituál
seppuku	seppuk	k1gInSc2	seppuk
sestával	sestávat	k5eAaImAgInS	sestávat
z	z	k7c2	z
meditace	meditace	k1gFnSc2	meditace
<g/>
,	,	kIx,	,
složení	složení	k1gNnSc2	složení
krátké	krátký	k2eAgFnSc2d1	krátká
básně	báseň	k1gFnSc2	báseň
(	(	kIx(	(
<g/>
haiku	haiku	k6eAd1	haiku
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
pohrdání	pohrdání	k1gNnSc2	pohrdání
smrtí	smrt	k1gFnPc2	smrt
a	a	k8xC	a
samotné	samotný	k2eAgFnSc2d1	samotná
sebevraždy	sebevražda	k1gFnSc2	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
obrovskému	obrovský	k2eAgNnSc3d1	obrovské
nervovému	nervový	k2eAgNnSc3d1	nervové
vypětí	vypětí	k1gNnSc3	vypětí
byl	být	k5eAaImAgInS	být
ale	ale	k9	ale
rituál	rituál	k1gInSc1	rituál
málokdy	málokdy	k6eAd1	málokdy
dodržen	dodržen	k2eAgMnSc1d1	dodržen
se	se	k3xPyFc4	se
všemi	všecek	k3xTgFnPc7	všecek
náležitostmi	náležitost	k1gFnPc7	náležitost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
životě	život	k1gInSc6	život
se	se	k3xPyFc4	se
seppuku	seppuk	k1gMnSc3	seppuk
provádělo	provádět	k5eAaImAgNnS	provádět
většinou	většinou	k6eAd1	většinou
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
samuraj	samuraj	k1gMnSc1	samuraj
napadl	napadnout	k5eAaPmAgMnS	napadnout
na	na	k7c6	na
wakizaši	wakizaš	k1gInSc6	wakizaš
a	a	k8xC	a
pobočník	pobočník	k1gMnSc1	pobočník
mu	on	k3xPp3gMnSc3	on
setnul	setnout	k5eAaPmAgMnS	setnout
hlavu	hlava	k1gFnSc4	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
absenci	absence	k1gFnSc6	absence
stětí	stěť	k1gFnPc2	stěť
mohl	moct	k5eAaImAgInS	moct
konec	konec	k1gInSc1	konec
přicházet	přicházet	k5eAaImF	přicházet
velmi	velmi	k6eAd1	velmi
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
generál	generál	k1gMnSc1	generál
Takidžiró	Takidžiró	k1gFnSc4	Takidžiró
Óniši	Óniše	k1gFnSc4	Óniše
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
při	při	k7c6	při
seppuku	seppuk	k1gInSc6	seppuk
umíral	umírat	k5eAaImAgMnS	umírat
17	[number]	k4	17
hodin	hodina	k1gFnPc2	hodina
za	za	k7c2	za
plného	plný	k2eAgNnSc2d1	plné
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
.	.	kIx.	.
</s>
<s>
Seppuku	Seppuk	k1gInSc3	Seppuk
bylo	být	k5eAaImAgNnS	být
součástí	součást	k1gFnSc7	součást
samurajského	samurajský	k2eAgInSc2d1	samurajský
kodexu	kodex	k1gInSc2	kodex
(	(	kIx(	(
<g/>
bušidó	bušidó	k?	bušidó
–	–	k?	–
cesta	cesta	k1gFnSc1	cesta
válečníka	válečník	k1gMnSc2	válečník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
bylo	být	k5eAaImAgNnS	být
používáno	používat	k5eAaImNgNnS	používat
proto	proto	k6eAd1	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
bojovníci	bojovník	k1gMnPc1	bojovník
předešli	předejít	k5eAaPmAgMnP	předejít
zajetí	zajetí	k1gNnPc2	zajetí
nepřítelem	nepřítel	k1gMnSc7	nepřítel
a	a	k8xC	a
nebo	nebo	k8xC	nebo
získali	získat	k5eAaPmAgMnP	získat
zpět	zpět	k6eAd1	zpět
ztracenou	ztracený	k2eAgFnSc4d1	ztracená
čest	čest	k1gFnSc4	čest
<g/>
.	.	kIx.	.
</s>
<s>
Samuraj	samuraj	k1gMnSc1	samuraj
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
k	k	k7c3	k
seppuku	seppuk	k1gInSc3	seppuk
vyzván	vyzvat	k5eAaPmNgMnS	vyzvat
svým	svůj	k3xOyFgMnSc7	svůj
pánem	pán	k1gMnSc7	pán
<g/>
.	.	kIx.	.
</s>
<s>
Seppuku	Seppuk	k1gInSc3	Seppuk
bylo	být	k5eAaImAgNnS	být
také	také	k6eAd1	také
jedinou	jediný	k2eAgFnSc7d1	jediná
formou	forma	k1gFnSc7	forma
"	"	kIx"	"
<g/>
protestu	protest	k1gInSc2	protest
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
mohl	moct	k5eAaImAgMnS	moct
samuraj	samuraj	k1gMnSc1	samuraj
dát	dát	k5eAaPmF	dát
najevo	najevo	k6eAd1	najevo
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
s	s	k7c7	s
činy	čin	k1gInPc7	čin
svého	své	k1gNnSc2	své
pána	pán	k1gMnSc2	pán
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
v	v	k7c6	v
období	období	k1gNnSc6	období
Edo	Eda	k1gMnSc5	Eda
(	(	kIx(	(
<g/>
1603	[number]	k4	1603
<g/>
–	–	k?	–
<g/>
1868	[number]	k4	1868
<g/>
)	)	kIx)	)
šogunátu	šogunát	k1gInSc2	šogunát
Tokugawa	Tokugaw	k1gInSc2	Tokugaw
bylo	být	k5eAaImAgNnS	být
seppuku	seppuk	k1gMnSc3	seppuk
používáno	používán	k2eAgNnSc1d1	používáno
jako	jako	k8xS	jako
forma	forma	k1gFnSc1	forma
popravy	poprava	k1gFnSc2	poprava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
však	však	k9	však
byla	být	k5eAaImAgFnS	být
vždy	vždy	k6eAd1	vždy
výsadou	výsada	k1gFnSc7	výsada
samurajské	samurajský	k2eAgFnSc2d1	samurajská
vrstvy	vrstva	k1gFnSc2	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Břicho	břicho	k1gNnSc1	břicho
(	(	kIx(	(
<g/>
hara	hara	k1gMnSc1	hara
<g/>
)	)	kIx)	)
člověka	člověk	k1gMnSc4	člověk
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
centrum	centrum	k1gNnSc4	centrum
těla	tělo	k1gNnSc2	tělo
a	a	k8xC	a
lidské	lidský	k2eAgFnSc2d1	lidská
duševní	duševní	k2eAgFnSc2d1	duševní
aktivity	aktivita	k1gFnSc2	aktivita
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gNnSc7	jeho
proříznutím	proříznutí	k1gNnSc7	proříznutí
tak	tak	k9	tak
válečník	válečník	k1gMnSc1	válečník
obrazně	obrazně	k6eAd1	obrazně
ukazoval	ukazovat	k5eAaImAgMnS	ukazovat
čistotu	čistota	k1gFnSc4	čistota
svého	svůj	k3xOyFgNnSc2	svůj
nitra	nitro	k1gNnSc2	nitro
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
tato	tento	k3xDgFnSc1	tento
forma	forma	k1gFnSc1	forma
sebevraždy	sebevražda	k1gFnPc4	sebevražda
nebyla	být	k5eNaImAgFnS	být
povolena	povolit	k5eAaPmNgFnS	povolit
jiným	jiný	k2eAgFnPc3d1	jiná
vrstvám	vrstva	k1gFnPc3	vrstva
japonského	japonský	k2eAgNnSc2d1	Japonské
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1873	[number]	k4	1873
byla	být	k5eAaImAgFnS	být
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
zrušením	zrušení	k1gNnSc7	zrušení
samurajské	samurajský	k2eAgFnSc2d1	samurajská
vrstvy	vrstva	k1gFnSc2	vrstva
(	(	kIx(	(
<g/>
vyhláška	vyhláška	k1gFnSc1	vyhláška
Haitorei	Haitore	k1gFnSc2	Haitore
v	v	k7c6	v
období	období	k1gNnSc6	období
Meidži	Meidž	k1gFnSc6	Meidž
<g/>
)	)	kIx)	)
tato	tento	k3xDgFnSc1	tento
praktika	praktika	k1gFnSc1	praktika
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
zakázána	zakázat	k5eAaPmNgFnS	zakázat
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnPc1	její
obdoby	obdoba	k1gFnPc1	obdoba
však	však	k9	však
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
i	i	k9	i
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
japonské	japonský	k2eAgFnSc6d1	japonská
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
určitý	určitý	k2eAgInSc4d1	určitý
druh	druh	k1gInSc4	druh
seppuku	seppuk	k1gInSc2	seppuk
se	se	k3xPyFc4	se
dají	dát	k5eAaPmIp3nP	dát
považovat	považovat	k5eAaImF	považovat
i	i	k9	i
sebevraždy	sebevražda	k1gFnPc1	sebevražda
japonského	japonský	k2eAgNnSc2d1	Japonské
civilního	civilní	k2eAgNnSc2d1	civilní
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
v	v	k7c6	v
době	doba	k1gFnSc6	doba
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
samuraj	samuraj	k1gMnSc1	samuraj
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
seppuku	seppuk	k1gInSc3	seppuk
spáchat	spáchat	k5eAaPmF	spáchat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
vyvržen	vyvrhnout	k5eAaPmNgMnS	vyvrhnout
ze	z	k7c2	z
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
živořil	živořit	k5eAaImAgMnS	živořit
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
okraji	okraj	k1gInSc6	okraj
(	(	kIx(	(
<g/>
nebyl	být	k5eNaImAgInS	být
<g/>
-li	i	k?	-li
popraven	popraven	k2eAgInSc1d1	popraven
jiným	jiný	k2eAgInSc7d1	jiný
<g/>
,	,	kIx,	,
potupnějším	potupný	k2eAgInSc7d2	potupný
způsobem	způsob	k1gInSc7	způsob
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zpravidla	zpravidla	k6eAd1	zpravidla
se	se	k3xPyFc4	se
takoví	takový	k3xDgMnPc1	takový
jedinci	jedinec	k1gMnPc1	jedinec
nechali	nechat	k5eAaPmAgMnP	nechat
zaměstnávat	zaměstnávat	k5eAaImF	zaměstnávat
jako	jako	k9	jako
nájemní	nájemní	k2eAgMnPc1d1	nájemní
vrahové	vrah	k1gMnPc1	vrah
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
živili	živit	k5eAaImAgMnP	živit
jako	jako	k9	jako
bandité	bandita	k1gMnPc1	bandita
a	a	k8xC	a
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
době	doba	k1gFnSc6	doba
představovali	představovat	k5eAaImAgMnP	představovat
opravdu	opravdu	k6eAd1	opravdu
reálnou	reálný	k2eAgFnSc4d1	reálná
společenskou	společenský	k2eAgFnSc4d1	společenská
hrozbu	hrozba	k1gFnSc4	hrozba
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Kabukimono	Kabukimona	k1gFnSc5	Kabukimona
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samuraj	samuraj	k1gMnSc1	samuraj
bez	bez	k7c2	bez
pána	pán	k1gMnSc2	pán
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
rónin	rónin	k1gInSc1	rónin
<g/>
.	.	kIx.	.
</s>
<s>
Podrobné	podrobný	k2eAgNnSc4d1	podrobné
svědectví	svědectví	k1gNnSc4	svědectví
o	o	k7c6	o
seppuku	seppuk	k1gInSc6	seppuk
samuraje	samuraj	k1gMnSc2	samuraj
odsouzeného	odsouzený	k1gMnSc2	odsouzený
za	za	k7c4	za
útok	útok	k1gInSc4	útok
na	na	k7c4	na
evropské	evropský	k2eAgMnPc4d1	evropský
obchodníky	obchodník	k1gMnPc4	obchodník
z	z	k7c2	z
pera	pero	k1gNnSc2	pero
anglického	anglický	k2eAgMnSc2d1	anglický
diplomata	diplomat	k1gMnSc2	diplomat
Mitforda	Mitford	k1gMnSc2	Mitford
uvádí	uvádět	k5eAaImIp3nS	uvádět
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
o	o	k7c6	o
bušidó	bušidó	k?	bušidó
Inazo	Inaza	k1gFnSc5	Inaza
Nitobe	Nitob	k1gInSc5	Nitob
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Byli	být	k5eAaImAgMnP	být
jsme	být	k5eAaImIp1nP	být
(	(	kIx(	(
<g/>
sedm	sedm	k4xCc1	sedm
cizích	cizí	k2eAgMnPc2d1	cizí
zástupců	zástupce	k1gMnPc2	zástupce
<g/>
)	)	kIx)	)
vyzváni	vyzvat	k5eAaPmNgMnP	vyzvat
japonskými	japonský	k2eAgMnPc7d1	japonský
svědky	svědek	k1gMnPc7	svědek
<g/>
,	,	kIx,	,
bychom	by	kYmCp1nP	by
se	se	k3xPyFc4	se
odebrali	odebrat	k5eAaPmAgMnP	odebrat
do	do	k7c2	do
hondo	honda	k1gFnSc5	honda
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnPc4d1	hlavní
síně	síň	k1gFnPc4	síň
chrámové	chrámový	k2eAgFnPc4d1	chrámová
<g/>
,	,	kIx,	,
kdež	kdež	k9	kdež
ceremonie	ceremonie	k1gFnSc1	ceremonie
tato	tento	k3xDgFnSc1	tento
měla	mít	k5eAaImAgFnS	mít
se	se	k3xPyFc4	se
odbývati	odbývat	k5eAaImF	odbývat
<g/>
.	.	kIx.	.
</s>
<s>
Scéna	scéna	k1gFnSc1	scéna
byla	být	k5eAaImAgFnS	být
nadmíru	nadmíru	k6eAd1	nadmíru
dojemná	dojemný	k2eAgFnSc1d1	dojemná
<g/>
.	.	kIx.	.
</s>
<s>
Veliká	veliký	k2eAgFnSc1d1	veliká
místnost	místnost	k1gFnSc1	místnost
<g/>
,	,	kIx,	,
s	s	k7c7	s
vysokými	vysoký	k2eAgInPc7d1	vysoký
dřevěnými	dřevěný	k2eAgInPc7d1	dřevěný
sloupy	sloup	k1gInPc7	sloup
podepřeným	podepřený	k2eAgInSc7d1	podepřený
stropem	strop	k1gInSc7	strop
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
jevištěm	jeviště	k1gNnSc7	jeviště
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
stropu	strop	k1gInSc2	strop
viselo	viset	k5eAaImAgNnS	viset
množství	množství	k1gNnSc1	množství
oněch	onen	k3xDgFnPc2	onen
obrovských	obrovský	k2eAgFnPc2d1	obrovská
pozlacených	pozlacený	k2eAgFnPc2d1	pozlacená
lamp	lampa	k1gFnPc2	lampa
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vídáme	vídat	k5eAaImIp1nP	vídat
v	v	k7c6	v
buddhistických	buddhistický	k2eAgFnPc6d1	buddhistická
svatyních	svatyně	k1gFnPc6	svatyně
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
hlavním	hlavní	k2eAgInSc7d1	hlavní
oltářem	oltář	k1gInSc7	oltář
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
podlaha	podlaha	k1gFnSc1	podlaha
<g/>
,	,	kIx,	,
novými	nový	k2eAgFnPc7d1	nová
rohožemi	rohož	k1gFnPc7	rohož
pokrytá	pokrytý	k2eAgFnSc1d1	pokrytá
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
málo	málo	k6eAd1	málo
zvýšena	zvýšit	k5eAaPmNgFnS	zvýšit
<g/>
,	,	kIx,	,
ležela	ležet	k5eAaImAgFnS	ležet
pokrývka	pokrývka	k1gFnSc1	pokrývka
z	z	k7c2	z
šarlatově	šarlatově	k6eAd1	šarlatově
červené	červený	k2eAgFnSc2d1	červená
plstě	plstit	k5eAaImSgMnS	plstit
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
svíce	svíce	k1gFnPc1	svíce
postavené	postavený	k2eAgFnPc1d1	postavená
ve	v	k7c6	v
stejných	stejný	k2eAgFnPc6d1	stejná
vzdálenostech	vzdálenost	k1gFnPc6	vzdálenost
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
zářily	zářit	k5eAaImAgFnP	zářit
mdlým	mdlý	k2eAgNnSc7d1	mdlé
<g/>
,	,	kIx,	,
tajemným	tajemný	k2eAgNnSc7d1	tajemné
světlem	světlo	k1gNnSc7	světlo
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jsme	být	k5eAaImIp1nP	být
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
položení	položení	k1gNnSc6	položení
mohli	moct	k5eAaImAgMnP	moct
všechno	všechen	k3xTgNnSc4	všechen
pozorovati	pozorovat	k5eAaImF	pozorovat
<g/>
.	.	kIx.	.
</s>
<s>
Oněch	onen	k3xDgNnPc2	onen
sedm	sedm	k4xCc1	sedm
japonských	japonský	k2eAgMnPc2d1	japonský
svědků	svědek	k1gMnPc2	svědek
zasedlo	zasednout	k5eAaPmAgNnS	zasednout
na	na	k7c6	na
levé	levý	k2eAgFnSc6d1	levá
straně	strana	k1gFnSc6	strana
pódia	pódium	k1gNnSc2	pódium
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
my	my	k3xPp1nPc1	my
<g/>
,	,	kIx,	,
svědkové	svědek	k1gMnPc1	svědek
cizí	cizí	k2eAgFnSc2d1	cizí
<g/>
,	,	kIx,	,
na	na	k7c4	na
pravou	pravý	k2eAgFnSc4d1	pravá
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
nás	my	k3xPp1nPc4	my
nebyl	být	k5eNaImAgInS	být
nikdo	nikdo	k3yNnSc1	nikdo
divákem	divák	k1gMnSc7	divák
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
minutách	minuta	k1gFnPc6	minuta
trapného	trapný	k2eAgNnSc2d1	trapné
čekání	čekání	k1gNnSc2	čekání
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
místnosti	místnost	k1gFnSc2	místnost
Taki	Tak	k1gFnSc2	Tak
Zenzaburo	Zenzabura	k1gFnSc5	Zenzabura
<g/>
,	,	kIx,	,
silný	silný	k2eAgInSc1d1	silný
<g/>
,	,	kIx,	,
asi	asi	k9	asi
dvaatřicet	dvaatřicet	k4xCc4	dvaatřicet
roků	rok	k1gInPc2	rok
starý	starý	k2eAgMnSc1d1	starý
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
tahů	tah	k1gInPc2	tah
velmi	velmi	k6eAd1	velmi
ušlechtilých	ušlechtilý	k2eAgMnPc2d1	ušlechtilý
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
oděn	odět	k5eAaPmNgInS	odět
v	v	k7c4	v
slavnostní	slavnostní	k2eAgInSc4d1	slavnostní
háv	háv	k1gInSc4	háv
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
byla	být	k5eAaImAgNnP	být
připevněna	připevněn	k2eAgNnPc1d1	připevněno
zvláštní	zvláštní	k2eAgNnPc1d1	zvláštní
konopná	konopný	k2eAgNnPc1d1	konopné
křídla	křídlo	k1gNnPc1	křídlo
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
na	na	k7c6	na
hávu	háv	k1gInSc6	háv
užívaném	užívaný	k2eAgInSc6d1	užívaný
při	při	k7c6	při
slavnostech	slavnost	k1gFnPc6	slavnost
<g/>
.	.	kIx.	.
</s>
<s>
Doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
jej	on	k3xPp3gInSc2	on
Kaišaku	Kaišak	k1gInSc2	Kaišak
a	a	k8xC	a
tři	tři	k4xCgMnPc1	tři
důstojníci	důstojník	k1gMnPc1	důstojník
<g/>
,	,	kIx,	,
oblečení	oblečení	k1gNnSc1	oblečení
v	v	k7c6	v
žimbaori	žimbaor	k1gInSc6	žimbaor
<g/>
,	,	kIx,	,
slavnostní	slavnostní	k2eAgInSc1d1	slavnostní
vojenský	vojenský	k2eAgInSc1d1	vojenský
kabátec	kabátec	k1gInSc1	kabátec
<g/>
.	.	kIx.	.
</s>
<s>
Připomínám	připomínat	k5eAaImIp1nS	připomínat
<g/>
,	,	kIx,	,
že	že	k8xS	že
slovo	slovo	k1gNnSc1	slovo
Kaišaku	Kaišak	k1gInSc2	Kaišak
nemíní	mínit	k5eNaImIp3nS	mínit
našeho	náš	k3xOp1gMnSc2	náš
kata	kat	k1gMnSc2	kat
<g/>
.	.	kIx.	.
</s>
<s>
Úřad	úřad	k1gInSc1	úřad
ten	ten	k3xDgInSc1	ten
jest	být	k5eAaImIp3nS	být
zastupován	zastupován	k2eAgInSc4d1	zastupován
šlechticem	šlechtic	k1gMnSc7	šlechtic
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
případech	případ	k1gInPc6	případ
vezme	vzít	k5eAaPmIp3nS	vzít
na	na	k7c4	na
se	se	k3xPyFc4	se
úřad	úřad	k1gInSc4	úřad
ten	ten	k3xDgMnSc1	ten
přítel	přítel	k1gMnSc1	přítel
nebo	nebo	k8xC	nebo
příbuzný	příbuzný	k1gMnSc1	příbuzný
odsouzeného	odsouzený	k1gMnSc2	odsouzený
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
kaišakem	kaišak	k1gInSc7	kaišak
žák	žák	k1gMnSc1	žák
Taki	Tak	k1gFnSc2	Tak
Zenzaburův	Zenzaburův	k2eAgMnSc1d1	Zenzaburův
<g/>
,	,	kIx,	,
kteréhož	kteréhož	k?	kteréhož
přátelé	přítel	k1gMnPc1	přítel
odsouzeného	odsouzený	k1gMnSc2	odsouzený
vyvolili	vyvolit	k5eAaPmAgMnP	vyvolit
se	s	k7c7	s
zřetelem	zřetel	k1gInSc7	zřetel
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
zručnost	zručnost	k1gFnSc4	zručnost
v	v	k7c6	v
šermu	šerm	k1gInSc6	šerm
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
S	s	k7c7	s
Kaišakem	Kaišak	k1gInSc7	Kaišak
po	po	k7c6	po
pravici	pravice	k1gFnSc6	pravice
kráčel	kráčet	k5eAaImAgMnS	kráčet
Taki	Take	k1gFnSc4	Take
Zenzaburo	Zenzabura	k1gFnSc5	Zenzabura
pomalu	pomalu	k6eAd1	pomalu
k	k	k7c3	k
japonským	japonský	k2eAgMnPc3d1	japonský
svědkům	svědek	k1gMnPc3	svědek
<g/>
,	,	kIx,	,
poklonil	poklonit	k5eAaPmAgMnS	poklonit
se	se	k3xPyFc4	se
před	před	k7c7	před
nimi	on	k3xPp3gInPc7	on
a	a	k8xC	a
pozdravil	pozdravit	k5eAaPmAgMnS	pozdravit
potom	potom	k6eAd1	potom
cizí	cizí	k2eAgMnSc1d1	cizí
zástupce	zástupce	k1gMnSc1	zástupce
týmž	týž	k3xTgInSc7	týž
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
ba	ba	k9	ba
spíše	spíše	k9	spíše
mnohem	mnohem	k6eAd1	mnohem
zdvořileji	zdvořile	k6eAd2	zdvořile
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
obou	dva	k4xCgNnPc2	dva
stran	strana	k1gFnPc2	strana
byl	být	k5eAaImAgInS	být
pozdrav	pozdrav	k1gInSc1	pozdrav
slavnostně	slavnostně	k6eAd1	slavnostně
opětován	opětovat	k5eAaImNgInS	opětovat
<g/>
.	.	kIx.	.
</s>
<s>
Pomalu	pomalu	k6eAd1	pomalu
a	a	k8xC	a
důstojně	důstojně	k6eAd1	důstojně
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
potom	potom	k6eAd1	potom
odsouzenec	odsouzenec	k1gMnSc1	odsouzenec
na	na	k7c4	na
pódium	pódium	k1gNnSc4	pódium
<g/>
,	,	kIx,	,
poklonil	poklonit	k5eAaPmAgMnS	poklonit
se	se	k3xPyFc4	se
dvakráte	dvakráte	k6eAd1	dvakráte
velmi	velmi	k6eAd1	velmi
hluboce	hluboko	k6eAd1	hluboko
před	před	k7c7	před
oltářem	oltář	k1gInSc7	oltář
a	a	k8xC	a
posadil	posadit	k5eAaPmAgMnS	posadit
se	se	k3xPyFc4	se
na	na	k7c4	na
ono	onen	k3xDgNnSc4	onen
plstěné	plstěný	k2eAgNnSc4d1	plstěné
prostěradlo	prostěradlo	k1gNnSc4	prostěradlo
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
zády	záda	k1gNnPc7	záda
obrácen	obrátit	k5eAaPmNgMnS	obrátit
ku	k	k7c3	k
oltáři	oltář	k1gInSc3	oltář
<g/>
.	.	kIx.	.
</s>
<s>
Kaišaku	Kaišak	k1gInSc2	Kaišak
byl	být	k5eAaImAgInS	být
po	po	k7c6	po
pravé	pravý	k2eAgFnSc6d1	pravá
straně	strana	k1gFnSc6	strana
jeho	jeho	k3xOp3gMnSc1	jeho
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
oněch	onen	k3xDgMnPc2	onen
tří	tři	k4xCgMnPc2	tři
důstojníků	důstojník	k1gMnPc2	důstojník
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
v	v	k7c6	v
popředí	popředí	k1gNnSc6	popředí
nesa	nést	k5eAaImSgInS	nést
podstavec	podstavec	k1gInSc1	podstavec
<g/>
,	,	kIx,	,
jakého	jaký	k3yRgInSc2	jaký
užívá	užívat	k5eAaImIp3nS	užívat
se	se	k3xPyFc4	se
k	k	k7c3	k
obětem	oběť	k1gFnPc3	oběť
chrámovým	chrámový	k2eAgFnPc3d1	chrámová
<g/>
;	;	kIx,	;
na	na	k7c6	na
podstavci	podstavec	k1gInSc6	podstavec
tom	ten	k3xDgNnSc6	ten
ležel	ležet	k5eAaImAgMnS	ležet
v	v	k7c6	v
papíře	papír	k1gInSc6	papír
zabalený	zabalený	k2eAgMnSc1d1	zabalený
wakizaši	wakizasat	k5eAaPmIp1nSwK	wakizasat
<g/>
,	,	kIx,	,
japonský	japonský	k2eAgInSc1d1	japonský
krátký	krátký	k2eAgInSc1d1	krátký
meč	meč	k1gInSc1	meč
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
dýka	dýka	k1gFnSc1	dýka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
ostří	ostří	k1gNnSc1	ostří
i	i	k8xC	i
špička	špička	k1gFnSc1	špička
jsou	být	k5eAaImIp3nP	být
jako	jako	k9	jako
břitva	břitva	k1gFnSc1	břitva
nabroušeny	nabroušen	k2eAgFnPc1d1	nabroušena
<g/>
.	.	kIx.	.
</s>
<s>
Podal	podat	k5eAaPmAgMnS	podat
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
pokleknuv	pokleknout	k5eAaPmDgInS	pokleknout
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
odsouzenému	odsouzený	k1gMnSc3	odsouzený
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
přijal	přijmout	k5eAaPmAgMnS	přijmout
tento	tento	k3xDgInSc4	tento
meč	meč	k1gInSc4	meč
s	s	k7c7	s
poklonou	poklona	k1gFnSc7	poklona
<g/>
,	,	kIx,	,
oběma	dva	k4xCgMnPc7	dva
rukama	ruka	k1gFnPc7	ruka
nad	nad	k7c4	nad
hlavu	hlava	k1gFnSc4	hlava
pozvednul	pozvednout	k5eAaPmAgMnS	pozvednout
a	a	k8xC	a
potom	potom	k6eAd1	potom
před	před	k7c4	před
sebe	sebe	k3xPyFc4	sebe
položil	položit	k5eAaPmAgMnS	položit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Po	po	k7c6	po
druhém	druhý	k4xOgMnSc6	druhý
<g/>
,	,	kIx,	,
hlubokém	hluboký	k2eAgInSc6d1	hluboký
úklonu	úklon	k1gInSc6	úklon
pravil	pravit	k5eAaBmAgMnS	pravit
Taki	Take	k1gFnSc4	Take
Zenzaburo	Zenzabura	k1gFnSc5	Zenzabura
hlasem	hlas	k1gInSc7	hlas
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
prozrazoval	prozrazovat	k5eAaImAgInS	prozrazovat
tolik	tolik	k6eAd1	tolik
<g />
.	.	kIx.	.
</s>
<s>
pohnutí	pohnutí	k1gNnSc4	pohnutí
a	a	k8xC	a
zdráhání	zdráhání	k1gNnSc4	zdráhání
jako	jako	k8xC	jako
hlas	hlas	k1gInSc4	hlas
muže	muž	k1gMnSc2	muž
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
skládá	skládat	k5eAaImIp3nS	skládat
trapné	trapný	k2eAgNnSc4d1	trapné
přiznání	přiznání	k1gNnSc4	přiznání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
pohnutí	pohnutí	k1gNnSc2	pohnutí
jediné	jediný	k2eAgFnSc2d1	jediná
žilky	žilka	k1gFnSc2	žilka
v	v	k7c6	v
obličeji	obličej	k1gInSc6	obličej
<g/>
:	:	kIx,	:
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
já	já	k3xPp1nSc1	já
sám	sám	k3xTgInSc4	sám
dal	dát	k5eAaPmAgInS	dát
jsem	být	k5eAaImIp1nS	být
nezákonitým	zákonitý	k2eNgInSc7d1	zákonitý
způsobem	způsob	k1gInSc7	způsob
rozkaz	rozkaz	k1gInSc1	rozkaz
ku	k	k7c3	k
střelbě	střelba	k1gFnSc3	střelba
na	na	k7c6	na
cizince	cizinka	k1gFnSc6	cizinka
v	v	k7c6	v
Kobe	Kobe	k1gFnSc6	Kobe
a	a	k8xC	a
nařídil	nařídit	k5eAaPmAgMnS	nařídit
jsem	být	k5eAaImIp1nS	být
střelbu	střelba	k1gFnSc4	střelba
i	i	k9	i
potom	potom	k6eAd1	potom
<g/>
,	,	kIx,	,
když	když	k8xS	když
tito	tento	k3xDgMnPc1	tento
chtěli	chtít	k5eAaImAgMnP	chtít
utéci	utéct	k5eAaPmF	utéct
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
zločin	zločin	k1gInSc1	zločin
tento	tento	k3xDgInSc1	tento
rozpářu	rozpárat	k5eAaPmIp1nS	rozpárat
nyní	nyní	k6eAd1	nyní
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
a	a	k8xC	a
prosím	prosit	k5eAaImIp1nS	prosit
všechny	všechen	k3xTgMnPc4	všechen
přítomné	přítomný	k1gMnPc4	přítomný
<g/>
,	,	kIx,	,
by	by	kYmCp3nP	by
poctili	poctít	k5eAaPmAgMnP	poctít
mne	já	k3xPp1nSc4	já
svým	svůj	k3xOyFgNnSc7	svůj
svědectvím	svědectví	k1gNnSc7	svědectví
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Mluvčí	mluvčí	k1gMnSc1	mluvčí
pokloniv	poklonit	k5eAaPmDgInS	poklonit
se	se	k3xPyFc4	se
ještě	ještě	k9	ještě
jednou	jeden	k4xCgFnSc7	jeden
<g/>
,	,	kIx,	,
spustil	spustit	k5eAaPmAgMnS	spustit
svůj	svůj	k3xOyFgInSc4	svůj
vrchní	vrchní	k2eAgInSc4d1	vrchní
šat	šat	k1gInSc4	šat
dolů	dolů	k6eAd1	dolů
až	až	k9	až
po	po	k7c4	po
pás	pás	k1gInSc4	pás
a	a	k8xC	a
seděl	sedět	k5eAaImAgMnS	sedět
takto	takto	k6eAd1	takto
po	po	k7c4	po
boky	bok	k1gInPc4	bok
obnažen	obnažen	k2eAgInSc1d1	obnažen
<g/>
.	.	kIx.	.
</s>
<s>
Pečlivě	pečlivě	k6eAd1	pečlivě
zastrčil	zastrčit	k5eAaPmAgInS	zastrčit
potom	potom	k6eAd1	potom
dle	dle	k7c2	dle
starého	starý	k2eAgInSc2d1	starý
zvyku	zvyk	k1gInSc2	zvyk
rukávy	rukáv	k1gInPc7	rukáv
obleku	oblek	k1gInSc2	oblek
svého	své	k1gNnSc2	své
pod	pod	k7c4	pod
kolena	koleno	k1gNnPc4	koleno
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
znemožnil	znemožnit	k5eAaPmAgInS	znemožnit
pád	pád	k1gInSc1	pád
těla	tělo	k1gNnSc2	tělo
na	na	k7c4	na
zad	zad	k1gInSc4	zad
<g/>
;	;	kIx,	;
urozený	urozený	k2eAgMnSc1d1	urozený
Japonec	Japonec	k1gMnSc1	Japonec
umíraje	umírat	k5eAaImSgInS	umírat
musí	muset	k5eAaImIp3nP	muset
padati	padat	k5eAaImF	padat
v	v	k7c6	v
před	před	k7c7	před
<g/>
.	.	kIx.	.
</s>
<s>
Klidně	klidně	k6eAd1	klidně
a	a	k8xC	a
jistě	jistě	k6eAd1	jistě
vzal	vzít	k5eAaPmAgMnS	vzít
do	do	k7c2	do
ruky	ruka	k1gFnSc2	ruka
dýku	dýka	k1gFnSc4	dýka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ležela	ležet	k5eAaImAgFnS	ležet
až	až	k9	až
dosud	dosud	k6eAd1	dosud
před	před	k7c7	před
ním	on	k3xPp3gNnSc7	on
<g/>
,	,	kIx,	,
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
ji	on	k3xPp3gFnSc4	on
zamyšlen	zamyšlen	k2eAgMnSc1d1	zamyšlen
<g/>
,	,	kIx,	,
ba	ba	k9	ba
skoro	skoro	k6eAd1	skoro
lásky	láska	k1gFnSc2	láska
plným	plný	k2eAgInSc7d1	plný
zrakem	zrak	k1gInSc7	zrak
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
jednou	jeden	k4xCgFnSc7	jeden
hleděl	hledět	k5eAaImAgInS	hledět
sebrati	sebrat	k5eAaPmF	sebrat
všechny	všechen	k3xTgFnPc4	všechen
myšlenky	myšlenka	k1gFnPc4	myšlenka
a	a	k8xC	a
potom	potom	k6eAd1	potom
vrazil	vrazit	k5eAaPmAgMnS	vrazit
dýku	dýka	k1gFnSc4	dýka
tu	tu	k6eAd1	tu
hluboko	hluboko	k6eAd1	hluboko
pod	pod	k7c7	pod
pasem	pas	k1gInSc7	pas
do	do	k7c2	do
levé	levý	k2eAgFnSc2d1	levá
strany	strana	k1gFnSc2	strana
těla	tělo	k1gNnSc2	tělo
<g/>
,	,	kIx,	,
táhl	táhnout	k5eAaImAgMnS	táhnout
jí	jíst	k5eAaImIp3nS	jíst
pomalu	pomalu	k6eAd1	pomalu
ku	k	k7c3	k
straně	strana	k1gFnSc3	strana
pravé	pravá	k1gFnSc2	pravá
a	a	k8xC	a
obrátiv	obrátit	k5eAaPmDgInS	obrátit
ji	on	k3xPp3gFnSc4	on
potom	potom	k6eAd1	potom
v	v	k7c6	v
ráně	rána	k1gFnSc6	rána
<g/>
,	,	kIx,	,
učinil	učinit	k5eAaPmAgInS	učinit
krátký	krátký	k2eAgInSc4d1	krátký
řez	řez	k1gInSc4	řez
směrem	směr	k1gInSc7	směr
nahoru	nahoru	k6eAd1	nahoru
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
hrozné	hrozný	k2eAgFnSc6d1	hrozná
chvíli	chvíle	k1gFnSc6	chvíle
nepohnul	pohnout	k5eNaPmAgMnS	pohnout
ani	ani	k9	ani
žilkou	žilka	k1gFnSc7	žilka
v	v	k7c6	v
obličeji	obličej	k1gInSc6	obličej
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
dýku	dýka	k1gFnSc4	dýka
z	z	k7c2	z
rány	rána	k1gFnSc2	rána
vytáhl	vytáhnout	k5eAaPmAgMnS	vytáhnout
<g/>
,	,	kIx,	,
uklonil	uklonit	k5eAaPmAgMnS	uklonit
se	se	k3xPyFc4	se
ku	k	k7c3	k
předu	příst	k5eAaImIp1nS	příst
a	a	k8xC	a
natáhl	natáhnout	k5eAaPmAgMnS	natáhnout
krk	krk	k1gInSc4	krk
<g/>
;	;	kIx,	;
nyní	nyní	k6eAd1	nyní
teprve	teprve	k6eAd1	teprve
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
známka	známka	k1gFnSc1	známka
bolesti	bolest	k1gFnSc2	bolest
na	na	k7c4	na
jeho	on	k3xPp3gInSc4	on
obličej	obličej	k1gInSc4	obličej
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
neprozradil	prozradit	k5eNaPmAgMnS	prozradit
bolest	bolest	k1gFnSc4	bolest
ni	on	k3xPp3gFnSc4	on
jediným	jediný	k2eAgInSc7d1	jediný
vzdechem	vzdech	k1gInSc7	vzdech
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
okamžiku	okamžik	k1gInSc6	okamžik
tomto	tento	k3xDgInSc6	tento
vyskočil	vyskočit	k5eAaPmAgInS	vyskočit
Kaišaku	Kaišak	k1gMnSc3	Kaišak
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dosud	dosud	k6eAd1	dosud
vedle	vedle	k7c2	vedle
odsouzence	odsouzenec	k1gMnSc2	odsouzenec
klečel	klečet	k5eAaImAgMnS	klečet
a	a	k8xC	a
každý	každý	k3xTgInSc4	každý
pohyb	pohyb	k1gInSc4	pohyb
jeho	jeho	k3xOp3gMnSc3	jeho
ostře	ostro	k6eAd1	ostro
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
mávnul	mávnout	k5eAaPmAgInS	mávnout
mečem	meč	k1gInSc7	meč
nad	nad	k7c7	nad
jeho	jeho	k3xOp3gFnSc7	jeho
hlavou	hlava	k1gFnSc7	hlava
–	–	k?	–
ozvala	ozvat	k5eAaPmAgFnS	ozvat
se	se	k3xPyFc4	se
těžká	těžký	k2eAgFnSc1d1	těžká
rána	rána	k1gFnSc1	rána
a	a	k8xC	a
hlomozný	hlomozný	k2eAgInSc1d1	hlomozný
pád	pád	k1gInSc1	pád
–	–	k?	–
jediným	jediný	k2eAgNnSc7d1	jediné
tnutím	tnutí	k1gNnSc7	tnutí
oddělil	oddělit	k5eAaPmAgInS	oddělit
hlavu	hlava	k1gFnSc4	hlava
od	od	k7c2	od
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Nastalo	nastat	k5eAaPmAgNnS	nastat
hrobové	hrobový	k2eAgNnSc1d1	hrobové
ticho	ticho	k1gNnSc1	ticho
<g/>
,	,	kIx,	,
přerušované	přerušovaný	k2eAgNnSc1d1	přerušované
jen	jen	k9	jen
odporným	odporný	k2eAgNnSc7d1	odporné
klokotáním	klokotání	k1gNnSc7	klokotání
krve	krev	k1gFnSc2	krev
prýštící	prýštící	k2eAgFnSc2d1	prýštící
se	se	k3xPyFc4	se
z	z	k7c2	z
bezhlavého	bezhlavý	k2eAgNnSc2d1	bezhlavé
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
které	který	k3yRgNnSc1	který
ještě	ještě	k9	ještě
před	před	k7c7	před
minutou	minuta	k1gFnSc7	minuta
bylo	být	k5eAaImAgNnS	být
udatným	udatný	k2eAgMnSc7d1	udatný
rytířem	rytíř	k1gMnSc7	rytíř
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
to	ten	k3xDgNnSc1	ten
hrozné	hrozný	k2eAgNnSc1d1	hrozné
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
"	"	kIx"	"
<g/>
Kaišaku	Kaišak	k1gInSc2	Kaišak
poklonil	poklonit	k5eAaPmAgMnS	poklonit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
otřel	otřít	k5eAaPmAgMnS	otřít
svůj	svůj	k3xOyFgInSc4	svůj
meč	meč	k1gInSc4	meč
kusem	kus	k1gInSc7	kus
papíru	papír	k1gInSc2	papír
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
měl	mít	k5eAaImAgInS	mít
k	k	k7c3	k
tomu	ten	k3xDgInSc3	ten
účelu	účel	k1gInSc3	účel
připravený	připravený	k2eAgMnSc1d1	připravený
a	a	k8xC	a
odešel	odejít	k5eAaPmAgMnS	odejít
z	z	k7c2	z
pódia	pódium	k1gNnSc2	pódium
<g/>
.	.	kIx.	.
</s>
<s>
Krví	krvit	k5eAaImIp3nS	krvit
potřísněná	potřísněný	k2eAgFnSc1d1	potřísněná
dýka	dýka	k1gFnSc1	dýka
byla	být	k5eAaImAgFnS	být
odnesena	odnést	k5eAaPmNgFnS	odnést
jakožto	jakožto	k8xS	jakožto
krvavý	krvavý	k2eAgInSc4d1	krvavý
důkaz	důkaz	k1gInSc4	důkaz
popravy	poprava	k1gFnSc2	poprava
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
zástupci	zástupce	k1gMnPc1	zástupce
Mikáda	mikádo	k1gNnSc2	mikádo
opustili	opustit	k5eAaPmAgMnP	opustit
nyní	nyní	k6eAd1	nyní
svá	svůj	k3xOyFgNnPc4	svůj
místa	místo	k1gNnPc4	místo
<g/>
,	,	kIx,	,
přešli	přejít	k5eAaPmAgMnP	přejít
ku	k	k7c3	k
místům	místo	k1gNnPc3	místo
cizích	cizí	k2eAgMnPc2d1	cizí
zástupců	zástupce	k1gMnPc2	zástupce
a	a	k8xC	a
volali	volat	k5eAaImAgMnP	volat
nás	my	k3xPp1nPc4	my
ku	k	k7c3	k
svědectví	svědectví	k1gNnSc3	svědectví
<g/>
,	,	kIx,	,
že	že	k8xS	že
ortel	ortel	k1gInSc4	ortel
smrti	smrt	k1gFnSc2	smrt
nad	nad	k7c7	nad
Taki	Taki	k1gNnSc7	Taki
Zenzaburem	Zenzabur	k1gInSc7	Zenzabur
byl	být	k5eAaImAgInS	být
věrně	věrně	k6eAd1	věrně
proveden	provést	k5eAaPmNgInS	provést
<g/>
.	.	kIx.	.
</s>
<s>
Ceremonie	ceremonie	k1gFnSc1	ceremonie
byla	být	k5eAaImAgFnS	být
skončena	skončit	k5eAaPmNgFnS	skončit
<g/>
,	,	kIx,	,
a	a	k8xC	a
my	my	k3xPp1nPc1	my
opustili	opustit	k5eAaPmAgMnP	opustit
svatyni	svatyně	k1gFnSc4	svatyně
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Právo	právo	k1gNnSc1	právo
na	na	k7c4	na
důstojný	důstojný	k2eAgInSc4d1	důstojný
odchod	odchod	k1gInSc4	odchod
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
světa	svět	k1gInSc2	svět
měly	mít	k5eAaImAgFnP	mít
i	i	k9	i
ženy	žena	k1gFnPc1	žena
z	z	k7c2	z
třídy	třída	k1gFnSc2	třída
válečníků	válečník	k1gMnPc2	válečník
<g/>
.	.	kIx.	.
</s>
<s>
Způsoby	způsob	k1gInPc1	způsob
jejich	jejich	k3xOp3gFnPc2	jejich
sebevražd	sebevražda	k1gFnPc2	sebevražda
se	se	k3xPyFc4	se
lišily	lišit	k5eAaImAgFnP	lišit
případ	případ	k1gInSc4	případ
od	od	k7c2	od
případu	případ	k1gInSc2	případ
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
si	se	k3xPyFc3	se
dýkou	dýka	k1gFnSc7	dýka
(	(	kIx(	(
<g/>
tantó	tantó	k?	tantó
<g/>
)	)	kIx)	)
prořízly	proříznout	k5eAaPmAgFnP	proříznout
hrdlo	hrdlo	k1gNnSc4	hrdlo
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
nejvíce	nejvíce	k6eAd1	nejvíce
popularizovaný	popularizovaný	k2eAgInSc4d1	popularizovaný
způsob	způsob	k1gInSc4	způsob
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Běžné	běžný	k2eAgFnSc2d1	běžná
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
nalehnutí	nalehnutí	k1gNnSc1	nalehnutí
na	na	k7c4	na
čepel	čepel	k1gFnSc4	čepel
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
ženy	žena	k1gFnPc1	žena
daly	dát	k5eAaPmAgFnP	dát
přednost	přednost	k1gFnSc4	přednost
rozpárání	rozpárání	k1gNnSc2	rozpárání
břicha	břicho	k1gNnSc2	břicho
po	po	k7c6	po
mužském	mužský	k2eAgInSc6d1	mužský
způsobu	způsob	k1gInSc6	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Předtím	předtím	k6eAd1	předtím
si	se	k3xPyFc3	se
však	však	k9	však
svázaly	svázat	k5eAaPmAgFnP	svázat
nohy	noha	k1gFnPc1	noha
u	u	k7c2	u
kotníků	kotník	k1gInPc2	kotník
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tělo	tělo	k1gNnSc1	tělo
nezaujalo	zaujmout	k5eNaPmAgNnS	zaujmout
ve	v	k7c6	v
smrtelných	smrtelný	k2eAgFnPc6d1	smrtelná
křečích	křeč	k1gFnPc6	křeč
necudnou	cudný	k2eNgFnSc4d1	necudná
polohu	poloha	k1gFnSc4	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
akt	akt	k1gInSc4	akt
následování	následování	k1gNnSc2	následování
pána	pán	k1gMnSc2	pán
ve	v	k7c6	v
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Posledním	poslední	k2eAgInSc7d1	poslední
slavným	slavný	k2eAgInSc7d1	slavný
případem	případ	k1gInSc7	případ
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
džunši	džunše	k1gFnSc4	džunše
generála	generál	k1gMnSc2	generál
Maresuke	Maresuk	k1gMnSc2	Maresuk
Nogiho	Nogi	k1gMnSc2	Nogi
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
následoval	následovat	k5eAaImAgInS	následovat
ve	v	k7c6	v
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
pána	pán	k1gMnSc2	pán
<g/>
,	,	kIx,	,
císaře	císař	k1gMnSc2	císař
Meidži	Meidž	k1gFnSc6	Meidž
<g/>
.	.	kIx.	.
</s>
<s>
Seppuku	Seppuk	k1gInSc2	Seppuk
jako	jako	k9	jako
pokárání	pokárání	k1gNnSc1	pokárání
svého	svůj	k3xOyFgMnSc2	svůj
pána	pán	k1gMnSc2	pán
<g/>
.	.	kIx.	.
</s>
<s>
Používalo	používat	k5eAaImAgNnS	používat
se	se	k3xPyFc4	se
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
všechna	všechen	k3xTgNnPc4	všechen
ostatní	ostatní	k1gNnPc4	ostatní
upozornění	upozornění	k1gNnSc2	upozornění
na	na	k7c4	na
nevhodnost	nevhodnost	k1gFnSc4	nevhodnost
pánova	pánův	k2eAgNnSc2d1	Pánovo
chování	chování	k1gNnSc2	chování
selhala	selhat	k5eAaPmAgFnS	selhat
<g/>
.	.	kIx.	.
</s>
<s>
Samuraj	samuraj	k1gMnSc1	samuraj
nikdy	nikdy	k6eAd1	nikdy
nemohl	moct	k5eNaImAgMnS	moct
výčitky	výčitka	k1gFnPc4	výčitka
projevovat	projevovat	k5eAaImF	projevovat
přímo	přímo	k6eAd1	přímo
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
nejzářnější	zářný	k2eAgInSc4d3	nejzářnější
příklad	příklad	k1gInSc4	příklad
kanši	kansat	k5eAaPmIp1nSwK	kansat
provedl	provést	k5eAaPmAgMnS	provést
Nakacukasa	Nakacukas	k1gMnSc4	Nakacukas
Hirate	Hirat	k1gMnSc5	Hirat
Masahide	Masahid	k1gMnSc5	Masahid
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
tak	tak	k6eAd1	tak
učinil	učinit	k5eAaImAgMnS	učinit
roku	rok	k1gInSc2	rok
1553	[number]	k4	1553
jako	jako	k8xS	jako
nesouhlas	nesouhlas	k1gInSc4	nesouhlas
s	s	k7c7	s
chováním	chování	k1gNnSc7	chování
svého	svůj	k3xOyFgMnSc2	svůj
pána	pán	k1gMnSc2	pán
<g/>
,	,	kIx,	,
Nobunagy	Nobunaga	k1gFnSc2	Nobunaga
Ody	Ody	k1gFnSc2	Ody
<g/>
.	.	kIx.	.
</s>
<s>
Ukončení	ukončení	k1gNnSc1	ukončení
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
jako	jako	k8xS	jako
výraz	výraz	k1gInSc1	výraz
znechucení	znechucení	k1gNnSc2	znechucení
nastalou	nastalý	k2eAgFnSc7d1	nastalá
situací	situace	k1gFnSc7	situace
<g/>
.	.	kIx.	.
</s>
<s>
Vůbec	vůbec	k9	vůbec
posledním	poslední	k2eAgInSc7d1	poslední
známým	známý	k2eAgInSc7d1	známý
případem	případ	k1gInSc7	případ
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
dal	dát	k5eAaPmAgInS	dát
charakterizovat	charakterizovat	k5eAaBmF	charakterizovat
jako	jako	k8xC	jako
Kanši	Kanše	k1gFnSc4	Kanše
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
Funši	Funš	k1gInSc3	Funš
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
seppuku	seppuk	k1gInSc3	seppuk
spisovatele	spisovatel	k1gMnSc2	spisovatel
a	a	k8xC	a
dramatika	dramatik	k1gMnSc2	dramatik
Mišimy	Mišima	k1gFnSc2	Mišima
Jukia	Jukius	k1gMnSc2	Jukius
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
25	[number]	k4	25
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1970	[number]	k4	1970
spáchal	spáchat	k5eAaPmAgInS	spáchat
seppuku	seppuk	k1gInSc3	seppuk
v	v	k7c6	v
tokijském	tokijský	k2eAgNnSc6d1	Tokijské
sídle	sídlo	k1gNnSc6	sídlo
japonských	japonský	k2eAgFnPc2d1	japonská
Sil	síla	k1gFnPc2	síla
sebeobrany	sebeobrana	k1gFnSc2	sebeobrana
(	(	kIx(	(
<g/>
Džieitai	Džieita	k1gFnSc2	Džieita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
když	když	k8xS	když
členové	člen	k1gMnPc1	člen
jednotek	jednotka	k1gFnPc2	jednotka
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
vyslyšet	vyslyšet	k5eAaPmF	vyslyšet
jeho	jeho	k3xOp3gFnSc4	jeho
řeč	řeč	k1gFnSc4	řeč
o	o	k7c6	o
nutnosti	nutnost	k1gFnSc6	nutnost
znovunavrácení	znovunavrácení	k1gNnSc2	znovunavrácení
hrdosti	hrdost	k1gFnSc2	hrdost
japonské	japonský	k2eAgFnPc1d1	japonská
armádě	armáda	k1gFnSc3	armáda
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
článku	článek	k1gInSc2	článek
9	[number]	k4	9
<g/>
.	.	kIx.	.
japonské	japonský	k2eAgFnSc2d1	japonská
ústavy	ústava	k1gFnSc2	ústava
Japonci	Japonec	k1gMnPc1	Japonec
nesmějí	smát	k5eNaImIp3nP	smát
mít	mít	k5eAaImF	mít
regulérní	regulérní	k2eAgFnSc4d1	regulérní
armádu	armáda	k1gFnSc4	armáda
a	a	k8xC	a
účastnit	účastnit	k5eAaImF	účastnit
se	se	k3xPyFc4	se
zahraničních	zahraniční	k2eAgInPc2d1	zahraniční
konfliktů	konflikt	k1gInPc2	konflikt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sebelikvidace	sebelikvidace	k1gFnSc1	sebelikvidace
v	v	k7c6	v
bezvýchodné	bezvýchodný	k2eAgFnSc6d1	bezvýchodná
situaci	situace	k1gFnSc6	situace
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
seppuku	seppuk	k1gInSc3	seppuk
generála	generál	k1gMnSc2	generál
Takedových	Takedův	k2eAgFnPc2d1	Takedův
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
,	,	kIx,	,
Kansukeho	Kansuke	k1gMnSc2	Kansuke
Harujuki	Harujuk	k1gFnSc2	Harujuk
Jamamota	Jamamot	k1gMnSc2	Jamamot
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
těžce	těžce	k6eAd1	těžce
raněný	raněný	k2eAgMnSc1d1	raněný
provedl	provést	k5eAaPmAgMnS	provést
seppuku	seppuk	k1gInSc2	seppuk
v	v	k7c6	v
obleženém	obležený	k2eAgInSc6d1	obležený
hradě	hrad	k1gInSc6	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Karóši	Karóše	k1gFnSc4	Karóše
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
nebo	nebo	k8xC	nebo
sebevražda	sebevražda	k1gFnSc1	sebevražda
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
přepracování	přepracování	k1gNnSc2	přepracování
<g/>
.	.	kIx.	.
</s>
<s>
Častý	častý	k2eAgInSc1d1	častý
jev	jev	k1gInSc1	jev
zejména	zejména	k9	zejména
v	v	k7c6	v
japonských	japonský	k2eAgFnPc6d1	japonská
firmách	firma	k1gFnPc6	firma
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
řádově	řádově	k6eAd1	řádově
10	[number]	k4	10
000	[number]	k4	000
takových	takový	k3xDgInPc2	takový
případů	případ	k1gInPc2	případ
<g/>
;	;	kIx,	;
0,5	[number]	k4	0,5
%	%	kIx~	%
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
má	mít	k5eAaImIp3nS	mít
soudní	soudní	k2eAgFnSc4d1	soudní
dohru	dohra	k1gFnSc4	dohra
<g/>
.	.	kIx.	.
</s>
<s>
Takamori	Takamori	k1gNnSc1	Takamori
Saigó	Saigó	k1gFnSc2	Saigó
Minamoto	Minamota	k1gFnSc5	Minamota
Jorimasa	Jorimasa	k1gFnSc1	Jorimasa
Minamoto	Minamota	k1gFnSc5	Minamota
Jošicune	Jošicun	k1gInSc5	Jošicun
Nagamasa	Nagamas	k1gMnSc2	Nagamas
Azai	Azai	k1gNnSc2	Azai
Nobunaga	Nobunag	k1gMnSc2	Nobunag
Oda	Oda	k1gMnSc2	Oda
Kacuie	Kacuie	k1gFnSc2	Kacuie
Šibata	Šibat	k1gMnSc2	Šibat
Keisuke	Keisuk	k1gMnSc2	Keisuk
Jamanami	Jamana	k1gFnPc7	Jamana
Udžimasa	Udžimas	k1gMnSc2	Udžimas
Hódžó	Hódžó	k1gMnSc2	Hódžó
Sen	sen	k1gInSc1	sen
no	no	k9	no
Rikjú	Rikjú	k1gMnSc1	Rikjú
Maresuke	Maresuk	k1gMnSc2	Maresuk
Nogi	Nogi	k1gNnSc2	Nogi
Korečika	Korečikum	k1gNnSc2	Korečikum
Anami	Ana	k1gFnPc7	Ana
Takidžiró	Takidžiró	k1gFnSc7	Takidžiró
Óniši	Óniše	k1gFnSc3	Óniše
Jukio	Jukio	k6eAd1	Jukio
Mišima	Mišima	k1gFnSc1	Mišima
Kazan	Kazana	k1gFnPc2	Kazana
Watanabe	Watanab	k1gInSc5	Watanab
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Seppuku	Seppuk	k1gInSc2	Seppuk
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Seppuku	Seppuk	k1gInSc2	Seppuk
<g/>
,	,	kIx,	,
slavná	slavný	k2eAgFnSc1d1	slavná
scéna	scéna	k1gFnSc1	scéna
z	z	k7c2	z
japonského	japonský	k2eAgNnSc2d1	Japonské
historické	historický	k2eAgFnPc4d1	historická
filmu	film	k1gInSc2	film
Seppuku	Seppuk	k1gInSc2	Seppuk
od	od	k7c2	od
Masaki	Masak	k1gFnSc2	Masak
Kobajašiho	Kobajaši	k1gMnSc2	Kobajaši
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1962	[number]	k4	1962
</s>
