<s>
Jak	jak	k6eAd1
se	se	k3xPyFc4
jmenuje	jmenovat	k5eAaImIp3nS,k5eAaBmIp3nS
místní	místní	k2eAgFnSc1d1
varieta	varieta	k1gFnSc1
češtiny	čeština	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
používá	používat	k5eAaImIp3nS
v	v	k7c6
hovorové	hovorový	k2eAgFnSc6d1
mluvě	mluva	k1gFnSc6
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
?	?	kIx.
</s>