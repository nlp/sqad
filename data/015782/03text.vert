<s>
Hantec	Hantec	k1gMnSc1
</s>
<s>
Hantec	Hantec	k1gInSc1
je	být	k5eAaImIp3nS
místní	místní	k2eAgFnSc1d1
varieta	varieta	k1gFnSc1
češtiny	čeština	k1gFnSc2
používaná	používaný	k2eAgFnSc1d1
v	v	k7c6
hovorové	hovorový	k2eAgFnSc6d1
mluvě	mluva	k1gFnSc6
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
specifická	specifický	k2eAgFnSc1d1
mluva	mluva	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
během	během	k7c2
několika	několik	k4yIc2
staletí	staletí	k1gNnPc2
smíšením	smíšení	k1gNnSc7
hanáckých	hanácký	k2eAgNnPc2d1
nářečí	nářečí	k1gNnPc2
češtiny	čeština	k1gFnSc2
s	s	k7c7
němčinou	němčina	k1gFnSc7
brněnských	brněnský	k2eAgMnPc2d1
Němců	Němec	k1gMnPc2
(	(	kIx(
<g/>
směs	směs	k1gFnSc1
spisovné	spisovný	k2eAgFnSc2d1
němčiny	němčina	k1gFnSc2
a	a	k8xC
rakouských	rakouský	k2eAgFnPc2d1
a	a	k8xC
jihoněmeckých	jihoněmecký	k2eAgNnPc2d1
nářečí	nářečí	k1gNnPc2
<g/>
)	)	kIx)
a	a	k8xC
historickým	historický	k2eAgInSc7d1
středoevropským	středoevropský	k2eAgInSc7d1
argotem	argot	k1gInSc7
<g/>
,	,	kIx,
zejména	zejména	k9
vídeňským	vídeňský	k2eAgMnPc3d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vliv	vliv	k1gInSc4
také	také	k9
měly	mít	k5eAaImAgFnP
jidiš	jidiš	k1gNnSc4
<g/>
,	,	kIx,
romština	romština	k1gFnSc1
a	a	k8xC
italština	italština	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
a	a	k8xC
na	na	k7c6
začátku	začátek	k1gInSc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
byl	být	k5eAaImAgInS
hantec	hantec	k1gInSc1
běžně	běžně	k6eAd1
používán	používat	k5eAaImNgInS
nižšími	nízký	k2eAgFnPc7d2
společenskými	společenský	k2eAgFnPc7d1
třídami	třída	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
není	být	k5eNaImIp3nS
původní	původní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
hantecu	hantecu	k5eAaPmIp1nS
běžná	běžný	k2eAgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
jednotlivých	jednotlivý	k2eAgInPc2d1
výrazů	výraz	k1gInPc2
je	být	k5eAaImIp3nS
v	v	k7c6
lokální	lokální	k2eAgFnSc6d1
mluvě	mluva	k1gFnSc6
obecně	obecně	k6eAd1
používáno	používán	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	O	kA
do	do	k7c2
jisté	jistý	k2eAgFnSc2d1
míry	míra	k1gFnSc2
přetvoření	přetvoření	k1gNnSc1
hantecu	hantecus	k1gInSc2
a	a	k8xC
rozšíření	rozšíření	k1gNnSc1
této	tento	k3xDgFnSc2
verze	verze	k1gFnSc2
do	do	k7c2
veřejného	veřejný	k2eAgNnSc2d1
povědomí	povědomí	k1gNnSc2
se	se	k3xPyFc4
od	od	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
postarala	postarat	k5eAaPmAgFnS
skupina	skupina	k1gFnSc1
brněnských	brněnský	k2eAgMnPc2d1
umělců	umělec	k1gMnPc2
(	(	kIx(
<g/>
Franta	Franta	k1gMnSc1
Kocourek	Kocourek	k1gMnSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
Donutil	donutit	k5eAaPmAgMnS
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
Hlaváček	Hlaváček	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původ	původ	k1gInSc1
slova	slovo	k1gNnSc2
hantec	hantec	k1gInSc1
je	být	k5eAaImIp3nS
možno	možno	k6eAd1
hledat	hledat	k5eAaImF
ve	v	k7c6
výrazu	výraz	k1gInSc6
hantýrka	hantýrka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
nejznámější	známý	k2eAgNnPc4d3
slova	slovo	k1gNnPc4
soudobého	soudobý	k2eAgInSc2d1
hantecu	hantecus	k1gInSc2
patří	patřit	k5eAaImIp3nS
šalina	šalina	k1gFnSc1
(	(	kIx(
<g/>
tramvaj	tramvaj	k1gFnSc1
–	–	k?
z	z	k7c2
německého	německý	k2eAgNnSc2d1
elektrische	elektrische	k1gNnSc2
Linie	linie	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
čurina	čurina	k1gFnSc1
(	(	kIx(
<g/>
legrace	legrace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
hokna	hokna	k1gFnSc1
(	(	kIx(
<g/>
práce	práce	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zoncna	zoncna	k1gFnSc1
(	(	kIx(
<g/>
slunce	slunce	k1gNnSc1
–	–	k?
z	z	k7c2
německého	německý	k2eAgInSc2d1
Sonne	Sonn	k1gMnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Prýgl	Prýgl	k1gInSc1
(	(	kIx(
<g/>
Brněnská	brněnský	k2eAgFnSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
přehrada	přehrada	k1gFnSc1
<g/>
,	,	kIx,
z	z	k7c2
německého	německý	k2eAgMnSc2d1
Brückl	Brückl	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
love	lov	k1gInSc5
(	(	kIx(
<g/>
peníze	peníz	k1gInPc1
–	–	k?
z	z	k7c2
romštiny	romština	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
čórka	čórka	k1gFnSc1
(	(	kIx(
<g/>
krádež	krádež	k1gFnSc1
–	–	k?
z	z	k7c2
romštiny	romština	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
šaškec	šaškec	k1gInSc1
(	(	kIx(
<g/>
blázinec	blázinec	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
škopek	škopek	k1gInSc1
(	(	kIx(
<g/>
pivo	pivo	k1gNnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
koc	koc	k?
(	(	kIx(
<g/>
dívka	dívka	k1gFnSc1
–	–	k?
z	z	k7c2
německého	německý	k2eAgInSc2d1
výrazu	výraz	k1gInSc2
pro	pro	k7c4
„	„	k?
<g/>
kočku	kočka	k1gFnSc4
<g/>
“	“	k?
Katze	Katze	k1gFnSc1
v	v	k7c6
jeho	jeho	k3xOp3gFnSc6
hovorové	hovorový	k2eAgFnSc6d1
rakouské	rakouský	k2eAgFnSc6d1
výslovnosti	výslovnost	k1gFnSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Hlavní	hlavní	k2eAgInPc1d1
znaky	znak	k1gInPc1
</s>
<s>
změna	změna	k1gFnSc1
ý	ý	k?
>	>	kIx)
é	é	k0
(	(	kIx(
<g/>
býk	býk	k1gMnSc1
>	>	kIx)
bék	bék	k?
<g/>
)	)	kIx)
a	a	k8xC
často	často	k6eAd1
i	i	k9
ej	ej	k0
>	>	kIx)
é	é	k0
(	(	kIx(
<g/>
nejlepší	dobrý	k2eAgMnPc4d3
>	>	kIx)
nélepší	nélepší	k2eAgMnPc4d1
<g/>
)	)	kIx)
</s>
<s>
změna	změna	k1gFnSc1
é	é	k0
>	>	kIx)
ý	ý	k?
/	/	kIx~
í	í	k0
(	(	kIx(
<g/>
dobré	dobrý	k2eAgNnSc1d1
>	>	kIx)
dobrý	dobrý	k2eAgMnSc1d1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
v	v	k7c6
českých	český	k2eAgNnPc6d1
nářečích	nářečí	k1gNnPc6
<g/>
)	)	kIx)
</s>
<s>
protetické	protetický	k2eAgNnSc1d1
v-	v-	k?
u	u	k7c2
slov	slovo	k1gNnPc2
začínajících	začínající	k2eAgFnPc2d1
na	na	k7c4
o-	o-	k?
(	(	kIx(
<g/>
okno	okno	k1gNnSc1
>	>	kIx)
vokno	vokno	k1gNnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
v	v	k7c6
českých	český	k2eAgNnPc6d1
nářečích	nářečí	k1gNnPc6
<g/>
)	)	kIx)
</s>
<s>
regresivní	regresivní	k2eAgFnSc1d1
asimilace	asimilace	k1gFnSc1
znělosti	znělost	k1gFnSc2
sh-	sh-	k?
na	na	k7c4
[	[	kIx(
<g/>
zh-	zh-	k?
<g/>
]	]	kIx)
(	(	kIx(
<g/>
zhoda	zhodo	k1gNnPc4
<g/>
,	,	kIx,
zhánět	zhánět	k5eAaImF,k5eAaPmF
<g/>
)	)	kIx)
</s>
<s>
skupina	skupina	k1gFnSc1
-jd-	-jd-	k?
se	se	k3xPyFc4
v	v	k7c6
předponových	předponový	k2eAgInPc6d1
tvarech	tvar	k1gInPc6
slovesa	sloveso	k1gNnSc2
jít	jít	k5eAaImF
mění	měnit	k5eAaImIp3nP
na	na	k7c6
-nd-	-nd-	k?
(	(	kIx(
<g/>
přijde	přijít	k5eAaPmIp3nS
>	>	kIx)
přinde	přind	k1gMnSc5
<g/>
)	)	kIx)
</s>
<s>
substantiva	substantivum	k1gNnPc1
skloňovaná	skloňovaný	k2eAgNnPc1d1
podle	podle	k7c2
vzoru	vzor	k1gInSc2
předseda	předseda	k1gMnSc1
mají	mít	k5eAaImIp3nP
v	v	k7c6
instrumentálu	instrumentál	k1gInSc6
singuláru	singulár	k1gInSc2
koncovku	koncovka	k1gFnSc4
-em	-em	k?
(	(	kIx(
<g/>
s	s	k7c7
předsedou	předseda	k1gMnSc7
>	>	kIx)
s	s	k7c7
předsedem	předsedem	k?
<g/>
)	)	kIx)
</s>
<s>
instrumentál	instrumentál	k1gInSc1
plurálu	plurál	k1gInSc2
je	být	k5eAaImIp3nS
většinou	většinou	k6eAd1
zakončen	zakončit	k5eAaPmNgInS
na	na	k7c4
-	-	kIx~
<g/>
(	(	kIx(
<g/>
a	a	k8xC
<g/>
)	)	kIx)
<g/>
ma	ma	k?
(	(	kIx(
<g/>
s	s	k7c7
vysokéma	vysokéma	k?
chlapama	chlapama	k?
atd.	atd.	kA
<g/>
)	)	kIx)
</s>
<s>
dvojhláska	dvojhláska	k1gFnSc1
ou	ou	k0
se	se	k3xPyFc4
mění	měnit	k5eAaImIp3nS
na	na	k7c4
ó	ó	k0
(	(	kIx(
<g/>
spadnout	spadnout	k5eAaPmF
>	>	kIx)
spadnót	spadnót	k5eAaPmF
<g/>
)	)	kIx)
</s>
<s>
dativ	dativ	k1gInSc1
a	a	k8xC
lokativ	lokativ	k1gInSc1
pronomina	pronomen	k1gNnSc2
já	já	k3xPp1nSc1
má	mít	k5eAaImIp3nS
tvar	tvar	k1gInSc1
ně	on	k3xPp3gMnPc4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
osoba	osoba	k1gFnSc1
plurálu	plurál	k1gInSc2
indikativu	indikativ	k1gInSc2
prezentu	prezentu	k?
u	u	k7c2
sloves	sloveso	k1gNnPc2
je	být	k5eAaImIp3nS
zakončena	zakončit	k5eAaPmNgFnS
na	na	k7c6
-	-	kIx~
<g/>
(	(	kIx(
<g/>
ij	ij	k?
<g/>
)	)	kIx)
<g/>
ó	ó	k0
(	(	kIx(
<g/>
dělajó	dělajó	k?
<g/>
,	,	kIx,
mluvijó	mluvijó	k?
<g/>
,	,	kIx,
só	só	k?
atd.	atd.	kA
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
osoba	osoba	k1gFnSc1
singuláru	singulár	k1gInSc2
indikativu	indikativ	k1gInSc2
prézentu	prézens	k1gInSc2
u	u	k7c2
sloves	sloveso	k1gNnPc2
3	#num#	k4
<g/>
.	.	kIx.
třídy	třída	k1gFnSc2
je	být	k5eAaImIp3nS
zakončena	zakončit	k5eAaPmNgFnS
na	na	k7c6
-u	-u	k?
(	(	kIx(
<g/>
pracuju	pracovat	k5eAaImIp1nS
<g/>
,	,	kIx,
chcu	chcu	k?
<g/>
,	,	kIx,
maluju	malovat	k5eAaImIp1nS
atd.	atd.	kA
<g/>
)	)	kIx)
</s>
<s>
minulý	minulý	k2eAgInSc4d1
čas	čas	k1gInSc4
slovesa	sloveso	k1gNnSc2
chtít	chtít	k5eAaImF
má	mít	k5eAaImIp3nS
tvar	tvar	k1gInSc1
chcel	chcel	k1gInSc1
<g/>
;	;	kIx,
přítomný	přítomný	k2eAgInSc1d1
čas	čas	k1gInSc1
má	mít	k5eAaImIp3nS
tvar	tvar	k1gInSc4
chcu	chcu	k?
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
osoba	osoba	k1gFnSc1
singuláru	singulár	k1gInSc2
indikativu	indikativ	k1gInSc2
slovesa	sloveso	k1gNnSc2
být	být	k5eAaImF
má	mít	k5eAaImIp3nS
tvar	tvar	k1gInSc1
</s>
<s>
su	su	k?
jako	jako	k9
plnovýznamové	plnovýznamový	k2eAgNnSc4d1
sloveso	sloveso	k1gNnSc4
</s>
<s>
sem	sem	k6eAd1
jako	jako	k9
pomocné	pomocný	k2eAgNnSc4d1
sloveso	sloveso	k1gNnSc4
při	při	k7c6
vytváření	vytváření	k1gNnSc6
minulého	minulý	k2eAgInSc2d1
času	čas	k1gInSc2
</s>
<s>
Slovní	slovní	k2eAgFnSc1d1
zásoba	zásoba	k1gFnSc1
a	a	k8xC
tvarosloví	tvarosloví	k1gNnSc1
</s>
<s>
Slovní	slovní	k2eAgFnSc1d1
zásoba	zásoba	k1gFnSc1
hantecu	hantecu	k6eAd1
obsahuje	obsahovat	k5eAaImIp3nS
četné	četný	k2eAgInPc4d1
germanismy	germanismus	k1gInPc4
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
výpůjčky	výpůjčka	k1gFnPc4
z	z	k7c2
italštiny	italština	k1gFnSc2
<g/>
,	,	kIx,
maďarštiny	maďarština	k1gFnSc2
<g/>
,	,	kIx,
jidiš	jidiš	k1gNnSc2
nebo	nebo	k8xC
též	též	k9
z	z	k7c2
romštiny	romština	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgInPc1
výrazy	výraz	k1gInPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
mluvčími	mluvčí	k1gFnPc7
brněnské	brněnský	k2eAgFnSc2d1
hovorové	hovorový	k2eAgFnSc2d1
mluvy	mluva	k1gFnSc2
neznalými	znalý	k2eNgFnPc7d1
považovány	považován	k2eAgInPc1d1
za	za	k7c4
urážlivé	urážlivý	k2eAgNnSc4d1
–	–	k?
například	například	k6eAd1
výraz	výraz	k1gInSc1
cajzl	cajznout	k5eAaPmAgInS
označující	označující	k2eAgMnPc4d1
obyvatele	obyvatel	k1gMnPc4
Čech	Čechy	k1gFnPc2
(	(	kIx(
<g/>
z	z	k7c2
německého	německý	k2eAgMnSc2d1
Zeisig	Zeisig	k1gMnSc1
<g/>
,	,	kIx,
tj.	tj.	kA
čížek	čížek	k1gMnSc1
<g/>
)	)	kIx)
nebo	nebo	k8xC
výraz	výraz	k1gInSc1
pro	pro	k7c4
dívku	dívka	k1gFnSc4
Mařka	Mařka	k1gFnSc1
<g/>
,	,	kIx,
Havajka	Havajka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Výrazně	výrazně	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
Brně	Brno	k1gNnSc6
projevují	projevovat	k5eAaImIp3nP
rysy	rys	k1gInPc1
středomoravských	středomoravský	k2eAgNnPc2d1
nářečí	nářečí	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Známé	známý	k2eAgNnSc4d1
je	být	k5eAaImIp3nS
sloveso	sloveso	k1gNnSc4
rožnout	rožnout	k5eAaPmF
(	(	kIx(
<g/>
a	a	k8xC
jeho	jeho	k3xOp3gInSc1
imperativ	imperativ	k1gInSc1
rožni	rožeň	k1gInSc3
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
je	být	k5eAaImIp3nS
ekvivalentem	ekvivalent	k1gInSc7
spisovného	spisovný	k2eAgNnSc2d1
rozsvítit	rozsvítit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
morfologii	morfologie	k1gFnSc6
se	se	k3xPyFc4
někdy	někdy	k6eAd1
množné	množný	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
užívá	užívat	k5eAaImIp3nS
v	v	k7c6
platnosti	platnost	k1gFnSc6
čísla	číslo	k1gNnSc2
jednotného	jednotný	k2eAgNnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podstatná	podstatný	k2eAgFnSc1d1
jména	jméno	k1gNnPc4
skloňovaná	skloňovaný	k2eAgNnPc4d1
dle	dle	k7c2
vzoru	vzor	k1gInSc2
předseda	předseda	k1gMnSc1
mají	mít	k5eAaImIp3nP
v	v	k7c6
7	#num#	k4
<g/>
.	.	kIx.
pádě	pád	k1gInSc6
tvar	tvar	k1gInSc4
zakončený	zakončený	k2eAgInSc1d1
na	na	k7c4
„	„	k?
<g/>
-em	-em	k?
<g/>
“	“	k?
(	(	kIx(
<g/>
tj.	tj.	kA
(	(	kIx(
<g/>
s	s	k7c7
<g/>
)	)	kIx)
předsedem	předsedem	k?
namísto	namísto	k7c2
(	(	kIx(
<g/>
s	s	k7c7
<g/>
)	)	kIx)
předsedou	předseda	k1gMnSc7
<g/>
)	)	kIx)
a	a	k8xC
proto	proto	k8xC
je	být	k5eAaImIp3nS
v	v	k7c6
Brně	Brno	k1gNnSc6
možné	možný	k2eAgNnSc1d1
slyšet	slyšet	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
„	„	k?
<g/>
máme	mít	k5eAaImIp1nP
hodinu	hodina	k1gFnSc4
matematiky	matematika	k1gFnSc2
s	s	k7c7
Kaňkem	Kaňkem	k?
<g/>
“	“	k?
(	(	kIx(
<g/>
namísto	namísto	k7c2
spisovného	spisovný	k2eAgInSc2d1
„	„	k?
<g/>
s	s	k7c7
Kaňkou	kaňka	k1gFnSc7
<g/>
“	“	k?
<g/>
)	)	kIx)
nebo	nebo	k8xC
„	„	k?
<g/>
mám	mít	k5eAaImIp1nS
za	za	k7c4
kamoša	kamoša	k?
Jirky	Jirka	k1gFnPc4
<g/>
“	“	k?
(	(	kIx(
<g/>
namísto	namísto	k7c2
„	„	k?
<g/>
mám	mít	k5eAaImIp1nS
za	za	k7c4
kamaráda	kamarád	k1gMnSc4
Jirku	Jirka	k1gMnSc4
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Místní	místní	k2eAgInPc1d1
specifické	specifický	k2eAgInPc1d1
výrazy	výraz	k1gInPc1
se	se	k3xPyFc4
někdy	někdy	k6eAd1
objevují	objevovat	k5eAaImIp3nP
i	i	k9
v	v	k7c6
na	na	k7c4
Brno	Brno	k1gNnSc1
zaměřeném	zaměřený	k2eAgNnSc6d1
marketingu	marketing	k1gInSc6
–	–	k?
např.	např.	kA
Dopravní	dopravní	k2eAgInSc1d1
podnik	podnik	k1gInSc1
města	město	k1gNnSc2
Brna	Brno	k1gNnSc2
označuje	označovat	k5eAaImIp3nS
i	i	k9
v	v	k7c6
oficiální	oficiální	k2eAgFnSc6d1
komunikaci	komunikace	k1gFnSc6
předplatní	předplatní	k2eAgFnSc4d1
jízdenku	jízdenka	k1gFnSc4
slovem	slovo	k1gNnSc7
šalinkarta	šalinkart	k1gMnSc2
(	(	kIx(
<g/>
odvozeno	odvodit	k5eAaPmNgNnS
od	od	k7c2
hantecového	hantecový	k2eAgNnSc2d1
označení	označení	k1gNnSc2
šalina	šalina	k1gFnSc1
pro	pro	k7c4
„	„	k?
<g/>
tramvaj	tramvaj	k1gFnSc4
<g/>
“	“	k?
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
kampani	kampaň	k1gFnSc6
za	za	k7c4
přesun	přesun	k1gInSc4
hlavního	hlavní	k2eAgNnSc2d1
nádraží	nádraží	k1gNnSc2
bylo	být	k5eAaImAgNnS
používáno	používán	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
„	„	k?
<g/>
Bócháme	Bócháme	k1gMnSc1
na	na	k7c6
tom	ten	k3xDgNnSc6
fest	fest	k6eAd1
–	–	k?
Brno	Brno	k1gNnSc1
staví	stavit	k5eAaImIp3nS,k5eAaPmIp3nS,k5eAaBmIp3nS
nové	nový	k2eAgNnSc1d1
město	město	k1gNnSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ukázka	ukázka	k1gFnSc1
</s>
<s>
Úryvek	úryvek	k1gInSc1
z	z	k7c2
jedné	jeden	k4xCgFnSc2
z	z	k7c2
brněnských	brněnský	k2eAgFnPc2d1
pověstí	pověst	k1gFnPc2
„	„	k?
<g/>
O	o	k7c6
křivé	křivý	k2eAgFnSc6d1
věži	věž	k1gFnSc6
<g/>
“	“	k?
napsané	napsaný	k2eAgNnSc4d1
v	v	k7c6
hantecu	hantecum	k1gNnSc6
od	od	k7c2
Aleše	Aleš	k1gMnSc2
„	„	k?
<g/>
Agi	Agi	k1gMnSc2
<g/>
“	“	k?
Bojanovského	Bojanovský	k1gMnSc2
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
„	„	k?
</s>
<s>
Prolítlo	prolítnout	k5eAaPmAgNnS
už	už	k9
hafo	hafo	k?
jařin	jařina	k1gFnPc2
vod	voda	k1gFnPc2
dob	doba	k1gFnPc2
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
se	se	k3xPyFc4
dával	dávat	k5eAaImAgMnS
do	do	k7c2
pucu	pucu	k?
Oltecové	Oltecové	k2eAgInSc1d1
rathaus	rathaus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
jednó	jednó	k?
véšky	véšk	k1gInPc1
z	z	k7c2
Rathausu	rathaus	k1gInSc2
hodili	hodit	k5eAaImAgMnP,k5eAaPmAgMnP
drát	drát	k5eAaImF
jednomu	jeden	k4xCgNnSc3
známýmu	známýmu	k?
borcovi	borec	k1gMnSc6
<g/>
,	,	kIx,
keré	keré	k6eAd1
bóchal	bóchat	k5eAaImAgMnS,k5eAaBmAgMnS,k5eAaPmAgMnS
se	s	k7c7
šutrama	šutrama	k?
a	a	k8xC
keré	keré	k6eAd1
chodil	chodit	k5eAaImAgMnS
do	do	k7c2
bódy	bóda	k1gFnSc2
k	k	k7c3
jednemu	jednem	k1gInSc6
névymakaňéšímu	névymakaňéý	k2eAgMnSc3d2,k2eAgMnSc3d1
a	a	k8xC
nénabiťéšímu	nénabiťéý	k2eAgMnSc3d2,k2eAgMnSc3d1
mistrovi	mistr	k1gMnSc3
z	z	k7c2
Práglu	Prágl	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sice	sice	k8xC
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
do	do	k7c2
Česka	Česko	k1gNnSc2
moc	moc	k6eAd1
klapat	klapat	k5eAaImF
nechcelo	chcet	k5eNaPmAgNnS,k5eNaImAgNnS
<g/>
,	,	kIx,
ale	ale	k8xC
když	když	k8xS
fotr	fotr	k?
prolepil	prolepit	k5eAaPmAgMnS
ňáké	ňáké	k?
škopek	škopek	k1gInSc4
<g/>
,	,	kIx,
nebylo	být	k5eNaImAgNnS
co	co	k3yQnSc4,k3yInSc4,k3yRnSc4
řešit	řešit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hodil	hodit	k5eAaPmAgMnS,k5eAaImAgMnS
na	na	k7c4
sebe	sebe	k3xPyFc4
mantl	mantl	k?
a	a	k8xC
solidní	solidní	k2eAgFnPc4d1
traťůvky	traťůvka	k1gFnPc4
a	a	k8xC
vodklapal	vodklapat	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ňáké	Ňáké	k?
jár	jár	k1gInSc1
krmil	krmit	k5eAaImAgInS
lepóchy	lepócha	k1gFnPc4
rozumama	rozumama	k?
toho	ten	k3xDgMnSc4
šéfa	šéf	k1gMnSc4
z	z	k7c2
Práglu	Prágl	k1gInSc2
a	a	k8xC
když	když	k8xS
už	už	k6eAd1
byl	být	k5eAaImAgInS
skoro	skoro	k6eAd1
hotové	hotový	k2eAgInPc4d1
<g/>
,	,	kIx,
tak	tak	k6eAd1
se	se	k3xPyFc4
vo	vo	k?
něm	on	k3xPp3gNnSc6
domákli	domáknout	k5eAaPmAgMnP
véšky	véšk	k1gInPc4
ze	z	k7c2
Štatlu	Štatl	k1gInSc2
a	a	k8xC
že	že	k8xS
pré	pré	k1gNnSc1
má	mít	k5eAaImIp3nS
naklapat	naklapat	k5eAaPmF
do	do	k7c2
Brniska	Brnisko	k1gNnSc2
a	a	k8xC
helfnót	helfnót	k5eAaPmF
jim	on	k3xPp3gFnPc3
s	s	k7c7
oltecovým	oltecův	k2eAgInSc7d1
Rathausem	rathaus	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
“	“	k?
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Jan	Jan	k1gMnSc1
Hugo	Hugo	k1gMnSc1
a	a	k8xC
kol	kolo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Slovník	slovník	k1gInSc1
nespisovné	spisovný	k2eNgFnSc2d1
češtiny	čeština	k1gFnSc2
<g/>
,	,	kIx,
Maxdorf	Maxdorf	k1gMnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
vydání	vydání	k1gNnSc1
<g/>
↑	↑	k?
BLÁHA	Bláha	k1gMnSc1
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mám	mít	k5eAaImIp1nS
tedy	tedy	k9
rožnit	rožnit	k5eAaImF
<g/>
,	,	kIx,
nebo	nebo	k8xC
rozsvítit	rozsvítit	k5eAaPmF
<g/>
?	?	kIx.
</s>
<s desamb="1">
Aneb	Aneb	k?
když	když	k8xS
si	se	k3xPyFc3
Češi	Čech	k1gMnPc1
s	s	k7c7
Moravany	Moravan	k1gMnPc7
nerozumí	rozumět	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okolo	okolo	k7c2
češtiny	čeština	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
Olomouc	Olomouc	k1gFnSc1
<g/>
,	,	kIx,
2018-06-26	2018-06-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Josef	Josef	k1gMnSc1
Patočka	Patočka	k1gMnSc1
<g/>
:	:	kIx,
Kdo	kdo	k3yInSc1,k3yQnSc1,k3yRnSc1
financuje	financovat	k5eAaBmIp3nS
kampaň	kampaň	k1gFnSc4
za	za	k7c4
odsun	odsun	k1gInSc4
brněnského	brněnský	k2eAgNnSc2d1
nádraží	nádraží	k1gNnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
Referendum	referendum	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Hantec	Hantec	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
:	:	kIx,
Křivá	křivý	k2eAgFnSc1d1
věž	věž	k1gFnSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
VESPALEC	VESPALEC	kA
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znalost	znalost	k1gFnSc1
výrazových	výrazový	k2eAgInPc2d1
prostředků	prostředek	k1gInPc2
hantecu	hantecu	k5eAaPmIp1nS
mimo	mimo	k7c4
město	město	k1gNnSc4
Brno	Brno	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bakalářská	bakalářský	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Masarykova	Masarykův	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
<g/>
,	,	kIx,
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedoucí	vedoucí	k2eAgFnSc1d1
práce	práce	k1gFnSc1
Marie	Maria	k1gFnSc2
Krčmová	krčmový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Hantec	Hantec	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
hantec	hantec	k1gInSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Stránky	stránka	k1gFnPc1
věnované	věnovaný	k2eAgFnPc1d1
hantecu	hantecu	k6eAd1
</s>
<s>
Slovník	slovník	k1gInSc1
hantecu	hantecu	k5eAaPmIp1nS
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Český	český	k2eAgInSc1d1
jazyk	jazyk	k1gInSc1
|	|	kIx~
Brno	Brno	k1gNnSc1
</s>
