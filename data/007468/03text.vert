<s>
Sovy	sova	k1gFnPc1	sova
(	(	kIx(	(
<g/>
Strigiformes	Strigiformes	k1gInSc1	Strigiformes
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
početný	početný	k2eAgInSc4d1	početný
řád	řád	k1gInSc4	řád
masožravých	masožravý	k2eAgMnPc2d1	masožravý
ptáků	pták	k1gMnPc2	pták
čítající	čítající	k2eAgMnSc1d1	čítající
více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
na	na	k7c6	na
všech	všecek	k3xTgInPc6	všecek
světových	světový	k2eAgInPc6d1	světový
kontinentech	kontinent	k1gInPc6	kontinent
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Antarktidy	Antarktida	k1gFnSc2	Antarktida
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
sov	sova	k1gFnPc2	sova
jsou	být	k5eAaImIp3nP	být
samostatně	samostatně	k6eAd1	samostatně
žijící	žijící	k2eAgMnPc1d1	žijící
noční	noční	k2eAgMnPc1d1	noční
živočichové	živočich	k1gMnPc1	živočich
(	(	kIx(	(
<g/>
ačkoli	ačkoli	k8xS	ačkoli
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
výjimky	výjimka	k1gFnPc4	výjimka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
sýček	sýček	k1gMnSc1	sýček
králičí	králičí	k2eAgMnSc1d1	králičí
<g/>
,	,	kIx,	,
Athene	Athen	k1gInSc5	Athen
cunicularia	cunicularium	k1gNnSc2	cunicularium
<g/>
)	)	kIx)	)
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
adaptací	adaptace	k1gFnPc2	adaptace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jim	on	k3xPp3gMnPc3	on
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
lovit	lovit	k5eAaImF	lovit
za	za	k7c4	za
tmy	tma	k1gFnPc4	tma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
řádu	řád	k1gInSc2	řád
rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
dvě	dva	k4xCgFnPc4	dva
čeledi	čeleď	k1gFnPc4	čeleď
<g/>
:	:	kIx,	:
početnější	početní	k2eAgFnPc4d2	početnější
a	a	k8xC	a
druhově	druhově	k6eAd1	druhově
rozmanitější	rozmanitý	k2eAgFnSc4d2	rozmanitější
čeleď	čeleď	k1gFnSc4	čeleď
puštíkovití	puštíkovitý	k2eAgMnPc1d1	puštíkovitý
(	(	kIx(	(
<g/>
Strigidae	Strigidae	k1gNnSc7	Strigidae
<g/>
)	)	kIx)	)
a	a	k8xC	a
méně	málo	k6eAd2	málo
početnější	početní	k2eAgFnSc4d2	početnější
čeleď	čeleď	k1gFnSc4	čeleď
sovovití	sovovitý	k2eAgMnPc1d1	sovovitý
(	(	kIx(	(
<g/>
Tytonidae	Tytonidae	k1gNnSc7	Tytonidae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
byly	být	k5eAaImAgFnP	být
sovy	sova	k1gFnPc1	sova
považovány	považován	k2eAgFnPc1d1	považována
za	za	k7c4	za
příbuzné	příbuzná	k1gFnPc4	příbuzná
s	s	k7c7	s
dravci	dravec	k1gMnPc7	dravec
<g/>
,	,	kIx,	,
podobnost	podobnost	k1gFnSc1	podobnost
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
řády	řád	k1gInPc7	řád
je	být	k5eAaImIp3nS	být
však	však	k9	však
zapříčiněna	zapříčinit	k5eAaPmNgFnS	zapříčinit
obdobným	obdobný	k2eAgInSc7d1	obdobný
způsobem	způsob	k1gInSc7	způsob
lovu	lov	k1gInSc2	lov
a	a	k8xC	a
výživy	výživa	k1gFnSc2	výživa
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc7	jejich
nejbližší	blízký	k2eAgFnSc7d3	nejbližší
příbuznou	příbuzný	k2eAgFnSc7d1	příbuzná
skupinou	skupina	k1gFnSc7	skupina
jsou	být	k5eAaImIp3nP	být
lelkové	lelek	k1gMnPc1	lelek
<g/>
.	.	kIx.	.
</s>
<s>
Sovy	sova	k1gFnPc1	sova
mají	mít	k5eAaImIp3nP	mít
vesměs	vesměs	k6eAd1	vesměs
velmi	velmi	k6eAd1	velmi
nenápadné	nápadný	k2eNgNnSc1d1	nenápadné
"	"	kIx"	"
<g/>
maskovací	maskovací	k2eAgNnSc4d1	maskovací
<g/>
"	"	kIx"	"
zbarvení	zbarvení	k1gNnSc4	zbarvení
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
jejich	jejich	k3xOp3gNnSc1	jejich
výjimečně	výjimečně	k6eAd1	výjimečně
měkké	měkký	k2eAgNnSc1d1	měkké
peří	peří	k1gNnSc1	peří
s	s	k7c7	s
hřebínkovitým	hřebínkovitý	k2eAgInSc7d1	hřebínkovitý
okrajem	okraj	k1gInSc7	okraj
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
naprosto	naprosto	k6eAd1	naprosto
tichý	tichý	k2eAgInSc1d1	tichý
let	let	k1gInSc1	let
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
jim	on	k3xPp3gMnPc3	on
nejen	nejen	k6eAd1	nejen
napomáhá	napomáhat	k5eAaBmIp3nS	napomáhat
při	při	k7c6	při
přiblížení	přiblížení	k1gNnSc6	přiblížení
k	k	k7c3	k
nic	nic	k6eAd1	nic
netušící	tušící	k2eNgFnSc3d1	netušící
kořisti	kořist	k1gFnSc3	kořist
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
navíc	navíc	k6eAd1	navíc
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
orientaci	orientace	k1gFnSc4	orientace
sluchem	sluch	k1gInSc7	sluch
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc7	který
by	by	kYmCp3nS	by
šustot	šustot	k1gInSc1	šustot
normálních	normální	k2eAgNnPc2d1	normální
křídel	křídlo	k1gNnPc2	křídlo
rušil	rušit	k5eAaImAgInS	rušit
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mají	mít	k5eAaImIp3nP	mít
tak	tak	k6eAd1	tak
načechrané	načechraný	k2eAgNnSc1d1	načechrané
peří	peří	k1gNnSc1	peří
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
dojem	dojem	k1gInSc4	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
větší	veliký	k2eAgMnPc1d2	veliký
<g/>
,	,	kIx,	,
než	než	k8xS	než
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
očí	oko	k1gNnPc2	oko
mají	mít	k5eAaImIp3nP	mít
vějířovitě	vějířovitě	k6eAd1	vějířovitě
uspořádané	uspořádaný	k2eAgNnSc4d1	uspořádané
peří	peří	k1gNnSc4	peří
–	–	k?	–
závoj	závoj	k1gInSc1	závoj
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
dravci	dravec	k1gMnPc7	dravec
mají	mít	k5eAaImIp3nP	mít
zahnutý	zahnutý	k2eAgInSc1d1	zahnutý
zobák	zobák	k1gInSc1	zobák
<g/>
.	.	kIx.	.
</s>
<s>
Nohy	noha	k1gFnPc1	noha
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
druhů	druh	k1gInPc2	druh
opeřené	opeřený	k2eAgNnSc1d1	opeřené
po	po	k7c4	po
prsty	prst	k1gInPc4	prst
<g/>
,	,	kIx,	,
u	u	k7c2	u
některých	některý	k3yIgInPc2	některý
až	až	k9	až
po	po	k7c4	po
drápy	dráp	k1gInPc4	dráp
<g/>
;	;	kIx,	;
u	u	k7c2	u
ketup	ketup	k1gInSc4	ketup
je	být	k5eAaImIp3nS	být
běhák	běhák	k1gInSc1	běhák
zcela	zcela	k6eAd1	zcela
neopeřený	opeřený	k2eNgInSc1d1	neopeřený
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtý	čtvrtý	k4xOgInSc4	čtvrtý
prst	prst	k1gInSc4	prst
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
vratiprst	vratiprst	k1gInSc1	vratiprst
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
otočit	otočit	k5eAaPmF	otočit
dopředu	dopředu	k6eAd1	dopředu
nebo	nebo	k8xC	nebo
dozadu	dozadu	k6eAd1	dozadu
<g/>
.	.	kIx.	.
</s>
<s>
Sovy	sova	k1gFnPc1	sova
se	se	k3xPyFc4	se
společně	společně	k6eAd1	společně
s	s	k7c7	s
papoušky	papoušek	k1gMnPc7	papoušek
a	a	k8xC	a
krkavcovitými	krkavcovitý	k2eAgMnPc7d1	krkavcovitý
řadí	řadit	k5eAaImIp3nP	řadit
k	k	k7c3	k
ptákům	pták	k1gMnPc3	pták
s	s	k7c7	s
nejdokonalejším	dokonalý	k2eAgInSc7d3	nejdokonalejší
mozkem	mozek	k1gInSc7	mozek
a	a	k8xC	a
nejrozvinutější	rozvinutý	k2eAgFnSc7d3	nejrozvinutější
mozkovou	mozkový	k2eAgFnSc7d1	mozková
činností	činnost	k1gFnSc7	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Souvisí	souviset	k5eAaImIp3nS	souviset
to	ten	k3xDgNnSc1	ten
patrně	patrně	k6eAd1	patrně
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
výkonnými	výkonný	k2eAgInPc7d1	výkonný
smysly	smysl	k1gInPc7	smysl
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
zrakem	zrak	k1gInSc7	zrak
a	a	k8xC	a
sluchem	sluch	k1gInSc7	sluch
<g/>
.	.	kIx.	.
</s>
<s>
Oči	oko	k1gNnPc1	oko
sov	sova	k1gFnPc2	sova
patří	patřit	k5eAaImIp3nP	patřit
k	k	k7c3	k
nejdokonalejším	dokonalý	k2eAgInPc3d3	nejdokonalejší
zrakovým	zrakový	k2eAgInPc3d1	zrakový
orgánům	orgán	k1gInPc3	orgán
mezi	mezi	k7c7	mezi
ptáky	pták	k1gMnPc7	pták
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
teleskopického	teleskopický	k2eAgInSc2d1	teleskopický
typu	typ	k1gInSc2	typ
<g/>
,	,	kIx,	,
přizpůsobeného	přizpůsobený	k2eAgInSc2d1	přizpůsobený
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
při	při	k7c6	při
minimálním	minimální	k2eAgNnSc6d1	minimální
světle	světlo	k1gNnSc6	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
<g/>
,	,	kIx,	,
téměř	téměř	k6eAd1	téměř
kulovitou	kulovitý	k2eAgFnSc4d1	kulovitá
duhovku	duhovka	k1gFnSc4	duhovka
<g/>
,	,	kIx,	,
silně	silně	k6eAd1	silně
vyklenutou	vyklenutý	k2eAgFnSc4d1	vyklenutá
rohovku	rohovka	k1gFnSc4	rohovka
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
malý	malý	k2eAgInSc1d1	malý
oční	oční	k2eAgInSc1d1	oční
vějířek	vějířek	k1gInSc1	vějířek
<g/>
.	.	kIx.	.
</s>
<s>
Silný	silný	k2eAgInSc4d1	silný
a	a	k8xC	a
vysoký	vysoký	k2eAgInSc4d1	vysoký
sklerotikální	sklerotikální	k2eAgInSc4d1	sklerotikální
prsten	prsten	k1gInSc4	prsten
jim	on	k3xPp3gMnPc3	on
dodává	dodávat	k5eAaImIp3nS	dodávat
tvar	tvar	k1gInSc1	tvar
komolého	komolý	k2eAgInSc2d1	komolý
kužele	kužel	k1gInSc2	kužel
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
základnou	základna	k1gFnSc7	základna
vsazen	vsazen	k2eAgMnSc1d1	vsazen
nehybně	hybně	k6eNd1	hybně
do	do	k7c2	do
velké	velký	k2eAgFnSc2d1	velká
očnice	očnice	k1gFnSc2	očnice
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
na	na	k7c4	na
dálku	dálka	k1gFnSc4	dálka
sovy	sova	k1gFnSc2	sova
vidí	vidět	k5eAaImIp3nS	vidět
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
<g/>
,	,	kIx,	,
na	na	k7c4	na
předměty	předmět	k1gInPc4	předmět
zcela	zcela	k6eAd1	zcela
blízké	blízký	k2eAgInPc1d1	blízký
nedokáží	dokázat	k5eNaPmIp3nP	dokázat
kvůli	kvůli	k7c3	kvůli
nehybnosti	nehybnost	k1gFnSc3	nehybnost
očí	oko	k1gNnPc2	oko
zamířit	zamířit	k5eAaPmF	zamířit
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
při	při	k7c6	při
porcování	porcování	k1gNnSc6	porcování
kořisti	kořist	k1gFnSc2	kořist
<g/>
,	,	kIx,	,
řídí	řídit	k5eAaImIp3nS	řídit
pomocí	pomocí	k7c2	pomocí
štětinovitých	štětinovitý	k2eAgNnPc2d1	štětinovitý
pírek	pírko	k1gNnPc2	pírko
u	u	k7c2	u
kraje	kraj	k1gInSc2	kraj
zobáku	zobák	k1gInSc2	zobák
(	(	kIx(	(
<g/>
vibrisy	vibris	k1gInPc4	vibris
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zorný	zorný	k2eAgInSc1d1	zorný
úhel	úhel	k1gInSc1	úhel
obou	dva	k4xCgNnPc2	dva
očí	oko	k1gNnPc2	oko
je	být	k5eAaImIp3nS	být
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
u	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
s	s	k7c7	s
postranním	postranní	k2eAgNnSc7d1	postranní
postavením	postavení	k1gNnSc7	postavení
očí	oko	k1gNnPc2	oko
(	(	kIx(	(
<g/>
160	[number]	k4	160
<g/>
°	°	k?	°
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sovy	sova	k1gFnPc4	sova
však	však	k8xC	však
tento	tento	k3xDgInSc4	tento
nedostatek	nedostatek	k1gInSc4	nedostatek
dokáží	dokázat	k5eAaPmIp3nP	dokázat
kompenzovat	kompenzovat	k5eAaBmF	kompenzovat
schopností	schopnost	k1gFnSc7	schopnost
otáčet	otáčet	k5eAaImF	otáčet
hlavou	hlava	k1gFnSc7	hlava
až	až	k9	až
o	o	k7c4	o
270	[number]	k4	270
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
všech	všecek	k3xTgMnPc2	všecek
ostatních	ostatní	k2eAgMnPc2d1	ostatní
ptáků	pták	k1gMnPc2	pták
používají	používat	k5eAaImIp3nP	používat
při	při	k7c6	při
mrkání	mrkání	k1gNnSc6	mrkání
horní	horní	k2eAgFnSc1d1	horní
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
dolní	dolní	k2eAgNnSc4d1	dolní
víčko	víčko	k1gNnSc4	víčko
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
vyvinutý	vyvinutý	k2eAgInSc1d1	vyvinutý
mají	mít	k5eAaImIp3nP	mít
též	též	k9	též
sluch	sluch	k1gInSc4	sluch
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dokáže	dokázat	k5eAaPmIp3nS	dokázat
vnímat	vnímat	k5eAaImF	vnímat
zvuky	zvuk	k1gInPc4	zvuk
v	v	k7c6	v
rozsahu	rozsah	k1gInSc6	rozsah
od	od	k7c2	od
cca	cca	kA	cca
50	[number]	k4	50
do	do	k7c2	do
21	[number]	k4	21
000	[number]	k4	000
Hz	Hz	kA	Hz
<g/>
.	.	kIx.	.
</s>
<s>
Citlivost	citlivost	k1gFnSc1	citlivost
sluchu	sluch	k1gInSc2	sluch
u	u	k7c2	u
sov	sova	k1gFnPc2	sova
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
výrazně	výrazně	k6eAd1	výrazně
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
u	u	k7c2	u
ostatních	ostatní	k2eAgMnPc2d1	ostatní
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jim	on	k3xPp3gMnPc3	on
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
lovit	lovit	k5eAaImF	lovit
i	i	k9	i
v	v	k7c6	v
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mohou	moct	k5eAaImIp3nP	moct
kořist	kořist	k1gFnSc4	kořist
přesně	přesně	k6eAd1	přesně
lokalizovat	lokalizovat	k5eAaBmF	lokalizovat
pouze	pouze	k6eAd1	pouze
pomocí	pomocí	k7c2	pomocí
sluchu	sluch	k1gInSc2	sluch
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
jim	on	k3xPp3gFnPc3	on
napomáhá	napomáhat	k5eAaBmIp3nS	napomáhat
také	také	k9	také
asymetričnost	asymetričnost	k1gFnSc1	asymetričnost
sluchového	sluchový	k2eAgInSc2d1	sluchový
aparátu	aparát	k1gInSc2	aparát
(	(	kIx(	(
<g/>
jedno	jeden	k4xCgNnSc1	jeden
ucho	ucho	k1gNnSc1	ucho
je	být	k5eAaImIp3nS	být
umístěno	umístit	k5eAaPmNgNnS	umístit
výše	vysoce	k6eAd2	vysoce
než	než	k8xS	než
druhé	druhý	k4xOgInPc4	druhý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
které	který	k3yIgFnSc3	který
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
určit	určit	k5eAaPmF	určit
pozici	pozice	k1gFnSc4	pozice
nejen	nejen	k6eAd1	nejen
jako	jako	k9	jako
stranový	stranový	k2eAgMnSc1d1	stranový
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
výškový	výškový	k2eAgInSc4d1	výškový
úhel	úhel	k1gInSc4	úhel
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
velikost	velikost	k1gFnSc1	velikost
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
rozmanitá	rozmanitý	k2eAgFnSc1d1	rozmanitá
<g/>
.	.	kIx.	.
</s>
<s>
Nejmenším	malý	k2eAgMnSc7d3	nejmenší
zástupcem	zástupce	k1gMnSc7	zástupce
celého	celý	k2eAgInSc2d1	celý
řádu	řád	k1gInSc2	řád
je	být	k5eAaImIp3nS	být
kulíšek	kulíšek	k1gMnSc1	kulíšek
trpasličí	trpasličí	k2eAgMnSc1d1	trpasličí
(	(	kIx(	(
<g/>
Micrathene	Micrathen	k1gMnSc5	Micrathen
whitneyi	whitney	k1gMnSc5	whitney
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
sotva	sotva	k6eAd1	sotva
13,5	[number]	k4	13,5
cm	cm	kA	cm
a	a	k8xC	a
váží	vážit	k5eAaImIp3nS	vážit
31	[number]	k4	31
g	g	kA	g
<g/>
,	,	kIx,	,
největšími	veliký	k2eAgMnPc7d3	veliký
jsou	být	k5eAaImIp3nP	být
naopak	naopak	k6eAd1	naopak
dva	dva	k4xCgInPc4	dva
druhy	druh	k1gInPc4	druh
výrů	výr	k1gMnPc2	výr
–	–	k?	–
výr	výr	k1gMnSc1	výr
velký	velký	k2eAgMnSc1d1	velký
(	(	kIx(	(
<g/>
Bubo	Bubo	k1gMnSc1	Bubo
bubo	bubo	k1gMnSc1	bubo
<g/>
)	)	kIx)	)
a	a	k8xC	a
výr	výr	k1gMnSc1	výr
Blakistonův	Blakistonův	k2eAgMnSc1d1	Blakistonův
(	(	kIx(	(
<g/>
Bubo	Bubo	k6eAd1	Bubo
blakistoni	blakiston	k1gMnPc1	blakiston
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mohou	moct	k5eAaImIp3nP	moct
dorůstat	dorůstat	k5eAaImF	dorůstat
velikosti	velikost	k1gFnPc4	velikost
mezi	mezi	k7c4	mezi
60	[number]	k4	60
–	–	k?	–
71	[number]	k4	71
cm	cm	kA	cm
<g/>
,	,	kIx,	,
v	v	k7c6	v
rozpětí	rozpětí	k1gNnSc6	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
měřit	měřit	k5eAaImF	měřit
téměř	téměř	k6eAd1	téměř
2	[number]	k4	2
m	m	kA	m
a	a	k8xC	a
vážit	vážit	k5eAaImF	vážit
až	až	k9	až
4,5	[number]	k4	4,5
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
sov	sova	k1gFnPc2	sova
loví	lovit	k5eAaImIp3nS	lovit
za	za	k7c2	za
soumraku	soumrak	k1gInSc2	soumrak
a	a	k8xC	a
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yRnSc3	což
jsou	být	k5eAaImIp3nP	být
přizpůsobeny	přizpůsobit	k5eAaPmNgInP	přizpůsobit
nehlučným	hlučný	k2eNgInSc7d1	nehlučný
letem	let	k1gInSc7	let
a	a	k8xC	a
vysoce	vysoce	k6eAd1	vysoce
specializovaným	specializovaný	k2eAgFnPc3d1	specializovaná
zrakovým	zrakový	k2eAgFnPc3d1	zraková
a	a	k8xC	a
sluchovým	sluchový	k2eAgFnPc3d1	sluchová
ústrojím	ústroj	k1gFnPc3	ústroj
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
malými	malý	k2eAgMnPc7d1	malý
savci	savec	k1gMnPc7	savec
<g/>
,	,	kIx,	,
hmyzem	hmyz	k1gInSc7	hmyz
a	a	k8xC	a
jinými	jiný	k2eAgInPc7d1	jiný
druhy	druh	k1gInPc7	druh
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
některé	některý	k3yIgFnPc1	některý
<g/>
,	,	kIx,	,
např.	např.	kA	např.
ketupy	ketupa	k1gFnPc1	ketupa
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
specializovány	specializovat	k5eAaBmNgInP	specializovat
na	na	k7c4	na
lov	lov	k1gInSc4	lov
ryb	ryba	k1gFnPc2	ryba
a	a	k8xC	a
druhotně	druhotně	k6eAd1	druhotně
mohou	moct	k5eAaImIp3nP	moct
požírat	požírat	k5eAaImF	požírat
i	i	k9	i
kraby	krab	k1gInPc4	krab
<g/>
.	.	kIx.	.
</s>
<s>
Kořist	kořist	k1gFnSc4	kořist
uchvacují	uchvacovat	k5eAaImIp3nP	uchvacovat
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
do	do	k7c2	do
ostrých	ostrý	k2eAgInPc2d1	ostrý
drápů	dráp	k1gInPc2	dráp
a	a	k8xC	a
často	často	k6eAd1	často
ji	on	k3xPp3gFnSc4	on
polykají	polykat	k5eAaImIp3nP	polykat
vcelku	vcelku	k6eAd1	vcelku
<g/>
,	,	kIx,	,
nestravitelné	stravitelný	k2eNgFnPc1d1	nestravitelná
části	část	k1gFnPc1	část
(	(	kIx(	(
<g/>
srst	srst	k1gFnSc1	srst
<g/>
,	,	kIx,	,
peří	peří	k1gNnSc1	peří
<g/>
,	,	kIx,	,
kosti	kost	k1gFnPc1	kost
<g/>
)	)	kIx)	)
pak	pak	k6eAd1	pak
vyvrhují	vyvrhovat	k5eAaImIp3nP	vyvrhovat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
vývržků	vývržek	k1gInPc2	vývržek
<g/>
.	.	kIx.	.
</s>
<s>
Hnízda	hnízdo	k1gNnPc1	hnízdo
zpravidla	zpravidla	k6eAd1	zpravidla
nestaví	stavit	k5eNaBmIp3nP	stavit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vejce	vejce	k1gNnPc1	vejce
kladou	klást	k5eAaImIp3nP	klást
do	do	k7c2	do
starých	stará	k1gFnPc2	stará
hnízd	hnízdo	k1gNnPc2	hnízdo
jiných	jiný	k2eAgMnPc2d1	jiný
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
dutin	dutina	k1gFnPc2	dutina
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
a	a	k8xC	a
v	v	k7c6	v
případě	případ	k1gInSc6	případ
jednoho	jeden	k4xCgInSc2	jeden
druhu	druh	k1gInSc2	druh
(	(	kIx(	(
<g/>
sýčka	sýček	k1gMnSc2	sýček
králičího	králičí	k2eAgMnSc2d1	králičí
<g/>
)	)	kIx)	)
do	do	k7c2	do
vyhloubených	vyhloubený	k2eAgFnPc2d1	vyhloubená
nor	nora	k1gFnPc2	nora
<g/>
.	.	kIx.	.
</s>
<s>
Vejce	vejce	k1gNnPc1	vejce
jsou	být	k5eAaImIp3nP	být
vesměs	vesměs	k6eAd1	vesměs
bílá	bílý	k2eAgNnPc1d1	bílé
a	a	k8xC	a
kulovitá	kulovitý	k2eAgFnSc1d1	kulovitá
<g/>
,	,	kIx,	,
velikost	velikost	k1gFnSc1	velikost
snůšky	snůška	k1gFnSc2	snůška
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
dostupnosti	dostupnost	k1gFnSc6	dostupnost
potravy	potrava	k1gFnSc2	potrava
<g/>
;	;	kIx,	;
zatímco	zatímco	k8xS	zatímco
při	při	k7c6	při
jejím	její	k3xOp3gInSc6	její
nedostatku	nedostatek	k1gInSc6	nedostatek
nemusejí	muset	k5eNaImIp3nP	muset
zahnízdit	zahnízdit	k5eAaPmF	zahnízdit
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
při	při	k7c6	při
nadbytku	nadbytek	k1gInSc6	nadbytek
jsou	být	k5eAaImIp3nP	být
snůšky	snůška	k1gFnPc1	snůška
naopak	naopak	k6eAd1	naopak
velmi	velmi	k6eAd1	velmi
početné	početný	k2eAgFnPc1d1	početná
<g/>
.	.	kIx.	.
</s>
<s>
Sezení	sezení	k1gNnSc1	sezení
začíná	začínat	k5eAaImIp3nS	začínat
od	od	k7c2	od
snesení	snesení	k1gNnSc2	snesení
prvního	první	k4xOgNnSc2	první
vejce	vejce	k1gNnSc2	vejce
<g/>
,	,	kIx,	,
inkubaci	inkubace	k1gFnSc4	inkubace
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
druhů	druh	k1gInPc2	druh
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
převážně	převážně	k6eAd1	převážně
nebo	nebo	k8xC	nebo
výhradně	výhradně	k6eAd1	výhradně
samice	samice	k1gFnSc1	samice
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
samec	samec	k1gMnSc1	samec
loví	lovit	k5eAaImIp3nS	lovit
a	a	k8xC	a
přináší	přinášet	k5eAaImIp3nS	přinášet
potravu	potrava	k1gFnSc4	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
líhnou	líhnout	k5eAaImIp3nP	líhnout
v	v	k7c6	v
intervalech	interval	k1gInPc6	interval
několika	několik	k4yIc2	několik
dnů	den	k1gInPc2	den
opeřená	opeřený	k2eAgFnSc1d1	opeřená
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
slepá	slepý	k2eAgFnSc1d1	slepá
a	a	k8xC	a
hluchá	hluchý	k2eAgFnSc1d1	hluchá
<g/>
.	.	kIx.	.
</s>
<s>
Sovy	sova	k1gFnPc1	sova
se	se	k3xPyFc4	se
v	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
kultuře	kultura	k1gFnSc6	kultura
objevují	objevovat	k5eAaImIp3nP	objevovat
již	již	k6eAd1	již
po	po	k7c6	po
tisíce	tisíc	k4xCgInSc2	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
kultuře	kultura	k1gFnSc6	kultura
jsou	být	k5eAaImIp3nP	být
spojovány	spojovat	k5eAaImNgInP	spojovat
s	s	k7c7	s
moudrostí	moudrost	k1gFnSc7	moudrost
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
dokáží	dokázat	k5eAaPmIp3nP	dokázat
vidět	vidět	k5eAaImF	vidět
ve	v	k7c6	v
tmě	tma	k1gFnSc6	tma
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
důvod	důvod	k1gInSc4	důvod
propojení	propojení	k1gNnSc2	propojení
sovy	sova	k1gFnSc2	sova
s	s	k7c7	s
bohyní	bohyně	k1gFnSc7	bohyně
moudrosti	moudrost	k1gFnSc2	moudrost
Athény	Athéna	k1gFnSc2	Athéna
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
zobrazované	zobrazovaný	k2eAgFnSc2d1	zobrazovaná
se	s	k7c7	s
sovou	sova	k1gFnSc7	sova
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rané	raný	k2eAgFnSc6d1	raná
křesťanské	křesťanský	k2eAgFnSc6d1	křesťanská
mytologii	mytologie	k1gFnSc6	mytologie
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
sova	sova	k1gFnSc1	sova
symbolem	symbol	k1gInSc7	symbol
ďábla	ďábel	k1gMnSc2	ďábel
<g/>
.	.	kIx.	.
</s>
<s>
Houkání	houkání	k1gNnSc1	houkání
sýčka	sýček	k1gMnSc2	sýček
je	být	k5eAaImIp3nS	být
chápáno	chápat	k5eAaImNgNnS	chápat
jako	jako	k8xC	jako
věštba	věštba	k1gFnSc1	věštba
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
je	být	k5eAaImIp3nS	být
sova	sova	k1gFnSc1	sova
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
symbol	symbol	k1gInSc4	symbol
smrti	smrt	k1gFnSc2	smrt
a	a	k8xC	a
špatné	špatný	k2eAgNnSc1d1	špatné
znamení	znamení	k1gNnSc1	znamení
<g/>
,	,	kIx,	,
u	u	k7c2	u
indiánů	indián	k1gMnPc2	indián
kmene	kmen	k1gInSc2	kmen
Hopi	Hop	k1gMnPc1	Hop
jsou	být	k5eAaImIp3nP	být
sovy	sova	k1gFnPc4	sova
tabuizované	tabuizovaný	k2eAgFnPc4d1	tabuizovaná
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jsou	být	k5eAaImIp3nP	být
nečisté	čistý	k2eNgFnPc1d1	nečistá
a	a	k8xC	a
zlověstné	zlověstný	k2eAgFnPc1d1	zlověstná
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
u	u	k7c2	u
kmene	kmen	k1gInSc2	kmen
Kwakiutlů	Kwakiutl	k1gMnPc2	Kwakiutl
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
nebo	nebo	k8xC	nebo
Kečuů	Keču	k1gMnPc2	Keču
v	v	k7c6	v
Peru	Peru	k1gNnSc6	Peru
<g/>
.	.	kIx.	.
</s>
<s>
HUDEC	Hudec	k1gMnSc1	Hudec
<g/>
,	,	kIx,	,
K.	K.	kA	K.
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
..	..	k?	..
Fauna	fauna	k1gFnSc1	fauna
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
1113	[number]	k4	1113
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Owl	Owl	k1gFnSc2	Owl
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
sovy	sova	k1gFnSc2	sova
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Taxon	taxon	k1gInSc1	taxon
Strigiformes	Strigiformes	k1gMnSc1	Strigiformes
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
CHOLEWIAK	CHOLEWIAK	kA	CHOLEWIAK
<g/>
,	,	kIx,	,
D.	D.	kA	D.
<g/>
.	.	kIx.	.
</s>
<s>
Strigiformes	Strigiformes	k1gInSc1	Strigiformes
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Animal	animal	k1gMnSc1	animal
Diversity	Diversit	k1gInPc1	Diversit
Web	web	k1gInSc1	web
–	–	k?	–
University	universita	k1gFnSc2	universita
of	of	k?	of
Michigan	Michigan	k1gInSc1	Michigan
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
