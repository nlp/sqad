<p>
<s>
Šance	šance	k1gFnSc1	šance
je	být	k5eAaImIp3nS	být
přírodní	přírodní	k2eAgFnPc4d1	přírodní
rezervace	rezervace	k1gFnPc4	rezervace
ev.	ev.	k?	ev.
č.	č.	k?	č.
738	[number]	k4	738
na	na	k7c6	na
území	území	k1gNnSc6	území
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
o	o	k7c6	o
rozloze	rozloha	k1gFnSc6	rozloha
117	[number]	k4	117
ha	ha	kA	ha
spravuje	spravovat	k5eAaImIp3nS	spravovat
Magistrát	magistrát	k1gInSc1	magistrát
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Rezervace	rezervace	k1gFnSc1	rezervace
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
1.7	[number]	k4	1.7
<g/>
.1982	.1982	k4	.1982
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
díky	díky	k7c3	díky
objevům	objev	k1gInPc3	objev
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
učinil	učinit	k5eAaImAgMnS	učinit
významný	významný	k2eAgMnSc1d1	významný
entomolog	entomolog	k1gMnSc1	entomolog
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
František	František	k1gMnSc1	František
Antonín	Antonín	k1gMnSc1	Antonín
Nickerl	Nickerl	k1gMnSc1	Nickerl
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
28	[number]	k4	28
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
1991	[number]	k4	1991
došlo	dojít	k5eAaPmAgNnS	dojít
vyhláškou	vyhláška	k1gFnSc7	vyhláška
č.	č.	k?	č.
3	[number]	k4	3
<g/>
/	/	kIx~	/
<g/>
91	[number]	k4	91
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
HMP	HMP	kA	HMP
k	k	k7c3	k
přehlášení	přehlášení	k1gNnSc3	přehlášení
rezervace	rezervace	k1gFnSc2	rezervace
<g/>
.	.	kIx.	.
</s>
<s>
Vyhláškou	vyhláška	k1gFnSc7	vyhláška
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
zřízení	zřízení	k1gNnSc3	zřízení
oblasti	oblast	k1gFnSc2	oblast
klidu	klid	k1gInSc2	klid
"	"	kIx"	"
<g/>
Modřanská	modřanský	k2eAgFnSc1d1	Modřanská
rokle	rokle	k1gFnSc1	rokle
-	-	kIx~	-
Cholupice	Cholupice	k1gFnSc1	Cholupice
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
přírodní	přírodní	k2eAgFnPc4d1	přírodní
rezervace	rezervace	k1gFnPc4	rezervace
spadá	spadat	k5eAaImIp3nS	spadat
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
se	se	k3xPyFc4	se
schválilo	schválit	k5eAaPmAgNnS	schválit
rozšíření	rozšíření	k1gNnSc1	rozšíření
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
Šance	šance	k1gFnSc2	šance
o	o	k7c4	o
lokalitu	lokalita	k1gFnSc4	lokalita
Břežanské	Břežanský	k2eAgNnSc1d1	Břežanské
údolí	údolí	k1gNnSc1	údolí
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
kvůli	kvůli	k7c3	kvůli
ochraně	ochrana	k1gFnSc3	ochrana
biotopu	biotop	k1gInSc2	biotop
evropsky	evropsky	k6eAd1	evropsky
významného	významný	k2eAgInSc2d1	významný
druhu	druh	k1gInSc2	druh
nočního	noční	k2eAgMnSc2d1	noční
motýla	motýl	k1gMnSc2	motýl
–	–	k?	–
přástevníka	přástevník	k1gMnSc2	přástevník
kostivalového	kostivalový	k2eAgMnSc2d1	kostivalový
(	(	kIx(	(
<g/>
Euplagia	Euplagium	k1gNnSc2	Euplagium
quadripunctaria	quadripunctarium	k1gNnSc2	quadripunctarium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
rozšířením	rozšíření	k1gNnSc7	rozšíření
se	se	k3xPyFc4	se
lokalita	lokalita	k1gFnSc1	lokalita
zařadila	zařadit	k5eAaPmAgFnS	zařadit
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
NATURA	NATURA	kA	NATURA
<g/>
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Aby	aby	kYmCp3nS	aby
rozšíření	rozšíření	k1gNnSc1	rozšíření
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
právními	právní	k2eAgInPc7d1	právní
předpisy	předpis	k1gInPc7	předpis
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgMnS	muset
se	se	k3xPyFc4	se
zrušit	zrušit	k5eAaPmF	zrušit
původní	původní	k2eAgInSc4d1	původní
zřizovací	zřizovací	k2eAgInSc4d1	zřizovací
právní	právní	k2eAgInSc4d1	právní
předpis	předpis	k1gInSc4	předpis
a	a	k8xC	a
přírodní	přírodní	k2eAgFnPc4d1	přírodní
rezervace	rezervace	k1gFnPc4	rezervace
byla	být	k5eAaImAgFnS	být
znovu	znovu	k6eAd1	znovu
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
<g/>
.	.	kIx.	.
</s>
<s>
Šance	šance	k1gFnSc1	šance
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
Vltavy	Vltava	k1gFnSc2	Vltava
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
menší	malý	k2eAgFnPc4d2	menší
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
keltského	keltský	k2eAgNnSc2d1	keltské
oppida	oppidum	k1gNnSc2	oppidum
Závist	závist	k1gFnSc1	závist
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
ochrany	ochrana	k1gFnSc2	ochrana
jsou	být	k5eAaImIp3nP	být
zejména	zejména	k9	zejména
chráněné	chráněný	k2eAgInPc4d1	chráněný
druhy	druh	k1gInPc4	druh
živočichů	živočich	k1gMnPc2	živočich
a	a	k8xC	a
rostlin	rostlina	k1gFnPc2	rostlina
vyskytující	vyskytující	k2eAgFnSc2d1	vyskytující
se	se	k3xPyFc4	se
na	na	k7c6	na
skalnatých	skalnatý	k2eAgInPc6d1	skalnatý
svazích	svah	k1gInPc6	svah
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Břežanského	Břežanský	k2eAgInSc2d1	Břežanský
potoka	potok	k1gInSc2	potok
<g/>
.	.	kIx.	.
</s>
<s>
Chráněny	chráněn	k2eAgInPc1d1	chráněn
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
přirozené	přirozený	k2eAgFnPc4d1	přirozená
doubravy	doubrava	k1gFnPc4	doubrava
<g/>
,	,	kIx,	,
vyskytující	vyskytující	k2eAgFnPc4d1	vyskytující
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
v	v	k7c6	v
několika	několik	k4yIc6	několik
typech	typ	k1gInPc6	typ
(	(	kIx(	(
<g/>
tolitová	tolitový	k2eAgFnSc1d1	tolitový
doubrava	doubrava	k1gFnSc1	doubrava
<g/>
,	,	kIx,	,
biková	bikový	k2eAgFnSc1d1	biková
doubrava	doubrava	k1gFnSc1	doubrava
<g/>
,	,	kIx,	,
habrová	habrový	k2eAgFnSc1d1	Habrová
doubrava	doubrava	k1gFnSc1	doubrava
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pozornost	pozornost	k1gFnSc1	pozornost
je	být	k5eAaImIp3nS	být
věnovaná	věnovaný	k2eAgFnSc1d1	věnovaná
také	také	k9	také
přirozenému	přirozený	k2eAgNnSc3d1	přirozené
bezlesí	bezlesí	k1gNnSc3	bezlesí
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
acidofilních	acidofilní	k2eAgInPc2d1	acidofilní
trávníků	trávník	k1gInPc2	trávník
a	a	k8xC	a
vřesových	vřesový	k2eAgFnPc2d1	Vřesová
ploch	plocha	k1gFnPc2	plocha
a	a	k8xC	a
neobvyklému	obvyklý	k2eNgInSc3d1	neobvyklý
geologickému	geologický	k2eAgInSc3d1	geologický
profilu	profil	k1gInSc3	profil
<g/>
.	.	kIx.	.
</s>
<s>
Územím	území	k1gNnSc7	území
vede	vést	k5eAaImIp3nS	vést
naučná	naučný	k2eAgFnSc1d1	naučná
stezka	stezka	k1gFnSc1	stezka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
zřízena	zřídit	k5eAaPmNgFnS	zřídit
Svazem	svaz	k1gInSc7	svaz
ochrany	ochrana	k1gFnSc2	ochrana
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
krajiny	krajina	k1gFnSc2	krajina
ČR	ČR	kA	ČR
z	z	k7c2	z
prostředků	prostředek	k1gInPc2	prostředek
Magistrátu	magistrát	k1gInSc2	magistrát
hl.	hl.	k?	hl.
m.	m.	k?	m.
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
s	s	k7c7	s
přispěním	přispění	k1gNnSc7	přispění
Lesů	les	k1gInPc2	les
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
Lesního	lesní	k2eAgInSc2d1	lesní
závodu	závod	k1gInSc2	závod
Konopiště	Konopiště	k1gNnSc1	Konopiště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Vrch	vrch	k1gInSc1	vrch
Šance	šance	k1gFnSc1	šance
(	(	kIx(	(
<g/>
380	[number]	k4	380
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
sousedním	sousední	k2eAgInSc7d1	sousední
vrchem	vrch	k1gInSc7	vrch
Hradiště	Hradiště	k1gNnSc2	Hradiště
(	(	kIx(	(
<g/>
391	[number]	k4	391
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
součástí	součást	k1gFnSc7	součást
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
největšího	veliký	k2eAgNnSc2d3	veliký
a	a	k8xC	a
nejznámějšího	známý	k2eAgNnSc2d3	nejznámější
oppida	oppidum	k1gNnSc2	oppidum
na	na	k7c6	na
území	území	k1gNnSc6	území
Čech	Čechy	k1gFnPc2	Čechy
-	-	kIx~	-
Oppida	oppidum	k1gNnSc2	oppidum
Závist	závist	k1gFnSc4	závist
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
Keltové	Kelt	k1gMnPc1	Kelt
byli	být	k5eAaImAgMnP	být
velcí	velký	k2eAgMnPc1d1	velký
válečníci	válečník	k1gMnPc1	válečník
<g/>
,	,	kIx,	,
měli	mít	k5eAaImAgMnP	mít
dobře	dobře	k6eAd1	dobře
propracovanou	propracovaný	k2eAgFnSc4d1	propracovaná
techniku	technika	k1gFnSc4	technika
opevňování	opevňování	k1gNnSc2	opevňování
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgInSc7d1	základní
článkem	článek	k1gInSc7	článek
opevnění	opevnění	k1gNnSc2	opevnění
byl	být	k5eAaImAgInS	být
valovitý	valovitý	k2eAgInSc1d1	valovitý
násep	násep	k1gInSc1	násep
z	z	k7c2	z
kamenů	kámen	k1gInPc2	kámen
a	a	k8xC	a
zeminy	zemina	k1gFnSc2	zemina
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dosahoval	dosahovat	k5eAaImAgMnS	dosahovat
šířky	šířka	k1gFnSc2	šířka
až	až	k6eAd1	až
25	[number]	k4	25
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
valu	val	k1gInSc2	val
stála	stát	k5eAaImAgFnS	stát
ještě	ještě	k9	ještě
ochranná	ochranný	k2eAgFnSc1d1	ochranná
palisádová	palisádový	k2eAgFnSc1d1	palisádová
předprseň	předprseň	k1gFnSc1	předprseň
<g/>
,	,	kIx,	,
vyrobená	vyrobený	k2eAgFnSc1d1	vyrobená
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
článkem	článek	k1gInSc7	článek
byl	být	k5eAaImAgInS	být
také	také	k9	také
příkop	příkop	k1gInSc4	příkop
táhnoucí	táhnoucí	k2eAgInSc4d1	táhnoucí
se	se	k3xPyFc4	se
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
délce	délka	k1gFnSc6	délka
severní	severní	k2eAgFnSc2d1	severní
strany	strana	k1gFnSc2	strana
opevnění	opevnění	k1gNnPc2	opevnění
kromě	kromě	k7c2	kromě
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
chráněna	chránit	k5eAaImNgFnS	chránit
skálou	skála	k1gFnSc7	skála
<g/>
.	.	kIx.	.
</s>
<s>
Příkop	příkop	k1gInSc1	příkop
byl	být	k5eAaImAgInS	být
široký	široký	k2eAgInSc1d1	široký
i	i	k9	i
přes	přes	k7c4	přes
deset	deset	k4xCc4	deset
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
až	až	k9	až
čtyři	čtyři	k4xCgInPc4	čtyři
metry	metr	k1gInPc4	metr
hluboký	hluboký	k2eAgInSc1d1	hluboký
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
specifické	specifický	k2eAgInPc1d1	specifický
opevňovací	opevňovací	k2eAgInPc1d1	opevňovací
valy	val	k1gInPc1	val
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
laténské	laténský	k2eAgFnSc6d1	laténská
době	doba	k1gFnSc6	doba
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
l.	l.	k?	l.
stol.	stol.	k?	stol.
př	př	kA	př
<g/>
.	.	kIx.	.
Kr.	Kr.	k1gMnSc2	Kr.
<g/>
)	)	kIx)	)
pojmenovány	pojmenován	k2eAgFnPc1d1	pojmenována
šance	šance	k1gFnPc1	šance
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
tedy	tedy	k9	tedy
název	název	k1gInSc4	název
vrcholku	vrcholek	k1gInSc2	vrcholek
i	i	k8xC	i
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Keltské	keltský	k2eAgFnPc1d1	keltská
osady	osada	k1gFnPc1	osada
byly	být	k5eAaImAgFnP	být
stavěny	stavit	k5eAaImNgFnP	stavit
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
vodního	vodní	k2eAgInSc2d1	vodní
toku	tok	k1gInSc2	tok
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
Břežanský	Břežanský	k2eAgInSc1d1	Břežanský
potok	potok	k1gInSc1	potok
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
řečeno	říct	k5eAaPmNgNnS	říct
<g/>
,	,	kIx,	,
hradiště	hradiště	k1gNnSc1	hradiště
Šance	šance	k1gFnSc2	šance
patřilo	patřit	k5eAaImAgNnS	patřit
k	k	k7c3	k
centrálnímu	centrální	k2eAgInSc3d1	centrální
komplexu	komplex	k1gInSc3	komplex
Závist	závist	k1gFnSc4	závist
<g/>
.	.	kIx.	.
</s>
<s>
Domy	dům	k1gInPc1	dům
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgInP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
zejména	zejména	k9	zejména
polo	polo	k6eAd1	polo
zemnicemi	zemnice	k1gFnPc7	zemnice
(	(	kIx(	(
<g/>
napůl	napůl	k6eAd1	napůl
zakopanými	zakopaný	k2eAgInPc7d1	zakopaný
do	do	k7c2	do
země	zem	k1gFnSc2	zem
<g/>
)	)	kIx)	)
a	a	k8xC	a
dřevěnými	dřevěný	k2eAgInPc7d1	dřevěný
sruby	srub	k1gInPc7	srub
<g/>
,	,	kIx,	,
vyskytovaly	vyskytovat	k5eAaImAgInP	vyskytovat
se	se	k3xPyFc4	se
však	však	k9	však
i	i	k9	i
domy	dům	k1gInPc1	dům
patrové	patrový	k2eAgInPc1d1	patrový
a	a	k8xC	a
jedno	jeden	k4xCgNnSc1	jeden
větší	veliký	k2eAgNnSc1d2	veliký
stavení	stavení	k1gNnSc1	stavení
pro	pro	k7c4	pro
místního	místní	k2eAgMnSc4d1	místní
náčelníka	náčelník	k1gMnSc4	náčelník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takto	takto	k6eAd1	takto
velkém	velký	k2eAgNnSc6d1	velké
oppidu	oppidum	k1gNnSc6	oppidum
byla	být	k5eAaImAgFnS	být
stavení	stavení	k1gNnSc2	stavení
uspořádána	uspořádat	k5eAaPmNgFnS	uspořádat
do	do	k7c2	do
ulic	ulice	k1gFnPc2	ulice
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
dokonce	dokonce	k9	dokonce
dlážděných	dlážděný	k2eAgFnPc2d1	dlážděná
<g/>
.	.	kIx.	.
</s>
<s>
Oppidum	oppidum	k1gNnSc1	oppidum
mělo	mít	k5eAaImAgNnS	mít
také	také	k9	také
svou	svůj	k3xOyFgFnSc4	svůj
malou	malý	k2eAgFnSc4d1	malá
svatyni	svatyně	k1gFnSc4	svatyně
<g/>
.	.	kIx.	.
<g/>
S	s	k7c7	s
výskytem	výskyt	k1gInSc7	výskyt
sídel	sídlo	k1gNnPc2	sídlo
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
i	i	k9	i
zemědělské	zemědělský	k2eAgNnSc1d1	zemědělské
využívání	využívání	k1gNnSc1	využívání
okolní	okolní	k2eAgFnSc2d1	okolní
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zániku	zánik	k1gInSc6	zánik
osady	osada	k1gFnSc2	osada
území	území	k1gNnSc2	území
opět	opět	k6eAd1	opět
zarostlo	zarůst	k5eAaPmAgNnS	zarůst
lesem	les	k1gInSc7	les
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
setkáváme	setkávat	k5eAaImIp1nP	setkávat
s	s	k7c7	s
lesním	lesní	k2eAgNnSc7d1	lesní
hospodářstvím	hospodářství	k1gNnSc7	hospodářství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přírodní	přírodní	k2eAgInPc1d1	přírodní
poměry	poměr	k1gInPc1	poměr
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Geologie	geologie	k1gFnSc2	geologie
===	===	k?	===
</s>
</p>
<p>
<s>
Matečnou	matečný	k2eAgFnSc4d1	matečná
horninu	hornina	k1gFnSc4	hornina
rezervace	rezervace	k1gFnSc2	rezervace
tvoří	tvořit	k5eAaImIp3nP	tvořit
horniny	hornina	k1gFnPc1	hornina
proterozoické	proterozoický	k2eAgFnPc1d1	proterozoický
-	-	kIx~	-
předprvohorní	předprvohorní	k2eAgFnPc1d1	předprvohorní
(	(	kIx(	(
<g/>
které	který	k3yIgNnSc1	který
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
nalezištěm	naleziště	k1gNnSc7	naleziště
mikroflóry	mikroflóra	k1gFnSc2	mikroflóra
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
<g/>
)	)	kIx)	)
a	a	k8xC	a
horniny	hornina	k1gFnSc2	hornina
ordovické	ordovický	k2eAgFnSc2d1	ordovická
(	(	kIx(	(
<g/>
prvohorní	prvohorní	k2eAgFnSc2d1	prvohorní
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Převládající	převládající	k2eAgFnPc1d1	převládající
jsou	být	k5eAaImIp3nP	být
břidlice	břidlice	k1gFnPc1	břidlice
letenského	letenský	k2eAgNnSc2d1	Letenské
souvrství	souvrství	k1gNnSc2	souvrství
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
najdeme	najít	k5eAaPmIp1nP	najít
malé	malý	k2eAgFnPc4d1	malá
plochy	plocha	k1gFnPc4	plocha
pískovce	pískovec	k1gInSc2	pískovec
<g/>
,	,	kIx,	,
tufu	tuf	k1gInSc6	tuf
<g/>
,	,	kIx,	,
andezitu	andezit	k1gInSc6	andezit
nebo	nebo	k8xC	nebo
prachovce	prachovka	k1gFnSc6	prachovka
a	a	k8xC	a
břidlice	břidlice	k1gFnSc1	břidlice
štěchovického	štěchovický	k2eAgNnSc2d1	štěchovický
souvrství	souvrství	k1gNnSc2	souvrství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
části	část	k1gFnSc6	část
Břežanského	Břežanský	k2eAgNnSc2d1	Břežanské
údolí	údolí	k1gNnSc2	údolí
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
jevem	jev	k1gInSc7	jev
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Závistský	Závistský	k2eAgInSc1d1	Závistský
přesmyk	přesmyk	k1gInSc1	přesmyk
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
tak	tak	k9	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
nejmladší	mladý	k2eAgFnPc4d3	nejmladší
horniny	hornina	k1gFnPc4	hornina
(	(	kIx(	(
<g/>
pískovce	pískovec	k1gInSc2	pískovec
a	a	k8xC	a
břidlice	břidlice	k1gFnSc2	břidlice
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
nasunuty	nasunut	k2eAgFnPc1d1	nasunuta
předprvohorní	předprvohorní	k2eAgFnPc1d1	předprvohorní
horniny	hornina	k1gFnPc1	hornina
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
došlo	dojít	k5eAaPmAgNnS	dojít
pohyby	pohyb	k1gInPc4	pohyb
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
biodiverzity	biodiverzita	k1gFnSc2	biodiverzita
jsou	být	k5eAaImIp3nP	být
významné	významný	k2eAgInPc1d1	významný
spility	spilit	k1gInPc1	spilit
(	(	kIx(	(
<g/>
podmořské	podmořský	k2eAgFnPc1d1	podmořská
vulkanity	vulkanita	k1gFnPc1	vulkanita
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yRgFnPc7	který
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
části	část	k1gFnSc6	část
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Břežanské	Břežanský	k2eAgFnSc6d1	Břežanská
rokli	rokle	k1gFnSc6	rokle
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
skály	skála	k1gFnSc2	skála
tvořené	tvořený	k2eAgInPc1d1	tvořený
davelskými	davelský	k2eAgInPc7d1	davelský
vulkanity	vulkanit	k1gInPc7	vulkanit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Flóra	Flóra	k1gFnSc1	Flóra
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
území	území	k1gNnSc6	území
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
různorodé	různorodý	k2eAgFnPc1d1	různorodá
stanovištní	stanovištní	k2eAgFnPc1d1	stanovištní
podmínky	podmínka	k1gFnPc1	podmínka
<g/>
,	,	kIx,	,
na	na	k7c4	na
což	což	k3yRnSc4	což
je	být	k5eAaImIp3nS	být
vázán	vázán	k2eAgInSc1d1	vázán
výskyt	výskyt	k1gInSc1	výskyt
dřevin	dřevina	k1gFnPc2	dřevina
i	i	k8xC	i
bylin	bylina	k1gFnPc2	bylina
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
můžeme	moct	k5eAaImIp1nP	moct
pozorovat	pozorovat	k5eAaImF	pozorovat
různé	různý	k2eAgInPc4d1	různý
druhy	druh	k1gInPc4	druh
hospodaření	hospodaření	k1gNnSc2	hospodaření
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
chudých	chudý	k2eAgFnPc6d1	chudá
a	a	k8xC	a
kyselých	kyselý	k2eAgFnPc6d1	kyselá
půdách	půda	k1gFnPc6	půda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
nacházely	nacházet	k5eAaImAgFnP	nacházet
pastviny	pastvina	k1gFnPc4	pastvina
a	a	k8xC	a
porosty	porost	k1gInPc4	porost
křovin	křovina	k1gFnPc2	křovina
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
najdeme	najít	k5eAaPmIp1nP	najít
převážně	převážně	k6eAd1	převážně
původní	původní	k2eAgFnPc1d1	původní
doubravy	doubrava	k1gFnPc1	doubrava
s	s	k7c7	s
přimíšenou	přimíšený	k2eAgFnSc7d1	přimíšená
břízou	bříza	k1gFnSc7	bříza
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgFnPc2d1	další
dřevin	dřevina	k1gFnPc2	dřevina
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
území	území	k1gNnPc2	území
včetně	včetně	k7c2	včetně
jižních	jižní	k2eAgInPc2d1	jižní
a	a	k8xC	a
západních	západní	k2eAgInPc2d1	západní
svahů	svah	k1gInPc2	svah
nachází	nacházet	k5eAaImIp3nS	nacházet
zejména	zejména	k9	zejména
habr	habr	k1gInSc1	habr
obecný	obecný	k2eAgInSc1d1	obecný
(	(	kIx(	(
<g/>
Carpinus	Carpinus	k1gInSc1	Carpinus
betulus	betulus	k1gInSc1	betulus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
buk	buk	k1gInSc4	buk
lesní	lesní	k2eAgInSc4d1	lesní
(	(	kIx(	(
<g/>
Fagus	Fagus	k1gInSc4	Fagus
sylvatica	sylvatic	k1gInSc2	sylvatic
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
borovice	borovice	k1gFnSc1	borovice
lesní	lesní	k2eAgFnSc1d1	lesní
(	(	kIx(	(
<g/>
Pinus	Pinus	k1gMnSc1	Pinus
sylvestris	sylvestris	k1gFnSc2	sylvestris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
javor	javor	k1gInSc1	javor
babyka	babyka	k1gFnSc1	babyka
(	(	kIx(	(
<g/>
Acer	Acer	k1gInSc1	Acer
campestre	campestr	k1gInSc5	campestr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
spodních	spodní	k2eAgFnPc6d1	spodní
částech	část	k1gFnPc6	část
svahů	svah	k1gInPc2	svah
pak	pak	k6eAd1	pak
najdeme	najít	k5eAaPmIp1nP	najít
trnovník	trnovník	k1gInSc4	trnovník
akát	akát	k1gInSc1	akát
(	(	kIx(	(
<g/>
Robinia	Robinium	k1gNnSc2	Robinium
pseudoacacia	pseudoacacium	k1gNnSc2	pseudoacacium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zde	zde	k6eAd1	zde
byl	být	k5eAaImAgInS	být
uměle	uměle	k6eAd1	uměle
vysazen	vysadit	k5eAaPmNgInS	vysadit
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nepůvodní	původní	k2eNgFnSc4d1	nepůvodní
velmi	velmi	k6eAd1	velmi
invazivní	invazivní	k2eAgFnSc4d1	invazivní
dřevinu	dřevina	k1gFnSc4	dřevina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
šíří	šířit	k5eAaImIp3nS	šířit
a	a	k8xC	a
zamezuje	zamezovat	k5eAaImIp3nS	zamezovat
růstu	růst	k1gInSc3	růst
původních	původní	k2eAgFnPc2d1	původní
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
údolích	údolí	k1gNnPc6	údolí
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
javořiny	javořina	k1gFnSc2	javořina
se	s	k7c7	s
zastoupením	zastoupení	k1gNnSc7	zastoupení
těchto	tento	k3xDgFnPc2	tento
dřevin	dřevina	k1gFnPc2	dřevina
<g/>
:	:	kIx,	:
javor	javor	k1gInSc1	javor
mléč	mléč	k1gInSc1	mléč
(	(	kIx(	(
<g/>
Acer	Acer	k1gMnSc1	Acer
platanoides	platanoides	k1gMnSc1	platanoides
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
javor	javor	k1gInSc1	javor
klen	klen	k1gInSc1	klen
(	(	kIx(	(
<g/>
Acer	Acer	k1gMnSc1	Acer
pseudoplatanus	pseudoplatanus	k1gMnSc1	pseudoplatanus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
javor	javor	k1gInSc1	javor
babyka	babyka	k1gFnSc1	babyka
(	(	kIx(	(
<g/>
Acer	Acer	k1gInSc1	Acer
campestre	campestr	k1gInSc5	campestr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lípa	lípa	k1gFnSc1	lípa
srdčitá	srdčitý	k2eAgFnSc1d1	srdčitá
(	(	kIx(	(
<g/>
Tilia	Tilia	k1gFnSc1	Tilia
cordata	cordata	k1gFnSc1	cordata
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jilm	jilm	k1gInSc4	jilm
polní	polní	k2eAgInSc4d1	polní
(	(	kIx(	(
<g/>
Ulmus	Ulmus	k1gInSc4	Ulmus
minor	minor	k2eAgInSc4d1	minor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jasan	jasan	k1gInSc1	jasan
ztepilý	ztepilý	k2eAgInSc1d1	ztepilý
(	(	kIx(	(
<g/>
Fraxinus	Fraxinus	k1gInSc1	Fraxinus
excelsior	excelsior	k1gInSc1	excelsior
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeřáb	jeřáb	k1gInSc1	jeřáb
obecný	obecný	k2eAgInSc1d1	obecný
(	(	kIx(	(
<g/>
Sorbus	Sorbus	k1gInSc1	Sorbus
aucuparia	aucuparium	k1gNnSc2	aucuparium
L.	L.	kA	L.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeřáb	jeřáb	k1gInSc1	jeřáb
muk	muk	k1gInSc1	muk
(	(	kIx(	(
<g/>
Sorbus	Sorbus	k1gInSc1	Sorbus
aria	ari	k1gInSc2	ari
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Najdeme	najít	k5eAaPmIp1nP	najít
zde	zde	k6eAd1	zde
i	i	k9	i
uměle	uměle	k6eAd1	uměle
vysázené	vysázený	k2eAgInPc1d1	vysázený
porosty	porost	k1gInPc1	porost
smrku	smrk	k1gInSc2	smrk
ztepilého	ztepilý	k2eAgInSc2d1	ztepilý
(	(	kIx(	(
<g/>
Picea	Pice	k1gInSc2	Pice
abies	abies	k1gInSc1	abies
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
modřínu	modřín	k1gInSc2	modřín
evropského	evropský	k2eAgInSc2d1	evropský
(	(	kIx(	(
<g/>
Larix	Larix	k1gInSc1	Larix
decidua	decidu	k1gInSc2	decidu
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
borovice	borovice	k1gFnSc2	borovice
černé	černá	k1gFnSc2	černá
(	(	kIx(	(
<g/>
Pinus	Pinus	k1gMnSc1	Pinus
nigra	nigr	k1gMnSc2	nigr
<g/>
)	)	kIx)	)
a	a	k8xC	a
dubu	dub	k1gInSc2	dub
červeného	červené	k1gNnSc2	červené
(	(	kIx(	(
<g/>
Quercus	Quercus	k1gInSc1	Quercus
rubra	rubrum	k1gNnSc2	rubrum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednotlivě	jednotlivě	k6eAd1	jednotlivě
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
i	i	k9	i
douglasky	douglaska	k1gFnPc1	douglaska
tisolisté	tisolistý	k2eAgFnPc1d1	tisolistá
(	(	kIx(	(
<g/>
Pseudotsuga	Pseudotsuga	k1gFnSc1	Pseudotsuga
menziesii	menziesie	k1gFnSc4	menziesie
<g/>
)	)	kIx)	)
a	a	k8xC	a
borovice	borovice	k1gFnSc1	borovice
vejmutovky	vejmutovka	k1gFnSc2	vejmutovka
(	(	kIx(	(
<g/>
Pinus	Pinus	k1gMnSc1	Pinus
strobus	strobus	k1gMnSc1	strobus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vlhčích	vlhký	k2eAgNnPc6d2	vlhčí
stanovištích	stanoviště	k1gNnPc6	stanoviště
okolí	okolí	k1gNnSc2	okolí
potoků	potok	k1gInPc2	potok
najdeme	najít	k5eAaPmIp1nP	najít
olši	olše	k1gFnSc4	olše
lepkavou	lepkavý	k2eAgFnSc4d1	lepkavá
(	(	kIx(	(
<g/>
Alnus	Alnus	k1gMnSc1	Alnus
glutinosa	glutinosa	k1gFnSc1	glutinosa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vrbu	vrba	k1gFnSc4	vrba
bílou	bílý	k2eAgFnSc4d1	bílá
(	(	kIx(	(
<g/>
Salix	Salix	k1gInSc1	Salix
alba	album	k1gNnSc2	album
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vyskytnout	vyskytnout	k5eAaPmF	vyskytnout
topol	topol	k1gInSc1	topol
osika	osika	k1gFnSc1	osika
(	(	kIx(	(
<g/>
Populus	Populus	k1gInSc1	Populus
tremula	tremulum	k1gNnSc2	tremulum
<g/>
)	)	kIx)	)
a	a	k8xC	a
vrba	vrba	k1gFnSc1	vrba
jíva	jíva	k1gFnSc1	jíva
(	(	kIx(	(
<g/>
Salix	Salix	k1gInSc1	Salix
caprea	capre	k1gInSc2	capre
L.	L.	kA	L.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
keřovém	keřový	k2eAgNnSc6d1	keřové
patře	patro	k1gNnSc6	patro
najdeme	najít	k5eAaPmIp1nP	najít
na	na	k7c6	na
slunných	slunný	k2eAgNnPc6d1	slunné
územích	území	k1gNnPc6	území
skalník	skalník	k1gInSc4	skalník
celokrajný	celokrajný	k2eAgInSc4d1	celokrajný
(	(	kIx(	(
<g/>
Cotoneaster	Cotoneaster	k1gInSc4	Cotoneaster
integerrimus	integerrimus	k1gInSc4	integerrimus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dřín	dřín	k1gInSc4	dřín
(	(	kIx(	(
<g/>
Cornus	Cornus	k1gInSc4	Cornus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
lesa	les	k1gInSc2	les
pak	pak	k6eAd1	pak
trnku	trnka	k1gFnSc4	trnka
obecnou	obecná	k1gFnSc4	obecná
(	(	kIx(	(
<g/>
Prunus	Prunus	k1gInSc4	Prunus
spinosa	spinosa	k1gFnSc1	spinosa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bez	bez	k1gInSc4	bez
černý	černý	k2eAgInSc4d1	černý
(	(	kIx(	(
<g/>
Sambucus	Sambucus	k1gInSc4	Sambucus
nigra	nigr	k1gMnSc2	nigr
<g/>
,	,	kIx,	,
L.	L.	kA	L.
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
bez	bez	k1gInSc1	bez
červený	červený	k2eAgInSc1d1	červený
(	(	kIx(	(
<g/>
Sambucus	Sambucus	k1gInSc1	Sambucus
racemosa	racemosa	k1gFnSc1	racemosa
<g/>
,	,	kIx,	,
L.	L.	kA	L.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pámelník	pámelník	k1gInSc4	pámelník
poříční	poříční	k2eAgInSc4d1	poříční
(	(	kIx(	(
<g/>
Symphoricarpos	Symphoricarpos	k1gInSc4	Symphoricarpos
rivularis	rivularis	k1gFnSc2	rivularis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kalinu	kalina	k1gFnSc4	kalina
obecnou	obecná	k1gFnSc4	obecná
(	(	kIx(	(
<g/>
Viburnum	Viburnum	k1gInSc1	Viburnum
opulus	opulus	k1gInSc1	opulus
L.	L.	kA	L.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lísku	líska	k1gFnSc4	líska
obecnou	obecný	k2eAgFnSc4d1	obecná
(	(	kIx(	(
<g/>
Corylus	Corylus	k1gMnSc1	Corylus
avellana	avellan	k1gMnSc2	avellan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
brslen	brslen	k1gInSc4	brslen
evropský	evropský	k2eAgInSc4d1	evropský
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Euonymus	Euonymus	k1gMnSc1	Euonymus
europaeus	europaeus	k1gMnSc1	europaeus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
růži	růže	k1gFnSc4	růže
šípkovou	šípkový	k2eAgFnSc4d1	šípková
(	(	kIx(	(
<g/>
Rosa	Rosa	k1gMnSc1	Rosa
canina	canina	k1gMnSc1	canina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hloh	hloh	k1gInSc1	hloh
obecný	obecný	k2eAgInSc1d1	obecný
(	(	kIx(	(
<g/>
Crataegus	Crataegus	k1gMnSc1	Crataegus
oxyacantha	oxyacantha	k1gMnSc1	oxyacantha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
meruzalku	meruzalka	k1gFnSc4	meruzalka
srstku	srstka	k1gFnSc4	srstka
(	(	kIx(	(
<g/>
Ribes	Ribes	k1gMnSc1	Ribes
uva-crispa	uvarisp	k1gMnSc2	uva-crisp
L.	L.	kA	L.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
svídu	svída	k1gFnSc4	svída
krvavou	krvavý	k2eAgFnSc4d1	krvavá
(	(	kIx(	(
<g/>
Cornus	Cornus	k1gMnSc1	Cornus
sanguinea	sanguinea	k1gMnSc1	sanguinea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
krušinu	krušina	k1gFnSc4	krušina
olšovou	olšový	k2eAgFnSc4d1	olšová
(	(	kIx(	(
<g/>
Frangula	Frangula	k1gFnSc1	Frangula
alnus	alnus	k1gMnSc1	alnus
<g/>
)	)	kIx)	)
a	a	k8xC	a
řešetlák	řešetlák	k1gInSc1	řešetlák
počistivý	počistivý	k2eAgInSc1d1	počistivý
(	(	kIx(	(
<g/>
Rhamnus	Rhamnus	k1gInSc1	Rhamnus
cathartica	cathartic	k1gInSc2	cathartic
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
bylinném	bylinný	k2eAgNnSc6d1	bylinné
patře	patro	k1gNnSc6	patro
na	na	k7c6	na
jižních	jižní	k2eAgInPc6d1	jižní
svazích	svah	k1gInPc6	svah
najdeme	najít	k5eAaPmIp1nP	najít
tolitu	tolita	k1gFnSc4	tolita
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
(	(	kIx(	(
<g/>
Vincetoxicum	Vincetoxicum	k1gInSc1	Vincetoxicum
hirundinaria	hirundinarium	k1gNnSc2	hirundinarium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jestřábník	jestřábník	k1gInSc1	jestřábník
chlupáček	chlupáček	k1gInSc1	chlupáček
(	(	kIx(	(
<g/>
Hieracium	Hieracium	k1gNnSc1	Hieracium
pilosella	pilosello	k1gNnSc2	pilosello
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jestřábník	jestřábník	k1gInSc1	jestřábník
bledý	bledý	k2eAgInSc1d1	bledý
(	(	kIx(	(
<g/>
Hieracium	Hieracium	k1gNnSc4	Hieracium
schmidtii	schmidtie	k1gFnSc4	schmidtie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kručinku	kručinka	k1gFnSc4	kručinka
barvířskou	barvířský	k2eAgFnSc4d1	barvířská
(	(	kIx(	(
<g/>
Genista	Genista	k1gMnSc1	Genista
tinctoria	tinctorium	k1gNnSc2	tinctorium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ostřicí	ostřice	k1gFnSc7	ostřice
nízkou	nízký	k2eAgFnSc7d1	nízká
(	(	kIx(	(
<g/>
Carex	Carex	k1gInSc1	Carex
humilis	humilis	k1gFnSc2	humilis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bělozářkou	bělozářka	k1gFnSc7	bělozářka
liliovitou	liliovitý	k2eAgFnSc7d1	liliovitá
(	(	kIx(	(
<g/>
Anthericum	Anthericum	k1gNnSc1	Anthericum
liliago	liliago	k1gNnSc1	liliago
<g/>
)	)	kIx)	)
a	a	k8xC	a
třemdavou	třemdava	k1gFnSc7	třemdava
bílou	bílý	k2eAgFnSc7d1	bílá
(	(	kIx(	(
<g/>
Dictamnus	Dictamnus	k1gMnSc1	Dictamnus
albus	albus	k1gMnSc1	albus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místech	místo	k1gNnPc6	místo
s	s	k7c7	s
hlubší	hluboký	k2eAgFnSc7d2	hlubší
a	a	k8xC	a
vlhčí	vlhký	k2eAgFnSc7d2	vlhčí
půdou	půda	k1gFnSc7	půda
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
černýšová	černýšový	k2eAgFnSc1d1	černýšová
dubohabřina	dubohabřina	k1gFnSc1	dubohabřina
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
roste	růst	k5eAaImIp3nS	růst
plicník	plicník	k1gInSc1	plicník
lékařský	lékařský	k2eAgInSc1d1	lékařský
(	(	kIx(	(
<g/>
Pulmonaria	Pulmonarium	k1gNnPc1	Pulmonarium
officinalis	officinalis	k1gFnPc2	officinalis
<g/>
)	)	kIx)	)
,	,	kIx,	,
jaterník	jaterník	k1gInSc1	jaterník
podléška	podléška	k1gFnSc1	podléška
(	(	kIx(	(
<g/>
Hepatica	Hepatic	k2eAgFnSc1d1	Hepatica
nobilis	nobilis	k1gFnSc1	nobilis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ptačinec	ptačinec	k1gInSc1	ptačinec
velkokvětý	velkokvětý	k2eAgInSc1d1	velkokvětý
(	(	kIx(	(
<g/>
Stellaria	Stellarium	k1gNnSc2	Stellarium
holostea	holoste	k1gInSc2	holoste
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hrachor	hrachor	k1gInSc1	hrachor
černý	černý	k2eAgInSc1d1	černý
(	(	kIx(	(
<g/>
Lathyrus	Lathyrus	k1gInSc1	Lathyrus
niger	niger	k1gInSc1	niger
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
černýš	černýš	k1gInSc1	černýš
hajní	hajní	k2eAgInSc1d1	hajní
(	(	kIx(	(
<g/>
Melampyrum	Melampyrum	k1gInSc1	Melampyrum
nemorosum	nemorosum	k1gInSc1	nemorosum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sasanka	sasanka	k1gFnSc1	sasanka
hajní	hajní	k2eAgFnSc1d1	hajní
(	(	kIx(	(
<g/>
Anemone	Anemon	k1gInSc5	Anemon
nemorosa	nemorosa	k1gFnSc1	nemorosa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hlístník	hlístník	k1gInSc1	hlístník
hnízdák	hnízdák	k1gInSc1	hnízdák
(	(	kIx(	(
<g/>
Neottia	Neottia	k1gFnSc1	Neottia
nidus-avis	nidusvis	k1gFnSc2	nidus-avis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
medovník	medovník	k1gInSc1	medovník
meduňkolistý	meduňkolistý	k2eAgInSc1d1	meduňkolistý
(	(	kIx(	(
<g/>
Melittis	Melittis	k1gInSc1	Melittis
melissophyllum	melissophyllum	k1gInSc1	melissophyllum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nízké	nízký	k2eAgFnPc1d1	nízká
doubravy	doubrava	k1gFnPc1	doubrava
jsou	být	k5eAaImIp3nP	být
obvyklé	obvyklý	k2eAgFnPc1d1	obvyklá
pro	pro	k7c4	pro
druhy	druh	k1gInPc4	druh
chudých	chudý	k2eAgFnPc2d1	chudá
kyselých	kyselý	k2eAgFnPc2d1	kyselá
půd	půda	k1gFnPc2	půda
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
vřes	vřes	k1gInSc4	vřes
obecný	obecný	k2eAgInSc4d1	obecný
(	(	kIx(	(
<g/>
Calluna	Calluna	k1gFnSc1	Calluna
vulgaris	vulgaris	k1gFnSc2	vulgaris
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bika	bika	k1gFnSc1	bika
bělavá	bělavý	k2eAgFnSc1d1	bělavá
(	(	kIx(	(
<g/>
Luzula	Luzula	k1gFnSc1	Luzula
luzuloides	luzuloides	k1gMnSc1	luzuloides
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
borůvka	borůvka	k1gFnSc1	borůvka
(	(	kIx(	(
<g/>
Vaccinium	Vaccinium	k1gNnSc1	Vaccinium
myrtillus	myrtillus	k1gInSc1	myrtillus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
skalních	skalní	k2eAgFnPc6d1	skalní
stepích	step	k1gFnPc6	step
najdeme	najít	k5eAaPmIp1nP	najít
kostřavu	kostřava	k1gFnSc4	kostřava
sivou	sivá	k1gFnSc4	sivá
(	(	kIx(	(
<g/>
Festuca	Festuca	k1gFnSc1	Festuca
pallens	pallens	k1gInSc1	pallens
<g/>
)	)	kIx)	)
křivatec	křivatec	k1gInSc1	křivatec
český	český	k2eAgInSc1d1	český
(	(	kIx(	(
<g/>
Gagea	Gage	k2eAgFnSc1d1	Gagea
bohemica	bohemica	k1gFnSc1	bohemica
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
významný	významný	k2eAgInSc4d1	významný
biotipem	biotip	k1gInSc7	biotip
jsou	být	k5eAaImIp3nP	být
acidofilní	acidofilní	k2eAgInPc1d1	acidofilní
trávníky	trávník	k1gInPc1	trávník
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
roste	růst	k5eAaImIp3nS	růst
koniklec	koniklec	k1gInSc1	koniklec
luční	luční	k2eAgInSc1d1	luční
(	(	kIx(	(
<g/>
Pulsatilla	Pulsatilla	k1gFnSc1	Pulsatilla
pratensis	pratensis	k1gFnSc2	pratensis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
řeřišničník	řeřišničník	k1gInSc1	řeřišničník
písečný	písečný	k2eAgInSc1d1	písečný
(	(	kIx(	(
<g/>
Cardaminopsis	Cardaminopsis	k1gInSc1	Cardaminopsis
arenosa	arenosa	k1gFnSc1	arenosa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ostřice	ostřice	k1gFnSc1	ostřice
jarní	jarní	k2eAgFnSc1d1	jarní
(	(	kIx(	(
<g/>
Carex	Carex	k1gInSc1	Carex
caryophyllea	caryophylle	k1gInSc2	caryophylle
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sesel	sesel	k1gInSc1	sesel
sivý	sivý	k2eAgInSc1d1	sivý
(	(	kIx(	(
<g/>
Seseli	Sesel	k1gInPc7	Sesel
osseum	osseum	k1gNnSc1	osseum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
přítoků	přítok	k1gInPc2	přítok
Břežanského	Břežanský	k2eAgInSc2d1	Břežanský
potoka	potok	k1gInSc2	potok
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
dalšími	další	k2eAgInPc7d1	další
zajímavými	zajímavý	k2eAgInPc7d1	zajímavý
druhy	druh	k1gInPc7	druh
<g/>
,	,	kIx,	,
např.	např.	kA	např.
samorostlík	samorostlík	k1gInSc1	samorostlík
klasnatý	klasnatý	k2eAgInSc1d1	klasnatý
(	(	kIx(	(
<g/>
Actea	Acte	k2eAgFnSc1d1	Actea
spicata	spicata	k1gFnSc1	spicata
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pižmovka	pižmovka	k1gFnSc1	pižmovka
mošusová	mošusový	k2eAgFnSc1d1	mošusová
(	(	kIx(	(
<g/>
Adoxa	Adox	k2eAgFnSc1d1	Adoxa
moschatellina	moschatellina	k1gFnSc1	moschatellina
<g/>
)	)	kIx)	)
či	či	k8xC	či
dymnivka	dymnivka	k1gFnSc1	dymnivka
bobovitá	bobovitý	k2eAgFnSc1d1	bobovitá
(	(	kIx(	(
<g/>
Corydalis	Corydalis	k1gFnSc1	Corydalis
intermedia	intermedium	k1gNnSc2	intermedium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fauna	fauna	k1gFnSc1	fauna
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c4	na
převážně	převážně	k6eAd1	převážně
dubové	dubový	k2eAgInPc4d1	dubový
lesní	lesní	k2eAgInPc4d1	lesní
porosty	porost	k1gInPc4	porost
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vázáno	vázat	k5eAaImNgNnS	vázat
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
setkáme	setkat	k5eAaPmIp1nP	setkat
s	s	k7c7	s
nápadným	nápadný	k2eAgNnSc7d1	nápadné
ohroženým	ohrožený	k2eAgNnSc7d1	ohrožené
a	a	k8xC	a
zákonem	zákon	k1gInSc7	zákon
chráněným	chráněný	k2eAgInSc7d1	chráněný
druhem	druh	k1gInSc7	druh
krajníkem	krajník	k1gInSc7	krajník
hnědým	hnědý	k2eAgFnPc3d1	hnědá
(	(	kIx(	(
<g/>
Calosoma	Calosoma	k1gFnSc1	Calosoma
inquisitor	inquisitor	k1gMnSc1	inquisitor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
druh	druh	k1gInSc4	druh
dravý	dravý	k2eAgInSc4d1	dravý
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
živí	živit	k5eAaImIp3nS	živit
housenkami	housenka	k1gFnPc7	housenka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pařezech	pařez	k1gInPc6	pařez
dubů	dub	k1gInPc2	dub
a	a	k8xC	a
habrů	habr	k1gInPc2	habr
žije	žít	k5eAaImIp3nS	žít
roháček	roháček	k1gInSc1	roháček
kozlík	kozlík	k1gInSc1	kozlík
(	(	kIx(	(
<g/>
Dorcus	Dorcus	k1gMnSc1	Dorcus
parallelipipedus	parallelipipedus	k1gMnSc1	parallelipipedus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
odumírajícím	odumírající	k2eAgNnSc6d1	odumírající
dřevě	dřevo	k1gNnSc6	dřevo
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
larvy	larva	k1gFnPc4	larva
tesaříkovitých	tesaříkovitý	k2eAgInPc2d1	tesaříkovitý
včetně	včetně	k7c2	včetně
vzácného	vzácný	k2eAgMnSc2d1	vzácný
tesaříka	tesařík	k1gMnSc2	tesařík
bukového	bukový	k2eAgMnSc2d1	bukový
(	(	kIx(	(
<g/>
Cerambyx	Cerambyx	k1gInSc4	Cerambyx
scopolii	scopolie	k1gFnSc4	scopolie
<g/>
)	)	kIx)	)
a	a	k8xC	a
tesaříka	tesařík	k1gMnSc2	tesařík
dubového	dubový	k2eAgMnSc2d1	dubový
(	(	kIx(	(
<g/>
Plagionotus	Plagionotus	k1gInSc4	Plagionotus
arcuatus	arcuatus	k1gInSc4	arcuatus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Setkáme	setkat	k5eAaPmIp1nP	setkat
se	se	k3xPyFc4	se
i	i	k9	i
s	s	k7c7	s
krásným	krásný	k2eAgInSc7d1	krásný
zlatohlávkem	zlatohlávek	k1gInSc7	zlatohlávek
zlatým	zlatý	k2eAgInSc7d1	zlatý
(	(	kIx(	(
<g/>
Cetonia	Cetonium	k1gNnSc2	Cetonium
aurata	aurata	k1gFnSc1	aurata
<g/>
)	)	kIx)	)
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
i	i	k9	i
množství	množství	k1gNnSc1	množství
motýlů	motýl	k1gMnPc2	motýl
<g/>
:	:	kIx,	:
např.	např.	kA	např.
otakárek	otakárek	k1gMnSc1	otakárek
fenyklový	fenyklový	k2eAgMnSc1d1	fenyklový
(	(	kIx(	(
<g/>
Papilio	Papilio	k1gMnSc1	Papilio
machaon	machaon	k1gMnSc1	machaon
<g/>
)	)	kIx)	)
otakárek	otakárek	k1gMnSc1	otakárek
ovocný	ovocný	k2eAgMnSc1d1	ovocný
(	(	kIx(	(
<g/>
Iphiclides	Iphiclides	k1gMnSc1	Iphiclides
podalirius	podalirius	k1gMnSc1	podalirius
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ostruháčci	ostruháček	k1gMnPc1	ostruháček
<g/>
,	,	kIx,	,
modrásci	modrásek	k1gMnPc1	modrásek
<g/>
,	,	kIx,	,
babočkovití	babočkovitý	k2eAgMnPc1d1	babočkovitý
-	-	kIx~	-
například	například	k6eAd1	například
batolec	batolec	k1gMnSc1	batolec
<g />
.	.	kIx.	.
</s>
<s>
červený	červený	k2eAgInSc1d1	červený
(	(	kIx(	(
<g/>
Apatura	Apatura	k1gFnSc1	Apatura
ilia	ilia	k1gMnSc1	ilia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bělopásek	bělopásek	k1gMnSc1	bělopásek
topolový	topolový	k2eAgMnSc1d1	topolový
(	(	kIx(	(
<g/>
Limenitis	Limenitis	k1gFnSc1	Limenitis
populi	popule	k1gFnSc4	popule
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
babočka	babočka	k1gFnSc1	babočka
paví	paví	k2eAgNnSc1d1	paví
oko	oko	k1gNnSc1	oko
(	(	kIx(	(
<g/>
Inachis	Inachis	k1gInSc1	Inachis
io	io	k?	io
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
nočních	noční	k2eAgFnPc2d1	noční
pak	pak	k6eAd1	pak
řada	řada	k1gFnSc1	řada
vzácných	vzácný	k2eAgInPc2d1	vzácný
druhů	druh	k1gInPc2	druh
můr	můra	k1gFnPc2	můra
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Luperina	Luperina	k1gMnSc1	Luperina
nickerlii	nickerlie	k1gFnSc4	nickerlie
(	(	kIx(	(
<g/>
Frr	Frr	k1gFnSc4	Frr
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
objeviteli	objevitel	k1gMnSc6	objevitel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ptačí	ptačí	k2eAgFnSc1d1	ptačí
populace	populace	k1gFnSc1	populace
v	v	k7c6	v
rezervaci	rezervace	k1gFnSc6	rezervace
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
rozmanitá	rozmanitý	k2eAgFnSc1d1	rozmanitá
<g/>
,	,	kIx,	,
právě	právě	k6eAd1	právě
kvůli	kvůli	k7c3	kvůli
různorodosti	různorodost	k1gFnSc3	různorodost
jejích	její	k3xOp3gInPc2	její
biotopů	biotop	k1gInPc2	biotop
<g/>
.	.	kIx.	.
</s>
<s>
Setkáme	setkat	k5eAaPmIp1nP	setkat
se	se	k3xPyFc4	se
z	z	k7c2	z
ptactvem	ptactvo	k1gNnSc7	ptactvo
vyhledávajícím	vyhledávající	k2eAgFnPc3d1	vyhledávající
otevřený	otevřený	k2eAgInSc4d1	otevřený
terén	terén	k1gInSc4	terén
<g/>
,	,	kIx,	,
skály	skála	k1gFnPc4	skála
<g/>
,	,	kIx,	,
vlhké	vlhký	k2eAgNnSc4d1	vlhké
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
slunné	slunný	k2eAgInPc4d1	slunný
svahy	svah	k1gInPc4	svah
a	a	k8xC	a
díky	díky	k7c3	díky
blízkosti	blízkost	k1gFnSc3	blízkost
lidských	lidský	k2eAgNnPc2d1	lidské
sídel	sídlo	k1gNnPc2	sídlo
i	i	k9	i
s	s	k7c7	s
ptactvem	ptactvo	k1gNnSc7	ptactvo
zahradním	zahradní	k2eAgNnSc7d1	zahradní
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
sov	sova	k1gFnPc2	sova
zde	zde	k6eAd1	zde
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
kalous	kalous	k1gMnSc1	kalous
ušatý	ušatý	k2eAgMnSc1d1	ušatý
(	(	kIx(	(
<g/>
Asio	Asio	k6eAd1	Asio
otus	otus	k6eAd1	otus
<g/>
)	)	kIx)	)
a	a	k8xC	a
puštík	puštík	k1gMnSc1	puštík
obecný	obecný	k2eAgMnSc1d1	obecný
(	(	kIx(	(
<g/>
Strix	Strix	k1gInSc1	Strix
aluco	aluco	k1gNnSc1	aluco
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dutinách	dutina	k1gFnPc6	dutina
stromů	strom	k1gInPc2	strom
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
strakapoud	strakapoud	k1gMnSc1	strakapoud
prostřední	prostřední	k2eAgMnSc1d1	prostřední
(	(	kIx(	(
<g/>
Dendrocopos	Dendrocopos	k1gMnSc1	Dendrocopos
medius	medius	k1gMnSc1	medius
<g/>
)	)	kIx)	)
a	a	k8xC	a
žluna	žluna	k1gFnSc1	žluna
zelená	zelená	k1gFnSc1	zelená
(	(	kIx(	(
<g/>
Picus	Picus	k1gInSc1	Picus
viridis	viridis	k1gFnSc2	viridis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgMnPc2d1	další
ptáků	pták	k1gMnPc2	pták
zde	zde	k6eAd1	zde
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
kukačka	kukačka	k1gFnSc1	kukačka
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Cuculus	Cuculus	k1gMnSc1	Cuculus
canorus	canorus	k1gMnSc1	canorus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sýkora	sýkora	k1gFnSc1	sýkora
koňadra	koňadra	k1gFnSc1	koňadra
(	(	kIx(	(
<g/>
Parus	Parus	k1gMnSc1	Parus
major	major	k1gMnSc1	major
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sýkora	sýkora	k1gFnSc1	sýkora
modřinka	modřinka	k1gFnSc1	modřinka
(	(	kIx(	(
<g/>
Cyanistes	Cyanistes	k1gMnSc1	Cyanistes
caeruleus	caeruleus	k1gMnSc1	caeruleus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
brhlík	brhlík	k1gMnSc1	brhlík
lesní	lesní	k2eAgMnSc1d1	lesní
(	(	kIx(	(
<g/>
Sitta	Sitta	k1gMnSc1	Sitta
europaea	europaea	k1gMnSc1	europaea
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pěnice	pěnice	k1gFnSc1	pěnice
pokřovní	pokřovní	k2eAgFnSc1d1	pokřovní
(	(	kIx(	(
<g/>
Sylvia	Sylvia	k1gFnSc1	Sylvia
<g />
.	.	kIx.	.
</s>
<s>
curruca	curruca	k1gFnSc1	curruca
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
budníček	budníček	k1gMnSc1	budníček
lesní	lesní	k2eAgMnSc1d1	lesní
(	(	kIx(	(
<g/>
Phylloscopus	Phylloscopus	k1gInSc1	Phylloscopus
sibilatrix	sibilatrix	k1gInSc1	sibilatrix
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
červenka	červenka	k1gFnSc1	červenka
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Erithacus	Erithacus	k1gInSc1	Erithacus
rubecula	rubeculum	k1gNnSc2	rubeculum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
drozd	drozd	k1gMnSc1	drozd
zpěvný	zpěvný	k2eAgMnSc1d1	zpěvný
(	(	kIx(	(
<g/>
Turdus	Turdus	k1gMnSc1	Turdus
philomelos	philomelos	k1gMnSc1	philomelos
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pěnkava	pěnkava	k1gFnSc1	pěnkava
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
Fringilla	Fringilla	k1gFnSc1	Fringilla
coelebs	coelebsa	k1gFnPc2	coelebsa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sedmihlásek	sedmihlásek	k1gMnSc1	sedmihlásek
hajní	hajní	k2eAgMnSc1d1	hajní
(	(	kIx(	(
<g/>
Hippolais	Hippolais	k1gInSc1	Hippolais
icterina	icterino	k1gNnSc2	icterino
<g/>
)	)	kIx)	)
a	a	k8xC	a
ťuhýk	ťuhýk	k1gMnSc1	ťuhýk
obecný	obecný	k2eAgMnSc1d1	obecný
(	(	kIx(	(
<g/>
Lanius	Lanius	k1gMnSc1	Lanius
collurio	collurio	k1gMnSc1	collurio
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
okrajích	okraj	k1gInPc6	okraj
lesa	les	k1gInSc2	les
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
bažantem	bažant	k1gMnSc7	bažant
obecným	obecný	k2eAgNnSc7d1	obecné
(	(	kIx(	(
<g/>
Phasianus	Phasianus	k1gInSc1	Phasianus
colchicus	colchicus	k1gInSc1	colchicus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
savců	savec	k1gMnPc2	savec
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
často	často	k6eAd1	často
setkat	setkat	k5eAaPmF	setkat
se	s	k7c7	s
zástupcem	zástupce	k1gMnSc7	zástupce
hlodavců	hlodavec	k1gMnPc2	hlodavec
veverkou	veverka	k1gFnSc7	veverka
obecnou	obecná	k1gFnSc7	obecná
(	(	kIx(	(
<g/>
Sciurus	Sciurus	k1gInSc1	Sciurus
vulgaris	vulgaris	k1gFnSc2	vulgaris
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
šelem	šelma	k1gFnPc2	šelma
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
potkat	potkat	k5eAaPmF	potkat
s	s	k7c7	s
liškou	liška	k1gFnSc7	liška
obecnou	obecný	k2eAgFnSc7d1	obecná
(	(	kIx(	(
<g/>
Vulpes	Vulpesa	k1gFnPc2	Vulpesa
vulpes	vulpes	k1gInSc1	vulpes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
živí	živit	k5eAaImIp3nP	živit
převážně	převážně	k6eAd1	převážně
drobnými	drobný	k2eAgMnPc7d1	drobný
hlodavci	hlodavec	k1gMnPc7	hlodavec
nebo	nebo	k8xC	nebo
dalšími	další	k2eAgMnPc7d1	další
obyvateli	obyvatel	k1gMnPc7	obyvatel
lesa	les	k1gInSc2	les
králíkem	králík	k1gMnSc7	králík
divokým	divoký	k2eAgInSc7d1	divoký
(	(	kIx(	(
<g/>
Oryctolagus	Oryctolagus	k1gInSc1	Oryctolagus
cuniculus	cuniculus	k1gInSc1	cuniculus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zajícem	zajíc	k1gMnSc7	zajíc
polním	polní	k2eAgNnSc7d1	polní
(	(	kIx(	(
<g/>
Lepus	Lepus	k1gInSc1	Lepus
europaeus	europaeus	k1gInSc1	europaeus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spíše	spíše	k9	spíše
výjimečně	výjimečně	k6eAd1	výjimečně
se	se	k3xPyFc4	se
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
kunou	kuna	k1gFnSc7	kuna
skalní	skalní	k2eAgFnSc7d1	skalní
(	(	kIx(	(
<g/>
Martes	Martesa	k1gFnPc2	Martesa
foina	foina	k6eAd1	foina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kunou	kuna	k1gFnSc7	kuna
lesní	lesní	k2eAgFnSc7d1	lesní
(	(	kIx(	(
<g/>
Martes	Martesa	k1gFnPc2	Martesa
martes	martes	k1gInSc1	martes
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
tchořem	tchoř	k1gMnSc7	tchoř
tmavým	tmavý	k2eAgFnPc3d1	tmavá
(	(	kIx(	(
<g/>
Mustela	Mustela	k1gFnSc1	Mustela
putorius	putorius	k1gMnSc1	putorius
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
velkých	velký	k2eAgNnPc2d1	velké
zvířat	zvíře	k1gNnPc2	zvíře
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
setkáme	setkat	k5eAaPmIp1nP	setkat
se	s	k7c7	s
srncem	srnec	k1gMnSc7	srnec
obecným	obecný	k2eAgNnPc3d1	obecné
(	(	kIx(	(
<g/>
Capreolus	Capreolus	k1gInSc4	Capreolus
capreolus	capreolus	k1gInSc4	capreolus
<g/>
)	)	kIx)	)
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
v	v	k7c6	v
rezavém	rezavý	k2eAgNnSc6d1	rezavé
a	a	k8xC	a
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
v	v	k7c6	v
šedohnědém	šedohnědý	k2eAgNnSc6d1	šedohnědé
zbarvení	zbarvení	k1gNnSc6	zbarvení
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc4	tento
zvířata	zvíře	k1gNnPc4	zvíře
vídáme	vídat	k5eAaImIp1nP	vídat
většinou	většina	k1gFnSc7	většina
v	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
ve	v	k7c6	v
větších	veliký	k2eAgFnPc6d2	veliký
skupinách	skupina	k1gFnPc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Poměrně	poměrně	k6eAd1	poměrně
hojně	hojně	k6eAd1	hojně
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
také	také	k9	také
prase	prase	k1gNnSc1	prase
divoké	divoký	k2eAgNnSc1d1	divoké
(	(	kIx(	(
<g/>
Sus	Sus	k1gMnSc4	Sus
scrofa	scrof	k1gMnSc4	scrof
<g/>
)	)	kIx)	)
a	a	k8xC	a
muflon	muflon	k1gMnSc1	muflon
(	(	kIx(	(
<g/>
Ovis	Ovis	k1gInSc1	Ovis
musimon	musimon	k1gInSc1	musimon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
významným	významný	k2eAgInSc7d1	významný
způsobem	způsob	k1gInSc7	způsob
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
okolní	okolní	k2eAgInPc1d1	okolní
biotopy	biotop	k1gInPc1	biotop
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ochrana	ochrana	k1gFnSc1	ochrana
==	==	k?	==
</s>
</p>
<p>
<s>
Předmětem	předmět	k1gInSc7	předmět
ochrany	ochrana	k1gFnSc2	ochrana
v	v	k7c6	v
přírodní	přírodní	k2eAgFnSc6d1	přírodní
rezervaci	rezervace	k1gFnSc6	rezervace
jsou	být	k5eAaImIp3nP	být
přirozené	přirozený	k2eAgInPc1d1	přirozený
porosty	porost	k1gInPc1	porost
doubrav	doubrava	k1gFnPc2	doubrava
orientované	orientovaný	k2eAgNnSc4d1	orientované
zejména	zejména	k9	zejména
na	na	k7c4	na
jižní	jižní	k2eAgInPc4d1	jižní
svahy	svah	k1gInPc4	svah
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
přirozené	přirozený	k2eAgNnSc4d1	přirozené
bezlesí	bezlesí	k1gNnSc4	bezlesí
tvořené	tvořený	k2eAgNnSc4d1	tvořené
plochami	plocha	k1gFnPc7	plocha
suchých	suchý	k2eAgNnPc2d1	suché
vřesovišť	vřesoviště	k1gNnPc2	vřesoviště
<g/>
,	,	kIx,	,
skalní	skalní	k2eAgFnSc7d1	skalní
vegetací	vegetace	k1gFnSc7	vegetace
s	s	k7c7	s
kostřavou	kostřava	k1gFnSc7	kostřava
sivou	sivá	k1gFnSc7	sivá
a	a	k8xC	a
acidofilními	acidofilní	k2eAgInPc7d1	acidofilní
trávníky	trávník	k1gInPc7	trávník
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
místní	místní	k2eAgNnSc4d1	místní
stanoviště	stanoviště	k1gNnSc4	stanoviště
vázané	vázaný	k2eAgInPc4d1	vázaný
chráněné	chráněný	k2eAgInPc4d1	chráněný
druhy	druh	k1gInPc4	druh
rostlin	rostlina	k1gFnPc2	rostlina
a	a	k8xC	a
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
je	být	k5eAaImIp3nS	být
stabilní	stabilní	k2eAgFnSc1d1	stabilní
populace	populace	k1gFnSc1	populace
přástevníka	přástevník	k1gMnSc2	přástevník
kostivalového	kostivalový	k2eAgMnSc2d1	kostivalový
(	(	kIx(	(
<g/>
Euplagia	Euplagium	k1gNnSc2	Euplagium
quadripunctaria	quadripunctarium	k1gNnSc2	quadripunctarium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgMnPc4d1	další
chráněné	chráněný	k2eAgMnPc4d1	chráněný
živočichy	živočich	k1gMnPc4	živočich
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
brouk	brouk	k1gMnSc1	brouk
krajník	krajník	k1gMnSc1	krajník
hnědý	hnědý	k2eAgMnSc1d1	hnědý
(	(	kIx(	(
<g/>
Calosoma	Calosoma	k1gFnSc1	Calosoma
inquisitor	inquisitor	k1gMnSc1	inquisitor
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
řada	řada	k1gFnSc1	řada
motýlů	motýl	k1gMnPc2	motýl
<g/>
:	:	kIx,	:
otakárek	otakárek	k1gMnSc1	otakárek
fenyklový	fenyklový	k2eAgMnSc1d1	fenyklový
(	(	kIx(	(
<g/>
Papilio	Papilio	k1gMnSc1	Papilio
machaon	machaon	k1gMnSc1	machaon
<g/>
)	)	kIx)	)
otakárek	otakárek	k1gMnSc1	otakárek
ovocný	ovocný	k2eAgMnSc1d1	ovocný
(	(	kIx(	(
<g/>
Iphiclides	Iphiclides	k1gMnSc1	Iphiclides
podalirius	podalirius	k1gMnSc1	podalirius
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
batolec	batolec	k1gMnSc1	batolec
červený	červený	k2eAgMnSc1d1	červený
(	(	kIx(	(
<g/>
Apatura	Apatura	k1gFnSc1	Apatura
ilia	ilia	k1gMnSc1	ilia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bělopásek	bělopásek	k1gMnSc1	bělopásek
topolový	topolový	k2eAgMnSc1d1	topolový
(	(	kIx(	(
<g/>
Limenitis	Limenitis	k1gFnSc1	Limenitis
populi	popule	k1gFnSc4	popule
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ptáku	pták	k1gMnSc6	pták
zde	zde	k6eAd1	zde
žije	žít	k5eAaImIp3nS	žít
ohrožený	ohrožený	k2eAgMnSc1d1	ohrožený
strakapoud	strakapoud	k1gMnSc1	strakapoud
prostřední	prostřední	k2eAgMnSc1d1	prostřední
(	(	kIx(	(
<g/>
Dendrocopos	Dendrocopos	k1gMnSc1	Dendrocopos
medius	medius	k1gMnSc1	medius
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
ťuhýk	ťuhýk	k1gMnSc1	ťuhýk
obecný	obecný	k2eAgMnSc1d1	obecný
(	(	kIx(	(
<g/>
Lanius	Lanius	k1gMnSc1	Lanius
collurio	collurio	k1gMnSc1	collurio
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Břežanské	Břežanský	k2eAgNnSc1d1	Břežanské
údolí	údolí	k1gNnSc1	údolí
===	===	k?	===
</s>
</p>
<p>
<s>
Břežanské	Břežanský	k2eAgNnSc1d1	Břežanské
údolí	údolí	k1gNnSc1	údolí
je	být	k5eAaImIp3nS	být
zařazeno	zařadit	k5eAaPmNgNnS	zařadit
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
evropsky	evropsky	k6eAd1	evropsky
významných	významný	k2eAgFnPc2d1	významná
lokalit	lokalita	k1gFnPc2	lokalita
soustavy	soustava	k1gFnSc2	soustava
Natura	Natura	k1gFnSc1	Natura
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
Lokalita	lokalita	k1gFnSc1	lokalita
byla	být	k5eAaImAgFnS	být
schválena	schválit	k5eAaPmNgFnS	schválit
evropskou	evropský	k2eAgFnSc7d1	Evropská
komisí	komise	k1gFnSc7	komise
již	již	k9	již
13	[number]	k4	13
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
k	k	k7c3	k
vyhlášení	vyhlášení	k1gNnSc3	vyhlášení
však	však	k9	však
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
5	[number]	k4	5
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Ochrana	ochrana	k1gFnSc1	ochrana
této	tento	k3xDgFnSc2	tento
lokality	lokalita	k1gFnSc2	lokalita
byla	být	k5eAaImAgFnS	být
zajištěna	zajistit	k5eAaPmNgFnS	zajistit
rozšířením	rozšíření	k1gNnSc7	rozšíření
stávajícího	stávající	k2eAgNnSc2d1	stávající
území	území	k1gNnSc2	území
přírodní	přírodní	k2eAgFnSc2d1	přírodní
rezervace	rezervace	k1gFnSc2	rezervace
Šance	šance	k1gFnSc1	šance
zhruba	zhruba	k6eAd1	zhruba
o	o	k7c4	o
82	[number]	k4	82
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
důvody	důvod	k1gInPc7	důvod
rozšíření	rozšíření	k1gNnSc2	rozšíření
území	území	k1gNnSc2	území
byla	být	k5eAaImAgFnS	být
zvláště	zvláště	k6eAd1	zvláště
ochrana	ochrana	k1gFnSc1	ochrana
populace	populace	k1gFnSc2	populace
přástevníka	přástevník	k1gMnSc2	přástevník
kostivalového	kostivalový	k2eAgMnSc2d1	kostivalový
(	(	kIx(	(
<g/>
Euplagia	Euplagium	k1gNnSc2	Euplagium
quadripunctaria	quadripunctarium	k1gNnSc2	quadripunctarium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
zejména	zejména	k9	zejména
výslunné	výslunný	k2eAgFnPc4d1	výslunná
stráně	stráň	k1gFnPc4	stráň
<g/>
,	,	kIx,	,
řídké	řídký	k2eAgFnPc4d1	řídká
teplomilné	teplomilný	k2eAgFnPc4d1	teplomilná
doubravy	doubrava	k1gFnPc4	doubrava
a	a	k8xC	a
křovinaté	křovinatý	k2eAgFnPc4d1	křovinatá
lesostepi	lesostep	k1gFnPc4	lesostep
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Břežanském	Břežanský	k2eAgNnSc6d1	Břežanské
údolí	údolí	k1gNnSc6	údolí
je	být	k5eAaImIp3nS	být
předmětem	předmět	k1gInSc7	předmět
ochrany	ochrana	k1gFnSc2	ochrana
také	také	k9	také
početná	početný	k2eAgFnSc1d1	početná
populace	populace	k1gFnSc1	populace
mloka	mlok	k1gMnSc2	mlok
skvrnitého	skvrnitý	k2eAgMnSc2d1	skvrnitý
(	(	kIx(	(
<g/>
Salamandra	salamandr	k1gMnSc2	salamandr
salamandra	salamandr	k1gMnSc2	salamandr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poblíž	poblíž	k7c2	poblíž
lokality	lokalita	k1gFnSc2	lokalita
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
dvě	dva	k4xCgFnPc4	dva
skládky	skládka	k1gFnPc4	skládka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
sesuvu	sesuv	k1gInSc3	sesuv
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
rozvalení	rozvalení	k1gNnSc2	rozvalení
části	část	k1gFnSc2	část
skládek	skládka	k1gFnPc2	skládka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
představuje	představovat	k5eAaImIp3nS	představovat
ohrožení	ohrožení	k1gNnSc4	ohrožení
potočních	potoční	k2eAgInPc2d1	potoční
biotopů	biotop	k1gInPc2	biotop
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Plán	plán	k1gInSc1	plán
péče	péče	k1gFnSc2	péče
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
lokalitu	lokalita	k1gFnSc4	lokalita
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
několik	několik	k4yIc1	několik
opatření	opatření	k1gNnPc2	opatření
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
se	se	k3xPyFc4	se
různí	různit	k5eAaImIp3nP	různit
s	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
konkrétní	konkrétní	k2eAgNnSc4d1	konkrétní
stanoviště	stanoviště	k1gNnSc4	stanoviště
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
podmáčené	podmáčený	k2eAgFnPc4d1	podmáčená
a	a	k8xC	a
těžko	těžko	k6eAd1	těžko
dostupné	dostupný	k2eAgFnPc1d1	dostupná
lokality	lokalita	k1gFnPc1	lokalita
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
ruční	ruční	k2eAgNnSc4d1	ruční
kosení	kosení	k1gNnSc4	kosení
mozaikovitou	mozaikovitý	k2eAgFnSc7d1	mozaikovitá
sečí	seč	k1gFnSc7	seč
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
xerotermní	xerotermní	k2eAgInPc4d1	xerotermní
trávníky	trávník	k1gInPc4	trávník
by	by	kYmCp3nP	by
byla	být	k5eAaImAgFnS	být
nejvhodnější	vhodný	k2eAgFnSc1d3	nejvhodnější
pastva	pastva	k1gFnSc1	pastva
kozami	koza	k1gFnPc7	koza
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
přástevníka	přástevník	k1gMnSc2	přástevník
kostivalového	kostivalový	k2eAgMnSc2d1	kostivalový
(	(	kIx(	(
<g/>
Euplagia	Euplagium	k1gNnSc2	Euplagium
quadripunctaria	quadripunctarium	k1gNnSc2	quadripunctarium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vhodnou	vhodný	k2eAgFnSc7d1	vhodná
péčí	péče	k1gFnSc7	péče
mozaikovitý	mozaikovitý	k2eAgInSc1d1	mozaikovitý
výřez	výřez	k1gInSc1	výřez
náletových	náletový	k2eAgFnPc2d1	náletová
křovin	křovina	k1gFnPc2	křovina
a	a	k8xC	a
dřevin	dřevina	k1gFnPc2	dřevina
<g/>
,	,	kIx,	,
možno	možno	k6eAd1	možno
kombinovat	kombinovat	k5eAaImF	kombinovat
s	s	k7c7	s
pastvou	pastva	k1gFnSc7	pastva
<g/>
.	.	kIx.	.
</s>
<s>
Veškeré	veškerý	k3xTgInPc1	veškerý
nelesní	lesní	k2eNgInPc1d1	nelesní
biotopy	biotop	k1gInPc1	biotop
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
chráněny	chránit	k5eAaImNgInP	chránit
proti	proti	k7c3	proti
zarůstání	zarůstání	k1gNnSc3	zarůstání
dřevinami	dřevina	k1gFnPc7	dřevina
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
měla	mít	k5eAaImAgFnS	mít
dělat	dělat	k5eAaImF	dělat
prořezávka	prořezávka	k1gFnSc1	prořezávka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lesích	les	k1gInPc6	les
je	být	k5eAaImIp3nS	být
žádoucí	žádoucí	k2eAgNnSc1d1	žádoucí
ponechávat	ponechávat	k5eAaImF	ponechávat
dostatek	dostatek	k1gInSc4	dostatek
odumřelého	odumřelý	k2eAgNnSc2d1	odumřelé
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
významné	významný	k2eAgNnSc1d1	významné
pro	pro	k7c4	pro
další	další	k2eAgInPc4d1	další
chráněné	chráněný	k2eAgInPc4d1	chráněný
organismy	organismus	k1gInPc4	organismus
zejména	zejména	k9	zejména
ptáky	pták	k1gMnPc4	pták
a	a	k8xC	a
saproxylický	saproxylický	k2eAgInSc1d1	saproxylický
hmyz	hmyz	k1gInSc1	hmyz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Turismus	turismus	k1gInSc4	turismus
==	==	k?	==
</s>
</p>
<p>
<s>
Územím	území	k1gNnSc7	území
vede	vést	k5eAaImIp3nS	vést
naučná	naučný	k2eAgFnSc1d1	naučná
stezka	stezka	k1gFnSc1	stezka
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
hojně	hojně	k6eAd1	hojně
využíváno	využívat	k5eAaPmNgNnS	využívat
k	k	k7c3	k
turistice	turistika	k1gFnSc3	turistika
<g/>
,	,	kIx,	,
cyklistice	cyklistika	k1gFnSc6	cyklistika
a	a	k8xC	a
jízdě	jízda	k1gFnSc6	jízda
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
odhazováním	odhazování	k1gNnSc7	odhazování
drobného	drobný	k2eAgInSc2d1	drobný
odpadu	odpad	k1gInSc2	odpad
a	a	k8xC	a
sešlapem	sešlapem	k?	sešlapem
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
místy	místo	k1gNnPc7	místo
i	i	k8xC	i
přínosem	přínos	k1gInSc7	přínos
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
udržování	udržování	k1gNnSc3	udržování
nezalesněných	zalesněný	k2eNgFnPc2d1	nezalesněná
plošek	ploška	k1gFnPc2	ploška
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
Břežanského	Břežanský	k2eAgNnSc2d1	Břežanské
údolí	údolí	k1gNnSc2	údolí
se	se	k3xPyFc4	se
na	na	k7c6	na
levé	levý	k2eAgFnSc6d1	levá
straně	strana	k1gFnSc6	strana
Břežanského	Břežanský	k2eAgInSc2d1	Břežanský
potoka	potok	k1gInSc2	potok
nachází	nacházet	k5eAaImIp3nS	nacházet
malá	malý	k2eAgFnSc1d1	malá
lesní	lesní	k2eAgFnSc1d1	lesní
zoo	zoo	k1gFnSc1	zoo
Zbraslav	Zbraslav	k1gFnSc1	Zbraslav
(	(	kIx(	(
<g/>
vzdálenost	vzdálenost	k1gFnSc1	vzdálenost
asi	asi	k9	asi
500	[number]	k4	500
<g/>
m	m	kA	m
od	od	k7c2	od
vlakové	vlakový	k2eAgFnSc2d1	vlaková
zastávky	zastávka	k1gFnSc2	zastávka
Zbraslav	Zbraslav	k1gFnSc1	Zbraslav
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
chráněných	chráněný	k2eAgNnPc2d1	chráněné
území	území	k1gNnPc2	území
Prahy	Praha	k1gFnSc2	Praha
</s>
</p>
<p>
<s>
Oppidum	oppidum	k1gNnSc1	oppidum
Závist	závist	k1gFnSc1	závist
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Šance	šance	k1gFnSc2	šance
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Detailní	detailní	k2eAgInSc1d1	detailní
popis	popis	k1gInSc1	popis
rezervace	rezervace	k1gFnSc2	rezervace
</s>
</p>
