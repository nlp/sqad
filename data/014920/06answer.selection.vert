<s>
Do	do	k7c2
Octárny	octárna	k1gFnSc2
se	se	k3xPyFc4
stéká	stékat	k5eAaImIp3nS
mnoho	mnoho	k4c1
malých	malý	k2eAgInPc2d1
rašelinných	rašelinný	k2eAgInPc2d1
potůčků	potůček	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
příčinou	příčina	k1gFnSc7
kyselosti	kyselost	k1gFnSc2
vody	voda	k1gFnSc2
v	v	k7c6
nádrži	nádrž	k1gFnSc6
(	(	kIx(
<g/>
pH	ph	kA
=	=	kIx~
4	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>