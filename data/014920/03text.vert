<s>
Octárna	octárna	k1gFnSc1
</s>
<s>
Octárna	octárna	k1gFnSc1
Vodní	vodní	k2eAgFnSc1d1
nádrž	nádrž	k1gFnSc1
Octárna	octárna	k1gFnSc1
v	v	k7c4
BrdechPoloha	BrdechPoloh	k1gMnSc4
Světadíl	světadíl	k1gInSc4
</s>
<s>
Evropa	Evropa	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Kraj	kraj	k7c2
</s>
<s>
Středočeský	středočeský	k2eAgInSc1d1
Okres	okres	k1gInSc1
</s>
<s>
Příbram	Příbram	k1gFnSc1
</s>
<s>
Octárna	octárna	k1gFnSc1
</s>
<s>
Zeměpisné	zeměpisný	k2eAgFnPc4d1
souřadnice	souřadnice	k1gFnPc4
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
43	#num#	k4
<g/>
′	′	k?
<g/>
5	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
13	#num#	k4
<g/>
°	°	k?
<g/>
55	#num#	k4
<g/>
′	′	k?
<g/>
31	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Rozměry	rozměra	k1gFnSc2
Rozloha	rozloha	k1gFnSc1
</s>
<s>
11	#num#	k4
ha	ha	kA
Délka	délka	k1gFnSc1
</s>
<s>
450	#num#	k4
m	m	kA
Šířka	šířka	k1gFnSc1
</s>
<s>
315	#num#	k4
m	m	kA
Max	max	kA
<g/>
.	.	kIx.
hloubka	hloubka	k1gFnSc1
</s>
<s>
11,5	11,5	k4
m	m	kA
Ostatní	ostatní	k2eAgInSc1d1
Typ	typ	k1gInSc1
</s>
<s>
vodárenská	vodárenský	k2eAgFnSc1d1
nádrž	nádrž	k1gFnSc1
Nadm	Nadm	k1gMnSc1
<g/>
.	.	kIx.
výška	výška	k1gFnSc1
</s>
<s>
570	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Přítok	přítok	k1gInSc4
vody	voda	k1gFnSc2
</s>
<s>
Obecnický	Obecnický	k2eAgInSc1d1
potok	potok	k1gInSc1
a	a	k8xC
mnoho	mnoho	k4c1
malých	malý	k2eAgInPc2d1
bezejmenných	bezejmenný	k2eAgInPc2d1
potůčků	potůček	k1gInPc2
Odtok	odtok	k1gInSc4
vody	voda	k1gFnSc2
</s>
<s>
Obecnický	Obecnický	k2eAgInSc1d1
potok	potok	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Octárna	octárna	k1gFnSc1
(	(	kIx(
<g/>
někdy	někdy	k6eAd1
též	též	k9
vodní	vodní	k2eAgFnSc1d1
nádrž	nádrž	k1gFnSc1
Obecnice	obecnice	k1gFnSc2
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vodní	vodní	k2eAgFnSc1d1
nádrž	nádrž	k1gFnSc1
na	na	k7c6
Obecnickém	Obecnický	k2eAgInSc6d1
potoce	potok	k1gInSc6
asi	asi	k9
5	#num#	k4
kilometrů	kilometr	k1gInPc2
severozápadně	severozápadně	k6eAd1
od	od	k7c2
Příbrami	Příbram	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
území	území	k1gNnSc6
bývalého	bývalý	k2eAgInSc2d1
vojenského	vojenský	k2eAgInSc2d1
újezdu	újezd	k1gInSc2
Brdy	Brdy	k1gInPc4
<g/>
,	,	kIx,
nyní	nyní	k6eAd1
na	na	k7c6
území	území	k1gNnSc6
obce	obec	k1gFnSc2
Obecnice	obecnice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Octárna	octárna	k1gFnSc1
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
v	v	k7c6
18	#num#	k4
<g/>
.	.	kIx.
stol.	stol.	k?
Roku	rok	k1gInSc2
1962	#num#	k4
byla	být	k5eAaImAgFnS
upravena	upravit	k5eAaPmNgFnS
a	a	k8xC
rozšířena	rozšířit	k5eAaPmNgFnS
na	na	k7c4
dnešní	dnešní	k2eAgFnSc4d1
výměru	výměra	k1gFnSc4
11	#num#	k4
ha	ha	kA
a	a	k8xC
prohloubena	prohlouben	k2eAgFnSc1d1
na	na	k7c4
maximální	maximální	k2eAgFnSc4d1
hloubku	hloubka	k1gFnSc4
11,5	11,5	k4
m.	m.	k?
</s>
<s>
Nádrž	nádrž	k1gFnSc1
slouží	sloužit	k5eAaImIp3nS
pro	pro	k7c4
zásobování	zásobování	k1gNnSc4
příbramské	příbramský	k2eAgFnSc2d1
aglomerace	aglomerace	k1gFnSc2
a	a	k8xC
zabezpečení	zabezpečení	k1gNnSc2
minimálních	minimální	k2eAgInPc2d1
průtoků	průtok	k1gInPc2
pod	pod	k7c7
hrází	hráz	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
Do	do	k7c2
Octárny	octárna	k1gFnSc2
se	se	k3xPyFc4
stéká	stékat	k5eAaImIp3nS
mnoho	mnoho	k4c1
malých	malý	k2eAgInPc2d1
rašelinných	rašelinný	k2eAgInPc2d1
potůčků	potůček	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
příčinou	příčina	k1gFnSc7
kyselosti	kyselost	k1gFnSc2
vody	voda	k1gFnSc2
v	v	k7c6
nádrži	nádrž	k1gFnSc6
(	(	kIx(
<g/>
pH	ph	kA
=	=	kIx~
4	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
to	ten	k3xDgNnSc1
je	být	k5eAaImIp3nS
důvod	důvod	k1gInSc4
proč	proč	k6eAd1
se	se	k3xPyFc4
zde	zde	k6eAd1
nikdy	nikdy	k6eAd1
nezdařila	zdařit	k5eNaPmAgFnS
výsadba	výsadba	k1gFnSc1
ryb	ryba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
přesto	přesto	k8xC
se	se	k3xPyFc4
v	v	k7c6
Obecnickém	Obecnický	k2eAgInSc6d1
potoce	potok	k1gInSc6
ještě	ještě	k9
50	#num#	k4
m	m	kA
metrů	metr	k1gInPc2
před	před	k7c7
vtokem	vtok	k1gInSc7
do	do	k7c2
Octárny	octárna	k1gFnSc2
vyskytují	vyskytovat	k5eAaImIp3nP
např.	např.	kA
pstruzi	pstruh	k1gMnPc1
nebo	nebo	k8xC
okouni	okoun	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
zajímavostí	zajímavost	k1gFnSc7
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Octárna	octárna	k1gFnSc1
je	být	k5eAaImIp3nS
jediné	jediný	k2eAgNnSc4d1
místo	místo	k1gNnSc4
Čechách	Čechy	k1gFnPc6
kde	kde	k6eAd1
roste	růst	k5eAaImIp3nS
orobinec	orobinec	k1gInSc1
stříbrošedý	stříbrošedý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Octárna	octárna	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
