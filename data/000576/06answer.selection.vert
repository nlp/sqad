<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
známým	známý	k2eAgMnSc7d1	známý
tvůrcem	tvůrce	k1gMnSc7	tvůrce
nebo	nebo	k8xC	nebo
spolutvůrcem	spolutvůrce	k1gMnSc7	spolutvůrce
televizního	televizní	k2eAgInSc2d1	televizní
seriálu	seriál	k1gInSc2	seriál
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jonathanem	Jonathan	k1gMnSc7	Jonathan
Glassnerem	Glassner	k1gMnSc7	Glassner
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Stargate	Stargat	k1gInSc5	Stargat
Atlantis	Atlantis	k1gFnSc1	Atlantis
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Robertem	Robert	k1gMnSc7	Robert
C.	C.	kA	C.
Cooperem	Cooper	k1gMnSc7	Cooper
<g/>
)	)	kIx)	)
a	a	k8xC	a
Stargate	Stargat	k1gInSc5	Stargat
Universe	Universe	k1gFnPc4	Universe
(	(	kIx(	(
<g/>
také	také	k9	také
s	s	k7c7	s
Cooperem	Coopero	k1gNnSc7	Coopero
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
<g/>
,	,	kIx,	,
Ontario	Ontario	k1gNnSc1	Ontario
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
.	.	kIx.	.
</s>
