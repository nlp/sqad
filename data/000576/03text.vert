<s>
Brad	brada	k1gFnPc2	brada
Wright	Wright	k1gMnSc1	Wright
je	být	k5eAaImIp3nS	být
Kanadský	kanadský	k2eAgMnSc1d1	kanadský
televizní	televizní	k2eAgMnSc1d1	televizní
producent	producent	k1gMnSc1	producent
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
a	a	k8xC	a
herec	herec	k1gMnSc1	herec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
známým	známý	k2eAgMnSc7d1	známý
tvůrcem	tvůrce	k1gMnSc7	tvůrce
nebo	nebo	k8xC	nebo
spolutvůrcem	spolutvůrce	k1gMnSc7	spolutvůrce
televizního	televizní	k2eAgInSc2d1	televizní
seriálu	seriál	k1gInSc2	seriál
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jonathanem	Jonathan	k1gMnSc7	Jonathan
Glassnerem	Glassner	k1gMnSc7	Glassner
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Stargate	Stargat	k1gInSc5	Stargat
Atlantis	Atlantis	k1gFnSc1	Atlantis
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Robertem	Robert	k1gMnSc7	Robert
C.	C.	kA	C.
Cooperem	Cooper	k1gMnSc7	Cooper
<g/>
)	)	kIx)	)
a	a	k8xC	a
Stargate	Stargat	k1gInSc5	Stargat
Universe	Universe	k1gFnPc4	Universe
(	(	kIx(	(
<g/>
také	také	k9	také
s	s	k7c7	s
Cooperem	Coopero	k1gNnSc7	Coopero
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Torontu	Toronto	k1gNnSc6	Toronto
<g/>
,	,	kIx,	,
Ontario	Ontario	k1gNnSc1	Ontario
<g/>
,	,	kIx,	,
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
vznikem	vznik	k1gInSc7	vznik
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
brány	brána	k1gFnSc2	brána
<g/>
,	,	kIx,	,
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
výkonný	výkonný	k2eAgMnSc1d1	výkonný
producent	producent	k1gMnSc1	producent
a	a	k8xC	a
scenárista	scenárista	k1gMnSc1	scenárista
seriálu	seriál	k1gInSc2	seriál
Krajní	krajní	k2eAgFnSc2d1	krajní
meze	mez	k1gFnSc2	mez
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Psal	psát	k5eAaImAgInS	psát
také	také	k9	také
scénáře	scénář	k1gInPc4	scénář
pro	pro	k7c4	pro
několik	několik	k4yIc4	několik
dalších	další	k2eAgInPc2d1	další
televizních	televizní	k2eAgInPc2d1	televizní
seriálů	seriál	k1gInPc2	seriál
jako	jako	k8xS	jako
Neon	neon	k1gInSc1	neon
Rider	Ridra	k1gFnPc2	Ridra
<g/>
,	,	kIx,	,
Dobrodružství	dobrodružství	k1gNnSc2	dobrodružství
černého	černý	k2eAgMnSc4d1	černý
hřebce	hřebec	k1gMnSc4	hřebec
<g/>
,	,	kIx,	,
Odysea	odysea	k1gFnSc1	odysea
<g/>
,	,	kIx,	,
Highlander	Highlander	k1gInSc1	Highlander
a	a	k8xC	a
Poltergeist	Poltergeist	k1gInSc1	Poltergeist
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Brad	brada	k1gFnPc2	brada
Wright	Wrightum	k1gNnPc2	Wrightum
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
