<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Německa	Německo	k1gNnSc2	Německo
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
vodorovných	vodorovný	k2eAgInPc2d1	vodorovný
pruhů	pruh	k1gInPc2	pruh
barev	barva	k1gFnPc2	barva
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
červená	červený	k2eAgFnSc1d1	červená
a	a	k8xC	a
zlatá	zlatý	k2eAgFnSc1d1	zlatá
<g/>
,	,	kIx,	,
poměr	poměr	k1gInSc1	poměr
délek	délka	k1gFnPc2	délka
stran	strana	k1gFnPc2	strana
je	být	k5eAaImIp3nS	být
5	[number]	k4	5
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
