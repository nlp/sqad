<s>
Z	z	k7c2	z
psychologického	psychologický	k2eAgInSc2d1	psychologický
pohledu	pohled	k1gInSc2	pohled
je	být	k5eAaImIp3nS	být
láska	láska	k1gFnSc1	láska
primární	primární	k2eAgFnSc1d1	primární
lidskou	lidský	k2eAgFnSc7d1	lidská
potřebou	potřeba	k1gFnSc7	potřeba
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
také	také	k9	také
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejrozšířenějších	rozšířený	k2eAgInPc2d3	nejrozšířenější
námětů	námět	k1gInPc2	námět
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
–	–	k?	–
v	v	k7c6	v
knihách	kniha	k1gFnPc6	kniha
<g/>
,	,	kIx,	,
filmech	film	k1gInPc6	film
<g/>
,	,	kIx,	,
divadelních	divadelní	k2eAgFnPc6d1	divadelní
hrách	hra	k1gFnPc6	hra
i	i	k8xC	i
v	v	k7c6	v
písních	píseň	k1gFnPc6	píseň
různých	různý	k2eAgInPc2d1	různý
žánrů	žánr	k1gInPc2	žánr
<g/>
.	.	kIx.	.
</s>
