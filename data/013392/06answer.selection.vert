<s>
Mateřídouška	mateřídouška	k1gFnSc1	mateřídouška
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
hluchavkovité	hluchavkovitý	k2eAgInPc4d1	hluchavkovitý
typické	typický	k2eAgInPc4d1	typický
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
množství	množství	k1gNnSc1	množství
silic	silice	k1gFnPc2	silice
(	(	kIx(	(
<g/>
thymol	thymol	k1gInSc1	thymol
<g/>
,	,	kIx,	,
cymol	cymol	k1gInSc1	cymol
<g/>
,	,	kIx,	,
karvakrol	karvakrol	k1gInSc1	karvakrol
<g/>
,	,	kIx,	,
linalol	linalol	k1gInSc1	linalol
<g/>
,	,	kIx,	,
terpineol	terpineol	k1gInSc1	terpineol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kyselinu	kyselina	k1gFnSc4	kyselina
ursolovou	ursolový	k2eAgFnSc4d1	ursolová
<g/>
,	,	kIx,	,
flavonoidy	flavonoid	k1gInPc1	flavonoid
<g/>
,	,	kIx,	,
flavony	flavon	k1gInPc1	flavon
<g/>
,	,	kIx,	,
karvanol	karvanol	k1gInSc1	karvanol
<g/>
,	,	kIx,	,
třísloviny	tříslovina	k1gFnPc1	tříslovina
a	a	k8xC	a
hořčiny	hořčina	k1gFnPc1	hořčina
<g/>
.	.	kIx.	.
</s>
