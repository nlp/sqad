<p>
<s>
Patologie	patologie	k1gFnSc1	patologie
je	být	k5eAaImIp3nS	být
lékařská	lékařský	k2eAgFnSc1d1	lékařská
věda	věda	k1gFnSc1	věda
zabývající	zabývající	k2eAgFnSc1d1	zabývající
se	s	k7c7	s
studiem	studio	k1gNnSc7	studio
a	a	k8xC	a
diagnostikou	diagnostika	k1gFnSc7	diagnostika
nemocí	nemoc	k1gFnPc2	nemoc
živých	živý	k2eAgInPc2d1	živý
systémů	systém	k1gInPc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
slova	slovo	k1gNnSc2	slovo
pathos	pathos	k1gInSc1	pathos
(	(	kIx(	(
<g/>
π	π	k?	π
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
zážitek	zážitek	k1gInSc1	zážitek
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
muka	muka	k1gFnSc1	muka
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
a	a	k8xC	a
-logia	ogia	k1gFnSc1	-logia
(	(	kIx(	(
<g/>
-λ	-λ	k?	-λ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
výčet	výčet	k1gInSc4	výčet
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
oblastech	oblast	k1gFnPc6	oblast
zájmu	zájem	k1gInSc2	zájem
nehrozí	hrozit	k5eNaImIp3nS	hrozit
riziko	riziko	k1gNnSc4	riziko
záměny	záměna	k1gFnSc2	záměna
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
pojem	pojem	k1gInSc1	pojem
patologie	patologie	k1gFnSc2	patologie
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
oborech	obor	k1gInPc6	obor
i	i	k8xC	i
jako	jako	k9	jako
synonymum	synonymum	k1gNnSc1	synonymum
pro	pro	k7c4	pro
specializovanější	specializovaný	k2eAgNnSc4d2	specializovanější
pole	pole	k1gNnSc4	pole
působnosti	působnost	k1gFnSc2	působnost
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
lékařský	lékařský	k2eAgInSc1d1	lékařský
obor	obor	k1gInSc1	obor
patologická	patologický	k2eAgFnSc1d1	patologická
anatomie	anatomie	k1gFnSc1	anatomie
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
mezi	mezi	k7c7	mezi
zdravotníky	zdravotník	k1gMnPc7	zdravotník
nazývá	nazývat	k5eAaImIp3nS	nazývat
patologie	patologie	k1gFnSc1	patologie
<g/>
.	.	kIx.	.
</s>
<s>
Plurál	plurál	k1gInSc1	plurál
pojmu	pojem	k1gInSc2	pojem
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
progrese	progrese	k1gFnSc2	progrese
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
,	,	kIx,	,
např.	např.	kA	např.
"	"	kIx"	"
<g/>
tato	tento	k3xDgFnSc1	tento
nákaza	nákaza	k1gFnSc1	nákaza
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
tyto	tento	k3xDgFnPc4	tento
patologie	patologie	k1gFnPc4	patologie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Přídavné	přídavný	k2eAgNnSc1d1	přídavné
jméno	jméno	k1gNnSc1	jméno
patologický	patologický	k2eAgMnSc1d1	patologický
se	se	k3xPyFc4	se
poměrně	poměrně	k6eAd1	poměrně
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
nezdravý	zdravý	k2eNgInSc4d1	nezdravý
<g/>
,	,	kIx,	,
porušený	porušený	k2eAgInSc4d1	porušený
nebo	nebo	k8xC	nebo
odchylný	odchylný	k2eAgInSc4d1	odchylný
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
mimo	mimo	k7c4	mimo
medicínský	medicínský	k2eAgInSc4d1	medicínský
kontext	kontext	k1gInSc4	kontext
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Weierstrassova	Weierstrassův	k2eAgFnSc1d1	Weierstrassova
funkce	funkce	k1gFnSc1	funkce
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
příklad	příklad	k1gInSc1	příklad
patologické	patologický	k2eAgFnSc2d1	patologická
funkce	funkce	k1gFnSc2	funkce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Affix	Affix	k1gInSc1	Affix
pojmu	pojem	k1gInSc2	pojem
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
-patie	atie	k1gFnSc1	-patie
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
pro	pro	k7c4	pro
pojmenování	pojmenování	k1gNnSc4	pojmenování
stavu	stav	k1gInSc2	stav
indispozice	indispozice	k1gFnSc2	indispozice
<g/>
,	,	kIx,	,
např.	např.	kA	např.
psychopatie	psychopatie	k1gFnSc1	psychopatie
<g/>
.	.	kIx.	.
</s>
<s>
Lékař	lékař	k1gMnSc1	lékař
se	s	k7c7	s
specializací	specializace	k1gFnSc7	specializace
patologie	patologie	k1gFnSc2	patologie
se	se	k3xPyFc4	se
na	na	k7c6	na
nazývá	nazývat	k5eAaImIp3nS	nazývat
patolog	patolog	k1gMnSc1	patolog
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
patologický	patologický	k2eAgInSc4d1	patologický
stav	stav	k1gInSc4	stav
se	se	k3xPyFc4	se
považuje	považovat	k5eAaImIp3nS	považovat
stav	stav	k1gInSc4	stav
způsobený	způsobený	k2eAgInSc4d1	způsobený
nemocí	nemoc	k1gFnSc7	nemoc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
předmětem	předmět	k1gInSc7	předmět
patologie	patologie	k1gFnSc2	patologie
je	být	k5eAaImIp3nS	být
výzkum	výzkum	k1gInSc1	výzkum
čtyř	čtyři	k4xCgFnPc2	čtyři
složek	složka	k1gFnPc2	složka
nemoci	nemoc	k1gFnSc2	nemoc
<g/>
:	:	kIx,	:
příčiny	příčina	k1gFnPc1	příčina
(	(	kIx(	(
<g/>
etiologie	etiologie	k1gFnPc1	etiologie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mechanismus	mechanismus	k1gInSc1	mechanismus
vývoje	vývoj	k1gInSc2	vývoj
nemoci	nemoc	k1gFnSc2	nemoc
(	(	kIx(	(
<g/>
patogeneze	patogeneze	k1gFnSc1	patogeneze
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
strukturální	strukturální	k2eAgFnPc4d1	strukturální
změny	změna	k1gFnPc4	změna
buněk	buňka	k1gFnPc2	buňka
(	(	kIx(	(
<g/>
morfologie	morfologie	k1gFnSc1	morfologie
<g/>
)	)	kIx)	)
a	a	k8xC	a
důsledky	důsledek	k1gInPc1	důsledek
těchto	tento	k3xDgFnPc2	tento
změn	změna	k1gFnPc2	změna
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
klinických	klinický	k2eAgInPc2d1	klinický
projevů	projev	k1gInPc2	projev
<g/>
.	.	kIx.	.
</s>
<s>
Obecná	obecný	k2eAgFnSc1d1	obecná
patologie	patologie	k1gFnSc1	patologie
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
analýzou	analýza	k1gFnSc7	analýza
klinických	klinický	k2eAgFnPc2d1	klinická
abnormalit	abnormalita	k1gFnPc2	abnormalita
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
znaky	znak	k1gInPc4	znak
a	a	k8xC	a
prekurzory	prekurzora	k1gFnPc4	prekurzora
infekčních	infekční	k2eAgNnPc2d1	infekční
a	a	k8xC	a
neinfekčních	infekční	k2eNgNnPc2d1	neinfekční
onemocnění	onemocnění	k1gNnPc2	onemocnění
a	a	k8xC	a
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
se	se	k3xPyFc4	se
na	na	k7c4	na
klinickou	klinický	k2eAgFnSc4d1	klinická
a	a	k8xC	a
anatomickou	anatomický	k2eAgFnSc4d1	anatomická
patologii	patologie	k1gFnSc4	patologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
patologie	patologie	k1gFnSc2	patologie
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Rozdělení	rozdělení	k1gNnSc1	rozdělení
patologie	patologie	k1gFnSc2	patologie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Podle	podle	k7c2	podle
studovaných	studovaný	k2eAgInPc2d1	studovaný
systémů	systém	k1gInPc2	systém
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Humánní	humánní	k2eAgFnSc2d1	humánní
patologie	patologie	k1gFnSc2	patologie
====	====	k?	====
</s>
</p>
<p>
<s>
Humánní	humánní	k2eAgFnSc1d1	humánní
(	(	kIx(	(
<g/>
lidská	lidský	k2eAgFnSc1d1	lidská
<g/>
)	)	kIx)	)
patologie	patologie	k1gFnSc1	patologie
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
studiem	studio	k1gNnSc7	studio
a	a	k8xC	a
diagnostikou	diagnostika	k1gFnSc7	diagnostika
somatických	somatický	k2eAgNnPc2d1	somatické
onemocnění	onemocnění	k1gNnPc2	onemocnění
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
představována	představovat	k5eAaImNgFnS	představovat
lékařským	lékařský	k2eAgInSc7d1	lékařský
oborem	obor	k1gInSc7	obor
Patologická	patologický	k2eAgFnSc1d1	patologická
anatomie	anatomie	k1gFnSc1	anatomie
<g/>
,	,	kIx,	,
výkon	výkon	k1gInSc1	výkon
této	tento	k3xDgFnSc2	tento
profese	profes	k1gFnSc2	profes
je	být	k5eAaImIp3nS	být
vyhrazen	vyhradit	k5eAaPmNgInS	vyhradit
lékařům	lékař	k1gMnPc3	lékař
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
se	se	k3xPyFc4	se
jako	jako	k9	jako
relativně	relativně	k6eAd1	relativně
samostatný	samostatný	k2eAgInSc1d1	samostatný
obor	obor	k1gInSc1	obor
vyděluje	vydělovat	k5eAaImIp3nS	vydělovat
klinická	klinický	k2eAgFnSc1d1	klinická
patologie	patologie	k1gFnSc1	patologie
<g/>
.	.	kIx.	.
</s>
<s>
Čistě	čistě	k6eAd1	čistě
vědecký	vědecký	k2eAgInSc4d1	vědecký
směr	směr	k1gInSc4	směr
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
tradičně	tradičně	k6eAd1	tradičně
nazýval	nazývat	k5eAaImAgMnS	nazývat
experimentální	experimentální	k2eAgFnSc2d1	experimentální
patologie	patologie	k1gFnSc2	patologie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
nazývá	nazývat	k5eAaImIp3nS	nazývat
patologická	patologický	k2eAgFnSc1d1	patologická
fyziologie	fyziologie	k1gFnSc1	fyziologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Veterinární	veterinární	k2eAgFnSc2d1	veterinární
patologie	patologie	k1gFnSc2	patologie
====	====	k?	====
</s>
</p>
<p>
<s>
Veterinární	veterinární	k2eAgFnSc1d1	veterinární
patologie	patologie	k1gFnSc1	patologie
je	být	k5eAaImIp3nS	být
oborem	obor	k1gInSc7	obor
veterinárního	veterinární	k2eAgNnSc2d1	veterinární
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
studium	studium	k1gNnSc1	studium
a	a	k8xC	a
diagnostika	diagnostika	k1gFnSc1	diagnostika
onemocnění	onemocnění	k1gNnSc2	onemocnění
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
především	především	k9	především
pak	pak	k9	pak
zvířat	zvíře	k1gNnPc2	zvíře
domácích	domácí	k1gFnPc2	domácí
a	a	k8xC	a
zvířat	zvíře	k1gNnPc2	zvíře
hospodářsky	hospodářsky	k6eAd1	hospodářsky
významných	významný	k2eAgMnPc2d1	významný
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInPc1	její
prostředky	prostředek	k1gInPc1	prostředek
jsou	být	k5eAaImIp3nP	být
podobné	podobný	k2eAgInPc1d1	podobný
prostředkům	prostředek	k1gInPc3	prostředek
humánní	humánní	k2eAgFnSc2d1	humánní
patologie	patologie	k1gFnSc2	patologie
<g/>
,	,	kIx,	,
výkon	výkon	k1gInSc4	výkon
profese	profes	k1gFnSc2	profes
je	být	k5eAaImIp3nS	být
však	však	k9	však
vyhrazen	vyhradit	k5eAaPmNgInS	vyhradit
veterinárním	veterinární	k2eAgMnSc7d1	veterinární
lékařům	lékař	k1gMnPc3	lékař
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Fytopatologie	fytopatologie	k1gFnSc2	fytopatologie
====	====	k?	====
</s>
</p>
<p>
<s>
Fytopatologie	fytopatologie	k1gFnSc1	fytopatologie
je	být	k5eAaImIp3nS	být
oborem	obor	k1gInSc7	obor
agronomie	agronomie	k1gFnSc2	agronomie
zabývajícím	zabývající	k2eAgMnSc7d1	zabývající
se	se	k3xPyFc4	se
studiem	studio	k1gNnSc7	studio
a	a	k8xC	a
diagnostikou	diagnostika	k1gFnSc7	diagnostika
onemocnění	onemocnění	k1gNnSc2	onemocnění
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgInSc1d1	zvláštní
důraz	důraz	k1gInSc1	důraz
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
kladen	klást	k5eAaImNgInS	klást
na	na	k7c4	na
hospodářsky	hospodářsky	k6eAd1	hospodářsky
významné	významný	k2eAgInPc4d1	významný
druhy	druh	k1gInPc4	druh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Psychopatologie	psychopatologie	k1gFnSc2	psychopatologie
====	====	k?	====
</s>
</p>
<p>
<s>
Psychopatologie	psychopatologie	k1gFnSc1	psychopatologie
je	být	k5eAaImIp3nS	být
oborem	obor	k1gInSc7	obor
psychologie	psychologie	k1gFnSc2	psychologie
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
studiem	studio	k1gNnSc7	studio
duševních	duševní	k2eAgFnPc2d1	duševní
poruch	porucha	k1gFnPc2	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
blízký	blízký	k2eAgInSc1d1	blízký
vztah	vztah	k1gInSc1	vztah
má	mít	k5eAaImIp3nS	mít
k	k	k7c3	k
psychiatrii	psychiatrie	k1gFnSc3	psychiatrie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Sociální	sociální	k2eAgFnSc1d1	sociální
patologie	patologie	k1gFnSc1	patologie
====	====	k?	====
</s>
</p>
<p>
<s>
Sociální	sociální	k2eAgFnSc1d1	sociální
patologie	patologie	k1gFnSc1	patologie
je	být	k5eAaImIp3nS	být
společenskovědní	společenskovědní	k2eAgInSc4d1	společenskovědní
obor	obor	k1gInSc4	obor
studující	studující	k1gFnSc4	studující
společensky	společensky	k6eAd1	společensky
nežádoucí	žádoucí	k2eNgInPc4d1	nežádoucí
jevy	jev	k1gInPc4	jev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Paleopatologie	Paleopatologie	k1gFnSc2	Paleopatologie
====	====	k?	====
</s>
</p>
<p>
<s>
Paleopatologie	Paleopatologie	k1gFnSc1	Paleopatologie
je	být	k5eAaImIp3nS	být
obor	obor	k1gInSc4	obor
patologie	patologie	k1gFnSc2	patologie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
studiem	studio	k1gNnSc7	studio
chorobných	chorobný	k2eAgFnPc2d1	chorobná
změn	změna	k1gFnPc2	změna
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
řazena	řadit	k5eAaImNgFnS	řadit
mezi	mezi	k7c4	mezi
pomocné	pomocný	k2eAgFnPc4d1	pomocná
vědy	věda	k1gFnPc4	věda
historické	historický	k2eAgFnPc4d1	historická
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
vedle	vedle	k7c2	vedle
historie	historie	k1gFnSc2	historie
má	mít	k5eAaImIp3nS	mít
řadu	řada	k1gFnSc4	řada
průniků	průnik	k1gInPc2	průnik
i	i	k8xC	i
s	s	k7c7	s
archeologií	archeologie	k1gFnSc7	archeologie
a	a	k8xC	a
paleontologií	paleontologie	k1gFnSc7	paleontologie
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
pro	pro	k7c4	pro
potřeby	potřeba	k1gFnPc4	potřeba
historického	historický	k2eAgInSc2d1	historický
výzkumu	výzkum	k1gInSc2	výzkum
úzce	úzko	k6eAd1	úzko
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
oborem	obor	k1gInSc7	obor
dějiny	dějiny	k1gFnPc4	dějiny
lékařství	lékařství	k1gNnSc2	lékařství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Podle	podle	k7c2	podle
míry	míra	k1gFnSc2	míra
obecnosti	obecnost	k1gFnSc2	obecnost
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Obecná	obecný	k2eAgFnSc1d1	obecná
patologie	patologie	k1gFnSc1	patologie
====	====	k?	====
</s>
</p>
<p>
<s>
Obecná	obecný	k2eAgFnSc1d1	obecná
patologie	patologie	k1gFnSc1	patologie
je	být	k5eAaImIp3nS	být
vědecký	vědecký	k2eAgInSc4d1	vědecký
obor	obor	k1gInSc4	obor
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
studovat	studovat	k5eAaImF	studovat
obecné	obecný	k2eAgInPc4d1	obecný
mechanizmy	mechanizmus	k1gInPc4	mechanizmus
vzniku	vznik	k1gInSc2	vznik
poškození	poškození	k1gNnSc4	poškození
živého	živý	k2eAgInSc2d1	živý
systému	systém	k1gInSc2	systém
a	a	k8xC	a
reakce	reakce	k1gFnSc2	reakce
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
poškození	poškození	k1gNnSc1	poškození
je	být	k5eAaImIp3nS	být
studováno	studovat	k5eAaImNgNnS	studovat
od	od	k7c2	od
úrovně	úroveň	k1gFnSc2	úroveň
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
molekul	molekula	k1gFnPc2	molekula
až	až	k9	až
do	do	k7c2	do
úrovně	úroveň	k1gFnSc2	úroveň
celých	celý	k2eAgInPc2d1	celý
organizmů	organizmus	k1gInPc2	organizmus
<g/>
.	.	kIx.	.
</s>
<s>
Obecná	obecný	k2eAgFnSc1d1	obecná
patologie	patologie	k1gFnSc1	patologie
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stýká	stýkat	k5eAaImIp3nS	stýkat
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
řadou	řada	k1gFnSc7	řada
dalších	další	k2eAgInPc2d1	další
oborů	obor	k1gInPc2	obor
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
obecná	obecný	k2eAgFnSc1d1	obecná
humánní	humánní	k2eAgFnSc1d1	humánní
patologie	patologie	k1gFnSc1	patologie
se	se	k3xPyFc4	se
stýká	stýkat	k5eAaImIp3nS	stýkat
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
s	s	k7c7	s
biofyzikou	biofyzika	k1gFnSc7	biofyzika
<g/>
,	,	kIx,	,
molekulární	molekulární	k2eAgFnSc7d1	molekulární
biologií	biologie	k1gFnSc7	biologie
<g/>
,	,	kIx,	,
Bioinformatikou	Bioinformatika	k1gFnSc7	Bioinformatika
<g/>
,	,	kIx,	,
mikrobiologií	mikrobiologie	k1gFnSc7	mikrobiologie
a	a	k8xC	a
toxikologií	toxikologie	k1gFnSc7	toxikologie
a	a	k8xC	a
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
s	s	k7c7	s
klinickými	klinický	k2eAgInPc7d1	klinický
lékařskými	lékařský	k2eAgInPc7d1	lékařský
obory	obor	k1gInPc7	obor
a	a	k8xC	a
antropologií	antropologie	k1gFnSc7	antropologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poznatky	poznatek	k1gInPc1	poznatek
obecné	obecný	k2eAgFnSc2d1	obecná
patologie	patologie	k1gFnSc2	patologie
přestavují	přestavovat	k5eAaImIp3nP	přestavovat
základní	základní	k2eAgFnPc4d1	základní
informace	informace	k1gFnPc4	informace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
porozumět	porozumět	k5eAaPmF	porozumět
chorobným	chorobný	k2eAgFnPc3d1	chorobná
změnám	změna	k1gFnPc3	změna
a	a	k8xC	a
tak	tak	k6eAd1	tak
příslušné	příslušný	k2eAgFnPc4d1	příslušná
choroby	choroba	k1gFnPc4	choroba
cíleně	cíleně	k6eAd1	cíleně
diagnostikovat	diagnostikovat	k5eAaBmF	diagnostikovat
a	a	k8xC	a
léčit	léčit	k5eAaImF	léčit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Speciální	speciální	k2eAgFnSc1d1	speciální
patologie	patologie	k1gFnSc1	patologie
====	====	k?	====
</s>
</p>
<p>
<s>
Speciální	speciální	k2eAgFnSc1d1	speciální
patologie	patologie	k1gFnSc1	patologie
je	být	k5eAaImIp3nS	být
vědecký	vědecký	k2eAgInSc4d1	vědecký
a	a	k8xC	a
aplikovaný	aplikovaný	k2eAgInSc4d1	aplikovaný
obor	obor	k1gInSc4	obor
zabývající	zabývající	k2eAgInSc4d1	zabývající
se	s	k7c7	s
studiem	studio	k1gNnSc7	studio
a	a	k8xC	a
diagnostikou	diagnostika	k1gFnSc7	diagnostika
konkrétních	konkrétní	k2eAgFnPc2d1	konkrétní
chorobných	chorobný	k2eAgFnPc2d1	chorobná
změn	změna	k1gFnPc2	změna
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
samotného	samotný	k2eAgNnSc2d1	samotné
rozpoznání	rozpoznání	k1gNnSc2	rozpoznání
choroby	choroba	k1gFnSc2	choroba
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
stanovení	stanovení	k1gNnSc1	stanovení
diagnózy	diagnóza	k1gFnSc2	diagnóza
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
speciální	speciální	k2eAgFnSc1d1	speciální
patologie	patologie	k1gFnSc1	patologie
zabývá	zabývat	k5eAaImIp3nS	zabývat
i	i	k9	i
dalšími	další	k2eAgInPc7d1	další
projevy	projev	k1gInPc7	projev
poruchy	porucha	k1gFnSc2	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Etiologie	etiologie	k1gFnSc1	etiologie
znamená	znamenat	k5eAaImIp3nS	znamenat
nalezení	nalezení	k1gNnSc4	nalezení
příčiny	příčina	k1gFnSc2	příčina
vyvolávající	vyvolávající	k2eAgFnSc4d1	vyvolávající
příslušnou	příslušný	k2eAgFnSc4d1	příslušná
poruchu	porucha	k1gFnSc4	porucha
organizmu	organizmus	k1gInSc2	organizmus
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
noxy	noxa	k1gFnSc2	noxa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Noxa	noxa	k1gFnSc1	noxa
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
známá	známý	k2eAgFnSc1d1	známá
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
identifikovaná	identifikovaný	k2eAgFnSc1d1	identifikovaná
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Mycobacterium	Mycobacterium	k1gNnSc4	Mycobacterium
tuberculosis	tuberculosis	k1gInSc1	tuberculosis
jako	jako	k9	jako
původce	původce	k1gMnSc4	původce
tuberkulózy	tuberkulóza	k1gFnSc2	tuberkulóza
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
známy	znám	k2eAgInPc1d1	znám
pouze	pouze	k6eAd1	pouze
rizikové	rizikový	k2eAgInPc1d1	rizikový
faktory	faktor	k1gInPc1	faktor
a	a	k8xC	a
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
případech	případ	k1gInPc6	případ
není	být	k5eNaImIp3nS	být
původce	původce	k1gMnSc1	původce
nemoci	nemoc	k1gFnSc2	nemoc
známý	známý	k2eAgMnSc1d1	známý
-	-	kIx~	-
potom	potom	k6eAd1	potom
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
daném	daný	k2eAgNnSc6d1	dané
onemocnění	onemocnění	k1gNnSc6	onemocnění
jako	jako	k8xS	jako
o	o	k7c6	o
onemocněním	onemocnění	k1gNnPc3	onemocnění
idiopatickém	idiopatický	k2eAgMnSc6d1	idiopatický
nebo	nebo	k8xC	nebo
primárním	primární	k2eAgMnSc6d1	primární
<g/>
.	.	kIx.	.
</s>
<s>
Patogeneze	patogeneze	k1gFnSc1	patogeneze
je	být	k5eAaImIp3nS	být
studium	studium	k1gNnSc4	studium
rozvoje	rozvoj	k1gInSc2	rozvoj
chorobných	chorobný	k2eAgFnPc2d1	chorobná
změn	změna	k1gFnPc2	změna
v	v	k7c6	v
organizmu	organizmus	k1gInSc6	organizmus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ideálním	ideální	k2eAgInSc6d1	ideální
případě	případ	k1gInSc6	případ
jsou	být	k5eAaImIp3nP	být
dokonale	dokonale	k6eAd1	dokonale
prozkoumány	prozkoumán	k2eAgInPc4d1	prozkoumán
molekulární	molekulární	k2eAgInPc4d1	molekulární
mechanizmy	mechanizmus	k1gInPc4	mechanizmus
rozvoje	rozvoj	k1gInSc2	rozvoj
onemocnění	onemocnění	k1gNnPc2	onemocnění
a	a	k8xC	a
hovoří	hovořit	k5eAaImIp3nS	hovořit
se	se	k3xPyFc4	se
o	o	k7c6	o
kauzální	kauzální	k2eAgFnSc6d1	kauzální
patogenezi	patogeneze	k1gFnSc6	patogeneze
<g/>
.	.	kIx.	.
</s>
<s>
Jindy	jindy	k6eAd1	jindy
jsou	být	k5eAaImIp3nP	být
známy	znám	k2eAgInPc1d1	znám
jen	jen	k9	jen
popisy	popis	k1gInPc1	popis
nebo	nebo	k8xC	nebo
obecné	obecný	k2eAgInPc1d1	obecný
principy	princip	k1gInPc1	princip
změn	změna	k1gFnPc2	změna
a	a	k8xC	a
pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
hovoří	hovořit	k5eAaImIp3nS	hovořit
o	o	k7c6	o
formální	formální	k2eAgFnSc6d1	formální
patogenezi	patogeneze	k1gFnSc6	patogeneze
<g/>
.	.	kIx.	.
</s>
<s>
Etiopatogeneze	Etiopatogeneze	k1gFnSc1	Etiopatogeneze
je	být	k5eAaImIp3nS	být
spojením	spojení	k1gNnSc7	spojení
etiologie	etiologie	k1gFnSc2	etiologie
a	a	k8xC	a
patogeneze	patogeneze	k1gFnSc2	patogeneze
v	v	k7c4	v
jediný	jediný	k2eAgInSc4d1	jediný
logicky	logicky	k6eAd1	logicky
koherentní	koherentní	k2eAgInSc4d1	koherentní
řetězec	řetězec	k1gInSc4	řetězec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
vývoj	vývoj	k1gInSc4	vývoj
choroby	choroba	k1gFnSc2	choroba
od	od	k7c2	od
okamžiku	okamžik	k1gInSc2	okamžik
prvního	první	k4xOgInSc2	první
kontaktu	kontakt	k1gInSc2	kontakt
s	s	k7c7	s
noxou	noxa	k1gFnSc7	noxa
až	až	k9	až
po	po	k7c4	po
vznik	vznik	k1gInSc4	vznik
manifestní	manifestní	k2eAgFnSc2d1	manifestní
choroby	choroba	k1gFnSc2	choroba
a	a	k8xC	a
případně	případně	k6eAd1	případně
i	i	k9	i
další	další	k2eAgInSc4d1	další
vývoj	vývoj	k1gInSc4	vývoj
choroby	choroba	k1gFnSc2	choroba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Podle	podle	k7c2	podle
škály	škála	k1gFnSc2	škála
studovaných	studovaný	k2eAgFnPc2d1	studovaná
změn	změna	k1gFnPc2	změna
===	===	k?	===
</s>
</p>
<p>
<s>
Rozdělení	rozdělení	k1gNnSc1	rozdělení
patologie	patologie	k1gFnSc2	patologie
podle	podle	k7c2	podle
škály	škála	k1gFnSc2	škála
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
neostré	ostrý	k2eNgNnSc1d1	neostré
<g/>
,	,	kIx,	,
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
úrovně	úroveň	k1gFnPc1	úroveň
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
a	a	k8xC	a
prolínají	prolínat	k5eAaImIp3nP	prolínat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Molekulární	molekulární	k2eAgFnSc1d1	molekulární
patologie	patologie	k1gFnSc1	patologie
====	====	k?	====
</s>
</p>
<p>
<s>
Molekulární	molekulární	k2eAgFnSc1d1	molekulární
patologie	patologie	k1gFnSc1	patologie
je	být	k5eAaImIp3nS	být
obor	obor	k1gInSc4	obor
studující	studující	k2eAgFnSc2d1	studující
chorobné	chorobný	k2eAgFnSc2d1	chorobná
změny	změna	k1gFnSc2	změna
na	na	k7c6	na
nejnižší	nízký	k2eAgFnSc6d3	nejnižší
biologicky	biologicky	k6eAd1	biologicky
relevantní	relevantní	k2eAgFnSc6d1	relevantní
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
především	především	k9	především
studiem	studio	k1gNnSc7	studio
změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
molekul	molekula	k1gFnPc2	molekula
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
změny	změna	k1gFnPc4	změna
DNA	DNA	kA	DNA
<g/>
,	,	kIx,	,
změny	změna	k1gFnPc1	změna
aktivity	aktivita	k1gFnSc2	aktivita
enzymů	enzym	k1gInPc2	enzym
nebo	nebo	k8xC	nebo
působení	působení	k1gNnSc2	působení
cizorodých	cizorodý	k2eAgFnPc2d1	cizorodá
molekul	molekula	k1gFnPc2	molekula
a	a	k8xC	a
atomů	atom	k1gInPc2	atom
<g/>
.	.	kIx.	.
</s>
<s>
Molekulární	molekulární	k2eAgFnSc1d1	molekulární
patologie	patologie	k1gFnSc1	patologie
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stýká	stýkat	k5eAaImIp3nS	stýkat
především	především	k9	především
s	s	k7c7	s
chemickými	chemický	k2eAgInPc7d1	chemický
obory	obor	k1gInPc7	obor
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
s	s	k7c7	s
biofyzikou	biofyzika	k1gFnSc7	biofyzika
<g/>
,	,	kIx,	,
bioinformatikou	bioinformatika	k1gFnSc7	bioinformatika
a	a	k8xC	a
molekulární	molekulární	k2eAgFnSc7d1	molekulární
biologií	biologie	k1gFnSc7	biologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Buněčná	buněčný	k2eAgFnSc1d1	buněčná
patologie	patologie	k1gFnSc1	patologie
====	====	k?	====
</s>
</p>
<p>
<s>
Buněčná	buněčný	k2eAgFnSc1d1	buněčná
patologie	patologie	k1gFnSc1	patologie
je	být	k5eAaImIp3nS	být
obor	obor	k1gInSc4	obor
studující	studující	k2eAgFnSc2d1	studující
chorobné	chorobný	k2eAgFnSc2d1	chorobná
změny	změna	k1gFnSc2	změna
probíhající	probíhající	k2eAgFnSc2d1	probíhající
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
organel	organela	k1gFnPc2	organela
a	a	k8xC	a
celých	celý	k2eAgFnPc2d1	celá
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
výzkumné	výzkumný	k2eAgFnSc6d1	výzkumná
části	část	k1gFnSc6	část
je	být	k5eAaImIp3nS	být
podoborem	podobor	k1gInSc7	podobor
buněčné	buněčný	k2eAgFnSc2d1	buněčná
biologie	biologie	k1gFnSc2	biologie
<g/>
.	.	kIx.	.
</s>
<s>
Klinický	klinický	k2eAgInSc1d1	klinický
obor	obor	k1gInSc1	obor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
využitím	využití	k1gNnSc7	využití
buněčných	buněčný	k2eAgFnPc2d1	buněčná
změn	změna	k1gFnPc2	změna
k	k	k7c3	k
detekci	detekce	k1gFnSc3	detekce
a	a	k8xC	a
diagnostice	diagnostika	k1gFnSc3	diagnostika
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
cytologie	cytologie	k1gFnSc1	cytologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Histopatologie	Histopatologie	k1gFnSc2	Histopatologie
====	====	k?	====
</s>
</p>
<p>
<s>
Histopatologie	Histopatologie	k1gFnSc1	Histopatologie
je	být	k5eAaImIp3nS	být
obor	obor	k1gInSc4	obor
studující	studující	k2eAgFnSc2d1	studující
chorobné	chorobný	k2eAgFnSc2d1	chorobná
změny	změna	k1gFnSc2	změna
probíhající	probíhající	k2eAgFnSc2d1	probíhající
ve	v	k7c6	v
tkáních	tkáň	k1gFnPc6	tkáň
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
na	na	k7c6	na
mikroskopické	mikroskopický	k2eAgFnSc6d1	mikroskopická
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
humánní	humánní	k2eAgFnSc2d1	humánní
a	a	k8xC	a
veterinární	veterinární	k2eAgFnSc2d1	veterinární
patologie	patologie	k1gFnSc2	patologie
je	být	k5eAaImIp3nS	být
histopatologické	histopatologický	k2eAgNnSc1d1	histopatologické
vyšetření	vyšetření	k1gNnSc1	vyšetření
bioptických	bioptický	k2eAgInPc2d1	bioptický
a	a	k8xC	a
autoptických	autoptický	k2eAgInPc2d1	autoptický
vzorků	vzorek	k1gInPc2	vzorek
tkání	tkáň	k1gFnPc2	tkáň
základním	základní	k2eAgInSc7d1	základní
postupem	postup	k1gInSc7	postup
při	při	k7c6	při
hledání	hledání	k1gNnSc6	hledání
diagnózy	diagnóza	k1gFnSc2	diagnóza
a	a	k8xC	a
tedy	tedy	k9	tedy
hlavní	hlavní	k2eAgFnSc7d1	hlavní
náplní	náplň	k1gFnSc7	náplň
lékařů	lékař	k1gMnPc2	lékař
pracujících	pracující	k2eAgMnPc2d1	pracující
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
oborech	obor	k1gInPc6	obor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Orgánová	orgánový	k2eAgFnSc1d1	orgánová
patologie	patologie	k1gFnSc1	patologie
====	====	k?	====
</s>
</p>
<p>
<s>
Orgánová	orgánový	k2eAgFnSc1d1	orgánová
patologie	patologie	k1gFnSc1	patologie
je	být	k5eAaImIp3nS	být
obor	obor	k1gInSc4	obor
studující	studující	k2eAgFnSc2d1	studující
chorobné	chorobný	k2eAgFnSc2d1	chorobná
změny	změna	k1gFnSc2	změna
na	na	k7c6	na
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
orgánech	orgán	k1gInPc6	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
řadu	řada	k1gFnSc4	řada
onemocnění	onemocnění	k1gNnSc2	onemocnění
jsou	být	k5eAaImIp3nP	být
orgánové	orgánový	k2eAgFnPc1d1	orgánová
změny	změna	k1gFnPc1	změna
jen	jen	k6eAd1	jen
nespecifické	specifický	k2eNgFnPc1d1	nespecifická
a	a	k8xC	a
proto	proto	k8xC	proto
samotné	samotný	k2eAgNnSc1d1	samotné
makroskopické	makroskopický	k2eAgNnSc1d1	makroskopické
vyšetření	vyšetření	k1gNnSc1	vyšetření
obvykle	obvykle	k6eAd1	obvykle
nepostačuje	postačovat	k5eNaImIp3nS	postačovat
pro	pro	k7c4	pro
vlastní	vlastní	k2eAgFnSc4d1	vlastní
diagnózu	diagnóza	k1gFnSc4	diagnóza
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
vyšetřovat	vyšetřovat	k5eAaImF	vyšetřovat
histopatologicky	histopatologicky	k6eAd1	histopatologicky
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obory	obor	k1gInPc4	obor
úzce	úzko	k6eAd1	úzko
související	související	k2eAgInPc4d1	související
s	s	k7c7	s
patologií	patologie	k1gFnSc7	patologie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Klinická	klinický	k2eAgFnSc1d1	klinická
patologie	patologie	k1gFnSc1	patologie
===	===	k?	===
</s>
</p>
<p>
<s>
Pojem	pojem	k1gInSc1	pojem
klinická	klinický	k2eAgFnSc1d1	klinická
patologie	patologie	k1gFnSc1	patologie
je	být	k5eAaImIp3nS	být
doslovným	doslovný	k2eAgInSc7d1	doslovný
překladem	překlad	k1gInSc7	překlad
anglického	anglický	k2eAgInSc2d1	anglický
pojmu	pojem	k1gInSc2	pojem
clinical	clinicat	k5eAaPmAgInS	clinicat
pathology	patholog	k1gMnPc4	patholog
<g/>
,	,	kIx,	,
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
prostředí	prostředí	k1gNnSc6	prostředí
se	se	k3xPyFc4	se
však	však	k9	však
tento	tento	k3xDgInSc1	tento
pojem	pojem	k1gInSc1	pojem
nepoužívá	používat	k5eNaImIp3nS	používat
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
mu	on	k3xPp3gMnSc3	on
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
pojem	pojem	k1gInSc1	pojem
laboratorní	laboratorní	k2eAgFnSc1d1	laboratorní
medicína	medicína	k1gFnSc1	medicína
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
pojem	pojem	k1gInSc4	pojem
zastřešující	zastřešující	k2eAgFnSc4d1	zastřešující
zejména	zejména	k9	zejména
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
biochemii	biochemie	k1gFnSc4	biochemie
a	a	k8xC	a
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
mikrobiologii	mikrobiologie	k1gFnSc4	mikrobiologie
a	a	k8xC	a
laboratorní	laboratorní	k2eAgInPc4d1	laboratorní
úseky	úsek	k1gInPc4	úsek
hematologie	hematologie	k1gFnSc2	hematologie
<g/>
,	,	kIx,	,
lékařské	lékařský	k2eAgFnSc2d1	lékařská
genetiky	genetika	k1gFnSc2	genetika
a	a	k8xC	a
klinické	klinický	k2eAgFnSc2d1	klinická
imunologie	imunologie	k1gFnSc2	imunologie
</s>
</p>
<p>
<s>
===	===	k?	===
Soudní	soudní	k2eAgNnSc1d1	soudní
lékařství	lékařství	k1gNnSc1	lékařství
===	===	k?	===
</s>
</p>
<p>
<s>
Soudní	soudní	k2eAgNnSc1d1	soudní
lékařství	lékařství	k1gNnSc1	lékařství
je	být	k5eAaImIp3nS	být
obor	obor	k1gInSc4	obor
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
z	z	k7c2	z
patologické	patologický	k2eAgFnSc2d1	patologická
anatomie	anatomie	k1gFnSc2	anatomie
<g/>
.	.	kIx.	.
</s>
<s>
Zabývá	zabývat	k5eAaImIp3nS	zabývat
se	se	k3xPyFc4	se
především	především	k9	především
zjišťováním	zjišťování	k1gNnSc7	zjišťování
příčin	příčina	k1gFnPc2	příčina
náhlých	náhlý	k2eAgFnPc2d1	náhlá
a	a	k8xC	a
neočekávaných	očekávaný	k2eNgFnPc2d1	neočekávaná
úmrtí	úmrť	k1gFnPc2	úmrť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Toxikologie	toxikologie	k1gFnSc2	toxikologie
===	===	k?	===
</s>
</p>
<p>
<s>
Toxikologie	toxikologie	k1gFnSc1	toxikologie
je	být	k5eAaImIp3nS	být
především	především	k9	především
chemický	chemický	k2eAgInSc4d1	chemický
obor	obor	k1gInSc4	obor
zabývající	zabývající	k2eAgInSc4d1	zabývající
se	se	k3xPyFc4	se
jedy	jed	k1gInPc1	jed
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nox	noxa	k1gFnPc2	noxa
vyvolávajících	vyvolávající	k2eAgFnPc2d1	vyvolávající
řadu	řada	k1gFnSc4	řada
chorobných	chorobný	k2eAgFnPc2d1	chorobná
změn	změna	k1gFnPc2	změna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mikrobiologie	mikrobiologie	k1gFnSc2	mikrobiologie
===	===	k?	===
</s>
</p>
<p>
<s>
Mikrobiologie	mikrobiologie	k1gFnPc4	mikrobiologie
a	a	k8xC	a
biologické	biologický	k2eAgInPc4d1	biologický
obory	obor	k1gInPc4	obor
s	s	k7c7	s
ní	on	k3xPp3gFnSc7	on
související	související	k2eAgFnSc1d1	související
(	(	kIx(	(
<g/>
virologie	virologie	k1gFnSc1	virologie
<g/>
,	,	kIx,	,
parazitologie	parazitologie	k1gFnSc1	parazitologie
<g/>
,	,	kIx,	,
mykologie	mykologie	k1gFnSc1	mykologie
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
studiem	studio	k1gNnSc7	studio
biologických	biologický	k2eAgFnPc2d1	biologická
nox	noxa	k1gFnPc2	noxa
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
patologie	patologie	k1gFnSc2	patologie
má	mít	k5eAaImIp3nS	mít
mikrobiologie	mikrobiologie	k1gFnPc1	mikrobiologie
další	další	k2eAgInPc4d1	další
výrazné	výrazný	k2eAgInPc4d1	výrazný
kontakty	kontakt	k1gInPc4	kontakt
s	s	k7c7	s
infekčním	infekční	k2eAgNnSc7d1	infekční
lékařstvím	lékařství	k1gNnSc7	lékařství
a	a	k8xC	a
epidemiologií	epidemiologie	k1gFnSc7	epidemiologie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Imunologie	imunologie	k1gFnSc2	imunologie
===	===	k?	===
</s>
</p>
<p>
<s>
Imunologie	imunologie	k1gFnSc1	imunologie
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
studiem	studio	k1gNnSc7	studio
funkce	funkce	k1gFnSc2	funkce
i	i	k8xC	i
poruch	porucha	k1gFnPc2	porucha
imunitního	imunitní	k2eAgInSc2d1	imunitní
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
funkce	funkce	k1gFnSc1	funkce
hraje	hrát	k5eAaImIp3nS	hrát
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
v	v	k7c6	v
patogenezi	patogeneze	k1gFnSc6	patogeneze
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
chorob	choroba	k1gFnPc2	choroba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Genetika	genetika	k1gFnSc1	genetika
===	===	k?	===
</s>
</p>
<p>
<s>
Genetika	genetika	k1gFnSc1	genetika
studuje	studovat	k5eAaImIp3nS	studovat
dědičnost	dědičnost	k1gFnSc4	dědičnost
organizmů	organizmus	k1gInPc2	organizmus
<g/>
.	.	kIx.	.
</s>
<s>
Ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
řada	řada	k1gFnSc1	řada
onemocnění	onemocnění	k1gNnPc2	onemocnění
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
původ	původ	k1gInSc4	původ
přímo	přímo	k6eAd1	přímo
v	v	k7c6	v
chybné	chybný	k2eAgFnSc6d1	chybná
genetické	genetický	k2eAgFnSc3d1	genetická
informaci	informace	k1gFnSc3	informace
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
jiné	jiné	k1gNnSc4	jiné
představují	představovat	k5eAaImIp3nP	představovat
zděděné	zděděný	k2eAgFnPc4d1	zděděná
vlohy	vloha	k1gFnPc4	vloha
vyšší	vysoký	k2eAgNnSc4d2	vyšší
riziko	riziko	k1gNnSc4	riziko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Klinická	klinický	k2eAgFnSc1d1	klinická
biochemie	biochemie	k1gFnSc1	biochemie
===	===	k?	===
</s>
</p>
<p>
<s>
Klinická	klinický	k2eAgFnSc1d1	klinická
biochemie	biochemie	k1gFnSc1	biochemie
je	být	k5eAaImIp3nS	být
lékařský	lékařský	k2eAgInSc4d1	lékařský
obor	obor	k1gInSc4	obor
zabývající	zabývající	k2eAgInSc4d1	zabývající
se	s	k7c7	s
studiem	studio	k1gNnSc7	studio
a	a	k8xC	a
diagnostickým	diagnostický	k2eAgNnSc7d1	diagnostické
využitím	využití	k1gNnSc7	využití
kolísání	kolísání	k1gNnSc2	kolísání
chemických	chemický	k2eAgFnPc2d1	chemická
látek	látka	k1gFnPc2	látka
v	v	k7c6	v
tělesných	tělesný	k2eAgFnPc6d1	tělesná
tekutinách	tekutina	k1gFnPc6	tekutina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
MAČÁK	MAČÁK	kA	MAČÁK
<g/>
,	,	kIx,	,
Jirka	Jirka	k1gFnSc1	Jirka
<g/>
.	.	kIx.	.
</s>
<s>
Obecná	obecný	k2eAgFnSc1d1	obecná
patologie	patologie	k1gFnSc1	patologie
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
:	:	kIx,	:
Univerzita	univerzita	k1gFnSc1	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
189	[number]	k4	189
s.	s.	k?	s.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Patologická	patologický	k2eAgFnSc1d1	patologická
anatomie	anatomie	k1gFnSc1	anatomie
</s>
</p>
<p>
<s>
Patologická	patologický	k2eAgFnSc1d1	patologická
fyziologie	fyziologie	k1gFnSc1	fyziologie
</s>
</p>
<p>
<s>
Veterinární	veterinární	k2eAgFnSc1d1	veterinární
patologie	patologie	k1gFnSc1	patologie
</s>
</p>
<p>
<s>
Fytopatologie	fytopatologie	k1gFnSc1	fytopatologie
</s>
</p>
<p>
<s>
Psychopatologie	psychopatologie	k1gFnSc1	psychopatologie
</s>
</p>
<p>
<s>
Sociální	sociální	k2eAgFnSc1d1	sociální
patologie	patologie	k1gFnSc1	patologie
</s>
</p>
<p>
<s>
Paleopatologie	Paleopatologie	k1gFnSc1	Paleopatologie
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
patologie	patologie	k1gFnSc2	patologie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
patologie	patologie	k1gFnSc2	patologie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Pathologie	pathologie	k1gFnSc2	pathologie
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Společnost	společnost	k1gFnSc1	společnost
českých	český	k2eAgMnPc2d1	český
patologů	patolog	k1gMnPc2	patolog
</s>
</p>
