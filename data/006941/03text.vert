<s>
Rohan	Rohan	k1gMnSc1	Rohan
Bopanna	Bopanno	k1gNnSc2	Bopanno
(	(	kIx(	(
<g/>
*	*	kIx~	*
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1980	[number]	k4	1980
Bengalúru	Bengalúr	k1gInSc2	Bengalúr
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
indický	indický	k2eAgMnSc1d1	indický
profesionální	profesionální	k2eAgMnSc1d1	profesionální
tenista	tenista	k1gMnSc1	tenista
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
specializuje	specializovat	k5eAaBmIp3nS	specializovat
na	na	k7c4	na
čtyřhru	čtyřhra	k1gFnSc4	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
finále	finále	k1gNnSc2	finále
debla	deblo	k1gNnSc2	deblo
se	se	k3xPyFc4	se
probojoval	probojovat	k5eAaPmAgMnS	probojovat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ajsámem	Ajsám	k1gMnSc7	Ajsám
Kúreším	Kúreší	k2eAgMnSc7d1	Kúreší
na	na	k7c6	na
newyorském	newyorský	k2eAgInSc6d1	newyorský
grandslamu	grandslam	k1gInSc6	grandslam
US	US	kA	US
Open	Open	k1gInSc4	Open
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgNnSc6	jenž
nestačili	stačit	k5eNaBmAgMnP	stačit
na	na	k7c6	na
bratry	bratr	k1gMnPc4	bratr
Boba	Bob	k1gMnSc4	Bob
a	a	k8xC	a
Mika	Mik	k1gMnSc4	Mik
Bryanovi	Bryanův	k2eAgMnPc1d1	Bryanův
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
dosavadní	dosavadní	k2eAgFnSc6d1	dosavadní
kariéře	kariéra	k1gFnSc6	kariéra
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
na	na	k7c6	na
okruhu	okruh	k1gInSc6	okruh
ATP	atp	kA	atp
World	World	k1gMnSc1	World
Tour	Tour	k1gMnSc1	Tour
osm	osm	k4xCc4	osm
turnajů	turnaj	k1gInPc2	turnaj
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
challengerech	challenger	k1gInPc6	challenger
ATP	atp	kA	atp
a	a	k8xC	a
okruhu	okruh	k1gInSc2	okruh
Futures	Futures	k1gMnSc1	Futures
získal	získat	k5eAaPmAgMnS	získat
pět	pět	k4xCc4	pět
titulů	titul	k1gInPc2	titul
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
a	a	k8xC	a
sedmnáct	sedmnáct	k4xCc4	sedmnáct
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
žebříčku	žebříčko	k1gNnSc6	žebříčko
ATP	atp	kA	atp
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
nejvýše	nejvýše	k6eAd1	nejvýše
klasifikován	klasifikovat	k5eAaImNgInS	klasifikovat
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2007	[number]	k4	2007
na	na	k7c4	na
213	[number]	k4	213
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
a	a	k8xC	a
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
pak	pak	k6eAd1	pak
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2012	[number]	k4	2012
na	na	k7c4	na
8	[number]	k4	8
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
krajanem	krajan	k1gMnSc7	krajan
Maheshem	Mahesh	k1gInSc7	Mahesh
Bhupathim	Bhupathim	k1gMnSc1	Bhupathim
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgMnS	zahrát
semifinále	semifinále	k1gNnSc4	semifinále
čtyřhry	čtyřhra	k1gFnSc2	čtyřhra
na	na	k7c6	na
londýnském	londýnský	k2eAgInSc6d1	londýnský
Turnaji	turnaj	k1gInSc6	turnaj
mistrů	mistr	k1gMnPc2	mistr
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
podlehli	podlehnout	k5eAaPmAgMnP	podlehnout
španělskému	španělský	k2eAgInSc3d1	španělský
páru	pár	k1gInSc3	pár
a	a	k8xC	a
pozdějším	pozdní	k2eAgMnPc3d2	pozdější
vítězům	vítěz	k1gMnPc3	vítěz
Marcelu	Marcela	k1gFnSc4	Marcela
Granollersovi	Granollers	k1gMnSc3	Granollers
a	a	k8xC	a
Marcu	Marc	k1gMnSc3	Marc
Lópezovi	López	k1gMnSc3	López
po	po	k7c6	po
prvních	první	k4xOgInPc6	první
dvou	dva	k4xCgInPc6	dva
setech	set	k1gInPc6	set
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
v	v	k7c6	v
supertiebreaku	supertiebreak	k1gInSc6	supertiebreak
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
indickém	indický	k2eAgInSc6d1	indický
daviscupovém	daviscupový	k2eAgInSc6d1	daviscupový
týmu	tým	k1gInSc6	tým
debutoval	debutovat	k5eAaBmAgInS	debutovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
adelaidským	adelaidský	k2eAgNnSc7d1	adelaidský
utkáním	utkání	k1gNnSc7	utkání
1	[number]	k4	1
<g/>
.	.	kIx.	.
kola	kolo	k1gNnSc2	kolo
Světové	světový	k2eAgFnSc2d1	světová
skupiny	skupina	k1gFnSc2	skupina
proti	proti	k7c3	proti
Austrálii	Austrálie	k1gFnSc3	Austrálie
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
prohrál	prohrát	k5eAaPmAgInS	prohrát
dvouhru	dvouhra	k1gFnSc4	dvouhra
se	s	k7c7	s
Scottem	Scott	k1gMnSc7	Scott
Draperem	Draper	k1gMnSc7	Draper
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
k	k	k7c3	k
sedmnácti	sedmnáct	k4xCc3	sedmnáct
mezistátním	mezistátní	k2eAgNnPc3d1	mezistátní
utkáním	utkání	k1gNnSc7	utkání
s	s	k7c7	s
bilancí	bilance	k1gFnSc7	bilance
9	[number]	k4	9
<g/>
–	–	k?	–
<g/>
17	[number]	k4	17
ve	v	k7c6	v
dvouhře	dvouhra	k1gFnSc6	dvouhra
a	a	k8xC	a
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
4	[number]	k4	4
ve	v	k7c6	v
čtyřhře	čtyřhra	k1gFnSc6	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
Indii	Indie	k1gFnSc4	Indie
reprezentoval	reprezentovat	k5eAaImAgInS	reprezentovat
na	na	k7c6	na
londýnských	londýnský	k2eAgFnPc6d1	londýnská
Letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
2012	[number]	k4	2012
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Maheshem	Mahesh	k1gInSc7	Mahesh
Bhupathim	Bhupathima	k1gFnPc2	Bhupathima
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
mužské	mužský	k2eAgFnSc2d1	mužská
čtyřhry	čtyřhra	k1gFnSc2	čtyřhra
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pozice	pozice	k1gFnPc4	pozice
sedmého	sedmý	k4xOgInSc2	sedmý
nasazeného	nasazený	k2eAgInSc2d1	nasazený
páru	pár	k1gInSc2	pár
vypadli	vypadnout	k5eAaPmAgMnP	vypadnout
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
kole	kolo	k1gNnSc6	kolo
s	s	k7c7	s
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
dvojicí	dvojice	k1gFnSc7	dvojice
Julien	Julina	k1gFnPc2	Julina
Benneteau	Benneteaus	k1gInSc2	Benneteaus
a	a	k8xC	a
Richard	Richard	k1gMnSc1	Richard
Gasquet	Gasquet	k1gMnSc1	Gasquet
po	po	k7c6	po
dvousetovém	dvousetový	k2eAgInSc6d1	dvousetový
průběhu	průběh	k1gInSc6	průběh
<g/>
.	.	kIx.	.
</s>
<s>
Indické	indický	k2eAgFnSc2d1	indická
barvy	barva	k1gFnSc2	barva
také	také	k9	také
hájil	hájit	k5eAaImAgMnS	hájit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2007	[number]	k4	2007
a	a	k8xC	a
2008	[number]	k4	2008
na	na	k7c6	na
turnaji	turnaj	k1gInSc6	turnaj
smíšených	smíšený	k2eAgNnPc2d1	smíšené
družstev	družstvo	k1gNnPc2	družstvo
Hopman	Hopman	k1gMnSc1	Hopman
Cup	cup	k1gInSc1	cup
v	v	k7c6	v
západoaustralském	západoaustralský	k2eAgInSc6d1	západoaustralský
Perthu	Perth	k1gInSc6	Perth
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
rodném	rodný	k2eAgInSc6d1	rodný
Bengalúru	Bengalúr	k1gInSc6	Bengalúr
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
spoluvlastní	spoluvlastnit	k5eAaImIp3nP	spoluvlastnit
restaturaci	restaturace	k1gFnSc4	restaturace
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
manželkou	manželka	k1gFnSc7	manželka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
Suprija	Suprija	k1gFnSc1	Suprija
Annaiah	Annaiaha	k1gFnPc2	Annaiaha
<g/>
.	.	kIx.	.
</s>
