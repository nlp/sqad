<p>
<s>
Audi	Audi	k1gNnSc1	Audi
TT	TT	kA	TT
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgInSc1d1	malý
dvoudveřový	dvoudveřový	k2eAgInSc1d1	dvoudveřový
sportovní	sportovní	k2eAgInSc1d1	sportovní
automobil	automobil	k1gInSc1	automobil
vyráběný	vyráběný	k2eAgInSc1d1	vyráběný
Volkswagen	volkswagen	k1gInSc1	volkswagen
Group	Group	k1gInSc1	Group
<g/>
,	,	kIx,	,
značkou	značka	k1gFnSc7	značka
Audi	Audi	k1gNnSc2	Audi
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
vyráběný	vyráběný	k2eAgInSc1d1	vyráběný
v	v	k7c6	v
maďarském	maďarský	k2eAgNnSc6d1	Maďarské
městě	město	k1gNnSc6	město
Győr	Győra	k1gFnPc2	Győra
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
skeletů	skelet	k1gInPc2	skelet
vyrobených	vyrobený	k2eAgInPc2d1	vyrobený
a	a	k8xC	a
nabarvených	nabarvený	k2eAgInPc2d1	nabarvený
v	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
Audi	Audi	k1gNnSc2	Audi
v	v	k7c6	v
německém	německý	k2eAgNnSc6d1	německé
městě	město	k1gNnSc6	město
Ingolstadt	Ingolstadt	k1gInSc1	Ingolstadt
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
změnilo	změnit	k5eAaPmAgNnS	změnit
s	s	k7c7	s
třetí	třetí	k4xOgFnSc7	třetí
generací	generace	k1gFnSc7	generace
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
používá	používat	k5eAaImIp3nS	používat
výhradně	výhradně	k6eAd1	výhradně
součásti	součást	k1gFnPc4	součást
vyrobené	vyrobený	k2eAgFnPc4d1	vyrobená
maďarskou	maďarský	k2eAgFnSc7d1	maďarská
továrnou	továrna	k1gFnSc7	továrna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
generací	generace	k1gFnPc2	generace
je	být	k5eAaImIp3nS	být
dostupná	dostupný	k2eAgFnSc1d1	dostupná
jako	jako	k8xS	jako
dvoumístné	dvoumístný	k2eAgNnSc1d1	dvoumístné
kupé	kupé	k1gNnSc1	kupé
a	a	k8xC	a
jako	jako	k9	jako
dvoumístný	dvoumístný	k2eAgInSc4d1	dvoumístný
roadster	roadster	k1gInSc4	roadster
<g/>
,	,	kIx,	,
postavený	postavený	k2eAgInSc4d1	postavený
na	na	k7c6	na
platformě	platforma	k1gFnSc6	platforma
Volkswagen	volkswagen	k1gInSc1	volkswagen
Group	Group	k1gInSc4	Group
A.	A.	kA	A.
Jako	jako	k8xS	jako
výsledek	výsledek	k1gInSc1	výsledek
sdílení	sdílení	k1gNnSc2	sdílení
platformy	platforma	k1gFnSc2	platforma
má	mít	k5eAaImIp3nS	mít
Audi	Audi	k1gNnSc1	Audi
TT	TT	kA	TT
identickou	identický	k2eAgFnSc4d1	identická
pohonnou	pohonný	k2eAgFnSc4d1	pohonná
soustavu	soustava	k1gFnSc4	soustava
a	a	k8xC	a
odpružení	odpružení	k1gNnSc4	odpružení
jako	jako	k8xS	jako
svoji	svůj	k3xOyFgMnPc1	svůj
příbuzní	příbuzný	k1gMnPc1	příbuzný
z	z	k7c2	z
koncernu	koncern	k1gInSc2	koncern
<g/>
;	;	kIx,	;
včetně	včetně	k7c2	včetně
napříč	napříč	k6eAd1	napříč
umístěného	umístěný	k2eAgInSc2d1	umístěný
motoru	motor	k1gInSc2	motor
vpředu	vpředu	k6eAd1	vpředu
<g/>
,	,	kIx,	,
pohonu	pohon	k1gInSc2	pohon
předních	přední	k2eAgInPc2d1	přední
nebo	nebo	k8xC	nebo
všech	všecek	k3xTgNnPc2	všecek
kol	kolo	k1gNnPc2	kolo
a	a	k8xC	a
nezávislého	závislý	k2eNgNnSc2d1	nezávislé
odpružení	odpružení	k1gNnSc2	odpružení
kol	kolo	k1gNnPc2	kolo
s	s	k7c7	s
nápravou	náprava	k1gFnSc7	náprava
typu	typ	k1gInSc2	typ
MacPherson	MacPherson	k1gInSc1	MacPherson
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
pojmenování	pojmenování	k1gNnSc1	pojmenování
==	==	k?	==
</s>
</p>
<p>
<s>
Audi	Audi	k1gNnSc1	Audi
TT	TT	kA	TT
je	být	k5eAaImIp3nS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
z	z	k7c2	z
úspěšné	úspěšný	k2eAgFnSc2d1	úspěšná
tradice	tradice	k1gFnSc2	tradice
motocyklových	motocyklový	k2eAgInPc2d1	motocyklový
závodů	závod	k1gInPc2	závod
automobilky	automobilka	k1gFnSc2	automobilka
NSU	NSU	kA	NSU
na	na	k7c6	na
britském	britský	k2eAgInSc6d1	britský
ostrově	ostrov	k1gInSc6	ostrov
Man	mana	k1gFnPc2	mana
TT	TT	kA	TT
(	(	kIx(	(
<g/>
Tourist	Tourist	k1gMnSc1	Tourist
Trophy	Tropha	k1gFnSc2	Tropha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NSU	NSU	kA	NSU
začalo	začít	k5eAaPmAgNnS	začít
soutěžit	soutěžit	k5eAaImF	soutěžit
v	v	k7c6	v
TT	TT	kA	TT
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
začlenilo	začlenit	k5eAaPmAgNnS	začlenit
do	do	k7c2	do
společnosti	společnost	k1gFnSc2	společnost
známé	známá	k1gFnSc2	známá
jako	jako	k8xS	jako
Audi	Audi	k1gNnSc1	Audi
</s>
</p>
<p>
<s>
Audi	Audi	k1gNnSc1	Audi
TT	TT	kA	TT
také	také	k6eAd1	také
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
NSU	NSU	kA	NSU
1000	[number]	k4	1000
<g/>
TT	TT	kA	TT
<g/>
,	,	kIx,	,
1200TT	[number]	k4	1200TT
a	a	k8xC	a
TTS	TTS	kA	TTS
z	z	k7c2	z
šedesátých	šedesátý	k4xOgNnPc2	šedesátý
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
také	také	k9	také
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
podle	podle	k7c2	podle
tohoto	tento	k3xDgInSc2	tento
závodu	závod	k1gInSc2	závod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jméno	jméno	k1gNnSc1	jméno
TT	TT	kA	TT
bylo	být	k5eAaImAgNnS	být
také	také	k6eAd1	také
přisuzováno	přisuzovat	k5eAaImNgNnS	přisuzovat
sloganu	slogan	k1gInSc2	slogan
"	"	kIx"	"
<g/>
Technology	technolog	k1gMnPc4	technolog
&	&	k?	&
Tradition	Tradition	k1gInSc1	Tradition
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
"	"	kIx"	"
<g/>
technologie	technologie	k1gFnSc1	technologie
a	a	k8xC	a
tradice	tradice	k1gFnSc1	tradice
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
První	první	k4xOgFnSc1	první
generace	generace	k1gFnSc1	generace
(	(	kIx(	(
<g/>
Typ	typ	k1gInSc1	typ
8	[number]	k4	8
<g/>
N	N	kA	N
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Výroba	výroba	k1gFnSc1	výroba
modelu	model	k1gInSc2	model
(	(	kIx(	(
<g/>
označení	označení	k1gNnSc1	označení
8	[number]	k4	8
<g/>
N	N	kA	N
<g/>
)	)	kIx)	)
začala	začít	k5eAaPmAgFnS	začít
jako	jako	k9	jako
kupé	kupé	k1gNnSc7	kupé
v	v	k7c6	v
září	září	k1gNnSc6	září
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
začala	začít	k5eAaPmAgFnS	začít
i	i	k9	i
výroba	výroba	k1gFnSc1	výroba
karoserie	karoserie	k1gFnSc1	karoserie
roadster	roadster	k1gInSc4	roadster
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
postavená	postavený	k2eAgFnSc1d1	postavená
na	na	k7c6	na
platformě	platforma	k1gFnSc6	platforma
skupiny	skupina	k1gFnSc2	skupina
VW	VW	kA	VW
A4	A4	k1gMnSc1	A4
(	(	kIx(	(
<g/>
PQ	PQ	kA	PQ
34	[number]	k4	34
<g/>
)	)	kIx)	)
používané	používaný	k2eAgNnSc1d1	používané
ve	v	k7c6	v
VW	VW	kA	VW
Golf	golf	k1gInSc4	golf
čtvrté	čtvrtá	k1gFnSc2	čtvrtá
generace	generace	k1gFnSc2	generace
<g/>
,	,	kIx,	,
první	první	k4xOgNnSc1	první
Audi	Audi	k1gNnSc1	Audi
S	s	k7c7	s
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
Škodě	škoda	k1gFnSc6	škoda
Octavii	octavia	k1gFnSc6	octavia
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Design	design	k1gInSc1	design
se	se	k3xPyFc4	se
mírně	mírně	k6eAd1	mírně
lišil	lišit	k5eAaImAgMnS	lišit
od	od	k7c2	od
konceptu	koncept	k1gInSc2	koncept
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
pozměněné	pozměněný	k2eAgInPc4d1	pozměněný
nárazníky	nárazník	k1gInPc4	nárazník
a	a	k8xC	a
přidání	přidání	k1gNnSc4	přidání
malého	malý	k2eAgNnSc2d1	malé
brzdového	brzdový	k2eAgNnSc2d1	brzdové
světélka	světélko	k1gNnSc2	světélko
za	za	k7c4	za
dveře	dveře	k1gFnPc4	dveře
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
verze	verze	k1gFnSc1	verze
modelu	model	k1gInSc2	model
TT	TT	kA	TT
získaly	získat	k5eAaPmAgInP	získat
pozornost	pozornost	k1gFnSc4	pozornost
po	po	k7c6	po
sérii	série	k1gFnSc6	série
nehod	nehoda	k1gFnPc2	nehoda
ve	v	k7c6	v
vysokých	vysoký	k2eAgFnPc6d1	vysoká
rychlostech	rychlost	k1gFnPc6	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Oznámené	oznámený	k2eAgFnPc1d1	oznámená
nehody	nehoda	k1gFnPc1	nehoda
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
problémy	problém	k1gInPc1	problém
se	se	k3xPyFc4	se
objevovaly	objevovat	k5eAaImAgInP	objevovat
při	při	k7c6	při
rychlostech	rychlost	k1gFnPc6	rychlost
nad	nad	k7c7	nad
180	[number]	k4	180
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
během	během	k7c2	během
náhlých	náhlý	k2eAgFnPc2d1	náhlá
změn	změna	k1gFnPc2	změna
směru	směr	k1gInSc2	směr
nebo	nebo	k8xC	nebo
při	při	k7c6	při
ostrých	ostrý	k2eAgFnPc6d1	ostrá
zatáčkách	zatáčka	k1gFnPc6	zatáčka
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
modely	model	k1gInPc1	model
kupé	kupé	k1gNnSc2	kupé
a	a	k8xC	a
roadster	roadster	k1gInSc1	roadster
byly	být	k5eAaImAgFnP	být
svolány	svolat	k5eAaPmNgInP	svolat
do	do	k7c2	do
servisů	servis	k1gInPc2	servis
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1999	[number]	k4	1999
a	a	k8xC	a
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zlepšila	zlepšit	k5eAaPmAgFnS	zlepšit
ovladatelnost	ovladatelnost	k1gFnSc1	ovladatelnost
a	a	k8xC	a
předvídavost	předvídavost	k1gFnSc1	předvídavost
řízení	řízení	k1gNnSc2	řízení
ve	v	k7c6	v
vysokých	vysoký	k2eAgFnPc6d1	vysoká
rychlostech	rychlost	k1gFnPc6	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Audi	Audi	k1gNnSc1	Audi
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Electronic	Electronice	k1gFnPc2	Electronice
Stability	stabilita	k1gFnSc2	stabilita
Programme	Programme	k1gFnSc2	Programme
<g/>
,	,	kIx,	,
a	a	k8xC	a
zadní	zadní	k2eAgInSc4d1	zadní
spoiler	spoiler	k1gInSc4	spoiler
byly	být	k5eAaImAgFnP	být
přidány	přidán	k2eAgFnPc1d1	přidána
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
se	se	k3xPyFc4	se
změnilo	změnit	k5eAaPmAgNnS	změnit
odpružení	odpružení	k1gNnSc1	odpružení
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
změny	změna	k1gFnPc1	změna
byly	být	k5eAaImAgFnP	být
aplikovány	aplikovat	k5eAaBmNgFnP	aplikovat
do	do	k7c2	do
výroby	výroba	k1gFnSc2	výroba
dalších	další	k2eAgFnPc2d1	další
verzí	verze	k1gFnPc2	verze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Druhá	druhý	k4xOgFnSc1	druhý
generace	generace	k1gFnSc1	generace
(	(	kIx(	(
<g/>
Typ	typ	k1gInSc1	typ
8	[number]	k4	8
<g/>
J	J	kA	J
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
–	–	k?	–
<g/>
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2004	[number]	k4	2004
Audi	Audi	k1gNnSc1	Audi
oznámilo	oznámit	k5eAaPmAgNnS	oznámit
že	že	k8xS	že
další	další	k2eAgFnPc1d1	další
generace	generace	k1gFnPc1	generace
modelu	model	k1gInSc2	model
TT	TT	kA	TT
bude	být	k5eAaImBp3nS	být
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
z	z	k7c2	z
hliníku	hliník	k1gInSc2	hliník
a	a	k8xC	a
že	že	k8xS	že
půjdu	jít	k5eAaImIp1nS	jít
do	do	k7c2	do
výroby	výroba	k1gFnSc2	výroba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Ukázka	ukázka	k1gFnSc1	ukázka
druhé	druhý	k4xOgFnSc2	druhý
generace	generace	k1gFnSc2	generace
modelu	model	k1gInSc2	model
TT	TT	kA	TT
byla	být	k5eAaImAgFnS	být
představena	představit	k5eAaPmNgFnS	představit
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
konceptu	koncept	k1gInSc2	koncept
Audi	Audi	k1gNnSc1	Audi
Shooting	Shooting	k1gInSc1	Shooting
Brake	Brake	k1gFnSc1	Brake
<g/>
,	,	kIx,	,
ukázané	ukázaný	k2eAgFnPc1d1	ukázaná
na	na	k7c6	na
autosalonu	autosalon	k1gInSc6	autosalon
v	v	k7c6	v
Tokyu	Tokyus	k1gInSc6	Tokyus
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
koncept	koncept	k1gInSc1	koncept
byl	být	k5eAaImAgInS	být
vhledem	vhled	k1gInSc7	vhled
do	do	k7c2	do
nové	nový	k2eAgFnSc2d1	nová
generace	generace	k1gFnSc2	generace
Audi	Audi	k1gNnSc2	Audi
TT	TT	kA	TT
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byl	být	k5eAaImAgInS	být
hranatější	hranatý	k2eAgInSc1d2	hranatější
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
dvoudveřovou	dvoudveřový	k2eAgFnSc4d1	dvoudveřová
karoserii	karoserie	k1gFnSc4	karoserie
"	"	kIx"	"
<g/>
shooting	shooting	k1gInSc1	shooting
brake	brak	k1gInSc2	brak
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Třetí	třetí	k4xOgFnSc1	třetí
generace	generace	k1gFnSc1	generace
(	(	kIx(	(
<g/>
2014	[number]	k4	2014
<g/>
-dodnes	odnesa	k1gFnPc2	-dodnesa
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Jako	jako	k8xC	jako
svoji	svůj	k3xOyFgFnSc4	svůj
předchůdci	předchůdce	k1gMnPc1	předchůdce
<g/>
,	,	kIx,	,
i	i	k8xC	i
Audi	Audi	k1gNnSc1	Audi
TT	TT	kA	TT
třetí	třetí	k4xOgFnSc1	třetí
generace	generace	k1gFnSc1	generace
bylo	být	k5eAaImAgNnS	být
předznamenáno	předznamenat	k5eAaPmNgNnS	předznamenat
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
konceptu	koncept	k1gInSc2	koncept
Audi	Audi	k1gNnSc1	Audi
Allroad	Allroad	k1gInSc1	Allroad
Shooting	Shooting	k1gInSc4	Shooting
Brake	Brak	k1gFnSc2	Brak
<g/>
,	,	kIx,	,
ukázané	ukázaný	k2eAgFnSc2d1	ukázaná
na	na	k7c6	na
autosalonu	autosalon	k1gInSc6	autosalon
v	v	k7c6	v
Detroitu	Detroit	k1gInSc6	Detroit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Audi	Audi	k1gNnSc2	Audi
TT	TT	kA	TT
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Official	Official	k1gInSc1	Official
Audi	Audi	k1gNnSc4	Audi
TT	TT	kA	TT
microsite	microsit	k1gInSc5	microsit
</s>
</p>
<p>
<s>
In	In	k?	In
Depth	Depth	k1gMnSc1	Depth
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
New	New	k1gMnSc2	New
Audi	Audi	k1gNnSc2	Audi
TT	TT	kA	TT
Coupé	Coupý	k2eAgFnSc2d1	Coupý
</s>
</p>
<p>
<s>
Audi	Audi	k1gNnSc1	Audi
TT	TT	kA	TT
katalog	katalog	k1gInSc4	katalog
dílů	díl	k1gInPc2	díl
</s>
</p>
