<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
malý	malý	k2eAgInSc1d1	malý
dvoudveřový	dvoudveřový	k2eAgInSc1d1	dvoudveřový
sportovní	sportovní	k2eAgInSc1d1	sportovní
automobil	automobil	k1gInSc1	automobil
vyráběný	vyráběný	k2eAgInSc1d1	vyráběný
Volkswagen	volkswagen	k1gInSc1	volkswagen
Group	Group	k1gInSc1	Group
<g/>
,	,	kIx,	,
značkou	značka	k1gFnSc7	značka
Audi	Audi	k1gNnSc2	Audi
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
?	?	kIx.	?
</s>
