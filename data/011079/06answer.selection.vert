<s>
Audi	Audi	k1gNnSc1	Audi
TT	TT	kA	TT
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgInSc1d1	malý
dvoudveřový	dvoudveřový	k2eAgInSc1d1	dvoudveřový
sportovní	sportovní	k2eAgInSc1d1	sportovní
automobil	automobil	k1gInSc1	automobil
vyráběný	vyráběný	k2eAgInSc1d1	vyráběný
Volkswagen	volkswagen	k1gInSc1	volkswagen
Group	Group	k1gInSc1	Group
<g/>
,	,	kIx,	,
značkou	značka	k1gFnSc7	značka
Audi	Audi	k1gNnSc2	Audi
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
vyráběný	vyráběný	k2eAgInSc1d1	vyráběný
v	v	k7c6	v
maďarském	maďarský	k2eAgNnSc6d1	Maďarské
městě	město	k1gNnSc6	město
Győr	Győra	k1gFnPc2	Győra
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
skeletů	skelet	k1gInPc2	skelet
vyrobených	vyrobený	k2eAgInPc2d1	vyrobený
a	a	k8xC	a
nabarvených	nabarvený	k2eAgInPc2d1	nabarvený
v	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
Audi	Audi	k1gNnSc2	Audi
v	v	k7c6	v
německém	německý	k2eAgNnSc6d1	německé
městě	město	k1gNnSc6	město
Ingolstadt	Ingolstadt	k1gInSc1	Ingolstadt
<g/>
.	.	kIx.	.
</s>
