<p>
<s>
Bod	bod	k1gInSc1	bod
je	být	k5eAaImIp3nS	být
bezrozměrný	bezrozměrný	k2eAgInSc1d1	bezrozměrný
základní	základní	k2eAgInSc1d1	základní
geometrický	geometrický	k2eAgInSc1d1	geometrický
útvar	útvar	k1gInSc1	útvar
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
Euklidových	Euklidův	k2eAgInPc2d1	Euklidův
Základů	základ	k1gInPc2	základ
je	být	k5eAaImIp3nS	být
bod	bod	k1gInSc1	bod
něco	něco	k3yInSc4	něco
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
nemá	mít	k5eNaImIp3nS	mít
části	část	k1gFnPc4	část
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
to	ten	k3xDgNnSc1	ten
co	co	k3yQnSc4	co
již	již	k6eAd1	již
nelze	lze	k6eNd1	lze
dále	daleko	k6eAd2	daleko
dělit	dělit	k5eAaImF	dělit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Značení	značení	k1gNnSc2	značení
==	==	k?	==
</s>
</p>
<p>
<s>
Graficky	graficky	k6eAd1	graficky
se	se	k3xPyFc4	se
bod	bod	k1gInSc1	bod
znázorňuje	znázorňovat	k5eAaImIp3nS	znázorňovat
křížkem	křížek	k1gInSc7	křížek
<g/>
,	,	kIx,	,
malým	malý	k2eAgNnSc7d1	malé
kolečkem	kolečko	k1gNnSc7	kolečko
nebo	nebo	k8xC	nebo
kroužkem	kroužek	k1gInSc7	kroužek
<g/>
,	,	kIx,	,
označuje	označovat	k5eAaImIp3nS	označovat
se	se	k3xPyFc4	se
velkým	velký	k2eAgNnSc7d1	velké
tiskacím	tiskací	k2eAgNnSc7d1	tiskací
písmenem	písmeno	k1gNnSc7	písmeno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Znázornění	znázornění	k1gNnSc1	znázornění
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Algebraický	algebraický	k2eAgInSc1d1	algebraický
zápis	zápis	k1gInSc1	zápis
==	==	k?	==
</s>
</p>
<p>
<s>
Algebraicky	algebraicky	k6eAd1	algebraicky
je	být	k5eAaImIp3nS	být
každý	každý	k3xTgInSc1	každý
bod	bod	k1gInSc1	bod
pevně	pevně	k6eAd1	pevně
určen	určit	k5eAaPmNgInS	určit
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
N	N	kA	N
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
souřadnicemi	souřadnice	k1gFnPc7	souřadnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
N	N	kA	N
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
rozměr	rozměr	k1gInSc4	rozměr
prostoru	prostor	k1gInSc2	prostor
<g/>
,	,	kIx,	,
v	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
trojrozměrném	trojrozměrný	k2eAgInSc6d1	trojrozměrný
prostoru	prostor	k1gInSc6	prostor
je	být	k5eAaImIp3nS	být
bod	bod	k1gInSc1	bod
pevně	pevně	k6eAd1	pevně
dán	dát	k5eAaPmNgInS	dát
třemi	tři	k4xCgFnPc7	tři
souřadnicemi	souřadnice	k1gFnPc7	souřadnice
<g/>
,	,	kIx,	,
v	v	k7c6	v
dvojrozměrném	dvojrozměrný	k2eAgInSc6d1	dvojrozměrný
prostoru	prostor	k1gInSc6	prostor
je	být	k5eAaImIp3nS	být
bod	bod	k1gInSc1	bod
určen	určit	k5eAaPmNgInS	určit
dvěma	dva	k4xCgFnPc7	dva
souřadnicemi	souřadnice	k1gFnPc7	souřadnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Všechny	všechen	k3xTgInPc1	všechen
geometrické	geometrický	k2eAgInPc1d1	geometrický
útvary	útvar	k1gInPc1	útvar
jsou	být	k5eAaImIp3nP	být
definovány	definován	k2eAgInPc1d1	definován
jako	jako	k8xC	jako
množina	množina	k1gFnSc1	množina
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bod	bod	k1gInSc1	bod
lze	lze	k6eAd1	lze
také	také	k9	také
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
úsečku	úsečka	k1gFnSc4	úsečka
nulové	nulový	k2eAgFnSc2d1	nulová
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dva	dva	k4xCgInPc1	dva
body	bod	k1gInPc1	bod
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
shodné	shodný	k2eAgInPc1d1	shodný
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
rozdílné	rozdílný	k2eAgInPc1d1	rozdílný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Typy	typ	k1gInPc1	typ
bodů	bod	k1gInPc2	bod
==	==	k?	==
</s>
</p>
<p>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
určité	určitý	k2eAgInPc4d1	určitý
typy	typ	k1gInPc4	typ
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
a	a	k8xC	a
často	často	k6eAd1	často
také	také	k9	také
obvyklá	obvyklý	k2eAgNnPc4d1	obvyklé
písmena	písmeno	k1gNnPc4	písmeno
užívaná	užívaný	k2eAgNnPc4d1	užívané
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnSc4	jejich
vyznačení	vyznačení	k1gNnSc4	vyznačení
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
se	se	k3xPyFc4	se
k	k	k7c3	k
těmto	tento	k3xDgNnPc3	tento
písmenům	písmeno	k1gNnPc3	písmeno
přidávají	přidávat	k5eAaImIp3nP	přidávat
číselné	číselný	k2eAgInPc4d1	číselný
indexy	index	k1gInPc4	index
nebo	nebo	k8xC	nebo
čárky	čárka	k1gFnPc4	čárka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
užívané	užívaný	k2eAgInPc1d1	užívaný
a	a	k8xC	a
s	s	k7c7	s
poměrně	poměrně	k6eAd1	poměrně
ustáleným	ustálený	k2eAgNnSc7d1	ustálené
značením	značení	k1gNnSc7	značení
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
krajní	krajní	k2eAgInSc1d1	krajní
bod	bod	k1gInSc1	bod
úsečky	úsečka	k1gFnPc1	úsečka
nebo	nebo	k8xC	nebo
polopřímky	polopřímka	k1gFnPc1	polopřímka
<g/>
,	,	kIx,	,
určující	určující	k2eAgInSc1d1	určující
bod	bod	k1gInSc1	bod
přímky	přímka	k1gFnPc1	přímka
nebo	nebo	k8xC	nebo
polopřímky	polopřímka	k1gFnPc1	polopřímka
(	(	kIx(	(
<g/>
většina	většina	k1gFnSc1	většina
bodů	bod	k1gInPc2	bod
má	mít	k5eAaImIp3nS	mít
tento	tento	k3xDgInSc1	tento
význam	význam	k1gInSc1	význam
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
vrchol	vrchol	k1gInSc1	vrchol
(	(	kIx(	(
<g/>
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
...	...	k?	...
<g/>
;	;	kIx,	;
K	K	kA	K
<g/>
,	,	kIx,	,
L	L	kA	L
<g/>
,	,	kIx,	,
M	M	kA	M
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
-	-	kIx~	-
viz	vidět	k5eAaImRp2nS	vidět
zejména	zejména	k9	zejména
Mnohoúhelník	mnohoúhelník	k1gInSc4	mnohoúhelník
</s>
</p>
<p>
<s>
střed	střed	k1gInSc1	střed
(	(	kIx(	(
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
S	s	k7c7	s
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
průsečík	průsečík	k1gInSc1	průsečík
(	(	kIx(	(
<g/>
P	P	kA	P
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
bod	bod	k1gInSc1	bod
dotyku	dotyk	k1gInSc2	dotyk
(	(	kIx(	(
<g/>
T	T	kA	T
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
těžiště	těžiště	k1gNnSc1	těžiště
(	(	kIx(	(
<g/>
T	T	kA	T
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
průsečík	průsečík	k1gInSc1	průsečík
výšek	výška	k1gFnPc2	výška
(	(	kIx(	(
<g/>
V	V	kA	V
<g/>
)	)	kIx)	)
-	-	kIx~	-
použitelné	použitelný	k2eAgNnSc1d1	použitelné
především	především	k9	především
u	u	k7c2	u
trojúhelníka	trojúhelník	k1gInSc2	trojúhelník
</s>
</p>
<p>
<s>
ohnisko	ohnisko	k1gNnSc1	ohnisko
(	(	kIx(	(
<g/>
F	F	kA	F
<g/>
,	,	kIx,	,
příp	příp	kA	příp
<g/>
.	.	kIx.	.
také	také	k9	také
E	E	kA	E
u	u	k7c2	u
elipsy	elipsa	k1gFnSc2	elipsa
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Geometrie	geometrie	k1gFnSc1	geometrie
</s>
</p>
<p>
<s>
Geometrický	geometrický	k2eAgInSc1d1	geometrický
útvar	útvar	k1gInSc1	útvar
</s>
</p>
<p>
<s>
Přímka	přímka	k1gFnSc1	přímka
</s>
</p>
<p>
<s>
Rovina	rovina	k1gFnSc1	rovina
</s>
</p>
<p>
<s>
Prostor	prostor	k1gInSc1	prostor
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
bod	bod	k1gInSc1	bod
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
bod	bod	k1gInSc1	bod
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
