<s>
Klobása	klobása	k1gFnSc1	klobása
je	být	k5eAaImIp3nS	být
uzenina	uzenina	k1gFnSc1	uzenina
z	z	k7c2	z
mletého	mletý	k2eAgNnSc2d1	mleté
masa	maso	k1gNnSc2	maso
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
tvrdé	tvrdý	k2eAgFnSc2d1	tvrdá
konzistence	konzistence	k1gFnSc2	konzistence
<g/>
,	,	kIx,	,
pikantní	pikantní	k2eAgFnSc1d1	pikantní
<g/>
,	,	kIx,	,
kořeněná	kořeněný	k2eAgFnSc1d1	kořeněná
a	a	k8xC	a
trvanlivá	trvanlivý	k2eAgFnSc1d1	trvanlivá
<g/>
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
konzumovat	konzumovat	k5eAaBmF	konzumovat
za	za	k7c4	za
studena	studeno	k1gNnPc4	studeno
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
se	se	k3xPyFc4	se
ale	ale	k9	ale
ohřívá	ohřívat	k5eAaImIp3nS	ohřívat
v	v	k7c6	v
horké	horký	k2eAgFnSc6d1	horká
vodě	voda	k1gFnSc6	voda
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
v	v	k7c6	v
páře	pára	k1gFnSc6	pára
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
opéká	opékat	k5eAaImIp3nS	opékat
na	na	k7c6	na
grilu	gril	k1gInSc6	gril
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
platí	platit	k5eAaImIp3nS	platit
<g/>
,	,	kIx,	,
že	že	k8xS	že
čím	čí	k3xOyQgNnSc7	čí
je	být	k5eAaImIp3nS	být
klobása	klobása	k1gFnSc1	klobása
tvrdší	tvrdý	k2eAgFnSc1d2	tvrdší
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
lepší	dobrý	k2eAgMnSc1d2	lepší
a	a	k8xC	a
trvanlivější	trvanlivý	k2eAgMnSc1d2	trvanlivější
<g/>
.	.	kIx.	.
</s>
<s>
Měkčí	měkčit	k5eAaImIp3nS	měkčit
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
vaří	vařit	k5eAaImIp3nS	vařit
<g/>
,	,	kIx,	,
tvrdé	tvrdé	k1gNnSc1	tvrdé
(	(	kIx(	(
<g/>
kvalitnější	kvalitní	k2eAgMnSc1d2	kvalitnější
a	a	k8xC	a
dražší	drahý	k2eAgMnSc1d2	dražší
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
jíst	jíst	k5eAaImF	jíst
v	v	k7c6	v
menším	malý	k2eAgNnSc6d2	menší
množství	množství	k1gNnSc6	množství
i	i	k9	i
zastudena	zastudena	k6eAd1	zastudena
<g/>
.	.	kIx.	.
</s>
<s>
Klobásy	klobása	k1gFnPc1	klobása
se	se	k3xPyFc4	se
konzumují	konzumovat	k5eAaBmIp3nP	konzumovat
zpravidla	zpravidla	k6eAd1	zpravidla
s	s	k7c7	s
pečivem	pečivo	k1gNnSc7	pečivo
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
chléb	chléb	k1gInSc1	chléb
nebo	nebo	k8xC	nebo
houska	houska	k1gFnSc1	houska
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
příloha	příloha	k1gFnSc1	příloha
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
podávají	podávat	k5eAaImIp3nP	podávat
křen	křen	k1gInSc4	křen
<g/>
,	,	kIx,	,
hořčice	hořčice	k1gFnSc1	hořčice
nebo	nebo	k8xC	nebo
kečup	kečup	k1gInSc1	kečup
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
klobás	klobása	k1gFnPc2	klobása
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
<g/>
,	,	kIx,	,
vyráběli	vyrábět	k5eAaImAgMnP	vyrábět
ji	on	k3xPp3gFnSc4	on
starověcí	starověký	k2eAgMnPc1d1	starověký
Sumerové	Sumer	k1gMnPc1	Sumer
<g/>
,	,	kIx,	,
Číňané	Číňan	k1gMnPc1	Číňan
<g/>
,	,	kIx,	,
Řekové	Řek	k1gMnPc1	Řek
<g/>
,	,	kIx,	,
Římané	Říman	k1gMnPc1	Říman
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
národy	národ	k1gInPc1	národ
<g/>
.	.	kIx.	.
</s>
<s>
Potravina	potravina	k1gFnSc1	potravina
byla	být	k5eAaImAgFnS	být
vyráběna	vyrábět	k5eAaImNgFnS	vyrábět
zejména	zejména	k9	zejména
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
trvanlivosti	trvanlivost	k1gFnSc2	trvanlivost
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
umožňovalo	umožňovat	k5eAaImAgNnS	umožňovat
její	její	k3xOp3gNnSc4	její
dlouhodobější	dlouhodobý	k2eAgNnSc4d2	dlouhodobější
skladování	skladování	k1gNnSc4	skladování
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
této	tento	k3xDgFnSc6	tento
potravině	potravina	k1gFnSc6	potravina
psal	psát	k5eAaImAgMnS	psát
např	např	kA	např
i	i	k8xC	i
Homér	Homér	k1gMnSc1	Homér
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
Odyseji	odysea	k1gFnSc6	odysea
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
Epicharmus	Epicharmus	k1gMnSc1	Epicharmus
nazval	nazvat	k5eAaBmAgMnS	nazvat
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
divadelních	divadelní	k2eAgFnPc2d1	divadelní
her	hra	k1gFnPc2	hra
Klobása	klobása	k1gFnSc1	klobása
<g/>
.	.	kIx.	.
</s>
<s>
Klobásy	klobás	k1gInPc1	klobás
byly	být	k5eAaImAgInP	být
známy	znám	k2eAgInPc1d1	znám
od	od	k7c2	od
nepaměti	nepaměť	k1gFnSc2	nepaměť
i	i	k9	i
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
se	se	k3xPyFc4	se
na	na	k7c6	na
venkově	venkov	k1gInSc6	venkov
vyráběly	vyrábět	k5eAaImAgInP	vyrábět
podomácku	podomácku	k6eAd1	podomácku
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
je	on	k3xPp3gNnSc4	on
vyráběli	vyrábět	k5eAaImAgMnP	vyrábět
ve	v	k7c6	v
městech	město	k1gNnPc6	město
uzenáři	uzenář	k1gMnPc1	uzenář
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
od	od	k7c2	od
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
cechu	cech	k1gInSc6	cech
s	s	k7c7	s
řezníky	řezník	k1gMnPc7	řezník
<g/>
.	.	kIx.	.
</s>
<s>
Maso	maso	k1gNnSc1	maso
se	se	k3xPyFc4	se
konzervovalo	konzervovat	k5eAaBmAgNnS	konzervovat
nejen	nejen	k6eAd1	nejen
uzením	uzení	k1gNnSc7	uzení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
sušením	sušení	k1gNnSc7	sušení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
domácnostech	domácnost	k1gFnPc6	domácnost
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
v	v	k7c6	v
zimním	zimní	k2eAgNnSc6d1	zimní
období	období	k1gNnSc6	období
udí	udit	k5eAaImIp3nP	udit
klobásy	klobás	k1gInPc4	klobás
podle	podle	k7c2	podle
tradičních	tradiční	k2eAgFnPc2d1	tradiční
receptur	receptura	k1gFnPc2	receptura
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Kvalita	kvalita	k1gFnSc1	kvalita
klobás	klobása	k1gFnPc2	klobása
i	i	k8xC	i
ostatních	ostatní	k2eAgInPc2d1	ostatní
uzenářských	uzenářský	k2eAgInPc2d1	uzenářský
výrobků	výrobek	k1gInPc2	výrobek
byla	být	k5eAaImAgFnS	být
vždy	vždy	k6eAd1	vždy
pečlivě	pečlivě	k6eAd1	pečlivě
hlídána	hlídán	k2eAgFnSc1d1	hlídána
<g/>
,	,	kIx,	,
za	za	k7c4	za
šizení	šizení	k1gNnSc4	šizení
výrobků	výrobek	k1gInPc2	výrobek
docházelo	docházet	k5eAaImAgNnS	docházet
už	už	k6eAd1	už
od	od	k7c2	od
středověku	středověk	k1gInSc2	středověk
až	až	k9	až
k	k	k7c3	k
drastickým	drastický	k2eAgInPc3d1	drastický
trestům	trest	k1gInPc3	trest
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
Rakouský	rakouský	k2eAgInSc1d1	rakouský
potravinářský	potravinářský	k2eAgInSc1d1	potravinářský
kodex	kodex	k1gInSc1	kodex
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
platil	platit	k5eAaImAgInS	platit
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
s	s	k7c7	s
obměnami	obměna	k1gFnPc7	obměna
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
nahrazen	nahradit	k5eAaPmNgInS	nahradit
státními	státní	k2eAgFnPc7d1	státní
normami	norma	k1gFnPc7	norma
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
předpisy	předpis	k1gInPc1	předpis
a	a	k8xC	a
normy	norma	k1gFnPc1	norma
zakazovaly	zakazovat	k5eAaImAgFnP	zakazovat
přidávat	přidávat	k5eAaImF	přidávat
do	do	k7c2	do
masných	masný	k2eAgInPc2d1	masný
výrobků	výrobek	k1gInPc2	výrobek
jakékoli	jakýkoli	k3yIgFnSc2	jakýkoli
cizorodé	cizorodý	k2eAgFnSc2d1	cizorodá
látky	látka	k1gFnSc2	látka
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
soli	sůl	k1gFnSc2	sůl
<g/>
,	,	kIx,	,
koření	koření	k1gNnSc2	koření
<g/>
,	,	kIx,	,
vody	voda	k1gFnPc4	voda
v	v	k7c6	v
povoleném	povolený	k2eAgNnSc6d1	povolené
množství	množství	k1gNnSc6	množství
při	při	k7c6	při
solení	solení	k1gNnSc2	solení
nastřikováním	nastřikování	k1gNnSc7	nastřikování
<g/>
,	,	kIx,	,
vitamínů	vitamín	k1gInPc2	vitamín
či	či	k8xC	či
minerálních	minerální	k2eAgFnPc2d1	minerální
látek	látka	k1gFnPc2	látka
<g/>
.	.	kIx.	.
</s>
<s>
Počátkem	počátkem	k7c2	počátkem
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
však	však	k9	však
byly	být	k5eAaImAgFnP	být
tyto	tento	k3xDgFnPc1	tento
normy	norma	k1gFnPc1	norma
zrušeny	zrušen	k2eAgFnPc1d1	zrušena
a	a	k8xC	a
na	na	k7c6	na
českém	český	k2eAgInSc6d1	český
trhu	trh	k1gInSc6	trh
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgInP	začít
objevovat	objevovat	k5eAaImF	objevovat
výrobky	výrobek	k1gInPc1	výrobek
deklarované	deklarovaný	k2eAgInPc1d1	deklarovaný
jako	jako	k8xC	jako
párky	párek	k1gInPc1	párek
či	či	k8xC	či
klobásy	klobása	k1gFnPc1	klobása
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
však	však	k9	však
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
různé	různý	k2eAgFnPc1d1	různá
náhražky	náhražka	k1gFnPc1	náhražka
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
drůbeží	drůbeží	k2eAgInSc4d1	drůbeží
separát	separát	k1gInSc4	separát
<g/>
,	,	kIx,	,
škrob	škrob	k1gInSc4	škrob
<g/>
,	,	kIx,	,
rostlinné	rostlinný	k2eAgFnPc4d1	rostlinná
bílkoviny	bílkovina	k1gFnPc4	bílkovina
<g/>
,	,	kIx,	,
mouka	mouka	k1gFnSc1	mouka
<g/>
,	,	kIx,	,
zahušťovadla	zahušťovadlo	k1gNnSc2	zahušťovadlo
<g/>
,	,	kIx,	,
apod.	apod.	kA	apod.
</s>
