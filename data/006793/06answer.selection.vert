<s>
Potravina	potravina	k1gFnSc1	potravina
byla	být	k5eAaImAgFnS	být
vyráběna	vyrábět	k5eAaImNgFnS	vyrábět
zejména	zejména	k9	zejména
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
trvanlivosti	trvanlivost	k1gFnSc2	trvanlivost
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
umožňovalo	umožňovat	k5eAaImAgNnS	umožňovat
její	její	k3xOp3gNnSc4	její
dlouhodobější	dlouhodobý	k2eAgNnSc4d2	dlouhodobější
skladování	skladování	k1gNnSc4	skladování
<g/>
.	.	kIx.	.
</s>
