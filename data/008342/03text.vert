<p>
<s>
Jizerská	jizerský	k2eAgFnSc1d1	Jizerská
padesátka	padesátka	k1gFnSc1	padesátka
je	být	k5eAaImIp3nS	být
závod	závod	k1gInSc4	závod
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
klasickou	klasický	k2eAgFnSc7d1	klasická
technikou	technika	k1gFnSc7	technika
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
přibližně	přibližně	k6eAd1	přibližně
50	[number]	k4	50
km	km	kA	km
vedoucí	vedoucí	k2eAgInPc1d1	vedoucí
Jizerskými	jizerský	k2eAgFnPc7d1	Jizerská
horami	hora	k1gFnPc7	hora
po	po	k7c6	po
tzv.	tzv.	kA	tzv.
Jizerské	jizerský	k2eAgFnSc6d1	Jizerská
magistrále	magistrála	k1gFnSc6	magistrála
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
ročník	ročník	k1gInSc1	ročník
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
a	a	k8xC	a
2013	[number]	k4	2013
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
4	[number]	k4	4
800	[number]	k4	800
závodníků	závodník	k1gMnPc2	závodník
–	–	k?	–
maximální	maximální	k2eAgInSc4d1	maximální
počet	počet	k1gInSc4	počet
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
závod	závod	k1gInSc4	závod
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
ochranou	ochrana	k1gFnSc7	ochrana
krajiny	krajina	k1gFnSc2	krajina
povolen	povolit	k5eAaPmNgInS	povolit
<g/>
.	.	kIx.	.
</s>
<s>
Závod	závod	k1gInSc1	závod
se	se	k3xPyFc4	se
jede	jet	k5eAaImIp3nS	jet
klasickou	klasický	k2eAgFnSc7d1	klasická
technikou	technika	k1gFnSc7	technika
a	a	k8xC	a
měří	měřit	k5eAaImIp3nS	měřit
50	[number]	k4	50
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
hlavního	hlavní	k2eAgInSc2d1	hlavní
závodu	závod	k1gInSc2	závod
ale	ale	k8xC	ale
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
přibyly	přibýt	k5eAaPmAgFnP	přibýt
i	i	k9	i
další	další	k2eAgFnPc1d1	další
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
30	[number]	k4	30
km	km	kA	km
volnou	volný	k2eAgFnSc7d1	volná
technikou	technika	k1gFnSc7	technika
a	a	k8xC	a
25	[number]	k4	25
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
jezdí	jezdit	k5eAaImIp3nP	jezdit
i	i	k9	i
dětský	dětský	k2eAgInSc4d1	dětský
závod	závod	k1gInSc4	závod
a	a	k8xC	a
závod	závod	k1gInSc4	závod
firemních	firemní	k2eAgFnPc2d1	firemní
štafet	štafeta	k1gFnPc2	štafeta
na	na	k7c4	na
4	[number]	k4	4
<g/>
×	×	k?	×
<g/>
3	[number]	k4	3
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Závod	závod	k1gInSc1	závod
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
součástí	součást	k1gFnSc7	součást
prestižní	prestižní	k2eAgFnSc2d1	prestižní
série	série	k1gFnSc2	série
dálkových	dálkový	k2eAgInPc2d1	dálkový
běhů	běh	k1gInPc2	běh
Worldloppet	Worldloppeta	k1gFnPc2	Worldloppeta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
závodu	závod	k1gInSc2	závod
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgInSc4	první
ročník	ročník	k1gInSc4	ročník
Jizerské	jizerský	k2eAgNnSc1d1	Jizerské
50	[number]	k4	50
se	se	k3xPyFc4	se
jel	jet	k5eAaImAgMnS	jet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počátcích	počátek	k1gInPc6	počátek
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
oddílový	oddílový	k2eAgInSc4d1	oddílový
závod	závod	k1gInSc4	závod
TJ	tj	kA	tj
Lokomotiva	lokomotiva	k1gFnSc1	lokomotiva
Liberec	Liberec	k1gInSc1	Liberec
a	a	k8xC	a
součást	součást	k1gFnSc1	součást
horolezecké	horolezecký	k2eAgFnSc2d1	horolezecká
přípravy	příprava	k1gFnSc2	příprava
některých	některý	k3yIgMnPc2	některý
členů	člen	k1gMnPc2	člen
oddílu	oddíl	k1gInSc2	oddíl
<g/>
.	.	kIx.	.
<g/>
Dalším	další	k2eAgInSc7d1	další
důležitým	důležitý	k2eAgInSc7d1	důležitý
bodem	bod	k1gInSc7	bod
byl	být	k5eAaImAgMnS	být
pro	pro	k7c4	pro
Jizerskou	jizerský	k2eAgFnSc4d1	Jizerská
50	[number]	k4	50
rok	rok	k1gInSc1	rok
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
závodu	závod	k1gInSc2	závod
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
všech	všecek	k3xTgMnPc2	všecek
14	[number]	k4	14
členů	člen	k1gMnPc2	člen
Expedice	expedice	k1gFnSc2	expedice
Peru	Peru	k1gNnSc1	Peru
'	'	kIx"	'
<g/>
70	[number]	k4	70
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
zahynuli	zahynout	k5eAaPmAgMnP	zahynout
pod	pod	k7c7	pod
kamennou	kamenný	k2eAgFnSc7d1	kamenná
lavinou	lavina	k1gFnSc7	lavina
na	na	k7c6	na
Huascaránu	Huascarán	k1gInSc6	Huascarán
dne	den	k1gInSc2	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nese	nést	k5eAaImIp3nS	nést
závod	závod	k1gInSc1	závod
přídomek	přídomek	k1gInSc1	přídomek
Memoriál	memoriál	k1gInSc1	memoriál
Expedice	expedice	k1gFnSc1	expedice
Peru	Peru	k1gNnSc1	Peru
70	[number]	k4	70
<g/>
.	.	kIx.	.
</s>
<s>
Vzpomínka	vzpomínka	k1gFnSc1	vzpomínka
na	na	k7c4	na
horolezce	horolezec	k1gMnPc4	horolezec
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nedílnou	dílný	k2eNgFnSc7d1	nedílná
součástí	součást	k1gFnSc7	součást
slavnostního	slavnostní	k2eAgNnSc2d1	slavnostní
zahájení	zahájení	k1gNnSc2	zahájení
Jizerské	jizerský	k2eAgFnSc2d1	Jizerská
50	[number]	k4	50
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Závod	závod	k1gInSc4	závod
v	v	k7c6	v
sedmdesátých	sedmdesátý	k4xOgNnPc6	sedmdesátý
letech	léto	k1gNnPc6	léto
získával	získávat	k5eAaImAgMnS	získávat
na	na	k7c6	na
popularitě	popularita	k1gFnSc6	popularita
a	a	k8xC	a
desátého	desátý	k4xOgInSc2	desátý
ročníku	ročník	k1gInSc2	ročník
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
přibližně	přibližně	k6eAd1	přibližně
7800	[number]	k4	7800
závodníků	závodník	k1gMnPc2	závodník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
je	být	k5eAaImIp3nS	být
závod	závod	k1gInSc1	závod
zařazen	zařadit	k5eAaPmNgInS	zařadit
do	do	k7c2	do
světové	světový	k2eAgFnSc2d1	světová
ligy	liga	k1gFnSc2	liga
dálkových	dálkový	k2eAgInPc2d1	dálkový
běhů	běh	k1gInPc2	běh
Worldloppet	Worldloppeta	k1gFnPc2	Worldloppeta
a	a	k8xC	a
účastní	účastnit	k5eAaImIp3nS	účastnit
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
závodníci	závodník	k1gMnPc1	závodník
z	z	k7c2	z
české	český	k2eAgFnSc2d1	Česká
i	i	k8xC	i
světové	světový	k2eAgFnSc2d1	světová
špičky	špička	k1gFnSc2	špička
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
různé	různý	k2eAgFnPc4d1	různá
známé	známý	k2eAgFnPc4d1	známá
osobnosti	osobnost	k1gFnPc4	osobnost
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
je	být	k5eAaImIp3nS	být
Jizerská	jizerský	k2eAgFnSc1d1	Jizerská
50	[number]	k4	50
součástí	součást	k1gFnPc2	součást
českého	český	k2eAgInSc2d1	český
seriálu	seriál	k1gInSc2	seriál
Ski	ski	k1gFnSc2	ski
Tour	Toura	k1gFnPc2	Toura
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
je	být	k5eAaImIp3nS	být
také	také	k9	také
součástí	součást	k1gFnSc7	součást
seriálu	seriál	k1gInSc2	seriál
Ski	ski	k1gFnSc2	ski
Classics	Classicsa	k1gFnPc2	Classicsa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
došlo	dojít	k5eAaPmAgNnS	dojít
také	také	k9	také
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
několika	několik	k4yIc2	několik
doprovodných	doprovodný	k2eAgInPc2d1	doprovodný
závodů	závod	k1gInPc2	závod
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
50	[number]	k4	50
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
se	se	k3xPyFc4	se
jede	jet	k5eAaImIp3nS	jet
ještě	ještě	k9	ještě
25	[number]	k4	25
km	km	kA	km
klasicky	klasicky	k6eAd1	klasicky
<g/>
,	,	kIx,	,
30	[number]	k4	30
km	km	kA	km
volnou	volný	k2eAgFnSc7d1	volná
technikou	technika	k1gFnSc7	technika
a	a	k8xC	a
kratší	krátký	k2eAgFnSc4d2	kratší
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
<g/>
)	)	kIx)	)
a	a	k8xC	a
několikrát	několikrát	k6eAd1	několikrát
se	se	k3xPyFc4	se
jela	jet	k5eAaImAgFnS	jet
také	také	k9	také
letní	letní	k2eAgFnSc1d1	letní
varianta	varianta	k1gFnSc1	varianta
závodu	závod	k1gInSc2	závod
na	na	k7c6	na
horských	horský	k2eAgNnPc6d1	horské
kolech	kolo	k1gNnPc6	kolo
<g/>
,	,	kIx,	,
Cyklo	Cyklo	k1gNnSc1	Cyklo
Jizerská	jizerský	k2eAgFnSc1d1	Jizerská
50	[number]	k4	50
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
se	se	k3xPyFc4	se
na	na	k7c6	na
druhém	druhý	k4xOgNnSc6	druhý
místě	místo	k1gNnSc6	místo
za	za	k7c7	za
vítězným	vítězný	k2eAgMnSc7d1	vítězný
Norem	Nor	k1gMnSc7	Nor
Anndersem	Annders	k1gMnSc7	Annders
Auklandem	Aukland	k1gInSc7	Aukland
umístil	umístit	k5eAaPmAgMnS	umístit
český	český	k2eAgMnSc1d1	český
závodník	závodník	k1gMnSc1	závodník
Lukáš	Lukáš	k1gMnSc1	Lukáš
Bauer	Bauer	k1gMnSc1	Bauer
<g/>
.	.	kIx.	.
<g/>
Ročník	ročník	k1gInSc1	ročník
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2013	[number]	k4	2013
spuštěním	spuštění	k1gNnSc7	spuštění
přihlášek	přihláška	k1gFnPc2	přihláška
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
ČEZ	ČEZ	kA	ČEZ
<g/>
,	,	kIx,	,
sponzor	sponzor	k1gMnSc1	sponzor
akce	akce	k1gFnSc2	akce
<g/>
,	,	kIx,	,
oznámila	oznámit	k5eAaPmAgFnS	oznámit
zaplnění	zaplnění	k1gNnSc4	zaplnění
stanovených	stanovený	k2eAgFnPc2d1	stanovená
kvót	kvóta	k1gFnPc2	kvóta
(	(	kIx(	(
<g/>
počet	počet	k1gInSc1	počet
přihlášek	přihláška	k1gFnPc2	přihláška
<g/>
)	)	kIx)	)
už	už	k9	už
za	za	k7c4	za
sto	sto	k4xCgNnSc4	sto
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
nedostatek	nedostatek	k1gInSc4	nedostatek
sněhu	sníh	k1gInSc2	sníh
byl	být	k5eAaImAgInS	být
však	však	k9	však
závod	závod	k1gInSc1	závod
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2014	[number]	k4	2014
popáté	popáté	k4xO	popáté
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Přehled	přehled	k1gInSc4	přehled
vítězů	vítěz	k1gMnPc2	vítěz
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Trať	trať	k1gFnSc4	trať
závodu	závod	k1gInSc2	závod
==	==	k?	==
</s>
</p>
<p>
<s>
Jizerská	jizerský	k2eAgFnSc1d1	Jizerská
50	[number]	k4	50
i	i	k8xC	i
doprovodné	doprovodný	k2eAgInPc1d1	doprovodný
závody	závod	k1gInPc1	závod
se	se	k3xPyFc4	se
jezdí	jezdit	k5eAaImIp3nP	jezdit
po	po	k7c6	po
Jizerské	jizerský	k2eAgFnSc6d1	Jizerská
magistrále	magistrála	k1gFnSc6	magistrála
<g/>
.	.	kIx.	.
</s>
<s>
Start	start	k1gInSc1	start
i	i	k8xC	i
cíl	cíl	k1gInSc1	cíl
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
v	v	k7c6	v
Bedřichově	Bedřichův	k2eAgInSc6d1	Bedřichův
v	v	k7c6	v
Jizerských	jizerský	k2eAgFnPc6d1	Jizerská
horách	hora	k1gFnPc6	hora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jizerská	jizerský	k2eAgFnSc1d1	Jizerská
50	[number]	k4	50
===	===	k?	===
</s>
</p>
<p>
<s>
Start	start	k1gInSc1	start
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
v	v	k7c6	v
Bedřichově	Bedřichův	k2eAgNnSc6d1	Bedřichovo
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
závodníci	závodník	k1gMnPc1	závodník
jedou	jet	k5eAaImIp3nP	jet
na	na	k7c4	na
Novou	nový	k2eAgFnSc4d1	nová
Louku	louka	k1gFnSc4	louka
<g/>
,	,	kIx,	,
Kristiánov	Kristiánov	k1gInSc4	Kristiánov
(	(	kIx(	(
<g/>
tam	tam	k6eAd1	tam
je	být	k5eAaImIp3nS	být
první	první	k4xOgFnSc1	první
občerstvovací	občerstvovací	k2eAgFnSc1d1	občerstvovací
stanice	stanice	k1gFnSc1	stanice
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
Rozmezí	rozmezí	k1gNnSc4	rozmezí
<g/>
,	,	kIx,	,
Knajpu	knajpa	k1gFnSc4	knajpa
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
na	na	k7c4	na
občerstvovací	občerstvovací	k2eAgFnSc4d1	občerstvovací
stanici	stanice	k1gFnSc4	stanice
Hraniční	hraniční	k2eAgFnPc1d1	hraniční
<g/>
,	,	kIx,	,
další	další	k2eAgNnSc1d1	další
občerstvení	občerstvení	k1gNnSc1	občerstvení
na	na	k7c6	na
Jizerce	Jizerka	k1gFnSc6	Jizerka
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
stanici	stanice	k1gFnSc4	stanice
na	na	k7c4	na
Smědavě	smědavě	k6eAd1	smědavě
a	a	k8xC	a
zpátky	zpátky	k6eAd1	zpátky
na	na	k7c4	na
Knajpu	knajpa	k1gFnSc4	knajpa
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
vede	vést	k5eAaImIp3nS	vést
cesta	cesta	k1gFnSc1	cesta
ke	k	k7c3	k
Krásné	krásný	k2eAgFnSc3d1	krásná
Máří	Máří	k?	Máří
na	na	k7c4	na
Hřebínek	hřebínek	k1gInSc4	hřebínek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
poslední	poslední	k2eAgFnPc4d1	poslední
občerstvovací	občerstvovací	k2eAgFnPc4d1	občerstvovací
stanice	stanice	k1gFnPc4	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
přes	přes	k7c4	přes
Bílou	bílý	k2eAgFnSc4d1	bílá
kuchyni	kuchyně	k1gFnSc4	kuchyně
<g/>
,	,	kIx,	,
Olivetskou	olivetský	k2eAgFnSc4d1	Olivetská
horu	hora	k1gFnSc4	hora
<g/>
,	,	kIx,	,
Vládní	vládní	k2eAgFnSc1d1	vládní
a	a	k8xC	a
U	u	k7c2	u
Buku	buk	k1gInSc2	buk
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
stadion	stadion	k1gInSc4	stadion
v	v	k7c6	v
Bedřichově	Bedřichův	k2eAgNnSc6d1	Bedřichovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Jizerská	jizerský	k2eAgFnSc1d1	Jizerská
25	[number]	k4	25
===	===	k?	===
</s>
</p>
<p>
<s>
Začátek	začátek	k1gInSc1	začátek
závodu	závod	k1gInSc2	závod
je	být	k5eAaImIp3nS	být
podobný	podobný	k2eAgInSc1d1	podobný
jako	jako	k9	jako
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Jizerské	jizerský	k2eAgNnSc1d1	Jizerské
50	[number]	k4	50
<g/>
.	.	kIx.	.
</s>
<s>
Závodníci	Závodník	k1gMnPc1	Závodník
vyrážejí	vyrážet	k5eAaImIp3nP	vyrážet
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Novou	nový	k2eAgFnSc4d1	nová
Louku	louka	k1gFnSc4	louka
a	a	k8xC	a
Kristiánov	Kristiánov	k1gInSc4	Kristiánov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
v	v	k7c6	v
hlavním	hlavní	k2eAgInSc6d1	hlavní
závodě	závod	k1gInSc6	závod
občerstvení	občerstvení	k1gNnSc2	občerstvení
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
50	[number]	k4	50
<g/>
kilometrového	kilometrový	k2eAgInSc2d1	kilometrový
závodu	závod	k1gInSc2	závod
však	však	k8xC	však
běžci	běžec	k1gMnPc1	běžec
nepokračují	pokračovat	k5eNaImIp3nP	pokračovat
na	na	k7c4	na
Rozmezí	rozmezí	k1gNnSc4	rozmezí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vydávají	vydávat	k5eAaImIp3nP	vydávat
se	s	k7c7	s
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
Bílé	bílý	k2eAgInPc4d1	bílý
Buky	buk	k1gInPc4	buk
a	a	k8xC	a
na	na	k7c4	na
Hřebínek	hřebínek	k1gInSc4	hřebínek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
další	další	k2eAgFnPc4d1	další
občerstvovací	občerstvovací	k2eAgFnPc4d1	občerstvovací
stanice	stanice	k1gFnPc4	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
jedou	jet	k5eAaImIp3nP	jet
přes	přes	k7c4	přes
Gregorův	Gregorův	k2eAgInSc4d1	Gregorův
kříž	kříž	k1gInSc4	kříž
a	a	k8xC	a
Novou	nový	k2eAgFnSc4d1	nová
Louku	louka	k1gFnSc4	louka
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
cíle	cíl	k1gInSc2	cíl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Bedřichovská	Bedřichovský	k2eAgFnSc1d1	Bedřichovská
30	[number]	k4	30
===	===	k?	===
</s>
</p>
<p>
<s>
Bedřichovská	Bedřichovský	k2eAgFnSc1d1	Bedřichovská
30	[number]	k4	30
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
od	od	k7c2	od
ostatních	ostatní	k2eAgInPc2d1	ostatní
závodů	závod	k1gInPc2	závod
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jede	jet	k5eAaImIp3nS	jet
volným	volný	k2eAgInSc7d1	volný
stylem	styl	k1gInSc7	styl
<g/>
.	.	kIx.	.
</s>
<s>
Prvních	první	k4xOgFnPc2	první
zhruba	zhruba	k6eAd1	zhruba
6	[number]	k4	6
km	km	kA	km
závodu	závod	k1gInSc2	závod
je	být	k5eAaImIp3nS	být
shodných	shodný	k2eAgInPc2d1	shodný
s	s	k7c7	s
oběma	dva	k4xCgInPc7	dva
závody	závod	k1gInPc7	závod
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
jedou	jet	k5eAaImIp3nP	jet
klasickou	klasický	k2eAgFnSc7d1	klasická
technikou	technika	k1gFnSc7	technika
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Kristiánova	Kristiánův	k2eAgNnSc2d1	Kristiánovo
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
na	na	k7c4	na
Rozmezí	rozmezí	k1gNnSc4	rozmezí
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
vydávají	vydávat	k5eAaPmIp3nP	vydávat
na	na	k7c4	na
Čihadla	čihadlo	k1gNnPc4	čihadlo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zatáčejí	zatáčet	k5eAaImIp3nP	zatáčet
na	na	k7c4	na
Tetřeví	tetřeví	k2eAgFnPc4d1	tetřeví
boudy	bouda	k1gFnPc4	bouda
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
závodníci	závodník	k1gMnPc1	závodník
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
na	na	k7c4	na
Hřebínek	hřebínek	k1gInSc4	hřebínek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
občerstvovací	občerstvovací	k2eAgFnPc4d1	občerstvovací
stanice	stanice	k1gFnPc4	stanice
<g/>
,	,	kIx,	,
na	na	k7c4	na
Olivetskou	olivetský	k2eAgFnSc4d1	Olivetská
horu	hora	k1gFnSc4	hora
a	a	k8xC	a
do	do	k7c2	do
cíle	cíl	k1gInSc2	cíl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Krkonošská	krkonošský	k2eAgFnSc1d1	Krkonošská
70	[number]	k4	70
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jizerská	jizerský	k2eAgFnSc1d1	Jizerská
padesátka	padesátka	k1gFnSc1	padesátka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
závodu	závod	k1gInSc2	závod
<g/>
:	:	kIx,	:
http://www.jiz50.cz/	[url]	k4	http://www.jiz50.cz/
</s>
</p>
<p>
<s>
stránky	stránka	k1gFnPc1	stránka
SkiTour	SkiToura	k1gFnPc2	SkiToura
<g/>
:	:	kIx,	:
http://www.ski-tour.cz/	[url]	k?	http://www.ski-tour.cz/
</s>
</p>
<p>
<s>
stránky	stránka	k1gFnPc1	stránka
Worldloppet	Worldloppeta	k1gFnPc2	Worldloppeta
<g/>
:	:	kIx,	:
http://www.worldloppet.com/	[url]	k?	http://www.worldloppet.com/
</s>
</p>
<p>
<s>
stránky	stránka	k1gFnPc4	stránka
Ski	ski	k1gFnSc2	ski
Classics	Classicsa	k1gFnPc2	Classicsa
<g/>
:	:	kIx,	:
http://www.skiclassics.com/	[url]	k?	http://www.skiclassics.com/
</s>
</p>
<p>
<s>
web	web	k1gInSc1	web
o	o	k7c4	o
běžkování	běžkování	k1gNnSc4	běžkování
<g/>
:	:	kIx,	:
http://www.bezkuj.com	[url]	k1gInSc1	http://www.bezkuj.com
</s>
</p>
<p>
<s>
Hrabětická	Hrabětický	k2eAgFnSc1d1	Hrabětický
louka	louka	k1gFnSc1	louka
-	-	kIx~	-
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
hromadném	hromadný	k2eAgInSc6d1	hromadný
startu	start	k1gInSc6	start
J50	J50	k1gFnSc4	J50
a	a	k8xC	a
historické	historický	k2eAgNnSc4d1	historické
foto	foto	k1gNnSc4	foto
zde	zde	k6eAd1	zde
</s>
</p>
