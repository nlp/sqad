<p>
<s>
Codex	Codex	k1gInSc1	Codex
gigas	gigasa	k1gFnPc2	gigasa
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgFnSc1d3	veliký
rukopisná	rukopisný	k2eAgFnSc1d1	rukopisná
kniha	kniha	k1gFnSc1	kniha
(	(	kIx(	(
<g/>
kodex	kodex	k1gInSc1	kodex
<g/>
)	)	kIx)	)
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
napsána	napsat	k5eAaPmNgFnS	napsat
počátkem	počátkem	k7c2	počátkem
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
v	v	k7c6	v
podlažickém	podlažický	k2eAgInSc6d1	podlažický
klášteře	klášter	k1gInSc6	klášter
v	v	k7c6	v
Podlažicích	Podlažice	k1gFnPc6	Podlažice
u	u	k7c2	u
Chrudimi	Chrudim	k1gFnSc2	Chrudim
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
označována	označovat	k5eAaImNgFnS	označovat
latinsko-řecky	latinsko-řecky	k6eAd1	latinsko-řecky
Codex	Codex	k1gInSc1	Codex
gigas	gigasa	k1gFnPc2	gigasa
(	(	kIx(	(
<g/>
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
"	"	kIx"	"
<g/>
Obrovská	obrovský	k2eAgFnSc1d1	obrovská
kniha	kniha	k1gFnSc1	kniha
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
počeštěně	počeštěně	k6eAd1	počeštěně
Kodex	kodex	k1gInSc1	kodex
gigas	gigasa	k1gFnPc2	gigasa
<g/>
,	,	kIx,	,
švédsky	švédsky	k6eAd1	švédsky
Djävulsbibeln	Djävulsbibeln	k1gInSc1	Djävulsbibeln
nebo	nebo	k8xC	nebo
podle	podle	k7c2	podle
výjimečného	výjimečný	k2eAgNnSc2d1	výjimečné
vyobrazení	vyobrazení	k1gNnSc2	vyobrazení
ďábla	ďábel	k1gMnSc2	ďábel
také	také	k6eAd1	také
jako	jako	k8xC	jako
Ďáblova	ďáblův	k2eAgFnSc1d1	Ďáblova
bible	bible	k1gFnSc1	bible
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
je	být	k5eAaImIp3nS	být
či	či	k8xC	či
dříve	dříve	k6eAd2	dříve
byla	být	k5eAaImAgFnS	být
také	také	k9	také
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k9	jako
Codex	Codex	k1gInSc1	Codex
giganteus	giganteus	k1gInSc1	giganteus
<g/>
,	,	kIx,	,
Liber	libra	k1gFnPc2	libra
pergrandis	pergrandis	k1gFnPc2	pergrandis
či	či	k8xC	či
Gigas	Gigasa	k1gFnPc2	Gigasa
librorum	librorum	k1gNnSc4	librorum
<g/>
.	.	kIx.	.
</s>
<s>
Záměrem	záměr	k1gInSc7	záměr
autora	autor	k1gMnSc2	autor
bylo	být	k5eAaImAgNnS	být
shrnout	shrnout	k5eAaPmF	shrnout
veškeré	veškerý	k3xTgNnSc4	veškerý
vědění	vědění	k1gNnSc4	vědění
do	do	k7c2	do
jediného	jediný	k2eAgNnSc2d1	jediné
díla	dílo	k1gNnSc2	dílo
<g/>
,	,	kIx,	,
vytvořit	vytvořit	k5eAaPmF	vytvořit
jakousi	jakýsi	k3yIgFnSc4	jakýsi
"	"	kIx"	"
<g/>
knihovnu	knihovna	k1gFnSc4	knihovna
v	v	k7c6	v
jediné	jediný	k2eAgFnSc6d1	jediná
knize	kniha	k1gFnSc6	kniha
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vzhled	vzhled	k1gInSc4	vzhled
kodexu	kodex	k1gInSc2	kodex
==	==	k?	==
</s>
</p>
<p>
<s>
Codex	Codex	k1gInSc1	Codex
gigas	gigasa	k1gFnPc2	gigasa
je	být	k5eAaImIp3nS	být
svázán	svázat	k5eAaPmNgMnS	svázat
v	v	k7c6	v
dřevěných	dřevěný	k2eAgFnPc6d1	dřevěná
deskách	deska	k1gFnPc6	deska
potažených	potažený	k2eAgFnPc6d1	potažená
světlou	světlý	k2eAgFnSc7d1	světlá
kůží	kůže	k1gFnPc2	kůže
<g/>
,	,	kIx,	,
opatřených	opatřený	k2eAgInPc2d1	opatřený
kovovým	kovový	k2eAgNnSc7d1	kovové
zdobením	zdobení	k1gNnSc7	zdobení
<g/>
.	.	kIx.	.
</s>
<s>
Rozměr	rozměr	k1gInSc1	rozměr
desek	deska	k1gFnPc2	deska
je	být	k5eAaImIp3nS	být
920	[number]	k4	920
<g/>
×	×	k?	×
<g/>
505	[number]	k4	505
<g/>
×	×	k?	×
<g/>
220	[number]	k4	220
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
320	[number]	k4	320
pergamenových	pergamenový	k2eAgInPc2d1	pergamenový
listů	list	k1gInPc2	list
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
640	[number]	k4	640
stran	strana	k1gFnPc2	strana
<g/>
)	)	kIx)	)
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
890	[number]	k4	890
<g/>
×	×	k?	×
<g/>
490	[number]	k4	490
mm	mm	kA	mm
<g/>
.	.	kIx.	.
</s>
<s>
Osm	osm	k4xCc1	osm
listů	list	k1gInPc2	list
bylo	být	k5eAaImAgNnS	být
vyříznuto	vyříznut	k2eAgNnSc1d1	vyříznuto
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
kdy	kdy	k6eAd1	kdy
<g/>
,	,	kIx,	,
kým	kdo	k3yRnSc7	kdo
a	a	k8xC	a
proč	proč	k6eAd1	proč
<g/>
.	.	kIx.	.
</s>
<s>
Odstraněné	odstraněný	k2eAgInPc1d1	odstraněný
listy	list	k1gInPc1	list
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
obsahovaly	obsahovat	k5eAaImAgInP	obsahovat
řeholní	řeholní	k2eAgInPc1d1	řeholní
pravidla	pravidlo	k1gNnPc1	pravidlo
benediktinského	benediktinský	k2eAgInSc2d1	benediktinský
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
celého	celý	k2eAgInSc2d1	celý
kodexu	kodex	k1gInSc2	kodex
je	být	k5eAaImIp3nS	být
75	[number]	k4	75
kg	kg	kA	kg
a	a	k8xC	a
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
potřebného	potřebný	k2eAgInSc2d1	potřebný
pergamenu	pergamen	k1gInSc2	pergamen
bylo	být	k5eAaImAgNnS	být
spotřebováno	spotřebovat	k5eAaPmNgNnS	spotřebovat
160	[number]	k4	160
kůží	kůže	k1gFnPc2	kůže
oslů	osel	k1gMnPc2	osel
či	či	k8xC	či
mezků	mezek	k1gMnPc2	mezek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
kodexu	kodex	k1gInSc2	kodex
==	==	k?	==
</s>
</p>
<p>
<s>
Kodex	kodex	k1gInSc1	kodex
patrně	patrně	k6eAd1	patrně
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
Podlažice	Podlažice	k1gFnSc2	Podlažice
u	u	k7c2	u
Chrudimi	Chrudim	k1gFnSc2	Chrudim
<g/>
.	.	kIx.	.
</s>
<s>
Záznamy	záznam	k1gInPc1	záznam
v	v	k7c6	v
kodexu	kodex	k1gInSc6	kodex
končí	končit	k5eAaImIp3nS	končit
rokem	rok	k1gInSc7	rok
1229	[number]	k4	1229
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1245	[number]	k4	1245
byl	být	k5eAaImAgInS	být
kodex	kodex	k1gInSc1	kodex
zastaven	zastaven	k2eAgInSc1d1	zastaven
cisterciáckému	cisterciácký	k2eAgInSc3d1	cisterciácký
klášteru	klášter	k1gInSc3	klášter
v	v	k7c6	v
Sedlci	Sedlec	k1gInSc6	Sedlec
<g/>
.	.	kIx.	.
</s>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Bavor	Bavor	k1gMnSc1	Bavor
z	z	k7c2	z
Nečtin	Nečtin	k1gMnSc1	Nečtin
(	(	kIx(	(
<g/>
1290	[number]	k4	1290
až	až	k9	až
1332	[number]	k4	1332
<g/>
)	)	kIx)	)
jej	on	k3xPp3gInSc2	on
roku	rok	k1gInSc2	rok
1295	[number]	k4	1295
odkoupil	odkoupit	k5eAaPmAgInS	odkoupit
pro	pro	k7c4	pro
břevnovský	břevnovský	k2eAgInSc4d1	břevnovský
klášter	klášter	k1gInSc4	klášter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1477	[number]	k4	1477
<g/>
–	–	k?	–
<g/>
1593	[number]	k4	1593
se	se	k3xPyFc4	se
kodex	kodex	k1gInSc1	kodex
nacházel	nacházet	k5eAaImAgInS	nacházet
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
v	v	k7c6	v
Broumově	Broumov	k1gInSc6	Broumov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1594	[number]	k4	1594
byl	být	k5eAaImAgMnS	být
převezen	převézt	k5eAaPmNgMnS	převézt
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
součástí	součást	k1gFnSc7	součást
sbírky	sbírka	k1gFnSc2	sbírka
císaře	císař	k1gMnSc2	císař
Rudolfa	Rudolf	k1gMnSc2	Rudolf
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1648	[number]	k4	1648
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
švédské	švédský	k2eAgFnSc3d1	švédská
armádě	armáda	k1gFnSc3	armáda
podařil	podařit	k5eAaPmAgInS	podařit
nečekaný	čekaný	k2eNgInSc1d1	nečekaný
přepad	přepad	k1gInSc1	přepad
Prahy	Praha	k1gFnSc2	Praha
(	(	kIx(	(
<g/>
dobyli	dobýt	k5eAaPmAgMnP	dobýt
však	však	k9	však
pouze	pouze	k6eAd1	pouze
levobřežní	levobřežní	k2eAgFnSc4d1	levobřežní
část	část	k1gFnSc4	část
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
němž	jenž	k3xRgNnSc6	jenž
ukořistili	ukořistit	k5eAaPmAgMnP	ukořistit
poklady	poklad	k1gInPc4	poklad
veliké	veliký	k2eAgFnSc2d1	veliká
hodnoty	hodnota	k1gFnSc2	hodnota
–	–	k?	–
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
obsah	obsah	k1gInSc4	obsah
Rudolfovy	Rudolfův	k2eAgFnSc2d1	Rudolfova
kunstkomory	kunstkomora	k1gFnSc2	kunstkomora
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
odvezena	odvézt	k5eAaPmNgFnS	odvézt
do	do	k7c2	do
Stockholmu	Stockholm	k1gInSc2	Stockholm
<g/>
.	.	kIx.	.
</s>
<s>
Rukopis	rukopis	k1gInSc1	rukopis
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
uložen	uložit	k5eAaPmNgInS	uložit
v	v	k7c6	v
Královské	královský	k2eAgFnSc6d1	královská
knihovně	knihovna	k1gFnSc6	knihovna
ve	v	k7c6	v
Stockholmu	Stockholm	k1gInSc6	Stockholm
a	a	k8xC	a
přes	přes	k7c4	přes
opakované	opakovaný	k2eAgFnPc4d1	opakovaná
snahy	snaha	k1gFnPc4	snaha
se	se	k3xPyFc4	se
nepodařilo	podařit	k5eNaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
jej	on	k3xPp3gInSc4	on
od	od	k7c2	od
Švédska	Švédsko	k1gNnSc2	Švédsko
zpět	zpět	k6eAd1	zpět
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
září	září	k1gNnSc2	září
2007	[number]	k4	2007
do	do	k7c2	do
března	březen	k1gInSc2	březen
2008	[number]	k4	2008
byl	být	k5eAaImAgMnS	být
zapůjčen	zapůjčit	k5eAaPmNgMnS	zapůjčit
na	na	k7c4	na
výstavu	výstava	k1gFnSc4	výstava
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
Klementinu	Klementinum	k1gNnSc6	Klementinum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nedaleko	nedaleko	k7c2	nedaleko
místa	místo	k1gNnSc2	místo
svého	svůj	k3xOyFgInSc2	svůj
vzniku	vznik	k1gInSc2	vznik
<g/>
,	,	kIx,	,
v	v	k7c6	v
expozici	expozice	k1gFnSc6	expozice
Městského	městský	k2eAgNnSc2d1	Městské
muzea	muzeum	k1gNnSc2	muzeum
v	v	k7c6	v
Chrasti	chrast	k1gFnSc6	chrast
je	být	k5eAaImIp3nS	být
připomenut	připomenout	k5eAaPmNgInS	připomenout
alespoň	alespoň	k9	alespoň
maketou	maketa	k1gFnSc7	maketa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsah	obsah	k1gInSc1	obsah
kodexu	kodex	k1gInSc2	kodex
==	==	k?	==
</s>
</p>
<p>
<s>
Celý	celý	k2eAgInSc1d1	celý
rukopis	rukopis	k1gInSc1	rukopis
je	být	k5eAaImIp3nS	být
napsán	napsat	k5eAaPmNgInS	napsat
latinsky	latinsky	k6eAd1	latinsky
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
obsahem	obsah	k1gInSc7	obsah
je	být	k5eAaImIp3nS	být
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
různých	různý	k2eAgInPc2d1	různý
textů	text	k1gInPc2	text
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
desíti	deset	k4xCc2	deset
skupin	skupina	k1gFnPc2	skupina
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
text	text	k1gInSc1	text
bible	bible	k1gFnSc2	bible
v	v	k7c6	v
latinském	latinský	k2eAgInSc6d1	latinský
překladu	překlad	k1gInSc6	překlad
(	(	kIx(	(
<g/>
Starý	starý	k2eAgMnSc1d1	starý
a	a	k8xC	a
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
<g/>
)	)	kIx)	)
v	v	k7c6	v
poněkud	poněkud	k6eAd1	poněkud
jiné	jiný	k2eAgFnSc6d1	jiná
podobě	podoba	k1gFnSc6	podoba
<g/>
,	,	kIx,	,
než	než	k8xS	než
jsme	být	k5eAaImIp1nP	být
zvyklí	zvyklý	k2eAgMnPc1d1	zvyklý
z	z	k7c2	z
moderních	moderní	k2eAgInPc2d1	moderní
překladů	překlad	k1gInPc2	překlad
(	(	kIx(	(
<g/>
pořadí	pořadí	k1gNnSc1	pořadí
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
knih	kniha	k1gFnPc2	kniha
je	být	k5eAaImIp3nS	být
poněkud	poněkud	k6eAd1	poněkud
jiné	jiný	k2eAgNnSc1d1	jiné
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
obsaženy	obsáhnout	k5eAaPmNgInP	obsáhnout
i	i	k9	i
deuterokanonické	deuterokanonický	k2eAgInPc1d1	deuterokanonický
texty	text	k1gInPc1	text
jako	jako	k8xC	jako
např.	např.	kA	např.
kniha	kniha	k1gFnSc1	kniha
Báruch	Báruch	k1gMnSc1	Báruch
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Josephus	Josephus	k1gMnSc1	Josephus
Flavius	Flavius	k1gMnSc1	Flavius
–	–	k?	–
oba	dva	k4xCgMnPc4	dva
jeho	jeho	k3xOp3gInPc4	jeho
spisy	spis	k1gInPc4	spis
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
historie	historie	k1gFnSc2	historie
–	–	k?	–
tj.	tj.	kA	tj.
Židovské	židovská	k1gFnSc2	židovská
starožitnosti	starožitnost	k1gFnSc2	starožitnost
a	a	k8xC	a
Židovská	židovský	k2eAgFnSc1d1	židovská
válka	válka	k1gFnSc1	válka
</s>
</p>
<p>
<s>
Isidor	Isidor	k1gMnSc1	Isidor
ze	z	k7c2	z
Sevilly	Sevilla	k1gFnSc2	Sevilla
–	–	k?	–
kopie	kopie	k1gFnSc2	kopie
jeho	jeho	k3xOp3gFnPc1	jeho
monumentální	monumentální	k2eAgFnPc1d1	monumentální
encyklopedie	encyklopedie	k1gFnPc1	encyklopedie
zvané	zvaný	k2eAgFnSc2d1	zvaná
Etymologie	etymologie	k1gFnSc2	etymologie
</s>
</p>
<p>
<s>
několik	několik	k4yIc1	několik
drobnějších	drobný	k2eAgInPc2d2	drobnější
spisů	spis	k1gInPc2	spis
s	s	k7c7	s
lékařskou	lékařský	k2eAgFnSc7d1	lékařská
tematikou	tematika	k1gFnSc7	tematika
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterých	který	k3yIgInPc2	který
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
vyučovala	vyučovat	k5eAaImAgFnS	vyučovat
medicína	medicína	k1gFnSc1	medicína
a	a	k8xC	a
které	který	k3yQgNnSc1	který
souvisí	souviset	k5eAaImIp3nS	souviset
především	především	k9	především
s	s	k7c7	s
činností	činnost	k1gFnSc7	činnost
tzv.	tzv.	kA	tzv.
salernské	salernský	k2eAgFnSc2d1	salernský
lékařské	lékařský	k2eAgFnSc2d1	lékařská
školy	škola	k1gFnSc2	škola
–	–	k?	–
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
úvod	úvod	k1gInSc1	úvod
(	(	kIx(	(
<g/>
Isagoge	Isagoge	k1gFnSc1	Isagoge
<g/>
)	)	kIx)	)
do	do	k7c2	do
lékařské	lékařský	k2eAgFnSc2d1	lékařská
problematiky	problematika	k1gFnSc2	problematika
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
latinským	latinský	k2eAgInSc7d1	latinský
překladem	překlad	k1gInSc7	překlad
spisu	spis	k1gInSc2	spis
Masá	Masá	k1gFnSc1	Masá
<g/>
'	'	kIx"	'
<g/>
il	il	k?	il
fi	fi	k0	fi
t-tibb	tibb	k1gMnSc1	t-tibb
arabského	arabský	k2eAgMnSc2d1	arabský
učence	učenec	k1gMnSc2	učenec
Hunajna	Hunajn	k1gMnSc2	Hunajn
ibn	ibn	k?	ibn
Isháqa	Isháqus	k1gMnSc2	Isháqus
(	(	kIx(	(
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Konstantin	Konstantin	k1gMnSc1	Konstantin
Afričan	Afričan	k1gMnSc1	Afričan
v	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
</s>
</p>
<p>
<s>
Liber	libra	k1gFnPc2	libra
de	de	k?	de
oculis	oculis	k1gFnSc2	oculis
–	–	k?	–
tj.	tj.	kA	tj.
spisek	spisek	k1gInSc4	spisek
oftalmologický	oftalmologický	k2eAgInSc4d1	oftalmologický
od	od	k7c2	od
stejného	stejný	k2eAgInSc2d1	stejný
autora	autor	k1gMnSc2	autor
přeložený	přeložený	k2eAgInSc1d1	přeložený
stejným	stejný	k2eAgNnPc3d1	stejné
překladatelem	překladatel	k1gMnSc7	překladatel
</s>
</p>
<p>
<s>
vyznání	vyznání	k1gNnSc1	vyznání
hříchů	hřích	k1gInPc2	hřích
</s>
</p>
<p>
<s>
obranná	obranný	k2eAgNnPc4d1	obranné
zaklínadla	zaklínadlo	k1gNnPc4	zaklínadlo
pro	pro	k7c4	pro
vyhánění	vyhánění	k1gNnSc4	vyhánění
démonů	démon	k1gMnPc2	démon
(	(	kIx(	(
<g/>
strany	strana	k1gFnPc1	strana
na	na	k7c6	na
zčernalém	zčernalý	k2eAgInSc6d1	zčernalý
pergamenu	pergamen	k1gInSc6	pergamen
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Kosmova	Kosmův	k2eAgFnSc1d1	Kosmova
kronika	kronika	k1gFnSc1	kronika
česká	český	k2eAgFnSc1d1	Česká
</s>
</p>
<p>
<s>
kalendář	kalendář	k1gInSc4	kalendář
s	s	k7c7	s
nekrologiem	nekrologium	k1gNnSc7	nekrologium
<g/>
,	,	kIx,	,
seznam	seznam	k1gInSc1	seznam
bratří	bratr	k1gMnPc2	bratr
podlažického	podlažický	k2eAgInSc2d1	podlažický
kláštera	klášter	k1gInSc2	klášter
</s>
</p>
<p>
<s>
strana	strana	k1gFnSc1	strana
s	s	k7c7	s
hlaholskou	hlaholský	k2eAgFnSc7d1	hlaholská
a	a	k8xC	a
cyrilskou	cyrilský	k2eAgFnSc7d1	Cyrilská
abecedou	abeceda	k1gFnSc7	abeceda
</s>
</p>
<p>
<s>
dalších	další	k2eAgFnPc2d1	další
padesát	padesát	k4xCc1	padesát
drobných	drobný	k2eAgFnPc2d1	drobná
poznámek	poznámka	k1gFnPc2	poznámka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
do	do	k7c2	do
kodexu	kodex	k1gInSc2	kodex
zapsány	zapsat	k5eAaPmNgInP	zapsat
dodatečně	dodatečně	k6eAd1	dodatečně
ještě	ještě	k6eAd1	ještě
během	během	k7c2	během
středověku	středověk	k1gInSc2	středověk
ale	ale	k8xC	ale
i	i	k9	i
pozdějiKodex	pozdějiKodex	k1gInSc1	pozdějiKodex
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
iluminace	iluminace	k1gFnPc4	iluminace
červené	červený	k2eAgFnPc4d1	červená
<g/>
,	,	kIx,	,
modré	modrý	k2eAgFnPc4d1	modrá
<g/>
,	,	kIx,	,
žluté	žlutý	k2eAgFnPc4d1	žlutá
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgFnPc4d1	zelená
a	a	k8xC	a
zlaté	zlatý	k2eAgFnPc4d1	zlatá
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
iniciály	iniciála	k1gFnPc4	iniciála
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
bohaté	bohatý	k2eAgInPc1d1	bohatý
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
přes	přes	k7c4	přes
celou	celý	k2eAgFnSc4d1	celá
výši	výše	k1gFnSc4	výše
stránky	stránka	k1gFnSc2	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Značně	značně	k6eAd1	značně
poutavé	poutavý	k2eAgFnPc1d1	poutavá
jsou	být	k5eAaImIp3nP	být
spirálovitě	spirálovitě	k6eAd1	spirálovitě
zdobené	zdobený	k2eAgInPc1d1	zdobený
vzory	vzor	k1gInPc1	vzor
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
různé	různý	k2eAgInPc4d1	různý
květinové	květinový	k2eAgInPc4d1	květinový
výhonky	výhonek	k1gInPc4	výhonek
<g/>
.	.	kIx.	.
</s>
<s>
Stylově	stylově	k6eAd1	stylově
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
klasickou	klasický	k2eAgFnSc4d1	klasická
ukázku	ukázka	k1gFnSc4	ukázka
románského	románský	k2eAgNnSc2d1	románské
knižního	knižní	k2eAgNnSc2d1	knižní
malířství	malířství	k1gNnSc2	malířství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
díle	díl	k1gInSc6	díl
fascinuje	fascinovat	k5eAaBmIp3nS	fascinovat
jeho	jeho	k3xOp3gFnSc1	jeho
jednota	jednota	k1gFnSc1	jednota
<g/>
,	,	kIx,	,
ráz	ráz	k1gInSc1	ráz
písma	písmo	k1gNnSc2	písmo
je	být	k5eAaImIp3nS	být
beze	beze	k7c2	beze
změny	změna	k1gFnSc2	změna
téměř	téměř	k6eAd1	téměř
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
rozsahu	rozsah	k1gInSc6	rozsah
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
vlivu	vliv	k1gInSc2	vliv
stárnutí	stárnutí	k1gNnSc2	stárnutí
písaře	písař	k1gMnSc2	písař
<g/>
,	,	kIx,	,
nemocí	nemoc	k1gFnSc7	nemoc
<g/>
,	,	kIx,	,
počasí	počasí	k1gNnSc6	počasí
či	či	k8xC	či
nálad	nálada	k1gFnPc2	nálada
<g/>
.	.	kIx.	.
</s>
<s>
Možnost	možnost	k1gFnSc1	možnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
rukopis	rukopis	k1gInSc1	rukopis
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
písařské	písařský	k2eAgFnSc2d1	písařská
dílny	dílna	k1gFnSc2	dílna
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
minimalizována	minimalizovat	k5eAaBmNgFnS	minimalizovat
absencí	absence	k1gFnSc7	absence
odlišných	odlišný	k2eAgInPc2d1	odlišný
rukopisů	rukopis	k1gInPc2	rukopis
<g/>
.	.	kIx.	.
</s>
<s>
Odlišné	odlišný	k2eAgInPc4d1	odlišný
rukopisy	rukopis	k1gInPc4	rukopis
jsou	být	k5eAaImIp3nP	být
znatelné	znatelný	k2eAgInPc4d1	znatelný
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
stranách	strana	k1gFnPc6	strana
216	[number]	k4	216
<g/>
–	–	k?	–
<g/>
224	[number]	k4	224
(	(	kIx(	(
<g/>
přepis	přepis	k1gInSc1	přepis
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
dvaceti	dvacet	k4xCc2	dvacet
etymologií	etymologie	k1gFnPc2	etymologie
od	od	k7c2	od
Isidora	Isidor	k1gMnSc2	Isidor
ze	z	k7c2	z
Sevilly	Sevilla	k1gFnSc2	Sevilla
<g/>
)	)	kIx)	)
a	a	k8xC	a
stranách	strana	k1gFnPc6	strana
305	[number]	k4	305
<g/>
–	–	k?	–
<g/>
311	[number]	k4	311
(	(	kIx(	(
<g/>
kalendář	kalendář	k1gInSc4	kalendář
s	s	k7c7	s
nekrologem	nekrolog	k1gInSc7	nekrolog
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Veškeré	veškerý	k3xTgInPc1	veškerý
údaje	údaj	k1gInPc1	údaj
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
díla	dílo	k1gNnSc2	dílo
a	a	k8xC	a
autora	autor	k1gMnSc2	autor
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
chybí	chybit	k5eAaPmIp3nS	chybit
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
listu	list	k1gInSc6	list
označovaném	označovaný	k2eAgInSc6d1	označovaný
číslem	číslo	k1gNnSc7	číslo
289	[number]	k4	289
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
serveru	server	k1gInSc2	server
Manuscriptorium	Manuscriptorium	k1gNnSc1	Manuscriptorium
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
)	)	kIx)	)
či	či	k8xC	či
290	[number]	k4	290
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
stránek	stránka	k1gFnPc2	stránka
Švédské	švédský	k2eAgFnSc2d1	švédská
národní	národní	k2eAgFnSc2d1	národní
knihovny	knihovna	k1gFnSc2	knihovna
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
jinak	jinak	k6eAd1	jinak
prázdné	prázdný	k2eAgFnSc6d1	prázdná
stránce	stránka	k1gFnSc6	stránka
naprosto	naprosto	k6eAd1	naprosto
unikátní	unikátní	k2eAgNnSc1d1	unikátní
vyobrazení	vyobrazení	k1gNnSc1	vyobrazení
ďábla	ďábel	k1gMnSc2	ďábel
<g/>
,	,	kIx,	,
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
skoro	skoro	k6eAd1	skoro
50	[number]	k4	50
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
předchozích	předchozí	k2eAgInPc2d1	předchozí
listů	list	k1gInPc2	list
je	být	k5eAaImIp3nS	být
napsáno	napsat	k5eAaPmNgNnS	napsat
na	na	k7c6	na
zčernalém	zčernalý	k2eAgInSc6d1	zčernalý
pergamenu	pergamen	k1gInSc6	pergamen
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
charakter	charakter	k1gInSc1	charakter
velmi	velmi	k6eAd1	velmi
ponurý	ponurý	k2eAgInSc1d1	ponurý
<g/>
,	,	kIx,	,
hodně	hodně	k6eAd1	hodně
odlišný	odlišný	k2eAgInSc1d1	odlišný
od	od	k7c2	od
celého	celý	k2eAgInSc2d1	celý
zbytku	zbytek	k1gInSc2	zbytek
kodexu	kodex	k1gInSc2	kodex
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Legenda	legenda	k1gFnSc1	legenda
o	o	k7c4	o
sepsání	sepsání	k1gNnSc4	sepsání
Ďáblovy	ďáblův	k2eAgFnSc2d1	Ďáblova
bible	bible	k1gFnSc2	bible
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
legendy	legenda	k1gFnSc2	legenda
v	v	k7c6	v
klášteře	klášter	k1gInSc6	klášter
žil	žít	k5eAaImAgMnS	žít
mnich	mnich	k1gMnSc1	mnich
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zhřešil	zhřešit	k5eAaPmAgMnS	zhřešit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1212	[number]	k4	1212
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
královskou	královský	k2eAgFnSc7d1	královská
výpravou	výprava	k1gFnSc7	výprava
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
I.	I.	kA	I.
do	do	k7c2	do
Basileje	Basilej	k1gFnSc2	Basilej
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
od	od	k7c2	od
Fridricha	Fridrich	k1gMnSc2	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
přijal	přijmout	k5eAaPmAgMnS	přijmout
Zlatou	zlatý	k2eAgFnSc4d1	zlatá
bulu	bula	k1gFnSc4	bula
sicilskou	sicilský	k2eAgFnSc4d1	sicilská
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
jedním	jeden	k4xCgMnSc7	jeden
tajemným	tajemný	k2eAgMnSc7d1	tajemný
mágem	mág	k1gMnSc7	mág
<g/>
,	,	kIx,	,
Michaelem	Michael	k1gMnSc7	Michael
Scottem	Scott	k1gMnSc7	Scott
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
ho	on	k3xPp3gNnSc2	on
údajně	údajně	k6eAd1	údajně
zasvětil	zasvětit	k5eAaPmAgMnS	zasvětit
do	do	k7c2	do
temné	temný	k2eAgFnSc2d1	temná
magie	magie	k1gFnSc2	magie
a	a	k8xC	a
uctívání	uctívání	k1gNnSc2	uctívání
Satana	Satan	k1gMnSc2	Satan
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Podlažic	Podlažice	k1gFnPc2	Podlažice
se	se	k3xPyFc4	se
ale	ale	k9	ale
o	o	k7c6	o
jeho	jeho	k3xOp3gInPc6	jeho
satanistických	satanistický	k2eAgInPc6d1	satanistický
rituálech	rituál	k1gInPc6	rituál
dozvěděl	dozvědět	k5eAaPmAgInS	dozvědět
opat	opat	k1gMnSc1	opat
a	a	k8xC	a
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vyhnul	vyhnout	k5eAaPmAgMnS	vyhnout
přísnému	přísný	k2eAgInSc3d1	přísný
trestu	trest	k1gInSc3	trest
zazdění	zazdění	k1gNnPc2	zazdění
za	za	k7c4	za
živa	živ	k2eAgNnPc4d1	živo
<g/>
,	,	kIx,	,
slíbil	slíbit	k5eAaPmAgInS	slíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
napíše	napsat	k5eAaPmIp3nS	napsat
za	za	k7c4	za
jedinou	jediný	k2eAgFnSc4d1	jediná
noc	noc	k1gFnSc4	noc
největší	veliký	k2eAgFnSc4d3	veliký
knihu	kniha	k1gFnSc4	kniha
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnPc1d1	obsahující
všechny	všechen	k3xTgFnPc1	všechen
vědomosti	vědomost	k1gFnPc1	vědomost
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Dostal	dostat	k5eAaPmAgMnS	dostat
veškerý	veškerý	k3xTgInSc4	veškerý
potřebný	potřebný	k2eAgInSc4d1	potřebný
materiál	materiál	k1gInSc4	materiál
a	a	k8xC	a
pustil	pustit	k5eAaPmAgMnS	pustit
se	se	k3xPyFc4	se
do	do	k7c2	do
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
půlnoci	půlnoc	k1gFnSc6	půlnoc
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
úkol	úkol	k1gInSc1	úkol
nemůže	moct	k5eNaImIp3nS	moct
zvládnout	zvládnout	k5eAaPmF	zvládnout
<g/>
,	,	kIx,	,
a	a	k8xC	a
zaprodal	zaprodat	k5eAaPmAgMnS	zaprodat
svoji	svůj	k3xOyFgFnSc4	svůj
duši	duše	k1gFnSc4	duše
ďáblu	ďábel	k1gMnSc3	ďábel
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
za	za	k7c4	za
něj	on	k3xPp3gNnSc4	on
celé	celý	k2eAgNnSc4d1	celé
veledílo	veledílo	k1gNnSc4	veledílo
sepsal	sepsat	k5eAaPmAgMnS	sepsat
<g/>
,	,	kIx,	,
biblické	biblický	k2eAgFnPc1d1	biblická
pasáže	pasáž	k1gFnPc1	pasáž
ovšem	ovšem	k9	ovšem
s	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
sebezapřením	sebezapření	k1gNnSc7	sebezapření
<g/>
,	,	kIx,	,
odporem	odpor	k1gInSc7	odpor
a	a	k8xC	a
nechutí	nechuť	k1gFnSc7	nechuť
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
<g/>
,	,	kIx,	,
že	že	k8xS	že
kniha	kniha	k1gFnSc1	kniha
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
také	také	k9	také
řadu	řada	k1gFnSc4	řada
zaklínadel	zaklínadlo	k1gNnPc2	zaklínadlo
<g/>
,	,	kIx,	,
např.	např.	kA	např.
pro	pro	k7c4	pro
vyléčení	vyléčení	k1gNnSc4	vyléčení
<g/>
,	,	kIx,	,
vypátrání	vypátrání	k1gNnSc4	vypátrání
zloděje	zloděj	k1gMnSc2	zloděj
apod.	apod.	kA	apod.
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
pro	pro	k7c4	pro
vymítání	vymítání	k1gNnSc4	vymítání
ďábla	ďábel	k1gMnSc2	ďábel
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
Benediktinský	benediktinský	k2eAgInSc1d1	benediktinský
řád	řád	k1gInSc1	řád
exorcismus	exorcismus	k1gInSc1	exorcismus
prováděl	provádět	k5eAaImAgInS	provádět
<g/>
.	.	kIx.	.
</s>
<s>
Mnich	mnich	k1gMnSc1	mnich
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
vděčnosti	vděčnost	k1gFnSc2	vděčnost
přidal	přidat	k5eAaPmAgMnS	přidat
ďáblův	ďáblův	k2eAgInSc4d1	ďáblův
obrázek	obrázek	k1gInSc4	obrázek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
290	[number]	k4	290
a	a	k8xC	a
vyobrazuje	vyobrazovat	k5eAaImIp3nS	vyobrazovat
ho	on	k3xPp3gMnSc4	on
zde	zde	k6eAd1	zde
opravdu	opravdu	k6eAd1	opravdu
netradičně	tradičně	k6eNd1	tradičně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
I	i	k9	i
přes	přes	k7c4	přes
tuto	tento	k3xDgFnSc4	tento
legendu	legenda	k1gFnSc4	legenda
o	o	k7c6	o
svém	svůj	k3xOyFgNnSc6	svůj
sepsání	sepsání	k1gNnSc6	sepsání
se	se	k3xPyFc4	se
Codex	Codex	k1gInSc1	Codex
gigas	gigasa	k1gFnPc2	gigasa
dlouho	dlouho	k6eAd1	dlouho
užíval	užívat	k5eAaImAgInS	užívat
<g/>
,	,	kIx,	,
četl	číst	k5eAaImAgMnS	číst
a	a	k8xC	a
studoval	studovat	k5eAaImAgMnS	studovat
různými	různý	k2eAgMnPc7d1	různý
učenci	učenec	k1gMnPc7	učenec
a	a	k8xC	a
nebyl	být	k5eNaImAgInS	být
nikdy	nikdy	k6eAd1	nikdy
předmětem	předmět	k1gInSc7	předmět
šetření	šetření	k1gNnSc2	šetření
inkvizice	inkvizice	k1gFnSc2	inkvizice
<g/>
.	.	kIx.	.
<g/>
Legenda	legenda	k1gFnSc1	legenda
i	i	k8xC	i
kodex	kodex	k1gInSc4	kodex
hrají	hrát	k5eAaImIp3nP	hrát
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
roli	role	k1gFnSc4	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Ďáblova	ďáblův	k2eAgFnSc1d1	Ďáblova
lest	lest	k1gFnSc1	lest
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
BÁRTL	Bártl	k1gMnSc1	Bártl
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
<g/>
;	;	kIx,	;
KOSTELECKÝ	Kostelecký	k1gMnSc1	Kostelecký
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
.	.	kIx.	.
</s>
<s>
Ďáblova	ďáblův	k2eAgFnSc1d1	Ďáblova
bible	bible	k1gFnSc1	bible
<g/>
.	.	kIx.	.
</s>
<s>
Tajemství	tajemství	k1gNnSc1	tajemství
největší	veliký	k2eAgFnSc2d3	veliký
knihy	kniha	k1gFnSc2	kniha
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
85192	[number]	k4	85192
<g/>
-	-	kIx~	-
<g/>
64	[number]	k4	64
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Codex	Codex	k1gInSc1	Codex
gigas	gigas	k1gInSc4	gigas
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Codex	Codex	k1gInSc1	Codex
gigas	gigas	k1gInSc1	gigas
-	-	kIx~	-
online	onlinout	k5eAaPmIp3nS	onlinout
na	na	k7c4	na
Manuscriptorium	Manuscriptorium	k1gNnSc4	Manuscriptorium
(	(	kIx(	(
<g/>
s	s	k7c7	s
vodoznaky	vodoznak	k1gInPc7	vodoznak
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Codex	Codex	k1gInSc1	Codex
gigas	gigas	k1gInSc1	gigas
-	-	kIx~	-
online	onlinout	k5eAaPmIp3nS	onlinout
na	na	k7c4	na
World	World	k1gInSc4	World
Digital	Digital	kA	Digital
Library	Librara	k1gFnPc1	Librara
</s>
</p>
<p>
<s>
Codex	Codex	k1gInSc1	Codex
gigas	gigas	k1gInSc1	gigas
ve	v	k7c6	v
Švédské	švédský	k2eAgFnSc6d1	švédská
královské	královský	k2eAgFnSc6d1	královská
knihovně	knihovna	k1gFnSc6	knihovna
-	-	kIx~	-
česky	česky	k6eAd1	česky
(	(	kIx(	(
<g/>
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
méně	málo	k6eAd2	málo
informací	informace	k1gFnPc2	informace
<g/>
)	)	kIx)	)
a	a	k8xC	a
anglicky	anglicky	k6eAd1	anglicky
(	(	kIx(	(
<g/>
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
více	hodně	k6eAd2	hodně
informací	informace	k1gFnPc2	informace
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
o	o	k7c6	o
obsahu	obsah	k1gInSc6	obsah
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Codex	Codex	k1gInSc1	Codex
gigas	gigas	k1gInSc1	gigas
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc1	jeho
obsah	obsah	k1gInSc1	obsah
a	a	k8xC	a
funkce	funkce	k1gFnSc1	funkce
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Uhlíř	Uhlíř	k1gMnSc1	Uhlíř
</s>
</p>
<p>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Novotný	Novotný	k1gMnSc1	Novotný
-	-	kIx~	-
Velikonoce	Velikonoce	k1gFnPc1	Velikonoce
a	a	k8xC	a
Codex	Codex	k1gInSc1	Codex
gigas	gigasa	k1gFnPc2	gigasa
(	(	kIx(	(
<g/>
Záznam	záznam	k1gInSc1	záznam
schůzky	schůzka	k1gFnSc2	schůzka
Kosmologické	kosmologický	k2eAgFnSc2d1	kosmologická
sekce	sekce	k1gFnSc2	sekce
České	český	k2eAgFnSc2d1	Česká
astronomické	astronomický	k2eAgFnSc2d1	astronomická
společnosti	společnost	k1gFnSc2	společnost
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
s	s	k7c7	s
přednáškou	přednáška	k1gFnSc7	přednáška
Ing.	ing.	kA	ing.
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Novotného	Novotný	k1gMnSc2	Novotný
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
iDnes	iDnes	k1gInSc1	iDnes
<g/>
:	:	kIx,	:
Ďáblova	ďáblův	k2eAgFnSc1d1	Ďáblova
bible	bible	k1gFnSc1	bible
se	se	k3xPyFc4	se
po	po	k7c6	po
360	[number]	k4	360
letech	léto	k1gNnPc6	léto
vrátí	vrátit	k5eAaPmIp3nS	vrátit
do	do	k7c2	do
Česka	Česko	k1gNnSc2	Česko
</s>
</p>
<p>
<s>
aktualne	aktualnout	k5eAaImIp3nS	aktualnout
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
:	:	kIx,	:
Vzácná	vzácný	k2eAgFnSc1d1	vzácná
Ďáblova	ďáblův	k2eAgFnSc1d1	Ďáblova
bible	bible	k1gFnSc1	bible
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nS	vrátit
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
čas	čas	k1gInSc4	čas
</s>
</p>
<p>
<s>
iDnes	iDnes	k1gInSc1	iDnes
<g/>
:	:	kIx,	:
Ďáblova	ďáblův	k2eAgFnSc1d1	Ďáblova
bible	bible	k1gFnSc1	bible
přijela	přijet	k5eAaPmAgFnS	přijet
s	s	k7c7	s
policejním	policejní	k2eAgInSc7d1	policejní
doprovodem	doprovod	k1gInSc7	doprovod
</s>
</p>
