<s>
Arboretum	arboretum	k1gNnSc1	arboretum
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
arbor	arbor	k1gInSc1	arbor
=	=	kIx~	=
strom	strom	k1gInSc1	strom
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
sbírka	sbírka	k1gFnSc1	sbírka
živých	živý	k2eAgFnPc2d1	živá
dřevin	dřevina	k1gFnPc2	dřevina
-	-	kIx~	-
dendrologická	dendrologický	k2eAgFnSc1d1	Dendrologická
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
botanická	botanický	k2eAgFnSc1d1	botanická
zahrada	zahrada	k1gFnSc1	zahrada
specializující	specializující	k2eAgFnSc1d1	specializující
se	se	k3xPyFc4	se
na	na	k7c4	na
výzkum	výzkum	k1gInSc4	výzkum
a	a	k8xC	a
pěstění	pěstění	k1gNnSc4	pěstění
dřevin	dřevina	k1gFnPc2	dřevina
<g/>
.	.	kIx.	.
</s>
<s>
Mívá	mívat	k5eAaImIp3nS	mívat
buď	buď	k8xC	buď
parkovou	parkový	k2eAgFnSc4d1	parková
úpravu	úprava	k1gFnSc4	úprava
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
výsadba	výsadba	k1gFnSc1	výsadba
prováděna	provádět	k5eAaImNgFnS	provádět
podobně	podobně	k6eAd1	podobně
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
u	u	k7c2	u
zakládaných	zakládaný	k2eAgInPc2d1	zakládaný
lesních	lesní	k2eAgInPc2d1	lesní
porostů	porost	k1gInPc2	porost
<g/>
,	,	kIx,	,
např.	např.	kA	např.
u	u	k7c2	u
arboret	arboretum	k1gNnPc2	arboretum
sloužících	sloužící	k2eAgNnPc2d1	sloužící
k	k	k7c3	k
výzkumným	výzkumný	k2eAgInPc3d1	výzkumný
lesnickým	lesnický	k2eAgInPc3d1	lesnický
účelům	účel	k1gInPc3	účel
<g/>
.	.	kIx.	.
</s>
<s>
Arboretum	arboretum	k1gNnSc1	arboretum
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
specializované	specializovaný	k2eAgNnSc1d1	specializované
na	na	k7c4	na
jednotlivé	jednotlivý	k2eAgInPc4d1	jednotlivý
rody	rod	k1gInPc4	rod
(	(	kIx(	(
<g/>
čeledi	čeleď	k1gFnSc2	čeleď
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
dřeviny	dřevina	k1gFnPc4	dřevina
určitého	určitý	k2eAgNnSc2d1	určité
území	území	k1gNnSc2	území
nebo	nebo	k8xC	nebo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
všeobecné	všeobecný	k2eAgNnSc1d1	všeobecné
<g/>
,	,	kIx,	,
zaměřené	zaměřený	k2eAgNnSc1d1	zaměřené
na	na	k7c4	na
všechny	všechen	k3xTgFnPc4	všechen
dřeviny	dřevina	k1gFnPc4	dřevina
bez	bez	k7c2	bez
rozdílu	rozdíl	k1gInSc2	rozdíl
<g/>
.	.	kIx.	.
</s>
<s>
Arboreta	arboretum	k1gNnPc1	arboretum
vědeckovýzkumná	vědeckovýzkumný	k2eAgNnPc1d1	vědeckovýzkumné
kladou	klást	k5eAaImIp3nP	klást
důraz	důraz	k1gInSc4	důraz
především	především	k9	především
na	na	k7c4	na
přesný	přesný	k2eAgInSc4d1	přesný
původ	původ	k1gInSc4	původ
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc7	jejich
dokonalou	dokonalý	k2eAgFnSc7d1	dokonalá
evidenci	evidence	k1gFnSc4	evidence
<g/>
,	,	kIx,	,
pravidelné	pravidelný	k2eAgNnSc4d1	pravidelné
sledování	sledování	k1gNnSc4	sledování
a	a	k8xC	a
vyhodnocování	vyhodnocování	k1gNnSc4	vyhodnocování
<g/>
.	.	kIx.	.
</s>
<s>
Arboreta	arboretum	k1gNnPc1	arboretum
okrasného	okrasný	k2eAgInSc2d1	okrasný
typu	typ	k1gInSc2	typ
kladou	klást	k5eAaImIp3nP	klást
důraz	důraz	k1gInSc4	důraz
především	především	k9	především
na	na	k7c4	na
zahradní	zahradní	k2eAgFnSc4d1	zahradní
architekturu	architektura	k1gFnSc4	architektura
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
odborně	odborně	k6eAd1	odborně
méně	málo	k6eAd2	málo
náročná	náročný	k2eAgFnSc1d1	náročná
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
rozšířená	rozšířený	k2eAgNnPc1d1	rozšířené
jsou	být	k5eAaImIp3nP	být
arboreta	arboretum	k1gNnPc4	arboretum
při	při	k7c6	při
různých	různý	k2eAgFnPc6d1	různá
odborných	odborný	k2eAgFnPc6d1	odborná
školách	škola	k1gFnPc6	škola
<g/>
:	:	kIx,	:
zde	zde	k6eAd1	zde
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
didaktická	didaktický	k2eAgFnSc1d1	didaktická
pomůcka	pomůcka	k1gFnSc1	pomůcka
při	při	k7c6	při
výuce	výuka	k1gFnSc6	výuka
a	a	k8xC	a
poznávání	poznávání	k1gNnSc6	poznávání
cizích	cizí	k2eAgInPc2d1	cizí
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
arboretum	arboretum	k1gNnSc4	arboretum
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
použit	použít	k5eAaPmNgInS	použít
Johnem	John	k1gMnSc7	John
Claudiem	Claudium	k1gNnSc7	Claudium
Loudonem	Loudon	k1gMnSc7	Loudon
v	v	k7c6	v
anglické	anglický	k2eAgFnSc6d1	anglická
publikaci	publikace	k1gFnSc6	publikace
Gardener	Gardener	k1gInSc1	Gardener
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Magazine	Magazin	k1gInSc5	Magazin
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
samotná	samotný	k2eAgFnSc1d1	samotná
koncepce	koncepce	k1gFnSc1	koncepce
existovala	existovat	k5eAaImAgFnS	existovat
již	již	k6eAd1	již
dávno	dávno	k6eAd1	dávno
předtím	předtím	k6eAd1	předtím
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgNnSc7	první
arboretem	arboretum	k1gNnSc7	arboretum
bylo	být	k5eAaImAgNnS	být
arboretum	arboretum	k1gNnSc1	arboretum
Trsteno	Trsten	k2eAgNnSc1d1	Trsteno
blízko	blízko	k7c2	blízko
Dubrovníku	Dubrovník	k1gInSc2	Dubrovník
v	v	k7c6	v
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Datum	datum	k1gInSc4	datum
jeho	on	k3xPp3gNnSc2	on
založení	založení	k1gNnSc2	založení
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ví	vědět	k5eAaImIp3nS	vědět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
existovalo	existovat	k5eAaImAgNnS	existovat
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1492	[number]	k4	1492
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
jeho	jeho	k3xOp3gNnSc4	jeho
zavlažování	zavlažování	k1gNnSc4	zavlažování
vybudován	vybudován	k2eAgInSc1d1	vybudován
akvadukt	akvadukt	k1gInSc1	akvadukt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
dosud	dosud	k6eAd1	dosud
používá	používat	k5eAaImIp3nS	používat
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Tradice	tradice	k1gFnSc1	tradice
zakládání	zakládání	k1gNnSc2	zakládání
arboret	arboretum	k1gNnPc2	arboretum
spadá	spadat	k5eAaImIp3nS	spadat
asi	asi	k9	asi
do	do	k7c2	do
období	období	k1gNnSc2	období
renesance	renesance	k1gFnSc2	renesance
<g/>
.	.	kIx.	.
</s>
<s>
Tou	ten	k3xDgFnSc7	ten
dobou	doba	k1gFnSc7	doba
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
v	v	k7c6	v
zámeckých	zámecký	k2eAgInPc6d1	zámecký
parcích	park	k1gInPc6	park
objevovat	objevovat	k5eAaImF	objevovat
první	první	k4xOgInPc1	první
exotické	exotický	k2eAgInPc1d1	exotický
druhy	druh	k1gInPc1	druh
dřevin	dřevina	k1gFnPc2	dřevina
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc1	jejich
sbírky	sbírka	k1gFnPc1	sbírka
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
rychle	rychle	k6eAd1	rychle
rozšiřovaly	rozšiřovat	k5eAaImAgFnP	rozšiřovat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
sbírek	sbírka	k1gFnPc2	sbírka
se	se	k3xPyFc4	se
však	však	k9	však
jen	jen	k9	jen
málokteré	málokterý	k3yIgFnPc1	málokterý
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
do	do	k7c2	do
dnešních	dnešní	k2eAgFnPc2d1	dnešní
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
současných	současný	k2eAgNnPc2d1	současné
arboret	arboretum	k1gNnPc2	arboretum
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
V	v	k7c6	v
závorce	závorka	k1gFnSc6	závorka
je	být	k5eAaImIp3nS	být
uveden	uveden	k2eAgInSc1d1	uveden
okres	okres	k1gInSc1	okres
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Americká	americký	k2eAgFnSc1d1	americká
zahrada	zahrada	k1gFnSc1	zahrada
(	(	kIx(	(
<g/>
Klatovy	Klatovy	k1gInPc1	Klatovy
<g/>
)	)	kIx)	)
Arboretum	arboretum	k1gNnSc1	arboretum
Bukovina	Bukovina	k1gFnSc1	Bukovina
(	(	kIx(	(
<g/>
Semily	Semily	k1gInPc1	Semily
<g/>
)	)	kIx)	)
Arboretum	arboretum	k1gNnSc1	arboretum
Borotín	Borotína	k1gFnPc2	Borotína
(	(	kIx(	(
<g/>
Blansko	Blansko	k1gNnSc1	Blansko
<g/>
)	)	kIx)	)
Arboretum	arboretum	k1gNnSc4	arboretum
Bílá	bílý	k2eAgFnSc1d1	bílá
Lhota	Lhota	k1gFnSc1	Lhota
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
)	)	kIx)	)
Arboretum	arboretum	k1gNnSc1	arboretum
ČZU	ČZU	kA	ČZU
v	v	k7c6	v
Kostelci	Kostelec	k1gInSc6	Kostelec
nad	nad	k7c7	nad
Černými	černý	k2eAgInPc7d1	černý
lesy	les	k1gInPc7	les
Arboretum	arboretum	k1gNnSc1	arboretum
Šmelcovna	šmelcovna	k1gFnSc1	šmelcovna
Boskovice	Boskovice	k1gInPc1	Boskovice
(	(	kIx(	(
<g/>
Blansko	Blansko	k1gNnSc1	Blansko
<g/>
)	)	kIx)	)
Arboretum	arboretum	k1gNnSc1	arboretum
Děčín-Libverda	Děčín-Libverda	k1gFnSc1	Děčín-Libverda
(	(	kIx(	(
<g/>
Děčín	Děčín	k1gInSc1	Děčín
<g/>
)	)	kIx)	)
Arboretum	arboretum	k1gNnSc1	arboretum
Sofronka	Sofronka	k1gFnSc1	Sofronka
<g/>
,	,	kIx,	,
Plzeň-Bolevec	Plzeň-Bolevec	k1gInSc1	Plzeň-Bolevec
(	(	kIx(	(
<g/>
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
)	)	kIx)	)
Arboretum	arboretum	k1gNnSc1	arboretum
Střední	střední	k2eAgFnSc2d1	střední
lesnické	lesnický	k2eAgFnSc2d1	lesnická
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Hranicích	Hranice	k1gFnPc6	Hranice
(	(	kIx(	(
<g/>
Přerov	Přerov	k1gInSc4	Přerov
<g/>
)	)	kIx)	)
Arboretum	arboretum	k1gNnSc4	arboretum
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
<g />
.	.	kIx.	.
</s>
<s>
Chvojno	Chvojno	k1gNnSc1	Chvojno
(	(	kIx(	(
<g/>
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
)	)	kIx)	)
Botanická	botanický	k2eAgFnSc1d1	botanická
zahrada	zahrada	k1gFnSc1	zahrada
a	a	k8xC	a
arboretum	arboretum	k1gNnSc1	arboretum
Mendelovy	Mendelův	k2eAgFnSc2d1	Mendelova
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
a	a	k8xC	a
lesnické	lesnický	k2eAgFnSc2d1	lesnická
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
(	(	kIx(	(
<g/>
Brno	Brno	k1gNnSc1	Brno
<g/>
)	)	kIx)	)
Arboretum	arboretum	k1gNnSc1	arboretum
Habrůvka	Habrůvka	k1gFnSc1	Habrůvka
(	(	kIx(	(
<g/>
Blansko	Blansko	k1gNnSc1	Blansko
<g/>
)	)	kIx)	)
Arboretum	arboretum	k1gNnSc1	arboretum
Pardubice	Pardubice	k1gInPc1	Pardubice
(	(	kIx(	(
<g/>
Pardubice	Pardubice	k1gInPc1	Pardubice
<g/>
)	)	kIx)	)
Arboretum	arboretum	k1gNnSc1	arboretum
Řícmanice	Řícmanice	k1gFnSc2	Řícmanice
(	(	kIx(	(
<g/>
Brno-venkov	Brnoenkov	k1gInSc4	Brno-venkov
<g/>
)	)	kIx)	)
Arboretum	arboretum	k1gNnSc4	arboretum
Křtiny	křtiny	k1gFnPc1	křtiny
(	(	kIx(	(
<g/>
Blansko	Blansko	k1gNnSc1	Blansko
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Hvězdárna	hvězdárna	k1gFnSc1	hvězdárna
a	a	k8xC	a
arboretum	arboretum	k1gNnSc1	arboretum
Ondřejov	Ondřejov	k1gInSc1	Ondřejov
(	(	kIx(	(
<g/>
Praha-východ	Prahaýchod	k1gInSc1	Praha-východ
<g/>
)	)	kIx)	)
Královská	královský	k2eAgFnSc1d1	královská
obora	obora	k1gFnSc1	obora
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
7	[number]	k4	7
<g/>
)	)	kIx)	)
Arboretum	arboretum	k1gNnSc4	arboretum
Nový	nový	k2eAgInSc1d1	nový
Dvůr	Dvůr	k1gInSc1	Dvůr
(	(	kIx(	(
<g/>
Opava	Opava	k1gFnSc1	Opava
<g/>
)	)	kIx)	)
Dendrologická	dendrologický	k2eAgFnSc1d1	Dendrologická
zahrada	zahrada	k1gFnSc1	zahrada
Průhonice	Průhonice	k1gFnPc1	Průhonice
(	(	kIx(	(
<g/>
Praha-západ	Prahaápad	k1gInSc1	Praha-západ
<g/>
)	)	kIx)	)
Zámecký	zámecký	k2eAgInSc4d1	zámecký
park	park	k1gInSc4	park
Průhonice	Průhonice	k1gFnPc4	Průhonice
(	(	kIx(	(
<g/>
Praha-západ	Prahaápad	k1gInSc1	Praha-západ
<g/>
)	)	kIx)	)
Arboretum	arboretum	k1gNnSc1	arboretum
Žampach	Žampacha	k1gFnPc2	Žampacha
(	(	kIx(	(
<g/>
Ústí	ústí	k1gNnSc1	ústí
nad	nad	k7c7	nad
Orlicí	Orlice	k1gFnSc7	Orlice
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Arboretum	arboretum	k1gNnSc1	arboretum
Burgholtz	Burgholtz	k1gInSc1	Burgholtz
(	(	kIx(	(
<g/>
Německo	Německo	k1gNnSc1	Německo
<g/>
)	)	kIx)	)
Arboretum	arboretum	k1gNnSc1	arboretum
Mlyňany	Mlyňan	k1gMnPc4	Mlyňan
(	(	kIx(	(
<g/>
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
)	)	kIx)	)
Westonbirt	Westonbirt	k1gInSc1	Westonbirt
Arboretum	arboretum	k1gNnSc1	arboretum
(	(	kIx(	(
<g/>
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
)	)	kIx)	)
Udhagamandalam	Udhagamandalam	k1gInSc1	Udhagamandalam
(	(	kIx(	(
<g/>
Ooty	Oota	k1gFnPc1	Oota
<g/>
)	)	kIx)	)
Arboretum	arboretum	k1gNnSc1	arboretum
(	(	kIx(	(
<g/>
Indie	Indie	k1gFnSc1	Indie
<g/>
)	)	kIx)	)
Abney	Abnea	k1gFnSc2	Abnea
Park	park	k1gInSc1	park
Arboretum	arboretum	k1gNnSc4	arboretum
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
(	(	kIx(	(
<g/>
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
)	)	kIx)	)
Kew	Kew	k1gFnSc1	Kew
Gardens	Gardens	k1gInSc1	Gardens
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
(	(	kIx(	(
<g/>
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
)	)	kIx)	)
</s>
