<s>
Arboretum	arboretum	k1gNnSc1	arboretum
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
arbor	arbor	k1gInSc1	arbor
=	=	kIx~	=
strom	strom	k1gInSc1	strom
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
sbírka	sbírka	k1gFnSc1	sbírka
živých	živý	k2eAgFnPc2d1	živá
dřevin	dřevina	k1gFnPc2	dřevina
-	-	kIx~	-
dendrologická	dendrologický	k2eAgFnSc1d1	Dendrologická
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
botanická	botanický	k2eAgFnSc1d1	botanická
zahrada	zahrada	k1gFnSc1	zahrada
specializující	specializující	k2eAgFnSc1d1	specializující
se	se	k3xPyFc4	se
na	na	k7c4	na
výzkum	výzkum	k1gInSc4	výzkum
a	a	k8xC	a
pěstění	pěstění	k1gNnSc4	pěstění
dřevin	dřevina	k1gFnPc2	dřevina
<g/>
.	.	kIx.	.
</s>
