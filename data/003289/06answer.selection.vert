<s>
Jejich	jejich	k3xOp3gNnSc4	jejich
další	další	k2eAgNnSc4d1	další
album	album	k1gNnSc4	album
Sin	sin	kA	sin
After	Aftero	k1gNnPc2	Aftero
Sin	sin	kA	sin
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1977	[number]	k4	1977
bylo	být	k5eAaImAgNnS	být
prvním	první	k4xOgMnPc3	první
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
Judas	Judas	k1gMnSc1	Judas
Priest	Priest	k1gMnSc1	Priest
nahráli	nahrát	k5eAaPmAgMnP	nahrát
pod	pod	k7c7	pod
velkou	velký	k2eAgFnSc7d1	velká
nahrávací	nahrávací	k2eAgFnSc7d1	nahrávací
společností	společnost	k1gFnSc7	společnost
-	-	kIx~	-
CBS	CBS	kA	CBS
a	a	k8xC	a
první	první	k4xOgInSc4	první
z	z	k7c2	z
jedenácti	jedenáct	k4xCc2	jedenáct
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
jdoucích	jdoucí	k2eAgInPc6d1	jdoucí
albech	album	k1gNnPc6	album
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
dostalo	dostat	k5eAaPmAgNnS	dostat
certifikaci	certifikace	k1gFnSc4	certifikace
Gold	Gold	k1gInSc4	Gold
nebo	nebo	k8xC	nebo
ještě	ještě	k6eAd1	ještě
vyšší	vysoký	k2eAgNnSc4d2	vyšší
(	(	kIx(	(
<g/>
hodnocení	hodnocení	k1gNnSc4	hodnocení
RIAA	RIAA	kA	RIAA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
