<s>
Ze	z	k7c2	z
severu	sever	k1gInSc2	sever
jej	on	k3xPp3gMnSc4	on
ohraničuje	ohraničovat	k5eAaImIp3nS	ohraničovat
Severní	severní	k2eAgInSc1d1	severní
ledový	ledový	k2eAgInSc1d1	ledový
oceán	oceán	k1gInSc1	oceán
<g/>
,	,	kIx,	,
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
Atlantský	atlantský	k2eAgInSc1d1	atlantský
oceán	oceán	k1gInSc1	oceán
<g/>
,	,	kIx,	,
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
Středozemní	středozemní	k2eAgInPc4d1	středozemní
a	a	k8xC	a
Černé	Černé	k2eAgNnSc4d1	Černé
moře	moře	k1gNnSc4	moře
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vodními	vodní	k2eAgFnPc7d1	vodní
cestami	cesta	k1gFnPc7	cesta
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
je	on	k3xPp3gMnPc4	on
spojují	spojovat	k5eAaImIp3nP	spojovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
z	z	k7c2	z
východu	východ	k1gInSc2	východ
Asie	Asie	k1gFnSc2	Asie
(	(	kIx(	(
<g/>
o	o	k7c6	o
přesném	přesný	k2eAgInSc6d1	přesný
průběhu	průběh	k1gInSc6	průběh
této	tento	k3xDgFnSc2	tento
části	část	k1gFnSc2	část
hranice	hranice	k1gFnSc2	hranice
nepanuje	panovat	k5eNaImIp3nS	panovat
konsenzus	konsenzus	k1gInSc1	konsenzus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
