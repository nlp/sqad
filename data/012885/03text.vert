<p>
<s>
Trejv	Trejv	k1gInSc1	Trejv
je	být	k5eAaImIp3nS	být
povrchové	povrchový	k2eAgNnSc4d1	povrchové
zařízení	zařízení	k1gNnSc4	zařízení
středověkých	středověký	k2eAgInPc2d1	středověký
dolů	dol	k1gInPc2	dol
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
těžní	těžní	k2eAgInSc4d1	těžní
stroj	stroj	k1gInSc4	stroj
<g/>
,	,	kIx,	,
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
vrátek	vrátek	k1gInSc4	vrátek
<g/>
,	,	kIx,	,
na	na	k7c4	na
převážně	převážně	k6eAd1	převážně
koňský	koňský	k2eAgInSc4d1	koňský
pohon	pohon	k1gInSc4	pohon
pomocí	pomocí	k7c2	pomocí
žentouru	žentour	k1gInSc2	žentour
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgInPc1d2	menší
vrátky	vrátek	k1gInPc1	vrátek
mohly	moct	k5eAaImAgInP	moct
být	být	k5eAaImF	být
poháněna	pohánět	k5eAaImNgFnS	pohánět
lidskou	lidský	k2eAgFnSc7d1	lidská
silou	síla	k1gFnSc7	síla
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
zařízení	zařízení	k1gNnSc4	zařízení
na	na	k7c4	na
dopravu	doprava	k1gFnSc4	doprava
materiálu	materiál	k1gInSc2	materiál
z	z	k7c2	z
dolu	dol	k1gInSc2	dol
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Jáchymovský	jáchymovský	k2eAgInSc1d1	jáchymovský
trejv	trejv	k1gInSc1	trejv
==	==	k?	==
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
trejv	trejv	k1gInSc4	trejv
s	s	k7c7	s
dochovaným	dochovaný	k2eAgNnSc7d1	dochované
dřevěným	dřevěný	k2eAgNnSc7d1	dřevěné
soustrojím	soustrojí	k1gNnSc7	soustrojí
<g/>
,	,	kIx,	,
postavený	postavený	k2eAgInSc1d1	postavený
v	v	k7c6	v
šestnáctém	šestnáctý	k4xOgNnSc6	šestnáctý
století	století	k1gNnSc6	století
při	při	k7c6	při
dole	dol	k1gInSc6	dol
Helena	Helena	k1gFnSc1	Helena
Huber	Huber	k1gInSc1	Huber
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
Josef	Josef	k1gMnSc1	Josef
<g/>
)	)	kIx)	)
v	v	k7c6	v
Jáchymově	Jáchymov	k1gInSc6	Jáchymov
<g/>
.	.	kIx.	.
</s>
<s>
Používán	používat	k5eAaImNgInS	používat
byl	být	k5eAaImAgInS	být
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1901	[number]	k4	1901
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
byl	být	k5eAaImAgInS	být
důl	důl	k1gInSc1	důl
dočasně	dočasně	k6eAd1	dočasně
zasypán	zasypán	k2eAgInSc1d1	zasypán
(	(	kIx(	(
<g/>
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Trejv	Trejv	k1gMnSc1	Trejv
zde	zde	k6eAd1	zde
zůstal	zůstat	k5eAaPmAgMnS	zůstat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1948	[number]	k4	1948
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
ohrožen	ohrozit	k5eAaPmNgInS	ohrozit
těžbou	těžba	k1gFnSc7	těžba
uranu	uran	k1gInSc2	uran
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
byl	být	k5eAaImAgInS	být
rozebrán	rozebrat	k5eAaPmNgInS	rozebrat
a	a	k8xC	a
instalován	instalovat	k5eAaBmNgInS	instalovat
roku	rok	k1gInSc3	rok
1949	[number]	k4	1949
v	v	k7c6	v
Kutné	kutný	k2eAgFnSc6d1	Kutná
Hoře	hora	k1gFnSc6	hora
ve	v	k7c6	v
venkovní	venkovní	k2eAgFnSc6d1	venkovní
expozici	expozice	k1gFnSc6	expozice
Muzea	muzeum	k1gNnSc2	muzeum
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Pohon	pohon	k1gInSc1	pohon
zajišťovaly	zajišťovat	k5eAaImAgFnP	zajišťovat
čtyři	čtyři	k4xCgFnPc1	čtyři
páry	pára	k1gFnPc1	pára
koní	kůň	k1gMnPc2	kůň
<g/>
.	.	kIx.	.
</s>
<s>
Dokázal	dokázat	k5eAaPmAgMnS	dokázat
vyzdvihnout	vyzdvihnout	k5eAaPmF	vyzdvihnout
až	až	k9	až
tunu	tuna	k1gFnSc4	tuna
nákladu	náklad	k1gInSc2	náklad
z	z	k7c2	z
hloubky	hloubka	k1gFnSc2	hloubka
250	[number]	k4	250
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
