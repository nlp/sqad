<s>
Seznam	seznam	k1gInSc1
nemateriálních	materiální	k2eNgInPc2d1
statků	statek	k1gInPc2
tradiční	tradiční	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Seznam	seznam	k1gInSc4
nemateriálních	materiální	k2eNgInPc2d1
statků	statek	k1gInPc2
tradiční	tradiční	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
vede	vést	k5eAaImIp3nS
Národní	národní	k2eAgInSc1d1
ústav	ústav	k1gInSc1
lidové	lidový	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
(	(	kIx(
<g/>
NÚLK	NÚLK	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
hlavním	hlavní	k2eAgInSc7d1
účelem	účel	k1gInSc7
je	být	k5eAaImIp3nS
ochrana	ochrana	k1gFnSc1
<g/>
,	,	kIx,
zachování	zachování	k1gNnSc1
<g/>
,	,	kIx,
identifikace	identifikace	k1gFnSc1
<g/>
,	,	kIx,
rozvoj	rozvoj	k1gInSc1
a	a	k8xC
podpora	podpora	k1gFnSc1
nemateriálního	materiální	k2eNgNnSc2d1
kulturního	kulturní	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
na	na	k7c6
území	území	k1gNnSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zaměřuje	zaměřovat	k5eAaImIp3nS
se	se	k3xPyFc4
především	především	k9
na	na	k7c4
jevy	jev	k1gInPc4
tradiční	tradiční	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
venkovských	venkovský	k2eAgFnPc2d1
komunit	komunita	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgInS
zřízen	zřídit	k5eAaPmNgInS
příkazem	příkaz	k1gInSc7
ministra	ministr	k1gMnSc2
č.	č.	k?
41	#num#	k4
<g/>
/	/	kIx~
<g/>
2008	#num#	k4
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
usnesením	usnesení	k1gNnSc7
vlády	vláda	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
ze	z	k7c2
dne	den	k1gInSc2
11	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2003	#num#	k4
č.	č.	k?
571	#num#	k4
ke	k	k7c3
Koncepci	koncepce	k1gFnSc3
účinnější	účinný	k2eAgFnSc2d2
péče	péče	k1gFnSc2
o	o	k7c4
tradiční	tradiční	k2eAgFnSc4d1
lidovou	lidový	k2eAgFnSc4d1
kulturu	kultura	k1gFnSc4
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
(	(	kIx(
<g/>
bod	bod	k1gInSc1
4.2	4.2	k4
<g/>
.5	.5	k4
<g/>
.	.	kIx.
<g/>
)	)	kIx)
a	a	k8xC
k	k	k7c3
implementaci	implementace	k1gFnSc3
Úmluvy	úmluva	k1gFnSc2
o	o	k7c6
zachování	zachování	k1gNnSc6
nemateriálního	materiální	k2eNgNnSc2d1
kulturního	kulturní	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc7
smluvní	smluvní	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
se	se	k3xPyFc4
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
stala	stát	k5eAaPmAgFnS
dne	den	k1gInSc2
18	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
novelizován	novelizovat	k5eAaBmNgMnS
příkazem	příkaz	k1gInSc7
ministryně	ministryně	k1gFnSc1
č.	č.	k?
45	#num#	k4
<g/>
/	/	kIx~
<g/>
2012	#num#	k4
v	v	k7c6
souladu	soulad	k1gInSc6
s	s	k7c7
usnesením	usnesení	k1gNnSc7
vlády	vláda	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
ze	z	k7c2
dne	den	k1gInSc2
5	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2011	#num#	k4
č.	č.	k?
11	#num#	k4
ke	k	k7c3
Koncepci	koncepce	k1gFnSc3
účinnější	účinný	k2eAgFnSc2d2
péče	péče	k1gFnSc2
o	o	k7c4
tradiční	tradiční	k2eAgFnSc4d1
lidovou	lidový	k2eAgFnSc4d1
kulturu	kultura	k1gFnSc4
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
a	a	k8xC
v	v	k7c6
rámci	rámec	k1gInSc6
implementace	implementace	k1gFnSc2
Úmluvy	úmluva	k1gFnSc2
o	o	k7c6
zachování	zachování	k1gNnSc6
nemateriálního	materiální	k2eNgNnSc2d1
kulturního	kulturní	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
odvoláním	odvolání	k1gNnSc7
na	na	k7c4
příkaz	příkaz	k1gInSc4
ministra	ministr	k1gMnSc2
č.	č.	k?
41	#num#	k4
ze	z	k7c2
dne	den	k1gInSc2
22	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2008	#num#	k4
a	a	k8xC
na	na	k7c4
Úmluvu	úmluva	k1gFnSc4
o	o	k7c6
zachování	zachování	k1gNnSc6
nemateriálního	materiální	k2eNgNnSc2d1
kulturního	kulturní	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
vydal	vydat	k5eAaPmAgInS
odbor	odbor	k1gInSc1
regionální	regionální	k2eAgFnSc2d1
a	a	k8xC
národnostní	národnostní	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
Ministerstva	ministerstvo	k1gNnSc2
kultury	kultura	k1gFnSc2
metodický	metodický	k2eAgInSc4d1
pokyn	pokyn	k1gInSc4
pro	pro	k7c4
vedení	vedení	k1gNnSc4
tohoto	tento	k3xDgInSc2
Seznamu	seznam	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Jedna	jeden	k4xCgFnSc1
z	z	k7c2
podmínek	podmínka	k1gFnPc2
pro	pro	k7c4
zapsaní	zapsaný	k2eAgMnPc1d1
na	na	k7c6
seznamu	seznam	k1gInSc6
ústního	ústní	k2eAgNnSc2d1
a	a	k8xC
nehmotného	hmotný	k2eNgNnSc2d1
dědictví	dědictví	k1gNnSc2
UNESCO	Unesco	k1gNnSc1
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
kulturní	kulturní	k2eAgInSc1d1
statek	statek	k1gInSc1
byl	být	k5eAaImAgInS
na	na	k7c6
Seznamu	seznam	k1gInSc6
nemateriálních	materiální	k2eNgInPc2d1
statků	statek	k1gInPc2
tradiční	tradiční	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Aktuální	aktuální	k2eAgInPc1d1
statky	statek	k1gInPc1
</s>
<s>
Pořadové	pořadový	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
položky	položka	k1gFnSc2
</s>
<s>
NÚLK	NÚLK	kA
</s>
<s>
Název	název	k1gInSc1
statku	statek	k1gInSc2
</s>
<s>
Objednavatel	objednavatel	k1gMnSc1
</s>
<s>
Vyhlášeno	vyhlášen	k2eAgNnSc1d1
</s>
<s>
Obrázek	obrázek	k1gInSc1
</s>
<s>
Commons	Commons	k6eAd1
</s>
<s>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
<g/>
Slovácký	slovácký	k2eAgInSc1d1
verbuňkNárodní	verbuňkNárodní	k2eAgInSc1d1
ústav	ústav	k1gInSc1
lidové	lidový	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
ve	v	k7c6
Strážnici	Strážnice	k1gFnSc6
<g/>
2009	#num#	k4
<g/>
Kategorie	kategorie	k1gFnSc2
„	„	k?
<g/>
Prague	Prague	k1gInSc1
Castle	Castle	k1gFnSc1
<g/>
“	“	k?
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
<g/>
Vesnické	vesnický	k2eAgFnPc1d1
masopustní	masopustní	k2eAgFnPc1d1
obchůzky	obchůzka	k1gFnPc1
a	a	k8xC
masky	maska	k1gFnPc1
na	na	k7c6
HlineckuNPÚ	HlineckuNPÚ	k1gFnSc6
–	–	k?
SLS	SLS	kA
Vysočina	vysočina	k1gFnSc1
<g/>
,	,	kIx,
Hlinsko	Hlinsko	k1gNnSc1
<g/>
2009	#num#	k4
<g/>
Kategorie	kategorie	k1gFnSc2
„	„	k?
<g/>
Prague	Prague	k1gInSc1
Castle	Castle	k1gFnSc1
<g/>
“	“	k?
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
<g/>
Jízdy	jízda	k1gFnSc2
králů	král	k1gMnPc2
na	na	k7c4
SlováckuNárodní	SlováckuNárodní	k2eAgInSc4d1
ústav	ústav	k1gInSc4
lidové	lidový	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
ve	v	k7c6
Strážnici	Strážnice	k1gFnSc6
<g/>
2009	#num#	k4
<g/>
Kategorie	kategorie	k1gFnSc2
„	„	k?
<g/>
Ride	Rid	k1gFnSc2
of	of	k?
the	the	k?
Kings	Kings	k1gInSc1
<g/>
“	“	k?
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
<g/>
Sokolnictví	sokolnictví	k1gNnSc2
–	–	k?
umění	umění	k1gNnSc1
chovu	chov	k1gInSc2
dravců	dravec	k1gMnPc2
<g/>
,	,	kIx,
jejich	jejich	k3xOp3gFnSc2
ochrany	ochrana	k1gFnSc2
<g/>
,	,	kIx,
výcviku	výcvik	k1gInSc2
a	a	k8xC
lovu	lov	k1gInSc2
s	s	k7c7
nimiMinisterstvo	nimiMinisterstvo	k1gNnSc1
zemědělství	zemědělství	k1gNnSc2
<g/>
2009	#num#	k4
<g/>
Kategorie	kategorie	k1gFnSc2
„	„	k?
<g/>
Falconry	Falconra	k1gFnSc2
<g/>
“	“	k?
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
2011	#num#	k4
<g/>
Myslivost	myslivost	k1gFnSc1
–	–	k?
plánovitě	plánovitě	k6eAd1
trvale	trvale	k6eAd1
udržitelné	udržitelný	k2eAgNnSc1d1
obhospodařování	obhospodařování	k1gNnSc1
zvěře	zvěř	k1gFnSc2
a	a	k8xC
jejího	její	k3xOp3gNnSc2
prostředí	prostředí	k1gNnSc2
jako	jako	k8xC,k8xS
přirozená	přirozený	k2eAgFnSc1d1
součást	součást	k1gFnSc1
života	život	k1gInSc2
na	na	k7c4
venkověČeskomoravská	venkověČeskomoravský	k2eAgFnSc1d1
myslivecká	myslivecký	k2eAgFnSc1d1
jednota	jednota	k1gFnSc1
<g/>
2011	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2012	#num#	k4
<g/>
Valašský	valašský	k2eAgMnSc1d1
odzemekValašské	odzemekValašský	k2eAgNnSc4d1
muzeum	muzeum	k1gNnSc4
v	v	k7c6
přírodě	příroda	k1gFnSc6
v	v	k7c6
Rožnově	Rožnov	k1gInSc6
pod	pod	k7c7
Radhoštěm	Radhošť	k1gInSc7
<g/>
2012	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
2012	#num#	k4
<g/>
Vodění	vodění	k1gNnSc6
jidášeNárodní	jidášeNárodní	k2eAgInSc4d1
památkový	památkový	k2eAgInSc4d1
ústav	ústav	k1gInSc4
<g/>
,	,	kIx,
ú.o.	ú.o.	k?
<g/>
p.	p.	k?
Pardubice	Pardubice	k1gInPc1
<g/>
2012	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
2012	#num#	k4
<g/>
Východočeské	východočeský	k2eAgFnSc2d1
loutkářstvíKrálovéhradecký	loutkářstvíKrálovéhradecký	k2eAgInSc4d1
kraj	kraj	k1gInSc4
<g/>
,	,	kIx,
Pardubický	pardubický	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
2012	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
2013	#num#	k4
<g/>
Běh	běh	k1gInSc1
o	o	k7c4
BarchanMěsto	BarchanMěsta	k1gMnSc5
Jemnice	Jemnice	k1gFnPc5
<g/>
2013	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
/	/	kIx~
<g/>
2014	#num#	k4
<g/>
Technologie	technologie	k1gFnSc2
výroby	výroba	k1gFnSc2
modrotiskuNárodní	modrotiskuNárodní	k2eAgInSc4d1
ústav	ústav	k1gInSc4
lidové	lidový	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
ve	v	k7c6
Strážnici	Strážnice	k1gFnSc6
<g/>
2014	#num#	k4
<g/>
Kategorie	kategorie	k1gFnSc2
„	„	k?
<g/>
Blue	Blue	k1gInSc1
printing	printing	k1gInSc1
<g/>
“	“	k?
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
11	#num#	k4
<g/>
/	/	kIx~
<g/>
2014	#num#	k4
<g/>
Tradiční	tradiční	k2eAgFnPc1d1
léčebné	léčebný	k2eAgFnPc1d1
procedury	procedura	k1gFnPc1
a	a	k8xC
odkaz	odkaz	k1gInSc1
V.	V.	kA
PriessnitzeOlomoucký	PriessnitzeOlomoucký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
2014	#num#	k4
<g/>
Kategorie	kategorie	k1gFnSc2
„	„	k?
<g/>
Vincent	Vincent	k1gMnSc1
Priessnitz	Priessnitz	k1gMnSc1
<g/>
“	“	k?
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
2014	#num#	k4
<g/>
České	český	k2eAgFnSc2d1
loutkářství	loutkářství	k1gNnPc2
–	–	k?
lidové	lidový	k2eAgFnSc2d1
interpretační	interpretační	k2eAgFnSc2d1
uměníNárodní	uměníNárodní	k2eAgFnSc2d1
informační	informační	k2eAgFnSc2d1
a	a	k8xC
poradenské	poradenský	k2eAgFnSc2d1
středisko	středisko	k1gNnSc4
pro	pro	k7c4
kulturu	kultura	k1gFnSc4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
2014	#num#	k4
<g/>
Kategorie	kategorie	k1gFnSc1
„	„	k?
<g/>
Puppets	Puppets	k1gInSc1
<g/>
“	“	k?
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
13	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
<g/>
Betlémská	betlémský	k2eAgFnSc1d1
cesta	cesta	k1gFnSc1
v	v	k7c4
TřeštiMuzeum	TřeštiMuzeum	k1gInSc4
Vysočiny	vysočina	k1gFnSc2
Třebíč	Třebíč	k1gFnSc1
<g/>
2015	#num#	k4
</s>
<s>
14	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
<g/>
Lidová	lidový	k2eAgFnSc1d1
tradice	tradice	k1gFnSc1
výroby	výroba	k1gFnSc2
vánočních	vánoční	k2eAgFnPc2d1
ozdob	ozdoba	k1gFnPc2
ze	z	k7c2
skleněných	skleněný	k2eAgMnPc2d1
perličekMuzeum	perličekMuzeum	k1gInSc4
Českého	český	k2eAgInSc2d1
ráje	ráj	k1gInSc2
v	v	k7c6
Turnově	Turnov	k1gInSc6
<g/>
2015	#num#	k4
</s>
<s>
15	#num#	k4
<g/>
/	/	kIx~
<g/>
2016	#num#	k4
<g/>
Skřipácká	Skřipácký	k2eAgFnSc1d1
muzika	muzika	k1gFnSc1
na	na	k7c4
JihlavskuMuzeum	JihlavskuMuzeum	k1gInSc4
Vysočiny	vysočina	k1gFnSc2
Třebíč	Třebíč	k1gFnSc1
<g/>
2016	#num#	k4
</s>
<s>
15	#num#	k4
<g/>
/	/	kIx~
<g/>
2016	#num#	k4
<g/>
Velikonoční	velikonoční	k2eAgFnSc2d1
obchůzky	obchůzka	k1gFnSc2
s	s	k7c7
Jidáši	jidáš	k1gInPc7
na	na	k7c4
BučovickuMasarykovo	BučovickuMasarykův	k2eAgNnSc4d1
muzeum	muzeum	k1gNnSc4
v	v	k7c6
Hodoníně	Hodonín	k1gInSc6
<g/>
2016	#num#	k4
</s>
<s>
17	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
<g/>
Historický	historický	k2eAgInSc1d1
morový	morový	k2eAgInSc1d1
průvod	průvod	k1gInSc1
v	v	k7c4
BrtniciMuzeum	BrtniciMuzeum	k1gInSc4
Vysočiny	vysočina	k1gFnSc2
Třebíč	Třebíč	k1gFnSc1
<g/>
2017	#num#	k4
</s>
<s>
18	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
<g/>
Milevské	milevský	k2eAgNnSc1d1
maškaryMaškarní	maškaryMaškarní	k2eAgNnSc1d1
sdružení	sdružení	k1gNnSc1
<g/>
,	,	kIx,
spolek	spolek	k1gInSc1
pro	pro	k7c4
udržování	udržování	k1gNnSc4
tradic	tradice	k1gFnPc2
v	v	k7c6
Milevsku	Milevsko	k1gNnSc6
<g/>
2017	#num#	k4
<g/>
Kategorie	kategorie	k1gFnSc2
„	„	k?
<g/>
Masopust	masopust	k1gInSc1
masks	masks	k1gInSc1
in	in	k?
Milevsko	Milevsko	k1gNnSc1
<g/>
“	“	k?
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
19	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
<g/>
Ochotnické	ochotnický	k2eAgFnSc2d1
divadlo	divadlo	k1gNnSc4
v	v	k7c4
České	český	k2eAgFnPc4d1
republiceNárodní	republiceNárodní	k2eAgFnPc4d1
informační	informační	k2eAgFnPc4d1
a	a	k8xC
poradenské	poradenský	k2eAgNnSc1d1
středisko	středisko	k1gNnSc1
pro	pro	k7c4
kulturu	kultura	k1gFnSc4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
2017	#num#	k4
<g/>
Kategorie	kategorie	k1gFnSc1
„	„	k?
<g/>
Amateur	Amateura	k1gFnPc2
theatre	theatr	k1gInSc5
<g/>
“	“	k?
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
20	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
<g/>
Ruční	ruční	k2eAgFnSc1d1
výroba	výroba	k1gFnSc1
masopustních	masopustní	k2eAgFnPc2d1
masek	maska	k1gFnPc2
v	v	k7c4
ZákupechMuzeum	ZákupechMuzeum	k1gNnSc4
Českého	český	k2eAgInSc2d1
ráje	ráj	k1gInSc2
v	v	k7c6
Turnově	Turnov	k1gInSc6
<g/>
2017	#num#	k4
</s>
<s>
21	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
<g/>
Velikonoční	velikonoční	k2eAgFnSc2d1
slavnosti	slavnost	k1gFnSc2
Matiček	matička	k1gFnPc2
a	a	k8xC
Ježíškových	Ježíškův	k2eAgFnPc2d1
Matiček	matička	k1gFnPc2
na	na	k7c4
HanéOlomoucký	HanéOlomoucký	k2eAgInSc4d1
kraj	kraj	k1gInSc4
<g/>
2017	#num#	k4
</s>
<s>
22	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
<g/>
Tradice	tradice	k1gFnSc1
vorařství	vorařství	k1gNnSc2
na	na	k7c6
řece	řeka	k1gFnSc6
VltavěVltavan	VltavěVltavan	k1gMnSc1
Čechy	Čechy	k1gFnPc4
<g/>
2017	#num#	k4
<g/>
Kategorie	kategorie	k1gFnSc2
„	„	k?
<g/>
Timber	Timber	k1gInSc1
floating	floating	k1gInSc1
<g/>
“	“	k?
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
23	#num#	k4
<g/>
/	/	kIx~
<g/>
2018	#num#	k4
<g/>
Technologie	technologie	k1gFnPc4
pivovarnického	pivovarnický	k2eAgInSc2d1
bednářstvíPlzeňský	bednářstvíPlzeňský	k2eAgInSc1d1
Prazdroj	prazdroj	k1gInSc1
<g/>
,	,	kIx,
a.s.	a.s.	k?
<g/>
2018	#num#	k4
</s>
<s>
24	#num#	k4
<g/>
/	/	kIx~
<g/>
2018	#num#	k4
<g/>
Technologie	technologie	k1gFnSc2
tkaní	tkaní	k1gNnSc2
činovati	činovať	k1gFnSc2
na	na	k7c4
HorňáckuNárodní	HorňáckuNárodní	k2eAgInSc4d1
ústav	ústav	k1gInSc4
lidové	lidový	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
ve	v	k7c6
Strážnici	Strážnice	k1gFnSc6
<g/>
2018	#num#	k4
</s>
<s>
25	#num#	k4
<g/>
/	/	kIx~
<g/>
2018	#num#	k4
<g/>
Dudácká	dudácký	k2eAgFnSc1d1
tradice	tradice	k1gFnSc1
v	v	k7c4
ČRNárodní	ČRNárodní	k2eAgInSc4d1
ústav	ústav	k1gInSc4
lidové	lidový	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
ve	v	k7c6
Strážnici	Strážnice	k1gFnSc6
<g/>
2018	#num#	k4
<g/>
Kategorie	kategorie	k1gFnSc2
„	„	k?
<g/>
Bagpipes	Bagpipes	k1gInSc1
<g/>
“	“	k?
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
27	#num#	k4
<g/>
/	/	kIx~
<g/>
2019	#num#	k4
<g/>
Tradice	tradice	k1gFnSc1
krajkářství	krajkářství	k1gNnSc2
na	na	k7c4
VambereckuKrálovéhradecký	VambereckuKrálovéhradecký	k2eAgInSc4d1
kraj	kraj	k1gInSc4
<g/>
2019	#num#	k4
</s>
<s>
28	#num#	k4
<g/>
/	/	kIx~
<g/>
2019	#num#	k4
<g/>
Tradiční	tradiční	k2eAgInPc4d1
hody	hod	k1gInPc4
s	s	k7c7
právem	právo	k1gNnSc7
na	na	k7c4
UherskohradišťskuSlovácké	UherskohradišťskuSlovácký	k2eAgNnSc4d1
muzeum	muzeum	k1gNnSc4
<g/>
,	,	kIx,
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
<g/>
2019	#num#	k4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
nemateriálních	materiální	k2eNgInPc2d1
statků	statek	k1gInPc2
tradiční	tradiční	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
Ministerstvo	ministerstvo	k1gNnSc1
kultury	kultura	k1gFnSc2
</s>
<s>
Seznam	seznam	k1gInSc1
nemateriálních	materiální	k2eNgInPc2d1
statků	statek	k1gInPc2
tradiční	tradiční	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
Národní	národní	k2eAgInSc1d1
ústav	ústav	k1gInSc1
lidové	lidový	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
</s>
<s>
Národní	národní	k2eAgInSc1d1
seznam	seznam	k1gInSc1
<g/>
,	,	kIx,
Živý	živý	k2eAgInSc1d1
folklór	folklór	k1gInSc1
<g/>
,	,	kIx,
Lidová	lidový	k2eAgFnSc1d1
kultura	kultura	k1gFnSc1
</s>
