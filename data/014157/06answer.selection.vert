<s>
Seznam	seznam	k1gInSc4
nemateriálních	materiální	k2eNgInPc2d1
statků	statek	k1gInPc2
tradiční	tradiční	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
vede	vést	k5eAaImIp3nS
Národní	národní	k2eAgInSc1d1
ústav	ústav	k1gInSc1
lidové	lidový	k2eAgFnSc2d1
kultury	kultura	k1gFnSc2
(	(	kIx(
<g/>
NÚLK	NÚLK	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
hlavním	hlavní	k2eAgInSc7d1
účelem	účel	k1gInSc7
je	být	k5eAaImIp3nS
ochrana	ochrana	k1gFnSc1
<g/>
,	,	kIx,
zachování	zachování	k1gNnSc1
<g/>
,	,	kIx,
identifikace	identifikace	k1gFnSc1
<g/>
,	,	kIx,
rozvoj	rozvoj	k1gInSc1
a	a	k8xC
podpora	podpora	k1gFnSc1
nemateriálního	materiální	k2eNgNnSc2d1
kulturního	kulturní	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
na	na	k7c6
území	území	k1gNnSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>