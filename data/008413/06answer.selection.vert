<s>
Gene	gen	k1gInSc5	gen
Deitch	Deitch	k1gInSc1	Deitch
(	(	kIx(	(
<g/>
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
Eugene	Eugen	k1gInSc5	Eugen
Merrill	Merrilla	k1gFnPc2	Merrilla
Deitch	Deitch	k1gInSc1	Deitch
<g/>
,	,	kIx,	,
*	*	kIx~	*
8	[number]	k4	8
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
Chicago	Chicago	k1gNnSc1	Chicago
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
animovaných	animovaný	k2eAgInPc2d1	animovaný
filmů	film	k1gInPc2	film
žijící	žijící	k2eAgMnSc1d1	žijící
více	hodně	k6eAd2	hodně
než	než	k8xS	než
půl	půl	k1xP	půl
století	století	k1gNnSc2	století
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
