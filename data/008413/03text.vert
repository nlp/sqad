<p>
<s>
Gene	gen	k1gInSc5	gen
Deitch	Deitch	k1gInSc1	Deitch
(	(	kIx(	(
<g/>
celým	celý	k2eAgNnSc7d1	celé
jménem	jméno	k1gNnSc7	jméno
Eugene	Eugen	k1gInSc5	Eugen
Merrill	Merrilla	k1gFnPc2	Merrilla
Deitch	Deitch	k1gInSc1	Deitch
<g/>
,	,	kIx,	,
*	*	kIx~	*
8	[number]	k4	8
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
Chicago	Chicago	k1gNnSc1	Chicago
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
režisér	režisér	k1gMnSc1	režisér
a	a	k8xC	a
producent	producent	k1gMnSc1	producent
animovaných	animovaný	k2eAgInPc2d1	animovaný
filmů	film	k1gInPc2	film
žijící	žijící	k2eAgMnSc1d1	žijící
více	hodně	k6eAd2	hodně
než	než	k8xS	než
půl	půl	k1xP	půl
století	století	k1gNnSc2	století
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Začínal	začínat	k5eAaImAgMnS	začínat
jako	jako	k9	jako
výtvarník	výtvarník	k1gMnSc1	výtvarník
v	v	k7c6	v
hudebním	hudební	k2eAgInSc6d1	hudební
časopise	časopis	k1gInSc6	časopis
The	The	k1gMnSc1	The
Record	Record	k1gMnSc1	Record
Changer	Changer	k1gMnSc1	Changer
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
natáčel	natáčet	k5eAaImAgMnS	natáčet
kreslené	kreslený	k2eAgInPc4d1	kreslený
filmy	film	k1gInPc4	film
a	a	k8xC	a
reklamy	reklama	k1gFnPc4	reklama
pro	pro	k7c4	pro
studio	studio	k1gNnSc4	studio
United	United	k1gMnSc1	United
Productions	Productionsa	k1gFnPc2	Productionsa
of	of	k?	of
America	America	k1gMnSc1	America
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
film	film	k1gInSc1	film
Sidney	Sidnea	k1gFnSc2	Sidnea
<g/>
́	́	k?	́
<g/>
s	s	k7c7	s
Family	Famil	k1gMnPc7	Famil
Tree	Tre	k1gFnSc2	Tre
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
ho	on	k3xPp3gInSc4	on
společnost	společnost	k1gFnSc4	společnost
Rembrandt	Rembrandt	k2eAgInSc4d1	Rembrandt
Films	Films	k1gInSc4	Films
vyslala	vyslat	k5eAaPmAgFnS	vyslat
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
domluvil	domluvit	k5eAaPmAgInS	domluvit
natáčení	natáčení	k1gNnSc4	natáčení
seriálu	seriál	k1gInSc2	seriál
Tom	Tom	k1gMnSc1	Tom
a	a	k8xC	a
Jerry	Jerra	k1gFnPc1	Jerra
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
Bratři	bratr	k1gMnPc1	bratr
v	v	k7c6	v
triku	triko	k1gNnSc6	triko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
film	film	k1gInSc4	film
Munro	Munro	k1gNnSc1	Munro
<g/>
,	,	kIx,	,
satira	satira	k1gFnSc1	satira
o	o	k7c6	o
čtyřletém	čtyřletý	k2eAgMnSc6d1	čtyřletý
chlapci	chlapec	k1gMnSc6	chlapec
povolaném	povolaný	k1gMnSc6	povolaný
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
získala	získat	k5eAaPmAgFnS	získat
Oscara	Oscar	k1gMnSc4	Oscar
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
krátký	krátký	k2eAgInSc4d1	krátký
animovaný	animovaný	k2eAgInSc4d1	animovaný
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
se	se	k3xPyFc4	se
Deitch	Deitch	k1gMnSc1	Deitch
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
produkční	produkční	k2eAgFnSc7d1	produkční
Zdenkou	Zdenka	k1gFnSc7	Zdenka
Najmanovou	Najmanův	k2eAgFnSc7d1	Najmanův
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
byl	být	k5eAaImAgInS	být
hned	hned	k6eAd1	hned
dvakrát	dvakrát	k6eAd1	dvakrát
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
za	za	k7c4	za
filmy	film	k1gInPc4	film
How	How	k1gMnPc2	How
to	ten	k3xDgNnSc1	ten
Avoid	Avoida	k1gFnPc2	Avoida
Friendship	Friendship	k1gInSc1	Friendship
a	a	k8xC	a
Here	Here	k1gInSc1	Here
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Nudnik	Nudnik	k1gInSc1	Nudnik
(	(	kIx(	(
<g/>
série	série	k1gFnSc1	série
s	s	k7c7	s
postavičkou	postavička	k1gFnSc7	postavička
věčného	věčný	k2eAgMnSc2d1	věčný
poplety	popleta	k1gMnSc2	popleta
Yaramaze	Yaramaha	k1gFnSc3	Yaramaha
Nudnika	Nudnik	k1gMnSc2	Nudnik
měla	mít	k5eAaImAgFnS	mít
osm	osm	k4xCc4	osm
pokračování	pokračování	k1gNnPc2	pokračování
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1966	[number]	k4	1966
jako	jako	k8xS	jako
první	první	k4xOgFnSc7	první
zfilmoval	zfilmovat	k5eAaPmAgMnS	zfilmovat
Tolkienova	Tolkienův	k2eAgMnSc4d1	Tolkienův
Hobita	hobit	k1gMnSc4	hobit
<g/>
:	:	kIx,	:
obrázky	obrázek	k1gInPc1	obrázek
nakreslil	nakreslit	k5eAaPmAgMnS	nakreslit
Adolf	Adolf	k1gMnSc1	Adolf
Born	Born	k1gMnSc1	Born
a	a	k8xC	a
celý	celý	k2eAgInSc1d1	celý
děj	děj	k1gInSc1	děj
byl	být	k5eAaImAgInS	být
zhuštěn	zhustit	k5eAaPmNgInS	zhustit
do	do	k7c2	do
deseti	deset	k4xCc2	deset
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
invazi	invaze	k1gFnSc6	invaze
vojsk	vojsko	k1gNnPc2	vojsko
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
zareagoval	zareagovat	k5eAaPmAgMnS	zareagovat
filmem	film	k1gInSc7	film
Obři	obr	k1gMnPc1	obr
<g/>
,	,	kIx,	,
oceněným	oceněný	k2eAgNnSc7d1	oceněné
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
San	San	k1gMnSc6	San
Sebastianu	Sebastian	k1gMnSc6	Sebastian
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
jeho	jeho	k3xOp3gInPc7	jeho
úspěšnými	úspěšný	k2eAgInPc7d1	úspěšný
filmy	film	k1gInPc7	film
byla	být	k5eAaImAgFnS	být
Hloupá	hloupý	k2eAgFnSc1d1	hloupá
žába	žába	k1gFnSc1	žába
(	(	kIx(	(
<g/>
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
písničky	písnička	k1gFnSc2	písnička
Pete	Pete	k1gFnSc1	Pete
Seegera	Seegera	k1gFnSc1	Seegera
<g/>
,	,	kIx,	,
Mrňous	Mrňous	k1gInSc1	Mrňous
a	a	k8xC	a
čarodějnice	čarodějnice	k1gFnSc1	čarodějnice
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
a	a	k8xC	a
Císařovy	Císařův	k2eAgInPc1d1	Císařův
nové	nový	k2eAgInPc1d1	nový
šaty	šat	k1gInPc1	šat
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zahrál	zahrát	k5eAaPmAgInS	zahrát
si	se	k3xPyFc3	se
také	také	k6eAd1	také
epizodní	epizodní	k2eAgFnSc4d1	epizodní
roli	role	k1gFnSc4	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Ještě	ještě	k9	ještě
větší	veliký	k2eAgMnSc1d2	veliký
blbec	blbec	k1gMnSc1	blbec
<g/>
,	,	kIx,	,
než	než	k8xS	než
jsme	být	k5eAaImIp1nP	být
doufali	doufat	k5eAaImAgMnP	doufat
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vydal	vydat	k5eAaPmAgInS	vydat
autobiografii	autobiografie	k1gFnSc4	autobiografie
For	forum	k1gNnPc2	forum
the	the	k?	the
Love	lov	k1gInSc5	lov
of	of	k?	of
Prague	Prague	k1gNnPc7	Prague
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
obdržel	obdržet	k5eAaPmAgInS	obdržet
Annie	Annie	k1gFnSc2	Annie
Award	Award	k1gInSc1	Award
za	za	k7c4	za
celoživotní	celoživotní	k2eAgNnSc4d1	celoživotní
dílo	dílo	k1gNnSc4	dílo
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
animovaného	animovaný	k2eAgInSc2d1	animovaný
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
synové	syn	k1gMnPc1	syn
Kim	Kim	k1gMnSc1	Kim
Deitch	Deitch	k1gMnSc1	Deitch
<g/>
,	,	kIx,	,
Simon	Simon	k1gMnSc1	Simon
Deitch	Deitch	k1gMnSc1	Deitch
a	a	k8xC	a
Seth	Seth	k1gMnSc1	Seth
Deitch	Deitch	k1gMnSc1	Deitch
jsou	být	k5eAaImIp3nP	být
uznávanými	uznávaný	k2eAgMnPc7d1	uznávaný
komiksovými	komiksový	k2eAgMnPc7d1	komiksový
výtvarníky	výtvarník	k1gMnPc7	výtvarník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Gene	gen	k1gInSc5	gen
Deitch	Deit	k1gFnPc6	Deit
</s>
</p>
<p>
<s>
https://web.archive.org/web/20050303173042/http://www.genedeitch.com/	[url]	k4	https://web.archive.org/web/20050303173042/http://www.genedeitch.com/
</s>
</p>
<p>
<s>
http://genedeitchcredits.com/	[url]	k?	http://genedeitchcredits.com/
</s>
</p>
<p>
<s>
https://web.archive.org/web/20150924064425/http://www.pametnaroda.cz/witness/index/id/3965/#cs_3965	[url]	k4	https://web.archive.org/web/20150924064425/http://www.pametnaroda.cz/witness/index/id/3965/#cs_3965
</s>
</p>
<p>
<s>
Gene	gen	k1gInSc5	gen
Deitch	Deit	k1gFnPc6	Deit
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Gene	gen	k1gInSc5	gen
Deitch	Deitcha	k1gFnPc2	Deitcha
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
http://www.sanquis.cz/index2.php?linkID=art733	[url]	k4	http://www.sanquis.cz/index2.php?linkID=art733
</s>
</p>
<p>
<s>
http://www.ceskatelevize.cz/ct24/kultura/212389-oscarovy-gene-deitch-tocil-toma-a-jerryho-v-ceskoslovensku/	[url]	k4	http://www.ceskatelevize.cz/ct24/kultura/212389-oscarovy-gene-deitch-tocil-toma-a-jerryho-v-ceskoslovensku/
</s>
</p>
<p>
<s>
https://web.archive.org/web/20140429230604/http://praguepost.com/profile/33837-spotlight-on-prague-expat-writer-gene-deitch	[url]	k1gMnSc1	https://web.archive.org/web/20140429230604/http://praguepost.com/profile/33837-spotlight-on-prague-expat-writer-gene-deitch
</s>
</p>
