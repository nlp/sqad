<p>
<s>
Ashburton	Ashburton	k1gInSc1	Ashburton
Grove	Groev	k1gFnSc2	Groev
<g/>
,	,	kIx,	,
ze	z	k7c2	z
sponzorských	sponzorský	k2eAgInPc2d1	sponzorský
důvodů	důvod	k1gInPc2	důvod
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
Emirates	Emirates	k1gMnSc1	Emirates
Stadium	stadium	k1gNnSc1	stadium
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jednoduše	jednoduše	k6eAd1	jednoduše
Emirates	Emiratesa	k1gFnPc2	Emiratesa
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
fotbalový	fotbalový	k2eAgInSc4d1	fotbalový
stadion	stadion	k1gInSc4	stadion
nacházející	nacházející	k2eAgInSc4d1	nacházející
se	se	k3xPyFc4	se
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Islington	Islington	k1gInSc4	Islington
<g/>
,	,	kIx,	,
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Londýna	Londýn	k1gInSc2	Londýn
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
domácím	domácí	k2eAgInSc7d1	domácí
stadionem	stadion	k1gInSc7	stadion
Arsenalu	Arsenal	k1gInSc2	Arsenal
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
kapacitou	kapacita	k1gFnSc7	kapacita
60	[number]	k4	60
361	[number]	k4	361
diváků	divák	k1gMnPc2	divák
je	být	k5eAaImIp3nS	být
Emirates	Emirates	k1gInSc4	Emirates
třetí	třetí	k4xOgInSc4	třetí
největší	veliký	k2eAgInSc4d3	veliký
fotbalový	fotbalový	k2eAgInSc4d1	fotbalový
stadion	stadion	k1gInSc4	stadion
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
hned	hned	k6eAd1	hned
po	po	k7c4	po
Wembley	Wemblea	k1gFnPc4	Wemblea
a	a	k8xC	a
po	po	k7c6	po
Old	Olda	k1gFnPc2	Olda
Trafford	Traffordo	k1gNnPc2	Traffordo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
čtvrtým	čtvrtý	k4xOgInSc7	čtvrtý
největším	veliký	k2eAgInSc7d3	veliký
fotbalovým	fotbalový	k2eAgInSc7d1	fotbalový
stadionem	stadion	k1gInSc7	stadion
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
začal	začít	k5eAaPmAgInS	začít
Arsenal	Arsenal	k1gFnSc2	Arsenal
zjišťovat	zjišťovat	k5eAaImF	zjišťovat
možnosti	možnost	k1gFnPc4	možnost
na	na	k7c4	na
přesun	přesun	k1gInSc4	přesun
do	do	k7c2	do
nového	nový	k2eAgInSc2d1	nový
stadionu	stadion	k1gInSc2	stadion
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
rozšíření	rozšíření	k1gNnSc1	rozšíření
stávajícího	stávající	k2eAgInSc2d1	stávající
Highbury	Highbur	k1gInPc4	Highbur
nebylo	být	k5eNaImAgNnS	být
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
zamítnutí	zamítnutí	k1gNnSc2	zamítnutí
stavebního	stavební	k2eAgInSc2d1	stavební
povolení	povolení	k1gNnSc6	povolení
radou	rada	k1gMnSc7	rada
městské	městský	k2eAgFnSc2d1	městská
části	část	k1gFnSc2	část
Islington	Islington	k1gInSc1	Islington
možné	možný	k2eAgInPc1d1	možný
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zvážení	zvážení	k1gNnSc6	zvážení
několika	několik	k4yIc2	několik
dalších	další	k2eAgFnPc2d1	další
variant	varianta	k1gFnPc2	varianta
se	se	k3xPyFc4	se
klub	klub	k1gInSc1	klub
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
koupit	koupit	k5eAaPmF	koupit
pozemek	pozemek	k1gInSc4	pozemek
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgNnSc6	který
dříve	dříve	k6eAd2	dříve
sídlila	sídlit	k5eAaImAgFnS	sídlit
firma	firma	k1gFnSc1	firma
na	na	k7c4	na
likvidace	likvidace	k1gFnPc4	likvidace
odpadu	odpad	k1gInSc2	odpad
<g/>
,	,	kIx,	,
v	v	k7c4	v
Ashburton	Ashburton	k1gInSc4	Ashburton
Grove	Groev	k1gFnSc2	Groev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
plán	plán	k1gInSc1	plán
předložen	předložit	k5eAaPmNgInS	předložit
veřejnosti	veřejnost	k1gFnSc3	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
odporu	odpor	k1gInSc3	odpor
místních	místní	k2eAgMnPc2d1	místní
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
klubových	klubový	k2eAgMnPc2d1	klubový
akcionářů	akcionář	k1gMnPc2	akcionář
proti	proti	k7c3	proti
přesunu	přesun	k1gInSc3	přesun
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Arsenalu	Arsenal	k1gMnSc3	Arsenal
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
souhlas	souhlas	k1gInSc4	souhlas
městské	městský	k2eAgFnSc2d1	městská
rady	rada	k1gFnSc2	rada
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
nového	nový	k2eAgInSc2d1	nový
stadionu	stadion	k1gInSc2	stadion
<g/>
.	.	kIx.	.
</s>
<s>
Arsè	Arsè	k?	Arsè
Wenger	Wenger	k1gInSc1	Wenger
to	ten	k3xDgNnSc4	ten
později	pozdě	k6eAd2	pozdě
popsal	popsat	k5eAaPmAgMnS	popsat
jako	jako	k8xS	jako
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
největší	veliký	k2eAgNnSc4d3	veliký
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
Arsenalu	Arsenal	k1gInSc2	Arsenal
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
vedení	vedení	k1gNnSc1	vedení
rozhodlo	rozhodnout	k5eAaPmAgNnS	rozhodnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
přivést	přivést	k5eAaPmF	přivést
na	na	k7c4	na
lavičku	lavička	k1gFnSc4	lavička
Herberta	Herbert	k1gMnSc2	Herbert
Chapmana	Chapman	k1gMnSc2	Chapman
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Demoliční	demoliční	k2eAgFnSc1d1	demoliční
práce	práce	k1gFnSc1	práce
na	na	k7c6	na
novém	nový	k2eAgInSc6d1	nový
pozemku	pozemek	k1gInSc6	pozemek
začaly	začít	k5eAaPmAgInP	začít
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
stadionu	stadion	k1gInSc2	stadion
potom	potom	k6eAd1	potom
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
měsíce	měsíc	k1gInPc4	měsíc
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Financování	financování	k1gNnSc1	financování
stadionu	stadion	k1gInSc2	stadion
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
býti	být	k5eAaImF	být
složité	složitý	k2eAgNnSc1d1	složité
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
klub	klub	k1gInSc1	klub
pozdržel	pozdržet	k5eAaPmAgInS	pozdržet
práce	práce	k1gFnSc2	práce
na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
až	až	k9	až
do	do	k7c2	do
února	únor	k1gInSc2	únor
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2004	[number]	k4	2004
byla	být	k5eAaImAgFnS	být
jako	jako	k9	jako
hlavní	hlavní	k2eAgMnSc1d1	hlavní
sponzor	sponzor	k1gMnSc1	sponzor
oznámena	oznámen	k2eAgFnSc1d1	oznámena
společnost	společnost	k1gFnSc1	společnost
Emirates	Emiratesa	k1gFnPc2	Emiratesa
<g/>
.	.	kIx.	.
</s>
<s>
Stavební	stavební	k2eAgFnSc1d1	stavební
práce	práce	k1gFnSc1	práce
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
byly	být	k5eAaImAgFnP	být
ukončeny	ukončit	k5eAaPmNgFnP	ukončit
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
se	se	k3xPyFc4	se
vyšplhala	vyšplhat	k5eAaPmAgFnS	vyšplhat
na	na	k7c4	na
390	[number]	k4	390
milionů	milion	k4xCgInPc2	milion
£	£	k?	£
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
zápasů	zápas	k1gInPc2	zápas
mimo	mimo	k7c4	mimo
anglickou	anglický	k2eAgFnSc4d1	anglická
ligu	liga	k1gFnSc4	liga
a	a	k8xC	a
poháry	pohár	k1gInPc7	pohár
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
název	název	k1gInSc4	název
používán	používán	k2eAgInSc4d1	používán
termín	termín	k1gInSc4	termín
"	"	kIx"	"
<g/>
Arsenal	Arsenal	k1gFnSc4	Arsenal
Stadium	stadium	k1gNnSc1	stadium
<g/>
"	"	kIx"	"
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
<g/>
,	,	kIx,	,
že	že	k8xS	že
společnost	společnost	k1gFnSc1	společnost
Fly	Fly	k1gFnSc2	Fly
Emirates	Emiratesa	k1gFnPc2	Emiratesa
nemá	mít	k5eNaImIp3nS	mít
podepsanou	podepsaný	k2eAgFnSc4d1	podepsaná
reklamní	reklamní	k2eAgFnSc4d1	reklamní
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Evropskou	evropský	k2eAgFnSc7d1	Evropská
fotbalovou	fotbalový	k2eAgFnSc7d1	fotbalová
asociací	asociace	k1gFnSc7	asociace
(	(	kIx(	(
<g/>
UEFA	UEFA	kA	UEFA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Emirates	Emirates	k1gInSc1	Emirates
Stadium	stadium	k1gNnSc1	stadium
nahradil	nahradit	k5eAaPmAgInS	nahradit
starší	starý	k2eAgMnPc4d2	starší
Highbury	Highbur	k1gMnPc4	Highbur
o	o	k7c6	o
kapacitě	kapacita	k1gFnSc6	kapacita
38	[number]	k4	38
419	[number]	k4	419
diváků	divák	k1gMnPc2	divák
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
pyšnit	pyšnit	k5eAaImF	pyšnit
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
trávníků	trávník	k1gInPc2	trávník
na	na	k7c6	na
stadionech	stadion	k1gInPc6	stadion
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počátkem	počátkem	k7c2	počátkem
srpna	srpen	k1gInSc2	srpen
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
prošel	projít	k5eAaPmAgInS	projít
stadion	stadion	k1gInSc1	stadion
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
Arsenalizací	Arsenalizace	k1gFnPc2	Arsenalizace
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
obnovit	obnovit	k5eAaPmF	obnovit
historii	historie	k1gFnSc4	historie
a	a	k8xC	a
dědictví	dědictví	k1gNnSc4	dědictví
klubu	klub	k1gInSc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
Stadion	stadion	k1gInSc1	stadion
často	často	k6eAd1	často
hostil	hostit	k5eAaImAgInS	hostit
hudební	hudební	k2eAgInPc4d1	hudební
koncerty	koncert	k1gInPc4	koncert
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
fotbalové	fotbalový	k2eAgInPc4d1	fotbalový
zápasy	zápas	k1gInPc4	zápas
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
představila	představit	k5eAaPmAgFnS	představit
Brazilská	brazilský	k2eAgFnSc1d1	brazilská
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
reprezentace	reprezentace	k1gFnSc1	reprezentace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Původ	původ	k1gInSc1	původ
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c6	na
tragédii	tragédie	k1gFnSc6	tragédie
v	v	k7c4	v
Hillsborough	Hillsborough	k1gInSc4	Hillsborough
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1989	[number]	k4	1989
bylo	být	k5eAaImAgNnS	být
spuštěno	spuštěn	k2eAgNnSc1d1	spuštěno
Lordem	lord	k1gMnSc7	lord
Taylorem	Taylor	k1gMnSc7	Taylor
z	z	k7c2	z
Gosforthu	Gosforth	k1gInSc2	Gosforth
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
davů	dav	k1gInPc2	dav
na	na	k7c6	na
sportovištích	sportoviště	k1gNnPc6	sportoviště
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
závěrečná	závěrečný	k2eAgFnSc1d1	závěrečná
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
doporučovala	doporučovat	k5eAaImAgFnS	doporučovat
snížení	snížení	k1gNnSc4	snížení
kapacity	kapacita	k1gFnSc2	kapacita
stadionů	stadion	k1gInPc2	stadion
o	o	k7c4	o
15	[number]	k4	15
%	%	kIx~	%
a	a	k8xC	a
výstavbu	výstavba	k1gFnSc4	výstavba
sedadel	sedadlo	k1gNnPc2	sedadlo
na	na	k7c6	na
dřívějších	dřívější	k2eAgNnPc6d1	dřívější
místech	místo	k1gNnPc6	místo
pro	pro	k7c4	pro
stání	stání	k1gNnPc4	stání
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
fotbalových	fotbalový	k2eAgInPc2d1	fotbalový
klubů	klub	k1gInPc2	klub
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
potýkalo	potýkat	k5eAaImAgNnS	potýkat
s	s	k7c7	s
požadavkem	požadavek	k1gInSc7	požadavek
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
měly	mít	k5eAaImAgInP	mít
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
sezóny	sezóna	k1gFnSc2	sezóna
1994	[number]	k4	1994
<g/>
/	/	kIx~	/
<g/>
95	[number]	k4	95
na	na	k7c6	na
stadionech	stadion	k1gInPc6	stadion
pouze	pouze	k6eAd1	pouze
místa	místo	k1gNnPc1	místo
k	k	k7c3	k
sezení	sezení	k1gNnSc3	sezení
<g/>
.	.	kIx.	.
</s>
<s>
Snažily	snažit	k5eAaImAgFnP	snažit
se	se	k3xPyFc4	se
nalézt	nalézt	k5eAaPmF	nalézt
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
zvýšit	zvýšit	k5eAaPmF	zvýšit
příjmy	příjem	k1gInPc4	příjem
za	za	k7c4	za
přechod	přechod	k1gInSc4	přechod
od	od	k7c2	od
míst	místo	k1gNnPc2	místo
ke	k	k7c3	k
stání	stání	k1gNnSc3	stání
k	k	k7c3	k
místům	místo	k1gNnPc3	místo
k	k	k7c3	k
sezení	sezení	k1gNnSc3	sezení
<g/>
.	.	kIx.	.
</s>
<s>
Místopředseda	místopředseda	k1gMnSc1	místopředseda
představenstva	představenstvo	k1gNnSc2	představenstvo
Arsenalu	Arsenal	k1gInSc2	Arsenal
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Dein	Dein	k1gMnSc1	Dein
<g/>
,	,	kIx,	,
zavedl	zavést	k5eAaPmAgMnS	zavést
na	na	k7c6	na
konci	konec	k1gInSc6	konec
sezóny	sezóna	k1gFnSc2	sezóna
1990	[number]	k4	1990
<g/>
/	/	kIx~	/
<g/>
91	[number]	k4	91
systém	systém	k1gInSc4	systém
dluhopisů	dluhopis	k1gInPc2	dluhopis
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgMnSc7	ten
získali	získat	k5eAaPmAgMnP	získat
fanoušci	fanoušek	k1gMnPc1	fanoušek
právo	právo	k1gNnSc4	právo
na	na	k7c4	na
zakoupení	zakoupení	k1gNnSc4	zakoupení
celoroční	celoroční	k2eAgFnSc2d1	celoroční
vstupenky	vstupenka	k1gFnSc2	vstupenka
(	(	kIx(	(
<g/>
permanentky	permanentka	k1gFnSc2	permanentka
<g/>
)	)	kIx)	)
na	na	k7c4	na
nově	nově	k6eAd1	nově
přestavěnou	přestavěný	k2eAgFnSc4d1	přestavěná
tribunu	tribuna	k1gFnSc4	tribuna
North	North	k1gMnSc1	North
Bank	banka	k1gFnPc2	banka
na	na	k7c4	na
Highbury	Highbur	k1gMnPc4	Highbur
<g/>
.	.	kIx.	.
</s>
<s>
Dluhopisy	dluhopis	k1gInPc1	dluhopis
na	na	k7c4	na
150	[number]	k4	150
let	léto	k1gNnPc2	léto
s	s	k7c7	s
cenami	cena	k1gFnPc7	cena
mezi	mezi	k7c7	mezi
£	£	k?	£
1000	[number]	k4	1000
<g/>
–	–	k?	–
<g/>
1500	[number]	k4	1500
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
terčem	terč	k1gInSc7	terč
kritiky	kritika	k1gFnSc2	kritika
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
vlastních	vlastní	k2eAgMnPc2d1	vlastní
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Těm	ten	k3xDgMnPc3	ten
se	se	k3xPyFc4	se
nelíbila	líbit	k5eNaImAgFnS	líbit
vysoká	vysoký	k2eAgFnSc1d1	vysoká
prodejní	prodejní	k2eAgFnSc1d1	prodejní
cena	cena	k1gFnSc1	cena
těchto	tento	k3xDgFnPc2	tento
vstupenek	vstupenka	k1gFnPc2	vstupenka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
si	se	k3xPyFc3	se
mohly	moct	k5eAaImAgFnP	moct
dovolit	dovolit	k5eAaPmF	dovolit
pouze	pouze	k6eAd1	pouze
bohatí	bohatý	k2eAgMnPc1d1	bohatý
fanoušci	fanoušek	k1gMnPc1	fanoušek
<g/>
,	,	kIx,	,
a	a	k8xC	a
cítili	cítit	k5eAaImAgMnP	cítit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebudou	být	k5eNaImBp3nP	být
moci	moct	k5eAaImF	moct
podporovat	podporovat	k5eAaImF	podporovat
dále	daleko	k6eAd2	daleko
svůj	svůj	k3xOyFgInSc4	svůj
klub	klub	k1gInSc4	klub
<g/>
.	.	kIx.	.
</s>
<s>
Odvetná	odvetný	k2eAgFnSc1d1	odvetná
kampaň	kampaň	k1gFnSc1	kampaň
režírovaná	režírovaný	k2eAgFnSc1d1	režírovaná
zástupci	zástupce	k1gMnSc3	zástupce
asociace	asociace	k1gFnSc2	asociace
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
fanoušků	fanoušek	k1gMnPc2	fanoušek
Arsenalu	Arsenal	k1gInSc2	Arsenal
přinesla	přinést	k5eAaPmAgFnS	přinést
relativní	relativní	k2eAgInSc4d1	relativní
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
prodána	prodat	k5eAaPmNgFnS	prodat
pouze	pouze	k6eAd1	pouze
třetina	třetina	k1gFnSc1	třetina
těchto	tento	k3xDgFnPc2	tento
permanentek	permanentka	k1gFnPc2	permanentka
<g/>
.	.	kIx.	.
<g/>
Tribuna	tribuna	k1gFnSc1	tribuna
North	North	k1gInSc4	North
Bank	bank	k1gInSc1	bank
Stand	Standa	k1gFnPc2	Standa
<g/>
,	,	kIx,	,
poslední	poslední	k2eAgFnSc1d1	poslední
část	část	k1gFnSc1	část
Highbury	Highbura	k1gFnSc2	Highbura
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
modernizována	modernizovat	k5eAaBmNgFnS	modernizovat
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
modernizaci	modernizace	k1gFnSc4	modernizace
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
hodnoty	hodnota	k1gFnPc1	hodnota
20	[number]	k4	20
milionů	milion	k4xCgInPc2	milion
£	£	k?	£
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
kapacita	kapacita	k1gFnSc1	kapacita
stadionu	stadion	k1gInSc2	stadion
byla	být	k5eAaImAgFnS	být
výrazně	výrazně	k6eAd1	výrazně
snížena	snížit	k5eAaPmNgFnS	snížit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
desetiletí	desetiletí	k1gNnSc2	desetiletí
měl	mít	k5eAaImAgInS	mít
kapacitu	kapacita	k1gFnSc4	kapacita
57	[number]	k4	57
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
po	po	k7c6	po
modernizaci	modernizace	k1gFnSc6	modernizace
necelých	celý	k2eNgInPc2d1	necelý
40	[number]	k4	40
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
vysokými	vysoký	k2eAgFnPc7d1	vysoká
cenami	cena	k1gFnPc7	cena
vstupenek	vstupenka	k1gFnPc2	vstupenka
<g/>
,	,	kIx,	,
stávajícími	stávající	k2eAgInPc7d1	stávající
dluhy	dluh	k1gInPc7	dluh
a	a	k8xC	a
nízkými	nízký	k2eAgFnPc7d1	nízká
návštěvami	návštěva	k1gFnPc7	návštěva
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Arsenal	Arsenal	k1gMnSc1	Arsenal
začal	začít	k5eAaPmAgMnS	začít
porozhlížet	porozhlížet	k5eAaPmF	porozhlížet
po	po	k7c6	po
možnosti	možnost	k1gFnSc6	možnost
vybudování	vybudování	k1gNnSc2	vybudování
většího	veliký	k2eAgInSc2d2	veliký
stadionu	stadion	k1gInSc2	stadion
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
by	by	kYmCp3nS	by
přilákal	přilákat	k5eAaPmAgInS	přilákat
stále	stále	k6eAd1	stále
se	s	k7c7	s
zvětšující	zvětšující	k2eAgFnSc7d1	zvětšující
základnu	základna	k1gFnSc4	základna
fanoušků	fanoušek	k1gMnPc2	fanoušek
a	a	k8xC	a
finančně	finančně	k6eAd1	finančně
by	by	kYmCp3nS	by
konkuroval	konkurovat	k5eAaImAgMnS	konkurovat
největším	veliký	k2eAgMnPc3d3	veliký
klubům	klub	k1gInPc3	klub
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
Manchester	Manchester	k1gInSc1	Manchester
United	United	k1gInSc1	United
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
za	za	k7c4	za
vstupné	vstupné	k1gNnSc4	vstupné
43,9	[number]	k4	43,9
milionů	milion	k4xCgInPc2	milion
£	£	k?	£
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
už	už	k6eAd1	už
částka	částka	k1gFnSc1	částka
činila	činit	k5eAaImAgFnS	činit
87,9	[number]	k4	87,9
milionů	milion	k4xCgInPc2	milion
£	£	k?	£
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
dopomohlo	dopomoct	k5eAaPmAgNnS	dopomoct
rozšíření	rozšíření	k1gNnSc1	rozšíření
Old	Olda	k1gFnPc2	Olda
Traffordu	Trafford	k1gInSc2	Trafford
<g/>
.	.	kIx.	.
<g/>
Původní	původní	k2eAgInSc1d1	původní
plán	plán	k1gInSc1	plán
Arsenalu	Arsenal	k1gInSc2	Arsenal
měl	mít	k5eAaImAgInS	mít
rozšířit	rozšířit	k5eAaPmF	rozšířit
Highbury	Highbur	k1gMnPc4	Highbur
na	na	k7c4	na
48	[number]	k4	48
000	[number]	k4	000
míst	místo	k1gNnPc2	místo
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
přestavby	přestavba	k1gFnSc2	přestavba
dvou	dva	k4xCgFnPc2	dva
tribun	tribuna	k1gFnPc2	tribuna
–	–	k?	–
West	West	k1gInSc1	West
Stand	Standa	k1gFnPc2	Standa
a	a	k8xC	a
Clock	Clocka	k1gFnPc2	Clocka
End	End	k1gFnSc2	End
<g/>
.	.	kIx.	.
</s>
<s>
Nápad	nápad	k1gInSc1	nápad
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgInS	setkat
s	s	k7c7	s
nesouhlasem	nesouhlas	k1gInSc7	nesouhlas
místních	místní	k2eAgMnPc2d1	místní
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
záznamu	záznam	k1gInSc2	záznam
z	z	k7c2	z
července	červenec	k1gInSc2	červenec
1997	[number]	k4	1997
měla	mít	k5eAaImAgFnS	mít
tribuna	tribuna	k1gFnSc1	tribuna
East	Easta	k1gFnPc2	Easta
Stand	Standa	k1gFnPc2	Standa
stupeň	stupeň	k1gInSc1	stupeň
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
mnoha	mnoho	k4c6	mnoho
diskuzích	diskuze	k1gFnPc6	diskuze
klub	klub	k1gInSc1	klub
nakonec	nakonec	k6eAd1	nakonec
od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
plánu	plán	k1gInSc2	plán
upustil	upustit	k5eAaPmAgMnS	upustit
a	a	k8xC	a
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
kapacita	kapacita	k1gFnSc1	kapacita
by	by	kYmCp3nS	by
stejně	stejně	k6eAd1	stejně
nebyla	být	k5eNaImAgFnS	být
dostatečně	dostatečně	k6eAd1	dostatečně
velká	velký	k2eAgFnSc1d1	velká
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
tedy	tedy	k9	tedy
hledat	hledat	k5eAaImF	hledat
vhodné	vhodný	k2eAgNnSc4d1	vhodné
místo	místo	k1gNnSc4	místo
pro	pro	k7c4	pro
výstavbu	výstavba	k1gFnSc4	výstavba
nového	nový	k2eAgInSc2d1	nový
stadionu	stadion	k1gInSc2	stadion
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1998	[number]	k4	1998
Arsenal	Arsenal	k1gMnSc1	Arsenal
zkoumal	zkoumat	k5eAaImAgMnS	zkoumat
možnost	možnost	k1gFnSc4	možnost
přestěhovat	přestěhovat	k5eAaPmF	přestěhovat
se	se	k3xPyFc4	se
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
starého	starý	k2eAgMnSc2d1	starý
Wembley	Wemblea	k1gMnSc2	Wemblea
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgFnSc4d1	oficiální
nabídku	nabídka	k1gFnSc4	nabídka
podal	podat	k5eAaPmAgMnS	podat
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
sezóny	sezóna	k1gFnSc2	sezóna
1997	[number]	k4	1997
<g/>
/	/	kIx~	/
<g/>
98	[number]	k4	98
byla	být	k5eAaImAgFnS	být
nabídka	nabídka	k1gFnSc1	nabídka
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Arsenalu	Arsenal	k1gInSc2	Arsenal
stažena	stáhnout	k5eAaPmNgFnS	stáhnout
a	a	k8xC	a
Wembley	Wemblea	k1gFnSc2	Wemblea
bylo	být	k5eAaImAgNnS	být
zakoupeno	zakoupit	k5eAaPmNgNnS	zakoupit
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
1998	[number]	k4	1998
anglickou	anglický	k2eAgFnSc7d1	anglická
fotbalovou	fotbalový	k2eAgFnSc7d1	fotbalová
asociací	asociace	k1gFnSc7	asociace
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
sezón	sezóna	k1gFnPc2	sezóna
1998	[number]	k4	1998
<g/>
/	/	kIx~	/
<g/>
99	[number]	k4	99
a	a	k8xC	a
1999	[number]	k4	1999
<g/>
/	/	kIx~	/
<g/>
2000	[number]	k4	2000
však	však	k9	však
stadion	stadion	k1gInSc1	stadion
hostil	hostit	k5eAaImAgInS	hostit
všechny	všechen	k3xTgFnPc4	všechen
domácí	domácí	k1gFnPc4	domácí
utkání	utkání	k1gNnSc2	utkání
Arsenalu	Arsenal	k1gInSc2	Arsenal
v	v	k7c6	v
Lize	liga	k1gFnSc6	liga
mistrů	mistr	k1gMnPc2	mistr
UEFA	UEFA	kA	UEFA
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
klubu	klub	k1gInSc2	klub
se	se	k3xPyFc4	se
v	v	k7c6	v
soutěži	soutěž	k1gFnSc6	soutěž
nedařilo	dařit	k5eNaImAgNnS	dařit
<g/>
.	.	kIx.	.
</s>
<s>
Dvakrát	dvakrát	k6eAd1	dvakrát
po	po	k7c6	po
sobě	sebe	k3xPyFc6	sebe
byl	být	k5eAaImAgMnS	být
vyřazen	vyřadit	k5eAaPmNgMnS	vyřadit
hned	hned	k6eAd1	hned
ve	v	k7c6	v
skupinové	skupinový	k2eAgFnSc6d1	skupinová
fázi	fáze	k1gFnSc6	fáze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1998	[number]	k4	1998
v	v	k7c6	v
zápase	zápas	k1gInSc6	zápas
proti	proti	k7c3	proti
RC	RC	kA	RC
Lens	Lens	k1gInSc1	Lens
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
návštěva	návštěva	k1gFnSc1	návštěva
na	na	k7c4	na
zápas	zápas	k1gInSc4	zápas
Arsenalu	Arsenal	k1gInSc2	Arsenal
<g/>
.	.	kIx.	.
</s>
<s>
Zápas	zápas	k1gInSc4	zápas
tehdy	tehdy	k6eAd1	tehdy
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
73	[number]	k4	73
707	[number]	k4	707
diváků	divák	k1gMnPc2	divák
a	a	k8xC	a
klub	klub	k1gInSc1	klub
na	na	k7c6	na
vstupném	vstupné	k1gNnSc6	vstupné
získal	získat	k5eAaPmAgMnS	získat
1	[number]	k4	1
milion	milion	k4xCgInSc4	milion
£	£	k?	£
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
mu	on	k3xPp3gMnSc3	on
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
potenciál	potenciál	k1gInSc4	potenciál
kapacitně	kapacitně	k6eAd1	kapacitně
větších	veliký	k2eAgInPc2d2	veliký
stadionů	stadion	k1gInPc2	stadion
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výběr	výběr	k1gInSc1	výběr
místa	místo	k1gNnSc2	místo
a	a	k8xC	a
konflikt	konflikt	k1gInSc1	konflikt
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1999	[number]	k4	1999
Arsenal	Arsenal	k1gMnSc1	Arsenal
zkoumal	zkoumat	k5eAaImAgMnS	zkoumat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
realitního	realitní	k2eAgMnSc2d1	realitní
makléře	makléř	k1gMnSc2	makléř
a	a	k8xC	a
klubového	klubový	k2eAgMnSc2d1	klubový
poradce	poradce	k1gMnSc2	poradce
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
majetku	majetek	k1gInSc2	majetek
<g/>
,	,	kIx,	,
Anthony	Anthon	k1gInPc7	Anthon
Spencera	Spencero	k1gNnSc2	Spencero
<g/>
,	,	kIx,	,
proveditelnost	proveditelnost	k1gFnSc1	proveditelnost
stavby	stavba	k1gFnSc2	stavba
nového	nový	k2eAgInSc2d1	nový
stadionu	stadion	k1gInSc2	stadion
v	v	k7c4	v
Ashburton	Ashburton	k1gInSc4	Ashburton
Grove	Groev	k1gFnSc2	Groev
<g/>
.	.	kIx.	.
</s>
<s>
Pozemek	pozemek	k1gInSc1	pozemek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
nacházel	nacházet	k5eAaImAgMnS	nacházet
460	[number]	k4	460
m	m	kA	m
od	od	k7c2	od
Highbury	Highbura	k1gFnSc2	Highbura
<g/>
,	,	kIx,	,
zahrnoval	zahrnovat	k5eAaImAgInS	zahrnovat
podnik	podnik	k1gInSc1	podnik
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
zpracovával	zpracovávat	k5eAaImAgInS	zpracovávat
odpadky	odpadek	k1gInPc4	odpadek
<g/>
,	,	kIx,	,
a	a	k8xC	a
další	další	k2eAgFnSc2d1	další
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
nemovitosti	nemovitost	k1gFnSc2	nemovitost
<g/>
.	.	kIx.	.
</s>
<s>
Osmdesát	osmdesát	k4xCc4	osmdesát
procent	procento	k1gNnPc2	procento
bylo	být	k5eAaImAgNnS	být
ve	v	k7c4	v
vlastnictví	vlastnictví	k1gNnPc4	vlastnictví
rady	rada	k1gFnSc2	rada
čtvrti	čtvrt	k1gFnSc2	čtvrt
Islington	Islington	k1gInSc1	Islington
a	a	k8xC	a
společností	společnost	k1gFnSc7	společnost
Railtrack	Railtracka	k1gFnPc2	Railtracka
a	a	k8xC	a
Sainsbury	Sainsbura	k1gFnSc2	Sainsbura
<g/>
'	'	kIx"	'
<g/>
s.	s.	k?	s.
Tento	tento	k3xDgInSc4	tento
krok	krok	k1gInSc4	krok
proto	proto	k8xC	proto
závisel	záviset	k5eAaImAgMnS	záviset
na	na	k7c6	na
odkupu	odkup	k1gInSc6	odkup
majetku	majetek	k1gInSc2	majetek
od	od	k7c2	od
místních	místní	k2eAgMnPc2d1	místní
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
následné	následný	k2eAgInPc4d1	následný
náklady	náklad	k1gInPc4	náklad
spojené	spojený	k2eAgInPc4d1	spojený
s	s	k7c7	s
hrazením	hrazení	k1gNnSc7	hrazení
jejich	jejich	k3xOp3gNnSc2	jejich
přestěhování	přestěhování	k1gNnSc2	přestěhování
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
prvním	první	k4xOgInSc6	první
významnějším	významný	k2eAgInSc6d2	významnější
úspěchu	úspěch	k1gInSc6	úspěch
u	u	k7c2	u
městské	městský	k2eAgFnSc2d1	městská
rady	rada	k1gFnSc2	rada
Arsenal	Arsenal	k1gMnSc1	Arsenal
předložil	předložit	k5eAaPmAgMnS	předložit
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2000	[number]	k4	2000
návrh	návrh	k1gInSc1	návrh
na	na	k7c4	na
vybudování	vybudování	k1gNnSc4	vybudování
nového	nový	k2eAgNnSc2d1	nové
60	[number]	k4	60
000	[number]	k4	000
stadionu	stadion	k1gInSc2	stadion
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
přestavbu	přestavba	k1gFnSc4	přestavba
projektu	projekt	k1gInSc2	projekt
v	v	k7c4	v
Drayton	Drayton	k1gInSc4	Drayton
Parku	park	k1gInSc2	park
<g/>
,	,	kIx,	,
přeměnu	přeměna	k1gFnSc4	přeměna
stávajícího	stávající	k2eAgMnSc2d1	stávající
Highbury	Highbura	k1gFnPc1	Highbura
na	na	k7c4	na
byty	byt	k1gInPc4	byt
a	a	k8xC	a
novou	nový	k2eAgFnSc4d1	nová
budovu	budova	k1gFnSc4	budova
na	na	k7c6	na
práci	práce	k1gFnSc6	práce
s	s	k7c7	s
odpadem	odpad	k1gInSc7	odpad
na	na	k7c4	na
Lough	Lough	k1gInSc4	Lough
Road	Roada	k1gFnPc2	Roada
<g/>
.	.	kIx.	.
</s>
<s>
Plán	plán	k1gInSc1	plán
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
vytvoření	vytvoření	k1gNnSc6	vytvoření
1800	[number]	k4	1800
nových	nový	k2eAgNnPc2d1	nové
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
a	a	k8xC	a
2300	[number]	k4	2300
nových	nový	k2eAgInPc2d1	nový
domovů	domov	k1gInPc2	domov
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
byla	být	k5eAaImAgFnS	být
přislíbena	přislíben	k2eAgFnSc1d1	přislíbena
modernizace	modernizace	k1gFnSc1	modernizace
tří	tři	k4xCgFnPc2	tři
vlakových	vlakový	k2eAgFnPc2d1	vlaková
stanic	stanice	k1gFnPc2	stanice
–	–	k?	–
Holloway	Holloway	k1gInPc1	Holloway
Road	Roado	k1gNnPc2	Roado
<g/>
,	,	kIx,	,
Drayton	Drayton	k1gInSc1	Drayton
Park	park	k1gInSc1	park
a	a	k8xC	a
Finsbury	Finsbura	k1gFnPc1	Finsbura
Park	park	k1gInSc1	park
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
vypořádání	vypořádání	k1gNnSc2	vypořádání
se	se	k3xPyFc4	se
s	s	k7c7	s
davy	dav	k1gInPc7	dav
v	v	k7c4	v
den	den	k1gInSc4	den
zápasu	zápas	k1gInSc2	zápas
<g/>
.	.	kIx.	.
<g/>
Přechod	přechod	k1gInSc1	přechod
na	na	k7c4	na
Ashburton	Ashburton	k1gInSc4	Ashburton
Grove	Groev	k1gFnSc2	Groev
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
odporem	odpor	k1gInSc7	odpor
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
asociace	asociace	k1gFnSc2	asociace
nezávislých	závislý	k2eNgMnPc2d1	nezávislý
fanoušků	fanoušek	k1gMnPc2	fanoušek
Arsenalu	Arsenal	k1gInSc2	Arsenal
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
byli	být	k5eAaImAgMnP	být
znepokojeni	znepokojen	k2eAgMnPc1d1	znepokojen
otázkami	otázka	k1gFnPc7	otázka
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2000	[number]	k4	2000
byla	být	k5eAaImAgFnS	být
zřízena	zřízen	k2eAgFnSc1d1	zřízena
aliance	aliance	k1gFnSc1	aliance
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Islington	Islington	k1gInSc1	Islington
Stadium	stadium	k1gNnSc1	stadium
Communities	Communities	k1gInSc1	Communities
Alliance	Alliance	k1gFnSc1	Alliance
<g/>
)	)	kIx)	)
16	[number]	k4	16
skupin	skupina	k1gFnPc2	skupina
zastupujících	zastupující	k2eAgFnPc2d1	zastupující
místní	místní	k2eAgMnPc4d1	místní
obyvatele	obyvatel	k1gMnPc4	obyvatel
a	a	k8xC	a
podnikatele	podnikatel	k1gMnPc4	podnikatel
<g/>
.	.	kIx.	.
</s>
<s>
Účelem	účel	k1gInSc7	účel
bylo	být	k5eAaImAgNnS	být
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
povědomí	povědomí	k1gNnSc4	povědomí
o	o	k7c6	o
asanaci	asanace	k1gFnSc6	asanace
<g/>
.	.	kIx.	.
</s>
<s>
Alison	Alison	k1gInSc1	Alison
Carmichaelová	Carmichaelová	k1gFnSc1	Carmichaelová
<g/>
,	,	kIx,	,
mluvčí	mluvčí	k1gMnSc1	mluvčí
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
k	k	k7c3	k
přesunu	přesun	k1gInSc3	přesun
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Na	na	k7c4	na
první	první	k4xOgInSc4	první
pohled	pohled	k1gInSc4	pohled
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
zdát	zdát	k5eAaImF	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
Arsenal	Arsenal	k1gFnSc1	Arsenal
dělá	dělat	k5eAaImIp3nS	dělat
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
oblast	oblast	k1gFnSc4	oblast
velkou	velký	k2eAgFnSc4d1	velká
věc	věc	k1gFnSc4	věc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
samotném	samotný	k2eAgInSc6d1	samotný
detailu	detail	k1gInSc6	detail
je	být	k5eAaImIp3nS	být
plán	plán	k1gInSc1	plán
příšerný	příšerný	k2eAgInSc1d1	příšerný
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
viníme	vinit	k5eAaImIp1nP	vinit
radu	rada	k1gFnSc4	rada
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
klub	klub	k1gInSc4	klub
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
snaží	snažit	k5eAaImIp3nS	snažit
dále	daleko	k6eAd2	daleko
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
vyššího	vysoký	k2eAgInSc2d2	vyšší
zisku	zisk	k1gInSc2	zisk
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
O	o	k7c4	o
sedm	sedm	k4xCc4	sedm
měsíců	měsíc	k1gInPc2	měsíc
později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
podána	podat	k5eAaPmNgFnS	podat
žádost	žádost	k1gFnSc1	žádost
o	o	k7c4	o
potvrzení	potvrzení	k1gNnSc4	potvrzení
stavebního	stavební	k2eAgInSc2d1	stavební
plánu	plán	k1gInSc2	plán
<g/>
.	.	kIx.	.
</s>
<s>
Průzkum	průzkum	k1gInSc1	průzkum
ukázal	ukázat	k5eAaPmAgInS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
75	[number]	k4	75
%	%	kIx~	%
respondentů	respondent	k1gMnPc2	respondent
bylo	být	k5eAaImAgNnS	být
proti	proti	k7c3	proti
návrhu	návrh	k1gInSc3	návrh
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2	[number]	k4	2
133	[number]	k4	133
proti	proti	k7c3	proti
a	a	k8xC	a
712	[number]	k4	712
pro	pro	k7c4	pro
<g/>
)	)	kIx)	)
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2001	[number]	k4	2001
vydal	vydat	k5eAaPmAgInS	vydat
klub	klub	k1gInSc1	klub
prohlášení	prohlášení	k1gNnSc2	prohlášení
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
oznamoval	oznamovat	k5eAaImAgMnS	oznamovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
průzkumu	průzkum	k1gInSc6	průzkum
mezi	mezi	k7c7	mezi
obyvateli	obyvatel	k1gMnPc7	obyvatel
Islingtonu	Islington	k1gInSc2	Islington
je	být	k5eAaImIp3nS	být
70	[number]	k4	70
%	%	kIx~	%
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
pro	pro	k7c4	pro
přijetí	přijetí	k1gNnSc4	přijetí
plánu	plán	k1gInSc2	plán
<g/>
.	.	kIx.	.
<g/>
Ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
podpory	podpora	k1gFnSc2	podpora
místních	místní	k2eAgMnPc2d1	místní
obyvatel	obyvatel	k1gMnPc2	obyvatel
nechal	nechat	k5eAaPmAgInS	nechat
Arsenal	Arsenal	k1gMnSc3	Arsenal
vyrobit	vyrobit	k5eAaPmF	vyrobit
slogany	slogan	k1gInPc4	slogan
kolem	kolem	k7c2	kolem
Highbury	Highbura	k1gFnSc2	Highbura
s	s	k7c7	s
textem	text	k1gInSc7	text
"	"	kIx"	"
<g/>
Nechte	nechat	k5eAaPmRp2nP	nechat
Arsenal	Arsenal	k1gFnSc4	Arsenal
podpořit	podpořit	k5eAaPmF	podpořit
Islington	Islington	k1gInSc4	Islington
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Slogany	slogan	k1gInPc1	slogan
byly	být	k5eAaImAgInP	být
poté	poté	k6eAd1	poté
použity	použít	k5eAaPmNgInP	použít
v	v	k7c6	v
zápasech	zápas	k1gInPc6	zápas
proti	proti	k7c3	proti
Aston	Astona	k1gFnPc2	Astona
Ville	Ville	k1gFnSc2	Ville
a	a	k8xC	a
Juventusu	Juventus	k1gInSc2	Juventus
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2001	[number]	k4	2001
a	a	k8xC	a
jako	jako	k9	jako
pozadí	pozadí	k1gNnSc2	pozadí
při	při	k7c6	při
tiskových	tiskový	k2eAgFnPc6d1	tisková
konferencích	konference	k1gFnPc6	konference
Arsè	Arsè	k1gFnSc2	Arsè
Wengera	Wengero	k1gNnSc2	Wengero
až	až	k9	až
do	do	k7c2	do
Vánoc	Vánoce	k1gFnPc2	Vánoce
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2001	[number]	k4	2001
schválila	schválit	k5eAaPmAgFnS	schválit
rada	rada	k1gFnSc1	rada
Islingtonu	Islington	k1gInSc2	Islington
plán	plán	k1gInSc1	plán
Arsenalu	Arsenal	k1gInSc2	Arsenal
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
rozvoje	rozvoj	k1gInSc2	rozvoj
Ashburton	Ashburton	k1gInSc1	Ashburton
Grove	Groev	k1gFnPc4	Groev
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
34	[number]	k4	34
radních	radní	k1gMnPc2	radní
<g/>
,	,	kIx,	,
sedm	sedm	k4xCc1	sedm
radních	radní	k1gMnPc2	radní
bylo	být	k5eAaImAgNnS	být
proti	proti	k7c3	proti
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
se	se	k3xPyFc4	se
zdržel	zdržet	k5eAaPmAgMnS	zdržet
hlasování	hlasování	k1gNnSc4	hlasování
<g/>
.	.	kIx.	.
31	[number]	k4	31
lidí	člověk	k1gMnPc2	člověk
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
pro	pro	k7c4	pro
přemístění	přemístění	k1gNnSc4	přemístění
recyklační	recyklační	k2eAgFnSc2d1	recyklační
továrny	továrna	k1gFnSc2	továrna
na	na	k7c4	na
Lough	Lough	k1gInSc4	Lough
Road	Roada	k1gFnPc2	Roada
<g/>
,	,	kIx,	,
8	[number]	k4	8
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
proti	proti	k7c3	proti
<g/>
.	.	kIx.	.
</s>
<s>
Závěrečné	závěrečný	k2eAgNnSc1d1	závěrečné
hlasování	hlasování	k1gNnSc1	hlasování
bylo	být	k5eAaImAgNnS	být
ratifikováno	ratifikovat	k5eAaBmNgNnS	ratifikovat
starostou	starosta	k1gMnSc7	starosta
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
Kenem	Ken	k1gInSc7	Ken
Livingstonem	Livingston	k1gInSc7	Livingston
<g/>
,	,	kIx,	,
a	a	k8xC	a
ministrem	ministr	k1gMnSc7	ministr
dopravy	doprava	k1gFnSc2	doprava
<g/>
,	,	kIx,	,
Stephenem	Stephen	k1gMnSc7	Stephen
Byersem	Byers	k1gMnSc7	Byers
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2002	[number]	k4	2002
dostal	dostat	k5eAaPmAgMnS	dostat
Arsenal	Arsenal	k1gFnSc4	Arsenal
"	"	kIx"	"
<g/>
zelenou	zelený	k2eAgFnSc4d1	zelená
<g/>
"	"	kIx"	"
a	a	k8xC	a
mohlo	moct	k5eAaImAgNnS	moct
se	se	k3xPyFc4	se
začít	začít	k5eAaPmF	začít
s	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
<g/>
,	,	kIx,	,
když	když	k8xS	když
poslední	poslední	k2eAgFnSc1d1	poslední
stížnost	stížnost	k1gFnSc1	stížnost
místních	místní	k2eAgMnPc2d1	místní
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
uskupení	uskupení	k1gNnSc2	uskupení
ISCA	ISCA	kA	ISCA
zamítl	zamítnout	k5eAaPmAgMnS	zamítnout
soudce	soudce	k1gMnSc1	soudce
u	u	k7c2	u
vrchního	vrchní	k2eAgInSc2d1	vrchní
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2005	[number]	k4	2005
klub	klub	k1gInSc1	klub
uspěl	uspět	k5eAaPmAgInS	uspět
proti	proti	k7c3	proti
dalšímu	další	k2eAgInSc3d1	další
pokusu	pokus	k1gInSc3	pokus
o	o	k7c4	o
zastavení	zastavení	k1gNnSc4	zastavení
výstavby	výstavba	k1gFnSc2	výstavba
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
malých	malý	k2eAgFnPc2d1	malá
firem	firma	k1gFnPc2	firma
<g/>
.	.	kIx.	.
</s>
<s>
Vrchní	vrchní	k2eAgInSc1d1	vrchní
soud	soud	k1gInSc1	soud
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
vicepremiéra	vicepremiér	k1gMnSc2	vicepremiér
<g/>
,	,	kIx,	,
Johna	John	k1gMnSc2	John
Prescotta	Prescott	k1gMnSc2	Prescott
<g/>
,	,	kIx,	,
o	o	k7c6	o
podpoře	podpora	k1gFnSc6	podpora
programu	program	k1gInSc2	program
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
místních	místní	k2eAgFnPc6d1	místní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
stadion	stadion	k1gInSc4	stadion
hlavním	hlavní	k2eAgInSc7d1	hlavní
problémem	problém	k1gInSc7	problém
<g/>
.	.	kIx.	.
</s>
<s>
Londýnská	londýnský	k2eAgFnSc1d1	londýnská
městská	městský	k2eAgFnSc1d1	městská
policie	policie	k1gFnSc1	policie
požadovala	požadovat	k5eAaImAgFnS	požadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
autobusy	autobus	k1gInPc1	autobus
s	s	k7c7	s
fanoušky	fanoušek	k1gMnPc7	fanoušek
parkovaly	parkovat	k5eAaImAgFnP	parkovat
v	v	k7c6	v
nedalekém	daleký	k2eNgNnSc6d1	nedaleké
sportovním	sportovní	k2eAgNnSc6d1	sportovní
centru	centrum	k1gNnSc6	centrum
Sobel	Sobel	k1gInSc4	Sobel
a	a	k8xC	a
ne	ne	k9	ne
v	v	k7c6	v
podzemním	podzemní	k2eAgNnSc6d1	podzemní
parkovišti	parkoviště	k1gNnSc6	parkoviště
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
by	by	kYmCp3nS	by
byl	být	k5eAaImAgMnS	být
v	v	k7c4	v
den	den	k1gInSc4	den
zápasu	zápas	k1gInSc2	zápas
omezen	omezen	k2eAgInSc4d1	omezen
provoz	provoz	k1gInSc4	provoz
ve	v	k7c6	v
14	[number]	k4	14
ulicích	ulice	k1gFnPc6	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
stadion	stadion	k1gInSc1	stadion
nesplňoval	splňovat	k5eNaImAgInS	splňovat
tyto	tento	k3xDgFnPc4	tento
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgMnS	být
by	by	kYmCp3nS	by
klubu	klub	k1gInSc2	klub
vydán	vydat	k5eAaPmNgInS	vydat
certifikát	certifikát	k1gInSc1	certifikát
o	o	k7c6	o
bezpečnosti	bezpečnost	k1gFnSc6	bezpečnost
a	a	k8xC	a
ochraně	ochrana	k1gFnSc6	ochrana
zdraví	zdraví	k1gNnSc2	zdraví
na	na	k7c6	na
pracovišti	pracoviště	k1gNnSc6	pracoviště
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
by	by	kYmCp3nS	by
stadion	stadion	k1gInSc1	stadion
nemohl	moct	k5eNaImAgInS	moct
být	být	k5eAaImF	být
otevřen	otevřít	k5eAaPmNgInS	otevřít
<g/>
.	.	kIx.	.
</s>
<s>
Uzavírky	uzavírka	k1gFnPc1	uzavírka
ulic	ulice	k1gFnPc2	ulice
byly	být	k5eAaImAgFnP	být
předány	předán	k2eAgFnPc1d1	předána
na	na	k7c6	na
zasedání	zasedání	k1gNnSc6	zasedání
rady	rada	k1gFnSc2	rada
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
i	i	k9	i
tak	tak	k6eAd1	tak
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
předmětem	předmět	k1gInSc7	předmět
zkoumání	zkoumání	k1gNnSc2	zkoumání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Financování	financování	k1gNnSc1	financování
a	a	k8xC	a
název	název	k1gInSc1	název
===	===	k?	===
</s>
</p>
<p>
<s>
Financování	financování	k1gNnSc1	financování
projektu	projekt	k1gInSc2	projekt
se	se	k3xPyFc4	se
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
jako	jako	k9	jako
obtížné	obtížný	k2eAgNnSc1d1	obtížné
<g/>
.	.	kIx.	.
</s>
<s>
Arsenal	Arsenat	k5eAaImAgMnS	Arsenat
totiž	totiž	k9	totiž
od	od	k7c2	od
vlády	vláda	k1gFnSc2	vláda
neobdržel	obdržet	k5eNaPmAgMnS	obdržet
žádnou	žádný	k3yNgFnSc4	žádný
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
dotaci	dotace	k1gFnSc4	dotace
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
proto	proto	k8xC	proto
začal	začít	k5eAaPmAgInS	začít
vyhledávat	vyhledávat	k5eAaImF	vyhledávat
jiné	jiný	k2eAgInPc4d1	jiný
zdroje	zdroj	k1gInPc4	zdroj
příjmů	příjem	k1gInPc2	příjem
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétně	konkrétně	k6eAd1	konkrétně
nákupem	nákup	k1gInSc7	nákup
nových	nový	k2eAgMnPc2d1	nový
hráčů	hráč	k1gMnPc2	hráč
za	za	k7c4	za
nízkou	nízký	k2eAgFnSc4d1	nízká
přestupní	přestupní	k2eAgFnSc4d1	přestupní
částku	částka	k1gFnSc4	částka
<g/>
,	,	kIx,	,
prodejem	prodej	k1gInSc7	prodej
vlastních	vlastní	k2eAgMnPc2d1	vlastní
hráčů	hráč	k1gMnPc2	hráč
za	za	k7c4	za
vysoké	vysoký	k2eAgFnPc4d1	vysoká
ceny	cena	k1gFnPc4	cena
a	a	k8xC	a
podpisy	podpis	k1gInPc4	podpis
sponzorských	sponzorský	k2eAgFnPc2d1	sponzorská
smluv	smlouva	k1gFnPc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Arsenalu	Arsenala	k1gFnSc4	Arsenala
se	se	k3xPyFc4	se
z	z	k7c2	z
přestupů	přestup	k1gInPc2	přestup
Nicolase	Nicolasa	k1gFnSc3	Nicolasa
Anelky	Anelka	k1gMnSc2	Anelka
a	a	k8xC	a
Marca	Marcus	k1gMnSc2	Marcus
Overmarse	Overmarse	k1gFnSc2	Overmarse
podařilo	podařit	k5eAaPmAgNnS	podařit
získat	získat	k5eAaPmF	získat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
£	£	k?	£
<g/>
.	.	kIx.	.
</s>
<s>
Anelka	Anelka	k1gMnSc1	Anelka
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
do	do	k7c2	do
Realu	Real	k1gInSc2	Real
Madrid	Madrid	k1gInSc1	Madrid
a	a	k8xC	a
Overmars	Overmars	k1gInSc1	Overmars
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgInS	připojit
k	k	k7c3	k
Barceloně	Barcelona	k1gFnSc3	Barcelona
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
bývalým	bývalý	k2eAgMnSc7d1	bývalý
spoluhráčem	spoluhráč	k1gMnSc7	spoluhráč
<g/>
,	,	kIx,	,
Emmanuelem	Emmanuel	k1gMnSc7	Emmanuel
Petitem	petit	k1gInSc7	petit
<g/>
.	.	kIx.	.
</s>
<s>
Přestup	přestup	k1gInSc1	přestup
Anelky	Anelka	k1gFnSc2	Anelka
pomohl	pomoct	k5eAaPmAgInS	pomoct
při	při	k7c6	při
výstavbě	výstavba	k1gFnSc6	výstavba
nového	nový	k2eAgNnSc2d1	nové
tréninkového	tréninkový	k2eAgNnSc2d1	tréninkové
centra	centrum	k1gNnSc2	centrum
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1999	[number]	k4	1999
v	v	k7c6	v
londýnském	londýnský	k2eAgNnSc6d1	Londýnské
Colney	Colneum	k1gNnPc7	Colneum
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
září	září	k1gNnSc6	září
2000	[number]	k4	2000
koupila	koupit	k5eAaPmAgFnS	koupit
Granada	Granada	k1gFnSc1	Granada
Media	medium	k1gNnSc2	medium
Group	Group	k1gInSc4	Group
5	[number]	k4	5
%	%	kIx~	%
akcií	akcie	k1gFnPc2	akcie
Arsenalu	Arsenal	k1gInSc2	Arsenal
za	za	k7c4	za
47	[number]	k4	47
milionů	milion	k4xCgInPc2	milion
£	£	k?	£
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
akvizice	akvizice	k1gFnSc2	akvizice
se	se	k3xPyFc4	se
Granada	Granada	k1gFnSc1	Granada
stala	stát	k5eAaPmAgFnS	stát
hlavním	hlavní	k2eAgMnSc7d1	hlavní
mediálním	mediální	k2eAgMnSc7d1	mediální
zástupcem	zástupce	k1gMnSc7	zástupce
klubu	klub	k1gInSc2	klub
<g/>
,	,	kIx,	,
starala	starat	k5eAaImAgFnS	starat
se	se	k3xPyFc4	se
o	o	k7c4	o
reklamu	reklama	k1gFnSc4	reklama
<g/>
,	,	kIx,	,
sponzoring	sponzoring	k1gInSc4	sponzoring
<g/>
,	,	kIx,	,
propagaci	propagace	k1gFnSc4	propagace
a	a	k8xC	a
licenční	licenční	k2eAgFnSc2d1	licenční
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
dále	daleko	k6eAd2	daleko
do	do	k7c2	do
klubu	klub	k1gInSc2	klub
investovala	investovat	k5eAaBmAgFnS	investovat
20	[number]	k4	20
milionů	milion	k4xCgInPc2	milion
£	£	k?	£
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
AFC	AFC	kA	AFC
Broadband	Broadband	k1gInSc4	Broadband
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
zabývala	zabývat	k5eAaImAgFnS	zabývat
návštěvností	návštěvnost	k1gFnSc7	návštěvnost
obsahu	obsah	k1gInSc2	obsah
týkajícího	týkající	k2eAgInSc2d1	týkající
se	se	k3xPyFc4	se
Arsenalu	Arsenal	k1gInSc2	Arsenal
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
Arsenalu	Arsenal	k1gInSc2	Arsenal
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Hill-Wood	Hill-Wooda	k1gFnPc2	Hill-Wooda
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
smlouvě	smlouva	k1gFnSc3	smlouva
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Toto	tento	k3xDgNnSc1	tento
partnerství	partnerství	k1gNnSc1	partnerství
nám	my	k3xPp1nPc3	my
pomůže	pomoct	k5eAaPmIp3nS	pomoct
při	při	k7c6	při
plnění	plnění	k1gNnSc6	plnění
dvou	dva	k4xCgInPc2	dva
strategických	strategický	k2eAgInPc2d1	strategický
plánů	plán	k1gInPc2	plán
Arsenalu	Arsenal	k1gInSc2	Arsenal
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
prvé	prvý	k4xOgNnSc4	prvý
<g/>
,	,	kIx,	,
vybudování	vybudování	k1gNnSc1	vybudování
nového	nový	k2eAgInSc2d1	nový
prvotřídního	prvotřídní	k2eAgInSc2d1	prvotřídní
stadionu	stadion	k1gInSc2	stadion
s	s	k7c7	s
vyšší	vysoký	k2eAgFnSc7d2	vyšší
kapacitou	kapacita	k1gFnSc7	kapacita
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
více	hodně	k6eAd2	hodně
našich	náš	k3xOp1gMnPc2	náš
fanoušků	fanoušek	k1gMnPc2	fanoušek
mohlo	moct	k5eAaImAgNnS	moct
sledovat	sledovat	k5eAaImF	sledovat
zápasy	zápas	k1gInPc4	zápas
svého	svůj	k3xOyFgInSc2	svůj
oblíbeného	oblíbený	k2eAgInSc2d1	oblíbený
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
druhé	druhý	k4xOgNnSc4	druhý
<g/>
,	,	kIx,	,
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
značku	značka	k1gFnSc4	značka
Arsenalu	Arsenal	k1gInSc2	Arsenal
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rozšíření	rozšíření	k1gNnSc2	rozšíření
naší	náš	k3xOp1gFnSc2	náš
domácí	domácí	k2eAgFnSc2d1	domácí
základny	základna	k1gFnSc2	základna
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Výkonný	výkonný	k2eAgMnSc1d1	výkonný
ředitel	ředitel	k1gMnSc1	ředitel
<g/>
,	,	kIx,	,
Keith	Keith	k1gMnSc1	Keith
Edelman	Edelman	k1gMnSc1	Edelman
<g/>
,	,	kIx,	,
ještě	ještě	k6eAd1	ještě
dodal	dodat	k5eAaPmAgMnS	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tato	tento	k3xDgFnSc1	tento
investice	investice	k1gFnSc1	investice
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
použít	použít	k5eAaPmF	použít
přímo	přímo	k6eAd1	přímo
na	na	k7c4	na
financování	financování	k1gNnSc4	financování
nového	nový	k2eAgInSc2d1	nový
stadionu	stadion	k1gInSc2	stadion
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2002	[number]	k4	2002
nastal	nastat	k5eAaPmAgInS	nastat
kolaps	kolaps	k1gInSc1	kolaps
ITV	ITV	kA	ITV
Digital	Digital	kA	Digital
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
z	z	k7c2	z
části	část	k1gFnSc2	část
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
Granada	Granada	k1gFnSc1	Granada
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
musela	muset	k5eAaImAgFnS	muset
Granada	Granada	k1gFnSc1	Granada
zaplatit	zaplatit	k5eAaPmF	zaplatit
30	[number]	k4	30
milionů	milion	k4xCgInPc2	milion
£	£	k?	£
hned	hned	k6eAd1	hned
potom	potom	k6eAd1	potom
<g/>
,	,	kIx,	,
co	co	k9	co
byly	být	k5eAaImAgFnP	být
dokončeny	dokončit	k5eAaPmNgFnP	dokončit
opatření	opatření	k1gNnPc4	opatření
pro	pro	k7c4	pro
nový	nový	k2eAgInSc4d1	nový
stadion	stadion	k1gInSc4	stadion
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
byla	být	k5eAaImAgFnS	být
za	za	k7c4	za
rozpočtový	rozpočtový	k2eAgInSc4d1	rozpočtový
rok	rok	k1gInSc4	rok
2001	[number]	k4	2001
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
ohlášena	ohlášen	k2eAgFnSc1d1	ohlášena
ztráta	ztráta	k1gFnSc1	ztráta
22,3	[number]	k4	22,3
milionů	milion	k4xCgInPc2	milion
£	£	k?	£
před	před	k7c7	před
zdaněním	zdanění	k1gNnSc7	zdanění
<g/>
,	,	kIx,	,
klub	klub	k1gInSc1	klub
představil	představit	k5eAaPmAgInS	představit
plán	plán	k1gInSc4	plán
na	na	k7c4	na
snížení	snížení	k1gNnSc4	snížení
mzdových	mzdový	k2eAgInPc2d1	mzdový
nákladů	náklad	k1gInPc2	náklad
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
dále	daleko	k6eAd2	daleko
pokračovat	pokračovat	k5eAaImF	pokračovat
s	s	k7c7	s
prací	práce	k1gFnSc7	práce
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
března	březen	k1gInSc2	březen
2003	[number]	k4	2003
byla	být	k5eAaImAgFnS	být
oslovena	osloven	k2eAgFnSc1d1	oslovena
investiční	investiční	k2eAgFnSc1d1	investiční
banka	banka	k1gFnSc1	banka
NM	NM	kA	NM
Rothschild	Rothschild	k1gMnSc1	Rothschild
and	and	k?	and
Sons	Sons	k1gInSc1	Sons
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
analyzovala	analyzovat	k5eAaImAgFnS	analyzovat
finanční	finanční	k2eAgFnSc4d1	finanční
situaci	situace	k1gFnSc4	situace
v	v	k7c6	v
klubu	klub	k1gInSc6	klub
a	a	k8xC	a
poradila	poradit	k5eAaPmAgFnS	poradit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
Arsenal	Arsenal	k1gMnSc4	Arsenal
proveditelné	proveditelný	k2eAgNnSc1d1	proveditelné
pokračovat	pokračovat	k5eAaImF	pokračovat
s	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
stadionu	stadion	k1gInSc2	stadion
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
si	se	k3xPyFc3	se
Arsenal	Arsenal	k1gMnSc1	Arsenal
zajistil	zajistit	k5eAaPmAgMnS	zajistit
půjčku	půjčka	k1gFnSc4	půjčka
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
260	[number]	k4	260
milionů	milion	k4xCgInPc2	milion
£	£	k?	£
od	od	k7c2	od
skupiny	skupina	k1gFnSc2	skupina
bank	banka	k1gFnPc2	banka
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Royal	Royal	k1gMnSc1	Royal
Bank	bank	k1gInSc1	bank
of	of	k?	of
Scotland	Scotland	k1gInSc1	Scotland
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2003	[number]	k4	2003
byly	být	k5eAaImAgFnP	být
práce	práce	k1gFnPc1	práce
v	v	k7c4	v
Ashburton	Ashburton	k1gInSc4	Ashburton
Grove	Groev	k1gFnSc2	Groev
pozastaveny	pozastavit	k5eAaPmNgFnP	pozastavit
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
vydal	vydat	k5eAaPmAgInS	vydat
prohlášení	prohlášení	k1gNnSc4	prohlášení
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
V	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
měsících	měsíc	k1gInPc6	měsíc
jsme	být	k5eAaImIp1nP	být
zažili	zažít	k5eAaPmAgMnP	zažít
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
řadě	řada	k1gFnSc6	řada
otázek	otázka	k1gFnPc2	otázka
několik	několik	k4yIc1	několik
zdržení	zdržení	k1gNnPc2	zdržení
v	v	k7c6	v
přípravách	příprava	k1gFnPc6	příprava
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
nového	nový	k2eAgInSc2d1	nový
stadionu	stadion	k1gInSc2	stadion
<g/>
.	.	kIx.	.
</s>
<s>
Vlivem	vlivem	k7c2	vlivem
těchto	tento	k3xDgNnPc2	tento
zdržení	zdržení	k1gNnPc2	zdržení
nebudeme	být	k5eNaImBp1nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
stadion	stadion	k1gInSc4	stadion
otevřít	otevřít	k5eAaPmF	otevřít
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
sezóny	sezóna	k1gFnSc2	sezóna
2005	[number]	k4	2005
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
se	se	k3xPyFc4	se
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
zvýšily	zvýšit	k5eAaPmAgFnP	zvýšit
o	o	k7c4	o
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
£	£	k?	£
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
se	se	k3xPyFc4	se
počítalo	počítat	k5eAaImAgNnS	počítat
s	s	k7c7	s
náklady	náklad	k1gInPc7	náklad
kolem	kolem	k7c2	kolem
400	[number]	k4	400
milionů	milion	k4xCgInPc2	milion
£	£	k?	£
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
snaze	snaha	k1gFnSc6	snaha
bojovat	bojovat	k5eAaImF	bojovat
proti	proti	k7c3	proti
finančním	finanční	k2eAgFnPc3d1	finanční
potížím	potíž	k1gFnPc3	potíž
dal	dát	k5eAaPmAgInS	dát
po	po	k7c4	po
celé	celý	k2eAgNnSc4d1	celé
léto	léto	k1gNnSc4	léto
2003	[number]	k4	2003
Arsenal	Arsenal	k1gFnSc2	Arsenal
fanouškům	fanoušek	k1gMnPc3	fanoušek
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
skrze	skrze	k?	skrze
své	své	k1gNnSc4	své
internetové	internetový	k2eAgFnSc2d1	internetová
stránky	stránka	k1gFnSc2	stránka
možnost	možnost	k1gFnSc1	možnost
zakoupil	zakoupit	k5eAaPmAgInS	zakoupit
klubové	klubový	k2eAgInPc4d1	klubový
dluhopisy	dluhopis	k1gInPc4	dluhopis
<g/>
.	.	kIx.	.
</s>
<s>
Klub	klub	k1gInSc1	klub
plánoval	plánovat	k5eAaImAgInS	plánovat
vydat	vydat	k5eAaPmF	vydat
3000	[number]	k4	3000
dluhopisů	dluhopis	k1gInPc2	dluhopis
v	v	k7c6	v
cenovém	cenový	k2eAgNnSc6d1	cenové
rozpětí	rozpětí	k1gNnSc6	rozpětí
od	od	k7c2	od
3500	[number]	k4	3500
až	až	k9	až
5000	[number]	k4	5000
£	£	k?	£
k	k	k7c3	k
celoročním	celoroční	k2eAgFnPc3d1	celoroční
permanentkám	permanentka	k1gFnPc3	permanentka
na	na	k7c4	na
Highbury	Highbur	k1gInPc4	Highbur
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
na	na	k7c4	na
Ashburton	Ashburton	k1gInSc4	Ashburton
Grove	Groev	k1gFnSc2	Groev
<g/>
.	.	kIx.	.
</s>
<s>
Příznivci	příznivec	k1gMnPc1	příznivec
Arsenalu	Arsenal	k1gInSc2	Arsenal
na	na	k7c6	na
znovuzavedení	znovuzavedení	k1gNnSc6	znovuzavedení
dluhopisů	dluhopis	k1gInPc2	dluhopis
reagovali	reagovat	k5eAaBmAgMnP	reagovat
s	s	k7c7	s
překvapením	překvapení	k1gNnSc7	překvapení
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
AISA	AISA	kA	AISA
<g/>
,	,	kIx,	,
Steven	Steven	k2eAgMnSc1d1	Steven
Powell	Powell	k1gMnSc1	Powell
<g/>
,	,	kIx,	,
dodal	dodat	k5eAaPmAgMnS	dodat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jsme	být	k5eAaImIp1nP	být
zklamaní	zklamaný	k2eAgMnPc1d1	zklamaný
skutečností	skutečnost	k1gFnPc2	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
klub	klub	k1gInSc1	klub
nejdříve	dříve	k6eAd3	dříve
toto	tento	k3xDgNnSc4	tento
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
neprojednal	projednat	k5eNaPmAgMnS	projednat
se	se	k3xPyFc4	se
zástupci	zástupce	k1gMnSc3	zástupce
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
nebylo	být	k5eNaImAgNnS	být
nikdy	nikdy	k6eAd1	nikdy
uvedeno	uvést	k5eAaPmNgNnS	uvést
množství	množství	k1gNnSc1	množství
prodaných	prodaný	k2eAgInPc2d1	prodaný
dluhopisů	dluhopis	k1gInPc2	dluhopis
<g/>
,	,	kIx,	,
i	i	k9	i
tak	tak	k9	tak
Arsenal	Arsenal	k1gFnSc4	Arsenal
vydělal	vydělat	k5eAaPmAgInS	vydělat
několik	několik	k4yIc4	několik
milionů	milion	k4xCgInPc2	milion
liber	libra	k1gFnPc2	libra
<g/>
.	.	kIx.	.
<g/>
Poskytovatel	poskytovatel	k1gMnSc1	poskytovatel
sportovního	sportovní	k2eAgNnSc2d1	sportovní
oblečení	oblečení	k1gNnSc2	oblečení
<g/>
,	,	kIx,	,
Nike	Nike	k1gFnSc2	Nike
<g/>
,	,	kIx,	,
podepsal	podepsat	k5eAaPmAgMnS	podepsat
s	s	k7c7	s
Arsenalem	Arsenal	k1gInSc7	Arsenal
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2003	[number]	k4	2003
prodloužení	prodloužení	k1gNnSc4	prodloužení
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
zůstal	zůstat	k5eAaPmAgInS	zůstat
hlavním	hlavní	k2eAgMnSc7d1	hlavní
klubovým	klubový	k2eAgMnSc7d1	klubový
dodavatelem	dodavatel	k1gMnSc7	dodavatel
sportovního	sportovní	k2eAgInSc2d1	sportovní
úboru	úbor	k1gInSc2	úbor
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
představeno	představen	k2eAgNnSc1d1	představeno
jako	jako	k8xC	jako
nové	nový	k2eAgNnSc1d1	nové
řešení	řešení	k1gNnSc1	řešení
financování	financování	k1gNnSc2	financování
stadionu	stadion	k1gInSc2	stadion
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
zaplacení	zaplacení	k1gNnSc2	zaplacení
55	[number]	k4	55
milionů	milion	k4xCgInPc2	milion
liber	libra	k1gFnPc2	libra
v	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
sedmi	sedm	k4xCc6	sedm
letech	léto	k1gNnPc6	léto
Nike	Nike	k1gFnSc2	Nike
ještě	ještě	k9	ještě
platil	platit	k5eAaImAgInS	platit
klubu	klub	k1gInSc2	klub
minimálně	minimálně	k6eAd1	minimálně
1	[number]	k4	1
milion	milion	k4xCgInSc4	milion
liber	libra	k1gFnPc2	libra
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
prodeji	prodej	k1gInSc6	prodej
oblečení	oblečení	k1gNnSc2	oblečení
<g/>
.	.	kIx.	.
</s>
<s>
Arsenal	Arsenat	k5eAaPmAgInS	Arsenat
Holding	holding	k1gInSc1	holding
(	(	kIx(	(
<g/>
mateřská	mateřský	k2eAgFnSc1d1	mateřská
společnost	společnost	k1gFnSc1	společnost
klubu	klub	k1gInSc2	klub
<g/>
)	)	kIx)	)
23	[number]	k4	23
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2004	[number]	k4	2004
oznámil	oznámit	k5eAaPmAgInS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
byly	být	k5eAaImAgInP	být
zajištěny	zajistit	k5eAaPmNgInP	zajistit
finanční	finanční	k2eAgInPc1d1	finanční
prostředky	prostředek	k1gInPc1	prostředek
potřebné	potřebný	k2eAgInPc1d1	potřebný
k	k	k7c3	k
výstavbě	výstavba	k1gFnSc3	výstavba
stadionu	stadion	k1gInSc2	stadion
a	a	k8xC	a
stavební	stavební	k2eAgFnSc2d1	stavební
práce	práce	k1gFnSc2	práce
budou	být	k5eAaImBp3nP	být
moci	moct	k5eAaImF	moct
pokračovat	pokračovat	k5eAaImF	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Wenger	Wenger	k1gMnSc1	Wenger
se	se	k3xPyFc4	se
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
později	pozdě	k6eAd2	pozdě
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Podílet	podílet	k5eAaImF	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
rozvoji	rozvoj	k1gInSc6	rozvoj
klubu	klub	k1gInSc2	klub
a	a	k8xC	a
přesunu	přesun	k1gInSc2	přesun
na	na	k7c4	na
nový	nový	k2eAgInSc4d1	nový
stadion	stadion	k1gInSc4	stadion
bylo	být	k5eAaImAgNnS	být
nutností	nutnost	k1gFnPc2	nutnost
a	a	k8xC	a
mým	můj	k3xOp1gFnPc3	můj
velkým	velká	k1gFnPc3	velká
cíl	cíl	k1gInSc4	cíl
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
nám	my	k3xPp1nPc3	my
to	ten	k3xDgNnSc1	ten
umožní	umožnit	k5eAaPmIp3nS	umožnit
stát	stát	k5eAaImF	stát
se	se	k3xPyFc4	se
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
klubů	klub	k1gInPc2	klub
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Úroky	úrok	k1gInPc1	úrok
na	na	k7c4	na
dluh	dluh	k1gInSc4	dluh
260	[number]	k4	260
milionů	milion	k4xCgInPc2	milion
liber	libra	k1gFnPc2	libra
byly	být	k5eAaImAgFnP	být
stanoveny	stanovit	k5eAaPmNgFnP	stanovit
pevnou	pevný	k2eAgFnSc7d1	pevná
úrokovou	úrokový	k2eAgFnSc7d1	úroková
sazbou	sazba	k1gFnSc7	sazba
na	na	k7c6	na
období	období	k1gNnSc6	období
14	[number]	k4	14
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
refinancování	refinancování	k1gNnSc3	refinancování
nákladů	náklad	k1gInPc2	náklad
klub	klub	k1gInSc1	klub
plánoval	plánovat	k5eAaImAgInS	plánovat
přenést	přenést	k5eAaPmF	přenést
peníze	peníz	k1gInPc4	peníz
do	do	k7c2	do
30	[number]	k4	30
<g/>
letého	letý	k2eAgInSc2d1	letý
dluhopisu	dluhopis	k1gInSc2	dluhopis
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
by	by	kYmCp3nP	by
byl	být	k5eAaImAgInS	být
financován	financovat	k5eAaBmNgMnS	financovat
bankami	banka	k1gFnPc7	banka
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2006	[number]	k4	2006
byly	být	k5eAaImAgFnP	být
navrženy	navržen	k2eAgFnPc1d1	navržena
emise	emise	k1gFnPc1	emise
dluhopisů	dluhopis	k1gInPc2	dluhopis
<g/>
.	.	kIx.	.
</s>
<s>
Arsenal	Arsenat	k5eAaPmAgMnS	Arsenat
vydal	vydat	k5eAaPmAgMnS	vydat
dluhopisy	dluhopis	k1gInPc7	dluhopis
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
210	[number]	k4	210
milionů	milion	k4xCgInPc2	milion
liber	libra	k1gFnPc2	libra
na	na	k7c4	na
období	období	k1gNnSc4	období
13,5	[number]	k4	13,5
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
navýšily	navýšit	k5eAaPmAgFnP	navýšit
svou	svůj	k3xOyFgFnSc4	svůj
hodnotu	hodnota	k1gFnSc4	hodnota
o	o	k7c4	o
52	[number]	k4	52
bazických	bazický	k2eAgInPc2d1	bazický
bodů	bod	k1gInPc2	bod
vůči	vůči	k7c3	vůči
státním	státní	k2eAgFnPc3d1	státní
dluhopisů	dluhopis	k1gInPc2	dluhopis
a	a	k8xC	a
dluhopisy	dluhopis	k1gInPc1	dluhopis
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
50	[number]	k4	50
milionů	milion	k4xCgInPc2	milion
liber	libra	k1gFnPc2	libra
na	na	k7c4	na
období	období	k1gNnSc4	období
7,1	[number]	k4	7,1
roků	rok	k1gInPc2	rok
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
navýšily	navýšit	k5eAaPmAgFnP	navýšit
svou	svůj	k3xOyFgFnSc4	svůj
hodnotu	hodnota	k1gFnSc4	hodnota
o	o	k7c4	o
22	[number]	k4	22
bazických	bazický	k2eAgInPc2d1	bazický
bodů	bod	k1gInPc2	bod
vůči	vůči	k7c3	vůči
LIBORu	Libor	k1gMnSc3	Libor
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
první	první	k4xOgFnSc4	první
veřejně	veřejně	k6eAd1	veřejně
prodejné	prodejný	k2eAgNnSc4d1	prodejné
zálohované	zálohovaný	k2eAgNnSc4d1	zálohované
aktivum	aktivum	k1gNnSc4	aktivum
emise	emise	k1gFnSc2	emise
dluhopisů	dluhopis	k1gInPc2	dluhopis
vydané	vydaný	k2eAgNnSc1d1	vydané
evropským	evropský	k2eAgInSc7d1	evropský
fotbalovým	fotbalový	k2eAgInSc7d1	fotbalový
klubem	klub	k1gInSc7	klub
<g/>
.	.	kIx.	.
</s>
<s>
Efektivní	efektivní	k2eAgFnSc1d1	efektivní
úroková	úrokový	k2eAgFnSc1d1	úroková
míra	míra	k1gFnSc1	míra
těchto	tento	k3xDgInPc2	tento
dluhopisů	dluhopis	k1gInPc2	dluhopis
je	být	k5eAaImIp3nS	být
5,14	[number]	k4	5,14
%	%	kIx~	%
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
5,97	[number]	k4	5,97
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Splaceny	splacen	k2eAgInPc1d1	splacen
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
v	v	k7c6	v
období	období	k1gNnSc6	období
25	[number]	k4	25
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Přesun	přesun	k1gInSc1	přesun
k	k	k7c3	k
dluhopisům	dluhopis	k1gInPc3	dluhopis
snížil	snížit	k5eAaPmAgMnS	snížit
roční	roční	k2eAgInSc4d1	roční
dluh	dluh	k1gInSc4	dluh
klubu	klub	k1gInSc2	klub
na	na	k7c4	na
přibližnou	přibližný	k2eAgFnSc4d1	přibližná
sumu	suma	k1gFnSc4	suma
20	[number]	k4	20
milionů	milion	k4xCgInPc2	milion
£	£	k?	£
ročně	ročně	k6eAd1	ročně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2010	[number]	k4	2010
Arsenal	Arsenal	k1gMnSc1	Arsenal
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
projekt	projekt	k1gInSc4	projekt
Highbury	Highbura	k1gFnSc2	Highbura
square	square	k1gInSc1	square
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
hlavních	hlavní	k2eAgInPc2d1	hlavní
zdrojů	zdroj	k1gInPc2	zdroj
příjmů	příjem	k1gInPc2	příjem
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
dluhu	dluh	k1gInSc2	dluh
za	za	k7c4	za
stadion	stadion	k1gInSc4	stadion
<g/>
,	,	kIx,	,
generuje	generovat	k5eAaImIp3nS	generovat
výnos	výnos	k1gInSc1	výnos
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
bez	bez	k7c2	bez
dluhů	dluh	k1gInPc2	dluh
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2004	[number]	k4	2004
podepsala	podepsat	k5eAaPmAgFnS	podepsat
společnost	společnost	k1gFnSc1	společnost
Emirates	Emiratesa	k1gFnPc2	Emiratesa
s	s	k7c7	s
Arsenalem	Arsenal	k1gInSc7	Arsenal
15	[number]	k4	15
<g/>
letý	letý	k2eAgInSc1d1	letý
kontrakt	kontrakt	k1gInSc1	kontrakt
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
odhadované	odhadovaný	k2eAgFnSc6d1	odhadovaná
výši	výše	k1gFnSc6	výše
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
£	£	k?	£
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
smlouvy	smlouva	k1gFnSc2	smlouva
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Emirates	Emirates	k1gMnSc1	Emirates
bude	být	k5eAaImBp3nS	být
vlastnit	vlastnit	k5eAaImF	vlastnit
práva	právo	k1gNnPc4	právo
na	na	k7c4	na
pojmenování	pojmenování	k1gNnSc4	pojmenování
stadionu	stadion	k1gInSc2	stadion
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
logo	logo	k1gNnSc1	logo
bude	být	k5eAaImBp3nS	být
zobrazeno	zobrazit	k5eAaPmNgNnS	zobrazit
na	na	k7c6	na
klubových	klubový	k2eAgInPc6d1	klubový
dresech	dres	k1gInPc6	dres
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
měl	mít	k5eAaImAgMnS	mít
Arsenal	Arsenal	k1gFnPc4	Arsenal
na	na	k7c6	na
dresech	dres	k1gInPc6	dres
logo	logo	k1gNnSc1	logo
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgMnPc7	jenž
měl	mít	k5eAaImAgMnS	mít
platný	platný	k2eAgInSc4d1	platný
kontrakt	kontrakt	k1gInSc4	kontrakt
do	do	k7c2	do
sezóny	sezóna	k1gFnSc2	sezóna
2005	[number]	k4	2005
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
stadionu	stadion	k1gInSc2	stadion
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
zkracován	zkracován	k2eAgInSc1d1	zkracován
z	z	k7c2	z
"	"	kIx"	"
<g/>
Emirates	Emirates	k1gInSc1	Emirates
stadium	stadium	k1gNnSc1	stadium
<g/>
"	"	kIx"	"
na	na	k7c4	na
"	"	kIx"	"
<g/>
Emirates	Emirates	k1gInSc4	Emirates
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
někteří	některý	k3yIgMnPc1	některý
fanoušci	fanoušek	k1gMnPc1	fanoušek
používají	používat	k5eAaImIp3nP	používat
název	název	k1gInSc4	název
"	"	kIx"	"
<g/>
Ashburton	Ashburton	k1gInSc1	Ashburton
Grove	Groev	k1gFnSc2	Groev
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
"	"	kIx"	"
<g/>
Grove	Groev	k1gFnSc2	Groev
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
používají	používat	k5eAaImIp3nP	používat
zejména	zejména	k9	zejména
odpůrci	odpůrce	k1gMnPc1	odpůrce
názvu	název	k1gInSc2	název
podle	podle	k7c2	podle
obchodního	obchodní	k2eAgInSc2d1	obchodní
sponzoringu	sponzoring	k1gInSc2	sponzoring
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
předpisům	předpis	k1gInPc3	předpis
UEFA	UEFA	kA	UEFA
vztahujícím	vztahující	k2eAgFnPc3d1	vztahující
se	se	k3xPyFc4	se
k	k	k7c3	k
názvu	název	k1gInSc3	název
stadionu	stadion	k1gInSc2	stadion
podle	podle	k7c2	podle
sponzora	sponzor	k1gMnSc2	sponzor
<g/>
,	,	kIx,	,
nesmí	smět	k5eNaImIp3nS	smět
stadion	stadion	k1gInSc4	stadion
nést	nést	k5eAaImF	nést
během	během	k7c2	během
zápasů	zápas	k1gInPc2	zápas
v	v	k7c6	v
Lize	liga	k1gFnSc6	liga
mistrů	mistr	k1gMnPc2	mistr
název	název	k1gInSc4	název
Emirates	Emiratesa	k1gFnPc2	Emiratesa
stadium	stadium	k1gNnSc1	stadium
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Emirates	Emirates	k1gMnSc1	Emirates
není	být	k5eNaImIp3nS	být
oficiálním	oficiální	k2eAgMnSc7d1	oficiální
sponzorem	sponzor	k1gMnSc7	sponzor
Ligy	liga	k1gFnSc2	liga
mistrů	mistr	k1gMnPc2	mistr
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
opatření	opatření	k1gNnSc1	opatření
se	se	k3xPyFc4	se
netýká	týkat	k5eNaImIp3nS	týkat
například	například	k6eAd1	například
Allianz	Allianz	k1gInSc1	Allianz
Areny	Arena	k1gFnSc2	Arena
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
tomuto	tento	k3xDgNnSc3	tento
pravidlu	pravidlo	k1gNnSc3	pravidlo
již	již	k6eAd1	již
dříve	dříve	k6eAd2	dříve
vyhnula	vyhnout	k5eAaPmAgFnS	vyhnout
<g/>
.	.	kIx.	.
</s>
<s>
UEFA	UEFA	kA	UEFA
nazývá	nazývat	k5eAaImIp3nS	nazývat
stadion	stadion	k1gInSc4	stadion
Arsenal	Arsenal	k1gFnSc2	Arsenal
stadium	stadium	k1gNnSc4	stadium
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgMnS	být
oficiální	oficiální	k2eAgInSc4d1	oficiální
název	název	k1gInSc4	název
bývalého	bývalý	k2eAgInSc2d1	bývalý
stadionu	stadion	k1gInSc2	stadion
v	v	k7c4	v
Highbury	Highbura	k1gFnPc4	Highbura
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2012	[number]	k4	2012
Arsenal	Arsenal	k1gMnSc7	Arsenal
společně	společně	k6eAd1	společně
s	s	k7c7	s
Emirates	Emirates	k1gMnSc1	Emirates
oznámili	oznámit	k5eAaPmAgMnP	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
byla	být	k5eAaImAgFnS	být
podepsána	podepsán	k2eAgFnSc1d1	podepsána
nová	nový	k2eAgFnSc1d1	nová
sponzorská	sponzorský	k2eAgFnSc1d1	sponzorská
smlouva	smlouva	k1gFnSc1	smlouva
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
150	[number]	k4	150
milionů	milion	k4xCgInPc2	milion
£	£	k?	£
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
partnerství	partnerství	k1gNnSc1	partnerství
udrží	udržet	k5eAaPmIp3nS	udržet
logo	logo	k1gNnSc4	logo
Emirates	Emiratesa	k1gFnPc2	Emiratesa
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
sponzora	sponzor	k1gMnSc2	sponzor
klubu	klub	k1gInSc2	klub
<g/>
,	,	kIx,	,
na	na	k7c6	na
dresech	dres	k1gInPc6	dres
a	a	k8xC	a
na	na	k7c6	na
tréninkovém	tréninkový	k2eAgNnSc6d1	tréninkové
oblečení	oblečení	k1gNnSc6	oblečení
dalších	další	k2eAgNnPc2d1	další
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Smlouva	smlouva	k1gFnSc1	smlouva
rovněž	rovněž	k9	rovněž
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
Emirates	Emirates	k1gInSc1	Emirates
práva	právo	k1gNnSc2	právo
na	na	k7c4	na
pojmenování	pojmenování	k1gNnSc4	pojmenování
stadionu	stadion	k1gInSc2	stadion
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2028	[number]	k4	2028
<g/>
.	.	kIx.	.
</s>
<s>
Platební	platební	k2eAgFnPc1d1	platební
podmínky	podmínka	k1gFnPc1	podmínka
byly	být	k5eAaImAgFnP	být
předloženy	předložit	k5eAaPmNgFnP	předložit
pro	pro	k7c4	pro
možnou	možný	k2eAgFnSc4d1	možná
brzkou	brzký	k2eAgFnSc4d1	brzká
investici	investice	k1gFnSc4	investice
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
celkový	celkový	k2eAgInSc1d1	celkový
rozsah	rozsah	k1gInSc1	rozsah
smlouvy	smlouva	k1gFnSc2	smlouva
nebyl	být	k5eNaImAgInS	být
odhalen	odhalit	k5eAaPmNgInS	odhalit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Výstavba	výstavba	k1gFnSc1	výstavba
a	a	k8xC	a
otevření	otevření	k1gNnSc1	otevření
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2004	[number]	k4	2004
byla	být	k5eAaImAgFnS	být
započata	započat	k2eAgFnSc1d1	započata
skutečná	skutečný	k2eAgFnSc1d1	skutečná
výstavba	výstavba	k1gFnSc1	výstavba
stadionu	stadion	k1gInSc2	stadion
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2002	[number]	k4	2002
Arsenal	Arsenal	k1gMnSc1	Arsenal
najal	najmout	k5eAaPmAgMnS	najmout
firmu	firma	k1gFnSc4	firma
Sir	sir	k1gMnSc1	sir
Robert	Robert	k1gMnSc1	Robert
McAlpine	McAlpin	k1gInSc5	McAlpin
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
provedla	provést	k5eAaPmAgFnS	provést
stavební	stavební	k2eAgFnPc4d1	stavební
práce	práce	k1gFnPc4	práce
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
<g/>
.	.	kIx.	.
</s>
<s>
Stadion	stadion	k1gInSc1	stadion
byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
společností	společnost	k1gFnSc7	společnost
Populous	Populous	k1gInSc4	Populous
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
proslavila	proslavit	k5eAaPmAgFnS	proslavit
především	především	k6eAd1	především
návrhem	návrh	k1gInSc7	návrh
stadionu	stadion	k1gInSc2	stadion
Australia	Australium	k1gNnSc2	Australium
a	a	k8xC	a
přestavbou	přestavba	k1gFnSc7	přestavba
ascotské	ascotský	k2eAgFnSc2d1	ascotský
závodní	závodní	k2eAgFnSc2d1	závodní
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
procesu	proces	k1gInSc6	proces
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
angažovali	angažovat	k5eAaBmAgMnP	angažovat
konstrukční	konstrukční	k2eAgMnPc1d1	konstrukční
poradci	poradce	k1gMnPc1	poradce
z	z	k7c2	z
firmy	firma	k1gFnSc2	firma
Arcadis	Arcadis	k1gFnSc2	Arcadis
a	a	k8xC	a
ze	z	k7c2	z
společnosti	společnost	k1gFnSc2	společnost
Buro	Buro	k?	Buro
Happold	Happold	k1gInSc1	Happold
<g/>
.	.	kIx.	.
<g/>
První	první	k4xOgInSc4	první
fází	fáze	k1gFnPc2	fáze
byla	být	k5eAaImAgFnS	být
demolice	demolice	k1gFnSc1	demolice
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
dva	dva	k4xCgInPc4	dva
měsíce	měsíc	k1gInPc4	měsíc
později	pozdě	k6eAd2	pozdě
už	už	k6eAd1	už
stály	stát	k5eAaImAgInP	stát
první	první	k4xOgInPc1	první
sloupy	sloup	k1gInPc1	sloup
západní	západní	k2eAgFnSc2d1	západní
<g/>
,	,	kIx,	,
východní	východní	k2eAgFnSc2d1	východní
a	a	k8xC	a
severní	severní	k2eAgFnSc2d1	severní
tribuny	tribuna	k1gFnSc2	tribuna
(	(	kIx(	(
<g/>
West	West	k1gMnSc1	West
<g/>
,	,	kIx,	,
East	East	k1gMnSc1	East
a	a	k8xC	a
North	North	k1gMnSc1	North
stand	stand	k?	stand
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2004	[number]	k4	2004
byly	být	k5eAaImAgInP	být
dokončeny	dokončen	k2eAgInPc1d1	dokončen
dva	dva	k4xCgInPc1	dva
mosty	most	k1gInPc1	most
přes	přes	k7c4	přes
železniční	železniční	k2eAgFnSc4d1	železniční
trať	trať	k1gFnSc4	trať
Northern	Northerna	k1gFnPc2	Northerna
City	City	k1gFnSc2	City
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
spojení	spojení	k1gNnSc3	spojení
stadionu	stadion	k1gInSc2	stadion
s	s	k7c7	s
Drayton	Drayton	k1gInSc1	Drayton
Parkem	park	k1gInSc7	park
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2005	[number]	k4	2005
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
výstavba	výstavba	k1gFnSc1	výstavba
stadionu	stadion	k1gInSc2	stadion
svého	svůj	k3xOyFgInSc2	svůj
vrcholu	vrchol	k1gInSc2	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2005	[number]	k4	2005
již	již	k6eAd1	již
bylo	být	k5eAaImAgNnS	být
hotové	hotový	k2eAgNnSc1d1	hotové
venkovní	venkovní	k2eAgNnSc1d1	venkovní
zasklení	zasklení	k1gNnSc1	zasklení
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
zařízena	zařídit	k5eAaPmNgFnS	zařídit
elektrická	elektrický	k2eAgFnSc1d1	elektrická
síť	síť	k1gFnSc1	síť
a	a	k8xC	a
vodovod	vodovod	k1gInSc1	vodovod
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
sedadlo	sedadlo	k1gNnSc1	sedadlo
na	na	k7c6	na
novém	nový	k2eAgInSc6d1	nový
stadionu	stadion	k1gInSc6	stadion
slavnostně	slavnostně	k6eAd1	slavnostně
instaloval	instalovat	k5eAaBmAgInS	instalovat
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2006	[number]	k4	2006
záložník	záložník	k1gMnSc1	záložník
Arsenalu	Arsenala	k1gFnSc4	Arsenala
<g/>
,	,	kIx,	,
Abou	Aboa	k1gFnSc4	Aboa
Diaby	Diaba	k1gFnSc2	Diaba
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
na	na	k7c4	na
Highbury	Highbur	k1gInPc4	Highbur
byl	být	k5eAaImAgInS	být
i	i	k9	i
na	na	k7c4	na
Emirates	Emirates	k1gInSc4	Emirates
vybrán	vybrán	k2eAgInSc4d1	vybrán
trávník	trávník	k1gInSc4	trávník
Desso	Dessa	k1gFnSc5	Dessa
GrassMaster	GrassMastrum	k1gNnPc2	GrassMastrum
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
návrhu	návrh	k1gInSc3	návrh
a	a	k8xC	a
instalaci	instalace	k1gFnSc3	instalace
hrací	hrací	k2eAgFnSc2d1	hrací
plochy	plocha	k1gFnSc2	plocha
byla	být	k5eAaImAgFnS	být
najata	najat	k2eAgFnSc1d1	najata
společnost	společnost	k1gFnSc1	společnost
Hewitt	Hewitt	k1gMnSc1	Hewitt
Sportsturf	Sportsturf	k1gMnSc1	Sportsturf
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
úspěšnému	úspěšný	k2eAgInSc3d1	úspěšný
otestování	otestování	k1gNnSc3	otestování
osvětlení	osvětlení	k1gNnSc1	osvětlení
stadionu	stadion	k1gInSc2	stadion
došlo	dojít	k5eAaPmAgNnS	dojít
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc4d1	následující
den	den	k1gInSc4	den
byly	být	k5eAaImAgFnP	být
poté	poté	k6eAd1	poté
postaveny	postaven	k2eAgFnPc1d1	postavena
brankové	brankový	k2eAgFnPc1d1	branková
konstrukce	konstrukce	k1gFnPc1	konstrukce
<g/>
.	.	kIx.	.
<g/>
Pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
licence	licence	k1gFnSc2	licence
se	se	k3xPyFc4	se
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
musely	muset	k5eAaImAgFnP	muset
konat	konat	k5eAaImF	konat
tři	tři	k4xCgFnPc1	tři
události	událost	k1gFnPc1	událost
s	s	k7c7	s
omezeným	omezený	k2eAgInSc7d1	omezený
počtem	počet	k1gInSc7	počet
diváků	divák	k1gMnPc2	divák
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
konal	konat	k5eAaImAgInS	konat
den	den	k1gInSc1	den
otevřených	otevřený	k2eAgFnPc2d1	otevřená
dveří	dveře	k1gFnPc2	dveře
pro	pro	k7c4	pro
akcionáře	akcionář	k1gMnPc4	akcionář
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
otevřený	otevřený	k2eAgInSc4d1	otevřený
trénink	trénink	k1gInSc4	trénink
<g/>
,	,	kIx,	,
na	na	k7c4	na
který	který	k3yRgInSc4	který
bylo	být	k5eAaImAgNnS	být
pozváno	pozván	k2eAgNnSc4d1	pozváno
20	[number]	k4	20
000	[number]	k4	000
členů	člen	k1gInPc2	člen
fanklubu	fanklub	k1gInSc2	fanklub
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
utkání	utkání	k1gNnSc1	utkání
zde	zde	k6eAd1	zde
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
22	[number]	k4	22
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
–	–	k?	–
rozlučkový	rozlučkový	k2eAgInSc1d1	rozlučkový
zápas	zápas	k1gInSc1	zápas
Dennise	Dennise	k1gFnSc1	Dennise
Bergkampa	Bergkampa	k1gFnSc1	Bergkampa
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
proti	proti	k7c3	proti
Arsenalu	Arsenal	k1gMnSc3	Arsenal
hrál	hrát	k5eAaImAgInS	hrát
Bergkampův	Bergkampův	k2eAgInSc1d1	Bergkampův
původní	původní	k2eAgInSc1d1	původní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
AFC	AFC	kA	AFC
Ajax	Ajax	k1gInSc1	Ajax
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
utkání	utkání	k1gNnSc6	utkání
bylo	být	k5eAaImAgNnS	být
54	[number]	k4	54
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
<g/>
,	,	kIx,	,
Arsenal	Arsenal	k1gMnSc1	Arsenal
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
první	první	k4xOgInSc4	první
gól	gól	k1gInSc4	gól
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
hráč	hráč	k1gMnSc1	hráč
Ajaxu	Ajax	k1gInSc2	Ajax
<g/>
,	,	kIx,	,
Klaas	Klaas	k1gMnSc1	Klaas
Jan	Jan	k1gMnSc1	Jan
Huntelaar	Huntelaar	k1gMnSc1	Huntelaar
(	(	kIx(	(
<g/>
ve	v	k7c6	v
druhém	druhý	k4xOgInSc6	druhý
poločase	poločas	k1gInSc6	poločas
vstřelil	vstřelit	k5eAaPmAgMnS	vstřelit
první	první	k4xOgInSc4	první
gól	gól	k1gInSc4	gól
Arsenalu	Arsenal	k1gInSc2	Arsenal
na	na	k7c6	na
novém	nový	k2eAgNnSc6d1	nové
hřišti	hřiště	k1gNnSc6	hřiště
Thierry	Thierra	k1gFnSc2	Thierra
Henry	henry	k1gInSc2	henry
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Oficiálně	oficiálně	k6eAd1	oficiálně
byl	být	k5eAaImAgInS	být
stadion	stadion	k1gInSc1	stadion
otevřen	otevřít	k5eAaPmNgInS	otevřít
na	na	k7c6	na
slavnosti	slavnost	k1gFnSc6	slavnost
26	[number]	k4	26
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
za	za	k7c2	za
účasti	účast	k1gFnSc2	účast
prince	princ	k1gMnSc2	princ
Philipa	Philip	k1gMnSc2	Philip
<g/>
,	,	kIx,	,
vévody	vévoda	k1gMnSc2	vévoda
z	z	k7c2	z
Edinburghu	Edinburgh	k1gInSc2	Edinburgh
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
slavnosti	slavnost	k1gFnSc2	slavnost
měla	mít	k5eAaImAgFnS	mít
účastnit	účastnit	k5eAaImF	účastnit
i	i	k9	i
královna	královna	k1gFnSc1	královna
Alžběta	Alžběta	k1gFnSc1	Alžběta
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
zranění	zranění	k1gNnSc1	zranění
zad	záda	k1gNnPc2	záda
se	se	k3xPyFc4	se
nebyla	být	k5eNaImAgFnS	být
schopna	schopen	k2eAgFnSc1d1	schopna
zúčastnit	zúčastnit	k5eAaPmF	zúčastnit
<g/>
.	.	kIx.	.
</s>
<s>
Princ	princ	k1gMnSc1	princ
Philip	Philip	k1gInSc4	Philip
vtipkoval	vtipkovat	k5eAaImAgMnS	vtipkovat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
davu	dav	k1gInSc3	dav
a	a	k8xC	a
pronesl	pronést	k5eAaPmAgMnS	pronést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
No	no	k9	no
<g/>
,	,	kIx,	,
možná	možná	k9	možná
nebudete	být	k5eNaImBp2nP	být
mít	mít	k5eAaImF	mít
moji	můj	k3xOp1gFnSc4	můj
ženu	žena	k1gFnSc4	žena
<g/>
,	,	kIx,	,
zato	zato	k6eAd1	zato
máte	mít	k5eAaImIp2nP	mít
odhalenou	odhalený	k2eAgFnSc7d1	odhalená
druhou	druhý	k4xOgFnSc4	druhý
nejzkušenější	zkušený	k2eAgFnSc4d3	nejzkušenější
plaketu	plaketa	k1gFnSc4	plaketa
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Královská	královský	k2eAgFnSc1d1	královská
návštěva	návštěva	k1gFnSc1	návštěva
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
po	po	k7c6	po
více	hodně	k6eAd2	hodně
než	než	k8xS	než
půl	půl	k1xP	půl
století	století	k1gNnSc2	století
zopakovala	zopakovat	k5eAaPmAgFnS	zopakovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
při	při	k7c6	při
slavnostním	slavnostní	k2eAgNnSc6d1	slavnostní
otevření	otevření	k1gNnSc6	otevření
západní	západní	k2eAgFnSc2d1	západní
tribuny	tribuna	k1gFnSc2	tribuna
Highbury	Highbura	k1gFnSc2	Highbura
(	(	kIx(	(
<g/>
West	West	k1gInSc1	West
Stand	Standa	k1gFnPc2	Standa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
slavnosti	slavnost	k1gFnSc2	slavnost
účastnil	účastnit	k5eAaImAgMnS	účastnit
královnin	královnin	k2eAgMnSc1d1	královnin
strýc	strýc	k1gMnSc1	strýc
<g/>
,	,	kIx,	,
Princ	princ	k1gMnSc1	princ
z	z	k7c2	z
Walesu	Wales	k1gInSc2	Wales
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
král	král	k1gMnSc1	král
Eduard	Eduard	k1gMnSc1	Eduard
VIII	VIII	kA	VIII
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
změny	změna	k1gFnSc2	změna
plánu	plán	k1gInSc2	plán
poctila	poctít	k5eAaPmAgFnS	poctít
královna	královna	k1gFnSc1	královna
Alžběta	Alžběta	k1gFnSc1	Alžběta
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2007	[number]	k4	2007
předsedu	předseda	k1gMnSc4	předseda
klubu	klub	k1gInSc2	klub
a	a	k8xC	a
první	první	k4xOgInSc4	první
tým	tým	k1gInSc4	tým
svým	svůj	k3xOyFgNnSc7	svůj
pozváním	pozvání	k1gNnSc7	pozvání
do	do	k7c2	do
Buckinghamského	buckinghamský	k2eAgInSc2d1	buckinghamský
paláce	palác	k1gInSc2	palác
na	na	k7c4	na
odpolední	odpolední	k2eAgInSc4d1	odpolední
čaj	čaj	k1gInSc4	čaj
<g/>
.	.	kIx.	.
</s>
<s>
Arsenal	Arsenat	k5eAaPmAgMnS	Arsenat
byl	být	k5eAaImAgInS	být
prvním	první	k4xOgInSc7	první
klubem	klub	k1gInSc7	klub
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
byl	být	k5eAaImAgInS	být
při	při	k7c6	při
takové	takový	k3xDgFnSc6	takový
události	událost	k1gFnSc6	událost
pozván	pozvat	k5eAaPmNgMnS	pozvat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Arsenalizace	Arsenalizace	k1gFnSc2	Arsenalizace
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2009	[number]	k4	2009
začal	začít	k5eAaPmAgMnS	začít
Arsenal	Arsenal	k1gFnSc4	Arsenal
na	na	k7c4	na
Emirates	Emirates	k1gInSc4	Emirates
stadium	stadium	k1gNnSc4	stadium
s	s	k7c7	s
plánem	plán	k1gInSc7	plán
Arsenalizace	Arsenalizace	k1gFnSc2	Arsenalizace
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
vyslechl	vyslechnout	k5eAaPmAgInS	vyslechnout
zpětnou	zpětný	k2eAgFnSc4d1	zpětná
vazbu	vazba	k1gFnSc4	vazba
od	od	k7c2	od
fanoušků	fanoušek	k1gMnPc2	fanoušek
na	na	k7c6	na
fóru	fórum	k1gNnSc6	fórum
<g/>
.	.	kIx.	.
</s>
<s>
Arsenalizaci	Arsenalizace	k1gFnSc4	Arsenalizace
vedl	vést	k5eAaImAgMnS	vést
výkonný	výkonný	k2eAgMnSc1d1	výkonný
ředitel	ředitel	k1gMnSc1	ředitel
klubu	klub	k1gInSc2	klub
Ivan	Ivan	k1gMnSc1	Ivan
Gazidis	Gazidis	k1gFnSc1	Gazidis
<g/>
.	.	kIx.	.
</s>
<s>
Záměrem	záměr	k1gInSc7	záměr
bylo	být	k5eAaImAgNnS	být
udělat	udělat	k5eAaPmF	udělat
ze	z	k7c2	z
stadionu	stadion	k1gInSc2	stadion
"	"	kIx"	"
<g/>
viditelnou	viditelný	k2eAgFnSc4d1	viditelná
pevnost	pevnost	k1gFnSc4	pevnost
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
kterých	který	k3yQgMnPc2	který
kdy	kdy	k6eAd1	kdy
Arsenal	Arsenal	k1gMnPc3	Arsenal
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
<g/>
,	,	kIx,	,
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
různých	různý	k2eAgInPc2d1	různý
tvůrčích	tvůrčí	k2eAgInPc2d1	tvůrčí
a	a	k8xC	a
uměleckých	umělecký	k2eAgInPc2d1	umělecký
prostředků	prostředek	k1gInPc2	prostředek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Arsenalizace	Arsenalizace	k1gFnSc2	Arsenalizace
provedeny	provést	k5eAaPmNgInP	provést
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
instalace	instalace	k1gFnSc1	instalace
bílých	bílý	k2eAgFnPc2d1	bílá
sedaček	sedačka	k1gFnPc2	sedačka
a	a	k8xC	a
následné	následný	k2eAgNnSc4d1	následné
vytvoření	vytvoření	k1gNnSc4	vytvoření
klubového	klubový	k2eAgInSc2d1	klubový
znaku	znak	k1gInSc2	znak
–	–	k?	–
kanónu	kanón	k1gInSc2	kanón
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
úrovni	úroveň	k1gFnSc6	úroveň
stadionu	stadion	k1gInSc2	stadion
hned	hned	k6eAd1	hned
naproti	naproti	k7c3	naproti
vstupnímu	vstupní	k2eAgInSc3d1	vstupní
tunelu	tunel	k1gInSc3	tunel
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Duch	duch	k1gMnSc1	duch
Highbury	Highbura	k1gFnSc2	Highbura
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Spirit	Spirit	k1gInSc1	Spirit
of	of	k?	of
Highbury	Highbura	k1gFnSc2	Highbura
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
–	–	k?	–
svatyně	svatyně	k1gFnSc2	svatyně
zobrazující	zobrazující	k2eAgNnSc1d1	zobrazující
jméno	jméno	k1gNnSc1	jméno
každého	každý	k3xTgMnSc2	každý
hráče	hráč	k1gMnSc2	hráč
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
za	za	k7c2	za
93	[number]	k4	93
let	léto	k1gNnPc2	léto
existence	existence	k1gFnSc2	existence
Highbury	Highbura	k1gFnSc2	Highbura
hrál	hrát	k5eAaImAgInS	hrát
za	za	k7c4	za
Arsenal	Arsenal	k1gFnSc4	Arsenal
<g/>
.	.	kIx.	.
</s>
<s>
Svatyně	svatyně	k1gFnSc1	svatyně
byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
vnější	vnější	k2eAgFnSc2d1	vnější
části	část	k1gFnSc2	část
stadionu	stadion	k1gInSc2	stadion
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
fasádu	fasáda	k1gFnSc4	fasáda
stadionu	stadion	k1gInSc2	stadion
nainstalováno	nainstalovat	k5eAaPmNgNnS	nainstalovat
osm	osm	k4xCc1	osm
velkých	velký	k2eAgFnPc2d1	velká
nástěnných	nástěnný	k2eAgFnPc2d1	nástěnná
maleb	malba	k1gFnPc2	malba
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
znázorňovala	znázorňovat	k5eAaImAgFnS	znázorňovat
legendy	legenda	k1gFnPc4	legenda
Arsenalu	Arsenal	k1gInSc2	Arsenal
držících	držící	k2eAgInPc2d1	držící
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
ramen	rameno	k1gNnPc2	rameno
<g/>
.	.	kIx.	.
</s>
<s>
Smyslem	smysl	k1gInSc7	smysl
dokončeného	dokončený	k2eAgInSc2d1	dokončený
návrhu	návrh	k1gInSc2	návrh
bylo	být	k5eAaImAgNnS	být
vyobrazit	vyobrazit	k5eAaPmF	vyobrazit
32	[number]	k4	32
legend	legenda	k1gFnPc2	legenda
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
hloučku	hlouček	k1gInSc6	hlouček
a	a	k8xC	a
táhly	táhnout	k5eAaImAgInP	táhnout
se	se	k3xPyFc4	se
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
obvodu	obvod	k1gInSc6	obvod
Emirates	Emirates	k1gInSc1	Emirates
stadium	stadium	k1gNnSc1	stadium
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Kolem	kolem	k7c2	kolem
dolního	dolní	k2eAgInSc2d1	dolní
vestibulu	vestibul	k1gInSc2	vestibul
stadionu	stadion	k1gInSc2	stadion
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
další	další	k2eAgFnSc2d1	další
nástěnné	nástěnný	k2eAgFnSc2d1	nástěnná
malby	malba	k1gFnSc2	malba
zobrazující	zobrazující	k2eAgInSc4d1	zobrazující
12	[number]	k4	12
"	"	kIx"	"
<g/>
nejlepších	dobrý	k2eAgInPc2d3	nejlepší
momentů	moment	k1gInPc2	moment
historie	historie	k1gFnSc2	historie
Arsenalu	Arsenal	k1gInSc2	Arsenal
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vybrány	vybrán	k2eAgFnPc1d1	vybrána
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
na	na	k7c6	na
webových	webový	k2eAgFnPc6d1	webová
stránkách	stránka	k1gFnPc6	stránka
klubu	klub	k1gInSc2	klub
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
sezóny	sezóna	k1gFnSc2	sezóna
2010	[number]	k4	2010
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
byly	být	k5eAaImAgInP	být
změněny	změněn	k2eAgInPc1d1	změněn
názvy	název	k1gInPc1	název
tribun	tribuna	k1gFnPc2	tribuna
na	na	k7c4	na
East	East	k1gInSc4	East
Stand	Standa	k1gFnPc2	Standa
<g/>
,	,	kIx,	,
West	Westa	k1gFnPc2	Westa
Stand	Standa	k1gFnPc2	Standa
<g/>
,	,	kIx,	,
North	Northa	k1gFnPc2	Northa
Bank	banka	k1gFnPc2	banka
<g/>
,	,	kIx,	,
a	a	k8xC	a
Clock	Clock	k1gMnSc1	Clock
End	End	k1gMnSc1	End
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
na	na	k7c4	na
Highbury	Highbura	k1gFnPc4	Highbura
<g/>
,	,	kIx,	,
i	i	k8xC	i
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgFnP	být
na	na	k7c4	na
tribunu	tribuna	k1gFnSc4	tribuna
Clock	Clock	k1gMnSc1	Clock
End	End	k1gMnSc1	End
nainstalovány	nainstalován	k2eAgFnPc4d1	nainstalována
hodiny	hodina	k1gFnPc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Odhaleny	odhalen	k2eAgFnPc1d1	odhalena
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
ligovém	ligový	k2eAgInSc6d1	ligový
zápase	zápas	k1gInSc6	zápas
proti	proti	k7c3	proti
Blackpoolu	Blackpool	k1gInSc3	Blackpool
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
2011	[number]	k4	2011
Arsenal	Arsenal	k1gMnSc1	Arsenal
přejmenoval	přejmenovat	k5eAaPmAgMnS	přejmenovat
dva	dva	k4xCgInPc4	dva
mosty	most	k1gInPc4	most
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
stadionu	stadion	k1gInSc2	stadion
po	po	k7c6	po
ředitelích	ředitel	k1gMnPc6	ředitel
klubu	klub	k1gInSc2	klub
Danny	Danna	k1gFnPc1	Danna
Fiszmanovi	Fiszman	k1gMnSc6	Fiszman
a	a	k8xC	a
Kenu	Ken	k1gMnSc6	Ken
Friarovi	Friar	k1gMnSc6	Friar
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
125	[number]	k4	125
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
založení	založení	k1gNnSc2	založení
klubu	klub	k1gInSc2	klub
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2011	[number]	k4	2011
Arsenal	Arsenal	k1gMnSc1	Arsenal
odhalil	odhalit	k5eAaPmAgMnS	odhalit
před	před	k7c7	před
stadionem	stadion	k1gInSc7	stadion
tři	tři	k4xCgFnPc4	tři
sochy	socha	k1gFnPc1	socha
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
byla	být	k5eAaImAgFnS	být
socha	socha	k1gFnSc1	socha
bývalého	bývalý	k2eAgMnSc2d1	bývalý
kapitána	kapitán	k1gMnSc2	kapitán
Tonyho	Tony	k1gMnSc2	Tony
Adamse	Adams	k1gMnSc2	Adams
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
nejlepšího	dobrý	k2eAgInSc2d3	nejlepší
střelce	střelec	k1gMnPc4	střelec
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
klubu	klub	k1gInSc2	klub
<g/>
,	,	kIx,	,
Thierryho	Thierry	k1gMnSc2	Thierry
Henryho	Henry	k1gMnSc2	Henry
<g/>
,	,	kIx,	,
a	a	k8xC	a
třetí	třetí	k4xOgMnSc1	třetí
bývalého	bývalý	k2eAgMnSc2d1	bývalý
manažera	manažer	k1gMnSc2	manažer
Herberta	Herbert	k1gMnSc2	Herbert
Chapmana	Chapman	k1gMnSc2	Chapman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Konstrukce	konstrukce	k1gFnPc4	konstrukce
a	a	k8xC	a
zázemí	zázemí	k1gNnSc4	zázemí
==	==	k?	==
</s>
</p>
<p>
<s>
Emirates	Emirates	k1gMnSc1	Emirates
stadium	stadium	k1gNnSc4	stadium
je	být	k5eAaImIp3nS	být
třístupňová	třístupňový	k2eAgFnSc1d1	třístupňová
oválná	oválný	k2eAgFnSc1d1	oválná
stavba	stavba	k1gFnSc1	stavba
s	s	k7c7	s
průhlednou	průhledný	k2eAgFnSc7d1	průhledná
polykarbonátovou	polykarbonátův	k2eAgFnSc7d1	polykarbonátův
střechou	střecha	k1gFnSc7	střecha
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zakrývá	zakrývat	k5eAaImIp3nS	zakrývat
pouze	pouze	k6eAd1	pouze
tribuny	tribuna	k1gFnPc4	tribuna
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
hrací	hrací	k2eAgFnSc4d1	hrací
plochu	plocha	k1gFnSc4	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Spodní	spodní	k2eAgFnSc1d1	spodní
strana	strana	k1gFnSc1	strana
je	být	k5eAaImIp3nS	být
pokryta	pokrýt	k5eAaPmNgFnS	pokrýt
kovovými	kovový	k2eAgInPc7d1	kovový
panely	panel	k1gInPc7	panel
<g/>
.	.	kIx.	.
</s>
<s>
Střecha	střecha	k1gFnSc1	střecha
je	být	k5eAaImIp3nS	být
podporována	podporovat	k5eAaImNgFnS	podporovat
čtyřmi	čtyři	k4xCgInPc7	čtyři
trojúhelníkovými	trojúhelníkový	k2eAgInPc7d1	trojúhelníkový
nosníky	nosník	k1gInPc7	nosník
–	–	k?	–
svařované	svařovaný	k2eAgFnPc4d1	svařovaná
ocelové	ocelový	k2eAgFnPc4d1	ocelová
trubky	trubka	k1gFnPc4	trubka
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
z	z	k7c2	z
nosníků	nosník	k1gInPc2	nosník
jsou	být	k5eAaImIp3nP	být
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
200	[number]	k4	200
m	m	kA	m
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
postaveny	postavit	k5eAaPmNgInP	postavit
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
sever	sever	k1gInSc4	sever
<g/>
–	–	k?	–
<g/>
jih	jih	k1gInSc1	jih
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
zbylé	zbylý	k2eAgInPc1d1	zbylý
dva	dva	k4xCgInPc1	dva
nosník	nosník	k1gInSc1	nosník
jsou	být	k5eAaImIp3nP	být
vystavěny	vystavět	k5eAaPmNgInP	vystavět
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
východ	východ	k1gInSc4	východ
<g/>
–	–	k?	–
<g/>
západ	západ	k1gInSc1	západ
<g/>
.	.	kIx.	.
</s>
<s>
Nosníky	nosník	k1gInPc1	nosník
jsou	být	k5eAaImIp3nP	být
usazeny	usadit	k5eAaPmNgInP	usadit
v	v	k7c6	v
betonových	betonový	k2eAgInPc6d1	betonový
základech	základ	k1gInPc6	základ
stadionu	stadion	k1gInSc2	stadion
<g/>
.	.	kIx.	.
</s>
<s>
Osm	osm	k4xCc1	osm
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jich	on	k3xPp3gMnPc2	on
je	být	k5eAaImIp3nS	být
spojených	spojený	k2eAgFnPc2d1	spojená
pomocí	pomoc	k1gFnPc2	pomoc
ocelové	ocelový	k2eAgFnSc2d1	ocelová
trojnožky	trojnožka	k1gFnSc2	trojnožka
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
části	část	k1gFnSc6	část
stadionu	stadion	k1gInSc2	stadion
nachází	nacházet	k5eAaImIp3nS	nacházet
čtyři	čtyři	k4xCgNnPc4	čtyři
schodiště	schodiště	k1gNnPc4	schodiště
<g/>
,	,	kIx,	,
výtah	výtah	k1gInSc4	výtah
a	a	k8xC	a
vstup	vstup	k1gInSc4	vstup
pro	pro	k7c4	pro
personál	personál	k1gInSc4	personál
<g/>
.	.	kIx.	.
</s>
<s>
Fasáda	fasáda	k1gFnSc1	fasáda
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
prosklená	prosklený	k2eAgFnSc1d1	prosklená
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
vyčnívá	vyčnívat	k5eAaImIp3nS	vyčnívat
betonová	betonový	k2eAgFnSc1d1	betonová
konstrukce	konstrukce	k1gFnSc1	konstrukce
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
návštěvníkům	návštěvník	k1gMnPc3	návštěvník
nahlédnout	nahlédnout	k5eAaPmF	nahlédnout
z	z	k7c2	z
vyvýšeného	vyvýšený	k2eAgNnSc2d1	vyvýšené
místa	místo	k1gNnSc2	místo
do	do	k7c2	do
vnitřku	vnitřek	k1gInSc2	vnitřek
stadionu	stadion	k1gInSc2	stadion
<g/>
.	.	kIx.	.
</s>
<s>
Sklo	sklo	k1gNnSc1	sklo
a	a	k8xC	a
ocelové	ocelový	k2eAgFnPc1d1	ocelová
konstrukce	konstrukce	k1gFnPc1	konstrukce
byly	být	k5eAaImAgFnP	být
navrženy	navrhnout	k5eAaPmNgFnP	navrhnout
společností	společnost	k1gFnSc7	společnost
Populous	Populous	k1gInSc1	Populous
<g/>
.	.	kIx.	.
</s>
<s>
Design	design	k1gInSc1	design
těchto	tento	k3xDgInPc2	tento
prvků	prvek	k1gInPc2	prvek
měl	mít	k5eAaImAgMnS	mít
dávat	dávat	k5eAaImF	dávat
dojem	dojem	k1gInSc4	dojem
třpytícího	třpytící	k2eAgInSc2d1	třpytící
se	se	k3xPyFc4	se
stadionu	stadion	k1gInSc2	stadion
za	za	k7c2	za
slunečného	slunečný	k2eAgInSc2d1	slunečný
dne	den	k1gInSc2	den
a	a	k8xC	a
zářícího	zářící	k2eAgInSc2d1	zářící
stadionu	stadion	k1gInSc2	stadion
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
.	.	kIx.	.
<g/>
Horní	horní	k2eAgFnSc1d1	horní
část	část	k1gFnSc1	část
stadionu	stadion	k1gInSc2	stadion
s	s	k7c7	s
kapacitou	kapacita	k1gFnSc7	kapacita
pro	pro	k7c4	pro
26	[number]	k4	26
646	[number]	k4	646
diváků	divák	k1gMnPc2	divák
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
standardní	standardní	k2eAgFnPc4d1	standardní
sedadla	sedadlo	k1gNnPc4	sedadlo
<g/>
.	.	kIx.	.
</s>
<s>
Tytéž	týž	k3xTgInPc1	týž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
i	i	k9	i
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
části	část	k1gFnSc6	část
stadionu	stadion	k1gInSc2	stadion
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
kapacitu	kapacita	k1gFnSc4	kapacita
24	[number]	k4	24
425	[number]	k4	425
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
stadionem	stadion	k1gInSc7	stadion
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
dvě	dva	k4xCgNnPc4	dva
podzemní	podzemní	k2eAgNnPc4d1	podzemní
patra	patro	k1gNnPc4	patro
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
další	další	k2eAgFnSc2d1	další
prostory	prostora	k1gFnSc2	prostora
jako	jako	k8xC	jako
kuchyně	kuchyně	k1gFnSc2	kuchyně
<g/>
,	,	kIx,	,
šatny	šatna	k1gFnSc2	šatna
<g/>
,	,	kIx,	,
tisková	tiskový	k2eAgFnSc1d1	tisková
a	a	k8xC	a
vzdělávací	vzdělávací	k2eAgNnPc1d1	vzdělávací
centra	centrum	k1gNnPc1	centrum
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prostřední	prostřední	k2eAgFnSc6d1	prostřední
části	část	k1gFnSc6	část
tribun	tribuna	k1gFnPc2	tribuna
<g/>
,	,	kIx,	,
známé	známý	k2eAgFnPc4d1	známá
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Club	club	k1gInSc1	club
Level	level	k1gInSc1	level
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
finančně	finančně	k6eAd1	finančně
náročná	náročný	k2eAgFnSc1d1	náročná
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
director	director	k1gInSc1	director
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
box	box	k1gInSc1	box
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
7139	[number]	k4	7139
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
prodávány	prodávat	k5eAaImNgInP	prodávat
v	v	k7c6	v
licencích	licence	k1gFnPc6	licence
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
trvající	trvající	k2eAgMnSc1d1	trvající
jeden	jeden	k4xCgInSc4	jeden
až	až	k8xS	až
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Bezprostředně	bezprostředně	k6eAd1	bezprostředně
nad	nad	k7c4	nad
director	director	k1gInSc4	director
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
box	box	k1gInSc1	box
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgInSc1d1	malý
okruh	okruh	k1gInSc1	okruh
skládající	skládající	k2eAgFnSc2d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
150	[number]	k4	150
sky	sky	k?	sky
boxů	box	k1gInPc2	box
po	po	k7c4	po
10	[number]	k4	10
<g/>
,	,	kIx,	,
12	[number]	k4	12
a	a	k8xC	a
15	[number]	k4	15
místech	místo	k1gNnPc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
počet	počet	k1gInSc1	počet
míst	místo	k1gNnPc2	místo
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
sekci	sekce	k1gFnSc6	sekce
stadionu	stadion	k1gInSc2	stadion
činí	činit	k5eAaImIp3nS	činit
2222	[number]	k4	2222
<g/>
.	.	kIx.	.
</s>
<s>
Vysoká	vysoký	k2eAgFnSc1d1	vysoká
poptávka	poptávka	k1gFnSc1	poptávka
po	po	k7c6	po
vstupenkách	vstupenka	k1gFnPc6	vstupenka
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
relativní	relativní	k2eAgNnSc4d1	relativní
bohatství	bohatství	k1gNnSc4	bohatství
londýnských	londýnský	k2eAgMnPc2d1	londýnský
příznivců	příznivec	k1gMnPc2	příznivec
<g/>
,	,	kIx,	,
zapříčinily	zapříčinit	k5eAaPmAgFnP	zapříčinit
zisk	zisk	k1gInSc4	zisk
z	z	k7c2	z
premiových	premiový	k2eAgNnPc2d1	premiové
míst	místo	k1gNnPc2	místo
a	a	k8xC	a
firemních	firemní	k2eAgInPc2d1	firemní
sky	sky	k?	sky
boxů	box	k1gInPc2	box
zhruba	zhruba	k6eAd1	zhruba
na	na	k7c6	na
stejné	stejný	k2eAgFnSc6d1	stejná
výši	výše	k1gFnSc6	výše
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
příjmy	příjem	k1gInPc1	příjem
ze	z	k7c2	z
vstupného	vstupné	k1gNnSc2	vstupné
na	na	k7c6	na
starém	staré	k1gNnSc6	staré
Highbury	Highbura	k1gFnSc2	Highbura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vrchní	vrchní	k2eAgFnSc1d1	vrchní
část	část	k1gFnSc1	část
tribun	tribuna	k1gFnPc2	tribuna
je	být	k5eAaImIp3nS	být
tvarována	tvarovat	k5eAaImNgFnS	tvarovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
v	v	k7c6	v
rozích	roh	k1gInPc6	roh
stadionu	stadion	k1gInSc2	stadion
byl	být	k5eAaImAgInS	být
ponechán	ponechat	k5eAaPmNgInS	ponechat
otevřený	otevřený	k2eAgInSc1d1	otevřený
prostor	prostor	k1gInSc1	prostor
<g/>
,	,	kIx,	,
a	a	k8xC	a
střecha	střecha	k1gFnSc1	střecha
byla	být	k5eAaImAgFnS	být
výrazně	výrazně	k6eAd1	výrazně
nakloněna	naklonit	k5eAaPmNgFnS	naklonit
směrem	směr	k1gInSc7	směr
dovnitř	dovnitř	k6eAd1	dovnitř
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
tyto	tento	k3xDgInPc1	tento
prvky	prvek	k1gInPc1	prvek
mají	mít	k5eAaImIp3nP	mít
poskytnout	poskytnout	k5eAaPmF	poskytnout
dostatek	dostatek	k1gInSc4	dostatek
slunečního	sluneční	k2eAgNnSc2d1	sluneční
světla	světlo	k1gNnSc2	světlo
a	a	k8xC	a
proudění	proudění	k1gNnSc2	proudění
vzduchu	vzduch	k1gInSc2	vzduch
na	na	k7c4	na
hrací	hrací	k2eAgFnSc4d1	hrací
plochu	plocha	k1gFnSc4	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Stadion	stadion	k1gInSc1	stadion
rovněž	rovněž	k9	rovněž
dává	dávat	k5eAaImIp3nS	dávat
iluzi	iluze	k1gFnSc4	iluze
<g/>
,	,	kIx,	,
že	že	k8xS	že
fanoušci	fanoušek	k1gMnPc1	fanoušek
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
tribuny	tribuna	k1gFnSc2	tribuna
nejsou	být	k5eNaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
vidět	vidět	k5eAaImF	vidět
příznivce	příznivec	k1gMnPc4	příznivec
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
výšce	výška	k1gFnSc6	výška
<g/>
.	.	kIx.	.
</s>
<s>
Stadion	stadion	k1gInSc1	stadion
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
díky	díky	k7c3	díky
smlouvě	smlouva	k1gFnSc3	smlouva
se	se	k3xPyFc4	se
Sony	Sony	kA	Sony
prvním	první	k4xOgNnSc7	první
místem	místo	k1gNnSc7	místo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
zavedeno	zavést	k5eAaPmNgNnS	zavést
HDTV	HDTV	kA	HDTV
vysílání	vysílání	k1gNnSc2	vysílání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severozápadním	severozápadní	k2eAgInSc6d1	severozápadní
a	a	k8xC	a
jihovýchodním	jihovýchodní	k2eAgInSc6d1	jihovýchodní
rohu	roh	k1gInSc6	roh
stadionu	stadion	k1gInSc2	stadion
jsou	být	k5eAaImIp3nP	být
ze	z	k7c2	z
stropu	strop	k1gInSc2	strop
zavěšeny	zavěšen	k2eAgFnPc4d1	zavěšena
obří	obří	k2eAgFnPc4d1	obří
obrazovky	obrazovka	k1gFnPc4	obrazovka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fotbalové	fotbalový	k2eAgNnSc1d1	fotbalové
hřiště	hřiště	k1gNnSc1	hřiště
má	mít	k5eAaImIp3nS	mít
rozměry	rozměr	k1gInPc4	rozměr
105	[number]	k4	105
<g/>
×	×	k?	×
<g/>
68	[number]	k4	68
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
travnatá	travnatý	k2eAgFnSc1d1	travnatá
plocha	plocha	k1gFnSc1	plocha
na	na	k7c4	na
Emirates	Emirates	k1gInSc4	Emirates
má	mít	k5eAaImIp3nS	mít
rozměry	rozměr	k1gInPc4	rozměr
113	[number]	k4	113
<g/>
×	×	k?	×
<g/>
76	[number]	k4	76
m.	m.	k?	m.
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
na	na	k7c4	na
Highbury	Highbura	k1gFnPc4	Highbura
je	být	k5eAaImIp3nS	být
i	i	k9	i
zde	zde	k6eAd1	zde
hřiště	hřiště	k1gNnSc1	hřiště
vedeno	vést	k5eAaImNgNnS	vést
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
<s>
Hráčský	hráčský	k2eAgInSc1d1	hráčský
tunel	tunel	k1gInSc1	tunel
a	a	k8xC	a
podzemní	podzemní	k2eAgFnPc1d1	podzemní
prostory	prostora	k1gFnPc1	prostora
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
straně	strana	k1gFnSc6	strana
hned	hned	k6eAd1	hned
pod	pod	k7c7	pod
hlavní	hlavní	k2eAgFnSc7d1	hlavní
televizní	televizní	k2eAgFnSc7d1	televizní
kamerou	kamera	k1gFnSc7	kamera
<g/>
.	.	kIx.	.
</s>
<s>
Fanoušci	Fanoušek	k1gMnPc1	Fanoušek
hostů	host	k1gMnPc2	host
mají	mít	k5eAaImIp3nP	mít
svůj	svůj	k3xOyFgInSc4	svůj
sektor	sektor	k1gInSc4	sektor
v	v	k7c6	v
dolní	dolní	k2eAgFnSc6d1	dolní
části	část	k1gFnSc6	část
stadionu	stadion	k1gInSc2	stadion
<g/>
,	,	kIx,	,
v	v	k7c6	v
jihovýchodním	jihovýchodní	k2eAgInSc6d1	jihovýchodní
rohu	roh	k1gInSc6	roh
<g/>
.	.	kIx.	.
</s>
<s>
Sektor	sektor	k1gInSc1	sektor
hostů	host	k1gMnPc2	host
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
rozšířen	rozšířit	k5eAaPmNgInS	rozšířit
z	z	k7c2	z
1500	[number]	k4	1500
na	na	k7c4	na
4500	[number]	k4	4500
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
když	když	k8xS	když
by	by	kYmCp3nS	by
jim	on	k3xPp3gMnPc3	on
připadl	připadnout	k5eAaPmAgInS	připadnout
ještě	ještě	k6eAd1	ještě
sektor	sektor	k1gInSc1	sektor
za	za	k7c7	za
jižní	jižní	k2eAgFnSc7d1	jižní
brankou	branka	k1gFnSc7	branka
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
dalších	další	k2eAgNnPc2d1	další
4500	[number]	k4	4500
míst	místo	k1gNnPc2	místo
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
tribuny	tribuna	k1gFnSc2	tribuna
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
možné	možný	k2eAgNnSc1d1	možné
pro	pro	k7c4	pro
hosty	host	k1gMnPc4	host
vyhradit	vyhradit	k5eAaPmF	vyhradit
9000	[number]	k4	9000
míst	místo	k1gNnPc2	místo
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
splňuje	splňovat	k5eAaImIp3nS	splňovat
nařízení	nařízení	k1gNnSc1	nařízení
15	[number]	k4	15
%	%	kIx~	%
stadionu	stadion	k1gInSc2	stadion
pro	pro	k7c4	pro
domácí	domácí	k2eAgInSc4d1	domácí
pohár	pohár	k1gInSc4	pohár
a	a	k8xC	a
soutěže	soutěž	k1gFnPc4	soutěž
jako	jako	k8xS	jako
FA	fa	kA	fa
Cup	cup	k1gInSc1	cup
<g/>
,	,	kIx,	,
či	či	k8xC	či
ligový	ligový	k2eAgInSc4d1	ligový
pohár	pohár	k1gInSc4	pohár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Emirates	Emirates	k1gInSc1	Emirates
stadium	stadium	k1gNnSc1	stadium
vzdal	vzdát	k5eAaPmAgInS	vzdát
hold	hold	k1gInSc4	hold
bývalému	bývalý	k2eAgMnSc3d1	bývalý
domácímu	domácí	k1gMnSc3	domácí
stadionu	stadion	k1gInSc2	stadion
Arsenalu	Arsenal	k1gMnSc3	Arsenal
<g/>
,	,	kIx,	,
Highbury	Highbur	k1gInPc7	Highbur
<g/>
.	.	kIx.	.
</s>
<s>
Klubové	klubový	k2eAgFnPc1d1	klubová
kanceláře	kancelář	k1gFnPc1	kancelář
jsou	být	k5eAaImIp3nP	být
oficiálně	oficiálně	k6eAd1	oficiálně
nazývány	nazývat	k5eAaImNgFnP	nazývat
jako	jako	k8xC	jako
dům	dům	k1gInSc1	dům
Highbury	Highbura	k1gFnSc2	Highbura
(	(	kIx(	(
<g/>
Highbury	Highbur	k1gInPc1	Highbur
house	house	k1gNnSc1	house
<g/>
)	)	kIx)	)
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
busta	busta	k1gFnSc1	busta
Herberta	Herbert	k1gMnSc4	Herbert
Chapmana	Chapman	k1gMnSc4	Chapman
<g/>
,	,	kIx,	,
která	který	k3yRgNnPc1	který
dříve	dříve	k6eAd2	dříve
sídlila	sídlit	k5eAaImAgNnP	sídlit
na	na	k7c4	na
Highbury	Highbura	k1gFnPc4	Highbura
<g/>
,	,	kIx,	,
severovýchodně	severovýchodně	k6eAd1	severovýchodně
od	od	k7c2	od
Emirates	Emiratesa	k1gFnPc2	Emiratesa
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgFnPc1	tři
zbývající	zbývající	k2eAgFnPc1d1	zbývající
busty	busta	k1gFnPc1	busta
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
se	se	k3xPyFc4	se
také	také	k9	také
dříve	dříve	k6eAd2	dříve
nacházely	nacházet	k5eAaImAgFnP	nacházet
na	na	k7c4	na
Highbury	Highbura	k1gFnPc4	Highbura
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
přesuny	přesun	k1gInPc1	přesun
na	na	k7c4	na
Emirates	Emirates	k1gInSc4	Emirates
stadium	stadium	k1gNnSc4	stadium
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
před	před	k7c7	před
vstupem	vstup	k1gInSc7	vstup
do	do	k7c2	do
Diamond	Diamonda	k1gFnPc2	Diamonda
Clubu	club	k1gInSc2	club
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
busty	busta	k1gFnPc4	busta
Clauda	Claud	k1gMnSc2	Claud
Ferriera	Ferrier	k1gMnSc2	Ferrier
(	(	kIx(	(
<g/>
architekt	architekt	k1gMnSc1	architekt
East	East	k1gMnSc1	East
stand	stand	k?	stand
–	–	k?	–
východní	východní	k2eAgFnSc2d1	východní
tribuny	tribuna	k1gFnSc2	tribuna
na	na	k7c4	na
Highbury	Highbur	k1gInPc4	Highbur
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Denise	Denisa	k1gFnSc3	Denisa
Hill	Hill	k1gMnSc1	Hill
<g/>
-	-	kIx~	-
<g/>
Wooda	Wooda	k1gMnSc1	Wooda
(	(	kIx(	(
<g/>
bývalý	bývalý	k2eAgMnSc1d1	bývalý
předseda	předseda	k1gMnSc1	předseda
Arsenalu	Arsenal	k1gInSc2	Arsenal
a	a	k8xC	a
otec	otec	k1gMnSc1	otec
současného	současný	k2eAgMnSc2d1	současný
předsedy	předseda	k1gMnSc2	předseda
Petera	Peter	k1gMnSc2	Peter
Hill	Hill	k1gMnSc1	Hill
<g/>
-	-	kIx~	-
<g/>
Wooda	Wooda	k1gMnSc1	Wooda
<g/>
)	)	kIx)	)
a	a	k8xC	a
současného	současný	k2eAgMnSc2d1	současný
manažera	manažer	k1gMnSc2	manažer
<g/>
,	,	kIx,	,
Arsè	Arsè	k1gMnSc2	Arsè
Wengera	Wenger	k1gMnSc2	Wenger
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc1	dva
mosty	most	k1gInPc1	most
vedoucí	vedoucí	k2eAgInPc1d1	vedoucí
přes	přes	k7c4	přes
železniční	železniční	k2eAgFnSc4d1	železniční
trať	trať	k1gFnSc4	trať
a	a	k8xC	a
spojující	spojující	k2eAgInSc1d1	spojující
stadion	stadion	k1gInSc1	stadion
s	s	k7c7	s
Drayton	Drayton	k1gInSc1	Drayton
Parkem	park	k1gInSc7	park
na	na	k7c4	na
východ	východ	k1gInSc4	východ
od	od	k7c2	od
Emirates	Emiratesa	k1gFnPc2	Emiratesa
nazývány	nazývat	k5eAaImNgFnP	nazývat
podle	podle	k7c2	podle
bývalých	bývalý	k2eAgFnPc2d1	bývalá
tribun	tribuna	k1gFnPc2	tribuna
na	na	k7c4	na
Highbury	Highbura	k1gFnPc4	Highbura
Clock	Clocko	k1gNnPc2	Clocko
End	End	k1gFnPc2	End
bridge	bridge	k1gFnPc2	bridge
a	a	k8xC	a
North	Northa	k1gFnPc2	Northa
Bank	banka	k1gFnPc2	banka
bridge	bridge	k6eAd1	bridge
<g/>
.	.	kIx.	.
</s>
<s>
Hodiny	hodina	k1gFnPc1	hodina
<g/>
,	,	kIx,	,
pojmenované	pojmenovaný	k2eAgInPc1d1	pojmenovaný
podle	podle	k7c2	podle
starého	staré	k1gNnSc2	staré
Clock	Clocka	k1gFnPc2	Clocka
Endu	Endus	k1gInSc2	Endus
byly	být	k5eAaImAgInP	být
opraveny	opravit	k5eAaPmNgInP	opravit
a	a	k8xC	a
zmodernizovány	zmodernizovat	k5eAaPmNgInP	zmodernizovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
stala	stát	k5eAaPmAgFnS	stát
nová	nový	k2eAgFnSc1d1	nová
větší	veliký	k2eAgFnSc1d2	veliký
replika	replika	k1gFnSc1	replika
původních	původní	k2eAgFnPc2d1	původní
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2006	[number]	k4	2006
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
stadionu	stadion	k1gInSc2	stadion
klubové	klubový	k2eAgNnSc1d1	klubové
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
nalézalo	nalézat	k5eAaImAgNnS	nalézat
pod	pod	k7c7	pod
tribunou	tribuna	k1gFnSc7	tribuna
North	Northa	k1gFnPc2	Northa
Bank	banka	k1gFnPc2	banka
stand	stand	k?	stand
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k6eAd1	uvnitř
jsou	být	k5eAaImIp3nP	být
mramorové	mramorový	k2eAgFnPc1d1	mramorová
sochy	socha	k1gFnPc1	socha
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
byly	být	k5eAaImAgFnP	být
kdysi	kdysi	k6eAd1	kdysi
v	v	k7c6	v
sálech	sál	k1gInPc6	sál
Highbury	Highbura	k1gFnSc2	Highbura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgNnPc1d1	další
využití	využití	k1gNnPc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Emirates	Emirates	k1gInSc1	Emirates
stadium	stadium	k1gNnSc1	stadium
funguje	fungovat	k5eAaImIp3nS	fungovat
také	také	k9	také
jako	jako	k9	jako
konferenční	konferenční	k2eAgNnSc1d1	konferenční
centrum	centrum	k1gNnSc1	centrum
a	a	k8xC	a
dějiště	dějiště	k1gNnSc1	dějiště
hudebních	hudební	k2eAgInPc2d1	hudební
koncertů	koncert	k1gInPc2	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2008	[number]	k4	2008
stadion	stadion	k1gInSc1	stadion
hostil	hostit	k5eAaImAgInS	hostit
summit	summit	k1gInSc4	summit
mezi	mezi	k7c7	mezi
britským	britský	k2eAgMnSc7d1	britský
ministerským	ministerský	k2eAgMnSc7d1	ministerský
předsedou	předseda	k1gMnSc7	předseda
<g/>
,	,	kIx,	,
Gordonem	Gordon	k1gMnSc7	Gordon
Brownem	Brown	k1gMnSc7	Brown
<g/>
,	,	kIx,	,
a	a	k8xC	a
francouzským	francouzský	k2eAgMnSc7d1	francouzský
prezidentem	prezident	k1gMnSc7	prezident
<g/>
,	,	kIx,	,
Nicolasem	Nicolas	k1gInSc7	Nicolas
Sarkozym	Sarkozym	k1gInSc1	Sarkozym
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
místa	místo	k1gNnSc2	místo
konání	konání	k1gNnSc2	konání
byl	být	k5eAaImAgMnS	být
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
Emirates	Emirates	k1gInSc1	Emirates
byl	být	k5eAaImAgInS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
"	"	kIx"	"
<g/>
zářný	zářný	k2eAgInSc1d1	zářný
příklad	příklad	k1gInSc1	příklad
anglo	anglo	k1gNnSc1	anglo
<g/>
-	-	kIx~	-
<g/>
francouzské	francouzský	k2eAgFnSc2d1	francouzská
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
hudební	hudební	k2eAgInSc1d1	hudební
koncert	koncert	k1gInSc1	koncert
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgNnSc6	který
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
Bruce	Bruec	k1gInPc4	Bruec
Springsteen	Springsteno	k1gNnPc2	Springsteno
a	a	k8xC	a
E	E	kA	E
Street	Street	k1gInSc1	Street
Band	band	k1gInSc1	band
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
odehrál	odehrát	k5eAaPmAgInS	odehrát
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
jejich	jejich	k3xOp3gNnPc2	jejich
vystoupení	vystoupení	k1gNnPc2	vystoupení
se	se	k3xPyFc4	se
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
následnující	následnující	k2eAgFnSc4d1	následnující
noc	noc	k1gFnSc4	noc
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
7	[number]	k4	7
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2009	[number]	k4	2009
se	se	k3xPyFc4	se
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
konal	konat	k5eAaImAgInS	konat
inaugurační	inaugurační	k2eAgInSc1d1	inaugurační
ples	ples	k1gInSc1	ples
Capital	Capital	k1gMnSc1	Capital
FM	FM	kA	FM
Summertime	Summertim	k1gMnSc5	Summertim
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
účinkovali	účinkovat	k5eAaImAgMnP	účinkovat
umělci	umělec	k1gMnPc1	umělec
jako	jako	k8xC	jako
Lionel	Lionel	k1gInSc1	Lionel
Richie	Richie	k1gFnSc2	Richie
<g/>
,	,	kIx,	,
Leona	Leona	k1gFnSc1	Leona
Lewis	Lewis	k1gFnSc1	Lewis
a	a	k8xC	a
skupina	skupina	k1gFnSc1	skupina
Blue	Blue	k1gFnSc1	Blue
<g/>
.	.	kIx.	.
</s>
<s>
Britská	britský	k2eAgFnSc1d1	britská
skupina	skupina	k1gFnSc1	skupina
Coldplay	Coldplaa	k1gFnSc2	Coldplaa
tu	tu	k6eAd1	tu
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2012	[number]	k4	2012
odehrála	odehrát	k5eAaPmAgFnS	odehrát
tři	tři	k4xCgInPc4	tři
koncerty	koncert	k1gInPc4	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Vstupenky	vstupenka	k1gFnPc4	vstupenka
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
událost	událost	k1gFnSc4	událost
byly	být	k5eAaImAgFnP	být
vyprodané	vyprodaný	k2eAgFnPc1d1	vyprodaná
po	po	k7c4	po
30	[number]	k4	30
minutách	minuta	k1gFnPc6	minuta
prodeje	prodej	k1gInSc2	prodej
<g/>
.	.	kIx.	.
<g/>
Emirates	Emirates	k1gInSc1	Emirates
stadium	stadium	k1gNnSc1	stadium
byl	být	k5eAaImAgInS	být
potvrzen	potvrzen	k2eAgInSc1d1	potvrzen
jako	jako	k8xS	jako
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
12	[number]	k4	12
stadionů	stadion	k1gInPc2	stadion
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
konat	konat	k5eAaImF	konat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
ragby	ragby	k1gNnSc6	ragby
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
ještě	ještě	k6eAd1	ještě
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
kolik	kolik	k4yQc4	kolik
zápasů	zápas	k1gInPc2	zápas
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
odehraje	odehrát	k5eAaPmIp3nS	odehrát
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
potvrzeno	potvrzen	k2eAgNnSc1d1	potvrzeno
<g/>
,	,	kIx,	,
že	že	k8xS	že
stadion	stadion	k1gInSc1	stadion
bude	být	k5eAaImBp3nS	být
hostit	hostit	k5eAaImF	hostit
zápas	zápas	k1gInSc4	zápas
o	o	k7c4	o
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Emirates	Emirates	k1gInSc1	Emirates
stadium	stadium	k1gNnSc1	stadium
figuroval	figurovat	k5eAaImAgInS	figurovat
v	v	k7c6	v
neúspěšné	úspěšný	k2eNgFnSc6d1	neúspěšná
kandidatuře	kandidatura	k1gFnSc6	kandidatura
anglické	anglický	k2eAgFnSc2d1	anglická
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
asociace	asociace	k1gFnSc2	asociace
na	na	k7c6	na
pořádání	pořádání	k1gNnSc6	pořádání
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
fotbale	fotbal	k1gInSc6	fotbal
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2012	[number]	k4	2012
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
vystoupí	vystoupit	k5eAaPmIp3nS	vystoupit
1	[number]	k4	1
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2013	[number]	k4	2013
americká	americký	k2eAgFnSc1d1	americká
punk-rocková	punkockový	k2eAgFnSc1d1	punk-rocková
kapela	kapela	k1gFnSc1	kapela
Green	Grena	k1gFnPc2	Grena
Day	Day	k1gFnPc2	Day
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
fotbalová	fotbalový	k2eAgFnSc1d1	fotbalová
utkání	utkání	k1gNnSc1	utkání
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
stadionu	stadion	k1gInSc6	stadion
se	se	k3xPyFc4	se
odehrálo	odehrát	k5eAaPmAgNnS	odehrát
množství	množství	k1gNnSc4	množství
mezinárodních	mezinárodní	k2eAgNnPc2d1	mezinárodní
přátelských	přátelský	k2eAgNnPc2d1	přátelské
utkání	utkání	k1gNnPc2	utkání
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgFnPc6	jenž
vždy	vždy	k6eAd1	vždy
figuroval	figurovat	k5eAaImAgInS	figurovat
brazilský	brazilský	k2eAgInSc1d1	brazilský
národní	národní	k2eAgInSc1d1	národní
tým	tým	k1gInSc1	tým
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
zápas	zápas	k1gInSc4	zápas
sehrála	sehrát	k5eAaPmAgFnS	sehrát
Brazílie	Brazílie	k1gFnSc1	Brazílie
proti	proti	k7c3	proti
Argentině	Argentina	k1gFnSc3	Argentina
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Zápas	zápas	k1gInSc1	zápas
skončil	skončit	k5eAaPmAgInS	skončit
vítězstvím	vítězství	k1gNnSc7	vítězství
Brazílie	Brazílie	k1gFnSc2	Brazílie
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rekordy	rekord	k1gInPc4	rekord
==	==	k?	==
</s>
</p>
<p>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
návštěva	návštěva	k1gFnSc1	návštěva
na	na	k7c4	na
Emirates	Emirates	k1gInSc4	Emirates
stadium	stadium	k1gNnSc4	stadium
<g/>
,	,	kIx,	,
60	[number]	k4	60
161	[number]	k4	161
diváků	divák	k1gMnPc2	divák
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
zaznamenána	zaznamenat	k5eAaPmNgFnS	zaznamenat
na	na	k7c4	na
utkání	utkání	k1gNnSc4	utkání
proti	proti	k7c3	proti
Manchesteru	Manchester	k1gInSc3	Manchester
United	United	k1gInSc1	United
3	[number]	k4	3
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
návštěvnost	návštěvnost	k1gFnSc1	návštěvnost
při	při	k7c6	při
zápasech	zápas	k1gInPc6	zápas
prvního	první	k4xOgInSc2	první
týmu	tým	k1gInSc2	tým
Arsenalu	Arsenal	k1gInSc2	Arsenal
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
2006	[number]	k4	2006
<g/>
/	/	kIx~	/
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
byla	být	k5eAaImAgFnS	být
59	[number]	k4	59
837	[number]	k4	837
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
utkání	utkání	k1gNnSc1	utkání
Premier	Premira	k1gFnPc2	Premira
league	league	k1gNnSc1	league
přihlíželo	přihlížet	k5eAaImAgNnS	přihlížet
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
60	[number]	k4	60
045	[number]	k4	045
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2011	[number]	k4	2011
zde	zde	k6eAd1	zde
pořadatelé	pořadatel	k1gMnPc1	pořadatel
naměřili	naměřit	k5eAaBmAgMnP	naměřit
při	při	k7c6	při
zápasu	zápas	k1gInSc6	zápas
ligového	ligový	k2eAgInSc2d1	ligový
poháru	pohár	k1gInSc2	pohár
mezi	mezi	k7c7	mezi
Arsenalem	Arsenal	k1gInSc7	Arsenal
a	a	k8xC	a
Shrewsbury	Shrewsbur	k1gInPc1	Shrewsbur
Town	Towno	k1gNnPc2	Towno
vůbec	vůbec	k9	vůbec
nejnižší	nízký	k2eAgFnSc4d3	nejnižší
návštěvu	návštěva	k1gFnSc4	návštěva
při	při	k7c6	při
utkání	utkání	k1gNnSc6	utkání
Arsenalu	Arsenal	k1gInSc2	Arsenal
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
se	se	k3xPyFc4	se
přišlo	přijít	k5eAaPmAgNnS	přijít
na	na	k7c6	na
utkání	utkání	k1gNnSc6	utkání
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
Arsenal	Arsenal	k1gMnSc1	Arsenal
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
podívat	podívat	k5eAaImF	podívat
46	[number]	k4	46
539	[number]	k4	539
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
==	==	k?	==
</s>
</p>
<p>
<s>
Emirates	Emirates	k1gMnSc1	Emirates
stadium	stadium	k1gNnSc4	stadium
je	být	k5eAaImIp3nS	být
obsluhován	obsluhován	k2eAgInSc1d1	obsluhován
několika	několik	k4yIc7	několik
stanicemi	stanice	k1gFnPc7	stanice
londýnského	londýnský	k2eAgMnSc2d1	londýnský
metra	metr	k1gMnSc2	metr
a	a	k8xC	a
autobusovými	autobusový	k2eAgFnPc7d1	autobusová
linkami	linka	k1gFnPc7	linka
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
Arsenal	Arsenal	k1gFnSc2	Arsenal
je	být	k5eAaImIp3nS	být
nejblíže	blízce	k6eAd3	blízce
k	k	k7c3	k
severní	severní	k2eAgFnSc3d1	severní
části	část	k1gFnSc3	část
stadionu	stadion	k1gInSc2	stadion
<g/>
.	.	kIx.	.
</s>
<s>
Jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
Emirates	Emiratesa	k1gFnPc2	Emiratesa
zase	zase	k9	zase
obsluhuje	obsluhovat	k5eAaImIp3nS	obsluhovat
stanice	stanice	k1gFnSc1	stanice
Highbury	Highbura	k1gFnSc2	Highbura
&	&	k?	&
Islington	Islington	k1gInSc1	Islington
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
nejblíže	blízce	k6eAd3	blízce
je	být	k5eAaImIp3nS	být
stanice	stanice	k1gFnSc1	stanice
Holloway	Hollowaa	k1gFnSc2	Hollowaa
Road	Roada	k1gFnPc2	Roada
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
pouze	pouze	k6eAd1	pouze
před	před	k7c7	před
a	a	k8xC	a
po	po	k7c6	po
zápase	zápas	k1gInSc6	zápas
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
předešlo	předejít	k5eAaPmAgNnS	předejít
přecpanosti	přecpanost	k1gFnSc2	přecpanost
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgFnPc6d1	jednotlivá
stanicích	stanice	k1gFnPc6	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
stanice	stanice	k1gFnSc1	stanice
Drayton	Drayton	k1gInSc4	Drayton
Park	park	k1gInSc1	park
nacházející	nacházející	k2eAgInSc1d1	nacházející
se	se	k3xPyFc4	se
hned	hned	k6eAd1	hned
vedle	vedle	k7c2	vedle
mostu	most	k1gInSc2	most
Clock	Clock	k1gMnSc1	Clock
End	End	k1gMnSc1	End
je	být	k5eAaImIp3nS	být
během	během	k7c2	během
hracího	hrací	k2eAgInSc2d1	hrací
dne	den	k1gInSc2	den
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
o	o	k7c6	o
víkendech	víkend	k1gInPc6	víkend
do	do	k7c2	do
desáté	desátá	k1gFnSc2	desátá
hodiny	hodina	k1gFnPc4	hodina
večerní	večerní	k2eAgFnPc4d1	večerní
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
modernizaci	modernizace	k1gFnSc6	modernizace
ulic	ulice	k1gFnPc2	ulice
Drayton	Drayton	k1gInSc1	Drayton
Park	park	k1gInSc1	park
a	a	k8xC	a
Holloway	Hollowaa	k1gFnSc2	Hollowaa
Road	Roada	k1gFnPc2	Roada
bylo	být	k5eAaImAgNnS	být
vyčleněno	vyčlenit	k5eAaPmNgNnS	vyčlenit
7,6	[number]	k4	7,6
milionů	milion	k4xCgInPc2	milion
liber	libra	k1gFnPc2	libra
<g/>
.	.	kIx.	.
</s>
<s>
Londýnský	londýnský	k2eAgInSc1d1	londýnský
dopravní	dopravní	k2eAgInSc1d1	dopravní
podnik	podnik	k1gInSc1	podnik
se	se	k3xPyFc4	se
nicméně	nicméně	k8xC	nicméně
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
tyto	tento	k3xDgFnPc4	tento
ulice	ulice	k1gFnPc4	ulice
nemodernizovat	modernizovat	k5eNaBmF	modernizovat
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
toho	ten	k3xDgInSc2	ten
byly	být	k5eAaImAgFnP	být
zmodernizovány	zmodernizován	k2eAgFnPc1d1	zmodernizována
křižovatky	křižovatka	k1gFnPc1	křižovatka
u	u	k7c2	u
stanic	stanice	k1gFnPc2	stanice
metra	metro	k1gNnSc2	metro
Highbury	Highbura	k1gFnSc2	Highbura
&	&	k?	&
Islington	Islington	k1gInSc1	Islington
a	a	k8xC	a
Finsbury	Finsbura	k1gFnSc2	Finsbura
Park	park	k1gInSc1	park
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
zmíněné	zmíněný	k2eAgFnPc1d1	zmíněná
stanice	stanice	k1gFnPc1	stanice
obsluhované	obsluhovaný	k2eAgFnSc2d1	obsluhovaná
londýnským	londýnský	k2eAgNnSc7d1	Londýnské
metrem	metro	k1gNnSc7	metro
a	a	k8xC	a
společností	společnost	k1gFnSc7	společnost
First	First	k1gMnSc1	First
Capital	Capital	k1gMnSc1	Capital
Connect	Connect	k1gMnSc1	Connect
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
zhruba	zhruba	k6eAd1	zhruba
10	[number]	k4	10
minut	minuta	k1gFnPc2	minuta
chůze	chůze	k1gFnSc2	chůze
od	od	k7c2	od
stadionu	stadion	k1gInSc2	stadion
<g/>
.	.	kIx.	.
<g/>
Jízda	jízda	k1gFnSc1	jízda
na	na	k7c4	na
Emirates	Emirates	k1gInSc4	Emirates
autem	auto	k1gNnSc7	auto
se	se	k3xPyFc4	se
důrazně	důrazně	k6eAd1	důrazně
nedoporučuje	doporučovat	k5eNaImIp3nS	doporučovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c4	v
den	den	k1gInSc4	den
zápasu	zápas	k1gInSc2	zápas
jsou	být	k5eAaImIp3nP	být
kolem	kolem	k7c2	kolem
stadionu	stadion	k1gInSc2	stadion
přísná	přísný	k2eAgNnPc1d1	přísné
omezení	omezení	k1gNnPc1	omezení
provozu	provoz	k1gInSc2	provoz
a	a	k8xC	a
parkování	parkování	k1gNnSc2	parkování
<g/>
.	.	kIx.	.
</s>
<s>
Hodinu	hodina	k1gFnSc4	hodina
před	před	k7c7	před
výkopem	výkop	k1gInSc7	výkop
a	a	k8xC	a
hodinu	hodina	k1gFnSc4	hodina
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
zápasu	zápas	k1gInSc2	zápas
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
okolních	okolní	k2eAgFnPc6d1	okolní
ulicích	ulice	k1gFnPc6	ulice
úplný	úplný	k2eAgInSc1d1	úplný
zákaz	zákaz	k1gInSc1	zákaz
automobilového	automobilový	k2eAgInSc2d1	automobilový
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nP	tvořit
místní	místní	k2eAgMnPc1d1	místní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
a	a	k8xC	a
podnikatelé	podnikatel	k1gMnPc1	podnikatel
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
do	do	k7c2	do
uzavřených	uzavřený	k2eAgFnPc2d1	uzavřená
ulic	ulice	k1gFnPc2	ulice
mají	mít	k5eAaImIp3nP	mít
přístup	přístup	k1gInSc4	přístup
<g/>
.	.	kIx.	.
<g/>
Stadion	stadion	k1gInSc1	stadion
se	se	k3xPyFc4	se
majitelům	majitel	k1gMnPc3	majitel
vstupenek	vstupenka	k1gFnPc2	vstupenka
otevírá	otevírat	k5eAaImIp3nS	otevírat
vždy	vždy	k6eAd1	vždy
dvě	dva	k4xCgFnPc4	dva
hodiny	hodina	k1gFnPc4	hodina
před	před	k7c7	před
výkopem	výkop	k1gInSc7	výkop
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc4d1	hlavní
klubový	klubový	k2eAgInSc4d1	klubový
obchod	obchod	k1gInSc4	obchod
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
The	The	k1gFnSc1	The
Armoury	Armoura	k1gFnSc2	Armoura
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
a	a	k8xC	a
pokladny	pokladna	k1gFnPc1	pokladna
se	se	k3xPyFc4	se
nalézají	nalézat	k5eAaImIp3nP	nalézat
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
tribuny	tribuna	k1gFnSc2	tribuna
West	West	k1gInSc1	West
stand	stand	k?	stand
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
klubové	klubový	k2eAgInPc1d1	klubový
obchody	obchod	k1gInPc1	obchod
jsou	být	k5eAaImIp3nP	být
potom	potom	k6eAd1	potom
k	k	k7c3	k
nalezení	nalezení	k1gNnSc3	nalezení
u	u	k7c2	u
mostu	most	k1gInSc2	most
North	North	k1gInSc4	North
Bank	bank	k1gInSc1	bank
Bridge	Bridge	k1gFnSc1	Bridge
(	(	kIx(	(
<g/>
'	'	kIx"	'
<g/>
All	All	k1gMnSc1	All
Arsenal	Arsenal	k1gMnSc1	Arsenal
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
a	a	k8xC	a
vedle	vedle	k7c2	vedle
stanice	stanice	k1gFnSc2	stanice
Finsbury	Finsbura	k1gFnSc2	Finsbura
Park	park	k1gInSc1	park
(	(	kIx(	(
<g/>
'	'	kIx"	'
<g/>
Arsenal	Arsenal	k1gMnSc5	Arsenal
Store	Stor	k1gMnSc5	Stor
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Arsenal	Arsenat	k5eAaImAgMnS	Arsenat
provozuje	provozovat	k5eAaImIp3nS	provozovat
elektronický	elektronický	k2eAgInSc4d1	elektronický
systém	systém	k1gInSc4	systém
lístků	lístek	k1gInPc2	lístek
pro	pro	k7c4	pro
členy	člen	k1gMnPc4	člen
'	'	kIx"	'
<g/>
The	The	k1gMnSc4	The
Arsenal	Arsenal	k1gMnSc4	Arsenal
<g/>
'	'	kIx"	'
(	(	kIx(	(
<g/>
klubový	klubový	k2eAgInSc4d1	klubový
fanklub	fanklub	k1gInSc4	fanklub
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
využívají	využívat	k5eAaImIp3nP	využívat
klubové	klubový	k2eAgFnSc2d1	klubová
karty	karta	k1gFnSc2	karta
ke	k	k7c3	k
vstupu	vstup	k1gInSc3	vstup
na	na	k7c4	na
stadion	stadion	k1gInSc4	stadion
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gFnPc4	on
odstraněna	odstraněn	k2eAgFnSc1d1	odstraněna
nutnost	nutnost	k1gFnSc1	nutnost
projít	projít	k5eAaPmF	projít
turniketem	turniket	k1gInSc7	turniket
<g/>
.	.	kIx.	.
</s>
<s>
Ostatním	ostatní	k2eAgMnPc3d1	ostatní
příznivcům	příznivec	k1gMnPc3	příznivec
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
standardní	standardní	k2eAgFnPc4d1	standardní
papírové	papírový	k2eAgFnPc4d1	papírová
vstupenky	vstupenka	k1gFnPc4	vstupenka
s	s	k7c7	s
RFID	RFID	kA	RFID
štítkem	štítek	k1gInSc7	štítek
umožňující	umožňující	k2eAgNnSc4d1	umožňující
jim	on	k3xPp3gFnPc3	on
vstup	vstup	k1gInSc1	vstup
na	na	k7c4	na
stadion	stadion	k1gInSc4	stadion
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Emirates	Emirates	k1gInSc4	Emirates
Stadium	stadium	k1gNnSc1	stadium
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ANDREWS	ANDREWS	kA	ANDREWS
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
L.	L.	kA	L.
Sport	sport	k1gInSc1	sport
and	and	k?	and
corporate	corporat	k1gInSc5	corporat
nationalisms	nationalisms	k1gInSc4	nationalisms
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
<g/>
:	:	kIx,	:
Berg	Berg	k1gInSc1	Berg
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
85973	[number]	k4	85973
<g/>
-	-	kIx~	-
<g/>
799	[number]	k4	799
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BERNSTEIN	BERNSTEIN	kA	BERNSTEIN
<g/>
,	,	kIx,	,
George	Georg	k1gMnSc2	Georg
L.	L.	kA	L.
The	The	k1gMnSc1	The
Myth	Myth	k1gMnSc1	Myth
Of	Of	k1gMnSc1	Of
Decline	Declin	k1gInSc5	Declin
<g/>
:	:	kIx,	:
The	The	k1gFnSc3	The
Rise	Rise	k1gNnSc2	Rise
of	of	k?	of
Britain	Britain	k1gMnSc1	Britain
Since	Sinec	k1gInSc2	Sinec
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
<g/>
:	:	kIx,	:
Pimlico	Pimlico	k1gNnSc1	Pimlico
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
84413	[number]	k4	84413
<g/>
-	-	kIx~	-
<g/>
102	[number]	k4	102
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
BROWN	BROWN	kA	BROWN
<g/>
,	,	kIx,	,
Adam	Adam	k1gMnSc1	Adam
<g/>
.	.	kIx.	.
</s>
<s>
Fanatics	Fanatics	k6eAd1	Fanatics
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
:	:	kIx,	:
power	powra	k1gFnPc2	powra
<g/>
,	,	kIx,	,
identity	identita	k1gFnSc2	identita
<g/>
,	,	kIx,	,
and	and	k?	and
fandom	fandom	k1gInSc1	fandom
in	in	k?	in
football	football	k1gInSc1	football
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
<g/>
:	:	kIx,	:
Routledge	Routledge	k1gNnSc1	Routledge
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
415	[number]	k4	415
<g/>
-	-	kIx~	-
<g/>
18103	[number]	k4	18103
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
CONN	CONN	kA	CONN
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Beautiful	Beautiful	k1gInSc1	Beautiful
Game	game	k1gInSc1	game
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
:	:	kIx,	:
Searching	Searching	k1gInSc1	Searching
for	forum	k1gNnPc2	forum
the	the	k?	the
Soul	Soul	k1gInSc1	Soul
of	of	k?	of
Football	Football	k1gInSc1	Football
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
<g/>
:	:	kIx,	:
Random	Random	k1gInSc1	Random
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
4464	[number]	k4	4464
<g/>
-	-	kIx~	-
<g/>
2042	[number]	k4	2042
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GLINERT	GLINERT	kA	GLINERT
<g/>
,	,	kIx,	,
Ed	Ed	k1gMnSc1	Ed
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
London	London	k1gMnSc1	London
Football	Football	k1gMnSc1	Football
Companion	Companion	k1gInSc1	Companion
<g/>
:	:	kIx,	:
A	a	k9	a
Site-by-site	Siteyit	k1gInSc5	Site-by-sit
Celebration	Celebration	k1gInSc1	Celebration
of	of	k?	of
the	the	k?	the
Capital	Capital	k1gMnSc1	Capital
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Favourite	Favourit	k1gInSc5	Favourit
Sport	sport	k1gInSc1	sport
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
<g/>
:	:	kIx,	:
Bloomsbury	Bloomsbur	k1gInPc1	Bloomsbur
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
7475	[number]	k4	7475
<g/>
-	-	kIx~	-
<g/>
9516	[number]	k4	9516
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
SPURLING	SPURLING	kA	SPURLING
<g/>
,	,	kIx,	,
Jon	Jon	k1gFnSc1	Jon
<g/>
.	.	kIx.	.
</s>
<s>
Highbury	Highbur	k1gInPc1	Highbur
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Story	story	k1gFnSc2	story
of	of	k?	of
Arsenal	Arsenal	k1gMnSc1	Arsenal
In	In	k1gMnSc1	In
<g/>
,	,	kIx,	,
Issue	Issue	k1gFnSc1	Issue
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Londýn	Londýn	k1gInSc1	Londýn
<g/>
:	:	kIx,	:
Hachette	Hachett	k1gMnSc5	Hachett
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
4091	[number]	k4	4091
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
579	[number]	k4	579
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Emirates	Emiratesa	k1gFnPc2	Emiratesa
Stadium	stadium	k1gNnSc4	stadium
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Emirates	Emirates	k1gInSc1	Emirates
Stadium	stadium	k1gNnSc1	stadium
na	na	k7c4	na
Arsenal	Arsenal	k1gFnSc4	Arsenal
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
