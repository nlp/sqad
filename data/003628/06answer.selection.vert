<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
vysílá	vysílat	k5eAaImIp3nS	vysílat
denně	denně	k6eAd1	denně
od	od	k7c2	od
6	[number]	k4	6
ráno	ráno	k6eAd1	ráno
do	do	k7c2	do
půlnoci	půlnoc	k1gFnSc2	půlnoc
soukromá	soukromý	k2eAgFnSc1d1	soukromá
stanice	stanice	k1gFnSc1	stanice
Brněnská	brněnský	k2eAgFnSc1d1	brněnská
televize	televize	k1gFnSc1	televize
(	(	kIx(	(
<g/>
B-TV	B-TV	k1gFnSc1	B-TV
<g/>
)	)	kIx)	)
dostupná	dostupný	k2eAgFnSc1d1	dostupná
v	v	k7c6	v
kabelových	kabelový	k2eAgFnPc6d1	kabelová
sítích	síť	k1gFnPc6	síť
UPC	UPC	kA	UPC
<g/>
,	,	kIx,	,
O2TV	O2TV	k1gFnSc1	O2TV
a	a	k8xC	a
NETBOX	NETBOX	kA	NETBOX
–	–	k?	–
věnuje	věnovat	k5eAaImIp3nS	věnovat
se	se	k3xPyFc4	se
lokálnímu	lokální	k2eAgNnSc3d1	lokální
zpravodajství	zpravodajství	k1gNnSc3	zpravodajství
z	z	k7c2	z
města	město	k1gNnSc2	město
a	a	k8xC	a
kraje	kraj	k1gInSc2	kraj
<g/>
.	.	kIx.	.
</s>
