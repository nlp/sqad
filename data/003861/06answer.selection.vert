<s>
Řím	Řím	k1gInSc1	Řím
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
a	a	k8xC	a
latinsky	latinsky	k6eAd1	latinsky
Roma	Rom	k1gMnSc4	Rom
<g/>
,	,	kIx,	,
přezdívaný	přezdívaný	k2eAgMnSc1d1	přezdívaný
též	též	k9	též
Věčné	věčný	k2eAgNnSc1d1	věčné
město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
oblasti	oblast	k1gFnSc2	oblast
Lazio	Lazio	k6eAd1	Lazio
a	a	k8xC	a
provincie	provincie	k1gFnSc1	provincie
Roma	Rom	k1gMnSc2	Rom
<g/>
.	.	kIx.	.
</s>
