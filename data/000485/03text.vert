<s>
Joule	joule	k1gInSc1	joule
[	[	kIx(	[
<g/>
džaul	džaul	k1gInSc1	džaul
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
J	J	kA	J
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednotka	jednotka	k1gFnSc1	jednotka
práce	práce	k1gFnSc2	práce
a	a	k8xC	a
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
SI	se	k3xPyFc3	se
patří	patřit	k5eAaImIp3nS	patřit
joule	joule	k1gInSc4	joule
mezi	mezi	k7c4	mezi
odvozené	odvozený	k2eAgFnPc4d1	odvozená
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
,	,	kIx,	,
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
jednotek	jednotka	k1gFnPc2	jednotka
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
vztahem	vztah	k1gInSc7	vztah
<g/>
:	:	kIx,	:
J	J	kA	J
=	=	kIx~	=
kg	kg	kA	kg
·	·	k?	·
m	m	kA	m
<g/>
2	[number]	k4	2
·	·	k?	·
s	s	k7c7	s
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
(	(	kIx(	(
<g/>
též	též	k9	též
N	N	kA	N
·	·	k?	·
m	m	kA	m
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
kg	kg	kA	kg
je	být	k5eAaImIp3nS	být
kilogram	kilogram	k1gInSc4	kilogram
<g/>
,	,	kIx,	,
m	m	kA	m
je	být	k5eAaImIp3nS	být
metr	metr	k1gInSc4	metr
<g/>
,	,	kIx,	,
s	s	k7c7	s
je	být	k5eAaImIp3nS	být
sekunda	sekunda	k1gFnSc1	sekunda
<g/>
,	,	kIx,	,
N	N	kA	N
je	být	k5eAaImIp3nS	být
newton	newton	k1gInSc4	newton
1	[number]	k4	1
Joule	joule	k1gInSc1	joule
je	být	k5eAaImIp3nS	být
definován	definovat	k5eAaBmNgInS	definovat
jako	jako	k8xC	jako
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
koná	konat	k5eAaImIp3nS	konat
síla	síla	k1gFnSc1	síla
1	[number]	k4	1
N	N	kA	N
působící	působící	k2eAgFnSc1d1	působící
po	po	k7c6	po
dráze	dráha	k1gFnSc6	dráha
1	[number]	k4	1
m	m	kA	m
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
pohybu	pohyb	k1gInSc2	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Jednotka	jednotka	k1gFnSc1	jednotka
joule	joule	k1gInSc1	joule
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
na	na	k7c4	na
počest	počest	k1gFnSc4	počest
anglického	anglický	k2eAgMnSc2d1	anglický
fyzika	fyzik	k1gMnSc2	fyzik
Jamese	Jamese	k1gFnSc2	Jamese
P.	P.	kA	P.
Joulea	Jouleus	k1gMnSc2	Jouleus
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
zapamatování	zapamatování	k1gNnSc3	zapamatování
základních	základní	k2eAgFnPc2d1	základní
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yQgFnPc2	který
je	být	k5eAaImIp3nS	být
složena	složen	k2eAgFnSc1d1	složena
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
rovnici	rovnice	k1gFnSc4	rovnice
[[	[[	k?	[[
<g/>
E	E	kA	E
=	=	kIx~	=
m	m	kA	m
c	c	k0	c
<g/>
2	[number]	k4	2
<g/>
]]	]]	k?	]]
(	(	kIx(	(
<g/>
E	E	kA	E
je	být	k5eAaImIp3nS	být
energie	energie	k1gFnPc4	energie
v	v	k7c6	v
joulech	joule	k1gInPc6	joule
<g/>
,	,	kIx,	,
m	m	kA	m
hmotnost	hmotnost	k1gFnSc4	hmotnost
v	v	k7c6	v
kilogramech	kilogram	k1gInPc6	kilogram
<g/>
,	,	kIx,	,
c	c	k0	c
rychlost	rychlost	k1gFnSc4	rychlost
světla	světlo	k1gNnSc2	světlo
v	v	k7c6	v
metrech	metr	k1gInPc6	metr
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
jednotka	jednotka	k1gFnSc1	jednotka
1	[number]	k4	1
joule	joule	k1gInSc1	joule
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
malá	malý	k2eAgFnSc1d1	malá
<g/>
,	,	kIx,	,
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
její	její	k3xOp3gInPc4	její
násobky	násobek	k1gInPc4	násobek
(	(	kIx(	(
<g/>
kilojoule	kilojoule	k1gInSc1	kilojoule
<g/>
,	,	kIx,	,
megajoule	megajoule	k1gInSc1	megajoule
<g/>
,	,	kIx,	,
gigajoule	gigajoule	k1gInSc1	gigajoule
<g/>
,	,	kIx,	,
terajoule	terajoule	k1gFnSc1	terajoule
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
jednotky	jednotka	k1gFnPc1	jednotka
odvozené	odvozený	k2eAgFnPc1d1	odvozená
z	z	k7c2	z
jednotky	jednotka	k1gFnSc2	jednotka
výkonu	výkon	k1gInSc2	výkon
(	(	kIx(	(
<g/>
1	[number]	k4	1
Watt	watt	k1gInSc1	watt
<g/>
)	)	kIx)	)
-	-	kIx~	-
například	například	k6eAd1	například
kilowatthodina	kilowatthodina	k1gFnSc1	kilowatthodina
(	(	kIx(	(
<g/>
kWh	kwh	kA	kwh
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgFnSc3d1	samotná
jednotce	jednotka	k1gFnSc3	jednotka
joule	joule	k1gInSc4	joule
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
ekvivalentní	ekvivalentní	k2eAgFnSc1d1	ekvivalentní
wattsekunda	wattsekunda	k1gFnSc1	wattsekunda
<g/>
.	.	kIx.	.
</s>
<s>
Přepočty	přepočet	k1gInPc1	přepočet
joule	joule	k1gInSc1	joule
na	na	k7c4	na
kWh	kwh	kA	kwh
<g/>
:	:	kIx,	:
1	[number]	k4	1
J	J	kA	J
=	=	kIx~	=
2,778	[number]	k4	2,778
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
kWh	kwh	kA	kwh
1	[number]	k4	1
J	J	kA	J
=	=	kIx~	=
1	[number]	k4	1
Ws	Ws	k1gFnSc1	Ws
1	[number]	k4	1
kWh	kwh	kA	kwh
=	=	kIx~	=
3	[number]	k4	3
600	[number]	k4	600
000	[number]	k4	000
J	J	kA	J
=	=	kIx~	=
3,6	[number]	k4	3,6
MJ	mj	kA	mj
=	=	kIx~	=
1,343	[number]	k4	1,343
hph	hph	k?	hph
(	(	kIx(	(
<g/>
horse	hors	k1gMnSc2	hors
power	power	k1gMnSc1	power
hour	hour	k1gMnSc1	hour
-	-	kIx~	-
hodin	hodina	k1gFnPc2	hodina
koňské	koňský	k2eAgFnSc2d1	koňská
síly	síla	k1gFnSc2	síla
<g/>
)	)	kIx)	)
Starší	starý	k2eAgFnPc1d2	starší
jednotky	jednotka	k1gFnPc1	jednotka
energie	energie	k1gFnSc2	energie
jsou	být	k5eAaImIp3nP	být
kalorie	kalorie	k1gFnPc1	kalorie
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
cal	cal	k?	cal
<g/>
)	)	kIx)	)
a	a	k8xC	a
erg	erg	k1gInSc4	erg
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
malé	malý	k2eAgFnPc4d1	malá
energie	energie	k1gFnPc4	energie
na	na	k7c6	na
atomární	atomární	k2eAgFnSc6d1	atomární
úrovni	úroveň	k1gFnSc6	úroveň
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
též	též	k9	též
jednotka	jednotka	k1gFnSc1	jednotka
elektronvolt	elektronvolt	k1gInSc1	elektronvolt
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
eV	eV	k?	eV
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
cal	cal	k?	cal
=	=	kIx~	=
4,187	[number]	k4	4,187
J	J	kA	J
1	[number]	k4	1
kcal	kcalum	k1gNnPc2	kcalum
=	=	kIx~	=
1000	[number]	k4	1000
Cal	Cal	k1gFnPc2	Cal
=	=	kIx~	=
4187	[number]	k4	4187
J	J	kA	J
(	(	kIx(	(
<g/>
energie	energie	k1gFnSc1	energie
nutná	nutný	k2eAgFnSc1d1	nutná
za	za	k7c2	za
standardních	standardní	k2eAgFnPc2d1	standardní
podmínek	podmínka	k1gFnPc2	podmínka
k	k	k7c3	k
ohřátí	ohřátý	k2eAgMnPc1d1	ohřátý
1	[number]	k4	1
kg	kg	kA	kg
vody	voda	k1gFnSc2	voda
o	o	k7c4	o
1	[number]	k4	1
°	°	k?	°
<g/>
C	C	kA	C
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
1	[number]	k4	1
erg	erg	k1gInSc1	erg
=	=	kIx~	=
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
J	J	kA	J
1	[number]	k4	1
eV	eV	k?	eV
=	=	kIx~	=
1,602	[number]	k4	1,602
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
J	J	kA	J
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
spotřebou	spotřeba	k1gFnSc7	spotřeba
energie	energie	k1gFnSc2	energie
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
i	i	k9	i
jednotka	jednotka	k1gFnSc1	jednotka
zvaná	zvaný	k2eAgFnSc1d1	zvaná
ekvivalent	ekvivalent	k1gInSc4	ekvivalent
tuny	tuna	k1gFnSc2	tuna
ropy	ropa	k1gFnSc2	ropa
(	(	kIx(	(
<g/>
Ton	Ton	k1gMnSc1	Ton
of	of	k?	of
Oil	Oil	k1gMnSc1	Oil
Equivalent	Equivalent	k1gMnSc1	Equivalent
-	-	kIx~	-
TOE	TOE	kA	TOE
nebo	nebo	k8xC	nebo
toe	toe	k?	toe
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
energii	energie	k1gFnSc4	energie
získané	získaný	k2eAgNnSc1d1	získané
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
tuny	tuna	k1gFnSc2	tuna
ropy	ropa	k1gFnSc2	ropa
<g/>
.	.	kIx.	.
</s>
<s>
Řidčeji	řídce	k6eAd2	řídce
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
jednotky	jednotka	k1gFnPc1	jednotka
jako	jako	k8xC	jako
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
barelu	barel	k1gInSc2	barel
ropy	ropa	k1gFnSc2	ropa
(	(	kIx(	(
<g/>
Barrel	Barrel	k1gInSc1	Barrel
of	of	k?	of
oil	oil	k?	oil
equivalent	equivalent	k1gMnSc1	equivalent
-	-	kIx~	-
BOE	BOE	kA	BOE
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
zavedené	zavedený	k2eAgFnPc1d1	zavedená
jednotky	jednotka	k1gFnPc1	jednotka
jsou	být	k5eAaImIp3nP	být
BTU	BTU	kA	BTU
(	(	kIx(	(
<g/>
British	British	k1gMnSc1	British
Thermal	Thermal	k1gMnSc1	Thermal
Unit	Unit	k1gMnSc1	Unit
-	-	kIx~	-
britská	britský	k2eAgFnSc1d1	britská
tepelná	tepelný	k2eAgFnSc1d1	tepelná
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
therm	thermy	k1gFnPc2	thermy
a	a	k8xC	a
tTNT	tTNT	k?	tTNT
(	(	kIx(	(
<g/>
tuna	tuna	k1gFnSc1	tuna
trinitrotoluenu	trinitrotoluen	k1gInSc2	trinitrotoluen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
1	[number]	k4	1
J	J	kA	J
=	=	kIx~	=
2,39	[number]	k4	2,39
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
TOE	TOE	kA	TOE
1	[number]	k4	1
TOE	TOE	kA	TOE
=	=	kIx~	=
41,868	[number]	k4	41,868
<g/>
×	×	k?	×
<g/>
109	[number]	k4	109
J	J	kA	J
1	[number]	k4	1
BOE	BOE	kA	BOE
=	=	kIx~	=
0,146	[number]	k4	0,146
TOE	TOE	kA	TOE
≈	≈	k?	≈
6,113	[number]	k4	6,113
<g/>
×	×	k?	×
<g/>
109	[number]	k4	109
J	J	kA	J
1	[number]	k4	1
BTU	BTU	kA	BTU
=	=	kIx~	=
1055	[number]	k4	1055
J	J	kA	J
1	[number]	k4	1
therm	thermy	k1gFnPc2	thermy
=	=	kIx~	=
105	[number]	k4	105
MJ	mj	kA	mj
≈	≈	k?	≈
1	[number]	k4	1
000	[number]	k4	000
000	[number]	k4	000
BTU	BTU	kA	BTU
1	[number]	k4	1
tTNT	tTNT	k?	tTNT
=	=	kIx~	=
4,187	[number]	k4	4,187
<g/>
×	×	k?	×
<g/>
109	[number]	k4	109
J	J	kA	J
≈	≈	k?	≈
1	[number]	k4	1
Gcal	Gcalum	k1gNnPc2	Gcalum
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
joule	joule	k1gInSc1	joule
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
