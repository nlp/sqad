<p>
<s>
Vstupní	vstupní	k2eAgInSc1d1	vstupní
draft	draft	k1gInSc1	draft
NHL	NHL	kA	NHL
2016	[number]	k4	2016
byl	být	k5eAaImAgInS	být
54	[number]	k4	54
<g/>
.	.	kIx.	.
vstupním	vstupní	k2eAgInSc7d1	vstupní
draftem	draft	k1gInSc7	draft
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
NHL	NHL	kA	NHL
<g/>
.	.	kIx.	.
</s>
<s>
Konal	konat	k5eAaImAgInS	konat
se	se	k3xPyFc4	se
od	od	k7c2	od
24	[number]	k4	24
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
do	do	k7c2	do
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2016	[number]	k4	2016
ve	v	k7c4	v
First	First	k1gInSc4	First
Niagara	Niagara	k1gFnSc1	Niagara
Center	centrum	k1gNnPc2	centrum
v	v	k7c6	v
Buffalu	Buffal	k1gInSc6	Buffal
<g/>
,	,	kIx,	,
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc2d1	americký
(	(	kIx(	(
<g/>
v	v	k7c6	v
domácí	domácí	k2eAgFnSc6d1	domácí
aréně	aréna	k1gFnSc6	aréna
Buffala	Buffala	k1gMnSc1	Buffala
Sabres	Sabres	k1gMnSc1	Sabres
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Buffalo	Buffat	k5eAaPmAgNnS	Buffat
hostilo	hostit	k5eAaImAgNnS	hostit
vstupní	vstupní	k2eAgInSc4d1	vstupní
draft	draft	k1gInSc4	draft
potřetí	potřetí	k4xO	potřetí
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
předchozí	předchozí	k2eAgInPc4d1	předchozí
drafty	draft	k1gInPc4	draft
konaly	konat	k5eAaImAgFnP	konat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
a	a	k8xC	a
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Věkové	věkový	k2eAgNnSc1d1	věkové
omezení	omezení	k1gNnSc1	omezení
==	==	k?	==
</s>
</p>
<p>
<s>
Nárok	nárok	k1gInSc1	nárok
na	na	k7c4	na
výběr	výběr	k1gInSc4	výběr
v	v	k7c6	v
draftu	draft	k1gInSc6	draft
měli	mít	k5eAaImAgMnP	mít
lední	lední	k2eAgMnPc1d1	lední
hokejisté	hokejista	k1gMnPc1	hokejista
narození	narozený	k2eAgMnPc1d1	narozený
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1996	[number]	k4	1996
do	do	k7c2	do
15	[number]	k4	15
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
nedraftování	nedraftování	k1gNnPc2	nedraftování
hráči	hráč	k1gMnPc1	hráč
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
narodili	narodit	k5eAaPmAgMnP	narodit
mimo	mimo	k7c4	mimo
Severní	severní	k2eAgFnSc4d1	severní
Ameriku	Amerika	k1gFnSc4	Amerika
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
mohli	moct	k5eAaImAgMnP	moct
být	být	k5eAaImF	být
rovněž	rovněž	k9	rovněž
vybráni	vybrán	k2eAgMnPc1d1	vybrán
v	v	k7c6	v
draftu	draft	k1gInSc6	draft
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
hráči	hráč	k1gMnPc1	hráč
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
draftováni	draftovat	k5eAaImNgMnP	draftovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nepodepsali	podepsat	k5eNaPmAgMnP	podepsat
do	do	k7c2	do
draftu	draft	k1gInSc2	draft
žádnou	žádný	k3yNgFnSc4	žádný
profesionální	profesionální	k2eAgFnSc4d1	profesionální
smlouvu	smlouva	k1gFnSc4	smlouva
v	v	k7c6	v
NHL	NHL	kA	NHL
<g/>
,	,	kIx,	,
a	a	k8xC	a
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
narodili	narodit	k5eAaPmAgMnP	narodit
po	po	k7c6	po
30	[number]	k4	30
<g/>
.	.	kIx.	.
červnu	červen	k1gInSc3	červen
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
mohli	moct	k5eAaImAgMnP	moct
být	být	k5eAaImF	být
rovněž	rovněž	k9	rovněž
oprávnění	oprávnění	k1gNnSc4	oprávnění
být	být	k5eAaImF	být
znovu	znovu	k6eAd1	znovu
draftováni	draftován	k2eAgMnPc1d1	draftován
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Loterie	loterie	k1gFnSc1	loterie
před	před	k7c7	před
draftem	draft	k1gInSc7	draft
==	==	k?	==
</s>
</p>
<p>
<s>
Všech	všecek	k3xTgInPc2	všecek
čtrnáct	čtrnáct	k4xCc4	čtrnáct
týmů	tým	k1gInPc2	tým
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
sezóně	sezóna	k1gFnSc6	sezóna
NHL	NHL	kA	NHL
2012	[number]	k4	2012
<g/>
/	/	kIx~	/
<g/>
2013	[number]	k4	2013
neprobojovali	probojovat	k5eNaPmAgMnP	probojovat
do	do	k7c2	do
play-off	playff	k1gMnSc1	play-off
o	o	k7c6	o
Stanley	Stanlea	k1gMnSc2	Stanlea
Cupu	cup	k1gInSc6	cup
měly	mít	k5eAaImAgFnP	mít
větší	veliký	k2eAgFnSc4d2	veliký
šanci	šance	k1gFnSc4	šance
získat	získat	k5eAaPmF	získat
první	první	k4xOgInSc4	první
celkový	celkový	k2eAgInSc4d1	celkový
výběr	výběr	k1gInSc4	výběr
tohoto	tento	k3xDgInSc2	tento
draftu	draft	k1gInSc2	draft
<g/>
.	.	kIx.	.
</s>
<s>
Počínaje	počínaje	k7c7	počínaje
sezónou	sezóna	k1gFnSc7	sezóna
2014	[number]	k4	2014
<g/>
/	/	kIx~	/
<g/>
2015	[number]	k4	2015
změnila	změnit	k5eAaPmAgFnS	změnit
NHL	NHL	kA	NHL
systémem	systém	k1gInSc7	systém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
v	v	k7c6	v
minulých	minulý	k2eAgInPc6d1	minulý
letech	let	k1gInPc6	let
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nového	nový	k2eAgInSc2d1	nový
systému	systém	k1gInSc2	systém
se	se	k3xPyFc4	se
šance	šance	k1gFnSc1	šance
na	na	k7c4	na
výhru	výhra	k1gFnSc4	výhra
v	v	k7c6	v
loterii	loterie	k1gFnSc6	loterie
před	před	k7c7	před
draftem	draft	k1gInSc7	draft
pro	pro	k7c4	pro
čtyři	čtyři	k4xCgInPc4	čtyři
nejhorší	zlý	k2eAgInPc4d3	Nejhorší
týmy	tým	k1gInPc4	tým
v	v	k7c6	v
lize	liga	k1gFnSc6	liga
snížily	snížit	k5eAaPmAgFnP	snížit
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
šance	šance	k1gFnSc1	šance
pro	pro	k7c4	pro
ostatní	ostatní	k2eAgInPc4d1	ostatní
nepostupující	postupující	k2eNgInPc4d1	nepostupující
týmy	tým	k1gInPc4	tým
do	do	k7c2	do
play-off	playff	k1gInSc1	play-off
se	se	k3xPyFc4	se
zvýšily	zvýšit	k5eAaPmAgFnP	zvýšit
<g/>
.	.	kIx.	.
</s>
<s>
Počínaje	počínaje	k7c7	počínaje
tímto	tento	k3xDgInSc7	tento
návrhem	návrh	k1gInSc7	návrh
budou	být	k5eAaImBp3nP	být
první	první	k4xOgInPc4	první
tři	tři	k4xCgInPc4	tři
výběry	výběr	k1gInPc4	výběr
uděleny	udělen	k2eAgFnPc4d1	udělena
losováním	losování	k1gNnSc7	losování
<g/>
.	.	kIx.	.
</s>
<s>
Šance	šance	k1gFnSc1	šance
na	na	k7c4	na
výhru	výhra	k1gFnSc4	výhra
druhého	druhý	k4xOgInSc2	druhý
a	a	k8xC	a
třetího	třetí	k4xOgInSc2	třetí
výběru	výběr	k1gInSc2	výběr
se	se	k3xPyFc4	se
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
tým	tým	k1gInSc1	tým
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
předchozí	předchozí	k2eAgNnSc4d1	předchozí
losování	losování	k1gNnSc4	losování
<g/>
.	.	kIx.	.
</s>
<s>
Loterie	loterie	k1gFnSc1	loterie
před	před	k7c7	před
draftem	draft	k1gInSc7	draft
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Nejlépe	dobře	k6eAd3	dobře
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
Toronto	Toronto	k1gNnSc1	Toronto
Maple	Maple	k1gNnSc2	Maple
Leafs	Leafsa	k1gFnPc2	Leafsa
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
udrželo	udržet	k5eAaPmAgNnS	udržet
nejvyšší	vysoký	k2eAgFnPc4d3	nejvyšší
počáteční	počáteční	k2eAgFnPc4d1	počáteční
šance	šance	k1gFnPc4	šance
na	na	k7c4	na
první	první	k4xOgInSc4	první
výběr	výběr	k1gInSc4	výběr
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
druhý	druhý	k4xOgMnSc1	druhý
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
Winnipeg	Winnipega	k1gFnPc2	Winnipega
Jets	Jets	k1gInSc4	Jets
a	a	k8xC	a
třetí	třetí	k4xOgMnSc1	třetí
Columbus	Columbus	k1gMnSc1	Columbus
Blue	Blu	k1gFnSc2	Blu
Jackets	Jackets	k1gInSc1	Jackets
svá	svůj	k3xOyFgNnPc4	svůj
umístění	umístění	k1gNnPc4	umístění
v	v	k7c6	v
prognózách	prognóza	k1gFnPc6	prognóza
pro	pro	k7c4	pro
výběr	výběr	k1gInSc4	výběr
vylepšili	vylepšit	k5eAaPmAgMnP	vylepšit
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
posunuli	posunout	k5eAaPmAgMnP	posunout
ze	z	k7c2	z
šestého	šestý	k4xOgNnSc2	šestý
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
čtvrtého	čtvrtý	k4xOgNnSc2	čtvrtý
místa	místo	k1gNnSc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
procesu	proces	k1gInSc6	proces
Edmonton	Edmonton	k1gInSc1	Edmonton
Oilers	Oilers	k1gInSc1	Oilers
a	a	k8xC	a
Vancouver	Vancouver	k1gInSc1	Vancouver
Canucks	Canucksa	k1gFnPc2	Canucksa
klesli	klesnout	k5eAaPmAgMnP	klesnout
o	o	k7c4	o
dvě	dva	k4xCgNnPc4	dva
místa	místo	k1gNnPc4	místo
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
a	a	k8xC	a
třetí	třetí	k4xOgFnSc2	třetí
výběrové	výběrový	k2eAgFnSc2d1	výběrová
příčky	příčka	k1gFnSc2	příčka
na	na	k7c4	na
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
a	a	k8xC	a
pátou	pátý	k4xOgFnSc4	pátý
celkově	celkově	k6eAd1	celkově
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Calgary	Calgary	k1gNnSc1	Calgary
Flames	Flamesa	k1gFnPc2	Flamesa
kleslo	klesnout	k5eAaPmAgNnS	klesnout
z	z	k7c2	z
pátého	pátý	k4xOgNnSc2	pátý
na	na	k7c4	na
celkové	celkový	k2eAgNnSc4d1	celkové
šesté	šestý	k4xOgNnSc4	šestý
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nejlepší	dobrý	k2eAgFnPc4d3	nejlepší
vyhlídky	vyhlídka	k1gFnPc4	vyhlídka
==	==	k?	==
</s>
</p>
<p>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
<g/>
:	:	kIx,	:
Centrální	centrální	k2eAgInSc1d1	centrální
úřad	úřad	k1gInSc1	úřad
skautingu	skauting	k1gInSc2	skauting
NHL	NHL	kA	NHL
(	(	kIx(	(
<g/>
NHL	NHL	kA	NHL
Central	Central	k1gFnSc1	Central
Scouting	Scouting	k1gInSc1	Scouting
Bureau	Bureaus	k1gInSc2	Bureaus
<g/>
)	)	kIx)	)
konečné	konečný	k2eAgNnSc1d1	konečné
pořadí	pořadí	k1gNnSc1	pořadí
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výběry	výběr	k1gInPc7	výběr
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgNnPc6d1	jednotlivé
kolech	kolo	k1gNnPc6	kolo
==	==	k?	==
</s>
</p>
<p>
<s>
Výběr	výběr	k1gInSc1	výběr
kol	kol	k7c2	kol
draftu	draft	k1gInSc2	draft
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
</s>
</p>
<p>
<s>
===	===	k?	===
1	[number]	k4	1
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
2	[number]	k4	2
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
3	[number]	k4	3
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
4	[number]	k4	4
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
5	[number]	k4	5
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
6	[number]	k4	6
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
7	[number]	k4	7
<g/>
.	.	kIx.	.
kolo	kolo	k1gNnSc1	kolo
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Draftovaní	draftovaný	k2eAgMnPc1d1	draftovaný
podle	podle	k7c2	podle
národnosti	národnost	k1gFnSc2	národnost
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
2016	[number]	k4	2016
NHL	NHL	kA	NHL
Entry	Entra	k1gFnSc2	Entra
Draft	draft	k1gInSc1	draft
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Vstupní	vstupní	k2eAgInSc1d1	vstupní
draft	draft	k1gInSc1	draft
NHL	NHL	kA	NHL
2016	[number]	k4	2016
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
draftsite	draftsit	k1gInSc5	draftsit
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
