<s>
Některé	některý	k3yIgInPc1	některý
státy	stát	k1gInPc1	stát
mají	mít	k5eAaImIp3nP	mít
více	hodně	k6eAd2	hodně
hlavních	hlavní	k2eAgNnPc2d1	hlavní
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
více	hodně	k6eAd2	hodně
sídelních	sídelní	k2eAgNnPc2d1	sídelní
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jsou	být	k5eAaImIp3nP	být
rozlišena	rozlišen	k2eAgNnPc4d1	rozlišeno
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jaká	jaký	k3yIgFnSc1	jaký
ústavní	ústavní	k2eAgFnSc1d1	ústavní
instituce	instituce	k1gFnSc1	instituce
či	či	k8xC	či
ústřední	ústřední	k2eAgInSc1d1	ústřední
orgán	orgán	k1gInSc1	orgán
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
sídlí	sídlet	k5eAaImIp3nP	sídlet
<g/>
.	.	kIx.	.
</s>
