<p>
<s>
Stůl	stůl	k1gInSc1	stůl
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
nábytku	nábytek	k1gInSc2	nábytek
tvořený	tvořený	k2eAgInSc4d1	tvořený
deskou	deska	k1gFnSc7	deska
a	a	k8xC	a
nohou	noha	k1gFnSc7	noha
či	či	k8xC	či
nohami	noha	k1gFnPc7	noha
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
má	mít	k5eAaImIp3nS	mít
desku	deska	k1gFnSc4	deska
podloženou	podložený	k2eAgFnSc4d1	podložená
lubem	lub	k1gInSc7	lub
či	či	k8xC	či
doplněnou	doplněný	k2eAgFnSc7d1	doplněná
zásuvkou	zásuvka	k1gFnSc7	zásuvka
nebo	nebo	k8xC	nebo
zásuvkami	zásuvka	k1gFnPc7	zásuvka
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
odkládání	odkládání	k1gNnSc3	odkládání
či	či	k8xC	či
ukládání	ukládání	k1gNnSc3	ukládání
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pro	pro	k7c4	pro
stolování	stolování	k1gNnSc4	stolování
nebo	nebo	k8xC	nebo
k	k	k7c3	k
práci	práce	k1gFnSc3	práce
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
mít	mít	k5eAaImF	mít
obvyklou	obvyklý	k2eAgFnSc4d1	obvyklá
nebo	nebo	k8xC	nebo
pohodlnou	pohodlný	k2eAgFnSc4d1	pohodlná
výšku	výška	k1gFnSc4	výška
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
variabilně	variabilně	k6eAd1	variabilně
nastavitelnou	nastavitelný	k2eAgFnSc4d1	nastavitelná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Stůl	stůl	k1gInSc1	stůl
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
používán	používat	k5eAaImNgInS	používat
pro	pro	k7c4	pro
odkládání	odkládání	k1gNnSc4	odkládání
či	či	k8xC	či
ukládání	ukládání	k1gNnSc4	ukládání
věcí	věc	k1gFnPc2	věc
<g/>
,	,	kIx,	,
jídla	jídlo	k1gNnSc2	jídlo
a	a	k8xC	a
nádobí	nádobí	k1gNnSc2	nádobí
a	a	k8xC	a
náčiní	náčiní	k1gNnSc2	náčiní
během	během	k7c2	během
konzumace	konzumace	k1gFnSc2	konzumace
<g/>
,	,	kIx,	,
nádob	nádoba	k1gFnPc2	nádoba
na	na	k7c4	na
nápoje	nápoj	k1gInPc4	nápoj
<g/>
,	,	kIx,	,
knih	kniha	k1gFnPc2	kniha
(	(	kIx(	(
<g/>
zvláště	zvláště	k6eAd1	zvláště
velkých	velký	k2eAgInPc2d1	velký
a	a	k8xC	a
těžkých	těžký	k2eAgInPc2d1	těžký
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rozkladatelných	rozkladatelný	k2eAgFnPc2d1	rozkladatelná
map	mapa	k1gFnPc2	mapa
<g/>
,	,	kIx,	,
papírů	papír	k1gInPc2	papír
během	během	k7c2	během
psaní	psaní	k1gNnSc2	psaní
a	a	k8xC	a
čehokoliv	cokoliv	k3yInSc2	cokoliv
co	co	k9	co
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
mít	mít	k5eAaImF	mít
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
více	hodně	k6eAd2	hodně
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Předměty	předmět	k1gInPc1	předmět
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
položeny	položit	k5eAaPmNgInP	položit
na	na	k7c6	na
stole	stol	k1gInSc6	stol
stále	stále	k6eAd1	stále
<g/>
,	,	kIx,	,
např.	např.	kA	např.
televizor	televizor	k1gInSc1	televizor
<g/>
,	,	kIx,	,
počítač	počítač	k1gInSc1	počítač
<g/>
,	,	kIx,	,
dekorace	dekorace	k1gFnSc1	dekorace
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ubrus	ubrus	k1gInSc4	ubrus
<g/>
,	,	kIx,	,
pokrývající	pokrývající	k2eAgInSc4d1	pokrývající
obvykle	obvykle	k6eAd1	obvykle
celý	celý	k2eAgInSc4d1	celý
stůl	stůl	k1gInSc4	stůl
<g/>
)	)	kIx)	)
atd.	atd.	kA	atd.
Prostírání	prostírání	k1gNnSc1	prostírání
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
uspořádání	uspořádání	k1gNnSc4	uspořádání
nádobí	nádobí	k1gNnSc2	nádobí
a	a	k8xC	a
potravin	potravina	k1gFnPc2	potravina
na	na	k7c6	na
stole	stol	k1gInSc6	stol
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
především	především	k9	především
tradicí	tradice	k1gFnSc7	tradice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozdělení	rozdělení	k1gNnSc1	rozdělení
podle	podle	k7c2	podle
účelu	účel	k1gInSc2	účel
==	==	k?	==
</s>
</p>
<p>
<s>
Psací	psací	k2eAgInSc1d1	psací
stůl	stůl	k1gInSc1	stůl
-	-	kIx~	-
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
pracovnách	pracovna	k1gFnPc6	pracovna
<g/>
,	,	kIx,	,
domácnostech	domácnost	k1gFnPc6	domácnost
<g/>
,	,	kIx,	,
učebnách	učebna	k1gFnPc6	učebna
(	(	kIx(	(
<g/>
katedra	katedra	k1gFnSc1	katedra
pro	pro	k7c4	pro
učitele	učitel	k1gMnPc4	učitel
<g/>
)	)	kIx)	)
i	i	k9	i
na	na	k7c6	na
úřadech	úřad	k1gInPc6	úřad
<g/>
;	;	kIx,	;
stojací	stojací	k2eAgFnPc4d1	stojací
<g/>
:	:	kIx,	:
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
šikmou	šikmý	k2eAgFnSc4d1	šikmá
desku	deska	k1gFnSc4	deska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jídelní	jídelní	k2eAgInSc1d1	jídelní
stůl	stůl	k1gInSc1	stůl
-	-	kIx~	-
slouží	sloužit	k5eAaImIp3nS	sloužit
ke	k	k7c3	k
stravování	stravování	k1gNnSc3	stravování
v	v	k7c6	v
domácnosti	domácnost	k1gFnSc6	domácnost
<g/>
,	,	kIx,	,
školní	školní	k2eAgFnSc6d1	školní
jídelně	jídelna	k1gFnSc6	jídelna
<g/>
,	,	kIx,	,
restauraci	restaurace	k1gFnSc4	restaurace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozkládací	rozkládací	k2eAgInPc1d1	rozkládací
stoly	stol	k1gInPc1	stol
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
zahradní	zahradní	k2eAgFnPc4d1	zahradní
oslavy	oslava	k1gFnPc4	oslava
a	a	k8xC	a
snadný	snadný	k2eAgInSc4d1	snadný
transport	transport	k1gInSc4	transport
<g/>
,	,	kIx,	,
např.	např.	kA	např.
pro	pro	k7c4	pro
kemping	kemping	k1gInSc4	kemping
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Konferenční	konferenční	k2eAgInSc4d1	konferenční
stůl	stůl	k1gInSc4	stůl
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
odkládání	odkládání	k1gNnSc3	odkládání
během	během	k7c2	během
sezení	sezení	k1gNnSc2	sezení
na	na	k7c6	na
pohovce	pohovka	k1gFnSc6	pohovka
<g/>
,	,	kIx,	,
sedací	sedací	k2eAgFnSc1d1	sedací
souprava	souprava	k1gFnSc1	souprava
</s>
</p>
<p>
<s>
Pracovní	pracovní	k2eAgInSc1d1	pracovní
stůl	stůl	k1gInSc1	stůl
řemeslnický	řemeslnický	k2eAgInSc1d1	řemeslnický
<g/>
,	,	kIx,	,
např.	např.	kA	např.
řeznický	řeznický	k2eAgInSc1d1	řeznický
k	k	k7c3	k
porcování	porcování	k1gNnSc3	porcování
masa	maso	k1gNnSc2	maso
<g/>
,	,	kIx,	,
truhlářský	truhlářský	k2eAgMnSc1d1	truhlářský
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
ponk	ponk	k1gInSc1	ponk
</s>
</p>
<p>
<s>
Hráčský	hráčský	k2eAgInSc1d1	hráčský
stůl	stůl	k1gInSc1	stůl
či	či	k8xC	či
stolek	stolek	k1gInSc1	stolek
<g/>
,	,	kIx,	,
např.	např.	kA	např.
šachový	šachový	k2eAgMnSc1d1	šachový
či	či	k8xC	či
karetní	karetní	k2eAgMnSc1d1	karetní
<g/>
,	,	kIx,	,
míval	mívat	k5eAaImAgMnS	mívat
v	v	k7c6	v
rozích	roh	k1gInPc6	roh
desky	deska	k1gFnSc2	deska
prohlubně	prohlubeň	k1gFnSc2	prohlubeň
pro	pro	k7c4	pro
žetony	žeton	k1gInPc4	žeton
nebo	nebo	k8xC	nebo
mince	mince	k1gFnPc4	mince
<g/>
;	;	kIx,	;
má	mít	k5eAaImIp3nS	mít
mít	mít	k5eAaImF	mít
stabilní	stabilní	k2eAgFnPc4d1	stabilní
nohy	noha	k1gFnPc4	noha
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
nemohlo	moct	k5eNaImAgNnS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
posunu	posun	k1gInSc3	posun
předmětů	předmět	k1gInPc2	předmět
</s>
</p>
<p>
<s>
Obětní	obětní	k2eAgInSc1d1	obětní
stůl	stůl	k1gInSc1	stůl
(	(	kIx(	(
<g/>
menza	menza	k1gFnSc1	menza
<g/>
)	)	kIx)	)
-	-	kIx~	-
součást	součást	k1gFnSc1	součást
oltáře	oltář	k1gInSc2	oltář
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
</s>
</p>
<p>
<s>
Dekorační	dekorační	k2eAgInSc4d1	dekorační
stůl	stůl	k1gInSc4	stůl
-	-	kIx~	-
ve	v	k7c6	v
společenských	společenský	k2eAgFnPc6d1	společenská
prostorách	prostora	k1gFnPc6	prostora
<g/>
:	:	kIx,	:
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
konzolový	konzolový	k2eAgMnSc1d1	konzolový
přízední	přízednit	k5eAaPmIp3nS	přízednit
<g/>
,	,	kIx,	,
o	o	k7c6	o
jedné	jeden	k4xCgFnSc6	jeden
či	či	k8xC	či
dvou	dva	k4xCgMnPc6	dva
nohách	noha	k1gFnPc6	noha
nebo	nebo	k8xC	nebo
zavěšený	zavěšený	k2eAgInSc1d1	zavěšený
na	na	k7c6	na
stěně	stěna	k1gFnSc6	stěna
a	a	k8xC	a
sklápěcí	sklápěcí	k2eAgMnSc1d1	sklápěcí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavost	zajímavost	k1gFnSc4	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Tradiční	tradiční	k2eAgInPc1d1	tradiční
stoly	stol	k1gInPc1	stol
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
chabudai	chabuda	k1gFnSc2	chabuda
jsou	být	k5eAaImIp3nP	být
nízké	nízký	k2eAgFnPc1d1	nízká
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
mají	mít	k5eAaImIp3nP	mít
kruhový	kruhový	k2eAgInSc4d1	kruhový
tvar	tvar	k1gInSc4	tvar
a	a	k8xC	a
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
na	na	k7c6	na
konzumaci	konzumace	k1gFnSc6	konzumace
čaje	čaj	k1gInSc2	čaj
a	a	k8xC	a
jídla	jídlo	k1gNnSc2	jídlo
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
stolovník	stolovník	k1gMnSc1	stolovník
u	u	k7c2	u
nich	on	k3xPp3gFnPc2	on
sedí	sedit	k5eAaImIp3nP	sedit
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Wirth	Wirth	k1gMnSc1	Wirth
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Cimburek	Cimburek	k1gMnSc1	Cimburek
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Vladimír	Vladimír	k1gMnSc1	Vladimír
Herain	Herain	k1gMnSc1	Herain
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
nábytkového	nábytkový	k2eAgNnSc2d1	nábytkové
umění	umění	k1gNnSc2	umění
I.	I.	kA	I.
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
.	.	kIx.	.
reprint	reprint	k1gInSc4	reprint
Argo	Argo	k6eAd1	Argo
Praha	Praha	k1gFnSc1	Praha
1995	[number]	k4	1995
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Židle	židle	k1gFnSc1	židle
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
stůl	stůl	k1gInSc4	stůl
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
