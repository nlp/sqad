<s>
Nejdelším	dlouhý	k2eAgInSc7d3	nejdelší
tunelem	tunel	k1gInSc7	tunel
světa	svět	k1gInSc2	svět
je	být	k5eAaImIp3nS	být
železniční	železniční	k2eAgInSc1d1	železniční
Gotthardský	Gotthardský	k2eAgInSc1d1	Gotthardský
úpatní	úpatní	k2eAgInSc1d1	úpatní
tunel	tunel	k1gInSc1	tunel
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
délku	délka	k1gFnSc4	délka
57	[number]	k4	57
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
