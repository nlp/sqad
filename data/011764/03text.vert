<p>
<s>
Tunel	tunel	k1gInSc1	tunel
je	být	k5eAaImIp3nS	být
dopravní	dopravní	k2eAgFnSc1d1	dopravní
stavba	stavba	k1gFnSc1	stavba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vede	vést	k5eAaImIp3nS	vést
pod	pod	k7c7	pod
zemí	zem	k1gFnSc7	zem
skrz	skrz	k7c4	skrz
krajinnou	krajinný	k2eAgFnSc4d1	krajinná
vyvýšeninu	vyvýšenina	k1gFnSc4	vyvýšenina
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
mořem	moře	k1gNnSc7	moře
<g/>
,	,	kIx,	,
říčním	říční	k2eAgInSc7d1	říční
tokem	tok	k1gInSc7	tok
či	či	k8xC	či
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
silniční	silniční	k2eAgFnSc4d1	silniční
<g/>
,	,	kIx,	,
kolejovou	kolejový	k2eAgFnSc4d1	kolejová
nebo	nebo	k8xC	nebo
pěší	pěší	k2eAgFnSc4d1	pěší
dopravu	doprava	k1gFnSc4	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgInPc1d1	vodní
tunely	tunel	k1gInPc1	tunel
vybudované	vybudovaný	k2eAgInPc1d1	vybudovaný
k	k	k7c3	k
přivádění	přivádění	k1gNnSc3	přivádění
nebo	nebo	k8xC	nebo
odvádění	odvádění	k1gNnSc4	odvádění
vody	voda	k1gFnSc2	voda
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
označují	označovat	k5eAaImIp3nP	označovat
též	též	k9	též
jako	jako	k9	jako
štoly	štola	k1gFnSc2	štola
<g/>
.	.	kIx.	.
</s>
<s>
Železniční	železniční	k2eAgInPc1d1	železniční
i	i	k8xC	i
silniční	silniční	k2eAgInPc1d1	silniční
tunely	tunel	k1gInPc1	tunel
i	i	k8xC	i
prostory	prostor	k1gInPc1	prostor
metra	metro	k1gNnSc2	metro
ve	v	k7c6	v
válečných	válečný	k2eAgFnPc6d1	válečná
dobách	doba	k1gFnPc6	doba
mohou	moct	k5eAaImIp3nP	moct
posloužit	posloužit	k5eAaPmF	posloužit
též	též	k9	též
jako	jako	k9	jako
protiletecké	protiletecký	k2eAgInPc4d1	protiletecký
kryty	kryt	k1gInPc4	kryt
pro	pro	k7c4	pro
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
válečnou	válečný	k2eAgFnSc4d1	válečná
výrobu	výroba	k1gFnSc4	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
tunelu	tunel	k1gInSc2	tunel
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
tunelování	tunelování	k1gNnSc1	tunelování
<g/>
.	.	kIx.	.
</s>
<s>
Metaforicky	metaforicky	k6eAd1	metaforicky
přenesený	přenesený	k2eAgInSc4d1	přenesený
význam	význam	k1gInSc4	význam
slova	slovo	k1gNnSc2	slovo
tunelování	tunelování	k1gNnSc2	tunelování
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
označením	označení	k1gNnSc7	označení
pro	pro	k7c4	pro
druh	druh	k1gInSc4	druh
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
podvodu	podvod	k1gInSc2	podvod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejdelším	dlouhý	k2eAgInSc7d3	nejdelší
tunelem	tunel	k1gInSc7	tunel
světa	svět	k1gInSc2	svět
je	být	k5eAaImIp3nS	být
železniční	železniční	k2eAgInSc1d1	železniční
Gotthardský	Gotthardský	k2eAgInSc1d1	Gotthardský
úpatní	úpatní	k2eAgInSc1d1	úpatní
tunel	tunel	k1gInSc1	tunel
ve	v	k7c6	v
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
má	mít	k5eAaImIp3nS	mít
délku	délka	k1gFnSc4	délka
57	[number]	k4	57
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
nejdelším	dlouhý	k2eAgInSc7d3	nejdelší
silničním	silniční	k2eAgInSc7d1	silniční
tunelem	tunel	k1gInSc7	tunel
tunel	tunel	k1gInSc1	tunel
Panenská	panenský	k2eAgFnSc1d1	panenská
(	(	kIx(	(
<g/>
2115	[number]	k4	2115
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
a	a	k8xC	a
z	z	k7c2	z
budovaných	budovaný	k2eAgInPc2d1	budovaný
silničních	silniční	k2eAgInPc2d1	silniční
tunelů	tunel	k1gInPc2	tunel
tunel	tunel	k1gInSc1	tunel
Blanka	Blanka	k1gFnSc1	Blanka
(	(	kIx(	(
<g/>
5502	[number]	k4	5502
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
železničních	železniční	k2eAgInPc2d1	železniční
tunelů	tunel	k1gInPc2	tunel
je	být	k5eAaImIp3nS	být
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
Ejpovický	ejpovický	k2eAgInSc1d1	ejpovický
tunel	tunel	k1gInSc1	tunel
(	(	kIx(	(
<g/>
4150	[number]	k4	4150
metrů	metr	k1gInPc2	metr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zamýšlený	zamýšlený	k2eAgInSc1d1	zamýšlený
Barrandovský	barrandovský	k2eAgInSc1d1	barrandovský
tunel	tunel	k1gInSc1	tunel
na	na	k7c6	na
vysokorychlostní	vysokorychlostní	k2eAgFnSc6d1	vysokorychlostní
trati	trať	k1gFnSc6	trať
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Beroun	Beroun	k1gInSc1	Beroun
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
měřit	měřit	k5eAaImF	měřit
asi	asi	k9	asi
19	[number]	k4	19
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Celkovou	celkový	k2eAgFnSc7d1	celková
délkou	délka	k1gFnSc7	délka
je	být	k5eAaImIp3nS	být
však	však	k9	však
předčí	předčit	k5eAaBmIp3nP	předčit
tunely	tunel	k1gInPc1	tunel
pražského	pražský	k2eAgNnSc2d1	Pražské
metra	metro	k1gNnSc2	metro
nebo	nebo	k8xC	nebo
pražské	pražský	k2eAgFnSc2d1	Pražská
kolektorové	kolektorový	k2eAgFnSc2d1	kolektorová
sítě	síť	k1gFnSc2	síť
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Podrobněji	podrobně	k6eAd2	podrobně
v	v	k7c6	v
článku	článek	k1gInSc2	článek
Tunely	tunel	k1gInPc4	tunel
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
tunelů	tunel	k1gInPc2	tunel
==	==	k?	==
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
tunely	tunel	k1gInPc1	tunel
se	se	k3xPyFc4	se
stavěly	stavět	k5eAaImAgInP	stavět
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
zavlažování	zavlažování	k1gNnSc2	zavlažování
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
tunely	tunel	k1gInPc1	tunel
využívaly	využívat	k5eAaPmAgInP	využívat
ke	k	k7c3	k
spojeni	spojit	k5eAaPmNgMnP	spojit
akvaduktů	akvadukt	k1gInPc2	akvadukt
přivádějících	přivádějící	k2eAgFnPc2d1	přivádějící
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
významný	významný	k2eAgInSc1d1	významný
tunel	tunel	k1gInSc1	tunel
pro	pro	k7c4	pro
přepravu	přeprava	k1gFnSc4	přeprava
nákladů	náklad	k1gInPc2	náklad
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
až	až	k9	až
roku	rok	k1gInSc2	rok
1670	[number]	k4	1670
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
proražen	proražen	k2eAgInSc4d1	proražen
158	[number]	k4	158
metrů	metr	k1gInPc2	metr
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
tunel	tunel	k1gInSc1	tunel
srze	srha	k1gFnSc3	srha
skalnatý	skalnatý	k2eAgInSc1d1	skalnatý
kopec	kopec	k1gInSc1	kopec
blízko	blízko	k7c2	blízko
Béziers	Béziersa	k1gFnPc2	Béziersa
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Stavbu	stavba	k1gFnSc4	stavba
tunelů	tunel	k1gInPc2	tunel
urychlil	urychlit	k5eAaPmAgInS	urychlit
vývoj	vývoj	k1gInSc1	vývoj
železnice	železnice	k1gFnSc2	železnice
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
činnost	činnost	k1gFnSc4	činnost
významně	významně	k6eAd1	významně
usnadnil	usnadnit	k5eAaPmAgInS	usnadnit
Marc	Marc	k1gInSc1	Marc
Brunel	Brunel	k1gInSc1	Brunel
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
roku	rok	k1gInSc2	rok
1818	[number]	k4	1818
vynalezl	vynaleznout	k5eAaPmAgInS	vynaleznout
razicí	razicí	k2eAgInSc1d1	razicí
štít	štít	k1gInSc1	štít
během	během	k7c2	během
ražení	ražení	k1gNnSc2	ražení
tunelu	tunel	k1gInSc2	tunel
pod	pod	k7c7	pod
Temží	Temže	k1gFnSc7	Temže
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vynález	vynález	k1gInSc1	vynález
významně	významně	k6eAd1	významně
ulehčil	ulehčit	k5eAaPmAgInS	ulehčit
stavbu	stavba	k1gFnSc4	stavba
tunelů	tunel	k1gInPc2	tunel
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
železniční	železniční	k2eAgInPc1d1	železniční
tunely	tunel	k1gInPc1	tunel
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
mnohdy	mnohdy	k6eAd1	mnohdy
metodou	metoda	k1gFnSc7	metoda
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yIgFnSc6	který
byl	být	k5eAaImAgInS	být
vyhlouben	vyhlouben	k2eAgInSc4d1	vyhlouben
velký	velký	k2eAgInSc4d1	velký
výkop	výkop	k1gInSc4	výkop
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
překryl	překrýt	k5eAaPmAgMnS	překrýt
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
se	se	k3xPyFc4	se
tunel	tunel	k1gInSc1	tunel
může	moct	k5eAaImIp3nS	moct
stavět	stavět	k5eAaImF	stavět
z	z	k7c2	z
prefabrikovaných	prefabrikovaný	k2eAgFnPc2d1	prefabrikovaná
betonových	betonový	k2eAgFnPc2d1	betonová
sekcí	sekce	k1gFnPc2	sekce
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
upevní	upevnit	k5eAaPmIp3nP	upevnit
a	a	k8xC	a
spojí	spojit	k5eAaPmIp3nP	spojit
<g/>
.	.	kIx.	.
</s>
<s>
Voda	voda	k1gFnSc1	voda
a	a	k8xC	a
kal	kal	k1gInSc1	kal
je	být	k5eAaImIp3nS	být
následně	následně	k6eAd1	následně
odčerpána	odčerpán	k2eAgFnSc1d1	odčerpána
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
však	však	k9	však
více	hodně	k6eAd2	hodně
metod	metoda	k1gFnPc2	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Moderní	moderní	k2eAgInSc1d1	moderní
způsob	způsob	k1gInSc1	způsob
"	"	kIx"	"
<g/>
automatického	automatický	k2eAgNnSc2d1	automatické
<g/>
"	"	kIx"	"
hloubení	hloubení	k1gNnSc2	hloubení
tunelů	tunel	k1gInPc2	tunel
<g/>
,	,	kIx,	,
využívá	využívat	k5eAaImIp3nS	využívat
hydraulicky	hydraulicky	k6eAd1	hydraulicky
poháněnou	poháněný	k2eAgFnSc4d1	poháněná
frézovací	frézovací	k2eAgFnSc4d1	frézovací
hlavu	hlava	k1gFnSc4	hlava
s	s	k7c7	s
řeznými	řezný	k2eAgInPc7d1	řezný
zuby	zub	k1gInPc7	zub
z	z	k7c2	z
karbidu	karbid	k1gInSc2	karbid
wolframu	wolfram	k1gInSc2	wolfram
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nejdelší	dlouhý	k2eAgInPc1d3	nejdelší
tunely	tunel	k1gInPc1	tunel
světa	svět	k1gInSc2	svět
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
všechny	všechen	k3xTgInPc4	všechen
tunely	tunel	k1gInPc4	tunel
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
nejméně	málo	k6eAd3	málo
16	[number]	k4	16
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
Nezahrnuje	zahrnovat	k5eNaImIp3nS	zahrnovat
tunelové	tunelový	k2eAgInPc4d1	tunelový
vodovodní	vodovodní	k2eAgInPc4d1	vodovodní
přivaděče	přivaděč	k1gInPc4	přivaděč
<g/>
,	,	kIx,	,
kolektory	kolektor	k1gInPc4	kolektor
ani	ani	k8xC	ani
tunely	tunel	k1gInPc4	tunel
metra	metro	k1gNnSc2	metro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ve	v	k7c6	v
stavbě	stavba	k1gFnSc6	stavba
===	===	k?	===
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
seznam	seznam	k1gInSc1	seznam
nezahrnuje	zahrnovat	k5eNaImIp3nS	zahrnovat
tunely	tunel	k1gInPc4	tunel
v	v	k7c6	v
projekční	projekční	k2eAgFnSc6d1	projekční
fázi	fáze	k1gFnSc6	fáze
ani	ani	k8xC	ani
ve	v	k7c6	v
fázi	fáze	k1gFnSc6	fáze
ražení	ražení	k1gNnPc2	ražení
průzkumné	průzkumný	k2eAgFnSc2d1	průzkumná
štoly	štola	k1gFnSc2	štola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Tunely	tunel	k1gInPc1	tunel
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
</s>
</p>
<p>
<s>
Kolektor	kolektor	k1gInSc1	kolektor
(	(	kIx(	(
<g/>
stavebnictví	stavebnictví	k1gNnSc1	stavebnictví
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Štola	štola	k1gFnSc1	štola
</s>
</p>
<p>
<s>
Kanalizace	kanalizace	k1gFnSc1	kanalizace
</s>
</p>
<p>
<s>
Tunelovací	Tunelovací	k2eAgFnPc1d1	Tunelovací
metody	metoda	k1gFnPc1	metoda
</s>
</p>
<p>
<s>
Propustek	propustek	k1gInSc1	propustek
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
tunel	tunel	k1gInSc1	tunel
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
tunel	tunel	k1gInSc1	tunel
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Historické	historický	k2eAgInPc1d1	historický
profily	profil	k1gInPc1	profil
tunelů	tunel	k1gInPc2	tunel
</s>
</p>
