<s>
Moaré	moaré	k1gNnSc1	moaré
(	(	kIx(	(
<g/>
z	z	k7c2	z
franc	franc	k1gFnSc1	franc
<g/>
.	.	kIx.	.
moiré	moirý	k2eAgFnPc4d1	moirý
[	[	kIx(	[
<g/>
muaré	muarý	k2eAgFnPc4d1	muarý
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rušivý	rušivý	k2eAgInSc4d1	rušivý
optický	optický	k2eAgInSc4d1	optický
efekt	efekt	k1gInSc4	efekt
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
překrýváním	překrývání	k1gNnSc7	překrývání
nebo	nebo	k8xC	nebo
interferencí	interference	k1gFnSc7	interference
dvou	dva	k4xCgInPc2	dva
pravidelných	pravidelný	k2eAgInPc2d1	pravidelný
a	a	k8xC	a
jen	jen	k9	jen
málo	málo	k4c4	málo
odlišných	odlišný	k2eAgInPc2d1	odlišný
rastrů	rastr	k1gInPc2	rastr
<g/>
.	.	kIx.	.
</s>
