<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
rušivý	rušivý	k2eAgInSc1d1	rušivý
efekt	efekt	k1gInSc1	efekt
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
překrýváním	překrývání	k1gNnSc7	překrývání
dvou	dva	k4xCgFnPc2	dva
pravidelných	pravidelný	k2eAgFnPc2d1	pravidelná
a	a	k8xC	a
jen	jen	k9	jen
málo	málo	k4c4	málo
odlišných	odlišný	k2eAgInPc2d1	odlišný
rastrů	rastr	k1gInPc2	rastr
<g/>
?	?	kIx.	?
</s>
