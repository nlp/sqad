<s>
Moaré	moaré	k1gNnSc1	moaré
(	(	kIx(	(
<g/>
z	z	k7c2	z
franc	franc	k1gFnSc1	franc
<g/>
.	.	kIx.	.
moiré	moirý	k2eAgFnPc4d1	moirý
[	[	kIx(	[
<g/>
muaré	muarý	k2eAgFnPc4d1	muarý
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rušivý	rušivý	k2eAgInSc4d1	rušivý
optický	optický	k2eAgInSc4d1	optický
efekt	efekt	k1gInSc4	efekt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
překrýváním	překrývání	k1gNnSc7	překrývání
nebo	nebo	k8xC	nebo
interferencí	interference	k1gFnSc7	interference
dvou	dva	k4xCgInPc2	dva
pravidelných	pravidelný	k2eAgInPc2d1	pravidelný
a	a	k8xC	a
jen	jen	k9	jen
málo	málo	k4c4	málo
odlišných	odlišný	k2eAgInPc2d1	odlišný
rastrů	rastr	k1gInPc2	rastr
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
problém	problém	k1gInSc4	problém
se	s	k7c7	s
vzorkováním	vzorkování	k1gNnSc7	vzorkování
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
s	s	k7c7	s
interferencí	interference	k1gFnSc7	interference
/	/	kIx~	/
kolizí	kolize	k1gFnPc2	kolize
dvou	dva	k4xCgNnPc2	dva
vzorkování	vzorkování	k1gNnPc2	vzorkování
<g/>
,	,	kIx,	,
typicky	typicky	k6eAd1	typicky
při	při	k7c6	při
převzorkovávání	převzorkovávání	k1gNnSc6	převzorkovávání
signálu	signál	k1gInSc2	signál
již	již	k6eAd1	již
jednou	jednou	k6eAd1	jednou
navzorkovaného	navzorkovaný	k2eAgInSc2d1	navzorkovaný
jedním	jeden	k4xCgInSc7	jeden
kmitočtem	kmitočet	k1gInSc7	kmitočet
následně	následně	k6eAd1	následně
zas	zas	k6eAd1	zas
podle	podle	k7c2	podle
jiného	jiný	k2eAgInSc2d1	jiný
blízkého	blízký	k2eAgInSc2d1	blízký
kmitočtu	kmitočet	k1gInSc2	kmitočet
<g/>
,	,	kIx,	,
opakovaným	opakovaný	k2eAgNnSc7d1	opakované
převzorkováním	převzorkování	k1gNnSc7	převzorkování
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
navzorkoval	navzorkovat	k5eAaPmAgInS	navzorkovat
původní	původní	k2eAgInSc1d1	původní
analogový	analogový	k2eAgInSc1d1	analogový
signál	signál	k1gInSc1	signál
<g/>
.	.	kIx.	.
</s>
<s>
Jistou	jistý	k2eAgFnSc7d1	jistá
obdobou	obdoba	k1gFnSc7	obdoba
moaré	moaré	k1gNnPc2	moaré
ve	v	k7c6	v
zvukové	zvukový	k2eAgFnSc6d1	zvuková
oblasti	oblast	k1gFnSc6	oblast
je	být	k5eAaImIp3nS	být
zázněj	zázněj	k1gFnSc1	zázněj
<g/>
,	,	kIx,	,
skládání	skládání	k1gNnSc2	skládání
dvou	dva	k4xCgInPc2	dva
blízkých	blízký	k2eAgInPc2d1	blízký
kmitočtů	kmitočet	k1gInPc2	kmitočet
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
stroboskopický	stroboskopický	k2eAgInSc1d1	stroboskopický
jev	jev	k1gInSc1	jev
<g/>
.	.	kIx.	.
</s>
<s>
Moaré	moaré	k1gNnSc1	moaré
efekt	efekt	k1gInSc1	efekt
vzniká	vznikat	k5eAaImIp3nS	vznikat
například	například	k6eAd1	například
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
když	když	k8xS	když
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
obrazec	obrazec	k1gInSc1	obrazec
pole	pole	k1gNnSc2	pole
buněk	buňka	k1gFnPc2	buňka
snímače	snímač	k1gInSc2	snímač
fotoaparátu	fotoaparát	k1gInSc2	fotoaparát
nebo	nebo	k8xC	nebo
zobrazovacích	zobrazovací	k2eAgInPc2d1	zobrazovací
bodů	bod	k1gInPc2	bod
obrazovky	obrazovka	k1gFnSc2	obrazovka
nebo	nebo	k8xC	nebo
displeje	displej	k1gInSc2	displej
interferuje	interferovat	k5eAaImIp3nS	interferovat
s	s	k7c7	s
nějakým	nějaký	k3yIgInSc7	nějaký
pravidelným	pravidelný	k2eAgInSc7d1	pravidelný
vzorem	vzor	k1gInSc7	vzor
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
zobrazení	zobrazení	k1gNnSc2	zobrazení
(	(	kIx(	(
<g/>
např.	např.	kA	např.
struktura	struktura	k1gFnSc1	struktura
tkaniny	tkanina	k1gFnSc2	tkanina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Překrývání	překrývání	k1gNnSc1	překrývání
dvou	dva	k4xCgMnPc2	dva
pravidelných	pravidelný	k2eAgMnPc2d1	pravidelný
obrazců	obrazec	k1gInPc2	obrazec
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc4	jenž
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
podobné	podobný	k2eAgInPc4d1	podobný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejsou	být	k5eNaImIp3nP	být
dokonale	dokonale	k6eAd1	dokonale
vyrovnány	vyrovnat	k5eAaBmNgInP	vyrovnat
<g/>
,	,	kIx,	,
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
sady	sada	k1gFnSc2	sada
vzorů	vzor	k1gInPc2	vzor
-	-	kIx~	-
moaré	moaré	k1gNnSc2	moaré
efektu	efekt	k1gInSc2	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
jako	jako	k9	jako
barevné	barevný	k2eAgInPc4d1	barevný
pruhy	pruh	k1gInPc4	pruh
nebo	nebo	k8xC	nebo
kruhy	kruh	k1gInPc4	kruh
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
obrazové	obrazový	k2eAgInPc4d1	obrazový
moiré	moirý	k2eAgInPc4d1	moirý
artefakty	artefakt	k1gInPc4	artefakt
<g/>
.	.	kIx.	.
</s>
<s>
Efekt	efekt	k1gInSc1	efekt
může	moct	k5eAaImIp3nS	moct
např.	např.	kA	např.
pokazit	pokazit	k5eAaPmF	pokazit
kvalitu	kvalita	k1gFnSc4	kvalita
snímků	snímek	k1gInPc2	snímek
zaznamenaných	zaznamenaný	k2eAgInPc2d1	zaznamenaný
digitálním	digitální	k2eAgInSc7d1	digitální
fotoaparátem	fotoaparát	k1gInSc7	fotoaparát
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
televizním	televizní	k2eAgNnSc6d1	televizní
vysílání	vysílání	k1gNnSc6	vysílání
-	-	kIx~	-
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
vystoupení	vystoupení	k1gNnSc4	vystoupení
v	v	k7c6	v
televizi	televize	k1gFnSc6	televize
nevhodné	vhodný	k2eNgInPc4d1	nevhodný
oděvy	oděv	k1gInPc4	oděv
s	s	k7c7	s
drobným	drobný	k2eAgInSc7d1	drobný
vzorkem	vzorek	k1gInSc7	vzorek
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
stejnému	stejný	k2eAgInSc3d1	stejný
efektu	efekt	k1gInSc3	efekt
může	moct	k5eAaImIp3nS	moct
docházet	docházet	k5eAaImF	docházet
také	také	k9	také
při	při	k7c6	při
ofsetovém	ofsetový	k2eAgInSc6d1	ofsetový
tisku	tisk	k1gInSc6	tisk
užitím	užití	k1gNnSc7	užití
např.	např.	kA	např.
barevného	barevný	k2eAgInSc2d1	barevný
modelu	model	k1gInSc2	model
CMYK	CMYK	kA	CMYK
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
každá	každý	k3xTgFnSc1	každý
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
barev	barva	k1gFnPc2	barva
je	být	k5eAaImIp3nS	být
tištěna	tisknout	k5eAaImNgFnS	tisknout
v	v	k7c6	v
rastru	rastr	k1gInSc6	rastr
s	s	k7c7	s
určitým	určitý	k2eAgInSc7d1	určitý
úhlem	úhel	k1gInSc7	úhel
natočení	natočení	k1gNnSc2	natočení
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nevhodném	vhodný	k2eNgNnSc6d1	nevhodné
natočení	natočení	k1gNnSc6	natočení
těchto	tento	k3xDgMnPc2	tento
rastrů	rastr	k1gInPc2	rastr
tak	tak	k9	tak
může	moct	k5eAaImIp3nS	moct
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
jejich	jejich	k3xOp3gFnSc3	jejich
interferenci	interference	k1gFnSc3	interference
<g/>
.	.	kIx.	.
</s>
<s>
Moaré	moaré	k1gNnSc1	moaré
efekt	efekt	k1gInSc1	efekt
vzniká	vznikat	k5eAaImIp3nS	vznikat
také	také	k9	také
na	na	k7c6	na
tkaninách	tkanina	k1gFnPc6	tkanina
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
buď	buď	k8xC	buď
o	o	k7c4	o
úmyslný	úmyslný	k2eAgInSc4d1	úmyslný
efekt	efekt	k1gInSc4	efekt
pravého	pravý	k2eAgNnSc2d1	pravé
Moaré	moaré	k1gNnSc2	moaré
na	na	k7c6	na
stejnojmenné	stejnojmenný	k2eAgFnSc6d1	stejnojmenná
tkanině	tkanina	k1gFnSc6	tkanina
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
o	o	k7c4	o
vadu	vada	k1gFnSc4	vada
<g/>
:	:	kIx,	:
přetržením	přetržení	k1gNnSc7	přetržení
příze	příz	k1gFnSc2	příz
nebo	nebo	k8xC	nebo
periodickou	periodický	k2eAgFnSc7d1	periodická
změnou	změna	k1gFnSc7	změna
tloušťky	tloušťka	k1gFnSc2	tloušťka
útkové	útkový	k2eAgFnSc2d1	útková
příze	příz	k1gFnSc2	příz
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
nejvíce	nejvíce	k6eAd1	nejvíce
viditelné	viditelný	k2eAgInPc1d1	viditelný
u	u	k7c2	u
jemného	jemný	k2eAgNnSc2d1	jemné
jednobarevného	jednobarevný	k2eAgNnSc2d1	jednobarevné
hedvábí	hedvábí	k1gNnSc2	hedvábí
(	(	kIx(	(
<g/>
nejvíce	nejvíce	k6eAd1	nejvíce
u	u	k7c2	u
modrého	modré	k1gNnSc2	modré
nebo	nebo	k8xC	nebo
zeleného	zelené	k1gNnSc2	zelené
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
textiliích	textilie	k1gFnPc6	textilie
jako	jako	k9	jako
dekorační	dekorační	k2eAgFnSc1d1	dekorační
vlastnost	vlastnost	k1gFnSc1	vlastnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oboru	obor	k1gInSc6	obor
počítačového	počítačový	k2eAgNnSc2d1	počítačové
vidění	vidění	k1gNnSc2	vidění
lze	lze	k6eAd1	lze
moiré	moirý	k2eAgNnSc1d1	moiré
<g/>
,	,	kIx,	,
nastavené	nastavený	k2eAgNnSc1d1	nastavené
známým	známý	k2eAgInSc7d1	známý
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
použít	použít	k5eAaPmF	použít
pro	pro	k7c4	pro
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
a	a	k8xC	a
modelování	modelování	k1gNnSc4	modelování
tvaru	tvar	k1gInSc2	tvar
pozorovaného	pozorovaný	k2eAgInSc2d1	pozorovaný
neznámého	známý	k2eNgInSc2d1	neznámý
3	[number]	k4	3
<g/>
D-prostorového	Drostorový	k2eAgInSc2d1	D-prostorový
povrchu	povrch	k1gInSc2	povrch
(	(	kIx(	(
<g/>
při	při	k7c6	při
známé	známý	k2eAgFnSc6d1	známá
míře	míra	k1gFnSc6	míra
nepřesnosti	nepřesnost	k1gFnSc2	nepřesnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Moaré	moaré	k1gNnSc2	moaré
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
Moaré	moaré	k1gNnSc2	moaré
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Moaré	moaré	k1gNnSc4	moaré
a	a	k8xC	a
nastavení	nastavení	k1gNnSc4	nastavení
úhlů	úhel	k1gInPc2	úhel
separací	separace	k1gFnPc2	separace
K	k	k7c3	k
čemu	co	k3yInSc3	co
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
tisku	tisk	k1gInSc6	tisk
rastr	rastr	k1gInSc1	rastr
a	a	k8xC	a
jak	jak	k6eAd1	jak
vzniká	vznikat	k5eAaImIp3nS	vznikat
<g/>
?	?	kIx.	?
</s>
