<s>
Vikariát	vikariát	k1gInSc1
Plzeň-město	Plzeň-města	k1gMnSc5
</s>
<s>
Vikariát	vikariát	k1gInSc1
Plzeň-město	Plzeň-města	k1gMnSc5
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
deseti	deset	k4xCc2
vikariátů	vikariát	k1gInPc2
Diecéze	diecéze	k1gFnSc2
plzeňské	plzeňské	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geograficky	geograficky	k6eAd1
vikariát	vikariát	k1gInSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
oblast	oblast	k1gFnSc4
města	město	k1gNnSc2
Plzně	Plzeň	k1gFnSc2
a	a	k8xC
částečně	částečně	k6eAd1
přesahuje	přesahovat	k5eAaImIp3nS
do	do	k7c2
okresů	okres	k1gInPc2
Plzeň-jih	Plzeň-jiha	k1gFnPc2
a	a	k8xC
Plzeň-sever	Plzeň-sevra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Členění	členění	k1gNnSc1
vikariátu	vikariát	k1gInSc2
</s>
<s>
Vikariát	vikariát	k1gInSc1
Plzeň-město	Plzeň-města	k1gMnSc5
se	se	k3xPyFc4
člení	členit	k5eAaImIp3nS
na	na	k7c4
následujících	následující	k2eAgInPc2d1
sedm	sedm	k4xCc4
farností	farnost	k1gFnPc2
<g/>
:	:	kIx,
</s>
<s>
FarnostSprávceSídloFarní	FarnostSprávceSídloFarní	k2eAgNnSc1d1
kostelFiliální	kostelFiliální	k2eAgNnSc1d1
kostelyDatum	kostelyDatum	k1gNnSc1
zřízení	zřízení	k1gNnSc1
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Plzeň	Plzeň	k1gFnSc1
u	u	k7c2
katedrály	katedrála	k1gFnSc2
svatého	svatý	k1gMnSc2
BartolomějeMons	BartolomějeMons	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Emil	Emil	k1gMnSc1
Soukup	Soukup	k1gMnSc1
<g/>
,	,	kIx,
farářFrantiškánská	farářFrantiškánský	k2eAgFnSc1d1
11	#num#	k4
<g/>
,	,	kIx,
301	#num#	k4
00	#num#	k4
PlzeňKatedrála	PlzeňKatedrál	k1gMnSc2
sv.	sv.	kA
Bartoloměje	Bartoloměj	k1gMnSc2
</s>
<s>
Kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
</s>
<s>
Kaple	kaple	k1gFnSc1
sv.	sv.	kA
Maxmiliána	Maxmilián	k1gMnSc2
Kolbeho	Kolbe	k1gMnSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Plzeň-SlovanyDominikániP	Plzeň-SlovanyDominikániP	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojtěch	Vojtěch	k1gMnSc1
Soudský	Soudský	k2eAgMnSc1d1
OP	op	k1gMnSc1
<g/>
,	,	kIx,
moderátorJiráskovo	moderátorJiráskův	k2eAgNnSc1d1
nám.	nám.	k?
30	#num#	k4
<g/>
/	/	kIx~
<g/>
814	#num#	k4
<g/>
,	,	kIx,
326	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
PlzeňKostel	PlzeňKostel	k1gInSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
Růžencové	růžencový	k2eAgFnSc2d1
<g/>
1	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
1910	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Plzeň-LobzySalesiáni	Plzeň-LobzySalesián	k1gMnPc1
Dona	dona	k1gFnSc1
BoskaP	BoskaP	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojtěch	Vojtěch	k1gMnSc1
Sivek	sivka	k1gFnPc2
SDB	SDB	kA
<g/>
,	,	kIx,
moderátorRevoluční	moderátorRevoluční	k2eAgFnSc1d1
98	#num#	k4
<g/>
,	,	kIx,
312	#num#	k4
0	#num#	k4
<g/>
7	#num#	k4
<g/>
,	,	kIx,
PlzeňKostel	PlzeňKostel	k1gMnSc1
sv.	sv.	kA
Martina	Martin	k1gMnSc4
a	a	k8xC
Prokopa	Prokop	k1gMnSc4
</s>
<s>
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Jiří	Jiří	k1gMnSc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1993	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Plzeň-Severní	Plzeň-Severní	k2eAgFnSc1d1
předměstíŘád	předměstíŘáda	k1gFnPc2
bratří	bratr	k1gMnPc2
františkánůP	františkánůP	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Didak	Didak	k1gMnSc1
Klučka	Klučka	k1gMnSc1
OFM	OFM	kA
<g/>
,	,	kIx,
moderátorKomenského	moderátorKomenský	k2eAgNnSc2d1
17	#num#	k4
<g/>
,	,	kIx,
323	#num#	k4
13	#num#	k4
<g/>
,	,	kIx,
Plzeň	Plzeň	k1gFnSc1
</s>
<s>
Kostel	kostel	k1gInSc1
Všech	všecek	k3xTgFnPc2
svatých	svatá	k1gFnPc2
</s>
<s>
Kostel	kostel	k1gInSc1
v	v	k7c6
Chotíkově	Chotíkův	k2eAgFnSc6d1
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
1996	#num#	k4
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Plzeň-LiticeKoinonia	Plzeň-LiticeKoinonium	k1gNnSc2
Jan	Jan	k1gMnSc1
KřtitelAlvaro	KřtitelAlvara	k1gFnSc5
Grammatica	Grammatic	k2eAgNnPc1d1
<g/>
,	,	kIx,
moderátorBudilovo	moderátorBudilův	k2eAgNnSc1d1
nám.	nám.	k?
2	#num#	k4
<g/>
,	,	kIx,
321	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
Plzeň-LiticeKostel	Plzeň-LiticeKostel	k1gMnSc1
sv.	sv.	kA
Petra	Petr	k1gMnSc4
a	a	k8xC
Pavla	Pavel	k1gMnSc4
</s>
<s>
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Jakuba	Jakub	k1gMnSc2
Staršího	starší	k1gMnSc2
na	na	k7c6
Prusinách	Prusina	k1gFnPc6
</s>
<s>
Kaple	kaple	k1gFnSc1
Panny	Panna	k1gFnSc2
Marie	Marie	k1gFnSc1
Pomocnice	pomocnice	k1gFnSc1
křesťanů	křesťan	k1gMnPc2
<g/>
,	,	kIx,
Chlumčany	Chlumčan	k1gMnPc4
</s>
<s>
Dálniční	dálniční	k2eAgFnSc1d1
Kaple	kaple	k1gFnSc1
Smíření	smíření	k1gNnSc2
</s>
<s>
Kaple	kaple	k1gFnSc1
sv.	sv.	kA
Anny	Anna	k1gFnSc2
<g/>
,	,	kIx,
Lhota	Lhota	k1gFnSc1
u	u	k7c2
Dobřan	Dobřany	k1gInPc2
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
1994	#num#	k4
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Plzeň-BoryP	Plzeň-BoryP	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mgr.	Mgr.	kA
Miroslav	Miroslav	k1gMnSc1
Martiš	Martiš	k1gMnSc1
CM	cm	kA
<g/>
,	,	kIx,
farářKlatovská	farářKlatovský	k2eAgFnSc1d1
1793	#num#	k4
<g/>
/	/	kIx~
<g/>
176	#num#	k4
<g/>
,	,	kIx,
301	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
PlzeňKostel	PlzeňKostel	k1gInSc1
sv.	sv.	kA
Jana	Jan	k1gMnSc2
Nepomuckého	Nepomucký	k1gMnSc2
</s>
<s>
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Prokopa	Prokop	k1gMnSc2
ve	v	k7c6
Štěnovicích	Štěnovice	k1gFnPc6
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Plzeň-ZápadP	Plzeň-ZápadP	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Miroslav	Miroslav	k1gMnSc1
VerčimákKřimická	VerčimákKřimický	k2eAgFnSc1d1
73	#num#	k4
<g/>
,	,	kIx,
PlzeňKostel	PlzeňKostel	k1gInSc1
sv.	sv.	kA
Vojtěcha	Vojtěch	k1gMnSc2
</s>
<s>
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Jiří	Jiří	k1gMnSc2
</s>
<s>
Krypta	krypta	k1gFnSc1
v	v	k7c6
Křimické	Křimický	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2009	#num#	k4
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Město	město	k1gNnSc1
TouškovMons	TouškovMons	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vladimír	Vladimír	k1gMnSc1
Gajdušek	Gajdušek	k1gInSc1
-	-	kIx~
farářKostelní	farářKostelní	k2eAgNnSc1d1
náměstí	náměstí	k1gNnSc1
107	#num#	k4
<g/>
,	,	kIx,
330	#num#	k4
33	#num#	k4
Město	město	k1gNnSc1
TouškovKostel	TouškovKostela	k1gFnPc2
Narození	narození	k1gNnSc2
sv.	sv.	kA
Jana	Jan	k1gMnSc4
Křtitele	křtitel	k1gMnSc4
</s>
<s>
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Štěpána	Štěpán	k1gMnSc2
Kozolupy	Kozolupa	k1gFnSc2
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Dýšina	Dýšina	k1gFnSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Šimona	Šimona	k1gFnSc1
a	a	k8xC
Judy	judo	k1gNnPc7
</s>
<s>
Kostel	kostel	k1gInSc1
Nanebevzetí	nanebevzetí	k1gNnSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
v	v	k7c6
Plané	Planá	k1gFnSc6
nade	nad	k7c7
Mží	Mže	k1gFnSc7
</s>
<s>
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Jiří	Jiří	k1gMnSc2
v	v	k7c6
Kostelci	Kostelec	k1gInSc6
</s>
<s>
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Maří	mařit	k5eAaImIp3nS
Magdaleny	Magdalena	k1gFnPc4
v	v	k7c6
Druztové	Druztová	k1gFnSc6
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Dobřany	Dobřany	k1gInPc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Mikuláše	Mikuláš	k1gMnSc2
</s>
<s>
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Víta	Vít	k1gMnSc2
v	v	k7c6
Dobřanech	Dobřany	k1gInPc6
</s>
<s>
Kostel	kostel	k1gInSc1
Povýšení	povýšení	k1gNnSc2
sv.	sv.	kA
Kříže	kříž	k1gInSc2
v	v	k7c6
Dobřanech	Dobřany	k1gInPc6
</s>
<s>
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Jiří	Jiří	k1gMnSc2
v	v	k7c6
Bukové	bukový	k2eAgFnSc6d1
</s>
<s>
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Petra	Petr	k1gMnSc2
a	a	k8xC
Pavla	Pavel	k1gMnSc2
v	v	k7c6
Dolní	dolní	k2eAgFnSc6d1
Lukavici	Lukavice	k1gFnSc6
</s>
<s>
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Václava	Václav	k1gMnSc2
v	v	k7c6
Dnešici	Dnešice	k1gFnSc6
</s>
<s>
Kostel	kostel	k1gInSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
v	v	k7c6
Chotěšově	Chotěšův	k2eAgFnSc6d1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Na	na	k7c6
základě	základ	k1gInSc6
výnosu	výnos	k1gInSc2
Ministerstva	ministerstvo	k1gNnSc2
kultury	kultura	k1gFnSc2
a	a	k8xC
vyučování	vyučování	k1gNnSc2
ze	z	k7c2
dne	den	k1gInSc2
28	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
1908	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Martinovský	Martinovský	k2eAgInSc1d1
<g/>
,	,	kIx,
s.	s.	k?
174	#num#	k4
<g/>
↑	↑	k?
Macík	Macík	k?
<g/>
,	,	kIx,
s.	s.	k?
251	#num#	k4
2	#num#	k4
Vikariát	vikariát	k1gInSc1
Plzeň-město	Plzeň-města	k1gMnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plzeňská	plzeňský	k2eAgFnSc1d1
diecéze	diecéze	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
DOUŠA	Douša	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
;	;	kIx,
MARTINOVSKÝ	MARTINOVSKÝ	kA
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
<g/>
,	,	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dějiny	dějiny	k1gFnPc4
Plzně	Plzeň	k1gFnSc2
v	v	k7c6
datech	datum	k1gNnPc6
:	:	kIx,
od	od	k7c2
prvních	první	k4xOgNnPc2
stop	stop	k2eAgNnPc2d1
osídlení	osídlení	k1gNnPc2
až	až	k9
po	po	k7c4
současnost	současnost	k1gFnSc4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Lidové	lidový	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
.	.	kIx.
788	#num#	k4
s.	s.	k?
(	(	kIx(
<g/>
Dějiny	dějiny	k1gFnPc1
českých	český	k2eAgNnPc2d1
měst	město	k1gNnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7106	#num#	k4
<g/>
-	-	kIx~
<g/>
723	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
MACÍK	MACÍK	k?
<g/>
,	,	kIx,
Oldřich	Oldřich	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Martina	Martin	k1gMnSc2
a	a	k8xC
Prokopa	Prokop	k1gMnSc2
<g/>
:	:	kIx,
1906	#num#	k4
<g/>
–	–	k?
<g/>
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plzeň	Plzeň	k1gFnSc1
<g/>
:	:	kIx,
Římskokatolická	římskokatolický	k2eAgFnSc1d1
farnost	farnost	k1gFnSc1
Plzeň-Lobzy	Plzeň-Lobza	k1gFnSc2
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
nepřiděleno	přidělen	k2eNgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Vikariáty	vikariát	k1gInPc1
plzeňské	plzeňský	k2eAgFnSc2d1
diecéze	diecéze	k1gFnSc2
</s>
<s>
domažlický	domažlický	k2eAgInSc1d1
•	•	k?
chebský	chebský	k2eAgInSc1d1
•	•	k?
karlovarský	karlovarský	k2eAgInSc1d1
•	•	k?
klatovský	klatovský	k2eAgInSc1d1
•	•	k?
Plzeň-město	Plzeň-města	k1gMnSc5
•	•	k?
Plzeň-jih	Plzeň-jih	k1gInSc1
•	•	k?
Plzeň-sever	Plzeň-sever	k1gInSc1
•	•	k?
rokycanský	rokycanský	k2eAgInSc1d1
•	•	k?
sokolovský	sokolovský	k2eAgInSc1d1
•	•	k?
tachovský	tachovský	k2eAgInSc1d1
</s>
<s>
Farnosti	farnost	k1gFnPc1
vikariátu	vikariát	k1gInSc2
Plzeň-město	Plzeň-města	k1gMnSc5
</s>
<s>
Plzeň	Plzeň	k1gFnSc1
u	u	k7c2
katedrály	katedrála	k1gFnSc2
sv.	sv.	kA
Bartoloměje	Bartoloměj	k1gMnSc2
•	•	k?
Plzeň	Plzeň	k1gFnSc1
–	–	k?
Slovany	Slovan	k1gInPc1
u	u	k7c2
kostela	kostel	k1gInSc2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
Růžencové	růžencový	k2eAgFnSc2d1
•	•	k?
Plzeň-Lobzy	Plzeň-Lobza	k1gFnSc2
•	•	k?
Plzeň-Severní	Plzeň-Severní	k2eAgNnSc4d1
předměstí	předměstí	k1gNnSc4
•	•	k?
Plzeň-Litice	Plzeň-Litice	k1gFnSc2
•	•	k?
Plzeň-Bory	Plzeň-Bora	k1gFnSc2
•	•	k?
Plzeň-Západ	Plzeň-Západ	k1gInSc1
</s>
