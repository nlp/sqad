<p>
<s>
98	[number]	k4	98
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
cyklistického	cyklistický	k2eAgInSc2d1	cyklistický
závodu	závod	k1gInSc2	závod
Milán	Milán	k1gInSc1	Milán
-	-	kIx~	-
San	San	k1gMnSc1	San
Remo	Remo	k1gMnSc1	Remo
(	(	kIx(	(
<g/>
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
24	[number]	k4	24
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Startovalo	startovat	k5eAaBmAgNnS	startovat
se	se	k3xPyFc4	se
v	v	k7c6	v
Miláně	Milán	k1gInSc6	Milán
<g/>
,	,	kIx,	,
cíl	cíl	k1gInSc1	cíl
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
San	San	k1gFnSc6	San
Remu	Remus	k1gInSc2	Remus
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
závodu	závod	k1gInSc2	závod
se	se	k3xPyFc4	se
započítávaly	započítávat	k5eAaImAgFnP	započítávat
do	do	k7c2	do
klasifikace	klasifikace	k1gFnSc2	klasifikace
UCI	UCI	kA	UCI
ProTour	ProTour	k1gMnSc1	ProTour
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
zařazen	zařadit	k5eAaPmNgMnS	zařadit
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
S.	S.	kA	S.
</s>
</p>
<p>
<s>
==	==	k?	==
Propozice	propozice	k1gFnSc2	propozice
==	==	k?	==
</s>
</p>
<p>
<s>
Startovalo	startovat	k5eAaBmAgNnS	startovat
<g/>
:	:	kIx,	:
197	[number]	k4	197
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dokončilo	dokončit	k5eAaPmAgNnS	dokončit
<g/>
:	:	kIx,	:
160	[number]	k4	160
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Km	km	kA	km
<g/>
:	:	kIx,	:
294	[number]	k4	294
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Průměr	průměr	k1gInSc1	průměr
<g/>
:	:	kIx,	:
43,660	[number]	k4	43,660
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc1	průběh
závodu	závod	k1gInSc2	závod
==	==	k?	==
</s>
</p>
<p>
<s>
Startovalo	startovat	k5eAaBmAgNnS	startovat
se	se	k3xPyFc4	se
v	v	k7c6	v
Miláně	Milán	k1gInSc6	Milán
a	a	k8xC	a
začátek	začátek	k1gInSc1	začátek
závodu	závod	k1gInSc2	závod
byl	být	k5eAaImAgInS	být
poměrně	poměrně	k6eAd1	poměrně
ostrý	ostrý	k2eAgInSc1d1	ostrý
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
rychlost	rychlost	k1gFnSc1	rychlost
první	první	k4xOgFnSc2	první
hodiny	hodina	k1gFnSc2	hodina
závodu	závod	k1gInSc2	závod
byla	být	k5eAaImAgFnS	být
46,1	[number]	k4	46,1
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
</s>
</p>
<p>
<s>
Donati	Donat	k1gMnPc1	Donat
a	a	k8xC	a
Aggiano	Aggiana	k1gFnSc5	Aggiana
vyprovokovali	vyprovokovat	k5eAaPmAgMnP	vyprovokovat
únik	únik	k1gInSc4	únik
na	na	k7c4	na
69	[number]	k4	69
<g/>
.	.	kIx.	.
km	km	kA	km
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgNnSc6	který
byli	být	k5eAaImAgMnP	být
také	také	k9	také
Quinziato	Quinziat	k2eAgNnSc4d1	Quinziato
<g/>
,	,	kIx,	,
Usau	Usaus	k1gInSc3	Usaus
<g/>
,	,	kIx,	,
Florencio	Florencia	k1gFnSc5	Florencia
<g/>
,	,	kIx,	,
Duclos-Lassalle	Duclos-Lassalle	k1gFnSc5	Duclos-Lassalle
<g/>
,	,	kIx,	,
Laurent	Laurent	k1gMnSc1	Laurent
<g/>
,	,	kIx,	,
Luengo	Luengo	k1gMnSc1	Luengo
<g/>
,	,	kIx,	,
Bru	Bru	k1gMnSc1	Bru
<g/>
,	,	kIx,	,
Mengin	Mengin	k1gMnSc1	Mengin
<g/>
,	,	kIx,	,
Fothen	Fothen	k2eAgMnSc1d1	Fothen
<g/>
,	,	kIx,	,
Baldato	Baldata	k1gFnSc5	Baldata
<g/>
,	,	kIx,	,
Horrillo	Horrilla	k1gFnSc5	Horrilla
<g/>
,	,	kIx,	,
Bellin	Bellina	k1gFnPc2	Bellina
<g/>
,	,	kIx,	,
Petacchi	Petacchi	k1gNnPc2	Petacchi
a	a	k8xC	a
Knees	Kneesa	k1gFnPc2	Kneesa
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
si	se	k3xPyFc3	se
vypracovala	vypracovat	k5eAaPmAgFnS	vypracovat
15	[number]	k4	15
sekundový	sekundový	k2eAgInSc4d1	sekundový
náskok	náskok	k1gInSc4	náskok
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
74	[number]	k4	74
<g/>
.	.	kIx.	.
km	km	kA	km
dojeta	dojet	k5eAaPmNgFnS	dojet
<g/>
.	.	kIx.	.
</s>
<s>
Závod	závod	k1gInSc1	závod
neustále	neustále	k6eAd1	neustále
nabíral	nabírat	k5eAaImAgInS	nabírat
na	na	k7c6	na
tempu	tempo	k1gNnSc6	tempo
a	a	k8xC	a
po	po	k7c6	po
dvou	dva	k4xCgFnPc6	dva
hodinách	hodina	k1gFnPc6	hodina
činila	činit	k5eAaImAgFnS	činit
průměrná	průměrný	k2eAgFnSc1d1	průměrná
rychlost	rychlost	k1gFnSc1	rychlost
46,2	[number]	k4	46,2
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
86	[number]	k4	86
<g/>
.	.	kIx.	.
km	km	kA	km
se	se	k3xPyFc4	se
pokusili	pokusit	k5eAaPmAgMnP	pokusit
poodjet	poodjet	k5eAaPmF	poodjet
Kunickij	Kunickij	k1gMnPc1	Kunickij
<g/>
,	,	kIx,	,
De	De	k?	De
Kort	Kort	k1gMnSc1	Kort
<g/>
,	,	kIx,	,
Sella	Sella	k1gMnSc1	Sella
<g/>
,	,	kIx,	,
Hernandez	Hernandez	k1gMnSc1	Hernandez
<g/>
,	,	kIx,	,
Traficante	Traficant	k1gMnSc5	Traficant
a	a	k8xC	a
Brutt	Brutt	k1gInSc4	Brutt
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc4	jejich
náskok	náskok	k1gInSc4	náskok
na	na	k7c4	na
peloton	peloton	k1gInSc4	peloton
činil	činit	k5eAaImAgInS	činit
na	na	k7c4	na
108	[number]	k4	108
<g/>
.	.	kIx.	.
km	km	kA	km
již	již	k9	již
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
45	[number]	k4	45
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
na	na	k7c4	na
120	[number]	k4	120
<g/>
.	.	kIx.	.
km	km	kA	km
narostl	narůst	k5eAaPmAgInS	narůst
na	na	k7c4	na
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
51	[number]	k4	51
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
museli	muset	k5eAaImAgMnP	muset
odstoupit	odstoupit	k5eAaPmF	odstoupit
Guesdon	Guesdon	k1gInSc4	Guesdon
a	a	k8xC	a
Gutierrez	Gutierrez	k1gInSc4	Gutierrez
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Passo	Passa	k1gFnSc5	Passa
del	del	k?	del
Turchino	Turchino	k1gNnSc1	Turchino
ztráta	ztráta	k1gFnSc1	ztráta
pelotonu	peloton	k1gInSc6	peloton
na	na	k7c4	na
uprchlíky	uprchlík	k1gMnPc4	uprchlík
činila	činit	k5eAaImAgFnS	činit
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
sjezdu	sjezd	k1gInSc6	sjezd
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
pádu	pád	k1gInSc3	pád
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
<g/>
,	,	kIx,	,
Fertonani	Fertonan	k1gMnPc1	Fertonan
a	a	k8xC	a
Contrini	Contrin	k1gMnPc1	Contrin
odstoupili	odstoupit	k5eAaPmAgMnP	odstoupit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Michailov	Michailov	k1gInSc1	Michailov
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
Arezzanem	Arezzan	k1gInSc7	Arezzan
(	(	kIx(	(
<g/>
162	[number]	k4	162
km	km	kA	km
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
pelotonu	peloton	k1gInSc6	peloton
aktivní	aktivní	k2eAgInSc4d1	aktivní
tým	tým	k1gInSc4	tým
Milram	Milram	k1gInSc1	Milram
a	a	k8xC	a
stáhl	stáhnout	k5eAaPmAgInS	stáhnout
ztrátu	ztráta	k1gFnSc4	ztráta
na	na	k7c4	na
6	[number]	k4	6
<g/>
:	:	kIx,	:
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Před	před	k7c7	před
Celle	Celle	k1gNnSc7	Celle
Ligure	Ligur	k1gMnSc5	Ligur
došlo	dojít	k5eAaPmAgNnS	dojít
znovu	znovu	k6eAd1	znovu
k	k	k7c3	k
pádu	pád	k1gInSc3	pád
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
skupině	skupina	k1gFnSc6	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Uprchlíci	uprchlík	k1gMnPc1	uprchlík
si	se	k3xPyFc3	se
neustále	neustále	k6eAd1	neustále
drželi	držet	k5eAaImAgMnP	držet
stejný	stejný	k2eAgInSc4d1	stejný
odstup	odstup	k1gInSc4	odstup
až	až	k9	až
k	k	k7c3	k
Noli	Noli	k1gNnSc3	Noli
(	(	kIx(	(
<g/>
200	[number]	k4	200
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
začal	začít	k5eAaPmAgInS	začít
peloton	peloton	k1gInSc1	peloton
ztrátu	ztráta	k1gFnSc4	ztráta
dotahovat	dotahovat	k5eAaImF	dotahovat
<g/>
,	,	kIx,	,
během	během	k7c2	během
15	[number]	k4	15
km	km	kA	km
o	o	k7c4	o
dvě	dva	k4xCgFnPc4	dva
minuty	minuta	k1gFnPc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
výjezdu	výjezd	k1gInSc6	výjezd
z	z	k7c2	z
Alassia	Alassius	k1gMnSc2	Alassius
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
hromadnému	hromadný	k2eAgInSc3d1	hromadný
pádu	pád	k1gInSc3	pád
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgNnSc6	který
byli	být	k5eAaImAgMnP	být
Krivcov	Krivcov	k1gInSc4	Krivcov
<g/>
,	,	kIx,	,
Duclos-Lassalle	Duclos-Lassalle	k1gInSc4	Duclos-Lassalle
<g/>
,	,	kIx,	,
Scheirlinckx	Scheirlinckx	k1gInSc4	Scheirlinckx
<g/>
,	,	kIx,	,
Berthou	Bertha	k1gFnSc7	Bertha
<g/>
,	,	kIx,	,
Bělohvoščiks	Bělohvoščiks	k1gInSc1	Bělohvoščiks
<g/>
,	,	kIx,	,
Del	Del	k1gMnSc1	Del
Nero	Nero	k1gMnSc1	Nero
<g/>
,	,	kIx,	,
Merckx	Merckx	k1gInSc1	Merckx
<g/>
,	,	kIx,	,
Erviti	Ervit	k1gMnPc1	Ervit
<g/>
,	,	kIx,	,
Rebellin	Rebellin	k2eAgMnSc1d1	Rebellin
a	a	k8xC	a
Hinault	Hinault	k2eAgMnSc1d1	Hinault
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stoupání	stoupání	k1gNnSc6	stoupání
na	na	k7c6	na
Capo	capa	k1gFnSc5	capa
Mele	mlít	k5eAaImIp3nS	mlít
spadl	spadnout	k5eAaPmAgMnS	spadnout
také	také	k9	také
De	De	k?	De
Kort	Kort	k1gMnSc1	Kort
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
pětice	pětice	k1gFnSc2	pětice
uprchlíků	uprchlík	k1gMnPc2	uprchlík
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
Capo	capa	k1gFnSc5	capa
Cervo	Cerva	k1gFnSc5	Cerva
nestačil	stačit	k5eNaBmAgInS	stačit
tempu	tempo	k1gNnSc6	tempo
Sella	Sello	k1gNnSc2	Sello
a	a	k8xC	a
z	z	k7c2	z
pětice	pětice	k1gFnSc2	pětice
se	se	k3xPyFc4	se
zbyla	zbýt	k5eAaPmAgFnS	zbýt
jen	jen	k9	jen
trojice	trojice	k1gFnSc1	trojice
uprchlíků	uprchlík	k1gMnPc2	uprchlík
<g/>
.	.	kIx.	.
</s>
<s>
Ztráta	ztráta	k1gFnSc1	ztráta
pelotonu	peloton	k1gInSc2	peloton
byla	být	k5eAaImAgFnS	být
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Capo	capa	k1gFnSc5	capa
Cervo	Cervo	k1gNnSc4	Cervo
dojel	dojet	k5eAaPmAgMnS	dojet
peloton	peloton	k1gInSc4	peloton
De	De	k?	De
Korta	Kort	k1gInSc2	Kort
i	i	k9	i
Sellu	Sella	k1gFnSc4	Sella
a	a	k8xC	a
snížil	snížit	k5eAaPmAgInS	snížit
náskok	náskok	k1gInSc4	náskok
uprchlíků	uprchlík	k1gMnPc2	uprchlík
na	na	k7c4	na
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
sjezdu	sjezd	k1gInSc6	sjezd
z	z	k7c2	z
Capo	capa	k1gFnSc5	capa
Cerva	Cervo	k1gNnSc2	Cervo
spadl	spadnout	k5eAaPmAgInS	spadnout
Kopp	Kopp	k1gMnSc1	Kopp
<g/>
,	,	kIx,	,
kterého	který	k3yQgMnSc4	který
odvezla	odvézt	k5eAaPmAgFnS	odvézt
sanitka	sanitka	k1gFnSc1	sanitka
<g/>
,	,	kIx,	,
Haussler	Haussler	k1gInSc1	Haussler
a	a	k8xC	a
Wegmann	Wegmann	k1gInSc1	Wegmann
po	po	k7c6	po
pádu	pád	k1gInSc6	pád
pokračovali	pokračovat	k5eAaImAgMnP	pokračovat
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
<g/>
.	.	kIx.	.
</s>
<s>
Pří	pře	k1gFnSc7	pře
průjezdu	průjezd	k1gInSc2	průjezd
městem	město	k1gNnSc7	město
Imperia	Imperium	k1gNnSc2	Imperium
na	na	k7c6	na
kluzkém	kluzký	k2eAgInSc6d1	kluzký
asfaltu	asfalt	k1gInSc6	asfalt
spadla	spadnout	k5eAaPmAgFnS	spadnout
další	další	k2eAgFnSc1d1	další
skupina	skupina	k1gFnSc1	skupina
cyklistů	cyklista	k1gMnPc2	cyklista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Posledních	poslední	k2eAgInPc2d1	poslední
30	[number]	k4	30
km	km	kA	km
byl	být	k5eAaImAgMnS	být
náskok	náskok	k1gInSc4	náskok
trojice	trojice	k1gFnSc2	trojice
Kunickij	Kunickij	k1gFnSc1	Kunickij
<g/>
,	,	kIx,	,
Hernandez	Hernandez	k1gInSc1	Hernandez
a	a	k8xC	a
Brutt	Brutt	k1gInSc1	Brutt
již	již	k6eAd1	již
jen	jen	k9	jen
38	[number]	k4	38
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Mechanické	mechanický	k2eAgInPc1d1	mechanický
problémy	problém	k1gInPc1	problém
potkaly	potkat	k5eAaPmAgInP	potkat
Bettiniho	Bettiniha	k1gMnSc5	Bettiniha
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stoupání	stoupání	k1gNnSc6	stoupání
na	na	k7c6	na
Cipressu	Cipress	k1gInSc6	Cipress
se	se	k3xPyFc4	se
skupinka	skupinka	k1gFnSc1	skupinka
uprchlíku	uprchlík	k1gMnSc3	uprchlík
roztrhala	roztrhat	k5eAaPmAgFnS	roztrhat
<g/>
,	,	kIx,	,
Hernandez	Hernandez	k1gInSc1	Hernandez
poodjel	poodjet	k5eAaPmAgInS	poodjet
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Kunického	Kunický	k2eAgMnSc2d1	Kunický
a	a	k8xC	a
Bruttiho	Brutti	k1gMnSc2	Brutti
pohltil	pohltit	k5eAaPmAgMnS	pohltit
peloton	peloton	k1gInSc4	peloton
<g/>
.	.	kIx.	.
</s>
<s>
Peloton	peloton	k1gInSc1	peloton
se	se	k3xPyFc4	se
natáhl	natáhnout	k5eAaPmAgInS	natáhnout
do	do	k7c2	do
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
řady	řada	k1gFnSc2	řada
a	a	k8xC	a
dojel	dojet	k5eAaPmAgMnS	dojet
i	i	k9	i
posledního	poslední	k2eAgMnSc2d1	poslední
uprchlíka	uprchlík	k1gMnSc2	uprchlík
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
útok	útok	k1gInSc4	útok
Pellizottiho	Pellizotti	k1gMnSc2	Pellizotti
zareagoval	zareagovat	k5eAaPmAgMnS	zareagovat
Moletta	Moletta	k1gMnSc1	Moletta
a	a	k8xC	a
vzápětí	vzápětí	k6eAd1	vzápětí
i	i	k9	i
Popovič	Popovič	k1gMnSc1	Popovič
<g/>
.	.	kIx.	.
</s>
<s>
Čelo	čelo	k1gNnSc1	čelo
pelotonu	peloton	k1gInSc2	peloton
si	se	k3xPyFc3	se
hlídaly	hlídat	k5eAaImAgFnP	hlídat
stáje	stáj	k1gFnPc1	stáj
Lampre	Lampr	k1gInSc5	Lampr
a	a	k8xC	a
Milram	Milram	k1gInSc4	Milram
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sjezdu	sjezd	k1gInSc6	sjezd
spadl	spadnout	k5eAaPmAgMnS	spadnout
Moletta	Moletta	k1gMnSc1	Moletta
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
rozjetý	rozjetý	k2eAgInSc1d1	rozjetý
závod	závod	k1gInSc1	závod
skončil	skončit	k5eAaPmAgInS	skončit
v	v	k7c6	v
sanitním	sanitní	k2eAgInSc6d1	sanitní
voze	vůz	k1gInSc6	vůz
<g/>
.	.	kIx.	.
</s>
<s>
Pellizotti	Pellizotti	k1gNnSc1	Pellizotti
a	a	k8xC	a
Popovič	Popovič	k1gInSc4	Popovič
získali	získat	k5eAaPmAgMnP	získat
k	k	k7c3	k
dobru	dobro	k1gNnSc3	dobro
8	[number]	k4	8
sekund	sekunda	k1gFnPc2	sekunda
na	na	k7c4	na
další	další	k2eAgFnSc4d1	další
dvojici	dvojice	k1gFnSc4	dvojice
Celestino	Celestin	k2eAgNnSc1d1	Celestino
<g/>
,	,	kIx,	,
Villa	Villa	k1gFnSc1	Villa
a	a	k8xC	a
18	[number]	k4	18
sekund	sekunda	k1gFnPc2	sekunda
na	na	k7c4	na
peloton	peloton	k1gInSc4	peloton
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
cyklisté	cyklista	k1gMnPc1	cyklista
stoupali	stoupat	k5eAaImAgMnP	stoupat
na	na	k7c4	na
Poggio	Poggio	k1gNnSc4	Poggio
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
už	už	k9	už
peloton	peloton	k1gInSc1	peloton
znovu	znovu	k6eAd1	znovu
pohromadě	pohromadě	k6eAd1	pohromadě
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
vrcholem	vrchol	k1gInSc7	vrchol
to	ten	k3xDgNnSc4	ten
znovu	znovu	k6eAd1	znovu
zkusil	zkusit	k5eAaPmAgMnS	zkusit
marně	marně	k6eAd1	marně
Popovič	Popovič	k1gMnSc1	Popovič
<g/>
.	.	kIx.	.
</s>
<s>
Gilbert	Gilbert	k1gMnSc1	Gilbert
<g/>
,	,	kIx,	,
Riccò	Riccò	k1gMnSc1	Riccò
a	a	k8xC	a
Kessler	Kessler	k1gMnSc1	Kessler
byli	být	k5eAaImAgMnP	být
dalšími	další	k2eAgMnPc7d1	další
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
o	o	k7c6	o
unik	unikum	k1gNnPc2	unikum
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
hromadný	hromadný	k2eAgInSc4d1	hromadný
spurt	spurt	k1gInSc4	spurt
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nejlépe	dobře	k6eAd3	dobře
zvládl	zvládnout	k5eAaPmAgInS	zvládnout
Freire	Freir	k1gInSc5	Freir
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klasifikace	klasifikace	k1gFnSc2	klasifikace
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Startovní	startovní	k2eAgFnSc1d1	startovní
listina	listina	k1gFnSc1	listina
==	==	k?	==
</s>
</p>
