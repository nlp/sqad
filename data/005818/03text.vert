<s>
Mandrage	Mandrage	k1gFnSc1	Mandrage
je	být	k5eAaImIp3nS	být
plzeňská	plzeňský	k2eAgFnSc1d1	Plzeňská
popová	popový	k2eAgFnSc1d1	popová
kapela	kapela	k1gFnSc1	kapela
s	s	k7c7	s
vícejazyčně	vícejazyčně	k6eAd1	vícejazyčně
znějícím	znějící	k2eAgInSc7d1	znějící
názvem	název	k1gInSc7	název
(	(	kIx(	(
<g/>
maďarsky	maďarsky	k6eAd1	maďarsky
Mandrage	Mandrage	k1gNnSc1	Mandrage
čteno	číst	k5eAaImNgNnS	číst
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
mandráš	mandrat	k5eAaImIp2nS	mandrat
<g/>
"	"	kIx"	"
viz	vidět	k5eAaImRp2nS	vidět
záznam	záznam	k1gInSc1	záznam
pořadu	pořad	k1gInSc2	pořad
TV	TV	kA	TV
Óčko	Óčko	k1gNnSc4	Óčko
Drive	drive	k1gInSc1	drive
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
zveřejněného	zveřejněný	k2eAgInSc2d1	zveřejněný
na	na	k7c6	na
serveru	server	k1gInSc6	server
Youtube	Youtub	k1gInSc5	Youtub
<g/>
,	,	kIx,	,
japonsky	japonsky	k6eAd1	japonsky
Mandrage	Mandrage	k1gFnPc2	Mandrage
čteno	číst	k5eAaImNgNnS	číst
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
mindžáž	mindžáž	k1gFnSc1	mindžáž
<g/>
"	"	kIx"	"
viz	vidět	k5eAaImRp2nS	vidět
fotografie	fotografia	k1gFnSc2	fotografia
sdílená	sdílený	k2eAgFnSc1d1	sdílená
na	na	k7c6	na
sociální	sociální	k2eAgFnSc6d1	sociální
síti	síť	k1gFnSc6	síť
instagram	instagram	k1gInSc1	instagram
uživatelem	uživatel	k1gMnSc7	uživatel
matyasvorda	matyasvorda	k1gMnSc1	matyasvorda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
proslavila	proslavit	k5eAaPmAgFnS	proslavit
hity	hit	k1gInPc4	hit
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
Šrouby	šroub	k1gInPc1	šroub
a	a	k8xC	a
matice	matice	k1gFnPc1	matice
<g/>
,	,	kIx,	,
Františkovy	Františkův	k2eAgFnPc1d1	Františkova
lázně	lázeň	k1gFnPc1	lázeň
<g/>
,	,	kIx,	,
Hledá	hledat	k5eAaImIp3nS	hledat
se	se	k3xPyFc4	se
žena	žena	k1gFnSc1	žena
a	a	k8xC	a
Na	na	k7c6	na
dlani	dlaň	k1gFnSc6	dlaň
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
5	[number]	k4	5
členů	člen	k1gInPc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
otcové	otec	k1gMnPc1	otec
Víta	Vít	k1gMnSc2	Vít
a	a	k8xC	a
Matyáše	Matyáš	k1gMnSc2	Matyáš
byli	být	k5eAaImAgMnP	být
na	na	k7c6	na
společné	společný	k2eAgFnSc6d1	společná
oslavě	oslava	k1gFnSc6	oslava
skupiny	skupina	k1gFnSc2	skupina
Mediterian	Mediterian	k1gInSc1	Mediterian
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
si	se	k3xPyFc3	se
kluci	kluk	k1gMnPc1	kluk
spolu	spolu	k6eAd1	spolu
mohli	moct	k5eAaImAgMnP	moct
zahrát	zahrát	k5eAaPmF	zahrát
společné	společný	k2eAgFnPc4d1	společná
písně	píseň	k1gFnPc4	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
duo	duo	k1gNnSc1	duo
Víťa	Víťa	k?	Víťa
(	(	kIx(	(
<g/>
zpěv	zpěv	k1gInSc1	zpěv
a	a	k8xC	a
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
a	a	k8xC	a
Mates	mates	k1gInSc1	mates
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgFnSc1d1	bicí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
2002	[number]	k4	2002
a	a	k8xC	a
2003	[number]	k4	2003
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
přistoupil	přistoupit	k5eAaPmAgMnS	přistoupit
jejich	jejich	k3xOp3gMnSc1	jejich
kamarád	kamarád	k1gMnSc1	kamarád
(	(	kIx(	(
<g/>
klávesy	kláves	k1gInPc1	kláves
a	a	k8xC	a
harmonika	harmonika	k1gFnSc1	harmonika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
po	po	k7c6	po
čase	čas	k1gInSc6	čas
však	však	k9	však
odešel	odejít	k5eAaPmAgMnS	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Kluci	kluk	k1gMnPc1	kluk
si	se	k3xPyFc3	se
začali	začít	k5eAaPmAgMnP	začít
skládat	skládat	k5eAaImF	skládat
vlastní	vlastní	k2eAgFnPc4d1	vlastní
písně	píseň	k1gFnPc4	píseň
a	a	k8xC	a
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
se	se	k3xPyFc4	se
bezúspěšně	bezúspěšně	k6eAd1	bezúspěšně
soutěže	soutěž	k1gFnSc2	soutěž
Múza	Múza	k1gFnSc1	Múza
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
akci	akce	k1gFnSc6	akce
potkali	potkat	k5eAaPmAgMnP	potkat
Pepu	Pepa	k1gFnSc4	Pepa
Bolana	Bolan	k1gMnSc2	Bolan
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
Velvet	Velveta	k1gFnPc2	Velveta
Ecstasy	Ecstasa	k1gFnSc2	Ecstasa
<g/>
,	,	kIx,	,
a	a	k8xC	a
Coopiho	Coopi	k1gMnSc4	Coopi
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
hrál	hrát	k5eAaImAgMnS	hrát
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
Pornocowboys	Pornocowboysa	k1gFnPc2	Pornocowboysa
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
potom	potom	k6eAd1	potom
odehráli	odehrát	k5eAaPmAgMnP	odehrát
několik	několik	k4yIc4	několik
koncertů	koncert	k1gInPc2	koncert
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
se	se	k3xPyFc4	se
do	do	k7c2	do
kapely	kapela	k1gFnSc2	kapela
Mandrage	Mandrag	k1gFnSc2	Mandrag
(	(	kIx(	(
<g/>
Víťa	Víťa	k?	Víťa
(	(	kIx(	(
<g/>
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mates	Mates	k1gMnSc1	Mates
(	(	kIx(	(
<g/>
bicí	bicí	k2eAgMnSc1d1	bicí
<g/>
)	)	kIx)	)
a	a	k8xC	a
Coopi	Coopi	k1gNnSc1	Coopi
(	(	kIx(	(
<g/>
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
))	))	k?	))
přidali	přidat	k5eAaPmAgMnP	přidat
i	i	k9	i
František	František	k1gMnSc1	František
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
Bolan	Bolan	k1gMnSc1	Bolan
(	(	kIx(	(
<g/>
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
kytara	kytara	k1gFnSc1	kytara
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
demo	demo	k2eAgMnPc2d1	demo
Říkala	říkat	k5eAaImAgFnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jí	on	k3xPp3gFnSc3	on
trápí	trápit	k5eAaImIp3nS	trápit
cosi	cosi	k3yInSc4	cosi
<g/>
,	,	kIx,	,
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
Mandrage	Mandrage	k1gInSc4	Mandrage
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Universal	Universal	k1gFnSc7	Universal
Music	Musice	k1gFnPc2	Musice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
skupina	skupina	k1gFnSc1	skupina
vydala	vydat	k5eAaPmAgFnS	vydat
dlouhohrající	dlouhohrající	k2eAgFnSc1d1	dlouhohrající
CD	CD	kA	CD
s	s	k7c7	s
názvem	název	k1gInSc7	název
Přišli	přijít	k5eAaPmAgMnP	přijít
jsme	být	k5eAaImIp1nP	být
si	se	k3xPyFc3	se
pro	pro	k7c4	pro
vaše	váš	k3xOp2gFnPc4	váš
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
byla	být	k5eAaImAgFnS	být
nominována	nominovat	k5eAaBmNgFnS	nominovat
na	na	k7c4	na
"	"	kIx"	"
<g/>
Objev	objev	k1gInSc4	objev
roku	rok	k1gInSc2	rok
<g/>
"	"	kIx"	"
v	v	k7c6	v
cenách	cena	k1gFnPc6	cena
Anděl	Anděla	k1gFnPc2	Anděla
a	a	k8xC	a
cenách	cena	k1gFnPc6	cena
hudební	hudební	k2eAgFnSc2d1	hudební
televize	televize	k1gFnSc2	televize
Óčko	Óčko	k1gNnSc1	Óčko
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
také	také	k9	také
jako	jako	k9	jako
předskokan	předskokan	k1gMnSc1	předskokan
skupiny	skupina	k1gFnSc2	skupina
PEHA	PEHA	kA	PEHA
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
skupina	skupina	k1gFnSc1	skupina
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
skupiny	skupina	k1gFnSc2	skupina
Wanastowi	Wanastow	k1gFnSc2	Wanastow
Vjecy	Vjeca	k1gFnSc2	Vjeca
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
skupina	skupina	k1gFnSc1	skupina
vydala	vydat	k5eAaPmAgFnS	vydat
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
druhé	druhý	k4xOgFnSc2	druhý
CD	CD	kA	CD
s	s	k7c7	s
názvem	název	k1gInSc7	název
Hledá	hledat	k5eAaImIp3nS	hledat
se	se	k3xPyFc4	se
žena	žena	k1gFnSc1	žena
pojmenované	pojmenovaný	k2eAgFnSc2d1	pojmenovaná
podle	podle	k7c2	podle
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
hitu	hit	k1gInSc2	hit
<g/>
.	.	kIx.	.
</s>
<s>
Stali	stát	k5eAaPmAgMnP	stát
se	se	k3xPyFc4	se
vítězi	vítěz	k1gMnPc7	vítěz
Óčko	Óčko	k6eAd1	Óčko
hudebních	hudební	k2eAgFnPc2d1	hudební
cen	cena	k1gFnPc2	cena
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
rock	rock	k1gInSc4	rock
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
singl	singl	k1gInSc1	singl
Hledá	hledat	k5eAaImIp3nS	hledat
se	se	k3xPyFc4	se
žena	žena	k1gFnSc1	žena
obsadil	obsadit	k5eAaPmAgInS	obsadit
první	první	k4xOgInSc4	první
místa	místo	k1gNnPc4	místo
hitparád	hitparáda	k1gFnPc2	hitparáda
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c4	na
první	první	k4xOgNnSc4	první
větší	veliký	k2eAgNnSc4d2	veliký
turné	turné	k1gNnSc4	turné
pojmenované	pojmenovaný	k2eAgFnSc2d1	pojmenovaná
Hledá	hledat	k5eAaImIp3nS	hledat
se	se	k3xPyFc4	se
žena	žena	k1gFnSc1	žena
Tour	Toura	k1gFnPc2	Toura
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
vystupovala	vystupovat	k5eAaImAgFnS	vystupovat
na	na	k7c6	na
koncertě	koncert	k1gInSc6	koncert
skupiny	skupina	k1gFnSc2	skupina
Divokej	Divokej	k?	Divokej
Bill	Bill	k1gMnSc1	Bill
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejvíce	nejvíce	k6eAd1	nejvíce
koncertujících	koncertující	k2eAgFnPc2d1	koncertující
skupin	skupina	k1gFnPc2	skupina
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
Hledá	hledat	k5eAaImIp3nS	hledat
se	se	k3xPyFc4	se
žena	žena	k1gFnSc1	žena
byla	být	k5eAaImAgFnS	být
nominována	nominovat	k5eAaBmNgFnS	nominovat
na	na	k7c6	na
cenách	cena	k1gFnPc6	cena
Anděl	Anděla	k1gFnPc2	Anděla
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
vítězem	vítěz	k1gMnSc7	vítěz
v	v	k7c6	v
hudebních	hudební	k2eAgFnPc6d1	hudební
cenách	cena	k1gFnPc6	cena
TV	TV	kA	TV
Óčko	Óčko	k1gNnSc4	Óčko
a	a	k8xC	a
vítězem	vítěz	k1gMnSc7	vítěz
v	v	k7c4	v
kategorii	kategorie	k1gFnSc4	kategorie
objev	objev	k1gInSc4	objev
roku	rok	k1gInSc2	rok
slovenské	slovenský	k2eAgFnSc2d1	slovenská
Musiq	Musiq	k1gFnSc2	Musiq
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
Mandrage	Mandrag	k1gInSc2	Mandrag
"	"	kIx"	"
<g/>
Hledá	hledat	k5eAaImIp3nS	hledat
se	se	k3xPyFc4	se
žena	žena	k1gFnSc1	žena
Tour	Toura	k1gFnPc2	Toura
<g/>
"	"	kIx"	"
zahrnovalo	zahrnovat	k5eAaImAgNnS	zahrnovat
projekt	projekt	k1gInSc4	projekt
hledání	hledání	k1gNnSc2	hledání
ženy	žena	k1gFnSc2	žena
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yQgNnSc2	který
se	se	k3xPyFc4	se
zapojilo	zapojit	k5eAaPmAgNnS	zapojit
přes	přes	k7c4	přes
250	[number]	k4	250
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
dívek	dívka	k1gFnPc2	dívka
(	(	kIx(	(
<g/>
na	na	k7c6	na
webových	webový	k2eAgFnPc6d1	webová
stránkách	stránka	k1gFnPc6	stránka
pro	pro	k7c4	pro
ně	on	k3xPp3gInPc4	on
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
000	[number]	k4	000
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výherkyně	výherkyně	k1gFnSc1	výherkyně
potom	potom	k6eAd1	potom
strávila	strávit	k5eAaPmAgFnS	strávit
víkend	víkend	k1gInSc4	víkend
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
skupina	skupina	k1gFnSc1	skupina
oslavila	oslavit	k5eAaPmAgFnS	oslavit
10	[number]	k4	10
let	léto	k1gNnPc2	léto
fungování	fungování	k1gNnSc2	fungování
<g/>
,	,	kIx,	,
s	s	k7c7	s
výročím	výročí	k1gNnSc7	výročí
bylo	být	k5eAaImAgNnS	být
spojeno	spojen	k2eAgNnSc1d1	spojeno
turné	turné	k1gNnSc4	turné
Tour	Toura	k1gFnPc2	Toura
10	[number]	k4	10
let	léto	k1gNnPc2	léto
Mandrage	Mandrage	k1gFnPc2	Mandrage
po	po	k7c6	po
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
SR	SR	kA	SR
<g/>
,	,	kIx,	,
VB	VB	kA	VB
a	a	k8xC	a
SRN	SRN	kA	SRN
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
vydala	vydat	k5eAaPmAgFnS	vydat
nové	nový	k2eAgNnSc4d1	nové
album	album	k1gNnSc4	album
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
Moje	můj	k3xOp1gFnSc1	můj
krevní	krevní	k2eAgFnSc1d1	krevní
skupina	skupina	k1gFnSc1	skupina
a	a	k8xC	a
do	do	k7c2	do
rádií	rádio	k1gNnPc2	rádio
pustila	pustit	k5eAaPmAgFnS	pustit
jako	jako	k9	jako
první	první	k4xOgFnSc1	první
z	z	k7c2	z
alba	album	k1gNnSc2	album
singl	singl	k1gInSc1	singl
Františkovy	Františkův	k2eAgFnSc2d1	Františkova
lázně	lázeň	k1gFnSc2	lázeň
<g/>
.	.	kIx.	.
</s>
<s>
Singl	singl	k1gInSc1	singl
byl	být	k5eAaImAgInS	být
pojatý	pojatý	k2eAgInSc1d1	pojatý
jinak	jinak	k6eAd1	jinak
než	než	k8xS	než
předchozí	předchozí	k2eAgFnPc4d1	předchozí
písně	píseň	k1gFnPc4	píseň
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
přidáno	přidat	k5eAaPmNgNnS	přidat
více	hodně	k6eAd2	hodně
electra	electrum	k1gNnSc2	electrum
a	a	k8xC	a
zvukových	zvukový	k2eAgInPc2d1	zvukový
efektů	efekt	k1gInPc2	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
vydala	vydat	k5eAaPmAgFnS	vydat
i	i	k9	i
Šrouby	šroub	k1gInPc4	šroub
a	a	k8xC	a
matice	matice	k1gFnPc4	matice
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
album	album	k1gNnSc1	album
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
27	[number]	k4	27
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
opět	opět	k6eAd1	opět
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
cenu	cena	k1gFnSc4	cena
TV	TV	kA	TV
Óčko	Óčko	k6eAd1	Óčko
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
rock	rock	k1gInSc4	rock
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
měla	mít	k5eAaImAgFnS	mít
píseň	píseň	k1gFnSc4	píseň
Šrouby	šroub	k1gInPc1	šroub
a	a	k8xC	a
matice	matice	k1gFnSc1	matice
úspěch	úspěch	k1gInSc1	úspěch
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
hitparádách	hitparáda	k1gFnPc6	hitparáda
moderní	moderní	k2eAgFnSc2d1	moderní
hudby	hudba	k1gFnSc2	hudba
v	v	k7c6	v
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
nejhranější	hraný	k2eAgFnSc7d3	nejhranější
skladbou	skladba	k1gFnSc7	skladba
v	v	k7c6	v
rádiích	rádio	k1gNnPc6	rádio
tohoto	tento	k3xDgInSc2	tento
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
tour	tour	k1gInSc1	tour
Moje	můj	k3xOp1gFnSc1	můj
krevní	krevní	k2eAgFnSc1d1	krevní
skupina	skupina	k1gFnSc1	skupina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
cenách	cena	k1gFnPc6	cena
Anděl	Anděla	k1gFnPc2	Anděla
skupina	skupina	k1gFnSc1	skupina
obsadila	obsadit	k5eAaPmAgFnS	obsadit
s	s	k7c7	s
písní	píseň	k1gFnSc7	píseň
Šrouby	šroub	k1gInPc1	šroub
a	a	k8xC	a
matice	matice	k1gFnPc1	matice
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
"	"	kIx"	"
<g/>
Skladba	skladba	k1gFnSc1	skladba
roku	rok	k1gInSc2	rok
<g/>
"	"	kIx"	"
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
"	"	kIx"	"
<g/>
Skupina	skupina	k1gFnSc1	skupina
roku	rok	k1gInSc2	rok
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Žebřík	žebřík	k1gInSc4	žebřík
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
songem	song	k1gInSc7	song
obsadila	obsadit	k5eAaPmAgFnS	obsadit
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
"	"	kIx"	"
<g/>
Skladba	skladba	k1gFnSc1	skladba
roku	rok	k1gInSc2	rok
<g/>
"	"	kIx"	"
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
"	"	kIx"	"
<g/>
Skupina	skupina	k1gFnSc1	skupina
roku	rok	k1gInSc2	rok
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
Český	český	k2eAgMnSc1d1	český
Slavík	Slavík	k1gMnSc1	Slavík
Mattoni	Mattoň	k1gFnSc6	Mattoň
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
jako	jako	k8xC	jako
skupina	skupina	k1gFnSc1	skupina
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
fanoušci	fanoušek	k1gMnPc1	fanoušek
nominovali	nominovat	k5eAaBmAgMnP	nominovat
skupinu	skupina	k1gFnSc4	skupina
Mandrage	Mandrage	k1gNnSc2	Mandrage
v	v	k7c6	v
hudebních	hudební	k2eAgFnPc6d1	hudební
cenách	cena	k1gFnPc6	cena
Óčka	Óčkum	k1gNnSc2	Óčkum
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
"	"	kIx"	"
<g/>
Skupina	skupina	k1gFnSc1	skupina
roku	rok	k1gInSc2	rok
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
skupina	skupina	k1gFnSc1	skupina
skončila	skončit	k5eAaPmAgFnS	skončit
na	na	k7c6	na
druhém	druhý	k4xOgInSc6	druhý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
odehrála	odehrát	k5eAaPmAgFnS	odehrát
skupina	skupina	k1gFnSc1	skupina
mnoho	mnoho	k6eAd1	mnoho
koncertů	koncert	k1gInPc2	koncert
a	a	k8xC	a
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
se	se	k3xPyFc4	se
velkých	velký	k2eAgInPc2d1	velký
festivalů	festival	k1gInPc2	festival
<g/>
.	.	kIx.	.
</s>
<s>
Skončila	skončit	k5eAaPmAgFnS	skončit
3	[number]	k4	3
<g/>
.	.	kIx.	.
v	v	k7c6	v
hudebních	hudební	k2eAgFnPc6d1	hudební
cenách	cena	k1gFnPc6	cena
Óčka	Óčkum	k1gNnSc2	Óčkum
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
"	"	kIx"	"
<g/>
Skupina	skupina	k1gFnSc1	skupina
roku	rok	k1gInSc2	rok
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
8	[number]	k4	8
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
vydala	vydat	k5eAaPmAgFnS	vydat
album	album	k1gNnSc4	album
"	"	kIx"	"
<g/>
Siluety	silueta	k1gFnSc2	silueta
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Hudební	hudební	k2eAgMnPc1d1	hudební
kritici	kritik	k1gMnPc1	kritik
jej	on	k3xPp3gMnSc4	on
přijali	přijmout	k5eAaPmAgMnP	přijmout
pozitivně	pozitivně	k6eAd1	pozitivně
a	a	k8xC	a
hodnocení	hodnocení	k1gNnSc1	hodnocení
bylo	být	k5eAaImAgNnS	být
převážně	převážně	k6eAd1	převážně
kladné	kladný	k2eAgNnSc1d1	kladné
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
pro	pro	k7c4	pro
Kapku	kapka	k1gFnSc4	kapka
naděje	naděje	k1gFnSc2	naděje
v	v	k7c6	v
O2	O2	k1gFnSc6	O2
aréně	aréna	k1gFnSc6	aréna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
na	na	k7c6	na
Tříkrálové	tříkrálový	k2eAgFnSc6d1	Tříkrálová
sbírce	sbírka	k1gFnSc6	sbírka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1	[number]	k4	1
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
odstartovalo	odstartovat	k5eAaPmAgNnS	odstartovat
jejich	jejich	k3xOp3gNnSc4	jejich
doposud	doposud	k6eAd1	doposud
největší	veliký	k2eAgNnSc4d3	veliký
turné	turné	k1gNnSc4	turné
Siluety	silueta	k1gFnSc2	silueta
tour	toura	k1gFnPc2	toura
<g/>
.	.	kIx.	.
</s>
<s>
Odehráli	odehrát	k5eAaPmAgMnP	odehrát
celkem	celkem	k6eAd1	celkem
14	[number]	k4	14
koncertů	koncert	k1gInPc2	koncert
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
hudebních	hudební	k2eAgFnPc6d1	hudební
cenách	cena	k1gFnPc6	cena
Žebřík	žebřík	k1gInSc4	žebřík
vyhráli	vyhrát	k5eAaPmAgMnP	vyhrát
1	[number]	k4	1
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
skupina	skupina	k1gFnSc1	skupina
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
skladba	skladba	k1gFnSc1	skladba
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
bodovala	bodovat	k5eAaImAgFnS	bodovat
píseň	píseň	k1gFnSc1	píseň
Siluety	silueta	k1gFnSc2	silueta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
vydali	vydat	k5eAaPmAgMnP	vydat
Mandrage	Mandrage	k1gFnSc4	Mandrage
zatím	zatím	k6eAd1	zatím
svou	svůj	k3xOyFgFnSc4	svůj
poslední	poslední	k2eAgFnSc4d1	poslední
řadovou	řadový	k2eAgFnSc4d1	řadová
desku	deska	k1gFnSc4	deska
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
Potmě	potmě	k6eAd1	potmě
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgFnPc4	všechen
kočky	kočka	k1gFnPc4	kočka
černý	černý	k2eAgMnSc1d1	černý
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
velmi	velmi	k6eAd1	velmi
taneční	taneční	k2eAgNnSc4d1	taneční
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
iReport	iReport	k1gInSc4	iReport
uvedli	uvést	k5eAaPmAgMnP	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
spíše	spíše	k9	spíše
zasněných	zasněný	k2eAgFnPc6d1	zasněná
melodiích	melodie	k1gFnPc6	melodie
(	(	kIx(	(
<g/>
Siluety	silueta	k1gFnSc2	silueta
<g/>
)	)	kIx)	)
a	a	k8xC	a
akustickém	akustický	k2eAgNnSc6d1	akustické
divadelním	divadelní	k2eAgNnSc6d1	divadelní
turné	turné	k1gNnSc6	turné
chtěli	chtít	k5eAaImAgMnP	chtít
vnést	vnést	k5eAaPmF	vnést
trochu	trocha	k1gFnSc4	trocha
života	život	k1gInSc2	život
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
produkce	produkce	k1gFnSc2	produkce
<g/>
.	.	kIx.	.
</s>
<s>
Vít	Vít	k1gMnSc1	Vít
Starý	Starý	k1gMnSc1	Starý
(	(	kIx(	(
<g/>
Víťa	Víťa	k?	Víťa
<g/>
;	;	kIx,	;
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
texty	text	k1gInPc1	text
a	a	k8xC	a
kytara	kytara	k1gFnSc1	kytara
<g/>
;	;	kIx,	;
frontman	frontman	k1gMnSc1	frontman
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
*	*	kIx~	*
23	[number]	k4	23
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1989	[number]	k4	1989
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc4d1	základní
školu	škola	k1gFnSc4	škola
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
na	na	k7c6	na
Skvrňanech	Skvrňan	k1gMnPc6	Skvrňan
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
Střední	střední	k2eAgFnSc1d1	střední
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
škola	škola	k1gFnSc1	škola
dopravní	dopravní	k2eAgFnSc1d1	dopravní
a	a	k8xC	a
soukromá	soukromý	k2eAgFnSc1d1	soukromá
Sportovní	sportovní	k2eAgFnSc1d1	sportovní
a	a	k8xC	a
podnikatelská	podnikatelský	k2eAgFnSc1d1	podnikatelská
střední	střední	k2eAgFnSc1d1	střední
škola	škola	k1gFnSc1	škola
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
hudbou	hudba	k1gFnSc7	hudba
začínal	začínat	k5eAaImAgMnS	začínat
již	již	k6eAd1	již
jako	jako	k8xC	jako
malý	malý	k2eAgMnSc1d1	malý
kluk	kluk	k1gMnSc1	kluk
<g/>
,	,	kIx,	,
na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
začal	začít	k5eAaPmAgMnS	začít
hrát	hrát	k5eAaImF	hrát
asi	asi	k9	asi
v	v	k7c6	v
8	[number]	k4	8
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
pod	pod	k7c7	pod
dohledem	dohled	k1gInSc7	dohled
otce	otec	k1gMnSc2	otec
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
po	po	k7c6	po
roce	rok	k1gInSc6	rok
začal	začít	k5eAaPmAgInS	začít
chodit	chodit	k5eAaImF	chodit
k	k	k7c3	k
Jirkovi	Jirka	k1gMnSc3	Jirka
Sadílkovi	Sadílek	k1gMnSc3	Sadílek
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterého	který	k3yIgMnSc4	který
strávil	strávit	k5eAaPmAgMnS	strávit
2	[number]	k4	2
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
14	[number]	k4	14
letech	léto	k1gNnPc6	léto
chodil	chodit	k5eAaImAgMnS	chodit
ke	k	k7c3	k
kytaristovi	kytarista	k1gMnSc3	kytarista
Honzovi	Honz	k1gMnSc3	Honz
Militkýmu	Militkým	k1gMnSc3	Militkým
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
byl	být	k5eAaImAgInS	být
jen	jen	k9	jen
na	na	k7c6	na
pár	pár	k4xCyI	pár
hodinách	hodina	k1gFnPc6	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
samoukem	samouk	k1gMnSc7	samouk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
založil	založit	k5eAaPmAgInS	založit
společně	společně	k6eAd1	společně
s	s	k7c7	s
Matyášem	Matyáš	k1gMnSc7	Matyáš
Vordou	Vorda	k1gFnSc7	Vorda
kapelu	kapela	k1gFnSc4	kapela
Mandrage	Mandrag	k1gFnSc2	Mandrag
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Miloslav	Miloslav	k1gMnSc1	Miloslav
Starý	Starý	k1gMnSc1	Starý
hraje	hrát	k5eAaImIp3nS	hrát
společně	společně	k6eAd1	společně
s	s	k7c7	s
Matyášovým	Matyášův	k2eAgMnSc7d1	Matyášův
otcem	otec	k1gMnSc7	otec
Vlastimilem	Vlastimil	k1gMnSc7	Vlastimil
Vordou	Vorda	k1gMnSc7	Vorda
v	v	k7c6	v
plzeňské	plzeňský	k2eAgFnSc6d1	Plzeňská
kapele	kapela	k1gFnSc6	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
také	také	k9	také
složil	složit	k5eAaPmAgMnS	složit
několik	několik	k4yIc4	několik
písniček	písnička	k1gFnPc2	písnička
pro	pro	k7c4	pro
Mandrage	Mandrage	k1gFnSc4	Mandrage
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Bolan	Bolan	k1gMnSc1	Bolan
(	(	kIx(	(
<g/>
Pepa	Pepa	k1gMnSc1	Pepa
<g/>
;	;	kIx,	;
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
texty	text	k1gInPc1	text
a	a	k8xC	a
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
*	*	kIx~	*
1976	[number]	k4	1976
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slova	slovo	k1gNnPc1	slovo
Pepy	Pepa	k1gFnSc2	Pepa
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Na	na	k7c4	na
kytaru	kytara	k1gFnSc4	kytara
jsem	být	k5eAaImIp1nS	být
začal	začít	k5eAaPmAgMnS	začít
hrát	hrát	k5eAaImF	hrát
až	až	k9	až
někdy	někdy	k6eAd1	někdy
na	na	k7c6	na
střední	střední	k2eAgFnSc6d1	střední
škole	škola	k1gFnSc6	škola
a	a	k8xC	a
stašně	stašně	k6eAd1	stašně
mi	já	k3xPp1nSc3	já
nebavilo	bavit	k5eNaImAgNnS	bavit
se	se	k3xPyFc4	se
učit	učit	k5eAaImF	učit
teorii	teorie	k1gFnSc4	teorie
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
dodnes	dodnes	k6eAd1	dodnes
nevim	nevim	k?	nevim
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
vlastně	vlastně	k9	vlastně
hraju	hrát	k5eAaImIp1nS	hrát
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
žádnému	žádný	k3yNgMnSc3	žádný
učiteli	učitel	k1gMnSc3	učitel
jsem	být	k5eAaImIp1nS	být
taky	taky	k6eAd1	taky
nikdy	nikdy	k6eAd1	nikdy
nechodil	chodit	k5eNaImAgMnS	chodit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
mě	já	k3xPp1nSc4	já
nutili	nutit	k5eAaImAgMnP	nutit
se	se	k3xPyFc4	se
učit	učit	k5eAaImF	učit
noty	nota	k1gFnPc1	nota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
jsem	být	k5eAaImIp1nS	být
založil	založit	k5eAaPmAgInS	založit
kapelu	kapela	k1gFnSc4	kapela
VELVET	VELVET	kA	VELVET
ECSTASY	ECSTASY	kA	ECSTASY
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
jsme	být	k5eAaImIp1nP	být
nahráli	nahrát	k5eAaPmAgMnP	nahrát
4	[number]	k4	4
demáče	demáč	k1gInSc2	demáč
a	a	k8xC	a
hráli	hrát	k5eAaImAgMnP	hrát
i	i	k9	i
na	na	k7c6	na
velkých	velký	k2eAgInPc6d1	velký
festivalech	festival	k1gInPc6	festival
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
mě	já	k3xPp1nSc4	já
začali	začít	k5eAaPmAgMnP	začít
všichni	všechen	k3xTgMnPc1	všechen
srát	srát	k5eAaImF	srát
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
jsem	být	k5eAaImIp1nS	být
začal	začít	k5eAaPmAgMnS	začít
hrát	hrát	k5eAaImF	hrát
s	s	k7c7	s
klukama	klukama	k?	klukama
z	z	k7c2	z
MANDRAGE	MANDRAGE	kA	MANDRAGE
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Michal	Michal	k1gMnSc1	Michal
Faitl	Faitl	k1gMnSc1	Faitl
(	(	kIx(	(
<g/>
Coopi	Coopi	k1gNnSc1	Coopi
<g/>
;	;	kIx,	;
baskytara	baskytara	k1gFnSc1	baskytara
<g/>
,	,	kIx,	,
*	*	kIx~	*
7	[number]	k4	7
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
1986	[number]	k4	1986
Plzeň	Plzeň	k1gFnSc1	Plzeň
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Než	než	k8xS	než
začala	začít	k5eAaPmAgFnS	začít
jeho	jeho	k3xOp3gFnSc1	jeho
kariéra	kariéra	k1gFnSc1	kariéra
s	s	k7c7	s
Mandrage	Mandrage	k1gFnSc7	Mandrage
<g/>
,	,	kIx,	,
hrál	hrát	k5eAaImAgMnS	hrát
v	v	k7c6	v
kapele	kapela	k1gFnSc6	kapela
Porno	porno	k1gNnSc4	porno
Cowboys	Cowboysa	k1gFnPc2	Cowboysa
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gMnSc1	jeho
bývalý	bývalý	k2eAgMnSc1d1	bývalý
spoluhráč	spoluhráč	k1gMnSc1	spoluhráč
OrG	OrG	k1gMnSc1	OrG
(	(	kIx(	(
<g/>
Tuzex	Tuzex	k1gInSc1	Tuzex
Christ	Christ	k1gInSc1	Christ
ze	z	k7c2	z
Sunshine	Sunshin	k1gInSc5	Sunshin
<g/>
)	)	kIx)	)
teď	teď	k6eAd1	teď
hraje	hrát	k5eAaImIp3nS	hrát
s	s	k7c7	s
Matesem	mates	k1gInSc7	mates
v	v	k7c4	v
Super	super	k2eAgInSc4d1	super
Tuzex	Tuzex	k1gInSc4	Tuzex
Bros	Brosa	k1gFnPc2	Brosa
<g/>
.	.	kIx.	.
</s>
<s>
Matyáš	Matyáš	k1gMnSc1	Matyáš
Vorda	Vorda	k1gMnSc1	Vorda
(	(	kIx(	(
<g/>
Mates	Mates	k1gMnSc1	Mates
<g/>
;	;	kIx,	;
bicí	bicí	k2eAgMnSc1d1	bicí
<g/>
,	,	kIx,	,
*	*	kIx~	*
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1990	[number]	k4	1990
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
odmaturoval	odmaturovat	k5eAaPmAgMnS	odmaturovat
na	na	k7c6	na
gymnáziu	gymnázium	k1gNnSc6	gymnázium
Františka	František	k1gMnSc2	František
Křižíka	Křižík	k1gMnSc2	Křižík
v	v	k7c6	v
Plzni	Plzeň	k1gFnSc6	Plzeň
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
bicí	bicí	k2eAgNnSc4d1	bicí
hraje	hrát	k5eAaImIp3nS	hrát
od	od	k7c2	od
devíti	devět	k4xCc2	devět
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
v	v	k7c6	v
čemž	což	k3yRnSc6	což
se	se	k3xPyFc4	se
učil	učit	k5eAaImAgMnS	učit
u	u	k7c2	u
bubeníka	bubeník	k1gMnSc2	bubeník
Badyho	Bady	k1gMnSc2	Bady
Zbořila	Zbořil	k1gMnSc2	Zbořil
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
zpěvákem	zpěvák	k1gMnSc7	zpěvák
Víťou	Víťou	k?	Víťou
Starým	Starý	k1gMnPc3	Starý
hrál	hrát	k5eAaImAgInS	hrát
od	od	k7c2	od
jedenácti	jedenáct	k4xCc2	jedenáct
let	léto	k1gNnPc2	léto
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
spoluzaložil	spoluzaložit	k5eAaPmAgInS	spoluzaložit
skupinu	skupina	k1gFnSc4	skupina
Mandrage	Mandrag	k1gInSc2	Mandrag
<g/>
.	.	kIx.	.
</s>
<s>
Hraje	hrát	k5eAaImIp3nS	hrát
také	také	k9	také
v	v	k7c6	v
kapelách	kapela	k1gFnPc6	kapela
Super	super	k2eAgInSc1d1	super
Tuzex	Tuzex	k1gInSc1	Tuzex
Bros	Brosa	k1gFnPc2	Brosa
a	a	k8xC	a
Burlesque	Burlesqu	k1gFnSc2	Burlesqu
CZ	CZ	kA	CZ
a	a	k8xC	a
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
jako	jako	k9	jako
DJ	DJ	kA	DJ
Dead	Dead	k1gMnSc1	Dead
Sailor	Sailor	k1gMnSc1	Sailor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
rytmickou	rytmický	k2eAgFnSc7d1	rytmická
skupinou	skupina	k1gFnSc7	skupina
Muerta	Muert	k1gMnSc2	Muert
Mente	Mente	k?	Mente
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Bořík	Bořík	k1gMnSc1	Bořík
(	(	kIx(	(
<g/>
klávesy	kláves	k1gInPc1	kláves
<g/>
,	,	kIx,	,
*	*	kIx~	*
27	[number]	k4	27
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
října	říjen	k1gInSc2	říjen
2011	[number]	k4	2011
účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
v	v	k7c6	v
bubenickém	bubenický	k2eAgNnSc6d1	bubenické
seskupení	seskupení	k1gNnSc6	seskupení
Muerta	Muerta	k1gFnSc1	Muerta
Mente	Mente	k?	Mente
(	(	kIx(	(
<g/>
téměř	téměř	k6eAd1	téměř
7	[number]	k4	7
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jeho	jeho	k3xOp3gInPc7	jeho
záliby	zálib	k1gInPc7	zálib
patří	patřit	k5eAaImIp3nS	patřit
lezení	lezení	k1gNnSc1	lezení
na	na	k7c4	na
komíny	komín	k1gInPc4	komín
<g/>
.	.	kIx.	.
</s>
<s>
Říkala	říkat	k5eAaImAgFnS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jí	on	k3xPp3gFnSc3	on
trápí	trápit	k5eAaImIp3nS	trápit
cosi	cosi	k3yInSc1	cosi
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Přišli	přijít	k5eAaPmAgMnP	přijít
jsme	být	k5eAaImIp1nP	být
si	se	k3xPyFc3	se
pro	pro	k7c4	pro
Vaše	váš	k3xOp2gFnPc4	váš
děti	dítě	k1gFnPc4	dítě
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
Hledá	hledat	k5eAaImIp3nS	hledat
se	se	k3xPyFc4	se
žena	žena	k1gFnSc1	žena
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
Moje	můj	k3xOp1gFnSc1	můj
krevní	krevní	k2eAgFnSc1d1	krevní
skupina	skupina	k1gFnSc1	skupina
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Siluety	silueta	k1gFnSc2	silueta
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
Potmě	potmě	k6eAd1	potmě
jsou	být	k5eAaImIp3nP	být
všechny	všechen	k3xTgFnPc1	všechen
kočky	kočka	k1gFnPc1	kočka
černý	černý	k1gMnSc1	černý
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
Kapky	kapka	k1gFnSc2	kapka
proti	proti	k7c3	proti
slzám	slza	k1gFnPc3	slza
Punk	punk	k1gInSc4	punk
Rock	rock	k1gInSc1	rock
song	song	k1gInSc1	song
Hledá	hledat	k5eAaImIp3nS	hledat
se	se	k3xPyFc4	se
žena	žena	k1gFnSc1	žena
Už	už	k6eAd1	už
mě	já	k3xPp1nSc4	já
víckrát	víckrát	k6eAd1	víckrát
neuvidíš	uvidět	k5eNaPmIp2nS	uvidět
Františkovy	Františkův	k2eAgFnSc2d1	Františkova
lázně	lázeň	k1gFnSc2	lázeň
Šrouby	šroub	k1gInPc1	šroub
a	a	k8xC	a
matice	matice	k1gFnPc1	matice
Mechanik	mechanika	k1gFnPc2	mechanika
Na	na	k7c6	na
dlani	dlaň	k1gFnSc6	dlaň
Siluety	silueta	k1gFnSc2	silueta
Tanči	tančit	k5eAaImRp2nS	tančit
dokud	dokud	k8xS	dokud
můžeš	moct	k5eAaImIp2nS	moct
Brouci	brouk	k1gMnPc1	brouk
Support	support	k1gInSc4	support
Peha	Peha	k1gFnSc1	Peha
tour	tour	k1gInSc1	tour
2007	[number]	k4	2007
Hledá	hledat	k5eAaImIp3nS	hledat
se	se	k3xPyFc4	se
žena	žena	k1gFnSc1	žena
tour	toura	k1gFnPc2	toura
2010	[number]	k4	2010
Tour	Toura	k1gFnPc2	Toura
10	[number]	k4	10
let	léto	k1gNnPc2	léto
Mandrage	Mandrage	k1gFnPc2	Mandrage
po	po	k7c6	po
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
SR	SR	kA	SR
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
VB	VB	kA	VB
a	a	k8xC	a
SRN	SRN	kA	SRN
,2011	,2011	k4	,2011
Moje	můj	k3xOp1gNnSc1	můj
krevní	krevní	k2eAgFnSc1d1	krevní
skupina	skupina	k1gFnSc1	skupina
tour	tour	k1gInSc1	tour
2012	[number]	k4	2012
Siluety	silueta	k1gFnSc2	silueta
tour	tour	k1gInSc4	tour
2014	[number]	k4	2014
Siluety	silueta	k1gFnSc2	silueta
tour	toura	k1gFnPc2	toura
II	II	kA	II
2014	[number]	k4	2014
O	o	k7c4	o
<g/>
(	(	kIx(	(
<g/>
d	d	k?	d
<g/>
)	)	kIx)	)
začátku	začátek	k1gInSc2	začátek
2015	[number]	k4	2015
Utubering	Utubering	k1gInSc1	Utubering
2015	[number]	k4	2015
Seznam	seznam	k1gInSc1	seznam
děl	dít	k5eAaImAgInS	dít
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Mandrage	Mandrage	k1gFnSc1	Mandrage
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Bandzone	Bandzon	k1gInSc5	Bandzon
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
Mandrage	Mandrag	k1gMnSc2	Mandrag
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
MUSICstage	MUSICstage	k1gFnSc1	MUSICstage
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
-	-	kIx~	-
Mandrage	Mandrage	k1gFnSc1	Mandrage
<g/>
:	:	kIx,	:
pódiová	pódiový	k2eAgFnSc1d1	pódiová
technika	technika	k1gFnSc1	technika
</s>
