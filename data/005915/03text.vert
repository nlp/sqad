<s>
Zkratka	zkratka	k1gFnSc1	zkratka
MMORPG	MMORPG	kA	MMORPG
(	(	kIx(	(
<g/>
z	z	k7c2	z
anglického	anglický	k2eAgInSc2d1	anglický
massively	massivel	k1gMnPc4	massivel
multiplayer	multiplayero	k1gNnPc2	multiplayero
online	onlinout	k5eAaPmIp3nS	onlinout
role-playing	rolelaying	k1gInSc1	role-playing
game	game	k1gInSc1	game
<g/>
,	,	kIx,	,
volně	volně	k6eAd1	volně
Hra	hra	k1gFnSc1	hra
obrovského	obrovský	k2eAgInSc2d1	obrovský
počtu	počet	k1gInSc2	počet
hráču	hráču	k?	hráču
s	s	k7c7	s
RPG	RPG	kA	RPG
prvky	prvek	k1gInPc4	prvek
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc1	označení
žánru	žánr	k1gInSc2	žánr
počítačových	počítačový	k2eAgFnPc2d1	počítačová
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
počítačovou	počítačový	k2eAgFnSc4d1	počítačová
online	onlinout	k5eAaPmIp3nS	onlinout
hru	hra	k1gFnSc4	hra
na	na	k7c4	na
hrdiny	hrdina	k1gMnPc4	hrdina
o	o	k7c4	o
více	hodně	k6eAd2	hodně
hráčích	hráč	k1gMnPc6	hráč
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
připojení	připojení	k1gNnSc4	připojení
i	i	k9	i
tisíců	tisíc	k4xCgInPc2	tisíc
hráčů	hráč	k1gMnPc2	hráč
najednou	najednou	k6eAd1	najednou
<g/>
;	;	kIx,	;
zpravidla	zpravidla	k6eAd1	zpravidla
skrze	skrze	k?	skrze
Internet	Internet	k1gInSc1	Internet
<g/>
.	.	kIx.	.
</s>
<s>
Hra	hra	k1gFnSc1	hra
se	se	k3xPyFc4	se
obvykle	obvykle	k6eAd1	obvykle
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
jiné	jiný	k2eAgFnPc1d1	jiná
hry	hra	k1gFnPc1	hra
na	na	k7c4	na
hrdiny	hrdina	k1gMnPc4	hrdina
<g/>
,	,	kIx,	,
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
ve	v	k7c6	v
fiktivním	fiktivní	k2eAgInSc6d1	fiktivní
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
ve	v	k7c4	v
fantasy	fantas	k1gInPc4	fantas
či	či	k8xC	či
sci-fi	scii	k1gFnPc4	sci-fi
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Hry	hra	k1gFnPc1	hra
žánru	žánr	k1gInSc2	žánr
MMORPG	MMORPG	kA	MMORPG
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgInP	vyvinout
z	z	k7c2	z
textových	textový	k2eAgInPc2d1	textový
RPG	RPG	kA	RPG
online	onlinout	k5eAaPmIp3nS	onlinout
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
MUDů	MUD	k1gMnPc2	MUD
<g/>
,	,	kIx,	,
a	a	k8xC	a
původně	původně	k6eAd1	původně
(	(	kIx(	(
<g/>
kolem	kolem	k7c2	kolem
poloviny	polovina	k1gFnSc2	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
označovány	označovat	k5eAaImNgInP	označovat
jako	jako	k8xC	jako
grafické	grafický	k2eAgInPc1d1	grafický
MUDy	MUDy	k1gInPc1	MUDy
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
termínem	termín	k1gInSc7	termín
MMORPG	MMORPG	kA	MMORPG
přišel	přijít	k5eAaPmAgMnS	přijít
až	až	k6eAd1	až
Richard	Richard	k1gMnSc1	Richard
Garriott	Garriott	k1gMnSc1	Garriott
<g/>
,	,	kIx,	,
tvůrce	tvůrce	k1gMnSc1	tvůrce
Ultimy	ultimo	k1gNnPc7	ultimo
Online	Onlin	k1gInSc5	Onlin
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
Ultima	ultimo	k1gNnPc1	ultimo
Online	Onlin	k1gMnSc5	Onlin
<g/>
,	,	kIx,	,
ač	ač	k8xS	ač
nebyla	být	k5eNaImAgFnS	být
zcela	zcela	k6eAd1	zcela
první	první	k4xOgFnSc4	první
grafickou	grafický	k2eAgFnSc4d1	grafická
online	onlinout	k5eAaPmIp3nS	onlinout
hrou	hra	k1gFnSc7	hra
na	na	k7c4	na
hrdiny	hrdina	k1gMnPc4	hrdina
<g/>
,	,	kIx,	,
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
takový	takový	k3xDgInSc4	takový
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
že	že	k8xS	že
hry	hra	k1gFnPc1	hra
podobného	podobný	k2eAgInSc2d1	podobný
rázu	ráz	k1gInSc2	ráz
jsou	být	k5eAaImIp3nP	být
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nazývány	nazývat	k5eAaImNgInP	nazývat
MMORPG	MMORPG	kA	MMORPG
<g/>
.	.	kIx.	.
</s>
<s>
MUDy	MUDa	k1gFnPc1	MUDa
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
někdy	někdy	k6eAd1	někdy
naopak	naopak	k6eAd1	naopak
označované	označovaný	k2eAgFnPc1d1	označovaná
i	i	k8xC	i
jako	jako	k8xS	jako
textové	textový	k2eAgFnPc4d1	textová
MMORPG	MMORPG	kA	MMORPG
hry	hra	k1gFnPc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
PCGamesN	PCGamesN	k1gFnSc2	PCGamesN
mezi	mezi	k7c4	mezi
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
MMORPG	MMORPG	kA	MMORPG
patří	patřit	k5eAaImIp3nS	patřit
EVE	EVE	kA	EVE
Online	Onlin	k1gInSc5	Onlin
<g/>
,	,	kIx,	,
Final	Final	k1gMnSc1	Final
Fantasy	fantas	k1gInPc7	fantas
XIV	XIV	kA	XIV
<g/>
:	:	kIx,	:
A	A	kA	A
Realm	Realm	k1gMnSc1	Realm
Reborn	Reborn	k1gMnSc1	Reborn
<g/>
,	,	kIx,	,
Guild	Guild	k1gMnSc1	Guild
Wars	Wars	k1gInSc4	Wars
2	[number]	k4	2
<g/>
,	,	kIx,	,
Lord	lord	k1gMnSc1	lord
of	of	k?	of
the	the	k?	the
Rings	Rings	k1gInSc1	Rings
Online	Onlin	k1gMnSc5	Onlin
<g/>
,	,	kIx,	,
PlanetSide	PlanetSid	k1gMnSc5	PlanetSid
2	[number]	k4	2
<g/>
,	,	kIx,	,
Rift	Rift	k1gInSc1	Rift
<g/>
,	,	kIx,	,
Runescape	Runescap	k1gInSc5	Runescap
3	[number]	k4	3
<g/>
,	,	kIx,	,
Star	Star	kA	Star
Wars	Wars	k1gInSc1	Wars
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Old	Olda	k1gFnPc2	Olda
Republic	Republice	k1gFnPc2	Republice
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Secret	Secret	k1gMnSc1	Secret
World	World	k1gMnSc1	World
a	a	k8xC	a
World	World	k1gMnSc1	World
of	of	k?	of
Warcraft	Warcraft	k1gMnSc1	Warcraft
<g/>
:	:	kIx,	:
Warlords	Warlords	k1gInSc1	Warlords
of	of	k?	of
Draenor	Draenor	k1gInSc1	Draenor
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
českou	český	k2eAgFnSc4d1	Česká
MMORPG	MMORPG	kA	MMORPG
je	být	k5eAaImIp3nS	být
Chmatákov	Chmatákov	k1gInSc1	Chmatákov
Online	Onlin	k1gInSc5	Onlin
<g/>
.	.	kIx.	.
</s>
