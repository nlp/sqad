<p>
<s>
Ruben	ruben	k2eAgMnSc1d1	ruben
je	být	k5eAaImIp3nS	být
mužské	mužský	k2eAgNnSc1d1	mužské
křestní	křestní	k2eAgNnSc1d1	křestní
jméno	jméno	k1gNnSc1	jméno
hebrejského	hebrejský	k2eAgInSc2d1	hebrejský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Vykládá	vykládat	k5eAaImIp3nS	vykládat
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
ejhle	ejhle	k0	ejhle
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Starém	starý	k2eAgInSc6d1	starý
zákoně	zákon	k1gInSc6	zákon
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
Jákoba	Jákob	k1gMnSc2	Jákob
a	a	k8xC	a
Ley	Lea	k1gFnSc2	Lea
a	a	k8xC	a
předek	předek	k1gInSc1	předek
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
dvanácti	dvanáct	k4xCc2	dvanáct
izraelských	izraelský	k2eAgInPc2d1	izraelský
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Další	další	k2eAgFnPc1d1	další
podoby	podoba	k1gFnPc1	podoba
==	==	k?	==
</s>
</p>
<p>
<s>
Španělsky	španělsky	k6eAd1	španělsky
<g/>
:	:	kIx,	:
Rúben	Rúben	k2eAgInSc1d1	Rúben
</s>
</p>
<p>
<s>
Anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
Reuben	Reuben	k2eAgInSc4d1	Reuben
<g/>
,	,	kIx,	,
Rubin	Rubin	k2eAgInSc4d1	Rubin
</s>
</p>
<p>
<s>
Polsky	Polska	k1gFnPc1	Polska
<g/>
:	:	kIx,	:
Rubin	Rubin	k1gInSc1	Rubin
</s>
</p>
<p>
<s>
Rusky	Ruska	k1gFnPc1	Ruska
<g/>
:	:	kIx,	:
Ruvin	Ruvin	k1gInSc1	Ruvin
</s>
</p>
<p>
<s>
Hebrejsky	hebrejsky	k6eAd1	hebrejsky
<g/>
:	:	kIx,	:
Reuven	Reuven	k2eAgInSc1d1	Reuven
</s>
</p>
<p>
<s>
Řecky	řecky	k6eAd1	řecky
<g/>
:	:	kIx,	:
Rouben	rouben	k2eAgInSc1d1	rouben
</s>
</p>
<p>
<s>
Latinsky	latinsky	k6eAd1	latinsky
<g/>
:	:	kIx,	:
Ruben	ruben	k2eAgInSc1d1	ruben
</s>
</p>
<p>
<s>
Finsky	finsky	k6eAd1	finsky
<g/>
:	:	kIx,	:
Ruuben	Ruuben	k2eAgInSc1d1	Ruuben
</s>
</p>
<p>
<s>
==	==	k?	==
Známí	známý	k2eAgMnPc1d1	známý
nositelé	nositel	k1gMnPc1	nositel
==	==	k?	==
</s>
</p>
<p>
<s>
Rúben	Rúben	k1gInSc1	Rúben
<g/>
,	,	kIx,	,
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
patriarchy	patriarcha	k1gMnSc2	patriarcha
Jákoba	Jákob	k1gMnSc2	Jákob
a	a	k8xC	a
předek	předek	k1gInSc1	předek
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
dvanácti	dvanáct	k4xCc2	dvanáct
izraelských	izraelský	k2eAgInPc2d1	izraelský
kmenů	kmen	k1gInPc2	kmen
</s>
</p>
<p>
<s>
Ruben	ruben	k2eAgMnSc1d1	ruben
Allinger	Allinger	k1gMnSc1	Allinger
<g/>
,	,	kIx,	,
švédský	švédský	k2eAgMnSc1d1	švédský
hokejista	hokejista	k1gMnSc1	hokejista
</s>
</p>
<p>
<s>
Ruben	ruben	k2eAgInSc1d1	ruben
Bemelmans	Bemelmans	k1gInSc1	Bemelmans
<g/>
,	,	kIx,	,
belgický	belgický	k2eAgMnSc1d1	belgický
tenista	tenista	k1gMnSc1	tenista
</s>
</p>
<p>
<s>
Re	re	k9	re
<g/>
'	'	kIx"	'
<g/>
uven	uven	k1gInSc1	uven
Dafni	Dafn	k1gMnPc1	Dafn
<g/>
,	,	kIx,	,
židovský	židovský	k2eAgMnSc1d1	židovský
partyzán	partyzán	k1gMnSc1	partyzán
</s>
</p>
<p>
<s>
Rubén	Rubén	k1gInSc1	Rubén
Darío	Darío	k6eAd1	Darío
<g/>
,	,	kIx,	,
nikaraguajský	nikaraguajský	k2eAgMnSc1d1	nikaraguajský
diplomat	diplomat	k1gMnSc1	diplomat
</s>
</p>
<p>
<s>
Rubén	Rubén	k1gInSc1	Rubén
David	David	k1gMnSc1	David
González	González	k1gMnSc1	González
Gallego	Gallego	k1gMnSc1	Gallego
<g/>
,	,	kIx,	,
ruský	ruský	k2eAgMnSc1d1	ruský
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
novinář	novinář	k1gMnSc1	novinář
</s>
</p>
<p>
<s>
Rubén	Rubén	k1gInSc1	Rubén
González	González	k1gInSc1	González
<g/>
,	,	kIx,	,
kubánský	kubánský	k2eAgMnSc1d1	kubánský
klavírista	klavírista	k1gMnSc1	klavírista
</s>
</p>
<p>
<s>
Rubén	Rubén	k1gInSc1	Rubén
Ramírez	Ramírez	k1gMnSc1	Ramírez
Hidalgo	Hidalgo	k1gMnSc1	Hidalgo
<g/>
,	,	kIx,	,
španělský	španělský	k2eAgMnSc1d1	španělský
tenista	tenista	k1gMnSc1	tenista
</s>
</p>
<p>
<s>
Ernst	Ernst	k1gMnSc1	Ernst
Ruben	ruben	k2eAgMnSc1d1	ruben
Lagus	Lagus	k1gMnSc1	Lagus
<g/>
,	,	kIx,	,
finský	finský	k2eAgMnSc1d1	finský
voják	voják	k1gMnSc1	voják
</s>
</p>
<p>
<s>
Elder	Elder	k1gInSc4	Elder
Ruben	ruben	k2eAgInSc4d1	ruben
<g/>
,	,	kIx,	,
Disco	disco	k1gNnPc7	disco
Priest	Priest	k1gInSc1	Priest
<g/>
,	,	kIx,	,
Knights	Knights	k1gInSc1	Knights
of	of	k?	of
Blood	Blood	k1gInSc1	Blood
Oath	Oath	k1gInSc1	Oath
</s>
</p>
<p>
<s>
Rubén	Rubén	k1gInSc1	Rubén
Neves	Neves	k1gInSc1	Neves
<g/>
,	,	kIx,	,
portugalský	portugalský	k2eAgMnSc1d1	portugalský
fotbalista	fotbalista	k1gMnSc1	fotbalista
hrající	hrající	k2eAgMnSc1d1	hrající
za	za	k7c4	za
Wolverhampton	Wolverhampton	k1gInSc4	Wolverhampton
Wanderers	Wanderersa	k1gFnPc2	Wanderersa
</s>
</p>
<p>
<s>
==	==	k?	==
Fiktivní	fiktivní	k2eAgMnPc1d1	fiktivní
nositelé	nositel	k1gMnPc1	nositel
==	==	k?	==
</s>
</p>
<p>
<s>
Re	re	k9	re
<g/>
'	'	kIx"	'
<g/>
uven	uven	k1gMnSc1	uven
Dagan	Dagan	k1gMnSc1	Dagan
<g/>
,	,	kIx,	,
izraelská	izraelský	k2eAgFnSc1d1	izraelská
postava	postava	k1gFnSc1	postava
ze	z	k7c2	z
seriálu	seriál	k1gInSc2	seriál
Be	Be	k1gFnSc2	Be
Tipul	Tipula	k1gFnPc2	Tipula
<g/>
.	.	kIx.	.
</s>
<s>
Hrál	hrát	k5eAaImAgMnS	hrát
ho	on	k3xPp3gNnSc4	on
Asaf	Asaf	k1gMnSc1	Asaf
Dajan	Dajan	k1gMnSc1	Dajan
</s>
</p>
<p>
<s>
Ruben	ruben	k2eAgInSc1d1	ruben
Victoriano	Victoriana	k1gFnSc5	Victoriana
(	(	kIx(	(
<g/>
Ruvik	Ruvikum	k1gNnPc2	Ruvikum
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fiktivní	fiktivní	k2eAgFnSc1d1	fiktivní
postava	postava	k1gFnSc1	postava
z	z	k7c2	z
hororové	hororový	k2eAgFnSc2d1	hororová
videohry	videohra	k1gFnSc2	videohra
The	The	k1gMnSc1	The
Evil	Evil	k1gMnSc1	Evil
Within	Within	k1gMnSc1	Within
</s>
</p>
<p>
<s>
==	==	k?	==
Ruben	rubit	k5eAaImNgInS	rubit
jako	jako	k9	jako
příjmení	příjmení	k1gNnSc2	příjmení
==	==	k?	==
</s>
</p>
<p>
<s>
Gloria	Gloria	k1gFnSc1	Gloria
Reuben	Reubna	k1gFnPc2	Reubna
<g/>
,	,	kIx,	,
afro-americká	afromerický	k2eAgFnSc1d1	afro-americká
herečka	herečka	k1gFnSc1	herečka
</s>
</p>
<p>
<s>
Christian	Christian	k1gMnSc1	Christian
Ruben	ruben	k2eAgMnSc1d1	ruben
(	(	kIx(	(
<g/>
1805	[number]	k4	1805
<g/>
–	–	k?	–
<g/>
1875	[number]	k4	1875
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
Akademie	akademie	k1gFnSc2	akademie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
</s>
</p>
<p>
<s>
Sam	Sam	k1gMnSc1	Sam
Ruben	ruben	k2eAgMnSc1d1	ruben
(	(	kIx(	(
<g/>
1913	[number]	k4	1913
<g/>
-	-	kIx~	-
<g/>
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
americký	americký	k2eAgMnSc1d1	americký
biochemik	biochemik	k1gMnSc1	biochemik
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Behind	Behind	k1gInSc1	Behind
the	the	k?	the
Name	Name	k1gInSc1	Name
</s>
</p>
