<s>
Alfons	Alfons	k1gMnSc1	Alfons
Maria	Mario	k1gMnSc2	Mario
Mucha	Mucha	k1gMnSc1	Mucha
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1860	[number]	k4	1860
Ivančice	Ivančice	k1gFnPc1	Ivančice
-	-	kIx~	-
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1939	[number]	k4	1939
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
malíř	malíř	k1gMnSc1	malíř
a	a	k8xC	a
designér	designér	k1gMnSc1	designér
období	období	k1gNnSc2	období
secese	secese	k1gFnSc2	secese
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
a	a	k8xC	a
francouzštině	francouzština	k1gFnSc6	francouzština
známo	znám	k2eAgNnSc1d1	známo
pod	pod	k7c7	pod
francouzským	francouzský	k2eAgNnSc7d1	francouzské
označením	označení	k1gNnSc7	označení
Art	Art	k1gFnSc2	Art
nouveau	nouveaus	k1gInSc2	nouveaus
(	(	kIx(	(
<g/>
nové	nový	k2eAgNnSc1d1	nové
umění	umění	k1gNnSc1	umění
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Alfons	Alfons	k1gMnSc1	Alfons
Mucha	Mucha	k1gMnSc1	Mucha
byl	být	k5eAaImAgMnS	být
otcem	otec	k1gMnSc7	otec
spisovatele	spisovatel	k1gMnSc2	spisovatel
Jiřího	Jiří	k1gMnSc2	Jiří
Muchy	Mucha	k1gMnSc2	Mucha
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
známým	známý	k1gMnSc7	známý
téměř	téměř	k6eAd1	téměř
přes	přes	k7c4	přes
noc	noc	k1gFnSc4	noc
díky	díky	k7c3	díky
divadelnímu	divadelní	k2eAgInSc3d1	divadelní
plakátu	plakát	k1gInSc3	plakát
Gismonda	Gismond	k1gMnSc2	Gismond
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
-	-	kIx~	-
<g/>
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
si	se	k3xPyFc3	se
u	u	k7c2	u
něj	on	k3xPp3gNnSc2	on
objednala	objednat	k5eAaPmAgFnS	objednat
pařížská	pařížský	k2eAgFnSc1d1	Pařížská
herečka	herečka	k1gFnSc1	herečka
Sarah	Sarah	k1gFnSc1	Sarah
Bernhardtová	Bernhardtová	k1gFnSc1	Bernhardtová
<g/>
.	.	kIx.	.
</s>
<s>
Herečka	herečka	k1gFnSc1	herečka
později	pozdě	k6eAd2	pozdě
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
s	s	k7c7	s
Muchou	Mucha	k1gMnSc7	Mucha
smlouvu	smlouva	k1gFnSc4	smlouva
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
a	a	k8xC	a
Divadlo	divadlo	k1gNnSc1	divadlo
Renesance	renesance	k1gFnSc2	renesance
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
ještě	ještě	k9	ještě
několik	několik	k4yIc4	několik
dalších	další	k2eAgInPc2d1	další
plakátů	plakát	k1gInPc2	plakát
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
životním	životní	k2eAgInSc7d1	životní
dílem	díl	k1gInSc7	díl
je	být	k5eAaImIp3nS	být
cyklus	cyklus	k1gInSc4	cyklus
velkoformátových	velkoformátový	k2eAgNnPc2d1	velkoformátové
pláten	plátno	k1gNnPc2	plátno
Slovanská	slovanský	k2eAgFnSc1d1	Slovanská
epopej	epopej	k1gFnSc1	epopej
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yRgInSc6	který
pracoval	pracovat	k5eAaImAgMnS	pracovat
osmnáct	osmnáct	k4xCc4	osmnáct
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
také	také	k9	také
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Jaroslavem	Jaroslav	k1gMnSc7	Jaroslav
Kvapilem	Kvapil	k1gMnSc7	Kvapil
a	a	k8xC	a
Ladislavem	Ladislav	k1gMnSc7	Ladislav
Syllabou	Syllaba	k1gMnSc7	Syllaba
o	o	k7c6	o
obnovení	obnovení	k1gNnSc6	obnovení
svobodného	svobodný	k2eAgNnSc2d1	svobodné
zednářství	zednářství	k1gNnSc2	zednářství
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Ivančicích	Ivančice	k1gFnPc6	Ivančice
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
soudního	soudní	k2eAgMnSc2d1	soudní
zřízence	zřízenec	k1gMnSc2	zřízenec
Ondřeje	Ondřej	k1gMnSc2	Ondřej
Muchy	Mucha	k1gMnSc2	Mucha
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
manželky	manželka	k1gFnSc2	manželka
Amálie	Amálie	k1gFnSc2	Amálie
rozené	rozený	k2eAgFnSc2d1	rozená
Malé	Malá	k1gFnSc2	Malá
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
dvě	dva	k4xCgFnPc4	dva
vlastní	vlastní	k2eAgFnPc4d1	vlastní
sestry	sestra	k1gFnPc4	sestra
<g/>
,	,	kIx,	,
Annu	Anna	k1gFnSc4	Anna
a	a	k8xC	a
Andělu	Anděla	k1gFnSc4	Anděla
<g/>
.	.	kIx.	.
</s>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
na	na	k7c6	na
Slovanském	slovanský	k2eAgNnSc6d1	slovanské
gymnáziu	gymnázium	k1gNnSc6	gymnázium
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
přivydělával	přivydělávat	k5eAaImAgMnS	přivydělávat
jako	jako	k9	jako
chrámový	chrámový	k2eAgMnSc1d1	chrámový
zpěvák	zpěvák	k1gMnSc1	zpěvák
v	v	k7c6	v
petrovském	petrovský	k2eAgInSc6d1	petrovský
chlapeckém	chlapecký	k2eAgInSc6d1	chlapecký
sboru	sbor	k1gInSc6	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
neúspěchu	neúspěch	k1gInSc6	neúspěch
u	u	k7c2	u
přijímacích	přijímací	k2eAgFnPc2d1	přijímací
zkoušek	zkouška	k1gFnPc2	zkouška
na	na	k7c4	na
pražskou	pražský	k2eAgFnSc4d1	Pražská
Akademii	akademie	k1gFnSc4	akademie
krátce	krátce	k6eAd1	krátce
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
písař	písař	k1gMnSc1	písař
u	u	k7c2	u
ivančického	ivančický	k2eAgInSc2d1	ivančický
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
připravoval	připravovat	k5eAaImAgInS	připravovat
ochotnická	ochotnický	k2eAgNnPc4d1	ochotnické
divadelní	divadelní	k2eAgNnPc4d1	divadelní
představení	představení	k1gNnPc4	představení
<g/>
,	,	kIx,	,
maloval	malovat	k5eAaImAgMnS	malovat
dekorace	dekorace	k1gFnPc4	dekorace
a	a	k8xC	a
plakáty	plakát	k1gInPc4	plakát
<g/>
,	,	kIx,	,
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
pozvánky	pozvánka	k1gFnSc2	pozvánka
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
roku	rok	k1gInSc2	rok
1875	[number]	k4	1875
pochází	pocházet	k5eAaImIp3nS	pocházet
Muchova	Muchův	k2eAgFnSc1d1	Muchova
první	první	k4xOgFnSc1	první
známá	známá	k1gFnSc1	známá
přesně	přesně	k6eAd1	přesně
datovaná	datovaný	k2eAgFnSc1d1	datovaná
kresba	kresba	k1gFnSc1	kresba
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
pastel	pastel	k1gInSc4	pastel
představující	představující	k2eAgFnSc4d1	představující
Janu	Jana	k1gFnSc4	Jana
z	z	k7c2	z
Arku	arkus	k1gInSc2	arkus
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1879	[number]	k4	1879
odchází	odcházet	k5eAaImIp3nS	odcházet
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
pracovat	pracovat	k5eAaImF	pracovat
jako	jako	k9	jako
malíř	malíř	k1gMnSc1	malíř
divadelních	divadelní	k2eAgFnPc2d1	divadelní
dekorací	dekorace	k1gFnPc2	dekorace
do	do	k7c2	do
firmy	firma	k1gFnSc2	firma
Kautský-Brioschi-Burghardt	Kautský-Brioschi-Burghardta	k1gFnPc2	Kautský-Brioschi-Burghardta
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Mikulova	Mikulov	k1gInSc2	Mikulov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
maloval	malovat	k5eAaImAgMnS	malovat
portréty	portrét	k1gInPc4	portrét
místních	místní	k2eAgMnPc2d1	místní
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1883	[number]	k4	1883
byl	být	k5eAaImAgMnS	být
pozván	pozvat	k5eAaPmNgMnS	pozvat
hrabětem	hrabě	k1gMnSc7	hrabě
Khuen-Belassim	Khuen-Belassima	k1gFnPc2	Khuen-Belassima
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
dekorací	dekorace	k1gFnSc7	dekorace
interiérů	interiér	k1gInPc2	interiér
zámku	zámek	k1gInSc2	zámek
Emmahof	Emmahof	k1gInSc1	Emmahof
či	či	k8xC	či
Emín	Emín	k1gInSc1	Emín
nedaleko	nedaleko	k7c2	nedaleko
Hrušovan	Hrušovany	k1gInPc2	Hrušovany
nad	nad	k7c7	nad
Jevišovkou	Jevišovka	k1gFnSc7	Jevišovka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1885	[number]	k4	1885
odešel	odejít	k5eAaPmAgMnS	odejít
na	na	k7c6	na
Akademii	akademie	k1gFnSc6	akademie
výtvarných	výtvarný	k2eAgNnPc2d1	výtvarné
umění	umění	k1gNnPc2	umění
do	do	k7c2	do
Mnichova	Mnichov	k1gInSc2	Mnichov
(	(	kIx(	(
<g/>
Akademie	akademie	k1gFnSc1	akademie
der	drát	k5eAaImRp2nS	drát
Bildenden	Bildendno	k1gNnPc2	Bildendno
Künste	Künst	k1gMnSc5	Künst
München	Münchno	k1gNnPc2	Münchno
<g/>
)	)	kIx)	)
a	a	k8xC	a
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
na	na	k7c4	na
Julianovu	Julianův	k2eAgFnSc4d1	Julianova
akademii	akademie	k1gFnSc4	akademie
(	(	kIx(	(
<g/>
Académie	Académie	k1gFnSc1	Académie
Julian	Julian	k1gMnSc1	Julian
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
však	však	k9	však
přešel	přejít	k5eAaPmAgInS	přejít
na	na	k7c4	na
Colarossiho	Colarossi	k1gMnSc4	Colarossi
akademii	akademie	k1gFnSc6	akademie
(	(	kIx(	(
<g/>
Académie	Académie	k1gFnSc2	Académie
Colarossi	Colarosse	k1gFnSc4	Colarosse
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
byl	být	k5eAaImAgInS	být
pověřen	pověřit	k5eAaPmNgMnS	pověřit
ilustrovat	ilustrovat	k5eAaBmF	ilustrovat
Scè	Scè	k1gMnSc1	Scè
et	et	k?	et
épisodes	épisodes	k1gMnSc1	épisodes
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
histoire	histoir	k1gInSc5	histoir
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Allemagne	Allemagn	k1gInSc5	Allemagn
(	(	kIx(	(
<g/>
Výjevy	výjev	k1gInPc4	výjev
a	a	k8xC	a
epizody	epizoda	k1gFnPc4	epizoda
z	z	k7c2	z
německé	německý	k2eAgFnSc2d1	německá
historie	historie	k1gFnSc2	historie
<g/>
)	)	kIx)	)
od	od	k7c2	od
Charlese	Charles	k1gMnSc2	Charles
Seignobose	Seignobosa	k1gFnSc6	Seignobosa
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
osudovému	osudový	k2eAgInSc3d1	osudový
zlomu	zlom	k1gInSc3	zlom
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
kariéře	kariéra	k1gFnSc6	kariéra
došlo	dojít	k5eAaPmAgNnS	dojít
roku	rok	k1gInSc2	rok
1894	[number]	k4	1894
<g/>
,	,	kIx,	,
když	když	k8xS	když
získal	získat	k5eAaPmAgMnS	získat
objednávku	objednávka	k1gFnSc4	objednávka
na	na	k7c4	na
plakát	plakát	k1gInSc4	plakát
pro	pro	k7c4	pro
Sarah	Sarah	k1gFnSc4	Sarah
Bernhardtovou	Bernhardtový	k2eAgFnSc4d1	Bernhardtová
<g/>
.	.	kIx.	.
</s>
<s>
Nejslavnější	slavný	k2eAgNnSc1d3	nejslavnější
Muchovo	Muchův	k2eAgNnSc1d1	Muchovo
období	období	k1gNnSc1	období
nastalo	nastat	k5eAaPmAgNnS	nastat
kolem	kolem	k7c2	kolem
přelomu	přelom	k1gInSc2	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Desátého	desátý	k4xOgInSc2	desátý
června	červen	k1gInSc2	červen
1906	[number]	k4	1906
se	se	k3xPyFc4	se
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Rocha	Roch	k1gMnSc2	Roch
na	na	k7c6	na
Strahově	Strahov	k1gInSc6	Strahov
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
o	o	k7c4	o
23	[number]	k4	23
let	léto	k1gNnPc2	léto
mladší	mladý	k2eAgMnSc1d2	mladší
Marií	Maria	k1gFnPc2	Maria
Chytilovou	Chytilová	k1gFnSc7	Chytilová
(	(	kIx(	(
<g/>
1882	[number]	k4	1882
<g/>
-	-	kIx~	-
<g/>
1959	[number]	k4	1959
<g/>
,	,	kIx,	,
jí	on	k3xPp3gFnSc7	on
bylo	být	k5eAaImAgNnS	být
23	[number]	k4	23
<g/>
,	,	kIx,	,
jemu	on	k3xPp3gMnSc3	on
46	[number]	k4	46
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
a	a	k8xC	a
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
si	se	k3xPyFc3	se
vydělal	vydělat	k5eAaPmAgMnS	vydělat
na	na	k7c4	na
realizaci	realizace	k1gFnSc4	realizace
svého	svůj	k3xOyFgInSc2	svůj
velkého	velký	k2eAgInSc2d1	velký
snu	sen	k1gInSc2	sen
<g/>
,	,	kIx,	,
Slovanské	slovanský	k2eAgFnPc1d1	Slovanská
epopeje	epopej	k1gFnPc1	epopej
<g/>
.	.	kIx.	.
</s>
<s>
Dostalo	dostat	k5eAaPmAgNnS	dostat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
tam	tam	k6eAd1	tam
uvítání	uvítání	k1gNnSc1	uvítání
jako	jako	k8xC	jako
největšímu	veliký	k2eAgMnSc3d3	veliký
dekorativnímu	dekorativní	k2eAgMnSc3d1	dekorativní
umělci	umělec	k1gMnSc3	umělec
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
toužil	toužit	k5eAaImAgInS	toužit
vrátit	vrátit	k5eAaPmF	vrátit
do	do	k7c2	do
vlasti	vlast	k1gFnSc2	vlast
<g/>
.	.	kIx.	.
</s>
<s>
Mucha	Mucha	k1gMnSc1	Mucha
však	však	k9	však
nebyl	být	k5eNaImAgInS	být
pouze	pouze	k6eAd1	pouze
grafikem	grafikon	k1gNnSc7	grafikon
a	a	k8xC	a
tvůrcem	tvůrce	k1gMnSc7	tvůrce
světoznámých	světoznámý	k2eAgInPc2d1	světoznámý
plakátů	plakát	k1gInPc2	plakát
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
art	art	k?	art
nouveau	nouveaus	k1gInSc2	nouveaus
<g/>
.	.	kIx.	.
</s>
<s>
Všestranným	všestranný	k2eAgNnSc7d1	všestranné
nadáním	nadání	k1gNnSc7	nadání
zasahoval	zasahovat	k5eAaImAgMnS	zasahovat
do	do	k7c2	do
všech	všecek	k3xTgInPc2	všecek
uměleckých	umělecký	k2eAgInPc2d1	umělecký
oborů	obor	k1gInPc2	obor
<g/>
,	,	kIx,	,
sochařstvím	sochařství	k1gNnPc3	sochařství
počínaje	počínaje	k7c7	počínaje
a	a	k8xC	a
divadlem	divadlo	k1gNnSc7	divadlo
konče	konče	k7c7	konče
<g/>
.	.	kIx.	.
</s>
<s>
Mucha	Mucha	k1gMnSc1	Mucha
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
známým	známý	k2eAgMnSc7d1	známý
jako	jako	k8xS	jako
umělec	umělec	k1gMnSc1	umělec
pracující	pracující	k1gMnSc1	pracující
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
užitého	užitý	k2eAgNnSc2d1	užité
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
reklamních	reklamní	k2eAgInPc2d1	reklamní
plakátů	plakát	k1gInPc2	plakát
tvořil	tvořit	k5eAaImAgMnS	tvořit
i	i	k8xC	i
návrhy	návrh	k1gInPc4	návrh
obalů	obal	k1gInPc2	obal
pro	pro	k7c4	pro
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
spotřebních	spotřební	k2eAgInPc2d1	spotřební
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
maloval	malovat	k5eAaImAgMnS	malovat
návrhy	návrh	k1gInPc1	návrh
jídelních	jídelní	k2eAgNnPc2d1	jídelní
menu	menu	k1gNnPc2	menu
<g/>
,	,	kIx,	,
kalendářů	kalendář	k1gInPc2	kalendář
a	a	k8xC	a
dekorativních	dekorativní	k2eAgFnPc2d1	dekorativní
zástěn	zástěna	k1gFnPc2	zástěna
<g/>
,	,	kIx,	,
doplňoval	doplňovat	k5eAaImAgMnS	doplňovat
knihy	kniha	k1gFnPc4	kniha
svými	svůj	k3xOyFgFnPc7	svůj
ilustracemi	ilustrace	k1gFnPc7	ilustrace
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Otčenáš	otčenáš	k1gInSc1	otčenáš
(	(	kIx(	(
<g/>
Le	Le	k1gFnPc1	Le
Pater	patro	k1gNnPc2	patro
<g/>
,	,	kIx,	,
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
grafickou	grafický	k2eAgFnSc4d1	grafická
předlohu	předloha	k1gFnSc4	předloha
k	k	k7c3	k
první	první	k4xOgFnSc3	první
československé	československý	k2eAgFnSc3d1	Československá
poštovní	poštovní	k2eAgFnSc3d1	poštovní
známce	známka	k1gFnSc3	známka
a	a	k8xC	a
navrhoval	navrhovat	k5eAaImAgMnS	navrhovat
i	i	k9	i
grafickou	grafický	k2eAgFnSc4d1	grafická
podobu	podoba	k1gFnSc4	podoba
československých	československý	k2eAgFnPc2d1	Československá
bankovek	bankovka	k1gFnPc2	bankovka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
tvorba	tvorba	k1gFnSc1	tvorba
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
návrhy	návrh	k1gInPc4	návrh
interiérů	interiér	k1gInPc2	interiér
<g/>
,	,	kIx,	,
nádobí	nádobí	k1gNnSc2	nádobí
<g/>
,	,	kIx,	,
šperků	šperk	k1gInPc2	šperk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
návrh	návrh	k1gInSc4	návrh
okna	okno	k1gNnSc2	okno
v	v	k7c6	v
katedrále	katedrála	k1gFnSc6	katedrála
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
scény	scéna	k1gFnPc1	scéna
z	z	k7c2	z
života	život	k1gInSc2	život
sv.	sv.	kA	sv.
Cyrila	Cyril	k1gMnSc2	Cyril
a	a	k8xC	a
Metoděje	Metoděj	k1gMnSc2	Metoděj
realizovala	realizovat	k5eAaBmAgFnS	realizovat
firma	firma	k1gFnSc1	firma
Jan	Jan	k1gMnSc1	Jan
Veselý	Veselý	k1gMnSc1	Veselý
<g/>
,	,	kIx,	,
od	od	k7c2	od
západu	západ	k1gInSc2	západ
třetí	třetí	k4xOgNnSc4	třetí
okno	okno	k1gNnSc4	okno
vlevo	vlevo	k6eAd1	vlevo
<g/>
)	)	kIx)	)
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Alfons	Alfons	k1gMnSc1	Alfons
Mucha	Mucha	k1gMnSc1	Mucha
byl	být	k5eAaImAgMnS	být
velkým	velký	k2eAgMnSc7d1	velký
vlastencem	vlastenec	k1gMnSc7	vlastenec
a	a	k8xC	a
celý	celý	k2eAgInSc1d1	celý
život	život	k1gInSc1	život
snil	snít	k5eAaImAgInS	snít
o	o	k7c4	o
realizaci	realizace	k1gFnSc4	realizace
cyklu	cyklus	k1gInSc2	cyklus
velkoformátových	velkoformátový	k2eAgInPc2d1	velkoformátový
obrazů	obraz	k1gInPc2	obraz
Slovanská	slovanský	k2eAgFnSc1d1	Slovanská
epopej	epopej	k1gFnSc1	epopej
<g/>
,	,	kIx,	,
kterým	který	k3yRgInSc7	který
chtěl	chtít	k5eAaImAgMnS	chtít
shrnout	shrnout	k5eAaPmF	shrnout
dějiny	dějiny	k1gFnPc4	dějiny
slovanského	slovanský	k2eAgInSc2d1	slovanský
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Cyklus	cyklus	k1gInSc1	cyklus
velkých	velký	k2eAgNnPc2d1	velké
pláten	plátno	k1gNnPc2	plátno
maloval	malovat	k5eAaImAgMnS	malovat
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Zbiroh	Zbiroh	k1gInSc4	Zbiroh
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
<g/>
.	.	kIx.	.
</s>
<s>
Epopej	epopej	k1gFnSc1	epopej
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
a	a	k8xC	a
Muchův	Muchův	k2eAgMnSc1d1	Muchův
americký	americký	k2eAgMnSc1d1	americký
mecenáš	mecenáš	k1gMnSc1	mecenáš
Charles	Charles	k1gMnSc1	Charles
R.	R.	kA	R.
Crane	Cran	k1gInSc5	Cran
ji	on	k3xPp3gFnSc4	on
věnoval	věnovat	k5eAaPmAgMnS	věnovat
Praze	Praha	k1gFnSc3	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc4	tento
cyklus	cyklus	k1gInSc4	cyklus
k	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
Krumlově	Krumlov	k1gInSc6	Krumlov
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
ve	v	k7c6	v
Veletržním	veletržní	k2eAgInSc6d1	veletržní
paláci	palác	k1gInSc6	palác
(	(	kIx(	(
<g/>
výstava	výstava	k1gFnSc1	výstava
otevřena	otevřen	k2eAgFnSc1d1	otevřena
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Alfons	Alfons	k1gMnSc1	Alfons
Mucha	Mucha	k1gMnSc1	Mucha
byl	být	k5eAaImAgMnS	být
také	také	k6eAd1	také
dobrým	dobrý	k2eAgMnSc7d1	dobrý
fotografem	fotograf	k1gMnSc7	fotograf
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
své	svůj	k3xOyFgInPc4	svůj
snímky	snímek	k1gInPc4	snímek
využíval	využívat	k5eAaImAgInS	využívat
jako	jako	k8xS	jako
předlohu	předloha	k1gFnSc4	předloha
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
malířské	malířský	k2eAgFnSc6d1	malířská
tvorbě	tvorba	k1gFnSc6	tvorba
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInPc4	jeho
snímky	snímek	k1gInPc4	snímek
se	se	k3xPyFc4	se
tak	tak	k9	tak
kompozičně	kompozičně	k6eAd1	kompozičně
velmi	velmi	k6eAd1	velmi
podobají	podobat	k5eAaImIp3nP	podobat
jeho	jeho	k3xOp3gInPc3	jeho
slavným	slavný	k2eAgInPc3d1	slavný
obrazům	obraz	k1gInPc3	obraz
<g/>
.	.	kIx.	.
</s>
<s>
Muchova	Muchův	k2eAgFnSc1d1	Muchova
dokumentace	dokumentace	k1gFnSc1	dokumentace
z	z	k7c2	z
cest	cesta	k1gFnPc2	cesta
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
z	z	k7c2	z
období	období	k1gNnSc2	období
před	před	k7c7	před
Říjnovou	říjnový	k2eAgFnSc7d1	říjnová
revolucí	revoluce	k1gFnSc7	revoluce
<g/>
,	,	kIx,	,
tvořila	tvořit	k5eAaImAgFnS	tvořit
důležité	důležitý	k2eAgInPc4d1	důležitý
podklady	podklad	k1gInPc4	podklad
právě	právě	k9	právě
pro	pro	k7c4	pro
cyklus	cyklus	k1gInSc4	cyklus
Slovanská	slovanský	k2eAgFnSc1d1	Slovanská
epopej	epopej	k1gFnSc1	epopej
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
dostal	dostat	k5eAaPmAgMnS	dostat
od	od	k7c2	od
komisaře	komisař	k1gMnSc2	komisař
pařížské	pařížský	k2eAgFnSc2d1	Pařížská
Světové	světový	k2eAgFnSc2d1	světová
výstavy	výstava	k1gFnSc2	výstava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
vyzdobit	vyzdobit	k5eAaPmF	vyzdobit
pavilon	pavilon	k1gInSc4	pavilon
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
<g/>
,	,	kIx,	,
vypravil	vypravit	k5eAaPmAgMnS	vypravit
se	se	k3xPyFc4	se
s	s	k7c7	s
fotoaparátem	fotoaparát	k1gInSc7	fotoaparát
na	na	k7c4	na
Balkán	Balkán	k1gInSc4	Balkán
a	a	k8xC	a
až	až	k9	až
poté	poté	k6eAd1	poté
začal	začít	k5eAaPmAgMnS	začít
malovat	malovat	k5eAaImF	malovat
obří	obří	k2eAgInSc4d1	obří
dekorativní	dekorativní	k2eAgInSc4d1	dekorativní
panel	panel	k1gInSc4	panel
(	(	kIx(	(
<g/>
panó	panó	k1gNnSc2	panó
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
dominantou	dominanta	k1gFnSc7	dominanta
vídeňské	vídeňský	k2eAgFnSc2d1	Vídeňská
expozice	expozice	k1gFnSc2	expozice
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
zakladatele	zakladatel	k1gMnPc4	zakladatel
české	český	k2eAgFnSc2d1	Česká
školy	škola	k1gFnSc2	škola
klasického	klasický	k2eAgInSc2d1	klasický
fotografického	fotografický	k2eAgInSc2d1	fotografický
aktu	akt	k1gInSc2	akt
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
fotografie	fotografia	k1gFnPc1	fotografia
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
součástí	součást	k1gFnSc7	součást
sbírky	sbírka	k1gFnSc2	sbírka
Fotografis	Fotografis	k1gFnSc2	Fotografis
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
představena	představit	k5eAaPmNgFnS	představit
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Alfons	Alfons	k1gMnSc1	Alfons
Mucha	Mucha	k1gMnSc1	Mucha
zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1939	[number]	k4	1939
po	po	k7c6	po
výslechu	výslech	k1gInSc6	výslech
gestapem	gestapo	k1gNnSc7	gestapo
na	na	k7c4	na
zápal	zápal	k1gInSc4	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Pochován	pochován	k2eAgMnSc1d1	pochován
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Slavíně	Slavín	k1gInSc6	Slavín
na	na	k7c6	na
Vyšehradském	vyšehradský	k2eAgInSc6d1	vyšehradský
hřbitově	hřbitov	k1gInSc6	hřbitov
<g/>
.	.	kIx.	.
</s>
<s>
Výběr	výběr	k1gInSc4	výběr
z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
a	a	k8xC	a
některé	některý	k3yIgInPc1	některý
jeho	jeho	k3xOp3gInPc1	jeho
osobní	osobní	k2eAgInPc1d1	osobní
předměty	předmět	k1gInPc1	předmět
jsou	být	k5eAaImIp3nP	být
vystaveny	vystavit	k5eAaPmNgInP	vystavit
v	v	k7c6	v
Muchově	Muchův	k2eAgNnSc6d1	Muchovo
muzeu	muzeum	k1gNnSc6	muzeum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Muchova	Muchův	k2eAgFnSc1d1	Muchova
matka	matka	k1gFnSc1	matka
Amálie	Amálie	k1gFnSc2	Amálie
Malá	malý	k2eAgFnSc1d1	malá
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
5	[number]	k4	5
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1822	[number]	k4	1822
v	v	k7c6	v
Budišově	Budišův	k2eAgFnSc6d1	Budišova
u	u	k7c2	u
Třebíče	Třebíč	k1gFnSc2	Třebíč
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
mlynáře	mlynář	k1gMnSc2	mlynář
Tomáše	Tomáš	k1gMnSc2	Tomáš
Malého	Malý	k1gMnSc2	Malý
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
matce	matka	k1gFnSc6	matka
Alžbětě	Alžběta	k1gFnSc3	Alžběta
Malé	Malá	k1gFnSc3	Malá
(	(	kIx(	(
<g/>
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Ratkowské	Ratkowské	k2eAgFnSc1d1	Ratkowské
<g/>
)	)	kIx)	)
měla	mít	k5eAaImAgFnS	mít
polské	polský	k2eAgInPc4d1	polský
předky	předek	k1gInPc4	předek
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
krásná	krásný	k2eAgFnSc1d1	krásná
<g/>
,	,	kIx,	,
zbožná	zbožný	k2eAgFnSc1d1	zbožná
<g/>
,	,	kIx,	,
vzdělaná	vzdělaný	k2eAgFnSc1d1	vzdělaná
a	a	k8xC	a
sečtělá	sečtělý	k2eAgFnSc1d1	sečtělá
žena	žena	k1gFnSc1	žena
se	s	k7c7	s
zájmem	zájem	k1gInSc7	zájem
o	o	k7c4	o
výtvarné	výtvarný	k2eAgNnSc4d1	výtvarné
umění	umění	k1gNnSc4	umění
byla	být	k5eAaImAgFnS	být
vychovatelkou	vychovatelka	k1gFnSc7	vychovatelka
ve	v	k7c6	v
vídeňské	vídeňský	k2eAgFnSc6d1	Vídeňská
šlechtické	šlechtický	k2eAgFnSc6d1	šlechtická
rodině	rodina	k1gFnSc6	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
žila	žít	k5eAaImAgFnS	žít
do	do	k7c2	do
sedmatřiceti	sedmatřicet	k4xCc2	sedmatřicet
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
na	na	k7c4	na
Moravu	Morava	k1gFnSc4	Morava
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c4	za
vdovce	vdovec	k1gMnPc4	vdovec
<g/>
,	,	kIx,	,
vinaře	vinař	k1gMnPc4	vinař
a	a	k8xC	a
soudního	soudní	k2eAgMnSc4d1	soudní
zaměstnance	zaměstnanec	k1gMnSc4	zaměstnanec
Ondřeje	Ondřej	k1gMnSc4	Ondřej
Muchu	Mucha	k1gMnSc4	Mucha
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
měla	mít	k5eAaImAgFnS	mít
3	[number]	k4	3
děti	dítě	k1gFnPc1	dítě
<g/>
:	:	kIx,	:
syna	syn	k1gMnSc2	syn
Alfonse	Alfons	k1gMnSc2	Alfons
a	a	k8xC	a
dcery	dcera	k1gFnSc2	dcera
Andělu	Anděla	k1gFnSc4	Anděla
a	a	k8xC	a
Annu	Anna	k1gFnSc4	Anna
<g/>
.	.	kIx.	.
</s>
<s>
Anna	Anna	k1gFnSc1	Anna
Muchová	Muchová	k1gFnSc1	Muchová
se	se	k3xPyFc4	se
vdala	vdát	k5eAaPmAgFnS	vdát
za	za	k7c4	za
Filipa	Filip	k1gMnSc4	Filip
Kubera	Kuber	k1gMnSc4	Kuber
<g/>
,	,	kIx,	,
patriota	patriot	k1gMnSc4	patriot
a	a	k8xC	a
představitele	představitel	k1gMnSc4	představitel
české	český	k2eAgFnSc2d1	Česká
menšiny	menšina	k1gFnSc2	menšina
v	v	k7c6	v
Hustopečích	Hustopeč	k1gFnPc6	Hustopeč
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterým	který	k3yRgFnPc3	který
její	její	k3xOp3gFnSc6	její
bratr	bratr	k1gMnSc1	bratr
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
na	na	k7c4	na
propagaci	propagace	k1gFnSc4	propagace
místní	místní	k2eAgFnSc2d1	místní
menšiny	menšina	k1gFnSc2	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
dcerou	dcera	k1gFnSc7	dcera
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1889	[number]	k4	1889
přistěhoval	přistěhovat	k5eAaPmAgMnS	přistěhovat
Ondřej	Ondřej	k1gMnSc1	Ondřej
Mucha	Mucha	k1gMnSc1	Mucha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Hustopečích	Hustopeč	k1gFnPc6	Hustopeč
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
žila	žít	k5eAaImAgFnS	žít
i	i	k9	i
Anděla	Anděla	k1gFnSc1	Anděla
Muchová	Muchová	k1gFnSc1	Muchová
s	s	k7c7	s
manželem	manžel	k1gMnSc7	manžel
Janem	Jan	k1gMnSc7	Jan
Remundou	remunda	k1gFnSc7	remunda
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
když	když	k8xS	když
bylo	být	k5eAaImAgNnS	být
Muchovi	Mucha	k1gMnSc3	Mucha
téměř	téměř	k6eAd1	téměř
46	[number]	k4	46
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Marií	Maria	k1gFnSc7	Maria
Chytilovou	Chytilův	k2eAgFnSc7d1	Chytilova
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
měl	mít	k5eAaImAgMnS	mít
dceru	dcera	k1gFnSc4	dcera
Jaroslavu	Jaroslava	k1gFnSc4	Jaroslava
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
-	-	kIx~	-
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
a	a	k8xC	a
syna	syn	k1gMnSc2	syn
Jiřího	Jiří	k1gMnSc2	Jiří
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
-	-	kIx~	-
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
divadelní	divadelní	k2eAgInSc1d1	divadelní
plakát	plakát	k1gInSc1	plakát
Gismonda	Gismonda	k1gFnSc1	Gismonda
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
-	-	kIx~	-
<g/>
1895	[number]	k4	1895
<g/>
)	)	kIx)	)
divadelní	divadelní	k2eAgInSc4d1	divadelní
plakát	plakát	k1gInSc4	plakát
Dáma	dáma	k1gFnSc1	dáma
s	s	k7c7	s
kaméliemi	kamélie	k1gFnPc7	kamélie
(	(	kIx(	(
<g/>
La	la	k1gNnSc7	la
Dame	Dam	k1gFnSc2	Dam
aux	aux	k?	aux
Camelias	Camelias	k1gMnSc1	Camelias
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
divadelní	divadelní	k2eAgInSc4d1	divadelní
plakát	plakát	k1gInSc4	plakát
Lorenzaccio	Lorenzaccio	k6eAd1	Lorenzaccio
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
barevný	barevný	k2eAgInSc1d1	barevný
kalendář	kalendář	k1gInSc1	kalendář
Biscuits	Biscuitsa	k1gFnPc2	Biscuitsa
Lefevre-Utile	Lefevre-Util	k1gMnSc5	Lefevre-Util
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
divadelní	divadelní	k2eAgFnSc2d1	divadelní
<g />
.	.	kIx.	.
</s>
<s>
plakát	plakát	k1gInSc1	plakát
Médea	Médea	k1gFnSc1	Médea
(	(	kIx(	(
<g/>
Medee	Medee	k1gFnSc1	Medee
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
divadelní	divadelní	k2eAgInSc1d1	divadelní
plakát	plakát	k1gInSc1	plakát
Samaritánka	samaritánka	k1gFnSc1	samaritánka
(	(	kIx(	(
<g/>
La	la	k0	la
Samaritaine	Samaritain	k1gMnSc5	Samaritain
<g/>
)	)	kIx)	)
divadelní	divadelní	k2eAgInSc4d1	divadelní
plakát	plakát	k1gInSc4	plakát
Tosca	Tosca	k1gFnSc1	Tosca
(	(	kIx(	(
<g/>
La	la	k1gNnSc1	la
Tosca	Tosca	k1gFnSc1	Tosca
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
divadelní	divadelní	k2eAgInSc4d1	divadelní
plakát	plakát	k1gInSc4	plakát
Hamlet	Hamlet	k1gMnSc1	Hamlet
(	(	kIx(	(
<g/>
Tragique	Tragiquus	k1gMnSc5	Tragiquus
Histoire	Histoir	k1gMnSc5	Histoir
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Hamlet	Hamlet	k1gMnSc1	Hamlet
-	-	kIx~	-
Prince	princ	k1gMnSc2	princ
de	de	k?	de
<g />
.	.	kIx.	.
</s>
<s>
Danemark	Danemark	k1gInSc1	Danemark
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
<g/>
)	)	kIx)	)
plakát	plakát	k1gInSc1	plakát
Zvěrokruh	zvěrokruh	k1gInSc1	zvěrokruh
-	-	kIx~	-
La	la	k0	la
Plume	Plum	k1gMnSc5	Plum
(	(	kIx(	(
<g/>
okolo	okolo	k7c2	okolo
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
návrh	návrh	k1gInSc1	návrh
interiéru	interiér	k1gInSc2	interiér
Primátorského	primátorský	k2eAgInSc2d1	primátorský
salonku	salonek	k1gInSc2	salonek
v	v	k7c6	v
Obecním	obecní	k2eAgInSc6d1	obecní
domě	dům	k1gInSc6	dům
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
návrh	návrh	k1gInSc4	návrh
klenotnictví	klenotnictví	k1gNnSc2	klenotnictví
Fouquet	Fouqueta	k1gFnPc2	Fouqueta
pro	pro	k7c4	pro
George	Georg	k1gMnSc4	Georg
Fouqueta	Fouquet	k1gMnSc4	Fouquet
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
návrh	návrh	k1gInSc4	návrh
pavilonu	pavilon	k1gInSc2	pavilon
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
pro	pro	k7c4	pro
Všeobecnou	všeobecný	k2eAgFnSc4d1	všeobecná
výstavu	výstava	k1gFnSc4	výstava
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
návrh	návrh	k1gInSc1	návrh
<g />
.	.	kIx.	.
</s>
<s>
vitráže	vitráž	k1gFnPc1	vitráž
okna	okno	k1gNnSc2	okno
v	v	k7c6	v
Nové	Nová	k1gFnSc6	Nová
arcibiskupské	arcibiskupský	k2eAgFnSc6d1	arcibiskupská
(	(	kIx(	(
<g/>
Horově	Horův	k2eAgFnSc6d1	Horova
<g/>
)	)	kIx)	)
kapli	kaple	k1gFnSc4	kaple
katedrály	katedrála	k1gFnSc2	katedrála
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
-	-	kIx~	-
Slovanští	slovanštit	k5eAaImIp3nP	slovanštit
věrozvěstové	věrozvěst	k1gMnPc1	věrozvěst
sv.	sv.	kA	sv.
Cyril	Cyril	k1gMnSc1	Cyril
a	a	k8xC	a
Metoděj	Metoděj	k1gMnSc1	Metoděj
<g/>
,	,	kIx,	,
sponzorovala	sponzorovat	k5eAaImAgFnS	sponzorovat
banka	banka	k1gFnSc1	banka
Slavie	slavie	k1gFnSc2	slavie
soubor	soubor	k1gInSc4	soubor
20	[number]	k4	20
velkoformátových	velkoformátový	k2eAgInPc2d1	velkoformátový
obrazů	obraz	k1gInPc2	obraz
Slovanská	slovanský	k2eAgFnSc1d1	Slovanská
epopej	epopej	k1gFnSc1	epopej
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
-	-	kIx~	-
<g/>
1928	[number]	k4	1928
<g/>
)	)	kIx)	)
-	-	kIx~	-
vznikal	vznikat	k5eAaImAgInS	vznikat
na	na	k7c6	na
zámku	zámek	k1gInSc6	zámek
Zbiroh	Zbiroh	k1gInSc1	Zbiroh
<g/>
,	,	kIx,	,
darován	darován	k2eAgInSc1d1	darován
Hlavnímu	hlavní	k2eAgInSc3d1	hlavní
městu	město	k1gNnSc3	město
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Alfonsu	Alfonso	k1gMnSc6	Alfonso
Muchovi	Mucha	k1gMnSc6	Mucha
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
Antonín	Antonín	k1gMnSc1	Antonín
Mrkos	Mrkos	k1gMnSc1	Mrkos
planetku	planetka	k1gFnSc4	planetka
(	(	kIx(	(
<g/>
5122	[number]	k4	5122
<g/>
)	)	kIx)	)
Mucha	Mucha	k1gMnSc1	Mucha
<g/>
.	.	kIx.	.
</s>
