<s>
Maledivy	Maledivy	k1gFnPc1	Maledivy
(	(	kIx(	(
<g/>
Maledivská	maledivský	k2eAgFnSc1d1	Maledivská
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
ostrovní	ostrovní	k2eAgInSc4d1	ostrovní
stát	stát	k1gInSc4	stát
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Asie	Asie	k1gFnSc2	Asie
v	v	k7c6	v
Indickém	indický	k2eAgInSc6d1	indický
oceánu	oceán	k1gInSc6	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nS	tvořit
je	on	k3xPp3gNnSc4	on
stejnojmenné	stejnojmenný	k2eAgNnSc4d1	stejnojmenné
souostroví	souostroví	k1gNnSc4	souostroví
–	–	k?	–
řetězec	řetězec	k1gInSc1	řetězec
19	[number]	k4	19
korálových	korálový	k2eAgInPc2d1	korálový
atolů	atol	k1gInPc2	atol
<g/>
,	,	kIx,	,
počínající	počínající	k2eAgFnSc6d1	počínající
500	[number]	k4	500
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
břehů	břeh	k1gInPc2	břeh
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
táhnoucí	táhnoucí	k2eAgInPc1d1	táhnoucí
se	se	k3xPyFc4	se
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
zhruba	zhruba	k6eAd1	zhruba
800	[number]	k4	800
km	km	kA	km
severojižním	severojižní	k2eAgInSc7d1	severojižní
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
Maledivy	Maledivy	k1gFnPc1	Maledivy
sestávají	sestávat	k5eAaImIp3nP	sestávat
z	z	k7c2	z
1196	[number]	k4	1196
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
obydleno	obydlet	k5eAaPmNgNnS	obydlet
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
200	[number]	k4	200
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
nejplošším	plochý	k2eAgMnSc7d3	nejplošší
a	a	k8xC	a
nejníže	nízce	k6eAd3	nízce
položeným	položený	k2eAgInSc7d1	položený
státem	stát	k1gInSc7	stát
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
žádné	žádný	k3yNgNnSc4	žádný
místo	místo	k1gNnSc4	místo
neleží	ležet	k5eNaImIp3nS	ležet
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2,5	[number]	k4	2,5
m	m	kA	m
nad	nad	k7c7	nad
hladinou	hladina	k1gFnSc7	hladina
oceánu	oceán	k1gInSc2	oceán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
vyhledávané	vyhledávaný	k2eAgInPc4d1	vyhledávaný
turistické	turistický	k2eAgInPc4d1	turistický
cíle	cíl	k1gInPc4	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Maledivy	Maledivy	k1gFnPc1	Maledivy
tvoří	tvořit	k5eAaImIp3nP	tvořit
řetěz	řetěz	k1gInSc4	řetěz
19	[number]	k4	19
korálových	korálový	k2eAgInPc2d1	korálový
ostrovů	ostrov	k1gInPc2	ostrov
v	v	k7c6	v
Indickém	indický	k2eAgInSc6d1	indický
oceánu	oceán	k1gInSc6	oceán
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
Srí	Srí	k1gFnSc1	Srí
Lanky	lanko	k1gNnPc7	lanko
<g/>
.	.	kIx.	.
</s>
<s>
Rozprostírají	rozprostírat	k5eAaImIp3nP	rozprostírat
se	se	k3xPyFc4	se
v	v	k7c6	v
severojižním	severojižní	k2eAgInSc6d1	severojižní
směrem	směr	k1gInSc7	směr
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
871	[number]	k4	871
km	km	kA	km
poblíž	poblíž	k6eAd1	poblíž
73	[number]	k4	73
<g/>
°	°	k?	°
východní	východní	k2eAgFnSc2d1	východní
délky	délka	k1gFnSc2	délka
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
mezi	mezi	k7c7	mezi
rovníkem	rovník	k1gInSc7	rovník
a	a	k8xC	a
osmým	osmý	k4xOgInSc7	osmý
stupněm	stupeň	k1gInSc7	stupeň
severní	severní	k2eAgFnSc2d1	severní
šířky	šířka	k1gFnSc2	šířka
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
indických	indický	k2eAgFnPc2d1	indická
Lakadiv	Lakadivy	k1gFnPc2	Lakadivy
je	on	k3xPp3gNnSc4	on
dělí	dělit	k5eAaImIp3nS	dělit
Průliv	průliv	k1gInSc1	průliv
osmého	osmý	k4xOgInSc2	osmý
stupně	stupeň	k1gInSc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
následují	následovat	k5eAaImIp3nP	následovat
nejosídlenější	osídlený	k2eAgInPc4d3	osídlený
ostrovy	ostrov	k1gInPc4	ostrov
(	(	kIx(	(
<g/>
sčítání	sčítání	k1gNnSc2	sčítání
březen	březen	k1gInSc4	březen
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Male	male	k6eAd1	male
<g/>
:	:	kIx,	:
104	[number]	k4	104
403	[number]	k4	403
obyvatel	obyvatel	k1gMnPc2	obyvatel
Hithadhoo	Hithadhoo	k6eAd1	Hithadhoo
<g/>
:	:	kIx,	:
9	[number]	k4	9
407	[number]	k4	407
obyvatel	obyvatel	k1gMnPc2	obyvatel
Fuvammulah	Fuvammulaha	k1gFnPc2	Fuvammulaha
<g/>
:	:	kIx,	:
7	[number]	k4	7
642	[number]	k4	642
obyvatel	obyvatel	k1gMnPc2	obyvatel
Kulhudhuffushi	Kulhudhuffush	k1gFnSc2	Kulhudhuffush
<g/>
:	:	kIx,	:
7	[number]	k4	7
206	[number]	k4	206
obyvatel	obyvatel	k1gMnPc2	obyvatel
Thinadhoo	Thinadhoo	k6eAd1	Thinadhoo
<g/>
:	:	kIx,	:
4	[number]	k4	4
453	[number]	k4	453
obyvatel	obyvatel	k1gMnPc2	obyvatel
První	první	k4xOgNnPc1	první
osídlení	osídlení	k1gNnSc1	osídlení
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
500	[number]	k4	500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
rybáři	rybář	k1gMnPc1	rybář
z	z	k7c2	z
Indie	Indie	k1gFnSc2	Indie
a	a	k8xC	a
Srí	Srí	k1gFnSc1	Srí
Lanky	lanko	k1gNnPc7	lanko
<g/>
,	,	kIx,	,
zmínky	zmínka	k1gFnPc4	zmínka
o	o	k7c6	o
Maledivách	Maledivy	k1gFnPc6	Maledivy
však	však	k9	však
pocházejí	pocházet	k5eAaImIp3nP	pocházet
už	už	k6eAd1	už
z	z	k7c2	z
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
od	od	k7c2	od
arabských	arabský	k2eAgMnPc2d1	arabský
obchodníků	obchodník	k1gMnPc2	obchodník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
zastavovali	zastavovat	k5eAaImAgMnP	zastavovat
na	na	k7c4	na
odpočinek	odpočinek	k1gInSc4	odpočinek
<g/>
.	.	kIx.	.
</s>
<s>
Mezníkem	mezník	k1gInSc7	mezník
je	být	k5eAaImIp3nS	být
rok	rok	k1gInSc4	rok
1153	[number]	k4	1153
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
panovník	panovník	k1gMnSc1	panovník
nařídil	nařídit	k5eAaPmAgMnS	nařídit
okamžitý	okamžitý	k2eAgInSc4d1	okamžitý
přestup	přestup	k1gInSc4	přestup
na	na	k7c4	na
islám	islám	k1gInSc4	islám
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
příchodem	příchod	k1gInSc7	příchod
Portugalců	Portugalec	k1gMnPc2	Portugalec
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
vládlo	vládnout	k5eAaImAgNnS	vládnout
Maledivám	Maledivy	k1gFnPc3	Maledivy
celkem	celkem	k6eAd1	celkem
6	[number]	k4	6
sultánských	sultánský	k2eAgFnPc2d1	sultánská
dynastií	dynastie	k1gFnPc2	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Portugalská	portugalský	k2eAgFnSc1d1	portugalská
okupace	okupace	k1gFnSc1	okupace
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
trvala	trvat	k5eAaImAgFnS	trvat
jen	jen	k9	jen
15	[number]	k4	15
let	léto	k1gNnPc2	léto
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
jedinou	jediný	k2eAgFnSc4d1	jediná
opravdovou	opravdový	k2eAgFnSc4d1	opravdová
nadvládu	nadvláda	k1gFnSc4	nadvláda
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
staletí	staletí	k1gNnSc2	staletí
byly	být	k5eAaImAgInP	být
opakované	opakovaný	k2eAgInPc1d1	opakovaný
pokusy	pokus	k1gInPc1	pokus
o	o	k7c4	o
ovládnutí	ovládnutí	k1gNnSc4	ovládnutí
souostroví	souostroví	k1gNnSc2	souostroví
jak	jak	k8xC	jak
Portugalci	Portugalec	k1gMnPc1	Portugalec
<g/>
,	,	kIx,	,
tak	tak	k9	tak
Indy	Indus	k1gInPc1	Indus
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
také	také	k9	také
Holanďany	Holanďan	k1gMnPc7	Holanďan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
proto	proto	k8xC	proto
akceptovaly	akceptovat	k5eAaBmAgFnP	akceptovat
Maledivy	Maledivy	k1gFnPc1	Maledivy
ochranu	ochrana	k1gFnSc4	ochrana
od	od	k7c2	od
Holanďanů	Holanďan	k1gMnPc2	Holanďan
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
tehdy	tehdy	k6eAd1	tehdy
vládli	vládnout	k5eAaImAgMnP	vládnout
Srí	Srí	k1gFnSc4	Srí
Lance	lance	k1gNnSc2	lance
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
také	také	k9	také
uzavřena	uzavřen	k2eAgFnSc1d1	uzavřena
mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
s	s	k7c7	s
Francouzi	Francouz	k1gMnPc7	Francouz
a	a	k8xC	a
Brity	Brit	k1gMnPc4	Brit
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
jim	on	k3xPp3gMnPc3	on
zaručovala	zaručovat	k5eAaImAgFnS	zaručovat
ochranu	ochrana	k1gFnSc4	ochrana
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
autonomii	autonomie	k1gFnSc4	autonomie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1796	[number]	k4	1796
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
britským	britský	k2eAgInSc7d1	britský
protektorátem	protektorát	k1gInSc7	protektorát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
uznala	uznat	k5eAaPmAgFnS	uznat
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
sehrála	sehrát	k5eAaPmAgFnS	sehrát
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
Malediv	Maledivy	k1gFnPc2	Maledivy
za	za	k7c2	za
uplynulých	uplynulý	k2eAgNnPc2d1	uplynulé
několik	několik	k4yIc4	několik
století	století	k1gNnPc2	století
významnou	významný	k2eAgFnSc4d1	významná
roli	role	k1gFnSc4	role
<g/>
,	,	kIx,	,
souostroví	souostroví	k1gNnSc4	souostroví
za	za	k7c4	za
suverénní	suverénní	k2eAgInSc4d1	suverénní
a	a	k8xC	a
nezávislý	závislý	k2eNgInSc4d1	nezávislý
sultanát	sultanát	k1gInSc4	sultanát
a	a	k8xC	a
zbavila	zbavit	k5eAaPmAgFnS	zbavit
se	se	k3xPyFc4	se
tak	tak	k9	tak
odpovědnosti	odpovědnost	k1gFnSc2	odpovědnost
za	za	k7c4	za
jeho	jeho	k3xOp3gFnSc4	jeho
obranu	obrana	k1gFnSc4	obrana
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1968	[number]	k4	1968
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
republika	republika	k1gFnSc1	republika
opustila	opustit	k5eAaPmAgFnS	opustit
Společenství	společenství	k1gNnSc4	společenství
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
před	před	k7c7	před
vyhlášením	vyhlášení	k1gNnSc7	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
byly	být	k5eAaImAgFnP	být
Maledivy	Maledivy	k1gFnPc1	Maledivy
málo	málo	k6eAd1	málo
vyspělé	vyspělý	k2eAgFnPc1d1	vyspělá
<g/>
,	,	kIx,	,
nejedná	jednat	k5eNaImIp3nS	jednat
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
o	o	k7c4	o
nijak	nijak	k6eAd1	nijak
chudý	chudý	k2eAgInSc4d1	chudý
stát	stát	k1gInSc4	stát
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
rozvojovou	rozvojový	k2eAgFnSc7d1	rozvojová
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
díky	díky	k7c3	díky
rozvoji	rozvoj	k1gInSc3	rozvoj
turistického	turistický	k2eAgInSc2d1	turistický
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
významným	významný	k2eAgInSc7d1	významný
sektorem	sektor	k1gInSc7	sektor
je	být	k5eAaImIp3nS	být
rybářství	rybářství	k1gNnSc1	rybářství
<g/>
.	.	kIx.	.
</s>
<s>
HDP	HDP	kA	HDP
(	(	kIx(	(
<g/>
PPP	PPP	kA	PPP
<g/>
)	)	kIx)	)
v	v	k7c6	v
přepočtu	přepočet	k1gInSc6	přepočet
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
9000	[number]	k4	9000
USD	USD	kA	USD
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
stojí	stát	k5eAaImIp3nS	stát
prezident	prezident	k1gMnSc1	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
je	být	k5eAaImIp3nS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
parlamentem	parlament	k1gInSc7	parlament
<g/>
,	,	kIx,	,
nominace	nominace	k1gFnSc1	nominace
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
schválena	schválit	k5eAaPmNgFnS	schválit
v	v	k7c6	v
národním	národní	k2eAgNnSc6d1	národní
referendu	referendum	k1gNnSc6	referendum
(	(	kIx(	(
<g/>
požadováno	požadovat	k5eAaImNgNnS	požadovat
je	být	k5eAaImIp3nS	být
nejméně	málo	k6eAd3	málo
51	[number]	k4	51
<g/>
%	%	kIx~	%
rozdílu	rozdíl	k1gInSc2	rozdíl
hlasů	hlas	k1gInPc2	hlas
mezi	mezi	k7c7	mezi
kandidáty	kandidát	k1gMnPc7	kandidát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgInS	volit
na	na	k7c4	na
pětileté	pětiletý	k2eAgNnSc4d1	pětileté
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
byl	být	k5eAaImAgInS	být
znovuzvolen	znovuzvolit	k5eAaPmNgMnS	znovuzvolit
prezident	prezident	k1gMnSc1	prezident
Maumoon	Maumoon	k1gMnSc1	Maumoon
Abdul	Abdul	k1gMnSc1	Abdul
Gayoom	Gayoom	k1gInSc4	Gayoom
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
v	v	k7c6	v
referendu	referendum	k1gNnSc6	referendum
90,9	[number]	k4	90,9
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
byl	být	k5eAaImAgInS	být
však	však	k9	však
nově	nově	k6eAd1	nově
zvolen	zvolen	k2eAgMnSc1d1	zvolen
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Mohamed	Mohamed	k1gMnSc1	Mohamed
Waheed	Waheed	k1gMnSc1	Waheed
Hassan	Hassan	k1gMnSc1	Hassan
Manik	Manik	k1gMnSc1	Manik
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
je	být	k5eAaImIp3nS	být
jmenována	jmenovat	k5eAaBmNgFnS	jmenovat
prezidentem	prezident	k1gMnSc7	prezident
(	(	kIx(	(
<g/>
člen	člen	k1gMnSc1	člen
vlády	vláda	k1gFnSc2	vláda
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
členem	člen	k1gMnSc7	člen
parlamentu	parlament	k1gInSc2	parlament
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Majlis	Majlis	k1gFnSc1	Majlis
(	(	kIx(	(
<g/>
národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
50	[number]	k4	50
členů	člen	k1gInPc2	člen
<g/>
,	,	kIx,	,
42	[number]	k4	42
členů	člen	k1gMnPc2	člen
je	být	k5eAaImIp3nS	být
voleno	volen	k2eAgNnSc1d1	voleno
na	na	k7c4	na
pětileté	pětiletý	k2eAgNnSc4d1	pětileté
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
<g/>
,	,	kIx,	,
8	[number]	k4	8
členů	člen	k1gMnPc2	člen
je	být	k5eAaImIp3nS	být
jmenováno	jmenován	k2eAgNnSc1d1	jmenováno
prezidentem	prezident	k1gMnSc7	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
posledních	poslední	k2eAgFnPc2d1	poslední
voleb	volba	k1gFnPc2	volba
22	[number]	k4	22
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2005	[number]	k4	2005
byli	být	k5eAaImAgMnP	být
voleni	volen	k2eAgMnPc1d1	volen
pouze	pouze	k6eAd1	pouze
nestraníci	nestraník	k1gMnPc1	nestraník
<g/>
.	.	kIx.	.
</s>
<s>
Neexistují	existovat	k5eNaImIp3nP	existovat
žádné	žádný	k3yNgNnSc1	žádný
legální	legální	k2eAgFnPc1d1	legální
politické	politický	k2eAgFnPc1d1	politická
strany	strana	k1gFnPc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
42	[number]	k4	42
členů	člen	k1gInPc2	člen
parlamentu	parlament	k1gInSc2	parlament
je	být	k5eAaImIp3nS	být
20	[number]	k4	20
(	(	kIx(	(
<g/>
3,3	[number]	k4	3,3
%	%	kIx~	%
voličů	volič	k1gMnPc2	volič
<g/>
)	)	kIx)	)
podporováno	podporován	k2eAgNnSc4d1	podporováno
parlamentem	parlament	k1gInSc7	parlament
a	a	k8xC	a
18	[number]	k4	18
(	(	kIx(	(
<g/>
31,1	[number]	k4	31,1
%	%	kIx~	%
voličů	volič	k1gMnPc2	volič
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
podporováno	podporován	k2eAgNnSc1d1	podporováno
Maledivskou	maledivský	k2eAgFnSc7d1	Maledivská
demokratickou	demokratický	k2eAgFnSc7d1	demokratická
stranou	strana	k1gFnSc7	strana
(	(	kIx(	(
<g/>
MDP	MDP	kA	MDP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
Civilní	civilní	k2eAgInSc1d1	civilní
soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
Trestní	trestní	k2eAgInSc1d1	trestní
soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
Soud	soud	k1gInSc1	soud
pro	pro	k7c4	pro
rodiny	rodina	k1gFnPc4	rodina
a	a	k8xC	a
mladistvé	mladistvý	k1gMnPc4	mladistvý
a	a	k8xC	a
204	[number]	k4	204
obecných	obecný	k2eAgInPc2d1	obecný
soudů	soud	k1gInPc2	soud
(	(	kIx(	(
<g/>
na	na	k7c6	na
každém	každý	k3xTgInSc6	každý
obydleném	obydlený	k2eAgInSc6d1	obydlený
ostrově	ostrov	k1gInSc6	ostrov
jeden	jeden	k4xCgMnSc1	jeden
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Soudce	soudce	k1gMnSc1	soudce
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
prezident	prezident	k1gMnSc1	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgMnSc1d3	nejvyšší
státní	státní	k2eAgMnSc1d1	státní
zástupce	zástupce	k1gMnSc1	zástupce
<g/>
:	:	kIx,	:
Hassan	Hassan	k1gMnSc1	Hassan
Saeed	Saeed	k1gMnSc1	Saeed
Poslední	poslední	k2eAgFnSc2d1	poslední
verze	verze	k1gFnSc2	verze
ústavy	ústava	k1gFnSc2	ústava
byla	být	k5eAaImAgFnS	být
přijata	přijat	k2eAgFnSc1d1	přijata
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Volební	volební	k2eAgNnSc4d1	volební
právo	právo	k1gNnSc4	právo
mají	mít	k5eAaImIp3nP	mít
lidé	člověk	k1gMnPc1	člověk
nad	nad	k7c4	nad
21	[number]	k4	21
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
muži	muž	k1gMnPc1	muž
i	i	k8xC	i
ženy	žena	k1gFnPc1	žena
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
volit	volit	k5eAaImF	volit
mohou	moct	k5eAaImIp3nP	moct
pouze	pouze	k6eAd1	pouze
muslimové	muslim	k1gMnPc1	muslim
Předchozí	předchozí	k2eAgFnSc1d1	předchozí
hlava	hlava	k1gFnSc1	hlava
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
Maumoon	Maumoon	k1gMnSc1	Maumoon
Abdul	Abdul	k1gMnSc1	Abdul
Gayoom	Gayoom	k1gInSc1	Gayoom
[	[	kIx(	[
<g/>
gajúm	gajúm	k1gInSc1	gajúm
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
poprvé	poprvé	k6eAd1	poprvé
zvolen	zvolit	k5eAaPmNgMnS	zvolit
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
nástupcem	nástupce	k1gMnSc7	nástupce
Ibrahima	Ibrahim	k1gMnSc2	Ibrahim
Násira	Násir	k1gMnSc2	Násir
<g/>
.	.	kIx.	.
</s>
<s>
Naposled	naposled	k6eAd1	naposled
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
(	(	kIx(	(
<g/>
pošesté	pošesté	k4xO	pošesté
<g/>
)	)	kIx)	)
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2003	[number]	k4	2003
(	(	kIx(	(
<g/>
90,28	[number]	k4	90,28
%	%	kIx~	%
všech	všecek	k3xTgInPc2	všecek
hlasů	hlas	k1gInPc2	hlas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
byl	být	k5eAaImAgMnS	být
prezidentem	prezident	k1gMnSc7	prezident
zvolen	zvolit	k5eAaPmNgMnS	zvolit
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Mohamed	Mohamed	k1gMnSc1	Mohamed
Waheed	Waheed	k1gMnSc1	Waheed
Hassan	Hassan	k1gMnSc1	Hassan
Manik	Manik	k1gMnSc1	Manik
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Maledivách	Maledivy	k1gFnPc6	Maledivy
není	být	k5eNaImIp3nS	být
povolena	povolit	k5eAaPmNgFnS	povolit
činnost	činnost	k1gFnSc1	činnost
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Jediná	jediný	k2eAgFnSc1d1	jediná
politická	politický	k2eAgFnSc1d1	politická
strana	strana	k1gFnSc1	strana
je	být	k5eAaImIp3nS	být
exilová	exilový	k2eAgFnSc1d1	exilová
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
skupinou	skupina	k1gFnSc7	skupina
lidí	člověk	k1gMnPc2	člověk
o	o	k7c6	o
42	[number]	k4	42
členech	člen	k1gInPc6	člen
(	(	kIx(	(
<g/>
členové	člen	k1gMnPc1	člen
parlamentu	parlament	k1gInSc2	parlament
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
ministr	ministr	k1gMnSc1	ministr
kabinetu	kabinet	k1gInSc2	kabinet
<g/>
,	,	kIx,	,
bohatí	bohatý	k2eAgMnPc1d1	bohatý
maledivští	maledivský	k2eAgMnPc1d1	maledivský
obchodníci	obchodník	k1gMnPc1	obchodník
<g/>
,	,	kIx,	,
akademičtí	akademický	k2eAgMnPc1d1	akademický
pracovníci	pracovník	k1gMnPc1	pracovník
a	a	k8xC	a
sportovci	sportovec	k1gMnPc1	sportovec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vedení	vedení	k1gNnSc1	vedení
bylo	být	k5eAaImAgNnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
zformováno	zformovat	k5eAaPmNgNnS	zformovat
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2003	[number]	k4	2003
na	na	k7c6	na
Srí	Srí	k1gFnSc6	Srí
Lance	lance	k1gNnSc2	lance
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2004	[number]	k4	2004
byli	být	k5eAaImAgMnP	být
zvoleni	zvolen	k2eAgMnPc1d1	zvolen
členové	člen	k1gMnPc1	člen
generální	generální	k2eAgFnSc2d1	generální
rady	rada	k1gFnSc2	rada
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Předsedou	předseda	k1gMnSc7	předseda
strany	strana	k1gFnSc2	strana
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Mohamed	Mohamed	k1gMnSc1	Mohamed
Latheef	Latheef	k1gMnSc1	Latheef
s	s	k7c7	s
trvalým	trvalý	k2eAgInSc7d1	trvalý
pobytem	pobyt	k1gInSc7	pobyt
na	na	k7c6	na
Srí	Srí	k1gFnSc6	Srí
Lance	lance	k1gNnSc2	lance
<g/>
.	.	kIx.	.
</s>
<s>
Strana	strana	k1gFnSc1	strana
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
kvůli	kvůli	k7c3	kvůli
prosazování	prosazování	k1gNnSc3	prosazování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
demokracie	demokracie	k1gFnSc2	demokracie
na	na	k7c6	na
Maledivách	Maledivy	k1gFnPc6	Maledivy
<g/>
.	.	kIx.	.
</s>
<s>
Prezident	prezident	k1gMnSc1	prezident
Gayoom	Gayoom	k1gInSc1	Gayoom
ovšem	ovšem	k9	ovšem
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
tuto	tento	k3xDgFnSc4	tento
stranu	strana	k1gFnSc4	strana
registrovat	registrovat	k5eAaBmF	registrovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgNnSc6	tento
rozhodnutí	rozhodnutí	k1gNnSc6	rozhodnutí
se	se	k3xPyFc4	se
strana	strana	k1gFnSc1	strana
začala	začít	k5eAaPmAgFnS	začít
orientovat	orientovat	k5eAaBmF	orientovat
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
nenásilného	násilný	k2eNgInSc2d1	nenásilný
odporu	odpor	k1gInSc2	odpor
proti	proti	k7c3	proti
maledivské	maledivský	k2eAgFnSc3d1	Maledivská
vládě	vláda	k1gFnSc3	vláda
a	a	k8xC	a
na	na	k7c4	na
informování	informování	k1gNnSc4	informování
lidí	člověk	k1gMnPc2	člověk
o	o	k7c6	o
jejich	jejich	k3xOp3gNnPc6	jejich
právech	právo	k1gNnPc6	právo
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosinci	prosinec	k1gInSc6	prosinec
2004	[number]	k4	2004
vypršely	vypršet	k5eAaPmAgInP	vypršet
mandáty	mandát	k1gInPc4	mandát
členům	člen	k1gMnPc3	člen
Majlisu	Majlis	k1gInSc2	Majlis
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnPc1d1	poslední
volby	volba	k1gFnPc1	volba
do	do	k7c2	do
parlamentu	parlament	k1gInSc2	parlament
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
22	[number]	k4	22
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2005	[number]	k4	2005
(	(	kIx(	(
<g/>
původní	původní	k2eAgInSc1d1	původní
termín	termín	k1gInSc1	termín
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
v	v	k7c6	v
31	[number]	k4	31
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
katastrofě	katastrofa	k1gFnSc3	katastrofa
<g/>
,	,	kIx,	,
zemětřesení	zemětřesení	k1gNnSc4	zemětřesení
a	a	k8xC	a
tsunami	tsunami	k1gNnSc4	tsunami
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
zasáhlo	zasáhnout	k5eAaPmAgNnS	zasáhnout
i	i	k9	i
Maledivy	Maledivy	k1gFnPc1	Maledivy
<g/>
,	,	kIx,	,
z	z	k7c2	z
konce	konec	k1gInSc2	konec
prosince	prosinec	k1gInSc2	prosinec
2004	[number]	k4	2004
se	se	k3xPyFc4	se
termín	termín	k1gInSc1	termín
posunul	posunout	k5eAaPmAgInS	posunout
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
149	[number]	k4	149
kandidátů	kandidát	k1gMnPc2	kandidát
<g/>
,	,	kIx,	,
všichni	všechen	k3xTgMnPc1	všechen
byli	být	k5eAaImAgMnP	být
nestraníci	nestraník	k1gMnPc1	nestraník
(	(	kIx(	(
<g/>
bylo	být	k5eAaImAgNnS	být
zde	zde	k6eAd1	zde
ovšem	ovšem	k9	ovšem
mnoho	mnoho	k4c4	mnoho
přesvědčení	přesvědčení	k1gNnPc2	přesvědčení
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
všeobecně	všeobecně	k6eAd1	všeobecně
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
má	mít	k5eAaImIp3nS	mít
jaké	jaký	k3yIgFnPc4	jaký
politické	politický	k2eAgFnPc4d1	politická
myšlenky	myšlenka	k1gFnPc4	myšlenka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
den	den	k1gInSc4	den
voleb	volba	k1gFnPc2	volba
policie	policie	k1gFnSc2	policie
zatkla	zatknout	k5eAaPmAgFnS	zatknout
20	[number]	k4	20
příslušníků	příslušník	k1gMnPc2	příslušník
opozice	opozice	k1gFnSc2	opozice
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
zapečetit	zapečetit	k5eAaPmF	zapečetit
hlasovací	hlasovací	k2eAgFnPc4d1	hlasovací
urny	urna	k1gFnPc4	urna
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
do	do	k7c2	do
nich	on	k3xPp3gMnPc2	on
voliči	volič	k1gMnPc1	volič
vhodili	vhodit	k5eAaPmAgMnP	vhodit
své	svůj	k3xOyFgInPc4	svůj
hlasy	hlas	k1gInPc4	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomuto	tento	k3xDgInSc3	tento
incidentu	incident	k1gInSc3	incident
musely	muset	k5eAaImAgFnP	muset
zůstat	zůstat	k5eAaPmF	zůstat
hlasovací	hlasovací	k2eAgFnPc4d1	hlasovací
místnosti	místnost	k1gFnPc4	místnost
otevřené	otevřený	k2eAgNnSc1d1	otevřené
i	i	k9	i
den	den	k1gInSc4	den
po	po	k7c6	po
dni	den	k1gInSc6	den
voleb	volba	k1gFnPc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Úředníci	úředník	k1gMnPc1	úředník
z	z	k7c2	z
Commonwealthu	Commonwealth	k1gInSc2	Commonwealth
a	a	k8xC	a
Jihoasijské	jihoasijský	k2eAgFnSc2d1	jihoasijská
asociace	asociace	k1gFnSc2	asociace
pro	pro	k7c4	pro
regionální	regionální	k2eAgFnSc4d1	regionální
spolupráci	spolupráce	k1gFnSc4	spolupráce
(	(	kIx(	(
<g/>
SAARC	SAARC	kA	SAARC
<g/>
)	)	kIx)	)
monitorovali	monitorovat	k5eAaImAgMnP	monitorovat
volby	volba	k1gFnPc4	volba
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
nedošlo	dojít	k5eNaPmAgNnS	dojít
ke	k	k7c3	k
zfalšování	zfalšování	k1gNnSc3	zfalšování
či	či	k8xC	či
ovlivnění	ovlivnění	k1gNnSc3	ovlivnění
voličů	volič	k1gInPc2	volič
<g/>
.	.	kIx.	.
</s>
<s>
Volební	volební	k2eAgFnSc1d1	volební
účast	účast	k1gFnSc1	účast
byla	být	k5eAaImAgFnS	být
70	[number]	k4	70
%	%	kIx~	%
z	z	k7c2	z
156	[number]	k4	156
766	[number]	k4	766
oprávněných	oprávněný	k2eAgMnPc2d1	oprávněný
voličů	volič	k1gMnPc2	volič
<g/>
.	.	kIx.	.
</s>
<s>
Maledivská	maledivský	k2eAgFnSc1d1	Maledivská
demokratická	demokratický	k2eAgFnSc1d1	demokratická
strana	strana	k1gFnSc1	strana
(	(	kIx(	(
<g/>
MDP	MDP	kA	MDP
<g/>
)	)	kIx)	)
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gMnPc1	její
přívrženci	přívrženec	k1gMnPc1	přívrženec
získali	získat	k5eAaPmAgMnP	získat
18	[number]	k4	18
ze	z	k7c2	z
42	[number]	k4	42
křesel	křeslo	k1gNnPc2	křeslo
<g/>
,	,	kIx,	,
provládní	provládní	k2eAgMnPc1d1	provládní
členové	člen	k1gMnPc1	člen
jich	on	k3xPp3gFnPc2	on
získali	získat	k5eAaPmAgMnP	získat
22	[number]	k4	22
a	a	k8xC	a
nezávislí	závislý	k2eNgMnPc1d1	nezávislý
získali	získat	k5eAaPmAgMnP	získat
2	[number]	k4	2
křesla	křeslo	k1gNnSc2	křeslo
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
naopak	naopak	k6eAd1	naopak
prohlásila	prohlásit	k5eAaPmAgFnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
provládní	provládní	k2eAgMnPc1d1	provládní
členové	člen	k1gMnPc1	člen
získali	získat	k5eAaPmAgMnP	získat
30	[number]	k4	30
křesel	křeslo	k1gNnPc2	křeslo
<g/>
,	,	kIx,	,
MDP	MDP	kA	MDP
8	[number]	k4	8
a	a	k8xC	a
nezávislí	závislý	k2eNgMnPc1d1	nezávislý
4	[number]	k4	4
křesla	křeslo	k1gNnSc2	křeslo
<g/>
.	.	kIx.	.
</s>
<s>
Důvod	důvod	k1gInSc1	důvod
těchto	tento	k3xDgInPc2	tento
rozdílů	rozdíl	k1gInPc2	rozdíl
v	v	k7c6	v
tvrzeních	tvrzení	k1gNnPc6	tvrzení
je	být	k5eAaImIp3nS	být
ten	ten	k3xDgInSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
politické	politický	k2eAgFnPc1d1	politická
strany	strana	k1gFnPc1	strana
zakázány	zakázat	k5eAaPmNgFnP	zakázat
a	a	k8xC	a
u	u	k7c2	u
členů	člen	k1gMnPc2	člen
parlamentu	parlament	k1gInSc2	parlament
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
určit	určit	k5eAaPmF	určit
pouze	pouze	k6eAd1	pouze
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
příslušnost	příslušnost	k1gFnSc4	příslušnost
ke	k	k7c3	k
straně	strana	k1gFnSc3	strana
<g/>
.	.	kIx.	.
</s>
<s>
Funkční	funkční	k2eAgNnSc1d1	funkční
období	období	k1gNnSc1	období
je	být	k5eAaImIp3nS	být
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
příští	příští	k2eAgFnPc1d1	příští
volby	volba	k1gFnPc1	volba
se	se	k3xPyFc4	se
měly	mít	k5eAaImAgFnP	mít
konat	konat	k5eAaImF	konat
začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Mohamed	Mohamed	k1gMnSc1	Mohamed
Nasheed	Nasheed	k1gMnSc1	Nasheed
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
přímo	přímo	k6eAd1	přímo
zvoleno	zvolit	k5eAaPmNgNnS	zvolit
42	[number]	k4	42
členů	člen	k1gMnPc2	člen
<g/>
,	,	kIx,	,
8	[number]	k4	8
jich	on	k3xPp3gFnPc2	on
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
prezident	prezident	k1gMnSc1	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
prezident	prezident	k1gMnSc1	prezident
Gayoom	Gayoom	k1gInSc4	Gayoom
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
vpustí	vpustit	k5eAaPmIp3nS	vpustit
do	do	k7c2	do
politiky	politika	k1gFnSc2	politika
demokracii	demokracie	k1gFnSc4	demokracie
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
reforma	reforma	k1gFnSc1	reforma
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
obsahovat	obsahovat	k5eAaImF	obsahovat
ustanovení	ustanovení	k1gNnSc4	ustanovení
systému	systém	k1gInSc2	systém
vícestranné	vícestranný	k2eAgFnSc2d1	vícestranná
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
zřízení	zřízení	k1gNnSc1	zřízení
kanceláře	kancelář	k1gFnSc2	kancelář
předsedy	předseda	k1gMnSc2	předseda
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Padl	padnout	k5eAaPmAgInS	padnout
také	také	k9	také
návrh	návrh	k1gInSc1	návrh
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
schválena	schválit	k5eAaPmNgFnS	schválit
přímá	přímý	k2eAgFnSc1d1	přímá
volba	volba	k1gFnSc1	volba
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
na	na	k7c6	na
území	území	k1gNnSc6	území
politické	politický	k2eAgInPc1d1	politický
problémy	problém	k1gInPc1	problém
nevyskytují	vyskytovat	k5eNaImIp3nP	vyskytovat
<g/>
,	,	kIx,	,
na	na	k7c6	na
základě	základ	k1gInSc6	základ
přání	přání	k1gNnSc4	přání
lidu	lid	k1gInSc2	lid
probíhá	probíhat	k5eAaImIp3nS	probíhat
transformace	transformace	k1gFnSc1	transformace
zákonů	zákon	k1gInPc2	zákon
a	a	k8xC	a
ústavy	ústava	k1gFnSc2	ústava
(	(	kIx(	(
<g/>
poslední	poslední	k2eAgFnSc1d1	poslední
úprava	úprava	k1gFnSc1	úprava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Maledivy	Maledivy	k1gFnPc1	Maledivy
udržují	udržovat	k5eAaImIp3nP	udržovat
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
Evropskou	evropský	k2eAgFnSc7d1	Evropská
unií	unie	k1gFnSc7	unie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dotuje	dotovat	k5eAaBmIp3nS	dotovat
nové	nový	k2eAgInPc4d1	nový
projekty	projekt	k1gInPc4	projekt
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
turismu	turismus	k1gInSc2	turismus
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
země	zem	k1gFnSc2	zem
kooperují	kooperovat	k5eAaImIp3nP	kooperovat
s	s	k7c7	s
OSN	OSN	kA	OSN
(	(	kIx(	(
<g/>
Rozvojový	rozvojový	k2eAgInSc1d1	rozvojový
program	program	k1gInSc1	program
OSN	OSN	kA	OSN
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Asijskou	asijský	k2eAgFnSc7d1	asijská
rozvojovou	rozvojový	k2eAgFnSc7d1	rozvojová
bankou	banka	k1gFnSc7	banka
(	(	kIx(	(
<g/>
ADB	ADB	kA	ADB
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Islámskou	islámský	k2eAgFnSc7d1	islámská
rozvojovou	rozvojový	k2eAgFnSc7d1	rozvojová
bankou	banka	k1gFnSc7	banka
(	(	kIx(	(
<g/>
IDB	IDB	kA	IDB
<g/>
)	)	kIx)	)
a	a	k8xC	a
Světovou	světový	k2eAgFnSc7d1	světová
bankou	banka	k1gFnSc7	banka
(	(	kIx(	(
<g/>
WB	WB	kA	WB
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
bilaterálně	bilaterálně	k6eAd1	bilaterálně
spolupracují	spolupracovat	k5eAaImIp3nP	spolupracovat
s	s	k7c7	s
Indií	Indie	k1gFnSc7	Indie
<g/>
,	,	kIx,	,
Japonskem	Japonsko	k1gNnSc7	Japonsko
<g/>
,	,	kIx,	,
Austrálií	Austrálie	k1gFnSc7	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Stálý	stálý	k2eAgMnSc1d1	stálý
reprezentant	reprezentant	k1gMnSc1	reprezentant
u	u	k7c2	u
OSN	OSN	kA	OSN
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
:	:	kIx,	:
Mohamed	Mohamed	k1gMnSc1	Mohamed
Latheef	Latheef	k1gMnSc1	Latheef
Hlavou	Hlava	k1gMnSc7	Hlava
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1968	[number]	k4	1968
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgMnS	být
sultán	sultán	k1gMnSc1	sultán
(	(	kIx(	(
<g/>
kromě	kromě	k7c2	kromě
let	léto	k1gNnPc2	léto
1953	[number]	k4	1953
a	a	k8xC	a
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1945	[number]	k4	1945
po	po	k7c4	po
současnost	současnost	k1gFnSc4	současnost
<g/>
:	:	kIx,	:
sultán	sultán	k1gMnSc1	sultán
Amin	amin	k1gInSc1	amin
'	'	kIx"	'
<g/>
Abd	Abd	k1gFnSc1	Abd
al-Majid	al-Majida	k1gFnPc2	al-Majida
<g />
.	.	kIx.	.
</s>
<s>
Didi	Didi	k1gNnSc1	Didi
;	;	kIx,	;
3	[number]	k4	3
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1945	[number]	k4	1945
–	–	k?	–
21	[number]	k4	21
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1952	[number]	k4	1952
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
)	)	kIx)	)
předseda	předseda	k1gMnSc1	předseda
maledivského	maledivský	k2eAgInSc2d1	maledivský
protektorátu	protektorát	k1gInSc2	protektorát
Amir	Amir	k1gInSc1	Amir
Ahmad	Ahmad	k1gInSc1	Ahmad
Muhammad	Muhammad	k1gInSc1	Muhammad
Amin	amin	k1gInSc4	amin
Didi	Did	k1gFnSc2	Did
;	;	kIx,	;
21	[number]	k4	21
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1952	[number]	k4	1952
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1953	[number]	k4	1953
(	(	kIx(	(
<g/>
+	+	kIx~	+
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
Amir	Amir	k1gInSc1	Amir
Ahmad	Ahmad	k1gInSc1	Ahmad
Muhammad	Muhammad	k1gInSc1	Muhammad
Amin	amin	k1gInSc4	amin
<g />
.	.	kIx.	.
</s>
<s>
Didi	Didi	k1gNnSc1	Didi
;	;	kIx,	;
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1953	[number]	k4	1953
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1953	[number]	k4	1953
(	(	kIx(	(
<g/>
+	+	kIx~	+
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
Amir	Amir	k1gInSc1	Amir
Ibrahim	Ibrahima	k1gFnPc2	Ibrahima
Muhammad	Muhammad	k1gInSc4	Muhammad
Didi	Dide	k1gFnSc4	Dide
;	;	kIx,	;
2	[number]	k4	2
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1953	[number]	k4	1953
-	-	kIx~	-
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1954	[number]	k4	1954
sultán	sultán	k1gMnSc1	sultán
Ali	Ali	k1gMnSc1	Ali
Muhammad	Muhammad	k1gInSc4	Muhammad
Farid	Farid	k1gInSc1	Farid
Didi	Didi	k1gNnSc1	Didi
;	;	kIx,	;
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1954	[number]	k4	1954
–	–	k?	–
11	[number]	k4	11
<g/>
.	.	kIx.	.
<g />
.	.	kIx.	.
</s>
<s>
listopadu	listopad	k1gInSc2	listopad
1968	[number]	k4	1968
(	(	kIx(	(
<g/>
+	+	kIx~	+
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
Ibrahim	Ibrahim	k1gMnSc1	Ibrahim
Nasir	Nasir	k1gMnSc1	Nasir
R.	R.	kA	R.
Kilagefaanu	Kilagefaan	k1gInSc6	Kilagefaan
;	;	kIx,	;
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1968	[number]	k4	1968
–	–	k?	–
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1978	[number]	k4	1978
Maumoon	Maumoon	k1gMnSc1	Maumoon
Abdul	Abdul	k1gMnSc1	Abdul
Gayoom	Gayoom	k1gInSc1	Gayoom
;	;	kIx,	;
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1978	[number]	k4	1978
(	(	kIx(	(
<g/>
znovuzvolen	znovuzvolit	k5eAaPmNgInS	znovuzvolit
1983	[number]	k4	1983
<g/>
,	,	kIx,	,
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
a	a	k8xC	a
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
'	'	kIx"	'
Mohammad	Mohammad	k1gInSc1	Mohammad
Našíd	Našída	k1gFnPc2	Našída
-	-	kIx~	-
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
Maledivy	Maledivy	k1gFnPc1	Maledivy
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
přes	přes	k7c4	přes
1,2	[number]	k4	1,2
milionů	milion	k4xCgInPc2	milion
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
posloužila	posloužit	k5eAaPmAgFnS	posloužit
geografická	geografický	k2eAgFnSc1d1	geografická
poloha	poloha	k1gFnSc1	poloha
Malediv	Maledivy	k1gFnPc2	Maledivy
k	k	k7c3	k
sérii	série	k1gFnSc3	série
měření	měření	k1gNnSc2	měření
znečištění	znečištění	k1gNnSc1	znečištění
atmosféry	atmosféra	k1gFnSc2	atmosféra
leteckou	letecký	k2eAgFnSc7d1	letecká
dopravou	doprava	k1gFnSc7	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Experiment	experiment	k1gInSc1	experiment
přispěl	přispět	k5eAaPmAgInS	přispět
k	k	k7c3	k
potvrzení	potvrzení	k1gNnSc3	potvrzení
fenoménu	fenomén	k1gInSc2	fenomén
globálního	globální	k2eAgNnSc2d1	globální
stmívání	stmívání	k1gNnSc2	stmívání
<g/>
.	.	kIx.	.
</s>
<s>
Maledivský	maledivský	k2eAgMnSc1d1	maledivský
prezident	prezident	k1gMnSc1	prezident
Mohammad	Mohammad	k1gInSc4	Mohammad
Našíd	Našíd	k1gMnSc1	Našíd
uvedl	uvést	k5eAaPmAgMnS	uvést
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2008	[number]	k4	2008
pro	pro	k7c4	pro
britský	britský	k2eAgInSc4d1	britský
deník	deník	k1gInSc4	deník
The	The	k1gMnSc1	The
Guardian	Guardian	k1gMnSc1	Guardian
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
země	země	k1gFnSc1	země
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
vytvořit	vytvořit	k5eAaPmF	vytvořit
fond	fond	k1gInSc4	fond
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgInSc2	který
by	by	kYmCp3nS	by
si	se	k3xPyFc3	se
Maledivy	Maledivy	k1gFnPc1	Maledivy
mohly	moct	k5eAaImAgFnP	moct
nakoupit	nakoupit	k5eAaPmF	nakoupit
nová	nový	k2eAgNnPc4d1	nové
území	území	k1gNnPc4	území
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
vlivem	vliv	k1gInSc7	vliv
globálního	globální	k2eAgNnSc2d1	globální
oteplování	oteplování	k1gNnSc2	oteplování
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
být	být	k5eAaImF	být
ostrovní	ostrovní	k2eAgInSc1d1	ostrovní
stát	stát	k1gInSc1	stát
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
zaplaven	zaplavit	k5eAaPmNgMnS	zaplavit
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
už	už	k6eAd1	už
začala	začít	k5eAaPmAgFnS	začít
možnost	možnost	k1gFnSc4	možnost
odkup	odkup	k1gInSc1	odkup
území	území	k1gNnSc2	území
sondovat	sondovat	k5eAaImF	sondovat
-	-	kIx~	-
v	v	k7c4	v
úvahu	úvaha	k1gFnSc4	úvaha
připadá	připadat	k5eAaPmIp3nS	připadat
Šrí	Šrí	k1gFnSc1	Šrí
Lanka	lanko	k1gNnSc2	lanko
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc2	Indie
či	či	k8xC	či
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Maledivy	Maledivy	k1gFnPc1	Maledivy
jsou	být	k5eAaImIp3nP	být
nejmenší	malý	k2eAgInPc1d3	nejmenší
asijskou	asijský	k2eAgFnSc7d1	asijská
zemí	zem	k1gFnSc7	zem
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nejmenší	malý	k2eAgFnSc7d3	nejmenší
muslimskou	muslimský	k2eAgFnSc7d1	muslimská
zemí	zem	k1gFnSc7	zem
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Maledivách	Maledivy	k1gFnPc6	Maledivy
platí	platit	k5eAaImIp3nS	platit
islámské	islámský	k2eAgNnSc1d1	islámské
právo	právo	k1gNnSc1	právo
šaría	šarí	k1gInSc2	šarí
a	a	k8xC	a
jediným	jediný	k2eAgNnSc7d1	jediné
povoleným	povolený	k2eAgNnSc7d1	povolené
náboženstvím	náboženství	k1gNnSc7	náboženství
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
islám	islám	k1gInSc1	islám
<g/>
.	.	kIx.	.
</s>
<s>
Alkohol	alkohol	k1gInSc1	alkohol
je	být	k5eAaImIp3nS	být
dostupný	dostupný	k2eAgInSc1d1	dostupný
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
cizince	cizinec	k1gMnPc4	cizinec
v	v	k7c6	v
hotelech	hotel	k1gInPc6	hotel
<g/>
.	.	kIx.	.
</s>
<s>
Homosexualita	homosexualita	k1gFnSc1	homosexualita
je	být	k5eAaImIp3nS	být
nelegální	legální	k2eNgInSc4d1	nelegální
a	a	k8xC	a
pohlavní	pohlavní	k2eAgInSc4d1	pohlavní
styk	styk	k1gInSc4	styk
s	s	k7c7	s
osobou	osoba	k1gFnSc7	osoba
téhož	týž	k3xTgNnSc2	týž
pohlaví	pohlaví	k1gNnSc2	pohlaví
se	se	k3xPyFc4	se
trestá	trestat	k5eAaImIp3nS	trestat
až	až	k9	až
8	[number]	k4	8
lety	léto	k1gNnPc7	léto
vězení	vězení	k1gNnSc1	vězení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
odsoudil	odsoudit	k5eAaPmAgInS	odsoudit
soud	soud	k1gInSc1	soud
ke	k	k7c3	k
100	[number]	k4	100
ranám	rána	k1gFnPc3	rána
bičem	bič	k1gInSc7	bič
za	za	k7c4	za
předmanželský	předmanželský	k2eAgInSc4d1	předmanželský
sex	sex	k1gInSc4	sex
patnáctiletou	patnáctiletý	k2eAgFnSc4d1	patnáctiletá
dívku	dívka	k1gFnSc4	dívka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
znásilnil	znásilnit	k5eAaPmAgMnS	znásilnit
její	její	k3xOp3gMnSc1	její
nevlastní	vlastní	k2eNgMnSc1d1	nevlastní
otec	otec	k1gMnSc1	otec
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Filipský	Filipský	k2eAgMnSc1d1	Filipský
<g/>
,	,	kIx,	,
Blanka	Blanka	k1gFnSc1	Blanka
Knotková-Čapková	Knotková-Čapková	k1gFnSc1	Knotková-Čapková
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Marek	Marek	k1gMnSc1	Marek
<g/>
,	,	kIx,	,
Stanislava	Stanislava	k1gFnSc1	Stanislava
Vavroušková	Vavroušková	k1gFnSc1	Vavroušková
<g/>
:	:	kIx,	:
Dějiny	dějiny	k1gFnPc1	dějiny
Bangladéše	Bangladéš	k1gInSc2	Bangladéš
<g/>
,	,	kIx,	,
Bhútánu	Bhútán	k1gInSc2	Bhútán
<g/>
,	,	kIx,	,
Malediv	Maledivy	k1gFnPc2	Maledivy
<g/>
,	,	kIx,	,
Nepálu	Nepál	k1gInSc2	Nepál
<g/>
,	,	kIx,	,
Pákistánu	Pákistán	k1gInSc2	Pákistán
a	a	k8xC	a
Šrí	Šrí	k1gFnSc1	Šrí
Lanky	lanko	k1gNnPc7	lanko
<g/>
,	,	kIx,	,
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnSc2d1	lidová
noviny	novina	k1gFnSc2	novina
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7106-647-8	[number]	k4	80-7106-647-8
Maldives	Maldives	k1gInSc1	Maldives
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gMnPc2	International
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Maldives	Maldives	k1gInSc1	Maldives
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc4	Freedom
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Bureau	Burea	k2eAgFnSc4d1	Burea
of	of	k?	of
South	Southa	k1gFnPc2	Southa
and	and	k?	and
Central	Central	k1gMnSc1	Central
Asian	Asian	k1gMnSc1	Asian
Affairs	Affairs	k1gInSc4	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gInSc1	Note
<g/>
:	:	kIx,	:
Maldives	Maldives	k1gInSc1	Maldives
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Maldives	Maldives	k1gInSc1	Maldives
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
REV	REV	kA	REV
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
26	[number]	k4	26
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Maledivy	Maledivy	k1gFnPc1	Maledivy
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
28	[number]	k4	28
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Maldives	Maldives	k1gInSc1	Maldives
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Maledivy	Maledivy	k1gFnPc1	Maledivy
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Maledivy	Maledivy	k1gFnPc1	Maledivy
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
