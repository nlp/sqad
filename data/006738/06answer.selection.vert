<s>
Roku	rok	k1gInSc2	rok
1881	[number]	k4	1881
(	(	kIx(	(
<g/>
1883	[number]	k4	1883
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
,	,	kIx,	,
1882	[number]	k4	1882
byla	být	k5eAaImAgFnS	být
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgInSc2d1	Karlův
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
část	část	k1gFnSc4	část
českou	český	k2eAgFnSc4d1	Česká
a	a	k8xC	a
německou	německý	k2eAgFnSc4d1	německá
<g/>
,	,	kIx,	,
vznikají	vznikat	k5eAaImIp3nP	vznikat
nová	nový	k2eAgNnPc1d1	nové
nakladatelství	nakladatelství	k1gNnPc1	nakladatelství
(	(	kIx(	(
<g/>
např.	např.	kA	např.
r.	r.	kA	r.
1871	[number]	k4	1871
nakladatelství	nakladatelství	k1gNnPc2	nakladatelství
Jan	Jan	k1gMnSc1	Jan
Otto	Otto	k1gMnSc1	Otto
<g/>
,	,	kIx,	,
r.	r.	kA	r.
1872	[number]	k4	1872
nakladatelství	nakladatelství	k1gNnPc2	nakladatelství
J.	J.	kA	J.
R.	R.	kA	R.
Vilímka	Vilímek	k1gMnSc2	Vilímek
<g/>
)	)	kIx)	)
a	a	k8xC	a
vychází	vycházet	k5eAaImIp3nS	vycházet
množství	množství	k1gNnSc1	množství
almanachů	almanach	k1gInPc2	almanach
(	(	kIx(	(
<g/>
např.	např.	kA	např.
r.	r.	kA	r.
1868	[number]	k4	1868
almanach	almanach	k1gInSc1	almanach
Ruch	ruch	k1gInSc4	ruch
<g/>
,	,	kIx,	,
r.	r.	kA	r.
1871	[number]	k4	1871
Anemonky	anemonka	k1gFnSc2	anemonka
r.	r.	kA	r.
1878	[number]	k4	1878
nový	nový	k2eAgInSc4d1	nový
almanach	almanach	k1gInSc4	almanach
Máj	máj	k1gInSc1	máj
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
