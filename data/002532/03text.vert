<s>
Kaple	kaple	k1gFnSc1	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
je	být	k5eAaImIp3nS	být
barokní	barokní	k2eAgFnSc1d1	barokní
šestiboká	šestiboký	k2eAgFnSc1d1	šestiboká
kaple	kaple	k1gFnSc1	kaple
na	na	k7c6	na
křižovatce	křižovatka	k1gFnSc6	křižovatka
silnic	silnice	k1gFnPc2	silnice
v	v	k7c6	v
zámku	zámek	k1gInSc6	zámek
v	v	k7c6	v
obci	obec	k1gFnSc6	obec
Běstvina	Běstvina	k1gFnSc1	Běstvina
<g/>
.	.	kIx.	.
</s>
<s>
Kaple	kaple	k1gFnSc1	kaple
byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1720	[number]	k4	1720
na	na	k7c6	na
půdorysu	půdorys	k1gInSc6	půdorys
šestiboké	šestiboký	k2eAgFnSc2d1	šestiboká
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
byla	být	k5eAaImAgFnS	být
mylně	mylně	k6eAd1	mylně
připsána	připsán	k2eAgFnSc1d1	připsána
známému	známý	k2eAgMnSc3d1	známý
baroknímu	barokní	k2eAgMnSc3d1	barokní
staviteli	stavitel	k1gMnSc3	stavitel
Janu	Jan	k1gMnSc3	Jan
Blažeji	Blažej	k1gMnSc3	Blažej
Santinimu	Santinim	k1gMnSc3	Santinim
<g/>
.	.	kIx.	.
</s>
<s>
Skutečným	skutečný	k2eAgMnSc7d1	skutečný
tvůrcem	tvůrce	k1gMnSc7	tvůrce
kaple	kaple	k1gFnSc2	kaple
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
Santiniho	Santini	k1gMnSc2	Santini
epigon	epigon	k1gMnSc1	epigon
Jan	Jan	k1gMnSc1	Jan
J.	J.	kA	J.
Vogler	Vogler	k1gMnSc1	Vogler
<g/>
.	.	kIx.	.
</s>
<s>
Kaple	kaple	k1gFnSc1	kaple
je	být	k5eAaImIp3nS	být
zaklenutá	zaklenutý	k2eAgFnSc1d1	zaklenutá
<g/>
,	,	kIx,	,
s	s	k7c7	s
malou	malý	k2eAgFnSc7d1	malá
centrální	centrální	k2eAgFnSc7d1	centrální
věžičkou	věžička	k1gFnSc7	věžička
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
kaple	kaple	k1gFnSc2	kaple
se	se	k3xPyFc4	se
nalézala	nalézat	k5eAaImAgFnS	nalézat
na	na	k7c6	na
šestibokém	šestiboký	k2eAgInSc6d1	šestiboký
podstavci	podstavec	k1gInSc6	podstavec
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
barokní	barokní	k2eAgFnSc1d1	barokní
socha	socha	k1gFnSc1	socha
Apoteóza	apoteóza	k1gFnSc1	apoteóza
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
od	od	k7c2	od
sochaře	sochař	k1gMnSc2	sochař
Ignáce	Ignác	k1gMnSc2	Ignác
Rohrbacha	Rohrbach	k1gMnSc2	Rohrbach
z	z	k7c2	z
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
1726	[number]	k4	1726
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
připisovaná	připisovaný	k2eAgFnSc1d1	připisovaná
Řehoři	Řehoř	k1gMnPc7	Řehoř
Thénymu	Thénym	k1gInSc2	Thénym
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dlouholetém	dlouholetý	k2eAgNnSc6d1	dlouholeté
vystavení	vystavení	k1gNnSc6	vystavení
povětrnostním	povětrnostní	k2eAgFnPc3d1	povětrnostní
podmínkám	podmínka	k1gFnPc3	podmínka
byla	být	k5eAaImAgFnS	být
socha	socha	k1gFnSc1	socha
z	z	k7c2	z
kaple	kaple	k1gFnSc2	kaple
deponována	deponovat	k5eAaBmNgFnS	deponovat
a	a	k8xC	a
následně	následně	k6eAd1	následně
restaurována	restaurován	k2eAgFnSc1d1	restaurována
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
zapůjčena	zapůjčit	k5eAaPmNgNnP	zapůjčit
do	do	k7c2	do
expozice	expozice	k1gFnSc2	expozice
Muzea	muzeum	k1gNnSc2	muzeum
Barokních	barokní	k2eAgFnPc2d1	barokní
soch	socha	k1gFnPc2	socha
v	v	k7c6	v
Chrudimi	Chrudim	k1gFnSc6	Chrudim
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Kaple	kaple	k1gFnSc2	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Kaple	kaple	k1gFnSc1	kaple
Běstvina	Běstvina	k1gFnSc1	Běstvina
(	(	kIx(	(
<g/>
stránky	stránka	k1gFnPc1	stránka
obce	obec	k1gFnPc1	obec
<g/>
)	)	kIx)	)
</s>
