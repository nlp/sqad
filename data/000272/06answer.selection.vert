<s>
Volejte	volat	k5eAaImRp2nP	volat
řediteli	ředitel	k1gMnSc3	ředitel
byl	být	k5eAaImAgInS	být
televizní	televizní	k2eAgInSc1d1	televizní
pořad	pořad	k1gInSc1	pořad
na	na	k7c6	na
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
běžel	běžet	k5eAaImAgMnS	běžet
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
televizi	televize	k1gFnSc4	televize
vedl	vést	k5eAaImAgMnS	vést
Vladimír	Vladimír	k1gMnSc1	Vladimír
Železný	Železný	k1gMnSc1	Železný
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
pořad	pořad	k1gInSc1	pořad
zároveň	zároveň	k6eAd1	zároveň
uváděl	uvádět	k5eAaImAgInS	uvádět
<g/>
.	.	kIx.	.
</s>
