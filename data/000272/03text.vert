<s>
Volejte	volat	k5eAaImRp2nP	volat
řediteli	ředitel	k1gMnSc3	ředitel
byl	být	k5eAaImAgInS	být
televizní	televizní	k2eAgInSc1d1	televizní
pořad	pořad	k1gInSc1	pořad
na	na	k7c6	na
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
běžel	běžet	k5eAaImAgMnS	běžet
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
televizi	televize	k1gFnSc4	televize
vedl	vést	k5eAaImAgMnS	vést
Vladimír	Vladimír	k1gMnSc1	Vladimír
Železný	Železný	k1gMnSc1	Železný
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pořad	pořad	k1gInSc1	pořad
zároveň	zároveň	k6eAd1	zároveň
uváděl	uvádět	k5eAaImAgInS	uvádět
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
pořad	pořad	k1gInSc1	pořad
nazývá	nazývat	k5eAaImIp3nS	nazývat
Volejte	volat	k5eAaImRp2nP	volat
Novu	nova	k1gFnSc4	nova
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
originální	originální	k2eAgInSc4d1	originální
formát	formát	k1gInSc4	formát
pořadu	pořad	k1gInSc2	pořad
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
nemá	mít	k5eNaImIp3nS	mít
ani	ani	k9	ani
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
obdoby	obdoba	k1gFnSc2	obdoba
<g/>
.	.	kIx.	.
</s>
<s>
Železný	Železný	k1gMnSc1	Železný
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
informoval	informovat	k5eAaBmAgMnS	informovat
o	o	k7c6	o
novinkách	novinka	k1gFnPc6	novinka
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
pořadu	pořad	k1gInSc2	pořad
též	též	k9	též
tvořily	tvořit	k5eAaImAgInP	tvořit
jeho	jeho	k3xOp3gNnSc1	jeho
vlastní	vlastní	k2eAgInPc1d1	vlastní
názory	názor	k1gInPc1	názor
na	na	k7c4	na
politiku	politika	k1gFnSc4	politika
a	a	k8xC	a
další	další	k2eAgFnPc4d1	další
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
také	také	k9	také
řešil	řešit	k5eAaImAgMnS	řešit
své	svůj	k3xOyFgInPc4	svůj
vlastní	vlastní	k2eAgInPc4d1	vlastní
spory	spor	k1gInPc4	spor
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Železného	Železný	k1gMnSc2	Železný
výroky	výrok	k1gInPc4	výrok
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
čelila	čelit	k5eAaImAgFnS	čelit
televize	televize	k1gFnSc1	televize
nebo	nebo	k8xC	nebo
i	i	k9	i
samotný	samotný	k2eAgMnSc1d1	samotný
ředitel	ředitel	k1gMnSc1	ředitel
několika	několik	k4yIc7	několik
žalobám	žaloba	k1gFnPc3	žaloba
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
pořadu	pořad	k1gInSc3	pořad
hrozila	hrozit	k5eAaImAgFnS	hrozit
Rada	rada	k1gFnSc1	rada
pro	pro	k7c4	pro
rozhlasové	rozhlasový	k2eAgNnSc4d1	rozhlasové
a	a	k8xC	a
televizní	televizní	k2eAgNnSc4d1	televizní
vysílání	vysílání	k1gNnSc4	vysílání
jistou	jistý	k2eAgFnSc4d1	jistá
dobu	doba	k1gFnSc4	doba
také	také	k9	také
televizi	televize	k1gFnSc6	televize
odebráním	odebrání	k1gNnSc7	odebrání
licence	licence	k1gFnSc2	licence
<g/>
.	.	kIx.	.
</s>
<s>
Řízení	řízení	k1gNnSc1	řízení
bylo	být	k5eAaImAgNnS	být
ale	ale	k9	ale
později	pozdě	k6eAd2	pozdě
zastaveno	zastavit	k5eAaPmNgNnS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
Formát	formát	k1gInSc1	formát
pořadu	pořad	k1gInSc2	pořad
vyvolával	vyvolávat	k5eAaImAgInS	vyvolávat
dojem	dojem	k1gInSc1	dojem
<g/>
,	,	kIx,	,
že	že	k8xS	že
Vladimír	Vladimír	k1gMnSc1	Vladimír
Železný	Železný	k1gMnSc1	Železný
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
na	na	k7c4	na
otázky	otázka	k1gFnPc4	otázka
pokládané	pokládaný	k2eAgFnPc4d1	pokládaná
diváky	divák	k1gMnPc7	divák
v	v	k7c6	v
přímém	přímý	k2eAgInSc6d1	přímý
přenosu	přenos	k1gInSc6	přenos
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
pouze	pouze	k6eAd1	pouze
naaranžované	naaranžovaný	k2eAgNnSc1d1	naaranžované
divadlo	divadlo	k1gNnSc1	divadlo
a	a	k8xC	a
všechny	všechen	k3xTgFnPc4	všechen
otázky	otázka	k1gFnPc4	otázka
a	a	k8xC	a
odpovědi	odpověď	k1gFnPc4	odpověď
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
byly	být	k5eAaImAgFnP	být
předem	předem	k6eAd1	předem
připraveny	připravit	k5eAaPmNgFnP	připravit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Železného	Železný	k1gMnSc2	Železný
z	z	k7c2	z
Novy	nova	k1gFnSc2	nova
byl	být	k5eAaImAgInS	být
pořad	pořad	k1gInSc1	pořad
nahrazen	nahradit	k5eAaPmNgInS	nahradit
již	již	k6eAd1	již
nekontroverzním	kontroverzní	k2eNgInSc7d1	nekontroverzní
Volejte	volat	k5eAaImRp2nP	volat
Novu	nova	k1gFnSc4	nova
<g/>
,	,	kIx,	,
které	který	k3yIgFnSc3	který
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
čistě	čistě	k6eAd1	čistě
pouze	pouze	k6eAd1	pouze
o	o	k7c6	o
věcech	věc	k1gFnPc6	věc
souvisejících	související	k2eAgInPc2d1	související
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
televizní	televizní	k2eAgFnSc7d1	televizní
stanicí	stanice	k1gFnSc7	stanice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roli	role	k1gFnSc6	role
moderátora	moderátor	k1gMnSc2	moderátor
se	se	k3xPyFc4	se
střídají	střídat	k5eAaImIp3nP	střídat
různé	různý	k2eAgFnPc1d1	různá
osobnosti	osobnost	k1gFnPc1	osobnost
televize	televize	k1gFnSc2	televize
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
pořadu	pořad	k1gInSc2	pořad
jsou	být	k5eAaImIp3nP	být
dotazy	dotaz	k1gInPc1	dotaz
diváků	divák	k1gMnPc2	divák
<g/>
,	,	kIx,	,
soutěž	soutěž	k1gFnSc4	soutěž
v	v	k7c6	v
odhadu	odhad	k1gInSc6	odhad
počtu	počet	k1gInSc2	počet
diváků	divák	k1gMnPc2	divák
kteří	který	k3yQgMnPc1	který
budou	být	k5eAaImBp3nP	být
sledovat	sledovat	k5eAaImF	sledovat
vybraný	vybraný	k2eAgInSc1d1	vybraný
pořad	pořad	k1gInSc1	pořad
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
<g/>
,	,	kIx,	,
a	a	k8xC	a
reportáže	reportáž	k1gFnPc1	reportáž
o	o	k7c6	o
filmech	film	k1gInPc6	film
<g/>
,	,	kIx,	,
seriálech	seriál	k1gInPc6	seriál
a	a	k8xC	a
událostech	událost	k1gFnPc6	událost
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
<g/>
.	.	kIx.	.
</s>
<s>
Vladimír	Vladimír	k1gMnSc1	Vladimír
Železný	Železný	k1gMnSc1	Železný
pořad	pořad	k1gInSc4	pořad
Volejte	volat	k5eAaImRp2nP	volat
řediteli	ředitel	k1gMnSc5	ředitel
obnovil	obnovit	k5eAaPmAgInS	obnovit
dne	den	k1gInSc2	den
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2013	[number]	k4	2013
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Volejte	volat	k5eAaImRp2nP	volat
Železnému	železný	k2eAgInSc3d1	železný
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vysílán	vysílat	k5eAaImNgInS	vysílat
na	na	k7c6	na
TV	TV	kA	TV
Barrandov	Barrandov	k1gInSc1	Barrandov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Železný	Železný	k1gMnSc1	Železný
usedl	usednout	k5eAaPmAgMnS	usednout
do	do	k7c2	do
funkce	funkce	k1gFnSc2	funkce
generálního	generální	k2eAgMnSc2d1	generální
ředitele	ředitel	k1gMnSc2	ředitel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgNnSc6	první
odvysílaném	odvysílaný	k2eAgNnSc6d1	odvysílané
díle	dílo	k1gNnSc6	dílo
<g/>
,	,	kIx,	,
sám	sám	k3xTgMnSc1	sám
Železný	Železný	k1gMnSc1	Železný
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
pořad	pořad	k1gInSc1	pořad
znovu	znovu	k6eAd1	znovu
jmenoval	jmenovat	k5eAaImAgInS	jmenovat
Volejte	volat	k5eAaImRp2nP	volat
řediteli	ředitel	k1gMnPc7	ředitel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nedovolují	dovolovat	k5eNaImIp3nP	dovolovat
tomu	ten	k3xDgNnSc3	ten
autorská	autorský	k2eAgNnPc1d1	autorské
práva	právo	k1gNnPc1	právo
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
dodal	dodat	k5eAaPmAgMnS	dodat
<g/>
,	,	kIx,	,
že	že	k8xS	že
TV	TV	kA	TV
Barrandov	Barrandov	k1gInSc1	Barrandov
si	se	k3xPyFc3	se
již	již	k6eAd1	již
požádala	požádat	k5eAaPmAgFnS	požádat
o	o	k7c4	o
odebrání	odebrání	k1gNnSc4	odebrání
autorských	autorský	k2eAgNnPc2d1	autorské
práv	právo	k1gNnPc2	právo
TV	TV	kA	TV
Nova	novum	k1gNnSc2	novum
<g/>
,	,	kIx,	,
důvodem	důvod	k1gInSc7	důvod
uvedl	uvést	k5eAaPmAgMnS	uvést
dlouhodobé	dlouhodobý	k2eAgNnSc1d1	dlouhodobé
neužívání	neužívání	k1gNnSc1	neužívání
značky	značka	k1gFnSc2	značka
a	a	k8xC	a
věří	věřit	k5eAaImIp3nS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
časem	časem	k6eAd1	časem
pořad	pořad	k1gInSc1	pořad
přejmenuje	přejmenovat	k5eAaPmIp3nS	přejmenovat
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
Volejte	volat	k5eAaImRp2nP	volat
řediteli	ředitel	k1gMnSc5	ředitel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Železný	Železný	k1gMnSc1	Železný
řeší	řešit	k5eAaImIp3nS	řešit
mnoho	mnoho	k4c4	mnoho
dalších	další	k2eAgFnPc2d1	další
věcí	věc	k1gFnPc2	věc
než	než	k8xS	než
otázky	otázka	k1gFnPc4	otázka
ohledně	ohledně	k7c2	ohledně
programu	program	k1gInSc2	program
TV	TV	kA	TV
Barrandov	Barrandov	k1gInSc1	Barrandov
<g/>
,	,	kIx,	,
komentuje	komentovat	k5eAaBmIp3nS	komentovat
zde	zde	k6eAd1	zde
i	i	k9	i
aktuální	aktuální	k2eAgFnSc4d1	aktuální
situaci	situace	k1gFnSc4	situace
v	v	k7c6	v
politice	politika	k1gFnSc6	politika
či	či	k8xC	či
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
ke	k	k7c3	k
starým	starý	k2eAgFnPc3d1	stará
událostem	událost	k1gFnPc3	událost
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
Česko	Česko	k1gNnSc1	Česko
a	a	k8xC	a
arbitráž	arbitráž	k1gFnSc1	arbitráž
s	s	k7c7	s
10	[number]	k4	10
miliardami	miliarda	k4xCgFnPc7	miliarda
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
podporuje	podporovat	k5eAaImIp3nS	podporovat
změnu	změna	k1gFnSc4	změna
názvu	název	k1gInSc2	název
z	z	k7c2	z
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
na	na	k7c4	na
pouhé	pouhý	k2eAgNnSc4d1	pouhé
Česko	Česko	k1gNnSc4	Česko
<g/>
,	,	kIx,	,
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
sociální	sociální	k2eAgFnSc4d1	sociální
síť	síť	k1gFnSc4	síť
Facebook	Facebook	k1gInSc4	Facebook
či	či	k8xC	či
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
strana	strana	k1gFnSc1	strana
získala	získat	k5eAaPmAgFnS	získat
jeho	jeho	k3xOp3gInSc4	jeho
hlas	hlas	k1gInSc4	hlas
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
druhého	druhý	k4xOgInSc2	druhý
dílu	díl	k1gInSc2	díl
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
propaguje	propagovat	k5eAaImIp3nS	propagovat
časopis	časopis	k1gInSc1	časopis
Sedmička	sedmička	k1gFnSc1	sedmička
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
pořadu	pořad	k1gInSc2	pořad
Volejte	volat	k5eAaImRp2nP	volat
Novu	nov	k1gInSc2	nov
Záznam	záznam	k1gInSc1	záznam
čtvrtého	čtvrtý	k4xOgInSc2	čtvrtý
dílu	díl	k1gInSc2	díl
pořadu	pořad	k1gInSc2	pořad
Volejte	volat	k5eAaImRp2nP	volat
Železnému	Železný	k1gMnSc3	Železný
Záznamy	záznam	k1gInPc7	záznam
všech	všecek	k3xTgInPc2	všecek
dílů	díl	k1gInPc2	díl
pořadu	pořad	k1gInSc2	pořad
Volejte	volat	k5eAaImRp2nP	volat
Železnému	železný	k2eAgMnSc3d1	železný
</s>
