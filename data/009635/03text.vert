<p>
<s>
Vjačeslav	Vjačeslav	k1gMnSc1	Vjačeslav
Ivanovič	Ivanovič	k1gMnSc1	Ivanovič
Ivanov	Ivanov	k1gInSc1	Ivanov
(	(	kIx(	(
<g/>
rusky	rusky	k6eAd1	rusky
В	В	k?	В
И	И	k?	И
И	И	k?	И
<g/>
;	;	kIx,	;
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1866	[number]	k4	1866
Moskva	Moskva	k1gFnSc1	Moskva
-	-	kIx~	-
16	[number]	k4	16
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1949	[number]	k4	1949
Řím	Řím	k1gInSc1	Řím
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
ruský	ruský	k2eAgMnSc1d1	ruský
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
symbolista	symbolista	k1gMnSc1	symbolista
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
<g/>
,	,	kIx,	,
dramaturg	dramaturg	k1gMnSc1	dramaturg
<g/>
,	,	kIx,	,
literární	literární	k2eAgMnSc1d1	literární
kritik	kritik	k1gMnSc1	kritik
<g/>
,	,	kIx,	,
doktor	doktor	k1gMnSc1	doktor
filosofie	filosofie	k1gFnSc2	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
ideových	ideový	k2eAgMnPc2d1	ideový
inspirátorů	inspirátor	k1gMnPc2	inspirátor
"	"	kIx"	"
<g/>
stříbrného	stříbrný	k2eAgInSc2d1	stříbrný
věku	věk	k1gInSc2	věk
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
a	a	k8xC	a
tvorba	tvorba	k1gFnSc1	tvorba
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Počátky	počátek	k1gInPc1	počátek
tvorby	tvorba	k1gFnSc2	tvorba
===	===	k?	===
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
zeměměřiče	zeměměřič	k1gMnSc2	zeměměřič
Ivana	Ivan	k1gMnSc2	Ivan
Evstichijeviče	Evstichijevič	k1gMnSc2	Evstichijevič
Ivanova	Ivanův	k2eAgMnSc2d1	Ivanův
(	(	kIx(	(
<g/>
1816	[number]	k4	1816
<g/>
-	-	kIx~	-
<g/>
1871	[number]	k4	1871
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
studiích	studie	k1gFnPc6	studie
na	na	k7c6	na
Prvním	první	k4xOgNnSc6	první
moskevském	moskevský	k2eAgNnSc6d1	moskevské
gymnáziu	gymnázium	k1gNnSc6	gymnázium
nejprve	nejprve	k6eAd1	nejprve
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
historicko-filologické	historickoilologický	k2eAgFnSc6d1	historicko-filologický
fakultě	fakulta	k1gFnSc6	fakulta
Moskevské	moskevský	k2eAgFnSc2d1	Moskevská
univerzity	univerzita	k1gFnSc2	univerzita
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
-	-	kIx~	-
<g/>
1886	[number]	k4	1886
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
na	na	k7c6	na
Berlínské	berlínský	k2eAgFnSc6d1	Berlínská
univerzitě	univerzita	k1gFnSc6	univerzita
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
-	-	kIx~	-
<g/>
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
filologii	filologie	k1gFnSc6	filologie
věnoval	věnovat	k5eAaPmAgMnS	věnovat
také	také	k9	také
filosofii	filosofie	k1gFnSc3	filosofie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
jeho	on	k3xPp3gInSc4	on
světonázor	světonázor	k1gInSc4	světonázor
měli	mít	k5eAaImAgMnP	mít
největší	veliký	k2eAgInSc4d3	veliký
vliv	vliv	k1gInSc4	vliv
Friedrich	Friedrich	k1gMnSc1	Friedrich
Nietzsche	Nietzsch	k1gInSc2	Nietzsch
<g/>
,	,	kIx,	,
Vladimir	Vladimir	k1gMnSc1	Vladimir
Solovjov	Solovjovo	k1gNnPc2	Solovjovo
<g/>
,	,	kIx,	,
slavjanofilové	slavjanofil	k1gMnPc1	slavjanofil
a	a	k8xC	a
němečtí	německý	k2eAgMnPc1d1	německý
romantici	romantik	k1gMnPc1	romantik
(	(	kIx(	(
<g/>
Novalis	Novalis	k1gInSc1	Novalis
<g/>
,	,	kIx,	,
Friedrich	Friedrich	k1gMnSc1	Friedrich
Hölderlin	Hölderlin	k1gInSc1	Hölderlin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1891	[number]	k4	1891
mnoho	mnoho	k6eAd1	mnoho
cestoval	cestovat	k5eAaImAgMnS	cestovat
<g/>
.	.	kIx.	.
</s>
<s>
Projel	projet	k5eAaPmAgInS	projet
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
Evropy	Evropa	k1gFnSc2	Evropa
(	(	kIx(	(
<g/>
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
v	v	k7c6	v
Ženevě	Ženeva	k1gFnSc6	Ženeva
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Palestinu	Palestina	k1gFnSc4	Palestina
a	a	k8xC	a
Egypt	Egypt	k1gInSc4	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Žil	žít	k5eAaImAgMnS	žít
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
(	(	kIx(	(
<g/>
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
se	se	k3xPyFc4	se
definitivně	definitivně	k6eAd1	definitivně
přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1893	[number]	k4	1893
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Lidií	Lidie	k1gFnSc7	Lidie
Zinovjevovou-Annibalovou	Zinovjevovou-Annibalův	k2eAgFnSc7d1	Zinovjevovou-Annibalův
<g/>
,	,	kIx,	,
básnířkou	básnířka	k1gFnSc7	básnířka
a	a	k8xC	a
překladatelkou	překladatelka	k1gFnSc7	překladatelka
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
po	po	k7c6	po
pěti	pět	k4xCc6	pět
letech	léto	k1gNnPc6	léto
oženil	oženit	k5eAaPmAgMnS	oženit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInPc4	první
sonety	sonet	k1gInPc4	sonet
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
Ivanov	Ivanov	k1gInSc1	Ivanov
pod	pod	k7c7	pod
silným	silný	k2eAgInSc7d1	silný
vlivem	vliv	k1gInSc7	vliv
poezie	poezie	k1gFnSc2	poezie
katolické	katolický	k2eAgFnSc2d1	katolická
mystiky	mystika	k1gFnSc2	mystika
<g/>
.	.	kIx.	.
</s>
<s>
Sonety	sonet	k1gInPc1	sonet
popisují	popisovat	k5eAaImIp3nP	popisovat
horskou	horský	k2eAgFnSc4d1	horská
přírodu	příroda	k1gFnSc4	příroda
Lombardie	Lombardie	k1gFnSc2	Lombardie
a	a	k8xC	a
Alp	Alpy	k1gFnPc2	Alpy
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
vydání	vydání	k1gNnSc1	vydání
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
k	k	k7c3	k
roku	rok	k1gInSc3	rok
1898	[number]	k4	1898
(	(	kIx(	(
<g/>
velkou	velká	k1gFnSc4	velká
pomoc	pomoc	k1gFnSc1	pomoc
mu	on	k3xPp3gMnSc3	on
tehdy	tehdy	k6eAd1	tehdy
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
Vladimír	Vladimír	k1gMnSc1	Vladimír
Sovoljov	Sovoljov	k1gInSc4	Sovoljov
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yQgInSc7	který
se	se	k3xPyFc4	se
Ivanov	Ivanov	k1gInSc1	Ivanov
seznámil	seznámit	k5eAaPmAgInS	seznámit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1903-1904	[number]	k4	1903-1904
se	se	k3xPyFc4	se
Ivanov	Ivanov	k1gInSc1	Ivanov
seznámil	seznámit	k5eAaPmAgInS	seznámit
s	s	k7c7	s
Valerijem	Valerij	k1gMnSc7	Valerij
Brjusovem	Brjusov	k1gInSc7	Brjusov
<g/>
,	,	kIx,	,
Konstantinem	Konstantin	k1gMnSc7	Konstantin
Balmontem	Balmont	k1gMnSc7	Balmont
<g/>
,	,	kIx,	,
Jurgisem	Jurgis	k1gInSc7	Jurgis
Baltrušajtisem	Baltrušajtis	k1gInSc7	Baltrušajtis
<g/>
,	,	kIx,	,
Zinaidou	Zinaida	k1gFnSc7	Zinaida
Gippiusovou	Gippiusový	k2eAgFnSc7d1	Gippiusový
a	a	k8xC	a
Alexandrem	Alexandr	k1gMnSc7	Alexandr
Blokem	blok	k1gInSc7	blok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literární	literární	k2eAgInSc4d1	literární
salón	salón	k1gInSc4	salón
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1905	[number]	k4	1905
se	se	k3xPyFc4	se
básník	básník	k1gMnSc1	básník
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
do	do	k7c2	do
Petrohradu	Petrohrad	k1gInSc2	Petrohrad
do	do	k7c2	do
bytu	byt	k1gInSc2	byt
v	v	k7c6	v
domě	dům	k1gInSc6	dům
na	na	k7c6	na
rohu	roh	k1gInSc6	roh
Tavričeské	Tavričeský	k2eAgFnSc2d1	Tavričeský
a	a	k8xC	a
Tverské	Tverský	k2eAgFnSc2d1	Tverská
ulice	ulice	k1gFnSc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Byt	byt	k1gInSc4	byt
byl	být	k5eAaImAgInS	být
přezdíván	přezdíván	k2eAgInSc1d1	přezdíván
"	"	kIx"	"
<g/>
věž	věž	k1gFnSc1	věž
<g/>
"	"	kIx"	"
a	a	k8xC	a
každou	každý	k3xTgFnSc4	každý
středu	středa	k1gFnSc4	středa
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
setkávali	setkávat	k5eAaImAgMnP	setkávat
symbolisté	symbolista	k1gMnPc1	symbolista
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
ideových	ideový	k2eAgNnPc2d1	ideové
center	centrum	k1gNnPc2	centrum
ruského	ruský	k2eAgInSc2d1	ruský
symbolismu	symbolismus	k1gInSc2	symbolismus
a	a	k8xC	a
"	"	kIx"	"
<g/>
tvůrčí	tvůrčí	k2eAgMnPc1d1	tvůrčí
laboratoří	laboratoř	k1gFnPc2	laboratoř
<g/>
"	"	kIx"	"
básníků	básník	k1gMnPc2	básník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ivanov	Ivanov	k1gInSc1	Ivanov
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
v	v	k7c6	v
časopisech	časopis	k1gInPc6	časopis
"	"	kIx"	"
<g/>
Váhy	Váhy	k1gFnPc1	Váhy
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Zlaté	zlatý	k2eAgNnSc4d1	Zlaté
rouno	rouno	k1gNnSc4	rouno
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Práce	práce	k1gFnPc1	práce
a	a	k8xC	a
dni	den	k1gInPc1	den
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Apollón	Apollón	k1gMnSc1	Apollón
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Nová	nový	k2eAgFnSc1d1	nová
cesta	cesta	k1gFnSc1	cesta
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vedl	vést	k5eAaImAgMnS	vést
nakladatelství	nakladatelství	k1gNnSc4	nakladatelství
"	"	kIx"	"
<g/>
Křiky	křik	k1gInPc7	křik
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
činný	činný	k2eAgMnSc1d1	činný
v	v	k7c6	v
Petrohradském	petrohradský	k2eAgInSc6d1	petrohradský
nábožensko-filosofickém	náboženskoilosofický	k2eAgInSc6d1	nábožensko-filosofický
spolku	spolek	k1gInSc6	spolek
a	a	k8xC	a
publikoval	publikovat	k5eAaBmAgMnS	publikovat
v	v	k7c6	v
almanachu	almanach	k1gInSc6	almanach
"	"	kIx"	"
<g/>
Severní	severní	k2eAgInPc4d1	severní
květy	květ	k1gInPc4	květ
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Přednášel	přednášet	k5eAaImAgMnS	přednášet
historii	historie	k1gFnSc4	historie
a	a	k8xC	a
starořeckou	starořecký	k2eAgFnSc4d1	starořecká
literaturu	literatura	k1gFnSc4	literatura
na	na	k7c6	na
dívčích	dívčí	k2eAgFnPc6d1	dívčí
vyšších	vysoký	k2eAgFnPc6d2	vyšší
školách	škola	k1gFnPc6	škola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1906	[number]	k4	1906
se	se	k3xPyFc4	se
sblížil	sblížit	k5eAaPmAgMnS	sblížit
se	s	k7c7	s
Sergejem	Sergej	k1gMnSc7	Sergej
Mitrofanovič	Mitrofanovič	k1gInSc4	Mitrofanovič
Gorodeckým	Gorodecký	k2eAgInSc7d1	Gorodecký
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterého	který	k3yQgInSc2	který
načerpal	načerpat	k5eAaPmAgInS	načerpat
nová	nový	k2eAgNnPc4d1	nové
témata	téma	k1gNnPc4	téma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
vydal	vydat	k5eAaPmAgInS	vydat
sborník	sborník	k1gInSc1	sborník
"	"	kIx"	"
<g/>
Eros	Eros	k1gMnSc1	Eros
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velkou	velký	k2eAgFnSc7d1	velká
ránou	rána	k1gFnSc7	rána
pro	pro	k7c4	pro
něj	on	k3xPp3gNnSc4	on
byla	být	k5eAaImAgFnS	být
nečekaná	čekaný	k2eNgFnSc1d1	nečekaná
smrt	smrt	k1gFnSc1	smrt
ženy	žena	k1gFnSc2	žena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1907	[number]	k4	1907
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1912	[number]	k4	1912
si	se	k3xPyFc3	se
vzal	vzít	k5eAaPmAgInS	vzít
svou	svůj	k3xOyFgFnSc4	svůj
nevlastní	vlastní	k2eNgFnSc4d1	nevlastní
dceru	dcera	k1gFnSc4	dcera
Věru	Věra	k1gFnSc4	Věra
Švarsalonovou	Švarsalonová	k1gFnSc4	Švarsalonová
<g/>
,	,	kIx,	,
dceru	dcera	k1gFnSc4	dcera
Zinovjevové-Annibalové	Zinovjevové-Annibalový	k2eAgFnPc1d1	Zinovjevové-Annibalový
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yIgFnSc7	který
měl	mít	k5eAaImAgMnS	mít
syna	syn	k1gMnSc2	syn
Dmitrije	Dmitrije	k1gMnSc1	Dmitrije
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
-	-	kIx~	-
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1907	[number]	k4	1907
<g/>
-	-	kIx~	-
<g/>
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
co	co	k9	co
Ivanov	Ivanov	k1gInSc1	Ivanov
podpořil	podpořit	k5eAaPmAgInS	podpořit
teorii	teorie	k1gFnSc4	teorie
mystického	mystický	k2eAgInSc2d1	mystický
anarchismu	anarchismus	k1gInSc2	anarchismus
Georga	Georg	k1gMnSc2	Georg
Čulkova	Čulkov	k1gInSc2	Čulkov
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
rozešel	rozejít	k5eAaPmAgInS	rozejít
se	s	k7c7	s
staršími	starý	k2eAgMnPc7d2	starší
symbolisty	symbolista	k1gMnPc7	symbolista
(	(	kIx(	(
<g/>
nicméně	nicméně	k8xC	nicméně
přátelit	přátelit	k5eAaImF	přátelit
se	se	k3xPyFc4	se
s	s	k7c7	s
Valerijem	Valerij	k1gMnSc7	Valerij
Brjusovem	Brjusov	k1gInSc7	Brjusov
nepřestal	přestat	k5eNaPmAgInS	přestat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
z	z	k7c2	z
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
Itálie	Itálie	k1gFnSc2	Itálie
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
-	-	kIx~	-
<g/>
1913	[number]	k4	1913
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
Ivanov	Ivanov	k1gInSc1	Ivanov
usadil	usadit	k5eAaPmAgInS	usadit
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
na	na	k7c6	na
Zubovském	Zubovský	k2eAgInSc6d1	Zubovský
bulváru	bulvár	k1gInSc6	bulvár
a	a	k8xC	a
sblížil	sblížit	k5eAaPmAgMnS	sblížit
se	se	k3xPyFc4	se
s	s	k7c7	s
literárním	literární	k2eAgMnSc7d1	literární
kritikem	kritik	k1gMnSc7	kritik
Michailem	Michail	k1gMnSc7	Michail
Geršenzonem	Geršenzon	k1gMnSc7	Geršenzon
<g/>
,	,	kIx,	,
filosofem	filosof	k1gMnSc7	filosof
Sergejem	Sergej	k1gMnSc7	Sergej
Bulgakovem	Bulgakov	k1gInSc7	Bulgakov
<g/>
,	,	kIx,	,
teologem	teolog	k1gMnSc7	teolog
Florenským	Florenský	k2eAgMnSc7d1	Florenský
a	a	k8xC	a
skladatelem	skladatel	k1gMnSc7	skladatel
Alexandrem	Alexandr	k1gMnSc7	Alexandr
Skrjabinem	Skrjabin	k1gMnSc7	Skrjabin
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
S	s	k7c7	s
říjnovou	říjnový	k2eAgFnSc7d1	říjnová
bolševickou	bolševický	k2eAgFnSc7d1	bolševická
revolucí	revoluce	k1gFnSc7	revoluce
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
kulturně-pedagogické	kulturněedagogický	k2eAgFnSc6d1	kulturně-pedagogický
činnosti	činnost	k1gFnSc6	činnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1921-1924	[number]	k4	1921-1924
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
Baku	Baku	k1gNnSc2	Baku
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
přednášel	přednášet	k5eAaImAgMnS	přednášet
na	na	k7c6	na
universitě	universita	k1gFnSc6	universita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
obhájil	obhájit	k5eAaPmAgInS	obhájit
svou	svůj	k3xOyFgFnSc4	svůj
disertační	disertační	k2eAgFnSc4d1	disertační
práci	práce	k1gFnSc4	práce
o	o	k7c6	o
kultu	kult	k1gInSc6	kult
Dionýsa	Dionýsos	k1gMnSc2	Dionýsos
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Emigrace	emigrace	k1gFnSc2	emigrace
===	===	k?	===
</s>
</p>
<p>
<s>
Poslední	poslední	k2eAgNnSc4d1	poslední
desetiletí	desetiletí	k1gNnSc4	desetiletí
prožil	prožít	k5eAaPmAgMnS	prožít
Ivanov	Ivanov	k1gInSc4	Ivanov
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
začal	začít	k5eAaPmAgInS	začít
publikovat	publikovat	k5eAaBmF	publikovat
svá	svůj	k3xOyFgNnPc4	svůj
díla	dílo	k1gNnPc4	dílo
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
"	"	kIx"	"
<g/>
Soudobé	soudobý	k2eAgFnSc2d1	soudobá
zápisky	zápiska	k1gFnSc2	zápiska
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ivanov	Ivanov	k1gInSc1	Ivanov
žil	žít	k5eAaImAgInS	žít
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
osamoceně	osamoceně	k6eAd1	osamoceně
a	a	k8xC	a
udržoval	udržovat	k5eAaImAgInS	udržovat
styky	styk	k1gInPc4	styk
pouze	pouze	k6eAd1	pouze
s	s	k7c7	s
několika	několik	k4yIc7	několik
ruskými	ruský	k2eAgMnPc7d1	ruský
emigranty	emigrant	k1gMnPc7	emigrant
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1926	[number]	k4	1926
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
na	na	k7c4	na
katolickou	katolický	k2eAgFnSc4d1	katolická
víru	víra	k1gFnSc4	víra
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1926-1943	[number]	k4	1926-1943
přednášel	přednášet	k5eAaImAgInS	přednášet
cizí	cizí	k2eAgInPc4d1	cizí
jazyky	jazyk	k1gInPc4	jazyk
a	a	k8xC	a
ruskou	ruský	k2eAgFnSc4d1	ruská
literaturu	literatura	k1gFnSc4	literatura
v	v	k7c6	v
Pávii	pávie	k1gFnSc6	pávie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1941-1949	[number]	k4	1941-1949
tvořil	tvořit	k5eAaImAgInS	tvořit
pro	pro	k7c4	pro
Kongregaci	kongregace	k1gFnSc4	kongregace
pro	pro	k7c4	pro
východní	východní	k2eAgFnPc4d1	východní
církve	církev	k1gFnPc4	církev
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
jeho	jeho	k3xOp3gFnSc2	jeho
literární	literární	k2eAgFnSc2d1	literární
tvorby	tvorba	k1gFnSc2	tvorba
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
sborník	sborník	k1gInSc1	sborník
veršů	verš	k1gInPc2	verš
"	"	kIx"	"
<g/>
Svit	svit	k1gInSc4	svit
večerní	večerní	k2eAgInSc4d1	večerní
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Svět	svět	k1gInSc1	svět
večernij	večernít	k5eAaPmRp2nS	večernít
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
posmrtně	posmrtně	k6eAd1	posmrtně
vydán	vydat	k5eAaPmNgInS	vydat
v	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1962	[number]	k4	1962
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hrob	hrob	k1gInSc1	hrob
Ivanova	Ivanův	k2eAgInSc2d1	Ivanův
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
protestanském	protestanský	k2eAgInSc6d1	protestanský
hřbitově	hřbitov	k1gInSc6	hřbitov
Testaccio	Testaccio	k6eAd1	Testaccio
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tvorba	tvorba	k1gFnSc1	tvorba
a	a	k8xC	a
tvůrčí	tvůrčí	k2eAgInPc1d1	tvůrčí
postupy	postup	k1gInPc1	postup
===	===	k?	===
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
vědeckých	vědecký	k2eAgNnPc6d1	vědecké
dílech	dílo	k1gNnPc6	dílo
se	se	k3xPyFc4	se
Ivanov	Ivanov	k1gInSc1	Ivanov
věnoval	věnovat	k5eAaPmAgInS	věnovat
religiózním	religiózní	k2eAgMnPc3d1	religiózní
mýtům	mýtus	k1gInPc3	mýtus
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
přednášel	přednášet	k5eAaImAgMnS	přednášet
o	o	k7c6	o
religiózním	religiózní	k2eAgInSc6d1	religiózní
kultu	kult	k1gInSc6	kult
Dionýsa	Dionýsos	k1gMnSc2	Dionýsos
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tvůrčí	tvůrčí	k2eAgInPc4d1	tvůrčí
ideály	ideál	k1gInPc4	ideál
hledá	hledat	k5eAaImIp3nS	hledat
Ivanov	Ivanov	k1gInSc1	Ivanov
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
(	(	kIx(	(
<g/>
Antika	antika	k1gFnSc1	antika
<g/>
,	,	kIx,	,
středověk	středověk	k1gInSc1	středověk
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
Byzantská	byzantský	k2eAgFnSc1d1	byzantská
říše	říš	k1gFnSc2	říš
-	-	kIx~	-
jejím	její	k3xOp3gNnSc7	její
uměním	umění	k1gNnSc7	umění
se	se	k3xPyFc4	se
básníci	básník	k1gMnPc1	básník
inspirují	inspirovat	k5eAaBmIp3nP	inspirovat
jen	jen	k9	jen
zřídka	zřídka	k6eAd1	zřídka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
nachází	nacházet	k5eAaImIp3nS	nacházet
skutečný	skutečný	k2eAgInSc4d1	skutečný
symbolismus	symbolismus	k1gInSc4	symbolismus
a	a	k8xC	a
kulturu	kultura	k1gFnSc4	kultura
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
dává	dávat	k5eAaImIp3nS	dávat
do	do	k7c2	do
protikladu	protiklad	k1gInSc2	protiklad
k	k	k7c3	k
buržoazní	buržoazní	k2eAgFnSc3d1	buržoazní
kultuře	kultura	k1gFnSc3	kultura
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
odráží	odrážet	k5eAaImIp3nS	odrážet
tvorba	tvorba	k1gFnSc1	tvorba
starších	starý	k2eAgMnPc2d2	starší
symbolistů	symbolista	k1gMnPc2	symbolista
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
teoretik	teoretik	k1gMnSc1	teoretik
mladších	mladý	k2eAgMnPc2d2	mladší
symbolistů	symbolista	k1gMnPc2	symbolista
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
Ivanov	Ivanov	k1gInSc1	Ivanov
často	často	k6eAd1	často
s	s	k7c7	s
kritikou	kritika	k1gFnSc7	kritika
dekadence	dekadence	k1gFnSc2	dekadence
a	a	k8xC	a
impresionismu	impresionismus	k1gInSc2	impresionismus
v	v	k7c6	v
poezii	poezie	k1gFnSc6	poezie
starších	starý	k2eAgMnPc2d2	starší
symbolistů	symbolista	k1gMnPc2	symbolista
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Ivanova	Ivanův	k2eAgNnSc2d1	Ivanovo
je	být	k5eAaImIp3nS	být
úkolem	úkol	k1gInSc7	úkol
symbolistů	symbolista	k1gMnPc2	symbolista
oproštění	oproštění	k1gNnSc2	oproštění
od	od	k7c2	od
individuálního	individuální	k2eAgNnSc2d1	individuální
<g/>
,	,	kIx,	,
intimního	intimní	k2eAgNnSc2d1	intimní
pojetí	pojetí	k1gNnSc2	pojetí
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
vytvoření	vytvoření	k1gNnSc1	vytvoření
národního	národní	k2eAgNnSc2d1	národní
<g/>
,	,	kIx,	,
syntetického	syntetický	k2eAgNnSc2d1	syntetické
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
zříká	zříkat	k5eAaImIp3nS	zříkat
iluzí	iluze	k1gFnSc7	iluze
a	a	k8xC	a
vyjadřuje	vyjadřovat	k5eAaImIp3nS	vyjadřovat
objektivní	objektivní	k2eAgFnSc4d1	objektivní
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Básník	básník	k1gMnSc1	básník
nesmí	smět	k5eNaImIp3nS	smět
usilovat	usilovat	k5eAaImF	usilovat
o	o	k7c4	o
osamocení	osamocení	k1gNnSc4	osamocení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
povinen	povinen	k2eAgMnSc1d1	povinen
se	se	k3xPyFc4	se
stát	stát	k5eAaPmF	stát
hlasem	hlas	k1gInSc7	hlas
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Ivanov	Ivanov	k1gInSc1	Ivanov
odmítá	odmítat	k5eAaImIp3nS	odmítat
princip	princip	k1gInSc4	princip
parnasismu	parnasismus	k1gInSc2	parnasismus
-	-	kIx~	-
umění	umění	k1gNnSc1	umění
pro	pro	k7c4	pro
umění	umění	k1gNnSc4	umění
a	a	k8xC	a
přechází	přecházet	k5eAaImIp3nS	přecházet
k	k	k7c3	k
religióznímu	religiózní	k2eAgNnSc3d1	religiózní
umění	umění	k1gNnSc3	umění
<g/>
,	,	kIx,	,
realistickému	realistický	k2eAgMnSc3d1	realistický
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
protiváha	protiváha	k1gFnSc1	protiváha
k	k	k7c3	k
idealistickému	idealistický	k2eAgMnSc3d1	idealistický
<g/>
)	)	kIx)	)
symbolismu	symbolismus	k1gInSc3	symbolismus
<g/>
.	.	kIx.	.
</s>
<s>
Metafora	metafora	k1gFnSc1	metafora
<g/>
,	,	kIx,	,
fungující	fungující	k2eAgInSc4d1	fungující
jako	jako	k8xS	jako
základní	základní	k2eAgInSc4d1	základní
poetický	poetický	k2eAgInSc4d1	poetický
prostředek	prostředek	k1gInSc4	prostředek
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
v	v	k7c6	v
lyrice	lyrika	k1gFnSc6	lyrika
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
religiózním	religiózní	k2eAgInSc7d1	religiózní
mýtem	mýtus	k1gInSc7	mýtus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
skutečností	skutečnost	k1gFnSc7	skutečnost
lidského	lidský	k2eAgMnSc2d1	lidský
ducha	duch	k1gMnSc2	duch
<g/>
.	.	kIx.	.
</s>
<s>
Zajistit	zajistit	k5eAaPmF	zajistit
tuto	tento	k3xDgFnSc4	tento
výměnu	výměna	k1gFnSc4	výměna
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
právě	právě	k9	právě
podle	podle	k7c2	podle
Ivanova	Ivanův	k2eAgMnSc2d1	Ivanův
skutečný	skutečný	k2eAgInSc4d1	skutečný
úkol	úkol	k1gInSc4	úkol
symbolisty	symbolista	k1gMnSc2	symbolista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Základní	základní	k2eAgNnPc1d1	základní
témata	téma	k1gNnPc1	téma
jeho	jeho	k3xOp3gFnPc4	jeho
poezie	poezie	k1gFnPc1	poezie
jsou	být	k5eAaImIp3nP	být
smrt	smrt	k1gFnSc4	smrt
a	a	k8xC	a
následné	následný	k2eAgNnSc4d1	následné
obrození	obrození	k1gNnSc4	obrození
<g/>
,	,	kIx,	,
zoufalství	zoufalství	k1gNnSc4	zoufalství
a	a	k8xC	a
za	za	k7c7	za
ním	on	k3xPp3gInSc7	on
přicházející	přicházející	k2eAgFnSc1d1	přicházející
naděje	naděje	k1gFnSc1	naděje
<g/>
.	.	kIx.	.
</s>
<s>
Témata	téma	k1gNnPc1	téma
vzkříšení	vzkříšení	k1gNnSc2	vzkříšení
a	a	k8xC	a
blahoslaveného	blahoslavený	k2eAgNnSc2d1	blahoslavené
obětního	obětní	k2eAgNnSc2d1	obětní
utrpení	utrpení	k1gNnSc2	utrpení
odlišují	odlišovat	k5eAaImIp3nP	odlišovat
tvorbu	tvorba	k1gFnSc4	tvorba
Ivanova	Ivanův	k2eAgNnSc2d1	Ivanovo
od	od	k7c2	od
tvorby	tvorba	k1gFnSc2	tvorba
starších	starý	k2eAgMnPc2d2	starší
symbolistů	symbolista	k1gMnPc2	symbolista
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgFnPc4	který
mělo	mít	k5eAaImAgNnS	mít
důležité	důležitý	k2eAgNnSc1d1	důležité
místo	místo	k1gNnSc1	místo
téma	téma	k1gNnSc1	téma
bezvýchodnosti	bezvýchodnost	k1gFnSc2	bezvýchodnost
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
poezie	poezie	k1gFnSc2	poezie
Ivanova	Ivanův	k2eAgNnSc2d1	Ivanovo
je	být	k5eAaImIp3nS	být
optimistický	optimistický	k2eAgInSc1d1	optimistický
počátek	počátek	k1gInSc1	počátek
<g/>
.	.	kIx.	.
</s>
<s>
Ivanov	Ivanov	k1gInSc1	Ivanov
nesdílí	sdílet	k5eNaImIp3nS	sdílet
pesimismus	pesimismus	k1gInSc4	pesimismus
starších	starý	k2eAgMnPc2d2	starší
symbolistů	symbolista	k1gMnPc2	symbolista
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
vidí	vidět	k5eAaImIp3nS	vidět
zrod	zrod	k1gInSc4	zrod
radostného	radostný	k2eAgInSc2d1	radostný
ANO	ano	k9	ano
<g/>
.	.	kIx.	.
</s>
<s>
Dekadentní	dekadentní	k2eAgNnPc1d1	dekadentní
témata	téma	k1gNnPc1	téma
samoty	samota	k1gFnSc2	samota
a	a	k8xC	a
zoufalství	zoufalství	k1gNnSc2	zoufalství
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
Ivanově	Ivanův	k2eAgFnSc6d1	Ivanova
tvorbě	tvorba	k1gFnSc6	tvorba
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
<g/>
.	.	kIx.	.
</s>
<s>
Přední	přední	k2eAgMnPc1d1	přední
místo	místo	k6eAd1	místo
zaujímají	zaujímat	k5eAaImIp3nP	zaujímat
témata	téma	k1gNnPc1	téma
jako	jako	k8xC	jako
triumf	triumf	k1gInSc1	triumf
"	"	kIx"	"
<g/>
sobornosti	sobornost	k1gFnPc1	sobornost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
hledání	hledání	k1gNnSc4	hledání
Boha	bůh	k1gMnSc2	bůh
<g/>
,	,	kIx,	,
projevení	projevení	k1gNnSc4	projevení
Boží	boží	k2eAgFnSc2d1	boží
přítomnosti	přítomnost	k1gFnSc2	přítomnost
<g/>
,	,	kIx,	,
mystická	mystický	k2eAgFnSc1d1	mystická
láska	láska	k1gFnSc1	láska
<g/>
,	,	kIx,	,
překonání	překonání	k1gNnSc1	překonání
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
impresionistické	impresionistický	k2eAgFnSc2d1	impresionistická
poezie	poezie	k1gFnSc2	poezie
starších	starý	k2eAgMnPc2d2	starší
symbolistů	symbolista	k1gMnPc2	symbolista
je	být	k5eAaImIp3nS	být
poezie	poezie	k1gFnSc1	poezie
Ivanova	Ivanův	k2eAgFnSc1d1	Ivanova
založena	založen	k2eAgFnSc1d1	založena
na	na	k7c6	na
harmonii	harmonie	k1gFnSc6	harmonie
<g/>
,	,	kIx,	,
jednotě	jednota	k1gFnSc6	jednota
a	a	k8xC	a
celistvosti	celistvost	k1gFnSc6	celistvost
<g/>
.	.	kIx.	.
</s>
<s>
Ivanov	Ivanov	k1gInSc1	Ivanov
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
najít	najít	k5eAaPmF	najít
skutečně	skutečně	k6eAd1	skutečně
symbolické	symbolický	k2eAgInPc4d1	symbolický
výrazové	výrazový	k2eAgInPc4d1	výrazový
prostředky	prostředek	k1gInPc4	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Lyrika	lyrika	k1gFnSc1	lyrika
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
často	často	k6eAd1	často
poznamenána	poznamenán	k2eAgFnSc1d1	poznamenána
epikou	epika	k1gFnSc7	epika
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
dramatizovaná	dramatizovaný	k2eAgFnSc1d1	dramatizovaná
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
v	v	k7c4	v
monolog	monolog	k1gInSc4	monolog
nebo	nebo	k8xC	nebo
dramatickou	dramatický	k2eAgFnSc4d1	dramatická
scénu	scéna	k1gFnSc4	scéna
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
poezii	poezie	k1gFnSc4	poezie
Ivanova	Ivanův	k2eAgInSc2d1	Ivanův
není	být	k5eNaImIp3nS	být
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
subjektivní	subjektivní	k2eAgInSc1d1	subjektivní
či	či	k8xC	či
individuální	individuální	k2eAgInSc1d1	individuální
pohled	pohled	k1gInSc1	pohled
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohý	k2eAgInPc1d1	mnohý
verše	verš	k1gInPc1	verš
jsou	být	k5eAaImIp3nP	být
blízké	blízký	k2eAgInPc1d1	blízký
ódám	óda	k1gFnPc3	óda
či	či	k8xC	či
dithyrambám	dithyramba	k1gFnPc3	dithyramba
a	a	k8xC	a
jako	jako	k9	jako
by	by	kYmCp3nP	by
byly	být	k5eAaImAgInP	být
určeny	určit	k5eAaPmNgInP	určit
pro	pro	k7c4	pro
rituální	rituální	k2eAgInPc4d1	rituální
děje	děj	k1gInPc4	děj
či	či	k8xC	či
oslavné	oslavný	k2eAgFnPc4d1	oslavná
deklamace	deklamace	k1gFnPc4	deklamace
<g/>
.	.	kIx.	.
</s>
<s>
Chladná	chladný	k2eAgFnSc1d1	chladná
poezie	poezie	k1gFnSc1	poezie
Ivanova	Ivanův	k2eAgFnSc1d1	Ivanova
je	být	k5eAaImIp3nS	být
bohatá	bohatý	k2eAgFnSc1d1	bohatá
na	na	k7c4	na
anachronismy	anachronismus	k1gInPc4	anachronismus
a	a	k8xC	a
vytříbené	vytříbený	k2eAgFnPc1d1	vytříbená
a	a	k8xC	a
vybroušené	vybroušený	k2eAgFnPc1d1	vybroušená
fráze	fráze	k1gFnPc1	fráze
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poezie	poezie	k1gFnSc1	poezie
Ivanova	Ivanův	k2eAgFnSc1d1	Ivanova
je	být	k5eAaImIp3nS	být
statická	statický	k2eAgFnSc1d1	statická
<g/>
,	,	kIx,	,
předmětná	předmětný	k2eAgFnSc1d1	předmětná
a	a	k8xC	a
majestátní	majestátní	k2eAgFnSc1d1	majestátní
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
zpěvná	zpěvný	k2eAgFnSc1d1	zpěvná
a	a	k8xC	a
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
bohoslužebné	bohoslužebný	k2eAgFnSc6d1	bohoslužebná
nehybnosti	nehybnost	k1gFnSc6	nehybnost
jí	on	k3xPp3gFnSc3	on
chybí	chybit	k5eAaPmIp3nS	chybit
dynamismus	dynamismus	k1gInSc1	dynamismus
<g/>
.	.	kIx.	.
</s>
<s>
Ivanov	Ivanov	k1gInSc1	Ivanov
dává	dávat	k5eAaImIp3nS	dávat
přednost	přednost	k1gFnSc4	přednost
jambu	jamb	k1gInSc2	jamb
a	a	k8xC	a
trocheji	trochej	k1gInSc3	trochej
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vybraná	vybraný	k2eAgNnPc1d1	vybrané
díla	dílo	k1gNnPc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Verše	verš	k1gInPc1	verš
<g/>
,	,	kIx,	,
poémy	poém	k1gInPc1	poém
===	===	k?	===
</s>
</p>
<p>
<s>
Kormčije	Kormčít	k5eAaPmIp3nS	Kormčít
zvjozdy	zvjozda	k1gFnPc4	zvjozda
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Prozračnost	prozračnost	k1gFnSc1	prozračnost
(	(	kIx(	(
<g/>
1904	[number]	k4	1904
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tantal	tantal	k1gInSc1	tantal
(	(	kIx(	(
<g/>
1905	[number]	k4	1905
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Eros	Eros	k1gMnSc1	Eros
(	(	kIx(	(
<g/>
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Cor	Cor	k?	Cor
ardens	ardens	k1gInSc1	ardens
(	(	kIx(	(
<g/>
1911	[number]	k4	1911
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nežnaja	Nežnaja	k1gFnSc1	Nežnaja
tajna	tajna	k?	tajna
(	(	kIx(	(
<g/>
1912	[number]	k4	1912
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Mladenčestvo	Mladenčestvo	k1gNnSc1	Mladenčestvo
(	(	kIx(	(
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Prometej	prometat	k5eAaImRp2nS	prometat
(	(	kIx(	(
<g/>
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Čelověk	Čelověk	k1gInSc1	Čelověk
(	(	kIx(	(
<g/>
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Svět	svět	k1gInSc1	svět
večernij	večernít	k5eAaPmRp2nS	večernít
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Sbírky	sbírka	k1gFnPc1	sbírka
kritických	kritický	k2eAgFnPc2d1	kritická
statí	stať	k1gFnPc2	stať
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
zvjozdam	zvjozdam	k6eAd1	zvjozdam
(	(	kIx(	(
<g/>
1909	[number]	k4	1909
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Borozdy	Borozda	k1gFnPc1	Borozda
i	i	k9	i
meži	meži	k6eAd1	meži
(	(	kIx(	(
<g/>
1916	[number]	k4	1916
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Rodnoje	Rodnoj	k1gInPc1	Rodnoj
i	i	k8xC	i
vsělěnskoje	vsělěnskoj	k1gInPc1	vsělěnskoj
(	(	kIx(	(
<g/>
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Perepiska	Perepiska	k1gFnSc1	Perepiska
iz	iz	k?	iz
dvuch	dvuch	k1gInSc1	dvuch
uglov	uglov	k1gInSc1	uglov
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Dostojewski	Dostojewsk	k1gMnPc1	Dostojewsk
<g/>
,	,	kIx,	,
Tragödie-Mythos-Mystik	Tragödie-Mythos-Mystik	k1gMnSc1	Tragödie-Mythos-Mystik
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
===	===	k?	===
</s>
</p>
<p>
<s>
Subjekt	subjekt	k1gInSc1	subjekt
a	a	k8xC	a
kosmos	kosmos	k1gInSc1	kosmos
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Vjačeslav	Vjačeslav	k1gMnSc1	Vjačeslav
Ivanovič	Ivanovič	k1gMnSc1	Ivanovič
Ivanov	Ivanov	k1gInSc4	Ivanov
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vjačeslav	Vjačeslava	k1gFnPc2	Vjačeslava
Ivanov	Ivanovo	k1gNnPc2	Ivanovo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
