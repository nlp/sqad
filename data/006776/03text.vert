<s>
Svatováclavská	svatováclavský	k2eAgFnSc1d1	Svatováclavská
koruna	koruna	k1gFnSc1	koruna
(	(	kIx(	(
<g/>
Koruna	koruna	k1gFnSc1	koruna
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
česká	český	k2eAgFnSc1d1	Česká
královská	královský	k2eAgFnSc1d1	královská
koruna	koruna	k1gFnSc1	koruna
nejstarší	starý	k2eAgFnSc1d3	nejstarší
a	a	k8xC	a
nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
součástí	součást	k1gFnSc7	součást
českých	český	k2eAgInPc2d1	český
korunovačních	korunovační	k2eAgInPc2d1	korunovační
klenotů	klenot	k1gInPc2	klenot
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
po	po	k7c6	po
zániku	zánik	k1gInSc6	zánik
monarchie	monarchie	k1gFnSc2	monarchie
nestala	stát	k5eNaPmAgFnS	stát
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
oficiálních	oficiální	k2eAgInPc2d1	oficiální
symbolů	symbol	k1gInPc2	symbol
novodobého	novodobý	k2eAgInSc2d1	novodobý
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
těší	těšit	k5eAaImIp3nS	těšit
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
zvláštní	zvláštní	k2eAgFnSc6d1	zvláštní
úctě	úcta	k1gFnSc6	úcta
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zasvěcena	zasvěcen	k2eAgFnSc1d1	zasvěcena
svatému	svatý	k1gMnSc3	svatý
Václavovi	Václav	k1gMnSc3	Václav
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
korunovačním	korunovační	k2eAgNnSc7d1	korunovační
žezlem	žezlo	k1gNnSc7	žezlo
a	a	k8xC	a
jablkem	jablko	k1gNnSc7	jablko
je	být	k5eAaImIp3nS	být
uchovávaná	uchovávaný	k2eAgFnSc1d1	uchovávaná
v	v	k7c6	v
tzv.	tzv.	kA	tzv.
korunní	korunní	k2eAgFnSc6d1	korunní
komoře	komora	k1gFnSc6	komora
nad	nad	k7c7	nad
svatováclavskou	svatováclavský	k2eAgFnSc7d1	Svatováclavská
kaplí	kaple	k1gFnSc7	kaple
katedrály	katedrála	k1gFnSc2	katedrála
svatého	svatý	k2eAgMnSc2d1	svatý
Víta	Vít	k1gMnSc2	Vít
<g/>
,	,	kIx,	,
Václava	Václav	k1gMnSc2	Václav
a	a	k8xC	a
Vojtěcha	Vojtěch	k1gMnSc2	Vojtěch
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Vystavována	vystavován	k2eAgFnSc1d1	vystavována
je	být	k5eAaImIp3nS	být
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
při	při	k7c6	při
významných	významný	k2eAgFnPc6d1	významná
státních	státní	k2eAgFnPc6d1	státní
událostech	událost	k1gFnPc6	událost
<g/>
.	.	kIx.	.
</s>
<s>
Korunu	koruna	k1gFnSc4	koruna
nechal	nechat	k5eAaPmAgMnS	nechat
zhotovit	zhotovit	k5eAaPmF	zhotovit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1346	[number]	k4	1346
ještě	ještě	k6eAd1	ještě
za	za	k7c2	za
života	život	k1gInSc2	život
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
Jana	Jan	k1gMnSc2	Jan
následník	následník	k1gMnSc1	následník
českého	český	k2eAgInSc2d1	český
trůnu	trůn	k1gInSc2	trůn
Karel	Karel	k1gMnSc1	Karel
Lucemburský	lucemburský	k2eAgInSc1d1	lucemburský
<g/>
.	.	kIx.	.
</s>
<s>
Svatováclavská	svatováclavský	k2eAgFnSc1d1	Svatováclavská
koruna	koruna	k1gFnSc1	koruna
měla	mít	k5eAaImAgFnS	mít
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
charakter	charakter	k1gInSc4	charakter
veřejnoprávní	veřejnoprávní	k2eAgInSc4d1	veřejnoprávní
<g/>
.	.	kIx.	.
</s>
<s>
Nebyla	být	k5eNaImAgFnS	být
osobním	osobní	k2eAgInSc7d1	osobní
majetkem	majetek	k1gInSc7	majetek
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
patřila	patřit	k5eAaImAgFnS	patřit
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
jejího	její	k3xOp3gMnSc2	její
nebeského	nebeský	k2eAgMnSc2d1	nebeský
patrona	patron	k1gMnSc2	patron
<g/>
.	.	kIx.	.
</s>
<s>
Mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
užívána	užívat	k5eAaImNgFnS	užívat
jen	jen	k9	jen
při	při	k7c6	při
zcela	zcela	k6eAd1	zcela
výjimečných	výjimečný	k2eAgFnPc6d1	výjimečná
příležitostech	příležitost	k1gFnPc6	příležitost
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
při	při	k7c6	při
korunovacích	korunovace	k1gFnPc6	korunovace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
demonstrovala	demonstrovat	k5eAaBmAgFnS	demonstrovat
spojení	spojení	k1gNnSc4	spojení
krále	král	k1gMnSc2	král
a	a	k8xC	a
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Koruna	koruna	k1gFnSc1	koruna
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
přímo	přímo	k6eAd1	přímo
vtělením	vtělení	k1gNnSc7	vtělení
české	český	k2eAgFnSc2d1	Česká
státnosti	státnost	k1gFnSc2	státnost
a	a	k8xC	a
po	po	k7c4	po
staletí	staletí	k1gNnSc4	staletí
udržovala	udržovat	k5eAaImAgFnS	udržovat
vědomí	vědomí	k1gNnSc4	vědomí
o	o	k7c4	o
svébytnosti	svébytnost	k1gFnPc4	svébytnost
Českého	český	k2eAgNnSc2d1	české
království	království	k1gNnSc2	království
mezi	mezi	k7c7	mezi
ostatními	ostatní	k2eAgInPc7d1	ostatní
evropskými	evropský	k2eAgInPc7d1	evropský
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
též	též	k9	též
symbolem	symbol	k1gInSc7	symbol
společným	společný	k2eAgInSc7d1	společný
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
země	zem	k1gFnPc4	zem
Koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
jednotu	jednota	k1gFnSc4	jednota
ztělesňovala	ztělesňovat	k5eAaImAgFnS	ztělesňovat
<g/>
.	.	kIx.	.
</s>
<s>
Posvátný	posvátný	k2eAgInSc4d1	posvátný
charakter	charakter	k1gInSc4	charakter
koruny	koruna	k1gFnSc2	koruna
umocňovala	umocňovat	k5eAaImAgFnS	umocňovat
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
korunu	koruna	k1gFnSc4	koruna
relikviářovou	relikviářový	k2eAgFnSc4d1	relikviářová
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byl	být	k5eAaImAgInS	být
totiž	totiž	k9	totiž
do	do	k7c2	do
křížku	křížek	k1gInSc2	křížek
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
vrcholu	vrchol	k1gInSc6	vrchol
zasazen	zasazen	k2eAgInSc4d1	zasazen
údajný	údajný	k2eAgInSc4d1	údajný
trn	trn	k1gInSc4	trn
z	z	k7c2	z
Kristovy	Kristův	k2eAgFnSc2d1	Kristova
trnové	trnový	k2eAgFnSc2d1	Trnová
koruny	koruna	k1gFnSc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Český	český	k2eAgMnSc1d1	český
král	král	k1gMnSc1	král
tak	tak	k6eAd1	tak
byl	být	k5eAaImAgMnS	být
symbolicky	symbolicky	k6eAd1	symbolicky
korunován	korunovat	k5eAaBmNgMnS	korunovat
stejnou	stejný	k2eAgFnSc7d1	stejná
korunou	koruna	k1gFnSc7	koruna
jako	jako	k8xS	jako
samotný	samotný	k2eAgMnSc1d1	samotný
Kristus	Kristus	k1gMnSc1	Kristus
<g/>
.	.	kIx.	.
</s>
<s>
Koruna	koruna	k1gFnSc1	koruna
navíc	navíc	k6eAd1	navíc
stála	stát	k5eAaImAgFnS	stát
pod	pod	k7c7	pod
přímou	přímý	k2eAgFnSc7d1	přímá
papežskou	papežský	k2eAgFnSc7d1	Papežská
ochranou	ochrana	k1gFnSc7	ochrana
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
kdokoli	kdokoli	k3yInSc1	kdokoli
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
by	by	kYmCp3nS	by
překročil	překročit	k5eAaPmAgInS	překročit
některé	některý	k3yIgFnPc4	některý
z	z	k7c2	z
ustanovení	ustanovení	k1gNnPc2	ustanovení
vymezujících	vymezující	k2eAgNnPc2d1	vymezující
její	její	k3xOp3gNnPc4	její
užívání	užívání	k1gNnSc4	užívání
<g/>
,	,	kIx,	,
měl	mít	k5eAaImAgMnS	mít
už	už	k6eAd1	už
samým	samý	k3xTgInSc7	samý
činem	čin	k1gInSc7	čin
propadnout	propadnout	k5eAaPmF	propadnout
nejtěžšímu	těžký	k2eAgInSc3d3	nejtěžší
církevnímu	církevní	k2eAgInSc3d1	církevní
trestu	trest	k1gInSc3	trest
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
vyobcování	vyobcování	k1gNnSc1	vyobcování
z	z	k7c2	z
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
České	český	k2eAgNnSc1d1	české
království	království	k1gNnSc1	království
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevilo	objevit	k5eAaPmAgNnS	objevit
na	na	k7c6	na
mapě	mapa	k1gFnSc6	mapa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1085	[number]	k4	1085
<g/>
,	,	kIx,	,
dávno	dávno	k6eAd1	dávno
před	před	k7c7	před
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
než	než	k8xS	než
byla	být	k5eAaImAgFnS	být
zhotovena	zhotoven	k2eAgFnSc1d1	zhotovena
svatováclavská	svatováclavský	k2eAgFnSc1d1	Svatováclavská
koruna	koruna	k1gFnSc1	koruna
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
korunou	koruna	k1gFnSc7	koruna
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
českých	český	k2eAgFnPc6d1	Česká
mincích	mince	k1gFnPc6	mince
(	(	kIx(	(
<g/>
denárech	denár	k1gInPc6	denár
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
pečetích	pečeť	k1gFnPc6	pečeť
vyobrazeni	vyobrazen	k2eAgMnPc1d1	vyobrazen
již	již	k9	již
první	první	k4xOgNnPc1	první
dva	dva	k4xCgMnPc1	dva
čeští	český	k2eAgMnPc1d1	český
králové	král	k1gMnPc1	král
<g/>
,	,	kIx,	,
Vratislav	Vratislav	k1gMnSc1	Vratislav
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
Vladislav	Vladislav	k1gMnSc1	Vladislav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
koruna	koruna	k1gFnSc1	koruna
byla	být	k5eAaImAgFnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
čelenkou	čelenka	k1gFnSc7	čelenka
byzantského	byzantský	k2eAgInSc2d1	byzantský
typu	typ	k1gInSc2	typ
s	s	k7c7	s
přívěsky	přívěsek	k1gInPc7	přívěsek
na	na	k7c4	na
páru	pára	k1gFnSc4	pára
řetízků	řetízek	k1gInPc2	řetízek
<g/>
.	.	kIx.	.
</s>
<s>
Těmto	tento	k3xDgMnPc3	tento
králům	král	k1gMnPc3	král
byla	být	k5eAaImAgFnS	být
koruna	koruna	k1gFnSc1	koruna
propůjčena	propůjčit	k5eAaPmNgFnS	propůjčit
pro	pro	k7c4	pro
výjimečné	výjimečný	k2eAgFnPc4d1	výjimečná
příležitosti	příležitost	k1gFnPc4	příležitost
<g/>
.	.	kIx.	.
</s>
<s>
Dědičný	dědičný	k2eAgInSc1d1	dědičný
královský	královský	k2eAgInSc1d1	královský
titul	titul	k1gInSc1	titul
si	se	k3xPyFc3	se
dokázal	dokázat	k5eAaPmAgInS	dokázat
udržet	udržet	k5eAaPmF	udržet
až	až	k9	až
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
I.	I.	kA	I.
Není	být	k5eNaImIp3nS	být
třeba	třeba	k6eAd1	třeba
pochybovat	pochybovat	k5eAaImF	pochybovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
té	ten	k3xDgFnSc6	ten
příležitosti	příležitost	k1gFnSc6	příležitost
existovala	existovat	k5eAaImAgFnS	existovat
královská	královský	k2eAgFnSc1d1	královská
koruna	koruna	k1gFnSc1	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
vypadala	vypadat	k5eAaImAgFnS	vypadat
přemyslovská	přemyslovský	k2eAgFnSc1d1	Přemyslovská
koruna	koruna	k1gFnSc1	koruna
<g/>
,	,	kIx,	,
svědčí	svědčit	k5eAaImIp3nS	svědčit
vyobrazení	vyobrazení	k1gNnSc1	vyobrazení
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
Přemysla	Přemysl	k1gMnSc2	Přemysl
Otakara	Otakar	k1gMnSc2	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
jeho	jeho	k3xOp3gFnPc1	jeho
majestátní	majestátní	k2eAgFnPc1d1	majestátní
královské	královský	k2eAgFnPc1d1	královská
pečeti	pečeť	k1gFnPc1	pečeť
<g/>
.	.	kIx.	.
</s>
<s>
Měla	mít	k5eAaImAgFnS	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
čelenku	čelenka	k1gFnSc4	čelenka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
přecházela	přecházet	k5eAaImAgFnS	přecházet
ve	v	k7c4	v
stylizované	stylizovaný	k2eAgInPc4d1	stylizovaný
květy	květ	k1gInPc4	květ
lilií	lilie	k1gFnPc2	lilie
<g/>
.	.	kIx.	.
</s>
<s>
Stejnou	stejný	k2eAgFnSc4d1	stejná
podobu	podoba	k1gFnSc4	podoba
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
koruna	koruna	k1gFnSc1	koruna
z	z	k7c2	z
Přemyslových	Přemyslův	k2eAgFnPc2d1	Přemyslova
pohřebních	pohřební	k2eAgFnPc2d1	pohřební
insignií	insignie	k1gFnPc2	insignie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1298	[number]	k4	1298
<g/>
.	.	kIx.	.
</s>
<s>
Novou	nový	k2eAgFnSc4d1	nová
přemyslovskou	přemyslovský	k2eAgFnSc4d1	Přemyslovská
korunu	koruna	k1gFnSc4	koruna
dal	dát	k5eAaPmAgMnS	dát
roku	rok	k1gInSc2	rok
1297	[number]	k4	1297
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
pražské	pražský	k2eAgFnSc3d1	Pražská
korunovaci	korunovace	k1gFnSc3	korunovace
zhotovit	zhotovit	k5eAaPmF	zhotovit
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
a	a	k8xC	a
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
měl	mít	k5eAaImAgInS	mít
zlatý	zlatý	k2eAgInSc1d1	zlatý
meč	meč	k1gInSc1	meč
a	a	k8xC	a
zlatý	zlatý	k2eAgInSc1d1	zlatý
heraldický	heraldický	k2eAgInSc1d1	heraldický
štít	štít	k1gInSc1	štít
<g/>
.	.	kIx.	.
</s>
<s>
Stejnou	stejný	k2eAgFnSc7d1	stejná
korunou	koruna	k1gFnSc7	koruna
byl	být	k5eAaImAgInS	být
korunován	korunován	k2eAgMnSc1d1	korunován
i	i	k9	i
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
<g/>
.	.	kIx.	.
</s>
<s>
Tuto	tento	k3xDgFnSc4	tento
korunu	koruna	k1gFnSc4	koruna
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
daroval	darovat	k5eAaPmAgMnS	darovat
do	do	k7c2	do
dómské	dómský	k2eAgFnSc2d1	Dómská
pokladnice	pokladnice	k1gFnSc2	pokladnice
v	v	k7c6	v
Cáchách	Cáchy	k1gFnPc6	Cáchy
na	na	k7c4	na
bustu	busta	k1gFnSc4	busta
svého	svůj	k3xOyFgMnSc2	svůj
patrona	patron	k1gMnSc2	patron
sv.	sv.	kA	sv.
Karla	Karel	k1gMnSc2	Karel
Velikého	veliký	k2eAgMnSc2d1	veliký
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
sám	sám	k3xTgMnSc1	sám
objednal	objednat	k5eAaPmAgInS	objednat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
druhé	druhý	k4xOgFnSc2	druhý
hypotézy	hypotéza	k1gFnSc2	hypotéza
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
korunu	koruna	k1gFnSc4	koruna
darovanou	darovaný	k2eAgFnSc4d1	darovaná
na	na	k7c4	na
bustu	busta	k1gFnSc4	busta
sv.	sv.	kA	sv.
Zikmunda	Zikmund	k1gMnSc4	Zikmund
do	do	k7c2	do
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
Svatováclavské	svatováclavský	k2eAgFnSc2d1	Svatováclavská
koruny	koruna	k1gFnSc2	koruna
Karla	Karel	k1gMnSc2	Karel
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
inspirován	inspirovat	k5eAaBmNgInS	inspirovat
tvarem	tvar	k1gInSc7	tvar
liliovitými	liliovitý	k2eAgInPc7d1	liliovitý
motivy	motiv	k1gInPc7	motiv
přemyslovské	přemyslovský	k2eAgFnSc2d1	Přemyslovská
koruny	koruna	k1gFnSc2	koruna
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
podobou	podoba	k1gFnSc7	podoba
koruny	koruna	k1gFnSc2	koruna
francouzských	francouzský	k2eAgMnPc2d1	francouzský
králů	král	k1gMnPc2	král
<g/>
.	.	kIx.	.
</s>
<s>
Shodný	shodný	k2eAgInSc4d1	shodný
tvar	tvar	k1gInSc4	tvar
měla	mít	k5eAaImAgFnS	mít
Karlova	Karlův	k2eAgFnSc1d1	Karlova
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
pohřební	pohřební	k2eAgFnSc1d1	pohřební
koruna	koruna	k1gFnSc1	koruna
(	(	kIx(	(
<g/>
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
dochovala	dochovat	k5eAaPmAgFnS	dochovat
ve	v	k7c6	v
fragmentech	fragment	k1gInPc6	fragment
<g/>
)	)	kIx)	)
i	i	k8xC	i
polská	polský	k2eAgFnSc1d1	polská
koruna	koruna	k1gFnSc1	koruna
krále	král	k1gMnSc2	král
Kazimíra	Kazimír	k1gMnSc2	Kazimír
III	III	kA	III
<g/>
.	.	kIx.	.
</s>
<s>
Svatováclavskou	svatováclavský	k2eAgFnSc4d1	Svatováclavská
korunu	koruna	k1gFnSc4	koruna
nechal	nechat	k5eAaPmAgMnS	nechat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1346	[number]	k4	1346
zhotovit	zhotovit	k5eAaPmF	zhotovit
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
korunovaci	korunovace	k1gFnSc4	korunovace
českým	český	k2eAgMnSc7d1	český
králem	král	k1gMnSc7	král
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
existence	existence	k1gFnSc1	existence
je	být	k5eAaImIp3nS	být
poprvé	poprvé	k6eAd1	poprvé
zmíněna	zmínit	k5eAaPmNgFnS	zmínit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1351	[number]	k4	1351
v	v	k7c6	v
ochranném	ochranný	k2eAgInSc6d1	ochranný
dokumentu	dokument	k1gInSc6	dokument
ze	z	k7c2	z
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1346	[number]	k4	1346
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
vystavil	vystavit	k5eAaPmAgMnS	vystavit
papež	papež	k1gMnSc1	papež
Klement	Klement	k1gMnSc1	Klement
VI	VI	kA	VI
<g/>
.	.	kIx.	.
</s>
<s>
Svatováclavská	svatováclavský	k2eAgFnSc1d1	Svatováclavská
koruna	koruna	k1gFnSc1	koruna
měla	mít	k5eAaImAgFnS	mít
podle	podle	k7c2	podle
papežské	papežský	k2eAgFnSc2d1	Papežská
buly	bula	k1gFnSc2	bula
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1346	[number]	k4	1346
spočinout	spočinout	k5eAaPmF	spočinout
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
v	v	k7c6	v
pražském	pražský	k2eAgInSc6d1	pražský
chrámu	chrám	k1gInSc6	chrám
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
<g/>
.	.	kIx.	.
</s>
<s>
Spočívala	spočívat	k5eAaImAgFnS	spočívat
na	na	k7c6	na
lebce	lebka	k1gFnSc6	lebka
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
v	v	k7c6	v
relikviářové	relikviářový	k2eAgFnSc6d1	relikviářová
bustě	busta	k1gFnSc6	busta
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc4	Václav
ze	z	k7c2	z
zlaceného	zlacený	k2eAgNnSc2d1	zlacené
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
dal	dát	k5eAaPmAgInS	dát
zhotovit	zhotovit	k5eAaPmF	zhotovit
již	již	k6eAd1	již
Karlův	Karlův	k2eAgMnSc1d1	Karlův
dědeček	dědeček	k1gMnSc1	dědeček
<g/>
,	,	kIx,	,
král	král	k1gMnSc1	král
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
jejího	její	k3xOp3gNnSc2	její
zasvěcení	zasvěcení	k1gNnSc2	zasvěcení
svatému	svatý	k1gMnSc3	svatý
Václavu	Václav	k1gMnSc3	Václav
se	se	k3xPyFc4	se
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
její	její	k3xOp3gInSc4	její
název	název	k1gInSc4	název
koruna	koruna	k1gFnSc1	koruna
svatého	svatý	k2eAgMnSc2d1	svatý
Václava	Václav	k1gMnSc2	Václav
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
inventáři	inventář	k1gInSc6	inventář
pražské	pražský	k2eAgFnSc2d1	Pražská
katedrály	katedrála	k1gFnSc2	katedrála
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1387	[number]	k4	1387
pod	pod	k7c7	pod
latinskou	latinský	k2eAgFnSc7d1	Latinská
podobou	podoba	k1gFnSc7	podoba
corona	coron	k1gMnSc2	coron
sancti	sancti	k1gNnSc7	sancti
Wenceslai	Wencesla	k1gFnSc2	Wencesla
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
latinských	latinský	k2eAgInPc6d1	latinský
pramenech	pramen	k1gInPc6	pramen
bývala	bývat	k5eAaImAgFnS	bývat
označována	označován	k2eAgFnSc1d1	označována
též	též	k9	též
jako	jako	k8xS	jako
corona	corona	k1gFnSc1	corona
regni	regeň	k1gFnSc3	regeň
Bohemiae	Bohemia	k1gInSc2	Bohemia
(	(	kIx(	(
<g/>
koruna	koruna	k1gFnSc1	koruna
Království	království	k1gNnSc2	království
českého	český	k2eAgNnSc2d1	české
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
samotný	samotný	k2eAgInSc1d1	samotný
český	český	k2eAgInSc1d1	český
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Posvátný	posvátný	k2eAgInSc1d1	posvátný
charakter	charakter	k1gInSc1	charakter
měly	mít	k5eAaImAgInP	mít
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
francouzské	francouzský	k2eAgFnSc2d1	francouzská
královské	královský	k2eAgFnSc2d1	královská
koruny	koruna	k1gFnSc2	koruna
sv.	sv.	kA	sv.
Ludvíka	Ludvík	k1gMnSc2	Ludvík
ty	ten	k3xDgFnPc4	ten
koruny	koruna	k1gFnPc4	koruna
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgFnPc3	jenž
francouzští	francouzský	k2eAgMnPc1d1	francouzský
králové	králová	k1gFnSc2	králová
darovali	darovat	k5eAaPmAgMnP	darovat
trn	trn	k1gInSc4	trn
z	z	k7c2	z
trnové	trnový	k2eAgFnSc2d1	Trnová
koruny	koruna	k1gFnSc2	koruna
Kristovy	Kristův	k2eAgFnSc2d1	Kristova
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
Karlova	Karlův	k2eAgFnSc1d1	Karlova
koruna	koruna	k1gFnSc1	koruna
byla	být	k5eAaImAgFnS	být
korunou	koruna	k1gFnSc7	koruna
relikviářovou	relikviářová	k1gFnSc7	relikviářová
<g/>
.	.	kIx.	.
</s>
<s>
Trn	trn	k1gInSc1	trn
z	z	k7c2	z
Kristovy	Kristův	k2eAgFnSc2d1	Kristova
trnové	trnový	k2eAgFnSc2d1	Trnová
koruny	koruna	k1gFnSc2	koruna
byl	být	k5eAaImAgInS	být
zasazen	zasadit	k5eAaPmNgInS	zasadit
do	do	k7c2	do
křížku	křížek	k1gInSc2	křížek
v	v	k7c6	v
křížení	křížení	k1gNnSc6	křížení
kamar	kamara	k1gFnPc2	kamara
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
absence	absence	k1gFnSc1	absence
byla	být	k5eAaImAgFnS	být
zjištěna	zjistit	k5eAaPmNgFnS	zjistit
teprve	teprve	k6eAd1	teprve
při	při	k7c6	při
posledním	poslední	k2eAgNnSc6d1	poslední
restaurování	restaurování	k1gNnSc6	restaurování
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
králem	král	k1gMnSc7	král
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgMnS	být
korunován	korunován	k2eAgMnSc1d1	korunován
touto	tento	k3xDgFnSc7	tento
korunou	koruna	k1gFnSc7	koruna
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1347	[number]	k4	1347
Karel	Karla	k1gFnPc2	Karla
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
a	a	k8xC	a
posledním	poslední	k2eAgMnSc6d1	poslední
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
V.	V.	kA	V.
Habsbursko-Lotrinský	habsburskootrinský	k2eAgMnSc5d1	habsbursko-lotrinský
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
byla	být	k5eAaImAgFnS	být
uložena	uložit	k5eAaPmNgFnS	uložit
v	v	k7c6	v
pražské	pražský	k2eAgFnSc6d1	Pražská
katedrále	katedrála	k1gFnSc6	katedrála
a	a	k8xC	a
jejím	její	k3xOp3gMnSc7	její
faktickým	faktický	k2eAgMnSc7d1	faktický
správcem	správce	k1gMnSc7	správce
byla	být	k5eAaImAgFnS	být
svatovítská	svatovítský	k2eAgFnSc1d1	Svatovítská
kapitula	kapitula	k1gFnSc1	kapitula
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
stavovské	stavovský	k2eAgFnSc2d1	stavovská
monarchie	monarchie	k1gFnSc2	monarchie
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
na	na	k7c6	na
Karlštejně	Karlštejn	k1gInSc6	Karlštejn
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
byla	být	k5eAaImAgFnS	být
vydávána	vydávat	k5eAaPmNgFnS	vydávat
jen	jen	k9	jen
při	při	k7c6	při
příležitosti	příležitost	k1gFnSc6	příležitost
korunovace	korunovace	k1gFnSc2	korunovace
<g/>
.	.	kIx.	.
</s>
<s>
Samozřejmě	samozřejmě	k6eAd1	samozřejmě
se	se	k3xPyFc4	se
ovšem	ovšem	k9	ovšem
koruně	koruna	k1gFnSc3	koruna
nevyhnul	vyhnout	k5eNaPmAgInS	vyhnout
pohyb	pohyb	k1gInSc1	pohyb
v	v	k7c6	v
době	doba	k1gFnSc6	doba
ohrožení	ohrožení	k1gNnSc2	ohrožení
<g/>
,	,	kIx,	,
její	její	k3xOp3gInSc1	její
pohyb	pohyb	k1gInSc1	pohyb
ovšem	ovšem	k9	ovšem
není	být	k5eNaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
přesně	přesně	k6eAd1	přesně
zmapován	zmapovat	k5eAaPmNgInS	zmapovat
<g/>
.	.	kIx.	.
</s>
<s>
Události	událost	k1gFnPc1	událost
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
přinesly	přinést	k5eAaPmAgFnP	přinést
změny	změna	k1gFnPc1	změna
i	i	k9	i
pro	pro	k7c4	pro
korunovační	korunovační	k2eAgInPc4d1	korunovační
klenoty	klenot	k1gInPc4	klenot
<g/>
,	,	kIx,	,
mnohokrát	mnohokrát	k6eAd1	mnohokrát
změnily	změnit	k5eAaPmAgInP	změnit
místo	místo	k7c2	místo
uložení	uložení	k1gNnSc2	uložení
<g/>
.	.	kIx.	.
</s>
<s>
Ocitly	ocitnout	k5eAaPmAgInP	ocitnout
se	se	k3xPyFc4	se
ve	v	k7c6	v
svatovítském	svatovítský	k2eAgInSc6d1	svatovítský
chrámu	chrám	k1gInSc6	chrám
i	i	k8xC	i
v	v	k7c6	v
úřadě	úřad	k1gInSc6	úřad
Desk	deska	k1gFnPc2	deska
zemských	zemský	k2eAgFnPc2d1	zemská
nebo	nebo	k8xC	nebo
Staroměstské	staroměstský	k2eAgFnSc6d1	Staroměstská
radnici	radnice	k1gFnSc6	radnice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vídni	Vídeň	k1gFnSc6	Vídeň
se	se	k3xPyFc4	se
na	na	k7c6	na
dlouho	dlouho	k6eAd1	dlouho
ocitly	ocitnout	k5eAaPmAgFnP	ocitnout
v	v	k7c6	v
době	doba	k1gFnSc6	doba
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
obávala	obávat	k5eAaImAgFnS	obávat
nevěrnosti	nevěrnost	k1gFnPc4	nevěrnost
českých	český	k2eAgFnPc2d1	Česká
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1791	[number]	k4	1791
král	král	k1gMnSc1	král
Leopold	Leopold	k1gMnSc1	Leopold
II	II	kA	II
<g/>
.	.	kIx.	.
vyhověl	vyhovět	k5eAaPmAgInS	vyhovět
žádostem	žádost	k1gFnPc3	žádost
českých	český	k2eAgInPc2d1	český
stavů	stav	k1gInPc2	stav
o	o	k7c6	o
vrácení	vrácení	k1gNnSc6	vrácení
klenotů	klenot	k1gInPc2	klenot
z	z	k7c2	z
Vídně	Vídeň	k1gFnSc2	Vídeň
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Ať	ať	k9	ať
už	už	k6eAd1	už
byly	být	k5eAaImAgFnP	být
ovšem	ovšem	k9	ovšem
uloženy	uložit	k5eAaPmNgFnP	uložit
kdekoliv	kdekoliv	k6eAd1	kdekoliv
<g/>
,	,	kIx,	,
do	do	k7c2	do
Prahy	Praha	k1gFnSc2	Praha
se	se	k3xPyFc4	se
vracely	vracet	k5eAaImAgInP	vracet
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
korunovace	korunovace	k1gFnSc2	korunovace
<g/>
.	.	kIx.	.
</s>
<s>
Svatováclavská	svatováclavský	k2eAgFnSc1d1	Svatováclavská
koruna	koruna	k1gFnSc1	koruna
je	být	k5eAaImIp3nS	být
penězi	peníze	k1gInPc7	peníze
nevyčíslitelný	vyčíslitelný	k2eNgInSc1d1	nevyčíslitelný
klenot	klenot	k1gInSc4	klenot
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
jablkem	jablko	k1gNnSc7	jablko
<g/>
,	,	kIx,	,
žezlem	žezlo	k1gNnSc7	žezlo
a	a	k8xC	a
ostatními	ostatní	k2eAgFnPc7d1	ostatní
částmi	část	k1gFnPc7	část
korunovačních	korunovační	k2eAgInPc2d1	korunovační
klenotů	klenot	k1gInPc2	klenot
uložen	uložen	k2eAgInSc1d1	uložen
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
<g/>
,	,	kIx,	,
chráněn	chránit	k5eAaImNgInS	chránit
sedmi	sedm	k4xCc7	sedm
zámky	zámek	k1gInPc7	zámek
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgFnPc3	jenž
má	mít	k5eAaImIp3nS	mít
klíč	klíč	k1gInSc4	klíč
sedm	sedm	k4xCc1	sedm
představitelů	představitel	k1gMnPc2	představitel
české	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
státnosti	státnost	k1gFnSc2	státnost
(	(	kIx(	(
<g/>
prezident	prezident	k1gMnSc1	prezident
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
Senátu	senát	k1gInSc2	senát
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
Poslanecké	poslanecký	k2eAgFnSc2d1	Poslanecká
sněmovny	sněmovna	k1gFnSc2	sněmovna
<g/>
,	,	kIx,	,
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
primátor	primátor	k1gMnSc1	primátor
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
pražský	pražský	k2eAgMnSc1d1	pražský
a	a	k8xC	a
probošt	probošt	k1gMnSc1	probošt
Metropolitní	metropolitní	k2eAgFnSc2d1	metropolitní
kapituly	kapitula	k1gFnSc2	kapitula
u	u	k7c2	u
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
tradice	tradice	k1gFnSc1	tradice
sedmi	sedm	k4xCc2	sedm
klíčů	klíč	k1gInPc2	klíč
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
Leopolda	Leopold	k1gMnSc2	Leopold
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
klenoty	klenot	k1gInPc1	klenot
byly	být	k5eAaImAgInP	být
uloženy	uložit	k5eAaPmNgInP	uložit
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
sv.	sv.	kA	sv.
Víta	Vít	k1gMnSc2	Vít
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
v	v	k7c6	v
korunním	korunní	k2eAgInSc6d1	korunní
archivu	archiv	k1gInSc6	archiv
za	za	k7c7	za
Svatováclavskou	svatováclavský	k2eAgFnSc7d1	Svatováclavská
kaplí	kaple	k1gFnSc7	kaple
<g/>
,	,	kIx,	,
za	za	k7c7	za
železnými	železný	k2eAgFnPc7d1	železná
dveřmi	dveře	k1gFnPc7	dveře
s	s	k7c7	s
pěti	pět	k4xCc2	pět
zámky	zámek	k1gInPc7	zámek
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
skříň	skříň	k1gFnSc1	skříň
opatřená	opatřený	k2eAgFnSc1d1	opatřená
dalšími	další	k2eAgInPc7d1	další
dvěma	dva	k4xCgInPc7	dva
zámky	zámek	k1gInPc7	zámek
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgFnPc3	jenž
dostali	dostat	k5eAaPmAgMnP	dostat
klíče	klíč	k1gInPc1	klíč
dva	dva	k4xCgInPc4	dva
strážci	strážce	k1gMnPc1	strážce
klenotů	klenot	k1gInPc2	klenot
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
novodobé	novodobý	k2eAgFnSc2d1	novodobá
pověsti	pověst	k1gFnSc2	pověst
každý	každý	k3xTgMnSc1	každý
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
není	být	k5eNaImIp3nS	být
oprávněn	oprávnit	k5eAaPmNgMnS	oprávnit
korunu	koruna	k1gFnSc4	koruna
nosit	nosit	k5eAaImF	nosit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nasadí	nasadit	k5eAaPmIp3nS	nasadit
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
do	do	k7c2	do
roka	rok	k1gInSc2	rok
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
prohlédl	prohlédnout	k5eAaPmAgMnS	prohlédnout
a	a	k8xC	a
možná	možná	k6eAd1	možná
(	(	kIx(	(
<g/>
důkazy	důkaz	k1gInPc1	důkaz
neexistují	existovat	k5eNaImIp3nP	existovat
<g/>
)	)	kIx)	)
i	i	k8xC	i
nasadil	nasadit	k5eAaPmAgMnS	nasadit
zastupující	zastupující	k2eAgMnSc1d1	zastupující
říšský	říšský	k2eAgMnSc1d1	říšský
protektor	protektor	k1gMnSc1	protektor
Reinhard	Reinhard	k1gMnSc1	Reinhard
Heydrich	Heydrich	k1gMnSc1	Heydrich
<g/>
.	.	kIx.	.
</s>
<s>
Heydrich	Heydrich	k1gMnSc1	Heydrich
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
následky	následek	k1gInPc4	následek
atentátu	atentát	k1gInSc2	atentát
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1942	[number]	k4	1942
<g/>
.	.	kIx.	.
</s>
<s>
Koruna	koruna	k1gFnSc1	koruna
je	být	k5eAaImIp3nS	být
zhotovena	zhotovit	k5eAaPmNgFnS	zhotovit
ze	z	k7c2	z
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
z	z	k7c2	z
21	[number]	k4	21
<g/>
–	–	k?	–
<g/>
22	[number]	k4	22
karátového	karátový	k2eAgInSc2d1	karátový
1	[number]	k4	1
mm	mm	kA	mm
silného	silný	k2eAgInSc2d1	silný
zlatého	zlatý	k2eAgInSc2d1	zlatý
plechu	plech	k1gInSc2	plech
<g/>
.	.	kIx.	.
</s>
<s>
Váží	vážit	k5eAaImIp3nS	vážit
2,358	[number]	k4	2,358
kg	kg	kA	kg
a	a	k8xC	a
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
96	[number]	k4	96
drahokamů	drahokam	k1gInPc2	drahokam
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
také	také	k9	také
jedny	jeden	k4xCgInPc4	jeden
z	z	k7c2	z
největších	veliký	k2eAgInPc2d3	veliký
safírů	safír	k1gInPc2	safír
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Korunu	koruna	k1gFnSc4	koruna
dále	daleko	k6eAd2	daleko
zdobí	zdobit	k5eAaImIp3nS	zdobit
20	[number]	k4	20
perel	perla	k1gFnPc2	perla
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
koruny	koruna	k1gFnSc2	koruna
je	být	k5eAaImIp3nS	být
kříž	kříž	k1gInSc4	kříž
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
prý	prý	k9	prý
uložen	uložen	k2eAgInSc1d1	uložen
trn	trn	k1gInSc1	trn
z	z	k7c2	z
Kristovy	Kristův	k2eAgFnSc2d1	Kristova
trnové	trnový	k2eAgFnSc2d1	Trnová
koruny	koruna	k1gFnSc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Koruna	koruna	k1gFnSc1	koruna
má	mít	k5eAaImIp3nS	mít
průměr	průměr	k1gInSc4	průměr
19	[number]	k4	19
cm	cm	kA	cm
<g/>
,	,	kIx,	,
výška	výška	k1gFnSc1	výška
koruny	koruna	k1gFnSc2	koruna
i	i	k9	i
s	s	k7c7	s
vrcholovým	vrcholový	k2eAgInSc7d1	vrcholový
křížkem	křížek	k1gInSc7	křížek
je	být	k5eAaImIp3nS	být
též	též	k9	též
19	[number]	k4	19
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
ze	z	k7c2	z
čtyř	čtyři	k4xCgInPc2	čtyři
dílů	díl	k1gInPc2	díl
spojených	spojený	k2eAgInPc2d1	spojený
závlačkami	závlačka	k1gFnPc7	závlačka
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
každá	každý	k3xTgFnSc1	každý
je	být	k5eAaImIp3nS	být
zakončena	zakončit	k5eAaPmNgFnS	zakončit
spinelem	spinel	k1gInSc7	spinel
<g/>
.	.	kIx.	.
</s>
<s>
Díly	díl	k1gInPc1	díl
<g/>
,	,	kIx,	,
mající	mající	k2eAgFnSc4d1	mající
podobu	podoba	k1gFnSc4	podoba
stylizovaného	stylizovaný	k2eAgInSc2d1	stylizovaný
květu	květ	k1gInSc2	květ
lilie	lilie	k1gFnSc2	lilie
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
pokryty	pokrýt	k5eAaPmNgInP	pokrýt
drahokamy	drahokam	k1gInPc7	drahokam
<g/>
,	,	kIx,	,
upevněnými	upevněný	k2eAgFnPc7d1	upevněná
v	v	k7c6	v
kornoutovitých	kornoutovitý	k2eAgInPc6d1	kornoutovitý
šatonech	šaton	k1gInPc6	šaton
<g/>
,	,	kIx,	,
asi	asi	k9	asi
2	[number]	k4	2
cm	cm	kA	cm
vysokých	vysoká	k1gFnPc2	vysoká
<g/>
.	.	kIx.	.
</s>
<s>
Kameny	kámen	k1gInPc1	kámen
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
velkých	velký	k2eAgInPc2d1	velký
rozměrů	rozměr	k1gInPc2	rozměr
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
upevněné	upevněný	k2eAgInPc1d1	upevněný
zlatými	zlatý	k2eAgInPc7d1	zlatý
drápky	drápek	k1gInPc7	drápek
<g/>
.	.	kIx.	.
</s>
<s>
Barevné	barevný	k2eAgNnSc1d1	barevné
uspořádání	uspořádání	k1gNnSc1	uspořádání
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
drahokamů	drahokam	k1gInPc2	drahokam
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
symbolický	symbolický	k2eAgInSc4d1	symbolický
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
čelní	čelní	k2eAgFnSc6d1	čelní
a	a	k8xC	a
zadní	zadní	k2eAgFnSc6d1	zadní
části	část	k1gFnSc6	část
koruny	koruna	k1gFnSc2	koruna
převládají	převládat	k5eAaImIp3nP	převládat
kameny	kámen	k1gInPc1	kámen
barvy	barva	k1gFnSc2	barva
červené	červený	k2eAgFnSc2d1	červená
<g/>
,	,	kIx,	,
na	na	k7c6	na
bočních	boční	k2eAgInPc6d1	boční
dílech	díl	k1gInPc6	díl
koruny	koruna	k1gFnSc2	koruna
je	být	k5eAaImIp3nS	být
rozhodující	rozhodující	k2eAgFnSc1d1	rozhodující
barva	barva	k1gFnSc1	barva
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
.	.	kIx.	.
</s>
<s>
Hrot	hrot	k1gInSc1	hrot
každé	každý	k3xTgFnSc2	každý
lilie	lilie	k1gFnSc2	lilie
je	být	k5eAaImIp3nS	být
zakončen	zakončit	k5eAaPmNgInS	zakončit
perlou	perla	k1gFnSc7	perla
<g/>
.	.	kIx.	.
</s>
<s>
Koruna	koruna	k1gFnSc1	koruna
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
spojena	spojit	k5eAaPmNgFnS	spojit
dvěma	dva	k4xCgInPc7	dva
křížícími	křížící	k2eAgInPc7d1	křížící
se	se	k3xPyFc4	se
oblouky	oblouk	k1gInPc1	oblouk
-	-	kIx~	-
kamarami	kamara	k1gFnPc7	kamara
-	-	kIx~	-
na	na	k7c4	na
které	který	k3yQgMnPc4	který
byly	být	k5eAaImAgInP	být
upevněny	upevněn	k2eAgInPc1d1	upevněn
čtyři	čtyři	k4xCgInPc1	čtyři
díly	díl	k1gInPc1	díl
jiné	jiný	k2eAgFnSc2d1	jiná
čelenky	čelenka	k1gFnSc2	čelenka
<g/>
,	,	kIx,	,
pokryté	pokrytý	k2eAgInPc4d1	pokrytý
smaragdy	smaragd	k1gInPc7	smaragd
<g/>
,	,	kIx,	,
spinely	spinel	k1gInPc7	spinel
a	a	k8xC	a
perlami	perla	k1gFnPc7	perla
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
vzácný	vzácný	k2eAgMnSc1d1	vzácný
je	být	k5eAaImIp3nS	být
srdčitý	srdčitý	k2eAgInSc1d1	srdčitý
spinel	spinel	k1gInSc1	spinel
zdobící	zdobící	k2eAgFnSc2d1	zdobící
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
lilií	lilie	k1gFnPc2	lilie
koruny	koruna	k1gFnSc2	koruna
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
největší	veliký	k2eAgInPc4d3	veliký
drahokamové	drahokamový	k2eAgInPc4d1	drahokamový
spinely	spinel	k1gInPc4	spinel
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
kategorie	kategorie	k1gFnPc4	kategorie
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
timúr	timúr	k1gMnSc1	timúr
ruby	rub	k1gInPc4	rub
<g/>
,	,	kIx,	,
timúrův	timúrův	k2eAgInSc4d1	timúrův
rubínspinel	rubínspinel	k1gInSc4	rubínspinel
britských	britský	k2eAgInPc2d1	britský
korunovačních	korunovační	k2eAgInPc2d1	korunovační
klenotů	klenot	k1gInPc2	klenot
<g/>
.	.	kIx.	.
</s>
<s>
Spinely	spinel	k1gInPc1	spinel
zdobící	zdobící	k2eAgFnSc4d1	zdobící
korunu	koruna	k1gFnSc4	koruna
pocházejí	pocházet	k5eAaImIp3nP	pocházet
nejspíše	nejspíše	k9	nejspíše
(	(	kIx(	(
<g/>
J.	J.	kA	J.
Hyršl	Hyršl	k1gInSc1	Hyršl
<g/>
,	,	kIx,	,
P.	P.	kA	P.
Klásek	klásek	k1gInSc1	klásek
<g/>
)	)	kIx)	)
z	z	k7c2	z
Tádžikistánu	Tádžikistán	k1gInSc2	Tádžikistán
<g/>
,	,	kIx,	,
proslulého	proslulý	k2eAgNnSc2d1	proslulé
starověkého	starověký	k2eAgNnSc2d1	starověké
naleziště	naleziště	k1gNnSc2	naleziště
u	u	k7c2	u
řeky	řeka	k1gFnSc2	řeka
Pjandž	Pjandž	k1gFnSc1	Pjandž
(	(	kIx(	(
<g/>
Pamir	Pamir	k1gInSc1	Pamir
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spinel	spinel	k1gInSc1	spinel
růžové	růžový	k2eAgFnSc2d1	růžová
<g/>
,	,	kIx,	,
vínové	vínový	k2eAgFnSc2d1	vínová
a	a	k8xC	a
rubínové	rubínový	k2eAgFnSc2d1	rubínová
barvy	barva	k1gFnSc2	barva
byl	být	k5eAaImAgInS	být
kamenem	kámen	k1gInSc7	kámen
dhramy	dhram	k1gInPc1	dhram
(	(	kIx(	(
<g/>
řádu	řád	k1gInSc2	řád
a	a	k8xC	a
životní	životní	k2eAgFnSc2d1	životní
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
osvícení	osvícení	k1gNnSc2	osvícení
<g/>
)	)	kIx)	)
v	v	k7c6	v
buddhismu	buddhismus	k1gInSc6	buddhismus
mahájána	maháján	k1gMnSc2	maháján
a	a	k8xC	a
vadžrajána	vadžraján	k1gMnSc2	vadžraján
(	(	kIx(	(
<g/>
Bimaránský	Bimaránský	k2eAgInSc1d1	Bimaránský
relikviář	relikviář	k1gInSc1	relikviář
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
kámen	kámen	k1gInSc1	kámen
muslimského	muslimský	k2eAgInSc2d1	muslimský
ráje	ráj	k1gInSc2	ráj
si	se	k3xPyFc3	se
ho	on	k3xPp3gNnSc4	on
oblíbili	oblíbit	k5eAaPmAgMnP	oblíbit
Peršané	Peršan	k1gMnPc1	Peršan
a	a	k8xC	a
Arabové	Arab	k1gMnPc1	Arab
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
křesťanskou	křesťanský	k2eAgFnSc4d1	křesťanská
Evropu	Evropa	k1gFnSc4	Evropa
to	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
drahokam	drahokam	k1gInSc1	drahokam
Krista	Krista	k1gFnSc1	Krista
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc2	jeho
oběti	oběť	k1gFnSc2	oběť
<g/>
,	,	kIx,	,
krve	krev	k1gFnSc2	krev
<g/>
,	,	kIx,	,
vzkříšení	vzkříšení	k1gNnSc2	vzkříšení
a	a	k8xC	a
věčného	věčný	k2eAgInSc2d1	věčný
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Indů	Ind	k1gMnPc2	Ind
ho	on	k3xPp3gNnSc4	on
Evropané	Evropan	k1gMnPc1	Evropan
dlouho	dlouho	k6eAd1	dlouho
nerozlišovali	rozlišovat	k5eNaImAgMnP	rozlišovat
od	od	k7c2	od
rubínu	rubín	k1gInSc2	rubín
dováženého	dovážený	k2eAgNnSc2d1	dovážené
arabskými	arabský	k2eAgMnPc7d1	arabský
obchodníky	obchodník	k1gMnPc7	obchodník
ze	z	k7c2	z
Srí	Srí	k1gFnPc2	Srí
lanky	lanko	k1gNnPc7	lanko
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Pamiru	Pamir	k1gInSc2	Pamir
nebo	nebo	k8xC	nebo
Hindukúše	Hindukúše	k1gFnSc2	Hindukúše
asi	asi	k9	asi
také	také	k9	také
pochází	pocházet	k5eAaImIp3nS	pocházet
údajný	údajný	k2eAgInSc1d1	údajný
turmalín	turmalín	k1gInSc1	turmalín
(	(	kIx(	(
<g/>
rubelit	rubelit	k1gInSc1	rubelit
<g/>
)	)	kIx)	)
na	na	k7c6	na
čelence	čelenka	k1gFnSc6	čelenka
koruny	koruna	k1gFnSc2	koruna
(	(	kIx(	(
<g/>
Hyršl	Hyršl	k1gInSc1	Hyršl
<g/>
,	,	kIx,	,
Malíková	Malíková	k1gFnSc1	Malíková
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ivo	Ivo	k1gMnSc1	Ivo
Hlobil	Hlobil	k1gMnSc1	Hlobil
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
,	,	kIx,	,
České	český	k2eAgInPc1d1	český
královské	královský	k2eAgInPc1d1	královský
korunovační	korunovační	k2eAgInPc1d1	korunovační
klenoty	klenot	k1gInPc1	klenot
<g/>
,	,	kIx,	,
Správa	správa	k1gFnSc1	správa
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Karel	Karel	k1gMnSc1	Karel
Otavský	otavský	k2eAgMnSc1d1	otavský
<g/>
,	,	kIx,	,
Sankt	Sankt	k1gInSc1	Sankt
Wenzelskrone	Wenzelskron	k1gInSc5	Wenzelskron
im	im	k?	im
Prager	Prager	k1gMnSc1	Prager
Domschatz	Domschatz	k1gMnSc1	Domschatz
und	und	k?	und
die	die	k?	die
frage	fragat	k5eAaPmIp3nS	fragat
der	drát	k5eAaImRp2nS	drát
kunstauffassung	kunstauffassung	k1gInSc1	kunstauffassung
am	am	k?	am
Hofe	Hofe	k1gInSc1	Hofe
Kaiser	Kaiser	k1gMnSc1	Kaiser
Karls	Karlsa	k1gFnPc2	Karlsa
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Reihe	Reihus	k1gMnSc5	Reihus
Kunstgeschichte	Kunstgeschicht	k1gMnSc5	Kunstgeschicht
XXVIII	XXVIII	kA	XXVIII
<g/>
,	,	kIx,	,
Band	band	k1gInSc1	band
142	[number]	k4	142
<g/>
.	.	kIx.	.
</s>
<s>
Peter	Peter	k1gMnSc1	Peter
Lang	Lang	k1gMnSc1	Lang
Bern-Frankfurt	Bern-Frankfurt	k1gInSc1	Bern-Frankfurt
a.	a.	k?	a.
<g/>
M.	M.	kA	M.
<g/>
-New	-New	k?	-New
York-Paris-Wien	York-Paris-Wien	k1gInSc1	York-Paris-Wien
1992	[number]	k4	1992
CHYTIL	Chytil	k1gMnSc1	Chytil
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
;	;	kIx,	;
VRBA	Vrba	k1gMnSc1	Vrba
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
;	;	kIx,	;
PODLAHA	podlaha	k1gFnSc1	podlaha
<g/>
,	,	kIx,	,
Antonín	Antonín	k1gMnSc1	Antonín
<g/>
.	.	kIx.	.
</s>
<s>
Korunovační	korunovační	k2eAgInPc4d1	korunovační
klenoty	klenot	k1gInPc4	klenot
království	království	k1gNnSc2	království
Českého	český	k2eAgNnSc2d1	české
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Archeologická	archeologický	k2eAgFnSc1d1	archeologická
komise	komise	k1gFnSc1	komise
při	při	k7c6	při
České	český	k2eAgFnSc6d1	Česká
Akademii	akademie	k1gFnSc6	akademie
císaře	císař	k1gMnSc2	císař
Františka	František	k1gMnSc2	František
Josefa	Josef	k1gMnSc2	Josef
pro	pro	k7c4	pro
vědy	věda	k1gFnPc4	věda
<g/>
,	,	kIx,	,
1912	[number]	k4	1912
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
Anna	Anna	k1gFnSc1	Anna
Bauerová	Bauerová	k1gFnSc1	Bauerová
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Bauer	Bauer	k1gMnSc1	Bauer
<g/>
,	,	kIx,	,
Tajemství	tajemství	k1gNnSc1	tajemství
chrámových	chrámový	k2eAgInPc2d1	chrámový
pokladů	poklad	k1gInPc2	poklad
<g/>
,	,	kIx,	,
ed	ed	k?	ed
<g/>
.	.	kIx.	.
</s>
<s>
Papyrus	papyrus	k1gInSc1	papyrus
a	a	k8xC	a
Jeva	Jeva	k1gFnSc1	Jeva
<g/>
,	,	kIx,	,
Rudná	rudný	k2eAgFnSc1d1	Rudná
u	u	k7c2	u
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
1993	[number]	k4	1993
Josef	Josef	k1gMnSc1	Josef
Cibulka	Cibulka	k1gMnSc1	Cibulka
<g/>
,	,	kIx,	,
Český	český	k2eAgInSc1d1	český
řád	řád	k1gInSc1	řád
korunovační	korunovační	k2eAgInSc1d1	korunovační
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
původ	původ	k1gInSc1	původ
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1934	[number]	k4	1934
Josef	Josef	k1gMnSc1	Josef
Cibulka	Cibulka	k1gMnSc1	Cibulka
<g/>
,	,	kIx,	,
Korunovační	korunovační	k2eAgInPc4d1	korunovační
klenoty	klenot	k1gInPc4	klenot
Království	království	k1gNnSc2	království
českého	český	k2eAgNnSc2d1	české
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1969	[number]	k4	1969
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Hyršl	Hyršl	k1gMnSc1	Hyršl
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgNnSc1d1	Nové
určení	určení	k1gNnSc1	určení
drahokamů	drahokam	k1gInPc2	drahokam
Svatováclavské	svatováclavský	k2eAgFnSc2d1	Svatováclavská
koruny	koruna	k1gFnSc2	koruna
<g/>
,	,	kIx,	,
časopis	časopis	k1gInSc1	časopis
Vesmír	vesmír	k1gInSc1	vesmír
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Kašpar	Kašpar	k1gMnSc1	Kašpar
<g/>
,	,	kIx,	,
O	o	k7c6	o
drahokamech	drahokam	k1gInPc6	drahokam
Svatováclavské	svatováclavský	k2eAgFnSc2d1	Svatováclavská
koruny	koruna	k1gFnSc2	koruna
<g/>
,	,	kIx,	,
Turnov	Turnov	k1gInSc1	Turnov
1947	[number]	k4	1947
R.	R.	kA	R.
Nový	nový	k2eAgMnSc1d1	nový
<g/>
,	,	kIx,	,
Použití	použití	k1gNnSc1	použití
drahých	drahý	k2eAgInPc2d1	drahý
nerostů	nerost	k1gInPc2	nerost
a	a	k8xC	a
kovů	kov	k1gInPc2	kov
v	v	k7c6	v
ájurvédské	ájurvédský	k2eAgFnSc6d1	ájurvédská
a	a	k8xC	a
tantrické	tantrický	k2eAgFnSc6d1	tantrická
medicíně	medicína	k1gFnSc6	medicína
<g/>
,	,	kIx,	,
in	in	k?	in
Hornická	hornický	k2eAgFnSc1d1	hornická
Příbram	Příbram	k1gFnSc1	Příbram
ve	v	k7c6	v
vědě	věda	k1gFnSc6	věda
a	a	k8xC	a
technice	technika	k1gFnSc6	technika
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Příbram	Příbram	k1gFnSc1	Příbram
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
oceněno	ocenit	k5eAaPmNgNnS	ocenit
bronzovou	bronzový	k2eAgFnSc7d1	bronzová
medailí	medaile	k1gFnSc7	medaile
Jiří	Jiří	k1gMnSc1	Jiří
Kouřimský	kouřimský	k2eAgMnSc1d1	kouřimský
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Bouška	Bouška	k1gMnSc1	Bouška
<g/>
,	,	kIx,	,
Drahé	drahý	k2eAgInPc1d1	drahý
kameny	kámen	k1gInPc1	kámen
kolem	kolem	k7c2	kolem
nás	my	k3xPp1nPc2	my
<g/>
,	,	kIx,	,
SPN	SPN	kA	SPN
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1983	[number]	k4	1983
V.	V.	kA	V.
Sobolevskij	Sobolevskij	k1gFnPc2	Sobolevskij
<g/>
,	,	kIx,	,
V.	V.	kA	V.
Bouška	Bouška	k1gMnSc1	Bouška
<g/>
,	,	kIx,	,
Klenoty	klenot	k1gInPc1	klenot
přírody	příroda	k1gFnSc2	příroda
<g/>
,	,	kIx,	,
Lidové	lidový	k2eAgNnSc1d1	lidové
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Svatováclavská	svatováclavský	k2eAgFnSc1d1	Svatováclavská
koruna	koruna	k1gFnSc1	koruna
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Svatováclavská	svatováclavský	k2eAgFnSc1d1	Svatováclavská
koruna	koruna	k1gFnSc1	koruna
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Hradu	hrad	k1gInSc2	hrad
České	český	k2eAgInPc1d1	český
korunovační	korunovační	k2eAgInPc1d1	korunovační
klenoty	klenot	k1gInPc1	klenot
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Hradu	hrad	k1gInSc2	hrad
</s>
