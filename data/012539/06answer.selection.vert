<s>
První	první	k4xOgFnSc1	první
pražská	pražský	k2eAgFnSc1d1	Pražská
defenestrace	defenestrace	k1gFnSc1	defenestrace
je	být	k5eAaImIp3nS	být
událost	událost	k1gFnSc4	událost
z	z	k7c2	z
neděle	neděle	k1gFnSc2	neděle
30	[number]	k4	30
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1419	[number]	k4	1419
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byli	být	k5eAaImAgMnP	být
protihusitští	protihusitský	k2eAgMnPc1d1	protihusitský
novoměstští	novoměstský	k2eAgMnPc1d1	novoměstský
radní	radní	k1gMnPc1	radní
zabiti	zabít	k5eAaPmNgMnP	zabít
svržením	svržení	k1gNnPc3	svržení
z	z	k7c2	z
okna	okno	k1gNnSc2	okno
radnice	radnice	k1gFnSc2	radnice
<g/>
.	.	kIx.	.
</s>
