<p>
<s>
První	první	k4xOgFnSc1	první
pražská	pražský	k2eAgFnSc1d1	Pražská
defenestrace	defenestrace	k1gFnSc1	defenestrace
je	být	k5eAaImIp3nS	být
událost	událost	k1gFnSc4	událost
z	z	k7c2	z
neděle	neděle	k1gFnSc2	neděle
30	[number]	k4	30
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1419	[number]	k4	1419
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byli	být	k5eAaImAgMnP	být
protihusitští	protihusitský	k2eAgMnPc1d1	protihusitský
novoměstští	novoměstský	k2eAgMnPc1d1	novoměstský
radní	radní	k1gMnPc1	radní
zabiti	zabít	k5eAaPmNgMnP	zabít
svržením	svržení	k1gNnPc3	svržení
z	z	k7c2	z
okna	okno	k1gNnSc2	okno
radnice	radnice	k1gFnSc2	radnice
<g/>
.	.	kIx.	.
</s>
<s>
Událost	událost	k1gFnSc1	událost
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
počátek	počátek	k1gInSc4	počátek
husitských	husitský	k2eAgFnPc2d1	husitská
válek	válka	k1gFnPc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Účastníci	účastník	k1gMnPc1	účastník
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
shromáždili	shromáždit	k5eAaPmAgMnP	shromáždit
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Sněžné	sněžný	k2eAgFnSc2d1	sněžná
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
kázal	kázat	k5eAaImAgMnS	kázat
kněz	kněz	k1gMnSc1	kněz
a	a	k8xC	a
pozdější	pozdní	k2eAgMnSc1d2	pozdější
vůdce	vůdce	k1gMnSc1	vůdce
pražských	pražský	k2eAgInPc2d1	pražský
radikálů	radikál	k1gMnPc2	radikál
Jan	Jan	k1gMnSc1	Jan
Želivský	želivský	k2eAgMnSc1d1	želivský
<g/>
.	.	kIx.	.
</s>
<s>
Násilný	násilný	k2eAgInSc1d1	násilný
převrat	převrat	k1gInSc1	převrat
byl	být	k5eAaImAgInS	být
zřejmě	zřejmě	k6eAd1	zřejmě
předem	předem	k6eAd1	předem
plánován	plánovat	k5eAaImNgInS	plánovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
mnoho	mnoho	k4c1	mnoho
stoupenců	stoupenec	k1gMnPc2	stoupenec
Želivského	želivský	k2eAgInSc2d1	želivský
dostavilo	dostavit	k5eAaPmAgNnS	dostavit
ozbrojeno	ozbrojit	k5eAaPmNgNnS	ozbrojit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
kázání	kázání	k1gNnSc6	kázání
se	se	k3xPyFc4	se
jím	jíst	k5eAaImIp1nS	jíst
vedený	vedený	k2eAgInSc1d1	vedený
dav	dav	k1gInSc1	dav
odebral	odebrat	k5eAaPmAgInS	odebrat
před	před	k7c4	před
nedalekou	daleký	k2eNgFnSc4d1	nedaleká
Novoměstskou	novoměstský	k2eAgFnSc4d1	Novoměstská
radnici	radnice	k1gFnSc4	radnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c6	na
radních	radní	k1gFnPc6	radní
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
purkmistra	purkmistr	k1gMnSc2	purkmistr
Jana	Jan	k1gMnSc2	Jan
Podivínského	podivínský	k2eAgMnSc2d1	podivínský
požadoval	požadovat	k5eAaImAgMnS	požadovat
propuštění	propuštění	k1gNnSc3	propuštění
vězněných	vězněný	k2eAgMnPc2d1	vězněný
stoupenců	stoupenec	k1gMnPc2	stoupenec
přijímání	přijímání	k1gNnSc2	přijímání
podobojí	podobojí	k2eAgInPc1d1	podobojí
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
konšelé	konšel	k1gMnPc1	konšel
byli	být	k5eAaImAgMnP	být
po	po	k7c6	po
útoku	útok	k1gInSc6	útok
na	na	k7c6	na
radnici	radnice	k1gFnSc6	radnice
vyhozeni	vyhodit	k5eAaPmNgMnP	vyhodit
okny	okno	k1gNnPc7	okno
a	a	k8xC	a
krutě	krutě	k6eAd1	krutě
ubiti	ubit	k2eAgMnPc1d1	ubit
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
svědectví	svědectví	k1gNnSc2	svědectví
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
z	z	k7c2	z
Březové	březový	k2eAgFnSc2d1	Březová
se	se	k3xPyFc4	se
útoku	útok	k1gInSc3	útok
na	na	k7c4	na
radnici	radnice	k1gFnSc4	radnice
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Předehra	předehra	k1gFnSc1	předehra
1415	[number]	k4	1415
<g/>
–	–	k?	–
<g/>
1419	[number]	k4	1419
(	(	kIx(	(
<g/>
dle	dle	k7c2	dle
P.	P.	kA	P.
Čorneje	Čornej	k1gInSc2	Čornej
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
6	[number]	k4	6
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
1415	[number]	k4	1415
-	-	kIx~	-
10	[number]	k4	10
<g/>
.	.	kIx.	.
leden	leden	k1gInSc1	leden
1417	[number]	k4	1417
(	(	kIx(	(
<g/>
expozice	expozice	k1gFnSc2	expozice
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Upálení	upálení	k1gNnSc1	upálení
Husa	Hus	k1gMnSc2	Hus
v	v	k7c6	v
Kostnici	Kostnice	k1gFnSc6	Kostnice
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1415	[number]	k4	1415
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
velkou	velký	k2eAgFnSc7d1	velká
ranou	rána	k1gFnSc7	rána
všem	všecek	k3xTgMnPc3	všecek
příznivcům	příznivec	k1gMnPc3	příznivec
jeho	jeho	k3xOp3gFnPc2	jeho
myšlenek	myšlenka	k1gFnPc2	myšlenka
<g/>
,	,	kIx,	,
a	a	k8xC	a
prohloubilo	prohloubit	k5eAaPmAgNnS	prohloubit
propast	propast	k1gFnSc4	propast
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gMnPc7	on
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
jedné	jeden	k4xCgFnSc2	jeden
a	a	k8xC	a
církví	církev	k1gFnSc7	církev
a	a	k8xC	a
Zikmundem	Zikmund	k1gMnSc7	Zikmund
Lucemburským	lucemburský	k2eAgMnSc7d1	lucemburský
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
druhé	druhý	k4xOgFnSc6	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
a	a	k8xC	a
moravští	moravský	k2eAgMnPc1d1	moravský
šlechtici	šlechtic	k1gMnPc1	šlechtic
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
dříve	dříve	k6eAd2	dříve
podpořili	podpořit	k5eAaPmAgMnP	podpořit
svými	svůj	k3xOyFgFnPc7	svůj
pečetěmi	pečeť	k1gFnPc7	pečeť
stížné	stížný	k2eAgFnPc1d1	stížný
listy	lista	k1gFnPc1	lista
na	na	k7c4	na
obranu	obrana	k1gFnSc4	obrana
a	a	k8xC	a
proti	proti	k7c3	proti
věznění	věznění	k1gNnSc3	věznění
Jana	Jan	k1gMnSc2	Jan
Husa	Hus	k1gMnSc2	Hus
a	a	k8xC	a
Jeronýma	Jeroným	k1gMnSc2	Jeroným
Pražského	pražský	k2eAgMnSc2d1	pražský
adresované	adresovaný	k2eAgNnSc4d1	adresované
koncilu	koncil	k1gInSc2	koncil
a	a	k8xC	a
Zikmundovi	Zikmund	k1gMnSc3	Zikmund
založili	založit	k5eAaPmAgMnP	založit
5	[number]	k4	5
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
1415	[number]	k4	1415
husitský	husitský	k2eAgInSc1d1	husitský
svaz	svaz	k1gInSc1	svaz
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
brodě	brod	k1gInSc6	brod
ustavena	ustavit	k5eAaPmNgFnS	ustavit
Katolická	katolický	k2eAgFnSc1d1	katolická
liga	liga	k1gFnSc1	liga
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
1415	[number]	k4	1415
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1415	[number]	k4	1415
na	na	k7c4	na
nátlak	nátlak	k1gInSc4	nátlak
Koncilu	koncil	k1gInSc2	koncil
a	a	k8xC	a
Jana	Jan	k1gMnSc2	Jan
Železného	Železný	k1gMnSc2	Železný
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
pražský	pražský	k2eAgMnSc1d1	pražský
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Konrád	Konrád	k1gMnSc1	Konrád
z	z	k7c2	z
Vechty	Vechta	k1gFnSc2	Vechta
(	(	kIx(	(
<g/>
který	který	k3yQgMnSc1	který
nebyl	být	k5eNaImAgMnS	být
horlivým	horlivý	k2eAgMnSc7d1	horlivý
katolíkem	katolík	k1gMnSc7	katolík
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
Václavův	Václavův	k2eAgMnSc1d1	Václavův
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
měl	mít	k5eAaImAgMnS	mít
sympatie	sympatie	k1gFnSc2	sympatie
k	k	k7c3	k
Husovi	Hus	k1gMnSc3	Hus
<g/>
)	)	kIx)	)
zpřísnění	zpřísnění	k1gNnSc1	zpřísnění
interdiktu	interdikt	k1gInSc2	interdikt
nad	nad	k7c7	nad
Prahou	Praha	k1gFnSc7	Praha
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
pro	pro	k7c4	pro
přítomnost	přítomnost	k1gFnSc4	přítomnost
Jana	Jan	k1gMnSc2	Jan
z	z	k7c2	z
Jesenice	Jesenice	k1gFnSc2	Jesenice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Počátkem	počátkem	k7c2	počátkem
listopadu	listopad	k1gInSc2	listopad
provedli	provést	k5eAaPmAgMnP	provést
sympatizanti	sympatizant	k1gMnPc1	sympatizant
Husa	Hus	k1gMnSc2	Hus
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
takzvaný	takzvaný	k2eAgInSc4d1	takzvaný
"	"	kIx"	"
<g/>
Dušičkový	dušičkový	k2eAgInSc4d1	dušičkový
<g/>
"	"	kIx"	"
převrat	převrat	k1gInSc4	převrat
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
z	z	k7c2	z
části	část	k1gFnSc2	část
kostelů	kostel	k1gInPc2	kostel
a	a	k8xC	a
far	fara	k1gFnPc2	fara
vyhnali	vyhnat	k5eAaPmAgMnP	vyhnat
katolické	katolický	k2eAgMnPc4d1	katolický
duchovní	duchovní	k1gMnPc4	duchovní
a	a	k8xC	a
nahradili	nahradit	k5eAaPmAgMnP	nahradit
je	být	k5eAaImIp3nS	být
vlastními	vlastní	k2eAgInPc7d1	vlastní
<g/>
,	,	kIx,	,
oddanými	oddaný	k2eAgInPc7d1	oddaný
kalichu	kalich	k1gInSc3	kalich
<g/>
.	.	kIx.	.
</s>
<s>
Pohrůžkami	pohrůžka	k1gFnPc7	pohrůžka
nutili	nutit	k5eAaImAgMnP	nutit
k	k	k7c3	k
obnovení	obnovení	k1gNnSc3	obnovení
bohoslužeb	bohoslužba	k1gFnPc2	bohoslužba
(	(	kIx(	(
<g/>
nerespektovat	respektovat	k5eNaImF	respektovat
interdikt	interdikt	k1gInSc4	interdikt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podobná	podobný	k2eAgFnSc1d1	podobná
událost	událost	k1gFnSc1	událost
se	se	k3xPyFc4	se
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
následně	následně	k6eAd1	následně
udála	udát	k5eAaPmAgFnS	udát
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1416	[number]	k4	1416
<g/>
.	.	kIx.	.
</s>
<s>
Obojí	obojí	k4xRgMnSc1	obojí
bez	bez	k7c2	bez
větších	veliký	k2eAgInPc2d2	veliký
excesů	exces	k1gInPc2	exces
<g/>
.	.	kIx.	.
</s>
<s>
Katolíci	katolík	k1gMnPc1	katolík
po	po	k7c6	po
převratech	převrat	k1gInPc6	převrat
mohli	moct	k5eAaImAgMnP	moct
navštěvovat	navštěvovat	k5eAaImF	navštěvovat
své	svůj	k3xOyFgFnPc4	svůj
bohoslužby	bohoslužba	k1gFnPc4	bohoslužba
na	na	k7c6	na
Vyšehradě	Vyšehrad	k1gInSc6	Vyšehrad
nebo	nebo	k8xC	nebo
mimo	mimo	k7c4	mimo
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
jinde	jinde	k6eAd1	jinde
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
podobným	podobný	k2eAgFnPc3d1	podobná
událostem	událost	k1gFnPc3	událost
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
katolíky	katolík	k1gMnPc7	katolík
provedenými	provedený	k2eAgInPc7d1	provedený
<g/>
,	,	kIx,	,
někde	někde	k6eAd1	někde
i	i	k9	i
s	s	k7c7	s
kriminálními	kriminální	k2eAgInPc7d1	kriminální
motivy	motiv	k1gInPc7	motiv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1416	[number]	k4	1416
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
Kostnici	Kostnice	k1gFnSc6	Kostnice
k	k	k7c3	k
upálení	upálení	k1gNnSc3	upálení
dalšího	další	k2eAgMnSc2d1	další
reformisty	reformista	k1gMnSc2	reformista
Jeronýma	Jeroným	k1gMnSc2	Jeroným
Pražského	pražský	k2eAgMnSc2d1	pražský
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
král	král	k1gMnSc1	král
přikázal	přikázat	k5eAaPmAgMnS	přikázat
vrátit	vrátit	k5eAaPmF	vrátit
pražské	pražský	k2eAgFnSc2d1	Pražská
fary	fara	k1gFnSc2	fara
vyhnaným	vyhnaný	k2eAgMnPc3d1	vyhnaný
katolíkům	katolík	k1gMnPc3	katolík
a	a	k8xC	a
ponechal	ponechat	k5eAaPmAgMnS	ponechat
jim	on	k3xPp3gMnPc3	on
osm	osm	k4xCc1	osm
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
a	a	k8xC	a
tříměsíční	tříměsíční	k2eAgInSc1d1	tříměsíční
odchod	odchod	k1gInSc1	odchod
Jana	Jan	k1gMnSc2	Jan
z	z	k7c2	z
Jesenice	Jesenice	k1gFnSc2	Jesenice
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
<g/>
(	(	kIx(	(
<g/>
nerespektováno	respektován	k2eNgNnSc1d1	respektován
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
10	[number]	k4	10
<g/>
.	.	kIx.	.
leden	leden	k1gInSc1	leden
1417	[number]	k4	1417
-	-	kIx~	-
únor	únor	k1gInSc1	únor
1419	[number]	k4	1419
(	(	kIx(	(
<g/>
kolize	kolize	k1gFnSc2	kolize
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
10	[number]	k4	10
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1417	[number]	k4	1417
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
Konrád	Konrád	k1gMnSc1	Konrád
z	z	k7c2	z
Vechty	Vechta	k1gFnSc2	Vechta
zákaz	zákaz	k1gInSc4	zákaz
kalicha	kalich	k1gInSc2	kalich
a	a	k8xC	a
po	po	k7c6	po
protestu	protest	k1gInSc6	protest
pražské	pražský	k2eAgFnSc2d1	Pražská
univerzity	univerzita	k1gFnSc2	univerzita
zakázal	zakázat	k5eAaPmAgInS	zakázat
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
mistrovské	mistrovský	k2eAgFnSc2d1	mistrovská
zkoušky	zkouška	k1gFnSc2	zkouška
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
vyvolal	vyvolat	k5eAaPmAgMnS	vyvolat
velkou	velký	k2eAgFnSc4d1	velká
nevoli	nevole	k1gFnSc4	nevole
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
byl	být	k5eAaImAgMnS	být
donucen	donucen	k2eAgMnSc1d1	donucen
světící	světící	k2eAgMnSc1d1	světící
biskup	biskup	k1gMnSc1	biskup
(	(	kIx(	(
<g/>
odpůrce	odpůrce	k1gMnSc1	odpůrce
reformismu	reformismus	k1gInSc2	reformismus
<g/>
)	)	kIx)	)
Čeňkem	Čeněk	k1gMnSc7	Čeněk
z	z	k7c2	z
Vartenberka	Vartenberka	k1gFnSc1	Vartenberka
vysvětit	vysvětit	k5eAaPmF	vysvětit
kališnické	kališnický	k2eAgInPc4d1	kališnický
kněží	kněz	k1gMnPc5	kněz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
13	[number]	k4	13
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
na	na	k7c6	na
nádvoří	nádvoří	k1gNnSc6	nádvoří
pražské	pražský	k2eAgFnSc2d1	Pražská
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
koleje	kolej	k1gFnSc2	kolej
byli	být	k5eAaImAgMnP	být
násilně	násilně	k6eAd1	násilně
donuceni	donucen	k2eAgMnPc1d1	donucen
Petr	Petr	k1gMnSc1	Petr
z	z	k7c2	z
Uničova	Uničův	k2eAgInSc2d1	Uničův
a	a	k8xC	a
Jan	Jan	k1gMnSc1	Jan
z	z	k7c2	z
Hradce	Hradec	k1gInSc2	Hradec
k	k	k7c3	k
sepsání	sepsání	k1gNnSc3	sepsání
odvolání	odvolání	k1gNnSc2	odvolání
svých	svůj	k3xOyFgInPc2	svůj
protihusitských	protihusitský	k2eAgInPc2d1	protihusitský
postojů	postoj	k1gInPc2	postoj
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
toto	tento	k3xDgNnSc1	tento
se	se	k3xPyFc4	se
opakovalo	opakovat	k5eAaImAgNnS	opakovat
mimo	mimo	k7c4	mimo
Prahu	Praha	k1gFnSc4	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
Čeněk	Čeněk	k1gMnSc1	Čeněk
rozkázal	rozkázat	k5eAaPmAgMnS	rozkázat
na	na	k7c6	na
Rožmberských	rožmberský	k2eAgNnPc6d1	rožmberské
panstvích	panství	k1gNnPc6	panství
výhradně	výhradně	k6eAd1	výhradně
podávat	podávat	k5eAaImF	podávat
pod	pod	k7c7	pod
obojí	oboj	k1gFnSc7	oboj
způsobou	způsoba	k1gFnSc7	způsoba
<g/>
.	.	kIx.	.
</s>
<s>
Příkladu	příklad	k1gInSc3	příklad
pak	pak	k6eAd1	pak
následovala	následovat	k5eAaImAgFnS	následovat
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgMnPc2d1	další
pánů	pan	k1gMnPc2	pan
(	(	kIx(	(
<g/>
i	i	k9	i
předtím	předtím	k6eAd1	předtím
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1417	[number]	k4	1417
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Kostnici	Kostnice	k1gFnSc6	Kostnice
zvolen	zvolit	k5eAaPmNgMnS	zvolit
nový	nový	k2eAgMnSc1d1	nový
papež	papež	k1gMnSc1	papež
-	-	kIx~	-
ukončeno	ukončen	k2eAgNnSc1d1	ukončeno
papežské	papežský	k2eAgNnSc1d1	papežské
schizma	schizma	k1gNnSc1	schizma
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
Kostnické	kostnický	k2eAgInPc4d1	kostnický
dekrety	dekret	k1gInPc4	dekret
proti	proti	k7c3	proti
českým	český	k2eAgMnPc3d1	český
kacířům	kacíř	k1gMnPc3	kacíř
a	a	k8xC	a
pražské	pražský	k2eAgFnSc3d1	Pražská
univerzitě	univerzita	k1gFnSc3	univerzita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
16	[number]	k4	16
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1419	[number]	k4	1419
poslal	poslat	k5eAaPmAgMnS	poslat
římský	římský	k2eAgMnSc1d1	římský
král	král	k1gMnSc1	král
Zikmund	Zikmund	k1gMnSc1	Zikmund
nevlastnímu	vlastní	k2eNgMnSc3d1	nevlastní
bratru	bratr	k1gMnSc3	bratr
Václavovi	Václav	k1gMnSc3	Václav
list	list	k1gInSc4	list
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
ho	on	k3xPp3gMnSc4	on
vyzýval	vyzývat	k5eAaImAgInS	vyzývat
k	k	k7c3	k
nápravě	náprava	k1gFnSc3	náprava
poměrů	poměr	k1gInPc2	poměr
jinak	jinak	k6eAd1	jinak
riskuje	riskovat	k5eAaBmIp3nS	riskovat
ztrátu	ztráta	k1gFnSc4	ztráta
trůnu	trůn	k1gInSc2	trůn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Únor	únor	k1gInSc1	únor
-	-	kIx~	-
30	[number]	k4	30
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
1419	[number]	k4	1419
(	(	kIx(	(
<g/>
krize	krize	k1gFnSc2	krize
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
s	s	k7c7	s
výslovným	výslovný	k2eAgNnSc7d1	výslovné
odvoláním	odvolání	k1gNnSc7	odvolání
na	na	k7c4	na
Zikmundův	Zikmundův	k2eAgInSc4d1	Zikmundův
nátlak	nátlak	k1gInSc4	nátlak
vypověděl	vypovědět	k5eAaPmAgMnS	vypovědět
Jana	Jan	k1gMnSc4	Jan
z	z	k7c2	z
Jesenice	Jesenice	k1gFnSc2	Jesenice
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
částečnou	částečný	k2eAgFnSc4d1	částečná
restituci	restituce	k1gFnSc4	restituce
církevních	církevní	k2eAgInPc2d1	církevní
poměrů	poměr	k1gInPc2	poměr
a	a	k8xC	a
legalizoval	legalizovat	k5eAaBmAgMnS	legalizovat
změnu	změna	k1gFnSc4	změna
po	po	k7c6	po
Vratislavském	vratislavský	k2eAgInSc6d1	vratislavský
převratu	převrat	k1gInSc6	převrat
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1418	[number]	k4	1418
(	(	kIx(	(
<g/>
ten	ten	k3xDgInSc1	ten
zřejmě	zřejmě	k6eAd1	zřejmě
inspiroval	inspirovat	k5eAaBmAgInS	inspirovat
Želivského	želivský	k2eAgNnSc2d1	Želivské
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
smířlivá	smířlivý	k2eAgFnSc1d1	smířlivá
reakce	reakce	k1gFnSc1	reakce
Václava	Václav	k1gMnSc2	Václav
na	na	k7c4	na
převrat	převrat	k1gInSc4	převrat
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
26	[number]	k4	26
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
odvolal	odvolat	k5eAaPmAgMnS	odvolat
Konrád	Konrád	k1gMnSc1	Konrád
z	z	k7c2	z
Vechty	Vechta	k1gFnSc2	Vechta
interdikt	interdikt	k1gInSc1	interdikt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
16	[number]	k4	16
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
na	na	k7c4	na
Burkováku-Tábor	Burkováku-Tábor	k1gInSc4	Burkováku-Tábor
u	u	k7c2	u
Bechyně	Bechyně	k1gFnSc2	Bechyně
(	(	kIx(	(
<g/>
na	na	k7c4	na
půl	půl	k1xP	půl
cesty	cesta	k1gFnSc2	cesta
mezi	mezi	k7c7	mezi
Pískem	Písek	k1gInSc7	Písek
a	a	k8xC	a
Sezimovým	Sezimův	k2eAgNnSc7d1	Sezimovo
Ústím	ústí	k1gNnSc7	ústí
<g/>
,	,	kIx,	,
hlavními	hlavní	k2eAgInPc7d1	hlavní
středisky	středisko	k1gNnPc7	středisko
jihočeských	jihočeský	k2eAgMnPc2d1	jihočeský
husitů	husita	k1gMnPc2	husita
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
uspořádána	uspořádat	k5eAaPmNgFnS	uspořádat
první	první	k4xOgFnSc1	první
masová	masový	k2eAgFnSc1d1	masová
pouť	pouť	k1gFnSc1	pouť
na	na	k7c4	na
hory	hora	k1gFnPc4	hora
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
k	k	k7c3	k
násilným	násilný	k2eAgInPc3d1	násilný
incidentům	incident	k1gInPc3	incident
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
oběťmi	oběť	k1gFnPc7	oběť
(	(	kIx(	(
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
zaútočil	zaútočit	k5eAaPmAgInS	zaútočit
husitský	husitský	k2eAgInSc1d1	husitský
dav	dav	k1gInSc1	dav
na	na	k7c4	na
kostel	kostel	k1gInSc4	kostel
sv.	sv.	kA	sv.
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
po	po	k7c6	po
předchozí	předchozí	k2eAgFnSc6d1	předchozí
smrti	smrt	k1gFnSc6	smrt
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
června	červen	k1gInSc2	červen
byl	být	k5eAaImAgMnS	být
Václav	Václav	k1gMnSc1	Václav
při	při	k7c6	při
návštěvě	návštěva	k1gFnSc6	návštěva
kostela	kostel	k1gInSc2	kostel
sv.	sv.	kA	sv.
Apolináře	Apolinář	k1gMnSc2	Apolinář
obestoupen	obestoupen	k2eAgInSc1d1	obestoupen
davem	dav	k1gInSc7	dav
husitských	husitský	k2eAgMnPc2d1	husitský
poddaných	poddaný	k1gMnPc2	poddaný
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Mikulášem	mikuláš	k1gInSc7	mikuláš
z	z	k7c2	z
Husi	Hus	k1gFnSc2	Hus
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
přednesl	přednést	k5eAaPmAgInS	přednést
požadavky	požadavek	k1gInPc4	požadavek
o	o	k7c4	o
návrat	návrat	k1gInSc4	návrat
poměrů	poměr	k1gInPc2	poměr
před	před	k7c4	před
únor	únor	k1gInSc4	únor
1419	[number]	k4	1419
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
nevyjádřil	vyjádřit	k5eNaPmAgMnS	vyjádřit
k	k	k7c3	k
obsahu	obsah	k1gInSc3	obsah
a	a	k8xC	a
vyložil	vyložit	k5eAaPmAgMnS	vyložit
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
jako	jako	k9	jako
opovážlivost	opovážlivost	k1gFnSc1	opovážlivost
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
je	být	k5eAaImIp3nS	být
nutno	nutno	k6eAd1	nutno
potrestat	potrestat	k5eAaPmF	potrestat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
vypověděl	vypovědět	k5eAaPmAgMnS	vypovědět
Mikuláše	mikuláš	k1gInPc4	mikuláš
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
(	(	kIx(	(
<g/>
provokativně	provokativně	k6eAd1	provokativně
na	na	k7c4	na
výročí	výročí	k1gNnSc4	výročí
<g/>
)	)	kIx)	)
kompletně	kompletně	k6eAd1	kompletně
vyměnil	vyměnit	k5eAaPmAgMnS	vyměnit
novoměstskou	novoměstský	k2eAgFnSc4d1	Novoměstská
radu	rada	k1gFnSc4	rada
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
viníka	viník	k1gMnSc4	viník
<g/>
.	.	kIx.	.
</s>
<s>
Noví	nový	k2eAgMnPc1d1	nový
konšelé	konšel	k1gMnPc1	konšel
zatkli	zatknout	k5eAaPmAgMnP	zatknout
několik	několik	k4yIc4	několik
husitů	husita	k1gMnPc2	husita
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
účastníků	účastník	k1gMnPc2	účastník
vystoupení	vystoupení	k1gNnSc2	vystoupení
Mikuláše	Mikuláš	k1gMnSc2	Mikuláš
u	u	k7c2	u
krále	král	k1gMnSc2	král
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vyhnali	vyhnat	k5eAaPmAgMnP	vyhnat
z	z	k7c2	z
far	fara	k1gFnPc2	fara
husitské	husitský	k2eAgFnSc2d1	husitská
žáky	žák	k1gMnPc7	žák
<g/>
,	,	kIx,	,
zakázali	zakázat	k5eAaPmAgMnP	zakázat
husitská	husitský	k2eAgNnPc4d1	husitské
procesí	procesí	k1gNnPc4	procesí
(	(	kIx(	(
<g/>
směřováno	směřován	k2eAgNnSc4d1	směřováno
hlavně	hlavně	k9	hlavně
proti	proti	k7c3	proti
Janu	Jan	k1gMnSc3	Jan
Želivskému	želivský	k2eAgInSc3d1	želivský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
22	[number]	k4	22
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
na	na	k7c6	na
husitské	husitský	k2eAgFnSc6d1	husitská
pouti	pouť	k1gFnSc6	pouť
na	na	k7c6	na
vrchu	vrch	k1gInSc6	vrch
Burkovák-Tábor	Burkovák-Tábora	k1gFnPc2	Burkovák-Tábora
(	(	kIx(	(
<g/>
s	s	k7c7	s
až	až	k9	až
40.000	[number]	k4	40.000
účastníky	účastník	k1gMnPc7	účastník
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
Pražští	pražský	k2eAgMnPc1d1	pražský
husité	husita	k1gMnPc1	husita
a	a	k8xC	a
organizátoři	organizátor	k1gMnPc1	organizátor
poutí	pouť	k1gFnPc2	pouť
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
na	na	k7c6	na
vzájemné	vzájemný	k2eAgFnSc6d1	vzájemná
pomoci	pomoc	k1gFnSc6	pomoc
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
plánů	plán	k1gInPc2	plán
na	na	k7c4	na
30	[number]	k4	30
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
30	[number]	k4	30
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc1	červenec
1419	[number]	k4	1419
==	==	k?	==
</s>
</p>
<p>
<s>
Účastníci	účastník	k1gMnPc1	účastník
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
shromáždili	shromáždit	k5eAaPmAgMnP	shromáždit
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
Sněžné	sněžný	k2eAgFnSc2d1	sněžná
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
kázal	kázat	k5eAaImAgMnS	kázat
kněz	kněz	k1gMnSc1	kněz
a	a	k8xC	a
pozdější	pozdní	k2eAgMnSc1d2	pozdější
vůdce	vůdce	k1gMnSc1	vůdce
pražských	pražský	k2eAgInPc2d1	pražský
radikálů	radikál	k1gMnPc2	radikál
Jan	Jan	k1gMnSc1	Jan
Želivský	želivský	k2eAgMnSc1d1	želivský
<g/>
.	.	kIx.	.
</s>
<s>
Násilný	násilný	k2eAgInSc1d1	násilný
převrat	převrat	k1gInSc1	převrat
byl	být	k5eAaImAgInS	být
předem	předem	k6eAd1	předem
plánován	plánovat	k5eAaImNgInS	plánovat
<g/>
,	,	kIx,	,
mnoho	mnoho	k4c1	mnoho
stoupenců	stoupenec	k1gMnPc2	stoupenec
Želivského	želivský	k2eAgInSc2d1	želivský
se	se	k3xPyFc4	se
dostavilo	dostavit	k5eAaPmAgNnS	dostavit
ozbrojeno	ozbrojen	k2eAgNnSc1d1	ozbrojeno
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
kázání	kázání	k1gNnSc6	kázání
se	se	k3xPyFc4	se
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
davu	dav	k1gInSc2	dav
a	a	k8xC	a
tělem	tělo	k1gNnSc7	tělo
božím	božit	k5eAaImIp1nS	božit
nad	nad	k7c7	nad
sebou	se	k3xPyFc7	se
(	(	kIx(	(
<g/>
čímž	což	k3yQnSc7	což
porušil	porušit	k5eAaPmAgInS	porušit
zákaz	zákaz	k1gInSc1	zákaz
procesí	procesí	k1gNnSc2	procesí
<g/>
)	)	kIx)	)
odebral	odebrat	k5eAaPmAgMnS	odebrat
k	k	k7c3	k
farnímu	farní	k2eAgInSc3d1	farní
kostelu	kostel	k1gInSc3	kostel
sv.	sv.	kA	sv.
Štěpána	Štěpán	k1gMnSc2	Štěpán
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
před	před	k7c7	před
únorem	únor	k1gInSc7	únor
(	(	kIx(	(
<g/>
1419	[number]	k4	1419
<g/>
)	)	kIx)	)
husitským	husitský	k2eAgNnSc7d1	husitské
působištěm	působiště	k1gNnSc7	působiště
a	a	k8xC	a
kde	kde	k6eAd1	kde
probíhala	probíhat	k5eAaImAgFnS	probíhat
nedělní	nedělní	k2eAgFnSc1d1	nedělní
mše	mše	k1gFnSc1	mše
<g/>
.	.	kIx.	.
</s>
<s>
Vyhnali	vyhnat	k5eAaPmAgMnP	vyhnat
přítomné	přítomný	k1gMnPc4	přítomný
a	a	k8xC	a
zdevastovali	zdevastovat	k5eAaPmAgMnP	zdevastovat
i	i	k9	i
blízkou	blízký	k2eAgFnSc4d1	blízká
faru	fara	k1gFnSc4	fara
<g/>
,	,	kIx,	,
možná	možná	k9	možná
zde	zde	k6eAd1	zde
Želivský	želivský	k2eAgMnSc1d1	želivský
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
bohoslužbu	bohoslužba	k1gFnSc4	bohoslužba
za	za	k7c4	za
první	první	k4xOgNnSc4	první
vítězství	vítězství	k1gNnSc4	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
půl	půl	k1xP	půl
desátou	desátý	k4xOgFnSc4	desátý
se	se	k3xPyFc4	se
dav	dav	k1gInSc4	dav
Žitnou	žitná	k1gFnSc7	žitná
nebo	nebo	k8xC	nebo
Ječnou	ječný	k2eAgFnSc7d1	Ječná
ulicí	ulice	k1gFnSc7	ulice
vydal	vydat	k5eAaPmAgInS	vydat
před	před	k7c4	před
nedalekou	daleký	k2eNgFnSc4d1	nedaleká
Novoměstskou	novoměstský	k2eAgFnSc4d1	Novoměstská
radnici	radnice	k1gFnSc4	radnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c6	na
radních	radní	k1gFnPc6	radní
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
purkmistra	purkmistr	k1gMnSc2	purkmistr
Jana	Jan	k1gMnSc2	Jan
Podivínského	podivínský	k2eAgMnSc2d1	podivínský
požadoval	požadovat	k5eAaImAgMnS	požadovat
propuštění	propuštění	k1gNnSc3	propuštění
vězněných	vězněný	k2eAgMnPc2d1	vězněný
stoupenců	stoupenec	k1gMnPc2	stoupenec
přijímání	přijímání	k1gNnSc2	přijímání
podobojí	podobojí	k2eAgInPc1d1	podobojí
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
konšelé	konšel	k1gMnPc1	konšel
byli	být	k5eAaImAgMnP	být
po	po	k7c6	po
útoku	útok	k1gInSc6	útok
na	na	k7c6	na
radnici	radnice	k1gFnSc6	radnice
vyhozeni	vyhodit	k5eAaPmNgMnP	vyhodit
okny	okno	k1gNnPc7	okno
a	a	k8xC	a
krutě	krutě	k6eAd1	krutě
ubiti	ubit	k2eAgMnPc1d1	ubit
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
svědectví	svědectví	k1gNnSc2	svědectví
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
z	z	k7c2	z
Březové	březový	k2eAgFnSc2d1	Březová
se	se	k3xPyFc4	se
útoku	útok	k1gInSc3	útok
na	na	k7c4	na
radnici	radnice	k1gFnSc4	radnice
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Krále	Král	k1gMnSc2	Král
Václava	Václav	k1gMnSc2	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c4	o
zabití	zabití	k1gNnSc4	zabití
jím	jíst	k5eAaImIp1nS	jíst
dosazených	dosazený	k2eAgMnPc2d1	dosazený
konšelů	konšel	k1gMnPc2	konšel
dostihla	dostihnout	k5eAaPmAgFnS	dostihnout
na	na	k7c6	na
Novém	Nový	k1gMnSc6	Nový
hradu	hrad	k1gInSc2	hrad
u	u	k7c2	u
Kunratic	Kunratice	k1gFnPc2	Kunratice
a	a	k8xC	a
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
u	u	k7c2	u
něj	on	k3xPp3gInSc2	on
záchvat	záchvat	k1gInSc4	záchvat
hněvu	hněv	k1gInSc2	hněv
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yIgNnSc7	který
bývá	bývat	k5eAaImIp3nS	bývat
spojována	spojovat	k5eAaImNgFnS	spojovat
jeho	jeho	k3xOp3gFnSc1	jeho
smrt	smrt	k1gFnSc1	smrt
o	o	k7c4	o
několik	několik	k4yIc4	několik
týdnů	týden	k1gInPc2	týden
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Událost	událost	k1gFnSc1	událost
je	být	k5eAaImIp3nS	být
ztvárněna	ztvárnit	k5eAaPmNgFnS	ztvárnit
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Jan	Jan	k1gMnSc1	Jan
Žižka	Žižka	k1gMnSc1	Žižka
<g/>
,	,	kIx,	,
druhém	druhý	k4xOgInSc6	druhý
dílu	díl	k1gInSc6	díl
Vávrovy	Vávrův	k2eAgFnSc2d1	Vávrova
Husitské	husitský	k2eAgFnSc2d1	husitská
trilogie	trilogie	k1gFnSc2	trilogie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
tohoto	tento	k3xDgInSc2	tento
dne	den	k1gInSc2	den
už	už	k6eAd1	už
nemělo	mít	k5eNaImAgNnS	mít
být	být	k5eAaImF	být
nic	nic	k3yNnSc1	nic
jako	jako	k8xS	jako
dřív	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Následné	následný	k2eAgFnPc1d1	následná
bezprostřední	bezprostřední	k2eAgFnPc1d1	bezprostřední
události	událost	k1gFnPc1	událost
==	==	k?	==
</s>
</p>
<p>
<s>
Město	město	k1gNnSc1	město
rychle	rychle	k6eAd1	rychle
opustilo	opustit	k5eAaPmAgNnS	opustit
mnoho	mnoho	k4c1	mnoho
nepřátel	nepřítel	k1gMnPc2	nepřítel
kalicha	kalich	k1gInSc2	kalich
(	(	kIx(	(
<g/>
Jan	Jan	k1gMnSc1	Jan
Žuvač	Žuvač	k1gMnSc1	Žuvač
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Šilhan	Šilhan	k1gMnSc1	Šilhan
<g/>
,	,	kIx,	,
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
Navara	Navar	k1gMnSc2	Navar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
velká	velký	k2eAgFnSc1d1	velká
obec	obec	k1gFnSc1	obec
zvolila	zvolit	k5eAaPmAgFnS	zvolit
čtyři	čtyři	k4xCgMnPc4	čtyři
hejtmany	hejtman	k1gMnPc4	hejtman
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
spravovali	spravovat	k5eAaImAgMnP	spravovat
Nové	Nové	k2eAgNnSc4d1	Nové
Město	město	k1gNnSc4	město
do	do	k7c2	do
ustavení	ustavení	k1gNnSc2	ustavení
nových	nový	k2eAgMnPc2d1	nový
konšelů	konšel	k1gMnPc2	konšel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
prvotním	prvotní	k2eAgInSc6d1	prvotní
hněvu	hněv	k1gInSc6	hněv
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
kapituloval	kapitulovat	k5eAaBmAgMnS	kapitulovat
a	a	k8xC	a
už	už	k6eAd1	už
2	[number]	k4	2
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
(	(	kIx(	(
<g/>
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
hradě	hrad	k1gInSc6	hrad
<g/>
)	)	kIx)	)
oficiálně	oficiálně	k6eAd1	oficiálně
schválil	schválit	k5eAaPmAgInS	schválit
nově	nově	k6eAd1	nově
zvolené	zvolený	k2eAgMnPc4d1	zvolený
konšely	konšel	k1gMnPc4	konšel
(	(	kIx(	(
<g/>
na	na	k7c6	na
královském	královský	k2eAgInSc6d1	královský
dvoře	dvůr	k1gInSc6	dvůr
bylo	být	k5eAaImAgNnS	být
mnoho	mnoho	k4c1	mnoho
sympatizantů	sympatizant	k1gMnPc2	sympatizant
převratu	převrat	k1gInSc2	převrat
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
o	o	k7c6	o
něm	on	k3xPp3gInSc6	on
byli	být	k5eAaImAgMnP	být
předem	předem	k6eAd1	předem
informování	informování	k1gNnSc4	informování
a	a	k8xC	a
kteří	který	k3yQgMnPc1	který
na	na	k7c4	na
krále	král	k1gMnSc4	král
cíleně	cíleně	k6eAd1	cíleně
působili	působit	k5eAaImAgMnP	působit
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
dalším	další	k2eAgInPc3d1	další
útokům	útok	k1gInPc3	útok
na	na	k7c4	na
kláštery	klášter	k1gInPc4	klášter
žebravých	žebravý	k2eAgInPc2d1	žebravý
řádů	řád	k1gInPc2	řád
(	(	kIx(	(
<g/>
v	v	k7c6	v
Klatovech	Klatovy	k1gInPc6	Klatovy
<g/>
,	,	kIx,	,
Žatci	Žatec	k1gInSc6	Žatec
<g/>
,	,	kIx,	,
Lounech	Louny	k1gInPc6	Louny
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
zemřel	zemřít	k5eAaPmAgMnS	zemřít
král	král	k1gMnSc1	král
Václav	Václav	k1gMnSc1	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
a	a	k8xC	a
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
vyvolala	vyvolat	k5eAaPmAgFnS	vyvolat
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
vlnu	vlna	k1gFnSc4	vlna
vyhánění	vyhánění	k1gNnPc2	vyhánění
katolíků	katolík	k1gMnPc2	katolík
a	a	k8xC	a
obrazoborectví	obrazoborectví	k1gNnSc2	obrazoborectví
(	(	kIx(	(
<g/>
nejznámější	známý	k2eAgFnSc7d3	nejznámější
obětí	oběť	k1gFnSc7	oběť
byl	být	k5eAaImAgInS	být
17	[number]	k4	17
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
vypálený	vypálený	k2eAgInSc1d1	vypálený
kartuziánský	kartuziánský	k2eAgInSc1d1	kartuziánský
klášter	klášter	k1gInSc1	klášter
Zahrada	zahrada	k1gFnSc1	zahrada
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc4	seznam
defenestrovaných	defenestrovaný	k2eAgInPc2d1	defenestrovaný
==	==	k?	==
</s>
</p>
<p>
<s>
purkmistr	purkmistr	k1gMnSc1	purkmistr
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Podvinský	Podvinský	k2eAgMnSc1d1	Podvinský
</s>
</p>
<p>
<s>
konšelé	konšel	k1gMnPc1	konšel
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Čeněk	Čeněk	k1gMnSc1	Čeněk
Patriarcha	patriarcha	k1gMnSc1	patriarcha
</s>
</p>
<p>
<s>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
Rehla	Rehla	k1gMnSc1	Rehla
Drevník	Drevník	k1gMnSc1	Drevník
</s>
</p>
<p>
<s>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Humpolec	Humpolec	k1gInSc1	Humpolec
(	(	kIx(	(
<g/>
bývalí	bývalý	k2eAgMnPc1d1	bývalý
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Václav	Václav	k1gMnSc1	Václav
Barbořina	Barbořin	k2eAgInSc2d1	Barbořin
</s>
</p>
<p>
<s>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Ryba	Ryba	k1gMnSc1	Ryba
</s>
</p>
<p>
<s>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
Sasín	Sasín	k1gMnSc1	Sasín
</s>
</p>
<p>
<s>
==	==	k?	==
Slovy	slovo	k1gNnPc7	slovo
středověkých	středověký	k2eAgFnPc2d1	středověká
kronik	kronika	k1gFnPc2	kronika
==	==	k?	==
</s>
</p>
<p>
<s>
Husitská	husitský	k2eAgFnSc1d1	husitská
kronika	kronika	k1gFnSc1	kronika
Vavřince	Vavřinec	k1gMnSc2	Vavřinec
z	z	k7c2	z
Březové	březový	k2eAgFnSc2d1	Březová
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Potom	potom	k6eAd1	potom
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
po	po	k7c6	po
sv.	sv.	kA	sv.
Jakubu	Jakub	k1gMnSc6	Jakub
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
30	[number]	k4	30
<g/>
.	.	kIx.	.
dne	den	k1gInSc2	den
měsíce	měsíc	k1gInSc2	měsíc
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
purkmistr	purkmistr	k1gMnSc1	purkmistr
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
konšelé	konšel	k1gMnPc1	konšel
Nového	Nového	k2eAgNnSc2d1	Nového
Města	město	k1gNnSc2	město
s	s	k7c7	s
podrychtářem	podrychtář	k1gMnSc7	podrychtář
<g/>
,	,	kIx,	,
nepřátelé	nepřítel	k1gMnPc1	nepřítel
přijímání	přijímání	k1gNnPc2	přijímání
z	z	k7c2	z
kalicha	kalich	k1gInSc2	kalich
<g/>
,	,	kIx,	,
od	od	k7c2	od
obecného	obecný	k2eAgInSc2d1	obecný
lidu	lid	k1gInSc2	lid
a	a	k8xC	a
Jana	Jan	k1gMnSc2	Jan
Žižky	Žižka	k1gMnSc2	Žižka
<g/>
,	,	kIx,	,
dvořana	dvořan	k1gMnSc2	dvořan
svrchujmenovaného	svrchujmenovaný	k2eAgMnSc2d1	svrchujmenovaný
českého	český	k2eAgMnSc2d1	český
krále	král	k1gMnSc2	král
<g/>
,	,	kIx,	,
za	za	k7c4	za
<g />
.	.	kIx.	.
</s>
<s>
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
posmívali	posmívat	k5eAaImAgMnP	posmívat
procesí	procesí	k1gNnSc2	procesí
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
tamtudy	tamtudy	k6eAd1	tamtudy
mimo	mimo	k7c4	mimo
radnici	radnice	k1gFnSc4	radnice
vracelo	vracet	k5eAaImAgNnS	vracet
s	s	k7c7	s
velebnou	velebný	k2eAgFnSc7d1	velebná
svátostí	svátost	k1gFnSc7	svátost
oltářní	oltářní	k2eAgFnSc7d1	oltářní
od	od	k7c2	od
svatého	svatý	k2eAgMnSc2d1	svatý
Štěpána	Štěpán	k1gMnSc2	Štěpán
Na	na	k7c6	na
rybníčku	rybníček	k1gInSc6	rybníček
do	do	k7c2	do
kláštera	klášter	k1gInSc2	klášter
blahoslavené	blahoslavený	k2eAgFnSc2d1	blahoslavená
Panny	Panna	k1gFnSc2	Panna
Na	na	k7c6	na
písku	písek	k1gInSc6	písek
<g/>
,	,	kIx,	,
ohavně	ohavně	k6eAd1	ohavně
shozeni	shozen	k2eAgMnPc1d1	shozen
z	z	k7c2	z
Novoměstské	novoměstský	k2eAgFnSc2d1	Novoměstská
radnice	radnice	k1gFnSc2	radnice
a	a	k8xC	a
ukrutně	ukrutně	k6eAd1	ukrutně
ubiti	ubit	k2eAgMnPc1d1	ubit
a	a	k8xC	a
usmrceni	usmrcen	k2eAgMnPc1d1	usmrcen
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
král	král	k1gMnSc1	král
Václav	Václav	k1gMnSc1	Václav
dlel	dlít	k5eAaImAgMnS	dlít
se	s	k7c7	s
svým	svůj	k3xOyFgNnSc7	svůj
<g />
.	.	kIx.	.
</s>
<s>
dvorem	dvůr	k1gInSc7	dvůr
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
hradě	hrad	k1gInSc6	hrad
<g/>
,	,	kIx,	,
vzdáleném	vzdálený	k2eAgNnSc6d1	vzdálené
skoro	skoro	k6eAd1	skoro
míli	míle	k1gFnSc4	míle
od	od	k7c2	od
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
...	...	k?	...
Konečně	konečně	k6eAd1	konečně
bylo	být	k5eAaImAgNnS	být
se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
obou	dva	k4xCgFnPc2	dva
stran	strana	k1gFnPc2	strana
usneseno	usnést	k5eAaPmNgNnS	usnést
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
Novoměstská	novoměstský	k2eAgFnSc1d1	Novoměstská
obec	obec	k1gFnSc1	obec
pokořila	pokořit	k5eAaPmAgFnS	pokořit
králi	král	k1gMnSc3	král
za	za	k7c4	za
výtržnost	výtržnost	k1gFnSc4	výtržnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnSc3	který
se	se	k3xPyFc4	se
dopustila	dopustit	k5eAaPmAgFnS	dopustit
<g/>
,	,	kIx,	,
shodivši	shodit	k5eAaPmDgFnS	shodit
konšely	konšel	k1gMnPc4	konšel
od	od	k7c2	od
něho	on	k3xPp3gNnSc2	on
ustanovené	ustanovený	k2eAgInPc1d1	ustanovený
a	a	k8xC	a
hanebně	hanebně	k6eAd1	hanebně
je	být	k5eAaImIp3nS	být
usmrtivši	usmrtit	k5eAaPmDgFnS	usmrtit
<g/>
,	,	kIx,	,
a	a	k8xC	a
král	král	k1gMnSc1	král
aby	aby	kYmCp3nS	aby
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
nové	nový	k2eAgMnPc4d1	nový
konšely	konšel	k1gMnPc4	konšel
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
tehdy	tehdy	k6eAd1	tehdy
zvolené	zvolený	k2eAgNnSc1d1	zvolené
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
to	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
vskutku	vskutku	k9	vskutku
i	i	k9	i
stalo	stát	k5eAaPmAgNnS	stát
<g/>
.	.	kIx.	.
<g/>
Kronika	kronika	k1gFnSc1	kronika
Bartoška	Bartošek	k1gMnSc2	Bartošek
z	z	k7c2	z
Drahonic	Drahonice	k1gFnPc2	Drahonice
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Léta	léto	k1gNnPc4	léto
Páně	páně	k2eAgFnSc1d1	páně
1419	[number]	k4	1419
v	v	k7c6	v
neděli	neděle	k1gFnSc6	neděle
po	po	k7c6	po
sv.	sv.	kA	sv.
Jakubu	Jakub	k1gMnSc6	Jakub
/	/	kIx~	/
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
/	/	kIx~	/
byli	být	k5eAaImAgMnP	být
rychtář	rychtář	k1gMnSc1	rychtář
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
<g/>
,	,	kIx,	,
purkmistr	purkmistr	k1gMnSc1	purkmistr
a	a	k8xC	a
konšelé	konšel	k1gMnPc1	konšel
Nového	Nového	k2eAgNnSc2d1	Nového
Města	město	k1gNnSc2	město
pražského	pražský	k2eAgNnSc2d1	Pražské
svrženi	svržen	k2eAgMnPc1d1	svržen
z	z	k7c2	z
oken	okno	k1gNnPc2	okno
radnice	radnice	k1gFnSc2	radnice
a	a	k8xC	a
zabiti	zabít	k5eAaPmNgMnP	zabít
Žižkou	Žižka	k1gMnSc7	Žižka
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
spolčenci	spolčenec	k1gMnPc1	spolčenec
<g/>
.	.	kIx.	.
<g/>
Historie	historie	k1gFnSc1	historie
česká	český	k2eAgFnSc1d1	Česká
Eneáše	Eneáš	k1gMnSc4	Eneáš
Silvia	Silvia	k1gFnSc1	Silvia
Piccolominiho	Piccolomini	k1gMnSc2	Piccolomini
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Avšak	avšak	k8xC	avšak
30	[number]	k4	30
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
se	se	k3xPyFc4	se
sešli	sejít	k5eAaPmAgMnP	sejít
kacíři	kacíř	k1gMnPc1	kacíř
v	v	k7c6	v
kostele	kostel	k1gInSc6	kostel
karmelitánů	karmelitán	k1gMnPc2	karmelitán
<g/>
,	,	kIx,	,
ozbrojeni	ozbrojen	k2eAgMnPc1d1	ozbrojen
obešli	obejít	k5eAaPmAgMnP	obejít
s	s	k7c7	s
nejsvětější	nejsvětější	k2eAgFnSc7d1	nejsvětější
svátostí	svátost	k1gFnSc7	svátost
mnoho	mnoho	k6eAd1	mnoho
ulic	ulice	k1gFnPc2	ulice
a	a	k8xC	a
kostelů	kostel	k1gInPc2	kostel
až	až	k9	až
ke	k	k7c3	k
kostelu	kostel	k1gInSc3	kostel
svatého	svatý	k2eAgMnSc2d1	svatý
Štěpána	Štěpán	k1gMnSc2	Štěpán
a	a	k8xC	a
tam	tam	k6eAd1	tam
vydrancovali	vydrancovat	k5eAaPmAgMnP	vydrancovat
dům	dům	k1gInSc4	dům
kněze	kněz	k1gMnSc2	kněz
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
s	s	k7c7	s
jejich	jejich	k3xOp3gInPc7	jejich
bludy	blud	k1gInPc7	blud
<g/>
,	,	kIx,	,
a	a	k8xC	a
rozzuřeni	rozzuřen	k2eAgMnPc1d1	rozzuřen
zamířili	zamířit	k5eAaPmAgMnP	zamířit
s	s	k7c7	s
mohutným	mohutný	k2eAgInSc7d1	mohutný
křikem	křik	k1gInSc7	křik
k	k	k7c3	k
radnici	radnice	k1gFnSc3	radnice
<g/>
.	.	kIx.	.
</s>
<s>
Jedenáct	jedenáct	k4xCc1	jedenáct
konšelů	konšel	k1gMnPc2	konšel
zachvácených	zachvácený	k2eAgMnPc2d1	zachvácený
strachem	strach	k1gInSc7	strach
se	se	k3xPyFc4	se
spasilo	spasit	k5eAaPmAgNnS	spasit
útěkem	útěk	k1gInSc7	útěk
<g/>
,	,	kIx,	,
sedm	sedm	k4xCc4	sedm
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zůstali	zůstat	k5eAaPmAgMnP	zůstat
<g/>
,	,	kIx,	,
a	a	k8xC	a
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
městský	městský	k2eAgMnSc1d1	městský
rychtář	rychtář	k1gMnSc1	rychtář
a	a	k8xC	a
několik	několik	k4yIc1	několik
jiných	jiný	k2eAgMnPc2d1	jiný
měšťanů	měšťan	k1gMnPc2	měšťan
bylo	být	k5eAaImAgNnS	být
okamžitě	okamžitě	k6eAd1	okamžitě
polapeno	polapen	k2eAgNnSc1d1	polapeno
a	a	k8xC	a
vyhozeno	vyhozen	k2eAgNnSc1d1	vyhozeno
z	z	k7c2	z
nejvyšších	vysoký	k2eAgNnPc2d3	nejvyšší
oken	okno	k1gNnPc2	okno
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
;	;	kIx,	;
nabodli	nabodnout	k5eAaPmAgMnP	nabodnout
se	se	k3xPyFc4	se
na	na	k7c4	na
kopí	kopí	k1gNnPc4	kopí
a	a	k8xC	a
sudlice	sudlice	k1gFnPc4	sudlice
rozezleného	rozezlený	k2eAgInSc2d1	rozezlený
lidu	lid	k1gInSc2	lid
a	a	k8xC	a
poskytli	poskytnout	k5eAaPmAgMnP	poskytnout
roztrháni	roztrhat	k5eAaPmNgMnP	roztrhat
hroznou	hrozný	k2eAgFnSc7d1	hrozná
podívanou	podívaná	k1gFnSc7	podívaná
<g/>
;	;	kIx,	;
mnich	mnich	k1gMnSc1	mnich
Jan	Jan	k1gMnSc1	Jan
stál	stát	k5eAaImAgMnS	stát
při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
na	na	k7c6	na
náměstí	náměstí	k1gNnSc1	náměstí
a	a	k8xC	a
ukazoval	ukazovat	k5eAaImAgMnS	ukazovat
svaté	svatý	k2eAgNnSc4d1	svaté
Kristovo	Kristův	k2eAgNnSc4d1	Kristovo
tělo	tělo	k1gNnSc4	tělo
davu	dav	k1gInSc3	dav
dychtícímu	dychtící	k2eAgInSc3d1	dychtící
po	po	k7c6	po
krvi	krev	k1gFnSc6	krev
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
královský	královský	k2eAgMnSc1d1	královský
komoří	komoří	k1gMnSc1	komoří
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
přispěchal	přispěchat	k5eAaPmAgMnS	přispěchat
se	s	k7c7	s
třemi	tři	k4xCgNnPc7	tři
sty	sto	k4xCgNnPc7	sto
jezdci	jezdec	k1gMnSc3	jezdec
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
uklidnil	uklidnit	k5eAaPmAgMnS	uklidnit
vzpouru	vzpoura	k1gFnSc4	vzpoura
<g/>
,	,	kIx,	,
spatřil	spatřit	k5eAaPmAgMnS	spatřit
zuřící	zuřící	k2eAgInSc4d1	zuřící
dav	dav	k1gInSc4	dav
<g/>
,	,	kIx,	,
dal	dát	k5eAaPmAgMnS	dát
se	se	k3xPyFc4	se
v	v	k7c6	v
obavě	obava	k1gFnSc6	obava
o	o	k7c4	o
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
na	na	k7c4	na
útěk	útěk	k1gInSc4	útěk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ČORNEJ	ČORNEJ	kA	ČORNEJ
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
7	[number]	k4	7
<g/>
.	.	kIx.	.
1419	[number]	k4	1419
–	–	k?	–
První	první	k4xOgFnSc1	první
pražská	pražský	k2eAgFnSc1d1	Pražská
defenestrace	defenestrace	k1gFnSc1	defenestrace
:	:	kIx,	:
krvavá	krvavý	k2eAgFnSc1d1	krvavá
neděle	neděle	k1gFnSc1	neděle
uprostřed	uprostřed	k7c2	uprostřed
léta	léto	k1gNnSc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Havran	Havran	k1gMnSc1	Havran
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
200	[number]	k4	200
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87341	[number]	k4	87341
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
DUFKOVÁ	Dufková	k1gFnSc1	Dufková
<g/>
,	,	kIx,	,
Vlastimila	Vlastimila	k1gFnSc1	Vlastimila
<g/>
.	.	kIx.	.
</s>
<s>
Slavné	slavný	k2eAgFnPc1d1	slavná
defenestrace	defenestrace	k1gFnPc1	defenestrace
<g/>
:	:	kIx,	:
Oknem	okno	k1gNnSc7	okno
vyletí	vyletět	k5eAaPmIp3nS	vyletět
královna	královna	k1gFnSc1	královna
<g/>
,	,	kIx,	,
konšelé	konšel	k1gMnPc1	konšel
i	i	k8xC	i
místodržící	místodržící	k1gMnPc1	místodržící
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
</s>
<s>
Epocha	epocha	k1gFnSc1	epocha
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
,	,	kIx,	,
s.	s.	k?	s.
22	[number]	k4	22
<g/>
-	-	kIx~	-
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
202	[number]	k4	202
<g/>
.	.	kIx.	.
schůzka	schůzka	k1gFnSc1	schůzka
<g/>
:	:	kIx,	:
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
defenestrace	defenestrace	k1gFnSc1	defenestrace
<g/>
,	,	kIx,	,
pořadové	pořadový	k2eAgNnSc1d1	pořadové
číslo	číslo	k1gNnSc1	číslo
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Toulky	toulka	k1gFnSc2	toulka
českou	český	k2eAgFnSc7d1	Česká
minulostí	minulost	k1gFnSc7	minulost
</s>
</p>
