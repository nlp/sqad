<p>
<s>
Morčák	morčák	k1gMnSc1	morčák
velký	velký	k2eAgMnSc1d1	velký
(	(	kIx(	(
<g/>
Mergus	Mergus	k1gMnSc1	Mergus
merganser	merganser	k1gMnSc1	merganser
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgMnSc1d1	velký
vrubozobý	vrubozobý	k2eAgMnSc1d1	vrubozobý
pták	pták	k1gMnSc1	pták
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
kachnovití	kachnovití	k1gMnPc1	kachnovití
s	s	k7c7	s
rozsáhlým	rozsáhlý	k2eAgInSc7d1	rozsáhlý
areálem	areál	k1gInSc7	areál
rozšíření	rozšíření	k1gNnSc2	rozšíření
<g/>
.	.	kIx.	.
</s>
<s>
Hojně	hojně	k6eAd1	hojně
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
na	na	k7c6	na
velkém	velký	k2eAgNnSc6d1	velké
území	území	k1gNnSc6	území
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
Asie	Asie	k1gFnSc2	Asie
a	a	k8xC	a
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
mapka	mapka	k1gFnSc1	mapka
níže	níže	k1gFnSc2	níže
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
<g/>
:	:	kIx,	:
58	[number]	k4	58
–	–	k?	–
80	[number]	k4	80
cm	cm	kA	cm
</s>
</p>
<p>
<s>
Rozpětí	rozpětí	k1gNnSc1	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
<g/>
:	:	kIx,	:
100	[number]	k4	100
cm	cm	kA	cm
</s>
</p>
<p>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
:	:	kIx,	:
1100	[number]	k4	1100
–	–	k?	–
2000	[number]	k4	2000
g	g	kA	g
</s>
</p>
<p>
<s>
Morčák	morčák	k1gMnSc1	morčák
velký	velký	k2eAgMnSc1d1	velký
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
větší	veliký	k2eAgFnSc1d2	veliký
než	než	k8xS	než
kachna	kachna	k1gFnSc1	kachna
divoká	divoký	k2eAgFnSc1d1	divoká
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
tělo	tělo	k1gNnSc4	tělo
<g/>
,	,	kIx,	,
velkou	velký	k2eAgFnSc4d1	velká
hlavu	hlava	k1gFnSc4	hlava
a	a	k8xC	a
tenký	tenký	k2eAgInSc4d1	tenký
<g/>
,	,	kIx,	,
na	na	k7c6	na
konci	konec	k1gInSc6	konec
zahnutý	zahnutý	k2eAgInSc4d1	zahnutý
zobák	zobák	k1gInSc4	zobák
s	s	k7c7	s
pilovitými	pilovitý	k2eAgInPc7d1	pilovitý
okraji	okraj	k1gInPc7	okraj
pro	pro	k7c4	pro
lepší	dobrý	k2eAgNnSc4d2	lepší
uchopení	uchopení	k1gNnSc4	uchopení
kořisti	kořist	k1gFnSc2	kořist
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
svatebním	svatební	k2eAgInSc6d1	svatební
šatě	šat	k1gInSc6	šat
má	mít	k5eAaImIp3nS	mít
samec	samec	k1gMnSc1	samec
lesklou	lesklý	k2eAgFnSc4d1	lesklá
<g/>
,	,	kIx,	,
tmavě	tmavě	k6eAd1	tmavě
zelenou	zelený	k2eAgFnSc4d1	zelená
hlavu	hlava	k1gFnSc4	hlava
<g/>
,	,	kIx,	,
šedozelený	šedozelený	k2eAgInSc4d1	šedozelený
hřbet	hřbet	k1gInSc4	hřbet
<g/>
,	,	kIx,	,
sněhově	sněhově	k6eAd1	sněhově
bílé	bílý	k2eAgFnPc4d1	bílá
boky	boka	k1gFnPc4	boka
a	a	k8xC	a
červené	červený	k2eAgFnPc4d1	červená
končetiny	končetina	k1gFnPc4	končetina
a	a	k8xC	a
zobák	zobák	k1gInSc4	zobák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Samice	samice	k1gFnSc1	samice
není	být	k5eNaImIp3nS	být
zdaleka	zdaleka	k6eAd1	zdaleka
tak	tak	k6eAd1	tak
výrazně	výrazně	k6eAd1	výrazně
zbarvena	zbarven	k2eAgFnSc1d1	zbarvena
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
přesto	přesto	k8xC	přesto
působí	působit	k5eAaImIp3nP	působit
půvabným	půvabný	k2eAgInSc7d1	půvabný
dojmem	dojem	k1gInSc7	dojem
hlavně	hlavně	k9	hlavně
díky	díky	k7c3	díky
odstávajícím	odstávající	k2eAgNnPc3d1	odstávající
perům	pero	k1gNnPc3	pero
v	v	k7c6	v
týle	týl	k1gInSc6	týl
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nejsou	být	k5eNaImIp3nP	být
u	u	k7c2	u
samce	samec	k1gInSc2	samec
tak	tak	k6eAd1	tak
nápadná	nápadný	k2eAgFnSc1d1	nápadná
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
celá	celý	k2eAgFnSc1d1	celá
šedá	šedá	k1gFnSc1	šedá
s	s	k7c7	s
hnědou	hnědý	k2eAgFnSc7d1	hnědá
hlavou	hlava	k1gFnSc7	hlava
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
samec	samec	k1gMnSc1	samec
má	mít	k5eAaImIp3nS	mít
červený	červený	k2eAgInSc4d1	červený
zobák	zobák	k1gInSc4	zobák
a	a	k8xC	a
končetiny	končetina	k1gFnPc4	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
roku	rok	k1gInSc2	rok
jsou	být	k5eAaImIp3nP	být
obě	dva	k4xCgNnPc4	dva
pohlaví	pohlaví	k1gNnPc4	pohlaví
zbarvena	zbarven	k2eAgNnPc4d1	zbarveno
stejně	stejně	k6eAd1	stejně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chování	chování	k1gNnSc1	chování
==	==	k?	==
</s>
</p>
<p>
<s>
Morčáka	morčák	k1gMnSc4	morčák
velkého	velký	k2eAgInSc2d1	velký
nalezneme	nalézt	k5eAaBmIp1nP	nalézt
na	na	k7c6	na
jezerech	jezero	k1gNnPc6	jezero
a	a	k8xC	a
pomalu	pomalu	k6eAd1	pomalu
tekoucích	tekoucí	k2eAgFnPc6d1	tekoucí
řekách	řeka	k1gFnPc6	řeka
<g/>
,	,	kIx,	,
velice	velice	k6eAd1	velice
často	často	k6eAd1	často
bývá	bývat	k5eAaImIp3nS	bývat
chován	chován	k2eAgMnSc1d1	chován
jako	jako	k8xC	jako
okrasný	okrasný	k2eAgMnSc1d1	okrasný
pták	pták	k1gMnSc1	pták
v	v	k7c6	v
parkových	parkový	k2eAgInPc6d1	parkový
jezírcích	jezírek	k1gInPc6	jezírek
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
částečně	částečně	k6eAd1	částečně
tažný	tažný	k2eAgMnSc1d1	tažný
<g/>
,	,	kIx,	,
jedinci	jedinec	k1gMnPc1	jedinec
ze	z	k7c2	z
severoevropských	severoevropský	k2eAgFnPc2d1	severoevropská
oblastí	oblast	k1gFnPc2	oblast
na	na	k7c4	na
zimu	zima	k1gFnSc4	zima
migrují	migrovat	k5eAaImIp3nP	migrovat
do	do	k7c2	do
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
jižní	jižní	k2eAgFnSc2d1	jižní
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zdržují	zdržovat	k5eAaImIp3nP	zdržovat
od	od	k7c2	od
listopadu	listopad	k1gInSc2	listopad
do	do	k7c2	do
března	březen	k1gInSc2	březen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mimo	mimo	k7c4	mimo
období	období	k1gNnSc4	období
rozmnožování	rozmnožování	k1gNnSc2	rozmnožování
se	se	k3xPyFc4	se
nijak	nijak	k6eAd1	nijak
neozývá	ozývat	k5eNaImIp3nS	ozývat
<g/>
,	,	kIx,	,
tokající	tokající	k2eAgInSc1d1	tokající
samec	samec	k1gInSc1	samec
však	však	k9	však
vydává	vydávat	k5eAaPmIp3nS	vydávat
hluboké	hluboký	k2eAgNnSc1d1	hluboké
"	"	kIx"	"
<g/>
kerr	kerr	k1gInSc1	kerr
kerr	kerr	k1gInSc1	kerr
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Morčák	morčák	k1gMnSc1	morčák
je	být	k5eAaImIp3nS	být
skvěle	skvěle	k6eAd1	skvěle
přizpůsoben	přizpůsobit	k5eAaPmNgInS	přizpůsobit
k	k	k7c3	k
lovu	lov	k1gInSc3	lov
ryb	ryba	k1gFnPc2	ryba
<g/>
,	,	kIx,	,
dokáže	dokázat	k5eAaPmIp3nS	dokázat
poměrně	poměrně	k6eAd1	poměrně
rychle	rychle	k6eAd1	rychle
plavat	plavat	k5eAaImF	plavat
i	i	k9	i
v	v	k7c6	v
3	[number]	k4	3
metrových	metrový	k2eAgFnPc6d1	metrová
hloubkách	hloubka	k1gFnPc6	hloubka
a	a	k8xC	a
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
lepšímu	dobrý	k2eAgNnSc3d2	lepší
uchopení	uchopení	k1gNnSc3	uchopení
má	mít	k5eAaImIp3nS	mít
skvěle	skvěle	k6eAd1	skvěle
přizpůsobený	přizpůsobený	k2eAgInSc4d1	přizpůsobený
zobák	zobák	k1gInSc4	zobák
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
popis	popis	k1gInSc1	popis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
potravou	potrava	k1gFnSc7	potrava
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
stávají	stávat	k5eAaImIp3nP	stávat
i	i	k9	i
jiní	jiný	k2eAgMnPc1d1	jiný
vodní	vodní	k2eAgMnPc1d1	vodní
živočichové	živočich	k1gMnPc1	živočich
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
měkkýši	měkkýš	k1gMnPc1	měkkýš
<g/>
,	,	kIx,	,
korýši	korýš	k1gMnPc1	korýš
nebo	nebo	k8xC	nebo
slávky	slávka	k1gFnPc1	slávka
<g/>
;	;	kIx,	;
pro	pro	k7c4	pro
mláďata	mládě	k1gNnPc4	mládě
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
složkou	složka	k1gFnSc7	složka
potravy	potrava	k1gFnSc2	potrava
vodní	vodní	k2eAgInSc4d1	vodní
hmyz	hmyz	k1gInSc4	hmyz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
do	do	k7c2	do
května	květen	k1gInSc2	květen
a	a	k8xC	a
nejčastěji	často	k6eAd3	často
zahnizďuje	zahnizďovat	k5eAaImIp3nS	zahnizďovat
v	v	k7c6	v
dutinách	dutina	k1gFnPc6	dutina
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
loděnicemi	loděnice	k1gFnPc7	loděnice
<g/>
,	,	kIx,	,
na	na	k7c6	na
skalách	skála	k1gFnPc6	skála
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
dřevěných	dřevěný	k2eAgFnPc6d1	dřevěná
budkách	budka	k1gFnPc6	budka
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
klade	klást	k5eAaImIp3nS	klást
6	[number]	k4	6
až	až	k9	až
17	[number]	k4	17
vajec	vejce	k1gNnPc2	vejce
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yQgFnPc6	který
sedí	sedit	k5eAaImIp3nS	sedit
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
30	[number]	k4	30
–	–	k?	–
35	[number]	k4	35
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
se	se	k3xPyFc4	se
plně	plně	k6eAd1	plně
osamostatňují	osamostatňovat	k5eAaImIp3nP	osamostatňovat
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
60	[number]	k4	60
–	–	k?	–
70	[number]	k4	70
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Poddruhy	poddruh	k1gInPc4	poddruh
==	==	k?	==
</s>
</p>
<p>
<s>
M.	M.	kA	M.
m.	m.	k?	m.
merganser	merganser	k1gInSc1	merganser
–	–	k?	–
Evropa	Evropa	k1gFnSc1	Evropa
a	a	k8xC	a
severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
M.	M.	kA	M.
m.	m.	k?	m.
orientalis	orientalis	k1gInSc1	orientalis
–	–	k?	–
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
M.	M.	kA	M.
m.	m.	k?	m.
americanus	americanus	k1gInSc1	americanus
–	–	k?	–
Severní	severní	k2eAgFnSc1d1	severní
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byly	být	k5eAaImAgInP	být
použity	použit	k2eAgInPc1d1	použit
překlady	překlad	k1gInPc1	překlad
textů	text	k1gInPc2	text
z	z	k7c2	z
článků	článek	k1gInPc2	článek
Common	Common	k1gMnSc1	Common
Merganser	Merganser	k1gMnSc1	Merganser
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
a	a	k8xC	a
Tracz	Tracz	k1gInSc1	Tracz
nurogęś	nurogęś	k?	nurogęś
na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Bezzel	Bezzet	k5eAaPmAgMnS	Bezzet
<g/>
,	,	kIx,	,
E.	E.	kA	E.
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Ptáci	pták	k1gMnPc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Rebo	Rebo	k1gNnSc1	Rebo
Productions	Productionsa	k1gFnPc2	Productionsa
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-7234-292-1	[number]	k4	978-80-7234-292-1
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
morčák	morčák	k1gMnSc1	morčák
velký	velký	k2eAgMnSc1d1	velký
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
morčák	morčák	k1gMnSc1	morčák
velký	velký	k2eAgMnSc1d1	velký
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Biolib	Biolib	k1gMnSc1	Biolib
</s>
</p>
