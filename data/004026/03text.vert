<s>
Energie	energie	k1gFnSc1	energie
je	být	k5eAaImIp3nS	být
skalární	skalární	k2eAgFnSc1d1	skalární
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
veličina	veličina	k1gFnSc1	veličina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
popisuje	popisovat	k5eAaImIp3nS	popisovat
schopnost	schopnost	k1gFnSc4	schopnost
hmoty	hmota	k1gFnSc2	hmota
(	(	kIx(	(
<g/>
látky	látka	k1gFnPc4	látka
nebo	nebo	k8xC	nebo
pole	pole	k1gNnPc4	pole
<g/>
)	)	kIx)	)
konat	konat	k5eAaImF	konat
práci	práce	k1gFnSc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Energie	energie	k1gFnSc1	energie
je	být	k5eAaImIp3nS	být
slovo	slovo	k1gNnSc4	slovo
vytvořené	vytvořený	k2eAgFnSc2d1	vytvořená
fyziky	fyzika	k1gFnSc2	fyzika
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
devatenáctého	devatenáctý	k4xOgNnSc2	devatenáctý
století	století	k1gNnSc2	století
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
energeia	energeium	k1gNnSc2	energeium
(	(	kIx(	(
<g/>
vůle	vůle	k1gFnSc1	vůle
<g/>
,	,	kIx,	,
síla	síla	k1gFnSc1	síla
či	či	k8xC	či
schopnost	schopnost	k1gFnSc1	schopnost
k	k	k7c3	k
činům	čin	k1gInPc3	čin
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Energie	energie	k1gFnSc1	energie
je	být	k5eAaImIp3nS	být
popsána	popsat	k5eAaPmNgFnS	popsat
stavovou	stavový	k2eAgFnSc7d1	stavová
veličinou	veličina	k1gFnSc7	veličina
<g/>
.	.	kIx.	.
</s>
<s>
Energie	energie	k1gFnSc1	energie
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
různé	různý	k2eAgFnPc4d1	různá
formy	forma	k1gFnPc4	forma
<g/>
.	.	kIx.	.
</s>
<s>
Existuje	existovat	k5eAaImIp3nS	existovat
např.	např.	kA	např.
kinetická	kinetický	k2eAgFnSc1d1	kinetická
energie	energie	k1gFnSc1	energie
(	(	kIx(	(
<g/>
tu	tu	k6eAd1	tu
lze	lze	k6eAd1	lze
spočítat	spočítat	k5eAaPmF	spočítat
dle	dle	k7c2	dle
formule	formule	k1gFnSc2	formule
Ek	Ek	k1gFnSc2	Ek
=	=	kIx~	=
1⁄	1⁄	k?	1⁄
m	m	kA	m
<g/>
·	·	k?	·
<g/>
v	v	k7c6	v
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
a	a	k8xC	a
konfigurační	konfigurační	k2eAgMnPc1d1	konfigurační
(	(	kIx(	(
<g/>
polohové	polohový	k2eAgNnSc1d1	polohové
či	či	k8xC	či
potenciální	potenciální	k2eAgNnSc1d1	potenciální
<g/>
)	)	kIx)	)
energie	energie	k1gFnSc1	energie
(	(	kIx(	(
<g/>
dané	daný	k2eAgNnSc1d1	dané
vzájemnou	vzájemný	k2eAgFnSc7d1	vzájemná
polohou	poloha	k1gFnSc7	poloha
a	a	k8xC	a
přitahováním	přitahování	k1gNnSc7	přitahování
nebo	nebo	k8xC	nebo
odpuzováním	odpuzování	k1gNnSc7	odpuzování
částic	částice	k1gFnPc2	částice
<g/>
,	,	kIx,	,
např.	např.	kA	např.
gravitací	gravitace	k1gFnSc7	gravitace
nebo	nebo	k8xC	nebo
magnetismem	magnetismus	k1gInSc7	magnetismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
zachování	zachování	k1gNnSc2	zachování
energie	energie	k1gFnSc2	energie
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
energie	energie	k1gFnSc1	energie
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
měnit	měnit	k5eAaImF	měnit
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
druhu	druh	k1gInSc2	druh
na	na	k7c4	na
jiný	jiný	k2eAgInSc4d1	jiný
<g/>
,	,	kIx,	,
nelze	lze	k6eNd1	lze
ji	on	k3xPp3gFnSc4	on
vytvořit	vytvořit	k5eAaPmF	vytvořit
ani	ani	k8xC	ani
zničit	zničit	k5eAaPmF	zničit
<g/>
,	,	kIx,	,
v	v	k7c6	v
izolované	izolovaný	k2eAgFnSc6d1	izolovaná
soustavě	soustava	k1gFnSc6	soustava
však	však	k9	však
její	její	k3xOp3gNnSc1	její
celkové	celkový	k2eAgNnSc1d1	celkové
množství	množství	k1gNnSc1	množství
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
stejné	stejný	k2eAgNnSc1d1	stejné
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
součet	součet	k1gInSc1	součet
velikosti	velikost	k1gFnSc2	velikost
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
těleso	těleso	k1gNnSc1	těleso
nebo	nebo	k8xC	nebo
pole	pole	k1gNnSc1	pole
vykoná	vykonat	k5eAaPmIp3nS	vykonat
<g/>
,	,	kIx,	,
a	a	k8xC	a
vydaného	vydaný	k2eAgNnSc2d1	vydané
tepla	teplo	k1gNnSc2	teplo
se	se	k3xPyFc4	se
rovná	rovnat	k5eAaImIp3nS	rovnat
úbytku	úbytek	k1gInSc2	úbytek
jeho	jeho	k3xOp3gFnSc2	jeho
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
přemění	přeměnit	k5eAaPmIp3nS	přeměnit
v	v	k7c4	v
jinou	jiný	k2eAgFnSc4d1	jiná
formu	forma	k1gFnSc4	forma
<g/>
.	.	kIx.	.
</s>
<s>
Energie	energie	k1gFnSc1	energie
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
klidová	klidový	k2eAgFnSc1d1	klidová
energie	energie	k1gFnSc1	energie
<g/>
)	)	kIx)	)
přísluší	příslušet	k5eAaImIp3nS	příslušet
též	též	k9	též
každému	každý	k3xTgInSc3	každý
objektu	objekt	k1gInSc3	objekt
s	s	k7c7	s
klidovou	klidový	k2eAgFnSc7d1	klidová
hmotností	hmotnost	k1gFnSc7	hmotnost
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
jeho	jeho	k3xOp3gInSc4	jeho
pohybový	pohybový	k2eAgInSc4d1	pohybový
stav	stav	k1gInSc4	stav
a	a	k8xC	a
působení	působení	k1gNnSc4	působení
silových	silový	k2eAgFnPc2d1	silová
polí	pole	k1gFnPc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
Přeměna	přeměna	k1gFnSc1	přeměna
této	tento	k3xDgFnSc2	tento
energie	energie	k1gFnSc2	energie
na	na	k7c4	na
jiné	jiný	k2eAgFnPc4d1	jiná
formy	forma	k1gFnPc4	forma
bývá	bývat	k5eAaImIp3nS	bývat
nesprávně	správně	k6eNd1	správně
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k8xC	jako
přeměna	přeměna	k1gFnSc1	přeměna
hmoty	hmota	k1gFnSc2	hmota
(	(	kIx(	(
<g/>
hmotnosti	hmotnost	k1gFnSc2	hmotnost
<g/>
)	)	kIx)	)
v	v	k7c6	v
energii	energie	k1gFnSc6	energie
<g/>
.	.	kIx.	.
</s>
<s>
Lidstvo	lidstvo	k1gNnSc1	lidstvo
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nezná	znát	k5eNaImIp3nS	znát
všechny	všechen	k3xTgFnPc4	všechen
možné	možný	k2eAgFnPc4d1	možná
formy	forma	k1gFnPc4	forma
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
vesmíru	vesmír	k1gInSc2	vesmír
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
dnes	dnes	k6eAd1	dnes
zcela	zcela	k6eAd1	zcela
neznámou	známý	k2eNgFnSc7d1	neznámá
formou	forma	k1gFnSc7	forma
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nese	nést	k5eAaImIp3nS	nést
přes	přes	k7c4	přes
70	[number]	k4	70
%	%	kIx~	%
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
prozatím	prozatím	k6eAd1	prozatím
říká	říkat	k5eAaImIp3nS	říkat
"	"	kIx"	"
<g/>
temná	temný	k2eAgFnSc1d1	temná
energie	energie	k1gFnSc1	energie
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
nějaká	nějaký	k3yIgFnSc1	nějaký
forma	forma	k1gFnSc1	forma
hmoty	hmota	k1gFnSc2	hmota
<g/>
,	,	kIx,	,
znamenalo	znamenat	k5eAaImAgNnS	znamenat
by	by	kYmCp3nS	by
to	ten	k3xDgNnSc1	ten
podstatnou	podstatný	k2eAgFnSc4d1	podstatná
změnu	změna	k1gFnSc4	změna
v	v	k7c6	v
představách	představa	k1gFnPc6	představa
o	o	k7c6	o
stavbě	stavba	k1gFnSc6	stavba
vesmíru	vesmír	k1gInSc2	vesmír
a	a	k8xC	a
pojmech	pojem	k1gInPc6	pojem
hmota	hmota	k1gFnSc1	hmota
a	a	k8xC	a
energie	energie	k1gFnSc1	energie
<g/>
.	.	kIx.	.
</s>
<s>
Množství	množství	k1gNnSc1	množství
energie	energie	k1gFnSc2	energie
spotřebované	spotřebovaný	k2eAgFnSc2d1	spotřebovaná
za	za	k7c4	za
jednotku	jednotka	k1gFnSc4	jednotka
času	čas	k1gInSc2	čas
udává	udávat	k5eAaImIp3nS	udávat
veličina	veličina	k1gFnSc1	veličina
příkon	příkon	k1gInSc1	příkon
<g/>
,	,	kIx,	,
poměr	poměr	k1gInSc1	poměr
vydané	vydaný	k2eAgFnSc2d1	vydaná
a	a	k8xC	a
dodané	dodaný	k2eAgFnSc2d1	dodaná
energie	energie	k1gFnSc2	energie
udává	udávat	k5eAaImIp3nS	udávat
veličina	veličina	k1gFnSc1	veličina
účinnost	účinnost	k1gFnSc1	účinnost
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
symbol	symbol	k1gInSc1	symbol
energie	energie	k1gFnSc2	energie
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
písmeno	písmeno	k1gNnSc1	písmeno
E.	E.	kA	E.
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
jednotka	jednotka	k1gFnSc1	jednotka
energie	energie	k1gFnSc2	energie
i	i	k8xC	i
práce	práce	k1gFnSc2	práce
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
SI	se	k3xPyFc3	se
je	být	k5eAaImIp3nS	být
joule	joule	k1gInSc1	joule
<g/>
,	,	kIx,	,
značka	značka	k1gFnSc1	značka
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
:	:	kIx,	:
J.	J.	kA	J.
Je	být	k5eAaImIp3nS	být
definován	definovat	k5eAaBmNgInS	definovat
jako	jako	k8xS	jako
práce	práce	k1gFnSc1	práce
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
vykoná	vykonat	k5eAaPmIp3nS	vykonat
síla	síla	k1gFnSc1	síla
1	[number]	k4	1
N	N	kA	N
působící	působící	k2eAgFnSc1d1	působící
po	po	k7c6	po
dráze	dráha	k1gFnSc6	dráha
1	[number]	k4	1
m.	m.	k?	m.
Další	další	k2eAgFnPc4d1	další
používané	používaný	k2eAgFnPc4d1	používaná
mimosoustavové	mimosoustavový	k2eAgFnPc4d1	mimosoustavový
jednotky	jednotka	k1gFnPc4	jednotka
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
kalorie	kalorie	k1gFnPc4	kalorie
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
cal	cal	k?	cal
<g/>
,	,	kIx,	,
rovná	rovnat	k5eAaImIp3nS	rovnat
se	s	k7c7	s
4,185	[number]	k4	4,185
J	J	kA	J
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
používala	používat	k5eAaImAgFnS	používat
se	se	k3xPyFc4	se
ve	v	k7c6	v
fyzice	fyzika	k1gFnSc6	fyzika
před	před	k7c7	před
zavedením	zavedení	k1gNnSc7	zavedení
metrické	metrický	k2eAgFnSc2d1	metrická
jednotky	jednotka	k1gFnSc2	jednotka
kilokalorie	kilokalorie	k1gFnSc2	kilokalorie
(	(	kIx(	(
<g/>
kcal	kcal	k1gInSc1	kcal
<g/>
,	,	kIx,	,
rovná	rovnat	k5eAaImIp3nS	rovnat
se	s	k7c7	s
4185	[number]	k4	4185
J	J	kA	J
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
zhruba	zhruba	k6eAd1	zhruba
množství	množství	k1gNnSc1	množství
energii	energie	k1gFnSc6	energie
potřebné	potřebný	k2eAgFnPc1d1	potřebná
pro	pro	k7c4	pro
ohřátí	ohřátý	k2eAgMnPc1d1	ohřátý
litru	litr	k1gInSc6	litr
vody	voda	k1gFnSc2	voda
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
stupeň	stupeň	k1gInSc4	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dodnes	dodnes	k6eAd1	dodnes
se	se	k3xPyFc4	se
<g />
.	.	kIx.	.
</s>
<s>
občas	občas	k6eAd1	občas
používá	používat	k5eAaImIp3nS	používat
při	při	k7c6	při
výpočtu	výpočet	k1gInSc6	výpočet
energetické	energetický	k2eAgFnSc2d1	energetická
hodnoty	hodnota	k1gFnSc2	hodnota
potravin	potravina	k1gFnPc2	potravina
elektronvolt	elektronvolt	k1gInSc1	elektronvolt
(	(	kIx(	(
<g/>
eV	eV	k?	eV
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
přibližně	přibližně	k6eAd1	přibližně
1,602	[number]	k4	1,602
<g/>
×	×	k?	×
<g/>
10	[number]	k4	10
<g/>
−	−	k?	−
<g/>
19	[number]	k4	19
J	J	kA	J
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
energie	energie	k1gFnSc1	energie
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
elektron	elektron	k1gInSc1	elektron
získá	získat	k5eAaPmIp3nS	získat
proběhnutím	proběhnutí	k1gNnSc7	proběhnutí
elektrického	elektrický	k2eAgNnSc2d1	elektrické
pole	pole	k1gNnSc2	pole
s	s	k7c7	s
potenciálem	potenciál	k1gInSc7	potenciál
jednoho	jeden	k4xCgMnSc2	jeden
voltu	volt	k1gInSc2	volt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
především	především	k9	především
v	v	k7c6	v
částicové	částicový	k2eAgFnSc6d1	částicová
<g />
.	.	kIx.	.
</s>
<s hack="1">
fyzice	fyzika	k1gFnSc3	fyzika
Tuna	tuna	k1gFnSc1	tuna
měrného	měrný	k2eAgNnSc2d1	měrné
paliva	palivo	k1gNnSc2	palivo
-	-	kIx~	-
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
v	v	k7c6	v
energetice	energetika	k1gFnSc6	energetika
kilowatthodina	kilowatthodina	k1gFnSc1	kilowatthodina
-	-	kIx~	-
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
v	v	k7c6	v
energetice	energetika	k1gFnSc6	energetika
a	a	k8xC	a
silové	silový	k2eAgFnSc6d1	silová
elektrotechnice	elektrotechnika	k1gFnSc6	elektrotechnika
Ze	z	k7c2	z
speciální	speciální	k2eAgFnSc2d1	speciální
teorie	teorie	k1gFnSc2	teorie
relativity	relativita	k1gFnSc2	relativita
plyne	plynout	k5eAaImIp3nS	plynout
<g/>
,	,	kIx,	,
že	že	k8xS	že
hmotnost	hmotnost	k1gFnSc1	hmotnost
(	(	kIx(	(
<g/>
nikoliv	nikoliv	k9	nikoliv
hmota	hmota	k1gFnSc1	hmota
<g/>
)	)	kIx)	)
a	a	k8xC	a
energie	energie	k1gFnPc1	energie
jsou	být	k5eAaImIp3nP	být
ekvivalentní	ekvivalentní	k2eAgFnPc1d1	ekvivalentní
podle	podle	k7c2	podle
Einsteinova	Einsteinův	k2eAgInSc2d1	Einsteinův
vztahu	vztah	k1gInSc2	vztah
E	E	kA	E
=	=	kIx~	=
mc	mc	k?	mc
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
m	m	kA	m
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
m	m	kA	m
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
,	,	kIx,	,
:	:	kIx,	:
:	:	kIx,	:
:	:	kIx,	:
c	c	k0	c
:	:	kIx,	:
:	:	kIx,	:
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
c	c	k0	c
<g/>
}	}	kIx)	}
je	být	k5eAaImIp3nS	být
rychlost	rychlost	k1gFnSc1	rychlost
světla	světlo	k1gNnSc2	světlo
ve	v	k7c6	v
vakuu	vakuum	k1gNnSc6	vakuum
<g/>
.	.	kIx.	.
</s>
<s>
Druhy	druh	k1gInPc1	druh
energie	energie	k1gFnSc2	energie
se	se	k3xPyFc4	se
rozlišují	rozlišovat	k5eAaImIp3nP	rozlišovat
např.	např.	kA	např.
podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
působí	působit	k5eAaImIp3nS	působit
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
zdroje	zdroj	k1gInSc2	zdroj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
energii	energie	k1gFnSc4	energie
vydává	vydávat	k5eAaPmIp3nS	vydávat
<g/>
,	,	kIx,	,
ap.	ap.	kA	ap.
<g/>
:	:	kIx,	:
Mechanická	mechanický	k2eAgFnSc1d1	mechanická
energie	energie	k1gFnSc1	energie
Kinetická	kinetický	k2eAgFnSc1d1	kinetická
(	(	kIx(	(
<g/>
pohybová	pohybový	k2eAgFnSc1d1	pohybová
<g/>
)	)	kIx)	)
energie	energie	k1gFnSc1	energie
Potenciální	potenciální	k2eAgFnSc1d1	potenciální
(	(	kIx(	(
<g/>
polohová	polohový	k2eAgFnSc1d1	polohová
<g/>
)	)	kIx)	)
energie	energie	k1gFnSc1	energie
Gravitační	gravitační	k2eAgFnSc2d1	gravitační
potenciální	potenciální	k2eAgFnSc2d1	potenciální
energie	energie	k1gFnSc2	energie
Potenciální	potenciální	k2eAgFnSc2d1	potenciální
energie	energie	k1gFnSc2	energie
pružnosti	pružnost	k1gFnSc2	pružnost
Tlaková	tlakový	k2eAgFnSc1d1	tlaková
potenciální	potenciální	k2eAgFnSc1d1	potenciální
energie	energie	k1gFnSc1	energie
Elektrická	elektrický	k2eAgFnSc1d1	elektrická
energie	energie	k1gFnSc2	energie
Magnetická	magnetický	k2eAgFnSc1d1	magnetická
energie	energie	k1gFnSc1	energie
Energie	energie	k1gFnSc2	energie
záření	záření	k1gNnSc2	záření
Energie	energie	k1gFnSc2	energie
vln	vlna	k1gFnPc2	vlna
Vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
energie	energie	k1gFnSc2	energie
Tepelná	tepelný	k2eAgFnSc1d1	tepelná
energie	energie	k1gFnSc1	energie
je	být	k5eAaImIp3nS	být
spojena	spojit	k5eAaPmNgFnS	spojit
s	s	k7c7	s
chaotickým	chaotický	k2eAgInSc7d1	chaotický
pohybem	pohyb	k1gInSc7	pohyb
(	(	kIx(	(
<g/>
vibrací	vibrace	k1gFnPc2	vibrace
a	a	k8xC	a
rotací	rotace	k1gFnPc2	rotace
<g/>
)	)	kIx)	)
molekul	molekula	k1gFnPc2	molekula
<g/>
.	.	kIx.	.
</s>
<s>
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
energie	energie	k1gFnSc1	energie
Chemická	chemický	k2eAgFnSc1d1	chemická
energie	energie	k1gFnSc1	energie
(	(	kIx(	(
<g/>
energie	energie	k1gFnSc1	energie
chemické	chemický	k2eAgFnSc2d1	chemická
vazby	vazba	k1gFnSc2	vazba
<g/>
,	,	kIx,	,
vazebná	vazebný	k2eAgFnSc1d1	vazebná
energie	energie	k1gFnSc1	energie
<g/>
)	)	kIx)	)
Klidová	klidový	k2eAgFnSc1d1	klidová
energie	energie	k1gFnSc1	energie
Sluneční	sluneční	k2eAgFnSc2d1	sluneční
energie	energie	k1gFnSc2	energie
Vodní	vodní	k2eAgFnSc2d1	vodní
energie	energie	k1gFnSc2	energie
Větrná	větrný	k2eAgFnSc1d1	větrná
energie	energie	k1gFnSc1	energie
Geotermální	geotermální	k2eAgFnSc2d1	geotermální
energie	energie	k1gFnSc2	energie
Energie	energie	k1gFnSc2	energie
mořských	mořský	k2eAgFnPc2d1	mořská
vln	vlna	k1gFnPc2	vlna
Parní	parní	k2eAgFnSc2d1	parní
energie	energie	k1gFnSc2	energie
Svalová	svalový	k2eAgFnSc1d1	svalová
energie	energie	k1gFnSc1	energie
Světelná	světelný	k2eAgFnSc1d1	světelná
energie	energie	k1gFnSc1	energie
Energie	energie	k1gFnSc1	energie
ohně	oheň	k1gInSc2	oheň
Jaderná	jaderný	k2eAgFnSc1d1	jaderná
energie	energie	k1gFnSc1	energie
Energie	energie	k1gFnSc2	energie
jednoho	jeden	k4xCgInSc2	jeden
druhu	druh	k1gInSc2	druh
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
přeměňuje	přeměňovat	k5eAaImIp3nS	přeměňovat
v	v	k7c4	v
jiný	jiný	k2eAgInSc4d1	jiný
druh	druh	k1gInSc4	druh
konáním	konání	k1gNnSc7	konání
práce	práce	k1gFnSc2	práce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
makroskopickém	makroskopický	k2eAgInSc6d1	makroskopický
popisu	popis	k1gInSc6	popis
se	se	k3xPyFc4	se
však	však	k9	však
od	od	k7c2	od
mikroskopického	mikroskopický	k2eAgNnSc2d1	mikroskopické
působení	působení	k1gNnSc2	působení
silových	silový	k2eAgFnPc2d1	silová
interakcí	interakce	k1gFnPc2	interakce
zpravidla	zpravidla	k6eAd1	zpravidla
odhlíží	odhlížet	k5eAaImIp3nS	odhlížet
a	a	k8xC	a
přeměna	přeměna	k1gFnSc1	přeměna
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
jevit	jevit	k5eAaImF	jevit
jako	jako	k9	jako
bezprostřední	bezprostřední	k2eAgFnPc4d1	bezprostřední
(	(	kIx(	(
<g/>
při	při	k7c6	při
anihilaci	anihilace	k1gFnSc6	anihilace
částice	částice	k1gFnSc2	částice
a	a	k8xC	a
antičástice	antičástice	k1gFnSc2	antičástice
látky	látka	k1gFnSc2	látka
v	v	k7c6	v
klidu	klid	k1gInSc6	klid
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
zavádějí	zavádět	k5eAaImIp3nP	zavádět
nové	nový	k2eAgFnPc4d1	nová
veličiny	veličina	k1gFnPc4	veličina
fenomenologicky	fenomenologicky	k6eAd1	fenomenologicky
popisující	popisující	k2eAgFnSc4d1	popisující
disipaci	disipace	k1gFnSc4	disipace
či	či	k8xC	či
skrytý	skrytý	k2eAgInSc4d1	skrytý
přenos	přenos	k1gInSc4	přenos
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
formulují	formulovat	k5eAaImIp3nP	formulovat
se	se	k3xPyFc4	se
nová	nový	k2eAgNnPc1d1	nové
pravidla	pravidlo	k1gNnPc1	pravidlo
pro	pro	k7c4	pro
energetické	energetický	k2eAgInPc4d1	energetický
děje	děj	k1gInPc4	děj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
termodynamice	termodynamika	k1gFnSc6	termodynamika
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
zavádí	zavádět	k5eAaImIp3nS	zavádět
teplo	teplo	k1gNnSc1	teplo
a	a	k8xC	a
přeměna	přeměna	k1gFnSc1	přeměna
energie	energie	k1gFnSc2	energie
se	se	k3xPyFc4	se
v	v	k7c6	v
termodynamickém	termodynamický	k2eAgInSc6d1	termodynamický
popisu	popis	k1gInSc6	popis
řídí	řídit	k5eAaImIp3nS	řídit
prvním	první	k4xOgInSc7	první
a	a	k8xC	a
druhým	druhý	k4xOgInSc7	druhý
zákonem	zákon	k1gInSc7	zákon
termodynamiky	termodynamika	k1gFnSc2	termodynamika
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
druh	druh	k1gInSc1	druh
energie	energie	k1gFnSc2	energie
(	(	kIx(	(
<g/>
přeměňované	přeměňovaný	k2eAgInPc1d1	přeměňovaný
či	či	k8xC	či
přeměněné	přeměněný	k2eAgInPc1d1	přeměněný
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
zpravidla	zpravidla	k6eAd1	zpravidla
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
energii	energie	k1gFnSc4	energie
potenciální	potenciální	k2eAgFnSc4d1	potenciální
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
uložena	uložit	k5eAaPmNgFnS	uložit
<g/>
"	"	kIx"	"
v	v	k7c6	v
silovém	silový	k2eAgNnSc6d1	silové
poli	pole	k1gNnSc6	pole
(	(	kIx(	(
<g/>
polohová	polohový	k2eAgFnSc1d1	polohová
energie	energie	k1gFnSc1	energie
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
klidové	klidový	k2eAgFnSc2d1	klidová
hmotnosti	hmotnost	k1gFnSc2	hmotnost
(	(	kIx(	(
<g/>
klidová	klidový	k2eAgFnSc1d1	klidová
energie	energie	k1gFnSc1	energie
<g/>
)	)	kIx)	)
daného	daný	k2eAgInSc2d1	daný
fyzikálního	fyzikální	k2eAgInSc2d1	fyzikální
systému	systém	k1gInSc2	systém
i	i	k9	i
v	v	k7c6	v
jeho	jeho	k3xOp3gInSc6	jeho
relativním	relativní	k2eAgInSc6d1	relativní
klidu	klid	k1gInSc6	klid
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
je	být	k5eAaImIp3nS	být
energií	energie	k1gFnSc7	energie
dynamickou	dynamický	k2eAgFnSc7d1	dynamická
<g/>
,	,	kIx,	,
projevující	projevující	k2eAgFnSc7d1	projevující
se	se	k3xPyFc4	se
v	v	k7c6	v
časové	časový	k2eAgFnSc6d1	časová
přeměně	přeměna	k1gFnSc6	přeměna
či	či	k8xC	či
pohybu	pohyb	k1gInSc6	pohyb
(	(	kIx(	(
<g/>
kinetická	kinetický	k2eAgFnSc1d1	kinetická
energie	energie	k1gFnSc1	energie
<g/>
,	,	kIx,	,
energie	energie	k1gFnSc1	energie
vlnění	vlnění	k1gNnSc2	vlnění
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
jsou	být	k5eAaImIp3nP	být
jisté	jistý	k2eAgInPc1d1	jistý
druhy	druh	k1gInPc1	druh
energie	energie	k1gFnSc2	energie
zaváděny	zaváděn	k2eAgInPc1d1	zaváděn
jako	jako	k8xS	jako
teoretický	teoretický	k2eAgInSc1d1	teoretický
koncept	koncept	k1gInSc1	koncept
a	a	k8xC	a
současná	současný	k2eAgFnSc1d1	současná
fyzika	fyzika	k1gFnSc1	fyzika
zatím	zatím	k6eAd1	zatím
neumí	umět	k5eNaImIp3nS	umět
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
jejich	jejich	k3xOp3gFnSc4	jejich
podstatu	podstata	k1gFnSc4	podstata
ani	ani	k8xC	ani
zákony	zákon	k1gInPc1	zákon
jejich	jejich	k3xOp3gFnSc2	jejich
přeměny	přeměna	k1gFnSc2	přeměna
(	(	kIx(	(
<g/>
např.	např.	kA	např.
tzv.	tzv.	kA	tzv.
temná	temnat	k5eAaImIp3nS	temnat
energie	energie	k1gFnSc1	energie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
ani	ani	k8xC	ani
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
lze	lze	k6eAd1	lze
zachování	zachování	k1gNnSc4	zachování
energie	energie	k1gFnSc2	energie
či	či	k8xC	či
druhý	druhý	k4xOgInSc1	druhý
termodynamický	termodynamický	k2eAgInSc1d1	termodynamický
zákon	zákon	k1gInSc1	zákon
vztahovat	vztahovat	k5eAaImF	vztahovat
na	na	k7c4	na
vesmír	vesmír	k1gInSc4	vesmír
jako	jako	k8xC	jako
celek	celek	k1gInSc4	celek
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgFnPc1d1	různá
teorie	teorie	k1gFnPc1	teorie
nabízejí	nabízet	k5eAaImIp3nP	nabízet
různá	různý	k2eAgNnPc1d1	různé
řešení	řešení	k1gNnPc1	řešení
a	a	k8xC	a
experimenty	experiment	k1gInPc1	experiment
a	a	k8xC	a
pozorování	pozorování	k1gNnSc1	pozorování
nestačí	stačit	k5eNaBmIp3nS	stačit
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
vyvracení	vyvracení	k1gNnSc4	vyvracení
(	(	kIx(	(
<g/>
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
takové	takový	k3xDgNnSc4	takový
vyvrácení	vyvrácení	k1gNnSc4	vyvrácení
ani	ani	k8xC	ani
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
kvůli	kvůli	k7c3	kvůli
neopakovatelnosti	neopakovatelnost	k1gFnSc3	neopakovatelnost
vývoje	vývoj	k1gInSc2	vývoj
vesmíru	vesmír	k1gInSc2	vesmír
jako	jako	k8xC	jako
celku	celek	k1gInSc2	celek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přeměny	přeměna	k1gFnPc1	přeměna
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
jí	on	k3xPp3gFnSc2	on
vyvolané	vyvolaný	k2eAgFnSc2d1	vyvolaná
změny	změna	k1gFnSc2	změna
struktury	struktura	k1gFnSc2	struktura
molekul	molekula	k1gFnPc2	molekula
a	a	k8xC	a
látek	látka	k1gFnPc2	látka
jsou	být	k5eAaImIp3nP	být
podstatou	podstata	k1gFnSc7	podstata
chemických	chemický	k2eAgInPc2d1	chemický
a	a	k8xC	a
biologických	biologický	k2eAgInPc2d1	biologický
dějů	děj	k1gInPc2	děj
<g/>
.	.	kIx.	.
</s>
<s>
Využitím	využití	k1gNnSc7	využití
přeměny	přeměna	k1gFnSc2	přeměna
energie	energie	k1gFnSc2	energie
pro	pro	k7c4	pro
lidské	lidský	k2eAgInPc4d1	lidský
účely	účel	k1gInPc4	účel
se	se	k3xPyFc4	se
zabývají	zabývat	k5eAaImIp3nP	zabývat
energetika	energetika	k1gFnSc1	energetika
i	i	k8xC	i
další	další	k2eAgInPc1d1	další
obory	obor	k1gInPc1	obor
techniky	technika	k1gFnSc2	technika
(	(	kIx(	(
<g/>
elektrotechnika	elektrotechnika	k1gFnSc1	elektrotechnika
<g/>
,	,	kIx,	,
radiotechnika	radiotechnika	k1gFnSc1	radiotechnika
<g/>
,	,	kIx,	,
dopravní	dopravní	k2eAgFnSc1d1	dopravní
technika	technika	k1gFnSc1	technika
<g/>
,	,	kIx,	,
optika	optika	k1gFnSc1	optika
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
přeměnou	přeměna	k1gFnSc7	přeměna
energie	energie	k1gFnSc2	energie
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
podstatě	podstata	k1gFnSc6	podstata
spojeny	spojit	k5eAaPmNgFnP	spojit
i	i	k9	i
mnohé	mnohý	k2eAgFnPc1d1	mnohá
aplikace	aplikace	k1gFnPc1	aplikace
v	v	k7c6	v
hospodářství	hospodářství	k1gNnSc6	hospodářství
(	(	kIx(	(
<g/>
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
zemědělství	zemědělství	k1gNnSc1	zemědělství
<g/>
,	,	kIx,	,
doprava	doprava	k1gFnSc1	doprava
<g/>
,	,	kIx,	,
lékařství	lékařství	k1gNnSc1	lékařství
<g/>
)	)	kIx)	)
i	i	k9	i
společenském	společenský	k2eAgInSc6d1	společenský
životě	život	k1gInSc6	život
(	(	kIx(	(
<g/>
technické	technický	k2eAgInPc4d1	technický
prostředky	prostředek	k1gInPc4	prostředek
pro	pro	k7c4	pro
kulturní	kulturní	k2eAgFnSc4d1	kulturní
tvorbu	tvorba	k1gFnSc4	tvorba
<g/>
,	,	kIx,	,
didaktiku	didaktika	k1gFnSc4	didaktika
<g/>
,	,	kIx,	,
vědu	věda	k1gFnSc4	věda
a	a	k8xC	a
výzkum	výzkum	k1gInSc4	výzkum
i	i	k9	i
v	v	k7c6	v
humanitních	humanitní	k2eAgInPc6d1	humanitní
oborech	obor	k1gInPc6	obor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Energetika	energetika	k1gFnSc1	energetika
Práce	práce	k1gFnSc1	práce
Síla	síla	k1gFnSc1	síla
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
energie	energie	k1gFnSc2	energie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
energie	energie	k1gFnSc2	energie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
