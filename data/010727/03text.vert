<p>
<s>
Zinek	zinek	k1gInSc1	zinek
(	(	kIx(	(
<g/>
chemická	chemický	k2eAgFnSc1d1	chemická
značka	značka	k1gFnSc1	značka
Zn	zn	kA	zn
<g/>
,	,	kIx,	,
latinsky	latinsky	k6eAd1	latinsky
Zincum	Zincum	k1gInSc1	Zincum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
měkký	měkký	k2eAgInSc1d1	měkký
lehce	lehko	k6eAd1	lehko
tavitelný	tavitelný	k2eAgInSc1d1	tavitelný
kov	kov	k1gInSc1	kov
<g/>
,	,	kIx,	,
používaný	používaný	k2eAgInSc1d1	používaný
člověkem	člověk	k1gMnSc7	člověk
již	již	k6eAd1	již
od	od	k7c2	od
starověku	starověk	k1gInSc2	starověk
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
různých	různý	k2eAgFnPc2d1	různá
slitin	slitina	k1gFnPc2	slitina
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
barviv	barvivo	k1gNnPc2	barvivo
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
přítomnost	přítomnost	k1gFnSc1	přítomnost
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
je	být	k5eAaImIp3nS	být
nezbytná	zbytný	k2eNgFnSc1d1	zbytný
pro	pro	k7c4	pro
správný	správný	k2eAgInSc4d1	správný
vývoj	vývoj	k1gInSc4	vývoj
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Základní	základní	k2eAgFnPc1d1	základní
fyzikálně-chemické	fyzikálněhemický	k2eAgFnPc1d1	fyzikálně-chemická
vlastnosti	vlastnost	k1gFnPc1	vlastnost
==	==	k?	==
</s>
</p>
<p>
<s>
Zinek	zinek	k1gInSc1	zinek
je	být	k5eAaImIp3nS	být
modrobílý	modrobílý	k2eAgInSc1d1	modrobílý
kovový	kovový	k2eAgInSc1d1	kovový
prvek	prvek	k1gInSc1	prvek
se	s	k7c7	s
silným	silný	k2eAgInSc7d1	silný
leskem	lesk	k1gInSc7	lesk
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
však	však	k9	však
na	na	k7c6	na
vlhkém	vlhký	k2eAgInSc6d1	vlhký
vzduchu	vzduch	k1gInSc6	vzduch
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
<g/>
.	.	kIx.	.
</s>
<s>
Mřížka	mřížka	k1gFnSc1	mřížka
zinku	zinek	k1gInSc2	zinek
krystaluje	krystalovat	k5eAaImIp3nS	krystalovat
v	v	k7c6	v
hexagonálním	hexagonální	k2eAgNnSc6d1	hexagonální
těsném	těsný	k2eAgNnSc6d1	těsné
uspořádání	uspořádání	k1gNnSc6	uspořádání
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
normální	normální	k2eAgFnPc4d1	normální
teploty	teplota	k1gFnPc4	teplota
je	být	k5eAaImIp3nS	být
křehký	křehký	k2eAgInSc1d1	křehký
<g/>
,	,	kIx,	,
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
teplot	teplota	k1gFnPc2	teplota
100	[number]	k4	100
<g/>
–	–	k?	–
<g/>
150	[number]	k4	150
°	°	k?	°
<g/>
C	C	kA	C
je	být	k5eAaImIp3nS	být
tažný	tažný	k2eAgInSc1d1	tažný
a	a	k8xC	a
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
válcovat	válcovat	k5eAaImF	válcovat
na	na	k7c4	na
plech	plech	k1gInSc4	plech
a	a	k8xC	a
vytahovat	vytahovat	k5eAaImF	vytahovat
na	na	k7c4	na
dráty	drát	k1gInPc4	drát
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
200	[number]	k4	200
°	°	k?	°
<g/>
C	C	kA	C
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
křehký	křehký	k2eAgInSc1d1	křehký
a	a	k8xC	a
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
rozetřít	rozetřít	k5eAaPmF	rozetřít
na	na	k7c4	na
prach	prach	k1gInSc4	prach
<g/>
.	.	kIx.	.
</s>
<s>
Zinek	zinek	k1gInSc1	zinek
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
snadno	snadno	k6eAd1	snadno
tavitelný	tavitelný	k2eAgInSc1d1	tavitelný
a	a	k8xC	a
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejsnáze	snadno	k6eAd3	snadno
těkajícím	těkající	k2eAgInPc3d1	těkající
kovům	kov	k1gInPc3	kov
<g/>
.	.	kIx.	.
</s>
<s>
Tepelná	tepelný	k2eAgFnSc1d1	tepelná
vodivost	vodivost	k1gFnSc1	vodivost
zinku	zinek	k1gInSc2	zinek
je	být	k5eAaImIp3nS	být
61	[number]	k4	61
<g/>
–	–	k?	–
<g/>
64	[number]	k4	64
%	%	kIx~	%
a	a	k8xC	a
elektrická	elektrický	k2eAgFnSc1d1	elektrická
vodivost	vodivost	k1gFnSc1	vodivost
27	[number]	k4	27
%	%	kIx~	%
vodivosti	vodivost	k1gFnSc3	vodivost
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
teplotách	teplota	k1gFnPc6	teplota
pod	pod	k7c4	pod
0,875	[number]	k4	0,875
K	K	kA	K
je	být	k5eAaImIp3nS	být
supravodivý	supravodivý	k2eAgInSc1d1	supravodivý
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
přechodné	přechodný	k2eAgInPc4d1	přechodný
prvky	prvek	k1gInPc4	prvek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
mají	mít	k5eAaImIp3nP	mít
valenční	valenční	k2eAgInPc1d1	valenční
elektrony	elektron	k1gInPc1	elektron
v	v	k7c6	v
d-sféře	dféra	k1gFnSc6	d-sféra
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
oxidačním	oxidační	k2eAgNnSc6d1	oxidační
čísle	číslo	k1gNnSc6	číslo
II	II	kA	II
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
známe	znát	k5eAaImIp1nP	znát
i	i	k9	i
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
zinek	zinek	k1gInSc1	zinek
v	v	k7c6	v
oxidačním	oxidační	k2eAgNnSc6d1	oxidační
čísle	číslo	k1gNnSc6	číslo
I.	I.	kA	I.
Ty	ten	k3xDgInPc1	ten
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
ionty	ion	k1gInPc1	ion
Zn	zn	kA	zn
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
a	a	k8xC	a
je	on	k3xPp3gFnPc4	on
nutno	nutno	k6eAd1	nutno
je	být	k5eAaImIp3nS	být
stabilizovat	stabilizovat	k5eAaBmF	stabilizovat
stericky	stericky	k6eAd1	stericky
objemnými	objemný	k2eAgInPc7d1	objemný
ligandy	ligand	k1gInPc7	ligand
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
silných	silný	k2eAgFnPc6d1	silná
minerálních	minerální	k2eAgFnPc6d1	minerální
kyselinách	kyselina	k1gFnPc6	kyselina
se	se	k3xPyFc4	se
zinek	zinek	k1gInSc1	zinek
velmi	velmi	k6eAd1	velmi
ochotně	ochotně	k6eAd1	ochotně
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
za	za	k7c2	za
vývoje	vývoj	k1gInSc2	vývoj
plynného	plynný	k2eAgInSc2d1	plynný
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
je	být	k5eAaImIp3nS	být
zinek	zinek	k1gInSc1	zinek
stálý	stálý	k2eAgInSc1d1	stálý
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
pokryje	pokrýt	k5eAaPmIp3nS	pokrýt
tenkou	tenký	k2eAgFnSc7d1	tenká
vrstvičkou	vrstvička	k1gFnSc7	vrstvička
oxidu	oxid	k1gInSc2	oxid
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
jej	on	k3xPp3gMnSc4	on
účinně	účinně	k6eAd1	účinně
chrání	chránit	k5eAaImIp3nS	chránit
proti	proti	k7c3	proti
korozi	koroze	k1gFnSc3	koroze
vzdušným	vzdušný	k2eAgInSc7d1	vzdušný
kyslíkem	kyslík	k1gInSc7	kyslík
i	i	k8xC	i
vlhkostí	vlhkost	k1gFnSc7	vlhkost
(	(	kIx(	(
<g/>
vodou	voda	k1gFnSc7	voda
<g/>
)	)	kIx)	)
–	–	k?	–
tzv.	tzv.	kA	tzv.
pasivace	pasivace	k1gFnSc2	pasivace
<g/>
.	.	kIx.	.
</s>
<s>
Zinek	zinek	k1gInSc1	zinek
se	se	k3xPyFc4	se
ale	ale	k9	ale
také	také	k9	také
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
v	v	k7c6	v
roztocích	roztok	k1gInPc6	roztok
hydroxidů	hydroxid	k1gInPc2	hydroxid
<g/>
,	,	kIx,	,
vodném	vodný	k2eAgInSc6d1	vodný
amoniaku	amoniak	k1gInSc6	amoniak
a	a	k8xC	a
za	za	k7c2	za
tepla	teplo	k1gNnSc2	teplo
také	také	k9	také
v	v	k7c6	v
chloridu	chlorid	k1gInSc6	chlorid
amonném	amonný	k2eAgInSc6d1	amonný
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
projevem	projev	k1gInSc7	projev
jeho	jeho	k3xOp3gFnSc2	jeho
amfoterity	amfoterit	k1gInPc4	amfoterit
(	(	kIx(	(
<g/>
rozpouštění	rozpouštění	k1gNnSc3	rozpouštění
v	v	k7c6	v
kyselinách	kyselina	k1gFnPc6	kyselina
i	i	k8xC	i
hydroxidech	hydroxid	k1gInPc6	hydroxid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
zinek	zinek	k1gInSc4	zinek
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
čistém	čistý	k2eAgInSc6d1	čistý
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
rozpouštění	rozpouštění	k1gNnSc1	rozpouštění
v	v	k7c6	v
kyselinách	kyselina	k1gFnPc6	kyselina
ani	ani	k8xC	ani
hydroxidech	hydroxid	k1gInPc6	hydroxid
neprobíhá	probíhat	k5eNaImIp3nS	probíhat
nebo	nebo	k8xC	nebo
probíhá	probíhat	k5eAaImIp3nS	probíhat
velmi	velmi	k6eAd1	velmi
pomalu	pomalu	k6eAd1	pomalu
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zinek	zinek	k1gInSc1	zinek
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
při	při	k7c6	při
zahřátí	zahřátí	k1gNnSc6	zahřátí
hoří	hořet	k5eAaImIp3nS	hořet
jasně	jasně	k6eAd1	jasně
svítivým	svítivý	k2eAgInSc7d1	svítivý
modrozeleným	modrozelený	k2eAgInSc7d1	modrozelený
plamenem	plamen	k1gInSc7	plamen
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
vzniká	vznikat	k5eAaImIp3nS	vznikat
bílý	bílý	k2eAgInSc1d1	bílý
oxid	oxid	k1gInSc1	oxid
zinečnatý	zinečnatý	k2eAgInSc1d1	zinečnatý
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
červeného	červený	k2eAgInSc2d1	červený
žáru	žár	k1gInSc2	žár
se	se	k3xPyFc4	se
zinek	zinek	k1gInSc1	zinek
oxiduje	oxidovat	k5eAaBmIp3nS	oxidovat
také	také	k9	také
vodní	vodní	k2eAgFnSc7d1	vodní
parou	para	k1gFnSc7	para
a	a	k8xC	a
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
redukuje	redukovat	k5eAaBmIp3nS	redukovat
na	na	k7c4	na
oxid	oxid	k1gInSc4	oxid
uhelnatý	uhelnatý	k2eAgInSc4d1	uhelnatý
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
halogeny	halogen	k1gInPc7	halogen
reaguje	reagovat	k5eAaBmIp3nS	reagovat
zinek	zinek	k1gInSc1	zinek
velmi	velmi	k6eAd1	velmi
neochotně	ochotně	k6eNd1	ochotně
a	a	k8xC	a
pouze	pouze	k6eAd1	pouze
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
vlhkosti	vlhkost	k1gFnSc2	vlhkost
<g/>
.	.	kIx.	.
</s>
<s>
Sulfan	Sulfan	k1gInSc1	Sulfan
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
zinek	zinek	k1gInSc4	zinek
za	za	k7c2	za
normální	normální	k2eAgFnSc2d1	normální
teploty	teplota	k1gFnSc2	teplota
a	a	k8xC	a
vzniká	vznikat	k5eAaImIp3nS	vznikat
tak	tak	k9	tak
sulfid	sulfid	k1gInSc1	sulfid
zinečnatý	zinečnatý	k2eAgInSc1d1	zinečnatý
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
tepla	teplo	k1gNnSc2	teplo
se	se	k3xPyFc4	se
zinek	zinek	k1gInSc1	zinek
slučuje	slučovat	k5eAaImIp3nS	slučovat
také	také	k9	také
se	s	k7c7	s
sírou	síra	k1gFnSc7	síra
a	a	k8xC	a
fosforem	fosfor	k1gInSc7	fosfor
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
dusíkem	dusík	k1gInSc7	dusík
<g/>
,	,	kIx,	,
vodíkem	vodík	k1gInSc7	vodík
a	a	k8xC	a
uhlíkem	uhlík	k1gInSc7	uhlík
se	se	k3xPyFc4	se
neslučuje	slučovat	k5eNaImIp3nS	slučovat
vůbec	vůbec	k9	vůbec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
amoniakem	amoniak	k1gInSc7	amoniak
tvoří	tvořit	k5eAaImIp3nP	tvořit
za	za	k7c2	za
vysokých	vysoký	k2eAgFnPc2d1	vysoká
teplot	teplota	k1gFnPc2	teplota
nitrid	nitrid	k1gInSc1	nitrid
zinečnatý	zinečnatý	k2eAgMnSc1d1	zinečnatý
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
velkým	velký	k2eAgNnSc7d1	velké
množstvím	množství	k1gNnSc7	množství
kovů	kov	k1gInPc2	kov
je	být	k5eAaImIp3nS	být
zinek	zinek	k1gInSc1	zinek
neomezeně	omezeně	k6eNd1	omezeně
mísitelný	mísitelný	k2eAgInSc1d1	mísitelný
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nS	tvořit
slitiny	slitina	k1gFnSc2	slitina
a	a	k8xC	a
s	s	k7c7	s
některými	některý	k3yIgNnPc7	některý
tvoří	tvořit	k5eAaImIp3nP	tvořit
dokonce	dokonce	k9	dokonce
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historický	historický	k2eAgInSc1d1	historický
vývoj	vývoj	k1gInSc1	vývoj
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgNnSc1	první
použití	použití	k1gNnSc1	použití
zinku	zinek	k1gInSc2	zinek
lze	lze	k6eAd1	lze
datovat	datovat	k5eAaImF	datovat
ve	v	k7c6	v
starověku	starověk	k1gInSc6	starověk
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgMnS	používat
ve	v	k7c6	v
slitině	slitina	k1gFnSc6	slitina
s	s	k7c7	s
mědí	měď	k1gFnSc7	měď
jako	jako	k8xC	jako
mosaz	mosaz	k1gFnSc1	mosaz
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
již	již	k6eAd1	již
ve	v	k7c6	v
starověkém	starověký	k2eAgInSc6d1	starověký
Egyptě	Egypt	k1gInSc6	Egypt
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1400	[number]	k4	1400
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
nebo	nebo	k8xC	nebo
Homérově	Homérův	k2eAgFnSc3d1	Homérova
době	doba	k1gFnSc3	doba
<g/>
.	.	kIx.	.
</s>
<s>
Mosaz	mosaz	k1gFnSc1	mosaz
se	se	k3xPyFc4	se
získávala	získávat	k5eAaImAgFnS	získávat
tavením	tavení	k1gNnSc7	tavení
mědi	měď	k1gFnSc2	měď
se	s	k7c7	s
zinkovou	zinkový	k2eAgFnSc7d1	zinková
rudou	ruda	k1gFnSc7	ruda
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
Řekové	Řek	k1gMnPc1	Řek
označovali	označovat	k5eAaImAgMnP	označovat
jako	jako	k8xC	jako
cadmia	cadmia	k1gFnSc1	cadmia
–	–	k?	–
časem	časem	k6eAd1	časem
se	se	k3xPyFc4	se
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
názvu	název	k1gInSc2	název
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
název	název	k1gInSc1	název
kalamín	kalamína	k1gFnPc2	kalamína
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc1d3	nejstarší
známá	známý	k2eAgFnSc1d1	známá
zinková	zinkový	k2eAgFnSc1d1	zinková
ruda	ruda	k1gFnSc1	ruda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čistý	čistý	k2eAgInSc1d1	čistý
zinek	zinek	k1gInSc1	zinek
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
poprvé	poprvé	k6eAd1	poprvé
připravit	připravit	k5eAaPmF	připravit
ve	v	k7c6	v
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
znalost	znalost	k1gFnSc1	znalost
přenesla	přenést	k5eAaPmAgFnS	přenést
do	do	k7c2	do
Činy	čina	k1gFnSc2	čina
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
období	období	k1gNnSc6	období
dynastie	dynastie	k1gFnSc2	dynastie
Ming	Minga	k1gFnPc2	Minga
v	v	k7c6	v
letech	let	k1gInPc6	let
1368	[number]	k4	1368
<g/>
–	–	k?	–
<g/>
1644	[number]	k4	1644
používaly	používat	k5eAaImAgFnP	používat
zinkové	zinkový	k2eAgFnPc1d1	zinková
mince	mince	k1gFnPc1	mince
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
nebyla	být	k5eNaImAgFnS	být
výroba	výroba	k1gFnSc1	výroba
zinku	zinek	k1gInSc2	zinek
známa	znám	k2eAgFnSc1d1	známa
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
zinek	zinek	k1gInSc1	zinek
v	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
a	a	k8xC	a
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
dovážel	dovážet	k5eAaImAgMnS	dovážet
z	z	k7c2	z
Číny	Čína	k1gFnSc2	Čína
loděmi	loď	k1gFnPc7	loď
nizozemské	nizozemský	k2eAgFnSc2d1	nizozemská
Východoindické	východoindický	k2eAgFnSc2d1	Východoindická
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
výroba	výroba	k1gFnSc1	výroba
zinku	zinek	k1gInSc2	zinek
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
započala	započnout	k5eAaPmAgFnS	započnout
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Bristolu	Bristol	k1gInSc2	Bristol
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
započalo	započnout	k5eAaPmAgNnS	započnout
s	s	k7c7	s
výrobou	výroba	k1gFnSc7	výroba
také	také	k9	také
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
a	a	k8xC	a
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
.	.	kIx.	.
</s>
<s>
Původ	původ	k1gInSc1	původ
slova	slovo	k1gNnSc2	slovo
zinek	zinek	k1gInSc1	zinek
není	být	k5eNaImIp3nS	být
sice	sice	k8xC	sice
úplně	úplně	k6eAd1	úplně
jasný	jasný	k2eAgInSc1d1	jasný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejčastější	častý	k2eAgFnSc1d3	nejčastější
domněnka	domněnka	k1gFnSc1	domněnka
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
německého	německý	k2eAgNnSc2d1	německé
slova	slovo	k1gNnSc2	slovo
Zinke	Zink	k1gFnSc2	Zink
(	(	kIx(	(
<g/>
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
"	"	kIx"	"
<g/>
bodec	bodec	k1gInSc1	bodec
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
zub	zub	k1gInSc1	zub
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
podle	podle	k7c2	podle
vzhledu	vzhled	k1gInSc2	vzhled
kovu	kov	k1gInSc2	kov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
je	být	k5eAaImIp3nS	být
zinek	zinek	k1gInSc1	zinek
poměrně	poměrně	k6eAd1	poměrně
bohatě	bohatě	k6eAd1	bohatě
zastoupen	zastoupit	k5eAaPmNgMnS	zastoupit
<g/>
.	.	kIx.	.
</s>
<s>
Průměrný	průměrný	k2eAgInSc1d1	průměrný
obsah	obsah	k1gInSc1	obsah
činí	činit	k5eAaImIp3nS	činit
kolem	kolem	k7c2	kolem
100	[number]	k4	100
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
,	,	kIx,	,
čemuž	což	k3yRnSc3	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
76	[number]	k4	76
ppm	ppm	k?	ppm
(	(	kIx(	(
<g/>
parts	partsa	k1gFnPc2	partsa
per	pero	k1gNnPc2	pero
milion	milion	k4xCgInSc4	milion
=	=	kIx~	=
počet	počet	k1gInSc4	počet
částic	částice	k1gFnPc2	částice
na	na	k7c4	na
1	[number]	k4	1
milion	milion	k4xCgInSc4	milion
částic	částice	k1gFnPc2	částice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgNnSc7	tento
rozšířením	rozšíření	k1gNnSc7	rozšíření
v	v	k7c6	v
zemské	zemský	k2eAgFnSc6d1	zemská
kůře	kůra	k1gFnSc6	kůra
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
k	k	k7c3	k
prvkům	prvek	k1gInPc3	prvek
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
rubidium	rubidium	k1gNnSc1	rubidium
(	(	kIx(	(
<g/>
78	[number]	k4	78
ppm	ppm	k?	ppm
<g/>
)	)	kIx)	)
a	a	k8xC	a
měď	měď	k1gFnSc4	měď
(	(	kIx(	(
<g/>
68	[number]	k4	68
ppm	ppm	k?	ppm
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
v	v	k7c6	v
mořské	mořský	k2eAgFnSc6d1	mořská
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
koncentrace	koncentrace	k1gFnSc1	koncentrace
značně	značně	k6eAd1	značně
vysoká	vysoký	k2eAgFnSc1d1	vysoká
–	–	k?	–
0,01	[number]	k4	0,01
miligramu	miligram	k1gInSc2	miligram
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
litru	litr	k1gInSc6	litr
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
vesmíru	vesmír	k1gInSc6	vesmír
připadá	připadat	k5eAaPmIp3nS	připadat
na	na	k7c4	na
jeden	jeden	k4xCgInSc4	jeden
atom	atom	k1gInSc4	atom
zinku	zinek	k1gInSc2	zinek
přibližně	přibližně	k6eAd1	přibližně
100	[number]	k4	100
milionů	milion	k4xCgInPc2	milion
atomů	atom	k1gInPc2	atom
vodíku	vodík	k1gInSc2	vodík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
minerálem	minerál	k1gInSc7	minerál
a	a	k8xC	a
rudou	ruda	k1gFnSc7	ruda
pro	pro	k7c4	pro
průmyslovou	průmyslový	k2eAgFnSc4d1	průmyslová
výrobu	výroba	k1gFnSc4	výroba
zinku	zinek	k1gInSc2	zinek
je	být	k5eAaImIp3nS	být
sfalerit	sfalerit	k1gInSc1	sfalerit
neboli	neboli	k8xC	neboli
blejno	blejno	k1gNnSc1	blejno
zinkové	zinkový	k2eAgFnSc2d1	zinková
ZnS	ZnS	k1gFnSc2	ZnS
<g/>
,	,	kIx,	,
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
v	v	k7c6	v
malém	malý	k2eAgNnSc6d1	malé
množství	množství	k1gNnSc6	množství
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
také	také	k9	také
další	další	k2eAgInSc4d1	další
minerál	minerál	k1gInSc4	minerál
se	s	k7c7	s
složením	složení	k1gNnSc7	složení
ZnS	ZnS	k1gFnSc2	ZnS
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
jiné	jiný	k2eAgFnSc6d1	jiná
krystalové	krystalový	k2eAgFnSc6d1	krystalová
modifikaci	modifikace	k1gFnSc6	modifikace
známý	známý	k2eAgInSc1d1	známý
jako	jako	k8xC	jako
wurtzit	wurtzit	k1gInSc1	wurtzit
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgInPc7d1	další
minerály	minerál	k1gInPc7	minerál
zinku	zinek	k1gInSc2	zinek
jsou	být	k5eAaImIp3nP	být
smithsonit	smithsonit	k1gInSc4	smithsonit
neboli	neboli	k8xC	neboli
kalamín	kalamín	k1gInSc4	kalamín
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
ZnCO	ZnCO	k1gFnSc7	ZnCO
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
kalamín	kalamín	k1gInSc1	kalamín
křemičitý	křemičitý	k2eAgInSc1d1	křemičitý
Zn	zn	kA	zn
<g/>
2	[number]	k4	2
<g/>
SiO	SiO	k1gFnSc1	SiO
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
<g/>
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
,	,	kIx,	,
willemit	willemit	k1gInSc1	willemit
Zn	zn	kA	zn
<g/>
2	[number]	k4	2
<g/>
SiO	SiO	k1gFnSc1	SiO
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
troosit	troosita	k1gFnPc2	troosita
(	(	kIx(	(
<g/>
Zn	zn	kA	zn
<g/>
,	,	kIx,	,
Mn	Mn	k1gFnSc1	Mn
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
SiO	SiO	k1gFnPc2	SiO
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
zinkit	zinkit	k1gInSc1	zinkit
neboli	neboli	k8xC	neboli
červená	červený	k2eAgFnSc1d1	červená
ruda	ruda	k1gFnSc1	ruda
zinková	zinkový	k2eAgFnSc1d1	zinková
ZnO	ZnO	k1gFnSc7	ZnO
<g/>
,	,	kIx,	,
franklinit	franklinit	k5eAaPmF	franklinit
(	(	kIx(	(
<g/>
Zn	zn	kA	zn
<g/>
,	,	kIx,	,
Mn	Mn	k1gFnSc1	Mn
<g/>
)	)	kIx)	)
<g/>
O.	O.	kA	O.
<g/>
Fe	Fe	k1gFnSc1	Fe
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
zinečnatý	zinečnatý	k2eAgInSc1d1	zinečnatý
spinel	spinel	k1gInSc1	spinel
ZnO	ZnO	k1gFnPc2	ZnO
<g/>
.	.	kIx.	.
<g/>
Al	ala	k1gFnPc2	ala
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
3	[number]	k4	3
a	a	k8xC	a
hemimorfit	hemimorfit	k1gInSc1	hemimorfit
Zn	zn	kA	zn
<g/>
4	[number]	k4	4
<g/>
Si	se	k3xPyFc3	se
<g/>
2	[number]	k4	2
<g/>
O	o	k7c4	o
<g/>
7	[number]	k4	7
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Vzácně	vzácně	k6eAd1	vzácně
se	se	k3xPyFc4	se
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
můžeme	moct	k5eAaImIp1nP	moct
setkat	setkat	k5eAaPmF	setkat
i	i	k9	i
s	s	k7c7	s
elementárním	elementární	k2eAgInSc7d1	elementární
<g/>
,	,	kIx,	,
kovovým	kovový	k2eAgInSc7d1	kovový
zinkem	zinek	k1gInSc7	zinek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Velká	velká	k1gFnSc1	velká
naleziště	naleziště	k1gNnSc2	naleziště
zinkových	zinkový	k2eAgFnPc2d1	zinková
rud	ruda	k1gFnPc2	ruda
<g/>
,	,	kIx,	,
zejména	zejména	k6eAd1	zejména
sfaleritu	sfalerit	k1gInSc2	sfalerit
a	a	k8xC	a
smithsonitu	smithsonit	k1gInSc2	smithsonit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
a	a	k8xC	a
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Malá	Malá	k1gFnSc1	Malá
množství	množství	k1gNnSc2	množství
zinku	zinek	k1gInSc2	zinek
bývají	bývat	k5eAaImIp3nP	bývat
také	také	k9	také
přimíšena	přimíšen	k2eAgFnSc1d1	přimíšena
v	v	k7c6	v
železných	železný	k2eAgFnPc6d1	železná
rudách	ruda	k1gFnPc6	ruda
a	a	k8xC	a
při	při	k7c6	při
zpracování	zpracování	k1gNnSc6	zpracování
rud	ruda	k1gFnPc2	ruda
železa	železo	k1gNnSc2	železo
ve	v	k7c6	v
vysoké	vysoký	k2eAgFnSc6d1	vysoká
peci	pec	k1gFnSc6	pec
se	se	k3xPyFc4	se
hromadí	hromadit	k5eAaImIp3nP	hromadit
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
zinkového	zinkový	k2eAgInSc2d1	zinkový
prachu	prach	k1gInSc2	prach
z	z	k7c2	z
kychtových	kychtový	k2eAgInPc2d1	kychtový
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
to	ten	k3xDgNnSc1	ten
být	být	k5eAaImF	být
30	[number]	k4	30
%	%	kIx~	%
i	i	k9	i
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
zinku	zinek	k1gInSc2	zinek
vychází	vycházet	k5eAaImIp3nS	vycházet
i	i	k9	i
z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Výroba	výroba	k1gFnSc1	výroba
==	==	k?	==
</s>
</p>
<p>
<s>
Zinek	zinek	k1gInSc1	zinek
se	se	k3xPyFc4	se
z	z	k7c2	z
90	[number]	k4	90
%	%	kIx~	%
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
sulfidických	sulfidický	k2eAgFnPc2d1	sulfidická
rud	ruda	k1gFnPc2	ruda
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
výroby	výroba	k1gFnSc2	výroba
začíná	začínat	k5eAaImIp3nS	začínat
koncentrací	koncentrace	k1gFnSc7	koncentrace
rudy	ruda	k1gFnSc2	ruda
sedimentačními	sedimentační	k2eAgFnPc7d1	sedimentační
nebo	nebo	k8xC	nebo
flotačními	flotační	k2eAgFnPc7d1	flotační
technikami	technika	k1gFnPc7	technika
a	a	k8xC	a
následným	následný	k2eAgNnSc7d1	následné
pražením	pražení	k1gNnSc7	pražení
rudy	ruda	k1gFnSc2	ruda
za	za	k7c2	za
přístupu	přístup	k1gInSc2	přístup
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
3	[number]	k4	3
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
O	o	k7c6	o
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
O	o	k7c6	o
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
<s>
O	o	k7c6	o
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
ZnS	ZnS	k1gMnSc1	ZnS
<g/>
+	+	kIx~	+
<g/>
3	[number]	k4	3
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
O_	O_	k1gMnSc1	O_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
to	ten	k3xDgNnSc1	ten
\	\	kIx~	\
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
ZnO	ZnO	k1gMnSc1	ZnO
<g/>
+	+	kIx~	+
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
SO_	SO_	k1gMnSc1	SO_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
Sulfid	sulfid	k1gInSc1	sulfid
zinečnatý	zinečnatý	k2eAgInSc1d1	zinečnatý
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
kyslíkem	kyslík	k1gInSc7	kyslík
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
oxidu	oxid	k1gInSc2	oxid
zinečnatého	zinečnatý	k2eAgInSc2d1	zinečnatý
a	a	k8xC	a
oxidu	oxid	k1gInSc2	oxid
siřičitéhoVznikající	siřičitéhoVznikající	k2eAgInSc1d1	siřičitéhoVznikající
oxid	oxid	k1gInSc1	oxid
siřičitý	siřičitý	k2eAgInSc1d1	siřičitý
se	se	k3xPyFc4	se
přitom	přitom	k6eAd1	přitom
obvykle	obvykle	k6eAd1	obvykle
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
a	a	k8xC	a
používá	používat	k5eAaImIp3nS	používat
následně	následně	k6eAd1	následně
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
kyseliny	kyselina	k1gFnSc2	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
zinečnatý	zinečnatý	k2eAgInSc1d1	zinečnatý
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
elektrolyticky	elektrolyticky	k6eAd1	elektrolyticky
nebo	nebo	k8xC	nebo
tavením	tavení	k1gNnSc7	tavení
s	s	k7c7	s
koksem	koks	k1gInSc7	koks
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
elektrolytickém	elektrolytický	k2eAgInSc6d1	elektrolytický
způsobu	způsob	k1gInSc6	způsob
se	se	k3xPyFc4	se
oxid	oxid	k1gInSc1	oxid
zinečnatý	zinečnatý	k2eAgInSc1d1	zinečnatý
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
sírové	sírový	k2eAgFnSc2d1	sírová
a	a	k8xC	a
z	z	k7c2	z
výluhu	výluh	k1gInSc2	výluh
se	se	k3xPyFc4	se
cementací	cementace	k1gFnSc7	cementace
zinkovým	zinkový	k2eAgInSc7d1	zinkový
prachem	prach	k1gInSc7	prach
získává	získávat	k5eAaImIp3nS	získávat
kadmium	kadmium	k1gNnSc1	kadmium
<g/>
.	.	kIx.	.
</s>
<s>
Roztok	roztok	k1gInSc1	roztok
síranu	síran	k1gInSc2	síran
zinečnatého	zinečnatý	k2eAgInSc2d1	zinečnatý
se	se	k3xPyFc4	se
elektrolyzuje	elektrolyzovat	k5eAaBmIp3nS	elektrolyzovat
a	a	k8xC	a
kov	kov	k1gInSc1	kov
s	s	k7c7	s
čistotou	čistota	k1gFnSc7	čistota
99,95	[number]	k4	99,95
%	%	kIx~	%
se	se	k3xPyFc4	se
vylučuje	vylučovat	k5eAaImIp3nS	vylučovat
na	na	k7c6	na
hliníkové	hliníkový	k2eAgFnSc6d1	hliníková
katodě	katoda	k1gFnSc6	katoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Redukce	redukce	k1gFnSc1	redukce
oxidu	oxid	k1gInSc2	oxid
zinečnatého	zinečnatý	k2eAgInSc2d1	zinečnatý
koksem	koks	k1gInSc7	koks
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
prováděla	provádět	k5eAaImAgFnS	provádět
v	v	k7c6	v
soustavě	soustava	k1gFnSc6	soustava
horizontálních	horizontální	k2eAgFnPc2d1	horizontální
retort	retorta	k1gFnPc2	retorta
s	s	k7c7	s
vnějším	vnější	k2eAgInSc7d1	vnější
ohřevem	ohřev	k1gInSc7	ohřev
a	a	k8xC	a
pracovaly	pracovat	k5eAaImAgInP	pracovat
vsádkovým	vsádkový	k2eAgInSc7d1	vsádkový
způsobem	způsob	k1gInSc7	způsob
<g/>
.	.	kIx.	.
</s>
<s>
Časem	časem	k6eAd1	časem
byly	být	k5eAaImAgFnP	být
nahrazeny	nahrazen	k2eAgInPc1d1	nahrazen
kontinuálně	kontinuálně	k6eAd1	kontinuálně
pracujícími	pracující	k2eAgFnPc7d1	pracující
vertikálními	vertikální	k2eAgFnPc7d1	vertikální
retortami	retorta	k1gFnPc7	retorta
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
někdy	někdy	k6eAd1	někdy
elektrické	elektrický	k2eAgNnSc4d1	elektrické
vyhřívání	vyhřívání	k1gNnSc4	vyhřívání
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
dvou	dva	k4xCgInPc2	dva
procesů	proces	k1gInPc2	proces
nemá	mít	k5eNaImIp3nS	mít
takovou	takový	k3xDgFnSc4	takový
termickou	termický	k2eAgFnSc4d1	termická
účinnost	účinnost	k1gFnSc4	účinnost
jako	jako	k8xC	jako
má	mít	k5eAaImIp3nS	mít
vysoká	vysoký	k2eAgFnSc1d1	vysoká
pec	pec	k1gFnSc1	pec
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgInPc4	který
spalování	spalování	k1gNnSc1	spalování
paliva	palivo	k1gNnSc2	palivo
pro	pro	k7c4	pro
zahřívání	zahřívání	k1gNnSc4	zahřívání
probíhá	probíhat	k5eAaImIp3nS	probíhat
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
prostoru	prostor	k1gInSc6	prostor
jako	jako	k9	jako
redukce	redukce	k1gFnSc1	redukce
oxidu	oxid	k1gInSc2	oxid
<g/>
.	.	kIx.	.
</s>
<s>
Problém	problém	k1gInSc1	problém
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
zinku	zinek	k1gInSc2	zinek
tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
redukce	redukce	k1gFnSc1	redukce
oxidu	oxid	k1gInSc2	oxid
zinečnatého	zinečnatý	k2eAgInSc2d1	zinečnatý
uhlíkem	uhlík	k1gInSc7	uhlík
neprobíhá	probíhat	k5eNaImIp3nS	probíhat
pod	pod	k7c7	pod
teplotou	teplota	k1gFnSc7	teplota
varu	var	k1gInSc2	var
zinku	zinek	k1gInSc2	zinek
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
při	při	k7c6	při
následném	následný	k2eAgInSc6d1	následný
ochlazení	ochlazení	k1gNnSc2	ochlazení
par	para	k1gFnPc2	para
zinku	zinek	k1gInSc2	zinek
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
reakci	reakce	k1gFnSc3	reakce
těchto	tento	k3xDgFnPc2	tento
par	para	k1gFnPc2	para
s	s	k7c7	s
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
oxidu	oxid	k1gInSc2	oxid
zinečnatého	zinečnatý	k2eAgInSc2d1	zinečnatý
a	a	k8xC	a
oxidu	oxid	k1gInSc2	oxid
uhelnatého	uhelnatý	k2eAgInSc2d1	uhelnatý
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
v	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
vyrobit	vyrobit	k5eAaPmF	vyrobit
takovou	takový	k3xDgFnSc4	takový
vysokou	vysoký	k2eAgFnSc4d1	vysoká
pec	pec	k1gFnSc4	pec
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
dokázala	dokázat	k5eAaPmAgFnS	dokázat
zvládnout	zvládnout	k5eAaPmF	zvládnout
problém	problém	k1gInSc4	problém
výroby	výroba	k1gFnSc2	výroba
<g/>
.	.	kIx.	.
</s>
<s>
Zinkové	zinkový	k2eAgInPc1d1	zinkový
páry	pár	k1gInPc1	pár
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
opouští	opouštět	k5eAaImIp3nP	opouštět
vrchol	vrchol	k1gInSc4	vrchol
pece	pec	k1gFnSc2	pec
jsou	být	k5eAaImIp3nP	být
tak	tak	k6eAd1	tak
rychle	rychle	k6eAd1	rychle
schlazovány	schlazovat	k5eAaImNgFnP	schlazovat
a	a	k8xC	a
rozpouštěny	rozpouštět	k5eAaImNgFnP	rozpouštět
zkrápěným	zkrápěný	k2eAgNnSc7d1	zkrápěné
olovem	olovo	k1gNnSc7	olovo
<g/>
,	,	kIx,	,
že	že	k8xS	že
zpětná	zpětný	k2eAgFnSc1d1	zpětná
oxidace	oxidace	k1gFnSc1	oxidace
na	na	k7c4	na
oxid	oxid	k1gInSc4	oxid
zinečnatý	zinečnatý	k2eAgInSc4d1	zinečnatý
je	být	k5eAaImIp3nS	být
minimální	minimální	k2eAgInSc4d1	minimální
<g/>
.	.	kIx.	.
</s>
<s>
Zinek	zinek	k1gInSc1	zinek
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
jako	jako	k9	jako
kapalina	kapalina	k1gFnSc1	kapalina
s	s	k7c7	s
99	[number]	k4	99
%	%	kIx~	%
čistotou	čistota	k1gFnSc7	čistota
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
je	být	k5eAaImIp3nS	být
rafinován	rafinovat	k5eAaImNgInS	rafinovat
vakuovou	vakuový	k2eAgFnSc7d1	vakuová
destilací	destilace	k1gFnSc7	destilace
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
získá	získat	k5eAaPmIp3nS	získat
99,99	[number]	k4	99,99
%	%	kIx~	%
čistý	čistý	k2eAgInSc1d1	čistý
zinek	zinek	k1gInSc1	zinek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
O	o	k7c6	o
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
</p>
<p>
<s>
↔	↔	k?	↔
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
<s>
Z	z	k7c2	z
</s>
</p>
<p>
<s>
n	n	k0	n
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
</p>
<p>
<s>
O	o	k7c6	o
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mathrm	mathrm	k1gInSc1	mathrm
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
ZnO	ZnO	k1gMnSc1	ZnO
<g/>
+	+	kIx~	+
<g/>
C	C	kA	C
<g/>
\	\	kIx~	\
\	\	kIx~	\
<g/>
leftrightarrow	leftrightarrow	k?	leftrightarrow
\	\	kIx~	\
2	[number]	k4	2
<g/>
\	\	kIx~	\
<g/>
,	,	kIx,	,
<g/>
Zn	zn	kA	zn
<g/>
+	+	kIx~	+
<g/>
CO_	CO_	k1gMnSc1	CO_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
<s>
Oxid	oxid	k1gInSc1	oxid
zinečnatý	zinečnatý	k2eAgInSc1d1	zinečnatý
reaguje	reagovat	k5eAaBmIp3nS	reagovat
s	s	k7c7	s
uhlíkem	uhlík	k1gInSc7	uhlík
za	za	k7c2	za
vzniku	vznik	k1gInSc2	vznik
zinku	zinek	k1gInSc2	zinek
a	a	k8xC	a
oxidu	oxid	k1gInSc2	oxid
uhličitéhoSvětová	uhličitéhoSvětová	k1gFnSc1	uhličitéhoSvětová
produkce	produkce	k1gFnSc2	produkce
zinku	zinek	k1gInSc2	zinek
je	být	k5eAaImIp3nS	být
stálá	stálý	k2eAgFnSc1d1	stálá
a	a	k8xC	a
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
6	[number]	k4	6
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
za	za	k7c4	za
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Zinek	zinek	k1gInSc1	zinek
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
železe	železo	k1gNnSc6	železo
<g/>
,	,	kIx,	,
mědi	měď	k1gFnSc6	měď
a	a	k8xC	a
hliníku	hliník	k1gInSc6	hliník
čtvrtým	čtvrtý	k4xOgNnSc7	čtvrtý
nejvíce	hodně	k6eAd3	hodně
průmyslově	průmyslově	k6eAd1	průmyslově
vyráběným	vyráběný	k2eAgInSc7d1	vyráběný
kovem	kov	k1gInSc7	kov
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	nejvíce	k6eAd1	nejvíce
rudy	ruda	k1gFnPc1	ruda
se	se	k3xPyFc4	se
vytěží	vytěžit	k5eAaPmIp3nP	vytěžit
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
většina	většina	k1gFnSc1	většina
se	se	k3xPyFc4	se
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
jinde	jinde	k6eAd1	jinde
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
==	==	k?	==
</s>
</p>
<p>
<s>
Elementární	elementární	k2eAgInSc1d1	elementární
zinek	zinek	k1gInSc1	zinek
nachází	nacházet	k5eAaImIp3nS	nacházet
významné	významný	k2eAgNnSc4d1	významné
uplatnění	uplatnění	k1gNnSc4	uplatnění
jako	jako	k8xC	jako
antikorozní	antikorozní	k2eAgInSc4d1	antikorozní
ochranný	ochranný	k2eAgInSc4d1	ochranný
materiál	materiál	k1gInSc4	materiál
především	především	k9	především
pro	pro	k7c4	pro
železo	železo	k1gNnSc4	železo
a	a	k8xC	a
jeho	jeho	k3xOp3gFnPc4	jeho
slitiny	slitina	k1gFnPc4	slitina
<g/>
.	.	kIx.	.
</s>
<s>
Pozinkovaný	pozinkovaný	k2eAgInSc1d1	pozinkovaný
železný	železný	k2eAgInSc1d1	železný
plech	plech	k1gInSc1	plech
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
řadou	řada	k1gFnSc7	řada
postupů	postup	k1gInPc2	postup
<g/>
,	,	kIx,	,
nejčastější	častý	k2eAgNnSc1d3	nejčastější
je	být	k5eAaImIp3nS	být
galvanické	galvanický	k2eAgNnSc1d1	galvanické
pokovování	pokovování	k1gNnSc1	pokovování
<g/>
,	,	kIx,	,
postřikování	postřikování	k1gNnSc1	postřikování
<g/>
,	,	kIx,	,
napařování	napařování	k1gNnSc1	napařování
nebo	nebo	k8xC	nebo
žárové	žárový	k2eAgNnSc1d1	žárové
nanášení	nanášení	k1gNnSc1	nanášení
tenkého	tenký	k2eAgInSc2d1	tenký
povlaku	povlak	k1gInSc2	povlak
zinku	zinek	k1gInSc2	zinek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zinek	zinek	k1gInSc1	zinek
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
dobré	dobrý	k2eAgFnPc4d1	dobrá
vlastnosti	vlastnost	k1gFnPc4	vlastnost
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
odlitků	odlitek	k1gInPc2	odlitek
–	–	k?	–
díky	díky	k7c3	díky
výborné	výborná	k1gFnSc3	výborná
zatékavosti	zatékavost	k1gFnSc2	zatékavost
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
roztavený	roztavený	k2eAgInSc1d1	roztavený
zinek	zinek	k1gInSc1	zinek
dokonale	dokonale	k6eAd1	dokonale
odlévací	odlévací	k2eAgFnSc4d1	odlévací
formu	forma	k1gFnSc4	forma
<g/>
.	.	kIx.	.
</s>
<s>
Vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
kovové	kovový	k2eAgFnPc1d1	kovová
součástky	součástka	k1gFnPc1	součástka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
dobře	dobře	k6eAd1	dobře
odolné	odolný	k2eAgFnSc2d1	odolná
vůči	vůči	k7c3	vůči
atmosférickým	atmosférický	k2eAgInPc3d1	atmosférický
vlivům	vliv	k1gInPc3	vliv
(	(	kIx(	(
<g/>
v	v	k7c6	v
suchu	sucho	k1gNnSc6	sucho
nekorodují	korodovat	k5eNaImIp3nP	korodovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
vlhku	vlhko	k1gNnSc3	vlhko
výrazně	výrazně	k6eAd1	výrazně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nemusejí	muset	k5eNaImIp3nP	muset
snášet	snášet	k5eAaImF	snášet
výrazné	výrazný	k2eAgNnSc4d1	výrazné
mechanické	mechanický	k2eAgNnSc4d1	mechanické
namáhání	namáhání	k1gNnSc4	namáhání
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zinek	zinek	k1gInSc1	zinek
je	být	k5eAaImIp3nS	být
mechanicky	mechanicky	k6eAd1	mechanicky
velmi	velmi	k6eAd1	velmi
málo	málo	k6eAd1	málo
odolný	odolný	k2eAgInSc1d1	odolný
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
některé	některý	k3yIgFnPc4	některý
části	část	k1gFnPc4	část
motorových	motorový	k2eAgInPc2d1	motorový
karburátorů	karburátor	k1gInPc2	karburátor
<g/>
,	,	kIx,	,
kovové	kovový	k2eAgFnPc1d1	kovová
ozdoby	ozdoba	k1gFnPc1	ozdoba
<g/>
,	,	kIx,	,
okenní	okenní	k2eAgFnPc1d1	okenní
kliky	klika	k1gFnPc1	klika
<g/>
,	,	kIx,	,
konve	konev	k1gFnPc1	konev
<g/>
,	,	kIx,	,
vědra	vědro	k1gNnPc1	vědro
<g/>
,	,	kIx,	,
vany	van	k1gInPc1	van
<g/>
,	,	kIx,	,
střešní	střešní	k2eAgInPc1d1	střešní
okapy	okap	k1gInPc1	okap
<g/>
,	,	kIx,	,
střechy	střecha	k1gFnPc1	střecha
<g/>
,	,	kIx,	,
obkládání	obkládání	k1gNnSc1	obkládání
nádrží	nádrž	k1gFnPc2	nádrž
<g/>
,	,	kIx,	,
skříní	skříň	k1gFnPc2	skříň
<g/>
,	,	kIx,	,
ledniček	lednička	k1gFnPc2	lednička
apod.	apod.	kA	apod.
</s>
</p>
<p>
<s>
Titanzinkový	Titanzinkový	k2eAgInSc1d1	Titanzinkový
plech	plech	k1gInSc1	plech
na	na	k7c4	na
střechy	střecha	k1gFnPc4	střecha
je	být	k5eAaImIp3nS	být
slitina	slitina	k1gFnSc1	slitina
zinku	zinek	k1gInSc2	zinek
s	s	k7c7	s
cca	cca	kA	cca
0,3	[number]	k4	0,3
%	%	kIx~	%
titanu	titan	k1gInSc2	titan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poměrně	poměrně	k6eAd1	poměrně
významné	významný	k2eAgNnSc1d1	významné
místo	místo	k1gNnSc1	místo
patřilo	patřit	k5eAaImAgNnS	patřit
zinku	zinek	k1gInSc3	zinek
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
galvanických	galvanický	k2eAgInPc2d1	galvanický
článků	článek	k1gInPc2	článek
(	(	kIx(	(
<g/>
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
baterií	baterie	k1gFnPc2	baterie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dodnes	dodnes	k6eAd1	dodnes
je	být	k5eAaImIp3nS	být
běžně	běžně	k6eAd1	běžně
užíván	užíván	k2eAgInSc1d1	užíván
zinko-uhlíkový	zinkohlíkový	k2eAgInSc1d1	zinko-uhlíkový
článek	článek	k1gInSc1	článek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
oblasti	oblast	k1gFnSc6	oblast
se	se	k3xPyFc4	se
ale	ale	k9	ale
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
využívají	využívat	k5eAaPmIp3nP	využívat
jiné	jiný	k2eAgInPc4d1	jiný
principy	princip	k1gInPc4	princip
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
pracují	pracovat	k5eAaImIp3nP	pracovat
s	s	k7c7	s
jinými	jiný	k2eAgInPc7d1	jiný
prvky	prvek	k1gInPc7	prvek
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
niklem	nikl	k1gInSc7	nikl
a	a	k8xC	a
lithiem	lithium	k1gNnSc7	lithium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
slitin	slitina	k1gFnPc2	slitina
zinku	zinek	k1gInSc2	zinek
je	být	k5eAaImIp3nS	být
nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
slitina	slitina	k1gFnSc1	slitina
s	s	k7c7	s
mědí	měď	k1gFnSc7	měď
–	–	k?	–
bílá	bílý	k2eAgFnSc1d1	bílá
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
mosaz	mosaz	k1gFnSc1	mosaz
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
řady	řada	k1gFnSc2	řada
různých	různý	k2eAgFnPc2d1	různá
mosazí	mosaz	k1gFnPc2	mosaz
s	s	k7c7	s
odlišným	odlišný	k2eAgInSc7d1	odlišný
poměrem	poměr	k1gInSc7	poměr
obou	dva	k4xCgInPc2	dva
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
jak	jak	k6eAd1	jak
barvou	barva	k1gFnSc7	barva
tak	tak	k8xS	tak
mechanickými	mechanický	k2eAgFnPc7d1	mechanická
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
–	–	k?	–
tvrdostí	tvrdost	k1gFnSc7	tvrdost
<g/>
,	,	kIx,	,
kujností	kujnost	k1gFnSc7	kujnost
<g/>
,	,	kIx,	,
tažností	tažnost	k1gFnSc7	tažnost
i	i	k8xC	i
odolností	odolnost	k1gFnSc7	odolnost
proti	proti	k7c3	proti
vlivům	vliv	k1gInPc3	vliv
okolního	okolní	k2eAgNnSc2d1	okolní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
mosaz	mosaz	k1gFnSc1	mosaz
oproti	oproti	k7c3	oproti
čistému	čistý	k2eAgInSc3d1	čistý
zinku	zinek	k1gInSc3	zinek
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
výrazně	výrazně	k6eAd1	výrazně
lepší	dobrý	k2eAgFnSc7d2	lepší
mechanickou	mechanický	k2eAgFnSc7d1	mechanická
odolností	odolnost	k1gFnSc7	odolnost
i	i	k8xC	i
vzhledem	vzhled	k1gInSc7	vzhled
<g/>
.	.	kIx.	.
</s>
<s>
Bílá	bílý	k2eAgFnSc1d1	bílá
mosaz	mosaz	k1gFnSc1	mosaz
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
85	[number]	k4	85
%	%	kIx~	%
zinku	zinek	k1gInSc2	zinek
<g/>
,	,	kIx,	,
5	[number]	k4	5
%	%	kIx~	%
hliníku	hliník	k1gInSc2	hliník
a	a	k8xC	a
10	[number]	k4	10
%	%	kIx~	%
mědi	měď	k1gFnSc2	měď
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
významnými	významný	k2eAgFnPc7d1	významná
slitinami	slitina	k1gFnPc7	slitina
jsou	být	k5eAaImIp3nP	být
různé	různý	k2eAgInPc4d1	různý
druhy	druh	k1gInPc4	druh
bronzu	bronz	k1gInSc2	bronz
–	–	k?	–
například	například	k6eAd1	například
se	s	k7c7	s
složením	složení	k1gNnSc7	složení
88	[number]	k4	88
%	%	kIx~	%
cínu	cín	k1gInSc2	cín
<g/>
,	,	kIx,	,
6	[number]	k4	6
%	%	kIx~	%
hliníku	hliník	k1gInSc2	hliník
a	a	k8xC	a
6	[number]	k4	6
%	%	kIx~	%
mědi	měď	k1gFnSc2	měď
a	a	k8xC	a
slitina	slitina	k1gFnSc1	slitina
zelco	zelco	k6eAd1	zelco
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
má	mít	k5eAaImIp3nS	mít
složení	složení	k1gNnSc4	složení
83	[number]	k4	83
%	%	kIx~	%
zinku	zinek	k1gInSc2	zinek
<g/>
,	,	kIx,	,
15	[number]	k4	15
%	%	kIx~	%
hliníku	hliník	k1gInSc2	hliník
a	a	k8xC	a
2	[number]	k4	2
%	%	kIx~	%
mědi	měď	k1gFnSc2	měď
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zinek	zinek	k1gInSc1	zinek
se	se	k3xPyFc4	se
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
klenotnických	klenotnický	k2eAgFnPc2d1	klenotnická
slitin	slitina	k1gFnPc2	slitina
se	s	k7c7	s
zlatem	zlato	k1gNnSc7	zlato
<g/>
,	,	kIx,	,
stříbrem	stříbro	k1gNnSc7	stříbro
<g/>
,	,	kIx,	,
mědí	měď	k1gFnSc7	měď
a	a	k8xC	a
niklem	nikl	k1gInSc7	nikl
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaImIp3nS	využívat
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
také	také	k9	také
k	k	k7c3	k
srážení	srážení	k1gNnSc3	srážení
zlata	zlato	k1gNnSc2	zlato
vyluhovaného	vyluhovaný	k2eAgInSc2d1	vyluhovaný
kyanidem	kyanid	k1gInSc7	kyanid
a	a	k8xC	a
v	v	k7c6	v
hutnictví	hutnictví	k1gNnSc6	hutnictví
k	k	k7c3	k
odstříbřování	odstříbřování	k1gNnSc3	odstříbřování
olova	olovo	k1gNnSc2	olovo
–	–	k?	–
tzv.	tzv.	kA	tzv.
parkesování	parkesování	k1gNnSc2	parkesování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgNnSc1d1	další
využití	využití	k1gNnSc1	využití
zinku	zinek	k1gInSc2	zinek
je	být	k5eAaImIp3nS	být
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
závaží	závaží	k1gNnSc2	závaží
pro	pro	k7c4	pro
vyvažování	vyvažování	k1gNnPc4	vyvažování
automobilových	automobilový	k2eAgNnPc2d1	automobilové
kol	kolo	k1gNnPc2	kolo
jako	jako	k8xS	jako
náhrada	náhrada	k1gFnSc1	náhrada
za	za	k7c4	za
toxické	toxický	k2eAgNnSc4d1	toxické
olovo	olovo	k1gNnSc4	olovo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mnoho	mnoho	k6eAd1	mnoho
ze	z	k7c2	z
sloučenin	sloučenina	k1gFnPc2	sloučenina
zinku	zinek	k1gInSc2	zinek
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
jako	jako	k9	jako
nátěrové	nátěrový	k2eAgFnPc4d1	nátěrová
barvy	barva	k1gFnPc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nejznámějším	známý	k2eAgMnPc3d3	nejznámější
patří	patřit	k5eAaImIp3nS	patřit
lithopon	lithopon	k1gNnSc1	lithopon
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
směs	směs	k1gFnSc1	směs
sulfidu	sulfid	k1gInSc2	sulfid
zinečnatého	zinečnatý	k2eAgInSc2d1	zinečnatý
a	a	k8xC	a
síranu	síran	k1gInSc2	síran
barnatého	barnatý	k2eAgInSc2d1	barnatý
a	a	k8xC	a
zinková	zinkový	k2eAgFnSc1d1	zinková
běloba	běloba	k1gFnSc1	běloba
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
jemně	jemně	k6eAd1	jemně
práškovaný	práškovaný	k2eAgInSc1d1	práškovaný
oxid	oxid	k1gInSc1	oxid
zinečnatý	zinečnatý	k2eAgInSc1d1	zinečnatý
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
známá	známý	k2eAgFnSc1d1	známá
barva	barva	k1gFnSc1	barva
je	být	k5eAaImIp3nS	být
jemně	jemně	k6eAd1	jemně
práškované	práškovaný	k2eAgNnSc1d1	práškovaný
zinkové	zinkový	k2eAgNnSc1d1	zinkové
blejno	blejno	k1gNnSc1	blejno
<g/>
,	,	kIx,	,
chemicky	chemicky	k6eAd1	chemicky
sulfid	sulfid	k1gInSc1	sulfid
zinečnatý	zinečnatý	k2eAgMnSc1d1	zinečnatý
ZnS	ZnS	k1gMnSc1	ZnS
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
antikorózní	antikorózní	k2eAgInSc1d1	antikorózní
nátěr	nátěr	k1gInSc1	nátěr
na	na	k7c4	na
železo	železo	k1gNnSc4	železo
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
natírají	natírat	k5eAaImIp3nP	natírat
mosty	most	k1gInPc1	most
a	a	k8xC	a
části	část	k1gFnPc1	část
strojů	stroj	k1gInPc2	stroj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ze	z	k7c2	z
zinku	zinek	k1gInSc2	zinek
se	se	k3xPyFc4	se
také	také	k9	také
razily	razit	k5eAaImAgInP	razit
mince	mince	k1gFnSc2	mince
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
za	za	k7c2	za
válečných	válečný	k2eAgNnPc2d1	válečné
období	období	k1gNnPc2	období
<g/>
)	)	kIx)	)
–	–	k?	–
Protektorátní	protektorátní	k2eAgInSc1d1	protektorátní
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
,	,	kIx,	,
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
,	,	kIx,	,
50	[number]	k4	50
<g/>
haléře	haléř	k1gInSc2	haléř
a	a	k8xC	a
1	[number]	k4	1
<g/>
koruny	koruna	k1gFnPc1	koruna
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
říšské	říšský	k2eAgFnPc1d1	říšská
pfennigy	pfenniga	k1gFnPc1	pfenniga
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sloučeniny	sloučenina	k1gFnPc1	sloučenina
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
sloučeninách	sloučenina	k1gFnPc6	sloučenina
se	se	k3xPyFc4	se
zinek	zinek	k1gInSc1	zinek
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
vždy	vždy	k6eAd1	vždy
jako	jako	k8xS	jako
kladně	kladně	k6eAd1	kladně
dvojmocný	dvojmocný	k2eAgInSc4d1	dvojmocný
zinečnatý	zinečnatý	k2eAgInSc4d1	zinečnatý
kation	kation	k1gInSc4	kation
<g/>
.	.	kIx.	.
</s>
<s>
Sloučeniny	sloučenina	k1gFnPc1	sloučenina
zinku	zinek	k1gInSc2	zinek
jsou	být	k5eAaImIp3nP	být
bezbarvé	bezbarvý	k2eAgFnPc1d1	bezbarvá
(	(	kIx(	(
<g/>
bílé	bílý	k2eAgFnPc1d1	bílá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
anion	anion	k1gInSc1	anion
vázaný	vázaný	k2eAgInSc1d1	vázaný
k	k	k7c3	k
zinečnatému	zinečnatý	k2eAgInSc3d1	zinečnatý
kationu	kation	k1gInSc3	kation
barevný	barevný	k2eAgMnSc1d1	barevný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Anorganické	anorganický	k2eAgFnPc1d1	anorganická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
===	===	k?	===
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
zinečnatých	zinečnatý	k2eAgFnPc2d1	zinečnatá
sloučenin	sloučenina	k1gFnPc2	sloučenina
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
rozpustných	rozpustný	k2eAgFnPc2d1	rozpustná
<g/>
.	.	kIx.	.
</s>
<s>
Nerozpustné	rozpustný	k2eNgInPc1d1	nerozpustný
jsou	být	k5eAaImIp3nP	být
především	především	k9	především
zásadité	zásaditý	k2eAgFnPc1d1	zásaditá
soli	sůl	k1gFnPc1	sůl
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
ve	v	k7c6	v
vodném	vodný	k2eAgInSc6d1	vodný
roztoku	roztok	k1gInSc6	roztok
amoniaku	amoniak	k1gInSc2	amoniak
na	na	k7c4	na
komplexní	komplexní	k2eAgFnPc4d1	komplexní
aminosloučeniny	aminosloučenina	k1gFnPc4	aminosloučenina
nebo	nebo	k8xC	nebo
v	v	k7c6	v
nadbytečném	nadbytečný	k2eAgNnSc6d1	nadbytečné
množství	množství	k1gNnSc6	množství
hydroxidu	hydroxid	k1gInSc2	hydroxid
na	na	k7c4	na
hydroxozinečnatany	hydroxozinečnatan	k1gInPc4	hydroxozinečnatan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oxid	oxid	k1gInSc1	oxid
zinečnatý	zinečnatý	k2eAgInSc1d1	zinečnatý
ZnO	ZnO	k1gFnSc4	ZnO
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
práškovitá	práškovitý	k2eAgFnSc1d1	práškovitá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
se	se	k3xPyFc4	se
v	v	k7c6	v
zředěných	zředěný	k2eAgFnPc6d1	zředěná
kyselinách	kyselina	k1gFnPc6	kyselina
a	a	k8xC	a
roztocích	roztok	k1gInPc6	roztok
hydroxidů	hydroxid	k1gInPc2	hydroxid
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jako	jako	k9	jako
nerost	nerost	k1gInSc1	nerost
zinkit	zinkit	k1gInSc1	zinkit
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
jako	jako	k9	jako
netoxický	toxický	k2eNgInSc1d1	netoxický
bílý	bílý	k2eAgInSc1d1	bílý
pigment	pigment	k1gInSc1	pigment
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
barviv	barvivo	k1gNnPc2	barvivo
známých	známý	k1gMnPc2	známý
jako	jako	k8xS	jako
zinková	zinkový	k2eAgFnSc1d1	zinková
běloba	běloba	k1gFnSc1	běloba
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
také	také	k9	také
jako	jako	k9	jako
plnicí	plnicí	k2eAgInSc4d1	plnicí
prostředek	prostředek	k1gInSc4	prostředek
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
vulkanizovaného	vulkanizovaný	k2eAgInSc2d1	vulkanizovaný
kaučuku	kaučuk	k1gInSc2	kaučuk
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
uplatnění	uplatnění	k1gNnSc4	uplatnění
i	i	k9	i
v	v	k7c6	v
keramickém	keramický	k2eAgInSc6d1	keramický
a	a	k8xC	a
sklářském	sklářský	k2eAgInSc6d1	sklářský
průmyslu	průmysl	k1gInSc6	průmysl
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
speciálních	speciální	k2eAgNnPc2d1	speciální
chemicky	chemicky	k6eAd1	chemicky
odolných	odolný	k2eAgNnPc2d1	odolné
skel	sklo	k1gNnPc2	sklo
a	a	k8xC	a
glazur	glazura	k1gFnPc2	glazura
nebo	nebo	k8xC	nebo
emailů	email	k1gInPc2	email
<g/>
.	.	kIx.	.
</s>
<s>
Oxid	oxid	k1gInSc1	oxid
zinečnatý	zinečnatý	k2eAgMnSc1d1	zinečnatý
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
připravit	připravit	k5eAaPmF	připravit
termickým	termický	k2eAgInSc7d1	termický
rozkladem	rozklad	k1gInSc7	rozklad
hydroxidu	hydroxid	k1gInSc2	hydroxid
zinečnatého	zinečnatý	k2eAgInSc2d1	zinečnatý
<g/>
,	,	kIx,	,
uhličitanu	uhličitan	k1gInSc2	uhličitan
zinečnatého	zinečnatý	k2eAgInSc2d1	zinečnatý
nebo	nebo	k8xC	nebo
dusičnanu	dusičnan	k1gInSc2	dusičnan
zinečnatého	zinečnatý	k2eAgInSc2d1	zinečnatý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
průmyslově	průmyslově	k6eAd1	průmyslově
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
spalováním	spalování	k1gNnSc7	spalování
zinku	zinek	k1gInSc2	zinek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hydroxid	hydroxid	k1gInSc4	hydroxid
zinečnatý	zinečnatý	k2eAgInSc4d1	zinečnatý
Zn	zn	kA	zn
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
práškovitá	práškovitý	k2eAgFnSc1d1	práškovitá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
v	v	k7c6	v
roztocích	roztok	k1gInPc6	roztok
zředěných	zředěný	k2eAgFnPc2d1	zředěná
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
,	,	kIx,	,
koncentrovanějších	koncentrovaný	k2eAgInPc6d2	koncentrovanější
roztocích	roztok	k1gInPc6	roztok
alkalických	alkalický	k2eAgInPc2d1	alkalický
hydroxidů	hydroxid	k1gInPc2	hydroxid
<g/>
,	,	kIx,	,
vodném	vodné	k1gNnSc6	vodné
roztoku	roztok	k1gInSc2	roztok
amoniaku	amoniak	k1gInSc2	amoniak
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
se	se	k3xPyFc4	se
rozpouští	rozpouštět	k5eAaImIp3nS	rozpouštět
také	také	k9	také
ve	v	k7c6	v
vodných	vodný	k2eAgInPc6d1	vodný
roztocích	roztok	k1gInPc6	roztok
amonných	amonný	k2eAgFnPc2d1	amonná
solí	sůl	k1gFnPc2	sůl
<g/>
.	.	kIx.	.
</s>
<s>
Hydroxid	hydroxid	k1gInSc1	hydroxid
zinečnatý	zinečnatý	k2eAgMnSc1d1	zinečnatý
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
5	[number]	k4	5
krystalických	krystalický	k2eAgFnPc6d1	krystalická
modifikacích	modifikace	k1gFnPc6	modifikace
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
za	za	k7c2	za
normální	normální	k2eAgFnSc2d1	normální
teploty	teplota	k1gFnSc2	teplota
je	být	k5eAaImIp3nS	být
stabilní	stabilní	k2eAgFnSc1d1	stabilní
pouze	pouze	k6eAd1	pouze
jedna	jeden	k4xCgFnSc1	jeden
<g/>
.	.	kIx.	.
</s>
<s>
Hydroxid	hydroxid	k1gInSc1	hydroxid
zinečnatý	zinečnatý	k2eAgMnSc1d1	zinečnatý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
srážením	srážení	k1gNnSc7	srážení
rozpustných	rozpustný	k2eAgFnPc2d1	rozpustná
zinečnatých	zinečnatý	k2eAgFnPc2d1	zinečnatá
solí	sůl	k1gFnPc2	sůl
rozpustným	rozpustný	k2eAgInSc7d1	rozpustný
alkalickým	alkalický	k2eAgInSc7d1	alkalický
hydroxidem	hydroxid	k1gInSc7	hydroxid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Sulfid	sulfid	k1gInSc1	sulfid
zinečnatý	zinečnatý	k2eAgMnSc1d1	zinečnatý
ZnS	ZnS	k1gMnSc1	ZnS
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
čistém	čistý	k2eAgInSc6d1	čistý
stavu	stav	k1gInSc6	stav
bílá	bílý	k2eAgFnSc1d1	bílá
práškovitá	práškovitý	k2eAgFnSc1d1	práškovitá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
v	v	k7c6	v
čerstvém	čerstvý	k2eAgInSc6d1	čerstvý
stavu	stav	k1gInSc6	stav
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
zředěných	zředěný	k2eAgFnPc6d1	zředěná
kyselinách	kyselina	k1gFnPc6	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
nerost	nerost	k1gInSc4	nerost
sfalerit	sfalerit	k1gInSc1	sfalerit
neboli	neboli	k8xC	neboli
blejno	blejno	k1gNnSc1	blejno
zinkové	zinkový	k2eAgFnSc2d1	zinková
a	a	k8xC	a
wurtzit	wurtzit	k1gInSc1	wurtzit
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaImIp3nS	využívat
se	se	k3xPyFc4	se
jako	jako	k8xS	jako
nátěrová	nátěrový	k2eAgFnSc1d1	nátěrová
barva	barva	k1gFnSc1	barva
známá	známý	k2eAgFnSc1d1	známá
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
zinkové	zinkový	k2eAgNnSc4d1	zinkové
blejno	blejno	k1gNnSc4	blejno
a	a	k8xC	a
ve	v	k7c6	v
směsi	směs	k1gFnSc6	směs
se	s	k7c7	s
síranem	síran	k1gInSc7	síran
barnatým	barnatý	k2eAgInSc7d1	barnatý
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
lithopon	lithopona	k1gFnPc2	lithopona
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc7d1	zvláštní
modifikací	modifikace	k1gFnSc7	modifikace
sulfidu	sulfid	k1gInSc2	sulfid
zinečnatého	zinečnatý	k2eAgInSc2d1	zinečnatý
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
Sidotovo	Sidotův	k2eAgNnSc1d1	Sidotův
blejno	blejno	k1gNnSc1	blejno
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
luminiscenční	luminiscenční	k2eAgFnSc1d1	luminiscenční
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
základní	základní	k2eAgFnSc1d1	základní
látka	látka	k1gFnSc1	látka
pro	pro	k7c4	pro
světélkující	světélkující	k2eAgInPc4d1	světélkující
nátěry	nátěr	k1gInPc4	nátěr
hodinových	hodinový	k2eAgFnPc2d1	hodinová
ručiček	ručička	k1gFnPc2	ručička
a	a	k8xC	a
v	v	k7c6	v
podobných	podobný	k2eAgFnPc6d1	podobná
aplikacích	aplikace	k1gFnPc6	aplikace
<g/>
.	.	kIx.	.
</s>
<s>
Sulfid	sulfid	k1gInSc1	sulfid
zinečnatý	zinečnatý	k2eAgMnSc1d1	zinečnatý
se	se	k3xPyFc4	se
připraví	připravit	k5eAaPmIp3nS	připravit
srážením	srážení	k1gNnSc7	srážení
vodného	vodný	k2eAgInSc2d1	vodný
roztoku	roztok	k1gInSc2	roztok
zinečnaté	zinečnatý	k2eAgFnSc2d1	zinečnatá
soli	sůl	k1gFnSc2	sůl
sulfanem	sulfan	k1gInSc7	sulfan
nebo	nebo	k8xC	nebo
alkalickým	alkalický	k2eAgInSc7d1	alkalický
sulfidem	sulfid	k1gInSc7	sulfid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
zinečnatý	zinečnatý	k2eAgInSc4d1	zinečnatý
ZnCl	ZnCl	k1gInSc4	ZnCl
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
methanolu	methanol	k1gInSc6	methanol
<g/>
,	,	kIx,	,
ethanolu	ethanol	k1gInSc6	ethanol
<g/>
,	,	kIx,	,
etheru	ether	k1gInSc6	ether
<g/>
,	,	kIx,	,
acetonu	aceton	k1gInSc6	aceton
<g/>
,	,	kIx,	,
glycerinu	glycerin	k1gInSc6	glycerin
<g/>
,	,	kIx,	,
pyridinu	pyridin	k1gInSc6	pyridin
<g/>
,	,	kIx,	,
anilinu	anilin	k1gInSc6	anilin
a	a	k8xC	a
dalších	další	k2eAgNnPc6d1	další
organických	organický	k2eAgNnPc6d1	organické
rozpouštědlech	rozpouštědlo	k1gNnPc6	rozpouštědlo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
značně	značně	k6eAd1	značně
hygroskopický	hygroskopický	k2eAgInSc1d1	hygroskopický
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
síran	síran	k1gInSc1	síran
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
impregnační	impregnační	k2eAgInSc1d1	impregnační
prostředek	prostředek	k1gInSc1	prostředek
pro	pro	k7c4	pro
ochranu	ochrana	k1gFnSc4	ochrana
dřeva	dřevo	k1gNnSc2	dřevo
před	před	k7c7	před
plísněmi	plíseň	k1gFnPc7	plíseň
a	a	k8xC	a
hnilobou	hniloba	k1gFnSc7	hniloba
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
také	také	k9	také
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
deodorantů	deodorant	k1gInPc2	deodorant
<g/>
,	,	kIx,	,
v	v	k7c6	v
lékařství	lékařství	k1gNnSc6	lékařství
<g/>
,	,	kIx,	,
v	v	k7c6	v
tisku	tisk	k1gInSc6	tisk
tkanin	tkanina	k1gFnPc2	tkanina
<g/>
,	,	kIx,	,
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
organických	organický	k2eAgNnPc2d1	organické
barviv	barvivo	k1gNnPc2	barvivo
a	a	k8xC	a
například	například	k6eAd1	například
při	při	k7c6	při
naleptávání	naleptávání	k1gNnSc6	naleptávání
kovů	kov	k1gInPc2	kov
při	při	k7c6	při
pájení	pájení	k1gNnSc6	pájení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
podvojné	podvojný	k2eAgFnPc4d1	podvojná
adiční	adiční	k2eAgFnPc4d1	adiční
i	i	k8xC	i
komplexní	komplexní	k2eAgFnPc4d1	komplexní
sloučeniny	sloučenina	k1gFnPc4	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Chlorid	chlorid	k1gInSc1	chlorid
zinečnatý	zinečnatý	k2eAgMnSc1d1	zinečnatý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
sulfidu	sulfid	k1gInSc2	sulfid
zinečnatého	zinečnatý	k2eAgInSc2d1	zinečnatý
<g/>
,	,	kIx,	,
hydroxidu	hydroxid	k1gInSc2	hydroxid
zinečnatého	zinečnatý	k2eAgInSc2d1	zinečnatý
<g/>
,	,	kIx,	,
uhličitanu	uhličitan	k1gInSc2	uhličitan
zinečnatého	zinečnatý	k2eAgInSc2d1	zinečnatý
<g/>
,	,	kIx,	,
oxidu	oxid	k1gInSc2	oxid
zinečnatého	zinečnatý	k2eAgInSc2d1	zinečnatý
nebo	nebo	k8xC	nebo
zinkových	zinkový	k2eAgInPc2d1	zinkový
odpadů	odpad	k1gInPc2	odpad
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
chlorovodíkové	chlorovodíkový	k2eAgFnSc6d1	chlorovodíková
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bromid	bromid	k1gInSc1	bromid
zinečnatý	zinečnatý	k2eAgInSc4d1	zinečnatý
ZnBr	ZnBr	k1gInSc4	ZnBr
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
tvoří	tvořit	k5eAaImIp3nP	tvořit
adiční	adiční	k2eAgFnPc1d1	adiční
<g/>
,	,	kIx,	,
podvojné	podvojný	k2eAgFnPc1d1	podvojná
i	i	k8xC	i
komplexní	komplexní	k2eAgFnPc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
hydroxidu	hydroxid	k1gInSc2	hydroxid
zinečnatého	zinečnatý	k2eAgInSc2d1	zinečnatý
<g/>
,	,	kIx,	,
uhličitanu	uhličitan	k1gInSc2	uhličitan
zinečnatého	zinečnatý	k2eAgInSc2d1	zinečnatý
<g/>
,	,	kIx,	,
oxidu	oxid	k1gInSc2	oxid
zinečnatého	zinečnatý	k2eAgInSc2d1	zinečnatý
nebo	nebo	k8xC	nebo
zinkových	zinkový	k2eAgInPc2d1	zinkový
odpadů	odpad	k1gInPc2	odpad
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
bromovodíkové	bromovodíkový	k2eAgFnSc6d1	bromovodíkový
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jodid	jodid	k1gInSc1	jodid
zinečnatý	zinečnatý	k2eAgInSc1d1	zinečnatý
ZnI	znít	k5eAaImRp2nS	znít
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
nažloutlá	nažloutlý	k2eAgFnSc1d1	nažloutlá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
v	v	k7c6	v
lékařství	lékařství	k1gNnSc6	lékařství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
tvoří	tvořit	k5eAaImIp3nP	tvořit
podvojné	podvojný	k2eAgFnPc1d1	podvojná
<g/>
,	,	kIx,	,
adiční	adiční	k2eAgFnPc1d1	adiční
i	i	k8xC	i
komplexní	komplexní	k2eAgFnPc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
hydroxidu	hydroxid	k1gInSc2	hydroxid
zinečnatého	zinečnatý	k2eAgInSc2d1	zinečnatý
<g/>
,	,	kIx,	,
uhličitanu	uhličitan	k1gInSc2	uhličitan
zinečnatého	zinečnatý	k2eAgInSc2d1	zinečnatý
<g/>
,	,	kIx,	,
oxidu	oxid	k1gInSc2	oxid
zinečnatého	zinečnatý	k2eAgInSc2d1	zinečnatý
nebo	nebo	k8xC	nebo
zinkových	zinkový	k2eAgInPc2d1	zinkový
odpadů	odpad	k1gInPc2	odpad
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
jodovodíkové	jodovodíkový	k2eAgFnSc6d1	jodovodíková
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Fluorid	fluorid	k1gInSc4	fluorid
zinečnatý	zinečnatý	k2eAgInSc4d1	zinečnatý
ZnF	ZnF	k1gFnSc7	ZnF
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
špatně	špatně	k6eAd1	špatně
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
ke	k	k7c3	k
konzervování	konzervování	k1gNnSc3	konzervování
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
tvoří	tvořit	k5eAaImIp3nP	tvořit
podvojné	podvojný	k2eAgFnPc1d1	podvojná
<g/>
,	,	kIx,	,
adiční	adiční	k2eAgFnPc1d1	adiční
i	i	k8xC	i
komplexní	komplexní	k2eAgFnPc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
hydroxidu	hydroxid	k1gInSc2	hydroxid
zinečnatého	zinečnatý	k2eAgInSc2d1	zinečnatý
<g/>
,	,	kIx,	,
uhličitanu	uhličitan	k1gInSc2	uhličitan
zinečnatého	zinečnatý	k2eAgInSc2d1	zinečnatý
<g/>
,	,	kIx,	,
oxidu	oxid	k1gInSc2	oxid
zinečnatého	zinečnatý	k2eAgInSc2d1	zinečnatý
nebo	nebo	k8xC	nebo
zinkových	zinkový	k2eAgInPc2d1	zinkový
odpadů	odpad	k1gInPc2	odpad
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
fluorovodíkové	fluorovodíkový	k2eAgFnSc6d1	fluorovodíková
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dusičnan	dusičnan	k1gInSc4	dusičnan
zinečnatý	zinečnatý	k2eAgInSc4d1	zinečnatý
Zn	zn	kA	zn
<g/>
(	(	kIx(	(
<g/>
NO	no	k9	no
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
čtyř	čtyři	k4xCgInPc2	čtyři
hydrátů	hydrát	k1gInPc2	hydrát
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
hydroxidu	hydroxid	k1gInSc2	hydroxid
zinečnatého	zinečnatý	k2eAgInSc2d1	zinečnatý
<g/>
,	,	kIx,	,
uhličitanu	uhličitan	k1gInSc2	uhličitan
zinečnatého	zinečnatý	k2eAgInSc2d1	zinečnatý
<g/>
,	,	kIx,	,
oxidu	oxid	k1gInSc2	oxid
zinečnatého	zinečnatý	k2eAgInSc2d1	zinečnatý
nebo	nebo	k8xC	nebo
zinkových	zinkový	k2eAgInPc2d1	zinkový
odpadů	odpad	k1gInPc2	odpad
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
dusičné	dusičný	k2eAgFnSc6d1	dusičná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Síran	síran	k1gInSc4	síran
zinečnatý	zinečnatý	k2eAgInSc4d1	zinečnatý
ZnSO	ZnSO	k1gFnSc7	ZnSO
<g/>
4	[number]	k4	4
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
také	také	k9	také
jako	jako	k8xC	jako
bílá	bílý	k2eAgFnSc1d1	bílá
skalice	skalice	k1gFnSc1	skalice
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
svého	svůj	k3xOyFgInSc2	svůj
heptahydrátu	heptahydrát	k1gInSc2	heptahydrát
síranu	síran	k1gInSc2	síran
zinečnatého	zinečnatý	k2eAgMnSc4d1	zinečnatý
ZnSO	ZnSO	k1gMnSc4	ZnSO
<g/>
4.7	[number]	k4	4.7
H	H	kA	H
<g/>
2	[number]	k4	2
<g/>
O.	O.	kA	O.
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
nerost	nerost	k1gInSc4	nerost
goslarit	goslarita	k1gFnPc2	goslarita
<g/>
.	.	kIx.	.
</s>
<s>
Síran	síran	k1gInSc1	síran
zinečnatý	zinečnatý	k2eAgInSc1d1	zinečnatý
slouží	sloužit	k5eAaImIp3nS	sloužit
jako	jako	k9	jako
součást	součást	k1gFnSc1	součást
barviv	barvivo	k1gNnPc2	barvivo
pro	pro	k7c4	pro
potisk	potisk	k1gInSc4	potisk
tkanin	tkanina	k1gFnPc2	tkanina
i	i	k8xC	i
přípravků	přípravek	k1gInPc2	přípravek
pro	pro	k7c4	pro
impregnaci	impregnace	k1gFnSc4	impregnace
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
lithoponu	lithopon	k1gInSc2	lithopon
<g/>
,	,	kIx,	,
v	v	k7c6	v
galvanostegii	galvanostegie	k1gFnSc6	galvanostegie
<g/>
,	,	kIx,	,
v	v	k7c6	v
lékařství	lékařství	k1gNnSc6	lékařství
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
základní	základní	k2eAgFnSc7d1	základní
látkou	látka	k1gFnSc7	látka
pro	pro	k7c4	pro
přípravu	příprava	k1gFnSc4	příprava
dalších	další	k2eAgFnPc2d1	další
zinečnatých	zinečnatý	k2eAgFnPc2d1	zinečnatá
sloučenin	sloučenina	k1gFnPc2	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Zředěné	zředěný	k2eAgInPc1d1	zředěný
vodné	vodný	k2eAgInPc1d1	vodný
roztoky	roztok	k1gInPc1	roztok
této	tento	k3xDgFnSc2	tento
soli	sůl	k1gFnSc2	sůl
mají	mít	k5eAaImIp3nP	mít
dezinfekční	dezinfekční	k2eAgInPc1d1	dezinfekční
účinky	účinek	k1gInPc1	účinek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
síran	síran	k1gInSc1	síran
zinečnatý	zinečnatý	k2eAgInSc1d1	zinečnatý
adiční	adiční	k2eAgInSc1d1	adiční
a	a	k8xC	a
podvojné	podvojný	k2eAgFnPc1d1	podvojná
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
hydroxidu	hydroxid	k1gInSc2	hydroxid
zinečnatého	zinečnatý	k2eAgInSc2d1	zinečnatý
<g/>
,	,	kIx,	,
uhličitanu	uhličitan	k1gInSc2	uhličitan
zinečnatého	zinečnatý	k2eAgInSc2d1	zinečnatý
<g/>
,	,	kIx,	,
oxidu	oxid	k1gInSc2	oxid
zinečnatého	zinečnatý	k2eAgInSc2d1	zinečnatý
nebo	nebo	k8xC	nebo
zinkových	zinkový	k2eAgInPc2d1	zinkový
odpadů	odpad	k1gInPc2	odpad
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
sírové	sírový	k2eAgFnSc6d1	sírová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kyanid	kyanid	k1gInSc4	kyanid
zinečnatý	zinečnatý	k2eAgInSc4d1	zinečnatý
Zn	zn	kA	zn
<g/>
(	(	kIx(	(
<g/>
CN	CN	kA	CN
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
práškovitá	práškovitý	k2eAgFnSc1d1	práškovitá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
lihu	líh	k1gInSc6	líh
<g/>
,	,	kIx,	,
snadno	snadno	k6eAd1	snadno
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
v	v	k7c6	v
nadbytku	nadbytek	k1gInSc6	nadbytek
kyanidu	kyanid	k1gInSc2	kyanid
na	na	k7c4	na
kyanozinečnatan	kyanozinečnatan	k1gInSc4	kyanozinečnatan
<g/>
.	.	kIx.	.
</s>
<s>
Kyanid	kyanid	k1gInSc1	kyanid
zinečnatý	zinečnatý	k2eAgInSc1d1	zinečnatý
je	být	k5eAaImIp3nS	být
látka	látka	k1gFnSc1	látka
bez	bez	k7c2	bez
chuti	chuť	k1gFnSc2	chuť
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jedovatá	jedovatý	k2eAgFnSc1d1	jedovatá
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
lékařství	lékařství	k1gNnSc6	lékařství
<g/>
.	.	kIx.	.
</s>
<s>
Kyanid	kyanid	k1gInSc1	kyanid
zinečnatý	zinečnatý	k2eAgMnSc1d1	zinečnatý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
srážením	srážení	k1gNnSc7	srážení
roztoků	roztok	k1gInPc2	roztok
zinečnatých	zinečnatý	k2eAgMnPc2d1	zinečnatý
solí	solit	k5eAaImIp3nP	solit
alkalickým	alkalický	k2eAgInSc7d1	alkalický
kyanidem	kyanid	k1gInSc7	kyanid
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Uhličitan	uhličitan	k1gInSc4	uhličitan
zinečnatý	zinečnatý	k2eAgInSc4d1	zinečnatý
ZnCO	ZnCO	k1gFnSc7	ZnCO
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
práškovitá	práškovitý	k2eAgFnSc1d1	práškovitá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
v	v	k7c6	v
zředěných	zředěný	k2eAgInPc6d1	zředěný
roztocích	roztok	k1gInPc6	roztok
kyselin	kyselina	k1gFnPc2	kyselina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jako	jako	k9	jako
nerost	nerost	k1gInSc4	nerost
kalamín	kalamín	k1gInSc1	kalamín
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
neboli	neboli	k8xC	neboli
smithsonit	smithsonit	k1gInSc1	smithsonit
a	a	k8xC	a
hydrozinkit	hydrozinkit	k1gInSc1	hydrozinkit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
připravit	připravit	k5eAaPmF	připravit
různé	různý	k2eAgInPc4d1	různý
zásadité	zásaditý	k2eAgInPc4d1	zásaditý
uhličitany	uhličitan	k1gInPc4	uhličitan
a	a	k8xC	a
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
poté	poté	k6eAd1	poté
adiční	adiční	k2eAgFnPc1d1	adiční
<g/>
,	,	kIx,	,
podvojné	podvojný	k2eAgFnPc1d1	podvojná
i	i	k8xC	i
komplexní	komplexní	k2eAgFnPc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Uhličitan	uhličitan	k1gInSc1	uhličitan
zinečnatý	zinečnatý	k2eAgMnSc1d1	zinečnatý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
srážením	srážení	k1gNnSc7	srážení
roztoků	roztok	k1gInPc2	roztok
rozpustné	rozpustný	k2eAgFnSc2d1	rozpustná
zinečnaté	zinečnatý	k2eAgFnSc2d1	zinečnatá
soli	sůl	k1gFnSc2	sůl
roztokem	roztok	k1gInSc7	roztok
alkalického	alkalický	k2eAgInSc2d1	alkalický
uhličitanu	uhličitan	k1gInSc2	uhličitan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Křemičitan	křemičitan	k1gInSc4	křemičitan
zinečnatý	zinečnatý	k2eAgInSc4d1	zinečnatý
ZnSiO	ZnSiO	k1gFnSc7	ZnSiO
<g/>
3	[number]	k4	3
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
práškovitá	práškovitý	k2eAgFnSc1d1	práškovitá
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jako	jako	k9	jako
nerost	nerost	k1gInSc1	nerost
willemit	willemit	k1gInSc1	willemit
<g/>
,	,	kIx,	,
troosit	troosita	k1gFnPc2	troosita
a	a	k8xC	a
kalamín	kalamín	k1gInSc1	kalamín
křemičitý	křemičitý	k2eAgInSc1d1	křemičitý
<g/>
.	.	kIx.	.
</s>
<s>
Křemičitan	křemičitan	k1gInSc1	křemičitan
zinečnatý	zinečnatý	k2eAgMnSc1d1	zinečnatý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
srážením	srážení	k1gNnSc7	srážení
roztoků	roztok	k1gInPc2	roztok
rozpustné	rozpustný	k2eAgFnSc2d1	rozpustná
zinečnaté	zinečnatý	k2eAgFnSc2d1	zinečnatá
soli	sůl	k1gFnSc2	sůl
alkalickým	alkalický	k2eAgInSc7d1	alkalický
uhličitanem	uhličitan	k1gInSc7	uhličitan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hydrid	hydrid	k1gInSc4	hydrid
zinečnatý	zinečnatý	k2eAgInSc4d1	zinečnatý
ZnH	ZnH	k1gFnSc7	ZnH
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
teplotou	teplota	k1gFnSc7	teplota
nad	nad	k7c7	nad
90	[number]	k4	90
°	°	k?	°
<g/>
C	C	kA	C
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
boranem	boran	k1gInSc7	boran
a	a	k8xC	a
methylidem	methylid	k1gInSc7	methylid
zinečnatým	zinečnatý	k2eAgInSc7d1	zinečnatý
tvoří	tvořit	k5eAaImIp3nS	tvořit
podvojné	podvojný	k2eAgFnPc4d1	podvojná
sloučeniny	sloučenina	k1gFnPc4	sloučenina
rozpustné	rozpustný	k2eAgFnPc4d1	rozpustná
v	v	k7c6	v
etheru	ether	k1gInSc6	ether
<g/>
.	.	kIx.	.
</s>
<s>
Hydrid	hydrid	k1gInSc1	hydrid
zinečnatý	zinečnatý	k2eAgMnSc1d1	zinečnatý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
působením	působení	k1gNnSc7	působení
tetrahydridohlinitanu	tetrahydridohlinitan	k1gInSc2	tetrahydridohlinitan
lithného	lithný	k2eAgInSc2d1	lithný
na	na	k7c4	na
jodid	jodid	k1gInSc4	jodid
zinečnatý	zinečnatý	k2eAgInSc4d1	zinečnatý
nebo	nebo	k8xC	nebo
methylid	methylid	k1gInSc4	methylid
zinečnatý	zinečnatý	k2eAgInSc4d1	zinečnatý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Komplexní	komplexní	k2eAgFnPc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
===	===	k?	===
</s>
</p>
<p>
<s>
Tvorba	tvorba	k1gFnSc1	tvorba
komplexních	komplexní	k2eAgInPc2d1	komplexní
anionů	anion	k1gInPc2	anion
u	u	k7c2	u
zinku	zinek	k1gInSc2	zinek
není	být	k5eNaImIp3nS	být
tak	tak	k6eAd1	tak
výrazná	výrazný	k2eAgFnSc1d1	výrazná
jako	jako	k8xC	jako
u	u	k7c2	u
předchozích	předchozí	k2eAgInPc2d1	předchozí
kovů	kov	k1gInPc2	kov
4	[number]	k4	4
<g/>
.	.	kIx.	.
periody	perioda	k1gFnSc2	perioda
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
předchozích	předchozí	k2eAgInPc2d1	předchozí
kovů	kov	k1gInPc2	kov
však	však	k9	však
velmi	velmi	k6eAd1	velmi
ochotně	ochotně	k6eAd1	ochotně
váže	vázat	k5eAaImIp3nS	vázat
hydroxidové	hydroxidový	k2eAgFnPc4d1	hydroxidový
aniony	aniona	k1gFnPc4	aniona
a	a	k8xC	a
vytváří	vytvářet	k5eAaImIp3nS	vytvářet
tak	tak	k6eAd1	tak
hydroxozinečnatanový	hydroxozinečnatanový	k2eAgInSc4d1	hydroxozinečnatanový
anion	anion	k1gInSc4	anion
a	a	k8xC	a
hydroxozinečnatany	hydroxozinečnatan	k1gInPc4	hydroxozinečnatan
<g/>
.	.	kIx.	.
</s>
<s>
Zinek	zinek	k1gInSc1	zinek
tvoří	tvořit	k5eAaImIp3nS	tvořit
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
velmi	velmi	k6eAd1	velmi
ochotně	ochotně	k6eAd1	ochotně
komplexní	komplexní	k2eAgInPc1d1	komplexní
kationty	kation	k1gInPc1	kation
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
a	a	k8xC	a
amoniakem	amoniak	k1gInSc7	amoniak
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
aquakomplexy	aquakomplex	k1gInPc1	aquakomplex
jsou	být	k5eAaImIp3nP	být
výhradně	výhradně	k6eAd1	výhradně
oktaedrické	oktaedrický	k2eAgFnPc1d1	oktaedrický
<g/>
,	,	kIx,	,
nepodařilo	podařit	k5eNaPmAgNnS	podařit
se	se	k3xPyFc4	se
doposud	doposud	k6eAd1	doposud
v	v	k7c6	v
roztoku	roztok	k1gInSc6	roztok
připravit	připravit	k5eAaPmF	připravit
aminokomplex	aminokomplex	k1gInSc4	aminokomplex
zinku	zinek	k1gInSc2	zinek
s	s	k7c7	s
6	[number]	k4	6
komplexně	komplexně	k6eAd1	komplexně
vázanými	vázaný	k2eAgFnPc7d1	vázaná
molekulami	molekula	k1gFnPc7	molekula
amoniaku	amoniak	k1gInSc2	amoniak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hydroxozinečnatany	Hydroxozinečnatan	k1gInPc1	Hydroxozinečnatan
vznikají	vznikat	k5eAaImIp3nP	vznikat
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
zinku	zinek	k1gInSc2	zinek
<g/>
,	,	kIx,	,
oxidu	oxid	k1gInSc2	oxid
zinečnatého	zinečnatý	k2eAgInSc2d1	zinečnatý
nebo	nebo	k8xC	nebo
hydroxidu	hydroxid	k1gInSc2	hydroxid
zinečnatého	zinečnatý	k2eAgInSc2d1	zinečnatý
v	v	k7c6	v
nadbytečném	nadbytečný	k2eAgNnSc6d1	nadbytečné
množství	množství	k1gNnSc6	množství
alkalického	alkalický	k2eAgInSc2d1	alkalický
hydroxidu	hydroxid	k1gInSc2	hydroxid
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
bezbarvé	bezbarvý	k2eAgFnPc1d1	bezbarvá
sloučeniny	sloučenina	k1gFnPc1	sloučenina
nejčastěji	často	k6eAd3	často
složení	složení	k1gNnSc2	složení
M	M	kA	M
<g/>
2	[number]	k4	2
<g/>
I	I	kA	I
<g/>
[	[	kIx(	[
<g/>
Zn	zn	kA	zn
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
s	s	k7c7	s
tetraedrickým	tetraedrický	k2eAgNnSc7d1	tetraedrický
uspořádáním	uspořádání	k1gNnSc7	uspořádání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
existují	existovat	k5eAaImIp3nP	existovat
i	i	k8xC	i
MI	já	k3xPp1nSc3	já
<g/>
[	[	kIx(	[
<g/>
Zn	zn	kA	zn
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
a	a	k8xC	a
M	M	kA	M
<g/>
4	[number]	k4	4
<g/>
I	I	kA	I
<g/>
[	[	kIx(	[
<g/>
Zn	zn	kA	zn
<g/>
(	(	kIx(	(
<g/>
OH	OH	kA	OH
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Amoniakáty	Amoniakáta	k1gFnPc1	Amoniakáta
zinku	zinek	k1gInSc2	zinek
jsou	být	k5eAaImIp3nP	být
bezbarvé	bezbarvý	k2eAgFnPc1d1	bezbarvá
komplexní	komplexní	k2eAgFnPc1d1	komplexní
sloučeniny	sloučenina	k1gFnPc1	sloučenina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vznikají	vznikat	k5eAaImIp3nP	vznikat
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
sloučenin	sloučenina	k1gFnPc2	sloučenina
zinku	zinek	k1gInSc2	zinek
ve	v	k7c6	v
vodném	vodné	k1gNnSc6	vodné
roztoku	roztok	k1gInSc2	roztok
amoniaku	amoniak	k1gInSc2	amoniak
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
sloučeniny	sloučenina	k1gFnPc1	sloučenina
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
dobře	dobře	k6eAd1	dobře
rozpustné	rozpustný	k2eAgNnSc1d1	rozpustné
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
nejčastěji	často	k6eAd3	často
složení	složení	k1gNnSc1	složení
[	[	kIx(	[
<g/>
Zn	zn	kA	zn
<g/>
(	(	kIx(	(
<g/>
NH	NH	kA	NH
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
s	s	k7c7	s
rovinným	rovinný	k2eAgNnSc7d1	rovinné
uspořádáním	uspořádání	k1gNnSc7	uspořádání
a	a	k8xC	a
[	[	kIx(	[
<g/>
Zn	zn	kA	zn
<g/>
(	(	kIx(	(
<g/>
NH	NH	kA	NH
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
s	s	k7c7	s
tetraedrickým	tetraedrický	k2eAgNnSc7d1	tetraedrický
uspořádáním	uspořádání	k1gNnSc7	uspořádání
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
suché	suchý	k2eAgFnSc6d1	suchá
cestě	cesta	k1gFnSc6	cesta
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
připravit	připravit	k5eAaPmF	připravit
sloučeniny	sloučenina	k1gFnPc4	sloučenina
se	s	k7c7	s
složením	složení	k1gNnSc7	složení
[	[	kIx(	[
<g/>
Zn	zn	kA	zn
<g/>
(	(	kIx(	(
<g/>
NH	NH	kA	NH
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
Zn	zn	kA	zn
<g/>
(	(	kIx(	(
<g/>
NH	NH	kA	NH
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
5	[number]	k4	5
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
a	a	k8xC	a
[	[	kIx(	[
<g/>
Zn	zn	kA	zn
<g/>
(	(	kIx(	(
<g/>
NH	NH	kA	NH
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
6	[number]	k4	6
<g/>
]	]	kIx)	]
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
,	,	kIx,	,
tyto	tento	k3xDgFnPc1	tento
sloučeniny	sloučenina	k1gFnPc1	sloučenina
však	však	k9	však
za	za	k7c2	za
běžných	běžný	k2eAgFnPc2d1	běžná
podmínek	podmínka	k1gFnPc2	podmínka
odštěpují	odštěpovat	k5eAaImIp3nP	odštěpovat
molekuly	molekula	k1gFnPc1	molekula
amoniaku	amoniak	k1gInSc2	amoniak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kyanozinečnatanový	Kyanozinečnatanový	k2eAgInSc1d1	Kyanozinečnatanový
anion	anion	k1gInSc1	anion
je	být	k5eAaImIp3nS	být
bezbarvý	bezbarvý	k2eAgInSc1d1	bezbarvý
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
rozpustný	rozpustný	k2eAgMnSc1d1	rozpustný
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
nejčastěji	často	k6eAd3	často
složení	složení	k1gNnSc2	složení
M	M	kA	M
<g/>
2	[number]	k4	2
<g/>
I	I	kA	I
<g/>
[	[	kIx(	[
<g/>
Zn	zn	kA	zn
<g/>
(	(	kIx(	(
<g/>
CN	CN	kA	CN
<g/>
)	)	kIx)	)
<g/>
4	[number]	k4	4
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
sloučeniny	sloučenina	k1gFnPc1	sloučenina
o	o	k7c6	o
složení	složení	k1gNnSc6	složení
MI	já	k3xPp1nSc3	já
<g/>
[	[	kIx(	[
<g/>
Zn	zn	kA	zn
<g/>
(	(	kIx(	(
<g/>
CN	CN	kA	CN
<g/>
)	)	kIx)	)
<g/>
3	[number]	k4	3
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Roztoky	roztoka	k1gFnPc1	roztoka
kyanidu	kyanid	k1gInSc2	kyanid
zinečnatého	zinečnatý	k2eAgMnSc2d1	zinečnatý
a	a	k8xC	a
měďného	měďný	k1gMnSc2	měďný
v	v	k7c6	v
nadbytečném	nadbytečný	k2eAgInSc6d1	nadbytečný
alkalickém	alkalický	k2eAgInSc6d1	alkalický
kyanidu	kyanid	k1gInSc6	kyanid
slouží	sloužit	k5eAaImIp3nS	sloužit
v	v	k7c6	v
galvanostegii	galvanostegie	k1gFnSc6	galvanostegie
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
mosazných	mosazný	k2eAgInPc2d1	mosazný
povlaků	povlak	k1gInPc2	povlak
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Organické	organický	k2eAgFnPc1d1	organická
sloučeniny	sloučenina	k1gFnPc1	sloučenina
===	===	k?	===
</s>
</p>
<p>
<s>
Octan	octan	k1gInSc4	octan
zinečnatý	zinečnatý	k2eAgInSc4d1	zinečnatý
Zn	zn	kA	zn
<g/>
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
H	H	kA	H
<g/>
3	[number]	k4	3
<g/>
O	o	k7c4	o
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
2	[number]	k4	2
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
se	se	k3xPyFc4	se
však	však	k9	však
částečně	částečně	k6eAd1	částečně
hydrolyticky	hydrolyticky	k6eAd1	hydrolyticky
štěpí	štěpit	k5eAaImIp3nS	štěpit
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
jako	jako	k9	jako
ochranný	ochranný	k2eAgInSc1d1	ochranný
prostředek	prostředek	k1gInSc1	prostředek
proti	proti	k7c3	proti
ohni	oheň	k1gInSc3	oheň
<g/>
,	,	kIx,	,
v	v	k7c6	v
lékařství	lékařství	k1gNnSc1	lékařství
jako	jako	k8xC	jako
kloktadlo	kloktadlo	k1gNnSc1	kloktadlo
a	a	k8xC	a
k	k	k7c3	k
omývání	omývání	k1gNnSc3	omývání
při	při	k7c6	při
kožních	kožní	k2eAgNnPc6d1	kožní
onemocněních	onemocnění	k1gNnPc6	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Octan	octan	k1gInSc1	octan
zinečnatý	zinečnatý	k2eAgMnSc1d1	zinečnatý
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
rozpouštěním	rozpouštění	k1gNnSc7	rozpouštění
oxidu	oxid	k1gInSc2	oxid
zinečnatého	zinečnatý	k2eAgInSc2d1	zinečnatý
v	v	k7c6	v
kyselině	kyselina	k1gFnSc6	kyselina
octové	octový	k2eAgFnSc6d1	octová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Šťavelan	šťavelan	k1gInSc4	šťavelan
zinečnatý	zinečnatý	k2eAgInSc4d1	zinečnatý
ZnC	ZnC	k1gFnSc7	ZnC
<g/>
2	[number]	k4	2
<g/>
O	O	kA	O
<g/>
4	[number]	k4	4
je	být	k5eAaImIp3nS	být
bílá	bílý	k2eAgFnSc1d1	bílá
krystalická	krystalický	k2eAgFnSc1d1	krystalická
látka	látka	k1gFnSc1	látka
<g/>
,	,	kIx,	,
nerozpustná	rozpustný	k2eNgFnSc1d1	nerozpustná
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
rozpustná	rozpustný	k2eAgFnSc1d1	rozpustná
v	v	k7c6	v
nadbytku	nadbytek	k1gInSc6	nadbytek
alkalického	alkalický	k2eAgInSc2d1	alkalický
šťavelanu	šťavelan	k1gInSc2	šťavelan
na	na	k7c4	na
rozpustné	rozpustný	k2eAgFnPc4d1	rozpustná
komplexní	komplexní	k2eAgFnPc4d1	komplexní
sloučeniny	sloučenina	k1gFnPc4	sloučenina
<g/>
.	.	kIx.	.
</s>
<s>
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
srážením	srážení	k1gNnSc7	srážení
roztoku	roztok	k1gInSc2	roztok
rozpustné	rozpustný	k2eAgFnSc2d1	rozpustná
zinečnaté	zinečnatý	k2eAgFnSc2d1	zinečnatá
soli	sůl	k1gFnSc2	sůl
kyselinou	kyselina	k1gFnSc7	kyselina
šťavelovou	šťavelový	k2eAgFnSc7d1	šťavelová
nebo	nebo	k8xC	nebo
alkalickým	alkalický	k2eAgInSc7d1	alkalický
šťavelanem	šťavelan	k1gInSc7	šťavelan
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biologický	biologický	k2eAgInSc4d1	biologický
význam	význam	k1gInSc4	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Zinek	zinek	k1gInSc1	zinek
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c7	mezi
prvky	prvek	k1gInPc7	prvek
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
mají	mít	k5eAaImIp3nP	mít
velmi	velmi	k6eAd1	velmi
významný	významný	k2eAgInSc4d1	významný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
správný	správný	k2eAgInSc4d1	správný
vývoj	vývoj	k1gInSc4	vývoj
všech	všecek	k3xTgInPc2	všecek
živých	živý	k2eAgInPc2d1	živý
organizmů	organizmus	k1gInPc2	organizmus
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
i	i	k8xC	i
živočišných	živočišný	k2eAgInPc2d1	živočišný
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
zinek	zinek	k1gInSc1	zinek
není	být	k5eNaImIp3nS	být
obsažen	obsáhnout	k5eAaPmNgInS	obsáhnout
v	v	k7c6	v
živých	živý	k2eAgFnPc6d1	živá
tkáních	tkáň	k1gFnPc6	tkáň
ve	v	k7c6	v
vysokém	vysoký	k2eAgNnSc6d1	vysoké
množství	množství	k1gNnSc6	množství
–	–	k?	–
uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
tělo	tělo	k1gNnSc1	tělo
dospělého	dospělý	k2eAgMnSc2d1	dospělý
člověka	člověk	k1gMnSc2	člověk
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
pouze	pouze	k6eAd1	pouze
přibližně	přibližně	k6eAd1	přibližně
2	[number]	k4	2
g	g	kA	g
zinku	zinek	k1gInSc2	zinek
<g/>
.	.	kIx.	.
</s>
<s>
Doporučená	doporučený	k2eAgFnSc1d1	Doporučená
denní	denní	k2eAgFnSc1d1	denní
dávka	dávka	k1gFnSc1	dávka
zinku	zinek	k1gInSc2	zinek
v	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
potravě	potrava	k1gFnSc6	potrava
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
25	[number]	k4	25
mg	mg	kA	mg
prvku	prvek	k1gInSc2	prvek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
zinku	zinek	k1gInSc2	zinek
v	v	k7c6	v
organizmu	organizmus	k1gInSc6	organizmus
je	být	k5eAaImIp3nS	být
nezbytnou	zbytný	k2eNgFnSc7d1	zbytný
podmínkou	podmínka	k1gFnSc7	podmínka
pro	pro	k7c4	pro
správné	správný	k2eAgNnSc4d1	správné
fungování	fungování	k1gNnSc4	fungování
řady	řada	k1gFnSc2	řada
enzymatických	enzymatický	k2eAgInPc2d1	enzymatický
systémů	systém	k1gInPc2	systém
–	–	k?	–
nejvýznamnější	významný	k2eAgMnPc4d3	nejvýznamnější
je	být	k5eAaImIp3nS	být
patrně	patrně	k6eAd1	patrně
inzulínový	inzulínový	k2eAgInSc1d1	inzulínový
<g/>
.	.	kIx.	.
</s>
<s>
Přítomnost	přítomnost	k1gFnSc1	přítomnost
zinku	zinek	k1gInSc2	zinek
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
je	být	k5eAaImIp3nS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
době	doba	k1gFnSc6	doba
růstu	růst	k1gInSc2	růst
organizmu	organizmus	k1gInSc2	organizmus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jeho	jeho	k3xOp3gInSc1	jeho
nedostatek	nedostatek	k1gInSc1	nedostatek
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
opožďování	opožďování	k1gNnSc3	opožďování
tělesného	tělesný	k2eAgNnSc2d1	tělesné
i	i	k8xC	i
duševního	duševní	k2eAgNnSc2d1	duševní
dospívání	dospívání	k1gNnSc2	dospívání
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
dospělosti	dospělost	k1gFnSc6	dospělost
<g/>
.	.	kIx.	.
</s>
<s>
Nedostatečné	dostatečný	k2eNgNnSc1d1	nedostatečné
množství	množství	k1gNnSc1	množství
zinku	zinek	k1gInSc2	zinek
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
totiž	totiž	k9	totiž
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
nechtěný	chtěný	k2eNgInSc4d1	nechtěný
úbytek	úbytek	k1gInSc4	úbytek
na	na	k7c6	na
váze	váha	k1gFnSc6	váha
<g/>
,	,	kIx,	,
pomalé	pomalý	k2eAgNnSc1d1	pomalé
hojení	hojení	k1gNnSc1	hojení
ran	rána	k1gFnPc2	rána
<g/>
,	,	kIx,	,
zhoršování	zhoršování	k1gNnSc3	zhoršování
paměti	paměť	k1gFnSc2	paměť
a	a	k8xC	a
smyslové	smyslový	k2eAgFnSc2d1	smyslová
poruchy	porucha	k1gFnSc2	porucha
–	–	k?	–
především	především	k6eAd1	především
zrakové	zrakový	k2eAgFnPc1d1	zraková
<g/>
,	,	kIx,	,
čichové	čichový	k2eAgFnPc1d1	čichová
<g/>
.	.	kIx.	.
</s>
<s>
Zinek	zinek	k1gInSc1	zinek
je	být	k5eAaImIp3nS	být
přítomen	přítomen	k2eAgMnSc1d1	přítomen
v	v	k7c6	v
poměrně	poměrně	k6eAd1	poměrně
značném	značný	k2eAgNnSc6d1	značné
množství	množství	k1gNnSc6	množství
ve	v	k7c6	v
spermatu	sperma	k1gNnSc6	sperma
a	a	k8xC	a
jeho	on	k3xPp3gInSc4	on
dostatek	dostatek	k1gInSc4	dostatek
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
je	být	k5eAaImIp3nS	být
podmínkou	podmínka	k1gFnSc7	podmínka
pro	pro	k7c4	pro
správný	správný	k2eAgInSc4d1	správný
pohlavní	pohlavní	k2eAgInSc4d1	pohlavní
vývoj	vývoj	k1gInSc4	vývoj
i	i	k8xC	i
dokonalou	dokonalý	k2eAgFnSc4d1	dokonalá
funkci	funkce	k1gFnSc4	funkce
pohlavních	pohlavní	k2eAgInPc2d1	pohlavní
orgánů	orgán	k1gInPc2	orgán
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
jsou	být	k5eAaImIp3nP	být
hlavními	hlavní	k2eAgInPc7d1	hlavní
zdroji	zdroj	k1gInPc7	zdroj
zinku	zinek	k1gInSc2	zinek
játra	játra	k1gNnPc1	játra
<g/>
,	,	kIx,	,
tmavé	tmavý	k2eAgNnSc1d1	tmavé
maso	maso	k1gNnSc1	maso
<g/>
,	,	kIx,	,
mléko	mléko	k1gNnSc1	mléko
<g/>
,	,	kIx,	,
vaječné	vaječný	k2eAgInPc1d1	vaječný
žloutky	žloutek	k1gInPc1	žloutek
a	a	k8xC	a
mořští	mořský	k2eAgMnPc1d1	mořský
živočichové	živočich	k1gMnPc1	živočich
–	–	k?	–
především	především	k9	především
ústřice	ústřice	k1gFnSc1	ústřice
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
rostlinných	rostlinný	k2eAgInPc2d1	rostlinný
produktů	produkt	k1gInPc2	produkt
jde	jít	k5eAaImIp3nS	jít
především	především	k9	především
o	o	k7c4	o
celozrnné	celozrnný	k2eAgFnPc4d1	celozrnná
cereálie	cereálie	k1gFnPc4	cereálie
<g/>
,	,	kIx,	,
fazole	fazole	k1gFnPc4	fazole
<g/>
,	,	kIx,	,
ořechy	ořech	k1gInPc4	ořech
a	a	k8xC	a
dýňová	dýňový	k2eAgNnPc4d1	dýňové
semena	semeno	k1gNnPc4	semeno
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
množství	množství	k1gNnSc1	množství
přijímaného	přijímaný	k2eAgInSc2d1	přijímaný
zinku	zinek	k1gInSc2	zinek
<g/>
,	,	kIx,	,
obsaženého	obsažený	k2eAgInSc2d1	obsažený
v	v	k7c6	v
živočišné	živočišný	k2eAgFnSc6d1	živočišná
potravě	potrava	k1gFnSc6	potrava
značně	značně	k6eAd1	značně
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
objem	objem	k1gInSc1	objem
zinku	zinek	k1gInSc2	zinek
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
získán	získat	k5eAaPmNgInS	získat
z	z	k7c2	z
rostlinné	rostlinný	k2eAgFnSc2d1	rostlinná
potravy	potrava	k1gFnSc2	potrava
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
důležité	důležitý	k2eAgNnSc1d1	důležité
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
přísní	přísný	k2eAgMnPc1d1	přísný
vegetariáni	vegetarián	k1gMnPc1	vegetarián
dbali	dbát	k5eAaImAgMnP	dbát
o	o	k7c4	o
dostatečný	dostatečný	k2eAgInSc4d1	dostatečný
příjem	příjem	k1gInSc4	příjem
zinku	zinek	k1gInSc2	zinek
především	především	k9	především
v	v	k7c6	v
případě	případ	k1gInSc6	případ
těhotných	těhotný	k2eAgFnPc2d1	těhotná
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
doporučovaná	doporučovaný	k2eAgFnSc1d1	doporučovaná
dávka	dávka	k1gFnSc1	dávka
zinku	zinek	k1gInSc2	zinek
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
úrovni	úroveň	k1gFnSc6	úroveň
kolem	kolem	k7c2	kolem
25	[number]	k4	25
mg	mg	kA	mg
Zn	zn	kA	zn
<g/>
/	/	kIx~	/
<g/>
den	den	k1gInSc1	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rostlinami	rostlina	k1gFnPc7	rostlina
je	být	k5eAaImIp3nS	být
zinek	zinek	k1gInSc1	zinek
přijímán	přijímat	k5eAaImNgInS	přijímat
z	z	k7c2	z
půdního	půdní	k2eAgInSc2d1	půdní
roztoku	roztok	k1gInSc2	roztok
pomocí	pomocí	k7c2	pomocí
fytometaloforů	fytometalofor	k1gInPc2	fytometalofor
(	(	kIx(	(
<g/>
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
u	u	k7c2	u
železa	železo	k1gNnSc2	železo
<g/>
,	,	kIx,	,
fytometalofory	fytometalofora	k1gFnSc2	fytometalofora
pro	pro	k7c4	pro
železo	železo	k1gNnSc4	železo
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
fytosiderofory	fytosiderofora	k1gFnPc1	fytosiderofora
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
nedostatek	nedostatek	k1gInSc1	nedostatek
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
odumírání	odumírání	k1gNnSc4	odumírání
vzrostných	vzrostný	k2eAgInPc2d1	vzrostný
vrcholů	vrchol	k1gInPc2	vrchol
<g/>
,	,	kIx,	,
nadbytek	nadbytek	k1gInSc1	nadbytek
chlorózu	chloróza	k1gFnSc4	chloróza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Doplněk	doplněk	k1gInSc1	doplněk
stravy	strava	k1gFnSc2	strava
==	==	k?	==
</s>
</p>
<p>
<s>
Absorbovatelnost	Absorbovatelnost	k1gFnSc1	Absorbovatelnost
zinku	zinek	k1gInSc2	zinek
z	z	k7c2	z
doplňků	doplněk	k1gInPc2	doplněk
stravy	strava	k1gFnSc2	strava
u	u	k7c2	u
mladých	mladý	k2eAgMnPc2d1	mladý
lidí	člověk	k1gMnPc2	člověk
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
61,3	[number]	k4	61,3
%	%	kIx~	%
u	u	k7c2	u
citronanu	citronan	k1gInSc2	citronan
zinečnatého	zinečnatý	k2eAgInSc2d1	zinečnatý
<g/>
,	,	kIx,	,
60,9	[number]	k4	60,9
%	%	kIx~	%
u	u	k7c2	u
glukonátu	glukonát	k1gInSc2	glukonát
zinečnatého	zinečnatý	k2eAgNnSc2d1	zinečnatý
a	a	k8xC	a
49,9	[number]	k4	49,9
%	%	kIx~	%
u	u	k7c2	u
oxidu	oxid	k1gInSc2	oxid
zinečnatého	zinečnatý	k2eAgInSc2d1	zinečnatý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
oxidu	oxid	k1gInSc2	oxid
zinečnatého	zinečnatý	k2eAgInSc2d1	zinečnatý
se	se	k3xPyFc4	se
snižuje	snižovat	k5eAaImIp3nS	snižovat
využitelnost	využitelnost	k1gFnSc1	využitelnost
v	v	k7c6	v
případě	případ	k1gInSc6	případ
podání	podání	k1gNnSc2	podání
společně	společně	k6eAd1	společně
s	s	k7c7	s
jídlem	jídlo	k1gNnSc7	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiného	jiný	k2eAgInSc2d1	jiný
výzkumu	výzkum	k1gInSc2	výzkum
je	být	k5eAaImIp3nS	být
využitelnost	využitelnost	k1gFnSc1	využitelnost
zinku	zinek	k1gInSc2	zinek
měřená	měřený	k2eAgFnSc1d1	měřená
koncentrací	koncentrace	k1gFnSc7	koncentrace
v	v	k7c6	v
plazmě	plazma	k1gFnSc6	plazma
ze	z	k7c2	z
glukonátu	glukonát	k1gInSc2	glukonát
zinečnatého	zinečnatý	k2eAgInSc2d1	zinečnatý
v	v	k7c6	v
intervalu	interval	k1gInSc6	interval
10-23	[number]	k4	10-23
%	%	kIx~	%
a	a	k8xC	a
z	z	k7c2	z
oxidu	oxid	k1gInSc2	oxid
zinečnatého	zinečnatý	k2eAgNnSc2d1	zinečnatý
1,9	[number]	k4	1,9
<g/>
-	-	kIx~	-
<g/>
14,3	[number]	k4	14,3
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Cotton	Cotton	k1gInSc1	Cotton
F.	F.	kA	F.
A.	A.	kA	A.
<g/>
,	,	kIx,	,
Wilkinson	Wilkinson	k1gMnSc1	Wilkinson
J.	J.	kA	J.
<g/>
:	:	kIx,	:
<g/>
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
souborné	souborný	k2eAgNnSc1d1	souborné
zpracování	zpracování	k1gNnSc1	zpracování
pro	pro	k7c4	pro
pokročilé	pokročilý	k1gMnPc4	pokročilý
<g/>
,	,	kIx,	,
ACADEMIA	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1973	[number]	k4	1973
</s>
</p>
<p>
<s>
Holzbecher	Holzbechra	k1gFnPc2	Holzbechra
Z.	Z.	kA	Z.
<g/>
:	:	kIx,	:
<g/>
Analytická	analytický	k2eAgFnSc1d1	analytická
chemie	chemie	k1gFnSc1	chemie
<g/>
,	,	kIx,	,
SNTL	SNTL	kA	SNTL
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1974	[number]	k4	1974
</s>
</p>
<p>
<s>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Heinrich	Heinrich	k1gMnSc1	Heinrich
Remy	remy	k1gNnSc2	remy
<g/>
,	,	kIx,	,
Anorganická	anorganický	k2eAgFnSc1d1	anorganická
chemie	chemie	k1gFnSc1	chemie
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc4	vydání
1961	[number]	k4	1961
</s>
</p>
<p>
<s>
N.	N.	kA	N.
N.	N.	kA	N.
Greenwood	Greenwood	k1gInSc1	Greenwood
–	–	k?	–
A.	A.	kA	A.
Earnshaw	Earnshaw	k1gFnSc2	Earnshaw
<g/>
,	,	kIx,	,
Chemie	chemie	k1gFnSc1	chemie
prvků	prvek	k1gInPc2	prvek
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
1993	[number]	k4	1993
ISBN	ISBN	kA	ISBN
80-85427-38-9	[number]	k4	80-85427-38-9
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
zinek	zinek	k1gInSc1	zinek
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
zinek	zinek	k1gInSc4	zinek
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
zinek	zinek	k1gInSc1	zinek
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Zinek	zinek	k1gInSc1	zinek
–	–	k?	–
Chemický	chemický	k2eAgInSc1d1	chemický
vzdělávací	vzdělávací	k2eAgInSc1d1	vzdělávací
portál	portál	k1gInSc1	portál
</s>
</p>
<p>
<s>
Zinek	zinek	k1gInSc1	zinek
v	v	k7c6	v
potravinách	potravina	k1gFnPc6	potravina
</s>
</p>
<p>
<s>
Zinek	zinek	k1gInSc4	zinek
a	a	k8xC	a
lidské	lidský	k2eAgNnSc4d1	lidské
zdraví	zdraví	k1gNnSc4	zdraví
</s>
</p>
