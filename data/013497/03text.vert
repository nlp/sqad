<s>
Prefektura	prefektura	k1gFnSc1
Tottori	Tottor	k1gFnSc2
</s>
<s>
Prefektura	prefektura	k1gFnSc1
Tottori	Tottor	k1gFnSc2
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
vlajka	vlajka	k1gFnSc1
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Prefektura	prefektura	k1gFnSc1
Tottori	Tottor	k1gFnSc2
na	na	k7c6
mapě	mapa	k1gFnSc6
Japonska	Japonsko	k1gNnSc2
Hlavní	hlavní	k2eAgFnSc6d1
město	město	k1gNnSc1
</s>
<s>
Tottori	Tottori	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
35	#num#	k4
<g/>
°	°	k?
<g/>
27	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
133	#num#	k4
<g/>
°	°	k?
<g/>
46	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
3	#num#	k4
507,05	507,05	k4
km²	km²	k?
Obyvatelstvo	obyvatelstvo	k1gNnSc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
549	#num#	k4
925	#num#	k4
(	(	kIx(
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
156,8	156,8	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Oficiální	oficiální	k2eAgInSc4d1
web	web	k1gInSc4
</s>
<s>
www.pref.tottori.lg.jp	www.pref.tottori.lg.jp	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mapa	mapa	k1gFnSc1
prefektury	prefektura	k1gFnSc2
Tottori	Tottor	k1gFnSc2
</s>
<s>
Písečné	písečný	k2eAgFnPc1d1
duny	duna	k1gFnPc1
v	v	k7c6
Tottori	Tottor	k1gFnSc6
</s>
<s>
Vlajka	vlajka	k1gFnSc1
prefektury	prefektura	k1gFnSc2
</s>
<s>
Prefektura	prefektura	k1gFnSc1
Tottori	Tottori	k1gFnSc1
(	(	kIx(
<g/>
japonsky	japonsky	k6eAd1
<g/>
:	:	kIx,
鳥	鳥	kA
<g/>
,	,	kIx,
Tottori-ken	Tottori-ken	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jednou	jedna	k4gFnSc7
ze	z	k7c2
47	#num#	k4
prefektur	prefektura	k1gFnPc2
Japonska	Japonsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
regionu	region	k1gInSc6
Čúgoku	Čúgok	k1gInSc2
na	na	k7c6
ostrově	ostrov	k1gInSc6
Honšú	Honšú	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgNnSc7d1
městem	město	k1gNnSc7
je	být	k5eAaImIp3nS
Tottori	Tottor	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
prefektura	prefektura	k1gFnSc1
s	s	k7c7
nejmenším	malý	k2eAgInSc7d3
počtem	počet	k1gInSc7
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Prefektura	prefektura	k1gFnSc1
má	mít	k5eAaImIp3nS
rozlohu	rozloha	k1gFnSc4
3	#num#	k4
507,21	507,21	k4
km²	km²	k?
a	a	k8xC
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2007	#num#	k4
měla	mít	k5eAaImAgFnS
600	#num#	k4
209	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Geografie	geografie	k1gFnSc1
</s>
<s>
Města	město	k1gNnSc2
</s>
<s>
V	v	k7c6
prefektuře	prefektura	k1gFnSc6
Tottori	Tottori	k1gNnSc2
leží	ležet	k5eAaImIp3nS
4	#num#	k4
velká	velká	k1gFnSc1
města	město	k1gNnSc2
(	(	kIx(
<g/>
市	市	k?
<g/>
,	,	kIx,
ši	ši	k?
<g/>
)	)	kIx)
<g/>
:	:	kIx,
</s>
<s>
Jonago	Jonago	k1gNnSc1
(	(	kIx(
<g/>
米	米	k?
<g/>
)	)	kIx)
</s>
<s>
Kurajoši	Kurajoš	k1gMnPc1
(	(	kIx(
<g/>
倉	倉	k?
<g/>
)	)	kIx)
</s>
<s>
Sakaiminato	Sakaiminato	k6eAd1
(	(	kIx(
<g/>
境	境	k?
<g/>
)	)	kIx)
</s>
<s>
Tottori	Tottori	k1gNnSc1
(	(	kIx(
<g/>
鳥	鳥	k?
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
Ekonomika	ekonomika	k1gFnSc1
</s>
<s>
Hospodářství	hospodářství	k1gNnSc1
prefektury	prefektura	k1gFnSc2
je	být	k5eAaImIp3nS
silně	silně	k6eAd1
závislé	závislý	k2eAgNnSc1d1
na	na	k7c6
zemědělství	zemědělství	k1gNnSc6
a	a	k8xC
rybolovu	rybolov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
celém	celý	k2eAgNnSc6d1
Japonsku	Japonsko	k1gNnSc6
jsou	být	k5eAaImIp3nP
proslulé	proslulý	k2eAgFnPc1d1
zdejší	zdejší	k2eAgFnPc1d1
hrušky	hruška	k1gFnPc1
naši	naši	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
V	v	k7c6
prefektuře	prefektura	k1gFnSc6
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
písečné	písečný	k2eAgFnPc1d1
duny	duna	k1gFnPc1
(	(	kIx(
<g/>
鳥	鳥	k?
<g/>
;	;	kIx,
tottori-sakjú	tottori-sakjú	k?
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
tvoří	tvořit	k5eAaImIp3nP
jedinou	jediný	k2eAgFnSc4d1
japonskou	japonský	k2eAgFnSc4d1
poušť	poušť	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Tottori	Tottor	k1gFnSc2
Prefecture	Prefectur	k1gMnSc5
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Prefektura	prefektura	k1gFnSc1
Tottori	Tottor	k1gInSc6
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
prefektury	prefektura	k1gFnSc2
Tottori	Tottor	k1gFnSc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Japonsko	Japonsko	k1gNnSc1
–	–	k?
日	日	k?
(	(	kIx(
<g/>
Nihon	Nihon	k1gMnSc1
<g/>
)	)	kIx)
–	–	k?
(	(	kIx(
<g/>
J	J	kA
<g/>
)	)	kIx)
Prefektury	prefektura	k1gFnSc2
(	(	kIx(
<g/>
都	都	k?
<g/>
)	)	kIx)
a	a	k8xC
jejich	jejich	k3xOp3gNnPc4
hlavní	hlavní	k2eAgNnPc4d1
města	město	k1gNnPc4
</s>
<s>
Prefektura	prefektura	k1gFnSc1
Aiči	Aič	k1gFnSc2
(	(	kIx(
<g/>
Nagoja	Nagoja	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Akita	Akita	k1gFnSc1
(	(	kIx(
<g/>
Akita	Akita	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Aomori	Aomor	k1gFnSc2
(	(	kIx(
<g/>
Aomori	Aomor	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Čiba	Čiba	k1gFnSc1
(	(	kIx(
<g/>
Čiba	Čiba	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Ehime	Ehim	k1gInSc5
(	(	kIx(
<g/>
Macujama	Macujamum	k1gNnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Fukui	Fuku	k1gFnSc2
(	(	kIx(
<g/>
Fukui	Fuku	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Fukuoka	Fukuoka	k1gFnSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
Fukuoka	Fukuoka	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Fukušima	Fukušima	k1gFnSc1
(	(	kIx(
<g/>
Fukušima	Fukušima	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Gifu	Gifus	k1gInSc2
(	(	kIx(
<g/>
Gifu	Gifus	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Gunma	Gunma	k1gFnSc1
(	(	kIx(
<g/>
Maebaši	Maebaše	k1gFnSc4
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Hirošima	Hirošima	k1gFnSc1
(	(	kIx(
<g/>
Hirošima	Hirošima	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Hokkaidó	Hokkaidó	k1gFnSc1
(	(	kIx(
<g/>
Sapporo	Sappora	k1gFnSc5
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Hjógo	Hjógo	k1gMnSc1
(	(	kIx(
<g/>
Kóbe	Kóbe	k1gNnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Ibaraki	Ibarak	k1gFnSc2
(	(	kIx(
<g/>
Mito	Mito	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Išikawa	Išikawa	k1gFnSc1
(	(	kIx(
<g/>
Kanazawa	Kanazawa	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Iwate	Iwat	k1gInSc5
(	(	kIx(
<g/>
Morioka	Morioko	k1gNnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Jamagata	Jamagata	k1gFnSc1
(	(	kIx(
<g/>
Jamagata	Jamagata	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Jamaguči	Jamaguč	k1gInPc7
(	(	kIx(
<g/>
Jamaguči	Jamaguč	k1gInPc7
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Jamanaši	Jamanaše	k1gFnSc4
(	(	kIx(
<g/>
Kófu	Kófa	k1gFnSc4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Prefektura	prefektura	k1gFnSc1
Kagawa	Kagawa	k1gFnSc1
(	(	kIx(
<g/>
Takamacu	Takamacus	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Kagošima	Kagošima	k1gFnSc1
(	(	kIx(
<g/>
Kagošima	Kagošima	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Kanagawa	Kanagawa	k1gFnSc1
(	(	kIx(
<g/>
Jokohama	Jokohama	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Kjóto	Kjóto	k1gNnSc4
(	(	kIx(
<g/>
Kjóto	Kjóto	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Kóči	Kóč	k1gFnSc2
(	(	kIx(
<g/>
Kóči	Kóč	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Kumamoto	Kumamota	k1gFnSc5
(	(	kIx(
<g/>
Kumamoto	Kumamota	k1gFnSc5
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
Mie	Mie	k1gFnSc1
(	(	kIx(
<g/>
Cu	Cu	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Mijagi	Mijag	k1gFnSc2
(	(	kIx(
<g/>
Sendai	Senda	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Mijazaki	Mijazak	k1gFnSc2
(	(	kIx(
<g/>
Mijazaki	Mijazak	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Nagano	Nagano	k1gNnSc4
(	(	kIx(
<g/>
Nagano	Nagano	k1gNnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Nagasaki	Nagasaki	k1gNnSc2
(	(	kIx(
<g/>
Nagasaki	Nagasaki	k1gNnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Nara	Nara	k1gFnSc1
(	(	kIx(
<g/>
Nara	Nara	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Niigata	Niigata	k1gFnSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
Niigata	Niigata	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Óita	Óita	k1gFnSc1
(	(	kIx(
<g/>
Óita	Óita	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Okajama	Okajama	k1gFnSc1
(	(	kIx(
<g/>
Okajama	Okajama	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Okinawa	Okinawa	k1gFnSc1
(	(	kIx(
<g/>
Naha	naho	k1gNnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Ósaka	Ósaka	k1gFnSc1
(	(	kIx(
<g/>
Ósaka	Ósaka	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Saga	Saga	k1gFnSc1
(	(	kIx(
<g/>
Saga	Saga	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Saitama	Saitama	k1gFnSc1
(	(	kIx(
<g/>
Saitama	Saitama	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Šiga	Šiga	k1gFnSc1
(	(	kIx(
<g/>
Ócu	Ócu	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Šimane	Šiman	k1gMnSc5
(	(	kIx(
<g/>
Macue	Macue	k1gNnSc7
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Šizuoka	Šizuoka	k1gFnSc1
(	(	kIx(
<g/>
Šizuoka	Šizuoka	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Točigi	Točig	k1gFnSc2
(	(	kIx(
<g/>
Ucunomija	Ucunomija	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Tokušima	Tokušima	k1gFnSc1
(	(	kIx(
<g/>
Tokušima	Tokušima	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Tokio	Tokio	k1gNnSc4
(	(	kIx(
<g/>
Šindžuku	Šindžuk	k1gInSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Tottori	Tottor	k1gFnSc2
(	(	kIx(
<g/>
Tottori	Tottor	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Tojama	Tojama	k1gFnSc1
(	(	kIx(
<g/>
Tojama	Tojama	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Wakajama	Wakajama	k1gFnSc1
(	(	kIx(
<g/>
Wakajama	Wakajama	k1gFnSc1
<g/>
)	)	kIx)
Japonské	japonský	k2eAgInPc1d1
regiony	region	k1gInPc1
(	(	kIx(
<g/>
地	地	k?
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Hokkaidó	Hokkaidó	k1gFnSc1
•	•	k?
Tóhoku	Tóhok	k1gInSc2
•	•	k?
Kantó	Kantó	k1gMnSc1
•	•	k?
Čúbu	Čúba	k1gFnSc4
•	•	k?
Kansai	Kansa	k1gFnSc3
•	•	k?
Čúgoku	Čúgok	k1gInSc6
•	•	k?
Šikoku	Šikok	k1gInSc2
•	•	k?
Kjúšú	Kjúšú	k1gMnSc1
</s>
<s>
Prefektura	prefektura	k1gFnSc1
Tottori	Tottor	k1gFnSc2
–	–	k?
鳥	鳥	k?
(	(	kIx(
<g/>
Tottori-ken	Tottori-ken	k1gInSc1
<g/>
)	)	kIx)
Města	město	k1gNnSc2
(	(	kIx(
<g/>
市	市	k?
<g/>
,	,	kIx,
ši	ši	k?
<g/>
)	)	kIx)
</s>
<s>
Jonago	Jonago	k1gNnSc1
(	(	kIx(
<g/>
米	米	k?
<g/>
)	)	kIx)
•	•	k?
Kurajoši	Kurajoš	k1gMnPc1
(	(	kIx(
<g/>
倉	倉	k?
<g/>
)	)	kIx)
•	•	k?
Sakaiminato	Sakaiminat	k2eAgNnSc1d1
(	(	kIx(
<g/>
境	境	k?
<g/>
)	)	kIx)
•	•	k?
Tottori	Tottor	k1gFnSc2
(	(	kIx(
<g/>
鳥	鳥	k?
<g/>
)	)	kIx)
(	(	kIx(
<g/>
hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
prefektury	prefektura	k1gFnSc2
<g/>
)	)	kIx)
Okresy	okres	k1gInPc1
(	(	kIx(
<g/>
郡	郡	k?
<g/>
,	,	kIx,
gun	gun	k?
<g/>
)	)	kIx)
</s>
<s>
městečka	městečko	k1gNnPc4
(	(	kIx(
<g/>
町	町	k?
<g/>
,	,	kIx,
čó	čó	k?
<g/>
)	)	kIx)
<g/>
:	:	kIx,
•	•	k?
vesnice	vesnice	k1gFnSc2
<g/>
(	(	kIx(
<g/>
村	村	k?
<g/>
,	,	kIx,
son	son	k1gInSc1
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Iwami	Iwa	k1gFnPc7
(	(	kIx(
<g/>
岩	岩	k?
<g/>
)	)	kIx)
</s>
<s>
Iwami-čó	Iwami-čó	k?
(	(	kIx(
<g/>
岩	岩	k?
<g/>
)	)	kIx)
Jazu	Jazus	k1gInSc2
(	(	kIx(
<g/>
八	八	k?
<g/>
)	)	kIx)
</s>
<s>
Wakasa-čó	Wakasa-čó	k?
(	(	kIx(
<g/>
若	若	k?
<g/>
)	)	kIx)
•	•	k?
Čizu-čó	Čizu-čó	k1gFnSc1
(	(	kIx(
<g/>
智	智	k?
<g/>
)	)	kIx)
•	•	k?
Jazu-čó	Jazu-čó	k1gFnSc1
(	(	kIx(
<g/>
八	八	k?
<g/>
)	)	kIx)
Tóhaku	Tóhak	k1gInSc2
(	(	kIx(
<g/>
東	東	k?
<g/>
)	)	kIx)
</s>
<s>
Misasa-čó	Misasa-čó	k?
(	(	kIx(
<g/>
三	三	k?
<g/>
)	)	kIx)
•	•	k?
Jurihama-čó	Jurihama-čó	k1gFnSc1
(	(	kIx(
<g/>
湯	湯	k?
<g/>
)	)	kIx)
•	•	k?
Kotoura-čó	Kotoura-čó	k1gFnSc1
(	(	kIx(
<g/>
琴	琴	k?
<g/>
)	)	kIx)
•	•	k?
Hokuei-čó	Hokuei-čó	k1gFnSc1
(	(	kIx(
<g/>
北	北	k?
<g/>
)	)	kIx)
Saihaku	Saihak	k1gInSc2
(	(	kIx(
<g/>
西	西	k?
<g/>
)	)	kIx)
</s>
<s>
Daisen-čó	Daisen-čó	k?
(	(	kIx(
<g/>
大	大	k?
<g/>
)	)	kIx)
•	•	k?
Nanbu-čó	Nanbu-čó	k1gFnSc1
(	(	kIx(
<g/>
南	南	k?
<g/>
)	)	kIx)
•	•	k?
Hóki-čó	Hóki-čó	k1gFnSc1
(	(	kIx(
<g/>
伯	伯	k?
<g/>
)	)	kIx)
•	•	k?
Hiezu-son	Hiezu-son	k1gInSc1
(	(	kIx(
<g/>
日	日	k?
<g/>
)	)	kIx)
Hino	Hino	k1gMnSc1
(	(	kIx(
<g/>
日	日	k?
<g/>
)	)	kIx)
</s>
<s>
Ničinan-čó	Ničinan-čó	k?
(	(	kIx(
<g/>
日	日	k?
<g/>
)	)	kIx)
•	•	k?
Hino-čó	Hino-čó	k1gFnSc1
(	(	kIx(
<g/>
日	日	k?
<g/>
)	)	kIx)
•	•	k?
Kófu-čó	Kófu-čó	k1gFnSc1
(	(	kIx(
<g/>
江	江	k?
<g/>
)	)	kIx)
Jiné	jiný	k2eAgFnPc1d1
</s>
<s>
ostrovy	ostrov	k1gInPc1
<g/>
:	:	kIx,
Aošima	Aošima	k1gFnSc1
(	(	kIx(
<g/>
青	青	k?
<g/>
)	)	kIx)
•	•	k?
Amódžima	Amódžima	k1gFnSc1
(	(	kIx(
<g/>
海	海	k?
<g/>
)	)	kIx)
a	a	k8xC
j.	j.	k?
•	•	k?
jezero	jezero	k1gNnSc1
Kojama-ike	Kojama-ike	k1gFnSc1
(	(	kIx(
<g/>
湖	湖	k?
<g/>
)	)	kIx)
•	•	k?
vodopády	vodopád	k1gInPc4
<g/>
:	:	kIx,
Amedaki	Amedak	k1gFnPc4
(	(	kIx(
<g/>
雨	雨	k?
<g/>
)	)	kIx)
•	•	k?
Daisentaki	Daisentak	k1gFnSc2
(	(	kIx(
<g/>
大	大	k?
<g/>
)	)	kIx)
•	•	k?
duny	duna	k1gFnPc1
<g/>
:	:	kIx,
Tottori	Tottori	k1gNnSc1
sakjú	sakjú	k?
•	•	k?
hory	hora	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
sopky	sopka	k1gFnSc2
<g/>
:	:	kIx,
Daisen	Daisen	k1gInSc1
(	(	kIx(
<g/>
大	大	k?
<g/>
)	)	kIx)
•	•	k?
Karasu-ga-sen	Karasu-ga-sen	k1gInSc1
(	(	kIx(
<g/>
烏	烏	k?
<g/>
)	)	kIx)
•	•	k?
Šičijama	Šičijama	k1gFnSc1
(	(	kIx(
<g/>
駟	駟	k?
<g/>
)	)	kIx)
•	•	k?
Dógojama	Dógojama	k1gFnSc1
(	(	kIx(
<g/>
道	道	k?
<g/>
)	)	kIx)
•	•	k?
Hjónosen	Hjónosen	k2eAgInSc4d1
(	(	kIx(
<g/>
氷	氷	k?
<g/>
)	)	kIx)
•	•	k?
Hiruzen	Hiruzen	k2eAgInSc4d1
(	(	kIx(
<g/>
蒜	蒜	k?
<g/>
)	)	kIx)
•	•	k?
Mitokusan	Mitokusan	k1gInSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
三	三	k?
<g/>
)	)	kIx)
•	•	k?
Mimurojama	Mimurojama	k1gFnSc1
(	(	kIx(
<g/>
三	三	k?
<g/>
)	)	kIx)
•	•	k?
onseny	onsena	k1gFnSc2
<g/>
:	:	kIx,
•	•	k?
Tottori-onsen	Tottori-onsen	k1gInSc1
(	(	kIx(
<g/>
鳥	鳥	k?
<g/>
)	)	kIx)
•	•	k?
Kaike-onsen	Kaike-onsen	k1gInSc1
(	(	kIx(
<g/>
皆	皆	k?
<g/>
)	)	kIx)
•	•	k?
Šikano-onsen	Šikano-onsen	k1gInSc1
(	(	kIx(
<g/>
鹿	鹿	k?
<g/>
)	)	kIx)
•	•	k?
Tógó-onsen	Tógó-onsen	k1gInSc1
(	(	kIx(
<g/>
東	東	k?
<g/>
)	)	kIx)
•	•	k?
hrady	hrad	k1gInPc4
<g/>
:	:	kIx,
Hrad	hrad	k1gInSc4
v	v	k7c6
Tottori	Tottor	k1gFnSc6
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
鳥	鳥	k?
<g/>
)	)	kIx)
•	•	k?
Ueši-džó	Ueši-džó	k1gFnSc1
(	(	kIx(
<g/>
羽	羽	k?
<g/>
)	)	kIx)
•	•	k?
Kawahara-džó	Kawahara-džó	k1gFnSc1
(	(	kIx(
<g/>
河	河	k?
<g/>
)	)	kIx)
•	•	k?
Šikano-džó	Šikano-džó	k1gFnSc1
(	(	kIx(
<g/>
鹿	鹿	k?
<g/>
)	)	kIx)
•	•	k?
Cuzurao-džó	Cuzurao-džó	k1gFnSc1
(	(	kIx(
<g/>
防	防	k?
<g/>
)	)	kIx)
•	•	k?
Fusetendžinjama-džó	Fusetendžinjama-džó	k1gFnSc1
(	(	kIx(
<g/>
布	布	k?
<g/>
)	)	kIx)
•	•	k?
Hoššódži-džó	Hoššódži-džó	k1gFnSc1
(	(	kIx(
<g/>
法	法	k?
<g/>
)	)	kIx)
•	•	k?
Jabase-džó	Jabase-džó	k1gFnSc1
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
八	八	k?
<g/>
)	)	kIx)
•	•	k?
Jonago-džó	Jonago-džó	k1gFnSc1
(	(	kIx(
<g/>
米	米	k?
<g/>
)	)	kIx)
•	•	k?
Wakasaoni-ga-džó	Wakasaoni-ga-džó	k1gFnSc1
(	(	kIx(
<g/>
若	若	k?
<g/>
)	)	kIx)
•	•	k?
chrámy	chrám	k1gInPc4
<g/>
:	:	kIx,
Sanbucu-dži	Sanbucu-dž	k1gFnSc6
(	(	kIx(
<g/>
三	三	k?
<g/>
)	)	kIx)
•	•	k?
Daisen-dži	Daisen-dž	k1gFnSc6
(	(	kIx(
<g/>
大	大	k?
<g/>
)	)	kIx)
•	•	k?
Kannon-in	Kannon-in	k1gInSc1
(	(	kIx(
<g/>
観	観	k?
<g/>
)	)	kIx)
•	•	k?
Genčú-dži	Genčú-dž	k1gFnSc6
(	(	kIx(
<g/>
玄	玄	k?
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Kóun-dži	Kóun-dž	k1gFnSc6
(	(	kIx(
<g/>
興	興	k?
<g/>
)	)	kIx)
•	•	k?
Kózen-dži	Kózen-dž	k1gFnSc6
(	(	kIx(
<g/>
興	興	k?
<g/>
)	)	kIx)
•	•	k?
Daigaku-in	Daigaku-in	k1gInSc1
(	(	kIx(
<g/>
大	大	k?
<g/>
)	)	kIx)
•	•	k?
Dairen-dži	Dairen-dž	k1gFnSc6
(	(	kIx(
<g/>
大	大	k?
<g/>
)	)	kIx)
•	•	k?
Čišaku-dži	Čišaku-dž	k1gFnSc6
(	(	kIx(
<g/>
智	智	k?
<g/>
)	)	kIx)
•	•	k?
Manidera	Manidera	k1gFnSc1
(	(	kIx(
<g/>
摩	摩	k?
<g/>
)	)	kIx)
•	•	k?
Rjútoku-dži	Rjútoku-dž	k1gFnSc6
(	(	kIx(
<g/>
龍	龍	k?
<g/>
)	)	kIx)
•	•	k?
svatyně	svatyně	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
:	:	kIx,
Amenohohinomikoto-džindža	Amenohohinomikoto-džindž	k1gInSc2
(	(	kIx(
<g/>
天	天	k?
<g/>
)	)	kIx)
•	•	k?
Ube-džindža	Ube-džindža	k1gFnSc1
(	(	kIx(
<g/>
宇	宇	k?
<g/>
)	)	kIx)
•	•	k?
Ógamijama-džindža	Ógamijama-džindža	k1gFnSc1
(	(	kIx(
<g/>
大	大	k?
<g/>
)	)	kIx)
•	•	k?
Kanemoči-džindža	Kanemoči-džindža	k1gFnSc1
(	(	kIx(
<g/>
金	金	k?
<g/>
)	)	kIx)
•	•	k?
Šitori-džindža	Šitori-džindža	k1gFnSc1
(	(	kIx(
<g/>
倭	倭	k?
<g/>
)	)	kIx)
•	•	k?
Suwa-džindža	Suwa-džindža	k1gFnSc1
(	(	kIx(
<g/>
諏	諏	k?
<g/>
)	)	kIx)
•	•	k?
Tottori-Tóšógú	Tottori-Tóšógú	k1gFnSc1
(	(	kIx(
<g/>
鳥	鳥	k?
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Nawa-džindža	Nawa-džindž	k1gInSc2
(	(	kIx(
<g/>
名	名	k?
<g/>
)	)	kIx)
•	•	k?
Hakuto-džindža	Hakuto-džindža	k1gFnSc1
(	(	kIx(
<g/>
白	白	k?
<g/>
)	)	kIx)
•	•	k?
Wakasa-džindža	Wakasa-džindža	k1gFnSc1
(	(	kIx(
<g/>
若	若	k?
<g/>
)	)	kIx)
Sousedící	sousedící	k2eAgFnSc2d1
prefektury	prefektura	k1gFnSc2
(	(	kIx(
<g/>
県	県	k?
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Prefektura	prefektura	k1gFnSc1
Hjógo	Hjógo	k1gMnSc1
(	(	kIx(
<g/>
兵	兵	k?
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Okajama	Okajama	k1gFnSc1
(	(	kIx(
<g/>
岡	岡	k?
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Hirošima	Hirošima	k1gFnSc1
(	(	kIx(
<g/>
広	広	k?
<g/>
)	)	kIx)
•	•	k?
Prefektura	prefektura	k1gFnSc1
Šimane	Šiman	k1gMnSc5
(	(	kIx(
<g/>
島	島	k?
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Japonsko	Japonsko	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4389010-6	4389010-6	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0004	#num#	k4
0388	#num#	k4
9984	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
82010829	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
122361243	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
82010829	#num#	k4
</s>
