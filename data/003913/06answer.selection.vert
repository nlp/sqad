<s>
Od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1991	[number]	k4	1991
je	být	k5eAaImIp3nS	být
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc4	Lichtenštejnsko
členem	člen	k1gMnSc7	člen
Evropského	evropský	k2eAgNnSc2d1	Evropské
sdružení	sdružení	k1gNnSc2	sdružení
volného	volný	k2eAgInSc2d1	volný
obchodu	obchod	k1gInSc2	obchod
(	(	kIx(	(
<g/>
ESVO	ESVO	kA	ESVO
-	-	kIx~	-
EFTA	EFTA	kA	EFTA
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
tam	tam	k6eAd1	tam
jeho	jeho	k3xOp3gInPc4	jeho
zájmy	zájem	k1gInPc4	zájem
zastupovalo	zastupovat	k5eAaImAgNnS	zastupovat
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
.	.	kIx.	.
</s>
