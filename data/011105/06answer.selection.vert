<s>
Průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
revoluce	revoluce	k1gFnSc1	revoluce
narušila	narušit	k5eAaPmAgFnS	narušit
přirozený	přirozený	k2eAgInSc4d1	přirozený
koloběh	koloběh	k1gInSc4	koloběh
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
do	do	k7c2	do
ovzduší	ovzduší	k1gNnSc2	ovzduší
začala	začít	k5eAaPmAgFnS	začít
dodávat	dodávat	k5eAaImF	dodávat
velká	velký	k2eAgNnPc4d1	velké
množství	množství	k1gNnSc4	množství
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
