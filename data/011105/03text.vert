<p>
<s>
Globální	globální	k2eAgNnSc1d1	globální
oteplování	oteplování	k1gNnSc1	oteplování
představuje	představovat	k5eAaImIp3nS	představovat
dlouhodobý	dlouhodobý	k2eAgInSc4d1	dlouhodobý
nárůst	nárůst	k1gInSc4	nárůst
průměrné	průměrný	k2eAgFnSc2d1	průměrná
teploty	teplota	k1gFnSc2	teplota
klimatického	klimatický	k2eAgInSc2d1	klimatický
systému	systém	k1gInSc2	systém
Země	zem	k1gFnSc2	zem
<g/>
;	;	kIx,	;
tento	tento	k3xDgInSc4	tento
aspekt	aspekt	k1gInSc4	aspekt
změny	změna	k1gFnSc2	změna
klimatu	klima	k1gNnSc2	klima
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
jak	jak	k6eAd1	jak
v	v	k7c6	v
přístrojových	přístrojový	k2eAgNnPc6d1	přístrojové
měřeních	měření	k1gNnPc6	měření
teploty	teplota	k1gFnSc2	teplota
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
dalšími	další	k2eAgInPc7d1	další
projevy	projev	k1gInPc7	projev
oteplování	oteplování	k1gNnSc2	oteplování
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
se	se	k3xPyFc4	se
všeobecně	všeobecně	k6eAd1	všeobecně
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
pozorované	pozorovaný	k2eAgNnSc4d1	pozorované
oteplování	oteplování	k1gNnSc4	oteplování
způsobené	způsobený	k2eAgNnSc4d1	způsobené
především	především	k6eAd1	především
člověkem	člověk	k1gMnSc7	člověk
od	od	k7c2	od
doby	doba	k1gFnSc2	doba
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
revoluce	revoluce	k1gFnSc2	revoluce
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
předpokládané	předpokládaný	k2eAgNnSc4d1	předpokládané
pokračování	pokračování	k1gNnSc4	pokračování
<g/>
.	.	kIx.	.
</s>
<s>
Období	období	k1gNnSc1	období
globálního	globální	k2eAgNnSc2d1	globální
oteplování	oteplování	k1gNnSc2	oteplování
existovaly	existovat	k5eAaImAgFnP	existovat
i	i	k9	i
v	v	k7c6	v
dávnější	dávný	k2eAgFnSc6d2	dávnější
historii	historie	k1gFnSc6	historie
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
tyto	tento	k3xDgInPc4	tento
jevy	jev	k1gInPc4	jev
používají	používat	k5eAaImIp3nP	používat
termíny	termín	k1gInPc4	termín
globální	globální	k2eAgFnSc2d1	globální
oteplování	oteplování	k1gNnSc3	oteplování
i	i	k8xC	i
globální	globální	k2eAgFnSc1d1	globální
změna	změna	k1gFnSc1	změna
klimatu	klima	k1gNnSc2	klima
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
vzájemně	vzájemně	k6eAd1	vzájemně
zaměnitelné	zaměnitelný	k2eAgInPc1d1	zaměnitelný
<g/>
,	,	kIx,	,
pojem	pojem	k1gInSc4	pojem
klimatická	klimatický	k2eAgFnSc1d1	klimatická
změna	změna	k1gFnSc1	změna
však	však	k9	však
šířeji	šířej	k1gMnSc3	šířej
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
jak	jak	k6eAd1	jak
globální	globální	k2eAgNnSc4d1	globální
oteplování	oteplování	k1gNnSc4	oteplování
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
i	i	k9	i
jeho	jeho	k3xOp3gInPc4	jeho
další	další	k2eAgInPc4d1	další
účinky	účinek	k1gInPc4	účinek
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
změny	změna	k1gFnPc1	změna
srážek	srážka	k1gFnPc2	srážka
a	a	k8xC	a
různé	různý	k2eAgInPc4d1	různý
regionální	regionální	k2eAgInPc4d1	regionální
dopady	dopad	k1gInPc4	dopad
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohé	k1gNnSc1	mnohé
z	z	k7c2	z
pozorovaných	pozorovaný	k2eAgFnPc2d1	pozorovaná
změn	změna	k1gFnPc2	změna
od	od	k7c2	od
padesátých	padesátý	k4xOgNnPc2	padesátý
let	léto	k1gNnPc2	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
instrumentálních	instrumentální	k2eAgNnPc2d1	instrumentální
teplotních	teplotní	k2eAgNnPc2d1	teplotní
měření	měření	k1gNnPc2	měření
bezprecedentní	bezprecedentní	k2eAgInPc1d1	bezprecedentní
a	a	k8xC	a
nemají	mít	k5eNaImIp3nP	mít
obdoby	obdoba	k1gFnPc4	obdoba
ani	ani	k8xC	ani
v	v	k7c6	v
historických	historický	k2eAgFnPc6d1	historická
a	a	k8xC	a
paleoklimatických	paleoklimatický	k2eAgFnPc6d1	paleoklimatický
proxy	prox	k1gInPc1	prox
záznamech	záznam	k1gInPc6	záznam
klimatických	klimatický	k2eAgFnPc2d1	klimatická
změn	změna	k1gFnPc2	změna
po	po	k7c4	po
tisíce	tisíc	k4xCgInPc4	tisíc
až	až	k9	až
milióny	milión	k4xCgInPc4	milión
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
Termín	termín	k1gInSc1	termín
"	"	kIx"	"
<g/>
globální	globální	k2eAgNnSc4d1	globální
oteplování	oteplování	k1gNnSc4	oteplování
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
používán	používat	k5eAaImNgInS	používat
pro	pro	k7c4	pro
zvyšování	zvyšování	k1gNnSc4	zvyšování
průměrné	průměrný	k2eAgFnSc2d1	průměrná
teploty	teplota	k1gFnSc2	teplota
<g />
.	.	kIx.	.
</s>
<s>
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
povrchových	povrchový	k2eAgFnPc2d1	povrchová
vod	voda	k1gFnPc2	voda
<g/>
,	,	kIx,	,
odborníci	odborník	k1gMnPc1	odborník
však	však	k9	však
uvádějí	uvádět	k5eAaImIp3nP	uvádět
10	[number]	k4	10
indikátorů	indikátor	k1gInPc2	indikátor
globálního	globální	k2eAgNnSc2d1	globální
oteplování	oteplování	k1gNnSc2	oteplování
–	–	k?	–
kromě	kromě	k7c2	kromě
vzestupu	vzestup	k1gInSc2	vzestup
teploty	teplota	k1gFnSc2	teplota
vzduchu	vzduch	k1gInSc2	vzduch
nad	nad	k7c7	nad
povrchem	povrch	k1gInSc7	povrch
země	zem	k1gFnSc2	zem
a	a	k8xC	a
nad	nad	k7c7	nad
oceány	oceán	k1gInPc7	oceán
a	a	k8xC	a
vzestupu	vzestup	k1gInSc2	vzestup
teploty	teplota	k1gFnSc2	teplota
povrchových	povrchový	k2eAgFnPc2d1	povrchová
vrstev	vrstva	k1gFnPc2	vrstva
oceánů	oceán	k1gInPc2	oceán
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
úbytek	úbytek	k1gInSc1	úbytek
mořského	mořský	k2eAgInSc2d1	mořský
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
tání	tání	k1gNnSc1	tání
ledovců	ledovec	k1gInPc2	ledovec
<g/>
,	,	kIx,	,
vzestup	vzestup	k1gInSc1	vzestup
hladiny	hladina	k1gFnSc2	hladina
moří	mořit	k5eAaImIp3nS	mořit
<g/>
,	,	kIx,	,
vzestup	vzestup	k1gInSc1	vzestup
vzdušné	vzdušný	k2eAgFnSc2d1	vzdušná
vlhkosti	vlhkost	k1gFnSc2	vlhkost
<g/>
,	,	kIx,	,
úbytek	úbytek	k1gInSc4	úbytek
sněhu	sníh	k1gInSc2	sníh
a	a	k8xC	a
vzestup	vzestup	k1gInSc4	vzestup
tepla	teplo	k1gNnSc2	teplo
celkově	celkově	k6eAd1	celkově
zachyceného	zachycený	k2eAgInSc2d1	zachycený
v	v	k7c6	v
oceánech	oceán	k1gInPc6	oceán
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
většině	většina	k1gFnSc3	většina
navýšení	navýšení	k1gNnSc2	navýšení
akumulované	akumulovaný	k2eAgFnSc2d1	akumulovaná
tepelné	tepelný	k2eAgFnSc2d1	tepelná
energie	energie	k1gFnSc2	energie
(	(	kIx(	(
<g/>
90	[number]	k4	90
%	%	kIx~	%
<g/>
)	)	kIx)	)
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
došlo	dojít	k5eAaPmAgNnS	dojít
právě	právě	k6eAd1	právě
v	v	k7c6	v
oceánech	oceán	k1gInPc6	oceán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1910	[number]	k4	1910
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
nárůstu	nárůst	k1gInSc3	nárůst
teploty	teplota	k1gFnSc2	teplota
vzduchu	vzduch	k1gInSc2	vzduch
a	a	k8xC	a
povrchových	povrchový	k2eAgFnPc2d1	povrchová
vod	voda	k1gFnPc2	voda
o	o	k7c4	o
1,0	[number]	k4	1,0
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
asi	asi	k9	asi
dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
nárůstu	nárůst	k1gInSc2	nárůst
nastaly	nastat	k5eAaPmAgFnP	nastat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
<g/>
.	.	kIx.	.
</s>
<s>
Každé	každý	k3xTgNnSc1	každý
z	z	k7c2	z
posledních	poslední	k2eAgNnPc2d1	poslední
tří	tři	k4xCgNnPc2	tři
desetiletí	desetiletí	k1gNnPc2	desetiletí
bylo	být	k5eAaImAgNnS	být
postupně	postupně	k6eAd1	postupně
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Země	zem	k1gFnSc2	zem
teplejší	teplý	k2eAgInSc4d2	teplejší
než	než	k8xS	než
jakékoli	jakýkoli	k3yIgInPc4	jakýkoli
z	z	k7c2	z
předcházející	předcházející	k2eAgFnSc2d1	předcházející
desetiletí	desetiletí	k1gNnPc2	desetiletí
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
uvedla	uvést	k5eAaPmAgFnS	uvést
Pátá	pátý	k4xOgFnSc1	pátý
hodnotící	hodnotící	k2eAgFnSc1d1	hodnotící
zpráva	zpráva	k1gFnSc1	zpráva
Mezivládního	mezivládní	k2eAgInSc2d1	mezivládní
panelu	panel	k1gInSc2	panel
pro	pro	k7c4	pro
změnu	změna	k1gFnSc4	změna
klimatu	klima	k1gNnSc2	klima
(	(	kIx(	(
<g/>
IPCC	IPCC	kA	IPCC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
je	být	k5eAaImIp3nS	být
extrémně	extrémně	k6eAd1	extrémně
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidský	lidský	k2eAgInSc1d1	lidský
vliv	vliv	k1gInSc1	vliv
je	být	k5eAaImIp3nS	být
dominantní	dominantní	k2eAgFnSc7d1	dominantní
příčinou	příčina	k1gFnSc7	příčina
pozorovaného	pozorovaný	k2eAgNnSc2d1	pozorované
oteplování	oteplování	k1gNnSc2	oteplování
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Největší	veliký	k2eAgInSc1d3	veliký
antropogenní	antropogenní	k2eAgInSc1d1	antropogenní
vliv	vliv	k1gInSc1	vliv
je	být	k5eAaImIp3nS	být
způsoben	způsobit	k5eAaPmNgInS	způsobit
emisemi	emise	k1gFnPc7	emise
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
<g/>
,	,	kIx,	,
methan	methan	k1gInSc1	methan
a	a	k8xC	a
oxid	oxid	k1gInSc1	oxid
dusný	dusný	k2eAgInSc1d1	dusný
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
dominantní	dominantní	k2eAgFnSc3d1	dominantní
roli	role	k1gFnSc3	role
lidské	lidský	k2eAgFnSc2d1	lidská
činnosti	činnost	k1gFnSc2	činnost
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
mluví	mluvit	k5eAaImIp3nS	mluvit
o	o	k7c4	o
"	"	kIx"	"
<g/>
antropogenním	antropogenní	k2eAgNnSc7d1	antropogenní
globálním	globální	k2eAgNnSc7d1	globální
oteplováním	oteplování	k1gNnSc7	oteplování
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
antropogenní	antropogenní	k2eAgFnSc3d1	antropogenní
změně	změna	k1gFnSc3	změna
klimatu	klima	k1gNnSc2	klima
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
zjištění	zjištění	k1gNnPc1	zjištění
akceptují	akceptovat	k5eAaBmIp3nP	akceptovat
akademie	akademie	k1gFnPc1	akademie
věd	věda	k1gFnPc2	věda
všech	všecek	k3xTgInPc2	všecek
významných	významný	k2eAgInPc2d1	významný
industrializovaných	industrializovaný	k2eAgInPc2d1	industrializovaný
států	stát	k1gInPc2	stát
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
zpochybněna	zpochybnit	k5eAaPmNgFnS	zpochybnit
jakýmkoliv	jakýkoliv	k3yIgInSc7	jakýkoliv
státním	státní	k2eAgMnSc7d1	státní
či	či	k8xC	či
mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
vědeckým	vědecký	k2eAgInSc7d1	vědecký
orgánem	orgán	k1gInSc7	orgán
<g/>
.	.	kIx.	.
<g/>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2100	[number]	k4	2100
by	by	kYmCp3nP	by
měla	mít	k5eAaImAgFnS	mít
povrchová	povrchový	k2eAgFnSc1d1	povrchová
teplota	teplota	k1gFnSc1	teplota
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
stoupnout	stoupnout	k5eAaPmF	stoupnout
o	o	k7c4	o
0,3	[number]	k4	0,3
až	až	k9	až
1,7	[number]	k4	1,7
°	°	k?	°
<g/>
C	C	kA	C
pro	pro	k7c4	pro
scénáře	scénář	k1gInPc4	scénář
s	s	k7c7	s
výrazným	výrazný	k2eAgNnSc7d1	výrazné
snižováním	snižování	k1gNnSc7	snižování
produkce	produkce	k1gFnSc2	produkce
CO	co	k9	co
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
o	o	k7c4	o
2,6	[number]	k4	2,6
až	až	k9	až
4,8	[number]	k4	4,8
°	°	k?	°
<g/>
C	C	kA	C
pro	pro	k7c4	pro
scénář	scénář	k1gInSc4	scénář
s	s	k7c7	s
dnešním	dnešní	k2eAgNnSc7d1	dnešní
tempem	tempo	k1gNnSc7	tempo
produkce	produkce	k1gFnSc2	produkce
CO	co	k9	co
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Nejistoty	nejistota	k1gFnPc1	nejistota
v	v	k7c6	v
odhadech	odhad	k1gInPc6	odhad
nárůstu	nárůst	k1gInSc2	nárůst
teploty	teplota	k1gFnPc1	teplota
plynou	plynout	k5eAaImIp3nP	plynout
z	z	k7c2	z
používání	používání	k1gNnSc2	používání
modelů	model	k1gInPc2	model
s	s	k7c7	s
různou	různý	k2eAgFnSc7d1	různá
citlivostí	citlivost	k1gFnSc7	citlivost
změny	změna	k1gFnSc2	změna
teploty	teplota	k1gFnSc2	teplota
na	na	k7c4	na
koncentraci	koncentrace	k1gFnSc4	koncentrace
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Očekávané	očekávaný	k2eAgNnSc1d1	očekávané
budoucí	budoucí	k2eAgNnSc1d1	budoucí
oteplování	oteplování	k1gNnSc1	oteplování
a	a	k8xC	a
související	související	k2eAgFnPc1d1	související
změny	změna	k1gFnPc1	změna
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
rovnoměrné	rovnoměrný	k2eAgFnPc1d1	rovnoměrná
a	a	k8xC	a
budou	být	k5eAaImBp3nP	být
se	se	k3xPyFc4	se
lišit	lišit	k5eAaImF	lišit
region	region	k1gInSc4	region
od	od	k7c2	od
regionu	region	k1gInSc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Variabilita	variabilita	k1gFnSc1	variabilita
klimatu	klima	k1gNnSc2	klima
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
lokálně	lokálně	k6eAd1	lokálně
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
globálně	globálně	k6eAd1	globálně
snižovat	snižovat	k5eAaImF	snižovat
<g/>
.	.	kIx.	.
</s>
<s>
Očekává	očekávat	k5eAaImIp3nS	očekávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
oteplování	oteplování	k1gNnSc2	oteplování
bude	být	k5eAaImBp3nS	být
větší	veliký	k2eAgFnSc1d2	veliký
nad	nad	k7c7	nad
pevninou	pevnina	k1gFnSc7	pevnina
než	než	k8xS	než
nad	nad	k7c7	nad
oceány	oceán	k1gInPc7	oceán
a	a	k8xC	a
nejvýraznější	výrazný	k2eAgFnSc1d3	nejvýraznější
bude	být	k5eAaImBp3nS	být
v	v	k7c6	v
Arktidě	Arktida	k1gFnSc6	Arktida
<g/>
,	,	kIx,	,
a	a	k8xC	a
bude	být	k5eAaImBp3nS	být
spojeno	spojit	k5eAaPmNgNnS	spojit
s	s	k7c7	s
pokračujícím	pokračující	k2eAgNnSc7d1	pokračující
táním	tání	k1gNnSc7	tání
ledovců	ledovec	k1gInPc2	ledovec
<g/>
,	,	kIx,	,
věčně	věčně	k6eAd1	věčně
zmrzlé	zmrzlý	k2eAgFnSc2d1	zmrzlá
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
mořského	mořský	k2eAgInSc2d1	mořský
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
bude	být	k5eAaImBp3nS	být
doprovázet	doprovázet	k5eAaImF	doprovázet
zvyšování	zvyšování	k1gNnSc4	zvyšování
hladiny	hladina	k1gFnSc2	hladina
oceánů	oceán	k1gInPc2	oceán
<g/>
,	,	kIx,	,
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
a	a	k8xC	a
formě	forma	k1gFnSc6	forma
srážek	srážka	k1gFnPc2	srážka
a	a	k8xC	a
rozšiřování	rozšiřování	k1gNnSc6	rozšiřování
subtropických	subtropický	k2eAgFnPc2d1	subtropická
pouští	poušť	k1gFnPc2	poušť
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
očekávané	očekávaný	k2eAgInPc4d1	očekávaný
jevy	jev	k1gInPc4	jev
patří	patřit	k5eAaImIp3nP	patřit
častější	častý	k2eAgInPc4d2	častější
extrémní	extrémní	k2eAgInPc4d1	extrémní
projevy	projev	k1gInPc4	projev
počasí	počasí	k1gNnSc2	počasí
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
období	období	k1gNnSc4	období
veder	vedro	k1gNnPc2	vedro
<g/>
,	,	kIx,	,
suchá	suchý	k2eAgNnPc1d1	suché
období	období	k1gNnPc1	období
<g/>
,	,	kIx,	,
lesní	lesní	k2eAgFnSc1d1	lesní
požáry	požár	k1gInPc4	požár
<g/>
,	,	kIx,	,
přívalové	přívalový	k2eAgInPc4d1	přívalový
deště	dešť	k1gInPc4	dešť
se	s	k7c7	s
záplavami	záplava	k1gFnPc7	záplava
<g/>
,	,	kIx,	,
intenzivní	intenzivní	k2eAgFnPc1d1	intenzivní
sněhové	sněhový	k2eAgFnPc1d1	sněhová
srážky	srážka	k1gFnPc1	srážka
<g/>
,	,	kIx,	,
okyselování	okyselování	k1gNnSc1	okyselování
oceánů	oceán	k1gInPc2	oceán
či	či	k8xC	či
masivní	masivní	k2eAgNnPc4d1	masivní
vymírání	vymírání	k1gNnPc4	vymírání
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
následků	následek	k1gInPc2	následek
významných	významný	k2eAgInPc2d1	významný
pro	pro	k7c4	pro
člověka	člověk	k1gMnSc4	člověk
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
především	především	k9	především
ztráta	ztráta	k1gFnSc1	ztráta
potravinové	potravinový	k2eAgFnSc2d1	potravinová
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
kvůli	kvůli	k7c3	kvůli
klesajícímu	klesající	k2eAgInSc3d1	klesající
výnosu	výnos	k1gInSc3	výnos
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
plodin	plodina	k1gFnPc2	plodina
a	a	k8xC	a
ztráta	ztráta	k1gFnSc1	ztráta
obyvatelného	obyvatelný	k2eAgNnSc2d1	obyvatelné
prostředí	prostředí	k1gNnSc2	prostředí
zaplavením	zaplavení	k1gNnSc7	zaplavení
pobřežních	pobřežní	k2eAgFnPc2d1	pobřežní
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
klimatický	klimatický	k2eAgInSc1d1	klimatický
systém	systém	k1gInSc1	systém
má	mít	k5eAaImIp3nS	mít
velkou	velký	k2eAgFnSc4d1	velká
setrvačnost	setrvačnost	k1gFnSc4	setrvačnost
a	a	k8xC	a
skleníkové	skleníkový	k2eAgInPc1d1	skleníkový
plyny	plyn	k1gInPc1	plyn
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
mnohé	mnohý	k2eAgFnPc4d1	mnohá
z	z	k7c2	z
těchto	tento	k3xDgInPc2	tento
účinků	účinek	k1gInPc2	účinek
přetrvají	přetrvat	k5eAaPmIp3nP	přetrvat
nejen	nejen	k6eAd1	nejen
desetiletí	desetiletí	k1gNnPc4	desetiletí
nebo	nebo	k8xC	nebo
staletí	staletí	k1gNnPc4	staletí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k8xC	i
desítky	desítka	k1gFnPc4	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
možným	možný	k2eAgFnPc3d1	možná
reakcím	reakce	k1gFnPc3	reakce
společnosti	společnost	k1gFnSc2	společnost
na	na	k7c4	na
globální	globální	k2eAgNnSc4d1	globální
oteplování	oteplování	k1gNnSc4	oteplování
patří	patřit	k5eAaImIp3nS	patřit
zmírňování	zmírňování	k1gNnSc4	zmírňování
změny	změna	k1gFnSc2	změna
klimatu	klima	k1gNnSc2	klima
(	(	kIx(	(
<g/>
mitigační	mitigační	k2eAgNnPc1d1	mitigační
opatření	opatření	k1gNnPc1	opatření
<g/>
)	)	kIx)	)
snížením	snížení	k1gNnSc7	snížení
emisí	emise	k1gFnPc2	emise
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
adaptace	adaptace	k1gFnSc1	adaptace
na	na	k7c4	na
globální	globální	k2eAgNnSc4d1	globální
oteplování	oteplování	k1gNnSc4	oteplování
budováním	budování	k1gNnSc7	budování
systémů	systém	k1gInPc2	systém
odolných	odolný	k2eAgInPc2d1	odolný
vůči	vůči	k7c3	vůči
jeho	jeho	k3xOp3gInPc3	jeho
účinkům	účinek	k1gInPc3	účinek
(	(	kIx(	(
<g/>
resilience	resilience	k1gFnSc2	resilience
<g/>
)	)	kIx)	)
a	a	k8xC	a
případné	případný	k2eAgNnSc1d1	případné
klimatické	klimatický	k2eAgNnSc1d1	klimatické
inženýrství	inženýrství	k1gNnSc1	inženýrství
(	(	kIx(	(
<g/>
geoengineering	geoengineering	k1gInSc1	geoengineering
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
států	stát	k1gInPc2	stát
světa	svět	k1gInSc2	svět
je	být	k5eAaImIp3nS	být
účastníky	účastník	k1gMnPc4	účastník
Rámcové	rámcový	k2eAgFnSc2d1	rámcová
úmluvy	úmluva	k1gFnSc2	úmluva
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
o	o	k7c6	o
změně	změna	k1gFnSc6	změna
klimatu	klima	k1gNnSc2	klima
(	(	kIx(	(
<g/>
UNFCCC	UNFCCC	kA	UNFCCC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
cílem	cíl	k1gInSc7	cíl
je	být	k5eAaImIp3nS	být
zabránit	zabránit	k5eAaPmF	zabránit
nebezpečným	bezpečný	k2eNgFnPc3d1	nebezpečná
antropogenním	antropogenní	k2eAgFnPc3d1	antropogenní
změnám	změna	k1gFnPc3	změna
klimatu	klima	k1gNnSc2	klima
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
státy	stát	k1gInPc1	stát
UNFCCC	UNFCCC	kA	UNFCCC
přijaly	přijmout	k5eAaPmAgInP	přijmout
Pařížskou	pařížský	k2eAgFnSc4d1	Pařížská
dohodu	dohoda	k1gFnSc4	dohoda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
omezit	omezit	k5eAaPmF	omezit
globální	globální	k2eAgNnSc4d1	globální
oteplování	oteplování	k1gNnSc4	oteplování
na	na	k7c4	na
výrazně	výrazně	k6eAd1	výrazně
nižší	nízký	k2eAgFnSc4d2	nižší
hodnotu	hodnota	k1gFnSc4	hodnota
než	než	k8xS	než
2,0	[number]	k4	2,0
°	°	k?	°
<g/>
C	C	kA	C
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
předindustriální	předindustriální	k2eAgFnSc7d1	předindustriální
úrovní	úroveň	k1gFnSc7	úroveň
<g/>
,	,	kIx,	,
s	s	k7c7	s
úsilím	úsilí	k1gNnSc7	úsilí
o	o	k7c4	o
omezení	omezení	k1gNnSc4	omezení
oteplení	oteplení	k1gNnSc2	oteplení
do	do	k7c2	do
1,5	[number]	k4	1,5
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2018	[number]	k4	2018
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
Zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
zpráva	zpráva	k1gFnSc1	zpráva
<g />
.	.	kIx.	.
</s>
<s>
IPCC	IPCC	kA	IPCC
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
významný	významný	k2eAgInSc4d1	významný
rozdíl	rozdíl	k1gInSc4	rozdíl
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
podaří	podařit	k5eAaPmIp3nS	podařit
globální	globální	k2eAgNnSc4d1	globální
oteplování	oteplování	k1gNnSc4	oteplování
udržet	udržet	k5eAaPmF	udržet
na	na	k7c6	na
hodnotě	hodnota	k1gFnSc6	hodnota
1,5	[number]	k4	1,5
°	°	k?	°
<g/>
C	C	kA	C
oproti	oproti	k7c3	oproti
2	[number]	k4	2
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2018	[number]	k4	2018
se	se	k3xPyFc4	se
na	na	k7c6	na
konferenci	konference	k1gFnSc6	konference
v	v	k7c6	v
Katovicích	Katovice	k1gFnPc6	Katovice
státy	stát	k1gInPc4	stát
shodly	shodnout	k5eAaBmAgFnP	shodnout
na	na	k7c6	na
části	část	k1gFnSc6	část
konkrétních	konkrétní	k2eAgNnPc2d1	konkrétní
opatření	opatření	k1gNnPc2	opatření
k	k	k7c3	k
plnění	plnění	k1gNnSc3	plnění
Pařížské	pařížský	k2eAgFnSc2d1	Pařížská
dohody	dohoda	k1gFnSc2	dohoda
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2020	[number]	k4	2020
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Pozorované	pozorovaný	k2eAgFnPc1d1	pozorovaná
změny	změna	k1gFnPc1	změna
teploty	teplota	k1gFnSc2	teplota
==	==	k?	==
</s>
</p>
<p>
<s>
Mnoho	mnoho	k4c1	mnoho
nezávisle	závisle	k6eNd1	závisle
vytvořených	vytvořený	k2eAgInPc2d1	vytvořený
datových	datový	k2eAgInPc2d1	datový
souborů	soubor	k1gInPc2	soubor
potvrzuje	potvrzovat	k5eAaImIp3nS	potvrzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
se	se	k3xPyFc4	se
globální	globální	k2eAgFnSc1d1	globální
průměrná	průměrný	k2eAgFnSc1d1	průměrná
(	(	kIx(	(
<g/>
povrchová	povrchový	k2eAgFnSc1d1	povrchová
a	a	k8xC	a
oceánská	oceánský	k2eAgFnSc1d1	oceánská
<g/>
)	)	kIx)	)
povrchová	povrchový	k2eAgFnSc1d1	povrchová
teplota	teplota	k1gFnSc1	teplota
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
o	o	k7c4	o
1,0	[number]	k4	1,0
(	(	kIx(	(
<g/>
0,8	[number]	k4	0,8
až	až	k9	až
1,2	[number]	k4	1,2
<g/>
)	)	kIx)	)
°	°	k?	°
<g/>
C.	C.	kA	C.
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
průměrná	průměrný	k2eAgFnSc1d1	průměrná
povrchová	povrchový	k2eAgFnSc1d1	povrchová
teplota	teplota	k1gFnSc1	teplota
Země	zem	k1gFnSc2	zem
o	o	k7c4	o
0,74	[number]	k4	0,74
±	±	k?	±
0,18	[number]	k4	0,18
°	°	k?	°
<g/>
C.	C.	kA	C.
Rychlost	rychlost	k1gFnSc1	rychlost
oteplování	oteplování	k1gNnSc2	oteplování
se	se	k3xPyFc4	se
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
téměř	téměř	k6eAd1	téměř
zdvojnásobila	zdvojnásobit	k5eAaPmAgFnS	zdvojnásobit
(	(	kIx(	(
<g/>
0,13	[number]	k4	0,13
±	±	k?	±
0,03	[number]	k4	0,03
°	°	k?	°
<g/>
C	C	kA	C
za	za	k7c4	za
desetiletí	desetiletí	k1gNnPc4	desetiletí
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
0,07	[number]	k4	0,07
±	±	k?	±
0,02	[number]	k4	0,02
°	°	k?	°
<g/>
C	C	kA	C
za	za	k7c4	za
desetiletí	desetiletí	k1gNnPc4	desetiletí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
média	médium	k1gNnPc1	médium
často	často	k6eAd1	často
uvádí	uvádět	k5eAaImIp3nP	uvádět
jako	jako	k9	jako
měřítko	měřítko	k1gNnSc4	měřítko
globálního	globální	k2eAgNnSc2d1	globální
oteplování	oteplování	k1gNnSc2	oteplování
nárůst	nárůst	k1gInSc1	nárůst
průměrné	průměrný	k2eAgFnSc2d1	průměrná
atmosférické	atmosférický	k2eAgFnSc2d1	atmosférická
teploty	teplota	k1gFnSc2	teplota
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
dodatečné	dodatečný	k2eAgFnSc2d1	dodatečná
energie	energie	k1gFnSc2	energie
uložené	uložený	k2eAgFnSc2d1	uložená
v	v	k7c6	v
klimatickém	klimatický	k2eAgInSc6d1	klimatický
systému	systém	k1gInSc6	systém
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
se	se	k3xPyFc4	se
nahromadila	nahromadit	k5eAaPmAgFnS	nahromadit
v	v	k7c6	v
oceánech	oceán	k1gInPc6	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
zbytek	zbytek	k1gInSc1	zbytek
způsobil	způsobit	k5eAaPmAgInS	způsobit
tání	tání	k1gNnSc4	tání
ledu	led	k1gInSc2	led
a	a	k8xC	a
zvýšení	zvýšení	k1gNnSc4	zvýšení
teploty	teplota	k1gFnSc2	teplota
kontinentů	kontinent	k1gInPc2	kontinent
a	a	k8xC	a
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
<g/>
Nejzávažnějším	závažný	k2eAgInSc7d3	nejzávažnější
projevem	projev	k1gInSc7	projev
globálního	globální	k2eAgNnSc2d1	globální
oteplování	oteplování	k1gNnSc2	oteplování
je	být	k5eAaImIp3nS	být
zvyšování	zvyšování	k1gNnSc4	zvyšování
teploty	teplota	k1gFnSc2	teplota
oceánů	oceán	k1gInPc2	oceán
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
nich	on	k3xPp3gFnPc6	on
se	se	k3xPyFc4	se
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
cca	cca	kA	cca
93	[number]	k4	93
%	%	kIx~	%
nárůstu	nárůst	k1gInSc2	nárůst
tepelné	tepelný	k2eAgFnSc2d1	tepelná
energie	energie	k1gFnSc2	energie
klimatického	klimatický	k2eAgInSc2d1	klimatický
systému	systém	k1gInSc2	systém
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
vzniká	vznikat	k5eAaImIp3nS	vznikat
díky	díky	k7c3	díky
antropogenním	antropogenní	k2eAgInPc3d1	antropogenní
skleníkovým	skleníkový	k2eAgInPc3d1	skleníkový
plynům	plyn	k1gInPc3	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
teploty	teplota	k1gFnPc1	teplota
vzduchu	vzduch	k1gInSc2	vzduch
rostou	růst	k5eAaImIp3nP	růst
čím	čí	k3xOyQgNnSc7	čí
dál	daleko	k6eAd2	daleko
rychleji	rychle	k6eAd2	rychle
<g/>
,	,	kIx,	,
teplota	teplota	k1gFnSc1	teplota
povrchových	povrchový	k2eAgFnPc2d1	povrchová
i	i	k8xC	i
hlubších	hluboký	k2eAgFnPc2d2	hlubší
vrstev	vrstva	k1gFnPc2	vrstva
oceánu	oceán	k1gInSc2	oceán
roste	růst	k5eAaImIp3nS	růst
konstantním	konstantní	k2eAgNnSc7d1	konstantní
tempem	tempo	k1gNnSc7	tempo
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
horní	horní	k2eAgFnPc1d1	horní
vrstvy	vrstva	k1gFnPc1	vrstva
oceánu	oceán	k1gInSc2	oceán
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
700	[number]	k4	700
m	m	kA	m
<g/>
)	)	kIx)	)
ohřály	ohřát	k5eAaPmAgFnP	ohřát
v	v	k7c6	v
období	období	k1gNnSc6	období
1971	[number]	k4	1971
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
ohřevu	ohřev	k1gInSc3	ohřev
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1870	[number]	k4	1870
a	a	k8xC	a
1971	[number]	k4	1971
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
globálním	globální	k2eAgNnSc6d1	globální
měřítku	měřítko	k1gNnSc6	měřítko
se	se	k3xPyFc4	se
nejvíce	nejvíce	k6eAd1	nejvíce
ohřívaly	ohřívat	k5eAaImAgFnP	ohřívat
povrchové	povrchový	k2eAgFnPc1d1	povrchová
vody	voda	k1gFnPc1	voda
(	(	kIx(	(
<g/>
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
75	[number]	k4	75
m	m	kA	m
<g/>
)	)	kIx)	)
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
rychlostí	rychlost	k1gFnSc7	rychlost
0,11	[number]	k4	0,11
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
0,09	[number]	k4	0,09
až	až	k9	až
0,14	[number]	k4	0,14
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
za	za	k7c4	za
dekádu	dekáda	k1gFnSc4	dekáda
<g/>
.	.	kIx.	.
<g/>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
spodní	spodní	k2eAgFnSc2d1	spodní
troposféry	troposféra	k1gFnSc2	troposféra
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
satelitních	satelitní	k2eAgFnPc2d1	satelitní
měření	měření	k1gNnPc2	měření
teploty	teplota	k1gFnSc2	teplota
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
mezi	mezi	k7c4	mezi
0,13	[number]	k4	0,13
a	a	k8xC	a
0,22	[number]	k4	0,22
°	°	k?	°
<g/>
C	C	kA	C
za	za	k7c4	za
každé	každý	k3xTgNnSc4	každý
desetiletí	desetiletí	k1gNnSc4	desetiletí
<g/>
.	.	kIx.	.
</s>
<s>
Klimatické	klimatický	k2eAgInPc1d1	klimatický
proxy	prox	k1gInPc1	prox
data	datum	k1gNnSc2	datum
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
teplota	teplota	k1gFnSc1	teplota
byla	být	k5eAaImAgFnS	být
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1850	[number]	k4	1850
během	během	k7c2	během
jednoho	jeden	k4xCgMnSc2	jeden
nebo	nebo	k8xC	nebo
dvou	dva	k4xCgInPc2	dva
tisíc	tisíc	k4xCgInSc4	tisíc
let	léto	k1gNnPc2	léto
poměrně	poměrně	k6eAd1	poměrně
stabilní	stabilní	k2eAgFnSc1d1	stabilní
s	s	k7c7	s
měnícími	měnící	k2eAgFnPc7d1	měnící
se	se	k3xPyFc4	se
regionálními	regionální	k2eAgInPc7d1	regionální
výkyvy	výkyv	k1gInPc7	výkyv
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
bylo	být	k5eAaImAgNnS	být
středověké	středověký	k2eAgNnSc1d1	středověké
klimatické	klimatický	k2eAgNnSc1d1	klimatické
optimum	optimum	k1gNnSc1	optimum
a	a	k8xC	a
malá	malý	k2eAgFnSc1d1	malá
doba	doba	k1gFnSc1	doba
ledová	ledový	k2eAgFnSc1d1	ledová
<g/>
.	.	kIx.	.
<g/>
Oteplení	oteplení	k1gNnSc1	oteplení
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
na	na	k7c6	na
instrumentálních	instrumentální	k2eAgInPc6d1	instrumentální
záznamech	záznam	k1gInPc6	záznam
teplot	teplota	k1gFnPc2	teplota
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
celou	celý	k2eAgFnSc7d1	celá
řadou	řada	k1gFnSc7	řada
pozorování	pozorování	k1gNnPc2	pozorování
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
dokumentuje	dokumentovat	k5eAaBmIp3nS	dokumentovat
mnoho	mnoho	k4c1	mnoho
nezávislých	závislý	k2eNgFnPc2d1	nezávislá
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Indikátory	indikátor	k1gInPc1	indikátor
oteplení	oteplení	k1gNnSc2	oteplení
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
zvýšení	zvýšení	k1gNnSc4	zvýšení
hladiny	hladina	k1gFnSc2	hladina
moří	moře	k1gNnPc2	moře
<g/>
,	,	kIx,	,
rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
tání	tání	k1gNnSc1	tání
sněhu	sníh	k1gInSc2	sníh
a	a	k8xC	a
pozemního	pozemní	k2eAgInSc2d1	pozemní
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
navýšení	navýšení	k1gNnSc2	navýšení
akumulované	akumulovaný	k2eAgFnSc2d1	akumulovaná
tepelné	tepelný	k2eAgFnSc2d1	tepelná
energie	energie	k1gFnSc2	energie
v	v	k7c6	v
oceánech	oceán	k1gInPc6	oceán
<g/>
,	,	kIx,	,
zvýšení	zvýšení	k1gNnSc1	zvýšení
vlhkosti	vlhkost	k1gFnSc2	vlhkost
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
a	a	k8xC	a
dřívější	dřívější	k2eAgNnSc1d1	dřívější
načasování	načasování	k1gNnSc1	načasování
jarních	jarní	k2eAgFnPc2d1	jarní
událostí	událost	k1gFnPc2	událost
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
např.	např.	kA	např.
kvetení	kvetení	k1gNnSc1	kvetení
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobnost	pravděpodobnost	k1gFnSc1	pravděpodobnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
tyto	tento	k3xDgFnPc1	tento
změny	změna	k1gFnPc1	změna
mohly	moct	k5eAaImAgFnP	moct
vzniknout	vzniknout	k5eAaPmF	vzniknout
nezávisle	závisle	k6eNd1	závisle
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
nulová	nulový	k2eAgFnSc1d1	nulová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Regionální	regionální	k2eAgInPc1d1	regionální
trendy	trend	k1gInPc1	trend
a	a	k8xC	a
krátkodobé	krátkodobý	k2eAgInPc1d1	krátkodobý
výkyvy	výkyv	k1gInPc1	výkyv
===	===	k?	===
</s>
</p>
<p>
<s>
Globální	globální	k2eAgNnSc1d1	globální
oteplování	oteplování	k1gNnSc1	oteplování
je	být	k5eAaImIp3nS	být
definováno	definovat	k5eAaBmNgNnS	definovat
celosvětovými	celosvětový	k2eAgFnPc7d1	celosvětová
průměrnými	průměrný	k2eAgFnPc7d1	průměrná
teplotami	teplota	k1gFnPc7	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Trendy	trend	k1gInPc1	trend
ale	ale	k8xC	ale
nejsou	být	k5eNaImIp3nP	být
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
stejné	stejný	k2eAgNnSc1d1	stejné
<g/>
:	:	kIx,	:
účinky	účinek	k1gInPc1	účinek
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
lišit	lišit	k5eAaImF	lišit
podle	podle	k7c2	podle
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
vzrostly	vzrůst	k5eAaPmAgFnP	vzrůst
celosvětové	celosvětový	k2eAgFnPc1d1	celosvětová
průměrné	průměrný	k2eAgFnPc1d1	průměrná
teploty	teplota	k1gFnPc1	teplota
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
přibližně	přibližně	k6eAd1	přibližně
dvakrát	dvakrát	k6eAd1	dvakrát
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
průměrné	průměrný	k2eAgFnPc1d1	průměrná
průměrné	průměrný	k2eAgFnPc1d1	průměrná
teploty	teplota	k1gFnPc1	teplota
oceánu	oceán	k1gInSc2	oceán
(	(	kIx(	(
<g/>
0,25	[number]	k4	0,25
°	°	k?	°
<g/>
C	C	kA	C
za	za	k7c2	za
desetiletí	desetiletí	k1gNnPc2	desetiletí
oproti	oproti	k7c3	oproti
0,13	[number]	k4	0,13
°	°	k?	°
<g/>
C	C	kA	C
za	za	k7c4	za
desetiletí	desetiletí	k1gNnPc4	desetiletí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Teploty	teplota	k1gFnPc1	teplota
oceánu	oceán	k1gInSc2	oceán
se	se	k3xPyFc4	se
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
pomaleji	pomale	k6eAd2	pomale
než	než	k8xS	než
teploty	teplota	k1gFnPc1	teplota
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
oceány	oceán	k1gInPc1	oceán
mají	mít	k5eAaImIp3nP	mít
daleko	daleko	k6eAd1	daleko
větší	veliký	k2eAgFnSc4d2	veliký
tepelnou	tepelný	k2eAgFnSc4d1	tepelná
kapacitu	kapacita	k1gFnSc4	kapacita
(	(	kIx(	(
<g/>
díky	díky	k7c3	díky
mořským	mořský	k2eAgInPc3d1	mořský
proudům	proud	k1gInPc3	proud
se	se	k3xPyFc4	se
oteplují	oteplovat	k5eAaImIp3nP	oteplovat
i	i	k9	i
hlubší	hluboký	k2eAgFnPc4d2	hlubší
části	část	k1gFnPc4	část
oceánů	oceán	k1gInPc2	oceán
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
se	se	k3xPyFc4	se
daleko	daleko	k6eAd1	daleko
více	hodně	k6eAd2	hodně
ochlazují	ochlazovat	k5eAaImIp3nP	ochlazovat
odpařováním	odpařování	k1gNnSc7	odpařování
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
industrializace	industrializace	k1gFnSc2	industrializace
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
kvůli	kvůli	k7c3	kvůli
tání	tání	k1gNnSc3	tání
mořského	mořský	k2eAgInSc2d1	mořský
ledu	led	k1gInSc2	led
a	a	k8xC	a
sněhu	sníh	k1gInSc2	sníh
a	a	k8xC	a
vyššímu	vysoký	k2eAgInSc3d2	vyšší
podílu	podíl	k1gInSc3	podíl
pevniny	pevnina	k1gFnSc2	pevnina
na	na	k7c6	na
severu	sever	k1gInSc6	sever
teplotní	teplotní	k2eAgInSc4d1	teplotní
rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
severní	severní	k2eAgFnSc7d1	severní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc7d1	jižní
polokoulí	polokoule	k1gFnSc7	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
100	[number]	k4	100
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
průměrné	průměrný	k2eAgFnPc1d1	průměrná
teploty	teplota	k1gFnPc1	teplota
v	v	k7c6	v
Arktidě	Arktida	k1gFnSc6	Arktida
zvýšily	zvýšit	k5eAaPmAgInP	zvýšit
téměř	téměř	k6eAd1	téměř
o	o	k7c4	o
dvojnásobek	dvojnásobek	k1gInSc4	dvojnásobek
proti	proti	k7c3	proti
zbytku	zbytek	k1gInSc3	zbytek
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
je	být	k5eAaImIp3nS	být
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xC	jako
polární	polární	k2eAgNnSc1d1	polární
zesílení	zesílení	k1gNnSc1	zesílení
<g/>
.	.	kIx.	.
<g/>
Ačkoliv	ačkoliv	k8xS	ačkoliv
na	na	k7c6	na
severu	sever	k1gInSc6	sever
vzniká	vznikat	k5eAaImIp3nS	vznikat
více	hodně	k6eAd2	hodně
antropogenních	antropogenní	k2eAgInPc2d1	antropogenní
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
než	než	k8xS	než
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
,	,	kIx,	,
nepřispívá	přispívat	k5eNaImIp3nS	přispívat
to	ten	k3xDgNnSc1	ten
k	k	k7c3	k
rozdílu	rozdíl	k1gInSc3	rozdíl
v	v	k7c6	v
oteplování	oteplování	k1gNnSc6	oteplování
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
hlavní	hlavní	k2eAgInPc1d1	hlavní
skleníkové	skleníkový	k2eAgInPc1d1	skleníkový
plyny	plyn	k1gInPc1	plyn
přetrvávají	přetrvávat	k5eAaImIp3nP	přetrvávat
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
tak	tak	k6eAd1	tak
dlouho	dlouho	k6eAd1	dlouho
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
rozptýleny	rozptýlit	k5eAaPmNgFnP	rozptýlit
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
každé	každý	k3xTgFnSc2	každý
polokoule	polokoule	k1gFnSc2	polokoule
<g />
.	.	kIx.	.
</s>
<s>
i	i	k9	i
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
polokoulemi	polokoule	k1gFnPc7	polokoule
<g/>
.	.	kIx.	.
<g/>
Existují	existovat	k5eAaImIp3nP	existovat
různé	různý	k2eAgInPc1d1	různý
faktory	faktor	k1gInPc1	faktor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
klimatickou	klimatický	k2eAgFnSc4d1	klimatická
změnu	změna	k1gFnSc4	změna
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
klimatický	klimatický	k2eAgInSc1d1	klimatický
systém	systém	k1gInSc1	systém
má	mít	k5eAaImIp3nS	mít
značnou	značný	k2eAgFnSc4d1	značná
tepelnou	tepelný	k2eAgFnSc4d1	tepelná
setrvačnost	setrvačnost	k1gFnSc4	setrvačnost
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
trvat	trvat	k5eAaImF	trvat
staletí	staletí	k1gNnPc4	staletí
–	–	k?	–
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
déle	dlouho	k6eAd2	dlouho
–	–	k?	–
než	než	k8xS	než
se	se	k3xPyFc4	se
klima	klima	k1gNnSc1	klima
vrátí	vrátit	k5eAaPmIp3nS	vrátit
do	do	k7c2	do
rovnováhy	rovnováha	k1gFnSc2	rovnováha
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nP	by
byly	být	k5eAaImAgInP	být
skleníkové	skleníkový	k2eAgInPc1d1	skleníkový
plyny	plyn	k1gInPc1	plyn
stabilizovány	stabilizován	k2eAgInPc1d1	stabilizován
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
povrchové	povrchový	k2eAgFnSc2d1	povrchová
teploty	teplota	k1gFnSc2	teplota
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
klimatologů	klimatolog	k1gMnPc2	klimatolog
přesto	přesto	k8xC	přesto
zvýšily	zvýšit	k5eAaPmAgFnP	zvýšit
o	o	k7c4	o
0,5	[number]	k4	0,5
°	°	k?	°
<g/>
C.	C.	kA	C.
A	a	k9	a
pokud	pokud	k8xS	pokud
by	by	kYmCp3nP	by
byly	být	k5eAaImAgInP	být
stabilizovány	stabilizovat	k5eAaBmNgInP	stabilizovat
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
povrchové	povrchový	k2eAgNnSc1d1	povrchové
oteplování	oteplování	k1gNnSc1	oteplování
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
překročit	překročit	k5eAaPmF	překročit
celý	celý	k2eAgInSc4d1	celý
1	[number]	k4	1
°	°	k?	°
<g/>
C.	C.	kA	C.
Je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
část	část	k1gFnSc1	část
současného	současný	k2eAgNnSc2d1	současné
oteplování	oteplování	k1gNnSc2	oteplování
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
způsobována	způsobovat	k5eAaImNgFnS	způsobovat
přírodními	přírodní	k2eAgInPc7d1	přírodní
výkyvy	výkyv	k1gInPc7	výkyv
z	z	k7c2	z
minulosti	minulost	k1gFnSc2	minulost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
dosud	dosud	k6eAd1	dosud
nedosáhly	dosáhnout	k5eNaPmAgFnP	dosáhnout
rovnováhy	rovnováha	k1gFnPc1	rovnováha
v	v	k7c6	v
klimatickém	klimatický	k2eAgInSc6d1	klimatický
systému	systém	k1gInSc6	systém
<g/>
.	.	kIx.	.
<g/>
Globální	globální	k2eAgFnSc1d1	globální
teplota	teplota	k1gFnSc1	teplota
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
krátkodobé	krátkodobý	k2eAgInPc4d1	krátkodobý
výkyvy	výkyv	k1gInPc4	výkyv
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
překrývají	překrývat	k5eAaImIp3nP	překrývat
dlouhodobé	dlouhodobý	k2eAgInPc4d1	dlouhodobý
trendy	trend	k1gInPc4	trend
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
je	on	k3xPp3gFnPc4	on
dočasně	dočasně	k6eAd1	dočasně
potlačit	potlačit	k5eAaPmF	potlačit
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
naopak	naopak	k6eAd1	naopak
zvětšit	zvětšit	k5eAaPmF	zvětšit
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
takovéto	takovýto	k3xDgFnSc2	takovýto
situace	situace	k1gFnSc2	situace
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
relativní	relativní	k2eAgFnSc1d1	relativní
stabilita	stabilita	k1gFnSc1	stabilita
povrchové	povrchový	k2eAgFnSc2d1	povrchová
teploty	teplota	k1gFnSc2	teplota
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
médii	médium	k1gNnPc7	médium
a	a	k8xC	a
některými	některý	k3yIgMnPc7	některý
vědci	vědec	k1gMnPc7	vědec
nazývána	nazývat	k5eAaImNgFnS	nazývat
zastavením	zastavení	k1gNnSc7	zastavení
globálního	globální	k2eAgNnSc2d1	globální
oteplování	oteplování	k1gNnSc2	oteplování
<g/>
.	.	kIx.	.
</s>
<s>
Aktualizace	aktualizace	k1gFnSc1	aktualizace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
zohledňující	zohledňující	k2eAgFnPc1d1	zohledňující
odlišné	odlišný	k2eAgFnPc1d1	odlišná
metody	metoda	k1gFnPc1	metoda
měření	měření	k1gNnSc2	měření
povrchových	povrchový	k2eAgFnPc2d1	povrchová
teplot	teplota	k1gFnPc2	teplota
oceánu	oceán	k1gInSc2	oceán
však	však	k9	však
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
teploty	teplota	k1gFnPc1	teplota
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
stoupaly	stoupat	k5eAaImAgFnP	stoupat
i	i	k8xC	i
v	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nejteplejší	teplý	k2eAgInPc4d3	nejteplejší
roky	rok	k1gInPc4	rok
vs	vs	k?	vs
<g/>
.	.	kIx.	.
celkový	celkový	k2eAgInSc1d1	celkový
trend	trend	k1gInSc1	trend
===	===	k?	===
</s>
</p>
<p>
<s>
Osmnáct	osmnáct	k4xCc1	osmnáct
ze	z	k7c2	z
devatenácti	devatenáct	k4xCc2	devatenáct
nejteplejších	teplý	k2eAgInPc2d3	nejteplejší
roků	rok	k1gInPc2	rok
za	za	k7c2	za
posledních	poslední	k2eAgNnPc2d1	poslední
150	[number]	k4	150
let	léto	k1gNnPc2	léto
bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2000	[number]	k4	2000
<g/>
;	;	kIx,	;
pět	pět	k4xCc1	pět
nejteplejších	teplý	k2eAgNnPc2d3	nejteplejší
let	léto	k1gNnPc2	léto
bylo	být	k5eAaImAgNnS	být
během	během	k7c2	během
posledních	poslední	k2eAgNnPc2d1	poslední
pěti	pět	k4xCc2	pět
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
poslední	poslední	k2eAgInPc4d1	poslední
údaje	údaj	k1gInPc4	údaj
pro	pro	k7c4	pro
rok	rok	k1gInSc4	rok
2018	[number]	k4	2018
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
rekordní	rekordní	k2eAgInPc1d1	rekordní
roky	rok	k1gInPc1	rok
přitahují	přitahovat	k5eAaImIp3nP	přitahovat
značný	značný	k2eAgInSc4d1	značný
zájem	zájem	k1gInSc4	zájem
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
globální	globální	k2eAgNnSc4d1	globální
oteplování	oteplování	k1gNnSc4	oteplování
jsou	být	k5eAaImIp3nP	být
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
roky	rok	k1gInPc1	rok
méně	málo	k6eAd2	málo
významné	významný	k2eAgInPc1d1	významný
než	než	k8xS	než
celkový	celkový	k2eAgInSc1d1	celkový
trend	trend	k1gInSc1	trend
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
klimatologové	klimatolog	k1gMnPc1	klimatolog
kritizovali	kritizovat	k5eAaImAgMnP	kritizovat
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
média	médium	k1gNnPc1	médium
věnují	věnovat	k5eAaPmIp3nP	věnovat
statistikám	statistika	k1gFnPc3	statistika
nejteplejšího	teplý	k2eAgInSc2d3	nejteplejší
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnPc1d1	zvláštní
oscilace	oscilace	k1gFnPc1	oscilace
oceánů	oceán	k1gInPc2	oceán
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
Jižní	jižní	k2eAgFnSc1d1	jižní
oscilace	oscilace	k1gFnSc1	oscilace
El	Ela	k1gFnPc2	Ela
Niñ	Niñ	k1gMnSc1	Niñ
(	(	kIx(	(
<g/>
ENSO	ENSO	kA	ENSO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mohou	moct	k5eAaImIp3nP	moct
způsobit	způsobit	k5eAaPmF	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
teplota	teplota	k1gFnSc1	teplota
daného	daný	k2eAgInSc2d1	daný
roku	rok	k1gInSc2	rok
je	být	k5eAaImIp3nS	být
neobvykle	obvykle	k6eNd1	obvykle
vysoká	vysoký	k2eAgFnSc1d1	vysoká
nebo	nebo	k8xC	nebo
nízká	nízký	k2eAgFnSc1d1	nízká
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
nesouvisejících	související	k2eNgInPc2d1	nesouvisející
s	s	k7c7	s
celkovým	celkový	k2eAgInSc7d1	celkový
vývojem	vývoj	k1gInSc7	vývoj
klimatických	klimatický	k2eAgFnPc2d1	klimatická
změn	změna	k1gFnPc2	změna
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgMnSc1d1	americký
klimatolog	klimatolog	k1gMnSc1	klimatolog
Gavin	Gavin	k1gMnSc1	Gavin
Schmidt	Schmidt	k1gMnSc1	Schmidt
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Dlouhodobé	dlouhodobý	k2eAgInPc1d1	dlouhodobý
trendy	trend	k1gInPc1	trend
nebo	nebo	k8xC	nebo
očekávaná	očekávaný	k2eAgFnSc1d1	očekávaná
sekvence	sekvence	k1gFnSc1	sekvence
záznamů	záznam	k1gInPc2	záznam
jsou	být	k5eAaImIp3nP	být
daleko	daleko	k6eAd1	daleko
důležitější	důležitý	k2eAgNnSc4d2	důležitější
než	než	k8xS	než
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
nějaký	nějaký	k3yIgInSc4	nějaký
rok	rok	k1gInSc4	rok
rekordní	rekordní	k2eAgInSc4d1	rekordní
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
ne	ne	k9	ne
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
==	==	k?	==
Počáteční	počáteční	k2eAgFnPc1d1	počáteční
příčiny	příčina	k1gFnPc1	příčina
teplotních	teplotní	k2eAgFnPc2d1	teplotní
změn	změna	k1gFnPc2	změna
(	(	kIx(	(
<g/>
vnější	vnější	k2eAgFnSc2d1	vnější
síly	síla	k1gFnSc2	síla
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
Klimatický	klimatický	k2eAgInSc1d1	klimatický
systém	systém	k1gInSc1	systém
sám	sám	k3xTgInSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
může	moct	k5eAaImIp3nS	moct
generovat	generovat	k5eAaImF	generovat
náhodné	náhodný	k2eAgFnPc4d1	náhodná
změny	změna	k1gFnPc4	změna
globálních	globální	k2eAgFnPc2d1	globální
teplot	teplota	k1gFnPc2	teplota
po	po	k7c4	po
celá	celý	k2eAgNnPc4d1	celé
léta	léto	k1gNnPc4	léto
až	až	k8xS	až
desetiletí	desetiletí	k1gNnPc4	desetiletí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dlouhodobé	dlouhodobý	k2eAgFnPc1d1	dlouhodobá
změny	změna	k1gFnPc1	změna
vycházejí	vycházet	k5eAaImIp3nP	vycházet
pouze	pouze	k6eAd1	pouze
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
vnějších	vnější	k2eAgFnPc2d1	vnější
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
síly	síla	k1gFnPc1	síla
jsou	být	k5eAaImIp3nP	být
"	"	kIx"	"
<g/>
vnější	vnější	k2eAgFnSc7d1	vnější
<g/>
"	"	kIx"	"
vůči	vůči	k7c3	vůči
klimatickému	klimatický	k2eAgInSc3d1	klimatický
systému	systém	k1gInSc3	systém
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
nutně	nutně	k6eAd1	nutně
vnější	vnější	k2eAgMnSc1d1	vnější
vůči	vůči	k7c3	vůči
Zemi	zem	k1gFnSc3	zem
<g/>
.	.	kIx.	.
</s>
<s>
Příklady	příklad	k1gInPc1	příklad
vnějších	vnější	k2eAgFnPc2d1	vnější
sil	síla	k1gFnPc2	síla
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
změny	změna	k1gFnPc4	změna
složení	složení	k1gNnSc2	složení
atmosféry	atmosféra	k1gFnSc2	atmosféra
(	(	kIx(	(
<g/>
např.	např.	kA	např.
zvýšené	zvýšený	k2eAgFnSc2d1	zvýšená
koncentrace	koncentrace	k1gFnSc2	koncentrace
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sluneční	sluneční	k2eAgNnSc1d1	sluneční
záření	záření	k1gNnSc1	záření
<g/>
,	,	kIx,	,
vulkanické	vulkanický	k2eAgFnPc1d1	vulkanická
erupce	erupce	k1gFnPc1	erupce
a	a	k8xC	a
změny	změna	k1gFnPc1	změna
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
Země	zem	k1gFnSc2	zem
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Skleníkové	skleníkový	k2eAgInPc1d1	skleníkový
plyny	plyn	k1gInPc1	plyn
===	===	k?	===
</s>
</p>
<p>
<s>
Skleníkový	skleníkový	k2eAgInSc1d1	skleníkový
efekt	efekt	k1gInSc1	efekt
je	být	k5eAaImIp3nS	být
proces	proces	k1gInSc4	proces
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yRgInSc6	který
plyny	plyn	k1gInPc1	plyn
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
absorpci	absorpce	k1gFnSc4	absorpce
a	a	k8xC	a
vyzařování	vyzařování	k1gNnSc4	vyzařování
infračerveného	infračervený	k2eAgNnSc2d1	infračervené
záření	záření	k1gNnSc2	záření
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
ohřívání	ohřívání	k1gNnSc4	ohřívání
dolních	dolní	k2eAgFnPc2d1	dolní
vrstev	vrstva	k1gFnPc2	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
a	a	k8xC	a
povrchu	povrch	k1gInSc2	povrch
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
jev	jev	k1gInSc4	jev
prvně	prvně	k?	prvně
popsal	popsat	k5eAaPmAgMnS	popsat
Joseph	Joseph	k1gMnSc1	Joseph
Fourier	Fourier	k1gMnSc1	Fourier
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1824	[number]	k4	1824
<g/>
,	,	kIx,	,
podrobněji	podrobně	k6eAd2	podrobně
ho	on	k3xPp3gMnSc4	on
popsal	popsat	k5eAaPmAgMnS	popsat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1860	[number]	k4	1860
John	John	k1gMnSc1	John
Tyndall	Tyndall	k1gMnSc1	Tyndall
<g/>
.	.	kIx.	.
</s>
<s>
Kvantitativně	kvantitativně	k6eAd1	kvantitativně
ho	on	k3xPp3gMnSc4	on
prvně	prvně	k?	prvně
vyčíslil	vyčíslit	k5eAaPmAgInS	vyčíslit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1896	[number]	k4	1896
Svante	Svant	k1gInSc5	Svant
Arrhenius	Arrhenius	k1gInSc4	Arrhenius
<g/>
,	,	kIx,	,
když	když	k8xS	když
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
práci	práce	k1gFnSc6	práce
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
zdvojnásobení	zdvojnásobení	k1gNnSc6	zdvojnásobení
koncentrací	koncentrace	k1gFnPc2	koncentrace
CO2	CO2	k1gMnPc2	CO2
narostou	narůst	k5eAaPmIp3nP	narůst
povrchové	povrchový	k2eAgFnPc1d1	povrchová
teploty	teplota	k1gFnPc1	teplota
o	o	k7c4	o
4	[number]	k4	4
°	°	k?	°
<g/>
C.	C.	kA	C.
Dále	daleko	k6eAd2	daleko
problém	problém	k1gInSc1	problém
rozvinul	rozvinout	k5eAaPmAgInS	rozvinout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1930	[number]	k4	1930
<g/>
–	–	k?	–
<g/>
1960	[number]	k4	1960
Gue	Gue	k1gFnPc2	Gue
Steward	Steward	k1gMnSc1	Steward
Callendar	Callendar	k1gMnSc1	Callendar
<g/>
.	.	kIx.	.
<g/>
Přirozený	přirozený	k2eAgInSc1d1	přirozený
skleníkový	skleníkový	k2eAgInSc1d1	skleníkový
efekt	efekt	k1gInSc1	efekt
je	být	k5eAaImIp3nS	být
podmínkou	podmínka	k1gFnSc7	podmínka
života	život	k1gInSc2	život
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ho	on	k3xPp3gMnSc4	on
známe	znát	k5eAaImIp1nP	znát
<g/>
.	.	kIx.	.
</s>
<s>
Bez	bez	k7c2	bez
zemské	zemský	k2eAgFnSc2d1	zemská
atmosféry	atmosféra	k1gFnSc2	atmosféra
by	by	k9	by
teploty	teplota	k1gFnPc1	teplota
prakticky	prakticky	k6eAd1	prakticky
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
povrchu	povrch	k1gInSc6	povrch
Země	zem	k1gFnSc2	zem
byly	být	k5eAaImAgFnP	být
pod	pod	k7c7	pod
bodem	bod	k1gInSc7	bod
mrazu	mráz	k1gInSc2	mráz
<g/>
.	.	kIx.	.
</s>
<s>
Přirozeně	přirozeně	k6eAd1	přirozeně
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgInPc1d1	vyskytující
skleníkové	skleníkový	k2eAgInPc1d1	skleníkový
plyny	plyn	k1gInPc1	plyn
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
nárůst	nárůst	k1gInSc4	nárůst
teplot	teplota	k1gFnPc2	teplota
o	o	k7c4	o
cca	cca	kA	cca
33	[number]	k4	33
°	°	k?	°
<g/>
C.	C.	kA	C.
Hlavními	hlavní	k2eAgInPc7d1	hlavní
skleníkovými	skleníkový	k2eAgInPc7d1	skleníkový
plyny	plyn	k1gInPc7	plyn
jsou	být	k5eAaImIp3nP	být
vodní	vodní	k2eAgFnSc1d1	vodní
pára	pára	k1gFnSc1	pára
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
36	[number]	k4	36
<g/>
–	–	k?	–
<g/>
70	[number]	k4	70
%	%	kIx~	%
skleníkového	skleníkový	k2eAgInSc2d1	skleníkový
jevu	jev	k1gInSc2	jev
<g/>
,	,	kIx,	,
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
může	moct	k5eAaImIp3nS	moct
za	za	k7c4	za
9	[number]	k4	9
<g/>
–	–	k?	–
<g/>
26	[number]	k4	26
%	%	kIx~	%
skleníkového	skleníkový	k2eAgInSc2d1	skleníkový
efektu	efekt	k1gInSc2	efekt
<g/>
,	,	kIx,	,
methan	methan	k1gInSc1	methan
může	moct	k5eAaImIp3nS	moct
za	za	k7c4	za
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
%	%	kIx~	%
a	a	k8xC	a
ozon	ozon	k1gInSc1	ozon
<g/>
,	,	kIx,	,
kterému	který	k3yRgInSc3	který
je	být	k5eAaImIp3nS	být
přičítáno	přičítán	k2eAgNnSc4d1	přičítáno
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
%	%	kIx~	%
skleníkového	skleníkový	k2eAgInSc2d1	skleníkový
efektu	efekt	k1gInSc2	efekt
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
Mezivládního	mezivládní	k2eAgInSc2d1	mezivládní
panelu	panel	k1gInSc2	panel
pro	pro	k7c4	pro
změnu	změna	k1gFnSc4	změna
klimatu	klima	k1gNnSc2	klima
jsou	být	k5eAaImIp3nP	být
lidské	lidský	k2eAgInPc4d1	lidský
vlivy	vliv	k1gInPc4	vliv
hlavním	hlavní	k2eAgInSc7d1	hlavní
faktorem	faktor	k1gInSc7	faktor
tzv.	tzv.	kA	tzv.
radiačního	radiační	k2eAgNnSc2d1	radiační
působení	působení	k1gNnSc2	působení
na	na	k7c4	na
klima	klima	k1gNnSc4	klima
<g/>
.	.	kIx.	.
</s>
<s>
Radiační	radiační	k2eAgNnSc1d1	radiační
působení	působení	k1gNnSc1	působení
je	být	k5eAaImIp3nS	být
změna	změna	k1gFnSc1	změna
radiační	radiační	k2eAgFnSc2d1	radiační
bilance	bilance	k1gFnSc2	bilance
v	v	k7c6	v
tropopauze	tropopauza	k1gFnSc6	tropopauza
vlivem	vlivem	k7c2	vlivem
dodatečného	dodatečný	k2eAgNnSc2d1	dodatečné
působení	působení	k1gNnSc2	působení
na	na	k7c4	na
klimatický	klimatický	k2eAgInSc4d1	klimatický
systém	systém	k1gInSc4	systém
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
revoluce	revoluce	k1gFnSc2	revoluce
lidská	lidský	k2eAgFnSc1d1	lidská
činnost	činnost	k1gFnSc1	činnost
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
množství	množství	k1gNnSc4	množství
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
zvýšenému	zvýšený	k2eAgNnSc3d1	zvýšené
radiačnímu	radiační	k2eAgNnSc3d1	radiační
působení	působení	k1gNnSc3	působení
způsobené	způsobený	k2eAgFnSc2d1	způsobená
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
<g/>
,	,	kIx,	,
methanem	methan	k1gInSc7	methan
<g/>
,	,	kIx,	,
troposférickým	troposférický	k2eAgInSc7d1	troposférický
ozonem	ozon	k1gInSc7	ozon
<g/>
,	,	kIx,	,
freony	freon	k1gInPc7	freon
a	a	k8xC	a
oxidem	oxid	k1gInSc7	oxid
dusným	dusný	k2eAgInSc7d1	dusný
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
zvýšení	zvýšení	k1gNnSc1	zvýšení
koncentrací	koncentrace	k1gFnPc2	koncentrace
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
zvýšení	zvýšení	k1gNnSc3	zvýšení
teploty	teplota	k1gFnSc2	teplota
ovzduší	ovzduší	k1gNnSc2	ovzduší
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
důsledkem	důsledek	k1gInSc7	důsledek
Planckova	Planckov	k1gInSc2	Planckov
a	a	k8xC	a
Stefan	Stefan	k1gMnSc1	Stefan
<g/>
–	–	k?	–
<g/>
Boltzmannova	Boltzmannův	k2eAgInSc2d1	Boltzmannův
zákona	zákon	k1gInSc2	zákon
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
absorpčních	absorpční	k2eAgFnPc2d1	absorpční
spekter	spektrum	k1gNnPc2	spektrum
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
v	v	k7c6	v
infračervené	infračervený	k2eAgFnSc6d1	infračervená
oblasti	oblast	k1gFnSc6	oblast
(	(	kIx(	(
<g/>
proměřených	proměřený	k2eAgInPc2d1	proměřený
laboratorně	laboratorně	k6eAd1	laboratorně
<g/>
)	)	kIx)	)
a	a	k8xC	a
zákona	zákon	k1gInSc2	zákon
zachování	zachování	k1gNnSc2	zachování
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Koncentrace	koncentrace	k1gFnSc1	koncentrace
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
se	se	k3xPyFc4	se
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
oproti	oproti	k7c3	oproti
období	období	k1gNnSc3	období
před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
revoluce	revoluce	k1gFnSc2	revoluce
z	z	k7c2	z
tehdejších	tehdejší	k2eAgInPc2d1	tehdejší
280	[number]	k4	280
ppm	ppm	k?	ppm
na	na	k7c6	na
dnešních	dnešní	k2eAgFnPc6d1	dnešní
více	hodně	k6eAd2	hodně
než	než	k8xS	než
400	[number]	k4	400
ppm	ppm	k?	ppm
(	(	kIx(	(
<g/>
leden	leden	k1gInSc1	leden
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
v	v	k7c4	v
předcházejících	předcházející	k2eAgNnPc2d1	předcházející
8	[number]	k4	8
000	[number]	k4	000
letech	let	k1gInPc6	let
(	(	kIx(	(
<g/>
před	před	k7c7	před
rokem	rok	k1gInSc7	rok
1750	[number]	k4	1750
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
hladina	hladina	k1gFnSc1	hladina
CO2	CO2	k1gFnSc1	CO2
relativně	relativně	k6eAd1	relativně
stabilní	stabilní	k2eAgFnSc1d1	stabilní
<g/>
,	,	kIx,	,
dá	dát	k5eAaPmIp3nS	dát
se	se	k3xPyFc4	se
předpokládat	předpokládat	k5eAaImF	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
nebýt	být	k5eNaImF	být
lidského	lidský	k2eAgInSc2d1	lidský
zásahu	zásah	k1gInSc2	zásah
<g/>
,	,	kIx,	,
udržela	udržet	k5eAaPmAgFnS	udržet
stabilní	stabilní	k2eAgFnSc1d1	stabilní
i	i	k9	i
nadále	nadále	k6eAd1	nadále
<g/>
.	.	kIx.	.
</s>
<s>
Nárůst	nárůst	k1gInSc4	nárůst
množství	množství	k1gNnSc2	množství
atmosférického	atmosférický	k2eAgMnSc2d1	atmosférický
CO2	CO2	k1gMnSc2	CO2
je	být	k5eAaImIp3nS	být
výsledkem	výsledek	k1gInSc7	výsledek
lidských	lidský	k2eAgFnPc2d1	lidská
aktivit	aktivita	k1gFnPc2	aktivita
<g/>
:	:	kIx,	:
hlavně	hlavně	k9	hlavně
spalování	spalování	k1gNnSc1	spalování
fosilních	fosilní	k2eAgNnPc2d1	fosilní
paliv	palivo	k1gNnPc2	palivo
<g/>
,	,	kIx,	,
odlesňování	odlesňování	k1gNnSc2	odlesňování
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
změn	změna	k1gFnPc2	změna
ve	v	k7c6	v
využívání	využívání	k1gNnSc6	využívání
půdy	půda	k1gFnSc2	půda
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
pálení	pálení	k1gNnSc4	pálení
biomasy	biomasa	k1gFnSc2	biomasa
<g/>
,	,	kIx,	,
rostlinná	rostlinný	k2eAgFnSc1d1	rostlinná
výroba	výroba	k1gFnSc1	výroba
a	a	k8xC	a
přeměna	přeměna	k1gFnSc1	přeměna
pastvin	pastvina	k1gFnPc2	pastvina
na	na	k7c4	na
ornou	orný	k2eAgFnSc4d1	orná
půdu	půda	k1gFnSc4	půda
<g/>
.	.	kIx.	.
</s>
<s>
Průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
revoluce	revoluce	k1gFnSc1	revoluce
narušila	narušit	k5eAaPmAgFnS	narušit
přirozený	přirozený	k2eAgInSc4d1	přirozený
koloběh	koloběh	k1gInSc4	koloběh
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
do	do	k7c2	do
ovzduší	ovzduší	k1gNnSc2	ovzduší
začala	začít	k5eAaPmAgFnS	začít
dodávat	dodávat	k5eAaImF	dodávat
velká	velký	k2eAgNnPc4d1	velké
množství	množství	k1gNnSc4	množství
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Uhlík	Uhlík	k1gMnSc1	Uhlík
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
před	před	k7c7	před
mnoha	mnoho	k4c7	mnoho
miliony	milion	k4xCgInPc7	milion
let	léto	k1gNnPc2	léto
uložen	uložit	k5eAaPmNgInS	uložit
do	do	k7c2	do
rezervoárů	rezervoár	k1gInPc2	rezervoár
fosilního	fosilní	k2eAgInSc2d1	fosilní
uhlíku	uhlík	k1gInSc2	uhlík
pod	pod	k7c4	pod
zem	zem	k1gFnSc4	zem
(	(	kIx(	(
<g/>
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
mimo	mimo	k7c4	mimo
uhlíkový	uhlíkový	k2eAgInSc4d1	uhlíkový
cyklus	cyklus	k1gInSc4	cyklus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
oběhu	oběh	k1gInSc2	oběh
v	v	k7c6	v
emisích	emise	k1gFnPc6	emise
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
antropogenních	antropogenní	k2eAgFnPc2d1	antropogenní
emisí	emise	k1gFnPc2	emise
CO2	CO2	k1gFnSc2	CO2
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1750	[number]	k4	1750
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
spalování	spalování	k1gNnSc2	spalování
fosilních	fosilní	k2eAgNnPc2d1	fosilní
paliv	palivo	k1gNnPc2	palivo
a	a	k8xC	a
zhruba	zhruba	k6eAd1	zhruba
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
ze	z	k7c2	z
změn	změna	k1gFnPc2	změna
ve	v	k7c6	v
využití	využití	k1gNnSc6	využití
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
45	[number]	k4	45
%	%	kIx~	%
tohoto	tento	k3xDgMnSc2	tento
dodatečného	dodatečný	k2eAgMnSc2d1	dodatečný
CO2	CO2	k1gMnSc2	CO2
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
zbylých	zbylý	k2eAgNnPc2d1	zbylé
55	[number]	k4	55
%	%	kIx~	%
pohltily	pohltit	k5eAaPmAgInP	pohltit
oceány	oceán	k1gInPc1	oceán
a	a	k8xC	a
pozemská	pozemský	k2eAgFnSc1d1	pozemská
biosféra	biosféra	k1gFnSc1	biosféra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1750	[number]	k4	1750
vzrostly	vzrůst	k5eAaPmAgFnP	vzrůst
koncentrace	koncentrace	k1gFnPc1	koncentrace
i	i	k8xC	i
dalších	další	k2eAgInPc2d1	další
přírodních	přírodní	k2eAgInPc2d1	přírodní
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
<g/>
:	:	kIx,	:
methanu	methan	k1gInSc2	methan
ze	z	k7c2	z
700	[number]	k4	700
na	na	k7c4	na
1800	[number]	k4	1800
ppb	ppb	k?	ppb
<g/>
,	,	kIx,	,
oxidu	oxid	k1gInSc2	oxid
dusného	dusný	k2eAgNnSc2d1	dusné
z	z	k7c2	z
270	[number]	k4	270
na	na	k7c4	na
320	[number]	k4	320
ppb	ppb	k?	ppb
a	a	k8xC	a
troposférického	troposférický	k2eAgInSc2d1	troposférický
ozonu	ozon	k1gInSc2	ozon
z	z	k7c2	z
25	[number]	k4	25
na	na	k7c4	na
34	[number]	k4	34
ppb	ppb	k?	ppb
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
ovzduší	ovzduší	k1gNnSc2	ovzduší
se	se	k3xPyFc4	se
dostaly	dostat	k5eAaPmAgFnP	dostat
i	i	k9	i
umělé	umělý	k2eAgFnPc1d1	umělá
látky	látka	k1gFnPc1	látka
–	–	k?	–
freony	freon	k1gInPc7	freon
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc1	jejich
koncentrace	koncentrace	k1gFnPc1	koncentrace
jsou	být	k5eAaImIp3nP	být
sice	sice	k8xC	sice
ještě	ještě	k9	ještě
o	o	k7c4	o
několik	několik	k4yIc4	několik
řádů	řád	k1gInPc2	řád
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
však	však	k9	však
silný	silný	k2eAgInSc4d1	silný
relativní	relativní	k2eAgInSc4d1	relativní
účinek	účinek	k1gInSc4	účinek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
páté	pátý	k4xOgFnSc2	pátý
hodnotící	hodnotící	k2eAgFnSc2d1	hodnotící
zprávy	zpráva	k1gFnSc2	zpráva
IPCC	IPCC	kA	IPCC
je	být	k5eAaImIp3nS	být
celkové	celkový	k2eAgNnSc1d1	celkové
antropogenní	antropogenní	k2eAgNnSc1d1	antropogenní
radiační	radiační	k2eAgNnSc1d1	radiační
působení	působení	k1gNnSc1	působení
(	(	kIx(	(
<g/>
RP	RP	kA	RP
<g/>
)	)	kIx)	)
za	za	k7c4	za
období	období	k1gNnSc4	období
1750	[number]	k4	1750
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
2,29	[number]	k4	2,29
(	(	kIx(	(
<g/>
1,33	[number]	k4	1,33
až	až	k9	až
3,33	[number]	k4	3,33
<g/>
)	)	kIx)	)
Wm	Wm	k1gFnSc1	Wm
<g/>
−	−	k?	−
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
pak	pak	k6eAd1	pak
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
radiační	radiační	k2eAgNnSc1d1	radiační
působení	působení	k1gNnSc1	působení
emisí	emise	k1gFnPc2	emise
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
je	být	k5eAaImIp3nS	být
3,00	[number]	k4	3,00
(	(	kIx(	(
<g/>
2,22	[number]	k4	2,22
až	až	k9	až
3,78	[number]	k4	3,78
<g/>
)	)	kIx)	)
Wm	Wm	k1gFnSc1	Wm
<g/>
−	−	k?	−
<g/>
2	[number]	k4	2
<g/>
sám	sám	k3xTgMnSc1	sám
CO2	CO2	k1gMnSc4	CO2
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
radiační	radiační	k2eAgNnSc1d1	radiační
působení	působení	k1gNnSc1	působení
1,68	[number]	k4	1,68
(	(	kIx(	(
<g/>
1,33	[number]	k4	1,33
až	až	k9	až
2,03	[number]	k4	2,03
<g/>
)	)	kIx)	)
Wm	Wm	k1gFnSc1	Wm
<g/>
−	−	k?	−
<g/>
2	[number]	k4	2
</s>
</p>
<p>
<s>
emise	emise	k1gFnPc1	emise
methanu	methan	k1gInSc2	methan
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
radiační	radiační	k2eAgNnSc4d1	radiační
působení	působení	k1gNnSc4	působení
0,97	[number]	k4	0,97
(	(	kIx(	(
<g/>
0,74	[number]	k4	0,74
až	až	k9	až
1,20	[number]	k4	1,20
<g/>
)	)	kIx)	)
Wm	Wm	k1gFnSc1	Wm
<g/>
−	−	k?	−
<g/>
2	[number]	k4	2
</s>
</p>
<p>
<s>
freony	freon	k1gInPc1	freon
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
radiační	radiační	k2eAgNnSc4d1	radiační
působení	působení	k1gNnSc4	působení
0,18	[number]	k4	0,18
(	(	kIx(	(
<g/>
0,01	[number]	k4	0,01
až	až	k9	až
0,35	[number]	k4	0,35
<g/>
)	)	kIx)	)
Wm	Wm	k1gFnSc1	Wm
<g/>
−	−	k?	−
<g/>
2	[number]	k4	2
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
radiační	radiační	k2eAgNnSc1d1	radiační
působení	působení	k1gNnSc1	působení
celkového	celkový	k2eAgNnSc2d1	celkové
působení	působení	k1gNnSc2	působení
aerosolů	aerosol	k1gInPc2	aerosol
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
i	i	k9	i
zvyšování	zvyšování	k1gNnSc1	zvyšování
oblačnosti	oblačnost	k1gFnSc2	oblačnost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
−	−	k?	−
(	(	kIx(	(
<g/>
−	−	k?	−
<g/>
1,9	[number]	k4	1,9
až	až	k8xS	až
−	−	k?	−
<g/>
)	)	kIx)	)
Wm	Wm	k1gFnSc1	Wm
<g/>
−	−	k?	−
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
působení	působení	k1gNnSc1	působení
je	být	k5eAaImIp3nS	být
kombinací	kombinace	k1gFnSc7	kombinace
negativního	negativní	k2eAgNnSc2d1	negativní
působení	působení	k1gNnSc2	působení
aerosolů	aerosol	k1gInPc2	aerosol
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
pozitivním	pozitivní	k2eAgNnSc7d1	pozitivní
působením	působení	k1gNnSc7	působení
černého	černý	k2eAgInSc2d1	černý
uhlíku	uhlík	k1gInSc2	uhlík
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
interakce	interakce	k1gFnSc1	interakce
aerosolů	aerosol	k1gInPc2	aerosol
s	s	k7c7	s
mraky	mrak	k1gInPc7	mrak
způsobily	způsobit	k5eAaPmAgInP	způsobit
posun	posun	k1gInSc4	posun
v	v	k7c6	v
celkovém	celkový	k2eAgNnSc6d1	celkové
průměrném	průměrný	k2eAgNnSc6d1	průměrné
radiačním	radiační	k2eAgNnSc6d1	radiační
působení	působení	k1gNnSc6	působení
<g/>
;	;	kIx,	;
přispívají	přispívat	k5eAaImIp3nP	přispívat
k	k	k7c3	k
největším	veliký	k2eAgFnPc3d3	veliký
nejistotám	nejistota	k1gFnPc3	nejistota
v	v	k7c6	v
určení	určení	k1gNnSc6	určení
celkového	celkový	k2eAgNnSc2d1	celkové
radiačního	radiační	k2eAgNnSc2d1	radiační
působení	působení	k1gNnSc2	působení
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
působení	působení	k1gNnSc1	působení
částic	částice	k1gFnPc2	částice
z	z	k7c2	z
vulkanické	vulkanický	k2eAgFnSc2d1	vulkanická
činnosti	činnost	k1gFnSc2	činnost
má	mít	k5eAaImIp3nS	mít
velký	velký	k2eAgInSc4d1	velký
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
klima	klima	k1gNnSc4	klima
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
následujících	následující	k2eAgNnPc6d1	následující
po	po	k7c6	po
velkých	velký	k2eAgFnPc6d1	velká
erupcích	erupce	k1gFnPc6	erupce
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
let	léto	k1gNnPc2	léto
2008	[number]	k4	2008
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
je	být	k5eAaImIp3nS	být
odhadováno	odhadovat	k5eAaImNgNnS	odhadovat
toto	tento	k3xDgNnSc1	tento
působení	působení	k1gNnSc1	působení
na	na	k7c4	na
−	−	k?	−
(	(	kIx(	(
<g/>
−	−	k?	−
<g/>
0,15	[number]	k4	0,15
až	až	k8xS	až
−	−	k?	−
<g/>
)	)	kIx)	)
Wm	Wm	k1gFnSc1	Wm
<g/>
−	−	k?	−
<g/>
2	[number]	k4	2
<g/>
;	;	kIx,	;
</s>
</p>
<p>
<s>
působení	působení	k1gNnSc1	působení
aktivit	aktivita	k1gFnPc2	aktivita
Slunce	slunce	k1gNnSc2	slunce
je	být	k5eAaImIp3nS	být
odhadováno	odhadovat	k5eAaImNgNnS	odhadovat
na	na	k7c4	na
0,05	[number]	k4	0,05
(	(	kIx(	(
<g/>
0,00	[number]	k4	0,00
až	až	k9	až
0,10	[number]	k4	0,10
<g/>
)	)	kIx)	)
Wm	Wm	k1gFnSc1	Wm
<g/>
−	−	k?	−
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Satelitní	satelitní	k2eAgNnPc1d1	satelitní
pozorování	pozorování	k1gNnPc1	pozorování
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1978	[number]	k4	1978
až	až	k8xS	až
2011	[number]	k4	2011
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
poslední	poslední	k2eAgNnSc1d1	poslední
solární	solární	k2eAgNnSc1d1	solární
minimum	minimum	k1gNnSc1	minimum
bylo	být	k5eAaImAgNnS	být
výraznější	výrazný	k2eAgNnSc1d2	výraznější
<g/>
,	,	kIx,	,
než	než	k8xS	než
předchozí	předchozí	k2eAgFnPc1d1	předchozí
dvě	dva	k4xCgNnPc1	dva
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
RP	RP	kA	RP
−	−	k?	−
(	(	kIx(	(
<g/>
−	−	k?	−
<g/>
0,08	[number]	k4	0,08
až	až	k9	až
0,00	[number]	k4	0,00
<g/>
)	)	kIx)	)
Wm	Wm	k1gFnSc1	Wm
<g/>
−	−	k?	−
<g/>
2	[number]	k4	2
při	při	k7c6	při
porovnání	porovnání	k1gNnSc6	porovnání
minim	minimum	k1gNnPc2	minimum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
a	a	k8xC	a
1986	[number]	k4	1986
<g/>
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Citlivost	citlivost	k1gFnSc1	citlivost
klimatu	klima	k1gNnSc2	klima
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
míra	míra	k1gFnSc1	míra
odezvy	odezva	k1gFnSc2	odezva
klimatického	klimatický	k2eAgInSc2d1	klimatický
systému	systém	k1gInSc2	systém
na	na	k7c4	na
radiační	radiační	k2eAgNnSc4d1	radiační
působení	působení	k1gNnSc4	působení
<g/>
,	,	kIx,	,
ze	z	k7c2	z
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
oteplení	oteplení	k1gNnSc1	oteplení
na	na	k7c6	na
základě	základ	k1gInSc6	základ
změny	změna	k1gFnSc2	změna
toku	tok	k1gInSc2	tok
záření	záření	k1gNnSc2	záření
(	(	kIx(	(
<g/>
způsobené	způsobený	k2eAgNnSc1d1	způsobené
změnou	změna	k1gFnSc7	změna
koncentrací	koncentrace	k1gFnPc2	koncentrace
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
zhruba	zhruba	k6eAd1	zhruba
rovnu	roven	k2eAgFnSc4d1	rovna
0,8	[number]	k4	0,8
K	K	kA	K
<g/>
/	/	kIx~	/
<g/>
(	(	kIx(	(
<g/>
W	W	kA	W
<g/>
/	/	kIx~	/
<g/>
m	m	kA	m
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
studie	studie	k1gFnPc1	studie
ale	ale	k9	ale
udávají	udávat	k5eAaImIp3nP	udávat
jiné	jiný	k2eAgFnPc4d1	jiná
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Částice	částice	k1gFnPc4	částice
a	a	k8xC	a
saze	saze	k1gFnPc4	saze
===	===	k?	===
</s>
</p>
<p>
<s>
Globální	globální	k2eAgNnSc1d1	globální
stmívání	stmívání	k1gNnSc1	stmívání
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
globální	globální	k2eAgInSc1d1	globální
pokles	pokles	k1gInSc1	pokles
přímého	přímý	k2eAgNnSc2d1	přímé
ozařování	ozařování	k1gNnSc2	ozařování
zemského	zemský	k2eAgInSc2d1	zemský
povrchu	povrch	k1gInSc2	povrch
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
pozorováno	pozorovat	k5eAaImNgNnS	pozorovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
až	až	k9	až
minimálně	minimálně	k6eAd1	minimálně
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Pevné	pevný	k2eAgFnPc1d1	pevná
a	a	k8xC	a
kapalné	kapalný	k2eAgFnPc1d1	kapalná
částice	částice	k1gFnPc1	částice
známé	známý	k2eAgFnPc1d1	známá
jako	jako	k8xC	jako
aerosoly	aerosol	k1gInPc1	aerosol
produkované	produkovaný	k2eAgInPc1d1	produkovaný
sopkami	sopka	k1gFnPc7	sopka
a	a	k8xC	a
znečišťující	znečišťující	k2eAgFnPc4d1	znečišťující
látky	látka	k1gFnPc4	látka
produkované	produkovaný	k2eAgFnPc4d1	produkovaná
průmyslem	průmysl	k1gInSc7	průmysl
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
hlavní	hlavní	k2eAgFnSc4d1	hlavní
příčinu	příčina	k1gFnSc4	příčina
tohoto	tento	k3xDgNnSc2	tento
stmívání	stmívání	k1gNnSc2	stmívání
(	(	kIx(	(
<g/>
např.	např.	kA	např.
polétavý	polétavý	k2eAgInSc1d1	polétavý
prach	prach	k1gInSc1	prach
<g/>
,	,	kIx,	,
sírany	síran	k1gInPc1	síran
<g/>
,	,	kIx,	,
nitráty	nitrát	k1gInPc1	nitrát
<g/>
,	,	kIx,	,
organický	organický	k2eAgInSc4d1	organický
uhlík	uhlík	k1gInSc4	uhlík
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
částice	částice	k1gFnPc1	částice
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
ochlazovací	ochlazovací	k2eAgInSc4d1	ochlazovací
efekt	efekt	k1gInSc4	efekt
zvýšeným	zvýšený	k2eAgInSc7d1	zvýšený
odrazem	odraz	k1gInSc7	odraz
přicházejícího	přicházející	k2eAgNnSc2d1	přicházející
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Účinky	účinek	k1gInPc1	účinek
produktů	produkt	k1gInPc2	produkt
spalování	spalování	k1gNnSc1	spalování
fosilních	fosilní	k2eAgNnPc2d1	fosilní
paliv	palivo	k1gNnPc2	palivo
–	–	k?	–
CO2	CO2	k1gFnSc2	CO2
a	a	k8xC	a
aerosolů	aerosol	k1gInPc2	aerosol
–	–	k?	–
se	se	k3xPyFc4	se
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
míře	míra	k1gFnSc6	míra
v	v	k7c6	v
minulých	minulý	k2eAgNnPc6d1	Minulé
desetiletích	desetiletí	k1gNnPc6	desetiletí
navzájem	navzájem	k6eAd1	navzájem
kompenzovaly	kompenzovat	k5eAaBmAgFnP	kompenzovat
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
čisté	čistý	k2eAgNnSc1d1	čisté
oteplování	oteplování	k1gNnSc1	oteplování
bylo	být	k5eAaImAgNnS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
nárůstem	nárůst	k1gInSc7	nárůst
dalších	další	k2eAgInPc2d1	další
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
methan	methan	k1gInSc4	methan
<g/>
.	.	kIx.	.
</s>
<s>
Radiační	radiační	k2eAgNnSc1d1	radiační
působení	působení	k1gNnSc1	působení
těchto	tento	k3xDgFnPc2	tento
částic	částice	k1gFnPc2	částice
je	být	k5eAaImIp3nS	být
však	však	k9	však
časově	časově	k6eAd1	časově
omezené	omezený	k2eAgFnSc2d1	omezená
díky	díky	k7c3	díky
mokré	mokrý	k2eAgFnSc3d1	mokrá
depozici	depozice	k1gFnSc3	depozice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gFnSc1	jejich
doba	doba	k1gFnSc1	doba
setrvání	setrvání	k1gNnSc2	setrvání
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
týden	týden	k1gInSc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
oxid	oxid	k1gInSc1	oxid
uhličitý	uhličitý	k2eAgInSc1d1	uhličitý
má	mít	k5eAaImIp3nS	mít
životnost	životnost	k1gFnSc4	životnost
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
století	století	k1gNnSc2	století
i	i	k9	i
více	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
zvýšené	zvýšený	k2eAgFnPc1d1	zvýšená
koncentrace	koncentrace	k1gFnPc1	koncentrace
částic	částice	k1gFnPc2	částice
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
pouze	pouze	k6eAd1	pouze
pozdrží	pozdržet	k5eAaPmIp3nP	pozdržet
klimatické	klimatický	k2eAgFnPc4d1	klimatická
změny	změna	k1gFnPc4	změna
způsobené	způsobený	k2eAgFnPc4d1	způsobená
oxidem	oxid	k1gInSc7	oxid
uhličitým	uhličitý	k2eAgInSc7d1	uhličitý
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
černý	černý	k2eAgInSc4d1	černý
uhlík	uhlík	k1gInSc4	uhlík
má	mít	k5eAaImIp3nS	mít
<g/>
,	,	kIx,	,
po	po	k7c6	po
oxidu	oxid	k1gInSc6	oxid
uhličitém	uhličitý	k2eAgInSc6d1	uhličitý
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
největší	veliký	k2eAgInSc4d3	veliký
příspěvek	příspěvek	k1gInSc4	příspěvek
ke	k	k7c3	k
globálnímu	globální	k2eAgNnSc3d1	globální
oteplování	oteplování	k1gNnSc3	oteplování
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
k	k	k7c3	k
jejich	jejich	k3xOp3gInSc3	jejich
přímému	přímý	k2eAgInSc3d1	přímý
vlivu	vliv	k1gInSc3	vliv
díky	díky	k7c3	díky
rozptylu	rozptyl	k1gInSc3	rozptyl
a	a	k8xC	a
absorpci	absorpce	k1gFnSc3	absorpce
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
mají	mít	k5eAaImIp3nP	mít
částice	částice	k1gFnPc4	částice
nepřímý	přímý	k2eNgInSc4d1	nepřímý
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
energetický	energetický	k2eAgInSc4d1	energetický
účet	účet	k1gInSc4	účet
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Sulfáty	sulfát	k1gInPc1	sulfát
působí	působit	k5eAaImIp3nP	působit
jako	jako	k9	jako
kondenzační	kondenzační	k2eAgNnPc1d1	kondenzační
jádra	jádro	k1gNnPc1	jádro
mraků	mrak	k1gInPc2	mrak
a	a	k8xC	a
vznikají	vznikat	k5eAaImIp3nP	vznikat
tak	tak	k9	tak
mraky	mrak	k1gInPc1	mrak
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
menších	malý	k2eAgFnPc2d2	menší
kapiček	kapička	k1gFnPc2	kapička
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
mraky	mrak	k1gInPc1	mrak
odrážejí	odrážet	k5eAaImIp3nP	odrážet
sluneční	sluneční	k2eAgNnSc4d1	sluneční
záření	záření	k1gNnSc4	záření
účinněji	účinně	k6eAd2	účinně
než	než	k8xS	než
mraky	mrak	k1gInPc1	mrak
s	s	k7c7	s
menším	malý	k2eAgNnSc7d2	menší
množstvím	množství	k1gNnSc7	množství
větších	veliký	k2eAgFnPc2d2	veliký
kapek	kapka	k1gFnPc2	kapka
–	–	k?	–
tento	tento	k3xDgInSc4	tento
jev	jev	k1gInSc4	jev
nese	nést	k5eAaImIp3nS	nést
název	název	k1gInSc1	název
Twomeyův	Twomeyův	k2eAgInSc1d1	Twomeyův
jev	jev	k1gInSc1	jev
(	(	kIx(	(
<g/>
Twomey	Twomey	k1gInPc1	Twomey
effect	effecta	k1gFnPc2	effecta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
též	též	k9	též
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
částice	částice	k1gFnPc1	částice
mají	mít	k5eAaImIp3nP	mít
ve	v	k7c6	v
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
stejnou	stejný	k2eAgFnSc4d1	stejná
velikost	velikost	k1gFnSc4	velikost
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
omezuje	omezovat	k5eAaImIp3nS	omezovat
vznik	vznik	k1gInSc4	vznik
dešťových	dešťový	k2eAgFnPc2d1	dešťová
kapek	kapka	k1gFnPc2	kapka
a	a	k8xC	a
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
větší	veliký	k2eAgInSc4d2	veliký
odraz	odraz	k1gInSc4	odraz
přicházejícího	přicházející	k2eAgNnSc2d1	přicházející
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
mraky	mrak	k1gInPc1	mrak
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Albrechtův	Albrechtův	k2eAgInSc1d1	Albrechtův
jev	jev	k1gInSc1	jev
(	(	kIx(	(
<g/>
Albrecht	Albrecht	k1gMnSc1	Albrecht
effect	effect	k1gMnSc1	effect
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nepřímé	přímý	k2eNgInPc1d1	nepřímý
vlivy	vliv	k1gInPc1	vliv
jsou	být	k5eAaImIp3nP	být
nejvíce	nejvíce	k6eAd1	nejvíce
patrné	patrný	k2eAgFnPc1d1	patrná
v	v	k7c6	v
případě	případ	k1gInSc6	případ
stratiformní	stratiformní	k2eAgFnSc2d1	stratiformní
oblačnosti	oblačnost	k1gFnSc2	oblačnost
nad	nad	k7c7	nad
oceány	oceán	k1gInPc7	oceán
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
jen	jen	k9	jen
malý	malý	k2eAgInSc4d1	malý
vliv	vliv	k1gInSc4	vliv
v	v	k7c6	v
případě	případ	k1gInSc6	případ
konvektivní	konvektivní	k2eAgFnSc2d1	konvektivní
oblačnosti	oblačnost	k1gFnSc2	oblačnost
<g/>
.	.	kIx.	.
</s>
<s>
Nepřímé	přímý	k2eNgInPc1d1	nepřímý
účinky	účinek	k1gInPc1	účinek
aerosolů	aerosol	k1gInPc2	aerosol
tvoří	tvořit	k5eAaImIp3nP	tvořit
největší	veliký	k2eAgFnSc4d3	veliký
nejistotu	nejistota	k1gFnSc4	nejistota
v	v	k7c6	v
bilanci	bilance	k1gFnSc6	bilance
radiačního	radiační	k2eAgNnSc2d1	radiační
působení	působení	k1gNnSc2	působení
<g/>
.	.	kIx.	.
<g/>
Saze	saze	k1gFnPc1	saze
mohou	moct	k5eAaImIp3nP	moct
jak	jak	k6eAd1	jak
ohřívat	ohřívat	k5eAaImF	ohřívat
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
ochlazovat	ochlazovat	k5eAaImF	ochlazovat
povrch	povrch	k1gInSc4	povrch
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
ovzduší	ovzduší	k1gNnSc6	ovzduší
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
jsou	být	k5eAaImIp3nP	být
uložené	uložený	k2eAgInPc1d1	uložený
<g/>
.	.	kIx.	.
</s>
<s>
Atmosférické	atmosférický	k2eAgFnPc1d1	atmosférická
saze	saze	k1gFnPc1	saze
přímo	přímo	k6eAd1	přímo
pohlcují	pohlcovat	k5eAaImIp3nP	pohlcovat
sluneční	sluneční	k2eAgNnSc4d1	sluneční
záření	záření	k1gNnSc4	záření
a	a	k8xC	a
ohřívají	ohřívat	k5eAaImIp3nP	ohřívat
tím	ten	k3xDgNnSc7	ten
atmosféru	atmosféra	k1gFnSc4	atmosféra
a	a	k8xC	a
ochlazují	ochlazovat	k5eAaImIp3nP	ochlazovat
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
izolovaných	izolovaný	k2eAgFnPc6d1	izolovaná
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
velká	velký	k2eAgFnSc1d1	velká
produkce	produkce	k1gFnSc1	produkce
sazí	saze	k1gFnPc2	saze
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
vnitrozemí	vnitrozemí	k1gNnSc1	vnitrozemí
Indie	Indie	k1gFnSc2	Indie
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
až	až	k9	až
50	[number]	k4	50
%	%	kIx~	%
povrchového	povrchový	k2eAgNnSc2d1	povrchové
oteplování	oteplování	k1gNnSc2	oteplování
díky	díky	k7c3	díky
skleníkovým	skleníkový	k2eAgInPc3d1	skleníkový
plynům	plyn	k1gInPc3	plyn
maskováno	maskovat	k5eAaBmNgNnS	maskovat
tzv.	tzv.	kA	tzv.
hnědými	hnědý	k2eAgInPc7d1	hnědý
mraky	mrak	k1gInPc7	mrak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
usazení	usazení	k1gNnSc2	usazení
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
ledovců	ledovec	k1gInPc2	ledovec
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c6	na
ledu	led	k1gInSc6	led
v	v	k7c6	v
arktických	arktický	k2eAgFnPc6d1	arktická
oblastech	oblast	k1gFnPc6	oblast
způsobí	způsobit	k5eAaPmIp3nS	způsobit
nižší	nízký	k2eAgInSc1d2	nižší
povrchový	povrchový	k2eAgInSc1d1	povrchový
odraz	odraz	k1gInSc1	odraz
(	(	kIx(	(
<g/>
albedo	albedo	k1gNnSc1	albedo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
může	moct	k5eAaImIp3nS	moct
přímo	přímo	k6eAd1	přímo
ohřívat	ohřívat	k5eAaImF	ohřívat
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
částic	částice	k1gFnPc2	částice
včetně	včetně	k7c2	včetně
černého	černý	k2eAgInSc2d1	černý
uhlíku	uhlík	k1gInSc2	uhlík
je	být	k5eAaImIp3nS	být
nejvýraznější	výrazný	k2eAgFnSc1d3	nejvýraznější
v	v	k7c6	v
tropech	trop	k1gInPc6	trop
a	a	k8xC	a
subtropech	subtropy	k1gInPc6	subtropy
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
účinky	účinek	k1gInPc1	účinek
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
jsou	být	k5eAaImIp3nP	být
dominantní	dominantní	k2eAgMnPc1d1	dominantní
v	v	k7c6	v
mírných	mírný	k2eAgInPc6d1	mírný
pásech	pás	k1gInPc6	pás
a	a	k8xC	a
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
polokouli	polokoule	k1gFnSc6	polokoule
<g/>
.	.	kIx.	.
</s>
<s>
Výrazný	výrazný	k2eAgInSc1d1	výrazný
krátkodobý	krátkodobý	k2eAgInSc1d1	krátkodobý
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
klima	klima	k1gNnSc4	klima
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
erupce	erupce	k1gFnPc4	erupce
vulkánů	vulkán	k1gInPc2	vulkán
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sluneční	sluneční	k2eAgFnSc1d1	sluneční
aktivita	aktivita	k1gFnSc1	aktivita
===	===	k?	===
</s>
</p>
<p>
<s>
Z	z	k7c2	z
přírodních	přírodní	k2eAgInPc2d1	přírodní
faktorů	faktor	k1gInPc2	faktor
ovlivňujících	ovlivňující	k2eAgInPc2d1	ovlivňující
klima	klima	k1gNnSc4	klima
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
Slunce	slunce	k1gNnSc2	slunce
jakožto	jakožto	k8xS	jakožto
základní	základní	k2eAgInSc4d1	základní
zdroj	zdroj	k1gInSc4	zdroj
energie	energie	k1gFnSc2	energie
pro	pro	k7c4	pro
klimatický	klimatický	k2eAgInSc4d1	klimatický
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Korelace	korelace	k1gFnSc1	korelace
změn	změna	k1gFnPc2	změna
sluneční	sluneční	k2eAgFnSc2d1	sluneční
aktivity	aktivita	k1gFnSc2	aktivita
a	a	k8xC	a
změn	změna	k1gFnPc2	změna
teplot	teplota	k1gFnPc2	teplota
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
velice	velice	k6eAd1	velice
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
:	:	kIx,	:
okolo	okolo	k7c2	okolo
0,8	[number]	k4	0,8
<g/>
.	.	kIx.	.
</s>
<s>
Ať	ať	k9	ať
už	už	k9	už
za	za	k7c4	za
posledních	poslední	k2eAgInPc2d1	poslední
1	[number]	k4	1
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
za	za	k7c4	za
posledních	poslední	k2eAgInPc2d1	poslední
150	[number]	k4	150
roků	rok	k1gInPc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
nárůst	nárůst	k1gInSc4	nárůst
sluneční	sluneční	k2eAgFnSc2d1	sluneční
aktivity	aktivita	k1gFnSc2	aktivita
v	v	k7c6	v
první	první	k4xOgFnSc6	první
půli	půle	k1gFnSc6	půle
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
za	za	k7c4	za
pět	pět	k4xCc4	pět
set	sto	k4xCgNnPc2	sto
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
poukázal	poukázat	k5eAaPmAgMnS	poukázat
tým	tým	k1gInSc4	tým
Solankiho	Solanki	k1gMnSc2	Solanki
a	a	k8xC	a
Usoskina	Usoskin	k1gMnSc2	Usoskin
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
tento	tento	k3xDgInSc4	tento
nárůst	nárůst	k1gInSc4	nárůst
rozhodujícím	rozhodující	k2eAgInSc7d1	rozhodující
faktorem	faktor	k1gInSc7	faktor
oteplování	oteplování	k1gNnSc2	oteplování
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
přímých	přímý	k2eAgNnPc2d1	přímé
satelitních	satelitní	k2eAgNnPc2d1	satelitní
měření	měření	k1gNnPc2	měření
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
(	(	kIx(	(
<g/>
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
lze	lze	k6eAd1	lze
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
jistotou	jistota	k1gFnSc7	jistota
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
změny	změna	k1gFnPc1	změna
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
nepřispěly	přispět	k5eNaPmAgFnP	přispět
k	k	k7c3	k
vzestupu	vzestup	k1gInSc3	vzestup
globálních	globální	k2eAgFnPc2d1	globální
průměrných	průměrný	k2eAgFnPc2d1	průměrná
teplot	teplota	k1gFnPc2	teplota
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Země	zem	k1gFnSc2	zem
v	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
1986	[number]	k4	1986
<g/>
–	–	k?	–
<g/>
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
střední	střední	k2eAgFnSc7d1	střední
jistotou	jistota	k1gFnSc7	jistota
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
jedenáctileté	jedenáctiletý	k2eAgInPc1d1	jedenáctiletý
sluneční	sluneční	k2eAgInPc1d1	sluneční
cykly	cyklus	k1gInPc1	cyklus
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
Země	zem	k1gFnSc2	zem
fluktuace	fluktuace	k1gFnSc2	fluktuace
v	v	k7c6	v
klimatických	klimatický	k2eAgInPc6d1	klimatický
projevech	projev	k1gInPc6	projev
<g/>
.	.	kIx.	.
</s>
<s>
Nebyl	být	k5eNaImAgInS	být
zjištěn	zjistit	k5eAaPmNgInS	zjistit
těsnější	těsný	k2eAgInSc1d2	těsnější
vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
kosmickými	kosmický	k2eAgInPc7d1	kosmický
paprsky	paprsek	k1gInPc7	paprsek
a	a	k8xC	a
oblačností	oblačnost	k1gFnSc7	oblačnost
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
ověření	ověření	k1gNnSc3	ověření
vlivu	vliv	k1gInSc2	vliv
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
jsou	být	k5eAaImIp3nP	být
používány	používán	k2eAgInPc1d1	používán
klimatické	klimatický	k2eAgInPc1d1	klimatický
modely	model	k1gInPc1	model
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
modely	model	k1gInPc1	model
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
rychlé	rychlý	k2eAgNnSc4d1	rychlé
oteplování	oteplování	k1gNnSc4	oteplování
posledních	poslední	k2eAgNnPc2d1	poslední
desetiletí	desetiletí	k1gNnPc2	desetiletí
nelze	lze	k6eNd1	lze
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
pouze	pouze	k6eAd1	pouze
změnami	změna	k1gFnPc7	změna
intenzity	intenzita	k1gFnSc2	intenzita
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
a	a	k8xC	a
vulkanickou	vulkanický	k2eAgFnSc7d1	vulkanická
činností	činnost	k1gFnSc7	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
poslední	poslední	k2eAgInSc1d1	poslední
sluneční	sluneční	k2eAgInSc1d1	sluneční
cyklus	cyklus	k1gInSc1	cyklus
je	být	k5eAaImIp3nS	být
výrazně	výrazně	k6eAd1	výrazně
slabší	slabý	k2eAgFnSc1d2	slabší
<g/>
,	,	kIx,	,
než	než	k8xS	než
cykly	cyklus	k1gInPc4	cyklus
předchozí	předchozí	k2eAgInPc4d1	předchozí
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
však	však	k9	však
jsou	být	k5eAaImIp3nP	být
do	do	k7c2	do
modelů	model	k1gInPc2	model
započítány	započítat	k5eAaPmNgInP	započítat
i	i	k9	i
antropogenní	antropogenní	k2eAgInPc1d1	antropogenní
vlivy	vliv	k1gInPc1	vliv
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
schopny	schopen	k2eAgFnPc1d1	schopna
reprodukovat	reprodukovat	k5eAaBmF	reprodukovat
teplotní	teplotní	k2eAgInSc4d1	teplotní
vzestup	vzestup	k1gInSc4	vzestup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgInSc7d1	další
důkaz	důkaz	k1gInSc1	důkaz
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Slunce	slunce	k1gNnSc1	slunce
není	být	k5eNaImIp3nS	být
příčinou	příčina	k1gFnSc7	příčina
současných	současný	k2eAgFnPc2d1	současná
klimatických	klimatický	k2eAgFnPc2d1	klimatická
změn	změna	k1gFnPc2	změna
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
dán	dát	k5eAaPmNgInS	dát
pozorováním	pozorování	k1gNnSc7	pozorování
změn	změna	k1gFnPc2	změna
teplot	teplota	k1gFnPc2	teplota
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
atmosférických	atmosférický	k2eAgFnPc6d1	atmosférická
vrstvách	vrstva	k1gFnPc6	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
základních	základní	k2eAgInPc2d1	základní
fyzikálních	fyzikální	k2eAgInPc2d1	fyzikální
principů	princip	k1gInPc2	princip
působí	působit	k5eAaImIp3nP	působit
skleníkové	skleníkový	k2eAgInPc1d1	skleníkový
plyny	plyn	k1gInPc1	plyn
ohřívání	ohřívání	k1gNnSc2	ohřívání
dolních	dolní	k2eAgFnPc2d1	dolní
vrstev	vrstva	k1gFnPc2	vrstva
atmosféry	atmosféra	k1gFnSc2	atmosféra
–	–	k?	–
troposféry	troposféra	k1gFnSc2	troposféra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zároveň	zároveň	k6eAd1	zároveň
ochlazování	ochlazování	k1gNnSc3	ochlazování
vyšších	vysoký	k2eAgFnPc2d2	vyšší
vrstev	vrstva	k1gFnPc2	vrstva
–	–	k?	–
stratosféry	stratosféra	k1gFnSc2	stratosféra
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
by	by	kYmCp3nP	by
bylo	být	k5eAaImAgNnS	být
příčinou	příčina	k1gFnSc7	příčina
globálního	globální	k2eAgNnSc2d1	globální
oteplení	oteplení	k1gNnSc2	oteplení
Slunce	slunce	k1gNnSc2	slunce
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
by	by	kYmCp3nS	by
třeba	třeba	k6eAd1	třeba
očekávat	očekávat	k5eAaImF	očekávat
oteplení	oteplení	k1gNnSc4	oteplení
jak	jak	k8xS	jak
v	v	k7c6	v
troposféře	troposféra	k1gFnSc6	troposféra
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
ve	v	k7c6	v
stratosféře	stratosféra	k1gFnSc6	stratosféra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Změny	změna	k1gFnPc4	změna
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
Země	zem	k1gFnSc2	zem
===	===	k?	===
</s>
</p>
<p>
<s>
Změny	změna	k1gFnPc1	změna
sklonu	sklon	k1gInSc2	sklon
osy	osa	k1gFnSc2	osa
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
tvar	tvar	k1gInSc1	tvar
oběžné	oběžný	k2eAgFnSc2d1	oběžná
dráhy	dráha	k1gFnSc2	dráha
kolem	kolem	k7c2	kolem
Slunce	slunce	k1gNnSc2	slunce
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
pomalu	pomalu	k6eAd1	pomalu
za	za	k7c4	za
desítky	desítka	k1gFnPc4	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
těmto	tento	k3xDgFnPc3	tento
změnám	změna	k1gFnPc3	změna
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
sezónního	sezónní	k2eAgNnSc2d1	sezónní
a	a	k8xC	a
zeměpisného	zeměpisný	k2eAgNnSc2d1	zeměpisné
rozložení	rozložení	k1gNnSc2	rozložení
příchozí	příchozí	k1gFnSc2	příchozí
sluneční	sluneční	k2eAgFnSc2d1	sluneční
energie	energie	k1gFnSc2	energie
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
klimatu	klima	k1gNnSc2	klima
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
posledních	poslední	k2eAgInPc2d1	poslední
několika	několik	k4yIc2	několik
tisíc	tisíc	k4xCgInPc2	tisíc
let	let	k1gInSc4	let
tento	tento	k3xDgInSc4	tento
jev	jev	k1gInSc4	jev
přispěl	přispět	k5eAaPmAgMnS	přispět
k	k	k7c3	k
pomalému	pomalý	k2eAgInSc3d1	pomalý
trendu	trend	k1gInSc3	trend
ochlazování	ochlazování	k1gNnSc2	ochlazování
ve	v	k7c6	v
vysokých	vysoký	k2eAgFnPc6d1	vysoká
zeměpisných	zeměpisný	k2eAgFnPc6d1	zeměpisná
šířkách	šířka	k1gFnPc6	šířka
severní	severní	k2eAgFnSc2d1	severní
polokoule	polokoule	k1gFnSc2	polokoule
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
<g/>
;	;	kIx,	;
tento	tento	k3xDgInSc1	tento
trend	trend	k1gInSc1	trend
se	se	k3xPyFc4	se
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
obrátil	obrátit	k5eAaPmAgMnS	obrátit
díky	díky	k7c3	díky
oteplování	oteplování	k1gNnSc3	oteplování
vyvolaného	vyvolaný	k2eAgInSc2d1	vyvolaný
skleníkovými	skleníkový	k2eAgInPc7d1	skleníkový
plyny	plyn	k1gInPc7	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
následujících	následující	k2eAgNnPc2d1	následující
50	[number]	k4	50
000	[number]	k4	000
let	léto	k1gNnPc2	léto
nelze	lze	k6eNd1	lze
očekávat	očekávat	k5eAaImF	očekávat
orbitální	orbitální	k2eAgFnPc4d1	orbitální
změny	změna	k1gFnPc4	změna
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
by	by	kYmCp3nP	by
vedly	vést	k5eAaImAgInP	vést
k	k	k7c3	k
ochlazování	ochlazování	k1gNnSc3	ochlazování
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klimatická	klimatický	k2eAgFnSc1d1	klimatická
zpětná	zpětný	k2eAgFnSc1d1	zpětná
vazba	vazba	k1gFnSc1	vazba
==	==	k?	==
</s>
</p>
<p>
<s>
Klimatický	klimatický	k2eAgInSc1d1	klimatický
systém	systém	k1gInSc1	systém
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
zpětných	zpětný	k2eAgFnPc2d1	zpětná
vazeb	vazba	k1gFnPc2	vazba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mění	měnit	k5eAaImIp3nP	měnit
reakce	reakce	k1gFnPc1	reakce
systému	systém	k1gInSc2	systém
na	na	k7c4	na
změny	změna	k1gFnPc4	změna
ve	v	k7c4	v
vnější	vnější	k2eAgFnPc4d1	vnější
síly	síla	k1gFnPc4	síla
<g/>
.	.	kIx.	.
</s>
<s>
Pozitivní	pozitivní	k2eAgFnSc1d1	pozitivní
zpětná	zpětný	k2eAgFnSc1d1	zpětná
vazba	vazba	k1gFnSc1	vazba
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
zvýšení	zvýšení	k1gNnSc4	zvýšení
odezvy	odezva	k1gFnSc2	odezva
klimatického	klimatický	k2eAgInSc2d1	klimatický
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
negativní	negativní	k2eAgFnSc1d1	negativní
zpětná	zpětný	k2eAgFnSc1d1	zpětná
vazba	vazba	k1gFnSc1	vazba
tyto	tento	k3xDgFnPc4	tento
odezvy	odezva	k1gFnPc4	odezva
snižuje	snižovat	k5eAaImIp3nS	snižovat
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c4	mezi
zpětné	zpětný	k2eAgFnPc4d1	zpětná
vazby	vazba	k1gFnPc4	vazba
klimatického	klimatický	k2eAgInSc2d1	klimatický
systému	systém	k1gInSc2	systém
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nP	řadit
vodní	vodní	k2eAgInPc1d1	vodní
páry	pár	k1gInPc1	pár
<g/>
,	,	kIx,	,
změny	změna	k1gFnPc1	změna
na	na	k7c6	na
ledovém	ledový	k2eAgInSc6d1	ledový
a	a	k8xC	a
sněhovém	sněhový	k2eAgInSc6d1	sněhový
povrchu	povrch	k1gInSc6	povrch
(	(	kIx(	(
<g/>
sněhový	sněhový	k2eAgInSc4d1	sněhový
a	a	k8xC	a
ledový	ledový	k2eAgInSc4d1	ledový
kryt	kryt	k1gInSc4	kryt
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
množství	množství	k1gNnSc1	množství
pohlceného	pohlcený	k2eAgNnSc2d1	pohlcené
nebo	nebo	k8xC	nebo
odráženého	odrážený	k2eAgNnSc2d1	odrážené
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
mraky	mrak	k1gInPc1	mrak
a	a	k8xC	a
změny	změna	k1gFnPc1	změna
v	v	k7c6	v
koloběhu	koloběh	k1gInSc6	koloběh
uhlíku	uhlík	k1gInSc2	uhlík
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
(	(	kIx(	(
<g/>
např.	např.	kA	např.
uvolňování	uvolňování	k1gNnSc1	uvolňování
uhlíku	uhlík	k1gInSc2	uhlík
z	z	k7c2	z
půdy	půda	k1gFnSc2	půda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
negativní	negativní	k2eAgFnSc7d1	negativní
zpětnou	zpětný	k2eAgFnSc7d1	zpětná
vazbou	vazba	k1gFnSc7	vazba
je	být	k5eAaImIp3nS	být
energie	energie	k1gFnSc1	energie
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
zemský	zemský	k2eAgInSc1d1	zemský
povrch	povrch	k1gInSc1	povrch
vyzařuje	vyzařovat	k5eAaImIp3nS	vyzařovat
do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
jako	jako	k8xS	jako
infračervené	infračervený	k2eAgNnSc4d1	infračervené
záření	záření	k1gNnSc4	záření
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
vyzařování	vyzařování	k1gNnSc1	vyzařování
narůstá	narůstat	k5eAaImIp3nS	narůstat
silně	silně	k6eAd1	silně
s	s	k7c7	s
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
teplotou	teplota	k1gFnSc7	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Zpětné	zpětný	k2eAgFnPc1d1	zpětná
vazby	vazba	k1gFnPc1	vazba
jsou	být	k5eAaImIp3nP	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
faktorem	faktor	k1gInSc7	faktor
při	při	k7c6	při
určování	určování	k1gNnSc6	určování
citlivosti	citlivost	k1gFnSc2	citlivost
klimatického	klimatický	k2eAgInSc2d1	klimatický
systému	systém	k1gInSc2	systém
na	na	k7c6	na
zvýšení	zvýšení	k1gNnSc6	zvýšení
koncentrací	koncentrace	k1gFnPc2	koncentrace
atmosférických	atmosférický	k2eAgInPc2d1	atmosférický
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
klimatická	klimatický	k2eAgFnSc1d1	klimatická
citlivost	citlivost	k1gFnSc1	citlivost
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
daném	daný	k2eAgNnSc6d1	dané
zvýšení	zvýšení	k1gNnSc6	zvýšení
účinku	účinek	k1gInSc2	účinek
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
dojde	dojít	k5eAaPmIp3nS	dojít
díky	díky	k7c3	díky
zpětným	zpětný	k2eAgFnPc3d1	zpětná
vazbám	vazba	k1gFnPc3	vazba
k	k	k7c3	k
většímu	veliký	k2eAgNnSc3d2	veliký
oteplování	oteplování	k1gNnSc3	oteplování
<g/>
.	.	kIx.	.
</s>
<s>
Nejistoty	nejistota	k1gFnPc1	nejistota
ohledně	ohledně	k7c2	ohledně
vlivu	vliv	k1gInSc2	vliv
zpětných	zpětný	k2eAgFnPc2d1	zpětná
vazeb	vazba	k1gFnPc2	vazba
jsou	být	k5eAaImIp3nP	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
důvodem	důvod	k1gInSc7	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
různé	různý	k2eAgInPc1d1	různý
klimatické	klimatický	k2eAgInPc1d1	klimatický
modely	model	k1gInPc1	model
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
různé	různý	k2eAgFnPc4d1	různá
velikosti	velikost	k1gFnPc4	velikost
oteplování	oteplování	k1gNnSc2	oteplování
pro	pro	k7c4	pro
daný	daný	k2eAgInSc4d1	daný
scénář	scénář	k1gInSc4	scénář
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
další	další	k2eAgInSc4d1	další
výzkum	výzkum	k1gInSc4	výzkum
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
pochopili	pochopit	k5eAaPmAgMnP	pochopit
úlohu	úloha	k1gFnSc4	úloha
mraků	mrak	k1gInPc2	mrak
a	a	k8xC	a
zpětné	zpětný	k2eAgFnSc2d1	zpětná
vazby	vazba	k1gFnSc2	vazba
uhlíkového	uhlíkový	k2eAgInSc2d1	uhlíkový
cyklu	cyklus	k1gInSc2	cyklus
v	v	k7c6	v
projekcích	projekce	k1gFnPc6	projekce
klimatu	klima	k1gNnSc2	klima
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
Harvardu	Harvard	k1gInSc6	Harvard
provedena	proveden	k2eAgFnSc1d1	provedena
studie	studie	k1gFnSc1	studie
závislost	závislost	k1gFnSc4	závislost
změn	změna	k1gFnPc2	změna
klimatu	klima	k1gNnSc2	klima
na	na	k7c4	na
uvolňování	uvolňování	k1gNnSc4	uvolňování
uhlíku	uhlík	k1gInSc2	uhlík
z	z	k7c2	z
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
studie	studie	k1gFnSc1	studie
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
se	se	k3xPyFc4	se
očekává	očekávat	k5eAaImIp3nS	očekávat
uvolnění	uvolnění	k1gNnSc3	uvolnění
asi	asi	k9	asi
190.109	[number]	k4	190.109
t	t	k?	t
půdního	půdní	k2eAgInSc2d1	půdní
uhlíku	uhlík	k1gInSc2	uhlík
z	z	k7c2	z
metrové	metrový	k2eAgFnSc2d1	metrová
povrchové	povrchový	k2eAgFnSc2d1	povrchová
vrstvy	vrstva	k1gFnSc2	vrstva
půdy	půda	k1gFnSc2	půda
díky	díky	k7c3	díky
změnám	změna	k1gFnPc3	změna
mikrobiálních	mikrobiální	k2eAgFnPc2d1	mikrobiální
společenstev	společenstvo	k1gNnPc2	společenstvo
působením	působení	k1gNnSc7	působení
zvýšených	zvýšený	k2eAgFnPc2d1	zvýšená
teplot	teplota	k1gFnPc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
ekvivalent	ekvivalent	k1gInSc1	ekvivalent
emisí	emise	k1gFnPc2	emise
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
ze	z	k7c2	z
spalování	spalování	k1gNnSc2	spalování
fosilních	fosilní	k2eAgNnPc2d1	fosilní
paliv	palivo	k1gNnPc2	palivo
za	za	k7c4	za
období	období	k1gNnSc4	období
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
studie	studie	k1gFnSc1	studie
provedená	provedený	k2eAgFnSc1d1	provedená
výzkumnými	výzkumný	k2eAgMnPc7d1	výzkumný
pracovníky	pracovník	k1gMnPc7	pracovník
Harvardu	Harvard	k1gInSc2	Harvard
naznačuje	naznačovat	k5eAaImIp3nS	naznačovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
koncentrace	koncentrace	k1gFnSc1	koncentrace
vodních	vodní	k2eAgFnPc2d1	vodní
par	para	k1gFnPc2	para
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
dostávají	dostávat	k5eAaImIp3nP	dostávat
do	do	k7c2	do
stratosféry	stratosféra	k1gFnSc2	stratosféra
kvůli	kvůli	k7c3	kvůli
rostoucí	rostoucí	k2eAgFnSc3d1	rostoucí
teplotě	teplota	k1gFnSc3	teplota
narušují	narušovat	k5eAaImIp3nP	narušovat
stratosférický	stratosférický	k2eAgInSc1d1	stratosférický
ozon	ozon	k1gInSc1	ozon
a	a	k8xC	a
následně	následně	k6eAd1	následně
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
pravděpodobnost	pravděpodobnost	k1gFnSc4	pravděpodobnost
rakoviny	rakovina	k1gFnSc2	rakovina
kůže	kůže	k1gFnSc2	kůže
a	a	k8xC	a
poškozování	poškozování	k1gNnSc1	poškozování
úrody	úroda	k1gFnSc2	úroda
<g/>
.	.	kIx.	.
<g/>
Arktické	arktický	k2eAgFnPc1d1	arktická
teploty	teplota	k1gFnPc1	teplota
stoupají	stoupat	k5eAaImIp3nP	stoupat
téměř	téměř	k6eAd1	téměř
dvojnásobnou	dvojnásobný	k2eAgFnSc7d1	dvojnásobná
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
celosvětový	celosvětový	k2eAgInSc1d1	celosvětový
průměr	průměr	k1gInSc1	průměr
oteplování	oteplování	k1gNnSc2	oteplování
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
jak	jak	k8xC	jak
intenzifikace	intenzifikace	k1gFnSc1	intenzifikace
přenosu	přenos	k1gInSc2	přenos
tepla	tepnout	k5eAaPmAgFnS	tepnout
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
pólu	pól	k1gInSc3	pól
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
změny	změna	k1gFnPc1	změna
lokální	lokální	k2eAgFnSc2d1	lokální
čisté	čistý	k2eAgFnSc2d1	čistá
radiační	radiační	k2eAgFnSc2d1	radiační
rovnováhy	rovnováha	k1gFnSc2	rovnováha
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
faktory	faktor	k1gInPc1	faktor
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
přispívají	přispívat	k5eAaImIp3nP	přispívat
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
intenzivnímu	intenzivní	k2eAgNnSc3d1	intenzivní
oteplování	oteplování	k1gNnSc3	oteplování
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
úbytek	úbytek	k1gInSc4	úbytek
sněhové	sněhový	k2eAgFnSc2d1	sněhová
pokrývky	pokrývka	k1gFnSc2	pokrývka
a	a	k8xC	a
mořského	mořský	k2eAgInSc2d1	mořský
ledu	led	k1gInSc2	led
<g/>
,	,	kIx,	,
změny	změna	k1gFnPc1	změna
atmosférických	atmosférický	k2eAgNnPc2d1	atmosférické
a	a	k8xC	a
oceánských	oceánský	k2eAgNnPc2d1	oceánské
proudění	proudění	k1gNnPc2	proudění
<g/>
,	,	kIx,	,
přítomnost	přítomnost	k1gFnSc1	přítomnost
antropogenních	antropogenní	k2eAgFnPc2d1	antropogenní
sazí	saze	k1gFnPc2	saze
v	v	k7c6	v
arktickém	arktický	k2eAgNnSc6d1	arktické
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
zvýšení	zvýšení	k1gNnSc4	zvýšení
oblačnosti	oblačnost	k1gFnSc2	oblačnost
a	a	k8xC	a
vodní	vodní	k2eAgInPc1d1	vodní
páry	pár	k1gInPc1	pár
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zprávy	zpráva	k1gFnSc2	zpráva
IPCC	IPCC	kA	IPCC
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
mají	mít	k5eAaImIp3nP	mít
modely	model	k1gInPc1	model
často	často	k6eAd1	často
tendenci	tendence	k1gFnSc4	tendence
podceňovat	podceňovat	k5eAaImF	podceňovat
Arktické	arktický	k2eAgNnSc4d1	arktické
zesílení	zesílení	k1gNnSc4	zesílení
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnPc1	studie
ukázaly	ukázat	k5eAaPmAgFnP	ukázat
souvislosti	souvislost	k1gFnPc4	souvislost
mezi	mezi	k7c7	mezi
oteplující	oteplující	k2eAgFnSc7d1	oteplující
se	se	k3xPyFc4	se
Arktidou	Arktida	k1gFnSc7	Arktida
s	s	k7c7	s
mizící	mizící	k2eAgFnSc7d1	mizící
kryosférou	kryosféra	k1gFnSc7	kryosféra
a	a	k8xC	a
výskyty	výskyt	k1gInPc1	výskyt
extrémního	extrémní	k2eAgNnSc2d1	extrémní
počasí	počasí	k1gNnSc2	počasí
ve	v	k7c6	v
středních	střední	k2eAgFnPc6d1	střední
šířkách	šířka	k1gFnPc6	šířka
a	a	k8xC	a
změnami	změna	k1gFnPc7	změna
tryskového	tryskový	k2eAgNnSc2d1	tryskové
proudění	proudění	k1gNnSc2	proudění
<g/>
.	.	kIx.	.
<g/>
Vodní	vodní	k2eAgFnPc1d1	vodní
páry	pára	k1gFnPc1	pára
mohou	moct	k5eAaImIp3nP	moct
přibývat	přibývat	k5eAaImF	přibývat
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
růst	růst	k1gInSc4	růst
antropogenního	antropogenní	k2eAgNnSc2d1	antropogenní
CO	co	k8xS	co
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
přírodní	přírodní	k2eAgNnSc4d1	přírodní
oteplování	oteplování	k1gNnSc4	oteplování
<g/>
.	.	kIx.	.
</s>
<s>
Vyšší	vysoký	k2eAgFnSc3d2	vyšší
hladině	hladina	k1gFnSc3	hladina
sluneční	sluneční	k2eAgFnSc2d1	sluneční
činnosti	činnost	k1gFnSc2	činnost
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
vyšší	vysoký	k2eAgFnSc1d2	vyšší
koncentrace	koncentrace	k1gFnSc1	koncentrace
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
růst	růst	k1gInSc1	růst
teplot	teplota	k1gFnPc2	teplota
zpomalil	zpomalit	k5eAaPmAgInS	zpomalit
<g/>
,	,	kIx,	,
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
poklesu	pokles	k1gInSc3	pokles
koncentrace	koncentrace	k1gFnSc2	koncentrace
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Stefanova-Boltzmannova	Stefanova-Boltzmannův	k2eAgInSc2d1	Stefanova-Boltzmannův
zákona	zákon	k1gInSc2	zákon
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
při	při	k7c6	při
nárůstu	nárůst	k1gInSc6	nárůst
teploty	teplota	k1gFnSc2	teplota
na	na	k7c4	na
dvojnásobek	dvojnásobek	k1gInSc4	dvojnásobek
se	se	k3xPyFc4	se
vyzářená	vyzářený	k2eAgFnSc1d1	vyzářená
energie	energie	k1gFnSc1	energie
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
šestnáctkrát	šestnáctkrát	k6eAd1	šestnáctkrát
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
při	při	k7c6	při
vzrůstu	vzrůst	k1gInSc6	vzrůst
teploty	teplota	k1gFnSc2	teplota
o	o	k7c4	o
1	[number]	k4	1
%	%	kIx~	%
vyzářená	vyzářený	k2eAgFnSc1d1	vyzářená
energie	energie	k1gFnSc1	energie
vzroste	vzrůst	k5eAaPmIp3nS	vzrůst
o	o	k7c4	o
přibližně	přibližně	k6eAd1	přibližně
4	[number]	k4	4
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
koncentrace	koncentrace	k1gFnSc2	koncentrace
vodních	vodní	k2eAgFnPc2d1	vodní
par	para	k1gFnPc2	para
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
je	být	k5eAaImIp3nS	být
reakcí	reakce	k1gFnSc7	reakce
na	na	k7c4	na
změny	změna	k1gFnPc4	změna
povrchového	povrchový	k2eAgNnSc2d1	povrchové
klimatu	klima	k1gNnSc2	klima
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
zpětnou	zpětný	k2eAgFnSc4d1	zpětná
vazbu	vazba	k1gFnSc4	vazba
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
za	za	k7c4	za
radiační	radiační	k2eAgNnSc4d1	radiační
působení	působení	k1gNnSc4	působení
<g/>
.	.	kIx.	.
</s>
<s>
Nelinearita	Nelinearita	k1gFnSc1	Nelinearita
této	tento	k3xDgFnSc2	tento
vazby	vazba	k1gFnSc2	vazba
a	a	k8xC	a
existence	existence	k1gFnSc2	existence
dalších	další	k2eAgFnPc2d1	další
<g/>
,	,	kIx,	,
negativních	negativní	k2eAgFnPc2d1	negativní
zpětných	zpětný	k2eAgFnPc2d1	zpětná
vazeb	vazba	k1gFnPc2	vazba
ale	ale	k8xC	ale
zajišťují	zajišťovat	k5eAaImIp3nP	zajišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
teplota	teplota	k1gFnSc1	teplota
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
při	při	k7c6	při
tomto	tento	k3xDgInSc6	tento
procesu	proces	k1gInSc6	proces
nemůže	moct	k5eNaImIp3nS	moct
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
lavinovitě	lavinovitě	k6eAd1	lavinovitě
a	a	k8xC	a
nemůže	moct	k5eNaImIp3nS	moct
samovolně	samovolně	k6eAd1	samovolně
narůst	narůst	k5eAaPmF	narůst
na	na	k7c4	na
libovolně	libovolně	k6eAd1	libovolně
vysoké	vysoký	k2eAgFnPc4d1	vysoká
hodnoty	hodnota	k1gFnPc4	hodnota
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnSc1d1	vodní
pára	pára	k1gFnSc1	pára
funguje	fungovat	k5eAaImIp3nS	fungovat
jako	jako	k9	jako
zesilovač	zesilovač	k1gInSc1	zesilovač
vlivu	vliv	k1gInSc2	vliv
ostatních	ostatní	k2eAgInPc2d1	ostatní
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Přímé	přímý	k2eAgFnPc1d1	přímá
emise	emise	k1gFnPc1	emise
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
při	při	k7c6	při
lidské	lidský	k2eAgFnSc6d1	lidská
činnosti	činnost	k1gFnSc6	činnost
přinášejí	přinášet	k5eAaImIp3nP	přinášet
zanedbatelný	zanedbatelný	k2eAgInSc4d1	zanedbatelný
příspěvek	příspěvek	k1gInSc4	příspěvek
k	k	k7c3	k
radiačnímu	radiační	k2eAgNnSc3d1	radiační
působení	působení	k1gNnSc3	působení
<g/>
.	.	kIx.	.
</s>
<s>
Emise	emise	k1gFnPc1	emise
vzniklé	vzniklý	k2eAgFnPc1d1	vzniklá
při	při	k7c6	při
zavlažování	zavlažování	k1gNnSc6	zavlažování
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
méně	málo	k6eAd2	málo
než	než	k8xS	než
1	[number]	k4	1
%	%	kIx~	%
přírodních	přírodní	k2eAgInPc2d1	přírodní
zdrojů	zdroj	k1gInPc2	zdroj
vodní	vodní	k2eAgFnSc2d1	vodní
páry	pára	k1gFnSc2	pára
<g/>
.	.	kIx.	.
</s>
<s>
Vypouštění	vypouštění	k1gNnSc1	vypouštění
páry	pára	k1gFnSc2	pára
při	při	k7c6	při
spalování	spalování	k1gNnSc6	spalování
fosilních	fosilní	k2eAgNnPc2d1	fosilní
paliv	palivo	k1gNnPc2	palivo
je	být	k5eAaImIp3nS	být
významně	významně	k6eAd1	významně
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
její	její	k3xOp3gFnSc1	její
emise	emise	k1gFnSc1	emise
při	při	k7c6	při
zemědělské	zemědělský	k2eAgFnSc6d1	zemědělská
činnosti	činnost	k1gFnSc6	činnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klimatické	klimatický	k2eAgInPc1d1	klimatický
modely	model	k1gInPc1	model
==	==	k?	==
</s>
</p>
<p>
<s>
Klimatický	klimatický	k2eAgInSc1d1	klimatický
model	model	k1gInSc1	model
představuje	představovat	k5eAaImIp3nS	představovat
fyzikální	fyzikální	k2eAgInSc1d1	fyzikální
<g/>
,	,	kIx,	,
chemické	chemický	k2eAgInPc1d1	chemický
a	a	k8xC	a
biologické	biologický	k2eAgInPc1d1	biologický
procesy	proces	k1gInPc1	proces
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
působí	působit	k5eAaImIp3nP	působit
na	na	k7c4	na
klimatický	klimatický	k2eAgInSc4d1	klimatický
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
<g/>
Přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
výzkumníci	výzkumník	k1gMnPc1	výzkumník
pokoušejí	pokoušet	k5eAaImIp3nP	pokoušet
zahrnout	zahrnout	k5eAaPmF	zahrnout
do	do	k7c2	do
modelů	model	k1gInPc2	model
co	co	k9	co
nejvíce	hodně	k6eAd3	hodně
procesů	proces	k1gInPc2	proces
<g/>
,	,	kIx,	,
zjednodušení	zjednodušení	k1gNnSc3	zjednodušení
skutečného	skutečný	k2eAgInSc2d1	skutečný
klimatického	klimatický	k2eAgInSc2d1	klimatický
systému	systém	k1gInSc2	systém
omezením	omezení	k1gNnSc7	omezení
výpočetním	výpočetní	k2eAgFnPc3d1	výpočetní
kapacitám	kapacita	k1gFnPc3	kapacita
a	a	k8xC	a
omezením	omezení	k1gNnSc7	omezení
znalostí	znalost	k1gFnSc7	znalost
klimatického	klimatický	k2eAgNnSc2d1	klimatické
systémuj	systémovat	k5eAaImRp2nS	systémovat
sou	sou	k1gInSc3	sou
nevyhnutelná	vyhnutelný	k2eNgFnSc1d1	nevyhnutelná
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
modelů	model	k1gInPc2	model
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
také	také	k9	také
lišit	lišit	k5eAaImF	lišit
díky	díky	k7c3	díky
různým	různý	k2eAgInPc3d1	různý
vstupům	vstup	k1gInPc3	vstup
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
a	a	k8xC	a
klimatické	klimatický	k2eAgFnPc4d1	klimatická
citlivosti	citlivost	k1gFnPc4	citlivost
modelu	model	k1gInSc2	model
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
nejistota	nejistota	k1gFnSc1	nejistota
v	v	k7c6	v
projekcích	projekce	k1gFnPc6	projekce
IPCC	IPCC	kA	IPCC
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
používáním	používání	k1gNnSc7	používání
více	hodně	k6eAd2	hodně
modelů	model	k1gInPc2	model
s	s	k7c7	s
rozdílnou	rozdílný	k2eAgFnSc7d1	rozdílná
citlivostí	citlivost	k1gFnSc7	citlivost
na	na	k7c4	na
koncentrace	koncentrace	k1gFnPc4	koncentrace
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
použitím	použití	k1gNnSc7	použití
rozdílných	rozdílný	k2eAgInPc2d1	rozdílný
odhadů	odhad	k1gInPc2	odhad
budoucích	budoucí	k2eAgFnPc2d1	budoucí
antropogenních	antropogenní	k2eAgFnPc2d1	antropogenní
emisí	emise	k1gFnPc2	emise
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
jakýmikoliv	jakýkoliv	k3yIgFnPc7	jakýkoliv
dalšími	další	k2eAgFnPc7d1	další
emisemi	emise	k1gFnPc7	emise
z	z	k7c2	z
klimatické	klimatický	k2eAgFnSc2d1	klimatická
zpětné	zpětný	k2eAgFnSc2d1	zpětná
vazby	vazba	k1gFnSc2	vazba
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
nebyly	být	k5eNaImAgInP	být
zahrnuty	zahrnout	k5eAaPmNgInP	zahrnout
do	do	k7c2	do
modelů	model	k1gInPc2	model
IPCC	IPCC	kA	IPCC
používaných	používaný	k2eAgInPc2d1	používaný
k	k	k7c3	k
přípravě	příprava	k1gFnSc3	příprava
zprávy	zpráva	k1gFnSc2	zpráva
<g/>
,	,	kIx,	,
např.	např.	kA	např.
uvolňování	uvolňování	k1gNnSc1	uvolňování
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
z	z	k7c2	z
permafrostu	permafrost	k1gInSc2	permafrost
<g/>
.	.	kIx.	.
<g/>
Modely	model	k1gInPc1	model
nepředpokládají	předpokládat	k5eNaImIp3nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
klima	klima	k1gNnSc1	klima
se	se	k3xPyFc4	se
zahřeje	zahřát	k5eAaPmIp3nS	zahřát
kvůli	kvůli	k7c3	kvůli
rostoucím	rostoucí	k2eAgFnPc3d1	rostoucí
koncentracím	koncentrace	k1gFnPc3	koncentrace
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Namísto	namísto	k7c2	namísto
toho	ten	k3xDgNnSc2	ten
předpovídají	předpovídat	k5eAaImIp3nP	předpovídat
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
budou	být	k5eAaImBp3nP	být
skleníkové	skleníkový	k2eAgInPc1d1	skleníkový
plyny	plyn	k1gInPc1	plyn
interagovat	interagovat	k5eAaImF	interagovat
s	s	k7c7	s
radiačním	radiační	k2eAgInSc7d1	radiační
přenosem	přenos	k1gInSc7	přenos
a	a	k8xC	a
jinými	jiný	k2eAgInPc7d1	jiný
fyzikálními	fyzikální	k2eAgInPc7d1	fyzikální
procesy	proces	k1gInPc7	proces
<g/>
.	.	kIx.	.
</s>
<s>
Ohřev	ohřev	k1gInSc1	ohřev
nebo	nebo	k8xC	nebo
chlazení	chlazení	k1gNnSc1	chlazení
jsou	být	k5eAaImIp3nP	být
tedy	tedy	k9	tedy
výsledkem	výsledek	k1gInSc7	výsledek
<g/>
,	,	kIx,	,
ne	ne	k9	ne
předpokladem	předpoklad	k1gInSc7	předpoklad
modelů	model	k1gInPc2	model
<g/>
.	.	kIx.	.
<g/>
Zvláště	zvláště	k6eAd1	zvláště
obtížné	obtížný	k2eAgNnSc1d1	obtížné
je	být	k5eAaImIp3nS	být
předvídat	předvídat	k5eAaImF	předvídat
oblačnost	oblačnost	k1gFnSc1	oblačnost
a	a	k8xC	a
její	její	k3xOp3gInPc1	její
účinky	účinek	k1gInPc1	účinek
<g/>
;	;	kIx,	;
jejich	jejich	k3xOp3gNnSc1	jejich
modelování	modelování	k1gNnSc1	modelování
je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgNnSc7d1	důležité
tématem	téma	k1gNnSc7	téma
výzkumu	výzkum	k1gInSc2	výzkum
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Zlepšení	zlepšení	k1gNnSc1	zlepšení
zastoupení	zastoupení	k1gNnSc1	zastoupení
modelů	model	k1gInPc2	model
mraků	mrak	k1gInPc2	mrak
je	být	k5eAaImIp3nS	být
proto	proto	k8xC	proto
důležitým	důležitý	k2eAgNnSc7d1	důležité
tématem	téma	k1gNnSc7	téma
v	v	k7c6	v
současném	současný	k2eAgInSc6d1	současný
výzkumu	výzkum	k1gInSc6	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
významným	významný	k2eAgNnSc7d1	významné
výzkumným	výzkumný	k2eAgNnSc7d1	výzkumné
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
rozšiřování	rozšiřování	k1gNnSc4	rozšiřování
a	a	k8xC	a
zlepšování	zlepšování	k1gNnSc4	zlepšování
reprezentace	reprezentace	k1gFnSc2	reprezentace
uhlíkového	uhlíkový	k2eAgInSc2d1	uhlíkový
cyklu	cyklus	k1gInSc2	cyklus
<g/>
.	.	kIx.	.
<g/>
Modely	model	k1gInPc1	model
jsou	být	k5eAaImIp3nP	být
také	také	k6eAd1	také
používány	používat	k5eAaImNgFnP	používat
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
pomohly	pomoct	k5eAaPmAgFnP	pomoct
zkoumat	zkoumat	k5eAaImF	zkoumat
příčiny	příčina	k1gFnPc1	příčina
klimatických	klimatický	k2eAgFnPc2d1	klimatická
změn	změna	k1gFnPc2	změna
v	v	k7c6	v
nedávné	dávný	k2eNgFnSc6d1	nedávná
minulosti	minulost	k1gFnSc6	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
pro	pro	k7c4	pro
období	období	k1gNnSc4	období
1910	[number]	k4	1910
<g/>
–	–	k?	–
<g/>
1945	[number]	k4	1945
modely	model	k1gInPc7	model
neurčují	určovat	k5eNaImIp3nP	určovat
jednoznačně	jednoznačně	k6eAd1	jednoznačně
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
změny	změna	k1gFnPc4	změna
přírodní	přírodní	k2eAgFnPc4d1	přírodní
či	či	k8xC	či
antropogenní	antropogenní	k2eAgFnPc4d1	antropogenní
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
oteplování	oteplování	k1gNnSc4	oteplování
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1970	[number]	k4	1970
dominují	dominovat	k5eAaImIp3nP	dominovat
jako	jako	k8xS	jako
příčina	příčina	k1gFnSc1	příčina
jasně	jasně	k6eAd1	jasně
antropogenní	antropogenní	k2eAgFnSc1d1	antropogenní
emise	emise	k1gFnSc1	emise
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
<g/>
Správnost	správnost	k1gFnSc1	správnost
modelů	model	k1gInPc2	model
je	být	k5eAaImIp3nS	být
testována	testovat	k5eAaImNgFnS	testovat
zkoumáním	zkoumání	k1gNnSc7	zkoumání
jejich	jejich	k3xOp3gFnSc2	jejich
schopnosti	schopnost	k1gFnSc2	schopnost
simulovat	simulovat	k5eAaImF	simulovat
současné	současný	k2eAgNnSc4d1	současné
nebo	nebo	k8xC	nebo
minulé	minulý	k2eAgNnSc4d1	Minulé
klima	klima	k1gNnSc4	klima
<g/>
.	.	kIx.	.
</s>
<s>
Klimatické	klimatický	k2eAgInPc1d1	klimatický
modely	model	k1gInPc1	model
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
relativně	relativně	k6eAd1	relativně
dobře	dobře	k6eAd1	dobře
shodují	shodovat	k5eAaImIp3nP	shodovat
s	s	k7c7	s
pozorovanými	pozorovaný	k2eAgInPc7d1	pozorovaný
průměry	průměr	k1gInPc7	průměr
globálních	globální	k2eAgFnPc2d1	globální
teplotních	teplotní	k2eAgFnPc2d1	teplotní
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nesimulují	simulovat	k5eNaImIp3nP	simulovat
správně	správně	k6eAd1	správně
všechny	všechen	k3xTgInPc4	všechen
aspekty	aspekt	k1gInPc4	aspekt
klimatu	klima	k1gNnSc2	klima
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
úbytek	úbytek	k1gInSc1	úbytek
ledu	led	k1gInSc2	led
v	v	k7c6	v
Antarktidě	Antarktida	k1gFnSc6	Antarktida
byl	být	k5eAaImAgMnS	být
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
<g/>
,	,	kIx,	,
než	než	k8xS	než
bylo	být	k5eAaImAgNnS	být
předpovídáno	předpovídat	k5eAaImNgNnS	předpovídat
<g/>
.	.	kIx.	.
</s>
<s>
Srážky	srážka	k1gFnPc1	srážka
vzrostly	vzrůst	k5eAaPmAgFnP	vzrůst
proporcionálně	proporcionálně	k6eAd1	proporcionálně
s	s	k7c7	s
atmosférickou	atmosférický	k2eAgFnSc7d1	atmosférická
vlhkostí	vlhkost	k1gFnSc7	vlhkost
<g/>
,	,	kIx,	,
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
výrazně	výrazně	k6eAd1	výrazně
rychleji	rychle	k6eAd2	rychle
<g/>
,	,	kIx,	,
než	než	k8xS	než
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
globální	globální	k2eAgInPc4d1	globální
klimatické	klimatický	k2eAgInPc4d1	klimatický
modely	model	k1gInPc4	model
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
také	také	k9	také
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
hladina	hladina	k1gFnSc1	hladina
moře	moře	k1gNnSc2	moře
podstatně	podstatně	k6eAd1	podstatně
rychleji	rychle	k6eAd2	rychle
<g/>
,	,	kIx,	,
než	než	k8xS	než
předpokládaly	předpokládat	k5eAaImAgInP	předpokládat
modely	model	k1gInPc1	model
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
zveřejnily	zveřejnit	k5eAaPmAgFnP	zveřejnit
národní	národní	k2eAgNnSc4d1	národní
hodnocení	hodnocení	k1gNnSc4	hodnocení
klimatu	klima	k1gNnSc2	klima
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
klimatické	klimatický	k2eAgInPc1d1	klimatický
modely	model	k1gInPc1	model
stále	stále	k6eAd1	stále
podceňují	podceňovat	k5eAaImIp3nP	podceňovat
vývoj	vývoj	k1gInSc4	vývoj
oteplování	oteplování	k1gNnSc2	oteplování
nebo	nebo	k8xC	nebo
chybí	chybit	k5eAaPmIp3nS	chybit
příslušné	příslušný	k2eAgInPc4d1	příslušný
procesy	proces	k1gInPc4	proces
zpětné	zpětný	k2eAgFnSc2d1	zpětná
vazby	vazba	k1gFnSc2	vazba
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Pro	pro	k7c4	pro
předpověď	předpověď	k1gFnSc4	předpověď
budoucího	budoucí	k2eAgInSc2d1	budoucí
vývoje	vývoj	k1gInSc2	vývoj
globálního	globální	k2eAgNnSc2d1	globální
oteplování	oteplování	k1gNnSc2	oteplování
používají	používat	k5eAaImIp3nP	používat
vědci	vědec	k1gMnPc1	vědec
hierarchickou	hierarchický	k2eAgFnSc4d1	hierarchická
řadu	řada	k1gFnSc4	řada
klimatických	klimatický	k2eAgInPc2d1	klimatický
modelů	model	k1gInPc2	model
od	od	k7c2	od
jednoduchých	jednoduchý	k2eAgFnPc2d1	jednoduchá
přes	přes	k7c4	přes
středně	středně	k6eAd1	středně
složité	složitý	k2eAgInPc4d1	složitý
až	až	k9	až
po	po	k7c4	po
komplexní	komplexní	k2eAgInPc4d1	komplexní
klimatické	klimatický	k2eAgInPc4d1	klimatický
modely	model	k1gInPc4	model
a	a	k8xC	a
modely	model	k1gInPc4	model
systému	systém	k1gInSc2	systém
Země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
Earth	Earth	k1gInSc4	Earth
System	Systo	k1gNnSc7	Systo
models	modelsa	k1gFnPc2	modelsa
<g/>
,	,	kIx,	,
ESM	ESM	kA	ESM
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
tyto	tento	k3xDgInPc1	tento
modely	model	k1gInPc1	model
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
simulovat	simulovat	k5eAaImF	simulovat
budoucí	budoucí	k2eAgFnPc4d1	budoucí
změny	změna	k1gFnPc4	změna
klimatu	klima	k1gNnSc2	klima
na	na	k7c6	na
základě	základ	k1gInSc6	základ
různých	různý	k2eAgInPc2d1	různý
scénářů	scénář	k1gInPc2	scénář
antropogenního	antropogenní	k2eAgInSc2d1	antropogenní
vlivu	vliv	k1gInSc2	vliv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
simulacích	simulace	k1gFnPc6	simulace
pro	pro	k7c4	pro
Pátou	pátá	k1gFnSc4	pátá
hodnotící	hodnotící	k2eAgFnSc4d1	hodnotící
zprávu	zpráva	k1gFnSc4	zpráva
IPCC	IPCC	kA	IPCC
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
projektu	projekt	k1gInSc2	projekt
CMIP5	CMIP5	k1gFnSc2	CMIP5
(	(	kIx(	(
<g/>
Coupled	Coupled	k1gInSc1	Coupled
Model	model	k1gInSc1	model
Intercomparison	Intercomparison	k1gInSc1	Intercomparison
Project	Project	k1gInSc1	Project
Phase	Phase	k1gFnSc1	Phase
5	[number]	k4	5
<g/>
)	)	kIx)	)
Světového	světový	k2eAgInSc2d1	světový
programu	program	k1gInSc2	program
výzkumu	výzkum	k1gInSc2	výzkum
klimatu	klima	k1gNnSc2	klima
(	(	kIx(	(
<g/>
WCRP	WCRP	kA	WCRP
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
scénáře	scénář	k1gInPc1	scénář
nově	nově	k6eAd1	nově
využity	využít	k5eAaPmNgInP	využít
tzv.	tzv.	kA	tzv.
reprezentativní	reprezentativní	k2eAgInPc1d1	reprezentativní
směry	směr	k1gInPc1	směr
vývoje	vývoj	k1gInSc2	vývoj
koncentrací	koncentrace	k1gFnSc7	koncentrace
(	(	kIx(	(
<g/>
RCP	RCP	kA	RCP
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Modely	model	k1gInPc1	model
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
počítají	počítat	k5eAaImIp3nP	počítat
s	s	k7c7	s
antropogenními	antropogenní	k2eAgInPc7d1	antropogenní
i	i	k8xC	i
přírodními	přírodní	k2eAgInPc7d1	přírodní
vlivy	vliv	k1gInPc7	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
antropogenní	antropogenní	k2eAgInPc4d1	antropogenní
vlivy	vliv	k1gInPc4	vliv
jsou	být	k5eAaImIp3nP	být
započítávány	započítáván	k2eAgFnPc4d1	započítávána
změny	změna	k1gFnPc4	změna
koncentrací	koncentrace	k1gFnSc7	koncentrace
plynů	plyn	k1gInPc2	plyn
s	s	k7c7	s
dlouhou	dlouhý	k2eAgFnSc7d1	dlouhá
životností	životnost	k1gFnSc7	životnost
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
(	(	kIx(	(
<g/>
CO	co	k6eAd1	co
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
CH	Ch	kA	Ch
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
halogenovaných	halogenovaný	k2eAgInPc2d1	halogenovaný
uhlovodíků	uhlovodík	k1gInPc2	uhlovodík
a	a	k8xC	a
N	N	kA	N
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plynů	plyn	k1gInPc2	plyn
s	s	k7c7	s
krátkou	krátký	k2eAgFnSc7d1	krátká
životností	životnost	k1gFnSc7	životnost
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
(	(	kIx(	(
<g/>
CO	co	k3yRnSc4	co
<g/>
,	,	kIx,	,
NMVOC	NMVOC	kA	NMVOC
a	a	k8xC	a
NOx	noxa	k1gFnPc2	noxa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aerosolů	aerosol	k1gInPc2	aerosol
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc2	jejich
prekurzorů	prekurzor	k1gInPc2	prekurzor
<g/>
,	,	kIx,	,
změn	změna	k1gFnPc2	změna
oblačnosti	oblačnost	k1gFnSc2	oblačnost
vlivem	vliv	k1gInSc7	vliv
aerosolů	aerosol	k1gInPc2	aerosol
a	a	k8xC	a
změn	změna	k1gFnPc2	změna
albeda	albeda	k1gMnSc1	albeda
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
změn	změna	k1gFnPc2	změna
využití	využití	k1gNnSc2	využití
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
přírodní	přírodní	k2eAgInPc4d1	přírodní
vlivy	vliv	k1gInPc4	vliv
jsou	být	k5eAaImIp3nP	být
započítány	započítán	k2eAgFnPc4d1	započítána
změny	změna	k1gFnPc4	změna
příkonu	příkon	k1gInSc2	příkon
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
spolehlivost	spolehlivost	k1gFnSc1	spolehlivost
určení	určení	k1gNnSc2	určení
vlivu	vliv	k1gInSc2	vliv
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
a	a	k8xC	a
aerosolů	aerosol	k1gInPc2	aerosol
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
modelech	model	k1gInPc6	model
vysoká	vysoká	k1gFnSc1	vysoká
<g/>
,	,	kIx,	,
až	až	k9	až
velmi	velmi	k6eAd1	velmi
vysoká	vysoký	k2eAgFnSc1d1	vysoká
<g/>
,	,	kIx,	,
vlivy	vliv	k1gInPc7	vliv
plynů	plyn	k1gInPc2	plyn
s	s	k7c7	s
krátkou	krátký	k2eAgFnSc7d1	krátká
životností	životnost	k1gFnSc7	životnost
<g/>
,	,	kIx,	,
vliv	vliv	k1gInSc1	vliv
změn	změna	k1gFnPc2	změna
albeda	albeda	k1gMnSc1	albeda
a	a	k8xC	a
změn	změna	k1gFnPc2	změna
v	v	k7c6	v
příkonu	příkon	k1gInSc6	příkon
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
modelech	model	k1gInPc6	model
určena	určen	k2eAgFnSc1d1	určena
se	s	k7c7	s
střední	střední	k2eAgFnSc7d1	střední
spolehlivostí	spolehlivost	k1gFnSc7	spolehlivost
<g/>
,	,	kIx,	,
nejméně	málo	k6eAd3	málo
spolehlivé	spolehlivý	k2eAgInPc1d1	spolehlivý
v	v	k7c6	v
modelech	model	k1gInPc6	model
je	být	k5eAaImIp3nS	být
určení	určení	k1gNnSc1	určení
vlivů	vliv	k1gInPc2	vliv
změn	změna	k1gFnPc2	změna
oblačnosti	oblačnost	k1gFnSc2	oblačnost
vlivem	vlivem	k7c2	vlivem
aerosolů	aerosol	k1gInPc2	aerosol
<g/>
.	.	kIx.	.
<g/>
Modelování	modelování	k1gNnSc1	modelování
podle	podle	k7c2	podle
všech	všecek	k3xTgInPc2	všecek
scénářů	scénář	k1gInPc2	scénář
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
další	další	k2eAgFnPc4d1	další
emise	emise	k1gFnPc4	emise
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
způsobí	způsobit	k5eAaPmIp3nS	způsobit
další	další	k2eAgNnSc4d1	další
oteplení	oteplení	k1gNnSc4	oteplení
a	a	k8xC	a
změny	změna	k1gFnPc1	změna
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
složkách	složka	k1gFnPc6	složka
klimatického	klimatický	k2eAgInSc2d1	klimatický
systému	systém	k1gInSc2	systém
<g/>
.	.	kIx.	.
</s>
<s>
Omezení	omezení	k1gNnSc1	omezení
klimatické	klimatický	k2eAgFnSc2d1	klimatická
změny	změna	k1gFnSc2	změna
bude	být	k5eAaImBp3nS	být
vyžadovat	vyžadovat	k5eAaImF	vyžadovat
podstatné	podstatný	k2eAgNnSc1d1	podstatné
a	a	k8xC	a
trvalé	trvalý	k2eAgNnSc1d1	trvalé
snižování	snižování	k1gNnSc1	snižování
emisí	emise	k1gFnPc2	emise
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
Modelování	modelování	k1gNnSc1	modelování
vývoje	vývoj	k1gInSc2	vývoj
klimatu	klima	k1gNnSc2	klima
do	do	k7c2	do
konce	konec	k1gInSc2	konec
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
předpovídá	předpovídat	k5eAaImIp3nS	předpovídat
nárůsty	nárůst	k1gInPc4	nárůst
průměrných	průměrný	k2eAgFnPc2d1	průměrná
globálních	globální	k2eAgFnPc2d1	globální
teplot	teplota	k1gFnPc2	teplota
při	při	k7c6	při
povrchu	povrch	k1gInSc6	povrch
a	a	k8xC	a
vzestup	vzestup	k1gInSc1	vzestup
hladiny	hladina	k1gFnSc2	hladina
moří	mořit	k5eAaImIp3nS	mořit
podle	podle	k7c2	podle
různých	různý	k2eAgInPc2d1	různý
scénářů	scénář	k1gInPc2	scénář
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
scénáře	scénář	k1gInPc1	scénář
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
změny	změna	k1gFnPc4	změna
proti	proti	k7c3	proti
průměru	průměr	k1gInSc3	průměr
let	léto	k1gNnPc2	léto
1986	[number]	k4	1986
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
scénáře	scénář	k1gInSc2	scénář
RCP	RCP	kA	RCP
2.6	[number]	k4	2.6
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
počítá	počítat	k5eAaImIp3nS	počítat
s	s	k7c7	s
prakticky	prakticky	k6eAd1	prakticky
okamžitým	okamžitý	k2eAgNnSc7d1	okamžité
výrazným	výrazný	k2eAgNnSc7d1	výrazné
snižováním	snižování	k1gNnSc7	snižování
produkce	produkce	k1gFnSc2	produkce
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
průměrné	průměrný	k2eAgFnPc1d1	průměrná
teploty	teplota	k1gFnPc1	teplota
v	v	k7c6	v
letech	let	k1gInPc6	let
2046	[number]	k4	2046
<g/>
–	–	k?	–
<g/>
2065	[number]	k4	2065
narůst	narůst	k5eAaPmF	narůst
o	o	k7c4	o
1,0	[number]	k4	1,0
(	(	kIx(	(
<g/>
0,4	[number]	k4	0,4
až	až	k9	až
1,6	[number]	k4	1,6
<g/>
)	)	kIx)	)
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
2081	[number]	k4	2081
<g/>
–	–	k?	–
<g/>
<g />
.	.	kIx.	.
</s>
<s>
2100	[number]	k4	2100
pak	pak	k6eAd1	pak
už	už	k6eAd1	už
nepředpokládá	předpokládat	k5eNaImIp3nS	předpokládat
další	další	k2eAgInSc4d1	další
růst	růst	k1gInSc4	růst
teplot	teplota	k1gFnPc2	teplota
–	–	k?	–
1,0	[number]	k4	1,0
(	(	kIx(	(
<g/>
0,3	[number]	k4	0,3
až	až	k9	až
1,7	[number]	k4	1,7
<g/>
)	)	kIx)	)
<g/>
°	°	k?	°
<g/>
C.	C.	kA	C.
V	v	k7c6	v
případě	případ	k1gInSc6	případ
vzestupu	vzestup	k1gInSc2	vzestup
hladiny	hladina	k1gFnSc2	hladina
moří	moře	k1gNnPc2	moře
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
tento	tento	k3xDgInSc4	tento
scénář	scénář	k1gInSc4	scénář
vzestup	vzestup	k1gInSc1	vzestup
o	o	k7c4	o
0,24	[number]	k4	0,24
(	(	kIx(	(
<g/>
0,17	[number]	k4	0,17
až	až	k9	až
0,32	[number]	k4	0,32
<g/>
)	)	kIx)	)
m	m	kA	m
v	v	k7c6	v
letech	let	k1gInPc6	let
2046	[number]	k4	2046
<g/>
–	–	k?	–
<g/>
2065	[number]	k4	2065
a	a	k8xC	a
0,4	[number]	k4	0,4
(	(	kIx(	(
<g/>
0,26	[number]	k4	0,26
až	až	k9	až
0,55	[number]	k4	0,55
<g/>
)	)	kIx)	)
m	m	kA	m
v	v	k7c6	v
letech	let	k1gInPc6	let
2081	[number]	k4	2081
<g/>
–	–	k?	–
<g/>
2100	[number]	k4	2100
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
scénáře	scénář	k1gInSc2	scénář
RCP	RCP	kA	RCP
8.5	[number]	k4	8.5
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
počítá	počítat	k5eAaImIp3nS	počítat
s	s	k7c7	s
produkcí	produkce	k1gFnSc7	produkce
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
prakticky	prakticky	k6eAd1	prakticky
bez	bez	k7c2	bez
omezení	omezení	k1gNnSc2	omezení
<g/>
,	,	kIx,	,
by	by	kYmCp3nP	by
měly	mít	k5eAaImAgFnP	mít
průměrné	průměrný	k2eAgFnPc1d1	průměrná
teploty	teplota	k1gFnPc1	teplota
v	v	k7c6	v
letech	let	k1gInPc6	let
2046	[number]	k4	2046
<g/>
–	–	k?	–
<g/>
2065	[number]	k4	2065
narůst	narůst	k5eAaPmF	narůst
o	o	k7c4	o
2,0	[number]	k4	2,0
(	(	kIx(	(
<g/>
1,4	[number]	k4	1,4
až	až	k9	až
2,6	[number]	k4	2,6
<g/>
)	)	kIx)	)
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
v	v	k7c6	v
letech	let	k1gInPc6	let
2081	[number]	k4	2081
<g/>
–	–	k?	–
<g/>
2100	[number]	k4	2100
pak	pak	k6eAd1	pak
o	o	k7c4	o
3,7	[number]	k4	3,7
(	(	kIx(	(
<g/>
2,6	[number]	k4	2,6
až	až	k9	až
4,8	[number]	k4	4,8
<g/>
)	)	kIx)	)
°	°	k?	°
<g/>
C	C	kA	C
proti	proti	k7c3	proti
současným	současný	k2eAgFnPc3d1	současná
teplotám	teplota	k1gFnPc3	teplota
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
vzestupu	vzestup	k1gInSc2	vzestup
hladiny	hladina	k1gFnSc2	hladina
moří	moře	k1gNnPc2	moře
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
tento	tento	k3xDgInSc4	tento
scénář	scénář	k1gInSc4	scénář
vzestup	vzestup	k1gInSc1	vzestup
o	o	k7c4	o
0,30	[number]	k4	0,30
(	(	kIx(	(
<g/>
0,22	[number]	k4	0,22
až	až	k9	až
0,38	[number]	k4	0,38
<g/>
)	)	kIx)	)
m	m	kA	m
v	v	k7c6	v
letech	let	k1gInPc6	let
2046	[number]	k4	2046
<g/>
–	–	k?	–
<g/>
2065	[number]	k4	2065
a	a	k8xC	a
0,63	[number]	k4	0,63
(	(	kIx(	(
<g/>
0,45	[number]	k4	0,45
až	až	k9	až
0,82	[number]	k4	0,82
<g/>
)	)	kIx)	)
m	m	kA	m
v	v	k7c6	v
letech	let	k1gInPc6	let
2081	[number]	k4	2081
<g/>
–	–	k?	–
<g/>
2100	[number]	k4	2100
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
modelů	model	k1gInPc2	model
v	v	k7c6	v
IPCC	IPCC	kA	IPCC
AR5	AR5	k1gFnSc2	AR5
bude	být	k5eAaImBp3nS	být
oteplování	oteplování	k1gNnSc1	oteplování
nadále	nadále	k6eAd1	nadále
vykazovat	vykazovat	k5eAaImF	vykazovat
variabilitu	variabilita	k1gFnSc4	variabilita
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
roky	rok	k1gInPc7	rok
a	a	k8xC	a
dekádami	dekáda	k1gFnPc7	dekáda
a	a	k8xC	a
nebude	být	k5eNaImBp3nS	být
stejné	stejné	k1gNnSc1	stejné
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Modely	model	k1gInPc1	model
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
zvyšování	zvyšování	k1gNnSc4	zvyšování
rozdílů	rozdíl	k1gInPc2	rozdíl
srážkových	srážkový	k2eAgInPc2d1	srážkový
úhrnů	úhrn	k1gInPc2	úhrn
mezi	mezi	k7c7	mezi
vlhkými	vlhký	k2eAgFnPc7d1	vlhká
a	a	k8xC	a
suchými	suchý	k2eAgFnPc7d1	suchá
oblastmi	oblast	k1gFnPc7	oblast
a	a	k8xC	a
mezi	mezi	k7c7	mezi
suchými	suchý	k2eAgNnPc7d1	suché
a	a	k8xC	a
vlhkými	vlhký	k2eAgNnPc7d1	vlhké
obdobími	období	k1gNnPc7	období
s	s	k7c7	s
regionálními	regionální	k2eAgFnPc7d1	regionální
výjimkami	výjimka	k1gFnPc7	výjimka
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
také	také	k9	také
<g/>
,	,	kIx,	,
že	že	k8xS	že
teplo	teplo	k1gNnSc1	teplo
v	v	k7c6	v
oceánech	oceán	k1gInPc6	oceán
bude	být	k5eAaImBp3nS	být
pronikat	pronikat	k5eAaImF	pronikat
z	z	k7c2	z
povrchu	povrch	k1gInSc2	povrch
do	do	k7c2	do
hlubokých	hluboký	k2eAgFnPc2d1	hluboká
vrstev	vrstva	k1gFnPc2	vrstva
oceánu	oceán	k1gInSc2	oceán
a	a	k8xC	a
ovlivní	ovlivnit	k5eAaPmIp3nS	ovlivnit
cirkulaci	cirkulace	k1gFnSc4	cirkulace
vody	voda	k1gFnSc2	voda
v	v	k7c6	v
oceánu	oceán	k1gInSc6	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Bude	být	k5eAaImBp3nS	být
pokračovat	pokračovat	k5eAaImF	pokračovat
acidifikace	acidifikace	k1gFnSc1	acidifikace
oceánů	oceán	k1gInPc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Dojde	dojít	k5eAaPmIp3nS	dojít
také	také	k9	také
k	k	k7c3	k
pokračujícímu	pokračující	k2eAgNnSc3d1	pokračující
tání	tání	k1gNnSc3	tání
ledovců	ledovec	k1gInPc2	ledovec
–	–	k?	–
globální	globální	k2eAgInSc1d1	globální
objem	objem	k1gInSc1	objem
ledovců	ledovec	k1gInPc2	ledovec
bude	být	k5eAaImBp3nS	být
nadále	nadále	k6eAd1	nadále
klesat	klesat	k5eAaImF	klesat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
historických	historický	k2eAgNnPc2d1	historické
dat	datum	k1gNnPc2	datum
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
pozorován	pozorovat	k5eAaImNgInS	pozorovat
pokles	pokles	k1gInSc1	pokles
variability	variabilita	k1gFnSc2	variabilita
klimatu	klima	k1gNnSc2	klima
s	s	k7c7	s
růstem	růst	k1gInSc7	růst
teplot	teplota	k1gFnPc2	teplota
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
i	i	k9	i
jistým	jistý	k2eAgInPc3d1	jistý
modelům	model	k1gInPc3	model
<g/>
.	.	kIx.	.
</s>
<s>
Klimatická	klimatický	k2eAgFnSc1d1	klimatická
změna	změna	k1gFnSc1	změna
způsobená	způsobený	k2eAgFnSc1d1	způsobená
člověkem	člověk	k1gMnSc7	člověk
tak	tak	k9	tak
může	moct	k5eAaImIp3nS	moct
odvrátit	odvrátit	k5eAaPmF	odvrátit
následující	následující	k2eAgFnSc4d1	následující
dobu	doba	k1gFnSc4	doba
ledovou	ledový	k2eAgFnSc4d1	ledová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dopady	dopad	k1gInPc4	dopad
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
desetiletích	desetiletí	k1gNnPc6	desetiletí
způsobily	způsobit	k5eAaPmAgFnP	způsobit
změny	změna	k1gFnPc4	změna
klimatu	klima	k1gNnSc2	klima
změny	změna	k1gFnSc2	změna
v	v	k7c6	v
přírodních	přírodní	k2eAgInPc6d1	přírodní
i	i	k8xC	i
antropogenních	antropogenní	k2eAgInPc6d1	antropogenní
systémech	systém	k1gInPc6	systém
napříč	napříč	k7c7	napříč
všemi	všecek	k3xTgInPc7	všecek
kontinenty	kontinent	k1gInPc7	kontinent
i	i	k8xC	i
oceány	oceán	k1gInPc7	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Důkazy	důkaz	k1gInPc4	důkaz
dopadů	dopad	k1gInPc2	dopad
změny	změna	k1gFnSc2	změna
klimatu	klima	k1gNnSc2	klima
jsou	být	k5eAaImIp3nP	být
nejsilnější	silný	k2eAgNnSc4d3	nejsilnější
a	a	k8xC	a
nejrozsáhlejší	rozsáhlý	k2eAgNnSc4d3	nejrozsáhlejší
pro	pro	k7c4	pro
přírodní	přírodní	k2eAgInPc4d1	přírodní
systémy	systém	k1gInPc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
antropogenních	antropogenní	k2eAgInPc2d1	antropogenní
systémů	systém	k1gInPc2	systém
lze	lze	k6eAd1	lze
také	také	k9	také
pozorovat	pozorovat	k5eAaImF	pozorovat
některé	některý	k3yIgInPc4	některý
vlivy	vliv	k1gInPc4	vliv
<g/>
,	,	kIx,	,
postupně	postupně	k6eAd1	postupně
získáváme	získávat	k5eAaImIp1nP	získávat
znalosti	znalost	k1gFnPc4	znalost
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
změny	změna	k1gFnPc1	změna
tohoto	tento	k3xDgInSc2	tento
systému	systém	k1gInSc2	systém
jsou	být	k5eAaImIp3nP	být
skutečně	skutečně	k6eAd1	skutečně
způsobeny	způsobit	k5eAaPmNgFnP	způsobit
změnou	změna	k1gFnSc7	změna
klimatu	klima	k1gNnSc2	klima
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
největší	veliký	k2eAgFnSc6d3	veliký
míře	míra	k1gFnSc6	míra
jsou	být	k5eAaImIp3nP	být
ovlivňovány	ovlivňován	k2eAgInPc1d1	ovlivňován
ekosystémy	ekosystém	k1gInPc1	ekosystém
pevnin	pevnina	k1gFnPc2	pevnina
a	a	k8xC	a
oceánů	oceán	k1gInPc2	oceán
<g/>
,	,	kIx,	,
pobřežní	pobřežní	k2eAgInPc1d1	pobřežní
systémy	systém	k1gInPc1	systém
<g/>
,	,	kIx,	,
vodní	vodní	k2eAgInPc1d1	vodní
zdroje	zdroj	k1gInPc1	zdroj
a	a	k8xC	a
kryosféra	kryosféra	k1gFnSc1	kryosféra
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
oblastech	oblast	k1gFnPc6	oblast
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
srážkové	srážkový	k2eAgFnSc2d1	srážková
bilance	bilance	k1gFnSc2	bilance
nebo	nebo	k8xC	nebo
tání	tání	k1gNnSc2	tání
sněhu	sníh	k1gInSc2	sníh
a	a	k8xC	a
ledu	led	k1gInSc2	led
a	a	k8xC	a
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
hydrologických	hydrologický	k2eAgMnPc2d1	hydrologický
systémů	systém	k1gInPc2	systém
a	a	k8xC	a
ovlivnění	ovlivnění	k1gNnSc1	ovlivnění
kvalitativních	kvalitativní	k2eAgInPc2d1	kvalitativní
i	i	k8xC	i
kvantitativních	kvantitativní	k2eAgInPc2d1	kvantitativní
parametrů	parametr	k1gInPc2	parametr
vodních	vodní	k2eAgInPc2d1	vodní
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
výraznému	výrazný	k2eAgInSc3d1	výrazný
úbytku	úbytek	k1gInSc3	úbytek
ledovců	ledovec	k1gInPc2	ledovec
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
je	být	k5eAaImIp3nS	být
ovlivňován	ovlivňován	k2eAgInSc1d1	ovlivňován
odtok	odtok	k1gInSc1	odtok
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
a	a	k8xC	a
navazující	navazující	k2eAgInPc1d1	navazující
vodní	vodní	k2eAgInPc1d1	vodní
zdroje	zdroj	k1gInPc1	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
klimatu	klima	k1gNnSc2	klima
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
oteplování	oteplování	k1gNnSc4	oteplování
a	a	k8xC	a
rozmrazování	rozmrazování	k1gNnSc4	rozmrazování
permafrostu	permafrost	k1gInSc2	permafrost
v	v	k7c6	v
regionech	region	k1gInPc6	region
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
pólů	pól	k1gInPc2	pól
a	a	k8xC	a
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
s	s	k7c7	s
vysokými	vysoký	k2eAgFnPc7d1	vysoká
nadmořskými	nadmořský	k2eAgFnPc7d1	nadmořská
výškami	výška	k1gFnPc7	výška
<g/>
.	.	kIx.	.
<g/>
Mnoho	mnoho	k6eAd1	mnoho
suchozemských	suchozemský	k2eAgInPc2d1	suchozemský
<g/>
,	,	kIx,	,
sladkovodních	sladkovodní	k2eAgInPc2d1	sladkovodní
a	a	k8xC	a
mořských	mořský	k2eAgInPc2d1	mořský
druhů	druh	k1gInPc2	druh
živočichů	živočich	k1gMnPc2	živočich
změnilo	změnit	k5eAaPmAgNnS	změnit
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
klimatické	klimatický	k2eAgFnPc4d1	klimatická
změny	změna	k1gFnPc4	změna
své	svůj	k3xOyFgNnSc4	svůj
geografické	geografický	k2eAgNnSc4d1	geografické
působiště	působiště	k1gNnSc4	působiště
<g/>
,	,	kIx,	,
sezónní	sezónní	k2eAgFnPc4d1	sezónní
aktivity	aktivita	k1gFnPc4	aktivita
<g/>
,	,	kIx,	,
migrační	migrační	k2eAgInPc4d1	migrační
modely	model	k1gInPc4	model
<g/>
,	,	kIx,	,
hojnosti	hojnost	k1gFnPc4	hojnost
výskytu	výskyt	k1gInSc2	výskyt
a	a	k8xC	a
také	také	k9	také
druhové	druhový	k2eAgFnPc4d1	druhová
interakce	interakce	k1gFnPc4	interakce
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
zatím	zatím	k6eAd1	zatím
je	být	k5eAaImIp3nS	být
změně	změna	k1gFnSc3	změna
klimatu	klima	k1gNnSc2	klima
přisuzováno	přisuzován	k2eAgNnSc4d1	přisuzováno
jen	jen	k6eAd1	jen
malé	malý	k2eAgNnSc4d1	malé
množství	množství	k1gNnSc4	množství
zániku	zánik	k1gInSc2	zánik
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
z	z	k7c2	z
historie	historie	k1gFnSc2	historie
(	(	kIx(	(
<g/>
v	v	k7c6	v
řádu	řád	k1gInSc6	řád
tisíců	tisíc	k4xCgInPc2	tisíc
až	až	k9	až
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
víme	vědět	k5eAaImIp1nP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
výrazně	výrazně	k6eAd1	výrazně
menší	malý	k2eAgFnPc4d2	menší
změny	změna	k1gFnPc4	změna
klimatu	klima	k1gNnSc2	klima
<g/>
,	,	kIx,	,
než	než	k8xS	než
jsou	být	k5eAaImIp3nP	být
současné	současný	k2eAgFnPc4d1	současná
antropogenní	antropogenní	k2eAgFnPc4d1	antropogenní
změny	změna	k1gFnPc4	změna
<g/>
,	,	kIx,	,
způsobily	způsobit	k5eAaPmAgInP	způsobit
významné	významný	k2eAgInPc1d1	významný
posuny	posun	k1gInPc1	posun
v	v	k7c6	v
ekosystémech	ekosystém	k1gInPc6	ekosystém
a	a	k8xC	a
významná	významný	k2eAgNnPc4d1	významné
vymírání	vymírání	k1gNnPc4	vymírání
druhů	druh	k1gInPc2	druh
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
mnoha	mnoho	k4c2	mnoho
studií	studio	k1gNnPc2	studio
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
celou	celý	k2eAgFnSc4d1	celá
řadu	řada	k1gFnSc4	řada
regionů	region	k1gInPc2	region
a	a	k8xC	a
plodin	plodina	k1gFnPc2	plodina
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
negativní	negativní	k2eAgInPc4d1	negativní
dopady	dopad	k1gInPc4	dopad
změny	změna	k1gFnSc2	změna
klimatu	klima	k1gNnSc2	klima
na	na	k7c4	na
výnosy	výnos	k1gInPc1	výnos
budou	být	k5eAaImBp3nP	být
častější	častý	k2eAgInPc1d2	častější
než	než	k8xS	než
pozitivní	pozitivní	k2eAgInPc4d1	pozitivní
dopady	dopad	k1gInPc4	dopad
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
studie	studie	k1gFnPc1	studie
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
pozitivní	pozitivní	k2eAgInPc4d1	pozitivní
dopady	dopad	k1gInPc4	dopad
na	na	k7c4	na
zemědělské	zemědělský	k2eAgInPc4d1	zemědělský
výnosy	výnos	k1gInPc4	výnos
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
rovníku	rovník	k1gInSc2	rovník
<g/>
.	.	kIx.	.
</s>
<s>
Zatím	zatím	k6eAd1	zatím
není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
jaký	jaký	k3yQgInSc4	jaký
byl	být	k5eAaImAgInS	být
poměr	poměr	k1gInSc1	poměr
kladných	kladný	k2eAgInPc2d1	kladný
a	a	k8xC	a
záporných	záporný	k2eAgInPc2d1	záporný
dopadů	dopad	k1gInPc2	dopad
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
regionech	region	k1gInPc6	region
<g/>
.	.	kIx.	.
</s>
<s>
Změna	změna	k1gFnSc1	změna
klimatu	klima	k1gNnSc2	klima
negativně	negativně	k6eAd1	negativně
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
regionech	region	k1gInPc6	region
výnosy	výnos	k1gInPc7	výnos
pšenice	pšenice	k1gFnSc2	pšenice
a	a	k8xC	a
kukuřice	kukuřice	k1gFnSc2	kukuřice
<g/>
.	.	kIx.	.
</s>
<s>
Účinky	účinek	k1gInPc1	účinek
na	na	k7c4	na
výnosy	výnos	k1gInPc4	výnos
rýže	rýže	k1gFnSc2	rýže
a	a	k8xC	a
sójových	sójový	k2eAgInPc2d1	sójový
bobů	bob	k1gInPc2	bob
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
hlavních	hlavní	k2eAgFnPc6d1	hlavní
výrobních	výrobní	k2eAgFnPc6d1	výrobní
oblastech	oblast	k1gFnPc6	oblast
a	a	k8xC	a
celosvětově	celosvětově	k6eAd1	celosvětově
menší	malý	k2eAgFnSc1d2	menší
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
byly	být	k5eAaImAgFnP	být
i	i	k9	i
příznivé	příznivý	k2eAgFnPc1d1	příznivá
<g/>
.	.	kIx.	.
</s>
<s>
Rychlé	Rychlé	k2eAgInPc1d1	Rychlé
nárůsty	nárůst	k1gInPc1	nárůst
cen	cena	k1gFnPc2	cena
potravin	potravina	k1gFnPc2	potravina
a	a	k8xC	a
cereálií	cereálie	k1gFnPc2	cereálie
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
na	na	k7c4	na
vysokou	vysoký	k2eAgFnSc4d1	vysoká
citlivost	citlivost	k1gFnSc4	citlivost
současných	současný	k2eAgInPc2d1	současný
trhů	trh	k1gInPc2	trh
na	na	k7c4	na
klimatické	klimatický	k2eAgInPc4d1	klimatický
extrémy	extrém	k1gInPc4	extrém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sociální	sociální	k2eAgInPc4d1	sociální
dopady	dopad	k1gInPc4	dopad
===	===	k?	===
</s>
</p>
<p>
<s>
Přímý	přímý	k2eAgInSc1d1	přímý
vliv	vliv	k1gInSc1	vliv
klimatických	klimatický	k2eAgFnPc2d1	klimatická
změn	změna	k1gFnPc2	změna
na	na	k7c4	na
lidské	lidský	k2eAgNnSc4d1	lidské
zdraví	zdraví	k1gNnSc4	zdraví
není	být	k5eNaImIp3nS	být
doposud	doposud	k6eAd1	doposud
dostatečně	dostatečně	k6eAd1	dostatečně
kvantifikovatelný	kvantifikovatelný	k2eAgInSc1d1	kvantifikovatelný
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenán	k2eAgNnSc1d1	zaznamenáno
zvýšení	zvýšení	k1gNnSc1	zvýšení
úmrtnosti	úmrtnost	k1gFnSc2	úmrtnost
související	související	k2eAgFnPc4d1	související
s	s	k7c7	s
teplem	teplo	k1gNnSc7	teplo
a	a	k8xC	a
snížení	snížení	k1gNnSc1	snížení
úmrtnosti	úmrtnost	k1gFnSc2	úmrtnost
související	související	k2eAgFnPc4d1	související
s	s	k7c7	s
chladem	chlad	k1gInSc7	chlad
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgFnPc1d1	místní
změny	změna	k1gFnPc1	změna
teploty	teplota	k1gFnSc2	teplota
a	a	k8xC	a
srážky	srážka	k1gFnPc1	srážka
změnily	změnit	k5eAaPmAgFnP	změnit
distribuci	distribuce	k1gFnSc4	distribuce
některých	některý	k3yIgInPc2	některý
vodních	vodní	k2eAgInPc2d1	vodní
zdrojů	zdroj	k1gInPc2	zdroj
a	a	k8xC	a
s	s	k7c7	s
těmito	tento	k3xDgFnPc7	tento
změnami	změna	k1gFnPc7	změna
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
i	i	k9	i
výskyt	výskyt	k1gInSc1	výskyt
nemocí	nemoc	k1gFnPc2	nemoc
<g/>
.	.	kIx.	.
<g/>
Rozdíly	rozdíl	k1gInPc1	rozdíl
ve	v	k7c6	v
zranitelnosti	zranitelnost	k1gFnSc6	zranitelnost
obyvatel	obyvatel	k1gMnPc2	obyvatel
vznikají	vznikat	k5eAaImIp3nP	vznikat
z	z	k7c2	z
neklimatických	klimatický	k2eNgInPc2d1	klimatický
faktorů	faktor	k1gInPc2	faktor
a	a	k8xC	a
z	z	k7c2	z
vícerozměrných	vícerozměrný	k2eAgFnPc2d1	vícerozměrná
nerovností	nerovnost	k1gFnPc2	nerovnost
často	často	k6eAd1	často
způsobovaných	způsobovaný	k2eAgFnPc2d1	způsobovaná
nerovnoměrným	rovnoměrný	k2eNgInSc7d1	nerovnoměrný
vývojem	vývoj	k1gInSc7	vývoj
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
oblastech	oblast	k1gFnPc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Lidé	člověk	k1gMnPc1	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jsou	být	k5eAaImIp3nP	být
sociálně	sociálně	k6eAd1	sociálně
<g/>
,	,	kIx,	,
ekonomicky	ekonomicky	k6eAd1	ekonomicky
<g/>
,	,	kIx,	,
kulturně	kulturně	k6eAd1	kulturně
<g/>
,	,	kIx,	,
politicky	politicky	k6eAd1	politicky
<g/>
,	,	kIx,	,
institucionálně	institucionálně	k6eAd1	institucionálně
nebo	nebo	k8xC	nebo
jinak	jinak	k6eAd1	jinak
marginalizováni	marginalizován	k2eAgMnPc1d1	marginalizován
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zvláště	zvláště	k6eAd1	zvláště
ohroženi	ohrozit	k5eAaPmNgMnP	ohrozit
změnou	změna	k1gFnSc7	změna
klimatu	klima	k1gNnSc2	klima
a	a	k8xC	a
také	také	k9	také
některými	některý	k3yIgInPc7	některý
adaptačními	adaptační	k2eAgInPc7d1	adaptační
a	a	k8xC	a
mitigačními	mitigační	k2eAgNnPc7d1	mitigační
opatřeními	opatření	k1gNnPc7	opatření
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
zranitelnost	zranitelnost	k1gFnSc1	zranitelnost
má	mít	k5eAaImIp3nS	mít
většinou	většinou	k6eAd1	většinou
více	hodně	k6eAd2	hodně
důvodů	důvod	k1gInPc2	důvod
najednou	najednou	k6eAd1	najednou
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
diskriminaci	diskriminace	k1gFnSc4	diskriminace
na	na	k7c6	na
základě	základ	k1gInSc6	základ
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
,	,	kIx,	,
třídy	třída	k1gFnSc2	třída
<g/>
,	,	kIx,	,
etnického	etnický	k2eAgInSc2d1	etnický
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
věku	věk	k1gInSc2	věk
a	a	k8xC	a
schopností	schopnost	k1gFnPc2	schopnost
<g/>
.	.	kIx.	.
<g/>
Dopady	dopad	k1gInPc1	dopad
extrémních	extrémní	k2eAgInPc2d1	extrémní
jevů	jev	k1gInPc2	jev
souvisejících	související	k2eAgInPc2d1	související
s	s	k7c7	s
klimatem	klima	k1gNnSc7	klima
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
vlny	vlna	k1gFnPc1	vlna
horka	horko	k1gNnSc2	horko
<g/>
,	,	kIx,	,
sucha	sucho	k1gNnSc2	sucho
<g/>
,	,	kIx,	,
záplavy	záplava	k1gFnPc4	záplava
<g/>
,	,	kIx,	,
cyklóny	cyklón	k1gInPc4	cyklón
a	a	k8xC	a
požáry	požár	k1gInPc4	požár
<g/>
,	,	kIx,	,
odhalují	odhalovat	k5eAaImIp3nP	odhalovat
značnou	značný	k2eAgFnSc4d1	značná
zranitelnost	zranitelnost	k1gFnSc4	zranitelnost
některých	některý	k3yIgInPc2	některý
ekosystémů	ekosystém	k1gInPc2	ekosystém
a	a	k8xC	a
obyvatel	obyvatel	k1gMnPc2	obyvatel
současnými	současný	k2eAgFnPc7d1	současná
změnami	změna	k1gFnPc7	změna
klimatu	klima	k1gNnSc2	klima
<g/>
.	.	kIx.	.
</s>
<s>
Dopady	dopad	k1gInPc1	dopad
takových	takový	k3xDgInPc2	takový
extrémů	extrém	k1gInPc2	extrém
<g/>
,	,	kIx,	,
souvisejících	související	k2eAgInPc2d1	související
s	s	k7c7	s
klimatem	klima	k1gNnSc7	klima
<g/>
,	,	kIx,	,
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
změny	změna	k1gFnPc1	změna
ekosystémů	ekosystém	k1gInPc2	ekosystém
<g/>
,	,	kIx,	,
narušení	narušení	k1gNnSc1	narušení
výroby	výroba	k1gFnSc2	výroba
potravy	potrava	k1gFnSc2	potrava
a	a	k8xC	a
zásobování	zásobování	k1gNnSc1	zásobování
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
poškození	poškození	k1gNnSc4	poškození
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
a	a	k8xC	a
sídel	sídlo	k1gNnPc2	sídlo
<g/>
,	,	kIx,	,
nemocnost	nemocnost	k1gFnSc4	nemocnost
<g/>
,	,	kIx,	,
úmrtnost	úmrtnost	k1gFnSc4	úmrtnost
<g/>
,	,	kIx,	,
dopady	dopad	k1gInPc4	dopad
na	na	k7c4	na
zdraví	zdraví	k1gNnSc4	zdraví
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
duševního	duševní	k2eAgNnSc2d1	duševní
<g/>
.	.	kIx.	.
</s>
<s>
Žádný	žádný	k3yNgInSc1	žádný
stát	stát	k1gInSc1	stát
není	být	k5eNaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
dostatečně	dostatečně	k6eAd1	dostatečně
připravený	připravený	k2eAgInSc1d1	připravený
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
sektorech	sektor	k1gInPc6	sektor
na	na	k7c4	na
probíhající	probíhající	k2eAgFnPc4d1	probíhající
změny	změna	k1gFnPc4	změna
klimatu	klima	k1gNnSc2	klima
<g/>
.	.	kIx.	.
<g/>
Rizika	riziko	k1gNnSc2	riziko
související	související	k2eAgNnSc1d1	související
s	s	k7c7	s
klimatem	klima	k1gNnSc7	klima
zhoršují	zhoršovat	k5eAaImIp3nP	zhoršovat
ostatní	ostatní	k2eAgMnPc4d1	ostatní
stresory	stresor	k1gMnPc4	stresor
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
s	s	k7c7	s
negativními	negativní	k2eAgInPc7d1	negativní
výsledky	výsledek	k1gInPc7	výsledek
pro	pro	k7c4	pro
život	život	k1gInSc4	život
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
chudé	chudý	k2eAgMnPc4d1	chudý
<g/>
.	.	kIx.	.
</s>
<s>
Rizika	riziko	k1gNnSc2	riziko
související	související	k2eAgNnSc1d1	související
s	s	k7c7	s
klimatem	klima	k1gNnSc7	klima
ovlivňují	ovlivňovat	k5eAaImIp3nP	ovlivňovat
životy	život	k1gInPc1	život
chudých	chudý	k2eAgMnPc2d1	chudý
lidí	člověk	k1gMnPc2	člověk
přímo	přímo	k6eAd1	přímo
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
dopadů	dopad	k1gInPc2	dopad
na	na	k7c4	na
živobytí	živobytí	k1gNnSc4	živobytí
<g/>
,	,	kIx,	,
snižováním	snižování	k1gNnSc7	snižování
výnosů	výnos	k1gInPc2	výnos
plodin	plodina	k1gFnPc2	plodina
nebo	nebo	k8xC	nebo
zničením	zničení	k1gNnSc7	zničení
sídel	sídlo	k1gNnPc2	sídlo
a	a	k8xC	a
nepřímo	přímo	k6eNd1	přímo
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
například	například	k6eAd1	například
zvýšení	zvýšení	k1gNnSc4	zvýšení
cen	cena	k1gFnPc2	cena
potravin	potravina	k1gFnPc2	potravina
a	a	k8xC	a
nedostatkem	nedostatek	k1gInSc7	nedostatek
potravin	potravina	k1gFnPc2	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Pozitivní	pozitivní	k2eAgInPc4d1	pozitivní
účinky	účinek	k1gInPc4	účinek
pro	pro	k7c4	pro
chudé	chudý	k2eAgMnPc4d1	chudý
a	a	k8xC	a
marginalizované	marginalizovaný	k2eAgMnPc4d1	marginalizovaný
obyvatele	obyvatel	k1gMnPc4	obyvatel
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
omezené	omezený	k2eAgFnPc1d1	omezená
a	a	k8xC	a
nepřímé	přímý	k2eNgFnPc1d1	nepřímá
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
diverzifikace	diverzifikace	k1gFnSc1	diverzifikace
sociálních	sociální	k2eAgFnPc2d1	sociální
sítí	síť	k1gFnPc2	síť
a	a	k8xC	a
zlepšení	zlepšení	k1gNnSc4	zlepšení
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
praktik	praktika	k1gFnPc2	praktika
<g/>
.	.	kIx.	.
<g/>
Násilné	násilný	k2eAgInPc1d1	násilný
konflikty	konflikt	k1gInPc1	konflikt
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
zranitelnost	zranitelnost	k1gFnSc4	zranitelnost
vůči	vůči	k7c3	vůči
změně	změna	k1gFnSc3	změna
klimatu	klima	k1gNnSc2	klima
<g/>
.	.	kIx.	.
</s>
<s>
Větší	veliký	k2eAgInPc1d2	veliký
násilné	násilný	k2eAgInPc1d1	násilný
konflikty	konflikt	k1gInPc1	konflikt
poškozují	poškozovat	k5eAaImIp3nP	poškozovat
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
potřeba	potřeba	k6eAd1	potřeba
pro	pro	k7c4	pro
adaptaci	adaptace	k1gFnSc4	adaptace
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
<g/>
,	,	kIx,	,
institucí	instituce	k1gFnPc2	instituce
<g/>
,	,	kIx,	,
přírodních	přírodní	k2eAgInPc2d1	přírodní
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
sociálního	sociální	k2eAgInSc2d1	sociální
kapitálu	kapitál	k1gInSc2	kapitál
a	a	k8xC	a
možností	možnost	k1gFnPc2	možnost
obživy	obživa	k1gFnSc2	obživa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vzestup	vzestup	k1gInSc1	vzestup
hladin	hladina	k1gFnPc2	hladina
moří	moře	k1gNnPc2	moře
a	a	k8xC	a
oceánů	oceán	k1gInPc2	oceán
===	===	k?	===
</s>
</p>
<p>
<s>
Hladina	hladina	k1gFnSc1	hladina
moře	moře	k1gNnSc2	moře
rostla	růst	k5eAaImAgFnS	růst
v	v	k7c6	v
letech	let	k1gInPc6	let
1961	[number]	k4	1961
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
vlivem	vliv	k1gInSc7	vliv
teplotní	teplotní	k2eAgFnSc2d1	teplotní
roztažnosti	roztažnost	k1gFnSc2	roztažnost
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
táním	tání	k1gNnSc7	tání
pevninských	pevninský	k2eAgInPc2d1	pevninský
ledovců	ledovec	k1gInPc2	ledovec
<g/>
,	,	kIx,	,
o	o	k7c4	o
1,7	[number]	k4	1,7
(	(	kIx(	(
<g/>
1,5	[number]	k4	1,5
až	až	k9	až
1,9	[number]	k4	1,9
<g/>
)	)	kIx)	)
mm	mm	kA	mm
<g/>
·	·	k?	·
<g/>
rok	rok	k1gInSc1	rok
<g/>
−	−	k?	−
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
stoupla	stoupnout	k5eAaPmAgFnS	stoupnout
hladina	hladina	k1gFnSc1	hladina
oceánů	oceán	k1gInPc2	oceán
za	za	k7c4	za
období	období	k1gNnSc4	období
1901	[number]	k4	1901
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
o	o	k7c4	o
19	[number]	k4	19
(	(	kIx(	(
<g/>
17	[number]	k4	17
až	až	k9	až
21	[number]	k4	21
<g/>
)	)	kIx)	)
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
u	u	k7c2	u
výšky	výška	k1gFnSc2	výška
mořské	mořský	k2eAgFnSc2d1	mořská
hladiny	hladina	k1gFnSc2	hladina
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
oscilacím	oscilace	k1gFnPc3	oscilace
<g/>
,	,	kIx,	,
způsobeným	způsobený	k2eAgInSc7d1	způsobený
jak	jak	k6eAd1	jak
dočasným	dočasný	k2eAgInSc7d1	dočasný
"	"	kIx"	"
<g/>
přesunem	přesun	k1gInSc7	přesun
<g/>
"	"	kIx"	"
vod	voda	k1gFnPc2	voda
na	na	k7c4	na
pevninu	pevnina	k1gFnSc4	pevnina
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
především	především	k9	především
díky	díky	k7c3	díky
jevům	jev	k1gInPc3	jev
El	Ela	k1gFnPc2	Ela
Niñ	Niñ	k1gFnPc4	Niñ
a	a	k8xC	a
La	la	k1gNnPc4	la
Niňa	Niň	k2eAgNnPc4d1	Niň
<g/>
.	.	kIx.	.
</s>
<s>
IPCC	IPCC	kA	IPCC
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
další	další	k2eAgInSc4d1	další
nárůst	nárůst	k1gInSc4	nárůst
zvyšování	zvyšování	k1gNnSc2	zvyšování
rychlosti	rychlost	k1gFnSc2	rychlost
hladiny	hladina	k1gFnSc2	hladina
moří	moře	k1gNnPc2	moře
a	a	k8xC	a
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejpravděpodobněji	pravděpodobně	k6eAd3	pravděpodobně
stoupne	stoupnout	k5eAaPmIp3nS	stoupnout
hladina	hladina	k1gFnSc1	hladina
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2100	[number]	k4	2100
o	o	k7c4	o
80	[number]	k4	80
cm	cm	kA	cm
až	až	k9	až
1	[number]	k4	1
m.	m.	k?	m.
</s>
</p>
<p>
<s>
===	===	k?	===
Tání	tání	k1gNnPc2	tání
ledovců	ledovec	k1gInPc2	ledovec
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
posledních	poslední	k2eAgNnPc2d1	poslední
dvou	dva	k4xCgNnPc2	dva
desetiletí	desetiletí	k1gNnPc2	desetiletí
došlo	dojít	k5eAaPmAgNnS	dojít
podle	podle	k7c2	podle
vyhodnocení	vyhodnocení	k1gNnSc2	vyhodnocení
měření	měření	k1gNnSc2	měření
k	k	k7c3	k
úbytku	úbytek	k1gInSc3	úbytek
zalednění	zalednění	k1gNnSc2	zalednění
v	v	k7c6	v
Grónsku	Grónsko	k1gNnSc6	Grónsko
a	a	k8xC	a
v	v	k7c6	v
Antarktidě	Antarktida	k1gFnSc6	Antarktida
<g/>
.	.	kIx.	.
</s>
<s>
Ledovce	ledovec	k1gInPc1	ledovec
ubývají	ubývat	k5eAaImIp3nP	ubývat
na	na	k7c6	na
většině	většina	k1gFnSc6	většina
míst	místo	k1gNnPc2	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
a	a	k8xC	a
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
polokouli	polokoule	k1gFnSc6	polokoule
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
úbytku	úbytek	k1gInSc3	úbytek
sněhové	sněhový	k2eAgFnSc2d1	sněhová
pokrývky	pokrývka	k1gFnSc2	pokrývka
v	v	k7c6	v
jarních	jarní	k2eAgInPc6d1	jarní
měsících	měsíc	k1gInPc6	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Úbytek	úbytek	k1gInSc1	úbytek
masy	masa	k1gFnSc2	masa
ledu	led	k1gInSc2	led
v	v	k7c6	v
ledovcích	ledovec	k1gInPc6	ledovec
byl	být	k5eAaImAgMnS	být
podle	podle	k7c2	podle
IPCC	IPCC	kA	IPCC
celosvětově	celosvětově	k6eAd1	celosvětově
odhadován	odhadovat	k5eAaImNgInS	odhadovat
na	na	k7c4	na
275	[number]	k4	275
(	(	kIx(	(
<g/>
140	[number]	k4	140
až	až	k9	až
410	[number]	k4	410
<g/>
)	)	kIx)	)
Gt	Gt	k1gFnSc1	Gt
<g/>
/	/	kIx~	/
<g/>
rok	rok	k1gInSc1	rok
v	v	k7c6	v
období	období	k1gNnSc6	období
let	léto	k1gNnPc2	léto
1993	[number]	k4	1993
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
též	též	k9	též
ke	k	k7c3	k
zmenšení	zmenšení	k1gNnSc3	zmenšení
tloušťky	tloušťka	k1gFnSc2	tloušťka
permafrostu	permafrost	k1gInSc2	permafrost
<g/>
,	,	kIx,	,
rozlohy	rozloha	k1gFnSc2	rozloha
sezónně	sezónně	k6eAd1	sezónně
zamrzlé	zamrzlý	k2eAgFnSc2d1	zamrzlá
půdy	půda	k1gFnSc2	půda
a	a	k8xC	a
zkrácení	zkrácení	k1gNnSc4	zkrácení
doby	doba	k1gFnSc2	doba
zamrznutí	zamrznutí	k1gNnSc2	zamrznutí
řek	řeka	k1gFnPc2	řeka
a	a	k8xC	a
jezer	jezero	k1gNnPc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Satelitní	satelitní	k2eAgNnPc1d1	satelitní
data	datum	k1gNnPc1	datum
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
roční	roční	k2eAgFnSc1d1	roční
průměrná	průměrný	k2eAgFnSc1d1	průměrná
rozloha	rozloha	k1gFnSc1	rozloha
arktického	arktický	k2eAgInSc2d1	arktický
ledu	led	k1gInSc2	led
se	se	k3xPyFc4	se
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
zmenšovala	zmenšovat	k5eAaImAgFnS	zmenšovat
o	o	k7c4	o
2,7	[number]	k4	2,7
%	%	kIx~	%
±	±	k?	±
0,6	[number]	k4	0,6
%	%	kIx~	%
za	za	k7c4	za
desetiletí	desetiletí	k1gNnPc4	desetiletí
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
Antarktidě	Antarktida	k1gFnSc6	Antarktida
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Východní	východní	k2eAgFnSc2d1	východní
Antarktidy	Antarktida	k1gFnSc2	Antarktida
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
studií	studie	k1gFnPc2	studie
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
úbytkům	úbytek	k1gInPc3	úbytek
pevninského	pevninský	k2eAgInSc2d1	pevninský
ledovce	ledovec	k1gInSc2	ledovec
(	(	kIx(	(
<g/>
rychlost	rychlost	k1gFnSc1	rychlost
úbytku	úbytek	k1gInSc2	úbytek
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
70	[number]	k4	70
Gt	Gt	k1gFnPc2	Gt
<g/>
/	/	kIx~	/
<g/>
rok	rok	k1gInSc4	rok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
mořský	mořský	k2eAgInSc1d1	mořský
led	led	k1gInSc1	led
obklopující	obklopující	k2eAgInSc1d1	obklopující
Antarktidu	Antarktida	k1gFnSc4	Antarktida
roste	růst	k5eAaImIp3nS	růst
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
teplota	teplota	k1gFnSc1	teplota
moře	moře	k1gNnSc2	moře
roste	růst	k5eAaImIp3nS	růst
obdobným	obdobný	k2eAgNnSc7d1	obdobné
tempem	tempo	k1gNnSc7	tempo
<g/>
.	.	kIx.	.
</s>
<s>
Odborníci	odborník	k1gMnPc1	odborník
vysvětlují	vysvětlovat	k5eAaImIp3nP	vysvětlovat
tento	tento	k3xDgInSc4	tento
protiklad	protiklad	k1gInSc4	protiklad
působením	působení	k1gNnSc7	působení
ozonové	ozonový	k2eAgFnSc2d1	ozonová
díry	díra	k1gFnSc2	díra
nad	nad	k7c7	nad
Antarktidou	Antarktida	k1gFnSc7	Antarktida
<g/>
,	,	kIx,	,
změnami	změna	k1gFnPc7	změna
mořských	mořský	k2eAgInPc2d1	mořský
proudů	proud	k1gInPc2	proud
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
přispívá	přispívat	k5eAaImIp3nS	přispívat
sladká	sladký	k2eAgFnSc1d1	sladká
voda	voda	k1gFnSc1	voda
z	z	k7c2	z
tajících	tající	k2eAgInPc2d1	tající
pevninských	pevninský	k2eAgInPc2d1	pevninský
ledovců	ledovec	k1gInPc2	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
NASA	NASA	kA	NASA
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
sice	sice	k8xC	sice
zatím	zatím	k6eAd1	zatím
stále	stále	k6eAd1	stále
v	v	k7c6	v
Antarktidě	Antarktida	k1gFnSc6	Antarktida
ledu	led	k1gInSc2	led
přibývá	přibývat	k5eAaImIp3nS	přibývat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tento	tento	k3xDgInSc1	tento
přírůstek	přírůstek	k1gInSc1	přírůstek
klesá	klesat	k5eAaImIp3nS	klesat
a	a	k8xC	a
během	během	k7c2	během
20	[number]	k4	20
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zastaví	zastavit	k5eAaPmIp3nS	zastavit
<g/>
.	.	kIx.	.
</s>
<s>
NASA	NASA	kA	NASA
zároveň	zároveň	k6eAd1	zároveň
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
s	s	k7c7	s
výsledky	výsledek	k1gInPc7	výsledek
studie	studie	k1gFnSc2	studie
konstatuje	konstatovat	k5eAaBmIp3nS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
celkově	celkově	k6eAd1	celkově
úbytek	úbytek	k1gInSc1	úbytek
Arktických	arktický	k2eAgInPc2d1	arktický
ledovců	ledovec	k1gInPc2	ledovec
výrazně	výrazně	k6eAd1	výrazně
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
přírůstky	přírůstek	k1gInPc1	přírůstek
v	v	k7c6	v
Antarktidě	Antarktida	k1gFnSc6	Antarktida
a	a	k8xC	a
že	že	k8xS	že
v	v	k7c6	v
celosvětovém	celosvětový	k2eAgNnSc6d1	celosvětové
měřítku	měřítko	k1gNnSc6	měřítko
dochází	docházet	k5eAaImIp3nS	docházet
nadále	nadále	k6eAd1	nadále
k	k	k7c3	k
ubývání	ubývání	k1gNnSc3	ubývání
ledovců	ledovec	k1gInPc2	ledovec
<g/>
.	.	kIx.	.
<g/>
Globální	globální	k2eAgNnSc4d1	globální
oteplení	oteplení	k1gNnSc4	oteplení
vede	vést	k5eAaImIp3nS	vést
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
k	k	k7c3	k
ústupu	ústup	k1gInSc3	ústup
ledovců	ledovec	k1gInPc2	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Oerlemans	Oerlemans	k1gInSc1	Oerlemans
prokázal	prokázat	k5eAaPmAgInS	prokázat
podle	podle	k7c2	podle
záznamů	záznam	k1gInPc2	záznam
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
jednoznačný	jednoznačný	k2eAgInSc1d1	jednoznačný
ústup	ústup	k1gInSc1	ústup
142	[number]	k4	142
ze	z	k7c2	z
144	[number]	k4	144
horských	horský	k2eAgInPc2d1	horský
ledovců	ledovec	k1gInPc2	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
se	se	k3xPyFc4	se
ústup	ústup	k1gInSc1	ústup
ledovců	ledovec	k1gInPc2	ledovec
značně	značně	k6eAd1	značně
zrychlil	zrychlit	k5eAaPmAgMnS	zrychlit
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
Dyurgerov	Dyurgerovo	k1gNnPc2	Dyurgerovo
a	a	k8xC	a
Meier	Meira	k1gFnPc2	Meira
zprůměrovali	zprůměrovat	k5eAaPmAgMnP	zprůměrovat
data	datum	k1gNnPc4	datum
o	o	k7c4	o
velikosti	velikost	k1gFnPc4	velikost
ledovců	ledovec	k1gInPc2	ledovec
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
velkých	velký	k2eAgInPc2d1	velký
regionů	region	k1gInPc2	region
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
)	)	kIx)	)
a	a	k8xC	a
zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
regionu	region	k1gInSc6	region
došlo	dojít	k5eAaPmAgNnS	dojít
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
k	k	k7c3	k
celkovému	celkový	k2eAgInSc3d1	celkový
ústupu	ústup	k1gInSc3	ústup
ledovců	ledovec	k1gInPc2	ledovec
<g/>
,	,	kIx,	,
ačkoli	ačkoli	k8xS	ačkoli
některé	některý	k3yIgInPc1	některý
lokální	lokální	k2eAgInPc1d1	lokální
regiony	region	k1gInPc1	region
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Skandinávie	Skandinávie	k1gFnSc2	Skandinávie
<g/>
)	)	kIx)	)
vykázaly	vykázat	k5eAaPmAgInP	vykázat
nárůsty	nárůst	k1gInPc1	nárůst
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
ledovce	ledovec	k1gInPc1	ledovec
již	již	k6eAd1	již
zmizely	zmizet	k5eAaPmAgInP	zmizet
zcela	zcela	k6eAd1	zcela
a	a	k8xC	a
očekává	očekávat	k5eAaImIp3nS	očekávat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
rostoucí	rostoucí	k2eAgFnPc1d1	rostoucí
teploty	teplota	k1gFnPc1	teplota
způsobí	způsobit	k5eAaPmIp3nP	způsobit
neustálý	neustálý	k2eAgInSc4d1	neustálý
ústup	ústup	k1gInSc4	ústup
i	i	k8xC	i
většiny	většina	k1gFnSc2	většina
ostatních	ostatní	k2eAgInPc2d1	ostatní
horských	horský	k2eAgInPc2d1	horský
ledovců	ledovec	k1gInPc2	ledovec
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
více	hodně	k6eAd2	hodně
než	než	k8xS	než
90	[number]	k4	90
%	%	kIx~	%
ledovců	ledovec	k1gInPc2	ledovec
zaznamenalo	zaznamenat	k5eAaPmAgNnS	zaznamenat
Světové	světový	k2eAgNnSc1d1	světové
středisko	středisko	k1gNnSc1	středisko
pro	pro	k7c4	pro
monitorování	monitorování	k1gNnSc4	monitorování
ledovců	ledovec	k1gInPc2	ledovec
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
jejich	jejich	k3xOp3gInSc4	jejich
ústup	ústup	k1gInSc4	ústup
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Změny	změna	k1gFnPc4	změna
uhlíkového	uhlíkový	k2eAgInSc2d1	uhlíkový
cyklu	cyklus	k1gInSc2	cyklus
a	a	k8xC	a
ostatních	ostatní	k2eAgInPc2d1	ostatní
biogeochemických	biogeochemický	k2eAgInPc2d1	biogeochemický
cyklů	cyklus	k1gInPc2	cyklus
===	===	k?	===
</s>
</p>
<p>
<s>
Atmosférické	atmosférický	k2eAgFnPc1d1	atmosférická
koncentrace	koncentrace	k1gFnPc1	koncentrace
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
,	,	kIx,	,
methanu	methan	k1gInSc2	methan
a	a	k8xC	a
oxidu	oxid	k1gInSc2	oxid
dusného	dusný	k2eAgInSc2d1	dusný
se	se	k3xPyFc4	se
zvýšily	zvýšit	k5eAaPmAgFnP	zvýšit
na	na	k7c4	na
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
úroveň	úroveň	k1gFnSc4	úroveň
za	za	k7c2	za
posledních	poslední	k2eAgNnPc2d1	poslední
minimálně	minimálně	k6eAd1	minimálně
800	[number]	k4	800
tisíc	tisíc	k4xCgInPc2	tisíc
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Koncentrace	koncentrace	k1gFnPc1	koncentrace
CO2	CO2	k1gMnPc2	CO2
vzrostly	vzrůst	k5eAaPmAgFnP	vzrůst
od	od	k7c2	od
předindustriální	předindustriální	k2eAgFnSc2d1	předindustriální
doby	doba	k1gFnSc2	doba
o	o	k7c4	o
40	[number]	k4	40
%	%	kIx~	%
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
díky	díky	k7c3	díky
spalování	spalování	k1gNnSc3	spalování
fosilních	fosilní	k2eAgNnPc2d1	fosilní
paliv	palivo	k1gNnPc2	palivo
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
pak	pak	k6eAd1	pak
změnami	změna	k1gFnPc7	změna
využití	využití	k1gNnSc2	využití
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Oceány	oceán	k1gInPc1	oceán
absorbují	absorbovat	k5eAaBmIp3nP	absorbovat
asi	asi	k9	asi
30	[number]	k4	30
%	%	kIx~	%
emitovaného	emitovaný	k2eAgInSc2d1	emitovaný
antropogenního	antropogenní	k2eAgInSc2d1	antropogenní
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
jejich	jejich	k3xOp3gNnPc4	jejich
okyselování	okyselování	k1gNnPc4	okyselování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Acidifikace	Acidifikace	k1gFnPc4	Acidifikace
moří	mořit	k5eAaImIp3nP	mořit
===	===	k?	===
</s>
</p>
<p>
<s>
Rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
kyselostí	kyselost	k1gFnSc7	kyselost
mořské	mořský	k2eAgFnSc2d1	mořská
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
stoupá	stoupat	k5eAaImIp3nS	stoupat
díky	díky	k7c3	díky
nárůstu	nárůst	k1gInSc3	nárůst
CO2	CO2	k1gFnSc2	CO2
v	v	k7c6	v
ovzduší	ovzduší	k1gNnSc6	ovzduší
<g/>
,	,	kIx,	,
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
změnám	změna	k1gFnPc3	změna
v	v	k7c6	v
mořských	mořský	k2eAgInPc6d1	mořský
ekosystémech	ekosystém	k1gInPc6	ekosystém
<g/>
.	.	kIx.	.
</s>
<s>
Nejde	jít	k5eNaImIp3nS	jít
jen	jen	k9	jen
o	o	k7c4	o
výrazný	výrazný	k2eAgInSc4d1	výrazný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
korály	korál	k1gInPc4	korál
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
na	na	k7c4	na
celé	celý	k2eAgInPc4d1	celý
potravní	potravní	k2eAgInPc4d1	potravní
řetězce	řetězec	k1gInPc4	řetězec
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
výnosů	výnos	k1gInPc2	výnos
z	z	k7c2	z
rybolovu	rybolov	k1gInSc2	rybolov
apod.	apod.	kA	apod.
Vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
korály	korál	k1gInPc4	korál
ale	ale	k8xC	ale
nemusí	muset	k5eNaImIp3nS	muset
být	být	k5eAaImF	být
takový	takový	k3xDgMnSc1	takový
<g/>
.	.	kIx.	.
</s>
<s>
Zvýšení	zvýšení	k1gNnSc1	zvýšení
těchto	tento	k3xDgFnPc2	tento
hodnot	hodnota	k1gFnPc2	hodnota
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
pro	pro	k7c4	pro
organismy	organismus	k1gInPc4	organismus
ovlivněním	ovlivnění	k1gNnSc7	ovlivnění
výskytu	výskyt	k1gInSc2	výskyt
fytoplanktonu	fytoplankton	k1gInSc2	fytoplankton
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
základem	základ	k1gInSc7	základ
potravního	potravní	k2eAgInSc2d1	potravní
řetězce	řetězec	k1gInSc2	řetězec
mnoha	mnoho	k4c2	mnoho
druhů	druh	k1gInPc2	druh
zvířat	zvíře	k1gNnPc2	zvíře
v	v	k7c6	v
oceánu	oceán	k1gInSc6	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgNnSc7d1	další
rizikem	riziko	k1gNnSc7	riziko
je	být	k5eAaImIp3nS	být
rostoucí	rostoucí	k2eAgFnSc1d1	rostoucí
míra	míra	k1gFnSc1	míra
plastů	plast	k1gInPc2	plast
v	v	k7c6	v
oceánech	oceán	k1gInPc6	oceán
a	a	k8xC	a
hromadění	hromadění	k1gNnSc6	hromadění
odpadků	odpadek	k1gInPc2	odpadek
například	například	k6eAd1	například
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Velká	velký	k2eAgFnSc1d1	velká
tichomořská	tichomořský	k2eAgFnSc1d1	tichomořská
odpadková	odpadkový	k2eAgFnSc1d1	odpadková
skvrna	skvrna	k1gFnSc1	skvrna
<g/>
.	.	kIx.	.
</s>
<s>
Vliv	vliv	k1gInSc1	vliv
rozpouštějících	rozpouštějící	k2eAgInPc2d1	rozpouštějící
se	se	k3xPyFc4	se
plastů	plast	k1gInPc2	plast
na	na	k7c4	na
světový	světový	k2eAgInSc4d1	světový
oceán	oceán	k1gInSc4	oceán
je	být	k5eAaImIp3nS	být
zatím	zatím	k6eAd1	zatím
předmětem	předmět	k1gInSc7	předmět
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
jeví	jevit	k5eAaImIp3nS	jevit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
řasy	řasa	k1gFnPc4	řasa
a	a	k8xC	a
mořský	mořský	k2eAgInSc4d1	mořský
kelp	kelp	k1gInSc4	kelp
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
organismy	organismus	k1gInPc4	organismus
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
neblahý	blahý	k2eNgInSc4d1	neblahý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oceánech	oceán	k1gInPc6	oceán
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ohromné	ohromný	k2eAgNnSc1d1	ohromné
množství	množství	k1gNnSc1	množství
mikroorganismů	mikroorganismus	k1gInPc2	mikroorganismus
<g/>
,	,	kIx,	,
bakterie	bakterie	k1gFnSc1	bakterie
a	a	k8xC	a
protozoa	protozoa	k1gFnSc1	protozoa
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
souhrnně	souhrnně	k6eAd1	souhrnně
známé	známý	k2eAgInPc1d1	známý
jako	jako	k8xS	jako
plankton	plankton	k1gInSc1	plankton
a	a	k8xC	a
míra	míra	k1gFnSc1	míra
acidifikace	acidifikace	k1gFnSc2	acidifikace
má	mít	k5eAaImIp3nS	mít
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
jeho	on	k3xPp3gInSc4	on
výskyt	výskyt	k1gInSc4	výskyt
a	a	k8xC	a
složení	složení	k1gNnSc4	složení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reakce	reakce	k1gFnPc1	reakce
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Mitigační	Mitigační	k2eAgNnSc1d1	Mitigační
opatření	opatření	k1gNnSc1	opatření
(	(	kIx(	(
<g/>
zmírňování	zmírňování	k1gNnSc1	zmírňování
následků	následek	k1gInPc2	následek
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Snížení	snížení	k1gNnSc1	snížení
rozsahu	rozsah	k1gInSc2	rozsah
budoucí	budoucí	k2eAgFnSc2d1	budoucí
změny	změna	k1gFnSc2	změna
klimatu	klima	k1gNnSc2	klima
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
mitigace	mitigace	k1gFnSc1	mitigace
(	(	kIx(	(
<g/>
zmírňování	zmírňování	k1gNnSc1	zmírňování
následků	následek	k1gInPc2	následek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
IPCC	IPCC	kA	IPCC
definuje	definovat	k5eAaBmIp3nS	definovat
mitigaci	mitigace	k1gFnSc4	mitigace
jako	jako	k8xS	jako
činnosti	činnost	k1gFnPc4	činnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
snižují	snižovat	k5eAaImIp3nP	snižovat
emise	emise	k1gFnPc4	emise
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
nebo	nebo	k8xC	nebo
zvyšují	zvyšovat	k5eAaImIp3nP	zvyšovat
kapacitu	kapacita	k1gFnSc4	kapacita
propadů	propad	k1gInPc2	propad
uhlíku	uhlík	k1gInSc2	uhlík
pohlcovat	pohlcovat	k5eAaImF	pohlcovat
skleníkové	skleníkový	k2eAgInPc4d1	skleníkový
plyny	plyn	k1gInPc4	plyn
z	z	k7c2	z
atmosféry	atmosféra	k1gFnSc2	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnPc1	studie
naznačují	naznačovat	k5eAaImIp3nP	naznačovat
značný	značný	k2eAgInSc4d1	značný
potenciál	potenciál	k1gInSc4	potenciál
pro	pro	k7c4	pro
budoucí	budoucí	k2eAgNnSc4d1	budoucí
snížení	snížení	k1gNnSc4	snížení
emisí	emise	k1gFnPc2	emise
kombinací	kombinace	k1gFnSc7	kombinace
aktivit	aktivita	k1gFnPc2	aktivita
na	na	k7c6	na
snižování	snižování	k1gNnSc6	snižování
emisí	emise	k1gFnPc2	emise
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
úspory	úspora	k1gFnPc1	úspora
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
nárůst	nárůst	k1gInSc4	nárůst
energetické	energetický	k2eAgFnSc2d1	energetická
účinnosti	účinnost	k1gFnSc2	účinnost
a	a	k8xC	a
větším	většit	k5eAaImIp1nS	většit
uspokojení	uspokojení	k1gNnSc4	uspokojení
poptávky	poptávka	k1gFnSc2	poptávka
společnosti	společnost	k1gFnSc2	společnost
po	po	k7c6	po
obnovitelných	obnovitelný	k2eAgInPc6d1	obnovitelný
zdrojích	zdroj	k1gInPc6	zdroj
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
jaderné	jaderný	k2eAgFnSc2d1	jaderná
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Zmírňování	zmírňování	k1gNnSc1	zmírňování
změny	změna	k1gFnSc2	změna
klimatu	klima	k1gNnSc2	klima
také	také	k9	také
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
činnosti	činnost	k1gFnPc4	činnost
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zlepšují	zlepšovat	k5eAaImIp3nP	zlepšovat
přírodní	přírodní	k2eAgInPc4d1	přírodní
propady	propad	k1gInPc4	propad
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
výsadba	výsadba	k1gFnSc1	výsadba
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
znovuzalesnění	znovuzalesnění	k1gNnPc2	znovuzalesnění
<g/>
.	.	kIx.	.
<g/>
Za	za	k7c7	za
účelem	účel	k1gInSc7	účel
omezení	omezení	k1gNnSc2	omezení
oteplování	oteplování	k1gNnSc2	oteplování
v	v	k7c6	v
dolním	dolní	k2eAgInSc6d1	dolní
rozsahu	rozsah	k1gInSc6	rozsah
popsaném	popsaný	k2eAgInSc6d1	popsaný
ve	v	k7c6	v
zprávě	zpráva	k1gFnSc6	zpráva
IPCC	IPCC	kA	IPCC
"	"	kIx"	"
<g/>
Shrnutí	shrnutí	k1gNnSc1	shrnutí
pro	pro	k7c4	pro
politické	politický	k2eAgMnPc4d1	politický
představitele	představitel	k1gMnPc4	představitel
<g/>
"	"	kIx"	"
bude	být	k5eAaImBp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
přijmout	přijmout	k5eAaPmF	přijmout
politická	politický	k2eAgNnPc4d1	politické
opatření	opatření	k1gNnPc4	opatření
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
omezí	omezit	k5eAaPmIp3nP	omezit
emise	emise	k1gFnPc4	emise
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
podle	podle	k7c2	podle
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
několika	několik	k4yIc2	několik
výrazně	výrazně	k6eAd1	výrazně
odlišných	odlišný	k2eAgInPc2d1	odlišný
scénářů	scénář	k1gInPc2	scénář
popsaných	popsaný	k2eAgInPc2d1	popsaný
v	v	k7c6	v
úplné	úplný	k2eAgFnSc6d1	úplná
zprávě	zpráva	k1gFnSc6	zpráva
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
bude	být	k5eAaImBp3nS	být
stále	stále	k6eAd1	stále
těžší	těžký	k2eAgNnSc4d2	těžší
a	a	k8xC	a
těžší	těžký	k2eAgNnSc4d2	těžší
s	s	k7c7	s
každoročním	každoroční	k2eAgInSc7d1	každoroční
nárůstem	nárůst	k1gInSc7	nárůst
objemů	objem	k1gInPc2	objem
emisí	emise	k1gFnPc2	emise
a	a	k8xC	a
v	v	k7c6	v
pozdějších	pozdní	k2eAgNnPc6d2	pozdější
letech	léto	k1gNnPc6	léto
bude	být	k5eAaImBp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
přijmout	přijmout	k5eAaPmF	přijmout
ještě	ještě	k9	ještě
drastičtější	drastický	k2eAgNnPc4d2	drastičtější
opatření	opatření	k1gNnPc4	opatření
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
stabilizaci	stabilizace	k1gFnSc3	stabilizace
požadované	požadovaný	k2eAgFnSc2d1	požadovaná
atmosférické	atmosférický	k2eAgFnSc2d1	atmosférická
koncentrace	koncentrace	k1gFnSc2	koncentrace
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Emise	emise	k1gFnPc1	emise
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
(	(	kIx(	(
<g/>
CO	co	k8xS	co
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
související	související	k2eAgInSc1d1	související
se	s	k7c7	s
spotřebou	spotřeba	k1gFnSc7	spotřeba
energie	energie	k1gFnSc2	energie
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
,	,	kIx,	,
pokořily	pokořit	k5eAaPmAgFnP	pokořit
předchozí	předchozí	k2eAgInSc4d1	předchozí
rekord	rekord	k1gInSc4	rekord
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Adaptační	adaptační	k2eAgNnSc1d1	adaptační
opatření	opatření	k1gNnSc1	opatření
(	(	kIx(	(
<g/>
přizpůsobení	přizpůsobení	k1gNnSc1	přizpůsobení
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Další	další	k2eAgFnSc1d1	další
politická	politický	k2eAgFnSc1d1	politická
reakce	reakce	k1gFnSc1	reakce
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
adaptaci	adaptace	k1gFnSc4	adaptace
na	na	k7c4	na
změnu	změna	k1gFnSc4	změna
klimatu	klima	k1gNnSc2	klima
(	(	kIx(	(
<g/>
přizpůsobení	přizpůsobení	k1gNnSc2	přizpůsobení
klimatické	klimatický	k2eAgFnSc6d1	klimatická
změně	změna	k1gFnSc6	změna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Adaptace	adaptace	k1gFnSc1	adaptace
na	na	k7c4	na
změnu	změna	k1gFnSc4	změna
klimatu	klima	k1gNnSc2	klima
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
plánována	plánován	k2eAgFnSc1d1	plánována
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
buď	buď	k8xC	buď
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
nebo	nebo	k8xC	nebo
v	v	k7c6	v
předvídání	předvídání	k1gNnSc6	předvídání
klimatických	klimatický	k2eAgFnPc2d1	klimatická
změn	změna	k1gFnPc2	změna
a	a	k8xC	a
nebo	nebo	k8xC	nebo
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
spontánní	spontánní	k2eAgFnSc1d1	spontánní
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
bez	bez	k7c2	bez
zásahu	zásah	k1gInSc2	zásah
oficiálních	oficiální	k2eAgNnPc2d1	oficiální
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
probíhají	probíhat	k5eAaImIp3nP	probíhat
v	v	k7c6	v
omezené	omezený	k2eAgFnSc6d1	omezená
míře	míra	k1gFnSc6	míra
plánované	plánovaný	k2eAgFnSc2d1	plánovaná
adaptace	adaptace	k1gFnSc2	adaptace
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
plně	plně	k6eAd1	plně
pochopeny	pochopen	k2eAgFnPc1d1	pochopena
překážky	překážka	k1gFnPc1	překážka
<g/>
,	,	kIx,	,
limity	limit	k1gInPc1	limit
a	a	k8xC	a
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
budoucí	budoucí	k2eAgFnPc4d1	budoucí
adaptace	adaptace	k1gFnPc4	adaptace
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
takových	takový	k3xDgFnPc2	takový
strategií	strategie	k1gFnPc2	strategie
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
obrana	obrana	k1gFnSc1	obrana
proti	proti	k7c3	proti
růstu	růst	k1gInSc3	růst
hladiny	hladina	k1gFnSc2	hladina
moří	mořit	k5eAaImIp3nS	mořit
nebo	nebo	k8xC	nebo
zabezpečení	zabezpečení	k1gNnSc4	zabezpečení
dostupnosti	dostupnost	k1gFnSc2	dostupnost
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Koncept	koncept	k1gInSc1	koncept
vztahující	vztahující	k2eAgNnPc1d1	vztahující
se	se	k3xPyFc4	se
k	k	k7c3	k
adaptaci	adaptace	k1gFnSc3	adaptace
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
adaptační	adaptační	k2eAgFnSc1d1	adaptační
kapacita	kapacita	k1gFnSc1	kapacita
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
schopnost	schopnost	k1gFnSc4	schopnost
systému	systém	k1gInSc2	systém
(	(	kIx(	(
<g/>
lidského	lidský	k2eAgMnSc2d1	lidský
<g/>
,	,	kIx,	,
přírodního	přírodní	k2eAgInSc2d1	přírodní
nebo	nebo	k8xC	nebo
řízeného	řízený	k2eAgInSc2d1	řízený
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
změnám	změna	k1gFnPc3	změna
klimatu	klima	k1gNnSc2	klima
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
lokální	lokální	k2eAgFnSc2d1	lokální
proměnlivosti	proměnlivost	k1gFnSc2	proměnlivost
klimatu	klima	k1gNnSc2	klima
a	a	k8xC	a
extrémů	extrém	k1gInPc2	extrém
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
snížily	snížit	k5eAaPmAgFnP	snížit
případné	případný	k2eAgFnPc1d1	případná
škody	škoda	k1gFnPc1	škoda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
využily	využít	k5eAaPmAgFnP	využít
výhody	výhoda	k1gFnPc1	výhoda
příležitostí	příležitost	k1gFnPc2	příležitost
nebo	nebo	k8xC	nebo
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vypořádalo	vypořádat	k5eAaPmAgNnS	vypořádat
s	s	k7c7	s
následky	následek	k1gInPc7	následek
<g/>
.	.	kIx.	.
</s>
<s>
Nezmírňované	zmírňovaný	k2eNgFnPc1d1	zmírňovaný
změny	změna	k1gFnPc1	změna
klimatu	klima	k1gNnSc2	klima
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
budoucí	budoucí	k2eAgFnPc4d1	budoucí
změny	změna	k1gFnPc4	změna
klimatu	klima	k1gNnSc2	klima
bez	bez	k7c2	bez
účinných	účinný	k2eAgFnPc2d1	účinná
snah	snaha	k1gFnPc2	snaha
o	o	k7c6	o
omezení	omezení	k1gNnSc6	omezení
emisí	emise	k1gFnPc2	emise
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
<g/>
)	)	kIx)	)
by	by	kYmCp3nS	by
v	v	k7c6	v
dlouhodobém	dlouhodobý	k2eAgInSc6d1	dlouhodobý
horizontu	horizont	k1gInSc6	horizont
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
překročily	překročit	k5eAaPmAgFnP	překročit
schopnost	schopnost	k1gFnSc4	schopnost
přírodních	přírodní	k2eAgInPc2d1	přírodní
<g/>
,	,	kIx,	,
řízených	řízený	k2eAgInPc2d1	řízený
a	a	k8xC	a
lidských	lidský	k2eAgInPc2d1	lidský
systémů	systém	k1gInPc2	systém
se	se	k3xPyFc4	se
přizpůsobit	přizpůsobit	k5eAaPmF	přizpůsobit
<g/>
.	.	kIx.	.
<g/>
Ekologické	ekologický	k2eAgFnPc1d1	ekologická
organizace	organizace	k1gFnPc1	organizace
a	a	k8xC	a
osobnosti	osobnost	k1gFnPc1	osobnost
veřejného	veřejný	k2eAgInSc2d1	veřejný
života	život	k1gInSc2	život
zdůrazňují	zdůrazňovat	k5eAaImIp3nP	zdůrazňovat
změny	změna	k1gFnPc1	změna
klimatu	klima	k1gNnSc2	klima
a	a	k8xC	a
rizika	riziko	k1gNnSc2	riziko
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
s	s	k7c7	s
sebou	se	k3xPyFc7	se
nesou	nést	k5eAaImIp3nP	nést
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
současnou	současný	k2eAgFnSc7d1	současná
podporou	podpora	k1gFnSc7	podpora
přizpůsobování	přizpůsobování	k1gNnSc2	přizpůsobování
se	se	k3xPyFc4	se
změnám	změna	k1gFnPc3	změna
potřeb	potřeba	k1gFnPc2	potřeba
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
a	a	k8xC	a
snižování	snižování	k1gNnSc2	snižování
emisí	emise	k1gFnPc2	emise
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
vědci	vědec	k1gMnPc1	vědec
zpochybňují	zpochybňovat	k5eAaImIp3nP	zpochybňovat
proveditelnost	proveditelnost	k1gFnSc4	proveditelnost
adaptace	adaptace	k1gFnSc2	adaptace
na	na	k7c4	na
klimatické	klimatický	k2eAgFnPc4d1	klimatická
změny	změna	k1gFnPc4	změna
<g/>
,	,	kIx,	,
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
dojde	dojít	k5eAaPmIp3nS	dojít
jen	jen	k9	jen
k	k	k7c3	k
malému	malý	k2eAgNnSc3d1	malé
snížení	snížení	k1gNnSc3	snížení
emisí	emise	k1gFnPc2	emise
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geoengineering	Geoengineering	k1gInSc4	Geoengineering
==	==	k?	==
</s>
</p>
<p>
<s>
Geoengineering	Geoengineering	k1gInSc1	Geoengineering
(	(	kIx(	(
<g/>
klimatické	klimatický	k2eAgNnSc1d1	klimatické
inženýrství	inženýrství	k1gNnSc1	inženýrství
<g/>
,	,	kIx,	,
klimatické	klimatický	k2eAgFnPc1d1	klimatická
intervence	intervence	k1gFnPc1	intervence
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
výraz	výraz	k1gInSc1	výraz
používaný	používaný	k2eAgInSc1d1	používaný
pro	pro	k7c4	pro
záměrné	záměrný	k2eAgFnPc4d1	záměrná
změny	změna	k1gFnPc4	změna
klimatu	klima	k1gNnSc2	klima
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
zkoumána	zkoumán	k2eAgFnSc1d1	zkoumána
jako	jako	k8xS	jako
možná	možný	k2eAgFnSc1d1	možná
reakce	reakce	k1gFnSc1	reakce
na	na	k7c4	na
globální	globální	k2eAgNnSc4d1	globální
oteplování	oteplování	k1gNnSc4	oteplování
<g/>
,	,	kIx,	,
např.	např.	kA	např.
NASA	NASA	kA	NASA
a	a	k8xC	a
Royal	Royal	k1gInSc4	Royal
Society	societa	k1gFnSc2	societa
<g/>
.	.	kIx.	.
</s>
<s>
Techniky	technika	k1gFnPc1	technika
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
jsou	být	k5eAaImIp3nP	být
předmětem	předmět	k1gInSc7	předmět
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
spadají	spadat	k5eAaImIp3nP	spadat
do	do	k7c2	do
kategorií	kategorie	k1gFnPc2	kategorie
ovládání	ovládání	k1gNnSc2	ovládání
slunečního	sluneční	k2eAgNnSc2d1	sluneční
záření	záření	k1gNnSc2	záření
a	a	k8xC	a
odstraňování	odstraňování	k1gNnSc2	odstraňování
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
ale	ale	k9	ale
navrženy	navrhnout	k5eAaPmNgInP	navrhnout
i	i	k8xC	i
různá	různý	k2eAgNnPc4d1	různé
jiná	jiný	k2eAgNnPc4d1	jiné
opatření	opatření	k1gNnPc4	opatření
<g/>
.	.	kIx.	.
</s>
<s>
Studie	studie	k1gFnSc1	studie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
zkoumala	zkoumat	k5eAaImAgFnS	zkoumat
nejčastější	častý	k2eAgFnPc4d3	nejčastější
metody	metoda	k1gFnPc4	metoda
klimatického	klimatický	k2eAgNnSc2d1	klimatické
inženýrství	inženýrství	k1gNnSc2	inženýrství
a	a	k8xC	a
dospěla	dochvít	k5eAaPmAgFnS	dochvít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
buď	buď	k8xC	buď
neúčinné	účinný	k2eNgFnPc1d1	neúčinná
nebo	nebo	k8xC	nebo
mají	mít	k5eAaImIp3nP	mít
potenciálně	potenciálně	k6eAd1	potenciálně
závažné	závažný	k2eAgInPc4d1	závažný
vedlejší	vedlejší	k2eAgInPc4d1	vedlejší
účinky	účinek	k1gInPc4	účinek
a	a	k8xC	a
nelze	lze	k6eNd1	lze
je	být	k5eAaImIp3nS	být
zastavit	zastavit	k5eAaPmF	zastavit
bez	bez	k7c2	bez
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
způsobily	způsobit	k5eAaPmAgInP	způsobit
rychlou	rychlý	k2eAgFnSc4d1	rychlá
změnu	změna	k1gFnSc4	změna
klimatu	klima	k1gNnSc2	klima
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Společnost	společnost	k1gFnSc1	společnost
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Politické	politický	k2eAgFnPc1d1	politická
diskuse	diskuse	k1gFnPc1	diskuse
===	===	k?	===
</s>
</p>
<p>
<s>
Politickou	politický	k2eAgFnSc7d1	politická
reakcí	reakce	k1gFnSc7	reakce
na	na	k7c4	na
vědecké	vědecký	k2eAgFnPc4d1	vědecká
zprávy	zpráva	k1gFnPc4	zpráva
o	o	k7c6	o
globálním	globální	k2eAgNnSc6d1	globální
oteplování	oteplování	k1gNnSc6	oteplování
je	být	k5eAaImIp3nS	být
Rámcová	rámcový	k2eAgFnSc1d1	rámcová
úmluva	úmluva	k1gFnSc1	úmluva
OSN	OSN	kA	OSN
o	o	k7c6	o
změně	změna	k1gFnSc6	změna
klimatu	klima	k1gNnSc2	klima
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
ratifikovalo	ratifikovat	k5eAaBmAgNnS	ratifikovat
již	již	k6eAd1	již
197	[number]	k4	197
států	stát	k1gInPc2	stát
a	a	k8xC	a
subjektů	subjekt	k1gInPc2	subjekt
(	(	kIx(	(
<g/>
všechny	všechen	k3xTgFnPc4	všechen
členské	členský	k2eAgFnPc4d1	členská
země	zem	k1gFnPc4	zem
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
také	také	k9	také
Niue	Niu	k1gInPc1	Niu
<g/>
,	,	kIx,	,
Cookovy	Cookův	k2eAgInPc1d1	Cookův
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
,	,	kIx,	,
Stát	stát	k1gInSc1	stát
Palestina	Palestina	k1gFnSc1	Palestina
a	a	k8xC	a
Evropská	evropský	k2eAgFnSc1d1	Evropská
unie	unie	k1gFnSc1	unie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
této	tento	k3xDgFnSc2	tento
konvence	konvence	k1gFnSc2	konvence
je	být	k5eAaImIp3nS	být
zabránit	zabránit	k5eAaPmF	zabránit
změnám	změna	k1gFnPc3	změna
klimatu	klima	k1gNnSc2	klima
<g/>
,	,	kIx,	,
způsobeným	způsobený	k2eAgMnSc7d1	způsobený
člověkem	člověk	k1gMnSc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Signatářské	signatářský	k2eAgFnPc1d1	signatářská
země	zem	k1gFnPc1	zem
Rámcové	rámcový	k2eAgFnSc2d1	rámcová
úmluvy	úmluva	k1gFnSc2	úmluva
s	s	k7c7	s
shodly	shodnout	k5eAaBmAgFnP	shodnout
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
přijmout	přijmout	k5eAaPmF	přijmout
rázná	rázný	k2eAgNnPc4d1	rázné
opatření	opatření	k1gNnPc4	opatření
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
emisí	emise	k1gFnPc2	emise
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
a	a	k8xC	a
přijaly	přijmout	k5eAaPmAgInP	přijmout
i	i	k9	i
řadu	řada	k1gFnSc4	řada
opatření	opatření	k1gNnSc2	opatření
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
omezení	omezení	k1gNnSc3	omezení
<g/>
.	.	kIx.	.
</s>
<s>
Panuje	panovat	k5eAaImIp3nS	panovat
shoda	shoda	k1gFnSc1	shoda
<g/>
,	,	kIx,	,
že	že	k8xS	že
budoucí	budoucí	k2eAgNnSc1d1	budoucí
globální	globální	k2eAgNnSc1d1	globální
oteplení	oteplení	k1gNnSc1	oteplení
by	by	kYmCp3nS	by
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
omezeno	omezit	k5eAaPmNgNnS	omezit
na	na	k7c4	na
hodnotu	hodnota	k1gFnSc4	hodnota
pod	pod	k7c7	pod
2,0	[number]	k4	2,0
°	°	k?	°
<g/>
C	C	kA	C
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
hodnotám	hodnota	k1gFnPc3	hodnota
v	v	k7c6	v
předindustriálním	předindustriální	k2eAgNnSc6d1	předindustriální
období	období	k1gNnSc6	období
<g/>
.	.	kIx.	.
</s>
<s>
Zprávy	zpráva	k1gFnPc1	zpráva
publikované	publikovaný	k2eAgFnPc1d1	publikovaná
Programem	program	k1gInSc7	program
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
a	a	k8xC	a
Mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
energetickou	energetický	k2eAgFnSc7d1	energetická
agenturou	agentura	k1gFnSc7	agentura
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
obavy	obava	k1gFnPc1	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
doposud	doposud	k6eAd1	doposud
vynaložené	vynaložený	k2eAgFnPc1d1	vynaložená
snahy	snaha	k1gFnPc1	snaha
k	k	k7c3	k
dosažení	dosažení	k1gNnSc3	dosažení
cíle	cíl	k1gInSc2	cíl
maximálního	maximální	k2eAgNnSc2d1	maximální
oteplení	oteplení	k1gNnSc2	oteplení
o	o	k7c4	o
2	[number]	k4	2
°	°	k?	°
<g/>
C	C	kA	C
nemusí	muset	k5eNaImIp3nP	muset
být	být	k5eAaImF	být
dostatečné	dostatečný	k2eAgInPc1d1	dostatečný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Primární	primární	k2eAgFnSc7d1	primární
světovou	světový	k2eAgFnSc7d1	světová
dohodou	dohoda	k1gFnSc7	dohoda
o	o	k7c6	o
boji	boj	k1gInSc6	boj
se	s	k7c7	s
změnou	změna	k1gFnSc7	změna
klimatu	klima	k1gNnSc2	klima
je	být	k5eAaImIp3nS	být
Kjótský	Kjótský	k2eAgInSc4d1	Kjótský
protokol	protokol	k1gInSc4	protokol
k	k	k7c3	k
Rámcové	rámcový	k2eAgFnSc3d1	rámcová
úmluvě	úmluva	k1gFnSc3	úmluva
OSN	OSN	kA	OSN
o	o	k7c6	o
změně	změna	k1gFnSc6	změna
klimatu	klima	k1gNnSc2	klima
<g/>
.	.	kIx.	.
</s>
<s>
Státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
ratifikovaly	ratifikovat	k5eAaBmAgFnP	ratifikovat
tuto	tento	k3xDgFnSc4	tento
dohodu	dohoda	k1gFnSc4	dohoda
<g/>
,	,	kIx,	,
souhlasily	souhlasit	k5eAaImAgFnP	souhlasit
s	s	k7c7	s
omezením	omezení	k1gNnSc7	omezení
svých	svůj	k3xOyFgFnPc2	svůj
emisí	emise	k1gFnPc2	emise
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
a	a	k8xC	a
pěti	pět	k4xCc2	pět
dalších	další	k2eAgInPc2d1	další
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
zavázaly	zavázat	k5eAaPmAgInP	zavázat
k	k	k7c3	k
obchodu	obchod	k1gInSc3	obchod
s	s	k7c7	s
emisemi	emise	k1gFnPc7	emise
v	v	k7c6	v
případě	případ	k1gInSc6	případ
<g/>
,	,	kIx,	,
že	že	k8xS	že
nesníží	snížit	k5eNaPmIp3nP	snížit
své	svůj	k3xOyFgFnPc4	svůj
emise	emise	k1gFnPc4	emise
těchto	tento	k3xDgInPc2	tento
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2015	[number]	k4	2015
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
závěr	závěr	k1gInSc4	závěr
Klimatické	klimatický	k2eAgFnSc2d1	klimatická
konference	konference	k1gFnSc2	konference
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
tzv.	tzv.	kA	tzv.
Pařížská	pařížský	k2eAgFnSc1d1	Pařížská
dohoda	dohoda	k1gFnSc1	dohoda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
omezit	omezit	k5eAaPmF	omezit
emise	emise	k1gFnPc4	emise
CO2	CO2	k1gFnPc2	CO2
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2020	[number]	k4	2020
a	a	k8xC	a
navázat	navázat	k5eAaPmF	navázat
tak	tak	k6eAd1	tak
na	na	k7c4	na
Kjótský	Kjótský	k2eAgInSc4d1	Kjótský
protokol	protokol	k1gInSc4	protokol
<g/>
.	.	kIx.	.
</s>
<s>
Dohoda	dohoda	k1gFnSc1	dohoda
byla	být	k5eAaImAgFnS	být
schválena	schválit	k5eAaPmNgFnS	schválit
všemi	všecek	k3xTgFnPc7	všecek
195	[number]	k4	195
smluvními	smluvní	k2eAgFnPc7d1	smluvní
stranami	strana	k1gFnPc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Stanovuje	stanovovat	k5eAaImIp3nS	stanovovat
závazky	závazek	k1gInPc4	závazek
všech	všecek	k3xTgFnPc2	všecek
smluvních	smluvní	k2eAgFnPc2d1	smluvní
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
největších	veliký	k2eAgMnPc2d3	veliký
světových	světový	k2eAgMnPc2d1	světový
producentů	producent	k1gMnPc2	producent
emisí	emise	k1gFnPc2	emise
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
Čína	Čína	k1gFnSc1	Čína
<g/>
,	,	kIx,	,
USA	USA	kA	USA
či	či	k8xC	či
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Dohoda	dohoda	k1gFnSc1	dohoda
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Mezivládní	mezivládní	k2eAgInSc1d1	mezivládní
panel	panel	k1gInSc1	panel
pro	pro	k7c4	pro
změny	změna	k1gFnPc4	změna
klimatu	klima	k1gNnSc2	klima
====	====	k?	====
</s>
</p>
<p>
<s>
Ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
otázek	otázka	k1gFnPc2	otázka
změny	změna	k1gFnSc2	změna
klimatu	klima	k1gNnSc2	klima
založil	založit	k5eAaPmAgInS	založit
Program	program	k1gInSc1	program
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
životní	životní	k2eAgNnSc4d1	životní
prostředí	prostředí	k1gNnSc4	prostředí
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
se	s	k7c7	s
Světovou	světový	k2eAgFnSc7d1	světová
meteorologickou	meteorologický	k2eAgFnSc7d1	meteorologická
organizací	organizace	k1gFnSc7	organizace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
Mezivládní	mezivládní	k2eAgInSc1d1	mezivládní
panel	panel	k1gInSc1	panel
pro	pro	k7c4	pro
změny	změna	k1gFnPc4	změna
klimatu	klima	k1gNnSc2	klima
(	(	kIx(	(
<g/>
IPCC	IPCC	kA	IPCC
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
vědecký	vědecký	k2eAgInSc1d1	vědecký
orgán	orgán	k1gInSc1	orgán
pod	pod	k7c7	pod
záštitou	záštita	k1gFnSc7	záštita
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
panel	panel	k1gInSc1	panel
v	v	k7c6	v
období	období	k1gNnSc6	období
od	od	k7c2	od
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
2014	[number]	k4	2014
vydal	vydat	k5eAaPmAgInS	vydat
svou	svůj	k3xOyFgFnSc4	svůj
již	již	k6eAd1	již
Pátou	pátá	k1gFnSc4	pátá
hodnotící	hodnotící	k2eAgFnSc4d1	hodnotící
zprávu	zpráva	k1gFnSc4	zpráva
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yQgFnPc4	který
shrnuje	shrnovat	k5eAaImIp3nS	shrnovat
současné	současný	k2eAgInPc4d1	současný
vědecké	vědecký	k2eAgInPc4d1	vědecký
poznatky	poznatek	k1gInPc4	poznatek
<g/>
.	.	kIx.	.
</s>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
vědci	vědec	k1gMnPc1	vědec
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
na	na	k7c4	na
95	[number]	k4	95
<g/>
–	–	k?	–
<g/>
100	[number]	k4	100
%	%	kIx~	%
jisti	jist	k2eAgMnPc1d1	jist
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
současného	současný	k2eAgNnSc2d1	současné
globálního	globální	k2eAgNnSc2d1	globální
oteplování	oteplování	k1gNnSc2	oteplování
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
zvýšenými	zvýšený	k2eAgFnPc7d1	zvýšená
koncentracemi	koncentrace	k1gFnPc7	koncentrace
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
a	a	k8xC	a
že	že	k8xS	že
k	k	k7c3	k
navyšování	navyšování	k1gNnSc3	navyšování
koncentrací	koncentrace	k1gFnPc2	koncentrace
dochází	docházet	k5eAaImIp3nS	docházet
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
lidských	lidský	k2eAgFnPc2d1	lidská
aktivit	aktivita	k1gFnPc2	aktivita
a	a	k8xC	a
že	že	k8xS	že
primární	primární	k2eAgFnSc7d1	primární
příčinou	příčina	k1gFnSc7	příčina
nárůstu	nárůst	k1gInSc2	nárůst
teplot	teplota	k1gFnPc2	teplota
jsou	být	k5eAaImIp3nP	být
emise	emise	k1gFnSc2	emise
CO2	CO2	k1gFnSc2	CO2
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
lidské	lidský	k2eAgFnSc2d1	lidská
činnosti	činnost	k1gFnSc2	činnost
především	především	k6eAd1	především
spalováním	spalování	k1gNnSc7	spalování
fosilních	fosilní	k2eAgNnPc2d1	fosilní
paliv	palivo	k1gNnPc2	palivo
a	a	k8xC	a
změnami	změna	k1gFnPc7	změna
využití	využití	k1gNnSc2	využití
krajiny	krajina	k1gFnSc2	krajina
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
odlesňování	odlesňování	k1gNnSc1	odlesňování
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
nanejvýš	nanejvýš	k6eAd1	nanejvýš
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
příčinou	příčina	k1gFnSc7	příčina
globálního	globální	k2eAgNnSc2d1	globální
oteplování	oteplování	k1gNnSc2	oteplování
lidská	lidský	k2eAgFnSc1d1	lidská
činnost	činnost	k1gFnSc1	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
vzroste	vzrůst	k5eAaPmIp3nS	vzrůst
do	do	k7c2	do
konce	konec	k1gInSc2	konec
století	století	k1gNnSc2	století
o	o	k7c4	o
0,3	[number]	k4	0,3
až	až	k9	až
4,8	[number]	k4	4,8
stupně	stupeň	k1gInSc2	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c4	na
množství	množství	k1gNnPc4	množství
spálených	spálený	k2eAgNnPc2d1	spálené
fosilních	fosilní	k2eAgNnPc2d1	fosilní
paliv	palivo	k1gNnPc2	palivo
<g/>
.	.	kIx.	.
</s>
<s>
Teplota	teplota	k1gFnSc1	teplota
svrchní	svrchní	k2eAgFnSc2d1	svrchní
vrstvy	vrstva	k1gFnSc2	vrstva
oceánů	oceán	k1gInPc2	oceán
v	v	k7c6	v
období	období	k1gNnSc6	období
1971	[number]	k4	1971
až	až	k9	až
2010	[number]	k4	2010
stoupla	stoupnout	k5eAaPmAgFnS	stoupnout
<g/>
.	.	kIx.	.
</s>
<s>
Oceány	oceán	k1gInPc1	oceán
se	se	k3xPyFc4	se
budou	být	k5eAaImBp3nP	být
zahřívat	zahřívat	k5eAaImF	zahřívat
i	i	k9	i
nadále	nadále	k6eAd1	nadále
a	a	k8xC	a
teplo	teplo	k6eAd1	teplo
bude	být	k5eAaImBp3nS	být
pronikat	pronikat	k5eAaImF	pronikat
od	od	k7c2	od
jejich	jejich	k3xOp3gInSc2	jejich
povrchu	povrch	k1gInSc2	povrch
do	do	k7c2	do
hloubky	hloubka	k1gFnSc2	hloubka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vědecká	vědecký	k2eAgFnSc1d1	vědecká
diskuse	diskuse	k1gFnSc1	diskuse
===	===	k?	===
</s>
</p>
<p>
<s>
Vědecká	vědecký	k2eAgFnSc1d1	vědecká
diskuse	diskuse	k1gFnSc1	diskuse
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
ve	v	k7c6	v
vědeckých	vědecký	k2eAgInPc6d1	vědecký
článcích	článek	k1gInPc6	článek
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
recenzovány	recenzován	k2eAgFnPc4d1	recenzována
a	a	k8xC	a
hodnoceny	hodnocen	k2eAgFnPc4d1	hodnocena
vědci	vědec	k1gMnPc7	vědec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
pracují	pracovat	k5eAaImIp3nP	pracovat
v	v	k7c6	v
příslušných	příslušný	k2eAgInPc6d1	příslušný
oborech	obor	k1gInPc6	obor
<g/>
.	.	kIx.	.
</s>
<s>
Vědecký	vědecký	k2eAgInSc1d1	vědecký
konsensus	konsensus	k1gInSc1	konsensus
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
uvedl	uvést	k5eAaPmAgMnS	uvést
v	v	k7c6	v
páté	pátý	k4xOgFnSc6	pátý
hodnotící	hodnotící	k2eAgFnSc6d1	hodnotící
zprávě	zpráva	k1gFnSc6	zpráva
IPCC	IPCC	kA	IPCC
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidský	lidský	k2eAgInSc1d1	lidský
vliv	vliv	k1gInSc1	vliv
byl	být	k5eAaImAgInS	být
dominantní	dominantní	k2eAgFnSc7d1	dominantní
příčinou	příčina	k1gFnSc7	příčina
pozorovaného	pozorovaný	k2eAgNnSc2d1	pozorované
oteplování	oteplování	k1gNnSc2	oteplování
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
Americké	americký	k2eAgFnSc2d1	americká
národní	národní	k2eAgFnSc2d1	národní
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
většina	většina	k1gFnSc1	většina
vědců	vědec	k1gMnPc2	vědec
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
shodla	shodnout	k5eAaBmAgFnS	shodnout
na	na	k7c6	na
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pozorované	pozorovaný	k2eAgNnSc4d1	pozorované
oteplování	oteplování	k1gNnSc4	oteplování
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
desetiletích	desetiletí	k1gNnPc6	desetiletí
bylo	být	k5eAaImAgNnS	být
primárně	primárně	k6eAd1	primárně
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
lidskými	lidský	k2eAgFnPc7d1	lidská
aktivitami	aktivita	k1gFnPc7	aktivita
zvyšujícími	zvyšující	k2eAgFnPc7d1	zvyšující
množství	množství	k1gNnSc1	množství
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
v	v	k7c6	v
atmosféře	atmosféra	k1gFnSc6	atmosféra
<g/>
.	.	kIx.	.
</s>
<s>
Královská	královský	k2eAgFnSc1d1	královská
společnost	společnost	k1gFnSc1	společnost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
když	když	k8xS	když
drtivá	drtivý	k2eAgFnSc1d1	drtivá
většina	většina	k1gFnSc1	většina
vědců	vědec	k1gMnPc2	vědec
souhlasila	souhlasit	k5eAaImAgFnS	souhlasit
s	s	k7c7	s
hlavními	hlavní	k2eAgInPc7d1	hlavní
body	bod	k1gInPc7	bod
<g/>
,	,	kIx,	,
někteří	některý	k3yIgMnPc1	některý
jednotlivci	jednotlivec	k1gMnPc1	jednotlivec
a	a	k8xC	a
organizace	organizace	k1gFnPc1	organizace
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
postavily	postavit	k5eAaPmAgFnP	postavit
proti	proti	k7c3	proti
konsensu	konsens	k1gInSc3	konsens
o	o	k7c6	o
naléhavých	naléhavý	k2eAgNnPc6d1	naléhavé
opatřeních	opatření	k1gNnPc6	opatření
potřebných	potřebný	k2eAgInPc2d1	potřebný
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
emisí	emise	k1gFnPc2	emise
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pokusily	pokusit	k5eAaPmAgFnP	pokusit
podkopat	podkopat	k5eAaPmF	podkopat
vědu	věda	k1gFnSc4	věda
a	a	k8xC	a
práci	práce	k1gFnSc4	práce
IPCC	IPCC	kA	IPCC
<g/>
.	.	kIx.	.
</s>
<s>
Národní	národní	k2eAgFnSc2d1	národní
vědecké	vědecký	k2eAgFnSc2d1	vědecká
akademie	akademie	k1gFnSc2	akademie
vyzývají	vyzývat	k5eAaImIp3nP	vyzývat
světové	světový	k2eAgMnPc4d1	světový
vůdce	vůdce	k1gMnPc4	vůdce
k	k	k7c3	k
politikám	politika	k1gFnPc3	politika
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
globálních	globální	k2eAgFnPc2d1	globální
emisí	emise	k1gFnPc2	emise
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
vydal	vydat	k5eAaPmAgMnS	vydat
IPCC	IPCC	kA	IPCC
zprávu	zpráva	k1gFnSc4	zpráva
SR	SR	kA	SR
<g/>
15	[number]	k4	15
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
varovala	varovat	k5eAaImAgFnS	varovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
nebude	být	k5eNaImBp3nS	být
současná	současný	k2eAgFnSc1d1	současná
míra	míra	k1gFnSc1	míra
emisí	emise	k1gFnPc2	emise
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
zmírněna	zmírnit	k5eAaPmNgFnS	zmírnit
<g/>
,	,	kIx,	,
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2040	[number]	k4	2040
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
mohly	moct	k5eAaImAgFnP	moct
objevit	objevit	k5eAaPmF	objevit
závažné	závažný	k2eAgFnSc2d1	závažná
krize	krize	k1gFnSc2	krize
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
planeta	planeta	k1gFnSc1	planeta
zahřeje	zahřát	k5eAaPmIp3nS	zahřát
o	o	k7c4	o
1,5	[number]	k4	1,5
stupně	stupeň	k1gInSc2	stupeň
Celsia	Celsius	k1gMnSc2	Celsius
<g/>
.	.	kIx.	.
</s>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
předcházení	předcházený	k2eAgMnPc1d1	předcházený
takovým	takový	k3xDgFnPc3	takový
krizím	krize	k1gFnPc3	krize
bude	být	k5eAaImBp3nS	být
vyžadovat	vyžadovat	k5eAaImF	vyžadovat
rychlou	rychlý	k2eAgFnSc4d1	rychlá
transformaci	transformace	k1gFnSc4	transformace
globální	globální	k2eAgFnSc2d1	globální
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
"	"	kIx"	"
<g/>
nemá	mít	k5eNaImIp3nS	mít
žádný	žádný	k3yNgInSc4	žádný
dokumentovaný	dokumentovaný	k2eAgInSc4d1	dokumentovaný
historický	historický	k2eAgInSc4d1	historický
precedens	precedens	k1gNnSc4	precedens
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
Ve	v	k7c6	v
vědecké	vědecký	k2eAgFnSc6d1	vědecká
literatuře	literatura	k1gFnSc6	literatura
existuje	existovat	k5eAaImIp3nS	existovat
silná	silný	k2eAgFnSc1d1	silná
shoda	shoda	k1gFnSc1	shoda
<g/>
,	,	kIx,	,
že	že	k8xS	že
globální	globální	k2eAgFnPc1d1	globální
povrchové	povrchový	k2eAgFnPc1d1	povrchová
teploty	teplota	k1gFnPc1	teplota
se	se	k3xPyFc4	se
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
desetiletích	desetiletí	k1gNnPc6	desetiletí
zvýšily	zvýšit	k5eAaPmAgFnP	zvýšit
a	a	k8xC	a
že	že	k8xS	že
trend	trend	k1gInSc1	trend
je	být	k5eAaImIp3nS	být
způsoben	způsobit	k5eAaPmNgInS	způsobit
hlavně	hlavně	k6eAd1	hlavně
emisemi	emise	k1gFnPc7	emise
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
způsobených	způsobený	k2eAgInPc2d1	způsobený
lidmi	člověk	k1gMnPc7	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Žádný	žádný	k3yNgInSc1	žádný
vědecký	vědecký	k2eAgInSc1d1	vědecký
orgán	orgán	k1gInSc1	orgán
s	s	k7c7	s
národním	národní	k2eAgInSc7d1	národní
nebo	nebo	k8xC	nebo
mezinárodním	mezinárodní	k2eAgInSc7d1	mezinárodní
postojem	postoj	k1gInSc7	postoj
tento	tento	k3xDgInSc1	tento
názor	názor	k1gInSc1	názor
nepopřel	popřít	k5eNaPmAgInS	popřít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
"	"	kIx"	"
<g/>
Druhé	druhý	k4xOgNnSc1	druhý
varování	varování	k1gNnSc1	varování
pro	pro	k7c4	pro
lidstvo	lidstvo	k1gNnSc4	lidstvo
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
15	[number]	k4	15
364	[number]	k4	364
vědců	vědec	k1gMnPc2	vědec
z	z	k7c2	z
184	[number]	k4	184
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
uvedlo	uvést	k5eAaPmAgNnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
současná	současný	k2eAgFnSc1d1	současná
trajektorie	trajektorie	k1gFnPc1	trajektorie
potenciálně	potenciálně	k6eAd1	potenciálně
katastrofické	katastrofický	k2eAgFnPc4d1	katastrofická
změny	změna	k1gFnPc4	změna
klimatu	klima	k1gNnSc2	klima
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
rostoucích	rostoucí	k2eAgInPc2d1	rostoucí
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
ze	z	k7c2	z
spalování	spalování	k1gNnSc2	spalování
fosilních	fosilní	k2eAgNnPc2d1	fosilní
paliv	palivo	k1gNnPc2	palivo
<g/>
,	,	kIx,	,
odlesňování	odlesňování	k1gNnSc1	odlesňování
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
produkce	produkce	k1gFnSc2	produkce
–	–	k?	–
zejména	zejména	k9	zejména
ze	z	k7c2	z
zemědělských	zemědělský	k2eAgMnPc2d1	zemědělský
přežvýkavců	přežvýkavec	k1gMnPc2	přežvýkavec
pro	pro	k7c4	pro
spotřebu	spotřeba	k1gFnSc4	spotřeba
masa	maso	k1gNnSc2	maso
"	"	kIx"	"
<g/>
je	být	k5eAaImIp3nS	být
obzvlášť	obzvlášť	k6eAd1	obzvlášť
znepokojující	znepokojující	k2eAgFnSc1d1	znepokojující
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Studie	studie	k1gFnSc1	studie
z	z	k7c2	z
července	červenec	k1gInSc2	červenec
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
zveřejněná	zveřejněný	k2eAgFnSc1d1	zveřejněná
v	v	k7c6	v
Environmental	Environmental	k1gMnSc1	Environmental
Research	Researcha	k1gFnPc2	Researcha
Letters	Lettersa	k1gFnPc2	Lettersa
<g/>
,	,	kIx,	,
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejvýznamnější	významný	k2eAgFnSc1d3	nejvýznamnější
akce	akce	k1gFnSc1	akce
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
by	by	kYmCp3nP	by
jednotlivci	jednotlivec	k1gMnPc1	jednotlivec
mohli	moct	k5eAaImAgMnP	moct
udělat	udělat	k5eAaPmF	udělat
ke	k	k7c3	k
zmírnění	zmírnění	k1gNnSc3	zmírnění
své	svůj	k3xOyFgFnSc2	svůj
vlastní	vlastní	k2eAgFnSc2d1	vlastní
uhlíkové	uhlíkový	k2eAgFnSc2d1	uhlíková
stopy	stopa	k1gFnSc2	stopa
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
mít	mít	k5eAaImF	mít
méně	málo	k6eAd2	málo
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
vzdát	vzdát	k5eAaPmF	vzdát
se	se	k3xPyFc4	se
vlastního	vlastní	k2eAgNnSc2d1	vlastní
auta	auto	k1gNnSc2	auto
<g/>
,	,	kIx,	,
nepoužívat	používat	k5eNaImF	používat
leteckou	letecký	k2eAgFnSc4d1	letecká
dopravu	doprava	k1gFnSc4	doprava
a	a	k8xC	a
přejít	přejít	k5eAaPmF	přejít
na	na	k7c4	na
vegetariánskou	vegetariánský	k2eAgFnSc4d1	vegetariánská
stravu	strava	k1gFnSc4	strava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Veřejné	veřejný	k2eAgNnSc4d1	veřejné
mínění	mínění	k1gNnSc4	mínění
a	a	k8xC	a
spory	spor	k1gInPc4	spor
===	===	k?	===
</s>
</p>
<p>
<s>
Diskuse	diskuse	k1gFnSc1	diskuse
o	o	k7c6	o
globálním	globální	k2eAgNnSc6d1	globální
oteplování	oteplování	k1gNnSc6	oteplování
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
řady	řada	k1gFnPc4	řada
sporů	spor	k1gInPc2	spor
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
daleko	daleko	k6eAd1	daleko
výraznější	výrazný	k2eAgFnSc1d2	výraznější
než	než	k8xS	než
ve	v	k7c6	v
vědecké	vědecký	k2eAgFnSc6d1	vědecká
literatuře	literatura	k1gFnSc6	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
spory	spor	k1gInPc1	spor
se	se	k3xPyFc4	se
týkají	týkat	k5eAaImIp3nP	týkat
povahy	povaha	k1gFnSc2	povaha
<g/>
,	,	kIx,	,
příčin	příčina	k1gFnPc2	příčina
a	a	k8xC	a
důsledků	důsledek	k1gInPc2	důsledek
globálního	globální	k2eAgNnSc2d1	globální
oteplování	oteplování	k1gNnSc2	oteplování
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
sporné	sporný	k2eAgFnPc4d1	sporná
otázky	otázka	k1gFnPc4	otázka
patří	patřit	k5eAaImIp3nP	patřit
příčiny	příčina	k1gFnPc1	příčina
zvýšené	zvýšený	k2eAgFnSc2d1	zvýšená
globální	globální	k2eAgFnSc2d1	globální
průměrné	průměrný	k2eAgFnSc2d1	průměrná
teploty	teplota	k1gFnSc2	teplota
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
oteplovací	oteplovací	k2eAgInSc1d1	oteplovací
trend	trend	k1gInSc1	trend
bezprecedentní	bezprecedentní	k2eAgMnSc1d1	bezprecedentní
nebo	nebo	k8xC	nebo
v	v	k7c6	v
normálních	normální	k2eAgFnPc6d1	normální
klimatických	klimatický	k2eAgFnPc6d1	klimatická
mezích	mez	k1gFnPc6	mez
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
k	k	k7c3	k
němu	on	k3xPp3gNnSc3	on
člověk	člověk	k1gMnSc1	člověk
významně	významně	k6eAd1	významně
přispěl	přispět	k5eAaPmAgMnS	přispět
a	a	k8xC	a
zda	zda	k8xS	zda
je	být	k5eAaImIp3nS	být
toto	tento	k3xDgNnSc4	tento
zvýšení	zvýšení	k1gNnSc4	zvýšení
zcela	zcela	k6eAd1	zcela
nebo	nebo	k8xC	nebo
částečně	částečně	k6eAd1	částečně
artefakt	artefakt	k1gInSc4	artefakt
špatných	špatný	k2eAgNnPc2d1	špatné
měření	měření	k1gNnPc2	měření
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
spory	spor	k1gInPc1	spor
se	se	k3xPyFc4	se
týkají	týkat	k5eAaImIp3nP	týkat
odhadu	odhad	k1gInSc3	odhad
klimatické	klimatický	k2eAgFnSc2d1	klimatická
citlivosti	citlivost	k1gFnSc2	citlivost
<g/>
,	,	kIx,	,
předpovědí	předpověď	k1gFnPc2	předpověď
dalšího	další	k2eAgNnSc2d1	další
oteplování	oteplování	k1gNnSc2	oteplování
a	a	k8xC	a
také	také	k9	také
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgNnSc1	jaký
budou	být	k5eAaImBp3nP	být
následky	následek	k1gInPc1	následek
globálního	globální	k2eAgNnSc2d1	globální
oteplování	oteplování	k1gNnSc2	oteplování
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
politiků	politik	k1gMnPc2	politik
<g/>
,	,	kIx,	,
především	především	k9	především
pravicových	pravicový	k2eAgMnPc2d1	pravicový
<g/>
,	,	kIx,	,
nesouhlasí	souhlasit	k5eNaImIp3nP	souhlasit
buď	buď	k8xC	buď
vůbec	vůbec	k9	vůbec
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
za	za	k7c4	za
oteplování	oteplování	k1gNnSc4	oteplování
mohl	moct	k5eAaImAgMnS	moct
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
pak	pak	k6eAd1	pak
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
proti	proti	k7c3	proti
jeho	jeho	k3xOp3gFnPc3	jeho
příčinám	příčina	k1gFnPc3	příčina
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
přijímána	přijímat	k5eAaImNgFnS	přijímat
jakákoliv	jakýkoliv	k3yIgFnSc1	jakýkoliv
opatření	opatření	k1gNnPc4	opatření
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ta	ten	k3xDgFnSc1	ten
by	by	kYmCp3nS	by
znamenala	znamenat	k5eAaImAgFnS	znamenat
omezení	omezení	k1gNnSc3	omezení
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
svobody	svoboda	k1gFnSc2	svoboda
<g/>
.	.	kIx.	.
<g/>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
začaly	začít	k5eAaPmAgFnP	začít
americké	americký	k2eAgFnPc1d1	americká
konzervativní	konzervativní	k2eAgFnPc1d1	konzervativní
think-tanky	thinkanka	k1gFnPc1	think-tanka
diskutovat	diskutovat	k5eAaImF	diskutovat
o	o	k7c6	o
legitimnosti	legitimnost	k1gFnSc6	legitimnost
globálního	globální	k2eAgNnSc2d1	globální
oteplování	oteplování	k1gNnSc2	oteplování
jako	jako	k8xC	jako
sociálního	sociální	k2eAgInSc2d1	sociální
problému	problém	k1gInSc2	problém
<g/>
.	.	kIx.	.
</s>
<s>
Zpochybňovaly	zpochybňovat	k5eAaImAgInP	zpochybňovat
vědecké	vědecký	k2eAgInPc1d1	vědecký
důkazy	důkaz	k1gInPc1	důkaz
<g/>
,	,	kIx,	,
argumentovaly	argumentovat	k5eAaImAgFnP	argumentovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
globální	globální	k2eAgNnSc4d1	globální
oteplování	oteplování	k1gNnSc4	oteplování
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
prospěšné	prospěšný	k2eAgNnSc1d1	prospěšné
<g/>
,	,	kIx,	,
a	a	k8xC	a
tvrdily	tvrdit	k5eAaImAgFnP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
navržená	navržený	k2eAgNnPc1d1	navržené
řešení	řešení	k1gNnPc1	řešení
by	by	kYmCp3nP	by
udělala	udělat	k5eAaPmAgNnP	udělat
více	hodně	k6eAd2	hodně
škod	škoda	k1gFnPc2	škoda
než	než	k8xS	než
užitku	užitek	k1gInSc2	užitek
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
popírají	popírat	k5eAaImIp3nP	popírat
aspekty	aspekt	k1gInPc1	aspekt
vědy	věda	k1gFnSc2	věda
o	o	k7c6	o
klimatických	klimatický	k2eAgFnPc6d1	klimatická
změnách	změna	k1gFnPc6	změna
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnSc1	organizace
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
liberální	liberální	k2eAgInSc1d1	liberální
Competitive	Competitiv	k1gInSc5	Competitiv
Enterprise	Enterpris	k1gInSc5	Enterpris
Institute	institut	k1gInSc5	institut
<g/>
,	,	kIx,	,
konzervativní	konzervativní	k2eAgMnPc1d1	konzervativní
komentátoři	komentátor	k1gMnPc1	komentátor
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
společnosti	společnost	k1gFnPc1	společnost
jako	jako	k8xS	jako
ExxonMobil	ExxonMobil	k1gFnPc1	ExxonMobil
napadly	napadnout	k5eAaPmAgFnP	napadnout
scénáře	scénář	k1gInPc4	scénář
změny	změna	k1gFnSc2	změna
klimatu	klima	k1gNnSc2	klima
IPCC	IPCC	kA	IPCC
<g/>
,	,	kIx,	,
financovaly	financovat	k5eAaBmAgFnP	financovat
vědce	vědec	k1gMnPc4	vědec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nesouhlasí	souhlasit	k5eNaImIp3nP	souhlasit
s	s	k7c7	s
vědeckým	vědecký	k2eAgInSc7d1	vědecký
konsenzem	konsenz	k1gInSc7	konsenz
<g/>
,	,	kIx,	,
a	a	k8xC	a
poskytly	poskytnout	k5eAaPmAgFnP	poskytnout
vlastní	vlastní	k2eAgFnPc1d1	vlastní
projekce	projekce	k1gFnPc1	projekce
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
nákladů	náklad	k1gInPc2	náklad
na	na	k7c4	na
přísnější	přísný	k2eAgFnPc4d2	přísnější
regulace	regulace	k1gFnPc4	regulace
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
některé	některý	k3yIgFnSc2	některý
společnosti	společnost	k1gFnSc2	společnost
pro	pro	k7c4	pro
fosilní	fosilní	k2eAgNnPc4d1	fosilní
paliva	palivo	k1gNnPc4	palivo
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
zvětšily	zvětšit	k5eAaPmAgFnP	zvětšit
své	svůj	k3xOyFgNnSc4	svůj
úsilí	úsilí	k1gNnSc4	úsilí
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
vyzývaly	vyzývat	k5eAaImAgInP	vyzývat
k	k	k7c3	k
politikám	politika	k1gFnPc3	politika
ke	k	k7c3	k
snížení	snížení	k1gNnSc3	snížení
globálního	globální	k2eAgNnSc2d1	globální
oteplování	oteplování	k1gNnSc2	oteplování
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgFnPc1d1	velká
ropné	ropný	k2eAgFnPc1d1	ropná
společnosti	společnost	k1gFnPc1	společnost
začaly	začít	k5eAaPmAgFnP	začít
uznávat	uznávat	k5eAaImF	uznávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
změna	změna	k1gFnSc1	změna
klimatu	klima	k1gNnSc2	klima
existuje	existovat	k5eAaImIp3nS	existovat
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
lidskými	lidský	k2eAgFnPc7d1	lidská
činnostmi	činnost	k1gFnPc7	činnost
a	a	k8xC	a
spalováním	spalování	k1gNnSc7	spalování
fosilních	fosilní	k2eAgNnPc2d1	fosilní
paliv	palivo	k1gNnPc2	palivo
<g/>
.	.	kIx.	.
<g/>
Problém	problém	k1gInSc1	problém
globálního	globální	k2eAgNnSc2d1	globální
oteplování	oteplování	k1gNnSc2	oteplování
přinesl	přinést	k5eAaPmAgInS	přinést
koncem	koncem	k7c2	koncem
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
pozornost	pozornost	k1gFnSc4	pozornost
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
voliči	volič	k1gMnPc1	volič
začali	začít	k5eAaPmAgMnP	začít
sledovat	sledovat	k5eAaImF	sledovat
názory	názor	k1gInPc4	názor
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
<g/>
,	,	kIx,	,
zpočátku	zpočátku	k6eAd1	zpočátku
především	především	k9	především
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhodobý	dlouhodobý	k2eAgInSc1d1	dlouhodobý
výzkum	výzkum	k1gInSc1	výzkum
názorů	názor	k1gInPc2	názor
Gallupova	Gallupův	k2eAgInSc2d1	Gallupův
institutu	institut	k1gInSc2	institut
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
zjistil	zjistit	k5eAaPmAgInS	zjistit
relativně	relativně	k6eAd1	relativně
malé	malý	k2eAgFnPc4d1	malá
odchylky	odchylka	k1gFnPc4	odchylka
v	v	k7c6	v
názorech	názor	k1gInPc6	názor
občanů	občan	k1gMnPc2	občan
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c4	o
10	[number]	k4	10
%	%	kIx~	%
<g/>
,	,	kIx,	,
ve	v	k7c6	v
stanovisku	stanovisko	k1gNnSc6	stanovisko
k	k	k7c3	k
závažnosti	závažnost	k1gFnSc3	závažnost
globálního	globální	k2eAgNnSc2d1	globální
oteplování	oteplování	k1gNnSc2	oteplování
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
rostoucí	rostoucí	k2eAgFnSc7d1	rostoucí
polarizací	polarizace	k1gFnSc7	polarizace
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
o	o	k7c4	o
problém	problém	k1gInSc4	problém
zajímají	zajímat	k5eAaImIp3nP	zajímat
a	a	k8xC	a
nezainteresovanými	zainteresovaný	k2eNgInPc7d1	nezainteresovaný
<g/>
.	.	kIx.	.
<g/>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
matoucímu	matoucí	k2eAgNnSc3d1	matoucí
mediálnímu	mediální	k2eAgNnSc3d1	mediální
zpravodajství	zpravodajství	k1gNnSc3	zpravodajství
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byly	být	k5eAaImAgFnP	být
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
vyčerpání	vyčerpání	k1gNnSc1	vyčerpání
ozónové	ozónový	k2eAgFnSc2d1	ozónová
vrstvy	vrstva	k1gFnSc2	vrstva
a	a	k8xC	a
změna	změna	k1gFnSc1	změna
klimatu	klima	k1gNnSc2	klima
často	často	k6eAd1	často
zaměňovány	zaměňován	k2eAgFnPc1d1	zaměňována
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
pochopení	pochopení	k1gNnSc1	pochopení
těchto	tento	k3xDgInPc2	tento
problémů	problém	k1gInPc2	problém
veřejností	veřejnost	k1gFnPc2	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
Američanů	Američan	k1gMnPc2	Američan
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
většina	většina	k1gFnSc1	většina
věřila	věřit	k5eAaImAgFnS	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ozonová	ozonový	k2eAgFnSc1d1	ozonová
vrstva	vrstva	k1gFnSc1	vrstva
a	a	k8xC	a
postřikovače	postřikovač	k1gInPc1	postřikovač
přispívají	přispívat	k5eAaImIp3nP	přispívat
k	k	k7c3	k
globálnímu	globální	k2eAgNnSc3d1	globální
oteplování	oteplování	k1gNnSc3	oteplování
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
existuje	existovat	k5eAaImIp3nS	existovat
několik	několik	k4yIc4	několik
oblastí	oblast	k1gFnPc2	oblast
spojování	spojování	k1gNnPc2	spojování
<g/>
,	,	kIx,	,
vztah	vztah	k1gInSc1	vztah
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gFnPc7	on
není	být	k5eNaImIp3nS	být
silný	silný	k2eAgInSc1d1	silný
<g/>
.	.	kIx.	.
</s>
<s>
Snížený	snížený	k2eAgInSc1d1	snížený
stratosférický	stratosférický	k2eAgInSc1d1	stratosférický
ozon	ozon	k1gInSc1	ozon
měl	mít	k5eAaImAgInS	mít
mírný	mírný	k2eAgInSc1d1	mírný
chladící	chladící	k2eAgInSc1d1	chladící
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
povrchové	povrchový	k2eAgFnPc4d1	povrchová
teploty	teplota	k1gFnPc4	teplota
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
zvýšený	zvýšený	k2eAgInSc1d1	zvýšený
troposférický	troposférický	k2eAgInSc1d1	troposférický
ozon	ozon	k1gInSc1	ozon
měl	mít	k5eAaImAgInS	mít
poněkud	poněkud	k6eAd1	poněkud
větší	veliký	k2eAgInSc4d2	veliký
účinek	účinek	k1gInSc4	účinek
na	na	k7c4	na
oteplování	oteplování	k1gNnSc4	oteplování
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
CFC	CFC	kA	CFC
používané	používaný	k2eAgInPc4d1	používaný
v	v	k7c6	v
rozprašovacích	rozprašovací	k2eAgFnPc6d1	rozprašovací
nádobách	nádoba	k1gFnPc6	nádoba
jsou	být	k5eAaImIp3nP	být
silné	silný	k2eAgInPc1d1	silný
skleníkové	skleníkový	k2eAgInPc1d1	skleníkový
plyny	plyn	k1gInPc1	plyn
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
některé	některý	k3yIgInPc1	některý
odhady	odhad	k1gInPc1	odhad
připisují	připisovat	k5eAaImIp3nP	připisovat
emise	emise	k1gFnPc1	emise
CFC	CFC	kA	CFC
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
způsobily	způsobit	k5eAaPmAgInP	způsobit
téměř	téměř	k6eAd1	téměř
polovinu	polovina	k1gFnSc4	polovina
globálního	globální	k2eAgNnSc2d1	globální
oteplování	oteplování	k1gNnSc2	oteplování
pro	pro	k7c4	pro
toto	tento	k3xDgNnSc4	tento
desetiletí	desetiletí	k1gNnSc4	desetiletí
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
dělal	dělat	k5eAaImAgInS	dělat
Gallupův	Gallupův	k2eAgInSc1d1	Gallupův
ústav	ústav	k1gInSc1	ústav
výzkum	výzkum	k1gInSc1	výzkum
v	v	k7c6	v
111	[number]	k4	111
zemích	zem	k1gFnPc6	zem
a	a	k8xC	a
dospěl	dochvít	k5eAaPmAgMnS	dochvít
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
<g/>
–	–	k?	–
<g/>
2008	[number]	k4	2008
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
výraznému	výrazný	k2eAgInSc3d1	výrazný
poklesu	pokles	k1gInSc3	pokles
počtu	počet	k1gInSc2	počet
Američanů	Američan	k1gMnPc2	Američan
a	a	k8xC	a
Evropanů	Evropan	k1gMnPc2	Evropan
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
považovali	považovat	k5eAaImAgMnP	považovat
globální	globální	k2eAgNnSc4d1	globální
oteplování	oteplování	k1gNnSc4	oteplování
za	za	k7c4	za
vážnou	vážný	k2eAgFnSc4d1	vážná
hrozbu	hrozba	k1gFnSc4	hrozba
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
jen	jen	k9	jen
málo	málo	k6eAd1	málo
přes	přes	k7c4	přes
polovinu	polovina	k1gFnSc4	polovina
populace	populace	k1gFnSc2	populace
(	(	kIx(	(
<g/>
53	[number]	k4	53
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
považovalo	považovat	k5eAaImAgNnS	považovat
globální	globální	k2eAgNnSc4d1	globální
oteplování	oteplování	k1gNnSc4	oteplování
za	za	k7c4	za
vážný	vážný	k2eAgInSc4d1	vážný
zájem	zájem	k1gInSc4	zájem
buď	buď	k8xC	buď
pro	pro	k7c4	pro
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
pro	pro	k7c4	pro
své	své	k1gNnSc4	své
rodiny	rodina	k1gFnSc2	rodina
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
o	o	k7c4	o
10	[number]	k4	10
bodů	bod	k1gInPc2	bod
pod	pod	k7c7	pod
průzkumem	průzkum	k1gInSc7	průzkum
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2008	[number]	k4	2008
(	(	kIx(	(
<g/>
63	[number]	k4	63
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Latinská	latinský	k2eAgFnSc1d1	Latinská
Amerika	Amerika	k1gFnSc1	Amerika
měla	mít	k5eAaImAgFnS	mít
největší	veliký	k2eAgInSc4d3	veliký
nárůst	nárůst	k1gInSc4	nárůst
obav	obava	k1gFnPc2	obava
<g/>
:	:	kIx,	:
73	[number]	k4	73
<g/>
%	%	kIx~	%
uvedlo	uvést	k5eAaPmAgNnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
globální	globální	k2eAgNnSc1d1	globální
oteplování	oteplování	k1gNnSc1	oteplování
představuje	představovat	k5eAaImIp3nS	představovat
vážnou	vážný	k2eAgFnSc4d1	vážná
hrozbu	hrozba	k1gFnSc4	hrozba
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gFnPc4	jejich
rodiny	rodina	k1gFnPc4	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
globální	globální	k2eAgFnSc1d1	globální
anketa	anketa	k1gFnSc1	anketa
také	také	k9	také
zjistila	zjistit	k5eAaPmAgFnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
s	s	k7c7	s
větší	veliký	k2eAgFnSc7d2	veliký
pravděpodobností	pravděpodobnost	k1gFnSc7	pravděpodobnost
přičítají	přičítat	k5eAaImIp3nP	přičítat
globální	globální	k2eAgNnSc4d1	globální
oteplování	oteplování	k1gNnSc4	oteplování
lidským	lidský	k2eAgFnPc3d1	lidská
činnostem	činnost	k1gFnPc3	činnost
než	než	k8xS	než
přirozeným	přirozený	k2eAgFnPc3d1	přirozená
příčinám	příčina	k1gFnPc3	příčina
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
téměř	téměř	k6eAd1	téměř
polovina	polovina	k1gFnSc1	polovina
(	(	kIx(	(
<g/>
47	[number]	k4	47
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
populace	populace	k1gFnSc1	populace
připisuje	připisovat	k5eAaImIp3nS	připisovat
globální	globální	k2eAgNnSc4d1	globální
oteplování	oteplování	k1gNnSc4	oteplování
přirozeným	přirozený	k2eAgFnPc3d1	přirozená
příčinám	příčina	k1gFnPc3	příčina
<g/>
.	.	kIx.	.
<g/>
Průzkum	průzkum	k1gInSc1	průzkum
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
a	a	k8xC	a
květnu	květen	k1gInSc6	květen
2013	[number]	k4	2013
od	od	k7c2	od
Pew	Pew	k1gFnSc2	Pew
Research	Research	k1gInSc1	Research
Center	centrum	k1gNnPc2	centrum
pro	pro	k7c4	pro
lidi	člověk	k1gMnPc4	člověk
a	a	k8xC	a
tisk	tisk	k1gInSc4	tisk
informoval	informovat	k5eAaBmAgMnS	informovat
39	[number]	k4	39
zemí	zem	k1gFnPc2	zem
o	o	k7c6	o
globálních	globální	k2eAgFnPc6d1	globální
hrozbách	hrozba	k1gFnPc6	hrozba
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
54	[number]	k4	54
%	%	kIx~	%
dotázaných	dotázaný	k2eAgFnPc2d1	dotázaná
se	se	k3xPyFc4	se
globální	globální	k2eAgNnSc1d1	globální
oteplování	oteplování	k1gNnSc1	oteplování
stalo	stát	k5eAaPmAgNnS	stát
vrcholem	vrchol	k1gInSc7	vrchol
vnímání	vnímání	k1gNnSc2	vnímání
globálních	globální	k2eAgFnPc2d1	globální
hrozeb	hrozba	k1gFnPc2	hrozba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Situace	situace	k1gFnSc1	situace
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
==	==	k?	==
</s>
</p>
<p>
<s>
Atlas	Atlas	k1gInSc1	Atlas
podnebí	podnebí	k1gNnSc2	podnebí
Česka	Česko	k1gNnSc2	Česko
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
období	období	k1gNnSc6	období
1961	[number]	k4	1961
<g/>
–	–	k?	–
<g/>
2000	[number]	k4	2000
roční	roční	k2eAgFnSc1d1	roční
průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
ČR	ČR	kA	ČR
(	(	kIx(	(
<g/>
průměr	průměr	k1gInSc1	průměr
z	z	k7c2	z
311	[number]	k4	311
stanic	stanice	k1gFnPc2	stanice
<g/>
)	)	kIx)	)
silně	silně	k6eAd1	silně
kolísala	kolísat	k5eAaImAgFnS	kolísat
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
měla	mít	k5eAaImAgFnS	mít
statisticky	statisticky	k6eAd1	statisticky
významný	významný	k2eAgInSc4d1	významný
oteplovací	oteplovací	k2eAgInSc4d1	oteplovací
trend	trend	k1gInSc4	trend
0,28	[number]	k4	0,28
°	°	k?	°
<g/>
C	C	kA	C
za	za	k7c4	za
dekádu	dekáda	k1gFnSc4	dekáda
<g/>
.	.	kIx.	.
</s>
<s>
Oteplování	oteplování	k1gNnSc1	oteplování
bylo	být	k5eAaImAgNnS	být
nejvýraznější	výrazný	k2eAgNnSc1d3	nejvýraznější
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
a	a	k8xC	a
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
<g/>
,	,	kIx,	,
nevýznamné	významný	k2eNgMnPc4d1	nevýznamný
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
<g/>
.	.	kIx.	.
</s>
<s>
Nejteplejší	teplý	k2eAgInSc1d3	nejteplejší
byl	být	k5eAaImAgInS	být
rok	rok	k1gInSc1	rok
2018	[number]	k4	2018
s	s	k7c7	s
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
teplotou	teplota	k1gFnSc7	teplota
9,6	[number]	k4	9,6
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
mimořádně	mimořádně	k6eAd1	mimořádně
nadnormální	nadnormální	k2eAgFnSc1d1	nadnormální
<g/>
;	;	kIx,	;
s	s	k7c7	s
odchylkou	odchylka	k1gFnSc7	odchylka
+1,7	+1,7	k4	+1,7
°	°	k?	°
<g/>
C	C	kA	C
od	od	k7c2	od
normálu	normál	k1gInSc2	normál
1981	[number]	k4	1981
<g/>
–	–	k?	–
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgInP	být
roky	rok	k1gInPc1	rok
2014	[number]	k4	2014
a	a	k8xC	a
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
oba	dva	k4xCgInPc1	dva
s	s	k7c7	s
průměrnou	průměrný	k2eAgFnSc7d1	průměrná
teplotou	teplota	k1gFnSc7	teplota
9,4	[number]	k4	9,4
°	°	k?	°
<g/>
C.	C.	kA	C.
Oteplování	oteplování	k1gNnSc1	oteplování
<g/>
,	,	kIx,	,
obdobné	obdobný	k2eAgFnPc1d1	obdobná
se	s	k7c7	s
světovými	světový	k2eAgNnPc7d1	světové
pozorováními	pozorování	k1gNnPc7	pozorování
<g/>
,	,	kIx,	,
potvrzují	potvrzovat	k5eAaImIp3nP	potvrzovat
i	i	k9	i
další	další	k2eAgFnPc4d1	další
práce	práce	k1gFnPc4	práce
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
trend	trend	k1gInSc1	trend
oteplování	oteplování	k1gNnSc2	oteplování
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1961	[number]	k4	1961
<g/>
–	–	k?	–
<g/>
2000	[number]	k4	2000
překryt	překrýt	k5eAaPmNgInS	překrýt
kratšími	krátký	k2eAgInPc7d2	kratší
výkyvy	výkyv	k1gInPc7	výkyv
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
i	i	k9	i
v	v	k7c6	v
nejteplejším	teplý	k2eAgNnSc6d3	nejteplejší
posledním	poslední	k2eAgNnSc6d1	poslední
desetiletí	desetiletí	k1gNnSc6	desetiletí
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
se	se	k3xPyFc4	se
vyskytl	vyskytnout	k5eAaPmAgInS	vyskytnout
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
nejchladnějších	chladný	k2eAgInPc2d3	nejchladnější
roků	rok	k1gInPc2	rok
celého	celý	k2eAgInSc2d1	celý
čtyřicetiletí	čtyřicetiletý	k2eAgMnPc1d1	čtyřicetiletý
<g/>
,	,	kIx,	,
rok	rok	k1gInSc4	rok
1996	[number]	k4	1996
s	s	k7c7	s
průměrem	průměr	k1gInSc7	průměr
6,3	[number]	k4	6,3
°	°	k?	°
<g/>
C.	C.	kA	C.
Vlivem	vlivem	k7c2	vlivem
lidské	lidský	k2eAgFnSc2d1	lidská
činnosti	činnost	k1gFnSc2	činnost
rostl	růst	k5eAaImAgInS	růst
efekt	efekt	k1gInSc1	efekt
tepelného	tepelný	k2eAgInSc2d1	tepelný
ostrova	ostrov	k1gInSc2	ostrov
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
projevující	projevující	k2eAgFnSc2d1	projevující
se	se	k3xPyFc4	se
celoročním	celoroční	k2eAgNnSc7d1	celoroční
zvýšením	zvýšení	k1gNnSc7	zvýšení
nočních	noční	k2eAgFnPc2d1	noční
teplot	teplota	k1gFnPc2	teplota
a	a	k8xC	a
zvýšením	zvýšení	k1gNnSc7	zvýšení
průměrných	průměrný	k2eAgFnPc2d1	průměrná
teplot	teplota	k1gFnPc2	teplota
v	v	k7c6	v
chladné	chladný	k2eAgFnSc6d1	chladná
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
(	(	kIx(	(
<g/>
říjen	říjen	k1gInSc1	říjen
<g/>
–	–	k?	–
<g/>
březen	březen	k1gInSc1	březen
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Českého	český	k2eAgInSc2d1	český
hydrometeorologického	hydrometeorologický	k2eAgInSc2d1	hydrometeorologický
ústavu	ústav	k1gInSc2	ústav
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
průměrná	průměrný	k2eAgFnSc1d1	průměrná
teplota	teplota	k1gFnSc1	teplota
za	za	k7c4	za
celé	celý	k2eAgNnSc4d1	celé
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
o	o	k7c4	o
1,1	[number]	k4	1,1
<g/>
–	–	k?	–
<g/>
1,3	[number]	k4	1,3
°	°	k?	°
<g/>
C.V	C.V	k1gFnSc1	C.V
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
poklesly	poklesnout	k5eAaPmAgFnP	poklesnout
hodnoty	hodnota	k1gFnPc1	hodnota
všech	všecek	k3xTgFnPc2	všecek
charakteristik	charakteristika	k1gFnPc2	charakteristika
spojených	spojený	k2eAgFnPc2d1	spojená
se	s	k7c7	s
sněhem	sníh	k1gInSc7	sníh
<g/>
.	.	kIx.	.
</s>
<s>
Snižují	snižovat	k5eAaImIp3nP	snižovat
se	se	k3xPyFc4	se
počty	počet	k1gInPc7	počet
dní	den	k1gInPc2	den
se	s	k7c7	s
sněhovou	sněhový	k2eAgFnSc7d1	sněhová
pokrývkou	pokrývka	k1gFnSc7	pokrývka
i	i	k8xC	i
měsíční	měsíční	k2eAgFnSc1d1	měsíční
a	a	k8xC	a
sezónní	sezónní	k2eAgFnSc1d1	sezónní
maxima	maxima	k1gFnSc1	maxima
výšky	výška	k1gFnSc2	výška
sněhové	sněhový	k2eAgFnSc2d1	sněhová
pokrývky	pokrývka	k1gFnSc2	pokrývka
<g/>
.	.	kIx.	.
</s>
<s>
Sněhu	sníh	k1gInSc2	sníh
ubývá	ubývat	k5eAaImIp3nS	ubývat
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
i	i	k8xC	i
na	na	k7c6	na
horách	hora	k1gFnPc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
výskyt	výskyt	k1gInSc1	výskyt
sněhu	sníh	k1gInSc2	sníh
je	být	k5eAaImIp3nS	být
důležitým	důležitý	k2eAgInSc7d1	důležitý
předpokladem	předpoklad	k1gInSc7	předpoklad
vytvoření	vytvoření	k1gNnSc4	vytvoření
dostatečného	dostatečný	k2eAgNnSc2d1	dostatečné
množství	množství	k1gNnSc2	množství
povrchové	povrchový	k2eAgFnSc2d1	povrchová
i	i	k8xC	i
podzemní	podzemní	k2eAgFnSc2d1	podzemní
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Změny	změna	k1gFnPc1	změna
klimatu	klima	k1gNnSc2	klima
jsou	být	k5eAaImIp3nP	být
průkazně	průkazně	k6eAd1	průkazně
pozorovatelné	pozorovatelný	k2eAgFnPc1d1	pozorovatelná
i	i	k8xC	i
v	v	k7c6	v
živé	živý	k2eAgFnSc6d1	živá
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
pozorování	pozorování	k1gNnPc2	pozorování
v	v	k7c6	v
moravských	moravský	k2eAgInPc6d1	moravský
lužních	lužní	k2eAgInPc6d1	lužní
lesích	les	k1gInPc6	les
vyplývá	vyplývat	k5eAaImIp3nS	vyplývat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
období	období	k1gNnSc6	období
1961	[number]	k4	1961
<g/>
–	–	k?	–
<g/>
2000	[number]	k4	2000
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
posunulo	posunout	k5eAaPmAgNnS	posunout
do	do	k7c2	do
dřívější	dřívější	k2eAgFnSc2d1	dřívější
doby	doba	k1gFnSc2	doba
rašení	rašení	k1gNnSc2	rašení
listů	list	k1gInPc2	list
u	u	k7c2	u
vybraných	vybraný	k2eAgInPc2d1	vybraný
druhů	druh	k1gInPc2	druh
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
kvetení	kvetení	k1gNnPc2	kvetení
u	u	k7c2	u
vybraných	vybraný	k2eAgInPc2d1	vybraný
keřů	keř	k1gInPc2	keř
a	a	k8xC	a
bylin	bylina	k1gFnPc2	bylina
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
vybraných	vybraný	k2eAgInPc2d1	vybraný
ptačích	ptačí	k2eAgInPc2d1	ptačí
druhů	druh	k1gInPc2	druh
pak	pak	k6eAd1	pak
byl	být	k5eAaImAgInS	být
zaznamenán	zaznamenat	k5eAaPmNgInS	zaznamenat
posun	posun	k1gInSc1	posun
začátku	začátek	k1gInSc2	začátek
hnízdění	hnízdění	k1gNnSc2	hnízdění
<g/>
.	.	kIx.	.
<g/>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
projevy	projev	k1gInPc7	projev
globálního	globální	k2eAgNnSc2d1	globální
oteplování	oteplování	k1gNnSc2	oteplování
pro	pro	k7c4	pro
Českou	český	k2eAgFnSc4d1	Česká
republiku	republika	k1gFnSc4	republika
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
Národního	národní	k2eAgInSc2d1	národní
akčního	akční	k2eAgInSc2d1	akční
plánu	plán	k1gInSc2	plán
adaptace	adaptace	k1gFnSc2	adaptace
na	na	k7c4	na
změnu	změna	k1gFnSc4	změna
klimatu	klima	k1gNnSc2	klima
<g/>
,	,	kIx,	,
budou	být	k5eAaImBp3nP	být
dlouhodobé	dlouhodobý	k2eAgNnSc1d1	dlouhodobé
sucho	sucho	k1gNnSc1	sucho
<g/>
,	,	kIx,	,
povodně	povodeň	k1gFnPc1	povodeň
a	a	k8xC	a
přívalové	přívalový	k2eAgFnPc1d1	přívalová
povodně	povodeň	k1gFnPc1	povodeň
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
zvyšování	zvyšování	k1gNnSc1	zvyšování
teplot	teplota	k1gFnPc2	teplota
<g/>
,	,	kIx,	,
extrémní	extrémní	k2eAgInPc4d1	extrémní
meteorologické	meteorologický	k2eAgInPc4d1	meteorologický
jevy	jev	k1gInPc4	jev
<g/>
,	,	kIx,	,
vydatné	vydatný	k2eAgFnPc4d1	vydatná
srážky	srážka	k1gFnPc4	srážka
<g/>
,	,	kIx,	,
extrémně	extrémně	k6eAd1	extrémně
vysoké	vysoký	k2eAgFnPc1d1	vysoká
teploty	teplota	k1gFnPc1	teplota
(	(	kIx(	(
<g/>
vlny	vlna	k1gFnPc1	vlna
veder	vedro	k1gNnPc2	vedro
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
extrémní	extrémní	k2eAgInSc1d1	extrémní
vítr	vítr	k1gInSc1	vítr
a	a	k8xC	a
přírodní	přírodní	k2eAgInPc1d1	přírodní
požáry	požár	k1gInPc1	požár
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Etymologie	etymologie	k1gFnSc2	etymologie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc3	století
výzkumníci	výzkumník	k1gMnPc1	výzkumník
nejprve	nejprve	k6eAd1	nejprve
naznačili	naznačit	k5eAaPmAgMnP	naznačit
nárůst	nárůst	k1gInSc4	nárůst
teplot	teplota	k1gFnPc2	teplota
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1952	[number]	k4	1952
noviny	novina	k1gFnSc2	novina
psaly	psát	k5eAaImAgInP	psát
o	o	k7c4	o
"	"	kIx"	"
<g/>
změně	změna	k1gFnSc3	změna
klimatu	klima	k1gNnSc2	klima
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
výraz	výraz	k1gInSc1	výraz
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
objevil	objevit	k5eAaPmAgMnS	objevit
ve	v	k7c6	v
zprávě	zpráva	k1gFnSc6	zpráva
v	v	k7c6	v
deníku	deník	k1gInSc6	deník
The	The	k1gMnSc1	The
Hammond	Hammond	k1gMnSc1	Hammond
Times	Times	k1gMnSc1	Times
(	(	kIx(	(
<g/>
Indiana	Indiana	k1gFnSc1	Indiana
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
z	z	k7c2	z
listopadu	listopad	k1gInSc2	listopad
1957	[number]	k4	1957
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
popsal	popsat	k5eAaPmAgMnS	popsat
výzkum	výzkum	k1gInSc4	výzkum
Rogera	Roger	k1gMnSc2	Roger
Revelleho	Revelle	k1gMnSc2	Revelle
zaměřený	zaměřený	k2eAgInSc4d1	zaměřený
na	na	k7c4	na
zvýšené	zvýšený	k2eAgInPc4d1	zvýšený
účinky	účinek	k1gInPc4	účinek
emisí	emise	k1gFnPc2	emise
CO2	CO2	k1gFnSc2	CO2
způsobené	způsobený	k2eAgFnSc2d1	způsobená
člověkem	člověk	k1gMnSc7	člověk
na	na	k7c4	na
skleníkový	skleníkový	k2eAgInSc4d1	skleníkový
efekt	efekt	k1gInSc4	efekt
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
"	"	kIx"	"
<g/>
může	moct	k5eAaImIp3nS	moct
mít	mít	k5eAaImF	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
měřítku	měřítko	k1gNnSc6	měřítko
globální	globální	k2eAgNnSc4d1	globální
oteplování	oteplování	k1gNnSc4	oteplování
<g/>
,	,	kIx,	,
s	s	k7c7	s
radikálními	radikální	k2eAgFnPc7d1	radikální
změnami	změna	k1gFnPc7	změna
klimatu	klima	k1gNnSc2	klima
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
pojmy	pojem	k1gInPc1	pojem
byly	být	k5eAaImAgInP	být
používány	používat	k5eAaImNgInP	používat
pouze	pouze	k6eAd1	pouze
příležitostně	příležitostně	k6eAd1	příležitostně
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Wallace	Wallace	k1gFnSc1	Wallace
Smith	Smith	k1gMnSc1	Smith
Broecker	Broecker	k1gMnSc1	Broecker
publikoval	publikovat	k5eAaBmAgMnS	publikovat
vědecký	vědecký	k2eAgInSc4d1	vědecký
článek	článek	k1gInSc4	článek
na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
téma	téma	k1gNnSc4	téma
<g/>
;	;	kIx,	;
"	"	kIx"	"
<g/>
Klimatické	klimatický	k2eAgFnPc1d1	klimatická
změny	změna	k1gFnPc1	změna
<g/>
:	:	kIx,	:
jsme	být	k5eAaImIp1nP	být
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
výrazného	výrazný	k2eAgNnSc2d1	výrazné
globálního	globální	k2eAgNnSc2d1	globální
oteplování	oteplování	k1gNnSc2	oteplování
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
"	"	kIx"	"
Obě	dva	k4xCgFnPc1	dva
fráze	fráze	k1gFnPc1	fráze
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
běžně	běžně	k6eAd1	běžně
používat	používat	k5eAaImF	používat
a	a	k8xC	a
začalo	začít	k5eAaPmAgNnS	začít
se	se	k3xPyFc4	se
široce	široko	k6eAd1	široko
šířit	šířit	k5eAaImF	šířit
prohlášení	prohlášení	k1gNnSc4	prohlášení
Michaila	Michail	k1gMnSc2	Michail
Budyka	Budyek	k1gMnSc2	Budyek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
globální	globální	k2eAgNnSc1d1	globální
oteplování	oteplování	k1gNnSc1	oteplování
začalo	začít	k5eAaPmAgNnS	začít
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jiné	jiný	k2eAgFnPc1d1	jiná
studie	studie	k1gFnPc1	studie
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
zpráva	zpráva	k1gFnSc1	zpráva
MIT	MIT	kA	MIT
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
<g/>
,	,	kIx,	,
odkazovaly	odkazovat	k5eAaImAgFnP	odkazovat
na	na	k7c4	na
vliv	vliv	k1gInSc4	vliv
člověka	člověk	k1gMnSc2	člověk
jako	jako	k8xC	jako
na	na	k7c4	na
"	"	kIx"	"
<g/>
neúmyslnou	úmyslný	k2eNgFnSc4d1	neúmyslná
změnu	změna	k1gFnSc4	změna
klimatu	klima	k1gNnSc2	klima
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vlivná	vlivný	k2eAgFnSc1d1	vlivná
studie	studie	k1gFnSc1	studie
Národní	národní	k2eAgFnSc2d1	národní
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Julem	Julem	k?	Julem
Charneyem	Charney	k1gMnSc7	Charney
následovala	následovat	k5eAaImAgFnS	následovat
Broeckera	Broeckera	k1gFnSc1	Broeckera
použitím	použití	k1gNnSc7	použití
pojmu	pojem	k1gInSc2	pojem
globální	globální	k2eAgFnSc2d1	globální
oteplování	oteplování	k1gNnSc4	oteplování
pro	pro	k7c4	pro
nárůst	nárůst	k1gInSc4	nárůst
povrchových	povrchový	k2eAgFnPc2d1	povrchová
teplot	teplota	k1gFnPc2	teplota
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
širší	široký	k2eAgInSc4d2	širší
účinky	účinek	k1gInPc4	účinek
nárůstu	nárůst	k1gInSc2	nárůst
CO2	CO2	k1gFnSc2	CO2
popsala	popsat	k5eAaPmAgFnS	popsat
jako	jako	k9	jako
změnu	změna	k1gFnSc4	změna
klimatu	klima	k1gNnSc2	klima
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
a	a	k8xC	a
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1987	[number]	k4	1987
klimatolog	klimatolog	k1gMnSc1	klimatolog
NASA	NASA	kA	NASA
James	James	k1gInSc1	James
Hansen	Hansen	k1gInSc1	Hansen
poskytl	poskytnout	k5eAaPmAgInS	poskytnout
své	svůj	k3xOyFgNnSc4	svůj
svědectví	svědectví	k1gNnSc4	svědectví
o	o	k7c6	o
globálním	globální	k2eAgNnSc6d1	globální
oteplování	oteplování	k1gNnSc6	oteplování
Kongresu	kongres	k1gInSc2	kongres
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
USA	USA	kA	USA
rostoucí	rostoucí	k2eAgFnSc2d1	rostoucí
vlny	vlna	k1gFnSc2	vlna
veder	vedro	k1gNnPc2	vedro
a	a	k8xC	a
problémy	problém	k1gInPc1	problém
se	s	k7c7	s
suchem	sucho	k1gNnSc7	sucho
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
Hansen	Hansen	k1gInSc1	Hansen
svědčil	svědčit	k5eAaImAgInS	svědčit
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
<g/>
,	,	kIx,	,
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
to	ten	k3xDgNnSc1	ten
celosvětový	celosvětový	k2eAgInSc1d1	celosvětový
zájem	zájem	k1gInSc1	zájem
<g/>
.	.	kIx.	.
</s>
<s>
Řekl	říct	k5eAaPmAgMnS	říct
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
globální	globální	k2eAgNnSc4d1	globální
oteplování	oteplování	k1gNnSc4	oteplování
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
takové	takový	k3xDgFnPc4	takový
úrovně	úroveň	k1gFnPc4	úroveň
<g/>
,	,	kIx,	,
že	že	k8xS	že
můžeme	moct	k5eAaImIp1nP	moct
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
mírou	míra	k1gFnSc7	míra
důvěry	důvěra	k1gFnSc2	důvěra
popsat	popsat	k5eAaPmF	popsat
příčinný	příčinný	k2eAgInSc4d1	příčinný
vztah	vztah	k1gInSc4	vztah
mezi	mezi	k7c7	mezi
skleníkovým	skleníkový	k2eAgInSc7d1	skleníkový
efektem	efekt	k1gInSc7	efekt
a	a	k8xC	a
pozorovaným	pozorovaný	k2eAgNnSc7d1	pozorované
oteplováním	oteplování	k1gNnSc7	oteplování
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Pozornost	pozornost	k1gFnSc1	pozornost
veřejnosti	veřejnost	k1gFnSc2	veřejnost
vzrostla	vzrůst	k5eAaPmAgFnS	vzrůst
během	během	k7c2	během
léta	léto	k1gNnSc2	léto
a	a	k8xC	a
termíny	termín	k1gInPc4	termín
globální	globální	k2eAgNnSc4d1	globální
oteplování	oteplování	k1gNnSc4	oteplování
či	či	k8xC	či
změna	změna	k1gFnSc1	změna
klimatu	klima	k1gNnSc2	klima
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
populárními	populární	k2eAgFnPc7d1	populární
<g/>
,	,	kIx,	,
a	a	k8xC	a
běžně	běžně	k6eAd1	běžně
<g />
.	.	kIx.	.
</s>
<s>
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
jak	jak	k6eAd1	jak
tisku	tisk	k1gInSc2	tisk
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
ve	v	k7c6	v
veřejném	veřejný	k2eAgInSc6d1	veřejný
diskursu	diskurs	k1gInSc6	diskurs
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
článku	článek	k1gInSc6	článek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
věnovaném	věnovaný	k2eAgNnSc6d1	věnované
používání	používání	k1gNnSc6	používání
pojmu	pojem	k1gInSc2	pojem
definoval	definovat	k5eAaBmAgMnS	definovat
Erik	Erik	k1gMnSc1	Erik
M.	M.	kA	M.
Conway	Conwaa	k1gFnPc1	Conwaa
globální	globální	k2eAgNnSc1d1	globální
oteplování	oteplování	k1gNnSc1	oteplování
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
zvýšení	zvýšení	k1gNnSc3	zvýšení
zemské	zemský	k2eAgFnSc2d1	zemská
průměrné	průměrný	k2eAgFnSc2d1	průměrná
povrchové	povrchový	k2eAgFnSc2d1	povrchová
teploty	teplota	k1gFnSc2	teplota
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
stoupání	stoupání	k1gNnSc2	stoupání
hodnoty	hodnota	k1gFnSc2	hodnota
koncentrací	koncentrace	k1gFnPc2	koncentrace
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
změna	změna	k1gFnSc1	změna
klimatu	klima	k1gNnSc2	klima
byla	být	k5eAaImAgFnS	být
"	"	kIx"	"
<g/>
dlouhodobá	dlouhodobý	k2eAgFnSc1d1	dlouhodobá
změna	změna	k1gFnSc1	změna
v	v	k7c6	v
zemském	zemský	k2eAgNnSc6d1	zemské
klimatu	klima	k1gNnSc6	klima
nebo	nebo	k8xC	nebo
regionech	region	k1gInPc6	region
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Účinky	účinek	k1gInPc1	účinek
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
změna	změna	k1gFnSc1	změna
vzorců	vzorec	k1gInPc2	vzorec
srážek	srážka	k1gFnPc2	srážka
a	a	k8xC	a
stoupající	stoupající	k2eAgFnPc1d1	stoupající
hladiny	hladina	k1gFnPc1	hladina
moří	mořit	k5eAaImIp3nP	mořit
<g/>
,	,	kIx,	,
by	by	kYmCp3nP	by
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
měly	mít	k5eAaImAgFnP	mít
mít	mít	k5eAaImF	mít
větší	veliký	k2eAgInSc4d2	veliký
dopad	dopad	k1gInSc4	dopad
než	než	k8xS	než
samotná	samotný	k2eAgFnSc1d1	samotná
teplota	teplota	k1gFnSc1	teplota
<g/>
,	,	kIx,	,
Conway	Conway	k1gInPc4	Conway
považuje	považovat	k5eAaImIp3nS	považovat
globální	globální	k2eAgFnSc4d1	globální
změnu	změna	k1gFnSc4	změna
klimatu	klima	k1gNnSc2	klima
za	za	k7c4	za
více	hodně	k6eAd2	hodně
vědecky	vědecky	k6eAd1	vědecky
přesný	přesný	k2eAgInSc4d1	přesný
termín	termín	k1gInSc4	termín
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
Mezivládní	mezivládní	k2eAgInSc1d1	mezivládní
panel	panel	k1gInSc1	panel
pro	pro	k7c4	pro
změny	změna	k1gFnPc4	změna
klimatu	klima	k1gNnSc2	klima
na	na	k7c6	na
internetových	internetový	k2eAgFnPc6d1	internetová
stránkách	stránka	k1gFnPc6	stránka
NASA	NASA	kA	NASA
rád	rád	k6eAd1	rád
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
tento	tento	k3xDgInSc1	tento
širší	široký	k2eAgInSc1d2	širší
kontext	kontext	k1gInSc1	kontext
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Global	globat	k5eAaImAgMnS	globat
warming	warming	k1gInSc4	warming
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
referencích	reference	k1gFnPc6	reference
jsou	být	k5eAaImIp3nP	být
použity	použít	k5eAaPmNgFnP	použít
následující	následující	k2eAgFnPc1d1	následující
zkratky	zkratka	k1gFnPc1	zkratka
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
TAR	TAR	kA	TAR
-	-	kIx~	-
IPCC	IPCC	kA	IPCC
Third	Third	k1gInSc1	Third
Assessment	Assessment	k1gInSc1	Assessment
Report	report	k1gInSc4	report
<g/>
:	:	kIx,	:
Climate	Climat	k1gInSc5	Climat
Change	change	k1gFnSc1	change
2001	[number]	k4	2001
(	(	kIx(	(
<g/>
TAR	TAR	kA	TAR
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
AR4	AR4	k4	AR4
-	-	kIx~	-
IPCC	IPCC	kA	IPCC
Fourth	Fourth	k1gInSc1	Fourth
Assessment	Assessment	k1gInSc1	Assessment
Report	report	k1gInSc4	report
<g/>
:	:	kIx,	:
Climate	Climat	k1gInSc5	Climat
Change	change	k1gFnSc1	change
2007	[number]	k4	2007
(	(	kIx(	(
<g/>
AR	ar	k1gInSc1	ar
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
AR5	AR5	k4	AR5
-	-	kIx~	-
IPCC	IPCC	kA	IPCC
Fifth	Fifth	k1gInSc1	Fifth
Assessment	Assessment	k1gInSc1	Assessment
Report	report	k1gInSc4	report
<g/>
:	:	kIx,	:
Climate	Climat	k1gInSc5	Climat
Change	change	k1gFnSc1	change
2013	[number]	k4	2013
(	(	kIx(	(
<g/>
AR	ar	k1gInSc1	ar
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SR15	SR15	k4	SR15
-	-	kIx~	-
Special	Special	k1gInSc1	Special
Report	report	k1gInSc1	report
on	on	k3xPp3gMnSc1	on
Global	globat	k5eAaImAgInS	globat
Warming	Warming	k1gInSc4	Warming
of	of	k?	of
1.5	[number]	k4	1.5
°	°	k?	°
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
SR	SR	kA	SR
<g/>
15	[number]	k4	15
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
SPM	SPM	kA	SPM
-	-	kIx~	-
Summary	Summar	k1gInPc1	Summar
for	forum	k1gNnPc2	forum
Policymakers	Policymakersa	k1gFnPc2	Policymakersa
</s>
</p>
<p>
<s>
WG1	WG1	k4	WG1
-	-	kIx~	-
Working	Working	k1gInSc1	Working
Group	Group	k1gInSc1	Group
1	[number]	k4	1
</s>
</p>
<p>
<s>
WG2	WG2	k4	WG2
-	-	kIx~	-
Working	Working	k1gInSc1	Working
Group	Group	k1gInSc1	Group
2	[number]	k4	2
</s>
</p>
<p>
<s>
WG3-	WG3-	k4	WG3-
Working	Working	k1gInSc1	Working
Group	Group	k1gInSc1	Group
3	[number]	k4	3
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Klimatická	klimatický	k2eAgFnSc1d1	klimatická
změna	změna	k1gFnSc1	změna
</s>
</p>
<p>
<s>
Smog	smog	k1gInSc1	smog
</s>
</p>
<p>
<s>
Zachycování	zachycování	k1gNnSc1	zachycování
a	a	k8xC	a
ukládání	ukládání	k1gNnSc1	ukládání
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
</s>
</p>
<p>
<s>
Vymírání	vymírání	k1gNnSc1	vymírání
v	v	k7c6	v
holocénu	holocén	k1gInSc6	holocén
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Česky	česky	k6eAd1	česky
MEZIVLÁDNI	MEZIVLÁDNI	kA	MEZIVLÁDNI
PANEL	panel	k1gInSc1	panel
PRO	pro	k7c4	pro
ZMĚNU	změna	k1gFnSc4	změna
KLIMATU	klima	k1gNnSc2	klima
<g/>
.	.	kIx.	.
</s>
<s>
Fyzikální	fyzikální	k2eAgInPc1d1	fyzikální
základy	základ	k1gInPc1	základ
<g/>
.	.	kIx.	.
</s>
<s>
Příspěvek	příspěvek	k1gInSc1	příspěvek
Pracovní	pracovní	k2eAgFnSc2d1	pracovní
skupiny	skupina	k1gFnSc2	skupina
I	i	k9	i
k	k	k7c3	k
Páté	pátá	k1gFnSc3	pátá
hodnoticí	hodnoticí	k2eAgFnSc3d1	hodnoticí
zprávě	zpráva	k1gFnSc3	zpráva
Mezivládního	mezivládní	k2eAgInSc2d1	mezivládní
panelu	panel	k1gInSc2	panel
pro	pro	k7c4	pro
změnu	změna	k1gFnSc4	změna
klimatu	klima	k1gNnSc2	klima
(	(	kIx(	(
<g/>
IPCC	IPCC	kA	IPCC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Shrnutí	shrnutí	k1gNnSc1	shrnutí
pro	pro	k7c4	pro
politické	politický	k2eAgMnPc4d1	politický
představitele	představitel	k1gMnPc4	představitel
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
/	/	kIx~	/
<g/>
IPCC	IPCC	kA	IPCC
<g/>
,	,	kIx,	,
2013-11-27	[number]	k4	2013-11-27
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Acot	Acot	k1gInSc1	Acot
<g/>
,	,	kIx,	,
Pascal	pascal	k1gInSc1	pascal
<g/>
.	.	kIx.	.
</s>
<s>
Historie	historie	k1gFnSc1	historie
a	a	k8xC	a
změny	změna	k1gFnPc1	změna
klimatu	klima	k1gNnSc2	klima
<g/>
:	:	kIx,	:
od	od	k7c2	od
velkého	velký	k2eAgInSc2d1	velký
třesku	třesk	k1gInSc2	třesk
ke	k	k7c3	k
klimatickým	klimatický	k2eAgFnPc3d1	klimatická
katastrofám	katastrofa	k1gFnPc3	katastrofa
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
237	[number]	k4	237
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
246	[number]	k4	246
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
869	[number]	k4	869
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Atmosféra	atmosféra	k1gFnSc1	atmosféra
a	a	k8xC	a
klima	klima	k1gNnSc1	klima
<g/>
:	:	kIx,	:
aktuální	aktuální	k2eAgFnSc1d1	aktuální
otázky	otázka	k1gFnPc1	otázka
ochrany	ochrana	k1gFnSc2	ochrana
ovzduší	ovzduší	k1gNnSc2	ovzduší
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Martin	Martin	k1gMnSc1	Martin
Braniš	Braniš	k1gMnSc1	Braniš
<g/>
,	,	kIx,	,
Iva	Iva	k1gFnSc1	Iva
Hůnová	Hůnová	k1gFnSc1	Hůnová
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
351	[number]	k4	351
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
246	[number]	k4	246
<g/>
-	-	kIx~	-
<g/>
1598	[number]	k4	1598
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GORE	GORE	kA	GORE
<g/>
,	,	kIx,	,
Al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
</s>
<s>
Země	zem	k1gFnPc1	zem
na	na	k7c6	na
misce	miska	k1gFnSc6	miska
vah	váha	k1gFnPc2	váha
<g/>
:	:	kIx,	:
ekologie	ekologie	k1gFnSc1	ekologie
a	a	k8xC	a
lidský	lidský	k2eAgMnSc1d1	lidský
duch	duch	k1gMnSc1	duch
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Argo	Argo	k1gNnSc1	Argo
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
374	[number]	k4	374
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7203	[number]	k4	7203
<g/>
-	-	kIx~	-
<g/>
310	[number]	k4	310
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
HOUGHTON	HOUGHTON	kA	HOUGHTON
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
<g/>
.	.	kIx.	.
</s>
<s>
Globální	globální	k2eAgNnSc1d1	globální
oteplování	oteplování	k1gNnSc1	oteplování
<g/>
:	:	kIx,	:
úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
studia	studio	k1gNnSc2	studio
změn	změna	k1gFnPc2	změna
klimatu	klima	k1gNnSc2	klima
a	a	k8xC	a
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
228	[number]	k4	228
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
636	[number]	k4	636
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KADRNOŽKA	KADRNOŽKA	kA	KADRNOŽKA
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
.	.	kIx.	.
</s>
<s>
Energie	energie	k1gFnPc4	energie
a	a	k8xC	a
globální	globální	k2eAgNnSc4d1	globální
oteplování	oteplování	k1gNnSc4	oteplování
<g/>
:	:	kIx,	:
Země	zem	k1gFnSc2	zem
v	v	k7c6	v
proměnách	proměna	k1gFnPc6	proměna
při	při	k7c6	při
opatřování	opatřování	k1gNnSc6	opatřování
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
<g/>
:	:	kIx,	:
VUTIUM	VUTIUM	kA	VUTIUM
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
189	[number]	k4	189
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
214	[number]	k4	214
<g/>
-	-	kIx~	-
<g/>
2919	[number]	k4	2919
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KALVOVÁ	KALVOVÁ	kA	KALVOVÁ
<g/>
,	,	kIx,	,
Jaroslava	Jaroslava	k1gFnSc1	Jaroslava
<g/>
;	;	kIx,	;
MOLDAN	MOLDAN	kA	MOLDAN
<g/>
,	,	kIx,	,
Bedřich	Bedřich	k1gMnSc1	Bedřich
<g/>
.	.	kIx.	.
</s>
<s>
Klima	klima	k1gNnSc1	klima
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
změna	změna	k1gFnSc1	změna
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
emisí	emise	k1gFnPc2	emise
skleníkových	skleníkový	k2eAgInPc2d1	skleníkový
plynů	plyn	k1gInPc2	plyn
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Univerzira	Univerzira	k1gFnSc1	Univerzira
Karlova	Karlův	k2eAgFnSc1d1	Karlova
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
161	[number]	k4	161
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7184	[number]	k4	7184
<g/>
-	-	kIx~	-
<g/>
315	[number]	k4	315
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KOPÁČEK	Kopáček	k1gMnSc1	Kopáček
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
<g/>
;	;	kIx,	;
BEDNÁŘ	Bednář	k1gMnSc1	Bednář
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
vzniká	vznikat	k5eAaImIp3nS	vznikat
počasí	počasí	k1gNnSc1	počasí
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
226	[number]	k4	226
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
s.	s.	k?	s.
obr	obr	k1gMnSc1	obr
<g/>
.	.	kIx.	.
příl	příl	k1gMnSc1	příl
<g/>
.	.	kIx.	.
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
246	[number]	k4	246
<g/>
-	-	kIx~	-
<g/>
1002	[number]	k4	1002
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KUTÍLEK	KUTÍLEK	kA	KUTÍLEK
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
.	.	kIx.	.
</s>
<s>
Racionálně	racionálně	k6eAd1	racionálně
o	o	k7c6	o
globálním	globální	k2eAgNnSc6d1	globální
oteplování	oteplování	k1gNnSc6	oteplování
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Dokořán	dokořán	k6eAd1	dokořán
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
185	[number]	k4	185
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7363	[number]	k4	7363
<g/>
-	-	kIx~	-
<g/>
183	[number]	k4	183
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MAREK	Marek	k1gMnSc1	Marek
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
V.	V.	kA	V.
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Uhlík	uhlík	k1gInSc1	uhlík
v	v	k7c6	v
ekosystémech	ekosystém	k1gInPc6	ekosystém
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
měnícím	měnící	k2eAgMnSc6d1	měnící
se	se	k3xPyFc4	se
klimatu	klima	k1gNnSc3	klima
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
253	[number]	k4	253
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
1876	[number]	k4	1876
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
904351	[number]	k4	904351
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
METELKA	METELKA	k?	METELKA
<g/>
,	,	kIx,	,
Ladislav	Ladislav	k1gMnSc1	Ladislav
<g/>
;	;	kIx,	;
TOLASZ	TOLASZ	kA	TOLASZ
<g/>
,	,	kIx,	,
Radim	Radim	k1gMnSc1	Radim
<g/>
.	.	kIx.	.
</s>
<s>
Klimatické	klimatický	k2eAgFnPc1d1	klimatická
změny	změna	k1gFnPc1	změna
<g/>
:	:	kIx,	:
fakta	faktum	k1gNnPc4	faktum
bez	bez	k7c2	bez
mýtů	mýtus	k1gInPc2	mýtus
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Centrum	centrum	k1gNnSc1	centrum
pro	pro	k7c4	pro
otázky	otázka	k1gFnPc4	otázka
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
35	[number]	k4	35
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87076	[number]	k4	87076
<g/>
-	-	kIx~	-
<g/>
13	[number]	k4	13
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MCKIBBEN	MCKIBBEN	kA	MCKIBBEN
<g/>
,	,	kIx,	,
Bill	Bill	k1gMnSc1	Bill
<g/>
.	.	kIx.	.
</s>
<s>
Zeemě	Zeemě	k6eAd1	Zeemě
<g/>
:	:	kIx,	:
jak	jak	k8xS	jak
přežít	přežít	k5eAaPmF	přežít
na	na	k7c6	na
naší	náš	k3xOp1gFnSc6	náš
nové	nový	k2eAgFnSc6d1	nová
nehostinné	hostinný	k2eNgFnSc6d1	nehostinná
planetě	planeta	k1gFnSc6	planeta
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Paseka	Paseka	k1gMnSc1	Paseka
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
256	[number]	k4	256
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7432	[number]	k4	7432
<g/>
-	-	kIx~	-
<g/>
251	[number]	k4	251
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MOLDAN	MOLDAN	kA	MOLDAN
<g/>
,	,	kIx,	,
Bedřich	Bedřich	k1gMnSc1	Bedřich
<g/>
.	.	kIx.	.
</s>
<s>
Podmaněná	podmaněný	k2eAgFnSc1d1	Podmaněná
planeta	planeta	k1gFnSc1	planeta
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Karolinum	Karolinum	k1gNnSc1	Karolinum
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
419	[number]	k4	419
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
246	[number]	k4	246
<g/>
-	-	kIx~	-
<g/>
1580	[number]	k4	1580
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NÁTR	NÁTR	kA	NÁTR
<g/>
,	,	kIx,	,
Lubomír	Lubomír	k1gMnSc1	Lubomír
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
jako	jako	k8xC	jako
skleník	skleník	k1gInSc1	skleník
<g/>
:	:	kIx,	:
proč	proč	k6eAd1	proč
se	se	k3xPyFc4	se
bát	bát	k5eAaImF	bát
CO	co	k9	co
<g/>
2	[number]	k4	2
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
142	[number]	k4	142
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
1362	[number]	k4	1362
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
NOVÁČEK	Nováček	k1gMnSc1	Nováček
<g/>
,	,	kIx,	,
Pavel	Pavel	k1gMnSc1	Pavel
<g/>
;	;	kIx,	;
HUBA	huba	k1gFnSc1	huba
<g/>
,	,	kIx,	,
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
<g/>
.	.	kIx.	.
</s>
<s>
Ohrožená	ohrožený	k2eAgFnSc1d1	ohrožená
planeta	planeta	k1gFnSc1	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
:	:	kIx,	:
Univerzita	univerzita	k1gFnSc1	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
202	[number]	k4	202
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7067	[number]	k4	7067
<g/>
-	-	kIx~	-
<g/>
382	[number]	k4	382
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TORALF	TORALF	kA	TORALF
<g/>
,	,	kIx,	,
Staud	Staud	k1gMnSc1	Staud
<g/>
;	;	kIx,	;
REIMER	REIMER	kA	REIMER
<g/>
,	,	kIx,	,
Nick	Nick	k1gInSc1	Nick
<g/>
.	.	kIx.	.
</s>
<s>
Zachraňme	zachránit	k5eAaPmRp1nP	zachránit
klima	klima	k1gNnSc4	klima
<g/>
:	:	kIx,	:
ještě	ještě	k6eAd1	ještě
není	být	k5eNaImIp3nS	být
pozdě	pozdě	k6eAd1	pozdě
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
285	[number]	k4	285
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
242	[number]	k4	242
<g/>
-	-	kIx~	-
<g/>
2119	[number]	k4	2119
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SVOBODA	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
VAŠKŮ	Vašek	k1gMnPc2	Vašek
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
<g/>
;	;	kIx,	;
CÍLEK	CÍLEK	kA	CÍLEK
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
kniha	kniha	k1gFnSc1	kniha
o	o	k7c6	o
klimatu	klima	k1gNnSc6	klima
Zemí	zem	k1gFnPc2	zem
koruny	koruna	k1gFnSc2	koruna
české	český	k2eAgFnSc2d1	Česká
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
Regia	Regia	k1gFnSc1	Regia
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
655	[number]	k4	655
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86367	[number]	k4	86367
<g/>
-	-	kIx~	-
<g/>
34	[number]	k4	34
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VYSOUDIL	vysoudit	k5eAaPmAgMnS	vysoudit
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
<g/>
.	.	kIx.	.
</s>
<s>
Meteorologie	meteorologie	k1gFnSc1	meteorologie
a	a	k8xC	a
klimatologie	klimatologie	k1gFnSc1	klimatologie
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
:	:	kIx,	:
Univerzita	univerzita	k1gFnSc1	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
281	[number]	k4	281
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
244	[number]	k4	244
<g/>
-	-	kIx~	-
<g/>
1455	[number]	k4	1455
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Anglicky	anglicky	k6eAd1	anglicky
Climate	Climat	k1gInSc5	Climat
Change	change	k1gFnSc1	change
2013	[number]	k4	2013
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Physical	Physical	k1gFnSc1	Physical
Science	Science	k1gFnSc1	Science
Basis	Basis	k?	Basis
<g/>
.	.	kIx.	.
</s>
<s>
Technical	Technicat	k5eAaPmAgMnS	Technicat
Summary	Summara	k1gFnPc4	Summara
<g/>
.	.	kIx.	.
</s>
<s>
Geneva	Geneva	k1gFnSc1	Geneva
<g/>
:	:	kIx,	:
IPCC	IPCC	kA	IPCC
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
84	[number]	k4	84
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Pátá	pátý	k4xOgFnSc1	pátý
hodnotící	hodnotící	k2eAgFnSc1d1	hodnotící
zpráva	zpráva	k1gFnSc1	zpráva
IPCC	IPCC	kA	IPCC
–	–	k?	–
Pracovní	pracovní	k2eAgFnSc1d1	pracovní
skupina	skupina	k1gFnSc1	skupina
I	I	kA	I
–	–	k?	–
Technická	technický	k2eAgFnSc1d1	technická
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Climate	Climat	k1gInSc5	Climat
Change	change	k1gFnSc1	change
2013	[number]	k4	2013
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
Physical	Physical	k1gFnSc1	Physical
Science	Science	k1gFnSc1	Science
Basis	Basis	k?	Basis
<g/>
.	.	kIx.	.
</s>
<s>
Contribution	Contribution	k1gInSc1	Contribution
of	of	k?	of
Working	Working	k1gInSc1	Working
Group	Group	k1gInSc1	Group
I	i	k8xC	i
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Fifth	Fifth	k1gInSc1	Fifth
Assessment	Assessment	k1gMnSc1	Assessment
Report	report	k1gInSc1	report
of	of	k?	of
the	the	k?	the
Intergovernmental	Intergovernmental	k1gMnSc1	Intergovernmental
Panel	panel	k1gInSc1	panel
on	on	k3xPp3gMnSc1	on
Climate	Climat	k1gInSc5	Climat
Change	change	k1gFnSc5	change
<g/>
.	.	kIx.	.
</s>
<s>
Geneva	Geneva	k1gFnSc1	Geneva
<g/>
:	:	kIx,	:
IPCC	IPCC	kA	IPCC
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
1552	[number]	k4	1552
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Pátá	pátý	k4xOgFnSc1	pátý
hodnotící	hodnotící	k2eAgFnSc1d1	hodnotící
zpráva	zpráva	k1gFnSc1	zpráva
IPCC	IPCC	kA	IPCC
–	–	k?	–
Pracovní	pracovní	k2eAgFnSc1d1	pracovní
skupina	skupina	k1gFnSc1	skupina
I	i	k8xC	i
–	–	k?	–
Fyzikální	fyzikální	k2eAgInPc4d1	fyzikální
základy	základ	k1gInPc4	základ
<g/>
.	.	kIx.	.
</s>
<s>
Plný	plný	k2eAgInSc1d1	plný
text	text	k1gInSc1	text
zprávy	zpráva	k1gFnSc2	zpráva
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Atlas	Atlas	k1gInSc1	Atlas
of	of	k?	of
Mortality	mortalita	k1gFnSc2	mortalita
and	and	k?	and
Economic	Economic	k1gMnSc1	Economic
Losses	Losses	k1gMnSc1	Losses
from	from	k1gMnSc1	from
Weather	Weathra	k1gFnPc2	Weathra
<g/>
,	,	kIx,	,
Climate	Climat	k1gInSc5	Climat
and	and	k?	and
Water	Water	k1gMnSc1	Water
Extremes	Extremes	k1gMnSc1	Extremes
(	(	kIx(	(
<g/>
1970	[number]	k4	1970
<g/>
–	–	k?	–
<g/>
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Geneva	Geneva	k1gFnSc1	Geneva
<g/>
:	:	kIx,	:
World	World	k1gInSc1	World
Meteorological	Meteorological	k1gMnSc1	Meteorological
Organization	Organization	k1gInSc1	Organization
(	(	kIx(	(
<g/>
WMO	WMO	kA	WMO
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
48	[number]	k4	48
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
92	[number]	k4	92
<g/>
-	-	kIx~	-
<g/>
63	[number]	k4	63
<g/>
-	-	kIx~	-
<g/>
11123	[number]	k4	11123
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Atlas	Atlas	k1gInSc1	Atlas
mortality	mortalita	k1gFnSc2	mortalita
a	a	k8xC	a
ekonomických	ekonomický	k2eAgFnPc2d1	ekonomická
ztrát	ztráta	k1gFnPc2	ztráta
<g/>
,	,	kIx,	,
způsobených	způsobený	k2eAgFnPc2d1	způsobená
počasím	počasí	k1gNnSc7	počasí
<g/>
,	,	kIx,	,
klimatickým	klimatický	k2eAgNnSc7d1	klimatické
a	a	k8xC	a
vodními	vodní	k2eAgInPc7d1	vodní
extrémy	extrém	k1gInPc7	extrém
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1970	[number]	k4	1970
<g/>
–	–	k?	–
<g/>
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
oficiální	oficiální	k2eAgFnSc1d1	oficiální
publikace	publikace	k1gFnSc1	publikace
Světové	světový	k2eAgFnSc2d1	světová
zdravotnické	zdravotnický	k2eAgFnSc2d1	zdravotnická
organizace	organizace	k1gFnSc2	organizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
globální	globální	k2eAgFnSc2d1	globální
oteplování	oteplování	k1gNnSc4	oteplování
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
instituce	instituce	k1gFnSc2	instituce
Klimatický	klimatický	k2eAgInSc1d1	klimatický
panel	panel	k1gInSc1	panel
OSN	OSN	kA	OSN
<g/>
/	/	kIx~	/
<g/>
IPCC	IPCC	kA	IPCC
–	–	k?	–
Intergovernmental	Intergovernmental	k1gMnSc1	Intergovernmental
Panel	panel	k1gInSc1	panel
on	on	k3xPp3gMnSc1	on
Climate	Climat	k1gInSc5	Climat
Change	change	k1gFnSc1	change
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2018	[number]	k4	2018
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Opatření	opatření	k1gNnPc1	opatření
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
klimatu	klima	k1gNnSc2	klima
<g/>
.	.	kIx.	.
</s>
<s>
Opatření	opatření	k1gNnPc1	opatření
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
klimatu	klima	k1gNnSc2	klima
-	-	kIx~	-
European	European	k1gInSc1	European
Commission	Commission	k1gInSc1	Commission
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2018	[number]	k4	2018
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
MŽP	MŽP	kA	MŽP
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
klimatu	klima	k1gNnSc2	klima
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
garant	garant	k1gMnSc1	garant
problematiky	problematika	k1gFnSc2	problematika
za	za	k7c4	za
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
meziresortní	meziresortní	k2eAgMnSc1d1	meziresortní
koordinátor	koordinátor	k1gMnSc1	koordinátor
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
www.mzp.cz	www.mzp.cz	k1gInSc1	www.mzp.cz
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2008-08-11	[number]	k4	2008-08-11
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2018	[number]	k4	2018
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zmírnění	zmírnění	k1gNnSc1	zmírnění
změny	změna	k1gFnSc2	změna
klimatu	klima	k1gNnSc2	klima
<g/>
.	.	kIx.	.
</s>
<s>
European	European	k1gMnSc1	European
Environment	Environment	k1gMnSc1	Environment
Agency	Agenca	k1gFnSc2	Agenca
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2018	[number]	k4	2018
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Global	globat	k5eAaImAgInS	globat
Warming	Warming	k1gInSc1	Warming
<g/>
.	.	kIx.	.
earthobservatory	earthobservator	k1gInPc1	earthobservator
<g/>
.	.	kIx.	.
<g/>
nasa	nasa	k1gFnSc1	nasa
<g/>
.	.	kIx.	.
<g/>
gov	gov	k?	gov
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2010-06-03	[number]	k4	2010-06-03
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2018	[number]	k4	2018
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CzechGlobe	CzechGlobat	k5eAaPmIp3nS	CzechGlobat
|	|	kIx~	|
Ústav	ústav	k1gInSc1	ústav
výzkumu	výzkum	k1gInSc2	výzkum
globální	globální	k2eAgFnSc2d1	globální
změny	změna	k1gFnSc2	změna
AV	AV	kA	AV
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
v.	v.	k?	v.
v.	v.	k?	v.
i.	i.	k?	i.
<g/>
.	.	kIx.	.
www.czechglobe.cz	www.czechglobe.cz	k1gMnSc1	www.czechglobe.cz
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2018	[number]	k4	2018
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
NOAA	NOAA	kA	NOAA
Status	status	k1gInSc1	status
Alert	Alert	k1gInSc1	Alert
-	-	kIx~	-
měscíční	měscíčnět	k5eAaPmIp3nS	měscíčnět
klimatické	klimatický	k2eAgFnPc4d1	klimatická
zprávy	zpráva	k1gFnPc4	zpráva
NOAA	NOAA	kA	NOAA
pro	pro	k7c4	pro
USA	USA	kA	USA
a	a	k8xC	a
celosvětové	celosvětový	k2eAgNnSc1d1	celosvětové
<g/>
.	.	kIx.	.
governmentshutdown	governmentshutdown	k1gNnSc1	governmentshutdown
<g/>
.	.	kIx.	.
<g/>
noaa	noaa	k1gFnSc1	noaa
<g/>
.	.	kIx.	.
<g/>
gov	gov	k?	gov
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2019	[number]	k4	2019
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Climate	Climat	k1gInSc5	Climat
and	and	k?	and
Weather	Weathra	k1gFnPc2	Weathra
-	-	kIx~	-
Expert	expert	k1gMnSc1	expert
Reports	Reportsa	k1gFnPc2	Reportsa
-	-	kIx~	-
Division	Division	k1gInSc4	Division
on	on	k3xPp3gMnSc1	on
Earth	Earth	k1gMnSc1	Earth
and	and	k?	and
Life	Life	k1gInSc1	Life
Studies	Studies	k1gInSc1	Studies
-	-	kIx~	-
repository	repositor	k1gInPc1	repositor
zpráv	zpráva	k1gFnPc2	zpráva
Národních	národní	k2eAgFnPc2d1	národní
akademií	akademie	k1gFnPc2	akademie
USA	USA	kA	USA
<g/>
.	.	kIx.	.
dels	dels	k1gInSc1	dels
<g/>
.	.	kIx.	.
<g/>
nas	nas	k?	nas
<g/>
.	.	kIx.	.
<g/>
edu	edu	k?	edu
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2019	[number]	k4	2019
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Nature	Natur	k1gMnSc5	Natur
Climate	Climat	k1gInSc5	Climat
Change	change	k1gFnSc1	change
-	-	kIx~	-
časopis	časopis	k1gInSc1	časopis
Nature	Natur	k1gMnSc5	Natur
<g/>
,	,	kIx,	,
sekce	sekce	k1gFnSc2	sekce
Klimatická	klimatický	k2eAgFnSc1d1	klimatická
změna	změna	k1gFnSc1	změna
<g/>
.	.	kIx.	.
</s>
<s>
Nature	Natur	k1gMnSc5	Natur
Climate	Climat	k1gInSc5	Climat
Change	change	k1gFnSc1	change
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2019	[number]	k4	2019
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Climate	Climat	k1gInSc5	Climat
Guide	Guid	k1gInSc5	Guid
-	-	kIx~	-
Klimatický	klimatický	k2eAgMnSc1d1	klimatický
průvodce	průvodce	k1gMnSc1	průvodce
anglické	anglický	k2eAgFnSc2d1	anglická
meteorologické	meteorologický	k2eAgFnSc2d1	meteorologická
služby	služba	k1gFnSc2	služba
<g/>
.	.	kIx.	.
</s>
<s>
Met	met	k1gInSc1	met
Office	Office	kA	Office
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2019	[number]	k4	2019
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
EdGCM	EdGCM	k?	EdGCM
-	-	kIx~	-
simulátor	simulátor	k1gInSc1	simulátor
klimatické	klimatický	k2eAgFnSc2d1	klimatická
změny	změna	k1gFnSc2	změna
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2019	[number]	k4	2019
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Populární	populární	k2eAgFnPc4d1	populární
stránky	stránka	k1gFnPc4	stránka
k	k	k7c3	k
problematice	problematika	k1gFnSc3	problematika
globálního	globální	k2eAgNnSc2d1	globální
oteplování	oteplování	k1gNnSc2	oteplování
Global	globat	k5eAaImAgInS	globat
Warming	Warming	k1gInSc1	Warming
and	and	k?	and
Climate	Climat	k1gInSc5	Climat
Change	change	k1gFnSc2	change
skepticism	skepticismo	k1gNnPc2	skepticismo
examined	examined	k1gMnSc1	examined
-stránky	tránka	k1gFnSc2	-stránka
vysvětlující	vysvětlující	k2eAgMnSc1d1	vysvětlující
různé	různý	k2eAgInPc4d1	různý
problémy	problém	k1gInPc4	problém
globálního	globální	k2eAgNnSc2d1	globální
oteplování	oteplování	k1gNnSc2	oteplování
a	a	k8xC	a
také	také	k9	také
objasňující	objasňující	k2eAgFnSc4d1	objasňující
většinu	většina	k1gFnSc4	většina
odlišných	odlišný	k2eAgInPc2d1	odlišný
názorů	názor	k1gInPc2	názor
na	na	k7c4	na
něj	on	k3xPp3gNnSc4	on
-	-	kIx~	-
částečně	částečně	k6eAd1	částečně
i	i	k9	i
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Skeptical	Skepticat	k5eAaPmAgMnS	Skepticat
Science	Science	k1gFnPc4	Science
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2019	[number]	k4	2019
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Aktuality	aktualita	k1gFnPc1	aktualita
-	-	kIx~	-
Změna	změna	k1gFnSc1	změna
klimatu	klima	k1gNnSc2	klima
<g/>
.	.	kIx.	.
www.zmenaklimatu.cz	www.zmenaklimatu.cz	k1gInSc1	www.zmenaklimatu.cz
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2019	[number]	k4	2019
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Globální	globální	k2eAgInSc1d1	globální
klimatický	klimatický	k2eAgInSc1d1	klimatický
rozvrat	rozvrat	k1gInSc1	rozvrat
-	-	kIx~	-
elektronická	elektronický	k2eAgFnSc1d1	elektronická
knihovna	knihovna	k1gFnSc1	knihovna
dokumentů	dokument	k1gInPc2	dokument
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
.	.	kIx.	.
amper	amper	k1gInSc1	amper
<g/>
.	.	kIx.	.
<g/>
ped	ped	k?	ped
<g/>
.	.	kIx.	.
<g/>
muni	muni	k?	muni
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2019	[number]	k4	2019
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
RealClimate	RealClimat	k1gInSc5	RealClimat
-	-	kIx~	-
stránky	stránka	k1gFnSc2	stránka
M.	M.	kA	M.
Manna	Mann	k1gMnSc2	Mann
<g/>
.	.	kIx.	.
</s>
<s>
RealClimate	RealClimat	k1gInSc5	RealClimat
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2019	[number]	k4	2019
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Informace	informace	k1gFnPc1	informace
pro	pro	k7c4	pro
laiky	laik	k1gMnPc4	laik
Weather	Weathra	k1gFnPc2	Weathra
and	and	k?	and
Climate	Climat	k1gInSc5	Climat
Basics	Basics	k1gInSc4	Basics
-stránky	tránka	k1gFnSc2	-stránka
Národního	národní	k2eAgNnSc2d1	národní
centra	centrum	k1gNnSc2	centrum
pro	pro	k7c4	pro
atmosférický	atmosférický	k2eAgInSc4d1	atmosférický
výzkum	výzkum	k1gInSc4	výzkum
(	(	kIx(	(
<g/>
NCAR	NCAR	kA	NCAR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
www.eo.ucar.edu	www.eo.ucar.edu	k6eAd1	www.eo.ucar.edu
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2019	[number]	k4	2019
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Center	centrum	k1gNnPc2	centrum
for	forum	k1gNnPc2	forum
Climate	Climat	k1gInSc5	Climat
and	and	k?	and
Energy	Energ	k1gInPc1	Energ
Solutions	Solutions	k1gInSc1	Solutions
-	-	kIx~	-
Základní	základní	k2eAgFnSc1d1	základní
informace	informace	k1gFnSc1	informace
ke	k	k7c3	k
globálnímu	globální	k2eAgNnSc3d1	globální
oteplování	oteplování	k1gNnSc3	oteplování
<g/>
]	]	kIx)	]
stránky	stránka	k1gFnPc4	stránka
Centra	centrum	k1gNnSc2	centrum
pro	pro	k7c4	pro
klima	klima	k1gNnSc4	klima
a	a	k8xC	a
energetická	energetický	k2eAgNnPc4d1	energetické
řešení	řešení	k1gNnPc4	řešení
(	(	kIx(	(
<g/>
C	C	kA	C
<g/>
2	[number]	k4	2
<g/>
ES	ES	kA	ES
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2014-03-13	[number]	k4	2014-03-13
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2019	[number]	k4	2019
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
America	America	k1gFnSc1	America
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Climate	Climat	k1gInSc5	Climat
Choices	Choices	k1gMnSc1	Choices
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
:	:	kIx,	:
Division	Division	k1gInSc4	Division
on	on	k3xPp3gMnSc1	on
Earth	Earth	k1gMnSc1	Earth
and	and	k?	and
Life	Life	k1gInSc1	Life
Studies	Studies	k1gInSc1	Studies
-	-	kIx~	-
stránky	stránka	k1gFnSc2	stránka
Národních	národní	k2eAgInPc2d1	národní
akademii	akademie	k1gFnSc3	akademie
věd	věda	k1gFnPc2	věda
USA	USA	kA	USA
<g/>
.	.	kIx.	.
dels	dels	k1gInSc1	dels
<g/>
.	.	kIx.	.
<g/>
nas	nas	k?	nas
<g/>
.	.	kIx.	.
<g/>
edu	edu	k?	edu
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2019	[number]	k4	2019
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Climate	Climat	k1gInSc5	Climat
Change	change	k1gFnSc1	change
(	(	kIx(	(
<g/>
main	main	k1gInSc1	main
<g/>
)	)	kIx)	)
-	-	kIx~	-
The	The	k1gFnSc3	The
Encyclopedia	Encyclopedium	k1gNnSc2	Encyclopedium
of	of	k?	of
Earth	Earth	k1gMnSc1	Earth
-	-	kIx~	-
Články	článek	k1gInPc1	článek
o	o	k7c6	o
změně	změna	k1gFnSc6	změna
klimatu	klima	k1gNnSc2	klima
-	-	kIx~	-
stránky	stránka	k1gFnSc2	stránka
na	na	k7c4	na
Encyklopedii	encyklopedie	k1gFnSc4	encyklopedie
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
editors	editors	k1gInSc1	editors
<g/>
.	.	kIx.	.
<g/>
eol	eol	k?	eol
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2019	[number]	k4	2019
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
