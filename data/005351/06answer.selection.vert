<s>
Irská	irský	k2eAgFnSc1d1	irská
neutrální	neutrální	k2eAgFnSc1d1	neutrální
politika	politika	k1gFnSc1	politika
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
členem	člen	k1gMnSc7	člen
NATO	NATO	kA	NATO
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
na	na	k7c6	na
mírových	mírový	k2eAgFnPc6d1	mírová
operacích	operace	k1gFnPc6	operace
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
