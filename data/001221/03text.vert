<s>
Barack	Barack	k1gMnSc1	Barack
Hussein	Hussein	k1gMnSc1	Hussein
Obama	Obama	k?	Obama
(	(	kIx(	(
<g/>
*	*	kIx~	*
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1961	[number]	k4	1961
Honolulu	Honolulu	k1gNnSc2	Honolulu
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2009	[number]	k4	2009
stávající	stávající	k2eAgFnSc1d1	stávající
44	[number]	k4	44
<g/>
.	.	kIx.	.
prezident	prezident	k1gMnSc1	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
a	a	k8xC	a
první	první	k4xOgMnSc1	první
Afroameričan	Afroameričan	k1gMnSc1	Afroameričan
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
úřadu	úřad	k1gInSc6	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1992	[number]	k4	1992
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yIgFnSc4	který
zasedal	zasedat	k5eAaImAgInS	zasedat
v	v	k7c6	v
senátech	senát	k1gInPc6	senát
státu	stát	k1gInSc2	stát
Illinois	Illinois	k1gFnSc1	Illinois
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
Kongresu	kongres	k1gInSc6	kongres
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nositelem	nositel	k1gMnSc7	nositel
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
míru	mír	k1gInSc2	mír
a	a	k8xC	a
časopis	časopis	k1gInSc1	časopis
Time	Tim	k1gInSc2	Tim
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2008	[number]	k4	2008
a	a	k8xC	a
2012	[number]	k4	2012
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
Osobností	osobnost	k1gFnSc7	osobnost
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2012	[number]	k4	2012
podruhé	podruhé	k6eAd1	podruhé
vyhrál	vyhrát	k5eAaPmAgMnS	vyhrát
prezidentské	prezidentský	k2eAgFnPc4d1	prezidentská
volby	volba	k1gFnPc4	volba
a	a	k8xC	a
obhájil	obhájit	k5eAaPmAgMnS	obhájit
tím	ten	k3xDgNnSc7	ten
svůj	svůj	k3xOyFgInSc4	svůj
úřad	úřad	k1gInSc4	úřad
na	na	k7c4	na
další	další	k2eAgInPc4d1	další
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Barack	Barack	k1gInSc1	Barack
Obama	Obama	k?	Obama
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
do	do	k7c2	do
rodiny	rodina	k1gFnSc2	rodina
matky	matka	k1gFnSc2	matka
Stanley	Stanlea	k1gFnSc2	Stanlea
Ann	Ann	k1gFnSc2	Ann
Dunhamové	Dunhamový	k2eAgNnSc1d1	Dunhamový
(	(	kIx(	(
<g/>
americká	americký	k2eAgFnSc1d1	americká
běloška	běloška	k1gFnSc1	běloška
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc7	jeho
otcem	otec	k1gMnSc7	otec
byl	být	k5eAaImAgMnS	být
Barack	Barack	k1gMnSc1	Barack
Hussein	Hussein	k1gMnSc1	Hussein
Obama	Obama	k?	Obama
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
kmene	kmen	k1gInSc2	kmen
Luo	Luo	k1gFnSc2	Luo
v	v	k7c6	v
Keni	Keňa	k1gFnSc6	Keňa
(	(	kIx(	(
<g/>
která	který	k3yIgFnSc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgNnP	být
ještě	ještě	k6eAd1	ještě
kolonií	kolonie	k1gFnSc7	kolonie
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
vzděláním	vzdělání	k1gNnSc7	vzdělání
ekonom	ekonom	k1gMnSc1	ekonom
a	a	k8xC	a
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c6	na
Harvardově	Harvardův	k2eAgFnSc6d1	Harvardova
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
rodiny	rodina	k1gFnSc2	rodina
záhy	záhy	k6eAd1	záhy
odešel	odejít	k5eAaPmAgMnS	odejít
a	a	k8xC	a
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
rodné	rodný	k2eAgFnSc2d1	rodná
země	zem	k1gFnSc2	zem
Keni	Keňa	k1gFnSc2	Keňa
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vládním	vládní	k2eAgMnSc7d1	vládní
úředníkem	úředník	k1gMnSc7	úředník
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Nairobi	Nairobi	k1gNnSc2	Nairobi
při	při	k7c6	při
automobilové	automobilový	k2eAgFnSc6d1	automobilová
nehodě	nehoda	k1gFnSc6	nehoda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
<s>
Obamova	Obamův	k2eAgFnSc1d1	Obamova
matka	matka	k1gFnSc1	matka
vystudovala	vystudovat	k5eAaPmAgFnS	vystudovat
na	na	k7c6	na
Havajské	havajský	k2eAgFnSc6d1	Havajská
univerzitě	univerzita	k1gFnSc6	univerzita
obor	obora	k1gFnPc2	obora
kulturní	kulturní	k2eAgFnSc2d1	kulturní
antropologie	antropologie	k1gFnSc2	antropologie
a	a	k8xC	a
zemřela	zemřít	k5eAaPmAgFnS	zemřít
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
<g/>
.	.	kIx.	.
</s>
<s>
Obama	Obama	k?	Obama
strávil	strávit	k5eAaPmAgInS	strávit
své	svůj	k3xOyFgNnSc4	svůj
rané	raný	k2eAgNnSc4d1	rané
dětství	dětství	k1gNnSc4	dětství
v	v	k7c6	v
havajském	havajský	k2eAgNnSc6d1	havajské
Honolulu	Honolulu	k1gNnSc6	Honolulu
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
od	od	k7c2	od
svých	svůj	k3xOyFgNnPc2	svůj
šesti	šest	k4xCc2	šest
do	do	k7c2	do
deseti	deset	k4xCc2	deset
let	léto	k1gNnPc2	léto
žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Indonésie	Indonésie	k1gFnSc1	Indonésie
Jakartě	Jakarta	k1gFnSc3	Jakarta
se	s	k7c7	s
svojí	svůj	k3xOyFgFnSc7	svůj
matkou	matka	k1gFnSc7	matka
a	a	k8xC	a
nevlastním	vlastní	k2eNgMnSc7d1	nevlastní
otcem	otec	k1gMnSc7	otec
Lolo	Lolo	k1gMnSc1	Lolo
Soetorem	Soetor	k1gMnSc7	Soetor
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterého	který	k3yIgMnSc4	který
se	se	k3xPyFc4	se
Dunhamová	Dunhamový	k2eAgFnSc1d1	Dunhamová
vdala	vdát	k5eAaPmAgFnS	vdát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1965	[number]	k4	1965
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
byl	být	k5eAaImAgInS	být
poslán	poslat	k5eAaPmNgInS	poslat
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
prarodičům	prarodič	k1gMnPc3	prarodič
na	na	k7c4	na
Havaj	Havaj	k1gFnSc4	Havaj
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mohl	moct	k5eAaImAgInS	moct
získat	získat	k5eAaPmF	získat
lepší	dobrý	k2eAgNnSc4d2	lepší
vzdělání	vzdělání	k1gNnSc4	vzdělání
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
o	o	k7c4	o
něj	on	k3xPp3gMnSc4	on
starala	starat	k5eAaImAgFnS	starat
jeho	jeho	k3xOp3gFnSc1	jeho
babička	babička	k1gFnSc1	babička
Madelyn	Madelyna	k1gFnPc2	Madelyna
Dunhamová	Dunhamový	k2eAgFnSc1d1	Dunhamová
(	(	kIx(	(
<g/>
zemřela	zemřít	k5eAaPmAgFnS	zemřít
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2008	[number]	k4	2008
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prarodiče	prarodič	k1gMnPc1	prarodič
mu	on	k3xPp3gMnSc3	on
zaplatili	zaplatit	k5eAaPmAgMnP	zaplatit
soukromé	soukromý	k2eAgNnSc4d1	soukromé
studium	studium	k1gNnSc4	studium
na	na	k7c6	na
akademii	akademie	k1gFnSc6	akademie
Punahou	Punaha	k1gFnSc7	Punaha
School	School	k1gInSc1	School
v	v	k7c6	v
Honolulu	Honolulu	k1gNnSc6	Honolulu
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
se	se	k3xPyFc4	se
rozvedla	rozvést	k5eAaPmAgFnS	rozvést
a	a	k8xC	a
vrátila	vrátit	k5eAaPmAgFnS	vrátit
se	se	k3xPyFc4	se
na	na	k7c4	na
Havaj	Havaj	k1gFnSc4	Havaj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
absolvování	absolvování	k1gNnSc6	absolvování
Punahou	Punaha	k1gFnSc7	Punaha
School	School	k1gInSc1	School
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
si	se	k3xPyFc3	se
Obama	Obama	k?	Obama
pro	pro	k7c4	pro
další	další	k2eAgNnSc4d1	další
studium	studium	k1gNnSc4	studium
překvapivě	překvapivě	k6eAd1	překvapivě
vybral	vybrat	k5eAaPmAgMnS	vybrat
Occidental	Occidental	k1gMnSc1	Occidental
College	Colleg	k1gInSc2	Colleg
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pobytu	pobyt	k1gInSc6	pobyt
v	v	k7c6	v
Kalifornii	Kalifornie	k1gFnSc6	Kalifornie
si	se	k3xPyFc3	se
vyjasnil	vyjasnit	k5eAaPmAgMnS	vyjasnit
svoji	svůj	k3xOyFgFnSc4	svůj
rasově	rasově	k6eAd1	rasově
smíšenou	smíšený	k2eAgFnSc4d1	smíšená
identitu	identita	k1gFnSc4	identita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
na	na	k7c4	na
známou	známý	k2eAgFnSc4d1	známá
Columbia	Columbia	k1gFnSc1	Columbia
University	universita	k1gFnPc1	universita
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
získal	získat	k5eAaPmAgInS	získat
titul	titul	k1gInSc1	titul
bakaláře	bakalář	k1gMnSc2	bakalář
při	při	k7c6	při
studiu	studio	k1gNnSc6	studio
politologie	politologie	k1gFnSc2	politologie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
studií	studio	k1gNnPc2	studio
se	se	k3xPyFc4	se
zajímal	zajímat	k5eAaImAgMnS	zajímat
např.	např.	kA	např.
o	o	k7c4	o
jaderné	jaderný	k2eAgNnSc4d1	jaderné
odzbrojování	odzbrojování	k1gNnSc4	odzbrojování
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
studijní	studijní	k2eAgInSc1d1	studijní
průměr	průměr	k1gInSc1	průměr
byl	být	k5eAaImAgInS	být
2,6	[number]	k4	2,6
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
studiích	studio	k1gNnPc6	studio
se	se	k3xPyFc4	se
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k8xS	jako
komunitní	komunitní	k2eAgMnSc1d1	komunitní
pracovník	pracovník	k1gMnSc1	pracovník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
přijal	přijmout	k5eAaPmAgMnS	přijmout
nabídku	nabídka	k1gFnSc4	nabídka
práce	práce	k1gFnSc2	práce
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
jako	jako	k8xS	jako
terénní	terénní	k2eAgMnSc1d1	terénní
pracovník	pracovník	k1gMnSc1	pracovník
v	v	k7c6	v
tamější	tamější	k2eAgFnSc6d1	tamější
velké	velký	k2eAgFnSc6d1	velká
chudinské	chudinský	k2eAgFnSc6d1	chudinská
čtvrti	čtvrt	k1gFnSc6	čtvrt
Far	fara	k1gFnPc2	fara
South	South	k1gInSc1	South
Side	Sid	k1gInSc2	Sid
s	s	k7c7	s
převážně	převážně	k6eAd1	převážně
černošským	černošský	k2eAgNnSc7d1	černošské
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
prvního	první	k4xOgMnSc2	první
afroamerického	afroamerický	k2eAgMnSc2d1	afroamerický
starosty	starosta	k1gMnSc2	starosta
Chicaga	Chicago	k1gNnSc2	Chicago
Harolda	Harold	k1gMnSc2	Harold
Washingtona	Washington	k1gMnSc2	Washington
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
právnickému	právnický	k2eAgInSc3d1	právnický
vzdělání	vzdělání	k1gNnSc1	vzdělání
dokázal	dokázat	k5eAaPmAgInS	dokázat
prosadit	prosadit	k5eAaPmF	prosadit
mnoho	mnoho	k4c4	mnoho
opatření	opatření	k1gNnPc2	opatření
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
menšin	menšina	k1gFnPc2	menšina
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Obama	Obama	k?	Obama
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
studovat	studovat	k5eAaImF	studovat
na	na	k7c6	na
Právnické	právnický	k2eAgFnSc6d1	právnická
fakultě	fakulta	k1gFnSc6	fakulta
(	(	kIx(	(
<g/>
Law	Law	k1gFnSc1	Law
School	Schoola	k1gFnPc2	Schoola
<g/>
)	)	kIx)	)
Harvardovy	Harvardův	k2eAgFnSc2d1	Harvardova
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Bostonu	Boston	k1gInSc6	Boston
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
byl	být	k5eAaImAgMnS	být
brzy	brzy	k6eAd1	brzy
zvolen	zvolit	k5eAaPmNgMnS	zvolit
prvním	první	k4xOgMnSc6	první
afroamerickým	afroamerický	k2eAgMnSc7d1	afroamerický
šéfredaktorem	šéfredaktor	k1gMnSc7	šéfredaktor
prestižního	prestižní	k2eAgInSc2d1	prestižní
školního	školní	k2eAgInSc2d1	školní
časopisu	časopis	k1gInSc2	časopis
Harvard	Harvard	k1gInSc1	Harvard
Law	Law	k1gMnSc1	Law
Review	Review	k1gMnSc1	Review
<g/>
.	.	kIx.	.
</s>
<s>
Právnickou	právnický	k2eAgFnSc4d1	právnická
fakultu	fakulta	k1gFnSc4	fakulta
této	tento	k3xDgFnSc2	tento
univerzity	univerzita	k1gFnSc2	univerzita
ukončil	ukončit	k5eAaPmAgInS	ukončit
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
s	s	k7c7	s
vyznamenáním	vyznamenání	k1gNnSc7	vyznamenání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
se	se	k3xPyFc4	se
Barack	Barack	k1gMnSc1	Barack
Obama	Obama	k?	Obama
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
afroamerickou	afroamerický	k2eAgFnSc7d1	afroamerická
právničkou	právnička	k1gFnSc7	právnička
Michelle	Michelle	k1gFnSc2	Michelle
Robinsonovou	Robinsonová	k1gFnSc4	Robinsonová
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
při	při	k7c6	při
praxi	praxe	k1gFnSc6	praxe
v	v	k7c6	v
advokátní	advokátní	k2eAgFnSc6d1	advokátní
kanceláři	kancelář	k1gFnSc6	kancelář
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
spolu	spolu	k6eAd1	spolu
dvě	dva	k4xCgFnPc4	dva
dcery	dcera	k1gFnPc4	dcera
<g/>
;	;	kIx,	;
Malia	Malia	k1gFnSc1	Malia
Ann	Ann	k1gFnSc1	Ann
se	se	k3xPyFc4	se
narodila	narodit	k5eAaPmAgFnS	narodit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
a	a	k8xC	a
Natasha	Natasha	k1gMnSc1	Natasha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
nabídek	nabídka	k1gFnPc2	nabídka
z	z	k7c2	z
renomovaných	renomovaný	k2eAgFnPc2d1	renomovaná
advokátních	advokátní	k2eAgFnPc2d1	advokátní
kanceláří	kancelář	k1gFnPc2	kancelář
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
pro	pro	k7c4	pro
kancelář	kancelář	k1gFnSc4	kancelář
Miner	Minra	k1gFnPc2	Minra
<g/>
,	,	kIx,	,
Barnhill	Barnhilla	k1gFnPc2	Barnhilla
<g/>
&	&	k?	&
<g/>
Galland	Gallanda	k1gFnPc2	Gallanda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
pomocí	pomoc	k1gFnPc2	pomoc
minoritám	minorita	k1gFnPc3	minorita
a	a	k8xC	a
ochranou	ochrana	k1gFnSc7	ochrana
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1993	[number]	k4	1993
<g/>
-	-	kIx~	-
<g/>
2004	[number]	k4	2004
přednášel	přednášet	k5eAaImAgMnS	přednášet
ústavní	ústavní	k2eAgNnSc4d1	ústavní
právo	právo	k1gNnSc4	právo
na	na	k7c6	na
Chicagské	chicagský	k2eAgFnSc6d1	Chicagská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
napsal	napsat	k5eAaBmAgMnS	napsat
svou	svůj	k3xOyFgFnSc4	svůj
autobiografii	autobiografie	k1gFnSc4	autobiografie
Sny	sen	k1gInPc1	sen
mého	můj	k1gMnSc2	můj
otce	otec	k1gMnSc2	otec
<g/>
:	:	kIx,	:
Příběh	příběh	k1gInSc1	příběh
rasy	rasa	k1gFnSc2	rasa
a	a	k8xC	a
dědictví	dědictví	k1gNnSc2	dědictví
(	(	kIx(	(
<g/>
Dreams	Dreams	k1gInSc1	Dreams
from	from	k6eAd1	from
My	my	k3xPp1nPc1	my
Father	Fathra	k1gFnPc2	Fathra
<g/>
:	:	kIx,	:
A	a	k9	a
Story	story	k1gFnSc1	story
of	of	k?	of
Race	Rac	k1gFnSc2	Rac
and	and	k?	and
Inheritance	inheritance	k1gFnSc2	inheritance
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
stala	stát	k5eAaPmAgFnS	stát
bestsellerem	bestseller	k1gInSc7	bestseller
a	a	k8xC	a
její	její	k3xOp3gFnSc1	její
mluvená	mluvený	k2eAgFnSc1d1	mluvená
verze	verze	k1gFnSc1	verze
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
cenu	cena	k1gFnSc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
napsal	napsat	k5eAaBmAgInS	napsat
Obama	Obama	k?	Obama
další	další	k2eAgFnSc4d1	další
knihu	kniha	k1gFnSc4	kniha
s	s	k7c7	s
názvem	název	k1gInSc7	název
Smělost	smělost	k1gFnSc1	smělost
naděje	naděje	k1gFnSc1	naděje
<g/>
:	:	kIx,	:
Myšlenky	myšlenka	k1gFnPc1	myšlenka
na	na	k7c4	na
uskutečnění	uskutečnění	k1gNnSc4	uskutečnění
amerického	americký	k2eAgInSc2d1	americký
snu	sen	k1gInSc2	sen
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Audacity	Audacit	k1gInPc1	Audacit
of	of	k?	of
Hope	Hope	k1gInSc1	Hope
<g/>
:	:	kIx,	:
Thoughts	Thoughts	k1gInSc1	Thoughts
on	on	k3xPp3gInSc1	on
Reclaiming	Reclaiming	k1gInSc1	Reclaiming
the	the	k?	the
American	American	k1gInSc1	American
Dream	Dream	k1gInSc1	Dream
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obama	Obama	k?	Obama
má	mít	k5eAaImIp3nS	mít
následujících	následující	k2eAgInPc2d1	následující
osm	osm	k4xCc4	osm
žijících	žijící	k2eAgMnPc2d1	žijící
nevlastních	vlastní	k2eNgMnPc2d1	nevlastní
sourozenců	sourozenec	k1gMnPc2	sourozenec
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
otce	otec	k1gMnSc2	otec
a	a	k8xC	a
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgMnSc1	jeden
bratr	bratr	k1gMnSc1	bratr
již	již	k6eAd1	již
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Abongo	Abongo	k1gMnSc1	Abongo
Obama	Obama	k?	Obama
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
-	-	kIx~	-
zvaný	zvaný	k2eAgInSc1d1	zvaný
Malik	Malik	k1gInSc1	Malik
Auma	Aum	k1gInSc2	Aum
Obama	Obama	k?	Obama
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
-	-	kIx~	-
sestra	sestra	k1gFnSc1	sestra
<g/>
,	,	kIx,	,
vystudovala	vystudovat	k5eAaPmAgFnS	vystudovat
germanistiku	germanistika	k1gFnSc4	germanistika
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žila	žít	k5eAaImAgFnS	žít
16	[number]	k4	16
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
-	-	kIx~	-
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Získala	získat	k5eAaPmAgFnS	získat
zde	zde	k6eAd1	zde
doktorát	doktorát	k1gInSc4	doktorát
(	(	kIx(	(
<g/>
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
phil	phil	k1gMnSc1	phil
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
Keni	Keňa	k1gFnSc6	Keňa
<g/>
.	.	kIx.	.
</s>
<s>
Abo	Abo	k?	Abo
Obama	Obama	k?	Obama
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
Bernard	Bernard	k1gMnSc1	Bernard
Obama	Obama	k?	Obama
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
Mark	Mark	k1gMnSc1	Mark
Ndesandjo	Ndesandjo	k1gMnSc1	Ndesandjo
David	David	k1gMnSc1	David
Ndesandjo	Ndesandjo	k1gMnSc1	Ndesandjo
(	(	kIx(	(
<g/>
zemřel	zemřít	k5eAaPmAgInS	zemřít
<g/>
)	)	kIx)	)
Joseph	Joseph	k1gInSc1	Joseph
Ndesandjo	Ndesandjo	k1gMnSc1	Ndesandjo
Maya	Maya	k?	Maya
Soetoro-Ng	Soetoro-Ng	k1gMnSc1	Soetoro-Ng
-	-	kIx~	-
sestra	sestra	k1gFnSc1	sestra
George	Georg	k1gFnSc2	Georg
Hussein	Hussein	k1gInSc1	Hussein
Obama	Obama	k?	Obama
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2016	[number]	k4	2016
zpravodajský	zpravodajský	k2eAgInSc4d1	zpravodajský
server	server	k1gInSc4	server
CNN	CNN	kA	CNN
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
prezidentského	prezidentský	k2eAgInSc2d1	prezidentský
úřadu	úřad	k1gInSc2	úřad
<g/>
,	,	kIx,	,
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
vyjádřil	vyjádřit	k5eAaPmAgInS	vyjádřit
Obama	Obama	k?	Obama
záměr	záměr	k1gInSc4	záměr
si	se	k3xPyFc3	se
pronajmout	pronajmout	k5eAaPmF	pronajmout
dům	dům	k1gInSc4	dům
ve	v	k7c6	v
washingtonské	washingtonský	k2eAgFnSc6d1	Washingtonská
rezidenční	rezidenční	k2eAgFnSc6d1	rezidenční
čtvrti	čtvrt	k1gFnSc6	čtvrt
Kalorama	Kaloram	k1gMnSc2	Kaloram
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
zakoupil	zakoupit	k5eAaPmAgMnS	zakoupit
bývalý	bývalý	k2eAgMnSc1d1	bývalý
Clintonův	Clintonův	k2eAgMnSc1d1	Clintonův
tiskový	tiskový	k2eAgMnSc1d1	tiskový
mluvčí	mluvčí	k1gMnSc1	mluvčí
Joe	Joe	k1gFnPc2	Joe
Lockhart	Lockhart	k1gInSc4	Lockhart
za	za	k7c7	za
více	hodně	k6eAd2	hodně
než	než	k8xS	než
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
postavený	postavený	k2eAgInSc1d1	postavený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
disponoval	disponovat	k5eAaBmAgInS	disponovat
devíti	devět	k4xCc7	devět
ložnicemi	ložnice	k1gFnPc7	ložnice
a	a	k8xC	a
osmi	osm	k4xCc7	osm
koupelnami	koupelna	k1gFnPc7	koupelna
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
krokem	krok	k1gInSc7	krok
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
odlišil	odlišit	k5eAaPmAgInS	odlišit
od	od	k7c2	od
většiny	většina	k1gFnSc2	většina
předchozích	předchozí	k2eAgMnPc2d1	předchozí
prezidentů	prezident	k1gMnPc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
důvod	důvod	k1gInSc1	důvod
setrvání	setrvání	k1gNnSc2	setrvání
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
metropoli	metropol	k1gFnSc6	metropol
několik	několik	k4yIc4	několik
dalších	další	k2eAgNnPc2d1	další
let	léto	k1gNnPc2	léto
uvedl	uvést	k5eAaPmAgMnS	uvést
mladší	mladý	k2eAgFnSc4d2	mladší
dceru	dcera	k1gFnSc4	dcera
Sashu	Sasha	k1gMnSc4	Sasha
Obamovou	Obamová	k1gFnSc4	Obamová
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
dokončení	dokončení	k1gNnSc4	dokončení
střední	střední	k2eAgFnSc2d1	střední
školy	škola	k1gFnSc2	škola
bez	bez	k7c2	bez
nutnosti	nutnost	k1gFnSc2	nutnost
změny	změna	k1gFnSc2	změna
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
byl	být	k5eAaImAgInS	být
Barack	Barack	k1gInSc1	Barack
Obama	Obama	k?	Obama
poprvé	poprvé	k6eAd1	poprvé
zvolen	zvolit	k5eAaPmNgInS	zvolit
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
státu	stát	k1gInSc2	stát
Illinois	Illinois	k1gFnSc2	Illinois
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
sloužil	sloužit	k5eAaImAgMnS	sloužit
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
neúspěšně	úspěšně	k6eNd1	úspěšně
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
do	do	k7c2	do
Sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
pak	pak	k6eAd1	pak
oznámil	oznámit	k5eAaPmAgMnS	oznámit
svoji	svůj	k3xOyFgFnSc4	svůj
kandidaturu	kandidatura	k1gFnSc4	kandidatura
na	na	k7c4	na
post	post	k1gInSc4	post
senátora	senátor	k1gMnSc2	senátor
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
začal	začít	k5eAaPmAgMnS	začít
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
pracovat	pracovat	k5eAaImF	pracovat
skvělý	skvělý	k2eAgMnSc1d1	skvělý
manažer	manažer	k1gMnSc1	manažer
předvolebních	předvolební	k2eAgFnPc2d1	předvolební
kampaní	kampaň	k1gFnPc2	kampaň
David	David	k1gMnSc1	David
Axelrod	Axelrod	k1gInSc1	Axelrod
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
demokratických	demokratický	k2eAgFnPc6d1	demokratická
primárkách	primárky	k1gFnPc6	primárky
pro	pro	k7c4	pro
tyto	tento	k3xDgFnPc4	tento
volby	volba	k1gFnPc4	volba
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
Obama	Obama	k?	Obama
s	s	k7c7	s
53	[number]	k4	53
%	%	kIx~	%
všech	všecek	k3xTgInPc2	všecek
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
tehdy	tehdy	k6eAd1	tehdy
používal	používat	k5eAaImAgMnS	používat
volební	volební	k2eAgNnSc4d1	volební
heslo	heslo	k1gNnSc4	heslo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Yes	Yes	k1gMnSc1	Yes
<g/>
,	,	kIx,	,
we	we	k?	we
can	can	k?	can
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Ano	ano	k9	ano
<g/>
,	,	kIx,	,
my	my	k3xPp1nPc1	my
můžeme	moct	k5eAaImIp1nP	moct
<g/>
"	"	kIx"	"
-	-	kIx~	-
jako	jako	k9	jako
odpověď	odpověď	k1gFnSc4	odpověď
na	na	k7c4	na
snahu	snaha	k1gFnSc4	snaha
o	o	k7c4	o
změnu	změna	k1gFnSc4	změna
ve	v	k7c6	v
společenských	společenský	k2eAgInPc6d1	společenský
poměrech	poměr	k1gInPc6	poměr
v	v	k7c6	v
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
přednesl	přednést	k5eAaPmAgMnS	přednést
na	na	k7c6	na
konventu	konvent	k1gInSc6	konvent
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
hlavní	hlavní	k2eAgInSc1d1	hlavní
projev	projev	k1gInSc1	projev
sjezdu	sjezd	k1gInSc2	sjezd
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterému	který	k3yIgMnSc3	který
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
velký	velký	k2eAgInSc4d1	velký
nárůst	nárůst	k1gInSc4	nárůst
popularity	popularita	k1gFnSc2	popularita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
do	do	k7c2	do
Senátu	senát	k1gInSc2	senát
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Obdržel	obdržet	k5eAaPmAgMnS	obdržet
více	hodně	k6eAd2	hodně
než	než	k8xS	než
70	[number]	k4	70
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
oproti	oproti	k7c3	oproti
29	[number]	k4	29
%	%	kIx~	%
republikánského	republikánský	k2eAgMnSc4d1	republikánský
protikandidáta	protikandidát	k1gMnSc4	protikandidát
Afroameričana	Afroameričan	k1gMnSc2	Afroameričan
Alana	Alan	k1gMnSc2	Alan
Keyese	Keyese	k1gFnSc2	Keyese
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
teprve	teprve	k6eAd1	teprve
pátým	pátý	k4xOgMnSc7	pátý
Afroameričanem	Afroameričan	k1gMnSc7	Afroameričan
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
americkým	americký	k2eAgMnSc7d1	americký
senátorem	senátor	k1gMnSc7	senátor
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
svém	svůj	k3xOyFgNnSc6	svůj
působení	působení	k1gNnSc6	působení
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
USA	USA	kA	USA
se	se	k3xPyFc4	se
Obama	Obama	k?	Obama
zaměřoval	zaměřovat	k5eAaImAgMnS	zaměřovat
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
záležitosti	záležitost	k1gFnPc4	záležitost
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
státu	stát	k1gInSc3	stát
Illinois	Illinois	k1gFnPc2	Illinois
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
členem	člen	k1gInSc7	člen
senátního	senátní	k2eAgInSc2d1	senátní
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
mezinárodní	mezinárodní	k2eAgInPc4d1	mezinárodní
vztahy	vztah	k1gInPc4	vztah
a	a	k8xC	a
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Rusko	Rusko	k1gNnSc4	Rusko
<g/>
,	,	kIx,	,
východní	východní	k2eAgFnSc4d1	východní
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
Izrael	Izrael	k1gInSc1	Izrael
a	a	k8xC	a
Irák	Irák	k1gInSc1	Irák
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Afriku	Afrika	k1gFnSc4	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Keni	Keňa	k1gFnSc6	Keňa
<g/>
,	,	kIx,	,
zemi	zem	k1gFnSc6	zem
původu	původ	k1gInSc2	původ
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgMnS	nechat
vyšetřit	vyšetřit	k5eAaPmF	vyšetřit
na	na	k7c6	na
HIV	HIV	kA	HIV
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tak	tak	k6eAd1	tak
propagoval	propagovat	k5eAaImAgMnS	propagovat
prevenci	prevence	k1gFnSc4	prevence
a	a	k8xC	a
vyšetření	vyšetření	k1gNnSc4	vyšetření
této	tento	k3xDgFnSc2	tento
choroby	choroba	k1gFnSc2	choroba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čadu	Čad	k1gInSc6	Čad
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
uprchlíky	uprchlík	k1gMnPc7	uprchlík
z	z	k7c2	z
Dárfúru	Dárfúr	k1gInSc2	Dárfúr
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
mu	on	k3xPp3gMnSc3	on
Súdánem	Súdán	k1gInSc7	Súdán
nebylo	být	k5eNaImAgNnS	být
uděleno	udělit	k5eAaPmNgNnS	udělit
vízum	vízum	k1gNnSc1	vízum
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
kritizoval	kritizovat	k5eAaImAgMnS	kritizovat
tamější	tamější	k2eAgFnSc4d1	tamější
situaci	situace	k1gFnSc4	situace
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
senátor	senátor	k1gMnSc1	senátor
USA	USA	kA	USA
podpořil	podpořit	k5eAaPmAgInS	podpořit
návrhy	návrh	k1gInPc4	návrh
zákonů	zákon	k1gInPc2	zákon
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
zaměřeny	zaměřit	k5eAaPmNgInP	zaměřit
na	na	k7c4	na
větší	veliký	k2eAgFnSc4d2	veliký
kontrolu	kontrola	k1gFnSc4	kontrola
zbraní	zbraň	k1gFnPc2	zbraň
občanů	občan	k1gMnPc2	občan
a	a	k8xC	a
větší	veliký	k2eAgFnSc4d2	veliký
kontrolu	kontrola	k1gFnSc4	kontrola
využívání	využívání	k1gNnSc2	využívání
federálních	federální	k2eAgInPc2d1	federální
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
.	.	kIx.	.
</s>
<s>
Vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
se	se	k3xPyFc4	se
také	také	k9	také
pro	pro	k7c4	pro
návrhy	návrh	k1gInPc4	návrh
zákonů	zákon	k1gInPc2	zákon
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
prosazovaly	prosazovat	k5eAaImAgFnP	prosazovat
zpřísnění	zpřísnění	k1gNnSc4	zpřísnění
kontroly	kontrola	k1gFnSc2	kontrola
lobbistů	lobbista	k1gMnPc2	lobbista
<g/>
,	,	kIx,	,
volebních	volební	k2eAgMnPc2d1	volební
podvodů	podvod	k1gInPc2	podvod
<g/>
,	,	kIx,	,
změn	změna	k1gFnPc2	změna
klimatu	klima	k1gNnSc2	klima
<g/>
,	,	kIx,	,
nukleárního	nukleární	k2eAgInSc2d1	nukleární
terorismu	terorismus	k1gInSc2	terorismus
a	a	k8xC	a
péče	péče	k1gFnSc2	péče
o	o	k7c4	o
válečné	válečný	k2eAgMnPc4d1	válečný
veterány	veterán	k1gMnPc4	veterán
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2007	[number]	k4	2007
ve	v	k7c6	v
Springfieldu	Springfield	k1gInSc6	Springfield
v	v	k7c6	v
Illinois	Illinois	k1gFnSc6	Illinois
Barack	Barack	k1gMnSc1	Barack
Obama	Obama	k?	Obama
oficiálně	oficiálně	k6eAd1	oficiálně
oznámil	oznámit	k5eAaPmAgMnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
ucházet	ucházet	k5eAaImF	ucházet
o	o	k7c4	o
nominaci	nominace	k1gFnSc4	nominace
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
do	do	k7c2	do
prezidentských	prezidentský	k2eAgFnPc2d1	prezidentská
voleb	volba	k1gFnPc2	volba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
strategie	strategie	k1gFnSc1	strategie
byla	být	k5eAaImAgFnS	být
založená	založený	k2eAgFnSc1d1	založená
na	na	k7c4	na
využití	využití	k1gNnSc4	využití
internetu	internet	k1gInSc2	internet
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
který	který	k3yRgInSc4	který
dostal	dostat	k5eAaPmAgMnS	dostat
drobné	drobný	k2eAgInPc4d1	drobný
finanční	finanční	k2eAgInPc4d1	finanční
dary	dar	k1gInPc4	dar
od	od	k7c2	od
milionů	milion	k4xCgInPc2	milion
občanů	občan	k1gMnPc2	občan
<g/>
.	.	kIx.	.
</s>
<s>
Nominačního	nominační	k2eAgNnSc2d1	nominační
klání	klání	k1gNnSc2	klání
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
kandidáty	kandidát	k1gMnPc7	kandidát
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
největší	veliký	k2eAgFnSc4d3	veliký
podporu	podpora	k1gFnSc4	podpora
získala	získat	k5eAaPmAgFnS	získat
Hillary	Hillara	k1gFnSc2	Hillara
Clintonová	Clintonová	k1gFnSc1	Clintonová
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
posledních	poslední	k2eAgFnPc6d1	poslední
primárkách	primárky	k1gFnPc6	primárky
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Dakotě	Dakota	k1gFnSc6	Dakota
a	a	k8xC	a
Montaně	Montana	k1gFnSc6	Montana
získal	získat	k5eAaPmAgInS	získat
Barack	Barack	k1gInSc1	Barack
Obama	Obama	k?	Obama
definitivně	definitivně	k6eAd1	definitivně
nominaci	nominace	k1gFnSc4	nominace
na	na	k7c4	na
prezidentskou	prezidentský	k2eAgFnSc4d1	prezidentská
kandidaturu	kandidatura	k1gFnSc4	kandidatura
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
překročil	překročit	k5eAaPmAgInS	překročit
hranici	hranice	k1gFnSc4	hranice
potřebných	potřebný	k2eAgInPc2d1	potřebný
2118	[number]	k4	2118
delegátů	delegát	k1gMnPc2	delegát
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
oficiálně	oficiálně	k6eAd1	oficiálně
přidělena	přidělit	k5eAaPmNgFnS	přidělit
na	na	k7c6	na
srpnovém	srpnový	k2eAgInSc6d1	srpnový
sjezdu	sjezd	k1gInSc6	sjezd
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
(	(	kIx(	(
<g/>
25	[number]	k4	25
<g/>
.	.	kIx.	.
-	-	kIx~	-
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
v	v	k7c6	v
Pepsi	Pepsi	k1gFnSc6	Pepsi
Centru	centr	k1gInSc2	centr
v	v	k7c6	v
Denveru	Denver	k1gInSc6	Denver
(	(	kIx(	(
<g/>
stát	stát	k1gInSc1	stát
Colorado	Colorado	k1gNnSc1	Colorado
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
vůbec	vůbec	k9	vůbec
prvním	první	k4xOgInSc7	první
oficiálním	oficiální	k2eAgInSc7d1	oficiální
kandidátem	kandidát	k1gMnSc7	kandidát
jedné	jeden	k4xCgFnSc2	jeden
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
největších	veliký	k2eAgFnPc2d3	veliký
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
neměl	mít	k5eNaImAgInS	mít
bílou	bílý	k2eAgFnSc4d1	bílá
barvu	barva	k1gFnSc4	barva
pleti	pleť	k1gFnSc2	pleť
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2008	[number]	k4	2008
oznámil	oznámit	k5eAaPmAgMnS	oznámit
Obama	Obama	k?	Obama
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
kandidáta	kandidát	k1gMnSc4	kandidát
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
USA	USA	kA	USA
za	za	k7c4	za
Demokraty	demokrat	k1gMnPc4	demokrat
si	se	k3xPyFc3	se
vybral	vybrat	k5eAaPmAgMnS	vybrat
delawareského	delawareský	k2eAgMnSc4d1	delawareský
senátora	senátor	k1gMnSc4	senátor
Joe	Joe	k1gMnSc4	Joe
Bidena	Biden	k1gMnSc4	Biden
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
mj.	mj.	kA	mj.
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
zastával	zastávat	k5eAaImAgMnS	zastávat
post	post	k1gInSc4	post
předsedy	předseda	k1gMnSc2	předseda
Výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
zahraniční	zahraniční	k2eAgFnPc4d1	zahraniční
záležitosti	záležitost	k1gFnPc4	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
volební	volební	k2eAgFnSc2d1	volební
kampaně	kampaň	k1gFnSc2	kampaň
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
zatkly	zatknout	k5eAaPmAgInP	zatknout
americké	americký	k2eAgInPc1d1	americký
úřady	úřad	k1gInPc1	úřad
v	v	k7c6	v
Denveru	Denver	k1gInSc6	Denver
čtyři	čtyři	k4xCgFnPc1	čtyři
osoby	osoba	k1gFnPc1	osoba
podezřelé	podezřelý	k2eAgFnPc1d1	podezřelá
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
chtěly	chtít	k5eAaImAgFnP	chtít
na	na	k7c4	na
Obamu	Obama	k1gFnSc4	Obama
spáchat	spáchat	k5eAaPmF	spáchat
atentát	atentát	k1gInSc4	atentát
<g/>
.	.	kIx.	.
</s>
<s>
Oznámila	oznámit	k5eAaPmAgFnS	oznámit
to	ten	k3xDgNnSc1	ten
25	[number]	k4	25
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2008	[number]	k4	2008
televizní	televizní	k2eAgFnSc2d1	televizní
stanice	stanice	k1gFnSc2	stanice
CBS	CBS	kA	CBS
a	a	k8xC	a
mluvčí	mluvčí	k1gMnSc1	mluvčí
Federálního	federální	k2eAgInSc2d1	federální
úřadu	úřad	k1gInSc2	úřad
pro	pro	k7c4	pro
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
(	(	kIx(	(
<g/>
FBI	FBI	kA	FBI
<g/>
)	)	kIx)	)
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
případ	případ	k1gInSc1	případ
vyšetřován	vyšetřován	k2eAgInSc1d1	vyšetřován
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
televize	televize	k1gFnSc2	televize
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
zatčených	zatčený	k1gMnPc2	zatčený
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
chtěl	chtít	k5eAaImAgMnS	chtít
střílet	střílet	k5eAaImF	střílet
na	na	k7c4	na
Obamu	Obama	k1gFnSc4	Obama
z	z	k7c2	z
velmi	velmi	k6eAd1	velmi
výhodné	výhodný	k2eAgFnSc2d1	výhodná
pozice	pozice	k1gFnSc2	pozice
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
puškou	puška	k1gFnSc7	puška
na	na	k7c4	na
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
750	[number]	k4	750
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Atentát	atentát	k1gInSc1	atentát
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
spáchán	spáchat	k5eAaPmNgInS	spáchat
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2008	[number]	k4	2008
během	během	k7c2	během
Obamova	Obamův	k2eAgInSc2d1	Obamův
projevu	projev	k1gInSc2	projev
na	na	k7c6	na
sjezdu	sjezd	k1gInSc6	sjezd
Demokratické	demokratický	k2eAgFnSc2d1	demokratická
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
jej	on	k3xPp3gMnSc4	on
měl	mít	k5eAaImAgInS	mít
oficiálně	oficiálně	k6eAd1	oficiálně
nominovat	nominovat	k5eAaBmF	nominovat
jako	jako	k9	jako
kandidáta	kandidát	k1gMnSc4	kandidát
na	na	k7c4	na
úřad	úřad	k1gInSc4	úřad
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
října	říjen	k1gInSc2	říjen
do	do	k7c2	do
volebního	volební	k2eAgInSc2d1	volební
dne	den	k1gInSc2	den
byly	být	k5eAaImAgFnP	být
naplánovány	naplánovat	k5eAaBmNgFnP	naplánovat
tři	tři	k4xCgFnPc1	tři
televizní	televizní	k2eAgFnPc1d1	televizní
<g/>
,	,	kIx,	,
celostátně	celostátně	k6eAd1	celostátně
vysílané	vysílaný	k2eAgFnPc4d1	vysílaná
debaty	debata	k1gFnPc4	debata
mezi	mezi	k7c7	mezi
prezidentskými	prezidentský	k2eAgMnPc7d1	prezidentský
kandidáty	kandidát	k1gMnPc7	kandidát
<g/>
,	,	kIx,	,
Obamou	Obama	k1gMnSc7	Obama
a	a	k8xC	a
McCainem	McCain	k1gMnSc7	McCain
<g/>
,	,	kIx,	,
trvající	trvající	k2eAgFnSc7d1	trvající
zhruba	zhruba	k6eAd1	zhruba
hodinu	hodina	k1gFnSc4	hodina
a	a	k8xC	a
půl	půl	k1xP	půl
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k9	již
první	první	k4xOgFnSc4	první
debatu	debata	k1gFnSc4	debata
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
vyhrál	vyhrát	k5eAaPmAgInS	vyhrát
podle	podle	k7c2	podle
bezprostředních	bezprostřední	k2eAgFnPc2d1	bezprostřední
anket	anketa	k1gFnPc2	anketa
mezi	mezi	k7c7	mezi
diváky	divák	k1gMnPc7	divák
stanic	stanice	k1gFnPc2	stanice
CNN	CNN	kA	CNN
a	a	k8xC	a
CBS	CBS	kA	CBS
Barack	Baracko	k1gNnPc2	Baracko
Obama	Obama	k?	Obama
<g/>
.	.	kIx.	.
</s>
<s>
Duel	duel	k1gInSc1	duel
se	se	k3xPyFc4	se
konal	konat	k5eAaImAgInS	konat
za	za	k7c2	za
situace	situace	k1gFnSc2	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
kandidát	kandidát	k1gMnSc1	kandidát
Republikánské	republikánský	k2eAgFnSc2d1	republikánská
strany	strana	k1gFnSc2	strana
John	John	k1gMnSc1	John
McCain	McCain	k1gMnSc1	McCain
naléhavě	naléhavě	k6eAd1	naléhavě
potřeboval	potřebovat	k5eAaImAgMnS	potřebovat
zvrátit	zvrátit	k5eAaPmF	zvrátit
rostoucí	rostoucí	k2eAgFnSc4d1	rostoucí
podporu	podpora	k1gFnSc4	podpora
veřejnosti	veřejnost	k1gFnSc2	veřejnost
pro	pro	k7c4	pro
Obamu	Obama	k1gFnSc4	Obama
jak	jak	k8xS	jak
na	na	k7c6	na
celostátní	celostátní	k2eAgFnSc6d1	celostátní
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
klíčových	klíčový	k2eAgInPc6d1	klíčový
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
souboj	souboj	k1gInSc4	souboj
nejtěsnější	těsný	k2eAgMnSc1d3	nejtěsnější
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
pro	pro	k7c4	pro
CNN	CNN	kA	CNN
považovalo	považovat	k5eAaImAgNnS	považovat
54	[number]	k4	54
%	%	kIx~	%
diváků	divák	k1gMnPc2	divák
za	za	k7c2	za
vítěze	vítěz	k1gMnSc2	vítěz
televizního	televizní	k2eAgNnSc2d1	televizní
klání	klání	k1gNnSc2	klání
Obamu	Obam	k1gInSc2	Obam
<g/>
,	,	kIx,	,
McCaina	McCain	k2eAgMnSc2d1	McCain
jen	jen	k9	jen
30	[number]	k4	30
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důležité	důležitý	k2eAgFnSc6d1	důležitá
otázce	otázka	k1gFnSc6	otázka
ekonomické	ekonomický	k2eAgFnSc2d1	ekonomická
situace	situace	k1gFnSc2	situace
měl	mít	k5eAaImAgInS	mít
Obama	Obama	k?	Obama
rovněž	rovněž	k9	rovněž
zhruba	zhruba	k6eAd1	zhruba
dvacetiprocentní	dvacetiprocentní	k2eAgInSc4d1	dvacetiprocentní
náskok	náskok	k1gInSc4	náskok
před	před	k7c7	před
svým	svůj	k3xOyFgMnSc7	svůj
soupeřem	soupeř	k1gMnSc7	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2008	[number]	k4	2008
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
Barack	Barack	k1gMnSc1	Barack
Obama	Obama	k?	Obama
v	v	k7c6	v
prezidentských	prezidentský	k2eAgFnPc6d1	prezidentská
volbách	volba	k1gFnPc6	volba
<g/>
,	,	kIx,	,
když	když	k8xS	když
porazil	porazit	k5eAaPmAgMnS	porazit
republikánského	republikánský	k2eAgMnSc4d1	republikánský
kandidáta	kandidát	k1gMnSc4	kandidát
Johna	John	k1gMnSc4	John
McCaina	McCain	k1gMnSc4	McCain
<g/>
.	.	kIx.	.
</s>
<s>
Historicky	historicky	k6eAd1	historicky
je	být	k5eAaImIp3nS	být
vůbec	vůbec	k9	vůbec
prvním	první	k4xOgMnSc7	první
afroamerickým	afroamerický	k2eAgMnSc7d1	afroamerický
prezidentem	prezident	k1gMnSc7	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2009	[number]	k4	2009
složil	složit	k5eAaPmAgMnS	složit
za	za	k7c2	za
přítomnosti	přítomnost	k1gFnSc2	přítomnost
téměř	téměř	k6eAd1	téměř
dvou	dva	k4xCgInPc2	dva
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
,	,	kIx,	,
D.	D.	kA	D.
C.	C.	kA	C.
slavnostní	slavnostní	k2eAgFnSc4d1	slavnostní
přísahu	přísaha	k1gFnSc4	přísaha
a	a	k8xC	a
ujal	ujmout	k5eAaPmAgInS	ujmout
se	se	k3xPyFc4	se
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Obama	Obama	k?	Obama
zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
především	především	k6eAd1	především
díky	díky	k7c3	díky
podpoře	podpora	k1gFnSc3	podpora
mladších	mladý	k2eAgMnPc2d2	mladší
<g/>
,	,	kIx,	,
vzdělanějších	vzdělaný	k2eAgMnPc2d2	vzdělanější
voličů	volič	k1gMnPc2	volič
<g/>
,	,	kIx,	,
afroameričanů	afroameričan	k1gMnPc2	afroameričan
a	a	k8xC	a
tzv.	tzv.	kA	tzv.
latinos	latinos	k1gInSc1	latinos
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
občanů	občan	k1gMnPc2	občan
pocházejících	pocházející	k2eAgMnPc2d1	pocházející
z	z	k7c2	z
některé	některý	k3yIgFnSc2	některý
latinskoamerické	latinskoamerický	k2eAgFnSc2d1	latinskoamerická
země	zem	k1gFnSc2	zem
(	(	kIx(	(
<g/>
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
,	,	kIx,	,
Kuba	Kuba	k1gFnSc1	Kuba
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
průzkumů	průzkum	k1gInPc2	průzkum
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
90	[number]	k4	90
%	%	kIx~	%
afroameričanů	afroameričan	k1gMnPc2	afroameričan
pro	pro	k7c4	pro
Obamu	Obama	k1gFnSc4	Obama
<g/>
.	.	kIx.	.
</s>
<s>
Menší	malý	k2eAgFnSc4d2	menší
podporu	podpora	k1gFnSc4	podpora
měl	mít	k5eAaImAgInS	mít
u	u	k7c2	u
starších	starý	k2eAgFnPc2d2	starší
a	a	k8xC	a
manuálně	manuálně	k6eAd1	manuálně
pracujících	pracující	k2eAgMnPc2d1	pracující
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yQgInPc2	který
v	v	k7c6	v
předvolebních	předvolební	k2eAgInPc6d1	předvolební
průzkumech	průzkum	k1gInPc6	průzkum
vedl	vést	k5eAaImAgMnS	vést
kandidát	kandidát	k1gMnSc1	kandidát
Republikánské	republikánský	k2eAgFnSc2d1	republikánská
strany	strana	k1gFnSc2	strana
McCain	McCain	k1gInSc4	McCain
o	o	k7c4	o
několik	několik	k4yIc4	několik
procentních	procentní	k2eAgInPc2d1	procentní
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Obama	Obama	k?	Obama
měl	mít	k5eAaImAgInS	mít
větší	veliký	k2eAgFnPc4d2	veliký
preference	preference	k1gFnPc4	preference
u	u	k7c2	u
žen	žena	k1gFnPc2	žena
než	než	k8xS	než
u	u	k7c2	u
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
po	po	k7c6	po
vítězství	vítězství	k1gNnSc6	vítězství
začal	začít	k5eAaPmAgInS	začít
Obama	Obama	k?	Obama
sestavovat	sestavovat	k5eAaImF	sestavovat
svou	svůj	k3xOyFgFnSc4	svůj
budoucí	budoucí	k2eAgFnSc4d1	budoucí
vládu	vláda	k1gFnSc4	vláda
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
infobox	infobox	k1gInSc1	infobox
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
první	první	k4xOgFnSc6	první
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
počátkem	počátkem	k7c2	počátkem
dubna	duben	k1gInSc2	duben
2009	[number]	k4	2009
se	se	k3xPyFc4	se
nejprve	nejprve	k6eAd1	nejprve
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
summitu	summit	k1gInSc2	summit
skupiny	skupina	k1gFnPc4	skupina
G20	G20	k1gFnPc2	G20
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
summitu	summit	k1gInSc2	summit
NATO	NATO	kA	NATO
ve	v	k7c6	v
Štrasburku	Štrasburk	k1gInSc6	Štrasburk
a	a	k8xC	a
4	[number]	k4	4
<g/>
.	.	kIx.	.
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
summitu	summit	k1gInSc2	summit
USA	USA	kA	USA
a	a	k8xC	a
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
na	na	k7c6	na
Hradčanském	hradčanský	k2eAgNnSc6d1	Hradčanské
náměstí	náměstí	k1gNnSc6	náměstí
přednesl	přednést	k5eAaPmAgInS	přednést
projev	projev	k1gInSc1	projev
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
týkal	týkat	k5eAaImAgInS	týkat
převážně	převážně	k6eAd1	převážně
problematiky	problematika	k1gFnSc2	problematika
jaderného	jaderný	k2eAgNnSc2d1	jaderné
odzbrojování	odzbrojování	k1gNnSc2	odzbrojování
<g/>
.	.	kIx.	.
</s>
<s>
Projev	projev	k1gInSc4	projev
vyslechlo	vyslechnout	k5eAaPmAgNnS	vyslechnout
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
asi	asi	k9	asi
30	[number]	k4	30
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
sledovaly	sledovat	k5eAaImAgFnP	sledovat
jej	on	k3xPp3gMnSc4	on
milióny	milión	k4xCgInPc1	milión
rozhlasových	rozhlasový	k2eAgMnPc2d1	rozhlasový
a	a	k8xC	a
televizních	televizní	k2eAgMnPc2d1	televizní
posluchačů	posluchač	k1gMnPc2	posluchač
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Obama	Obama	k?	Obama
vyjádřil	vyjádřit	k5eAaPmAgMnS	vyjádřit
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
projevu	projev	k1gInSc6	projev
též	též	k9	též
obdiv	obdiv	k1gInSc4	obdiv
k	k	k7c3	k
demokratickým	demokratický	k2eAgFnPc3d1	demokratická
tradicím	tradice	k1gFnPc3	tradice
českého	český	k2eAgInSc2d1	český
národa	národ	k1gInSc2	národ
<g/>
.	.	kIx.	.
</s>
<s>
Obama	Obama	k?	Obama
začínal	začínat	k5eAaImAgInS	začínat
své	svůj	k3xOyFgNnSc4	svůj
funkční	funkční	k2eAgNnSc4d1	funkční
období	období	k1gNnSc4	období
jako	jako	k8xC	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgMnPc2d3	nejpopulárnější
prezidentů	prezident	k1gMnPc2	prezident
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
průzkumů	průzkum	k1gInPc2	průzkum
jej	on	k3xPp3gMnSc4	on
v	v	k7c6	v
době	doba	k1gFnSc6	doba
nástupu	nástup	k1gInSc2	nástup
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
podporovalo	podporovat	k5eAaImAgNnS	podporovat
78	[number]	k4	78
%	%	kIx~	%
Američanů	Američan	k1gMnPc2	Američan
<g/>
.	.	kIx.	.
</s>
<s>
Míra	Míra	k1gFnSc1	Míra
podpory	podpora	k1gFnSc2	podpora
však	však	k9	však
brzy	brzy	k6eAd1	brzy
začala	začít	k5eAaPmAgFnS	začít
klesat	klesat	k5eAaImF	klesat
a	a	k8xC	a
ve	v	k7c6	v
třetím	třetí	k4xOgNnSc6	třetí
čtvrtletí	čtvrtletí	k1gNnSc6	čtvrtletí
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
čítala	čítat	k5eAaImAgFnS	čítat
již	již	k9	již
jen	jen	k9	jen
53	[number]	k4	53
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Obama	Obama	k?	Obama
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prezidentem	prezident	k1gMnSc7	prezident
v	v	k7c6	v
době	doba	k1gFnSc6	doba
počátku	počátek	k1gInSc2	počátek
finanční	finanční	k2eAgFnSc2d1	finanční
krize	krize	k1gFnSc2	krize
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
reakci	reakce	k1gFnSc4	reakce
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
začal	začít	k5eAaPmAgMnS	začít
vydávat	vydávat	k5eAaImF	vydávat
balíčky	balíček	k1gInPc4	balíček
na	na	k7c4	na
záchranu	záchrana	k1gFnSc4	záchrana
ekonomiky	ekonomika	k1gFnSc2	ekonomika
včetně	včetně	k7c2	včetně
nákupu	nákup	k1gInSc2	nákup
dluhopisů	dluhopis	k1gInPc2	dluhopis
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
se	se	k3xPyFc4	se
ekonomika	ekonomika	k1gFnSc1	ekonomika
USA	USA	kA	USA
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
recese	recese	k1gFnSc2	recese
podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
Obamovi	Obamův	k2eAgMnPc1d1	Obamův
rychle	rychle	k6eAd1	rychle
vrátit	vrátit	k5eAaPmF	vrátit
růst	růst	k1gInSc4	růst
HDP	HDP	kA	HDP
a	a	k8xC	a
klesání	klesání	k1gNnSc2	klesání
nezaměstnanosti	nezaměstnanost	k1gFnSc2	nezaměstnanost
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
prvního	první	k4xOgNnSc2	první
období	období	k1gNnSc2	období
zvýšil	zvýšit	k5eAaPmAgInS	zvýšit
zadlužení	zadlužení	k1gNnSc4	zadlužení
země	zem	k1gFnSc2	zem
o	o	k7c4	o
20	[number]	k4	20
<g/>
%	%	kIx~	%
HDP	HDP	kA	HDP
<g/>
,	,	kIx,	,
ke	k	k7c3	k
konci	konec	k1gInSc6	konec
prvního	první	k4xOgNnSc2	první
období	období	k1gNnSc2	období
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
ovšem	ovšem	k9	ovšem
podařilo	podařit	k5eAaPmAgNnS	podařit
deficit	deficit	k1gInSc4	deficit
snížit	snížit	k5eAaPmF	snížit
<g/>
.	.	kIx.	.
</s>
<s>
Inflace	inflace	k1gFnSc1	inflace
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
krize	krize	k1gFnSc2	krize
velmi	velmi	k6eAd1	velmi
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
,	,	kIx,	,
vzápětí	vzápětí	k6eAd1	vzápětí
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
zvyšovat	zvyšovat	k5eAaImF	zvyšovat
a	a	k8xC	a
Obamovi	Obamův	k2eAgMnPc1d1	Obamův
se	se	k3xPyFc4	se
ji	on	k3xPp3gFnSc4	on
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
období	období	k1gNnSc6	období
nepodařilo	podařit	k5eNaPmAgNnS	podařit
snížit	snížit	k5eAaPmF	snížit
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Patient	Patient	k1gInSc1	Patient
Protection	Protection	k1gInSc1	Protection
and	and	k?	and
Affordable	Affordable	k1gMnSc5	Affordable
Care	car	k1gMnSc5	car
Act	Act	k1gMnSc5	Act
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
Obamova	Obamův	k2eAgFnSc1d1	Obamova
obliba	obliba	k1gFnSc1	obliba
u	u	k7c2	u
občanů	občan	k1gMnPc2	občan
USA	USA	kA	USA
klesla	klesnout	k5eAaPmAgFnS	klesnout
–	–	k?	–
podle	podle	k7c2	podle
jednoho	jeden	k4xCgInSc2	jeden
průzkumu	průzkum	k1gInSc2	průzkum
veřejného	veřejný	k2eAgNnSc2d1	veřejné
mínění	mínění	k1gNnSc2	mínění
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
–	–	k?	–
dočasně	dočasně	k6eAd1	dočasně
pod	pod	k7c7	pod
50	[number]	k4	50
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
komentátorů	komentátor	k1gMnPc2	komentátor
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
byla	být	k5eAaImAgFnS	být
hlavní	hlavní	k2eAgFnSc7d1	hlavní
příčinou	příčina	k1gFnSc7	příčina
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
špatná	špatný	k2eAgFnSc1d1	špatná
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
situace	situace	k1gFnSc1	situace
po	po	k7c4	po
velké	velká	k1gFnPc4	velká
finanční	finanční	k2eAgFnSc4d1	finanční
krizi	krize	k1gFnSc4	krize
let	léto	k1gNnPc2	léto
2008	[number]	k4	2008
a	a	k8xC	a
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Kritiky	kritika	k1gFnPc1	kritika
politických	politický	k2eAgMnPc2d1	politický
oponentů	oponent	k1gMnPc2	oponent
i	i	k8xC	i
mnohých	mnohý	k2eAgMnPc2d1	mnohý
řadových	řadový	k2eAgMnPc2d1	řadový
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
souhlasu	souhlas	k1gInSc2	souhlas
odborných	odborný	k2eAgInPc2d1	odborný
kruhů	kruh	k1gInPc2	kruh
a	a	k8xC	a
značné	značný	k2eAgFnSc2d1	značná
části	část	k1gFnSc2	část
veřejnosti	veřejnost	k1gFnSc2	veřejnost
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
finančním	finanční	k2eAgFnPc3d1	finanční
injekcím	injekce	k1gFnPc3	injekce
vlády	vláda	k1gFnSc2	vláda
USA	USA	kA	USA
a	a	k8xC	a
Federální	federální	k2eAgFnSc2d1	federální
rezervní	rezervní	k2eAgFnSc2d1	rezervní
banky	banka	k1gFnSc2	banka
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
oživení	oživení	k1gNnSc2	oživení
ekonomiky	ekonomika	k1gFnSc2	ekonomika
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
souhlasem	souhlas	k1gInSc7	souhlas
a	a	k8xC	a
kritikou	kritika	k1gFnSc7	kritika
se	se	k3xPyFc4	se
setkala	setkat	k5eAaPmAgFnS	setkat
také	také	k9	také
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
reforma	reforma	k1gFnSc1	reforma
(	(	kIx(	(
<g/>
The	The	k1gFnSc1	The
Patient	Patient	k1gMnSc1	Patient
Protection	Protection	k1gInSc1	Protection
and	and	k?	and
Affordable	Affordable	k1gMnSc5	Affordable
Care	car	k1gMnSc5	car
Act	Act	k1gMnSc5	Act
<g/>
,	,	kIx,	,
též	též	k9	též
populárně	populárně	k6eAd1	populárně
nazývána	nazývat	k5eAaImNgFnS	nazývat
jako	jako	k8xC	jako
Obamacare	Obamacar	k1gMnSc5	Obamacar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
narazila	narazit	k5eAaPmAgFnS	narazit
na	na	k7c4	na
odpor	odpor	k1gInSc4	odpor
republikánů	republikán	k1gMnPc2	republikán
i	i	k8xC	i
určité	určitý	k2eAgFnSc2d1	určitá
části	část	k1gFnSc2	část
demokratů	demokrat	k1gMnPc2	demokrat
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
ve	v	k7c6	v
Sněmovně	sněmovna	k1gFnSc6	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
a	a	k8xC	a
jejímuž	jejíž	k3xOyRp3gNnSc3	jejíž
prosazení	prosazení	k1gNnSc3	prosazení
věnoval	věnovat	k5eAaImAgMnS	věnovat
prezident	prezident	k1gMnSc1	prezident
Obama	Obama	k?	Obama
značné	značný	k2eAgNnSc1d1	značné
úsilí	úsilí	k1gNnSc1	úsilí
od	od	k7c2	od
podzimu	podzim	k1gInSc2	podzim
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
určitých	určitý	k2eAgInPc6d1	určitý
počátečních	počáteční	k2eAgInPc6d1	počáteční
úspěších	úspěch	k1gInPc6	úspěch
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
schválení	schválení	k1gNnSc1	schválení
sněmovní	sněmovní	k2eAgFnSc2d1	sněmovní
verze	verze	k1gFnSc2	verze
reformy	reforma	k1gFnSc2	reforma
ve	v	k7c6	v
Sněmovně	sněmovna	k1gFnSc6	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
a	a	k8xC	a
senátní	senátní	k2eAgFnPc4d1	senátní
verze	verze	k1gFnPc4	verze
reformy	reforma	k1gFnSc2	reforma
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc6	jenž
Obama	Obama	k?	Obama
upřednostňoval	upřednostňovat	k5eAaImAgInS	upřednostňovat
spíše	spíše	k9	spíše
tu	tu	k6eAd1	tu
druhou	druhý	k4xOgFnSc4	druhý
verzi	verze	k1gFnSc4	verze
<g/>
,	,	kIx,	,
však	však	k9	však
Obamovo	Obamův	k2eAgNnSc4d1	Obamovo
úsilí	úsilí	k1gNnSc4	úsilí
o	o	k7c4	o
rychlé	rychlý	k2eAgNnSc4d1	rychlé
schválení	schválení	k1gNnSc4	schválení
reformy	reforma	k1gFnSc2	reforma
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gFnPc2	jeho
představ	představa	k1gFnPc2	představa
narazilo	narazit	k5eAaPmAgNnS	narazit
na	na	k7c4	na
překážky	překážka	k1gFnPc4	překážka
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2010	[number]	k4	2010
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
doplňovacím	doplňovací	k2eAgFnPc3d1	doplňovací
senátním	senátní	k2eAgFnPc3d1	senátní
volbám	volba	k1gFnPc3	volba
v	v	k7c6	v
Massachusetts	Massachusetts	k1gNnSc6	Massachusetts
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
hladké	hladký	k2eAgNnSc1d1	hladké
vítězství	vítězství	k1gNnSc1	vítězství
demokratů	demokrat	k1gMnPc2	demokrat
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
dané	daný	k2eAgNnSc4d1	dané
křeslo	křeslo	k1gNnSc4	křeslo
drželi	držet	k5eAaImAgMnP	držet
57	[number]	k4	57
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
senátní	senátní	k2eAgFnPc1d1	senátní
volby	volba	k1gFnPc1	volba
v	v	k7c6	v
Massachusetts	Massachusetts	k1gNnSc6	Massachusetts
neprohráli	prohrát	k5eNaPmAgMnP	prohrát
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
a	a	k8xC	a
jejichž	jejichž	k3xOyRp3gFnSc1	jejichž
kandidátka	kandidátka	k1gFnSc1	kandidátka
měla	mít	k5eAaImAgFnS	mít
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
kampaně	kampaň	k1gFnSc2	kampaň
dle	dle	k7c2	dle
průzkumů	průzkum	k1gInPc2	průzkum
preferencí	preference	k1gFnPc2	preference
20	[number]	k4	20
%	%	kIx~	%
náskok	náskok	k1gInSc4	náskok
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
republikán	republikán	k1gMnSc1	republikán
Scott	Scott	k1gMnSc1	Scott
Brown	Brown	k1gMnSc1	Brown
postavil	postavit	k5eAaPmAgMnS	postavit
svoji	svůj	k3xOyFgFnSc4	svůj
kampaň	kampaň	k1gFnSc4	kampaň
především	především	k9	především
na	na	k7c4	na
odmítání	odmítání	k1gNnSc4	odmítání
Obamovy	Obamův	k2eAgFnSc2d1	Obamova
zdravotnické	zdravotnický	k2eAgFnSc2d1	zdravotnická
reformy	reforma	k1gFnSc2	reforma
a	a	k8xC	a
demokratickou	demokratický	k2eAgFnSc4d1	demokratická
kandidátku	kandidátka	k1gFnSc4	kandidátka
Marthu	Marth	k1gInSc2	Marth
Coakley	Coaklea	k1gFnSc2	Coaklea
nakonec	nakonec	k6eAd1	nakonec
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
tématem	téma	k1gNnSc7	téma
porazil	porazit	k5eAaPmAgMnS	porazit
o	o	k7c4	o
5	[number]	k4	5
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
demokratičtí	demokratický	k2eAgMnPc1d1	demokratický
představitelé	představitel	k1gMnPc1	představitel
nejprve	nejprve	k6eAd1	nejprve
tvrdili	tvrdit	k5eAaImAgMnP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
to	ten	k3xDgNnSc1	ten
nemění	měnit	k5eNaImIp3nS	měnit
nic	nic	k3yNnSc1	nic
na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
snaze	snaha	k1gFnSc6	snaha
prosadit	prosadit	k5eAaPmF	prosadit
reformu	reforma	k1gFnSc4	reforma
co	co	k9	co
nejdříve	dříve	k6eAd3	dříve
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
vítězstvím	vítězství	k1gNnSc7	vítězství
Browna	Brown	k1gInSc2	Brown
ztratili	ztratit	k5eAaPmAgMnP	ztratit
fakticky	fakticky	k6eAd1	fakticky
supervětšinu	supervětšina	k1gFnSc4	supervětšina
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
a	a	k8xC	a
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
i	i	k9	i
schopnost	schopnost	k1gFnSc4	schopnost
čelit	čelit	k5eAaImF	čelit
republikánskému	republikánský	k2eAgInSc3d1	republikánský
filibusteringu	filibustering	k1gInSc3	filibustering
<g/>
.	.	kIx.	.
</s>
<s>
Senát	senát	k1gInSc1	senát
tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
konečné	konečný	k2eAgNnSc4d1	konečné
schválení	schválení	k1gNnSc4	schválení
reformy	reforma	k1gFnSc2	reforma
dočasně	dočasně	k6eAd1	dočasně
neprůchodný	průchodný	k2eNgInSc1d1	neprůchodný
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
oznámila	oznámit	k5eAaPmAgFnS	oznámit
demokratická	demokratický	k2eAgFnSc1d1	demokratická
mluvčí	mluvčí	k1gFnSc1	mluvčí
Sněmovny	sněmovna	k1gFnSc2	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
Nancy	Nancy	k1gFnSc2	Nancy
Pelosi	Pelose	k1gFnSc4	Pelose
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
schválení	schválení	k1gNnSc4	schválení
senátní	senátní	k2eAgFnSc2d1	senátní
verze	verze	k1gFnSc2	verze
beze	beze	k7c2	beze
změn	změna	k1gFnPc2	změna
nebude	být	k5eNaImBp3nS	být
ve	v	k7c6	v
Sněmovně	sněmovna	k1gFnSc6	sněmovna
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
dostatek	dostatek	k1gInSc4	dostatek
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
21	[number]	k4	21
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2010	[number]	k4	2010
podařilo	podařit	k5eAaPmAgNnS	podařit
schválit	schválit	k5eAaPmF	schválit
ve	v	k7c6	v
Sněmovně	sněmovna	k1gFnSc6	sněmovna
reprezentantů	reprezentant	k1gMnPc2	reprezentant
kompromisní	kompromisní	k2eAgInSc4d1	kompromisní
návrh	návrh	k1gInSc4	návrh
zdravotnické	zdravotnický	k2eAgFnSc2d1	zdravotnická
reformy	reforma	k1gFnSc2	reforma
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
který	který	k3yQgInSc4	který
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
219	[number]	k4	219
demokratů	demokrat	k1gMnPc2	demokrat
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
návrhu	návrh	k1gInSc3	návrh
hlasovalo	hlasovat	k5eAaImAgNnS	hlasovat
34	[number]	k4	34
demokratů	demokrat	k1gMnPc2	demokrat
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
republikánští	republikánský	k2eAgMnPc1d1	republikánský
poslanci	poslanec	k1gMnPc1	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byla	být	k5eAaImAgFnS	být
takto	takto	k6eAd1	takto
upravená	upravený	k2eAgFnSc1d1	upravená
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
reforma	reforma	k1gFnSc1	reforma
schválena	schválit	k5eAaPmNgFnS	schválit
v	v	k7c6	v
Senátu	senát	k1gInSc6	senát
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Unijní	unijní	k2eAgInPc1d1	unijní
státy	stát	k1gInPc1	stát
ovládané	ovládaný	k2eAgInPc1d1	ovládaný
Republikánskou	republikánský	k2eAgFnSc7d1	republikánská
stranou	strana	k1gFnSc7	strana
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
reagovaly	reagovat	k5eAaBmAgInP	reagovat
vytyčováním	vytyčování	k1gNnSc7	vytyčování
legislativy	legislativa	k1gFnSc2	legislativa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
má	mít	k5eAaImIp3nS	mít
znemožnit	znemožnit	k5eAaPmF	znemožnit
zavedení	zavedení	k1gNnSc4	zavedení
některých	některý	k3yIgInPc2	některý
principů	princip	k1gInPc2	princip
reformy	reforma	k1gFnSc2	reforma
na	na	k7c4	na
jejich	jejich	k3xOp3gNnSc4	jejich
území	území	k1gNnSc4	území
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
a	a	k8xC	a
vůči	vůči	k7c3	vůči
jejich	jejich	k3xOp3gMnPc3	jejich
občanům	občan	k1gMnPc3	občan
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
oznámily	oznámit	k5eAaPmAgInP	oznámit
některé	některý	k3yIgInPc1	některý
státy	stát	k1gInPc1	stát
úmysl	úmysl	k1gInSc1	úmysl
napadnout	napadnout	k5eAaPmF	napadnout
reformu	reforma	k1gFnSc4	reforma
u	u	k7c2	u
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
USA	USA	kA	USA
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nedosáhly	dosáhnout	k5eNaPmAgFnP	dosáhnout
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2011	[number]	k4	2011
tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
americká	americký	k2eAgFnSc1d1	americká
ministryně	ministryně	k1gFnSc1	ministryně
zahraničí	zahraničí	k1gNnSc2	zahraničí
Hillary	Hillara	k1gFnSc2	Hillara
Clintonová	Clintonová	k1gFnSc1	Clintonová
přesvědčila	přesvědčit	k5eAaPmAgFnS	přesvědčit
Obamu	Obama	k1gFnSc4	Obama
o	o	k7c6	o
nutnosti	nutnost	k1gFnSc6	nutnost
vojensky	vojensky	k6eAd1	vojensky
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
v	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
protivládním	protivládní	k2eAgFnPc3d1	protivládní
povstalcům	povstalec	k1gMnPc3	povstalec
proti	proti	k7c3	proti
libyjskému	libyjský	k2eAgMnSc3d1	libyjský
vůdci	vůdce	k1gMnSc3	vůdce
Muammaru	Muammar	k1gMnSc3	Muammar
Kaddáfímu	Kaddáfí	k1gMnSc3	Kaddáfí
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
přes	přes	k7c4	přes
odpor	odpor	k1gInSc4	odpor
amerického	americký	k2eAgNnSc2d1	americké
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
obrany	obrana	k1gFnSc2	obrana
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
k	k	k7c3	k
válce	válka	k1gFnSc3	válka
stavělo	stavět	k5eAaImAgNnS	stavět
odmítavě	odmítavě	k6eAd1	odmítavě
<g/>
.	.	kIx.	.
</s>
<s>
Kaddáfí	Kaddáfí	k1gMnSc1	Kaddáfí
byl	být	k5eAaImAgMnS	být
obviněn	obvinit	k5eAaPmNgMnS	obvinit
<g/>
,	,	kIx,	,
že	že	k8xS	že
masakruje	masakrovat	k5eAaBmIp3nS	masakrovat
vlastní	vlastní	k2eAgNnSc4d1	vlastní
civilní	civilní	k2eAgNnSc4d1	civilní
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Rezoluce	rezoluce	k1gFnSc1	rezoluce
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
č.	č.	k?	č.
1973	[number]	k4	1973
požadovala	požadovat	k5eAaImAgFnS	požadovat
"	"	kIx"	"
<g/>
okamžité	okamžitý	k2eAgNnSc4d1	okamžité
příměří	příměří	k1gNnSc4	příměří
<g/>
"	"	kIx"	"
a	a	k8xC	a
umožnila	umožnit	k5eAaPmAgFnS	umožnit
zřízení	zřízení	k1gNnSc4	zřízení
bezletové	bezletový	k2eAgFnSc2d1	bezletová
zóny	zóna	k1gFnSc2	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
zpravodajská	zpravodajský	k2eAgFnSc1d1	zpravodajská
služba	služba	k1gFnSc1	služba
CIA	CIA	kA	CIA
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
odpovědná	odpovědný	k2eAgFnSc1d1	odpovědná
prezidentovi	prezident	k1gMnSc3	prezident
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
od	od	k7c2	od
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
angažuje	angažovat	k5eAaBmIp3nS	angažovat
v	v	k7c6	v
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
a	a	k8xC	a
při	při	k7c6	při
pokusu	pokus	k1gInSc6	pokus
o	o	k7c4	o
svržení	svržení	k1gNnSc4	svržení
syrské	syrský	k2eAgFnSc2d1	Syrská
vlády	vláda	k1gFnSc2	vláda
vycvičila	vycvičit	k5eAaPmAgFnS	vycvičit
a	a	k8xC	a
vyzbrojila	vyzbrojit	k5eAaPmAgFnS	vyzbrojit
přibližně	přibližně	k6eAd1	přibližně
10	[number]	k4	10
000	[number]	k4	000
protivládních	protivládní	k2eAgMnPc2d1	protivládní
povstalců	povstalec	k1gMnPc2	povstalec
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
přeběhli	přeběhnout	k5eAaPmAgMnP	přeběhnout
k	k	k7c3	k
Islámskému	islámský	k2eAgInSc3d1	islámský
státu	stát	k1gInSc3	stát
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2012	[number]	k4	2012
se	se	k3xPyFc4	se
konaly	konat	k5eAaImAgFnP	konat
v	v	k7c6	v
USA	USA	kA	USA
prezidentské	prezidentský	k2eAgFnPc4d1	prezidentská
volby	volba	k1gFnPc4	volba
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yRgFnPc6	který
Obama	Obama	k?	Obama
porazil	porazit	k5eAaPmAgInS	porazit
svého	svůj	k3xOyFgMnSc4	svůj
konkurenta	konkurent	k1gMnSc4	konkurent
<g/>
,	,	kIx,	,
republikánského	republikánský	k2eAgMnSc4d1	republikánský
kandidáta	kandidát	k1gMnSc4	kandidát
Mitta	Mitt	k1gMnSc4	Mitt
Romneyho	Romney	k1gMnSc4	Romney
a	a	k8xC	a
obhájil	obhájit	k5eAaPmAgMnS	obhájit
svůj	svůj	k3xOyFgInSc4	svůj
úřad	úřad	k1gInSc4	úřad
na	na	k7c4	na
další	další	k2eAgInPc4d1	další
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
(	(	kIx(	(
<g/>
do	do	k7c2	do
ledna	leden	k1gInSc2	leden
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vítězství	vítězství	k1gNnSc3	vítězství
bylo	být	k5eAaImAgNnS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
získat	získat	k5eAaPmF	získat
270	[number]	k4	270
volitelských	volitelský	k2eAgInPc2d1	volitelský
hlasů	hlas	k1gInPc2	hlas
(	(	kIx(	(
<g/>
electoral	electorat	k5eAaImAgInS	electorat
vote	vote	k1gFnSc4	vote
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obama	Obama	k?	Obama
získal	získat	k5eAaPmAgInS	získat
332	[number]	k4	332
volitelských	volitelský	k2eAgInPc2d1	volitelský
hlasů	hlas	k1gInPc2	hlas
včetně	včetně	k7c2	včetně
těch	ten	k3xDgMnPc2	ten
za	za	k7c4	za
stát	stát	k1gInSc4	stát
Floridu	Florida	k1gFnSc4	Florida
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
hlasy	hlas	k1gInPc7	hlas
voličů	volič	k1gMnPc2	volič
musely	muset	k5eAaImAgInP	muset
přepočítávat	přepočítávat	k5eAaImF	přepočítávat
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
také	také	k9	také
u	u	k7c2	u
většiny	většina	k1gFnSc2	většina
voličů	volič	k1gMnPc2	volič
(	(	kIx(	(
<g/>
popular	popular	k1gMnSc1	popular
vote	vot	k1gFnSc2	vot
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
sice	sice	k8xC	sice
poměrem	poměr	k1gInSc7	poměr
hlasů	hlas	k1gInPc2	hlas
50,5	[number]	k4	50,5
%	%	kIx~	%
(	(	kIx(	(
<g/>
Obama	Obama	k?	Obama
<g/>
)	)	kIx)	)
ku	k	k7c3	k
48,0	[number]	k4	48,0
%	%	kIx~	%
(	(	kIx(	(
<g/>
Romney	Romnea	k1gFnPc1	Romnea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhého	druhý	k4xOgNnSc2	druhý
období	období	k1gNnSc2	období
se	se	k3xPyFc4	se
Obamovi	Obam	k1gMnSc3	Obam
podařilo	podařit	k5eAaPmAgNnS	podařit
velmi	velmi	k6eAd1	velmi
snížit	snížit	k5eAaPmF	snížit
deficit	deficit	k1gInSc4	deficit
rozpočtu	rozpočet	k1gInSc2	rozpočet
až	až	k9	až
na	na	k7c4	na
2,5	[number]	k4	2,5
%	%	kIx~	%
HDP	HDP	kA	HDP
(	(	kIx(	(
<g/>
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Též	též	k9	též
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
snížit	snížit	k5eAaPmF	snížit
nezaměstnanost	nezaměstnanost	k1gFnSc4	nezaměstnanost
na	na	k7c4	na
6	[number]	k4	6
<g/>
%	%	kIx~	%
(	(	kIx(	(
<g/>
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Růst	růst	k1gInSc1	růst
HDP	HDP	kA	HDP
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
rychlý	rychlý	k2eAgInSc1d1	rychlý
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
prvního	první	k4xOgInSc2	první
kvartálu	kvartál	k1gInSc2	kvartál
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
klesl	klesnout	k5eAaPmAgInS	klesnout
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
tuhé	tuhý	k2eAgFnSc2d1	tuhá
zimy	zima	k1gFnSc2	zima
a	a	k8xC	a
menšího	malý	k2eAgNnSc2d2	menší
vytváření	vytváření	k1gNnSc2	vytváření
zásob	zásoba	k1gFnPc2	zásoba
<g/>
,	,	kIx,	,
a	a	k8xC	a
ve	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
čtvrtletí	čtvrtletí	k1gNnSc6	čtvrtletí
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
opět	opět	k6eAd1	opět
očekáván	očekávat	k5eAaImNgInS	očekávat
růst	růst	k1gInSc1	růst
3,5	[number]	k4	3,5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Inflace	inflace	k1gFnSc1	inflace
se	se	k3xPyFc4	se
držela	držet	k5eAaImAgFnS	držet
během	během	k7c2	během
druhého	druhý	k4xOgNnSc2	druhý
období	období	k1gNnSc2	období
kolem	kolem	k7c2	kolem
2	[number]	k4	2
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
9	[number]	k4	9
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2009	[number]	k4	2009
obdržel	obdržet	k5eAaPmAgInS	obdržet
Barack	Barack	k1gInSc1	Barack
Obama	Obama	k?	Obama
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
míru	mír	k1gInSc2	mír
jako	jako	k8xC	jako
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
nejméně	málo	k6eAd3	málo
očekávaných	očekávaný	k2eAgMnPc2d1	očekávaný
kandidátů	kandidát	k1gMnPc2	kandidát
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
zvolení	zvolení	k1gNnSc4	zvolení
Obamy	Obama	k1gFnSc2	Obama
byly	být	k5eAaImAgInP	být
podle	podle	k7c2	podle
příslušného	příslušný	k2eAgInSc2d1	příslušný
výboru	výbor	k1gInSc2	výbor
jeho	jeho	k3xOp3gFnSc2	jeho
"	"	kIx"	"
<g/>
mimořádné	mimořádný	k2eAgFnSc2d1	mimořádná
snahy	snaha	k1gFnSc2	snaha
řešit	řešit	k5eAaImF	řešit
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
důrazem	důraz	k1gInSc7	důraz
na	na	k7c4	na
právo	právo	k1gNnSc4	právo
a	a	k8xC	a
dialog	dialog	k1gInSc4	dialog
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Barack	Barack	k1gInSc1	Barack
Obama	Obama	k?	Obama
se	se	k3xPyFc4	se
v	v	k7c6	v
několika	několik	k4yIc6	několik
svých	svůj	k3xOyFgInPc6	svůj
projevech	projev	k1gInPc6	projev
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
pro	pro	k7c4	pro
svět	svět	k1gInSc4	svět
bez	bez	k7c2	bez
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
podpořil	podpořit	k5eAaPmAgInS	podpořit
jednání	jednání	k1gNnSc4	jednání
s	s	k7c7	s
muslimskými	muslimský	k2eAgFnPc7d1	muslimská
zeměmi	zem	k1gFnPc7	zem
a	a	k8xC	a
věnoval	věnovat	k5eAaPmAgMnS	věnovat
se	se	k3xPyFc4	se
také	také	k9	také
blízkovýchodnímu	blízkovýchodní	k2eAgNnSc3d1	blízkovýchodní
mírovému	mírový	k2eAgNnSc3d1	Mírové
procesu	proces	k1gInSc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
Obamu	Obama	k1gFnSc4	Obama
hlasoval	hlasovat	k5eAaImAgMnS	hlasovat
pětičlenný	pětičlenný	k2eAgInSc4d1	pětičlenný
výbor	výbor	k1gInSc4	výbor
jednomyslně	jednomyslně	k6eAd1	jednomyslně
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
souhlasných	souhlasný	k2eAgFnPc2d1	souhlasná
ozvěn	ozvěna	k1gFnPc2	ozvěna
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
bylo	být	k5eAaImAgNnS	být
ocenění	ocenění	k1gNnSc1	ocenění
Obamy	Obama	k1gFnSc2	Obama
podrobeno	podrobit	k5eAaPmNgNnS	podrobit
některými	některý	k3yIgFnPc7	některý
komentátory	komentátor	k1gMnPc4	komentátor
silné	silný	k2eAgFnSc3d1	silná
kritice	kritika	k1gFnSc3	kritika
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
podle	podle	k7c2	podle
nich	on	k3xPp3gMnPc2	on
obdržel	obdržet	k5eAaPmAgMnS	obdržet
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
styl	styl	k1gInSc4	styl
<g/>
,	,	kIx,	,
sliby	slib	k1gInPc1	slib
a	a	k8xC	a
projevy	projev	k1gInPc1	projev
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
za	za	k7c4	za
reálné	reálný	k2eAgInPc4d1	reálný
výsledky	výsledek	k1gInPc4	výsledek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ocenění	ocenění	k1gNnSc2	ocenění
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
na	na	k7c6	na
rozpacích	rozpak	k1gInPc6	rozpak
i	i	k9	i
sám	sám	k3xTgMnSc1	sám
Obama	Obama	k?	Obama
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Abych	aby	kYmCp1nS	aby
byl	být	k5eAaImAgMnS	být
upřímný	upřímný	k2eAgMnSc1d1	upřímný
<g/>
,	,	kIx,	,
nemám	mít	k5eNaImIp1nS	mít
pocit	pocit	k1gInSc4	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bych	by	kYmCp1nS	by
si	se	k3xPyFc3	se
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
být	být	k5eAaImF	být
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
tolika	tolik	k4xDc2	tolik
přelomových	přelomový	k2eAgFnPc2d1	přelomová
osobností	osobnost	k1gFnPc2	osobnost
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Obama	Obama	k?	Obama
přislíbil	přislíbit	k5eAaPmAgMnS	přislíbit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
nyní	nyní	k6eAd1	nyní
ještě	ještě	k6eAd1	ještě
více	hodně	k6eAd2	hodně
snažit	snažit	k5eAaImF	snažit
o	o	k7c4	o
udržení	udržení	k1gNnSc4	udržení
míru	mír	k1gInSc2	mír
<g/>
.	.	kIx.	.
</s>
<s>
Cenu	cena	k1gFnSc4	cena
převzal	převzít	k5eAaPmAgInS	převzít
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
oznámil	oznámit	k5eAaPmAgMnS	oznámit
vyslání	vyslání	k1gNnSc4	vyslání
dalších	další	k2eAgInPc2d1	další
30	[number]	k4	30
tisíc	tisíc	k4xCgInPc2	tisíc
mužů	muž	k1gMnPc2	muž
do	do	k7c2	do
Afghánistánu	Afghánistán	k1gInSc2	Afghánistán
<g/>
,	,	kIx,	,
během	během	k7c2	během
přebírání	přebírání	k1gNnSc2	přebírání
ceny	cena	k1gFnSc2	cena
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
projevu	projev	k1gInSc6	projev
hájil	hájit	k5eAaImAgInS	hájit
tzv.	tzv.	kA	tzv.
spravedlivou	spravedlivý	k2eAgFnSc4d1	spravedlivá
válku	válka	k1gFnSc4	válka
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Spory	spora	k1gFnSc2	spora
o	o	k7c4	o
volitelnost	volitelnost	k1gFnSc4	volitelnost
Baracka	Baracko	k1gNnSc2	Baracko
Obamy	Obama	k1gFnSc2	Obama
<g/>
.	.	kIx.	.
</s>
<s>
Barack	Barack	k1gMnSc1	Barack
Obama	Obama	k?	Obama
čelil	čelit	k5eAaImAgMnS	čelit
obviněním	obvinění	k1gNnSc7	obvinění
<g/>
,	,	kIx,	,
že	že	k8xS	že
udržuje	udržovat	k5eAaImIp3nS	udržovat
dlouholeté	dlouholetý	k2eAgNnSc4d1	dlouholeté
přátelství	přátelství	k1gNnSc4	přátelství
s	s	k7c7	s
Billem	Bill	k1gMnSc7	Bill
Ayersem	Ayers	k1gMnSc7	Ayers
a	a	k8xC	a
Bernardine	bernardin	k1gMnSc5	bernardin
Dohrnovou	Dohrnový	k2eAgFnSc4d1	Dohrnový
<g/>
,	,	kIx,	,
bývalými	bývalý	k2eAgMnPc7d1	bývalý
vůdci	vůdce	k1gMnPc1	vůdce
skupiny	skupina	k1gFnSc2	skupina
Weatherman	Weatherman	k1gMnSc1	Weatherman
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
význačné	význačný	k2eAgMnPc4d1	význačný
představitele	představitel	k1gMnPc4	představitel
chicagské	chicagský	k2eAgFnSc2d1	Chicagská
extrémní	extrémní	k2eAgFnSc2d1	extrémní
levice	levice	k1gFnSc2	levice
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
se	se	k3xPyFc4	se
prokázalo	prokázat	k5eAaPmAgNnS	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
neměl	mít	k5eNaImAgMnS	mít
s	s	k7c7	s
Ayersem	Ayers	k1gInSc7	Ayers
blízký	blízký	k2eAgInSc1d1	blízký
vztah	vztah	k1gInSc1	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Mentorem	mentor	k1gInSc7	mentor
mladého	mladý	k2eAgMnSc4d1	mladý
Barryho	Barry	k1gMnSc4	Barry
Obamy	Obama	k1gFnSc2	Obama
na	na	k7c6	na
Havaji	Havaj	k1gFnSc6	Havaj
byl	být	k5eAaImAgMnS	být
černošský	černošský	k2eAgMnSc1d1	černošský
komunistický	komunistický	k2eAgMnSc1d1	komunistický
aktivista	aktivista	k1gMnSc1	aktivista
a	a	k8xC	a
spisovatel	spisovatel	k1gMnSc1	spisovatel
Frank	Frank	k1gMnSc1	Frank
Marshall	Marshall	k1gMnSc1	Marshall
Davis	Davis	k1gInSc4	Davis
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
(	(	kIx(	(
<g/>
mccarthismus	mccarthismus	k1gInSc1	mccarthismus
<g/>
)	)	kIx)	)
vyšetřován	vyšetřovat	k5eAaImNgInS	vyšetřovat
americkým	americký	k2eAgInSc7d1	americký
Sněmovním	sněmovní	k2eAgInSc7d1	sněmovní
výborem	výbor	k1gInSc7	výbor
pro	pro	k7c4	pro
neamerickou	americký	k2eNgFnSc4d1	neamerická
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
Davis	Davis	k1gInSc4	Davis
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
mladého	mladý	k2eAgInSc2d1	mladý
Obamu	Obam	k1gInSc2	Obam
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc4	jehož
vychovávali	vychovávat	k5eAaImAgMnP	vychovávat
jeho	jeho	k3xOp3gMnPc1	jeho
bílí	bílý	k2eAgMnPc1d1	bílý
prarodiče	prarodič	k1gMnPc1	prarodič
z	z	k7c2	z
matčiny	matčin	k2eAgFnSc2d1	matčina
strany	strana	k1gFnSc2	strana
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
více	hodně	k6eAd2	hodně
zdůrazňoval	zdůrazňovat	k5eAaImAgInS	zdůrazňovat
svou	svůj	k3xOyFgFnSc4	svůj
černošskou	černošský	k2eAgFnSc4d1	černošská
identitu	identita	k1gFnSc4	identita
a	a	k8xC	a
jako	jako	k8xC	jako
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
jméno	jméno	k1gNnSc4	jméno
používal	používat	k5eAaImAgMnS	používat
nikoli	nikoli	k9	nikoli
Barry	Barr	k1gMnPc4	Barr
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
Barack	Barack	k1gInSc1	Barack
<g/>
.	.	kIx.	.
</s>
<s>
Obamova	Obamův	k2eAgFnSc1d1	Obamova
manželka	manželka	k1gFnSc1	manželka
Michelle	Michelle	k1gFnSc1	Michelle
za	za	k7c2	za
studií	studio	k1gNnPc2	studio
na	na	k7c6	na
Princetonu	Princeton	k1gInSc6	Princeton
sepsala	sepsat	k5eAaPmAgFnS	sepsat
práci	práce	k1gFnSc4	práce
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yQgFnSc6	který
popisovala	popisovat	k5eAaImAgFnS	popisovat
dvojí	dvojí	k4xRgInSc4	dvojí
možný	možný	k2eAgInSc4d1	možný
černošský	černošský	k2eAgInSc4d1	černošský
postoj	postoj	k1gInSc4	postoj
k	k	k7c3	k
americké	americký	k2eAgFnSc3d1	americká
společnosti	společnost	k1gFnSc3	společnost
<g/>
:	:	kIx,	:
integracionistický	integracionistický	k2eAgInSc4d1	integracionistický
a	a	k8xC	a
segregacionistický	segregacionistický	k2eAgInSc4d1	segregacionistický
<g/>
.	.	kIx.	.
</s>
<s>
Integracionistický	Integracionistický	k2eAgMnSc1d1	Integracionistický
znamená	znamenat	k5eAaImIp3nS	znamenat
přijetí	přijetí	k1gNnSc1	přijetí
hodnot	hodnota	k1gFnPc2	hodnota
americké	americký	k2eAgFnPc1d1	americká
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
bílé	bílý	k2eAgFnPc1d1	bílá
<g/>
,	,	kIx,	,
kapitalistické	kapitalistický	k2eAgFnPc1d1	kapitalistická
<g/>
)	)	kIx)	)
společnosti	společnost	k1gFnPc1	společnost
a	a	k8xC	a
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
cestou	cesta	k1gFnSc7	cesta
k	k	k7c3	k
úspěchu	úspěch	k1gInSc3	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
se	se	k3xPyFc4	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
však	však	k9	však
také	také	k6eAd1	také
dívat	dívat	k5eAaImF	dívat
jako	jako	k9	jako
na	na	k7c4	na
zradu	zrada	k1gFnSc4	zrada
černošství	černošství	k1gNnSc2	černošství
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Obamovým	Obamův	k2eAgMnPc3d1	Obamův
přátelům	přítel	k1gMnPc3	přítel
se	se	k3xPyFc4	se
také	také	k9	také
řadil	řadit	k5eAaImAgMnS	řadit
radikální	radikální	k2eAgMnSc1d1	radikální
pastor	pastor	k1gMnSc1	pastor
Jeremiah	Jeremiah	k1gMnSc1	Jeremiah
Wright	Wright	k1gMnSc1	Wright
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
jeho	jeho	k3xOp3gInPc4	jeho
výroky	výrok	k1gInPc4	výrok
patřilo	patřit	k5eAaImAgNnS	patřit
tvrzení	tvrzení	k1gNnPc4	tvrzení
<g/>
,	,	kIx,	,
že	že	k8xS	že
virus	virus	k1gInSc1	virus
HIV	HIV	kA	HIV
byl	být	k5eAaImAgInS	být
záměrně	záměrně	k6eAd1	záměrně
vynalezen	vynaleznout	k5eAaPmNgInS	vynaleznout
a	a	k8xC	a
použit	použít	k5eAaPmNgInS	použít
americkou	americký	k2eAgFnSc7d1	americká
vládou	vláda	k1gFnSc7	vláda
jako	jako	k8xS	jako
zbraň	zbraň	k1gFnSc1	zbraň
proti	proti	k7c3	proti
černochům	černoch	k1gMnPc3	černoch
a	a	k8xC	a
obvinění	obvinění	k1gNnSc2	obvinění
vlády	vláda	k1gFnSc2	vláda
USA	USA	kA	USA
ze	z	k7c2	z
zosnování	zosnování	k1gNnSc2	zosnování
útoků	útok	k1gInPc2	útok
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Wright	Wright	k1gMnSc1	Wright
patřil	patřit	k5eAaImAgMnS	patřit
v	v	k7c6	v
osmdesátých	osmdesátý	k4xOgNnPc6	osmdesátý
letech	léto	k1gNnPc6	léto
minulého	minulý	k2eAgNnSc2d1	Minulé
století	století	k1gNnSc2	století
k	k	k7c3	k
Obamovým	Obamův	k2eAgMnPc3d1	Obamův
blízkým	blízký	k2eAgMnPc3d1	blízký
přátelům	přítel	k1gMnPc3	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Wright	Wright	k1gMnSc1	Wright
Obamu	Obam	k1gInSc2	Obam
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
manželku	manželka	k1gFnSc4	manželka
Michelle	Michelle	k1gInSc1	Michelle
oddal	oddat	k5eAaPmAgInS	oddat
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
i	i	k9	i
pokřtil	pokřtít	k5eAaPmAgMnS	pokřtít
jejich	jejich	k3xOp3gFnPc4	jejich
dvě	dva	k4xCgFnPc4	dva
dcery	dcera	k1gFnPc4	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Barack	Barack	k1gInSc1	Barack
Obama	Obama	k?	Obama
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
nicméně	nicméně	k8xC	nicméně
od	od	k7c2	od
reverenda	reverend	k1gMnSc2	reverend
Wrighta	Wright	k1gMnSc2	Wright
distancoval	distancovat	k5eAaBmAgMnS	distancovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
se	se	k3xPyFc4	se
vyskytly	vyskytnout	k5eAaPmAgFnP	vyskytnout
také	také	k9	také
pochyby	pochyba	k1gFnPc1	pochyba
ohledně	ohledně	k7c2	ohledně
pravosti	pravost	k1gFnSc2	pravost
Obamova	Obamův	k2eAgInSc2d1	Obamův
rodného	rodný	k2eAgInSc2d1	rodný
listu	list	k1gInSc2	list
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
části	část	k1gFnSc2	část
americké	americký	k2eAgFnSc2d1	americká
veřejnosti	veřejnost	k1gFnSc2	veřejnost
<g/>
,	,	kIx,	,
především	především	k9	především
u	u	k7c2	u
republikánů	republikán	k1gMnPc2	republikán
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
názor	názor	k1gInSc1	názor
<g/>
,	,	kIx,	,
že	že	k8xS	že
Obama	Obama	k?	Obama
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Keni	Keňa	k1gFnSc6	Keňa
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
na	na	k7c6	na
Havaji	Havaj	k1gFnSc6	Havaj
a	a	k8xC	a
že	že	k8xS	že
jeho	jeho	k3xOp3gInSc1	jeho
rodný	rodný	k2eAgInSc1d1	rodný
list	list	k1gInSc1	list
byl	být	k5eAaImAgInS	být
zfalšován	zfalšovat	k5eAaPmNgInS	zfalšovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
uváděl	uvádět	k5eAaImAgInS	uvádět
jako	jako	k9	jako
místo	místo	k7c2	místo
narození	narození	k1gNnSc2	narození
město	město	k1gNnSc1	město
Honolulu	Honolulu	k1gNnSc3	Honolulu
<g/>
.	.	kIx.	.
</s>
<s>
Obamova	Obamův	k2eAgFnSc1d1	Obamova
administrativa	administrativa	k1gFnSc1	administrativa
byla	být	k5eAaImAgFnS	být
kritizována	kritizovat	k5eAaImNgFnS	kritizovat
za	za	k7c4	za
podporu	podpora	k1gFnSc4	podpora
saúdské	saúdský	k2eAgFnSc2d1	Saúdská
vojenské	vojenský	k2eAgFnSc2d1	vojenská
intervence	intervence	k1gFnSc2	intervence
v	v	k7c6	v
Jemenu	Jemen	k1gInSc6	Jemen
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
útokům	útok	k1gInPc3	útok
na	na	k7c4	na
civilisty	civilista	k1gMnPc4	civilista
a	a	k8xC	a
dalším	další	k2eAgInPc3d1	další
válečným	válečný	k2eAgInPc3d1	válečný
zločinům	zločin	k1gInPc3	zločin
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
arabské	arabský	k2eAgFnSc2d1	arabská
koalice	koalice	k1gFnSc2	koalice
vedené	vedený	k2eAgFnSc2d1	vedená
Saúdy	Saúda	k1gFnSc2	Saúda
<g/>
.	.	kIx.	.
</s>
<s>
Obamova	Obamův	k2eAgFnSc1d1	Obamova
vláda	vláda	k1gFnSc1	vláda
čelila	čelit	k5eAaImAgFnS	čelit
také	také	k6eAd1	také
kritice	kritika	k1gFnSc3	kritika
kvůli	kvůli	k7c3	kvůli
dodávkám	dodávka	k1gFnPc3	dodávka
zbraní	zbraň	k1gFnPc2	zbraň
do	do	k7c2	do
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
v	v	k7c6	v
celkové	celkový	k2eAgFnSc6d1	celková
hodnotě	hodnota	k1gFnSc6	hodnota
110	[number]	k4	110
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
za	za	k7c4	za
mlčení	mlčení	k1gNnSc4	mlčení
k	k	k7c3	k
porušování	porušování	k1gNnSc3	porušování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Barack	Barack	k1gInSc1	Barack
Obama	Obama	k?	Obama
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
uváděn	uvádět	k5eAaImNgInS	uvádět
jako	jako	k9	jako
vzor	vzor	k1gInSc4	vzor
dobře	dobře	k6eAd1	dobře
zvládnutého	zvládnutý	k2eAgInSc2d1	zvládnutý
politického	politický	k2eAgInSc2d1	politický
marketingu	marketing	k1gInSc2	marketing
<g/>
.	.	kIx.	.
</s>
<s>
Volební	volební	k2eAgInSc1d1	volební
tým	tým	k1gInSc1	tým
Baracka	Baracko	k1gNnSc2	Baracko
Obamy	Obama	k1gFnSc2	Obama
využil	využít	k5eAaPmAgInS	využít
ve	v	k7c6	v
volbách	volba	k1gFnPc6	volba
2012	[number]	k4	2012
internetové	internetový	k2eAgFnPc4d1	internetová
stránky	stránka	k1gFnPc4	stránka
s	s	k7c7	s
výzvou	výzva	k1gFnSc7	výzva
k	k	k7c3	k
akci	akce	k1gFnSc3	akce
<g/>
,	,	kIx,	,
A	A	kA	A
<g/>
/	/	kIx~	/
<g/>
B	B	kA	B
testování	testování	k1gNnSc1	testování
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
zpracovanou	zpracovaný	k2eAgFnSc4d1	zpracovaná
stránku	stránka	k1gFnSc4	stránka
pro	pro	k7c4	pro
darování	darování	k1gNnSc4	darování
<g/>
,	,	kIx,	,
e-mailový	eailový	k2eAgInSc4d1	e-mailový
marketing	marketing	k1gInSc4	marketing
<g/>
,	,	kIx,	,
blog	blog	k1gInSc4	blog
<g/>
,	,	kIx,	,
prvky	prvek	k1gInPc4	prvek
sociálních	sociální	k2eAgFnPc2d1	sociální
sítí	síť	k1gFnPc2	síť
integrované	integrovaný	k2eAgFnSc2d1	integrovaná
do	do	k7c2	do
webu	web	k1gInSc2	web
<g/>
,	,	kIx,	,
segmentaci	segmentace	k1gFnSc4	segmentace
kampaní	kampaň	k1gFnPc2	kampaň
podle	podle	k7c2	podle
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
intenzivní	intenzivní	k2eAgInSc1d1	intenzivní
komunikaci	komunikace	k1gFnSc4	komunikace
na	na	k7c6	na
Facebooku	Facebook	k1gInSc6	Facebook
<g/>
,	,	kIx,	,
Twitteru	Twitter	k1gInSc6	Twitter
i	i	k8xC	i
YouTube	YouTub	k1gInSc5	YouTub
a	a	k8xC	a
mobilní	mobilní	k2eAgFnSc4d1	mobilní
verzi	verze	k1gFnSc4	verze
stránek	stránka	k1gFnPc2	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
comScore	comScor	k1gInSc5	comScor
zveřejnila	zveřejnit	k5eAaPmAgFnS	zveřejnit
analýzu	analýza	k1gFnSc4	analýza
o	o	k7c4	o
využití	využití	k1gNnSc4	využití
online	onlinout	k5eAaPmIp3nS	onlinout
médií	médium	k1gNnPc2	médium
během	během	k7c2	během
kampaně	kampaň	k1gFnSc2	kampaň
<g/>
.	.	kIx.	.
</s>
<s>
Obama	Obama	k?	Obama
zaplnil	zaplnit	k5eAaPmAgInS	zaplnit
web	web	k1gInSc4	web
svojí	svojit	k5eAaImIp3nS	svojit
kampaní	kampaň	k1gFnSc7	kampaň
z	z	k7c2	z
86	[number]	k4	86
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Dařilo	dařit	k5eAaImAgNnS	dařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
také	také	k6eAd1	také
na	na	k7c6	na
sociálních	sociální	k2eAgInPc6d1	sociální
médiích	médium	k1gNnPc6	médium
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nasbíral	nasbírat	k5eAaPmAgMnS	nasbírat
26	[number]	k4	26
milionů	milion	k4xCgInPc2	milion
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Protikandidát	protikandidát	k1gMnSc1	protikandidát
Mitt	Mitt	k2eAgMnSc1d1	Mitt
Romney	Romnea	k1gFnPc4	Romnea
získal	získat	k5eAaPmAgMnS	získat
pouze	pouze	k6eAd1	pouze
1,6	[number]	k4	1,6
milionů	milion	k4xCgInPc2	milion
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
dohoda	dohoda	k1gFnSc1	dohoda
START	start	k1gInSc1	start
Jemenský	jemenský	k2eAgInSc4d1	jemenský
model	model	k1gInSc4	model
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Barack	Baracka	k1gFnPc2	Baracka
Obama	Obama	k?	Obama
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Barack	Barack	k1gInSc1	Barack
Obama	Obama	k?	Obama
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Galerie	galerie	k1gFnSc1	galerie
Barack	Barack	k1gInSc1	Barack
Obama	Obama	k?	Obama
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Barack	Barack	k1gInSc1	Barack
Obama	Obama	k?	Obama
Kategorie	kategorie	k1gFnPc1	kategorie
Barack	Baracka	k1gFnPc2	Baracka
Obama	Obama	k?	Obama
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
Baracka	Baracko	k1gNnSc2	Baracko
Obamy	Obama	k1gFnSc2	Obama
Volební	volební	k2eAgNnSc1d1	volební
centrum	centrum	k1gNnSc1	centrum
televizní	televizní	k2eAgFnSc2d1	televizní
stanice	stanice	k1gFnSc2	stanice
CNN	CNN	kA	CNN
-	-	kIx~	-
výsledky	výsledek	k1gInPc1	výsledek
voleb	volba	k1gFnPc2	volba
a	a	k8xC	a
primárek	primárky	k1gFnPc2	primárky
Debata	debata	k1gFnSc1	debata
z	z	k7c2	z
primárek	primárky	k1gFnPc2	primárky
demokratů	demokrat	k1gMnPc2	demokrat
Je	být	k5eAaImIp3nS	být
Barack	Barack	k1gInSc4	Barack
Obama	Obama	k?	Obama
vůbec	vůbec	k9	vůbec
volitelný	volitelný	k2eAgInSc1d1	volitelný
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
prezidenta	prezident	k1gMnSc2	prezident
USA	USA	kA	USA
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
Britské	britský	k2eAgInPc1d1	britský
listy	list	k1gInPc1	list
Událost	událost	k1gFnSc1	událost
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
:	:	kIx,	:
Barack	Barack	k1gInSc1	Barack
Obama	Obama	k?	Obama
americkým	americký	k2eAgMnSc7d1	americký
prezidentem	prezident	k1gMnSc7	prezident
z	z	k7c2	z
vůle	vůle	k1gFnSc2	vůle
médií	médium	k1gNnPc2	médium
<g/>
,	,	kIx,	,
deník	deník	k1gInSc1	deník
E15	E15	k1gFnSc2	E15
č.	č.	k?	č.
277	[number]	k4	277
<g/>
,	,	kIx,	,
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
12	[number]	k4	12
-	-	kIx~	-
13	[number]	k4	13
Patrick	Patrick	k1gInSc1	Patrick
J.	J.	kA	J.
Buchanan	Buchanan	k1gInSc1	Buchanan
<g/>
:	:	kIx,	:
Obama	Obama	k?	Obama
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
First	First	k1gInSc1	First
100	[number]	k4	100
Days	Daysa	k1gFnPc2	Daysa
<g/>
–	–	k?	–
<g/>
Amnesty	Amnest	k1gInPc1	Amnest
Will	Willa	k1gFnPc2	Willa
Just	just	k6eAd1	just
Be	Be	k1gFnSc1	Be
The	The	k1gFnSc1	The
Start	start	k1gInSc1	start
Stránka	stránka	k1gFnSc1	stránka
ČT24	ČT24	k1gFnSc1	ČT24
věnovaná	věnovaný	k2eAgFnSc1d1	věnovaná
prezidentským	prezidentský	k2eAgInSc7d1	prezidentský
volbám	volba	k1gFnPc3	volba
2008	[number]	k4	2008
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Obama	Obama	k?	Obama
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
'	'	kIx"	'
<g/>
Hate	Hat	k1gMnSc2	Hat
Bill	Bill	k1gMnSc1	Bill
<g/>
'	'	kIx"	'
Steals	Steals	k1gInSc1	Steals
Right	Right	k1gInSc1	Right
to	ten	k3xDgNnSc4	ten
Free	Free	k1gNnSc4	Free
Speech	speech	k1gInSc4	speech
Barack	Barack	k1gInSc1	Barack
Obama	Obama	k?	Obama
<g/>
:	:	kIx,	:
Zbavit	zbavit	k5eAaPmF	zbavit
se	se	k3xPyFc4	se
jaderných	jaderný	k2eAgFnPc2d1	jaderná
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
?	?	kIx.	?
</s>
<s>
Yes	Yes	k?	Yes
<g/>
,	,	kIx,	,
we	we	k?	we
can	can	k?	can
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
,	,	kIx,	,
český	český	k2eAgInSc1d1	český
překlad	překlad	k1gInSc1	překlad
projevu	projev	k1gInSc2	projev
proneseného	pronesený	k2eAgInSc2d1	pronesený
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
Hospodářské	hospodářský	k2eAgFnPc4d1	hospodářská
noviny	novina	k1gFnPc4	novina
<g/>
,	,	kIx,	,
6	[number]	k4	6
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
2009	[number]	k4	2009
</s>
