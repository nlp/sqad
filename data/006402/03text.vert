<s>
Bludný	bludný	k2eAgInSc1d1	bludný
balvan	balvan	k1gInSc1	balvan
(	(	kIx(	(
<g/>
zkráceně	zkráceně	k6eAd1	zkráceně
Bl.	Bl.	k1gMnSc2	Bl.
b.	b.	k?	b.
nebo	nebo	k8xC	nebo
BlB	blb	k1gMnSc1	blb
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
cena	cena	k1gFnSc1	cena
resp.	resp.	kA	resp.
anticena	anticen	k2eAgFnSc1d1	anticena
Českého	český	k2eAgInSc2d1	český
klubu	klub	k1gInSc2	klub
skeptiků	skeptik	k1gMnPc2	skeptik
Sisyfos	Sisyfos	k1gMnSc1	Sisyfos
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
ji	on	k3xPp3gFnSc4	on
uděluje	udělovat	k5eAaImIp3nS	udělovat
za	za	k7c4	za
pseudovědeckou	pseudovědecký	k2eAgFnSc4d1	pseudovědecká
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
je	být	k5eAaImIp3nS	být
pravidelně	pravidelně	k6eAd1	pravidelně
udělována	udělovat	k5eAaImNgFnS	udělovat
každý	každý	k3xTgInSc4	každý
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
vždy	vždy	k6eAd1	vždy
za	za	k7c4	za
rok	rok	k1gInSc4	rok
předchozí	předchozí	k2eAgInSc4d1	předchozí
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
ročník	ročník	k1gInSc1	ročník
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
Bludné	bludný	k2eAgInPc1d1	bludný
balvany	balvan	k1gInPc1	balvan
jsou	být	k5eAaImIp3nP	být
udělovány	udělovat	k5eAaImNgInP	udělovat
v	v	k7c6	v
kategoriích	kategorie	k1gFnPc6	kategorie
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
a	a	k8xC	a
družstev	družstvo	k1gNnPc2	družstvo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
každém	každý	k3xTgInSc6	každý
ročníku	ročník	k1gInSc6	ročník
se	se	k3xPyFc4	se
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
kategorii	kategorie	k1gFnSc6	kategorie
udělují	udělovat	k5eAaImIp3nP	udělovat
nejvýše	nejvýše	k6eAd1	nejvýše
tři	tři	k4xCgInPc1	tři
Bludné	bludný	k2eAgInPc1d1	bludný
kameny	kámen	k1gInPc1	kámen
s	s	k7c7	s
odlišeným	odlišený	k2eAgNnSc7d1	odlišené
pořadím	pořadí	k1gNnSc7	pořadí
<g/>
,	,	kIx,	,
vzestupně	vzestupně	k6eAd1	vzestupně
od	od	k7c2	od
bronzového	bronzový	k2eAgNnSc2d1	bronzové
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
stříbrný	stříbrný	k1gInSc4	stříbrný
až	až	k9	až
po	po	k7c4	po
vítězný	vítězný	k2eAgInSc4d1	vítězný
zlatý	zlatý	k1gInSc4	zlatý
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
10	[number]	k4	10
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
udílení	udílení	k1gNnSc2	udílení
cen	cena	k1gFnPc2	cena
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
udělil	udělit	k5eAaPmAgInS	udělit
klub	klub	k1gInSc1	klub
navíc	navíc	k6eAd1	navíc
výjimečnou	výjimečný	k2eAgFnSc4d1	výjimečná
cenu	cena	k1gFnSc4	cena
Jubilejní	jubilejní	k2eAgMnSc1d1	jubilejní
Diamantový	diamantový	k2eAgMnSc1d1	diamantový
Bludný	bludný	k2eAgMnSc1d1	bludný
Arcibalvan	Arcibalvan	k1gMnSc1	Arcibalvan
<g/>
.	.	kIx.	.
</s>
<s>
Cenu	cena	k1gFnSc4	cena
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
a	a	k8xC	a
vyrobila	vyrobit	k5eAaPmAgFnS	vyrobit
výtvarnice	výtvarnice	k1gFnSc1	výtvarnice
Jitka	Jitka	k1gFnSc1	Jitka
Splítková	Splítková	k1gFnSc1	Splítková
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
kameny	kámen	k1gInPc4	kámen
nabarvené	nabarvený	k2eAgNnSc1d1	nabarvené
příslušnou	příslušný	k2eAgFnSc7d1	příslušná
barvou	barva	k1gFnSc7	barva
<g/>
:	:	kIx,	:
bronzovou	bronzový	k2eAgFnSc7d1	bronzová
<g/>
,	,	kIx,	,
stříbřenkou	stříbřenka	k1gFnSc7	stříbřenka
a	a	k8xC	a
zlatěnkou	zlatěnka	k1gFnSc7	zlatěnka
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
tvořily	tvořit	k5eAaImAgInP	tvořit
jubilejní	jubilejní	k2eAgInPc1d1	jubilejní
diamantové	diamantový	k2eAgInPc1d1	diamantový
balvany	balvan	k1gInPc1	balvan
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byly	být	k5eAaImAgFnP	být
předány	předán	k2eAgInPc1d1	předán
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
pytlíku	pytlík	k1gInSc2	pytlík
sazí	saze	k1gFnPc2	saze
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
aplikaci	aplikace	k1gFnSc6	aplikace
vysokého	vysoký	k2eAgInSc2d1	vysoký
tlaku	tlak	k1gInSc2	tlak
a	a	k8xC	a
teploty	teplota	k1gFnSc2	teplota
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
promění	proměnit	k5eAaPmIp3nS	proměnit
v	v	k7c4	v
diamanty	diamant	k1gInPc4	diamant
<g/>
.	.	kIx.	.
</s>
<s>
Sdružení	sdružení	k1gNnSc1	sdružení
Sisyfos	Sisyfos	k1gMnSc1	Sisyfos
uděluje	udělovat	k5eAaImIp3nS	udělovat
Bludné	bludný	k2eAgInPc4d1	bludný
balvany	balvan	k1gInPc4	balvan
"	"	kIx"	"
<g/>
pro	pro	k7c4	pro
zvýraznění	zvýraznění	k1gNnSc4	zvýraznění
přínosu	přínos	k1gInSc2	přínos
jednotlivců	jednotlivec	k1gMnPc2	jednotlivec
i	i	k8xC	i
různých	různý	k2eAgInPc2d1	různý
spolků	spolek	k1gInPc2	spolek
k	k	k7c3	k
matení	matení	k1gNnSc3	matení
české	český	k2eAgFnSc2d1	Česká
veřejnosti	veřejnost	k1gFnSc2	veřejnost
a	a	k8xC	a
rozvoji	rozvoj	k1gInSc3	rozvoj
blátivého	blátivý	k2eAgInSc2d1	blátivý
způsobu	způsob	k1gInSc2	způsob
myšlení	myšlení	k1gNnSc2	myšlení
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Nominovat	nominovat	k5eAaBmF	nominovat
lze	lze	k6eAd1	lze
kohokoliv	kdokoliv	k3yInSc4	kdokoliv
<g/>
,	,	kIx,	,
kdo	kdo	k3yInSc1	kdo
v	v	k7c6	v
daném	daný	k2eAgInSc6d1	daný
roce	rok	k1gInSc6	rok
působil	působit	k5eAaImAgMnS	působit
na	na	k7c6	na
území	území	k1gNnSc6	území
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
navrhnout	navrhnout	k5eAaPmF	navrhnout
kandidáta	kandidát	k1gMnSc4	kandidát
může	moct	k5eAaImIp3nS	moct
jakýkoliv	jakýkoliv	k3yIgInSc1	jakýkoliv
člen	člen	k1gInSc1	člen
Sisyfa	Sisyfos	k1gMnSc2	Sisyfos
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
kandidátů	kandidát	k1gMnPc2	kandidát
pak	pak	k6eAd1	pak
nejméně	málo	k6eAd3	málo
pětičlenný	pětičlenný	k2eAgInSc4d1	pětičlenný
Komitét	komitét	k1gInSc4	komitét
pro	pro	k7c4	pro
udělování	udělování	k1gNnSc4	udělování
BlB	blb	k1gMnSc1	blb
navrhne	navrhnout	k5eAaPmIp3nS	navrhnout
tajném	tajný	k2eAgNnSc6d1	tajné
hlasování	hlasování	k1gNnSc6	hlasování
udělení	udělení	k1gNnSc4	udělení
cen	cena	k1gFnPc2	cena
<g/>
.	.	kIx.	.
</s>
<s>
Konečné	Konečné	k2eAgNnSc1d1	Konečné
rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
o	o	k7c6	o
udělení	udělení	k1gNnSc6	udělení
cen	cena	k1gFnPc2	cena
přísluší	příslušet	k5eAaImIp3nS	příslušet
výkonnému	výkonný	k2eAgInSc3d1	výkonný
výboru	výbor	k1gInSc3	výbor
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
udělení	udělení	k1gNnSc4	udělení
cen	cena	k1gFnPc2	cena
potvrdí	potvrdit	k5eAaPmIp3nS	potvrdit
taktéž	taktéž	k?	taktéž
v	v	k7c6	v
tajném	tajný	k2eAgNnSc6d1	tajné
hlasování	hlasování	k1gNnSc6	hlasování
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
udělení	udělení	k1gNnSc3	udělení
ceny	cena	k1gFnSc2	cena
není	být	k5eNaImIp3nS	být
"	"	kIx"	"
<g/>
odvolání	odvolání	k1gNnSc4	odvolání
ani	ani	k8xC	ani
slitování	slitování	k1gNnSc4	slitování
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
pokud	pokud	k8xS	pokud
oceněný	oceněný	k2eAgMnSc1d1	oceněný
získá	získat	k5eAaPmIp3nS	získat
v	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
povinen	povinen	k2eAgMnSc1d1	povinen
Bludný	bludný	k2eAgMnSc1d1	bludný
balvan	balvan	k1gMnSc1	balvan
vrátit	vrátit	k5eAaPmF	vrátit
a	a	k8xC	a
výbor	výbor	k1gInSc1	výbor
Sysifa	Sysif	k1gMnSc2	Sysif
ho	on	k3xPp3gMnSc4	on
vymaže	vymazat	k5eAaPmIp3nS	vymazat
z	z	k7c2	z
rejstříku	rejstřík	k1gInSc2	rejstřík
laureátů	laureát	k1gMnPc2	laureát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
<g/>
[	[	kIx(	[
<g/>
kdy	kdy	k6eAd1	kdy
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
v	v	k7c6	v
Komitétu	komitét	k1gInSc6	komitét
pro	pro	k7c4	pro
udělování	udělování	k1gNnSc4	udělování
Bludných	bludný	k2eAgInPc2d1	bludný
balvanů	balvan	k1gInPc2	balvan
působí	působit	k5eAaImIp3nS	působit
Martin	Martin	k1gMnSc1	Martin
Bloch	Bloch	k1gMnSc1	Bloch
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Brodský	Brodský	k1gMnSc1	Brodský
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
Grygar	Grygar	k1gMnSc1	Grygar
<g/>
,	,	kIx,	,
Olga	Olga	k1gFnSc1	Olga
Kracíková	Kracíková	k1gFnSc1	Kracíková
<g/>
,	,	kIx,	,
Miroslav	Miroslav	k1gMnSc1	Miroslav
Kutílek	Kutílek	k1gMnSc1	Kutílek
<g/>
,	,	kIx,	,
Leoš	Leoš	k1gMnSc1	Leoš
Kyša	Kyša	k1gMnSc1	Kyša
<g/>
,	,	kIx,	,
Věra	Věra	k1gFnSc1	Věra
Nosková	Nosková	k1gFnSc1	Nosková
<g/>
,	,	kIx,	,
Luděk	Luděk	k1gMnSc1	Luděk
Pekárek	Pekárek	k1gMnSc1	Pekárek
<g/>
,	,	kIx,	,
Vladimír	Vladimír	k1gMnSc1	Vladimír
Přibyl	Přibyl	k1gMnSc1	Přibyl
<g/>
,	,	kIx,	,
Lenka	Lenka	k1gFnSc1	Lenka
Přibylová	Přibylová	k1gFnSc1	Přibylová
a	a	k8xC	a
Čeněk	Čeněk	k1gMnSc1	Čeněk
Zlatník	zlatník	k1gMnSc1	zlatník
<g/>
.	.	kIx.	.
</s>
<s>
Předávání	předávání	k1gNnSc1	předávání
Bludných	bludný	k2eAgInPc2d1	bludný
balvanů	balvan	k1gInPc2	balvan
probíhá	probíhat	k5eAaImIp3nS	probíhat
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
posluchárně	posluchárna	k1gFnSc6	posluchárna
Matematicko-fyzikální	matematickoyzikální	k2eAgFnSc2d1	matematicko-fyzikální
fakulty	fakulta	k1gFnSc2	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
ceremoniály	ceremoniál	k1gInPc1	ceremoniál
předávání	předávání	k1gNnSc2	předávání
ceny	cena	k1gFnSc2	cena
probíhaly	probíhat	k5eAaImAgInP	probíhat
jak	jak	k6eAd1	jak
na	na	k7c6	na
Přírodovědecké	přírodovědecký	k2eAgFnSc6d1	Přírodovědecká
fakultě	fakulta	k1gFnSc6	fakulta
UK	UK	kA	UK
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
ČR	ČR	kA	ČR
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgNnSc1d1	samotné
slavnostní	slavnostní	k2eAgNnSc1d1	slavnostní
předávání	předávání	k1gNnSc1	předávání
je	být	k5eAaImIp3nS	být
pojato	pojmout	k5eAaPmNgNnS	pojmout
v	v	k7c6	v
recesistickém	recesistický	k2eAgMnSc6d1	recesistický
duchu	duch	k1gMnSc6	duch
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
komitétu	komitét	k1gInSc2	komitét
bývají	bývat	k5eAaImIp3nP	bývat
ověnčeni	ověnčen	k2eAgMnPc1d1	ověnčen
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
oblečeni	oblečen	k2eAgMnPc1d1	oblečen
v	v	k7c6	v
antických	antický	k2eAgFnPc6d1	antická
řízách	říza	k1gFnPc6	říza
<g/>
,	,	kIx,	,
doprovází	doprovázet	k5eAaImIp3nP	doprovázet
je	on	k3xPp3gFnPc4	on
tři	tři	k4xCgFnPc4	tři
Grácie	Grácie	k1gFnPc4	Grácie
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
z	z	k7c2	z
laureátů	laureát	k1gMnPc2	laureát
je	být	k5eAaImIp3nS	být
předem	předem	k6eAd1	předem
informován	informovat	k5eAaBmNgMnS	informovat
o	o	k7c4	o
udělení	udělení	k1gNnSc4	udělení
ceny	cena	k1gFnSc2	cena
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vyzván	vyzvat	k5eAaPmNgMnS	vyzvat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
ceremoniálu	ceremoniál	k1gInSc6	ceremoniál
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
se	se	k3xPyFc4	se
ale	ale	k9	ale
nedostaví	dostavit	k5eNaPmIp3nS	dostavit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
předání	předání	k1gNnSc6	předání
cen	cena	k1gFnPc2	cena
následuje	následovat	k5eAaImIp3nS	následovat
recesistická	recesistický	k2eAgFnSc1d1	recesistická
scénka	scénka	k1gFnSc1	scénka
<g/>
.	.	kIx.	.
</s>
<s>
Aleš	Aleš	k1gMnSc1	Aleš
Uhlíř	Uhlíř	k1gMnSc1	Uhlíř
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
článku	článek	k1gInSc6	článek
na	na	k7c6	na
serveru	server	k1gInSc6	server
Britské	britský	k2eAgFnSc2d1	britská
listy	lista	k1gFnSc2	lista
zejména	zejména	k9	zejména
teatrálnost	teatrálnost	k1gFnSc4	teatrálnost
ceremonie	ceremonie	k1gFnSc2	ceremonie
předávání	předávání	k1gNnSc2	předávání
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
důstojné	důstojný	k2eAgNnSc1d1	důstojné
vědců	vědec	k1gMnPc2	vědec
z	z	k7c2	z
výzkumných	výzkumný	k2eAgInPc2d1	výzkumný
ústavů	ústav	k1gInPc2	ústav
<g/>
,	,	kIx,	,
Akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
a	a	k8xC	a
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
rad	rada	k1gFnPc2	rada
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
inscenovat	inscenovat	k5eAaBmF	inscenovat
již	již	k9	již
na	na	k7c4	na
pohled	pohled	k1gInSc4	pohled
poněkud	poněkud	k6eAd1	poněkud
slabomyslné	slabomyslný	k2eAgNnSc4d1	slabomyslné
divadlo	divadlo	k1gNnSc4	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Novinář	novinář	k1gMnSc1	novinář
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
prezidentův	prezidentův	k2eAgMnSc1d1	prezidentův
mluvčí	mluvčí	k1gMnSc1	mluvčí
Petr	Petr	k1gMnSc1	Petr
Hájek	Hájek	k1gMnSc1	Hájek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
dostal	dostat	k5eAaPmAgMnS	dostat
zlatý	zlatý	k2eAgInSc4d1	zlatý
Bludný	bludný	k2eAgInSc4d1	bludný
balvan	balvan	k1gInSc4	balvan
za	za	k7c4	za
popírání	popírání	k1gNnSc4	popírání
evoluční	evoluční	k2eAgFnSc2d1	evoluční
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
označil	označit	k5eAaPmAgMnS	označit
celou	celý	k2eAgFnSc4d1	celá
soutěž	soutěž	k1gFnSc4	soutěž
za	za	k7c4	za
něco	něco	k3yInSc4	něco
"	"	kIx"	"
<g/>
vysoce	vysoce	k6eAd1	vysoce
hloupého	hloupý	k2eAgInSc2d1	hloupý
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Biolog	biolog	k1gMnSc1	biolog
Jan	Jan	k1gMnSc1	Jan
Kolář	Kolář	k1gMnSc1	Kolář
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
popularizaci	popularizace	k1gFnSc4	popularizace
vědy	věda	k1gFnSc2	věda
nemůže	moct	k5eNaImIp3nS	moct
negativní	negativní	k2eAgFnSc4d1	negativní
kampaň	kampaň	k1gFnSc4	kampaň
fungovat	fungovat	k5eAaImF	fungovat
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
důvodů	důvod	k1gInPc2	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Nejdříve	dříve	k6eAd3	dříve
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
udílením	udílení	k1gNnSc7	udílení
cen	cena	k1gFnPc2	cena
Sisyfos	Sisyfos	k1gMnSc1	Sisyfos
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
oceněným	oceněný	k2eAgMnSc7d1	oceněný
propagaci	propagace	k1gFnSc4	propagace
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
"	"	kIx"	"
<g/>
i	i	k8xC	i
negativní	negativní	k2eAgFnSc1d1	negativní
reklama	reklama	k1gFnSc1	reklama
je	být	k5eAaImIp3nS	být
reklama	reklama	k1gFnSc1	reklama
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
asymetrickou	asymetrický	k2eAgFnSc4d1	asymetrická
situaci	situace	k1gFnSc4	situace
v	v	k7c6	v
konfrontaci	konfrontace	k1gFnSc6	konfrontace
"	"	kIx"	"
<g/>
Sisyfos	Sisyfos	k1gMnSc1	Sisyfos
versus	versus	k7c1	versus
pseudověda	pseudověda	k1gFnSc1	pseudověda
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
veřejnost	veřejnost	k1gFnSc1	veřejnost
podle	podle	k7c2	podle
Koláře	Kolář	k1gMnSc2	Kolář
nemívá	mívat	k5eNaImIp3nS	mívat
ráda	rád	k2eAgFnSc1d1	ráda
<g/>
,	,	kIx,	,
když	když	k8xS	když
někdo	někdo	k3yInSc1	někdo
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
oponenty	oponent	k1gMnPc4	oponent
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
oficiální	oficiální	k2eAgFnSc2d1	oficiální
autority	autorita	k1gFnSc2	autorita
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
třetí	třetí	k4xOgInSc4	třetí
nedostatek	nedostatek	k1gInSc4	nedostatek
považuje	považovat	k5eAaImIp3nS	považovat
Kolář	Kolář	k1gMnSc1	Kolář
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sisyfos	Sisyfos	k1gMnSc1	Sisyfos
nenabízí	nabízet	k5eNaImIp3nS	nabízet
lákavější	lákavý	k2eAgFnSc4d2	lákavější
alternativu	alternativa	k1gFnSc4	alternativa
<g/>
,	,	kIx,	,
než	než	k8xS	než
představují	představovat	k5eAaImIp3nP	představovat
nevědecké	vědecký	k2eNgInPc4d1	nevědecký
myšlenkové	myšlenkový	k2eAgInPc4d1	myšlenkový
směry	směr	k1gInPc4	směr
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
nositelů	nositel	k1gMnPc2	nositel
Bludného	bludný	k2eAgInSc2d1	bludný
balvanu	balvan	k1gInSc2	balvan
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nositele	nositel	k1gMnPc4	nositel
Bludných	bludný	k2eAgInPc2d1	bludný
balvanů	balvan	k1gInPc2	balvan
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
bývalý	bývalý	k2eAgMnSc1d1	bývalý
český	český	k2eAgMnSc1d1	český
ministr	ministr	k1gMnSc1	ministr
Antonín	Antonín	k1gMnSc1	Antonín
Baudyš	Baudyš	k1gMnSc1	Baudyš
<g/>
,	,	kIx,	,
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
spisovatel	spisovatel	k1gMnSc1	spisovatel
Erich	Erich	k1gMnSc1	Erich
von	von	k1gInSc4	von
Däniken	Däniken	k2eAgInSc4d1	Däniken
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
psychotroniky	psychotronika	k1gFnSc2	psychotronika
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Rejdák	Rejdák	k1gMnSc1	Rejdák
nebo	nebo	k8xC	nebo
český	český	k2eAgMnSc1d1	český
herec	herec	k1gMnSc1	herec
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Dušek	Dušek	k1gMnSc1	Dušek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
organizací	organizace	k1gFnPc2	organizace
pak	pak	k6eAd1	pak
například	například	k6eAd1	například
tři	tři	k4xCgFnPc1	tři
nejvýznamnější	významný	k2eAgFnPc1d3	nejvýznamnější
české	český	k2eAgFnPc1d1	Česká
televizní	televizní	k2eAgFnPc1d1	televizní
stanice	stanice	k1gFnPc1	stanice
(	(	kIx(	(
<g/>
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
<g/>
,	,	kIx,	,
TV	TV	kA	TV
Nova	nova	k1gFnSc1	nova
a	a	k8xC	a
Prima	prima	k1gFnSc1	prima
TV	TV	kA	TV
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
redakce	redakce	k1gFnSc1	redakce
časopisu	časopis	k1gInSc2	časopis
Vesmír	vesmír	k1gInSc1	vesmír
nebo	nebo	k8xC	nebo
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
obchodu	obchod	k1gInSc2	obchod
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
