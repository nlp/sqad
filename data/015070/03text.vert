<s>
Čeřovka	Čeřovka	k1gFnSc1
</s>
<s>
Čeřovka	Čeřovka	k1gFnSc1
Rozhledna	rozhledna	k1gFnSc1
Milohlídka	Milohlídka	k1gFnSc1
na	na	k7c6
Čeřovce	Čeřovka	k1gFnSc6
</s>
<s>
Vrchol	vrchol	k1gInSc1
</s>
<s>
331	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
Prominence	prominence	k1gFnSc2
</s>
<s>
40	#num#	k4
m	m	kA
↓	↓	k?
Izolace	izolace	k1gFnSc1
</s>
<s>
1,2	1,2	k4
km	km	kA
→	→	k?
Zebín	Zebína	k1gFnPc2
Poloha	poloha	k1gFnSc1
Světadíl	světadíl	k1gInSc1
</s>
<s>
Evropa	Evropa	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc4
Pohoří	pohořet	k5eAaPmIp3nS
</s>
<s>
Jičínská	jičínský	k2eAgFnSc1d1
pahorkatina	pahorkatina	k1gFnSc1
/	/	kIx~
Turnovská	turnovský	k2eAgFnSc1d1
pahorkatina	pahorkatina	k1gFnSc1
/	/	kIx~
Jičínská	jičínský	k2eAgFnSc1d1
kotlina	kotlina	k1gFnSc1
/	/	kIx~
Úlibická	Úlibický	k2eAgFnSc1d1
tabule	tabule	k1gFnSc1
/	/	kIx~
Zebínská	Zebínský	k2eAgFnSc1d1
část	část	k1gFnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
26	#num#	k4
<g/>
′	′	k?
<g/>
41	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
15	#num#	k4
<g/>
°	°	k?
<g/>
21	#num#	k4
<g/>
′	′	k?
<g/>
37	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Čeřovka	Čeřovka	k1gFnSc1
</s>
<s>
Hornina	hornina	k1gFnSc1
</s>
<s>
jílovec	jílovec	k1gInSc1
<g/>
,	,	kIx,
slínovec	slínovec	k1gInSc1
<g/>
,	,	kIx,
čedič	čedič	k1gInSc1
<g/>
,	,	kIx,
brekcie	brekcie	k1gFnSc1
Povodí	povodí	k1gNnSc2
</s>
<s>
Cidlina	Cidlina	k1gFnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Čeřovka	Čeřovka	k1gFnSc1
(	(	kIx(
<g/>
331	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
vrch	vrch	k1gInSc4
v	v	k7c6
okrese	okres	k1gInSc6
Jičín	Jičín	k1gInSc1
Královéhradeckého	královéhradecký	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
,	,	kIx,
ležící	ležící	k2eAgFnSc1d1
při	při	k7c6
severním	severní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
města	město	k1gNnSc2
Jičín	Jičín	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
severní	severní	k2eAgFnSc6d1
části	část	k1gFnSc6
Valdického	valdický	k2eAgNnSc2d1
Předměstí	předměstí	k1gNnSc2
<g/>
,	,	kIx,
na	na	k7c6
katastrálním	katastrální	k2eAgNnSc6d1
území	území	k1gNnSc6
Jičín	Jičín	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
vrcholu	vrchol	k1gInSc2
kopce	kopec	k1gInSc2
stojí	stát	k5eAaImIp3nS
rozhledna	rozhledna	k1gFnSc1
zvaná	zvaný	k2eAgFnSc1d1
Milohlídka	Milohlídka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Geologie	geologie	k1gFnSc1
</s>
<s>
Z	z	k7c2
minerálů	minerál	k1gInPc2
se	se	k3xPyFc4
na	na	k7c6
lokalitě	lokalita	k1gFnSc6
vyskytuje	vyskytovat	k5eAaImIp3nS
amfibol	amfibol	k1gInSc4
<g/>
,	,	kIx,
augit	augit	k1gInSc4
<g/>
,	,	kIx,
hlízovitý	hlízovitý	k2eAgInSc4d1
kalcit	kalcit	k1gInSc4
<g/>
,	,	kIx,
magnetit	magnetit	k1gInSc4
<g/>
,	,	kIx,
titanomagnetit	titanomagnetit	k5eAaPmF
<g/>
,	,	kIx,
olivín	olivín	k1gInSc4
a	a	k8xC
zeolity	zeolit	k1gInPc4
jako	jako	k8xS,k8xC
například	například	k6eAd1
natrolit	natrolit	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Geomorfologické	geomorfologický	k2eAgNnSc1d1
zařazení	zařazení	k1gNnSc1
</s>
<s>
Vrch	vrch	k1gInSc1
náleží	náležet	k5eAaImIp3nS
do	do	k7c2
celku	celek	k1gInSc2
Jičínská	jičínský	k2eAgFnSc1d1
pahorkatina	pahorkatina	k1gFnSc1
<g/>
,	,	kIx,
podcelku	podcelka	k1gFnSc4
Turnovská	turnovský	k2eAgFnSc1d1
pahorkatina	pahorkatina	k1gFnSc1
<g/>
,	,	kIx,
okrsku	okrsek	k1gInSc6
Jičínská	jičínský	k2eAgFnSc1d1
kotlina	kotlina	k1gFnSc1
<g/>
,	,	kIx,
podokrsku	podokrska	k1gFnSc4
Úlibická	Úlibický	k2eAgFnSc1d1
tabule	tabule	k1gFnSc1
a	a	k8xC
Zebínské	Zebínský	k2eAgFnPc1d1
části	část	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mykologie	mykologie	k1gFnSc1
</s>
<s>
Z	z	k7c2
hub	houba	k1gFnPc2
roste	růst	k5eAaImIp3nS
na	na	k7c6
Čeřovce	Čeřovka	k1gFnSc6
například	například	k6eAd1
třepenitka	třepenitka	k1gFnSc1
svazčitá	svazčitý	k2eAgFnSc1d1
<g/>
,	,	kIx,
křehutka	křehutka	k1gFnSc1
vodomilná	vodomilný	k2eAgFnSc1d1
<g/>
,	,	kIx,
vláknice	vláknice	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Z	z	k7c2
jedlých	jedlý	k2eAgMnPc2d1
hřib	hřib	k1gInSc4
žlutomasý	žlutomasý	k2eAgInSc1d1
<g/>
,	,	kIx,
ucho	ucho	k1gNnSc1
Jidášovo	Jidášův	k2eAgNnSc1d1
<g/>
,	,	kIx,
pýchavka	pýchavka	k1gFnSc1
obecná	obecná	k1gFnSc1
<g/>
,	,	kIx,
pečárka	pečárka	k1gFnSc1
lesní	lesní	k2eAgFnSc1d1
<g/>
,	,	kIx,
pečárka	pečárka	k1gFnSc1
ovčí	ovčí	k2eAgFnSc1d1
<g/>
,	,	kIx,
špička	špička	k1gFnSc1
obecná	obecná	k1gFnSc1
<g/>
,	,	kIx,
penízovka	penízovka	k1gFnSc1
sametonohá	sametonohý	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vzácnější	vzácný	k2eAgFnPc4d2
houby	houba	k1gFnPc4
zastupuje	zastupovat	k5eAaImIp3nS
hřib	hřib	k1gInSc1
nachový	nachový	k2eAgInSc1d1
<g/>
,	,	kIx,
hřib	hřib	k1gInSc1
satan	satan	k1gInSc1
<g/>
,	,	kIx,
hřib	hřib	k1gInSc1
plavý	plavý	k2eAgInSc1d1
nebo	nebo	k8xC
muchomůrka	muchomůrka	k?
stroupkatá	stroupkatý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Květena	květena	k1gFnSc1
</s>
<s>
Většinu	většina	k1gFnSc4
plochy	plocha	k1gFnSc2
Čeřovky	Čeřovka	k1gFnSc2
tvoří	tvořit	k5eAaImIp3nS
les	les	k1gInSc1
jehož	jehož	k3xOyRp3gInSc7
základem	základ	k1gInSc7
je	být	k5eAaImIp3nS
dubohabřina	dubohabřina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavními	hlavní	k2eAgFnPc7d1
dřevinami	dřevina	k1gFnPc7
jsou	být	k5eAaImIp3nP
dub	dub	k1gInSc4
letní	letní	k2eAgInSc4d1
a	a	k8xC
habr	habr	k1gInSc4
obecný	obecný	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
severní	severní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
porost	porost	k1gInSc1
doplňuje	doplňovat	k5eAaImIp3nS
buk	buk	k1gInSc4
lesní	lesní	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
dalších	další	k2eAgFnPc2d1
listnatých	listnatý	k2eAgFnPc2d1
dřevin	dřevina	k1gFnPc2
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
lípa	lípa	k1gFnSc1
malolistá	malolistý	k2eAgFnSc1d1
<g/>
,	,	kIx,
jasan	jasan	k1gInSc1
ztepilý	ztepilý	k2eAgInSc1d1
<g/>
,	,	kIx,
javor	javor	k1gInSc1
babyka	babyka	k1gFnSc1
<g/>
,	,	kIx,
jilm	jilm	k1gInSc1
horský	horský	k2eAgInSc1d1
<g/>
,	,	kIx,
jilm	jilm	k1gInSc1
habrolistý	habrolistý	k2eAgInSc1d1
<g/>
,	,	kIx,
bříza	bříza	k1gFnSc1
bělokorá	bělokorý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jehličnaté	jehličnatý	k2eAgFnPc4d1
dřeviny	dřevina	k1gFnPc4
zastupuje	zastupovat	k5eAaImIp3nS
smrk	smrk	k1gInSc1
ztepilý	ztepilý	k2eAgInSc1d1
a	a	k8xC
modřín	modřín	k1gInSc1
opadavý	opadavý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c4
introdukované	introdukovaný	k2eAgFnPc4d1
dřeviny	dřevina	k1gFnPc4
patří	patřit	k5eAaImIp3nS
douglaska	douglaska	k1gFnSc1
tisolistá	tisolistý	k2eAgFnSc1d1
<g/>
,	,	kIx,
tsuga	tsuga	k1gFnSc1
kanadská	kanadský	k2eAgFnSc1d1
<g/>
,	,	kIx,
borovice	borovice	k1gFnSc1
černá	černý	k2eAgFnSc1d1
<g/>
,	,	kIx,
cypřišek	cypřišek	k1gInSc1
Lawsonův	Lawsonův	k2eAgInSc1d1
a	a	k8xC
invazivní	invazivní	k2eAgInSc1d1
trnovník	trnovník	k1gInSc1
akát	akát	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Keřové	keřový	k2eAgNnSc1d1
patro	patro	k1gNnSc1
zastupuje	zastupovat	k5eAaImIp3nS
bez	bez	k1gInSc4
černý	černý	k2eAgInSc4d1
<g/>
,	,	kIx,
pámelník	pámelník	k1gInSc4
bílý	bílý	k2eAgInSc4d1
<g/>
,	,	kIx,
brslen	brslen	k1gInSc4
evropský	evropský	k2eAgInSc4d1
či	či	k8xC
břečťan	břečťan	k1gInSc4
popínavý	popínavý	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s>
Bylinné	bylinný	k2eAgNnSc1d1
patro	patro	k1gNnSc1
tvoří	tvořit	k5eAaImIp3nS
sasanka	sasanka	k1gFnSc1
hajní	hajní	k2eAgFnSc1d1
<g/>
,	,	kIx,
pitulník	pitulník	k1gInSc1
žlutý	žlutý	k2eAgInSc1d1
<g/>
,	,	kIx,
lipnice	lipnice	k1gFnPc4
hajní	hajní	k2eAgFnPc4d1
<g/>
,	,	kIx,
kuklík	kuklík	k1gInSc1
městský	městský	k2eAgInSc1d1
<g/>
,	,	kIx,
hrachor	hrachor	k1gInSc1
černý	černý	k2eAgInSc1d1
<g/>
,	,	kIx,
hrachor	hrachor	k1gInSc1
jarní	jarní	k2eAgInSc1d1
<g/>
,	,	kIx,
strdivka	strdivka	k1gFnSc1
nící	nící	k1gFnSc1
<g/>
,	,	kIx,
kopretina	kopretina	k1gFnSc1
řimbaba	řimbaba	k1gFnSc1
<g/>
,	,	kIx,
ptačinec	ptačinec	k1gInSc1
hajní	hajní	k2eAgInSc1d1
či	či	k8xC
netýkavka	netýkavka	k1gFnSc1
malokvětá	malokvětý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Zvířena	zvířena	k1gFnSc1
</s>
<s>
Na	na	k7c6
Čeřovce	Čeřovka	k1gFnSc6
se	se	k3xPyFc4
vyskytuje	vyskytovat	k5eAaImIp3nS
jedenáct	jedenáct	k4xCc1
druhů	druh	k1gInPc2
ohrožených	ohrožený	k2eAgMnPc2d1
živočichů	živočich	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
hmyzu	hmyz	k1gInSc2
čmelák	čmelák	k1gMnSc1
luční	luční	k2eAgMnSc1d1
<g/>
,	,	kIx,
čmelák	čmelák	k1gMnSc1
rokytový	rokytový	k2eAgMnSc1d1
<g/>
,	,	kIx,
čmelák	čmelák	k1gMnSc1
zemní	zemní	k2eAgFnSc2d1
<g/>
,	,	kIx,
pačmelák	pačmelák	k1gMnSc1
panenský	panenský	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Saproxylický	Saproxylický	k2eAgInSc1d1
hmyz	hmyz	k1gInSc1
zastupuje	zastupovat	k5eAaImIp3nS
vzácný	vzácný	k2eAgMnSc1d1
kovařík	kovařík	k1gMnSc1
rezavý	rezavý	k2eAgMnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Z	z	k7c2
ohroženého	ohrožený	k2eAgNnSc2d1
ptactva	ptactvo	k1gNnSc2
na	na	k7c6
lokalitě	lokalita	k1gFnSc6
hnízdí	hnízdit	k5eAaImIp3nS
krutihlav	krutihlav	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
<g/>
,	,	kIx,
do	do	k7c2
prostoru	prostor	k1gInSc2
lesoparku	lesopark	k1gInSc2
zaletuje	zaletovat	k5eAaImIp3nS,k5eAaPmIp3nS
rorýs	rorýs	k1gMnSc1
obecný	obecný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
plazů	plaz	k1gMnPc2
byl	být	k5eAaImAgMnS
popsán	popsán	k2eAgMnSc1d1
slepýš	slepýš	k1gMnSc1
křehký	křehký	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lesoparku	lesopark	k1gInSc6
je	být	k5eAaImIp3nS
menší	malý	k2eAgFnSc1d2
populace	populace	k1gFnSc1
veverky	veverka	k1gFnSc2
obecné	obecný	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
minulosti	minulost	k1gFnSc6
byl	být	k5eAaImAgInS
na	na	k7c6
lokalitě	lokalita	k1gFnSc6
lesoparku	lesopark	k1gInSc2
zaznamenán	zaznamenán	k2eAgMnSc1d1
i	i	k8xC
výr	výr	k1gMnSc1
velký	velký	k2eAgMnSc1d1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
na	na	k7c4
Čeřovce	Čeřovec	k1gMnSc4
již	již	k6eAd1
nehnízdí	hnízdit	k5eNaImIp3nS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
Čeřovce	Čeřovka	k1gFnSc6
mají	mít	k5eAaImIp3nP
v	v	k7c6
některých	některý	k3yIgInPc6
dutinových	dutinový	k2eAgInPc6d1
stromech	strom	k1gInPc6
letní	letní	k2eAgFnSc2d1
kolonie	kolonie	k1gFnSc2
netopýři	netopýr	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
nalezen	nalezen	k2eAgMnSc1d1
uhynulý	uhynulý	k2eAgMnSc1d1
jedinec	jedinec	k1gMnSc1
netopýra	netopýr	k1gMnSc2
rezavého	rezavý	k2eAgMnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
dalších	další	k2eAgMnPc2d1
<g/>
,	,	kIx,
vzácnějších	vzácný	k2eAgMnPc2d2
živočichů	živočich	k1gMnPc2
jmenujme	jmenovat	k5eAaImRp1nP,k5eAaBmRp1nP
tesaříka	tesařík	k1gMnSc4
pilunu	piluna	k1gFnSc4
<g/>
,	,	kIx,
puštíka	puštík	k1gMnSc4
obecného	obecný	k2eAgMnSc4d1
<g/>
,	,	kIx,
žlunu	žluna	k1gFnSc4
šedou	šedý	k2eAgFnSc4d1
nebo	nebo	k8xC
datla	datel	k1gMnSc2
černého	černé	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lesní	lesní	k2eAgNnSc1d1
hospodaření	hospodaření	k1gNnSc1
</s>
<s>
Les	les	k1gInSc1
v	v	k7c6
lesoparku	lesopark	k1gInSc6
Čeřovka	Čeřovka	k1gFnSc1
je	být	k5eAaImIp3nS
pozůstatkem	pozůstatek	k1gInSc7
starobylého	starobylý	k2eAgNnSc2d1
lesního	lesní	k2eAgNnSc2d1
hospodaření	hospodaření	k1gNnSc2
-	-	kIx~
tzv.	tzv.	kA
pařeziny	pařezina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
různověký	různověký	k2eAgInSc4d1
les	les	k1gInSc4
s	s	k7c7
polykormony	polykormon	k1gInPc7
<g/>
,	,	kIx,
dendrotelmami	dendrotelma	k1gFnPc7
a	a	k8xC
výstavky	výstavek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
lese	les	k1gInSc6
město	město	k1gNnSc1
Jičín	Jičín	k1gInSc1
zachovává	zachovávat	k5eAaImIp3nS
doupné	doupný	k2eAgInPc4d1
stromy	strom	k1gInPc4
označené	označený	k2eAgInPc4d1
v	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
modrým	modrý	k2eAgInSc7d1
trojúhelníkem	trojúhelník	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Pro	pro	k7c4
vzácné	vzácný	k2eAgMnPc4d1
brouky	brouk	k1gMnPc4
jsou	být	k5eAaImIp3nP
v	v	k7c6
lokalitě	lokalita	k1gFnSc6
zachovávány	zachováván	k2eAgInPc4d1
padlé	padlý	k2eAgInPc4d1
kmeny	kmen	k1gInPc4
stomů	stom	k1gInPc2
<g/>
,	,	kIx,
mrtvé	mrtvý	k2eAgNnSc1d1
dřevo	dřevo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
V	v	k7c6
mladší	mladý	k2eAgFnSc6d2
době	doba	k1gFnSc6
kamenné	kamenný	k2eAgNnSc1d1
(	(	kIx(
<g/>
6000	#num#	k4
<g/>
–	–	k?
<g/>
2000	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
se	se	k3xPyFc4
využívaly	využívat	k5eAaPmAgInP,k5eAaImAgInP
rohovce	rohovec	k1gInPc1
z	z	k7c2
Čeřovky	Čeřovka	k1gFnSc2
jako	jako	k8xC,k8xS
zdroj	zdroj	k1gInSc1
štípané	štípaný	k2eAgFnSc2d1
industrie	industrie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
První	první	k4xOgFnSc1
písemná	písemný	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
porostlině	porostlina	k1gFnSc6
a	a	k8xC
háji	háj	k1gInSc6
na	na	k7c6
Čeřovce	Čeřovka	k1gFnSc6
z	z	k7c2
roku	rok	k1gInSc2
1393	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čeřovku	Čeřovka	k1gFnSc4
vlastnil	vlastnit	k5eAaImAgMnS
Šimon	Šimon	k1gMnSc1
z	z	k7c2
Čeřova	Čeřov	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
druhé	druhý	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
má	mít	k5eAaImIp3nS
v	v	k7c6
držení	držení	k1gNnSc6
les	les	k1gInSc1
na	na	k7c6
Čeřovce	Čeřovka	k1gFnSc6
město	město	k1gNnSc1
Jičín	Jičín	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Od	od	k7c2
roku	rok	k1gInSc2
1843	#num#	k4
stojí	stát	k5eAaImIp3nS
na	na	k7c6
temeni	temeno	k1gNnSc6
kopce	kopec	k1gInSc2
Čeřovka	Čeřovka	k1gFnSc1
rozhledna	rozhledna	k1gFnSc1
Milohlídka	Milohlídka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nechal	nechat	k5eAaPmAgMnS
ji	on	k3xPp3gFnSc4
vybudovat	vybudovat	k5eAaPmF
krajský	krajský	k2eAgMnSc1d1
hejtman	hejtman	k1gMnSc1
Hansgirg	Hansgirg	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1
v	v	k7c6
padesátých	padesátý	k4xOgNnPc6
letech	léto	k1gNnPc6
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
otevřen	otevřít	k5eAaPmNgInS
na	na	k7c6
jihozápadním	jihozápadní	k2eAgInSc6d1
svahu	svah	k1gInSc6
kopce	kopec	k1gInSc2
čedičový	čedičový	k2eAgInSc4d1
lom	lom	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Těžba	těžba	k1gFnSc1
byla	být	k5eAaImAgFnS
ukončena	ukončit	k5eAaPmNgFnS
do	do	k7c2
třiceti	třicet	k4xCc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Mezi	mezi	k7c7
lety	let	k1gInPc7
1887	#num#	k4
<g/>
–	–	k?
<g/>
1889	#num#	k4
změnil	změnit	k5eAaPmAgInS
místní	místní	k2eAgInSc1d1
okrašlovací	okrašlovací	k2eAgInSc1d1
spolek	spolek	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gFnSc7
členkou	členka	k1gFnSc7
byla	být	k5eAaImAgFnS
i	i	k9
dcera	dcera	k1gFnSc1
Boženy	Božena	k1gFnSc2
Němcové	Němcová	k1gFnSc2
–	–	k?
Theodora	Theodora	k1gFnSc1
<g/>
,	,	kIx,
prostor	prostor	k1gInSc1
Čeřovky	Čeřovka	k1gFnSc2
na	na	k7c4
městský	městský	k2eAgInSc4d1
přírodní	přírodní	k2eAgInSc4d1
park	park	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
1889	#num#	k4
byl	být	k5eAaImAgInS
na	na	k7c6
Čeřovce	Čeřovka	k1gFnSc6
odhalen	odhalit	k5eAaPmNgInS
pomník	pomník	k1gInSc1
spisovatele	spisovatel	k1gMnSc2
a	a	k8xC
novináře	novinář	k1gMnSc2
Antonína	Antonín	k1gMnSc2
Štraucha	Štrauch	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Roku	rok	k1gInSc2
1908	#num#	k4
byl	být	k5eAaImAgInS
vybudován	vybudovat	k5eAaPmNgInS
pod	pod	k7c7
Milohlídkou	Milohlídka	k1gFnSc7
vodojem	vodojem	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
zásobuje	zásobovat	k5eAaImIp3nS
vodou	voda	k1gFnSc7
přilehlou	přilehlý	k2eAgFnSc4d1
zástavbu	zástavba	k1gFnSc4
a	a	k8xC
okresní	okresní	k2eAgFnSc4d1
nemocnici	nemocnice	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Roku	rok	k1gInSc2
1915	#num#	k4
byla	být	k5eAaImAgFnS
k	k	k7c3
jezírku	jezírko	k1gNnSc3
na	na	k7c6
Čeřovce	Čeřovka	k1gFnSc6
umístěna	umístěn	k2eAgFnSc1d1
socha	socha	k1gFnSc1
Neptuna	Neptun	k1gMnSc2
s	s	k7c7
trojzubcem	trojzubec	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
válce	válka	k1gFnSc6
se	se	k3xPyFc4
ztratila	ztratit	k5eAaPmAgFnS
neznámo	neznámo	k6eAd1
kam	kam	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Za	za	k7c2
druhé	druhý	k4xOgFnSc2
světové	světový	k2eAgFnSc2d1
války	válka	k1gFnSc2
byl	být	k5eAaImAgInS
do	do	k7c2
prostoru	prostor	k1gInSc2
kolem	kolem	k7c2
rozhledny	rozhledna	k1gFnSc2
zakázán	zakázat	k5eAaPmNgInS
vstup	vstup	k1gInSc1
a	a	k8xC
na	na	k7c6
vrcholu	vrchol	k1gInSc6
Milohlídky	Milohlídka	k1gFnSc2
byl	být	k5eAaImAgInS
namontován	namontovat	k5eAaPmNgInS
triangulační	triangulační	k2eAgInSc1d1
stožár	stožár	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vojáci	voják	k1gMnPc1
Wehrmachtu	wehrmacht	k1gInSc2
koncem	koncem	k7c2
války	válka	k1gFnSc2
sledovali	sledovat	k5eAaImAgMnP
z	z	k7c2
rozhledny	rozhledna	k1gFnSc2
letecký	letecký	k2eAgInSc4d1
provoz	provoz	k1gInSc4
spojenců	spojenec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
válce	válka	k1gFnSc6
byla	být	k5eAaImAgFnS
vybudována	vybudovat	k5eAaPmNgFnS
na	na	k7c6
okraji	okraj	k1gInSc6
Čeřovky	Čeřovka	k1gFnSc2
tzv.	tzv.	kA
Ruská	ruský	k2eAgFnSc1d1
mohyla	mohyla	k1gFnSc1
<g/>
,	,	kIx,
hrob	hrob	k1gInSc1
padlých	padlý	k2eAgMnPc2d1
osvoboditelů	osvoboditel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1959	#num#	k4
byla	být	k5eAaImAgFnS
k	k	k7c3
stému	stý	k4xOgNnSc3
výročí	výročí	k1gNnSc3
narození	narození	k1gNnSc2
instalována	instalovat	k5eAaBmNgFnS
na	na	k7c4
Čeřovku	Čeřovka	k1gFnSc4
busta	busta	k1gFnSc1
hudebního	hudební	k2eAgMnSc2d1
skladatele	skladatel	k1gMnSc2
J.	J.	kA
B.	B.	kA
Foerstera	Foerster	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2015	#num#	k4
probíhá	probíhat	k5eAaImIp3nS
v	v	k7c6
na	na	k7c6
Čeřovce	Čeřovka	k1gFnSc6
každý	každý	k3xTgInSc4
rok	rok	k1gInSc4
hudební	hudební	k2eAgInSc1d1
festival	festival	k1gInSc1
Propadák	propadák	k1gInSc1
a	a	k8xC
cyklokrosový	cyklokrosový	k2eAgInSc1d1
závod	závod	k1gInSc1
Toi	Toi	k1gFnSc2
Toi	Toi	k1gFnSc2
Cup	cup	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červenci	červenec	k1gInSc6
2017	#num#	k4
byla	být	k5eAaImAgFnS
u	u	k7c2
vstupu	vstup	k1gInSc2
do	do	k7c2
prostoru	prostor	k1gInSc2
bývalého	bývalý	k2eAgInSc2d1
lomu	lom	k1gInSc2
umístěna	umístit	k5eAaPmNgFnS
informační	informační	k2eAgFnSc1d1
tabule	tabule	k1gFnSc1
o	o	k7c6
geologii	geologie	k1gFnSc6
<g/>
,	,	kIx,
mykologii	mykologie	k1gFnSc6
<g/>
,	,	kIx,
květeně	květena	k1gFnSc6
<g/>
,	,	kIx,
zvířeně	zvířena	k1gFnSc6
a	a	k8xC
historii	historie	k1gFnSc6
lokality	lokalita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Rozhledna	rozhledna	k1gFnSc1
Milohlídka	Milohlídka	k1gFnSc1
</s>
<s>
Na	na	k7c6
temeni	temeno	k1gNnSc6
nejmenšího	malý	k2eAgInSc2d3
kopce	kopec	k1gInSc2
v	v	k7c6
okolí	okolí	k1gNnSc6
města	město	k1gNnSc2
Jičína	Jičín	k1gInSc2
se	se	k3xPyFc4
vypíná	vypínat	k5eAaImIp3nS
nejstarší	starý	k2eAgFnSc1d3
rozhledna	rozhledna	k1gFnSc1
v	v	k7c6
Českém	český	k2eAgInSc6d1
ráji	ráj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
profesora	profesor	k1gMnSc2
Úlehly	Úlehly	k1gMnSc2
byla	být	k5eAaImAgFnS
rozhledna	rozhledna	k1gFnSc1
Milohlídka	Milohlídka	k1gFnSc1
postavena	postaven	k2eAgFnSc1d1
roku	rok	k1gInSc2
1844	#num#	k4
(	(	kIx(
<g/>
nikoliv	nikoliv	k9
1843	#num#	k4
<g/>
)	)	kIx)
z	z	k7c2
darů	dar	k1gInPc2
jičínských	jičínský	k2eAgMnPc2d1
občanů	občan	k1gMnPc2
uprostřed	uprostřed	k7c2
obecního	obecní	k2eAgInSc2d1
lesa	les	k1gInSc2
na	na	k7c6
vrchu	vrch	k1gInSc6
Čeřovka	Čeřovka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Podnět	podnět	k1gInSc1
k	k	k7c3
jejímu	její	k3xOp3gNnSc3
postavení	postavení	k1gNnSc3
dal	dát	k5eAaPmAgInS
tehdejší	tehdejší	k2eAgMnSc1d1
krajský	krajský	k2eAgMnSc1d1
hejtman	hejtman	k1gMnSc1
Josef	Josef	k1gMnSc1
Hansgirg	Hansgirg	k1gMnSc1
a	a	k8xC
na	na	k7c6
stavbě	stavba	k1gFnSc6
pracovali	pracovat	k5eAaImAgMnP
ti	ten	k3xDgMnPc1
nejchudší	chudý	k2eAgMnPc1d3
lidé	člověk	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnPc1
výstavba	výstavba	k1gFnSc1
tak	tak	k6eAd1
měla	mít	k5eAaImAgFnS
i	i	k9
sociální	sociální	k2eAgInSc4d1
rozměr	rozměr	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhledna	rozhledna	k1gFnSc1
je	být	k5eAaImIp3nS
postavena	postaven	k2eAgFnSc1d1
v	v	k7c6
novogotickém	novogotický	k2eAgInSc6d1
stavebním	stavební	k2eAgInSc6d1
slohu	sloh	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
sloužila	sloužit	k5eAaImAgFnS
rozhledna	rozhledna	k1gFnSc1
jako	jako	k8xS,k8xC
cíl	cíl	k1gInSc1
romantických	romantický	k2eAgFnPc2d1
vycházek	vycházka	k1gFnPc2
místních	místní	k2eAgMnPc2d1
občanů	občan	k1gMnPc2
<g/>
,	,	kIx,
protože	protože	k8xS
tehdy	tehdy	k6eAd1
byl	být	k5eAaImAgInS
ještě	ještě	k9
vrch	vrch	k1gInSc4
Čeřovka	Čeřovka	k1gFnSc1
mimo	mimo	k7c4
zástavbu	zástavba	k1gFnSc4
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
Čeřovky	Čeřovka	k1gFnSc2
se	se	k3xPyFc4
rozprostírala	rozprostírat	k5eAaImAgFnS
pole	pole	k1gNnSc2
<g/>
,	,	kIx,
louky	louka	k1gFnPc4
a	a	k8xC
vojenské	vojenský	k2eAgNnSc4d1
cvičiště	cvičiště	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dnes	dnes	k6eAd1
již	již	k6eAd1
dosahuje	dosahovat	k5eAaImIp3nS
zástavba	zástavba	k1gFnSc1
Jičína	Jičín	k1gInSc2
vrchu	vrch	k1gInSc2
Čeřovka	Čeřovka	k1gFnSc1
a	a	k8xC
stromy	strom	k1gInPc1
rozhlednu	rozhledna	k1gFnSc4
přerostly	přerůst	k5eAaPmAgInP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Milohlídka	Milohlídka	k1gFnSc1
má	mít	k5eAaImIp3nS
pouhých	pouhý	k2eAgInPc2d1
8	#num#	k4
m	m	kA
a	a	k8xC
na	na	k7c4
vrcholovou	vrcholový	k2eAgFnSc4d1
plošinu	plošina	k1gFnSc4
vede	vést	k5eAaImIp3nS
60	#num#	k4
schodů	schod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
má	mít	k5eAaImIp3nS
svůj	svůj	k3xOyFgInSc4
důvod	důvod	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
prostoru	prostor	k1gInSc6
Čeřovky	Čeřovka	k1gFnSc2
se	se	k3xPyFc4
rozprostíral	rozprostírat	k5eAaImAgInS
obecní	obecní	k2eAgInSc1d1
pařezinový	pařezinový	k2eAgInSc1d1
les	les	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
měl	mít	k5eAaImAgInS
podobu	podoba	k1gFnSc4
nízkého	nízký	k2eAgInSc2d1
křovinatého	křovinatý	k2eAgInSc2d1
lesa	les	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
těchto	tento	k3xDgInPc2
důvodů	důvod	k1gInPc2
bylo	být	k5eAaImAgNnS
z	z	k7c2
rozhledny	rozhledna	k1gFnSc2
vidět	vidět	k5eAaImF
do	do	k7c2
okolí	okolí	k1gNnSc2
města	město	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
průběhu	průběh	k1gInSc6
let	léto	k1gNnPc2
se	se	k3xPyFc4
změnil	změnit	k5eAaPmAgInS
status	status	k1gInSc1
obecního	obecní	k2eAgInSc2d1
lesa	les	k1gInSc2
na	na	k7c4
lesopark	lesopark	k1gInSc4
<g/>
,	,	kIx,
stromy	strom	k1gInPc4
vyrostly	vyrůst	k5eAaPmAgFnP
a	a	k8xC
dnes	dnes	k6eAd1
je	být	k5eAaImIp3nS
výhled	výhled	k1gInSc4
již	již	k6eAd1
pouze	pouze	k6eAd1
jihozápadním	jihozápadní	k2eAgInSc7d1
směrem	směr	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
rozhledny	rozhledna	k1gFnSc2
lze	lze	k6eAd1
spatřit	spatřit	k5eAaPmF
rybník	rybník	k1gInSc4
Kníže	kníže	k1gMnSc1
<g/>
,	,	kIx,
Holínské	Holínský	k2eAgNnSc1d1
předměstí	předměstí	k1gNnSc1
města	město	k1gNnSc2
Jičína	Jičín	k1gInSc2
<g/>
,	,	kIx,
zříceninu	zřícenina	k1gFnSc4
hradu	hrad	k1gInSc2
Veliš	Veliš	k1gInSc4
s	s	k7c7
triangulačním	triangulační	k2eAgInSc7d1
bodem	bod	k1gInSc7
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
Šlikova	Šlikův	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
s	s	k7c7
barokní	barokní	k2eAgFnSc7d1
sýpkou	sýpka	k1gFnSc7
s	s	k7c7
bílou	bílý	k2eAgFnSc7d1
střechou	střecha	k1gFnSc7
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
Březina	Březina	k1gMnSc1
<g/>
,	,	kIx,
táhlý	táhlý	k2eAgMnSc1d1
Velišský	Velišský	k2eAgMnSc1d1
hřbet	hřbet	k1gMnSc1
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
Ostružno	Ostružno	k6eAd1
s	s	k7c7
dvouvěžovým	dvouvěžový	k2eAgInSc7d1
kostelem	kostel	k1gInSc7
Povýšení	povýšení	k1gNnSc2
svatého	svatý	k2eAgInSc2d1
Kříže	kříž	k1gInSc2
<g/>
,	,	kIx,
zalesněný	zalesněný	k2eAgInSc1d1
vrch	vrch	k1gInSc1
a	a	k8xC
současně	současně	k6eAd1
Přírodní	přírodní	k2eAgFnSc4d1
památku	památka	k1gFnSc4
Svatá	svatý	k2eAgFnSc1d1
Anna	Anna	k1gFnSc1
<g/>
,	,	kIx,
obec	obec	k1gFnSc1
Holín	Holína	k1gFnPc2
a	a	k8xC
Prachovské	prachovský	k2eAgFnSc2d1
skály	skála	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Rozhledna	rozhledna	k1gFnSc1
Milohlídka	Milohlídka	k1gFnSc1
je	být	k5eAaImIp3nS
i	i	k9
předmětem	předmět	k1gInSc7
diskuzí	diskuze	k1gFnPc2
a	a	k8xC
sporů	spor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
železnou	železný	k2eAgFnSc7d1
pravidelností	pravidelnost	k1gFnSc7
několika	několik	k4yIc2
let	léto	k1gNnPc2
se	se	k3xPyFc4
v	v	k7c6
Jičíně	Jičín	k1gInSc6
obnovuje	obnovovat	k5eAaImIp3nS
diskuze	diskuze	k1gFnPc4
a	a	k8xC
vedou	vést	k5eAaImIp3nP
se	se	k3xPyFc4
spory	spor	k1gInPc1
o	o	k7c4
výhled	výhled	k1gInSc4
z	z	k7c2
rozhledny	rozhledna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedni	jeden	k4xCgMnPc1
dávají	dávat	k5eAaImIp3nP
přednost	přednost	k1gFnSc4
výhledu	výhled	k1gInSc2
z	z	k7c2
rozhledny	rozhledna	k1gFnSc2
<g/>
,	,	kIx,
druzí	druhý	k4xOgMnPc1
lesoparku	lesopark	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Faktem	faktum	k1gNnSc7
ale	ale	k9
zůstává	zůstávat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
rozhledna	rozhledna	k1gFnSc1
i	i	k8xC
lesopark	lesopark	k1gInSc1
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
funkční	funkční	k2eAgInSc1d1
celek	celek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Milohlídka	Milohlídka	k1gFnSc1
je	být	k5eAaImIp3nS
cílem	cíl	k1gInSc7
turistů	turist	k1gMnPc2
<g/>
,	,	kIx,
lesopark	lesopark	k1gInSc4
místem	místem	k6eAd1
relaxace	relaxace	k1gFnSc2
a	a	k8xC
oddychu	oddych	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přístup	přístup	k1gInSc1
</s>
<s>
Automobilem	automobil	k1gInSc7
se	se	k3xPyFc4
dá	dát	k5eAaPmIp3nS
přijet	přijet	k5eAaPmF
přímo	přímo	k6eAd1
pod	pod	k7c4
kopec	kopec	k1gInSc4
a	a	k8xC
odtud	odtud	k6eAd1
pěšky	pěšky	k6eAd1
po	po	k7c6
modré	modrý	k2eAgFnSc6d1
turistické	turistický	k2eAgFnSc6d1
značce	značka	k1gFnSc6
až	až	k9
na	na	k7c4
vrchol	vrchol	k1gInSc4
k	k	k7c3
rozhledně	rozhledna	k1gFnSc3
<g/>
,	,	kIx,
nebo	nebo	k8xC
využít	využít	k5eAaPmF
jičínskou	jičínský	k2eAgFnSc4d1
MHD	MHD	kA
<g/>
,	,	kIx,
vystoupit	vystoupit	k5eAaPmF
ve	v	k7c6
Foersterově	Foersterův	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
na	na	k7c6
zastávce	zastávka	k1gFnSc6
Jičín	Jičín	k1gInSc1
<g/>
,	,	kIx,
Čeřovka	Čeřovka	k1gFnSc1
a	a	k8xC
zbylých	zbylý	k2eAgNnPc2d1
250	#num#	k4
metrů	metr	k1gInPc2
k	k	k7c3
Čeřovce	Čeřovka	k1gFnSc3
dojít	dojít	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
BALATKA	balatka	k1gFnSc1
<g/>
,	,	kIx,
Břetislav	Břetislav	k1gMnSc1
<g/>
;	;	kIx,
KALVODA	Kalvoda	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geomorfologické	geomorfologický	k2eAgInPc1d1
členění	členění	k1gNnSc4
reliéfu	reliéf	k1gInSc2
Čech	Čechy	k1gFnPc2
=	=	kIx~
Geomorphological	Geomorphological	k1gFnSc1
regionalization	regionalization	k1gInSc1
of	of	k?
the	the	k?
relief	relief	k1gInSc1
of	of	k?
Bohemia	bohemia	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Kartografie	kartografie	k1gFnSc1
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
.	.	kIx.
80	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7011	#num#	k4
<g/>
-	-	kIx~
<g/>
913	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
OCLC	OCLC	kA
1001471218	#num#	k4
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Čeřovka	Čeřovka	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geopark	Geopark	k1gInSc1
Český	český	k2eAgInSc1d1
ráj	ráj	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
KOŠŤÁKOVÁ	KOŠŤÁKOVÁ	kA
<g/>
,	,	kIx,
Veronika	Veronika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pečárkotvaré	Pečárkotvarý	k2eAgFnPc4d1
<g/>
,	,	kIx,
holubinkotvaré	holubinkotvarý	k2eAgFnPc4d1
a	a	k8xC
jejich	jejich	k3xOp3gNnSc2
využití	využití	k1gNnSc2
ve	v	k7c6
školní	školní	k2eAgFnSc6d1
praxi	praxe	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
2009	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
137	#num#	k4
s.	s.	k?
Diplomová	diplomový	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pedagogická	pedagogický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Univerzity	univerzita	k1gFnSc2
Karlovy	Karlův	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedoucí	vedoucí	k2eAgFnSc1d1
práce	práce	k1gFnSc1
Petr	Petr	k1gMnSc1
Dostál	Dostál	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
MERTLIK	MERTLIK	kA
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Saproxylické	Saproxylický	k2eAgInPc1d1
druhy	druh	k1gInPc1
kovaříků	kovařík	k1gMnPc2
(	(	kIx(
<g/>
Coleoptera	Coleopter	k1gMnSc2
<g/>
:	:	kIx,
Elateridae	Elaterida	k1gMnSc2
<g/>
)	)	kIx)
na	na	k7c6
území	území	k1gNnSc6
východních	východní	k2eAgFnPc2d1
Čech	Čechy	k1gFnPc2
<g/>
,	,	kIx,
s	s	k7c7
přehledem	přehled	k1gInSc7
biotopů	biotop	k1gInPc2
druhů	druh	k1gInPc2
osídlujících	osídlující	k2eAgInPc2d1
dubové	dubový	k2eAgInPc4d1
lesy	les	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Elateridarium	Elateridarium	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Únor	únor	k1gInSc1
2017	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
11	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
17	#num#	k4
<g/>
–	–	k?
<g/>
110	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1802	#num#	k4
<g/>
-	-	kIx~
<g/>
4858	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
ŠORF	ŠORF	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zoologický	zoologický	k2eAgInSc4d1
výzkum	výzkum	k1gInSc4
lesoparku	lesopark	k1gInSc2
Čeřovka	Čeřovka	k1gFnSc1
v	v	k7c6
Jičíně	Jičín	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
,	,	kIx,
2017	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
69	#num#	k4
s.	s.	k?
Bakalářská	bakalářský	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přírodovědecká	přírodovědecký	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
Univerzity	univerzita	k1gFnSc2
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedoucí	vedoucí	k2eAgFnSc1d1
práce	práce	k1gFnSc1
Michal	Michal	k1gMnSc1
Andreas	Andreas	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
KUČERA	Kučera	k1gMnSc1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Revitalizace	revitalizace	k1gFnSc1
lesoparku	lesopark	k1gInSc2
Čeřovka	Čeřovka	k1gFnSc1
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
citlivá	citlivý	k2eAgFnSc1d1
<g/>
,	,	kIx,
broukům	brouk	k1gMnPc3
nechá	nechat	k5eAaPmIp3nS
mrtvé	mrtvý	k2eAgNnSc1d1
dřevo	dřevo	k1gNnSc1
<g/>
.	.	kIx.
iDnes	iDnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-07-19	2017-07-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
JIRÁSEK	Jirásek	k1gMnSc1
<g/>
,	,	kIx,
Jakub	Jakub	k1gMnSc1
<g/>
;	;	kIx,
SIVEK	sivka	k1gFnPc2
<g/>
,	,	kIx,
Martin	Martin	k1gMnSc1
<g/>
;	;	kIx,
LÁZNIČKA	Láznička	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Historie	historie	k1gFnSc1
těžby	těžba	k1gFnSc2
a	a	k8xC
využití	využití	k1gNnSc2
nerostů	nerost	k1gInPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Technická	technický	k2eAgFnSc1d1
univerzita	univerzita	k1gFnSc1
Ostrava	Ostrava	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Rozhledna	rozhledna	k1gFnSc1
Milohlídka	Milohlídka	k1gFnSc1
–	–	k?
Čeřovka	Čeřovka	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Město	město	k1gNnSc1
Jičín	Jičín	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Jičín	Jičín	k1gInSc1
–	–	k?
pomník	pomník	k1gInSc4
Antonína	Antonín	k1gMnSc2
Štraucha	Štrauch	k1gMnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Interregion	Interregion	k1gInSc1
Jičín	Jičín	k1gInSc4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Plán	plán	k1gInSc1
rozvoje	rozvoj	k1gInSc2
vodovodů	vodovod	k1gInPc2
a	a	k8xC
kanalizací	kanalizace	k1gFnPc2
na	na	k7c6
území	území	k1gNnSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
[	[	kIx(
<g/>
PDF	PDF	kA
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ministerstvo	ministerstvo	k1gNnSc1
zemědělství	zemědělství	k1gNnSc1
<g/>
,	,	kIx,
2007	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
23	#num#	k4
<g/>
–	–	k?
<g/>
24	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Socha	Socha	k1gMnSc1
Neptuna	Neptun	k1gMnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jičínská	jičínský	k2eAgFnSc1d1
beseda	beseda	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Hrob	hrob	k1gInSc4
vojáků	voják	k1gMnPc2
Rudé	rudý	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolek	spolek	k1gInSc1
pro	pro	k7c4
vojenská	vojenský	k2eAgNnPc4d1
pietní	pietní	k2eAgNnPc4d1
místa	místo	k1gNnPc4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zhanobená	zhanobený	k2eAgFnSc1d1
busta	busta	k1gFnSc1
J.	J.	kA
B.	B.	kA
Foerstera	Foerster	k1gMnSc2
celkově	celkově	k6eAd1
chátrá	chátrat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dobré	dobrý	k2eAgFnPc4d1
zprávy	zpráva	k1gFnPc4
z	z	k7c2
Českého	český	k2eAgInSc2d1
ráje	ráj	k1gInSc2
a	a	k8xC
okolí	okolí	k1gNnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2016-03-14	2016-03-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Tajemná	tajemný	k2eAgFnSc1d1
věž	věž	k1gFnSc1
Milohlídka	Milohlídka	k1gFnSc1
působí	působit	k5eAaImIp3nS
nad	nad	k7c7
jičínskou	jičínský	k2eAgFnSc7d1
rozeklanou	rozeklaný	k2eAgFnSc7d1
skálou	skála	k1gFnSc7
jako	jako	k8xS,k8xC
zjevení	zjevení	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-11-02	2017-11-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SÁL	sál	k1gInSc1
<g/>
,	,	kIx,
Radovan	Radovan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nastaví	nastavět	k5eAaBmIp3nS,k5eAaPmIp3nS
rozhlednu	rozhledna	k1gFnSc4
na	na	k7c6
jičínské	jičínský	k2eAgFnSc6d1
Čeřovce	Čeřovka	k1gFnSc6
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jičínský	jičínský	k2eAgInSc1d1
deník	deník	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2008-06-15	2008-06-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Mapy	mapa	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
|	|	kIx~
Geografie	geografie	k1gFnSc1
</s>
