<p>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
Komenského	Komenský	k1gMnSc2	Komenský
v	v	k7c6	v
Přerově	Přerov	k1gInSc6	Přerov
je	být	k5eAaImIp3nS	být
muzeum	muzeum	k1gNnSc1	muzeum
v	v	k7c6	v
Přerově	Přerov	k1gInSc6	Přerov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
zámku	zámek	k1gInSc6	zámek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Kořeny	kořen	k1gInPc1	kořen
muzea	muzeum	k1gNnSc2	muzeum
můžeme	moct	k5eAaImIp1nP	moct
najít	najít	k5eAaPmF	najít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1888	[number]	k4	1888
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
zásluhou	zásluha	k1gFnSc7	zásluha
Františka	František	k1gMnSc2	František
Slaměníka	Slaměník	k1gMnSc2	Slaměník
vzniká	vznikat	k5eAaImIp3nS	vznikat
Muzeum	muzeum	k1gNnSc4	muzeum
Komenského	Komenský	k1gMnSc2	Komenský
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
založeno	založit	k5eAaPmNgNnS	založit
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
svého	svůj	k3xOyFgInSc2	svůj
druhu	druh	k1gInSc2	druh
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úspěšné	úspěšný	k2eAgFnSc6d1	úspěšná
národopisné	národopisný	k2eAgFnSc3d1	národopisná
a	a	k8xC	a
průmyslové	průmyslový	k2eAgFnSc3d1	průmyslová
výstavě	výstava	k1gFnSc3	výstava
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
v	v	k7c6	v
Přerově	Přerov	k1gInSc6	Přerov
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1902	[number]	k4	1902
i	i	k8xC	i
Městské	městský	k2eAgNnSc4d1	Městské
muzeum	muzeum	k1gNnSc4	muzeum
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
zaměřilo	zaměřit	k5eAaPmAgNnS	zaměřit
na	na	k7c4	na
regionální	regionální	k2eAgFnSc4d1	regionální
historii	historie	k1gFnSc4	historie
<g/>
,	,	kIx,	,
archeologii	archeologie	k1gFnSc4	archeologie
<g/>
,	,	kIx,	,
numismatiku	numismatika	k1gFnSc4	numismatika
<g/>
,	,	kIx,	,
národopis	národopis	k1gInSc4	národopis
<g/>
,	,	kIx,	,
výtvarné	výtvarný	k2eAgNnSc4d1	výtvarné
umění	umění	k1gNnSc4	umění
<g/>
,	,	kIx,	,
mineralogii	mineralogie	k1gFnSc4	mineralogie
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
též	též	k9	též
na	na	k7c6	na
ornitologii	ornitologie	k1gFnSc6	ornitologie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
sloučení	sloučení	k1gNnSc3	sloučení
obou	dva	k4xCgMnPc2	dva
muzeí	muzeum	k1gNnPc2	muzeum
v	v	k7c4	v
muzeum	muzeum	k1gNnSc4	muzeum
okresní	okresní	k2eAgMnSc1d1	okresní
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c4	na
Okresní	okresní	k2eAgNnSc4d1	okresní
vlastivědné	vlastivědný	k2eAgNnSc4d1	Vlastivědné
muzeum	muzeum	k1gNnSc4	muzeum
J.	J.	kA	J.
A.	A.	kA	A.
Komenského	Komenského	k2eAgInSc4d1	Komenského
Přerov	Přerov	k1gInSc4	Přerov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
bylo	být	k5eAaImAgNnS	být
Okresní	okresní	k2eAgNnSc1d1	okresní
vlastivědné	vlastivědný	k2eAgNnSc1d1	Vlastivědné
muzeum	muzeum	k1gNnSc1	muzeum
J.	J.	kA	J.
A.	A.	kA	A.
Komenského	Komenský	k1gMnSc2	Komenský
přejmenováno	přejmenovat	k5eAaPmNgNnS	přejmenovat
na	na	k7c4	na
Muzeum	muzeum	k1gNnSc4	muzeum
Komenského	Komenský	k1gMnSc2	Komenský
v	v	k7c6	v
Přerově	Přerov	k1gInSc6	Přerov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
v	v	k7c6	v
muzeu	muzeum	k1gNnSc6	muzeum
stále	stále	k6eAd1	stále
expozice	expozice	k1gFnPc4	expozice
týkající	týkající	k2eAgFnPc4d1	týkající
se	se	k3xPyFc4	se
archeologie	archeologie	k1gFnSc2	archeologie
Přerovska	Přerovsko	k1gNnSc2	Přerovsko
<g/>
,	,	kIx,	,
ukázky	ukázka	k1gFnSc2	ukázka
školných	školný	k2eAgFnPc2d1	školný
tříd	třída	k1gFnPc2	třída
z	z	k7c2	z
různých	různý	k2eAgInPc6d1	různý
obdobích	období	k1gNnPc6	období
<g/>
,	,	kIx,	,
památník	památník	k1gInSc4	památník
Jana	Jan	k1gMnSc2	Jan
Amose	Amos	k1gMnSc2	Amos
Komenského	Komenský	k1gMnSc2	Komenský
<g/>
,	,	kIx,	,
národopisu	národopis	k1gInSc2	národopis
Hané	Haná	k1gFnSc2	Haná
a	a	k8xC	a
Záhoří	Záhoří	k1gNnSc2	Záhoří
<g/>
,	,	kIx,	,
entomologie	entomologie	k1gFnSc2	entomologie
<g/>
,	,	kIx,	,
mineralogie	mineralogie	k1gFnSc2	mineralogie
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
===	===	k?	===
Ředitelé	ředitel	k1gMnPc1	ředitel
muzea	muzeum	k1gNnSc2	muzeum
===	===	k?	===
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Hýbl	hýbnout	k5eAaPmAgMnS	hýbnout
(	(	kIx(	(
<g/>
*	*	kIx~	*
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
pracoval	pracovat	k5eAaImAgMnS	pracovat
od	od	k7c2	od
srpna	srpen	k1gInSc2	srpen
1992	[number]	k4	1992
do	do	k7c2	do
konce	konec	k1gInSc2	konec
prosince	prosinec	k1gInSc2	prosinec
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
důchodu	důchod	k1gInSc2	důchod
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gMnSc1	jeho
ředitel	ředitel	k1gMnSc1	ředitel
<g/>
;	;	kIx,	;
ve	v	k7c6	v
svojí	svůj	k3xOyFgFnSc6	svůj
odborné	odborný	k2eAgFnSc6d1	odborná
činnosti	činnost	k1gFnSc6	činnost
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaImAgMnS	věnovat
především	především	k9	především
bádání	bádání	k1gNnSc4	bádání
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
komeniologie	komeniologie	k1gFnSc2	komeniologie
a	a	k8xC	a
objasnění	objasnění	k1gNnSc1	objasnění
vražd	vražda	k1gFnPc2	vražda
Karpatských	karpatský	k2eAgMnPc2d1	karpatský
Němců	Němec	k1gMnPc2	Němec
na	na	k7c6	na
Švédských	švédský	k2eAgFnPc6d1	švédská
šancích	šance	k1gFnPc6	šance
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Muzeum	muzeum	k1gNnSc1	muzeum
Komenského	Komenského	k2eAgNnSc2d1	Komenského
v	v	k7c6	v
Přerově	Přerov	k1gInSc6	Přerov
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
