<p>
<s>
Trhanov	Trhanov	k1gInSc1	Trhanov
je	být	k5eAaImIp3nS	být
zámek	zámek	k1gInSc1	zámek
vybudovaný	vybudovaný	k2eAgInSc1d1	vybudovaný
Wolfem	Wolf	k1gMnSc7	Wolf
Maxmiliánem	Maxmilián	k1gMnSc7	Maxmilián
Lamingerem	Laminger	k1gMnSc7	Laminger
z	z	k7c2	z
Albenreuthu	Albenreuth	k1gInSc2	Albenreuth
–	–	k?	–
zvaným	zvaný	k2eAgMnSc7d1	zvaný
Lomikarem	Lomikar	k1gMnSc7	Lomikar
–	–	k?	–
mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
1676	[number]	k4	1676
a	a	k8xC	a
1677	[number]	k4	1677
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Vybudován	vybudován	k2eAgMnSc1d1	vybudován
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
barokním	barokní	k2eAgInSc6d1	barokní
slohu	sloh	k1gInSc6	sloh
a	a	k8xC	a
sloužil	sloužit	k5eAaImAgInS	sloužit
jako	jako	k9	jako
letní	letní	k2eAgNnSc4d1	letní
sídlo	sídlo	k1gNnSc4	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Kolem	kolem	k7c2	kolem
zámku	zámek	k1gInSc2	zámek
začala	začít	k5eAaPmAgFnS	začít
vznikat	vznikat	k5eAaImF	vznikat
vesnice	vesnice	k1gFnPc4	vesnice
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
Trhanov	Trhanov	k1gInSc1	Trhanov
<g/>
.	.	kIx.	.
</s>
<s>
Lomikar	Lomikar	k1gMnSc1	Lomikar
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
mrtvici	mrtvice	k1gFnSc4	mrtvice
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1696	[number]	k4	1696
<g/>
,	,	kIx,	,
necelý	celý	k2eNgInSc4d1	necelý
rok	rok	k1gInSc4	rok
po	po	k7c6	po
Janu	Jan	k1gMnSc6	Jan
Sladkém	Sladký	k1gMnSc6	Sladký
Kozinovi	Kozin	k1gMnSc6	Kozin
<g/>
,	,	kIx,	,
vůdci	vůdce	k1gMnPc1	vůdce
zdejšího	zdejší	k2eAgNnSc2d1	zdejší
povstání	povstání	k1gNnSc2	povstání
proti	proti	k7c3	proti
vrchnosti	vrchnost	k1gFnSc3	vrchnost
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
popravě	poprava	k1gFnSc6	poprava
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
plzeňského	plzeňský	k2eAgInSc2d1	plzeňský
pivovaru	pivovar	k1gInSc2	pivovar
podle	podle	k7c2	podle
pověsti	pověst	k1gFnSc2	pověst
pronesl	pronést	k5eAaPmAgInS	pronést
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Lomikare	Lomikar	k1gMnSc5	Lomikar
<g/>
,	,	kIx,	,
Lomikare	Lomikar	k1gMnSc5	Lomikar
<g/>
!	!	kIx.	!
</s>
<s>
Do	do	k7c2	do
roka	rok	k1gInSc2	rok
ha	ha	kA	ha
do	do	k7c2	do
dne	den	k1gInSc2	den
budeme	být	k5eAaImBp1nP	být
spolu	spolu	k6eAd1	spolu
stát	stát	k5eAaImF	stát
před	před	k7c7	před
súdnú	súdnú	k?	súdnú
stolicí	stolice	k1gFnSc7	stolice
boží	boží	k2eAgInPc1d1	boží
<g/>
,	,	kIx,	,
hin	hin	k1gInSc1	hin
sa	sa	k?	sa
hukáže	hukáž	k1gFnSc2	hukáž
<g/>
,	,	kIx,	,
hdo	hdo	k?	hdo
z	z	k7c2	z
nás	my	k3xPp1nPc2	my
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
Po	po	k7c6	po
Lamingerech	Lamingero	k1gNnPc6	Lamingero
získali	získat	k5eAaPmAgMnP	získat
panství	panství	k1gNnSc4	panství
Stadionové	Stadionový	k2eAgNnSc4d1	Stadionový
<g/>
.	.	kIx.	.
</s>
<s>
Ti	ten	k3xDgMnPc1	ten
roku	rok	k1gInSc2	rok
1810	[number]	k4	1810
po	po	k7c6	po
požáru	požár	k1gInSc6	požár
nechali	nechat	k5eAaPmAgMnP	nechat
trhanovský	trhanovský	k2eAgInSc4d1	trhanovský
zámek	zámek	k1gInSc4	zámek
přestavět	přestavět	k5eAaPmF	přestavět
v	v	k7c6	v
empírovém	empírový	k2eAgInSc6d1	empírový
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1817	[number]	k4	1817
nechali	nechat	k5eAaPmAgMnP	nechat
dále	daleko	k6eAd2	daleko
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
původní	původní	k2eAgFnSc2d1	původní
kaple	kaple	k1gFnSc2	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1706	[number]	k4	1706
vybudovat	vybudovat	k5eAaPmF	vybudovat
kostel	kostel	k1gInSc4	kostel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
využívala	využívat	k5eAaPmAgFnS	využívat
prostory	prostora	k1gFnPc4	prostora
zámku	zámek	k1gInSc2	zámek
armáda	armáda	k1gFnSc1	armáda
a	a	k8xC	a
po	po	k7c6	po
sametové	sametový	k2eAgFnSc6d1	sametová
revoluci	revoluce	k1gFnSc6	revoluce
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
Galerie	galerie	k1gFnSc1	galerie
bratří	bratřit	k5eAaImIp3nS	bratřit
Špillarů	Špillar	k1gInPc2	Špillar
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
ministerstvem	ministerstvo	k1gNnSc7	ministerstvo
školství	školství	k1gNnSc2	školství
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
nabízelo	nabízet	k5eAaImAgNnS	nabízet
prostory	prostora	k1gFnPc4	prostora
pro	pro	k7c4	pro
školy	škola	k1gFnPc4	škola
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
<g/>
Objekt	objekt	k1gInSc1	objekt
je	být	k5eAaImIp3nS	být
chráněn	chránit	k5eAaImNgInS	chránit
jako	jako	k8xC	jako
nemovitá	movitý	k2eNgFnSc1d1	nemovitá
kulturní	kulturní	k2eAgFnSc1d1	kulturní
památka	památka	k1gFnSc1	památka
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
také	také	k9	také
pro	pro	k7c4	pro
příznivce	příznivec	k1gMnPc4	příznivec
geocachingu	geocaching	k1gInSc2	geocaching
umístěna	umístit	k5eAaPmNgFnS	umístit
keška	keška	k1gFnSc1	keška
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
"	"	kIx"	"
<g/>
GC	GC	kA	GC
<g/>
4544	[number]	k4	4544
<g/>
T	T	kA	T
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
zámků	zámek	k1gInPc2	zámek
v	v	k7c6	v
Plzeňském	plzeňský	k2eAgInSc6d1	plzeňský
kraji	kraj	k1gInSc6	kraj
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Trhanov	Trhanovo	k1gNnPc2	Trhanovo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
oficiální	oficiální	k2eAgFnPc4d1	oficiální
stránky	stránka	k1gFnPc4	stránka
</s>
</p>
