<p>
<s>
Memory	Memor	k1gInPc1	Memor
Almost	Almost	k1gFnSc1	Almost
Full	Full	k1gInSc1	Full
je	být	k5eAaImIp3nS	být
čtrnácté	čtrnáctý	k4xOgNnSc1	čtrnáctý
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
Paula	Paul	k1gMnSc2	Paul
McCartneyho	McCartney	k1gMnSc2	McCartney
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
4	[number]	k4	4
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2007	[number]	k4	2007
a	a	k8xC	a
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
o	o	k7c4	o
den	den	k1gInSc4	den
později	pozdě	k6eAd2	pozdě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Seznam	seznam	k1gInSc1	seznam
skladeb	skladba	k1gFnPc2	skladba
==	==	k?	==
</s>
</p>
<p>
<s>
Autorem	autor	k1gMnSc7	autor
všech	všecek	k3xTgFnPc2	všecek
skladeb	skladba	k1gFnPc2	skladba
je	být	k5eAaImIp3nS	být
Paul	Paul	k1gMnSc1	Paul
McCartney	McCartnea	k1gFnSc2	McCartnea
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Dance	Danka	k1gFnSc6	Danka
Tonight	Tonighta	k1gFnPc2	Tonighta
<g/>
"	"	kIx"	"
–	–	k?	–
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
54	[number]	k4	54
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Ever	Ever	k1gInSc1	Ever
Present	Present	k1gInSc1	Present
Past	past	k1gFnSc1	past
<g/>
"	"	kIx"	"
–	–	k?	–
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
57	[number]	k4	57
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
See	See	k1gMnSc1	See
Your	Your	k1gMnSc1	Your
Sunshine	Sunshin	k1gInSc5	Sunshin
<g/>
"	"	kIx"	"
–	–	k?	–
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
20	[number]	k4	20
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Only	Only	k1gInPc1	Only
Mama	mama	k1gFnSc1	mama
Knows	Knows	k1gInSc1	Knows
<g/>
"	"	kIx"	"
–	–	k?	–
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
17	[number]	k4	17
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
You	You	k1gMnSc1	You
Tell	Tell	k1gMnSc1	Tell
Me	Me	k1gMnSc1	Me
<g/>
"	"	kIx"	"
–	–	k?	–
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
15	[number]	k4	15
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Mr	Mr	k1gFnPc6	Mr
<g/>
.	.	kIx.	.
</s>
<s>
Bellamy	Bellam	k1gInPc1	Bellam
<g/>
"	"	kIx"	"
–	–	k?	–
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
39	[number]	k4	39
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Gratitude	Gratitud	k1gInSc5	Gratitud
<g/>
"	"	kIx"	"
–	–	k?	–
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
19	[number]	k4	19
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Vintage	Vintage	k1gNnPc1	Vintage
Clothes	Clothesa	k1gFnPc2	Clothesa
<g/>
"	"	kIx"	"
–	–	k?	–
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
22	[number]	k4	22
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
That	That	k2eAgMnSc1d1	That
Was	Was	k1gMnSc1	Was
Me	Me	k1gMnSc1	Me
<g/>
"	"	kIx"	"
–	–	k?	–
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
38	[number]	k4	38
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Feet	Feet	k1gInSc1	Feet
in	in	k?	in
the	the	k?	the
Clouds	Clouds	k1gInSc1	Clouds
<g/>
"	"	kIx"	"
–	–	k?	–
3	[number]	k4	3
<g/>
:	:	kIx,	:
<g/>
24	[number]	k4	24
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
House	house	k1gNnSc1	house
of	of	k?	of
Wax	Wax	k1gMnSc2	Wax
<g/>
"	"	kIx"	"
–	–	k?	–
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
59	[number]	k4	59
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
The	The	k1gMnPc7	The
End	End	k1gMnSc7	End
of	of	k?	of
the	the	k?	the
End	End	k1gFnSc1	End
<g/>
"	"	kIx"	"
–	–	k?	–
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
57	[number]	k4	57
</s>
</p>
<p>
<s>
"	"	kIx"	"
<g/>
Nod	Nod	k1gMnSc1	Nod
Your	Your	k1gMnSc1	Your
Head	Head	k1gMnSc1	Head
<g/>
"	"	kIx"	"
–	–	k?	–
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
58	[number]	k4	58
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Memory	Memora	k1gFnSc2	Memora
Almost	Almost	k1gInSc1	Almost
Full	Full	k1gInSc1	Full
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
