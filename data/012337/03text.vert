<p>
<s>
Hokejka	hokejka	k1gFnSc1	hokejka
či	či	k8xC	či
hůl	hůl	k1gFnSc1	hůl
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc4	část
výzbroje	výzbroj	k1gFnSc2	výzbroj
k	k	k7c3	k
hokejové	hokejový	k2eAgFnSc3d1	hokejová
hře	hra	k1gFnSc3	hra
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
pomocí	pomoc	k1gFnSc7	pomoc
se	se	k3xPyFc4	se
ovládá	ovládat	k5eAaImIp3nS	ovládat
kotouč	kotouč	k1gInSc1	kotouč
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zhotovena	zhotovit	k5eAaPmNgFnS	zhotovit
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
umělé	umělý	k2eAgFnSc2d1	umělá
hmoty	hmota	k1gFnSc2	hmota
nebo	nebo	k8xC	nebo
z	z	k7c2	z
jiného	jiný	k2eAgInSc2d1	jiný
materiálu	materiál	k1gInSc2	materiál
(	(	kIx(	(
<g/>
hliník	hliník	k1gInSc1	hliník
<g/>
,	,	kIx,	,
laminát	laminát	k1gInSc1	laminát
<g/>
,	,	kIx,	,
grafit	grafit	k1gInSc1	grafit
<g/>
,	,	kIx,	,
kevlar	kevlar	k1gInSc1	kevlar
<g/>
,	,	kIx,	,
titan	titan	k1gInSc1	titan
<g/>
)	)	kIx)	)
schváleného	schválený	k2eAgNnSc2d1	schválené
IIHF	IIHF	kA	IIHF
<g/>
.	.	kIx.	.
</s>
<s>
Hůl	hůl	k1gFnSc1	hůl
nesmí	smět	k5eNaImIp3nS	smět
mít	mít	k5eAaImF	mít
žádné	žádný	k3yNgInPc4	žádný
výčnělky	výčnělek	k1gInPc4	výčnělek
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
omotána	omotat	k5eAaPmNgFnS	omotat
na	na	k7c6	na
jakémkoli	jakýkoli	k3yIgInSc6	jakýkoli
místě	místo	k1gNnSc6	místo
izolační	izolační	k2eAgFnSc7d1	izolační
páskou	páska	k1gFnSc7	páska
libovolné	libovolný	k2eAgFnSc2d1	libovolná
barvy	barva	k1gFnSc2	barva
a	a	k8xC	a
smí	smět	k5eAaImIp3nS	smět
nést	nést	k5eAaImF	nést
reklamu	reklama	k1gFnSc4	reklama
svého	svůj	k3xOyFgMnSc2	svůj
výrobce	výrobce	k1gMnSc2	výrobce
<g/>
.	.	kIx.	.
</s>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
délka	délka	k1gFnSc1	délka
hole	hole	k1gFnSc2	hole
je	být	k5eAaImIp3nS	být
163	[number]	k4	163
cm	cm	kA	cm
od	od	k7c2	od
patky	patka	k1gFnSc2	patka
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
rukojeti	rukojeť	k1gFnSc2	rukojeť
a	a	k8xC	a
32	[number]	k4	32
cm	cm	kA	cm
od	od	k7c2	od
patky	patka	k1gFnSc2	patka
ke	k	k7c3	k
konci	konec	k1gInSc3	konec
čepele	čepel	k1gFnSc2	čepel
<g/>
.	.	kIx.	.
</s>
<s>
Čepel	čepel	k1gFnSc1	čepel
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
v	v	k7c6	v
žádném	žádný	k3yNgNnSc6	žádný
místě	místo	k1gNnSc6	místo
širší	široký	k2eAgFnSc1d2	širší
než	než	k8xS	než
7,5	[number]	k4	7,5
cm	cm	kA	cm
a	a	k8xC	a
užší	úzký	k2eAgInPc4d2	užší
než	než	k8xS	než
5	[number]	k4	5
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Zakřivení	zakřivení	k1gNnSc1	zakřivení
čepele	čepel	k1gInSc2	čepel
je	být	k5eAaImIp3nS	být
omezeno	omezit	k5eAaPmNgNnS	omezit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
délka	délka	k1gFnSc1	délka
kolmice	kolmice	k1gFnSc1	kolmice
od	od	k7c2	od
přímky	přímka	k1gFnSc2	přímka
spojující	spojující	k2eAgFnSc4d1	spojující
patku	patka	k1gFnSc4	patka
s	s	k7c7	s
koncem	konec	k1gInSc7	konec
čepele	čepel	k1gFnSc2	čepel
k	k	k7c3	k
bodu	bod	k1gInSc3	bod
největšího	veliký	k2eAgNnSc2d3	veliký
zakřivení	zakřivení	k1gNnSc2	zakřivení
nepřesáhne	přesáhnout	k5eNaPmIp3nS	přesáhnout
1,5	[number]	k4	1,5
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
hráčské	hráčský	k2eAgFnSc2d1	hráčská
hole	hole	k1gFnSc2	hole
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nS	lišit
brankářská	brankářský	k2eAgFnSc1d1	brankářská
hůl	hůl	k1gFnSc1	hůl
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
čepel	čepel	k1gFnSc1	čepel
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
v	v	k7c6	v
žádném	žádný	k3yNgNnSc6	žádný
místě	místo	k1gNnSc6	místo
širší	široký	k2eAgFnSc1d2	širší
než	než	k8xS	než
9	[number]	k4	9
cm	cm	kA	cm
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
patky	patka	k1gFnSc2	patka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
širší	široký	k2eAgInSc4d2	širší
než	než	k8xS	než
11,5	[number]	k4	11,5
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Délka	délka	k1gFnSc1	délka
čepele	čepel	k1gInSc2	čepel
nesmí	smět	k5eNaImIp3nS	smět
překročit	překročit	k5eAaPmF	překročit
39	[number]	k4	39
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Široká	široký	k2eAgFnSc1d1	široká
část	část	k1gFnSc1	část
rukojeti	rukojeť	k1gFnSc2	rukojeť
brankářské	brankářský	k2eAgFnSc2d1	brankářská
hole	hole	k1gFnSc2	hole
od	od	k7c2	od
patky	patka	k1gFnSc2	patka
nahoru	nahoru	k6eAd1	nahoru
nesmí	smět	k5eNaImIp3nS	smět
být	být	k5eAaImF	být
delší	dlouhý	k2eAgFnSc4d2	delší
než	než	k8xS	než
71	[number]	k4	71
cm	cm	kA	cm
a	a	k8xC	a
širší	široký	k2eAgInSc4d2	širší
než	než	k8xS	než
9	[number]	k4	9
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
častému	častý	k2eAgNnSc3d1	časté
většímu	veliký	k2eAgNnSc3d2	veliký
zakřivení	zakřivení	k1gNnSc3	zakřivení
čepele	čepel	k1gInSc2	čepel
hráčské	hráčský	k2eAgFnSc2d1	hráčská
hole	hole	k1gFnSc2	hole
<g/>
,	,	kIx,	,
než	než	k8xS	než
dovolují	dovolovat	k5eAaImIp3nP	dovolovat
pravidla	pravidlo	k1gNnPc1	pravidlo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
kapitána	kapitán	k1gMnSc2	kapitán
družstva	družstvo	k1gNnSc2	družstvo
vyžádat	vyžádat	k5eAaPmF	vyžádat
během	během	k7c2	během
utkání	utkání	k1gNnSc2	utkání
u	u	k7c2	u
rozhodčího	rozhodčí	k1gMnSc2	rozhodčí
přeměření	přeměření	k1gNnSc2	přeměření
soupeřovy	soupeřův	k2eAgFnSc2d1	soupeřova
hole	hole	k1gFnSc2	hole
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
kterou	který	k3yIgFnSc4	který
je	být	k5eAaImIp3nS	být
v	v	k7c4	v
tu	ten	k3xDgFnSc4	ten
dobu	doba	k1gFnSc4	doba
na	na	k7c6	na
ledě	led	k1gInSc6	led
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodčí	rozhodčí	k1gMnSc1	rozhodčí
je	být	k5eAaImIp3nS	být
povinen	povinen	k2eAgMnSc1d1	povinen
hůl	hůl	k1gFnSc4	hůl
ihned	ihned	k6eAd1	ihned
přeměřit	přeměřit	k5eAaPmF	přeměřit
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
je	být	k5eAaImIp3nS	být
stížnost	stížnost	k1gFnSc1	stížnost
oprávněná	oprávněný	k2eAgFnSc1d1	oprávněná
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
uložen	uložen	k2eAgInSc1d1	uložen
trest	trest	k1gInSc1	trest
(	(	kIx(	(
<g/>
2	[number]	k4	2
min	min	kA	min
<g/>
)	)	kIx)	)
hráči	hráč	k1gMnSc3	hráč
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
hraje	hrát	k5eAaImIp3nS	hrát
s	s	k7c7	s
nepřípustnou	přípustný	k2eNgFnSc7d1	nepřípustná
holí	hole	k1gFnSc7	hole
<g/>
.	.	kIx.	.
</s>
<s>
Jestliže	jestliže	k8xS	jestliže
je	být	k5eAaImIp3nS	být
stížnost	stížnost	k1gFnSc1	stížnost
neoprávněná	oprávněný	k2eNgFnSc1d1	neoprávněná
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
uložen	uložen	k2eAgInSc1d1	uložen
menší	malý	k2eAgInSc1d2	menší
trest	trest	k1gInSc1	trest
pro	pro	k7c4	pro
hráčskou	hráčský	k2eAgFnSc4d1	hráčská
lavici	lavice	k1gFnSc4	lavice
mužstvu	mužstvo	k1gNnSc3	mužstvo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
žádalo	žádat	k5eAaImAgNnS	žádat
přeměření	přeměření	k1gNnPc4	přeměření
<g/>
.	.	kIx.	.
</s>
<s>
Dá	dát	k5eAaPmIp3nS	dát
<g/>
-li	i	k?	-li
hráč	hráč	k1gMnSc1	hráč
z	z	k7c2	z
trestného	trestný	k2eAgNnSc2d1	trestné
střílení	střílení	k1gNnSc2	střílení
branku	branka	k1gFnSc4	branka
nepřípustnou	přípustný	k2eNgFnSc7d1	nepřípustná
holí	hole	k1gFnSc7	hole
<g/>
,	,	kIx,	,
branka	branka	k1gFnSc1	branka
není	být	k5eNaImIp3nS	být
uznána	uznán	k2eAgFnSc1d1	uznána
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jiný	jiný	k2eAgInSc1d1	jiný
trest	trest	k1gInSc1	trest
není	být	k5eNaImIp3nS	být
uložen	uložit	k5eAaPmNgInS	uložit
<g/>
.	.	kIx.	.
</s>
<s>
Nedá	dát	k5eNaPmIp3nS	dát
<g/>
-li	i	k?	-li
však	však	k9	však
branku	branka	k1gFnSc4	branka
<g/>
,	,	kIx,	,
dostane	dostat	k5eAaPmIp3nS	dostat
menší	malý	k2eAgInSc4d2	menší
trest	trest	k1gInSc4	trest
<g/>
.	.	kIx.	.
</s>
<s>
Hráč	hráč	k1gMnSc1	hráč
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
odmítá	odmítat	k5eAaImIp3nS	odmítat
předat	předat	k5eAaPmF	předat
svou	svůj	k3xOyFgFnSc4	svůj
hůl	hůl	k1gFnSc4	hůl
k	k	k7c3	k
přeměření	přeměření	k1gNnSc3	přeměření
<g/>
,	,	kIx,	,
dostane	dostat	k5eAaPmIp3nS	dostat
menší	malý	k2eAgInSc4d2	menší
trest	trest	k1gInSc4	trest
a	a	k8xC	a
osobní	osobní	k2eAgInSc4d1	osobní
trest	trest	k1gInSc4	trest
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historický	historický	k2eAgInSc1d1	historický
vývoj	vývoj	k1gInSc1	vývoj
hokejky	hokejka	k1gFnSc2	hokejka
==	==	k?	==
</s>
</p>
<p>
<s>
Hůl	hůl	k1gFnSc1	hůl
s	s	k7c7	s
rovnou	rovný	k2eAgFnSc7d1	rovná
čepelí	čepel	k1gFnSc7	čepel
<g/>
,	,	kIx,	,
spočívající	spočívající	k2eAgFnSc7d1	spočívající
celou	celý	k2eAgFnSc7d1	celá
dolní	dolní	k2eAgFnSc7d1	dolní
hranou	hrana	k1gFnSc7	hrana
na	na	k7c6	na
ledě	led	k1gInSc6	led
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
plochým	plochý	k2eAgInSc7d1	plochý
kotoučem	kotouč	k1gInSc7	kotouč
rozhodující	rozhodující	k2eAgFnSc7d1	rozhodující
změnou	změna	k1gFnSc7	změna
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
vytvoření	vytvoření	k1gNnSc3	vytvoření
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
z	z	k7c2	z
předchozích	předchozí	k2eAgFnPc2d1	předchozí
her	hra	k1gFnPc2	hra
na	na	k7c6	na
ledě	led	k1gInSc6	led
<g/>
,	,	kIx,	,
provozovaných	provozovaný	k2eAgInPc2d1	provozovaný
holemi	hole	k1gFnPc7	hole
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
konci	konec	k1gInSc6	konec
zakřivenými	zakřivený	k2eAgInPc7d1	zakřivený
a	a	k8xC	a
kulatým	kulatý	k2eAgInSc7d1	kulatý
míčkem	míček	k1gInSc7	míček
<g/>
.	.	kIx.	.
</s>
<s>
Hůl	hůl	k1gFnSc1	hůl
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
tovární	tovární	k2eAgFnSc6d1	tovární
výrobě	výroba	k1gFnSc6	výroba
produkovala	produkovat	k5eAaImAgFnS	produkovat
už	už	k6eAd1	už
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
hokejové	hokejový	k2eAgFnPc4d1	hokejová
hole	hole	k1gFnPc4	hole
přivezli	přivézt	k5eAaPmAgMnP	přivézt
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
pražští	pražský	k2eAgMnPc1d1	pražský
hráči	hráč	k1gMnPc1	hráč
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
turnaje	turnaj	k1gInSc2	turnaj
v	v	k7c6	v
Chamonix	Chamonix	k1gNnPc6	Chamonix
1909	[number]	k4	1909
a	a	k8xC	a
tam	tam	k6eAd1	tam
sedm	sedm	k4xCc4	sedm
holí	hole	k1gFnPc2	hole
nového	nový	k2eAgInSc2d1	nový
typu	typ	k1gInSc2	typ
zakoupili	zakoupit	k5eAaPmAgMnP	zakoupit
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
brankářskou	brankářský	k2eAgFnSc4d1	brankářská
hůl	hůl	k1gFnSc4	hůl
s	s	k7c7	s
rozšířenou	rozšířený	k2eAgFnSc7d1	rozšířená
čepelí	čepel	k1gFnSc7	čepel
měl	mít	k5eAaImAgInS	mít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
podle	podle	k7c2	podle
fotografické	fotografický	k2eAgFnSc2d1	fotografická
dokumentace	dokumentace	k1gFnSc2	dokumentace
brankář	brankář	k1gMnSc1	brankář
Václav	Václav	k1gMnSc1	Václav
Pondělíček	Pondělíček	k1gMnSc1	Pondělíček
na	na	k7c4	na
ME	ME	kA	ME
1914	[number]	k4	1914
<g/>
.	.	kIx.	.
</s>
<s>
Zásadních	zásadní	k2eAgFnPc2d1	zásadní
změn	změna	k1gFnPc2	změna
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
hráčská	hráčský	k2eAgFnSc1d1	hráčská
hůl	hůl	k1gFnSc1	hůl
dlouho	dlouho	k6eAd1	dlouho
nedoznala	doznat	k5eNaPmAgFnS	doznat
<g/>
,	,	kIx,	,
první	první	k4xOgInPc1	první
zakřivené	zakřivený	k2eAgInPc1d1	zakřivený
čepele	čepel	k1gInPc1	čepel
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgInP	objevit
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
1962	[number]	k4	1962
zásluhou	zásluhou	k7c2	zásluhou
Stanleyho	Stanley	k1gMnSc2	Stanley
Mikity	Mikita	k1gMnSc2	Mikita
a	a	k8xC	a
Roberta	Robert	k1gMnSc2	Robert
Hulla	Hull	k1gMnSc2	Hull
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
nahříváním	nahřívání	k1gNnPc3	nahřívání
hole	hole	k6eAd1	hole
tvarovali	tvarovat	k5eAaImAgMnP	tvarovat
tlakem	tlak	k1gInSc7	tlak
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pod	pod	k7c7	pod
dveřmi	dveře	k1gFnPc7	dveře
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
hůl	hůl	k1gFnSc1	hůl
se	s	k7c7	s
zakřivenou	zakřivený	k2eAgFnSc7d1	zakřivená
čepelí	čepel	k1gFnSc7	čepel
přivezl	přivézt	k5eAaPmAgMnS	přivézt
do	do	k7c2	do
ČSSR	ČSSR	kA	ČSSR
obránce	obránce	k1gMnSc1	obránce
Jozef	Jozef	k1gMnSc1	Jozef
Čapla	čapnout	k5eAaPmAgNnP	čapnout
ze	z	k7c2	z
zájezdu	zájezd	k1gInSc2	zájezd
čs	čs	kA	čs
<g/>
.	.	kIx.	.
mužstva	mužstvo	k1gNnPc4	mužstvo
do	do	k7c2	do
Kanady	Kanada	k1gFnSc2	Kanada
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
dostal	dostat	k5eAaPmAgMnS	dostat
od	od	k7c2	od
Stanleyho	Stanley	k1gMnSc2	Stanley
Mikity	Mikita	k1gMnSc2	Mikita
<g/>
.	.	kIx.	.
</s>
<s>
Hole	hole	k1gFnSc1	hole
s	s	k7c7	s
čepelí	čepel	k1gFnSc7	čepel
zakřivenou	zakřivený	k2eAgFnSc7d1	zakřivená
do	do	k7c2	do
pravidelného	pravidelný	k2eAgInSc2d1	pravidelný
oblouku	oblouk	k1gInSc2	oblouk
nebo	nebo	k8xC	nebo
jen	jen	k9	jen
ve	v	k7c6	v
špičce	špička	k1gFnSc6	špička
či	či	k8xC	či
do	do	k7c2	do
"	"	kIx"	"
<g/>
vrtule	vrtule	k1gFnSc2	vrtule
<g/>
"	"	kIx"	"
s	s	k7c7	s
otevřenou	otevřený	k2eAgFnSc7d1	otevřená
špičkou	špička	k1gFnSc7	špička
se	se	k3xPyFc4	se
v	v	k7c6	v
širokém	široký	k2eAgNnSc6d1	široké
měřítku	měřítko	k1gNnSc6	měřítko
používaly	používat	k5eAaImAgFnP	používat
od	od	k7c2	od
70	[number]	k4	70
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
vyráběné	vyráběný	k2eAgFnPc1d1	vyráběná
v	v	k7c6	v
továrně	továrna	k1gFnSc6	továrna
<g/>
.	.	kIx.	.
</s>
<s>
Zakřivená	zakřivený	k2eAgFnSc1d1	zakřivená
čepel	čepel	k1gFnSc1	čepel
zvýhodňuje	zvýhodňovat	k5eAaImIp3nS	zvýhodňovat
biomechanicky	biomechanicky	k6eAd1	biomechanicky
kontrolu	kontrola	k1gFnSc4	kontrola
kotouče	kotouč	k1gInSc2	kotouč
a	a	k8xC	a
střelbu	střelba	k1gFnSc4	střelba
po	po	k7c6	po
ruce	ruka	k1gFnSc6	ruka
(	(	kIx(	(
<g/>
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
střelba	střelba	k1gFnSc1	střelba
přes	přes	k7c4	přes
ruku	ruka	k1gFnSc4	ruka
a	a	k8xC	a
přihrávka	přihrávka	k1gFnSc1	přihrávka
přes	přes	k7c4	přes
ruku	ruka	k1gFnSc4	ruka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výroba	výroba	k1gFnSc1	výroba
hokejky	hokejka	k1gFnSc2	hokejka
==	==	k?	==
</s>
</p>
<p>
<s>
Hůl	hůl	k1gFnSc1	hůl
vyráběná	vyráběný	k2eAgFnSc1d1	vyráběná
zpočátku	zpočátku	k6eAd1	zpočátku
z	z	k7c2	z
bukového	bukový	k2eAgNnSc2d1	bukové
nebo	nebo	k8xC	nebo
jasanového	jasanový	k2eAgNnSc2d1	jasanové
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
z	z	k7c2	z
jednoho	jeden	k4xCgInSc2	jeden
kusu	kus	k1gInSc2	kus
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
skládat	skládat	k5eAaImF	skládat
z	z	k7c2	z
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
dílů	díl	k1gInPc2	díl
<g/>
,	,	kIx,	,
lepila	lepit	k5eAaImAgFnS	lepit
se	se	k3xPyFc4	se
lepidly	lepidlo	k1gNnPc7	lepidlo
vzdorujícími	vzdorující	k2eAgFnPc7d1	vzdorující
vodě	voda	k1gFnSc6	voda
a	a	k8xC	a
mrazu	mráz	k1gInSc6	mráz
a	a	k8xC	a
zpevňovala	zpevňovat	k5eAaImAgFnS	zpevňovat
se	se	k3xPyFc4	se
na	na	k7c6	na
čepeli	čepel	k1gFnSc6	čepel
a	a	k8xC	a
patce	patka	k1gFnSc6	patka
izolační	izolační	k2eAgFnSc7d1	izolační
páskou	páska	k1gFnSc7	páska
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozdější	pozdní	k2eAgFnSc6d2	pozdější
době	doba	k1gFnSc6	doba
pronikly	proniknout	k5eAaPmAgFnP	proniknout
do	do	k7c2	do
výroby	výroba	k1gFnSc2	výroba
nejrůznější	různý	k2eAgInPc1d3	nejrůznější
i	i	k8xC	i
letecké	letecký	k2eAgInPc4d1	letecký
lamináty	laminát	k1gInPc4	laminát
<g/>
,	,	kIx,	,
kombinované	kombinovaný	k2eAgNnSc4d1	kombinované
a	a	k8xC	a
lepené	lepený	k2eAgNnSc4d1	lepené
se	s	k7c7	s
dřevem	dřevo	k1gNnSc7	dřevo
v	v	k7c6	v
několika	několik	k4yIc6	několik
vrstvách	vrstva	k1gFnPc6	vrstva
(	(	kIx(	(
<g/>
frézování	frézování	k1gNnSc2	frézování
laminátových	laminátový	k2eAgInPc2d1	laminátový
plátků	plátek	k1gInPc2	plátek
<g/>
,	,	kIx,	,
lamelová	lamelový	k2eAgFnSc1d1	lamelová
rukojeť	rukojeť	k1gFnSc1	rukojeť
<g/>
,	,	kIx,	,
čepel	čepel	k1gFnSc1	čepel
obalovaná	obalovaný	k2eAgFnSc1d1	obalovaná
sklolaminátem	sklolaminát	k1gInSc7	sklolaminát
a	a	k8xC	a
prosáklá	prosáklý	k2eAgFnSc1d1	prosáklá
pryskyřicí	pryskyřice	k1gFnSc7	pryskyřice
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
špičkové	špičkový	k2eAgMnPc4d1	špičkový
hráče	hráč	k1gMnPc4	hráč
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
individuální	individuální	k2eAgInPc1d1	individuální
vzory	vzor	k1gInPc1	vzor
holí	holit	k5eAaImIp3nP	holit
na	na	k7c4	na
míru	míra	k1gFnSc4	míra
a	a	k8xC	a
hráči	hráč	k1gMnPc1	hráč
si	se	k3xPyFc3	se
sami	sám	k3xTgMnPc1	sám
hoblíkem	hoblík	k1gInSc7	hoblík
nebo	nebo	k8xC	nebo
rašplí	rašple	k1gFnSc7	rašple
ještě	ještě	k9	ještě
upravují	upravovat	k5eAaImIp3nP	upravovat
špičku	špička	k1gFnSc4	špička
<g/>
,	,	kIx,	,
patku	patka	k1gFnSc4	patka
čepele	čepel	k1gInSc2	čepel
či	či	k8xC	či
rukojeť	rukojeť	k1gFnSc4	rukojeť
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
pronikly	proniknout	k5eAaPmAgFnP	proniknout
na	na	k7c4	na
trh	trh	k1gInSc4	trh
hole	hole	k6eAd1	hole
vyrobené	vyrobený	k2eAgFnPc1d1	vyrobená
z	z	k7c2	z
kovových	kovový	k2eAgFnPc2d1	kovová
slitin	slitina	k1gFnPc2	slitina
či	či	k8xC	či
umělých	umělý	k2eAgFnPc2d1	umělá
hmot	hmota	k1gFnPc2	hmota
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zaručují	zaručovat	k5eAaImIp3nP	zaručovat
jednodušší	jednoduchý	k2eAgFnSc4d2	jednodušší
a	a	k8xC	a
lacinější	laciný	k2eAgFnSc4d2	lacinější
výrobu	výroba	k1gFnSc4	výroba
a	a	k8xC	a
značně	značně	k6eAd1	značně
vyšší	vysoký	k2eAgFnSc1d2	vyšší
trvanlivost	trvanlivost	k1gFnSc1	trvanlivost
hole	hole	k1gFnSc2	hole
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
masovém	masový	k2eAgInSc6d1	masový
a	a	k8xC	a
výkonnostním	výkonnostní	k2eAgInSc6d1	výkonnostní
hokeji	hokej	k1gInSc6	hokej
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
používat	používat	k5eAaImF	používat
z	z	k7c2	z
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
důvodů	důvod	k1gInPc2	důvod
i	i	k9	i
vyměnitelné	vyměnitelný	k2eAgInPc4d1	vyměnitelný
nasouvací	nasouvací	k2eAgInPc4d1	nasouvací
čepele	čepel	k1gInPc4	čepel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
GUT	GUT	kA	GUT
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
;	;	kIx,	;
PACINA	PACINA	kA	PACINA
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
<g/>
.	.	kIx.	.
</s>
<s>
Malá	malý	k2eAgFnSc1d1	malá
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
ledního	lední	k2eAgInSc2d1	lední
hokeje	hokej	k1gInSc2	hokej
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Olympia	Olympia	k1gFnSc1	Olympia
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Šport	Šport	k1gInSc1	Šport
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
150	[number]	k4	150
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
hokejka	hokejka	k1gFnSc1	hokejka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
