<s>
Skupina	skupina	k1gFnSc1	skupina
sehrála	sehrát	k5eAaPmAgFnS	sehrát
významnou	významný	k2eAgFnSc4d1	významná
úlohu	úloha	k1gFnSc4	úloha
také	také	k9	také
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
typickou	typický	k2eAgFnSc4d1	typická
metalovou	metalový	k2eAgFnSc4d1	metalová
módu	móda	k1gFnSc4	móda
<g/>
,	,	kIx,	,
Rob	roba	k1gFnPc2	roba
Halford	Halforda	k1gFnPc2	Halforda
poprvé	poprvé	k6eAd1	poprvé
předvedl	předvést	k5eAaPmAgMnS	předvést
kožené	kožený	k2eAgNnSc4d1	kožené
oblečení	oblečení	k1gNnSc4	oblečení
<g/>
,	,	kIx,	,
cvočky	cvoček	k1gInPc1	cvoček
<g/>
,	,	kIx,	,
pyramidy	pyramida	k1gFnPc1	pyramida
atd.	atd.	kA	atd.
Inspiraci	inspirace	k1gFnSc4	inspirace
hledal	hledat	k5eAaImAgInS	hledat
v	v	k7c6	v
gay	gay	k1gMnSc1	gay
klubech	klub	k1gInPc6	klub
<g/>
,	,	kIx,	,
<g/>
což	což	k3yQnSc1	což
byla	být	k5eAaImAgFnS	být
reakce	reakce	k1gFnSc1	reakce
na	na	k7c4	na
zvýšení	zvýšení	k1gNnSc4	zvýšení
popularity	popularita	k1gFnSc2	popularita
punkové	punkový	k2eAgInPc1d1	punkový
mody	modus	k1gInPc1	modus
a	a	k8xC	a
vzhled	vzhled	k1gInSc1	vzhled
glam	glam	k6eAd1	glam
rockových	rockový	k2eAgMnPc2d1	rockový
hudebníků	hudebník	k1gMnPc2	hudebník
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
skupina	skupina	k1gFnSc1	skupina
Kiss	Kissa	k1gFnPc2	Kissa
nebo	nebo	k8xC	nebo
Alice	Alice	k1gFnSc2	Alice
Cooper	Coopra	k1gFnPc2	Coopra
<g/>
.	.	kIx.	.
</s>
