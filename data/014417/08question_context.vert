<s desamb="1">
Napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
a	a	k8xC
natočil	natočit	k5eAaBmAgMnS
jej	on	k3xPp3gMnSc4
jako	jako	k9
svůj	svůj	k3xOyFgInSc4
režisérský	režisérský	k2eAgInSc4d1
debut	debut	k1gInSc4
Richard	Richard	k1gMnSc1
Kelly	Kella	k1gFnSc2
a	a	k8xC
hlavní	hlavní	k2eAgFnSc2d1
role	role	k1gFnSc2
se	se	k3xPyFc4
zhostil	zhostit	k5eAaPmAgInS
Jake	Jak	k1gFnSc2
Gyllenhaal	Gyllenhaal	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
za	za	k7c7
ní	on	k3xPp3gFnSc7
získal	získat	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
ocenění	ocenění	k1gNnPc2
Young	Young	k1gInSc4
Hollywood	Hollywood	k1gInSc1
Awards	Awardsa	k1gFnPc2
za	za	k7c4
průlomové	průlomový	k2eAgNnSc4d1
mužské	mužský	k2eAgNnSc4d1
vystoupení	vystoupení	k1gNnSc4
<g/>
.	.	kIx.
</s>