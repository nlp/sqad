<s>
Donnie	Donnie	k1gFnPc1
Darko	Darko	k1gNnSc1
</s>
<s>
Donnie	Donnie	k1gFnSc1
Darko	Darko	k1gNnSc4
Původní	původní	k2eAgInSc1d1
název	název	k1gInSc1
</s>
<s>
Donnie	Donnie	k1gFnSc1
Darko	Darko	k1gNnSc1
Země	zem	k1gFnSc2
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Jazyk	jazyk	k1gInSc4
</s>
<s>
angličtina	angličtina	k1gFnSc1
Délka	délka	k1gFnSc1
</s>
<s>
113	#num#	k4
min	mina	k1gFnPc2
a	a	k8xC
134	#num#	k4
min	mina	k1gFnPc2
Žánry	žánr	k1gInPc1
</s>
<s>
komediální	komediální	k2eAgFnPc4d1
dramafantasy	dramafantasa	k1gFnPc4
filmsci-fi	filmsci-fi	k6eAd1
filmfilmové	filmfilmové	k2eAgMnSc1d1
dramafilmový	dramafilmový	k2eAgMnSc1d1
horormysteriózní	horormysteriózní	k2eAgMnSc1d1
filmmagic	filmmagic	k1gMnSc1
realist	realist	k1gMnSc1
film	film	k1gInSc4
Scénář	scénář	k1gInSc1
</s>
<s>
Richard	Richard	k1gMnSc1
Kelly	Kella	k1gFnSc2
Režie	režie	k1gFnSc1
</s>
<s>
Richard	Richard	k1gMnSc1
Kelly	Kella	k1gMnSc2
Obsazení	obsazení	k1gNnSc2
a	a	k8xC
filmový	filmový	k2eAgInSc1d1
štáb	štáb	k1gInSc1
Hlavní	hlavní	k2eAgFnSc1d1
role	role	k1gFnSc1
</s>
<s>
Patrick	Patrick	k1gMnSc1
SwayzeMary	SwayzeMara	k1gFnSc2
McDonnellováJake	McDonnellováJak	k1gFnSc2
GyllenhaalMaggie	GyllenhaalMaggie	k1gFnSc2
GyllenhaalKatharine	GyllenhaalKatharin	k1gInSc5
Rossová	Rossová	k1gFnSc1
<g/>
…	…	k?
více	hodně	k6eAd2
na	na	k7c6
Wikidatech	Wikidat	k1gInPc6
Produkce	produkce	k1gFnSc1
</s>
<s>
Drew	Drew	k?
BarrymoreNancy	BarrymoreNanca	k1gFnSc2
Juvonenová	Juvonenový	k2eAgFnSc1d1
Hudba	hudba	k1gFnSc1
</s>
<s>
Michael	Michael	k1gMnSc1
Andrews	Andrewsa	k1gFnPc2
Kamera	kamera	k1gFnSc1
</s>
<s>
Steven	Steven	k2eAgInSc1d1
Poster	poster	k1gInSc1
Výroba	výroba	k1gFnSc1
a	a	k8xC
distribuce	distribuce	k1gFnSc1
Premiéra	premiéra	k1gFnSc1
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2001	#num#	k4
(	(	kIx(
<g/>
Park	park	k1gInSc1
City	city	k1gNnSc1
<g/>
)	)	kIx)
<g/>
26	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2001	#num#	k4
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
Produkční	produkční	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
</s>
<s>
Flower	Flower	k1gInSc1
Films	Films	k1gInSc1
Distribuce	distribuce	k1gFnSc1
</s>
<s>
MoviemaxNetflixFandangoNow	MoviemaxNetflixFandangoNow	k?
Předchozí	předchozí	k2eAgInSc1d1
a	a	k8xC
následující	následující	k2eAgInSc1d1
díl	díl	k1gInSc1
</s>
<s>
S.	S.	kA
Darko	Darko	k1gNnSc1
</s>
<s>
Donnie	Donnie	k1gFnSc1
Darko	Darko	k1gNnSc1
na	na	k7c6
ČSFD	ČSFD	kA
<g/>
,	,	kIx,
IMDbNěkterá	IMDbNěkterý	k2eAgNnPc1d1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Donnie	Donnie	k1gMnSc1
Darko	Darko	k1gMnSc1
je	být	k5eAaImIp3nS
americké	americký	k2eAgInPc4d1
mysteriózní	mysteriózní	k2eAgInPc4d1
a	a	k8xC
sci-fi	sci-fi	k1gNnSc4
filmové	filmový	k2eAgNnSc4d1
psychologické	psychologický	k2eAgNnSc4d1
drama	drama	k1gNnSc4
z	z	k7c2
roku	rok	k1gInSc2
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Napsal	napsat	k5eAaPmAgMnS,k5eAaBmAgMnS
a	a	k8xC
natočil	natočit	k5eAaBmAgMnS
jej	on	k3xPp3gMnSc4
jako	jako	k9
svůj	svůj	k3xOyFgInSc4
režisérský	režisérský	k2eAgInSc4d1
debut	debut	k1gInSc4
Richard	Richard	k1gMnSc1
Kelly	Kella	k1gFnSc2
a	a	k8xC
hlavní	hlavní	k2eAgFnSc2d1
role	role	k1gFnSc2
se	se	k3xPyFc4
zhostil	zhostit	k5eAaPmAgInS
Jake	Jak	k1gFnSc2
Gyllenhaal	Gyllenhaal	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
za	za	k7c7
ní	on	k3xPp3gFnSc7
získal	získat	k5eAaPmAgMnS
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
ocenění	ocenění	k1gNnPc2
Young	Young	k1gInSc4
Hollywood	Hollywood	k1gInSc1
Awards	Awardsa	k1gFnPc2
za	za	k7c4
průlomové	průlomový	k2eAgNnSc4d1
mužské	mužský	k2eAgNnSc4d1
vystoupení	vystoupení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Film	film	k1gInSc1
líčí	líčit	k5eAaImIp3nS
příběh	příběh	k1gInSc1
mladíka	mladík	k1gMnSc2
Donalda	Donald	k1gMnSc2
Darka	Darek	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nS
odolat	odolat	k5eAaPmF
obrovskému	obrovský	k2eAgMnSc3d1
králíkovi	králík	k1gMnSc3
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
zjevuje	zjevovat	k5eAaImIp3nS
a	a	k8xC
snaží	snažit	k5eAaImIp3nS
se	se	k3xPyFc4
jej	on	k3xPp3gMnSc4
dostat	dostat	k5eAaPmF
pod	pod	k7c4
svůj	svůj	k3xOyFgInSc4
temný	temný	k2eAgInSc4d1
vliv	vliv	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
filmu	film	k1gInSc2
</s>
<s>
Donnie	Donnie	k1gFnSc1
Darko	Darko	k1gNnSc1
v	v	k7c6
Česko-Slovenské	česko-slovenský	k2eAgFnSc6d1
filmové	filmový	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
Donnie	Donnie	k1gFnPc1
Darko	Darko	k1gNnSc4
v	v	k7c4
Internet	Internet	k1gInSc4
Movie	Movie	k1gFnSc2
Database	Databasa	k1gFnSc3
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
7601353-4	7601353-4	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
194774052	#num#	k4
</s>
