<s>
Jan	Jan	k1gMnSc1
Vrba	Vrba	k1gMnSc1
(	(	kIx(
<g/>
politik	politik	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Ing.	ing.	kA
Jan	Jan	k1gMnSc1
Vrba	Vrba	k1gMnSc1
</s>
<s>
Ministr	ministr	k1gMnSc1
průmyslu	průmysl	k1gInSc2
ČR	ČR	kA
<g/>
(	(	kIx(
<g/>
v	v	k7c6
rámci	rámec	k1gInSc6
federace	federace	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Ve	v	k7c6
funkci	funkce	k1gFnSc6
<g/>
:	:	kIx,
<g/>
29	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1990	#num#	k4
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1992	#num#	k4
Předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
</s>
<s>
Petr	Petr	k1gMnSc1
Pithart	Pitharta	k1gFnPc2
Předchůdce	předchůdce	k1gMnSc1
</s>
<s>
Petr	Petr	k1gMnSc1
Hojer	Hojer	k1gMnSc1
Nástupce	nástupce	k1gMnSc1
</s>
<s>
Vladimír	Vladimír	k1gMnSc1
Dlouhý	Dlouhý	k1gMnSc1
</s>
<s>
Stranická	stranický	k2eAgFnSc1d1
příslušnost	příslušnost	k1gFnSc1
Členství	členství	k1gNnSc2
</s>
<s>
OFOH	OFOH	kA
</s>
<s>
Narození	narození	k1gNnSc1
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1937	#num#	k4
PrahaČeskoslovensko	PrahaČeskoslovensko	k1gNnSc4
Československo	Československo	k1gNnSc1
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2020	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
83	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Choť	choť	k1gFnSc1
</s>
<s>
ženatý	ženatý	k2eAgMnSc1d1
Děti	dítě	k1gFnPc1
</s>
<s>
jedno	jeden	k4xCgNnSc1
dítě	dítě	k1gNnSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
VŠ	vš	k0
strojní	strojní	k2eAgFnSc1d1
a	a	k8xC
textilní	textilní	k2eAgInSc1d1
Liberec	Liberec	k1gInSc1
Profese	profes	k1gFnSc2
</s>
<s>
politik	politik	k1gMnSc1
a	a	k8xC
ekonom	ekonom	k1gMnSc1
Commons	Commonsa	k1gFnPc2
</s>
<s>
Jan	Jan	k1gMnSc1
Vrba	Vrba	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybět	k5eAaImIp3nS,k5eAaPmIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Jan	Jan	k1gMnSc1
Vrba	Vrba	k1gMnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1937	#num#	k4
Praha	Praha	k1gFnSc1
–	–	k?
4	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2020	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
a	a	k8xC
československý	československý	k2eAgMnSc1d1
ekonom	ekonom	k1gMnSc1
a	a	k8xC
bývalý	bývalý	k2eAgMnSc1d1
politik	politik	k1gMnSc1
Občanského	občanský	k2eAgNnSc2d1
fóra	fórum	k1gNnSc2
a	a	k8xC
Občanského	občanský	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
<g/>
,	,	kIx,
po	po	k7c6
sametové	sametový	k2eAgFnSc6d1
revoluci	revoluce	k1gFnSc6
český	český	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
průmyslu	průmysl	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Biografie	biografie	k1gFnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1963	#num#	k4
absolvoval	absolvovat	k5eAaPmAgMnS
strojní	strojní	k2eAgFnSc4d1
fakultu	fakulta	k1gFnSc4
Vysoké	vysoký	k2eAgFnSc2d1
školy	škola	k1gFnSc2
strojní	strojní	k2eAgMnSc1d1
a	a	k8xC
textilní	textilní	k2eAgMnSc1d1
v	v	k7c6
Liberci	Liberec	k1gInSc6
(	(	kIx(
<g/>
obor	obor	k1gInSc1
konstrukce	konstrukce	k1gFnSc2
textilních	textilní	k2eAgInPc2d1
strojů	stroj	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1974	#num#	k4
ještě	ještě	k6eAd1
absolvoval	absolvovat	k5eAaPmAgInS
Institut	institut	k1gInSc1
řízení	řízení	k1gNnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pracoval	pracovat	k5eAaImAgMnS
v	v	k7c6
podniku	podnik	k1gInSc6
Textilana	textilana	k1gFnSc1
Liberec	Liberec	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
byl	být	k5eAaImAgInS
vedoucím	vedoucí	k2eAgInSc7d1
několika	několik	k4yIc2
textilních	textilní	k2eAgInPc2d1
závodů	závod	k1gInPc2
tohoto	tento	k3xDgInSc2
podniku	podnik	k1gInSc2
ve	v	k7c6
Stříbře	stříbro	k1gNnSc6
<g/>
,	,	kIx,
Aši	Aš	k1gInSc6
a	a	k8xC
Novém	nový	k2eAgNnSc6d1
Městě	město	k1gNnSc6
pod	pod	k7c7
Smrkem	smrk	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1978	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
ředitelem	ředitel	k1gMnSc7
Textilany	textilana	k1gFnSc2
Liberec	Liberec	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
září	září	k1gNnSc6
1984	#num#	k4
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
náměstkem	náměstek	k1gMnSc7
ministra	ministr	k1gMnSc2
průmyslu	průmysl	k1gInSc2
České	český	k2eAgFnSc2d1
socialistické	socialistický	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
ženatý	ženatý	k2eAgMnSc1d1
<g/>
,	,	kIx,
měl	mít	k5eAaImAgMnS
jedno	jeden	k4xCgNnSc4
dítě	dítě	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
sametové	sametový	k2eAgFnSc6d1
revoluci	revoluce	k1gFnSc6
se	se	k3xPyFc4
zapojil	zapojit	k5eAaPmAgMnS
do	do	k7c2
politiky	politika	k1gFnSc2
<g/>
.	.	kIx.
29	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1990	#num#	k4
byl	být	k5eAaImAgMnS
jmenován	jmenovat	k5eAaBmNgMnS,k5eAaImNgMnS
ministrem	ministr	k1gMnSc7
průmyslu	průmysl	k1gInSc2
ve	v	k7c6
vládě	vláda	k1gFnSc6
Petra	Petr	k1gMnSc2
Pitharta	Pithart	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Portfolio	portfolio	k1gNnSc1
si	se	k3xPyFc3
udržel	udržet	k5eAaPmAgMnS
do	do	k7c2
konce	konec	k1gInSc2
existence	existence	k1gFnSc2
této	tento	k3xDgFnSc2
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
tedy	tedy	k9
do	do	k7c2
2	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1992	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
vlády	vláda	k1gFnSc2
usedl	usednout	k5eAaPmAgMnS
za	za	k7c4
Občanské	občanský	k2eAgNnSc4d1
fórum	fórum	k1gNnSc4
<g/>
,	,	kIx,
po	po	k7c6
jeho	jeho	k3xOp3gInSc6
rozpadu	rozpad	k1gInSc6
zastupoval	zastupovat	k5eAaImAgMnS
Občanské	občanský	k2eAgNnSc4d1
hnutí	hnutí	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
funkci	funkce	k1gFnSc6
ministra	ministr	k1gMnSc2
spolu	spolu	k6eAd1
s	s	k7c7
premiérem	premiér	k1gMnSc7
Petrem	Petr	k1gMnSc7
Pithartem	Pithart	k1gMnSc7
v	v	k7c6
dubnu	duben	k1gInSc6
roce	rok	k1gInSc6
1991	#num#	k4
potvrdil	potvrdit	k5eAaPmAgMnS
privatizaci	privatizace	k1gFnSc4
podniku	podnik	k1gInSc2
Škoda	škoda	k6eAd1
Auto	auto	k1gNnSc1
německé	německý	k2eAgNnSc1d1
firmě	firma	k1gFnSc3
Volkswagen	volkswagen	k1gInSc1
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
připravili	připravit	k5eAaPmAgMnP
v	v	k7c6
2	#num#	k4
<g/>
.	.	kIx.
polovině	polovina	k1gFnSc6
roku	rok	k1gInSc2
1990	#num#	k4
a	a	k8xC
domluvili	domluvit	k5eAaPmAgMnP
s	s	k7c7
představiteli	představitel	k1gMnPc7
Volkswagenu	volkswagen	k1gInSc2
<g/>
||	||	k?
(	(	kIx(
<g/>
gen.	gen.	kA
ředitelem	ředitel	k1gMnSc7
Carlem	Carl	k1gMnSc7
Hahnem	Hahn	k1gMnSc7
ministr	ministr	k1gMnSc1
Miroslav	Miroslav	k1gMnSc1
Grégr	Grégr	k1gMnSc1
<g/>
,	,	kIx,
místopředseda	místopředseda	k1gMnSc1
vlády	vláda	k1gFnSc2
František	František	k1gMnSc1
Vlasák	Vlasák	k1gMnSc1
<g/>
,	,	kIx,
tehdejší	tehdejší	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
Škody	škoda	k1gFnSc2
Dědek	Dědek	k1gMnSc1
<g/>
,	,	kIx,
mladobolesavské	mladobolesavský	k2eAgInPc1d1
odbory	odbor	k1gInPc1
a	a	k8xC
další	další	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
smlouvu	smlouva	k1gFnSc4
ve	v	k7c6
věci	věc	k1gFnSc6
privatizace	privatizace	k1gFnSc2
podepsali	podepsat	k5eAaPmAgMnP
v	v	k7c6
prosinci	prosinec	k1gInSc6
1990	#num#	k4
ze	z	k7c2
vládu	vláda	k1gFnSc4
Miroslav	Miroslav	k1gMnSc1
Grégr	Grégr	k1gMnSc1
a	a	k8xC
za	za	k7c4
VW	VW	kA
Carl	Carl	k1gMnSc1
Hahn	Hahn	k1gMnSc1
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Federální	federální	k2eAgInSc1d1
ministr	ministr	k1gMnSc1
financí	finance	k1gFnPc2
Václav	Václav	k1gMnSc1
Klaus	Klaus	k1gMnSc1
tehdy	tehdy	k6eAd1
přitom	přitom	k6eAd1
favorizoval	favorizovat	k5eAaImAgMnS
metodu	metoda	k1gFnSc4
kupónové	kupónový	k2eAgFnSc2d1
privatizace	privatizace	k1gFnSc2
a	a	k8xC
po	po	k7c6
vítězství	vítězství	k1gNnSc6
ODS	ODS	kA
ve	v	k7c6
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
1992	#num#	k4
zastavil	zastavit	k5eAaPmAgInS
další	další	k2eAgInPc4d1
privatizační	privatizační	k2eAgInPc4d1
projekty	projekt	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
ministr	ministr	k1gMnSc1
Vrba	Vrba	k1gMnSc1
chystal	chystat	k5eAaImAgMnS
pro	pro	k7c4
podniky	podnik	k1gInPc4
Tatra	Tatra	k1gFnSc1
Kopřivnice	Kopřivnice	k1gFnSc1
<g/>
,	,	kIx,
Liaz	liaz	k1gInSc1
a	a	k8xC
Škoda	škoda	k1gFnSc1
Plzeň	Plzeň	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
se	se	k3xPyFc4
uvádělo	uvádět	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
pracoval	pracovat	k5eAaImAgMnS
pro	pro	k7c4
skandinávský	skandinávský	k2eAgInSc4d1
investiční	investiční	k2eAgInSc4d1
fond	fond	k1gInSc4
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Zemřel	zemřít	k5eAaPmAgMnS
dne	den	k1gInSc2
4	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2020	#num#	k4
ve	v	k7c6
věku	věk	k1gInSc6
83	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Před	před	k7c7
smrtí	smrt	k1gFnSc7
onemocněl	onemocnět	k5eAaPmAgMnS
covidem-	covidem-	k?
<g/>
19	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemřel	zemřít	k5eAaPmAgMnS
bývalý	bývalý	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
průmyslu	průmysl	k1gInSc2
z	z	k7c2
Pithartovy	Pithartův	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
Jan	Jan	k1gMnSc1
Vrba	Vrba	k1gMnSc1
<g/>
,	,	kIx,
stál	stát	k5eAaImAgMnS
za	za	k7c7
privatizací	privatizace	k1gFnSc7
Škody	škoda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářské	hospodářský	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
(	(	kIx(
<g/>
iHNed	ihned	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-11-06	2020-11-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Zpráva	zpráva	k1gFnSc1
ČTK	ČTK	kA
<g/>
:	:	kIx,
Ing.	ing.	kA
<g/>
Tomáš	Tomáš	k1gMnSc1
Hradílek	Hradílek	k1gMnSc1
<g/>
,	,	kIx,
ministr	ministr	k1gMnSc1
vnitra	vnitro	k1gNnSc2
<g/>
,	,	kIx,
Datum	datum	k1gNnSc1
vydání	vydání	k1gNnSc1
<g/>
:	:	kIx,
29.6	29.6	k4
<g/>
.1990	.1990	k4
<g/>
,	,	kIx,
Čas	čas	k1gInSc1
vydání	vydání	k1gNnSc2
<g/>
:	:	kIx,
<g/>
,	,	kIx,
ID	Ida	k1gFnPc2
<g/>
:	:	kIx,
A-	A-	k1gFnSc1
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
<g/>
↑	↑	k?
Vláda	vláda	k1gFnSc1
Petra	Petra	k1gFnSc1
Pitharta	Pitharta	k1gFnSc1
(	(	kIx(
<g/>
29.06	29.06	k4
<g/>
.1990	.1990	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2.07	2.07	k4
<g/>
.1992	.1992	k4
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
vlada	vlada	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Přehled	přehled	k1gInSc1
členů	člen	k1gInPc2
vlád	vláda	k1gFnPc2
(	(	kIx(
<g/>
ministrů	ministr	k1gMnPc2
a	a	k8xC
jejich	jejich	k3xOp3gMnPc2
náměstků	náměstek	k1gMnPc2
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
psp	psp	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
srovn	srovn	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Miroslav	Miroslav	k1gMnSc1
Grégr	Grégr	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
Hroník	Hroník	k1gMnSc1
<g/>
:	:	kIx,
Atomový	atomový	k2eAgMnSc1d1
dědek	dědek	k1gMnSc1
Miroslav	Miroslav	k1gMnSc1
Grégr	Grégr	k1gMnSc1
<g/>
:	:	kIx,
Se	s	k7c7
skromností	skromnost	k1gFnSc7
sobě	se	k3xPyFc3
vlastní	vlastnit	k5eAaImIp3nP
<g/>
,	,	kIx,
Euromedia	Euromedium	k1gNnSc2
Group	Group	k1gInSc4
2020	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
150	#num#	k4
<g/>
-	-	kIx~
<g/>
152.1	152.1	k4
2	#num#	k4
Klaus	Klaus	k1gMnSc1
chtěl	chtít	k5eAaImAgMnS
Škodovku	škodovka	k1gFnSc4
prodat	prodat	k5eAaPmF
přes	přes	k7c4
kupony	kupon	k1gInPc4
<g/>
,	,	kIx,
vzpomíná	vzpomínat	k5eAaImIp3nS
exministr	exministr	k1gMnSc1
Vrba	Vrba	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
idnes	idnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Zemřel	zemřít	k5eAaPmAgMnS
bývalý	bývalý	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
průmyslu	průmysl	k1gInSc2
Jan	Jan	k1gMnSc1
Vrba	Vrba	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-11-06	2020-11-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zemřel	zemřít	k5eAaPmAgMnS
Jan	Jan	k1gMnSc1
Vrba	Vrba	k1gMnSc1
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
průmyslu	průmysl	k1gInSc2
z	z	k7c2
Pithartovy	Pithartův	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
mu	on	k3xPp3gMnSc3
83	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
před	před	k7c7
smrtí	smrt	k1gFnSc7
se	se	k3xPyFc4
léčil	léčit	k5eAaImAgMnS
s	s	k7c7
covidem	covid	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-11-06	2020-11-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Vláda	vláda	k1gFnSc1
Petra	Petr	k1gMnSc2
Pitharta	Pithart	k1gMnSc2
Předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnPc4
</s>
<s>
Petr	Petr	k1gMnSc1
Pithart	Pithart	k1gInSc1
(	(	kIx(
<g/>
jmenován	jmenován	k2eAgMnSc1d1
29	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1990	#num#	k4
<g/>
,	,	kIx,
OF	OF	kA
<g/>
,	,	kIx,
později	pozdě	k6eAd2
OH	OH	kA
<g/>
)	)	kIx)
Členové	člen	k1gMnPc1
v	v	k7c4
den	den	k1gInSc4
jmenování	jmenování	k1gNnSc2
vlády	vláda	k1gFnSc2
<g/>
29	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1990	#num#	k4
</s>
<s>
Milan	Milan	k1gMnSc1
Lukeš	Lukeš	k1gMnSc1
(	(	kIx(
<g/>
místopředseda	místopředseda	k1gMnSc1
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
OF	OF	kA
<g/>
,	,	kIx,
později	pozdě	k6eAd2
OH	OH	kA
<g/>
)	)	kIx)
•	•	k?
František	František	k1gMnSc1
Vlasák	Vlasák	k1gMnSc1
(	(	kIx(
<g/>
místopředseda	místopředseda	k1gMnSc1
vlády	vláda	k1gFnSc2
-	-	kIx~
demise	demise	k1gFnSc1
31	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1991	#num#	k4
<g/>
,	,	kIx,
OF	OF	kA
<g/>
,	,	kIx,
později	pozdě	k6eAd2
OH	OH	kA
<g/>
)	)	kIx)
•	•	k?
Antonín	Antonín	k1gMnSc1
Baudyš	Baudyš	k1gMnSc1
(	(	kIx(
<g/>
místopředseda	místopředseda	k1gMnSc1
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
KDU-ČSL	KDU-ČSL	k1gFnSc2
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Karel	Karel	k1gMnSc1
Špaček	Špaček	k1gMnSc1
(	(	kIx(
<g/>
financí	finance	k1gFnPc2
<g/>
,	,	kIx,
OF	OF	kA
<g/>
,	,	kIx,
později	pozdě	k6eAd2
OH	OH	kA
<g/>
)	)	kIx)
•	•	k?
Leon	Leona	k1gFnPc2
Richter	Richter	k1gMnSc1
(	(	kIx(
<g/>
spravedlnost	spravedlnost	k1gFnSc1
–	–	k?
demise	demise	k1gFnSc1
2	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1992	#num#	k4
<g/>
,	,	kIx,
nestraník	nestraník	k1gMnSc1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
OH	OH	kA
<g/>
)	)	kIx)
•	•	k?
Bedřich	Bedřich	k1gMnSc1
Moldan	Moldan	k1gMnSc1
(	(	kIx(
<g/>
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
–	–	k?
demise	demise	k1gFnSc1
24	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1991	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
OF	OF	kA
<g/>
)	)	kIx)
•	•	k?
Jan	Jan	k1gMnSc1
Vrba	Vrba	k1gMnSc1
(	(	kIx(
<g/>
průmyslu	průmysl	k1gInSc2
<g/>
,	,	kIx,
nestraník	nestraník	k1gMnSc1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
OH	OH	kA
<g/>
)	)	kIx)
•	•	k?
Vlasta	Vlasta	k1gFnSc1
Štěpová	štěpový	k2eAgFnSc1d1
(	(	kIx(
<g/>
obchodu	obchod	k1gInSc2
a	a	k8xC
cestovního	cestovní	k2eAgInSc2d1
ruchu	ruch	k1gInSc2
<g/>
,	,	kIx,
OF	OF	kA
<g/>
,	,	kIx,
později	pozdě	k6eAd2
OH	OH	kA
<g/>
)	)	kIx)
•	•	k?
Bohumil	Bohumil	k1gMnSc1
Kubát	Kubát	k1gMnSc1
(	(	kIx(
<g/>
zemědělství	zemědělství	k1gNnSc1
a	a	k8xC
výživa	výživa	k1gFnSc1
<g/>
,	,	kIx,
OF	OF	kA
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
později	pozdě	k6eAd2
ODA	ODA	kA
<g/>
)	)	kIx)
•	•	k?
Petr	Petr	k1gMnSc1
Vopěnka	Vopěnka	k1gFnSc1
(	(	kIx(
<g/>
školství	školství	k1gNnSc2
<g/>
,	,	kIx,
mládeže	mládež	k1gFnSc2
a	a	k8xC
tělovýchovy	tělovýchova	k1gFnSc2
<g/>
,	,	kIx,
KDS	KDS	kA
<g/>
)	)	kIx)
•	•	k?
Tomáš	Tomáš	k1gMnSc1
Hradílek	Hradílek	k1gMnSc1
(	(	kIx(
<g/>
vnitro	vnitro	k1gNnSc1
–	–	k?
demise	demise	k1gFnSc1
14	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1990	#num#	k4
<g/>
,	,	kIx,
OF	OF	kA
<g/>
)	)	kIx)
•	•	k?
Milan	Milan	k1gMnSc1
Horálek	Horálek	k1gMnSc1
(	(	kIx(
<g/>
práce	práce	k1gFnSc2
a	a	k8xC
sociálních	sociální	k2eAgFnPc2d1
věcí	věc	k1gFnPc2
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
OF	OF	kA
<g/>
)	)	kIx)
•	•	k?
Martin	Martin	k1gMnSc1
Bojar	bojar	k1gMnSc1
(	(	kIx(
<g/>
zdravotnictví	zdravotnictví	k1gNnSc1
<g/>
,	,	kIx,
nestraník	nestraník	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Milan	Milan	k1gMnSc1
Uhde	Uhd	k1gFnSc2
(	(	kIx(
<g/>
kultury	kultura	k1gFnSc2
<g/>
,	,	kIx,
OF	OF	kA
<g/>
,	,	kIx,
později	pozdě	k6eAd2
ODS	ODS	kA
<g/>
)	)	kIx)
•	•	k?
Ludvík	Ludvík	k1gMnSc1
Motyčka	Motyčka	k1gMnSc1
(	(	kIx(
<g/>
výstavba	výstavba	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
–	–	k?
zrušeno	zrušen	k2eAgNnSc1d1
31	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1990	#num#	k4
<g/>
,	,	kIx,
KDU-ČSL	KDU-ČSL	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Bohumil	Bohumil	k1gMnSc1
Tichý	Tichý	k1gMnSc1
(	(	kIx(
<g/>
státní	státní	k2eAgFnSc2d1
kontroly	kontrola	k1gFnSc2
–	–	k?
demise	demise	k1gFnSc1
20	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1991	#num#	k4
<g/>
,	,	kIx,
HSDMS	HSDMS	kA
<g/>
)	)	kIx)
•	•	k?
Tomáš	Tomáš	k1gMnSc1
Ježek	Ježek	k1gMnSc1
(	(	kIx(
<g/>
bez	bez	k7c2
portfeje	portfej	k1gInSc2
<g/>
;	;	kIx,
správy	správa	k1gFnSc2
národního	národní	k2eAgInSc2d1
majetku	majetek	k1gInSc2
a	a	k8xC
privatizace	privatizace	k1gFnSc2
<g/>
,	,	kIx,
OF	OF	kA
<g/>
,	,	kIx,
později	pozdě	k6eAd2
ODA	ODA	kA
<g/>
)	)	kIx)
•	•	k?
Karel	Karel	k1gMnSc1
Dyba	Dyba	k1gMnSc1
(	(	kIx(
<g/>
bez	bez	k7c2
portfeje	portfej	k1gInSc2
<g/>
;	;	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
hospodářskou	hospodářský	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
a	a	k8xC
rozvoj	rozvoj	k1gInSc4
<g/>
,	,	kIx,
OF	OF	kA
<g/>
,	,	kIx,
později	pozdě	k6eAd2
ODS	ODS	kA
<g/>
)	)	kIx)
•	•	k?
Miroslav	Miroslav	k1gMnSc1
Grégr	Grégr	k1gMnSc1
(	(	kIx(
<g/>
bez	bez	k7c2
portfeje	portfej	k1gInSc2
<g/>
;	;	kIx,
strojírenství	strojírenství	k1gNnSc2
a	a	k8xC
elektrotechniky	elektrotechnika	k1gFnSc2
–	–	k?
zrušeno	zrušen	k2eAgNnSc1d1
31	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1990	#num#	k4
<g/>
,	,	kIx,
nestraník	nestraník	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Jaroslav	Jaroslav	k1gMnSc1
Šabata	Šabata	k1gMnSc1
(	(	kIx(
<g/>
bez	bez	k7c2
portfeje	portfej	k1gInSc2
<g/>
,	,	kIx,
OF	OF	kA
<g/>
,	,	kIx,
později	pozdě	k6eAd2
OH	OH	kA
<g/>
)	)	kIx)
Členové	člen	k1gMnPc1
jmenovaní	jmenovaný	k2eAgMnPc1d1
později	pozdě	k6eAd2
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1990	#num#	k4
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Sokol	Sokol	k1gMnSc1
(	(	kIx(
<g/>
vnitro	vnitro	k1gNnSc1
<g/>
,	,	kIx,
OF	OF	kA
<g/>
,	,	kIx,
později	pozdě	k6eAd2
OH	OH	kA
<g/>
)	)	kIx)
24	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1991	#num#	k4
</s>
<s>
Ivan	Ivan	k1gMnSc1
Dejmal	Dejmal	k1gMnSc1
(	(	kIx(
<g/>
životní	životní	k2eAgNnSc1d1
prostředí	prostředí	k1gNnSc1
<g/>
,	,	kIx,
KDS	KDS	kA
<g/>
)	)	kIx)
1	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1991	#num#	k4
</s>
<s>
Jan	Jan	k1gMnSc1
Stráský	Stráský	k1gMnSc1
(	(	kIx(
<g/>
místopředseda	místopředseda	k1gMnSc1
vlády	vláda	k1gFnSc2
<g/>
,	,	kIx,
ODS	ODS	kA
<g/>
)	)	kIx)
20	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1991	#num#	k4
</s>
<s>
Igor	Igor	k1gMnSc1
Němec	Němec	k1gMnSc1
(	(	kIx(
<g/>
státní	státní	k2eAgFnSc2d1
kontroly	kontrola	k1gFnSc2
<g/>
,	,	kIx,
ODS	ODS	kA
<g/>
)	)	kIx)
24	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1992	#num#	k4
</s>
<s>
Jiří	Jiří	k1gMnSc1
Novák	Novák	k1gMnSc1
(	(	kIx(
<g/>
spravedlnost	spravedlnost	k1gFnSc1
<g/>
,	,	kIx,
ODS	ODS	kA
<g/>
)	)	kIx)
</s>
