<s>
Fotoreceptor	Fotoreceptor	k1gInSc1	Fotoreceptor
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
smyslový	smyslový	k2eAgInSc1d1	smyslový
receptor	receptor	k1gInSc1	receptor
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
podnětem	podnět	k1gInSc7	podnět
je	být	k5eAaImIp3nS	být
světlo	světlo	k1gNnSc1	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
molekulární	molekulární	k2eAgFnSc6d1	molekulární
úrovni	úroveň	k1gFnSc6	úroveň
k	k	k7c3	k
fotoreceptorům	fotoreceptor	k1gMnPc3	fotoreceptor
patří	patřit	k5eAaImIp3nS	patřit
světlo	světlo	k1gNnSc4	světlo
vnímající	vnímající	k2eAgInPc4d1	vnímající
proteiny	protein	k1gInPc4	protein
-	-	kIx~	-
fotosenzory	fotosenzor	k1gInPc4	fotosenzor
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
lidský	lidský	k2eAgInSc4d1	lidský
rodopsin	rodopsin	k1gInSc4	rodopsin
či	či	k8xC	či
jodopsin	jodopsin	k1gInSc4	jodopsin
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
buněk	buňka	k1gFnPc2	buňka
jsou	být	k5eAaImIp3nP	být
fotoreceptory	fotoreceptor	k1gInPc1	fotoreceptor
vlastně	vlastně	k9	vlastně
světlo	světlo	k1gNnSc1	světlo
vnímající	vnímající	k2eAgFnSc2d1	vnímající
buňky	buňka	k1gFnSc2	buňka
-	-	kIx~	-
zejména	zejména	k9	zejména
různé	různý	k2eAgFnPc1d1	různá
světločivné	světločivný	k2eAgFnPc1d1	světločivná
buňky	buňka	k1gFnPc1	buňka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vnímají	vnímat	k5eAaImIp3nP	vnímat
světlo	světlo	k1gNnSc4	světlo
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
čípky	čípek	k1gInPc4	čípek
a	a	k8xC	a
tyčinky	tyčinka	k1gFnPc4	tyčinka
v	v	k7c6	v
lidském	lidský	k2eAgNnSc6d1	lidské
oku	oko	k1gNnSc6	oko
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
mnohých	mnohý	k2eAgInPc2d1	mnohý
organismů	organismus	k1gInPc2	organismus
vznikají	vznikat	k5eAaImIp3nP	vznikat
specializované	specializovaný	k2eAgInPc1d1	specializovaný
smyslové	smyslový	k2eAgInPc1d1	smyslový
orgány	orgán	k1gInPc1	orgán
-	-	kIx~	-
různé	různý	k2eAgFnPc1d1	různá
světločivné	světločivný	k2eAgFnPc1d1	světločivná
skvrny	skvrna	k1gFnPc1	skvrna
(	(	kIx(	(
<g/>
stigmata	stigma	k1gNnPc1	stigma
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jednoduchá	jednoduchý	k2eAgNnPc1d1	jednoduché
očka	očko	k1gNnPc1	očko
(	(	kIx(	(
<g/>
oceli	ocel	k1gFnSc2	ocel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
složené	složený	k2eAgInPc1d1	složený
oči	oko	k1gNnPc4	oko
či	či	k8xC	či
například	například	k6eAd1	například
komorové	komorový	k2eAgNnSc1d1	komorové
oko	oko	k1gNnSc1	oko
hlavonožců	hlavonožec	k1gMnPc2	hlavonožec
a	a	k8xC	a
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
.	.	kIx.	.
</s>
<s>
Fotoreceptory	Fotoreceptor	k1gInPc1	Fotoreceptor
jsou	být	k5eAaImIp3nP	být
zřejmě	zřejmě	k6eAd1	zřejmě
nejvíce	nejvíce	k6eAd1	nejvíce
variabilní	variabilní	k2eAgInSc1d1	variabilní
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
druhů	druh	k1gInPc2	druh
smyslových	smyslový	k2eAgInPc2d1	smyslový
receptorů	receptor	k1gInPc2	receptor
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
schopnost	schopnost	k1gFnSc1	schopnost
vnímat	vnímat	k5eAaImF	vnímat
světlo	světlo	k1gNnSc4	světlo
měly	mít	k5eAaImAgFnP	mít
již	již	k6eAd1	již
původní	původní	k2eAgFnPc1d1	původní
ektodermální	ektodermální	k2eAgFnPc1d1	ektodermální
buňky	buňka	k1gFnPc1	buňka
živočichů	živočich	k1gMnPc2	živočich
a	a	k8xC	a
u	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
živočichů	živočich	k1gMnPc2	živočich
(	(	kIx(	(
<g/>
krabi	krab	k1gMnPc1	krab
<g/>
)	)	kIx)	)
dokonce	dokonce	k9	dokonce
vnímají	vnímat	k5eAaImIp3nP	vnímat
světlo	světlo	k1gNnSc4	světlo
pouhá	pouhý	k2eAgNnPc1d1	pouhé
nervová	nervový	k2eAgNnPc1d1	nervové
zakončení	zakončení	k1gNnPc1	zakončení
v	v	k7c6	v
pokožce	pokožka	k1gFnSc6	pokožka
<g/>
.	.	kIx.	.
</s>
<s>
Smyslové	smyslový	k2eAgInPc1d1	smyslový
fotoreceptorické	fotoreceptorický	k2eAgInPc1d1	fotoreceptorický
orgány	orgán	k1gInPc1	orgán
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
však	však	k9	však
různě	různě	k6eAd1	různě
složité	složitý	k2eAgNnSc1d1	složité
a	a	k8xC	a
také	také	k9	také
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
různý	různý	k2eAgInSc4d1	různý
typ	typ	k1gInSc4	typ
počitků	počitek	k1gInPc2	počitek
<g/>
,	,	kIx,	,
od	od	k7c2	od
rozmazaného	rozmazaný	k2eAgInSc2d1	rozmazaný
černobílého	černobílý	k2eAgInSc2d1	černobílý
po	po	k7c4	po
barevný	barevný	k2eAgInSc4d1	barevný
precizní	precizní	k2eAgInSc4d1	precizní
obraz	obraz	k1gInSc4	obraz
<g/>
.	.	kIx.	.
</s>
