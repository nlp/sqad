<p>
<s>
Církevní	církevní	k2eAgFnSc1d1	církevní
provincie	provincie	k1gFnSc1	provincie
Lille	Lille	k1gFnSc2	Lille
je	být	k5eAaImIp3nS	být
římskokatolickou	římskokatolický	k2eAgFnSc7d1	Římskokatolická
církevní	církevní	k2eAgFnSc7d1	církevní
provincií	provincie	k1gFnSc7	provincie
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgNnPc1d1	ležící
v	v	k7c6	v
regionu	region	k1gInSc6	region
Nord-Pas-de-Calais	Nord-Pase-Calais	k1gFnSc2	Nord-Pas-de-Calais
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
provincie	provincie	k1gFnSc2	provincie
stojí	stát	k5eAaImIp3nS	stát
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
<g/>
–	–	k?	–
<g/>
metropolita	metropolita	k1gMnSc1	metropolita
z	z	k7c2	z
Lille	Lille	k1gFnSc2	Lille
<g/>
.	.	kIx.	.
</s>
<s>
Provincie	provincie	k1gFnSc1	provincie
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
papež	papež	k1gMnSc1	papež
Benedikt	Benedikt	k1gMnSc1	Benedikt
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
povýšil	povýšit	k5eAaPmAgInS	povýšit
biskupství	biskupství	k1gNnSc4	biskupství
v	v	k7c6	v
Lille	Lill	k1gInSc6	Lill
na	na	k7c4	na
metropolitní	metropolitní	k2eAgNnSc4d1	metropolitní
arcibiskupství	arcibiskupství	k1gNnSc4	arcibiskupství
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnPc3d1	současný
(	(	kIx(	(
<g/>
prvním	první	k4xOgMnSc6	první
<g/>
)	)	kIx)	)
metropolitou	metropolita	k1gMnSc7	metropolita
je	být	k5eAaImIp3nS	být
arcibiskup	arcibiskup	k1gMnSc1	arcibiskup
Laurent	Laurent	k1gMnSc1	Laurent
Ulrich	Ulrich	k1gMnSc1	Ulrich
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Přesunutím	přesunutí	k1gNnSc7	přesunutí
metropolitního	metropolitní	k2eAgInSc2d1	metropolitní
stolce	stolec	k1gInSc2	stolec
z	z	k7c2	z
Cambrai	Cambra	k1gFnSc2	Cambra
do	do	k7c2	do
Lille	Lille	k1gFnSc2	Lille
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
Církevní	církevní	k2eAgFnSc1d1	církevní
provincie	provincie	k1gFnSc2	provincie
Cambrai	Cambra	k1gFnSc2	Cambra
(	(	kIx(	(
<g/>
založena	založen	k2eAgFnSc1d1	založena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1559	[number]	k4	1559
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
sufragánní	sufragánní	k2eAgFnSc1d1	sufragánní
diecéze	diecéze	k1gFnSc1	diecéze
Arras	Arrasa	k1gFnPc2	Arrasa
byla	být	k5eAaImAgFnS	být
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
samotnou	samotný	k2eAgFnSc7d1	samotná
Cambraiskou	Cambraiska	k1gFnSc7	Cambraiska
arcidiecézí	arcidiecéze	k1gFnPc2	arcidiecéze
<g/>
)	)	kIx)	)
převedena	převést	k5eAaPmNgFnS	převést
do	do	k7c2	do
nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgFnSc2d1	vzniklá
provincie	provincie	k1gFnSc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Metropolitním	metropolitní	k2eAgInSc7d1	metropolitní
arcidiecézí	arcidiecéze	k1gFnPc2	arcidiecéze
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stala	stát	k5eAaPmAgFnS	stát
diecéze	diecéze	k1gFnSc1	diecéze
Lille	Lille	k1gFnSc1	Lille
(	(	kIx(	(
<g/>
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
druhým	druhý	k4xOgMnSc7	druhý
sufragánem	sufragán	k1gMnSc7	sufragán
arcidiecéze	arcidiecéze	k1gFnSc2	arcidiecéze
Cambrai	Cambra	k1gFnSc2	Cambra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Členění	členění	k1gNnSc1	členění
==	==	k?	==
</s>
</p>
<p>
<s>
Území	území	k1gNnSc1	území
provincie	provincie	k1gFnSc2	provincie
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
diecéze	diecéze	k1gFnPc4	diecéze
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Arcidiecéze	arcidiecéze	k1gFnSc1	arcidiecéze
Lille	Lille	k1gFnSc1	Lille
<g/>
,	,	kIx,	,
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
na	na	k7c6	na
arcidiecézi	arcidiecéze	k1gFnSc6	arcidiecéze
povýšena	povýšen	k2eAgFnSc1d1	povýšena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
</s>
</p>
<p>
<s>
Arcidiecéze	arcidiecéze	k1gFnSc1	arcidiecéze
cambraiská	cambraiská	k1gFnSc1	cambraiská
<g/>
,	,	kIx,	,
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
580	[number]	k4	580
<g/>
,	,	kIx,	,
na	na	k7c6	na
arcidiecézi	arcidiecéze	k1gFnSc6	arcidiecéze
povýšena	povýšen	k2eAgFnSc1d1	povýšena
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1559	[number]	k4	1559
<g/>
,	,	kIx,	,
statut	statut	k1gInSc1	statut
odebrán	odebrat	k5eAaPmNgInS	odebrat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1801	[number]	k4	1801
<g/>
,	,	kIx,	,
znovu	znovu	k6eAd1	znovu
udělen	udělen	k2eAgInSc1d1	udělen
roku	rok	k1gInSc3	rok
1841	[number]	k4	1841
</s>
</p>
<p>
<s>
Diecéze	diecéze	k1gFnSc1	diecéze
Arras	Arrasa	k1gFnPc2	Arrasa
<g/>
,	,	kIx,	,
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
499	[number]	k4	499
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Province	province	k1gFnSc2	province
ecclésiastique	ecclésiastiqu	k1gFnSc2	ecclésiastiqu
de	de	k?	de
Lille	Lille	k1gFnSc2	Lille
na	na	k7c6	na
francouzské	francouzský	k2eAgFnSc6d1	francouzská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
církev	církev	k1gFnSc1	církev
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
</s>
</p>
