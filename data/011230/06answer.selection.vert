<s>
Třicetiletá	třicetiletý	k2eAgFnSc1d1	třicetiletá
válka	válka	k1gFnSc1	válka
(	(	kIx(	(
<g/>
1618	[number]	k4	1618
<g/>
–	–	k?	–
<g/>
1648	[number]	k4	1648
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
evropský	evropský	k2eAgInSc1d1	evropský
ozbrojený	ozbrojený	k2eAgInSc1d1	ozbrojený
konflikt	konflikt	k1gInSc1	konflikt
známý	známý	k2eAgInSc1d1	známý
především	především	k9	především
jako	jako	k9	jako
vyvrcholení	vyvrcholení	k1gNnSc1	vyvrcholení
sporů	spor	k1gInPc2	spor
mezi	mezi	k7c7	mezi
římskokatolickou	římskokatolický	k2eAgFnSc7d1	Římskokatolická
církví	církev	k1gFnSc7	církev
a	a	k8xC	a
zastánci	zastánce	k1gMnPc1	zastánce
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
po	po	k7c4	po
reformaci	reformace	k1gFnSc4	reformace
v	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
kalvinismem	kalvinismus	k1gInSc7	kalvinismus
a	a	k8xC	a
luteránstvím	luteránství	k1gNnSc7	luteránství
<g/>
.	.	kIx.	.
</s>
