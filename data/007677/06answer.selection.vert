<s>
Kvůli	kvůli	k7c3	kvůli
těmto	tento	k3xDgFnPc3	tento
prohraným	prohraný	k2eAgFnPc3d1	prohraná
bitvám	bitva	k1gFnPc3	bitva
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
kvůli	kvůli	k7c3	kvůli
strachu	strach	k1gInSc3	strach
z	z	k7c2	z
útoku	útok	k1gInSc2	útok
na	na	k7c4	na
Čechy	Čech	k1gMnPc4	Čech
Marie	Maria	k1gFnSc2	Maria
Terezie	Terezie	k1gFnSc2	Terezie
podepsala	podepsat	k5eAaPmAgFnS	podepsat
další	další	k2eAgInSc4d1	další
mír	mír	k1gInSc4	mír
(	(	kIx(	(
<g/>
už	už	k9	už
třetí	třetí	k4xOgFnSc7	třetí
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
)	)	kIx)	)
s	s	k7c7	s
Fridrichem	Fridrich	k1gMnSc7	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
dne	den	k1gInSc2	den
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1745	[number]	k4	1745
v	v	k7c6	v
Drážďanech	Drážďany	k1gInPc6	Drážďany
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgNnSc6	který
se	se	k3xPyFc4	se
definitivně	definitivně	k6eAd1	definitivně
zřekla	zřeknout	k5eAaPmAgFnS	zřeknout
Slezska	Slezsko	k1gNnSc2	Slezsko
<g/>
.	.	kIx.	.
</s>
