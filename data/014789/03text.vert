<s>
HMS	HMS	kA
Invincible	Invincible	k1gFnSc1
(	(	kIx(
<g/>
1907	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Základní	základní	k2eAgInPc1d1
údaje	údaj	k1gInPc1
</s>
<s>
Typ	typ	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
bitevní	bitevní	k2eAgInSc1d1
křižník	křižník	k1gInSc1
</s>
<s>
Třída	třída	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
třída	třída	k1gFnSc1
Invincible	Invincible	k1gFnSc2
</s>
<s>
Objednána	objednán	k2eAgFnSc1d1
<g/>
:	:	kIx,
</s>
<s>
1906	#num#	k4
</s>
<s>
Zahájení	zahájení	k1gNnSc1
stavby	stavba	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1906	#num#	k4
</s>
<s>
Spuštěna	spuštěn	k2eAgNnPc1d1
na	na	k7c4
vodu	voda	k1gFnSc4
<g/>
:	:	kIx,
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1907	#num#	k4
</s>
<s>
Uvedena	uveden	k2eAgFnSc1d1
do	do	k7c2
služby	služba	k1gFnSc2
<g/>
:	:	kIx,
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1907	#num#	k4
</s>
<s>
Osud	osud	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
potopen	potopen	k2eAgMnSc1d1
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Jutska	Jutsko	k1gNnSc2
31	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1916	#num#	k4
</s>
<s>
Takticko-technická	takticko-technický	k2eAgNnPc1d1
data	datum	k1gNnPc1
</s>
<s>
Výtlak	výtlak	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
17	#num#	k4
600	#num#	k4
t	t	k?
</s>
<s>
Délka	délka	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
171	#num#	k4
m	m	kA
</s>
<s>
Šířka	šířka	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
23,9	23,9	k4
m	m	kA
</s>
<s>
Ponor	ponor	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
8	#num#	k4
m	m	kA
</s>
<s>
Pohon	pohon	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
turbínová	turbínový	k2eAgFnSc1d1
soustrojí	soustrojí	k1gNnSc4
Parsons	Parsons	k1gInSc1
<g/>
31	#num#	k4
kotlů	kotel	k1gInPc2
<g/>
4	#num#	k4
lodní	lodní	k2eAgInPc4d1
šrouby	šroub	k1gInPc4
</s>
<s>
Rychlost	rychlost	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
26,6	26,6	k4
uzlů	uzel	k1gInPc2
</s>
<s>
Dosah	dosah	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
5	#num#	k4
000	#num#	k4
nám.	nám.	k?
mil	míle	k1gFnPc2
</s>
<s>
Posádka	posádka	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
837	#num#	k4
</s>
<s>
Pancíř	pancíř	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
boky	bok	k1gInPc1
101	#num#	k4
<g/>
–	–	k?
<g/>
178	#num#	k4
mmpaluby	mmpaluba	k1gFnSc2
76	#num#	k4
a	a	k8xC
25	#num#	k4
mmmůstek	mmmůstka	k1gFnPc2
305	#num#	k4
mmvěže	mmvězat	k5eAaPmIp3nS
178	#num#	k4
mmbarbety	mmbarbeta	k1gFnSc2
věží	věžit	k5eAaImIp3nP
203	#num#	k4
mm	mm	kA
</s>
<s>
Výzbroj	výzbroj	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
8	#num#	k4
<g/>
×	×	k?
305	#num#	k4
mm	mm	kA
(	(	kIx(
<g/>
4	#num#	k4
<g/>
×	×	k?
<g/>
2	#num#	k4
<g/>
)	)	kIx)
<g/>
16	#num#	k4
<g/>
×	×	k?
102	#num#	k4
mm	mm	kA
(	(	kIx(
<g/>
16	#num#	k4
<g/>
×	×	k?
<g/>
1	#num#	k4
<g/>
)	)	kIx)
<g/>
5	#num#	k4
<g/>
×	×	k?
457	#num#	k4
mm	mm	kA
torpédomet	torpédomet	k1gInSc1
</s>
<s>
HMS	HMS	kA
Invincible	Invincible	k1gMnPc2
byl	být	k5eAaImAgInS
bitevní	bitevní	k2eAgInSc1d1
křižník	křižník	k1gInSc1
Royal	Royal	k1gInSc1
Navy	Navy	k?
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
operoval	operovat	k5eAaImAgMnS
v	v	k7c6
první	první	k4xOgFnSc6
světové	světový	k2eAgFnSc6d1
válce	válka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvíc	hodně	k6eAd3,k6eAd1
proslul	proslout	k5eAaPmAgInS
svou	svůj	k3xOyFgFnSc7
účastí	účast	k1gFnSc7
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Jutska	Jutsko	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgMnS
vlajkovou	vlajkový	k2eAgFnSc7d1
lodí	loď	k1gFnSc7
3	#num#	k4
<g/>
.	.	kIx.
eskadry	eskadra	k1gFnSc2
bitevních	bitevní	k2eAgInPc2d1
křižníků	křižník	k1gInPc2
<g/>
,	,	kIx,
vedené	vedený	k2eAgNnSc1d1
kontradmirálem	kontradmirál	k1gMnSc7
Horacem	Horace	k1gMnSc7
Hoodem	Hood	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyletěl	vyletět	k5eAaPmAgMnS
do	do	k7c2
povětří	povětří	k1gNnSc2
po	po	k7c6
zásahu	zásah	k1gInSc6
muničních	muniční	k2eAgNnPc2d1
skladišť	skladiště	k1gNnPc2
od	od	k7c2
německého	německý	k2eAgInSc2d1
bitevního	bitevní	k2eAgInSc2d1
křižníku	křižník	k1gInSc2
SMS	SMS	kA
Lützow	Lützow	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
výjimkou	výjimka	k1gFnSc7
šesti	šest	k4xCc2
lidí	člověk	k1gMnPc2
zahynula	zahynout	k5eAaPmAgFnS
celá	celý	k2eAgFnSc1d1
jeho	jeho	k3xOp3gFnSc1
posádka	posádka	k1gFnSc1
<g/>
,	,	kIx,
včetně	včetně	k7c2
kontradmirála	kontradmirál	k1gMnSc2
Hooda	Hood	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Před	před	k7c7
bitvou	bitva	k1gFnSc7
u	u	k7c2
Jutska	Jutsko	k1gNnSc2
se	se	k3xPyFc4
účastnil	účastnit	k5eAaImAgMnS
první	první	k4xOgFnPc4
bitvy	bitva	k1gFnPc4
u	u	k7c2
Helgolandu	Helgoland	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
tvořil	tvořit	k5eAaImAgInS
společně	společně	k6eAd1
s	s	k7c7
HMS	HMS	kA
New	New	k1gFnSc4
Zealand	Zealanda	k1gFnPc2
svaz	svaz	k1gInSc4
„	„	k?
<g/>
K	k	k7c3
<g/>
“	“	k?
kontradmirála	kontradmirál	k1gMnSc2
Moorea	Mooreus	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dále	daleko	k6eAd2
bitvy	bitva	k1gFnPc1
u	u	k7c2
Falklandských	Falklandský	k2eAgInPc2d1
ostrovů	ostrov	k1gInPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
podílel	podílet	k5eAaImAgMnS
na	na	k7c4
potopení	potopení	k1gNnSc4
německých	německý	k2eAgInPc2d1
pancéřových	pancéřový	k2eAgInPc2d1
křižníků	křižník	k1gInPc2
SMS	SMS	kA
Scharnhorst	Scharnhorst	k1gInSc1
a	a	k8xC
SMS	SMS	kA
Gneisenau	Gneisenaus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sám	sám	k3xTgMnSc1
však	však	k9
v	v	k7c6
této	tento	k3xDgFnSc6
bitvě	bitva	k1gFnSc6
dostal	dostat	k5eAaPmAgMnS
22	#num#	k4
zásahů	zásah	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
HMS	HMS	kA
Invincible	Invincible	k1gMnSc1
(	(	kIx(
<g/>
1907	#num#	k4
<g/>
)	)	kIx)
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
HYNEK	Hynek	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
<g/>
;	;	kIx,
KLUČINA	klučina	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
;	;	kIx,
ŠKŇOUŘIL	škňouřit	k5eAaImAgMnS
<g/>
,	,	kIx,
Evžen	Evžen	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Válečné	válečný	k2eAgFnPc4d1
lodě	loď	k1gFnPc4
3	#num#	k4
<g/>
:	:	kIx,
První	první	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Naše	náš	k3xOp1gNnSc1
vojsko	vojsko	k1gNnSc1
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
.	.	kIx.
28	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
29	#num#	k4
<g/>
-	-	kIx~
<g/>
88	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Kosiarz	Kosiarz	k1gMnSc1
<g/>
,	,	kIx,
Edmund	Edmund	k1gMnSc1
<g/>
,	,	kIx,
Námorné	Námorný	k2eAgFnPc1d1
bitky	bitka	k1gFnPc1
<g/>
,	,	kIx,
Nakľadatelstvo	Nakľadatelstvo	k1gNnSc1
Pravda	pravda	k1gFnSc1
<g/>
,	,	kIx,
Naše	náš	k3xOp1gNnSc1
vojsko	vojsko	k1gNnSc1
<g/>
,	,	kIx,
Bratislava	Bratislava	k1gFnSc1
1984	#num#	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
britských	britský	k2eAgInPc2d1
bitevních	bitevní	k2eAgInPc2d1
křižníků	křižník	k1gInPc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
HMS	HMS	kA
Invincible	Invincible	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Royal	Royal	k1gInSc1
Navy	Navy	k?
Log	log	k1gInSc1
Books	Books	k1gInSc1
of	of	k?
the	the	k?
World	World	k1gInSc1
War	War	k1gFnSc1
1	#num#	k4
Era	Era	k1gMnSc1
<g/>
:	:	kIx,
HMS	HMS	kA
INVINCIBLE	INVINCIBLE	kA
–	–	k?
August	August	k1gMnSc1
1914	#num#	k4
to	ten	k3xDgNnSc1
February	Februar	k1gInPc1
1915	#num#	k4
<g/>
,	,	kIx,
British	British	k1gMnSc1
Home	Hom	k1gFnSc2
Waters	Waters	k1gInSc1
<g/>
,	,	kIx,
Battle	Battle	k1gFnSc1
of	of	k?
Heligoland	Heligoland	k1gInSc1
Bight	Bighta	k1gFnPc2
<g/>
,	,	kIx,
Atlantic	Atlantice	k1gFnPc2
<g/>
,	,	kIx,
Battle	Battle	k1gFnSc1
of	of	k?
the	the	k?
Falklands	Falklands	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příprava	příprava	k1gFnSc1
vydání	vydání	k1gNnSc2
Howard	Howarda	k1gFnPc2
Stagg	Stagga	k1gFnPc2
<g/>
.	.	kIx.
naval-history	naval-histor	k1gInPc1
<g/>
.	.	kIx.
<g/>
net	net	k?
<g/>
,	,	kIx,
rev	rev	k?
<g/>
.	.	kIx.
2014-03-10	2014-03-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Britské	britský	k2eAgInPc1d1
bitevní	bitevní	k2eAgInPc1d1
křižníky	křižník	k1gInPc1
třídy	třída	k1gFnSc2
Invincible	Invincible	k1gFnSc2
</s>
<s>
Invincible	Invincible	k6eAd1
•	•	k?
Inflexible	Inflexible	k1gMnSc1
•	•	k?
Indomitable	Indomitable	k1gMnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Loďstvo	loďstvo	k1gNnSc1
|	|	kIx~
První	první	k4xOgFnSc1
světová	světový	k2eAgFnSc1d1
válka	válka	k1gFnSc1
|	|	kIx~
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
