<s desamb="1">
Nejvíc	hodně	k6eAd3,k6eAd1
proslul	proslout	k5eAaPmAgInS
svou	svůj	k3xOyFgFnSc7
účastí	účast	k1gFnSc7
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Jutska	Jutsko	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgMnS
vlajkovou	vlajkový	k2eAgFnSc7d1
lodí	loď	k1gFnSc7
3	#num#	k4
<g/>
.	.	kIx.
eskadry	eskadra	k1gFnSc2
bitevních	bitevní	k2eAgInPc2d1
křižníků	křižník	k1gInPc2
<g/>
,	,	kIx,
vedené	vedený	k2eAgNnSc1d1
kontradmirálem	kontradmirál	k1gMnSc7
Horacem	Horace	k1gMnSc7
Hoodem	Hood	k1gMnSc7
<g/>
.	.	kIx.
</s>