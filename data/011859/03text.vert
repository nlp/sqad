<p>
<s>
Pláž	pláž	k1gFnSc1	pláž
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc1	část
zábřeží	zábřeží	k1gNnSc1	zábřeží
složená	složený	k2eAgFnSc1d1	složená
z	z	k7c2	z
písku	písek	k1gInSc2	písek
nebo	nebo	k8xC	nebo
drobných	drobný	k2eAgInPc2d1	drobný
kamínků	kamínek	k1gInPc2	kamínek
<g/>
.	.	kIx.	.
</s>
<s>
Pláž	pláž	k1gFnSc1	pláž
je	být	k5eAaImIp3nS	být
základním	základní	k2eAgInSc7d1	základní
akumulačním	akumulační	k2eAgInSc7d1	akumulační
tvarem	tvar	k1gInSc7	tvar
vzniklým	vzniklý	k2eAgInSc7d1	vzniklý
procesy	proces	k1gInPc4	proces
pobřežní	pobřežní	k2eAgFnSc2d1	pobřežní
modelace	modelace	k1gFnSc2	modelace
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pláže	pláž	k1gFnSc2	pláž
rozpoznáváme	rozpoznávat	k5eAaImIp1nP	rozpoznávat
čelo	čelo	k1gNnSc4	čelo
pláže	pláž	k1gFnSc2	pláž
<g/>
,	,	kIx,	,
hranu	hrana	k1gFnSc4	hrana
plážové	plážový	k2eAgFnSc2d1	plážová
terasy	terasa	k1gFnSc2	terasa
<g/>
,	,	kIx,	,
plážovou	plážový	k2eAgFnSc4d1	plážová
terasu	terasa	k1gFnSc4	terasa
(	(	kIx(	(
<g/>
berm	berm	k6eAd1	berm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plážový	plážový	k2eAgInSc4d1	plážový
stupeň	stupeň	k1gInSc4	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Letní	letní	k2eAgFnSc1d1	letní
pláž	pláž	k1gFnSc1	pláž
vzniká	vznikat	k5eAaImIp3nS	vznikat
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
předbřežních	předbřežní	k2eAgInPc2d1	předbřežní
valů	val	k1gInPc2	val
(	(	kIx(	(
<g/>
příbojových	příbojový	k2eAgInPc2d1	příbojový
valů	val	k1gInPc2	val
<g/>
,	,	kIx,	,
bouřkových	bouřkový	k2eAgInPc2d1	bouřkový
valů	val	k1gInPc2	val
<g/>
)	)	kIx)	)
v	v	k7c6	v
období	období	k1gNnSc6	období
s	s	k7c7	s
málo	málo	k6eAd1	málo
četným	četný	k2eAgInSc7d1	četný
výskytem	výskyt	k1gInSc7	výskyt
příbojových	příbojový	k2eAgFnPc2d1	příbojová
vln	vlna	k1gFnPc2	vlna
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
zimní	zimní	k2eAgFnSc1d1	zimní
pláž	pláž	k1gFnSc1	pláž
vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
období	období	k1gNnSc6	období
s	s	k7c7	s
četným	četný	k2eAgInSc7d1	četný
výskytem	výskyt	k1gInSc7	výskyt
příbojových	příbojový	k2eAgFnPc2d1	příbojová
vln	vlna	k1gFnPc2	vlna
<g/>
,	,	kIx,	,
čemuž	což	k3yQnSc3	což
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
plné	plný	k2eAgNnSc1d1	plné
vyvinutí	vyvinutí	k1gNnSc1	vyvinutí
předbřežních	předbřežní	k2eAgInPc2d1	předbřežní
valů	val	k1gInPc2	val
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
mírně	mírně	k6eAd1	mírně
se	se	k3xPyFc4	se
svažuje	svažovat	k5eAaImIp3nS	svažovat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
vodě	voda	k1gFnSc3	voda
<g/>
.	.	kIx.	.
</s>
<s>
Pláž	pláž	k1gFnSc1	pláž
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
vyskytovat	vyskytovat	k5eAaImF	vyskytovat
na	na	k7c6	na
březích	břeh	k1gInPc6	břeh
řek	řeka	k1gFnPc2	řeka
<g/>
,	,	kIx,	,
jezer	jezero	k1gNnPc2	jezero
<g/>
,	,	kIx,	,
moří	moře	k1gNnPc2	moře
či	či	k8xC	či
oceánů	oceán	k1gInPc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
i	i	k8xC	i
desítky	desítka	k1gFnPc1	desítka
až	až	k8xS	až
stovky	stovka	k1gFnPc1	stovka
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Splaveniny	splavenina	k1gFnPc1	splavenina
akumulované	akumulovaný	k2eAgFnPc1d1	akumulovaná
v	v	k7c6	v
korytě	koryto	k1gNnSc6	koryto
řek	řeka	k1gFnPc2	řeka
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
jako	jako	k9	jako
náplavy	náplava	k1gFnPc1	náplava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
pláž	pláž	k1gFnSc1	pláž
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
pláž	pláž	k1gFnSc1	pláž
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
