<s>
Národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
Petrified	Petrified	k1gMnSc1
Forest	Forest	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Zdroje	zdroj	k1gInPc1
k	k	k7c3
infoboxuNárodní	infoboxuNárodní	k2eAgFnSc3d1
park	park	k1gInSc1
Petrified	Petrified	k1gMnSc1
ForestPetrified	ForestPetrified	k1gMnSc1
Forest	Forest	k1gMnSc1
National	National	k1gMnSc1
ParkIUCN	ParkIUCN	k1gMnSc1
kategorie	kategorie	k1gFnSc2
II	II	kA
(	(	kIx(
<g/>
Národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
<g/>
)	)	kIx)
Pohled	pohled	k1gInSc1
na	na	k7c4
hory	hora	k1gFnPc4
<g/>
,	,	kIx,
pojmenované	pojmenovaný	k2eAgFnPc4d1
kvůli	kvůli	k7c3
svému	svůj	k3xOyFgInSc3
tvaru	tvar	k1gInSc3
"	"	kIx"
<g/>
Týpí	týpí	k1gNnSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
angl.	angl.	k?
"	"	kIx"
<g/>
Tepee	Tepee	k1gFnSc1
<g/>
"	"	kIx"
<g/>
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Vyhlášení	vyhlášení	k1gNnSc2
</s>
<s>
31	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1909	#num#	k4
Rozloha	rozloha	k1gFnSc1
</s>
<s>
380	#num#	k4
km	km	kA
<g/>
2	#num#	k4
Správa	správa	k1gFnSc1
</s>
<s>
National	Nationat	k5eAaPmAgInS,k5eAaImAgInS
Park	park	k1gInSc1
Service	Service	k1gFnSc2
Počet	počet	k1gInSc1
návštěvníků	návštěvník	k1gMnPc2
</s>
<s>
2	#num#	k4
697	#num#	k4
182	#num#	k4
(	(	kIx(
<g/>
1962	#num#	k4
<g/>
)	)	kIx)
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Stát	stát	k5eAaImF,k5eAaPmF
USA	USA	kA
</s>
<s>
Arizona	Arizona	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
35	#num#	k4
<g/>
°	°	k?
<g/>
3	#num#	k4
<g/>
′	′	k?
<g/>
38	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
109	#num#	k4
<g/>
°	°	k?
<g/>
46	#num#	k4
<g/>
′	′	k?
<g/>
51	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
Národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
Petrified	Petrified	k1gMnSc1
Forest	Forest	k1gMnSc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Web	web	k1gInSc1
</s>
<s>
www.nps.gov/pefo/index.htm	www.nps.gov/pefo/index.htm	k6eAd1
</s>
<s>
Obrázky	obrázek	k1gInPc4
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc4
či	či	k8xC
videa	video	k1gNnPc4
v	v	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Tento	tento	k3xDgInSc1
box	box	k1gInSc1
<g/>
:	:	kIx,
zobrazit	zobrazit	k5eAaPmF
•	•	k?
diskuse	diskuse	k1gFnSc2
</s>
<s>
Národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
Petrified	Petrified	k1gMnSc1
Forest	Forest	k1gMnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
amerických	americký	k2eAgFnPc2d1
<g/>
,	,	kIx,
v	v	k7c6
Arizoně	Arizona	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plocha	plocha	k1gFnSc1
parku	park	k1gInSc2
je	být	k5eAaImIp3nS
380	#num#	k4
kilometrů	kilometr	k1gInPc2
čtverečních	čtvereční	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stáří	stáří	k1gNnSc1
sedimentů	sediment	k1gInPc2
z	z	k7c2
této	tento	k3xDgFnSc2
lokality	lokalita	k1gFnSc2
činí	činit	k5eAaImIp3nS
asi	asi	k9
225	#num#	k4
až	až	k9
209	#num#	k4
milionů	milion	k4xCgInPc2
let	léto	k1gNnPc2
(	(	kIx(
<g/>
svrchní	svrchní	k2eAgInSc1d1
trias	trias	k1gInSc1
<g/>
,	,	kIx,
geologický	geologický	k2eAgInSc1d1
stupeň	stupeň	k1gInSc1
nor	nora	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Popis	popis	k1gInSc1
</s>
<s>
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
je	být	k5eAaImIp3nS
1556	#num#	k4
m	m	kA
na	na	k7c6
jižním	jižní	k2eAgInSc6d1
a	a	k8xC
1902	#num#	k4
m	m	kA
na	na	k7c6
severním	severní	k2eAgInSc6d1
vstupu	vstup	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počasí	počasí	k1gNnSc1
nemá	mít	k5eNaImIp3nS
žádné	žádný	k3yNgInPc4
extrémy	extrém	k1gInPc4
<g/>
,	,	kIx,
nejvíce	hodně	k6eAd3,k6eAd1
srážek	srážka	k1gFnPc2
bývá	bývat	k5eAaImIp3nS
pří	pře	k1gFnPc2
letních	letní	k2eAgFnPc6d1
bouřkách	bouřka	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Charakter	charakter	k1gInSc1
krajiny	krajina	k1gFnSc2
je	být	k5eAaImIp3nS
náhorní	náhorní	k2eAgFnSc1d1
plošina	plošina	k1gFnSc1
pouštního	pouštní	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
zde	zde	k6eAd1
zčásti	zčásti	k6eAd1
pěkně	pěkně	k6eAd1
zbarvené	zbarvený	k2eAgFnPc1d1
<g/>
,	,	kIx,
geologicky	geologicky	k6eAd1
vrstvené	vrstvený	k2eAgInPc1d1
vrchy	vrch	k1gInPc1
a	a	k8xC
pahorky	pahorek	k1gInPc1
<g/>
,	,	kIx,
hlavní	hlavní	k2eAgInPc1d1
turistickou	turistický	k2eAgFnSc7d1
atrakcí	atrakce	k1gFnSc7
jsou	být	k5eAaImIp3nP
zkamenělé	zkamenělý	k2eAgInPc1d1
kusy	kus	k1gInPc1
dřeva	dřevo	k1gNnSc2
a	a	k8xC
kmenů	kmen	k1gInPc2
pravěkých	pravěký	k2eAgFnPc2d1
dřevin	dřevina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Vývoj	vývoj	k1gInSc1
</s>
<s>
Krajina	Krajina	k1gFnSc1
na	na	k7c6
území	území	k1gNnSc6
dnešního	dnešní	k2eAgInSc2d1
Národního	národní	k2eAgInSc2d1
parku	park	k1gInSc2
Petrified	Petrified	k1gMnSc1
forest	forest	k1gMnSc1
je	být	k5eAaImIp3nS
pustá	pustý	k2eAgFnSc1d1
a	a	k8xC
suchá	suchý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslední	poslední	k2eAgInSc1d1
les	les	k1gInSc1
tu	tu	k6eAd1
rostl	růst	k5eAaImAgInS
pouze	pouze	k6eAd1
v	v	k7c6
dávné	dávný	k2eAgFnSc6d1
minulosti	minulost	k1gFnSc6
–	–	k?
asi	asi	k9
před	před	k7c7
220	#num#	k4
miliony	milion	k4xCgInPc7
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Tenkrát	tenkrát	k6eAd1
<g/>
,	,	kIx,
v	v	k7c6
období	období	k1gNnSc6
triasu	trias	k1gInSc2
<g/>
,	,	kIx,
pokrývaly	pokrývat	k5eAaImAgFnP
jižní	jižní	k2eAgInSc4d1
okraj	okraj	k1gInSc4
pouze	pouze	k6eAd1
velké	velký	k2eAgFnSc2d1
bažinaté	bažinatý	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
<g/>
,	,	kIx,
protékané	protékaný	k2eAgFnSc2d1
několika	několik	k4yIc7
řekami	řeka	k1gFnPc7
<g/>
,	,	kIx,
husté	hustý	k2eAgInPc1d1
porosty	porost	k1gInPc1
jehličnatých	jehličnatý	k2eAgInPc2d1
stromů	strom	k1gInPc2
<g/>
,	,	kIx,
podobných	podobný	k2eAgInPc2d1
dnešním	dnešní	k2eAgNnSc7d1
jedlím	jedlí	k1gNnSc7
<g/>
,	,	kIx,
pod	pod	k7c7
nimiž	jenž	k3xRgMnPc7
vyrůstaly	vyrůstat	k5eAaImAgFnP
obrovské	obrovský	k2eAgFnPc1d1
kapradiny	kapradina	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vodách	voda	k1gFnPc6
žili	žít	k5eAaImAgMnP
velcí	velký	k2eAgMnPc1d1
plazi	plaz	k1gMnPc1
<g/>
,	,	kIx,
předchůdci	předchůdce	k1gMnPc1
dnešních	dnešní	k2eAgInPc2d1
krokodýlů	krokodýl	k1gMnPc2
<g/>
,	,	kIx,
a	a	k8xC
bohatou	bohatý	k2eAgFnSc7d1
vegetací	vegetace	k1gFnSc7
na	na	k7c6
březích	břeh	k1gInPc6
rejdili	rejdit	k5eAaImAgMnP
malí	malý	k2eAgMnPc1d1
ještěři	ještěr	k1gMnPc1
a	a	k8xC
dinosauři	dinosaurus	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Odumřelé	odumřelý	k2eAgNnSc4d1
<g/>
,	,	kIx,
podmáčené	podmáčený	k2eAgNnSc4d1
nebo	nebo	k8xC
větrem	vítr	k1gInSc7
vyvrácené	vyvrácený	k2eAgInPc4d1
stromy	strom	k1gInPc4
byly	být	k5eAaImAgFnP
říčnímy	říčním	k1gInPc4
proudy	proud	k1gInPc1
odplavovány	odplavován	k2eAgInPc1d1
do	do	k7c2
močálů	močál	k1gInPc2
a	a	k8xC
tam	tam	k6eAd1
<g/>
,	,	kIx,
společně	společně	k6eAd1
s	s	k7c7
uhynulými	uhynulý	k2eAgMnPc7d1
živočichy	živočich	k1gMnPc7
<g/>
,	,	kIx,
pohřbeny	pohřben	k2eAgFnPc1d1
hluboko	hluboko	k6eAd1
pod	pod	k7c7
silnými	silný	k2eAgFnPc7d1
vrstvami	vrstva	k1gFnPc7
bahna	bahno	k1gNnSc2
a	a	k8xC
kalů	kal	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
organismy	organismus	k1gInPc4
<g/>
,	,	kIx,
dokonale	dokonale	k6eAd1
odříznuty	odříznout	k5eAaPmNgFnP
od	od	k7c2
přívodu	přívod	k1gInSc2
kyslíku	kyslík	k1gInSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
rozkládaly	rozkládat	k5eAaImAgInP
a	a	k8xC
trouchnivěly	trouchnivět	k5eAaImAgInP
jen	jen	k9
velmi	velmi	k6eAd1
pomalu	pomalu	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kostry	kostra	k1gFnPc4
zvířat	zvíře	k1gNnPc2
byly	být	k5eAaImAgFnP
dokonale	dokonale	k6eAd1
konzervovány	konzervován	k2eAgFnPc1d1
a	a	k8xC
mrtvé	mrtvý	k2eAgNnSc1d1
dřevo	dřevo	k1gNnSc1
postupně	postupně	k6eAd1
nasakovalo	nasakovat	k5eAaImAgNnS
vodou	voda	k1gFnSc7
<g/>
,	,	kIx,
obsahující	obsahující	k2eAgInPc1d1
vyloučené	vyloučený	k2eAgInPc1d1
křemičitany	křemičitan	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Organická	organický	k2eAgFnSc1d1
tkaniva	tkanivo	k1gNnSc2
byla	být	k5eAaImAgFnS
nahrazena	nahradit	k5eAaPmNgFnS
minerály	minerál	k1gInPc7
<g/>
,	,	kIx,
kusy	kus	k1gInPc7
dřeva	dřevo	k1gNnSc2
a	a	k8xC
celé	celý	k2eAgInPc1d1
stromy	strom	k1gInPc1
zkameněly	zkamenět	k5eAaPmAgInP
<g/>
.	.	kIx.
</s>
<s>
Krajina	Krajina	k1gFnSc1
byla	být	k5eAaImAgFnS
později	pozdě	k6eAd2
zasažena	zasáhnout	k5eAaPmNgFnS
silnými	silný	k2eAgInPc7d1
výbuchy	výbuch	k1gInPc7
sopek	sopka	k1gFnPc2
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gFnPc1
láva	láva	k1gFnSc1
a	a	k8xC
horký	horký	k2eAgInSc1d1
popel	popel	k1gInSc1
udusily	udusit	k5eAaPmAgFnP
všechno	všechen	k3xTgNnSc4
živé	živý	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následkem	následkem	k7c2
vzájemného	vzájemný	k2eAgInSc2d1
posuvu	posuv	k1gInSc2
kontinentálních	kontinentální	k2eAgFnPc2d1
desek	deska	k1gFnPc2
zemský	zemský	k2eAgInSc4d1
povrch	povrch	k1gInSc4
v	v	k7c6
této	tento	k3xDgFnSc6
části	část	k1gFnSc6
světa	svět	k1gInSc2
poklesl	poklesnout	k5eAaPmAgInS
a	a	k8xC
byl	být	k5eAaImAgInS
zaplaven	zaplavit	k5eAaPmNgInS
velkými	velký	k2eAgFnPc7d1
vnitrozemskými	vnitrozemský	k2eAgNnPc7d1
jezery	jezero	k1gNnPc7
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
na	na	k7c4
pohřbené	pohřbený	k2eAgInPc4d1
lesy	les	k1gInPc4
navršila	navršit	k5eAaPmAgFnS
další	další	k2eAgFnPc4d1
vrstvy	vrstva	k1gFnPc4
sedimentů	sediment	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
krajina	krajina	k1gFnSc1
při	při	k7c6
pozdějších	pozdní	k2eAgInPc6d2
horotvorných	horotvorný	k2eAgInPc6d1
pohybech	pohyb	k1gInPc6
opět	opět	k6eAd1
zvedla	zvednout	k5eAaPmAgFnS
<g/>
,	,	kIx,
zmíněné	zmíněný	k2eAgFnPc1d1
spodní	spodní	k2eAgFnPc1d1
geologické	geologický	k2eAgFnPc1d1
vrstvy	vrstva	k1gFnPc1
<g/>
,	,	kIx,
nahromaděné	nahromaděný	k2eAgFnPc1d1
v	v	k7c6
průběhu	průběh	k1gInSc6
miliónů	milión	k4xCgInPc2
let	léto	k1gNnPc2
<g/>
,	,	kIx,
praskaly	praskat	k5eAaImAgFnP
a	a	k8xC
s	s	k7c7
nimi	on	k3xPp3gMnPc7
praskaly	praskat	k5eAaImAgInP
také	také	k9
kmeny	kmen	k1gInPc1
zkamenělých	zkamenělý	k2eAgInPc2d1
stromů	strom	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Větrná	větrný	k2eAgFnSc1d1
a	a	k8xC
vodní	vodní	k2eAgFnSc1d1
eroze	eroze	k1gFnSc1
pak	pak	k6eAd1
postupně	postupně	k6eAd1
odkrývala	odkrývat	k5eAaImAgFnS
horní	horní	k2eAgFnPc4d1
vrstvy	vrstva	k1gFnPc4
a	a	k8xC
jednotlivé	jednotlivý	k2eAgInPc4d1
vzácné	vzácný	k2eAgInPc4d1
fragmenty	fragment	k1gInPc4
zkamenělého	zkamenělý	k2eAgNnSc2d1
dřeva	dřevo	k1gNnSc2
se	se	k3xPyFc4
tak	tak	k6eAd1
znovu	znovu	k6eAd1
dostaly	dostat	k5eAaPmAgFnP
na	na	k7c4
světlo	světlo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Park	park	k1gInSc1
</s>
<s>
Ukázky	ukázka	k1gFnPc1
těchto	tento	k3xDgFnPc2
jedinečných	jedinečný	k2eAgFnPc2d1
geologických	geologický	k2eAgFnPc2d1
forem	forma	k1gFnPc2
můžeme	moct	k5eAaImIp1nP
dnes	dnes	k6eAd1
nejlépe	dobře	k6eAd3
obdivovat	obdivovat	k5eAaImF
právě	právě	k9
na	na	k7c6
relativně	relativně	k6eAd1
malém	malý	k2eAgNnSc6d1
území	území	k1gNnSc6
Národního	národní	k2eAgInSc2d1
parku	park	k1gInSc2
Petrified	Petrified	k1gMnSc1
Forest	Forest	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kdyby	kdyby	k9
nebyl	být	k5eNaImAgInS
národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
zřízen	zřídit	k5eAaPmNgInS
<g/>
,	,	kIx,
kdoví	kdoví	k0
<g/>
,	,	kIx,
zda	zda	k8xS
by	by	kYmCp3nS
to	ten	k3xDgNnSc1
dnes	dnes	k6eAd1
ještě	ještě	k9
nějaký	nějaký	k3yIgInSc4
pestrobarevný	pestrobarevný	k2eAgInSc4d1
kus	kus	k1gInSc4
zkamenělého	zkamenělý	k2eAgNnSc2d1
dřeva	dřevo	k1gNnSc2
zůstal	zůstat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lovci	lovec	k1gMnPc1
suvenýrů	suvenýr	k1gInPc2
si	se	k3xPyFc3
z	z	k7c2
chráněného	chráněný	k2eAgNnSc2d1
území	území	k1gNnSc2
ve	v	k7c6
svých	svůj	k3xOyFgFnPc6
taškách	taška	k1gFnPc6
rok	rok	k1gInSc4
co	co	k9
rok	rok	k1gInSc4
nelegálně	legálně	k6eNd1
odnesou	odnést	k5eAaPmIp3nP
téměř	téměř	k6eAd1
tunu	tuna	k1gFnSc4
zkamenělého	zkamenělý	k2eAgNnSc2d1
dřeva	dřevo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
jižního	jižní	k2eAgMnSc2d1
k	k	k7c3
severnímu	severní	k2eAgInSc3d1
vstupu	vstup	k1gInSc3
do	do	k7c2
národního	národní	k2eAgInSc2d1
parku	park	k1gInSc2
vede	vést	k5eAaImIp3nS
27	#num#	k4
mil	míle	k1gFnPc2
dlouhá	dlouhý	k2eAgFnSc1d1
vyhlídková	vyhlídkový	k2eAgFnSc1d1
silnice	silnice	k1gFnSc1
Scenic	Scenice	k1gFnPc2
Drive	drive	k1gInSc1
.	.	kIx.
</s>
<s desamb="1">
Podél	podél	k7c2
ní	on	k3xPp3gFnSc2
jsou	být	k5eAaImIp3nP
umístěny	umístit	k5eAaPmNgInP
jednotlivé	jednotlivý	k2eAgInPc1d1
vyhlídkové	vyhlídkový	k2eAgInPc1d1
body	bod	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
v	v	k7c6
řadě	řada	k1gFnSc6
případů	případ	k1gInPc2
zároveň	zároveň	k6eAd1
představují	představovat	k5eAaImIp3nP
výchozí	výchozí	k2eAgInPc4d1
body	bod	k1gInPc4
pěších	pěší	k2eAgFnPc2d1
túr	túra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hned	hned	k6eAd1
za	za	k7c7
jižním	jižní	k2eAgInSc7d1
vstupem	vstup	k1gInSc7
je	být	k5eAaImIp3nS
Rainbow	Rainbow	k1gMnSc1
Forest	Forest	k1gMnSc1
Museum	museum	k1gNnSc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
jsou	být	k5eAaImIp3nP
vystaveny	vystaven	k2eAgInPc4d1
leštěné	leštěný	k2eAgInPc4d1
kusy	kus	k1gInPc4
zkamenělého	zkamenělý	k2eAgNnSc2d1
dřeva	dřevo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Galerie	galerie	k1gFnSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Čtyři	čtyři	k4xCgInPc1
rohy	roh	k1gInPc1
</s>
<s>
Koloradská	Koloradský	k2eAgFnSc1d1
plošina	plošina	k1gFnSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Cornelia	Cornelia	k1gFnSc1
Rasmussen	Rasmussen	k1gInSc1
<g/>
,	,	kIx,
Roland	Roland	k1gInSc1
Mundil	Mundil	k1gFnSc1
<g/>
,	,	kIx,
Randall	Randall	k1gInSc1
B.	B.	kA
Irmis	Irmis	k1gInSc1
<g/>
,	,	kIx,
Dominique	Dominique	k1gInSc1
Geisler	Geisler	k1gInSc1
<g/>
,	,	kIx,
George	George	k1gInSc1
E.	E.	kA
Gehrels	Gehrels	k1gInSc1
<g/>
,	,	kIx,
Paul	Paul	k1gMnSc1
E.	E.	kA
Olsen	Olsen	k1gInSc1
<g/>
,	,	kIx,
Dennis	Dennis	k1gInSc1
V.	V.	kA
Kent	Kent	k1gInSc1
<g/>
,	,	kIx,
Christopher	Christophra	k1gFnPc2
Lepre	Lepr	k1gInSc5
<g/>
,	,	kIx,
Sean	Sean	k1gMnSc1
T.	T.	kA
Kinney	Kinnea	k1gFnPc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
W.	W.	kA
Geissman	Geissman	k1gMnSc1
&	&	k?
William	William	k1gInSc1
G.	G.	kA
Parker	Parker	k1gInSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U-Pb	U-Pb	k1gMnSc1
zircon	zircon	k1gMnSc1
geochronology	geochronolog	k1gMnPc4
and	and	k?
depositional	depositionat	k5eAaPmAgInS,k5eAaImAgInS
age	age	k?
models	models	k6eAd1
for	forum	k1gNnPc2
the	the	k?
Upper	Upper	k1gMnSc1
Triassic	Triassic	k1gMnSc1
Chinle	Chinle	k1gFnSc2
Formation	Formation	k1gInSc1
(	(	kIx(
<g/>
Petrified	Petrified	k1gMnSc1
Forest	Forest	k1gMnSc1
National	National	k1gMnSc1
Park	park	k1gInSc1
<g/>
,	,	kIx,
Arizona	Arizona	k1gFnSc1
<g/>
,	,	kIx,
USA	USA	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Implications	Implications	k1gInSc1
for	forum	k1gNnPc2
Late	lat	k1gInSc5
Triassic	Triassice	k1gFnPc2
paleoecological	paleoecologicat	k5eAaPmAgMnS
and	and	k?
paleoenvironmental	paleoenvironmental	k1gMnSc1
change	change	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
GSA	GSA	kA
Bulletin	bulletin	k1gInSc1
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1130/B35485.1	https://doi.org/10.1130/B35485.1	k4
<g/>
↑	↑	k?
https://phys.org/news/2020-07-arizona-core-triassic-dark-ages.html	https://phys.org/news/2020-07-arizona-core-triassic-dark-ages.html	k1gMnSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Reilly	Reill	k1gInPc1
F.	F.	kA
Hayes	Hayesa	k1gFnPc2
<g/>
;	;	kIx,
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Modeling	Modeling	k1gInSc1
the	the	k?
dynamics	dynamics	k1gInSc1
of	of	k?
a	a	k8xC
Late	lat	k1gInSc5
Triassic	Triassice	k1gFnPc2
vertebrate	vertebrat	k1gInSc5
extinction	extinction	k1gInSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Adamanian	Adamanian	k1gMnSc1
<g/>
/	/	kIx~
<g/>
Revueltian	Revueltian	k1gMnSc1
faunal	faunat	k5eAaPmAgMnS,k5eAaImAgMnS
turnover	turnover	k1gMnSc1
<g/>
,	,	kIx,
Petrified	Petrified	k1gMnSc1
Forest	Forest	k1gMnSc1
National	National	k1gMnSc1
Park	park	k1gInSc1
<g/>
,	,	kIx,
Arizona	Arizona	k1gFnSc1
<g/>
,	,	kIx,
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geology	geolog	k1gMnPc4
(	(	kIx(
<g/>
advance	advanec	k1gMnSc4
online	onlinout	k5eAaPmIp3nS
publication	publication	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
doi	doi	k?
<g/>
:	:	kIx,
https://doi.org/10.1130/G47037.1	https://doi.org/10.1130/G47037.1	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Národní	národní	k2eAgInSc4d1
park	park	k1gInSc4
Petrified	Petrified	k1gInSc1
Forest	Forest	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
http://www.nps.gov/pefo/index.htm	http://www.nps.gov/pefo/index.htm	k6eAd1
Informace	informace	k1gFnPc1
na	na	k7c6
webu	web	k1gInSc6
NPS	NPS	kA
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Národní	národní	k2eAgInPc1d1
parky	park	k1gInPc1
ve	v	k7c6
Spojených	spojený	k2eAgInPc6d1
státech	stát	k1gInPc6
amerických	americký	k2eAgInPc6d1
</s>
<s>
Acadia	Acadium	k1gNnSc2
•	•	k?
Americká	americký	k2eAgFnSc1d1
Samoa	Samoa	k1gFnSc1
•	•	k?
Arches	Arches	k1gInSc1
•	•	k?
Badlands	Badlands	k1gInSc1
•	•	k?
Big	Big	k1gFnSc2
Bend	Bend	k1gMnSc1
•	•	k?
Biscayne	Biscayn	k1gInSc5
•	•	k?
Black	Black	k1gMnSc1
Canyon	Canyon	k1gMnSc1
of	of	k?
the	the	k?
Gunnison	Gunnison	k1gMnSc1
•	•	k?
Bryce	Bryce	k1gMnSc1
Canyon	Canyon	k1gMnSc1
•	•	k?
Canyonlands	Canyonlands	k1gInSc1
•	•	k?
Capitol	Capitol	k1gInSc1
Reef	Reef	k1gInSc1
•	•	k?
Carlsbadské	Carlsbadský	k2eAgFnSc2d1
jeskyně	jeskyně	k1gFnSc2
•	•	k?
Channel	Channel	k1gInSc1
Islands	Islands	k1gInSc1
•	•	k?
Congaree	Congaree	k1gInSc1
•	•	k?
Crater	Crater	k1gInSc1
Lake	Lak	k1gMnSc2
•	•	k?
Cuyahoga	Cuyahog	k1gMnSc2
Valley	Vallea	k1gMnSc2
•	•	k?
Death	Death	k1gMnSc1
Valley	Vallea	k1gFnSc2
•	•	k?
Denali	Denali	k1gMnSc1
•	•	k?
Dry	Dry	k1gMnSc1
Tortugas	Tortugas	k1gMnSc1
•	•	k?
Everglades	Everglades	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Gates	Gates	k1gMnSc1
of	of	k?
the	the	k?
Arctic	Arctice	k1gFnPc2
•	•	k?
Glacier	Glacier	k1gMnSc1
•	•	k?
Glacier	Glacier	k1gMnSc1
Bay	Bay	k1gMnSc1
•	•	k?
Grand	grand	k1gMnSc1
Canyon	Canyon	k1gMnSc1
•	•	k?
Grand	grand	k1gMnSc1
Teton	Teton	k1gMnSc1
•	•	k?
Great	Great	k2eAgMnSc1d1
Basin	Basin	k1gMnSc1
•	•	k?
Great	Great	k2eAgMnSc1d1
Sand	Sand	k1gMnSc1
Dunes	Dunes	k1gMnSc1
•	•	k?
Great	Great	k1gInSc1
Smoky	smok	k1gMnPc4
Mountains	Mountainsa	k1gFnPc2
•	•	k?
Guadalupe	Guadalup	k1gInSc5
Mountains	Mountains	k1gInSc4
•	•	k?
Haleakalā	Haleakalā	k1gFnSc2
•	•	k?
Hawaiʻ	Hawaiʻ	k1gFnSc2
Volcanoes	Volcanoes	k1gMnSc1
•	•	k?
Hot	hot	k0
Springs	Springs	k1gInSc4
•	•	k?
Isle	Isl	k1gInSc2
Royale	Royala	k1gFnSc3
•	•	k?
Joshua	Joshua	k1gMnSc1
Tree	Tre	k1gFnSc2
•	•	k?
Katmai	Katma	k1gFnSc2
•	•	k?
Kenai	Kena	k1gFnSc2
Fjords	Fjordsa	k1gFnPc2
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Kings	Kings	k1gInSc1
Canyon	Canyon	k1gInSc4
•	•	k?
Kobuk	Kobuk	k1gInSc1
Valley	Vallea	k1gFnSc2
•	•	k?
Lake	Lake	k1gInSc1
Clark	Clark	k1gInSc1
•	•	k?
Lassen	Lassen	k1gInSc1
Volcanic	Volcanice	k1gFnPc2
•	•	k?
Mammoth	Mammoth	k1gMnSc1
Cave	Cav	k1gFnSc2
•	•	k?
Mesa	Mesa	k1gMnSc1
Verde	Verd	k1gInSc5
•	•	k?
Mount	Mounto	k1gNnPc2
Rainier	Rainier	k1gInSc1
•	•	k?
North	North	k1gInSc1
Cascades	Cascades	k1gInSc1
•	•	k?
Olympijský	olympijský	k2eAgInSc1d1
•	•	k?
Petrified	Petrified	k1gInSc1
Forest	Forest	k1gInSc1
•	•	k?
Pinnacles	Pinnacles	k1gInSc1
•	•	k?
Redwood	Redwooda	k1gFnPc2
•	•	k?
Rocky	rock	k1gInPc1
Mountain	Mountain	k1gInSc1
•	•	k?
Saguaro	Saguara	k1gFnSc5
•	•	k?
Sequoia	Sequoium	k1gNnSc2
•	•	k?
Shenandoah	Shenandoah	k1gMnSc1
•	•	k?
Theodore	Theodor	k1gMnSc5
Roosevelt	Roosevelt	k1gMnSc1
•	•	k?
Virgin	Virgin	k1gInSc1
Islands	Islands	k1gInSc1
•	•	k?
Voyageurs	Voyageurs	k1gInSc1
•	•	k?
Wind	Wind	k1gInSc1
Cave	Cave	k1gFnSc1
•	•	k?
Wrangell-	Wrangell-	k1gFnPc2
<g/>
St.	st.	kA
Elias	Elias	k1gInSc1
•	•	k?
Yellowstonský	Yellowstonský	k2eAgInSc1d1
•	•	k?
Yosemitský	Yosemitský	k2eAgInSc1d1
•	•	k?
Zion	Zion	k1gInSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4406824-4	4406824-4	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2017074862	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
236541927	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2017074862	#num#	k4
</s>
