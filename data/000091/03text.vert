<s>
Peru	Peru	k1gNnSc1	Peru
(	(	kIx(	(
<g/>
ve	v	k7c6	v
španělštině	španělština	k1gFnSc6	španělština
Perú	Perú	k1gFnSc2	Perú
<g/>
,	,	kIx,	,
kečuánsky	kečuánsky	k6eAd1	kečuánsky
Piruw	Piruw	k1gFnSc1	Piruw
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
stát	stát	k5eAaPmF	stát
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
sousedy	soused	k1gMnPc7	soused
jsou	být	k5eAaImIp3nP	být
Bolívie	Bolívie	k1gFnSc1	Bolívie
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc1	Brazílie
<g/>
,	,	kIx,	,
Chile	Chile	k1gNnSc1	Chile
<g/>
,	,	kIx,	,
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
a	a	k8xC	a
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
před	před	k7c7	před
necelými	celý	k2eNgNnPc7d1	necelé
pěti	pět	k4xCc7	pět
stoletími	století	k1gNnPc7	století
představovalo	představovat	k5eAaImAgNnS	představovat
území	území	k1gNnSc3	území
Peru	Peru	k1gNnSc2	Peru
centrum	centrum	k1gNnSc1	centrum
říše	říš	k1gFnSc2	říš
Inků	Ink	k1gMnPc2	Ink
táhnoucí	táhnoucí	k2eAgFnSc2d1	táhnoucí
se	se	k3xPyFc4	se
i	i	k9	i
dál	daleko	k6eAd2	daleko
na	na	k7c4	na
jih	jih	k1gInSc4	jih
a	a	k8xC	a
sever	sever	k1gInSc4	sever
<g/>
.	.	kIx.	.
</s>
<s>
Potomci	potomek	k1gMnPc1	potomek
Inků	Ink	k1gMnPc2	Ink
nadále	nadále	k6eAd1	nadále
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
Andách	Anda	k1gFnPc6	Anda
tradičním	tradiční	k2eAgNnSc7d1	tradiční
způsobem	způsob	k1gInSc7	způsob
života	život	k1gInSc2	život
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
polovinu	polovina	k1gFnSc4	polovina
peruánské	peruánský	k2eAgFnSc2d1	peruánská
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Lima	Lima	k1gFnSc1	Lima
<g/>
,	,	kIx,	,
státní	státní	k2eAgNnSc1d1	státní
zřízení	zřízení	k1gNnSc1	zřízení
je	být	k5eAaImIp3nS	být
republika	republika	k1gFnSc1	republika
a	a	k8xC	a
platnou	platný	k2eAgFnSc7d1	platná
měnou	měna	k1gFnSc7	měna
je	být	k5eAaImIp3nS	být
nuevo	nuevo	k1gNnSc1	nuevo
sol	sol	k1gNnSc2	sol
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
náboženstvím	náboženství	k1gNnSc7	náboženství
je	být	k5eAaImIp3nS	být
křesťanství	křesťanství	k1gNnSc4	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
Peru	Peru	k1gNnSc7	Peru
osídlovala	osídlovat	k5eAaImAgFnS	osídlovat
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgFnPc2d3	nejstarší
světových	světový	k2eAgFnPc2d1	světová
kultur	kultura	k1gFnPc2	kultura
Norte	Nort	k1gInSc5	Nort
Chico	Chico	k6eAd1	Chico
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
incká	incký	k2eAgFnSc1d1	incká
říše	říše	k1gFnSc1	říše
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc1d3	veliký
stát	stát	k1gInSc1	stát
předkolumbovské	předkolumbovský	k2eAgFnSc2d1	předkolumbovská
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
území	území	k1gNnSc1	území
bylo	být	k5eAaImAgNnS	být
podrobeno	podrobit	k5eAaPmNgNnS	podrobit
v	v	k7c4	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnPc2	století
Španělskem	Španělsko	k1gNnSc7	Španělsko
a	a	k8xC	a
následně	následně	k6eAd1	následně
na	na	k7c6	na
něm	on	k3xPp3gMnSc6	on
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
místokrálovství	místokrálovství	k1gNnSc1	místokrálovství
Peru	Peru	k1gNnSc2	Peru
<g/>
,	,	kIx,	,
pod	pod	k7c4	pod
které	který	k3yQgFnPc4	který
patřila	patřit	k5eAaImAgFnS	patřit
většina	většina	k1gFnSc1	většina
španělských	španělský	k2eAgFnPc2d1	španělská
jihoamerických	jihoamerický	k2eAgFnPc2d1	jihoamerická
kolonií	kolonie	k1gFnPc2	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
získání	získání	k1gNnSc6	získání
nezávislosti	nezávislost	k1gFnSc2	nezávislost
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1821	[number]	k4	1821
prošlo	projít	k5eAaPmAgNnS	projít
Peru	Peru	k1gNnSc1	Peru
obdobími	období	k1gNnPc7	období
politických	politický	k2eAgMnPc2d1	politický
nepokojů	nepokoj	k1gInPc2	nepokoj
<g/>
,	,	kIx,	,
finančních	finanční	k2eAgFnPc2d1	finanční
krizí	krize	k1gFnPc2	krize
jakož	jakož	k8xC	jakož
i	i	k8xC	i
fázemi	fáze	k1gFnPc7	fáze
stability	stabilita	k1gFnSc2	stabilita
a	a	k8xC	a
hospodářského	hospodářský	k2eAgInSc2d1	hospodářský
růstu	růst	k1gInSc2	růst
<g/>
.	.	kIx.	.
</s>
<s>
Peru	Peru	k1gNnSc1	Peru
je	být	k5eAaImIp3nS	být
republika	republika	k1gFnSc1	republika
se	s	k7c7	s
zastupitelskou	zastupitelský	k2eAgFnSc7d1	zastupitelská
demokracií	demokracie	k1gFnSc7	demokracie
<g/>
,	,	kIx,	,
rozdělená	rozdělený	k2eAgFnSc1d1	rozdělená
do	do	k7c2	do
25	[number]	k4	25
regionů	region	k1gInPc2	region
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
geografie	geografie	k1gFnSc1	geografie
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
od	od	k7c2	od
vyprahlých	vyprahlý	k2eAgFnPc2d1	vyprahlá
planin	planina	k1gFnPc2	planina
u	u	k7c2	u
tichomořského	tichomořský	k2eAgNnSc2d1	Tichomořské
pobřeží	pobřeží	k1gNnSc2	pobřeží
po	po	k7c4	po
hory	hora	k1gFnPc4	hora
And	Anda	k1gFnPc2	Anda
a	a	k8xC	a
amazonský	amazonský	k2eAgInSc1d1	amazonský
tropický	tropický	k2eAgInSc1d1	tropický
prales	prales	k1gInSc1	prales
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
rozvojová	rozvojový	k2eAgFnSc1d1	rozvojová
země	země	k1gFnSc1	země
se	s	k7c7	s
středním	střední	k2eAgInSc7d1	střední
indexem	index	k1gInSc7	index
lidského	lidský	k2eAgInSc2d1	lidský
rozvoje	rozvoj	k1gInSc2	rozvoj
a	a	k8xC	a
asi	asi	k9	asi
40	[number]	k4	40
<g/>
%	%	kIx~	%
úrovní	úroveň	k1gFnSc7	úroveň
chudoby	chudoba	k1gFnSc2	chudoba
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnPc4d1	hlavní
hospodářské	hospodářský	k2eAgFnPc4d1	hospodářská
činnosti	činnost	k1gFnPc4	činnost
patří	patřit	k5eAaImIp3nP	patřit
zemědělství	zemědělství	k1gNnSc1	zemědělství
<g/>
,	,	kIx,	,
rybolov	rybolov	k1gInSc1	rybolov
<g/>
,	,	kIx,	,
důlní	důlní	k2eAgInSc1d1	důlní
průmysl	průmysl	k1gInSc1	průmysl
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
zboží	zboží	k1gNnSc2	zboží
jako	jako	k8xC	jako
např.	např.	kA	např.
textil	textil	k1gInSc1	textil
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Peru	Peru	k1gNnSc6	Peru
je	být	k5eAaImIp3nS	být
žije	žít	k5eAaImIp3nS	žít
přibližně	přibližně	k6eAd1	přibližně
28	[number]	k4	28
milionů	milion	k4xCgInPc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
různých	různý	k2eAgFnPc2d1	různá
národností	národnost	k1gFnPc2	národnost
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
původních	původní	k2eAgMnPc2d1	původní
obyvatel	obyvatel	k1gMnPc2	obyvatel
Peru	Peru	k1gNnPc2	Peru
<g/>
,	,	kIx,	,
Evropanů	Evropan	k1gMnPc2	Evropan
<g/>
,	,	kIx,	,
Afričanů	Afričan	k1gMnPc2	Afričan
a	a	k8xC	a
Asiatů	Asiat	k1gMnPc2	Asiat
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
je	být	k5eAaImIp3nS	být
španělština	španělština	k1gFnSc1	španělština
hlavním	hlavní	k2eAgMnSc7d1	hlavní
jazykem	jazyk	k1gMnSc7	jazyk
<g/>
,	,	kIx,	,
hodně	hodně	k6eAd1	hodně
Peruánců	Peruánec	k1gMnPc2	Peruánec
používá	používat	k5eAaImIp3nS	používat
ajmarštinu	ajmarština	k1gFnSc4	ajmarština
a	a	k8xC	a
kečuánštinu	kečuánština	k1gFnSc4	kečuánština
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
vedle	vedle	k7c2	vedle
španělštiny	španělština	k1gFnSc2	španělština
také	také	k9	také
úředními	úřední	k2eAgInPc7d1	úřední
jazyky	jazyk	k1gInPc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
směs	směs	k1gFnSc1	směs
kulturních	kulturní	k2eAgFnPc2d1	kulturní
tradic	tradice	k1gFnPc2	tradice
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
důsledek	důsledek	k1gInSc4	důsledek
velkou	velký	k2eAgFnSc4d1	velká
rozmanitost	rozmanitost	k1gFnSc4	rozmanitost
v	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
hudby	hudba	k1gFnSc2	hudba
a	a	k8xC	a
kuchyně	kuchyně	k1gFnSc2	kuchyně
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
se	se	k3xPyFc4	se
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
na	na	k7c4	na
tři	tři	k4xCgInPc4	tři
svislé	svislý	k2eAgInPc4d1	svislý
pruhy	pruh	k1gInPc4	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Pravý	pravý	k2eAgInSc1d1	pravý
pruh	pruh	k1gInSc1	pruh
a	a	k8xC	a
levý	levý	k2eAgInSc1d1	levý
pruh	pruh	k1gInSc1	pruh
je	být	k5eAaImIp3nS	být
červený	červený	k2eAgInSc1d1	červený
<g/>
,	,	kIx,	,
prostřední	prostřední	k2eAgInSc1d1	prostřední
pruh	pruh	k1gInSc1	pruh
je	být	k5eAaImIp3nS	být
zářivě	zářivě	k6eAd1	zářivě
bílý	bílý	k2eAgInSc1d1	bílý
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
,	,	kIx,	,
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
krev	krev	k1gFnSc1	krev
bojovníků	bojovník	k1gMnPc2	bojovník
prolitou	prolitý	k2eAgFnSc4d1	prolitá
v	v	k7c6	v
boji	boj	k1gInSc6	boj
za	za	k7c4	za
svobodu	svoboda	k1gFnSc4	svoboda
a	a	k8xC	a
bílá	bílý	k2eAgFnSc1d1	bílá
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
právo	právo	k1gNnSc4	právo
a	a	k8xC	a
spravedlnost	spravedlnost	k1gFnSc4	spravedlnost
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Peru	Peru	k1gNnSc2	Peru
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
důkazy	důkaz	k1gInPc1	důkaz
lidské	lidský	k2eAgFnSc2d1	lidská
činnosti	činnost	k1gFnSc2	činnost
na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
území	území	k1gNnSc6	území
se	se	k3xPyFc4	se
datují	datovat	k5eAaImIp3nP	datovat
přibližně	přibližně	k6eAd1	přibližně
do	do	k7c2	do
roku	rok	k1gInSc2	rok
11	[number]	k4	11
000	[number]	k4	000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
známá	známý	k2eAgFnSc1d1	známá
peruánská	peruánský	k2eAgFnSc1d1	peruánská
kultura	kultura	k1gFnSc1	kultura
<g/>
,	,	kIx,	,
Norte	Nort	k1gMnSc5	Nort
Chico	Chica	k1gMnSc5	Chica
<g/>
,	,	kIx,	,
vzkvétala	vzkvétat	k5eAaImAgFnS	vzkvétat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
3	[number]	k4	3
000	[number]	k4	000
až	až	k9	až
1	[number]	k4	1
800	[number]	k4	800
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
podél	podél	k7c2	podél
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
další	další	k2eAgInSc4d1	další
se	se	k3xPyFc4	se
rozvíjely	rozvíjet	k5eAaImAgInP	rozvíjet
kultury	kultura	k1gFnSc2	kultura
Chavínská	Chavínský	k2eAgFnSc1d1	Chavínský
<g/>
,	,	kIx,	,
Paracaská	Paracaský	k2eAgFnSc1d1	Paracaský
<g/>
,	,	kIx,	,
Močická	Močický	k2eAgFnSc1d1	Močický
<g/>
,	,	kIx,	,
Nasca	Nasca	k1gMnSc1	Nasca
<g/>
,	,	kIx,	,
Vari	vari	k1gMnSc1	vari
a	a	k8xC	a
Čimú	Čimú	k1gMnSc1	Čimú
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Inkové	Ink	k1gMnPc1	Ink
seskupili	seskupit	k5eAaPmAgMnP	seskupit
do	do	k7c2	do
silného	silný	k2eAgInSc2d1	silný
státu	stát	k1gInSc2	stát
a	a	k8xC	a
během	během	k7c2	během
století	století	k1gNnSc2	století
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
největší	veliký	k2eAgFnSc4d3	veliký
říši	říše	k1gFnSc4	říše
v	v	k7c6	v
předkolumbovské	předkolumbovský	k2eAgFnSc6d1	předkolumbovská
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Andské	andský	k2eAgFnPc1d1	andská
kultury	kultura	k1gFnPc1	kultura
byly	být	k5eAaImAgFnP	být
zemědělské	zemědělský	k2eAgFnPc1d1	zemědělská
a	a	k8xC	a
využívaly	využívat	k5eAaImAgFnP	využívat
zavlažování	zavlažování	k1gNnSc4	zavlažování
a	a	k8xC	a
terasy	terasa	k1gFnPc1	terasa
<g/>
;	;	kIx,	;
důležitý	důležitý	k2eAgInSc1d1	důležitý
byl	být	k5eAaImAgInS	být
rybolov	rybolov	k1gInSc1	rybolov
a	a	k8xC	a
hospodaření	hospodaření	k1gNnSc1	hospodaření
s	s	k7c7	s
velbloudovitými	velbloudovitý	k2eAgMnPc7d1	velbloudovitý
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
tyto	tento	k3xDgFnPc1	tento
kultury	kultura	k1gFnPc1	kultura
neměly	mít	k5eNaImAgFnP	mít
pojem	pojem	k1gInSc4	pojem
o	o	k7c6	o
trhu	trh	k1gInSc6	trh
nebo	nebo	k8xC	nebo
penězích	peníze	k1gInPc6	peníze
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc1	jejich
spolupráce	spolupráce	k1gFnSc1	spolupráce
se	se	k3xPyFc4	se
spoléhala	spoléhat	k5eAaImAgFnS	spoléhat
na	na	k7c4	na
oboustrannost	oboustrannost	k1gFnSc4	oboustrannost
a	a	k8xC	a
přerozdělování	přerozdělování	k1gNnSc4	přerozdělování
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Geografie	geografie	k1gFnSc2	geografie
Peru	Peru	k1gNnSc1	Peru
<g/>
.	.	kIx.	.
</s>
<s>
Peru	Peru	k1gNnSc1	Peru
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgInSc4	třetí
největší	veliký	k2eAgInSc4d3	veliký
stát	stát	k1gInSc4	stát
jihoamerického	jihoamerický	k2eAgInSc2d1	jihoamerický
kontinentu	kontinent	k1gInSc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
rozděleno	rozdělen	k2eAgNnSc1d1	rozděleno
na	na	k7c4	na
tři	tři	k4xCgFnPc4	tři
oblasti	oblast	k1gFnPc4	oblast
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
táhnou	táhnout	k5eAaImIp3nP	táhnout
ve	v	k7c6	v
směru	směr	k1gInSc6	směr
sever-jih	severih	k1gInSc1	sever-jih
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západě	západ	k1gInSc6	západ
je	být	k5eAaImIp3nS	být
poušť	poušť	k1gFnSc1	poušť
Sechura	Sechura	k1gFnSc1	Sechura
<g/>
,	,	kIx,	,
nejširší	široký	k2eAgFnSc1d3	nejširší
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
mnoho	mnoho	k4c1	mnoho
velkých	velký	k2eAgFnPc2d1	velká
písečných	písečný	k2eAgFnPc2d1	písečná
dun	duna	k1gFnPc2	duna
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
na	na	k7c4	na
jih	jih	k1gInSc4	jih
je	být	k5eAaImIp3nS	být
pobřeží	pobřeží	k1gNnSc1	pobřeží
převážně	převážně	k6eAd1	převážně
skalnaté	skalnatý	k2eAgNnSc1d1	skalnaté
se	s	k7c7	s
souvislým	souvislý	k2eAgNnSc7d1	souvislé
pásmem	pásmo	k1gNnSc7	pásmo
hor.	hor.	k?	hor.
Z	z	k7c2	z
peruánských	peruánský	k2eAgFnPc2d1	peruánská
And	Anda	k1gFnPc2	Anda
protéká	protékat	k5eAaImIp3nS	protékat
pouští	poušť	k1gFnSc7	poušť
asi	asi	k9	asi
50	[number]	k4	50
řek	řeka	k1gFnPc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Andy	Anda	k1gFnPc1	Anda
se	se	k3xPyFc4	se
zvedají	zvedat	k5eAaImIp3nP	zvedat
směrem	směr	k1gInSc7	směr
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
až	až	k9	až
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
přes	přes	k7c4	přes
5000	[number]	k4	5000
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
je	být	k5eAaImIp3nS	být
Huascarán	Huascarán	k1gInSc1	Huascarán
(	(	kIx(	(
<g/>
6768	[number]	k4	6768
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
dvě	dva	k4xCgNnPc4	dva
horská	horský	k2eAgNnPc4d1	horské
pásma	pásmo	k1gNnPc4	pásmo
Kordiller	Kordillery	k1gFnPc2	Kordillery
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
dosud	dosud	k6eAd1	dosud
činné	činný	k2eAgFnPc1d1	činná
sopky	sopka	k1gFnPc1	sopka
a	a	k8xC	a
seismickou	seismický	k2eAgFnSc4d1	seismická
aktivitu	aktivita	k1gFnSc4	aktivita
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
rozdělena	rozdělit	k5eAaPmNgNnP	rozdělit
vysoko	vysoko	k6eAd1	vysoko
položenou	položený	k2eAgFnSc7d1	položená
náhorní	náhorní	k2eAgFnSc7d1	náhorní
planinou	planina	k1gFnSc7	planina
<g/>
,	,	kIx,	,
na	na	k7c6	na
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jezero	jezero	k1gNnSc1	jezero
Titicaca	Titicac	k1gInSc2	Titicac
(	(	kIx(	(
<g/>
6900	[number]	k4	6900
km2	km2	k4	km2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
straně	strana	k1gFnSc6	strana
And	Anda	k1gFnPc2	Anda
se	se	k3xPyFc4	se
vrcholky	vrcholek	k1gInPc1	vrcholek
pozvolna	pozvolna	k6eAd1	pozvolna
svažují	svažovat	k5eAaImIp3nP	svažovat
do	do	k7c2	do
amazonské	amazonský	k2eAgFnSc2d1	Amazonská
nížiny	nížina	k1gFnSc2	nížina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zabírá	zabírat	k5eAaImIp3nS	zabírat
62	[number]	k4	62
%	%	kIx~	%
území	území	k1gNnSc6	území
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Peru	Peru	k1gNnSc1	Peru
má	mít	k5eAaImIp3nS	mít
zhruba	zhruba	k6eAd1	zhruba
dvakrát	dvakrát	k6eAd1	dvakrát
více	hodně	k6eAd2	hodně
obyvatel	obyvatel	k1gMnPc2	obyvatel
než	než	k8xS	než
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
hustota	hustota	k1gFnSc1	hustota
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
(	(	kIx(	(
<g/>
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
na	na	k7c4	na
1	[number]	k4	1
km2	km2	k4	km2
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nízká	nízký	k2eAgFnSc1d1	nízká
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
žije	žít	k5eAaImIp3nS	žít
na	na	k7c6	na
úzkém	úzký	k2eAgInSc6d1	úzký
pruhu	pruh	k1gInSc6	pruh
země	zem	k1gFnSc2	zem
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Peruánské	peruánský	k2eAgInPc1d1	peruánský
národní	národní	k2eAgInPc1d1	národní
parky	park	k1gInPc1	park
(	(	kIx(	(
<g/>
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
celkem	celkem	k6eAd1	celkem
13	[number]	k4	13
parků	park	k1gInPc2	park
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgNnPc1d1	další
chráněná	chráněný	k2eAgNnPc1d1	chráněné
území	území	k1gNnPc1	území
pokrývají	pokrývat	k5eAaImIp3nP	pokrývat
necelých	celý	k2eNgInPc2d1	necelý
17	[number]	k4	17
%	%	kIx~	%
rozlohy	rozloha	k1gFnSc2	rozloha
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Peru	Peru	k1gNnSc2	Peru
je	být	k5eAaImIp3nS	být
Lima	Lima	k1gFnSc1	Lima
<g/>
.	.	kIx.	.
</s>
<s>
Limu	Lima	k1gFnSc4	Lima
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1535	[number]	k4	1535
španělský	španělský	k2eAgMnSc1d1	španělský
conquistador	conquistador	k1gMnSc1	conquistador
Francisco	Francisco	k6eAd1	Francisco
Pizarro	Pizarro	k1gNnSc1	Pizarro
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
rychle	rychle	k6eAd1	rychle
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
na	na	k7c4	na
6,5	[number]	k4	6,5
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
sem	sem	k6eAd1	sem
přišli	přijít	k5eAaPmAgMnP	přijít
při	při	k7c6	při
putování	putování	k1gNnSc6	putování
za	za	k7c7	za
prací	práce	k1gFnSc7	práce
<g/>
.	.	kIx.	.
</s>
<s>
Bohatí	bohatý	k2eAgMnPc1d1	bohatý
bydlí	bydlet	k5eAaImIp3nP	bydlet
v	v	k7c6	v
nových	nový	k2eAgInPc6d1	nový
obytných	obytný	k2eAgInPc6d1	obytný
domech	dům	k1gInPc6	dům
nebo	nebo	k8xC	nebo
vilách	vila	k1gFnPc6	vila
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
<g/>
,	,	kIx,	,
chudí	chudý	k1gMnPc1	chudý
obývají	obývat	k5eAaImIp3nP	obývat
calampas	calampas	k1gInSc4	calampas
<g/>
,	,	kIx,	,
rozlehlé	rozlehlý	k2eAgFnPc4d1	rozlehlá
přelidněné	přelidněný	k2eAgFnPc4d1	přelidněná
čtvrtě	čtvrt	k1gFnPc4	čtvrt
chatrčí	chatrč	k1gFnPc2	chatrč
vyrostlých	vyrostlý	k2eAgFnPc2d1	vyrostlá
na	na	k7c6	na
periferii	periferie	k1gFnSc6	periferie
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Jezero	jezero	k1gNnSc1	jezero
Titicaca	Titicacum	k1gNnSc2	Titicacum
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
3812	[number]	k4	3812
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
nejvýše	vysoce	k6eAd3	vysoce
položenou	položený	k2eAgFnSc7d1	položená
vodní	vodní	k2eAgFnSc7d1	vodní
nádrží	nádrž	k1gFnSc7	nádrž
s	s	k7c7	s
pravidelnou	pravidelný	k2eAgFnSc7d1	pravidelná
lodní	lodní	k2eAgFnSc7d1	lodní
dopravou	doprava	k1gFnSc7	doprava
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
a	a	k8xC	a
přes	přes	k7c4	přes
hranici	hranice	k1gFnSc4	hranice
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
do	do	k7c2	do
Bolívie	Bolívie	k1gFnSc2	Bolívie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
vodách	voda	k1gFnPc6	voda
odnepaměti	odnepaměti	k6eAd1	odnepaměti
lovili	lovit	k5eAaImAgMnP	lovit
místní	místní	k2eAgMnPc1d1	místní
Indiáni	Indián	k1gMnPc1	Indián
ryby	ryba	k1gFnSc2	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Indiánští	indiánský	k2eAgMnPc1d1	indiánský
Uruové	Uruus	k1gMnPc1	Uruus
žijí	žít	k5eAaImIp3nP	žít
na	na	k7c6	na
jezeře	jezero	k1gNnSc6	jezero
v	v	k7c6	v
obydlích	obydlí	k1gNnPc6	obydlí
postavených	postavený	k2eAgNnPc6d1	postavené
na	na	k7c6	na
několika	několik	k4yIc6	několik
desítkách	desítka	k1gFnPc6	desítka
umělých	umělý	k2eAgNnPc2d1	umělé
"	"	kIx"	"
<g/>
rákosových	rákosový	k2eAgNnPc2d1	rákosové
<g/>
"	"	kIx"	"
ostrůvků	ostrůvek	k1gInPc2	ostrůvek
z	z	k7c2	z
rostliny	rostlina	k1gFnSc2	rostlina
totora	totor	k1gMnSc2	totor
(	(	kIx(	(
<g/>
botanicky	botanicky	k6eAd1	botanicky
skřípinec	skřípinec	k1gInSc1	skřípinec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc7	jejich
tradiční	tradiční	k2eAgFnSc7d1	tradiční
obživou	obživa	k1gFnSc7	obživa
je	být	k5eAaImIp3nS	být
pěstování	pěstování	k1gNnSc1	pěstování
brambor	brambora	k1gFnPc2	brambora
<g/>
,	,	kIx,	,
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
mála	málo	k4c2	málo
užitkových	užitkový	k2eAgFnPc2d1	užitková
rostlin	rostlina	k1gFnPc2	rostlina
plodících	plodící	k2eAgFnPc2d1	plodící
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
výšce	výška	k1gFnSc6	výška
<g/>
,	,	kIx,	,
rybaření	rybařený	k2eAgMnPc1d1	rybařený
a	a	k8xC	a
lov	lov	k1gInSc1	lov
ptáků	pták	k1gMnPc2	pták
žijících	žijící	k2eAgMnPc2d1	žijící
na	na	k7c6	na
jezeře	jezero	k1gNnSc6	jezero
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc6	jeho
březích	břeh	k1gInPc6	břeh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
převážná	převážný	k2eAgFnSc1d1	převážná
část	část	k1gFnSc1	část
z	z	k7c2	z
asi	asi	k9	asi
1200	[number]	k4	1200
obyvatel	obyvatel	k1gMnPc2	obyvatel
žijících	žijící	k2eAgMnPc2d1	žijící
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
živí	živit	k5eAaImIp3nP	živit
turismem	turismus	k1gInSc7	turismus
a	a	k8xC	a
prodejem	prodej	k1gInSc7	prodej
ručně	ručně	k6eAd1	ručně
vyráběných	vyráběný	k2eAgInPc2d1	vyráběný
předmětů	předmět	k1gInPc2	předmět
<g/>
.	.	kIx.	.
</s>
<s>
Pásmo	pásmo	k1gNnSc1	pásmo
And	Anda	k1gFnPc2	Anda
dělí	dělit	k5eAaImIp3nS	dělit
celou	celý	k2eAgFnSc4d1	celá
zemi	zem	k1gFnSc4	zem
do	do	k7c2	do
několika	několik	k4yIc2	několik
klimatických	klimatický	k2eAgFnPc2d1	klimatická
oblastí	oblast	k1gFnPc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pouštích	poušť	k1gFnPc6	poušť
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
je	být	k5eAaImIp3nS	být
podnebí	podnebí	k1gNnSc1	podnebí
chladné	chladný	k2eAgNnSc1d1	chladné
a	a	k8xC	a
suché	suchý	k2eAgNnSc1d1	suché
díky	díky	k7c3	díky
vlivu	vliv	k1gInSc3	vliv
Humboldtova	Humboldtův	k2eAgInSc2d1	Humboldtův
proudu	proud	k1gInSc2	proud
<g/>
,	,	kIx,	,
na	na	k7c6	na
severním	severní	k2eAgNnSc6d1	severní
pobřeží	pobřeží	k1gNnSc6	pobřeží
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
vlivu	vliv	k1gInSc3	vliv
jevu	jev	k1gInSc2	jev
El	Ela	k1gFnPc2	Ela
Niñ	Niñ	k1gFnPc2	Niñ
občas	občas	k6eAd1	občas
(	(	kIx(	(
<g/>
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
pět	pět	k4xCc4	pět
či	či	k8xC	či
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
<g/>
)	)	kIx)	)
objevují	objevovat	k5eAaImIp3nP	objevovat
období	období	k1gNnPc1	období
horká	horký	k2eAgNnPc1d1	horké
a	a	k8xC	a
vlhká	vlhký	k2eAgNnPc1d1	vlhké
<g/>
.	.	kIx.	.
</s>
<s>
Podnebí	podnebí	k1gNnSc1	podnebí
And	Anda	k1gFnPc2	Anda
je	být	k5eAaImIp3nS	být
vysokohorské	vysokohorský	k2eAgNnSc1d1	vysokohorské
a	a	k8xC	a
rozdílné	rozdílný	k2eAgNnSc1d1	rozdílné
podle	podle	k7c2	podle
nadmořské	nadmořský	k2eAgFnSc2d1	nadmořská
výšky	výška	k1gFnSc2	výška
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severovýchodní	severovýchodní	k2eAgFnSc6d1	severovýchodní
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
spadá	spadat	k5eAaPmIp3nS	spadat
do	do	k7c2	do
Amazonské	amazonský	k2eAgFnSc2d1	Amazonská
nížiny	nížina	k1gFnSc2	nížina
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
horké	horký	k2eAgNnSc1d1	horké
a	a	k8xC	a
suché	suchý	k2eAgNnSc1d1	suché
klima	klima	k1gNnSc1	klima
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Lima	Lima	k1gFnSc1	Lima
ležící	ležící	k2eAgFnSc1d1	ležící
nedaleko	nedaleko	k7c2	nedaleko
pobřeží	pobřeží	k1gNnSc2	pobřeží
Tichého	Tichého	k2eAgInSc2d1	Tichého
oceánu	oceán	k1gInSc2	oceán
má	mít	k5eAaImIp3nS	mít
lednovou	lednový	k2eAgFnSc4d1	lednová
průměrnou	průměrný	k2eAgFnSc4d1	průměrná
teplotu	teplota	k1gFnSc4	teplota
23	[number]	k4	23
°	°	k?	°
<g/>
C	C	kA	C
a	a	k8xC	a
červencovou	červencový	k2eAgFnSc4d1	červencová
teplotu	teplota	k1gFnSc4	teplota
16	[number]	k4	16
°	°	k?	°
<g/>
C.	C.	kA	C.
Ročně	ročně	k6eAd1	ročně
zde	zde	k6eAd1	zde
spadne	spadnout	k5eAaPmIp3nS	spadnout
pouze	pouze	k6eAd1	pouze
50	[number]	k4	50
mm	mm	kA	mm
srážek	srážka	k1gFnPc2	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Peru	Peru	k1gNnSc1	Peru
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
ústavy	ústava	k1gFnSc2	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1979	[number]	k4	1979
republika	republika	k1gFnSc1	republika
s	s	k7c7	s
prezidentským	prezidentský	k2eAgInSc7d1	prezidentský
systémem	systém	k1gInSc7	systém
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
rozpustil	rozpustit	k5eAaPmAgMnS	rozpustit
prezident	prezident	k1gMnSc1	prezident
Alberto	Alberta	k1gFnSc5	Alberta
Fujimori	Fujimori	k1gNnSc7	Fujimori
dvoukomorový	dvoukomorový	k2eAgInSc1d1	dvoukomorový
parlament	parlament	k1gInSc1	parlament
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
ústava	ústava	k1gFnSc1	ústava
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
v	v	k7c6	v
referendu	referendum	k1gNnSc6	referendum
<g/>
,	,	kIx,	,
zavedla	zavést	k5eAaPmAgFnS	zavést
jednokomorový	jednokomorový	k2eAgInSc4d1	jednokomorový
parlament	parlament	k1gInSc4	parlament
a	a	k8xC	a
posílila	posílit	k5eAaPmAgFnS	posílit
moc	moc	k1gFnSc4	moc
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
byl	být	k5eAaImAgInS	být
zahájen	zahájit	k5eAaPmNgInS	zahájit
proces	proces	k1gInSc1	proces
decentralizace	decentralizace	k1gFnSc2	decentralizace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
v	v	k7c6	v
Peru	Peru	k1gNnSc6	Peru
probíhala	probíhat	k5eAaImAgFnS	probíhat
teroristická	teroristický	k2eAgFnSc1d1	teroristická
kampaň	kampaň	k1gFnSc1	kampaň
partyzánských	partyzánský	k2eAgFnPc2d1	Partyzánská
skupin	skupina	k1gFnPc2	skupina
hnutí	hnutí	k1gNnSc2	hnutí
zvané	zvaný	k2eAgNnSc1d1	zvané
Sendero	Sendero	k1gNnSc1	Sendero
luminoso	luminosa	k1gFnSc5	luminosa
(	(	kIx(	(
<g/>
Světlá	světlý	k2eAgFnSc1d1	světlá
stezka	stezka	k1gFnSc1	stezka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
několik	několik	k4yIc1	několik
desítek	desítka	k1gFnPc2	desítka
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Extrémisté	extrémista	k1gMnPc1	extrémista
chtěli	chtít	k5eAaImAgMnP	chtít
nastolit	nastolit	k5eAaPmF	nastolit
komunistický	komunistický	k2eAgInSc4d1	komunistický
maoistický	maoistický	k2eAgInSc4d1	maoistický
režim	režim	k1gInSc4	režim
a	a	k8xC	a
ovládnout	ovládnout	k5eAaPmF	ovládnout
zemi	zem	k1gFnSc4	zem
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
teroru	teror	k1gInSc2	teror
<g/>
.	.	kIx.	.
</s>
<s>
Vraždili	vraždit	k5eAaImAgMnP	vraždit
odpůrce	odpůrce	k1gMnSc4	odpůrce
a	a	k8xC	a
zastrašovali	zastrašovat	k5eAaImAgMnP	zastrašovat
je	být	k5eAaImIp3nS	být
bombovými	bombový	k2eAgInPc7d1	bombový
atentáty	atentát	k1gInPc7	atentát
ve	v	k7c6	v
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1992	[number]	k4	1992
byl	být	k5eAaImAgMnS	být
jejich	jejich	k3xOp3gMnSc1	jejich
vůdce	vůdce	k1gMnSc1	vůdce
Abimael	Abimaela	k1gFnPc2	Abimaela
Guzmán	Guzmán	k2eAgInSc1d1	Guzmán
zajat	zajat	k2eAgInSc1d1	zajat
a	a	k8xC	a
uvězněn	uvězněn	k2eAgInSc1d1	uvězněn
<g/>
.	.	kIx.	.
</s>
<s>
Peru	Peru	k1gNnSc1	Peru
je	být	k5eAaImIp3nS	být
zapojeno	zapojit	k5eAaPmNgNnS	zapojit
do	do	k7c2	do
několika	několik	k4yIc2	několik
projektů	projekt	k1gInPc2	projekt
a	a	k8xC	a
organizací	organizace	k1gFnPc2	organizace
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
latinskoamerické	latinskoamerický	k2eAgFnSc2d1	latinskoamerická
integrace	integrace	k1gFnSc2	integrace
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
členským	členský	k2eAgInSc7d1	členský
státem	stát	k1gInSc7	stát
např.	např.	kA	např.
organizací	organizace	k1gFnSc7	organizace
Společenství	společenství	k1gNnSc1	společenství
latinskoamerických	latinskoamerický	k2eAgInPc2d1	latinskoamerický
a	a	k8xC	a
karibských	karibský	k2eAgInPc2d1	karibský
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
Unie	unie	k1gFnSc1	unie
jihoamerických	jihoamerický	k2eAgInPc2d1	jihoamerický
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
Pacifická	pacifický	k2eAgFnSc1d1	Pacifická
aliance	aliance	k1gFnSc1	aliance
<g/>
,	,	kIx,	,
Latinskoamerický	latinskoamerický	k2eAgInSc1d1	latinskoamerický
hospodářský	hospodářský	k2eAgInSc4d1	hospodářský
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
Latinskoamerické	latinskoamerický	k2eAgNnSc4d1	latinskoamerické
integrační	integrační	k2eAgNnSc4d1	integrační
sdružení	sdružení	k1gNnSc4	sdružení
<g/>
,	,	kIx,	,
Andské	andský	k2eAgNnSc4d1	Andské
společenství	společenství	k1gNnSc4	společenství
a	a	k8xC	a
přidruženým	přidružený	k2eAgInSc7d1	přidružený
státem	stát	k1gInSc7	stát
Mercosuru	Mercosur	k1gInSc2	Mercosur
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Administrativní	administrativní	k2eAgNnPc4d1	administrativní
dělení	dělení	k1gNnPc4	dělení
Peru	prát	k5eAaImIp1nS	prát
a	a	k8xC	a
Seznam	seznam	k1gInSc1	seznam
měst	město	k1gNnPc2	město
v	v	k7c6	v
Peru	Peru	k1gNnSc6	Peru
<g/>
.	.	kIx.	.
</s>
<s>
Území	území	k1gNnSc1	území
Peru	Peru	k1gNnSc2	Peru
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
18	[number]	k4	18
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2002	[number]	k4	2002
spravováno	spravovat	k5eAaImNgNnS	spravovat
25	[number]	k4	25
regionálními	regionální	k2eAgFnPc7d1	regionální
vládami	vláda	k1gFnPc7	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
regionální	regionální	k2eAgFnSc1d1	regionální
vláda	vláda	k1gFnSc1	vláda
spravuje	spravovat	k5eAaImIp3nS	spravovat
jeden	jeden	k4xCgInSc1	jeden
departement	departement	k1gInSc1	departement
(	(	kIx(	(
<g/>
počet	počet	k1gInSc1	počet
24	[number]	k4	24
<g/>
)	)	kIx)	)
a	a	k8xC	a
konstituční	konstituční	k2eAgFnSc6d1	konstituční
provincii	provincie	k1gFnSc6	provincie
Callao	Callao	k6eAd1	Callao
<g/>
.	.	kIx.	.
</s>
<s>
Departementy	departement	k1gInPc1	departement
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
provincie	provincie	k1gFnPc4	provincie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
distrikty	distrikt	k1gInPc1	distrikt
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
je	být	k5eAaImIp3nS	být
celé	celý	k2eAgNnSc1d1	celé
území	území	k1gNnSc1	území
rozděleno	rozdělit	k5eAaPmNgNnS	rozdělit
do	do	k7c2	do
195	[number]	k4	195
provincií	provincie	k1gFnPc2	provincie
a	a	k8xC	a
1838	[number]	k4	1838
okresů	okres	k1gInPc2	okres
<g/>
.	.	kIx.	.
</s>
<s>
Provincie	provincie	k1gFnSc1	provincie
Lima	limo	k1gNnSc2	limo
nespadá	spadat	k5eNaImIp3nS	spadat
pod	pod	k7c4	pod
žádnou	žádný	k3yNgFnSc4	žádný
regionální	regionální	k2eAgFnSc4d1	regionální
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Departamenty	Departament	k1gInPc1	Departament
<g/>
/	/	kIx~	/
<g/>
regiony	region	k1gInPc1	region
<g/>
:	:	kIx,	:
Provincie	provincie	k1gFnSc1	provincie
<g/>
:	:	kIx,	:
Callao	Callao	k1gNnSc1	Callao
Lima	limo	k1gNnSc2	limo
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Ekonomika	ekonomika	k1gFnSc1	ekonomika
Peru	Peru	k1gNnSc2	Peru
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
Peru	Peru	k1gNnSc1	Peru
má	mít	k5eAaImIp3nS	mít
pouze	pouze	k6eAd1	pouze
3	[number]	k4	3
%	%	kIx~	%
orné	orný	k2eAgFnSc2d1	orná
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
36	[number]	k4	36
%	%	kIx~	%
ekonomicky	ekonomicky	k6eAd1	ekonomicky
aktivních	aktivní	k2eAgMnPc2d1	aktivní
obyvatel	obyvatel	k1gMnPc2	obyvatel
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
farmách	farma	k1gFnPc6	farma
<g/>
.	.	kIx.	.
21	[number]	k4	21
%	%	kIx~	%
půdy	půda	k1gFnSc2	půda
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
pro	pro	k7c4	pro
pastevectví	pastevectví	k1gNnSc4	pastevectví
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgFnPc7d1	hlavní
oblastmi	oblast	k1gFnPc7	oblast
pěstování	pěstování	k1gNnSc2	pěstování
plodin	plodina	k1gFnPc2	plodina
jsou	být	k5eAaImIp3nP	být
území	území	k1gNnSc2	území
podél	podél	k7c2	podél
řek	řeka	k1gFnPc2	řeka
protékajících	protékající	k2eAgFnPc2d1	protékající
pouští	poušť	k1gFnPc2	poušť
<g/>
.	.	kIx.	.
</s>
<s>
Cukrová	cukrový	k2eAgFnSc1d1	cukrová
třtina	třtina	k1gFnSc1	třtina
a	a	k8xC	a
bavlník	bavlník	k1gInSc1	bavlník
jsou	být	k5eAaImIp3nP	být
hlavními	hlavní	k2eAgFnPc7d1	hlavní
obchodními	obchodní	k2eAgFnPc7d1	obchodní
plodinami	plodina	k1gFnPc7	plodina
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
obživu	obživa	k1gFnSc4	obživa
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
rýže	rýže	k1gFnSc2	rýže
<g/>
.	.	kIx.	.
</s>
<s>
Vinná	vinný	k2eAgFnSc1d1	vinná
réva	réva	k1gFnSc1	réva
<g/>
,	,	kIx,	,
olivy	oliva	k1gFnPc1	oliva
<g/>
,	,	kIx,	,
tabák	tabák	k1gInSc1	tabák
<g/>
,	,	kIx,	,
pomeranče	pomeranč	k1gInPc1	pomeranč
a	a	k8xC	a
banány	banán	k1gInPc1	banán
se	se	k3xPyFc4	se
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
v	v	k7c6	v
Andách	Anda	k1gFnPc6	Anda
<g/>
,	,	kIx,	,
ve	v	k7c6	v
vyšších	vysoký	k2eAgFnPc6d2	vyšší
polohách	poloha	k1gFnPc6	poloha
pak	pak	k6eAd1	pak
obiloviny	obilovina	k1gFnSc2	obilovina
<g/>
,	,	kIx,	,
fazole	fazole	k1gFnSc2	fazole
<g/>
,	,	kIx,	,
a	a	k8xC	a
brambory	brambora	k1gFnPc1	brambora
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východních	východní	k2eAgInPc6d1	východní
svazích	svah	k1gInPc6	svah
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
půda	půda	k1gFnSc1	půda
pro	pro	k7c4	pro
chov	chov	k1gInSc4	chov
ovcí	ovce	k1gFnPc2	ovce
<g/>
,	,	kIx,	,
lam	lama	k1gFnPc2	lama
a	a	k8xC	a
alpak	alpaka	k1gFnPc2	alpaka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
i	i	k9	i
koka	koka	k1gFnSc1	koka
<g/>
.	.	kIx.	.
</s>
<s>
Chladný	chladný	k2eAgInSc1d1	chladný
Humboldtův	Humboldtův	k2eAgInSc1d1	Humboldtův
proud	proud	k1gInSc1	proud
přináší	přinášet	k5eAaImIp3nS	přinášet
obrovské	obrovský	k2eAgNnSc4d1	obrovské
množství	množství	k1gNnSc4	množství
planktonu	plankton	k1gInSc2	plankton
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
výborná	výborný	k2eAgFnSc1d1	výborná
potrava	potrava	k1gFnSc1	potrava
pro	pro	k7c4	pro
ryby	ryba	k1gFnPc4	ryba
<g/>
.	.	kIx.	.
</s>
<s>
Peru	Peru	k1gNnSc1	Peru
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgMnPc2d3	veliký
producentů	producent	k1gMnPc2	producent
ryb	ryba	k1gFnPc2	ryba
s	s	k7c7	s
průměrným	průměrný	k2eAgInSc7d1	průměrný
ročním	roční	k2eAgInSc7d1	roční
úlovkem	úlovek	k1gInSc7	úlovek
45	[number]	k4	45
milionů	milion	k4xCgInPc2	milion
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Hlavními	hlavní	k2eAgInPc7d1	hlavní
druhy	druh	k1gInPc7	druh
ryb	ryba	k1gFnPc2	ryba
jsou	být	k5eAaImIp3nP	být
sardele	sardel	k1gFnPc1	sardel
<g/>
,	,	kIx,	,
tuňák	tuňák	k1gMnSc1	tuňák
<g/>
,	,	kIx,	,
makrelovité	makrelovitý	k2eAgFnPc4d1	makrelovitý
ryby	ryba	k1gFnPc4	ryba
a	a	k8xC	a
sardinky	sardinka	k1gFnPc4	sardinka
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgMnSc1	každý
9	[number]	k4	9
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
10	[number]	k4	10
<g/>
.	.	kIx.	.
rok	rok	k1gInSc1	rok
však	však	k9	však
teplý	teplý	k2eAgInSc1d1	teplý
mořský	mořský	k2eAgInSc1d1	mořský
proud	proud	k1gInSc1	proud
El	Ela	k1gFnPc2	Ela
Niñ	Niñ	k1gMnPc2	Niñ
naruší	narušit	k5eAaPmIp3nS	narušit
rovnováhu	rovnováha	k1gFnSc4	rovnováha
organického	organický	k2eAgInSc2d1	organický
života	život	k1gInSc2	život
a	a	k8xC	a
ryby	ryba	k1gFnSc2	ryba
odplují	odplout	k5eAaPmIp3nP	odplout
za	za	k7c7	za
potravou	potrava	k1gFnSc7	potrava
jinam	jinam	k6eAd1	jinam
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
samozřejmě	samozřejmě	k6eAd1	samozřejmě
těžce	těžce	k6eAd1	těžce
postihuje	postihovat	k5eAaImIp3nS	postihovat
rybářský	rybářský	k2eAgInSc1d1	rybářský
průmysl	průmysl	k1gInSc1	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Obrovské	obrovský	k2eAgFnPc1d1	obrovská
lesní	lesní	k2eAgFnPc1d1	lesní
plochy	plocha	k1gFnPc1	plocha
ve	v	k7c6	v
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
jsou	být	k5eAaImIp3nP	být
těžko	těžko	k6eAd1	těžko
přístupné	přístupný	k2eAgInPc1d1	přístupný
a	a	k8xC	a
využitelné	využitelný	k2eAgInPc1d1	využitelný
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
přepravě	přeprava	k1gFnSc3	přeprava
některých	některý	k3yIgInPc2	některý
produktů	produkt	k1gInPc2	produkt
se	se	k3xPyFc4	se
využívají	využívat	k5eAaPmIp3nP	využívat
řeky	řeka	k1gFnPc1	řeka
a	a	k8xC	a
přístav	přístav	k1gInSc1	přístav
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Iquitos	Iquitosa	k1gFnPc2	Iquitosa
<g/>
.	.	kIx.	.
</s>
<s>
Peru	Peru	k1gNnSc1	Peru
má	mít	k5eAaImIp3nS	mít
bohatá	bohatý	k2eAgNnPc4d1	bohaté
naleziště	naleziště	k1gNnPc4	naleziště
různých	různý	k2eAgInPc2d1	různý
nerostů	nerost	k1gInPc2	nerost
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
mědi	měď	k1gFnSc2	měď
<g/>
,	,	kIx,	,
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
železa	železo	k1gNnSc2	železo
a	a	k8xC	a
zlata	zlato	k1gNnSc2	zlato
a	a	k8xC	a
také	také	k9	také
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
.	.	kIx.	.
</s>
<s>
Peru	Peru	k1gNnSc4	Peru
je	být	k5eAaImIp3nS	být
druhé	druhý	k4xOgFnPc1	druhý
na	na	k7c6	na
světě	svět	k1gInSc6	svět
v	v	k7c6	v
produkci	produkce	k1gFnSc6	produkce
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
páté	pátá	k1gFnPc4	pátá
na	na	k7c6	na
světě	svět	k1gInSc6	svět
v	v	k7c6	v
produkci	produkce	k1gFnSc6	produkce
zlata	zlato	k1gNnSc2	zlato
(	(	kIx(	(
<g/>
8,2	[number]	k4	8,2
%	%	kIx~	%
světové	světový	k2eAgFnSc2d1	světová
produkce	produkce	k1gFnSc2	produkce
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
první	první	k4xOgFnSc7	první
v	v	k7c6	v
produkci	produkce	k1gFnSc6	produkce
olova	olovo	k1gNnSc2	olovo
a	a	k8xC	a
druhé	druhý	k4xOgNnSc1	druhý
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
mědi	měď	k1gFnSc2	měď
<g/>
.	.	kIx.	.
</s>
<s>
Centrum	centrum	k1gNnSc1	centrum
těžby	těžba	k1gFnSc2	těžba
železné	železný	k2eAgFnSc2d1	železná
rudy	ruda	k1gFnSc2	ruda
je	být	k5eAaImIp3nS	být
Marcona	Marcona	k1gFnSc1	Marcona
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
,	,	kIx,	,
ropa	ropa	k1gFnSc1	ropa
se	se	k3xPyFc4	se
těží	těžet	k5eAaImIp3nS	těžet
v	v	k7c6	v
Amazonské	amazonský	k2eAgFnSc6d1	Amazonská
nížině	nížina	k1gFnSc6	nížina
<g/>
.	.	kIx.	.
</s>
<s>
Mnohá	mnohý	k2eAgNnPc1d1	mnohé
průmyslová	průmyslový	k2eAgNnPc1d1	průmyslové
odvětví	odvětví	k1gNnPc1	odvětví
souvisí	souviset	k5eAaImIp3nP	souviset
se	s	k7c7	s
zpracováním	zpracování	k1gNnSc7	zpracování
nerostů	nerost	k1gInPc2	nerost
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
umístěna	umístit	k5eAaPmNgFnS	umístit
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
And	Anda	k1gFnPc2	Anda
<g/>
.	.	kIx.	.
</s>
<s>
Průmysl	průmysl	k1gInSc1	průmysl
se	se	k3xPyFc4	se
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
ve	v	k7c6	v
městech	město	k1gNnPc6	město
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mnohých	mnohý	k2eAgInPc6d1	mnohý
přístavech	přístav	k1gInPc6	přístav
je	být	k5eAaImIp3nS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
výroba	výroba	k1gFnSc1	výroba
rybí	rybí	k2eAgFnSc2d1	rybí
moučky	moučka	k1gFnSc2	moučka
<g/>
,	,	kIx,	,
hojně	hojně	k6eAd1	hojně
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
je	být	k5eAaImIp3nS	být
průmysl	průmysl	k1gInSc1	průmysl
potravinářský	potravinářský	k2eAgInSc1d1	potravinářský
a	a	k8xC	a
textilní	textilní	k2eAgInSc1d1	textilní
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dalším	další	k2eAgNnPc3d1	další
odvětvím	odvětví	k1gNnPc3	odvětví
patří	patřit	k5eAaImIp3nS	patřit
výroba	výroba	k1gFnSc1	výroba
cementu	cement	k1gInSc2	cement
<g/>
,	,	kIx,	,
oceli	ocel	k1gFnSc2	ocel
<g/>
,	,	kIx,	,
stavba	stavba	k1gFnSc1	stavba
lodí	loď	k1gFnPc2	loď
<g/>
,	,	kIx,	,
montáž	montáž	k1gFnSc1	montáž
aut	aut	k1gInSc1	aut
aj.	aj.	kA	aj.
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
průmyslovou	průmyslový	k2eAgFnSc7d1	průmyslová
oblastí	oblast	k1gFnSc7	oblast
je	být	k5eAaImIp3nS	být
Lima	Lima	k1gFnSc1	Lima
Callao	Callao	k6eAd1	Callao
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
soustředěno	soustředit	k5eAaPmNgNnS	soustředit
70	[number]	k4	70
%	%	kIx~	%
veškerého	veškerý	k3xTgInSc2	veškerý
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
57	[number]	k4	57
%	%	kIx~	%
elektrické	elektrický	k2eAgFnSc2d1	elektrická
energie	energie	k1gFnSc2	energie
dodávají	dodávat	k5eAaImIp3nP	dodávat
hydroelektrárny	hydroelektrárna	k1gFnPc1	hydroelektrárna
<g/>
.	.	kIx.	.
</s>
<s>
Pouště	poušť	k1gFnPc1	poušť
při	při	k7c6	při
pobřeží	pobřeží	k1gNnSc6	pobřeží
<g/>
,	,	kIx,	,
zalesněné	zalesněný	k2eAgNnSc4d1	zalesněné
vnitrozemí	vnitrozemí	k1gNnSc4	vnitrozemí
a	a	k8xC	a
pohoří	pohoří	k1gNnSc4	pohoří
And	Anda	k1gFnPc2	Anda
působí	působit	k5eAaImIp3nP	působit
problémy	problém	k1gInPc1	problém
v	v	k7c6	v
dopravě	doprava	k1gFnSc6	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Důležitá	důležitý	k2eAgFnSc1d1	důležitá
je	být	k5eAaImIp3nS	být
doprava	doprava	k1gFnSc1	doprava
letecká	letecký	k2eAgFnSc1d1	letecká
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
většina	většina	k1gFnSc1	většina
Peruánců	Peruánec	k1gMnPc2	Peruánec
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
nemá	mít	k5eNaImIp3nS	mít
prostředky	prostředek	k1gInPc7	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejobdivuhodnějších	obdivuhodný	k2eAgFnPc2d3	nejobdivuhodnější
železnic	železnice	k1gFnPc2	železnice
(	(	kIx(	(
<g/>
transandská	transandský	k2eAgFnSc1d1	transandský
<g/>
)	)	kIx)	)
světa	svět	k1gInSc2	svět
vede	vést	k5eAaImIp3nS	vést
z	z	k7c2	z
Limy	Lima	k1gFnSc2	Lima
do	do	k7c2	do
vnitrozemí	vnitrozemí	k1gNnSc2	vnitrozemí
do	do	k7c2	do
La	la	k1gNnSc2	la
Oroya	Oroy	k1gInSc2	Oroy
<g/>
.	.	kIx.	.
</s>
<s>
Měří	měřit	k5eAaImIp3nS	měřit
170	[number]	k4	170
km	km	kA	km
a	a	k8xC	a
překonává	překonávat	k5eAaImIp3nS	překonávat
výšku	výška	k1gFnSc4	výška
4816	[number]	k4	4816
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Silnice	silnice	k1gFnPc1	silnice
v	v	k7c6	v
Peru	Peru	k1gNnSc6	Peru
sčítájí	sčítájet	k5eAaImIp3nS	sčítájet
kolem	kolem	k7c2	kolem
137000	[number]	k4	137000
km	km	kA	km
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
tvoří	tvořit	k5eAaImIp3nP	tvořit
tři	tři	k4xCgFnPc1	tři
velké	velký	k2eAgFnPc1d1	velká
skupiny	skupina	k1gFnPc1	skupina
<g/>
:	:	kIx,	:
dálnice	dálnice	k1gFnPc1	dálnice
<g/>
,	,	kIx,	,
vnitřní	vnitřní	k2eAgFnPc1d1	vnitřní
silnice	silnice	k1gFnPc1	silnice
a	a	k8xC	a
spojovací	spojovací	k2eAgFnPc1d1	spojovací
sítě	síť	k1gFnPc1	síť
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc4	ten
všechno	všechen	k3xTgNnSc4	všechen
spojuje	spojovat	k5eAaImIp3nS	spojovat
okresy	okres	k1gInPc1	okres
a	a	k8xC	a
hlavní	hlavní	k2eAgNnPc1d1	hlavní
města	město	k1gNnPc1	město
provincii	provincie	k1gFnSc6	provincie
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
zase	zase	k9	zase
dovoluje	dovolovat	k5eAaImIp3nS	dovolovat
všem	všecek	k3xTgMnPc3	všecek
občanům	občan	k1gMnPc3	občan
cestovat	cestovat	k5eAaImF	cestovat
svým	svůj	k3xOyFgNnSc7	svůj
autem	auto	k1gNnSc7	auto
nebo	nebo	k8xC	nebo
autobusem	autobus	k1gInSc7	autobus
mnoha	mnoho	k4c2	mnoho
linek	linka	k1gFnPc2	linka
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
20000	[number]	k4	20000
km	km	kA	km
má	mít	k5eAaImIp3nS	mít
pevný	pevný	k2eAgInSc4d1	pevný
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
dopravní	dopravní	k2eAgFnSc7d1	dopravní
spojnicí	spojnice	k1gFnSc7	spojnice
je	být	k5eAaImIp3nS	být
Panamerická	panamerický	k2eAgFnSc1d1	Panamerická
dálnice	dálnice	k1gFnSc1	dálnice
(	(	kIx(	(
<g/>
3337	[number]	k4	3337
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Machu	Mach	k1gMnSc3	Mach
Picchu	Picch	k1gMnSc3	Picch
<g/>
.	.	kIx.	.
</s>
<s>
Vysoko	vysoko	k6eAd1	vysoko
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
And	Anda	k1gFnPc2	Anda
vystavěné	vystavěný	k2eAgFnSc2d1	vystavěná
Machu	Mach	k1gMnSc3	Mach
Picchu	Picch	k1gInSc2	Picch
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
a	a	k8xC	a
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
významným	významný	k2eAgNnSc7d1	významné
inckým	incký	k2eAgNnSc7d1	incké
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3	nejstarší
nálezy	nález	k1gInPc1	nález
lidských	lidský	k2eAgFnPc2d1	lidská
kultur	kultura	k1gFnPc2	kultura
na	na	k7c6	na
peruánské	peruánský	k2eAgFnSc6d1	peruánská
půdě	půda	k1gFnSc6	půda
pocházely	pocházet	k5eAaImAgFnP	pocházet
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
asi	asi	k9	asi
20	[number]	k4	20
000	[number]	k4	000
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
-	-	kIx~	-
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Ayacucha	Ayacucha	k1gFnSc1	Ayacucha
<g/>
.	.	kIx.	.
</s>
<s>
Opevněné	opevněný	k2eAgNnSc1d1	opevněné
sídlo	sídlo	k1gNnSc1	sídlo
uniklo	uniknout	k5eAaPmAgNnS	uniknout
pozornosti	pozornost	k1gFnSc2	pozornost
španělských	španělský	k2eAgMnPc2d1	španělský
dobyvatelů	dobyvatel	k1gMnPc2	dobyvatel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
sem	sem	k6eAd1	sem
vtrhli	vtrhnout	k5eAaPmAgMnP	vtrhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1532	[number]	k4	1532
<g/>
,	,	kIx,	,
a	a	k8xC	a
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
zapomenuto	zapomnět	k5eAaImNgNnS	zapomnět
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
objevil	objevit	k5eAaPmAgMnS	objevit
americký	americký	k2eAgMnSc1d1	americký
archeolog	archeolog	k1gMnSc1	archeolog
Hiram	Hiram	k1gInSc4	Hiram
Bingham	Bingham	k1gInSc1	Bingham
<g/>
.	.	kIx.	.
</s>
<s>
Zachovaly	zachovat	k5eAaPmAgFnP	zachovat
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
zbytky	zbytek	k1gInPc1	zbytek
kultovních	kultovní	k2eAgInPc2d1	kultovní
objektů	objekt	k1gInPc2	objekt
a	a	k8xC	a
143	[number]	k4	143
obytných	obytný	k2eAgInPc2d1	obytný
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
Peru	Peru	k1gNnSc1	Peru
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městech	město	k1gNnPc6	město
žije	žít	k5eAaImIp3nS	žít
70	[number]	k4	70
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Velké	velký	k2eAgFnSc2d1	velká
Limy	Lima	k1gFnSc2	Lima
<g/>
.	.	kIx.	.
50	[number]	k4	50
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
tvoří	tvořit	k5eAaImIp3nP	tvořit
Indiáni	Indián	k1gMnPc1	Indián
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
kmenů	kmen	k1gInPc2	kmen
Kečua	Keču	k1gInSc2	Keču
a	a	k8xC	a
Aymará	Aymarý	k2eAgFnSc1d1	Aymará
<g/>
,	,	kIx,	,
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Amazonii	Amazonie	k1gFnSc6	Amazonie
žije	žít	k5eAaImIp3nS	žít
více	hodně	k6eAd2	hodně
malých	malý	k2eAgFnPc2d1	malá
etnických	etnický	k2eAgFnPc2d1	etnická
skupin	skupina	k1gFnPc2	skupina
včetně	včetně	k7c2	včetně
několika	několik	k4yIc2	několik
nekontaktovaných	kontaktovaný	k2eNgInPc2d1	kontaktovaný
indiánských	indiánský	k2eAgInPc2d1	indiánský
kmenů	kmen	k1gInPc2	kmen
<g/>
.	.	kIx.	.
37	[number]	k4	37
%	%	kIx~	%
mestici	mestic	k1gMnPc1	mestic
(	(	kIx(	(
<g/>
míšenci	míšenec	k1gMnPc1	míšenec
Evropanů	Evropan	k1gMnPc2	Evropan
a	a	k8xC	a
Indiánů	Indián	k1gMnPc2	Indián
<g/>
)	)	kIx)	)
a	a	k8xC	a
15	[number]	k4	15
%	%	kIx~	%
Evropané	Evropan	k1gMnPc1	Evropan
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
španělského	španělský	k2eAgInSc2d1	španělský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
třetiny	třetina	k1gFnPc1	třetina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
největší	veliký	k2eAgNnPc4d3	veliký
města	město	k1gNnPc4	město
v	v	k7c6	v
Andách	Anda	k1gFnPc6	Anda
patří	patřit	k5eAaImIp3nS	patřit
Arequipa	Arequipa	k1gFnSc1	Arequipa
<g/>
,	,	kIx,	,
Cuzco	Cuzco	k1gNnSc1	Cuzco
a	a	k8xC	a
Huancayo	Huancayo	k1gNnSc1	Huancayo
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnPc1d1	další
důležitá	důležitý	k2eAgNnPc1d1	důležité
města	město	k1gNnPc1	město
jsou	být	k5eAaImIp3nP	být
pří	pře	k1gFnSc7	pře
pobřeží	pobřeží	k1gNnSc2	pobřeží
-	-	kIx~	-
Callao	Callao	k1gNnSc1	Callao
<g/>
,	,	kIx,	,
Trujillo	Trujillo	k1gNnSc1	Trujillo
<g/>
,	,	kIx,	,
Chiclayo	Chiclayo	k1gNnSc1	Chiclayo
<g/>
,	,	kIx,	,
Piura	Piura	k1gFnSc1	Piura
a	a	k8xC	a
Chimbote	Chimbot	k1gMnSc5	Chimbot
<g/>
.	.	kIx.	.
</s>
<s>
Iquitos	Iquitos	k1gInSc1	Iquitos
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc1d3	veliký
amazonský	amazonský	k2eAgInSc1d1	amazonský
přístav	přístav	k1gInSc1	přístav
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
Peruánců	Peruánec	k1gMnPc2	Peruánec
jsou	být	k5eAaImIp3nP	být
katolíci	katolík	k1gMnPc1	katolík
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gNnSc4	jejich
vyznání	vyznání	k1gNnSc4	vyznání
často	často	k6eAd1	často
prolínají	prolínat	k5eAaImIp3nP	prolínat
staroindiánské	staroindiánský	k2eAgInPc4d1	staroindiánský
elementy	element	k1gInPc4	element
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Lima	limo	k1gNnSc2	limo
bydlí	bydlet	k5eAaImIp3nS	bydlet
čtvrtina	čtvrtina	k1gFnSc1	čtvrtina
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
většina	většina	k1gFnSc1	většina
ve	v	k7c6	v
slumech	slum	k1gInPc6	slum
kolem	kolem	k7c2	kolem
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Limě	Lima	k1gFnSc6	Lima
je	být	k5eAaImIp3nS	být
nejstarší	starý	k2eAgFnSc1d3	nejstarší
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
<g/>
,	,	kIx,	,
založená	založený	k2eAgFnSc1d1	založená
roku	rok	k1gInSc2	rok
1551	[number]	k4	1551
<g/>
.	.	kIx.	.
</s>
