<s>
Oscar	Oscar	k1gMnSc1
Soto	Soto	k1gMnSc1
</s>
<s>
Oscar	Oscar	k1gMnSc1
Soto	Soto	k1gMnSc1
Narození	narození	k1gNnSc4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1983	#num#	k4
(	(	kIx(
<g/>
37	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Ciudad	Ciudad	k1gInSc1
de	de	k?
México	México	k6eAd1
Povolání	povolání	k1gNnSc1
</s>
<s>
moderní	moderní	k2eAgMnSc1d1
pětibojař	pětibojař	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Panamerické	panamerický	k2eAgFnPc1d1
hry	hra	k1gFnPc1
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
Guadalajara	Guadalajara	k1gFnSc1
2011	#num#	k4
</s>
<s>
pětiboj	pětiboj	k1gInSc1
</s>
<s>
Středoamerické	středoamerický	k2eAgFnPc1d1
a	a	k8xC
karibské	karibský	k2eAgFnPc1d1
hry	hra	k1gFnPc1
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
Guatemala	Guatemala	k1gFnSc1
2010	#num#	k4
</s>
<s>
pětiboj	pětiboj	k1gInSc1
</s>
<s>
zlato	zlato	k1gNnSc1
</s>
<s>
Santo	Sant	k2eAgNnSc1d1
Domingo	Domingo	k1gNnSc1
2006	#num#	k4
</s>
<s>
pětiboj	pětiboj	k1gInSc1
</s>
<s>
Panamerické	panamerický	k2eAgNnSc1d1
mistrovství	mistrovství	k1gNnSc1
</s>
<s>
stříbro	stříbro	k1gNnSc1
</s>
<s>
Rio	Rio	k?
de	de	k?
Janeiro	Janeiro	k1gNnSc1
2002	#num#	k4
</s>
<s>
pětiboj	pětiboj	k1gInSc1
</s>
<s>
Oscar	Oscar	k1gMnSc1
Soto	Soto	k1gMnSc1
Carillo	Carillo	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
6	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1983	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
mexický	mexický	k2eAgMnSc1d1
moderní	moderní	k2eAgMnSc1d1
pětibojař	pětibojař	k1gMnSc1
<g/>
,	,	kIx,
olympionik	olympionik	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Je	být	k5eAaImIp3nS
dvojnásobným	dvojnásobný	k2eAgMnSc7d1
vítězem	vítěz	k1gMnSc7
Středoamerických	středoamerický	k2eAgInPc2d1
a	a	k8xC
karibských	karibský	k2eAgFnPc2d1
her	hra	k1gFnPc2
z	z	k7c2
let	léto	k1gNnPc2
2006	#num#	k4
a	a	k8xC
2010	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
březnu	březen	k1gInSc6
2009	#num#	k4
vybojoval	vybojovat	k5eAaPmAgMnS
druhé	druhý	k4xOgNnSc4
místo	místo	k1gNnSc4
na	na	k7c6
zahajovacím	zahajovací	k2eAgInSc6d1
závodu	závod	k1gInSc6
Světového	světový	k2eAgInSc2d1
poháru	pohár	k1gInSc2
v	v	k7c4
Mexico	Mexico	k1gNnSc4
City	city	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
získal	získat	k5eAaPmAgMnS
zlatou	zlatý	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
na	na	k7c6
Panamerických	panamerický	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
červnu	červen	k1gInSc6
2012	#num#	k4
se	se	k3xPyFc4
umístil	umístit	k5eAaPmAgMnS
na	na	k7c6
druhém	druhý	k4xOgNnSc6
místě	místo	k1gNnSc6
na	na	k7c6
Otevřeném	otevřený	k2eAgNnSc6d1
mistrovství	mistrovství	k1gNnSc6
Maďarska	Maďarsko	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Zúčastnil	zúčastnit	k5eAaPmAgMnS
se	se	k3xPyFc4
závodu	závod	k1gInSc2
v	v	k7c6
moderním	moderní	k2eAgInSc6d1
pětiboji	pětiboj	k1gInSc6
na	na	k7c6
Letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
2012	#num#	k4
v	v	k7c6
Londýně	Londýn	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
se	s	k7c7
ziskem	zisk	k1gInSc7
5	#num#	k4
656	#num#	k4
bodů	bod	k1gInPc2
umístil	umístit	k5eAaPmAgMnS
na	na	k7c6
14	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
Athlete	Athlet	k1gInSc5
Profile	profil	k1gInSc5
<g/>
:	:	kIx,
Oscar	Oscar	k1gInSc1
Soto	Soto	k6eAd1
Carrillo	Carrillo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pentathlon	Pentathlon	k1gInSc1
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
14	#num#	k4
<g/>
.	.	kIx.
<g/>
srpen	srpen	k1gInSc1
2012	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
.	.	kIx.
</s>
