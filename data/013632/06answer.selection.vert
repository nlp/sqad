<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
tenisová	tenisový	k2eAgFnSc1d1
federace	federace	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
ITF	ITF	kA
–	–	k?
International	International	k1gMnSc1
Tennis	Tennis	k1gFnSc2
Federation	Federation	k1gInSc1
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
ILTF	ILTF	kA
–	–	k?
International	International	k1gMnSc1
Lawn	Lawn	k1gMnSc1
Tennis	Tennis	k1gFnPc2
Federation	Federation	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgFnSc1d1
tenisová	tenisový	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
řídí	řídit	k5eAaImIp3nS
dění	dění	k1gNnSc4
ve	v	k7c6
světovém	světový	k2eAgInSc6d1
tenise	tenis	k1gInSc6
<g/>
.	.	kIx.
</s>