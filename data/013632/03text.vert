<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
tenisová	tenisový	k2eAgFnSc1d1
federace	federace	k1gFnSc1
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
tenisová	tenisový	k2eAgFnSc1d1
federace	federace	k1gFnSc1
Zkratka	zkratka	k1gFnSc1
</s>
<s>
ITF	ITF	kA
Vznik	vznik	k1gInSc1
</s>
<s>
1913	#num#	k4
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Londýn	Londýn	k1gInSc4
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
51	#num#	k4
<g/>
°	°	k?
<g/>
27	#num#	k4
<g/>
′	′	k?
<g/>
32	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
0	#num#	k4
<g/>
°	°	k?
<g/>
15	#num#	k4
<g/>
′	′	k?
<g/>
19	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Působnost	působnost	k1gFnSc1
</s>
<s>
Země	zem	k1gFnPc1
Oficiální	oficiální	k2eAgFnPc1d1
web	web	k1gInSc4
</s>
<s>
www.itftennis.com	www.itftennis.com	k1gInSc1
Dřívější	dřívější	k2eAgInSc4d1
název	název	k1gInSc4
</s>
<s>
ILTF	ILTF	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
tenisová	tenisový	k2eAgFnSc1d1
federace	federace	k1gFnSc1
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
ITF	ITF	kA
–	–	k?
International	International	k1gMnSc1
Tennis	Tennis	k1gFnSc2
Federation	Federation	k1gInSc1
<g/>
,	,	kIx,
dříve	dříve	k6eAd2
ILTF	ILTF	kA
–	–	k?
International	International	k1gMnSc1
Lawn	Lawn	k1gMnSc1
Tennis	Tennis	k1gFnPc2
Federation	Federation	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
mezinárodní	mezinárodní	k2eAgFnSc1d1
tenisová	tenisový	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
řídí	řídit	k5eAaImIp3nS
dění	dění	k1gNnSc4
ve	v	k7c6
světovém	světový	k2eAgInSc6d1
tenise	tenis	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
roku	rok	k1gInSc3
2008	#num#	k4
bylo	být	k5eAaImAgNnS
jejími	její	k3xOp3gInPc7
členy	člen	k1gInPc7
205	#num#	k4
národních	národní	k2eAgInPc2d1
tenisových	tenisový	k2eAgInPc2d1
svazů	svaz	k1gInPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
Českého	český	k2eAgInSc2d1
tenisového	tenisový	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
a	a	k8xC
současnost	současnost	k1gFnSc1
</s>
<s>
ILTF	ILTF	kA
byla	být	k5eAaImAgFnS
založena	založit	k5eAaPmNgFnS
v	v	k7c6
Paříži	Paříž	k1gFnSc6
(	(	kIx(
<g/>
Francie	Francie	k1gFnSc1
<g/>
)	)	kIx)
1	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1913	#num#	k4
zástupci	zástupce	k1gMnPc1
13	#num#	k4
národních	národní	k2eAgInPc2d1
tenisových	tenisový	k2eAgInPc2d1
svazů	svaz	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
změně	změna	k1gFnSc3
názvu	název	k1gInSc2
s	s	k7c7
vynecháním	vynechání	k1gNnSc7
Lawn	Lawna	k1gFnPc2
(	(	kIx(
<g/>
travnatý	travnatý	k2eAgInSc1d1
<g/>
)	)	kIx)
došlo	dojít	k5eAaPmAgNnS
na	na	k7c6
valném	valný	k2eAgNnSc6d1
shromáždění	shromáždění	k1gNnSc6
federace	federace	k1gFnSc2
v	v	k7c6
Hamburku	Hamburk	k1gInSc6
(	(	kIx(
<g/>
SRN	SRN	kA
<g/>
)	)	kIx)
v	v	k7c6
roce	rok	k1gInSc6
1977	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Sídlo	sídlo	k1gNnSc1
ITF	ITF	kA
je	být	k5eAaImIp3nS
v	v	k7c6
Londýně	Londýn	k1gInSc6
(	(	kIx(
<g/>
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
roku	rok	k1gInSc2
1987	#num#	k4
měla	mít	k5eAaImAgFnS
adresu	adresa	k1gFnSc4
<g/>
:	:	kIx,
Church	Church	k1gMnSc1
Road	Road	k1gMnSc1
<g/>
,	,	kIx,
Wimbledon	Wimbledon	k1gInSc1
<g/>
,	,	kIx,
London	London	k1gMnSc1
<g/>
,	,	kIx,
SW	SW	kA
19	#num#	k4
5	#num#	k4
<g/>
TF	tf	k0wR
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1988	#num#	k4
se	se	k3xPyFc4
přestěhovala	přestěhovat	k5eAaPmAgFnS
na	na	k7c4
Barons	Barons	k1gInSc4
Court	Courta	k1gFnPc2
<g/>
,	,	kIx,
blízko	blízko	k7c2
Queens	Queensa	k1gFnPc2
Clubu	club	k1gInSc2
a	a	k8xC
o	o	k7c4
deset	deset	k4xCc4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
znovu	znovu	k6eAd1
tentokrát	tentokrát	k6eAd1
na	na	k7c6
The	The	k1gFnSc6
Bank	bank	k1gInSc1
of	of	k?
England	England	k1gInSc1
Sports	Sports	k1gInSc1
Ground	Ground	k1gInSc4
<g/>
,	,	kIx,
Roehampton	Roehampton	k1gInSc4
v	v	k7c6
Londýně	Londýn	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Oficiální	oficiální	k2eAgNnSc1d1
sídlo	sídlo	k1gNnSc1
ITF	ITF	kA
</s>
<s>
The	The	k?
International	International	k1gFnSc1
Tennis	Tennis	k1gFnSc1
Federation	Federation	k1gInSc4
</s>
<s>
Bank	bank	k1gInSc1
Lane	Lan	k1gFnSc2
</s>
<s>
Roehampton	Roehampton	k1gInSc1
</s>
<s>
London	London	k1gMnSc1
</s>
<s>
SW15	SW15	k4
5XZ	5XZ	k4
</s>
<s>
United	United	k1gInSc1
Kingdom	Kingdom	k1gInSc1
</s>
<s>
Orgány	orgán	k1gInPc1
</s>
<s>
Nejvyššími	vysoký	k2eAgInPc7d3
orgány	orgán	k1gInPc7
ITF	ITF	kA
jsou	být	k5eAaImIp3nP
výroční	výroční	k2eAgNnPc1d1
shromáždění	shromáždění	k1gNnPc1
zástupců	zástupce	k1gMnPc2
všech	všecek	k3xTgFnPc2
členských	členský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
se	se	k3xPyFc4
scházejí	scházet	k5eAaImIp3nP
obvykle	obvykle	k6eAd1
po	po	k7c6
wimbledonském	wimbledonský	k2eAgInSc6d1
turnaji	turnaj	k1gInSc6
a	a	k8xC
řídící	řídící	k2eAgInSc1d1
výbor	výbor	k1gInSc1
(	(	kIx(
<g/>
Committee	Committee	k1gInSc1
of	of	k?
Management	management	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
řídí	řídit	k5eAaImIp3nS
celou	celý	k2eAgFnSc4d1
agendu	agenda	k1gFnSc4
federace	federace	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Soutěže	soutěž	k1gFnPc1
</s>
<s>
ITF	ITF	kA
pořádá	pořádat	k5eAaImIp3nS
3	#num#	k4
základní	základní	k2eAgFnSc2d1
týmové	týmový	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
založené	založený	k2eAgFnSc2d1
na	na	k7c6
národním	národní	k2eAgInSc6d1
charakteru	charakter	k1gInSc6
<g/>
:	:	kIx,
</s>
<s>
Davis	Davis	k1gInSc1
Cup	cup	k1gInSc1
(	(	kIx(
<g/>
muži	muž	k1gMnPc1
<g/>
)	)	kIx)
</s>
<s>
Fed	Fed	k?
Cup	cup	k1gInSc1
(	(	kIx(
<g/>
ženy	žena	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
Hopmanův	Hopmanův	k2eAgInSc1d1
pohár	pohár	k1gInSc1
(	(	kIx(
<g/>
smíšená	smíšený	k2eAgNnPc1d1
družstva	družstvo	k1gNnPc1
<g/>
)	)	kIx)
</s>
<s>
Dále	daleko	k6eAd2
organizuje	organizovat	k5eAaBmIp3nS
čtyři	čtyři	k4xCgFnPc4
Grand	grand	k1gMnSc1
Slamy	slam	k1gInPc7
<g/>
:	:	kIx,
</s>
<s>
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
</s>
<s>
French	French	k1gMnSc1
Open	Open	k1gMnSc1
</s>
<s>
Wimbledon	Wimbledon	k1gInSc1
</s>
<s>
US	US	kA
Open	Open	k1gMnSc1
</s>
<s>
Organizuje	organizovat	k5eAaBmIp3nS
mužský	mužský	k2eAgInSc4d1
a	a	k8xC
ženský	ženský	k2eAgInSc4d1
okruh	okruh	k1gInSc4
ITF	ITF	kA
<g/>
,	,	kIx,
tj.	tj.	kA
turnaje	turnaj	k1gInPc4
nejnižší	nízký	k2eAgFnSc2d3
kategorie	kategorie	k1gFnSc2
(	(	kIx(
<g/>
turnaje	turnaj	k1gInPc1
vyšších	vysoký	k2eAgFnPc2d2
kategorií	kategorie	k1gFnPc2
organizují	organizovat	k5eAaBmIp3nP
ATP	atp	kA
a	a	k8xC
WTA	WTA	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pořádá	pořádat	k5eAaImIp3nS
také	také	k9
turnajové	turnajový	k2eAgInPc4d1
okruhy	okruh	k1gInPc4
pro	pro	k7c4
věkové	věkový	k2eAgFnPc4d1
kategorie	kategorie	k1gFnPc4
tenistů	tenista	k1gMnPc2
do	do	k7c2
osmnácti	osmnáct	k4xCc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Mistři	mistr	k1gMnPc1
světa	svět	k1gInSc2
v	v	k7c6
tenise	tenis	k1gInSc6
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Mistři	mistr	k1gMnPc1
světa	svět	k1gInSc2
ITF	ITF	kA
<g/>
.	.	kIx.
</s>
<s>
Mistři	mistr	k1gMnPc1
světa	svět	k1gInSc2
ITF	ITF	kA
jsou	být	k5eAaImIp3nP
každoročně	každoročně	k6eAd1
vyhlašováni	vyhlašovat	k5eAaImNgMnP
od	od	k7c2
roku	rok	k1gInSc2
1978	#num#	k4
Mezinárodní	mezinárodní	k2eAgFnSc7d1
tenisovou	tenisový	k2eAgFnSc7d1
federací	federace	k1gFnSc7
na	na	k7c6
základě	základ	k1gInSc6
dosažených	dosažený	k2eAgInPc2d1
výsledků	výsledek	k1gInPc2
v	v	k7c6
uplyné	uplyný	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Mistři	mistr	k1gMnPc1
světa	svět	k1gInSc2
mužů	muž	k1gMnPc2
<g/>
,	,	kIx,
žen	žena	k1gFnPc2
<g/>
,	,	kIx,
juniorů	junior	k1gMnPc2
a	a	k8xC
juniorek	juniorka	k1gFnPc2
ve	v	k7c6
dvouhře	dvouhra	k1gFnSc6
jsou	být	k5eAaImIp3nP
vyhlašováni	vyhlašován	k2eAgMnPc1d1
od	od	k7c2
roku	rok	k1gInSc2
1978	#num#	k4
<g/>
,	,	kIx,
kategorie	kategorie	k1gFnSc1
juniorských	juniorský	k2eAgFnPc2d1
čtyřher	čtyřhra	k1gFnPc2
byly	být	k5eAaImAgFnP
doplněny	doplnit	k5eAaPmNgFnP
v	v	k7c6
roce	rok	k1gInSc6
1982	#num#	k4
<g/>
,	,	kIx,
vozíčkáři	vozíčkář	k1gMnPc1
pak	pak	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
a	a	k8xC
konečně	konečně	k6eAd1
mužská	mužský	k2eAgFnSc1d1
a	a	k8xC
ženská	ženský	k2eAgFnSc1d1
čtyřhra	čtyřhra	k1gFnSc1
se	se	k3xPyFc4
vyhlašuje	vyhlašovat	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
1996	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Juniorské	juniorský	k2eAgFnPc1d1
singlové	singlový	k2eAgFnPc1d1
a	a	k8xC
deblové	deblový	k2eAgFnPc1d1
kategorie	kategorie	k1gFnPc1
byly	být	k5eAaImAgFnP
v	v	k7c6
roce	rok	k1gInSc6
2003	#num#	k4
sjednoceny	sjednotit	k5eAaPmNgFnP
do	do	k7c2
jediné	jediný	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Lichner	Lichner	k1gMnSc1
<g/>
,	,	kIx,
I.	I.	kA
et	et	k?
al	ala	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Malá	malý	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
tenisu	tenis	k1gInSc2
<g/>
,	,	kIx,
Olympia	Olympia	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Český	český	k2eAgInSc1d1
tenisový	tenisový	k2eAgInSc1d1
svaz	svaz	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Mezinárodní	mezinárodní	k2eAgFnSc1d1
tenisová	tenisový	k2eAgFnSc1d1
federace	federace	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
ITF	ITF	kA
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Tenis	tenis	k1gInSc1
Organizace	organizace	k1gFnSc2
</s>
<s>
ITF	ITF	kA
</s>
<s>
ATP	atp	kA
</s>
<s>
WTA	WTA	kA
</s>
<s>
ČTS	ČTS	kA
</s>
<s>
FFT	fft	k0
</s>
<s>
TA	ten	k3xDgFnSc1
</s>
<s>
USTA	USTA	kA
Okruhy	okruh	k1gInPc1
</s>
<s>
mužské	mužský	k2eAgNnSc1d1
</s>
<s>
ATP	atp	kA
Tour	Tour	k1gMnSc1
</s>
<s>
ATP	atp	kA
Challenger	Challenger	k1gMnSc1
Tour	Tour	k1gMnSc1
</s>
<s>
ITF	ITF	kA
Men	Men	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
World	World	k1gInSc1
Tennis	Tennis	k1gFnSc2
Tour	Toura	k1gFnPc2
ženské	ženský	k2eAgFnSc2d1
</s>
<s>
WTA	WTA	kA
Tour	Tour	k1gMnSc1
</s>
<s>
WTA	WTA	kA
125	#num#	k4
<g/>
s	s	k7c7
</s>
<s>
ITF	ITF	kA
Women	Women	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
World	World	k1gInSc1
Tennis	Tennis	k1gFnSc1
Tour	Tour	k1gMnSc1
</s>
<s>
Grand	grand	k1gMnSc1
Slam	sláma	k1gFnPc2
</s>
<s>
Australian	Australian	k1gMnSc1
Open	Open	k1gMnSc1
</s>
<s>
French	French	k1gMnSc1
Open	Open	k1gMnSc1
</s>
<s>
Wimbledon	Wimbledon	k1gInSc1
</s>
<s>
US	US	kA
Open	Open	k1gMnSc1
Údery	úder	k1gInPc7
</s>
<s>
bekhend	bekhend	k1gInSc1
</s>
<s>
čop	čop	k?
</s>
<s>
forhend	forhend	k1gInSc1
</s>
<s>
halfvolej	halfvolej	k1gInSc1
</s>
<s>
lob	lob	k1gInSc1
</s>
<s>
podání	podání	k1gNnSc1
</s>
<s>
smeč	smeč	k1gFnSc1
</s>
<s>
topspin	topspin	k1gInSc1
</s>
<s>
volej	volej	k1gInSc1
</s>
<s>
zkrácení	zkrácení	k1gNnSc4
hry	hra	k1gFnSc2
Dvorce	dvorec	k1gInPc4
a	a	k8xC
povrchy	povrch	k1gInPc4
</s>
<s>
antuka	antuka	k1gFnSc1
</s>
<s>
tráva	tráva	k1gFnSc1
</s>
<s>
tvrdý	tvrdý	k2eAgMnSc1d1
(	(	kIx(
<g/>
DecoTurf	DecoTurf	k1gMnSc1
•	•	k?
GreenSet	GreenSet	k1gMnSc1
•	•	k?
Laykold	Laykold	k1gMnSc1
•	•	k?
Plexicushion	Plexicushion	k1gInSc1
•	•	k?
Rebound	Rebound	k1gInSc1
Ace	Ace	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
hala	hala	k1gFnSc1
Vybavení	vybavení	k1gNnSc2
</s>
<s>
míč	míč	k1gInSc1
(	(	kIx(
<g/>
melton	melton	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
raketa	raketa	k1gFnSc1
</s>
<s>
výplet	výplet	k1gInSc1
Týmové	týmový	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
</s>
<s>
Davis	Davis	k1gInSc1
Cup	cup	k1gInSc1
</s>
<s>
Billie	Billie	k1gFnSc1
Jean	Jean	k1gMnSc1
King	King	k1gMnSc1
Cup	cup	k1gInSc4
</s>
<s>
Hopmanův	Hopmanův	k2eAgInSc4d1
pohár	pohár	k1gInSc4
</s>
<s>
ATP	atp	kA
Cup	cup	k1gInSc1
</s>
<s>
Laver	lavra	k1gFnPc2
Cup	cup	k0
</s>
<s>
Světový	světový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
družstev	družstvo	k1gNnPc2
</s>
<s>
Galeův	Galeův	k2eAgInSc4d1
pohár	pohár	k1gInSc4
Ostatní	ostatní	k2eAgFnSc2d1
</s>
<s>
Letní	letní	k2eAgFnPc1d1
olympijské	olympijský	k2eAgFnPc1d1
hry	hra	k1gFnPc1
</s>
<s>
terminologie	terminologie	k1gFnSc1
</s>
<s>
rozhodčí	rozhodčí	k1gMnSc1
</s>
<s>
jestřábí	jestřábí	k2eAgNnSc4d1
oko	oko	k1gNnSc4
</s>
<s>
žebříček	žebříček	k1gInSc4
ATP	atp	kA
a	a	k8xC
WTA	WTA	kA
</s>
<s>
historie	historie	k1gFnSc1
tenisu	tenis	k1gInSc2
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
tenisová	tenisový	k2eAgFnSc1d1
síň	síň	k1gFnSc1
slávy	sláva	k1gFnSc2
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc1d1
sportovní	sportovní	k2eAgFnSc1d1
federace	federace	k1gFnSc1
ASOIF	ASOIF	kA
(	(	kIx(
<g/>
28	#num#	k4
<g/>
)	)	kIx)
<g/>
Federace	federace	k1gFnSc1
letních	letní	k2eAgFnPc2d1
olympiád	olympiáda	k1gFnPc2
</s>
<s>
IAAF	IAAF	kA
(	(	kIx(
<g/>
atletika	atletika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
BWF	BWF	kA
(	(	kIx(
<g/>
badminton	badminton	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIBA	FIBA	kA
(	(	kIx(
<g/>
basketbal	basketbal	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
AIBA	AIBA	kA
(	(	kIx(
<g/>
box	box	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
UCI	UCI	kA
(	(	kIx(
<g/>
cyklistika	cyklistika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
FIFA	FIFA	kA
(	(	kIx(
<g/>
fotbal	fotbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IGF	IGF	kA
(	(	kIx(
<g/>
golf	golf	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
FIG	FIG	kA
(	(	kIx(
<g/>
gymnastika	gymnastik	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
IHF	IHF	kA
(	(	kIx(
<g/>
házená	házená	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ISAF	ISAF	kA
(	(	kIx(
<g/>
jachting	jachting	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FEI	FEI	kA
(	(	kIx(
<g/>
jezdectví	jezdectví	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
IJF	IJF	kA
(	(	kIx(
<g/>
judo	judo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ICF	ICF	kA
(	(	kIx(
<g/>
kanoistika	kanoistika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
WA	WA	kA
(	(	kIx(
<g/>
lukostřelba	lukostřelba	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
UIPM	UIPM	kA
(	(	kIx(
<g/>
moderní	moderní	k2eAgInSc4d1
pětiboj	pětiboj	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
FIH	FIH	kA
(	(	kIx(
<g/>
pozemní	pozemní	k2eAgInSc4d1
hokej	hokej	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
WR	WR	kA
(	(	kIx(
<g/>
ragby	ragby	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ISSF	ISSF	kA
(	(	kIx(
<g/>
sportovní	sportovní	k2eAgFnSc1d1
střelba	střelba	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
ITTF	ITTF	kA
(	(	kIx(
<g/>
stolní	stolní	k2eAgInSc1d1
tenis	tenis	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIE	FIE	kA
(	(	kIx(
<g/>
šerm	šerm	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WTF	WTF	kA
(	(	kIx(
<g/>
taekwondo	taekwondo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ITF	ITF	kA
(	(	kIx(
<g/>
tenis	tenis	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ITU	ITU	kA
(	(	kIx(
<g/>
triatlon	triatlon	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FISA	FISA	kA
(	(	kIx(
<g/>
veslování	veslování	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
FINA	Fina	k1gFnSc1
(	(	kIx(
<g/>
vodní	vodní	k2eAgInPc1d1
sporty	sport	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
FIVB	FIVB	kA
(	(	kIx(
<g/>
volejbal	volejbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IWF	IWF	kA
(	(	kIx(
<g/>
vzpírání	vzpírání	k1gNnSc4
<g/>
)	)	kIx)
</s>
<s>
UWW	UWW	kA
(	(	kIx(
<g/>
zápas	zápas	k1gInSc1
<g/>
)	)	kIx)
AIOWF	AIOWF	kA
(	(	kIx(
<g/>
7	#num#	k4
<g/>
)	)	kIx)
<g/>
Federace	federace	k1gFnSc1
zimních	zimní	k2eAgFnPc2d1
olympiád	olympiáda	k1gFnPc2
</s>
<s>
IBU	IBU	kA
(	(	kIx(
<g/>
biatlon	biatlon	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IBSF	IBSF	kA
(	(	kIx(
<g/>
boby	bob	k1gInPc1
a	a	k8xC
skeleton	skeleton	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ISU	ISU	kA
(	(	kIx(
<g/>
bruslení	bruslení	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
WCF	WCF	kA
(	(	kIx(
<g/>
curling	curling	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IIHF	IIHF	kA
(	(	kIx(
<g/>
lední	lední	k2eAgInSc4d1
hokej	hokej	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
FIS	FIS	kA
(	(	kIx(
<g/>
lyžařské	lyžařský	k2eAgInPc4d1
sporty	sport	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
FIL	FIL	kA
(	(	kIx(
<g/>
sáňkařský	sáňkařský	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
ARISF	ARISF	kA
(	(	kIx(
<g/>
37	#num#	k4
<g/>
)	)	kIx)
<g/>
Další	další	k2eAgFnSc1d1
federace	federace	k1gFnSc1
uznané	uznaný	k2eAgFnSc2d1
MOV	MOV	kA
</s>
<s>
IFAF	IFAF	kA
(	(	kIx(
<g/>
americký	americký	k2eAgInSc1d1
fotbal	fotbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIA	FIA	kA
(	(	kIx(
<g/>
automobilový	automobilový	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIB	FIB	kA
(	(	kIx(
<g/>
bandy	bandy	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
WBSC	WBSC	kA
(	(	kIx(
<g/>
baseball	baseball	k1gInSc1
a	a	k8xC
softball	softball	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WB	WB	kA
(	(	kIx(
<g/>
bowling	bowling	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WBF	WBF	kA
(	(	kIx(
<g/>
bridž	bridž	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IFF	IFF	kA
(	(	kIx(
<g/>
florbal	florbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
UIAA	UIAA	kA
(	(	kIx(
<g/>
horolezectví	horolezectví	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ICU	ICU	kA
(	(	kIx(
<g/>
cheerleading	cheerleading	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WKF	WKF	kA
(	(	kIx(
<g/>
karate	karate	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ICC	ICC	kA
(	(	kIx(
<g/>
kriket	kriket	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIRS	FIRS	kA
(	(	kIx(
<g/>
kolečkové	kolečkový	k2eAgInPc4d1
sporty	sport	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
IKF	IKF	kA
(	(	kIx(
<g/>
korfbal	korfbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
CMSB	CMSB	kA
(	(	kIx(
<g/>
koulové	koulový	k2eAgInPc4d1
sporty	sport	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
WCBS	WCBS	kA
(	(	kIx(
<g/>
kulečník	kulečník	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FAI	FAI	kA
(	(	kIx(
<g/>
letecký	letecký	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIM	FIM	kA
(	(	kIx(
<g/>
motocyklový	motocyklový	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IFMA	IFMA	kA
(	(	kIx(
<g/>
muay	mua	k2eAgFnPc1d1
thai	tha	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
IFNA	IFNA	kA
(	(	kIx(
<g/>
netball	netball	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
IOF	IOF	kA
(	(	kIx(
<g/>
orientační	orientační	k2eAgInSc1d1
běh	běh	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIPV	FIPV	kA
(	(	kIx(
<g/>
pelota	pelota	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
CMAS	CMAS	kA
(	(	kIx(
<g/>
podvodní	podvodní	k2eAgInPc4d1
sporty	sport	k1gInPc4
<g/>
)	)	kIx)
</s>
<s>
FIP	FIP	kA
(	(	kIx(
<g/>
pólo	pólo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
TWIF	TWIF	kA
(	(	kIx(
<g/>
přetahování	přetahování	k1gNnPc2
lanem	lano	k1gNnSc7
<g/>
)	)	kIx)
</s>
<s>
IRF	IRF	kA
(	(	kIx(
<g/>
raketbal	raketbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ISMF	ISMF	kA
(	(	kIx(
<g/>
skialpinismus	skialpinismus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IFSC	IFSC	kA
(	(	kIx(
<g/>
sportovní	sportovní	k2eAgNnSc1d1
lezení	lezení	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
WSF	WSF	kA
(	(	kIx(
<g/>
squash	squash	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IFS	IFS	kA
(	(	kIx(
<g/>
sumo	suma	k1gFnSc5
<g/>
)	)	kIx)
</s>
<s>
ISA	ISA	kA
(	(	kIx(
<g/>
surfing	surfing	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIDE	FIDE	kA
(	(	kIx(
<g/>
šachy	šach	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
WDSF	WDSF	kA
(	(	kIx(
<g/>
taneční	taneční	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WFDF	WFDF	kA
(	(	kIx(
<g/>
ultimate	ultimat	k1gInSc5
frisbee	frisbe	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
IWWF	IWWF	kA
(	(	kIx(
<g/>
vodní	vodní	k2eAgNnSc1d1
lyžování	lyžování	k1gNnSc1
a	a	k8xC
wakeboarding	wakeboarding	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
UIM	UIM	kA
(	(	kIx(
<g/>
vodní	vodní	k2eAgInSc1d1
motorismus	motorismus	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IWUF	IWUF	kA
(	(	kIx(
<g/>
wu-šu	wu-sat	k5eAaPmIp1nS
<g/>
)	)	kIx)
</s>
<s>
ILSF	ILSF	kA
(	(	kIx(
<g/>
záchranářský	záchranářský	k2eAgInSc1d1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
Ostatní	ostatní	k2eAgFnSc1d1
federace	federace	k1gFnSc1
ve	v	k7c6
SportAccordu	SportAccordo	k1gNnSc6
(	(	kIx(
<g/>
21	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
IAF	IAF	kA
(	(	kIx(
<g/>
aikido	aikida	k1gFnSc5
<g/>
)	)	kIx)
</s>
<s>
FMJD	FMJD	kA
(	(	kIx(
<g/>
dáma	dáma	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
IDBF	IDBF	kA
(	(	kIx(
<g/>
dračí	dračí	k2eAgFnPc1d1
lodě	loď	k1gFnPc1
<g/>
)	)	kIx)
</s>
<s>
JJIF	JJIF	kA
(	(	kIx(
<g/>
džú-džucu	džú-džucu	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
IFA	IFA	kA
(	(	kIx(
<g/>
faustball	faustball	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
IGF	IGF	kA
(	(	kIx(
<g/>
go	go	k?
<g/>
)	)	kIx)
</s>
<s>
IFI	IFI	kA
(	(	kIx(
<g/>
ice	ice	k?
stock	stock	k1gInSc1
sport	sport	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIK	fik	k0
(	(	kIx(
<g/>
kendó	kendó	k?
<g/>
)	)	kIx)
</s>
<s>
WAKO	WAKO	kA
(	(	kIx(
<g/>
kickboxing	kickboxing	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IFBB	IFBB	kA
(	(	kIx(
<g/>
kulturistika	kulturistika	k1gFnSc1
a	a	k8xC
fitness	fitness	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIL	FIL	kA
(	(	kIx(
<g/>
lakros	lakros	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WMF	WMF	kA
(	(	kIx(
<g/>
minigolf	minigolf	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ISFF	ISFF	kA
(	(	kIx(
<g/>
psí	psí	k2eAgNnSc1d1
spřežení	spřežení	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
ICSF	ICSF	kA
(	(	kIx(
<g/>
rybolovná	rybolovný	k2eAgFnSc1d1
technika	technika	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
FIAS	FIAS	kA
(	(	kIx(
<g/>
sambo	samba	k1gFnSc5
<g/>
)	)	kIx)
</s>
<s>
FISav	FISav	k1gInSc4
(	(	kIx(
<g/>
savate	savat	k1gMnSc5
<g/>
)	)	kIx)
</s>
<s>
ISTAF	ISTAF	kA
(	(	kIx(
<g/>
sepak	sepak	k1gMnSc1
takraw	takraw	k?
<g/>
)	)	kIx)
</s>
<s>
IPF	IPF	kA
(	(	kIx(
<g/>
silový	silový	k2eAgInSc1d1
trojboj	trojboj	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ISTF	ISTF	kA
(	(	kIx(
<g/>
soft	soft	k?
tenis	tenis	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
CIPS	CIPS	kA
(	(	kIx(
<g/>
sportovní	sportovní	k2eAgInSc1d1
rybolov	rybolov	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WDF	WDF	kA
(	(	kIx(
<g/>
šipky	šipka	k1gFnPc1
<g/>
)	)	kIx)
Jiné	jiný	k2eAgFnPc1d1
federace	federace	k1gFnPc1
(	(	kIx(
<g/>
19	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
WAF	WAF	kA
(	(	kIx(
<g/>
armwrestling	armwrestling	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ARI	ARI	kA
(	(	kIx(
<g/>
Australský	australský	k2eAgInSc1d1
fotbal	fotbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IBA	iba	k6eAd1
(	(	kIx(
<g/>
bodyboarding	bodyboarding	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
PBA	PBA	kA
(	(	kIx(
<g/>
bowls	bowls	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IFBA	IFBA	kA
(	(	kIx(
<g/>
broomball	broomball	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
IKAEF	IKAEF	kA
(	(	kIx(
<g/>
eskrima	eskrim	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
WFA	WFA	kA
(	(	kIx(
<g/>
footbag	footbag	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
ISBHF	ISBHF	kA
(	(	kIx(
<g/>
hokejbal	hokejbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
WCF	WCF	kA
(	(	kIx(
<g/>
kroket	kroket	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IKF	IKF	kA
(	(	kIx(
<g/>
kabaddi	kabaddit	k5eAaPmRp2nS
<g/>
)	)	kIx)
</s>
<s>
IMMAF	IMMAF	kA
(	(	kIx(
<g/>
MMA	MMA	kA
<g/>
)	)	kIx)
</s>
<s>
IFP	IFP	kA
(	(	kIx(
<g/>
poker	poker	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IPSC	IPSC	kA
(	(	kIx(
<g/>
practical	practicat	k5eAaPmAgInS
shooting	shooting	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IQA	IQA	kA
(	(	kIx(
<g/>
mudlovský	mudlovský	k2eAgInSc1d1
famfrpál	famfrpál	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IFMAR	IFMAR	kA
(	(	kIx(
<g/>
závody	závod	k1gInPc1
automobilových	automobilový	k2eAgInPc2d1
modelů	model	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
FIFTA	FIFTA	kA
(	(	kIx(
<g/>
nohejbal	nohejbal	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
IRF	IRF	kA
(	(	kIx(
<g/>
rogaining	rogaining	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
RLIF	RLIF	kA
(	(	kIx(
<g/>
třináctkové	třináctkový	k2eAgNnSc1d1
ragby	ragby	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
WSSA	WSSA	kA
(	(	kIx(
<g/>
sport	sport	k1gInSc1
stacking	stacking	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
ITPF	ITPF	kA
(	(	kIx(
<g/>
Tent	tent	k1gInSc1
pegging	pegging	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
FIT	fit	k2eAgInSc1d1
(	(	kIx(
<g/>
touch	touch	k1gInSc1
rugby	rugby	k1gNnSc1
<g/>
)	)	kIx)
Mezinárodní	mezinárodní	k2eAgInSc1d1
olympijský	olympijský	k2eAgInSc1d1
výbor	výbor	k1gInSc1
•	•	k?
Mezinárodní	mezinárodní	k2eAgFnSc1d1
asociace	asociace	k1gFnSc1
Světových	světový	k2eAgFnPc2d1
her	hra	k1gFnPc2
•	•	k?
SportAccord	SportAccord	k1gMnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
5048394-8	5048394-8	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2156	#num#	k4
6570	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
81078023	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
129835064	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
81078023	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Sport	sport	k1gInSc1
|	|	kIx~
Tenis	tenis	k1gInSc1
</s>
