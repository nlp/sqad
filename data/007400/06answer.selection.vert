<s>
Země	země	k1gFnSc1	země
má	mít	k5eAaImIp3nS	mít
relativně	relativně	k6eAd1	relativně
hustou	hustý	k2eAgFnSc4d1	hustá
atmosféru	atmosféra	k1gFnSc4	atmosféra
složenou	složený	k2eAgFnSc4d1	složená
ze	z	k7c2	z
78	[number]	k4	78
%	%	kIx~	%
dusíku	dusík	k1gInSc2	dusík
<g/>
,	,	kIx,	,
21	[number]	k4	21
%	%	kIx~	%
kyslíku	kyslík	k1gInSc2	kyslík
<g/>
,	,	kIx,	,
1	[number]	k4	1
%	%	kIx~	%
argonu	argon	k1gInSc2	argon
a	a	k8xC	a
stopového	stopový	k2eAgNnSc2d1	stopové
množství	množství	k1gNnSc2	množství
jiných	jiný	k2eAgInPc2d1	jiný
plynů	plyn	k1gInPc2	plyn
včetně	včetně	k7c2	včetně
oxidu	oxid	k1gInSc2	oxid
uhličitého	uhličitý	k2eAgInSc2d1	uhličitý
a	a	k8xC	a
vodních	vodní	k2eAgFnPc2d1	vodní
par	para	k1gFnPc2	para
<g/>
.	.	kIx.	.
</s>
