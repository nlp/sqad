<s>
Pico	Pico	k1gMnSc1	Pico
de	de	k?	de
Orizaba	Orizaba	k1gMnSc1	Orizaba
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
Citlaltépetl	Citlaltépetl	k1gFnSc1	Citlaltépetl
-	-	kIx~	-
z	z	k7c2	z
aztéckého	aztécký	k2eAgInSc2d1	aztécký
jazyka	jazyk	k1gInSc2	jazyk
Nahuatl	Nahuatl	k1gFnSc2	Nahuatl
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
citlalli	citlalle	k1gFnSc4	citlalle
znamená	znamenat	k5eAaImIp3nS	znamenat
hvězda	hvězda	k1gFnSc1	hvězda
a	a	k8xC	a
tepetl	tepést	k5eAaPmAgMnS	tepést
je	být	k5eAaImIp3nS	být
hora	hora	k1gFnSc1	hora
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
5636	[number]	k4	5636
m	m	kA	m
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
Mexika	Mexiko	k1gNnSc2	Mexiko
a	a	k8xC	a
třetím	třetí	k4xOgInSc7	třetí
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
