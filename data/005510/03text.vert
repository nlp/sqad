<s>
Pico	Pico	k1gMnSc1	Pico
de	de	k?	de
Orizaba	Orizaba	k1gMnSc1	Orizaba
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
také	také	k9	také
Citlaltépetl	Citlaltépetl	k1gFnSc1	Citlaltépetl
-	-	kIx~	-
z	z	k7c2	z
aztéckého	aztécký	k2eAgInSc2d1	aztécký
jazyka	jazyk	k1gInSc2	jazyk
Nahuatl	Nahuatl	k1gFnSc2	Nahuatl
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
citlalli	citlalle	k1gFnSc4	citlalle
znamená	znamenat	k5eAaImIp3nS	znamenat
hvězda	hvězda	k1gFnSc1	hvězda
a	a	k8xC	a
tepetl	tepést	k5eAaPmAgMnS	tepést
je	být	k5eAaImIp3nS	být
hora	hora	k1gFnSc1	hora
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
s	s	k7c7	s
5636	[number]	k4	5636
m	m	kA	m
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
Mexika	Mexiko	k1gNnSc2	Mexiko
a	a	k8xC	a
třetím	třetí	k4xOgInSc7	třetí
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	s	k7c7	s
120	[number]	k4	120
km	km	kA	km
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
města	město	k1gNnSc2	město
Veracruz	Veracruza	k1gFnPc2	Veracruza
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Vrchol	vrchol	k1gInSc1	vrchol
Citlaltépetlu	Citlaltépetl	k1gInSc2	Citlaltépetl
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
tří	tři	k4xCgMnPc2	tři
neustále	neustále	k6eAd1	neustále
zasněžených	zasněžený	k2eAgMnPc2d1	zasněžený
ledovců	ledovec	k1gInPc2	ledovec
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgMnPc1d1	ostatní
jsou	být	k5eAaImIp3nP	být
Popocatépetl	Popocatépetl	k1gMnPc4	Popocatépetl
a	a	k8xC	a
Iztaccíhuatl	Iztaccíhuatl	k1gMnPc4	Iztaccíhuatl
<g/>
.	.	kIx.	.
</s>
<s>
Výchozím	výchozí	k2eAgNnSc7d1	výchozí
místem	místo	k1gNnSc7	místo
pro	pro	k7c4	pro
výstup	výstup	k1gInSc4	výstup
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
je	být	k5eAaImIp3nS	být
malé	malý	k2eAgNnSc1d1	malé
město	město	k1gNnSc1	město
Tlachichuca	Tlachichuca	k1gFnSc1	Tlachichuca
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc1d1	ležící
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
straně	strana	k1gFnSc6	strana
hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Standardní	standardní	k2eAgFnSc1d1	standardní
výstupová	výstupový	k2eAgFnSc1d1	výstupová
trasa	trasa	k1gFnSc1	trasa
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
pak	pak	k6eAd1	pak
vede	vést	k5eAaImIp3nS	vést
ze	z	k7c2	z
severu	sever	k1gInSc2	sever
od	od	k7c2	od
chaty	chata	k1gFnSc2	chata
Piedra	Piedr	k1gMnSc2	Piedr
Grande	grand	k1gMnSc5	grand
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
výstupu	výstup	k1gInSc3	výstup
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
mít	mít	k5eAaImF	mít
kompletní	kompletní	k2eAgNnSc4d1	kompletní
vybavení	vybavení	k1gNnSc4	vybavení
pro	pro	k7c4	pro
postup	postup	k1gInSc4	postup
po	po	k7c6	po
ledovci	ledovec	k1gInSc6	ledovec
a	a	k8xC	a
být	být	k5eAaImF	být
dostatečně	dostatečně	k6eAd1	dostatečně
aklimatizovaný	aklimatizovaný	k2eAgMnSc1d1	aklimatizovaný
na	na	k7c4	na
vysokou	vysoký	k2eAgFnSc4d1	vysoká
nadmořskou	nadmořský	k2eAgFnSc4d1	nadmořská
výšku	výška	k1gFnSc4	výška
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Pico	Pico	k6eAd1	Pico
de	de	k?	de
Orizaba	Orizaba	k1gFnSc1	Orizaba
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Popis	popis	k1gInSc1	popis
výstupu	výstup	k1gInSc2	výstup
na	na	k7c4	na
Pico	Pico	k1gNnSc4	Pico
de	de	k?	de
Orizaba	Orizaba	k1gMnSc1	Orizaba
na	na	k7c6	na
Horalka	horalka	k1gFnSc1	horalka
<g/>
.	.	kIx.	.
<g/>
org	org	k?	org
</s>
