<s>
Ing.	ing.	kA	ing.
Jan	Jan	k1gMnSc1	Jan
Haubert	Haubert	k1gMnSc1	Haubert
(	(	kIx(	(
<g/>
zvaný	zvaný	k2eAgMnSc1d1	zvaný
Honny	Honen	k2eAgInPc4d1	Honen
Lemy	lem	k1gInPc4	lem
Ušaté	ušatý	k2eAgNnSc4d1	ušaté
Torpédo	torpédo	k1gNnSc4	torpédo
(	(	kIx(	(
<g/>
Trojpéro	Trojpéro	k1gNnSc4	Trojpéro
<g/>
)	)	kIx)	)
či	či	k8xC	či
VZ	VZ	kA	VZ
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
*	*	kIx~	*
9	[number]	k4	9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1960	[number]	k4	1960
Janovice	Janovice	k1gFnPc1	Janovice
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
textař	textař	k1gMnSc1	textař
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
kapely	kapela	k1gFnSc2	kapela
Visací	visací	k2eAgInSc4d1	visací
zámek	zámek	k1gInSc4	zámek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
v	v	k7c6	v
Janovicích	Janovice	k1gFnPc6	Janovice
<g/>
,	,	kIx,	,
části	část	k1gFnSc3	část
obce	obec	k1gFnSc2	obec
Rýmařov	Rýmařovo	k1gNnPc2	Rýmařovo
v	v	k7c6	v
Moravskoslezském	moravskoslezský	k2eAgInSc6d1	moravskoslezský
kraji	kraj	k1gInSc6	kraj
a	a	k8xC	a
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
stavební	stavební	k2eAgFnSc4d1	stavební
fakultu	fakulta	k1gFnSc4	fakulta
ČVUT	ČVUT	kA	ČVUT
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hudební	hudební	k2eAgFnSc1d1	hudební
tvorba	tvorba	k1gFnSc1	tvorba
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
založil	založit	k5eAaPmAgInS	založit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Michalem	Michal	k1gMnSc7	Michal
Pixou	Pixa	k1gMnSc7	Pixa
<g/>
,	,	kIx,	,
Vladimírem	Vladimír	k1gMnSc7	Vladimír
Šťástkou	Šťástka	k1gFnSc7	Šťástka
<g/>
,	,	kIx,	,
Ivanem	Ivan	k1gMnSc7	Ivan
Rutem	Rut	k1gMnSc7	Rut
a	a	k8xC	a
Jiřím	Jiří	k1gMnSc7	Jiří
Pátkem	Pátek	k1gMnSc7	Pátek
punkovou	punkový	k2eAgFnSc4d1	punková
kapelu	kapela	k1gFnSc4	kapela
Visací	visací	k2eAgInSc4d1	visací
zámek	zámek	k1gInSc4	zámek
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
nezměněné	změněný	k2eNgFnSc6d1	nezměněná
sestavě	sestava	k1gFnSc6	sestava
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
