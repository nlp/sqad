<p>
<s>
Ing.	ing.	kA	ing.
Jan	Jan	k1gMnSc1	Jan
Haubert	Haubert	k1gMnSc1	Haubert
(	(	kIx(	(
<g/>
zvaný	zvaný	k2eAgMnSc1d1	zvaný
Honny	Honen	k2eAgInPc4d1	Honen
Lemy	lem	k1gInPc4	lem
Ušaté	ušatý	k2eAgNnSc4d1	ušaté
Torpédo	torpédo	k1gNnSc4	torpédo
(	(	kIx(	(
<g/>
Trojpéro	Trojpéro	k1gNnSc4	Trojpéro
<g/>
)	)	kIx)	)
či	či	k8xC	či
VZ	VZ	kA	VZ
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
*	*	kIx~	*
9	[number]	k4	9
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1960	[number]	k4	1960
Janovice	Janovice	k1gFnPc1	Janovice
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
český	český	k2eAgMnSc1d1	český
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
textař	textař	k1gMnSc1	textař
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
kapely	kapela	k1gFnSc2	kapela
Visací	visací	k2eAgInSc4d1	visací
zámek	zámek	k1gInSc4	zámek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
v	v	k7c6	v
Janovicích	Janovice	k1gFnPc6	Janovice
<g/>
,	,	kIx,	,
části	část	k1gFnSc3	část
obce	obec	k1gFnSc2	obec
Rýmařov	Rýmařovo	k1gNnPc2	Rýmařovo
v	v	k7c6	v
Moravskoslezském	moravskoslezský	k2eAgInSc6d1	moravskoslezský
kraji	kraj	k1gInSc6	kraj
a	a	k8xC	a
vystudoval	vystudovat	k5eAaPmAgInS	vystudovat
stavební	stavební	k2eAgFnSc4d1	stavební
fakultu	fakulta	k1gFnSc4	fakulta
ČVUT	ČVUT	kA	ČVUT
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hudební	hudební	k2eAgFnSc1d1	hudební
tvorba	tvorba	k1gFnSc1	tvorba
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
založil	založit	k5eAaPmAgInS	založit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Michalem	Michal	k1gMnSc7	Michal
Pixou	Pixa	k1gMnSc7	Pixa
<g/>
,	,	kIx,	,
Vladimírem	Vladimír	k1gMnSc7	Vladimír
Šťástkou	Šťástka	k1gFnSc7	Šťástka
<g/>
,	,	kIx,	,
Ivanem	Ivan	k1gMnSc7	Ivan
Rutem	Rut	k1gMnSc7	Rut
a	a	k8xC	a
Jiřím	Jiří	k1gMnSc7	Jiří
Pátkem	Pátek	k1gMnSc7	Pátek
punkovou	punkový	k2eAgFnSc4d1	punková
kapelu	kapela	k1gFnSc4	kapela
Visací	visací	k2eAgInSc4d1	visací
zámek	zámek	k1gInSc4	zámek
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
nezměněné	změněný	k2eNgFnSc6d1	nezměněná
sestavě	sestava	k1gFnSc6	sestava
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Haubert	Haubert	k1gMnSc1	Haubert
je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
většiny	většina	k1gFnSc2	většina
textů	text	k1gInPc2	text
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
vyšla	vyjít	k5eAaPmAgFnS	vyjít
u	u	k7c2	u
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
Julius	Julius	k1gMnSc1	Julius
Zirkus	Zirkus	k1gMnSc1	Zirkus
</s>
</p>
<p>
<s>
deska	deska	k1gFnSc1	deska
mluveného	mluvený	k2eAgNnSc2d1	mluvené
slova	slovo	k1gNnSc2	slovo
s	s	k7c7	s
názvem	název	k1gInSc7	název
MASAKR	masakr	k1gInSc4	masakr
&	&	k?	&
HAŠIŠ	hašiš	k1gInSc1	hašiš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literární	literární	k2eAgFnSc1d1	literární
tvorba	tvorba	k1gFnSc1	tvorba
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
vydal	vydat	k5eAaPmAgMnS	vydat
u	u	k7c2	u
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
Julius	Julius	k1gMnSc1	Julius
Zirkus	Zirkus	k1gMnSc1	Zirkus
knihu	kniha	k1gFnSc4	kniha
Básně	báseň	k1gFnSc2	báseň
<g/>
,	,	kIx,	,
aneb	aneb	k?	aneb
<g/>
,	,	kIx,	,
Visací	visací	k2eAgInSc1d1	visací
zámek	zámek	k1gInSc1	zámek
podle	podle	k7c2	podle
Jana	Jan	k1gMnSc2	Jan
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
texty	text	k1gInPc4	text
písní	píseň	k1gFnPc2	píseň
kapely	kapela	k1gFnSc2	kapela
Visací	visací	k2eAgInSc4d1	visací
zámek	zámek	k1gInSc4	zámek
a	a	k8xC	a
úvodní	úvodní	k2eAgInSc4d1	úvodní
proslov	proslov	k1gInSc4	proslov
o	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
vyšla	vyjít	k5eAaPmAgFnS	vyjít
také	také	k9	také
u	u	k7c2	u
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
Julius	Julius	k1gMnSc1	Julius
Zirkus	Zirkus	k1gMnSc1	Zirkus
druhá	druhý	k4xOgFnSc1	druhý
Haubertova	Haubertův	k2eAgFnSc1d1	Haubertův
kniha	kniha	k1gFnSc1	kniha
Hluboká	Hluboká	k1gFnSc1	Hluboká
orba	orba	k1gFnSc1	orba
<g/>
,	,	kIx,	,
aneb	aneb	k?	aneb
<g/>
,	,	kIx,	,
20	[number]	k4	20
let	léto	k1gNnPc2	léto
Visacího	visací	k2eAgInSc2d1	visací
zámku	zámek	k1gInSc2	zámek
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
mapuje	mapovat	k5eAaImIp3nS	mapovat
historii	historie	k1gFnSc4	historie
kapely	kapela	k1gFnSc2	kapela
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
25	[number]	k4	25
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
její	její	k3xOp3gFnSc2	její
existence	existence	k1gFnSc2	existence
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
úryvky	úryvek	k1gInPc4	úryvek
z	z	k7c2	z
deníku	deník	k1gInSc2	deník
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
si	se	k3xPyFc3	se
Haubert	Haubert	k1gMnSc1	Haubert
psal	psát	k5eAaImAgMnS	psát
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
Deník	deník	k1gInSc1	deník
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
koncertování	koncertování	k1gNnSc4	koncertování
kapely	kapela	k1gFnSc2	kapela
<g/>
,	,	kIx,	,
dění	dění	k1gNnSc1	dění
při	při	k7c6	při
povodních	povodeň	k1gFnPc6	povodeň
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
či	či	k8xC	či
postřehy	postřeh	k1gInPc1	postřeh
ze	z	k7c2	z
zápasů	zápas	k1gInPc2	zápas
pražských	pražský	k2eAgFnPc2d1	Pražská
Bohemians	Bohemiansa	k1gFnPc2	Bohemiansa
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
byla	být	k5eAaImAgFnS	být
nominována	nominovat	k5eAaBmNgFnS	nominovat
na	na	k7c4	na
literární	literární	k2eAgFnSc4d1	literární
cenu	cena	k1gFnSc4	cena
Josefa	Josef	k1gMnSc2	Josef
Škvoreckého	Škvorecký	k2eAgMnSc2d1	Škvorecký
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
vydal	vydat	k5eAaPmAgMnS	vydat
sbírku	sbírka	k1gFnSc4	sbírka
povídek	povídka	k1gFnPc2	povídka
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Všelijaké	všelijaký	k3yIgFnSc2	všelijaký
povídky	povídka	k1gFnSc2	povídka
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
patnáct	patnáct	k4xCc4	patnáct
povídek	povídka	k1gFnPc2	povídka
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vydaná	vydaný	k2eAgNnPc4d1	vydané
opět	opět	k6eAd1	opět
u	u	k7c2	u
Julius	Julius	k1gMnSc1	Julius
Zirkus	Zirkus	k1gInSc1	Zirkus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
vyšla	vyjít	k5eAaPmAgFnS	vyjít
audiokniha	audiokniha	k1gFnSc1	audiokniha
Masakr	masakr	k1gInSc1	masakr
&	&	k?	&
Hašiš	hašiš	k1gInSc1	hašiš
u	u	k7c2	u
nakladatelství	nakladatelství	k1gNnSc2	nakladatelství
Julius	Julius	k1gMnSc1	Julius
Zirkus	Zirkus	k1gMnSc1	Zirkus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Bibliografie	bibliografie	k1gFnSc2	bibliografie
===	===	k?	===
</s>
</p>
<p>
<s>
Básně	báseň	k1gFnPc1	báseň
aneb	aneb	k?	aneb
Visací	visací	k2eAgInSc1d1	visací
zámek	zámek	k1gInSc1	zámek
podle	podle	k7c2	podle
Jana	Jan	k1gMnSc2	Jan
(	(	kIx(	(
<g/>
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hluboká	hluboký	k2eAgFnSc1d1	hluboká
orba	orba	k1gFnSc1	orba
aneb	aneb	k?	aneb
20	[number]	k4	20
let	léto	k1gNnPc2	léto
Visacího	visací	k2eAgInSc2d1	visací
zámku	zámek	k1gInSc2	zámek
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Básně	báseň	k1gFnPc1	báseň
<g/>
,	,	kIx,	,
aneb	aneb	k?	aneb
<g/>
,	,	kIx,	,
Visací	visací	k2eAgInSc1d1	visací
zámek	zámek	k1gInSc1	zámek
podle	podle	k7c2	podle
Jana	Jan	k1gMnSc2	Jan
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Hluboká	hluboký	k2eAgFnSc1d1	hluboká
orba	orba	k1gFnSc1	orba
aneb	aneb	k?	aneb
20	[number]	k4	20
let	léto	k1gNnPc2	léto
Visacího	visací	k2eAgInSc2d1	visací
zámku	zámek	k1gInSc2	zámek
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Všelijaké	všelijaký	k3yIgFnPc1	všelijaký
povídky	povídka	k1gFnPc1	povídka
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Masakr	masakr	k1gInSc1	masakr
&	&	k?	&
hašiš	hašiš	k1gInSc1	hašiš
(	(	kIx(	(
<g/>
audiokniha	audiokniha	k1gFnSc1	audiokniha
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Básně	báseň	k1gFnPc1	báseň
aneb	aneb	k?	aneb
Visací	visací	k2eAgInSc1d1	visací
zámek	zámek	k1gInSc1	zámek
podle	podle	k7c2	podle
Jana	Jan	k1gMnSc2	Jan
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc2	vydání
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Visací	visací	k2eAgInSc1d1	visací
zámek	zámek	k1gInSc1	zámek
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Jan	Jan	k1gMnSc1	Jan
Haubert	Haubert	k1gMnSc1	Haubert
</s>
</p>
<p>
<s>
Rozhovor	rozhovor	k1gInSc1	rozhovor
s	s	k7c7	s
Janem	Jan	k1gMnSc7	Jan
Haubertem	Haubert	k1gMnSc7	Haubert
o	o	k7c6	o
kapele	kapela	k1gFnSc6	kapela
Visací	visací	k2eAgInSc1d1	visací
Zámek	zámek	k1gInSc1	zámek
a	a	k8xC	a
knize	kniha	k1gFnSc6	kniha
Hluboká	hluboký	k2eAgFnSc1d1	hluboká
orba	orba	k1gFnSc1	orba
</s>
</p>
<p>
<s>
Recenze	recenze	k1gFnSc1	recenze
knihy	kniha	k1gFnSc2	kniha
Hluboká	hluboký	k2eAgFnSc1d1	hluboká
orba	orba	k1gFnSc1	orba
</s>
</p>
