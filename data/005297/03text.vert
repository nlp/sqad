<s>
Mennofer	Mennofer	k1gInSc1	Mennofer
(	(	kIx(	(
<g/>
též	též	k9	též
Mennefer	Mennefer	k1gInSc1	Mennefer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
i	i	k9	i
pod	pod	k7c7	pod
řeckým	řecký	k2eAgNnSc7d1	řecké
jménem	jméno	k1gNnSc7	jméno
Memfis	Memfis	k1gFnSc1	Memfis
(	(	kIx(	(
<g/>
Μ	Μ	k?	Μ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
starověkého	starověký	k2eAgInSc2d1	starověký
Egypta	Egypt	k1gInSc2	Egypt
v	v	k7c6	v
období	období	k1gNnSc6	období
Staré	Staré	k2eAgFnSc2d1	Staré
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Příležitostně	příležitostně	k6eAd1	příležitostně
plnilo	plnit	k5eAaImAgNnS	plnit
roli	role	k1gFnSc4	role
sídelního	sídelní	k2eAgNnSc2d1	sídelní
města	město	k1gNnSc2	město
panovníků	panovník	k1gMnPc2	panovník
i	i	k9	i
později	pozdě	k6eAd2	pozdě
a	a	k8xC	a
po	po	k7c4	po
celé	celý	k2eAgNnSc4d1	celé
období	období	k1gNnSc4	období
egyptského	egyptský	k2eAgInSc2d1	egyptský
starověku	starověk	k1gInSc2	starověk
zůstávalo	zůstávat	k5eAaImAgNnS	zůstávat
významným	významný	k2eAgInSc7d1	významný
administrativním	administrativní	k2eAgInSc7d1	administrativní
a	a	k8xC	a
vojenským	vojenský	k2eAgInSc7d1	vojenský
centrem	centr	k1gInSc7	centr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gNnSc6	jeho
okolí	okolí	k1gNnSc6	okolí
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
nekropole	nekropole	k1gFnPc1	nekropole
dnes	dnes	k6eAd1	dnes
nazývané	nazývaný	k2eAgInPc1d1	nazývaný
Sakkára	Sakkár	k1gMnSc4	Sakkár
a	a	k8xC	a
Abúsír	Abúsír	k1gInSc4	Abúsír
<g/>
.	.	kIx.	.
</s>
<s>
Archeologická	archeologický	k2eAgFnSc1d1	archeologická
lokalita	lokalita	k1gFnSc1	lokalita
odpovídající	odpovídající	k2eAgFnSc2d1	odpovídající
oblasti	oblast	k1gFnSc2	oblast
někdejšího	někdejší	k2eAgNnSc2d1	někdejší
města	město	k1gNnSc2	město
leží	ležet	k5eAaImIp3nS	ležet
u	u	k7c2	u
dnešní	dnešní	k2eAgFnSc2d1	dnešní
vesnice	vesnice	k1gFnSc2	vesnice
Mít	mít	k5eAaImF	mít
Rahína	Rahína	k1gFnSc1	Rahína
asi	asi	k9	asi
30	[number]	k4	30
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
nynějšího	nynější	k2eAgNnSc2d1	nynější
egyptského	egyptský	k2eAgNnSc2d1	egyptské
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Káhiry	Káhira	k1gFnSc2	Káhira
na	na	k7c6	na
západním	západní	k2eAgInSc6d1	západní
břehu	břeh	k1gInSc6	břeh
Nilu	Nil	k1gInSc2	Nil
téměř	téměř	k6eAd1	téměř
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
nilského	nilský	k2eAgNnSc2d1	nilské
údolí	údolí	k1gNnSc2	údolí
a	a	k8xC	a
delty	delta	k1gFnSc2	delta
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
město	město	k1gNnSc1	město
a	a	k8xC	a
tamní	tamní	k2eAgInSc1d1	tamní
velký	velký	k2eAgInSc1d1	velký
Ptahův	Ptahův	k2eAgInSc1d1	Ptahův
chrám	chrám	k1gInSc1	chrám
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
v	v	k7c6	v
egyptštině	egyptština	k1gFnSc6	egyptština
Hut	Hut	k1gMnSc1	Hut
ka	ka	k?	ka
Ptah	Ptah	k1gMnSc1	Ptah
–	–	k?	–
"	"	kIx"	"
<g/>
Chrám	chrám	k1gInSc1	chrám
<g/>
/	/	kIx~	/
<g/>
Palác	palác	k1gInSc1	palác
Ptahova	Ptahův	k2eAgInSc2d1	Ptahův
ka	ka	k?	ka
<g/>
"	"	kIx"	"
založil	založit	k5eAaPmAgMnS	založit
doposud	doposud	k6eAd1	doposud
historicky	historicky	k6eAd1	historicky
nedoložený	doložený	k2eNgMnSc1d1	nedoložený
první	první	k4xOgMnSc1	první
egyptský	egyptský	k2eAgMnSc1d1	egyptský
panovník	panovník	k1gMnSc1	panovník
Meni	Men	k1gFnSc2	Men
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
název	název	k1gInSc1	název
města	město	k1gNnSc2	město
byl	být	k5eAaImAgInS	být
Inebuhedž	Inebuhedž	k1gFnSc4	Inebuhedž
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Bílé	bílý	k2eAgFnPc1d1	bílá
zdi	zeď	k1gFnPc1	zeď
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
poukazuje	poukazovat	k5eAaImIp3nS	poukazovat
na	na	k7c4	na
opevněnou	opevněný	k2eAgFnSc4d1	opevněná
královskou	královský	k2eAgFnSc4d1	královská
rezidenci	rezidence	k1gFnSc4	rezidence
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
názvu	název	k1gInSc2	název
chrámového	chrámový	k2eAgInSc2d1	chrámový
komplexu	komplex	k1gInSc2	komplex
(	(	kIx(	(
<g/>
konkrétně	konkrétně	k6eAd1	konkrétně
z	z	k7c2	z
pozdější	pozdní	k2eAgFnSc2d2	pozdější
zkrácené	zkrácený	k2eAgFnSc2d1	zkrácená
hovorové	hovorový	k2eAgFnSc2d1	hovorová
varianty	varianta	k1gFnSc2	varianta
Hykupta	Hykupt	k1gInSc2	Hykupt
<g/>
)	)	kIx)	)
Řekové	Řek	k1gMnPc1	Řek
utvořili	utvořit	k5eAaPmAgMnP	utvořit
název	název	k1gInSc4	název
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
vztáhli	vztáhnout	k5eAaPmAgMnP	vztáhnout
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
zemi	zem	k1gFnSc4	zem
<g/>
:	:	kIx,	:
AIΓ	AIΓ	k1gFnSc1	AIΓ
(	(	kIx(	(
<g/>
Aigyptos	Aigyptos	k1gInSc1	Aigyptos
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
s	s	k7c7	s
tímto	tento	k3xDgNnSc7	tento
označením	označení	k1gNnSc7	označení
pro	pro	k7c4	pro
Egypt	Egypt	k1gInSc4	Egypt
setkali	setkat	k5eAaPmAgMnP	setkat
Římané	Říman	k1gMnPc1	Říman
<g/>
,	,	kIx,	,
převzali	převzít	k5eAaPmAgMnP	převzít
jej	on	k3xPp3gMnSc4	on
<g/>
,	,	kIx,	,
přizpůsobili	přizpůsobit	k5eAaPmAgMnP	přizpůsobit
jej	on	k3xPp3gMnSc4	on
svému	svůj	k3xOyFgInSc3	svůj
jazyku	jazyk	k1gInSc3	jazyk
a	a	k8xC	a
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
Æ	Æ	k?	Æ
(	(	kIx(	(
<g/>
Égyptus	Égyptus	k1gMnSc1	Égyptus
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
něhož	jenž	k3xRgNnSc2	jenž
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
odvozen	odvodit	k5eAaPmNgInS	odvodit
náš	náš	k3xOp1gInSc4	náš
současný	současný	k2eAgInSc4d1	současný
název	název	k1gInSc4	název
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
někdejšího	někdejší	k2eAgNnSc2d1	někdejší
města	město	k1gNnSc2	město
s	s	k7c7	s
chrámem	chrám	k1gInSc7	chrám
boha	bůh	k1gMnSc2	bůh
Ptaha	Ptah	k1gMnSc2	Ptah
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
pod	pod	k7c7	pod
současnou	současný	k2eAgFnSc7d1	současná
vesnicí	vesnice	k1gFnSc7	vesnice
<g/>
,	,	kIx,	,
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
staletí	staletí	k1gNnPc2	staletí
se	se	k3xPyFc4	se
centrum	centrum	k1gNnSc1	centrum
posouvalo	posouvat	k5eAaImAgNnS	posouvat
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
západu	západ	k1gInSc3	západ
k	k	k7c3	k
oblasti	oblast	k1gFnSc3	oblast
pyramidových	pyramidový	k2eAgInPc2d1	pyramidový
komplexů	komplex	k1gInPc2	komplex
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zkráceného	zkrácený	k2eAgInSc2d1	zkrácený
názvu	název	k1gInSc2	název
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
(	(	kIx(	(
<g/>
komplex	komplex	k1gInSc1	komplex
Pepiho	Pepi	k1gMnSc2	Pepi
I.	I.	kA	I.
<g/>
,	,	kIx,	,
Men	Men	k1gMnSc1	Men
nefer	nefer	k1gMnSc1	nefer
Pepi	Pepi	k1gMnSc1	Pepi
–	–	k?	–
"	"	kIx"	"
<g/>
Krása	krása	k1gFnSc1	krása
Pepiho	Pepi	k1gMnSc2	Pepi
je	být	k5eAaImIp3nS	být
trvalá	trvalý	k2eAgFnSc1d1	trvalá
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
v	v	k7c6	v
období	období	k1gNnSc6	období
Střední	střední	k2eAgFnSc1d1	střední
říše	říše	k1gFnSc1	říše
název	název	k1gInSc1	název
celého	celý	k2eAgNnSc2d1	celé
města	město	k1gNnSc2	město
–	–	k?	–
Mennofer	Mennofer	k1gMnSc1	Mennofer
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
lokalitě	lokalita	k1gFnSc6	lokalita
je	být	k5eAaImIp3nS	být
identifikováno	identifikovat	k5eAaBmNgNnS	identifikovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
sto	sto	k4xCgNnSc4	sto
archeologických	archeologický	k2eAgInPc2d1	archeologický
areálů	areál	k1gInPc2	areál
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgInPc1d1	dnešní
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
pocházejí	pocházet	k5eAaImIp3nP	pocházet
především	především	k9	především
z	z	k7c2	z
doby	doba	k1gFnSc2	doba
vlády	vláda	k1gFnSc2	vláda
panovníků	panovník	k1gMnPc2	panovník
Amenhotepa	Amenhotepa	k1gFnSc1	Amenhotepa
III	III	kA	III
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Ramesse	Ramesse	k1gFnSc1	Ramesse
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
Ptolemaia	Ptolemaios	k1gMnSc2	Ptolemaios
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
velký	velký	k2eAgInSc1d1	velký
Ptahův	Ptahův	k2eAgInSc1d1	Ptahův
chrám	chrám	k1gInSc1	chrám
<g/>
)	)	kIx)	)
a	a	k8xC	a
Vahjebrea	Vahjebrea	k1gFnSc1	Vahjebrea
(	(	kIx(	(
<g/>
palác	palác	k1gInSc1	palác
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
k	k	k7c3	k
rozvoji	rozvoj	k1gInSc3	rozvoj
města	město	k1gNnSc2	město
ovšem	ovšem	k9	ovšem
přispěli	přispět	k5eAaPmAgMnP	přispět
panovníci	panovník	k1gMnPc1	panovník
ze	z	k7c2	z
všech	všecek	k3xTgNnPc2	všecek
historických	historický	k2eAgNnPc2d1	historické
období	období	k1gNnPc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
osobností	osobnost	k1gFnSc7	osobnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
podílela	podílet	k5eAaImAgFnS	podílet
na	na	k7c4	na
utváření	utváření	k1gNnSc4	utváření
rázu	ráz	k1gInSc2	ráz
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
také	také	k9	také
princ	princ	k1gMnSc1	princ
Chamuaset	Chamuaset	k1gMnSc1	Chamuaset
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
Ramesse	Ramesse	k1gFnSc2	Ramesse
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
byl	být	k5eAaImAgInS	být
Mennofer	Mennofer	k1gInSc1	Mennofer
zařazen	zařadit	k5eAaPmNgInS	zařadit
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
<s>
Mennoferská	Mennoferský	k2eAgFnSc1d1	Mennoferská
kosmologie	kosmologie	k1gFnSc1	kosmologie
Ptah	Ptah	k1gMnSc1	Ptah
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Mennofer	Mennofra	k1gFnPc2	Mennofra
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
