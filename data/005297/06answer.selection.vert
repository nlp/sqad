<s>
Mennofer	Mennofer	k1gInSc1	Mennofer
(	(	kIx(	(
<g/>
též	též	k9	též
Mennefer	Mennefer	k1gInSc1	Mennefer
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
známý	známý	k2eAgMnSc1d1	známý
i	i	k9	i
pod	pod	k7c7	pod
řeckým	řecký	k2eAgNnSc7d1	řecké
jménem	jméno	k1gNnSc7	jméno
Memfis	Memfis	k1gFnSc1	Memfis
(	(	kIx(	(
<g/>
Μ	Μ	k?	Μ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
starověkého	starověký	k2eAgInSc2d1	starověký
Egypta	Egypt	k1gInSc2	Egypt
v	v	k7c6	v
období	období	k1gNnSc6	období
Staré	Staré	k2eAgFnSc2d1	Staré
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
