<s>
Karnes	Karnes	k1gInSc1
County	Counta	k1gFnSc2
</s>
<s>
Karnes	Karnes	k1gInSc1
County	Counta	k1gFnSc2
Geografie	geografie	k1gFnSc2
</s>
<s>
Karnes	Karnes	k1gInSc1
County	Counta	k1gFnSc2
na	na	k7c6
mapě	mapa	k1gFnSc6
Texasu	Texas	k1gInSc2
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
</s>
<s>
Karnes	Karnes	k1gInSc1
City	city	k1gNnSc1
Status	status	k1gInSc1
</s>
<s>
okres	okres	k1gInSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
28	#num#	k4
<g/>
°	°	k?
<g/>
54	#num#	k4
<g/>
′	′	k?
<g/>
36	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
97	#num#	k4
<g/>
°	°	k?
<g/>
51	#num#	k4
<g/>
′	′	k?
<g/>
36	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
1	#num#	k4
952	#num#	k4
km²	km²	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
Obyvatelstvo	obyvatelstvo	k1gNnSc1
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
14	#num#	k4
824	#num#	k4
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
7,6	7,6	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
regionu	region	k1gInSc2
Nadřazený	nadřazený	k2eAgInSc1d1
celek	celek	k1gInSc1
</s>
<s>
Texas	Texas	k1gInSc1
Vznik	vznik	k1gInSc1
</s>
<s>
1854	#num#	k4
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.co.karnes.tx.us	www.co.karnes.tx.us	k1gMnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Karnes	Karnes	k1gInSc1
County	Counta	k1gFnSc2
je	být	k5eAaImIp3nS
okres	okres	k1gInSc1
ve	v	k7c6
státě	stát	k1gInSc6
Texas	Texas	k1gInSc1
v	v	k7c6
USA	USA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
roku	rok	k1gInSc3
2010	#num#	k4
zde	zde	k6eAd1
žilo	žít	k5eAaImAgNnS
14	#num#	k4
824	#num#	k4
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Správním	správní	k2eAgNnSc7d1
městem	město	k1gNnSc7
okresu	okres	k1gInSc2
je	být	k5eAaImIp3nS
Karnes	Karnes	k1gInSc1
City	City	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celková	celkový	k2eAgFnSc1d1
rozloha	rozloha	k1gFnSc1
okresu	okres	k1gInSc2
činí	činit	k5eAaImIp3nS
1	#num#	k4
952	#num#	k4
km²	km²	k?
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Karnes	Karnesa	k1gFnPc2
County	Counta	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Okresy	okres	k1gInPc1
státu	stát	k1gInSc2
Texas	Texas	kA
</s>
<s>
Anderson	Anderson	k1gMnSc1
•	•	k?
Andrews	Andrews	k1gInSc1
•	•	k?
Angelina	Angelin	k2eAgMnSc4d1
•	•	k?
Aransas	Aransasa	k1gFnPc2
•	•	k?
Archer	Archra	k1gFnPc2
•	•	k?
Armstrong	Armstrong	k1gMnSc1
•	•	k?
Atascosa	Atascosa	k1gFnSc1
•	•	k?
Austin	Austin	k1gInSc1
•	•	k?
Bailey	Bailea	k1gMnSc2
•	•	k?
Bandera	Bander	k1gMnSc2
•	•	k?
Bastrop	Bastrop	k1gInSc1
•	•	k?
Baylor	Baylor	k1gInSc1
•	•	k?
Bee	Bee	k1gFnSc2
•	•	k?
Bell	bell	k1gInSc1
•	•	k?
Bexar	Bexar	k1gInSc1
•	•	k?
Blanco	blanco	k2eAgInSc1d1
•	•	k?
Borden	Bordno	k1gNnPc2
•	•	k?
Bosque	Bosqu	k1gFnSc2
•	•	k?
Bowie	Bowie	k1gFnSc1
•	•	k?
Brazoria	Brazorium	k1gNnSc2
•	•	k?
Brazos	Brazos	k1gMnSc1
•	•	k?
Brewster	Brewster	k1gMnSc1
•	•	k?
Briscoe	Briscoe	k1gInSc1
•	•	k?
Brooks	Brooks	k1gInSc1
•	•	k?
Brown	Brown	k1gMnSc1
•	•	k?
Burleson	Burleson	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Burnet	Burnet	k1gMnSc1
•	•	k?
Caldwell	Caldwell	k1gMnSc1
•	•	k?
Calhoun	Calhoun	k1gMnSc1
•	•	k?
Callahan	Callahan	k1gMnSc1
•	•	k?
Cameron	Cameron	k1gMnSc1
•	•	k?
Camp	camp	k1gInSc1
•	•	k?
Carson	Carson	k1gInSc1
•	•	k?
Cass	Cassa	k1gFnPc2
•	•	k?
Castro	Castro	k1gNnSc4
•	•	k?
Chambers	Chambers	k1gInSc1
•	•	k?
Cherokee	Cherokee	k1gInSc1
•	•	k?
Childress	Childress	k1gInSc1
•	•	k?
Clay	Claa	k1gFnSc2
•	•	k?
Cochran	Cochran	k1gInSc1
•	•	k?
Coke	Cok	k1gFnSc2
•	•	k?
Coleman	Coleman	k1gMnSc1
•	•	k?
Collin	Collin	k1gInSc1
•	•	k?
Collingsworth	Collingsworth	k1gInSc1
•	•	k?
Colorado	Colorado	k1gNnSc1
•	•	k?
Comal	Comal	k1gMnSc1
•	•	k?
Comanche	Comanche	k1gInSc1
•	•	k?
Concho	Concha	k1gFnSc5
•	•	k?
Cooke	Cook	k1gFnSc2
•	•	k?
Coryell	Coryell	k1gMnSc1
•	•	k?
Cottle	Cottle	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Crane	Cran	k1gInSc5
•	•	k?
Crockett	Crockettum	k1gNnPc2
•	•	k?
Crosby	Crosba	k1gFnSc2
•	•	k?
Culberson	Culberson	k1gInSc1
•	•	k?
Dallam	Dallam	k1gInSc1
•	•	k?
Dallas	Dallas	k1gInSc1
•	•	k?
Dawson	Dawson	k1gInSc1
•	•	k?
Deaf	Deaf	k1gInSc1
Smith	Smith	k1gMnSc1
•	•	k?
Delta	delta	k1gFnSc1
•	•	k?
Denton	Denton	k1gInSc1
•	•	k?
DeWitt	DeWitt	k1gInSc1
•	•	k?
Dickens	Dickens	k1gInSc1
•	•	k?
Dimmit	Dimmit	k1gInSc1
•	•	k?
Donley	Donlea	k1gFnSc2
•	•	k?
Duval	Duval	k1gInSc1
•	•	k?
Eastland	Eastland	k1gInSc1
•	•	k?
Ector	Ector	k1gInSc1
•	•	k?
Edwards	Edwards	k1gInSc1
•	•	k?
El	Ela	k1gFnPc2
Paso	Paso	k1gMnSc1
•	•	k?
Ellis	Ellis	k1gInSc1
•	•	k?
Erath	Erath	k1gInSc1
•	•	k?
Falls	Falls	k1gInSc1
•	•	k?
Fannin	Fannin	k1gInSc1
•	•	k?
Fayette	Fayett	k1gInSc5
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Fisher	Fishra	k1gFnPc2
•	•	k?
Floyd	Floyd	k1gMnSc1
•	•	k?
Foard	Foard	k1gMnSc1
•	•	k?
Fort	Fort	k?
Bend	Bend	k1gInSc1
•	•	k?
Franklin	Franklin	k1gInSc1
•	•	k?
Freestone	Freeston	k1gInSc5
•	•	k?
Frio	Frio	k1gMnSc1
•	•	k?
Gaines	Gaines	k1gMnSc1
•	•	k?
Galveston	Galveston	k1gInSc1
•	•	k?
Garza	Garza	k1gFnSc1
•	•	k?
Gillespie	Gillespie	k1gFnSc2
•	•	k?
Glasscock	Glasscock	k1gInSc1
•	•	k?
Goliad	Goliad	k1gInSc1
•	•	k?
Gonzales	Gonzales	k1gInSc1
•	•	k?
Gray	Graa	k1gFnSc2
•	•	k?
Grayson	Grayson	k1gMnSc1
•	•	k?
Gregg	Gregg	k1gMnSc1
•	•	k?
Grimes	Grimes	k1gMnSc1
•	•	k?
Guadalupe	Guadalup	k1gInSc5
•	•	k?
Hale	hala	k1gFnSc3
•	•	k?
Hall	Hall	k1gInSc1
•	•	k?
Hamilton	Hamilton	k1gInSc1
•	•	k?
Hansford	Hansford	k1gMnSc1
•	•	k?
Hardeman	Hardeman	k1gMnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Hardin	Hardin	k2eAgInSc1d1
•	•	k?
Harris	Harris	k1gInSc1
•	•	k?
Harrison	Harrison	k1gMnSc1
•	•	k?
Hartley	Hartlea	k1gFnSc2
•	•	k?
Haskell	Haskell	k1gInSc1
•	•	k?
Hays	Hays	k1gInSc1
•	•	k?
Hemphill	Hemphill	k1gMnSc1
•	•	k?
Henderson	Henderson	k1gMnSc1
•	•	k?
Hidalgo	Hidalgo	k1gMnSc1
•	•	k?
Hill	Hill	k1gMnSc1
•	•	k?
Hockley	Hocklea	k1gFnSc2
•	•	k?
Hood	Hood	k1gInSc1
•	•	k?
Hopkins	Hopkins	k1gInSc1
•	•	k?
Houston	Houston	k1gInSc1
•	•	k?
Howard	Howard	k1gInSc1
•	•	k?
Hudspeth	Hudspeth	k1gInSc1
•	•	k?
Hunt	hunt	k1gInSc1
•	•	k?
Hutchinson	Hutchinson	k1gMnSc1
•	•	k?
Irion	Irion	k1gMnSc1
•	•	k?
Jack	Jack	k1gMnSc1
•	•	k?
Jackson	Jackson	k1gMnSc1
•	•	k?
Jasper	Jasper	k1gMnSc1
•	•	k?
Jeff	Jeff	k1gMnSc1
Davis	Davis	k1gFnSc2
•	•	k?
Jefferson	Jefferson	k1gMnSc1
•	•	k?
Jim	on	k3xPp3gMnPc3
<g />
.	.	kIx.
</s>
<s hack="1">
Hogg	Hogg	k1gMnSc1
•	•	k?
Jim	on	k3xPp3gFnPc3
Wells	Wells	k1gInSc1
•	•	k?
Johnson	Johnson	k1gMnSc1
•	•	k?
Jones	Jones	k1gMnSc1
•	•	k?
Karnes	Karnes	k1gMnSc1
•	•	k?
Kaufman	Kaufman	k1gMnSc1
•	•	k?
Kendall	Kendall	k1gInSc1
•	•	k?
Kenedy	Keneda	k1gMnSc2
•	•	k?
Kent	Kent	k1gMnSc1
•	•	k?
Kerr	Kerr	k1gMnSc1
•	•	k?
Kimble	Kimble	k1gMnSc1
•	•	k?
King	King	k1gMnSc1
•	•	k?
Kinney	Kinnea	k1gFnSc2
•	•	k?
Kleberg	Kleberg	k1gInSc1
•	•	k?
Knox	Knox	k1gInSc1
•	•	k?
La	la	k1gNnSc7
Salle	Salle	k1gFnSc2
•	•	k?
Lamar	Lamar	k1gMnSc1
•	•	k?
Lamb	Lamb	k1gMnSc1
•	•	k?
Lampasas	Lampasas	k1gMnSc1
•	•	k?
Lavaca	Lavaca	k1gMnSc1
•	•	k?
Lee	Lea	k1gFnSc6
•	•	k?
Leon	Leona	k1gFnPc2
•	•	k?
Liberty	Libert	k1gInPc1
•	•	k?
Limestone	Limeston	k1gInSc5
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Lipscomb	Lipscomb	k1gInSc1
•	•	k?
Live	Live	k1gInSc1
Oak	Oak	k1gFnSc1
•	•	k?
Llano	Llano	k6eAd1
•	•	k?
Loving	Loving	k1gInSc1
•	•	k?
Lubbock	Lubbock	k1gInSc1
•	•	k?
Lynn	Lynn	k1gInSc1
•	•	k?
Madison	Madison	k1gInSc1
•	•	k?
Marion	Marion	k1gInSc1
•	•	k?
Martin	Martin	k1gMnSc1
•	•	k?
Mason	mason	k1gMnSc1
•	•	k?
Matagorda	Matagorda	k1gMnSc1
•	•	k?
Maverick	Maverick	k1gMnSc1
•	•	k?
McCulloch	McCulloch	k1gMnSc1
•	•	k?
McLennan	McLennan	k1gMnSc1
•	•	k?
McMullen	McMullen	k1gInSc1
•	•	k?
Medina	Medina	k1gFnSc1
•	•	k?
Menard	Menard	k1gInSc1
•	•	k?
Midland	Midland	k1gInSc1
•	•	k?
Milam	Milam	k1gInSc1
•	•	k?
Mills	Mills	k1gInSc1
•	•	k?
Mitchell	Mitchell	k1gInSc1
•	•	k?
Montague	Montague	k1gInSc1
•	•	k?
Montgomery	Montgomera	k1gFnSc2
•	•	k?
Moore	Moor	k1gInSc5
•	•	k?
Morris	Morris	k1gFnSc6
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Motley	Motlea	k1gFnSc2
•	•	k?
Nacogdoches	Nacogdoches	k1gMnSc1
•	•	k?
Navarro	Navarra	k1gFnSc5
•	•	k?
Newton	Newton	k1gMnSc1
•	•	k?
Nolan	Nolan	k1gMnSc1
•	•	k?
Nueces	Nueces	k1gMnSc1
•	•	k?
Ochiltree	Ochiltree	k1gInSc1
•	•	k?
Oldham	Oldham	k1gInSc1
•	•	k?
Orange	Orange	k1gInSc1
•	•	k?
Palo	Pala	k1gMnSc5
Pinto	pinta	k1gFnSc5
•	•	k?
Panola	Panola	k1gFnSc1
•	•	k?
Parker	Parker	k1gMnSc1
•	•	k?
Parmer	Parmer	k1gMnSc1
•	•	k?
Pecos	Pecos	k1gMnSc1
•	•	k?
Polk	Polk	k1gMnSc1
•	•	k?
Potter	Potter	k1gMnSc1
•	•	k?
Presidio	Presidio	k1gMnSc1
•	•	k?
Rains	Rains	k1gInSc1
•	•	k?
Randall	Randall	k1gMnSc1
•	•	k?
Reagan	Reagan	k1gMnSc1
•	•	k?
Real	Real	k1gInSc1
•	•	k?
Red	Red	k1gFnSc2
River	River	k1gMnSc1
•	•	k?
Reeves	Reeves	k1gMnSc1
•	•	k?
Refugio	Refugio	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Roberts	Roberts	k1gInSc1
•	•	k?
Robertson	Robertson	k1gInSc1
•	•	k?
Rockwall	Rockwall	k1gInSc1
•	•	k?
Runnels	Runnels	k1gInSc1
•	•	k?
Rusk	Rusk	k1gInSc1
•	•	k?
Sabine	Sabin	k1gInSc5
•	•	k?
San	San	k1gMnSc5
Augustine	Augustin	k1gMnSc5
•	•	k?
San	San	k1gMnSc1
Jacinto	Jacinta	k1gFnSc5
•	•	k?
San	San	k1gMnSc1
Patricio	Patricio	k1gMnSc1
•	•	k?
San	San	k1gMnSc1
Saba	Sabum	k1gNnSc2
•	•	k?
Schleicher	Schleichra	k1gFnPc2
•	•	k?
Scurry	Scurr	k1gInPc1
•	•	k?
Shackelford	Shackelford	k1gInSc1
•	•	k?
Shelby	Shelba	k1gFnSc2
•	•	k?
Sherman	Sherman	k1gMnSc1
•	•	k?
Smith	Smith	k1gMnSc1
•	•	k?
Somervell	Somervell	k1gMnSc1
•	•	k?
Starr	Starr	k1gMnSc1
•	•	k?
Stephens	Stephens	k1gInSc1
•	•	k?
Sterling	sterling	k1gInSc1
•	•	k?
Stonewall	Stonewall	k1gInSc1
•	•	k?
Sutton	Sutton	k1gInSc1
•	•	k?
Swisher	Swishra	k1gFnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Tarrant	Tarrant	k1gMnSc1
•	•	k?
Taylor	Taylor	k1gMnSc1
•	•	k?
Terrell	Terrell	k1gMnSc1
•	•	k?
Terry	Terra	k1gFnSc2
•	•	k?
Throckmorton	Throckmorton	k1gInSc1
•	•	k?
Titus	Titus	k1gMnSc1
•	•	k?
Tom	Tom	k1gMnSc1
Green	Green	k2eAgMnSc1d1
•	•	k?
Travis	Travis	k1gInSc1
•	•	k?
Trinity	Trinita	k1gFnSc2
•	•	k?
Tyler	Tyler	k1gMnSc1
•	•	k?
Upshur	Upshur	k1gMnSc1
•	•	k?
Upton	Upton	k1gMnSc1
•	•	k?
Uvalde	Uvald	k1gInSc5
•	•	k?
Val	val	k1gInSc4
Verde	Verd	k1gInSc5
•	•	k?
Van	van	k1gInSc1
Zandt	Zandt	k1gInSc1
•	•	k?
Victoria	Victorium	k1gNnSc2
•	•	k?
Walker	Walker	k1gMnSc1
•	•	k?
Waller	Waller	k1gMnSc1
•	•	k?
Ward	Ward	k1gMnSc1
•	•	k?
Washington	Washington	k1gInSc1
•	•	k?
Webb	Webb	k1gInSc1
•	•	k?
Wharton	Wharton	k1gInSc1
•	•	k?
Wheeler	Wheeler	k1gMnSc1
•	•	k?
Wichita	Wichita	k1gMnSc1
•	•	k?
Wilbarger	Wilbarger	k1gMnSc1
•	•	k?
Willacy	Willaca	k1gFnSc2
•	•	k?
Williamson	Williamson	k1gMnSc1
•	•	k?
Wilson	Wilson	k1gMnSc1
•	•	k?
Winkler	Winkler	k1gMnSc1
•	•	k?
Wise	Wis	k1gFnSc2
•	•	k?
Wood	Wood	k1gMnSc1
•	•	k?
Yoakum	Yoakum	k1gInSc1
•	•	k?
Young	Young	k1gInSc1
•	•	k?
Zapata	Zapat	k1gMnSc2
•	•	k?
Zavala	Zaval	k1gMnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k2eAgInSc4d1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc4
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc4
<g/>
:	:	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
79116000	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
141884854	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
79116000	#num#	k4
</s>
