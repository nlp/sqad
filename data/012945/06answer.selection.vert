<s>
Slavnost	slavnost	k1gFnSc1	slavnost
Všech	všecek	k3xTgFnPc2	všecek
svatých	svatá	k1gFnPc2	svatá
je	být	k5eAaImIp3nS	být
křesťanský	křesťanský	k2eAgInSc1d1	křesťanský
svátek	svátek	k1gInSc1	svátek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
připomíná	připomínat	k5eAaImIp3nS	připomínat
všechny	všechen	k3xTgFnPc4	všechen
svaté	svatá	k1gFnPc4	svatá
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
jsou	být	k5eAaImIp3nP	být
oficiálně	oficiálně	k6eAd1	oficiálně
kanonizováni	kanonizován	k2eAgMnPc1d1	kanonizován
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
o	o	k7c6	o
"	"	kIx"	"
<g/>
jejichž	jejichž	k3xOyRp3gFnPc4	jejichž
svatosti	svatost	k1gFnPc4	svatost
neví	vědět	k5eNaImIp3nS	vědět
kromě	kromě	k7c2	kromě
Boha	bůh	k1gMnSc2	bůh
nikdo	nikdo	k3yNnSc1	nikdo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
