<p>
<s>
Slavnost	slavnost	k1gFnSc1	slavnost
Všech	všecek	k3xTgFnPc2	všecek
svatých	svatá	k1gFnPc2	svatá
je	být	k5eAaImIp3nS	být
křesťanský	křesťanský	k2eAgInSc1d1	křesťanský
svátek	svátek	k1gInSc1	svátek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
připomíná	připomínat	k5eAaImIp3nS	připomínat
všechny	všechen	k3xTgFnPc4	všechen
svaté	svatá	k1gFnPc4	svatá
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jsou	být	k5eAaImIp3nP	být
oficiálně	oficiálně	k6eAd1	oficiálně
kanonizováni	kanonizován	k2eAgMnPc1d1	kanonizován
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
ty	ten	k3xDgInPc1	ten
<g/>
,	,	kIx,	,
o	o	k7c6	o
"	"	kIx"	"
<g/>
jejichž	jejichž	k3xOyRp3gFnPc4	jejichž
svatosti	svatost	k1gFnPc4	svatost
neví	vědět	k5eNaImIp3nS	vědět
kromě	kromě	k7c2	kromě
Boha	bůh	k1gMnSc2	bůh
nikdo	nikdo	k3yNnSc1	nikdo
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Svátek	svátek	k1gInSc1	svátek
je	být	k5eAaImIp3nS	být
slaven	slavit	k5eAaImNgInS	slavit
napříč	napříč	k7c7	napříč
katolickými	katolický	k2eAgInPc7d1	katolický
a	a	k8xC	a
ortodoxním	ortodoxní	k2eAgNnPc3d1	ortodoxní
křesťanstvím	křesťanství	k1gNnPc3	křesťanství
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
církve	církev	k1gFnSc2	církev
vzešlé	vzešlý	k2eAgFnSc6d1	vzešlá
ze	z	k7c2	z
západního	západní	k2eAgNnSc2d1	západní
křesťanství	křesťanství	k1gNnSc2	křesťanství
jej	on	k3xPp3gMnSc4	on
slaví	slavit	k5eAaImIp3nS	slavit
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
východní	východní	k2eAgMnPc1d1	východní
křesťané	křesťan	k1gMnPc1	křesťan
první	první	k4xOgFnSc4	první
neděli	neděle	k1gFnSc4	neděle
po	po	k7c6	po
letnicích	letnice	k1gFnPc6	letnice
<g/>
.	.	kIx.	.
</s>
<s>
Římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
církev	církev	k1gFnSc1	církev
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
další	další	k2eAgFnPc1d1	další
západní	západní	k2eAgFnPc1d1	západní
církve	církev	k1gFnPc1	církev
navazují	navazovat	k5eAaImIp3nP	navazovat
2	[number]	k4	2
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
svátek	svátek	k1gInSc4	svátek
Vzpomínkou	vzpomínka	k1gFnSc7	vzpomínka
na	na	k7c4	na
všechny	všechen	k3xTgMnPc4	všechen
věrné	věrný	k2eAgMnPc4d1	věrný
zemřelé	zemřelý	k1gMnPc4	zemřelý
<g/>
,	,	kIx,	,
lidově	lidově	k6eAd1	lidově
tzv.	tzv.	kA	tzv.
Dušičkami	Dušičky	k1gFnPc7	Dušičky
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tyto	tento	k3xDgInPc4	tento
dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
s	s	k7c7	s
křesťanskou	křesťanský	k2eAgFnSc7d1	křesťanská
tradicí	tradice	k1gFnSc7	tradice
zvykem	zvyk	k1gInSc7	zvyk
navštívit	navštívit	k5eAaPmF	navštívit
hřbitov	hřbitov	k1gInSc4	hřbitov
a	a	k8xC	a
rodinný	rodinný	k2eAgInSc4d1	rodinný
hrob	hrob	k1gInSc4	hrob
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
rozptylovou	rozptylový	k2eAgFnSc4d1	rozptylová
loučku	loučka	k1gFnSc4	loučka
<g/>
)	)	kIx)	)
a	a	k8xC	a
zapálit	zapálit	k5eAaPmF	zapálit
zde	zde	k6eAd1	zde
svíčku	svíčka	k1gFnSc4	svíčka
<g/>
,	,	kIx,	,
položit	položit	k5eAaPmF	položit
kytici	kytice	k1gFnSc4	kytice
či	či	k8xC	či
věnec	věnec	k1gInSc4	věnec
a	a	k8xC	a
se	s	k7c7	s
vzpomínkou	vzpomínka	k1gFnSc7	vzpomínka
se	se	k3xPyFc4	se
pomodlit	pomodlit	k5eAaPmF	pomodlit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
svátku	svátek	k1gInSc2	svátek
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
Východě	východ	k1gInSc6	východ
se	se	k3xPyFc4	se
od	od	k7c2	od
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
slavil	slavit	k5eAaImAgMnS	slavit
společný	společný	k2eAgInSc4d1	společný
svátek	svátek	k1gInSc4	svátek
všech	všecek	k3xTgMnPc2	všecek
mučedníků	mučedník	k1gMnPc2	mučedník
(	(	kIx(	(
<g/>
v	v	k7c6	v
syrském	syrský	k2eAgInSc6d1	syrský
obřadu	obřad	k1gInSc6	obřad
v	v	k7c6	v
době	doba	k1gFnSc6	doba
velikonoční	velikonoční	k2eAgNnSc1d1	velikonoční
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
byzantském	byzantský	k2eAgInSc6d1	byzantský
obřadu	obřad	k1gInSc6	obřad
první	první	k4xOgFnSc4	první
neděli	neděle	k1gFnSc4	neděle
po	po	k7c6	po
Letnicích	letnice	k1gFnPc6	letnice
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
zavedení	zavedení	k1gNnSc1	zavedení
svátku	svátek	k1gInSc2	svátek
byla	být	k5eAaImAgFnS	být
nemožnost	nemožnost	k1gFnSc1	nemožnost
slavit	slavit	k5eAaImF	slavit
každý	každý	k3xTgInSc4	každý
jednotlivý	jednotlivý	k2eAgInSc4d1	jednotlivý
den	den	k1gInSc4	den
světců	světec	k1gMnPc2	světec
<g/>
.	.	kIx.	.
</s>
<s>
Papež	Papež	k1gMnSc1	Papež
Řehoř	Řehoř	k1gMnSc1	Řehoř
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
následně	následně	k6eAd1	následně
Řehoř	Řehoř	k1gMnSc1	Řehoř
IV	IV	kA	IV
<g/>
.	.	kIx.	.
svátek	svátek	k1gInSc1	svátek
pro	pro	k7c4	pro
západní	západní	k2eAgFnSc4d1	západní
církev	církev	k1gFnSc4	církev
v	v	k7c6	v
7	[number]	k4	7
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
přesunuli	přesunout	k5eAaPmAgMnP	přesunout
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopad	listopad	k1gInSc4	listopad
<g/>
,	,	kIx,	,
výroční	výroční	k2eAgInSc4d1	výroční
den	den	k1gInSc4	den
posvěcení	posvěcení	k1gNnSc2	posvěcení
římského	římský	k2eAgInSc2d1	římský
Pantheonu	Pantheon	k1gInSc2	Pantheon
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
pohanského	pohanský	k2eAgInSc2d1	pohanský
chrámu	chrám	k1gInSc2	chrám
všech	všecek	k3xTgMnPc2	všecek
olympských	olympský	k2eAgMnPc2d1	olympský
bohů	bůh	k1gMnPc2	bůh
<g/>
)	)	kIx)	)
na	na	k7c4	na
křesťanský	křesťanský	k2eAgInSc4d1	křesťanský
kostel	kostel	k1gInSc4	kostel
dedikovaný	dedikovaný	k2eAgInSc4d1	dedikovaný
(	(	kIx(	(
<g/>
dedikace	dedikace	k1gFnSc1	dedikace
=	=	kIx~	=
věnování	věnování	k1gNnSc1	věnování
<g/>
,	,	kIx,	,
připsání	připsání	k1gNnSc1	připsání
<g/>
)	)	kIx)	)
Panně	Panna	k1gFnSc3	Panna
Marii	Maria	k1gFnSc3	Maria
a	a	k8xC	a
všem	všecek	k3xTgMnPc3	všecek
svatým	svatý	k2eAgMnPc3d1	svatý
mučedníkům	mučedník	k1gMnPc3	mučedník
(	(	kIx(	(
<g/>
13	[number]	k4	13
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
609	[number]	k4	609
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
8	[number]	k4	8
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
a	a	k8xC	a
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
začal	začít	k5eAaPmAgMnS	začít
slavit	slavit	k5eAaImF	slavit
svátek	svátek	k1gInSc4	svátek
Všech	všecek	k3xTgFnPc2	všecek
svatých	svatá	k1gFnPc2	svatá
(	(	kIx(	(
<g/>
nejen	nejen	k6eAd1	nejen
mučedníků	mučedník	k1gMnPc2	mučedník
<g/>
)	)	kIx)	)
1	[number]	k4	1
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
(	(	kIx(	(
<g/>
snad	snad	k9	snad
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
Keltů	Kelt	k1gMnPc2	Kelt
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc4	tento
den	den	k1gInSc4	den
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Slavení	slavení	k1gNnSc1	slavení
tohoto	tento	k3xDgInSc2	tento
svátku	svátek	k1gInSc2	svátek
se	se	k3xPyFc4	se
záhy	záhy	k6eAd1	záhy
rozšířilo	rozšířit	k5eAaPmAgNnS	rozšířit
a	a	k8xC	a
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
se	se	k3xPyFc4	se
tento	tento	k3xDgInSc1	tento
svátek	svátek	k1gInSc1	svátek
slaví	slavit	k5eAaImIp3nS	slavit
od	od	k7c2	od
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Svatý	svatý	k2eAgMnSc1d1	svatý
</s>
</p>
<p>
<s>
Dušičky	Dušičky	k1gFnPc1	Dušičky
</s>
</p>
<p>
<s>
Halloween	Halloween	k1gInSc1	Halloween
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Všech	všecek	k3xTgFnPc2	všecek
svatých	svatá	k1gFnPc2	svatá
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slavnost	slavnost	k1gFnSc1	slavnost
Všech	všecek	k3xTgFnPc2	všecek
svatých	svatá	k1gFnPc2	svatá
a	a	k8xC	a
Dušičky	dušička	k1gFnSc2	dušička
na	na	k7c6	na
Katolik	Katolik	k1gMnSc1	Katolik
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
