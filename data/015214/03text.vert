<s>
Rodrigo	Rodrigo	k1gMnSc1
Palacio	Palacio	k1gMnSc1
</s>
<s>
Rodrigo	Rodrigo	k1gMnSc1
Palacio	Palacio	k1gMnSc1
Rodrigo	Rodrigo	k1gMnSc1
Palacio	Palacio	k1gMnSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
Osobní	osobní	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Celé	celý	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Rodrigo	Rodrigo	k1gMnSc1
Sebastián	Sebastián	k1gMnSc1
Palacio	Palacio	k1gMnSc1
Alcalde	Alcald	k1gInSc5
Datum	datum	k1gNnSc1
narození	narození	k1gNnSc2
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1982	#num#	k4
(	(	kIx(
<g/>
39	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Místo	místo	k7c2
narození	narození	k1gNnSc2
</s>
<s>
Bahía	Bahíus	k1gMnSc4
Blanca	Blanc	k1gMnSc4
<g/>
,	,	kIx,
Argentina	Argentina	k1gFnSc1
Výška	výška	k1gFnSc1
</s>
<s>
176	#num#	k4
cm	cm	kA
Klubové	klubový	k2eAgFnSc2d1
informace	informace	k1gFnSc2
Současný	současný	k2eAgInSc1d1
klub	klub	k1gInSc1
</s>
<s>
Bologna	Bologna	k1gFnSc1
Číslo	číslo	k1gNnSc4
dresu	dres	k1gInSc2
</s>
<s>
24	#num#	k4
Pozice	pozice	k1gFnSc1
</s>
<s>
útočník	útočník	k1gMnSc1
Mládežnické	mládežnický	k2eAgInPc4d1
kluby	klub	k1gInPc4
</s>
<s>
1990	#num#	k4
<g/>
–	–	k?
<g/>
2002	#num#	k4
</s>
<s>
Bella	Bella	k1gMnSc1
Vista	vista	k2eAgFnSc2d1
</s>
<s>
Profesionální	profesionální	k2eAgInPc1d1
kluby	klub	k1gInPc1
</s>
<s>
Roky	rok	k1gInPc1
</s>
<s>
Klub	klub	k1gInSc1
</s>
<s>
Záp	Záp	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
góly	gól	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
2002	#num#	k4
<g/>
–	–	k?
<g/>
20042004	#num#	k4
<g/>
–	–	k?
<g/>
20052005	#num#	k4
<g/>
–	–	k?
<g/>
2009	#num#	k4
2009	#num#	k4
<g/>
–	–	k?
<g/>
2012	#num#	k4
2012	#num#	k4
<g/>
–	–	k?
<g/>
20172017	#num#	k4
<g/>
–	–	k?
CA	ca	kA
Huracán	Huracán	k2eAgInSc4d1
Banfield	Banfield	k1gInSc4
Boca	Boc	k1gInSc2
Juniors	Juniors	k1gInSc4
Janov	Janov	k1gInSc1
Inter	Inter	k1gInSc1
Milán	Milán	k1gInSc1
Bologna	Bologna	k1gFnSc1
<g/>
0	#num#	k4
<g/>
530	#num#	k4
<g/>
(	(	kIx(
<g/>
15	#num#	k4
<g/>
)	)	kIx)
<g/>
0	#num#	k4
<g/>
3600	#num#	k4
<g/>
(	(	kIx(
<g/>
9	#num#	k4
<g/>
)	)	kIx)
<g/>
1310	#num#	k4
<g/>
(	(	kIx(
<g/>
54	#num#	k4
<g/>
)	)	kIx)
<g/>
0	#num#	k4
<g/>
900	#num#	k4
<g/>
(	(	kIx(
<g/>
35	#num#	k4
<g/>
)	)	kIx)
<g/>
1400	#num#	k4
<g/>
(	(	kIx(
<g/>
39	#num#	k4
<g/>
)	)	kIx)
<g/>
1100	#num#	k4
<g/>
(	(	kIx(
<g/>
15	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Reprezentace	reprezentace	k1gFnSc1
<g/>
**	**	k?
</s>
<s>
Roky	rok	k1gInPc1
</s>
<s>
Reprezentace	reprezentace	k1gFnSc1
</s>
<s>
Záp	Záp	k?
<g/>
.	.	kIx.
(	(	kIx(
<g/>
góly	gól	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
2005	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
Argentina	Argentina	k1gFnSc1
<g/>
0	#num#	k4
<g/>
2700	#num#	k4
<g/>
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Úspěchy	úspěch	k1gInPc1
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
</s>
<s>
MS	MS	kA
2014	#num#	k4
</s>
<s>
Argentina	Argentina	k1gFnSc1
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
</s>
<s>
→	→	k?
Šipka	šipka	k1gFnSc1
znamená	znamenat	k5eAaImIp3nS
hostování	hostování	k1gNnSc4
hráče	hráč	k1gMnSc2
v	v	k7c6
daném	daný	k2eAgInSc6d1
klubu	klub	k1gInSc6
<g/>
.	.	kIx.
<g/>
*	*	kIx~
Starty	start	k1gInPc1
a	a	k8xC
góly	gól	k1gInPc1
v	v	k7c6
domácí	domácí	k2eAgFnSc6d1
lize	liga	k1gFnSc6
za	za	k7c4
klub	klub	k1gInSc4
aktuální	aktuální	k2eAgInSc4d1
k	k	k7c3
24	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2021	#num#	k4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Rodrigo	Rodrigo	k1gMnSc1
Sebastián	Sebastián	k1gMnSc1
Palacio	Palacio	k1gMnSc1
Alcalde	Alcald	k1gInSc5
[	[	kIx(
<g/>
rodrigo	rodrigo	k1gMnSc1
sebastiján	sebastiján	k1gMnSc1
palasjo	palasjo	k6eAd1
alkalde	alkalde	k6eAd1
<g/>
]	]	kIx)
(	(	kIx(
<g/>
*	*	kIx~
5	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
1982	#num#	k4
<g/>
,	,	kIx,
Bahía	Bahí	k2eAgMnSc4d1
Blanca	Blanc	k1gMnSc4
<g/>
,	,	kIx,
Argentina	Argentina	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
argentinský	argentinský	k2eAgMnSc1d1
fotbalový	fotbalový	k2eAgMnSc1d1
útočník	útočník	k1gMnSc1
a	a	k8xC
bývalý	bývalý	k2eAgMnSc1d1
reprezentant	reprezentant	k1gMnSc1
<g/>
,	,	kIx,
od	od	k7c2
roku	rok	k1gInSc2
2017	#num#	k4
působí	působit	k5eAaImIp3nS
v	v	k7c6
italském	italský	k2eAgInSc6d1
klubu	klub	k1gInSc6
Bologna	Bologna	k1gFnSc1
FC	FC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Operuje	operovat	k5eAaImIp3nS
nejčastěji	často	k6eAd3
jako	jako	k8xC,k8xS
druhý	druhý	k4xOgMnSc1
útočník	útočník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Účastník	účastník	k1gMnSc1
MS	MS	kA
2014	#num#	k4
v	v	k7c6
Brazílii	Brazílie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Klubová	klubový	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
V	v	k7c6
Argentině	Argentina	k1gFnSc6
působil	působit	k5eAaImAgMnS
postupně	postupně	k6eAd1
v	v	k7c6
klubech	klub	k1gInPc6
Club	club	k1gInSc4
Bella	Bella	k1gMnSc1
Vista	vista	k2eAgMnSc1d1
<g/>
,	,	kIx,
Huracán	Huracán	k2eAgInSc1d1
de	de	k?
Tres	tresa	k1gFnPc2
Arroyos	Arroyos	k1gMnSc1
<g/>
,	,	kIx,
CA	ca	kA
Banfield	Banfield	k1gInSc1
a	a	k8xC
CA	ca	kA
Boca	Boca	k1gFnSc1
Juniors	Juniors	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červenci	červenec	k1gInSc6
2009	#num#	k4
odešel	odejít	k5eAaPmAgMnS
do	do	k7c2
Itálie	Itálie	k1gFnSc2
do	do	k7c2
klubu	klub	k1gInSc2
FC	FC	kA
Janov	Janov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červenci	červenec	k1gInSc6
2012	#num#	k4
přestoupil	přestoupit	k5eAaPmAgMnS
do	do	k7c2
Interu	Inter	k1gInSc2
Milán	Milán	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
2017	#num#	k4
působí	působit	k5eAaImIp3nS
v	v	k7c6
Bologni	Bologna	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
začátku	začátek	k1gInSc6
března	březen	k1gInSc2
roku	rok	k1gInSc2
2021	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
nejstarším	starý	k2eAgMnSc7d3
fotbalistou	fotbalista	k1gMnSc7
v	v	k7c6
dějinách	dějiny	k1gFnPc6
nejvyšší	vysoký	k2eAgFnSc2d3
italské	italský	k2eAgFnSc2d1
ligové	ligový	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
dokázal	dokázat	k5eAaPmAgMnS
vsítit	vsítit	k5eAaPmF
hattrick	hattrick	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
ve	v	k7c6
věku	věk	k1gInSc6
39	#num#	k4
let	léto	k1gNnPc2
a	a	k8xC
86	#num#	k4
dní	den	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
tak	tak	k9
2	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
v	v	k7c6
utkání	utkání	k1gNnSc6
s	s	k7c7
Fiorentinou	Fiorentina	k1gFnSc7
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
Bologna	Bologna	k1gFnSc1
uhrála	uhrát	k5eAaPmAgFnS
díky	díky	k7c3
Palaciovi	Palaciův	k2eAgMnPc1d1
venkovní	venkovní	k2eAgFnSc1d1
remízu	remíz	k1gInSc2
3	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reprezentační	reprezentační	k2eAgFnSc1d1
kariéra	kariéra	k1gFnSc1
</s>
<s>
Rodrigo	Rodrigo	k1gMnSc1
Palacio	Palacio	k1gMnSc1
debutoval	debutovat	k5eAaBmAgMnS
v	v	k7c6
národním	národní	k2eAgInSc6d1
týmu	tým	k1gInSc6
Argentiny	Argentina	k1gFnSc2
9	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2005	#num#	k4
proti	proti	k7c3
Mexiku	Mexiko	k1gNnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Trenér	trenér	k1gMnSc1
Alejandro	Alejandra	k1gFnSc5
Sabella	Sabella	k1gMnSc1
jej	on	k3xPp3gMnSc4
vzal	vzít	k5eAaPmAgMnS
na	na	k7c4
Mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
2014	#num#	k4
v	v	k7c6
Brazílii	Brazílie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
finále	finále	k1gNnSc6
s	s	k7c7
Německem	Německo	k1gNnSc7
Argentina	Argentina	k1gFnSc1
prohrála	prohrát	k5eAaPmAgFnS
0	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
v	v	k7c6
prodloužení	prodloužení	k1gNnSc6
a	a	k8xC
získala	získat	k5eAaPmAgFnS
stříbrné	stříbrný	k2eAgFnPc4d1
medaile	medaile	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Palacio	Palacio	k1gMnSc1
<g/>
,	,	kIx,
Budimir	Budimir	k1gMnSc1
či	či	k8xC
Nestorovski	Nestorovsk	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Itálii	Itálie	k1gFnSc6
už	už	k6eAd1
řeší	řešit	k5eAaImIp3nS
Stramaccioniho	Stramaccioni	k1gMnSc4
posily	posila	k1gFnSc2
<g/>
,	,	kIx,
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
cit	cit	k1gInSc1
<g/>
.	.	kIx.
27	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
↑	↑	k?
Palacio	Palacio	k1gMnSc1
ältester	ältester	k1gMnSc1
Hattrick-Schütze	Hattrick-Schütze	k1gFnSc2
in	in	k?
Serie	serie	k1gFnSc2
A.	A.	kA
weltfussball	weltfussball	k1gMnSc1
<g/>
.	.	kIx.
<g/>
at	at	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2021-05-03	2021-05-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Profil	profil	k1gInSc1
hráče	hráč	k1gMnSc2
<g/>
,	,	kIx,
FIFA	FIFA	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
citováno	citován	k2eAgNnSc4d1
15	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
2014	#num#	k4
FIFA	FIFA	kA
World	World	k1gInSc1
Cup	cup	k1gInSc1
Brazil	Brazil	k1gFnSc1
™	™	k?
-	-	kIx~
List	list	k1gInSc1
of	of	k?
Players	Players	k1gInSc1
<g/>
,	,	kIx,
FIFA	FIFA	kA
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
citováno	citován	k2eAgNnSc4d1
28	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ovládlo	ovládnout	k5eAaPmAgNnS
Německo	Německo	k1gNnSc1
<g/>
,	,	kIx,
Argentinu	Argentina	k1gFnSc4
porazilo	porazit	k5eAaPmAgNnS
po	po	k7c6
prodloužení	prodloužení	k1gNnSc6
<g/>
,	,	kIx,
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
citováno	citován	k2eAgNnSc4d1
14	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Rodrigo	Rodrigo	k6eAd1
Palacio	Palacio	k6eAd1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Profil	profil	k1gInSc1
hráče	hráč	k1gMnSc2
na	na	k7c4
Transfermarkt	Transfermarkt	k1gInSc4
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Profil	profil	k1gInSc1
hráče	hráč	k1gMnSc2
na	na	k7c6
National	National	k1gFnSc6
Football	Footballa	k1gFnPc2
Teams	Teamsa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Medailisté	medailista	k1gMnPc1
–	–	k?
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
2014	#num#	k4
Německo	Německo	k1gNnSc1
</s>
<s>
Manuel	Manuel	k1gMnSc1
Neuer	Neuer	k1gMnSc1
•	•	k?
Ron-Robert	Ron-Robert	k1gMnSc1
Zieler	Zieler	k1gMnSc1
•	•	k?
Roman	Roman	k1gMnSc1
Weidenfeller	Weidenfeller	k1gMnSc1
•	•	k?
Kevin	Kevin	k1gMnSc1
Großkreutz	Großkreutz	k1gMnSc1
•	•	k?
Benedikt	Benedikt	k1gMnSc1
Höwedes	Höwedes	k1gMnSc1
•	•	k?
Mats	Mats	k1gInSc1
Hummels	Hummels	k1gInSc1
•	•	k?
Erik	Erik	k1gMnSc1
Durm	Durm	k1gMnSc1
•	•	k?
Philipp	Philipp	k1gMnSc1
Lahm	Lahm	k1gMnSc1
–	–	k?
•	•	k?
Per	pero	k1gNnPc2
Mertesacker	Mertesackero	k1gNnPc2
•	•	k?
Jérôme	Jérôm	k1gInSc5
Boateng	Boateng	k1gInSc4
•	•	k?
Shkodran	Shkodran	k1gInSc1
Mustafi	Mustaf	k1gFnSc2
•	•	k?
Matthias	Matthias	k1gMnSc1
Ginter	Ginter	k1gMnSc1
•	•	k?
Sami	sám	k3xTgMnPc1
Khedira	Khedira	k1gMnSc1
•	•	k?
Bastian	Bastian	k1gMnSc1
Schweinsteiger	Schweinsteiger	k1gMnSc1
•	•	k?
Mesut	Mesut	k1gMnSc1
Özil	Özil	k1gMnSc1
•	•	k?
André	André	k1gMnSc1
Schürrle	Schürrle	k1gFnSc2
•	•	k?
Lukas	Lukas	k1gMnSc1
Podolski	Podolsk	k1gFnSc2
•	•	k?
Thomas	Thomas	k1gMnSc1
Müller	Müller	k1gMnSc1
•	•	k?
Julian	Julian	k1gMnSc1
Draxler	Draxler	k1gMnSc1
•	•	k?
Toni	Toni	k1gMnSc1
Kroos	Kroos	k1gMnSc1
•	•	k?
Mario	Mario	k1gMnSc1
Götze	Götze	k1gFnSc2
•	•	k?
Christoph	Christoph	k1gMnSc1
Kramer	Kramer	k1gMnSc1
•	•	k?
Miroslav	Miroslav	k1gMnSc1
Klose	Klose	k1gFnSc2
</s>
<s>
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Joachim	Joachim	k1gMnSc1
Löw	Löw	k1gFnSc2
Argentina	Argentina	k1gFnSc1
</s>
<s>
Sergio	Sergio	k1gMnSc1
Germán	Germán	k1gMnSc1
Romero	Romero	k1gNnSc1
•	•	k?
Agustín	Agustín	k1gMnSc1
Orión	Orión	k1gMnSc1
•	•	k?
Mariano	Mariana	k1gFnSc5
Andújar	Andújar	k1gMnSc1
•	•	k?
Ezequiel	Ezequiel	k1gMnSc1
Garay	Garaa	k1gFnSc2
•	•	k?
Hugo	Hugo	k1gMnSc1
Campagnaro	Campagnara	k1gFnSc5
•	•	k?
Pablo	Pablo	k1gNnSc4
Zabaleta	Zabaleta	k1gFnSc1
•	•	k?
Martín	Martín	k1gInSc1
Demichelis	Demichelis	k1gFnSc2
•	•	k?
Marcos	Marcos	k1gMnSc1
Rojo	Rojo	k1gMnSc1
•	•	k?
Federico	Federico	k1gMnSc1
Fernández	Fernández	k1gMnSc1
•	•	k?
José	Josá	k1gFnSc2
María	Maríus	k1gMnSc2
Basanta	Basant	k1gMnSc2
•	•	k?
Fernando	Fernanda	k1gFnSc5
Gago	Gago	k1gMnSc1
•	•	k?
Lucas	Lucas	k1gMnSc1
Biglia	Biglium	k1gNnSc2
•	•	k?
Ángel	Ángel	k1gMnSc1
Di	Di	k1gMnSc1
María	María	k1gMnSc1
•	•	k?
Enzo	Enzo	k1gMnSc1
Pérez	Pérez	k1gMnSc1
•	•	k?
Maxi	maxi	k2eAgMnSc1d1
Rodríguez	Rodríguez	k1gMnSc1
•	•	k?
Augusto	Augusta	k1gMnSc5
Fernández	Fernández	k1gMnSc1
•	•	k?
Javier	Javier	k1gMnSc1
Mascherano	Mascherana	k1gFnSc5
•	•	k?
Ricardo	Ricardo	k1gNnSc1
Gabriel	Gabriel	k1gMnSc1
Álvarez	Álvarez	k1gMnSc1
•	•	k?
Gonzalo	Gonzalo	k1gMnSc1
Higuaín	Higuaín	k1gMnSc1
•	•	k?
Lionel	Lionel	k1gMnSc1
Messi	Mess	k1gMnSc3
–	–	k?
•	•	k?
Rodrigo	Rodrigo	k1gMnSc1
Palacio	Palacio	k1gMnSc1
•	•	k?
Sergio	Sergio	k1gMnSc1
Agüero	Agüero	k1gNnSc1
•	•	k?
Ezequiel	Ezequiel	k1gMnSc1
Lavezzi	Lavezze	k1gFnSc3
</s>
<s>
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Alejandro	Alejandra	k1gFnSc5
Sabella	Sabell	k1gMnSc4
Nizozemsko	Nizozemsko	k1gNnSc1
</s>
<s>
Jasper	Jasper	k1gMnSc1
Cillessen	Cillessen	k2eAgMnSc1d1
•	•	k?
Michel	Michel	k1gMnSc1
Vorm	Vorm	k1gMnSc1
•	•	k?
Tim	Tim	k?
Krul	krul	k1gInSc1
•	•	k?
Ron	ron	k1gInSc1
Vlaar	Vlaar	k1gMnSc1
•	•	k?
Stefan	Stefan	k1gMnSc1
de	de	k?
Vrij	Vrij	k1gMnSc1
•	•	k?
Bruno	Bruno	k1gMnSc1
Martins	Martinsa	k1gFnPc2
Indi	Ind	k1gFnSc2
•	•	k?
Daley	Dalea	k1gFnSc2
Blind	Blind	k1gMnSc1
•	•	k?
Daryl	Daryl	k1gInSc1
Janmaat	Janmaat	k1gInSc1
•	•	k?
Paul	Paul	k1gMnSc1
Verhaegh	Verhaegh	k1gMnSc1
•	•	k?
Joël	Joël	k1gMnSc1
Veltman	Veltman	k1gMnSc1
•	•	k?
Terence	Terence	k1gFnSc2
Kongolo	Kongola	k1gFnSc5
•	•	k?
Jonathan	Jonathan	k1gMnSc1
de	de	k?
Guzmán	Guzmán	k2eAgInSc1d1
•	•	k?
Nigel	Nigel	k1gInSc1
de	de	k?
Jong	Jong	k1gInSc1
•	•	k?
Wesley	Weslea	k1gFnSc2
Sneijder	Sneijder	k1gMnSc1
•	•	k?
Jordy	Jorda	k1gFnSc2
Clasie	Clasie	k1gFnSc2
•	•	k?
Leroy	Leroa	k1gFnSc2
Fer	Fer	k1gMnSc1
•	•	k?
Georginio	Georginio	k6eAd1
Wijnaldum	Wijnaldum	k1gInSc1
•	•	k?
Robin	robin	k2eAgInSc1d1
van	van	k1gInSc1
Persie	Persie	k1gFnSc2
–	–	k?
•	•	k?
Arjen	Arjen	k1gInSc1
Robben	Robben	k2eAgInSc1d1
•	•	k?
Dirk	Dirk	k1gInSc1
Kuijt	Kuijt	k1gInSc1
•	•	k?
Jeremain	Jeremain	k2eAgInSc1d1
Lens	Lens	k1gInSc1
•	•	k?
Klaas-Jan	Klaas-Jan	k1gInSc1
Huntelaar	Huntelaar	k1gInSc1
•	•	k?
Memphis	Memphis	k1gFnSc2
Depay	Depaa	k1gFnSc2
</s>
<s>
Trenér	trenér	k1gMnSc1
<g/>
:	:	kIx,
Louis	Louis	k1gMnSc1
van	vana	k1gFnPc2
Gaal	Gaal	k1gMnSc1
</s>
<s>
Argentinská	argentinský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
na	na	k7c4
Mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
2014	#num#	k4
</s>
<s>
1	#num#	k4
Sergio	Sergio	k1gMnSc1
Germán	Germán	k1gMnSc1
Romero	Romero	k1gNnSc1
•	•	k?
2	#num#	k4
Ezequiel	Ezequiel	k1gInSc1
Garay	Garaa	k1gFnSc2
•	•	k?
3	#num#	k4
Hugo	Hugo	k1gMnSc1
Campagnaro	Campagnara	k1gFnSc5
•	•	k?
4	#num#	k4
Pablo	Pablo	k1gNnSc4
Zabaleta	Zabaleta	k1gFnSc1
•	•	k?
5	#num#	k4
Fernando	Fernanda	k1gFnSc5
Gago	Gago	k1gNnSc4
•	•	k?
6	#num#	k4
Lucas	Lucasa	k1gFnPc2
Biglia	Biglia	k1gFnSc1
•	•	k?
7	#num#	k4
Ángel	Ánglo	k1gNnPc2
Di	Di	k1gFnPc2
María	María	k1gFnSc1
•	•	k?
8	#num#	k4
Enzo	Enzo	k6eAd1
Pérez	Pérez	k1gInSc1
•	•	k?
9	#num#	k4
Gonzalo	Gonzalo	k1gFnSc2
Higuaín	Higuaína	k1gFnPc2
•	•	k?
10	#num#	k4
Lionel	Lionel	k1gInSc1
Messi	Messe	k1gFnSc3
(	(	kIx(
<g/>
C	C	kA
<g/>
)	)	kIx)
•	•	k?
11	#num#	k4
Maxi	maxi	k2eAgInSc4d1
Rodríguez	Rodríguez	k1gInSc4
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
12	#num#	k4
Agustín	Agustína	k1gFnPc2
Orión	orión	k1gInSc4
•	•	k?
13	#num#	k4
Augusto	Augusta	k1gMnSc5
Fernández	Fernández	k1gInSc4
•	•	k?
14	#num#	k4
Javier	Javier	k1gInSc1
Mascherano	Mascherana	k1gFnSc5
•	•	k?
15	#num#	k4
Martín	Martín	k1gInSc1
Demichelis	Demichelis	k1gInSc4
•	•	k?
16	#num#	k4
Marcos	Marcosa	k1gFnPc2
Rojo	Rojo	k6eAd1
•	•	k?
17	#num#	k4
Federico	Federico	k6eAd1
Fernández	Fernández	k1gInSc1
•	•	k?
18	#num#	k4
Rodrigo	Rodrigo	k6eAd1
Palacio	Palacio	k6eAd1
•	•	k?
19	#num#	k4
Ricardo	Ricardo	k1gNnSc1
Gabriel	Gabriela	k1gFnPc2
Álvarez	Álvareza	k1gFnPc2
•	•	k?
20	#num#	k4
Sergio	Sergio	k6eAd1
Agüero	Agüero	k1gNnSc1
•	•	k?
21	#num#	k4
Mariano	Mariana	k1gFnSc5
Andújar	Andújar	k1gInSc4
•	•	k?
22	#num#	k4
Ezequiel	Ezequiela	k1gFnPc2
Lavezzi	Lavezze	k1gFnSc4
•	•	k?
23	#num#	k4
José	Josá	k1gFnSc6
María	Marí	k2eAgFnSc1d1
Basanta	Basanta	k1gFnSc1
•	•	k?
Trenér	trenér	k1gMnSc1
Alejandro	Alejandra	k1gFnSc5
Sabella	Sabello	k1gNnPc5
</s>
<s>
Nejlepší	dobrý	k2eAgMnPc1d3
střelci	střelec	k1gMnPc1
argentinské	argentinský	k2eAgFnSc2d1
fotbalové	fotbalový	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
</s>
<s>
1891	#num#	k4
<g/>
:	:	kIx,
Archer	Archra	k1gFnPc2
</s>
<s>
1892	#num#	k4
</s>
<s>
1893	#num#	k4
<g/>
:	:	kIx,
Leslie	Leslie	k1gFnSc1
</s>
<s>
1894	#num#	k4
<g/>
:	:	kIx,
Gifford	Gifford	k1gInSc1
</s>
<s>
1895	#num#	k4
<g/>
:	:	kIx,
(	(	kIx(
<g/>
No	no	k9
records	records	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
1896	#num#	k4
<g/>
:	:	kIx,
Allen	Allen	k1gMnSc1
/	/	kIx~
Anderson	Anderson	k1gMnSc1
</s>
<s>
1897	#num#	k4
<g/>
:	:	kIx,
Stirling	Stirling	k1gInSc1
</s>
<s>
1898	#num#	k4
<g/>
:	:	kIx,
Allen	Allen	k1gMnSc1
</s>
<s>
1899	#num#	k4
<g/>
:	:	kIx,
Hooton	Hooton	k1gInSc1
</s>
<s>
1900	#num#	k4
<g/>
:	:	kIx,
Leonard	Leonard	k1gMnSc1
</s>
<s>
1901	#num#	k4
<g/>
:	:	kIx,
Dorning	Dorning	k1gInSc1
</s>
<s>
1902	#num#	k4
<g/>
:	:	kIx,
J.	J.	kA
Brown	Brown	k1gMnSc1
</s>
<s>
1903	#num#	k4
<g/>
:	:	kIx,
J.	J.	kA
Brown	Brown	k1gMnSc1
</s>
<s>
1904	#num#	k4
<g/>
:	:	kIx,
A.	A.	kA
Brown	Brown	k1gMnSc1
</s>
<s>
1905	#num#	k4
<g/>
:	:	kIx,
Gonzáles	Gonzáles	k1gMnSc1
/	/	kIx~
Lett	Lett	k1gMnSc1
</s>
<s>
1906	#num#	k4
<g/>
:	:	kIx,
E.	E.	kA
Brown	Brown	k1gMnSc1
/	/	kIx~
Hooton	Hooton	k1gInSc1
/	/	kIx~
Lawrie	Lawrie	k1gFnPc1
/	/	kIx~
Whaley	Whalea	k1gFnPc1
</s>
<s>
1907	#num#	k4
<g/>
:	:	kIx,
E.	E.	kA
Brown	Brown	k1gMnSc1
</s>
<s>
1908	#num#	k4
<g/>
:	:	kIx,
E.	E.	kA
Brown	Brown	k1gMnSc1
</s>
<s>
1909	#num#	k4
<g/>
:	:	kIx,
E.	E.	kA
Brown	Brown	k1gMnSc1
</s>
<s>
1910	#num#	k4
<g/>
:	:	kIx,
A.	A.	kA
W.	W.	kA
Hutton	Hutton	k1gInSc4
</s>
<s>
1911	#num#	k4
<g/>
:	:	kIx,
Malbrán	Malbrán	k2eAgMnSc1d1
/	/	kIx~
E.	E.	kA
Lett	Lett	k1gMnSc1
/	/	kIx~
Piaggio	Piaggio	k1gMnSc1
</s>
<s>
1912	#num#	k4
<g/>
:	:	kIx,
Ohaco	Ohaco	k6eAd1
</s>
<s>
1912	#num#	k4
FAF	FAF	kA
<g/>
:	:	kIx,
Colla	Colla	k1gMnSc1
</s>
<s>
1913	#num#	k4
<g/>
:	:	kIx,
Ohaco	Ohaco	k6eAd1
</s>
<s>
1913	#num#	k4
FAF	FAF	kA
<g/>
:	:	kIx,
Dannaher	Dannahra	k1gFnPc2
</s>
<s>
1914	#num#	k4
<g/>
:	:	kIx,
Ohaco	Ohaco	k6eAd1
</s>
<s>
1914	#num#	k4
FAF	FAF	kA
<g/>
:	:	kIx,
Carabelli	Carabell	k1gMnSc3
</s>
<s>
1915	#num#	k4
<g/>
:	:	kIx,
Ohaco	Ohaco	k6eAd1
</s>
<s>
1916	#num#	k4
<g/>
:	:	kIx,
Hiller	Hiller	k1gInSc1
</s>
<s>
1917	#num#	k4
<g/>
:	:	kIx,
Marcovecchio	Marcovecchio	k6eAd1
</s>
<s>
1918	#num#	k4
<g/>
:	:	kIx,
Zabaleta	Zabaleta	k1gFnSc1
</s>
<s>
1919	#num#	k4
<g/>
:	:	kIx,
Garasini	Garasin	k2eAgMnPc1d1
/	/	kIx~
Martín	Martín	k1gMnSc1
</s>
<s>
1919	#num#	k4
AAmF	AAmF	k1gMnSc1
<g/>
:	:	kIx,
Marcovecchio	Marcovecchio	k1gMnSc1
</s>
<s>
1920	#num#	k4
<g/>
:	:	kIx,
F.	F.	kA
Lucarelli	Lucarelle	k1gFnSc4
</s>
<s>
1920	#num#	k4
AAmF	AAmF	k1gMnSc1
<g/>
:	:	kIx,
Carreras	Carreras	k1gMnSc1
</s>
<s>
1921	#num#	k4
<g/>
:	:	kIx,
Dannaher	Dannahra	k1gFnPc2
</s>
<s>
1921	#num#	k4
AAmF	AAmF	k1gFnSc1
<g/>
:	:	kIx,
Zabaleta	Zabaleta	k1gFnSc1
</s>
<s>
1922	#num#	k4
<g/>
:	:	kIx,
Clarke	Clark	k1gInSc2
/	/	kIx~
Tarasconi	Tarascon	k1gMnPc1
</s>
<s>
1922	#num#	k4
AAmF	AAmF	k1gFnSc1
<g/>
:	:	kIx,
Seoane	Seoan	k1gMnSc5
</s>
<s>
1923	#num#	k4
<g/>
:	:	kIx,
Tarasconi	Tarascoň	k1gFnSc6
</s>
<s>
1923	#num#	k4
AAmF	AAmF	k1gFnSc1
<g/>
:	:	kIx,
Barceló	Barceló	k1gFnSc1
</s>
<s>
1924	#num#	k4
<g/>
:	:	kIx,
Tarasconi	Tarascoň	k1gFnSc6
</s>
<s>
1924	#num#	k4
AAmF	AAmF	k1gFnSc1
<g/>
:	:	kIx,
R.	R.	kA
Lucarelli	Lucarelle	k1gFnSc6
/	/	kIx~
Ravaschino	Ravaschino	k1gNnSc1
</s>
<s>
1925	#num#	k4
<g/>
:	:	kIx,
Gaslini	Gaslin	k2eAgMnPc1d1
</s>
<s>
1925	#num#	k4
AAmF	AAmF	k1gFnSc1
<g/>
:	:	kIx,
Bellomo	Belloma	k1gFnSc5
</s>
<s>
1926	#num#	k4
<g/>
:	:	kIx,
Cherro	Cherro	k1gNnSc1
</s>
<s>
1926	#num#	k4
AAmF	AAmF	k1gFnSc1
<g/>
:	:	kIx,
Seoane	Seoan	k1gInSc5
</s>
<s>
1927	#num#	k4
<g/>
:	:	kIx,
Tarasconi	Tarascoň	k1gFnSc6
</s>
<s>
1928	#num#	k4
<g/>
:	:	kIx,
Cherro	Cherro	k1gNnSc1
</s>
<s>
1929	#num#	k4
<g/>
:	:	kIx,
Giúdice	Giúdice	k1gFnSc1
</s>
<s>
1930	#num#	k4
<g/>
:	:	kIx,
Cherro	Cherro	k1gNnSc1
</s>
<s>
1931	#num#	k4
<g/>
:	:	kIx,
Zozaya	Zozay	k1gInSc2
</s>
<s>
1932	#num#	k4
<g/>
:	:	kIx,
B.	B.	kA
Ferreyra	Ferreyra	k1gMnSc1
</s>
<s>
1933	#num#	k4
<g/>
:	:	kIx,
Varallo	Varallo	k1gNnSc1
</s>
<s>
1934	#num#	k4
<g/>
:	:	kIx,
Barrera	Barrera	k1gFnSc1
</s>
<s>
1935	#num#	k4
<g/>
:	:	kIx,
Cosso	Cossa	k1gFnSc5
</s>
<s>
1936	#num#	k4
<g/>
:	:	kIx,
Barrera	Barrera	k1gFnSc1
</s>
<s>
1937	#num#	k4
<g/>
:	:	kIx,
Erico	Erico	k6eAd1
</s>
<s>
1938	#num#	k4
<g/>
:	:	kIx,
Erico	Erico	k6eAd1
</s>
<s>
1939	#num#	k4
<g/>
:	:	kIx,
Erico	Erico	k6eAd1
</s>
<s>
1940	#num#	k4
<g/>
:	:	kIx,
Benítez	Benítez	k1gMnSc1
Cáceres	Cáceres	k1gMnSc1
/	/	kIx~
Lángara	Lángara	k1gFnSc1
</s>
<s>
1941	#num#	k4
<g/>
:	:	kIx,
Canteli	Cantel	k1gInSc3
</s>
<s>
1942	#num#	k4
<g/>
:	:	kIx,
Martino	Martin	k2eAgNnSc1d1
</s>
<s>
1943	#num#	k4
<g/>
:	:	kIx,
Arrieta	Arrieta	k1gFnSc1
/	/	kIx~
Labruna	Labruna	k1gFnSc1
/	/	kIx~
Frutos	Frutos	k1gInSc1
</s>
<s>
1944	#num#	k4
<g/>
:	:	kIx,
Mellone	Mellon	k1gInSc5
</s>
<s>
1945	#num#	k4
<g/>
:	:	kIx,
Labruna	Labruna	k1gFnSc1
</s>
<s>
1946	#num#	k4
<g/>
:	:	kIx,
Boyé	Boyý	k2eAgFnSc2d1
</s>
<s>
1947	#num#	k4
<g/>
:	:	kIx,
Di	Di	k1gFnSc1
Stéfano	Stéfana	k1gFnSc5
</s>
<s>
1948	#num#	k4
<g/>
:	:	kIx,
Santos	Santos	k1gInSc1
</s>
<s>
1949	#num#	k4
<g/>
:	:	kIx,
Simes	Simes	k1gInSc1
/	/	kIx~
Pizzuti	Pizzuť	k1gFnPc1
</s>
<s>
1950	#num#	k4
<g/>
:	:	kIx,
Papa	papa	k1gMnSc1
</s>
<s>
1951	#num#	k4
<g/>
:	:	kIx,
Vernazza	Vernazza	k1gFnSc1
</s>
<s>
1952	#num#	k4
<g/>
:	:	kIx,
Ricagni	Ricageň	k1gFnSc6
</s>
<s>
1953	#num#	k4
<g/>
:	:	kIx,
Pizzuti	Pizzuť	k1gFnSc2
/	/	kIx~
Benavídez	Benavídez	k1gInSc1
</s>
<s>
1954	#num#	k4
<g/>
:	:	kIx,
Berni	Berni	k?
/	/	kIx~
Conde	Cond	k1gInSc5
/	/	kIx~
Borello	Borello	k1gNnSc4
</s>
<s>
1955	#num#	k4
<g/>
:	:	kIx,
Massei	Masse	k1gFnSc2
</s>
<s>
1956	#num#	k4
<g/>
:	:	kIx,
Castro	Castro	k1gNnSc1
/	/	kIx~
Grillo	Grillo	k1gNnSc1
</s>
<s>
1957	#num#	k4
<g/>
:	:	kIx,
Rob	roba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zárate	Zárat	k1gInSc5
</s>
<s>
1958	#num#	k4
<g/>
:	:	kIx,
Sanfilippo	Sanfilippa	k1gFnSc5
</s>
<s>
1959	#num#	k4
<g/>
:	:	kIx,
Sanfilippo	Sanfilippa	k1gFnSc5
</s>
<s>
1960	#num#	k4
<g/>
:	:	kIx,
Sanfilippo	Sanfilippa	k1gFnSc5
</s>
<s>
1961	#num#	k4
<g/>
:	:	kIx,
Sanfilippo	Sanfilippa	k1gFnSc5
</s>
<s>
1962	#num#	k4
<g/>
:	:	kIx,
Artime	Artim	k1gInSc5
</s>
<s>
1963	#num#	k4
<g/>
:	:	kIx,
Artime	Artim	k1gInSc5
</s>
<s>
1964	#num#	k4
<g/>
:	:	kIx,
Veira	Veir	k1gInSc2
</s>
<s>
1965	#num#	k4
<g/>
:	:	kIx,
Carone	Caron	k1gInSc5
</s>
<s>
1966	#num#	k4
<g/>
:	:	kIx,
Artime	Artim	k1gInSc5
</s>
<s>
Met	met	k1gInSc1
1967	#num#	k4
<g/>
:	:	kIx,
B.	B.	kA
Acosta	Acosta	k1gMnSc1
</s>
<s>
Nac	Nac	k?
1967	#num#	k4
<g/>
:	:	kIx,
Artime	Artim	k1gInSc5
</s>
<s>
Met	met	k1gInSc1
1968	#num#	k4
<g/>
:	:	kIx,
Obberti	Obbert	k1gMnPc1
</s>
<s>
Nac	Nac	k?
1968	#num#	k4
<g/>
:	:	kIx,
Wehbe	Wehb	k1gInSc5
</s>
<s>
Met	met	k1gInSc1
1969	#num#	k4
<g/>
:	:	kIx,
Machado	Machada	k1gFnSc5
</s>
<s>
Nac	Nac	k?
1969	#num#	k4
<g/>
:	:	kIx,
Fischer	Fischer	k1gMnSc1
/	/	kIx~
Bulla	bulla	k1gFnSc1
</s>
<s>
Met	met	k1gInSc1
1970	#num#	k4
<g/>
:	:	kIx,
Más	Más	k1gFnSc1
</s>
<s>
Nac	Nac	k?
1970	#num#	k4
<g/>
:	:	kIx,
Bianchi	Bianch	k1gFnSc2
</s>
<s>
Met	met	k1gInSc1
1971	#num#	k4
<g/>
:	:	kIx,
Bianchi	Bianch	k1gFnSc2
</s>
<s>
Nac	Nac	k?
1971	#num#	k4
<g/>
:	:	kIx,
Obberti	Obbert	k1gMnPc1
/	/	kIx~
Luñ	Luñ	k1gMnSc1
</s>
<s>
Met	met	k1gInSc1
1972	#num#	k4
<g/>
:	:	kIx,
Brindisi	Brindisi	k1gNnSc2
</s>
<s>
Nac	Nac	k?
1972	#num#	k4
<g/>
:	:	kIx,
Morete	More	k1gNnSc2
</s>
<s>
Met	met	k1gInSc1
1973	#num#	k4
<g/>
:	:	kIx,
Más	Más	k1gMnPc1
/	/	kIx~
Curioni	Curion	k1gMnPc1
/	/	kIx~
Peñ	Peñ	k1gMnSc1
</s>
<s>
Nac	Nac	k?
1973	#num#	k4
<g/>
:	:	kIx,
Voglino	Voglin	k2eAgNnSc1d1
</s>
<s>
Met	met	k1gInSc1
1974	#num#	k4
<g/>
:	:	kIx,
Morete	More	k1gNnSc2
</s>
<s>
Nac	Nac	k?
1974	#num#	k4
<g/>
:	:	kIx,
Kempes	Kempes	k1gInSc1
</s>
<s>
Met	met	k1gInSc1
1975	#num#	k4
<g/>
:	:	kIx,
Scotta	Scott	k1gInSc2
</s>
<s>
Nac	Nac	k?
1975	#num#	k4
<g/>
:	:	kIx,
Scotta	Scott	k1gInSc2
</s>
<s>
Met	met	k1gInSc1
1976	#num#	k4
<g/>
:	:	kIx,
Kempes	Kempes	k1gInSc1
</s>
<s>
Nac	Nac	k?
1976	#num#	k4
<g/>
:	:	kIx,
Eresuma	Eresuma	k1gFnSc1
/	/	kIx~
Ludueñ	Ludueñ	k1gFnSc1
/	/	kIx~
Marchetti	Marchetti	k1gNnSc1
</s>
<s>
Met	met	k1gInSc1
1977	#num#	k4
<g/>
:	:	kIx,
Álvarez	Álvarez	k1gInSc1
</s>
<s>
Nac	Nac	k?
1977	#num#	k4
<g/>
:	:	kIx,
Letanu	Letan	k1gInSc2
</s>
<s>
Met	met	k1gInSc1
1978	#num#	k4
<g/>
:	:	kIx,
Maradona	Maradona	k1gFnSc1
/	/	kIx~
Andreuchi	Andreuchi	k1gNnSc1
</s>
<s>
Nac	Nac	k?
1978	#num#	k4
<g/>
:	:	kIx,
Reinaldi	Reinald	k1gMnPc1
</s>
<s>
Met	met	k1gInSc1
1979	#num#	k4
<g/>
:	:	kIx,
Maradona	Maradona	k1gFnSc1
/	/	kIx~
Fortunato	Fortunat	k2eAgNnSc1d1
</s>
<s>
Nac	Nac	k?
1979	#num#	k4
<g/>
:	:	kIx,
Maradona	Maradona	k1gFnSc1
</s>
<s>
Met	met	k1gInSc1
1980	#num#	k4
<g/>
:	:	kIx,
Maradona	Maradona	k1gFnSc1
</s>
<s>
Nac	Nac	k?
1980	#num#	k4
<g/>
:	:	kIx,
Maradona	Maradona	k1gFnSc1
</s>
<s>
Met	met	k1gInSc1
1981	#num#	k4
<g/>
:	:	kIx,
Chaparro	Chaparro	k1gNnSc1
</s>
<s>
Nac	Nac	k?
1981	#num#	k4
<g/>
:	:	kIx,
Bianchi	Bianch	k1gFnSc2
</s>
<s>
Nac	Nac	k?
1982	#num#	k4
<g/>
:	:	kIx,
Juárez	Juárez	k1gInSc1
</s>
<s>
Met	met	k1gInSc1
1982	#num#	k4
<g/>
:	:	kIx,
Morete	More	k1gNnSc2
</s>
<s>
Nac	Nac	k?
1983	#num#	k4
<g/>
:	:	kIx,
Husillos	Husillos	k1gInSc1
</s>
<s>
Met	met	k1gInSc1
1983	#num#	k4
<g/>
:	:	kIx,
Ramos	Ramos	k1gInSc1
</s>
<s>
Nac	Nac	k?
1984	#num#	k4
<g/>
:	:	kIx,
Pasculli	Pasculle	k1gFnSc6
</s>
<s>
Met	met	k1gInSc1
1984	#num#	k4
<g/>
:	:	kIx,
Francescoli	Francescole	k1gFnSc6
</s>
<s>
Nac	Nac	k?
1985	#num#	k4
<g/>
:	:	kIx,
Comas	Comas	k1gInSc1
</s>
<s>
1985	#num#	k4
<g/>
–	–	k?
<g/>
86	#num#	k4
<g/>
:	:	kIx,
Francescoli	Francescole	k1gFnSc6
</s>
<s>
1986	#num#	k4
<g/>
–	–	k?
<g/>
87	#num#	k4
<g/>
:	:	kIx,
Palma	palma	k1gFnSc1
</s>
<s>
1987	#num#	k4
<g/>
–	–	k?
<g/>
88	#num#	k4
<g/>
:	:	kIx,
J.	J.	kA
L.	L.	kA
Rodríguez	Rodríguez	k1gMnSc1
</s>
<s>
1988	#num#	k4
<g/>
–	–	k?
<g/>
89	#num#	k4
<g/>
:	:	kIx,
Dertycia	Dertycium	k1gNnSc2
/	/	kIx~
Gorosito	Gorosita	k1gFnSc5
</s>
<s>
1989	#num#	k4
<g/>
–	–	k?
<g/>
90	#num#	k4
<g/>
:	:	kIx,
Cozzoni	Cozzoň	k1gFnSc6
</s>
<s>
1990	#num#	k4
<g/>
–	–	k?
<g/>
91	#num#	k4
<g/>
:	:	kIx,
González	González	k1gMnSc1
</s>
<s>
Ap	ap	kA
1991	#num#	k4
<g/>
:	:	kIx,
Díaz	Díaz	k1gInSc1
</s>
<s>
Cl	Cl	k?
1992	#num#	k4
<g/>
:	:	kIx,
Scotto	Scotto	k1gNnSc1
/	/	kIx~
Latorre	Latorr	k1gMnSc5
</s>
<s>
Ap	ap	kA
1992	#num#	k4
<g/>
:	:	kIx,
A.	A.	kA
Acosta	Acosta	k1gMnSc1
</s>
<s>
Cl	Cl	k?
1993	#num#	k4
<g/>
:	:	kIx,
da	da	k?
Silva	Silva	k1gFnSc1
</s>
<s>
Ap	ap	kA
1993	#num#	k4
<g/>
:	:	kIx,
Martínez	Martínez	k1gInSc1
</s>
<s>
Cl	Cl	k?
1994	#num#	k4
<g/>
:	:	kIx,
Espina	Espino	k1gNnSc2
/	/	kIx~
Crespo	Crespa	k1gFnSc5
</s>
<s>
Ap	ap	kA
1994	#num#	k4
<g/>
:	:	kIx,
Francescoli	Francescole	k1gFnSc6
</s>
<s>
Cl	Cl	k?
1995	#num#	k4
<g/>
:	:	kIx,
Flores	Flores	k1gInSc1
</s>
<s>
Ap	ap	kA
1995	#num#	k4
<g/>
:	:	kIx,
Calderón	Calderón	k1gInSc1
</s>
<s>
Cl	Cl	k?
1996	#num#	k4
<g/>
:	:	kIx,
A.	A.	kA
López	López	k1gMnSc1
</s>
<s>
Ap	ap	kA
1996	#num#	k4
<g/>
:	:	kIx,
Reggi	Regg	k1gFnSc2
</s>
<s>
Cl	Cl	k?
1997	#num#	k4
<g/>
:	:	kIx,
Martínez	Martínez	k1gInSc1
</s>
<s>
Ap	ap	kA
1997	#num#	k4
<g/>
:	:	kIx,
da	da	k?
Silva	Silva	k1gFnSc1
</s>
<s>
Cl	Cl	k?
1998	#num#	k4
<g/>
:	:	kIx,
Sosa	Sos	k1gInSc2
</s>
<s>
Ap	ap	kA
1998	#num#	k4
<g/>
:	:	kIx,
Palermo	Palermo	k1gNnSc1
</s>
<s>
Cl	Cl	k?
1999	#num#	k4
<g/>
:	:	kIx,
Calderón	Calderón	k1gInSc1
</s>
<s>
Ap	ap	kA
1999	#num#	k4
<g/>
:	:	kIx,
Saviola	Saviola	k1gFnSc1
</s>
<s>
Cl	Cl	k?
2000	#num#	k4
<g/>
:	:	kIx,
Fuertes	Fuertes	k1gInSc1
</s>
<s>
Ap	ap	kA
2000	#num#	k4
<g/>
:	:	kIx,
Ángel	Ángel	k1gInSc1
</s>
<s>
Cl	Cl	k?
2001	#num#	k4
<g/>
:	:	kIx,
Romeo	Romeo	k1gMnSc1
</s>
<s>
Ap	ap	kA
2001	#num#	k4
<g/>
:	:	kIx,
Cardetti	Cardetť	k1gFnSc2
</s>
<s>
Cl	Cl	k?
2002	#num#	k4
<g/>
:	:	kIx,
Cavenaghi	Cavenagh	k1gFnSc2
</s>
<s>
Ap	ap	kA
2002	#num#	k4
<g/>
:	:	kIx,
Silvera	Silvera	k1gFnSc1
</s>
<s>
Cl	Cl	k?
2003	#num#	k4
<g/>
:	:	kIx,
Figueroa	Figuero	k1gInSc2
</s>
<s>
Ap	ap	kA
2003	#num#	k4
<g/>
:	:	kIx,
Farías	Farías	k1gInSc1
</s>
<s>
Cl	Cl	k?
2004	#num#	k4
<g/>
:	:	kIx,
Rolando	Rolanda	k1gFnSc5
Zárate	Zárat	k1gMnSc5
</s>
<s>
Ap	ap	kA
2004	#num#	k4
<g/>
:	:	kIx,
L.	L.	kA
López	López	k1gMnSc1
</s>
<s>
Cl	Cl	k?
2005	#num#	k4
<g/>
:	:	kIx,
Pavone	Pavon	k1gMnSc5
</s>
<s>
Ap	ap	kA
2005	#num#	k4
<g/>
:	:	kIx,
Cámpora	Cámpora	k1gFnSc1
</s>
<s>
Cl	Cl	k?
2006	#num#	k4
<g/>
:	:	kIx,
Vargas	Vargas	k1gInSc1
</s>
<s>
Ap	ap	kA
2006	#num#	k4
<g/>
:	:	kIx,
M.	M.	kA
Zárate	Zárat	k1gInSc5
/	/	kIx~
Palacio	Palacio	k1gMnSc1
</s>
<s>
Cl	Cl	k?
2007	#num#	k4
<g/>
:	:	kIx,
Palermo	Palermo	k1gNnSc1
</s>
<s>
Ap	ap	kA
2007	#num#	k4
<g/>
:	:	kIx,
Denis	Denisa	k1gFnPc2
</s>
<s>
Cl	Cl	k?
2008	#num#	k4
<g/>
:	:	kIx,
Cvitanich	Cvitanich	k1gInSc1
</s>
<s>
Ap	ap	kA
2008	#num#	k4
<g/>
:	:	kIx,
Sand	Sand	k1gInSc1
</s>
<s>
Cl	Cl	k?
2009	#num#	k4
<g/>
:	:	kIx,
Sand	Sand	k1gInSc1
</s>
<s>
Ap	ap	kA
2009	#num#	k4
<g/>
:	:	kIx,
Silva	Silva	k1gFnSc1
</s>
<s>
Cl	Cl	k?
2010	#num#	k4
<g/>
:	:	kIx,
Boselli	Boselle	k1gFnSc6
</s>
<s>
Ap	ap	kA
2010	#num#	k4
<g/>
:	:	kIx,
Stracqualursi	Stracqualurse	k1gFnSc6
/	/	kIx~
Silva	Silva	k1gFnSc1
</s>
<s>
Cl	Cl	k?
2011	#num#	k4
<g/>
:	:	kIx,
Cámpora	Cámpora	k1gFnSc1
/	/	kIx~
Gutiérrez	Gutiérrez	k1gInSc1
</s>
<s>
Ap	ap	kA
2011	#num#	k4
<g/>
:	:	kIx,
Ramírez	Ramírez	k1gInSc1
</s>
<s>
Cl	Cl	k?
2012	#num#	k4
<g/>
:	:	kIx,
Luna	luna	k1gFnSc1
</s>
<s>
In	In	k?
2012	#num#	k4
<g/>
:	:	kIx,
F.	F.	kA
Ferreyra	Ferreyra	k1gMnSc1
/	/	kIx~
Scocco	Scocco	k1gMnSc1
</s>
<s>
Fi	fi	k0
2013	#num#	k4
<g/>
:	:	kIx,
Gigliotti	Gigliotti	k1gNnSc2
/	/	kIx~
Scocco	Scocco	k6eAd1
</s>
<s>
In	In	k?
2013	#num#	k4
<g/>
:	:	kIx,
Pereyra	Pereyr	k1gInSc2
</s>
<s>
Fi	fi	k0
2014	#num#	k4
<g/>
:	:	kIx,
M.	M.	kA
Zárate	Zárat	k1gMnSc5
</s>
<s>
Tr	Tr	k?
2014	#num#	k4
<g/>
:	:	kIx,
Pratto	Pratto	k1gNnSc1
/	/	kIx~
M.	M.	kA
Rodríguez	Rodríguez	k1gInSc1
/	/	kIx~
Romero	Romero	k1gNnSc1
</s>
<s>
2015	#num#	k4
<g/>
:	:	kIx,
Ruben	ruben	k2eAgInSc1d1
</s>
<s>
2016	#num#	k4
<g/>
:	:	kIx,
Sand	Sand	k1gInSc1
</s>
<s>
2016	#num#	k4
<g/>
–	–	k?
<g/>
17	#num#	k4
<g/>
:	:	kIx,
Benedetto	Benedetto	k1gNnSc1
</s>
<s>
2017	#num#	k4
<g/>
–	–	k?
<g/>
18	#num#	k4
<g/>
:	:	kIx,
García	Garcí	k1gInSc2
</s>
<s>
2018	#num#	k4
<g/>
–	–	k?
<g/>
19	#num#	k4
<g/>
:	:	kIx,
L.	L.	kA
López	López	k1gMnSc1
</s>
<s>
2020	#num#	k4
<g/>
:	:	kIx,
Borré	Borrý	k2eAgNnSc1d1
/	/	kIx~
Romero	Romero	k1gNnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
