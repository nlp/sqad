<s>
Začít	začít	k5eAaPmF	začít
pilotovat	pilotovat	k5eAaImF	pilotovat
motorové	motorový	k2eAgNnSc4d1	motorové
letadlo	letadlo	k1gNnSc4	letadlo
lze	lze	k6eAd1	lze
od	od	k7c2	od
15	[number]	k4	15
let	léto	k1gNnPc2	léto
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
praktického	praktický	k2eAgInSc2d1	praktický
výcviku	výcvik	k1gInSc2	výcvik
pilotního	pilotní	k2eAgMnSc2d1	pilotní
žáka	žák	k1gMnSc2	žák
pro	pro	k7c4	pro
získání	získání	k1gNnSc4	získání
pilotního	pilotní	k2eAgInSc2d1	pilotní
průkazu	průkaz	k1gInSc2	průkaz
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
do	do	k7c2	do
18	[number]	k4	18
let	léto	k1gNnPc2	léto
je	být	k5eAaImIp3nS	být
zapotřebí	zapotřebí	k6eAd1	zapotřebí
písemný	písemný	k2eAgInSc4d1	písemný
souhlas	souhlas	k1gInSc4	souhlas
zákonných	zákonný	k2eAgMnPc2d1	zákonný
zástupců	zástupce	k1gMnPc2	zástupce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
