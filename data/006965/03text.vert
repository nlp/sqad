<s>
Smrt	smrt	k1gFnSc1	smrt
stopařek	stopařka	k1gFnPc2	stopařka
je	být	k5eAaImIp3nS	být
český	český	k2eAgInSc1d1	český
kriminální	kriminální	k2eAgInSc1d1	kriminální
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Poláka	Polák	k1gMnSc2	Polák
natočený	natočený	k2eAgInSc4d1	natočený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
podle	podle	k7c2	podle
skutečné	skutečný	k2eAgFnSc2d1	skutečná
události	událost	k1gFnSc2	událost
<g/>
.	.	kIx.	.
</s>
<s>
Řidič	řidič	k1gMnSc1	řidič
náklaďáku	náklaďák	k1gInSc2	náklaďák
(	(	kIx(	(
<g/>
Marek	Marek	k1gMnSc1	Marek
Perepeczko	Perepeczka	k1gFnSc5	Perepeczka
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
agresivní	agresivní	k2eAgInPc4d1	agresivní
sklony	sklon	k1gInPc4	sklon
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jednou	jeden	k4xCgFnSc7	jeden
vezme	vzít	k5eAaPmIp3nS	vzít
dvě	dva	k4xCgFnPc4	dva
mladé	mladý	k2eAgFnPc4d1	mladá
stopařky	stopařka	k1gFnPc4	stopařka
(	(	kIx(	(
<g/>
Jana	Jan	k1gMnSc2	Jan
Nagyová	Nagyová	k1gFnSc1	Nagyová
a	a	k8xC	a
Dagmar	Dagmar	k1gFnSc1	Dagmar
Patrasová	Patrasová	k1gFnSc1	Patrasová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zaveze	zavézt	k5eAaPmIp3nS	zavézt
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
lesa	les	k1gInSc2	les
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
je	on	k3xPp3gMnPc4	on
snaží	snažit	k5eAaImIp3nP	snažit
znásilnit	znásilnit	k5eAaPmF	znásilnit
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
pokusu	pokus	k1gInSc2	pokus
však	však	k9	však
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
nechtěně	chtěně	k6eNd1	chtěně
zabije	zabít	k5eAaPmIp3nS	zabít
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nucen	nutit	k5eAaImNgMnS	nutit
odstranit	odstranit	k5eAaPmF	odstranit
i	i	k9	i
druhou	druhý	k4xOgFnSc7	druhý
jako	jako	k8xS	jako
nežádoucího	žádoucí	k2eNgMnSc4d1	nežádoucí
svědka	svědek	k1gMnSc4	svědek
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
stopy	stopa	k1gFnPc1	stopa
po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
hrůzném	hrůzný	k2eAgInSc6d1	hrůzný
činu	čin	k1gInSc6	čin
s	s	k7c7	s
chybami	chyba	k1gFnPc7	chyba
zamaskuje	zamaskovat	k5eAaPmIp3nS	zamaskovat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
vyšetřovatelé	vyšetřovatel	k1gMnPc1	vyšetřovatel
případu	případ	k1gInSc6	případ
úspěšní	úspěšný	k2eAgMnPc1d1	úspěšný
a	a	k8xC	a
po	po	k7c6	po
krátkém	krátký	k2eAgNnSc6d1	krátké
pátrání	pátrání	k1gNnSc6	pátrání
jej	on	k3xPp3gNnSc4	on
s	s	k7c7	s
přispěním	přispění	k1gNnSc7	přispění
okolí	okolí	k1gNnSc2	okolí
odhalí	odhalit	k5eAaPmIp3nS	odhalit
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
verzi	verze	k1gFnSc6	verze
uvedené	uvedený	k2eAgFnSc6d1	uvedená
televizí	televize	k1gFnSc7	televize
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
chybí	chybit	k5eAaPmIp3nS	chybit
scéna	scéna	k1gFnSc1	scéna
s	s	k7c7	s
odhaleným	odhalený	k2eAgInSc7d1	odhalený
prsem	prs	k1gInSc7	prs
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ve	v	k7c6	v
verzi	verze	k1gFnSc6	verze
filmu	film	k1gInSc2	film
vysílané	vysílaný	k2eAgFnPc4d1	vysílaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
byla	být	k5eAaImAgFnS	být
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Smrt	smrt	k1gFnSc1	smrt
stopařek	stopařka	k1gFnPc2	stopařka
–	–	k?	–
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Cfn	Cfn	k1gFnSc2	Cfn
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Vrah	vrah	k1gMnSc1	vrah
stopárok	stopárok	k1gInSc1	stopárok
(	(	kIx(	(
<g/>
Najväčšie	Najväčšie	k1gFnSc1	Najväčšie
kriminálne	kriminálnout	k5eAaPmIp3nS	kriminálnout
prípady	prípada	k1gFnPc4	prípada
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
)	)	kIx)	)
Smrt	smrt	k1gFnSc1	smrt
stopařek	stopařka	k1gFnPc2	stopařka
-	-	kIx~	-
filmová	filmový	k2eAgNnPc1d1	filmové
místa	místo	k1gNnPc1	místo
na	na	k7c4	na
Filmová	filmový	k2eAgNnPc4d1	filmové
místa	místo	k1gNnPc4	místo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
