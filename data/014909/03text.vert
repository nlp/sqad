<s>
Nintendo	Nintendo	k6eAd1
Entertainment	Entertainment	k1gInSc1
System	Syst	k1gMnSc7
</s>
<s>
Nintendo	Nintendo	k6eAd1
Entertainment	Entertainment	k1gInSc1
System	Systo	k1gNnSc7
</s>
<s>
Nintendo	Nintendo	k6eAd1
Entertainment	Entertainment	k1gInSc1
System	Syst	k1gMnSc7
(	(	kIx(
<g/>
zkráceně	zkráceně	k6eAd1
NES	nést	k5eAaImRp2nS
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
se	s	k7c7
svými	svůj	k3xOyFgInPc7
60	#num#	k4
miliony	milion	k4xCgInPc7
celosvětově	celosvětově	k6eAd1
prodanými	prodaný	k2eAgInPc7d1
kusy	kus	k1gInPc7
nejúspěšnější	úspěšný	k2eAgFnSc1d3
8	#num#	k4
<g/>
bitová	bitový	k2eAgFnSc1d1
herní	herní	k2eAgFnSc1d1
konzole	konzola	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prodávat	prodávat	k5eAaImF
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
1985	#num#	k4
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
Severní	severní	k2eAgFnSc6d1
Americe	Amerika	k1gFnSc6
<g/>
,	,	kIx,
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
Brazílii	Brazílie	k1gFnSc6
a	a	k8xC
Austrálii	Austrálie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vývojově	vývojově	k6eAd1
vycházela	vycházet	k5eAaImAgNnP
z	z	k7c2
herní	herní	k2eAgFnSc2d1
konzole	konzola	k1gFnSc6
Nintendo	Nintendo	k1gNnSc1
Family	Famila	k1gFnSc2
Computer	computer	k1gInSc1
(	(	kIx(
<g/>
zkráceně	zkráceně	k6eAd1
Famicom	Famicom	k1gInSc1
nebo	nebo	k8xC
FC	FC	kA
<g/>
)	)	kIx)
vydané	vydaný	k2eAgInPc4d1
pro	pro	k7c4
japonský	japonský	k2eAgInSc4d1
trh	trh	k1gInSc4
již	již	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1983	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tzv.	tzv.	kA
krachu	krach	k1gInSc2
videoherního	videoherní	k2eAgInSc2d1
průmyslu	průmysl	k1gInSc2
přistupovalo	přistupovat	k5eAaImAgNnS
Nintendo	Nintendo	k6eAd1
k	k	k7c3
americkému	americký	k2eAgInSc3d1
trhu	trh	k1gInSc3
specifickým	specifický	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
a	a	k8xC
to	ten	k3xDgNnSc1
dalo	dát	k5eAaPmAgNnS
vzniknout	vzniknout	k5eAaPmF
nejen	nejen	k6eAd1
odlišnému	odlišný	k2eAgInSc3d1
designu	design	k1gInSc3
konzole	konzola	k1gFnSc3
oproti	oproti	k7c3
japonské	japonský	k2eAgFnSc3d1
verzi	verze	k1gFnSc3
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
odlišnému	odlišný	k2eAgInSc3d1
typu	typ	k1gInSc2
herních	herní	k2eAgFnPc2d1
cartridgí	cartridg	k1gFnPc2
(	(	kIx(
<g/>
herní	herní	k2eAgFnSc1d1
kazeta	kazeta	k1gFnSc1
NESu	nést	k5eAaImIp1nS
je	být	k5eAaImIp3nS
založena	založen	k2eAgFnSc1d1
na	na	k7c6
konektoru	konektor	k1gInSc6
se	s	k7c7
8	#num#	k4
piny	pin	k1gInPc7
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
pro	pro	k7c4
FC	FC	kA
jich	on	k3xPp3gMnPc2
měla	mít	k5eAaImAgFnS
pouze	pouze	k6eAd1
60	#num#	k4
<g/>
)	)	kIx)
či	či	k8xC
zcela	zcela	k6eAd1
novému	nový	k2eAgNnSc3d1
hernímu	herní	k2eAgNnSc3d1
příslušenství	příslušenství	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Herní	herní	k2eAgFnSc6d1
konzole	konzola	k1gFnSc6
Nintendo	Nintendo	k6eAd1
NES	nést	k5eAaImRp2nS
patří	patřit	k5eAaImIp3nS
do	do	k7c2
takzvané	takzvaný	k2eAgFnSc2d1
třetí	třetí	k4xOgFnSc2
generace	generace	k1gFnSc2
herních	herní	k2eAgNnPc2d1
konzolí	konzolí	k1gNnPc2
a	a	k8xC
dodnes	dodnes	k6eAd1
se	se	k3xPyFc4
mezi	mezi	k7c7
hráči	hráč	k1gMnPc7
těší	těšit	k5eAaImIp3nS
velké	velký	k2eAgFnSc3d1
oblibě	obliba	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnPc1d1
významné	významný	k2eAgFnPc1d1
konzole	konzola	k1gFnSc6
třetí	třetí	k4xOgFnSc2
generace	generace	k1gFnSc2
kromě	kromě	k7c2
NESu	nést	k5eAaImIp1nS
jsou	být	k5eAaImIp3nP
Master	master	k1gMnSc1
System	Syst	k1gInSc7
od	od	k7c2
Segy	Sega	k1gFnSc2
a	a	k8xC
Atari	Atar	k1gInSc3
7800	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opravdovým	opravdový	k2eAgMnSc7d1
konkurentem	konkurent	k1gMnSc7
pro	pro	k7c4
ni	on	k3xPp3gFnSc4
byl	být	k5eAaImAgMnS
jen	jen	k9
Master	master	k1gMnSc1
System	Syst	k1gMnSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
ovšem	ovšem	k9
nebyl	být	k5eNaImAgMnS
v	v	k7c6
Americe	Amerika	k1gFnSc6
a	a	k8xC
ani	ani	k8xC
doma	doma	k6eAd1
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
úspěšný	úspěšný	k2eAgMnSc1d1
a	a	k8xC
NESu	nést	k5eAaImIp1nS
konkuroval	konkurovat	k5eAaImAgMnS
jen	jen	k9
na	na	k7c6
evropských	evropský	k2eAgInPc6d1
trzích	trh	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Počátky	počátek	k1gInPc4
</s>
<s>
Původní	původní	k2eAgFnSc1d1
japonská	japonský	k2eAgFnSc1d1
konzole	konzola	k1gFnSc6
Famicom	Famicom	k1gInSc4
</s>
<s>
Po	po	k7c6
úspěších	úspěch	k1gInPc6
s	s	k7c7
konzolí	konzole	k1gFnSc7
pojmenovanou	pojmenovaný	k2eAgFnSc7d1
„	„	k?
<g/>
Color	Colora	k1gFnPc2
TV	TV	kA
Game	game	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
1977	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
zejména	zejména	k9
herními	herní	k2eAgInPc7d1
automaty	automat	k1gInPc7
na	na	k7c6
začátku	začátek	k1gInSc6
80	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
Nintendo	Nintendo	k6eAd1
přišlo	přijít	k5eAaPmAgNnS
s	s	k7c7
nápadem	nápad	k1gInSc7
vyvinout	vyvinout	k5eAaPmF
vlastní	vlastní	k2eAgFnSc4d1
stolní	stolní	k2eAgFnSc4d1
konzoli	konzole	k1gFnSc4
založenou	založený	k2eAgFnSc4d1
na	na	k7c6
cartridgovém	cartridgový	k2eAgInSc6d1
systému	systém	k1gInSc6
distribuce	distribuce	k1gFnSc2
her	hra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgMnSc7d1
designérem	designér	k1gMnSc7
systému	systém	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
se	se	k3xPyFc4
v	v	k7c6
Japonsku	Japonsko	k1gNnSc6
začal	začít	k5eAaPmAgInS
prodávat	prodávat	k5eAaImF
15	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
1983	#num#	k4
je	být	k5eAaImIp3nS
Masayuki	Masayuke	k1gFnSc4
Uemura	Uemur	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cena	cena	k1gFnSc1
byla	být	k5eAaImAgFnS
stanovena	stanovit	k5eAaPmNgFnS
na	na	k7c4
14800	#num#	k4
Yenů	Yen	k1gInPc2
(	(	kIx(
<g/>
cca	cca	kA
100	#num#	k4
dolarů	dolar	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
prvního	první	k4xOgInSc2
roku	rok	k1gInSc2
sklidil	sklidit	k5eAaPmAgMnS
mnoho	mnoho	k6eAd1
kritiky	kritika	k1gFnSc2
za	za	k7c4
svoji	svůj	k3xOyFgFnSc4
nespolehlivost	nespolehlivost	k1gFnSc4
–	–	k?
systém	systém	k1gInSc1
padal	padat	k5eAaImAgInS
a	a	k8xC
zamrzal	zamrzat	k5eAaImAgInS
<g/>
,	,	kIx,
v	v	k7c6
programovém	programový	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
byly	být	k5eAaImAgFnP
chyby	chyba	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
vše	všechen	k3xTgNnSc1
bylo	být	k5eAaImAgNnS
vyřešeno	vyřešit	k5eAaPmNgNnS
novou	nový	k2eAgFnSc7d1
revizí	revize	k1gFnSc7
systému	systém	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
dostala	dostat	k5eAaPmAgFnS
do	do	k7c2
vínku	vínek	k1gInSc2
zejména	zejména	k9
novou	nový	k2eAgFnSc4d1
základní	základní	k2eAgFnSc4d1
desku	deska	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
roku	rok	k1gInSc2
1984	#num#	k4
si	se	k3xPyFc3
už	už	k9
u	u	k7c2
Nintenda	Nintend	k1gMnSc2
mohli	moct	k5eAaImAgMnP
gratulovat	gratulovat	k5eAaImF
–	–	k?
Famicom	Famicom	k1gInSc1
byl	být	k5eAaImAgMnS
nejprodávanější	prodávaný	k2eAgFnSc7d3
japonskou	japonský	k2eAgFnSc7d1
konzolí	konzole	k1gFnSc7
a	a	k8xC
chystal	chystat	k5eAaImAgMnS
se	se	k3xPyFc4
k	k	k7c3
rozhodujícímu	rozhodující	k2eAgInSc3d1
kroku	krok	k1gInSc3
–	–	k?
vstupu	vstup	k1gInSc3
na	na	k7c4
severoamerický	severoamerický	k2eAgInSc4d1
trh	trh	k1gInSc4
jako	jako	k8xC,k8xS
NES	nést	k5eAaImRp2nS
s	s	k7c7
pozměněným	pozměněný	k2eAgInSc7d1
designem	design	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Vstup	vstup	k1gInSc1
na	na	k7c4
americký	americký	k2eAgInSc4d1
trh	trh	k1gInSc4
</s>
<s>
K	k	k7c3
tomu	ten	k3xDgNnSc3
ale	ale	k9
zpočátku	zpočátku	k6eAd1
chyběla	chybět	k5eAaImAgFnS
Nintendu	Nintenda	k1gFnSc4
sebedůvěra	sebedůvěra	k1gFnSc1
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
pokusilo	pokusit	k5eAaPmAgNnS
dojednat	dojednat	k5eAaPmF
smlouvu	smlouva	k1gFnSc4
se	s	k7c7
společností	společnost	k1gFnSc7
Atari	Atar	k1gFnSc2
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
NES	nést	k5eAaImRp2nS
prodával	prodávat	k5eAaImAgInS
pod	pod	k7c7
značkou	značka	k1gFnSc7
Atari	Atari	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celý	celý	k2eAgInSc1d1
kontrakt	kontrakt	k1gInSc1
skončil	skončit	k5eAaPmAgInS
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
Atari	Atari	k1gNnSc1
na	na	k7c6
tom	ten	k3xDgNnSc6
bylo	být	k5eAaImAgNnS
v	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
finančně	finančně	k6eAd1
celkem	celkem	k6eAd1
špatně	špatně	k6eAd1
a	a	k8xC
riziko	riziko	k1gNnSc1
neúspěchu	neúspěch	k1gInSc2
bylo	být	k5eAaImAgNnS
až	až	k9
příliš	příliš	k6eAd1
vysoké	vysoký	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dokonce	dokonce	k9
se	se	k3xPyFc4
uvažovalo	uvažovat	k5eAaImAgNnS
nad	nad	k7c7
stvořením	stvoření	k1gNnSc7
monstra	monstrum	k1gNnSc2
jménem	jméno	k1gNnSc7
„	„	k?
<g/>
Nintendo	Nintendo	k1gNnSc1
Advanced	Advanced	k1gMnSc1
Video	video	k1gNnSc1
System	Syst	k1gMnSc7
<g/>
“	“	k?
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
by	by	kYmCp3nS
měl	mít	k5eAaImAgInS
klávesnici	klávesnice	k1gFnSc4
<g/>
,	,	kIx,
bezdrátové	bezdrátový	k2eAgInPc4d1
joysticky	joysticky	k6eAd1
atd.	atd.	kA
Nakonec	nakonec	k9
Nintendo	Nintendo	k6eAd1
začalo	začít	k5eAaPmAgNnS
prodávat	prodávat	k5eAaImF
samo	sám	k3xTgNnSc1
<g/>
,	,	kIx,
stalo	stát	k5eAaPmAgNnS
se	se	k3xPyFc4
tak	tak	k9
18	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1985	#num#	k4
u	u	k7c2
příležitosti	příležitost	k1gFnSc2
výstavy	výstava	k1gFnSc2
Consumer	Consumer	k1gMnSc1
Electronics	Electronicsa	k1gFnPc2
Show	show	k1gFnPc2
a	a	k8xC
to	ten	k3xDgNnSc1
ještě	ještě	k9
jen	jen	k9
v	v	k7c6
New	New	k1gFnSc6
Yorku	York	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obchodníci	obchodník	k1gMnPc1
byli	být	k5eAaImAgMnP
tak	tak	k6eAd1
pesimističtí	pesimistický	k2eAgMnPc1d1
<g/>
,	,	kIx,
že	že	k8xS
dokonce	dokonce	k9
Nintendo	Nintendo	k6eAd1
muselo	muset	k5eAaImAgNnS
podepsat	podepsat	k5eAaPmF
smlouvy	smlouva	k1gFnPc4
<g/>
,	,	kIx,
že	že	k8xS
si	se	k3xPyFc3
případné	případný	k2eAgInPc4d1
neprodané	prodaný	k2eNgInPc4d1
systémy	systém	k1gInPc4
odkoupí	odkoupit	k5eAaPmIp3nS
zpátky	zpátky	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opak	opak	k1gInSc1
byl	být	k5eAaImAgInS
pravdou	pravda	k1gFnSc7
a	a	k8xC
první	první	k4xOgFnSc1
várka	várka	k1gFnSc1
100	#num#	k4
000	#num#	k4
kusů	kus	k1gInPc2
byla	být	k5eAaImAgFnS
pryč	pryč	k6eAd1
velice	velice	k6eAd1
rychle	rychle	k6eAd1
<g/>
,	,	kIx,
což	což	k3yQnSc4,k3yRnSc4
zajisté	zajisté	k9
podpořila	podpořit	k5eAaPmAgFnS
změna	změna	k1gFnSc1
designu	design	k1gInSc2
pro	pro	k7c4
Americký	americký	k2eAgInSc4d1
trh	trh	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naplno	naplno	k6eAd1
vše	všechen	k3xTgNnSc1
začalo	začít	k5eAaPmAgNnS
až	až	k9
v	v	k7c6
únoru	únor	k1gInSc6
dalšího	další	k2eAgInSc2d1
roku	rok	k1gInSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Nintendo	Nintendo	k6eAd1
začalo	začít	k5eAaPmAgNnS
naplno	naplno	k6eAd1
produkovat	produkovat	k5eAaImF
konzole	konzola	k1gFnSc3
určené	určený	k2eAgFnPc4d1
pro	pro	k7c4
koupěchtivé	koupěchtivý	k2eAgMnPc4d1
zákazníky	zákazník	k1gMnPc4
v	v	k7c6
US	US	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Start	start	k1gInSc1
byl	být	k5eAaImAgInS
podpořen	podpořit	k5eAaPmNgInS
jedenácti	jedenáct	k4xCc7
herními	herní	k2eAgInPc7d1
tituly	titul	k1gInPc7
<g/>
,	,	kIx,
mezi	mezi	k7c7
kterým	který	k3yQgInSc7,k3yIgInSc7,k3yRgInSc7
vyčnívaly	vyčnívat	k5eAaImAgFnP
zejména	zejména	k9
tři	tři	k4xCgFnPc1
hry	hra	k1gFnPc1
–	–	k?
Super	super	k1gInSc1
Mario	Mario	k1gMnSc1
Bros	Bros	k1gInSc1
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Donkey	Donkea	k1gFnPc4
Kong	Kongo	k1gNnPc2
Jr	Jr	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Math	Math	k1gInSc4
a	a	k8xC
Duck	Duck	k1gInSc4
Hunt	hunt	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátce	krátce	k6eAd1
nato	nato	k6eAd1
následoval	následovat	k5eAaImAgInS
start	start	k1gInSc4
evropský	evropský	k2eAgInSc4d1
<g/>
,	,	kIx,
zde	zde	k6eAd1
už	už	k6eAd1
ale	ale	k8xC
nedistribuovalo	distribuovat	k5eNaBmAgNnS
Nintendo	Nintendo	k6eAd1
přímo	přímo	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
spíše	spíše	k9
lokální	lokální	k2eAgMnPc1d1
distributoři	distributor	k1gMnPc1
v	v	k7c6
čele	čelo	k1gNnSc6
s	s	k7c7
hračkářskou	hračkářský	k2eAgFnSc7d1
korporací	korporace	k1gFnSc7
Mattel	Mattela	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1990	#num#	k4
se	se	k3xPyFc4
NES	nést	k5eAaImRp2nS
stal	stát	k5eAaPmAgMnS
nejprodávanější	prodávaný	k2eAgNnSc1d3
konzolí	konzolí	k1gNnSc1
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Konec	konec	k1gInSc1
života	život	k1gInSc2
</s>
<s>
Rok	rok	k1gInSc1
1990	#num#	k4
ale	ale	k8xC
znamenal	znamenat	k5eAaImAgInS
i	i	k9
něco	něco	k6eAd1
jiného	jiný	k2eAgNnSc2d1
–	–	k?
konkurenční	konkurenční	k2eAgFnSc1d1
Sega	Sega	k1gFnSc1
začala	začít	k5eAaPmAgFnS
prorážet	prorážet	k5eAaImF
se	se	k3xPyFc4
svojí	svojit	k5eAaImIp3nS
technologicky	technologicky	k6eAd1
mnohem	mnohem	k6eAd1
vyspělejší	vyspělý	k2eAgFnSc4d2
Segou	Segá	k1gFnSc4
Mega	mega	k1gNnSc2
Drive	drive	k1gInSc1
(	(	kIx(
<g/>
nebo	nebo	k8xC
také	také	k9
SEGA	SEGA	kA
Genesis	Genesis	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yQgFnSc4,k3yIgFnSc4
na	na	k7c4
trh	trh	k1gInSc4
uvedla	uvést	k5eAaPmAgFnS
rok	rok	k1gInSc4
<g/>
,	,	kIx,
potažmo	potažmo	k6eAd1
dva	dva	k4xCgMnPc1
předtím	předtím	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nintendo	Nintendo	k6eAd1
už	už	k6eAd1
sice	sice	k8xC
mělo	mít	k5eAaImAgNnS
hotovou	hotový	k2eAgFnSc4d1
svou	svůj	k3xOyFgFnSc4
vlastní	vlastnit	k5eAaImIp3nP
16	#num#	k4
<g/>
bit	bit	k1gInSc1
konzoli	konzoli	k6eAd1
Super	super	k2eAgNnSc4d1
Nintendo	Nintendo	k1gNnSc4
Entertainment	Entertainment	k1gMnSc1
System	Syst	k1gInSc7
(	(	kIx(
<g/>
SNES	snést	k5eAaPmRp2nS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ale	ale	k8xC
ta	ten	k3xDgFnSc1
zatím	zatím	k6eAd1
nebyla	být	k5eNaImAgFnS
v	v	k7c6
dostatečném	dostatečný	k2eAgNnSc6d1
množství	množství	k1gNnSc6
pro	pro	k7c4
domácí	domácí	k2eAgInSc4d1
japonský	japonský	k2eAgInSc4d1
trh	trh	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
Nintendo	Nintendo	k1gNnSc1
nezačalo	začít	k5eNaPmAgNnS
v	v	k7c6
Americe	Amerika	k1gFnSc6
ztrácet	ztrácet	k5eAaImF
půdu	půda	k1gFnSc4
pod	pod	k7c7
nohama	noha	k1gFnPc7
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
urychleně	urychleně	k6eAd1
dodána	dodat	k5eAaPmNgFnS
nová	nový	k2eAgFnSc1d1
verze	verze	k1gFnSc1
konzole	konzola	k1gFnSc3
NES-101	NES-101	k1gFnPc3
(	(	kIx(
<g/>
označovaná	označovaný	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
NES2	NES2	k1gFnSc1
či	či	k8xC
Top	topit	k5eAaImRp2nS
Loader	Loader	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
nakonec	nakonec	k6eAd1
vydržela	vydržet	k5eAaPmAgFnS
v	v	k7c6
prodeji	prodej	k1gInSc6
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1995	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prodejní	prodejní	k2eAgFnSc1d1
cena	cena	k1gFnSc1
byla	být	k5eAaImAgFnS
příznivých	příznivý	k2eAgNnPc6d1
49,95	49,95	k4
<g/>
$	$	kIx~
<g/>
,	,	kIx,
za	za	k7c4
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
jste	být	k5eAaImIp2nP
dostali	dostat	k5eAaPmAgMnP
zejména	zejména	k9
rozměrově	rozměrově	k6eAd1
velice	velice	k6eAd1
zajímavou	zajímavý	k2eAgFnSc4d1
krabičku	krabička	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
designu	design	k1gInSc2
konzole	konzola	k1gFnSc6
(	(	kIx(
<g/>
autorem	autor	k1gMnSc7
je	být	k5eAaImIp3nS
Lance	lance	k1gNnSc1
Barr	Barra	k1gFnPc2
<g/>
)	)	kIx)
se	se	k3xPyFc4
změnilo	změnit	k5eAaPmAgNnS
jen	jen	k9
pár	pár	k4xCyI
věcí	věc	k1gFnPc2
–	–	k?
zejména	zejména	k9
bylo	být	k5eAaImAgNnS
mnohem	mnohem	k6eAd1
snazší	snadný	k2eAgNnSc1d2
vkládat	vkládat	k5eAaImF
cartridge	cartridge	k6eAd1
do	do	k7c2
systému	systém	k1gInSc2
<g/>
,	,	kIx,
dále	daleko	k6eAd2
potom	potom	k6eAd1
u	u	k7c2
konzole	konzola	k1gFnSc3
byly	být	k5eAaImAgInP
nové	nový	k2eAgInPc1d1
ovladače	ovladač	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
byly	být	k5eAaImAgInP
velmi	velmi	k6eAd1
podobné	podobný	k2eAgInPc1d1
SNESovým	SNESův	k2eAgInPc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
konci	konec	k1gInSc3
90	#num#	k4
<g/>
.	.	kIx.
let	let	k1gInSc4
s	s	k7c7
rozmachem	rozmach	k1gInSc7
internetu	internet	k1gInSc2
začaly	začít	k5eAaPmAgFnP
vznikat	vznikat	k5eAaImF
první	první	k4xOgInPc4
emulátory	emulátor	k1gInPc4
a	a	k8xC
naplno	naplno	k6eAd1
se	se	k3xPyFc4
rozběhl	rozběhnout	k5eAaPmAgInS
bazarový	bazarový	k2eAgInSc1d1
obchod	obchod	k1gInSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
NES	nést	k5eAaImRp2nS
už	už	k6eAd1
byl	být	k5eAaImAgInS
velice	velice	k6eAd1
levný	levný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
se	se	k3xPyFc4
NES	nést	k5eAaImRp2nS
začal	začít	k5eAaPmAgInS
různě	různě	k6eAd1
po	po	k7c6
domácku	domáck	k1gInSc6
upravovat	upravovat	k5eAaImF
–	–	k?
nové	nový	k2eAgNnSc4d1
case	case	k1gNnSc4
<g/>
,	,	kIx,
propojení	propojení	k1gNnSc4
s	s	k7c7
PC	PC	kA
<g/>
,	,	kIx,
USB	USB	kA
nebo	nebo	k8xC
sériové	sériový	k2eAgInPc1d1
porty	port	k1gInPc1
<g/>
,	,	kIx,
přenosné	přenosný	k2eAgFnPc1d1
verze	verze	k1gFnPc1
napájené	napájený	k2eAgFnPc1d1
bateriemi	baterie	k1gFnPc7
či	či	k8xC
dokonce	dokonce	k9
i	i	k9
s	s	k7c7
vestavěným	vestavěný	k2eAgNnSc7d1
LCD	LCD	kA
displejem	displej	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Ovladače	ovladač	k1gInPc1
a	a	k8xC
příslušenství	příslušenství	k1gNnSc1
</s>
<s>
NES	nést	k5eAaImRp2nS
ovladač	ovladač	k1gInSc4
</s>
<s>
Původní	původní	k2eAgInSc1d1
ovladač	ovladač	k1gInSc1
se	se	k3xPyFc4
vyznačoval	vyznačovat	k5eAaImAgInS
zejména	zejména	k9
velikou	veliký	k2eAgFnSc7d1
jednoduchostí	jednoduchost	k1gFnSc7
<g/>
,	,	kIx,
dalo	dát	k5eAaPmAgNnS
by	by	kYmCp3nS
se	se	k3xPyFc4
říci	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
o	o	k7c4
ergonomii	ergonomie	k1gFnSc4
slyšel	slyšet	k5eAaImAgMnS
opravdu	opravdu	k6eAd1
jen	jen	k9
z	z	k7c2
dálky	dálka	k1gFnSc2
–	–	k?
tvar	tvar	k1gInSc4
malé	malý	k2eAgFnSc2d1
kostičky	kostička	k1gFnSc2
s	s	k7c7
křížovým	křížový	k2eAgInSc7d1
D-Padem	D-Pad	k1gInSc7
a	a	k8xC
tlačítky	tlačítko	k1gNnPc7
A	a	k8xC
a	a	k8xC
B	B	kA
(	(	kIx(
<g/>
na	na	k7c4
dlouhé	dlouhý	k2eAgNnSc4d1
hraní	hraní	k1gNnSc4
nepříliš	příliš	k6eNd1
ideální	ideální	k2eAgFnPc1d1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovládání	ovládání	k1gNnSc1
bylo	být	k5eAaImAgNnS
v	v	k7c6
podstatě	podstata	k1gFnSc6
převzato	převzat	k2eAgNnSc1d1
z	z	k7c2
přenosných	přenosný	k2eAgFnPc2d1
konzolí	konzole	k1gFnPc2
Game	game	k1gInSc1
&	&	k?
Watch	Watch	k1gInSc1
(	(	kIx(
<g/>
u	u	k7c2
nás	my	k3xPp1nPc2
tolik	tolik	k6eAd1
populární	populární	k2eAgInPc1d1
klony	klon	k1gInPc1
jako	jako	k8xC,k8xS
Chobotnice	chobotnice	k1gFnPc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
Chytání	chytání	k1gNnSc1
vajec	vejce	k1gNnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
prapůvodní	prapůvodní	k2eAgFnSc2d1
japonské	japonský	k2eAgFnSc2d1
verze	verze	k1gFnSc2
se	se	k3xPyFc4
ovladače	ovladač	k1gInPc1
lišily	lišit	k5eAaImAgInP
–	–	k?
zaprvé	zaprvé	k4xO
ovladače	ovladač	k1gInSc2
byly	být	k5eAaImAgInP
napevno	napevno	k6eAd1
propojeny	propojit	k5eAaPmNgInP
s	s	k7c7
konzolí	konzole	k1gFnSc7
a	a	k8xC
nešly	jít	k5eNaImAgFnP
vytáhnout	vytáhnout	k5eAaPmF
a	a	k8xC
zadruhé	zadruhé	k4xO
-	-	kIx~
ovladač	ovladač	k1gInSc4
A	a	k9
měl	mít	k5eAaImAgMnS
tlačítka	tlačítko	k1gNnSc2
Start	start	k1gInSc1
a	a	k8xC
Select	Select	k1gInSc1
a	a	k8xC
ovladač	ovladač	k1gInSc1
B	B	kA
měl	mít	k5eAaImAgInS
malý	malý	k2eAgInSc1d1
mikrofon	mikrofon	k1gInSc1
a	a	k8xC
reproduktor	reproduktor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tuto	tento	k3xDgFnSc4
funkci	funkce	k1gFnSc4
ovšem	ovšem	k9
používalo	používat	k5eAaImAgNnS
jen	jen	k9
málo	málo	k4c1
her	hra	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
Nintendo	Nintendo	k6eAd1
od	od	k7c2
mikrofonní	mikrofonní	k2eAgFnSc2d1
verze	verze	k1gFnSc2
upustilo	upustit	k5eAaPmAgNnS
a	a	k8xC
v	v	k7c6
pozdějších	pozdní	k2eAgFnPc6d2
verzích	verze	k1gFnPc6
už	už	k6eAd1
se	se	k3xPyFc4
setkáváme	setkávat	k5eAaImIp1nP
se	s	k7c7
Startem	start	k1gInSc7
a	a	k8xC
Selectem	Select	k1gInSc7
na	na	k7c6
obou	dva	k4xCgInPc6
ovladačích	ovladač	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Standardně	standardně	k6eAd1
NES	nést	k5eAaImRp2nS
podporuje	podporovat	k5eAaImIp3nS
jen	jen	k6eAd1
2	#num#	k4
hráče	hráč	k1gMnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
s	s	k7c7
rozšířením	rozšíření	k1gNnSc7
jménem	jméno	k1gNnSc7
FourScore	FourScor	k1gInSc5
mohli	moct	k5eAaImAgMnP
hrát	hrát	k5eAaImF
až	až	k9
čtyři	čtyři	k4xCgMnPc1
hráči	hráč	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ovšem	ovšem	k9
tato	tento	k3xDgFnSc1
funkce	funkce	k1gFnSc1
byla	být	k5eAaImAgFnS
využitelná	využitelný	k2eAgFnSc1d1
pouze	pouze	k6eAd1
zhruba	zhruba	k6eAd1
ve	v	k7c6
dvacítce	dvacítka	k1gFnSc6
her	hra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Zapper	Zapper	k1gMnSc1
</s>
<s>
NES	nést	k5eAaImRp2nS
Zapper	Zappra	k1gFnPc2
</s>
<s>
Velice	velice	k6eAd1
populárním	populární	k2eAgInSc7d1
ovladačem	ovladač	k1gInSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
také	také	k9
NES	nést	k5eAaImRp2nS
Zapper	Zapper	k1gInSc4
<g/>
,	,	kIx,
neboli	neboli	k8xC
světelná	světelný	k2eAgFnSc1d1
pistole	pistole	k1gFnSc1
pro	pro	k7c4
NES	nést	k5eAaImRp2nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Japonská	japonský	k2eAgFnSc1d1
verze	verze	k1gFnSc1
byla	být	k5eAaImAgFnS
dělaná	dělaný	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
revolver	revolver	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostatní	ostatní	k2eAgFnPc4d1
verze	verze	k1gFnPc4
(	(	kIx(
<g/>
a	a	k8xC
tedy	tedy	k9
i	i	k9
ta	ten	k3xDgFnSc1
se	se	k3xPyFc4
kterou	který	k3yQgFnSc4,k3yRgFnSc4,k3yIgFnSc4
jste	být	k5eAaImIp2nP
se	se	k3xPyFc4
mohli	moct	k5eAaImAgMnP
setkat	setkat	k5eAaPmF
v	v	k7c6
Čechách	Čechy	k1gFnPc6
<g/>
)	)	kIx)
měli	mít	k5eAaImAgMnP
spíše	spíše	k9
podobu	podoba	k1gFnSc4
větší	veliký	k2eAgFnSc2d2
zbraně	zbraň	k1gFnSc2
z	z	k7c2
nějakého	nějaký	k3yIgMnSc2
sci-fi	sci-fi	k1gNnPc2
béčkového	béčkový	k2eAgInSc2d1
filmu	film	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
dispozici	dispozice	k1gFnSc3
byly	být	k5eAaImAgInP
dvě	dva	k4xCgFnPc4
barvy	barva	k1gFnPc4
a	a	k8xC
to	ten	k3xDgNnSc1
šedá	šedat	k5eAaImIp3nS
a	a	k8xC
později	pozdě	k6eAd2
i	i	k9
oranžová	oranžový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
pistolí	pistol	k1gFnSc7
se	se	k3xPyFc4
míří	mířit	k5eAaImIp3nS
po	po	k7c6
obrazovce	obrazovka	k1gFnSc6
a	a	k8xC
střílí	střílet	k5eAaImIp3nS
se	se	k3xPyFc4
jako	jako	k9
skutečnou	skutečný	k2eAgFnSc7d1
zbraní	zbraň	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
styl	styl	k1gInSc4
hraní	hranit	k5eAaImIp3nS
byl	být	k5eAaImAgMnS
svého	svůj	k3xOyFgInSc2
času	čas	k1gInSc2
dost	dost	k6eAd1
populární	populární	k2eAgFnPc1d1
a	a	k8xC
pro	pro	k7c4
NES	nést	k5eAaImRp2nS
vzniklo	vzniknout	k5eAaPmAgNnS
celkem	celek	k1gInSc7
18	#num#	k4
her	hra	k1gFnPc2
využívajících	využívající	k2eAgFnPc2d1
Zapperu	Zappera	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Power	Power	k1gInSc1
Glove	Gloev	k1gFnSc2
</s>
<s>
Power	Power	k1gInSc1
Glove	Gloev	k1gFnSc2
je	být	k5eAaImIp3nS
vlastně	vlastně	k9
taková	takový	k3xDgFnSc1
kožená	kožený	k2eAgFnSc1d1
rukavice	rukavice	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
má	mít	k5eAaImIp3nS
ve	v	k7c6
vrchní	vrchní	k2eAgFnSc6d1
části	část	k1gFnSc6
vestavěna	vestavěn	k2eAgFnSc1d1
všechna	všechen	k3xTgNnPc4
tlačítka	tlačítko	k1gNnPc4
<g/>
,	,	kIx,
která	který	k3yQgNnPc1,k3yIgNnPc1,k3yRgNnPc1
najdete	najít	k5eAaPmIp2nP
i	i	k9
na	na	k7c6
standardním	standardní	k2eAgInSc6d1
ovladači	ovladač	k1gInSc6
<g/>
,	,	kIx,
mimoto	mimoto	k6eAd1
je	být	k5eAaImIp3nS
k	k	k7c3
dispozici	dispozice	k1gFnSc3
ještě	ještě	k6eAd1
dalších	další	k2eAgInPc2d1
12	#num#	k4
tlačítek	tlačítko	k1gNnPc2
<g/>
,	,	kIx,
která	který	k3yRgNnPc1,k3yIgNnPc1,k3yQgNnPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
hru	hra	k1gFnSc4
od	od	k7c2
hry	hra	k1gFnSc2
různě	různě	k6eAd1
použity	použít	k5eAaPmNgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mačkáním	mačkání	k1gNnSc7
prstů	prst	k1gInPc2
můžete	moct	k5eAaImIp2nP
mačkat	mačkat	k5eAaImF
i	i	k9
tlačítka	tlačítko	k1gNnPc1
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
třeba	třeba	k6eAd1
palcem	palec	k1gInSc7
tlačítko	tlačítko	k1gNnSc4
A.	A.	kA
Tento	tento	k3xDgInSc4
doplněk	doplněk	k1gInSc4
sice	sice	k8xC
není	být	k5eNaImIp3nS
přímo	přímo	k6eAd1
od	od	k7c2
Nintenda	Nintend	k1gMnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
dostatečně	dostatečně	k6eAd1
populárním	populární	k2eAgNnSc7d1
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
ho	on	k3xPp3gMnSc4
později	pozdě	k6eAd2
podporovalo	podporovat	k5eAaImAgNnS
přes	přes	k7c4
50	#num#	k4
her	hra	k1gFnPc2
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
včetně	včetně	k7c2
first-party	first-parta	k1gFnSc2
titulů	titul	k1gInPc2
jako	jako	k8xS,k8xC
třeba	třeba	k6eAd1
Zelda	Zelda	k1gFnSc1
2	#num#	k4
<g/>
,	,	kIx,
nebo	nebo	k8xC
Metroid	Metroid	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouze	pouze	k6eAd1
dva	dva	k4xCgInPc1
tituly	titul	k1gInPc1
měly	mít	k5eAaImAgInP
plnou	plný	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
včetně	včetně	k7c2
pohybů	pohyb	k1gInPc2
ruky	ruka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podpora	podpora	k1gFnSc1
pohybu	pohyb	k1gInSc2
ruky	ruka	k1gFnSc2
byla	být	k5eAaImAgFnS
málo	málo	k6eAd1
citlivá	citlivý	k2eAgFnSc1d1
a	a	k8xC
těžko	těžko	k6eAd1
ovladatelná	ovladatelný	k2eAgFnSc1d1
a	a	k8xC
proto	proto	k8xC
to	ten	k3xDgNnSc1
byl	být	k5eAaImAgInS
velký	velký	k2eAgInSc1d1
propadák	propadák	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
podstatě	podstata	k1gFnSc6
jste	být	k5eAaImIp2nP
před	před	k7c7
15	#num#	k4
lety	let	k1gInPc7
mohly	moct	k5eAaImAgInP
dokázat	dokázat	k5eAaPmF
s	s	k7c7
powerglove	powerglovat	k5eAaPmIp3nS
to	ten	k3xDgNnSc1
co	co	k9
dnes	dnes	k6eAd1
s	s	k7c7
WiiMote	WiiMot	k1gInSc5
nebo	nebo	k8xC
Eye	Eye	k1gMnSc6
Toy	Toy	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgNnSc1d1
příslušenství	příslušenství	k1gNnSc1
</s>
<s>
Díky	díky	k7c3
své	svůj	k3xOyFgFnSc3
popularitě	popularita	k1gFnSc3
získal	získat	k5eAaPmAgMnS
NES	nést	k5eAaImRp2nS
doslova	doslova	k6eAd1
tuny	tuna	k1gFnPc4
příslušenství	příslušenství	k1gNnPc2
od	od	k7c2
čteček	čtečka	k1gFnPc2
kazet	kazeta	k1gFnPc2
a	a	k8xC
disket	disketa	k1gFnPc2
přes	přes	k7c4
speciální	speciální	k2eAgInPc4d1
ovladače	ovladač	k1gInPc4
třeba	třeba	k6eAd1
na	na	k7c4
Arkanoid	Arkanoid	k1gInSc4
<g/>
,	,	kIx,
joysticky	joysticky	k6eAd1
<g/>
,	,	kIx,
bezdrátové	bezdrátový	k2eAgInPc4d1
ovladače	ovladač	k1gInPc4
<g/>
,	,	kIx,
věci	věc	k1gFnPc4
jako	jako	k8xS,k8xC
modem	modem	k1gInSc4
<g/>
,	,	kIx,
až	až	k9
po	po	k7c4
robota	robot	k1gMnSc4
připojitelného	připojitelný	k2eAgMnSc4d1
k	k	k7c3
NES	nést	k5eAaImRp2nS
nebo	nebo	k8xC
light	light	k2eAgMnSc1d1
gun	gun	k?
na	na	k7c4
hlavu	hlava	k1gFnSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
jste	být	k5eAaImIp2nP
okoukali	okoukat	k5eAaPmAgMnP
hledím	hledí	k1gNnSc7
a	a	k8xC
stříleli	střílet	k5eAaImAgMnP
řevem	řev	k1gInSc7
do	do	k7c2
mikrofonu	mikrofon	k1gInSc2
(	(	kIx(
<g/>
Laser	laser	k1gInSc1
Scope	Scop	k1gInSc5
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Hry	hra	k1gFnPc1
</s>
<s>
NES	nést	k5eAaImRp2nS
s	s	k7c7
vloženou	vložený	k2eAgFnSc7d1
hrou	hra	k1gFnSc7
Mario	Mario	k1gMnSc1
Bros	Bros	k1gInSc4
</s>
<s>
10NES	10NES	k4
</s>
<s>
Aby	aby	kYmCp3nP
vycházely	vycházet	k5eAaImAgFnP
jen	jen	k6eAd1
Nintendem	Nintend	k1gInSc7
autorizované	autorizovaný	k2eAgFnSc2d1
hry	hra	k1gFnSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
vymyšlen	vymyslet	k5eAaPmNgInS
takzvaný	takzvaný	k2eAgInSc1d1
10	#num#	k4
<g/>
NES	nést	k5eAaImRp2nS
<g/>
.	.	kIx.
10NES	10NES	k4
měl	mít	k5eAaImAgInS
2	#num#	k4
části	část	k1gFnSc2
–	–	k?
první	první	k4xOgInPc1
byla	být	k5eAaImAgFnS
zabudována	zabudovat	k5eAaPmNgFnS
v	v	k7c6
konzoli	konzoli	k6eAd1
a	a	k8xC
druhá	druhý	k4xOgFnSc1
v	v	k7c6
cartridgi	cartridgi	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hra	hra	k1gFnSc1
se	se	k3xPyFc4
načetla	načíst	k5eAaPmAgFnS,k5eAaBmAgFnS
pouze	pouze	k6eAd1
pokud	pokud	k8xS
byly	být	k5eAaImAgFnP
propojeny	propojit	k5eAaPmNgFnP
obě	dva	k4xCgFnPc1
části	část	k1gFnPc1
<g/>
.	.	kIx.
10NES	10NES	k4
způsoboval	způsobovat	k5eAaImAgInS
také	také	k9
nejznámější	známý	k2eAgFnSc4d3
chybu	chyba	k1gFnSc4
–	–	k?
červené	červený	k2eAgNnSc4d1
světýlko	světýlko	k1gNnSc4
značící	značící	k2eAgNnSc1d1
zapnutí	zapnutí	k1gNnSc1
systémů	systém	k1gInPc2
začalo	začít	k5eAaPmAgNnS
blikat	blikat	k5eAaImF
a	a	k8xC
konzole	konzola	k1gFnSc3
se	se	k3xPyFc4
dokola	dokola	k6eAd1
zapínala	zapínat	k5eAaImAgFnS
a	a	k8xC
vypínala	vypínat	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Problém	problém	k1gInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
čip	čip	k1gInSc1
byl	být	k5eAaImAgInS
dost	dost	k6eAd1
vybíravý	vybíravý	k2eAgInSc1d1
a	a	k8xC
vadily	vadit	k5eAaImAgInP
mu	on	k3xPp3gMnSc3
zanesené	zanesený	k2eAgInPc1d1
nebo	nebo	k8xC
zprohýbané	zprohýbaný	k2eAgInPc1d1
konektory	konektor	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vše	všechen	k3xTgNnSc1
se	se	k3xPyFc4
řešilo	řešit	k5eAaImAgNnS
různě	různě	k6eAd1
–	–	k?
profukováním	profukování	k1gNnSc7
konektorů	konektor	k1gInPc2
<g/>
,	,	kIx,
čištěním	čištění	k1gNnSc7
alkoholem	alkohol	k1gInSc7
nebo	nebo	k8xC
klasický	klasický	k2eAgInSc1d1
boucháním	bouchání	k1gNnSc7
do	do	k7c2
konzole	konzola	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnPc1
tyto	tento	k3xDgFnPc1
techniky	technika	k1gFnPc1
mohly	moct	k5eAaImAgFnP
vyústit	vyústit	k5eAaPmF
až	až	k9
ke	k	k7c3
zničení	zničení	k1gNnSc3
cartridge	cartridg	k1gInSc2
nebo	nebo	k8xC
konzole	konzola	k1gFnSc6
<g/>
.	.	kIx.
10NES	10NES	k4
chip	chip	k1gInSc1
byl	být	k5eAaImAgInS
také	také	k9
důvodem	důvod	k1gInSc7
<g/>
,	,	kIx,
proč	proč	k6eAd1
herní	herní	k2eAgFnPc4d1
kazety	kazeta	k1gFnPc4
mají	mít	k5eAaImIp3nP
více	hodně	k6eAd2
pinů	pin	k1gInPc2
než	než	k8xS
kazety	kazeta	k1gFnSc2
pro	pro	k7c4
Famicom	Famicom	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Famicom	Famicom	k1gInSc1
touto	tento	k3xDgFnSc7
protipirátskou	protipirátský	k2eAgFnSc7d1
ochranou	ochrana	k1gFnSc7
nebyl	být	k5eNaImAgInS
osazen	osadit	k5eAaPmNgInS
<g/>
,	,	kIx,
díky	díky	k7c3
čemuž	což	k3yRnSc3,k3yQnSc3
snadno	snadno	k6eAd1
posloužil	posloužit	k5eAaPmAgInS
jako	jako	k9
vzor	vzor	k1gInSc1
pro	pro	k7c4
drtivou	drtivý	k2eAgFnSc4d1
většinu	většina	k1gFnSc4
neoriginálních	originální	k2eNgFnPc2d1
napodobenin	napodobenina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Kvalita	kvalita	k1gFnSc1
her	hra	k1gFnPc2
</s>
<s>
Licencování	licencování	k1gNnSc1
her	hra	k1gFnPc2
pro	pro	k7c4
Nintendo	Nintendo	k1gNnSc4
systémy	systém	k1gInPc4
byl	být	k5eAaImAgInS
vždy	vždy	k6eAd1
problém	problém	k1gInSc4
a	a	k8xC
počátky	počátek	k1gInPc4
těchto	tento	k3xDgInPc2
dlouho	dlouho	k6eAd1
kritizovaných	kritizovaný	k2eAgInPc2d1
problémů	problém	k1gInPc2
leží	ležet	k5eAaImIp3nS
daleko	daleko	k6eAd1
v	v	k7c6
historii	historie	k1gFnSc6
u	u	k7c2
NESu	nést	k5eAaImIp1nS
–	–	k?
Nintendo	Nintendo	k1gNnSc1
si	se	k3xPyFc3
nastavovalo	nastavovat	k5eAaImAgNnS
podmínky	podmínka	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
hra	hra	k1gFnSc1
musela	muset	k5eAaImAgFnS
projít	projít	k5eAaPmF
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
pro	pro	k7c4
konzoli	konzole	k1gFnSc4
byla	být	k5eAaImAgFnS
schválena	schválit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
kontrolou	kontrola	k1gFnSc7
kvality	kvalita	k1gFnSc2
neprošla	projít	k5eNaPmAgFnS
<g/>
,	,	kIx,
tak	tak	k6eAd1
měla	mít	k5eAaImAgFnS
hra	hra	k1gFnSc1
smůlu	smůla	k1gFnSc4
a	a	k8xC
nešla	jít	k5eNaImAgFnS
do	do	k7c2
výrobního	výrobní	k2eAgInSc2d1
procesu	proces	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
díky	díky	k7c3
10NES	10NES	k4
chipu	chip	k1gInSc3
zajišťovalo	zajišťovat	k5eAaImAgNnS
jen	jen	k9
samo	sám	k3xTgNnSc1
Nintendo	Nintendo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ruka	ruka	k1gFnSc1
v	v	k7c6
ruce	ruka	k1gFnSc6
s	s	k7c7
tím	ten	k3xDgNnSc7
jde	jít	k5eAaImIp3nS
označení	označení	k1gNnSc1
„	„	k?
<g/>
Nintendo	Nintendo	k1gNnSc1
Seal	Seal	k1gMnSc1
of	of	k?
Quality	Qualita	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
v	v	k7c6
podstatě	podstata	k1gFnSc6
mělo	mít	k5eAaImAgNnS
označovat	označovat	k5eAaImF
jen	jen	k9
nejkvalitnější	kvalitní	k2eAgInPc4d3
tituly	titul	k1gInPc4
<g/>
,	,	kIx,
praxe	praxe	k1gFnSc1
byla	být	k5eAaImAgFnS
jiná	jiný	k2eAgFnSc1d1
–	–	k?
značku	značka	k1gFnSc4
dostal	dostat	k5eAaPmAgMnS
každý	každý	k3xTgMnSc1
<g/>
,	,	kIx,
kdo	kdo	k3yQnSc1,k3yRnSc1,k3yInSc1
si	se	k3xPyFc3
jí	on	k3xPp3gFnSc3
zaplatil	zaplatit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Navíc	navíc	k6eAd1
Nintendo	Nintendo	k6eAd1
tvrdě	tvrdě	k6eAd1
kontrolovalo	kontrolovat	k5eAaImAgNnS
<g/>
,	,	kIx,
kolik	kolik	k4yIc4,k4yRc4,k4yQc4
bude	být	k5eAaImBp3nS
vydáno	vydat	k5eAaPmNgNnS
cartridgí	cartridgí	k6eAd1
s	s	k7c7
jednotlivými	jednotlivý	k2eAgFnPc7d1
hrami	hra	k1gFnPc7
<g/>
,	,	kIx,
následkem	následkem	k7c2
toho	ten	k3xDgNnSc2
bylo	být	k5eAaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
reálně	reálně	k6eAd1
prodalo	prodat	k5eAaPmAgNnS
o	o	k7c4
několik	několik	k4yIc4
(	(	kIx(
<g/>
možná	možná	k9
i	i	k9
desítek	desítka	k1gFnPc2
<g/>
)	)	kIx)
milionů	milion	k4xCgInPc2
her	hra	k1gFnPc2
méně	málo	k6eAd2
<g/>
.	.	kIx.
</s>
<s>
Hry	hra	k1gFnPc1
</s>
<s>
Ale	ale	k9
ke	k	k7c3
hrám	hra	k1gFnPc3
–	–	k?
pro	pro	k7c4
NES	nést	k5eAaImRp2nS
bylo	být	k5eAaImAgNnS
celkem	celkem	k6eAd1
vydáno	vydat	k5eAaPmNgNnS
něco	něco	k6eAd1
kolem	kolem	k7c2
900	#num#	k4
kusů	kus	k1gInPc2
her	hra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svůj	svůj	k3xOyFgInSc4
život	život	k1gInSc4
zde	zde	k6eAd1
započaly	započnout	k5eAaPmAgFnP
největší	veliký	k2eAgFnPc4d3
herní	herní	k2eAgFnPc4d1
série	série	k1gFnPc4
Nintenda	Nintenda	k1gFnSc1
–	–	k?
ať	ať	k9
už	už	k6eAd1
Super	super	k2eAgMnSc1d1
Mario	Mario	k1gMnSc1
Bros	Brosa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
,	,	kIx,
The	The	k1gMnSc1
Legend	legenda	k1gFnPc2
of	of	k?
Zelda	Zelda	k1gMnSc1
<g/>
,	,	kIx,
Metroid	Metroid	k1gInSc1
<g/>
,	,	kIx,
Donkey	Donkea	k1gFnPc1
Kong	Kongo	k1gNnPc2
nebo	nebo	k8xC
třeba	třeba	k6eAd1
Final	Final	k1gMnSc1
Fantasy	fantas	k1gInPc4
a	a	k8xC
Metal	metat	k5eAaImAgMnS
Gear	Gear	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asi	asi	k9
největším	veliký	k2eAgInSc7d3
hitem	hit	k1gInSc7
(	(	kIx(
<g/>
prodejním	prodejní	k2eAgMnSc7d1
i	i	k9
kvalitativním	kvalitativní	k2eAgMnSc7d1
<g/>
)	)	kIx)
pro	pro	k7c4
NES	nést	k5eAaImRp2nS
a	a	k8xC
tahounem	tahoun	k1gInSc7
konzole	konzola	k1gFnSc3
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
hra	hra	k1gFnSc1
Super	super	k1gInSc1
Mario	Mario	k1gMnSc1
Bros	Bros	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prodalo	prodat	k5eAaPmAgNnS
se	se	k3xPyFc4
jí	on	k3xPp3gFnSc7
kolem	kolem	k6eAd1
40	#num#	k4
milionů	milion	k4xCgInPc2
kusů	kus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Klony	klon	k1gInPc1
a	a	k8xC
emulace	emulace	k1gFnPc1
</s>
<s>
Konzole	konzola	k1gFnSc3
NES	nést	k5eAaImRp2nS
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
velice	velice	k6eAd1
oblíbenou	oblíbený	k2eAgFnSc4d1
pro	pro	k7c4
různé	různý	k2eAgFnPc4d1
napodobeniny	napodobenina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
nás	my	k3xPp1nPc2
jste	být	k5eAaImIp2nP
již	již	k6eAd1
v	v	k7c6
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
mohli	moct	k5eAaImAgMnP
snadno	snadno	k6eAd1
sehnat	sehnat	k5eAaPmF
v	v	k7c6
kterékoliv	kterýkoliv	k3yIgFnSc6
z	z	k7c2
početných	početný	k2eAgFnPc2d1
vietnamských	vietnamský	k2eAgFnPc2d1
tržnic	tržnice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
modelu	model	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
byl	být	k5eAaImAgInS
podobný	podobný	k2eAgInSc1d1
i	i	k9
vizuálně	vizuálně	k6eAd1
<g/>
,	,	kIx,
se	se	k3xPyFc4
daly	dát	k5eAaPmAgFnP
sehnat	sehnat	k5eAaPmF
například	například	k6eAd1
–	–	k?
PolyStation	PolyStation	k1gInSc1
–	–	k?
NES	nést	k5eAaImRp2nS
v	v	k7c6
podobě	podoba	k1gFnSc6
prvního	první	k4xOgInSc2
Playstationu	Playstation	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
Mega	mega	k1gNnSc1
Kid	Kid	k1gFnSc2
MK-	MK-	k1gFnSc2
<g/>
1000	#num#	k4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
má	mít	k5eAaImIp3nS
vlastní	vlastní	k2eAgFnSc4d1
klávesnici	klávesnice	k1gFnSc4
<g/>
,	,	kIx,
a	a	k8xC
dá	dát	k5eAaPmIp3nS
se	se	k3xPyFc4
pod	pod	k7c7
ním	on	k3xPp3gInSc7
provozovat	provozovat	k5eAaImF
jednoduchá	jednoduchý	k2eAgFnSc1d1
verze	verze	k1gFnSc1
Basicu	Basicus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klonů	klon	k1gInPc2
bylo	být	k5eAaImAgNnS
více	hodně	k6eAd2
typů	typ	k1gInPc2
–	–	k?
například	například	k6eAd1
jen	jen	k9
v	v	k7c6
podobě	podoba	k1gFnSc6
ovladače	ovladač	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
se	se	k3xPyFc4
připojil	připojit	k5eAaPmAgMnS
k	k	k7c3
TV	TV	kA
anebo	anebo	k8xC
přenosný	přenosný	k2eAgInSc4d1
PocketFami	PocketFa	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
je	být	k5eAaImIp3nS
známo	znám	k2eAgNnSc1d1
přes	přes	k7c4
377	#num#	k4
druhů	druh	k1gInPc2
všemožných	všemožný	k2eAgFnPc2d1
napodobenin	napodobenina	k1gFnPc2
NESu	nést	k5eAaImIp1nS
<g/>
/	/	kIx~
<g/>
Famicomu	Famicom	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Systém	systém	k1gInSc1
je	být	k5eAaImIp3nS
velice	velice	k6eAd1
oblíbený	oblíbený	k2eAgInSc1d1
pro	pro	k7c4
velké	velký	k2eAgNnSc4d1
množství	množství	k1gNnSc4
emulátorů	emulátor	k1gInPc2
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
přestože	přestože	k8xS
paradoxně	paradoxně	k6eAd1
patří	patřit	k5eAaImIp3nS
k	k	k7c3
těm	ten	k3xDgInPc3
náročnějším	náročný	k2eAgInPc3d2
<g/>
,	,	kIx,
kvůli	kvůli	k7c3
nestandardnímu	standardní	k2eNgNnSc3d1
používání	používání	k1gNnSc3
procesoru	procesor	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Existuje	existovat	k5eAaImIp3nS
přes	přes	k7c4
40	#num#	k4
emulátorů	emulátor	k1gInPc2
pro	pro	k7c4
PC	PC	kA
<g/>
,	,	kIx,
ale	ale	k8xC
setkat	setkat	k5eAaPmF
se	se	k3xPyFc4
s	s	k7c7
nimi	on	k3xPp3gFnPc7
můžeme	moct	k5eAaImIp1nP
na	na	k7c6
PDA	PDA	kA
<g/>
,	,	kIx,
mobilních	mobilní	k2eAgInPc6d1
telefonech	telefon	k1gInPc6
<g/>
,	,	kIx,
jiných	jiný	k2eAgFnPc6d1
konzolích	konzole	k1gFnPc6
jako	jako	k8xS,k8xC
třeba	třeba	k6eAd1
GP	GP	kA
<g/>
2	#num#	k4
<g/>
x.	x.	k?
Za	za	k7c4
emulaci	emulace	k1gFnSc4
lze	lze	k6eAd1
považovat	považovat	k5eAaImF
i	i	k9
provozování	provozování	k1gNnSc4
her	hra	k1gFnPc2
přes	přes	k7c4
tzv.	tzv.	kA
virtuální	virtuální	k2eAgFnSc4d1
konzoli	konzole	k1gFnSc4
Virtual	Virtual	k1gMnSc1
Console	Console	k1gFnSc2
<g/>
™	™	k?
na	na	k7c6
Nintendu	Nintend	k1gInSc6
Wii	Wii	k1gMnPc2
a	a	k8xC
3	#num#	k4
<g/>
DS	DS	kA
<g/>
.	.	kIx.
</s>
<s>
Parametry	parametr	k1gInPc1
</s>
<s>
Tato	tento	k3xDgFnSc1
část	část	k1gFnSc1
článku	článek	k1gInSc2
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ji	on	k3xPp3gFnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Konkrétní	konkrétní	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
:	:	kIx,
česká	český	k2eAgFnSc1d1
typografie	typografie	k1gFnSc1
</s>
<s>
Rozměry	rozměra	k1gFnPc1
</s>
<s>
Severní	severní	k2eAgFnSc1d1
Amerika	Amerika	k1gFnSc1
<g/>
/	/	kIx~
<g/>
Evropa	Evropa	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
'	'	kIx"
<g/>
toaster	toaster	k1gInSc1
<g/>
'	'	kIx"
verze	verze	k1gFnSc1
<g/>
:	:	kIx,
10	#num#	k4
<g/>
"	"	kIx"
šířka	šířka	k1gFnSc1
x	x	k?
8	#num#	k4
<g/>
"	"	kIx"
délka	délka	k1gFnSc1
x	x	k?
2.5	2.5	k4
<g/>
"	"	kIx"
výška	výška	k1gFnSc1
(	(	kIx(
<g/>
s	s	k7c7
otevřenými	otevřený	k2eAgNnPc7d1
dvířky	dvířka	k1gNnPc7
ještě	ještě	k9
o	o	k7c4
1	#num#	k4
<g/>
"	"	kIx"
větší	veliký	k2eAgFnSc2d2
<g/>
)	)	kIx)
</s>
<s>
'	'	kIx"
<g/>
toploader	toploader	k1gInSc1
<g/>
'	'	kIx"
verze	verze	k1gFnSc1
<g/>
:	:	kIx,
6	#num#	k4
<g/>
"	"	kIx"
šířka	šířka	k1gFnSc1
x	x	k?
7	#num#	k4
<g/>
"	"	kIx"
délka	délka	k1gFnSc1
x	x	k?
1	#num#	k4
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2	#num#	k4
<g/>
"	"	kIx"
výška	výška	k1gFnSc1
</s>
<s>
cartridge	cartridge	k1gFnSc1
<g/>
:	:	kIx,
5.5	5.5	k4
<g/>
"	"	kIx"
délka	délka	k1gFnSc1
x	x	k?
4.1	4.1	k4
<g/>
"	"	kIx"
šířka	šířka	k1gFnSc1
</s>
<s>
Japonsko	Japonsko	k1gNnSc1
<g/>
:	:	kIx,
</s>
<s>
cartridge	cartridge	k1gFnSc1
<g/>
:	:	kIx,
3	#num#	k4
<g/>
"	"	kIx"
délka	délka	k1gFnSc1
x	x	k?
5.3	5.3	k4
<g/>
"	"	kIx"
šířka	šířka	k1gFnSc1
</s>
<s>
CPU	CPU	kA
<g/>
:	:	kIx,
Ricoh	Ricoh	k1gInSc4
8	#num#	k4
<g/>
bit	bit	k2eAgInSc1d1
procesor	procesor	k1gInSc1
postavený	postavený	k2eAgInSc1d1
na	na	k7c6
MOS	MOS	kA
Technology	technolog	k1gMnPc7
6502	#num#	k4
jádru	jádro	k1gNnSc6
<g/>
,	,	kIx,
obyčejný	obyčejný	k2eAgInSc1d1
zvukový	zvukový	k2eAgInSc1d1
hardware	hardware	k1gInSc1
a	a	k8xC
omezený	omezený	k2eAgInSc1d1
DMA	dmout	k5eAaImSgInS
ovladač	ovladač	k1gInSc4
on-die	on-die	k1gFnPc4
</s>
<s>
Rozdíly	rozdíl	k1gInPc1
mezi	mezi	k7c7
verzemi	verze	k1gFnPc7
</s>
<s>
NTSC	NTSC	kA
verze	verze	k1gFnSc1
<g/>
,	,	kIx,
pojmenovaná	pojmenovaný	k2eAgFnSc1d1
RP	RP	kA
<g/>
2	#num#	k4
<g/>
A	a	k9
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
,	,	kIx,
běžela	běžet	k5eAaImAgFnS
na	na	k7c4
1.79	1.79	k4
MHz	Mhz	kA
<g/>
;	;	kIx,
toto	tento	k3xDgNnSc4
CPU	CPU	kA
bylo	být	k5eAaImAgNnS
také	také	k6eAd1
použito	použít	k5eAaPmNgNnS
ve	v	k7c6
videoherním	videoherní	k2eAgInSc6d1
automatu	automat	k1gInSc6
PlayChoice-	PlayChoice-	k1gFnSc2
<g/>
10	#num#	k4
a	a	k8xC
v	v	k7c4
Nintendo	Nintendo	k1gNnSc4
Vs	Vs	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Series	Series	k1gInSc4
(	(	kIx(
<g/>
VideoAutomaty	VideoAutoma	k1gNnPc7
od	od	k7c2
Nintenda	Nintenda	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
PAL	pal	k1gInSc1
verze	verze	k1gFnSc1
<g/>
,	,	kIx,
pojmenovaná	pojmenovaný	k2eAgFnSc1d1
RP	RP	kA
<g/>
2	#num#	k4
<g/>
A	a	k9
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
,	,	kIx,
běžela	běžet	k5eAaImAgFnS
na	na	k7c4
1.66	1.66	k4
MHz	Mhz	kA
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1
RAM	RAM	kA
<g/>
:	:	kIx,
2	#num#	k4
KiB	KiB	k1gMnPc2
plus	plus	k6eAd1
rozšířená	rozšířený	k2eAgFnSc1d1
RAM	RAM	kA
paměť	paměť	k1gFnSc1
byla	být	k5eAaImAgFnS
<g/>
-li	-li	k?
přítomna	přítomen	k2eAgFnSc1d1
cartridge	cartridge	k1gFnSc1
</s>
<s>
ROM	ROM	kA
<g/>
:	:	kIx,
49128	#num#	k4
bytes	bytes	k1gInSc1
ROM	ROM	kA
<g/>
,	,	kIx,
expanded	expanded	k1gInSc1
RAM	RAM	kA
<g/>
,	,	kIx,
a	a	k8xC
cartridge	cartridge	k6eAd1
I	i	k9
<g/>
/	/	kIx~
<g/>
O	o	k7c4
<g/>
;	;	kIx,
paging	paging	k1gInSc4
možno	možno	k6eAd1
rozšířit	rozšířit	k5eAaPmF
podle	podle	k7c2
požadavků	požadavek	k1gInPc2
</s>
<s>
Audio	audio	k2eAgNnSc4d1
<g/>
:	:	kIx,
Pět	pět	k4xCc4
zvukových	zvukový	k2eAgInPc2d1
kanálů	kanál	k1gInPc2
</s>
<s>
2	#num#	k4
pulse-wave	pulse-wavat	k5eAaPmIp3nS
kanály	kanál	k1gInPc4
<g/>
,	,	kIx,
variabilní	variabilní	k2eAgInSc1d1
duty	duty	k?
cycle	cycle	k1gInSc1
(	(	kIx(
<g/>
25	#num#	k4
<g/>
%	%	kIx~
<g/>
,	,	kIx,
50	#num#	k4
<g/>
%	%	kIx~
<g/>
,	,	kIx,
75	#num#	k4
<g/>
%	%	kIx~
<g/>
,	,	kIx,
87.5	87.5	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
<g/>
,	,	kIx,
16	#num#	k4
<g/>
stupňové	stupňový	k2eAgFnPc1d1
ovládání	ovládání	k1gNnSc4
hlasitosti	hlasitost	k1gFnSc2
<g/>
,	,	kIx,
hardwarová	hardwarový	k2eAgFnSc1d1
pitch-bend	pitch-bend	k1gInSc4
podpora	podpora	k1gFnSc1
<g/>
,	,	kIx,
zvládá	zvládat	k5eAaImIp3nS
frekvence	frekvence	k1gFnSc1
od	od	k7c2
54	#num#	k4
Hz	Hz	kA
do	do	k7c2
28	#num#	k4
kHz	khz	kA
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
triangle-wave	triangle-wavat	k5eAaPmIp3nS
kanál	kanál	k1gInSc4
<g/>
,	,	kIx,
fixní	fixní	k2eAgFnSc4d1
hlasitost	hlasitost	k1gFnSc4
<g/>
,	,	kIx,
zvládá	zvládat	k5eAaImIp3nS
frekvence	frekvence	k1gFnSc1
od	od	k7c2
27	#num#	k4
Hz	Hz	kA
do	do	k7c2
56	#num#	k4
kHz	khz	kA
</s>
<s>
1	#num#	k4
white-noise	white-noise	k1gFnSc1
channel	channela	k1gFnPc2
<g/>
,	,	kIx,
16	#num#	k4
<g/>
stupňové	stupňový	k2eAgFnPc1d1
ovládání	ovládání	k1gNnSc4
hlasitosti	hlasitost	k1gFnSc2
<g/>
,	,	kIx,
podpora	podpora	k1gFnSc1
dvou	dva	k4xCgInPc2
módů	mód	k1gInPc2
(	(	kIx(
<g/>
regulací	regulace	k1gFnPc2
vstupů	vstup	k1gInPc2
na	na	k7c6
posuvném	posuvný	k2eAgInSc6d1
registru	registr	k1gInSc6
s	s	k7c7
lineární	lineární	k2eAgFnSc7d1
zpětnou	zpětný	k2eAgFnSc7d1
vazbou	vazba	k1gFnSc7
<g/>
)	)	kIx)
na	na	k7c6
16	#num#	k4
předprogramovaných	předprogramovaný	k2eAgFnPc6d1
frekvencích	frekvence	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
1	#num#	k4
delta	delta	k1gFnSc1
pulse-code	pulse-cod	k1gInSc5
modulation	modulation	k1gInSc1
(	(	kIx(
<g/>
DPCM	DPCM	kA
<g/>
)	)	kIx)
kanál	kanál	k1gInSc1
s	s	k7c7
6	#num#	k4
<g/>
bitovým	bitový	k2eAgInSc7d1
rozsahem	rozsah	k1gInSc7
<g/>
,	,	kIx,
používající	používající	k2eAgFnSc1d1
1	#num#	k4
<g/>
bit	bit	k1gInSc1
delta	delta	k1gNnSc2
kódování	kódování	k1gNnSc2
na	na	k7c4
16	#num#	k4
předprogramovaných	předprogramovaný	k2eAgFnPc2d1
sample	sample	k6eAd1
rates	ratesa	k1gFnPc2
od	od	k7c2
4.2	4.2	k4
kHz	khz	kA
do	do	k7c2
33.5	33.5	k4
kHz	khz	kA
<g/>
,	,	kIx,
také	také	k9
schopný	schopný	k2eAgMnSc1d1
přehrát	přehrát	k5eAaPmF
standard	standard	k1gInSc4
PCM	PCM	kA
zvuk	zvuk	k1gInSc1
zapsáním	zapsání	k1gNnPc3
individuální	individuální	k2eAgMnSc1d1
7	#num#	k4
<g/>
bit	bit	k2eAgInSc1d1
hodnotou	hodnota	k1gFnSc7
v	v	k7c6
časových	časový	k2eAgInPc6d1
intervalech	interval	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Picture	Pictur	k1gMnSc5
processing	processing	k1gInSc1
unit	unit	k1gMnSc1
(	(	kIx(
<g/>
PPU	PPU	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
Ricoh	Ricoh	k1gMnSc1
custom-made	custom-mást	k5eAaPmIp3nS
video	video	k1gNnSc4
procesor	procesor	k1gInSc1
</s>
<s>
Rozdíly	rozdíl	k1gInPc1
mezi	mezi	k7c7
verzemi	verze	k1gFnPc7
</s>
<s>
NTSC	NTSC	kA
verze	verze	k1gFnSc1
<g/>
,	,	kIx,
pojmenovaná	pojmenovaný	k2eAgFnSc1d1
RP	RP	kA
<g/>
2	#num#	k4
<g/>
C	C	kA
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
,	,	kIx,
běžící	běžící	k2eAgInSc4d1
na	na	k7c4
5.37	5.37	k4
MHz	Mhz	kA
+	+	kIx~
výstupy	výstup	k1gInPc1
composite	composit	k1gInSc5
video	video	k1gNnSc4
</s>
<s>
PAL	pal	k1gInSc1
verze	verze	k1gFnSc1
<g/>
,	,	kIx,
pojmenovaná	pojmenovaný	k2eAgFnSc1d1
RP	RP	kA
<g/>
2	#num#	k4
<g/>
C	C	kA
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
,	,	kIx,
běžící	běžící	k2eAgInSc4d1
na	na	k7c4
5.32	5.32	k4
MHz	Mhz	kA
+	+	kIx~
výstupy	výstup	k1gInPc1
composite	composit	k1gInSc5
video	video	k1gNnSc4
</s>
<s>
PlayChoice-	PlayChoice-	k?
<g/>
10	#num#	k4
verze	verze	k1gFnSc1
<g/>
,	,	kIx,
pojmenovaná	pojmenovaný	k2eAgFnSc1d1
RP	RP	kA
<g/>
2	#num#	k4
<g/>
C	C	kA
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
,	,	kIx,
běžící	běžící	k2eAgInSc4d1
na	na	k7c4
5.37	5.37	k4
MHz	Mhz	kA
+	+	kIx~
výstupy	výstup	k1gInPc1
RGB	RGB	kA
video	video	k1gNnSc4
(	(	kIx(
<g/>
na	na	k7c4
NTSC	NTSC	kA
frekvencích	frekvence	k1gFnPc6
<g/>
)	)	kIx)
</s>
<s>
Nintendo	Nintendo	k1gNnSc1
Vs	Vs	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Series	Series	k1gInSc1
verze	verze	k1gFnSc2
<g/>
,	,	kIx,
pojmenované	pojmenovaný	k2eAgFnSc2d1
RP2C04	RP2C04	k1gFnSc2
a	a	k8xC
RP	RP	kA
<g/>
2	#num#	k4
<g/>
C	C	kA
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
,	,	kIx,
běžící	běžící	k2eAgInSc4d1
na	na	k7c4
5.37	5.37	k4
MHz	Mhz	kA
+	+	kIx~
výstupy	výstup	k1gInPc1
RGB	RGB	kA
video	video	k1gNnSc4
(	(	kIx(
<g/>
na	na	k7c4
NTSC	NTSC	kA
frekvencích	frekvence	k1gFnPc6
<g/>
)	)	kIx)
používající	používající	k2eAgFnSc4d1
nestandardní	standardní	k2eNgFnSc4d1
paletu	paleta	k1gFnSc4
barev	barva	k1gFnPc2
kvůli	kvůli	k7c3
snazšímu	snadný	k2eAgNnSc3d2
přepínání	přepínání	k1gNnSc3
ROM	ROM	kA
s	s	k7c7
hrami	hra	k1gFnPc7
games	gamesa	k1gFnPc2
</s>
<s>
Barevná	barevný	k2eAgFnSc1d1
paleta	paleta	k1gFnSc1
<g/>
:	:	kIx,
48	#num#	k4
barev	barva	k1gFnPc2
a	a	k8xC
5	#num#	k4
odstínů	odstín	k1gInPc2
šedi	šeď	k1gFnSc2
v	v	k7c6
základu	základ	k1gInSc6
<g/>
;	;	kIx,
červená	červený	k2eAgFnSc1d1
<g/>
,	,	kIx,
zelená	zelený	k2eAgFnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
modrá	modré	k1gNnPc1
mohly	moct	k5eAaImAgFnP
být	být	k5eAaImF
individuálně	individuálně	k6eAd1
ztmaveny	ztmaven	k2eAgFnPc4d1
</s>
<s>
Zobrazení	zobrazení	k1gNnSc1
barev	barva	k1gFnPc2
<g/>
:	:	kIx,
25	#num#	k4
barev	barva	k1gFnPc2
najednou	najednou	k6eAd1
</s>
<s>
Hardwarová	hardwarový	k2eAgFnSc1d1
podpora	podpora	k1gFnSc1
spritů	sprit	k1gInPc2
</s>
<s>
Maximum	maximum	k1gNnSc1
najednou	najednou	k6eAd1
zobrazených	zobrazený	k2eAgInPc2d1
spritů	sprit	k1gInPc2
<g/>
:	:	kIx,
64	#num#	k4
</s>
<s>
Velikost	velikost	k1gFnSc1
spritů	sprit	k1gInPc2
<g/>
:	:	kIx,
8	#num#	k4
<g/>
×	×	k?
<g/>
8	#num#	k4
nebo	nebo	k8xC
8	#num#	k4
<g/>
×	×	k?
<g/>
16	#num#	k4
pixelů	pixel	k1gInPc2
</s>
<s>
PPU	PPU	kA
interní	interní	k2eAgFnSc4d1
paměť	paměť	k1gFnSc4
<g/>
:	:	kIx,
256	#num#	k4
bytů	byt	k1gInPc2
</s>
<s>
PPU	PPU	kA
externí	externí	k2eAgFnSc2d1
paměti	paměť	k1gFnSc2
(	(	kIx(
<g/>
Video	video	k1gNnSc1
RAM	RAM	kA
<g/>
)	)	kIx)
<g/>
:	:	kIx,
2	#num#	k4
KiB	KiB	k1gFnSc2
of	of	k?
RAM	RAM	kA
</s>
<s>
Rozlišení	rozlišení	k1gNnSc1
displaye	display	k1gFnSc2
<g/>
:	:	kIx,
256	#num#	k4
<g/>
×	×	k?
<g/>
240	#num#	k4
pixels	pixelsa	k1gFnPc2
<g/>
,	,	kIx,
ovšem	ovšem	k9
u	u	k7c2
NTSC	NTSC	kA
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
používalo	používat	k5eAaImAgNnS
256	#num#	k4
<g/>
×	×	k?
<g/>
224	#num#	k4
<g/>
,	,	kIx,
</s>
<s>
Video	video	k1gNnSc1
výstupy	výstup	k1gInPc7
</s>
<s>
Original	Originat	k5eAaImAgMnS,k5eAaPmAgMnS
NES	nést	k5eAaImRp2nS
<g/>
:	:	kIx,
RCA	RCA	kA
kompositní	kompositní	k2eAgInSc1d1
výstup	výstup	k1gInSc1
a	a	k8xC
RF	RF	kA
modulator	modulator	k1gInSc1
výstup	výstup	k1gInSc1
</s>
<s>
Original	Originat	k5eAaPmAgInS,k5eAaImAgInS
Famicom	Famicom	k1gInSc1
(	(	kIx(
<g/>
Japonsko	Japonsko	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
NES	nést	k5eAaImRp2nS
2	#num#	k4
<g/>
:	:	kIx,
pouze	pouze	k6eAd1
RF	RF	kA
modulator	modulator	k1gInSc1
výstup	výstup	k1gInSc1
</s>
<s>
AV	AV	kA
Famicom	Famicom	k1gInSc1
<g/>
:	:	kIx,
Pouze	pouze	k6eAd1
kompositní	kompositní	k2eAgNnSc4d1
video	video	k1gNnSc4
výstup	výstup	k1gInSc4
</s>
<s>
PlayChoice	PlayChoice	k1gFnSc1
10	#num#	k4
<g/>
:	:	kIx,
obrácený	obrácený	k2eAgMnSc1d1
RGB	RGB	kA
video	video	k1gNnSc4
výstup	výstup	k1gInSc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Nintendo	Nintendo	k1gNnSc1
Entertainment	Entertainment	k1gMnSc1
System	Syst	k1gMnSc7
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
http://ultimateconsoledatabase.com/famiclones.htm	http://ultimateconsoledatabase.com/famiclones.htm	k6eAd1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Nintendo	Nintendo	k6eAd1
</s>
<s>
Herní	herní	k2eAgFnSc3d1
konzole	konzola	k1gFnSc3
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Nintendo	Nintendo	k1gNnSc4
Entertainment	Entertainment	k1gMnSc1
System	Syst	k1gInSc7
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Webová	webový	k2eAgFnSc1d1
fan-stránka	fan-stránka	k1gFnSc1
o	o	k7c6
Systémech	systém	k1gInPc6
Nintendo	Nintendo	k6eAd1
</s>
<s>
Český	český	k2eAgMnSc1d1
prodejce	prodejce	k1gMnSc1
herních	herní	k2eAgFnPc2d1
konzolí	konzole	k1gFnPc2
Nintendo	Nintendo	k6eAd1
</s>
<s>
NESWorld	NESWorld	k6eAd1
-	-	kIx~
NES	nést	k5eAaImRp2nS
Archiv	archiv	k1gInSc1
(	(	kIx(
<g/>
EN	EN	kA
<g/>
)	)	kIx)
</s>
<s>
NES	nést	k5eAaImRp2nS
HQ	HQ	kA
-	-	kIx~
Archive	archiv	k1gInSc5
for	forum	k1gNnPc2
NES	nést	k5eAaImRp2nS
Informací	informace	k1gFnPc2
(	(	kIx(
<g/>
EN	EN	kA
<g/>
)	)	kIx)
</s>
<s>
Famicom	Famicom	k1gInSc4
World	Worlda	k1gFnPc2
<g/>
:	:	kIx,
Informace	informace	k1gFnPc4
o	o	k7c6
japonské	japonský	k2eAgFnSc6d1
verzi	verze	k1gFnSc6
NESu	nést	k5eAaImIp1nS
<g/>
,	,	kIx,
neboli	neboli	k8xC
Famicomu	Famicom	k1gInSc2
</s>
<s>
Tiscali	Tiscat	k5eAaPmAgMnP,k5eAaImAgMnP
Games	Games	k1gInSc4
-	-	kIx~
Nintendo	Nintendo	k6eAd1
Téma	téma	k1gNnSc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Nintendo	Nintendo	k6eAd1
stolní	stolní	k2eAgFnSc3d1
konzole	konzola	k1gFnSc3
</s>
<s>
Nintendo	Nintendo	k6eAd1
Entertainment	Entertainment	k1gInSc1
System	Systo	k1gNnSc7
<g/>
,	,	kIx,
Super	super	k2eAgNnSc1d1
Nintendo	Nintendo	k1gNnSc1
Entertainment	Entertainment	k1gInSc1
System	Systo	k1gNnSc7
<g/>
,	,	kIx,
Nintendo	Nintendo	k6eAd1
64	#num#	k4
<g/>
,	,	kIx,
Nintendo	Nintendo	k6eAd1
GameCube	GameCub	k1gInSc5
<g/>
,	,	kIx,
Nintendo	Nintendo	k1gNnSc1
Switch	Switch	k1gInSc1
řada	řada	k1gFnSc1
Wii	Wii	k1gFnSc1
</s>
<s>
Wii	Wii	k?
<g/>
,	,	kIx,
Wii	Wii	k1gMnSc5
Remote	Remot	k1gMnSc5
<g/>
,	,	kIx,
WiiConnect	WiiConnect	k1gInSc1
<g/>
24	#num#	k4
<g/>
,	,	kIx,
Virtual	Virtual	k1gInSc1
Console	Console	k1gFnSc2
handheldy	handhelda	k1gFnSc2
</s>
<s>
Game	game	k1gInSc1
Boy	boa	k1gFnSc2
<g/>
,	,	kIx,
Game	game	k1gInSc4
Boy	boa	k1gFnSc2
Color	Color	k1gInSc1
<g/>
,	,	kIx,
Game	game	k1gInSc1
Boy	boa	k1gFnSc2
Advance	Advance	k1gFnSc2
<g/>
,	,	kIx,
Nintendo	Nintendo	k6eAd1
DS	DS	kA
(	(	kIx(
<g/>
Lite	lit	k1gInSc5
<g/>
,	,	kIx,
DSi	DSi	k1gMnPc5
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4518195-0	4518195-0	k4
</s>
