<s>
Kopaný	kopaný	k2eAgInSc1d1
závoz	závoz	k1gInSc1
</s>
<s>
Kopaný	kopaný	k2eAgInSc1d1
závoz	závoz	k1gInSc1
Vrchol	vrchol	k1gInSc1
</s>
<s>
775	#num#	k4
m	m	kA
a	a	k8xC
775,3	775,3	k4
m	m	kA
Poloha	poloha	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Slovensko	Slovensko	k1gNnSc1
Slovensko	Slovensko	k1gNnSc4
Pohoří	pohořet	k5eAaPmIp3nS
</s>
<s>
Krupinská	Krupinský	k2eAgFnSc1d1
planina	planina	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
48	#num#	k4
<g/>
°	°	k?
<g/>
23	#num#	k4
<g/>
′	′	k?
<g/>
4	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
19	#num#	k4
<g/>
°	°	k?
<g/>
12	#num#	k4
<g/>
′	′	k?
<g/>
24	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Kopaný	kopaný	k2eAgInSc1d1
závoz	závoz	k1gInSc1
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Kopaný	kopaný	k2eAgInSc1d1
závoz	závoz	k1gInSc1
(	(	kIx(
<g/>
775	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgInSc4d3
vrchol	vrchol	k1gInSc4
Krupinské	Krupinský	k2eAgFnSc2d1
planiny	planina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
asi	asi	k9
3	#num#	k4
km	km	kA
severovýchodně	severovýchodně	k6eAd1
od	od	k7c2
obce	obec	k1gFnSc2
Senohrad	Senohrad	k1gInSc1
<g/>
,	,	kIx,
na	na	k7c6
území	území	k1gNnSc6
vojenského	vojenský	k2eAgInSc2d1
výcvikového	výcvikový	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
Lešť	Lešť	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tohoto	tento	k3xDgInSc2
důvodu	důvod	k1gInSc2
je	být	k5eAaImIp3nS
veřejnosti	veřejnost	k1gFnSc3
běžně	běžně	k6eAd1
nepřístupný	přístupný	k2eNgInSc1d1
a	a	k8xC
na	na	k7c4
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
vrchol	vrchol	k1gInSc4
nevede	vést	k5eNaImIp3nS
značená	značený	k2eAgFnSc1d1
turistická	turistický	k2eAgFnSc1d1
stezka	stezka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vrchol	vrchol	k1gInSc1
je	být	k5eAaImIp3nS
přístupný	přístupný	k2eAgInSc1d1
lesem	les	k1gInSc7
z	z	k7c2
obce	obec	k1gFnSc2
Senohrad	Senohrada	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velká	velký	k2eAgFnSc1d1
část	část	k1gFnSc1
vrcholu	vrchol	k1gInSc2
je	být	k5eAaImIp3nS
pokrytá	pokrytý	k2eAgNnPc4d1
lesním	lesní	k2eAgInSc7d1
porostem	porost	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Kopaný	kopaný	k2eAgInSc4d1
závoz	závoz	k1gInSc4
na	na	k7c6
slovenské	slovenský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Krupinská	Krupinský	k2eAgFnSc1d1
planina	planina	k1gFnSc1
–	–	k?
najvyšší	najvyššit	k5eAaPmIp3nS,k5eAaImIp3nS
vrch	vrch	k1gInSc1
Kopaný	kopaný	k2eAgInSc1d1
závoz	závoz	k1gInSc1
775	#num#	k4
m.	m.	k?
Zones	Zonesa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
sk	sk	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
slovensky	slovensky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Zdroje	zdroj	k1gInPc1
</s>
<s>
Poloha	poloha	k1gFnSc1
na	na	k7c6
turistické	turistický	k2eAgFnSc6d1
mapě	mapa	k1gFnSc6
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Slovensko	Slovensko	k1gNnSc1
</s>
