<s>
Kopaný	kopaný	k2eAgInSc1d1
závoz	závoz	k1gInSc1
(	(	kIx(
<g/>
775	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgInSc4d3
vrchol	vrchol	k1gInSc4
Krupinské	Krupinský	k2eAgFnSc2d1
planiny	planina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
asi	asi	k9
3	#num#	k4
km	km	kA
severovýchodně	severovýchodně	k6eAd1
od	od	k7c2
obce	obec	k1gFnSc2
Senohrad	Senohrad	k1gInSc1
<g/>
,	,	kIx,
na	na	k7c6
území	území	k1gNnSc6
vojenského	vojenský	k2eAgInSc2d1
výcvikového	výcvikový	k2eAgInSc2d1
prostoru	prostor	k1gInSc2
Lešť	Lešť	k1gFnSc1
<g/>
.	.	kIx.
</s>