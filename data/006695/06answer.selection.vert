<s>
Zmínky	zmínka	k1gFnPc1	zmínka
o	o	k7c6	o
různých	různý	k2eAgInPc6d1	různý
typech	typ	k1gInPc6	typ
rostlin	rostlina	k1gFnPc2	rostlina
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
ve	v	k7c6	v
staroindických	staroindický	k2eAgFnPc6d1	staroindická
védách	véda	k1gFnPc6	véda
<g/>
,	,	kIx,	,
rostlinami	rostlina	k1gFnPc7	rostlina
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
i	i	k9	i
antické	antický	k2eAgNnSc4d1	antické
dílo	dílo	k1gNnSc4	dílo
Historia	Historium	k1gNnSc2	Historium
plantarum	plantarum	k1gInSc1	plantarum
ze	z	k7c2	z
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
<g/>
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
autor	autor	k1gMnSc1	autor
Theofrastos	Theofrastos	k1gMnSc1	Theofrastos
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
otce	otec	k1gMnSc4	otec
botaniky	botanika	k1gFnSc2	botanika
<g/>
.	.	kIx.	.
</s>
