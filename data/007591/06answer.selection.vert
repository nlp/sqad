<s>
Když	když	k8xS	když
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
907	[number]	k4	907
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
pod	pod	k7c7	pod
náporem	nápor	k1gInSc7	nápor
kočovných	kočovný	k2eAgInPc2d1	kočovný
maďarských	maďarský	k2eAgInPc2d1	maďarský
kmenů	kmen	k1gInPc2	kmen
<g/>
,	,	kIx,	,
těžiště	těžiště	k1gNnSc1	těžiště
státního	státní	k2eAgInSc2d1	státní
vývoje	vývoj	k1gInSc2	vývoj
se	se	k3xPyFc4	se
přesunulo	přesunout	k5eAaPmAgNnS	přesunout
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
<g/>
.	.	kIx.	.
</s>
