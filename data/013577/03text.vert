<s>
Odranec	odranec	k1gMnSc1
pravý	pravý	k2eAgMnSc1d1
</s>
<s>
Odranec	odranec	k1gMnSc1
pravý	pravý	k2eAgInSc1d1
Stupeň	stupeň	k1gInSc1
ohrožení	ohrožení	k1gNnSc2
podle	podle	k7c2
IUCN	IUCN	kA
</s>
<s>
málo	málo	k6eAd1
dotčený	dotčený	k2eAgMnSc1d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Vědecká	vědecký	k2eAgFnSc1d1
klasifikace	klasifikace	k1gFnSc1
Říše	říš	k1gFnSc2
</s>
<s>
živočichové	živočich	k1gMnPc1
(	(	kIx(
<g/>
Animalia	Animalia	k1gFnSc1
<g/>
)	)	kIx)
Kmen	kmen	k1gInSc1
</s>
<s>
strunatci	strunatec	k1gMnPc1
(	(	kIx(
<g/>
Chordata	Chordata	k1gFnSc1
<g/>
)	)	kIx)
Podkmen	podkmen	k1gInSc1
</s>
<s>
obratlovci	obratlovec	k1gMnPc1
(	(	kIx(
<g/>
Vertebrata	Vertebrat	k2eAgFnSc1d1
<g/>
)	)	kIx)
Nadtřída	nadtřída	k1gFnSc1
</s>
<s>
ryby	ryba	k1gFnPc1
(	(	kIx(
<g/>
Osteichthyes	Osteichthyes	k1gInSc1
<g/>
)	)	kIx)
Třída	třída	k1gFnSc1
</s>
<s>
paprskoploutví	paprskoploutvit	k5eAaPmIp3nP,k5eAaImIp3nP
(	(	kIx(
<g/>
Actinopterygii	Actinopterygie	k1gFnSc4
<g/>
)	)	kIx)
Nadřád	nadřád	k1gInSc4
</s>
<s>
kostnatí	kostnatět	k5eAaImIp3nS
(	(	kIx(
<g/>
Teleostei	Teleostei	k1gNnSc1
<g/>
)	)	kIx)
Řád	řád	k1gInSc1
</s>
<s>
ropušnicotvární	ropušnicotvárný	k2eAgMnPc1d1
(	(	kIx(
<g/>
Scorpaeniformes	Scorpaeniformes	k1gMnSc1
<g/>
)	)	kIx)
Čeleď	čeleď	k1gFnSc1
</s>
<s>
odrancovití	odrancovitý	k2eAgMnPc1d1
(	(	kIx(
<g/>
Synanceiidae	Synanceiidae	k1gNnSc7
<g/>
)	)	kIx)
Rod	rod	k1gInSc1
</s>
<s>
odranec	odranec	k1gMnSc1
(	(	kIx(
<g/>
Synanceia	Synanceia	k1gFnSc1
)	)	kIx)
Binomické	binomický	k2eAgNnSc1d1
jméno	jméno	k1gNnSc1
</s>
<s>
''	''	k?
<g/>
Synanceia	Synanceia	k1gFnSc1
verrucosa	verrucosa	k1gFnSc1
<g/>
''	''	k?
<g/>
(	(	kIx(
<g/>
Bloch	Bloch	k1gMnSc1
&	&	k?
Schneider	Schneider	k1gMnSc1
<g/>
,	,	kIx,
1801	#num#	k4
<g/>
)	)	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Odranec	odranec	k1gMnSc1
pravý	pravý	k2eAgMnSc1d1
(	(	kIx(
<g/>
Synanceia	Synanceia	k1gFnSc1
verrucosa	verrucosa	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
mořská	mořský	k2eAgFnSc1d1
ryba	ryba	k1gFnSc1
<g/>
,	,	kIx,
žijící	žijící	k2eAgFnSc1d1
v	v	k7c6
mělkých	mělký	k2eAgFnPc6d1
vodách	voda	k1gFnPc6
teplých	teplý	k2eAgNnPc2d1
moří	moře	k1gNnPc2
podél	podél	k7c2
celého	celý	k2eAgInSc2d1
obratníku	obratník	k1gInSc2
Kozoroha	Kozoroh	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
nazýván	nazývat	k5eAaImNgInS
„	„	k?
<g/>
kamennou	kamenný	k2eAgFnSc7d1
rybou	ryba	k1gFnSc7
<g/>
“	“	k?
vzhledem	vzhledem	k7c3
ke	k	k7c3
svému	svůj	k3xOyFgNnSc3
dokonalému	dokonalý	k2eAgNnSc3d1
maskování	maskování	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ryba	ryba	k1gFnSc1
je	být	k5eAaImIp3nS
25	#num#	k4
<g/>
–	–	k?
<g/>
50	#num#	k4
cm	cm	kA
veliká	veliký	k2eAgFnSc1d1
<g/>
,	,	kIx,
nepravidelného	pravidelný	k2eNgInSc2d1
tvaru	tvar	k1gInSc2
<g/>
,	,	kIx,
s	s	k7c7
povrchem	povrch	k1gInSc7
těla	tělo	k1gNnSc2
připomínajícím	připomínající	k2eAgInSc7d1
korál	korál	k1gInSc4
nebo	nebo	k8xC
kámen	kámen	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
vysoce	vysoce	k6eAd1
jedovatá	jedovatý	k2eAgFnSc1d1
<g/>
,	,	kIx,
jedové	jedový	k2eAgFnPc4d1
žlázy	žláza	k1gFnPc4
má	mít	k5eAaImIp3nS
v	v	k7c6
paprscích	paprsek	k1gInPc6
hřbetních	hřbetní	k2eAgFnPc2d1
ploutví	ploutev	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc7
jed	jed	k1gInSc1
je	být	k5eAaImIp3nS
nebezpečný	bezpečný	k2eNgInSc1d1
i	i	k9
pro	pro	k7c4
člověka	člověk	k1gMnSc4
<g/>
,	,	kIx,
ročně	ročně	k6eAd1
zahyne	zahynout	k5eAaPmIp3nS
přes	přes	k7c4
dvacet	dvacet	k4xCc4
plavců	plavec	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
náhodou	náhodou	k6eAd1
šlápnou	šlápnout	k5eAaPmIp3nP
na	na	k7c4
zpola	zpola	k6eAd1
zahrabaného	zahrabaný	k2eAgMnSc4d1
odrance	odranec	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odranec	odranec	k1gMnSc1
je	být	k5eAaImIp3nS
schopen	schopen	k2eAgMnSc1d1
přetrvat	přetrvat	k5eAaPmF
až	až	k9
24	#num#	k4
hodin	hodina	k1gFnPc2
zahrabaný	zahrabaný	k2eAgMnSc1d1
v	v	k7c6
písku	písek	k1gInSc6
na	na	k7c6
pláži	pláž	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Jed	jed	k1gInSc1
je	být	k5eAaImIp3nS
bílkovinné	bílkovinný	k2eAgFnSc2d1
povahy	povaha	k1gFnSc2
<g/>
,	,	kIx,
způsobuje	způsobovat	k5eAaImIp3nS
rozpad	rozpad	k1gInSc1
tkáně	tkáň	k1gFnSc2
<g/>
,	,	kIx,
ochrnutí	ochrnutí	k1gNnSc2
až	až	k8xS
srdeční	srdeční	k2eAgFnSc4d1
zástavu	zástava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proti	proti	k7c3
jedu	jed	k1gInSc3
existuje	existovat	k5eAaImIp3nS
protilátka	protilátka	k1gFnSc1
<g/>
,	,	kIx,
ale	ale	k8xC
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
podána	podat	k5eAaPmNgFnS
do	do	k7c2
několika	několik	k4yIc2
hodin	hodina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Doporučuje	doporučovat	k5eAaImIp3nS
se	se	k3xPyFc4
poraněnou	poraněný	k2eAgFnSc4d1
nohu	noha	k1gFnSc4
ponořit	ponořit	k5eAaPmF
do	do	k7c2
horké	horký	k2eAgFnSc2d1
vody	voda	k1gFnSc2
–	–	k?
jed	jed	k1gInSc1
je	být	k5eAaImIp3nS
inaktivován	inaktivovat	k5eAaBmNgInS
teplem	teplo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Druh	druh	k1gInSc1
byl	být	k5eAaImAgInS
popsán	popsán	k2eAgInSc1d1
roku	rok	k1gInSc2
1801	#num#	k4
(	(	kIx(
<g/>
Bloch	Bloch	k1gMnSc1
and	and	k?
Schneider	Schneider	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
příbuzné	příbuzný	k2eAgInPc1d1
druhy	druh	k1gInPc1
<g/>
:	:	kIx,
</s>
<s>
Synanceia	Synanceia	k1gFnSc1
alula	alula	k1gMnSc1
Eschmeyer	Eschmeyer	k1gMnSc1
and	and	k?
Rama-Rao	Rama-Rao	k1gMnSc1
<g/>
,	,	kIx,
1973	#num#	k4
</s>
<s>
Synanceia	Synanceia	k1gFnSc1
horrida	horrida	k1gFnSc1
(	(	kIx(
<g/>
Linnaeus	Linnaeus	k1gMnSc1
<g/>
,	,	kIx,
1766	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Synanceia	Synanceia	k1gFnSc1
nana	nana	k1gMnSc1
Eschmeyer	Eschmeyer	k1gMnSc1
and	and	k?
Rama-Rao	Rama-Rao	k1gMnSc1
<g/>
,	,	kIx,
1973	#num#	k4
</s>
<s>
Synanceia	Synanceia	k1gFnSc1
platyrhyncha	platyrhyncha	k1gMnSc1
Bleeker	Bleeker	k1gMnSc1
<g/>
,	,	kIx,
1874	#num#	k4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
The	The	k1gFnSc1
IUCN	IUCN	kA
Red	Red	k1gFnSc1
List	list	k1gInSc1
of	of	k?
Threatened	Threatened	k1gInSc1
Species	species	k1gFnSc1
2021.1	2021.1	k4
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2021	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
odranec	odranec	k1gMnSc1
pravý	pravý	k2eAgMnSc1d1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Taxon	taxon	k1gInSc1
Synanceia	Synanceium	k1gNnSc2
verrucosa	verrucosa	k1gFnSc1
ve	v	k7c6
Wikidruzích	Wikidruze	k1gFnPc6
</s>
<s>
Odranec	odranec	k1gMnSc1
pravý	pravý	k2eAgMnSc1d1
<g/>
,	,	kIx,
Velká	velký	k2eAgFnSc1d1
encyklopedie	encyklopedie	k1gFnSc1
akvarijních	akvarijní	k2eAgFnPc2d1
rybiček	rybička	k1gFnPc2
<g/>
,	,	kIx,
Dadka	Dadka	k1gFnSc1
<g/>
21	#num#	k4
<g/>
,	,	kIx,
26	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
</s>
<s>
George	George	k6eAd1
Švehla	Švehla	k1gMnSc1
<g/>
,	,	kIx,
ROZCESTNÍK	rozcestník	k1gInSc1
<g/>
:	:	kIx,
Ryba	ryba	k1gFnSc1
jako	jako	k8xS,k8xC
kámen	kámen	k1gInSc1
<g/>
,	,	kIx,
Zvířetník	zvířetník	k1gInSc1
Neviditelného	viditelný	k2eNgMnSc2d1
psa	pes	k1gMnSc2
<g/>
,	,	kIx,
11	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2007	#num#	k4
</s>
<s>
VIDEO	video	k1gNnSc1
<g/>
:	:	kIx,
Smrtelně	smrtelně	k6eAd1
jedovatou	jedovatý	k2eAgFnSc4d1
rybu-kámen	rybu-kámen	k2eAgMnSc1d1
můžete	moct	k5eAaImIp2nP
potkat	potkat	k5eAaPmF
na	na	k7c6
dovolené	dovolená	k1gFnSc6
i	i	k9
vy	vy	k3xPp2nPc1
<g/>
,	,	kIx,
Hobby	hobby	k1gNnSc1
Idnes	Idnesa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
16	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
,	,	kIx,
mce	mce	k?
(	(	kIx(
<g/>
Martina	Martina	k1gFnSc1
Čermáková	Čermáková	k1gFnSc1
<g/>
)	)	kIx)
</s>
