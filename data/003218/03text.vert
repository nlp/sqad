<s>
Death	Death	k1gInSc1	Death
metal	metal	k1gInSc1	metal
je	být	k5eAaImIp3nS	být
hudební	hudební	k2eAgInSc4d1	hudební
styl	styl	k1gInSc4	styl
vycházející	vycházející	k2eAgInPc4d1	vycházející
z	z	k7c2	z
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
death	death	k1gInSc1	death
metalové	metalový	k2eAgFnSc2d1	metalová
skupiny	skupina	k1gFnSc2	skupina
vznikaly	vznikat	k5eAaImAgFnP	vznikat
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
osmdesátých	osmdesátý	k4xOgNnPc2	osmdesátý
let	léto	k1gNnPc2	léto
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
kapel	kapela	k1gFnPc2	kapela
byli	být	k5eAaImAgMnP	být
američtí	americký	k2eAgMnPc1d1	americký
Death	Death	k1gInSc4	Death
<g/>
.	.	kIx.	.
</s>
<s>
Death	Death	k1gInSc1	Death
metal	metal	k1gInSc1	metal
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
těžké	těžký	k2eAgNnSc1d1	těžké
přesně	přesně	k6eAd1	přesně
definovat	definovat	k5eAaBmF	definovat
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
kapel	kapela	k1gFnPc2	kapela
hrající	hrající	k2eAgInSc1d1	hrající
death	death	k1gInSc1	death
metal	metal	k1gInSc4	metal
je	být	k5eAaImIp3nS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
jinými	jiný	k2eAgInPc7d1	jiný
styly	styl	k1gInPc7	styl
nebo	nebo	k8xC	nebo
hraje	hrát	k5eAaImIp3nS	hrát
přímo	přímo	k6eAd1	přímo
kombinaci	kombinace	k1gFnSc4	kombinace
několika	několik	k4yIc2	několik
stylů	styl	k1gInPc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
obsahovat	obsahovat	k5eAaImF	obsahovat
hrubé	hrubý	k2eAgInPc4d1	hrubý
nebo	nebo	k8xC	nebo
temné	temný	k2eAgInPc4d1	temný
texty	text	k1gInPc4	text
zaměřené	zaměřený	k2eAgInPc4d1	zaměřený
na	na	k7c4	na
smrt	smrt	k1gFnSc4	smrt
jako	jako	k8xC	jako
nihilistickou	nihilistický	k2eAgFnSc4d1	nihilistická
metaforu	metafora	k1gFnSc4	metafora
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
neplatí	platit	k5eNaImIp3nS	platit
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
případech	případ	k1gInPc6	případ
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
na	na	k7c4	na
témata	téma	k1gNnPc4	téma
jako	jako	k8xS	jako
brutalita	brutalita	k1gFnSc1	brutalita
<g/>
,	,	kIx,	,
satanismus	satanismus	k1gInSc1	satanismus
<g/>
,	,	kIx,	,
politika	politika	k1gFnSc1	politika
nebo	nebo	k8xC	nebo
pocity	pocit	k1gInPc4	pocit
beznaděje	beznaděj	k1gFnSc2	beznaděj
<g/>
.	.	kIx.	.
</s>
<s>
Skladby	skladba	k1gFnPc1	skladba
nemají	mít	k5eNaImIp3nP	mít
pevnou	pevný	k2eAgFnSc4d1	pevná
strukturu	struktura	k1gFnSc4	struktura
<g/>
,	,	kIx,	,
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
klade	klást	k5eAaImIp3nS	klást
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
rozvíjení	rozvíjení	k1gNnSc4	rozvíjení
motivů	motiv	k1gInPc2	motiv
<g/>
.	.	kIx.	.
</s>
<s>
Žánr	žánr	k1gInSc1	žánr
se	se	k3xPyFc4	se
často	často	k6eAd1	často
spojuje	spojovat	k5eAaImIp3nS	spojovat
s	s	k7c7	s
podladěnými	podladěný	k2eAgInPc7d1	podladěný
<g/>
,	,	kIx,	,
technicky	technicky	k6eAd1	technicky
náročnými	náročný	k2eAgFnPc7d1	náročná
kytarami	kytara	k1gFnPc7	kytara
<g/>
,	,	kIx,	,
rychlými	rychlý	k2eAgInPc7d1	rychlý
údery	úder	k1gInPc7	úder
<g/>
,	,	kIx,	,
energičností	energičnost	k1gFnSc7	energičnost
a	a	k8xC	a
komplikovanou	komplikovaný	k2eAgFnSc7d1	komplikovaná
rytmikou	rytmika	k1gFnSc7	rytmika
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
větší	veliký	k2eAgFnSc3d2	veliký
zuřivosti	zuřivost	k1gFnSc3	zuřivost
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
neobyčejně	obyčejně	k6eNd1	obyčejně
rychlé	rychlý	k2eAgNnSc4d1	rychlé
bicí	bicí	k2eAgNnSc4d1	bicí
<g/>
.	.	kIx.	.
</s>
<s>
Zpěv	zpěv	k1gInSc1	zpěv
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
bručení	bručení	k1gNnSc2	bručení
<g/>
,	,	kIx,	,
vrčení	vrčení	k1gNnSc2	vrčení
a	a	k8xC	a
blábolení	blábolení	k1gNnSc2	blábolení
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
nazývaného	nazývaný	k2eAgMnSc2d1	nazývaný
"	"	kIx"	"
<g/>
death	death	k1gInSc1	death
grunts	grunts	k1gInSc1	grunts
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
growling	growling	k1gInSc4	growling
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
také	také	k9	také
screaming	screaming	k1gInSc1	screaming
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
mylně	mylně	k6eAd1	mylně
nazýván	nazýván	k2eAgMnSc1d1	nazýván
"	"	kIx"	"
<g/>
hrdelním	hrdelní	k2eAgInSc7d1	hrdelní
zpěvem	zpěv	k1gInSc7	zpěv
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Death	Death	k1gInSc1	Death
metal	metal	k1gInSc1	metal
je	být	k5eAaImIp3nS	být
známý	známý	k2eAgInSc1d1	známý
svým	svůj	k3xOyFgNnSc7	svůj
prudkým	prudký	k2eAgNnSc7d1	prudké
tempem	tempo	k1gNnSc7	tempo
<g/>
,	,	kIx,	,
nepředvídatelnými	předvídatelný	k2eNgFnPc7d1	nepředvídatelná
změnami	změna	k1gFnPc7	změna
ve	v	k7c6	v
struktuře	struktura	k1gFnSc6	struktura
<g/>
,	,	kIx,	,
a	a	k8xC	a
extrémně	extrémně	k6eAd1	extrémně
rychlými	rychlý	k2eAgFnPc7d1	rychlá
a	a	k8xC	a
komplikovanými	komplikovaný	k2eAgFnPc7d1	komplikovaná
kytarami	kytara	k1gFnPc7	kytara
a	a	k8xC	a
bicími	bicí	k2eAgFnPc7d1	bicí
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
některé	některý	k3yIgFnPc1	některý
kapely	kapela	k1gFnPc1	kapela
zaměřují	zaměřovat	k5eAaImIp3nP	zaměřovat
spíš	spíš	k9	spíš
na	na	k7c4	na
rychlost	rychlost	k1gFnSc4	rychlost
a	a	k8xC	a
surovost	surovost	k1gFnSc4	surovost
než	než	k8xS	než
na	na	k7c4	na
techniku	technika	k1gFnSc4	technika
hraní	hraní	k1gNnSc2	hraní
<g/>
.	.	kIx.	.
</s>
<s>
Kapely	kapela	k1gFnPc1	kapela
tohoto	tento	k3xDgInSc2	tento
stylu	styl	k1gInSc2	styl
často	často	k6eAd1	často
využívají	využívat	k5eAaPmIp3nP	využívat
hluboce	hluboko	k6eAd1	hluboko
podladěné	podladěný	k2eAgFnPc1d1	podladěná
kytary	kytara	k1gFnPc1	kytara
a	a	k8xC	a
baskytary	baskytara	k1gFnPc1	baskytara
a	a	k8xC	a
také	také	k9	také
dvojkopákové	dvojkopáková	k1gFnPc4	dvojkopáková
bicí	bicí	k2eAgFnPc4d1	bicí
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
standard	standard	k1gInSc4	standard
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
některé	některý	k3yIgFnPc1	některý
kapely	kapela	k1gFnPc1	kapela
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
jiné	jiný	k2eAgInPc1d1	jiný
nástroje	nástroj	k1gInPc1	nástroj
<g/>
,	,	kIx,	,
např.	např.	kA	např.
klávesy	klávesa	k1gFnSc2	klávesa
a	a	k8xC	a
saxofon	saxofon	k1gInSc4	saxofon
<g/>
.	.	kIx.	.
</s>
<s>
Death	Death	k1gInSc1	Death
metal	metal	k1gInSc1	metal
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
neobyčejně	obyčejně	k6eNd1	obyčejně
fyzicky	fyzicky	k6eAd1	fyzicky
náročný	náročný	k2eAgInSc1d1	náročný
na	na	k7c4	na
muzikanty	muzikant	k1gMnPc4	muzikant
<g/>
,	,	kIx,	,
především	především	k9	především
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
nejkomplikovanější	komplikovaný	k2eAgFnSc6d3	nejkomplikovanější
formě	forma	k1gFnSc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
Deathmetalové	Deathmetal	k1gMnPc1	Deathmetal
texty	text	k1gInPc4	text
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
zaobírají	zaobírat	k5eAaImIp3nP	zaobírat
nihilistickými	nihilistický	k2eAgNnPc7d1	nihilistické
tématy	téma	k1gNnPc7	téma
více	hodně	k6eAd2	hodně
než	než	k8xS	než
ostatní	ostatní	k2eAgFnPc1d1	ostatní
formy	forma	k1gFnPc1	forma
rockové	rockový	k2eAgFnSc2d1	rocková
hudby	hudba	k1gFnSc2	hudba
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
black	black	k6eAd1	black
metalu	metal	k1gInSc2	metal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
využívají	využívat	k5eAaImIp3nP	využívat
"	"	kIx"	"
<g/>
zvrácenosti	zvrácenost	k1gFnPc1	zvrácenost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
poukázaly	poukázat	k5eAaPmAgFnP	poukázat
na	na	k7c6	na
hlubší	hluboký	k2eAgFnSc6d2	hlubší
souvislosti	souvislost	k1gFnSc6	souvislost
<g/>
.	.	kIx.	.
</s>
<s>
Žánr	žánr	k1gInSc1	žánr
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
odnož	odnož	k1gInSc1	odnož
thrash	thrasha	k1gFnPc2	thrasha
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vzdával	vzdávat	k5eAaImAgInS	vzdávat
hold	hold	k1gInSc4	hold
temným	temný	k2eAgFnPc3d1	temná
<g/>
,	,	kIx,	,
principiálním	principiální	k2eAgFnPc3d1	principiální
idejím	idea	k1gFnPc3	idea
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zůstávaly	zůstávat	k5eAaImAgFnP	zůstávat
v	v	k7c6	v
metalu	metal	k1gInSc6	metal
nevyužity	využít	k5eNaPmNgFnP	využít
<g/>
.	.	kIx.	.
</s>
<s>
Zaměření	zaměření	k1gNnSc1	zaměření
se	se	k3xPyFc4	se
na	na	k7c4	na
smrt	smrt	k1gFnSc4	smrt
a	a	k8xC	a
extrémy	extrém	k1gInPc7	extrém
dalo	dát	k5eAaPmAgNnS	dát
podnět	podnět	k1gInSc4	podnět
na	na	k7c4	na
pojmenování	pojmenování	k1gNnSc4	pojmenování
žánru	žánr	k1gInSc2	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
tři	tři	k4xCgFnPc1	tři
teorie	teorie	k1gFnPc1	teorie
o	o	k7c6	o
původu	původ	k1gInSc6	původ
termínu	termín	k1gInSc2	termín
"	"	kIx"	"
<g/>
death	death	k1gInSc1	death
metal	metal	k1gInSc1	metal
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
na	na	k7c4	na
skladbu	skladba	k1gFnSc4	skladba
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Death	Death	k1gInSc1	Death
Metal	metal	k1gInSc1	metal
<g/>
"	"	kIx"	"
od	od	k7c2	od
kapely	kapela	k1gFnSc2	kapela
Possessed	Possessed	k1gMnSc1	Possessed
z	z	k7c2	z
jejich	jejich	k3xOp3gNnSc2	jejich
prvního	první	k4xOgNnSc2	první
alba	album	k1gNnSc2	album
"	"	kIx"	"
<g/>
Seven	Seven	k2eAgInSc1d1	Seven
Churches	Churches	k1gInSc1	Churches
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ale	ale	k9	ale
toho	ten	k3xDgInSc2	ten
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
také	také	k9	také
objevila	objevit	k5eAaPmAgFnS	objevit
skladba	skladba	k1gFnSc1	skladba
"	"	kIx"	"
<g/>
Death	Death	k1gInSc1	Death
Metal	metal	k1gInSc1	metal
<g/>
"	"	kIx"	"
na	na	k7c6	na
albu	album	k1gNnSc6	album
"	"	kIx"	"
<g/>
Power	Power	k1gMnSc1	Power
from	from	k1gMnSc1	from
Hell	Hell	k1gMnSc1	Hell
<g/>
"	"	kIx"	"
od	od	k7c2	od
anglických	anglický	k2eAgInPc2d1	anglický
thrasherů	thrasher	k1gInPc2	thrasher
Onslaught	Onslaughta	k1gFnPc2	Onslaughta
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
demo	demo	k2eAgFnPc1d1	demo
kapely	kapela	k1gFnPc1	kapela
Death	Deatha	k1gFnPc2	Deatha
se	se	k3xPyFc4	se
nazývalo	nazývat	k5eAaImAgNnS	nazývat
"	"	kIx"	"
<g/>
Death	Death	k1gInSc1	Death
by	by	kYmCp3nS	by
Metal	metat	k5eAaImAgInS	metat
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
teorie	teorie	k1gFnSc1	teorie
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
žánr	žánr	k1gInSc1	žánr
byl	být	k5eAaImAgInS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
podle	podle	k7c2	podle
průkopníků	průkopník	k1gMnPc2	průkopník
tohoto	tento	k3xDgInSc2	tento
stylu	styl	k1gInSc2	styl
<g/>
,	,	kIx,	,
Death	Death	k1gInSc1	Death
<g/>
.	.	kIx.	.
</s>
<s>
Říká	říkat	k5eAaImIp3nS	říkat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gInSc1	jejich
strohý	strohý	k2eAgInSc1d1	strohý
název	název	k1gInSc1	název
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgInS	ujmout
jako	jako	k9	jako
jméno	jméno	k1gNnSc4	jméno
pro	pro	k7c4	pro
nový	nový	k2eAgInSc4d1	nový
metalový	metalový	k2eAgInSc4d1	metalový
žánr	žánr	k1gInSc4	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
teorie	teorie	k1gFnSc1	teorie
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
<g/>
,	,	kIx,	,
když	když	k8xS	když
Quorthon	Quorthon	k1gMnSc1	Quorthon
<g/>
,	,	kIx,	,
zesnulý	zesnulý	k1gMnSc1	zesnulý
zakladatel	zakladatel	k1gMnSc1	zakladatel
blackmetalové	blackmetal	k1gMnPc1	blackmetal
skupiny	skupina	k1gFnSc2	skupina
Bathory	Bathora	k1gFnSc2	Bathora
<g/>
,	,	kIx,	,
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
s	s	k7c7	s
britským	britský	k2eAgMnSc7d1	britský
novinářem	novinář	k1gMnSc7	novinář
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
on	on	k3xPp3gMnSc1	on
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
pojem	pojem	k1gInSc4	pojem
"	"	kIx"	"
<g/>
death	death	k1gInSc1	death
metal	metal	k1gInSc1	metal
<g/>
"	"	kIx"	"
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
jeho	jeho	k3xOp3gFnSc1	jeho
kapela	kapela	k1gFnSc1	kapela
nikdy	nikdy	k6eAd1	nikdy
k	k	k7c3	k
death	death	k1gInSc4	death
metalu	metal	k1gInSc2	metal
nepatřila	patřit	k5eNaImAgFnS	patřit
<g/>
.	.	kIx.	.
</s>
<s>
Death	Death	k1gInSc1	Death
metal	metal	k1gInSc1	metal
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
z	z	k7c2	z
thrash	thrasha	k1gFnPc2	thrasha
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
Thrash	Thrash	k1gInSc1	Thrash
metal	metal	k1gInSc1	metal
je	být	k5eAaImIp3nS	být
žánr	žánr	k1gInSc4	žánr
extrémů	extrém	k1gInPc2	extrém
<g/>
:	:	kIx,	:
rychlá	rychlý	k2eAgNnPc1d1	rychlé
tempa	tempo	k1gNnPc1	tempo
<g/>
,	,	kIx,	,
hlučnost	hlučnost	k1gFnSc1	hlučnost
<g/>
,	,	kIx,	,
nestálost	nestálost	k1gFnSc1	nestálost
<g/>
,	,	kIx,	,
řev	řev	k1gInSc1	řev
(	(	kIx(	(
<g/>
screaming	screaming	k1gInSc1	screaming
<g/>
)	)	kIx)	)
a	a	k8xC	a
nepředvídatelné	předvídatelný	k2eNgFnPc1d1	nepředvídatelná
struktury	struktura	k1gFnPc1	struktura
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Thrash-metalové	Thrashetal	k1gMnPc1	Thrash-metal
kapely	kapela	k1gFnSc2	kapela
experimentovaly	experimentovat	k5eAaImAgInP	experimentovat
s	s	k7c7	s
novými	nový	k2eAgFnPc7d1	nová
technikami	technika	k1gFnPc7	technika
a	a	k8xC	a
nápady	nápad	k1gInPc7	nápad
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
posunuly	posunout	k5eAaPmAgFnP	posunout
žánr	žánr	k1gInSc4	žánr
dál	daleko	k6eAd2	daleko
od	od	k7c2	od
komerčního	komerční	k2eAgInSc2d1	komerční
rocku	rock	k1gInSc2	rock
<g/>
.	.	kIx.	.
</s>
<s>
Nejextrémnější	extrémní	k2eAgMnPc1d3	nejextrémnější
thrash-metalové	thrashetal	k1gMnPc1	thrash-metal
kapely	kapela	k1gFnSc2	kapela
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
proto	proto	k8xC	proto
death-metalový	deathetalový	k2eAgInSc4d1	death-metalový
zvuk	zvuk	k1gInSc4	zvuk
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
hrály	hrát	k5eAaImAgInP	hrát
ještě	ještě	k9	ještě
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
<g/>
,	,	kIx,	,
tvrdší	tvrdý	k2eAgInSc1d2	tvrdší
a	a	k8xC	a
temnější	temný	k2eAgInSc1d2	temnější
thrash	thrash	k1gInSc1	thrash
<g/>
.	.	kIx.	.
</s>
<s>
Okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1983	[number]	k4	1983
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
formovat	formovat	k5eAaImF	formovat
agresivní	agresivní	k2eAgFnPc1d1	agresivní
americké	americký	k2eAgFnPc1d1	americká
kapely	kapela	k1gFnPc1	kapela
jako	jako	k8xC	jako
floridští	floridský	k2eAgMnPc1d1	floridský
Death	Death	k1gInSc4	Death
<g/>
,	,	kIx,	,
kalifornští	kalifornský	k2eAgMnPc1d1	kalifornský
Possessed	Possessed	k1gMnSc1	Possessed
a	a	k8xC	a
chicagští	chicagský	k2eAgMnPc1d1	chicagský
Master	master	k1gMnSc1	master
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
jmenovaných	jmenovaný	k2eAgFnPc2d1	jmenovaná
kapel	kapela	k1gFnPc2	kapela
jako	jako	k8xS	jako
první	první	k4xOgMnPc1	první
vydali	vydat	k5eAaPmAgMnP	vydat
ukázku	ukázka	k1gFnSc4	ukázka
"	"	kIx"	"
<g/>
raného	raný	k2eAgMnSc2d1	raný
death	death	k1gMnSc1	death
metalu	metal	k1gInSc2	metal
<g/>
"	"	kIx"	"
Possessed	Possessed	k1gInSc1	Possessed
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
album	album	k1gNnSc1	album
"	"	kIx"	"
<g/>
Seven	Seven	k2eAgInSc1d1	Seven
Churches	Churches	k1gInSc1	Churches
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1985	[number]	k4	1985
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dál	daleko	k6eAd2	daleko
také	také	k9	také
Švýcaři	Švýcar	k1gMnPc1	Švýcar
Messiah	Messiah	k1gMnSc1	Messiah
(	(	kIx(	(
<g/>
album	album	k1gNnSc1	album
"	"	kIx"	"
<g/>
Hymn	Hymn	k1gInSc1	Hymn
to	ten	k3xDgNnSc1	ten
Abramelin	Abramelin	k2eAgMnSc1d1	Abramelin
–	–	k?	–
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kanaďané	Kanaďan	k1gMnPc1	Kanaďan
Slaughter	Slaughtra	k1gFnPc2	Slaughtra
(	(	kIx(	(
<g/>
Strappado	Strappada	k1gFnSc5	Strappada
–	–	k?	–
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
potom	potom	k8xC	potom
Death	Death	k1gInSc4	Death
vydali	vydat	k5eAaPmAgMnP	vydat
"	"	kIx"	"
<g/>
Scream	Scream	k1gInSc1	Scream
Bloody	Blooda	k1gFnSc2	Blooda
Gore	Gor	k1gFnSc2	Gor
<g/>
"	"	kIx"	"
a	a	k8xC	a
Necrophagia	Necrophagia	k1gFnSc1	Necrophagia
"	"	kIx"	"
<g/>
Season	Season	k1gInSc1	Season
of	of	k?	of
the	the	k?	the
Dead	Dead	k1gInSc1	Dead
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Morbid	Morbid	k1gInSc1	Morbid
Angel	Angela	k1gFnPc2	Angela
je	být	k5eAaImIp3nS	být
další	další	k2eAgFnSc1d1	další
kapela	kapela	k1gFnSc1	kapela
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
silně	silně	k6eAd1	silně
ovlivnila	ovlivnit	k5eAaPmAgFnS	ovlivnit
death-metalovou	deathetalův	k2eAgFnSc7d1	death-metalův
scénu	scéna	k1gFnSc4	scéna
početnými	početný	k2eAgFnPc7d1	početná
demo	demo	k2eAgFnPc7d1	demo
nahrávkami	nahrávka	k1gFnPc7	nahrávka
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
"	"	kIx"	"
<g/>
rané	raný	k2eAgInPc1d1	raný
death-metalové	deathetal	k1gMnPc1	death-metal
<g/>
"	"	kIx"	"
kapely	kapela	k1gFnPc1	kapela
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
žánr	žánr	k1gInSc4	žánr
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
podstatně	podstatně	k6eAd1	podstatně
liší	lišit	k5eAaImIp3nS	lišit
od	od	k7c2	od
svého	svůj	k3xOyFgMnSc2	svůj
nejbližšího	blízký	k2eAgMnSc2d3	nejbližší
příbuzného	příbuzný	k1gMnSc2	příbuzný
<g/>
,	,	kIx,	,
thrash	thrash	k1gInSc4	thrash
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgMnS	začít
hrát	hrát	k5eAaImF	hrát
death	death	k1gMnSc1	death
metal	metal	k1gInSc4	metal
ve	v	k7c6	v
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
<g/>
,	,	kIx,	,
obzvlášť	obzvlášť	k6eAd1	obzvlášť
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
vydaly	vydat	k5eAaPmAgFnP	vydat
svoje	svůj	k3xOyFgNnSc1	svůj
první	první	k4xOgNnPc1	první
alba	album	k1gNnPc1	album
kapely	kapela	k1gFnSc2	kapela
jako	jako	k8xS	jako
Nihilist	Nihilist	k1gInSc1	Nihilist
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
Entombed	Entombed	k1gInSc1	Entombed
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
God	God	k1gMnSc5	God
Macabre	Macabr	k1gMnSc5	Macabr
<g/>
,	,	kIx,	,
Carnage	Carnagus	k1gMnSc5	Carnagus
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
Dismember	Dismember	k1gInSc1	Dismember
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Unleashed	Unleashed	k1gInSc1	Unleashed
a	a	k8xC	a
Grave	grave	k1gNnSc1	grave
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
styl	styl	k1gInSc1	styl
se	se	k3xPyFc4	se
zakládal	zakládat	k5eAaImAgInS	zakládat
na	na	k7c6	na
odlišném	odlišný	k2eAgInSc6d1	odlišný
kytarovém	kytarový	k2eAgInSc6d1	kytarový
zvuku	zvuk	k1gInSc6	zvuk
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
nechal	nechat	k5eAaPmAgInS	nechat
inspirovat	inspirovat	k5eAaBmF	inspirovat
zvukem	zvuk	k1gInSc7	zvuk
britských	britský	k2eAgMnPc2d1	britský
grinderů	grinder	k1gMnPc2	grinder
Unseen	Unseen	k2eAgInSc4d1	Unseen
Terror	Terror	k1gInSc4	Terror
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
podíváme	podívat	k5eAaImIp1nP	podívat
z	z	k7c2	z
jiného	jiný	k2eAgInSc2d1	jiný
uhlu	uhel	k1gInSc2	uhel
pohledu	pohled	k1gInSc2	pohled
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
říct	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
moderní	moderní	k2eAgFnSc1d1	moderní
koncepce	koncepce	k1gFnSc1	koncepce
death	death	k1gMnSc1	death
metalu	metal	k1gInSc2	metal
–	–	k?	–
bod	bod	k1gInSc1	bod
zřetelného	zřetelný	k2eAgNnSc2d1	zřetelné
osamostatnění	osamostatnění	k1gNnSc2	osamostatnění
od	od	k7c2	od
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
a	a	k8xC	a
thrash	thrash	k1gMnSc1	thrash
metalu	metal	k1gInSc2	metal
–	–	k?	–
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
rýsovat	rýsovat	k5eAaImF	rýsovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
jako	jako	k8xS	jako
se	se	k3xPyFc4	se
Nová	nový	k2eAgFnSc1d1	nová
vlna	vlna	k1gFnSc1	vlna
britského	britský	k2eAgInSc2d1	britský
heavy	heav	k1gMnPc4	heav
metalu	metal	k1gInSc2	metal
nechala	nechat	k5eAaPmAgFnS	nechat
inspirovat	inspirovat	k5eAaBmF	inspirovat
energií	energie	k1gFnSc7	energie
punk	punk	k1gInSc1	punk
rocku	rock	k1gInSc3	rock
v	v	k7c4	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
metal	metat	k5eAaImAgInS	metat
na	na	k7c6	na
konci	konec	k1gInSc6	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	let	k1gInSc4	let
udělat	udělat	k5eAaPmF	udělat
to	ten	k3xDgNnSc4	ten
samé	samý	k3xTgFnSc3	samý
<g/>
.	.	kIx.	.
</s>
<s>
Chaotický	chaotický	k2eAgMnSc1d1	chaotický
a	a	k8xC	a
často	často	k6eAd1	často
matoucí	matoucí	k2eAgInSc4d1	matoucí
vývoj	vývoj	k1gInSc4	vývoj
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
děl	dít	k5eAaBmAgInS	dít
v	v	k7c6	v
těch	ten	k3xDgFnPc6	ten
dobách	doba	k1gFnPc6	doba
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
nejlépe	dobře	k6eAd3	dobře
vidět	vidět	k5eAaImF	vidět
v	v	k7c6	v
případě	případ	k1gInSc6	případ
britské	britský	k2eAgFnSc2d1	britská
kapely	kapela	k1gFnSc2	kapela
Napalm	napalm	k1gInSc1	napalm
Death	Death	k1gInSc1	Death
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
většinou	většinou	k6eAd1	většinou
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
grindcore	grindcor	k1gInSc5	grindcor
kapelu	kapela	k1gFnSc4	kapela
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kapela	kapela	k1gFnSc1	kapela
současně	současně	k6eAd1	současně
patřila	patřit	k5eAaImAgFnS	patřit
do	do	k7c2	do
hardcore	hardcor	k1gInSc5	hardcor
scény	scéna	k1gFnSc2	scéna
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
Napalm	napalm	k1gInSc1	napalm
Death	Deatha	k1gFnPc2	Deatha
začátkem	začátkem	k7c2	začátkem
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
grindcoru	grindcor	k1gInSc2	grindcor
vzdali	vzdát	k5eAaPmAgMnP	vzdát
a	a	k8xC	a
ubrali	ubrat	k5eAaPmAgMnP	ubrat
se	se	k3xPyFc4	se
jiným	jiný	k2eAgInSc7d1	jiný
směrem	směr	k1gInSc7	směr
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k9	především
na	na	k7c4	na
albu	alba	k1gFnSc4	alba
"	"	kIx"	"
<g/>
Harmony	Harmon	k1gInPc4	Harmon
Corruption	Corruption	k1gInSc1	Corruption
<g/>
"	"	kIx"	"
hráli	hrát	k5eAaImAgMnP	hrát
Napalm	napalm	k1gInSc4	napalm
Death	Deatha	k1gFnPc2	Deatha
něco	něco	k6eAd1	něco
<g/>
,	,	kIx,	,
co	co	k9	co
by	by	kYmCp3nS	by
většina	většina	k1gFnSc1	většina
fanoušků	fanoušek	k1gMnPc2	fanoušek
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nazvala	nazvat	k5eAaPmAgFnS	nazvat
death	death	k1gMnSc1	death
metal	metal	k1gInSc1	metal
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
moderní	moderní	k2eAgInSc1d1	moderní
death	death	k1gInSc1	death
metal	metat	k5eAaImAgInS	metat
<g/>
"	"	kIx"	"
podle	podle	k7c2	podle
uvedené	uvedený	k2eAgFnSc2d1	uvedená
charakteristiky	charakteristika	k1gFnSc2	charakteristika
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
album	album	k1gNnSc1	album
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
agresivní	agresivní	k2eAgNnSc1d1	agresivní
a	a	k8xC	a
značně	značně	k6eAd1	značně
technické	technický	k2eAgInPc1d1	technický
kytarové	kytarový	k2eAgInPc1d1	kytarový
riffy	riff	k1gInPc1	riff
<g/>
,	,	kIx,	,
složité	složitý	k2eAgInPc1d1	složitý
rytmy	rytmus	k1gInPc1	rytmus
<g/>
,	,	kIx,	,
důmyslný	důmyslný	k2eAgInSc1d1	důmyslný
growling	growling	k1gInSc1	growling
Marka	Marek	k1gMnSc2	Marek
"	"	kIx"	"
<g/>
Barneyho	Barney	k1gMnSc2	Barney
<g/>
"	"	kIx"	"
Greenwaya	Greenwayus	k1gMnSc2	Greenwayus
a	a	k8xC	a
hlubokomyslné	hlubokomyslný	k2eAgInPc4d1	hlubokomyslný
texty	text	k1gInPc4	text
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
skupiny	skupina	k1gFnPc4	skupina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
významně	významně	k6eAd1	významně
přispěly	přispět	k5eAaPmAgFnP	přispět
k	k	k7c3	k
tomuto	tento	k3xDgNnSc3	tento
ranému	raný	k2eAgNnSc3d1	rané
hnutí	hnutí	k1gNnSc3	hnutí
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
britští	britský	k2eAgMnPc1d1	britský
Bolt	Bolt	k2eAgMnSc1d1	Bolt
Thrower	Thrower	k1gMnSc1	Thrower
a	a	k8xC	a
Carcass	Carcassa	k1gFnPc2	Carcassa
<g/>
,	,	kIx,	,
američtí	americký	k2eAgMnPc1d1	americký
Cannibal	Cannibal	k1gInSc1	Cannibal
Corpse	corps	k1gInSc5	corps
<g/>
,	,	kIx,	,
Švédi	Švéd	k1gMnPc1	Švéd
Entombed	Entombed	k1gMnSc1	Entombed
<g/>
,	,	kIx,	,
Newyorčané	Newyorčan	k1gMnPc1	Newyorčan
Suffocation	Suffocation	k1gInSc4	Suffocation
<g/>
,	,	kIx,	,
a	a	k8xC	a
zejména	zejména	k9	zejména
Morbid	Morbid	k1gInSc4	Morbid
Angel	Angela	k1gFnPc2	Angela
z	z	k7c2	z
americké	americký	k2eAgFnSc2d1	americká
Floridy	Florida	k1gFnSc2	Florida
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
všemu	všecek	k3xTgNnSc3	všecek
skupina	skupina	k1gFnSc1	skupina
Death	Death	k1gInSc1	Death
přidala	přidat	k5eAaPmAgFnS	přidat
album	album	k1gNnSc4	album
"	"	kIx"	"
<g/>
Human	Human	k1gInSc1	Human
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
příklad	příklad	k1gInSc1	příklad
moderního	moderní	k2eAgNnSc2d1	moderní
death	death	k1gInSc1	death
metalu	metal	k1gInSc6	metal
<g/>
.	.	kIx.	.
</s>
<s>
Otec	otec	k1gMnSc1	otec
kapely	kapela	k1gFnSc2	kapela
Chuck	Chuck	k1gMnSc1	Chuck
Schuldiner	Schuldiner	k1gMnSc1	Schuldiner
nekompromisní	kompromisní	k2eNgFnSc4d1	nekompromisní
rychlost	rychlost	k1gFnSc4	rychlost
a	a	k8xC	a
technickou	technický	k2eAgFnSc4d1	technická
virtuozitu	virtuozita	k1gFnSc4	virtuozita
posunul	posunout	k5eAaPmAgInS	posunout
ještě	ještě	k6eAd1	ještě
dál	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
příklady	příklad	k1gInPc4	příklad
tohoto	tento	k3xDgNnSc2	tento
směrování	směrování	k1gNnSc2	směrování
patří	patřit	k5eAaImIp3nS	patřit
alba	alba	k1gFnSc1	alba
<g/>
:	:	kIx,	:
Carcass	Carcass	k1gInSc1	Carcass
–	–	k?	–
"	"	kIx"	"
<g/>
Necroticism	Necroticism	k1gMnSc1	Necroticism
<g/>
:	:	kIx,	:
Descanting	Descanting	k1gInSc1	Descanting
the	the	k?	the
Insalubrious	Insalubrious	k1gInSc1	Insalubrious
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Suffocation	Suffocation	k1gInSc1	Suffocation
–	–	k?	–
"	"	kIx"	"
<g/>
Human	Human	k1gMnSc1	Human
Waste	Wast	k1gInSc5	Wast
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Entombed	Entombed	k1gMnSc1	Entombed
–	–	k?	–
Clandestine	Clandestin	k1gInSc5	Clandestin
(	(	kIx(	(
<g/>
1992	[number]	k4	1992
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
bodě	bod	k1gInSc6	bod
se	se	k3xPyFc4	se
už	už	k6eAd1	už
zrcadlí	zrcadlit	k5eAaImIp3nP	zrcadlit
všechny	všechen	k3xTgFnPc1	všechen
uvedené	uvedený	k2eAgFnPc1d1	uvedená
charakteristiky	charakteristika	k1gFnPc1	charakteristika
<g/>
:	:	kIx,	:
prudké	prudký	k2eAgNnSc1d1	prudké
tempo	tempo	k1gNnSc1	tempo
a	a	k8xC	a
početné	početný	k2eAgInPc1d1	početný
zvraty	zvrat	k1gInPc1	zvrat
<g/>
,	,	kIx,	,
extrémně	extrémně	k6eAd1	extrémně
rychlé	rychlý	k2eAgInPc1d1	rychlý
bubnovaní	bubnovaný	k2eAgMnPc1d1	bubnovaný
<g/>
,	,	kIx,	,
morbidní	morbidní	k2eAgInPc4d1	morbidní
texty	text	k1gInPc4	text
a	a	k8xC	a
hrdelní	hrdelní	k2eAgInSc4d1	hrdelní
zpěv	zpěv	k1gInSc4	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
80	[number]	k4	80
<g/>
.	.	kIx.	.
a	a	k8xC	a
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
početné	početný	k2eAgFnSc2d1	početná
nahrávací	nahrávací	k2eAgFnSc2d1	nahrávací
společnosti	společnost	k1gFnSc2	společnost
začaly	začít	k5eAaPmAgFnP	začít
podepisovat	podepisovat	k5eAaImF	podepisovat
smlouvy	smlouva	k1gFnPc1	smlouva
s	s	k7c7	s
death-metalovými	deathetalův	k2eAgFnPc7d1	death-metalův
kapelami	kapela	k1gFnPc7	kapela
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
<g/>
.	.	kIx.	.
</s>
<s>
Earache	Earache	k6eAd1	Earache
Records	Records	k1gInSc1	Records
a	a	k8xC	a
Roadrunner	Roadrunner	k1gInSc1	Roadrunner
Records	Recordsa	k1gFnPc2	Recordsa
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
nejvýznamnějšími	významný	k2eAgFnPc7d3	nejvýznamnější
společnostmi	společnost	k1gFnPc7	společnost
žánru	žánr	k1gInSc2	žánr
<g/>
,	,	kIx,	,
s	s	k7c7	s
kapelami	kapela	k1gFnPc7	kapela
jako	jako	k9	jako
Carcass	Carcass	k1gInSc4	Carcass
<g/>
,	,	kIx,	,
Napalm	napalm	k1gInSc4	napalm
Death	Deatha	k1gFnPc2	Deatha
<g/>
,	,	kIx,	,
Morbid	Morbida	k1gFnPc2	Morbida
Angel	Angela	k1gFnPc2	Angela
<g/>
,	,	kIx,	,
Entombed	Entombed	k1gInSc1	Entombed
<g/>
,	,	kIx,	,
Obituary	Obituar	k1gInPc1	Obituar
<g/>
,	,	kIx,	,
Sepultura	Sepultura	k1gFnSc1	Sepultura
<g/>
,	,	kIx,	,
Pestilence	Pestilence	k1gFnSc1	Pestilence
<g/>
,	,	kIx,	,
a	a	k8xC	a
Deicide	Deicid	k1gMnSc5	Deicid
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
tyto	tento	k3xDgFnPc1	tento
společnosti	společnost	k1gFnPc1	společnost
nezačínaly	začínat	k5eNaImAgFnP	začínat
jako	jako	k8xC	jako
death-metalová	deathetalová	k1gFnSc1	death-metalová
vydavatelství	vydavatelství	k1gNnSc2	vydavatelství
(	(	kIx(	(
<g/>
Earache	Earache	k1gInSc1	Earache
byl	být	k5eAaImAgInS	být
grindcore	grindcor	k1gInSc5	grindcor
label	label	k1gMnSc1	label
a	a	k8xC	a
Roadrunner	Roadrunner	k1gMnSc1	Roadrunner
thrashový	thrashový	k2eAgInSc4d1	thrashový
label	label	k1gInSc4	label
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
vlajkovými	vlajkový	k2eAgFnPc7d1	vlajková
loděmi	loď	k1gFnPc7	loď
žánru	žánr	k1gInSc2	žánr
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
toho	ten	k3xDgInSc2	ten
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
nové	nový	k2eAgFnPc1d1	nová
labely	labela	k1gFnPc1	labela
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Nuclear	Nuclear	k1gMnSc1	Nuclear
Blast	Blast	k1gMnSc1	Blast
<g/>
,	,	kIx,	,
Century	Centura	k1gFnPc1	Centura
Media	medium	k1gNnSc2	medium
a	a	k8xC	a
Peaceville	Peaceville	k1gFnSc2	Peaceville
<g/>
;	;	kIx,	;
mnohá	mnohý	k2eAgNnPc4d1	mnohé
z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
vydavatelství	vydavatelství	k1gNnPc2	vydavatelství
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
úspěch	úspěch	k1gInSc4	úspěch
i	i	k9	i
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
metalových	metalový	k2eAgFnPc6d1	metalová
odnožích	odnož	k1gFnPc6	odnož
<g/>
.	.	kIx.	.
</s>
<s>
Popularita	popularita	k1gFnSc1	popularita
death	deatha	k1gFnPc2	deatha
metalu	metal	k1gInSc2	metal
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
vrcholu	vrchol	k1gInSc3	vrchol
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1992	[number]	k4	1992
<g/>
-	-	kIx~	-
<g/>
93	[number]	k4	93
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
některé	některý	k3yIgFnPc1	některý
kapely	kapela	k1gFnPc1	kapela
jako	jako	k8xS	jako
Morbid	Morbid	k1gInSc1	Morbid
Angel	Angela	k1gFnPc2	Angela
těšily	těšit	k5eAaImAgFnP	těšit
komerčnímu	komerční	k2eAgInSc3d1	komerční
úspěchu	úspěch	k1gInSc3	úspěch
<g/>
;	;	kIx,	;
avšak	avšak	k8xC	avšak
žánr	žánr	k1gInSc1	žánr
jako	jako	k8xC	jako
celek	celek	k1gInSc1	celek
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
extrémní	extrémní	k2eAgFnSc3d1	extrémní
povaze	povaha	k1gFnSc3	povaha
nikdy	nikdy	k6eAd1	nikdy
nedostal	dostat	k5eNaPmAgMnS	dostat
do	do	k7c2	do
středního	střední	k2eAgInSc2d1	střední
proudu	proud	k1gInSc2	proud
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
rozvinul	rozvinout	k5eAaPmAgInS	rozvinout
do	do	k7c2	do
následných	následný	k2eAgInPc2d1	následný
stylů	styl	k1gInPc2	styl
<g/>
:	:	kIx,	:
Melodický	melodický	k2eAgInSc1d1	melodický
death	death	k1gInSc1	death
metal	metat	k5eAaImAgInS	metat
<g/>
,	,	kIx,	,
zkráceně	zkráceně	k6eAd1	zkráceně
také	také	k9	také
melodeath	melodeath	k1gInSc1	melodeath
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
extrémní	extrémní	k2eAgFnSc1d1	extrémní
forma	forma	k1gFnSc1	forma
death	deatha	k1gFnPc2	deatha
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
Skladby	skladba	k1gFnPc1	skladba
se	se	k3xPyFc4	se
zakládají	zakládat	k5eAaImIp3nP	zakládat
na	na	k7c6	na
melodice	melodika	k1gFnSc6	melodika
kytarové	kytarový	k2eAgFnSc2d1	kytarová
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
převzaté	převzatý	k2eAgFnSc2d1	převzatá
od	od	k7c2	od
Iron	iron	k1gInSc4	iron
Maiden	Maidna	k1gFnPc2	Maidna
a	a	k8xC	a
growlingu	growling	k1gInSc2	growling
ve	v	k7c6	v
vysokých	vysoký	k2eAgInPc6d1	vysoký
tónech	tón	k1gInPc6	tón
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
album	album	k1gNnSc1	album
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
stylu	styl	k1gInSc6	styl
(	(	kIx(	(
<g/>
Heartwork	Heartwork	k1gInSc1	Heartwork
–	–	k?	–
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
někdy	někdy	k6eAd1	někdy
připisováno	připisovat	k5eAaImNgNnS	připisovat
grindcore	grindcor	k1gInSc5	grindcor
kapele	kapela	k1gFnSc3	kapela
Carcass	Carcass	k1gInSc4	Carcass
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejznámější	známý	k2eAgMnPc4d3	nejznámější
představitele	představitel	k1gMnPc4	představitel
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
Hypocrisy	Hypocris	k1gInPc4	Hypocris
<g/>
,	,	kIx,	,
Amon	Amon	k1gMnSc1	Amon
Amarth	Amarth	k1gMnSc1	Amarth
<g/>
,	,	kIx,	,
In	In	k1gMnSc1	In
Flames	Flames	k1gMnSc1	Flames
<g/>
,	,	kIx,	,
Arch	arch	k1gInSc1	arch
Enemy	Enema	k1gFnSc2	Enema
<g/>
,	,	kIx,	,
Dark	Dark	k1gMnSc1	Dark
Tranquillity	Tranquillita	k1gFnSc2	Tranquillita
a	a	k8xC	a
At	At	k1gFnSc2	At
the	the	k?	the
Gates	Gates	k1gMnSc1	Gates
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
průkopníky	průkopník	k1gMnPc4	průkopník
stylu	styl	k1gInSc2	styl
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
Gothenburského	Gothenburský	k2eAgInSc2d1	Gothenburský
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
Skandinávský	skandinávský	k2eAgMnSc1d1	skandinávský
death	death	k1gMnSc1	death
metal	metat	k5eAaImAgMnS	metat
je	on	k3xPp3gNnSc4	on
předchůdce	předchůdce	k1gMnSc1	předchůdce
melodického	melodický	k2eAgInSc2d1	melodický
death	death	k1gInSc1	death
metalu	metal	k1gInSc6	metal
<g/>
.	.	kIx.	.
</s>
<s>
Skupiny	skupina	k1gFnPc1	skupina
jako	jako	k8xS	jako
Entombed	Entombed	k1gMnSc1	Entombed
<g/>
,	,	kIx,	,
Dismember	Dismember	k1gMnSc1	Dismember
<g/>
,	,	kIx,	,
Unleashed	Unleashed	k1gMnSc1	Unleashed
a	a	k8xC	a
výše	výše	k1gFnSc1	výše
uvedení	uvedení	k1gNnSc2	uvedení
At	At	k1gFnSc2	At
the	the	k?	the
Gates	Gates	k1gInSc1	Gates
pomohly	pomoct	k5eAaPmAgInP	pomoct
definovat	definovat	k5eAaBmF	definovat
zvuk	zvuk	k1gInSc4	zvuk
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgInSc2	který
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgMnS	vyvinout
melodický	melodický	k2eAgMnSc1d1	melodický
death	death	k1gMnSc1	death
metal	metat	k5eAaImAgMnS	metat
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pojem	pojem	k1gInSc1	pojem
je	být	k5eAaImIp3nS	být
běžně	běžně	k6eAd1	běžně
zaměnitelný	zaměnitelný	k2eAgInSc1d1	zaměnitelný
s	s	k7c7	s
melodickým	melodický	k2eAgInSc7d1	melodický
death	death	k1gInSc1	death
metalem	metal	k1gInSc7	metal
<g/>
.	.	kIx.	.
</s>
<s>
Entombed	Entombed	k1gInSc1	Entombed
(	(	kIx(	(
<g/>
ex-Nihilist	ex-Nihilist	k1gInSc1	ex-Nihilist
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kapela	kapela	k1gFnSc1	kapela
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
začala	začít	k5eAaPmAgFnS	začít
kombinovat	kombinovat	k5eAaImF	kombinovat
punkové	punkový	k2eAgFnSc3d1	punková
a	a	k8xC	a
death	death	k1gInSc1	death
<g/>
/	/	kIx~	/
<g/>
thrashové	thrashový	k2eAgInPc1d1	thrashový
riffy	riff	k1gInPc1	riff
<g/>
;	;	kIx,	;
též	též	k9	též
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
nový	nový	k2eAgInSc4d1	nový
kytarový	kytarový	k2eAgInSc4d1	kytarový
zvuk	zvuk	k1gInSc4	zvuk
–	–	k?	–
využitím	využití	k1gNnSc7	využití
efektu	efekt	k1gInSc2	efekt
kytarového	kytarový	k2eAgInSc2d1	kytarový
pedálu	pedál	k1gInSc2	pedál
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
surový	surový	k2eAgMnSc1d1	surový
<g/>
,	,	kIx,	,
mechanický	mechanický	k2eAgInSc1d1	mechanický
<g/>
,	,	kIx,	,
elektrický	elektrický	k2eAgInSc1d1	elektrický
bzukot	bzukot	k1gInSc1	bzukot
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
pokusily	pokusit	k5eAaPmAgFnP	pokusit
mnohé	mnohý	k2eAgFnPc1d1	mnohá
kapely	kapela	k1gFnPc1	kapela
reprodukovat	reprodukovat	k5eAaBmF	reprodukovat
<g/>
.	.	kIx.	.
</s>
<s>
Takovýto	takovýto	k3xDgInSc1	takovýto
zvuk	zvuk	k1gInSc1	zvuk
má	mít	k5eAaImIp3nS	mít
kořeny	kořen	k1gInPc4	kořen
u	u	k7c2	u
britské	britský	k2eAgFnSc2d1	britská
deathgrindové	deathgrindový	k2eAgFnSc2d1	deathgrindový
kapely	kapela	k1gFnSc2	kapela
Unseen	Unseen	k2eAgInSc1d1	Unseen
Terror	Terror	k1gInSc1	Terror
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
alba	alba	k1gFnSc1	alba
"	"	kIx"	"
<g/>
Human	Human	k1gMnSc1	Human
Error	Error	k1gMnSc1	Error
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Floridský	floridský	k2eAgInSc1d1	floridský
death	death	k1gInSc1	death
metal	metal	k1gInSc1	metal
je	být	k5eAaImIp3nS	být
konzervativnější	konzervativní	k2eAgInSc1d2	konzervativnější
a	a	k8xC	a
útočnější	útoční	k2eAgInSc1d2	útoční
než	než	k8xS	než
jeho	jeho	k3xOp3gFnSc1	jeho
skandinávská	skandinávský	k2eAgFnSc1d1	skandinávská
verze	verze	k1gFnSc1	verze
<g/>
,	,	kIx,	,
důkladnější	důkladný	k2eAgInSc1d2	důkladnější
<g/>
,	,	kIx,	,
vybroušený	vybroušený	k2eAgInSc1d1	vybroušený
a	a	k8xC	a
tradiční	tradiční	k2eAgInSc1d1	tradiční
<g/>
;	;	kIx,	;
v	v	k7c6	v
mnohých	mnohý	k2eAgInPc6d1	mnohý
ohledech	ohled	k1gInPc6	ohled
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
tento	tento	k3xDgInSc4	tento
styl	styl	k1gInSc4	styl
považovat	považovat	k5eAaImF	považovat
<g/>
,	,	kIx,	,
minimálně	minimálně	k6eAd1	minimálně
v	v	k7c6	v
prvních	první	k4xOgNnPc6	první
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
za	za	k7c4	za
brutálnější	brutální	k2eAgInSc4d2	brutálnější
a	a	k8xC	a
komplikovanější	komplikovaný	k2eAgInSc4d2	komplikovanější
thrash	thrash	k1gInSc4	thrash
metal	metat	k5eAaImAgMnS	metat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
přímočařejší	přímočarý	k2eAgFnSc1d2	přímočařejší
a	a	k8xC	a
brutálnější	brutální	k2eAgFnSc1d2	brutálnější
než	než	k8xS	než
"	"	kIx"	"
<g/>
technický	technický	k2eAgInSc1d1	technický
death	death	k1gInSc1	death
metal	metal	k1gInSc1	metal
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
stylu	styl	k1gInSc3	styl
se	se	k3xPyFc4	se
zařazují	zařazovat	k5eAaImIp3nP	zařazovat
kapely	kapela	k1gFnPc1	kapela
jako	jako	k8xS	jako
Deicide	Deicid	k1gInSc5	Deicid
<g/>
,	,	kIx,	,
Resurrection	Resurrection	k1gInSc1	Resurrection
<g/>
,	,	kIx,	,
Malevolent	Malevolent	k1gInSc1	Malevolent
Creation	Creation	k1gInSc1	Creation
<g/>
,	,	kIx,	,
Monstrosity	monstrosita	k1gFnPc1	monstrosita
<g/>
,	,	kIx,	,
Obituary	Obituar	k1gInPc1	Obituar
<g/>
,	,	kIx,	,
Brutality	brutalita	k1gFnPc1	brutalita
<g/>
,	,	kIx,	,
Morbid	Morbid	k1gInSc1	Morbid
Angel	Angela	k1gFnPc2	Angela
a	a	k8xC	a
Death	Deatha	k1gFnPc2	Deatha
(	(	kIx(	(
<g/>
i	i	k9	i
když	když	k8xS	když
poslední	poslední	k2eAgFnPc1d1	poslední
nahrávky	nahrávka	k1gFnPc1	nahrávka
Death	Deatha	k1gFnPc2	Deatha
spadají	spadat	k5eAaPmIp3nP	spadat
i	i	k9	i
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
"	"	kIx"	"
<g/>
progresivní	progresivní	k2eAgInSc1d1	progresivní
death	death	k1gInSc1	death
metal	metat	k5eAaImAgInS	metat
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Technický	technický	k2eAgInSc1d1	technický
death	death	k1gInSc1	death
metal	metat	k5eAaImAgInS	metat
<g/>
.	.	kIx.	.
</s>
<s>
Technical	Technicat	k5eAaPmAgInS	Technicat
death	death	k1gInSc1	death
metal	metal	k1gInSc1	metal
<g/>
,	,	kIx,	,
omezený	omezený	k2eAgInSc1d1	omezený
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
význačný	význačný	k2eAgInSc4d1	význačný
žánr	žánr	k1gInSc4	žánr
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
spojuje	spojovat	k5eAaImIp3nS	spojovat
s	s	k7c7	s
kapelami	kapela	k1gFnPc7	kapela
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
obzvlášť	obzvlášť	k6eAd1	obzvlášť
vynikají	vynikat	k5eAaImIp3nP	vynikat
virtuozitou	virtuozita	k1gFnSc7	virtuozita
hráčů	hráč	k1gMnPc2	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Hudebně	hudebně	k6eAd1	hudebně
ho	on	k3xPp3gNnSc4	on
charakterizují	charakterizovat	k5eAaBmIp3nP	charakterizovat
chaotické	chaotický	k2eAgInPc1d1	chaotický
riffy	riff	k1gInPc1	riff
<g/>
,	,	kIx,	,
atypické	atypický	k2eAgInPc1d1	atypický
rytmy	rytmus	k1gInPc1	rytmus
<g/>
,	,	kIx,	,
vypracovaná	vypracovaný	k2eAgFnSc1d1	vypracovaná
produkce	produkce	k1gFnSc1	produkce
a	a	k8xC	a
nadčasovost	nadčasovost	k1gFnSc1	nadčasovost
<g/>
.	.	kIx.	.
</s>
<s>
Běžně	běžně	k6eAd1	běžně
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
řadí	řadit	k5eAaImIp3nP	řadit
kapely	kapela	k1gFnPc1	kapela
jako	jako	k8xC	jako
Amoral	Amoral	k1gMnSc1	Amoral
<g/>
,	,	kIx,	,
Atheist	Atheist	k1gMnSc1	Atheist
<g/>
,	,	kIx,	,
Cynic	Cynic	k1gMnSc1	Cynic
<g/>
,	,	kIx,	,
Sadist	Sadist	k1gMnSc1	Sadist
<g/>
,	,	kIx,	,
Gorguts	Gorguts	k1gInSc1	Gorguts
<g/>
,	,	kIx,	,
Immolation	Immolation	k1gInSc1	Immolation
<g/>
,	,	kIx,	,
Necrophagist	Necrophagist	k1gMnSc1	Necrophagist
<g/>
,	,	kIx,	,
Nocturnus	Nocturnus	k1gMnSc1	Nocturnus
<g/>
,	,	kIx,	,
Atheist	Atheist	k1gMnSc1	Atheist
<g/>
,	,	kIx,	,
Decapitated	Decapitated	k1gMnSc1	Decapitated
<g/>
,	,	kIx,	,
Cryptopsy	Cryptopsa	k1gFnPc1	Cryptopsa
<g/>
,	,	kIx,	,
Spawn	Spawn	k1gInSc1	Spawn
of	of	k?	of
Possession	Possession	k1gInSc1	Possession
<g/>
,	,	kIx,	,
Nile	Nil	k1gInSc5	Nil
<g/>
,	,	kIx,	,
Origin	Origin	k1gMnSc1	Origin
<g/>
,	,	kIx,	,
Dying	Dying	k1gMnSc1	Dying
Fetus	Fetus	k1gMnSc1	Fetus
<g/>
,	,	kIx,	,
Philosopher	Philosophra	k1gFnPc2	Philosophra
a	a	k8xC	a
Psycroptic	Psycroptice	k1gFnPc2	Psycroptice
<g/>
,	,	kIx,	,
Obscura	Obscura	k1gFnSc1	Obscura
<g/>
.	.	kIx.	.
</s>
<s>
Progresivní	progresivní	k2eAgInSc1d1	progresivní
death	death	k1gInSc1	death
metal	metal	k1gInSc1	metal
je	být	k5eAaImIp3nS	být
styl	styl	k1gInSc4	styl
death	deatha	k1gFnPc2	deatha
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
z	z	k7c2	z
progresivního	progresivní	k2eAgInSc2d1	progresivní
metalu	metal	k1gInSc2	metal
převzal	převzít	k5eAaPmAgMnS	převzít
nadčasovost	nadčasovost	k1gFnSc4	nadčasovost
a	a	k8xC	a
kolísavost	kolísavost	k1gFnSc4	kolísavost
nálad	nálada	k1gFnPc2	nálada
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc4	některý
znaky	znak	k1gInPc4	znak
těchto	tento	k3xDgInPc2	tento
žánrů	žánr	k1gInPc2	žánr
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
najít	najít	k5eAaPmF	najít
dokonce	dokonce	k9	dokonce
u	u	k7c2	u
obou	dva	k4xCgMnPc2	dva
<g/>
.	.	kIx.	.
</s>
<s>
Typické	typický	k2eAgInPc1d1	typický
znaky	znak	k1gInPc1	znak
death	deatha	k1gFnPc2	deatha
metalu	metal	k1gInSc2	metal
(	(	kIx(	(
<g/>
growling	growling	k1gInSc1	growling
<g/>
,	,	kIx,	,
rychlé	rychlý	k2eAgFnPc1d1	rychlá
bicí	bicí	k2eAgFnPc1d1	bicí
<g/>
,	,	kIx,	,
brutalita	brutalita	k1gFnSc1	brutalita
<g/>
)	)	kIx)	)
a	a	k8xC	a
progresivního	progresivní	k2eAgInSc2d1	progresivní
metalu	metal	k1gInSc2	metal
(	(	kIx(	(
<g/>
nestandardní	standardní	k2eNgFnSc1d1	nestandardní
délka	délka	k1gFnSc1	délka
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
netradiční	tradiční	k2eNgInPc4d1	netradiční
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
akustické	akustický	k2eAgFnPc4d1	akustická
části	část	k1gFnPc4	část
a	a	k8xC	a
pod	pod	k7c4	pod
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Žánr	žánr	k1gInSc4	žánr
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
kapely	kapela	k1gFnPc1	kapela
jako	jako	k8xS	jako
Amorphis	Amorphis	k1gFnPc1	Amorphis
<g/>
,	,	kIx,	,
Opeth	Opeth	k1gMnSc1	Opeth
<g/>
,	,	kIx,	,
Cynic	Cynic	k1gMnSc1	Cynic
<g/>
,	,	kIx,	,
Novembre	Novembr	k1gMnSc5	Novembr
<g/>
,	,	kIx,	,
Coprofago	Coprofago	k6eAd1	Coprofago
<g/>
,	,	kIx,	,
Death	Death	k1gInSc1	Death
<g/>
,	,	kIx,	,
Pestilence	Pestilence	k1gFnSc1	Pestilence
(	(	kIx(	(
<g/>
poslední	poslední	k2eAgNnSc1d1	poslední
album	album	k1gNnSc1	album
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nocturnus	Nocturnus	k1gMnSc1	Nocturnus
a	a	k8xC	a
Atheist	Atheist	k1gMnSc1	Atheist
<g/>
.	.	kIx.	.
</s>
<s>
Žánr	žánr	k1gInSc1	žánr
je	být	k5eAaImIp3nS	být
úzce	úzko	k6eAd1	úzko
spjatý	spjatý	k2eAgInSc1d1	spjatý
s	s	k7c7	s
technickým	technický	k2eAgInSc7d1	technický
death	death	k1gInSc1	death
metalem	metal	k1gInSc7	metal
a	a	k8xC	a
mnohé	mnohý	k2eAgFnPc1d1	mnohá
kapely	kapela	k1gFnPc1	kapela
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
obou	dva	k4xCgInPc2	dva
stylů	styl	k1gInPc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Brutální	brutální	k2eAgInSc1d1	brutální
death	death	k1gInSc1	death
metal	metal	k1gInSc1	metal
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
kombinací	kombinace	k1gFnSc7	kombinace
grindcore	grindcor	k1gInSc5	grindcor
a	a	k8xC	a
death	death	k1gInSc4	death
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neměl	mít	k5eNaImAgMnS	mít
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
zaměňovat	zaměňovat	k5eAaImF	zaměňovat
s	s	k7c7	s
deathgrindem	deathgrind	k1gInSc7	deathgrind
(	(	kIx(	(
<g/>
kombinace	kombinace	k1gFnSc1	kombinace
death	deatha	k1gFnPc2	deatha
metalu	metal	k1gInSc2	metal
a	a	k8xC	a
grindcore	grindcor	k1gInSc5	grindcor
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
50	[number]	k4	50
<g/>
:	:	kIx,	:
<g/>
50	[number]	k4	50
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nemá	mít	k5eNaImIp3nS	mít
nic	nic	k3yNnSc1	nic
nebo	nebo	k8xC	nebo
skoro	skoro	k6eAd1	skoro
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
s	s	k7c7	s
hardcore	hardcor	k1gInSc5	hardcor
punkovými	punkový	k2eAgInPc7d1	punkový
postoji	postoj	k1gInPc7	postoj
a	a	k8xC	a
estetikou	estetika	k1gFnSc7	estetika
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
tohoto	tento	k3xDgInSc2	tento
stylu	styl	k1gInSc2	styl
je	být	k5eAaImIp3nS	být
důležitá	důležitý	k2eAgFnSc1d1	důležitá
neotesanost	neotesanost	k1gFnSc1	neotesanost
<g/>
,	,	kIx,	,
hrubost	hrubost	k1gFnSc1	hrubost
a	a	k8xC	a
brutalita	brutalita	k1gFnSc1	brutalita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
také	také	k9	také
technická	technický	k2eAgFnSc1d1	technická
zručnost	zručnost	k1gFnSc1	zručnost
<g/>
.	.	kIx.	.
</s>
<s>
Brutální	brutální	k2eAgInSc1d1	brutální
death	death	k1gInSc1	death
metal	metal	k1gInSc1	metal
se	se	k3xPyFc4	se
spojuje	spojovat	k5eAaImIp3nS	spojovat
se	s	k7c7	s
skupinami	skupina	k1gFnPc7	skupina
jako	jako	k9	jako
Suffocation	Suffocation	k1gInSc4	Suffocation
<g/>
,	,	kIx,	,
Cannibal	Cannibal	k1gInSc4	Cannibal
Corpse	corps	k1gInSc5	corps
<g/>
,	,	kIx,	,
Nile	Nil	k1gInSc5	Nil
<g/>
,	,	kIx,	,
Iniquity	Iniquita	k1gMnPc4	Iniquita
<g/>
,	,	kIx,	,
Cryptopsy	Cryptops	k1gMnPc4	Cryptops
<g/>
,	,	kIx,	,
Disgorge	Disgorg	k1gMnPc4	Disgorg
<g/>
,	,	kIx,	,
Devourment	Devourment	k1gMnSc1	Devourment
<g/>
,	,	kIx,	,
Krisiun	Krisiun	k1gMnSc1	Krisiun
<g/>
,	,	kIx,	,
Disavowed	Disavowed	k1gMnSc1	Disavowed
a	a	k8xC	a
Deeds	Deeds	k1gInSc1	Deeds
of	of	k?	of
Flesh	Flesh	k1gInSc1	Flesh
<g/>
.	.	kIx.	.
</s>
<s>
Growling	Growling	k1gInSc1	Growling
je	být	k5eAaImIp3nS	být
většinou	většina	k1gFnSc7	většina
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
nízkých	nízký	k2eAgInPc6d1	nízký
tónech	tón	k1gInPc6	tón
a	a	k8xC	a
v	v	k7c6	v
textech	text	k1gInPc6	text
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
plno	plno	k1gNnSc1	plno
krve	krev	k1gFnSc2	krev
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
kytary	kytara	k1gFnPc1	kytara
hrají	hrát	k5eAaImIp3nP	hrát
většinou	většina	k1gFnSc7	většina
živelné	živelný	k2eAgInPc1d1	živelný
<g/>
,	,	kIx,	,
hyper	hyper	k2eAgInPc1d1	hyper
rychlé	rychlý	k2eAgInPc1d1	rychlý
a	a	k8xC	a
hluboce	hluboko	k6eAd1	hluboko
podladěné	podladěný	k2eAgFnPc4d1	podladěná
melodie	melodie	k1gFnPc4	melodie
<g/>
.	.	kIx.	.
</s>
<s>
Death	Death	k1gInSc1	Death
<g/>
/	/	kIx~	/
<g/>
doom	doom	k1gInSc1	doom
je	být	k5eAaImIp3nS	být
pomalý	pomalý	k2eAgInSc1d1	pomalý
a	a	k8xC	a
depresivní	depresivní	k2eAgInSc1d1	depresivní
styl	styl	k1gInSc1	styl
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nechal	nechat	k5eAaPmAgInS	nechat
inspirovat	inspirovat	k5eAaBmF	inspirovat
klasickým	klasický	k2eAgInSc7d1	klasický
doom	doom	k1gInSc1	doom
metalem	metal	k1gInSc7	metal
<g/>
.	.	kIx.	.
</s>
<s>
Styl	styl	k1gInSc1	styl
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
kapely	kapela	k1gFnPc1	kapela
jako	jako	k8xS	jako
Sempiternal	Sempiternal	k1gFnPc1	Sempiternal
Deathreign	Deathreign	k1gInSc1	Deathreign
<g/>
,	,	kIx,	,
Dream	Dream	k1gInSc1	Dream
Death	Death	k1gInSc1	Death
<g/>
,	,	kIx,	,
Delirium	delirium	k1gNnSc1	delirium
<g/>
,	,	kIx,	,
Asphyx	Asphyx	k1gInSc1	Asphyx
<g/>
,	,	kIx,	,
My	my	k3xPp1nPc1	my
Dying	Dying	k1gInSc1	Dying
Bride	Brid	k1gInSc5	Brid
<g/>
,	,	kIx,	,
Paradise	Paradise	k1gFnSc1	Paradise
Lost	Lost	k1gMnSc1	Lost
(	(	kIx(	(
<g/>
raná	raný	k2eAgFnSc1d1	raná
tvorba	tvorba	k1gFnSc1	tvorba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Anathema	anathema	k1gNnSc1	anathema
<g/>
,	,	kIx,	,
Winter	Winter	k1gMnSc1	Winter
a	a	k8xC	a
Disembowelment	Disembowelment	k1gMnSc1	Disembowelment
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
tradiční	tradiční	k2eAgInSc1d1	tradiční
doom	doom	k1gInSc1	doom
metal	metal	k1gInSc1	metal
se	se	k3xPyFc4	se
opírá	opírat	k5eAaImIp3nS	opírat
o	o	k7c4	o
pomalé	pomalý	k2eAgNnSc4d1	pomalé
tempo	tempo	k1gNnSc4	tempo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tak	tak	k6eAd1	tak
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
depresivní	depresivní	k2eAgFnSc4d1	depresivní
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
,	,	kIx,	,
death	death	k1gInSc4	death
<g/>
/	/	kIx~	/
<g/>
doom	doom	k6eAd1	doom
je	být	k5eAaImIp3nS	být
o	o	k7c4	o
něco	něco	k3yInSc4	něco
rychlejší	rychlý	k2eAgMnSc1d2	rychlejší
a	a	k8xC	a
klade	klást	k5eAaImIp3nS	klást
důraz	důraz	k1gInSc1	důraz
na	na	k7c4	na
melodie	melodie	k1gFnPc4	melodie
v	v	k7c6	v
mollové	mollový	k2eAgFnSc6d1	mollová
stupnici	stupnice	k1gFnSc6	stupnice
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
tak	tak	k6eAd1	tak
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
podobnou	podobný	k2eAgFnSc4d1	podobná
atmosféru	atmosféra	k1gFnSc4	atmosféra
<g/>
,	,	kIx,	,
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
využívá	využívat	k5eAaImIp3nS	využívat
kombinaci	kombinace	k1gFnSc4	kombinace
growlingu	growling	k1gInSc2	growling
a	a	k8xC	a
zpěvu	zpěv	k1gInSc2	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Blackened	Blackened	k1gMnSc1	Blackened
death	death	k1gMnSc1	death
metal	metal	k1gInSc4	metal
je	být	k5eAaImIp3nS	být
odnož	odnož	k1gInSc1	odnož
death	deatha	k1gFnPc2	deatha
metalu	metal	k1gInSc2	metal
spojená	spojený	k2eAgFnSc1d1	spojená
s	s	k7c7	s
celistvými	celistvý	k2eAgInPc7d1	celistvý
a	a	k8xC	a
melodickými	melodický	k2eAgInPc7d1	melodický
prvky	prvek	k1gInPc7	prvek
black	black	k6eAd1	black
metalu	metal	k1gInSc2	metal
<g/>
.	.	kIx.	.
</s>
<s>
Kapely	kapela	k1gFnPc1	kapela
tohoto	tento	k3xDgInSc2	tento
stylu	styl	k1gInSc2	styl
také	také	k6eAd1	také
převzaly	převzít	k5eAaPmAgInP	převzít
některé	některý	k3yIgInPc1	některý
náměty	námět	k1gInPc1	námět
black	blacka	k1gFnPc2	blacka
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	být	k5eAaImIp3nS	být
zlo	zlo	k1gNnSc1	zlo
<g/>
,	,	kIx,	,
satanismus	satanismus	k1gInSc1	satanismus
a	a	k8xC	a
okultismus	okultismus	k1gInSc1	okultismus
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
jsou	být	k5eAaImIp3nP	být
Necrophobic	Necrophobice	k1gFnPc2	Necrophobice
<g/>
,	,	kIx,	,
Goatwhore	Goatwhor	k1gMnSc5	Goatwhor
<g/>
,	,	kIx,	,
Behemoth	Behemoth	k1gInSc1	Behemoth
<g/>
,	,	kIx,	,
Belphegor	Belphegor	k1gInSc1	Belphegor
<g/>
,	,	kIx,	,
Zyklon	Zyklon	k1gInSc1	Zyklon
a	a	k8xC	a
Dissection	Dissection	k1gInSc1	Dissection
<g/>
.	.	kIx.	.
</s>
<s>
Blackened	Blackened	k1gMnSc1	Blackened
death	death	k1gMnSc1	death
metal	metal	k1gInSc4	metal
je	být	k5eAaImIp3nS	být
spíš	spíš	k9	spíš
black	black	k6eAd1	black
metal	metat	k5eAaImAgMnS	metat
než	než	k8xS	než
death	death	k1gMnSc1	death
metal	metat	k5eAaImAgMnS	metat
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
podle	podle	k7c2	podle
názvu	název	k1gInSc2	název
by	by	kYmCp3nP	by
to	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
opačně	opačně	k6eAd1	opačně
<g/>
;	;	kIx,	;
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mnohé	mnohé	k1gNnSc1	mnohé
black-metalové	blacketal	k1gMnPc1	black-metal
prvky	prvek	k1gInPc7	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Deathrash	Deathrash	k1gInSc1	Deathrash
(	(	kIx(	(
<g/>
též	též	k9	též
"	"	kIx"	"
<g/>
Death	Death	k1gMnSc1	Death
<g/>
/	/	kIx~	/
<g/>
Thrash	Thrash	k1gMnSc1	Thrash
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
death-thrash	deathhrash	k1gInSc1	death-thrash
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
thrash	thrasha	k1gFnPc2	thrasha
metalu	metal	k1gInSc2	metal
s	s	k7c7	s
prvky	prvek	k1gInPc7	prvek
death	deatha	k1gFnPc2	deatha
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yRgFnPc4	který
patří	patřit	k5eAaImIp3nS	patřit
rychlé	rychlý	k2eAgNnSc1d1	rychlé
bubnování	bubnování	k1gNnSc1	bubnování
<g/>
,	,	kIx,	,
kytary	kytara	k1gFnPc1	kytara
<g/>
,	,	kIx,	,
tematika	tematika	k1gFnSc1	tematika
smrti	smrt	k1gFnSc2	smrt
a	a	k8xC	a
občas	občas	k6eAd1	občas
také	také	k9	také
vokály	vokál	k1gInPc1	vokál
v	v	k7c6	v
death-metalovém	deathetalový	k2eAgInSc6d1	death-metalový
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prvním	první	k4xOgInSc6	první
období	období	k1gNnSc6	období
byl	být	k5eAaImAgMnS	být
tento	tento	k3xDgInSc4	tento
styl	styl	k1gInSc4	styl
přechodem	přechod	k1gInSc7	přechod
od	od	k7c2	od
thrash	thrasha	k1gFnPc2	thrasha
metalu	metal	k1gInSc2	metal
k	k	k7c3	k
death	death	k1gInSc4	death
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
začal	začít	k5eAaPmAgInS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ukázková	ukázkový	k2eAgNnPc4d1	ukázkové
alba	album	k1gNnPc4	album
stylu	styl	k1gInSc2	styl
patří	patřit	k5eAaImIp3nS	patřit
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Reign	Reign	k1gInSc1	Reign
in	in	k?	in
Blood	Blood	k1gInSc1	Blood
<g/>
"	"	kIx"	"
od	od	k7c2	od
Slayer	Slayra	k1gFnPc2	Slayra
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Beneath	Beneath	k1gInSc1	Beneath
the	the	k?	the
Remains	Remains	k1gInSc1	Remains
<g/>
"	"	kIx"	"
od	od	k7c2	od
Sepultury	Sepultura	k1gFnSc2	Sepultura
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Pleasure	Pleasur	k1gMnSc5	Pleasur
to	ten	k3xDgNnSc4	ten
Kill	Kill	k1gMnSc1	Kill
<g/>
"	"	kIx"	"
od	od	k7c2	od
Kreator	Kreator	k1gInSc4	Kreator
a	a	k8xC	a
"	"	kIx"	"
<g/>
Darkness	Darkness	k1gInSc1	Darkness
Descends	Descendsa	k1gFnPc2	Descendsa
<g/>
"	"	kIx"	"
od	od	k7c2	od
Dark	Darka	k1gFnPc2	Darka
Angel	Angela	k1gFnPc2	Angela
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
kapely	kapela	k1gFnPc4	kapela
patří	patřit	k5eAaImIp3nS	patřit
Pestilence	Pestilence	k1gFnSc1	Pestilence
<g/>
,	,	kIx,	,
Epidemic	Epidemic	k1gMnSc1	Epidemic
a	a	k8xC	a
Cancer	Cancer	k1gMnSc1	Cancer
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
považují	považovat	k5eAaImIp3nP	považovat
grindcore	grindcor	k1gInSc5	grindcor
za	za	k7c4	za
extrémní	extrémní	k2eAgFnSc4d1	extrémní
variantu	varianta	k1gFnSc4	varianta
death	deatha	k1gFnPc2	deatha
metalu	metal	k1gInSc2	metal
a	a	k8xC	a
hardcore	hardcor	k1gInSc5	hardcor
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
příznivci	příznivec	k1gMnPc1	příznivec
grindcore	grindcor	k1gInSc5	grindcor
a	a	k8xC	a
hudební	hudební	k2eAgMnPc1d1	hudební
historici	historik	k1gMnPc1	historik
ho	on	k3xPp3gNnSc4	on
spíš	spíš	k9	spíš
považují	považovat	k5eAaImIp3nP	považovat
za	za	k7c4	za
samostatný	samostatný	k2eAgInSc4d1	samostatný
žánr	žánr	k1gInSc4	žánr
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
se	se	k3xPyFc4	se
vyvíjel	vyvíjet	k5eAaImAgMnS	vyvíjet
paralelně	paralelně	k6eAd1	paralelně
s	s	k7c7	s
death	death	k1gInSc1	death
metalem	metal	k1gInSc7	metal
(	(	kIx(	(
<g/>
death	death	k1gInSc1	death
metal	metat	k5eAaImAgInS	metat
z	z	k7c2	z
thrash	thrasha	k1gFnPc2	thrasha
metalu	metal	k1gInSc2	metal
<g/>
,	,	kIx,	,
grindcore	grindcor	k1gInSc5	grindcor
z	z	k7c2	z
crust	crust	k1gFnSc4	crust
punku	punk	k1gInSc2	punk
<g/>
)	)	kIx)	)
a	a	k8xC	a
navzájem	navzájem	k6eAd1	navzájem
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
působily	působit	k5eAaImAgFnP	působit
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
má	mít	k5eAaImIp3nS	mít
raný	raný	k2eAgMnSc1d1	raný
grindcore	grindcor	k1gInSc5	grindcor
kořeny	kořen	k1gInPc4	kořen
v	v	k7c6	v
hardcore	hardcor	k1gInSc5	hardcor
<g/>
,	,	kIx,	,
punk	punk	k1gInSc4	punk
metalu	metal	k1gInSc2	metal
a	a	k8xC	a
anarcho-punku	anarchounk	k1gInSc2	anarcho-punk
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
první	první	k4xOgInSc4	první
grindcore	grindcor	k1gInSc5	grindcor
skupiny	skupina	k1gFnSc2	skupina
patří	patřit	k5eAaImIp3nS	patřit
Anal	Anal	k1gMnSc1	Anal
Cunt	Cunt	k1gMnSc1	Cunt
<g/>
,	,	kIx,	,
Napalm	napalm	k1gInSc1	napalm
Death	Death	k1gInSc1	Death
<g/>
,	,	kIx,	,
Carcass	Carcass	k1gInSc1	Carcass
<g/>
,	,	kIx,	,
Impetigo	impetigo	k1gNnSc1	impetigo
<g/>
,	,	kIx,	,
Fear	Fear	k1gMnSc1	Fear
of	of	k?	of
God	God	k1gMnSc1	God
<g/>
,	,	kIx,	,
Terrorizer	Terrorizer	k1gMnSc1	Terrorizer
<g/>
,	,	kIx,	,
Extreme	Extrem	k1gInSc5	Extrem
Noise	Noisa	k1gFnSc3	Noisa
Terror	Terror	k1gInSc1	Terror
<g/>
,	,	kIx,	,
Repulsion	Repulsion	k1gInSc1	Repulsion
a	a	k8xC	a
čeští	český	k2eAgMnPc1d1	český
Ingrowing	Ingrowing	k1gInSc4	Ingrowing
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
<g/>
,	,	kIx,	,
existují	existovat	k5eAaImIp3nP	existovat
metalové	metalový	k2eAgFnPc1d1	metalová
odnože	odnož	k1gFnPc1	odnož
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
kombinováním	kombinování	k1gNnSc7	kombinování
death	deatha	k1gFnPc2	deatha
metalu	metal	k1gInSc2	metal
a	a	k8xC	a
jazzu	jazz	k1gInSc2	jazz
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Pestilence	Pestilence	k1gFnSc2	Pestilence
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
album	album	k1gNnSc4	album
Spheres	Spheres	k1gInSc4	Spheres
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
výtvory	výtvor	k1gInPc1	výtvor
floridských	floridský	k2eAgInPc2d1	floridský
Atheist	Atheist	k1gInSc4	Atheist
a	a	k8xC	a
Cynic	Cynic	k1gMnSc1	Cynic
<g/>
.	.	kIx.	.
</s>
<s>
Gorguts	Gorguts	k1gInSc1	Gorguts
je	být	k5eAaImIp3nS	být
další	další	k2eAgFnSc7d1	další
kapelou	kapela	k1gFnSc7	kapela
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
používá	používat	k5eAaImIp3nS	používat
prvky	prvek	k1gInPc4	prvek
jazzu	jazz	k1gInSc2	jazz
(	(	kIx(	(
<g/>
album	album	k1gNnSc1	album
"	"	kIx"	"
<g/>
Obscura	Obscura	k1gFnSc1	Obscura
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nile	Nil	k1gInSc5	Nil
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
hudby	hudba	k1gFnSc2	hudba
zase	zase	k9	zase
začlenili	začlenit	k5eAaPmAgMnP	začlenit
egyptskou	egyptský	k2eAgFnSc4d1	egyptská
hudbu	hudba	k1gFnSc4	hudba
a	a	k8xC	a
tematiku	tematika	k1gFnSc4	tematika
Blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
si	se	k3xPyFc3	se
získává	získávat	k5eAaImIp3nS	získávat
stále	stále	k6eAd1	stále
větší	veliký	k2eAgFnSc4d2	veliký
popularitu	popularita	k1gFnSc4	popularita
metalcore	metalcor	k1gInSc5	metalcor
<g/>
,	,	kIx,	,
styl	styl	k1gInSc4	styl
kombinující	kombinující	k2eAgFnSc2d1	kombinující
moderní	moderní	k2eAgFnSc2d1	moderní
hardcore	hardcor	k1gInSc5	hardcor
punk	punk	k1gInSc1	punk
s	s	k7c7	s
thrash	thrash	k1gInSc1	thrash
metalem	metal	k1gInSc7	metal
a	a	k8xC	a
death	death	k1gInSc4	death
metalem	metal	k1gInSc7	metal
<g/>
.	.	kIx.	.
</s>
<s>
Dying	Dying	k1gMnSc1	Dying
Fetus	Fetus	k1gMnSc1	Fetus
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
takovýchto	takovýto	k3xDgFnPc2	takovýto
kapel	kapela	k1gFnPc2	kapela
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc4	jejíž
členové	člen	k1gMnPc1	člen
byli	být	k5eAaImAgMnP	být
aktivní	aktivní	k2eAgMnPc1d1	aktivní
v	v	k7c6	v
lokální	lokální	k2eAgFnSc6d1	lokální
hardcore-punkové	hardcoreunkový	k2eAgFnSc6d1	hardcore-punkový
scéně	scéna	k1gFnSc6	scéna
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
Dying	Dying	k1gMnSc1	Dying
Fetus	Fetus	k1gMnSc1	Fetus
nemá	mít	k5eNaImIp3nS	mít
s	s	k7c7	s
metalcore	metalcor	k1gInSc5	metalcor
kapelami	kapela	k1gFnPc7	kapela
mnoho	mnoho	k6eAd1	mnoho
společného	společný	k2eAgNnSc2d1	společné
<g/>
.	.	kIx.	.
</s>
<s>
Třeba	třeba	k6eAd1	třeba
také	také	k9	také
poznamenat	poznamenat	k5eAaPmF	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnoho	mnoho	k4c1	mnoho
skupin	skupina	k1gFnPc2	skupina
možno	možno	k6eAd1	možno
lehce	lehko	k6eAd1	lehko
zařadit	zařadit	k5eAaPmF	zařadit
do	do	k7c2	do
dvou	dva	k4xCgNnPc2	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
popisovaných	popisovaný	k2eAgInPc2d1	popisovaný
stylů	styl	k1gInPc2	styl
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
kategorizace	kategorizace	k1gFnSc1	kategorizace
kapely	kapela	k1gFnSc2	kapela
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
jen	jen	k9	jen
věcí	věc	k1gFnSc7	věc
názoru	názor	k1gInSc2	názor
a	a	k8xC	a
interpretace	interpretace	k1gFnSc2	interpretace
<g/>
.	.	kIx.	.
</s>
<s>
Melodic	Melodic	k1gMnSc1	Melodic
death	death	k1gMnSc1	death
metal	metat	k5eAaImAgMnS	metat
Brutal	Brutal	k1gMnSc1	Brutal
death	death	k1gMnSc1	death
metal	metal	k1gInSc4	metal
Heavy	Heava	k1gFnSc2	Heava
metal	metat	k5eAaImAgMnS	metat
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
death	deatha	k1gFnPc2	deatha
metal	metat	k5eAaImAgInS	metat
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Encyclopaedia	Encyclopaedium	k1gNnSc2	Encyclopaedium
Metallum	Metallum	k1gNnSc4	Metallum
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Metallian	Metallian	k1gInSc1	Metallian
<g/>
,	,	kIx,	,
další	další	k2eAgFnSc1d1	další
metalová	metalový	k2eAgFnSc1d1	metalová
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Metalová	metalový	k2eAgFnSc1d1	metalová
mapa	mapa	k1gFnSc1	mapa
Evropy	Evropa	k1gFnSc2	Evropa
</s>
