<s>
Zřetelný	zřetelný	k2eAgMnSc1d1	zřetelný
je	být	k5eAaImIp3nS	být
tvrdý	tvrdý	k2eAgInSc1d1	tvrdý
(	(	kIx(	(
<g/>
harsh	harsh	k1gInSc1	harsh
<g/>
)	)	kIx)	)
styl	styl	k1gInSc1	styl
vokálů	vokál	k1gInPc2	vokál
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
velmi	velmi	k6eAd1	velmi
hrdelní	hrdelní	k2eAgInSc1d1	hrdelní
skřehot	skřehot	k1gInSc1	skřehot
(	(	kIx(	(
<g/>
False	False	k1gFnSc1	False
Chord	chorda	k1gFnPc2	chorda
Scream	Scream	k1gInSc4	Scream
<g/>
,	,	kIx,	,
growling	growling	k1gInSc4	growling
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
vysoko-intenzivní	vysokontenzivní	k2eAgInSc1d1	vysoko-intenzivní
vřískot	vřískot	k1gInSc1	vřískot
(	(	kIx(	(
<g/>
screaming	screaming	k1gInSc1	screaming
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
