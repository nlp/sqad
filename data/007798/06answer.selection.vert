<s>
Klasický	klasický	k2eAgInSc1d1	klasický
Hollywood	Hollywood	k1gInSc1	Hollywood
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Zlatý	zlatý	k2eAgInSc1d1	zlatý
věk	věk	k1gInSc1	věk
Hollywoodu	Hollywood	k1gInSc2	Hollywood
<g/>
"	"	kIx"	"
a	a	k8xC	a
specifické	specifický	k2eAgInPc4d1	specifický
stylové	stylový	k2eAgInPc4d1	stylový
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
rozvinuté	rozvinutý	k2eAgInPc4d1	rozvinutý
během	během	k7c2	během
této	tento	k3xDgFnSc2	tento
periody	perioda	k1gFnSc2	perioda
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
klasický	klasický	k2eAgInSc4d1	klasický
hollywoodský	hollywoodský	k2eAgInSc4d1	hollywoodský
styl	styl	k1gInSc4	styl
<g/>
.	.	kIx.	.
</s>
