<s>
Rosalía	Rosalía	k6eAd1
</s>
<s>
Rosalía	Rosalí	k2eAgFnSc1d1
Rosalía	Rosalía	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
<g/>
Základní	základní	k2eAgFnPc4d1
informace	informace	k1gFnPc4
Rodné	rodný	k2eAgNnSc4d1
jméno	jméno	k1gNnSc4
</s>
<s>
Rosalía	Rosalía	k1gFnSc1
Vila	Vila	k1gFnSc1
Tobella	Tobella	k1gFnSc1
Jinak	jinak	k6eAd1
zvaná	zvaný	k2eAgNnPc1d1
</s>
<s>
„	„	k?
<g/>
Rosalía	Rosalía	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
La	la	k1gNnSc2
Rosalía	Rosalíus	k1gMnSc2
<g/>
“	“	k?
Narození	narození	k1gNnSc1
</s>
<s>
25	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1993	#num#	k4
(	(	kIx(
<g/>
27	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Sant	Sant	k1gInSc1
Esteve	Esteev	k1gFnSc2
Sesrovires	Sesroviresa	k1gFnPc2
<g/>
,	,	kIx,
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
Katalánsko	Katalánsko	k1gNnSc1
Španělsko	Španělsko	k1gNnSc1
Španělsko	Španělsko	k1gNnSc1
Žánry	žánr	k1gInPc1
</s>
<s>
Flamenco	flamenco	k1gNnSc4
popLatinskoamerická	popLatinskoamerický	k2eAgFnSc1d1
hudba	hudba	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
zpěvačka	zpěvačka	k1gFnSc1
<g/>
,	,	kIx,
herečka	herečka	k1gFnSc1
<g/>
,	,	kIx,
autorka	autorka	k1gFnSc1
písní	píseň	k1gFnPc2
a	a	k8xC
producentka	producentka	k1gFnSc1
Nástroje	nástroj	k1gInSc2
</s>
<s>
hlas	hlas	k1gInSc1
a	a	k8xC
Palmas	Palmas	k1gInSc1
Aktivní	aktivní	k2eAgInSc1d1
roky	rok	k1gInPc4
</s>
<s>
2013	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
Vydavatel	vydavatel	k1gMnSc1
</s>
<s>
ColumbiaSonyUniversal	ColumbiaSonyUniversat	k5eAaImAgInS,k5eAaPmAgInS
Významná	významný	k2eAgNnPc4d1
díla	dílo	k1gNnPc4
</s>
<s>
El	Ela	k1gFnPc2
mal	málit	k5eAaImRp2nS
quererFucking	quererFucking	k1gInSc4
Money	Monea	k1gMnSc2
ManMalamente	ManMalament	k1gMnSc5
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Premi	Pre	k1gFnPc7
Ciutat	Ciutat	k1gFnSc2
de	de	k?
Barcelona	Barcelona	k1gFnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
Latin	Latin	k1gMnSc1
Grammy	Gramma	k1gFnSc2
Award	Awarda	k1gFnPc2
for	forum	k1gNnPc2
Best	Best	k1gMnSc1
Alternative	Alternativ	k1gInSc5
Song	song	k1gInSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
Latin	Latin	k1gMnSc1
Grammy	Gramma	k1gFnSc2
Award	Awarda	k1gFnPc2
for	forum	k1gNnPc2
Best	Best	k1gMnSc1
Urban	Urban	k1gMnSc1
Fusion	Fusion	k1gInSc1
<g/>
/	/	kIx~
<g/>
Performance	performance	k1gFnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
Latin	Latin	k1gMnSc1
Grammy	Gramma	k1gFnSc2
Award	Awarda	k1gFnPc2
for	forum	k1gNnPc2
Best	Best	k1gMnSc1
Urban	Urban	k1gMnSc1
Song	song	k1gInSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
Latin	Latin	k1gMnSc1
Grammy	Gramma	k1gFnSc2
Award	Awarda	k1gFnPc2
for	forum	k1gNnPc2
Best	Best	k1gMnSc1
Contemporary	Contemporara	k1gFnSc2
Pop	pop	k1gMnSc1
Vocal	Vocal	k1gInSc1
Album	album	k1gNnSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
…	…	k?
více	hodně	k6eAd2
na	na	k7c6
Wikidatech	Wikidat	k1gInPc6
Web	web	k1gInSc4
</s>
<s>
rosalia	rosalia	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Rosalía	Rosalía	k1gFnSc1
(	(	kIx(
<g/>
celým	celý	k2eAgNnSc7d1
jménem	jméno	k1gNnSc7
Rosalía	Rosalíum	k1gNnSc2
Vila	vila	k1gFnSc1
Tobella	Tobella	k1gFnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
*	*	kIx~
25	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1993	#num#	k4
Sant	Santa	k1gFnPc2
Esteve	Esteev	k1gFnSc2
Sesrovires	Sesrovires	k1gInSc1
<g/>
,	,	kIx,
Barcelona	Barcelona	k1gFnSc1
<g/>
,	,	kIx,
Katalánsko	Katalánsko	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
španělská	španělský	k2eAgFnSc1d1
zpěvačka	zpěvačka	k1gFnSc1
<g/>
,	,	kIx,
skladatelka	skladatelka	k1gFnSc1
a	a	k8xC
producentka	producentka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proslavila	proslavit	k5eAaPmAgFnS
se	s	k7c7
spoluprácemi	spolupráce	k1gFnPc7
s	s	k7c7
umělci	umělec	k1gMnPc7
jako	jako	k8xS,k8xC
jsou	být	k5eAaImIp3nP
Travis	Travis	k1gFnSc4
Scott	Scotta	k1gFnPc2
<g/>
,	,	kIx,
J	J	kA
Balvin	Balvina	k1gFnPc2
<g/>
,	,	kIx,
Pharrell	Pharrella	k1gFnPc2
Williams	Williamsa	k1gFnPc2
nebo	nebo	k8xC
James	Jamesa	k1gFnPc2
Blake	Blak	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc1
hudba	hudba	k1gFnSc1
bývá	bývat	k5eAaImIp3nS
označována	označovat	k5eAaImNgFnS
jako	jako	k9
fúze	fúze	k1gFnSc1
flamenca	flamenco	k1gNnSc2
<g/>
,	,	kIx,
R	R	kA
<g/>
&	&	k?
<g/>
B	B	kA
<g/>
,	,	kIx,
popu	pop	k1gInSc2
a	a	k8xC
hip	hip	k0
hopu	hopu	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Své	svůj	k3xOyFgNnSc4
debutové	debutový	k2eAgNnSc1d1
album	album	k1gNnSc1
Los	los	k1gInSc1
Ángeles	Ángeles	k1gMnSc1
vydala	vydat	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
a	a	k8xC
o	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
vydala	vydat	k5eAaPmAgFnS
další	další	k2eAgNnSc4d1
album	album	k1gNnSc4
El	Ela	k1gFnPc2
Mal	málit	k5eAaImRp2nS
Querer	Querer	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
držitelkou	držitelka	k1gFnSc7
několika	několik	k4yIc2
prestižních	prestižní	k2eAgNnPc2d1
ocenění	ocenění	k1gNnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
pěti	pět	k4xCc2
cen	cena	k1gFnPc2
Latin	latina	k1gFnPc2
Grammy	Gramma	k1gFnSc2
Awards	Awards	k1gInSc1
a	a	k8xC
jedné	jeden	k4xCgFnSc2
Grammy	Gramma	k1gFnSc2
Award	Awarda	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Mládí	mládí	k1gNnSc1
a	a	k8xC
kariéra	kariéra	k1gFnSc1
</s>
<s>
Mládí	mládí	k1gNnSc1
</s>
<s>
Rosalía	Rosalía	k1gFnSc1
se	se	k3xPyFc4
narodila	narodit	k5eAaPmAgFnS
v	v	k7c6
katalánském	katalánský	k2eAgNnSc6d1
městě	město	k1gNnSc6
Sant	Sant		k1gFnSc2
Esteve	Esteev	k1gFnSc2
Sesrovires	Sesroviresa	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gInPc4
pradědeček	pradědeček	k1gMnSc1
byl	být	k5eAaImAgMnS
Kubánec	Kubánec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc1
matka	matka	k1gFnSc1
je	být	k5eAaImIp3nS
z	z	k7c2
Katalánska	Katalánsko	k1gNnSc2
a	a	k8xC
její	její	k3xOp3gMnSc1
otec	otec	k1gMnSc1
z	z	k7c2
Asturie	Asturie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rosalía	Rosalí	k1gInSc2
hovoří	hovořit	k5eAaImIp3nS
plynně	plynně	k6eAd1
katalánsky	katalánsky	k6eAd1
<g/>
,	,	kIx,
španělsky	španělsky	k6eAd1
a	a	k8xC
anglicky	anglicky	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyrostla	vyrůst	k5eAaPmAgFnS
v	v	k7c6
rodině	rodina	k1gFnSc6
bez	bez	k7c2
hudebních	hudební	k2eAgFnPc2d1
tradic	tradice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
mládí	mládí	k1gNnSc4
objevila	objevit	k5eAaPmAgFnS
flamenco	flamenco	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
absolventkou	absolventka	k1gFnSc7
školy	škola	k1gFnSc2
Escola	Escola	k1gFnSc1
Superior	superior	k1gMnSc1
de	de	k?
Música	Música	k1gMnSc1
de	de	k?
Catalunya	Catalunya	k1gMnSc1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
katalánská	katalánský	k2eAgFnSc1d1
hudební	hudební	k2eAgFnSc1d1
škola	škola	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
2017	#num#	k4
<g/>
:	:	kIx,
Los	los	k1gInSc1
Ángeles	Ángelesa	k1gFnPc2
</s>
<s>
V	v	k7c6
únoru	únor	k1gInSc6
2017	#num#	k4
Rosalía	Rosalía	k1gFnSc1
vydala	vydat	k5eAaPmAgFnS
své	svůj	k3xOyFgNnSc4
debutové	debutový	k2eAgNnSc4d1
album	album	k1gNnSc4
s	s	k7c7
názvem	název	k1gInSc7
Los	los	k1gInSc1
Ángeles	Ángelesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Album	album	k1gNnSc1
obsahuje	obsahovat	k5eAaImIp3nS
12	#num#	k4
písní	píseň	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
byly	být	k5eAaImAgFnP
složeny	složit	k5eAaPmNgFnP
v	v	k7c6
atmosféře	atmosféra	k1gFnSc6
připomínající	připomínající	k2eAgNnSc1d1
tradiční	tradiční	k2eAgNnSc1d1
flamenco	flamenco	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
nominována	nominovat	k5eAaBmNgFnS
na	na	k7c4
cenu	cena	k1gFnSc4
Latin	latina	k1gFnPc2
Grammy	Gramma	k1gFnSc2
Awards	Awardsa	k1gFnPc2
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
v	v	k7c6
kategorii	kategorie	k1gFnSc6
Nejlepší	dobrý	k2eAgMnSc1d3
nový	nový	k2eAgMnSc1d1
umělec	umělec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
2018	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
<g/>
:	:	kIx,
El	Ela	k1gFnPc2
Mal	málit	k5eAaImRp2nS
Querer	Querer	k1gMnSc1
</s>
<s>
V	v	k7c6
listopadu	listopad	k1gInSc6
2018	#num#	k4
vydala	vydat	k5eAaPmAgFnS
své	svůj	k3xOyFgNnSc4
druhé	druhý	k4xOgNnSc4
studiové	studiový	k2eAgNnSc4d1
album	album	k1gNnSc4
s	s	k7c7
názvem	název	k1gInSc7
El	Ela	k1gFnPc2
Mal	málit	k5eAaImRp2nS
Querer	Querer	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Album	album	k1gNnSc1
obsahuje	obsahovat	k5eAaImIp3nS
11	#num#	k4
písní	píseň	k1gFnPc2
nahraných	nahraný	k2eAgFnPc2d1
ve	v	k7c6
stylu	styl	k1gInSc6
flamenkové	flamenkový	k2eAgFnSc2d1
hudby	hudba	k1gFnSc2
v	v	k7c6
kombinaci	kombinace	k1gFnSc6
s	s	k7c7
různými	různý	k2eAgInPc7d1
jinými	jiný	k2eAgInPc7d1
žánry	žánr	k1gInPc7
hudby	hudba	k1gFnSc2
<g/>
,	,	kIx,
např.	např.	kA
pop	pop	k1gInSc1
<g/>
,	,	kIx,
R	R	kA
<g/>
&	&	k?
<g/>
B	B	kA
a	a	k8xC
elektronická	elektronický	k2eAgFnSc1d1
hudba	hudba	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Album	album	k1gNnSc1
bylo	být	k5eAaImAgNnS
na	na	k7c6
španělském	španělský	k2eAgInSc6d1
žebříčku	žebříček	k1gInSc6
a	a	k8xC
na	na	k7c6
americkém	americký	k2eAgInSc6d1
seznamu	seznam	k1gInSc6
nejpopulárnějších	populární	k2eAgNnPc2d3
latinsko-popových	latinsko-popový	k2eAgNnPc2d1
alb	album	k1gNnPc2
od	od	k7c2
časopisu	časopis	k1gInSc2
Billboard	billboard	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
alba	album	k1gNnSc2
vyšly	vyjít	k5eAaPmAgInP
4	#num#	k4
singly	singl	k1gInPc4
<g/>
:	:	kIx,
Malamente	Malament	k1gMnSc5
<g/>
,	,	kIx,
Pienso	Piensa	k1gFnSc5
en	en	k?
tu	tu	k6eAd1
mirá	mirat	k5eAaImIp3nS,k5eAaPmIp3nS,k5eAaBmIp3nS
<g/>
,	,	kIx,
Di	Di	k1gFnSc1
mi	já	k3xPp1nSc3
nombre	nombr	k1gInSc5
a	a	k8xC
Bagdad	Bagdad	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Píseň	píseň	k1gFnSc1
Malamente	Malament	k1gInSc5
získala	získat	k5eAaPmAgFnS
pět	pět	k4xCc4
nominací	nominace	k1gFnPc2
na	na	k7c4
cenu	cena	k1gFnSc4
Latin	latina	k1gFnPc2
Grammy	Gramma	k1gFnSc2
Awards	Awardsa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rosalía	Rosalía	k1gFnSc1
byla	být	k5eAaImAgFnS
nominována	nominovat	k5eAaBmNgFnS
na	na	k7c4
MTV	MTV	kA
Europe	Europ	k1gInSc5
Music	Musice	k1gInPc2
Awards	Awards	k1gInSc1
2018	#num#	k4
v	v	k7c6
kategorii	kategorie	k1gFnSc6
nejlepší	dobrý	k2eAgMnSc1d3
španělský	španělský	k2eAgMnSc1d1
umělec	umělec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
předávání	předávání	k1gNnSc2
cen	cena	k1gFnPc2
předvedla	předvést	k5eAaPmAgFnS
písně	píseň	k1gFnPc4
De	De	k?
Aquí	Aquí	k1gMnSc1
No	no	k9
Sales	Sales	k1gInSc4
a	a	k8xC
Malamente	Malament	k1gInSc5
<g/>
.	.	kIx.
</s>
<s>
Rosalía	Rosalía	k1gFnSc1
vystupující	vystupující	k2eAgFnSc1d1
v	v	k7c6
červnu	červen	k1gInSc6
2019	#num#	k4
v	v	k7c6
Barceloně	Barcelona	k1gFnSc6
před	před	k7c7
davem	dav	k1gInSc7
63	#num#	k4
000	#num#	k4
návštěvníků	návštěvník	k1gMnPc2
</s>
<s>
Jako	jako	k9
herečka	herečka	k1gFnSc1
debutovala	debutovat	k5eAaBmAgFnS
ve	v	k7c6
filmu	film	k1gInSc6
režiséra	režisér	k1gMnSc2
Pedra	Pedr	k1gMnSc2
Almodóvara	Almodóvar	k1gMnSc2
s	s	k7c7
názvem	název	k1gInSc7
Bolest	bolest	k1gFnSc1
a	a	k8xC
sláva	sláva	k1gFnSc1
(	(	kIx(
<g/>
španělsky	španělsky	k6eAd1
Dolor	Dolor	k1gInSc1
y	y	k?
Gloria	Gloria	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Film	film	k1gInSc1
měl	mít	k5eAaImAgInS
premiéru	premiéra	k1gFnSc4
ve	v	k7c6
Španělsku	Španělsko	k1gNnSc6
22	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
vydala	vydat	k5eAaPmAgFnS
několik	několik	k4yIc4
spolupráci	spolupráce	k1gFnSc6
a	a	k8xC
singlů	singl	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Diskografie	diskografie	k1gFnSc1
</s>
<s>
Studiová	studiový	k2eAgNnPc1d1
alba	album	k1gNnPc1
</s>
<s>
Los	los	k1gInSc1
Ángeles	Ángeles	k1gInSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
El	Ela	k1gFnPc2
Mal	málit	k5eAaImRp2nS
Querer	Querer	k1gMnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Rosalía	Rosalí	k1gInSc2
(	(	kIx(
<g/>
singer	singer	k1gInSc1
<g/>
)	)	kIx)
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0004	#num#	k4
6733	#num#	k4
3387	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
no	no	k9
<g/>
2018161169	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
2908152331602403260002	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc2
<g/>
:	:	kIx,
lccn-no	lccn-no	k6eAd1
<g/>
2018161169	#num#	k4
</s>
