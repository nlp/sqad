<p>
<s>
Harry	Harra	k1gMnSc2	Harra
James	James	k1gMnSc1	James
Potter	Potter	k1gMnSc1	Potter
(	(	kIx(	(
<g/>
*	*	kIx~	*
31	[number]	k4	31
<g/>
.	.	kIx.	.
červenec	červenec	k1gInSc4	červenec
1980	[number]	k4	1980
Godrikův	Godrikův	k2eAgInSc4d1	Godrikův
důl	důl	k1gInSc4	důl
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgFnSc1d1	hlavní
postava	postava	k1gFnSc1	postava
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
série	série	k1gFnSc2	série
od	od	k7c2	od
J.	J.	kA	J.
K.	K.	kA	K.
Rowlingové	Rowlingový	k2eAgFnPc1d1	Rowlingová
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Godrikově	Godrikův	k2eAgInSc6d1	Godrikův
dole	dol	k1gInSc6	dol
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
rodiči	rodič	k1gMnPc7	rodič
byli	být	k5eAaImAgMnP	být
James	James	k1gMnSc1	James
Potter	Potter	k1gMnSc1	Potter
a	a	k8xC	a
Lily	lít	k5eAaImAgFnP	lít
Potterová	Potterová	k1gFnSc1	Potterová
<g/>
,	,	kIx,	,
rozená	rozený	k2eAgFnSc1d1	rozená
Evansová	Evansová	k1gFnSc1	Evansová
<g/>
.	.	kIx.	.
</s>
<s>
Byli	být	k5eAaImAgMnP	být
však	však	k9	však
zavražděni	zavražděn	k2eAgMnPc1d1	zavražděn
zlým	zlý	k2eAgInSc7d1	zlý
čarodějem	čaroděj	k1gMnSc7	čaroděj
jménem	jméno	k1gNnSc7	jméno
Lord	lord	k1gMnSc1	lord
Voldemort	Voldemort	k1gInSc1	Voldemort
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc4	Harra
útok	útok	k1gInSc1	útok
přežil	přežít	k5eAaPmAgInS	přežít
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
vychováván	vychovávat	k5eAaImNgInS	vychovávat
u	u	k7c2	u
své	svůj	k3xOyFgFnSc2	svůj
tety	teta	k1gFnSc2	teta
a	a	k8xC	a
strýce	strýc	k1gMnSc4	strýc
<g/>
.	.	kIx.	.
</s>
<s>
Vernon	Vernon	k1gInSc1	Vernon
Dursley	Durslea	k1gFnSc2	Durslea
<g/>
,	,	kIx,	,
Petunie	Petunie	k1gFnSc1	Petunie
Dursleyová	Dursleyová	k1gFnSc1	Dursleyová
i	i	k8xC	i
jejich	jejich	k3xOp3gMnSc1	jejich
syn	syn	k1gMnSc1	syn
Dudley	Dudle	k2eAgInPc4d1	Dudle
Dursley	Dursley	k1gInPc4	Dursley
byli	být	k5eAaImAgMnP	být
mudlové	mudl	k1gMnPc1	mudl
(	(	kIx(	(
<g/>
lidé	člověk	k1gMnPc1	člověk
bez	bez	k7c2	bez
kapky	kapka	k1gFnSc2	kapka
čarodějné	čarodějný	k2eAgFnSc2d1	čarodějná
krve	krev	k1gFnSc2	krev
<g/>
)	)	kIx)	)
a	a	k8xC	a
chovali	chovat	k5eAaImAgMnP	chovat
se	se	k3xPyFc4	se
k	k	k7c3	k
Harrymu	Harrym	k1gInSc3	Harrym
skoro	skoro	k6eAd1	skoro
celých	celý	k2eAgInPc2d1	celý
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
jako	jako	k9	jako
ke	k	k7c3	k
kusu	kus	k1gInSc3	kus
hadru	hadr	k1gInSc2	hadr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
čaroději	čaroděj	k1gMnPc7	čaroděj
je	být	k5eAaImIp3nS	být
známou	známý	k2eAgFnSc7d1	známá
celebritou	celebrita	k1gFnSc7	celebrita
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
téměř	téměř	k6eAd1	téměř
nikdo	nikdo	k3yNnSc1	nikdo
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
něho	on	k3xPp3gNnSc2	on
<g/>
)	)	kIx)	)
neví	vědět	k5eNaImIp3nS	vědět
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
to	ten	k3xDgNnSc4	ten
udělal	udělat	k5eAaPmAgMnS	udělat
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
nejen	nejen	k6eAd1	nejen
jediným	jediné	k1gNnSc7	jediné
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
přežil	přežít	k5eAaPmAgMnS	přežít
Voldemortův	Voldemortův	k2eAgInSc4d1	Voldemortův
útok	útok	k1gInSc4	útok
kletbou	kletba	k1gFnSc7	kletba
Avada	Avado	k1gNnSc2	Avado
kedavra	kedavr	k1gInSc2	kedavr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
navíc	navíc	k6eAd1	navíc
přitom	přitom	k6eAd1	přitom
Voldemorta	Voldemort	k1gMnSc4	Voldemort
těžce	těžce	k6eAd1	těžce
zranil	zranit	k5eAaPmAgMnS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
dozví	dozvědět	k5eAaPmIp3nS	dozvědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
důvodem	důvod	k1gInSc7	důvod
jeho	jeho	k3xOp3gNnSc2	jeho
přežití	přežití	k1gNnSc2	přežití
byla	být	k5eAaImAgFnS	být
oběť	oběť	k1gFnSc1	oběť
jeho	jeho	k3xOp3gFnSc2	jeho
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
bylo	být	k5eAaImAgNnS	být
předpovězeno	předpovězen	k2eAgNnSc1d1	předpovězeno
<g/>
,	,	kIx,	,
že	že	k8xS	že
Harry	Harra	k1gFnPc4	Harra
a	a	k8xC	a
Voldemort	Voldemort	k1gInSc4	Voldemort
jsou	být	k5eAaImIp3nP	být
spojeni	spojit	k5eAaPmNgMnP	spojit
věštbou	věštba	k1gFnSc7	věštba
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
ani	ani	k8xC	ani
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
nemůže	moct	k5eNaImIp3nS	moct
žít	žít	k5eAaImF	žít
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
druhý	druhý	k4xOgMnSc1	druhý
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
naživu	naživu	k6eAd1	naživu
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
že	že	k8xS	že
jeden	jeden	k4xCgMnSc1	jeden
musí	muset	k5eAaImIp3nS	muset
zabít	zabít	k5eAaPmF	zabít
druhého	druhý	k4xOgMnSc4	druhý
(	(	kIx(	(
<g/>
existence	existence	k1gFnPc1	existence
této	tento	k3xDgFnSc2	tento
věštby	věštba	k1gFnSc2	věštba
byla	být	k5eAaImAgFnS	být
také	také	k9	také
důvodem	důvod	k1gInSc7	důvod
Voldemortova	Voldemortův	k2eAgInSc2d1	Voldemortův
útoku	útok	k1gInSc2	útok
na	na	k7c4	na
něj	on	k3xPp3gNnSc4	on
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
filmech	film	k1gInPc6	film
Harryho	Harry	k1gMnSc4	Harry
Pottera	Potter	k1gMnSc4	Potter
hrál	hrát	k5eAaImAgMnS	hrát
Daniel	Daniel	k1gMnSc1	Daniel
Radcliffe	Radcliff	k1gInSc5	Radcliff
<g/>
,	,	kIx,	,
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
znění	znění	k1gNnSc6	znění
ho	on	k3xPp3gMnSc4	on
namluvil	namluvit	k5eAaPmAgMnS	namluvit
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
Kotek	Kotek	k1gMnSc1	Kotek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Knihy	kniha	k1gFnSc2	kniha
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Harry	Harra	k1gMnSc2	Harra
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
Kámen	kámen	k1gInSc1	kámen
mudrců	mudrc	k1gMnPc2	mudrc
===	===	k?	===
</s>
</p>
<p>
<s>
Harry	Harr	k1gInPc1	Harr
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
počáteční	počáteční	k2eAgInSc4d1	počáteční
odpor	odpor	k1gInSc4	odpor
své	svůj	k3xOyFgFnSc2	svůj
adoptivní	adoptivní	k2eAgFnSc2d1	adoptivní
rodiny	rodina	k1gFnSc2	rodina
stal	stát	k5eAaPmAgMnS	stát
studentem	student	k1gMnSc7	student
nebelvírské	belvírský	k2eNgFnSc2d1	nebelvírská
koleje	kolej	k1gFnSc2	kolej
Školy	škola	k1gFnSc2	škola
čar	čára	k1gFnPc2	čára
a	a	k8xC	a
kouzel	kouzlo	k1gNnPc2	kouzlo
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
nejlepšími	dobrý	k2eAgMnPc7d3	nejlepší
přáteli	přítel	k1gMnPc7	přítel
jsou	být	k5eAaImIp3nP	být
spolužáci	spolužák	k1gMnPc1	spolužák
Ron	ron	k1gInSc4	ron
Weasley	Weasley	k1gInPc4	Weasley
a	a	k8xC	a
Hermiona	Hermiona	k1gFnSc1	Hermiona
Grangerová	Grangerová	k1gFnSc1	Grangerová
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
patří	patřit	k5eAaImIp3nS	patřit
spíš	spíš	k9	spíš
mezi	mezi	k7c4	mezi
průměrné	průměrný	k2eAgMnPc4d1	průměrný
studenty	student	k1gMnPc4	student
a	a	k8xC	a
zaplete	zaplést	k5eAaPmIp3nS	zaplést
se	se	k3xPyFc4	se
do	do	k7c2	do
nadprůměrného	nadprůměrný	k2eAgInSc2d1	nadprůměrný
počtu	počet	k1gInSc2	počet
průšvihů	průšvih	k1gInPc2	průšvih
<g/>
,	,	kIx,	,
ne	ne	k9	ne
vždy	vždy	k6eAd1	vždy
ovšem	ovšem	k9	ovšem
vlastní	vlastní	k2eAgFnSc7d1	vlastní
vinou	vina	k1gFnSc7	vina
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
v	v	k7c6	v
prvním	první	k4xOgInSc6	první
ročníku	ročník	k1gInSc6	ročník
(	(	kIx(	(
<g/>
ač	ač	k8xS	ač
to	ten	k3xDgNnSc1	ten
normálně	normálně	k6eAd1	normálně
není	být	k5eNaImIp3nS	být
povoleno	povolit	k5eAaPmNgNnS	povolit
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
chytačem	chytač	k1gInSc7	chytač
nebelvírského	belvírský	k2eNgNnSc2d1	nebelvírské
Famfrpálového	Famfrpálový	k2eAgNnSc2d1	Famfrpálové
mužstva	mužstvo	k1gNnSc2	mužstvo
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
kráčí	kráčet	k5eAaImIp3nS	kráčet
ve	v	k7c6	v
stopách	stopa	k1gFnPc6	stopa
svého	své	k1gNnSc2	své
otce	otec	k1gMnSc2	otec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
škole	škola	k1gFnSc6	škola
hned	hned	k6eAd1	hned
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
naráží	narážet	k5eAaPmIp3nS	narážet
na	na	k7c4	na
neshody	neshoda	k1gFnPc4	neshoda
s	s	k7c7	s
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
učitelů	učitel	k1gMnPc2	učitel
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Severus	Severus	k1gInSc1	Severus
Snape	Snap	k1gInSc5	Snap
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
jeho	jeho	k3xOp3gMnSc1	jeho
učitel	učitel	k1gMnSc1	učitel
lektvarů	lektvar	k1gInPc2	lektvar
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
ředitel	ředitel	k1gMnSc1	ředitel
Zmijozelské	Zmijozelský	k2eAgFnSc2d1	Zmijozelská
koleje	kolej	k1gFnSc2	kolej
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
jeho	jeho	k3xOp3gMnSc7	jeho
školním	školní	k2eAgMnSc7d1	školní
rivalem	rival	k1gMnSc7	rival
je	být	k5eAaImIp3nS	být
Draco	Draco	k1gMnSc1	Draco
Malfoy	Malfoa	k1gFnSc2	Malfoa
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
stejný	stejný	k2eAgInSc1d1	stejný
ročník	ročník	k1gInSc1	ročník
jako	jako	k8xC	jako
Harry	Harra	k1gFnPc1	Harra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
zmijozelské	zmijozelský	k2eAgFnSc6d1	zmijozelská
koleji	kolej	k1gFnSc6	kolej
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
překonává	překonávat	k5eAaImIp3nS	překonávat
nástrahy	nástraha	k1gFnSc2	nástraha
školního	školní	k2eAgInSc2d1	školní
života	život	k1gInSc2	život
a	a	k8xC	a
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
knihy	kniha	k1gFnSc2	kniha
zachrání	zachránit	k5eAaPmIp3nS	zachránit
Kámen	kámen	k1gInSc1	kámen
mudrců	mudrc	k1gMnPc2	mudrc
před	před	k7c7	před
Voldemortem	Voldemort	k1gInSc7	Voldemort
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
školního	školní	k2eAgInSc2d1	školní
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
vrací	vracet	k5eAaImIp3nS	vracet
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
tetě	teta	k1gFnSc3	teta
a	a	k8xC	a
svému	svůj	k3xOyFgMnSc3	svůj
strýci	strýc	k1gMnSc3	strýc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
oproti	oproti	k7c3	oproti
dřívějšku	dřívějšek	k1gInSc3	dřívějšek
má	mít	k5eAaImIp3nS	mít
naději	naděje	k1gFnSc4	naděje
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
prázdninách	prázdniny	k1gFnPc6	prázdniny
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nS	vrátit
mezi	mezi	k7c4	mezi
své	svůj	k3xOyFgMnPc4	svůj
kamarády	kamarád	k1gMnPc4	kamarád
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Harry	Harra	k1gMnSc2	Harra
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
Tajemná	tajemný	k2eAgFnSc1d1	tajemná
komnata	komnata	k1gFnSc1	komnata
===	===	k?	===
</s>
</p>
<p>
<s>
Harry	Harra	k1gFnPc4	Harra
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
u	u	k7c2	u
svého	svůj	k3xOyFgMnSc2	svůj
strýce	strýc	k1gMnSc2	strýc
a	a	k8xC	a
tety	teta	k1gFnSc2	teta
až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ho	on	k3xPp3gMnSc4	on
jeho	jeho	k3xOp3gNnSc1	jeho
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
přítel	přítel	k1gMnSc1	přítel
Ronald	Ronald	k1gMnSc1	Ronald
Weasley	Weaslea	k1gFnSc2	Weaslea
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
dva	dva	k4xCgMnPc1	dva
bratři	bratr	k1gMnPc1	bratr
odvážejí	odvážet	k5eAaImIp3nP	odvážet
létajícím	létající	k2eAgInSc7d1	létající
autem	aut	k1gInSc7	aut
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
domů	domů	k6eAd1	domů
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc4	Harra
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
přejít	přejít	k5eAaPmF	přejít
přes	přes	k7c4	přes
přepážku	přepážka	k1gFnSc4	přepážka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
k	k	k7c3	k
vlaku	vlak	k1gInSc3	vlak
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
znovu	znovu	k6eAd1	znovu
s	s	k7c7	s
Ronem	ron	k1gInSc7	ron
nasednout	nasednout	k5eAaPmF	nasednout
do	do	k7c2	do
létajícího	létající	k2eAgNnSc2d1	létající
auta	auto	k1gNnSc2	auto
a	a	k8xC	a
letět	letět	k5eAaImF	letět
do	do	k7c2	do
Bradavic	bradavice	k1gFnPc2	bradavice
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
tom	ten	k3xDgNnSc6	ten
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
podaří	podařit	k5eAaPmIp3nS	podařit
auto	auto	k1gNnSc4	auto
rozbít	rozbít	k5eAaPmF	rozbít
<g/>
.	.	kIx.	.
</s>
<s>
Hrozí	hrozit	k5eAaImIp3nS	hrozit
jim	on	k3xPp3gMnPc3	on
znovu	znovu	k6eAd1	znovu
vyloučení	vyloučení	k1gNnSc1	vyloučení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
profesorka	profesorka	k1gFnSc1	profesorka
McGonagallová	McGonagallová	k1gFnSc1	McGonagallová
přimhouří	přimhouřit	k5eAaPmIp3nS	přimhouřit
obě	dva	k4xCgNnPc4	dva
oči	oko	k1gNnPc4	oko
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gInPc4	Harr
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
disponuje	disponovat	k5eAaBmIp3nS	disponovat
tzv.	tzv.	kA	tzv.
hadím	hadí	k2eAgInSc7d1	hadí
jazykem	jazyk	k1gInSc7	jazyk
(	(	kIx(	(
<g/>
umí	umět	k5eAaImIp3nS	umět
mluvit	mluvit	k5eAaImF	mluvit
s	s	k7c7	s
hady	had	k1gMnPc7	had
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Studenti	student	k1gMnPc1	student
z	z	k7c2	z
mudlovských	mudlovský	k2eAgFnPc2d1	mudlovská
rodin	rodina	k1gFnPc2	rodina
kamení	kamení	k1gNnSc2	kamení
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgFnSc1d1	poslední
oběť	oběť	k1gFnSc1	oběť
je	být	k5eAaImIp3nS	být
jejich	jejich	k3xOp3gFnSc1	jejich
kamarádka	kamarádka	k1gFnSc1	kamarádka
Hermiona	Hermiona	k1gFnSc1	Hermiona
Grangerová	Grangerová	k1gFnSc1	Grangerová
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ta	ten	k3xDgFnSc1	ten
jim	on	k3xPp3gMnPc3	on
zanechává	zanechávat	k5eAaImIp3nS	zanechávat
záchytný	záchytný	k2eAgInSc4d1	záchytný
bod	bod	k1gInSc4	bod
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gInPc1	Harr
s	s	k7c7	s
Ronem	ron	k1gInSc7	ron
se	se	k3xPyFc4	se
dostávají	dostávat	k5eAaImIp3nP	dostávat
do	do	k7c2	do
bájné	bájný	k2eAgFnSc2d1	bájná
tajemné	tajemný	k2eAgFnSc2d1	tajemná
komnaty	komnata	k1gFnSc2	komnata
a	a	k8xC	a
Harry	Harra	k1gFnSc2	Harra
přemůže	přemoct	k5eAaPmIp3nS	přemoct
Voldemorta	Voldemort	k1gMnSc4	Voldemort
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgMnS	chtít
na	na	k7c4	na
svět	svět	k1gInSc4	svět
vrátit	vrátit	k5eAaPmF	vrátit
pomocí	pomocí	k7c2	pomocí
svého	svůj	k3xOyFgInSc2	svůj
starého	starý	k2eAgInSc2d1	starý
deníku	deník	k1gInSc2	deník
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zachrání	zachránit	k5eAaPmIp3nS	zachránit
Ginny	Ginna	k1gMnSc2	Ginna
–	–	k?	–
sestru	sestra	k1gFnSc4	sestra
Rona	Ron	k1gMnSc2	Ron
Weasleyho	Weasley	k1gMnSc2	Weasley
<g/>
.	.	kIx.	.
</s>
<s>
Zkamenělí	zkamenělý	k2eAgMnPc1d1	zkamenělý
studenti	student	k1gMnPc1	student
znovu	znovu	k6eAd1	znovu
ožijí	ožít	k5eAaPmIp3nP	ožít
díky	díky	k7c3	díky
lektvaru	lektvar	k1gInSc3	lektvar
z	z	k7c2	z
Mandragor	mandragora	k1gFnPc2	mandragora
(	(	kIx(	(
<g/>
Pekřínu	Pekřína	k1gFnSc4	Pekřína
<g/>
)	)	kIx)	)
které	který	k3yQgNnSc1	který
připravila	připravit	k5eAaPmAgFnS	připravit
Pomora	pomora	k1gFnSc1	pomora
Prýtová	Prýtová	k1gFnSc1	Prýtová
<g/>
,	,	kIx,	,
profesorka	profesorka	k1gFnSc1	profesorka
bylinkářství	bylinkářství	k1gNnSc2	bylinkářství
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
madame	madame	k1gFnSc7	madame
Pomfreyovou	Pomfreyová	k1gFnSc7	Pomfreyová
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
školy	škola	k1gFnSc2	škola
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
hajný	hajný	k1gMnSc1	hajný
Hagrid	Hagrid	k1gInSc1	Hagrid
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
vsazen	vsadit	k5eAaPmNgInS	vsadit
do	do	k7c2	do
Azkabanu	Azkaban	k1gInSc2	Azkaban
za	za	k7c4	za
podezření	podezření	k1gNnSc4	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
on	on	k3xPp3gMnSc1	on
vypustil	vypustit	k5eAaPmAgMnS	vypustit
baziliška	bazilišek	k1gMnSc4	bazilišek
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
vrací	vracet	k5eAaImIp3nS	vracet
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
rodině	rodina	k1gFnSc3	rodina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Harry	Harra	k1gMnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
a	a	k8xC	a
Vězeň	vězeň	k1gMnSc1	vězeň
z	z	k7c2	z
Azkabanu	Azkaban	k1gInSc2	Azkaban
===	===	k?	===
</s>
</p>
<p>
<s>
Harry	Harr	k1gInPc1	Harr
se	se	k3xPyFc4	se
začátkem	začátkem	k7c2	začátkem
prázdnin	prázdniny	k1gFnPc2	prázdniny
dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
utekl	utéct	k5eAaPmAgMnS	utéct
nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
zločinec	zločinec	k1gMnSc1	zločinec
Sirius	Sirius	k1gMnSc1	Sirius
Black	Black	k1gMnSc1	Black
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
návštěvu	návštěva	k1gFnSc4	návštěva
k	k	k7c3	k
Dursleyovým	Dursleyová	k1gFnPc3	Dursleyová
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
tetička	tetička	k1gFnSc1	tetička
Marge	Marge	k1gFnSc1	Marge
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
Harryho	Harry	k1gMnSc4	Harry
vyprovokuje	vyprovokovat	k5eAaPmIp3nS	vyprovokovat
a	a	k8xC	a
Harry	Harr	k1gInPc4	Harr
ji	on	k3xPp3gFnSc4	on
kouzlem	kouzlo	k1gNnSc7	kouzlo
nafoukne	nafouknout	k5eAaPmIp3nS	nafouknout
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc4	Harra
utíká	utíkat	k5eAaImIp3nS	utíkat
z	z	k7c2	z
domova	domov	k1gInSc2	domov
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
strach	strach	k1gInSc4	strach
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
vyloučen	vyloučen	k2eAgMnSc1d1	vyloučen
z	z	k7c2	z
Bradavic	bradavice	k1gFnPc2	bradavice
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
prázdnin	prázdniny	k1gFnPc2	prázdniny
stráví	strávit	k5eAaPmIp3nS	strávit
v	v	k7c6	v
Děravém	děravý	k2eAgInSc6d1	děravý
kotli	kotel	k1gInSc6	kotel
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
školy	škola	k1gFnSc2	škola
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
hrozivé	hrozivý	k2eAgNnSc1d1	hrozivé
stvoření	stvoření	k1gNnSc1	stvoření
je	být	k5eAaImIp3nS	být
mozkomor	mozkomor	k1gInSc4	mozkomor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
se	se	k3xPyFc4	se
mluví	mluvit	k5eAaImIp3nS	mluvit
jenom	jenom	k9	jenom
o	o	k7c6	o
tom	ten	k3xDgMnSc6	ten
uprchlém	uprchlý	k2eAgMnSc6d1	uprchlý
zločinci	zločinec	k1gMnSc6	zločinec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
utekl	utéct	k5eAaPmAgMnS	utéct
z	z	k7c2	z
Azkabanu	Azkaban	k1gInSc2	Azkaban
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgMnPc1d1	ostatní
studenti	student	k1gMnPc1	student
navštěvují	navštěvovat	k5eAaImIp3nP	navštěvovat
vesnici	vesnice	k1gFnSc4	vesnice
Prasinky	Prasinka	k1gFnSc2	Prasinka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
leží	ležet	k5eAaImIp3nS	ležet
kousek	kousek	k1gInSc4	kousek
od	od	k7c2	od
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gMnPc4	Harr
tam	tam	k6eAd1	tam
nesmí	smět	k5eNaImIp3nS	smět
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dvojčata	dvojče	k1gNnPc1	dvojče
Weasleyovi	Weasleya	k1gMnSc3	Weasleya
mu	on	k3xPp3gMnSc3	on
darují	darovat	k5eAaPmIp3nP	darovat
Pobertův	pobertův	k2eAgInSc4d1	pobertův
plánek	plánek	k1gInSc4	plánek
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
Harryho	Harry	k1gMnSc2	Harry
otec	otec	k1gMnSc1	otec
(	(	kIx(	(
<g/>
Dvanácterák	dvanácterák	k1gMnSc1	dvanácterák
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
tři	tři	k4xCgMnPc1	tři
nejlepší	dobrý	k2eAgMnPc1d3	nejlepší
přátelé	přítel	k1gMnPc1	přítel
–	–	k?	–
Peter	Peter	k1gMnSc1	Peter
Pettigrew	Pettigrew	k1gMnSc1	Pettigrew
<g/>
,	,	kIx,	,
Sirius	Sirius	k1gMnSc1	Sirius
Black	Black	k1gMnSc1	Black
a	a	k8xC	a
Remus	Remus	k1gMnSc1	Remus
Lupin	lupina	k1gFnPc2	lupina
(	(	kIx(	(
<g/>
Červíček	červíček	k1gMnSc1	červíček
<g/>
,	,	kIx,	,
Tichošlápek	tichošlápek	k1gMnSc1	tichošlápek
<g/>
,	,	kIx,	,
Náměsíčník	náměsíčník	k1gMnSc1	náměsíčník
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
řeknou	říct	k5eAaPmIp3nP	říct
mu	on	k3xPp3gMnSc3	on
o	o	k7c6	o
tajných	tajný	k2eAgInPc6d1	tajný
východech	východ	k1gInPc6	východ
z	z	k7c2	z
hradu	hrad	k1gInSc2	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
svému	svůj	k3xOyFgInSc3	svůj
neviditelnému	viditelný	k2eNgInSc3d1	neviditelný
plášti	plášť	k1gInSc3	plášť
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
Prasinek	Prasinka	k1gFnPc2	Prasinka
a	a	k8xC	a
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
uprchlý	uprchlý	k1gMnSc1	uprchlý
trestanec	trestanec	k1gMnSc1	trestanec
Sirius	Sirius	k1gMnSc1	Sirius
Black	Black	k1gMnSc1	Black
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gMnSc1	jeho
kmotr	kmotr	k1gMnSc1	kmotr
a	a	k8xC	a
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
kladou	klást	k5eAaImIp3nP	klást
za	za	k7c4	za
vinu	vina	k1gFnSc4	vina
smrt	smrt	k1gFnSc4	smrt
jeho	jeho	k3xOp3gMnPc2	jeho
rodičů	rodič	k1gMnPc2	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
školního	školní	k2eAgInSc2d1	školní
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
Harry	Harra	k1gFnSc2	Harra
setká	setkat	k5eAaPmIp3nS	setkat
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
kmotrem	kmotr	k1gMnSc7	kmotr
a	a	k8xC	a
ujasní	ujasnit	k5eAaPmIp3nS	ujasnit
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
to	ten	k3xDgNnSc1	ten
vlastně	vlastně	k9	vlastně
bylo	být	k5eAaImAgNnS	být
a	a	k8xC	a
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Ronova	Ronův	k2eAgFnSc1d1	Ronova
krysa	krysa	k1gFnSc1	krysa
je	být	k5eAaImIp3nS	být
zvěromág	zvěromág	k1gMnSc1	zvěromág
Peter	Peter	k1gMnSc1	Peter
Pettigrew	Pettigrew	k1gMnSc1	Pettigrew
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
smrt	smrt	k1gFnSc4	smrt
jeho	jeho	k3xOp3gMnPc2	jeho
rodičů	rodič	k1gMnPc2	rodič
–	–	k?	–
tomuto	tento	k3xDgInSc3	tento
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
podaří	podařit	k5eAaPmIp3nS	podařit
uprchnout	uprchnout	k5eAaPmF	uprchnout
<g/>
.	.	kIx.	.
</s>
<s>
Sirius	Sirius	k1gInSc1	Sirius
prchá	prchat	k5eAaImIp3nS	prchat
na	na	k7c6	na
hipogryfovi	hipogryf	k1gMnSc6	hipogryf
Klofanovi	Klofan	k1gMnSc6	Klofan
<g/>
,	,	kIx,	,
nespravedlivě	spravedlivě	k6eNd1	spravedlivě
odsouzenému	odsouzený	k1gMnSc3	odsouzený
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
za	za	k7c2	za
napadení	napadení	k1gNnSc2	napadení
Draca	Dracus	k1gMnSc2	Dracus
Malfoye	Malfoy	k1gMnSc2	Malfoy
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
nucen	nucen	k2eAgMnSc1d1	nucen
se	se	k3xPyFc4	se
skrývat	skrývat	k5eAaImF	skrývat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
školního	školní	k2eAgInSc2d1	školní
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
Harry	Harr	k1gInPc7	Harr
vrací	vracet	k5eAaImIp3nS	vracet
k	k	k7c3	k
Dursleyovým	Dursleyová	k1gFnPc3	Dursleyová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Harry	Harra	k1gMnSc2	Harra
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
Ohnivý	ohnivý	k2eAgInSc1d1	ohnivý
pohár	pohár	k1gInSc1	pohár
===	===	k?	===
</s>
</p>
<p>
<s>
Když	když	k8xS	když
Harry	Harr	k1gInPc1	Harr
přijede	přijet	k5eAaPmIp3nS	přijet
do	do	k7c2	do
Bradavic	bradavice	k1gFnPc2	bradavice
<g/>
,	,	kIx,	,
Brumbál	brumbál	k1gMnSc1	brumbál
studentům	student	k1gMnPc3	student
prozradí	prozradit	k5eAaPmIp3nP	prozradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
bude	být	k5eAaImBp3nS	být
konat	konat	k5eAaImF	konat
Turnaj	turnaj	k1gInSc1	turnaj
tří	tři	k4xCgMnPc2	tři
kouzelníků	kouzelník	k1gMnPc2	kouzelník
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Brumbál	brumbál	k1gMnSc1	brumbál
vyloví	vylovit	k5eAaPmIp3nS	vylovit
Harryho	Harry	k1gMnSc2	Harry
jméno	jméno	k1gNnSc1	jméno
z	z	k7c2	z
Ohnivého	ohnivý	k2eAgInSc2d1	ohnivý
poháru	pohár	k1gInSc2	pohár
<g/>
,	,	kIx,	,
začnou	začít	k5eAaPmIp3nP	začít
mu	on	k3xPp3gInSc3	on
všichni	všechen	k3xTgMnPc1	všechen
včetně	včetně	k7c2	včetně
Rona	Ron	k1gMnSc2	Ron
křivdit	křivdit	k5eAaImF	křivdit
a	a	k8xC	a
říkat	říkat	k5eAaImF	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
podvodník	podvodník	k1gMnSc1	podvodník
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc4	Harra
však	však	k9	však
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
do	do	k7c2	do
poháru	pohár	k1gInSc2	pohár
nehodil	hodit	k5eNaPmAgInS	hodit
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
během	během	k7c2	během
školního	školní	k2eAgInSc2d1	školní
roku	rok	k1gInSc2	rok
splnil	splnit	k5eAaPmAgInS	splnit
dva	dva	k4xCgInPc4	dva
úkoly	úkol	k1gInPc4	úkol
Turnaje	turnaj	k1gInSc2	turnaj
<g/>
,	,	kIx,	,
při	při	k7c6	při
třetím	třetí	k4xOgInSc6	třetí
úkolu	úkol	k1gInSc6	úkol
se	se	k3xPyFc4	se
s	s	k7c7	s
Cedrikem	Cedrik	k1gMnSc7	Cedrik
Diggorym	Diggorym	k1gInSc4	Diggorym
přenese	přenést	k5eAaPmIp3nS	přenést
z	z	k7c2	z
bludiště	bludiště	k1gNnSc2	bludiště
pomocí	pomocí	k7c2	pomocí
přenášedla	přenášedlo	k1gNnSc2	přenášedlo
(	(	kIx(	(
<g/>
Pohár	pohár	k1gInSc4	pohár
tří	tři	k4xCgMnPc2	tři
kouzelníků	kouzelník	k1gMnPc2	kouzelník
<g/>
)	)	kIx)	)
na	na	k7c4	na
hřbitov	hřbitov	k1gInSc4	hřbitov
v	v	k7c6	v
Malém	malý	k2eAgInSc6d1	malý
Visánku	Visánek	k1gInSc6	Visánek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
svědkem	svědek	k1gMnSc7	svědek
Voldemortova	Voldemortův	k2eAgInSc2d1	Voldemortův
návratu	návrat	k1gInSc2	návrat
<g/>
.	.	kIx.	.
</s>
<s>
Cedrik	Cedrik	k1gMnSc1	Cedrik
Diggory	Diggora	k1gFnSc2	Diggora
je	být	k5eAaImIp3nS	být
zabit	zabít	k5eAaPmNgInS	zabít
Petrem	Petr	k1gMnSc7	Petr
Pettigrewem	Pettigrew	k1gMnSc7	Pettigrew
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnSc2	Harra
se	se	k3xPyFc4	se
dokáže	dokázat	k5eAaPmIp3nS	dokázat
vrátit	vrátit	k5eAaPmF	vrátit
i	i	k9	i
s	s	k7c7	s
Cedrikovým	Cedrikův	k2eAgNnSc7d1	Cedrikovo
tělem	tělo	k1gNnSc7	tělo
zpět	zpět	k6eAd1	zpět
na	na	k7c6	na
bradavické	bradavická	k1gFnSc6	bradavická
pozemky	pozemek	k1gInPc4	pozemek
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
dozvídá	dozvídat	k5eAaImIp3nS	dozvídat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
vhodil	vhodit	k5eAaPmAgInS	vhodit
do	do	k7c2	do
Ohnivého	ohnivý	k2eAgInSc2d1	ohnivý
poháru	pohár	k1gInSc2	pohár
Barty	Barta	k1gFnSc2	Barta
Skrk	Skrk	k1gMnSc1	Skrk
mladší	mladý	k2eAgMnSc1d2	mladší
–	–	k?	–
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
mnoholičného	mnoholičný	k2eAgInSc2d1	mnoholičný
lektvaru	lektvar	k1gInSc2	lektvar
vydával	vydávat	k5eAaImAgInS	vydávat
za	za	k7c4	za
Alastora	Alastor	k1gMnSc4	Alastor
Moodyho	Moody	k1gMnSc4	Moody
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
políben	políbit	k5eAaPmNgInS	políbit
Mozkomorem	Mozkomor	k1gInSc7	Mozkomor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Harry	Harra	k1gMnSc2	Harra
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
Fénixův	fénixův	k2eAgInSc1d1	fénixův
řád	řád	k1gInSc1	řád
===	===	k?	===
</s>
</p>
<p>
<s>
Harryho	Harryze	k6eAd1	Harryze
vyzvednou	vyzvednout	k5eAaPmIp3nP	vyzvednout
od	od	k7c2	od
tety	teta	k1gFnSc2	teta
a	a	k8xC	a
strýce	strýc	k1gMnSc2	strýc
členové	člen	k1gMnPc1	člen
Fénixova	fénixův	k2eAgInSc2d1	fénixův
řádu	řád	k1gInSc2	řád
a	a	k8xC	a
dopraví	dopravit	k5eAaPmIp3nS	dopravit
na	na	k7c4	na
Grimmauldovo	Grimmauldův	k2eAgNnSc4d1	Grimmauldovo
náměstí	náměstí	k1gNnSc4	náměstí
12	[number]	k4	12
<g/>
,	,	kIx,	,
do	do	k7c2	do
domu	dům	k1gInSc2	dům
jeho	on	k3xPp3gMnSc2	on
kmotra	kmotr	k1gMnSc2	kmotr
<g/>
,	,	kIx,	,
Siriuse	Siriuse	k1gFnSc1	Siriuse
Blacka	Blacka	k1gFnSc1	Blacka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
kouzelnickém	kouzelnický	k2eAgInSc6d1	kouzelnický
i	i	k8xC	i
nekouzelnickém	kouzelnický	k2eNgInSc6d1	kouzelnický
světě	svět	k1gInSc6	svět
se	se	k3xPyFc4	se
dějí	dít	k5eAaImIp3nP	dít
hrozné	hrozný	k2eAgFnPc1d1	hrozná
věci	věc	k1gFnPc1	věc
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nP	vracet
do	do	k7c2	do
Bradavic	bradavice	k1gFnPc2	bradavice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
skoro	skoro	k6eAd1	skoro
všichni	všechen	k3xTgMnPc1	všechen
věří	věřit	k5eAaImIp3nP	věřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
lže	lhát	k5eAaImIp3nS	lhát
<g/>
,	,	kIx,	,
když	když	k8xS	když
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
lord	lord	k1gMnSc1	lord
Voldemort	Voldemort	k1gInSc4	Voldemort
vrátil	vrátit	k5eAaPmAgMnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Založí	založit	k5eAaPmIp3nS	založit
tajný	tajný	k2eAgInSc1d1	tajný
spolek	spolek	k1gInSc1	spolek
s	s	k7c7	s
názvem	název	k1gInSc7	název
Brumbálova	brumbálův	k2eAgFnSc1d1	Brumbálova
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Harry	Harra	k1gFnPc1	Harra
učí	učit	k5eAaImIp3nP	učit
ostatní	ostatní	k2eAgMnPc4d1	ostatní
studenty	student	k1gMnPc4	student
bránit	bránit	k5eAaImF	bránit
se	se	k3xPyFc4	se
proti	proti	k7c3	proti
černé	černý	k2eAgFnSc3d1	černá
magii	magie	k1gFnSc3	magie
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
snu	sen	k1gInSc6	sen
pokouše	pokousat	k5eAaPmIp3nS	pokousat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
hada	had	k1gMnSc2	had
Artura	Artur	k1gMnSc2	Artur
Weasleyho	Weasley	k1gMnSc2	Weasley
<g/>
,	,	kIx,	,
a	a	k8xC	a
když	když	k8xS	když
se	se	k3xPyFc4	se
probudí	probudit	k5eAaPmIp3nS	probudit
<g/>
,	,	kIx,	,
zburcuje	zburcovat	k5eAaPmIp3nS	zburcovat
Brumbála	brumbál	k1gMnSc4	brumbál
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Artur	Artur	k1gMnSc1	Artur
v	v	k7c6	v
nebezpečí	nebezpečí	k1gNnSc6	nebezpečí
<g/>
,	,	kIx,	,
a	a	k8xC	a
doopravdy	doopravdy	k6eAd1	doopravdy
ho	on	k3xPp3gMnSc4	on
najdou	najít	k5eAaPmIp3nP	najít
ve	v	k7c6	v
vážném	vážný	k2eAgInSc6d1	vážný
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
živého	živý	k1gMnSc4	živý
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zdá	zdát	k5eAaPmIp3nS	zdát
<g/>
,	,	kIx,	,
že	že	k8xS	že
Voldemort	Voldemort	k1gInSc1	Voldemort
drží	držet	k5eAaImIp3nS	držet
Siriuse	Siriuse	k1gFnPc4	Siriuse
na	na	k7c6	na
Ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
kouzel	kouzlo	k1gNnPc2	kouzlo
v	v	k7c6	v
Odboru	odbor	k1gInSc6	odbor
záhad	záhada	k1gFnPc2	záhada
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
s	s	k7c7	s
Ronem	ron	k1gInSc7	ron
Weasleym	Weasleymum	k1gNnPc2	Weasleymum
<g/>
,	,	kIx,	,
Hermionou	Hermiona	k1gFnSc7	Hermiona
Grangerovou	Grangerová	k1gFnSc7	Grangerová
<g/>
,	,	kIx,	,
Lenkou	Lenka	k1gFnSc7	Lenka
Láskorádovou	Láskorádový	k2eAgFnSc7d1	Láskorádová
<g/>
,	,	kIx,	,
Nevillem	Nevill	k1gMnSc7	Nevill
Longbottomem	Longbottom	k1gMnSc7	Longbottom
a	a	k8xC	a
Ginny	Ginn	k1gMnPc7	Ginn
Weasleyovou	Weasleyová	k1gFnSc7	Weasleyová
vypraví	vypravit	k5eAaPmIp3nP	vypravit
ho	on	k3xPp3gInSc4	on
zachránit	zachránit	k5eAaPmF	zachránit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
netuší	tušit	k5eNaImIp3nP	tušit
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
lest	lest	k1gFnSc1	lest
<g/>
.	.	kIx.	.
</s>
<s>
Čekají	čekat	k5eAaImIp3nP	čekat
tam	tam	k6eAd1	tam
na	na	k7c4	na
něj	on	k3xPp3gNnSc4	on
Smrtijedi	Smrtijed	k1gMnPc1	Smrtijed
a	a	k8xC	a
chtějí	chtít	k5eAaImIp3nP	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
jim	on	k3xPp3gMnPc3	on
dal	dát	k5eAaPmAgMnS	dát
věštbu	věštba	k1gFnSc4	věštba
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
tam	tam	k6eAd1	tam
našel	najít	k5eAaPmAgInS	najít
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
tam	tam	k6eAd1	tam
přijdou	přijít	k5eAaPmIp3nP	přijít
členové	člen	k1gMnPc1	člen
Fénixova	fénixův	k2eAgInSc2d1	fénixův
řádu	řád	k1gInSc2	řád
a	a	k8xC	a
bojují	bojovat	k5eAaImIp3nP	bojovat
se	s	k7c7	s
Smrtijedy	Smrtijed	k1gMnPc7	Smrtijed
<g/>
.	.	kIx.	.
</s>
<s>
Smrtijedka	Smrtijedka	k1gFnSc1	Smrtijedka
Belatrix	Belatrix	k1gInSc1	Belatrix
Lestrangeová	Lestrangeová	k1gFnSc1	Lestrangeová
zabije	zabít	k5eAaPmIp3nS	zabít
Siriuse	Siriuse	k1gFnPc4	Siriuse
a	a	k8xC	a
poté	poté	k6eAd1	poté
uprchne	uprchnout	k5eAaPmIp3nS	uprchnout
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gInPc1	Harr
se	se	k3xPyFc4	se
za	za	k7c2	za
ní	on	k3xPp3gFnSc2	on
vydá	vydat	k5eAaPmIp3nS	vydat
a	a	k8xC	a
v	v	k7c6	v
atriu	atrium	k1gNnSc6	atrium
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
kouzel	kouzlo	k1gNnPc2	kouzlo
se	se	k3xPyFc4	se
setká	setkat	k5eAaPmIp3nS	setkat
s	s	k7c7	s
Voldemortem	Voldemort	k1gInSc7	Voldemort
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
ho	on	k3xPp3gInSc2	on
znovu	znovu	k6eAd1	znovu
pokusí	pokusit	k5eAaPmIp3nS	pokusit
zabít	zabít	k5eAaPmF	zabít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
objeví	objevit	k5eAaPmIp3nS	objevit
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
Brumbál	brumbál	k1gMnSc1	brumbál
a	a	k8xC	a
Harryho	Harry	k1gMnSc4	Harry
zachrání	zachránit	k5eAaPmIp3nP	zachránit
<g/>
.	.	kIx.	.
</s>
<s>
Voldemort	Voldemort	k1gInSc1	Voldemort
zmizí	zmizet	k5eAaPmIp3nS	zmizet
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pracovníci	pracovník	k1gMnPc1	pracovník
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
kouzel	kouzlo	k1gNnPc2	kouzlo
ho	on	k3xPp3gMnSc4	on
zahlédnou	zahlédnout	k5eAaPmIp3nP	zahlédnout
a	a	k8xC	a
dosvědčí	dosvědčit	k5eAaPmIp3nP	dosvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
opravdu	opravdu	k6eAd1	opravdu
vrátil	vrátit	k5eAaPmAgMnS	vrátit
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnSc2	Harra
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
zpět	zpět	k6eAd1	zpět
k	k	k7c3	k
Dursleyovým	Dursleyův	k2eAgFnPc3d1	Dursleyův
zarmoucen	zarmoutit	k5eAaPmNgMnS	zarmoutit
smrtí	smrt	k1gFnSc7	smrt
svého	svůj	k3xOyFgMnSc2	svůj
milovaného	milovaný	k2eAgMnSc2d1	milovaný
kmotra	kmotr	k1gMnSc2	kmotr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Harry	Harra	k1gMnSc2	Harra
Potter	Potter	k1gMnSc1	Potter
a	a	k8xC	a
Princ	princ	k1gMnSc1	princ
dvojí	dvojí	k4xRgFnSc2	dvojí
krve	krev	k1gFnSc2	krev
===	===	k?	===
</s>
</p>
<p>
<s>
Harry	Harra	k1gFnPc1	Harra
se	se	k3xPyFc4	se
v	v	k7c6	v
šestém	šestý	k4xOgInSc6	šestý
dílu	díl	k1gInSc6	díl
setká	setkat	k5eAaPmIp3nS	setkat
s	s	k7c7	s
Horaciem	Horacium	k1gNnSc7	Horacium
Křiklanem	Křiklan	k1gMnSc7	Křiklan
(	(	kIx(	(
<g/>
bývalým	bývalý	k2eAgMnSc7d1	bývalý
učitelem	učitel	k1gMnSc7	učitel
lektvarů	lektvar	k1gInPc2	lektvar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gMnSc4	on
přiměl	přimět	k5eAaPmAgInS	přimět
vrátit	vrátit	k5eAaPmF	vrátit
se	se	k3xPyFc4	se
do	do	k7c2	do
Bradavic	bradavice	k1gFnPc2	bradavice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Křiklan	Křiklan	k1gInSc1	Křiklan
zná	znát	k5eAaImIp3nS	znát
nějaké	nějaký	k3yIgNnSc1	nějaký
tajemství	tajemství	k1gNnSc1	tajemství
o	o	k7c6	o
Voldemortovi	Voldemort	k1gMnSc6	Voldemort
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nechce	chtít	k5eNaImIp3nS	chtít
ho	on	k3xPp3gMnSc4	on
vyzradit	vyzradit	k5eAaPmF	vyzradit
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc4	Harra
najde	najít	k5eAaPmIp3nS	najít
učebnici	učebnice	k1gFnSc4	učebnice
lektvarů	lektvar	k1gInPc2	lektvar
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc2	který
jsou	být	k5eAaImIp3nP	být
poznámky	poznámka	k1gFnPc1	poznámka
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
kterým	který	k3yQgMnPc3	který
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
mistrem	mistr	k1gMnSc7	mistr
v	v	k7c6	v
lektvarech	lektvar	k1gInPc6	lektvar
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
majetkem	majetek	k1gInSc7	majetek
prince	princ	k1gMnSc2	princ
dvojí	dvojí	k4xRgFnSc2	dvojí
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Ron	Ron	k1gMnSc1	Ron
si	se	k3xPyFc3	se
najde	najít	k5eAaPmIp3nS	najít
přítelkyni	přítelkyně	k1gFnSc4	přítelkyně
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
opravdu	opravdu	k6eAd1	opravdu
zábavná	zábavný	k2eAgFnSc1d1	zábavná
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc4	Harra
dostane	dostat	k5eAaPmIp3nS	dostat
od	od	k7c2	od
Romildy	Romilda	k1gFnSc2	Romilda
Vaneové	Vaneové	k2eAgInPc4d1	Vaneové
bonbony	bonbon	k1gInPc4	bonbon
s	s	k7c7	s
lektvarem	lektvar	k1gInSc7	lektvar
lásky	láska	k1gFnSc2	láska
a	a	k8xC	a
Ron	Ron	k1gMnSc1	Ron
je	on	k3xPp3gInPc4	on
všechny	všechen	k3xTgInPc4	všechen
sní	snít	k5eAaImIp3nS	snít
a	a	k8xC	a
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harr	k1gMnPc4	Harr
jde	jít	k5eAaImIp3nS	jít
s	s	k7c7	s
Ronem	Ron	k1gMnSc7	Ron
ke	k	k7c3	k
Křiklanovi	Křiklan	k1gMnSc3	Křiklan
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
dal	dát	k5eAaPmAgInS	dát
protilék	protilék	k1gInSc1	protilék
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
si	se	k3xPyFc3	se
chtějí	chtít	k5eAaImIp3nP	chtít
dát	dát	k5eAaPmF	dát
medovinu	medovina	k1gFnSc4	medovina
na	na	k7c4	na
povzbuzení	povzbuzení	k1gNnSc4	povzbuzení
<g/>
,	,	kIx,	,
medovina	medovina	k1gFnSc1	medovina
je	být	k5eAaImIp3nS	být
však	však	k9	však
otrávená	otrávený	k2eAgFnSc1d1	otrávená
<g/>
,	,	kIx,	,
a	a	k8xC	a
Ron	ron	k1gInSc1	ron
skončí	skončit	k5eAaPmIp3nS	skončit
na	na	k7c6	na
ošetřovně	ošetřovna	k1gFnSc6	ošetřovna
<g/>
.	.	kIx.	.
</s>
<s>
Hermiona	Hermiona	k1gFnSc1	Hermiona
se	se	k3xPyFc4	se
zamiluje	zamilovat	k5eAaPmIp3nS	zamilovat
do	do	k7c2	do
Rona	Ron	k1gMnSc2	Ron
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jí	on	k3xPp3gFnSc3	on
mrzí	mrzet	k5eAaImIp3nS	mrzet
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
Ron	ron	k1gInSc1	ron
přítelkyni	přítelkyně	k1gFnSc4	přítelkyně
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
mu	on	k3xPp3gMnSc3	on
ublížit	ublížit	k5eAaPmF	ublížit
<g/>
,	,	kIx,	,
skončí	skončit	k5eAaPmIp3nS	skončit
to	ten	k3xDgNnSc1	ten
ale	ale	k9	ale
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
začnou	začít	k5eAaPmIp3nP	začít
znovu	znovu	k6eAd1	znovu
kamarádit	kamarádit	k5eAaImF	kamarádit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
tu	tu	k6eAd1	tu
také	také	k9	také
hraje	hrát	k5eAaImIp3nS	hrát
Malfoy	Malfoa	k1gFnPc4	Malfoa
(	(	kIx(	(
<g/>
Draco	Draco	k6eAd1	Draco
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
dělá	dělat	k5eAaImIp3nS	dělat
věci	věc	k1gFnPc4	věc
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gInSc3	on
příčí	příčit	k5eAaImIp3nS	příčit
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
zachránil	zachránit	k5eAaPmAgMnS	zachránit
svou	svůj	k3xOyFgFnSc4	svůj
rodinu	rodina	k1gFnSc4	rodina
i	i	k8xC	i
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
velice	velice	k6eAd1	velice
líto	líto	k6eAd1	líto
<g/>
,	,	kIx,	,
že	že	k8xS	že
musí	muset	k5eAaImIp3nP	muset
zabít	zabít	k5eAaPmF	zabít
Albuse	Albuse	k1gFnPc4	Albuse
Brumbála	brumbál	k1gMnSc2	brumbál
a	a	k8xC	a
pustit	pustit	k5eAaPmF	pustit
na	na	k7c4	na
hrad	hrad	k1gInSc4	hrad
smrtijedy	smrtijeda	k1gMnSc2	smrtijeda
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
zničit	zničit	k5eAaPmF	zničit
Harryho	Harry	k1gMnSc4	Harry
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Brumbál	brumbál	k1gMnSc1	brumbál
na	na	k7c6	na
konci	konec	k1gInSc6	konec
opravdu	opravdu	k6eAd1	opravdu
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
vinou	vina	k1gFnSc7	vina
Malfoye	Malfoy	k1gInSc2	Malfoy
<g/>
.	.	kIx.	.
</s>
<s>
Zabije	zabít	k5eAaPmIp3nS	zabít
ho	on	k3xPp3gInSc4	on
totiž	totiž	k9	totiž
Severus	Severus	k1gMnSc1	Severus
Snape	Snap	k1gMnSc5	Snap
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
on	on	k3xPp3gMnSc1	on
byl	být	k5eAaImAgMnS	být
princem	princ	k1gMnSc7	princ
dvojí	dvojí	k4xRgFnSc2	dvojí
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Harry	Harra	k1gMnSc2	Harra
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
Relikvie	relikvie	k1gFnSc1	relikvie
smrti	smrt	k1gFnSc2	smrt
===	===	k?	===
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
Harryho	Harry	k1gMnSc4	Harry
přijdou	přijít	k5eAaPmIp3nP	přijít
členové	člen	k1gMnPc1	člen
Fénixova	fénixův	k2eAgInSc2d1	fénixův
řádu	řád	k1gInSc2	řád
a	a	k8xC	a
eskortují	eskortovat	k5eAaBmIp3nP	eskortovat
ho	on	k3xPp3gMnSc4	on
k	k	k7c3	k
rodičům	rodič	k1gMnPc3	rodič
Nymfadory	Nymfador	k1gInPc4	Nymfador
Tonksové	Tonksový	k2eAgInPc4d1	Tonksový
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyléčí	vyléčit	k5eAaPmIp3nS	vyléčit
ze	z	k7c2	z
zranění	zranění	k1gNnSc2	zranění
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
jede	jet	k5eAaImIp3nS	jet
přenášedlem	přenášedlo	k1gNnSc7	přenášedlo
do	do	k7c2	do
Doupěte	doupě	k1gNnSc2	doupě
(	(	kIx(	(
<g/>
dům	dům	k1gInSc1	dům
Weasleyových	Weasleyová	k1gFnPc2	Weasleyová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
setkat	setkat	k5eAaPmF	setkat
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
<g/>
,	,	kIx,	,
Pošuk	pošuk	k1gMnSc1	pošuk
Moody	Mooda	k1gFnSc2	Mooda
(	(	kIx(	(
<g/>
vinou	vina	k1gFnSc7	vina
Mundunguse	Mundunguse	k1gFnSc2	Mundunguse
Fletchera	Fletchera	k1gFnSc1	Fletchera
<g/>
)	)	kIx)	)
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
oslaví	oslavit	k5eAaPmIp3nP	oslavit
své	svůj	k3xOyFgFnPc4	svůj
narozeniny	narozeniny	k1gFnPc4	narozeniny
a	a	k8xC	a
dostane	dostat	k5eAaPmIp3nS	dostat
i	i	k9	i
s	s	k7c7	s
Ronem	ron	k1gInSc7	ron
a	a	k8xC	a
Hermionou	Hermiona	k1gFnSc7	Hermiona
dědictví	dědictví	k1gNnSc2	dědictví
po	po	k7c6	po
Brumbálovi	brumbál	k1gMnSc6	brumbál
<g/>
.	.	kIx.	.
</s>
<s>
Odejdou	odejít	k5eAaPmIp3nP	odejít
z	z	k7c2	z
doupěte	doupě	k1gNnSc2	doupě
a	a	k8xC	a
hledají	hledat	k5eAaImIp3nP	hledat
viteály	viteála	k1gFnPc4	viteála
<g/>
,	,	kIx,	,
pátrání	pátrání	k1gNnSc4	pátrání
je	být	k5eAaImIp3nS	být
zavede	zavést	k5eAaPmIp3nS	zavést
na	na	k7c4	na
Ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
kouzel	kouzlo	k1gNnPc2	kouzlo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
pod	pod	k7c7	pod
nadvládou	nadvláda	k1gFnSc7	nadvláda
Smrtijedů	Smrtijed	k1gMnPc2	Smrtijed
<g/>
.	.	kIx.	.
</s>
<s>
Viteál	Viteál	k1gInSc1	Viteál
(	(	kIx(	(
<g/>
Medailonek	medailonek	k1gInSc1	medailonek
Salazara	Salazar	k1gMnSc2	Salazar
Zmijozela	Zmijozela	k1gMnSc2	Zmijozela
<g/>
)	)	kIx)	)
najdou	najít	k5eAaPmIp3nP	najít
u	u	k7c2	u
Umbridgeové	Umbridgeová	k1gFnSc2	Umbridgeová
<g/>
.	.	kIx.	.
</s>
<s>
Skrývají	skrývat	k5eAaImIp3nP	skrývat
se	se	k3xPyFc4	se
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
čím	čí	k3xOyRgNnSc7	čí
dál	daleko	k6eAd2	daleko
víc	hodně	k6eAd2	hodně
hádají	hádat	k5eAaImIp3nP	hádat
a	a	k8xC	a
Ron	ron	k1gInSc4	ron
odejde	odejít	k5eAaPmIp3nS	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
a	a	k8xC	a
Hermiona	Hermiona	k1gFnSc1	Hermiona
se	se	k3xPyFc4	se
sami	sám	k3xTgMnPc1	sám
vydají	vydat	k5eAaPmIp3nP	vydat
do	do	k7c2	do
Godrikova	Godrikův	k2eAgInSc2d1	Godrikův
dolu	dol	k1gInSc2	dol
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
čeká	čekat	k5eAaImIp3nS	čekat
Nagini	Nagin	k1gMnPc1	Nagin
(	(	kIx(	(
<g/>
had	had	k1gMnSc1	had
lorda	lord	k1gMnSc4	lord
Voldemorta	Voldemort	k1gMnSc4	Voldemort
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
viteálů	viteál	k1gMnPc2	viteál
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
vlásek	vlásek	k1gInSc4	vlásek
uniknou	uniknout	k5eAaPmIp3nP	uniknout
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Ron	ron	k1gInSc1	ron
se	se	k3xPyFc4	se
vrátí	vrátit	k5eAaPmIp3nS	vrátit
<g/>
,	,	kIx,	,
zachrání	zachránit	k5eAaPmIp3nS	zachránit
Harryho	Harry	k1gMnSc4	Harry
a	a	k8xC	a
zničí	zničit	k5eAaPmIp3nS	zničit
medailonek	medailonek	k1gInSc4	medailonek
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
vydají	vydat	k5eAaPmIp3nP	vydat
za	za	k7c7	za
Xenofiliusem	Xenofilius	k1gMnSc7	Xenofilius
Láskorádem	Láskorád	k1gMnSc7	Láskorád
(	(	kIx(	(
<g/>
otec	otec	k1gMnSc1	otec
Lenky	Lenka	k1gFnSc2	Lenka
Láskorádové	Láskorádový	k2eAgInPc4d1	Láskorádový
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
jim	on	k3xPp3gMnPc3	on
vysvětlí	vysvětlit	k5eAaPmIp3nS	vysvětlit
význam	význam	k1gInSc1	význam
symbolu	symbol	k1gInSc2	symbol
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
němuž	jenž	k3xRgInSc3	jenž
se	se	k3xPyFc4	se
za	za	k7c7	za
ním	on	k3xPp3gNnSc7	on
vypravili	vypravit	k5eAaPmAgMnP	vypravit
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
se	se	k3xPyFc4	se
dopídí	dopídit	k5eAaPmIp3nP	dopídit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Brumbál	brumbál	k1gMnSc1	brumbál
mu	on	k3xPp3gMnSc3	on
odkázal	odkázat	k5eAaPmAgMnS	odkázat
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
viteálů	viteál	k1gMnPc2	viteál
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
relikvií	relikvie	k1gFnPc2	relikvie
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
vloupají	vloupat	k5eAaPmIp3nP	vloupat
ke	k	k7c3	k
Gringottovým	Gringottův	k2eAgInPc3d1	Gringottův
a	a	k8xC	a
získají	získat	k5eAaPmIp3nP	získat
další	další	k2eAgInSc4d1	další
viteál	viteál	k1gInSc4	viteál
<g/>
.	.	kIx.	.
</s>
<s>
Infiltrují	infiltrovat	k5eAaBmIp3nP	infiltrovat
Bradavice	bradavice	k1gFnPc4	bradavice
<g/>
.	.	kIx.	.
</s>
<s>
Rozpoutá	rozpoutat	k5eAaPmIp3nS	rozpoutat
se	se	k3xPyFc4	se
bitva	bitva	k1gFnSc1	bitva
<g/>
,	,	kIx,	,
ve	v	k7c4	v
které	který	k3yIgFnPc4	který
je	být	k5eAaImIp3nS	být
zabit	zabit	k2eAgMnSc1d1	zabit
Fred	Fred	k1gMnSc1	Fred
Weasley	Weaslea	k1gFnSc2	Weaslea
<g/>
,	,	kIx,	,
Lupin	lupina	k1gFnPc2	lupina
<g/>
,	,	kIx,	,
Tonksová	Tonksová	k1gFnSc1	Tonksová
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
Harry	Harra	k1gFnSc2	Harra
zabije	zabít	k5eAaPmIp3nS	zabít
Voldemorta	Voldemorta	k1gFnSc1	Voldemorta
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
Harry	Harra	k1gFnSc2	Harra
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Ginny	Ginn	k1gMnPc7	Ginn
Weasleyovou	Weasleyová	k1gFnSc4	Weasleyová
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
spolu	spolu	k6eAd1	spolu
3	[number]	k4	3
děti	dítě	k1gFnPc1	dítě
<g/>
,	,	kIx,	,
Jamese	Jamese	k1gFnSc1	Jamese
Siriuse	Siriuse	k1gFnSc1	Siriuse
<g/>
,	,	kIx,	,
Albuse	Albuse	k1gFnSc1	Albuse
Severuse	Severuse	k1gFnSc1	Severuse
a	a	k8xC	a
Lily	lít	k5eAaImAgFnP	lít
Lenku	Lenka	k1gFnSc4	Lenka
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc4	Harra
pracuje	pracovat	k5eAaImIp3nS	pracovat
na	na	k7c6	na
ministerstvu	ministerstvo	k1gNnSc6	ministerstvo
jako	jako	k8xS	jako
bystrozor	bystrozor	k1gInSc1	bystrozor
společně	společně	k6eAd1	společně
s	s	k7c7	s
Ronem	ron	k1gInSc7	ron
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
je	být	k5eAaImIp3nS	být
vedoucím	vedoucí	k1gMnSc7	vedoucí
odboru	odbor	k1gInSc2	odbor
bystrozorů	bystrozor	k1gInPc2	bystrozor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Další	další	k2eAgFnSc7d1	další
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
vyšel	vyjít	k5eAaPmAgInS	vyjít
další	další	k2eAgFnPc4d1	další
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
osmý	osmý	k4xOgInSc4	osmý
díl	díl	k1gInSc4	díl
knihy	kniha	k1gFnSc2	kniha
o	o	k7c4	o
Harrym	Harrym	k1gInSc4	Harrym
Potterovi	Potterův	k2eAgMnPc1d1	Potterův
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
speciální	speciální	k2eAgNnPc4d1	speciální
vydání	vydání	k1gNnSc4	vydání
pracovního	pracovní	k2eAgInSc2d1	pracovní
scénáře	scénář	k1gInSc2	scénář
s	s	k7c7	s
názvem	název	k1gInSc7	název
<g/>
:	:	kIx,	:
Harry	Harr	k1gInPc1	Harr
Potter	Pottra	k1gFnPc2	Pottra
a	a	k8xC	a
prokleté	prokletý	k2eAgNnSc4d1	prokleté
dítě	dítě	k1gNnSc4	dítě
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rodina	rodina	k1gFnSc1	rodina
==	==	k?	==
</s>
</p>
<p>
<s>
Harry	Harr	k1gMnPc4	Harr
Potter	Pottrum	k1gNnPc2	Pottrum
má	mít	k5eAaImIp3nS	mít
kouzelnické	kouzelnický	k2eAgInPc4d1	kouzelnický
předky	předek	k1gInPc4	předek
z	z	k7c2	z
otcovy	otcův	k2eAgFnSc2d1	otcova
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
mudlovské	mudlovský	k2eAgInPc1d1	mudlovský
z	z	k7c2	z
matčiny	matčin	k2eAgFnSc2d1	matčina
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Rodina	rodina	k1gFnSc1	rodina
Potterových	Potterův	k2eAgMnPc2d1	Potterův
je	být	k5eAaImIp3nS	být
čistokrevnou	čistokrevný	k2eAgFnSc7d1	čistokrevná
kouzelnickou	kouzelnický	k2eAgFnSc7d1	kouzelnická
rodinou	rodina	k1gFnSc7	rodina
a	a	k8xC	a
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
Ignotuse	Ignotuse	k1gFnSc2	Ignotuse
Peverella	Peverell	k1gMnSc2	Peverell
<g/>
,	,	kIx,	,
nejmladšího	mladý	k2eAgMnSc2d3	nejmladší
ze	z	k7c2	z
tří	tři	k4xCgMnPc2	tři
bratrů	bratr	k1gMnPc2	bratr
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgMnPc3	jenž
Smrt	smrt	k1gFnSc1	smrt
dala	dát	k5eAaPmAgFnS	dát
různé	různý	k2eAgInPc4d1	různý
dary	dar	k1gInPc4	dar
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
Harryho	Harry	k1gMnSc4	Harry
dělá	dělat	k5eAaImIp3nS	dělat
vzdáleným	vzdálený	k2eAgMnSc7d1	vzdálený
příbuzným	příbuzný	k1gMnSc7	příbuzný
lorda	lord	k1gMnSc2	lord
Voldemorta	Voldemort	k1gMnSc2	Voldemort
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
potomkem	potomek	k1gMnSc7	potomek
prostředního	prostřední	k2eAgNnSc2d1	prostřední
bratra	bratr	k1gMnSc4	bratr
Cadmuse	Cadmuse	k1gFnSc1	Cadmuse
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomuto	tento	k3xDgNnSc3	tento
svému	svůj	k3xOyFgMnSc3	svůj
předkovi	předek	k1gMnSc3	předek
dědí	dědit	k5eAaImIp3nS	dědit
Potterovi	Potter	k1gMnSc3	Potter
neviditelný	viditelný	k2eNgInSc4d1	neviditelný
plášť	plášť	k1gInSc4	plášť
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
nikdy	nikdy	k6eAd1	nikdy
nepřestane	přestat	k5eNaPmIp3nS	přestat
fungovat	fungovat	k5eAaImF	fungovat
právě	právě	k9	právě
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
Smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
pod	pod	k7c7	pod
ním	on	k3xPp3gMnSc7	on
zároveň	zároveň	k6eAd1	zároveň
nemůže	moct	k5eNaImIp3nS	moct
nikoho	nikdo	k3yNnSc4	nikdo
spatřit	spatřit	k5eAaPmF	spatřit
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
osoba	osoba	k1gFnSc1	osoba
skrytá	skrytý	k2eAgFnSc1d1	skrytá
pod	pod	k7c7	pod
ním	on	k3xPp3gMnSc7	on
před	před	k7c4	před
Smrtí	smrtit	k5eAaImIp3nS	smrtit
chráněna	chráněn	k2eAgFnSc1d1	chráněna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
James	James	k1gMnSc1	James
Potter	Potter	k1gMnSc1	Potter
===	===	k?	===
</s>
</p>
<p>
<s>
James	James	k1gMnSc1	James
Potter	Potter	k1gMnSc1	Potter
(	(	kIx(	(
<g/>
27	[number]	k4	27
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1960	[number]	k4	1960
–	–	k?	–
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
otec	otec	k1gMnSc1	otec
Harryho	Harry	k1gMnSc4	Harry
Pottera	Potter	k1gMnSc4	Potter
<g/>
,	,	kIx,	,
manžel	manžel	k1gMnSc1	manžel
Lily	lít	k5eAaImAgFnP	lít
Potterové	Potterový	k2eAgMnPc4d1	Potterový
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
jediným	jediný	k2eAgNnSc7d1	jediné
dítětem	dítě	k1gNnSc7	dítě
svých	svůj	k3xOyFgMnPc2	svůj
rodičů	rodič	k1gMnPc2	rodič
Fleamonta	Fleamont	k1gInSc2	Fleamont
a	a	k8xC	a
Euphemie	Euphemie	k1gFnSc2	Euphemie
Potterových	Potterův	k2eAgMnPc2d1	Potterův
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
do	do	k7c2	do
bohaté	bohatý	k2eAgFnSc2d1	bohatá
rodiny	rodina	k1gFnSc2	rodina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
čistokrevný	čistokrevný	k2eAgInSc4d1	čistokrevný
původ	původ	k1gInSc4	původ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	let	k1gInPc6	let
1971	[number]	k4	1971
<g/>
–	–	k?	–
<g/>
1978	[number]	k4	1978
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
Bradavice	bradavice	k1gFnSc1	bradavice
jako	jako	k8xS	jako
student	student	k1gMnSc1	student
Nebelvíru	Nebelvír	k1gInSc2	Nebelvír
<g/>
,	,	kIx,	,
za	za	k7c4	za
který	který	k3yRgInSc4	který
hrál	hrát	k5eAaImAgMnS	hrát
také	také	k6eAd1	také
famfrpál	famfrpál	k1gMnSc1	famfrpál
na	na	k7c6	na
postu	post	k1gInSc6	post
chytače	chytač	k1gInSc2	chytač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
největšími	veliký	k2eAgMnPc7d3	veliký
přáteli	přítel	k1gMnPc7	přítel
byli	být	k5eAaImAgMnP	být
Sirius	Sirius	k1gMnSc1	Sirius
Black	Black	k1gMnSc1	Black
<g/>
,	,	kIx,	,
Remus	Remus	k1gMnSc1	Remus
Lupin	lupina	k1gFnPc2	lupina
a	a	k8xC	a
Peter	Peter	k1gMnSc1	Peter
Pettigrew	Pettigrew	k1gMnSc1	Pettigrew
<g/>
,	,	kIx,	,
skupina	skupina	k1gFnSc1	skupina
zvaná	zvaný	k2eAgFnSc1d1	zvaná
Pobertové	poberta	k1gMnPc1	poberta
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
čtyři	čtyři	k4xCgMnPc1	čtyři
vymysleli	vymyslet	k5eAaPmAgMnP	vymyslet
a	a	k8xC	a
zakreslili	zakreslit	k5eAaPmAgMnP	zakreslit
Pobertův	pobertův	k2eAgInSc4d1	pobertův
plánek	plánek	k1gInSc4	plánek
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
plán	plán	k1gInSc4	plán
celých	celý	k2eAgFnPc2d1	celá
Bradavic	bradavice	k1gFnPc2	bradavice
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
jsou	být	k5eAaImIp3nP	být
vyobrazeny	vyobrazit	k5eAaPmNgFnP	vyobrazit
také	také	k9	také
tajné	tajný	k2eAgFnPc1d1	tajná
chodby	chodba	k1gFnPc1	chodba
a	a	k8xC	a
plánek	plánek	k1gInSc1	plánek
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
každého	každý	k3xTgMnSc4	každý
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
a	a	k8xC	a
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
právě	právě	k6eAd1	právě
nachází	nacházet	k5eAaImIp3nS	nacházet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
plánku	plánek	k1gInSc6	plánek
vystupovali	vystupovat	k5eAaImAgMnP	vystupovat
pod	pod	k7c7	pod
přezdívkami	přezdívka	k1gFnPc7	přezdívka
<g/>
:	:	kIx,	:
James	James	k1gInSc1	James
-	-	kIx~	-
Dvanácterák	dvanácterák	k1gMnSc1	dvanácterák
<g/>
,	,	kIx,	,
Sirius	Sirius	k1gMnSc1	Sirius
-	-	kIx~	-
Tichošlápek	tichošlápek	k1gMnSc1	tichošlápek
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
-	-	kIx~	-
Červíček	červíček	k1gMnSc1	červíček
a	a	k8xC	a
Remus	Remus	k1gMnSc1	Remus
-	-	kIx~	-
Náměsíčník	náměsíčník	k1gMnSc1	náměsíčník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
třetím	třetí	k4xOgInSc6	třetí
roce	rok	k1gInSc6	rok
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
Remus	Remus	k1gMnSc1	Remus
je	být	k5eAaImIp3nS	být
vlkodlak	vlkodlak	k1gMnSc1	vlkodlak
(	(	kIx(	(
<g/>
Měsíčník	Měsíčník	k1gMnSc1	Měsíčník
<g/>
)	)	kIx)	)
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
zbytek	zbytek	k1gInSc1	zbytek
skupiny	skupina	k1gFnSc2	skupina
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
učil	učít	k5eAaPmAgMnS	učít
na	na	k7c4	na
zvěromágy	zvěromága	k1gFnPc4	zvěromága
(	(	kIx(	(
<g/>
nehlášené	hlášený	k2eNgFnPc4d1	nehlášená
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gMnSc1	James
se	se	k3xPyFc4	se
naučil	naučit	k5eAaPmAgMnS	naučit
přeměnu	přeměna	k1gFnSc4	přeměna
v	v	k7c4	v
jelena	jelen	k1gMnSc4	jelen
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
řikalo	řikat	k5eAaPmAgNnS	řikat
Dvanácterák	dvanácterák	k1gMnSc1	dvanácterák
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
studií	studio	k1gNnPc2	studio
byl	být	k5eAaImAgMnS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Lily	lít	k5eAaImAgFnP	lít
prý	prý	k9	prý
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
nejnadanějších	nadaný	k2eAgMnPc2d3	nejnadanější
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
Hagrid	Hagrid	k1gInSc1	Hagrid
Harrymu	Harrym	k1gInSc2	Harrym
povídal	povídat	k5eAaImAgInS	povídat
o	o	k7c6	o
rodičích	rodič	k1gMnPc6	rodič
<g/>
,	,	kIx,	,
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
diví	divit	k5eAaImIp3nS	divit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
je	být	k5eAaImIp3nS	být
Voldemort	Voldemort	k1gInSc1	Voldemort
nepokoušel	pokoušet	k5eNaImAgInS	pokoušet
přetáhnout	přetáhnout	k5eAaPmF	přetáhnout
na	na	k7c4	na
svoji	svůj	k3xOyFgFnSc4	svůj
stranu	strana	k1gFnSc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
zřejmě	zřejmě	k6eAd1	zřejmě
jen	jen	k9	jen
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
že	že	k8xS	že
byli	být	k5eAaImAgMnP	být
moc	moc	k6eAd1	moc
blízcí	blízký	k2eAgMnPc1d1	blízký
Brumbálovi	brumbálův	k2eAgMnPc1d1	brumbálův
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Když	když	k8xS	když
chtěl	chtít	k5eAaImAgMnS	chtít
Voldemort	Voldemort	k1gInSc4	Voldemort
zabít	zabít	k5eAaPmF	zabít
Potterovy	Potterův	k2eAgInPc4d1	Potterův
<g/>
,	,	kIx,	,
Pettigrew	Pettigrew	k1gFnSc1	Pettigrew
mu	on	k3xPp3gMnSc3	on
zrádně	zrádně	k6eAd1	zrádně
odhalil	odhalit	k5eAaPmAgMnS	odhalit
jejich	jejich	k3xOp3gInSc4	jejich
úkryt	úkryt	k1gInSc4	úkryt
<g/>
.	.	kIx.	.
</s>
<s>
James	James	k1gMnSc1	James
Potter	Potter	k1gMnSc1	Potter
byl	být	k5eAaImAgMnS	být
zavražděn	zavraždit	k5eAaPmNgMnS	zavraždit
jako	jako	k9	jako
první	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
překvapen	překvapen	k2eAgMnSc1d1	překvapen
a	a	k8xC	a
neozbrojen	ozbrojen	k2eNgMnSc1d1	neozbrojen
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
chránit	chránit	k5eAaImF	chránit
svou	svůj	k3xOyFgFnSc4	svůj
rodinu	rodina	k1gFnSc4	rodina
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohla	moct	k5eAaImAgFnS	moct
utéct	utéct	k5eAaPmF	utéct
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
a	a	k8xC	a
Lily	lít	k5eAaImAgFnP	lít
se	se	k3xPyFc4	se
již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
postavili	postavit	k5eAaPmAgMnP	postavit
Voldemortovi	Voldemortův	k2eAgMnPc1d1	Voldemortův
třikrát	třikrát	k6eAd1	třikrát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
profesoři	profesor	k1gMnPc1	profesor
(	(	kIx(	(
<g/>
Albus	Albus	k1gMnSc1	Albus
Brumbál	brumbál	k1gMnSc1	brumbál
<g/>
,	,	kIx,	,
Minerva	Minerva	k1gFnSc1	Minerva
McGonagallová	McGonagallová	k1gFnSc1	McGonagallová
<g/>
,	,	kIx,	,
Rubeus	Rubeus	k1gInSc1	Rubeus
Hagrid	Hagrida	k1gFnPc2	Hagrida
<g/>
,	,	kIx,	,
...	...	k?	...
<g/>
)	)	kIx)	)
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
vzpomínají	vzpomínat	k5eAaImIp3nP	vzpomínat
v	v	k7c6	v
dobrém	dobrý	k2eAgInSc6d1	dobrý
<g/>
,	,	kIx,	,
jen	jen	k6eAd1	jen
Severus	Severus	k1gInSc1	Severus
Snape	Snap	k1gInSc5	Snap
ne	ne	k9	ne
<g/>
.	.	kIx.	.
</s>
<s>
Vůči	vůči	k7c3	vůči
Severusi	Severuse	k1gFnSc3	Severuse
Snapeovi	Snapeův	k2eAgMnPc1d1	Snapeův
(	(	kIx(	(
<g/>
též	též	k9	též
známému	známý	k1gMnSc3	známý
jako	jako	k8xC	jako
Princ	princ	k1gMnSc1	princ
dvojí	dvojí	k4xRgFnSc2	dvojí
krve	krev	k1gFnSc2	krev
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
choval	chovat	k5eAaImAgInS	chovat
velmi	velmi	k6eAd1	velmi
arogantně	arogantně	k6eAd1	arogantně
a	a	k8xC	a
stali	stát	k5eAaPmAgMnP	stát
se	se	k3xPyFc4	se
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
nepřátelé	nepřítel	k1gMnPc1	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Severus	Severus	k1gInSc1	Severus
jejich	jejich	k3xOp3gFnSc4	jejich
skupinu	skupina	k1gFnSc4	skupina
často	často	k6eAd1	často
pronásledoval	pronásledovat	k5eAaImAgInS	pronásledovat
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
žárlil	žárlit	k5eAaImAgMnS	žárlit
na	na	k7c6	na
Jamese	James	k1gInSc6	James
kvůli	kvůli	k7c3	kvůli
Lily	lít	k5eAaImAgFnP	lít
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
miloval	milovat	k5eAaImAgMnS	milovat
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
jí	on	k3xPp3gFnSc3	on
Severus	Severus	k1gInSc1	Severus
omylem	omylem	k6eAd1	omylem
řekl	říct	k5eAaPmAgInS	říct
Mudlovská	Mudlovský	k2eAgFnSc5d1	Mudlovská
šmejdko	šmejdka	k1gFnSc5	šmejdka
<g/>
,	,	kIx,	,
zamilovala	zamilovat	k5eAaPmAgFnS	zamilovat
Lily	lít	k5eAaImAgFnP	lít
do	do	k7c2	do
Jamese	Jamese	k1gFnSc2	Jamese
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
vzpomínce	vzpomínka	k1gFnSc6	vzpomínka
Harry	Harra	k1gFnSc2	Harra
viděl	vidět	k5eAaImAgMnS	vidět
<g/>
,	,	kIx,	,
že	že	k8xS	že
i	i	k9	i
jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
řekla	říct	k5eAaPmAgFnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
James	James	k1gMnSc1	James
arogantní	arogantní	k2eAgInPc4d1	arogantní
a	a	k8xC	a
tak	tak	k6eAd1	tak
si	se	k3xPyFc3	se
Harry	Harr	k1gInPc4	Harr
myslel	myslet	k5eAaImAgInS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
James	James	k1gMnSc1	James
donutil	donutit	k5eAaPmAgMnS	donutit
Lily	lít	k5eAaImAgFnP	lít
nějakým	nějaký	k3yIgInSc7	nějaký
nekalým	kalý	k2eNgInSc7d1	nekalý
způsobem	způsob	k1gInSc7	způsob
ke	k	k7c3	k
svatbě	svatba	k1gFnSc3	svatba
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Siriuse	Siriuse	k1gFnSc2	Siriuse
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
ale	ale	k9	ale
vyrostl	vyrůst	k5eAaPmAgMnS	vyrůst
<g/>
,	,	kIx,	,
toho	ten	k3xDgMnSc4	ten
si	se	k3xPyFc3	se
Lily	lít	k5eAaImAgFnP	lít
všimla	všimnout	k5eAaPmAgFnS	všimnout
a	a	k8xC	a
v	v	k7c6	v
posledním	poslední	k2eAgInSc6d1	poslední
ročníku	ročník	k1gInSc6	ročník
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
začala	začít	k5eAaPmAgFnS	začít
chodit	chodit	k5eAaImF	chodit
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
osmnácti	osmnáct	k4xCc2	osmnáct
let	léto	k1gNnPc2	léto
vzali	vzít	k5eAaPmAgMnP	vzít
a	a	k8xC	a
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Poberty	poberta	k1gMnPc7	poberta
se	se	k3xPyFc4	se
stali	stát	k5eAaPmAgMnP	stát
členy	člen	k1gMnPc4	člen
Fénixova	fénixův	k2eAgInSc2d1	fénixův
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
31	[number]	k4	31
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1980	[number]	k4	1980
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
Harry	Harra	k1gFnSc2	Harra
<g/>
,	,	kIx,	,
Severus	Severus	k1gMnSc1	Severus
však	však	k9	však
nevědouce	vědět	k5eNaImSgFnP	vědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
syna	syn	k1gMnSc4	syn
Lily	lít	k5eAaImAgFnP	lít
<g/>
,	,	kIx,	,
řekl	říct	k5eAaPmAgMnS	říct
Voldemortovi	Voldemort	k1gMnSc3	Voldemort
o	o	k7c6	o
věštbě	věštba	k1gFnSc6	věštba
profesorky	profesorka	k1gFnSc2	profesorka
Sybilly	Sybilla	k1gFnSc2	Sybilla
Trelawneyové	Trelawneyová	k1gFnSc2	Trelawneyová
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
úkryt	úkryt	k1gInSc4	úkryt
v	v	k7c6	v
Gordikově	Gordikův	k2eAgInSc6d1	Gordikův
Dole	dol	k1gInSc6	dol
se	se	k3xPyFc4	se
o	o	k7c6	o
nich	on	k3xPp3gFnPc6	on
od	od	k7c2	od
Petera	Petera	k1gMnSc1	Petera
Voldemort	Voldemort	k1gInSc4	Voldemort
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
a	a	k8xC	a
oba	dva	k4xCgMnPc1	dva
zabil	zabít	k5eAaPmAgMnS	zabít
(	(	kIx(	(
<g/>
následkem	následkem	k7c2	následkem
byla	být	k5eAaImAgFnS	být
Lilyno	Lilyen	k2eAgNnSc4d1	Lilyen
obětování	obětování	k1gNnSc4	obětování
a	a	k8xC	a
následná	následný	k2eAgNnPc4d1	následné
odražení	odražení	k1gNnPc4	odražení
kletby	kletba	k1gFnSc2	kletba
Avada	Avada	k1gFnSc1	Avada
Kedavra	Kedavra	k1gFnSc1	Kedavra
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
rozdělilo	rozdělit	k5eAaPmAgNnS	rozdělit
duši	duše	k1gFnSc4	duše
Voldemorta	Voldemort	k1gMnSc2	Voldemort
na	na	k7c6	na
půl	půl	k1xP	půl
a	a	k8xC	a
druhý	druhý	k4xOgInSc1	druhý
půlka	půlka	k1gFnSc1	půlka
so	so	k?	so
do	do	k7c2	do
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1998	[number]	k4	1998
schovávala	schovávat	k5eAaImAgFnS	schovávat
v	v	k7c4	v
Harrym	Harrym	k1gInSc4	Harrym
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
byl	být	k5eAaImAgInS	být
viděn	vidět	k5eAaImNgInS	vidět
pětkrát	pětkrát	k6eAd1	pětkrát
<g/>
:	:	kIx,	:
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
ve	v	k7c6	v
vzpomínce	vzpomínka	k1gFnSc6	vzpomínka
Severuse	Severus	k1gMnSc5	Severus
Snape	Snap	k1gMnSc5	Snap
<g/>
,	,	kIx,	,
v	v	k7c6	v
Zrcadle	zrcadlo	k1gNnSc6	zrcadlo
z	z	k7c2	z
Erisedu	Erised	k1gMnSc3	Erised
<g/>
,	,	kIx,	,
při	pře	k1gFnSc3	pře
Priori	priori	k6eAd1	priori
Incantatem	Incantat	k1gInSc7	Incantat
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
s	s	k7c7	s
Voldemortem	Voldemort	k1gInSc7	Voldemort
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
začaroval	začarovat	k5eAaPmAgMnS	začarovat
Snape	Snap	k1gInSc5	Snap
také	také	k9	také
v	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
vzpomínkách	vzpomínka	k1gFnPc6	vzpomínka
a	a	k8xC	a
pomocí	pomocí	k7c2	pomocí
kamene	kámen	k1gInSc2	kámen
vzkříšení	vzkříšení	k1gNnSc1	vzkříšení
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
roky	rok	k1gInPc4	rok
2002	[number]	k4	2002
a	a	k8xC	a
2005	[number]	k4	2005
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
jeho	jeho	k3xOp3gMnSc1	jeho
vnuk	vnuk	k1gMnSc1	vnuk
James	James	k1gMnSc1	James
Sirius	Sirius	k1gMnSc1	Sirius
Potter	Potter	k1gMnSc1	Potter
a	a	k8xC	a
poté	poté	k6eAd1	poté
i	i	k9	i
další	další	k2eAgMnSc1d1	další
vnuk	vnuk	k1gMnSc1	vnuk
Albus	Albus	k1gMnSc1	Albus
Severus	Severus	k1gMnSc1	Severus
(	(	kIx(	(
<g/>
Harry	Harr	k1gInPc1	Harr
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Albusi	Albuse	k1gFnSc4	Albuse
Severusi	Severuse	k1gFnSc4	Severuse
Pottere	Potter	k1gInSc5	Potter
si	se	k3xPyFc3	se
pojmenovaný	pojmenovaný	k2eAgMnSc1d1	pojmenovaný
po	po	k7c6	po
dvou	dva	k4xCgMnPc6	dva
ředitelích	ředitel	k1gMnPc6	ředitel
Bradavic	bradavice	k1gFnPc2	bradavice
a	a	k8xC	a
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
byl	být	k5eAaImAgInS	být
ze	z	k7c2	z
Zmijozelu	Zmijozel	k1gInSc2	Zmijozel
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Harry	Harra	k1gFnSc2	Harra
1.9	[number]	k4	1.9
<g/>
.	.	kIx.	.
2017	[number]	k4	2017
<g/>
))	))	k?	))
a	a	k8xC	a
vnučka	vnučka	k1gFnSc1	vnučka
Lily	lít	k5eAaImAgFnP	lít
Lenka	Lenka	k1gFnSc1	Lenka
(	(	kIx(	(
<g/>
v	v	k7c6	v
originále	originál	k1gInSc6	originál
Luna	luna	k1gFnSc1	luna
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
zase	zase	k9	zase
podle	podle	k7c2	podle
manželky	manželka	k1gFnSc2	manželka
Jamese	Jamese	k1gFnSc2	Jamese
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
ho	on	k3xPp3gInSc4	on
představoval	představovat	k5eAaImAgMnS	představovat
herec	herec	k1gMnSc1	herec
Adrian	Adrian	k1gMnSc1	Adrian
Rawlins	Rawlins	k1gInSc4	Rawlins
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Lily	lít	k5eAaImAgFnP	lít
Potterová	Potterová	k1gFnSc1	Potterová
===	===	k?	===
</s>
</p>
<p>
<s>
Lily	lít	k5eAaImAgFnP	lít
Potterová	Potterová	k1gFnSc1	Potterová
(	(	kIx(	(
<g/>
rozená	rozený	k2eAgFnSc1d1	rozená
Evansová	Evansová	k1gFnSc1	Evansová
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1960	[number]	k4	1960
–	–	k?	–
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
matka	matka	k1gFnSc1	matka
Harryho	Harry	k1gMnSc2	Harry
Pottera	Potter	k1gMnSc2	Potter
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
Jamese	Jamese	k1gFnSc2	Jamese
Pottera	Potter	k1gMnSc2	Potter
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lily	lít	k5eAaImAgFnP	lít
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
mudlovské	mudlovský	k2eAgFnSc2d1	mudlovská
(	(	kIx(	(
<g/>
nekouzelnické	kouzelnický	k2eNgFnSc2d1	kouzelnický
<g/>
)	)	kIx)	)
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
její	její	k3xOp3gFnSc7	její
starší	starý	k2eAgFnSc7d2	starší
sestrou	sestra	k1gFnSc7	sestra
byla	být	k5eAaImAgFnS	být
Petunie	Petunie	k1gFnSc1	Petunie
Dursleyová	Dursleyová	k1gFnSc1	Dursleyová
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
Harryho	Harry	k1gMnSc4	Harry
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
manželem	manžel	k1gMnSc7	manžel
Veronem	Veron	k1gMnSc7	Veron
Dursleyem	Dursley	k1gMnSc7	Dursley
později	pozdě	k6eAd2	pozdě
vychovávala	vychovávat	k5eAaImAgFnS	vychovávat
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
čarodějka	čarodějka	k1gFnSc1	čarodějka
<g/>
,	,	kIx,	,
jí	on	k3xPp3gFnSc7	on
pověděl	povědět	k5eAaPmAgInS	povědět
Severus	Severus	k1gInSc1	Severus
Snape	Snap	k1gInSc5	Snap
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
bydlel	bydlet	k5eAaImAgMnS	bydlet
nedaleko	nedaleko	k7c2	nedaleko
Evansových	Evansová	k1gFnPc2	Evansová
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
většinu	většina	k1gFnSc4	většina
času	čas	k1gInSc2	čas
v	v	k7c6	v
bradavické	bradavický	k2eAgFnSc6d1	bradavická
škole	škola	k1gFnSc6	škola
jejím	její	k3xOp3gMnSc7	její
nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
přítelem	přítel	k1gMnSc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jí	jíst	k5eAaImIp3nS	jíst
přišel	přijít	k5eAaPmAgMnS	přijít
dopis	dopis	k1gInSc4	dopis
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
do	do	k7c2	do
Bradavic	bradavice	k1gFnPc2	bradavice
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnPc4	její
rodiče	rodič	k1gMnPc1	rodič
byli	být	k5eAaImAgMnP	být
nadšení	nadšený	k2eAgMnPc1d1	nadšený
<g/>
,	,	kIx,	,
sestra	sestra	k1gFnSc1	sestra
na	na	k7c4	na
ni	on	k3xPp3gFnSc4	on
ale	ale	k9	ale
žárlila	žárlit	k5eAaImAgNnP	žárlit
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
začala	začít	k5eAaPmAgNnP	začít
chovat	chovat	k5eAaImF	chovat
špatně	špatně	k6eAd1	špatně
<g/>
.	.	kIx.	.
</s>
<s>
Bradavickou	Bradavický	k2eAgFnSc4d1	Bradavická
nebelvírskou	belvírský	k2eNgFnSc4d1	nebelvírská
kolej	kolej	k1gFnSc4	kolej
navštěvovala	navštěvovat	k5eAaImAgFnS	navštěvovat
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
James	James	k1gMnSc1	James
v	v	k7c6	v
letech	let	k1gInPc6	let
1971	[number]	k4	1971
<g/>
–	–	k?	–
<g/>
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
oblíbenou	oblíbený	k2eAgFnSc7d1	oblíbená
studentkou	studentka	k1gFnSc7	studentka
Horacia	Horacius	k1gMnSc2	Horacius
Křiklana	Křiklan	k1gMnSc2	Křiklan
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
měla	mít	k5eAaImAgFnS	mít
velké	velký	k2eAgNnSc4d1	velké
nadání	nadání	k1gNnSc4	nadání
pro	pro	k7c4	pro
lektvary	lektvar	k1gInPc4	lektvar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pátém	pátý	k4xOgInSc6	pátý
ročníku	ročník	k1gInSc6	ročník
také	také	k9	také
skončilo	skončit	k5eAaPmAgNnS	skončit
její	její	k3xOp3gNnSc1	její
přátelství	přátelství	k1gNnSc1	přátelství
se	s	k7c7	s
Severusem	Severus	k1gMnSc7	Severus
Snapem	Snap	k1gMnSc7	Snap
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ji	on	k3xPp3gFnSc4	on
nazval	nazvat	k5eAaPmAgMnS	nazvat
mudlovskou	mudlovský	k2eAgFnSc7d1	mudlovská
šmejdkou	šmejdka	k1gFnSc7	šmejdka
(	(	kIx(	(
<g/>
nadávka	nadávka	k1gFnSc1	nadávka
označující	označující	k2eAgMnPc4d1	označující
mudlorozené	mudlorozený	k2eAgMnPc4d1	mudlorozený
kouzelníky	kouzelník	k1gMnPc4	kouzelník
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
studií	studie	k1gFnPc2	studie
se	se	k3xPyFc4	se
vdala	vdát	k5eAaPmAgFnS	vdát
za	za	k7c2	za
Jamese	Jamese	k1gFnSc2	Jamese
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc4	který
dříve	dříve	k6eAd2	dříve
neměla	mít	k5eNaImAgFnS	mít
ráda	rád	k2eAgFnSc1d1	ráda
<g/>
,	,	kIx,	,
chodit	chodit	k5eAaImF	chodit
spolu	spolu	k6eAd1	spolu
začali	začít	k5eAaPmAgMnP	začít
až	až	k9	až
v	v	k7c6	v
sedmém	sedmý	k4xOgInSc6	sedmý
ročníku	ročník	k1gInSc6	ročník
<g/>
,	,	kIx,	,
a	a	k8xC	a
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
se	se	k3xPyFc4	se
spolu	spolu	k6eAd1	spolu
do	do	k7c2	do
Godrikova	Godrikův	k2eAgInSc2d1	Godrikův
Dolu	dol	k1gInSc2	dol
<g/>
.	.	kIx.	.
</s>
<s>
Ona	onen	k3xDgFnSc1	onen
i	i	k9	i
její	její	k3xOp3gMnSc1	její
manžel	manžel	k1gMnSc1	manžel
byli	být	k5eAaImAgMnP	být
zrazeni	zradit	k5eAaPmNgMnP	zradit
Petrem	Petr	k1gMnSc7	Petr
Pettigrewem	Pettigrew	k1gMnSc7	Pettigrew
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
prozradil	prozradit	k5eAaPmAgMnS	prozradit
jejich	jejich	k3xOp3gInSc4	jejich
úkryt	úkryt	k1gInSc4	úkryt
lordu	lord	k1gMnSc3	lord
Voldemortovi	Voldemort	k1gMnSc3	Voldemort
<g/>
.	.	kIx.	.
</s>
<s>
Lily	lít	k5eAaImAgFnP	lít
Potterová	Potterový	k2eAgFnSc1d1	Potterová
byla	být	k5eAaImAgFnS	být
zavražděna	zavraždit	k5eAaPmNgFnS	zavraždit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
manželem	manžel	k1gMnSc7	manžel
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnSc1	její
láska	láska	k1gFnSc1	láska
však	však	k9	však
i	i	k9	i
posmrtně	posmrtně	k6eAd1	posmrtně
ochraňovala	ochraňovat	k5eAaImAgFnS	ochraňovat
jejího	její	k3xOp3gMnSc4	její
syna	syn	k1gMnSc4	syn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
filmech	film	k1gInPc6	film
ji	on	k3xPp3gFnSc4	on
hrála	hrát	k5eAaImAgFnS	hrát
Geraldine	Geraldin	k1gMnSc5	Geraldin
Somerville	Somervill	k1gMnSc5	Somervill
<g/>
,	,	kIx,	,
v	v	k7c4	v
mládí	mládí	k1gNnSc4	mládí
Ellie	Ellie	k1gFnSc2	Ellie
Darcey-Aldenová	Darcey-Aldenová	k1gFnSc1	Darcey-Aldenová
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Harryho	Harryha	k1gFnSc5	Harryha
děti	dítě	k1gFnPc4	dítě
===	===	k?	===
</s>
</p>
<p>
<s>
Nejstarším	starý	k2eAgMnSc7d3	nejstarší
synem	syn	k1gMnSc7	syn
páru	pár	k1gInSc2	pár
je	být	k5eAaImIp3nS	být
James	James	k1gMnSc1	James
Sirius	Sirius	k1gMnSc1	Sirius
Potter	Potter	k1gMnSc1	Potter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
je	být	k5eAaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
podobnou	podobný	k2eAgFnSc4d1	podobná
povahu	povaha	k1gFnSc4	povaha
jako	jako	k8xS	jako
jeho	jeho	k3xOp3gMnSc1	jeho
mrtvý	mrtvý	k2eAgMnSc1d1	mrtvý
dědeček	dědeček	k1gMnSc1	dědeček
James	James	k1gMnSc1	James
Potter	Potter	k1gMnSc1	Potter
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
velkou	velký	k2eAgFnSc4d1	velká
událost	událost	k1gFnSc4	událost
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
jeho	jeho	k3xOp3gFnSc1	jeho
sestřenice	sestřenice	k1gFnSc1	sestřenice
Victoire	Victoir	k1gInSc5	Victoir
Weasleyová	Weasleyová	k1gFnSc1	Weasleyová
líbá	líbat	k5eAaImIp3nS	líbat
s	s	k7c7	s
Teddym	Teddym	k1gInSc1	Teddym
Lupinem	Lupin	k1gInSc7	Lupin
<g/>
,	,	kIx,	,
synem	syn	k1gMnSc7	syn
Remuse	Remuse	k1gFnSc2	Remuse
Lupina	lupina	k1gFnSc1	lupina
a	a	k8xC	a
Nymfadory	Nymfador	k1gInPc1	Nymfador
Lupinové-Tonksové	Lupinové-Tonksová	k1gFnSc2	Lupinové-Tonksová
<g/>
,	,	kIx,	,
Harryho	Harry	k1gMnSc2	Harry
kmotřencem	kmotřenec	k1gMnSc7	kmotřenec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
opravdu	opravdu	k6eAd1	opravdu
velice	velice	k6eAd1	velice
podobný	podobný	k2eAgInSc1d1	podobný
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
svému	svůj	k3xOyFgMnSc3	svůj
otci	otec	k1gMnSc3	otec
tajně	tajně	k6eAd1	tajně
vzal	vzít	k5eAaPmAgMnS	vzít
Pobertův	pobertův	k2eAgInSc4d1	pobertův
plánek	plánek	k1gInSc4	plánek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
chtěl	chtít	k5eAaImAgInS	chtít
Harry	Harr	k1gInPc4	Harr
před	před	k7c7	před
svými	svůj	k3xOyFgFnPc7	svůj
dětmi	dítě	k1gFnPc7	dítě
utajit	utajit	k5eAaPmF	utajit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zařazen	zařadit	k5eAaPmNgMnS	zařadit
do	do	k7c2	do
Nebelvíru	Nebelvír	k1gInSc2	Nebelvír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhým	druhý	k4xOgMnSc7	druhý
synem	syn	k1gMnSc7	syn
Harryho	Harry	k1gMnSc2	Harry
a	a	k8xC	a
Ginny	Ginna	k1gMnSc2	Ginna
je	být	k5eAaImIp3nS	být
Albus	Albus	k1gMnSc1	Albus
Severus	Severus	k1gMnSc1	Severus
Potter	Potter	k1gMnSc1	Potter
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
v	v	k7c6	v
epilogu	epilog	k1gInSc6	epilog
sedmého	sedmý	k4xOgInSc2	sedmý
dílu	díl	k1gInSc2	díl
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2017	[number]	k4	2017
<g/>
)	)	kIx)	)
jede	jet	k5eAaImIp3nS	jet
poprvé	poprvé	k6eAd1	poprvé
do	do	k7c2	do
Bradavic	bradavice	k1gFnPc2	bradavice
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
nejméně	málo	k6eAd3	málo
o	o	k7c4	o
rok	rok	k1gInSc4	rok
mladší	mladý	k2eAgFnSc1d2	mladší
než	než	k8xS	než
James	James	k1gInSc1	James
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
ten	ten	k3xDgMnSc1	ten
už	už	k6eAd1	už
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
byl	být	k5eAaImAgMnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
vtipům	vtip	k1gInPc3	vtip
svého	svůj	k3xOyFgMnSc2	svůj
bratra	bratr	k1gMnSc2	bratr
se	se	k3xPyFc4	se
bojí	bát	k5eAaImIp3nP	bát
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
zařazen	zařadit	k5eAaPmNgInS	zařadit
do	do	k7c2	do
Zmijozelu	Zmijozel	k1gInSc2	Zmijozel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
mu	on	k3xPp3gMnSc3	on
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gMnSc4	on
nebudou	být	k5eNaImBp3nP	být
mít	mít	k5eAaImF	mít
kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
méně	málo	k6eAd2	málo
rádi	rád	k2eAgMnPc1d1	rád
a	a	k8xC	a
že	že	k8xS	že
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
ředitelů	ředitel	k1gMnPc2	ředitel
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterém	který	k3yRgInSc6	který
je	být	k5eAaImIp3nS	být
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
<g/>
,	,	kIx,	,
chodil	chodit	k5eAaImAgMnS	chodit
také	také	k9	také
do	do	k7c2	do
Zmijozelu	Zmijozel	k1gInSc2	Zmijozel
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
možná	možná	k9	možná
ten	ten	k3xDgMnSc1	ten
nejstatečnější	statečný	k2eAgMnSc1d3	nejstatečnější
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
jakého	jaký	k3yRgMnSc4	jaký
znal	znát	k5eAaImAgMnS	znát
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k6eAd1	také
mu	on	k3xPp3gMnSc3	on
řekne	říct	k5eAaPmIp3nS	říct
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
ještě	ještě	k9	ještě
žádnému	žádný	k3yNgNnSc3	žádný
svému	svůj	k3xOyFgNnSc3	svůj
dítěti	dítě	k1gNnSc3	dítě
neřekl	říct	k5eNaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gNnSc4	on
chtěl	chtít	k5eAaImAgInS	chtít
moudrý	moudrý	k2eAgInSc1d1	moudrý
klobouk	klobouk	k1gInSc1	klobouk
také	také	k9	také
zařadit	zařadit	k5eAaPmF	zařadit
do	do	k7c2	do
Zmijozelu	Zmijozel	k1gInSc2	Zmijozel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
on	on	k3xPp3gMnSc1	on
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
nepřál	přát	k5eNaImAgInS	přát
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
ho	on	k3xPp3gInSc4	on
zařadil	zařadit	k5eAaPmAgInS	zařadit
do	do	k7c2	do
Nebelvíru	Nebelvír	k1gInSc2	Nebelvír
<g/>
.	.	kIx.	.
</s>
<s>
Moudrý	moudrý	k2eAgInSc1d1	moudrý
klobouk	klobouk	k1gInSc1	klobouk
Albuse	Albuse	k1gFnSc2	Albuse
zařadí	zařadit	k5eAaPmIp3nS	zařadit
do	do	k7c2	do
Zmijozelu	Zmijozel	k1gInSc2	Zmijozel
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
najde	najít	k5eAaPmIp3nS	najít
nejlepšího	dobrý	k2eAgMnSc4d3	nejlepší
kamaráda	kamarád	k1gMnSc4	kamarád
<g/>
,	,	kIx,	,
Scorpiuse	Scorpiuse	k1gFnSc1	Scorpiuse
Malfoye	Malfoye	k1gInSc1	Malfoye
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
paradoxně	paradoxně	k6eAd1	paradoxně
syn	syn	k1gMnSc1	syn
Draca	Dracus	k1gMnSc2	Dracus
Malfoye	Malfoy	k1gMnSc2	Malfoy
<g/>
,	,	kIx,	,
soka	sok	k1gMnSc2	sok
Harryho	Harry	k1gMnSc2	Harry
Pottera	Potter	k1gMnSc2	Potter
<g/>
.	.	kIx.	.
</s>
<s>
Albus	Albus	k1gMnSc1	Albus
se	se	k3xPyFc4	se
z	z	k7c2	z
Harryho	Harry	k1gMnSc2	Harry
dětí	dítě	k1gFnPc2	dítě
podobá	podobat	k5eAaImIp3nS	podobat
svému	svůj	k3xOyFgMnSc3	svůj
otci	otec	k1gMnSc3	otec
nejvíce	nejvíce	k6eAd1	nejvíce
a	a	k8xC	a
také	také	k9	také
jako	jako	k9	jako
jediný	jediný	k2eAgMnSc1d1	jediný
zdědil	zdědit	k5eAaPmAgInS	zdědit
zelené	zelený	k2eAgFnPc4d1	zelená
oči	oko	k1gNnPc4	oko
Harryho	Harry	k1gMnSc2	Harry
matky	matka	k1gFnSc2	matka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
dcerou	dcera	k1gFnSc7	dcera
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
nejmladším	mladý	k2eAgNnSc7d3	nejmladší
dítětem	dítě	k1gNnSc7	dítě
Harryho	Harry	k1gMnSc2	Harry
a	a	k8xC	a
Ginny	Ginna	k1gMnSc2	Ginna
je	být	k5eAaImIp3nS	být
Lily	lít	k5eAaImAgFnP	lít
Lenka	Lenka	k1gFnSc1	Lenka
Potterová	Potterová	k1gFnSc1	Potterová
<g/>
,	,	kIx,	,
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
po	po	k7c6	po
Harryho	Harry	k1gMnSc2	Harry
matce	matka	k1gFnSc6	matka
a	a	k8xC	a
přítelkyni	přítelkyně	k1gFnSc4	přítelkyně
svých	svůj	k3xOyFgMnPc2	svůj
rodičů	rodič	k1gMnPc2	rodič
Lence	Lenka	k1gFnSc3	Lenka
Láskorádové	Láskorád	k1gMnPc1	Láskorád
<g/>
.	.	kIx.	.
</s>
<s>
Vypadá	vypadat	k5eAaPmIp3nS	vypadat
jako	jako	k9	jako
její	její	k3xOp3gFnSc1	její
matka	matka	k1gFnSc1	matka
a	a	k8xC	a
babička	babička	k1gFnSc1	babička
Molly	Molla	k1gFnSc2	Molla
<g/>
.	.	kIx.	.
</s>
<s>
Doprovází	doprovázet	k5eAaImIp3nP	doprovázet
rodiče	rodič	k1gMnPc1	rodič
<g/>
,	,	kIx,	,
když	když	k8xS	když
posílají	posílat	k5eAaImIp3nP	posílat
své	svůj	k3xOyFgMnPc4	svůj
dva	dva	k4xCgMnPc4	dva
syny	syn	k1gMnPc4	syn
do	do	k7c2	do
Bradavic	bradavice	k1gFnPc2	bradavice
v	v	k7c6	v
epilogu	epilog	k1gInSc6	epilog
sedmého	sedmý	k4xOgInSc2	sedmý
dílu	díl	k1gInSc2	díl
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zařazena	zařadit	k5eAaPmNgFnS	zařadit
do	do	k7c2	do
Nebelvíru	Nebelvír	k1gInSc2	Nebelvír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sova	sova	k1gFnSc1	sova
Hedvika	Hedvika	k1gFnSc1	Hedvika
==	==	k?	==
</s>
</p>
<p>
<s>
Hedvika	Hedvika	k1gFnSc1	Hedvika
je	být	k5eAaImIp3nS	být
Harryho	Harry	k1gMnSc4	Harry
sova	sova	k1gFnSc1	sova
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
první	první	k4xOgFnSc6	první
knize	kniha	k1gFnSc6	kniha
(	(	kIx(	(
<g/>
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
kámen	kámen	k1gInSc1	kámen
mudrců	mudrc	k1gMnPc2	mudrc
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
80	[number]	k4	80
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
Hagrid	Hagrid	k1gInSc1	Hagrid
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
koupit	koupit	k5eAaPmF	koupit
Harrymu	Harrym	k1gInSc3	Harrym
dárek	dárek	k1gInSc4	dárek
k	k	k7c3	k
jedenáctým	jedenáctý	k4xOgFnPc3	jedenáctý
narozeninám	narozeniny	k1gFnPc3	narozeniny
<g/>
.	.	kIx.	.
</s>
<s>
Sovu	sova	k1gFnSc4	sova
koupí	koupit	k5eAaPmIp3nP	koupit
na	na	k7c6	na
Příčné	příčný	k2eAgFnSc6d1	příčná
ulici	ulice	k1gFnSc6	ulice
ve	v	k7c6	v
Velkoprodejně	velkoprodejna	k1gFnSc6	velkoprodejna
Mžourov	Mžourov	k1gInSc4	Mžourov
<g/>
.	.	kIx.	.
</s>
<s>
Harry	Harra	k1gFnPc1	Harra
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
sovu	sova	k1gFnSc4	sova
pojmenovat	pojmenovat	k5eAaPmF	pojmenovat
Hedvika	Hedvika	k1gFnSc1	Hedvika
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
najde	najít	k5eAaPmIp3nS	najít
toto	tento	k3xDgNnSc4	tento
jméno	jméno	k1gNnSc4	jméno
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
nové	nový	k2eAgFnSc6d1	nová
učebnici	učebnice	k1gFnSc6	učebnice
Dějiny	dějiny	k1gFnPc1	dějiny
čar	čára	k1gFnPc2	čára
a	a	k8xC	a
kouzel	kouzlo	k1gNnPc2	kouzlo
<g/>
.	.	kIx.	.
</s>
<s>
Hedvika	Hedvika	k1gFnSc1	Hedvika
s	s	k7c7	s
Harrym	Harrymum	k1gNnPc2	Harrymum
prožívá	prožívat	k5eAaImIp3nS	prožívat
všechny	všechen	k3xTgInPc4	všechen
roky	rok	k1gInPc4	rok
jeho	jeho	k3xOp3gNnSc2	jeho
studia	studio	k1gNnSc2	studio
na	na	k7c6	na
Škole	škola	k1gFnSc6	škola
čar	čára	k1gFnPc2	čára
a	a	k8xC	a
kouzel	kouzlo	k1gNnPc2	kouzlo
v	v	k7c6	v
Bradavicích	bradavice	k1gFnPc6	bradavice
<g/>
.	.	kIx.	.
</s>
<s>
Většinu	většina	k1gFnSc4	většina
času	čas	k1gInSc2	čas
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
školním	školní	k2eAgInSc6d1	školní
sovinci	sovinec	k1gInSc6	sovinec
<g/>
.	.	kIx.	.
</s>
<s>
Hedvika	Hedvika	k1gFnSc1	Hedvika
zemře	zemřít	k5eAaPmIp3nS	zemřít
v	v	k7c6	v
sedmém	sedmý	k4xOgNnSc6	sedmý
díle	dílo	k1gNnSc6	dílo
knihy	kniha	k1gFnSc2	kniha
(	(	kIx(	(
<g/>
Harry	Harra	k1gMnSc2	Harra
Potter	Potter	k1gInSc1	Potter
a	a	k8xC	a
Relikvie	relikvie	k1gFnSc1	relikvie
smrti	smrt	k1gFnSc2	smrt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
převozu	převoz	k1gInSc6	převoz
Harryho	Harry	k1gMnSc2	Harry
ze	z	k7c2	z
Zobí	Zobí	k2eAgFnSc2d1	Zobí
ulice	ulice	k1gFnSc2	ulice
do	do	k7c2	do
bezpečí	bezpečí	k1gNnSc2	bezpečí
jí	on	k3xPp3gFnSc3	on
zasáhne	zasáhnout	k5eAaPmIp3nS	zasáhnout
smrtící	smrtící	k2eAgFnSc1d1	smrtící
kletba	kletba	k1gFnSc1	kletba
jednoho	jeden	k4xCgMnSc2	jeden
ze	z	k7c2	z
Smrtijedů	Smrtijed	k1gMnPc2	Smrtijed
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gInSc1	Potter
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Harry	Harra	k1gFnSc2	Harra
Potter	Potter	k1gInSc1	Potter
(	(	kIx(	(
<g/>
postava	postava	k1gFnSc1	postava
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
Harry	Harra	k1gFnPc1	Harra
Potter	Pottrum	k1gNnPc2	Pottrum
na	na	k7c4	na
Postavy	postav	k1gInPc4	postav
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
