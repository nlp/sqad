<s>
Chlamydióza	Chlamydióza	k1gFnSc1	Chlamydióza
je	být	k5eAaImIp3nS	být
obecné	obecný	k2eAgNnSc4d1	obecné
označení	označení	k1gNnSc4	označení
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
infekce	infekce	k1gFnSc2	infekce
různých	různý	k2eAgInPc2d1	různý
živočišných	živočišný	k2eAgInPc2d1	živočišný
druhů	druh	k1gInPc2	druh
vyvolávaných	vyvolávaný	k2eAgInPc2d1	vyvolávaný
obligátně	obligátně	k6eAd1	obligátně
intracelulárními	intracelulární	k2eAgFnPc7d1	intracelulární
bakteriemi	bakterie	k1gFnPc7	bakterie
z	z	k7c2	z
řádu	řád	k1gInSc2	řád
Chlamydiales	Chlamydialesa	k1gFnPc2	Chlamydialesa
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
infekce	infekce	k1gFnSc1	infekce
Chlamydophila	Chlamydophila	k1gFnSc1	Chlamydophila
psittaci	psittace	k1gFnSc3	psittace
jako	jako	k8xC	jako
vysoce	vysoce	k6eAd1	vysoce
nakažlivé	nakažlivý	k2eAgFnPc4d1	nakažlivá
<g/>
,	,	kIx,	,
zjevně	zjevně	k6eAd1	zjevně
nebo	nebo	k8xC	nebo
latentně	latentně	k6eAd1	latentně
probíhající	probíhající	k2eAgFnPc1d1	probíhající
respirační	respirační	k2eAgFnPc1d1	respirační
<g/>
,	,	kIx,	,
střevní	střevní	k2eAgNnSc1d1	střevní
nebo	nebo	k8xC	nebo
systémové	systémový	k2eAgNnSc1d1	systémové
onemocnění	onemocnění	k1gNnSc1	onemocnění
charakterizované	charakterizovaný	k2eAgNnSc1d1	charakterizované
zánětem	zánět	k1gInSc7	zánět
<g/>
,	,	kIx,	,
nekrózou	nekróza	k1gFnSc7	nekróza
a	a	k8xC	a
proliferativní	proliferativní	k2eAgFnSc7d1	proliferativní
reakcí	reakce	k1gFnSc7	reakce
na	na	k7c6	na
epikardu	epikard	k1gInSc6	epikard
<g/>
,	,	kIx,	,
vzdušných	vzdušný	k2eAgInPc6d1	vzdušný
vacích	vak	k1gInPc6	vak
<g/>
,	,	kIx,	,
plicích	plíce	k1gFnPc6	plíce
<g/>
,	,	kIx,	,
peritoneální	peritoneálnit	k5eAaPmIp3nP	peritoneálnit
seróze	seróza	k1gFnSc3	seróza
<g/>
,	,	kIx,	,
játrech	játra	k1gNnPc6	játra
<g/>
,	,	kIx,	,
perikardu	perikard	k1gInSc6	perikard
a	a	k8xC	a
střevech	střevo	k1gNnPc6	střevo
<g/>
.	.	kIx.	.
</s>
<s>
Generalizované	generalizovaný	k2eAgFnPc1d1	generalizovaná
infekce	infekce	k1gFnPc1	infekce
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
hypertermii	hypertermie	k1gFnSc3	hypertermie
<g/>
,	,	kIx,	,
anorexii	anorexie	k1gFnSc6	anorexie
<g/>
,	,	kIx,	,
letargii	letargie	k1gFnSc6	letargie
<g/>
,	,	kIx,	,
průjmu	průjem	k1gInSc6	průjem
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
až	až	k9	až
k	k	k7c3	k
úhynu	úhyn	k1gInSc3	úhyn
<g/>
.	.	kIx.	.
</s>
<s>
Manifestace	manifestace	k1gFnSc1	manifestace
nemoci	nemoc	k1gFnSc2	nemoc
je	být	k5eAaImIp3nS	být
ovlivňována	ovlivňovat	k5eAaImNgFnS	ovlivňovat
virulencí	virulence	k1gFnSc7	virulence
kmene	kmen	k1gInSc2	kmen
C.	C.	kA	C.
psittaci	psittace	k1gFnSc4	psittace
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
savců	savec	k1gMnPc2	savec
infekce	infekce	k1gFnSc2	infekce
C.	C.	kA	C.
psittaci	psittace	k1gFnSc4	psittace
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
pneumonie	pneumonie	k1gFnSc2	pneumonie
<g/>
,	,	kIx,	,
kloubní	kloubní	k2eAgFnSc2d1	kloubní
<g/>
,	,	kIx,	,
střevní	střevní	k2eAgFnSc2d1	střevní
a	a	k8xC	a
reprodukční	reprodukční	k2eAgFnSc2d1	reprodukční
poruchy	porucha	k1gFnSc2	porucha
<g/>
.	.	kIx.	.
</s>
<s>
Chlamydióza	Chlamydióza	k1gFnSc1	Chlamydióza
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
ekonomicky	ekonomicky	k6eAd1	ekonomicky
velmi	velmi	k6eAd1	velmi
závažné	závažný	k2eAgNnSc1d1	závažné
onemocnění	onemocnění	k1gNnSc1	onemocnění
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
intenzivních	intenzivní	k2eAgInPc6d1	intenzivní
chovech	chov	k1gInPc6	chov
kachen	kachna	k1gFnPc2	kachna
a	a	k8xC	a
krůt	krůta	k1gFnPc2	krůta
<g/>
,	,	kIx,	,
příp	příp	kA	příp
<g/>
.	.	kIx.	.
u	u	k7c2	u
chovatelů	chovatel	k1gMnPc2	chovatel
holubů	holub	k1gMnPc2	holub
a	a	k8xC	a
exotických	exotický	k2eAgMnPc2d1	exotický
ptáků	pták	k1gMnPc2	pták
(	(	kIx(	(
<g/>
konfiskace	konfiskace	k1gFnSc1	konfiskace
na	na	k7c6	na
porážce	porážka	k1gFnSc6	porážka
<g/>
,	,	kIx,	,
pokles	pokles	k1gInSc1	pokles
snášky	snáška	k1gFnSc2	snáška
<g/>
,	,	kIx,	,
vysoké	vysoký	k2eAgInPc4d1	vysoký
náklady	náklad	k1gInPc4	náklad
na	na	k7c4	na
léčbu	léčba	k1gFnSc4	léčba
<g/>
,	,	kIx,	,
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
trhem	trh	k1gInSc7	trh
ptáků	pták	k1gMnPc2	pták
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chlamydióza	Chlamydióza	k1gFnSc1	Chlamydióza
drůbeže	drůbež	k1gFnSc2	drůbež
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
nebezpečnou	bezpečný	k2eNgFnSc4d1	nebezpečná
nákazu	nákaza	k1gFnSc4	nákaza
<g/>
.	.	kIx.	.
</s>
<s>
Neméně	málo	k6eNd2	málo
důležitý	důležitý	k2eAgInSc1d1	důležitý
je	být	k5eAaImIp3nS	být
zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
význam	význam	k1gInSc1	význam
<g/>
.	.	kIx.	.
</s>
<s>
Chlamydióza	Chlamydióza	k1gFnSc1	Chlamydióza
je	být	k5eAaImIp3nS	být
zoonózou	zoonóza	k1gFnSc7	zoonóza
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
probíhá	probíhat	k5eAaImIp3nS	probíhat
za	za	k7c2	za
příznaků	příznak	k1gInPc2	příznak
atypických	atypický	k2eAgInPc2d1	atypický
zánětů	zánět	k1gInPc2	zánět
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Přenos	přenos	k1gInSc1	přenos
C.	C.	kA	C.
psittaci	psittace	k1gFnSc4	psittace
z	z	k7c2	z
člověka	člověk	k1gMnSc2	člověk
na	na	k7c4	na
člověka	člověk	k1gMnSc4	člověk
je	být	k5eAaImIp3nS	být
vzácný	vzácný	k2eAgMnSc1d1	vzácný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
možný	možný	k2eAgInSc1d1	možný
<g/>
.	.	kIx.	.
</s>
<s>
Přenos	přenos	k1gInSc1	přenos
z	z	k7c2	z
člověka	člověk	k1gMnSc2	člověk
na	na	k7c4	na
ptáka	pták	k1gMnSc4	pták
nebo	nebo	k8xC	nebo
drůbežím	drůbeží	k2eAgNnSc7d1	drůbeží
masem	maso	k1gNnSc7	maso
nebyl	být	k5eNaImAgInS	být
zatím	zatím	k6eAd1	zatím
popsán	popsat	k5eAaPmNgInS	popsat
<g/>
.	.	kIx.	.
</s>
<s>
Chlamydie	chlamydie	k1gFnSc1	chlamydie
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejrozšířenější	rozšířený	k2eAgInPc4d3	nejrozšířenější
mikroorganismy	mikroorganismus	k1gInPc4	mikroorganismus
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgFnP	být
již	již	k6eAd1	již
prokázány	prokázat	k5eAaPmNgFnP	prokázat
minimálně	minimálně	k6eAd1	minimálně
u	u	k7c2	u
230	[number]	k4	230
různých	různý	k2eAgInPc2d1	různý
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
u	u	k7c2	u
kterých	který	k3yIgInPc2	který
mohou	moct	k5eAaImIp3nP	moct
vyvolávat	vyvolávat	k5eAaImF	vyvolávat
manifestní	manifestní	k2eAgFnPc1d1	manifestní
i	i	k8xC	i
latentní	latentní	k2eAgFnPc1d1	latentní
infekce	infekce	k1gFnPc1	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
postihují	postihovat	k5eAaImIp3nP	postihovat
tropické	tropický	k2eAgMnPc4d1	tropický
chovné	chovný	k2eAgMnPc4d1	chovný
ptáky	pták	k1gMnPc4	pták
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
papouškovité	papouškovitý	k2eAgNnSc1d1	papouškovitý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
domácí	domácí	k2eAgMnPc4d1	domácí
ptáky	pták	k1gMnPc4	pták
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
kachny	kachna	k1gFnPc4	kachna
<g/>
,	,	kIx,	,
husy	husa	k1gFnPc4	husa
<g/>
,	,	kIx,	,
krůty	krůta	k1gFnPc4	krůta
a	a	k8xC	a
holuby	holub	k1gMnPc4	holub
<g/>
.	.	kIx.	.
</s>
<s>
Chlamydie	chlamydie	k1gFnPc1	chlamydie
jsou	být	k5eAaImIp3nP	být
patogenní	patogenní	k2eAgFnPc1d1	patogenní
také	také	k9	také
pro	pro	k7c4	pro
různé	různý	k2eAgInPc4d1	různý
druhy	druh	k1gInPc4	druh
laboratorních	laboratorní	k2eAgNnPc2d1	laboratorní
zvířat	zvíře	k1gNnPc2	zvíře
(	(	kIx(	(
<g/>
časté	častý	k2eAgFnPc4d1	častá
infekce	infekce	k1gFnPc4	infekce
v	v	k7c6	v
chovech	chov	k1gInPc6	chov
myší	myš	k1gFnPc2	myš
<g/>
)	)	kIx)	)
a	a	k8xC	a
divoce	divoce	k6eAd1	divoce
žijící	žijící	k2eAgNnPc1d1	žijící
zvířata	zvíře	k1gNnPc1	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Rozšíření	rozšíření	k1gNnSc1	rozšíření
chlamydií	chlamydie	k1gFnPc2	chlamydie
sahá	sahat	k5eAaImIp3nS	sahat
od	od	k7c2	od
subarktických	subarktický	k2eAgFnPc2d1	subarktická
oblastí	oblast	k1gFnPc2	oblast
přes	přes	k7c4	přes
mírné	mírný	k2eAgFnPc4d1	mírná
a	a	k8xC	a
tropické	tropický	k2eAgFnPc4d1	tropická
oblasti	oblast	k1gFnPc4	oblast
Starého	Starého	k2eAgInSc2d1	Starého
Světa	svět	k1gInSc2	svět
až	až	k9	až
do	do	k7c2	do
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
domácích	domácí	k2eAgNnPc2d1	domácí
zvířat	zvíře	k1gNnPc2	zvíře
(	(	kIx(	(
<g/>
skot	skot	k1gInSc1	skot
<g/>
,	,	kIx,	,
ovce	ovce	k1gFnSc1	ovce
<g/>
,	,	kIx,	,
kozy	koza	k1gFnPc1	koza
<g/>
,	,	kIx,	,
prasata	prase	k1gNnPc1	prase
<g/>
,	,	kIx,	,
koně	kůň	k1gMnPc1	kůň
<g/>
,	,	kIx,	,
psi	pes	k1gMnPc1	pes
<g/>
,	,	kIx,	,
kočky	kočka	k1gFnPc1	kočka
<g/>
)	)	kIx)	)
vyvolávají	vyvolávat	k5eAaImIp3nP	vyvolávat
chlamydie	chlamydie	k1gFnPc1	chlamydie
celé	celý	k2eAgNnSc1d1	celé
spektrum	spektrum	k1gNnSc1	spektrum
chorob	choroba	k1gFnPc2	choroba
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
pneumonie	pneumonie	k1gFnPc1	pneumonie
<g/>
,	,	kIx,	,
encefalitidy	encefalitida	k1gFnPc1	encefalitida
(	(	kIx(	(
<g/>
zánět	zánět	k1gInSc1	zánět
mozku	mozek	k1gInSc2	mozek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
encefalomyelitidy	encefalomyelitis	k1gFnSc2	encefalomyelitis
<g/>
,	,	kIx,	,
perikarditidy	perikarditis	k1gFnSc2	perikarditis
<g/>
,	,	kIx,	,
polyartritidy	polyartritis	k1gFnSc2	polyartritis
(	(	kIx(	(
<g/>
mnohočetný	mnohočetný	k2eAgInSc1d1	mnohočetný
zánět	zánět	k1gInSc1	zánět
kloubů	kloub	k1gInPc2	kloub
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
konjunktivitidy	konjunktivitis	k1gFnPc1	konjunktivitis
<g/>
,	,	kIx,	,
enteritidy	enteritida	k1gFnPc1	enteritida
<g/>
,	,	kIx,	,
mastitidy	mastitida	k1gFnPc1	mastitida
<g/>
,	,	kIx,	,
orchitidy	orchitida	k1gFnPc1	orchitida
(	(	kIx(	(
<g/>
zánět	zánět	k1gInSc1	zánět
vasrlat	vasrle	k1gNnPc2	vasrle
<g/>
)	)	kIx)	)
a	a	k8xC	a
aborty	abort	k1gInPc1	abort
<g/>
,	,	kIx,	,
s	s	k7c7	s
možnými	možný	k2eAgFnPc7d1	možná
závažnými	závažný	k2eAgFnPc7d1	závažná
hospodářskými	hospodářský	k2eAgFnPc7d1	hospodářská
ztrátami	ztráta	k1gFnPc7	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vztah	vztah	k1gInSc4	vztah
chlamydií	chlamydie	k1gFnPc2	chlamydie
ke	k	k7c3	k
klíšťatům	klíště	k1gNnPc3	klíště
<g/>
,	,	kIx,	,
ektoparazitickým	ektoparazitický	k2eAgFnPc3d1	ektoparazitický
vším	veš	k1gFnPc3	veš
a	a	k8xC	a
roztočům	roztoč	k1gMnPc3	roztoč
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
potenciálním	potenciální	k2eAgMnPc3d1	potenciální
přenášečům	přenášeč	k1gMnPc3	přenášeč
chlamydií	chlamydie	k1gFnPc2	chlamydie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
názory	názor	k1gInPc7	názor
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Savčí	savčí	k2eAgFnSc1d1	savčí
chlamydie	chlamydie	k1gFnSc1	chlamydie
(	(	kIx(	(
<g/>
C.	C.	kA	C.
trachomatis	trachomatis	k1gFnSc2	trachomatis
<g/>
)	)	kIx)	)
nejsou	být	k5eNaImIp3nP	být
odpovědné	odpovědný	k2eAgFnPc1d1	odpovědná
za	za	k7c4	za
výskyt	výskyt	k1gInSc4	výskyt
infekce	infekce	k1gFnSc2	infekce
u	u	k7c2	u
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Původcem	původce	k1gMnSc7	původce
onemocnění	onemocnění	k1gNnSc2	onemocnění
je	být	k5eAaImIp3nS	být
Chlamydophila	Chlamydophila	k1gFnSc1	Chlamydophila
psittaci	psittace	k1gFnSc4	psittace
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
přirozené	přirozený	k2eAgFnSc2d1	přirozená
patogenity	patogenita	k1gFnSc2	patogenita
pro	pro	k7c4	pro
domácí	domácí	k2eAgFnSc4d1	domácí
drůbež	drůbež	k1gFnSc4	drůbež
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
kmeny	kmen	k1gInPc1	kmen
C.	C.	kA	C.
psittaci	psittace	k1gFnSc6	psittace
izolované	izolovaný	k2eAgFnSc6d1	izolovaná
z	z	k7c2	z
těchto	tento	k3xDgMnPc2	tento
hostitelů	hostitel	k1gMnPc2	hostitel
rozdělit	rozdělit	k5eAaPmF	rozdělit
do	do	k7c2	do
2	[number]	k4	2
základních	základní	k2eAgFnPc2d1	základní
kategorií	kategorie	k1gFnPc2	kategorie
<g/>
:	:	kIx,	:
Vysoce	vysoce	k6eAd1	vysoce
virulentní	virulentní	k2eAgInPc1d1	virulentní
kmeny	kmen	k1gInPc1	kmen
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
akutní	akutní	k2eAgFnPc4d1	akutní
epidemie	epidemie	k1gFnPc4	epidemie
s	s	k7c7	s
5-30	[number]	k4	5-30
%	%	kIx~	%
úhynem	úhyn	k1gInSc7	úhyn
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Kmeny	kmen	k1gInPc1	kmen
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
jsou	být	k5eAaImIp3nP	být
nejčastěji	často	k6eAd3	často
izolovány	izolovat	k5eAaBmNgInP	izolovat
z	z	k7c2	z
krůt	krůta	k1gFnPc2	krůta
a	a	k8xC	a
občas	občas	k6eAd1	občas
ze	z	k7c2	z
zdravých	zdravý	k2eAgMnPc2d1	zdravý
divoce	divoce	k6eAd1	divoce
žijících	žijící	k2eAgMnPc2d1	žijící
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Označují	označovat	k5eAaImIp3nP	označovat
se	se	k3xPyFc4	se
také	také	k9	také
jako	jako	k9	jako
"	"	kIx"	"
<g/>
toxinogenní	toxinogenní	k2eAgFnSc1d1	toxinogenní
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Toxinogenní	Toxinogenní	k2eAgInPc1d1	Toxinogenní
kmeny	kmen	k1gInPc1	kmen
mají	mít	k5eAaImIp3nP	mít
široké	široký	k2eAgNnSc4d1	široké
spektrum	spektrum	k1gNnSc4	spektrum
patogenity	patogenita	k1gFnSc2	patogenita
pro	pro	k7c4	pro
laboratorní	laboratorní	k2eAgNnPc4d1	laboratorní
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
způsobit	způsobit	k5eAaPmF	způsobit
i	i	k9	i
onemocnění	onemocnění	k1gNnSc1	onemocnění
člověka	člověk	k1gMnSc2	člověk
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
i	i	k9	i
smrtelné	smrtelný	k2eAgFnSc3d1	smrtelná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toxinogenní	Toxinogenní	k2eAgInPc1d1	Toxinogenní
kmeny	kmen	k1gInPc1	kmen
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
laboratorní	laboratorní	k2eAgNnPc4d1	laboratorní
zvířata	zvíře	k1gNnPc4	zvíře
vysoce	vysoce	k6eAd1	vysoce
infekční	infekční	k2eAgFnSc2d1	infekční
a	a	k8xC	a
letální	letální	k2eAgFnSc2d1	letální
(	(	kIx(	(
<g/>
smrtelné	smrtelný	k2eAgFnSc2d1	smrtelná
<g/>
)	)	kIx)	)
např.	např.	kA	např.
pro	pro	k7c4	pro
myš	myš	k1gFnSc4	myš
<g/>
,	,	kIx,	,
morče	morče	k1gNnSc4	morče
<g/>
,	,	kIx,	,
krůty	krůta	k1gFnPc4	krůta
a	a	k8xC	a
papoušky	papoušek	k1gMnPc4	papoušek
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neškodné	škodný	k2eNgNnSc1d1	neškodné
pro	pro	k7c4	pro
holuby	holub	k1gMnPc4	holub
a	a	k8xC	a
vrabce	vrabec	k1gMnPc4	vrabec
<g/>
.	.	kIx.	.
</s>
<s>
Kmeny	kmen	k1gInPc1	kmen
o	o	k7c6	o
nízké	nízký	k2eAgFnSc6d1	nízká
virulenci	virulence	k1gFnSc6	virulence
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
slabé	slabý	k2eAgFnPc1d1	slabá
epidemie	epidemie	k1gFnPc1	epidemie
s	s	k7c7	s
mortalitou	mortalita	k1gFnSc7	mortalita
nižší	nízký	k2eAgFnSc7d2	nižší
než	než	k8xS	než
5	[number]	k4	5
%	%	kIx~	%
<g/>
,	,	kIx,	,
jestliže	jestliže	k8xS	jestliže
nedojde	dojít	k5eNaPmIp3nS	dojít
k	k	k7c3	k
sekundární	sekundární	k2eAgFnSc3d1	sekundární
bakteriální	bakteriální	k2eAgFnSc3d1	bakteriální
nebo	nebo	k8xC	nebo
parazitární	parazitární	k2eAgFnSc3d1	parazitární
komplikaci	komplikace	k1gFnSc3	komplikace
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
izolovány	izolovat	k5eAaBmNgInP	izolovat
z	z	k7c2	z
holubů	holub	k1gMnPc2	holub
a	a	k8xC	a
kachen	kachna	k1gFnPc2	kachna
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
z	z	k7c2	z
krůt	krůta	k1gFnPc2	krůta
a	a	k8xC	a
divokých	divoký	k2eAgMnPc2d1	divoký
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
infikovaní	infikovaný	k2eAgMnPc1d1	infikovaný
těmito	tento	k3xDgInPc7	tento
kmeny	kmen	k1gInPc7	kmen
obvykle	obvykle	k6eAd1	obvykle
neonemocní	onemocnět	k5eNaPmIp3nP	onemocnět
<g/>
.	.	kIx.	.
</s>
<s>
Toxinogenní	Toxinogenní	k2eAgInSc1d1	Toxinogenní
titr	titr	k1gInSc1	titr
na	na	k7c6	na
myších	myš	k1gFnPc6	myš
je	být	k5eAaImIp3nS	být
mnohem	mnohem	k6eAd1	mnohem
nižší	nízký	k2eAgFnSc1d2	nižší
než	než	k8xS	než
u	u	k7c2	u
předchozí	předchozí	k2eAgFnSc2d1	předchozí
kategorie	kategorie	k1gFnSc2	kategorie
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
těmto	tento	k3xDgInPc3	tento
kmenům	kmen	k1gInPc3	kmen
méně	málo	k6eAd2	málo
vnímavý	vnímavý	k2eAgMnSc1d1	vnímavý
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
infekciozitu	infekciozita	k1gFnSc4	infekciozita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
malou	malý	k2eAgFnSc4d1	malá
letalitu	letalita	k1gFnSc4	letalita
pro	pro	k7c4	pro
myši	myš	k1gFnPc4	myš
<g/>
,	,	kIx,	,
holuby	holub	k1gMnPc4	holub
<g/>
,	,	kIx,	,
vrabce	vrabec	k1gMnPc4	vrabec
a	a	k8xC	a
krůty	krůta	k1gFnPc4	krůta
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
vyvolají	vyvolat	k5eAaPmIp3nP	vyvolat
infekci	infekce	k1gFnSc4	infekce
u	u	k7c2	u
morčat	morče	k1gNnPc2	morče
<g/>
.	.	kIx.	.
</s>
<s>
Kmeny	kmen	k1gInPc1	kmen
obou	dva	k4xCgFnPc2	dva
skupin	skupina	k1gFnPc2	skupina
mají	mít	k5eAaImIp3nP	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
schopnost	schopnost	k1gFnSc4	schopnost
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
šířit	šířit	k5eAaImF	šířit
v	v	k7c6	v
hejnu	hejno	k1gNnSc6	hejno
drůbeže	drůbež	k1gFnSc2	drůbež
<g/>
.	.	kIx.	.
</s>
<s>
Chlamydióza	Chlamydióza	k1gFnSc1	Chlamydióza
holubů	holub	k1gMnPc2	holub
a	a	k8xC	a
kachen	kachna	k1gFnPc2	kachna
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
komplikována	komplikován	k2eAgFnSc1d1	komplikována
konkurentními	konkurentní	k2eAgFnPc7d1	konkurentní
infekcemi	infekce	k1gFnPc7	infekce
salmonel	salmonela	k1gFnPc2	salmonela
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovém	takový	k3xDgInSc6	takový
případě	případ	k1gInSc6	případ
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
mortalita	mortalita	k1gFnSc1	mortalita
vyšší	vysoký	k2eAgFnSc1d2	vyšší
<g/>
,	,	kIx,	,
chlamydie	chlamydie	k1gFnPc1	chlamydie
jsou	být	k5eAaImIp3nP	být
vylučovány	vylučovat	k5eAaImNgFnP	vylučovat
ve	v	k7c6	v
velkém	velký	k2eAgNnSc6d1	velké
množství	množství	k1gNnSc6	množství
a	a	k8xC	a
vnímavý	vnímavý	k2eAgMnSc1d1	vnímavý
hostitel	hostitel	k1gMnSc1	hostitel
v	v	k7c6	v
bezprostředním	bezprostřední	k2eAgNnSc6d1	bezprostřední
kontaktu	kontakt	k1gInSc6	kontakt
může	moct	k5eAaImIp3nS	moct
klinicky	klinicky	k6eAd1	klinicky
onemocnět	onemocnět	k5eAaPmF	onemocnět
<g/>
.	.	kIx.	.
</s>
<s>
Přirozeným	přirozený	k2eAgMnSc7d1	přirozený
hostitelem	hostitel	k1gMnSc7	hostitel
C.	C.	kA	C.
psittaci	psittace	k1gFnSc6	psittace
jsou	být	k5eAaImIp3nP	být
jak	jak	k6eAd1	jak
domácí	domácí	k2eAgMnPc1d1	domácí
a	a	k8xC	a
exotičtí	exotický	k2eAgMnPc1d1	exotický
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
mnoho	mnoho	k4c4	mnoho
druhů	druh	k1gInPc2	druh
volně	volně	k6eAd1	volně
žijících	žijící	k2eAgMnPc2d1	žijící
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Častým	častý	k2eAgInSc7d1	častý
rezervoárem	rezervoár	k1gInSc7	rezervoár
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
vysoce	vysoce	k6eAd1	vysoce
virulentních	virulentní	k2eAgInPc2d1	virulentní
kmenů	kmen	k1gInPc2	kmen
C.	C.	kA	C.
psittaci	psittace	k1gFnSc4	psittace
bývají	bývat	k5eAaImIp3nP	bývat
mořští	mořský	k2eAgMnPc1d1	mořský
a	a	k8xC	a
tažní	tažní	k2eAgMnPc1d1	tažní
ptáci	pták	k1gMnPc1	pták
jako	jako	k9	jako
jsou	být	k5eAaImIp3nP	být
rackové	racek	k1gMnPc1	racek
<g/>
,	,	kIx,	,
rajky	rajka	k1gFnPc4	rajka
<g/>
,	,	kIx,	,
divoké	divoký	k2eAgFnPc4d1	divoká
kachny	kachna	k1gFnPc4	kachna
a	a	k8xC	a
husy	husa	k1gFnPc4	husa
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
klinickému	klinický	k2eAgNnSc3d1	klinické
onemocnění	onemocnění	k1gNnSc3	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
u	u	k7c2	u
divoce	divoce	k6eAd1	divoce
žijících	žijící	k2eAgMnPc2d1	žijící
ptáků	pták	k1gMnPc2	pták
se	se	k3xPyFc4	se
ojediněle	ojediněle	k6eAd1	ojediněle
projevuje	projevovat	k5eAaImIp3nS	projevovat
pouze	pouze	k6eAd1	pouze
zvýšeným	zvýšený	k2eAgInSc7d1	zvýšený
úhynem	úhyn	k1gInSc7	úhyn
mláďat	mládě	k1gNnPc2	mládě
<g/>
,	,	kIx,	,
kdežto	kdežto	k8xS	kdežto
u	u	k7c2	u
dospělých	dospělí	k1gMnPc2	dospělí
zpravidla	zpravidla	k6eAd1	zpravidla
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
latentní	latentní	k2eAgFnSc6d1	latentní
(	(	kIx(	(
<g/>
skryté	skrytý	k2eAgFnSc6d1	skrytá
<g/>
)	)	kIx)	)
formě	forma	k1gFnSc6	forma
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
vylučování	vylučování	k1gNnSc2	vylučování
i	i	k8xC	i
množství	množství	k1gNnSc2	množství
vylučovaných	vylučovaný	k2eAgFnPc2d1	vylučovaná
chlamydií	chlamydie	k1gFnPc2	chlamydie
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
druhu	druh	k1gInSc6	druh
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
i	i	k9	i
sérologická	sérologický	k2eAgFnSc1d1	sérologická
konverze	konverze	k1gFnSc1	konverze
<g/>
.	.	kIx.	.
</s>
<s>
Mladá	mladý	k2eAgFnSc1d1	mladá
drůbež	drůbež	k1gFnSc1	drůbež
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
infekci	infekce	k1gFnSc3	infekce
i	i	k8xC	i
onemocnění	onemocnění	k1gNnSc3	onemocnění
obecně	obecně	k6eAd1	obecně
vnímavější	vnímavý	k2eAgFnSc4d2	vnímavější
než	než	k8xS	než
drůbež	drůbež	k1gFnSc4	drůbež
starší	starý	k2eAgMnPc1d2	starší
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
např.	např.	kA	např.
infekce	infekce	k1gFnSc2	infekce
starých	starý	k2eAgFnPc2d1	stará
krůt	krůta	k1gFnPc2	krůta
může	moct	k5eAaImIp3nS	moct
proběhnout	proběhnout	k5eAaPmF	proběhnout
i	i	k9	i
bez	bez	k7c2	bez
příznaků	příznak	k1gInPc2	příznak
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
nedojde	dojít	k5eNaPmIp3nS	dojít
ke	k	k7c3	k
stresování	stresování	k1gNnSc3	stresování
a	a	k8xC	a
exacerbaci	exacerbace	k1gFnSc3	exacerbace
nemoci	nemoc	k1gFnSc3	nemoc
<g/>
.	.	kIx.	.
</s>
<s>
Krocani	krocan	k1gMnPc1	krocan
zpravidla	zpravidla	k6eAd1	zpravidla
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
vyšší	vysoký	k2eAgFnSc4d2	vyšší
mortalitu	mortalita	k1gFnSc4	mortalita
než	než	k8xS	než
krůty	krůta	k1gFnPc4	krůta
<g/>
.	.	kIx.	.
</s>
<s>
Kur	kur	k1gMnSc1	kur
domácí	domácí	k1gMnSc1	domácí
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
infekci	infekce	k1gFnSc3	infekce
relativně	relativně	k6eAd1	relativně
odolnější	odolný	k2eAgFnSc4d2	odolnější
než	než	k8xS	než
vodní	vodní	k2eAgFnSc4d1	vodní
drůbež	drůbež	k1gFnSc4	drůbež
<g/>
.	.	kIx.	.
</s>
<s>
Chlamydie	chlamydie	k1gFnPc1	chlamydie
jsou	být	k5eAaImIp3nP	být
vylučovány	vylučovat	k5eAaImNgFnP	vylučovat
z	z	k7c2	z
organismu	organismus	k1gInSc2	organismus
exkrety	exkret	k1gInPc1	exkret
a	a	k8xC	a
sekrey	sekrey	k1gInPc1	sekrey
infikovaných	infikovaný	k2eAgMnPc2d1	infikovaný
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Infekce	infekce	k1gFnSc1	infekce
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
šíří	šířit	k5eAaImIp3nS	šířit
horizontálně	horizontálně	k6eAd1	horizontálně
přímým	přímý	k2eAgInSc7d1	přímý
i	i	k8xC	i
nepřímým	přímý	k2eNgInSc7d1	nepřímý
kontaktem	kontakt	k1gInSc7	kontakt
s	s	k7c7	s
infikovanými	infikovaný	k2eAgMnPc7d1	infikovaný
jedinci	jedinec	k1gMnPc7	jedinec
nebo	nebo	k8xC	nebo
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
nakažení	nakažení	k1gNnSc3	nakažení
dochází	docházet	k5eAaImIp3nS	docházet
respirační	respirační	k2eAgFnSc1d1	respirační
cestou	cesta	k1gFnSc7	cesta
(	(	kIx(	(
<g/>
vdechováním	vdechování	k1gNnSc7	vdechování
zvířeného	zvířený	k2eAgInSc2d1	zvířený
kontaminovaného	kontaminovaný	k2eAgInSc2d1	kontaminovaný
trusu	trus	k1gInSc2	trus
<g/>
,	,	kIx,	,
prachu	prach	k1gInSc2	prach
<g/>
,	,	kIx,	,
zaschlých	zaschlý	k2eAgInPc2d1	zaschlý
sekretů	sekret	k1gInPc2	sekret
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
často	často	k6eAd1	často
orálně	orálně	k6eAd1	orálně
<g/>
.	.	kIx.	.
</s>
<s>
Transovariální	Transovariální	k2eAgInSc1d1	Transovariální
přenos	přenos	k1gInSc1	přenos
chlamydií	chlamydie	k1gFnPc2	chlamydie
byl	být	k5eAaImAgInS	být
prokázán	prokázat	k5eAaPmNgInS	prokázat
u	u	k7c2	u
mnoha	mnoho	k4c2	mnoho
druhů	druh	k1gInPc2	druh
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Vektorem	vektor	k1gInSc7	vektor
jsou	být	k5eAaImIp3nP	být
inaparentně	inaparentně	k6eAd1	inaparentně
infikovaní	infikovaný	k2eAgMnPc1d1	infikovaný
volně	volně	k6eAd1	volně
žijící	žijící	k2eAgMnPc1d1	žijící
a	a	k8xC	a
migrující	migrující	k2eAgMnPc1d1	migrující
ptáci	pták	k1gMnPc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Význam	význam	k1gInSc1	význam
členovců	členovec	k1gMnPc2	členovec
v	v	k7c6	v
přenosu	přenos	k1gInSc6	přenos
infekce	infekce	k1gFnSc2	infekce
není	být	k5eNaImIp3nS	být
jasný	jasný	k2eAgInSc1d1	jasný
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
chlamydie	chlamydie	k1gFnPc1	chlamydie
byly	být	k5eAaImAgFnP	být
prokázány	prokázat	k5eAaPmNgFnP	prokázat
v	v	k7c6	v
homogenátu	homogenát	k1gInSc6	homogenát
čmelíků	čmelík	k1gMnPc2	čmelík
cizopasících	cizopasící	k2eAgMnPc2d1	cizopasící
u	u	k7c2	u
krůt	krůta	k1gFnPc2	krůta
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
možný	možný	k2eAgInSc1d1	možný
přenašeč	přenašeč	k1gInSc1	přenašeč
chlamydií	chlamydie	k1gFnPc2	chlamydie
v	v	k7c6	v
chovech	chov	k1gInPc6	chov
krůt	krůta	k1gFnPc2	krůta
jsou	být	k5eAaImIp3nP	být
uváděny	uváděn	k2eAgFnPc1d1	uváděna
muchničky	muchnička	k1gFnPc1	muchnička
<g/>
.	.	kIx.	.
</s>
<s>
Průběh	průběh	k1gInSc1	průběh
chlamydiózy	chlamydióza	k1gFnSc2	chlamydióza
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
ptáků	pták	k1gMnPc2	pták
i	i	k9	i
mezi	mezi	k7c7	mezi
druhy	druh	k1gInPc7	druh
domácími	domácí	k2eAgInPc7d1	domácí
drůbeže	drůbež	k1gFnPc4	drůbež
<g/>
.	.	kIx.	.
</s>
<s>
Časté	častý	k2eAgFnPc1d1	častá
jsou	být	k5eAaImIp3nP	být
latentní	latentní	k2eAgFnPc1d1	latentní
infekce	infekce	k1gFnPc1	infekce
<g/>
,	,	kIx,	,
k	k	k7c3	k
jejichž	jejichž	k3xOyRp3gFnSc3	jejichž
aktivaci	aktivace	k1gFnSc3	aktivace
dochází	docházet	k5eAaImIp3nS	docházet
působením	působení	k1gNnSc7	působení
stresorů	stresor	k1gInPc2	stresor
(	(	kIx(	(
<g/>
interkurentní	interkurentní	k2eAgFnSc1d1	interkurentní
infekce	infekce	k1gFnSc1	infekce
<g/>
,	,	kIx,	,
špatná	špatný	k2eAgFnSc1d1	špatná
zoohygiena	zoohygiena	k1gFnSc1	zoohygiena
<g/>
,	,	kIx,	,
transport	transport	k1gInSc1	transport
<g/>
,	,	kIx,	,
nekvalitní	kvalitní	k2eNgFnSc1d1	nekvalitní
výživa	výživa	k1gFnSc1	výživa
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chlamydie	chlamydie	k1gFnPc1	chlamydie
mají	mít	k5eAaImIp3nP	mít
tendenci	tendence	k1gFnSc4	tendence
k	k	k7c3	k
intracelulárnímu	intracelulární	k2eAgNnSc3d1	intracelulární
přetrvávání	přetrvávání	k1gNnSc3	přetrvávání
v	v	k7c6	v
organismu	organismus	k1gInSc6	organismus
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
k	k	k7c3	k
dlouhodobému	dlouhodobý	k2eAgNnSc3d1	dlouhodobé
nosičství	nosičství	k1gNnSc3	nosičství
<g/>
.	.	kIx.	.
</s>
<s>
Multiplikace	multiplikace	k1gFnSc1	multiplikace
chlamydií	chlamydie	k1gFnPc2	chlamydie
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
vaskulární	vaskulární	k2eAgNnSc1d1	vaskulární
poškození	poškození	k1gNnSc1	poškození
a	a	k8xC	a
fibrinózní	fibrinózní	k2eAgInSc1d1	fibrinózní
zánět	zánět	k1gInSc1	zánět
seróz	seróza	k1gFnPc2	seróza
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
orgánů	orgán	k1gInPc2	orgán
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
exacerbaci	exacerbace	k1gFnSc3	exacerbace
onemocnění	onemocnění	k1gNnSc2	onemocnění
pak	pak	k6eAd1	pak
dochází	docházet	k5eAaImIp3nS	docházet
působením	působení	k1gNnSc7	působení
stresorů	stresor	k1gInPc2	stresor
<g/>
.	.	kIx.	.
</s>
<s>
Inkubační	inkubační	k2eAgFnSc1d1	inkubační
doba	doba	k1gFnSc1	doba
(	(	kIx(	(
<g/>
ID	Ida	k1gFnPc2	Ida
<g/>
)	)	kIx)	)
u	u	k7c2	u
přirozeně	přirozeně	k6eAd1	přirozeně
infikovaných	infikovaný	k2eAgMnPc2d1	infikovaný
ptáků	pták	k1gMnPc2	pták
kolísá	kolísat	k5eAaImIp3nS	kolísat
<g/>
.	.	kIx.	.
</s>
<s>
Záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c4	na
množství	množství	k1gNnSc4	množství
inhalovaných	inhalovaný	k2eAgFnPc2d1	Inhalovaná
chlamydií	chlamydie	k1gFnPc2	chlamydie
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
virulenci	virulence	k1gFnSc4	virulence
a	a	k8xC	a
na	na	k7c6	na
druhu	druh	k1gInSc6	druh
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
<s>
Nižší	nízký	k2eAgFnPc1d2	nižší
dávky	dávka	k1gFnPc1	dávka
nebo	nebo	k8xC	nebo
infekce	infekce	k1gFnPc1	infekce
starších	starý	k2eAgMnPc2d2	starší
ptáků	pták	k1gMnPc2	pták
prodlužují	prodlužovat	k5eAaImIp3nP	prodlužovat
inkubační	inkubační	k2eAgFnSc4d1	inkubační
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
40	[number]	k4	40
a	a	k8xC	a
více	hodně	k6eAd2	hodně
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
holubů	holub	k1gMnPc2	holub
není	být	k5eNaImIp3nS	být
ID	Ida	k1gFnPc2	Ida
přesně	přesně	k6eAd1	přesně
známa	známo	k1gNnSc2	známo
<g/>
,	,	kIx,	,
udává	udávat	k5eAaImIp3nS	udávat
se	s	k7c7	s
5-15	[number]	k4	5-15
dnů	den	k1gInPc2	den
i	i	k9	i
více	hodně	k6eAd2	hodně
<g/>
.	.	kIx.	.
</s>
<s>
Infekce	infekce	k1gFnPc1	infekce
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
přetrvávají	přetrvávat	k5eAaImIp3nP	přetrvávat
v	v	k7c6	v
cyklu	cyklus	k1gInSc6	cyklus
rodiče	rodič	k1gMnPc1	rodič
-	-	kIx~	-
holoubata	holoubě	k1gNnPc4	holoubě
<g/>
.	.	kIx.	.
</s>
<s>
Poštovní	poštovní	k2eAgMnPc1d1	poštovní
holubi	holub	k1gMnPc1	holub
onemocní	onemocnět	k5eAaPmIp3nP	onemocnět
zpravidla	zpravidla	k6eAd1	zpravidla
za	za	k7c4	za
2-4	[number]	k4	2-4
týdny	týden	k1gInPc4	týden
po	po	k7c6	po
závodech	závod	k1gInPc6	závod
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
a	a	k8xC	a
průběh	průběh	k1gInSc4	průběh
onemocnění	onemocnění	k1gNnSc2	onemocnění
závisejí	záviset	k5eAaImIp3nP	záviset
na	na	k7c4	na
virulenci	virulence	k1gFnSc4	virulence
původce	původce	k1gMnSc2	původce
a	a	k8xC	a
druhu	druh	k1gInSc2	druh
hostitele	hostitel	k1gMnSc2	hostitel
<g/>
.	.	kIx.	.
</s>
<s>
Nemoc	nemoc	k1gFnSc1	nemoc
může	moct	k5eAaImIp3nS	moct
probíhat	probíhat	k5eAaImF	probíhat
akutně	akutně	k6eAd1	akutně
<g/>
,	,	kIx,	,
chronicky	chronicky	k6eAd1	chronicky
nebo	nebo	k8xC	nebo
latentně	latentně	k6eAd1	latentně
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecné	všeobecný	k2eAgInPc1d1	všeobecný
příznaky	příznak	k1gInPc1	příznak
jsou	být	k5eAaImIp3nP	být
nechutenství	nechutenství	k1gNnSc4	nechutenství
<g/>
,	,	kIx,	,
ospalost	ospalost	k1gFnSc1	ospalost
<g/>
,	,	kIx,	,
slabost	slabost	k1gFnSc1	slabost
<g/>
,	,	kIx,	,
průjem	průjem	k1gInSc1	průjem
<g/>
,	,	kIx,	,
zježené	zježený	k2eAgNnSc1d1	zježené
peří	peří	k1gNnSc1	peří
a	a	k8xC	a
pomalé	pomalý	k2eAgNnSc1d1	pomalé
hubnutí	hubnutí	k1gNnSc1	hubnutí
<g/>
.	.	kIx.	.
</s>
<s>
Peří	peřit	k5eAaImIp3nS	peřit
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
kloaky	kloaka	k1gFnSc2	kloaka
bývá	bývat	k5eAaImIp3nS	bývat
znečištěné	znečištěný	k2eAgNnSc1d1	znečištěné
a	a	k8xC	a
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
dlouhotrvajících	dlouhotrvající	k2eAgInPc2d1	dlouhotrvající
průjmů	průjem	k1gInPc2	průjem
vypadané	vypadaný	k2eAgNnSc1d1	vypadané
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
očí	oko	k1gNnPc2	oko
a	a	k8xC	a
nosních	nosní	k2eAgInPc2d1	nosní
otvorů	otvor	k1gInPc2	otvor
vytéká	vytékat	k5eAaImIp3nS	vytékat
sekret	sekret	k1gInSc1	sekret
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgInSc2	který
se	se	k3xPyFc4	se
tvoří	tvořit	k5eAaImIp3nS	tvořit
příškvary	příškvara	k1gFnSc2	příškvara
a	a	k8xC	a
z	z	k7c2	z
okolí	okolí	k1gNnSc2	okolí
vypadává	vypadávat	k5eAaImIp3nS	vypadávat
drobné	drobný	k2eAgNnSc4d1	drobné
peří	peří	k1gNnSc4	peří
<g/>
.	.	kIx.	.
</s>
<s>
Dech	dech	k1gInSc1	dech
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zrychlený	zrychlený	k2eAgInSc4d1	zrychlený
<g/>
,	,	kIx,	,
svědčící	svědčící	k2eAgInSc4d1	svědčící
o	o	k7c6	o
rozvíjejícím	rozvíjející	k2eAgMnSc6d1	rozvíjející
se	se	k3xPyFc4	se
zánětu	zánět	k1gInSc3	zánět
plic	plíce	k1gFnPc2	plíce
(	(	kIx(	(
<g/>
pneumonie	pneumonie	k1gFnSc1	pneumonie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
latentním	latentní	k2eAgInSc6d1	latentní
průběhu	průběh	k1gInSc6	průběh
se	se	k3xPyFc4	se
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
pouze	pouze	k6eAd1	pouze
únava	únava	k1gFnSc1	únava
<g/>
,	,	kIx,	,
posedávání	posedávání	k1gNnSc1	posedávání
nebo	nebo	k8xC	nebo
načepýřené	načepýřený	k2eAgNnSc1d1	načepýřené
peří	peří	k1gNnSc1	peří
<g/>
,	,	kIx,	,
příp	příp	kA	příp
<g/>
.	.	kIx.	.
slabý	slabý	k2eAgInSc4d1	slabý
výtok	výtok	k1gInSc4	výtok
z	z	k7c2	z
nosu	nos	k1gInSc2	nos
anebo	anebo	k8xC	anebo
očí	oko	k1gNnPc2	oko
<g/>
.	.	kIx.	.
</s>
<s>
Krůty	krůta	k1gFnPc1	krůta
<g/>
.	.	kIx.	.
</s>
<s>
Akutně	akutně	k6eAd1	akutně
postižení	postižený	k2eAgMnPc1d1	postižený
ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
neteční	netečný	k2eAgMnPc1d1	netečný
<g/>
,	,	kIx,	,
ospalí	ospalý	k2eAgMnPc1d1	ospalý
<g/>
,	,	kIx,	,
straní	stranit	k5eAaImIp3nP	stranit
se	se	k3xPyFc4	se
ostatních	ostatní	k2eAgFnPc2d1	ostatní
a	a	k8xC	a
nejeví	jevit	k5eNaImIp3nS	jevit
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
krmivo	krmivo	k1gNnSc4	krmivo
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
zvýšenou	zvýšený	k2eAgFnSc4d1	zvýšená
tělesnou	tělesný	k2eAgFnSc4d1	tělesná
teplotu	teplota	k1gFnSc4	teplota
<g/>
,	,	kIx,	,
žlutozelený	žlutozelený	k2eAgInSc4d1	žlutozelený
vazký	vazký	k2eAgInSc4d1	vazký
průjem	průjem	k1gInSc4	průjem
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
hubnou	hubnout	k5eAaImIp3nP	hubnout
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
nosních	nosní	k2eAgInPc2d1	nosní
otvorů	otvor	k1gInPc2	otvor
jim	on	k3xPp3gMnPc3	on
vytéká	vytékat	k5eAaImIp3nS	vytékat
serózní	serózní	k2eAgInSc4d1	serózní
až	až	k8xS	až
fibrinózní	fibrinózní	k2eAgInSc4d1	fibrinózní
odměšek	odměšek	k1gInSc4	odměšek
ztěžující	ztěžující	k2eAgNnSc1d1	ztěžující
dýchání	dýchání	k1gNnSc1	dýchání
<g/>
.	.	kIx.	.
</s>
<s>
Peří	peří	k1gNnSc1	peří
je	být	k5eAaImIp3nS	být
zježené	zježený	k2eAgNnSc1d1	zježené
<g/>
,	,	kIx,	,
neupravené	upravený	k2eNgNnSc1d1	neupravené
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
kloaky	kloaka	k1gFnSc2	kloaka
znečištěné	znečištěný	k2eAgInPc1d1	znečištěný
nebo	nebo	k8xC	nebo
i	i	k9	i
vypadané	vypadaný	k2eAgNnSc1d1	vypadané
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
nosnic	nosnice	k1gFnPc2	nosnice
rychle	rychle	k6eAd1	rychle
klesá	klesat	k5eAaImIp3nS	klesat
snáška	snáška	k1gFnSc1	snáška
na	na	k7c4	na
10-20	[number]	k4	10-20
%	%	kIx~	%
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
úplně	úplně	k6eAd1	úplně
zastavuje	zastavovat	k5eAaImIp3nS	zastavovat
<g/>
.	.	kIx.	.
</s>
<s>
Morbidita	morbidita	k1gFnSc1	morbidita
může	moct	k5eAaImIp3nS	moct
dosahovat	dosahovat	k5eAaImF	dosahovat
50-80	[number]	k4	50-80
%	%	kIx~	%
a	a	k8xC	a
mortalita	mortalita	k1gFnSc1	mortalita
10-30	[number]	k4	10-30
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
vyšší	vysoký	k2eAgFnSc1d2	vyšší
mortalita	mortalita	k1gFnSc1	mortalita
krocanů	krocan	k1gMnPc2	krocan
než	než	k8xS	než
krůt	krůta	k1gFnPc2	krůta
<g/>
.	.	kIx.	.
</s>
<s>
Infekce	infekce	k1gFnSc1	infekce
slabě	slabě	k6eAd1	slabě
virulentními	virulentní	k2eAgInPc7d1	virulentní
kmeny	kmen	k1gInPc7	kmen
se	se	k3xPyFc4	se
projevují	projevovat	k5eAaImIp3nP	projevovat
mírnějšími	mírný	k2eAgInPc7d2	mírnější
příznaky	příznak	k1gInPc7	příznak
(	(	kIx(	(
<g/>
morbidita	morbidita	k1gFnSc1	morbidita
5-20	[number]	k4	5-20
%	%	kIx~	%
<g/>
,	,	kIx,	,
mortalita	mortalita	k1gFnSc1	mortalita
1-4	[number]	k4	1-4
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
také	také	k9	také
menší	malý	k2eAgFnSc7d2	menší
redukcí	redukce	k1gFnSc7	redukce
snášky	snáška	k1gFnSc2	snáška
<g/>
.	.	kIx.	.
</s>
<s>
Chronický	chronický	k2eAgInSc1d1	chronický
průběh	průběh	k1gInSc1	průběh
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
projevuje	projevovat	k5eAaImIp3nS	projevovat
netečností	netečnost	k1gFnSc7	netečnost
<g/>
,	,	kIx,	,
načepýřeným	načepýřený	k2eAgNnSc7d1	načepýřené
peřím	peří	k1gNnSc7	peří
<g/>
,	,	kIx,	,
hubnutím	hubnutí	k1gNnSc7	hubnutí
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
slabým	slabý	k2eAgInSc7d1	slabý
výtokem	výtok	k1gInSc7	výtok
z	z	k7c2	z
nosu	nos	k1gInSc2	nos
a	a	k8xC	a
očí	oko	k1gNnPc2	oko
anebo	anebo	k8xC	anebo
průjmem	průjem	k1gInSc7	průjem
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
dospělých	dospělý	k2eAgMnPc2d1	dospělý
ptáků	pták	k1gMnPc2	pták
jsou	být	k5eAaImIp3nP	být
časté	častý	k2eAgFnPc1d1	častá
latentní	latentní	k2eAgFnPc1d1	latentní
infekce	infekce	k1gFnPc1	infekce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ojedinělých	ojedinělý	k2eAgInPc6d1	ojedinělý
případech	případ	k1gInPc6	případ
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
pozorovat	pozorovat	k5eAaImF	pozorovat
zvýšenou	zvýšený	k2eAgFnSc4d1	zvýšená
nemocnost	nemocnost	k1gFnSc4	nemocnost
ošetřujícího	ošetřující	k2eAgInSc2d1	ošetřující
personálu	personál	k1gInSc2	personál
<g/>
.	.	kIx.	.
</s>
<s>
Kachny	kachna	k1gFnPc1	kachna
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kachňat	kachně	k1gNnPc2	kachně
probíhá	probíhat	k5eAaImIp3nS	probíhat
chlamydióza	chlamydióza	k1gFnSc1	chlamydióza
často	často	k6eAd1	často
fatálně	fatálně	k6eAd1	fatálně
(	(	kIx(	(
<g/>
smrtelně	smrtelně	k6eAd1	smrtelně
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
u	u	k7c2	u
dospělých	dospělí	k1gMnPc2	dospělí
spíše	spíše	k9	spíše
latentně	latentně	k6eAd1	latentně
(	(	kIx(	(
<g/>
skrytě	skrytě	k6eAd1	skrytě
<g/>
)	)	kIx)	)
s	s	k7c7	s
vylučováním	vylučování	k1gNnSc7	vylučování
chlamydií	chlamydie	k1gFnPc2	chlamydie
do	do	k7c2	do
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Klinické	klinický	k2eAgInPc1d1	klinický
příznaky	příznak	k1gInPc1	příznak
u	u	k7c2	u
kachen	kachna	k1gFnPc2	kachna
jsou	být	k5eAaImIp3nP	být
charakterizovány	charakterizovat	k5eAaBmNgInP	charakterizovat
serózním	serózní	k2eAgInPc3d1	serózní
až	až	k9	až
purulentním	purulentní	k2eAgInPc3d1	purulentní
(	(	kIx(	(
<g/>
hnisavý	hnisavý	k2eAgMnSc1d1	hnisavý
<g/>
)	)	kIx)	)
výtokem	výtok	k1gInSc7	výtok
z	z	k7c2	z
očí	oko	k1gNnPc2	oko
a	a	k8xC	a
nosu	nos	k1gInSc2	nos
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
slepení	slepení	k1gNnSc4	slepení
a	a	k8xC	a
zašpinění	zašpinění	k1gNnSc4	zašpinění
peří	peří	k1gNnSc2	peří
na	na	k7c6	na
obličeji	obličej	k1gInSc6	obličej
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
vypadávání	vypadávání	k1gNnSc4	vypadávání
<g/>
.	.	kIx.	.
</s>
<s>
Zvířata	zvíře	k1gNnPc1	zvíře
nemají	mít	k5eNaImIp3nP	mít
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
krmivo	krmivo	k1gNnSc4	krmivo
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
zelený	zelený	k2eAgInSc4d1	zelený
vodnatý	vodnatý	k2eAgInSc4d1	vodnatý
průjem	průjem	k1gInSc4	průjem
<g/>
,	,	kIx,	,
pohybové	pohybový	k2eAgFnPc4d1	pohybová
poruchy	porucha	k1gFnPc4	porucha
<g/>
,	,	kIx,	,
zaostávají	zaostávat	k5eAaImIp3nP	zaostávat
v	v	k7c6	v
růstu	růst	k1gInSc6	růst
a	a	k8xC	a
postupně	postupně	k6eAd1	postupně
hubnou	hubnout	k5eAaImIp3nP	hubnout
a	a	k8xC	a
hynou	hynout	k5eAaImIp3nP	hynout
v	v	k7c6	v
křečích	křeč	k1gFnPc6	křeč
<g/>
.	.	kIx.	.
</s>
<s>
Morbidita	morbidita	k1gFnSc1	morbidita
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
širokém	široký	k2eAgNnSc6d1	široké
rozmezí	rozmezí	k1gNnSc6	rozmezí
(	(	kIx(	(
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
i	i	k9	i
mortalita	mortalita	k1gFnSc1	mortalita
(	(	kIx(	(
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
30	[number]	k4	30
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
závislosti	závislost	k1gFnSc6	závislost
na	na	k7c6	na
věku	věk	k1gInSc6	věk
i	i	k8xC	i
případné	případný	k2eAgFnSc3d1	případná
současné	současný	k2eAgFnSc3d1	současná
infekci	infekce	k1gFnSc3	infekce
salmonelami	salmonela	k1gFnPc7	salmonela
<g/>
.	.	kIx.	.
</s>
<s>
Holubi	holub	k1gMnPc1	holub
<g/>
.	.	kIx.	.
</s>
<s>
Klinicky	klinicky	k6eAd1	klinicky
nejčastěji	často	k6eAd3	často
onemocní	onemocnět	k5eAaPmIp3nP	onemocnět
mladí	mladý	k2eAgMnPc1d1	mladý
holubi	holub	k1gMnPc1	holub
do	do	k7c2	do
6	[number]	k4	6
měsíců	měsíc	k1gInPc2	měsíc
věku	věk	k1gInSc2	věk
<g/>
;	;	kIx,	;
u	u	k7c2	u
starších	starý	k2eAgMnPc2d2	starší
holubů	holub	k1gMnPc2	holub
jsou	být	k5eAaImIp3nP	být
spíše	spíše	k9	spíše
chronické	chronický	k2eAgFnPc1d1	chronická
nebo	nebo	k8xC	nebo
latentní	latentní	k2eAgFnPc1d1	latentní
infekce	infekce	k1gFnPc1	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Sérologicky	sérologicky	k6eAd1	sérologicky
se	se	k3xPyFc4	se
promořenost	promořenost	k1gFnSc1	promořenost
holubů	holub	k1gMnPc2	holub
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
na	na	k7c4	na
20-90	[number]	k4	20-90
%	%	kIx~	%
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
asi	asi	k9	asi
10-30	[number]	k4	10-30
%	%	kIx~	%
činí	činit	k5eAaImIp3nS	činit
aktivní	aktivní	k2eAgFnSc1d1	aktivní
infekce	infekce	k1gFnSc1	infekce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chovech	chov	k1gInPc6	chov
holubů	holub	k1gMnPc2	holub
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
infekce	infekce	k1gFnSc2	infekce
opakovaným	opakovaný	k2eAgInSc7d1	opakovaný
přenosem	přenos	k1gInSc7	přenos
z	z	k7c2	z
rodičů	rodič	k1gMnPc2	rodič
na	na	k7c4	na
holoubata	holoubě	k1gNnPc4	holoubě
<g/>
.	.	kIx.	.
</s>
<s>
Příznaky	příznak	k1gInPc1	příznak
nemoci	nemoc	k1gFnSc2	nemoc
jsou	být	k5eAaImIp3nP	být
variabilní	variabilní	k2eAgFnPc1d1	variabilní
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
akutním	akutní	k2eAgNnSc6d1	akutní
onemocnění	onemocnění	k1gNnSc6	onemocnění
se	se	k3xPyFc4	se
pozoruje	pozorovat	k5eAaImIp3nS	pozorovat
nechutenství	nechutenství	k1gNnSc1	nechutenství
<g/>
,	,	kIx,	,
neupravené	upravený	k2eNgNnSc1d1	neupravené
peří	peří	k1gNnSc1	peří
<g/>
,	,	kIx,	,
zaostávání	zaostávání	k1gNnSc1	zaostávání
v	v	k7c6	v
růstu	růst	k1gInSc6	růst
a	a	k8xC	a
průjem	průjem	k1gInSc1	průjem
<g/>
.	.	kIx.	.
</s>
<s>
Holubi	holub	k1gMnPc1	holub
mají	mít	k5eAaImIp3nP	mít
zduřelá	zduřelý	k2eAgNnPc4d1	zduřelé
víčka	víčko	k1gNnPc4	víčko
<g/>
,	,	kIx,	,
jedno-	jedno-	k?	jedno-
nebo	nebo	k8xC	nebo
oboustrannou	oboustranný	k2eAgFnSc4d1	oboustranná
konjunktivitidu	konjunktivitida	k1gFnSc4	konjunktivitida
a	a	k8xC	a
rhinitidu	rhinitis	k1gFnSc4	rhinitis
se	s	k7c7	s
serózním	serózní	k2eAgInSc7d1	serózní
až	až	k8xS	až
purulentním	purulentní	k2eAgInSc7d1	purulentní
exsudátem	exsudát	k1gInSc7	exsudát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
slepuje	slepovat	k5eAaImIp3nS	slepovat
peří	peří	k1gNnSc4	peří
kolem	kolem	k7c2	kolem
očí	oko	k1gNnPc2	oko
a	a	k8xC	a
nozder	nozdra	k1gFnPc2	nozdra
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
potřásají	potřásat	k5eAaImIp3nP	potřásat
hlavou	hlava	k1gFnSc7	hlava
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
uvolnit	uvolnit	k5eAaPmF	uvolnit
dýchací	dýchací	k2eAgInPc4d1	dýchací
otvory	otvor	k1gInPc4	otvor
<g/>
.	.	kIx.	.
</s>
<s>
Klinicky	klinicky	k6eAd1	klinicky
se	se	k3xPyFc4	se
zjišťují	zjišťovat	k5eAaImIp3nP	zjišťovat
chrapoty	chrapot	k1gInPc1	chrapot
při	při	k7c6	při
dýchání	dýchání	k1gNnSc6	dýchání
<g/>
.	.	kIx.	.
</s>
<s>
Holubi	holub	k1gMnPc1	holub
neradi	nerad	k2eAgMnPc1d1	nerad
létají	létat	k5eAaImIp3nP	létat
<g/>
,	,	kIx,	,
posedávají	posedávat	k5eAaImIp3nP	posedávat
a	a	k8xC	a
straní	stranit	k5eAaImIp3nP	stranit
se	se	k3xPyFc4	se
ostatních	ostatní	k2eAgInPc2d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Trpí	trpět	k5eAaImIp3nS	trpět
fotofobií	fotofobie	k1gFnPc2	fotofobie
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
slábnou	slábnout	k5eAaImIp3nP	slábnout
<g/>
,	,	kIx,	,
hubnou	hubnout	k5eAaImIp3nP	hubnout
a	a	k8xC	a
do	do	k7c2	do
2-3	[number]	k4	2-3
týdnů	týden	k1gInPc2	týden
mohou	moct	k5eAaImIp3nP	moct
uhynout	uhynout	k5eAaPmF	uhynout
<g/>
.	.	kIx.	.
</s>
<s>
Průběh	průběh	k1gInSc1	průběh
nemoci	nemoc	k1gFnSc2	nemoc
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
komplikován	komplikován	k2eAgInSc1d1	komplikován
infekcí	infekce	k1gFnPc2	infekce
salmonelami	salmonela	k1gFnPc7	salmonela
anebo	anebo	k8xC	anebo
trichomonádami	trichomonáda	k1gFnPc7	trichomonáda
<g/>
.	.	kIx.	.
</s>
<s>
Exotičtí	exotický	k2eAgMnPc1d1	exotický
ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
neteční	tečný	k2eNgMnPc1d1	tečný
<g/>
,	,	kIx,	,
ospalí	ospalý	k2eAgMnPc1d1	ospalý
<g/>
,	,	kIx,	,
s	s	k7c7	s
načepýřeným	načepýřený	k2eAgNnSc7d1	načepýřené
peřím	peří	k1gNnSc7	peří
<g/>
,	,	kIx,	,
nemají	mít	k5eNaImIp3nP	mít
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
krmivo	krmivo	k1gNnSc4	krmivo
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
hubnou	hubnout	k5eAaImIp3nP	hubnout
<g/>
.	.	kIx.	.
</s>
<s>
Trus	trus	k1gInSc1	trus
šedozelené	šedozelený	k2eAgFnSc2d1	šedozelená
barvy	barva	k1gFnSc2	barva
a	a	k8xC	a
želatinózní	želatinózní	k2eAgFnSc2d1	želatinózní
konzistence	konzistence	k1gFnSc2	konzistence
přechází	přecházet	k5eAaImIp3nS	přecházet
v	v	k7c4	v
průjem	průjem	k1gInSc4	průjem
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
s	s	k7c7	s
příměsí	příměs	k1gFnSc7	příměs
krve	krev	k1gFnSc2	krev
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
těžce	těžce	k6eAd1	těžce
dýchají	dýchat	k5eAaImIp3nP	dýchat
s	s	k7c7	s
otevřeným	otevřený	k2eAgInSc7d1	otevřený
zobákem	zobák	k1gInSc7	zobák
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
horečku	horečka	k1gFnSc4	horečka
a	a	k8xC	a
zastavují	zastavovat	k5eAaImIp3nP	zastavovat
případnou	případný	k2eAgFnSc4d1	případná
snášku	snáška	k1gFnSc4	snáška
<g/>
.	.	kIx.	.
</s>
<s>
Objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
i	i	k9	i
nervové	nervový	k2eAgInPc1d1	nervový
příznaky	příznak	k1gInPc1	příznak
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
před	před	k7c7	před
úhynem	úhyn	k1gInSc7	úhyn
nebo	nebo	k8xC	nebo
i	i	k9	i
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
nejsou	být	k5eNaImIp3nP	být
schopni	schopen	k2eAgMnPc1d1	schopen
udržet	udržet	k5eAaPmF	udržet
se	se	k3xPyFc4	se
na	na	k7c6	na
bidýlku	bidýlko	k1gNnSc6	bidýlko
<g/>
.	.	kIx.	.
</s>
<s>
Onemocnění	onemocnění	k1gNnSc1	onemocnění
může	moct	k5eAaImIp3nS	moct
trvat	trvat	k5eAaImF	trvat
několik	několik	k4yIc1	několik
dnů	den	k1gInPc2	den
až	až	k9	až
týdnů	týden	k1gInPc2	týden
a	a	k8xC	a
obvykle	obvykle	k6eAd1	obvykle
končí	končit	k5eAaImIp3nS	končit
úhynem	úhyn	k1gInSc7	úhyn
<g/>
.	.	kIx.	.
</s>
<s>
Případní	případný	k2eAgMnPc1d1	případný
rekonvalescenti	rekonvalescent	k1gMnPc1	rekonvalescent
se	se	k3xPyFc4	se
jen	jen	k9	jen
pomalu	pomalu	k6eAd1	pomalu
zotavují	zotavovat	k5eAaImIp3nP	zotavovat
<g/>
.	.	kIx.	.
</s>
<s>
Kur	kur	k1gMnSc1	kur
domácí	domácí	k1gMnSc1	domácí
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
odolný	odolný	k2eAgInSc1d1	odolný
na	na	k7c4	na
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
infekci	infekce	k1gFnSc4	infekce
C.	C.	kA	C.
psittaci	psittace	k1gFnSc4	psittace
<g/>
.	.	kIx.	.
</s>
<s>
Výskyt	výskyt	k1gInSc1	výskyt
akutního	akutní	k2eAgNnSc2d1	akutní
onemocnění	onemocnění	k1gNnSc2	onemocnění
s	s	k7c7	s
následnou	následný	k2eAgFnSc7d1	následná
mortalitou	mortalita	k1gFnSc7	mortalita
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
vzácný	vzácný	k2eAgMnSc1d1	vzácný
i	i	k8xC	i
u	u	k7c2	u
mladých	mladý	k2eAgNnPc2d1	mladé
kuřat	kuře	k1gNnPc2	kuře
<g/>
.	.	kIx.	.
</s>
<s>
Většinou	většina	k1gFnSc7	většina
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
bezsymptomní	bezsymptomní	k2eAgFnPc4d1	bezsymptomní
latentní	latentní	k2eAgFnPc4d1	latentní
infekce	infekce	k1gFnPc4	infekce
s	s	k7c7	s
nízkou	nízký	k2eAgFnSc7d1	nízká
incidencí	incidence	k1gFnSc7	incidence
<g/>
,	,	kIx,	,
přechodného	přechodný	k2eAgNnSc2d1	přechodné
trvání	trvání	k1gNnSc2	trvání
a	a	k8xC	a
prokazatelné	prokazatelný	k2eAgFnPc1d1	prokazatelná
pouze	pouze	k6eAd1	pouze
sérologicky	sérologicky	k6eAd1	sérologicky
<g/>
.	.	kIx.	.
</s>
<s>
Husy	husa	k1gFnPc1	husa
<g/>
.	.	kIx.	.
</s>
<s>
Infekce	infekce	k1gFnSc1	infekce
hus	husa	k1gFnPc2	husa
jsou	být	k5eAaImIp3nP	být
zaznamenávány	zaznamenávat	k5eAaImNgInP	zaznamenávat
zpravidla	zpravidla	k6eAd1	zpravidla
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
onemocněním	onemocnění	k1gNnSc7	onemocnění
kachen	kachna	k1gFnPc2	kachna
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
objevovat	objevovat	k5eAaImF	objevovat
sporadicky	sporadicky	k6eAd1	sporadicky
u	u	k7c2	u
mladých	mladý	k2eAgNnPc2d1	mladé
zvířat	zvíře	k1gNnPc2	zvíře
do	do	k7c2	do
6	[number]	k4	6
měsíců	měsíc	k1gInPc2	měsíc
věku	věk	k1gInSc2	věk
<g/>
,	,	kIx,	,
u	u	k7c2	u
starších	starší	k1gMnPc2	starší
mají	mít	k5eAaImIp3nP	mít
charakter	charakter	k1gInSc4	charakter
latentní	latentní	k2eAgFnSc2d1	latentní
infekce	infekce	k1gFnSc2	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Průběh	průběh	k1gInSc1	průběh
nemoci	nemoc	k1gFnSc2	nemoc
je	být	k5eAaImIp3nS	být
podobný	podobný	k2eAgInSc1d1	podobný
jako	jako	k8xS	jako
u	u	k7c2	u
kachen	kachna	k1gFnPc2	kachna
<g/>
.	.	kIx.	.
</s>
<s>
Bažanti	bažant	k1gMnPc1	bažant
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
ojedinělých	ojedinělý	k2eAgInPc2d1	ojedinělý
výskytů	výskyt	k1gInPc2	výskyt
chlamydiózy	chlamydióza	k1gFnSc2	chlamydióza
<g/>
,	,	kIx,	,
projevujících	projevující	k2eAgFnPc2d1	projevující
se	s	k7c7	s
příznaky	příznak	k1gInPc7	příznak
zánětu	zánět	k1gInSc2	zánět
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgInP	být
izolovány	izolovat	k5eAaBmNgInP	izolovat
nízko	nízko	k6eAd1	nízko
virulentní	virulentní	k2eAgInPc1d1	virulentní
kmeny	kmen	k1gInPc1	kmen
chlamydií	chlamydie	k1gFnPc2	chlamydie
<g/>
.	.	kIx.	.
</s>
<s>
Sérologické	sérologický	k2eAgInPc1d1	sérologický
průzkumy	průzkum	k1gInPc1	průzkum
divokých	divoký	k2eAgMnPc2d1	divoký
bažantů	bažant	k1gMnPc2	bažant
i	i	k8xC	i
intenzivních	intenzivní	k2eAgInPc2d1	intenzivní
chovů	chov	k1gInPc2	chov
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
poměrně	poměrně	k6eAd1	poměrně
malou	malý	k2eAgFnSc4d1	malá
promořenost	promořenost	k1gFnSc4	promořenost
<g/>
.	.	kIx.	.
</s>
<s>
Pštrosi	pštros	k1gMnPc1	pštros
<g/>
.	.	kIx.	.
</s>
<s>
Klinická	klinický	k2eAgFnSc1d1	klinická
chlamydióza	chlamydióza	k1gFnSc1	chlamydióza
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
u	u	k7c2	u
mladých	mladý	k2eAgNnPc2d1	mladé
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
mohou	moct	k5eAaImIp3nP	moct
vykazovat	vykazovat	k5eAaImF	vykazovat
depresi	deprese	k1gFnSc4	deprese
<g/>
,	,	kIx,	,
dyspnoe	dyspnoe	k1gFnPc4	dyspnoe
(	(	kIx(	(
<g/>
dýchací	dýchací	k2eAgFnPc4d1	dýchací
potíže	potíž	k1gFnPc4	potíž
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
konjunktivitidu	konjunktivitida	k1gFnSc4	konjunktivitida
<g/>
,	,	kIx,	,
pinguinní	pinguinný	k2eAgMnPc1d1	pinguinný
(	(	kIx(	(
<g/>
tučňákovský	tučňákovský	k2eAgInSc4d1	tučňákovský
<g/>
)	)	kIx)	)
postoj	postoj	k1gInSc4	postoj
i	i	k8xC	i
chůzi	chůze	k1gFnSc4	chůze
a	a	k8xC	a
náhlý	náhlý	k2eAgInSc4d1	náhlý
úhyn	úhyn	k1gInSc4	úhyn
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pitvě	pitva	k1gFnSc6	pitva
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
nekrotický	nekrotický	k2eAgInSc1d1	nekrotický
zánět	zánět	k1gInSc1	zánět
jater	játra	k1gNnPc2	játra
a	a	k8xC	a
sleziny	slezina	k1gFnSc2	slezina
<g/>
,	,	kIx,	,
fibrinopurulentní	fibrinopurulentní	k2eAgFnSc1d1	fibrinopurulentní
tracheitida	tracheitida	k1gFnSc1	tracheitida
<g/>
,	,	kIx,	,
pneumonie	pneumonie	k1gFnSc1	pneumonie
<g/>
,	,	kIx,	,
perikarditida	perikarditida	k1gFnSc1	perikarditida
a	a	k8xC	a
perihepatitida	perihepatitida	k1gFnSc1	perihepatitida
<g/>
.	.	kIx.	.
</s>
<s>
Mikroskopicky	mikroskopicky	k6eAd1	mikroskopicky
lze	lze	k6eAd1	lze
prokázat	prokázat	k5eAaPmF	prokázat
chlamydie	chlamydie	k1gFnPc4	chlamydie
<g/>
.	.	kIx.	.
</s>
<s>
Chlamydióza	Chlamydióza	k1gFnSc1	Chlamydióza
je	být	k5eAaImIp3nS	být
obecně	obecně	k6eAd1	obecně
charakterizována	charakterizovat	k5eAaBmNgFnS	charakterizovat
zánětem	zánět	k1gInSc7	zánět
serózních	serózní	k2eAgFnPc2d1	serózní
blan	blána	k1gFnPc2	blána
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
rekonvalescentů	rekonvalescent	k1gMnPc2	rekonvalescent
a	a	k8xC	a
latentních	latentní	k2eAgMnPc2d1	latentní
nosičů	nosič	k1gMnPc2	nosič
chlamydií	chlamydie	k1gFnSc7	chlamydie
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
změny	změna	k1gFnPc1	změna
minimální	minimální	k2eAgFnPc1d1	minimální
nebo	nebo	k8xC	nebo
nemusí	muset	k5eNaImIp3nP	muset
být	být	k5eAaImF	být
vůbec	vůbec	k9	vůbec
vyvinuty	vyvinout	k5eAaPmNgFnP	vyvinout
<g/>
.	.	kIx.	.
</s>
<s>
Charakter	charakter	k1gInSc1	charakter
patologických	patologický	k2eAgFnPc2d1	patologická
změn	změna	k1gFnPc2	změna
není	být	k5eNaImIp3nS	být
závislý	závislý	k2eAgInSc1d1	závislý
na	na	k7c4	na
virulenci	virulence	k1gFnSc4	virulence
infikujících	infikující	k2eAgFnPc2d1	infikující
chlamydií	chlamydie	k1gFnPc2	chlamydie
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
jejich	jejich	k3xOp3gFnSc1	jejich
intenzita	intenzita	k1gFnSc1	intenzita
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
infekci	infekce	k1gFnSc6	infekce
méně	málo	k6eAd2	málo
virulentními	virulentní	k2eAgInPc7d1	virulentní
kmeny	kmen	k1gInPc7	kmen
menší	malý	k2eAgFnSc2d2	menší
<g/>
.	.	kIx.	.
</s>
<s>
Akutní	akutní	k2eAgInSc1d1	akutní
průběh	průběh	k1gInSc1	průběh
nemoci	nemoc	k1gFnSc2	nemoc
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
doprovázen	doprovázet	k5eAaImNgInS	doprovázet
serózní	serózní	k2eAgInSc1d1	serózní
<g/>
,	,	kIx,	,
sérofibrinózní	sérofibrinózní	k2eAgInSc1d1	sérofibrinózní
nebo	nebo	k8xC	nebo
až	až	k9	až
séropurulentní	séropurulentní	k2eAgFnSc7d1	séropurulentní
perikarditidou	perikarditida	k1gFnSc7	perikarditida
<g/>
,	,	kIx,	,
pneumonií	pneumonie	k1gFnSc7	pneumonie
<g/>
,	,	kIx,	,
aerosakulitidou	aerosakulitida	k1gFnSc7	aerosakulitida
(	(	kIx(	(
<g/>
zánět	zánět	k1gInSc1	zánět
vzdušných	vzdušný	k2eAgInPc2d1	vzdušný
vaků	vak	k1gInPc2	vak
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
peritonitidou	peritonitida	k1gFnSc7	peritonitida
<g/>
,	,	kIx,	,
perihepatitidou	perihepatitida	k1gFnSc7	perihepatitida
(	(	kIx(	(
<g/>
zánět	zánět	k1gInSc1	zánět
jaterního	jaterní	k2eAgNnSc2d1	jaterní
pouzdra	pouzdro	k1gNnSc2	pouzdro
<g/>
)	)	kIx)	)
a	a	k8xC	a
katarální	katarální	k2eAgFnSc7d1	katarální
enteritidou	enteritida	k1gFnSc7	enteritida
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
infekci	infekce	k1gFnSc6	infekce
chlamydiemi	chlamydie	k1gFnPc7	chlamydie
vzniká	vznikat	k5eAaImIp3nS	vznikat
humorální	humorální	k2eAgFnSc1d1	humorální
i	i	k8xC	i
buněčná	buněčný	k2eAgFnSc1d1	buněčná
imunita	imunita	k1gFnSc1	imunita
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
však	však	k9	však
obecně	obecně	k6eAd1	obecně
slabá	slabý	k2eAgFnSc1d1	slabá
a	a	k8xC	a
krátkodobá	krátkodobý	k2eAgFnSc1d1	krátkodobá
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
pozorována	pozorovat	k5eAaImNgFnS	pozorovat
věková	věkový	k2eAgFnSc1d1	věková
i	i	k8xC	i
druhová	druhový	k2eAgFnSc1d1	druhová
rezistence	rezistence	k1gFnSc1	rezistence
k	k	k7c3	k
onemocnění	onemocnění	k1gNnSc3	onemocnění
<g/>
,	,	kIx,	,
nikoliv	nikoliv	k9	nikoliv
však	však	k9	však
k	k	k7c3	k
infekci	infekce	k1gFnSc3	infekce
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
holubi	holub	k1gMnPc1	holub
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
odolní	odolný	k2eAgMnPc1d1	odolný
k	k	k7c3	k
onemocnění	onemocnění	k1gNnSc3	onemocnění
i	i	k8xC	i
po	po	k7c6	po
infekci	infekce	k1gFnSc6	infekce
vysoce	vysoce	k6eAd1	vysoce
virulentními	virulentní	k2eAgInPc7d1	virulentní
kmeny	kmen	k1gInPc7	kmen
jiných	jiný	k2eAgInPc2d1	jiný
ptačích	ptačí	k2eAgInPc2d1	ptačí
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vysoce	vysoce	k6eAd1	vysoce
vnímaví	vnímavý	k2eAgMnPc1d1	vnímavý
na	na	k7c4	na
infekci	infekce	k1gFnSc4	infekce
izoláty	izolát	k1gInPc4	izolát
od	od	k7c2	od
holubů	holub	k1gMnPc2	holub
a	a	k8xC	a
vrabců	vrabec	k1gMnPc2	vrabec
<g/>
.	.	kIx.	.
</s>
<s>
Krůty	krůta	k1gFnPc1	krůta
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
slabě	slabě	k6eAd1	slabě
rezistentními	rezistentní	k2eAgInPc7d1	rezistentní
k	k	k7c3	k
onemocnění	onemocnění	k1gNnSc3	onemocnění
po	po	k7c4	po
15	[number]	k4	15
<g/>
.	.	kIx.	.
týdnu	týden	k1gInSc6	týden
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Rezistence	rezistence	k1gFnSc1	rezistence
a	a	k8xC	a
imunita	imunita	k1gFnSc1	imunita
u	u	k7c2	u
kachen	kachna	k1gFnPc2	kachna
není	být	k5eNaImIp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
známa	znám	k2eAgFnSc1d1	známa
<g/>
.	.	kIx.	.
</s>
<s>
Humorální	humorální	k2eAgFnSc1d1	humorální
imunita	imunita	k1gFnSc1	imunita
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
zjišťuje	zjišťovat	k5eAaImIp3nS	zjišťovat
reakcí	reakce	k1gFnSc7	reakce
vazby	vazba	k1gFnSc2	vazba
komplementu	komplement	k1gInSc2	komplement
(	(	kIx(	(
<g/>
detekce	detekce	k1gFnSc1	detekce
protilátek	protilátka	k1gFnPc2	protilátka
proti	proti	k7c3	proti
skupinovému	skupinový	k2eAgInSc3d1	skupinový
antigenu	antigen	k1gInSc2	antigen
chlamydií	chlamydie	k1gFnPc2	chlamydie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Protilátky	protilátka	k1gFnPc1	protilátka
vznikají	vznikat	k5eAaImIp3nP	vznikat
za	za	k7c4	za
7-10	[number]	k4	7-10
dní	den	k1gInPc2	den
po	po	k7c6	po
infekci	infekce	k1gFnSc6	infekce
(	(	kIx(	(
<g/>
PI	pi	k0	pi
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
maximálních	maximální	k2eAgInPc2d1	maximální
titrů	titr	k1gInPc2	titr
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
za	za	k7c4	za
30	[number]	k4	30
dní	den	k1gInPc2	den
PI	pi	k0	pi
<g/>
.	.	kIx.	.
</s>
<s>
Infekce	infekce	k1gFnSc1	infekce
chlamydiemi	chlamydie	k1gFnPc7	chlamydie
vede	vést	k5eAaImIp3nS	vést
také	také	k9	také
ke	k	k7c3	k
tvorbě	tvorba	k1gFnSc3	tvorba
hemaglutinačních	hemaglutinační	k2eAgFnPc2d1	hemaglutinační
a	a	k8xC	a
precipitačních	precipitační	k2eAgFnPc2d1	precipitační
protilátek	protilátka	k1gFnPc2	protilátka
<g/>
;	;	kIx,	;
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
míře	míra	k1gFnSc6	míra
i	i	k9	i
protilátek	protilátka	k1gFnPc2	protilátka
neutralizačních	neutralizační	k2eAgFnPc2d1	neutralizační
a	a	k8xC	a
ke	k	k7c3	k
stavu	stav	k1gInSc3	stav
hypersenzitivity	hypersenzitivita	k1gFnSc2	hypersenzitivita
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Předběžnou	předběžný	k2eAgFnSc4d1	předběžná
diagnózu	diagnóza	k1gFnSc4	diagnóza
chlamydiózy	chlamydióza	k1gFnSc2	chlamydióza
lze	lze	k6eAd1	lze
stanovit	stanovit	k5eAaPmF	stanovit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
anamnestických	anamnestický	k2eAgInPc2d1	anamnestický
údajů	údaj	k1gInPc2	údaj
<g/>
,	,	kIx,	,
epizootologického	epizootologický	k2eAgNnSc2d1	epizootologické
šetření	šetření	k1gNnSc2	šetření
<g/>
,	,	kIx,	,
klinického	klinický	k2eAgInSc2d1	klinický
a	a	k8xC	a
pitevního	pitevní	k2eAgInSc2d1	pitevní
nálezu	nález	k1gInSc2	nález
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
kolísající	kolísající	k2eAgFnSc3d1	kolísající
virulenci	virulence	k1gFnSc3	virulence
kmenů	kmen	k1gInPc2	kmen
C.	C.	kA	C.
psittaci	psittace	k1gFnSc6	psittace
jsou	být	k5eAaImIp3nP	být
projevy	projev	k1gInPc1	projev
nemoci	nemoc	k1gFnSc2	nemoc
velmi	velmi	k6eAd1	velmi
variabilní	variabilní	k2eAgInSc1d1	variabilní
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
pro	pro	k7c4	pro
konečnou	konečný	k2eAgFnSc4d1	konečná
diagnózu	diagnóza	k1gFnSc4	diagnóza
je	být	k5eAaImIp3nS	být
nezbytné	nezbytný	k2eAgNnSc1d1	nezbytný
laboratorní	laboratorní	k2eAgNnSc1d1	laboratorní
vyšetření	vyšetření	k1gNnSc1	vyšetření
<g/>
.	.	kIx.	.
</s>
<s>
Laboratorní	laboratorní	k2eAgFnSc1d1	laboratorní
diagnostika	diagnostika	k1gFnSc1	diagnostika
chlamydiových	chlamydiový	k2eAgFnPc2d1	chlamydiová
infekcí	infekce	k1gFnPc2	infekce
je	být	k5eAaImIp3nS	být
založena	založit	k5eAaPmNgFnS	založit
na	na	k7c6	na
přímém	přímý	k2eAgInSc6d1	přímý
mikroskopickém	mikroskopický	k2eAgInSc6d1	mikroskopický
průkazu	průkaz	k1gInSc6	průkaz
chlamydií	chlamydie	k1gFnPc2	chlamydie
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
postižených	postižený	k2eAgFnPc2d1	postižená
tkání	tkáň	k1gFnPc2	tkáň
<g/>
,	,	kIx,	,
na	na	k7c4	na
jejich	jejich	k3xOp3gFnSc4	jejich
izolaci	izolace	k1gFnSc4	izolace
ve	v	k7c6	v
vnímavých	vnímavý	k2eAgInPc6d1	vnímavý
substrátech	substrát	k1gInPc6	substrát
<g/>
,	,	kIx,	,
na	na	k7c6	na
sérologickém	sérologický	k2eAgNnSc6d1	sérologické
vyšetření	vyšetření	k1gNnSc6	vyšetření
a	a	k8xC	a
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
také	také	k9	také
na	na	k7c6	na
metodách	metoda	k1gFnPc6	metoda
imunochemických	imunochemický	k2eAgFnPc6d1	imunochemická
nebo	nebo	k8xC	nebo
molekulárně	molekulárně	k6eAd1	molekulárně
biologických	biologický	k2eAgInPc2d1	biologický
<g/>
.	.	kIx.	.
</s>
<s>
Diferenciální	diferenciální	k2eAgFnSc1d1	diferenciální
diagnostika	diagnostika	k1gFnSc1	diagnostika
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
krůt	krůta	k1gFnPc2	krůta
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
odlišit	odlišit	k5eAaPmF	odlišit
chlamydiózu	chlamydióza	k1gFnSc4	chlamydióza
zejména	zejména	k9	zejména
od	od	k7c2	od
pasteurelózy	pasteurelóza	k1gFnSc2	pasteurelóza
<g/>
,	,	kIx,	,
mykoplasmózy	mykoplasmóza	k1gFnPc1	mykoplasmóza
<g/>
,	,	kIx,	,
kolibacilózy	kolibacilóza	k1gFnPc1	kolibacilóza
a	a	k8xC	a
influenzy	influenza	k1gFnPc1	influenza
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
projevovat	projevovat	k5eAaImF	projevovat
podobnými	podobný	k2eAgInPc7d1	podobný
klinickými	klinický	k2eAgInPc7d1	klinický
i	i	k8xC	i
patologickými	patologický	k2eAgInPc7d1	patologický
nálezy	nález	k1gInPc7	nález
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
kachen	kachna	k1gFnPc2	kachna
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
odlišit	odlišit	k5eAaPmF	odlišit
salmonelózy	salmonelóza	k1gFnPc4	salmonelóza
<g/>
,	,	kIx,	,
influenzu	influenza	k1gFnSc4	influenza
a	a	k8xC	a
herpesvirovou	herpesvirový	k2eAgFnSc4d1	herpesvirový
enteritidu	enteritida	k1gFnSc4	enteritida
<g/>
,	,	kIx,	,
u	u	k7c2	u
holubů	holub	k1gMnPc2	holub
a	a	k8xC	a
exotů	exot	k1gMnPc2	exot
pak	pak	k6eAd1	pak
herpesvirové	herpesvirový	k2eAgFnSc2d1	herpesvirový
infekce	infekce	k1gFnSc2	infekce
a	a	k8xC	a
influenzu	influenza	k1gFnSc4	influenza
A.	A.	kA	A.
U	u	k7c2	u
domácí	domácí	k2eAgFnSc2d1	domácí
drůbeže	drůbež	k1gFnSc2	drůbež
se	se	k3xPyFc4	se
k	k	k7c3	k
terapii	terapie	k1gFnSc3	terapie
se	se	k3xPyFc4	se
nejčastěji	často	k6eAd3	často
používají	používat	k5eAaImIp3nP	používat
antibiotikum	antibiotikum	k1gNnSc1	antibiotikum
podávaná	podávaný	k2eAgFnSc1d1	podávaná
ve	v	k7c6	v
vysokých	vysoký	k2eAgFnPc6d1	vysoká
dávkách	dávka	k1gFnPc6	dávka
a	a	k8xC	a
po	po	k7c4	po
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Výsledek	výsledek	k1gInSc1	výsledek
léčby	léčba	k1gFnSc2	léčba
není	být	k5eNaImIp3nS	být
optimální	optimální	k2eAgMnSc1d1	optimální
<g/>
,	,	kIx,	,
ptáci	pták	k1gMnPc1	pták
mohou	moct	k5eAaImIp3nP	moct
i	i	k9	i
nadále	nadále	k6eAd1	nadále
vylučovat	vylučovat	k5eAaImF	vylučovat
chlamydie	chlamydie	k1gFnSc1	chlamydie
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
reinfikovat	reinfikovat	k5eAaBmF	reinfikovat
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
vhodnější	vhodný	k2eAgNnSc1d2	vhodnější
i	i	k9	i
z	z	k7c2	z
ekonomických	ekonomický	k2eAgInPc2d1	ekonomický
důvodů	důvod	k1gInPc2	důvod
po	po	k7c6	po
přeléčení	přeléčení	k1gNnSc6	přeléčení
drůbež	drůbež	k1gFnSc4	drůbež
odporazit	odporazit	k5eAaPmF	odporazit
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
holubů	holub	k1gMnPc2	holub
a	a	k8xC	a
exotických	exotický	k2eAgMnPc2d1	exotický
ptáků	pták	k1gMnPc2	pták
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
z	z	k7c2	z
těchto	tento	k3xDgNnPc2	tento
hledisek	hledisko	k1gNnPc2	hledisko
zvážit	zvážit	k5eAaPmF	zvážit
jejich	jejich	k3xOp3gFnSc4	jejich
chovatelskou	chovatelský	k2eAgFnSc4d1	chovatelská
hodnotu	hodnota	k1gFnSc4	hodnota
a	a	k8xC	a
místo	místo	k1gNnSc4	místo
terapie	terapie	k1gFnSc2	terapie
raději	rád	k6eAd2	rád
volit	volit	k5eAaImF	volit
neškodné	škodný	k2eNgNnSc4d1	neškodné
odstranění	odstranění	k1gNnSc4	odstranění
ptáků	pták	k1gMnPc2	pták
z	z	k7c2	z
chovu	chov	k1gInSc2	chov
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
preventivním	preventivní	k2eAgNnPc3d1	preventivní
opatřením	opatření	k1gNnPc3	opatření
patří	patřit	k5eAaImIp3nS	patřit
dodržování	dodržování	k1gNnSc4	dodržování
technologických	technologický	k2eAgInPc2d1	technologický
postupů	postup	k1gInPc2	postup
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
turnusového	turnusový	k2eAgInSc2d1	turnusový
způsobu	způsob	k1gInSc2	způsob
chovu	chov	k1gInSc2	chov
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zabránění	zabránění	k1gNnSc2	zabránění
volnému	volný	k2eAgInSc3d1	volný
přístupu	přístup	k1gInSc3	přístup
ptactva	ptactvo	k1gNnSc2	ptactvo
do	do	k7c2	do
výrobních	výrobní	k2eAgInPc2d1	výrobní
prostorů	prostor	k1gInPc2	prostor
<g/>
,	,	kIx,	,
dovoz	dovoz	k1gInSc1	dovoz
ptactva	ptactvo	k1gNnSc2	ptactvo
jen	jen	k9	jen
z	z	k7c2	z
prověřených	prověřený	k2eAgInPc2d1	prověřený
chovů	chov	k1gInPc2	chov
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
karanténování	karanténování	k1gNnSc4	karanténování
nejméně	málo	k6eAd3	málo
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
90	[number]	k4	90
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
dezinfekce	dezinfekce	k1gFnSc1	dezinfekce
objektů	objekt	k1gInPc2	objekt
a	a	k8xC	a
asanace	asanace	k1gFnSc2	asanace
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Specifická	specifický	k2eAgFnSc1d1	specifická
imunoprofylaxe	imunoprofylaxe	k1gFnSc1	imunoprofylaxe
se	se	k3xPyFc4	se
neprovádí	provádět	k5eNaImIp3nS	provádět
<g/>
,	,	kIx,	,
komerčně	komerčně	k6eAd1	komerčně
dostupné	dostupný	k2eAgFnPc1d1	dostupná
vakcíny	vakcína	k1gFnPc1	vakcína
zatím	zatím	k6eAd1	zatím
neexistují	existovat	k5eNaImIp3nP	existovat
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dezinfekci	dezinfekce	k1gFnSc3	dezinfekce
se	se	k3xPyFc4	se
doporučuje	doporučovat	k5eAaImIp3nS	doporučovat
2-4	[number]	k4	2-4
%	%	kIx~	%
louh	louh	k1gInSc1	louh
sodný	sodný	k2eAgInSc1d1	sodný
<g/>
,	,	kIx,	,
4	[number]	k4	4
%	%	kIx~	%
kresol	kresol	k1gInSc1	kresol
<g/>
,	,	kIx,	,
1-2	[number]	k4	1-2
%	%	kIx~	%
formaldehyd	formaldehyd	k1gInSc1	formaldehyd
<g/>
,	,	kIx,	,
4-8	[number]	k4	4-8
%	%	kIx~	%
chloramin	chloramin	k1gInSc1	chloramin
B	B	kA	B
<g/>
,	,	kIx,	,
chlorové	chlorový	k2eAgNnSc1d1	chlorové
vápno	vápno	k1gNnSc1	vápno
s	s	k7c7	s
2-4	[number]	k4	2-4
%	%	kIx~	%
aktivního	aktivní	k2eAgInSc2d1	aktivní
chloru	chlor	k1gInSc2	chlor
(	(	kIx(	(
<g/>
výběhy	výběh	k1gInPc4	výběh
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
20	[number]	k4	20
%	%	kIx~	%
suspenze	suspenze	k1gFnPc1	suspenze
čerstvě	čerstvě	k6eAd1	čerstvě
hašeného	hašený	k2eAgNnSc2d1	hašené
vápna	vápno	k1gNnSc2	vápno
<g/>
.	.	kIx.	.
</s>
<s>
Chlorové	chlorový	k2eAgInPc1d1	chlorový
preparáty	preparát	k1gInPc1	preparát
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
korozi	koroze	k1gFnSc4	koroze
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
nejsou	být	k5eNaImIp3nP	být
vhodné	vhodný	k2eAgInPc1d1	vhodný
na	na	k7c4	na
dezinfekci	dezinfekce	k1gFnSc4	dezinfekce
kovových	kovový	k2eAgFnPc2d1	kovová
klecí	klec	k1gFnPc2	klec
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
dezinfekci	dezinfekce	k1gFnSc4	dezinfekce
prostorů	prostor	k1gInPc2	prostor
zpracovatelského	zpracovatelský	k2eAgInSc2d1	zpracovatelský
průmyslu	průmysl	k1gInSc2	průmysl
(	(	kIx(	(
<g/>
např.	např.	kA	např.
porážky	porážka	k1gFnSc2	porážka
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
doporučován	doporučovat	k5eAaImNgInS	doporučovat
3-5	[number]	k4	3-5
%	%	kIx~	%
chloramin	chloramin	k1gInSc1	chloramin
B	B	kA	B
<g/>
,	,	kIx,	,
aerosol	aerosol	k1gInSc1	aerosol
kyseliny	kyselina	k1gFnSc2	kyselina
pyrohroznové	pyrohroznová	k1gFnSc2	pyrohroznová
nebo	nebo	k8xC	nebo
kyselina	kyselina	k1gFnSc1	kyselina
mléčná	mléčný	k2eAgFnSc1d1	mléčná
v	v	k7c6	v
množství	množství	k1gNnSc6	množství
10	[number]	k4	10
mg	mg	kA	mg
na	na	k7c4	na
1	[number]	k4	1
m	m	kA	m
<g/>
3	[number]	k4	3
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
Vhodné	vhodný	k2eAgNnSc1d1	vhodné
je	být	k5eAaImIp3nS	být
ozařování	ozařování	k1gNnSc1	ozařování
ultrafialovými	ultrafialový	k2eAgInPc7d1	ultrafialový
paprsky	paprsek	k1gInPc7	paprsek
<g/>
.	.	kIx.	.
</s>
