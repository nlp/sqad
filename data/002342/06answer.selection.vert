<s>
Chlamydióza	Chlamydióza	k1gFnSc1	Chlamydióza
je	být	k5eAaImIp3nS	být
obecné	obecný	k2eAgNnSc4d1	obecné
označení	označení	k1gNnSc4	označení
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
infekce	infekce	k1gFnSc2	infekce
různých	různý	k2eAgInPc2d1	různý
živočišných	živočišný	k2eAgInPc2d1	živočišný
druhů	druh	k1gInPc2	druh
vyvolávaných	vyvolávaný	k2eAgInPc2d1	vyvolávaný
obligátně	obligátně	k6eAd1	obligátně
intracelulárními	intracelulární	k2eAgFnPc7d1	intracelulární
bakteriemi	bakterie	k1gFnPc7	bakterie
z	z	k7c2	z
řádu	řád	k1gInSc2	řád
Chlamydiales	Chlamydialesa	k1gFnPc2	Chlamydialesa
<g/>
.	.	kIx.	.
</s>
