<s>
Minaret	minaret	k1gInSc1	minaret
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgMnS	být
na	na	k7c4	na
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
staletí	staletí	k1gNnPc4	staletí
zapomenut	zapomenut	k2eAgInSc1d1	zapomenut
a	a	k8xC	a
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1886	[number]	k4	1886
jej	on	k3xPp3gMnSc4	on
znovuobjevil	znovuobjevit	k5eAaPmAgMnS	znovuobjevit
Sir	sir	k1gMnSc1	sir
Thomas	Thomas	k1gMnSc1	Thomas
Holdich	Holdich	k1gMnSc1	Holdich
<g/>
.	.	kIx.	.
</s>
