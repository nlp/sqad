<p>
<s>
Minaret	minaret	k1gInSc1	minaret
Jam	jam	k1gInSc1	jam
(	(	kIx(	(
<g/>
م	م	k?	م
ج	ج	k?	ج
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
řeky	řeka	k1gFnSc2	řeka
Hari	Hari	k1gNnSc2	Hari
Rud	ruda	k1gFnPc2	ruda
<g/>
,	,	kIx,	,
persky	persky	k6eAd1	persky
<g/>
:	:	kIx,	:
ه	ه	k?	ه
Harī	Harī	k1gMnSc1	Harī
Rū	Rū	k1gMnSc1	Rū
<g/>
,	,	kIx,	,
angl	angl	k1gMnSc1	angl
<g/>
:	:	kIx,	:
Tejen	Tejen	k1gInSc1	Tejen
<g/>
,	,	kIx,	,
Tedzhen	Tedzhen	k1gInSc1	Tedzhen
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
také	také	k9	také
<g/>
:	:	kIx,	:
Rudkhaneh-ye	Rudkhanehe	k1gInSc1	Rudkhaneh-ye
Hari	Har	k1gFnSc2	Har
Rud	ruda	k1gFnPc2	ruda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Turkmenistánu	Turkmenistán	k1gInSc6	Turkmenistán
se	se	k3xPyFc4	se
řeka	řeka	k1gFnSc1	řeka
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
Tedžen	Tedžna	k1gFnPc2	Tedžna
<g/>
.	.	kIx.	.
</s>
<s>
Minaret	minaret	k1gInSc1	minaret
je	být	k5eAaImIp3nS	být
65	[number]	k4	65
metrů	metr	k1gInPc2	metr
vysoký	vysoký	k2eAgMnSc1d1	vysoký
a	a	k8xC	a
obklopují	obklopovat	k5eAaImIp3nP	obklopovat
jej	on	k3xPp3gInSc4	on
hory	hora	k1gFnPc1	hora
dosahující	dosahující	k2eAgFnSc2d1	dosahující
výšky	výška	k1gFnSc2	výška
až	až	k9	až
2	[number]	k4	2
400	[number]	k4	400
m.	m.	k?	m.
Celý	celý	k2eAgInSc1d1	celý
je	být	k5eAaImIp3nS	být
postaven	postaven	k2eAgInSc1d1	postaven
z	z	k7c2	z
pálených	pálený	k2eAgFnPc2d1	pálená
cihel	cihla	k1gFnPc2	cihla
</s>
</p>
<p>
<s>
Minaret	minaret	k1gInSc1	minaret
byl	být	k5eAaImAgInS	být
postaven	postavit	k5eAaPmNgInS	postavit
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgMnS	být
na	na	k7c4	na
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
staletí	staletí	k1gNnPc4	staletí
zapomenut	zapomenut	k2eAgInSc1d1	zapomenut
a	a	k8xC	a
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1886	[number]	k4	1886
jej	on	k3xPp3gMnSc4	on
znovuobjevil	znovuobjevit	k5eAaPmAgMnS	znovuobjevit
Sir	sir	k1gMnSc1	sir
Thomas	Thomas	k1gMnSc1	Thomas
Holdich	Holdich	k1gMnSc1	Holdich
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2002	[number]	k4	2002
je	být	k5eAaImIp3nS	být
minaret	minaret	k1gInSc4	minaret
součástí	součást	k1gFnPc2	součást
Seznamu	seznam	k1gInSc2	seznam
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	Unesco	k1gNnSc4	Unesco
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k8xS	tak
první	první	k4xOgFnSc7	první
afgánskou	afgánský	k2eAgFnSc7d1	afgánská
památkou	památka	k1gFnSc7	památka
zapsanou	zapsaný	k2eAgFnSc7d1	zapsaná
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
seznamu	seznam	k1gInSc6	seznam
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Minaret	minaret	k1gInSc1	minaret
Jam	jam	k1gInSc4	jam
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Minaret	minaret	k1gInSc1	minaret
of	of	k?	of
Jam	jam	k1gInSc1	jam
Archaeological	Archaeological	k1gMnSc1	Archaeological
Project	Project	k1gMnSc1	Project
</s>
</p>
