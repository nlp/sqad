<p>
<s>
Stith	Stith	k1gMnSc1	Stith
Thompson	Thompson	k1gMnSc1	Thompson
(	(	kIx(	(
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1885	[number]	k4	1885
Bloomfield	Bloomfielda	k1gFnPc2	Bloomfielda
<g/>
,	,	kIx,	,
Kentucky	Kentuck	k1gInPc7	Kentuck
–	–	k?	–
10	[number]	k4	10
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1976	[number]	k4	1976
Columbus	Columbus	k1gMnSc1	Columbus
<g/>
,	,	kIx,	,
Indiana	Indiana	k1gFnSc1	Indiana
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
folklorista	folklorista	k1gMnSc1	folklorista
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgMnPc2	první
zámořských	zámořský	k2eAgMnPc2d1	zámořský
představitelů	představitel	k1gMnPc2	představitel
tzv.	tzv.	kA	tzv.
finské	finský	k2eAgFnPc4d1	finská
školy	škola	k1gFnPc4	škola
ve	v	k7c6	v
folkloristice	folkloristika	k1gFnSc6	folkloristika
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
nejznámějším	známý	k2eAgInSc7d3	nejznámější
dílem	díl	k1gInSc7	díl
je	být	k5eAaImIp3nS	být
Motif-index	Motifndex	k1gInSc1	Motif-index
of	of	k?	of
Folk-literature	Folkiteratur	k1gMnSc5	Folk-literatur
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
jsou	být	k5eAaImIp3nP	být
klasifikovány	klasifikovat	k5eAaImNgInP	klasifikovat
motivy	motiv	k1gInPc7	motiv
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
nejmenší	malý	k2eAgFnPc1d3	nejmenší
jednotky	jednotka	k1gFnPc1	jednotka
tradičního	tradiční	k2eAgInSc2d1	tradiční
narativu	narativ	k1gInSc2	narativ
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
druhým	druhý	k4xOgInSc7	druhý
známým	známý	k2eAgInSc7d1	známý
počinem	počin	k1gInSc7	počin
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc2	jeho
práce	práce	k1gFnSc2	práce
na	na	k7c4	na
rozšíření	rozšíření	k1gNnSc4	rozšíření
katalogu	katalog	k1gInSc2	katalog
pohádek	pohádka	k1gFnPc2	pohádka
Verzeichnis	Verzeichnis	k1gFnSc2	Verzeichnis
der	drát	k5eAaImRp2nS	drát
Märchentypen	Märchentypen	k2eAgInSc1d1	Märchentypen
sestaveného	sestavený	k2eAgNnSc2d1	sestavené
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
finským	finský	k2eAgMnSc7d1	finský
folkloristou	folklorista	k1gMnSc7	folklorista
Antti	Antť	k1gFnSc2	Antť
Aarnem	Aarno	k1gNnSc7	Aarno
<g/>
.	.	kIx.	.
</s>
<s>
Thompson	Thompson	k1gMnSc1	Thompson
tento	tento	k3xDgInSc4	tento
katalog	katalog	k1gInSc4	katalog
přeložil	přeložit	k5eAaPmAgMnS	přeložit
do	do	k7c2	do
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
,	,	kIx,	,
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
ho	on	k3xPp3gMnSc4	on
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1928	[number]	k4	1928
vydal	vydat	k5eAaPmAgInS	vydat
pod	pod	k7c7	pod
anglickým	anglický	k2eAgInSc7d1	anglický
názvem	název	k1gInSc7	název
The	The	k1gFnSc2	The
Types	Types	k1gInSc1	Types
of	of	k?	of
the	the	k?	the
Folktale	Folktal	k1gMnSc5	Folktal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Dětství	dětství	k1gNnSc4	dětství
===	===	k?	===
</s>
</p>
<p>
<s>
Stith	Stith	k1gMnSc1	Stith
Thompson	Thompson	k1gMnSc1	Thompson
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
v	v	k7c6	v
Bloomfieldu	Bloomfield	k1gInSc6	Bloomfield
v	v	k7c6	v
Nelson	Nelson	k1gMnSc1	Nelson
County	Counta	k1gFnSc2	Counta
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Kentucky	Kentucka	k1gFnSc2	Kentucka
rodičům	rodič	k1gMnPc3	rodič
Johnu	John	k1gMnSc6	John
Wardenovi	Wardenův	k2eAgMnPc1d1	Wardenův
and	and	k?	and
Elize	elize	k1gFnPc4	elize
(	(	kIx(	(
<g/>
McClaskey	McClaske	k2eAgFnPc4d1	McClaske
<g/>
)	)	kIx)	)
Thompsonovým	Thompsonová	k1gFnPc3	Thompsonová
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
12	[number]	k4	12
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Indianapolis	Indianapolis	k1gFnSc2	Indianapolis
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
letech	let	k1gInPc6	let
1903	[number]	k4	1903
<g/>
–	–	k?	–
<g/>
1905	[number]	k4	1905
navštěvoval	navštěvovat	k5eAaImAgInS	navštěvovat
Butler	Butler	k1gInSc4	Butler
College	Colleg	k1gInSc2	Colleg
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vzdělání	vzdělání	k1gNnSc1	vzdělání
===	===	k?	===
</s>
</p>
<p>
<s>
Bakalářský	bakalářský	k2eAgInSc1d1	bakalářský
titul	titul	k1gInSc1	titul
získal	získat	k5eAaPmAgInS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1909	[number]	k4	1909
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
anglická	anglický	k2eAgFnSc1d1	anglická
literatura	literatura	k1gFnSc1	literatura
<g/>
;	;	kIx,	;
jeho	jeho	k3xOp3gFnSc1	jeho
bakalářská	bakalářský	k2eAgFnSc1d1	Bakalářská
práce	práce	k1gFnSc1	práce
se	se	k3xPyFc4	se
zabývala	zabývat	k5eAaImAgFnS	zabývat
otázkou	otázka	k1gFnSc7	otázka
revenantů	revenant	k1gInPc2	revenant
v	v	k7c6	v
lidových	lidový	k2eAgFnPc6d1	lidová
naracích	narace	k1gFnPc6	narace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1911	[number]	k4	1911
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
na	na	k7c4	na
Kalifornskou	kalifornský	k2eAgFnSc4d1	kalifornská
univerzitu	univerzita	k1gFnSc4	univerzita
v	v	k7c4	v
Berkeley	Berkele	k1gMnPc4	Berkele
<g/>
.	.	kIx.	.
</s>
<s>
Doktorát	doktorát	k1gInSc1	doktorát
získal	získat	k5eAaPmAgInS	získat
na	na	k7c6	na
Harvardu	Harvard	k1gInSc6	Harvard
po	po	k7c6	po
obhájení	obhájení	k1gNnSc6	obhájení
disertace	disertace	k1gFnSc2	disertace
o	o	k7c6	o
evropských	evropský	k2eAgFnPc6d1	Evropská
pohádkách	pohádka	k1gFnPc6	pohádka
u	u	k7c2	u
severoamerických	severoamerický	k2eAgMnPc2d1	severoamerický
indiánů	indián	k1gMnPc2	indián
publikované	publikovaný	k2eAgInPc1d1	publikovaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1919	[number]	k4	1919
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Univerzitní	univerzitní	k2eAgFnSc4d1	univerzitní
kariéru	kariéra	k1gFnSc4	kariéra
zahájil	zahájit	k5eAaPmAgMnS	zahájit
na	na	k7c6	na
Texaské	texaský	k2eAgFnSc6d1	texaská
univerzitě	univerzita	k1gFnSc6	univerzita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nejdéle	dlouho	k6eAd3	dlouho
působil	působit	k5eAaImAgMnS	působit
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Indianě	Indiana	k1gFnSc6	Indiana
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
až	až	k9	až
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
odchodu	odchod	k1gInSc2	odchod
do	do	k7c2	do
důchodu	důchod	k1gInSc2	důchod
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1955	[number]	k4	1955
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
tady	tady	k6eAd1	tady
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
detailněji	detailně	k6eAd2	detailně
zabývat	zabývat	k5eAaImF	zabývat
folkloristikou	folkloristika	k1gFnSc7	folkloristika
jako	jako	k8xC	jako
disciplínou	disciplína	k1gFnSc7	disciplína
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
folkloristika	folkloristika	k1gFnSc1	folkloristika
měla	mít	k5eAaImAgFnS	mít
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
své	svůj	k3xOyFgNnSc4	svůj
pevné	pevný	k2eAgNnSc4d1	pevné
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
univerzity	univerzita	k1gFnSc2	univerzita
byl	být	k5eAaImAgMnS	být
stále	stále	k6eAd1	stále
aktivním	aktivní	k2eAgMnSc7d1	aktivní
badatelem	badatel	k1gMnSc7	badatel
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
s	s	k7c7	s
ostatními	ostatní	k2eAgMnPc7d1	ostatní
folkloristy	folklorista	k1gMnPc7	folklorista
<g/>
,	,	kIx,	,
psal	psát	k5eAaImAgMnS	psát
antologie	antologie	k1gFnPc4	antologie
<g/>
,	,	kIx,	,
přepracovával	přepracovávat	k5eAaImAgInS	přepracovávat
Motif-index	Motifndex	k1gInSc1	Motif-index
of	of	k?	of
Folk	folk	k1gInSc1	folk
literature	literatur	k1gMnSc5	literatur
a	a	k8xC	a
opět	opět	k6eAd1	opět
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
Aarneho	Aarne	k1gMnSc2	Aarne
katalog	katalog	k1gInSc1	katalog
a	a	k8xC	a
spolupracoval	spolupracovat	k5eAaImAgInS	spolupracovat
i	i	k9	i
na	na	k7c6	na
přípravě	příprava	k1gFnSc6	příprava
katalogu	katalog	k1gInSc2	katalog
indických	indický	k2eAgFnPc2d1	indická
pohádek	pohádka	k1gFnPc2	pohádka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Badatelská	badatelský	k2eAgFnSc1d1	badatelská
činnost	činnost	k1gFnSc1	činnost
==	==	k?	==
</s>
</p>
<p>
<s>
Stith	Stith	k1gMnSc1	Stith
Thompson	Thompson	k1gMnSc1	Thompson
byl	být	k5eAaImAgMnS	být
zastáncem	zastánce	k1gMnSc7	zastánce
historicko-geografické	historickoeografický	k2eAgFnSc2d1	historicko-geografická
metody	metoda	k1gFnSc2	metoda
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
také	také	k9	také
použil	použít	k5eAaPmAgMnS	použít
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
jeho	jeho	k3xOp3gNnPc2	jeho
nejznámějších	známý	k2eAgNnPc2d3	nejznámější
studií	studio	k1gNnPc2	studio
The	The	k1gMnPc2	The
Star	Star	kA	Star
Husband	Husband	k1gInSc4	Husband
Tale	Tale	k1gNnSc2	Tale
<g/>
,	,	kIx,	,
publikované	publikovaný	k2eAgFnSc2d1	publikovaná
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
studii	studie	k1gFnSc6	studie
porovnával	porovnávat	k5eAaImAgMnS	porovnávat
různé	různý	k2eAgFnSc2d1	různá
verze	verze	k1gFnSc2	verze
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
původních	původní	k2eAgFnPc2d1	původní
pohádek	pohádka	k1gFnPc2	pohádka
severoamerických	severoamerický	k2eAgMnPc2d1	severoamerický
indiánů	indián	k1gMnPc2	indián
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
po	po	k7c6	po
velkém	velký	k2eAgNnSc6d1	velké
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
v	v	k7c6	v
arktických	arktický	k2eAgFnPc6d1	arktická
oblastech	oblast	k1gFnPc6	oblast
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
pohádce	pohádka	k1gFnSc6	pohádka
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
aplikovat	aplikovat	k5eAaBmF	aplikovat
evropský	evropský	k2eAgInSc4d1	evropský
přístup	přístup	k1gInSc4	přístup
založený	založený	k2eAgInSc4d1	založený
na	na	k7c4	na
komparaci	komparace	k1gFnSc4	komparace
různých	různý	k2eAgFnPc2d1	různá
verzí	verze	k1gFnPc2	verze
příběhu	příběh	k1gInSc2	příběh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Dorson	Dorson	k1gMnSc1	Dorson
<g/>
,	,	kIx,	,
Richard	Richard	k1gMnSc1	Richard
M.	M.	kA	M.
(	(	kIx(	(
<g/>
leden	leden	k1gInSc1	leden
<g/>
–	–	k?	–
<g/>
březen	březen	k1gInSc1	březen
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Stith	Stith	k1gMnSc1	Stith
Thompson	Thompson	k1gMnSc1	Thompson
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
-	-	kIx~	-
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Journal	Journal	k1gMnSc1	Journal
of	of	k?	of
American	American	k1gMnSc1	American
Folklore	folklor	k1gInSc5	folklor
<g/>
.	.	kIx.	.
</s>
<s>
American	American	k1gMnSc1	American
Folklore	folklor	k1gInSc5	folklor
Society	societa	k1gFnPc1	societa
<g/>
.	.	kIx.	.
90	[number]	k4	90
(	(	kIx(	(
<g/>
355	[number]	k4	355
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
JSTOR	JSTOR	kA	JSTOR
539017	[number]	k4	539017
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Haase	Haase	k1gFnSc1	Haase
<g/>
,	,	kIx,	,
Donald	Donald	k1gMnSc1	Donald
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Greenwood	Greenwood	k1gInSc1	Greenwood
Encyclopedia	Encyclopedium	k1gNnSc2	Encyclopedium
of	of	k?	of
Folktales	Folktales	k1gMnSc1	Folktales
and	and	k?	and
Fairy	Faira	k1gFnSc2	Faira
Tales	Talesa	k1gFnPc2	Talesa
<g/>
,	,	kIx,	,
Volumes	Volumesa	k1gFnPc2	Volumesa
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
GREENWOOD	GREENWOOD	kA	GREENWOOD
PRESS	PRESS	kA	PRESS
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Stith	Stitha	k1gFnPc2	Stitha
Thompson	Thompson	k1gNnSc4	Thompson
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
