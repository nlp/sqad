<s>
Kmenové	kmenový	k2eAgFnPc1d1	kmenová
buňky	buňka	k1gFnPc1	buňka
jsou	být	k5eAaImIp3nP	být
nediferencované	diferencovaný	k2eNgFnPc1d1	nediferencovaná
živočišné	živočišný	k2eAgFnPc1d1	živočišná
buňky	buňka	k1gFnPc1	buňka
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
schopnost	schopnost	k1gFnSc4	schopnost
se	se	k3xPyFc4	se
dělit	dělit	k5eAaImF	dělit
(	(	kIx(	(
<g/>
proliferovat	proliferovat	k5eAaImF	proliferovat
<g/>
)	)	kIx)	)
a	a	k8xC	a
přeměnit	přeměnit	k5eAaPmF	přeměnit
se	se	k3xPyFc4	se
na	na	k7c4	na
jiný	jiný	k2eAgInSc4d1	jiný
buněčný	buněčný	k2eAgInSc4d1	buněčný
typ	typ	k1gInSc4	typ
(	(	kIx(	(
<g/>
diferencovat	diferencovat	k5eAaImF	diferencovat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
