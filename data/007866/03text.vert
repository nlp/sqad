<s>
Zmije	zmije	k1gFnSc1	zmije
řetízková	řetízkový	k2eAgFnSc1d1	řetízková
(	(	kIx(	(
<g/>
Daboia	Daboia	k1gFnSc1	Daboia
russelli	russellit	k5eAaPmRp2nS	russellit
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jedovatý	jedovatý	k2eAgMnSc1d1	jedovatý
had	had	k1gMnSc1	had
z	z	k7c2	z
jižní	jižní	k2eAgFnSc2d1	jižní
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
Asie	Asie	k1gFnSc2	Asie
<g/>
,	,	kIx,	,
jediný	jediný	k2eAgInSc1d1	jediný
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Daboia	Daboium	k1gNnSc2	Daboium
<g/>
.	.	kIx.	.
</s>
<s>
Areál	areál	k1gInSc1	areál
jejího	její	k3xOp3gNnSc2	její
rozšíření	rozšíření	k1gNnSc2	rozšíření
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
oblast	oblast	k1gFnSc1	oblast
od	od	k7c2	od
Pákistánu	Pákistán	k1gInSc2	Pákistán
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
Indii	Indie	k1gFnSc4	Indie
<g/>
,	,	kIx,	,
Bangladéš	Bangladéš	k1gInSc4	Bangladéš
<g/>
,	,	kIx,	,
Nepál	Nepál	k1gInSc4	Nepál
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc4d1	jižní
Čínu	Čína	k1gFnSc4	Čína
<g/>
,	,	kIx,	,
Šrí	Šrí	k1gFnSc6	Šrí
Lanku	lanko	k1gNnSc6	lanko
<g/>
,	,	kIx,	,
Taiwan	Taiwan	k1gMnSc1	Taiwan
<g/>
,	,	kIx,	,
Myanmar	Myanmar	k1gMnSc1	Myanmar
<g/>
,	,	kIx,	,
Kambodžu	Kambodža	k1gFnSc4	Kambodža
<g/>
,	,	kIx,	,
Thajsko	Thajsko	k1gNnSc1	Thajsko
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c6	po
Indonésii	Indonésie	k1gFnSc6	Indonésie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
Jávě	Jáva	k1gFnSc6	Jáva
<g/>
,	,	kIx,	,
ostrovech	ostrov	k1gInPc6	ostrov
Lomben	Lomben	k2eAgMnSc1d1	Lomben
<g/>
,	,	kIx,	,
Komodo	komoda	k1gFnSc5	komoda
<g/>
,	,	kIx,	,
Endeh	Endeh	k1gMnSc1	Endeh
a	a	k8xC	a
Flores	Flores	k1gMnSc1	Flores
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
v	v	k7c6	v
Laosu	Laos	k1gInSc6	Laos
<g/>
,	,	kIx,	,
Vietnamu	Vietnam	k1gInSc6	Vietnam
a	a	k8xC	a
na	na	k7c6	na
Sumatře	Sumatra	k1gFnSc6	Sumatra
není	být	k5eNaImIp3nS	být
prokázána	prokázán	k2eAgFnSc1d1	prokázána
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
nedávnem	nedávno	k1gNnSc7	nedávno
bylo	být	k5eAaImAgNnS	být
uznáváno	uznávat	k5eAaImNgNnS	uznávat
několik	několik	k4yIc1	několik
poddruhů	poddruh	k1gInPc2	poddruh
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
uznávány	uznáván	k2eAgFnPc1d1	uznávána
již	již	k6eAd1	již
jen	jen	k9	jen
dva	dva	k4xCgMnPc4	dva
<g/>
:	:	kIx,	:
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Daboia	Daboius	k1gMnSc4	Daboius
russelli	russelnout	k5eAaImAgMnP	russelnout
russelli	russell	k1gMnPc1	russell
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byly	být	k5eAaImAgInP	být
zařazeny	zařadit	k5eAaPmNgInP	zařadit
dřívější	dřívější	k2eAgInPc1d1	dřívější
poddruhy	poddruh	k1gInPc1	poddruh
D.	D.	kA	D.
r.	r.	kA	r.
nordicus	nordicus	k1gInSc4	nordicus
a	a	k8xC	a
D.	D.	kA	D.
r.	r.	kA	r.
pulchella	pulchella	k1gMnSc1	pulchella
<g/>
.	.	kIx.	.
</s>
<s>
Poddruh	poddruh	k1gInSc1	poddruh
obývá	obývat	k5eAaImIp3nS	obývat
východní	východní	k2eAgInSc1d1	východní
Pákistán	Pákistán	k1gInSc1	Pákistán
(	(	kIx(	(
<g/>
provincie	provincie	k1gFnSc1	provincie
Sindh	Sindha	k1gFnPc2	Sindha
a	a	k8xC	a
Páňdžháb	Páňdžhába	k1gFnPc2	Páňdžhába
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Indii	Indie	k1gFnSc4	Indie
<g/>
,	,	kIx,	,
Bangladéš	Bangladéš	k1gInSc4	Bangladéš
<g/>
,	,	kIx,	,
Srí	Srí	k1gFnSc4	Srí
Lanku	lanko	k1gNnSc3	lanko
a	a	k8xC	a
Nepál	Nepál	k1gInSc1	Nepál
<g/>
.	.	kIx.	.
</s>
<s>
Daboia	Daboia	k1gFnSc1	Daboia
russelli	russell	k1gMnSc3	russell
siamensis	siamensis	k1gFnSc2	siamensis
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
byly	být	k5eAaImAgInP	být
zařazeny	zařadit	k5eAaPmNgInP	zařadit
tyto	tento	k3xDgInPc1	tento
poddruhy	poddruh	k1gInPc1	poddruh
<g/>
:	:	kIx,	:
D.	D.	kA	D.
r.	r.	kA	r.
formosensis	formosensis	k1gFnPc2	formosensis
<g/>
,	,	kIx,	,
D.	D.	kA	D.
r.	r.	kA	r.
limitis	limitis	k1gFnSc1	limitis
a	a	k8xC	a
D.	D.	kA	D.
r.	r.	kA	r.
sublimitis	sublimitis	k1gFnSc1	sublimitis
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
Myanmaru	Myanmar	k1gInSc6	Myanmar
<g/>
,	,	kIx,	,
Thajsku	Thajsko	k1gNnSc6	Thajsko
<g/>
,	,	kIx,	,
Kambodži	Kambodža	k1gFnSc6	Kambodža
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgFnSc6d1	jižní
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
Indonésii	Indonésie	k1gFnSc6	Indonésie
(	(	kIx(	(
<g/>
východní	východní	k2eAgFnSc1d1	východní
Jáva	Jáva	k1gFnSc1	Jáva
a	a	k8xC	a
ostrovy	ostrov	k1gInPc1	ostrov
Lomben	Lombna	k1gFnPc2	Lombna
<g/>
,	,	kIx,	,
Komodo	komoda	k1gFnSc5	komoda
<g/>
,	,	kIx,	,
Endeh	Endeh	k1gMnSc1	Endeh
a	a	k8xC	a
Flores	Flores	k1gMnSc1	Flores
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c4	na
Taiwanu	Taiwana	k1gFnSc4	Taiwana
<g/>
.	.	kIx.	.
</s>
<s>
Populace	populace	k1gFnSc1	populace
tohoto	tento	k3xDgInSc2	tento
podruhu	podruh	k1gMnSc3	podruh
jsou	být	k5eAaImIp3nP	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
navzájem	navzájem	k6eAd1	navzájem
odděleny	oddělit	k5eAaPmNgInP	oddělit
a	a	k8xC	a
žijí	žít	k5eAaImIp3nP	žít
spíše	spíše	k9	spíše
ostrůvkovitě	ostrůvkovitě	k6eAd1	ostrůvkovitě
<g/>
.	.	kIx.	.
</s>
<s>
Zmije	zmije	k1gFnSc1	zmije
má	mít	k5eAaImIp3nS	mít
klasické	klasický	k2eAgNnSc4d1	klasické
zmijí	zmijí	k2eAgNnSc4d1	zmijí
tělo	tělo	k1gNnSc4	tělo
<g/>
,	,	kIx,	,
oválné	oválný	k2eAgFnPc1d1	oválná
<g/>
,	,	kIx,	,
tlusté	tlustý	k2eAgFnPc1d1	tlustá
<g/>
,	,	kIx,	,
a	a	k8xC	a
s	s	k7c7	s
oválnou	oválný	k2eAgFnSc7d1	oválná
hlavou	hlava	k1gFnSc7	hlava
<g/>
.	.	kIx.	.
</s>
<s>
Dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
až	až	k9	až
120	[number]	k4	120
cm	cm	kA	cm
délky	délka	k1gFnSc2	délka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
největší	veliký	k2eAgInPc1d3	veliký
údaje	údaj	k1gInPc1	údaj
hovoří	hovořit	k5eAaImIp3nP	hovořit
až	až	k9	až
o	o	k7c4	o
185	[number]	k4	185
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Občas	občas	k6eAd1	občas
jsou	být	k5eAaImIp3nP	být
k	k	k7c3	k
nalezení	nalezení	k1gNnSc3	nalezení
jedinci	jedinec	k1gMnPc1	jedinec
dlouzí	dlouhý	k2eAgMnPc1d1	dlouhý
160	[number]	k4	160
cm	cm	kA	cm
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
to	ten	k3xDgNnSc1	ten
už	už	k9	už
je	být	k5eAaImIp3nS	být
vzácné	vzácný	k2eAgNnSc1d1	vzácné
<g/>
.	.	kIx.	.
</s>
<s>
Had	had	k1gMnSc1	had
má	mít	k5eAaImIp3nS	mít
výrazný	výrazný	k2eAgInSc4d1	výrazný
nasální	nasální	k2eAgInSc4d1	nasální
štítek	štítek	k1gInSc4	štítek
<g/>
.	.	kIx.	.
</s>
<s>
Zbarvení	zbarvení	k1gNnSc1	zbarvení
je	být	k5eAaImIp3nS	být
různorodé	různorodý	k2eAgNnSc1d1	různorodé
<g/>
,	,	kIx,	,
od	od	k7c2	od
šedé	šedá	k1gFnSc2	šedá
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
hnědou	hnědý	k2eAgFnSc4d1	hnědá
až	až	k6eAd1	až
po	po	k7c4	po
pískově	pískově	k6eAd1	pískově
žlutou	žlutý	k2eAgFnSc4d1	žlutá
<g/>
.	.	kIx.	.
</s>
<s>
Výraznou	výrazný	k2eAgFnSc7d1	výrazná
dominantou	dominanta	k1gFnSc7	dominanta
jsou	být	k5eAaImIp3nP	být
oválné	oválný	k2eAgFnPc1d1	oválná
skvrny	skvrna	k1gFnPc1	skvrna
uspořádané	uspořádaný	k2eAgFnPc1d1	uspořádaná
pravidelně	pravidelně	k6eAd1	pravidelně
na	na	k7c6	na
hřbetu	hřbet	k1gInSc6	hřbet
a	a	k8xC	a
bocích	bok	k1gInPc6	bok
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
nim	on	k3xPp3gInPc3	on
dostala	dostat	k5eAaPmAgFnS	dostat
své	svůj	k3xOyFgNnSc4	svůj
české	český	k2eAgNnSc4d1	české
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
připomínají	připomínat	k5eAaImIp3nP	připomínat
totiž	totiž	k9	totiž
řetěz	řetěz	k1gInSc4	řetěz
<g/>
.	.	kIx.	.
</s>
<s>
Okraje	okraj	k1gInPc1	okraj
skvrn	skvrna	k1gFnPc2	skvrna
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
bíle	bíle	k6eAd1	bíle
lemované	lemovaný	k2eAgInPc1d1	lemovaný
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
i	i	k9	i
navzájem	navzájem	k6eAd1	navzájem
dotýkají	dotýkat	k5eAaImIp3nP	dotýkat
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
krásnou	krásný	k2eAgFnSc4d1	krásná
kůži	kůže	k1gFnSc4	kůže
bývá	bývat	k5eAaImIp3nS	bývat
často	často	k6eAd1	často
lovena	lovit	k5eAaImNgFnS	lovit
<g/>
.	.	kIx.	.
</s>
<s>
Had	had	k1gMnSc1	had
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
různorodém	různorodý	k2eAgNnSc6d1	různorodé
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
od	od	k7c2	od
nížin	nížina	k1gFnPc2	nížina
až	až	k9	až
po	po	k7c4	po
hory	hora	k1gFnPc4	hora
<g/>
,	,	kIx,	,
od	od	k7c2	od
deštných	deštný	k2eAgInPc2d1	deštný
lesů	les	k1gInPc2	les
až	až	k9	až
po	po	k7c4	po
horské	horský	k2eAgInPc4d1	horský
lesy	les	k1gInPc4	les
<g/>
.	.	kIx.	.
</s>
<s>
Dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výšky	výška	k1gFnPc4	výška
i	i	k9	i
3000	[number]	k4	3000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Stejně	stejně	k6eAd1	stejně
různorodé	různorodý	k2eAgNnSc1d1	různorodé
je	být	k5eAaImIp3nS	být
i	i	k9	i
spektrum	spektrum	k1gNnSc1	spektrum
potravy	potrava	k1gFnSc2	potrava
<g/>
.	.	kIx.	.
</s>
<s>
Loví	lovit	k5eAaImIp3nS	lovit
vše	všechen	k3xTgNnSc1	všechen
od	od	k7c2	od
hmyzu	hmyz	k1gInSc2	hmyz
<g/>
,	,	kIx,	,
obojživelníků	obojživelník	k1gMnPc2	obojživelník
<g/>
,	,	kIx,	,
plazů	plaz	k1gMnPc2	plaz
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c4	po
ptáky	pták	k1gMnPc4	pták
a	a	k8xC	a
savce	savec	k1gMnPc4	savec
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
rodí	rodit	k5eAaImIp3nP	rodit
živá	živý	k2eAgNnPc4d1	živé
mláďata	mládě	k1gNnPc4	mládě
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
20	[number]	k4	20
až	až	k9	až
40	[number]	k4	40
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
občas	občas	k6eAd1	občas
i	i	k9	i
přes	přes	k7c4	přes
60	[number]	k4	60
<g/>
!	!	kIx.	!
</s>
<s>
Zmije	zmije	k1gFnSc1	zmije
řetízková	řetízkový	k2eAgFnSc1d1	řetízková
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejnebezpečnějších	bezpečný	k2eNgMnPc2d3	nejnebezpečnější
hadů	had	k1gMnPc2	had
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
její	její	k3xOp3gNnSc4	její
velké	velký	k2eAgNnSc4d1	velké
rozšíření	rozšíření	k1gNnSc4	rozšíření
a	a	k8xC	a
silný	silný	k2eAgInSc1d1	silný
jed	jed	k1gInSc1	jed
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
svědomí	svědomí	k1gNnSc4	svědomí
mnoho	mnoho	k4c4	mnoho
úmrtí	úmrť	k1gFnPc2	úmrť
především	především	k6eAd1	především
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
je	být	k5eAaImIp3nS	být
aktivní	aktivní	k2eAgFnSc1d1	aktivní
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
stane	stanout	k5eAaPmIp3nS	stanout
<g/>
,	,	kIx,	,
že	že	k8xS	že
leze	lézt	k5eAaImIp3nS	lézt
do	do	k7c2	do
lidských	lidský	k2eAgInPc2d1	lidský
příbytků	příbytek	k1gInPc2	příbytek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
uštknutí	uštknutí	k1gNnSc2	uštknutí
ještě	ještě	k6eAd1	ještě
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Ročně	ročně	k6eAd1	ročně
způsobí	způsobit	k5eAaPmIp3nS	způsobit
smrt	smrt	k1gFnSc4	smrt
několika	několik	k4yIc2	několik
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
jed	jed	k1gInSc1	jed
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
nedá	dát	k5eNaPmIp3nS	dát
srovnávat	srovnávat	k5eAaImF	srovnávat
s	s	k7c7	s
jedy	jed	k1gInPc7	jed
kober	kobra	k1gFnPc2	kobra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zmije	zmije	k1gFnSc1	zmije
ho	on	k3xPp3gMnSc4	on
má	mít	k5eAaImIp3nS	mít
daleko	daleko	k6eAd1	daleko
větší	veliký	k2eAgNnSc4d2	veliký
množství	množství	k1gNnSc4	množství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
Asii	Asie	k1gFnSc6	Asie
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k8xS	jako
zabiják	zabiják	k1gInSc1	zabiják
<g/>
,	,	kIx,	,
tři	tři	k4xCgFnPc4	tři
čtvrtiny	čtvrtina	k1gFnPc4	čtvrtina
všech	všecek	k3xTgNnPc2	všecek
úmrtí	úmrtí	k1gNnPc2	úmrtí
po	po	k7c6	po
uštknutí	uštknutí	k1gNnSc6	uštknutí
hadem	had	k1gMnSc7	had
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jsou	být	k5eAaImIp3nP	být
způsobeny	způsobit	k5eAaPmNgInP	způsobit
právě	právě	k6eAd1	právě
zmijí	zmije	k1gFnSc7	zmije
řetízkovou	řetízkový	k2eAgFnSc7d1	řetízková
<g/>
.	.	kIx.	.
</s>
<s>
Uštknutí	uštknutí	k1gNnSc1	uštknutí
je	být	k5eAaImIp3nS	být
provázeno	provázet	k5eAaImNgNnS	provázet
prudkou	prudký	k2eAgFnSc7d1	prudká
bolestí	bolest	k1gFnSc7	bolest
<g/>
,	,	kIx,	,
otoky	otok	k1gInPc7	otok
<g/>
,	,	kIx,	,
krvácením	krvácení	k1gNnSc7	krvácení
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
kolapsem	kolaps	k1gInSc7	kolaps
dýchacího	dýchací	k2eAgNnSc2d1	dýchací
ústrojí	ústrojí	k1gNnSc2	ústrojí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
je	být	k5eAaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
"	"	kIx"	"
<g/>
Velká	velký	k2eAgFnSc1d1	velká
indická	indický	k2eAgFnSc1d1	indická
čtyřka	čtyřka	k1gFnSc1	čtyřka
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
kobra	kobra	k1gFnSc1	kobra
indická	indický	k2eAgFnSc1d1	indická
<g/>
,	,	kIx,	,
bungar	bungar	k1gInSc1	bungar
modravý	modravý	k2eAgInSc1d1	modravý
<g/>
,	,	kIx,	,
zmije	zmije	k1gFnSc1	zmije
řetízková	řetízkový	k2eAgFnSc1d1	řetízková
a	a	k8xC	a
zmije	zmije	k1gFnSc1	zmije
paví	paví	k2eAgFnSc1d1	paví
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíc	nejvíc	k6eAd1	nejvíc
úmrtí	úmrtí	k1gNnPc2	úmrtí
způsobí	způsobit	k5eAaPmIp3nS	způsobit
právě	právě	k9	právě
zmije	zmije	k1gFnSc1	zmije
řetízková	řetízkový	k2eAgFnSc1d1	řetízková
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Zmije	zmije	k1gFnSc1	zmije
řetízková	řetízkový	k2eAgFnSc1d1	řetízková
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc1	Wikimedium
Commons	Commons	k1gInSc1	Commons
Taxon	taxon	k1gInSc1	taxon
Daboia	Daboius	k1gMnSc2	Daboius
russelii	russelie	k1gFnSc6	russelie
ve	v	k7c6	v
Wikidruzích	Wikidruh	k1gInPc6	Wikidruh
</s>
