<p>
<s>
Rodinná	rodinný	k2eAgFnSc1d1	rodinná
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
je	být	k5eAaImIp3nS	být
terapeutický	terapeutický	k2eAgInSc4d1	terapeutický
nástroj	nástroj	k1gInSc4	nástroj
vytvořený	vytvořený	k2eAgInSc4d1	vytvořený
Virginií	Virginie	k1gFnSc7	Virginie
Satirovou	Satirův	k2eAgFnSc7d1	Satirův
<g/>
.	.	kIx.	.
</s>
<s>
Využívá	využívat	k5eAaImIp3nS	využívat
rodinné	rodinný	k2eAgFnPc4d1	rodinná
mapy	mapa	k1gFnPc4	mapa
a	a	k8xC	a
psychodrama	psychodrama	k1gNnSc4	psychodrama
k	k	k7c3	k
uvědomění	uvědomění	k1gNnSc3	uvědomění
si	se	k3xPyFc3	se
nevědomých	vědomý	k2eNgInPc2d1	nevědomý
zátěží	zátěž	k1gFnSc7	zátěž
<g/>
,	,	kIx,	,
nevyřešených	vyřešený	k2eNgInPc2d1	nevyřešený
úkolů	úkol	k1gInPc2	úkol
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
generace	generace	k1gFnSc1	generace
rodiny	rodina	k1gFnSc2	rodina
klienta	klient	k1gMnSc2	klient
předávají	předávat	k5eAaImIp3nP	předávat
jedna	jeden	k4xCgFnSc1	jeden
druhé	druhý	k4xOgFnSc6	druhý
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
rodinné	rodinný	k2eAgFnSc2d1	rodinná
rekonstrukce	rekonstrukce	k1gFnSc2	rekonstrukce
je	být	k5eAaImIp3nS	být
odložit	odložit	k5eAaPmF	odložit
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
nevědomé	vědomý	k2eNgInPc4d1	nevědomý
přijaté	přijatý	k2eAgInPc4d1	přijatý
vzorce	vzorec	k1gInPc4	vzorec
chování	chování	k1gNnSc2	chování
a	a	k8xC	a
plněji	plně	k6eAd2	plně
žít	žít	k5eAaImF	žít
svůj	svůj	k3xOyFgInSc4	svůj
vlastni	vlastnit	k5eAaImRp2nS	vlastnit
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Rodinná	rodinný	k2eAgFnSc1d1	rodinná
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
je	být	k5eAaImIp3nS	být
předchůdce	předchůdce	k1gMnSc1	předchůdce
rodinných	rodinný	k2eAgFnPc2d1	rodinná
konstelací	konstelace	k1gFnPc2	konstelace
Berta	Berta	k1gFnSc1	Berta
Hellingera	Hellingera	k1gFnSc1	Hellingera
<g/>
.	.	kIx.	.
</s>
</p>
