<s>
Hluboký	hluboký	k2eAgInSc1d1	hluboký
vesmír	vesmír	k1gInSc1	vesmír
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Deep	Deep	k1gInSc1	Deep
sky	sky	k?	sky
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
termín	termín	k1gInSc1	termín
používaný	používaný	k2eAgInSc1d1	používaný
v	v	k7c6	v
amatérské	amatérský	k2eAgFnSc6d1	amatérská
astronomii	astronomie	k1gFnSc6	astronomie
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
převážně	převážně	k6eAd1	převážně
vizuálně	vizuálně	k6eAd1	vizuálně
slabých	slabý	k2eAgInPc2d1	slabý
objektů	objekt	k1gInPc2	objekt
mimo	mimo	k7c4	mimo
sluneční	sluneční	k2eAgFnSc4d1	sluneční
soustavu	soustava	k1gFnSc4	soustava
a	a	k8xC	a
naší	náš	k3xOp1gFnSc3	náš
galaxii	galaxie	k1gFnSc3	galaxie
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
cizí	cizí	k2eAgFnPc4d1	cizí
galaxie	galaxie	k1gFnPc4	galaxie
a	a	k8xC	a
hvězdokupy	hvězdokupa	k1gFnPc4	hvězdokupa
a	a	k8xC	a
mlhoviny	mlhovina	k1gFnPc4	mlhovina
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
objekty	objekt	k1gInPc1	objekt
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
od	od	k7c2	od
stovek	stovka	k1gFnPc2	stovka
do	do	k7c2	do
miliard	miliarda	k4xCgFnPc2	miliarda
světelných	světelný	k2eAgFnPc2d1	světelná
let	léto	k1gNnPc2	léto
od	od	k7c2	od
Země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Galaxie	galaxie	k1gFnPc1	galaxie
jsou	být	k5eAaImIp3nP	být
mimo	mimo	k6eAd1	mimo
naší	náš	k3xOp1gFnSc3	náš
galaxii	galaxie	k1gFnSc3	galaxie
<g/>
,	,	kIx,	,
naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
některé	některý	k3yIgFnPc1	některý
hvězdokupy	hvězdokupa	k1gFnPc1	hvězdokupa
a	a	k8xC	a
mlhoviny	mlhovina	k1gFnPc1	mlhovina
jsou	být	k5eAaImIp3nP	být
součástí	součást	k1gFnSc7	součást
Mléčné	mléčný	k2eAgFnSc2d1	mléčná
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgInSc1d3	nejznámější
katalog	katalog	k1gInSc1	katalog
deep-sky	deepka	k1gFnSc2	deep-ska
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnPc1d1	obsahující
některé	některý	k3yIgFnPc1	některý
nejjasnější	jasný	k2eAgFnPc1d3	nejjasnější
deep-sky	deepka	k1gFnPc1	deep-ska
objekty	objekt	k1gInPc4	objekt
na	na	k7c6	na
obloze	obloha	k1gFnSc6	obloha
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Messierův	Messierův	k2eAgInSc1d1	Messierův
katalog	katalog	k1gInSc1	katalog
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
110	[number]	k4	110
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yIgInPc4	který
patří	patřit	k5eAaImIp3nP	patřit
mlhoviny	mlhovina	k1gFnPc1	mlhovina
<g/>
,	,	kIx,	,
hvězdokupy	hvězdokupa	k1gFnPc1	hvězdokupa
a	a	k8xC	a
galaxie	galaxie	k1gFnPc1	galaxie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
několik	několik	k4yIc4	několik
dalších	další	k2eAgInPc2d1	další
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
do	do	k7c2	do
katalogu	katalog	k1gInSc2	katalog
dostaly	dostat	k5eAaPmAgInP	dostat
omylem	omylem	k6eAd1	omylem
(	(	kIx(	(
<g/>
např.	např.	kA	např.
dvojhvězda	dvojhvězda	k1gFnSc1	dvojhvězda
M	M	kA	M
<g/>
40	[number]	k4	40
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInPc1d1	další
katalogy	katalog	k1gInPc1	katalog
jsou	být	k5eAaImIp3nP	být
New	New	k1gFnSc7	New
General	General	k1gMnSc2	General
Catalogue	Catalogu	k1gMnSc2	Catalogu
(	(	kIx(	(
<g/>
NGC	NGC	kA	NGC
<g/>
)	)	kIx)	)
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc4	jeho
dva	dva	k4xCgInPc4	dva
dodatky	dodatek	k1gInPc4	dodatek
jsou	být	k5eAaImIp3nP	být
Index	index	k1gInSc4	index
Catalogue	Catalogu	k1gFnSc2	Catalogu
(	(	kIx(	(
<g/>
IC	IC	kA	IC
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
astrofotografie	astrofotografie	k1gFnSc1	astrofotografie
je	být	k5eAaImIp3nS	být
připisována	připisován	k2eAgFnSc1d1	připisována
Johnovi	John	k1gMnSc3	John
W.	W.	kA	W.
Draperovi	Draper	k1gMnSc3	Draper
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
vyfotografoval	vyfotografovat	k5eAaPmAgMnS	vyfotografovat
Měsíc	měsíc	k1gInSc4	měsíc
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1840	[number]	k4	1840
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
syn	syn	k1gMnSc1	syn
Henry	Henry	k1gMnSc1	Henry
Draper	Draper	k1gMnSc1	Draper
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1880	[number]	k4	1880
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nafotografoval	nafotografovat	k5eAaPmAgMnS	nafotografovat
Mlhovinu	mlhovina	k1gFnSc4	mlhovina
v	v	k7c6	v
Orionu	orion	k1gInSc6	orion
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgMnS	být
první	první	k4xOgMnSc1	první
astrosnímek	astrosnímek	k1gMnSc1	astrosnímek
hlubokého	hluboký	k2eAgInSc2d1	hluboký
vesmíru	vesmír	k1gInSc2	vesmír
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Deep	Deep	k1gInSc1	Deep
sky	sky	k?	sky
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Astrofotografie	astrofotografie	k1gFnSc2	astrofotografie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
