<s>
Hluboký	hluboký	k2eAgInSc1d1	hluboký
vesmír	vesmír	k1gInSc1	vesmír
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
Deep	Deep	k1gInSc1	Deep
sky	sky	k?	sky
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
termín	termín	k1gInSc1	termín
používaný	používaný	k2eAgInSc1d1	používaný
v	v	k7c6	v
amatérské	amatérský	k2eAgFnSc6d1	amatérská
astronomii	astronomie	k1gFnSc6	astronomie
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
převážně	převážně	k6eAd1	převážně
vizuálně	vizuálně	k6eAd1	vizuálně
slabých	slabý	k2eAgInPc2d1	slabý
objektů	objekt	k1gInPc2	objekt
mimo	mimo	k7c4	mimo
sluneční	sluneční	k2eAgFnSc4d1	sluneční
soustavu	soustava	k1gFnSc4	soustava
a	a	k8xC	a
naší	náš	k3xOp1gFnSc3	náš
galaxii	galaxie	k1gFnSc3	galaxie
<g/>
.	.	kIx.	.
</s>
