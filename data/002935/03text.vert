<s>
Lebka	lebka	k1gFnSc1	lebka
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
<g/>
:	:	kIx,	:
cranium	cranium	k1gNnSc1	cranium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
složitá	složitý	k2eAgFnSc1d1	složitá
součást	součást	k1gFnSc1	součást
kostry	kostra	k1gFnSc2	kostra
obratlovců	obratlovec	k1gMnPc2	obratlovec
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
chrání	chránit	k5eAaImIp3nS	chránit
mozek	mozek	k1gInSc4	mozek
a	a	k8xC	a
smyslové	smyslový	k2eAgInPc4d1	smyslový
orgány	orgán	k1gInPc4	orgán
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
vytvořena	vytvořit	k5eAaPmNgFnS	vytvořit
z	z	k7c2	z
řady	řada	k1gFnSc2	řada
lebečních	lebeční	k2eAgFnPc2d1	lebeční
kostí	kost	k1gFnPc2	kost
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
navázána	navázán	k2eAgFnSc1d1	navázána
pohyblivě	pohyblivě	k6eAd1	pohyblivě
na	na	k7c4	na
páteř	páteř	k1gFnSc4	páteř
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
kostí	kost	k1gFnPc2	kost
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
lebce	lebka	k1gFnSc6	lebka
spojena	spojen	k2eAgFnSc1d1	spojena
suturami	sutura	k1gFnPc7	sutura
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
jsou	být	k5eAaImIp3nP	být
pevné	pevný	k2eAgInPc1d1	pevný
vazivové	vazivový	k2eAgInPc1d1	vazivový
spoje	spoj	k1gInPc1	spoj
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
při	při	k7c6	při
porodu	porod	k1gInSc6	porod
nedošlo	dojít	k5eNaPmAgNnS	dojít
ke	k	k7c3	k
zlomenině	zlomenina	k1gFnSc3	zlomenina
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
novorozeňata	novorozeně	k1gNnPc4	novorozeně
mezi	mezi	k7c7	mezi
lebečními	lebeční	k2eAgFnPc7d1	lebeční
kostmi	kost	k1gFnPc7	kost
mezery	mezera	k1gFnPc4	mezera
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
vyplněny	vyplnit	k5eAaPmNgInP	vyplnit
vazivovými	vazivový	k2eAgFnPc7d1	vazivová
membránami	membrána	k1gFnPc7	membrána
(	(	kIx(	(
<g/>
fontanely	fontanela	k1gFnPc1	fontanela
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
na	na	k7c6	na
lebce	lebka	k1gFnSc6	lebka
-	-	kIx~	-
švy	šev	k1gInPc7	šev
-	-	kIx~	-
jsou	být	k5eAaImIp3nP	být
nepohyblivé	pohyblivý	k2eNgFnPc1d1	nepohyblivá
<g/>
.	.	kIx.	.
</s>
<s>
Pohyblivý	pohyblivý	k2eAgMnSc1d1	pohyblivý
je	být	k5eAaImIp3nS	být
jedině	jedině	k6eAd1	jedině
kloub	kloub	k1gInSc4	kloub
spojující	spojující	k2eAgFnSc4d1	spojující
mandibulu	mandibula	k1gFnSc4	mandibula
(	(	kIx(	(
<g/>
dolní	dolní	k2eAgFnSc4d1	dolní
čelist	čelist	k1gFnSc4	čelist
<g/>
)	)	kIx)	)
s	s	k7c7	s
lebkou	lebka	k1gFnSc7	lebka
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
lebky	lebka	k1gFnSc2	lebka
obvykle	obvykle	k6eAd1	obvykle
rozeznáváme	rozeznávat	k5eAaImIp1nP	rozeznávat
část	část	k1gFnSc4	část
obličejovou	obličejový	k2eAgFnSc4d1	obličejová
(	(	kIx(	(
<g/>
splanchocranium	splanchocranium	k1gNnSc4	splanchocranium
<g/>
)	)	kIx)	)
a	a	k8xC	a
mozkovnu	mozkovna	k1gFnSc4	mozkovna
(	(	kIx(	(
<g/>
neurocranium	neurocranium	k1gNnSc4	neurocranium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obličejová	obličejový	k2eAgFnSc1d1	obličejová
část	část	k1gFnSc1	část
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
vyústění	vyústění	k1gNnSc4	vyústění
smyslových	smyslový	k2eAgInPc2d1	smyslový
orgánů	orgán	k1gInPc2	orgán
a	a	k8xC	a
ústní	ústní	k2eAgInSc4d1	ústní
otvor	otvor	k1gInSc4	otvor
<g/>
.	.	kIx.	.
</s>
<s>
Dutá	dutý	k2eAgFnSc1d1	dutá
mozková	mozkový	k2eAgFnSc1d1	mozková
část	část	k1gFnSc1	část
chrání	chránit	k5eAaImIp3nS	chránit
mozek	mozek	k1gInSc4	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
oddíly	oddíl	k1gInPc1	oddíl
lebky	lebka	k1gFnSc2	lebka
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Klenba	klenba	k1gFnSc1	klenba
lebeční	lebeční	k2eAgFnSc1d1	lebeční
-	-	kIx~	-
lat.	lat.	k?	lat.
neurocranium	neurocranium	k1gNnSc4	neurocranium
Báze	báze	k1gFnSc2	báze
lební	lební	k2eAgFnSc1d1	lební
Kostra	kostra	k1gFnSc1	kostra
obličeje	obličej	k1gInSc2	obličej
-	-	kIx~	-
lat.	lat.	k?	lat.
splanchocranium	splanchocranium	k1gNnSc1	splanchocranium
Horní	horní	k2eAgFnSc4d1	horní
čelist	čelist	k1gFnSc4	čelist
Dutiny	dutina	k1gFnSc2	dutina
pro	pro	k7c4	pro
sluchový	sluchový	k2eAgInSc4d1	sluchový
aparát	aparát	k1gInSc4	aparát
Dutina	dutina	k1gFnSc1	dutina
lební	lební	k2eAgFnSc1d1	lební
(	(	kIx(	(
<g/>
v	v	k7c6	v
ní	on	k3xPp3gFnSc6	on
je	být	k5eAaImIp3nS	být
uložen	uložen	k2eAgInSc1d1	uložen
mozek	mozek	k1gInSc1	mozek
<g/>
)	)	kIx)	)
Kostra	kostra	k1gFnSc1	kostra
lebky	lebka	k1gFnSc2	lebka
(	(	kIx(	(
<g/>
cranium	cranium	k1gNnSc1	cranium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
větším	veliký	k2eAgInSc7d2	veliký
počtem	počet	k1gInSc7	počet
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
dolní	dolní	k2eAgFnSc2d1	dolní
čelisti	čelist	k1gFnSc2	čelist
navzájem	navzájem	k6eAd1	navzájem
pevně	pevně	k6eAd1	pevně
spojeny	spojit	k5eAaPmNgInP	spojit
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Lebka	lebka	k1gFnSc1	lebka
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Lebka	lebka	k1gFnSc1	lebka
člověka	člověk	k1gMnSc2	člověk
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
22	[number]	k4	22
kostí	kost	k1gFnPc2	kost
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
novorozence	novorozenec	k1gMnSc2	novorozenec
jsou	být	k5eAaImIp3nP	být
kosti	kost	k1gFnPc1	kost
relativně	relativně	k6eAd1	relativně
volné	volný	k2eAgFnPc1d1	volná
-	-	kIx~	-
prostor	prostor	k1gInSc1	prostor
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
tvoří	tvořit	k5eAaImIp3nS	tvořit
pružné	pružný	k2eAgNnSc1d1	pružné
vazivové	vazivový	k2eAgInPc1d1	vazivový
lupínky	lupínek	k1gInPc1	lupínek
(	(	kIx(	(
<g/>
fontanely	fontanela	k1gFnPc1	fontanela
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
prvních	první	k4xOgNnPc2	první
2	[number]	k4	2
let	let	k1gInSc4	let
života	život	k1gInSc2	život
postupně	postupně	k6eAd1	postupně
zarůstají	zarůstat	k5eAaImIp3nP	zarůstat
(	(	kIx(	(
<g/>
osifikují	osifikovat	k5eAaBmIp3nP	osifikovat
<g/>
)	)	kIx)	)
a	a	k8xC	a
lebka	lebka	k1gFnSc1	lebka
tak	tak	k6eAd1	tak
srůstá	srůstat	k5eAaImIp3nS	srůstat
<g/>
.	.	kIx.	.
</s>
<s>
Cranium	Cranium	k1gNnSc1	Cranium
se	se	k3xPyFc4	se
občas	občas	k6eAd1	občas
také	také	k9	také
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
2	[number]	k4	2
důležité	důležitý	k2eAgInPc4d1	důležitý
celky	celek	k1gInPc4	celek
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
na	na	k7c4	na
NEUROCRANIUM	NEUROCRANIUM	kA	NEUROCRANIUM
<g/>
(	(	kIx(	(
<g/>
NCR	NCR	kA	NCR
<g/>
)	)	kIx)	)
a	a	k8xC	a
SPLANCHNOCRANIUM	SPLANCHNOCRANIUM	kA	SPLANCHNOCRANIUM
<g/>
(	(	kIx(	(
<g/>
SPCR	SPCR	kA	SPCR
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
NCR	NCR	kA	NCR
-	-	kIx~	-
důležitý	důležitý	k2eAgInSc4d1	důležitý
obal	obal	k1gInSc4	obal
okolo	okolo	k7c2	okolo
mozkové	mozkový	k2eAgFnSc2d1	mozková
kůry	kůra	k1gFnSc2	kůra
a	a	k8xC	a
částí	část	k1gFnSc7	část
mozku	mozek	k1gInSc2	mozek
<g/>
.	.	kIx.	.
vrstva	vrstva	k1gFnSc1	vrstva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
dává	dávat	k5eAaImIp3nS	dávat
signály	signál	k1gInPc4	signál
k	k	k7c3	k
podnětnému	podnětný	k2eAgNnSc3d1	podnětné
vyjadřování	vyjadřování	k1gNnSc3	vyjadřování
mozkových	mozkový	k2eAgFnPc2d1	mozková
činností	činnost	k1gFnPc2	činnost
<g/>
.	.	kIx.	.
</s>
<s>
SSPCR	SSPCR	kA	SSPCR
-	-	kIx~	-
vrstva	vrstva	k1gFnSc1	vrstva
<g/>
,	,	kIx,	,
již	jenž	k3xRgFnSc4	jenž
z	z	k7c2	z
kostí	kost	k1gFnPc2	kost
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
chrání	chránit	k5eAaImIp3nS	chránit
mozek	mozek	k1gInSc4	mozek
(	(	kIx(	(
lat.	lat.	k?	lat.
cerebrum	cerebrum	k1gInSc1	cerebrum
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lebka	lebka	k1gFnSc1	lebka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
lebka	lebka	k1gFnSc1	lebka
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
