<s>
Rekord	rekord	k1gInSc1	rekord
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
získaných	získaný	k2eAgFnPc2d1	získaná
medailí	medaile	k1gFnPc2	medaile
z	z	k7c2	z
paralympiád	paralympiáda	k1gFnPc2	paralympiáda
drží	držet	k5eAaImIp3nS	držet
Ragnhild	Ragnhild	k1gMnSc1	Ragnhild
Myklebustová	Myklebustová	k1gFnSc1	Myklebustová
z	z	k7c2	z
Norska	Norsko	k1gNnSc2	Norsko
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
na	na	k7c6	na
zimních	zimní	k2eAgFnPc6d1	zimní
paralympijských	paralympijský	k2eAgFnPc6d1	paralympijská
hrách	hra	k1gFnPc6	hra
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
a	a	k8xC	a
2002	[number]	k4	2002
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
severském	severský	k2eAgNnSc6d1	severské
lyžování	lyžování	k1gNnSc6	lyžování
dohromady	dohromady	k6eAd1	dohromady
22	[number]	k4	22
medailí	medaile	k1gFnPc2	medaile
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
17	[number]	k4	17
bylo	být	k5eAaImAgNnS	být
zlatých	zlatý	k2eAgNnPc2d1	Zlaté
<g/>
.	.	kIx.	.
</s>
