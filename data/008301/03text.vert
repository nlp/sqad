<p>
<s>
Lidice	Lidice	k1gInPc1	Lidice
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Liditz	Liditz	k1gInSc1	Liditz
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
obec	obec	k1gFnSc4	obec
ve	v	k7c6	v
Středočeském	středočeský	k2eAgInSc6d1	středočeský
kraji	kraj	k1gInSc6	kraj
<g/>
,	,	kIx,	,
20	[number]	k4	20
km	km	kA	km
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
v	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
Buštěhradu	Buštěhrad	k1gInSc2	Buštěhrad
<g/>
,	,	kIx,	,
poblíž	poblíž	k7c2	poblíž
Kladna	Kladno	k1gNnSc2	Kladno
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
dne	den	k1gInSc2	den
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1942	[number]	k4	1942
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
obec	obec	k1gFnSc1	obec
vyhlazena	vyhlazen	k2eAgFnSc1d1	vyhlazena
německými	německý	k2eAgMnPc7d1	německý
nacisty	nacista	k1gMnPc7	nacista
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
heydrichiády	heydrichiáda	k1gFnSc2	heydrichiáda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
starých	starý	k2eAgInPc2d1	starý
Lidic	Lidice	k1gInPc2	Lidice
zřízen	zřídit	k5eAaPmNgInS	zřídit
památník	památník	k1gInSc1	památník
obětem	oběť	k1gFnPc3	oběť
s	s	k7c7	s
muzeem	muzeum	k1gNnSc7	muzeum
připomínající	připomínající	k2eAgFnSc4d1	připomínající
tuto	tento	k3xDgFnSc4	tento
tragédii	tragédie	k1gFnSc4	tragédie
a	a	k8xC	a
obec	obec	k1gFnSc1	obec
obnovena	obnoven	k2eAgFnSc1d1	obnovena
o	o	k7c4	o
několik	několik	k4yIc4	několik
set	sto	k4xCgNnPc2	sto
metrů	metr	k1gInPc2	metr
dále	daleko	k6eAd2	daleko
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
581	[number]	k4	581
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
obce	obec	k1gFnSc2	obec
==	==	k?	==
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgFnSc1d3	nejstarší
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
vsi	ves	k1gFnSc6	ves
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1318	[number]	k4	1318
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
celá	celý	k2eAgNnPc4d1	celé
staletí	staletí	k1gNnPc4	staletí
byly	být	k5eAaImAgFnP	být
Lidice	Lidice	k1gInPc4	Lidice
obyčejnou	obyčejný	k2eAgFnSc7d1	obyčejná
nevelkou	velký	k2eNgFnSc7d1	nevelká
zemědělskou	zemědělský	k2eAgFnSc7d1	zemědělská
vsí	ves	k1gFnSc7	ves
v	v	k7c6	v
mělkém	mělký	k2eAgNnSc6d1	mělké
údolí	údolí	k1gNnSc6	údolí
Lidického	lidický	k2eAgInSc2d1	lidický
potoka	potok	k1gInSc2	potok
<g/>
,	,	kIx,	,
příslušející	příslušející	k2eAgFnSc1d1	příslušející
k	k	k7c3	k
buštěhradskému	buštěhradský	k2eAgNnSc3d1	Buštěhradské
panství	panství	k1gNnSc3	panství
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
památkou	památka	k1gFnSc7	památka
a	a	k8xC	a
dominantou	dominanta	k1gFnSc7	dominanta
obce	obec	k1gFnSc2	obec
byl	být	k5eAaImAgInS	být
barokní	barokní	k2eAgInSc1d1	barokní
farní	farní	k2eAgInSc1d1	farní
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Martina	Martin	k1gMnSc2	Martin
<g/>
.	.	kIx.	.
</s>
<s>
Významnější	významný	k2eAgInSc1d2	významnější
nárůst	nárůst	k1gInSc1	nárůst
obce	obec	k1gFnSc2	obec
přišel	přijít	k5eAaPmAgInS	přijít
až	až	k9	až
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
hornictví	hornictví	k1gNnSc2	hornictví
a	a	k8xC	a
hutnictví	hutnictví	k1gNnSc2	hutnictví
na	na	k7c6	na
Kladensku	Kladensko	k1gNnSc6	Kladensko
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
vsi	ves	k1gFnSc6	ves
Lidice	Lidice	k1gInPc4	Lidice
(	(	kIx(	(
<g/>
512	[number]	k4	512
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
poštovna	poštovna	k1gFnSc1	poštovna
<g/>
,	,	kIx,	,
katolický	katolický	k2eAgInSc4d1	katolický
kostel	kostel	k1gInSc4	kostel
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
evidovány	evidovat	k5eAaImNgFnP	evidovat
tyto	tento	k3xDgFnPc1	tento
živnosti	živnost	k1gFnPc1	živnost
a	a	k8xC	a
obchody	obchod	k1gInPc1	obchod
<g/>
:	:	kIx,	:
3	[number]	k4	3
hostince	hostinec	k1gInSc2	hostinec
<g/>
,	,	kIx,	,
kolář	kolář	k1gMnSc1	kolář
<g/>
,	,	kIx,	,
kovář	kovář	k1gMnSc1	kovář
<g/>
,	,	kIx,	,
krejčí	krejčí	k1gMnSc1	krejčí
<g/>
,	,	kIx,	,
mlýn	mlýn	k1gInSc1	mlýn
<g/>
,	,	kIx,	,
obuvník	obuvník	k1gMnSc1	obuvník
<g/>
,	,	kIx,	,
povozník	povozník	k1gMnSc1	povozník
<g/>
,	,	kIx,	,
8	[number]	k4	8
rolníků	rolník	k1gMnPc2	rolník
<g/>
,	,	kIx,	,
3	[number]	k4	3
obchody	obchod	k1gInPc4	obchod
se	s	k7c7	s
smíšeným	smíšený	k2eAgNnSc7d1	smíšené
zbožím	zboží	k1gNnSc7	zboží
<g/>
,	,	kIx,	,
trafika	trafika	k1gFnSc1	trafika
<g/>
,	,	kIx,	,
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
uhlím	uhlí	k1gNnSc7	uhlí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vyhlazení	vyhlazení	k1gNnSc4	vyhlazení
obce	obec	k1gFnPc1	obec
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
atentátu	atentát	k1gInSc6	atentát
na	na	k7c4	na
zastupujícího	zastupující	k2eAgMnSc2d1	zastupující
německého	německý	k2eAgMnSc2d1	německý
říšského	říšský	k2eAgMnSc2d1	říšský
protektora	protektor	k1gMnSc2	protektor
Reinharda	Reinhard	k1gMnSc2	Reinhard
Heydricha	Heydrich	k1gMnSc2	Heydrich
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
byly	být	k5eAaImAgInP	být
Lidice	Lidice	k1gInPc1	Lidice
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
represí	represe	k1gFnPc2	represe
nazývaných	nazývaný	k2eAgFnPc2d1	nazývaná
heydrichiáda	heydrichiáda	k1gFnSc1	heydrichiáda
vybrány	vybrat	k5eAaPmNgFnP	vybrat
nacistickým	nacistický	k2eAgNnSc7d1	nacistické
vedením	vedení	k1gNnSc7	vedení
pro	pro	k7c4	pro
exemplární	exemplární	k2eAgInSc4d1	exemplární
kolektivní	kolektivní	k2eAgInSc4d1	kolektivní
trest	trest	k1gInSc4	trest
<g/>
.	.	kIx.	.
</s>
<s>
Výběr	výběr	k1gInSc1	výběr
Lidic	Lidice	k1gInPc2	Lidice
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
náhodný	náhodný	k2eAgInSc1d1	náhodný
<g/>
,	,	kIx,	,
podezření	podezření	k1gNnPc1	podezření
na	na	k7c4	na
spojení	spojení	k1gNnSc4	spojení
obyvatel	obyvatel	k1gMnPc2	obyvatel
s	s	k7c7	s
atentátem	atentát	k1gInSc7	atentát
bylo	být	k5eAaImAgNnS	být
jen	jen	k9	jen
záminkou	záminka	k1gFnSc7	záminka
a	a	k8xC	a
vycházelo	vycházet	k5eAaImAgNnS	vycházet
z	z	k7c2	z
nevýznamného	významný	k2eNgNnSc2d1	nevýznamné
a	a	k8xC	a
neprokázaného	prokázaný	k2eNgNnSc2d1	neprokázané
podezření	podezření	k1gNnSc2	podezření
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vyhlazení	vyhlazení	k1gNnSc3	vyhlazení
Lidic	Lidice	k1gInPc2	Lidice
–	–	k?	–
srovnání	srovnání	k1gNnSc4	srovnání
obce	obec	k1gFnSc2	obec
čítající	čítající	k2eAgFnSc2d1	čítající
104	[number]	k4	104
domů	dům	k1gInPc2	dům
s	s	k7c7	s
503	[number]	k4	503
obyvateli	obyvatel	k1gMnPc7	obyvatel
se	se	k3xPyFc4	se
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
bylo	být	k5eAaImAgNnS	být
popraveno	popraven	k2eAgNnSc1d1	popraveno
nebo	nebo	k8xC	nebo
odvlečeno	odvlečen	k2eAgNnSc1d1	odvlečen
do	do	k7c2	do
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
táborů	tábor	k1gInPc2	tábor
<g/>
,	,	kIx,	,
menší	malý	k2eAgFnSc1d2	menší
část	část	k1gFnSc1	část
dětí	dítě	k1gFnPc2	dítě
byla	být	k5eAaImAgFnS	být
určena	určit	k5eAaPmNgFnS	určit
k	k	k7c3	k
poněmčení	poněmčení	k1gNnSc3	poněmčení
v	v	k7c6	v
Říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
vrátilo	vrátit	k5eAaPmAgNnS	vrátit
143	[number]	k4	143
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
po	po	k7c6	po
usilovném	usilovný	k2eAgNnSc6d1	usilovné
pátrání	pátrání	k1gNnSc6	pátrání
i	i	k9	i
17	[number]	k4	17
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zrušení	zrušení	k1gNnSc1	zrušení
obce	obec	k1gFnSc2	obec
po	po	k7c6	po
vyhlazení	vyhlazení	k1gNnSc6	vyhlazení
===	===	k?	===
</s>
</p>
<p>
<s>
Obec	obec	k1gFnSc1	obec
byla	být	k5eAaImAgFnS	být
zrušena	zrušit	k5eAaPmNgFnS	zrušit
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
katastr	katastr	k1gInSc1	katastr
připojen	připojit	k5eAaPmNgInS	připojit
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
ministerstva	ministerstvo	k1gNnSc2	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
ze	z	k7c2	z
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1942	[number]	k4	1942
nejprve	nejprve	k6eAd1	nejprve
k	k	k7c3	k
sousední	sousední	k2eAgFnSc3d1	sousední
Hřebči	Hřebč	k1gFnSc3	Hřebč
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1942	[number]	k4	1942
ministerstvo	ministerstvo	k1gNnSc4	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
vydalo	vydat	k5eAaPmAgNnS	vydat
nové	nový	k2eAgNnSc4d1	nové
rozhodnutí	rozhodnutí	k1gNnSc4	rozhodnutí
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
území	území	k1gNnSc4	území
obce	obec	k1gFnSc2	obec
Lidice	Lidice	k1gInPc4	Lidice
připojilo	připojit	k5eAaPmAgNnS	připojit
k	k	k7c3	k
Buštěhradu	Buštěhrad	k1gInSc3	Buštěhrad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nové	Nové	k2eAgInPc1d1	Nové
Lidice	Lidice	k1gInPc1	Lidice
===	===	k?	===
</s>
</p>
<p>
<s>
Po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
byla	být	k5eAaImAgFnS	být
obec	obec	k1gFnSc1	obec
obnovena	obnoven	k2eAgFnSc1d1	obnovena
<g/>
,	,	kIx,	,
z	z	k7c2	z
pietních	pietní	k2eAgInPc2d1	pietní
důvodů	důvod	k1gInPc2	důvod
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
nově	nově	k6eAd1	nově
postavena	postaven	k2eAgFnSc1d1	postavena
na	na	k7c6	na
jiném	jiný	k2eAgNnSc6d1	jiné
místě	místo	k1gNnSc6	místo
nedaleko	daleko	k6eNd1	daleko
od	od	k7c2	od
obce	obec	k1gFnSc2	obec
původní	původní	k2eAgFnSc2d1	původní
<g/>
,	,	kIx,	,
na	na	k7c6	na
původním	původní	k2eAgNnSc6d1	původní
místě	místo	k1gNnSc6	místo
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
nachází	nacházet	k5eAaImIp3nP	nacházet
Památník	památník	k1gInSc4	památník
Lidice	Lidice	k1gInPc1	Lidice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Územněsprávní	územněsprávní	k2eAgNnSc4d1	územněsprávní
začlenění	začlenění	k1gNnSc4	začlenění
===	===	k?	===
</s>
</p>
<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
územněsprávního	územněsprávní	k2eAgNnSc2d1	územněsprávní
začleňování	začleňování	k1gNnSc2	začleňování
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
období	období	k1gNnSc4	období
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1850	[number]	k4	1850
do	do	k7c2	do
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
chronologickém	chronologický	k2eAgInSc6d1	chronologický
přehledu	přehled	k1gInSc6	přehled
je	být	k5eAaImIp3nS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
územně	územně	k6eAd1	územně
administrativní	administrativní	k2eAgFnSc4d1	administrativní
příslušnost	příslušnost	k1gFnSc4	příslušnost
obce	obec	k1gFnSc2	obec
v	v	k7c6	v
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
došlo	dojít	k5eAaPmAgNnS	dojít
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
1850	[number]	k4	1850
–	–	k?	–
země	země	k1gFnSc1	země
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
okres	okres	k1gInSc1	okres
Smíchov	Smíchov	k1gInSc1	Smíchov
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Unhošť	Unhošť	k1gFnSc1	Unhošť
</s>
</p>
<p>
<s>
1855	[number]	k4	1855
–	–	k?	–
země	země	k1gFnSc1	země
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
kraj	kraj	k1gInSc1	kraj
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Unhošť	Unhošť	k1gFnSc1	Unhošť
</s>
</p>
<p>
<s>
1868	[number]	k4	1868
–	–	k?	–
země	země	k1gFnSc1	země
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
okres	okres	k1gInSc1	okres
Smíchov	Smíchov	k1gInSc1	Smíchov
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Unhošť	Unhošť	k1gFnSc1	Unhošť
</s>
</p>
<p>
<s>
1893	[number]	k4	1893
–	–	k?	–
země	země	k1gFnSc1	země
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc1d1	politický
okres	okres	k1gInSc1	okres
Kladno	Kladno	k1gNnSc4	Kladno
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Unhošť	Unhošť	k1gFnSc1	Unhošť
</s>
</p>
<p>
<s>
1939	[number]	k4	1939
–	–	k?	–
země	země	k1gFnSc1	země
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
Oberlandrat	Oberlandrat	k1gInSc1	Oberlandrat
Kladno	Kladno	k1gNnSc4	Kladno
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc4d1	politický
okres	okres	k1gInSc4	okres
Kladno	Kladno	k1gNnSc1	Kladno
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Unhošť	Unhošť	k1gFnSc1	Unhošť
</s>
</p>
<p>
<s>
1942	[number]	k4	1942
–	–	k?	–
země	země	k1gFnSc1	země
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
Oberlandrat	Oberlandrat	k1gInSc1	Oberlandrat
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
politický	politický	k2eAgInSc4d1	politický
okres	okres	k1gInSc4	okres
Kladno	Kladno	k1gNnSc1	Kladno
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Unhošť	Unhošť	k1gFnSc1	Unhošť
</s>
</p>
<p>
<s>
1945	[number]	k4	1945
–	–	k?	–
země	země	k1gFnSc1	země
česká	český	k2eAgFnSc1d1	Česká
<g/>
,	,	kIx,	,
správní	správní	k2eAgInSc1d1	správní
okres	okres	k1gInSc1	okres
Kladno	Kladno	k1gNnSc4	Kladno
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgInSc1d1	soudní
okres	okres	k1gInSc1	okres
Unhošť	Unhošť	k1gFnSc1	Unhošť
</s>
</p>
<p>
<s>
1949	[number]	k4	1949
–	–	k?	–
Pražský	pražský	k2eAgInSc1d1	pražský
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Kladno	Kladno	k1gNnSc1	Kladno
</s>
</p>
<p>
<s>
1960	[number]	k4	1960
–	–	k?	–
Středočeský	středočeský	k2eAgInSc1d1	středočeský
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
okres	okres	k1gInSc1	okres
Kladno	Kladno	k1gNnSc1	Kladno
</s>
</p>
<p>
<s>
==	==	k?	==
Doprava	doprava	k1gFnSc1	doprava
==	==	k?	==
</s>
</p>
<p>
<s>
Silniční	silniční	k2eAgFnSc1d1	silniční
doprava	doprava	k1gFnSc1	doprava
–	–	k?	–
Do	do	k7c2	do
obce	obec	k1gFnSc2	obec
vedou	vést	k5eAaImIp3nP	vést
silnice	silnice	k1gFnPc1	silnice
III	III	kA	III
<g/>
.	.	kIx.	.
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
hranici	hranice	k1gFnSc6	hranice
katastru	katastr	k1gInSc2	katastr
obce	obec	k1gFnSc2	obec
vede	vést	k5eAaImIp3nS	vést
silnice	silnice	k1gFnSc1	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
61	[number]	k4	61
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
Kladno	Kladno	k1gNnSc4	Kladno
s	s	k7c7	s
dálnicí	dálnice	k1gFnSc7	dálnice
D7	D7	k1gFnSc1	D7
Praha	Praha	k1gFnSc1	Praha
–	–	k?	–
Slaný	Slaný	k1gInSc1	Slaný
–	–	k?	–
Louny	Louny	k1gInPc1	Louny
–	–	k?	–
Chomutov	Chomutov	k1gInSc1	Chomutov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
doprava	doprava	k1gFnSc1	doprava
–	–	k?	–
Železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
ani	ani	k8xC	ani
stanice	stanice	k1gFnSc1	stanice
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
nejsou	být	k5eNaImIp3nP	být
<g/>
.	.	kIx.	.
</s>
<s>
Nejblíže	blízce	k6eAd3	blízce
obci	obec	k1gFnSc3	obec
je	být	k5eAaImIp3nS	být
železniční	železniční	k2eAgFnSc1d1	železniční
stanice	stanice	k1gFnSc1	stanice
Středokluky	Středokluk	k1gInPc1	Středokluk
ve	v	k7c6	v
vzdálenosti	vzdálenost	k1gFnSc6	vzdálenost
5	[number]	k4	5
km	km	kA	km
ležící	ležící	k2eAgFnSc1d1	ležící
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
121	[number]	k4	121
z	z	k7c2	z
Hostivic	Hostivice	k1gFnPc2	Hostivice
do	do	k7c2	do
Podlešína	Podlešín	k1gInSc2	Podlešín
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
–	–	k?	–
V	v	k7c6	v
obci	obec	k1gFnSc6	obec
zastavovaly	zastavovat	k5eAaImAgInP	zastavovat
v	v	k7c6	v
září	září	k1gNnSc6	září
2011	[number]	k4	2011
autobusové	autobusový	k2eAgFnPc1d1	autobusová
linky	linka	k1gFnPc1	linka
jedoucí	jedoucí	k2eAgFnPc1d1	jedoucí
do	do	k7c2	do
těchto	tento	k3xDgInPc2	tento
cílů	cíl	k1gInPc2	cíl
<g/>
:	:	kIx,	:
Kladno	Kladno	k1gNnSc1	Kladno
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
Středokluky	Středokluk	k1gInPc1	Středokluk
<g/>
,	,	kIx,	,
Tuchoměřice	Tuchoměřice	k1gFnSc1	Tuchoměřice
(	(	kIx(	(
<g/>
dopravce	dopravce	k1gMnSc1	dopravce
ČSAD	ČSAD	kA	ČSAD
MHD	MHD	kA	MHD
Kladno	Kladno	k1gNnSc4	Kladno
<g/>
,	,	kIx,	,
a.	a.	k?	a.
s.	s.	k?	s.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Partnerská	partnerský	k2eAgNnPc1d1	partnerské
města	město	k1gNnPc1	město
==	==	k?	==
</s>
</p>
<p>
<s>
Coventry	Coventr	k1gInPc1	Coventr
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
(	(	kIx(	(
<g/>
1947	[number]	k4	1947
<g/>
)	)	kIx)	)
–	–	k?	–
ve	v	k7c6	v
městě	město	k1gNnSc6	město
je	být	k5eAaImIp3nS	být
náměstí	náměstí	k1gNnSc1	náměstí
"	"	kIx"	"
<g/>
Lidice	Lidice	k1gInPc4	Lidice
square	square	k1gInSc1	square
<g/>
"	"	kIx"	"
–	–	k?	–
pojmenované	pojmenovaný	k2eAgFnPc4d1	pojmenovaná
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
partnerství	partnerství	k1gNnPc2	partnerství
mezi	mezi	k7c7	mezi
městy	město	k1gNnPc7	město
kvůli	kvůli	k7c3	kvůli
podobnému	podobný	k2eAgInSc3d1	podobný
osudu	osud	k1gInSc3	osud
za	za	k7c4	za
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Fotogalerie	Fotogalerie	k1gFnSc2	Fotogalerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Ležáky	Ležáky	k1gInPc1	Ležáky
</s>
</p>
<p>
<s>
Český	český	k2eAgInSc1d1	český
Malín	Malín	k1gInSc1	Malín
</s>
</p>
<p>
<s>
Ploština	ploština	k1gFnSc1	ploština
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Štemberka	Štemberka	k1gFnSc1	Štemberka
</s>
</p>
<p>
<s>
František	František	k1gMnSc1	František
Saidl	Saidl	k1gMnSc1	Saidl
</s>
</p>
<p>
<s>
Lidický	lidický	k2eAgInSc1d1	lidický
okruh	okruh	k1gInSc1	okruh
</s>
</p>
<p>
<s>
Aféra	aféra	k1gFnSc1	aféra
lodi	loď	k1gFnSc2	loď
Lidice	Lidice	k1gInPc1	Lidice
</s>
</p>
<p>
<s>
Lidická	lidický	k2eAgFnSc1d1	Lidická
hrušeň	hrušeň	k1gFnSc1	hrušeň
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
Lidice	Lidice	k1gInPc4	Lidice
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Lidice	Lidice	k1gInPc4	Lidice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Lidice	Lidice	k1gInPc4	Lidice
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Lidice	Lidice	k1gInPc1	Lidice
ve	v	k7c6	v
Vlastenském	vlastenský	k2eAgInSc6d1	vlastenský
slovníku	slovník	k1gInSc6	slovník
historickém	historický	k2eAgInSc6d1	historický
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
<p>
<s>
Územně	územně	k6eAd1	územně
identifikační	identifikační	k2eAgInSc1d1	identifikační
registr	registr	k1gInSc1	registr
ČR	ČR	kA	ČR
<g/>
.	.	kIx.	.
</s>
<s>
Obec	obec	k1gFnSc1	obec
Lidice	Lidice	k1gInPc1	Lidice
v	v	k7c6	v
Územně	územně	k6eAd1	územně
identifikačním	identifikační	k2eAgInSc6d1	identifikační
registru	registr	k1gInSc6	registr
ČR	ČR	kA	ČR
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obce	obec	k1gFnPc1	obec
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
