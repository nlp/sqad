<s>
Plody	plod	k1gInPc1	plod
jsou	být	k5eAaImIp3nP	být
vejčité	vejčitý	k2eAgFnPc1d1	vejčitá
nebo	nebo	k8xC	nebo
podlouhlé	podlouhlý	k2eAgFnPc1d1	podlouhlá
olivověhnědé	olivověhnědý	k2eAgFnPc1d1	olivověhnědý
dvounažky	dvounažka	k1gFnPc1	dvounažka
se	s	k7c7	s
světlými	světlý	k2eAgInPc7d1	světlý
žebry	žebr	k1gInPc7	žebr
<g/>
,	,	kIx,	,
bývají	bývat	k5eAaImIp3nP	bývat
5	[number]	k4	5
až	až	k9	až
10	[number]	k4	10
mm	mm	kA	mm
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
a	a	k8xC	a
3	[number]	k4	3
až	až	k9	až
4	[number]	k4	4
mm	mm	kA	mm
široké	široký	k2eAgNnSc1d1	široké
<g/>
.	.	kIx.	.
</s>
