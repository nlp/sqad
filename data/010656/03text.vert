<p>
<s>
Timoj	Timoj	k1gInSc1	Timoj
trojlaločný	trojlaločný	k2eAgInSc1d1	trojlaločný
(	(	kIx(	(
<g/>
Laser	laser	k1gInSc1	laser
trilobum	trilobum	k1gInSc1	trilobum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jediný	jediný	k2eAgInSc1d1	jediný
popsaný	popsaný	k2eAgInSc1d1	popsaný
druh	druh	k1gInSc1	druh
bílé	bílý	k2eAgFnSc2d1	bílá
kvetoucí	kvetoucí	k2eAgFnSc2d1	kvetoucí
byliny	bylina	k1gFnSc2	bylina
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
timoj	timoj	k1gInSc1	timoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
rostlina	rostlina	k1gFnSc1	rostlina
z	z	k7c2	z
čeledě	čeleď	k1gFnSc2	čeleď
miřikovitých	miřikovitý	k2eAgMnPc2d1	miřikovitý
velice	velice	k6eAd1	velice
vzácná	vzácný	k2eAgFnSc1d1	vzácná
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
kriticky	kriticky	k6eAd1	kriticky
ohroženou	ohrožený	k2eAgFnSc4d1	ohrožená
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Druh	druh	k1gInSc1	druh
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
rozšířen	rozšířen	k2eAgMnSc1d1	rozšířen
pouze	pouze	k6eAd1	pouze
ostrůvkovitě	ostrůvkovitě	k6eAd1	ostrůvkovitě
<g/>
,	,	kIx,	,
hojnější	hojný	k2eAgMnSc1d2	hojnější
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
<g/>
,	,	kIx,	,
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
a	a	k8xC	a
v	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
části	část	k1gFnSc6	část
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
na	na	k7c6	na
Blízkém	blízký	k2eAgInSc6d1	blízký
východě	východ	k1gInSc6	východ
i	i	k8xC	i
v	v	k7c6	v
okolí	okolí	k1gNnSc6	okolí
Kavkazu	Kavkaz	k1gInSc2	Kavkaz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jeho	jeho	k3xOp3gNnPc1	jeho
stanoviště	stanoviště	k1gNnPc1	stanoviště
bývají	bývat	k5eAaImIp3nP	bývat
nejčastěji	často	k6eAd3	často
ve	v	k7c6	v
světlých	světlý	k2eAgInPc6d1	světlý
listnatých	listnatý	k2eAgInPc6d1	listnatý
lesích	les	k1gInPc6	les
teplých	teplý	k2eAgFnPc2d1	teplá
pahorkatin	pahorkatina	k1gFnPc2	pahorkatina
<g/>
,	,	kIx,	,
snáší	snášet	k5eAaImIp3nS	snášet
i	i	k9	i
polostín	polostín	k1gInSc1	polostín
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
alkalitu	alkalita	k1gFnSc4	alkalita
půdy	půda	k1gFnSc2	půda
není	být	k5eNaImIp3nS	být
náročný	náročný	k2eAgInSc1d1	náročný
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
však	však	k9	však
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
hlinitá	hlinitý	k2eAgFnSc1d1	hlinitá
a	a	k8xC	a
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
vlhká	vlhký	k2eAgFnSc1d1	vlhká
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
jediném	jediný	k2eAgNnSc6d1	jediné
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
v	v	k7c6	v
Diváckém	divácký	k2eAgInSc6d1	divácký
lese	les	k1gInSc6	les
mezi	mezi	k7c7	mezi
obcemi	obec	k1gFnPc7	obec
Diváky	Diváky	k1gInPc4	Diváky
a	a	k8xC	a
Kurdějov	Kurdějov	k1gInSc4	Kurdějov
nedaleko	nedaleko	k7c2	nedaleko
Hustopečí	Hustopeč	k1gFnPc2	Hustopeč
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Tento	tento	k3xDgInSc1	tento
hemikryptofyt	hemikryptofyt	k1gInSc1	hemikryptofyt
je	být	k5eAaImIp3nS	být
vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
rostlina	rostlina	k1gFnSc1	rostlina
s	s	k7c7	s
typickou	typický	k2eAgFnSc7d1	typická
vůni	vůně	k1gFnSc6	vůně
kmínu	kmín	k1gInSc2	kmín
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
60	[number]	k4	60
až	až	k9	až
120	[number]	k4	120
cm	cm	kA	cm
vysoká	vysoký	k2eAgFnSc1d1	vysoká
lodyha	lodyha	k1gFnSc1	lodyha
vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
z	z	k7c2	z
dužnatého	dužnatý	k2eAgNnSc2d1	dužnaté
<g/>
,	,	kIx,	,
hluboko	hluboko	k6eAd1	hluboko
sahajícího	sahající	k2eAgInSc2d1	sahající
oddenku	oddenek	k1gInSc2	oddenek
<g/>
.	.	kIx.	.
</s>
<s>
Lodyha	lodyha	k1gFnSc1	lodyha
je	být	k5eAaImIp3nS	být
lysá	lysý	k2eAgFnSc1d1	Lysá
<g/>
,	,	kIx,	,
jemně	jemně	k6eAd1	jemně
rýhovaná	rýhovaný	k2eAgFnSc1d1	rýhovaná
<g/>
,	,	kIx,	,
lehce	lehko	k6eAd1	lehko
ojíněná	ojíněný	k2eAgFnSc1d1	ojíněná
a	a	k8xC	a
nahoře	nahoře	k6eAd1	nahoře
se	se	k3xPyFc4	se
větví	větvit	k5eAaImIp3nS	větvit
<g/>
.	.	kIx.	.
</s>
<s>
Přízemní	přízemní	k2eAgInPc1d1	přízemní
listy	list	k1gInPc1	list
s	s	k7c7	s
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
oblými	oblý	k2eAgInPc7d1	oblý
řapíky	řapík	k1gInPc7	řapík
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
trojčetné	trojčetný	k2eAgInPc1d1	trojčetný
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc4	jejich
lístky	lístek	k1gInPc4	lístek
s	s	k7c7	s
dlouhými	dlouhý	k2eAgFnPc7d1	dlouhá
stopkami	stopka	k1gFnPc7	stopka
jsou	být	k5eAaImIp3nP	být
opět	opět	k6eAd1	opět
hluboce	hluboko	k6eAd1	hluboko
trojdílné	trojdílný	k2eAgInPc1d1	trojdílný
nebo	nebo	k8xC	nebo
trojklané	trojklaný	k2eAgInPc1d1	trojklaný
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc1	jejich
úkrojky	úkrojek	k1gInPc1	úkrojek
jsou	být	k5eAaImIp3nP	být
po	po	k7c6	po
obvodě	obvod	k1gInSc6	obvod
pravidelně	pravidelně	k6eAd1	pravidelně
vroubkované	vroubkovaný	k2eAgFnPc1d1	vroubkovaná
<g/>
.	.	kIx.	.
</s>
<s>
Lodyžní	lodyžní	k2eAgInPc1d1	lodyžní
listy	list	k1gInPc1	list
mají	mít	k5eAaImIp3nP	mít
nafouklé	nafouklý	k2eAgFnPc4d1	nafouklá
pochvy	pochva	k1gFnPc4	pochva
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
méně	málo	k6eAd2	málo
členité	členitý	k2eAgInPc1d1	členitý
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
jen	jen	k9	jen
jednoduše	jednoduše	k6eAd1	jednoduše
trojsečné	trojsečný	k2eAgNnSc1d1	trojsečný
s	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
lístky	lístek	k1gInPc7	lístek
nebo	nebo	k8xC	nebo
pouze	pouze	k6eAd1	pouze
trojlaločné	trojlaločný	k2eAgNnSc1d1	trojlaločné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Složený	složený	k2eAgInSc1d1	složený
okolík	okolík	k1gInSc1	okolík
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
11	[number]	k4	11
až	až	k9	až
20	[number]	k4	20
okolíčků	okolíček	k1gInPc2	okolíček
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
obal	obal	k1gInSc1	obal
chybí	chybit	k5eAaPmIp3nS	chybit
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
tvořen	tvořit	k5eAaImNgInS	tvořit
pouze	pouze	k6eAd1	pouze
1	[number]	k4	1
či	či	k8xC	či
2	[number]	k4	2
listeny	listen	k1gInPc7	listen
<g/>
,	,	kIx,	,
obalíčky	obalíček	k1gInPc7	obalíček
pak	pak	k6eAd1	pak
několika	několik	k4yIc7	několik
opadavými	opadavý	k2eAgMnPc7d1	opadavý
dlouze	dlouho	k6eAd1	dlouho
kopinatými	kopinatý	k2eAgInPc7d1	kopinatý
listeny	listen	k1gInPc7	listen
<g/>
.	.	kIx.	.
</s>
<s>
Protandrické	protandrický	k2eAgInPc1d1	protandrický
květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
převážně	převážně	k6eAd1	převážně
oboupohlavné	oboupohlavný	k2eAgFnPc1d1	oboupohlavná
<g/>
,	,	kIx,	,
v	v	k7c6	v
menším	malý	k2eAgInSc6d2	menší
počtu	počet	k1gInSc6	počet
samčí	samčí	k2eAgFnSc2d1	samčí
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
rozvíjejí	rozvíjet	k5eAaImIp3nP	rozvíjet
nejdříve	dříve	k6eAd3	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Drobné	drobné	k1gInPc1	drobné
<g/>
,	,	kIx,	,
vytrvalé	vytrvalý	k2eAgInPc1d1	vytrvalý
trojúhelníkovité	trojúhelníkovitý	k2eAgInPc1d1	trojúhelníkovitý
kališní	kališní	k2eAgInPc1d1	kališní
lístky	lístek	k1gInPc1	lístek
jsou	být	k5eAaImIp3nP	být
zašpičatělé	zašpičatělý	k2eAgInPc1d1	zašpičatělý
<g/>
.	.	kIx.	.
</s>
<s>
Vejčité	vejčitý	k2eAgInPc1d1	vejčitý
bílé	bílý	k2eAgInPc1d1	bílý
korunní	korunní	k2eAgInPc1d1	korunní
lístky	lístek	k1gInPc1	lístek
mají	mít	k5eAaImIp3nP	mít
své	svůj	k3xOyFgInPc4	svůj
konce	konec	k1gInPc4	konec
dovnitř	dovnitř	k6eAd1	dovnitř
ohnuté	ohnutý	k2eAgFnPc4d1	ohnutá
<g/>
.	.	kIx.	.
</s>
<s>
Rozkvétá	rozkvétat	k5eAaImIp3nS	rozkvétat
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
až	až	k8xS	až
červnu	červen	k1gInSc6	červen
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
opylení	opylení	k1gNnSc3	opylení
dochází	docházet	k5eAaImIp3nS	docházet
geitonogamicky	geitonogamicky	k6eAd1	geitonogamicky
nebo	nebo	k8xC	nebo
entomogamicky	entomogamicky	k6eAd1	entomogamicky
<g/>
,	,	kIx,	,
květy	květ	k1gInPc1	květ
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
opylovačům	opylovač	k1gMnPc3	opylovač
nektar	nektar	k1gInSc4	nektar
<g/>
.	.	kIx.	.
</s>
<s>
Ploidie	Ploidie	k1gFnSc1	Ploidie
je	být	k5eAaImIp3nS	být
n	n	k0	n
=	=	kIx~	=
22	[number]	k4	22
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Plody	plod	k1gInPc1	plod
jsou	být	k5eAaImIp3nP	být
vejčité	vejčitý	k2eAgFnPc1d1	vejčitá
nebo	nebo	k8xC	nebo
podlouhlé	podlouhlý	k2eAgFnPc1d1	podlouhlá
olivověhnědé	olivověhnědý	k2eAgFnPc1d1	olivověhnědý
dvounažky	dvounažka	k1gFnPc1	dvounažka
se	s	k7c7	s
světlými	světlý	k2eAgInPc7d1	světlý
žebry	žebr	k1gInPc7	žebr
<g/>
,	,	kIx,	,
bývají	bývat	k5eAaImIp3nP	bývat
5	[number]	k4	5
až	až	k9	až
10	[number]	k4	10
mm	mm	kA	mm
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
a	a	k8xC	a
3	[number]	k4	3
až	až	k9	až
4	[number]	k4	4
mm	mm	kA	mm
široké	široký	k2eAgNnSc1d1	široké
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
hřbetu	hřbet	k1gInSc2	hřbet
jsou	být	k5eAaImIp3nP	být
znatelně	znatelně	k6eAd1	znatelně
smáčknuté	smáčknutý	k2eAgInPc1d1	smáčknutý
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
patrný	patrný	k2eAgInSc1d1	patrný
5	[number]	k4	5
<g/>
zubý	zubý	k?	zubý
kalich	kalich	k1gInSc1	kalich
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
každém	každý	k3xTgInSc6	každý
plůdku	plůdek	k1gInSc6	plůdek
se	se	k3xPyFc4	se
podélně	podélně	k6eAd1	podélně
rýsuje	rýsovat	k5eAaImIp3nS	rýsovat
5	[number]	k4	5
hlavních	hlavní	k2eAgMnPc2d1	hlavní
a	a	k8xC	a
4	[number]	k4	4
vedlejší	vedlejší	k2eAgFnPc4d1	vedlejší
nitkovitá	nitkovitý	k2eAgNnPc4d1	nitkovitý
žebra	žebro	k1gNnPc4	žebro
<g/>
.	.	kIx.	.
</s>
<s>
Semeno	semeno	k1gNnSc1	semeno
bývá	bývat	k5eAaImIp3nS	bývat
dlouhé	dlouhý	k2eAgNnSc4d1	dlouhé
7	[number]	k4	7
mm	mm	kA	mm
a	a	k8xC	a
široké	široký	k2eAgInPc1d1	široký
3	[number]	k4	3
mm	mm	kA	mm
<g/>
,	,	kIx,	,
hmotnost	hmotnost	k1gFnSc1	hmotnost
tisíce	tisíc	k4xCgInPc1	tisíc
semen	semeno	k1gNnPc2	semeno
je	být	k5eAaImIp3nS	být
okolo	okolo	k7c2	okolo
19	[number]	k4	19
gramů	gram	k1gInPc2	gram
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ohrožení	ohrožení	k1gNnSc1	ohrožení
==	==	k?	==
</s>
</p>
<p>
<s>
S	s	k7c7	s
ohledem	ohled	k1gInSc7	ohled
na	na	k7c4	na
obzvláště	obzvláště	k6eAd1	obzvláště
malý	malý	k2eAgInSc4d1	malý
areál	areál	k1gInSc4	areál
výskytu	výskyt	k1gInSc2	výskyt
byl	být	k5eAaImAgInS	být
timoj	timoj	k1gInSc1	timoj
trojlaločný	trojlaločný	k2eAgInSc1d1	trojlaločný
"	"	kIx"	"
<g/>
Vyhláškou	vyhláška	k1gFnSc7	vyhláška
MŽP	MŽP	kA	MŽP
ČR	ČR	kA	ČR
č.	č.	k?	č.
395	[number]	k4	395
<g/>
/	/	kIx~	/
<g/>
1992	[number]	k4	1992
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
ve	v	k7c6	v
znění	znění	k1gNnSc6	znění
vyhl	vyhla	k1gFnPc2	vyhla
<g/>
.	.	kIx.	.
č.	č.	k?	č.
175	[number]	k4	175
<g/>
/	/	kIx~	/
<g/>
2006	[number]	k4	2006
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
i	i	k9	i
"	"	kIx"	"
<g/>
Červeným	červený	k2eAgInSc7d1	červený
seznamem	seznam	k1gInSc7	seznam
cévnatých	cévnatý	k2eAgFnPc2d1	cévnatá
rostlin	rostlina	k1gFnPc2	rostlina
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
<g/>
"	"	kIx"	"
prohlášen	prohlásit	k5eAaPmNgInS	prohlásit
za	za	k7c4	za
druh	druh	k1gInSc4	druh
kriticky	kriticky	k6eAd1	kriticky
ohrožený	ohrožený	k2eAgMnSc1d1	ohrožený
(	(	kIx(	(
<g/>
§	§	k?	§
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
1	[number]	k4	1
<g/>
r	r	kA	r
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
timoj	timoj	k1gInSc1	timoj
trojlaločný	trojlaločný	k2eAgMnSc1d1	trojlaločný
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Botanický	botanický	k2eAgInSc1d1	botanický
ústav	ústav	k1gInSc1	ústav
AV	AV	kA	AV
ČR	ČR	kA	ČR
–	–	k?	–
rozšíření	rozšíření	k1gNnSc1	rozšíření
timoje	timoj	k1gInSc2	timoj
trojlaločného	trojlaločný	k2eAgInSc2d1	trojlaločný
v	v	k7c6	v
ČR	ČR	kA	ČR
</s>
</p>
