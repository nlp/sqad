<s>
Aero	aero	k1gNnSc1	aero
L-39	L-39	k1gMnSc1	L-39
Albatros	albatros	k1gMnSc1	albatros
je	být	k5eAaImIp3nS	být
proudový	proudový	k2eAgInSc4d1	proudový
podzvukový	podzvukový	k2eAgInSc4d1	podzvukový
cvičný	cvičný	k2eAgInSc4d1	cvičný
letoun	letoun	k1gInSc4	letoun
vyvinutý	vyvinutý	k2eAgInSc4d1	vyvinutý
v	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
československém	československý	k2eAgNnSc6d1	Československé
Aeru	aero	k1gNnSc6	aero
Vodochody	Vodochod	k1gInPc1	Vodochod
<g/>
,	,	kIx,	,
hlavním	hlavní	k2eAgMnSc7d1	hlavní
konstruktérem	konstruktér	k1gMnSc7	konstruktér
Ing.	ing.	kA	ing.
Janem	Jan	k1gMnSc7	Jan
Vlčkem	Vlček	k1gMnSc7	Vlček
<g/>
.	.	kIx.	.
</s>
