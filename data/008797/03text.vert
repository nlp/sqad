<p>
<s>
Aero	aero	k1gNnSc1	aero
L-39	L-39	k1gMnSc1	L-39
Albatros	albatros	k1gMnSc1	albatros
je	být	k5eAaImIp3nS	být
proudový	proudový	k2eAgInSc4d1	proudový
podzvukový	podzvukový	k2eAgInSc4d1	podzvukový
cvičný	cvičný	k2eAgInSc4d1	cvičný
letoun	letoun	k1gInSc4	letoun
vyvinutý	vyvinutý	k2eAgInSc4d1	vyvinutý
v	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
československém	československý	k2eAgNnSc6d1	Československé
Aeru	aero	k1gNnSc6	aero
Vodochody	Vodochod	k1gInPc1	Vodochod
<g/>
,	,	kIx,	,
hlavním	hlavní	k2eAgMnSc7d1	hlavní
konstruktérem	konstruktér	k1gMnSc7	konstruktér
Ing.	ing.	kA	ing.
Janem	Jan	k1gMnSc7	Jan
Vlčkem	Vlček	k1gMnSc7	Vlček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
první	první	k4xOgInSc4	první
typ	typ	k1gInSc4	typ
cvičného	cvičný	k2eAgNnSc2d1	cvičné
proudového	proudový	k2eAgNnSc2d1	proudové
letadla	letadlo	k1gNnSc2	letadlo
druhé	druhý	k4xOgFnSc2	druhý
generace	generace	k1gFnSc2	generace
a	a	k8xC	a
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
typů	typ	k1gInPc2	typ
s	s	k7c7	s
dvouproudovým	dvouproudový	k2eAgInSc7d1	dvouproudový
motorem	motor	k1gInSc7	motor
<g/>
.	.	kIx.	.
</s>
<s>
L-39	L-39	k4	L-39
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
jednotným	jednotný	k2eAgInSc7d1	jednotný
výcvikovým	výcvikový	k2eAgInSc7d1	výcvikový
letounem	letoun	k1gInSc7	letoun
armád	armáda	k1gFnPc2	armáda
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Polska	Polsko	k1gNnSc2	Polsko
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
cca	cca	kA	cca
2800	[number]	k4	2800
kusů	kus	k1gInPc2	kus
součástí	součást	k1gFnPc2	součást
leteckého	letecký	k2eAgInSc2d1	letecký
parku	park	k1gInSc2	park
letectev	letectva	k1gFnPc2	letectva
tří	tři	k4xCgFnPc2	tři
desítek	desítka	k1gFnPc2	desítka
států	stát	k1gInPc2	stát
a	a	k8xC	a
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
nejpoužívanějším	používaný	k2eAgInSc7d3	nejpoužívanější
proudovým	proudový	k2eAgInSc7d1	proudový
výcvikovým	výcvikový	k2eAgInSc7d1	výcvikový
letounem	letoun	k1gInSc7	letoun
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
L-39	L-39	k4	L-39
Albatros	albatros	k1gMnSc1	albatros
je	být	k5eAaImIp3nS	být
všestranný	všestranný	k2eAgInSc4d1	všestranný
letoun	letoun	k1gInSc4	letoun
určený	určený	k2eAgInSc4d1	určený
pro	pro	k7c4	pro
základní	základní	k2eAgInSc4d1	základní
<g/>
,	,	kIx,	,
pokračovací	pokračovací	k2eAgInSc4d1	pokračovací
i	i	k8xC	i
bojový	bojový	k2eAgInSc4d1	bojový
letecký	letecký	k2eAgInSc4d1	letecký
výcvik	výcvik	k1gInSc4	výcvik
<g/>
,	,	kIx,	,
ve	v	k7c6	v
verzích	verze	k1gFnPc6	verze
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
instalaci	instalace	k1gFnSc4	instalace
výzbroje	výzbroj	k1gFnSc2	výzbroj
(	(	kIx(	(
<g/>
L-	L-	k1gFnSc1	L-
<g/>
39	[number]	k4	39
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
L-	L-	k1gFnSc1	L-
<g/>
39	[number]	k4	39
<g/>
ZO	ZO	kA	ZO
<g/>
,	,	kIx,	,
L-	L-	k1gFnSc1	L-
<g/>
39	[number]	k4	39
<g/>
ZA	za	k7c4	za
<g/>
)	)	kIx)	)
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
použit	použít	k5eAaPmNgInS	použít
jako	jako	k8xS	jako
lehký	lehký	k2eAgInSc1d1	lehký
bitevní	bitevní	k2eAgInSc1d1	bitevní
letoun	letoun	k1gInSc1	letoun
<g/>
,	,	kIx,	,
omezeně	omezeně	k6eAd1	omezeně
může	moct	k5eAaImIp3nS	moct
působit	působit	k5eAaImF	působit
i	i	k9	i
proti	proti	k7c3	proti
vzdušným	vzdušný	k2eAgInPc3d1	vzdušný
cílům	cíl	k1gInPc3	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
skvělým	skvělý	k2eAgFnPc3d1	skvělá
letovým	letový	k2eAgFnPc3d1	letová
vlastnostem	vlastnost	k1gFnPc3	vlastnost
a	a	k8xC	a
snadné	snadný	k2eAgFnSc6d1	snadná
ovladatelnosti	ovladatelnost	k1gFnSc6	ovladatelnost
je	být	k5eAaImIp3nS	být
vhodný	vhodný	k2eAgInSc1d1	vhodný
i	i	k9	i
pro	pro	k7c4	pro
leteckou	letecký	k2eAgFnSc4d1	letecká
akrobacii	akrobacie	k1gFnSc4	akrobacie
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
typem	typ	k1gInSc7	typ
předváděla	předvádět	k5eAaImAgFnS	předvádět
např.	např.	kA	např.
slovenská	slovenský	k2eAgFnSc1d1	slovenská
akrobatická	akrobatický	k2eAgFnSc1d1	akrobatická
skupina	skupina	k1gFnSc1	skupina
Biele	Biele	k1gFnSc2	Biele
Albatrosy	albatros	k1gMnPc7	albatros
(	(	kIx(	(
<g/>
1991	[number]	k4	1991
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
ruská	ruský	k2eAgFnSc1d1	ruská
skupina	skupina	k1gFnSc1	skupina
Rusi	Rus	k1gFnSc2	Rus
z	z	k7c2	z
Vjazmy	Vjazma	k1gFnSc2	Vjazma
nebo	nebo	k8xC	nebo
francouzský	francouzský	k2eAgInSc4d1	francouzský
Breitling	Breitling	k1gInSc4	Breitling
Jet	jet	k2eAgInSc1d1	jet
Team	team	k1gInSc1	team
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
přednosti	přednost	k1gFnPc4	přednost
patří	patřit	k5eAaImIp3nS	patřit
vysoká	vysoký	k2eAgFnSc1d1	vysoká
spolehlivost	spolehlivost	k1gFnSc1	spolehlivost
<g/>
,	,	kIx,	,
jednoduchost	jednoduchost	k1gFnSc1	jednoduchost
a	a	k8xC	a
nenáročnost	nenáročnost	k1gFnSc1	nenáročnost
na	na	k7c4	na
obsluhu	obsluha	k1gFnSc4	obsluha
<g/>
.	.	kIx.	.
</s>
<s>
Letoun	letoun	k1gInSc1	letoun
je	být	k5eAaImIp3nS	být
navržený	navržený	k2eAgInSc1d1	navržený
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mohl	moct	k5eAaImAgInS	moct
působit	působit	k5eAaImF	působit
bez	bez	k7c2	bez
problémů	problém	k1gInPc2	problém
i	i	k9	i
z	z	k7c2	z
nezpevněných	zpevněný	k2eNgFnPc2d1	nezpevněná
ploch	plocha	k1gFnPc2	plocha
(	(	kIx(	(
<g/>
vstupy	vstup	k1gInPc1	vstup
vzduchu	vzduch	k1gInSc2	vzduch
vysoko	vysoko	k6eAd1	vysoko
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
,	,	kIx,	,
kryté	krytý	k2eAgInPc1d1	krytý
před	před	k7c7	před
nečistotami	nečistota	k1gFnPc7	nečistota
křídlem	křídlo	k1gNnSc7	křídlo
<g/>
,	,	kIx,	,
zakryté	zakrytý	k2eAgFnPc1d1	zakrytá
podvozkové	podvozkový	k2eAgFnPc1d1	podvozková
šachty	šachta	k1gFnPc1	šachta
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Letoun	letoun	k1gInSc1	letoun
byl	být	k5eAaImAgInS	být
navržen	navrhnout	k5eAaPmNgInS	navrhnout
jako	jako	k8xC	jako
součást	součást	k1gFnSc1	součást
komplexního	komplexní	k2eAgInSc2d1	komplexní
výcvikového	výcvikový	k2eAgInSc2d1	výcvikový
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
pilotní	pilotní	k2eAgInSc1d1	pilotní
trenažér	trenažér	k1gInSc1	trenažér
TL-	TL-	k1gFnSc1	TL-
<g/>
39	[number]	k4	39
<g/>
,	,	kIx,	,
trenažér	trenažér	k1gInSc1	trenažér
katapultáže	katapultáž	k1gFnSc2	katapultáž
NKTL-	NKTL-	k1gFnSc2	NKTL-
<g/>
39	[number]	k4	39
<g/>
,	,	kIx,	,
pozemní	pozemní	k2eAgNnSc4d1	pozemní
kontrolní	kontrolní	k2eAgNnSc4d1	kontrolní
zařízení	zařízení	k1gNnSc4	zařízení
KL-39	KL-39	k1gFnSc2	KL-39
a	a	k8xC	a
vlečný	vlečný	k2eAgInSc4d1	vlečný
terč	terč	k1gInSc4	terč
KT-	KT-	k1gMnPc2	KT-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
provozuje	provozovat	k5eAaImIp3nS	provozovat
letouny	letoun	k1gInPc4	letoun
L-39	L-39	k1gFnSc2	L-39
213	[number]	k4	213
<g/>
.	.	kIx.	.
výcviková	výcvikový	k2eAgFnSc1d1	výcviková
letka	letka	k1gFnSc1	letka
v	v	k7c6	v
Čáslavi	Čáslav	k1gFnSc6	Čáslav
(	(	kIx(	(
<g/>
4	[number]	k4	4
kusy	kus	k1gInPc4	kus
verze	verze	k1gFnSc1	verze
L-	L-	k1gFnSc1	L-
<g/>
39	[number]	k4	39
<g/>
ZA	za	k7c4	za
<g/>
)	)	kIx)	)
a	a	k8xC	a
Centrum	centrum	k1gNnSc1	centrum
leteckého	letecký	k2eAgInSc2d1	letecký
výcviku	výcvik	k1gInSc2	výcvik
v	v	k7c6	v
Pardubicích	Pardubice	k1gInPc6	Pardubice
(	(	kIx(	(
<g/>
verze	verze	k1gFnPc1	verze
L-39C	L-39C	k1gFnSc2	L-39C
–	–	k?	–
ev.	ev.	k?	ev.
č.	č.	k?	č.
0	[number]	k4	0
<g/>
103	[number]	k4	103
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
113	[number]	k4	113
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
115	[number]	k4	115
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
441	[number]	k4	441
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
444	[number]	k4	444
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
445	[number]	k4	445
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
448	[number]	k4	448
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
představila	představit	k5eAaPmAgFnS	představit
firma	firma	k1gFnSc1	firma
AERO	aero	k1gNnSc4	aero
Vodochody	Vodochod	k1gInPc1	Vodochod
AEROSPACE	AEROSPACE	kA	AEROSPACE
a.s.	a.s.	k?	a.s.
na	na	k7c6	na
veletrhu	veletrh	k1gInSc6	veletrh
ve	v	k7c4	v
Farnborough	Farnborough	k1gInSc4	Farnborough
projekt	projekt	k1gInSc4	projekt
nové	nový	k2eAgFnSc2d1	nová
generace	generace	k1gFnSc2	generace
Albatrosu	albatros	k1gMnSc6	albatros
s	s	k7c7	s
názvem	název	k1gInSc7	název
L-39NG	L-39NG	k1gMnPc2	L-39NG
(	(	kIx(	(
<g/>
Next	Next	k2eAgInSc1d1	Next
Generation	Generation	k1gInSc1	Generation
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
prototyp	prototyp	k1gInSc1	prototyp
<g/>
,	,	kIx,	,
kombinující	kombinující	k2eAgInSc1d1	kombinující
stávající	stávající	k2eAgInSc1d1	stávající
drak	drak	k1gInSc1	drak
a	a	k8xC	a
nový	nový	k2eAgInSc1d1	nový
motor	motor	k1gInSc1	motor
<g/>
,	,	kIx,	,
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
vzlétnout	vzlétnout	k5eAaPmF	vzlétnout
v	v	k7c6	v
září	září	k1gNnSc6	září
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Letoun	letoun	k1gInSc1	letoun
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
představen	představit	k5eAaPmNgInS	představit
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
2016	[number]	k4	2016
a	a	k8xC	a
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
až	až	k9	až
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
==	==	k?	==
</s>
</p>
<p>
<s>
Letoun	letoun	k1gMnSc1	letoun
je	být	k5eAaImIp3nS	být
nástupcem	nástupce	k1gMnSc7	nástupce
předchozího	předchozí	k2eAgInSc2d1	předchozí
úspěšného	úspěšný	k2eAgInSc2d1	úspěšný
typu	typ	k1gInSc2	typ
Aero	aero	k1gNnSc1	aero
L-29	L-29	k1gMnSc1	L-29
Delfín	Delfín	k1gMnSc1	Delfín
<g/>
.	.	kIx.	.
</s>
<s>
Prototyp	prototyp	k1gInSc1	prototyp
L-39	L-39	k1gMnSc2	L-39
X-02	X-02	k1gMnSc2	X-02
(	(	kIx(	(
<g/>
OK-	OK-	k1gFnSc1	OK-
<g/>
32	[number]	k4	32
<g/>
)	)	kIx)	)
poprvé	poprvé	k6eAd1	poprvé
vzlétl	vzlétnout	k5eAaPmAgInS	vzlétnout
4	[number]	k4	4
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1968	[number]	k4	1968
s	s	k7c7	s
pilotem	pilot	k1gMnSc7	pilot
Rudolfem	Rudolf	k1gMnSc7	Rudolf
Duchoněm	Duchoň	k1gMnSc7	Duchoň
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgInSc4	druhý
letový	letový	k2eAgInSc4d1	letový
prototyp	prototyp	k1gInSc4	prototyp
X-03	X-03	k1gFnPc4	X-03
7	[number]	k4	7
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1969	[number]	k4	1969
a	a	k8xC	a
následně	následně	k6eAd1	následně
třetí	třetí	k4xOgInSc4	třetí
letový	letový	k2eAgInSc4d1	letový
prototyp	prototyp	k1gInSc4	prototyp
X-05	X-05	k1gFnSc2	X-05
(	(	kIx(	(
<g/>
OK-	OK-	k1gFnSc1	OK-
<g/>
25	[number]	k4	25
<g/>
)	)	kIx)	)
pak	pak	k6eAd1	pak
23	[number]	k4	23
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1969	[number]	k4	1969
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1970	[number]	k4	1970
zalétal	zalétat	k5eAaPmAgMnS	zalétat
tovární	tovární	k2eAgMnSc1d1	tovární
pilot	pilot	k1gMnSc1	pilot
J.	J.	kA	J.
Šouc	Šouc	k1gFnSc4	Šouc
čtvrtý	čtvrtý	k4xOgInSc1	čtvrtý
letový	letový	k2eAgInSc1d1	letový
prototyp	prototyp	k1gInSc1	prototyp
X-06	X-06	k1gFnSc1	X-06
(	(	kIx(	(
<g/>
OK-	OK-	k1gFnSc1	OK-
<g/>
186	[number]	k4	186
<g/>
)	)	kIx)	)
a	a	k8xC	a
X-07	X-07	k1gMnSc1	X-07
(	(	kIx(	(
<g/>
3907	[number]	k4	3907
<g/>
)	)	kIx)	)
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1970	[number]	k4	1970
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
byl	být	k5eAaImAgInS	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1971	[number]	k4	1971
vyráběn	vyrábět	k5eAaImNgInS	vyrábět
jako	jako	k8xS	jako
letoun	letoun	k1gInSc1	letoun
pro	pro	k7c4	pro
základní	základní	k2eAgInSc4d1	základní
výcvik	výcvik	k1gInSc4	výcvik
vojenských	vojenský	k2eAgMnPc2d1	vojenský
pilotů	pilot	k1gMnPc2	pilot
letectev	letectva	k1gFnPc2	letectva
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
,	,	kIx,	,
Československa	Československo	k1gNnSc2	Československo
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
zemí	zem	k1gFnPc2	zem
Varšavského	varšavský	k2eAgInSc2d1	varšavský
paktu	pakt	k1gInSc2	pakt
(	(	kIx(	(
<g/>
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
používalo	používat	k5eAaImAgNnS	používat
vlastní	vlastní	k2eAgInSc4d1	vlastní
letoun	letoun	k1gInSc4	letoun
PZL	PZL	kA	PZL
TS-11	TS-11	k1gMnSc2	TS-11
Iskra	Iskr	k1gMnSc2	Iskr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vznikl	vzniknout	k5eAaPmAgInS	vzniknout
také	také	k9	také
jeden	jeden	k4xCgInSc1	jeden
prototyp	prototyp	k1gInSc1	prototyp
nazývaný	nazývaný	k2eAgMnSc1d1	nazývaný
L-139	L-139	k1gMnSc1	L-139
Albatros	albatros	k1gMnSc1	albatros
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
používal	používat	k5eAaImAgInS	používat
americký	americký	k2eAgInSc1d1	americký
motor	motor	k1gInSc1	motor
Garrett	Garrett	k2eAgInSc1d1	Garrett
TFE731-4	TFE731-4	k1gFnSc4	TFE731-4
o	o	k7c6	o
tahu	tah	k1gInSc6	tah
18,15	[number]	k4	18,15
kN	kN	k?	kN
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Několika	několik	k4yIc7	několik
úpravami	úprava	k1gFnPc7	úprava
a	a	k8xC	a
modernizací	modernizace	k1gFnSc7	modernizace
při	při	k7c6	při
zachování	zachování	k1gNnSc6	zachování
stejného	stejný	k2eAgInSc2d1	stejný
designu	design	k1gInSc2	design
draku	drak	k1gInSc2	drak
letounu	letoun	k1gInSc2	letoun
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
typ	typ	k1gInSc1	typ
Aero	aero	k1gNnSc4	aero
L-59	L-59	k1gMnPc2	L-59
Super	super	k1gInPc2	super
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
montáží	montáž	k1gFnSc7	montáž
výkonnějšího	výkonný	k2eAgInSc2d2	výkonnější
motoru	motor	k1gInSc2	motor
<g/>
,	,	kIx,	,
prodloužením	prodloužení	k1gNnSc7	prodloužení
přední	přední	k2eAgFnSc2d1	přední
části	část	k1gFnSc2	část
trupu	trup	k1gInSc2	trup
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ukrývá	ukrývat	k5eAaImIp3nS	ukrývat
radarový	radarový	k2eAgInSc4d1	radarový
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
podstatnou	podstatný	k2eAgFnSc7d1	podstatná
modernizací	modernizace	k1gFnSc7	modernizace
avioniky	avionika	k1gFnSc2	avionika
a	a	k8xC	a
dalšími	další	k2eAgFnPc7d1	další
úpravami	úprava	k1gFnPc7	úprava
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
typ	typ	k1gInSc1	typ
Aero	aero	k1gNnSc4	aero
L-159	L-159	k1gMnPc2	L-159
Alca	Alc	k1gInSc2	Alc
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
2014	[number]	k4	2014
byl	být	k5eAaImAgInS	být
představen	představit	k5eAaPmNgInS	představit
projekt	projekt	k1gInSc1	projekt
nové	nový	k2eAgFnSc2d1	nová
generace	generace	k1gFnSc2	generace
cvičného	cvičný	k2eAgInSc2d1	cvičný
proudového	proudový	k2eAgInSc2d1	proudový
letounu	letoun	k1gInSc2	letoun
L-	L-	k1gFnSc2	L-
<g/>
39	[number]	k4	39
<g/>
NG	NG	kA	NG
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Operační	operační	k2eAgNnSc1d1	operační
nasazení	nasazení	k1gNnSc1	nasazení
==	==	k?	==
</s>
</p>
<p>
<s>
Albatros	albatros	k1gMnSc1	albatros
se	se	k3xPyFc4	se
do	do	k7c2	do
konce	konec	k1gInSc2	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
stal	stát	k5eAaPmAgInS	stát
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
nejrozšířenějších	rozšířený	k2eAgNnPc2d3	nejrozšířenější
cvičných	cvičný	k2eAgNnPc2d1	cvičné
proudových	proudový	k2eAgNnPc2d1	proudové
letadel	letadlo	k1gNnPc2	letadlo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Používaný	používaný	k2eAgInSc1d1	používaný
byl	být	k5eAaImAgInS	být
zejména	zejména	k9	zejména
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
Varšavské	varšavský	k2eAgFnSc2d1	Varšavská
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
Polska	Polsko	k1gNnSc2	Polsko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
používalo	používat	k5eAaImAgNnS	používat
vlastní	vlastní	k2eAgInSc4d1	vlastní
typ	typ	k1gInSc4	typ
PZL	PZL	kA	PZL
TS-11	TS-11	k1gMnSc2	TS-11
Iskra	Iskr	k1gMnSc2	Iskr
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgInS	být
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
L-29	L-29	k1gFnSc6	L-29
hlavním	hlavní	k2eAgNnSc7d1	hlavní
cvičným	cvičný	k2eAgNnSc7d1	cvičné
letadlem	letadlo	k1gNnSc7	letadlo
pro	pro	k7c4	pro
pokračovací	pokračovací	k2eAgInSc4d1	pokračovací
letecký	letecký	k2eAgInSc4d1	letecký
výcvik	výcvik	k1gInSc4	výcvik
i	i	k9	i
v	v	k7c6	v
SSSR	SSSR	kA	SSSR
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přestože	přestože	k8xS	přestože
jsou	být	k5eAaImIp3nP	být
Albatrosy	albatros	k1gMnPc4	albatros
postupně	postupně	k6eAd1	postupně
nahrazovány	nahrazován	k2eAgInPc1d1	nahrazován
modernějšími	moderní	k2eAgInPc7d2	modernější
typy	typ	k1gInPc7	typ
<g/>
,	,	kIx,	,
tisíce	tisíc	k4xCgInPc1	tisíc
těchto	tento	k3xDgInPc2	tento
strojů	stroj	k1gInPc2	stroj
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
dodnes	dodnes	k6eAd1	dodnes
v	v	k7c6	v
aktivní	aktivní	k2eAgFnSc6d1	aktivní
službě	služba	k1gFnSc6	služba
jako	jako	k9	jako
cvičné	cvičný	k2eAgInPc4d1	cvičný
letouny	letoun	k1gInPc4	letoun
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
mnoho	mnoho	k4c4	mnoho
těchto	tento	k3xDgNnPc2	tento
letadel	letadlo	k1gNnPc2	letadlo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
soukromých	soukromý	k2eAgFnPc6d1	soukromá
rukách	ruka	k1gFnPc6	ruka
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jejich	jejich	k3xOp3gFnSc1	jejich
cena	cena	k1gFnSc1	cena
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
200	[number]	k4	200
000	[number]	k4	000
až	až	k9	až
300	[number]	k4	300
000	[number]	k4	000
USD	USD	kA	USD
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
Albatros	albatros	k1gMnSc1	albatros
oblíbili	oblíbit	k5eAaPmAgMnP	oblíbit
letci	letec	k1gMnPc7	letec
hledající	hledající	k2eAgFnSc2d1	hledající
rychlé	rychlý	k2eAgFnSc2d1	rychlá
a	a	k8xC	a
agilní	agilní	k2eAgNnSc4d1	agilní
letadlo	letadlo	k1gNnSc4	letadlo
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
popularita	popularita	k1gFnSc1	popularita
vedla	vést	k5eAaImAgFnS	vést
k	k	k7c3	k
zařazení	zařazení	k1gNnSc3	zařazení
tohoto	tento	k3xDgNnSc2	tento
letadla	letadlo	k1gNnSc2	letadlo
do	do	k7c2	do
závodů	závod	k1gInPc2	závod
Reno	Reno	k6eAd1	Reno
Air	Air	k1gMnSc3	Air
Race	Race	k1gFnPc2	Race
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
strojů	stroj	k1gInPc2	stroj
s	s	k7c7	s
proudovým	proudový	k2eAgInSc7d1	proudový
motorem	motor	k1gInSc7	motor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
března	březen	k1gInSc2	březen
2006	[number]	k4	2006
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
federálním	federální	k2eAgInSc6d1	federální
rejstříku	rejstřík	k1gInSc6	rejstřík
letadel	letadlo	k1gNnPc2	letadlo
USA	USA	kA	USA
registrovaných	registrovaný	k2eAgMnPc2d1	registrovaný
257	[number]	k4	257
albatrosů	albatros	k1gMnPc2	albatros
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
L-39	L-39	k1gMnSc6	L-39
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
===	===	k?	===
</s>
</p>
<p>
<s>
Při	při	k7c6	při
dělení	dělení	k1gNnSc6	dělení
majetku	majetek	k1gInSc2	majetek
ČSFR	ČSFR	kA	ČSFR
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
získala	získat	k5eAaPmAgFnS	získat
SR	SR	kA	SR
8	[number]	k4	8
ks	ks	kA	ks
L-39C	L-39C	k1gFnSc2	L-39C
(	(	kIx(	(
<g/>
evidenční	evidenční	k2eAgNnPc4d1	evidenční
čísla	číslo	k1gNnPc4	číslo
<g/>
:	:	kIx,	:
0	[number]	k4	0
<g/>
101	[number]	k4	101
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
102	[number]	k4	102
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
111	[number]	k4	111
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
112	[number]	k4	112
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
442	[number]	k4	442
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
443	[number]	k4	443
<g/>
,	,	kIx,	,
4355	[number]	k4	4355
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
4357	[number]	k4	4357
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
9	[number]	k4	9
ks	ks	kA	ks
L-39ZA	L-39ZA	k1gFnSc2	L-39ZA
(	(	kIx(	(
<g/>
evidenční	evidenční	k2eAgNnPc4d1	evidenční
čísla	číslo	k1gNnPc4	číslo
<g/>
:	:	kIx,	:
4701	[number]	k4	4701
<g/>
,	,	kIx,	,
4703	[number]	k4	4703
<g/>
,	,	kIx,	,
4705	[number]	k4	4705
<g/>
,	,	kIx,	,
4707	[number]	k4	4707
<g/>
,	,	kIx,	,
4711	[number]	k4	4711
<g/>
,	,	kIx,	,
1701	[number]	k4	1701
<g/>
,	,	kIx,	,
1725	[number]	k4	1725
<g/>
,	,	kIx,	,
1730	[number]	k4	1730
<g/>
,	,	kIx,	,
3905	[number]	k4	3905
<g/>
)	)	kIx)	)
a	a	k8xC	a
2	[number]	k4	2
ks	ks	kA	ks
L-39V	L-39V	k1gFnSc2	L-39V
(	(	kIx(	(
<g/>
evidenční	evidenční	k2eAgNnPc4d1	evidenční
čísla	číslo	k1gNnPc4	číslo
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
730	[number]	k4	730
a	a	k8xC	a
0	[number]	k4	0
<g/>
745	[number]	k4	745
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Stroje	stroj	k1gInPc1	stroj
L-39C	L-39C	k1gFnSc2	L-39C
byly	být	k5eAaImAgInP	být
od	od	k7c2	od
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
zařazeny	zařazen	k2eAgInPc1d1	zařazen
k	k	k7c3	k
1	[number]	k4	1
<g/>
.	.	kIx.	.
letce	letka	k1gFnSc6	letka
5	[number]	k4	5
<g/>
.	.	kIx.	.
školního	školní	k2eAgInSc2d1	školní
leteckého	letecký	k2eAgInSc2d1	letecký
pluku	pluk	k1gInSc2	pluk
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yRgMnSc2	který
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
zformováno	zformovat	k5eAaPmNgNnS	zformovat
Výcvikové	výcvikový	k2eAgNnSc1d1	výcvikové
středisko	středisko	k1gNnSc1	středisko
letectva	letectvo	k1gNnSc2	letectvo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Během	během	k7c2	během
reorganizace	reorganizace	k1gFnSc2	reorganizace
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
bylo	být	k5eAaImAgNnS	být
Výcvikové	výcvikový	k2eAgNnSc1d1	výcvikové
středisko	středisko	k1gNnSc1	středisko
letectva	letectvo	k1gNnSc2	letectvo
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
zrušeno	zrušit	k5eAaPmNgNnS	zrušit
a	a	k8xC	a
šest	šest	k4xCc4	šest
strojů	stroj	k1gInPc2	stroj
L-39C	L-39C	k1gFnPc2	L-39C
bylo	být	k5eAaImAgNnS	být
přeřazeno	přeřadit	k5eAaPmNgNnS	přeřadit
k	k	k7c3	k
Vojenské	vojenský	k2eAgFnSc3d1	vojenská
letecké	letecký	k2eAgFnSc3d1	letecká
akademii	akademie	k1gFnSc3	akademie
gen.	gen.	kA	gen.
M.	M.	kA	M.
<g/>
R.	R.	kA	R.
Štefánika	Štefánik	k1gMnSc2	Štefánik
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
označen	označit	k5eAaPmNgMnS	označit
<g/>
\	\	kIx~	\
jako	jako	k8xC	jako
výcviková	výcvikový	k2eAgFnSc1d1	výcviková
letka	letka	k1gFnSc1	letka
<g/>
.	.	kIx.	.
</s>
<s>
Letiště	letiště	k1gNnSc1	letiště
Sliač	Sliač	k1gMnSc1	Sliač
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
1	[number]	k4	1
<g/>
.	.	kIx.	.
stíhací	stíhací	k2eAgInSc1d1	stíhací
letecký	letecký	k2eAgInSc1d1	letecký
pluk	pluk	k1gInSc1	pluk
získal	získat	k5eAaPmAgInS	získat
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
pět	pět	k4xCc4	pět
strojů	stroj	k1gInPc2	stroj
L-39ZA	L-39ZA	k1gFnSc2	L-39ZA
(	(	kIx(	(
<g/>
evidenční	evidenční	k2eAgNnPc4d1	evidenční
čísla	číslo	k1gNnPc4	číslo
<g/>
:	:	kIx,	:
<g/>
4701	[number]	k4	4701
<g/>
,	,	kIx,	,
4703	[number]	k4	4703
<g/>
,	,	kIx,	,
4705	[number]	k4	4705
<g/>
,	,	kIx,	,
4707	[number]	k4	4707
<g/>
,	,	kIx,	,
4711	[number]	k4	4711
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
stroj	stroj	k1gInSc1	stroj
L-39ZA	L-39ZA	k1gFnSc2	L-39ZA
(	(	kIx(	(
1701	[number]	k4	1701
<g/>
)	)	kIx)	)
získal	získat	k5eAaPmAgInS	získat
2	[number]	k4	2
<g/>
.	.	kIx.	.
smíšený	smíšený	k2eAgInSc1d1	smíšený
letecký	letecký	k2eAgInSc1d1	letecký
pluk	pluk	k1gInSc1	pluk
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
Trenčín	Trenčín	k1gInSc1	Trenčín
<g/>
.	.	kIx.	.
</s>
<s>
Zbývající	zbývající	k2eAgInPc1d1	zbývající
tři	tři	k4xCgInPc1	tři
stroje	stroj	k1gInPc1	stroj
(	(	kIx(	(
<g/>
evidenční	evidenční	k2eAgNnPc1d1	evidenční
čísla	číslo	k1gNnPc1	číslo
<g/>
:	:	kIx,	:
<g/>
1725	[number]	k4	1725
<g/>
,	,	kIx,	,
1730	[number]	k4	1730
<g/>
,	,	kIx,	,
3905	[number]	k4	3905
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
zařazeny	zařadit	k5eAaPmNgFnP	zařadit
k	k	k7c3	k
3	[number]	k4	3
<g/>
.	.	kIx.	.
stíhacímu	stíhací	k2eAgInSc3d1	stíhací
bombardovacímu	bombardovací	k2eAgInSc3d1	bombardovací
leteckému	letecký	k2eAgInSc3d1	letecký
pluku	pluk	k1gInSc3	pluk
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
Kuchyňa	Kuchyň	k1gInSc2	Kuchyň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1993	[number]	k4	1993
byly	být	k5eAaImAgFnP	být
přesunuty	přesunout	k5eAaPmNgInP	přesunout
do	do	k7c2	do
Trenčína	Trenčín	k1gInSc2	Trenčín
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
půjčili	půjčit	k5eAaPmAgMnP	půjčit
jeden	jeden	k4xCgMnSc1	jeden
L-39ZA	L-39ZA	k1gMnSc1	L-39ZA
(	(	kIx(	(
<g/>
ev.	ev.	k?	ev.
<g/>
č.	č.	k?	č.
3905	[number]	k4	3905
<g/>
)	)	kIx)	)
na	na	k7c4	na
základnu	základna	k1gFnSc4	základna
Sliač	Sliač	k1gMnSc1	Sliač
<g/>
.	.	kIx.	.
</s>
<s>
Trojice	trojice	k1gFnPc1	trojice
strojů	stroj	k1gInPc2	stroj
L-39ZA	L-39ZA	k1gFnSc1	L-39ZA
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
přesunuta	přesunout	k5eAaPmNgFnS	přesunout
k	k	k7c3	k
33	[number]	k4	33
<g/>
.	.	kIx.	.
stíhacího	stíhací	k2eAgInSc2d1	stíhací
bombardovacímu	bombardovací	k2eAgNnSc3d1	bombardovací
křídlu	křídlo	k1gNnSc3	křídlo
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
Kuchyňa	Kuchyň	k1gInSc2	Kuchyň
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
1995	[number]	k4	1995
znamenal	znamenat	k5eAaImAgInS	znamenat
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
L-39ZA	L-39ZA	k1gMnPc4	L-39ZA
zařazení	zařazení	k1gNnSc6	zařazení
k	k	k7c3	k
31	[number]	k4	31
<g/>
.	.	kIx.	.
stíhacímu	stíhací	k2eAgNnSc3d1	stíhací
křídlu	křídlo	k1gNnSc3	křídlo
z	z	k7c2	z
letiště	letiště	k1gNnSc2	letiště
Sliač	Sliač	k1gInSc1	Sliač
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
byly	být	k5eAaImAgFnP	být
zařazeny	zařadit	k5eAaPmNgInP	zařadit
k	k	k7c3	k
výcvikovým	výcvikový	k2eAgFnPc3d1	výcviková
letce	letec	k1gMnSc2	letec
<g/>
.	.	kIx.	.
</s>
<s>
Stroje	stroj	k1gInPc4	stroj
L-39V	L-39V	k1gFnSc2	L-39V
byly	být	k5eAaImAgFnP	být
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1993	[number]	k4	1993
přiřazeny	přiřazen	k2eAgInPc1d1	přiřazen
k	k	k7c3	k
5	[number]	k4	5
<g/>
.	.	kIx.	.
leteckému	letecký	k2eAgInSc3d1	letecký
školnímu	školní	k2eAgInSc3d1	školní
pluku	pluk	k1gInSc3	pluk
v	v	k7c6	v
Košicích	Košice	k1gInPc6	Košice
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
k	k	k7c3	k
výcvikovému	výcvikový	k2eAgNnSc3d1	výcvikové
středisku	středisko	k1gNnSc3	středisko
letectva	letectvo	k1gNnSc2	letectvo
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
reorganizace	reorganizace	k1gFnSc2	reorganizace
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
přesunuty	přesunout	k5eAaPmNgInP	přesunout
na	na	k7c4	na
leteckou	letecký	k2eAgFnSc4d1	letecká
základnu	základna	k1gFnSc4	základna
Sliač	Sliač	k1gMnSc1	Sliač
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byly	být	k5eAaImAgInP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
vyřazeny	vyřazen	k2eAgInPc4d1	vyřazen
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
ukončení	ukončení	k1gNnSc2	ukončení
technické	technický	k2eAgFnSc2d1	technická
životnosti	životnost	k1gFnSc2	životnost
<g/>
.	.	kIx.	.
</s>
<s>
Výcviková	výcvikový	k2eAgFnSc1d1	výcviková
letka	letka	k1gFnSc1	letka
má	mít	k5eAaImIp3nS	mít
nyní	nyní	k6eAd1	nyní
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
9	[number]	k4	9
letadel	letadlo	k1gNnPc2	letadlo
L-39	L-39	k1gMnSc1	L-39
Albatros	albatros	k1gMnSc1	albatros
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
prohlášení	prohlášení	k1gNnSc2	prohlášení
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
ministra	ministr	k1gMnSc2	ministr
obrany	obrana	k1gFnSc2	obrana
Ľubomíra	Ľubomír	k1gMnSc2	Ľubomír
Galka	Galek	k1gMnSc2	Galek
se	se	k3xPyFc4	se
šest	šest	k4xCc1	šest
strojů	stroj	k1gInPc2	stroj
podrobí	podrobit	k5eAaPmIp3nS	podrobit
generálním	generální	k2eAgFnPc3d1	generální
opravám	oprava	k1gFnPc3	oprava
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
bude	být	k5eAaImBp3nS	být
zajištěna	zajistit	k5eAaPmNgFnS	zajistit
jejich	jejich	k3xOp3gFnSc1	jejich
další	další	k2eAgFnSc1d1	další
letová	letový	k2eAgFnSc1d1	letová
činnost	činnost	k1gFnSc1	činnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Modernizace	modernizace	k1gFnSc2	modernizace
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
1996	[number]	k4	1996
a	a	k8xC	a
1997	[number]	k4	1997
se	se	k3xPyFc4	se
šestice	šestice	k1gFnSc1	šestice
strojů	stroj	k1gInPc2	stroj
L-39C	L-39C	k1gFnSc1	L-39C
podrobila	podrobit	k5eAaPmAgFnS	podrobit
generálním	generální	k2eAgFnPc3d1	generální
opravám	oprava	k1gFnPc3	oprava
<g/>
,	,	kIx,	,
během	během	k7c2	během
nichž	jenž	k3xRgFnPc2	jenž
jim	on	k3xPp3gFnPc3	on
vyměnili	vyměnit	k5eAaPmAgMnP	vyměnit
přední	přední	k2eAgFnPc4d1	přední
části	část	k1gFnPc4	část
trupu	trup	k1gInSc2	trup
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc1	jejich
životnost	životnost	k1gFnSc1	životnost
byla	být	k5eAaImAgFnS	být
prodloužena	prodloužit	k5eAaPmNgFnS	prodloužit
o	o	k7c4	o
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodnutí	rozhodnutí	k1gNnSc1	rozhodnutí
o	o	k7c6	o
modernizaci	modernizace	k1gFnSc6	modernizace
letadel	letadlo	k1gNnPc2	letadlo
instalováním	instalování	k1gNnSc7	instalování
nových	nový	k2eAgNnPc2d1	nové
křídel	křídlo	k1gNnPc2	křídlo
a	a	k8xC	a
zadní	zadní	k2eAgFnSc2d1	zadní
části	část	k1gFnSc2	část
trupu	trup	k1gInSc2	trup
s	s	k7c7	s
ocasními	ocasní	k2eAgFnPc7d1	ocasní
plochami	plocha	k1gFnPc7	plocha
bylo	být	k5eAaImAgNnS	být
přijato	přijat	k2eAgNnSc1d1	přijato
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
2000	[number]	k4	2000
až	až	k9	až
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
byla	být	k5eAaImAgFnS	být
jejich	jejich	k3xOp3gFnSc1	jejich
životnost	životnost	k1gFnSc1	životnost
prodloužena	prodloužit	k5eAaPmNgFnS	prodloužit
o	o	k7c4	o
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
s	s	k7c7	s
opravami	oprava	k1gFnPc7	oprava
naplánovanými	naplánovaný	k2eAgFnPc7d1	naplánovaná
na	na	k7c4	na
každých	každý	k3xTgNnPc2	každý
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Modernizovaná	modernizovaný	k2eAgFnSc1d1	modernizovaná
byla	být	k5eAaImAgFnS	být
také	také	k9	také
jejich	jejich	k3xOp3gFnSc1	jejich
avionika	avionika	k1gFnSc1	avionika
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
modernizace	modernizace	k1gFnSc2	modernizace
byl	být	k5eAaImAgInS	být
vyhotoven	vyhotovit	k5eAaPmNgInS	vyhotovit
leteckou	letecký	k2eAgFnSc7d1	letecká
akademií	akademie	k1gFnSc7	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
strojem	stroj	k1gInSc7	stroj
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
podrobil	podrobit	k5eAaPmAgMnS	podrobit
byl	být	k5eAaImAgInS	být
L-39C	L-39C	k1gFnSc7	L-39C
s	s	k7c7	s
ev.	ev.	k?	ev.
č.	č.	k?	č.
0	[number]	k4	0
<g/>
111	[number]	k4	111
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
byl	být	k5eAaImAgInS	být
převzat	převzít	k5eAaPmNgInS	převzít
výcvikovou	výcvikový	k2eAgFnSc7d1	výcviková
letkou	letka	k1gFnSc7	letka
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
přelet	přelet	k1gInSc1	přelet
na	na	k7c4	na
leteckou	letecký	k2eAgFnSc4d1	letecká
základnu	základna	k1gFnSc4	základna
Sliač	Sliač	k1gInSc1	Sliač
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2002	[number]	k4	2002
až	až	k8xS	až
2005	[number]	k4	2005
se	se	k3xPyFc4	se
tři	tři	k4xCgInPc1	tři
stroje	stroj	k1gInPc1	stroj
L-39ZA	L-39ZA	k1gFnSc7	L-39ZA
podrobily	podrobit	k5eAaPmAgInP	podrobit
modernizací	modernizace	k1gFnSc7	modernizace
avioniky	avionika	k1gFnPc1	avionika
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
modernizovaný	modernizovaný	k2eAgInSc1d1	modernizovaný
stroj	stroj	k1gInSc1	stroj
s	s	k7c7	s
ev.	ev.	k?	ev.
č.	č.	k?	č.
1725	[number]	k4	1725
byl	být	k5eAaImAgInS	být
osazen	osadit	k5eAaPmNgInS	osadit
přístroji	přístroj	k1gInPc7	přístroj
v	v	k7c6	v
metrických	metrický	k2eAgFnPc6d1	metrická
mírách	míra	k1gFnPc6	míra
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
bylo	být	k5eAaImAgNnS	být
uloženo	uložit	k5eAaPmNgNnS	uložit
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nekompatibility	nekompatibilita	k1gFnSc2	nekompatibilita
<g/>
.	.	kIx.	.
</s>
<s>
Zbývající	zbývající	k2eAgFnSc1d1	zbývající
dvojice	dvojice	k1gFnSc1	dvojice
strojů	stroj	k1gInPc2	stroj
L-39ZAM	L-39ZAM	k1gFnSc1	L-39ZAM
s	s	k7c7	s
ev.	ev.	k?	ev.
č.	č.	k?	č.
1701	[number]	k4	1701
a	a	k8xC	a
1730	[number]	k4	1730
byly	být	k5eAaImAgFnP	být
doplněny	doplnit	k5eAaPmNgFnP	doplnit
další	další	k2eAgFnSc7d1	další
dvojicí	dvojice	k1gFnSc7	dvojice
L-39ZAM	L-39ZAM	k1gFnSc2	L-39ZAM
s	s	k7c7	s
ev.	ev.	k?	ev.
č.	č.	k?	č.
4703	[number]	k4	4703
a	a	k8xC	a
4707	[number]	k4	4707
<g/>
.	.	kIx.	.
</s>
<s>
Letecký	letecký	k2eAgInSc1d1	letecký
park	park	k1gInSc1	park
výcvikové	výcvikový	k2eAgFnSc2d1	výcviková
letky	letka	k1gFnSc2	letka
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
5	[number]	k4	5
ks	ks	kA	ks
modernizovaného	modernizovaný	k2eAgInSc2d1	modernizovaný
modelu	model	k1gInSc2	model
L-39CM	L-39CM	k1gFnSc2	L-39CM
(	(	kIx(	(
<g/>
evidenční	evidenční	k2eAgNnPc4d1	evidenční
čísla	číslo	k1gNnPc4	číslo
<g/>
:	:	kIx,	:
<g/>
5252	[number]	k4	5252
<g/>
,	,	kIx,	,
5253	[number]	k4	5253
<g/>
,	,	kIx,	,
5254	[number]	k4	5254
<g/>
,	,	kIx,	,
5301	[number]	k4	5301
a	a	k8xC	a
5302	[number]	k4	5302
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
se	se	k3xPyFc4	se
musely	muset	k5eAaImAgInP	muset
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
podrobit	podrobit	k5eAaPmF	podrobit
generálním	generální	k2eAgFnPc3d1	generální
opravám	oprava	k1gFnPc3	oprava
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stroji	stroj	k1gInSc6	stroj
s	s	k7c7	s
ev.	ev.	k?	ev.
č.	č.	k?	č.
5301	[number]	k4	5301
již	již	k6eAd1	již
oprava	oprava	k1gFnSc1	oprava
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
leteckých	letecký	k2eAgFnPc6d1	letecká
opravnách	opravna	k1gFnPc6	opravna
Trenčín	Trenčín	k1gInSc4	Trenčín
<g/>
.	.	kIx.	.
</s>
<s>
Stroj	stroj	k1gInSc1	stroj
L-39CM	L-39CM	k1gFnSc2	L-39CM
(	(	kIx(	(
5252	[number]	k4	5252
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
opravě	oprava	k1gFnSc6	oprava
od	od	k7c2	od
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
L-39CM	L-39CM	k1gFnSc1	L-39CM
(	(	kIx(	(
<g/>
5253	[number]	k4	5253
<g/>
)	)	kIx)	)
od	od	k7c2	od
května	květen	k1gInSc2	květen
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
a	a	k8xC	a
L-39CM	L-39CM	k1gFnSc1	L-39CM
(	(	kIx(	(
<g/>
5254	[number]	k4	5254
<g/>
)	)	kIx)	)
od	od	k7c2	od
června	červen	k1gInSc2	červen
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
výzbroji	výzbroj	k1gInSc6	výzbroj
má	mít	k5eAaImIp3nS	mít
letka	letka	k1gFnSc1	letka
i	i	k9	i
4	[number]	k4	4
ks	ks	kA	ks
L-39ZAM	L-39ZAM	k1gFnSc2	L-39ZAM
(	(	kIx(	(
<g/>
evidenční	evidenční	k2eAgNnPc4d1	evidenční
čísla	číslo	k1gNnPc4	číslo
<g/>
:	:	kIx,	:
1701	[number]	k4	1701
<g/>
,	,	kIx,	,
1730	[number]	k4	1730
<g/>
,	,	kIx,	,
4703	[number]	k4	4703
<g/>
,	,	kIx,	,
4707	[number]	k4	4707
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Strojem	stroj	k1gInSc7	stroj
s	s	k7c7	s
ev.	ev.	k?	ev.
č.	č.	k?	č.
1701	[number]	k4	1701
a	a	k8xC	a
1730	[number]	k4	1730
vyprší	vypršet	k5eAaPmIp3nP	vypršet
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
technická	technický	k2eAgFnSc1d1	technická
životnost	životnost	k1gFnSc1	životnost
a	a	k8xC	a
budou	být	k5eAaImBp3nP	být
nahrazeny	nahradit	k5eAaPmNgInP	nahradit
2	[number]	k4	2
ks	ks	kA	ks
uložených	uložený	k2eAgFnPc2d1	uložená
L-	L-	k1gFnPc2	L-
<g/>
39	[number]	k4	39
<g/>
ZA	za	k7c2	za
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
projdou	projít	k5eAaPmIp3nP	projít
generální	generální	k2eAgFnSc7d1	generální
opravou	oprava	k1gFnSc7	oprava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Nehody	nehoda	k1gFnSc2	nehoda
====	====	k?	====
</s>
</p>
<p>
<s>
Během	během	k7c2	během
nácviku	nácvik	k1gInSc2	nácvik
skupinové	skupinový	k2eAgFnSc2d1	skupinová
zalétanosti	zalétanost	k1gFnSc2	zalétanost
akrobatické	akrobatický	k2eAgFnSc2d1	akrobatická
skupiny	skupina	k1gFnSc2	skupina
Biele	Biele	k1gFnSc2	Biele
Albatrosy	albatros	k1gMnPc7	albatros
dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1997	[number]	k4	1997
<g/>
,	,	kIx,	,
po	po	k7c6	po
srážce	srážka	k1gFnSc6	srážka
v	v	k7c6	v
turbulenci	turbulence	k1gFnSc6	turbulence
<g/>
,	,	kIx,	,
havaroval	havarovat	k5eAaPmAgMnS	havarovat
L-39C	L-39C	k1gMnSc1	L-39C
s	s	k7c7	s
ev.	ev.	k?	ev.
č.	č.	k?	č.
4357	[number]	k4	4357
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc1	jeho
piloti	pilot	k1gMnPc1	pilot
–	–	k?	–
pplk.	pplk.	kA	pplk.
Marián	Marián	k1gMnSc1	Marián
Sakáč	Sakáč	k1gMnSc1	Sakáč
a	a	k8xC	a
kpt.	kpt.	k?	kpt.
Róbert	Róbert	k1gMnSc1	Róbert
Rozenberg	Rozenberg	k1gMnSc1	Rozenberg
se	se	k3xPyFc4	se
katapultovali	katapultovat	k5eAaBmAgMnP	katapultovat
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
Bílé	bílý	k2eAgMnPc4d1	bílý
Albatrosy	albatros	k1gMnPc4	albatros
přišla	přijít	k5eAaPmAgFnS	přijít
o	o	k7c4	o
další	další	k2eAgFnSc4d1	další
L-39C	L-39C	k1gFnSc4	L-39C
s	s	k7c7	s
ev.	ev.	k?	ev.
č.	č.	k?	č.
4355	[number]	k4	4355
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
Sliač	Sliač	k1gInSc4	Sliač
dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
během	během	k7c2	během
jejich	jejich	k3xOp3gNnSc2	jejich
vystoupení	vystoupení	k1gNnSc2	vystoupení
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
nehodě	nehoda	k1gFnSc6	nehoda
zahynul	zahynout	k5eAaPmAgMnS	zahynout
pilot	pilot	k1gMnSc1	pilot
mjr.	mjr.	kA	mjr.
Ľuboš	Ľuboš	k1gMnSc1	Ľuboš
Novák	Novák	k1gMnSc1	Novák
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2000	[number]	k4	2000
se	se	k3xPyFc4	se
zřítil	zřítit	k5eAaPmAgInS	zřítit
L-39ZA	L-39ZA	k1gFnSc7	L-39ZA
s	s	k7c7	s
ev.	ev.	k?	ev.
č.	č.	k?	č.
3905	[number]	k4	3905
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Ožďany	Ožďana	k1gFnSc2	Ožďana
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
jeho	jeho	k3xOp3gMnSc1	jeho
pilot	pilot	k1gMnSc1	pilot
kpt.	kpt.	k?	kpt.
Pavel	Pavel	k1gMnSc1	Pavel
Serbín	Serbín	k1gMnSc1	Serbín
se	se	k3xPyFc4	se
katapultoval	katapultovat	k5eAaBmAgMnS	katapultovat
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
29	[number]	k4	29
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2002	[number]	k4	2002
se	se	k3xPyFc4	se
během	během	k7c2	během
výcviku	výcvik	k1gInSc2	výcvik
pilotů	pilot	k1gMnPc2	pilot
maďarské	maďarský	k2eAgFnSc2d1	maďarská
národnosti	národnost	k1gFnSc2	národnost
zřítilo	zřítit	k5eAaPmAgNnS	zřítit
letadlo	letadlo	k1gNnSc1	letadlo
L-39ZA	L-39ZA	k1gFnSc2	L-39ZA
s	s	k7c7	s
ev.	ev.	k?	ev.
č.	č.	k?	č.
4705	[number]	k4	4705
<g/>
.	.	kIx.	.
</s>
<s>
Nehoda	nehoda	k1gFnSc1	nehoda
se	se	k3xPyFc4	se
odehrála	odehrát	k5eAaPmAgFnS	odehrát
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Kalsa	Kalsa	k1gFnSc1	Kalsa
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc1	jeho
piloti	pilot	k1gMnPc1	pilot
–	–	k?	–
instruktor	instruktor	k1gMnSc1	instruktor
Jenő	Jenő	k1gMnSc1	Jenő
Vadaš	Vadaš	k1gMnSc1	Vadaš
a	a	k8xC	a
žák	žák	k1gMnSc1	žák
Gyula	Gyula	k1gMnSc1	Gyula
Molnár	Molnár	k1gMnSc1	Molnár
se	se	k3xPyFc4	se
katapultovali	katapultovat	k5eAaBmAgMnP	katapultovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Syrská	syrský	k2eAgFnSc1d1	Syrská
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
===	===	k?	===
</s>
</p>
<p>
<s>
Během	během	k7c2	během
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
nasadila	nasadit	k5eAaPmAgFnS	nasadit
armáda	armáda	k1gFnSc1	armáda
při	při	k7c6	při
bojích	boj	k1gInPc6	boj
v	v	k7c6	v
Aleppu	Alepp	k1gInSc6	Alepp
proti	proti	k7c3	proti
povstalcům	povstalec	k1gMnPc3	povstalec
také	také	k6eAd1	také
letouny	letoun	k1gInPc4	letoun
L-39	L-39	k1gFnSc2	L-39
vyzbrojené	vyzbrojený	k2eAgFnSc2d1	vyzbrojená
raketami	raketa	k1gFnPc7	raketa
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2017	[number]	k4	2017
byl	být	k5eAaImAgMnS	být
syrský	syrský	k2eAgMnSc1d1	syrský
L-39	L-39	k1gMnSc1	L-39
sestřelen	sestřelit	k5eAaPmNgMnS	sestřelit
přenosnou	přenosný	k2eAgFnSc7d1	přenosná
protiletadlovou	protiletadlový	k2eAgFnSc7d1	protiletadlová
raketou	raketa	k1gFnSc7	raketa
nedaleko	nedaleko	k7c2	nedaleko
letiště	letiště	k1gNnSc2	letiště
Hama	Hama	kA	Hama
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Varianty	variant	k1gInPc4	variant
==	==	k?	==
</s>
</p>
<p>
<s>
L-39X-02	L-39X-02	k4	L-39X-02
-	-	kIx~	-
X	X	kA	X
<g/>
11	[number]	k4	11
<g/>
Vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
10	[number]	k4	10
prototypů	prototyp	k1gInPc2	prototyp
<g/>
.	.	kIx.	.
</s>
<s>
X-08	X-08	k4	X-08
(	(	kIx(	(
<g/>
3908	[number]	k4	3908
<g/>
)	)	kIx)	)
<g/>
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1972	[number]	k4	1972
byl	být	k5eAaImAgInS	být
prototyp	prototyp	k1gInSc1	prototyp
L-39V	L-39V	k1gFnSc2	L-39V
a	a	k8xC	a
X-09	X-09	k1gFnSc2	X-09
(	(	kIx(	(
<g/>
3909	[number]	k4	3909
<g/>
)	)	kIx)	)
zálet	zálet	k1gInSc1	zálet
25	[number]	k4	25
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1975	[number]	k4	1975
prototyp	prototyp	k1gInSc1	prototyp
L-	L-	k1gFnSc2	L-
<g/>
39	[number]	k4	39
<g/>
Z.L	Z.L	k1gFnPc2	Z.L
<g/>
-	-	kIx~	-
<g/>
39	[number]	k4	39
<g/>
C	C	kA	C
(	(	kIx(	(
<g/>
C	C	kA	C
–	–	k?	–
Cvičná	cvičný	k2eAgFnSc1d1	cvičná
<g/>
)	)	kIx)	)
<g/>
Standardní	standardní	k2eAgFnSc1d1	standardní
výrobní	výrobní	k2eAgFnSc1d1	výrobní
verze	verze	k1gFnSc1	verze
<g/>
<g />
.	.	kIx.	.
</s>
<s>
<g/>
L-	L-	k1gFnSc1	L-
<g/>
39	[number]	k4	39
<g/>
V	V	kA	V
(	(	kIx(	(
<g/>
V	v	k7c6	v
–	–	k?	–
Vlečná	vlečný	k2eAgFnSc1d1	vlečná
<g/>
)	)	kIx)	)
<g/>
Jednosedadlová	jednosedadlový	k2eAgFnSc1d1	jednosedadlová
verze	verze	k1gFnSc1	verze
pro	pro	k7c4	pro
vlečení	vlečení	k1gNnSc4	vlečení
cvičných	cvičný	k2eAgInPc2d1	cvičný
terčů	terč	k1gInPc2	terč
KT-	KT-	k1gFnSc2	KT-
<g/>
0	[number]	k4	0
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
osm	osm	k4xCc1	osm
vyrobených	vyrobený	k2eAgInPc2d1	vyrobený
kusů	kus	k1gInPc2	kus
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
verzi	verze	k1gFnSc6	verze
<g/>
.	.	kIx.	.
<g/>
L-	L-	k1gFnSc1	L-
<g/>
39	[number]	k4	39
<g/>
ZO	ZO	kA	ZO
(	(	kIx(	(
<g/>
ZO	ZO	kA	ZO
–	–	k?	–
Zahraniční	zahraniční	k2eAgInSc4d1	zahraniční
Obchod	obchod	k1gInSc4	obchod
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Verze	verze	k1gFnSc1	verze
se	s	k7c7	s
čtyřmi	čtyři	k4xCgInPc7	čtyři
závěsníky	závěsník	k1gInPc7	závěsník
pro	pro	k7c4	pro
zbraňové	zbraňový	k2eAgInPc4d1	zbraňový
systémy	systém	k1gInPc4	systém
a	a	k8xC	a
zesílenou	zesílený	k2eAgFnSc7d1	zesílená
konstrukcí	konstrukce	k1gFnSc7	konstrukce
křídla	křídlo	k1gNnSc2	křídlo
<g/>
.	.	kIx.	.
<g/>
L-	L-	k1gFnPc2	L-
<g/>
39	[number]	k4	39
<g/>
ZAVýznamně	ZAVýznamně	k1gFnSc4	ZAVýznamně
vylepšená	vylepšený	k2eAgFnSc1d1	vylepšená
verze	verze	k1gFnSc1	verze
L-39ZO	L-39ZO	k1gFnSc2	L-39ZO
se	s	k7c7	s
silnějším	silný	k2eAgInSc7d2	silnější
podvozkem	podvozek	k1gInSc7	podvozek
<g/>
,	,	kIx,	,
vyššími	vysoký	k2eAgInPc7d2	vyšší
provozními	provozní	k2eAgInPc7d1	provozní
náklady	náklad	k1gInPc7	náklad
a	a	k8xC	a
svěšení	svěšení	k1gNnSc1	svěšení
dvouhlavňovým	dvouhlavňový	k2eAgMnSc7d1	dvouhlavňový
rychlopalných	rychlopalný	k2eAgFnPc2d1	rychlopalná
kanónem	kanón	k1gInSc7	kanón
GS-23	GS-23	k1gMnSc2	GS-23
ráže	ráže	k1gFnSc2	ráže
23	[number]	k4	23
mm	mm	kA	mm
se	s	k7c7	s
zásobníkem	zásobník	k1gInSc7	zásobník
nábojů	náboj	k1gInPc2	náboj
integrovaným	integrovaný	k2eAgInPc3d1	integrovaný
v	v	k7c6	v
trupu	trup	k1gInSc6	trup
<g/>
.	.	kIx.	.
<g/>
L-	L-	k1gFnSc1	L-
<g/>
39	[number]	k4	39
<g/>
Z	Z	kA	Z
<g/>
/	/	kIx~	/
<g/>
ARTThajská	ARTThajský	k2eAgFnSc1d1	ARTThajský
verze	verze	k1gFnSc1	verze
s	s	k7c7	s
avionikou	avionika	k1gFnSc7	avionika
Elbit	Elbit	k1gMnSc1	Elbit
<g/>
.	.	kIx.	.
<g/>
L-	L-	k1gFnSc1	L-
<g/>
39	[number]	k4	39
<g/>
MSL-	MSL-	k1gFnSc1	MSL-
<g/>
39	[number]	k4	39
<g/>
MS	MS	kA	MS
je	být	k5eAaImIp3nS	být
vojenské	vojenský	k2eAgNnSc4d1	vojenské
cvičné	cvičný	k2eAgNnSc4d1	cvičné
letadlo	letadlo	k1gNnSc4	letadlo
vyvíjené	vyvíjený	k2eAgNnSc4d1	vyvíjené
z	z	k7c2	z
L-	L-	k1gFnSc2	L-
<g/>
39	[number]	k4	39
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
původním	původní	k2eAgMnSc7d1	původní
Albatrosem	albatros	k1gMnSc7	albatros
má	mít	k5eAaImIp3nS	mít
L-39MS	L-39MS	k1gMnSc1	L-39MS
zesílený	zesílený	k2eAgInSc4d1	zesílený
trup	trup	k1gInSc4	trup
<g/>
,	,	kIx,	,
delší	dlouhý	k2eAgFnSc4d2	delší
příď	příď	k1gFnSc4	příď
<g/>
,	,	kIx,	,
modernizovaný	modernizovaný	k2eAgInSc4d1	modernizovaný
kokpit	kokpit	k1gInSc4	kokpit
a	a	k8xC	a
výkonnější	výkonný	k2eAgInSc1d2	výkonnější
motor	motor	k1gInSc1	motor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
prvního	první	k4xOgInSc2	první
letu	let	k1gInSc2	let
<g/>
,	,	kIx,	,
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1986	[number]	k4	1986
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
také	také	k9	také
nazýván	nazývat	k5eAaImNgInS	nazývat
L-	L-	k1gFnSc7	L-
<g/>
59	[number]	k4	59
<g/>
.	.	kIx.	.
<g/>
L-	L-	k1gFnSc2	L-
<g/>
39	[number]	k4	39
<g/>
NGProjekt	NGProjekt	k1gInSc1	NGProjekt
modernizace	modernizace	k1gFnSc2	modernizace
stávajících	stávající	k2eAgInPc2d1	stávající
letounů	letoun	k1gInPc2	letoun
na	na	k7c6	na
verzi	verze	k1gFnSc6	verze
L-	L-	k1gMnSc2	L-
<g/>
39	[number]	k4	39
<g/>
NG	NG	kA	NG
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
počítá	počítat	k5eAaImIp3nS	počítat
také	také	k9	také
s	s	k7c7	s
výrobou	výroba	k1gFnSc7	výroba
nových	nový	k2eAgInPc2d1	nový
strojů	stroj	k1gInPc2	stroj
<g/>
.	.	kIx.	.
<g/>
L-	L-	k1gFnSc2	L-
<g/>
39	[number]	k4	39
<g/>
CWPřestavba	CWPřestavba	k1gFnSc1	CWPřestavba
starších	starý	k2eAgInPc2d2	starší
letounů	letoun	k1gInPc2	letoun
L-39	L-39	k1gFnSc2	L-39
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
vybavení	vybavení	k1gNnSc2	vybavení
motorem	motor	k1gInSc7	motor
FJ44-4M	FJ44-4M	k1gMnSc2	FJ44-4M
a	a	k8xC	a
moderní	moderní	k2eAgFnSc7d1	moderní
avionikou	avionika	k1gFnSc7	avionika
<g/>
.	.	kIx.	.
</s>
<s>
Dokončení	dokončení	k1gNnSc1	dokončení
vývoje	vývoj	k1gInSc2	vývoj
L-39CW	L-39CW	k1gMnPc2	L-39CW
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Přestavbu	přestavba	k1gFnSc4	přestavba
šesti	šest	k4xCc2	šest
strojů	stroj	k1gInPc2	stroj
na	na	k7c6	na
verzi	verze	k1gFnSc6	verze
L-39CW	L-39CW	k1gFnSc6	L-39CW
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
objednala	objednat	k5eAaPmAgFnS	objednat
americká	americký	k2eAgFnSc1d1	americká
společnost	společnost	k1gFnSc1	společnost
RSW	RSW	kA	RSW
Aviation	Aviation	k1gInSc1	Aviation
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Technický	technický	k2eAgInSc1d1	technický
popis	popis	k1gInSc1	popis
==	==	k?	==
</s>
</p>
<p>
<s>
L-39C	L-39C	k4	L-39C
je	být	k5eAaImIp3nS	být
celokovový	celokovový	k2eAgInSc1d1	celokovový
dvoumístný	dvoumístný	k2eAgInSc1d1	dvoumístný
dolnoplošník	dolnoplošník	k1gInSc1	dolnoplošník
poloskořepinové	poloskořepinový	k2eAgFnSc2d1	poloskořepinový
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Trup	trup	k1gInSc1	trup
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
tří	tři	k4xCgFnPc2	tři
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
přední	přední	k2eAgNnSc4d1	přední
je	být	k5eAaImIp3nS	být
uloženo	uložit	k5eAaPmNgNnS	uložit
radiovybavení	radiovybavení	k1gNnSc1	radiovybavení
<g/>
,	,	kIx,	,
přetlaková	přetlakový	k2eAgFnSc1d1	přetlaková
kabina	kabina	k1gFnSc1	kabina
posádky	posádka	k1gFnSc2	posádka
a	a	k8xC	a
palivové	palivový	k2eAgFnSc2d1	palivová
nádrže	nádrž	k1gFnSc2	nádrž
(	(	kIx(	(
<g/>
1100	[number]	k4	1100
l	l	kA	l
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
části	část	k1gFnSc6	část
je	být	k5eAaImIp3nS	být
uložen	uložen	k2eAgInSc1d1	uložen
motor	motor	k1gInSc1	motor
AI-	AI-	k1gFnSc1	AI-
<g/>
25	[number]	k4	25
<g/>
TL	TL	kA	TL
<g/>
.	.	kIx.	.
</s>
<s>
Zadní	zadní	k2eAgFnSc1d1	zadní
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgFnPc4	který
jsou	být	k5eAaImIp3nP	být
uchyceny	uchycen	k2eAgFnPc4d1	uchycena
ocasní	ocasní	k2eAgFnPc4d1	ocasní
plochy	plocha	k1gFnPc4	plocha
lze	lze	k6eAd1	lze
pro	pro	k7c4	pro
montáž	montáž	k1gFnSc4	montáž
motoru	motor	k1gInSc2	motor
odpojit	odpojit	k5eAaPmF	odpojit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Křídlo	křídlo	k1gNnSc1	křídlo
je	být	k5eAaImIp3nS	být
přímé	přímý	k2eAgNnSc1d1	přímé
<g/>
,	,	kIx,	,
průběžné	průběžný	k2eAgFnPc1d1	průběžná
<g/>
,	,	kIx,	,
lichoběžníkového	lichoběžníkový	k2eAgInSc2d1	lichoběžníkový
tvaru	tvar	k1gInSc2	tvar
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
koncích	konec	k1gInPc6	konec
jsou	být	k5eAaImIp3nP	být
upevněny	upevnit	k5eAaPmNgFnP	upevnit
okrajové	okrajový	k2eAgFnPc1d1	okrajová
palivové	palivový	k2eAgFnPc1d1	palivová
nádrže	nádrž	k1gFnPc1	nádrž
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
x	x	k?	x
<g/>
150	[number]	k4	150
l	l	kA	l
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Křídlo	křídlo	k1gNnSc1	křídlo
je	být	k5eAaImIp3nS	být
opatřeno	opatřit	k5eAaPmNgNnS	opatřit
dvojštěrbinovými	dvojštěrbinový	k2eAgFnPc7d1	dvojštěrbinový
vztlakovými	vztlakový	k2eAgFnPc7d1	vztlaková
klapkami	klapka	k1gFnPc7	klapka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
spodní	spodní	k2eAgFnSc6d1	spodní
straně	strana	k1gFnSc6	strana
jsou	být	k5eAaImIp3nP	být
dva	dva	k4xCgInPc4	dva
brzdící	brzdící	k2eAgInPc4d1	brzdící
štíty	štít	k1gInPc4	štít
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
automaticky	automaticky	k6eAd1	automaticky
vysouvají	vysouvat	k5eAaImIp3nP	vysouvat
při	při	k7c6	při
překročení	překročení	k1gNnSc6	překročení
Machova	Machův	k2eAgNnSc2d1	Machovo
čísla	číslo	k1gNnSc2	číslo
0,78	[number]	k4	0,78
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Řízení	řízení	k1gNnSc1	řízení
je	být	k5eAaImIp3nS	být
klasické	klasický	k2eAgNnSc1d1	klasické
bez	bez	k7c2	bez
hydraulických	hydraulický	k2eAgInPc2d1	hydraulický
posilovačů	posilovač	k1gInPc2	posilovač
<g/>
,	,	kIx,	,
letoun	letoun	k1gInSc1	letoun
má	mít	k5eAaImIp3nS	mít
podélné	podélný	k2eAgNnSc4d1	podélné
a	a	k8xC	a
příčné	příčný	k2eAgNnSc4d1	příčné
vyvážení	vyvážení	k1gNnSc4	vyvážení
elektricky	elektricky	k6eAd1	elektricky
stavitelnými	stavitelný	k2eAgFnPc7d1	stavitelná
ploškami	ploška	k1gFnPc7	ploška
na	na	k7c6	na
kormidlech	kormidlo	k1gNnPc6	kormidlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podvozek	podvozek	k1gInSc1	podvozek
je	být	k5eAaImIp3nS	být
zatažitelný	zatažitelný	k2eAgInSc1d1	zatažitelný
tříbodový	tříbodový	k2eAgInSc1d1	tříbodový
příďového	příďový	k2eAgInSc2d1	příďový
typu	typ	k1gInSc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Příďový	příďový	k2eAgInSc1d1	příďový
podvozek	podvozek	k1gInSc1	podvozek
je	být	k5eAaImIp3nS	být
neřiditelný	řiditelný	k2eNgInSc1d1	neřiditelný
<g/>
,	,	kIx,	,
zasouvá	zasouvat	k5eAaImIp3nS	zasouvat
se	se	k3xPyFc4	se
dopředu	dopředu	k6eAd1	dopředu
do	do	k7c2	do
trupu	trup	k1gInSc2	trup
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
opatřen	opatřit	k5eAaPmNgInS	opatřit
tlumičem	tlumič	k1gInSc7	tlumič
bočních	boční	k2eAgInPc2d1	boční
kmitů	kmit	k1gInPc2	kmit
<g/>
.	.	kIx.	.
</s>
<s>
Nohy	noha	k1gFnPc1	noha
hlavního	hlavní	k2eAgInSc2d1	hlavní
podvozku	podvozek	k1gInSc2	podvozek
se	se	k3xPyFc4	se
zasouvají	zasouvat	k5eAaImIp3nP	zasouvat
k	k	k7c3	k
sobě	se	k3xPyFc3	se
do	do	k7c2	do
trupu	trup	k1gInSc2	trup
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc1	dva
kola	kolo	k1gNnPc1	kolo
jsou	být	k5eAaImIp3nP	být
brzděná	brzděný	k2eAgNnPc1d1	brzděné
<g/>
,	,	kIx,	,
brzdy	brzda	k1gFnPc1	brzda
mají	mít	k5eAaImIp3nP	mít
protismykový	protismykový	k2eAgInSc4d1	protismykový
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pohonná	pohonný	k2eAgFnSc1d1	pohonná
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
:	:	kIx,	:
Motor	motor	k1gInSc1	motor
AI-	AI-	k1gFnSc2	AI-
<g/>
25	[number]	k4	25
<g/>
TL	TL	kA	TL
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
spouštění	spouštění	k1gNnSc3	spouštění
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
turbostartér	turbostartér	k1gMnSc1	turbostartér
Saphir-	Saphir-	k1gFnSc2	Saphir-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hydraulická	hydraulický	k2eAgFnSc1d1	hydraulická
soustava	soustava	k1gFnSc1	soustava
je	být	k5eAaImIp3nS	být
použita	použít	k5eAaPmNgFnS	použít
k	k	k7c3	k
ovládání	ovládání	k1gNnSc3	ovládání
vztlakových	vztlakový	k2eAgFnPc2d1	vztlaková
klapek	klapka	k1gFnPc2	klapka
<g/>
,	,	kIx,	,
brzdících	brzdící	k2eAgInPc2d1	brzdící
štítů	štít	k1gInPc2	štít
<g/>
,	,	kIx,	,
brzd	brzda	k1gFnPc2	brzda
a	a	k8xC	a
náporové	náporový	k2eAgFnSc2d1	náporová
turbínky	turbínka	k1gFnSc2	turbínka
záložního	záložní	k2eAgNnSc2d1	záložní
dynama	dynamo	k1gNnSc2	dynamo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Elektrická	elektrický	k2eAgFnSc1d1	elektrická
soustava	soustava	k1gFnSc1	soustava
je	být	k5eAaImIp3nS	být
stejnosměrná	stejnosměrný	k2eAgFnSc1d1	stejnosměrná
28	[number]	k4	28
V.	V.	kA	V.
Hlavním	hlavní	k2eAgInSc7d1	hlavní
zdrojem	zdroj	k1gInSc7	zdroj
je	být	k5eAaImIp3nS	být
dynamo	dynamo	k1gNnSc1	dynamo
poháněné	poháněný	k2eAgNnSc1d1	poháněné
od	od	k7c2	od
motoru	motor	k1gInSc2	motor
<g/>
,	,	kIx,	,
záložním	záložní	k2eAgInSc7d1	záložní
dynamo	dynamo	k1gNnSc4	dynamo
poháněné	poháněný	k2eAgInPc1d1	poháněný
náporovou	náporový	k2eAgFnSc7d1	náporová
turbínkou	turbínka	k1gFnSc7	turbínka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
při	při	k7c6	při
vysazení	vysazení	k1gNnSc6	vysazení
hlavního	hlavní	k2eAgNnSc2d1	hlavní
dynama	dynamo	k1gNnSc2	dynamo
automaticky	automaticky	k6eAd1	automaticky
vysune	vysunout	k5eAaPmIp3nS	vysunout
pod	pod	k7c7	pod
trupem	trup	k1gInSc7	trup
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
transformaci	transformace	k1gFnSc4	transformace
energie	energie	k1gFnSc2	energie
pro	pro	k7c4	pro
některé	některý	k3yIgInPc4	některý
spotřebiče	spotřebič	k1gInPc4	spotřebič
jsou	být	k5eAaImIp3nP	být
určeny	určit	k5eAaPmNgInP	určit
dva	dva	k4xCgInPc1	dva
měniče	měnič	k1gInPc1	měnič
115	[number]	k4	115
V	V	kA	V
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
3	[number]	k4	3
x	x	k?	x
36	[number]	k4	36
V.	V.	kA	V.
</s>
</p>
<p>
<s>
Výzbroj	výzbroj	k1gFnSc1	výzbroj
<g/>
:	:	kIx,	:
verze	verze	k1gFnSc1	verze
C	C	kA	C
může	moct	k5eAaImIp3nS	moct
nést	nést	k5eAaImF	nést
pod	pod	k7c7	pod
každým	každý	k3xTgNnSc7	každý
křídlem	křídlo	k1gNnSc7	křídlo
jeden	jeden	k4xCgInSc1	jeden
závěsník	závěsník	k1gInSc1	závěsník
s	s	k7c7	s
nosností	nosnost	k1gFnSc7	nosnost
125	[number]	k4	125
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
lze	lze	k6eAd1	lze
podvěsit	podvěsit	k5eAaPmF	podvěsit
pumu	puma	k1gFnSc4	puma
<g/>
,	,	kIx,	,
raketový	raketový	k2eAgInSc1d1	raketový
blok	blok	k1gInSc1	blok
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
neřízenou	řízený	k2eNgFnSc4d1	neřízená
protileteckou	protiletecký	k2eAgFnSc4d1	protiletecká
střelu	střela	k1gFnSc4	střela
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Systém	systém	k1gInSc1	systém
záchrany	záchrana	k1gFnSc2	záchrana
posádky	posádka	k1gFnSc2	posádka
<g/>
:	:	kIx,	:
letoun	letoun	k1gInSc1	letoun
je	být	k5eAaImIp3nS	být
opatřen	opatřit	k5eAaPmNgInS	opatřit
vystřelovacími	vystřelovací	k2eAgFnPc7d1	vystřelovací
sedadly	sedadlo	k1gNnPc7	sedadlo
VS-1BRI	VS-1BRI	k1gMnSc2	VS-1BRI
československé	československý	k2eAgFnSc2d1	Československá
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Umožňují	umožňovat	k5eAaImIp3nP	umožňovat
záchranu	záchrana	k1gFnSc4	záchrana
z	z	k7c2	z
nulové	nulový	k2eAgFnSc2d1	nulová
výšky	výška	k1gFnSc2	výška
při	při	k7c6	při
rychlosti	rychlost	k1gFnSc6	rychlost
min	mina	k1gFnPc2	mina
<g/>
.	.	kIx.	.
150	[number]	k4	150
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h.	h.	k?	h.
</s>
</p>
<p>
<s>
==	==	k?	==
Uživatelé	uživatel	k1gMnPc1	uživatel
==	==	k?	==
</s>
</p>
<p>
<s>
Tabulka	tabulka	k1gFnSc1	tabulka
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
přehled	přehled	k1gInSc4	přehled
vojenských	vojenský	k2eAgMnPc2d1	vojenský
uživatelů	uživatel	k1gMnPc2	uživatel
L-39	L-39	k1gFnSc2	L-39
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
podle	podle	k7c2	podle
ročenky	ročenka	k1gFnSc2	ročenka
Flightglobal	Flightglobal	k1gFnSc2	Flightglobal
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Současní	současný	k2eAgMnPc1d1	současný
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Bývalí	bývalý	k2eAgMnPc1d1	bývalý
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Specifikace	specifikace	k1gFnSc1	specifikace
(	(	kIx(	(
<g/>
L-	L-	k1gFnSc1	L-
<g/>
39	[number]	k4	39
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Technické	technický	k2eAgInPc1d1	technický
údaje	údaj	k1gInPc1	údaj
===	===	k?	===
</s>
</p>
<p>
<s>
Posádka	posádka	k1gFnSc1	posádka
<g/>
:	:	kIx,	:
2	[number]	k4	2
(	(	kIx(	(
<g/>
instruktor	instruktor	k1gMnSc1	instruktor
a	a	k8xC	a
žák	žák	k1gMnSc1	žák
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
<g/>
:	:	kIx,	:
12,13	[number]	k4	12,13
m	m	kA	m
</s>
</p>
<p>
<s>
Rozpětí	rozpětí	k1gNnSc1	rozpětí
<g/>
:	:	kIx,	:
9,46	[number]	k4	9,46
m	m	kA	m
</s>
</p>
<p>
<s>
Výška	výška	k1gFnSc1	výška
<g/>
:	:	kIx,	:
4,77	[number]	k4	4,77
m	m	kA	m
</s>
</p>
<p>
<s>
Nosná	nosný	k2eAgFnSc1d1	nosná
plocha	plocha	k1gFnSc1	plocha
<g/>
:	:	kIx,	:
18,8	[number]	k4	18,8
m2	m2	k4	m2
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
křídla	křídlo	k1gNnSc2	křídlo
<g/>
:	:	kIx,	:
NACA	NACA	kA	NACA
64A012	[number]	k4	64A012
mod	mod	k?	mod
</s>
</p>
<p>
<s>
Prázdná	prázdný	k2eAgFnSc1d1	prázdná
hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
:	:	kIx,	:
3	[number]	k4	3
459	[number]	k4	459
kg	kg	kA	kg
</s>
</p>
<p>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
vzletová	vzletový	k2eAgFnSc1d1	vzletová
hmotnost	hmotnost	k1gFnSc1	hmotnost
:	:	kIx,	:
4	[number]	k4	4
700	[number]	k4	700
kg	kg	kA	kg
</s>
</p>
<p>
<s>
Pohonná	pohonný	k2eAgFnSc1d1	pohonná
jednotka	jednotka	k1gFnSc1	jednotka
<g/>
:	:	kIx,	:
1	[number]	k4	1
<g/>
×	×	k?	×
dvouproudový	dvouproudový	k2eAgInSc1d1	dvouproudový
motor	motor	k1gInSc1	motor
Progress	Progressa	k1gFnPc2	Progressa
<g/>
/	/	kIx~	/
<g/>
Ivčenko	Ivčenka	k1gFnSc5	Ivčenka
AI-25TL	AI-25TL	k1gFnPc3	AI-25TL
</s>
</p>
<p>
<s>
Tah	tah	k1gInSc1	tah
pohonné	pohonný	k2eAgFnSc2d1	pohonná
jednotky	jednotka	k1gFnSc2	jednotka
<g/>
:	:	kIx,	:
16,9	[number]	k4	16,9
kN	kN	k?	kN
</s>
</p>
<p>
<s>
===	===	k?	===
Výkony	výkon	k1gInPc1	výkon
===	===	k?	===
</s>
</p>
<p>
<s>
Maximální	maximální	k2eAgFnSc1d1	maximální
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
:	:	kIx,	:
750	[number]	k4	750
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
5	[number]	k4	5
000	[number]	k4	000
m	m	kA	m
</s>
</p>
<p>
<s>
Dolet	dolet	k1gInSc1	dolet
1	[number]	k4	1
100	[number]	k4	100
km	km	kA	km
v	v	k7c4	v
5	[number]	k4	5
000	[number]	k4	000
m	m	kA	m
</s>
</p>
<p>
<s>
1	[number]	k4	1
750	[number]	k4	750
km	km	kA	km
s	s	k7c7	s
přídavnými	přídavný	k2eAgFnPc7d1	přídavná
nádržemi	nádrž	k1gFnPc7	nádrž
</s>
</p>
<p>
<s>
Praktický	praktický	k2eAgInSc1d1	praktický
Dostup	dostup	k1gInSc1	dostup
<g/>
:	:	kIx,	:
11	[number]	k4	11
500	[number]	k4	500
m	m	kA	m
</s>
</p>
<p>
<s>
Počáteční	počáteční	k2eAgFnSc1d1	počáteční
Stoupavost	stoupavost	k1gFnSc1	stoupavost
<g/>
:	:	kIx,	:
22	[number]	k4	22
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
</s>
</p>
<p>
<s>
Plošné	plošný	k2eAgNnSc1d1	plošné
zatížení	zatížení	k1gNnSc1	zatížení
250,0	[number]	k4	250,0
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
m2	m2	k4	m2
</s>
</p>
<p>
<s>
Tah	tah	k1gInSc1	tah
<g/>
/	/	kIx~	/
<g/>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
<g/>
:	:	kIx,	:
0,37	[number]	k4	0,37
</s>
</p>
<p>
<s>
Vzletová	vzletový	k2eAgFnSc1d1	vzletová
dráha	dráha	k1gFnSc1	dráha
<g/>
:	:	kIx,	:
480	[number]	k4	480
m	m	kA	m
</s>
</p>
<p>
<s>
Přistávací	přistávací	k2eAgFnSc1d1	přistávací
dráha	dráha	k1gFnSc1	dráha
<g/>
:	:	kIx,	:
600	[number]	k4	600
m	m	kA	m
</s>
</p>
<p>
<s>
===	===	k?	===
Výzbroj	výzbroj	k1gInSc4	výzbroj
===	===	k?	===
</s>
</p>
<p>
<s>
Do	do	k7c2	do
284	[number]	k4	284
kg	kg	kA	kg
munice	munice	k1gFnSc2	munice
na	na	k7c6	na
dvou	dva	k4xCgInPc6	dva
externích	externí	k2eAgInPc6d1	externí
pylonech	pylon	k1gInPc6	pylon
</s>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
×	×	k?	×
přídavná	přídavný	k2eAgFnSc1d1	přídavná
nádrž	nádrž	k1gFnSc1	nádrž
na	na	k7c6	na
křídle	křídlo	k1gNnSc6	křídlo
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavosti	zajímavost	k1gFnPc1	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Podrobné	podrobný	k2eAgFnPc1d1	podrobná
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
zajimavostech	zajimavost	k1gFnPc6	zajimavost
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgInP	uvést
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
Zajímavosti	zajímavost	k1gFnSc2	zajímavost
o	o	k7c4	o
L-39	L-39	k1gFnSc4	L-39
Albatros	albatros	k1gMnSc1	albatros
</s>
</p>
<p>
<s>
===	===	k?	===
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
===	===	k?	===
</s>
</p>
<p>
<s>
Pod	pod	k7c7	pod
nohama	noha	k1gFnPc7	noha
nebe	nebe	k1gNnSc2	nebe
-	-	kIx~	-
český	český	k2eAgInSc1d1	český
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
<g/>
.	.	kIx.	.
</s>
<s>
L-39	L-39	k4	L-39
zde	zde	k6eAd1	zde
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
natočení	natočení	k1gNnSc4	natočení
záběrů	záběr	k1gInPc2	záběr
z	z	k7c2	z
kabiny	kabina	k1gFnSc2	kabina
MIG	mig	k1gInSc1	mig
21	[number]	k4	21
</s>
</p>
<p>
<s>
Zítřek	zítřek	k1gInSc1	zítřek
nikdy	nikdy	k6eAd1	nikdy
neumírá	umírat	k5eNaImIp3nS	umírat
-	-	kIx~	-
film	film	k1gInSc4	film
USA	USA	kA	USA
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
osmnáctá	osmnáctý	k4xOgFnSc1	osmnáctý
bondovka	bondovka	k?	bondovka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obchodník	obchodník	k1gMnSc1	obchodník
se	s	k7c7	s
smrtí	smrt	k1gFnSc7	smrt
-	-	kIx~	-
film	film	k1gInSc1	film
USA	USA	kA	USA
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Ve	v	k7c6	v
hrách	hra	k1gFnPc6	hra
===	===	k?	===
</s>
</p>
<p>
<s>
Grand	grand	k1gMnSc1	grand
Theft	Theft	k1gMnSc1	Theft
Auto	auto	k1gNnSc4	auto
V	v	k7c4	v
-	-	kIx~	-
videohra	videohra	k1gFnSc1	videohra
USA	USA	kA	USA
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Besra	Besr	k1gInSc2	Besr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Digital	Digital	kA	Digital
Combat	Combat	k1gMnSc1	Combat
Simulator	Simulator	k1gMnSc1	Simulator
<g/>
:	:	kIx,	:
L-39	L-39	k1gMnSc1	L-39
-	-	kIx~	-
videohra	videohra	k1gFnSc1	videohra
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Aero	aero	k1gNnSc1	aero
L-39	L-39	k1gMnSc1	L-39
Albatros	albatros	k1gMnSc1	albatros
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
L-39	L-39	k1gMnSc1	L-39
Albatros	albatros	k1gMnSc1	albatros
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
L-39	L-39	k4	L-39
Specifikace	specifikace	k1gFnSc1	specifikace
výrobce	výrobce	k1gMnPc4	výrobce
</s>
</p>
<p>
<s>
L-39	L-39	k4	L-39
Výcvikový	výcvikový	k2eAgInSc1d1	výcvikový
systém	systém	k1gInSc1	systém
–	–	k?	–
podrobná	podrobný	k2eAgNnPc1d1	podrobné
materiály	materiál	k1gInPc1	materiál
o	o	k7c6	o
všem	všecek	k3xTgNnSc6	všecek
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
letounu	letoun	k1gInSc2	letoun
L-39	L-39	k1gFnPc2	L-39
Albatros	albatros	k1gMnSc1	albatros
</s>
</p>
<p>
<s>
L-39	L-39	k4	L-39
Enthusiasts	Enthusiasts	k1gInSc1	Enthusiasts
</s>
</p>
<p>
<s>
Biele	Biele	k6eAd1	Biele
Albatrosy	albatros	k1gMnPc7	albatros
–	–	k?	–
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
oficiální	oficiální	k2eAgFnSc1d1	oficiální
akrobatická	akrobatický	k2eAgFnSc1d1	akrobatická
formace	formace	k1gFnSc1	formace
letectva	letectvo	k1gNnSc2	letectvo
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
její	její	k3xOp3gFnPc1	její
webové	webový	k2eAgFnPc1d1	webová
stránky	stránka	k1gFnPc1	stránka
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
stále	stále	k6eAd1	stále
funkční	funkční	k2eAgNnSc1d1	funkční
</s>
</p>
<p>
<s>
OK-JET	OK-JET	k?	OK-JET
–	–	k?	–
OK-JET	OK-JET	k1gMnSc1	OK-JET
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
a	a	k8xC	a
jediný	jediný	k2eAgMnSc1d1	jediný
Albatros	albatros	k1gMnSc1	albatros
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
provozovaný	provozovaný	k2eAgMnSc1d1	provozovaný
v	v	k7c6	v
ČR	ČR	kA	ČR
soukromým	soukromý	k2eAgFnPc3d1	soukromá
provozovatelem	provozovatel	k1gMnSc7	provozovatel
</s>
</p>
<p>
<s>
Aero	aero	k1gNnSc1	aero
dokončilo	dokončit	k5eAaPmAgNnS	dokončit
nový	nový	k2eAgInSc4d1	nový
typ	typ	k1gInSc4	typ
legendárního	legendární	k2eAgInSc2d1	legendární
letounu	letoun	k1gInSc2	letoun
Albatros	albatros	k1gMnSc1	albatros
</s>
</p>
<p>
<s>
Aero	aero	k1gNnSc1	aero
Vodochody	Vodochod	k1gInPc7	Vodochod
hlásí	hlásit	k5eAaImIp3nS	hlásit
pokrok	pokrok	k1gInSc4	pokrok
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
<g/>
,	,	kIx,	,
letoun	letoun	k1gInSc1	letoun
L-39CW	L-39CW	k1gMnPc2	L-39CW
získal	získat	k5eAaPmAgInS	získat
typový	typový	k2eAgInSc1d1	typový
certifikát	certifikát	k1gInSc1	certifikát
</s>
</p>
