<s>
Bohemistika	bohemistika	k1gFnSc1	bohemistika
je	být	k5eAaImIp3nS	být
vědní	vědní	k2eAgInSc4d1	vědní
obor	obor	k1gInSc4	obor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
českým	český	k2eAgInSc7d1	český
jazykem	jazyk	k1gInSc7	jazyk
a	a	k8xC	a
literaturou	literatura	k1gFnSc7	literatura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
méně	málo	k6eAd2	málo
častém	častý	k2eAgNnSc6d1	časté
pojetí	pojetí	k1gNnSc6	pojetí
výraz	výraz	k1gInSc1	výraz
označuje	označovat	k5eAaImIp3nS	označovat
obor	obor	k1gInSc4	obor
zabývající	zabývající	k2eAgInSc4d1	zabývající
se	se	k3xPyFc4	se
českým	český	k2eAgInSc7d1	český
národem	národ	k1gInSc7	národ
obecně	obecně	k6eAd1	obecně
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
včetně	včetně	k7c2	včetně
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
historie	historie	k1gFnSc2	historie
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obor	obor	k1gInSc1	obor
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
již	již	k6eAd1	již
ve	v	k7c6	v
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
objevily	objevit	k5eAaPmAgFnP	objevit
odborné	odborný	k2eAgFnPc1d1	odborná
práce	práce	k1gFnPc1	práce
zabývající	zabývající	k2eAgFnPc1d1	zabývající
se	se	k3xPyFc4	se
českým	český	k2eAgInSc7d1	český
jazykem	jazyk	k1gInSc7	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Klaret	klaret	k1gInSc1	klaret
Český	český	k2eAgMnSc1d1	český
učenec	učenec	k1gMnSc1	učenec
doby	doba	k1gFnSc2	doba
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Bartoloměj	Bartoloměj	k1gMnSc1	Bartoloměj
z	z	k7c2	z
Chlumce	Chlumec	k1gInSc2	Chlumec
<g/>
,	,	kIx,	,
zvaný	zvaný	k2eAgInSc1d1	zvaný
Klaret	klaret	k1gInSc1	klaret
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1320	[number]	k4	1320
–	–	k?	–
asi	asi	k9	asi
1379	[number]	k4	1379
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
jako	jako	k9	jako
první	první	k4xOgFnSc7	první
pokusil	pokusit	k5eAaPmAgMnS	pokusit
o	o	k7c4	o
vytvoření	vytvoření	k1gNnSc4	vytvoření
české	český	k2eAgFnSc2d1	Česká
vědecké	vědecký	k2eAgFnSc2d1	vědecká
terminologie	terminologie	k1gFnSc2	terminologie
<g/>
.	.	kIx.	.
</s>
<s>
Učinil	učinit	k5eAaImAgInS	učinit
tak	tak	k9	tak
zvláště	zvláště	k6eAd1	zvláště
ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
třech	tři	k4xCgInPc6	tři
velkých	velký	k2eAgInPc6d1	velký
veršovaných	veršovaný	k2eAgInPc6d1	veršovaný
slovnících	slovník	k1gInPc6	slovník
–	–	k?	–
Glosář	glosář	k1gInSc1	glosář
<g/>
,	,	kIx,	,
Bohemář	bohemář	k1gInSc1	bohemář
a	a	k8xC	a
Vokabulář	vokabulář	k1gInSc1	vokabulář
gramatický	gramatický	k2eAgInSc1d1	gramatický
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnSc1d1	další
<g/>
,	,	kIx,	,
menší	malý	k2eAgInSc1d2	menší
slovník	slovník	k1gInSc1	slovník
dostal	dostat	k5eAaPmAgInS	dostat
název	název	k1gInSc1	název
Exemplarius	Exemplarius	k1gInSc1	Exemplarius
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
novotvary	novotvar	k1gInPc1	novotvar
zavedené	zavedený	k2eAgInPc1d1	zavedený
Klaretem	klaret	k1gInSc7	klaret
existují	existovat	k5eAaImIp3nP	existovat
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
dodnes	dodnes	k6eAd1	dodnes
(	(	kIx(	(
<g/>
např.	např.	kA	např.
jepice	jepice	k1gFnSc1	jepice
<g/>
,	,	kIx,	,
háv	háv	k1gInSc1	háv
<g/>
,	,	kIx,	,
zlatohlav	zlatohlav	k1gInSc1	zlatohlav
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
O	o	k7c4	o
český	český	k2eAgInSc4d1	český
jazyk	jazyk	k1gInSc4	jazyk
se	se	k3xPyFc4	se
zajímal	zajímat	k5eAaImAgMnS	zajímat
také	také	k9	také
Jan	Jan	k1gMnSc1	Jan
Hus	Hus	k1gMnSc1	Hus
(	(	kIx(	(
<g/>
asi	asi	k9	asi
1370	[number]	k4	1370
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1415	[number]	k4	1415
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Husovo	Husův	k2eAgNnSc4d1	Husovo
autorství	autorství	k1gNnSc4	autorství
latinsky	latinsky	k6eAd1	latinsky
psaného	psaný	k2eAgNnSc2d1	psané
díla	dílo	k1gNnSc2	dílo
Orthographia	Orthographius	k1gMnSc2	Orthographius
Bohemica	Bohemicus	k1gMnSc2	Bohemicus
(	(	kIx(	(
<g/>
O	o	k7c6	o
pravopise	pravopis	k1gInSc6	pravopis
českém	český	k2eAgInSc6d1	český
<g/>
)	)	kIx)	)
nelze	lze	k6eNd1	lze
jednoznačně	jednoznačně	k6eAd1	jednoznačně
prokázat	prokázat	k5eAaPmF	prokázat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bývá	bývat	k5eAaImIp3nS	bývat
mu	on	k3xPp3gMnSc3	on
obvykle	obvykle	k6eAd1	obvykle
přisuzováno	přisuzovat	k5eAaImNgNnS	přisuzovat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
reformy	reforma	k1gFnSc2	reforma
navržené	navržený	k2eAgFnSc2d1	navržená
v	v	k7c6	v
uvedeném	uvedený	k2eAgInSc6d1	uvedený
spise	spis	k1gInSc6	spis
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
dosavadní	dosavadní	k2eAgInSc1d1	dosavadní
český	český	k2eAgInSc4d1	český
spřežkový	spřežkový	k2eAgInSc4d1	spřežkový
pravopis	pravopis	k1gInSc4	pravopis
nahradit	nahradit	k5eAaPmF	nahradit
pravopisem	pravopis	k1gInSc7	pravopis
diakritickým	diakritický	k2eAgInSc7d1	diakritický
<g/>
.	.	kIx.	.
</s>
<s>
Čeští	český	k2eAgMnPc1d1	český
humanisté	humanista	k1gMnPc1	humanista
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Viktorin	Viktorin	k1gInSc4	Viktorin
Kornel	Kornel	k1gMnSc1	Kornel
ze	z	k7c2	z
Všehrd	Všehrd	k1gMnSc1	Všehrd
<g/>
)	)	kIx)	)
hájili	hájit	k5eAaImAgMnP	hájit
češtinu	čeština	k1gFnSc4	čeština
proti	proti	k7c3	proti
výtkám	výtka	k1gFnPc3	výtka
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
latinou	latina	k1gFnSc7	latina
nedokonalá	dokonalý	k2eNgFnSc1d1	nedokonalá
<g/>
,	,	kIx,	,
a	a	k8xC	a
usilovali	usilovat	k5eAaImAgMnP	usilovat
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
čeština	čeština	k1gFnSc1	čeština
po	po	k7c6	po
stránce	stránka	k1gFnSc6	stránka
lexikální	lexikální	k2eAgFnSc6d1	lexikální
i	i	k8xC	i
stylistické	stylistický	k2eAgFnSc6d1	stylistická
vyrovnala	vyrovnat	k5eAaBmAgFnS	vyrovnat
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgMnPc1	tři
utrakvističtí	utrakvistický	k2eAgMnPc1d1	utrakvistický
kněží	kněz	k1gMnPc1	kněz
<g/>
,	,	kIx,	,
Beneš	Beneš	k1gMnSc1	Beneš
Optát	Optát	k1gMnSc1	Optát
z	z	k7c2	z
Telče	Telč	k1gFnSc2	Telč
<g/>
,	,	kIx,	,
Petr	Petr	k1gMnSc1	Petr
Gzel	Gzel	k1gMnSc1	Gzel
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
a	a	k8xC	a
Václav	Václav	k1gMnSc1	Václav
Philomates	Philomates	k1gMnSc1	Philomates
<g/>
,	,	kIx,	,
v	v	k7c6	v
první	první	k4xOgFnSc6	první
třetině	třetina	k1gFnSc6	třetina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
sestavili	sestavit	k5eAaPmAgMnP	sestavit
první	první	k4xOgFnSc4	první
mluvnici	mluvnice	k1gFnSc4	mluvnice
českého	český	k2eAgInSc2d1	český
jazyka	jazyk	k1gInSc2	jazyk
–	–	k?	–
Grammatiku	Grammatika	k1gFnSc4	Grammatika
českou	český	k2eAgFnSc4d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Ač	ač	k8xS	ač
nebyla	být	k5eNaImAgFnS	být
úplná	úplný	k2eAgFnSc1d1	úplná
<g/>
,	,	kIx,	,
položila	položit	k5eAaPmAgFnS	položit
základy	základ	k1gInPc4	základ
kodifikace	kodifikace	k1gFnSc2	kodifikace
humanistické	humanistický	k2eAgFnSc2d1	humanistická
češtiny	čeština	k1gFnSc2	čeština
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
pak	pak	k6eAd1	pak
v	v	k7c6	v
hláskosloví	hláskosloví	k1gNnSc6	hláskosloví
a	a	k8xC	a
tvarosloví	tvarosloví	k1gNnSc6	tvarosloví
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Blahoslav	blahoslavit	k5eAaImRp2nS	blahoslavit
Velkým	velký	k2eAgMnSc7d1	velký
znalcem	znalec	k1gMnSc7	znalec
českého	český	k2eAgInSc2d1	český
jazyka	jazyk	k1gInSc2	jazyk
byl	být	k5eAaImAgMnS	být
biskup	biskup	k1gMnSc1	biskup
jednoty	jednota	k1gFnSc2	jednota
bratrské	bratrský	k2eAgFnSc2d1	bratrská
Jan	Jan	k1gMnSc1	Jan
Blahoslav	blahoslavit	k5eAaImRp2nS	blahoslavit
(	(	kIx(	(
<g/>
1523	[number]	k4	1523
<g/>
–	–	k?	–
<g/>
1571	[number]	k4	1571
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
latiny	latina	k1gFnSc2	latina
a	a	k8xC	a
řečtiny	řečtina	k1gFnSc2	řečtina
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
přeložil	přeložit	k5eAaPmAgInS	přeložit
Nový	nový	k2eAgInSc1d1	nový
zákon	zákon	k1gInSc1	zákon
(	(	kIx(	(
<g/>
překlad	překlad	k1gInSc1	překlad
vydán	vydán	k2eAgInSc1d1	vydán
1564	[number]	k4	1564
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
dal	dát	k5eAaPmAgMnS	dát
tak	tak	k9	tak
podnět	podnět	k1gInSc4	podnět
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
Bible	bible	k1gFnSc2	bible
kralické	kralický	k2eAgFnSc2d1	Kralická
(	(	kIx(	(
<g/>
1576	[number]	k4	1576
<g/>
–	–	k?	–
<g/>
1594	[number]	k4	1594
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Blahoslavův	Blahoslavův	k2eAgInSc4d1	Blahoslavův
překlad	překlad	k1gInSc4	překlad
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
vzorem	vzor	k1gInSc7	vzor
dokonalého	dokonalý	k2eAgInSc2d1	dokonalý
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgInS	věnovat
Gramatice	gramatika	k1gFnSc3	gramatika
české	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
dokončil	dokončit	k5eAaPmAgInS	dokončit
ji	on	k3xPp3gFnSc4	on
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
komentovaným	komentovaný	k2eAgNnSc7d1	komentované
vydáním	vydání	k1gNnSc7	vydání
starší	starý	k2eAgFnSc2d2	starší
gramatiky	gramatika	k1gFnSc2	gramatika
češtiny	čeština	k1gFnSc2	čeština
od	od	k7c2	od
Beneše	Beneš	k1gMnSc2	Beneš
Optáta	Optát	k1gMnSc2	Optát
a	a	k8xC	a
Václava	Václav	k1gMnSc2	Václav
Philomatesa	Philomatesa	k1gFnSc1	Philomatesa
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
Blahoslavem	Blahoslav	k1gMnSc7	Blahoslav
doplněna	doplnit	k5eAaPmNgFnS	doplnit
o	o	k7c4	o
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
oddíl	oddíl	k1gInSc4	oddíl
o	o	k7c6	o
stylistické	stylistický	k2eAgFnSc6d1	stylistická
kultuře	kultura	k1gFnSc6	kultura
a	a	k8xC	a
normě	norma	k1gFnSc6	norma
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
doplněna	doplnit	k5eAaPmNgFnS	doplnit
sbírkou	sbírka	k1gFnSc7	sbírka
českých	český	k2eAgFnPc2d1	Česká
přísloví	přísloví	k1gNnPc2	přísloví
<g/>
.	.	kIx.	.
</s>
<s>
Matouš	Matouš	k1gMnSc1	Matouš
BenešovskýMatouš	BenešovskýMatouš	k1gInSc4	BenešovskýMatouš
Benešovský	benešovský	k2eAgInSc4d1	benešovský
zvaný	zvaný	k2eAgInSc4d1	zvaný
Philonomus	Philonomus	k1gInSc4	Philonomus
<g/>
,	,	kIx,	,
o	o	k7c6	o
jehož	jehož	k3xOyRp3gInSc6	jehož
životě	život	k1gInSc6	život
nemáme	mít	k5eNaImIp1nP	mít
příliš	příliš	k6eAd1	příliš
spolehlivých	spolehlivý	k2eAgInPc2d1	spolehlivý
dokladů	doklad	k1gInPc2	doklad
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
původcem	původce	k1gMnSc7	původce
dvou	dva	k4xCgNnPc2	dva
jazykovědných	jazykovědný	k2eAgNnPc2d1	jazykovědné
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Grammatica	Grammatica	k1gFnSc1	Grammatica
Bohemica	Bohemic	k2eAgFnSc1d1	Bohemica
vyšla	vyjít	k5eAaPmAgFnS	vyjít
roku	rok	k1gInSc2	rok
1577	[number]	k4	1577
a	a	k8xC	a
Knížka	knížka	k1gFnSc1	knížka
slov	slovo	k1gNnPc2	slovo
českých	český	k2eAgFnPc2d1	Česká
vyložených	vyložený	k2eAgFnPc2d1	vyložená
roku	rok	k1gInSc2	rok
1587	[number]	k4	1587
<g/>
.	.	kIx.	.
</s>
<s>
Benešovský	benešovský	k2eAgMnSc1d1	benešovský
se	se	k3xPyFc4	se
pokládá	pokládat	k5eAaImIp3nS	pokládat
za	za	k7c2	za
prvního	první	k4xOgMnSc2	první
autora	autor	k1gMnSc2	autor
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
popsal	popsat	k5eAaPmAgMnS	popsat
sedmipádovou	sedmipádový	k2eAgFnSc4d1	sedmipádový
strukturu	struktura	k1gFnSc4	struktura
češtiny	čeština	k1gFnSc2	čeština
a	a	k8xC	a
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
blízko	blízko	k7c2	blízko
objevení	objevení	k1gNnSc2	objevení
slovesného	slovesný	k2eAgInSc2d1	slovesný
vidu	vid	k1gInSc2	vid
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc7	jeho
specifikem	specifikon	k1gNnSc7	specifikon
je	být	k5eAaImIp3nS	být
práce	práce	k1gFnSc2	práce
s	s	k7c7	s
obecně	obecně	k6eAd1	obecně
slovanským	slovanský	k2eAgInSc7d1	slovanský
materiálem	materiál	k1gInSc7	materiál
<g/>
,	,	kIx,	,
především	především	k9	především
maloruským	maloruský	k2eAgInSc7d1	maloruský
(	(	kIx(	(
<g/>
ukrajinským	ukrajinský	k2eAgMnPc3d1	ukrajinský
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
pak	pak	k6eAd1	pak
jihoslovanským	jihoslovanský	k2eAgMnPc3d1	jihoslovanský
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
novodobé	novodobý	k2eAgFnSc6d1	novodobá
edici	edice	k1gFnSc6	edice
(	(	kIx(	(
<g/>
O.	O.	kA	O.
Koupil	koupit	k5eAaPmAgInS	koupit
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zván	zvát	k5eAaImNgMnS	zvát
protoslavistou	protoslavista	k1gMnSc7	protoslavista
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
humanistické	humanistický	k2eAgFnSc2d1	humanistická
tradice	tradice	k1gFnSc2	tradice
podává	podávat	k5eAaImIp3nS	podávat
také	také	k9	také
chválu	chvála	k1gFnSc4	chvála
jazyka	jazyk	k1gInSc2	jazyk
českého	český	k2eAgInSc2d1	český
a	a	k8xC	a
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
čistotu	čistota	k1gFnSc4	čistota
češtiny	čeština	k1gFnSc2	čeština
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
krajního	krajní	k2eAgInSc2d1	krajní
purizmu	purizmus	k1gInSc2	purizmus
<g/>
.	.	kIx.	.
</s>
<s>
Zcela	zcela	k6eAd1	zcela
jedinečné	jedinečný	k2eAgNnSc4d1	jedinečné
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
Knížce	knížka	k1gFnSc3	knížka
žalmy	žalm	k1gInPc4	žalm
v	v	k7c6	v
církevní	církevní	k2eAgFnSc6d1	církevní
slovanštině	slovanština	k1gFnSc6	slovanština
<g/>
,	,	kIx,	,
zapsané	zapsaný	k2eAgNnSc1d1	zapsané
českým	český	k2eAgInSc7d1	český
pravopisem	pravopis	k1gInSc7	pravopis
<g/>
.	.	kIx.	.
</s>
<s>
Daniel	Daniel	k1gMnSc1	Daniel
Adam	Adam	k1gMnSc1	Adam
z	z	k7c2	z
Veleslavína	Veleslavín	k1gInSc2	Veleslavín
Lexikograf	lexikograf	k1gMnSc1	lexikograf
Daniel	Daniel	k1gMnSc1	Daniel
Adam	Adam	k1gMnSc1	Adam
z	z	k7c2	z
Veleslavína	Veleslavín	k1gInSc2	Veleslavín
(	(	kIx(	(
<g/>
1546	[number]	k4	1546
<g/>
–	–	k?	–
<g/>
1599	[number]	k4	1599
<g/>
)	)	kIx)	)
sestavil	sestavit	k5eAaPmAgInS	sestavit
mj.	mj.	kA	mj.
čtyřjazyčný	čtyřjazyčný	k2eAgInSc4d1	čtyřjazyčný
slovník	slovník	k1gInSc4	slovník
Silva	Silva	k1gFnSc1	Silva
quadrilinguis	quadrilinguis	k1gFnSc1	quadrilinguis
(	(	kIx(	(
<g/>
Bohatství	bohatství	k1gNnSc1	bohatství
čtyř	čtyři	k4xCgMnPc2	čtyři
jazyků	jazyk	k1gMnPc2	jazyk
<g/>
,	,	kIx,	,
1598	[number]	k4	1598
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
vedle	vedle	k7c2	vedle
sebe	se	k3xPyFc2	se
kladl	klást	k5eAaImAgMnS	klást
dva	dva	k4xCgMnPc4	dva
jazyky	jazyk	k1gMnPc4	jazyk
mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
(	(	kIx(	(
<g/>
latinu	latina	k1gFnSc4	latina
a	a	k8xC	a
řečtinu	řečtina	k1gFnSc4	řečtina
<g/>
)	)	kIx)	)
a	a	k8xC	a
dva	dva	k4xCgMnPc4	dva
jazyky	jazyk	k1gMnPc4	jazyk
živé	živý	k2eAgNnSc1d1	živé
(	(	kIx(	(
<g/>
češtinu	čeština	k1gFnSc4	čeština
a	a	k8xC	a
němčinu	němčina	k1gFnSc4	němčina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Daniela	Daniel	k1gMnSc2	Daniel
Adama	Adam	k1gMnSc2	Adam
z	z	k7c2	z
Veleslavína	Veleslavín	k1gInSc2	Veleslavín
byla	být	k5eAaImAgFnS	být
celá	celý	k2eAgFnSc1d1	celá
jazyková	jazykový	k2eAgFnSc1d1	jazyková
(	(	kIx(	(
<g/>
a	a	k8xC	a
obecně	obecně	k6eAd1	obecně
kulturní	kulturní	k2eAgFnSc1d1	kulturní
<g/>
)	)	kIx)	)
epocha	epocha	k1gFnSc1	epocha
v	v	k7c6	v
českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
nazvána	nazvat	k5eAaPmNgFnS	nazvat
dobou	doba	k1gFnSc7	doba
veleslavínskou	veleslavínský	k2eAgFnSc7d1	Veleslavínská
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
na	na	k7c4	na
češtinu	čeština	k1gFnSc4	čeština
tohoto	tento	k3xDgNnSc2	tento
vrcholného	vrcholný	k2eAgNnSc2d1	vrcholné
období	období	k1gNnSc2	období
navázal	navázat	k5eAaPmAgInS	navázat
počátkem	počátkem	k7c2	počátkem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Josef	Josef	k1gMnSc1	Josef
Dobrovský	Dobrovský	k1gMnSc1	Dobrovský
při	při	k7c6	při
snaze	snaha	k1gFnSc6	snaha
o	o	k7c6	o
vytvoření	vytvoření	k1gNnSc6	vytvoření
moderní	moderní	k2eAgFnSc2d1	moderní
spisovné	spisovný	k2eAgFnSc2d1	spisovná
češtiny	čeština	k1gFnSc2	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Slováci	Slovák	k1gMnPc1	Slovák
Na	na	k7c6	na
rozvoji	rozvoj	k1gInSc6	rozvoj
bohemistiky	bohemistika	k1gFnSc2	bohemistika
se	se	k3xPyFc4	se
v	v	k7c6	v
době	doba	k1gFnSc6	doba
humanismu	humanismus	k1gInSc2	humanismus
podíleli	podílet	k5eAaImAgMnP	podílet
také	také	k9	také
Slováci	Slovák	k1gMnPc1	Slovák
působící	působící	k2eAgMnPc1d1	působící
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
byl	být	k5eAaImAgMnS	být
Vavřinec	Vavřinec	k1gMnSc1	Vavřinec
Benedikt	Benedikt	k1gMnSc1	Benedikt
z	z	k7c2	z
Nudožer	Nudožer	k1gMnSc1	Nudožer
(	(	kIx(	(
<g/>
1555	[number]	k4	1555
<g/>
–	–	k?	–
<g/>
1615	[number]	k4	1615
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
působil	působit	k5eAaImAgMnS	působit
na	na	k7c6	na
pražské	pražský	k2eAgFnSc6d1	Pražská
univerzitě	univerzita	k1gFnSc6	univerzita
a	a	k8xC	a
latinsky	latinsky	k6eAd1	latinsky
sepsal	sepsat	k5eAaPmAgMnS	sepsat
mluvnici	mluvnice	k1gFnSc4	mluvnice
češtiny	čeština	k1gFnSc2	čeština
známou	známý	k2eAgFnSc4d1	známá
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Grammaticæ	Grammaticæ	k1gFnSc1	Grammaticæ
bohemicæ	bohemicæ	k?	bohemicæ
libri	libri	k6eAd1	libri
duo	duo	k1gNnSc1	duo
(	(	kIx(	(
<g/>
Dvě	dva	k4xCgFnPc1	dva
knihy	kniha	k1gFnPc1	kniha
české	český	k2eAgFnSc2d1	Česká
mluvnice	mluvnice	k1gFnSc2	mluvnice
<g/>
,	,	kIx,	,
1603	[number]	k4	1603
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
výkladů	výklad	k1gInPc2	výklad
o	o	k7c6	o
mluvnici	mluvnice	k1gFnSc6	mluvnice
kniha	kniha	k1gFnSc1	kniha
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
řadu	řada	k1gFnSc4	řada
poznámek	poznámka	k1gFnPc2	poznámka
o	o	k7c6	o
nářečích	nářečí	k1gNnPc6	nářečí
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
kvalitní	kvalitní	k2eAgFnSc7d1	kvalitní
gramatikou	gramatika	k1gFnSc7	gramatika
byla	být	k5eAaImAgFnS	být
také	také	k9	také
Grammatica	Grammatic	k2eAgFnSc1d1	Grammatica
Bohemico-Slavica	Bohemico-Slavica	k1gFnSc1	Bohemico-Slavica
Pavla	Pavel	k1gMnSc2	Pavel
Doležala	Doležal	k1gMnSc2	Doležal
(	(	kIx(	(
<g/>
1746	[number]	k4	1746
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Doba	doba	k1gFnSc1	doba
slovesného	slovesný	k2eAgNnSc2d1	slovesné
baroka	baroko	k1gNnSc2	baroko
<g/>
,	,	kIx,	,
za	za	k7c4	za
niž	jenž	k3xRgFnSc4	jenž
se	se	k3xPyFc4	se
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
považuje	považovat	k5eAaImIp3nS	považovat
přibližně	přibližně	k6eAd1	přibližně
rozpětí	rozpětí	k1gNnSc4	rozpětí
let	léto	k1gNnPc2	léto
1620	[number]	k4	1620
<g/>
–	–	k?	–
<g/>
1770	[number]	k4	1770
<g/>
,	,	kIx,	,
s	s	k7c7	s
sebou	se	k3xPyFc7	se
přinesla	přinést	k5eAaPmAgFnS	přinést
zvýšený	zvýšený	k2eAgInSc4d1	zvýšený
teoretický	teoretický	k2eAgInSc4d1	teoretický
zájem	zájem	k1gInSc4	zájem
o	o	k7c4	o
český	český	k2eAgInSc4d1	český
jazyk	jazyk	k1gInSc4	jazyk
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
několika	několik	k4yIc2	několik
gramatik	gramatika	k1gFnPc2	gramatika
<g/>
,	,	kIx,	,
některých	některý	k3yIgInPc2	některý
mnohokrát	mnohokrát	k6eAd1	mnohokrát
vydaných	vydaný	k2eAgInPc2d1	vydaný
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
horečnatá	horečnatý	k2eAgFnSc1d1	horečnatá
jazykovědná	jazykovědný	k2eAgFnSc1d1	jazykovědná
aktivita	aktivita	k1gFnSc1	aktivita
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
spojuje	spojovat	k5eAaImIp3nS	spojovat
s	s	k7c7	s
komplikovaným	komplikovaný	k2eAgNnSc7d1	komplikované
postavením	postavení	k1gNnSc7	postavení
češtiny	čeština	k1gFnSc2	čeština
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
postupně	postupně	k6eAd1	postupně
vytlačována	vytlačovat	k5eAaImNgFnS	vytlačovat
ze	z	k7c2	z
sféry	sféra	k1gFnSc2	sféra
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
správy	správa	k1gFnSc2	správa
a	a	k8xC	a
světské	světský	k2eAgFnSc2d1	světská
elitní	elitní	k2eAgFnSc2d1	elitní
kultury	kultura	k1gFnSc2	kultura
a	a	k8xC	a
musela	muset	k5eAaImAgFnS	muset
odolávat	odolávat	k5eAaImF	odolávat
zvýšenému	zvýšený	k2eAgInSc3d1	zvýšený
tlaku	tlak	k1gInSc3	tlak
němčiny	němčina	k1gFnSc2	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Představy	představa	k1gFnPc1	představa
o	o	k7c6	o
jejím	její	k3xOp3gInSc6	její
úpadku	úpadek	k1gInSc6	úpadek
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
obrozeneckým	obrozenecký	k2eAgInSc7d1	obrozenecký
mýtem	mýtus	k1gInSc7	mýtus
<g/>
,	,	kIx,	,
na	na	k7c4	na
nějž	jenž	k3xRgMnSc4	jenž
navázala	navázat	k5eAaPmAgFnS	navázat
i	i	k8xC	i
pozitivistická	pozitivistický	k2eAgFnSc1d1	pozitivistická
bohemistika	bohemistika	k1gFnSc1	bohemistika
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
kvality	kvalita	k1gFnPc4	kvalita
pobělohorské	pobělohorský	k2eAgFnSc2d1	pobělohorská
literatury	literatura	k1gFnSc2	literatura
široce	široko	k6eAd1	široko
uznávány	uznáván	k2eAgInPc1d1	uznáván
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
důležitá	důležitý	k2eAgFnSc1d1	důležitá
role	role	k1gFnSc1	role
jezuitských	jezuitský	k2eAgMnPc2d1	jezuitský
autorů	autor	k1gMnPc2	autor
při	při	k7c6	při
péči	péče	k1gFnSc6	péče
o	o	k7c4	o
český	český	k2eAgInSc4d1	český
jazyk	jazyk	k1gInSc4	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
první	první	k4xOgNnPc4	první
barokní	barokní	k2eAgNnPc4d1	barokní
díla	dílo	k1gNnPc4	dílo
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
na	na	k7c4	na
jazyk	jazyk	k1gInSc4	jazyk
patří	patřit	k5eAaImIp3nS	patřit
Grammatica	Grammatic	k2eAgFnSc1d1	Grammatica
Boemica	Boemica	k1gFnSc1	Boemica
Jana	Jan	k1gMnSc2	Jan
Drachovského	Drachovský	k2eAgInSc2d1	Drachovský
z	z	k7c2	z
r.	r.	kA	r.
1660	[number]	k4	1660
a	a	k8xC	a
Brus	brus	k1gInSc1	brus
jazyka	jazyk	k1gInSc2	jazyk
českého	český	k2eAgInSc2d1	český
z	z	k7c2	z
r.	r.	kA	r.
1667	[number]	k4	1667
od	od	k7c2	od
Jiřího	Jiří	k1gMnSc2	Jiří
Konstance	Konstanc	k1gFnSc2	Konstanc
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
však	však	k9	však
vydal	vydat	k5eAaPmAgInS	vydat
až	až	k9	až
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
M.	M.	kA	M.
V.	V.	kA	V.
Šteyer	Šteyer	k1gMnSc1	Šteyer
<g/>
.	.	kIx.	.
</s>
<s>
Prakticky	prakticky	k6eAd1	prakticky
zaměřený	zaměřený	k2eAgInSc1d1	zaměřený
byl	být	k5eAaImAgInS	být
Šteyerův	Šteyerův	k2eAgInSc1d1	Šteyerův
tzv.	tzv.	kA	tzv.
Žáček	Žáček	k1gMnSc1	Žáček
<g/>
,	,	kIx,	,
plným	plný	k2eAgInSc7d1	plný
názvem	název	k1gInSc7	název
Výborně	výborně	k6eAd1	výborně
dobrý	dobrý	k2eAgInSc4d1	dobrý
způsob	způsob	k1gInSc4	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
má	mít	k5eAaImIp3nS	mít
dobře	dobře	k6eAd1	dobře
po	po	k7c6	po
česku	česk	k1gInSc6	česk
psáti	psát	k5eAaImF	psát
<g/>
,	,	kIx,	,
neb	neb	k8xC	neb
tisknouti	tisknout	k5eAaImF	tisknout
(	(	kIx(	(
<g/>
1668	[number]	k4	1668
<g/>
,	,	kIx,	,
1730	[number]	k4	1730
<g/>
,	,	kIx,	,
1781	[number]	k4	1781
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nic	nic	k3yNnSc1	nic
bližšího	blízký	k2eAgNnSc2d2	bližší
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
o	o	k7c6	o
mluvnici	mluvnice	k1gFnSc6	mluvnice
Kotelově	Kotelův	k2eAgFnSc6d1	Kotelův
kolem	kolem	k7c2	kolem
r.	r.	kA	r.
1691	[number]	k4	1691
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
původní	původní	k2eAgFnSc7d1	původní
mluvnickou	mluvnický	k2eAgFnSc7d1	mluvnická
prací	práce	k1gFnSc7	práce
je	být	k5eAaImIp3nS	být
však	však	k9	však
latinská	latinský	k2eAgFnSc1d1	Latinská
Čechořečnost	Čechořečnost	k1gFnSc1	Čechořečnost
<g/>
,	,	kIx,	,
seu	seu	k?	seu
Grammatica	Grammaticus	k1gMnSc2	Grammaticus
linguae	lingua	k1gMnSc2	lingua
Bohemicae	Bohemica	k1gMnSc2	Bohemica
Václava	Václav	k1gMnSc2	Václav
Jana	Jan	k1gMnSc2	Jan
Rosy	Rosa	k1gMnSc2	Rosa
z	z	k7c2	z
r.	r.	kA	r.
1672	[number]	k4	1672
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
také	také	k9	také
předmluvu	předmluva	k1gFnSc4	předmluva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
obranou	obrana	k1gFnSc7	obrana
češtiny	čeština	k1gFnSc2	čeština
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
nepřerušeném	přerušený	k2eNgInSc6d1	nepřerušený
zájmu	zájem	k1gInSc6	zájem
o	o	k7c4	o
jazyk	jazyk	k1gInSc4	jazyk
svědčí	svědčit	k5eAaImIp3nP	svědčit
také	také	k9	také
opakovaná	opakovaný	k2eAgNnPc1d1	opakované
vydání	vydání	k1gNnPc1	vydání
mluvnice	mluvnice	k1gFnSc2	mluvnice
Václava	Václav	k1gMnSc2	Václav
Jandyta	Jandyt	k1gMnSc2	Jandyt
Grammatica	Grammaticus	k1gMnSc2	Grammaticus
Linguae	Lingua	k1gMnSc2	Lingua
Boemicae	Boemica	k1gMnSc2	Boemica
(	(	kIx(	(
<g/>
1704	[number]	k4	1704
<g/>
,	,	kIx,	,
1705	[number]	k4	1705
<g/>
,	,	kIx,	,
1715	[number]	k4	1715
<g/>
,	,	kIx,	,
1732	[number]	k4	1732
<g/>
,	,	kIx,	,
1739	[number]	k4	1739
<g/>
,	,	kIx,	,
1753	[number]	k4	1753
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
opatřená	opatřený	k2eAgNnPc4d1	opatřené
stále	stále	k6eAd1	stále
přetiskovanou	přetiskovaný	k2eAgFnSc7d1	přetiskovaná
předmluvou	předmluva	k1gFnSc7	předmluva
<g/>
,	,	kIx,	,
obsahující	obsahující	k2eAgFnSc4d1	obsahující
zanícenou	zanícený	k2eAgFnSc4d1	zanícená
obranu	obrana	k1gFnSc4	obrana
českého	český	k2eAgInSc2d1	český
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
a	a	k8xC	a
příležitostně	příležitostně	k6eAd1	příležitostně
doplněná	doplněný	k2eAgNnPc4d1	doplněné
puristickým	puristický	k2eAgInSc7d1	puristický
slovníčkem	slovníček	k1gInSc7	slovníček
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
z	z	k7c2	z
vydání	vydání	k1gNnSc2	vydání
najdeme	najít	k5eAaPmIp1nP	najít
také	také	k9	také
návody	návod	k1gInPc1	návod
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
psát	psát	k5eAaImF	psát
českou	český	k2eAgFnSc4d1	Česká
divadelní	divadelní	k2eAgFnSc4d1	divadelní
hru	hra	k1gFnSc4	hra
nebo	nebo	k8xC	nebo
jak	jak	k6eAd1	jak
konverzovat	konverzovat	k5eAaImF	konverzovat
ve	v	k7c6	v
vyšší	vysoký	k2eAgFnSc6d2	vyšší
společnosti	společnost	k1gFnSc6	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Role	role	k1gFnSc1	role
těchto	tento	k3xDgInPc2	tento
návodů	návod	k1gInPc2	návod
stále	stále	k6eAd1	stále
čeká	čekat	k5eAaImIp3nS	čekat
na	na	k7c4	na
podrobnější	podrobný	k2eAgInSc4d2	podrobnější
výzkum	výzkum	k1gInSc4	výzkum
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
ukáže	ukázat	k5eAaPmIp3nS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
čeština	čeština	k1gFnSc1	čeština
nebyla	být	k5eNaImAgFnS	být
vytlačena	vytlačit	k5eAaPmNgFnS	vytlačit
z	z	k7c2	z
elitní	elitní	k2eAgFnSc2d1	elitní
světské	světský	k2eAgFnSc2d1	světská
kultury	kultura	k1gFnSc2	kultura
tak	tak	k6eAd1	tak
dokonale	dokonale	k6eAd1	dokonale
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
se	se	k3xPyFc4	se
tvrdívá	tvrdívat	k5eAaImIp3nS	tvrdívat
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Dobrovský	Dobrovský	k1gMnSc1	Dobrovský
Za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
novodobé	novodobý	k2eAgFnSc2d1	novodobá
vědecké	vědecký	k2eAgFnSc2d1	vědecká
bohemistiky	bohemistika	k1gFnSc2	bohemistika
(	(	kIx(	(
<g/>
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
slavistiky	slavistika	k1gFnSc2	slavistika
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
Josef	Josef	k1gMnSc1	Josef
Dobrovský	Dobrovský	k1gMnSc1	Dobrovský
(	(	kIx(	(
<g/>
1753	[number]	k4	1753
<g/>
–	–	k?	–
<g/>
1829	[number]	k4	1829
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vůdčí	vůdčí	k2eAgFnSc1d1	vůdčí
postava	postava	k1gFnSc1	postava
první	první	k4xOgFnSc2	první
generace	generace	k1gFnSc2	generace
obrozenců	obrozenec	k1gMnPc2	obrozenec
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
před	před	k7c7	před
touto	tento	k3xDgFnSc7	tento
generací	generace	k1gFnSc7	generace
stály	stát	k5eAaImAgInP	stát
obtížné	obtížný	k2eAgInPc1d1	obtížný
úkoly	úkol	k1gInPc1	úkol
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
kromě	kromě	k7c2	kromě
jiného	jiné	k1gNnSc2	jiné
<g/>
:	:	kIx,	:
učinit	učinit	k5eAaImF	učinit
český	český	k2eAgInSc4d1	český
jazyk	jazyk	k1gInSc4	jazyk
předmětem	předmět	k1gInSc7	předmět
teoretického	teoretický	k2eAgNnSc2d1	teoretické
studia	studio	k1gNnSc2	studio
<g/>
,	,	kIx,	,
popsat	popsat	k5eAaPmF	popsat
jeho	jeho	k3xOp3gFnPc4	jeho
dějiny	dějiny	k1gFnPc4	dějiny
<g/>
,	,	kIx,	,
vývoj	vývoj	k1gInSc4	vývoj
i	i	k8xC	i
současný	současný	k2eAgInSc4d1	současný
stav	stav	k1gInSc4	stav
a	a	k8xC	a
stavbu	stavba	k1gFnSc4	stavba
vědeckými	vědecký	k2eAgFnPc7d1	vědecká
metodami	metoda	k1gFnPc7	metoda
osvícenského	osvícenský	k2eAgInSc2d1	osvícenský
racionalismu	racionalismus	k1gInSc2	racionalismus
<g/>
;	;	kIx,	;
vytvořit	vytvořit	k5eAaPmF	vytvořit
základy	základ	k1gInPc4	základ
spisovného	spisovný	k2eAgInSc2d1	spisovný
jazyka	jazyk	k1gInSc2	jazyk
českého	český	k2eAgInSc2d1	český
stanovením	stanovení	k1gNnSc7	stanovení
nové	nový	k2eAgFnSc2d1	nová
<g/>
,	,	kIx,	,
jednotné	jednotný	k2eAgFnSc2d1	jednotná
normy	norma	k1gFnSc2	norma
a	a	k8xC	a
prosazením	prosazení	k1gNnSc7	prosazení
její	její	k3xOp3gFnSc2	její
kodifikace	kodifikace	k1gFnSc2	kodifikace
<g/>
;	;	kIx,	;
popsat	popsat	k5eAaPmF	popsat
dosavadní	dosavadní	k2eAgInSc4d1	dosavadní
vývoj	vývoj	k1gInSc4	vývoj
a	a	k8xC	a
současný	současný	k2eAgInSc4d1	současný
stav	stav	k1gInSc4	stav
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Dobrovský	Dobrovský	k1gMnSc1	Dobrovský
se	se	k3xPyFc4	se
těchto	tento	k3xDgInPc2	tento
úkolů	úkol	k1gInPc2	úkol
ujal	ujmout	k5eAaPmAgInS	ujmout
a	a	k8xC	a
už	už	k6eAd1	už
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1792	[number]	k4	1792
dokončil	dokončit	k5eAaPmAgInS	dokončit
německy	německy	k6eAd1	německy
psané	psaný	k2eAgNnSc4d1	psané
dílo	dílo	k1gNnSc4	dílo
Geschichte	Geschicht	k1gInSc5	Geschicht
der	drát	k5eAaImRp2nS	drát
böhmischen	böhmischen	k2eAgMnSc1d1	böhmischen
Sprache	Sprache	k1gFnSc7	Sprache
und	und	k?	und
Literatur	literatura	k1gFnPc2	literatura
(	(	kIx(	(
<g/>
Dějiny	dějiny	k1gFnPc4	dějiny
české	český	k2eAgFnSc2d1	Česká
řeči	řeč	k1gFnSc2	řeč
a	a	k8xC	a
literatury	literatura	k1gFnSc2	literatura
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
literárněhistorické	literárněhistorický	k2eAgFnSc6d1	literárněhistorická
studii	studie	k1gFnSc6	studie
Dobrovský	Dobrovský	k1gMnSc1	Dobrovský
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
periodizaci	periodizace	k1gFnSc4	periodizace
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
se	se	k3xPyFc4	se
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
vychází	vycházet	k5eAaImIp3nS	vycházet
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
vrcholné	vrcholný	k2eAgNnSc4d1	vrcholné
období	období	k1gNnSc4	období
českého	český	k2eAgNnSc2d1	české
písemnictví	písemnictví	k1gNnSc2	písemnictví
označil	označit	k5eAaPmAgMnS	označit
dobu	doba	k1gFnSc4	doba
veleslavínskou	veleslavínský	k2eAgFnSc4d1	Veleslavínská
<g/>
.	.	kIx.	.
</s>
<s>
Vyzdvihl	vyzdvihnout	k5eAaPmAgMnS	vyzdvihnout
však	však	k9	však
také	také	k9	také
dobu	doba	k1gFnSc4	doba
husitskou	husitský	k2eAgFnSc4d1	husitská
<g/>
.	.	kIx.	.
</s>
<s>
Dobrovského	Dobrovského	k2eAgFnPc1d1	Dobrovského
další	další	k2eAgFnPc1d1	další
práce	práce	k1gFnPc1	práce
<g/>
,	,	kIx,	,
Ausführliches	Ausführliches	k1gInSc1	Ausführliches
Lehrgebäude	Lehrgebäud	k1gInSc5	Lehrgebäud
der	drát	k5eAaImRp2nS	drát
böhmischen	böhmischen	k1gInSc4	böhmischen
Sprache	Sprach	k1gMnSc2	Sprach
(	(	kIx(	(
<g/>
Podrobná	podrobný	k2eAgFnSc1d1	podrobná
mluvnice	mluvnice	k1gFnSc1	mluvnice
českého	český	k2eAgInSc2d1	český
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
1809	[number]	k4	1809
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
označit	označit	k5eAaPmF	označit
za	za	k7c4	za
stěžejní	stěžejní	k2eAgNnSc4d1	stěžejní
dílo	dílo	k1gNnSc4	dílo
bohemistiky	bohemistika	k1gFnSc2	bohemistika
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
něhož	jenž	k3xRgInSc2	jenž
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgMnS	mít
současný	současný	k2eAgInSc4d1	současný
český	český	k2eAgInSc4d1	český
jazyk	jazyk	k1gInSc4	jazyk
výrazně	výrazně	k6eAd1	výrazně
jinou	jiný	k2eAgFnSc4d1	jiná
podobu	podoba	k1gFnSc4	podoba
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
navazoval	navazovat	k5eAaImAgMnS	navazovat
na	na	k7c4	na
humanistickou	humanistický	k2eAgFnSc4d1	humanistická
češtinu	čeština	k1gFnSc4	čeština
Bible	bible	k1gFnSc2	bible
kralické	kralický	k2eAgFnSc2d1	Kralická
a	a	k8xC	a
jeho	jeho	k3xOp3gInPc1	jeho
normativní	normativní	k2eAgInPc1d1	normativní
návrhy	návrh	k1gInPc1	návrh
byly	být	k5eAaImAgInP	být
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
brzy	brzy	k6eAd1	brzy
přijaty	přijmout	k5eAaPmNgFnP	přijmout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
O	o	k7c4	o
tuto	tento	k3xDgFnSc4	tento
kodifikační	kodifikační	k2eAgFnSc4d1	kodifikační
mluvnici	mluvnice	k1gFnSc4	mluvnice
se	se	k3xPyFc4	se
ve	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
opíral	opírat	k5eAaImAgMnS	opírat
i	i	k9	i
Jan	Jan	k1gMnSc1	Jan
Gebauer	Gebauer	k1gMnSc1	Gebauer
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Třetím	třetí	k4xOgInSc7	třetí
významným	významný	k2eAgInSc7d1	významný
počinem	počin	k1gInSc7	počin
Josefa	Josef	k1gMnSc2	Josef
Dobrovského	Dobrovský	k1gMnSc2	Dobrovský
na	na	k7c6	na
poli	pole	k1gNnSc6	pole
bohemistiky	bohemistika	k1gFnSc2	bohemistika
bylo	být	k5eAaImAgNnS	být
sestavení	sestavení	k1gNnSc1	sestavení
dvoudílného	dvoudílný	k2eAgNnSc2d1	dvoudílné
Deutsch-böhmisches	Deutschöhmisches	k1gMnSc1	Deutsch-böhmisches
Wörterbuch	Wörterbuch	k1gInSc1	Wörterbuch
(	(	kIx(	(
<g/>
Německo-český	německo-český	k2eAgInSc1d1	německo-český
slovník	slovník	k1gInSc1	slovník
<g/>
,	,	kIx,	,
1802	[number]	k4	1802
a	a	k8xC	a
1821	[number]	k4	1821
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Jungmann	Jungmann	k1gMnSc1	Jungmann
Dobrovský	Dobrovský	k1gMnSc1	Dobrovský
si	se	k3xPyFc3	se
s	s	k7c7	s
nastupující	nastupující	k2eAgFnSc7d1	nastupující
mladou	mladý	k2eAgFnSc7d1	mladá
generací	generace	k1gFnSc7	generace
obrozenců	obrozenec	k1gMnPc2	obrozenec
příliš	příliš	k6eAd1	příliš
nerozuměl	rozumět	k5eNaImAgMnS	rozumět
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
z	z	k7c2	z
jeho	on	k3xPp3gNnSc2	on
díla	dílo	k1gNnSc2	dílo
vycházel	vycházet	k5eAaImAgMnS	vycházet
další	další	k2eAgMnSc1d1	další
velikán	velikán	k1gMnSc1	velikán
bohemistiky	bohemistika	k1gFnSc2	bohemistika
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Jungmann	Jungmann	k1gMnSc1	Jungmann
(	(	kIx(	(
<g/>
1773	[number]	k4	1773
<g/>
–	–	k?	–
<g/>
1847	[number]	k4	1847
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
pětisvazkového	pětisvazkový	k2eAgInSc2d1	pětisvazkový
Slovníku	slovník	k1gInSc2	slovník
česko-německého	českoěmecký	k2eAgInSc2d1	česko-německý
(	(	kIx(	(
<g/>
vycházel	vycházet	k5eAaImAgMnS	vycházet
v	v	k7c6	v
letech	let	k1gInPc6	let
1834	[number]	k4	1834
<g/>
–	–	k?	–
<g/>
1839	[number]	k4	1839
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Shromáždil	shromáždit	k5eAaPmAgMnS	shromáždit
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
přibližně	přibližně	k6eAd1	přibližně
120	[number]	k4	120
000	[number]	k4	000
českých	český	k2eAgNnPc2d1	české
slov	slovo	k1gNnPc2	slovo
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc7	jejichž
zdrojem	zdroj	k1gInSc7	zdroj
byly	být	k5eAaImAgFnP	být
české	český	k2eAgFnPc1d1	Česká
literární	literární	k2eAgFnPc1d1	literární
památky	památka	k1gFnPc1	památka
a	a	k8xC	a
řeč	řeč	k1gFnSc1	řeč
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zahrnul	zahrnout	k5eAaPmAgInS	zahrnout
do	do	k7c2	do
něj	on	k3xPp3gInSc2	on
také	také	k9	také
výrazy	výraz	k1gInPc1	výraz
z	z	k7c2	z
dalších	další	k2eAgInPc2d1	další
slovanských	slovanský	k2eAgInPc2d1	slovanský
jazyků	jazyk	k1gInPc2	jazyk
(	(	kIx(	(
<g/>
zvl.	zvl.	kA	zvl.
z	z	k7c2	z
ruštiny	ruština	k1gFnSc2	ruština
a	a	k8xC	a
polštiny	polština	k1gFnSc2	polština
<g/>
)	)	kIx)	)
a	a	k8xC	a
vlastní	vlastní	k2eAgInPc4d1	vlastní
novotvary	novotvar	k1gInPc4	novotvar
<g/>
.	.	kIx.	.
</s>
<s>
Sestavením	sestavení	k1gNnSc7	sestavení
slovníku	slovník	k1gInSc2	slovník
chtěl	chtít	k5eAaImAgMnS	chtít
prokázat	prokázat	k5eAaPmF	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
čeština	čeština	k1gFnSc1	čeština
je	být	k5eAaImIp3nS	být
jazyk	jazyk	k1gInSc1	jazyk
stejně	stejně	k6eAd1	stejně
bohatý	bohatý	k2eAgInSc1d1	bohatý
a	a	k8xC	a
dokonalý	dokonalý	k2eAgInSc1d1	dokonalý
jako	jako	k8xC	jako
němčina	němčina	k1gFnSc1	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
českého	český	k2eAgNnSc2d1	české
odborného	odborný	k2eAgNnSc2d1	odborné
názvosloví	názvosloví	k1gNnSc2	názvosloví
byl	být	k5eAaImAgMnS	být
spjat	spjat	k2eAgMnSc1d1	spjat
s	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
české	český	k2eAgFnSc2d1	Česká
vědy	věda	k1gFnSc2	věda
v	v	k7c6	v
době	doba	k1gFnSc6	doba
národního	národní	k2eAgNnSc2d1	národní
obrození	obrození	k1gNnSc2	obrození
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Dobrovského	Dobrovského	k2eAgFnSc7d1	Dobrovského
Mluvnicí	mluvnice	k1gFnSc7	mluvnice
se	se	k3xPyFc4	se
Jungmannův	Jungmannův	k2eAgInSc1d1	Jungmannův
slovník	slovník	k1gInSc1	slovník
stal	stát	k5eAaPmAgInS	stát
základním	základní	k2eAgInSc7d1	základní
kamenem	kámen	k1gInSc7	kámen
pro	pro	k7c4	pro
pozvolné	pozvolný	k2eAgNnSc4d1	pozvolné
ustanovení	ustanovení	k1gNnSc4	ustanovení
normy	norma	k1gFnSc2	norma
i	i	k8xC	i
kodifikace	kodifikace	k1gFnSc2	kodifikace
moderní	moderní	k2eAgFnSc2d1	moderní
češtiny	čeština	k1gFnSc2	čeština
<g/>
.	.	kIx.	.
</s>
<s>
Jungmann	Jungmann	k1gMnSc1	Jungmann
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
také	také	k9	také
historií	historie	k1gFnSc7	historie
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
Historie	historie	k1gFnSc1	historie
literatury	literatura	k1gFnSc2	literatura
české	český	k2eAgFnSc2d1	Česká
aneb	aneb	k?	aneb
soustavný	soustavný	k2eAgInSc4d1	soustavný
přehled	přehled	k1gInSc4	přehled
spisů	spis	k1gInPc2	spis
českých	český	k2eAgInPc2d1	český
s	s	k7c7	s
krátkou	krátký	k2eAgFnSc7d1	krátká
historií	historie	k1gFnSc7	historie
národu	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
osvícení	osvícení	k1gNnSc2	osvícení
a	a	k8xC	a
jazyka	jazyk	k1gInSc2	jazyk
(	(	kIx(	(
<g/>
1825	[number]	k4	1825
<g/>
)	)	kIx)	)
sepsal	sepsat	k5eAaPmAgMnS	sepsat
české	český	k2eAgFnPc4d1	Česká
literární	literární	k2eAgFnPc4d1	literární
památky	památka	k1gFnPc4	památka
od	od	k7c2	od
nejstarších	starý	k2eAgFnPc2d3	nejstarší
dob	doba	k1gFnPc2	doba
po	po	k7c4	po
svou	svůj	k3xOyFgFnSc4	svůj
současnost	současnost	k1gFnSc4	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Periodizace	periodizace	k1gFnSc1	periodizace
dějin	dějiny	k1gFnPc2	dějiny
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
pojetí	pojetí	k1gNnSc2	pojetí
Josefa	Josef	k1gMnSc2	Josef
Dobrovského	Dobrovský	k1gMnSc2	Dobrovský
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
rozvíjely	rozvíjet	k5eAaImAgFnP	rozvíjet
pozitivistické	pozitivistický	k2eAgFnPc1d1	pozitivistická
metody	metoda	k1gFnPc1	metoda
zkoumání	zkoumání	k1gNnSc2	zkoumání
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Gebauer	Gebauer	k1gMnSc1	Gebauer
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
představitelem	představitel	k1gMnSc7	představitel
této	tento	k3xDgFnSc2	tento
fáze	fáze	k1gFnSc2	fáze
dějin	dějiny	k1gFnPc2	dějiny
bohemistiky	bohemistika	k1gFnSc2	bohemistika
byl	být	k5eAaImAgMnS	být
Jan	Jan	k1gMnSc1	Jan
Gebauer	Gebauer	k1gMnSc1	Gebauer
(	(	kIx(	(
<g/>
1838	[number]	k4	1838
<g/>
–	–	k?	–
<g/>
1907	[number]	k4	1907
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejuznávanějších	uznávaný	k2eAgMnPc2d3	nejuznávanější
českých	český	k2eAgMnPc2d1	český
vědců	vědec	k1gMnPc2	vědec
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
dílo	dílo	k1gNnSc1	dílo
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
pro	pro	k7c4	pro
český	český	k2eAgInSc4d1	český
jazyk	jazyk	k1gInSc4	jazyk
znamenalo	znamenat	k5eAaImAgNnS	znamenat
totéž	týž	k3xTgNnSc1	týž
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
dílo	dílo	k1gNnSc4	dílo
Josefa	Josef	k1gMnSc4	Josef
Dobrovského	Dobrovský	k1gMnSc2	Dobrovský
o	o	k7c4	o
100	[number]	k4	100
let	léto	k1gNnPc2	léto
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
docent	docent	k1gMnSc1	docent
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
profesor	profesor	k1gMnSc1	profesor
češtiny	čeština	k1gFnSc2	čeština
na	na	k7c6	na
Karlově	Karlův	k2eAgFnSc6d1	Karlova
univerzitě	univerzita	k1gFnSc6	univerzita
vychoval	vychovat	k5eAaPmAgMnS	vychovat
řadu	řada	k1gFnSc4	řada
vynikajících	vynikající	k2eAgMnPc2d1	vynikající
nástupců	nástupce	k1gMnPc2	nástupce
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Václav	Václav	k1gMnSc1	Václav
Ertl	Ertl	k1gMnSc1	Ertl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Gebauer	Gebauer	k1gInSc1	Gebauer
navázal	navázat	k5eAaPmAgInS	navázat
na	na	k7c4	na
práci	práce	k1gFnSc4	práce
Dobrovského	Dobrovský	k1gMnSc2	Dobrovský
a	a	k8xC	a
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
kodifikaci	kodifikace	k1gFnSc4	kodifikace
spisovné	spisovný	k2eAgFnSc2d1	spisovná
češtiny	čeština	k1gFnSc2	čeština
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
brala	brát	k5eAaImAgFnS	brát
ohled	ohled	k1gInSc4	ohled
na	na	k7c4	na
tehdejší	tehdejší	k2eAgFnPc4d1	tehdejší
aktuální	aktuální	k2eAgFnPc4d1	aktuální
potřeby	potřeba	k1gFnPc4	potřeba
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
ohledech	ohled	k1gInPc6	ohled
platná	platný	k2eAgNnPc4d1	platné
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Gebauerova	Gebauerův	k2eAgFnSc1d1	Gebauerova
Historická	historický	k2eAgFnSc1d1	historická
mluvnice	mluvnice	k1gFnSc1	mluvnice
jazyka	jazyk	k1gInSc2	jazyk
českého	český	k2eAgInSc2d1	český
je	on	k3xPp3gMnPc4	on
klíčovou	klíčový	k2eAgFnSc7d1	klíčová
bohemistickou	bohemistický	k2eAgFnSc7d1	bohemistická
prací	práce	k1gFnSc7	práce
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
stihnul	stihnout	k5eAaPmAgMnS	stihnout
publikovat	publikovat	k5eAaBmF	publikovat
tři	tři	k4xCgFnPc4	tři
části	část	k1gFnPc4	část
–	–	k?	–
Hláskosloví	hláskosloví	k1gNnSc1	hláskosloví
(	(	kIx(	(
<g/>
1894	[number]	k4	1894
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tvarosloví	tvarosloví	k1gNnSc1	tvarosloví
–	–	k?	–
Skloňování	skloňování	k1gNnPc2	skloňování
(	(	kIx(	(
<g/>
1896	[number]	k4	1896
<g/>
)	)	kIx)	)
a	a	k8xC	a
Tvarosloví	tvarosloví	k1gNnSc1	tvarosloví
–	–	k?	–
Časování	časování	k1gNnPc2	časování
(	(	kIx(	(
<g/>
1898	[number]	k4	1898
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrtou	čtvrtá	k1gFnSc4	čtvrtá
část	část	k1gFnSc4	část
<g/>
,	,	kIx,	,
Skladba	skladba	k1gFnSc1	skladba
upravil	upravit	k5eAaPmAgMnS	upravit
k	k	k7c3	k
vydání	vydání	k1gNnSc3	vydání
až	až	k9	až
František	František	k1gMnSc1	František
Trávníček	Trávníček	k1gMnSc1	Trávníček
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
<g/>
.	.	kIx.	.
</s>
<s>
Celé	celý	k2eAgNnSc1d1	celé
dílo	dílo	k1gNnSc1	dílo
je	být	k5eAaImIp3nS	být
podloženo	podložit	k5eAaPmNgNnS	podložit
rozsáhlým	rozsáhlý	k2eAgInSc7d1	rozsáhlý
<g/>
,	,	kIx,	,
pečlivě	pečlivě	k6eAd1	pečlivě
zpracovaným	zpracovaný	k2eAgInSc7d1	zpracovaný
materiálem	materiál	k1gInSc7	materiál
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
spolehlivý	spolehlivý	k2eAgInSc4d1	spolehlivý
pramen	pramen	k1gInSc4	pramen
<g/>
.	.	kIx.	.
</s>
<s>
Dvoudílný	dvoudílný	k2eAgInSc1d1	dvoudílný
Slovník	slovník	k1gInSc1	slovník
staročeský	staročeský	k2eAgInSc1d1	staročeský
(	(	kIx(	(
<g/>
1903	[number]	k4	1903
<g/>
)	)	kIx)	)
stihl	stihnout	k5eAaPmAgMnS	stihnout
Gebauer	Gebauer	k1gMnSc1	Gebauer
zpracovat	zpracovat	k5eAaPmF	zpracovat
a	a	k8xC	a
vydat	vydat	k5eAaPmF	vydat
jen	jen	k9	jen
po	po	k7c4	po
heslo	heslo	k1gNnSc4	heslo
naliti	nalit	k2eAgMnPc1d1	nalit
<g/>
.	.	kIx.	.
</s>
<s>
Dílo	dílo	k1gNnSc4	dílo
později	pozdě	k6eAd2	pozdě
dokončil	dokončit	k5eAaPmAgMnS	dokončit
profesor	profesor	k1gMnSc1	profesor
Emil	Emil	k1gMnSc1	Emil
Smetánka	smetánka	k1gFnSc1	smetánka
<g/>
.	.	kIx.	.
<g/>
Mluvnice	mluvnice	k1gFnSc1	mluvnice
česká	český	k2eAgFnSc1d1	Česká
pro	pro	k7c4	pro
školy	škola	k1gFnPc4	škola
střední	střední	k2eAgFnSc2d1	střední
a	a	k8xC	a
ústavy	ústava	k1gFnSc2	ústava
učitelské	učitelský	k2eAgFnSc2d1	učitelská
I	I	kA	I
<g/>
,	,	kIx,	,
II	II	kA	II
(	(	kIx(	(
<g/>
1890	[number]	k4	1890
<g/>
)	)	kIx)	)
původně	původně	k6eAd1	původně
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
jako	jako	k9	jako
školní	školní	k2eAgFnPc4d1	školní
učebnice	učebnice	k1gFnPc4	učebnice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
příručkou	příručka	k1gFnSc7	příručka
kodifikační	kodifikační	k2eAgFnSc1d1	kodifikační
a	a	k8xC	a
základním	základní	k2eAgNnSc7d1	základní
bohemistickým	bohemistický	k2eAgNnSc7d1	bohemistické
dílem	dílo	k1gNnSc7	dílo
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
ucelený	ucelený	k2eAgInSc4d1	ucelený
popis	popis	k1gInSc4	popis
soudobého	soudobý	k2eAgNnSc2d1	soudobé
českého	český	k2eAgNnSc2d1	české
hláskosloví	hláskosloví	k1gNnSc2	hláskosloví
<g/>
,	,	kIx,	,
slovotvorby	slovotvorba	k1gFnSc2	slovotvorba
<g/>
,	,	kIx,	,
tvarosloví	tvarosloví	k1gNnSc2	tvarosloví
<g/>
,	,	kIx,	,
skladby	skladba	k1gFnSc2	skladba
i	i	k8xC	i
pravopisu	pravopis	k1gInSc2	pravopis
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
toto	tento	k3xDgNnSc4	tento
zpracování	zpracování	k1gNnSc4	zpracování
české	český	k2eAgFnSc2d1	Česká
mluvnice	mluvnice	k1gFnSc2	mluvnice
navázali	navázat	k5eAaPmAgMnP	navázat
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Havránek	Havránek	k1gMnSc1	Havránek
a	a	k8xC	a
Alois	Alois	k1gMnSc1	Alois
Jedlička	Jedlička	k1gMnSc1	Jedlička
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
České	český	k2eAgFnSc6d1	Česká
mluvnici	mluvnice	k1gFnSc6	mluvnice
<g/>
.	.	kIx.	.
</s>
<s>
Výše	vysoce	k6eAd2	vysoce
uvedená	uvedený	k2eAgFnSc1d1	uvedená
Mluvnice	mluvnice	k1gFnSc1	mluvnice
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1891	[number]	k4	1891
také	také	k6eAd1	také
ve	v	k7c6	v
zkrácené	zkrácený	k2eAgFnSc6d1	zkrácená
<g/>
,	,	kIx,	,
přehlednější	přehlední	k2eAgFnSc6d2	přehlední
a	a	k8xC	a
přístupnější	přístupný	k2eAgFnSc6d2	přístupnější
podobě	podoba	k1gFnSc6	podoba
jako	jako	k8xS	jako
Krátká	krátký	k2eAgFnSc1d1	krátká
mluvnice	mluvnice	k1gFnSc1	mluvnice
česká	český	k2eAgFnSc1d1	Česká
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejích	její	k3xOp3gInPc6	její
pozdějších	pozdní	k2eAgInPc6d2	pozdější
vydáním	vydání	k1gNnSc7	vydání
pracoval	pracovat	k5eAaImAgInS	pracovat
kromě	kromě	k7c2	kromě
jiných	jiný	k1gMnPc2	jiný
už	už	k9	už
zmíněný	zmíněný	k2eAgMnSc1d1	zmíněný
Václav	Václav	k1gMnSc1	Václav
Ertl	Ertl	k1gMnSc1	Ertl
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnPc1d1	další
pozitivističtí	pozitivistický	k2eAgMnPc1d1	pozitivistický
bohemistéVáclav	bohemistéVáclat	k5eAaPmDgInS	bohemistéVáclat
Ertl	Ertl	k1gMnSc1	Ertl
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Zubatý	zubatý	k2eAgMnSc1d1	zubatý
<g/>
,	,	kIx,	,
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Vlček	Vlček	k1gMnSc1	Vlček
Mezi	mezi	k7c7	mezi
1	[number]	k4	1
<g/>
.	.	kIx.	.
a	a	k8xC	a
2	[number]	k4	2
<g/>
.	.	kIx.	.
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
se	se	k3xPyFc4	se
bohemistika	bohemistika	k1gFnSc1	bohemistika
částečně	částečně	k6eAd1	částečně
přiklonila	přiklonit	k5eAaPmAgFnS	přiklonit
ke	k	k7c3	k
strukturalismu	strukturalismus	k1gInSc3	strukturalismus
(	(	kIx(	(
<g/>
Vilém	Vilém	k1gMnSc1	Vilém
Mathesius	Mathesius	k1gMnSc1	Mathesius
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Mukařovský	Mukařovský	k1gMnSc1	Mukařovský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
2	[number]	k4	2
<g/>
.	.	kIx.	.
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
se	se	k3xPyFc4	se
prosazovalo	prosazovat	k5eAaImAgNnS	prosazovat
úsilí	úsilí	k1gNnSc1	úsilí
o	o	k7c4	o
marxistickou	marxistický	k2eAgFnSc4d1	marxistická
metodologii	metodologie	k1gFnSc4	metodologie
(	(	kIx(	(
<g/>
Bohuslav	Bohuslav	k1gMnSc1	Bohuslav
Havránek	Havránek	k1gMnSc1	Havránek
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Horálek	Horálek	k1gMnSc1	Horálek
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Nejedlý	Nejedlý	k1gMnSc1	Nejedlý
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ředitel	ředitel	k1gMnSc1	ředitel
Ústavu	ústav	k1gInSc2	ústav
pro	pro	k7c4	pro
jazyk	jazyk	k1gInSc4	jazyk
český	český	k2eAgMnSc1d1	český
František	František	k1gMnSc1	František
Daneš	Daneš	k1gMnSc1	Daneš
(	(	kIx(	(
<g/>
*	*	kIx~	*
1919	[number]	k4	1919
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lumír	Lumír	k1gMnSc1	Lumír
Klimeš	Klimeš	k1gMnSc1	Klimeš
<g/>
:	:	kIx,	:
Úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
vědecké	vědecký	k2eAgFnSc2d1	vědecká
práce	práce	k1gFnSc2	práce
v	v	k7c6	v
jazykovědné	jazykovědný	k2eAgFnSc6d1	jazykovědná
bohemistice	bohemistika	k1gFnSc6	bohemistika
<g/>
,	,	kIx,	,
Pedagogická	pedagogický	k2eAgFnSc1d1	pedagogická
fakulta	fakulta	k1gFnSc1	fakulta
Západočeské	západočeský	k2eAgFnSc2d1	Západočeská
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
,	,	kIx,	,
Plzeň	Plzeň	k1gFnSc1	Plzeň
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80-7082-740-8	[number]	k4	80-7082-740-8
Jana	Jana	k1gFnSc1	Jana
Pleskalová	Pleskalová	k1gFnSc1	Pleskalová
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
:	:	kIx,	:
Kapitoly	kapitola	k1gFnPc1	kapitola
z	z	k7c2	z
dějin	dějiny	k1gFnPc2	dějiny
české	český	k2eAgFnSc2d1	Česká
jazykovědné	jazykovědný	k2eAgFnSc2d1	jazykovědná
bohemistiky	bohemistika	k1gFnSc2	bohemistika
<g/>
,	,	kIx,	,
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2007	[number]	k4	2007
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-200-1523-5	[number]	k4	978-80-200-1523-5
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Pastyřík	Pastyřík	k1gMnSc1	Pastyřík
<g />
.	.	kIx.	.
</s>
<s>
<g/>
:	:	kIx,	:
Úvod	úvod	k1gInSc1	úvod
do	do	k7c2	do
studia	studio	k1gNnSc2	studio
bohemistiky	bohemistika	k1gFnSc2	bohemistika
<g/>
,	,	kIx,	,
Gaudeamus	Gaudeamus	k1gInSc1	Gaudeamus
<g/>
,	,	kIx,	,
Hradec	Hradec	k1gInSc1	Hradec
Králové	Králová	k1gFnSc2	Králová
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-7435-021-4	[number]	k4	978-80-7435-021-4
Daniel	Daniel	k1gMnSc1	Daniel
Bína	Bína	k1gMnSc1	Bína
<g/>
,	,	kIx,	,
Miloš	Miloš	k1gMnSc1	Miloš
Zelenka	Zelenka	k1gMnSc1	Zelenka
a	a	k8xC	a
kolektiv	kolektiv	k1gInSc1	kolektiv
<g/>
:	:	kIx,	:
Bohemistika	bohemistika	k1gFnSc1	bohemistika
v	v	k7c6	v
edukačním	edukační	k2eAgInSc6d1	edukační
procesu	proces	k1gInSc6	proces
–	–	k?	–
tradice	tradice	k1gFnSc2	tradice
<g/>
,	,	kIx,	,
problémy	problém	k1gInPc1	problém
<g/>
,	,	kIx,	,
perspektivy	perspektiva	k1gFnPc1	perspektiva
<g/>
,	,	kIx,	,
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
Johanus	Johanus	k1gMnSc1	Johanus
<g/>
,	,	kIx,	,
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
2012	[number]	k4	2012
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-87510-07-0	[number]	k4	978-80-87510-07-0
Lingvistika	lingvistika	k1gFnSc1	lingvistika
Lexikografie	lexikografie	k1gFnSc1	lexikografie
Literatura	literatura	k1gFnSc1	literatura
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
bohemistika	bohemistika	k1gFnSc1	bohemistika
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
ucjtk	ucjtk	k1gMnSc1	ucjtk
<g/>
.	.	kIx.	.
<g/>
ff	ff	kA	ff
<g/>
.	.	kIx.	.
<g/>
cuni	cuni	k6eAd1	cuni
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
Ústav	ústav	k1gInSc4	ústav
českého	český	k2eAgInSc2d1	český
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
teorie	teorie	k1gFnSc2	teorie
komunikace	komunikace	k1gFnSc2	komunikace
Filozofické	filozofický	k2eAgFnSc2d1	filozofická
fakulty	fakulta	k1gFnSc2	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
cl	cl	k?	cl
<g/>
.	.	kIx.	.
<g/>
ff	ff	kA	ff
<g/>
.	.	kIx.	.
<g/>
cuni	cuni	k6eAd1	cuni
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
Ústav	ústav	k1gInSc1	ústav
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
a	a	k8xC	a
literární	literární	k2eAgFnSc2d1	literární
vědy	věda	k1gFnSc2	věda
Filozofické	filozofický	k2eAgFnSc2d1	filozofická
fakulty	fakulta	k1gFnSc2	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
ubs	ubs	k?	ubs
<g/>
.	.	kIx.	.
<g/>
ff	ff	kA	ff
<g/>
.	.	kIx.	.
<g/>
cuni	cuni	k6eAd1	cuni
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
Ústav	ústav	k1gInSc1	ústav
bohemistických	bohemistický	k2eAgFnPc2d1	bohemistická
studií	studie	k1gFnPc2	studie
Filozofické	filozofický	k2eAgFnSc2d1	filozofická
fakulty	fakulta	k1gFnSc2	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
Machová	Machová	k1gFnSc1	Machová
<g/>
,	,	kIx,	,
Svatava	Svatava	k1gFnSc1	Svatava
<g/>
;	;	kIx,	;
Chvátalová	Chvátalová	k1gFnSc1	Chvátalová
<g/>
,	,	kIx,	,
Klára	Klára	k1gFnSc1	Klára
<g/>
;	;	kIx,	;
Velčovský	Velčovský	k1gMnSc1	Velčovský
<g/>
,	,	kIx,	,
Václav	Václav	k1gMnSc1	Václav
(	(	kIx(	(
<g/>
eds	eds	k?	eds
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Slovník	slovník	k1gInSc1	slovník
osobností	osobnost	k1gFnPc2	osobnost
jazykovědné	jazykovědný	k2eAgFnSc2d1	jazykovědná
bohemistiky	bohemistika	k1gFnSc2	bohemistika
</s>
