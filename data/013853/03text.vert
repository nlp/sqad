<s>
Chytrý	chytrý	k2eAgInSc1d1
majetek	majetek	k1gInSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Chytrý	chytrý	k2eAgInSc1d1
majetek	majetek	k1gInSc1
(	(	kIx(
<g/>
v	v	k7c6
angličtině	angličtina	k1gFnSc6
Smart_Property	Smart_Property	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
majetek	majetek	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gNnSc1
vlastnictví	vlastnictví	k1gNnSc1
je	být	k5eAaImIp3nS
řízeno	řídit	k5eAaImNgNnS
přes	přes	k7c4
bitcoinový	bitcoinový	k2eAgInSc4d1
blockchain	blockchain	k1gInSc4
za	za	k7c2
pomoci	pomoc	k1gFnSc2
kontraktů	kontrakt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chytrým	chytrý	k2eAgInSc7d1
majetkem	majetek	k1gInSc7
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
nefyzické	fyzický	k2eNgFnPc4d1
věci	věc	k1gFnPc4
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
např.	např.	kA
akcie	akcie	k1gFnSc2
firmy	firma	k1gFnSc2
<g/>
,	,	kIx,
přístupová	přístupový	k2eAgNnPc4d1
práva	právo	k1gNnPc4
k	k	k7c3
vzdálenému	vzdálený	k2eAgInSc3d1
počítači	počítač	k1gInSc3
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
fyzické	fyzický	k2eAgFnPc4d1
věci	věc	k1gFnPc4
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
např.	např.	kA
auta	auto	k1gNnPc4
<g/>
,	,	kIx,
mobilní	mobilní	k2eAgInPc4d1
telefony	telefon	k1gInPc4
<g/>
,	,	kIx,
domy	dům	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
se	se	k3xPyFc4
z	z	k7c2
obyčejného	obyčejný	k2eAgInSc2d1
majetku	majetek	k1gInSc2
stane	stanout	k5eAaPmIp3nS
chytrý	chytrý	k2eAgInSc4d1
majetek	majetek	k1gInSc4
<g/>
,	,	kIx,
pak	pak	k6eAd1
je	být	k5eAaImIp3nS
umožněno	umožnit	k5eAaPmNgNnS
jeho	jeho	k3xOp3gNnSc1
obchodování	obchodování	k1gNnSc1
s	s	k7c7
podstatně	podstatně	k6eAd1
menší	malý	k2eAgFnSc7d2
důvěrou	důvěra	k1gFnSc7
všech	všecek	k3xTgFnPc2
zúčastněných	zúčastněný	k2eAgFnPc2d1
stran	strana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
snižuje	snižovat	k5eAaImIp3nS
šance	šance	k1gFnSc1
na	na	k7c4
podvod	podvod	k1gInSc4
<g/>
,	,	kIx,
zmenšuje	zmenšovat	k5eAaImIp3nS
zprostředkovatelské	zprostředkovatelský	k2eAgInPc4d1
poplatky	poplatek	k1gInPc4
a	a	k8xC
umožňuje	umožňovat	k5eAaImIp3nS
provádět	provádět	k5eAaImF
obchody	obchod	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yRgInPc1,k3yIgInPc1
by	by	kYmCp3nP
se	se	k3xPyFc4
jinak	jinak	k6eAd1
nikdy	nikdy	k6eAd1
neuskutečnily	uskutečnit	k5eNaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
to	ten	k3xDgNnSc1
umožňuje	umožňovat	k5eAaImIp3nS
půjčení	půjčení	k1gNnSc1
peněz	peníze	k1gInPc2
od	od	k7c2
cizích	cizí	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
vezmou	vzít	k5eAaPmIp3nP
chytrý	chytrý	k2eAgInSc4d1
majetek	majetek	k1gInSc4
jako	jako	k8xS,k8xC
zástavu	zástava	k1gFnSc4
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
bude	být	k5eAaImBp3nS
půjčování	půjčování	k1gNnPc4
více	hodně	k6eAd2
konkurenční	konkurenční	k2eAgFnPc4d1
a	a	k8xC
tím	ten	k3xDgNnSc7
také	také	k9
levnější	levný	k2eAgMnSc1d2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Primitivní	primitivní	k2eAgFnPc1d1
formy	forma	k1gFnPc1
chytrého	chytrý	k2eAgInSc2d1
majetku	majetek	k1gInSc2
jsou	být	k5eAaImIp3nP
již	již	k6eAd1
běžné	běžný	k2eAgInPc1d1
–	–	k?
pokud	pokud	k8xS
vlastníte	vlastnit	k5eAaImIp2nP
auto	auto	k1gNnSc4
<g/>
,	,	kIx,
pravděpodobně	pravděpodobně	k6eAd1
je	být	k5eAaImIp3nS
vybaveno	vybavit	k5eAaPmNgNnS
imobilizérem	imobilizér	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Imobilizéry	imobilizér	k1gInPc7
rozšiřují	rozšiřovat	k5eAaImIp3nP
fyzický	fyzický	k2eAgInSc4d1
klíč	klíč	k1gInSc4
o	o	k7c4
kryptografický	kryptografický	k2eAgInSc4d1
identifikátor	identifikátor	k1gInSc4
(	(	kIx(
<g/>
digitální	digitální	k2eAgInSc4d1
klíč	klíč	k1gInSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
zajišťuje	zajišťovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
pouze	pouze	k6eAd1
vlastníci	vlastník	k1gMnPc1
tohoto	tento	k3xDgInSc2
identifikátoru	identifikátor	k1gInSc2
mohou	moct	k5eAaImIp3nP
nastartovat	nastartovat	k5eAaPmF
motor	motor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Imobilizéry	imobilizér	k1gInPc7
dramaticky	dramaticky	k6eAd1
snížily	snížit	k5eAaPmAgInP
krádeže	krádež	k1gFnPc4
aut	auto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Mnoho	mnoho	k4c1
dalších	další	k2eAgInPc2d1
případů	případ	k1gInPc2
moderního	moderní	k2eAgInSc2d1
majetku	majetek	k1gInSc2
je	být	k5eAaImIp3nS
chráněno	chránit	k5eAaImNgNnS
proti	proti	k7c3
krádeži	krádež	k1gFnSc3
pomocí	pomocí	k7c2
kryptografie	kryptografie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Například	například	k6eAd1
některé	některý	k3yIgInPc1
chytré	chytrý	k2eAgInPc1d1
telefony	telefon	k1gInPc1
odmítnou	odmítnout	k5eAaPmIp3nP
spolupráci	spolupráce	k1gFnSc4
s	s	k7c7
uživatelem	uživatel	k1gMnSc7
<g/>
,	,	kIx,
pokud	pokud	k8xS
není	být	k5eNaImIp3nS
zadán	zadán	k2eAgInSc1d1
správný	správný	k2eAgInSc1d1
PIN	pin	k1gInSc1
kód	kód	k1gInSc4
pro	pro	k7c4
odemknutí	odemknutí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kryptografie	kryptografie	k1gFnSc1
je	být	k5eAaImIp3nS
zde	zde	k6eAd1
využívána	využívat	k5eAaPmNgFnS,k5eAaImNgFnS
dvojím	dvojí	k4xRgInSc7
způsobem	způsob	k1gInSc7
<g/>
:	:	kIx,
mění	měnit	k5eAaImIp3nP
ukradená	ukradený	k2eAgNnPc1d1
zařízení	zařízení	k1gNnPc1
v	v	k7c4
nepoužitelná	použitelný	k2eNgNnPc4d1
a	a	k8xC
také	také	k9
brání	bránit	k5eAaImIp3nP
v	v	k7c6
ukradení	ukradení	k1gNnSc6
telefonního	telefonní	k2eAgNnSc2d1
čísla	číslo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
I	i	k9
přes	přes	k7c4
výše	vysoce	k6eAd2
uvedené	uvedený	k2eAgInPc4d1
příklady	příklad	k1gInPc4
nebyl	být	k5eNaImAgInS
celý	celý	k2eAgInSc1d1
potenciál	potenciál	k1gInSc1
kryptograficky	kryptograficky	k?
aktivovaného	aktivovaný	k2eAgInSc2d1
majetku	majetek	k1gInSc2
zatím	zatím	k6eAd1
plně	plně	k6eAd1
prozkoumán	prozkoumat	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Soukromý	soukromý	k2eAgInSc4d1
(	(	kIx(
<g/>
privátní	privátní	k2eAgInSc4d1
<g/>
)	)	kIx)
klíč	klíč	k1gInSc4
je	být	k5eAaImIp3nS
obvykle	obvykle	k6eAd1
uložen	uložit	k5eAaPmNgInS
ve	v	k7c6
fyzickém	fyzický	k2eAgInSc6d1
kontejneru	kontejner	k1gInSc6
(	(	kIx(
<g/>
fyzický	fyzický	k2eAgInSc4d1
klíč	klíč	k1gInSc4
nebo	nebo	k8xC
SIM	SIM	kA
karta	karta	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
není	být	k5eNaImIp3nS
možné	možný	k2eAgNnSc1d1
jej	on	k3xPp3gNnSc4
jednoduše	jednoduše	k6eAd1
přesunout	přesunout	k5eAaPmF
nebo	nebo	k8xC
pozměnit	pozměnit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chytrý	chytrý	k2eAgInSc1d1
majetek	majetek	k1gInSc1
toto	tento	k3xDgNnSc4
mění	měnit	k5eAaImIp3nS
<g/>
:	:	kIx,
vlastnictví	vlastnictví	k1gNnSc1
soukromého	soukromý	k2eAgInSc2d1
klíče	klíč	k1gInSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
přesunuto	přesunout	k5eAaPmNgNnS
pomocí	pomocí	k7c2
Bitcoinové	Bitcoinový	k2eAgFnSc2d1
transakce	transakce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Teorie	teorie	k1gFnSc1
</s>
<s>
Tato	tento	k3xDgFnSc1
sekce	sekce	k1gFnSc1
předpokládá	předpokládat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
čtenář	čtenář	k1gMnSc1
obeznámen	obeznámit	k5eAaPmNgMnS
s	s	k7c7
protokolem	protokol	k1gInSc7
Bitcoin	Bitcoina	k1gFnPc2
a	a	k8xC
rozumí	rozumět	k5eAaImIp3nS
kontraktům	kontrakt	k1gInPc3
<g/>
.	.	kIx.
</s>
<s>
Obchod	obchod	k1gInSc1
s	s	k7c7
chytrým	chytrý	k2eAgInSc7d1
majetkem	majetek	k1gInSc7
bude	být	k5eAaImBp3nS
demonstrován	demonstrovat	k5eAaBmNgMnS
na	na	k7c6
příkladu	příklad	k1gInSc6
s	s	k7c7
autem	auto	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počítač	počítač	k1gInSc1
auta	auto	k1gNnSc2
vyžaduje	vyžadovat	k5eAaImIp3nS
autentifikaci	autentifikace	k1gFnSc4
po	po	k7c6
vlastníkovi	vlastník	k1gMnSc6
klíče	klíč	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Digitální	digitální	k2eAgInSc1d1
klíč	klíč	k1gInSc1
je	být	k5eAaImIp3nS
založen	založit	k5eAaPmNgInS
na	na	k7c6
algoritmu	algoritmus	k1gInSc6
šifrování	šifrování	k1gNnSc2
s	s	k7c7
veřejným	veřejný	k2eAgInSc7d1
klíčem	klíč	k1gInSc7
<g/>
,	,	kIx,
např.	např.	kA
ECDSA-	ECDSA-	k1gFnSc1
<g/>
256	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Auto	auto	k1gNnSc1
začíná	začínat	k5eAaImIp3nS
žít	žít	k5eAaImF
svým	svůj	k3xOyFgInSc7
životem	život	k1gInSc7
<g/>
,	,	kIx,
když	když	k8xS
opustí	opustit	k5eAaPmIp3nP
brány	brána	k1gFnPc1
továrny	továrna	k1gFnSc2
s	s	k7c7
klíčem	klíč	k1gInSc7
<g/>
,	,	kIx,
kterým	který	k3yIgNnSc7,k3yRgNnSc7,k3yQgNnSc7
má	mít	k5eAaImIp3nS
veřejnou	veřejný	k2eAgFnSc4d1
část	část	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Malé	Malé	k2eAgInPc1d1
množství	množství	k1gNnSc4
Bitcoinu	Bitcoin	k1gInSc2
je	být	k5eAaImIp3nS
vloženo	vložit	k5eAaPmNgNnS
na	na	k7c4
tento	tento	k3xDgInSc4
klíč	klíč	k1gInSc4
<g/>
,	,	kIx,
např.	např.	kA
(	(	kIx(
<g/>
0.0001	0.0001	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
označíme	označit	k5eAaPmIp1nP
ho	on	k3xPp3gMnSc4
T.	T.	kA
Auto	auto	k1gNnSc1
je	být	k5eAaImIp3nS
vybaveno	vybavit	k5eAaPmNgNnS
digitálním	digitální	k2eAgInSc7d1
certifikátem	certifikát	k1gInSc7
od	od	k7c2
výrobce	výrobce	k1gMnSc2
a	a	k8xC
identifikačním	identifikační	k2eAgInSc7d1
klíčem	klíč	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
svázán	svázán	k2eAgInSc1d1
s	s	k7c7
veřejnou	veřejný	k2eAgFnSc7d1
částí	část	k1gFnSc7
v	v	k7c6
certifikátu	certifikát	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
umožňuje	umožňovat	k5eAaImIp3nS
autu	aut	k1gInSc2
prokázat	prokázat	k5eAaPmF
různé	různý	k2eAgFnPc4d1
skutečnosti	skutečnost	k1gFnPc4
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
např.	např.	kA
svou	svůj	k3xOyFgFnSc4
existenci	existence	k1gFnSc4
<g/>
,	,	kIx,
stáří	stáří	k1gNnSc4
nebo	nebo	k8xC
počet	počet	k1gInSc4
najetých	najetý	k2eAgInPc2d1
kilometrů	kilometr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Má	mít	k5eAaImIp3nS
<g/>
-li	-li	k?
být	být	k5eAaImF
auto	auto	k1gNnSc1
prodáno	prodat	k5eAaPmNgNnS
<g/>
,	,	kIx,
následuje	následovat	k5eAaImIp3nS
tento	tento	k3xDgInSc1
scénář	scénář	k1gInSc1
<g/>
:	:	kIx,
</s>
<s>
Kupující	kupující	k1gMnSc1
vygeneruje	vygenerovat	k5eAaPmIp3nS
nonci	nonce	k1gFnSc4
(	(	kIx(
<g/>
náhodné	náhodný	k2eAgNnSc4d1
číslo	číslo	k1gNnSc4
<g/>
)	)	kIx)
a	a	k8xC
požádá	požádat	k5eAaPmIp3nS
prodejce	prodejce	k1gMnPc4
o	o	k7c4
zaslání	zaslání	k1gNnSc4
dat	datum	k1gNnPc2
o	o	k7c6
autu	aut	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Prodejce	prodejce	k1gMnSc1
předá	předat	k5eAaPmIp3nS
autu	aut	k1gInSc2
náhodné	náhodný	k2eAgNnSc1d1
číslo	číslo	k1gNnSc1
a	a	k8xC
auto	auto	k1gNnSc1
vrátí	vrátit	k5eAaPmIp3nS
datovou	datový	k2eAgFnSc4d1
strukturu	struktura	k1gFnSc4
podepsanou	podepsaný	k2eAgFnSc4d1
svým	svůj	k3xOyFgInSc7
identifikačním	identifikační	k2eAgInSc7d1
klíčem	klíč	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Datová	datový	k2eAgFnSc1d1
struktura	struktura	k1gFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
nonci	nonce	k1gFnSc4
<g/>
,	,	kIx,
veřejný	veřejný	k2eAgInSc4d1
klíč	klíč	k1gInSc4
auta	auto	k1gNnSc2
<g/>
,	,	kIx,
technické	technický	k2eAgInPc4d1
údaje	údaj	k1gInPc4
o	o	k7c6
autu	aut	k1gInSc6
<g/>
,	,	kIx,
veřejný	veřejný	k2eAgInSc1d1
klíč	klíč	k1gInSc1
současného	současný	k2eAgMnSc2d1
vlastníka	vlastník	k1gMnSc2
<g/>
,	,	kIx,
transakci	transakce	k1gFnSc4
+	+	kIx~
větev	větev	k1gFnSc4
merkleova	merkleův	k2eAgInSc2d1
stromu	strom	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
obsahuje	obsahovat	k5eAaImIp3nS
informaci	informace	k1gFnSc4
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
kdo	kdo	k3yRnSc1,k3yQnSc1,k3yInSc1
je	být	k5eAaImIp3nS
poslední	poslední	k2eAgMnSc1d1
majitel	majitel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
zajišťuje	zajišťovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
kupující	kupující	k1gMnSc1
ví	vědět	k5eAaImIp3nS
<g/>
,	,	kIx,
co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
dostane	dostat	k5eAaPmIp3nS
a	a	k8xC
že	že	k8xS
auto	auto	k1gNnSc1
pochází	pocházet	k5eAaImIp3nS
od	od	k7c2
reálného	reálný	k2eAgMnSc2d1
prodejce	prodejce	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Prodejce	prodejce	k1gMnSc1
zvolí	zvolit	k5eAaPmIp3nS
klíč	klíč	k1gInSc4
k	k	k7c3
přijmutí	přijmutí	k1gNnSc3
platby	platba	k1gFnSc2
<g/>
,	,	kIx,
k	k	k7c3
<g/>
1	#num#	k4
a	a	k8xC
uvede	uvést	k5eAaPmIp3nS
cenu	cena	k1gFnSc4
P.	P.	kA
</s>
<s>
Kupující	kupující	k1gMnSc1
vygeneruje	vygenerovat	k5eAaPmIp3nS
nový	nový	k2eAgInSc4d1
vlastnický	vlastnický	k2eAgInSc4d1
klíč	klíč	k1gInSc4
<g/>
,	,	kIx,
k	k	k7c3
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Kupující	kupující	k1gMnSc1
provede	provést	k5eAaPmIp3nS
transakci	transakce	k1gFnSc4
se	s	k7c7
dvěma	dva	k4xCgInPc7
vstupy	vstup	k1gInPc7
a	a	k8xC
dvěma	dva	k4xCgInPc7
výstupy	výstup	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc1
vstup	vstup	k1gInSc1
slouží	sloužit	k5eAaImIp3nS
pro	pro	k7c4
podepsání	podepsání	k1gNnSc4
ceny	cena	k1gFnSc2
P.	P.	kA
Druhý	druhý	k4xOgInSc4
vstup	vstup	k1gInSc4
je	být	k5eAaImIp3nS
napojen	napojen	k2eAgMnSc1d1
na	na	k7c4
výstup	výstup	k1gInSc4
držící	držící	k2eAgInSc4d1
T	T	kA
mincí	mince	k1gFnPc2
Bitcoinu	Bitcoin	k1gInSc3
na	na	k7c6
adrese	adresa	k1gFnSc6
vlastníka	vlastník	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc4
výstup	výstup	k1gInSc4
pošle	pošle	k6eAd1
P	P	kA
mincí	mince	k1gFnPc2
Bitcoinu	Bitcoin	k1gInSc3
na	na	k7c6
k	k	k7c3
<g/>
1	#num#	k4
a	a	k8xC
druhý	druhý	k4xOgInSc4
výstup	výstup	k1gInSc4
pošle	pošle	k6eAd1
T	T	kA
mincí	mince	k1gFnPc2
na	na	k7c4
k	k	k7c3
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
transakce	transakce	k1gFnPc4
není	být	k5eNaImIp3nS
validní	validní	k2eAgNnSc1d1
<g/>
,	,	kIx,
protože	protože	k8xS
pouze	pouze	k6eAd1
první	první	k4xOgInSc4
vstup	vstup	k1gInSc4
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
podepsán	podepsat	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kupující	kupující	k1gMnSc1
přenechá	přenechat	k5eAaPmIp3nS
tuto	tento	k3xDgFnSc4
částečně	částečně	k6eAd1
dokončenou	dokončený	k2eAgFnSc4d1
transakci	transakce	k1gFnSc4
prodávajícímu	prodávající	k2eAgMnSc3d1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
pak	pak	k6eAd1
podepíše	podepsat	k5eAaPmIp3nS
druhý	druhý	k4xOgInSc4
vstup	vstup	k1gInSc4
s	s	k7c7
klíčem	klíč	k1gInSc7
současného	současný	k2eAgMnSc2d1
vlastníka	vlastník	k1gMnSc2
auta	auto	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
prodávající	prodávající	k2eAgFnSc1d1
vypustí	vypustit	k5eAaPmIp3nS
transakci	transakce	k1gFnSc4
do	do	k7c2
sítě	síť	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Obě	dva	k4xCgFnPc1
zúčastněné	zúčastněný	k2eAgFnPc1d1
strany	strana	k1gFnPc1
počkají	počkat	k5eAaPmIp3nP
na	na	k7c4
několik	několik	k4yIc4
potvrzení	potvrzení	k1gNnPc2
transakce	transakce	k1gFnSc2
a	a	k8xC
obchod	obchod	k1gInSc1
je	být	k5eAaImIp3nS
zrealizován	zrealizován	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
praxi	praxe	k1gFnSc6
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
výše	vysoce	k6eAd2
uvedený	uvedený	k2eAgInSc1d1
postup	postup	k1gInSc1
zrealizován	zrealizován	k2eAgInSc1d1
za	za	k7c7
pomocí	pomoc	k1gFnSc7
chytrých	chytrý	k2eAgInPc2d1
telefonů	telefon	k1gInPc2
s	s	k7c7
NFC	NFC	kA
hardwarem	hardware	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stačí	stačit	k5eAaBmIp3nS
telefon	telefon	k1gInSc4
přiblížit	přiblížit	k5eAaPmF
k	k	k7c3
přístrojové	přístrojový	k2eAgFnSc3d1
desce	deska	k1gFnSc3
auta	auto	k1gNnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
vyvolá	vyvolat	k5eAaPmIp3nS
spuštění	spuštění	k1gNnSc4
mobilní	mobilní	k2eAgFnSc2d1
aplikace	aplikace	k1gFnSc2
peněženky	peněženka	k1gFnSc2
ve	v	k7c6
speciálním	speciální	k2eAgInSc6d1
módu	mód	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
ví	vědět	k5eAaImIp3nS
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
provádět	provádět	k5eAaImF
obchody	obchod	k1gInPc4
s	s	k7c7
chytrým	chytrý	k2eAgInSc7d1
majetkem	majetek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
stačí	stačit	k5eAaBmIp3nS
na	na	k7c6
telefonu	telefon	k1gInSc6
zadat	zadat	k5eAaPmF
(	(	kIx(
<g/>
potvrdit	potvrdit	k5eAaPmF
<g/>
)	)	kIx)
cenu	cena	k1gFnSc4
a	a	k8xC
kupující	kupující	k1gMnSc1
a	a	k8xC
prodávající	prodávající	k2eAgMnSc1d1
přiblíží	přiblížit	k5eAaPmIp3nS
své	svůj	k3xOyFgInPc4
telefony	telefon	k1gInPc4
k	k	k7c3
sobě	se	k3xPyFc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
dokončili	dokončit	k5eAaPmAgMnP
obchod	obchod	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
I	i	k9
když	když	k8xS
je	on	k3xPp3gFnPc4
kryptografie	kryptografie	k1gFnPc4
komplexní	komplexní	k2eAgInSc1d1
obor	obor	k1gInSc1
<g/>
,	,	kIx,
účastníci	účastník	k1gMnPc1
obchodu	obchod	k1gInSc2
o	o	k7c6
ní	on	k3xPp3gFnSc6
nemusí	muset	k5eNaImIp3nS
nic	nic	k3yNnSc1
vědět	vědět	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Telefon	telefon	k1gInSc1
kupujícího	kupující	k1gMnSc4
pak	pak	k6eAd1
může	moct	k5eAaImIp3nS
plnit	plnit	k5eAaImF
další	další	k2eAgFnSc3d1
funkci	funkce	k1gFnSc3
–	–	k?
např.	např.	kA
startování	startování	k1gNnSc3
auta	auto	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Půjčky	půjčka	k1gFnPc1
a	a	k8xC
zástavy	zástava	k1gFnPc1
</s>
<s>
Možnost	možnost	k1gFnSc1
obchodovat	obchodovat	k5eAaImF
fyzický	fyzický	k2eAgInSc4d1
majetek	majetek	k1gInSc4
bez	bez	k7c2
rizika	riziko	k1gNnSc2
podvodu	podvod	k1gInSc2
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
užitečná	užitečný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
však	však	k9
využít	využít	k5eAaPmF
další	další	k2eAgFnSc4d1
možnost	možnost	k1gFnSc4
<g/>
,	,	kIx,
kterou	který	k3yRgFnSc4,k3yIgFnSc4,k3yQgFnSc4
nám	my	k3xPp1nPc3
kryptografie	kryptografie	k1gFnPc4
nabízí	nabízet	k5eAaImIp3nS
a	a	k8xC
to	ten	k3xDgNnSc4
realizaci	realizace	k1gFnSc4
půjček	půjčka	k1gFnPc2
s	s	k7c7
nízkou	nízký	k2eAgFnSc7d1
úrovní	úroveň	k1gFnSc7
důvěry	důvěra	k1gFnSc2
zúčastněných	zúčastněný	k2eAgFnPc2d1
stran	strana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Např.	např.	kA
žadatel	žadatel	k1gMnSc1
shání	shánět	k5eAaImIp3nS
půjčku	půjčka	k1gFnSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
kterou	který	k3yQgFnSc4,k3yIgFnSc4,k3yRgFnSc4
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
začít	začít	k5eAaPmF
malé	malý	k2eAgNnSc4d1
podnikání	podnikání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Místo	místo	k7c2
požádání	požádání	k1gNnSc2
banky	banka	k1gFnSc2
o	o	k7c4
úvěr	úvěr	k1gInSc4
<g/>
,	,	kIx,
požádá	požádat	k5eAaPmIp3nS
lidi	člověk	k1gMnPc4
z	z	k7c2
celého	celý	k2eAgInSc2d1
světa	svět	k1gInSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
podávali	podávat	k5eAaImAgMnP
nabídky	nabídka	k1gFnPc4
na	na	k7c4
jeho	jeho	k3xOp3gFnSc4
půjčku	půjčka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgNnSc7
pravděpodobně	pravděpodobně	k6eAd1
získá	získat	k5eAaPmIp3nS
nejlepší	dobrý	k2eAgInSc1d3
možný	možný	k2eAgInSc1d1
úrok	úrok	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
toto	tento	k3xDgNnSc1
fungovalo	fungovat	k5eAaImAgNnS
<g/>
,	,	kIx,
věřitelé	věřitel	k1gMnPc1
potřebují	potřebovat	k5eAaImIp3nP
vědět	vědět	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
když	když	k8xS
nebude	být	k5eNaImBp3nS
půjčka	půjčka	k1gFnSc1
splacena	splatit	k5eAaPmNgFnS
<g/>
,	,	kIx,
získají	získat	k5eAaPmIp3nP
od	od	k7c2
dlužníka	dlužník	k1gMnSc2
zástavu	zástav	k1gInSc2
–	–	k?
např.	např.	kA
auto	auto	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Toto	tento	k3xDgNnSc1
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
realizovat	realizovat	k5eAaBmF
přidáním	přidání	k1gNnSc7
přístupových	přístupový	k2eAgInPc2d1
klíčů	klíč	k1gInPc2
ke	k	k7c3
klíči	klíč	k1gInSc3
vlastníka	vlastník	k1gMnSc4
auta	auto	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přístupové	přístupový	k2eAgInPc1d1
klíče	klíč	k1gInPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
přidány	přidán	k2eAgFnPc1d1
či	či	k8xC
odebrány	odebrán	k2eAgFnPc1d1
tak	tak	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
vlastník	vlastník	k1gMnSc1
auta	auto	k1gNnSc2
podepíše	podepsat	k5eAaPmIp3nS
svým	svůj	k3xOyFgInSc7
vlastnickým	vlastnický	k2eAgInSc7d1
klíčem	klíč	k1gInSc7
zprávu	zpráva	k1gFnSc4
(	(	kIx(
<g/>
transakci	transakce	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přístupové	přístupový	k2eAgInPc1d1
klíče	klíč	k1gInPc1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
dočasné	dočasný	k2eAgNnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c4
dobu	doba	k1gFnSc4
trvání	trvání	k1gNnSc2
půjčky	půjčka	k1gFnSc2
se	se	k3xPyFc4
změní	změnit	k5eAaPmIp3nS
vlastnictví	vlastnictví	k1gNnSc2
auta	auto	k1gNnSc2
na	na	k7c4
věřitele	věřitel	k1gMnPc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
přístupové	přístupový	k2eAgInPc4d1
klíče	klíč	k1gInPc4
bude	být	k5eAaImBp3nS
mít	mít	k5eAaImF
stále	stále	k6eAd1
původní	původní	k2eAgMnSc1d1
vlastník	vlastník	k1gMnSc1
(	(	kIx(
<g/>
dlužník	dlužník	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
úspěšném	úspěšný	k2eAgNnSc6d1
splacení	splacení	k1gNnSc6
půjčky	půjčka	k1gFnSc2
je	být	k5eAaImIp3nS
vlastnictví	vlastnictví	k1gNnSc1
auta	auto	k1gNnSc2
zpátky	zpátky	k6eAd1
převedeno	převeden	k2eAgNnSc1d1
z	z	k7c2
věřitele	věřitel	k1gMnSc2
na	na	k7c4
původního	původní	k2eAgMnSc4d1
majitele	majitel	k1gMnSc4
(	(	kIx(
<g/>
dlužníka	dlužník	k1gMnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přístupovým	přístupový	k2eAgInPc3d1
klíčům	klíč	k1gInPc3
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
přiřadit	přiřadit	k5eAaPmF
časové	časový	k2eAgNnSc4d1
omezení	omezení	k1gNnSc4
<g/>
,	,	kIx,
takže	takže	k8xS
pokud	pokud	k8xS
dlužník	dlužník	k1gMnSc1
nesplácí	splácet	k5eNaImIp3nS
půjčku	půjčka	k1gFnSc4
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gInPc1
přístupové	přístupový	k2eAgInPc1d1
klíče	klíč	k1gInPc1
přestanou	přestat	k5eAaPmIp3nP
fungovat	fungovat	k5eAaImF
a	a	k8xC
není	být	k5eNaImIp3nS
možné	možný	k2eAgNnSc1d1
s	s	k7c7
nimi	on	k3xPp3gMnPc7
nastartovat	nastartovat	k5eAaPmF
auto	auto	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	Nový	k1gMnSc1
vlastník	vlastník	k1gMnSc1
nyní	nyní	k6eAd1
může	moct	k5eAaImIp3nS
přijít	přijít	k5eAaPmF
k	k	k7c3
autu	aut	k1gInSc3
a	a	k8xC
bez	bez	k7c2
problémů	problém	k1gInPc2
s	s	k7c7
ním	on	k3xPp3gInSc7
odjet	odjet	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
však	však	k9
chce	chtít	k5eAaImIp3nS
<g/>
,	,	kIx,
(	(	kIx(
<g/>
pochází	pocházet	k5eAaImIp3nS
např.	např.	kA
z	z	k7c2
jiné	jiný	k2eAgFnSc2d1
země	zem	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
auto	auto	k1gNnSc1
prodat	prodat	k5eAaPmF
užitím	užití	k1gNnSc7
prodejního	prodejní	k2eAgInSc2d1
protokolu	protokol	k1gInSc2
(	(	kIx(
<g/>
je	být	k5eAaImIp3nS
popsán	popsat	k5eAaPmNgInS
v	v	k7c6
odstavci	odstavec	k1gInSc6
Teorie	teorie	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
získat	získat	k5eAaPmF
peníze	peníz	k1gInPc4
za	za	k7c4
prodej	prodej	k1gInSc4
auta	auto	k1gNnSc2
bez	bez	k7c2
osobní	osobní	k2eAgFnSc2d1
návštěvy	návštěva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Ethereum	Ethereum	k1gInSc1
</s>
<s>
Kryptoměna	Kryptoměn	k2eAgFnSc1d1
</s>
<s>
Blockchain	Blockchain	k1gMnSc1
</s>
<s>
Chytrý	chytrý	k2eAgInSc1d1
kontrakt	kontrakt	k1gInSc1
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Smart	Smart	k1gInSc1
property	propert	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Smart	Smart	k1gInSc4
Property	Propert	k1gInPc7
<g/>
,	,	kIx,
Colored	Colored	k1gMnSc1
Coins	Coinsa	k1gFnPc2
and	and	k?
Mastercoin	Mastercoin	k1gMnSc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Smart	Smart	k1gInSc1
Property	Propert	k1gInPc1
in	in	k?
Action	Action	k1gInSc1
</s>
