<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
mezi	mezi	k7c7	mezi
první	první	k4xOgFnSc7	první
a	a	k8xC	a
druhou	druhý	k4xOgFnSc7	druhý
punskou	punský	k2eAgFnSc7d1	punská
válkou	válka	k1gFnSc7	válka
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
kartáginský	kartáginský	k2eAgMnSc1d1	kartáginský
velitel	velitel	k1gMnSc1	velitel
Hasdrubal	Hasdrubal	k1gMnSc1	Hasdrubal
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Římany	Říman	k1gMnPc7	Říman
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
zavázal	zavázat	k5eAaPmAgMnS	zavázat
nerozšiřovat	rozšiřovat	k5eNaImF	rozšiřovat
punskou	punský	k2eAgFnSc4d1	punská
říši	říše	k1gFnSc4	říše
v	v	k7c6	v
Ibérii	Ibérie	k1gFnSc6	Ibérie
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Ebro	Ebro	k1gNnSc1	Ebro
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
řeka	řeka	k1gFnSc1	řeka
stala	stát	k5eAaPmAgFnS	stát
hranicí	hranice	k1gFnSc7	hranice
kartáginské	kartáginský	k2eAgFnSc2d1	kartáginská
sféry	sféra	k1gFnSc2	sféra
vlivu	vliv	k1gInSc2	vliv
na	na	k7c6	na
Pyrenejském	pyrenejský	k2eAgInSc6d1	pyrenejský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Předchozí	předchozí	k2eAgInSc1d1	předchozí
vývoj	vývoj	k1gInSc1	vývoj
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
porážce	porážka	k1gFnSc6	porážka
v	v	k7c6	v
první	první	k4xOgFnSc6	první
punské	punský	k2eAgFnSc6d1	punská
válce	válka	k1gFnSc6	válka
ztratili	ztratit	k5eAaPmAgMnP	ztratit
Kartáginci	Kartáginec	k1gMnPc1	Kartáginec
území	území	k1gNnSc2	území
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
a	a	k8xC	a
Sardinii	Sardinie	k1gFnSc6	Sardinie
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgFnPc2	jenž
se	se	k3xPyFc4	se
zmocnili	zmocnit	k5eAaPmAgMnP	zmocnit
vítězní	vítězný	k2eAgMnPc1d1	vítězný
Římané	Říman	k1gMnPc1	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Punský	punský	k2eAgMnSc1d1	punský
velitel	velitel	k1gMnSc1	velitel
Hamilkar	Hamilkara	k1gFnPc2	Hamilkara
Barkas	barkasa	k1gFnPc2	barkasa
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
237	[number]	k4	237
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
vylodil	vylodit	k5eAaPmAgMnS	vylodit
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
Pyrenejského	pyrenejský	k2eAgInSc2d1	pyrenejský
poloostrova	poloostrov	k1gInSc2	poloostrov
a	a	k8xC	a
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
řeky	řeka	k1gFnSc2	řeka
Baetis	Baetis	k1gFnSc2	Baetis
(	(	kIx(	(
<g/>
Guadalquivir	Guadalquivir	k1gMnSc1	Guadalquivir
<g/>
)	)	kIx)	)
zahájil	zahájit	k5eAaPmAgMnS	zahájit
výboje	výboj	k1gInPc4	výboj
vedoucí	vedoucí	k2eAgInPc4d1	vedoucí
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
nové	nový	k2eAgFnSc2d1	nová
kartáginské	kartáginský	k2eAgFnSc2d1	kartáginská
zámořské	zámořský	k2eAgFnSc2d1	zámořská
domény	doména	k1gFnSc2	doména
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Hamilkarově	Hamilkarův	k2eAgFnSc6d1	Hamilkarův
smrti	smrt	k1gFnSc6	smrt
převzal	převzít	k5eAaPmAgInS	převzít
velení	velení	k1gNnSc4	velení
punských	punský	k2eAgFnPc2d1	punská
sil	síla	k1gFnPc2	síla
v	v	k7c6	v
Ibérii	Ibérie	k1gFnSc6	Ibérie
jeho	jeho	k3xOp3gMnSc1	jeho
zeť	zeť	k1gMnSc1	zeť
Hasdrubal	Hasdrubal	k1gMnSc1	Hasdrubal
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
v	v	k7c6	v
Hamilkarově	Hamilkarův	k2eAgNnSc6d1	Hamilkarův
díle	dílo	k1gNnSc6	dílo
a	a	k8xC	a
vojenskými	vojenský	k2eAgInPc7d1	vojenský
a	a	k8xC	a
diplomatickými	diplomatický	k2eAgInPc7d1	diplomatický
prostředky	prostředek	k1gInPc7	prostředek
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
a	a	k8xC	a
upevnil	upevnit	k5eAaPmAgInS	upevnit
kartáginskou	kartáginský	k2eAgFnSc4d1	kartáginská
moc	moc	k1gFnSc4	moc
na	na	k7c6	na
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihovýchodním	jihovýchodní	k2eAgNnSc6d1	jihovýchodní
pobřeží	pobřeží	k1gNnSc6	pobřeží
založil	založit	k5eAaPmAgInS	založit
město	město	k1gNnSc4	město
nazývané	nazývaný	k2eAgFnSc2d1	nazývaná
Římany	Říman	k1gMnPc4	Říman
Nové	Nové	k2eAgNnSc1d1	Nové
Kartágo	Kartágo	k1gNnSc1	Kartágo
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc1d1	dnešní
Cartagena	Cartagena	k1gFnSc1	Cartagena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Galové	Galové	k2eAgFnSc6d1	Galové
obývající	obývající	k2eAgFnSc6d1	obývající
severní	severní	k2eAgFnSc6d1	severní
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
území	území	k1gNnSc6	území
známé	známá	k1gFnSc2	známá
tehdy	tehdy	k6eAd1	tehdy
jako	jako	k8xS	jako
Předalpská	předalpský	k2eAgFnSc1d1	Předalpská
Galie	Galie	k1gFnSc1	Galie
<g/>
,	,	kIx,	,
začali	začít	k5eAaPmAgMnP	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
226	[number]	k4	226
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
vážně	vážně	k6eAd1	vážně
ohrožovat	ohrožovat	k5eAaImF	ohrožovat
římskou	římský	k2eAgFnSc4d1	římská
republiku	republika	k1gFnSc4	republika
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
připravovali	připravovat	k5eAaImAgMnP	připravovat
k	k	k7c3	k
mohutnému	mohutný	k2eAgInSc3d1	mohutný
vpádu	vpád	k1gInSc3	vpád
na	na	k7c4	na
Apeninský	apeninský	k2eAgInSc4d1	apeninský
poloostrov	poloostrov	k1gInSc4	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
shromáždili	shromáždit	k5eAaPmAgMnP	shromáždit
Římané	Říman	k1gMnPc1	Říman
značné	značný	k2eAgFnSc2d1	značná
vojenské	vojenský	k2eAgFnSc2d1	vojenská
síly	síla	k1gFnSc2	síla
k	k	k7c3	k
odražení	odražení	k1gNnSc3	odražení
Galů	Gal	k1gMnPc2	Gal
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
však	však	k9	však
nezanedbatelnou	zanedbatelný	k2eNgFnSc4d1	nezanedbatelná
část	část	k1gFnSc4	část
odeslali	odeslat	k5eAaPmAgMnP	odeslat
do	do	k7c2	do
Tarenta	Tarento	k1gNnSc2	Tarento
a	a	k8xC	a
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
<g/>
.	.	kIx.	.
</s>
<s>
Jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
konzulů	konzul	k1gMnPc2	konzul
vyslali	vyslat	k5eAaPmAgMnP	vyslat
s	s	k7c7	s
vojskem	vojsko	k1gNnSc7	vojsko
dokonce	dokonce	k9	dokonce
na	na	k7c6	na
Sardinii	Sardinie	k1gFnSc6	Sardinie
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
o	o	k7c4	o
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
dříve	dříve	k6eAd2	dříve
využili	využít	k5eAaPmAgMnP	využít
obdobných	obdobný	k2eAgFnPc2d1	obdobná
nesnází	nesnáz	k1gFnPc2	nesnáz
Kartáginců	Kartáginec	k1gInPc2	Kartáginec
<g/>
,	,	kIx,	,
způsobených	způsobený	k2eAgFnPc2d1	způsobená
žoldnéřskou	žoldnéřský	k2eAgFnSc7d1	žoldnéřská
válkou	válka	k1gFnSc7	válka
<g/>
,	,	kIx,	,
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
ostrov	ostrov	k1gInSc1	ostrov
jim	on	k3xPp3gMnPc3	on
odňali	odnít	k5eAaPmAgMnP	odnít
<g/>
.	.	kIx.	.
</s>
<s>
Obávali	obávat	k5eAaImAgMnP	obávat
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
Kartáginci	Kartáginec	k1gMnPc1	Kartáginec
nechopili	chopit	k5eNaPmAgMnP	chopit
galské	galský	k2eAgFnPc4d1	galská
krize	krize	k1gFnPc4	krize
jako	jako	k8xS	jako
příležitosti	příležitost	k1gFnSc2	příležitost
k	k	k7c3	k
opětovnému	opětovný	k2eAgNnSc3d1	opětovné
získání	získání	k1gNnSc3	získání
Sardinie	Sardinie	k1gFnSc2	Sardinie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sjednání	sjednání	k1gNnSc1	sjednání
smlouvy	smlouva	k1gFnSc2	smlouva
a	a	k8xC	a
její	její	k3xOp3gInSc1	její
obsah	obsah	k1gInSc1	obsah
==	==	k?	==
</s>
</p>
<p>
<s>
Souběžně	souběžně	k6eAd1	souběžně
s	s	k7c7	s
preventivními	preventivní	k2eAgMnPc7d1	preventivní
vojenskými	vojenský	k2eAgMnPc7d1	vojenský
opatřeními	opatření	k1gNnPc7	opatření
vyslali	vyslat	k5eAaPmAgMnP	vyslat
Římané	Říman	k1gMnPc1	Říman
poselstvo	poselstvo	k1gNnSc4	poselstvo
do	do	k7c2	do
Ibérie	Ibérie	k1gFnSc2	Ibérie
a	a	k8xC	a
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Hasdrubalem	Hasdrubal	k1gInSc7	Hasdrubal
<g/>
.	.	kIx.	.
</s>
<s>
Polybios	Polybios	k1gMnSc1	Polybios
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
znepokojoval	znepokojovat	k5eAaImAgMnS	znepokojovat
růst	růst	k1gInSc4	růst
jeho	jeho	k3xOp3gFnSc2	jeho
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
předpokládali	předpokládat	k5eAaImAgMnP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
budoucnu	budoucno	k1gNnSc6	budoucno
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
válce	válka	k1gFnSc3	válka
s	s	k7c7	s
Kartágem	Kartágo	k1gNnSc7	Kartágo
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
právě	právě	k6eAd1	právě
svírali	svírat	k5eAaImAgMnP	svírat
Galové	Gal	k1gMnPc1	Gal
<g/>
,	,	kIx,	,
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
s	s	k7c7	s
Hasdrubalem	Hasdrubal	k1gInSc7	Hasdrubal
přátelsky	přátelsky	k6eAd1	přátelsky
a	a	k8xC	a
doslova	doslova	k6eAd1	doslova
ho	on	k3xPp3gNnSc4	on
"	"	kIx"	"
<g/>
uchlácholit	uchlácholit	k5eAaPmF	uchlácholit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
mohli	moct	k5eAaImAgMnP	moct
nejprve	nejprve	k6eAd1	nejprve
vypořádat	vypořádat	k5eAaPmF	vypořádat
s	s	k7c7	s
bezprostředním	bezprostřední	k2eAgNnSc7d1	bezprostřední
ohrožením	ohrožení	k1gNnSc7	ohrožení
<g/>
.	.	kIx.	.
<g/>
Smlouva	smlouva	k1gFnSc1	smlouva
obsahovala	obsahovat	k5eAaImAgFnS	obsahovat
ujednání	ujednání	k1gNnSc4	ujednání
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Kartáginci	Kartáginec	k1gMnPc1	Kartáginec
nesmějí	smát	k5eNaImIp3nP	smát
s	s	k7c7	s
válečnými	válečný	k2eAgInPc7d1	válečný
úmysly	úmysl	k1gInPc7	úmysl
překročit	překročit	k5eAaPmF	překročit
řeku	řeka	k1gFnSc4	řeka
Ibér	Ibéra	k1gFnPc2	Ibéra
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Třebaže	třebaže	k8xS	třebaže
se	se	k3xPyFc4	se
dosud	dosud	k6eAd1	dosud
vedou	vést	k5eAaImIp3nP	vést
diskuse	diskuse	k1gFnPc1	diskuse
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
o	o	k7c4	o
jakou	jaký	k3yQgFnSc4	jaký
řeku	řeka	k1gFnSc4	řeka
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
<g/>
,	,	kIx,	,
považuje	považovat	k5eAaImIp3nS	považovat
se	se	k3xPyFc4	se
za	za	k7c4	za
téměř	téměř	k6eAd1	téměř
jisté	jistý	k2eAgNnSc1d1	jisté
<g/>
,	,	kIx,	,
že	že	k8xS	že
touto	tento	k3xDgFnSc7	tento
řekou	řeka	k1gFnSc7	řeka
bylo	být	k5eAaImAgNnS	být
myšleno	myslet	k5eAaImNgNnS	myslet
Ebro	Ebro	k1gNnSc1	Ebro
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
území	území	k1gNnSc4	území
ovládané	ovládaný	k2eAgNnSc4d1	ovládané
tehdy	tehdy	k6eAd1	tehdy
Hasdrubalem	Hasdrubal	k1gInSc7	Hasdrubal
sahalo	sahat	k5eAaImAgNnS	sahat
zhruba	zhruba	k6eAd1	zhruba
k	k	k7c3	k
řece	řeka	k1gFnSc3	řeka
Tagus	Tagus	k1gMnSc1	Tagus
(	(	kIx(	(
<g/>
Tajo	Tajo	k1gMnSc1	Tajo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
šlo	jít	k5eAaImAgNnS	jít
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Římanů	Říman	k1gMnPc2	Říman
o	o	k7c4	o
zjevný	zjevný	k2eAgInSc4d1	zjevný
ústupek	ústupek	k1gInSc4	ústupek
<g/>
.	.	kIx.	.
</s>
<s>
Kartáginskému	kartáginský	k2eAgMnSc3d1	kartáginský
veliteli	velitel	k1gMnPc7	velitel
ponechali	ponechat	k5eAaPmAgMnP	ponechat
totiž	totiž	k9	totiž
volnou	volný	k2eAgFnSc4d1	volná
ruku	ruka	k1gFnSc4	ruka
v	v	k7c6	v
takřka	takřka	k6eAd1	takřka
celé	celý	k2eAgFnSc6d1	celá
Ibérii	Ibérie	k1gFnSc6	Ibérie
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
severovýchodní	severovýchodní	k2eAgFnSc2d1	severovýchodní
části	část	k1gFnSc2	část
země	zem	k1gFnSc2	zem
a	a	k8xC	a
fakticky	fakticky	k6eAd1	fakticky
ho	on	k3xPp3gMnSc4	on
uznali	uznat	k5eAaPmAgMnP	uznat
za	za	k7c4	za
tamějšího	tamější	k2eAgMnSc4d1	tamější
vládce	vládce	k1gMnSc4	vládce
<g/>
.	.	kIx.	.
</s>
<s>
Výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
předešli	předejít	k5eAaPmAgMnP	předejít
riziku	riziko	k1gNnSc3	riziko
koordinovaného	koordinovaný	k2eAgInSc2d1	koordinovaný
útoku	útok	k1gInSc2	útok
Galů	Gal	k1gMnPc2	Gal
a	a	k8xC	a
Punů	Pun	k1gMnPc2	Pun
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Římané	Říman	k1gMnPc1	Říman
vymezili	vymezit	k5eAaPmAgMnP	vymezit
punskou	punský	k2eAgFnSc4d1	punská
sféru	sféra	k1gFnSc4	sféra
zájmů	zájem	k1gInPc2	zájem
řekou	řeka	k1gFnSc7	řeka
Ebro	Ebro	k6eAd1	Ebro
a	a	k8xC	a
nikoli	nikoli	k9	nikoli
pohořím	pohořet	k5eAaPmIp1nS	pohořet
Pyreneje	Pyreneje	k1gFnPc4	Pyreneje
<g/>
,	,	kIx,	,
tvořícím	tvořící	k2eAgMnSc7d1	tvořící
přirozenou	přirozený	k2eAgFnSc4d1	přirozená
hranici	hranice	k1gFnSc4	hranice
Ibérie	Ibérie	k1gFnSc2	Ibérie
<g/>
,	,	kIx,	,
zabránili	zabránit	k5eAaPmAgMnP	zabránit
navíc	navíc	k6eAd1	navíc
možnosti	možnost	k1gFnSc2	možnost
přímého	přímý	k2eAgInSc2d1	přímý
kontaktu	kontakt	k1gInSc2	kontakt
Hasdrubala	Hasdrubala	k1gMnPc2	Hasdrubala
s	s	k7c7	s
Galy	Gal	k1gMnPc7	Gal
<g/>
.	.	kIx.	.
<g/>
Smlouva	smlouva	k1gFnSc1	smlouva
byla	být	k5eAaImAgFnS	být
uzavřena	uzavřít	k5eAaPmNgFnS	uzavřít
patrně	patrně	k6eAd1	patrně
před	před	k7c7	před
vyvrcholením	vyvrcholení	k1gNnSc7	vyvrcholení
galské	galský	k2eAgFnSc2d1	galská
invaze	invaze	k1gFnSc2	invaze
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
znamená	znamenat	k5eAaImIp3nS	znamenat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
225	[number]	k4	225
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Ačkoli	ačkoli	k8xS	ačkoli
nelze	lze	k6eNd1	lze
vyloučit	vyloučit	k5eAaPmF	vyloučit
<g/>
,	,	kIx,	,
že	že	k8xS	že
sjednání	sjednání	k1gNnSc1	sjednání
smlouvy	smlouva	k1gFnSc2	smlouva
nastalo	nastat	k5eAaPmAgNnS	nastat
o	o	k7c4	o
rok	rok	k1gInSc4	rok
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
současní	současný	k2eAgMnPc1d1	současný
badatelé	badatel	k1gMnPc1	badatel
argumentují	argumentovat	k5eAaImIp3nP	argumentovat
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
pozdějšího	pozdní	k2eAgNnSc2d2	pozdější
data	datum	k1gNnSc2	datum
<g/>
.	.	kIx.	.
</s>
<s>
Poukazují	poukazovat	k5eAaImIp3nP	poukazovat
přitom	přitom	k6eAd1	přitom
na	na	k7c4	na
skutečnost	skutečnost	k1gFnSc4	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
krátce	krátce	k6eAd1	krátce
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
legie	legie	k1gFnPc1	legie
dorazily	dorazit	k5eAaPmAgFnP	dorazit
na	na	k7c6	na
Sardinii	Sardinie	k1gFnSc6	Sardinie
a	a	k8xC	a
Sicílii	Sicílie	k1gFnSc6	Sicílie
<g/>
,	,	kIx,	,
Římané	Říman	k1gMnPc1	Říman
je	on	k3xPp3gInPc4	on
opět	opět	k6eAd1	opět
odvolali	odvolat	k5eAaPmAgMnP	odvolat
<g/>
,	,	kIx,	,
k	k	k7c3	k
čemuž	což	k3yQnSc3	což
došlo	dojít	k5eAaPmAgNnS	dojít
zřejmě	zřejmě	k6eAd1	zřejmě
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
uzavření	uzavření	k1gNnSc4	uzavření
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
</s>
<s>
Kdyby	kdyby	kYmCp3nP	kdyby
smlouvu	smlouva	k1gFnSc4	smlouva
uzavřeli	uzavřít	k5eAaPmAgMnP	uzavřít
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
226	[number]	k4	226
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
nemuseli	muset	k5eNaImAgMnP	muset
by	by	kYmCp3nP	by
chránit	chránit	k5eAaImF	chránit
ostrovy	ostrov	k1gInPc4	ostrov
před	před	k7c7	před
případným	případný	k2eAgInSc7d1	případný
kartáginským	kartáginský	k2eAgInSc7d1	kartáginský
útokem	útok	k1gInSc7	útok
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sporná	sporný	k2eAgNnPc1d1	sporné
ujednání	ujednání	k1gNnPc1	ujednání
==	==	k?	==
</s>
</p>
<p>
<s>
Polybios	Polybios	k1gInSc1	Polybios
výslovně	výslovně	k6eAd1	výslovně
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS	zdůrazňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
o	o	k7c6	o
ostatní	ostatní	k2eAgFnSc6d1	ostatní
Ibérii	Ibérie	k1gFnSc6	Ibérie
se	se	k3xPyFc4	se
ve	v	k7c6	v
smlouvě	smlouva	k1gFnSc6	smlouva
nehovořilo	hovořit	k5eNaImAgNnS	hovořit
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgMnPc1d2	pozdější
autoři	autor	k1gMnPc1	autor
nicméně	nicméně	k8xC	nicméně
zmiňují	zmiňovat	k5eAaImIp3nP	zmiňovat
i	i	k9	i
jiná	jiný	k2eAgNnPc1d1	jiné
ustanovení	ustanovení	k1gNnPc1	ustanovení
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
její	její	k3xOp3gFnSc7	její
součástí	součást	k1gFnSc7	součást
<g/>
.	.	kIx.	.
</s>
<s>
Livius	Livius	k1gMnSc1	Livius
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
hranicí	hranice	k1gFnSc7	hranice
obou	dva	k4xCgInPc2	dva
územních	územní	k2eAgInPc2d1	územní
celků	celek	k1gInPc2	celek
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
řeka	řeka	k1gFnSc1	řeka
Hiberus	Hiberus	k1gInSc4	Hiberus
a	a	k8xC	a
Sagunťanům	Sagunťan	k1gMnPc3	Sagunťan
<g/>
,	,	kIx,	,
sídlícím	sídlící	k2eAgNnSc7d1	sídlící
mezi	mezi	k7c7	mezi
správními	správní	k2eAgMnPc7d1	správní
celky	celek	k1gInPc4	celek
obou	dva	k4xCgInPc2	dva
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
bude	být	k5eAaImBp3nS	být
ponechána	ponechat	k5eAaPmNgFnS	ponechat
svoboda	svoboda	k1gFnSc1	svoboda
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Appiános	Appiános	k1gInSc1	Appiános
udává	udávat	k5eAaImIp3nS	udávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
Sagunťané	Sagunťan	k1gMnPc1	Sagunťan
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
Řekové	Řek	k1gMnPc1	Řek
v	v	k7c6	v
Hispánii	Hispánie	k1gFnSc6	Hispánie
budou	být	k5eAaImBp3nP	být
samostatní	samostatný	k2eAgMnPc1d1	samostatný
a	a	k8xC	a
svobodní	svobodný	k2eAgMnPc1d1	svobodný
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Zároveň	zároveň	k6eAd1	zároveň
nesprávně	správně	k6eNd1	správně
určuje	určovat	k5eAaImIp3nS	určovat
polohu	poloha	k1gFnSc4	poloha
města	město	k1gNnSc2	město
Saguntum	Saguntum	k1gNnSc4	Saguntum
a	a	k8xC	a
umisťuje	umisťovat	k5eAaImIp3nS	umisťovat
ho	on	k3xPp3gMnSc4	on
mezi	mezi	k7c4	mezi
Pyreneje	Pyreneje	k1gFnPc4	Pyreneje
a	a	k8xC	a
Ebro	Ebro	k6eAd1	Ebro
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
severně	severně	k6eAd1	severně
od	od	k7c2	od
této	tento	k3xDgFnSc2	tento
řeky	řeka	k1gFnSc2	řeka
<g/>
.	.	kIx.	.
</s>
<s>
Existence	existence	k1gFnSc1	existence
těchto	tento	k3xDgNnPc2	tento
ustanovení	ustanovení	k1gNnPc2	ustanovení
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pasáží	pasáž	k1gFnPc2	pasáž
týkajících	týkající	k2eAgFnPc2d1	týkající
se	se	k3xPyFc4	se
Sagunta	Sagunt	k1gInSc2	Sagunt
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
sporná	sporný	k2eAgFnSc1d1	sporná
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
nápadně	nápadně	k6eAd1	nápadně
odpovídají	odpovídat	k5eAaImIp3nP	odpovídat
římské	římský	k2eAgFnSc3d1	římská
propagandě	propaganda	k1gFnSc3	propaganda
obviňující	obviňující	k2eAgFnSc6d1	obviňující
Kartágince	Kartáginka	k1gFnSc6	Kartáginka
z	z	k7c2	z
porušení	porušení	k1gNnSc2	porušení
smlouvy	smlouva	k1gFnSc2	smlouva
<g/>
.	.	kIx.	.
<g/>
Saguntum	Saguntum	k1gNnSc1	Saguntum
se	se	k3xPyFc4	se
nacházelo	nacházet	k5eAaImAgNnS	nacházet
na	na	k7c6	na
východním	východní	k2eAgNnSc6d1	východní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Iberského	iberský	k2eAgInSc2d1	iberský
poloostrova	poloostrov	k1gInSc2	poloostrov
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Ebro	Ebro	k6eAd1	Ebro
a	a	k8xC	a
spadalo	spadat	k5eAaPmAgNnS	spadat
do	do	k7c2	do
kartáginské	kartáginský	k2eAgFnSc2d1	kartáginská
sféry	sféra	k1gFnSc2	sféra
vlivu	vliv	k1gInSc2	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Polybiova	Polybiův	k2eAgNnSc2d1	Polybiův
tvrzení	tvrzení	k1gNnSc2	tvrzení
vstoupili	vstoupit	k5eAaPmAgMnP	vstoupit
Sagunťané	Sagunťan	k1gMnPc1	Sagunťan
mnoho	mnoho	k4c4	mnoho
let	léto	k1gNnPc2	léto
před	před	k7c7	před
nástupem	nástup	k1gInSc7	nástup
Hannibala	Hannibal	k1gMnSc2	Hannibal
pod	pod	k7c4	pod
ochranu	ochrana	k1gFnSc4	ochrana
Římanů	Říman	k1gMnPc2	Říman
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
chtěli	chtít	k5eAaImAgMnP	chtít
předejít	předejít	k5eAaPmF	předejít
ovládnutí	ovládnutý	k2eAgMnPc1d1	ovládnutý
Kartáginci	Kartáginec	k1gMnPc1	Kartáginec
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
stalo	stát	k5eAaPmAgNnS	stát
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Římané	Říman	k1gMnPc1	Říman
vyslali	vyslat	k5eAaPmAgMnP	vyslat
emisary	emisar	k1gMnPc4	emisar
k	k	k7c3	k
Hasdrubalovi	Hasdrubal	k1gMnSc3	Hasdrubal
<g/>
.	.	kIx.	.
</s>
<s>
Přesná	přesný	k2eAgFnSc1d1	přesná
povaha	povaha	k1gFnSc1	povaha
vztahů	vztah	k1gInPc2	vztah
mezi	mezi	k7c7	mezi
Římem	Řím	k1gInSc7	Řím
a	a	k8xC	a
Saguntem	Sagunt	k1gInSc7	Sagunt
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
přesto	přesto	k8xC	přesto
nejasná	jasný	k2eNgFnSc1d1	nejasná
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
všeho	všecek	k3xTgNnSc2	všecek
se	se	k3xPyFc4	se
nejednalo	jednat	k5eNaImAgNnS	jednat
o	o	k7c4	o
spojenecký	spojenecký	k2eAgInSc4d1	spojenecký
svazek	svazek	k1gInSc4	svazek
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
Sagunťané	Sagunťan	k1gMnPc1	Sagunťan
udržovali	udržovat	k5eAaImAgMnP	udržovat
s	s	k7c7	s
Římany	Říman	k1gMnPc4	Říman
přátelské	přátelský	k2eAgInPc1d1	přátelský
styky	styk	k1gInPc1	styk
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Hannibal	Hannibal	k1gInSc1	Hannibal
v	v	k7c6	v
roce	rok	k1gInSc6	rok
219	[number]	k4	219
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Saguntum	Saguntum	k1gNnSc4	Saguntum
oblehl	oblehnout	k5eAaPmAgMnS	oblehnout
<g/>
,	,	kIx,	,
neučinili	učinit	k5eNaPmAgMnP	učinit
Římané	Říman	k1gMnPc1	Říman
nic	nic	k3yNnSc4	nic
pro	pro	k7c4	pro
záchranu	záchrana	k1gFnSc4	záchrana
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
třebaže	třebaže	k8xS	třebaže
jeho	jeho	k3xOp3gInSc1	jeho
pád	pád	k1gInSc1	pád
jim	on	k3xPp3gMnPc3	on
následně	následně	k6eAd1	následně
posloužil	posloužit	k5eAaPmAgMnS	posloužit
jako	jako	k8xC	jako
záminka	záminka	k1gFnSc1	záminka
k	k	k7c3	k
vyhlášení	vyhlášení	k1gNnSc3	vyhlášení
druhé	druhý	k4xOgFnSc2	druhý
punské	punský	k2eAgFnSc2d1	punská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Obléhání	obléhání	k1gNnSc1	obléhání
Sagunta	Sagunt	k1gInSc2	Sagunt
</s>
</p>
<p>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
punská	punský	k2eAgFnSc1d1	punská
válka	válka	k1gFnSc1	válka
</s>
</p>
<p>
<s>
==	==	k?	==
Poznámky	poznámka	k1gFnSc2	poznámka
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Prameny	pramen	k1gInPc1	pramen
===	===	k?	===
</s>
</p>
<p>
<s>
APPIÁNOS	APPIÁNOS	kA	APPIÁNOS
<g/>
.	.	kIx.	.
</s>
<s>
Zrod	zrod	k1gInSc1	zrod
římského	římský	k2eAgNnSc2d1	římské
impéria	impérium	k1gNnSc2	impérium
<g/>
:	:	kIx,	:
římské	římský	k2eAgFnPc1d1	římská
dějiny	dějiny	k1gFnPc1	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
</s>
</p>
<p>
<s>
LIVIUS	LIVIUS	kA	LIVIUS
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
IV	Iva	k1gFnPc2	Iva
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1973	[number]	k4	1973
<g/>
,	,	kIx,	,
1975	[number]	k4	1975
</s>
</p>
<p>
<s>
POLYBIOS	POLYBIOS	kA	POLYBIOS
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
I	I	kA	I
<g/>
,	,	kIx,	,
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Arista	Arista	k1gMnSc1	Arista
<g/>
,	,	kIx,	,
Baset	Baset	k1gMnSc1	Baset
<g/>
,	,	kIx,	,
Maitrea	Maitrea	k1gMnSc1	Maitrea
<g/>
,	,	kIx,	,
TeMi	TeM	k1gMnPc1	TeM
CZ	CZ	kA	CZ
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-80-86410-56-2	[number]	k4	978-80-86410-56-2
ISBN	ISBN	kA	ISBN
978-80-86410-60-9	[number]	k4	978-80-86410-60-9
</s>
</p>
<p>
<s>
===	===	k?	===
Bibliografie	bibliografie	k1gFnSc2	bibliografie
===	===	k?	===
</s>
</p>
<p>
<s>
HOYOS	HOYOS	kA	HOYOS
<g/>
,	,	kIx,	,
Dexter	Dextra	k1gFnPc2	Dextra
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k8xC	a
Companion	Companion	k1gInSc1	Companion
to	ten	k3xDgNnSc4	ten
the	the	k?	the
Punic	Punice	k1gInPc2	Punice
Wars	Warsa	k1gFnPc2	Warsa
<g/>
.	.	kIx.	.
</s>
<s>
Hoboken	Hoboken	k1gInSc1	Hoboken
<g/>
:	:	kIx,	:
Wiley-Blackwell	Wiley-Blackwell	k1gInSc1	Wiley-Blackwell
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-1-4051-7600-2	[number]	k4	978-1-4051-7600-2
</s>
</p>
<p>
<s>
HOYOS	HOYOS	kA	HOYOS
<g/>
,	,	kIx,	,
Dexter	Dexter	k1gInSc1	Dexter
<g/>
.	.	kIx.	.
</s>
<s>
Hannibal	Hannibal	k1gInSc1	Hannibal
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Dynasty	dynasta	k1gMnPc7	dynasta
<g/>
:	:	kIx,	:
Power	Power	k1gInSc1	Power
and	and	k?	and
Politics	Politics	k1gInSc1	Politics
in	in	k?	in
the	the	k?	the
Western	Western	kA	Western
Mediterranean	Mediterranean	k1gInSc1	Mediterranean
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
<g/>
:	:	kIx,	:
Routledge	Routledge	k1gInSc1	Routledge
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0-203-41782-8	[number]	k4	0-203-41782-8
</s>
</p>
<p>
<s>
HOYOS	HOYOS	kA	HOYOS
<g/>
,	,	kIx,	,
Dexter	Dexter	k1gInSc1	Dexter
<g/>
.	.	kIx.	.
</s>
<s>
Mastering	Mastering	k1gInSc1	Mastering
the	the	k?	the
West	West	k1gInSc4	West
<g/>
:	:	kIx,	:
Rome	Rom	k1gMnSc5	Rom
and	and	k?	and
Carthage	Carthag	k1gMnSc2	Carthag
at	at	k?	at
War	War	k1gMnSc2	War
<g/>
.	.	kIx.	.
</s>
<s>
Oxford	Oxford	k1gInSc1	Oxford
<g/>
:	:	kIx,	:
Oxford	Oxford	k1gInSc1	Oxford
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978-0-19-939174-5	[number]	k4	978-0-19-939174-5
</s>
</p>
