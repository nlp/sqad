<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
mezi	mezi	k7c7	mezi
první	první	k4xOgFnSc7	první
a	a	k8xC	a
druhou	druhý	k4xOgFnSc7	druhý
punskou	punský	k2eAgFnSc7d1	punská
válkou	válka	k1gFnSc7	válka
uzavřel	uzavřít	k5eAaPmAgInS	uzavřít
kartáginský	kartáginský	k2eAgMnSc1d1	kartáginský
velitel	velitel	k1gMnSc1	velitel
Hasdrubal	Hasdrubal	k1gMnSc1	Hasdrubal
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Římany	Říman	k1gMnPc7	Říman
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
zavázal	zavázat	k5eAaPmAgMnS	zavázat
nerozšiřovat	rozšiřovat	k5eNaImF	rozšiřovat
punskou	punský	k2eAgFnSc4d1	punská
říši	říše	k1gFnSc4	říše
v	v	k7c6	v
Ibérii	Ibérie	k1gFnSc6	Ibérie
na	na	k7c4	na
sever	sever	k1gInSc4	sever
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Ebro	Ebro	k1gNnSc1	Ebro
<g/>
.	.	kIx.	.
</s>
