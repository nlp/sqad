<s>
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
na	na	k7c6	na
Letních	letní	k2eAgFnPc6d1	letní
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
1920	[number]	k4	1920
reprezentovala	reprezentovat	k5eAaImAgFnS	reprezentovat
výprava	výprava	k1gFnSc1	výprava
130	[number]	k4	130
sportovců	sportovec	k1gMnPc2	sportovec
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
129	[number]	k4	129
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
1	[number]	k4	1
žena	žena	k1gFnSc1	žena
v	v	k7c6	v
15	[number]	k4	15
sportech	sport	k1gInPc6	sport
<g/>
.	.	kIx.	.
</s>
