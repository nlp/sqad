<s>
Prohlubeň	prohlubeň	k1gFnSc1
Challenger	Challenger	k1gMnSc1
(	(	kIx(
<g/>
Challenger	Challenger	k1gMnSc1
Deep	Deep	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
Marianském	Marianský	k2eAgInSc6d1
příkopu	příkop	k1gInSc6
v	v	k7c6
Tichém	tichý	k2eAgInSc6d1
oceánu	oceán	k1gInSc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nejhlubší	hluboký	k2eAgNnSc1d3
místo	místo	k1gNnSc1
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>