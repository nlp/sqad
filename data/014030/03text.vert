<s>
Challenger	Challenger	k1gInSc1
(	(	kIx(
<g/>
prohlubeň	prohlubeň	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yQgFnPc4,k3yIgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Poloha	poloha	k1gFnSc1
prohlubně	prohlubeň	k1gFnSc2
Challenger	Challengra	k1gFnPc2
</s>
<s>
Prohlubeň	prohlubeň	k1gFnSc1
Challenger	Challenger	k1gMnSc1
(	(	kIx(
<g/>
Challenger	Challenger	k1gMnSc1
Deep	Deep	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
Marianském	Marianský	k2eAgInSc6d1
příkopu	příkop	k1gInSc6
v	v	k7c6
Tichém	tichý	k2eAgInSc6d1
oceánu	oceán	k1gInSc6
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nejhlubší	hluboký	k2eAgNnSc1d3
místo	místo	k1gNnSc1
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnSc1
hloubka	hloubka	k1gFnSc1
činí	činit	k5eAaImIp3nS
asi	asi	k9
11	#num#	k4
000	#num#	k4
metrů	metr	k1gInPc2
(	(	kIx(
<g/>
36	#num#	k4
000	#num#	k4
stop	stopa	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejbližší	blízký	k2eAgFnSc7d3
pevninou	pevnina	k1gFnSc7
je	být	k5eAaImIp3nS
ostrov	ostrov	k1gInSc1
Fais	Faisa	k1gFnPc2
<g/>
,	,	kIx,
součást	součást	k1gFnSc1
souostroví	souostroví	k1gNnSc2
Yap	Yap	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
jméno	jméno	k1gNnSc4
jí	on	k3xPp3gFnSc3
dala	dát	k5eAaPmAgFnS
průzkumná	průzkumný	k2eAgFnSc1d1
loď	loď	k1gFnSc1
Challenger	Challenger	k1gMnSc1
II	II	kA
<g/>
..	..	k?
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Zeměpisné	zeměpisný	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
světa	svět	k1gInSc2
</s>
<s>
Rekordy	rekord	k1gInPc1
pozemské	pozemský	k2eAgFnSc2d1
neživé	živý	k2eNgFnSc2d1
přírody	příroda	k1gFnSc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
</s>
