<s>
Kněžmost	Kněžmost	k1gFnSc1
</s>
<s>
Kněžmost	Kněžmost	k1gFnSc1
Náměstí	náměstí	k1gNnSc2
Na	na	k7c6
Rynku	rynek	k1gInSc6
</s>
<s>
znak	znak	k1gInSc1
Lokalita	lokalita	k1gFnSc1
Status	status	k1gInSc1
</s>
<s>
obec	obec	k1gFnSc1
LAU	LAU	kA
2	#num#	k4
(	(	kIx(
<g/>
obec	obec	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
CZ0207	CZ0207	k4
536041	#num#	k4
Pověřená	pověřený	k2eAgFnSc1d1
obec	obec	k1gFnSc1
a	a	k8xC
obec	obec	k1gFnSc1
s	s	k7c7
rozšířenou	rozšířený	k2eAgFnSc7d1
působností	působnost	k1gFnSc7
</s>
<s>
Mnichovo	mnichův	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
Okres	okres	k1gInSc1
(	(	kIx(
<g/>
LAU	LAU	kA
1	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Mladá	mladá	k1gFnSc1
Boleslav	Boleslav	k1gMnSc1
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
207	#num#	k4
<g/>
)	)	kIx)
Kraj	kraj	k1gInSc1
(	(	kIx(
<g/>
NUTS	NUTS	kA
3	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Středočeský	středočeský	k2eAgMnSc1d1
(	(	kIx(
<g/>
CZ	CZ	kA
<g/>
0	#num#	k4
<g/>
20	#num#	k4
<g/>
)	)	kIx)
Historická	historický	k2eAgFnSc1d1
země	země	k1gFnSc1
</s>
<s>
Čechy	Čechy	k1gFnPc1
Zeměpisné	zeměpisný	k2eAgFnSc2d1
souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
29	#num#	k4
<g/>
′	′	k?
<g/>
21	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
15	#num#	k4
<g/>
°	°	k?
<g/>
2	#num#	k4
<g/>
′	′	k?
<g/>
18	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Základní	základní	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Počet	počet	k1gInSc4
obyvatel	obyvatel	k1gMnPc2
</s>
<s>
2	#num#	k4
147	#num#	k4
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Rozloha	rozloha	k1gFnSc1
</s>
<s>
40,45	40,45	k4
km²	km²	k?
Nadmořská	nadmořský	k2eAgFnSc1d1
výška	výška	k1gFnSc1
</s>
<s>
242	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
PSČ	PSČ	kA
</s>
<s>
294	#num#	k4
02	#num#	k4
až	až	k9
294	#num#	k4
06	#num#	k4
Počet	počet	k1gInSc1
částí	část	k1gFnPc2
obce	obec	k1gFnSc2
</s>
<s>
15	#num#	k4
Počet	počet	k1gInSc1
k.	k.	k?
ú.	ú.	k?
</s>
<s>
9	#num#	k4
Počet	počet	k1gInSc1
ZSJ	ZSJ	kA
</s>
<s>
14	#num#	k4
Kontakt	kontakt	k1gInSc1
Adresa	adresa	k1gFnSc1
obecního	obecní	k2eAgInSc2d1
úřadu	úřad	k1gInSc2
</s>
<s>
Na	na	k7c6
Rynku	rynek	k1gInSc6
51294	#num#	k4
02	#num#	k4
Kněžmost	Kněžmost	k1gFnSc1
obec@knezmost.cz	obec@knezmost.cza	k1gFnPc2
Starosta	Starosta	k1gMnSc1
</s>
<s>
Ing.	ing.	kA
Karel	Karel	k1gMnSc1
Hlávka	Hlávka	k1gMnSc1
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
<g/>
:	:	kIx,
knezmost	knezmost	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Kněžmost	Kněžmost	k1gFnSc1
</s>
<s>
Další	další	k2eAgInPc1d1
údaje	údaj	k1gInPc1
Kód	kód	k1gInSc4
obce	obec	k1gFnPc1
</s>
<s>
536041	#num#	k4
Geodata	Geodata	k1gFnSc1
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Zdroje	zdroj	k1gInSc2
k	k	k7c3
infoboxu	infobox	k1gInSc3
a	a	k8xC
českým	český	k2eAgNnPc3d1
sídlům	sídlo	k1gNnPc3
<g/>
.	.	kIx.
<g/>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Obec	obec	k1gFnSc1
Kněžmost	Kněžmost	k1gFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
okrese	okres	k1gInSc6
Mladá	mladý	k2eAgFnSc1d1
Boleslav	Boleslav	k1gMnSc1
ve	v	k7c6
Středočeském	středočeský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
,	,	kIx,
zhruba	zhruba	k6eAd1
12	#num#	k4
km	km	kA
na	na	k7c4
severovýchod	severovýchod	k1gInSc4
od	od	k7c2
Mladé	mladá	k1gFnSc2
Boleslavi	Boleslaev	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obcí	obec	k1gFnPc2
protéká	protékat	k5eAaImIp3nS
potok	potok	k1gInSc4
Kněžmostka	Kněžmostka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žije	žít	k5eAaImIp3nS
zde	zde	k6eAd1
přibližně	přibližně	k6eAd1
2	#num#	k4
100	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
První	první	k4xOgFnSc1
písemná	písemný	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
obci	obec	k1gFnSc6
pochází	pocházet	k5eAaImIp3nS
z	z	k7c2
roku	rok	k1gInSc2
1318	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obec	obec	k1gFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
poblíž	poblíž	k7c2
knížecího	knížecí	k2eAgInSc2d1
mostu	most	k1gInSc2
přes	přes	k7c4
močál	močál	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Územněsprávní	územněsprávní	k2eAgNnSc4d1
začlenění	začlenění	k1gNnSc4
</s>
<s>
Dějiny	dějiny	k1gFnPc1
územněsprávního	územněsprávní	k2eAgNnSc2d1
začleňování	začleňování	k1gNnSc2
zahrnují	zahrnovat	k5eAaImIp3nP
období	období	k1gNnSc4
od	od	k7c2
roku	rok	k1gInSc2
1850	#num#	k4
do	do	k7c2
současnosti	současnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
chronologickém	chronologický	k2eAgInSc6d1
přehledu	přehled	k1gInSc6
je	být	k5eAaImIp3nS
uvedena	uvést	k5eAaPmNgFnS
územně	územně	k6eAd1
administrativní	administrativní	k2eAgFnSc4d1
příslušnost	příslušnost	k1gFnSc4
obce	obec	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
ke	k	k7c3
změně	změna	k1gFnSc3
došlo	dojít	k5eAaPmAgNnS
<g/>
:	:	kIx,
</s>
<s>
1850	#num#	k4
země	země	k1gFnSc1
česká	český	k2eAgFnSc1d1
<g/>
,	,	kIx,
kraj	kraj	k1gInSc1
Jičín	Jičín	k1gInSc1
<g/>
,	,	kIx,
politický	politický	k2eAgInSc1d1
okres	okres	k1gInSc1
Mladá	mladá	k1gFnSc1
Boleslav	Boleslav	k1gMnSc1
<g/>
,	,	kIx,
soudní	soudní	k2eAgInSc4d1
okres	okres	k1gInSc4
Mnichovo	mnichův	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1855	#num#	k4
země	země	k1gFnSc1
česká	český	k2eAgFnSc1d1
<g/>
,	,	kIx,
kraj	kraj	k1gInSc1
Mladá	mladá	k1gFnSc1
Boleslav	Boleslav	k1gMnSc1
<g/>
,	,	kIx,
soudní	soudní	k2eAgInSc4d1
okres	okres	k1gInSc4
Mnichovo	mnichův	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1868	#num#	k4
země	země	k1gFnSc1
česká	český	k2eAgFnSc1d1
<g/>
,	,	kIx,
politický	politický	k2eAgInSc4d1
i	i	k8xC
soudní	soudní	k2eAgInSc4d1
okres	okres	k1gInSc4
Mnichovo	mnichův	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1939	#num#	k4
země	země	k1gFnSc1
česká	český	k2eAgFnSc1d1
<g/>
,	,	kIx,
Oberlandrat	Oberlandrat	k1gInSc1
Jičín	Jičín	k1gInSc1
<g/>
,	,	kIx,
politický	politický	k2eAgInSc4d1
i	i	k8xC
soudní	soudní	k2eAgInSc4d1
okres	okres	k1gInSc4
Mnichovo	mnichův	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1942	#num#	k4
země	země	k1gFnSc1
česká	český	k2eAgFnSc1d1
<g/>
,	,	kIx,
Oberlandrat	Oberlandrat	k1gInSc1
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
politický	politický	k2eAgInSc1d1
okres	okres	k1gInSc1
Mladá	mladá	k1gFnSc1
Boleslav	Boleslav	k1gMnSc1
<g/>
,	,	kIx,
soudní	soudní	k2eAgInSc4d1
okres	okres	k1gInSc4
Mnichovo	mnichův	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1945	#num#	k4
země	země	k1gFnSc1
česká	český	k2eAgFnSc1d1
<g/>
,	,	kIx,
správní	správní	k2eAgFnSc1d1
i	i	k9
soudní	soudní	k2eAgInSc4d1
okres	okres	k1gInSc4
Mnichovo	mnichův	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1949	#num#	k4
Liberecký	liberecký	k2eAgInSc4d1
kraj	kraj	k1gInSc4
<g/>
,	,	kIx,
okres	okres	k1gInSc4
Mnichovo	mnichův	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1960	#num#	k4
Středočeský	středočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
<g/>
,	,	kIx,
okres	okres	k1gInSc1
Mladá	mladá	k1gFnSc1
Boleslav	Boleslav	k1gMnSc1
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rok	rok	k1gInSc1
1932	#num#	k4
</s>
<s>
V	v	k7c6
městysi	městys	k1gInSc6
Kněžmost	Kněžmost	k1gFnSc1
s	s	k7c7
1126	#num#	k4
obyvateli	obyvatel	k1gMnPc7
v	v	k7c6
roce	rok	k1gInSc6
1932	#num#	k4
byly	být	k5eAaImAgInP
evidovány	evidován	k2eAgInPc1d1
tyto	tento	k3xDgInPc1
úřady	úřad	k1gInPc1
<g/>
,	,	kIx,
živnosti	živnost	k1gFnPc1
a	a	k8xC
obchody	obchod	k1gInPc1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
poštovní	poštovní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
telegrafní	telegrafní	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
katolický	katolický	k2eAgInSc1d1
kostel	kostel	k1gInSc1
<g/>
,	,	kIx,
četnická	četnický	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
<g/>
,	,	kIx,
lékař	lékař	k1gMnSc1
<g/>
,	,	kIx,
nákladní	nákladní	k2eAgFnSc1d1
autodoprava	autodoprava	k1gFnSc1
<g/>
,	,	kIx,
2	#num#	k4
autodrožky	autodrožka	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
biograf	biograf	k1gMnSc1
Sokol	Sokol	k1gMnSc1
<g/>
,	,	kIx,
cihelna	cihelna	k1gFnSc1
<g/>
,	,	kIx,
cukrář	cukrář	k1gMnSc1
<g/>
,	,	kIx,
výroba	výroba	k1gFnSc1
čerpadel	čerpadlo	k1gNnPc2
<g/>
,	,	kIx,
3	#num#	k4
obchody	obchod	k1gInPc4
s	s	k7c7
dobytkem	dobytek	k1gInSc7
<g/>
,	,	kIx,
drogerie	drogerie	k1gFnSc1
<g/>
,	,	kIx,
elektrotechnický	elektrotechnický	k2eAgInSc1d1
závod	závod	k1gInSc1
<g/>
,	,	kIx,
3	#num#	k4
holiči	holič	k1gMnPc7
<g/>
,	,	kIx,
8	#num#	k4
hostinců	hostinec	k1gInPc2
<g/>
,	,	kIx,
kapelník	kapelník	k1gMnSc1
<g/>
,	,	kIx,
2	#num#	k4
klempíři	klempíř	k1gMnPc7
<g/>
,	,	kIx,
3	#num#	k4
koláři	kolář	k1gMnPc7
<g/>
,	,	kIx,
konsum	konsum	k1gInSc1
Svépomoc	svépomoc	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
2	#num#	k4
kováři	kovář	k1gMnPc7
<g/>
,	,	kIx,
3	#num#	k4
krejčí	krejčí	k1gMnSc1
<g/>
,	,	kIx,
továrna	továrna	k1gFnSc1
na	na	k7c4
kůže	kůže	k1gFnPc4
<g/>
,	,	kIx,
obchod	obchod	k1gInSc4
s	s	k7c7
lahůdkami	lahůdka	k1gFnPc7
<g/>
,	,	kIx,
malíř	malíř	k1gMnSc1
<g/>
,	,	kIx,
obchod	obchod	k1gInSc1
s	s	k7c7
máslem	máslo	k1gNnSc7
a	a	k8xC
vejci	vejce	k1gNnPc7
<g/>
,	,	kIx,
půjčovna	půjčovna	k1gFnSc1
mlátiček	mlátička	k1gFnPc2
<g/>
,	,	kIx,
2	#num#	k4
mlýny	mlýn	k1gInPc1
<g/>
,	,	kIx,
nodistka	nodistka	k1gFnSc1
<g/>
,	,	kIx,
3	#num#	k4
obchody	obchod	k1gInPc4
s	s	k7c7
obuví	obuv	k1gFnSc7
<g/>
,	,	kIx,
4	#num#	k4
obuvníci	obuvník	k1gMnPc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
obchod	obchod	k1gInSc1
s	s	k7c7
ovocem	ovoce	k1gNnSc7
a	a	k8xC
zeleninou	zelenina	k1gFnSc7
<g/>
,	,	kIx,
3	#num#	k4
pekaři	pekař	k1gMnPc7
<g/>
,	,	kIx,
obchod	obchod	k1gInSc4
s	s	k7c7
peřím	peřit	k5eAaImIp1nS
<g/>
,	,	kIx,
pohřební	pohřební	k2eAgInSc1d1
ústav	ústav	k1gInSc1
<g/>
,	,	kIx,
pokrývač	pokrývač	k1gMnSc1
<g/>
,	,	kIx,
2	#num#	k4
porodní	porodní	k2eAgFnPc1d1
asistentky	asistentka	k1gFnPc1
<g/>
,	,	kIx,
obchod	obchod	k1gInSc1
s	s	k7c7
rákosovým	rákosový	k2eAgNnSc7d1
zbožím	zboží	k1gNnSc7
<g/>
,	,	kIx,
3	#num#	k4
řezníci	řezník	k1gMnPc1
<g/>
,	,	kIx,
2	#num#	k4
sadaři	sadař	k1gMnPc7
<g/>
,	,	kIx,
sedlář	sedlář	k1gMnSc1
<g/>
,	,	kIx,
brusírna	brusírna	k1gFnSc1
skla	sklo	k1gNnSc2
<g/>
,	,	kIx,
7	#num#	k4
obchodů	obchod	k1gInPc2
se	s	k7c7
smíšeným	smíšený	k2eAgNnSc7d1
zbožím	zboží	k1gNnSc7
<g/>
,	,	kIx,
rolnicko-občanská	rolnicko-občanský	k2eAgFnSc1d1
záložna	záložna	k1gFnSc1
<g/>
,	,	kIx,
obchod	obchod	k1gInSc1
se	s	k7c7
střižním	střižní	k2eAgNnSc7d1
zbožím	zboží	k1gNnSc7
<g/>
,	,	kIx,
6	#num#	k4
švadlen	švadlena	k1gFnPc2
<g/>
,	,	kIx,
4	#num#	k4
trafiky	trafika	k1gFnSc2
<g/>
,	,	kIx,
2	#num#	k4
truhláři	truhlář	k1gMnPc7
<g/>
,	,	kIx,
2	#num#	k4
obchody	obchod	k1gInPc4
s	s	k7c7
uhlím	uhlí	k1gNnSc7
<g/>
,	,	kIx,
zámečník	zámečník	k1gMnSc1
<g/>
,	,	kIx,
obchod	obchod	k1gInSc1
se	s	k7c7
zemskými	zemský	k2eAgFnPc7d1
plodinami	plodina	k1gFnPc7
<g/>
,	,	kIx,
žehlírna	žehlírna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
vsi	ves	k1gFnSc6
Koprník-Násedlnice	Koprník-Násedlnice	k1gFnSc2
s	s	k7c7
512	#num#	k4
obyvateli	obyvatel	k1gMnPc7
(	(	kIx(
<g/>
v	v	k7c6
roce	rok	k1gInSc6
1932	#num#	k4
samostatné	samostatný	k2eAgFnPc4d1
vsi	ves	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
později	pozdě	k6eAd2
stala	stát	k5eAaPmAgFnS
součástí	součást	k1gFnSc7
Kněžmostu	Kněžmost	k1gInSc2
<g/>
)	)	kIx)
byly	být	k5eAaImAgFnP
evidovány	evidovat	k5eAaImNgFnP
tyto	tento	k3xDgFnPc1
živnosti	živnost	k1gFnPc1
a	a	k8xC
obchody	obchod	k1gInPc1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
obchod	obchod	k1gInSc1
s	s	k7c7
dobytkem	dobytek	k1gInSc7
<g/>
,	,	kIx,
2	#num#	k4
hostince	hostinec	k1gInSc2
<g/>
,	,	kIx,
kovář	kovář	k1gMnSc1
<g/>
,	,	kIx,
obuvník	obuvník	k1gMnSc1
<g/>
,	,	kIx,
2	#num#	k4
obchody	obchod	k1gInPc4
se	s	k7c7
smíšeným	smíšený	k2eAgNnSc7d1
zbožím	zboží	k1gNnSc7
<g/>
,	,	kIx,
2	#num#	k4
trafiky	trafika	k1gFnSc2
<g/>
,	,	kIx,
velkostatek	velkostatek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
vsi	ves	k1gFnSc6
Malobratřice	Malobratřice	k1gFnSc2
s	s	k7c7
266	#num#	k4
obyvateli	obyvatel	k1gMnPc7
(	(	kIx(
<g/>
v	v	k7c6
roce	rok	k1gInSc6
1932	#num#	k4
samostatné	samostatný	k2eAgFnPc4d1
vsi	ves	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
později	pozdě	k6eAd2
stala	stát	k5eAaPmAgFnS
součástí	součást	k1gFnSc7
Kněžmostu	Kněžmost	k1gInSc2
<g/>
)	)	kIx)
byly	být	k5eAaImAgFnP
evidovány	evidovat	k5eAaImNgFnP
tyto	tento	k3xDgFnPc1
živnosti	živnost	k1gFnPc1
a	a	k8xC
obchody	obchod	k1gInPc1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
cihelna	cihelna	k1gFnSc1
<g/>
,	,	kIx,
hostinec	hostinec	k1gInSc1
<g/>
,	,	kIx,
kovář	kovář	k1gMnSc1
<g/>
,	,	kIx,
obchod	obchod	k1gInSc1
se	s	k7c7
smíšeným	smíšený	k2eAgNnSc7d1
zbožím	zboží	k1gNnSc7
<g/>
,	,	kIx,
trafika	trafika	k1gFnSc1
<g/>
,	,	kIx,
truhlář	truhlář	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
vsi	ves	k1gFnSc6
Solec	Solec	k1gInSc4
s	s	k7c7
304	#num#	k4
obyvateli	obyvatel	k1gMnPc7
(	(	kIx(
<g/>
v	v	k7c6
roce	rok	k1gInSc6
1932	#num#	k4
samostatné	samostatný	k2eAgFnPc4d1
vsi	ves	k1gFnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
se	se	k3xPyFc4
později	pozdě	k6eAd2
stala	stát	k5eAaPmAgFnS
součástí	součást	k1gFnSc7
Kněžmostu	Kněžmost	k1gInSc2
<g/>
)	)	kIx)
byly	být	k5eAaImAgFnP
evidovány	evidovat	k5eAaImNgFnP
tyto	tento	k3xDgFnPc1
živnosti	živnost	k1gFnPc1
a	a	k8xC
obchody	obchod	k1gInPc1
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
obchod	obchod	k1gInSc1
s	s	k7c7
dobytkem	dobytek	k1gInSc7
<g/>
,	,	kIx,
3	#num#	k4
hostince	hostinec	k1gInSc2
<g/>
,	,	kIx,
kolář	kolář	k1gMnSc1
<g/>
,	,	kIx,
2	#num#	k4
kováři	kovář	k1gMnPc1
<g/>
,	,	kIx,
krejčí	krejčí	k1gMnPc1
<g/>
,	,	kIx,
2	#num#	k4
mlýny	mlýn	k1gInPc1
<g/>
,	,	kIx,
2	#num#	k4
obuvníci	obuvník	k1gMnPc1
<g/>
,	,	kIx,
obchod	obchod	k1gInSc1
se	s	k7c7
smíšeným	smíšený	k2eAgNnSc7d1
zbožím	zboží	k1gNnSc7
<g/>
,	,	kIx,
sadař	sadař	k1gMnSc1
<g/>
,	,	kIx,
spořitelní	spořitelní	k2eAgMnSc1d1
a	a	k8xC
záložní	záložní	k2eAgInSc1d1
spolek	spolek	k1gInSc1
<g/>
,	,	kIx,
výroba	výroba	k1gFnSc1
hospodářských	hospodářský	k2eAgInPc2d1
strojů	stroj	k1gInPc2
<g/>
,	,	kIx,
švadlena	švadlena	k1gFnSc1
<g/>
,	,	kIx,
3	#num#	k4
trafiky	trafika	k1gFnSc2
<g/>
,	,	kIx,
truhlář	truhlář	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Pamětihodnosti	pamětihodnost	k1gFnPc1
</s>
<s>
Kostel	kostel	k1gInSc1
svatého	svatý	k2eAgMnSc2d1
Františka	František	k1gMnSc2
Serafinského	Serafinský	k2eAgMnSc2d1
uprostřed	uprostřed	k7c2
vsi	ves	k1gFnSc2
<g/>
,	,	kIx,
postavený	postavený	k2eAgMnSc1d1
byl	být	k5eAaImAgInS
v	v	k7c6
letech	léto	k1gNnPc6
1838	#num#	k4
až	až	k9
1841	#num#	k4
ve	v	k7c6
stylu	styl	k1gInSc6
pozdního	pozdní	k2eAgInSc2d1
empíru	empír	k1gInSc2
(	(	kIx(
<g/>
opravovaný	opravovaný	k2eAgMnSc1d1
<g/>
)	)	kIx)
</s>
<s>
Socha	socha	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Jana	Jan	k1gMnSc2
Nepomuckého	Nepomucký	k1gMnSc2
na	na	k7c6
návsi	náves	k1gFnSc6
</s>
<s>
Několik	několik	k4yIc1
opravených	opravený	k2eAgInPc2d1
městských	městský	k2eAgInPc2d1
domů	dům	k1gInPc2
z	z	k7c2
18	#num#	k4
<g/>
.	.	kIx.
<g/>
–	–	k?
<g/>
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
na	na	k7c6
náměstí	náměstí	k1gNnSc6
</s>
<s>
Doprava	doprava	k1gFnSc1
</s>
<s>
Obcí	obec	k1gFnSc7
prochází	procházet	k5eAaImIp3nS
silnice	silnice	k1gFnSc1
II	II	kA
<g/>
/	/	kIx~
<g/>
268	#num#	k4
Mimoň	Mimoň	k1gFnSc1
-	-	kIx~
Mnichovo	mnichův	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
-	-	kIx~
Kněžmost	Kněžmost	k1gFnSc1
-	-	kIx~
Horní	horní	k2eAgInSc1d1
Bousov	Bousov	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
obci	obec	k1gFnSc6
končí	končit	k5eAaImIp3nS
silnice	silnice	k1gFnSc1
II	II	kA
<g/>
/	/	kIx~
<g/>
276	#num#	k4
Bělá	bělat	k5eAaImIp3nS
pod	pod	k7c7
Bezdězem	Bezděz	k1gInSc7
-	-	kIx~
Bakov	Bakov	k1gInSc1
nad	nad	k7c7
Jizerou	Jizera	k1gFnSc7
-	-	kIx~
Kněžmost	Kněžmost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Místní	místní	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
v	v	k7c6
bývalém	bývalý	k2eAgInSc6d1
městysi	městys	k1gInSc6
Kněžmostě	Kněžmost	k1gInSc6
provozoval	provozovat	k5eAaImAgMnS
v	v	k7c6
minulosti	minulost	k1gFnSc6
podnik	podnik	k1gInSc1
ČSAD	ČSAD	kA
a	a	k8xC
zůstala	zůstat	k5eAaPmAgFnS
i	i	k8xC
podniku	podnik	k1gInSc2
SECO	SECO	kA
Trans	trans	k1gInSc1
<g/>
,	,	kIx,
vzniklému	vzniklý	k2eAgInSc3d1
transformací	transformace	k1gFnSc7
kosmonoského	kosmonoský	k2eAgInSc2d1
dopravního	dopravní	k2eAgInSc2d1
závodu	závod	k1gInSc2
ČSAD	ČSAD	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
ke	k	k7c3
vzrůstajícím	vzrůstající	k2eAgInPc3d1
finančním	finanční	k2eAgInPc3d1
požadavkům	požadavek	k1gInPc3
dopravce	dopravce	k1gMnSc2
si	se	k3xPyFc3
obec	obec	k1gFnSc1
založila	založit	k5eAaPmAgFnS
v	v	k7c6
únoru	únor	k1gInSc6
1998	#num#	k4
vlastní	vlastní	k2eAgInSc1d1
Dopravní	dopravní	k2eAgInSc1d1
podnik	podnik	k1gInSc1
Kněžmost	Kněžmost	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
r.	r.	kA
o.	o.	k?
a	a	k8xC
převzala	převzít	k5eAaPmAgFnS
místní	místní	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Linky	linka	k1gFnSc2
260621	#num#	k4
–	–	k?
260628	#num#	k4
byly	být	k5eAaImAgInP
zpočátku	zpočátku	k6eAd1
v	v	k7c6
jízdních	jízdní	k2eAgInPc6d1
řádech	řád	k1gInPc6
označeny	označit	k5eAaPmNgFnP
zkráceným	zkrácený	k2eAgNnSc7d1
označením	označení	k1gNnSc7
(	(	kIx(
<g/>
1	#num#	k4
až	až	k9
8	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
byl	být	k5eAaImAgMnS
linek	linka	k1gFnPc2
již	již	k6eAd1
jen	jen	k9
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polookružní	Polookružný	k2eAgMnPc5d1
linky	linka	k1gFnPc1
1	#num#	k4
a	a	k8xC
4	#num#	k4
jezdí	jezdit	k5eAaImIp3nP
výhradně	výhradně	k6eAd1
po	po	k7c6
vesnicích	vesnice	k1gFnPc6
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
součástí	součást	k1gFnSc7
obce	obec	k1gFnSc2
Kněžmost	Kněžmost	k1gFnSc1
<g/>
,	,	kIx,
ostatní	ostatní	k2eAgFnPc1d1
linky	linka	k1gFnPc1
jsou	být	k5eAaImIp3nP
regionální	regionální	k2eAgFnPc1d1
meziměstské	meziměstská	k1gFnPc1
(	(	kIx(
<g/>
linka	linka	k1gFnSc1
6	#num#	k4
je	být	k5eAaImIp3nS
jako	jako	k9
"	"	kIx"
<g/>
hnědá	hnědý	k2eAgFnSc1d1
linka	linka	k1gFnSc1
<g/>
"	"	kIx"
cyklobusu	cyklobus	k1gInSc6
součástí	součást	k1gFnSc7
projektu	projekt	k1gInSc2
turistických	turistický	k2eAgInPc2d1
autobusů	autobus	k1gInPc2
Českého	český	k2eAgInSc2d1
ráje	ráj	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dopravu	doprava	k1gFnSc4
na	na	k7c6
těchto	tento	k3xDgFnPc6
linkách	linka	k1gFnPc6
zajišťovala	zajišťovat	k5eAaImAgFnS
dvě	dva	k4xCgNnPc4
vozidla	vozidlo	k1gNnPc4
<g/>
,	,	kIx,
Ford	ford	k1gInSc1
Champion	Champion	k1gInSc1
a	a	k8xC
IVECO	IVECO	kA
<g/>
,	,	kIx,
v	v	k7c6
případě	případ	k1gInSc6
poruchy	porucha	k1gFnSc2
některého	některý	k3yIgMnSc2
z	z	k7c2
nich	on	k3xPp3gMnPc2
si	se	k3xPyFc3
dopravce	dopravce	k1gMnSc1
půjčoval	půjčovat	k5eAaImAgMnS
mikrobus	mikrobus	k1gInSc4
nebo	nebo	k8xC
dodávkový	dodávkový	k2eAgInSc4d1
automobil	automobil	k1gInSc4
od	od	k7c2
firmy	firma	k1gFnSc2
IVECO	IVECO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začátkem	začátkem	k7c2
června	červen	k1gInSc2
2007	#num#	k4
obec	obec	k1gFnSc1
zakoupila	zakoupit	k5eAaPmAgFnS
nový	nový	k2eAgInSc4d1
autobus	autobus	k1gInSc4
SOR	SOR	kA
C	C	kA
<g/>
9.5	9.5	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
květnu	květen	k1gInSc6
2011	#num#	k4
jezdily	jezdit	k5eAaImAgInP
z	z	k7c2
Kněžmostu	Kněžmost	k1gInSc2
regionální	regionální	k2eAgFnSc2d1
autobusové	autobusový	k2eAgFnSc2d1
linky	linka	k1gFnSc2
například	například	k6eAd1
do	do	k7c2
těchto	tento	k3xDgInPc2
cílů	cíl	k1gInPc2
<g/>
:	:	kIx,
Bakov	Bakov	k1gInSc1
nad	nad	k7c7
Jizerou	Jizera	k1gFnSc7
<g/>
,	,	kIx,
Dolní	dolní	k2eAgInSc1d1
Bousov	Bousov	k1gInSc1
<g/>
,	,	kIx,
Jičín	Jičín	k1gInSc1
<g/>
,	,	kIx,
Mladá	mladá	k1gFnSc1
Boleslav	Boleslav	k1gMnSc1
<g/>
,	,	kIx,
Mnichovo	mnichův	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
<g/>
,	,	kIx,
Sobotka	Sobotka	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Železniční	železniční	k2eAgFnSc1d1
trať	trať	k1gFnSc1
063	#num#	k4
Bakov	Bakov	k1gInSc1
nad	nad	k7c7
Jizerou	Jizera	k1gFnSc7
-	-	kIx~
Dolní	dolní	k2eAgInSc1d1
Bousov	Bousov	k1gInSc1
-	-	kIx~
(	(	kIx(
<g/>
Kopidlno	Kopidlno	k1gNnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
jednokolejná	jednokolejný	k2eAgFnSc1d1
regionální	regionální	k2eAgFnSc1d1
trať	trať	k1gFnSc1
<g/>
,	,	kIx,
doprava	doprava	k1gFnSc1
byla	být	k5eAaImAgFnS
zahájena	zahájen	k2eAgFnSc1d1
roku	rok	k1gInSc2
1883	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
úseku	úsek	k1gInSc6
Bakov	Bakov	k1gInSc1
nad	nad	k7c7
Jizerou	Jizera	k1gFnSc7
-	-	kIx~
Dolní	dolní	k2eAgInSc4d1
Bousov	Bousov	k1gInSc4
jezdilo	jezdit	k5eAaImAgNnS
v	v	k7c6
pracovních	pracovní	k2eAgInPc6d1
dnech	den	k1gInPc6
roku	rok	k1gInSc2
2011	#num#	k4
5	#num#	k4
párů	pár	k1gInPc2
osobních	osobní	k2eAgInPc2d1
vlaků	vlak	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
úseku	úsek	k1gInSc6
z	z	k7c2
Dolního	dolní	k2eAgNnSc2d1
Bousova	Bousův	k2eAgNnSc2d1
do	do	k7c2
Kopidlna	Kopidlno	k1gNnSc2
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2007	#num#	k4
zastavena	zastaven	k2eAgFnSc1d1
osobní	osobní	k2eAgFnSc1d1
doprava	doprava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
území	území	k1gNnSc6
obce	obec	k1gFnSc2
leží	ležet	k5eAaImIp3nS
železniční	železniční	k2eAgFnSc2d1
zastávky	zastávka	k1gFnSc2
Kněžmost	Kněžmost	k1gFnSc1
a	a	k8xC
Litkovice	Litkovice	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Turistika	turistika	k1gFnSc1
</s>
<s>
Obec	obec	k1gFnSc1
je	být	k5eAaImIp3nS
zachycena	zachytit	k5eAaPmNgFnS
na	na	k7c6
turistických	turistický	k2eAgFnPc6d1
mapách	mapa	k1gFnPc6
KČT	KČT	kA
17	#num#	k4
a	a	k8xC
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poblíž	poblíž	k6eAd1
je	být	k5eAaImIp3nS
řada	řada	k1gFnSc1
rybníků	rybník	k1gInPc2
a	a	k8xC
koupališť	koupaliště	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Osobnosti	osobnost	k1gFnPc1
</s>
<s>
Alois	Alois	k1gMnSc1
Zima	Zima	k1gMnSc1
(	(	kIx(
<g/>
1873	#num#	k4
<g/>
–	–	k?
<g/>
1960	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
architekt	architekt	k1gMnSc1
<g/>
,	,	kIx,
stavitel	stavitel	k1gMnSc1
</s>
<s>
Části	část	k1gFnPc1
obce	obec	k1gFnSc2
</s>
<s>
Kněžmost	Kněžmost	k1gFnSc1
</s>
<s>
Býčina	Býčina	k1gFnSc1
</s>
<s>
Čížovka	Čížovka	k1gFnSc1
</s>
<s>
Drhleny	drhlen	k1gInPc1
</s>
<s>
Chlumín	Chlumín	k1gMnSc1
</s>
<s>
Koprník	koprník	k1gInSc1
</s>
<s>
Lítkovice	Lítkovice	k1gFnSc1
</s>
<s>
Malobratřice	Malobratřice	k1gFnSc1
</s>
<s>
Násedlnice	Násedlnice	k1gFnSc1
</s>
<s>
Solec	Solec	k1gMnSc1
</s>
<s>
Soleček	Soleček	k1gMnSc1
</s>
<s>
Srbsko	Srbsko	k1gNnSc1
</s>
<s>
Suhrovice	Suhrovice	k1gFnSc1
</s>
<s>
Úhelnice	úhelnice	k1gFnSc1
</s>
<s>
Žantov	Žantov	k1gInSc1
</s>
<s>
Okolní	okolní	k2eAgFnPc1d1
obce	obec	k1gFnPc1
</s>
<s>
Kněžmost	Kněžmost	k1gFnSc1
sousedí	sousedit	k5eAaImIp3nS
</s>
<s>
na	na	k7c6
severu	sever	k1gInSc6
s	s	k7c7
obcemi	obec	k1gFnPc7
Boseň	Boseň	k1gFnSc1
<g/>
,	,	kIx,
Branžež	Branžež	k1gFnSc1
a	a	k8xC
Žďár	Žďár	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
na	na	k7c6
východě	východ	k1gInSc6
s	s	k7c7
Libošovicemi	Libošovice	k1gFnPc7
<g/>
,	,	kIx,
Dobšínem	Dobšín	k1gInSc7
a	a	k8xC
Přepeřemi	Přepeř	k1gFnPc7
<g/>
,	,	kIx,
</s>
<s>
na	na	k7c6
jihu	jih	k1gInSc6
s	s	k7c7
obcemi	obec	k1gFnPc7
Obruby	obruba	k1gFnSc2
<g/>
,	,	kIx,
Obrubce	obrubka	k1gFnSc3
a	a	k8xC
Husí	husí	k2eAgFnSc1d1
Lhota	Lhota	k1gFnSc1
</s>
<s>
na	na	k7c6
západě	západ	k1gInSc6
s	s	k7c7
Dolními	dolní	k2eAgInPc7d1
Stakory	Stakor	k1gInPc7
<g/>
,	,	kIx,
Bakovem	Bakovo	k1gNnSc7
nad	nad	k7c7
Jizerou	Jizera	k1gFnSc7
a	a	k8xC
Mnichovým	mnichův	k2eAgNnSc7d1
Hradištěm	Hradiště	k1gNnSc7
</s>
<s>
Další	další	k2eAgFnPc1d1
fotografie	fotografia	k1gFnPc1
</s>
<s>
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Františka	František	k1gMnSc2
Serafínského	serafínský	k2eAgMnSc2d1
</s>
<s>
Náměstí	náměstí	k1gNnSc1
</s>
<s>
Dům	dům	k1gInSc1
v	v	k7c6
Žantově	Žantův	k2eAgNnSc6d1
<g/>
,	,	kIx,
předměstí	předměstí	k1gNnSc6
Kněžmostu	Kněžmost	k1gInSc2
</s>
<s>
Kaplička	kaplička	k1gFnSc1
v	v	k7c6
místní	místní	k2eAgFnSc6d1
části	část	k1gFnSc6
Býčina	Býčino	k1gNnSc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
:	:	kIx,
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnPc2
v	v	k7c6
obcích	obec	k1gFnPc6
-	-	kIx~
k	k	k7c3
1.1	1.1	k4
<g/>
.2020	.2020	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
1	#num#	k4
2	#num#	k4
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
–	–	k?
Stručný	stručný	k2eAgMnSc1d1
turistický	turistický	k2eAgMnSc1d1
průvodce	průvodce	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cheb	Cheb	k1gInSc1
<g/>
:	:	kIx,
Music	Music	k1gMnSc1
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
85925	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
Kněžmost	Kněžmost	k1gFnSc1
<g/>
,	,	kIx,
s.	s.	k?
220	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
Správní	správní	k2eAgInSc4d1
uspořádání	uspořádání	k1gNnSc1
Předlitavska	Předlitavsko	k1gNnSc2
1850	#num#	k4
<g/>
-	-	kIx~
<g/>
1918	#num#	k4
<g/>
↑	↑	k?
Amtliches	Amtliches	k1gMnSc1
Deutsches	Deutsches	k1gMnSc1
Ortsbuch	Ortsbuch	k1gMnSc1
für	für	k?
das	das	k?
Protektorat	Protektorat	k1gInSc1
Böhmen	Böhmen	k1gInSc1
und	und	k?
Mähren	Mährna	k1gFnPc2
<g/>
↑	↑	k?
Nařízení	nařízení	k1gNnSc1
ministra	ministr	k1gMnSc2
vnitra	vnitro	k1gNnSc2
č.	č.	k?
185	#num#	k4
<g/>
/	/	kIx~
<g/>
1942	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Dekret	dekret	k1gInSc1
presidenta	president	k1gMnSc2
republiky	republika	k1gFnSc2
č.	č.	k?
121	#num#	k4
<g/>
/	/	kIx~
<g/>
1945	#num#	k4
Sb	sb	kA
<g/>
..	..	k?
aplikace	aplikace	k1gFnSc1
<g/>
.	.	kIx.
<g/>
mvcr	mvcr	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Vládní	vládní	k2eAgNnSc1d1
nařízení	nařízení	k1gNnSc1
č.	č.	k?
3	#num#	k4
<g/>
/	/	kIx~
<g/>
1949	#num#	k4
Sb	sb	kA
<g/>
..	..	k?
aplikace	aplikace	k1gFnSc1
<g/>
.	.	kIx.
<g/>
mvcr	mvcr	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Zákon	zákon	k1gInSc1
č.	č.	k?
36	#num#	k4
<g/>
/	/	kIx~
<g/>
1960	#num#	k4
Sb	sb	kA
<g/>
..	..	k?
aplikace	aplikace	k1gFnSc1
<g/>
.	.	kIx.
<g/>
mvcr	mvcr	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Adresář	adresář	k1gInSc1
republiky	republika	k1gFnSc2
Československé	československý	k2eAgFnSc2d1
pro	pro	k7c4
průmysl	průmysl	k1gInSc4
<g/>
,	,	kIx,
živnosti	živnost	k1gFnPc4
<g/>
,	,	kIx,
obchod	obchod	k1gInSc4
a	a	k8xC
zemědělství	zemědělství	k1gNnSc4
<g/>
,	,	kIx,
sestavila	sestavit	k5eAaPmAgFnS
a	a	k8xC
vydala	vydat	k5eAaPmAgFnS
firma	firma	k1gFnSc1
Rudolf	Rudolfa	k1gFnPc2
Mosse	Mosse	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1932	#num#	k4
<g/>
,	,	kIx,
svazek	svazek	k1gInSc1
I	I	kA
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
strana	strana	k1gFnSc1
581	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
a	a	k8xC
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Adresář	adresář	k1gInSc1
republiky	republika	k1gFnSc2
Československé	československý	k2eAgFnSc2d1
pro	pro	k7c4
průmysl	průmysl	k1gInSc4
<g/>
,	,	kIx,
živnosti	živnost	k1gFnPc4
<g/>
,	,	kIx,
obchod	obchod	k1gInSc4
a	a	k8xC
zemědělství	zemědělství	k1gNnSc4
<g/>
,	,	kIx,
sestavila	sestavit	k5eAaPmAgFnS
a	a	k8xC
vydala	vydat	k5eAaPmAgFnS
firma	firma	k1gFnSc1
Rudolf	Rudolfa	k1gFnPc2
Mosse	Mosse	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1932	#num#	k4
<g/>
,	,	kIx,
svazek	svazek	k1gInSc1
I	I	kA
<g/>
,	,	kIx,
strana	strana	k1gFnSc1
603	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
česky	česky	k6eAd1
a	a	k8xC
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Adresář	adresář	k1gInSc1
republiky	republika	k1gFnSc2
Československé	československý	k2eAgFnSc2d1
pro	pro	k7c4
průmysl	průmysl	k1gInSc4
<g/>
,	,	kIx,
živnosti	živnost	k1gFnPc4
<g/>
,	,	kIx,
obchod	obchod	k1gInSc4
a	a	k8xC
zemědělství	zemědělství	k1gNnSc4
<g/>
,	,	kIx,
sestavila	sestavit	k5eAaPmAgFnS
a	a	k8xC
vydala	vydat	k5eAaPmAgFnS
firma	firma	k1gFnSc1
Rudolf	Rudolfa	k1gFnPc2
Mosse	Mosse	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1932	#num#	k4
<g/>
,	,	kIx,
svazek	svazek	k1gInSc1
I	I	kA
<g/>
,	,	kIx,
strana	strana	k1gFnSc1
703	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
a	a	k8xC
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
↑	↑	k?
Adresář	adresář	k1gInSc1
republiky	republika	k1gFnSc2
Československé	československý	k2eAgFnSc2d1
pro	pro	k7c4
průmysl	průmysl	k1gInSc4
<g/>
,	,	kIx,
živnosti	živnost	k1gFnPc4
<g/>
,	,	kIx,
obchod	obchod	k1gInSc4
a	a	k8xC
zemědělství	zemědělství	k1gNnSc4
<g/>
,	,	kIx,
sestavila	sestavit	k5eAaPmAgFnS
a	a	k8xC
vydala	vydat	k5eAaPmAgFnS
firma	firma	k1gFnSc1
Rudolf	Rudolfa	k1gFnPc2
Mosse	Mosse	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1932	#num#	k4
<g/>
,	,	kIx,
svazek	svazek	k1gInSc1
I	I	kA
<g/>
,	,	kIx,
strana	strana	k1gFnSc1
1575	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
a	a	k8xC
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Dopravní	dopravní	k2eAgInSc1d1
podnik	podnik	k1gInSc1
Kněžmost	Kněžmost	k1gFnSc1
s.	s.	k?
r.	r.	kA
<g />
.	.	kIx.
</s>
<s hack="1">
o.	o.	k?
Archivováno	archivován	k2eAgNnSc1d1
15	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
na	na	k7c6
webu	web	k1gInSc6
obce	obec	k1gFnSc2
<g/>
↑	↑	k?
Dopravní	dopravní	k2eAgInSc1d1
podnik	podnik	k1gInSc1
Kněžmost	Kněžmost	k1gFnSc1
<g/>
,	,	kIx,
na	na	k7c6
webu	web	k1gInSc6
ČNDS	ČNDS	kA
(	(	kIx(
<g/>
portál	portál	k1gInSc4
Českého	český	k2eAgNnSc2d1
nezávislého	závislý	k2eNgNnSc2d1
dopravního	dopravní	k2eAgNnSc2d1
sdružení	sdružení	k1gNnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Libor	Libor	k1gMnSc1
Hinčica	Hinčica	k1gMnSc1
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
,	,	kIx,
aktualizováno	aktualizován	k2eAgNnSc4d1
2006	#num#	k4
<g/>
↑	↑	k?
Portál	portál	k1gInSc1
CIS	cis	k1gNnSc2
o	o	k7c6
jízdních	jízdní	k2eAgInPc6d1
řádech	řád	k1gInPc6
<g/>
↑	↑	k?
Knižní	knižní	k2eAgInPc4d1
jízdní	jízdní	k2eAgInPc4d1
řády	řád	k1gInPc4
<g/>
.	.	kIx.
www.szdc.cz	www.szdc.cz	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Kněžmost	Kněžmost	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
Kněžmost	Kněžmost	k1gFnSc1
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Kněžmost	Kněžmost	k1gFnSc1
v	v	k7c6
Registru	registrum	k1gNnSc6
územní	územní	k2eAgFnSc2d1
identifikace	identifikace	k1gFnSc2
<g/>
,	,	kIx,
adres	adresa	k1gFnPc2
a	a	k8xC
nemovitostí	nemovitost	k1gFnPc2
(	(	kIx(
<g/>
RÚIAN	RÚIAN	kA
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Části	část	k1gFnPc1
obce	obec	k1gFnSc2
Kněžmost	Kněžmost	k1gFnSc1
Části	část	k1gFnSc2
obce	obec	k1gFnSc2
</s>
<s>
BýčinaČížovkaDrhlenyChlumínKněžmostKoprníkLítkoviceMalobratřiceNásedlniceSolecSolečekSrbskoSuhroviceÚhelniceŽantov	BýčinaČížovkaDrhlenyChlumínKněžmostKoprníkLítkoviceMalobratřiceNásedlniceSolecSolečekSrbskoSuhroviceÚhelniceŽantov	k1gInSc1
</s>
<s>
Města	město	k1gNnPc1
<g/>
,	,	kIx,
městyse	městys	k1gInPc1
a	a	k8xC
obce	obec	k1gFnPc1
okresu	okres	k1gInSc2
Mladá	mladá	k1gFnSc1
Boleslav	Boleslav	k1gMnSc1
</s>
<s>
Bakov	Bakov	k1gInSc1
nad	nad	k7c7
Jizerou	Jizera	k1gFnSc7
•	•	k?
Bělá	bělat	k5eAaImIp3nS
pod	pod	k7c7
Bezdězem	Bezděz	k1gInSc7
•	•	k?
Benátky	Benátky	k1gFnPc4
nad	nad	k7c7
Jizerou	Jizera	k1gFnSc7
•	•	k?
Bezno	Bezno	k6eAd1
•	•	k?
Bílá	bílý	k2eAgFnSc1d1
Hlína	hlína	k1gFnSc1
•	•	k?
Bítouchov	Bítouchov	k1gInSc1
•	•	k?
Boreč	Boreč	k1gInSc1
•	•	k?
Boseň	Boseň	k1gFnSc1
•	•	k?
Bradlec	Bradlec	k1gInSc1
•	•	k?
Branžež	Branžež	k1gFnSc1
•	•	k?
Brodce	Brodce	k1gMnSc1
•	•	k?
Březina	Březina	k1gMnSc1
•	•	k?
Březno	Březno	k1gNnSc4
•	•	k?
Březovice	Březovice	k1gFnSc1
•	•	k?
Bukovno	Bukovna	k1gFnSc5
•	•	k?
Ctiměřice	Ctiměřice	k1gFnSc2
•	•	k?
Čachovice	Čachovice	k1gFnSc2
•	•	k?
Čistá	čistý	k2eAgFnSc1d1
•	•	k?
Dalovice	Dalovice	k1gFnSc1
•	•	k?
Dlouhá	dlouhý	k2eAgFnSc1d1
Lhota	Lhota	k1gFnSc1
•	•	k?
Dobrovice	Dobrovice	k1gFnSc2
•	•	k?
Dobšín	Dobšín	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Dolní	dolní	k2eAgInSc4d1
Bousov	Bousov	k1gInSc4
•	•	k?
Dolní	dolní	k2eAgFnSc1d1
Krupá	Krupý	k2eAgFnSc1d1
•	•	k?
Dolní	dolní	k2eAgNnSc1d1
Slivno	Slivno	k1gNnSc1
•	•	k?
Dolní	dolní	k2eAgFnSc2d1
Stakory	Stakora	k1gFnSc2
•	•	k?
Domousnice	Domousnice	k1gFnSc2
•	•	k?
Doubravička	doubravička	k1gFnSc1
•	•	k?
Horky	horka	k1gFnSc2
nad	nad	k7c7
Jizerou	Jizera	k1gFnSc7
•	•	k?
Horní	horní	k2eAgFnSc1d1
Bukovina	Bukovina	k1gFnSc1
•	•	k?
Horní	horní	k2eAgNnSc4d1
Slivno	Slivno	k1gNnSc4
•	•	k?
Hrdlořezy	Hrdlořezy	k1gInPc1
•	•	k?
Hrušov	Hrušov	k1gInSc1
•	•	k?
Husí	husa	k1gFnPc2
Lhota	Lhota	k1gFnSc1
•	•	k?
Charvatce	Charvatec	k1gMnSc2
•	•	k?
Chocnějovice	Chocnějovice	k1gFnSc2
•	•	k?
Chotětov	Chotětov	k1gInSc1
•	•	k?
Chudíř	Chudíř	k1gInSc1
•	•	k?
Jabkenice	Jabkenice	k1gFnSc2
•	•	k?
Jivina	Jivina	k1gMnSc1
•	•	k?
Jizerní	Jizerní	k2eAgNnSc1d1
Vtelno	Vtelno	k1gNnSc1
•	•	k?
Josefův	Josefův	k2eAgInSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
Důl	důl	k1gInSc1
•	•	k?
Katusice	Katusice	k1gFnSc2
•	•	k?
Klášter	klášter	k1gInSc1
Hradiště	Hradiště	k1gNnSc2
nad	nad	k7c7
Jizerou	Jizera	k1gFnSc7
•	•	k?
Kluky	kluk	k1gMnPc4
•	•	k?
Kněžmost	Kněžmost	k1gFnSc1
•	•	k?
Kobylnice	Kobylnice	k1gFnPc4
•	•	k?
Kochánky	kochánek	k1gMnPc4
•	•	k?
Kolomuty	Kolomuty	k?
•	•	k?
Koryta	koryto	k1gNnSc2
•	•	k?
Kosmonosy	Kosmonosy	k1gInPc1
•	•	k?
Kosořice	Kosořice	k1gFnSc2
•	•	k?
Košátky	Košátka	k1gFnSc2
•	•	k?
Kováň	Kováň	k1gFnSc1
•	•	k?
Kovanec	Kovanec	k1gInSc1
•	•	k?
Krásná	krásný	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
•	•	k?
Krnsko	Krnsko	k1gNnSc1
•	•	k?
Kropáčova	Kropáčův	k2eAgFnSc1d1
Vrutice	Vrutice	k1gFnSc1
•	•	k?
Ledce	Ledce	k1gFnSc2
•	•	k?
Lhotky	Lhotka	k1gFnSc2
•	•	k?
Lipník	Lipník	k1gInSc1
•	•	k?
Loukov	Loukov	k1gInSc1
•	•	k?
Loukovec	Loukovec	k1gInSc1
•	•	k?
Luštěnice	Luštěnice	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Mečeříž	Mečeříž	k1gFnSc1
•	•	k?
Mladá	mladá	k1gFnSc1
Boleslav	Boleslav	k1gMnSc1
•	•	k?
Mnichovo	mnichův	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
•	•	k?
Mohelnice	Mohelnice	k1gFnSc1
nad	nad	k7c7
Jizerou	Jizera	k1gFnSc7
•	•	k?
Mukařov	Mukařov	k1gInSc4
•	•	k?
Němčice	Němčice	k1gFnSc2
•	•	k?
Nemyslovice	Nemyslovice	k1gFnSc2
•	•	k?
Nepřevázka	Nepřevázka	k1gFnSc1
•	•	k?
Neveklovice	Neveklovice	k1gFnSc2
•	•	k?
Niměřice	Niměřice	k1gFnSc2
•	•	k?
Nová	Nová	k1gFnSc1
Telib	Telib	k1gInSc1
•	•	k?
Nová	nový	k2eAgFnSc1d1
Ves	ves	k1gFnSc1
u	u	k7c2
Bakova	Bakov	k1gInSc2
•	•	k?
Obrubce	obrubka	k1gFnSc6
•	•	k?
Obruby	obruba	k1gFnSc2
•	•	k?
Pěčice	Pěčice	k1gFnSc2
•	•	k?
Pětikozly	Pětikozly	k1gMnSc1
•	•	k?
Petkovy	Petkov	k1gInPc1
•	•	k?
Písková	pískový	k2eAgFnSc1d1
Lhota	Lhota	k1gFnSc1
•	•	k?
Plazy	plaz	k1gInPc1
•	•	k?
Plužná	plužný	k2eAgFnSc1d1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Prodašice	Prodašice	k1gFnSc1
•	•	k?
Předměřice	Předměřice	k1gFnSc1
nad	nad	k7c7
Jizerou	Jizera	k1gFnSc7
•	•	k?
Přepeře	Přepera	k1gFnSc3
•	•	k?
Ptýrov	Ptýrov	k1gInSc1
•	•	k?
Rabakov	Rabakov	k1gInSc1
•	•	k?
Rohatsko	Rohatsko	k1gNnSc1
•	•	k?
Rokytá	Rokytý	k2eAgFnSc1d1
•	•	k?
Rokytovec	Rokytovec	k1gInSc1
•	•	k?
Řepov	Řepov	k1gInSc1
•	•	k?
Řitonice	Řitonice	k1gFnSc2
•	•	k?
Sedlec	Sedlec	k1gInSc1
•	•	k?
Semčice	Semčice	k1gFnSc2
•	•	k?
Sezemice	Sezemika	k1gFnSc3
•	•	k?
Skalsko	Skalsko	k1gNnSc4
•	•	k?
Skorkov	Skorkov	k1gInSc1
•	•	k?
Smilovice	Smilovice	k1gFnSc2
•	•	k?
Sojovice	Sojovice	k1gFnSc2
•	•	k?
Sovínky	Sovínka	k1gFnSc2
•	•	k?
Strašnov	Strašnov	k1gInSc1
•	•	k?
Strážiště	strážiště	k1gNnSc2
•	•	k?
Strenice	Strenice	k1gFnSc1
•	•	k?
Sudoměř	Sudoměř	k1gFnSc1
•	•	k?
Sukorady	Sukorada	k1gFnPc4
•	•	k?
Tuřice	tuřice	k1gFnPc4
•	•	k?
Ujkovice	Ujkovice	k1gFnSc2
•	•	k?
Velké	velký	k2eAgInPc1d1
Všelisy	Všelis	k1gInPc1
•	•	k?
Veselice	veselice	k1gFnSc2
•	•	k?
Vinařice	Vinařice	k1gFnSc2
•	•	k?
Vinec	Vinec	k1gMnSc1
•	•	k?
Vlkava	Vlkava	k1gFnSc1
•	•	k?
Vrátno	Vrátno	k6eAd1
•	•	k?
Všejany	Všejan	k1gInPc1
•	•	k?
Zdětín	Zdětín	k1gInSc1
•	•	k?
Žďár	Žďár	k1gInSc1
•	•	k?
Žerčice	Žerčice	k1gFnSc2
•	•	k?
Židněves	Židněves	k1gInSc1
legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
město	město	k1gNnSc1
<g/>
,	,	kIx,
městys	městys	k1gInSc1
.	.	kIx.
</s>
