<s>
Buenos	Buenos	k1gMnSc1	Buenos
Aires	Aires	k1gMnSc1	Aires
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
východě	východ	k1gInSc6	východ
země	zem	k1gFnSc2	zem
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejdůležitější	důležitý	k2eAgInSc1d3	nejdůležitější
argentinský	argentinský	k2eAgInSc1d1	argentinský
přístav	přístav	k1gInSc1	přístav
<g/>
,	,	kIx,	,
při	při	k7c6	při
společném	společný	k2eAgNnSc6d1	společné
ústí	ústí	k1gNnSc6	ústí
řek	řeka	k1gFnPc2	řeka
Uruguay	Uruguay	k1gFnSc1	Uruguay
a	a	k8xC	a
Paraná	Paraná	k1gFnSc1	Paraná
<g/>
.	.	kIx.	.
</s>
