<s>
Pyrantel	Pyrantel	k1gInSc1	Pyrantel
(	(	kIx(	(
<g/>
pyrantelum	pyrantelum	k1gInSc1	pyrantelum
pamoatum	pamoatum	k1gNnSc1	pamoatum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
léčivá	léčivý	k2eAgFnSc1d1	léčivá
látka	látka	k1gFnSc1	látka
patřící	patřící	k2eAgFnSc1d1	patřící
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
pyrimidinů	pyrimidin	k1gInPc2	pyrimidin
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
anthelmintikum	anthelmintikum	k1gNnSc1	anthelmintikum
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
účinný	účinný	k2eAgInSc4d1	účinný
proti	proti	k7c3	proti
střevním	střevní	k2eAgFnPc3d1	střevní
hlísticím	hlístice	k1gFnPc3	hlístice
rodů	rod	k1gInPc2	rod
Toxocara	Toxocar	k1gMnSc2	Toxocar
<g/>
,	,	kIx,	,
Toxascaris	Toxascaris	k1gFnSc2	Toxascaris
<g/>
,	,	kIx,	,
Ancylostoma	Ancylostomum	k1gNnSc2	Ancylostomum
a	a	k8xC	a
Uncinaria	Uncinarium	k1gNnSc2	Uncinarium
<g/>
.	.	kIx.	.
</s>
<s>
Působí	působit	k5eAaImIp3nS	působit
především	především	k9	především
na	na	k7c4	na
dospělé	dospělí	k1gMnPc4	dospělí
a	a	k8xC	a
nebo	nebo	k8xC	nebo
juvenilní	juvenilní	k2eAgMnPc4d1	juvenilní
červy	červ	k1gMnPc4	červ
ve	v	k7c6	v
střevě	střevo	k1gNnSc6	střevo
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
nepůsobí	působit	k5eNaImIp3nS	působit
na	na	k7c4	na
migrující	migrující	k2eAgFnPc4d1	migrující
larvy	larva	k1gFnPc4	larva
<g/>
.	.	kIx.	.
</s>
<s>
Mechanismus	mechanismus	k1gInSc1	mechanismus
účinku	účinek	k1gInSc2	účinek
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
váže	vázat	k5eAaImIp3nS	vázat
na	na	k7c4	na
nervové	nervový	k2eAgInPc4d1	nervový
receptory	receptor	k1gInPc4	receptor
hlístic	hlístice	k1gFnPc2	hlístice
a	a	k8xC	a
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
tak	tak	k9	tak
jejich	jejich	k3xOp3gFnSc4	jejich
spastickou	spastický	k2eAgFnSc4d1	spastická
paralýzu	paralýza	k1gFnSc4	paralýza
(	(	kIx(	(
<g/>
ochromení	ochromení	k1gNnSc2	ochromení
<g/>
)	)	kIx)	)
a	a	k8xC	a
následné	následný	k2eAgNnSc1d1	následné
samovolné	samovolný	k2eAgNnSc1d1	samovolné
vypuzení	vypuzení	k1gNnSc1	vypuzení
střevem	střevo	k1gNnSc7	střevo
z	z	k7c2	z
hostitele	hostitel	k1gMnSc2	hostitel
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
samostatně	samostatně	k6eAd1	samostatně
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Banminth	Banminth	k1gInSc1	Banminth
pasta	pasta	k1gFnSc1	pasta
<g/>
®	®	k?	®
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
častěji	často	k6eAd2	často
v	v	k7c6	v
kombinaci	kombinace	k1gFnSc6	kombinace
s	s	k7c7	s
jiným	jiný	k2eAgMnSc7d1	jiný
anthelmintikem	anthelmintik	k1gMnSc7	anthelmintik
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Drontal	Drontal	k1gMnSc1	Drontal
Plus	plus	k1gInSc1	plus
<g/>
®	®	k?	®
<g/>
,	,	kIx,	,
Helm-Ex	Helm-Ex	k1gInSc1	Helm-Ex
<g/>
®	®	k?	®
<g/>
,	,	kIx,	,
Caniverm	Caniverm	k1gInSc1	Caniverm
<g/>
®	®	k?	®
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
