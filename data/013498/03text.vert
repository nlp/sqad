<s>
John	John	k1gMnSc1
Flanagan	Flanagan	k1gMnSc1
</s>
<s>
John	John	k1gMnSc1
Flanagan	Flanagan	k1gMnSc1
John	John	k1gMnSc1
Flanagan	Flanagan	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
Narození	narození	k1gNnPc2
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1944	#num#	k4
(	(	kIx(
<g/>
76	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Sydney	Sydney	k1gNnPc2
<g/>
,	,	kIx,
Austrálie	Austrálie	k1gFnSc1
Austrálie	Austrálie	k1gFnSc2
Povolání	povolání	k1gNnSc2
</s>
<s>
spisovatel	spisovatel	k1gMnSc1
Národnost	národnost	k1gFnSc1
</s>
<s>
australská	australský	k2eAgFnSc1d1
Žánr	žánr	k1gInSc1
</s>
<s>
Fantasy	fantas	k1gInPc4
literatura	literatura	k1gFnSc1
pro	pro	k7c4
děti	dítě	k1gFnPc4
a	a	k8xC
mládež	mládež	k1gFnSc1
Významná	významný	k2eAgFnSc1d1
díla	dílo	k1gNnPc4
</s>
<s>
Hraničářův	hraničářův	k2eAgInSc1d1
učeňBratrstvo	učeňBratrstvo	k1gNnSc1
Partnerka	partnerka	k1gFnSc1
</s>
<s>
Leonie	Leonie	k1gFnPc4
Flanagan	Flanagana	k1gFnPc2
Děti	dítě	k1gFnPc4
</s>
<s>
Michael	Michael	k1gMnSc1
FlanaganKitty	FlanaganKitta	k1gMnSc2
FlanaganPenny	FlanaganPenna	k1gFnSc2
Flanagan	Flanagan	k1gMnSc1
</s>
<s>
oficiální	oficiální	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
John	John	k1gMnSc1
Flanagan	Flanagan	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
22	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1944	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
australský	australský	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
zejména	zejména	k9
fantasy	fantasy	k1gInPc7
<g/>
,	,	kIx,
jeho	jeho	k3xOp3gNnSc1
nejznámější	známý	k2eAgNnSc1d3
dílo	dílo	k1gNnSc1
je	být	k5eAaImIp3nS
dobrodružná	dobrodružný	k2eAgFnSc1d1
série	série	k1gFnPc1
Hraničářův	hraničářův	k2eAgMnSc1d1
učeň	učeň	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Životopis	životopis	k1gInSc1
</s>
<s>
John	John	k1gMnSc1
Flanagan	Flanagan	k1gMnSc1
zahájil	zahájit	k5eAaPmAgMnS
svoji	svůj	k3xOyFgFnSc4
pracovní	pracovní	k2eAgFnSc4d1
dráhu	dráha	k1gFnSc4
jako	jako	k8xC,k8xS
reklamní	reklamní	k2eAgMnSc1d1
autor	autor	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pracoval	pracovat	k5eAaImAgMnS
v	v	k7c6
agenturách	agentura	k1gFnPc6
v	v	k7c6
Londýně	Londýn	k1gInSc6
<g/>
,	,	kIx,
Sydney	Sydney	k1gNnSc6
a	a	k8xC
Singapuru	Singapur	k1gInSc6
<g/>
,	,	kIx,
potom	potom	k6eAd1
přešel	přejít	k5eAaPmAgInS
na	na	k7c4
volnou	volný	k2eAgFnSc4d1
nohu	noha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začal	začít	k5eAaPmAgMnS
psát	psát	k5eAaImF
televizní	televizní	k2eAgInPc4d1
scénáře	scénář	k1gInPc4
a	a	k8xC
byl	být	k5eAaImAgInS
hlavním	hlavní	k2eAgMnSc7d1
autorem	autor	k1gMnSc7
australské	australský	k2eAgFnSc2d1
nejdéle	dlouho	k6eAd3
vysílané	vysílaný	k2eAgFnSc2d1
situační	situační	k2eAgFnSc2d1
komedie	komedie	k1gFnSc2
Haló	haló	k0
<g/>
,	,	kIx,
tati	tati	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s>
Během	během	k7c2
pestré	pestrý	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
psal	psát	k5eAaImAgMnS
John	John	k1gMnSc1
reklamní	reklamní	k2eAgInPc4d1
slogany	slogan	k1gInPc4
a	a	k8xC
scénáře	scénář	k1gInPc4
<g/>
,	,	kIx,
komerční	komerční	k2eAgFnSc2d1
prezentace	prezentace	k1gFnSc2
<g/>
,	,	kIx,
scénáře	scénář	k1gInSc2
zábavných	zábavný	k2eAgInPc2d1
pořadů	pořad	k1gInPc2
a	a	k8xC
jednou	jednou	k6eAd1
dokonce	dokonce	k9
i	i	k9
projev	projev	k1gInSc4
guvernéra	guvernér	k1gMnSc2
státu	stát	k1gInSc2
Nový	nový	k2eAgInSc1d1
Jižní	jižní	k2eAgInSc1d1
Wales	Wales	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Sérii	série	k1gFnSc4
Hraničářův	hraničářův	k2eAgMnSc1d1
učeň	učeň	k1gMnSc1
začal	začít	k5eAaPmAgMnS
psát	psát	k5eAaImF
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
svého	svůj	k3xOyFgMnSc4
dvanáctiletého	dvanáctiletý	k2eAgMnSc4d1
syna	syn	k1gMnSc4
Michaela	Michael	k1gMnSc4
přilákal	přilákat	k5eAaPmAgInS
ke	k	k7c3
čtení	čtení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původně	původně	k6eAd1
obsahovala	obsahovat	k5eAaImAgFnS
dvacet	dvacet	k4xCc4
povídek	povídka	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
John	John	k1gMnSc1
později	pozdě	k6eAd2
přepracoval	přepracovat	k5eAaPmAgMnS
na	na	k7c4
první	první	k4xOgInSc4
díl	díl	k1gInSc4
Hraničářova	hraničářův	k2eAgMnSc2d1
učně	učeň	k1gMnSc2
s	s	k7c7
názvem	název	k1gInSc7
Rozvaliny	rozvalina	k1gFnSc2
Gorlanu	Gorlan	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brzy	brzy	k6eAd1
následovaly	následovat	k5eAaImAgInP
další	další	k2eAgInPc1d1
díly	díl	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc1
syn	syn	k1gMnSc1
<g/>
,	,	kIx,
nyní	nyní	k6eAd1
třicetiletý	třicetiletý	k2eAgMnSc1d1
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
nadšeným	nadšený	k2eAgMnSc7d1
čtenářem	čtenář	k1gMnSc7
všech	všecek	k3xTgFnPc2
těchto	tento	k3xDgFnPc2
knih	kniha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
John	John	k1gMnSc1
žije	žít	k5eAaImIp3nS
se	s	k7c7
svou	svůj	k3xOyFgFnSc7
ženou	žena	k1gFnSc7
Leonií	Leonie	k1gFnSc7
v	v	k7c4
Manly	Manla	k1gFnPc4
<g/>
,	,	kIx,
přímořském	přímořský	k2eAgNnSc6d1
předměstí	předměstí	k1gNnSc6
Sydney	Sydney	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
píše	psát	k5eAaImIp3nS
nové	nový	k2eAgInPc4d1
díly	díl	k1gInPc4
série	série	k1gFnSc2
Bratrstvo	bratrstvo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Co	co	k9
nejdříve	dříve	k6eAd3
by	by	kYmCp3nS
se	se	k3xPyFc4
měl	mít	k5eAaImAgInS
o	o	k7c4
sérii	série	k1gFnSc4
Hraničářův	hraničářův	k2eAgMnSc1d1
učeň	učeň	k1gMnSc1
natáčet	natáčet	k5eAaImF
už	už	k6eAd1
dlouho	dlouho	k6eAd1
odkládaný	odkládaný	k2eAgInSc4d1
celovečerní	celovečerní	k2eAgInSc4d1
film	film	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Díla	dílo	k1gNnPc1
</s>
<s>
Hraničářův	hraničářův	k2eAgMnSc1d1
učeň	učeň	k1gMnSc1
</s>
<s>
Rozvaliny	rozvalina	k1gFnPc1
Gorlanu	Gorlan	k1gInSc2
</s>
<s>
Hořící	hořící	k2eAgInSc1d1
most	most	k1gInSc1
</s>
<s>
Ledová	ledový	k2eAgFnSc1d1
země	země	k1gFnSc1
</s>
<s>
Nositelé	nositel	k1gMnPc1
dubového	dubový	k2eAgInSc2d1
listu	list	k1gInSc2
</s>
<s>
Výkupné	výkupné	k1gNnSc1
za	za	k7c4
Eraka	Eraek	k1gMnSc4
</s>
<s>
Čaroděj	čaroděj	k1gMnSc1
na	na	k7c6
severu	sever	k1gInSc6
</s>
<s>
Obléhání	obléhání	k1gNnSc1
Macindawu	Macindawus	k1gInSc2
</s>
<s>
Králové	Král	k1gMnPc1
Clonmelu	Clonmel	k1gInSc2
</s>
<s>
Halt	halt	k0
v	v	k7c6
nebezpečí	nebezpečí	k1gNnSc1
</s>
<s>
Císař	Císař	k1gMnSc1
Nihon-džinu	Nihon-džina	k1gFnSc4
</s>
<s>
Ztracené	ztracený	k2eAgInPc4d1
příběhy	příběh	k1gInPc4
</s>
<s>
Hraničářův	hraničářův	k2eAgMnSc1d1
učeň	učeň	k1gMnSc1
–	–	k?
První	první	k4xOgInPc4
roky	rok	k1gInPc4
</s>
<s>
Turnaj	turnaj	k1gInSc1
na	na	k7c4
Gorlanu	Gorlana	k1gFnSc4
</s>
<s>
Bitva	bitva	k1gFnSc1
na	na	k7c6
Hackhamské	Hackhamský	k2eAgFnSc6d1
pláni	pláň	k1gFnSc6
</s>
<s>
Hraničářův	hraničářův	k2eAgMnSc1d1
učeň	učeň	k1gMnSc1
–	–	k?
Královská	královský	k2eAgFnSc1d1
hraničářka	hraničářka	k1gFnSc1
</s>
<s>
Královská	královský	k2eAgFnSc1d1
hraničářka	hraničářka	k1gFnSc1
</s>
<s>
Klan	klan	k1gInSc1
Rudé	rudý	k2eAgFnSc2d1
lišky	liška	k1gFnSc2
</s>
<s>
Souboj	souboj	k1gInSc1
na	na	k7c4
Araluenu	Araluena	k1gFnSc4
</s>
<s>
Ztracený	ztracený	k2eAgMnSc1d1
princ	princ	k1gMnSc1
</s>
<s>
Bratrstvo	bratrstvo	k1gNnSc1
</s>
<s>
(	(	kIx(
<g/>
příběhy	příběh	k1gInPc4
Skandijců	Skandijce	k1gMnPc2
ze	z	k7c2
světa	svět	k1gInSc2
Hraničářova	hraničářův	k2eAgMnSc2d1
učně	učeň	k1gMnSc2
<g/>
)	)	kIx)
</s>
<s>
Vyděděnci	vyděděnec	k1gMnPc1
</s>
<s>
Nájezdníci	nájezdník	k1gMnPc1
</s>
<s>
Lovci	lovec	k1gMnPc1
</s>
<s>
Otroci	otrok	k1gMnPc1
ze	z	k7c2
Sokora	Sokor	k1gMnSc2
</s>
<s>
Hora	hora	k1gFnSc1
Štírů	štír	k1gMnPc2
</s>
<s>
Umrlčí	umrlčí	k2eAgFnPc1d1
tváře	tvář	k1gFnPc1
</s>
<s>
Kaldera	Kaldera	k1gFnSc1
</s>
<s>
Návrat	návrat	k1gInSc1
Temudžajů	Temudžaj	k1gMnPc2
</s>
<s>
Jesse	Jess	k1gMnSc4
Parker	Parker	k1gMnSc1
</s>
<s>
Storm	Storm	k1gInSc1
Peak	Peaka	k1gFnPc2
</s>
<s>
Avalanche	Avalanche	k1gFnSc1
Pass	Passa	k1gFnPc2
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
The	The	k1gMnSc1
Ranger	Ranger	k1gMnSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
Apprentice	Apprentice	k1gFnSc5
<g/>
,	,	kIx,
starseeker	starseeker	k1gMnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
John	John	k1gMnSc1
Flanagan	Flanagany	k1gInPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
John	John	k1gMnSc1
Flanagan	Flanagan	k1gMnSc1
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
Ranger	Rangra	k1gFnPc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Apprentice	Apprentice	k1gFnPc5
</s>
<s>
Webová	webový	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
Johna	John	k1gMnSc2
Flanagana	Flanagan	k1gMnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
John	John	k1gMnSc1
Flanagan	Flanagan	k1gMnSc1
Hraničářův	hraničářův	k2eAgMnSc1d1
učeň	učeň	k1gMnSc1
</s>
<s>
Rozvaliny	rozvalina	k1gFnPc1
Gorlanu	Gorlan	k1gInSc2
•	•	k?
Hořící	hořící	k2eAgInSc1d1
most	most	k1gInSc1
•	•	k?
Ledová	ledový	k2eAgFnSc1d1
země	země	k1gFnSc1
•	•	k?
Nositelé	nositel	k1gMnPc1
dubového	dubový	k2eAgInSc2d1
listu	list	k1gInSc2
•	•	k?
Výkupné	výkupný	k2eAgNnSc1d1
za	za	k7c2
Eraka	Eraek	k1gInSc2
•	•	k?
Čaroděj	čaroděj	k1gMnSc1
na	na	k7c6
severu	sever	k1gInSc6
•	•	k?
Obléhání	obléhání	k1gNnSc6
Macindawu	Macindawus	k1gInSc2
•	•	k?
Králové	Králové	k2eAgInSc2d1
Clonmelu	Clonmel	k1gInSc2
•	•	k?
Halt	halt	k0
v	v	k7c6
nebezpečí	nebezpečí	k1gNnPc2
•	•	k?
Císař	Císař	k1gMnSc1
Nihon-džinu	Nihon-džina	k1gFnSc4
•	•	k?
Ztracené	ztracený	k2eAgInPc4d1
příběhy	příběh	k1gInPc4
Hraničářův	hraničářův	k2eAgMnSc1d1
učeň	učeň	k1gMnSc1
–	–	k?
První	první	k4xOgInPc4
roky	rok	k1gInPc4
</s>
<s>
Turnaj	turnaj	k1gInSc1
na	na	k7c4
Gorlanu	Gorlana	k1gFnSc4
•	•	k?
Bitva	bitva	k1gFnSc1
na	na	k7c6
Hackhamské	Hackhamský	k2eAgFnSc6d1
pláni	pláň	k1gFnSc6
Hraničářův	hraničářův	k2eAgMnSc1d1
učeň	učeň	k1gMnSc1
–	–	k?
Královská	královský	k2eAgFnSc1d1
hraničářka	hraničářka	k1gFnSc1
</s>
<s>
Královská	královský	k2eAgFnSc1d1
hraničářka	hraničářka	k1gFnSc1
•	•	k?
Klan	klan	k1gInSc4
Rudé	rudý	k2eAgFnSc2d1
lišky	liška	k1gFnSc2
•	•	k?
Souboj	souboj	k1gInSc1
na	na	k7c4
Araluenu	Araluena	k1gFnSc4
•	•	k?
Ztracený	ztracený	k2eAgMnSc1d1
princ	princ	k1gMnSc1
Bratrstvo	bratrstvo	k1gNnSc1
</s>
<s>
Vyděděnci	vyděděnec	k1gMnPc1
•	•	k?
Nájezdníci	nájezdník	k1gMnPc1
•	•	k?
Lovci	lovec	k1gMnSc3
•	•	k?
Otroci	otrok	k1gMnPc1
ze	z	k7c2
Sokora	Sokor	k1gMnSc2
•	•	k?
Hora	Hora	k1gMnSc1
Štírů	štír	k1gMnPc2
•	•	k?
Umrlčí	umrlčí	k2eAgFnPc1d1
tváře	tvář	k1gFnPc1
•	•	k?
Kaldera	Kaldera	k1gFnSc1
•	•	k?
Návrat	návrat	k1gInSc4
Temudžajů	Temudžaj	k1gMnPc2
Jesse	Jesse	k1gFnSc2
Parker	Parker	k1gMnSc1
</s>
<s>
Storm	Storm	k1gInSc1
Peak	Peak	k1gInSc1
•	•	k?
Avalanche	Avalanche	k1gInSc1
Pass	Pass	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
81982	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
132422573	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
1450	#num#	k4
7820	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
2009049211	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
85841451	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
2009049211	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Literatura	literatura	k1gFnSc1
</s>
