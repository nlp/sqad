<p>
<s>
Blizna	blizna	k1gFnSc1	blizna
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
stigma	stigma	k1gNnSc1	stigma
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
část	část	k1gFnSc1	část
pestíku	pestík	k1gInSc2	pestík
v	v	k7c6	v
květech	květ	k1gInPc6	květ
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Vyrůstá	vyrůstat	k5eAaImIp3nS	vyrůstat
z	z	k7c2	z
čnělky	čnělka	k1gFnSc2	čnělka
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
chloupkatá	chloupkatý	k2eAgFnSc1d1	chloupkatá
nebo	nebo	k8xC	nebo
žláznatá	žláznatý	k2eAgFnSc1d1	žláznatá
<g/>
.	.	kIx.	.
</s>
<s>
Slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
zachytávání	zachytávání	k1gNnSc3	zachytávání
pylových	pylový	k2eAgNnPc2d1	pylové
zrn	zrno	k1gNnPc2	zrno
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
pak	pak	k6eAd1	pak
mohou	moct	k5eAaImIp3nP	moct
začít	začít	k5eAaPmF	začít
prorůstat	prorůstat	k5eAaImF	prorůstat
čnělkou	čnělka	k1gFnSc7	čnělka
do	do	k7c2	do
semeníku	semeník	k1gInSc2	semeník
a	a	k8xC	a
oplodnit	oplodnit	k5eAaPmF	oplodnit
vajíčka	vajíčko	k1gNnPc4	vajíčko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
blizna	blizna	k1gFnSc1	blizna
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
blizna	blizna	k1gFnSc1	blizna
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
