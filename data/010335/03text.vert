<p>
<s>
Vrbovka	vrbovka	k1gFnSc1	vrbovka
růžová	růžový	k2eAgFnSc1d1	růžová
(	(	kIx(	(
<g/>
Epilobium	Epilobium	k1gNnSc1	Epilobium
roseum	roseum	k1gNnSc1	roseum
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc1	druh
rostliny	rostlina	k1gFnSc2	rostlina
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
pupalkovité	pupalkovitý	k2eAgFnSc2d1	pupalkovitý
(	(	kIx(	(
<g/>
Onagraceae	Onagracea	k1gFnSc2	Onagracea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
vytrvalou	vytrvalý	k2eAgFnSc4d1	vytrvalá
rostlinu	rostlina	k1gFnSc4	rostlina
dosahující	dosahující	k2eAgFnSc2d1	dosahující
výšky	výška	k1gFnSc2	výška
nejčastěji	často	k6eAd3	často
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
90	[number]	k4	90
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Oddenky	oddenek	k1gInPc1	oddenek
jsou	být	k5eAaImIp3nP	být
poměrně	poměrně	k6eAd1	poměrně
krátké	krátký	k2eAgFnPc1d1	krátká
<g/>
,	,	kIx,	,
koncem	koncem	k7c2	koncem
léta	léto	k1gNnSc2	léto
se	se	k3xPyFc4	se
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
turiony	turion	k1gInPc1	turion
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
později	pozdě	k6eAd2	pozdě
vystupují	vystupovat	k5eAaImIp3nP	vystupovat
na	na	k7c4	na
povrch	povrch	k1gInSc4	povrch
<g/>
,	,	kIx,	,
zezelenají	zezelenat	k5eAaPmIp3nP	zezelenat
a	a	k8xC	a
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
se	se	k3xPyFc4	se
v	v	k7c4	v
malé	malý	k2eAgFnPc4d1	malá
listové	listový	k2eAgFnPc4d1	listová
růžice	růžice	k1gFnPc4	růžice
<g/>
.	.	kIx.	.
</s>
<s>
Lodyha	lodyha	k1gFnSc1	lodyha
bohatě	bohatě	k6eAd1	bohatě
větvená	větvený	k2eAgFnSc1d1	větvená
(	(	kIx(	(
<g/>
řidčeji	řídce	k6eAd2	řídce
skoro	skoro	k6eAd1	skoro
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dole	dole	k6eAd1	dole
lysá	lysat	k5eAaImIp3nS	lysat
nebo	nebo	k8xC	nebo
skoro	skoro	k6eAd1	skoro
lysá	lysat	k5eAaImIp3nS	lysat
<g/>
,	,	kIx,	,
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
přitiskle	přitiskle	k6eAd1	přitiskle
pýřitá	pýřitý	k2eAgFnSc1d1	pýřitá
s	s	k7c7	s
odstálými	odstálý	k2eAgInPc7d1	odstálý
žláznatými	žláznatý	k2eAgInPc7d1	žláznatý
chlupy	chlup	k1gInPc7	chlup
<g/>
,	,	kIx,	,
od	od	k7c2	od
listů	list	k1gInPc2	list
sbíhají	sbíhat	k5eAaImIp3nP	sbíhat
2	[number]	k4	2
úzké	úzký	k2eAgFnPc1d1	úzká
linie	linie	k1gFnPc1	linie
<g/>
.	.	kIx.	.
</s>
<s>
Dolní	dolní	k2eAgInPc1d1	dolní
listy	list	k1gInPc1	list
jsou	být	k5eAaImIp3nP	být
vstřícné	vstřícný	k2eAgInPc1d1	vstřícný
<g/>
,	,	kIx,	,
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
a	a	k8xC	a
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
pak	pak	k6eAd1	pak
střídavé	střídavý	k2eAgNnSc1d1	střídavé
<g/>
,	,	kIx,	,
relativně	relativně	k6eAd1	relativně
dlouze	dlouho	k6eAd1	dlouho
řapíkaté	řapíkatý	k2eAgFnPc1d1	řapíkatá
<g/>
,	,	kIx,	,
řapíky	řapík	k1gInPc1	řapík
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
mm	mm	kA	mm
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
<g/>
.	.	kIx.	.
</s>
<s>
Čepele	čepel	k1gFnPc1	čepel
jsou	být	k5eAaImIp3nP	být
nejčastěji	často	k6eAd3	často
kopinaté	kopinatý	k2eAgInPc1d1	kopinatý
až	až	k9	až
vejčitě	vejčitě	k6eAd1	vejčitě
kopinaté	kopinatý	k2eAgNnSc1d1	kopinaté
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
tmavě	tmavě	k6eAd1	tmavě
zelené	zelený	k2eAgInPc1d1	zelený
<g/>
,	,	kIx,	,
asi	asi	k9	asi
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
cm	cm	kA	cm
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
a	a	k8xC	a
1,5	[number]	k4	1,5
<g/>
–	–	k?	–
<g/>
3,5	[number]	k4	3,5
cm	cm	kA	cm
široké	široký	k2eAgFnPc1d1	široká
<g/>
,	,	kIx,	,
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
jemně	jemně	k6eAd1	jemně
pilovité	pilovitý	k2eAgInPc1d1	pilovitý
<g/>
,	,	kIx,	,
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
straně	strana	k1gFnSc6	strana
s	s	k7c7	s
10	[number]	k4	10
<g/>
–	–	k?	–
<g/>
70	[number]	k4	70
zuby	zub	k1gInPc7	zub
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
uspořádány	uspořádat	k5eAaPmNgInP	uspořádat
v	v	k7c6	v
květenstvích	květenství	k1gNnPc6	květenství
<g/>
,	,	kIx,	,
vrcholových	vrcholový	k2eAgInPc6d1	vrcholový
hroznech	hrozen	k1gInPc6	hrozen
a	a	k8xC	a
vyrůstají	vyrůstat	k5eAaImIp3nP	vyrůstat
z	z	k7c2	z
paždí	paždí	k1gNnSc2	paždí
listenu	listen	k1gInSc2	listen
<g/>
.	.	kIx.	.
</s>
<s>
Květy	květ	k1gInPc1	květ
jsou	být	k5eAaImIp3nP	být
čtyřčetné	čtyřčetný	k2eAgInPc1d1	čtyřčetný
<g/>
,	,	kIx,	,
kališní	kališní	k2eAgInPc1d1	kališní
lístky	lístek	k1gInPc1	lístek
jsou	být	k5eAaImIp3nP	být
4	[number]	k4	4
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
3	[number]	k4	3
<g/>
–	–	k?	–
<g/>
3,5	[number]	k4	3,5
mm	mm	kA	mm
dlouhé	dlouhý	k2eAgNnSc1d1	dlouhé
<g/>
.	.	kIx.	.
</s>
<s>
Korunní	korunní	k2eAgInPc1d1	korunní
lístky	lístek	k1gInPc1	lístek
jsou	být	k5eAaImIp3nP	být
taky	taky	k6eAd1	taky
4	[number]	k4	4
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
nejčastěji	často	k6eAd3	často
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
mm	mm	kA	mm
dlouhé	dlouhý	k2eAgFnPc1d1	dlouhá
<g/>
,	,	kIx,	,
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
mělce	mělce	k6eAd1	mělce
vykrojené	vykrojený	k2eAgInPc1d1	vykrojený
<g/>
,	,	kIx,	,
bělavé	bělavý	k2eAgInPc1d1	bělavý
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
bledě	bledě	k6eAd1	bledě
růžové	růžový	k2eAgInPc1d1	růžový
s	s	k7c7	s
tmavšími	tmavý	k2eAgFnPc7d2	tmavší
žilkami	žilka	k1gFnPc7	žilka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
kvete	kvést	k5eAaImIp3nS	kvést
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
až	až	k6eAd1	až
v	v	k7c6	v
září	září	k1gNnSc6	září
<g/>
.	.	kIx.	.
</s>
<s>
Tyčinek	tyčinka	k1gFnPc2	tyčinka
je	být	k5eAaImIp3nS	být
8	[number]	k4	8
ve	v	k7c6	v
2	[number]	k4	2
kruzích	kruh	k1gInPc6	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Semeník	semeník	k1gInSc1	semeník
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
4	[number]	k4	4
plodolistů	plodolist	k1gInPc2	plodolist
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
spodní	spodní	k2eAgNnSc1d1	spodní
<g/>
,	,	kIx,	,
čnělka	čnělka	k1gFnSc1	čnělka
je	být	k5eAaImIp3nS	být
přímá	přímý	k2eAgFnSc1d1	přímá
<g/>
,	,	kIx,	,
blizna	blizna	k1gFnSc1	blizna
je	být	k5eAaImIp3nS	být
celistvá	celistvý	k2eAgFnSc1d1	celistvá
<g/>
,	,	kIx,	,
kyjovitá	kyjovitý	k2eAgFnSc1d1	kyjovitá
<g/>
.	.	kIx.	.
</s>
<s>
Plodem	plod	k1gInSc7	plod
je	být	k5eAaImIp3nS	být
asi	asi	k9	asi
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
cm	cm	kA	cm
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
tobolka	tobolka	k1gFnSc1	tobolka
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
přitiskle	přitiskle	k6eAd1	přitiskle
pýřitá	pýřitý	k2eAgFnSc1d1	pýřitá
s	s	k7c7	s
odstálými	odstálý	k2eAgInPc7d1	odstálý
žláznatými	žláznatý	k2eAgInPc7d1	žláznatý
chlupy	chlup	k1gInPc7	chlup
<g/>
,	,	kIx,	,
v	v	k7c6	v
obrysu	obrys	k1gInSc6	obrys
čárkovitého	čárkovitý	k2eAgInSc2d1	čárkovitý
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
čtyřhranná	čtyřhranný	k2eAgFnSc1d1	čtyřhranná
a	a	k8xC	a
čtyřpouzdrá	čtyřpouzdrý	k2eAgFnSc1d1	čtyřpouzdrá
<g/>
,	,	kIx,	,
otvírá	otvírat	k5eAaImIp3nS	otvírat
se	s	k7c7	s
4	[number]	k4	4
chlopněmi	chlopeň	k1gFnPc7	chlopeň
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mnoho	mnoho	k4c4	mnoho
semen	semeno	k1gNnPc2	semeno
<g/>
.	.	kIx.	.
</s>
<s>
Semena	semeno	k1gNnPc1	semeno
jsou	být	k5eAaImIp3nP	být
cca	cca	kA	cca
0,9	[number]	k4	0,9
<g/>
–	–	k?	–
<g/>
1	[number]	k4	1
mm	mm	kA	mm
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
<g/>
,	,	kIx,	,
na	na	k7c6	na
vrcholu	vrchol	k1gInSc6	vrchol
s	s	k7c7	s
chmýrem	chmýr	k1gInSc7	chmýr
a	a	k8xC	a
osemení	osemení	k1gNnSc1	osemení
je	být	k5eAaImIp3nS	být
hustě	hustě	k6eAd1	hustě
papilnaté	papilnatý	k2eAgNnSc1d1	papilnatý
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
chromozómů	chromozóm	k1gInPc2	chromozóm
je	být	k5eAaImIp3nS	být
2	[number]	k4	2
<g/>
n	n	k0	n
<g/>
=	=	kIx~	=
<g/>
36	[number]	k4	36
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
==	==	k?	==
</s>
</p>
<p>
<s>
Vrbovka	vrbovka	k1gFnSc1	vrbovka
růžová	růžový	k2eAgFnSc1d1	růžová
je	být	k5eAaImIp3nS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
části	část	k1gFnSc6	část
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
na	na	k7c4	na
sever	sever	k1gInSc4	sever
až	až	k9	až
po	po	k7c6	po
jižní	jižní	k2eAgFnSc6d1	jižní
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
<g/>
,	,	kIx,	,
roste	růst	k5eAaImIp3nS	růst
i	i	k9	i
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
a	a	k8xC	a
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jen	jen	k9	jen
málo	málo	k6eAd1	málo
<g/>
,	,	kIx,	,
skoro	skoro	k6eAd1	skoro
chybí	chybět	k5eAaImIp3nS	chybět
na	na	k7c6	na
Pyrenejském	pyrenejský	k2eAgInSc6d1	pyrenejský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
východ	východ	k1gInSc4	východ
je	být	k5eAaImIp3nS	být
rozšířena	rozšířit	k5eAaPmNgFnS	rozšířit
i	i	k9	i
v	v	k7c6	v
evropské	evropský	k2eAgFnSc6d1	Evropská
části	část	k1gFnSc6	část
Ruska	Rusko	k1gNnSc2	Rusko
přibližně	přibližně	k6eAd1	přibližně
po	po	k7c4	po
řeku	řeka	k1gFnSc4	řeka
Don	dona	k1gFnPc2	dona
<g/>
,	,	kIx,	,
v	v	k7c6	v
Malé	Malé	k2eAgFnSc6d1	Malé
Asii	Asie	k1gFnSc6	Asie
na	na	k7c6	na
Kavkaze	Kavkaz	k1gInSc6	Kavkaz
<g/>
,	,	kIx,	,
další	další	k2eAgFnPc1d1	další
enklávy	enkláva	k1gFnPc1	enkláva
výskytu	výskyt	k1gInSc2	výskyt
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
dále	daleko	k6eAd2	daleko
v	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
včetně	včetně	k7c2	včetně
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Zavlečena	zavlečen	k2eAgFnSc1d1	zavlečena
do	do	k7c2	do
Severní	severní	k2eAgFnSc2d1	severní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnSc1	rozšíření
v	v	k7c6	v
ČR	ČR	kA	ČR
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
ČR	ČR	kA	ČR
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
roztroušeně	roztroušeně	k6eAd1	roztroušeně
od	od	k7c2	od
nížin	nížina	k1gFnPc2	nížina
do	do	k7c2	do
nižších	nízký	k2eAgFnPc2d2	nižší
horských	horský	k2eAgFnPc2d1	horská
poloh	poloha	k1gFnPc2	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastěji	často	k6eAd3	často
ji	on	k3xPp3gFnSc4	on
najdeme	najít	k5eAaPmIp1nP	najít
na	na	k7c6	na
rumištích	rumiště	k1gNnPc6	rumiště
<g/>
,	,	kIx,	,
podél	podél	k7c2	podél
struh	strouha	k1gFnPc2	strouha
a	a	k8xC	a
potoků	potok	k1gInPc2	potok
a	a	k8xC	a
v	v	k7c6	v
rákosinách	rákosina	k1gFnPc6	rákosina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Mapy	mapa	k1gFnPc1	mapa
florbase	florbase	k6eAd1	florbase
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
vrbovka	vrbovka	k1gFnSc1	vrbovka
růžová	růžový	k2eAgFnSc1d1	růžová
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
