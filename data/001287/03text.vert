<s>
Pýcha	pýcha	k1gFnSc1	pýcha
a	a	k8xC	a
předsudek	předsudek	k1gInSc1	předsudek
je	být	k5eAaImIp3nS	být
britsko-francouzský	britskorancouzský	k2eAgInSc1d1	britsko-francouzský
film	film	k1gInSc1	film
natočený	natočený	k2eAgInSc1d1	natočený
režisérem	režisér	k1gMnSc7	režisér
Joem	Joem	k1gMnSc1	Joem
Wrightem	Wright	k1gMnSc7	Wright
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
podle	podle	k7c2	podle
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
románu	román	k1gInSc2	román
Pýcha	pýcha	k1gFnSc1	pýcha
a	a	k8xC	a
předsudek	předsudek	k1gInSc1	předsudek
Jane	Jan	k1gMnSc5	Jan
Austenové	Austenové	k2eAgFnSc5d1	Austenové
<g/>
.	.	kIx.	.
</s>
<s>
Příběh	příběh	k1gInSc1	příběh
popisuje	popisovat	k5eAaImIp3nS	popisovat
dění	dění	k1gNnSc4	dění
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
Bennetových	Bennetový	k2eAgFnPc2d1	Bennetová
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
do	do	k7c2	do
nedalekého	daleký	k2eNgNnSc2d1	nedaleké
panství	panství	k1gNnSc2	panství
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
bohatý	bohatý	k2eAgMnSc1d1	bohatý
muž	muž	k1gMnSc1	muž
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
neméně	málo	k6eNd2	málo
movitým	movitý	k2eAgMnSc7d1	movitý
přítelem	přítel	k1gMnSc7	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Obzvlášť	obzvlášť	k6eAd1	obzvlášť
<g/>
,	,	kIx,	,
když	když	k8xS	když
všechny	všechen	k3xTgFnPc1	všechen
Bennetovy	Bennetův	k2eAgFnPc1d1	Bennetova
dcery	dcera	k1gFnPc1	dcera
mají	mít	k5eAaImIp3nP	mít
věk	věk	k1gInSc4	věk
na	na	k7c4	na
vdávání	vdávání	k1gNnSc4	vdávání
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
příběh	příběh	k1gInSc1	příběh
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
osud	osud	k1gInSc1	osud
manželů	manžel	k1gMnPc2	manžel
Bennetových	Bennetový	k2eAgFnPc2d1	Bennetová
a	a	k8xC	a
jejich	jejich	k3xOp3gFnPc2	jejich
pěti	pět	k4xCc3	pět
dcer	dcera	k1gFnPc2	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Paní	paní	k1gFnSc1	paní
Benettová	Benettová	k1gFnSc1	Benettová
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
domnění	domnění	k1gNnSc6	domnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
ženy	žena	k1gFnPc1	žena
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
šťastně	šťastně	k6eAd1	šťastně
jedině	jedině	k6eAd1	jedině
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
dobře	dobře	k6eAd1	dobře
vdají	vdát	k5eAaPmIp3nP	vdát
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
přesvědčení	přesvědčený	k2eAgMnPc1d1	přesvědčený
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nP	snažit
provdat	provdat	k5eAaPmF	provdat
všechny	všechen	k3xTgFnPc4	všechen
své	svůj	k3xOyFgFnPc4	svůj
dcery	dcera	k1gFnPc4	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Příležitost	příležitost	k1gFnSc4	příležitost
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
naskytá	naskytat	k5eAaImIp3nS	naskytat
<g/>
,	,	kIx,	,
když	když	k8xS	když
do	do	k7c2	do
vedlejšího	vedlejší	k2eAgNnSc2d1	vedlejší
panství	panství	k1gNnSc2	panství
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
zámožný	zámožný	k2eAgMnSc1d1	zámožný
a	a	k8xC	a
svobodný	svobodný	k2eAgMnSc1d1	svobodný
pan	pan	k1gMnSc1	pan
Bingley	Binglea	k1gFnSc2	Binglea
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
věrným	věrný	k2eAgMnSc7d1	věrný
přítelem	přítel	k1gMnSc7	přítel
panem	pan	k1gMnSc7	pan
Darcym	Darcym	k1gInSc4	Darcym
a	a	k8xC	a
zúčastňují	zúčastňovat	k5eAaImIp3nP	zúčastňovat
se	se	k3xPyFc4	se
místního	místní	k2eAgInSc2d1	místní
plesu	ples	k1gInSc2	ples
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
při	při	k7c6	při
prvním	první	k4xOgInSc6	první
setkání	setkání	k1gNnSc6	setkání
si	se	k3xPyFc3	se
nejstarší	starý	k2eAgFnSc1d3	nejstarší
Jane	Jan	k1gMnSc5	Jan
získává	získávat	k5eAaImIp3nS	získávat
srdce	srdce	k1gNnSc4	srdce
mladého	mladý	k2eAgMnSc2d1	mladý
boháče	boháč	k1gMnSc2	boháč
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přesto	přesto	k8xC	přesto
<g/>
,	,	kIx,	,
že	že	k8xS	že
její	její	k3xOp3gFnSc1	její
mladší	mladý	k2eAgFnSc1d2	mladší
sestra	sestra	k1gFnSc1	sestra
Elisabeth	Elisabetha	k1gFnPc2	Elisabetha
se	se	k3xPyFc4	se
s	s	k7c7	s
názory	názor	k1gInPc7	názor
své	svůj	k3xOyFgFnSc2	svůj
matky	matka	k1gFnSc2	matka
příliš	příliš	k6eAd1	příliš
neztotožňuje	ztotožňovat	k5eNaImIp3nS	ztotožňovat
<g/>
,	,	kIx,	,
touží	toužit	k5eAaImIp3nP	toužit
po	po	k7c6	po
seznámení	seznámení	k1gNnSc6	seznámení
s	s	k7c7	s
panem	pan	k1gMnSc7	pan
Darcym	Darcym	k1gInSc4	Darcym
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ji	on	k3xPp3gFnSc4	on
ovšem	ovšem	k9	ovšem
arogantně	arogantně	k6eAd1	arogantně
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Jane	Jan	k1gMnSc5	Jan
se	se	k3xPyFc4	se
s	s	k7c7	s
panem	pan	k1gMnSc7	pan
Bingley	Binglea	k1gFnSc2	Binglea
často	často	k6eAd1	často
setkává	setkávat	k5eAaImIp3nS	setkávat
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInSc1	jejich
vztah	vztah	k1gInSc1	vztah
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
stejný	stejný	k2eAgInSc1d1	stejný
jako	jako	k9	jako
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgMnSc1	tento
mladý	mladý	k2eAgMnSc1d1	mladý
muž	muž	k1gMnSc1	muž
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
přítelem	přítel	k1gMnSc7	přítel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
krátké	krátký	k2eAgFnSc6d1	krátká
době	doba	k1gFnSc6	doba
náhle	náhle	k6eAd1	náhle
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
<g/>
.	.	kIx.	.
</s>
<s>
Elisabeth	Elisabetha	k1gFnPc2	Elisabetha
je	být	k5eAaImIp3nS	být
nucena	nutit	k5eAaImNgFnS	nutit
matkou	matka	k1gFnSc7	matka
ke	k	k7c3	k
sňatku	sňatek	k1gInSc3	sňatek
se	se	k3xPyFc4	se
vzdáleným	vzdálený	k2eAgMnSc7d1	vzdálený
bratrancem	bratranec	k1gMnSc7	bratranec
panem	pan	k1gMnSc7	pan
Collinsem	Collins	k1gInSc7	Collins
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
jí	jíst	k5eAaImIp3nS	jíst
ani	ani	k8xC	ani
trochu	trochu	k6eAd1	trochu
nezamlouvá	zamlouvat	k5eNaImIp3nS	zamlouvat
a	a	k8xC	a
po	po	k7c6	po
souhlasu	souhlas	k1gInSc6	souhlas
otce	otka	k1gFnSc6	otka
ho	on	k3xPp3gMnSc4	on
odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
.	.	kIx.	.
</s>
<s>
Zdrcená	zdrcený	k2eAgFnSc1d1	zdrcená
Jane	Jan	k1gMnSc5	Jan
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
k	k	k7c3	k
tetě	teta	k1gFnSc3	teta
a	a	k8xC	a
strýci	strýc	k1gMnSc3	strýc
do	do	k7c2	do
Londýna	Londýn	k1gInSc2	Londýn
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Elisabeth	Elisabeth	k1gInSc1	Elisabeth
jede	jet	k5eAaImIp3nS	jet
na	na	k7c4	na
venkov	venkov	k1gInSc4	venkov
navštívit	navštívit	k5eAaPmF	navštívit
svou	svůj	k3xOyFgFnSc4	svůj
přítelkyni	přítelkyně	k1gFnSc4	přítelkyně
Charlottou	Charlotta	k1gFnSc7	Charlotta
Lucasovou	Lucasová	k1gFnSc7	Lucasová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
za	za	k7c2	za
pana	pan	k1gMnSc2	pan
Collinse	Collins	k1gMnSc2	Collins
provdala	provdat	k5eAaPmAgFnS	provdat
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
i	i	k9	i
pan	pan	k1gMnSc1	pan
Darcy	Darca	k1gFnSc2	Darca
<g/>
.	.	kIx.	.
</s>
<s>
Mladý	mladý	k2eAgMnSc1d1	mladý
gentleman	gentleman	k1gMnSc1	gentleman
přiznává	přiznávat	k5eAaImIp3nS	přiznávat
své	svůj	k3xOyFgFnSc2	svůj
city	city	k1gFnSc2	city
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
osvětluje	osvětlovat	k5eAaImIp3nS	osvětlovat
náhlý	náhlý	k2eAgInSc4d1	náhlý
odjezd	odjezd	k1gInSc4	odjezd
svého	svůj	k3xOyFgMnSc2	svůj
přítele	přítel	k1gMnSc2	přítel
pana	pan	k1gMnSc2	pan
Bingleyho	Bingley	k1gMnSc2	Bingley
-	-	kIx~	-
kvůli	kvůli	k7c3	kvůli
nevhodnosti	nevhodnost	k1gFnSc3	nevhodnost
Bennetovy	Bennetův	k2eAgFnPc1d1	Bennetova
rodiny	rodina	k1gFnPc1	rodina
mu	on	k3xPp3gMnSc3	on
to	ten	k3xDgNnSc4	ten
sám	sám	k3xTgMnSc1	sám
doporučil	doporučit	k5eAaPmAgInS	doporučit
<g/>
.	.	kIx.	.
</s>
<s>
Elisabeth	Elisabeth	k1gInSc1	Elisabeth
pana	pan	k1gMnSc2	pan
Darcyho	Darcy	k1gMnSc2	Darcy
odmítne	odmítnout	k5eAaPmIp3nS	odmítnout
<g/>
.	.	kIx.	.
</s>
<s>
Dostane	dostat	k5eAaPmIp3nS	dostat
od	od	k7c2	od
něj	on	k3xPp3gNnSc2	on
dopis	dopis	k1gInSc1	dopis
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jí	on	k3xPp3gFnSc3	on
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
své	svůj	k3xOyFgInPc4	svůj
činy	čin	k1gInPc4	čin
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
ho	on	k3xPp3gInSc4	on
Elisabeth	Elisabeth	k1gInSc4	Elisabeth
obvinila	obvinit	k5eAaPmAgFnS	obvinit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
domů	dům	k1gInPc2	dům
se	se	k3xPyFc4	se
setkává	setkávat	k5eAaImIp3nS	setkávat
s	s	k7c7	s
Jane	Jan	k1gMnSc5	Jan
<g/>
,	,	kIx,	,
i	i	k9	i
tetou	teta	k1gFnSc7	teta
a	a	k8xC	a
strýcem	strýc	k1gMnSc7	strýc
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
Jane	Jan	k1gMnSc5	Jan
dovezli	dovézt	k5eAaPmAgMnP	dovézt
domů	dům	k1gInPc2	dům
a	a	k8xC	a
zvou	zvát	k5eAaImIp3nP	zvát
jí	on	k3xPp3gFnSc3	on
s	s	k7c7	s
sebou	se	k3xPyFc7	se
na	na	k7c4	na
prázdniny	prázdniny	k1gFnPc4	prázdniny
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
odjíždí	odjíždět	k5eAaImIp3nS	odjíždět
s	s	k7c7	s
důstojníky	důstojník	k1gMnPc7	důstojník
i	i	k8xC	i
její	její	k3xOp3gFnSc1	její
mladší	mladý	k2eAgFnSc1d2	mladší
sestra	sestra	k1gFnSc1	sestra
Lydie	Lydie	k1gFnSc1	Lydie
<g/>
,	,	kIx,	,
manželka	manželka	k1gFnSc1	manželka
plukovníka	plukovník	k1gMnSc2	plukovník
ji	on	k3xPp3gFnSc4	on
bere	brát	k5eAaImIp3nS	brát
s	s	k7c7	s
sebou	se	k3xPyFc7	se
do	do	k7c2	do
Brightonu	Brighton	k1gInSc2	Brighton
jako	jako	k8xS	jako
společnici	společnice	k1gFnSc4	společnice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
cestách	cesta	k1gFnPc6	cesta
Elisabeth	Elisabetha	k1gFnPc2	Elisabetha
navštíví	navštívit	k5eAaPmIp3nS	navštívit
zámek	zámek	k1gInSc1	zámek
pana	pan	k1gMnSc2	pan
Darcyho	Darcy	k1gMnSc2	Darcy
<g/>
,	,	kIx,	,
neočekávaně	očekávaně	k6eNd1	očekávaně
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
s	s	k7c7	s
ním	on	k3xPp3gNnSc7	on
setkává	setkávat	k5eAaImIp3nS	setkávat
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
pozve	pozvat	k5eAaPmIp3nS	pozvat
ji	on	k3xPp3gFnSc4	on
i	i	k9	i
tetu	teta	k1gFnSc4	teta
a	a	k8xC	a
strýce	strýc	k1gMnSc4	strýc
na	na	k7c4	na
návštěvu	návštěva	k1gFnSc4	návštěva
a	a	k8xC	a
svým	svůj	k3xOyFgNnSc7	svůj
chováním	chování	k1gNnSc7	chování
si	se	k3xPyFc3	se
je	on	k3xPp3gMnPc4	on
získává	získávat	k5eAaImIp3nS	získávat
<g/>
.	.	kIx.	.
</s>
<s>
Seznámí	seznámit	k5eAaPmIp3nS	seznámit
Elisabeth	Elisabeth	k1gInSc1	Elisabeth
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
sestrou	sestra	k1gFnSc7	sestra
Georgianou	Georgiana	k1gFnSc7	Georgiana
<g/>
.	.	kIx.	.
</s>
<s>
Elisabeth	Elisabeth	k1gInSc1	Elisabeth
dostane	dostat	k5eAaPmIp3nS	dostat
dopis	dopis	k1gInSc4	dopis
od	od	k7c2	od
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jí	on	k3xPp3gFnSc3	on
sděluje	sdělovat	k5eAaImIp3nS	sdělovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Lydia	Lydia	k1gFnSc1	Lydia
utekla	utéct	k5eAaPmAgFnS	utéct
s	s	k7c7	s
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
důstojníků	důstojník	k1gMnPc2	důstojník
<g/>
,	,	kIx,	,
panem	pan	k1gMnSc7	pan
Wickhamem	Wickham	k1gInSc7	Wickham
<g/>
.	.	kIx.	.
</s>
<s>
Sdělí	sdělit	k5eAaPmIp3nS	sdělit
to	ten	k3xDgNnSc1	ten
i	i	k8xC	i
panu	pan	k1gMnSc3	pan
Darcymu	Darcym	k1gInSc2	Darcym
a	a	k8xC	a
všichni	všechen	k3xTgMnPc1	všechen
se	se	k3xPyFc4	se
urychleně	urychleně	k6eAd1	urychleně
vrací	vracet	k5eAaImIp3nS	vracet
domů	domů	k6eAd1	domů
<g/>
.	.	kIx.	.
</s>
<s>
Lydia	Lydia	k1gFnSc1	Lydia
se	se	k3xPyFc4	se
provdá	provdat	k5eAaPmIp3nS	provdat
za	za	k7c4	za
pana	pan	k1gMnSc4	pan
Wickhama	Wickham	k1gMnSc4	Wickham
<g/>
,	,	kIx,	,
za	za	k7c2	za
finanční	finanční	k2eAgFnSc2d1	finanční
pomoci	pomoc	k1gFnSc2	pomoc
pana	pan	k1gMnSc2	pan
Darcyho	Darcy	k1gMnSc2	Darcy
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yQnSc6	což
se	se	k3xPyFc4	se
nemá	mít	k5eNaImIp3nS	mít
zmiňovat	zmiňovat	k5eAaImF	zmiňovat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neuhlídá	uhlídat	k5eNaImIp3nS	uhlídat
se	se	k3xPyFc4	se
a	a	k8xC	a
Elisabeth	Elisabeth	k1gInSc1	Elisabeth
to	ten	k3xDgNnSc4	ten
sdělí	sdělit	k5eAaPmIp3nS	sdělit
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
pochopí	pochopit	k5eAaPmIp3nS	pochopit
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
je	on	k3xPp3gMnPc4	on
Darcy	Darc	k1gMnPc4	Darc
ušlechtilý	ušlechtilý	k2eAgMnSc1d1	ušlechtilý
<g/>
.	.	kIx.	.
</s>
<s>
Pan	Pan	k1gMnSc1	Pan
Bingley	Binglea	k1gFnSc2	Binglea
opět	opět	k6eAd1	opět
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
na	na	k7c4	na
své	svůj	k3xOyFgNnSc4	svůj
panství	panství	k1gNnSc4	panství
a	a	k8xC	a
požádá	požádat	k5eAaPmIp3nS	požádat
Jane	Jan	k1gMnSc5	Jan
o	o	k7c6	o
ruku	ruka	k1gFnSc4	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Elisabeth	Elisabeth	k1gInSc1	Elisabeth
navštíví	navštívit	k5eAaPmIp3nS	navštívit
lady	lady	k1gFnSc4	lady
Catherine	Catherin	k1gInSc5	Catherin
de	de	k?	de
Bourgh	Bourgh	k1gInSc1	Bourgh
<g/>
,	,	kIx,	,
teta	teta	k1gFnSc1	teta
pana	pan	k1gMnSc2	pan
Darcyho	Darcy	k1gMnSc2	Darcy
a	a	k8xC	a
chce	chtít	k5eAaImIp3nS	chtít
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
slib	slib	k1gInSc4	slib
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
s	s	k7c7	s
panem	pan	k1gMnSc7	pan
Darcym	Darcym	k1gInSc4	Darcym
nezasnoubí	zasnoubit	k5eNaPmIp3nP	zasnoubit
<g/>
.	.	kIx.	.
</s>
<s>
Elisabeth	Elisabeth	k1gInSc1	Elisabeth
ji	on	k3xPp3gFnSc4	on
odbyde	odbýt	k5eAaPmIp3nS	odbýt
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ránu	ráno	k1gNnSc3	ráno
se	se	k3xPyFc4	se
setkává	setkávat	k5eAaImIp3nS	setkávat
s	s	k7c7	s
panem	pan	k1gMnSc7	pan
Darcym	Darcym	k1gInSc4	Darcym
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
po	po	k7c6	po
návštěvě	návštěva	k1gFnSc6	návštěva
své	svůj	k3xOyFgFnSc2	svůj
tety	teta	k1gFnSc2	teta
začal	začít	k5eAaPmAgInS	začít
doufat	doufat	k5eAaImF	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Elisabeth	Elisabeth	k1gInSc4	Elisabeth
změnila	změnit	k5eAaPmAgFnS	změnit
svůj	svůj	k3xOyFgInSc4	svůj
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
opětovně	opětovně	k6eAd1	opětovně
Elisabeth	Elisabeth	k1gInSc1	Elisabeth
žádá	žádat	k5eAaImIp3nS	žádat
o	o	k7c4	o
ruku	ruka	k1gFnSc4	ruka
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
souhlasí	souhlasit	k5eAaImIp3nS	souhlasit
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
musí	muset	k5eAaImIp3nP	muset
Darcy	Darca	k1gFnPc1	Darca
požádat	požádat	k5eAaPmF	požádat
o	o	k7c4	o
ruku	ruka	k1gFnSc4	ruka
Elisabeth	Elisabeth	k1gMnSc1	Elisabeth
ještě	ještě	k9	ještě
pana	pan	k1gMnSc4	pan
Bennetta	Bennett	k1gMnSc4	Bennett
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
překvapen	překvapit	k5eAaPmNgMnS	překvapit
-	-	kIx~	-
všichni	všechen	k3xTgMnPc1	všechen
se	se	k3xPyFc4	se
domnívali	domnívat	k5eAaImAgMnP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Elisabeth	Elisabeth	k1gInSc1	Elisabeth
pana	pan	k1gMnSc2	pan
Darcyho	Darcy	k1gMnSc2	Darcy
nemá	mít	k5eNaImIp3nS	mít
ráda	rád	k2eAgFnSc1d1	ráda
<g/>
.	.	kIx.	.
</s>
<s>
Elisabeth	Elisabeth	k1gMnSc1	Elisabeth
otci	otec	k1gMnSc3	otec
vše	všechen	k3xTgNnSc1	všechen
vysvětlí	vysvětlit	k5eAaPmIp3nS	vysvětlit
a	a	k8xC	a
ten	ten	k3xDgMnSc1	ten
svoluje	svolovat	k5eAaImIp3nS	svolovat
ke	k	k7c3	k
sňatku	sňatek	k1gInSc3	sňatek
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
několikrát	několikrát	k6eAd1	několikrát
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
za	za	k7c4	za
výpravu	výprava	k1gFnSc4	výprava
<g/>
,	,	kIx,	,
kostýmy	kostým	k1gInPc4	kostým
<g/>
,	,	kIx,	,
hudbu	hudba	k1gFnSc4	hudba
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgFnSc4d1	hlavní
herečku	herečka	k1gFnSc4	herečka
(	(	kIx(	(
<g/>
Keira	Keira	k1gFnSc1	Keira
Knightley	Knightlea	k1gFnSc2	Knightlea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
získal	získat	k5eAaPmAgMnS	získat
cenu	cena	k1gFnSc4	cena
London	London	k1gMnSc1	London
Critics	Criticsa	k1gFnPc2	Criticsa
Circle	Circle	k1gFnSc2	Circle
Film	film	k1gInSc1	film
Awards	Awards	k1gInSc1	Awards
<g/>
,	,	kIx,	,
Empire	empir	k1gInSc5	empir
Awards	Awards	k1gInSc1	Awards
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
kompletně	kompletně	k6eAd1	kompletně
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měli	mít	k5eAaImAgMnP	mít
kameramani	kameraman	k1gMnPc1	kameraman
možnost	možnost	k1gFnSc4	možnost
poskytnou	poskytnout	k5eAaPmIp3nP	poskytnout
záběry	záběr	k1gInPc1	záběr
z	z	k7c2	z
domu	dům	k1gInSc2	dům
ven	ven	k6eAd1	ven
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
se	se	k3xPyFc4	se
natáčel	natáčet	k5eAaImAgInS	natáčet
v	v	k7c6	v
panském	panský	k2eAgNnSc6d1	panské
sídle	sídlo	k1gNnSc6	sídlo
Groombridge	Groombridg	k1gFnSc2	Groombridg
ze	z	k7c2	z
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
na	na	k7c4	na
11	[number]	k4	11
týdnů	týden	k1gInPc2	týden
stalo	stát	k5eAaPmAgNnS	stát
domem	dům	k1gInSc7	dům
Bennetových	Bennetový	k2eAgMnPc2d1	Bennetový
<g/>
.	.	kIx.	.
</s>
