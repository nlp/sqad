<s>
Haploidizace	Haploidizace	k1gFnSc1
</s>
<s>
Haploidizace	Haploidizace	k1gFnSc1
je	být	k5eAaImIp3nS
proces	proces	k1gInSc4
tvorby	tvorba	k1gFnSc2
haploidní	haploidní	k2eAgFnSc2d1
buňky	buňka	k1gFnSc2
(	(	kIx(
<g/>
obvykle	obvykle	k6eAd1
z	z	k7c2
diploidní	diploidní	k2eAgFnSc2d1
buňky	buňka	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Haploidní	haploidní	k2eAgFnSc1d1
buňka	buňka	k1gFnSc1
má	mít	k5eAaImIp3nS
pouze	pouze	k6eAd1
jednu	jeden	k4xCgFnSc4
sadu	sada	k1gFnSc4
chromozómů	chromozóm	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
savců	savec	k1gMnPc2
jsou	být	k5eAaImIp3nP
haploidní	haploidní	k2eAgFnPc1d1
buňky	buňka	k1gFnPc1
prakticky	prakticky	k6eAd1
jen	jen	k9
spermie	spermie	k1gFnPc4
či	či	k8xC
vajíčka	vajíčko	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Haploidizace	Haploidizace	k1gFnSc1
lidských	lidský	k2eAgFnPc2d1
buněk	buňka	k1gFnPc2
</s>
<s>
V	v	k7c6
případě	případ	k1gInSc6
člověka	člověk	k1gMnSc2
<g/>
,	,	kIx,
respektive	respektive	k9
lidských	lidský	k2eAgFnPc2d1
buněk	buňka	k1gFnPc2
<g/>
,	,	kIx,
jde	jít	k5eAaImIp3nS
tedy	tedy	k9
o	o	k7c4
odebrání	odebrání	k1gNnSc4
poloviny	polovina	k1gFnSc2
chromozomů	chromozom	k1gInPc2
z	z	k7c2
buňky	buňka	k1gFnSc2
za	za	k7c7
účelem	účel	k1gInSc7
vytvoření	vytvoření	k1gNnSc2
pohlavní	pohlavní	k2eAgFnSc2d1
buňky	buňka	k1gFnSc2
s	s	k7c7
vidinou	vidina	k1gFnSc7
jejího	její	k3xOp3gNnSc2
využití	využití	k1gNnSc2
při	při	k7c6
reprodukci	reprodukce	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Laboratorní	laboratorní	k2eAgInPc1d1
pokusy	pokus	k1gInPc1
v	v	k7c6
této	tento	k3xDgFnSc6
oblasti	oblast	k1gFnSc6
si	se	k3xPyFc3
kladou	klást	k5eAaImIp3nP
za	za	k7c4
cíl	cíl	k1gInSc4
buďto	buďto	k8xC
nahradit	nahradit	k5eAaPmF
poškozené	poškozený	k2eAgInPc4d1
chromozomy	chromozom	k1gInPc4
spermie	spermie	k1gFnSc1
odebráním	odebrání	k1gNnSc7
materiálu	materiál	k1gInSc2
z	z	k7c2
jiné	jiný	k2eAgFnSc2d1
buňky	buňka	k1gFnSc2
a	a	k8xC
separací	separace	k1gFnPc2
poloviny	polovina	k1gFnSc2
chromozomů	chromozom	k1gInPc2
=	=	kIx~
haploidizací	haploidizace	k1gFnPc2
a	a	k8xC
jejich	jejich	k3xOp3gNnSc1
následné	následný	k2eAgNnSc1d1
dodání	dodání	k1gNnSc1
-	-	kIx~
vyměnění	vyměnění	k1gNnSc1
za	za	k7c4
poškozené	poškozený	k2eAgMnPc4d1
a	a	k8xC
takto	takto	k6eAd1
vzniklou	vzniklý	k2eAgFnSc4d1
pohlavní	pohlavní	k2eAgFnSc4d1
buňku	buňka	k1gFnSc4
dodat	dodat	k5eAaPmF
do	do	k7c2
vajíčka	vajíčko	k1gNnSc2
(	(	kIx(
<g/>
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
má	mít	k5eAaImIp3nS
kompletní	kompletní	k2eAgFnSc4d1
chromozomovou	chromozomový	k2eAgFnSc4d1
výbavu	výbava	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
po	po	k7c4
proniknutí	proniknutí	k1gNnSc4
spermie	spermie	k1gFnSc2
polovinu	polovina	k1gFnSc4
samo	sám	k3xTgNnSc1
selektuje	selektovat	k5eAaBmIp3nS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
nebo	nebo	k8xC
jiné	jiný	k2eAgInPc1d1
zásahy	zásah	k1gInPc1
do	do	k7c2
přirozené	přirozený	k2eAgFnSc2d1
reprodukce	reprodukce	k1gFnSc2
s	s	k7c7
terapeutickým	terapeutický	k2eAgInSc7d1
účinkem	účinek	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Teoreticky	teoreticky	k6eAd1
otvírá	otvírat	k5eAaImIp3nS
možnost	možnost	k1gFnSc4
párům	pár	k1gInPc3
stejného	stejný	k2eAgNnSc2d1
pohlaví	pohlaví	k1gNnSc2
mít	mít	k5eAaImF
spolu	spolu	k6eAd1
"	"	kIx"
<g/>
vlastní	vlastní	k2eAgNnSc4d1
<g/>
"	"	kIx"
dítě	dítě	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Je	být	k5eAaImIp3nS
evidentní	evidentní	k2eAgMnSc1d1
že	že	k8xS
využití	využití	k1gNnSc1
haploidizace	haploidizace	k1gFnSc2
může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
silný	silný	k2eAgInSc4d1
etický	etický	k2eAgInSc4d1
rozměr	rozměr	k1gInSc4
<g/>
,	,	kIx,
na	na	k7c4
který	který	k3yIgInSc4,k3yRgInSc4,k3yQgInSc4
se	se	k3xPyFc4
v	v	k7c6
dnešní	dnešní	k2eAgFnSc6d1
vědě	věda	k1gFnSc6
často	často	k6eAd1
zapomíná	zapomínat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Reprodukční	reprodukční	k2eAgNnSc1d1
klonování	klonování	k1gNnSc1
člověka	člověk	k1gMnSc2
-	-	kIx~
více	hodně	k6eAd2
odborný	odborný	k2eAgInSc4d1
ráz	ráz	k1gInSc4
</s>
<s>
I	i	k9
staré	starý	k2eAgFnPc1d1
ženy	žena	k1gFnPc1
budou	být	k5eAaImBp3nP
jednou	jednou	k6eAd1
mít	mít	k5eAaImF
děti	dítě	k1gFnPc4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
