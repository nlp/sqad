<p>
<s>
The	The	k?	The
Princess	Princess	k1gInSc1	Princess
Switch	Switch	k1gInSc1	Switch
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgInSc1d1	americký
vánoční	vánoční	k2eAgInSc1d1	vánoční
romantický	romantický	k2eAgInSc1d1	romantický
komediální	komediální	k2eAgInSc1d1	komediální
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
Režie	režie	k1gFnSc1	režie
se	se	k3xPyFc4	se
ujal	ujmout	k5eAaPmAgMnS	ujmout
Mike	Mike	k1gInSc4	Mike
Rohl	rohnout	k5eAaPmAgMnS	rohnout
a	a	k8xC	a
scénáře	scénář	k1gInPc4	scénář
Robin	robin	k2eAgInSc4d1	robin
Bernheim	Bernheim	k1gInSc4	Bernheim
a	a	k8xC	a
Megan	Megan	k1gInSc4	Megan
Metzger	Metzgra	k1gFnPc2	Metzgra
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnPc4d1	hlavní
role	role	k1gFnPc4	role
hrají	hrát	k5eAaImIp3nP	hrát
Vanessa	Vanessa	k1gFnSc1	Vanessa
Hudgens	Hudgens	k1gInSc4	Hudgens
<g/>
,	,	kIx,	,
Sam	Sam	k1gMnSc1	Sam
Palladio	Palladio	k1gMnSc1	Palladio
a	a	k8xC	a
Nick	Nick	k1gMnSc1	Nick
Sagar	Sagar	k1gMnSc1	Sagar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
Vanessa	Vanessa	k1gFnSc1	Vanessa
Hudgens	Hudgensa	k1gFnPc2	Hudgensa
jako	jako	k8xS	jako
Margaret	Margareta	k1gFnPc2	Margareta
Delacourt	Delacourta	k1gFnPc2	Delacourta
<g/>
,	,	kIx,	,
vévodkyně	vévodkyně	k1gFnPc1	vévodkyně
z	z	k7c2	z
Montenara	Montenar	k1gMnSc2	Montenar
/	/	kIx~	/
Stacy	Staca	k1gMnSc2	Staca
DeNovo	DeNovo	k1gNnSc4	DeNovo
</s>
</p>
<p>
<s>
Sam	Sam	k1gMnSc1	Sam
Palladio	Palladio	k1gMnSc1	Palladio
jako	jako	k8xC	jako
princ	princ	k1gMnSc1	princ
Edward	Edward	k1gMnSc1	Edward
</s>
</p>
<p>
<s>
Nick	Nick	k1gMnSc1	Nick
Sagar	Sagar	k1gMnSc1	Sagar
jako	jako	k8xC	jako
Kevin	Kevin	k1gMnSc1	Kevin
Richards	Richardsa	k1gFnPc2	Richardsa
</s>
</p>
<p>
<s>
Mark	Mark	k1gMnSc1	Mark
Fleischmann	Fleischmann	k1gMnSc1	Fleischmann
jako	jako	k8xS	jako
Frank	Frank	k1gMnSc1	Frank
De	De	k?	De
Luca	Luca	k1gMnSc1	Luca
</s>
</p>
<p>
<s>
Suanne	Suannout	k5eAaImIp3nS	Suannout
Braun	Braun	k1gMnSc1	Braun
jako	jako	k8xC	jako
paní	paní	k1gFnSc1	paní
Donatelli	Donatelle	k1gFnSc4	Donatelle
</s>
</p>
<p>
<s>
Alexa	Alexa	k1gMnSc1	Alexa
Adeosun	Adeosun	k1gMnSc1	Adeosun
jako	jako	k8xC	jako
Olivia	Olivia	k1gFnSc1	Olivia
Richards	Richardsa	k1gFnPc2	Richardsa
</s>
</p>
<p>
<s>
Sara	Sara	k1gMnSc1	Sara
Stewart	Stewart	k1gMnSc1	Stewart
jako	jako	k8xC	jako
královna	královna	k1gFnSc1	královna
Caroline	Carolin	k1gInSc5	Carolin
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Douglas	Douglas	k1gMnSc1	Douglas
jako	jako	k8xS	jako
král	král	k1gMnSc1	král
George	Georg	k1gMnSc2	Georg
</s>
</p>
<p>
<s>
Amy	Amy	k?	Amy
Griffith	Griffith	k1gInSc1	Griffith
jako	jako	k8xS	jako
Brianna	Brianna	k1gFnSc1	Brianna
Michaels	Michaelsa	k1gFnPc2	Michaelsa
</s>
</p>
<p>
<s>
Robin	robin	k2eAgInSc1d1	robin
Soans	Soans	k1gInSc1	Soans
jako	jako	k9	jako
laskavý	laskavý	k2eAgMnSc1d1	laskavý
starý	starý	k2eAgMnSc1d1	starý
muž	muž	k1gMnSc1	muž
</s>
</p>
<p>
<s>
==	==	k?	==
Produkce	produkce	k1gFnSc1	produkce
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Vanessa	Vanessa	k1gFnSc1	Vanessa
Hudgens	Hudgensa	k1gFnPc2	Hudgensa
a	a	k8xC	a
Sam	Sam	k1gMnSc1	Sam
Palladio	Palladio	k1gMnSc1	Palladio
si	se	k3xPyFc3	se
zahrají	zahrát	k5eAaPmIp3nP	zahrát
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Netflixu	Netflix	k1gInSc2	Netflix
The	The	k1gFnSc2	The
Princess	Princessa	k1gFnPc2	Princessa
Switch	Switch	k1gInSc1	Switch
<g/>
.	.	kIx.	.
<g/>
Natáčení	natáčení	k1gNnSc1	natáčení
skončilo	skončit	k5eAaPmAgNnS	skončit
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
filmu	film	k1gInSc2	film
byla	být	k5eAaImAgFnS	být
natočena	natočit	k5eAaBmNgFnS	natočit
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Carei	Care	k1gFnSc2	Care
v	v	k7c6	v
Rumunsku	Rumunsko	k1gNnSc6	Rumunsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vydání	vydání	k1gNnSc1	vydání
==	==	k?	==
</s>
</p>
<p>
<s>
Film	film	k1gInSc1	film
měl	mít	k5eAaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
dne	den	k1gInSc2	den
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2018	[number]	k4	2018
na	na	k7c6	na
Netflixu	Netflix	k1gInSc6	Netflix
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
The	The	k1gFnSc2	The
Princess	Princessa	k1gFnPc2	Princessa
Switch	Switch	k1gMnSc1	Switch
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
The	The	k?	The
Princess	Princess	k1gInSc1	Princess
Switch	Switcha	k1gFnPc2	Switcha
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gFnSc1	The
Princess	Princess	k1gInSc1	Princess
Switch	Switch	k1gInSc1	Switch
v	v	k7c6	v
recenzním	recenzní	k2eAgInSc6d1	recenzní
agregátoru	agregátor	k1gInSc6	agregátor
Rotten	Rotten	k2eAgInSc4d1	Rotten
Tomatoes	Tomatoes	k1gInSc4	Tomatoes
</s>
</p>
