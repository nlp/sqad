<p>
<s>
Důmyslný	důmyslný	k2eAgMnSc1d1	důmyslný
rytíř	rytíř	k1gMnSc1	rytíř
don	don	k1gMnSc1	don
Quijote	Quijot	k1gInSc5	Quijot
de	de	k?	de
la	la	k1gNnSc1	la
Mancha	Mancha	k1gFnSc1	Mancha
(	(	kIx(	(
<g/>
originální	originální	k2eAgInSc1d1	originální
název	název	k1gInSc1	název
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1605	[number]	k4	1605
El	Ela	k1gFnPc2	Ela
ingenioso	ingenioso	k6eAd1	ingenioso
hidalgo	hidalgo	k1gMnSc1	hidalgo
Don	Don	k1gMnSc1	Don
Quixote	Quixot	k1gInSc5	Quixot
de	de	k?	de
la	la	k1gNnSc1	la
Mancha	Mancha	k1gFnSc1	Mancha
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
Don	Don	k1gMnSc1	Don
Quijote	Quijot	k1gInSc5	Quijot
de	de	k?	de
la	la	k1gNnSc2	la
Mancha	Manch	k1gMnSc2	Manch
či	či	k8xC	či
též	též	k9	též
Don	Don	k1gMnSc1	Don
Quijote	Quijot	k1gInSc5	Quijot
[	[	kIx(	[
<g/>
don	don	k1gMnSc1	don
kichote	kichot	k1gInSc5	kichot
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejslavnějším	slavný	k2eAgInSc7d3	nejslavnější
španělským	španělský	k2eAgInSc7d1	španělský
románem	román	k1gInSc7	román
a	a	k8xC	a
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
klíčových	klíčový	k2eAgNnPc2d1	klíčové
děl	dělo	k1gNnPc2	dělo
světové	světový	k2eAgFnSc2d1	světová
literatury	literatura	k1gFnSc2	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
autorem	autor	k1gMnSc7	autor
je	být	k5eAaImIp3nS	být
Miguel	Miguel	k1gMnSc1	Miguel
de	de	k?	de
Cervantes	Cervantes	k1gMnSc1	Cervantes
y	y	k?	y
Saavedra	Saavedra	k1gFnSc1	Saavedra
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
román	román	k1gInSc4	román
z	z	k7c2	z
období	období	k1gNnSc2	období
renesance	renesance	k1gFnSc2	renesance
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
uvedl	uvést	k5eAaPmAgMnS	uvést
do	do	k7c2	do
literatury	literatura	k1gFnSc2	literatura
komicky	komicky	k6eAd1	komicky
vyjádřený	vyjádřený	k2eAgInSc1d1	vyjádřený
rozpor	rozpor	k1gInSc1	rozpor
mezi	mezi	k7c7	mezi
skutečností	skutečnost	k1gFnSc7	skutečnost
a	a	k8xC	a
iluzí	iluze	k1gFnSc7	iluze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
Don	Don	k1gMnSc1	Don
Quijote	Quijot	k1gInSc5	Quijot
přeložen	přeložit	k5eAaPmNgInS	přeložit
do	do	k7c2	do
85	[number]	k4	85
různých	různý	k2eAgInPc2d1	různý
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Členění	členění	k1gNnSc1	členění
románu	román	k1gInSc2	román
==	==	k?	==
</s>
</p>
<p>
<s>
Román	román	k1gInSc1	román
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
dvou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
vyšla	vyjít	k5eAaPmAgFnS	vyjít
16	[number]	k4	16
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1605	[number]	k4	1605
v	v	k7c6	v
Madridu	Madrid	k1gInSc6	Madrid
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Důmyslný	důmyslný	k2eAgMnSc1d1	důmyslný
rytíř	rytíř	k1gMnSc1	rytíř
don	don	k1gMnSc1	don
Quijote	Quijot	k1gInSc5	Quijot
de	de	k?	de
la	la	k1gNnSc1	la
Mancha	Mancha	k1gMnSc1	Mancha
(	(	kIx(	(
<g/>
El	Ela	k1gFnPc2	Ela
ingenioso	ingenioso	k6eAd1	ingenioso
hidalgo	hidalgo	k1gMnSc1	hidalgo
don	don	k1gMnSc1	don
Quijote	Quijot	k1gInSc5	Quijot
de	de	k?	de
la	la	k1gNnSc1	la
Mancha	Mancha	k1gFnSc1	Mancha
<g/>
)	)	kIx)	)
a	a	k8xC	a
stala	stát	k5eAaPmAgFnS	stát
se	se	k3xPyFc4	se
bestsellerem	bestseller	k1gInSc7	bestseller
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
první	první	k4xOgInSc1	první
díl	díl	k1gInSc1	díl
byl	být	k5eAaImAgInS	být
napsán	napsat	k5eAaBmNgInS	napsat
během	během	k7c2	během
Cervantesova	Cervantesův	k2eAgInSc2d1	Cervantesův
pobytu	pobyt	k1gInSc2	pobyt
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
v	v	k7c6	v
letech	let	k1gInPc6	let
1597	[number]	k4	1597
<g/>
–	–	k?	–
<g/>
1602	[number]	k4	1602
<g/>
.	.	kIx.	.
<g/>
Roku	rok	k1gInSc2	rok
1614	[number]	k4	1614
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
druhé	druhý	k4xOgNnSc1	druhý
pokračování	pokračování	k1gNnSc1	pokračování
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
napsáno	napsat	k5eAaPmNgNnS	napsat
Cervantesem	Cervantes	k1gInSc7	Cervantes
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Segundo	Segundo	k6eAd1	Segundo
tomo	tomo	k6eAd1	tomo
del	del	k?	del
ingenioso	ingenioso	k6eAd1	ingenioso
hidalgo	hidalgo	k1gMnSc1	hidalgo
Don	Don	k1gMnSc1	Don
Quixote	Quixot	k1gInSc5	Quixot
de	de	k?	de
la	la	k1gNnSc1	la
Mancha	Mancha	k1gFnSc1	Mancha
-	-	kIx~	-
que	que	k?	que
contiene	contien	k1gInSc5	contien
la	la	k1gNnPc4	la
tercera	tercer	k1gMnSc2	tercer
salida	salid	k1gMnSc2	salid
<g/>
,	,	kIx,	,
y	y	k?	y
es	es	k1gNnSc1	es
la	la	k1gNnSc2	la
quinta	quint	k1gMnSc2	quint
parte	parte	k1gNnSc2	parte
de	de	k?	de
sus	sus	k?	sus
aventuras	aventurasa	k1gFnPc2	aventurasa
podepsána	podepsán	k2eAgFnSc1d1	podepsána
pseudonymem	pseudonym	k1gInSc7	pseudonym
Alonso	Alonsa	k1gFnSc5	Alonsa
Fernández	Fernández	k1gMnSc1	Fernández
de	de	k?	de
Avellaneda	Avellaneda	k1gMnSc1	Avellaneda
<g/>
,	,	kIx,	,
obdivovatelem	obdivovatel	k1gMnSc7	obdivovatel
Lope	Lop	k1gMnSc2	Lop
de	de	k?	de
Vegy	Vega	k1gMnSc2	Vega
<g/>
,	,	kIx,	,
Cervantesova	Cervantesův	k2eAgMnSc2d1	Cervantesův
rivala	rival	k1gMnSc2	rival
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
doby	doba	k1gFnSc2	doba
Cervantes	Cervantes	k1gInSc1	Cervantes
neplánoval	plánovat	k5eNaImAgInS	plánovat
žádné	žádný	k3yNgNnSc4	žádný
další	další	k2eAgNnSc4d1	další
pokračování	pokračování	k1gNnSc4	pokračování
Dona	Don	k1gMnSc2	Don
Quijota	Quijot	k1gMnSc2	Quijot
a	a	k8xC	a
nebyl	být	k5eNaImAgInS	být
zvlášť	zvlášť	k6eAd1	zvlášť
literárně	literárně	k6eAd1	literárně
činný	činný	k2eAgMnSc1d1	činný
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
pobouřen	pobouřit	k5eAaPmNgInS	pobouřit
dílem	dílo	k1gNnSc7	dílo
Fernándeze	Fernándeze	k1gFnSc2	Fernándeze
a	a	k8xC	a
rychle	rychle	k6eAd1	rychle
sepisuje	sepisovat	k5eAaImIp3nS	sepisovat
druhý	druhý	k4xOgInSc4	druhý
díl	díl	k1gInSc4	díl
své	svůj	k3xOyFgFnSc2	svůj
knihy	kniha	k1gFnSc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
roku	rok	k1gInSc2	rok
1615	[number]	k4	1615
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Segunda	Segunda	k1gFnSc1	Segunda
parte	parte	k1gNnSc1	parte
del	del	k?	del
ingenioso	ingenioso	k6eAd1	ingenioso
cavallero	cavallero	k1gNnSc1	cavallero
Don	dona	k1gFnPc2	dona
Quixote	Quixot	k1gInSc5	Quixot
de	de	k?	de
la	la	k1gNnSc1	la
Mancha	Mancha	k1gMnSc1	Mancha
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
vložil	vložit	k5eAaPmAgMnS	vložit
některé	některý	k3yIgInPc4	některý
své	svůj	k3xOyFgInPc4	svůj
názory	názor	k1gInPc4	názor
ohledně	ohledně	k7c2	ohledně
Fernandézova	Fernandézův	k2eAgNnSc2d1	Fernandézův
nepovoleného	povolený	k2eNgNnSc2d1	nepovolené
pokračování	pokračování	k1gNnSc2	pokračování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsah	obsah	k1gInSc1	obsah
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgFnSc1	první
část	část	k1gFnSc1	část
románu	román	k1gInSc2	román
popisuje	popisovat	k5eAaImIp3nS	popisovat
první	první	k4xOgFnPc4	první
dvě	dva	k4xCgFnPc4	dva
výpravy	výprava	k1gFnPc4	výprava
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
"	"	kIx"	"
<g/>
nalezenými	nalezený	k2eAgInPc7d1	nalezený
<g/>
"	"	kIx"	"
epitafy	epitaf	k1gInPc7	epitaf
na	na	k7c4	na
dona	don	k1gMnSc4	don
Quijota	Quijot	k1gMnSc4	Quijot
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
část	část	k1gFnSc1	část
popisuje	popisovat	k5eAaImIp3nS	popisovat
třetí	třetí	k4xOgFnSc4	třetí
výpravu	výprava	k1gFnSc4	výprava
rytíře	rytíř	k1gMnSc2	rytíř
a	a	k8xC	a
končí	končit	k5eAaImIp3nS	končit
popisem	popis	k1gInSc7	popis
jeho	jeho	k3xOp3gFnSc2	jeho
smrti	smrt	k1gFnSc2	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Román	román	k1gInSc1	román
začíná	začínat	k5eAaImIp3nS	začínat
představením	představení	k1gNnSc7	představení
hlavní	hlavní	k2eAgFnSc2d1	hlavní
postavy	postava	k1gFnSc2	postava
–	–	k?	–
chudého	chudý	k2eAgMnSc2d1	chudý
šlechtice	šlechtic	k1gMnSc2	šlechtic
neznámého	známý	k2eNgNnSc2d1	neznámé
jména	jméno	k1gNnSc2	jméno
(	(	kIx(	(
<g/>
nejspíš	nejspíš	k9	nejspíš
"	"	kIx"	"
<g/>
Alonso	Alonsa	k1gFnSc5	Alonsa
Quijano	Quijana	k1gFnSc5	Quijana
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
z	z	k7c2	z
kraje	kraj	k1gInSc2	kraj
La	la	k1gNnSc1	la
Mancha	Mancha	k1gMnSc1	Mancha
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
všechen	všechen	k3xTgInSc4	všechen
svůj	svůj	k3xOyFgInSc4	svůj
čas	čas	k1gInSc4	čas
tráví	trávit	k5eAaImIp3nS	trávit
čtením	čtení	k1gNnSc7	čtení
starých	starý	k2eAgInPc2d1	starý
rytířských	rytířský	k2eAgInPc2d1	rytířský
románů	román	k1gInPc2	román
<g/>
.	.	kIx.	.
</s>
<s>
Fiktivní	fiktivní	k2eAgInSc1d1	fiktivní
svět	svět	k1gInSc1	svět
ho	on	k3xPp3gMnSc4	on
tak	tak	k9	tak
pohltí	pohltit	k5eAaPmIp3nS	pohltit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
vzkřísit	vzkřísit	k5eAaPmF	vzkřísit
zašlou	zašlý	k2eAgFnSc4d1	zašlá
slávu	sláva	k1gFnSc4	sláva
rytířského	rytířský	k2eAgInSc2d1	rytířský
stavu	stav	k1gInSc2	stav
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
stane	stanout	k5eAaPmIp3nS	stanout
potulným	potulný	k2eAgMnSc7d1	potulný
rytířem	rytíř	k1gMnSc7	rytíř
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
napraví	napravit	k5eAaPmIp3nS	napravit
křivdy	křivda	k1gFnPc4	křivda
<g/>
,	,	kIx,	,
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
vdovám	vdova	k1gFnPc3	vdova
a	a	k8xC	a
sirotkům	sirotek	k1gMnPc3	sirotek
a	a	k8xC	a
ochraňuje	ochraňovat	k5eAaImIp3nS	ochraňovat
slečny	slečna	k1gFnSc2	slečna
<g/>
.	.	kIx.	.
</s>
<s>
Vytáhne	vytáhnout	k5eAaPmIp3nS	vytáhnout
tedy	tedy	k9	tedy
starou	starý	k2eAgFnSc4d1	stará
rezavou	rezavý	k2eAgFnSc4d1	rezavá
zbroj	zbroj	k1gFnSc4	zbroj
po	po	k7c6	po
předcích	předek	k1gInPc6	předek
<g/>
,	,	kIx,	,
opraví	opravit	k5eAaPmIp3nP	opravit
ji	on	k3xPp3gFnSc4	on
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc4	svůj
starou	starý	k2eAgFnSc4d1	stará
bílou	bílý	k2eAgFnSc4d1	bílá
herku	herka	k1gFnSc4	herka
ve	v	k7c6	v
stáji	stáj	k1gFnSc6	stáj
přejmenuje	přejmenovat	k5eAaPmIp3nS	přejmenovat
na	na	k7c4	na
Rocinantu	Rocinanta	k1gFnSc4	Rocinanta
<g/>
,	,	kIx,	,
jakožto	jakožto	k8xS	jakožto
toho	ten	k3xDgMnSc2	ten
nejlepšího	dobrý	k2eAgMnSc2d3	nejlepší
rytířského	rytířský	k2eAgMnSc2d1	rytířský
koně	kůň	k1gMnSc2	kůň
pod	pod	k7c7	pod
sluncem	slunce	k1gNnSc7	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
si	se	k3xPyFc3	se
pak	pak	k6eAd1	pak
dá	dát	k5eAaPmIp3nS	dát
vznešenější	vznešený	k2eAgNnSc1d2	vznešenější
jméno	jméno	k1gNnSc1	jméno
"	"	kIx"	"
<g/>
Don	Don	k1gMnSc1	Don
Quijote	Quijot	k1gInSc5	Quijot
de	de	k?	de
la	la	k1gNnSc1	la
Mancha	Mancha	k1gFnSc1	Mancha
<g/>
"	"	kIx"	"
a	a	k8xC	a
z	z	k7c2	z
prosté	prostý	k2eAgFnSc2d1	prostá
venkovské	venkovský	k2eAgFnSc2d1	venkovská
dívky	dívka	k1gFnSc2	dívka
-	-	kIx~	-
Aldonzy	Aldonza	k1gFnSc2	Aldonza
Lorenzové	Lorenzová	k1gFnSc2	Lorenzová
<g/>
,	,	kIx,	,
učiní	učinit	k5eAaImIp3nS	učinit
dámu	dáma	k1gFnSc4	dáma
a	a	k8xC	a
paní	paní	k1gFnSc4	paní
svého	svůj	k3xOyFgNnSc2	svůj
srdce	srdce	k1gNnSc2	srdce
pod	pod	k7c7	pod
novým	nový	k2eAgNnSc7d1	nové
jménem	jméno	k1gNnSc7	jméno
Dulcinea	Dulcinea	k1gFnSc1	Dulcinea
de	de	k?	de
Tobosa	Tobosa	k1gFnSc1	Tobosa
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
zbrojnošem	zbrojnoš	k1gMnSc7	zbrojnoš
Sanchem	Sanch	k1gMnSc7	Sanch
Panzou	Panza	k1gFnSc7	Panza
<g/>
,	,	kIx,	,
kterému	který	k3yIgMnSc3	který
za	za	k7c4	za
jeho	jeho	k3xOp3gFnPc4	jeho
služby	služba	k1gFnPc4	služba
slíbí	slíbit	k5eAaPmIp3nS	slíbit
ostrov	ostrov	k1gInSc1	ostrov
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
vyráží	vyrážet	k5eAaImIp3nS	vyrážet
na	na	k7c4	na
cesty	cesta	k1gFnPc4	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
jeho	on	k3xPp3gNnSc2	on
bláznovství	bláznovství	k1gNnSc2	bláznovství
a	a	k8xC	a
putování	putování	k1gNnSc2	putování
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
snaží	snažit	k5eAaImIp3nS	snažit
odradit	odradit	k5eAaPmF	odradit
jeho	jeho	k3xOp3gNnSc4	jeho
okolí	okolí	k1gNnSc4	okolí
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
jeho	jeho	k3xOp3gFnSc4	jeho
neteř	neteř	k1gFnSc4	neteř
<g/>
,	,	kIx,	,
farář	farář	k1gMnSc1	farář
a	a	k8xC	a
holič	holič	k1gMnSc1	holič
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
má	mít	k5eAaImIp3nS	mít
i	i	k9	i
Sancho	Sancha	k1gFnSc5	Sancha
často	často	k6eAd1	často
značné	značný	k2eAgFnPc4d1	značná
pochybnosti	pochybnost	k1gFnPc4	pochybnost
o	o	k7c6	o
pánově	pánův	k2eAgInSc6d1	pánův
zdravém	zdravý	k2eAgInSc6d1	zdravý
rozumu	rozum	k1gInSc6	rozum
<g/>
.	.	kIx.	.
</s>
<s>
Několikrát	několikrát	k6eAd1	několikrát
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
sice	sice	k8xC	sice
podaří	podařit	k5eAaPmIp3nS	podařit
dostat	dostat	k5eAaPmF	dostat
Dona	Don	k1gMnSc4	Don
Quijota	Quijot	k1gMnSc4	Quijot
zpátky	zpátky	k6eAd1	zpátky
domů	domů	k6eAd1	domů
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
vždy	vždy	k6eAd1	vždy
opět	opět	k6eAd1	opět
vydává	vydávat	k5eAaPmIp3nS	vydávat
zpátky	zpátky	k6eAd1	zpátky
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
pouť	pouť	k1gFnSc4	pouť
světem	svět	k1gInSc7	svět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgNnPc2d3	nejznámější
dobrodružství	dobrodružství	k1gNnPc2	dobrodružství
Dona	Don	k1gMnSc2	Don
Quijota	Quijot	k1gMnSc2	Quijot
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
boji	boj	k1gInSc6	boj
s	s	k7c7	s
větrnými	větrný	k2eAgInPc7d1	větrný
mlýny	mlýn	k1gInPc7	mlýn
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
místo	místo	k7c2	místo
nich	on	k3xPp3gMnPc2	on
viděl	vidět	k5eAaImAgMnS	vidět
obry	obr	k1gMnPc4	obr
a	a	k8xC	a
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
s	s	k7c7	s
nimi	on	k3xPp3gMnPc7	on
bojovatD	bojovatD	k?	bojovatD
</s>
</p>
<p>
<s>
==	==	k?	==
Význam	význam	k1gInSc1	význam
==	==	k?	==
</s>
</p>
<p>
<s>
Důmyslný	důmyslný	k2eAgMnSc1d1	důmyslný
rytíř	rytíř	k1gMnSc1	rytíř
don	don	k1gMnSc1	don
Quijote	Quijot	k1gInSc5	Quijot
de	de	k?	de
la	la	k1gNnSc1	la
Mancha	Mancha	k1gMnSc1	Mancha
se	se	k3xPyFc4	se
často	často	k6eAd1	často
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgNnPc2d3	veliký
děl	dělo	k1gNnPc2	dělo
beletrie	beletrie	k1gFnSc2	beletrie
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
autorů	autor	k1gMnPc2	autor
světa	svět	k1gInSc2	svět
pořádané	pořádaný	k2eAgFnPc1d1	pořádaná
Norwegian	Norwegian	k1gInSc4	Norwegian
Book	Booka	k1gFnPc2	Booka
Clubs	Clubsa	k1gFnPc2	Clubsa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
byla	být	k5eAaImAgFnS	být
zařazena	zařadit	k5eAaPmNgFnS	zařadit
mezi	mezi	k7c4	mezi
100	[number]	k4	100
nejlepších	dobrý	k2eAgNnPc2d3	nejlepší
děl	dělo	k1gNnPc2	dělo
beletrie	beletrie	k1gFnSc1	beletrie
<g/>
.	.	kIx.	.
<g/>
Některé	některý	k3yIgInPc1	některý
prvky	prvek	k1gInPc1	prvek
této	tento	k3xDgFnSc2	tento
knihy	kniha	k1gFnSc2	kniha
se	se	k3xPyFc4	se
přenesly	přenést	k5eAaPmAgFnP	přenést
i	i	k9	i
do	do	k7c2	do
samotného	samotný	k2eAgInSc2d1	samotný
jazyka	jazyk	k1gInSc2	jazyk
a	a	k8xC	a
staly	stát	k5eAaPmAgFnP	stát
se	se	k3xPyFc4	se
ustálenými	ustálený	k2eAgInPc7d1	ustálený
obraty	obrat	k1gInPc7	obrat
<g/>
.	.	kIx.	.
</s>
<s>
Označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
Don	Don	k1gMnSc1	Don
Kichot	Kichot	k1gMnSc1	Kichot
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
případně	případně	k6eAd1	případně
"	"	kIx"	"
<g/>
donkichotství	donkichotství	k1gNnSc1	donkichotství
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vžilo	vžít	k5eAaPmAgNnS	vžít
pro	pro	k7c4	pro
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
tento	tento	k3xDgMnSc1	tento
rytíř	rytíř	k1gMnSc1	rytíř
<g/>
,	,	kIx,	,
snaží	snažit	k5eAaImIp3nS	snažit
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
nemožného	možný	k2eNgNnSc2d1	nemožné
<g/>
,	,	kIx,	,
věčně	věčně	k6eAd1	věčně
sní	snít	k5eAaImIp3nP	snít
a	a	k8xC	a
nedívají	dívat	k5eNaImIp3nP	dívat
se	se	k3xPyFc4	se
na	na	k7c4	na
realitu	realita	k1gFnSc4	realita
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
"	"	kIx"	"
<g/>
boj	boj	k1gInSc1	boj
s	s	k7c7	s
větrnými	větrný	k2eAgInPc7d1	větrný
mlýny	mlýn	k1gInPc7	mlýn
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
označuje	označovat	k5eAaImIp3nS	označovat
zbytečná	zbytečný	k2eAgFnSc1d1	zbytečná
snaha	snaha	k1gFnSc1	snaha
<g/>
,	,	kIx,	,
nerovný	rovný	k2eNgInSc1d1	nerovný
boj	boj	k1gInSc1	boj
nebo	nebo	k8xC	nebo
boj	boj	k1gInSc1	boj
s	s	k7c7	s
fiktivním	fiktivní	k2eAgMnSc7d1	fiktivní
nepřítelem	nepřítel	k1gMnSc7	nepřítel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
umění	umění	k1gNnSc4	umění
==	==	k?	==
</s>
</p>
<p>
<s>
Don	Don	k1gMnSc1	Don
Quijote	Quijot	k1gInSc5	Quijot
posloužil	posloužit	k5eAaPmAgInS	posloužit
i	i	k9	i
jako	jako	k9	jako
námět	námět	k1gInSc4	námět
pro	pro	k7c4	pro
díla	dílo	k1gNnPc4	dílo
umělců	umělec	k1gMnPc2	umělec
jako	jako	k8xC	jako
Vicente	Vicent	k1gMnSc5	Vicent
Blasco	Blasca	k1gMnSc5	Blasca
Ibáñ	Ibáñ	k1gMnSc5	Ibáñ
<g/>
,	,	kIx,	,
Gustave	Gustav	k1gMnSc5	Gustav
Doré	Dorý	k2eAgNnSc4d1	Dorý
<g/>
,	,	kIx,	,
Salvador	Salvador	k1gMnSc1	Salvador
Dalí	Dalí	k1gMnSc1	Dalí
<g/>
,	,	kIx,	,
Pablo	Pablo	k1gNnSc1	Pablo
Picasso	Picassa	k1gFnSc5	Picassa
či	či	k8xC	či
Cyprián	Cyprián	k1gMnSc1	Cyprián
Majerník	Majerník	k1gMnSc1	Majerník
<g/>
.	.	kIx.	.
</s>
<s>
Salvador	Salvador	k1gMnSc1	Salvador
Dalí	Dalí	k1gMnSc1	Dalí
dokonce	dokonce	k9	dokonce
ilustroval	ilustrovat	k5eAaBmAgMnS	ilustrovat
jedno	jeden	k4xCgNnSc4	jeden
vydání	vydání	k1gNnSc4	vydání
dona	don	k1gMnSc2	don
Quijota	Quijot	k1gMnSc2	Quijot
a	a	k8xC	a
originály	originál	k1gInPc1	originál
těchto	tento	k3xDgFnPc2	tento
ilustrací	ilustrace	k1gFnPc2	ilustrace
jsou	být	k5eAaImIp3nP	být
vystavené	vystavená	k1gFnPc1	vystavená
v	v	k7c6	v
Dalího	Dalí	k2eAgNnSc2d1	Dalí
muzeu	muzeum	k1gNnSc3	muzeum
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
<g/>
Don	Don	k1gMnSc1	Don
Quijote	Quijot	k1gInSc5	Quijot
inspiroval	inspirovat	k5eAaBmAgMnS	inspirovat
i	i	k9	i
řadu	řada	k1gFnSc4	řada
hudebních	hudební	k2eAgNnPc2d1	hudební
děl	dělo	k1gNnPc2	dělo
<g/>
.	.	kIx.	.
</s>
<s>
Operu	oprat	k5eAaPmIp1nS	oprat
na	na	k7c4	na
tento	tento	k3xDgInSc4	tento
námět	námět	k1gInSc4	námět
složili	složit	k5eAaPmAgMnP	složit
například	například	k6eAd1	například
Georg	Georg	k1gMnSc1	Georg
Philipp	Philipp	k1gMnSc1	Philipp
Telemann	Telemann	k1gMnSc1	Telemann
(	(	kIx(	(
<g/>
1761	[number]	k4	1761
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Niccolò	Niccolò	k1gMnSc6	Niccolò
Piccinni	Piccinň	k1gMnSc6	Piccinň
(	(	kIx(	(
<g/>
1765	[number]	k4	1765
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Felix	Felix	k1gMnSc1	Felix
Mendelssohn-Bartholdy	Mendelssohn-Bartholda	k1gFnSc2	Mendelssohn-Bartholda
(	(	kIx(	(
<g/>
Die	Die	k1gMnSc1	Die
Hochzeit	Hochzeit	k1gMnSc1	Hochzeit
des	des	k1gNnPc2	des
Camacho	Camacha	k1gFnSc5	Camacha
<g/>
,	,	kIx,	,
1827	[number]	k4	1827
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Saverio	Saveria	k1gMnSc5	Saveria
Mercadante	Mercadant	k1gMnSc5	Mercadant
(	(	kIx(	(
<g/>
1829	[number]	k4	1829
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Henri	Henri	k1gNnSc1	Henri
Boulanger	Boulangra	k1gFnPc2	Boulangra
(	(	kIx(	(
<g/>
1869	[number]	k4	1869
<g/>
)	)	kIx)	)
a	a	k8xC	a
Manuel	Manuel	k1gMnSc1	Manuel
de	de	k?	de
Falla	Falla	k1gMnSc1	Falla
(	(	kIx(	(
<g/>
El	Ela	k1gFnPc2	Ela
Retablo	Retablo	k1gMnPc2	Retablo
de	de	k?	de
Maese	Maese	k1gFnSc2	Maese
Pedro	Pedro	k1gNnSc1	Pedro
<g/>
,	,	kIx,	,
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejznámější	známý	k2eAgNnSc1d3	nejznámější
je	být	k5eAaImIp3nS	být
zpracování	zpracování	k1gNnSc1	zpracování
Julese	Julese	k1gFnSc2	Julese
Masseneta	Massenet	k1gMnSc2	Massenet
-	-	kIx~	-
Don	Don	k1gMnSc1	Don
Quichotte	Quichott	k1gInSc5	Quichott
<g/>
,	,	kIx,	,
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
<g/>
.	.	kIx.	.
</s>
<s>
Don	Don	k1gMnSc1	Don
Quijote	Quijot	k1gInSc5	Quijot
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
i	i	k9	i
tématem	téma	k1gNnSc7	téma
baletu	balet	k1gInSc2	balet
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Don	Don	k1gMnSc1	Don
Quixote	Quixot	k1gInSc5	Quixot
(	(	kIx(	(
<g/>
Ludwig	Ludwig	k1gMnSc1	Ludwig
Minkus	Minkus	k1gMnSc1	Minkus
<g/>
,	,	kIx,	,
1869	[number]	k4	1869
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
Le	Le	k1gMnSc1	Le
Chevalier	chevalier	k1gMnSc1	chevalier
errant	errant	k1gMnSc1	errant
(	(	kIx(	(
<g/>
Jacques	Jacques	k1gMnSc1	Jacques
Ibert	Iberta	k1gFnPc2	Iberta
<g/>
,	,	kIx,	,
1935	[number]	k4	1935
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
symfonické	symfonický	k2eAgFnPc4d1	symfonická
básně	báseň	k1gFnPc4	báseň
(	(	kIx(	(
<g/>
Richard	Richard	k1gMnSc1	Richard
Strauss	Straussa	k1gFnPc2	Straussa
<g/>
,	,	kIx,	,
1897	[number]	k4	1897
<g/>
)	)	kIx)	)
či	či	k8xC	či
muzikálu	muzikál	k1gInSc2	muzikál
(	(	kIx(	(
<g/>
Mitch	Mitch	k1gMnSc1	Mitch
Leigh	Leigh	k1gMnSc1	Leigh
-	-	kIx~	-
Man	Man	k1gMnSc1	Man
of	of	k?	of
La	la	k1gNnSc2	la
Mancha	Manch	k1gMnSc2	Manch
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
<g/>
,	,	kIx,	,
Jacques	Jacques	k1gMnSc1	Jacques
Brel	Brel	k1gMnSc1	Brel
-	-	kIx~	-
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
homme	hommat	k5eAaPmIp3nS	hommat
de	de	k?	de
la	la	k1gNnSc1	la
Mancha	Mancha	k1gMnSc1	Mancha
<g/>
,	,	kIx,	,
1968	[number]	k4	1968
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
volné	volný	k2eAgInPc4d1	volný
motivy	motiv	k1gInPc4	motiv
této	tento	k3xDgFnSc2	tento
knihy	kniha	k1gFnSc2	kniha
uvedlo	uvést	k5eAaPmAgNnS	uvést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
brněnské	brněnský	k2eAgNnSc4d1	brněnské
Divadlo	divadlo	k1gNnSc4	divadlo
Klauniky	klaunika	k1gFnSc2	klaunika
stejnojmennou	stejnojmenný	k2eAgFnSc4d1	stejnojmenná
hru	hra	k1gFnSc4	hra
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2016	[number]	k4	2016
za	za	k7c4	za
sebou	se	k3xPyFc7	se
již	již	k6eAd1	již
5	[number]	k4	5
000	[number]	k4	000
repríz	repríza	k1gFnPc2	repríza
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Z	z	k7c2	z
Dona	Don	k1gMnSc2	Don
Quijota	Quijot	k1gMnSc2	Quijot
čerpala	čerpat	k5eAaImAgFnS	čerpat
inspiraci	inspirace	k1gFnSc3	inspirace
i	i	k9	i
literatura	literatura	k1gFnSc1	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Skotská	skotský	k2eAgFnSc1d1	skotská
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
Charlotte	Charlott	k1gInSc5	Charlott
Lennoxová	Lennoxový	k2eAgFnSc1d1	Lennoxová
jej	on	k3xPp3gMnSc4	on
použila	použít	k5eAaPmAgFnS	použít
jako	jako	k8xC	jako
námět	námět	k1gInSc4	námět
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
nejúspěšnější	úspěšný	k2eAgInSc4d3	nejúspěšnější
román	román	k1gInSc4	román
The	The	k1gMnSc5	The
Female	Femal	k1gMnSc5	Femal
Quixote	Quixot	k1gMnSc5	Quixot
<g/>
,	,	kIx,	,
or	or	k?	or
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
Adventrues	Adventrues	k1gMnSc1	Adventrues
of	of	k?	of
Arabella	Arabella	k1gMnSc1	Arabella
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
"	"	kIx"	"
<g/>
Quijotka	Quijotka	k1gFnSc1	Quijotka
aneb	aneb	k?	aneb
Arabellina	Arabellina	k1gFnSc1	Arabellina
dobrodružství	dobrodružství	k1gNnSc2	dobrodružství
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
České	český	k2eAgInPc1d1	český
překlady	překlad	k1gInPc1	překlad
==	==	k?	==
</s>
</p>
<p>
<s>
Bláznivý	bláznivý	k2eAgMnSc1d1	bláznivý
rytíř	rytíř	k1gMnSc1	rytíř
<g/>
:	:	kIx,	:
kratochvilné	kratochvilný	k2eAgNnSc1d1	kratochvilné
čtení	čtení	k1gNnSc1	čtení
pro	pro	k7c4	pro
lid	lid	k1gInSc4	lid
(	(	kIx(	(
<g/>
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Josef	Josef	k1gMnSc1	Josef
Pečírka	Pečírka	k1gFnSc1	Pečírka
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1864	[number]	k4	1864
<g/>
,	,	kIx,	,
2	[number]	k4	2
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Don	Don	k1gMnSc1	Don
Quijote	Quijot	k1gInSc5	Quijot
de	de	k?	de
la	la	k1gNnSc1	la
Mancha	Mancha	k1gMnSc1	Mancha
(	(	kIx(	(
<g/>
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Josef	Josef	k1gMnSc1	Josef
Bojislav	Bojislav	k1gMnSc1	Bojislav
Pichl	Pichl	k1gMnSc1	Pichl
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1866	[number]	k4	1866
<g/>
–	–	k?	–
<g/>
1868	[number]	k4	1868
<g/>
,	,	kIx,	,
2	[number]	k4	2
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Don	Don	k1gMnSc1	Don
Quijote	Quijot	k1gInSc5	Quijot
de	de	k?	de
la	la	k1gNnSc1	la
Mancha	Mancha	k1gMnSc1	Mancha
(	(	kIx(	(
<g/>
přeložil	přeložit	k5eAaPmAgMnS	přeložit
J.	J.	kA	J.
V.	V.	kA	V.
Kovář	Kovář	k1gMnSc1	Kovář
<g/>
,	,	kIx,	,
Telč	Telč	k1gFnSc1	Telč
1897	[number]	k4	1897
<g/>
,	,	kIx,	,
literatura	literatura	k1gFnSc1	literatura
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
<g/>
,	,	kIx,	,
zkrácená	zkrácený	k2eAgFnSc1d1	zkrácená
verze	verze	k1gFnSc1	verze
<g/>
,	,	kIx,	,
uveden	uvést	k5eAaPmNgMnS	uvést
jako	jako	k8xC	jako
překladatel	překladatel	k1gMnSc1	překladatel
J.	J.	kA	J.
V.	V.	kA	V.
Kabelík	Kabelík	k1gMnSc1	Kabelík
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Důmyslný	důmyslný	k2eAgMnSc1d1	důmyslný
rytíř	rytíř	k1gMnSc1	rytíř
Don	Don	k1gMnSc1	Don
Quijote	Quijot	k1gInSc5	Quijot
de	de	k?	de
la	la	k1gNnSc1	la
Mancha	Mancha	k1gMnSc1	Mancha
(	(	kIx(	(
<g/>
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Antonín	Antonín	k1gMnSc1	Antonín
Pikhart	pikhart	k1gMnSc1	pikhart
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1899	[number]	k4	1899
<g/>
,	,	kIx,	,
6	[number]	k4	6
sv.	sv.	kA	sv.
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1926	[number]	k4	1926
<g/>
,	,	kIx,	,
jen	jen	k9	jen
první	první	k4xOgInSc4	první
díl	díl	k1gInSc4	díl
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Důmyslný	důmyslný	k2eAgMnSc1d1	důmyslný
rytíř	rytíř	k1gMnSc1	rytíř
Don	Don	k1gMnSc1	Don
Quijote	Quijot	k1gInSc5	Quijot
de	de	k?	de
la	la	k1gNnSc1	la
Mancha	Mancha	k1gMnSc1	Mancha
(	(	kIx(	(
<g/>
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Eduard	Eduard	k1gMnSc1	Eduard
Drobílek	Drobílek	k1gMnSc1	Drobílek
<g/>
,	,	kIx,	,
Vinohrady	Vinohrady	k1gInPc1	Vinohrady
1914	[number]	k4	1914
<g/>
,	,	kIx,	,
jen	jen	k9	jen
první	první	k4xOgInSc4	první
díl	díl	k1gInSc4	díl
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Duchaplný	duchaplný	k2eAgMnSc1d1	duchaplný
rytíř	rytíř	k1gMnSc1	rytíř
Don	Don	k1gMnSc1	Don
Quijote	Quijot	k1gInSc5	Quijot
de	de	k?	de
la	la	k1gNnSc7	la
Manche	Manch	k1gFnSc2	Manch
(	(	kIx(	(
<g/>
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Hugo	Hugo	k1gMnSc1	Hugo
Kosterka	Kosterka	k1gFnSc1	Kosterka
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1924	[number]	k4	1924
<g/>
,	,	kIx,	,
4	[number]	k4	4
sv.	sv.	kA	sv.
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
2	[number]	k4	2
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
překlad	překlad	k1gInSc1	překlad
dále	daleko	k6eAd2	daleko
převyprávěn	převyprávět	k5eAaPmNgInS	převyprávět
a	a	k8xC	a
upraven	upravit	k5eAaPmNgInS	upravit
pro	pro	k7c4	pro
knihu	kniha	k1gFnSc4	kniha
Příběhy	příběh	k1gInPc1	příběh
dona	don	k1gMnSc2	don
Quijota	Quijot	k1gMnSc2	Quijot
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
1941	[number]	k4	1941
<g/>
,	,	kIx,	,
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
Jaromíra	Jaromír	k1gMnSc4	Jaromír
Johna	John	k1gMnSc4	John
</s>
</p>
<p>
<s>
Důmyslný	důmyslný	k2eAgMnSc1d1	důmyslný
rytíř	rytíř	k1gMnSc1	rytíř
Don	Don	k1gMnSc1	Don
Quijote	Quijot	k1gInSc5	Quijot
de	de	k?	de
la	la	k1gNnSc7	la
Manche	Manch	k1gFnSc2	Manch
(	(	kIx(	(
<g/>
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Marie	Marie	k1gFnSc1	Marie
L.	L.	kA	L.
Kühnlová	Kühnlová	k1gFnSc1	Kühnlová
<g/>
,	,	kIx,	,
3	[number]	k4	3
<g/>
.	.	kIx.	.
vydání	vydání	k1gNnSc1	vydání
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1928	[number]	k4	1928
<g/>
,	,	kIx,	,
2	[number]	k4	2
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Důmyslný	důmyslný	k2eAgMnSc1d1	důmyslný
rytíř	rytíř	k1gMnSc1	rytíř
Don	Don	k1gMnSc1	Don
Quijote	Quijot	k1gInSc5	Quijot
de	de	k?	de
la	la	k1gNnSc1	la
Mancha	Mancha	k1gMnSc1	Mancha
(	(	kIx(	(
<g/>
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Václav	Václav	k1gMnSc1	Václav
Černý	Černý	k1gMnSc1	Černý
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
4	[number]	k4	4
sv.	sv.	kA	sv.
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1947	[number]	k4	1947
<g/>
,	,	kIx,	,
2	[number]	k4	2
sv.	sv.	kA	sv.
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1966	[number]	k4	1966
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2012	[number]	k4	2012
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Důmyslný	důmyslný	k2eAgMnSc1d1	důmyslný
rytíř	rytíř	k1gMnSc1	rytíř
Don	Don	k1gMnSc1	Don
Quijote	Quijot	k1gInSc5	Quijot
de	de	k?	de
la	la	k1gNnSc1	la
Mancha	Mancha	k1gMnSc1	Mancha
(	(	kIx(	(
<g/>
přeložil	přeložit	k5eAaPmAgMnS	přeložit
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Šmíd	Šmíd	k1gMnSc1	Šmíd
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1952	[number]	k4	1952
<g/>
,	,	kIx,	,
2	[number]	k4	2
sv.	sv.	kA	sv.
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1982	[number]	k4	1982
<g/>
,	,	kIx,	,
2	[number]	k4	2
sv.	sv.	kA	sv.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Don	Don	k1gMnSc1	Don
Quijote	Quijot	k1gInSc5	Quijot
de	de	k?	de
la	la	k1gNnSc1	la
Mancha	Mancha	k1gFnSc1	Mancha
<g/>
:	:	kIx,	:
verze	verze	k1gFnSc1	verze
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
(	(	kIx(	(
<g/>
překladatel	překladatel	k1gMnSc1	překladatel
není	být	k5eNaImIp3nS	být
zřejmý	zřejmý	k2eAgMnSc1d1	zřejmý
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2014	[number]	k4	2014
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Důmyslný	důmyslný	k2eAgMnSc1d1	důmyslný
rytíř	rytíř	k1gMnSc1	rytíř
Don	Don	k1gMnSc1	Don
Quijote	Quijot	k1gInSc5	Quijot
de	de	k?	de
La	la	k1gNnSc1	la
Mancha	Mancha	k1gMnSc1	Mancha
(	(	kIx(	(
<g/>
překladatel	překladatel	k1gMnSc1	překladatel
není	být	k5eNaImIp3nS	být
zřejmý	zřejmý	k2eAgMnSc1d1	zřejmý
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
<g/>
Vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
několik	několik	k4yIc1	několik
kratších	krátký	k2eAgNnPc2d2	kratší
převyprávění	převyprávění	k1gNnPc2	převyprávění
a	a	k8xC	a
adaptací	adaptace	k1gFnSc7	adaptace
původní	původní	k2eAgNnPc1d1	původní
díla	dílo	k1gNnPc1	dílo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Kolektiv	kolektiv	k1gInSc1	kolektiv
autorů	autor	k1gMnPc2	autor
<g/>
.	.	kIx.	.
</s>
<s>
Literatura	literatura	k1gFnSc1	literatura
pro	pro	k7c4	pro
1	[number]	k4	1
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
SOŠ	SOŠ	kA	SOŠ
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
SPN	SPN	kA	SPN
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7235	[number]	k4	7235
<g/>
-	-	kIx~	-
<g/>
128	[number]	k4	128
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Miguel	Miguel	k1gMnSc1	Miguel
de	de	k?	de
Cervantes	Cervantes	k1gMnSc1	Cervantes
y	y	k?	y
Saavedra	Saavedra	k1gFnSc1	Saavedra
<g/>
,	,	kIx,	,
s.	s.	k?	s.
181	[number]	k4	181
<g/>
-	-	kIx~	-
<g/>
182	[number]	k4	182
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FOUSEK	Fousek	k1gMnSc1	Fousek
<g/>
,	,	kIx,	,
Michal	Michal	k1gMnSc1	Michal
<g/>
.	.	kIx.	.
</s>
<s>
Don	Don	k1gMnSc1	Don
Quijote	Quijot	k1gInSc5	Quijot
v	v	k7c6	v
proměnách	proměna	k1gFnPc6	proměna
času	čas	k1gInSc2	čas
a	a	k8xC	a
prostoru	prostor	k1gInSc2	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
,	,	kIx,	,
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7308	[number]	k4	7308
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
98	[number]	k4	98
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Boj	boj	k1gInSc1	boj
s	s	k7c7	s
větrnými	větrný	k2eAgInPc7d1	větrný
mlýny	mlýn	k1gInPc7	mlýn
</s>
</p>
<p>
<s>
Rocinanta	Rocinanta	k1gFnSc1	Rocinanta
</s>
</p>
<p>
<s>
Španělská	španělský	k2eAgFnSc1d1	španělská
literatura	literatura	k1gFnSc1	literatura
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Důmyslný	důmyslný	k2eAgMnSc1d1	důmyslný
rytíř	rytíř	k1gMnSc1	rytíř
Don	Don	k1gMnSc1	Don
Quijote	Quijot	k1gInSc5	Quijot
de	de	k?	de
la	la	k1gNnSc2	la
Mancha	Manch	k1gMnSc2	Manch
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Don	dona	k1gFnPc2	dona
Quijote	Quijot	k1gInSc5	Quijot
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Don	dona	k1gFnPc2	dona
Quijote	Quijot	k1gInSc5	Quijot
de	de	k?	de
la	la	k1gNnSc1	la
Mancha	Mancha	k1gFnSc1	Mancha
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g/>
)	)	kIx)	)
El	Ela	k1gFnPc2	Ela
ingenioso	ingenioso	k6eAd1	ingenioso
hidalgo	hidalgo	k1gMnSc1	hidalgo
Don	Don	k1gMnSc1	Don
Quijote	Quijot	k1gInSc5	Quijot
de	de	k?	de
la	la	k1gNnSc1	la
Mancha	Mancha	k1gFnSc1	Mancha
(	(	kIx(	(
<g/>
E-Book	E-Book	k1gInSc1	E-Book
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g/>
)	)	kIx)	)
Scan	Scan	k1gNnSc4	Scan
úvodní	úvodní	k2eAgFnSc2d1	úvodní
strany	strana	k1gFnSc2	strana
prvního	první	k4xOgNnSc2	první
vydání	vydání	k1gNnSc4	vydání
první	první	k4xOgFnSc2	první
části	část	k1gFnSc2	část
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g/>
)	)	kIx)	)
Scan	Scan	k1gNnSc1	Scan
Avellanedovy	Avellanedův	k2eAgFnSc2d1	Avellanedův
neoficiální	neoficiální	k2eAgFnSc2d1	neoficiální
druhé	druhý	k4xOgFnSc2	druhý
části	část	k1gFnSc2	část
originální	originální	k2eAgInSc4d1	originální
text	text	k1gInSc4	text
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
španělsky	španělsky	k6eAd1	španělsky
<g/>
)	)	kIx)	)
Scan	Scan	k1gNnSc4	Scan
Cervantesovy	Cervantesův	k2eAgFnSc2d1	Cervantesova
druhé	druhý	k4xOgFnSc2	druhý
části	část	k1gFnSc2	část
<g/>
,	,	kIx,	,
první	první	k4xOgMnPc1	první
vydaní	vydaný	k2eAgMnPc1d1	vydaný
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
Don	Don	k1gMnSc1	Don
Quijote	Quijot	k1gInSc5	Quijot
na	na	k7c6	na
Projektu	projekt	k1gInSc6	projekt
Gutenberg	Gutenberg	k1gMnSc1	Gutenberg
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Don	Don	k1gMnSc1	Don
Quijote	Quijot	k1gInSc5	Quijot
na	na	k7c4	na
Cervantes	Cervantesa	k1gFnPc2	Cervantesa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
28	[number]	k4	28
ilustrací	ilustrace	k1gFnPc2	ilustrace
k	k	k7c3	k
dílu	dílo	k1gNnSc3	dílo
Don	dona	k1gFnPc2	dona
Quijote	Quijot	k1gInSc5	Quijot
od	od	k7c2	od
Stefana	Stefan	k1gMnSc4	Stefan
Marta	Mars	k1gMnSc2	Mars
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Is	Is	k1gMnSc5	Is
There	Ther	k1gMnSc5	Ther
a	a	k8xC	a
Hidden	Hiddna	k1gFnPc2	Hiddna
Jewish	Jewisha	k1gFnPc2	Jewisha
Meaning	Meaning	k1gInSc1	Meaning
in	in	k?	in
Don	dona	k1gFnPc2	dona
Quixote	Quixot	k1gMnSc5	Quixot
<g/>
?	?	kIx.	?
</s>
<s>
by	by	kYmCp3nS	by
Michael	Michael	k1gMnSc1	Michael
McGaha	McGaha	k1gMnSc1	McGaha
</s>
</p>
<p>
<s>
Rytíř	Rytíř	k1gMnSc1	Rytíř
smutné	smutný	k2eAgFnSc2d1	smutná
postavy	postava	k1gFnSc2	postava
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
</s>
</p>
<p>
<s>
Publikace	publikace	k1gFnSc1	publikace
k	k	k7c3	k
400	[number]	k4	400
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc4	výročí
Dona	Don	k1gMnSc2	Don
Quijota	Quijot	k1gMnSc2	Quijot
na	na	k7c6	na
serveru	server	k1gInSc6	server
iliteratura	iliteratura	k1gFnSc1	iliteratura
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Reflexe	reflexe	k1gFnSc1	reflexe
významných	významný	k2eAgFnPc2d1	významná
českých	český	k2eAgFnPc2d1	Česká
osobností	osobnost	k1gFnPc2	osobnost
na	na	k7c4	na
téma	téma	k1gNnSc4	téma
Don	dona	k1gFnPc2	dona
Quijote	Quijot	k1gMnSc5	Quijot
</s>
</p>
