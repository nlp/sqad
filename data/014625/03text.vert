<s>
Diamantový	diamantový	k2eAgInSc1d1
brus	brus	k1gInSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
není	být	k5eNaImIp3nS
dostatečně	dostatečně	k6eAd1
ozdrojován	ozdrojován	k2eAgMnSc1d1
a	a	k8xC
může	moct	k5eAaImIp3nS
tedy	tedy	k9
obsahovat	obsahovat	k5eAaImF
informace	informace	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
ověřit	ověřit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
Jste	být	k5eAaImIp2nP
<g/>
-li	-li	k?
s	s	k7c7
popisovaným	popisovaný	k2eAgInSc7d1
předmětem	předmět	k1gInSc7
seznámeni	seznámen	k2eAgMnPc1d1
<g/>
,	,	kIx,
pomozte	pomoct	k5eAaPmRp2nPwC
doložit	doložit	k5eAaPmF
uvedená	uvedený	k2eAgNnPc4d1
tvrzení	tvrzení	k1gNnPc4
doplněním	doplnění	k1gNnSc7
referencí	reference	k1gFnPc2
na	na	k7c4
věrohodné	věrohodný	k2eAgInPc4d1
zdroje	zdroj	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Diamantový	diamantový	k2eAgInSc1d1
brus	brus	k1gInSc1
</s>
<s>
Diamantový	diamantový	k2eAgInSc1d1
brus	brus	k1gInSc1
je	být	k5eAaImIp3nS
způsob	způsob	k1gInSc4
opracování	opracování	k1gNnSc4
surového	surový	k2eAgInSc2d1
diamantu	diamant	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Brus	brus	k1gInSc1
označuje	označovat	k5eAaImIp3nS
souhrnně	souhrnně	k6eAd1
proporce	proporce	k1gFnSc1
<g/>
,	,	kIx,
symetrii	symetrie	k1gFnSc4
a	a	k8xC
lesk	lesk	k1gInSc4
diamantu	diamant	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Tvar	tvar	k1gInSc1
brusu	brus	k1gInSc2
je	být	k5eAaImIp3nS
jedním	jeden	k4xCgNnSc7
ze	z	k7c2
základních	základní	k2eAgNnPc2d1
kritérií	kritérion	k1gNnPc2
v	v	k7c6
hodnocení	hodnocení	k1gNnSc6
diamantů	diamant	k1gInPc2
<g/>
,	,	kIx,
tzv.	tzv.	kA
4C	4C	k4
(	(	kIx(
<g/>
Cut	Cut	k1gFnSc1
<g/>
,	,	kIx,
Color	Color	k1gInSc1
<g/>
,	,	kIx,
Carat	Carat	k1gInSc1
<g/>
,	,	kIx,
Clarity	Clarit	k1gInPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
překladu	překlad	k1gInSc6
brus	brus	k1gInSc1
a	a	k8xC
barva	barva	k1gFnSc1
diamantu	diamant	k1gInSc2
<g/>
,	,	kIx,
váha	váha	k1gFnSc1
a	a	k8xC
čistota	čistota	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Broušení	broušení	k1gNnSc1
diamantů	diamant	k1gInPc2
je	být	k5eAaImIp3nS
šperkařská	šperkařský	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
<g/>
,	,	kIx,
při	při	k7c6
které	který	k3yQgFnSc6,k3yRgFnSc6,k3yIgFnSc6
se	se	k3xPyFc4
ze	z	k7c2
surových	surový	k2eAgInPc2d1
diamantů	diamant	k1gInPc2
broušením	broušení	k1gNnSc7
vytváří	vytvářet	k5eAaImIp3nP,k5eAaPmIp3nP
drahokamy	drahokam	k1gInPc1
použitelné	použitelný	k2eAgInPc1d1
do	do	k7c2
šperků	šperk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
větších	veliký	k2eAgInPc2d2
a	a	k8xC
dražších	drahý	k2eAgInPc2d2
kamenů	kámen	k1gInPc2
je	být	k5eAaImIp3nS
broušení	broušení	k1gNnSc1
prováděno	provádět	k5eAaImNgNnS
fasérem	fasér	k1gInSc7
(	(	kIx(
<g/>
odborník	odborník	k1gMnSc1
<g/>
)	)	kIx)
ručně	ručně	k6eAd1
<g/>
,	,	kIx,
u	u	k7c2
menších	malý	k2eAgInPc2d2
kamenů	kámen	k1gInPc2
se	se	k3xPyFc4
dnes	dnes	k6eAd1
většinou	většinou	k6eAd1
používá	používat	k5eAaImIp3nS
strojového	strojový	k2eAgNnSc2d1
broušení	broušení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Diamantový	diamantový	k2eAgInSc1d1
brus	brus	k1gInSc1
se	se	k3xPyFc4
stále	stále	k6eAd1
zdokonaluje	zdokonalovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
průkopníky	průkopník	k1gMnPc4
v	v	k7c6
tomto	tento	k3xDgInSc6
oboru	obor	k1gInSc6
patří	patřit	k5eAaImIp3nS
matematik	matematik	k1gMnSc1
Marcel	Marcel	k1gMnSc1
Tolkowsky	Tolkowska	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1919	#num#	k4
pomocí	pomocí	k7c2
složitých	složitý	k2eAgInPc2d1
matematických	matematický	k2eAgInPc2d1
výpočtů	výpočet	k1gInPc2
zdokonalil	zdokonalit	k5eAaPmAgInS
briliantový	briliantový	k2eAgInSc1d1
brus	brus	k1gInSc1
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
docílil	docílit	k5eAaPmAgMnS
optimálního	optimální	k2eAgInSc2d1
odrazu	odraz	k1gInSc2
světla	světlo	k1gNnSc2
v	v	k7c6
celém	celý	k2eAgNnSc6d1
spektru	spektrum	k1gNnSc6
barev	barva	k1gFnPc2
a	a	k8xC
intenzitě	intenzita	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Diamant	diamant	k1gInSc1
s	s	k7c7
tímto	tento	k3xDgInSc7
brusem	brus	k1gInSc7
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
briliant	briliant	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Stupně	stupeň	k1gInPc1
kvality	kvalita	k1gFnSc2
brusu	brus	k1gInSc2
</s>
<s>
Výbrus	výbrus	k1gInSc1
diamantu	diamant	k1gInSc2
je	být	k5eAaImIp3nS
hodnocen	hodnotit	k5eAaImNgInS
z	z	k7c2
hlediska	hledisko	k1gNnSc2
kvality	kvalita	k1gFnSc2
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
provedení	provedení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
U	u	k7c2
kvality	kvalita	k1gFnSc2
brusu	brus	k1gInSc2
diamantů	diamant	k1gInPc2
se	se	k3xPyFc4
hodnotí	hodnotit	k5eAaImIp3nS
</s>
<s>
Proporce	proporce	k1gFnSc1
–	–	k?
úhly	úhel	k1gInPc1
a	a	k8xC
procentuální	procentuální	k2eAgInPc1d1
poměry	poměr	k1gInPc1
jednotlivých	jednotlivý	k2eAgFnPc2d1
částí	část	k1gFnPc2
brusu	brus	k1gInSc2
</s>
<s>
Nejlépe	dobře	k6eAd3
vybroušený	vybroušený	k2eAgInSc1d1
kámen	kámen	k1gInSc1
má	mít	k5eAaImIp3nS
dokonalý	dokonalý	k2eAgInSc4d1
soulad	soulad	k1gInSc4
mezi	mezi	k7c7
brilancí	brilance	k1gFnSc7
<g/>
,	,	kIx,
ohněm	oheň	k1gInSc7
a	a	k8xC
jiskřením	jiskření	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejdokonalejším	dokonalý	k2eAgInSc7d3
typem	typ	k1gInSc7
brusu	brus	k1gInSc2
je	být	k5eAaImIp3nS
stále	stále	k6eAd1
briliantový	briliantový	k2eAgInSc1d1
brus	brus	k1gInSc1
s	s	k7c7
57	#num#	k4
fasetami	faseta	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
jsou	být	k5eAaImIp3nP
vybroušeny	vybrousit	k5eAaPmNgFnP
do	do	k7c2
přesně	přesně	k6eAd1
stanovených	stanovený	k2eAgFnPc2d1
proporcí	proporce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Povrchové	povrchový	k2eAgNnSc4d1
opracování	opracování	k1gNnSc4
–	–	k?
kvalita	kvalita	k1gFnSc1
vybroušeného	vybroušený	k2eAgInSc2d1
povrchu	povrch	k1gInSc2
faset	faseta	k1gFnPc2
(	(	kIx(
<g/>
zda	zda	k8xS
je	být	k5eAaImIp3nS
dokonale	dokonale	k6eAd1
vyleštěný	vyleštěný	k2eAgInSc1d1
<g/>
,	,	kIx,
neobsahuje	obsahovat	k5eNaImIp3nS
stopy	stopa	k1gFnPc4
po	po	k7c6
broušení	broušení	k1gNnSc6
apod.	apod.	kA
<g/>
)	)	kIx)
</s>
<s>
Nejčastější	častý	k2eAgFnPc1d3
vady	vada	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
vyskytují	vyskytovat	k5eAaImIp3nP
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
rýhy	rýha	k1gFnPc1
a	a	k8xC
škrábance	škrábanec	k1gInPc1
<g/>
,	,	kIx,
linie	linie	k1gFnPc1
po	po	k7c4
broušení	broušení	k1gNnPc4
<g/>
,	,	kIx,
prohlubně	prohlubeň	k1gFnPc4
a	a	k8xC
důlky	důlek	k1gInPc4
<g/>
,	,	kIx,
abraze	abraze	k1gFnPc4
na	na	k7c6
hranách	hrana	k1gFnPc6
faset	faseta	k1gFnPc2
<g/>
,	,	kIx,
stopy	stopa	k1gFnPc4
po	po	k7c4
broušení	broušení	k1gNnSc4
(	(	kIx(
<g/>
povrchová	povrchový	k2eAgFnSc1d1
oxidace	oxidace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
naturálie	naturálie	k1gFnPc4
a	a	k8xC
nadbytečné	nadbytečný	k2eAgFnPc4d1
fasety	faseta	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Symetrie	symetrie	k1gFnSc1
brusu	brus	k1gInSc2
–	–	k?
hodnotí	hodnotit	k5eAaImIp3nS
<g/>
,	,	kIx,
zda	zda	k8xS
jsou	být	k5eAaImIp3nP
všechny	všechen	k3xTgFnPc4
fasety	faseta	k1gFnPc4
stejně	stejně	k6eAd1
velké	velký	k2eAgFnPc4d1
a	a	k8xC
broušené	broušený	k2eAgFnPc4d1
pod	pod	k7c7
stejnými	stejný	k2eAgInPc7d1
úhly	úhel	k1gInPc7
<g/>
,	,	kIx,
zda	zda	k8xS
je	být	k5eAaImIp3nS
tabulka	tabulka	k1gFnSc1
a	a	k8xC
kaleta	kaleta	k1gFnSc1
přesně	přesně	k6eAd1
ve	v	k7c6
středu	střed	k1gInSc6
atd.	atd.	kA
</s>
<s>
Pět	pět	k4xCc4
stupňů	stupeň	k1gInPc2
mezinárodní	mezinárodní	k2eAgFnSc2d1
stupnice	stupnice	k1gFnSc2
</s>
<s>
Excellent	Excellent	k1gInSc1
<g/>
/	/	kIx~
<g/>
Ideal	Ideal	k1gInSc1
cut	cut	k?
</s>
<s>
Excelentní	excelentní	k2eAgFnSc1d1
<g/>
,	,	kIx,
neboli	neboli	k8xC
ideální	ideální	k2eAgInSc1d1
výbrus	výbrus	k1gInSc1
označuje	označovat	k5eAaImIp3nS
špičkově	špičkově	k6eAd1
vybroušené	vybroušený	k2eAgInPc4d1
diamanty	diamant	k1gInPc4
té	ten	k3xDgFnSc2
nejvyšší	vysoký	k2eAgFnSc2d3
kvality	kvalita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
této	tento	k3xDgFnSc2
kategorie	kategorie	k1gFnSc2
spadá	spadat	k5eAaImIp3nS,k5eAaPmIp3nS
pouze	pouze	k6eAd1
cca	cca	kA
3	#num#	k4
<g/>
%	%	kIx~
produkce	produkce	k1gFnSc1
všech	všecek	k3xTgInPc2
diamantů	diamant	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Diamanty	diamant	k1gInPc1
této	tento	k3xDgFnSc2
třídy	třída	k1gFnSc2
odrážejí	odrážet	k5eAaImIp3nP
téměř	téměř	k6eAd1
všechno	všechen	k3xTgNnSc4
světlo	světlo	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
na	na	k7c4
ně	on	k3xPp3gMnPc4
dopadá	dopadat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Very	Vera	k1gFnPc1
good	good	k6eAd1
</s>
<s>
Tato	tento	k3xDgFnSc1
třída	třída	k1gFnSc1
označuje	označovat	k5eAaImIp3nS
brus	brus	k1gInSc4
s	s	k7c7
velmi	velmi	k6eAd1
dobrým	dobrý	k2eAgNnSc7d1
provedením	provedení	k1gNnSc7
<g/>
,	,	kIx,
díky	díky	k7c3
němuž	jenž	k3xRgNnSc3
kámen	kámen	k1gInSc1
odráží	odrážet	k5eAaImIp3nS
většinu	většina	k1gFnSc4
světla	světlo	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
na	na	k7c4
něj	on	k3xPp3gMnSc4
dopadá	dopadat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
oko	oko	k1gNnSc4
laika	laik	k1gMnSc4
je	být	k5eAaImIp3nS
tato	tento	k3xDgFnSc1
třída	třída	k1gFnSc1
nerozeznatelná	rozeznatelný	k2eNgFnSc1d1
od	od	k7c2
třídy	třída	k1gFnSc2
Ideal	Ideal	k1gInSc1
cut	cut	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asi	asi	k9
15	#num#	k4
%	%	kIx~
všech	všecek	k3xTgInPc2
vybroušených	vybroušený	k2eAgInPc2d1
diamantů	diamant	k1gInPc2
spadá	spadat	k5eAaPmIp3nS,k5eAaImIp3nS
do	do	k7c2
této	tento	k3xDgFnSc2
kategorie	kategorie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Good	Good	k6eAd1
</s>
<s>
Do	do	k7c2
této	tento	k3xDgFnSc2
kategorie	kategorie	k1gFnSc2
spadá	spadat	k5eAaImIp3nS,k5eAaPmIp3nS
zhruba	zhruba	k6eAd1
čtvrtina	čtvrtina	k1gFnSc1
diamantové	diamantový	k2eAgFnSc2d1
produkce	produkce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odrážejí	odrážet	k5eAaImIp3nP
většinu	většina	k1gFnSc4
světla	světlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Fair	fair	k6eAd1
<g/>
*	*	kIx~
</s>
<s>
Tato	tento	k3xDgFnSc1
třída	třída	k1gFnSc1
představuje	představovat	k5eAaImIp3nS
ještě	ještě	k9
stále	stále	k6eAd1
dobře	dobře	k6eAd1
vybroušené	vybroušený	k2eAgInPc4d1
diamanty	diamant	k1gInPc4
<g/>
,	,	kIx,
ale	ale	k8xC
odraz	odraz	k1gInSc1
světla	světlo	k1gNnSc2
je	být	k5eAaImIp3nS
o	o	k7c6
poznání	poznání	k1gNnSc6
menší	malý	k2eAgInPc1d2
a	a	k8xC
diamanty	diamant	k1gInPc1
se	se	k3xPyFc4
tak	tak	k9
méně	málo	k6eAd2
třpytí	třpytit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Poor	Poor	k1gMnSc1
</s>
<s>
Tyto	tento	k3xDgInPc1
diamanty	diamant	k1gInPc1
nesplňují	splňovat	k5eNaImIp3nP
nároky	nárok	k1gInPc4
na	na	k7c4
kvalitní	kvalitní	k2eAgInSc4d1
brus	brus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
světla	světlo	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
na	na	k7c4
diamant	diamant	k1gInSc4
dopadá	dopadat	k5eAaImIp3nS
<g/>
,	,	kIx,
se	se	k3xPyFc4
ztrácí	ztrácet	k5eAaImIp3nS
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
kámen	kámen	k1gInSc1
přichází	přicházet	k5eAaImIp3nS
o	o	k7c4
svůj	svůj	k3xOyFgInSc4
třpyt	třpyt	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgInPc1d1
parametry	parametr	k1gInPc1
brusu	brus	k1gInSc2
<g/>
,	,	kIx,
vztahující	vztahující	k2eAgNnPc4d1
se	se	k3xPyFc4
k	k	k7c3
jeho	jeho	k3xOp3gNnSc3
hodnocení	hodnocení	k1gNnSc3
</s>
<s>
Brilance	brilance	k1gFnSc1
</s>
<s>
Hodnotí	hodnotit	k5eAaImIp3nS
množství	množství	k1gNnSc1
odraženého	odražený	k2eAgNnSc2d1
bílého	bílý	k2eAgNnSc2d1
světla	světlo	k1gNnSc2
z	z	k7c2
vybroušeného	vybroušený	k2eAgInSc2d1
kamene	kámen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Oheň	oheň	k1gInSc1
</s>
<s>
Vyjadřuje	vyjadřovat	k5eAaImIp3nS
spektrální	spektrální	k2eAgFnPc4d1
barvy	barva	k1gFnPc4
<g/>
,	,	kIx,
do	do	k7c2
kterých	který	k3yQgInPc2,k3yIgInPc2,k3yRgInPc2
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
světlo	světlo	k1gNnSc4
vycházející	vycházející	k2eAgNnSc4d1
z	z	k7c2
vybroušeného	vybroušený	k2eAgInSc2d1
kamene	kámen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Jiskra	jiskra	k1gFnSc1
</s>
<s>
Znamená	znamenat	k5eAaImIp3nS
stupeň	stupeň	k1gInSc1
hry	hra	k1gFnSc2
barev	barva	k1gFnPc2
při	při	k7c6
pohybu	pohyb	k1gInSc2
vybroušeným	vybroušený	k2eAgInSc7d1
kamenem	kámen	k1gInSc7
nebo	nebo	k8xC
při	při	k7c6
pohybu	pohyb	k1gInSc6
hlavy	hlava	k1gFnSc2
pozorovatele	pozorovatel	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Diamantové	diamantový	k2eAgInPc1d1
brusy	brus	k1gInPc1
</s>
<s>
Výbrusy	výbrus	k1gInPc1
diamantu	diamant	k1gInSc2
</s>
<s>
Diamantové	diamantový	k2eAgNnSc1d1
řezání	řezání	k1gNnSc1
</s>
<s>
Brilliant	Brilliant	k1gMnSc1
<g/>
,	,	kIx,
tj.	tj.	kA
kulatý	kulatý	k2eAgInSc1d1
výbrus	výbrus	k1gInSc4
-Nejznámější	-Nejznámý	k2eAgInSc4d2
a	a	k8xC
nejrozšířenější	rozšířený	k2eAgInSc4d3
brus	brus	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
57	#num#	k4
faset	faseta	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
33	#num#	k4
na	na	k7c6
koruně	koruna	k1gFnSc6
a	a	k8xC
24	#num#	k4
na	na	k7c6
pavilonu	pavilon	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Korunou	koruna	k1gFnSc7
diamantu	diamant	k1gInSc2
označujeme	označovat	k5eAaImIp1nP
jeho	jeho	k3xOp3gFnSc4
horní	horní	k2eAgFnSc4d1
část	část	k1gFnSc4
<g/>
,	,	kIx,
pavilonem	pavilon	k1gInSc7
naopak	naopak	k6eAd1
spodní	spodní	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Uprostřed	uprostřed	k7c2
briliantu	briliant	k1gInSc2
je	být	k5eAaImIp3nS
tenký	tenký	k2eAgInSc4d1
neleštěný	leštěný	k2eNgInSc4d1
pás	pás	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Světlo	světlo	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
prochází	procházet	k5eAaImIp3nS
briliantem	briliant	k1gInSc7
<g/>
,	,	kIx,
se	se	k3xPyFc4
díky	díky	k7c3
tomuto	tento	k3xDgInSc3
tvaru	tvar	k1gInSc3
odráží	odrážet	k5eAaImIp3nS
tím	ten	k3xDgInSc7
nejlepším	dobrý	k2eAgInSc7d3
způsobem	způsob	k1gInSc7
a	a	k8xC
při	při	k7c6
maximální	maximální	k2eAgFnSc6d1
intenzitě	intenzita	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomu	ten	k3xDgNnSc3
jsou	být	k5eAaImIp3nP
šperky	šperk	k1gInPc1
s	s	k7c7
brilianty	briliant	k1gInPc7
tak	tak	k6eAd1
zářivé	zářivý	k2eAgNnSc1d1
a	a	k8xC
třpytivé	třpytivý	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Princess	Princess	k1gInSc1
-	-	kIx~
V	v	k7c6
poslední	poslední	k2eAgFnSc6d1
době	doba	k1gFnSc6
velmi	velmi	k6eAd1
populární	populární	k2eAgInSc1d1
brus	brus	k1gInSc1
u	u	k7c2
zásnubních	zásnubní	k2eAgInPc2d1
a	a	k8xC
zásnubních	zásnubní	k2eAgInPc2d1
šperků	šperk	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
čtyřboký	čtyřboký	k2eAgInSc4d1
brus	brus	k1gInSc4
s	s	k7c7
ostrými	ostrý	k2eAgInPc7d1
rohy	roh	k1gInPc7
<g/>
,	,	kIx,
někdy	někdy	k6eAd1
také	také	k9
trojboký	trojboký	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Má	mít	k5eAaImIp3nS
velmi	velmi	k6eAd1
dobrý	dobrý	k2eAgInSc4d1
oheň	oheň	k1gInSc4
a	a	k8xC
vysokou	vysoký	k2eAgFnSc4d1
brilanci	brilance	k1gFnSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
ale	ale	k9
náročný	náročný	k2eAgMnSc1d1
jak	jak	k8xS,k8xC
na	na	k7c4
počáteční	počáteční	k2eAgFnSc4d1
kvalitu	kvalita	k1gFnSc4
diamantu	diamant	k1gInSc2
<g/>
,	,	kIx,
tak	tak	k6eAd1
na	na	k7c4
práci	práce	k1gFnSc4
faséra	fasér	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Emerald	Emerald	k1gInSc1
(	(	kIx(
<g/>
Smaragdový	smaragdový	k2eAgInSc1d1
brus	brus	k1gInSc1
<g/>
)	)	kIx)
-	-	kIx~
Velmi	velmi	k6eAd1
elegantní	elegantní	k2eAgInSc1d1
a	a	k8xC
propracovaný	propracovaný	k2eAgInSc1d1
brus	brus	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgMnS
vůbec	vůbec	k9
jedním	jeden	k4xCgInSc7
z	z	k7c2
prvních	první	k4xOgInPc2
<g/>
,	,	kIx,
do	do	k7c2
kterého	který	k3yRgInSc2,k3yIgInSc2,k3yQgInSc2
byly	být	k5eAaImAgInP
drahé	drahý	k2eAgInPc1d1
kameny	kámen	k1gInPc1
upravovány	upravován	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Smaragdový	smaragdový	k2eAgInSc1d1
brus	brus	k1gInSc1
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
tento	tento	k3xDgInSc1
tvar	tvar	k1gInSc1
také	také	k9
nazývá	nazývat	k5eAaImIp3nS
<g/>
,	,	kIx,
patří	patřit	k5eAaImIp3nS
mezi	mezi	k7c4
"	"	kIx"
<g/>
stupňovité	stupňovitý	k2eAgInPc4d1
brusy	brus	k1gInPc4
<g/>
"	"	kIx"
<g/>
,	,	kIx,
do	do	k7c2
kterých	který	k3yIgFnPc2,k3yRgFnPc2,k3yQgFnPc2
řadíme	řadit	k5eAaImIp1nP
i	i	k8xC
Asscher	Asschra	k1gFnPc2
nebo	nebo	k8xC
Radiant	radiant	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Emerald	Emerald	k1gInSc1
je	být	k5eAaImIp3nS
typický	typický	k2eAgInSc1d1
svým	svůj	k3xOyFgInSc7
obdélníkovým	obdélníkový	k2eAgInSc7d1
tvarem	tvar	k1gInSc7
<g/>
,	,	kIx,
zkrácenými	zkrácený	k2eAgInPc7d1
rohy	roh	k1gInPc7
a	a	k8xC
širokou	široký	k2eAgFnSc7d1
plochou	plocha	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
při	při	k7c6
pohledu	pohled	k1gInSc6
shora	shora	k6eAd1
připomíná	připomínat	k5eAaImIp3nS
schodiště	schodiště	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Asscher	Asschra	k1gFnPc2
-	-	kIx~
Asscherův	Asscherův	k2eAgInSc1d1
brus	brus	k1gInSc1
je	být	k5eAaImIp3nS
velice	velice	k6eAd1
známý	známý	k2eAgMnSc1d1
nejen	nejen	k6eAd1
pro	pro	k7c4
svůj	svůj	k3xOyFgInSc4
výjimečný	výjimečný	k2eAgInSc4d1
lesk	lesk	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
také	také	k9
pro	pro	k7c4
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
typický	typický	k2eAgInSc4d1
a	a	k8xC
nádherný	nádherný	k2eAgInSc4d1
optický	optický	k2eAgInSc4d1
klam	klam	k1gInSc4
známý	známý	k1gMnSc1
jako	jako	k8xC,k8xS
„	„	k?
<g/>
zrcadlový	zrcadlový	k2eAgInSc1d1
sál	sál	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
díky	díky	k7c3
svému	svůj	k3xOyFgInSc3
hlubokému	hluboký	k2eAgInSc3d1
pavilonu	pavilon	k1gInSc3
<g/>
,	,	kIx,
fazetovému	fazetový	k2eAgNnSc3d1
kuletu	kuleto	k1gNnSc3
a	a	k8xC
malé	malý	k2eAgFnSc3d1
tabulce	tabulka	k1gFnSc3
ve	v	k7c6
vysoké	vysoký	k2eAgFnSc6d1
koruně	koruna	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmi	velmi	k6eAd1
často	často	k6eAd1
bývá	bývat	k5eAaImIp3nS
tento	tento	k3xDgInSc1
tvar	tvar	k1gInSc1
zaměňován	zaměňován	k2eAgInSc1d1
se	s	k7c7
čtvercovým	čtvercový	k2eAgInSc7d1
smaragdovým	smaragdový	k2eAgInSc7d1
tvarem	tvar	k1gInSc7
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc1
tvar	tvar	k1gInSc1
je	být	k5eAaImIp3nS
téměř	téměř	k6eAd1
totožný	totožný	k2eAgInSc1d1
a	a	k8xC
často	často	k6eAd1
vládnou	vládnout	k5eAaImIp3nP
neshody	neshoda	k1gFnPc1
ohledně	ohledně	k7c2
jejich	jejich	k3xOp3gInPc2
přesných	přesný	k2eAgInPc2d1
rozdílů	rozdíl	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Pear	Pear	k1gInSc1
-	-	kIx~
Krásný	krásný	k2eAgInSc1d1
hybridní	hybridní	k2eAgInSc1d1
tvar	tvar	k1gInSc4
hruškovitého	hruškovitý	k2eAgNnSc2d1
(	(	kIx(
<g/>
slzovitého	slzovitý	k2eAgMnSc2d1
<g/>
)	)	kIx)
tvaru	tvar	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
v	v	k7c6
sobě	sebe	k3xPyFc6
kombinuje	kombinovat	k5eAaImIp3nS
vlastnosti	vlastnost	k1gFnPc1
dvou	dva	k4xCgInPc2
jedinečných	jedinečný	k2eAgInPc2d1
designů	design	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvaru	tvar	k1gInSc2
„	„	k?
<g/>
marquise	marquise	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
díky	díky	k7c3
kterému	který	k3yIgMnSc3,k3yRgMnSc3,k3yQgMnSc3
má	mít	k5eAaImIp3nS
brus	brus	k1gInSc1
typickou	typický	k2eAgFnSc4d1
špičku	špička	k1gFnSc4
a	a	k8xC
tvaru	tvar	k1gInSc6
„	„	k?
<g/>
round	round	k1gInSc1
briliant	briliant	k1gInSc1
<g/>
“	“	k?
jež	jenž	k3xRgNnSc1
zakončuje	zakončovat	k5eAaImIp3nS
druhý	druhý	k4xOgInSc1
zaoblený	zaoblený	k2eAgInSc1d1
konec	konec	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Slzovité	slzovitý	k2eAgInPc1d1
brusy	brus	k1gInPc1
diamantů	diamant	k1gInPc2
se	se	k3xPyFc4
od	od	k7c2
sebe	sebe	k3xPyFc4
často	často	k6eAd1
svými	svůj	k3xOyFgInPc7
tvary	tvar	k1gInPc7
liší	lišit	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Setkat	setkat	k5eAaPmF
se	se	k3xPyFc4
můžeme	moct	k5eAaImIp1nP
například	například	k6eAd1
s	s	k7c7
úpravou	úprava	k1gFnSc7
do	do	k7c2
„	„	k?
<g/>
francouzské	francouzský	k2eAgFnSc2d1
špičky	špička	k1gFnSc2
<g/>
“	“	k?
nebo	nebo	k8xC
s	s	k7c7
hranatějšími	hranatý	k2eAgInPc7d2
designy	design	k1gInPc7
<g/>
,	,	kIx,
kterých	který	k3yIgInPc2,k3yRgInPc2,k3yQgInPc2
brusič	brusič	k1gInSc1
u	u	k7c2
tvaru	tvar	k1gInSc2
pear	pear	k1gInSc1
docílí	docílit	k5eAaPmIp3nS
vytvořením	vytvoření	k1gNnSc7
tzv.	tzv.	kA
„	„	k?
<g/>
vysokých	vysoký	k2eAgNnPc2d1
ramen	rameno	k1gNnPc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Radiant	radiant	k1gInSc1
-	-	kIx~
Hybridní	hybridní	k2eAgInSc1d1
čtvercový	čtvercový	k2eAgInSc1d1
tvar	tvar	k1gInSc1
s	s	k7c7
výraznými	výrazný	k2eAgInPc7d1
„	„	k?
<g/>
zastřiženými	zastřižený	k2eAgFnPc7d1
<g/>
“	“	k?
rohy	roh	k1gInPc7
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
je	být	k5eAaImIp3nS
význačný	význačný	k2eAgMnSc1d1
pro	pro	k7c4
spojení	spojení	k1gNnSc4
vlastností	vlastnost	k1gFnPc2
hned	hned	k6eAd1
tří	tři	k4xCgInPc2
brusů	brus	k1gInPc2
-	-	kIx~
brilanci	brilance	k1gFnSc4
a	a	k8xC
hloubku	hloubka	k1gFnSc4
tvaru	tvar	k1gInSc2
„	„	k?
<g/>
round	round	k1gInSc1
briliant	briliant	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
čtvercový	čtvercový	k2eAgInSc4d1
vzhled	vzhled	k1gInSc4
tvaru	tvar	k1gInSc2
„	„	k?
<g/>
princess	princess	k1gInSc1
<g/>
“	“	k?
a	a	k8xC
zkrácenými	zkrácený	k2eAgInPc7d1
rohy	roh	k1gInPc7
po	po	k7c6
vzoru	vzor	k1gInSc6
„	„	k?
<g/>
smaragdového	smaragdový	k2eAgInSc2d1
brusu	brus	k1gInSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomu	ten	k3xDgNnSc3
je	být	k5eAaImIp3nS
tento	tento	k3xDgInSc4
univerzální	univerzální	k2eAgInSc4d1
tvar	tvar	k1gInSc4
velmi	velmi	k6eAd1
oblíbenou	oblíbený	k2eAgFnSc7d1
variantou	varianta	k1gFnSc7
pro	pro	k7c4
osazení	osazení	k1gNnSc4
do	do	k7c2
jakéhokoli	jakýkoli	k3yIgInSc2
šperku	šperk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Oval	ovalit	k5eAaPmRp2nS
-	-	kIx~
Zaoblený	zaoblený	k2eAgInSc1d1
protáhlý	protáhlý	k2eAgInSc1d1
tvar	tvar	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
často	často	k6eAd1
využívaný	využívaný	k2eAgInSc1d1
pro	pro	k7c4
optimalizaci	optimalizace	k1gFnSc4
karátové	karátový	k2eAgFnSc2d1
hmotnosti	hmotnost	k1gFnSc2
drahého	drahý	k2eAgInSc2d1
kamene	kámen	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gInSc1
oválný	oválný	k2eAgInSc1d1
tvar	tvar	k1gInSc1
může	moct	k5eAaImIp3nS
pozorovateli	pozorovatel	k1gMnSc3
připadat	připadat	k5eAaPmF,k5eAaImF
větší	veliký	k2eAgInSc4d2
než	než	k8xS
tvar	tvar	k1gInSc4
kulatého	kulatý	k2eAgInSc2d1
briliantu	briliant	k1gInSc2
o	o	k7c6
stejné	stejný	k2eAgFnSc6d1
hmotnosti	hmotnost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejčastěji	často	k6eAd3
bývá	bývat	k5eAaImIp3nS
tento	tento	k3xDgInSc1
tvar	tvar	k1gInSc1
využíván	využívat	k5eAaPmNgInS,k5eAaImNgInS
formou	forma	k1gFnSc7
centrálního	centrální	k2eAgInSc2d1
drahokamu	drahokam	k1gInSc2
u	u	k7c2
diamantových	diamantový	k2eAgInPc2d1
prstenů	prsten	k1gInPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
podlouhlý	podlouhlý	k2eAgInSc4d1
tvar	tvar	k1gInSc4
diamantu	diamant	k1gInSc2
opticky	opticky	k6eAd1
prodlouží	prodloužit	k5eAaPmIp3nS
prsty	prst	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Cushion	Cushion	k1gInSc1
-	-	kIx~
Obdélníkový	obdélníkový	k2eAgInSc4d1
nebo	nebo	k8xC
čtvercový	čtvercový	k2eAgInSc4d1
tvar	tvar	k1gInSc4
s	s	k7c7
typickými	typický	k2eAgInPc7d1
zaoblenými	zaoblený	k2eAgInPc7d1
rohy	roh	k1gInPc7
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
zdánlivě	zdánlivě	k6eAd1
připomíná	připomínat	k5eAaImIp3nS
polštářek	polštářek	k1gInSc1
<g/>
,	,	kIx,
proto	proto	k8xC
tedy	tedy	k9
jeho	jeho	k3xOp3gInSc1
název	název	k1gInSc1
„	„	k?
<g/>
cushion	cushion	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
jeho	jeho	k3xOp3gFnPc3
velkým	velký	k2eAgFnPc3d1
broušeným	broušený	k2eAgFnPc3d1
fazetám	fazeta	k1gFnPc3
lépe	dobře	k6eAd2
odděluje	oddělovat	k5eAaImIp3nS
tento	tento	k3xDgInSc4
tvar	tvar	k1gInSc4
bílé	bílý	k2eAgNnSc4d1
světlo	světlo	k1gNnSc4
od	od	k7c2
spektrálních	spektrální	k2eAgFnPc2d1
barev	barva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejen	nejen	k6eAd1
<g/>
,	,	kIx,
že	že	k8xS
tím	ten	k3xDgNnSc7
rozptyluje	rozptylovat	k5eAaImIp3nS
více	hodně	k6eAd2
světla	světlo	k1gNnPc1
do	do	k7c2
celé	celý	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
kamene	kámen	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
efektivněji	efektivně	k6eAd2
tím	ten	k3xDgNnSc7
může	moct	k5eAaImIp3nS
zakrýt	zakrýt	k5eAaPmF
některé	některý	k3yIgFnPc4
z	z	k7c2
přírodních	přírodní	k2eAgFnPc2d1
vnitřních	vnitřní	k2eAgFnPc2d1
vad	vada	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Heart	Heart	k1gInSc1
-	-	kIx~
Tvar	tvar	k1gInSc1
srdce	srdce	k1gNnSc2
je	být	k5eAaImIp3nS
nejčastěji	často	k6eAd3
složen	složit	k5eAaPmNgInS
z	z	k7c2
56	#num#	k4
až	až	k9
58	#num#	k4
fazet	fazeta	k1gFnPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
6	#num#	k4
až	až	k9
8	#num#	k4
fazet	fazeta	k1gFnPc2
pavilonu	pavilon	k1gInSc2
tvoří	tvořit	k5eAaImIp3nP
hlavní	hlavní	k2eAgFnPc4d1
plochy	plocha	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Srdcové	srdcový	k2eAgInPc1d1
tvary	tvar	k1gInPc1
se	se	k3xPyFc4
od	od	k7c2
sebe	sebe	k3xPyFc4
svými	svůj	k3xOyFgInPc7
designy	design	k1gInPc7
mohou	moct	k5eAaImIp3nP
mírně	mírně	k6eAd1
lišit	lišit	k5eAaImF
v	v	k7c6
závislosti	závislost	k1gFnSc6
na	na	k7c6
konstrukci	konstrukce	k1gFnSc6
a	a	k8xC
vypracování	vypracování	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Velmi	velmi	k6eAd1
oblíbenou	oblíbený	k2eAgFnSc7d1
úpravou	úprava	k1gFnSc7
je	být	k5eAaImIp3nS
tzv.	tzv.	kA
„	„	k?
<g/>
francouzská	francouzský	k2eAgFnSc1d1
špička	špička	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
podstatně	podstatně	k6eAd1
zvyšuje	zvyšovat	k5eAaImIp3nS
lesk	lesk	k1gInSc4
diamantu	diamant	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
úprava	úprava	k1gFnSc1
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
oblíbená	oblíbený	k2eAgFnSc1d1
i	i	k9
u	u	k7c2
dalších	další	k2eAgInPc2d1
tvarů	tvar	k1gInPc2
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
„	„	k?
<g/>
Pear	Pear	k1gInSc1
<g/>
“	“	k?
nebo	nebo	k8xC
„	„	k?
<g/>
Marquise	Marquise	k1gFnSc1
<g/>
“	“	k?
</s>
<s>
Marquise	Marquise	k1gFnSc1
-	-	kIx~
Tvar	tvar	k1gInSc1
drahokamového	drahokamový	k2eAgInSc2d1
brusu	brus	k1gInSc2
„	„	k?
<g/>
marquise	marquise	k1gFnSc1
<g/>
“	“	k?
připomíná	připomínat	k5eAaImIp3nS
svým	svůj	k3xOyFgInSc7
designem	design	k1gInSc7
se	s	k7c7
dvěma	dva	k4xCgFnPc7
špičkami	špička	k1gFnPc7
trup	trup	k1gMnSc1
lodi	loď	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Složen	složen	k2eAgInSc1d1
je	být	k5eAaImIp3nS
nejčastěji	často	k6eAd3
z	z	k7c2
58	#num#	k4
fazet	fazeta	k1gFnPc2
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgInPc2
25	#num#	k4
tvoří	tvořit	k5eAaImIp3nP
pavilon	pavilon	k1gInSc1
a	a	k8xC
33	#num#	k4
korunu	koruna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
tvar	tvar	k1gInSc1
markýzy	markýza	k1gFnSc2
řezán	řezat	k5eAaImNgInS
jako	jako	k9
„	„	k?
<g/>
francouzská	francouzský	k2eAgFnSc1d1
špička	špička	k1gFnSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
je	být	k5eAaImIp3nS
hlavní	hlavní	k2eAgFnSc1d1
fazeta	fazeta	k1gFnSc1
koruny	koruna	k1gFnSc2
nahrazena	nahrazen	k2eAgFnSc1d1
hvězdou	hvězda	k1gFnSc7
tvořenou	tvořený	k2eAgFnSc4d1
větším	veliký	k2eAgInSc7d2
počtem	počet	k1gInSc7
malých	malý	k2eAgFnPc2d1
fazet	fazeta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
jako	jako	k8xC,k8xS
tvar	tvar	k1gInSc4
„	„	k?
<g/>
oval	ovalit	k5eAaPmRp2nS
<g/>
“	“	k?
i	i	k8xC
„	„	k?
<g/>
marquise	marquise	k1gFnSc2
<g/>
“	“	k?
díky	díky	k7c3
svému	svůj	k3xOyFgInSc3
podlouhlému	podlouhlý	k2eAgInSc3d1
tvaru	tvar	k1gInSc3
dobře	dobře	k6eAd1
optimalizuje	optimalizovat	k5eAaBmIp3nS
karátovou	karátový	k2eAgFnSc4d1
hmotnost	hmotnost	k1gFnSc4
drahokamu	drahokam	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k6eAd1
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
zdát	zdát	k5eAaPmF,k5eAaImF
větší	veliký	k2eAgInPc4d2
než	než	k8xS
jiné	jiný	k2eAgInPc4d1
brusy	brus	k1gInPc4
stejné	stejný	k2eAgFnSc2d1
váhy	váha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
PAGEL-THEISEN	PAGEL-THEISEN	k?
<g/>
,	,	kIx,
Verena	Verena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Diamanty	diamant	k1gInPc4
:	:	kIx,
příručka	příručka	k1gFnSc1
hodnocení	hodnocení	k1gNnSc2
diamantů	diamant	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
Ladislav	Ladislav	k1gMnSc1
Klaboch	Klaboch	k1gMnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
.	.	kIx.
335	#num#	k4
s.	s.	k?
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
254	#num#	k4
<g/>
-	-	kIx~
<g/>
6869	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
GIA	GIA	kA
-	-	kIx~
Mezinárodní	mezinárodní	k2eAgFnSc1d1
stupnice	stupnice	k1gFnSc1
broušení	broušení	k1gNnSc2
diamantů	diamant	k1gInPc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
