<s>
Vojtěch	Vojtěch	k1gMnSc1
Hřímalý	Hřímalý	k2eAgMnSc1d1
starší	starší	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c6
hudebním	hudební	k2eAgMnSc6d1
skladateli	skladatel	k1gMnSc6
Vojtěchu	Vojtěch	k1gMnSc6
Hřímalém	Hřímalý	k2eAgInSc6d1
(	(	kIx(
<g/>
1809	#num#	k4
<g/>
–	–	k?
<g/>
1880	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
ostatních	ostatní	k2eAgInPc6d1
členech	člen	k1gInPc6
této	tento	k3xDgFnSc2
hudební	hudební	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Hřímalý	Hřímalý	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Vojtěch	Vojtěch	k1gMnSc1
Hřímalý	Hřímalý	k2eAgMnSc1d1
staršíZákladní	staršíZákladný	k2eAgMnPc1d1
informace	informace	k1gFnPc4
Narození	narození	k1gNnPc1
</s>
<s>
29	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1810	#num#	k4
Blatná	blatný	k2eAgFnSc1d1
<g/>
,	,	kIx,
Čechy	Čechy	k1gFnPc1
Rakouské	rakouský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
Rakouské	rakouský	k2eAgNnSc1d1
císařství	císařství	k1gNnSc1
Úmrtí	úmrtí	k1gNnSc1
</s>
<s>
26	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1880	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
70	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Moskva	Moskva	k1gFnSc1
Ruské	ruský	k2eAgNnSc1d1
impérium	impérium	k1gNnSc1
Ruské	ruský	k2eAgNnSc1d1
impérium	impérium	k1gNnSc1
Žánry	žánr	k1gInPc7
</s>
<s>
klasická	klasický	k2eAgFnSc1d1
hudba	hudba	k1gFnSc1
a	a	k8xC
duchovní	duchovní	k2eAgFnSc1d1
hudba	hudba	k1gFnSc1
Povolání	povolání	k1gNnSc2
</s>
<s>
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
a	a	k8xC
varhaník	varhaník	k1gMnSc1
Nástroje	nástroj	k1gInSc2
</s>
<s>
varhany	varhany	k1gFnPc1
Děti	dítě	k1gFnPc1
</s>
<s>
Marie	Marie	k1gFnSc1
HřímaláAnna	HřímaláAnno	k1gNnSc2
HřímaláJan	HřímaláJany	k1gInPc2
HřímalýVojtěch	HřímalýVojtěcha	k1gFnPc2
Hřímalý	Hřímalý	k2eAgMnSc1d1
mladšíJaromír	mladšíJaromír	k1gMnSc1
Hřímalý	Hřímalý	k2eAgMnSc1d1
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
vnuk	vnuk	k1gMnSc1
Otakar	Otakar	k1gMnSc1
Hřímalý	Hřímalý	k2eAgMnSc1d1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vojtěch	Vojtěch	k1gMnSc1
Hřímalý	Hřímalý	k2eAgMnSc1d1
(	(	kIx(
<g/>
29	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1810	#num#	k4
Blatná	blatný	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
–	–	k?
26	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1880	#num#	k4
Moskva	Moskva	k1gFnSc1
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
český	český	k2eAgMnSc1d1
varhaník	varhaník	k1gMnSc1
a	a	k8xC
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
</s>
<s>
Hudební	hudební	k2eAgNnSc1d1
vzdělání	vzdělání	k1gNnSc1
získal	získat	k5eAaPmAgInS
u	u	k7c2
místního	místní	k2eAgMnSc2d1
učitele	učitel	k1gMnSc2
J.	J.	kA
Fialy	fiala	k1gFnSc2
a	a	k8xC
varhaníka	varhaník	k1gMnSc2
J.	J.	kA
Böhma	Böhm	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejprve	nejprve	k6eAd1
byl	být	k5eAaImAgInS
varhaníkem	varhaník	k1gMnSc7
ve	v	k7c6
svém	svůj	k3xOyFgNnSc6
rodišti	rodiště	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1835	#num#	k4
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
varhaníkem	varhaník	k1gMnSc7
v	v	k7c6
arciděkanském	arciděkanský	k2eAgInSc6d1
chrámu	chrám	k1gInSc6
svatého	svatý	k2eAgMnSc2d1
Bartoloměje	Bartoloměj	k1gMnSc2
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
zůstal	zůstat	k5eAaPmAgInS
až	až	k9
do	do	k7c2
odchodu	odchod	k1gInSc2
do	do	k7c2
důchodu	důchod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Stal	stát	k5eAaPmAgMnS
se	s	k7c7
zakladatelem	zakladatel	k1gMnSc7
hudební	hudební	k2eAgFnSc2d1
tradice	tradice	k1gFnSc2
rodu	rod	k1gInSc2
Hřímalých	Hřímalý	k2eAgFnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Všechny	všechen	k3xTgFnPc1
jeho	jeho	k3xOp3gFnPc1
děti	dítě	k1gFnPc1
se	se	k3xPyFc4
věnovaly	věnovat	k5eAaPmAgFnP,k5eAaImAgFnP
hudbě	hudba	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
nich	on	k3xPp3gMnPc2
nejvíce	nejvíce	k6eAd1,k6eAd3
vynikli	vyniknout	k5eAaPmAgMnP
houslista	houslista	k1gMnSc1
Jan	Jan	k1gMnSc1
Hřímalý	Hřímalý	k2eAgMnSc1d1
<g/>
,	,	kIx,
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
Vojtěch	Vojtěch	k1gMnSc1
Hřímalý	Hřímalý	k2eAgMnSc1d1
mladší	mladý	k2eAgMnSc1d2
a	a	k8xC
vnuk	vnuk	k1gMnSc1
<g/>
,	,	kIx,
hudební	hudební	k2eAgMnSc1d1
skladatel	skladatel	k1gMnSc1
Otakar	Otakar	k1gMnSc1
Hřímalý	Hřímalý	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zemřel	zemřít	k5eAaPmAgMnS
u	u	k7c2
svého	svůj	k3xOyFgMnSc2
syna	syn	k1gMnSc2
Jana	Jan	k1gMnSc2
v	v	k7c6
Moskvě	Moskva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Komponoval	komponovat	k5eAaImAgMnS
v	v	k7c6
duchu	duch	k1gMnSc6
pozdního	pozdní	k2eAgInSc2d1
klasicismu	klasicismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
dílo	dílo	k1gNnSc1
obsahuje	obsahovat	k5eAaImIp3nS
převážně	převážně	k6eAd1
chrámovou	chrámový	k2eAgFnSc4d1
hudbu	hudba	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
vesměs	vesměs	k6eAd1
zůstala	zůstat	k5eAaPmAgFnS
v	v	k7c6
rukopise	rukopis	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Psal	psát	k5eAaImAgMnS
však	však	k9
i	i	k9
drobné	drobný	k2eAgFnPc4d1
světské	světský	k2eAgFnPc4d1
skladby	skladba	k1gFnPc4
a	a	k8xC
písně	píseň	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Československý	československý	k2eAgInSc1d1
hudební	hudební	k2eAgInSc1d1
slovník	slovník	k1gInSc1
I	i	k9
(	(	kIx(
<g/>
A	a	k9
<g/>
–	–	k?
<g/>
L	L	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
1963	#num#	k4
<g/>
,	,	kIx,
SHV	SHV	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
Pazdírkův	Pazdírkův	k2eAgInSc1d1
hudební	hudební	k2eAgInSc1d1
slovník	slovník	k1gInSc1
naučný	naučný	k2eAgInSc1d1
:	:	kIx,
Část	část	k1gFnSc1
osobní	osobní	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
II	II	kA
<g/>
,	,	kIx,
Svazek	svazek	k1gInSc1
prvý	prvý	k4xOgInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
A-K	A-K	k1gFnSc1
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
1937	#num#	k4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Matriční	matriční	k2eAgInSc4d1
záznam	záznam	k1gInSc4
o	o	k7c6
narození	narození	k1gNnSc6
a	a	k8xC
křtu	křest	k1gInSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Vojtěch	Vojtěch	k1gMnSc1
Hřímalý	Hřímalý	k2eAgMnSc1d1
starší	starší	k1gMnSc1
</s>
<s>
Stránky	stránka	k1gFnPc1
obce	obec	k1gFnSc2
Blatná	blatný	k2eAgFnSc1d1
<g/>
,	,	kIx,
rodiště	rodiště	k1gNnSc1
skladatele	skladatel	k1gMnSc2
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
6581	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
1047617099	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
5758	#num#	k4
7640	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
85344874	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
|	|	kIx~
Lidé	člověk	k1gMnPc1
</s>
