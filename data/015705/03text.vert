<s>
Ostravsko-karvinská	ostravsko-karvinský	k2eAgFnSc1d1
uhelná	uhelný	k2eAgFnSc1d1
pánev	pánev	k1gFnSc1
</s>
<s>
Ostravsko-karvinská	ostravsko-karvinský	k2eAgFnSc1d1
uhelná	uhelný	k2eAgFnSc1d1
pánev	pánev	k1gFnSc1
kolem	kolem	k7c2
1900	#num#	k4
</s>
<s>
Důl	důl	k1gInSc1
Darkov	Darkov	k1gInSc1
v	v	k7c6
Karviné	Karviná	k1gFnSc6
<g/>
,	,	kIx,
v	v	k7c6
popředí	popředí	k1gNnSc6
krajina	krajina	k1gFnSc1
postižená	postižený	k2eAgFnSc1d1
důlními	důlní	k2eAgFnPc7d1
poklesy	pokles	k1gInPc4
</s>
<s>
Ostravsko-karvinská	ostravsko-karvinský	k2eAgFnSc1d1
uhelná	uhelný	k2eAgFnSc1d1
pánev	pánev	k1gFnSc1
<g/>
,	,	kIx,
pro	pro	k7c4
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
je	být	k5eAaImIp3nS
také	také	k9
užíván	užíván	k2eAgInSc1d1
hospodářský	hospodářský	k2eAgInSc1d1
termín	termín	k1gInSc1
Ostravsko-karvinský	ostravsko-karvinský	k2eAgInSc4d1
revír	revír	k1gInSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc1
části	část	k1gFnSc2
české	český	k2eAgFnSc2d1
části	část	k1gFnSc2
hornoslezské	hornoslezský	k2eAgFnSc2d1
uhelné	uhelný	k2eAgFnSc2d1
pánve	pánev	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
nejrozsáhlejší	rozsáhlý	k2eAgFnSc4d3
uhelnou	uhelný	k2eAgFnSc4d1
pánev	pánev	k1gFnSc4
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
zde	zde	k6eAd1
černé	černý	k2eAgNnSc1d1
uhlí	uhlí	k1gNnSc1
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
současnosti	současnost	k1gFnSc6
dobýváno	dobývat	k5eAaImNgNnS
výhradně	výhradně	k6eAd1
hlubinnou	hlubinný	k2eAgFnSc7d1
těžbou	těžba	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Umístění	umístění	k1gNnSc1
a	a	k8xC
členění	členění	k1gNnSc1
</s>
<s>
Ostravsko-karvinská	ostravsko-karvinský	k2eAgFnSc1d1
pánev	pánev	k1gFnSc1
je	být	k5eAaImIp3nS
na	na	k7c6
jihu	jih	k1gInSc6
oddělena	oddělen	k2eAgFnSc1d1
od	od	k7c2
podbeskydské	podbeskydský	k2eAgFnSc2d1
pánve	pánev	k1gFnSc2
tzv.	tzv.	kA
bludovickým	bludovický	k2eAgInSc7d1
zlomem	zlom	k1gInSc7
a	a	k8xC
dělí	dělit	k5eAaImIp3nP
se	se	k3xPyFc4
na	na	k7c4
část	část	k1gFnSc4
ostravskou	ostravský	k2eAgFnSc4d1
a	a	k8xC
karvinskou	karvinský	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostravská	ostravský	k2eAgFnSc1d1
část	část	k1gFnSc1
je	být	k5eAaImIp3nS
tvořena	tvořit	k5eAaImNgFnS
ostravskou	ostravský	k2eAgFnSc7d1
a	a	k8xC
petřvaldskou	petřvaldský	k2eAgFnSc7d1
dílčí	dílčí	k2eAgFnSc7d1
pánví	pánev	k1gFnSc7
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
jsou	být	k5eAaImIp3nP
vzájemně	vzájemně	k6eAd1
odděleny	oddělen	k2eAgInPc1d1
tzv.	tzv.	kA
michálkovickou	michálkovický	k2eAgFnSc7d1
poruchou	porucha	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ostravská	ostravský	k2eAgFnSc1d1
část	část	k1gFnSc1
je	být	k5eAaImIp3nS
pak	pak	k6eAd1
od	od	k7c2
karvinské	karvinský	k2eAgFnSc2d1
části	část	k1gFnSc2
oddělena	oddělen	k2eAgFnSc1d1
tzv.	tzv.	kA
orlovskou	orlovský	k2eAgFnSc7d1
strukturou	struktura	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Geomorfologický	geomorfologický	k2eAgInSc1d1
celek	celek	k1gInSc1
Ostravská	ostravský	k2eAgFnSc1d1
pánev	pánev	k1gFnSc1
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
ostravsko-karvinské	ostravsko-karvinský	k2eAgFnSc2d1
uhelné	uhelný	k2eAgFnSc2d1
pánve	pánev	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mocnost	mocnost	k1gFnSc1
slojí	sloj	k1gFnPc2
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
revíru	revír	k1gInSc6
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
dvě	dva	k4xCgNnPc1
uhelná	uhelný	k2eAgNnPc1d1
souvrství	souvrství	k1gNnPc1
<g/>
:	:	kIx,
ostravské	ostravský	k2eAgFnSc3d1
(	(	kIx(
<g/>
celková	celkový	k2eAgFnSc1d1
mocnost	mocnost	k1gFnSc1
2880	#num#	k4
m	m	kA
<g/>
)	)	kIx)
a	a	k8xC
karvinské	karvinský	k2eAgNnSc1d1
(	(	kIx(
<g/>
1200	#num#	k4
m	m	kA
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1
mocnost	mocnost	k1gFnSc1
slojí	sloj	k1gFnPc2
ostravského	ostravský	k2eAgNnSc2d1
souvrství	souvrství	k1gNnSc2
je	být	k5eAaImIp3nS
73	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
nejmohutnější	mohutný	k2eAgFnSc1d3
je	být	k5eAaImIp3nS
sloj	sloj	k1gFnSc1
386	#num#	k4
(	(	kIx(
<g/>
Mohutný	mohutný	k2eAgMnSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
mocnost	mocnost	k1gFnSc1
ve	v	k7c6
velké	velký	k2eAgFnSc6d1
části	část	k1gFnSc6
kolísá	kolísat	k5eAaImIp3nS
od	od	k7c2
2	#num#	k4
do	do	k7c2
4	#num#	k4
m	m	kA
<g/>
,	,	kIx,
ale	ale	k8xC
dosahuje	dosahovat	k5eAaImIp3nS
maximální	maximální	k2eAgFnPc4d1
mocnosti	mocnost	k1gFnPc4
až	až	k9
6	#num#	k4
m.	m.	k?
</s>
<s>
Karvinské	karvinský	k2eAgNnSc1d1
souvrství	souvrství	k1gNnSc1
má	mít	k5eAaImIp3nS
průměrnou	průměrný	k2eAgFnSc4d1
mocnost	mocnost	k1gFnSc4
slojí	sloj	k1gFnSc7
180	#num#	k4
cm	cm	kA
<g/>
,	,	kIx,
největší	veliký	k2eAgFnSc4d3
průměrnou	průměrný	k2eAgFnSc4d1
mocnost	mocnost	k1gFnSc4
má	mít	k5eAaImIp3nS
pak	pak	k6eAd1
sloj	sloj	k1gInSc1
504	#num#	k4
(	(	kIx(
<g/>
Prokop	Prokop	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
v	v	k7c6
dobývacím	dobývací	k2eAgInSc6d1
prostoru	prostor	k1gInSc6
dolu	dol	k1gInSc2
ČSM	ČSM	kA
dosahuje	dosahovat	k5eAaImIp3nS
mocnosti	mocnost	k1gFnPc4
až	až	k9
15	#num#	k4
m.	m.	k?
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7
jsou	být	k5eAaImIp3nP
intruze	intruze	k1gFnPc1
různého	různý	k2eAgNnSc2d1
stáří	stáří	k1gNnSc2
do	do	k7c2
uhelných	uhelný	k2eAgFnPc2d1
slojí	sloj	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
uhelnou	uhelný	k2eAgFnSc4d1
hmotu	hmota	k1gFnSc4
přetvořily	přetvořit	k5eAaPmAgFnP
nejčastěji	často	k6eAd3
v	v	k7c4
přírodní	přírodní	k2eAgInSc4d1
koks	koks	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1
těžba	těžba	k1gFnSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
</s>
<s>
Celkově	celkově	k6eAd1
zde	zde	k6eAd1
bylo	být	k5eAaImAgNnS
vytěženo	vytěžit	k5eAaPmNgNnS
asi	asi	k9
1,7	1,7	k4
mld	mld	k?
tun	tuna	k1gFnPc2
černého	černý	k2eAgNnSc2d1
uhlí	uhlí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
kol	kol	k7c2
<g/>
.	.	kIx.
autorů	autor	k1gMnPc2
<g/>
:	:	kIx,
Uhelné	uhelný	k2eAgNnSc4d1
hornictví	hornictví	k1gNnSc4
v	v	k7c6
ČSSR	ČSSR	kA
<g/>
;	;	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Profil	profil	k1gInSc1
<g/>
,	,	kIx,
1985	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Stručný	stručný	k2eAgInSc1d1
přehled	přehled	k1gInSc1
geologie	geologie	k1gFnSc2
uhlonosného	uhlonosný	k2eAgInSc2d1
karbonu	karbon	k1gInSc2
české	český	k2eAgFnSc2d1
části	část	k1gFnSc2
hornoslezské	hornoslezský	k2eAgFnSc2d1
pánve	pánev	k1gFnSc2
</s>
<s>
Ostravsko	Ostravsko	k1gNnSc4
karvinská	karvinský	k2eAgFnSc1d1
pánev	pánev	k1gFnSc1
na	na	k7c4
okd	okd	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Těžba	těžba	k1gFnSc1
</s>
