<s>
Ostravsko-karvinská	ostravsko-karvinský	k2eAgFnSc1d1
uhelná	uhelný	k2eAgFnSc1d1
pánev	pánev	k1gFnSc1
<g/>
,	,	kIx,
pro	pro	k7c4
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
je	být	k5eAaImIp3nS
také	také	k9
užíván	užíván	k2eAgInSc1d1
hospodářský	hospodářský	k2eAgInSc1d1
termín	termín	k1gInSc1
Ostravsko-karvinský	ostravsko-karvinský	k2eAgInSc4d1
revír	revír	k1gInSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
označení	označení	k1gNnSc1
části	část	k1gFnSc2
české	český	k2eAgFnSc2d1
části	část	k1gFnSc2
hornoslezské	hornoslezský	k2eAgFnSc2d1
uhelné	uhelný	k2eAgFnSc2d1
pánve	pánev	k1gFnSc2
<g/>
.	.	kIx.
</s>