<s>
Dne	den	k1gInSc2	den
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1989	[number]	k4	1989
byla	být	k5eAaImAgFnS	být
vyslána	vyslat	k5eAaPmNgFnS	vyslat
k	k	k7c3	k
Venuši	Venuše	k1gFnSc3	Venuše
americká	americký	k2eAgFnSc1d1	americká
sonda	sonda	k1gFnSc1	sonda
Magellan	Magellany	k1gInPc2	Magellany
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
provést	provést	k5eAaPmF	provést
podrobné	podrobný	k2eAgNnSc4d1	podrobné
zmapování	zmapování	k1gNnSc4	zmapování
povrchu	povrch	k1gInSc2	povrch
planety	planeta	k1gFnSc2	planeta
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
radaru	radar	k1gInSc2	radar
<g/>
.	.	kIx.	.
</s>
