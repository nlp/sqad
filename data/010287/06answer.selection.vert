<s>
Frygická	frygický	k2eAgFnSc1d1	frygická
čapka	čapka	k1gFnSc1	čapka
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
pokrývky	pokrývka	k1gFnSc2	pokrývka
hlavy	hlava	k1gFnSc2	hlava
s	s	k7c7	s
kónickým	kónický	k2eAgInSc7d1	kónický
tvarem	tvar	k1gInSc7	tvar
kužele	kužel	k1gInSc2	kužel
s	s	k7c7	s
ohnutou	ohnutý	k2eAgFnSc7d1	ohnutá
špicí	špice	k1gFnSc7	špice
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
vznik	vznik	k1gInSc1	vznik
bývá	bývat	k5eAaImIp3nS	bývat
spojován	spojován	k2eAgInSc1d1	spojován
se	s	k7c7	s
starověkou	starověký	k2eAgFnSc7d1	starověká
Frýgií	Frýgie	k1gFnSc7	Frýgie
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
název	název	k1gInSc1	název
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
