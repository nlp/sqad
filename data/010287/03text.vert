<p>
<s>
Frygická	frygický	k2eAgFnSc1d1	frygická
čapka	čapka	k1gFnSc1	čapka
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
pokrývky	pokrývka	k1gFnSc2	pokrývka
hlavy	hlava	k1gFnSc2	hlava
s	s	k7c7	s
kónickým	kónický	k2eAgInSc7d1	kónický
tvarem	tvar	k1gInSc7	tvar
kužele	kužel	k1gInSc2	kužel
s	s	k7c7	s
ohnutou	ohnutý	k2eAgFnSc7d1	ohnutá
špicí	špice	k1gFnSc7	špice
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
vznik	vznik	k1gInSc1	vznik
bývá	bývat	k5eAaImIp3nS	bývat
spojován	spojován	k2eAgInSc1d1	spojován
se	s	k7c7	s
starověkou	starověký	k2eAgFnSc7d1	starověká
Frýgií	Frýgie	k1gFnSc7	Frýgie
(	(	kIx(	(
<g/>
odtud	odtud	k6eAd1	odtud
název	název	k1gInSc1	název
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vyráběna	vyrábět	k5eAaImNgFnS	vyrábět
z	z	k7c2	z
rozličných	rozličný	k2eAgFnPc2d1	rozličná
látek	látka	k1gFnPc2	látka
–	–	k?	–
plátna	plátno	k1gNnSc2	plátno
<g/>
,	,	kIx,	,
vlny	vlna	k1gFnSc2	vlna
nebo	nebo	k8xC	nebo
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
symbolem	symbol	k1gInSc7	symbol
svobody	svoboda	k1gFnSc2	svoboda
a	a	k8xC	a
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
tvar	tvar	k1gInSc1	tvar
čapky	čapka	k1gFnSc2	čapka
znám	znát	k5eAaImIp1nS	znát
spíše	spíše	k9	spíše
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
animovanými	animovaný	k2eAgFnPc7d1	animovaná
postavičkami	postavička	k1gFnPc7	postavička
Šmoulů	šmoula	k1gMnPc2	šmoula
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Čapka	čapka	k1gFnSc1	čapka
nejspíše	nejspíše	k9	nejspíše
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
ve	v	k7c6	v
Frýgii	Frýgie	k1gFnSc6	Frýgie
a	a	k8xC	a
nosily	nosit	k5eAaImAgFnP	nosit
ji	on	k3xPp3gFnSc4	on
i	i	k9	i
další	další	k2eAgInPc4d1	další
maloasijské	maloasijský	k2eAgInPc4d1	maloasijský
národy	národ	k1gInPc4	národ
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byla	být	k5eAaImAgFnS	být
charakteristická	charakteristický	k2eAgFnSc1d1	charakteristická
pro	pro	k7c4	pro
Íránce	Íránec	k1gMnPc4	Íránec
a	a	k8xC	a
Thráky	Thrák	k1gMnPc4	Thrák
<g/>
.	.	kIx.	.
</s>
<s>
Perský	perský	k2eAgMnSc1d1	perský
hrdina	hrdina	k1gMnSc1	hrdina
Mithras	Mithras	k1gMnSc1	Mithras
je	být	k5eAaImIp3nS	být
zobrazován	zobrazován	k2eAgMnSc1d1	zobrazován
s	s	k7c7	s
frygickou	frygický	k2eAgFnSc7d1	frygická
čapkou	čapka	k1gFnSc7	čapka
<g/>
.	.	kIx.	.
</s>
<s>
Staří	starý	k2eAgMnPc1d1	starý
Řekové	Řek	k1gMnPc1	Řek
považovali	považovat	k5eAaImAgMnP	považovat
tuto	tento	k3xDgFnSc4	tento
pokrývku	pokrývka	k1gFnSc4	pokrývka
hlavy	hlava	k1gFnSc2	hlava
za	za	k7c4	za
typické	typický	k2eAgNnSc4d1	typické
oblečení	oblečení	k1gNnSc4	oblečení
barbarů	barbar	k1gMnPc2	barbar
a	a	k8xC	a
proto	proto	k8xC	proto
na	na	k7c6	na
vázách	váza	k1gFnPc6	váza
<g/>
,	,	kIx,	,
nástěnných	nástěnný	k2eAgFnPc6d1	nástěnná
malbách	malba	k1gFnPc6	malba
nebo	nebo	k8xC	nebo
mozaikách	mozaika	k1gFnPc6	mozaika
nosí	nosit	k5eAaImIp3nS	nosit
frygickou	frygický	k2eAgFnSc4d1	frygická
čapku	čapka	k1gFnSc4	čapka
převážně	převážně	k6eAd1	převážně
Peršané	Peršan	k1gMnPc1	Peršan
<g/>
.	.	kIx.	.
</s>
<s>
Makedonci	Makedonec	k1gMnPc1	Makedonec
přijali	přijmout	k5eAaPmAgMnP	přijmout
čapku	čapka	k1gFnSc4	čapka
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
tvarovanou	tvarovaný	k2eAgFnSc4d1	tvarovaná
přilbu	přilba	k1gFnSc4	přilba
od	od	k7c2	od
Thráků	Thrák	k1gMnPc2	Thrák
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
bibličtí	biblický	k2eAgMnPc1d1	biblický
Tři	tři	k4xCgMnPc1	tři
králové	král	k1gMnPc1	král
byli	být	k5eAaImAgMnP	být
v	v	k7c6	v
raných	raný	k2eAgFnPc6d1	raná
dobách	doba	k1gFnPc6	doba
zobrazováni	zobrazovat	k5eAaImNgMnP	zobrazovat
s	s	k7c7	s
touto	tento	k3xDgFnSc7	tento
pokrývkou	pokrývka	k1gFnSc7	pokrývka
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
raném	raný	k2eAgInSc6d1	raný
středověku	středověk	k1gInSc6	středověk
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
se	se	k3xPyFc4	se
čapka	čapka	k1gFnSc1	čapka
znovu	znovu	k6eAd1	znovu
ojediněle	ojediněle	k6eAd1	ojediněle
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
anglosaských	anglosaský	k2eAgInPc6d1	anglosaský
rukopisech	rukopis	k1gInPc6	rukopis
10	[number]	k4	10
<g/>
.	.	kIx.	.
a	a	k8xC	a
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
na	na	k7c6	na
vyobrazeních	vyobrazení	k1gNnPc6	vyobrazení
válečníků	válečník	k1gMnPc2	válečník
s	s	k7c7	s
frygickými	frygický	k2eAgFnPc7d1	frygická
čapkami	čapka	k1gFnPc7	čapka
nebo	nebo	k8xC	nebo
helmami	helma	k1gFnPc7	helma
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vrcholném	vrcholný	k2eAgInSc6d1	vrcholný
středověku	středověk	k1gInSc6	středověk
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
oděvu	oděv	k1gInSc2	oděv
neapolských	neapolský	k2eAgMnPc2d1	neapolský
námořníků	námořník	k1gMnPc2	námořník
<g/>
.	.	kIx.	.
</s>
<s>
Vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
se	se	k3xPyFc4	se
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
Corno	Corno	k6eAd1	Corno
Ducale	Ducala	k1gFnSc3	Ducala
<g/>
,	,	kIx,	,
pokrývka	pokrývka	k1gFnSc1	pokrývka
hlavy	hlava	k1gFnSc2	hlava
benátského	benátský	k2eAgNnSc2d1	benátské
dóžete	dóže	k1gNnSc2wR	dóže
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Čapka	Čapek	k1gMnSc2	Čapek
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
==	==	k?	==
</s>
</p>
<p>
<s>
Během	během	k7c2	během
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
nosili	nosit	k5eAaImAgMnP	nosit
frygickou	frygický	k2eAgFnSc4d1	frygická
čapku	čapka	k1gFnSc4	čapka
jakobíni	jakobín	k1gMnPc1	jakobín
či	či	k8xC	či
sansculoti	sansculot	k1gMnPc1	sansculot
jako	jako	k8xC	jako
výraz	výraz	k1gInSc1	výraz
příslušnosti	příslušnost	k1gFnSc2	příslušnost
ke	k	k7c3	k
svému	svůj	k3xOyFgNnSc3	svůj
politickému	politický	k2eAgNnSc3d1	politické
přesvědčení	přesvědčení	k1gNnSc3	přesvědčení
<g/>
.	.	kIx.	.
</s>
<s>
Ztotožnili	ztotožnit	k5eAaPmAgMnP	ztotožnit
si	se	k3xPyFc3	se
ji	on	k3xPp3gFnSc4	on
totiž	totiž	k9	totiž
s	s	k7c7	s
římskou	římský	k2eAgFnSc7d1	římská
čapkou	čapka	k1gFnSc7	čapka
(	(	kIx(	(
<g/>
pileus	pileus	k1gInSc1	pileus
<g/>
)	)	kIx)	)
dávanou	dávaný	k2eAgFnSc4d1	dávaná
propouštěným	propouštěný	k2eAgMnSc7d1	propouštěný
otrokům	otrok	k1gMnPc3	otrok
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
čepice	čepice	k1gFnSc1	čepice
stala	stát	k5eAaPmAgFnS	stát
postupně	postupně	k6eAd1	postupně
symbolem	symbol	k1gInSc7	symbol
demokratických	demokratický	k2eAgFnPc2d1	demokratická
a	a	k8xC	a
republikánských	republikánský	k2eAgFnPc2d1	republikánská
svobod	svoboda	k1gFnPc2	svoboda
nejen	nejen	k6eAd1	nejen
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jinde	jinde	k6eAd1	jinde
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
odpůrců	odpůrce	k1gMnPc2	odpůrce
revoluce	revoluce	k1gFnSc2	revoluce
zase	zase	k9	zase
představovala	představovat	k5eAaImAgFnS	představovat
znak	znak	k1gInSc4	znak
jakobínské	jakobínský	k2eAgFnSc2d1	jakobínská
hrůzovlády	hrůzovláda	k1gFnSc2	hrůzovláda
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
čapkou	čapka	k1gFnSc7	čapka
na	na	k7c6	na
hlavě	hlava	k1gFnSc6	hlava
bývá	bývat	k5eAaImIp3nS	bývat
zobrazována	zobrazován	k2eAgFnSc1d1	zobrazována
rovněž	rovněž	k9	rovněž
Marianne	Mariann	k1gInSc5	Mariann
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
ze	z	k7c2	z
symbolů	symbol	k1gInPc2	symbol
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Čapka	Čapka	k1gMnSc1	Čapka
se	se	k3xPyFc4	se
jako	jako	k9	jako
symbol	symbol	k1gInSc4	symbol
svobody	svoboda	k1gFnSc2	svoboda
dostala	dostat	k5eAaPmAgFnS	dostat
i	i	k9	i
do	do	k7c2	do
znaků	znak	k1gInPc2	znak
některých	některý	k3yIgInPc2	některý
amerických	americký	k2eAgInPc2d1	americký
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
mnohé	mnohé	k1gNnSc1	mnohé
získaly	získat	k5eAaPmAgInP	získat
nezávislost	nezávislost	k1gFnSc4	nezávislost
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
a	a	k8xC	a
Napoleonova	Napoleonův	k2eAgNnSc2d1	Napoleonovo
tažení	tažení	k1gNnSc2	tažení
<g/>
.	.	kIx.	.
</s>
<s>
Objevuje	objevovat	k5eAaImIp3nS	objevovat
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
ve	v	k7c6	v
státním	státní	k2eAgInSc6d1	státní
znaku	znak	k1gInSc6	znak
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
,	,	kIx,	,
Bolívie	Bolívie	k1gFnSc2	Bolívie
<g/>
,	,	kIx,	,
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
<g/>
,	,	kIx,	,
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
,	,	kIx,	,
Nikaraguy	Nikaragua	k1gFnSc2	Nikaragua
<g/>
,	,	kIx,	,
Salvadoru	Salvador	k1gInSc2	Salvador
i	i	k8xC	i
ve	v	k7c6	v
znaku	znak	k1gInSc6	znak
amerického	americký	k2eAgInSc2d1	americký
státu	stát	k1gInSc2	stát
Západní	západní	k2eAgFnSc2d1	západní
Virginie	Virginie	k1gFnSc2	Virginie
<g/>
.	.	kIx.	.
</s>
<s>
Frygická	frygický	k2eAgFnSc1d1	frygická
čapka	čapka	k1gFnSc1	čapka
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
nalézá	nalézat	k5eAaImIp3nS	nalézat
na	na	k7c6	na
vlajkách	vlajka	k1gFnPc6	vlajka
Salvadoru	Salvador	k1gInSc2	Salvador
<g/>
,	,	kIx,	,
Haiti	Haiti	k1gNnSc2	Haiti
<g/>
,	,	kIx,	,
Nikaraguy	Nikaragua	k1gFnSc2	Nikaragua
a	a	k8xC	a
na	na	k7c6	na
zadní	zadní	k2eAgFnSc6d1	zadní
straně	strana	k1gFnSc6	strana
vlajky	vlajka	k1gFnSc2	vlajka
Paraguae	Paragua	k1gFnSc2	Paragua
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
vlajek	vlajka	k1gFnPc2	vlajka
států	stát	k1gInPc2	stát
USA	USA	kA	USA
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
a	a	k8xC	a
New	New	k1gFnSc1	New
Jersey	Jersea	k1gFnSc2	Jersea
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
vlajky	vlajka	k1gFnPc4	vlajka
brazilského	brazilský	k2eAgInSc2d1	brazilský
státu	stát	k1gInSc2	stát
Santa	Santa	k1gMnSc1	Santa
Catarina	Catarina	k1gMnSc1	Catarina
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
byla	být	k5eAaImAgFnS	být
čapka	čapka	k1gFnSc1	čapka
součástí	součást	k1gFnSc7	součást
bývalého	bývalý	k2eAgInSc2d1	bývalý
znaku	znak	k1gInSc2	znak
Dominikánské	dominikánský	k2eAgFnSc2d1	Dominikánská
republiky	republika	k1gFnSc2	republika
(	(	kIx(	(
<g/>
1844	[number]	k4	1844
<g/>
–	–	k?	–
<g/>
1865	[number]	k4	1865
<g/>
)	)	kIx)	)
a	a	k8xC	a
bývalé	bývalý	k2eAgFnSc2d1	bývalá
vlajky	vlajka	k1gFnSc2	vlajka
Argentiny	Argentina	k1gFnSc2	Argentina
(	(	kIx(	(
<g/>
1836	[number]	k4	1836
<g/>
–	–	k?	–
<g/>
1849	[number]	k4	1849
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Phrygische	Phrygische	k1gNnSc2	Phrygische
Mütze	Mütze	k1gFnSc2	Mütze
na	na	k7c6	na
německé	německý	k2eAgFnSc6d1	německá
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Frygická	frygický	k2eAgFnSc1d1	frygická
čapka	čapka	k1gFnSc1	čapka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
