<s>
Země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
jedinou	jediný	k2eAgFnSc7d1	jediná
planetou	planeta	k1gFnSc7	planeta
naší	náš	k3xOp1gFnSc2	náš
sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
povrch	povrch	k1gInSc1	povrch
je	být	k5eAaImIp3nS	být
pokryt	pokrýt	k5eAaPmNgInS	pokrýt
kapalnou	kapalný	k2eAgFnSc7d1	kapalná
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
