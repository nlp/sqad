<s>
Kostel	kostel	k1gInSc1
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
Guadalupské	Guadalupský	k2eAgFnSc2d1
a	a	k8xC
svatého	svatý	k2eAgMnSc2d1
Filipa	Filip	k1gMnSc2
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
obsahuje	obsahovat	k5eAaImIp3nS
substituovaný	substituovaný	k2eAgInSc1d1
infobox	infobox	k1gInSc1
<g/>
.	.	kIx.
<g/>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
převedete	převést	k5eAaPmIp2nP
na	na	k7c4
standardní	standardní	k2eAgFnSc4d1
šablonu	šablona	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Nostra	Nostra	k1gFnSc1
Signora	signora	k1gFnSc1
di	di	k?
Guadalupee	Guadalupe	k1gFnSc2
S.	S.	kA
Filippo	Filippa	k1gFnSc5
MartireDominæ	MartireDominæ	k1gMnSc6
Nostræ	Nostræ	k1gFnSc7
de	de	k?
Guadalupeet	Guadalupeeta	k1gFnPc2
S.	S.	kA
Philippi	Philipp	k1gFnSc2
martyris	martyris	k1gFnSc2
in	in	k?
Via	via	k7c4
AureliaTitulární	AureliaTitulární	k2eAgInSc4d1
kostel	kostel	k1gInSc4
</s>
<s>
Základní	základní	k2eAgInPc1d1
údaje	údaj	k1gInPc1
</s>
<s>
Datum	datum	k1gNnSc1
ustanovení	ustanovení	k1gNnSc2
<g/>
:	:	kIx,
<g/>
28	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1991	#num#	k4
</s>
<s>
Titulární	titulární	k2eAgMnSc1d1
kardinál	kardinál	k1gMnSc1
<g/>
:	:	kIx,
<g/>
Juan	Juan	k1gMnSc1
Sandoval	Sandoval	k1gMnSc1
Íñ	Íñ	k1gMnSc1
</s>
<s>
Datum	datum	k1gNnSc1
jmenování	jmenování	k1gNnSc2
<g/>
:	:	kIx,
<g/>
26	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1994	#num#	k4
</s>
<s>
Funkce	funkce	k1gFnSc1
kardinála	kardinál	k1gMnSc2
<g/>
:	:	kIx,
<g/>
Arcibiskup	arcibiskup	k1gMnSc1
Guadalajary	Guadalajara	k1gFnSc2
</s>
<s>
Nostra	Nostra	k1gFnSc1
Signora	signora	k1gFnSc1
di	di	k?
Guadalupe	Guadalup	k1gInSc5
e	e	k0
S.	S.	kA
Filippo	Filippa	k1gFnSc5
Martire	Martir	k1gInSc5
je	on	k3xPp3gMnPc4
kardinálský	kardinálský	k2eAgInSc1d1
titulární	titulární	k2eAgInSc1d1
kostel	kostel	k1gInSc1
ustanovený	ustanovený	k2eAgInSc1d1
28	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1991	#num#	k4
papežem	papež	k1gMnSc7
Janem	Jan	k1gMnSc7
Pavlam	Pavlam	k1gInSc4
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
kostel	kostel	k1gInSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
na	na	k7c4
Via	via	k7c4
Aurelia	Aurelium	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgMnSc7
titulárním	titulární	k2eAgMnSc7d1
kardinálem	kardinál	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Juan	Juan	k1gMnSc1
Jesús	Jesúsa	k1gFnPc2
Posadas	Posadas	k1gMnSc1
Ocampo	Ocampa	k1gFnSc5
arcibiskup	arcibiskup	k1gMnSc1
Guadalajary	Guadalajara	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Titulární	titulární	k2eAgMnPc1d1
kardinálové	kardinál	k1gMnPc1
</s>
<s>
JménoFunkceOdDo	JménoFunkceOdDo	k6eAd1
</s>
<s>
Juan	Juan	k1gMnSc1
Jesús	Jesúsa	k1gFnPc2
Posadas	Posadas	k1gMnSc1
OcampoArcibiskup	OcampoArcibiskup	k1gMnSc1
Guadalajary	Guadalajara	k1gFnSc2
(	(	kIx(
<g/>
Mexiko	Mexiko	k1gNnSc1
<g/>
)	)	kIx)
<g/>
28.6	28.6	k4
<g/>
.199124	.199124	k4
<g/>
.5	.5	k4
<g/>
.1993	.1993	k4
</s>
<s>
Juan	Juan	k1gMnSc1
Sandoval	Sandoval	k1gFnSc2
Íñ	Íñ	k2eAgMnSc1d1
arcibiskup	arcibiskup	k1gMnSc1
Guadalajary	Guadalajara	k1gFnSc2
(	(	kIx(
<g/>
Mexiko	Mexiko	k1gNnSc4
<g/>
26.11	26.11	k4
<g/>
.1994	.1994	k4
<g/>
současnost	současnost	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
GCatholic	GCatholice	k1gFnPc2
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Catholic	Catholice	k1gFnPc2
hierarchy	hierarcha	k1gMnPc4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Křesťanství	křesťanství	k1gNnSc1
</s>
