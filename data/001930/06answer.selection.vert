<s>
Úpadek	úpadek	k1gInSc1	úpadek
říše	říš	k1gFnSc2	říš
se	se	k3xPyFc4	se
za	za	k7c2	za
následníků	následník	k1gMnPc2	následník
Ludvíka	Ludvík	k1gMnSc4	Ludvík
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
stále	stále	k6eAd1	stále
prohluboval	prohlubovat	k5eAaImAgInS	prohlubovat
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
příčin	příčina	k1gFnPc2	příčina
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
propukla	propuknout	k5eAaPmAgFnS	propuknout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1789	[number]	k4	1789
a	a	k8xC	a
vedla	vést	k5eAaImAgFnS	vést
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1791	[number]	k4	1791
k	k	k7c3	k
nahrazení	nahrazení	k1gNnSc3	nahrazení
absolutistické	absolutistický	k2eAgFnSc2d1	absolutistická
monarchie	monarchie	k1gFnSc2	monarchie
konstituční	konstituční	k2eAgFnSc1d1	konstituční
<g/>
,	,	kIx,	,
22	[number]	k4	22
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1792	[number]	k4	1792
byla	být	k5eAaImAgFnS	být
následně	následně	k6eAd1	následně
nastolena	nastolen	k2eAgFnSc1d1	nastolena
První	první	k4xOgFnSc1	první
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
