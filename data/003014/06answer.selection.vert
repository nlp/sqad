<s>
Etologie	etologie	k1gFnSc1	etologie
je	být	k5eAaImIp3nS	být
vědní	vědní	k2eAgInSc4d1	vědní
obor	obor	k1gInSc4	obor
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
zoologie	zoologie	k1gFnSc2	zoologie
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
studiem	studio	k1gNnSc7	studio
chování	chování	k1gNnSc2	chování
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
rozeznáváním	rozeznávání	k1gNnSc7	rozeznávání
jeho	jeho	k3xOp3gFnPc2	jeho
vrozených	vrozený	k2eAgFnPc2d1	vrozená
a	a	k8xC	a
naučených	naučený	k2eAgFnPc2d1	naučená
složek	složka	k1gFnPc2	složka
<g/>
,	,	kIx,	,
ontogenetického	ontogenetický	k2eAgInSc2d1	ontogenetický
i	i	k8xC	i
fylogenetického	fylogenetický	k2eAgInSc2d1	fylogenetický
vývoje	vývoj	k1gInSc2	vývoj
vzorců	vzorec	k1gInPc2	vzorec
chování	chování	k1gNnSc1	chování
a	a	k8xC	a
významu	význam	k1gInSc2	význam
určitých	určitý	k2eAgInPc2d1	určitý
vzorců	vzorec	k1gInPc2	vzorec
chování	chování	k1gNnSc2	chování
pro	pro	k7c4	pro
přežívání	přežívání	k1gNnSc4	přežívání
daného	daný	k2eAgInSc2d1	daný
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
