<s>
Etologie	etologie	k1gFnSc1	etologie
je	být	k5eAaImIp3nS	být
vědní	vědní	k2eAgInSc4d1	vědní
obor	obor	k1gInSc4	obor
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
zoologie	zoologie	k1gFnSc2	zoologie
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
studiem	studio	k1gNnSc7	studio
chování	chování	k1gNnSc2	chování
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
rozeznáváním	rozeznávání	k1gNnSc7	rozeznávání
jeho	jeho	k3xOp3gFnPc2	jeho
vrozených	vrozený	k2eAgFnPc2d1	vrozená
a	a	k8xC	a
naučených	naučený	k2eAgFnPc2d1	naučená
složek	složka	k1gFnPc2	složka
<g/>
,	,	kIx,	,
ontogenetického	ontogenetický	k2eAgInSc2d1	ontogenetický
i	i	k8xC	i
fylogenetického	fylogenetický	k2eAgInSc2d1	fylogenetický
vývoje	vývoj	k1gInSc2	vývoj
vzorců	vzorec	k1gInPc2	vzorec
chování	chování	k1gNnSc1	chování
a	a	k8xC	a
významu	význam	k1gInSc2	význam
určitých	určitý	k2eAgInPc2d1	určitý
vzorců	vzorec	k1gInPc2	vzorec
chování	chování	k1gNnSc2	chování
pro	pro	k7c4	pro
přežívání	přežívání	k1gNnSc4	přežívání
daného	daný	k2eAgInSc2d1	daný
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
analýze	analýza	k1gFnSc6	analýza
chování	chování	k1gNnSc2	chování
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
podle	podle	k7c2	podle
Niko	nika	k1gFnSc5	nika
Tinbergena	Tinbergena	k1gFnSc1	Tinbergena
měl	mít	k5eAaImAgMnS	mít
etolog	etolog	k1gMnSc1	etolog
ptát	ptát	k5eAaImF	ptát
při	při	k7c6	při
analýze	analýza	k1gFnSc6	analýza
chování	chování	k1gNnSc2	chování
po	po	k7c6	po
těchto	tento	k3xDgNnPc6	tento
čtyřech	čtyři	k4xCgNnPc6	čtyři
tématech	téma	k1gNnPc6	téma
<g/>
:	:	kIx,	:
Funkce	funkce	k1gFnSc1	funkce
chování	chování	k1gNnSc2	chování
(	(	kIx(	(
<g/>
jak	jak	k8xS	jak
dané	daný	k2eAgNnSc1d1	dané
chování	chování	k1gNnSc1	chování
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
organismu	organismus	k1gInSc2	organismus
přežít	přežít	k5eAaPmF	přežít
<g/>
/	/	kIx~	/
<g/>
proč	proč	k6eAd1	proč
se	s	k7c7	s
šíří	šíř	k1gFnSc7	šíř
populací	populace	k1gFnPc2	populace
<g/>
)	)	kIx)	)
Příčina	příčina	k1gFnSc1	příčina
chování	chování	k1gNnSc2	chování
(	(	kIx(	(
<g/>
na	na	k7c4	na
jaké	jaký	k3yIgInPc4	jaký
stimuly	stimul	k1gInPc4	stimul
reaguje	reagovat	k5eAaBmIp3nS	reagovat
a	a	k8xC	a
jak	jak	k6eAd1	jak
působí	působit	k5eAaImIp3nS	působit
<g />
.	.	kIx.	.
</s>
<s>
je	on	k3xPp3gFnPc4	on
<g/>
-li	i	k?	-li
vrozené	vrozený	k2eAgInPc1d1	vrozený
či	či	k8xC	či
získané	získaný	k2eAgInPc1d1	získaný
<g/>
)	)	kIx)	)
Vývoj	vývoj	k1gInSc1	vývoj
(	(	kIx(	(
<g/>
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
s	s	k7c7	s
věkem	věk	k1gInSc7	věk
<g/>
)	)	kIx)	)
Historie	historie	k1gFnSc1	historie
(	(	kIx(	(
<g/>
které	který	k3yRgInPc1	který
jiné	jiný	k2eAgInPc1d1	jiný
druhy	druh	k1gInPc1	druh
sdílí	sdílet	k5eAaImIp3nP	sdílet
tento	tento	k3xDgInSc4	tento
znak	znak	k1gInSc4	znak
<g/>
)	)	kIx)	)
vrozené	vrozený	k2eAgNnSc4d1	vrozené
-	-	kIx~	-
existuje	existovat	k5eAaImIp3nS	existovat
hned	hned	k6eAd1	hned
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
řízeno	řídit	k5eAaImNgNnS	řídit
instinktem	instinkt	k1gInSc7	instinkt
(	(	kIx(	(
<g/>
stěhování	stěhování	k1gNnSc1	stěhování
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
reflexy	reflex	k1gInPc4	reflex
<g/>
)	)	kIx)	)
získané	získaný	k2eAgInPc4d1	získaný
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
řízeno	řízen	k2eAgNnSc1d1	řízeno
napodobováním	napodobování	k1gNnSc7	napodobování
a	a	k8xC	a
zkušenostmi	zkušenost	k1gFnPc7	zkušenost
získanými	získaný	k2eAgFnPc7d1	získaná
během	během	k7c2	během
života	život	k1gInSc2	život
jedince	jedinec	k1gMnSc2	jedinec
(	(	kIx(	(
<g/>
zlepšení	zlepšení	k1gNnSc1	zlepšení
techniky	technika	k1gFnSc2	technika
lovu	lov	k1gInSc2	lov
<g/>
)	)	kIx)	)
chování	chování	k1gNnSc4	chování
obranné	obranný	k2eAgNnSc4d1	obranné
<g/>
:	:	kIx,	:
útěk	útěk	k1gInSc4	útěk
<g/>
,	,	kIx,	,
obrana	obrana	k1gFnSc1	obrana
chování	chování	k1gNnSc1	chování
rozmnožovací	rozmnožovací	k2eAgFnSc2d1	rozmnožovací
<g/>
:	:	kIx,	:
námluvy	námluva	k1gFnSc2	námluva
<g/>
,	,	kIx,	,
zásnubní	zásnubní	k2eAgInSc4d1	zásnubní
lety	léto	k1gNnPc7	léto
chování	chování	k1gNnSc2	chování
komfortní	komfortní	k2eAgNnSc1d1	komfortní
<g/>
:	:	kIx,	:
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
péčí	péče	k1gFnSc7	péče
-	-	kIx~	-
hygiena	hygiena	k1gFnSc1	hygiena
<g/>
,	,	kIx,	,
koupání	koupání	k1gNnSc2	koupání
neuroetologie	neuroetologie	k1gFnSc2	neuroetologie
ekologická	ekologický	k2eAgFnSc1d1	ekologická
etologie	etologie	k1gFnSc1	etologie
Etologie	etologie	k1gFnSc1	etologie
jako	jako	k8xC	jako
vědní	vědní	k2eAgInSc1d1	vědní
obor	obor	k1gInSc1	obor
vykrystalizovala	vykrystalizovat	k5eAaPmAgFnS	vykrystalizovat
až	až	k6eAd1	až
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
práce	práce	k1gFnPc1	práce
zabývající	zabývající	k2eAgFnPc1d1	zabývající
se	s	k7c7	s
chováním	chování	k1gNnSc7	chování
zvířat	zvíře	k1gNnPc2	zvíře
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
daleko	daleko	k6eAd1	daleko
před	před	k7c7	před
tímto	tento	k3xDgNnSc7	tento
datem	datum	k1gNnSc7	datum
<g/>
,	,	kIx,	,
byť	byť	k8xS	byť
se	se	k3xPyFc4	se
zpravidla	zpravidla	k6eAd1	zpravidla
nejednalo	jednat	k5eNaImAgNnS	jednat
o	o	k7c4	o
díla	dílo	k1gNnPc4	dílo
psaná	psaný	k2eAgFnSc1d1	psaná
za	za	k7c7	za
tímto	tento	k3xDgInSc7	tento
účelem	účel	k1gInSc7	účel
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
důkladné	důkladný	k2eAgInPc1d1	důkladný
popisy	popis	k1gInPc1	popis
chování	chování	k1gNnSc1	chování
zvířat	zvíře	k1gNnPc2	zvíře
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaBmF	nalézt
ve	v	k7c6	v
spisech	spis	k1gInPc6	spis
Hérodotových	Hérodotův	k2eAgInPc6d1	Hérodotův
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
významnými	významný	k2eAgFnPc7d1	významná
pracemi	práce	k1gFnPc7	práce
jsou	být	k5eAaImIp3nP	být
Aristotelovy	Aristotelův	k2eAgInPc4d1	Aristotelův
spisy	spis	k1gInPc4	spis
(	(	kIx(	(
<g/>
popsal	popsat	k5eAaPmAgMnS	popsat
např.	např.	kA	např.
včelí	včelí	k2eAgInSc4d1	včelí
tanec	tanec	k1gInSc4	tanec
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Historia	Historium	k1gNnSc2	Historium
naturalis	naturalis	k1gFnSc2	naturalis
Plinia	Plinium	k1gNnSc2	Plinium
Staršího	starší	k1gMnSc2	starší
<g/>
,	,	kIx,	,
De	De	k?	De
arte	arte	k6eAd1	arte
venandi	venand	k1gMnPc1	venand
cum	cum	k?	cum
avibus	avibus	k1gInSc1	avibus
císaře	císař	k1gMnSc2	císař
Fridricha	Fridrich	k1gMnSc2	Fridrich
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Studiem	studio	k1gNnSc7	studio
chování	chování	k1gNnSc2	chování
zvířat	zvíře	k1gNnPc2	zvíře
žijících	žijící	k2eAgMnPc2d1	žijící
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
anglický	anglický	k2eAgMnSc1d1	anglický
fyziolog	fyziolog	k1gMnSc1	fyziolog
a	a	k8xC	a
lékař	lékař	k1gMnSc1	lékař
William	William	k1gInSc4	William
Harvey	Harvea	k1gFnSc2	Harvea
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgMnPc7d1	další
pionýry	pionýr	k1gMnPc7	pionýr
etologie	etologie	k1gFnSc2	etologie
byli	být	k5eAaImAgMnP	být
Johan	Johan	k1gMnSc1	Johan
Pernauer	Pernauer	k1gMnSc1	Pernauer
z	z	k7c2	z
Rosenau	Rosenaus	k1gInSc2	Rosenaus
a	a	k8xC	a
Condillac	Condillac	k1gInSc1	Condillac
(	(	kIx(	(
<g/>
oba	dva	k4xCgMnPc1	dva
se	se	k3xPyFc4	se
zabývali	zabývat	k5eAaImAgMnP	zabývat
ptactvem	ptactvo	k1gNnSc7	ptactvo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hermann	Hermann	k1gMnSc1	Hermann
S.	S.	kA	S.
Reinmarus	Reinmarus	k1gMnSc1	Reinmarus
(	(	kIx(	(
<g/>
bezobratlí	bezobratlí	k1gMnPc1	bezobratlí
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Erasmus	Erasmus	k1gMnSc1	Erasmus
Darwin	Darwin	k1gMnSc1	Darwin
<g/>
,	,	kIx,	,
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
Lamarck	Lamarck	k1gInSc1	Lamarck
<g/>
,	,	kIx,	,
George	George	k1gInSc1	George
Cuvier	Cuvier	k1gInSc1	Cuvier
<g/>
,	,	kIx,	,
Saint-Hilairové	Saint-Hilairové	k2eAgInSc1d1	Saint-Hilairové
<g/>
.	.	kIx.	.
</s>
<s>
Velkou	velký	k2eAgFnSc4d1	velká
pozornost	pozornost	k1gFnSc4	pozornost
etologii	etologie	k1gFnSc4	etologie
věnovali	věnovat	k5eAaPmAgMnP	věnovat
oba	dva	k4xCgMnPc1	dva
tvůrci	tvůrce	k1gMnPc1	tvůrce
evoluční	evoluční	k2eAgFnSc2d1	evoluční
teorie	teorie	k1gFnSc2	teorie
<g/>
,	,	kIx,	,
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
a	a	k8xC	a
Alfred	Alfred	k1gMnSc1	Alfred
Russel	Russel	k1gMnSc1	Russel
Wallace	Wallace	k1gFnSc1	Wallace
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k1gNnSc1	další
významné	významný	k2eAgFnSc2d1	významná
práce	práce	k1gFnSc2	práce
přednesli	přednést	k5eAaPmAgMnP	přednést
John	John	k1gMnSc1	John
Lubbock	Lubbock	k1gMnSc1	Lubbock
(	(	kIx(	(
<g/>
komunikace	komunikace	k1gFnSc1	komunikace
hmyzu	hmyz	k1gInSc2	hmyz
a	a	k8xC	a
etologie	etologie	k1gFnSc2	etologie
psů	pes	k1gMnPc2	pes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
C.	C.	kA	C.
LLoyd	LLoyd	k1gMnSc1	LLoyd
Morgan	morgan	k1gMnSc1	morgan
(	(	kIx(	(
<g/>
etologie	etologie	k1gFnPc1	etologie
kuřat	kuře	k1gNnPc2	kuře
a	a	k8xC	a
kachen	kachna	k1gFnPc2	kachna
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Edward	Edward	k1gMnSc1	Edward
T.	T.	kA	T.
Thorndik	Thorndik	k1gMnSc1	Thorndik
(	(	kIx(	(
<g/>
zvířecí	zvířecí	k2eAgFnSc1d1	zvířecí
inteligence	inteligence	k1gFnSc1	inteligence
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
J.	J.	kA	J.
Henri	Henr	k1gMnSc5	Henr
Fabre	Fabr	k1gMnSc5	Fabr
(	(	kIx(	(
<g/>
chování	chování	k1gNnSc4	chování
hmyzu	hmyz	k1gInSc2	hmyz
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Jakob	Jakob	k1gMnSc1	Jakob
von	von	k1gInSc4	von
Uexküll	Uexküll	k1gMnSc1	Uexküll
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
zakladatele	zakladatel	k1gMnSc2	zakladatel
etologie	etologie	k1gFnSc2	etologie
jsou	být	k5eAaImIp3nP	být
považování	považování	k1gNnSc4	považování
Konrad	Konrada	k1gFnPc2	Konrada
Lorenz	Lorenz	k1gMnSc1	Lorenz
<g/>
,	,	kIx,	,
Erich	Erich	k1gMnSc1	Erich
von	von	k1gInSc1	von
Holst	Holst	k1gFnSc1	Holst
<g/>
,	,	kIx,	,
Karl	Karl	k1gInSc1	Karl
von	von	k1gInSc1	von
Frisch	Frisch	k1gInSc1	Frisch
a	a	k8xC	a
Nikolaas	Nikolaas	k1gInSc1	Nikolaas
Tinbergen	Tinbergen	k1gInSc1	Tinbergen
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějším	významný	k2eAgMnSc7d3	nejvýznamnější
českým	český	k2eAgMnSc7d1	český
etologem	etolog	k1gMnSc7	etolog
byl	být	k5eAaImAgMnS	být
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Veselovský	Veselovský	k1gMnSc1	Veselovský
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
zakladatele	zakladatel	k1gMnPc4	zakladatel
české	český	k2eAgFnSc2d1	Česká
etologie	etologie	k1gFnSc2	etologie
<g/>
.	.	kIx.	.
</s>
<s>
Slovník	slovník	k1gInSc1	slovník
Merriam-Webster	Merriam-Webster	k1gInSc1	Merriam-Webster
definuje	definovat	k5eAaBmIp3nS	definovat
instinkt	instinkt	k1gInSc4	instinkt
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
části	část	k1gFnSc2	část
dědičnou	dědičný	k2eAgFnSc4d1	dědičná
a	a	k8xC	a
neměnitelnou	měnitelný	k2eNgFnSc4d1	neměnitelná
tendenci	tendence	k1gFnSc4	tendence
organismu	organismus	k1gInSc2	organismus
vykonávat	vykonávat	k5eAaImF	vykonávat
komplexní	komplexní	k2eAgInSc4d1	komplexní
a	a	k8xC	a
specifické	specifický	k2eAgFnPc4d1	specifická
odpovědi	odpověď	k1gFnPc4	odpověď
na	na	k7c4	na
stimul	stimul	k1gInSc4	stimul
prostředí	prostředí	k1gNnSc2	prostředí
bez	bez	k7c2	bez
zapojení	zapojení	k1gNnSc2	zapojení
rozumu	rozum	k1gInSc2	rozum
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Fixed	Fixed	k1gInSc1	Fixed
action	action	k1gInSc1	action
pattern	pattern	k1gInSc1	pattern
<g/>
.	.	kIx.	.
</s>
<s>
Důležitý	důležitý	k2eAgInSc1d1	důležitý
posun	posun	k1gInSc1	posun
v	v	k7c6	v
etologii	etologie	k1gFnSc6	etologie
je	být	k5eAaImIp3nS	být
spojený	spojený	k2eAgInSc1d1	spojený
s	s	k7c7	s
rakouským	rakouský	k2eAgMnSc7d1	rakouský
etologem	etolog	k1gMnSc7	etolog
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
Konradem	Konrad	k1gInSc7	Konrad
Lorenzem	Lorenz	k1gMnSc7	Lorenz
(	(	kIx(	(
<g/>
přestože	přestože	k8xS	přestože
objev	objev	k1gInSc4	objev
učinil	učinit	k5eAaImAgMnS	učinit
spíše	spíše	k9	spíše
jeho	jeho	k3xOp3gMnSc1	jeho
učitel	učitel	k1gMnSc1	učitel
Oskar	Oskar	k1gMnSc1	Oskar
Heinroth	Heinroth	k1gMnSc1	Heinroth
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
Lorenz	Lorenz	k1gMnSc1	Lorenz
se	se	k3xPyFc4	se
přičinil	přičinit	k5eAaPmAgMnS	přičinit
o	o	k7c6	o
rozšíření	rozšíření	k1gNnSc6	rozšíření
znalosti	znalost	k1gFnSc2	znalost
fixed	fixed	k1gInSc1	fixed
action	action	k1gInSc1	action
patterns	patterns	k1gInSc1	patterns
(	(	kIx(	(
<g/>
FAPs	FAPs	k1gInSc1	FAPs
<g/>
)	)	kIx)	)
-	-	kIx~	-
pevně	pevně	k6eAd1	pevně
stanovené	stanovený	k2eAgFnPc4d1	stanovená
odpovědi	odpověď	k1gFnPc4	odpověď
organismu	organismus	k1gInSc2	organismus
-	-	kIx~	-
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
dnes	dnes	k6eAd1	dnes
víme	vědět	k5eAaImIp1nP	vědět
"	"	kIx"	"
<g/>
automaticky	automaticky	k6eAd1	automaticky
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
zapojení	zapojení	k1gNnSc2	zapojení
zadního	zadní	k2eAgInSc2d1	zadní
mozku	mozek	k1gInSc2	mozek
<g/>
)	)	kIx)	)
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
sympatické	sympatický	k2eAgInPc1d1	sympatický
a	a	k8xC	a
parasympatické	parasympatický	k2eAgInPc1d1	parasympatický
nervy	nerv	k1gInPc1	nerv
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
současné	současný	k2eAgFnSc2d1	současná
znalosti	znalost	k1gFnSc2	znalost
jsou	být	k5eAaImIp3nP	být
hlavním	hlavní	k2eAgInSc7d1	hlavní
motorem	motor	k1gInSc7	motor
nevědomých	vědomý	k2eNgFnPc2d1	nevědomá
tělesných	tělesný	k2eAgFnPc2d1	tělesná
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
jsou	být	k5eAaImIp3nP	být
reflexy	reflex	k1gInPc1	reflex
již	již	k6eAd1	již
dobře	dobře	k6eAd1	dobře
zmapované	zmapovaný	k2eAgFnPc1d1	zmapovaná
a	a	k8xC	a
používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
například	například	k6eAd1	například
k	k	k7c3	k
diagnostice	diagnostika	k1gFnSc3	diagnostika
nervových	nervový	k2eAgFnPc2d1	nervová
poruch	porucha	k1gFnPc2	porucha
v	v	k7c6	v
každé	každý	k3xTgFnSc6	každý
neurologické	urologický	k2eNgFnSc6d1	urologický
ordinaci	ordinace	k1gFnSc6	ordinace
<g/>
.	.	kIx.	.
</s>
<s>
Habituace	Habituace	k1gFnSc1	Habituace
je	být	k5eAaImIp3nS	být
jednoduchá	jednoduchý	k2eAgFnSc1d1	jednoduchá
forma	forma	k1gFnSc1	forma
učení	učení	k1gNnSc2	učení
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
proces	proces	k1gInSc1	proces
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
zvíře	zvíře	k1gNnSc1	zvíře
zablokuje	zablokovat	k5eAaPmIp3nS	zablokovat
svou	svůj	k3xOyFgFnSc4	svůj
odpověď	odpověď	k1gFnSc4	odpověď
na	na	k7c4	na
stimul	stimul	k1gInSc4	stimul
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
prérijní	prérijní	k2eAgMnPc1d1	prérijní
psi	pes	k1gMnPc1	pes
(	(	kIx(	(
<g/>
Cynomys	Cynomys	k1gInSc1	Cynomys
ludovicianus	ludovicianus	k1gInSc1	ludovicianus
<g/>
)	)	kIx)	)
za	za	k7c2	za
normálních	normální	k2eAgFnPc2d1	normální
okolností	okolnost	k1gFnPc2	okolnost
odstrašují	odstrašovat	k5eAaImIp3nP	odstrašovat
predátora	predátor	k1gMnSc2	predátor
štěkáním	štěkání	k1gNnSc7	štěkání
-	-	kIx~	-
pokud	pokud	k8xS	pokud
ale	ale	k8xC	ale
žijí	žít	k5eAaImIp3nP	žít
ve	v	k7c6	v
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
reflex	reflex	k1gInSc1	reflex
se	se	k3xPyFc4	se
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
<g/>
.	.	kIx.	.
</s>
<s>
Neurofyziologicky	neurofyziologicky	k6eAd1	neurofyziologicky
přespříliš	přespříliš	k6eAd1	přespříliš
opakovaný	opakovaný	k2eAgInSc4d1	opakovaný
nervový	nervový	k2eAgInSc4d1	nervový
signál	signál	k1gInSc4	signál
postupně	postupně	k6eAd1	postupně
přestane	přestat	k5eAaPmIp3nS	přestat
účinkovat	účinkovat	k5eAaImF	účinkovat
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
se	se	k3xPyFc4	se
tomu	ten	k3xDgMnSc3	ten
tak	tak	k9	tak
děje	dít	k5eAaImIp3nS	dít
například	například	k6eAd1	například
při	při	k7c6	při
jamais	jamais	k1gFnSc6	jamais
vu	vu	k?	vu
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Asociace	asociace	k1gFnSc2	asociace
(	(	kIx(	(
<g/>
psychologie	psychologie	k1gFnSc1	psychologie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Asociativní	asociativní	k2eAgNnSc1d1	asociativní
učení	učení	k1gNnSc1	učení
označuje	označovat	k5eAaImIp3nS	označovat
v	v	k7c6	v
kontextu	kontext	k1gInSc6	kontext
chování	chování	k1gNnSc2	chování
zvířat	zvíře	k1gNnPc2	zvíře
proces	proces	k1gInSc1	proces
učení	učení	k1gNnSc2	učení
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nová	nový	k2eAgFnSc1d1	nová
odpověď	odpověď	k1gFnSc1	odpověď
začne	začít	k5eAaPmIp3nS	začít
spojovat	spojovat	k5eAaImF	spojovat
s	s	k7c7	s
určitým	určitý	k2eAgInSc7d1	určitý
stimulem	stimul	k1gInSc7	stimul
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc4	první
studie	studie	k1gFnPc4	studie
provedl	provést	k5eAaPmAgMnS	provést
fyziolog	fyziolog	k1gMnSc1	fyziolog
Ivan	Ivan	k1gMnSc1	Ivan
P.	P.	kA	P.
Pavlov	Pavlov	k1gInSc1	Pavlov
<g/>
.	.	kIx.	.
</s>
<s>
Asociativní	asociativní	k2eAgNnSc4d1	asociativní
učení	učení	k1gNnSc4	učení
mohou	moct	k5eAaImIp3nP	moct
například	například	k6eAd1	například
pozorovat	pozorovat	k5eAaImF	pozorovat
chovatelé	chovatel	k1gMnPc1	chovatel
zlatých	zlatý	k2eAgFnPc2d1	zlatá
rybek	rybka	k1gFnPc2	rybka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
připlavou	připlavat	k5eAaPmIp3nP	připlavat
ke	k	k7c3	k
hladině	hladina	k1gFnSc3	hladina
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
je	být	k5eAaImIp3nS	být
majitel	majitel	k1gMnSc1	majitel
chystá	chystat	k5eAaImIp3nS	chystat
nakrmit	nakrmit	k5eAaPmF	nakrmit
<g/>
,	,	kIx,	,
či	či	k8xC	či
psů	pes	k1gMnPc2	pes
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
začínají	začínat	k5eAaImIp3nP	začínat
připravovat	připravovat	k5eAaImF	připravovat
na	na	k7c4	na
procházku	procházka	k1gFnSc4	procházka
<g/>
,	,	kIx,	,
kdykoli	kdykoli	k6eAd1	kdykoli
vidí	vidět	k5eAaImIp3nS	vidět
svého	svůj	k3xOyFgMnSc4	svůj
pána	pán	k1gMnSc4	pán
s	s	k7c7	s
vodítkem	vodítko	k1gNnSc7	vodítko
<g/>
.	.	kIx.	.
</s>
<s>
Asociativní	asociativní	k2eAgNnSc1d1	asociativní
učení	učení	k1gNnSc1	učení
souvisí	souviset	k5eAaImIp3nS	souviset
se	s	k7c7	s
vznikem	vznik	k1gInSc7	vznik
také	také	k9	také
například	například	k6eAd1	například
pověrčivého	pověrčivý	k2eAgNnSc2d1	pověrčivé
chování	chování	k1gNnSc2	chování
u	u	k7c2	u
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Holubi	holub	k1gMnPc1	holub
se	se	k3xPyFc4	se
v	v	k7c6	v
experimentech	experiment	k1gInPc6	experiment
snažili	snažit	k5eAaImAgMnP	snažit
napodobit	napodobit	k5eAaPmF	napodobit
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
zrovna	zrovna	k6eAd1	zrovna
dělali	dělat	k5eAaImAgMnP	dělat
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
jim	on	k3xPp3gInPc3	on
(	(	kIx(	(
<g/>
v	v	k7c4	v
náhodnou	náhodný	k2eAgFnSc4d1	náhodná
chvíli	chvíle	k1gFnSc4	chvíle
<g/>
)	)	kIx)	)
výzkumníci	výzkumník	k1gMnPc1	výzkumník
dali	dát	k5eAaPmAgMnP	dát
semínko	semínko	k1gNnSc4	semínko
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
naučeného	naučený	k2eAgNnSc2d1	naučené
chování	chování	k1gNnSc2	chování
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
brzkého	brzký	k2eAgInSc2d1	brzký
věku	věk	k1gInSc2	věk
<g/>
.	.	kIx.	.
</s>
<s>
Speciální	speciální	k2eAgInSc1d1	speciální
případ	případ	k1gInSc1	případ
takového	takový	k3xDgNnSc2	takový
učení	učení	k1gNnSc2	učení
pozoroval	pozorovat	k5eAaImAgMnS	pozorovat
poprvé	poprvé	k6eAd1	poprvé
Lorenz	Lorenz	k1gMnSc1	Lorenz
na	na	k7c6	na
mladých	mladý	k2eAgNnPc6d1	mladé
housatech	house	k1gNnPc6	house
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
chodí	chodit	k5eAaImIp3nP	chodit
za	za	k7c7	za
svojí	svůj	k3xOyFgFnSc7	svůj
matkou	matka	k1gFnSc7	matka
téměř	téměř	k6eAd1	téměř
ode	ode	k7c2	ode
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
vylíhnou	vylíhnout	k5eAaPmIp3nP	vylíhnout
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
navodíme	navodit	k5eAaPmIp1nP	navodit
správné	správný	k2eAgFnPc4d1	správná
podmínky	podmínka	k1gFnPc4	podmínka
v	v	k7c6	v
době	doba	k1gFnSc6	doba
klubání	klubání	k1gNnSc2	klubání
<g/>
,	,	kIx,	,
můžeme	moct	k5eAaImIp1nP	moct
stejné	stejný	k2eAgNnSc4d1	stejné
chování	chování	k1gNnSc4	chování
nabudit	nabudit	k5eAaPmF	nabudit
i	i	k9	i
uměle	uměle	k6eAd1	uměle
<g/>
.	.	kIx.	.
</s>
<s>
Kulturní	kulturní	k2eAgNnSc1d1	kulturní
učení	učení	k1gNnSc1	učení
(	(	kIx(	(
<g/>
cultural	culturat	k5eAaImAgInS	culturat
transmission	transmission	k1gInSc1	transmission
<g/>
)	)	kIx)	)
označuje	označovat	k5eAaImIp3nS	označovat
jakékoliv	jakýkoliv	k3yIgNnSc1	jakýkoliv
chování	chování	k1gNnSc1	chování
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
předáváno	předávat	k5eAaImNgNnS	předávat
pomocí	pomocí	k7c2	pomocí
imitace	imitace	k1gFnSc2	imitace
<g/>
,	,	kIx,	,
výuky	výuka	k1gFnSc2	výuka
<g/>
,	,	kIx,	,
učení	učení	k1gNnSc2	učení
či	či	k8xC	či
pozorování	pozorování	k1gNnSc2	pozorování
z	z	k7c2	z
rodičů	rodič	k1gMnPc2	rodič
na	na	k7c4	na
potomky	potomek	k1gMnPc4	potomek
(	(	kIx(	(
<g/>
vertikální	vertikální	k2eAgMnPc4d1	vertikální
memy	mem	k1gMnPc4	mem
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
výzkumy	výzkum	k1gInPc1	výzkum
i	i	k9	i
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
vlčích	vlčí	k2eAgFnPc2d1	vlčí
dětí	dítě	k1gFnPc2	dítě
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kulturní	kulturní	k2eAgNnSc4d1	kulturní
učení	učení	k1gNnSc4	učení
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
lidí	člověk	k1gMnPc2	člověk
klíčové	klíčový	k2eAgNnSc1d1	klíčové
pro	pro	k7c4	pro
schopnost	schopnost	k1gFnSc4	schopnost
ovládat	ovládat	k5eAaImF	ovládat
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
socializaci	socializace	k1gFnSc4	socializace
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
sexuální	sexuální	k2eAgFnSc4d1	sexuální
orientaci	orientace	k1gFnSc4	orientace
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
zvířat	zvíře	k1gNnPc2	zvíře
mohou	moct	k5eAaImIp3nP	moct
složité	složitý	k2eAgInPc1d1	složitý
kulturní	kulturní	k2eAgInPc1d1	kulturní
zvyky	zvyk	k1gInPc1	zvyk
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
při	při	k7c6	při
"	"	kIx"	"
<g/>
svatebních	svatební	k2eAgInPc6d1	svatební
tancích	tanec	k1gInPc6	tanec
<g/>
"	"	kIx"	"
některých	některý	k3yIgInPc2	některý
primátů	primát	k1gInPc2	primát
či	či	k8xC	či
dialektech	dialekt	k1gInPc6	dialekt
písní	píseň	k1gFnPc2	píseň
kobylek	kobylka	k1gFnPc2	kobylka
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterých	který	k3yQgMnPc2	který
poznají	poznat	k5eAaPmIp3nP	poznat
příslušníky	příslušník	k1gMnPc7	příslušník
stejného	stejný	k2eAgInSc2d1	stejný
kmene	kmen	k1gInSc2	kmen
<g/>
)	)	kIx)	)
zabránit	zabránit	k5eAaPmF	zabránit
hybridizaci	hybridizace	k1gFnSc4	hybridizace
<g/>
.	.	kIx.	.
</s>
<s>
Imitace	imitace	k1gFnSc1	imitace
je	být	k5eAaImIp3nS	být
pokročilou	pokročilý	k2eAgFnSc7d1	pokročilá
formou	forma	k1gFnSc7	forma
učení	učení	k1gNnSc2	učení
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
si	se	k3xPyFc3	se
potomci	potomek	k1gMnPc1	potomek
vštěpují	vštěpovat	k5eAaBmIp3nP	vštěpovat
chování	chování	k1gNnSc4	chování
napodobováním	napodobování	k1gNnSc7	napodobování
-	-	kIx~	-
nejlépe	dobře	k6eAd3	dobře
rodičů	rodič	k1gMnPc2	rodič
<g/>
;	;	kIx,	;
například	například	k6eAd1	například
u	u	k7c2	u
papoušků	papoušek	k1gMnPc2	papoušek
či	či	k8xC	či
opic	opice	k1gFnPc2	opice
se	se	k3xPyFc4	se
ale	ale	k9	ale
schopnost	schopnost	k1gFnSc1	schopnost
své	svůj	k3xOyFgMnPc4	svůj
rodiče	rodič	k1gMnPc4	rodič
napodobovat	napodobovat	k5eAaImF	napodobovat
posouvá	posouvat	k5eAaImIp3nS	posouvat
v	v	k7c6	v
případě	případ	k1gInSc6	případ
chování	chování	k1gNnSc2	chování
lidmi	člověk	k1gMnPc7	člověk
i	i	k8xC	i
na	na	k7c4	na
jejich	jejich	k3xOp3gMnSc4	jejich
chovatele	chovatel	k1gMnSc4	chovatel
<g/>
.	.	kIx.	.
</s>
<s>
Evoluce	evoluce	k1gFnSc1	evoluce
napodobování	napodobování	k1gNnSc2	napodobování
byla	být	k5eAaImAgFnS	být
nedávno	nedávno	k6eAd1	nedávno
zkoumána	zkoumat	k5eAaImNgFnS	zkoumat
i	i	k9	i
na	na	k7c6	na
šimpanzech	šimpanza	k1gMnPc6	šimpanza
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
si	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
ve	v	k7c6	v
shodě	shoda	k1gFnSc6	shoda
s	s	k7c7	s
hypotézou	hypotéza	k1gFnSc7	hypotéza
učení	učení	k1gNnSc2	učení
<g/>
,	,	kIx,	,
více	hodně	k6eAd2	hodně
užívali	užívat	k5eAaImAgMnP	užívat
napodobování	napodobování	k1gNnSc4	napodobování
hierarchicky	hierarchicky	k6eAd1	hierarchicky
výše	vysoce	k6eAd2	vysoce
postavených	postavený	k2eAgMnPc2d1	postavený
starších	starý	k2eAgMnPc2d2	starší
šimpanzů	šimpanz	k1gMnPc2	šimpanz
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
než	než	k8xS	než
mladších	mladý	k2eAgFnPc6d2	mladší
<g/>
.	.	kIx.	.
</s>
<s>
Zvířata	zvíře	k1gNnPc1	zvíře
často	často	k6eAd1	často
žijí	žít	k5eAaImIp3nP	žít
ve	v	k7c6	v
složitých	složitý	k2eAgFnPc6d1	složitá
hierarchických	hierarchický	k2eAgFnPc6d1	hierarchická
strukturách	struktura	k1gFnPc6	struktura
a	a	k8xC	a
udržování	udržování	k1gNnSc4	udržování
vzájemných	vzájemný	k2eAgInPc2d1	vzájemný
vztahů	vztah	k1gInPc2	vztah
často	často	k6eAd1	často
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
složitý	složitý	k2eAgInSc4d1	složitý
evoluční	evoluční	k2eAgInSc4d1	evoluční
aparát	aparát	k1gInSc4	aparát
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgFnPc2	některý
teorií	teorie	k1gFnPc2	teorie
stojí	stát	k5eAaImIp3nS	stát
i	i	k9	i
za	za	k7c7	za
vývojem	vývoj	k1gInSc7	vývoj
jazyka	jazyk	k1gInSc2	jazyk
<g/>
,	,	kIx,	,
jednoznačně	jednoznačně	k6eAd1	jednoznačně
tak	tak	k6eAd1	tak
funguje	fungovat	k5eAaImIp3nS	fungovat
například	například	k6eAd1	například
grooming	grooming	k1gInSc1	grooming
(	(	kIx(	(
<g/>
vzájemné	vzájemný	k2eAgNnSc1d1	vzájemné
zbavování	zbavování	k1gNnSc1	zbavování
parazitů	parazit	k1gMnPc2	parazit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Etologie	etologie	k1gFnSc1	etologie
může	moct	k5eAaImIp3nS	moct
na	na	k7c6	na
zvířecích	zvířecí	k2eAgNnPc6d1	zvířecí
společenstvích	společenství	k1gNnPc6	společenství
zkoumat	zkoumat	k5eAaImF	zkoumat
<g/>
:	:	kIx,	:
Strukturu	struktura	k1gFnSc4	struktura
skupiny	skupina	k1gFnSc2	skupina
(	(	kIx(	(
<g/>
vztah	vztah	k1gInSc1	vztah
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
,	,	kIx,	,
hierarchie	hierarchie	k1gFnSc1	hierarchie
<g/>
,	,	kIx,	,
dominance	dominance	k1gFnSc1	dominance
alfa-samců	alfaamec	k1gMnPc2	alfa-samec
<g/>
:	:	kIx,	:
<g/>
zkušenějších	zkušený	k2eAgMnPc2d2	zkušenější
<g/>
/	/	kIx~	/
<g/>
zdatnějších	zdatný	k2eAgMnPc2d2	zdatnější
<g/>
)	)	kIx)	)
Vnitřní	vnitřní	k2eAgInPc1d1	vnitřní
vztahy	vztah	k1gInPc1	vztah
(	(	kIx(	(
<g/>
sdílení	sdílení	k1gNnSc1	sdílení
zdrojů	zdroj	k1gInPc2	zdroj
<g/>
,	,	kIx,	,
vnitřní	vnitřní	k2eAgInPc4d1	vnitřní
boje	boj	k1gInPc4	boj
<g/>
,	,	kIx,	,
altruismus	altruismus	k1gInSc4	altruismus
<g/>
,	,	kIx,	,
zápasy	zápas	k1gInPc4	zápas
o	o	k7c4	o
samice	samice	k1gFnPc4	samice
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Komunikaci	komunikace	k1gFnSc4	komunikace
(	(	kIx(	(
<g/>
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
proměnlivost	proměnlivost	k1gFnSc1	proměnlivost
<g/>
)	)	kIx)	)
Skupinové	skupinový	k2eAgNnSc1d1	skupinové
chování	chování	k1gNnSc1	chování
(	(	kIx(	(
<g/>
koordinaci	koordinace	k1gFnSc3	koordinace
chování	chování	k1gNnSc2	chování
<g/>
)	)	kIx)	)
Etologie	etologie	k1gFnSc1	etologie
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
sexuálního	sexuální	k2eAgNnSc2d1	sexuální
chování	chování	k1gNnSc2	chování
zvířat	zvíře	k1gNnPc2	zvíře
zkoumat	zkoumat	k5eAaImF	zkoumat
<g/>
:	:	kIx,	:
Pohlavní	pohlavní	k2eAgInSc4d1	pohlavní
výběr	výběr	k1gInSc4	výběr
a	a	k8xC	a
preferenci	preference	k1gFnSc4	preference
samic	samice	k1gFnPc2	samice
Dominanci	dominance	k1gFnSc4	dominance
mezi	mezi	k7c7	mezi
pohlavími	pohlaví	k1gNnPc7	pohlaví
a	a	k8xC	a
harémy	harém	k1gInPc1	harém
Námluvní	námluvní	k2eAgInPc1d1	námluvní
rituály	rituál	k1gInPc1	rituál
a	a	k8xC	a
svatební	svatební	k2eAgInPc1d1	svatební
tance	tanec	k1gInPc1	tanec
"	"	kIx"	"
<g/>
Bitvu	bitva	k1gFnSc4	bitva
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
"	"	kIx"	"
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
mechanismy	mechanismus	k1gInPc1	mechanismus
reprodukce	reprodukce	k1gFnSc2	reprodukce
Samčí	samčí	k2eAgInPc1d1	samčí
zápasy	zápas	k1gInPc1	zápas
a	a	k8xC	a
lákání	lákání	k1gNnSc4	lákání
samic	samice	k1gFnPc2	samice
George	Georg	k1gMnSc2	Georg
Barlow	Barlow	k1gMnSc2	Barlow
Patrick	Patrick	k1gMnSc1	Patrick
Bateson	Bateson	k1gMnSc1	Bateson
John	John	k1gMnSc1	John
H.	H.	kA	H.
Crook	Crook	k1gInSc1	Crook
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
Richard	Richard	k1gMnSc1	Richard
Dawkins	Dawkinsa	k1gFnPc2	Dawkinsa
Vitus	Vitus	k1gMnSc1	Vitus
B.	B.	kA	B.
Dröscher	Dröschra	k1gFnPc2	Dröschra
Dian	Diana	k1gFnPc2	Diana
Fossey	Fossea	k1gFnSc2	Fossea
Karl	Karla	k1gFnPc2	Karla
von	von	k1gInSc1	von
Frisch	Frisch	k1gMnSc1	Frisch
Jane	Jan	k1gMnSc5	Jan
Goodall	Goodall	k1gInSc1	Goodall
Temple	templ	k1gInSc5	templ
Grandin	Grandin	k2eAgMnSc1d1	Grandin
Oskar	Oskar	k1gMnSc1	Oskar
Heinroth	Heinroth	k1gMnSc1	Heinroth
Robert	Robert	k1gMnSc1	Robert
Hinde	hind	k1gMnSc5	hind
Julian	Julian	k1gMnSc1	Julian
Huxley	Huxlea	k1gFnSc2	Huxlea
Julian	Julian	k1gMnSc1	Julian
Jaynes	Jaynes	k1gMnSc1	Jaynes
Konrad	Konrada	k1gFnPc2	Konrada
Lorenz	Lorenz	k1gMnSc1	Lorenz
Desmond	Desmond	k1gMnSc1	Desmond
Morris	Morris	k1gFnSc2	Morris
Ivan	Ivan	k1gMnSc1	Ivan
Pavlov	Pavlov	k1gInSc1	Pavlov
B.	B.	kA	B.
F.	F.	kA	F.
Skinner	Skinner	k1gInSc1	Skinner
William	William	k1gInSc1	William
Thorpe	Thorp	k1gInSc5	Thorp
Nikolaas	Nikolaasa	k1gFnPc2	Nikolaasa
Tinbergen	Tinbergen	k1gInSc1	Tinbergen
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Veselovský	Veselovský	k1gMnSc1	Veselovský
William	William	k1gInSc4	William
Morton	Morton	k1gInSc1	Morton
Wheeler	Wheeler	k1gInSc1	Wheeler
E.	E.	kA	E.
O.	O.	kA	O.
Wilson	Wilson	k1gMnSc1	Wilson
Frans	Frans	k1gMnSc1	Frans
de	de	k?	de
Waal	Waal	k1gMnSc1	Waal
</s>
