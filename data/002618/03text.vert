<s>
Železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
Margecany	Margecana	k1gFnSc2	Margecana
-	-	kIx~	-
Červená	Červená	k1gFnSc1	Červená
Skala	skát	k5eAaImAgFnS	skát
je	on	k3xPp3gNnSc4	on
jednokolejná	jednokolejný	k2eAgFnSc1d1	jednokolejná
železniční	železniční	k2eAgFnSc1d1	železniční
trať	trať	k1gFnSc1	trať
vedená	vedený	k2eAgFnSc1d1	vedená
z	z	k7c2	z
Margecan	Margecana	k1gFnPc2	Margecana
(	(	kIx(	(
<g/>
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
Košice	Košice	k1gInPc4	Košice
-	-	kIx~	-
Žilina	Žilina	k1gFnSc1	Žilina
<g/>
)	)	kIx)	)
údolím	údolí	k1gNnSc7	údolí
Hnilce	Hnilec	k1gInSc2	Hnilec
do	do	k7c2	do
obce	obec	k1gFnSc2	obec
Červená	červenat	k5eAaImIp3nS	červenat
Skala	Skala	k1gMnSc1	Skala
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
navazuje	navazovat	k5eAaImIp3nS	navazovat
trať	trať	k1gFnSc1	trať
do	do	k7c2	do
Banské	banský	k2eAgFnSc2d1	Banská
Bystrice	Bystrica	k1gFnSc2	Bystrica
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
tratě	trať	k1gFnSc2	trať
byla	být	k5eAaImAgFnS	být
zahájena	zahájen	k2eAgFnSc1d1	zahájena
31	[number]	k4	31
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1931	[number]	k4	1931
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nedostatku	nedostatek	k1gInSc2	nedostatek
státních	státní	k2eAgFnPc2d1	státní
financí	finance	k1gFnPc2	finance
jako	jako	k8xC	jako
důsledek	důsledek	k1gInSc1	důsledek
světové	světový	k2eAgFnSc2d1	světová
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
stavba	stavba	k1gFnSc1	stavba
probíhala	probíhat	k5eAaImAgFnS	probíhat
pomalu	pomalu	k6eAd1	pomalu
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
dokončena	dokončen	k2eAgFnSc1d1	dokončena
byla	být	k5eAaImAgFnS	být
až	až	k9	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1936	[number]	k4	1936
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
úseku	úsek	k1gInSc2	úsek
Margecany	Margecana	k1gFnSc2	Margecana
-	-	kIx~	-
Gelnica	Gelnica	k1gMnSc1	Gelnica
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
stavbu	stavba	k1gFnSc4	stavba
dráhy	dráha	k1gFnSc2	dráha
použito	použit	k2eAgNnSc1d1	použito
těleso	těleso	k1gNnSc1	těleso
bývalé	bývalý	k2eAgFnSc2d1	bývalá
úzkokolejky	úzkokolejka	k1gFnSc2	úzkokolejka
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
úplnou	úplný	k2eAgFnSc4d1	úplná
novostavbu	novostavba	k1gFnSc4	novostavba
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
stavěna	stavit	k5eAaImNgFnS	stavit
v	v	k7c6	v
náročném	náročný	k2eAgInSc6d1	náročný
horském	horský	k2eAgInSc6d1	horský
terénu	terén	k1gInSc6	terén
<g/>
.	.	kIx.	.
</s>
<s>
Trať	trať	k1gFnSc1	trať
byla	být	k5eAaImAgFnS	být
dána	dát	k5eAaPmNgFnS	dát
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1936	[number]	k4	1936
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
trati	trať	k1gFnSc6	trať
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
devět	devět	k4xCc1	devět
tunélů	tunél	k1gInPc2	tunél
o	o	k7c6	o
celkové	celkový	k2eAgFnSc6d1	celková
délce	délka	k1gFnSc6	délka
3800	[number]	k4	3800
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Nejzajímavější	zajímavý	k2eAgFnSc7d3	nejzajímavější
stavbou	stavba	k1gFnSc7	stavba
je	být	k5eAaImIp3nS	být
smyčka	smyčka	k1gFnSc1	smyčka
u	u	k7c2	u
Telgártu	Telgárt	k1gInSc2	Telgárt
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
2,3	[number]	k4	2,3
km	km	kA	km
<g/>
,	,	kIx,	,
pomocí	pomocí	k7c2	pomocí
níž	jenž	k3xRgFnSc2	jenž
trať	trať	k1gFnSc1	trať
překonává	překonávat	k5eAaImIp3nS	překonávat
výškový	výškový	k2eAgInSc4d1	výškový
rozdíl	rozdíl	k1gInSc4	rozdíl
31	[number]	k4	31
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
smyčky	smyčka	k1gFnSc2	smyčka
je	být	k5eAaImIp3nS	být
nejdelší	dlouhý	k2eAgInSc1d3	nejdelší
tunel	tunel	k1gInSc1	tunel
na	na	k7c6	na
trati	trať	k1gFnSc6	trať
(	(	kIx(	(
<g/>
1239	[number]	k4	1239
m	m	kA	m
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
<g/>
)	)	kIx)	)
a	a	k8xC	a
kamenný	kamenný	k2eAgInSc1d1	kamenný
viadukt	viadukt	k1gInSc1	viadukt
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
86	[number]	k4	86
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
výšce	výška	k1gFnSc6	výška
22	[number]	k4	22
metrů	metr	k1gInPc2	metr
nad	nad	k7c7	nad
údolím	údolí	k1gNnSc7	údolí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Besnickém	Besnický	k2eAgInSc6d1	Besnický
tunelu	tunel	k1gInSc6	tunel
pod	pod	k7c7	pod
Besnickým	Besnický	k2eAgNnSc7d1	Besnický
sedlem	sedlo	k1gNnSc7	sedlo
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
trať	trať	k1gFnSc1	trať
překonává	překonávat	k5eAaImIp3nS	překonávat
rozvodí	rozvodí	k1gNnSc4	rozvodí
mezi	mezi	k7c7	mezi
Hnilcem	Hnilec	k1gMnSc7	Hnilec
a	a	k8xC	a
Hronem	Hron	k1gMnSc7	Hron
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
955,5	[number]	k4	955,5
m	m	kA	m
nachází	nacházet	k5eAaImIp3nS	nacházet
nejvýše	nejvýše	k6eAd1	nejvýše
položený	položený	k2eAgInSc4d1	položený
bod	bod	k1gInSc4	bod
na	na	k7c6	na
normálněrozchodných	normálněrozchodný	k2eAgFnPc6d1	normálněrozchodná
tratích	trať	k1gFnPc6	trať
Slovenska	Slovensko	k1gNnSc2	Slovensko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
byl	být	k5eAaImAgMnS	být
o	o	k7c6	o
stavbě	stavba	k1gFnSc6	stavba
trati	trať	k1gFnSc2	trať
natočen	natočen	k2eAgInSc4d1	natočen
propagandisticky	propagandisticky	k6eAd1	propagandisticky
laděný	laděný	k2eAgInSc4d1	laděný
film	film	k1gInSc4	film
Boj	boj	k1gInSc1	boj
sa	sa	k?	sa
skončí	skončit	k5eAaPmIp3nS	skončit
zajtra	zajtra	k?	zajtra
<g/>
.	.	kIx.	.
</s>
