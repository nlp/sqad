<s>
Vír	Vír	k1gInSc1	Vír
I	i	k9	i
je	být	k5eAaImIp3nS	být
přehradní	přehradní	k2eAgFnSc1d1	přehradní
nádrž	nádrž	k1gFnSc1	nádrž
s	s	k7c7	s
hrází	hráz	k1gFnSc7	hráz
o	o	k7c6	o
výšce	výška	k1gFnSc6	výška
76,5	[number]	k4	76,5
m	m	kA	m
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Svratce	Svratka	k1gFnSc6	Svratka
nad	nad	k7c7	nad
obcí	obec	k1gFnSc7	obec
Vír	Vír	k1gInSc4	Vír
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vybudována	vybudovat	k5eAaPmNgFnS	vybudovat
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1947	[number]	k4	1947
až	až	k9	až
1957	[number]	k4	1957
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
stavba	stavba	k1gFnSc1	stavba
způsobila	způsobit	k5eAaPmAgFnS	způsobit
zánik	zánik	k1gInSc4	zánik
obce	obec	k1gFnSc2	obec
Chudobín	Chudobín	k1gInSc1	Chudobín
u	u	k7c2	u
Dalečína	Dalečín	k1gInSc2	Dalečín
a	a	k8xC	a
zánik	zánik	k1gInSc1	zánik
části	část	k1gFnSc2	část
obce	obec	k1gFnSc2	obec
Korouhvice	korouhvice	k1gFnSc2	korouhvice
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
vodní	vodní	k2eAgFnSc2d1	vodní
nádrže	nádrž	k1gFnSc2	nádrž
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
do	do	k7c2	do
plného	plný	k2eAgInSc2d1	plný
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Vystavěním	vystavění	k1gNnSc7	vystavění
71	[number]	k4	71
metrů	metr	k1gInPc2	metr
vysoké	vysoký	k2eAgFnSc2d1	vysoká
hráze	hráz	k1gFnSc2	hráz
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
jezero	jezero	k1gNnSc1	jezero
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
maximální	maximální	k2eAgFnSc1d1	maximální
rozloha	rozloha	k1gFnSc1	rozloha
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
223,5	[number]	k4	223,5
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
přehradě	přehrada	k1gFnSc3	přehrada
také	také	k9	také
náleží	náležet	k5eAaImIp3nS	náležet
hydroelektrárna	hydroelektrárna	k1gFnSc1	hydroelektrárna
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
turbínami	turbína	k1gFnPc7	turbína
a	a	k8xC	a
stanice	stanice	k1gFnSc2	stanice
pro	pro	k7c4	pro
úpravu	úprava	k1gFnSc4	úprava
na	na	k7c4	na
pitnou	pitný	k2eAgFnSc4d1	pitná
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
výroba	výroba	k1gFnSc1	výroba
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnSc2	voda
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
50	[number]	k4	50
-	-	kIx~	-
60	[number]	k4	60
litry	litr	k1gInPc4	litr
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
,	,	kIx,	,
maximálně	maximálně	k6eAd1	maximálně
až	až	k9	až
200	[number]	k4	200
litrů	litr	k1gInPc2	litr
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
normálním	normální	k2eAgInSc6d1	normální
denním	denní	k2eAgInSc6d1	denní
provozu	provoz	k1gInSc6	provoz
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
možno	možno	k6eAd1	možno
dodat	dodat	k5eAaPmF	dodat
přes	přes	k7c4	přes
5000	[number]	k4	5000
metrů	metr	k1gInPc2	metr
krychlových	krychlový	k2eAgInPc2d1	krychlový
pitné	pitný	k2eAgFnSc2d1	pitná
vody	voda	k1gFnPc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
této	tento	k3xDgFnSc3	tento
stavbě	stavba	k1gFnSc3	stavba
je	být	k5eAaImIp3nS	být
pitnou	pitný	k2eAgFnSc7d1	pitná
vodou	voda	k1gFnSc7	voda
zásobováno	zásobován	k2eAgNnSc1d1	zásobováno
Novoměstsko	Novoměstsko	k1gNnSc1	Novoměstsko
<g/>
,	,	kIx,	,
Bystřicko	Bystřicko	k1gNnSc1	Bystřicko
a	a	k8xC	a
část	část	k1gFnSc1	část
Žďárska	Žďárska	k1gFnSc1	Žďárska
<g/>
,	,	kIx,	,
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
také	také	k9	také
Brno	Brno	k1gNnSc1	Brno
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc2	jeho
okolí	okolí	k1gNnSc2	okolí
(	(	kIx(	(
<g/>
Vírský	vírský	k2eAgInSc1d1	vírský
oblastní	oblastní	k2eAgInSc1d1	oblastní
vodovod	vodovod	k1gInSc1	vodovod
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
je	být	k5eAaImIp3nS	být
lokalita	lokalita	k1gFnSc1	lokalita
nepřístupná	přístupný	k2eNgFnSc1d1	nepřístupná
k	k	k7c3	k
rekreaci	rekreace	k1gFnSc3	rekreace
a	a	k8xC	a
rybolovu	rybolov	k1gInSc3	rybolov
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
pro	pro	k7c4	pro
její	její	k3xOp3gNnSc4	její
okolí	okolí	k1gNnSc4	okolí
platí	platit	k5eAaImIp3nS	platit
vyhláška	vyhláška	k1gFnSc1	vyhláška
I.	I.	kA	I.
pásma	pásmo	k1gNnSc2	pásmo
hygienické	hygienický	k2eAgFnSc2d1	hygienická
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
Povodí	povodí	k1gNnSc1	povodí
Moravy	Morava	k1gFnSc2	Morava
udělalo	udělat	k5eAaPmAgNnS	udělat
menší	malý	k2eAgInSc4d2	menší
ústupek	ústupek	k1gInSc4	ústupek
a	a	k8xC	a
na	na	k7c6	na
levém	levý	k2eAgInSc6d1	levý
břehu	břeh	k1gInSc6	břeh
byla	být	k5eAaImAgFnS	být
zbudována	zbudován	k2eAgFnSc1d1	zbudována
asfaltová	asfaltový	k2eAgFnSc1d1	asfaltová
cesta	cesta	k1gFnSc1	cesta
sloužící	sloužící	k1gFnSc1	sloužící
jako	jako	k8xS	jako
cyklostezka	cyklostezka	k1gFnSc1	cyklostezka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vede	vést	k5eAaImIp3nS	vést
do	do	k7c2	do
Dalečína	Dalečín	k1gInSc2	Dalečín
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
vodohospodářská	vodohospodářský	k2eAgFnSc1d1	vodohospodářská
naučná	naučný	k2eAgFnSc1d1	naučná
stezka	stezka	k1gFnSc1	stezka
<g/>
.	.	kIx.	.
</s>
<s>
Vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
také	také	k9	také
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
zachycení	zachycení	k1gNnSc3	zachycení
povodní	povodeň	k1gFnPc2	povodeň
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
Vír	Vír	k1gInSc1	Vír
I	i	k9	i
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Vír	Vír	k1gInSc4	Vír
I.	I.	kA	I.
na	na	k7c6	na
portálu	portál	k1gInSc6	portál
Povodí	povodí	k1gNnSc2	povodí
Moravy	Morava	k1gFnSc2	Morava
<g/>
,	,	kIx,	,
s.	s.	k?	s.
p.	p.	k?	p.
Neoficiální	oficiální	k2eNgInSc1d1	neoficiální
informační	informační	k2eAgInSc1d1	informační
web	web	k1gInSc1	web
o	o	k7c6	o
vodním	vodní	k2eAgNnSc6d1	vodní
díle	dílo	k1gNnSc6	dílo
Stavba	stavba	k1gFnSc1	stavba
přehrady	přehrada	k1gFnSc2	přehrada
Vír	Vír	k1gInSc1	Vír
I.	I.	kA	I.
na	na	k7c6	na
fotografiích	fotografia	k1gFnPc6	fotografia
Zatopené	zatopený	k2eAgInPc4d1	zatopený
osudy	osud	k1gInPc4	osud
-	-	kIx~	-
Vír	Vír	k1gInSc4	Vír
<g/>
,	,	kIx,	,
dokument	dokument	k1gInSc4	dokument
ČT	ČT	kA	ČT
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc1	možnost
online	onlinout	k5eAaPmIp3nS	onlinout
přehrání	přehrání	k1gNnSc4	přehrání
Dobový	dobový	k2eAgInSc1d1	dobový
televizní	televizní	k2eAgInSc1d1	televizní
dokument	dokument	k1gInSc1	dokument
o	o	k7c6	o
stavbě	stavba	k1gFnSc6	stavba
přehrady	přehrada	k1gFnSc2	přehrada
Vír	Vír	k1gInSc1	Vír
I	I	kA	I
<g/>
,	,	kIx,	,
možnost	možnost	k1gFnSc1	možnost
online	onlinout	k5eAaPmIp3nS	onlinout
přehrání	přehrání	k1gNnSc4	přehrání
</s>
