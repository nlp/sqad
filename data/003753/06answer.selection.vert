<s>
FreeBSD	FreeBSD	k?	FreeBSD
je	být	k5eAaImIp3nS	být
svobodný	svobodný	k2eAgInSc1d1	svobodný
unixový	unixový	k2eAgInSc1d1	unixový
operační	operační	k2eAgInSc1d1	operační
systém	systém	k1gInSc1	systém
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
z	z	k7c2	z
BSD	BSD	kA	BSD
verze	verze	k1gFnSc1	verze
Unixu	Unix	k1gInSc2	Unix
vyvinutého	vyvinutý	k2eAgInSc2d1	vyvinutý
na	na	k7c6	na
Kalifornské	kalifornský	k2eAgFnSc6d1	kalifornská
Univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c4	v
Berkeley	Berkele	k1gMnPc4	Berkele
<g/>
.	.	kIx.	.
</s>
