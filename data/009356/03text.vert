<p>
<s>
Vratička	vratička	k1gFnSc1	vratička
měsíční	měsíční	k2eAgFnSc1d1	měsíční
(	(	kIx(	(
<g/>
Botrychium	Botrychium	k1gNnSc1	Botrychium
lunaria	lunarium	k1gNnSc2	lunarium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
malá	malý	k2eAgFnSc1d1	malá
nenápadná	nápadný	k2eNgFnSc1d1	nenápadná
kapradina	kapradina	k1gFnSc1	kapradina
s	s	k7c7	s
jediným	jediný	k2eAgInSc7d1	jediný
listem	list	k1gInSc7	list
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
čeledi	čeleď	k1gFnSc2	čeleď
hadilkovité	hadilkovitý	k2eAgFnSc2d1	hadilkovitý
(	(	kIx(	(
<g/>
Ophioglossaceae	Ophioglossacea	k1gFnSc2	Ophioglossacea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
se	se	k3xPyFc4	se
na	na	k7c6	na
suchých	suchý	k2eAgFnPc6d1	suchá
pastvinách	pastvina	k1gFnPc6	pastvina
<g/>
,	,	kIx,	,
travnatých	travnatý	k2eAgFnPc6d1	travnatá
stráních	stráň	k1gFnPc6	stráň
<g/>
,	,	kIx,	,
častěji	často	k6eAd2	často
na	na	k7c6	na
vápnitých	vápnitý	k2eAgFnPc6d1	vápnitá
půdách	půda	k1gFnPc6	půda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Vratička	vratička	k1gFnSc1	vratička
měsíční	měsíční	k2eAgFnSc1d1	měsíční
je	být	k5eAaImIp3nS	být
vytrvalá	vytrvalý	k2eAgFnSc1d1	vytrvalá
bylina	bylina	k1gFnSc1	bylina
s	s	k7c7	s
krátkým	krátký	k2eAgInSc7d1	krátký
podzemním	podzemní	k2eAgInSc7d1	podzemní
stonkem	stonek	k1gInSc7	stonek
<g/>
.	.	kIx.	.
</s>
<s>
Nese	nést	k5eAaImIp3nS	nést
dlouze	dlouho	k6eAd1	dlouho
řapíkatý	řapíkatý	k2eAgInSc1d1	řapíkatý
list	list	k1gInSc1	list
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
rozdělený	rozdělený	k2eAgInSc1d1	rozdělený
na	na	k7c4	na
sterilní	sterilní	k2eAgFnSc4d1	sterilní
a	a	k8xC	a
fertilní	fertilní	k2eAgFnSc4d1	fertilní
část	část	k1gFnSc4	část
<g/>
.	.	kIx.	.
</s>
<s>
Sterilní	sterilní	k2eAgFnSc1d1	sterilní
část	část	k1gFnSc1	část
listu	list	k1gInSc2	list
(	(	kIx(	(
<g/>
připomíná	připomínat	k5eAaImIp3nS	připomínat
list	list	k1gInSc1	list
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
přisedlá	přisedlý	k2eAgFnSc1d1	přisedlá
<g/>
,	,	kIx,	,
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
úkrojky	úkrojek	k1gInPc1	úkrojek
ledvinité	ledvinitý	k2eAgInPc1d1	ledvinitý
až	až	k6eAd1	až
kosodélníkovité	kosodélníkovitý	k2eAgInPc1d1	kosodélníkovitý
<g/>
,	,	kIx,	,
celokrajné	celokrajný	k2eAgInPc1d1	celokrajný
až	až	k6eAd1	až
laločnaté	laločnatý	k2eAgInPc1d1	laločnatý
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
střední	střední	k2eAgFnSc2d1	střední
žilky	žilka	k1gFnSc2	žilka
<g/>
.	.	kIx.	.
</s>
<s>
Fertilní	fertilní	k2eAgFnSc1d1	fertilní
část	část	k1gFnSc1	část
(	(	kIx(	(
<g/>
připomíná	připomínat	k5eAaImIp3nS	připomínat
lodyhu	lodyha	k1gFnSc4	lodyha
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
zpeřeně	zpeřeně	k6eAd1	zpeřeně
členěná	členěný	k2eAgFnSc1d1	členěná
<g/>
.	.	kIx.	.
</s>
<s>
List	list	k1gInSc1	list
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
jeden	jeden	k4xCgMnSc1	jeden
<g/>
,	,	kIx,	,
vzácně	vzácně	k6eAd1	vzácně
až	až	k9	až
čtyři	čtyři	k4xCgMnPc4	čtyři
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnPc1	rozšíření
a	a	k8xC	a
stanoviště	stanoviště	k1gNnPc1	stanoviště
==	==	k?	==
</s>
</p>
<p>
<s>
Druh	druh	k1gInSc1	druh
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
všech	všecek	k3xTgInPc6	všecek
kontinentech	kontinent	k1gInPc6	kontinent
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Antarktidy	Antarktida	k1gFnSc2	Antarktida
od	od	k7c2	od
mírného	mírný	k2eAgInSc2d1	mírný
pásu	pás	k1gInSc2	pás
až	až	k9	až
po	po	k7c4	po
polární	polární	k2eAgInSc4d1	polární
kruh	kruh	k1gInSc4	kruh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
bez	bez	k7c2	bez
výrazných	výrazný	k2eAgFnPc2d1	výrazná
preferencí	preference	k1gFnPc2	preference
<g/>
,	,	kIx,	,
roztroušeně	roztroušeně	k6eAd1	roztroušeně
po	po	k7c6	po
celém	celý	k2eAgNnSc6d1	celé
území	území	k1gNnSc6	území
od	od	k7c2	od
planárního	planární	k2eAgInSc2d1	planární
po	po	k7c4	po
subalpínský	subalpínský	k2eAgInSc4d1	subalpínský
stupeň	stupeň	k1gInSc4	stupeň
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
hojně	hojně	k6eAd1	hojně
např.	např.	kA	např.
v	v	k7c6	v
Jeseníkách	Jeseníky	k1gInPc6	Jeseníky
a	a	k8xC	a
naopak	naopak	k6eAd1	naopak
v	v	k7c6	v
Beskydech	Beskyd	k1gInPc6	Beskyd
extrémně	extrémně	k6eAd1	extrémně
vzácně	vzácně	k6eAd1	vzácně
<g/>
.	.	kIx.	.
</s>
<s>
Roste	růst	k5eAaImIp3nS	růst
na	na	k7c6	na
suchých	suchý	k2eAgFnPc6d1	suchá
až	až	k8xS	až
vlhkých	vlhký	k2eAgFnPc6d1	vlhká
loukách	louka	k1gFnPc6	louka
<g/>
,	,	kIx,	,
travnatých	travnatý	k2eAgFnPc6d1	travnatá
stráních	stráň	k1gFnPc6	stráň
<g/>
,	,	kIx,	,
častěji	často	k6eAd2	často
na	na	k7c6	na
vápnitých	vápnitý	k2eAgFnPc6d1	vápnitá
půdách	půda	k1gFnPc6	půda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ohrožení	ohrožení	k1gNnSc1	ohrožení
a	a	k8xC	a
ochrana	ochrana	k1gFnSc1	ochrana
==	==	k?	==
</s>
</p>
<p>
<s>
I	i	k9	i
přes	přes	k7c4	přes
značné	značný	k2eAgNnSc4d1	značné
geografické	geografický	k2eAgNnSc4d1	geografické
rozšíření	rozšíření	k1gNnSc4	rozšíření
je	být	k5eAaImIp3nS	být
vratička	vratička	k1gFnSc1	vratička
měsíční	měsíční	k2eAgFnSc1d1	měsíční
hodnocena	hodnocen	k2eAgFnSc1d1	hodnocena
jako	jako	k8xC	jako
silně	silně	k6eAd1	silně
ohrožený	ohrožený	k2eAgInSc4d1	ohrožený
druh	druh	k1gInSc4	druh
české	český	k2eAgFnSc2d1	Česká
květeny	květena	k1gFnSc2	květena
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
chráněna	chránit	k5eAaImNgFnS	chránit
zákonem	zákon	k1gInSc7	zákon
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
ohrožených	ohrožený	k2eAgInPc2d1	ohrožený
druhů	druh	k1gInPc2	druh
.	.	kIx.	.
</s>
<s>
Zákonné	zákonný	k2eAgFnSc3d1	zákonná
ochraně	ochrana	k1gFnSc3	ochrana
podléhá	podléhat	k5eAaImIp3nS	podléhat
i	i	k9	i
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Polsku	Polsko	k1gNnSc6	Polsko
a	a	k8xC	a
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Botrychium	Botrychium	k1gNnSc1	Botrychium
lunaria	lunarium	k1gNnSc2	lunarium
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
KUBÁT	Kubát	k1gMnSc1	Kubát
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
<g/>
.	.	kIx.	.
</s>
<s>
Klíč	klíč	k1gInSc1	klíč
ke	k	k7c3	k
květeně	květena	k1gFnSc3	květena
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
927	[number]	k4	927
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
836	[number]	k4	836
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
vratička	vratička	k1gFnSc1	vratička
měsíční	měsíční	k2eAgFnSc1d1	měsíční
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc4	taxon
Botrychium	Botrychium	k1gNnSc4	Botrychium
lunaria	lunarium	k1gNnSc2	lunarium
ve	v	k7c6	v
Wikidruzích	Wikidruh	k1gInPc6	Wikidruh
</s>
</p>
<p>
<s>
Botany	Botana	k1gFnPc1	Botana
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Prirodakarlovarska	Prirodakarlovarska	k1gFnSc1	Prirodakarlovarska
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Biolib	Biolib	k1gInSc1	Biolib
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
Naturabohemica	Naturabohemica	k1gFnSc1	Naturabohemica
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
