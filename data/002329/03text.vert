<s>
Bakteriémie	bakteriémie	k1gFnSc1	bakteriémie
(	(	kIx(	(
<g/>
též	též	k9	též
bakteriemie	bakteriemie	k1gFnSc1	bakteriemie
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
přítomnost	přítomnost	k1gFnSc1	přítomnost
bakterií	bakterie	k1gFnPc2	bakterie
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
<g/>
.	.	kIx.	.
</s>
<s>
Krev	krev	k1gFnSc1	krev
je	být	k5eAaImIp3nS	být
normálně	normálně	k6eAd1	normálně
sterilním	sterilní	k2eAgNnSc7d1	sterilní
prostředím	prostředí	k1gNnSc7	prostředí
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
zjištění	zjištění	k1gNnSc1	zjištění
bakterií	bakterie	k1gFnPc2	bakterie
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
její	její	k3xOp3gFnSc7	její
kultivací	kultivace	k1gFnSc7	kultivace
<g/>
)	)	kIx)	)
vždy	vždy	k6eAd1	vždy
abnormální	abnormální	k2eAgFnSc7d1	abnormální
situací	situace	k1gFnSc7	situace
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnPc1	bakterie
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
do	do	k7c2	do
krevního	krevní	k2eAgNnSc2d1	krevní
řečiště	řečiště	k1gNnSc2	řečiště
dostat	dostat	k5eAaPmF	dostat
jako	jako	k8xC	jako
vážná	vážný	k2eAgFnSc1d1	vážná
komplikace	komplikace	k1gFnSc1	komplikace
infekcí	infekce	k1gFnPc2	infekce
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pneumonie	pneumonie	k1gFnSc2	pneumonie
nebo	nebo	k8xC	nebo
meningitidy	meningitida	k1gFnSc2	meningitida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
chirurgických	chirurgický	k2eAgInPc6d1	chirurgický
výkonech	výkon	k1gInPc6	výkon
(	(	kIx(	(
<g/>
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
sliznicích	sliznice	k1gFnPc6	sliznice
<g/>
,	,	kIx,	,
např.	např.	kA	např.
v	v	k7c6	v
trávicím	trávicí	k2eAgInSc6d1	trávicí
traktu	trakt	k1gInSc6	trakt
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prostřednictvím	prostřednictví	k1gNnSc7	prostřednictví
katétrů	katétr	k1gInPc2	katétr
nebo	nebo	k8xC	nebo
jiných	jiný	k2eAgNnPc2d1	jiné
cizích	cizí	k2eAgNnPc2d1	cizí
těles	těleso	k1gNnPc2	těleso
vsouvaných	vsouvaný	k2eAgNnPc2d1	vsouvaný
do	do	k7c2	do
tepen	tepna	k1gFnPc2	tepna
a	a	k8xC	a
žil	žíla	k1gFnPc2	žíla
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
nitrožilní	nitrožilní	k2eAgFnSc2d1	nitrožilní
aplikace	aplikace	k1gFnSc2	aplikace
drog	droga	k1gFnPc2	droga
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bakteriémie	bakteriémie	k1gFnSc1	bakteriémie
má	mít	k5eAaImIp3nS	mít
několik	několik	k4yIc4	několik
důsledků	důsledek	k1gInPc2	důsledek
<g/>
.	.	kIx.	.
</s>
<s>
Imunitní	imunitní	k2eAgFnSc1d1	imunitní
odpověď	odpověď	k1gFnSc1	odpověď
na	na	k7c4	na
bakterie	bakterie	k1gFnPc4	bakterie
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
sepsi	sepse	k1gFnSc4	sepse
nebo	nebo	k8xC	nebo
septický	septický	k2eAgInSc4d1	septický
šok	šok	k1gInSc4	šok
s	s	k7c7	s
poměrně	poměrně	k6eAd1	poměrně
vysokou	vysoký	k2eAgFnSc7d1	vysoká
mortalitou	mortalita	k1gFnSc7	mortalita
<g/>
.	.	kIx.	.
</s>
<s>
Bakterie	bakterie	k1gFnSc1	bakterie
se	se	k3xPyFc4	se
také	také	k9	také
mohou	moct	k5eAaImIp3nP	moct
krví	krev	k1gFnSc7	krev
rozšířit	rozšířit	k5eAaPmF	rozšířit
do	do	k7c2	do
jiných	jiný	k2eAgFnPc2d1	jiná
částí	část	k1gFnPc2	část
těla	tělo	k1gNnSc2	tělo
(	(	kIx(	(
<g/>
hematogenní	hematogenní	k2eAgNnSc1d1	hematogenní
šíření	šíření	k1gNnSc1	šíření
<g/>
)	)	kIx)	)
a	a	k8xC	a
způsobovat	způsobovat	k5eAaImF	způsobovat
infekce	infekce	k1gFnPc4	infekce
mimo	mimo	k7c4	mimo
původní	původní	k2eAgFnSc4d1	původní
lokalizaci	lokalizace	k1gFnSc4	lokalizace
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
endokarditida	endokarditida	k1gFnSc1	endokarditida
nebo	nebo	k8xC	nebo
osteomyelitida	osteomyelitida	k1gFnSc1	osteomyelitida
<g/>
.	.	kIx.	.
</s>
<s>
Léčí	léčit	k5eAaImIp3nP	léčit
se	se	k3xPyFc4	se
antibiotiky	antibiotikum	k1gNnPc7	antibiotikum
<g/>
;	;	kIx,	;
v	v	k7c6	v
situacích	situace	k1gFnPc6	situace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
lze	lze	k6eAd1	lze
očekávat	očekávat	k5eAaImF	očekávat
takové	takový	k3xDgInPc4	takový
problémy	problém	k1gInPc4	problém
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
preventivní	preventivní	k2eAgFnSc4d1	preventivní
antibiotickou	antibiotický	k2eAgFnSc4d1	antibiotická
profylaxi	profylaxe	k1gFnSc4	profylaxe
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Bacteremia	Bacteremium	k1gNnSc2	Bacteremium
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Virémie	Virémie	k1gFnSc1	Virémie
Fungémie	Fungémie	k1gFnSc2	Fungémie
Sepse	sepse	k1gFnSc2	sepse
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Bakteriémie	bakteriémie	k1gFnSc2	bakteriémie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Bacteremia	Bacteremia	k1gFnSc1	Bacteremia
at	at	k?	at
the	the	k?	the
Merck	Merck	k1gInSc1	Merck
Manuals	Manuals	k1gInSc1	Manuals
Online	Onlin	k1gInSc5	Onlin
Medical	Medical	k1gFnPc4	Medical
Library	Librara	k1gFnPc1	Librara
</s>
