<s>
Rainbow	Rainbow	k?	Rainbow
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
duha	duha	k1gFnSc1	duha
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
britská	britský	k2eAgFnSc1d1	britská
hardrocková	hardrockový	k2eAgFnSc1d1	hardrocková
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
po	po	k7c6	po
odchodu	odchod	k1gInSc6	odchod
z	z	k7c2	z
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gMnSc2	Purple
založil	založit	k5eAaPmAgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
kytarista	kytarista	k1gMnSc1	kytarista
Ritchie	Ritchie	k1gFnPc1	Ritchie
Blackmore	Blackmor	k1gInSc5	Blackmor
<g/>
.	.	kIx.	.
</s>
<s>
Koncerty	koncert	k1gInPc1	koncert
Rainbow	Rainbow	k1gFnSc2	Rainbow
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
úrovní	úroveň	k1gFnSc7	úroveň
vyrovnaly	vyrovnat	k5eAaPmAgInP	vyrovnat
koncertům	koncert	k1gInPc3	koncert
Deep	Deep	k1gInSc1	Deep
Purple	Purple	k1gMnPc1	Purple
<g/>
.	.	kIx.	.
</s>
<s>
Možná	možná	k9	možná
víc	hodně	k6eAd2	hodně
rozvíjeli	rozvíjet	k5eAaImAgMnP	rozvíjet
improvizaci	improvizace	k1gFnSc4	improvizace
<g/>
,	,	kIx,	,
plynule	plynule	k6eAd1	plynule
přecházeli	přecházet	k5eAaImAgMnP	přecházet
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
skladby	skladba	k1gFnSc2	skladba
do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
a	a	k8xC	a
mezi	mezi	k7c4	mezi
skladby	skladba	k1gFnPc4	skladba
spontánně	spontánně	k6eAd1	spontánně
vkládali	vkládat	k5eAaImAgMnP	vkládat
melodie	melodie	k1gFnPc4	melodie
různého	různý	k2eAgInSc2d1	různý
druhu	druh	k1gInSc2	druh
například	například	k6eAd1	například
z	z	k7c2	z
klasické	klasický	k2eAgFnSc2d1	klasická
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1974	[number]	k4	1974
Ritchie	Ritchie	k1gFnSc1	Ritchie
Blackmore	Blackmor	k1gInSc5	Blackmor
odešel	odejít	k5eAaPmAgMnS	odejít
z	z	k7c2	z
Deep	Deep	k1gInSc4	Deep
Purple	Purple	k1gFnSc3	Purple
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
společně	společně	k6eAd1	společně
s	s	k7c7	s
členy	člen	k1gMnPc7	člen
Electric	Electric	k1gMnSc1	Electric
Elves	Elves	k1gMnSc1	Elves
založil	založit	k5eAaPmAgMnS	založit
skupinu	skupina	k1gFnSc4	skupina
Rainbow	Rainbow	k1gFnSc2	Rainbow
(	(	kIx(	(
<g/>
začátek	začátek	k1gInSc1	začátek
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
symbolem	symbol	k1gInSc7	symbol
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
napovídá	napovídat	k5eAaBmIp3nS	napovídat
název	název	k1gInSc4	název
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
duha	duha	k1gFnSc1	duha
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc7	který
se	se	k3xPyFc4	se
Ritchie	Ritchie	k1gFnSc1	Ritchie
inspiroval	inspirovat	k5eAaBmAgInS	inspirovat
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
z	z	k7c2	z
California	Californium	k1gNnSc2	Californium
Jamu	jam	k1gInSc2	jam
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
měl	mít	k5eAaImAgInS	mít
maketu	maketa	k1gFnSc4	maketa
velké	velký	k2eAgFnSc2d1	velká
dřevěné	dřevěný	k2eAgFnSc2d1	dřevěná
duhy	duha	k1gFnSc2	duha
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
se	s	k7c7	s
jménem	jméno	k1gNnSc7	jméno
Rainbow	Rainbow	k1gFnSc2	Rainbow
(	(	kIx(	(
<g/>
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
setkalo	setkat	k5eAaPmAgNnS	setkat
s	s	k7c7	s
úspěchem	úspěch	k1gInSc7	úspěch
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
především	především	k9	především
hit	hit	k1gInSc1	hit
Man	mana	k1gFnPc2	mana
on	on	k3xPp3gMnSc1	on
the	the	k?	the
Silver	Silver	k1gMnSc1	Silver
Mountain	Mountain	k1gMnSc1	Mountain
<g/>
.	.	kIx.	.
</s>
<s>
Ovšem	ovšem	k9	ovšem
tato	tento	k3xDgFnSc1	tento
první	první	k4xOgFnSc1	první
sestava	sestava	k1gFnSc1	sestava
nikdy	nikdy	k6eAd1	nikdy
živě	živě	k6eAd1	živě
nevystupovala	vystupovat	k5eNaImAgFnS	vystupovat
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
hudba	hudba	k1gFnSc1	hudba
Rainbow	Rainbow	k1gFnSc1	Rainbow
částečně	částečně	k6eAd1	částečně
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
klasickou	klasický	k2eAgFnSc7d1	klasická
hudbou	hudba	k1gFnSc7	hudba
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
mělo	mít	k5eAaImAgNnS	mít
za	za	k7c4	za
následek	následek	k1gInSc4	následek
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
skupina	skupina	k1gFnSc1	skupina
přibrala	přibrat	k5eAaPmAgFnS	přibrat
také	také	k9	také
violoncello	violoncello	k1gNnSc4	violoncello
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
hudba	hudba	k1gFnSc1	hudba
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
začala	začít	k5eAaPmAgFnS	začít
vypadat	vypadat	k5eAaPmF	vypadat
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
zapojovala	zapojovat	k5eAaImAgFnS	zapojovat
středověké	středověký	k2eAgInPc4d1	středověký
motivy	motiv	k1gInPc4	motiv
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
vydali	vydat	k5eAaPmAgMnP	vydat
Rainbow	Rainbow	k1gFnSc4	Rainbow
druhou	druhý	k4xOgFnSc4	druhý
desku	deska	k1gFnSc4	deska
Rising	Rising	k1gInSc1	Rising
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
obměněná	obměněný	k2eAgFnSc1d1	obměněná
sestava	sestava	k1gFnSc1	sestava
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1978	[number]	k4	1978
vydala	vydat	k5eAaPmAgFnS	vydat
třetí	třetí	k4xOgFnSc1	třetí
album	album	k1gNnSc4	album
Long	Long	k1gMnSc1	Long
live	liv	k1gInSc2	liv
Rock	rock	k1gInSc1	rock
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
Roll	Roll	k1gInSc4	Roll
se	s	k7c7	s
skladbami	skladba	k1gFnPc7	skladba
"	"	kIx"	"
<g/>
Gates	Gates	k1gInSc1	Gates
Babylon	Babylon	k1gInSc1	Babylon
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Kill	Kill	k1gMnSc1	Kill
King	King	k1gMnSc1	King
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Sensitive	sensitiv	k1gMnSc5	sensitiv
to	ten	k3xDgNnSc4	ten
Light	Light	k2eAgInSc4d1	Light
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
nahrání	nahrání	k1gNnSc6	nahrání
tohoto	tento	k3xDgNnSc2	tento
alba	album	k1gNnSc2	album
přišel	přijít	k5eAaPmAgMnS	přijít
známý	známý	k2eAgMnSc1d1	známý
australský	australský	k2eAgMnSc1d1	australský
baskytarista	baskytarista	k1gMnSc1	baskytarista
Bob	Bob	k1gMnSc1	Bob
Daisley	Daislea	k1gFnSc2	Daislea
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
celosvětovém	celosvětový	k2eAgNnSc6d1	celosvětové
turné	turné	k1gNnSc6	turné
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1977	[number]	k4	1977
<g/>
/	/	kIx~	/
<g/>
78	[number]	k4	78
se	se	k3xPyFc4	se
Ronnie	Ronnie	k1gFnSc1	Ronnie
James	James	k1gMnSc1	James
Dio	Dio	k1gMnSc1	Dio
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
skupinu	skupina	k1gFnSc4	skupina
opustit	opustit	k5eAaPmF	opustit
a	a	k8xC	a
odešel	odejít	k5eAaPmAgMnS	odejít
do	do	k7c2	do
Black	Blacka	k1gFnPc2	Blacka
Sabbath	Sabbatha	k1gFnPc2	Sabbatha
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
zrovna	zrovna	k6eAd1	zrovna
vyhodili	vyhodit	k5eAaPmAgMnP	vyhodit
Osbournea	Osbourneum	k1gNnSc2	Osbourneum
<g/>
.	.	kIx.	.
</s>
<s>
Potom	potom	k6eAd1	potom
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
Ronnie	Ronnie	k1gFnSc2	Ronnie
James	Jamesa	k1gFnPc2	Jamesa
Dio	Dio	k1gMnSc1	Dio
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
ze	z	k7c2	z
skupiny	skupina	k1gFnSc2	skupina
odejít	odejít	k5eAaPmF	odejít
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
Blackmore	Blackmor	k1gInSc5	Blackmor
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
zlanařit	zlanařit	k5eAaPmF	zlanařit
Iana	Ianus	k1gMnSc4	Ianus
Gillana	Gillan	k1gMnSc4	Gillan
<g/>
.	.	kIx.	.
</s>
<s>
Ian	Ian	k?	Ian
chtěl	chtít	k5eAaImAgMnS	chtít
naopak	naopak	k6eAd1	naopak
Blackmorea	Blackmorea	k1gMnSc1	Blackmorea
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
ož	ož	k?	ož
Ritchie	Ritchie	k1gFnSc2	Ritchie
odmítl	odmítnout	k5eAaPmAgInS	odmítnout
a	a	k8xC	a
přivedl	přivést	k5eAaPmAgInS	přivést
Grahama	Graham	k1gMnSc4	Graham
Bonneta	Bonnet	k1gMnSc4	Bonnet
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
první	první	k4xOgNnSc1	první
album	album	k1gNnSc1	album
nové	nový	k2eAgFnSc2d1	nová
sestavy	sestava	k1gFnSc2	sestava
mělo	mít	k5eAaImAgNnS	mít
velký	velký	k2eAgInSc4d1	velký
úspěch	úspěch	k1gInSc4	úspěch
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
písně	píseň	k1gFnPc1	píseň
"	"	kIx"	"
<g/>
All	All	k1gMnSc1	All
Night	Night	k1gMnSc1	Night
Long	Long	k1gMnSc1	Long
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
Since	Since	k1gFnSc1	Since
you	you	k?	you
Been	Been	k1gInSc1	Been
Gone	Gone	k1gInSc1	Gone
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
se	se	k3xPyFc4	se
Bonnet	Bonnet	k1gMnSc1	Bonnet
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
odejít	odejít	k5eAaPmF	odejít
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
přišel	přijít	k5eAaPmAgMnS	přijít
nový	nový	k2eAgMnSc1d1	nový
americký	americký	k2eAgMnSc1d1	americký
zpěvák	zpěvák	k1gMnSc1	zpěvák
Joe	Joe	k1gMnSc1	Joe
Lynn	Lynn	k1gMnSc1	Lynn
Turner	turner	k1gMnSc1	turner
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
sestavě	sestava	k1gFnSc6	sestava
vydala	vydat	k5eAaPmAgFnS	vydat
nové	nový	k2eAgNnSc4d1	nové
album	album	k1gNnSc4	album
Difficult	Difficult	k2eAgMnSc1d1	Difficult
to	ten	k3xDgNnSc4	ten
Cure	Cure	k1gNnSc4	Cure
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
povšimnutí	povšimnutí	k1gNnSc2	povšimnutí
stojí	stát	k5eAaImIp3nS	stát
verze	verze	k1gFnSc1	verze
Beethovenovy	Beethovenův	k2eAgFnPc4d1	Beethovenova
Deváté	devátý	k4xOgFnPc4	devátý
symfonie	symfonie	k1gFnPc4	symfonie
a	a	k8xC	a
nejúspěšnější	úspěšný	k2eAgInSc4d3	nejúspěšnější
singl	singl	k1gInSc4	singl
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
I	i	k9	i
Surrender	Surrender	k1gMnSc1	Surrender
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
skupina	skupina	k1gFnSc1	skupina
ukončila	ukončit	k5eAaPmAgFnS	ukončit
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
po	po	k7c6	po
osmileté	osmiletý	k2eAgFnSc6d1	osmiletá
přestávce	přestávka	k1gFnSc6	přestávka
znovuobnovila	znovuobnovit	k5eAaPmAgFnS	znovuobnovit
původní	původní	k2eAgFnSc1d1	původní
sestava	sestava	k1gFnSc1	sestava
Deep	Deep	k1gMnSc1	Deep
Purple	Purple	k1gMnSc1	Purple
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yRgFnSc2	který
se	se	k3xPyFc4	se
vrátili	vrátit	k5eAaPmAgMnP	vrátit
Blackmore	Blackmor	k1gInSc5	Blackmor
a	a	k8xC	a
Roger	Roger	k1gMnSc1	Roger
Glover	Glover	k1gMnSc1	Glover
<g/>
.	.	kIx.	.
14	[number]	k4	14
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1984	[number]	k4	1984
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
na	na	k7c6	na
japonském	japonský	k2eAgNnSc6d1	Japonské
turné	turné	k1gNnSc6	turné
<g/>
,	,	kIx,	,
za	za	k7c2	za
doprovodu	doprovod	k1gInSc2	doprovod
japonského	japonský	k2eAgInSc2d1	japonský
symfonického	symfonický	k2eAgInSc2d1	symfonický
orchestru	orchestr	k1gInSc2	orchestr
<g/>
,	,	kIx,	,
rozloučila	rozloučit	k5eAaPmAgFnS	rozloučit
s	s	k7c7	s
fanoušky	fanoušek	k1gMnPc7	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
odmlčela	odmlčet	k5eAaPmAgFnS	odmlčet
na	na	k7c4	na
devět	devět	k4xCc4	devět
roků	rok	k1gInPc2	rok
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterých	který	k3yRgMnPc2	který
vyšla	vyjít	k5eAaPmAgFnS	vyjít
jen	jen	k9	jen
remixovaná	remixovaný	k2eAgFnSc1d1	remixovaná
kompilace	kompilace	k1gFnSc1	kompilace
"	"	kIx"	"
<g/>
Finyl	Finyl	k1gInSc1	Finyl
Vinyl	vinyl	k1gInSc1	vinyl
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
a	a	k8xC	a
vzpomínkové	vzpomínkový	k2eAgNnSc4d1	vzpomínkové
živé	živý	k2eAgNnSc4d1	živé
album	album	k1gNnSc4	album
"	"	kIx"	"
<g/>
Live	Live	k1gInSc1	Live
in	in	k?	in
Germany	German	k1gMnPc7	German
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
roku	rok	k1gInSc2	rok
1993	[number]	k4	1993
Ritchie	Ritchie	k1gFnSc2	Ritchie
Blackmore	Blackmor	k1gInSc5	Blackmor
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
turné	turné	k1gNnSc2	turné
odešel	odejít	k5eAaPmAgInS	odejít
z	z	k7c2	z
Deep	Deep	k1gInSc4	Deep
Purple	Purple	k1gFnSc3	Purple
<g/>
,	,	kIx,	,
dal	dát	k5eAaPmAgMnS	dát
v	v	k7c6	v
nové	nový	k2eAgFnSc6d1	nová
sestavě	sestava	k1gFnSc6	sestava
Rainbow	Rainbow	k1gFnSc2	Rainbow
znova	znova	k6eAd1	znova
do	do	k7c2	do
kupy	kupa	k1gFnSc2	kupa
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
vydali	vydat	k5eAaPmAgMnP	vydat
album	album	k1gNnSc4	album
"	"	kIx"	"
<g/>
Stranger	Stranger	k1gMnSc1	Stranger
in	in	k?	in
Us	Us	k1gMnSc1	Us
All	All	k1gMnSc1	All
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
roce	rok	k1gInSc6	rok
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
přibyl	přibýt	k5eAaPmAgInS	přibýt
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
starý	starý	k2eAgInSc1d1	starý
záznam	záznam	k1gInSc1	záznam
z	z	k7c2	z
koncertu	koncert	k1gInSc2	koncert
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Live	Live	k1gInSc1	Live
in	in	k?	in
Europe	Europ	k1gMnSc5	Europ
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgNnSc6	který
hráli	hrát	k5eAaImAgMnP	hrát
v	v	k7c6	v
sestavě	sestava	k1gFnSc6	sestava
Ronnie	Ronnie	k1gFnSc2	Ronnie
James	James	k1gMnSc1	James
Dio	Dio	k1gMnSc1	Dio
-	-	kIx~	-
zpěv	zpěv	k1gInSc1	zpěv
<g/>
,	,	kIx,	,
Ritchie	Ritchie	k1gFnSc1	Ritchie
Blackmore	Blackmor	k1gInSc5	Blackmor
-	-	kIx~	-
kytara	kytara	k1gFnSc1	kytara
<g/>
,	,	kIx,	,
Tony	Tony	k1gMnSc1	Tony
Carey	Carea	k1gFnSc2	Carea
-	-	kIx~	-
klávesy	klávesa	k1gFnSc2	klávesa
<g/>
,	,	kIx,	,
Jimmy	Jimma	k1gFnSc2	Jimma
Bain	Baina	k1gFnPc2	Baina
-	-	kIx~	-
baskytara	baskytara	k1gFnSc1	baskytara
a	a	k8xC	a
Cozy	Coza	k1gFnPc1	Coza
Powell	Powell	k1gInSc1	Powell
-	-	kIx~	-
bicí	bicí	k2eAgFnSc1d1	bicí
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
začal	začít	k5eAaPmAgInS	začít
Ritchie	Ritchie	k1gFnPc1	Ritchie
Blackmore	Blackmor	k1gInSc5	Blackmor
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Blackmore	Blackmor	k1gInSc5	Blackmor
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Night	Night	k1gInSc1	Night
hrát	hrát	k5eAaImF	hrát
hlavně	hlavně	k9	hlavně
renesanční	renesanční	k2eAgFnSc4d1	renesanční
hudbu	hudba	k1gFnSc4	hudba
s	s	k7c7	s
akustickou	akustický	k2eAgFnSc7d1	akustická
kytarou	kytara	k1gFnSc7	kytara
<g/>
,	,	kIx,	,
flétnou	flétna	k1gFnSc7	flétna
a	a	k8xC	a
zpěvem	zpěv	k1gInSc7	zpěv
své	svůj	k3xOyFgFnSc2	svůj
manželky	manželka	k1gFnSc2	manželka
Candice	Candice	k1gFnSc2	Candice
Night	Nighta	k1gFnPc2	Nighta
<g/>
.	.	kIx.	.
</s>
<s>
LP	LP	kA	LP
<g/>
:	:	kIx,	:
Ritchie	Ritchie	k1gFnSc1	Ritchie
Blackmore	Blackmor	k1gInSc5	Blackmor
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Rainbow	Rainbow	k1gFnSc7	Rainbow
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
#	#	kIx~	#
<g/>
11	[number]	k4	11
UK	UK	kA	UK
<g/>
,	,	kIx,	,
#	#	kIx~	#
<g/>
30	[number]	k4	30
US	US	kA	US
Rising	Rising	k1gInSc1	Rising
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
#	#	kIx~	#
<g/>
12	[number]	k4	12
UK	UK	kA	UK
<g/>
,	,	kIx,	,
#	#	kIx~	#
<g/>
48	[number]	k4	48
US	US	kA	US
Long	Longa	k1gFnPc2	Longa
Live	Live	k1gFnSc1	Live
Rock	rock	k1gInSc1	rock
'	'	kIx"	'
<g/>
n	n	k0	n
<g/>
'	'	kIx"	'
Roll	Rollum	k1gNnPc2	Rollum
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
#	#	kIx~	#
<g/>
7	[number]	k4	7
UK	UK	kA	UK
<g/>
,	,	kIx,	,
#	#	kIx~	#
<g/>
89	[number]	k4	89
US	US	kA	US
Down	Downa	k1gFnPc2	Downa
to	ten	k3xDgNnSc4	ten
Earth	Earth	k1gMnSc1	Earth
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
#	#	kIx~	#
<g/>
6	[number]	k4	6
UK	UK	kA	UK
<g/>
,	,	kIx,	,
#	#	kIx~	#
<g/>
66	[number]	k4	66
US	US	kA	US
Difficult	Difficult	k2eAgMnSc1d1	Difficult
to	ten	k3xDgNnSc4	ten
Cure	Cure	k1gNnSc4	Cure
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
#	#	kIx~	#
<g/>
3	[number]	k4	3
UK	UK	kA	UK
<g/>
,	,	kIx,	,
#	#	kIx~	#
<g/>
50	[number]	k4	50
<g />
.	.	kIx.	.
</s>
<s>
US	US	kA	US
Straight	Straight	k2eAgInSc4d1	Straight
Between	Between	k1gInSc4	Between
the	the	k?	the
Eyes	Eyes	k1gInSc1	Eyes
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
#	#	kIx~	#
<g/>
5	[number]	k4	5
UK	UK	kA	UK
<g/>
,	,	kIx,	,
#	#	kIx~	#
<g/>
30	[number]	k4	30
US	US	kA	US
Bent	Bent	k2eAgMnSc1d1	Bent
Out	Out	k1gMnSc1	Out
of	of	k?	of
Shape	Shap	k1gInSc5	Shap
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
#	#	kIx~	#
<g/>
11	[number]	k4	11
UK	UK	kA	UK
<g/>
,	,	kIx,	,
#	#	kIx~	#
<g/>
34	[number]	k4	34
US	US	kA	US
Stranger	Stranger	k1gMnSc1	Stranger
in	in	k?	in
Us	Us	k1gMnSc1	Us
All	All	k1gMnSc1	All
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Live	Liv	k1gInSc2	Liv
<g/>
<g />
.	.	kIx.	.
</s>
<s>
:	:	kIx,	:
On	on	k3xPp3gInSc1	on
Stage	Stage	k1gInSc1	Stage
(	(	kIx(	(
<g/>
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
#	#	kIx~	#
<g/>
7	[number]	k4	7
UK	UK	kA	UK
<g/>
,	,	kIx,	,
#	#	kIx~	#
<g/>
65	[number]	k4	65
US	US	kA	US
Finyl	Finyl	k1gInSc1	Finyl
Vinyl	vinyl	k1gInSc1	vinyl
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
#	#	kIx~	#
<g/>
31	[number]	k4	31
UK	UK	kA	UK
<g/>
,	,	kIx,	,
#	#	kIx~	#
<g/>
87	[number]	k4	87
US	US	kA	US
Live	Live	k1gFnPc2	Live
in	in	k?	in
Germany	German	k1gMnPc7	German
(	(	kIx(	(
<g/>
1994	[number]	k4	1994
<g/>
)	)	kIx)	)
Live	Liv	k1gInSc2	Liv
in	in	k?	in
Europe	Europ	k1gInSc5	Europ
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
Live	Live	k1gFnSc3	Live
in	in	k?	in
Munich	Munich	k1gInSc1	Munich
1977	[number]	k4	1977
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Rainbow	Rainbow	k1gMnSc5	Rainbow
Live	Livus	k1gMnSc5	Livus
At	At	k1gMnSc5	At
Cologne	Cologn	k1gMnSc5	Cologn
SportHalle	SportHall	k1gMnSc5	SportHall
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Live	Live	k1gFnSc1	Live
In	In	k1gFnSc7	In
Germany	German	k1gInPc1	German
1976	[number]	k4	1976
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
th	th	k?	th
Anniversary	Anniversar	k1gInPc1	Anniversar
Edition	Edition	k1gInSc1	Edition
Box	box	k1gInSc4	box
<g/>
)	)	kIx)	)
-	-	kIx~	-
japonský	japonský	k2eAgInSc1d1	japonský
6CD	[number]	k4	6CD
boxset	boxseta	k1gFnPc2	boxseta
(	(	kIx(	(
<g/>
2006	[number]	k4	2006
<g/>
)	)	kIx)	)
Greatest	Greatest	k1gInSc1	Greatest
Hits	Hits	k1gInSc1	Hits
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
of	of	k?	of
<g />
.	.	kIx.	.
</s>
<s>
Rainbow	Rainbow	k?	Rainbow
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
#	#	kIx~	#
<g/>
14	[number]	k4	14
UK	UK	kA	UK
The	The	k1gMnSc1	The
Very	Vera	k1gFnSc2	Vera
Best	Best	k1gMnSc1	Best
of	of	k?	of
Rainbow	Rainbow	k1gMnSc1	Rainbow
(	(	kIx(	(
<g/>
1997	[number]	k4	1997
<g/>
)	)	kIx)	)
20	[number]	k4	20
<g/>
th	th	k?	th
Century	Centura	k1gFnSc2	Centura
Masters	Mastersa	k1gFnPc2	Mastersa
-	-	kIx~	-
The	The	k1gFnSc4	The
Millennium	millennium	k1gNnSc4	millennium
Collection	Collection	k1gInSc1	Collection
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Best	Best	k1gMnSc1	Best
of	of	k?	of
Rainbow	Rainbow	k1gMnSc1	Rainbow
(	(	kIx(	(
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
Pot	pot	k1gInSc1	pot
of	of	k?	of
Gold	Gold	k1gInSc1	Gold
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
Catch	Catch	k1gInSc1	Catch
the	the	k?	the
<g />
.	.	kIx.	.
</s>
<s>
Rainbow	Rainbow	k?	Rainbow
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Anthology	Antholog	k1gMnPc7	Antholog
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
Singly	singl	k1gInPc1	singl
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Man	Man	k1gMnSc1	Man
On	on	k3xPp3gMnSc1	on
The	The	k1gMnSc1	The
Silver	Silver	k1gMnSc1	Silver
Mountain	Mountain	k1gMnSc1	Mountain
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Catch	Catch	k1gMnSc1	Catch
The	The	k1gMnSc1	The
Rainbow	Rainbow	k1gMnSc1	Rainbow
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Starstruck	Starstruck	k1gInSc1	Starstruck
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Stargazer	Stargazer	k1gInSc1	Stargazer
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1976	[number]	k4	1976
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Kill	Kill	k1gMnSc1	Kill
The	The	k1gMnSc1	The
King	King	k1gMnSc1	King
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Long	Long	k1gInSc1	Long
Live	Live	k1gNnSc1	Live
Rock	rock	k1gInSc1	rock
'	'	kIx"	'
<g/>
N	N	kA	N
<g/>
'	'	kIx"	'
Roll	Roll	k1gMnSc1	Roll
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Gates	Gates	k1gMnSc1	Gates
Of	Of	k1gFnSc2	Of
Babylon	Babylon	k1gInSc1	Babylon
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Since	Since	k1gMnSc1	Since
You	You	k1gMnSc1	You
<g/>
'	'	kIx"	'
<g/>
ve	v	k7c4	v
Been	Been	k1gInSc4	Been
Gone	Gon	k1gFnSc2	Gon
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1979	[number]	k4	1979
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
All	All	k1gMnSc1	All
Night	Night	k1gMnSc1	Night
Long	Long	k1gMnSc1	Long
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
I	i	k9	i
Surrender	Surrender	k1gInSc1	Surrender
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Can	Can	k1gFnSc1	Can
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Happen	Happen	k2eAgInSc4d1	Happen
Here	Here	k1gInSc4	Here
<g />
.	.	kIx.	.
</s>
<s>
<g/>
"	"	kIx"	"
/	/	kIx~	/
"	"	kIx"	"
<g/>
Jealous	Jealous	k1gMnSc1	Jealous
Lover	Lover	k1gMnSc1	Lover
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1981	[number]	k4	1981
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Stone	ston	k1gInSc5	ston
Cold	Cold	k1gInSc4	Cold
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Power	Power	k1gInSc1	Power
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1982	[number]	k4	1982
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Can	Can	k1gFnSc1	Can
<g/>
'	'	kIx"	'
<g/>
t	t	k?	t
Let	let	k1gInSc1	let
You	You	k1gMnSc1	You
Go	Go	k1gMnSc1	Go
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Street	Street	k1gInSc1	Street
Of	Of	k1gFnSc2	Of
Dreams	Dreamsa	k1gFnPc2	Dreamsa
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Hunting	Hunting	k1gInSc1	Hunting
Humans	Humans	k1gInSc1	Humans
(	(	kIx(	(
<g/>
Insatiable	Insatiable	k1gFnSc1	Insatiable
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Ariel	Ariel	k1gInSc1	Ariel
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Rainbow	Rainbow	k1gFnSc2	Rainbow
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
The	The	k1gFnSc1	The
Rainbow	Rainbow	k1gFnSc1	Rainbow
Fanclan	Fanclan	k1gInSc4	Fanclan
Ritchie	Ritchie	k1gFnSc2	Ritchie
Blackmore	Blackmor	k1gInSc5	Blackmor
-	-	kIx~	-
oficiální	oficiální	k2eAgFnSc1d1	oficiální
stránka	stránka	k1gFnSc1	stránka
Ritchie	Ritchie	k1gFnSc2	Ritchie
Blackmore	Blackmor	k1gInSc5	Blackmor
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Revival	revival	k1gInSc1	revival
Band	banda	k1gFnPc2	banda
</s>
