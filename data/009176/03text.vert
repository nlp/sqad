<p>
<s>
Oratorium	oratorium	k1gNnSc1	oratorium
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
orare	orar	k1gMnSc5	orar
<g/>
,	,	kIx,	,
modlit	modlit	k5eAaImF	modlit
se	se	k3xPyFc4	se
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
hudební	hudební	k2eAgFnSc1d1	hudební
skladba	skladba	k1gFnSc1	skladba
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
na	na	k7c4	na
duchovní	duchovní	k2eAgNnSc4d1	duchovní
(	(	kIx(	(
<g/>
náboženské	náboženský	k2eAgNnSc4d1	náboženské
<g/>
)	)	kIx)	)
téma	téma	k1gNnSc4	téma
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
sóla	sólo	k1gNnPc4	sólo
<g/>
,	,	kIx,	,
sbor	sbor	k1gInSc1	sbor
a	a	k8xC	a
orchestr	orchestr	k1gInSc1	orchestr
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
prováděna	provádět	k5eAaImNgFnS	provádět
koncertně	koncertně	k6eAd1	koncertně
(	(	kIx(	(
<g/>
nescénicky	scénicky	k6eNd1	scénicky
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Forma	forma	k1gFnSc1	forma
==	==	k?	==
</s>
</p>
<p>
<s>
Oratorium	oratorium	k1gNnSc1	oratorium
se	se	k3xPyFc4	se
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
z	z	k7c2	z
dialogů	dialog	k1gInPc2	dialog
na	na	k7c4	na
náboženská	náboženský	k2eAgNnPc4d1	náboženské
témata	téma	k1gNnPc4	téma
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
byly	být	k5eAaImAgInP	být
zpívány	zpívat	k5eAaImNgInP	zpívat
v	v	k7c6	v
chrámech	chrám	k1gInPc6	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Dějovou	dějový	k2eAgFnSc7d1	dějová
linií	linie	k1gFnSc7	linie
oratoria	oratorium	k1gNnSc2	oratorium
provází	provázet	k5eAaImIp3nS	provázet
vypravěč	vypravěč	k1gMnSc1	vypravěč
formou	forma	k1gFnSc7	forma
recitativu	recitativ	k1gInSc2	recitativ
s	s	k7c7	s
generálním	generální	k2eAgInSc7d1	generální
basem	bas	k1gInSc7	bas
a	a	k8xC	a
odděluje	oddělovat	k5eAaImIp3nS	oddělovat
tak	tak	k6eAd1	tak
jednotlivá	jednotlivý	k2eAgNnPc4d1	jednotlivé
hudební	hudební	k2eAgNnPc4d1	hudební
čísla	číslo	k1gNnPc4	číslo
<g/>
.	.	kIx.	.
</s>
<s>
Vypravěčem	vypravěč	k1gMnSc7	vypravěč
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
tenor	tenor	k1gInSc1	tenor
<g/>
.	.	kIx.	.
</s>
<s>
Text	text	k1gInSc1	text
přednášený	přednášený	k2eAgInSc1d1	přednášený
vypravěčem	vypravěč	k1gMnSc7	vypravěč
bývá	bývat	k5eAaImIp3nS	bývat
zpravidla	zpravidla	k6eAd1	zpravidla
přesnou	přesný	k2eAgFnSc7d1	přesná
citací	citace	k1gFnSc7	citace
Bible	bible	k1gFnSc2	bible
či	či	k8xC	či
životopisů	životopis	k1gInPc2	životopis
svatých	svatá	k1gFnPc2	svatá
<g/>
.	.	kIx.	.
</s>
<s>
Texty	text	k1gInPc1	text
svěřené	svěřený	k2eAgInPc1d1	svěřený
sólistům	sólista	k1gMnPc3	sólista
a	a	k8xC	a
sboru	sbor	k1gInSc3	sbor
jsou	být	k5eAaImIp3nP	být
dílem	dílem	k6eAd1	dílem
autora	autor	k1gMnSc4	autor
libreta	libreto	k1gNnSc2	libreto
<g/>
.	.	kIx.	.
</s>
<s>
Častá	častý	k2eAgFnSc1d1	častá
je	být	k5eAaImIp3nS	být
čistě	čistě	k6eAd1	čistě
instrumentální	instrumentální	k2eAgFnSc1d1	instrumentální
předehra	předehra	k1gFnSc1	předehra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Nejstarší	starý	k2eAgNnSc1d3	nejstarší
dochované	dochovaný	k2eAgNnSc1d1	dochované
oratorium	oratorium	k1gNnSc1	oratorium
je	být	k5eAaImIp3nS	být
Rappresentatione	Rappresentation	k1gInSc5	Rappresentation
di	di	k?	di
Anima	animo	k1gNnSc2	animo
et	et	k?	et
di	di	k?	di
Corpo	Corpa	k1gFnSc5	Corpa
(	(	kIx(	(
<g/>
Představení	představení	k1gNnSc4	představení
duše	duše	k1gFnSc2	duše
a	a	k8xC	a
těla	tělo	k1gNnSc2	tělo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
zkomponoval	zkomponovat	k5eAaPmAgMnS	zkomponovat
Emilio	Emilio	k1gMnSc1	Emilio
de	de	k?	de
<g/>
'	'	kIx"	'
Cavalieri	Cavalier	k1gFnSc2	Cavalier
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1600	[number]	k4	1600
<g/>
.	.	kIx.	.
</s>
<s>
Vznik	vznik	k1gInSc1	vznik
oratoria	oratorium	k1gNnSc2	oratorium
je	být	k5eAaImIp3nS	být
úzce	úzko	k6eAd1	úzko
svázán	svázat	k5eAaPmNgMnS	svázat
se	s	k7c7	s
vznikem	vznik	k1gInSc7	vznik
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
Vždyť	vždyť	k9	vždyť
nejstarším	starý	k2eAgInSc7d3	nejstarší
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
ztraceným	ztracený	k2eAgInSc7d1	ztracený
<g/>
,	,	kIx,	,
dílem	díl	k1gInSc7	díl
považovaným	považovaný	k2eAgInSc7d1	považovaný
za	za	k7c4	za
operu	opera	k1gFnSc4	opera
je	být	k5eAaImIp3nS	být
Dafne	Dafn	k1gInSc5	Dafn
skladatele	skladatel	k1gMnSc2	skladatel
Jacopa	Jacop	k1gMnSc4	Jacop
Periho	Peri	k1gMnSc4	Peri
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
uvedena	uvést	k5eAaPmNgFnS	uvést
ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1597	[number]	k4	1597
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
oratoria	oratorium	k1gNnSc2	oratorium
vždy	vždy	k6eAd1	vždy
reagoval	reagovat	k5eAaBmAgMnS	reagovat
na	na	k7c4	na
inovace	inovace	k1gFnPc4	inovace
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
opery	opera	k1gFnSc2	opera
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
rozmachu	rozmach	k1gInSc2	rozmach
oratoria	oratorium	k1gNnSc2	oratorium
zejména	zejména	k9	zejména
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
a	a	k8xC	a
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
<g/>
.	.	kIx.	.
</s>
<s>
Nejslavnějším	slavný	k2eAgMnSc7d3	nejslavnější
skladatelem	skladatel	k1gMnSc7	skladatel
oratorií	oratorium	k1gNnPc2	oratorium
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
byl	být	k5eAaImAgInS	být
Giacomo	Giacoma	k1gFnSc5	Giacoma
Carissimi	Carissi	k1gFnPc7	Carissi
(	(	kIx(	(
<g/>
1605	[number]	k4	1605
<g/>
–	–	k?	–
<g/>
1674	[number]	k4	1674
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
svými	svůj	k3xOyFgNnPc7	svůj
díly	dílo	k1gNnPc7	dílo
na	na	k7c4	na
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
kodifikoval	kodifikovat	k5eAaBmAgInS	kodifikovat
formální	formální	k2eAgNnPc4d1	formální
pravidla	pravidlo	k1gNnPc4	pravidlo
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnPc7	jeho
následovníky	následovník	k1gMnPc7	následovník
byli	být	k5eAaImAgMnP	být
zejména	zejména	k9	zejména
Antonio	Antonio	k1gMnSc1	Antonio
Draghi	Dragh	k1gFnSc2	Dragh
(	(	kIx(	(
<g/>
1634	[number]	k4	1634
<g/>
–	–	k?	–
<g/>
1700	[number]	k4	1700
<g/>
)	)	kIx)	)
a	a	k8xC	a
Alessandro	Alessandra	k1gFnSc5	Alessandra
Stradella	Stradello	k1gNnPc1	Stradello
(	(	kIx(	(
<g/>
1639	[number]	k4	1639
<g/>
–	–	k?	–
<g/>
1682	[number]	k4	1682
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
pak	pak	k6eAd1	pak
Marc-Antoine	Marc-Antoin	k1gInSc5	Marc-Antoin
Charpentier	Charpentier	k1gMnSc1	Charpentier
(	(	kIx(	(
<g/>
1643	[number]	k4	1643
<g/>
–	–	k?	–
<g/>
1704	[number]	k4	1704
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
paralelně	paralelně	k6eAd1	paralelně
rozvíjely	rozvíjet	k5eAaImAgInP	rozvíjet
dva	dva	k4xCgInPc1	dva
typy	typ	k1gInPc1	typ
oratoria	oratorium	k1gNnSc2	oratorium
<g/>
:	:	kIx,	:
oratorio	oratoria	k1gMnSc5	oratoria
volgare	volgar	k1gMnSc5	volgar
(	(	kIx(	(
<g/>
lidové	lidový	k2eAgNnSc1d1	lidové
oratorium	oratorium	k1gNnSc1	oratorium
<g/>
)	)	kIx)	)
na	na	k7c4	na
italský	italský	k2eAgInSc4d1	italský
text	text	k1gInSc4	text
a	a	k8xC	a
oratorio	oratorio	k6eAd1	oratorio
latino	latina	k1gFnSc5	latina
(	(	kIx(	(
<g/>
latinské	latinský	k2eAgNnSc4d1	latinské
oratorium	oratorium	k1gNnSc4	oratorium
<g/>
)	)	kIx)	)
na	na	k7c4	na
biblický	biblický	k2eAgInSc4d1	biblický
námět	námět	k1gInSc4	námět
a	a	k8xC	a
latinský	latinský	k2eAgInSc4d1	latinský
text	text	k1gInSc4	text
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Alessandro	Alessandra	k1gFnSc5	Alessandra
Scarlatti	Scarlatť	k1gFnSc5	Scarlatť
a	a	k8xC	a
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
celá	celý	k2eAgFnSc1d1	celá
Neapolská	neapolský	k2eAgFnSc1d1	neapolská
operní	operní	k2eAgFnSc1d1	operní
škola	škola	k1gFnSc1	škola
přenesli	přenést	k5eAaPmAgMnP	přenést
z	z	k7c2	z
opery	opera	k1gFnSc2	opera
do	do	k7c2	do
oratoria	oratorium	k1gNnSc2	oratorium
recitativ	recitativ	k1gInSc4	recitativ
secco	secco	k6eAd1	secco
i	i	k9	i
accompagnato	accompagnato	k6eAd1	accompagnato
a	a	k8xC	a
árii	árie	k1gFnSc3	árie
da	da	k?	da
capo	capa	k1gFnSc5	capa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
byly	být	k5eAaImAgInP	být
velmi	velmi	k6eAd1	velmi
oblíbené	oblíbený	k2eAgFnSc2d1	oblíbená
"	"	kIx"	"
<g/>
historie	historie	k1gFnSc2	historie
<g/>
"	"	kIx"	"
Heinricha	Heinrich	k1gMnSc2	Heinrich
Schütze	Schütze	k1gFnSc2	Schütze
(	(	kIx(	(
<g/>
Velikonoční	velikonoční	k2eAgFnSc2d1	velikonoční
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
1623	[number]	k4	1623
<g/>
;	;	kIx,	;
Vánoční	vánoční	k2eAgFnSc2d1	vánoční
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
1664	[number]	k4	1664
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholem	vrchol	k1gInSc7	vrchol
tohoto	tento	k3xDgInSc2	tento
vývoje	vývoj	k1gInSc2	vývoj
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
oratoria	oratorium	k1gNnSc2	oratorium
Georga	Georg	k1gMnSc2	Georg
Friedricha	Friedrich	k1gMnSc2	Friedrich
Händela	Händel	k1gMnSc2	Händel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
nejznámější	známý	k2eAgMnPc4d3	nejznámější
jsou	být	k5eAaImIp3nP	být
Mesiáš	Mesiáš	k1gMnSc1	Mesiáš
(	(	kIx(	(
<g/>
1742	[number]	k4	1742
<g/>
)	)	kIx)	)
a	a	k8xC	a
Juda	judo	k1gNnSc2	judo
Makabejský	makabejský	k2eAgMnSc1d1	makabejský
(	(	kIx(	(
<g/>
1746	[number]	k4	1746
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mimořádnou	mimořádný	k2eAgFnSc4d1	mimořádná
formu	forma	k1gFnSc4	forma
má	mít	k5eAaImIp3nS	mít
Vánoční	vánoční	k2eAgNnSc1d1	vánoční
oratorium	oratorium	k1gNnSc1	oratorium
Johanna	Johann	k1gMnSc2	Johann
Sebastiana	Sebastian	k1gMnSc2	Sebastian
Bacha	Bacha	k?	Bacha
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
spíše	spíše	k9	spíše
cyklem	cyklus	k1gInSc7	cyklus
šesti	šest	k4xCc2	šest
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
kantát	kantáta	k1gFnPc2	kantáta
<g/>
.	.	kIx.	.
</s>
<s>
Bach	Bach	k1gMnSc1	Bach
zde	zde	k6eAd1	zde
použil	použít	k5eAaPmAgMnS	použít
i	i	k9	i
některé	některý	k3yIgFnSc2	některý
části	část	k1gFnSc2	část
svých	svůj	k3xOyFgFnPc2	svůj
světských	světský	k2eAgFnPc2d1	světská
kantát	kantáta	k1gFnPc2	kantáta
podložených	podložený	k2eAgFnPc2d1	podložená
novým	nový	k2eAgInSc7d1	nový
textem	text	k1gInSc7	text
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Období	období	k1gNnSc1	období
klasicismu	klasicismus	k1gInSc2	klasicismus
je	být	k5eAaImIp3nS	být
charakterizováno	charakterizovat	k5eAaBmNgNnS	charakterizovat
vrcholnými	vrcholný	k2eAgInPc7d1	vrcholný
díly	díl	k1gInPc7	díl
Josepha	Joseph	k1gMnSc2	Joseph
Haydna	Haydna	k1gFnSc1	Haydna
(	(	kIx(	(
<g/>
Stvoření	stvoření	k1gNnSc1	stvoření
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
Die	Die	k1gFnSc1	Die
Schöpfung	Schöpfung	k1gInSc1	Schöpfung
1798	[number]	k4	1798
<g/>
)	)	kIx)	)
a	a	k8xC	a
Roční	roční	k2eAgNnSc1d1	roční
období	období	k1gNnSc1	období
(	(	kIx(	(
<g/>
Die	Die	k1gFnPc1	Die
Jahreszeiten	Jahreszeitno	k1gNnPc2	Jahreszeitno
<g/>
,	,	kIx,	,
1801	[number]	k4	1801
<g/>
))	))	k?	))
a	a	k8xC	a
Ludwiga	Ludwig	k1gMnSc2	Ludwig
van	vana	k1gFnPc2	vana
Beethovena	Beethoven	k1gMnSc2	Beethoven
(	(	kIx(	(
<g/>
Kristus	Kristus	k1gMnSc1	Kristus
na	na	k7c6	na
hoře	hora	k1gFnSc6	hora
Olivetské	olivetský	k2eAgFnSc6d1	Olivetská
<g/>
,	,	kIx,	,
1800	[number]	k4	1800
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
romantismus	romantismus	k1gInSc1	romantismus
se	se	k3xPyFc4	se
tvorbě	tvorba	k1gFnSc3	tvorba
oratorií	oratorium	k1gNnPc2	oratorium
nevyhýbal	vyhýbat	k5eNaImAgInS	vyhýbat
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
díla	dílo	k1gNnPc1	dílo
jako	jako	k8xS	jako
Ráj	ráj	k1gInSc1	ráj
a	a	k8xC	a
Peri	peri	k1gFnSc1	peri
Roberta	Robert	k1gMnSc2	Robert
Schumanna	Schumanna	k1gFnSc1	Schumanna
<g/>
,	,	kIx,	,
Legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
sv.	sv.	kA	sv.
Alžbětě	Alžběta	k1gFnSc6	Alžběta
Ference	Ferenc	k1gMnSc4	Ferenc
Liszta	Liszt	k1gMnSc4	Liszt
či	či	k8xC	či
Svatá	svatý	k2eAgFnSc1d1	svatá
Ludmila	Ludmila	k1gFnSc1	Ludmila
Antonína	Antonín	k1gMnSc2	Antonín
Dvořáka	Dvořák	k1gMnSc2	Dvořák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hudební	hudební	k2eAgFnSc1d1	hudební
výoj	výoj	k1gFnSc1	výoj
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vedl	vést	k5eAaImAgMnS	vést
k	k	k7c3	k
uvolnění	uvolnění	k1gNnSc3	uvolnění
formy	forma	k1gFnSc2	forma
a	a	k8xC	a
oratoria	oratorium	k1gNnSc2	oratorium
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
se	se	k3xPyFc4	se
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
individuálním	individuální	k2eAgNnSc7d1	individuální
řešením	řešení	k1gNnSc7	řešení
skladatele	skladatel	k1gMnSc2	skladatel
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgMnPc4d3	nejvýznamnější
patří	patřit	k5eAaImIp3nP	patřit
např.	např.	kA	např.
Král	Král	k1gMnSc1	Král
David	David	k1gMnSc1	David
Arthura	Arthura	k1gFnSc1	Arthura
Honeggera	Honeggera	k1gFnSc1	Honeggera
<g/>
,	,	kIx,	,
Jakubův	Jakubův	k2eAgInSc1d1	Jakubův
žebřík	žebřík	k1gInSc1	žebřík
Arnolda	Arnold	k1gMnSc2	Arnold
Schoenberga	Schoenberg	k1gMnSc2	Schoenberg
či	či	k8xC	či
opera-oratorium	operaratorium	k1gNnSc1	opera-oratorium
Igora	Igor	k1gMnSc2	Igor
Stravinského	Stravinský	k2eAgMnSc2d1	Stravinský
Oedipux	Oedipux	k1gInSc1	Oedipux
Rex	Rex	k1gMnPc1	Rex
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Opera	opera	k1gFnSc1	opera
</s>
</p>
<p>
<s>
Kantáta	kantáta	k1gFnSc1	kantáta
</s>
</p>
<p>
<s>
Moteto	moteto	k1gNnSc1	moteto
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Ulrich	Ulrich	k1gMnSc1	Ulrich
Michels	Michelsa	k1gFnPc2	Michelsa
<g/>
:	:	kIx,	:
Encyklopedický	encyklopedický	k2eAgInSc4d1	encyklopedický
atlas	atlas	k1gInSc4	atlas
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
Lidové	lidový	k2eAgFnPc1d1	lidová
noviny	novina	k1gFnPc1	novina
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
2000	[number]	k4	2000
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
7106	[number]	k4	7106
238	[number]	k4	238
3	[number]	k4	3
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Smolka	Smolka	k1gMnSc1	Smolka
<g/>
:	:	kIx,	:
Česká	český	k2eAgFnSc1d1	Česká
kantáta	kantáta	k1gFnSc1	kantáta
a	a	k8xC	a
oratorium	oratorium	k1gNnSc1	oratorium
<g/>
.	.	kIx.	.
1970	[number]	k4	1970
<g/>
,	,	kIx,	,
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
oratorium	oratorium	k1gNnSc4	oratorium
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Co	co	k3yQnSc1	co
jeco	jeco	k6eAd1	jeco
<g/>
?	?	kIx.	?
</s>
</p>
