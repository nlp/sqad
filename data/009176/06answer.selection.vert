<s>
Oratorium	oratorium	k1gNnSc1	oratorium
(	(	kIx(	(
<g/>
z	z	k7c2	z
lat.	lat.	k?	lat.
orare	orar	k1gMnSc5	orar
<g/>
,	,	kIx,	,
modlit	modlit	k5eAaImF	modlit
se	se	k3xPyFc4	se
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
hudební	hudební	k2eAgFnSc1d1	hudební
skladba	skladba	k1gFnSc1	skladba
<g/>
,	,	kIx,	,
zpravidla	zpravidla	k6eAd1	zpravidla
na	na	k7c4	na
duchovní	duchovní	k2eAgNnSc4d1	duchovní
(	(	kIx(	(
<g/>
náboženské	náboženský	k2eAgNnSc4d1	náboženské
<g/>
)	)	kIx)	)
téma	téma	k1gNnSc4	téma
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
sóla	sólo	k1gNnPc4	sólo
<g/>
,	,	kIx,	,
sbor	sbor	k1gInSc1	sbor
a	a	k8xC	a
orchestr	orchestr	k1gInSc1	orchestr
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
prováděna	provádět	k5eAaImNgFnS	provádět
koncertně	koncertně	k6eAd1	koncertně
(	(	kIx(	(
<g/>
nescénicky	scénicky	k6eNd1	scénicky
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
