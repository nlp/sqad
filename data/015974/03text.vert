<s>
Circuit	Circuit	k1gInSc1
of	of	k?
the	the	k?
Americas	Americas	k1gInSc1
</s>
<s>
Circuit	Circuit	k1gInSc1
of	of	k?
the	the	k?
Americas	Americas	k1gInSc1
</s>
<s>
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Lokace	lokace	k1gFnSc2
</s>
<s>
Austin	Austin	k1gInSc1
<g/>
,	,	kIx,
Texas	Texas	k1gInSc1
<g/>
,	,	kIx,
USA	USA	kA
Rekord	rekord	k1gInSc4
okruhu	okruh	k1gInSc2
</s>
<s>
1	#num#	k4
<g/>
:	:	kIx,
<g/>
36.169	36.169	k4
(	(	kIx(
<g/>
Charles	Charles	k1gMnSc1
Leclerc	Leclerc	k1gFnSc1
<g/>
,	,	kIx,
Ferrari	Ferrari	k1gMnSc1
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
)	)	kIx)
Délka	délka	k1gFnSc1
okruhu	okruh	k1gInSc2
</s>
<s>
5,513	5,513	k4
km	km	kA
Počet	počet	k1gInSc1
kol	kol	k7c2
</s>
<s>
56	#num#	k4
Délka	délka	k1gFnSc1
závodu	závod	k1gInSc2
</s>
<s>
308,405	308,405	k4
km	km	kA
Nejvíce	hodně	k6eAd3,k6eAd1
výher	výhra	k1gFnPc2
(	(	kIx(
<g/>
jezdec	jezdec	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
)	)	kIx)
Nejvíce	hodně	k6eAd3,k6eAd1
výher	výhra	k1gFnPc2
(	(	kIx(
<g/>
stáj	stáj	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Mercedes	mercedes	k1gInSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
Poprvé	poprvé	k6eAd1
v	v	k7c6
kalendáři	kalendář	k1gInSc6
</s>
<s>
2012	#num#	k4
Poslední	poslední	k2eAgInSc1d1
závod	závod	k1gInSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
Vítěz	vítěz	k1gMnSc1
</s>
<s>
Valtteri	Valtteri	k6eAd1
Bottas	Bottas	k1gInSc1
Mercedes	mercedes	k1gInSc1
1	#num#	k4
<g/>
h	h	k?
33	#num#	k4
<g/>
m	m	kA
55.653	55.653	k4
<g/>
s	s	k7c7
Pole	pole	k1gFnSc2
position	position	k1gInSc4
</s>
<s>
Valtteri	Valtteri	k6eAd1
Bottas	Bottas	k1gInSc1
Mercedes	mercedes	k1gInSc1
1	#num#	k4
<g/>
:	:	kIx,
<g/>
32.029	32.029	k4
Nejrychlejší	rychlý	k2eAgInSc4d3
kolo	kolo	k1gNnSc1
</s>
<s>
Charles	Charles	k1gMnSc1
Leclerc	Leclerc	k1gInSc4
Ferrari	ferrari	k1gNnSc2
1	#num#	k4
<g/>
:	:	kIx,
<g/>
36.169	36.169	k4
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
30	#num#	k4
<g/>
°	°	k?
<g/>
7	#num#	k4
<g/>
′	′	k?
<g/>
58,08	58,08	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
97	#num#	k4
<g/>
°	°	k?
<g/>
38	#num#	k4
<g/>
′	′	k?
<g/>
27,96	27,96	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
</s>
<s>
Circuit	Circuit	k1gInSc1
of	of	k?
the	the	k?
Americas	Americas	k1gInSc1
je	být	k5eAaImIp3nS
okruh	okruh	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
USA	USA	kA
poblíž	poblíž	k7c2
města	město	k1gNnSc2
Austin	Austin	k1gInSc1
v	v	k7c6
Texasu	Texas	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
okruh	okruh	k1gInSc1
byl	být	k5eAaImAgInS
otevřen	otevřít	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
tomto	tento	k3xDgInSc6
okruhu	okruh	k1gInSc6
se	se	k3xPyFc4
jezdí	jezdit	k5eAaImIp3nP
od	od	k7c2
roku	rok	k1gInSc2
2012	#num#	k4
Formule	formule	k1gFnSc1
1	#num#	k4
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
2013	#num#	k4
se	se	k3xPyFc4
zde	zde	k6eAd1
jezdí	jezdit	k5eAaImIp3nP
MotoGP	MotoGP	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okruh	okruh	k1gInSc1
navrhl	navrhnout	k5eAaPmAgMnS
návrhář	návrhář	k1gMnSc1
Herman	Herman	k1gMnSc1
Tilke	Tilke	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
první	první	k4xOgInSc1
okruh	okruh	k1gInSc1
v	v	k7c6
USA	USA	kA
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
by	by	kYmCp3nS
speciálně	speciálně	k6eAd1
postaven	postavit	k5eAaPmNgInS
pro	pro	k7c4
závody	závod	k1gInPc4
Formule	formule	k1gFnSc2
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Okruh	okruh	k1gInSc1
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
„	„	k?
<g/>
směsicí	směsice	k1gFnSc7
<g/>
“	“	k?
slavných	slavný	k2eAgFnPc2d1
zatáček	zatáčka	k1gFnPc2
z	z	k7c2
okruhů	okruh	k1gInPc2
Silverstone	Silverston	k1gInSc5
(	(	kIx(
<g/>
Maggotts	Maggottsa	k1gFnPc2
<g/>
,	,	kIx,
Backetts	Backettsa	k1gFnPc2
a	a	k8xC
Chapel	Chapela	k1gFnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Hockenheimring	Hockenheimring	k1gInSc1
(	(	kIx(
<g/>
stadion	stadion	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
tureckého	turecký	k2eAgMnSc2d1
Istanbul	Istanbul	k1gInSc1
Racing	Racing	k1gInSc1
Circuit	Circuit	k1gInSc4
(	(	kIx(
<g/>
několikrát	několikrát	k6eAd1
lomená	lomený	k2eAgFnSc1d1
zatáčka	zatáčka	k1gFnSc1
č.	č.	k?
8	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zajímavá	zajímavý	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
také	také	k9
první	první	k4xOgFnSc1
zatáčka	zatáčka	k1gFnSc1
tohoto	tento	k3xDgInSc2
okruhu	okruh	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
zvedá	zvedat	k5eAaImIp3nS
na	na	k7c6
několika	několik	k4yIc6
desítkách	desítka	k1gFnPc6
metrů	metr	k1gMnPc2
o	o	k7c4
42	#num#	k4
m.	m.	k?
</s>
<s>
Vítězové	vítěz	k1gMnPc1
v	v	k7c6
jednotlivých	jednotlivý	k2eAgNnPc6d1
letech	léto	k1gNnPc6
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Jezdec	jezdec	k1gMnSc1
</s>
<s>
Konstruktér	konstruktér	k1gMnSc1
</s>
<s>
Výsledky	výsledek	k1gInPc1
</s>
<s>
2019	#num#	k4
</s>
<s>
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc1
</s>
<s>
Mercedes	mercedes	k1gInSc1
</s>
<s>
Výsledky	výsledek	k1gInPc1
</s>
<s>
2018	#num#	k4
</s>
<s>
Kimi	Kimi	k6eAd1
Räikkönen	Räikkönen	k2eAgInSc1d1
</s>
<s>
Ferrari	Ferrari	k1gMnSc1
</s>
<s>
Výsledky	výsledek	k1gInPc1
</s>
<s>
2017	#num#	k4
</s>
<s>
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc1
</s>
<s>
Mercedes	mercedes	k1gInSc1
</s>
<s>
Výsledky	výsledek	k1gInPc1
</s>
<s>
2016	#num#	k4
</s>
<s>
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc1
</s>
<s>
Mercedes	mercedes	k1gInSc1
</s>
<s>
Výsledky	výsledek	k1gInPc1
</s>
<s>
2015	#num#	k4
</s>
<s>
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc1
</s>
<s>
Mercedes	mercedes	k1gInSc1
</s>
<s>
Výsledky	výsledek	k1gInPc1
</s>
<s>
2014	#num#	k4
</s>
<s>
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc1
</s>
<s>
Mercedes	mercedes	k1gInSc1
</s>
<s>
Výsledky	výsledek	k1gInPc1
</s>
<s>
2013	#num#	k4
</s>
<s>
Sebastian	Sebastian	k1gMnSc1
Vettel	Vettel	k1gMnSc1
</s>
<s>
Red	Red	k?
Bull	bulla	k1gFnPc2
</s>
<s>
Výsledky	výsledek	k1gInPc1
</s>
<s>
2012	#num#	k4
</s>
<s>
Lewis	Lewis	k1gInSc1
Hamilton	Hamilton	k1gInSc1
</s>
<s>
McLaren	McLarna	k1gFnPc2
</s>
<s>
Výsledky	výsledek	k1gInPc1
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Okruhy	okruh	k1gInPc1
F1	F1	k1gFnSc2
Současné	současný	k2eAgInPc1d1
</s>
<s>
Austin	Austin	k2eAgMnSc1d1
•	•	k?
Baku	Baku	k1gNnSc6
•	•	k?
Catalunya	Cataluny	k2eAgFnSc1d1
•	•	k?
Džidda	Džidda	k1gFnSc1
•	•	k?
Hungaroring	Hungaroring	k1gInSc1
•	•	k?
Imola	Imola	k1gFnSc1
•	•	k?
Interlagos	Interlagosa	k1gFnPc2
•	•	k?
Melbourne	Melbourne	k1gNnSc6
•	•	k?
Mexico	Mexico	k1gMnSc1
City	city	k1gNnSc1
•	•	k?
Monte	Mont	k1gMnSc5
Carlo	Carla	k1gMnSc5
•	•	k?
Monza	Monz	k1gMnSc2
•	•	k?
Paul	Paul	k1gMnSc1
Ricard	Ricard	k1gMnSc1
•	•	k?
Portimã	Portimã	k1gMnSc1
•	•	k?
Red	Red	k1gFnSc2
Bull	bulla	k1gFnPc2
Ring	ring	k1gInSc1
•	•	k?
Sakhir	Sakhir	k1gInSc1
•	•	k?
Silverstone	Silverston	k1gInSc5
•	•	k?
Singapur	Singapur	k1gInSc4
•	•	k?
Soči	Soči	k1gNnSc2
•	•	k?
Spa	Spa	k1gMnSc2
•	•	k?
Suzuka	Suzuk	k1gMnSc2
•	•	k?
Yas	Yas	k1gFnSc2
Marina	Marina	k1gFnSc1
•	•	k?
Zandvoort	Zandvoort	k1gInSc4
Bývalé	bývalý	k2eAgFnSc2d1
</s>
<s>
Adelaide	Adelaid	k1gMnSc5
•	•	k?
Ain-Diab	Ain-Diab	k1gInSc1
•	•	k?
Aintree	Aintree	k1gInSc1
•	•	k?
Anderstorp	Anderstorp	k1gInSc1
•	•	k?
AVUS	AVUS	kA
•	•	k?
Brands	Brands	k1gInSc1
Hatch	Hatch	k1gInSc1
•	•	k?
Bremgarten	Bremgarten	k2eAgInSc1d1
•	•	k?
Buddh	Buddh	k1gInSc1
International	International	k1gMnSc1
Circuit	Circuit	k1gMnSc1
•	•	k?
Buenos	Buenos	k1gMnSc1
Aires	Aires	k1gMnSc1
•	•	k?
Caesars	Caesars	k1gInSc1
Palace	Palace	k1gFnSc1
•	•	k?
Clermont-Ferrand	Clermont-Ferrand	k1gInSc1
•	•	k?
Dallas	Dallas	k1gInSc1
•	•	k?
Detroit	Detroit	k1gInSc1
•	•	k?
Dijon	Dijon	k1gInSc1
•	•	k?
Donington	Donington	k1gInSc1
Park	park	k1gInSc1
•	•	k?
East	East	k1gInSc1
London	London	k1gMnSc1
•	•	k?
Estoril	Estoril	k1gMnSc1
•	•	k?
Fuji	Fuji	kA
•	•	k?
Hockenheim	Hockenheim	k1gInSc1
•	•	k?
Indianapolis	Indianapolis	k1gInSc1
•	•	k?
Istanbul	Istanbul	k1gInSc1
•	•	k?
Jacarepagua	Jacarepagu	k1gInSc2
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Jarama	Jarama	k?
•	•	k?
Jerez	Jerez	k1gInSc1
•	•	k?
Kyalami	Kyala	k1gFnPc7
•	•	k?
Le	Le	k1gFnSc3
Mans	Mansa	k1gFnPc2
•	•	k?
Long	Long	k1gMnSc1
Beach	Beach	k1gMnSc1
•	•	k?
Magny-Cours	Magny-Cours	k1gInSc1
•	•	k?
Monsanto	Monsanta	k1gFnSc5
•	•	k?
Montjuï	Montjuï	k1gFnSc4
•	•	k?
Montréal	Montréal	k1gMnSc1
•	•	k?
Mont-Tremblant	Mont-Tremblant	k1gMnSc1
•	•	k?
Mosport	Mosport	k1gInSc1
Park	park	k1gInSc1
•	•	k?
Mugello	Mugello	k1gNnSc4
•	•	k?
Nivelles-Baulers	Nivelles-Baulers	k1gInSc1
•	•	k?
Nürburgring	Nürburgring	k1gInSc1
•	•	k?
Oporto	Oporta	k1gFnSc5
•	•	k?
Pedralbes	Pedralbes	k1gInSc1
•	•	k?
Pescara	Pescara	k1gFnSc1
•	•	k?
Phoenix	Phoenix	k1gInSc1
•	•	k?
Reims	Reims	k1gInSc1
•	•	k?
Riverside	Riversid	k1gInSc5
•	•	k?
Rouen	Rouen	k2eAgInSc1d1
•	•	k?
Sebring	Sebring	k1gInSc1
•	•	k?
Sepang	Sepang	k1gInSc1
•	•	k?
Šanghaj	Šanghaj	k1gFnSc1
•	•	k?
TI	ten	k3xDgMnPc1
•	•	k?
Valencia	Valencius	k1gMnSc2
•	•	k?
Watkins	Watkins	k1gInSc1
Glen	Glen	k1gMnSc1
•	•	k?
Yeongam	Yeongam	k1gInSc1
•	•	k?
Zeltweg	Zeltweg	k1gInSc1
•	•	k?
Zolder	Zolder	k1gInSc1
Budoucí	budoucí	k2eAgInPc1d1
</s>
<s>
Miami	Miami	k1gNnSc1
(	(	kIx(
<g/>
2022	#num#	k4
<g/>
)	)	kIx)
Odložené	odložený	k2eAgFnSc2d1
</s>
<s>
Hanoj	Hanoj	k1gFnSc1
</s>
