<s>
Valčík	valčík	k1gInSc1
je	být	k5eAaImIp3nS
postupový	postupový	k2eAgInSc1d1
<g/>
,	,	kIx,
kolový	kolový	k2eAgInSc1d1
tanec	tanec	k1gInSc1
v	v	k7c6
3	[number]	k4
<g/>
/	/	kIx~
<g/>
4	[number]	k4
taktu	takt	k1gInSc2
nebo	nebo	k8xC
zřídka	zřídka	k6eAd1
(	(	kIx(
<g/>
spíše	spíše	k9
dříve	dříve	k6eAd2
<g/>
)	)	kIx)
v	v	k7c6
3	[number]	k4
<g/>
/	/	kIx~
<g/>
8	[number]	k4
taktu	takt	k1gInSc2
<g/>
,	,	kIx,
důraz	důraz	k1gInSc1
je	být	k5eAaImIp3nS
kladen	klást	k5eAaImNgInS
na	na	k7c4
první	první	k4xOgFnSc4
dobu	doba	k1gFnSc4
<g/>
.	.	kIx.
</s>