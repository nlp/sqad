<s>
Josef	Josef	k1gMnSc1
ha-Kohen	ha-Kohna	k1gFnPc2
</s>
<s>
Josef	Josef	k1gMnSc1
ha-Kohen	ha-Kohen	k2eAgMnSc1d1
Narození	narození	k1gNnSc2
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
1496	#num#	k4
<g/>
Avignon	Avignon	k1gInSc1
Úmrtí	úmrtí	k1gNnSc2
</s>
<s>
1558	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
61	#num#	k4
<g/>
–	–	k?
<g/>
62	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
nebo	nebo	k8xC
1578	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
81	#num#	k4
<g/>
–	–	k?
<g/>
82	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Janov	Janov	k1gInSc1
Národnost	národnost	k1gFnSc4
</s>
<s>
Sefardi	Sefard	k1gMnPc1
Povolání	povolání	k1gNnSc2
</s>
<s>
historik	historik	k1gMnSc1
<g/>
,	,	kIx,
lékař	lékař	k1gMnSc1
a	a	k8xC
rabín	rabín	k1gMnSc1
Nábož	Nábož	k1gFnSc1
<g/>
.	.	kIx.
vyznání	vyznání	k1gNnSc1
</s>
<s>
judaismus	judaismus	k1gInSc1
Některá	některý	k3yIgNnPc4
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Josef	Josef	k1gMnSc1
ha-Kohen	 ha-Kohen	k1gMnPc1
(	(	kIx(
<g/>
1496	#num#	k4
<g/>
,	,	kIx,
Avignon	Avignon	k1gInSc1
–	–	k?
1558	#num#	k4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
)	)	kIx)
byl	být	k5eAaImAgMnS
židovský	židovský	k2eAgMnSc1d1
lékař	lékař	k1gMnSc1
a	a	k8xC
historik	historik	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Život	život	k1gInSc1
a	a	k8xC
dílo	dílo	k1gNnSc1
</s>
<s>
Ha-Kohen	Ha-Kohen	k2eAgMnSc1d1
se	se	k3xPyFc4
narodil	narodit	k5eAaPmAgMnS
roku	rok	k1gInSc2
1496	#num#	k4
ve	v	k7c6
francouzském	francouzský	k2eAgInSc6d1
Avignonu	Avignon	k1gInSc6
jako	jako	k8xC,k8xS
potomek	potomek	k1gMnSc1
žida	žid	k1gMnSc2
<g/>
,	,	kIx,
vyhnaného	vyhnaný	k2eAgInSc2d1
roku	rok	k1gInSc2
1492	#num#	k4
ze	z	k7c2
Španělska	Španělsko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roku	rok	k1gInSc2
1501	#num#	k4
se	se	k3xPyFc4
rodina	rodina	k1gFnSc1
přestěhovala	přestěhovat	k5eAaPmAgFnS
do	do	k7c2
italského	italský	k2eAgInSc2d1
Janova	Janov	k1gInSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
ha-Kohen	ha-Kohen	k1gInSc1
obdržel	obdržet	k5eAaPmAgInS
tradiční	tradiční	k2eAgNnSc4d1
židovské	židovský	k2eAgNnSc4d1
vzdělání	vzdělání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
ukončení	ukončení	k1gNnSc6
studií	studio	k1gNnPc2
pracoval	pracovat	k5eAaImAgMnS
jako	jako	k9
lékař	lékař	k1gMnSc1
v	v	k7c6
různých	různý	k2eAgNnPc6d1
italských	italský	k2eAgNnPc6d1
městech	město	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gFnSc1
manželka	manželka	k1gFnSc1
mu	on	k3xPp3gMnSc3
porodila	porodit	k5eAaPmAgFnS
tři	tři	k4xCgMnPc4
syny	syn	k1gMnPc4
<g/>
,	,	kIx,
všichni	všechen	k3xTgMnPc1
však	však	k9
ještě	ještě	k9
za	za	k7c2
jeho	on	k3xPp3gInSc2,k3xOp3gInSc2
života	život	k1gInSc2
zemřeli	zemřít	k5eAaPmAgMnP
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
vyhnání	vyhnání	k1gNnSc6
židů	žid	k1gMnPc2
z	z	k7c2
Janova	Janov	k1gInSc2
psal	psát	k5eAaImAgMnS
knihy	kniha	k1gFnPc4
a	a	k8xC
poezii	poezie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgNnSc7
z	z	k7c2
nejvýznamnějších	významný	k2eAgNnPc2d3
děl	dělo	k1gNnPc2
je	být	k5eAaImIp3nS
historiografická	historiografický	k2eAgFnSc1d1
kniha	kniha	k1gFnSc1
Události	událost	k1gFnSc2
dnů	den	k1gInPc2
týkající	týkající	k2eAgInSc4d1
se	se	k3xPyFc4
francouzského	francouzský	k2eAgMnSc2d1
krále	král	k1gMnSc2
a	a	k8xC
osmanské	osmanský	k2eAgFnSc2d1
dynastie	dynastie	k1gFnSc2
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
ד	ד	k?
ה	ה	k?
ל	ל	k?
צ	צ	k?
ו	ו	k?
ב	ב	k?
א	א	k?
ה	ה	k?
<g/>
,	,	kIx,
Divrej	Divrej	k1gMnSc1
ha-jamim	ha-jamim	k1gMnSc1
le-malchej	le-malchat	k5eAaPmRp2nS,k5eAaImRp2nS
Corfat	Corfat	k1gMnSc1
u-le-malchej	u-le-malchat	k5eAaImRp2nS,k5eAaPmRp2nS
bejt	bejt	k?
otoman	otoman	k1gInSc1
ha-tugar	ha-tugar	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalším	další	k2eAgInSc7d1
významným	významný	k2eAgInSc7d1
dílem	díl	k1gInSc7
se	se	k3xPyFc4
stalo	stát	k5eAaPmAgNnS
dílo	dílo	k1gNnSc1
Údolí	údolí	k1gNnSc2
pláče	plakat	k5eAaImIp3nS
(	(	kIx(
<g/>
hebrejsky	hebrejsky	k6eAd1
ע	ע	k?
ה	ה	k?
<g/>
,	,	kIx,
Emek	Emek	k1gMnSc1
ha-bacha	ha-bacha	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc4
dokončil	dokončit	k5eAaPmAgInS
roku	rok	k1gInSc2
1558	#num#	k4
ve	v	k7c6
svých	svůj	k3xOyFgInPc6
79	#num#	k4
letech	let	k1gInPc6
a	a	k8xC
v	v	k7c6
němž	jenž	k3xRgInSc6
popisuje	popisovat	k5eAaImIp3nS
dějiny	dějiny	k1gFnPc4
izraelského	izraelský	k2eAgInSc2d1
lidu	lid	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
tohoto	tento	k3xDgInSc2
roku	rok	k1gInSc2
také	také	k9
pochází	pocházet	k5eAaImIp3nS
poslední	poslední	k2eAgFnSc1d1
zmínka	zmínka	k1gFnSc1
o	o	k7c6
Josefu	Josef	k1gMnSc6
ha-Kohenovi	ha-Kohen	k1gMnSc6
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
י	י	k?
ה	ה	k?
na	na	k7c6
hebrejské	hebrejský	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Divrej	Divrat	k5eAaImRp2nS,k5eAaPmRp2nS
ha-jamim	ha-jamim	k6eAd1
na	na	k7c6
HebrewBooks	HebrewBooksa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
org	org	k?
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
122750144	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0001	#num#	k4
2029	#num#	k4
5502	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
82020299	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
82204047	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
82020299	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hebraistika	hebraistika	k1gFnSc1
</s>
