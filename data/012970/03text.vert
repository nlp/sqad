<p>
<s>
Maté	maté	k1gNnSc1	maté
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
také	také	k9	také
yerba	yerba	k1gFnSc1	yerba
maté	maté	k1gNnSc2	maté
<g/>
,	,	kIx,	,
z	z	k7c2	z
původního	původní	k2eAgNnSc2d1	původní
mate	mást	k5eAaImIp3nS	mást
(	(	kIx(	(
<g/>
špan.	špan.	k?	špan.
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
port	port	k1gInSc1	port
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejoblíbenější	oblíbený	k2eAgInSc1d3	nejoblíbenější
jihoamerický	jihoamerický	k2eAgInSc1d1	jihoamerický
nápoj	nápoj	k1gInSc1	nápoj
<g/>
,	,	kIx,	,
připravovaný	připravovaný	k2eAgInSc1d1	připravovaný
z	z	k7c2	z
lístků	lístek	k1gInPc2	lístek
cesmíny	cesmín	k1gInPc4	cesmín
paraguayské	paraguayský	k2eAgInPc4d1	paraguayský
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
stálezelený	stálezelený	k2eAgMnSc1d1	stálezelený
<g/>
,	,	kIx,	,
asi	asi	k9	asi
15	[number]	k4	15
m	m	kA	m
vysoký	vysoký	k2eAgInSc4d1	vysoký
strom	strom	k1gInSc4	strom
<g/>
.	.	kIx.	.
</s>
<s>
Pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
na	na	k7c6	na
území	území	k1gNnSc6	území
Argentiny	Argentina	k1gFnSc2	Argentina
<g/>
,	,	kIx,	,
Brazílie	Brazílie	k1gFnSc2	Brazílie
<g/>
,	,	kIx,	,
Chile	Chile	k1gNnSc2	Chile
<g/>
,	,	kIx,	,
Uruguaye	Uruguay	k1gFnSc2	Uruguay
a	a	k8xC	a
Paraguaye	Paraguay	k1gFnSc2	Paraguay
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Účinky	účinek	k1gInPc4	účinek
==	==	k?	==
</s>
</p>
<p>
<s>
Maté	maté	k1gNnSc1	maté
je	být	k5eAaImIp3nS	být
známé	známý	k2eAgNnSc1d1	známé
především	především	k9	především
svými	svůj	k3xOyFgInPc7	svůj
výraznými	výrazný	k2eAgInPc7d1	výrazný
povzbujícími	povzbující	k2eAgInPc7d1	povzbující
účinky	účinek	k1gInPc7	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
alkaloid	alkaloid	k1gInSc1	alkaloid
kofein	kofein	k1gInSc1	kofein
(	(	kIx(	(
<g/>
v	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
s	s	k7c7	s
maté	maté	k1gNnSc7	maté
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k9	jako
matein	matein	k1gInSc1	matein
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
minerální	minerální	k2eAgFnPc1d1	minerální
látky	látka	k1gFnPc1	látka
(	(	kIx(	(
<g/>
hořčík	hořčík	k1gInSc1	hořčík
<g/>
,	,	kIx,	,
vápník	vápník	k1gInSc1	vápník
<g/>
,	,	kIx,	,
draslík	draslík	k1gInSc1	draslík
<g/>
,	,	kIx,	,
železo	železo	k1gNnSc1	železo
<g/>
,	,	kIx,	,
křemík	křemík	k1gInSc1	křemík
<g/>
,	,	kIx,	,
fosfor	fosfor	k1gInSc1	fosfor
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vitamíny	vitamín	k1gInPc1	vitamín
A	A	kA	A
<g/>
,	,	kIx,	,
B	B	kA	B
<g/>
,	,	kIx,	,
C	C	kA	C
<g/>
,	,	kIx,	,
E	E	kA	E
<g/>
,	,	kIx,	,
betakaroten	betakaroten	k2eAgMnSc1d1	betakaroten
a	a	k8xC	a
antioxidanty	antioxidant	k1gInPc1	antioxidant
<g/>
.	.	kIx.	.
</s>
<s>
Maté	maté	k1gNnSc1	maté
podporuje	podporovat	k5eAaImIp3nS	podporovat
funkci	funkce	k1gFnSc4	funkce
ledvin	ledvina	k1gFnPc2	ledvina
<g/>
,	,	kIx,	,
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
látkovou	látkový	k2eAgFnSc4d1	látková
výměnu	výměna	k1gFnSc4	výměna
<g/>
,	,	kIx,	,
normalizuje	normalizovat	k5eAaBmIp3nS	normalizovat
hladinu	hladina	k1gFnSc4	hladina
cukru	cukr	k1gInSc2	cukr
v	v	k7c6	v
krvi	krev	k1gFnSc6	krev
a	a	k8xC	a
posiluje	posilovat	k5eAaImIp3nS	posilovat
imunitní	imunitní	k2eAgInSc4d1	imunitní
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
také	také	k9	také
pitím	pití	k1gNnSc7	pití
maté	maté	k1gNnSc2	maté
tlumí	tlumit	k5eAaImIp3nS	tlumit
pocit	pocit	k1gInSc4	pocit
hladu	hlad	k1gInSc2	hlad
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
jim	on	k3xPp3gMnPc3	on
pomáhalo	pomáhat	k5eAaImAgNnS	pomáhat
přežít	přežít	k5eAaPmF	přežít
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
nouze	nouze	k1gFnSc2	nouze
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
tato	tento	k3xDgFnSc1	tento
vlastnost	vlastnost	k1gFnSc1	vlastnost
využívá	využívat	k5eAaPmIp3nS	využívat
při	při	k7c6	při
různých	různý	k2eAgFnPc6d1	různá
redukčních	redukční	k2eAgFnPc6d1	redukční
dietách	dieta	k1gFnPc6	dieta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pití	pití	k1gNnSc1	pití
čaje	čaj	k1gInSc2	čaj
===	===	k?	===
</s>
</p>
<p>
<s>
Maté	maté	k1gNnSc1	maté
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
podle	podle	k7c2	podle
tradičního	tradiční	k2eAgInSc2d1	tradiční
způsobu	způsob	k1gInSc2	způsob
podávat	podávat	k5eAaImF	podávat
v	v	k7c6	v
nádobě	nádoba	k1gFnSc6	nádoba
z	z	k7c2	z
vydlabané	vydlabaný	k2eAgFnSc2d1	vydlabaná
tykve	tykev	k1gFnSc2	tykev
s	s	k7c7	s
úzkým	úzký	k2eAgNnSc7d1	úzké
hrdlem	hrdlo	k1gNnSc7	hrdlo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
buď	buď	k8xC	buď
maté	maté	k1gNnSc1	maté
nebo	nebo	k8xC	nebo
kalabasa	kalabasa	k1gFnSc1	kalabasa
<g/>
.	.	kIx.	.
</s>
<s>
Nápoj	nápoj	k1gInSc1	nápoj
se	se	k3xPyFc4	se
usrkává	usrkávat	k5eAaImIp3nS	usrkávat
bambusovým	bambusový	k2eAgInSc7d1	bambusový
brčkem	brček	k1gInSc7	brček
nebo	nebo	k8xC	nebo
kovovou	kovový	k2eAgFnSc7d1	kovová
slámkou	slámka	k1gFnSc7	slámka
–	–	k?	–
bombillou	bombillat	k5eAaPmIp3nP	bombillat
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
konci	konec	k1gInSc6	konec
proděravělá	proděravělý	k2eAgFnSc1d1	proděravělá
a	a	k8xC	a
funguje	fungovat	k5eAaImIp3nS	fungovat
tak	tak	k6eAd1	tak
jako	jako	k9	jako
slámka	slámka	k1gFnSc1	slámka
a	a	k8xC	a
sítko	sítko	k1gNnSc1	sítko
zároveň	zároveň	k6eAd1	zároveň
<g/>
.	.	kIx.	.
</s>
<s>
Jihoameričané	Jihoameričan	k1gMnPc1	Jihoameričan
pijí	pít	k5eAaImIp3nP	pít
maté	maté	k1gNnSc4	maté
s	s	k7c7	s
limonádou	limonáda	k1gFnSc7	limonáda
<g/>
,	,	kIx,	,
džusem	džus	k1gInSc7	džus
nebo	nebo	k8xC	nebo
například	například	k6eAd1	například
pražené	pražený	k2eAgNnSc1d1	pražené
<g/>
.	.	kIx.	.
</s>
<s>
Maté	maté	k1gNnPc1	maté
popíjejí	popíjet	k5eAaImIp3nP	popíjet
v	v	k7c4	v
kteroukoliv	kterýkoliv	k3yIgFnSc4	kterýkoliv
denní	denní	k2eAgFnSc4d1	denní
nebo	nebo	k8xC	nebo
večerní	večerní	k2eAgFnSc4d1	večerní
dobu	doba	k1gFnSc4	doba
<g/>
.	.	kIx.	.
</s>
<s>
Kalabasa	Kalabasa	k1gFnSc1	Kalabasa
pro	pro	k7c4	pro
osobní	osobní	k2eAgNnSc4d1	osobní
použití	použití	k1gNnSc4	použití
je	být	k5eAaImIp3nS	být
relativně	relativně	k6eAd1	relativně
malá	malý	k2eAgFnSc1d1	malá
<g/>
,	,	kIx,	,
přibližně	přibližně	k6eAd1	přibližně
o	o	k7c6	o
obsahu	obsah	k1gInSc6	obsah
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
2	[number]	k4	2
dl.	dl.	k?	dl.
Větší	veliký	k2eAgInPc1d2	veliký
kalabasy	kalabas	k1gInPc1	kalabas
jsou	být	k5eAaImIp3nP	být
vhodné	vhodný	k2eAgInPc1d1	vhodný
pro	pro	k7c4	pro
skupinové	skupinový	k2eAgNnSc4d1	skupinové
pití	pití	k1gNnSc4	pití
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
dáno	dát	k5eAaPmNgNnS	dát
především	především	k6eAd1	především
vysokým	vysoký	k2eAgInSc7d1	vysoký
obsahem	obsah	k1gInSc7	obsah
kofeinu	kofein	k1gInSc2	kofein
v	v	k7c6	v
nápoji	nápoj	k1gInSc6	nápoj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Příprava	příprava	k1gFnSc1	příprava
maté	maté	k1gNnSc2	maté
==	==	k?	==
</s>
</p>
<p>
<s>
Kalabasa	Kalabasa	k1gFnSc1	Kalabasa
se	se	k3xPyFc4	se
naplní	naplnit	k5eAaPmIp3nS	naplnit
sypkým	sypký	k2eAgNnSc7d1	sypké
maté	maté	k1gNnSc7	maté
asi	asi	k9	asi
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
do	do	k7c2	do
tří	tři	k4xCgFnPc2	tři
čtvrtin	čtvrtina	k1gFnPc2	čtvrtina
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
otvor	otvor	k1gInSc1	otvor
nádoby	nádoba	k1gFnSc2	nádoba
zakryje	zakrýt	k5eAaPmIp3nS	zakrýt
rukou	ruka	k1gFnSc7	ruka
<g/>
,	,	kIx,	,
převrátí	převrátit	k5eAaPmIp3nS	převrátit
se	se	k3xPyFc4	se
a	a	k8xC	a
protřepe	protřepat	k5eAaPmIp3nS	protřepat
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
dlani	dlaň	k1gFnSc6	dlaň
vám	vy	k3xPp2nPc3	vy
ulpí	ulpět	k5eAaPmIp3nS	ulpět
část	část	k1gFnSc4	část
prachové	prachový	k2eAgFnSc2d1	prachová
složky	složka	k1gFnSc2	složka
maté	maté	k1gNnSc2	maté
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
vám	vy	k3xPp2nPc3	vy
maté	maté	k1gNnSc1	maté
prach	prach	k1gInSc1	prach
vadí	vadit	k5eAaImIp3nS	vadit
při	při	k7c6	při
pití	pití	k1gNnSc6	pití
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
koupit	koupit	k5eAaPmF	koupit
prosáté	prosátý	k2eAgNnSc4d1	prosáté
maté	maté	k1gNnSc4	maté
bez	bez	k7c2	bez
prachové	prachový	k2eAgFnSc2d1	prachová
složky	složka	k1gFnSc2	složka
<g/>
;	;	kIx,	;
někdo	někdo	k3yInSc1	někdo
naopak	naopak	k6eAd1	naopak
maté	maté	k1gNnSc1	maté
prach	prach	k1gInSc1	prach
schválně	schválně	k6eAd1	schválně
přidává	přidávat	k5eAaImIp3nS	přidávat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
můžete	moct	k5eAaImIp2nP	moct
udělat	udělat	k5eAaPmF	udělat
několikrát	několikrát	k6eAd1	několikrát
a	a	k8xC	a
na	na	k7c4	na
závěr	závěr	k1gInSc4	závěr
obrátíte	obrátit	k5eAaPmIp2nP	obrátit
kalabasu	kalabasa	k1gFnSc4	kalabasa
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
maté	maté	k1gNnSc1	maté
shromáždilo	shromáždit	k5eAaPmAgNnS	shromáždit
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
nádoby	nádoba	k1gFnSc2	nádoba
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
nádobu	nádoba	k1gFnSc4	nádoba
postavíte	postavit	k5eAaPmIp2nP	postavit
a	a	k8xC	a
maté	maté	k1gNnSc1	maté
by	by	kYmCp3nP	by
mělo	mít	k5eAaImAgNnS	mít
vytvořit	vytvořit	k5eAaPmF	vytvořit
kopeček	kopeček	k1gInSc4	kopeček
<g/>
,	,	kIx,	,
svažující	svažující	k2eAgInPc4d1	svažující
se	se	k3xPyFc4	se
od	od	k7c2	od
vrcholu	vrchol	k1gInSc2	vrchol
nádoby	nádoba	k1gFnSc2	nádoba
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
až	až	k9	až
na	na	k7c4	na
dno	dno	k1gNnSc4	dno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
chvíli	chvíle	k1gFnSc4	chvíle
se	se	k3xPyFc4	se
maté	maté	k1gNnSc1	maté
zalije	zalít	k5eAaPmIp3nS	zalít
trochou	trocha	k1gFnSc7	trocha
studené	studený	k2eAgFnSc2d1	studená
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
zajistíte	zajistit	k5eAaPmIp2nP	zajistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
maté	maté	k1gNnPc1	maté
po	po	k7c4	po
zalití	zalití	k1gNnSc4	zalití
horkou	horký	k2eAgFnSc7d1	horká
vodou	voda	k1gFnSc7	voda
"	"	kIx"	"
<g/>
nespálíte	spálit	k5eNaPmIp2nP	spálit
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
minutách	minuta	k1gFnPc6	minuta
do	do	k7c2	do
kalabasy	kalabasa	k1gFnSc2	kalabasa
zasunete	zasunout	k5eAaPmIp2nP	zasunout
bombillu	bombilla	k1gFnSc4	bombilla
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
ji	on	k3xPp3gFnSc4	on
zastrčíte	zastrčit	k5eAaPmIp2nP	zastrčit
do	do	k7c2	do
svahu	svah	k1gInSc2	svah
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
maté	maté	k1gNnSc4	maté
<g/>
,	,	kIx,	,
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
chvíle	chvíle	k1gFnSc2	chvíle
už	už	k6eAd1	už
ji	on	k3xPp3gFnSc4	on
nevytahujete	vytahovat	k5eNaImIp2nP	vytahovat
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
se	se	k3xPyFc4	se
vše	všechen	k3xTgNnSc1	všechen
zalije	zalít	k5eAaPmIp3nS	zalít
horkou	horka	k1gFnSc7	horka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
vařící	vařící	k2eAgFnSc7d1	vařící
vodou	voda	k1gFnSc7	voda
(	(	kIx(	(
<g/>
cca	cca	kA	cca
80	[number]	k4	80
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
nezbořil	zbořit	k5eNaPmAgInS	zbořit
svážek	svážek	k1gInSc1	svážek
a	a	k8xC	a
nad	nad	k7c7	nad
hladinou	hladina	k1gFnSc7	hladina
zůstal	zůstat	k5eAaPmAgMnS	zůstat
malý	malý	k2eAgInSc4d1	malý
ostrůvek	ostrůvek	k1gInSc4	ostrůvek
suchého	suchý	k2eAgNnSc2d1	suché
maté	maté	k1gNnSc2	maté
<g/>
.	.	kIx.	.
</s>
<s>
Maté	maté	k1gNnSc1	maté
se	se	k3xPyFc4	se
neslévá	slévat	k5eNaImIp3nS	slévat
a	a	k8xC	a
lístky	lístek	k1gInPc1	lístek
se	se	k3xPyFc4	se
ponechávají	ponechávat	k5eAaImIp3nP	ponechávat
v	v	k7c6	v
nádobě	nádoba	k1gFnSc6	nádoba
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
pití	pití	k1gNnSc2	pití
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
pití	pití	k1gNnSc4	pití
bombilla	bombillo	k1gNnSc2	bombillo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ortodoxní	ortodoxní	k2eAgMnPc1d1	ortodoxní
příznivci	příznivec	k1gMnPc1	příznivec
pijí	pít	k5eAaImIp3nP	pít
maté	maté	k1gNnSc4	maté
bez	bez	k7c2	bez
příchuti	příchuť	k1gFnSc2	příchuť
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
lze	lze	k6eAd1	lze
jej	on	k3xPp3gNnSc4	on
ochutit	ochutit	k5eAaPmF	ochutit
karamelem	karamel	k1gInSc7	karamel
<g/>
,	,	kIx,	,
cukrem	cukr	k1gInSc7	cukr
<g/>
,	,	kIx,	,
sirupem	sirup	k1gInSc7	sirup
<g/>
,	,	kIx,	,
medem	med	k1gInSc7	med
nebo	nebo	k8xC	nebo
citronem	citron	k1gInSc7	citron
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zajímavost	zajímavost	k1gFnSc4	zajímavost
==	==	k?	==
</s>
</p>
<p>
<s>
Pití	pití	k1gNnSc1	pití
maté	maté	k1gNnSc2	maté
je	být	k5eAaImIp3nS	být
nejrozšířenější	rozšířený	k2eAgFnSc1d3	nejrozšířenější
a	a	k8xC	a
nejoblíbenější	oblíbený	k2eAgFnSc1d3	nejoblíbenější
v	v	k7c6	v
Uruguayi	Uruguay	k1gFnSc6	Uruguay
a	a	k8xC	a
Argentině	Argentina	k1gFnSc6	Argentina
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
ho	on	k3xPp3gInSc4	on
na	na	k7c6	na
nápojovém	nápojový	k2eAgInSc6d1	nápojový
lístku	lístek	k1gInSc6	lístek
téměř	téměř	k6eAd1	téměř
nikde	nikde	k6eAd1	nikde
nenajdete	najít	k5eNaPmIp2nP	najít
<g/>
.	.	kIx.	.
</s>
<s>
Materiál	materiál	k1gInSc1	materiál
<g/>
,	,	kIx,	,
tvar	tvar	k1gInSc1	tvar
i	i	k8xC	i
zpracování	zpracování	k1gNnSc1	zpracování
kalabasy	kalabasa	k1gFnSc2	kalabasa
a	a	k8xC	a
bombilly	bombilla	k1gFnSc2	bombilla
jsou	být	k5eAaImIp3nP	být
předmětem	předmět	k1gInSc7	předmět
osobní	osobní	k2eAgFnSc2d1	osobní
preference	preference	k1gFnSc2	preference
a	a	k8xC	a
vztahu	vztah	k1gInSc2	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
restaurace	restaurace	k1gFnSc1	restaurace
či	či	k8xC	či
čerpací	čerpací	k2eAgFnSc1d1	čerpací
stanice	stanice	k1gFnSc1	stanice
zato	zato	k6eAd1	zato
nabízí	nabízet	k5eAaImIp3nS	nabízet
horkou	horký	k2eAgFnSc4d1	horká
vodu	voda	k1gFnSc4	voda
na	na	k7c4	na
zalití	zalití	k1gNnSc4	zalití
maté	maté	k1gNnSc2	maté
ve	v	k7c6	v
vlastní	vlastní	k2eAgFnSc6d1	vlastní
kalabase	kalabasa	k1gFnSc6	kalabasa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
maté	maté	k1gNnSc2	maté
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
maté	maté	k1gNnSc2	maté
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
yerba-mate	yerbaat	k1gInSc5	yerba-mat
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
portál	portál	k1gInSc1	portál
věnovaný	věnovaný	k2eAgMnSc1d1	věnovaný
Yerba	Yerba	k1gMnSc1	Yerba
Mate	mást	k5eAaImIp3nS	mást
</s>
</p>
<p>
<s>
Luckycesta	Luckycesta	k1gFnSc1	Luckycesta
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
–	–	k?	–
Detailní	detailní	k2eAgFnSc2d1	detailní
historie	historie	k1gFnSc2	historie
Yerba	Yerba	k1gMnSc1	Yerba
Mate	mást	k5eAaImIp3nS	mást
<g/>
,	,	kIx,	,
legendy	legenda	k1gFnPc1	legenda
<g/>
,	,	kIx,	,
servírování	servírování	k1gNnSc1	servírování
Yerby	Yerba	k1gFnSc2	Yerba
v	v	k7c6	v
Argentině	Argentina	k1gFnSc6	Argentina
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
<g/>
,	,	kIx,	,
popis	popis	k1gInSc1	popis
vybavení	vybavení	k1gNnSc2	vybavení
<g/>
.	.	kIx.	.
</s>
</p>
