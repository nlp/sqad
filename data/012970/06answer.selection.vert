<s>
Maté	maté	k1gNnSc1	maté
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
také	také	k9	také
yerba	yerba	k1gFnSc1	yerba
maté	maté	k1gNnSc2	maté
<g/>
,	,	kIx,	,
z	z	k7c2	z
původního	původní	k2eAgNnSc2d1	původní
mate	mást	k5eAaImIp3nS	mást
(	(	kIx(	(
<g/>
špan.	špan.	k?	špan.
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
port	port	k1gInSc1	port
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
ˈ	ˈ	k?	ˈ
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejoblíbenější	oblíbený	k2eAgInSc1d3	nejoblíbenější
jihoamerický	jihoamerický	k2eAgInSc1d1	jihoamerický
nápoj	nápoj	k1gInSc1	nápoj
<g/>
,	,	kIx,	,
připravovaný	připravovaný	k2eAgInSc1d1	připravovaný
z	z	k7c2	z
lístků	lístek	k1gInPc2	lístek
cesmíny	cesmín	k1gInPc4	cesmín
paraguayské	paraguayský	k2eAgInPc4d1	paraguayský
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
stálezelený	stálezelený	k2eAgMnSc1d1	stálezelený
<g/>
,	,	kIx,	,
asi	asi	k9	asi
15	[number]	k4	15
m	m	kA	m
vysoký	vysoký	k2eAgInSc4d1	vysoký
strom	strom	k1gInSc4	strom
<g/>
.	.	kIx.	.
</s>
