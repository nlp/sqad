<s>
Stát	stát	k1gInSc1	stát
Vatikán	Vatikán	k1gInSc1	Vatikán
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
na	na	k7c6	na
základě	základ	k1gInSc6	základ
Lateránských	lateránský	k2eAgFnPc2d1	Lateránská
smluv	smlouva	k1gFnPc2	smlouva
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
podepsal	podepsat	k5eAaPmAgMnS	podepsat
kardinál	kardinál	k1gMnSc1	kardinál
Pietro	Pietro	k1gNnSc4	Pietro
Gasparri	Gasparr	k1gFnSc2	Gasparr
jménem	jméno	k1gNnSc7	jméno
Svatého	svatý	k2eAgInSc2d1	svatý
stolce	stolec	k1gInSc2	stolec
a	a	k8xC	a
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
Benito	Benita	k1gMnSc5	Benita
Mussolini	Mussolin	k2eAgMnPc1d1	Mussolin
jménem	jméno	k1gNnSc7	jméno
království	království	k1gNnSc2	království
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
.	.	kIx.	.
</s>
