<s>
Jaký	jaký	k3yRgInSc1	jaký
je	být	k5eAaImIp3nS	být
nejsložitější	složitý	k2eAgInSc1d3	nejsložitější
a	a	k8xC	a
největší	veliký	k2eAgInSc1d3	veliký
kloub	kloub	k1gInSc1	kloub
v	v	k7c6	v
lidském	lidský	k2eAgNnSc6d1	lidské
těle	tělo	k1gNnSc6	tělo
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
spojuje	spojovat	k5eAaImIp3nS	spojovat
stehenní	stehenní	k2eAgFnSc4d1	stehenní
<g/>
,	,	kIx,	,
holenní	holenní	k2eAgFnSc4d1	holenní
kost	kost	k1gFnSc4	kost
a	a	k8xC	a
čéšku	čéška	k1gFnSc4	čéška
<g/>
?	?	kIx.	?
</s>
