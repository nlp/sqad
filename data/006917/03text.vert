<s>
Koleno	koleno	k1gNnSc1	koleno
či	či	k8xC	či
kolenní	kolenní	k2eAgInSc1d1	kolenní
kloub	kloub	k1gInSc1	kloub
(	(	kIx(	(
<g/>
articulatio	articulatio	k1gMnSc1	articulatio
genus	genus	k1gMnSc1	genus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
nejsložitější	složitý	k2eAgInSc4d3	nejsložitější
kloub	kloub	k1gInSc4	kloub
v	v	k7c6	v
lidském	lidský	k2eAgNnSc6d1	lidské
těle	tělo	k1gNnSc6	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Spojuje	spojovat	k5eAaImIp3nS	spojovat
stehenní	stehenní	k2eAgFnSc4d1	stehenní
kost	kost	k1gFnSc4	kost
<g/>
,	,	kIx,	,
holenní	holenní	k2eAgFnSc4d1	holenní
kost	kost	k1gFnSc4	kost
a	a	k8xC	a
největší	veliký	k2eAgFnSc4d3	veliký
sezamskou	sezamský	k2eAgFnSc4d1	sezamská
kost	kost	k1gFnSc4	kost
těla	tělo	k1gNnSc2	tělo
čéšku	čéška	k1gFnSc4	čéška
<g/>
.	.	kIx.	.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
pohyb	pohyb	k1gInSc1	pohyb
dolní	dolní	k2eAgFnSc2d1	dolní
části	část	k1gFnSc2	část
nohy	noha	k1gFnSc2	noha
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
chůzi	chůze	k1gFnSc4	chůze
<g/>
.	.	kIx.	.
</s>
<s>
Kolenní	kolenní	k2eAgInSc1d1	kolenní
kloub	kloub	k1gInSc1	kloub
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
v	v	k7c6	v
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
z	z	k7c2	z
kloubní	kloubní	k2eAgFnSc2d1	kloubní
plochy	plocha	k1gFnSc2	plocha
stehenní	stehenní	k2eAgFnSc2d1	stehenní
kosti	kost	k1gFnSc2	kost
(	(	kIx(	(
<g/>
femuru	femur	k1gInSc2	femur
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
dvou	dva	k4xCgInPc2	dva
výběžků	výběžek	k1gInPc2	výběžek
(	(	kIx(	(
<g/>
kondylů	kondyl	k1gInPc2	kondyl
<g/>
)	)	kIx)	)
působí	působit	k5eAaImIp3nS	působit
jako	jako	k9	jako
dvojitá	dvojitý	k2eAgFnSc1d1	dvojitá
hlavice	hlavice	k1gFnSc1	hlavice
kloubu	kloub	k1gInSc2	kloub
<g/>
,	,	kIx,	,
ve	v	k7c6	v
spodní	spodní	k2eAgFnSc6d1	spodní
části	část	k1gFnSc6	část
pak	pak	k6eAd1	pak
z	z	k7c2	z
kloubní	kloubní	k2eAgFnSc2d1	kloubní
plochy	plocha	k1gFnSc2	plocha
holenní	holenní	k2eAgFnSc2d1	holenní
kosti	kost	k1gFnSc2	kost
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
tibia	tibia	k1gFnSc1	tibia
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
mělkou	mělký	k2eAgFnSc4d1	mělká
dvojitou	dvojitý	k2eAgFnSc4d1	dvojitá
kloubní	kloubní	k2eAgFnSc4d1	kloubní
jamku	jamka	k1gFnSc4	jamka
<g/>
.	.	kIx.	.
</s>
<s>
Tvar	tvar	k1gInSc1	tvar
a	a	k8xC	a
stabilita	stabilita	k1gFnSc1	stabilita
kloubní	kloubní	k2eAgFnSc2d1	kloubní
jamky	jamka	k1gFnSc2	jamka
je	být	k5eAaImIp3nS	být
doplněna	doplnit	k5eAaPmNgFnS	doplnit
dvěma	dva	k4xCgInPc7	dva
postranními	postranní	k2eAgInPc7d1	postranní
poloměsíčitými	poloměsíčitý	k2eAgInPc7d1	poloměsíčitý
chrupavčitými	chrupavčitý	k2eAgInPc7d1	chrupavčitý
útvary	útvar	k1gInPc7	útvar
-	-	kIx~	-
menisky	meniskus	k1gInPc7	meniskus
(	(	kIx(	(
<g/>
vnitřním	vnitřní	k2eAgInSc6d1	vnitřní
-	-	kIx~	-
mediálním	mediální	k2eAgInSc6d1	mediální
a	a	k8xC	a
vnějším	vnější	k2eAgInSc6d1	vnější
-	-	kIx~	-
laterálním	laterální	k2eAgInSc6d1	laterální
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kloub	kloub	k1gInSc1	kloub
je	být	k5eAaImIp3nS	být
dále	daleko	k6eAd2	daleko
stabilizován	stabilizovat	k5eAaBmNgInS	stabilizovat
vazivovým	vazivový	k2eAgInSc7d1	vazivový
aparátem	aparát	k1gInSc7	aparát
<g/>
:	:	kIx,	:
zkřížené	zkřížený	k2eAgInPc1d1	zkřížený
vazy	vaz	k1gInPc1	vaz
-	-	kIx~	-
přední	přední	k2eAgInSc1d1	přední
zkřížený	zkřížený	k2eAgInSc1d1	zkřížený
vaz	vaz	k1gInSc1	vaz
(	(	kIx(	(
<g/>
ligamentum	ligamentum	k1gNnSc1	ligamentum
cruciatum	cruciatum	k1gNnSc1	cruciatum
anterius	anterius	k1gInSc1	anterius
<g/>
)	)	kIx)	)
a	a	k8xC	a
zadní	zadní	k2eAgInSc1d1	zadní
zkřížený	zkřížený	k2eAgInSc1d1	zkřížený
vaz	vaz	k1gInSc1	vaz
(	(	kIx(	(
<g/>
ligamentum	ligamentum	k1gNnSc1	ligamentum
cruciatum	cruciatum	k1gNnSc1	cruciatum
posterius	posterius	k1gInSc1	posterius
<g/>
)	)	kIx)	)
vazy	vaz	k1gInPc1	vaz
kloubního	kloubní	k2eAgNnSc2d1	kloubní
pouzdra	pouzdro	k1gNnSc2	pouzdro
-	-	kIx~	-
především	především	k9	především
postranními	postranní	k2eAgInPc7d1	postranní
kolenními	kolenní	k2eAgInPc7d1	kolenní
vazy	vaz	k1gInPc7	vaz
a	a	k8xC	a
šlachou	šlacha	k1gFnSc7	šlacha
čtyřhlavého	čtyřhlavý	k2eAgInSc2d1	čtyřhlavý
stehenního	stehenní	k2eAgInSc2d1	stehenní
svalu	sval	k1gInSc2	sval
v	v	k7c6	v
přední	přední	k2eAgFnSc6d1	přední
části	část	k1gFnSc6	část
Další	další	k2eAgFnSc4d1	další
stabilizaci	stabilizace	k1gFnSc4	stabilizace
poskytují	poskytovat	k5eAaImIp3nP	poskytovat
šlachy	šlacha	k1gFnPc1	šlacha
okolních	okolní	k2eAgInPc2d1	okolní
svalů	sval	k1gInPc2	sval
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
napětí	napětí	k1gNnSc2	napětí
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
kloubního	kloubní	k2eAgNnSc2d1	kloubní
pouzdra	pouzdro	k1gNnSc2	pouzdro
je	být	k5eAaImIp3nS	být
synoviální	synoviální	k2eAgFnSc1d1	synoviální
tekutina	tekutina	k1gFnSc1	tekutina
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
snižuje	snižovat	k5eAaImIp3nS	snižovat
tření	tření	k1gNnSc4	tření
při	při	k7c6	při
pohybu	pohyb	k1gInSc6	pohyb
v	v	k7c6	v
kloubu	kloub	k1gInSc6	kloub
a	a	k8xC	a
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
transport	transport	k1gInSc4	transport
živin	živina	k1gFnPc2	živina
k	k	k7c3	k
chrupavčitým	chrupavčitý	k2eAgFnPc3d1	chrupavčitá
strukturám	struktura	k1gFnPc3	struktura
uvnitř	uvnitř	k7c2	uvnitř
<g/>
.	.	kIx.	.
</s>
<s>
Výchlipky	výchlipka	k1gFnPc1	výchlipka
kloubního	kloubní	k2eAgNnSc2d1	kloubní
pouzdra	pouzdro	k1gNnSc2	pouzdro
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
tvoří	tvořit	k5eAaImIp3nP	tvořit
takzvané	takzvaný	k2eAgFnPc1d1	takzvaná
burzy	burza	k1gFnPc1	burza
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
v	v	k7c6	v
místech	místo	k1gNnPc6	místo
tlaku	tlak	k1gInSc2	tlak
či	či	k8xC	či
tření	tření	k1gNnSc2	tření
působí	působit	k5eAaImIp3nS	působit
jako	jako	k8xS	jako
ochrana	ochrana	k1gFnSc1	ochrana
proti	proti	k7c3	proti
mechanickému	mechanický	k2eAgNnSc3d1	mechanické
namáhání	namáhání	k1gNnSc3	namáhání
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
přední	přední	k2eAgFnSc6d1	přední
části	část	k1gFnSc6	část
kolenního	kolenní	k2eAgInSc2d1	kolenní
kloubu	kloub	k1gInSc2	kloub
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
čéška	čéška	k1gFnSc1	čéška
(	(	kIx(	(
<g/>
patella	patella	k1gFnSc1	patella
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
sezamskou	sezamský	k2eAgFnSc4d1	sezamská
kost	kost	k1gFnSc4	kost
vzniklou	vzniklý	k2eAgFnSc7d1	vzniklá
osifikací	osifikace	k1gFnSc7	osifikace
části	část	k1gFnSc2	část
šlachy	šlacha	k1gFnSc2	šlacha
čtyřhlavého	čtyřhlavý	k2eAgInSc2d1	čtyřhlavý
stehenního	stehenní	k2eAgInSc2d1	stehenní
svalu	sval	k1gInSc2	sval
<g/>
.	.	kIx.	.
</s>
<s>
Úderem	úder	k1gInSc7	úder
na	na	k7c4	na
čéšku	čéška	k1gFnSc4	čéška
se	se	k3xPyFc4	se
v	v	k7c6	v
neurologii	neurologie	k1gFnSc6	neurologie
testuje	testovat	k5eAaImIp3nS	testovat
tzv.	tzv.	kA	tzv.
patelární	patelární	k2eAgInSc1d1	patelární
reflex	reflex	k1gInSc1	reflex
<g/>
.	.	kIx.	.
</s>
<s>
Koleno	koleno	k1gNnSc1	koleno
je	být	k5eAaImIp3nS	být
důležitý	důležitý	k2eAgInSc4d1	důležitý
kloub	kloub	k1gInSc4	kloub
v	v	k7c6	v
lidském	lidský	k2eAgNnSc6d1	lidské
těle	tělo	k1gNnSc6	tělo
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
vystaven	vystaven	k2eAgInSc1d1	vystaven
velké	velký	k2eAgFnSc3d1	velká
zátěži	zátěž	k1gFnSc3	zátěž
<g/>
.	.	kIx.	.
</s>
<s>
Velké	velký	k2eAgNnSc1d1	velké
zatížení	zatížení	k1gNnSc1	zatížení
může	moct	k5eAaImIp3nS	moct
často	často	k6eAd1	často
skončit	skončit	k5eAaPmF	skončit
i	i	k9	i
úrazem	úraz	k1gInSc7	úraz
<g/>
.	.	kIx.	.
</s>
<s>
Následné	následný	k2eAgNnSc1d1	následné
léčení	léčení	k1gNnSc1	léčení
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
náročné	náročný	k2eAgNnSc1d1	náročné
a	a	k8xC	a
zdlouhavé	zdlouhavý	k2eAgNnSc1d1	zdlouhavé
a	a	k8xC	a
často	často	k6eAd1	často
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
trvalému	trvalý	k2eAgNnSc3d1	trvalé
poškození	poškození	k1gNnSc3	poškození
kloubu	kloub	k1gInSc2	kloub
<g/>
.	.	kIx.	.
</s>
<s>
Vyšetření	vyšetření	k1gNnSc1	vyšetření
kolenního	kolenní	k2eAgInSc2d1	kolenní
kloubu	kloub	k1gInSc2	kloub
endoskopickými	endoskopický	k2eAgFnPc7d1	endoskopická
metodami	metoda	k1gFnPc7	metoda
se	se	k3xPyFc4	se
odborně	odborně	k6eAd1	odborně
nazývá	nazývat	k5eAaImIp3nS	nazývat
artroskopie	artroskopie	k1gFnSc1	artroskopie
<g/>
.	.	kIx.	.
</s>
<s>
Poškození	poškození	k1gNnSc1	poškození
kolena	koleno	k1gNnSc2	koleno
může	moct	k5eAaImIp3nS	moct
znamenat	znamenat	k5eAaImF	znamenat
i	i	k9	i
trvalé	trvalý	k2eAgFnPc1d1	trvalá
komplikace	komplikace	k1gFnPc1	komplikace
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Koleno	koleno	k1gNnSc4	koleno
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
koleno	koleno	k1gNnSc4	koleno
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
