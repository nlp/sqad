<s>
Jean	Jean	k1gMnSc1	Jean
Arthur	Arthur	k1gMnSc1	Arthur
Nicolas	Nicolas	k1gMnSc1	Nicolas
Rimbaud	Rimbaud	k1gMnSc1	Rimbaud
[	[	kIx(	[
<g/>
rembo	remba	k1gFnSc5	remba
<g/>
]	]	kIx)	]
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1854	[number]	k4	1854
Charleville-Méziè	Charleville-Méziè	k1gFnPc2	Charleville-Méziè
-	-	kIx~	-
10	[number]	k4	10
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1891	[number]	k4	1891
Marseille	Marseille	k1gFnSc1	Marseille
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
francouzský	francouzský	k2eAgMnSc1d1	francouzský
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgMnPc2d3	nejvýznamnější
tzv.	tzv.	kA	tzv.
prokletých	prokletý	k2eAgMnPc2d1	prokletý
básníků	básník	k1gMnPc2	básník
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Charleville-Méziè	Charleville-Méziè	k1gFnSc6	Charleville-Méziè
v	v	k7c6	v
departmentu	department	k1gInSc6	department
Ardennes	Ardennes	k1gInSc1	Ardennes
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
byl	být	k5eAaImAgMnS	být
kapitán	kapitán	k1gMnSc1	kapitán
pěchoty	pěchota	k1gFnSc2	pěchota
<g/>
,	,	kIx,	,
dobrodruh	dobrodruh	k1gMnSc1	dobrodruh
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
nakonec	nakonec	k6eAd1	nakonec
rodinu	rodina	k1gFnSc4	rodina
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Rimbaud	Rimbaud	k1gMnSc1	Rimbaud
byl	být	k5eAaImAgMnS	být
velmi	velmi	k6eAd1	velmi
nepřizpůsobivý	přizpůsobivý	k2eNgMnSc1d1	nepřizpůsobivý
a	a	k8xC	a
na	na	k7c4	na
výchovu	výchova	k1gFnSc4	výchova
své	svůj	k3xOyFgFnSc2	svůj
konzervativní	konzervativní	k2eAgFnSc2d1	konzervativní
a	a	k8xC	a
nábožensky	nábožensky	k6eAd1	nábožensky
bigotní	bigotní	k2eAgFnSc2d1	bigotní
matky	matka	k1gFnSc2	matka
reagoval	reagovat	k5eAaBmAgInS	reagovat
výbuchy	výbuch	k1gInPc4	výbuch
hněvu	hněv	k1gInSc2	hněv
a	a	k8xC	a
útěky	útěk	k1gInPc4	útěk
z	z	k7c2	z
domova	domov	k1gInSc2	domov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
pařížské	pařížský	k2eAgFnSc2d1	Pařížská
komuny	komuna	k1gFnSc2	komuna
přišel	přijít	k5eAaPmAgInS	přijít
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
Verlainem	Verlain	k1gInSc7	Verlain
<g/>
.	.	kIx.	.
</s>
<s>
Verlaine	Verlainout	k5eAaPmIp3nS	Verlainout
kvůli	kvůli	k7c3	kvůli
němu	on	k3xPp3gMnSc3	on
rozbil	rozbít	k5eAaPmAgInS	rozbít
manželství	manželství	k1gNnSc4	manželství
<g/>
.	.	kIx.	.
</s>
<s>
Začali	začít	k5eAaPmAgMnP	začít
spolu	spolu	k6eAd1	spolu
žít	žít	k5eAaImF	žít
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
nejen	nejen	k6eAd1	nejen
jako	jako	k9	jako
přátelé	přítel	k1gMnPc1	přítel
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
jako	jako	k9	jako
milenci	milenec	k1gMnPc1	milenec
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gInSc1	jejich
život	život	k1gInSc1	život
byl	být	k5eAaImAgInS	být
velmi	velmi	k6eAd1	velmi
bohémský	bohémský	k2eAgInSc1d1	bohémský
(	(	kIx(	(
<g/>
žili	žít	k5eAaImAgMnP	žít
nejprve	nejprve	k6eAd1	nejprve
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
a	a	k8xC	a
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rozchodu	rozchod	k1gInSc6	rozchod
s	s	k7c7	s
Verlainem	Verlain	k1gInSc7	Verlain
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1873	[number]	k4	1873
Verlaine	Verlain	k1gInSc5	Verlain
Arthura	Arthura	k1gFnSc1	Arthura
postřelil	postřelit	k5eAaPmAgMnS	postřelit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
zničit	zničit	k5eAaPmF	zničit
své	svůj	k3xOyFgNnSc4	svůj
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc4	který
měl	mít	k5eAaImAgMnS	mít
Verlaine	Verlain	k1gInSc5	Verlain
naštěstí	naštěstí	k6eAd1	naštěstí
u	u	k7c2	u
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Arthur	Arthur	k1gMnSc1	Arthur
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
skončil	skončit	k5eAaPmAgMnS	skončit
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
,	,	kIx,	,
své	svůj	k3xOyFgNnSc4	svůj
dílo	dílo	k1gNnSc4	dílo
odsoudil	odsoudit	k5eAaPmAgInS	odsoudit
<g/>
,	,	kIx,	,
přestal	přestat	k5eAaPmAgInS	přestat
psát	psát	k5eAaImF	psát
a	a	k8xC	a
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
koloniálního	koloniální	k2eAgNnSc2d1	koloniální
vojska	vojsko	k1gNnSc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Jávě	Jáva	k1gFnSc6	Jáva
dezertoval	dezertovat	k5eAaBmAgMnS	dezertovat
<g/>
,	,	kIx,	,
s	s	k7c7	s
cirkusem	cirkus	k1gInSc7	cirkus
prošel	projít	k5eAaPmAgMnS	projít
Švédsko	Švédsko	k1gNnSc4	Švédsko
a	a	k8xC	a
Norsko	Norsko	k1gNnSc4	Norsko
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1880	[number]	k4	1880
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
na	na	k7c6	na
Kypru	Kypr	k1gInSc6	Kypr
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgMnS	vydat
jako	jako	k9	jako
obchodní	obchodní	k2eAgMnSc1d1	obchodní
zástupce	zástupce	k1gMnSc1	zástupce
do	do	k7c2	do
Adenu	Aden	k1gInSc2	Aden
<g/>
.	.	kIx.	.
</s>
<s>
Přešel	přejít	k5eAaPmAgInS	přejít
celou	celý	k2eAgFnSc4d1	celá
somálskou	somálský	k2eAgFnSc4d1	Somálská
poušť	poušť	k1gFnSc4	poušť
<g/>
.	.	kIx.	.
</s>
<s>
Naučil	naučit	k5eAaPmAgMnS	naučit
se	se	k3xPyFc4	se
mnoho	mnoho	k4c1	mnoho
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
zemřel	zemřít	k5eAaPmAgMnS	zemřít
na	na	k7c4	na
rakovinu	rakovina	k1gFnSc4	rakovina
(	(	kIx(	(
<g/>
měl	mít	k5eAaImAgInS	mít
zhoubný	zhoubný	k2eAgInSc1d1	zhoubný
nádor	nádor	k1gInSc1	nádor
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
koleni	kolit	k5eAaImNgMnP	kolit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
pochován	pochovat	k5eAaPmNgInS	pochovat
na	na	k7c6	na
hřbitově	hřbitov	k1gInSc6	hřbitov
'	'	kIx"	'
<g/>
Cimetiè	Cimetiè	k1gMnSc1	Cimetiè
de	de	k?	de
Charleville-Méziè	Charleville-Méziè	k1gMnSc1	Charleville-Méziè
<g/>
'	'	kIx"	'
<g/>
.	.	kIx.	.
</s>
<s>
Rimbaud	Rimbaud	k6eAd1	Rimbaud
v	v	k7c6	v
poezii	poezie	k1gFnSc6	poezie
změnil	změnit	k5eAaPmAgMnS	změnit
strukturu	struktura	k1gFnSc4	struktura
a	a	k8xC	a
metodu	metoda	k1gFnSc4	metoda
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
poezie	poezie	k1gFnSc1	poezie
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
subjektivních	subjektivní	k2eAgInPc6d1	subjektivní
prožitcích	prožitek	k1gInPc6	prožitek
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
pocitu	pocit	k1gInSc2	pocit
rozporu	rozpor	k1gInSc2	rozpor
mezi	mezi	k7c7	mezi
umělcem	umělec	k1gMnSc7	umělec
a	a	k8xC	a
společností	společnost	k1gFnSc7	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnPc4	jeho
básně	báseň	k1gFnPc4	báseň
je	být	k5eAaImIp3nS	být
typická	typický	k2eAgFnSc1d1	typická
bohatá	bohatý	k2eAgFnSc1d1	bohatá
fantazie	fantazie	k1gFnSc1	fantazie
<g/>
,	,	kIx,	,
odpor	odpor	k1gInSc1	odpor
k	k	k7c3	k
měšťákům	měšťák	k1gMnPc3	měšťák
a	a	k8xC	a
náboženství	náboženství	k1gNnSc2	náboženství
a	a	k8xC	a
obdiv	obdiv	k1gInSc1	obdiv
k	k	k7c3	k
revoluci	revoluce	k1gFnSc3	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
poezie	poezie	k1gFnSc1	poezie
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
základem	základ	k1gInSc7	základ
nové	nový	k2eAgFnSc2d1	nová
poetiky	poetika	k1gFnSc2	poetika
<g/>
.	.	kIx.	.
</s>
<s>
Odhaloval	odhalovat	k5eAaImAgMnS	odhalovat
iluzornost	iluzornost	k1gFnSc4	iluzornost
pravd	pravda	k1gFnPc2	pravda
uznávaných	uznávaný	k2eAgFnPc2d1	uznávaná
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
vystavil	vystavit	k5eAaPmAgInS	vystavit
pohrdavému	pohrdavý	k2eAgInSc3d1	pohrdavý
posměchu	posměch	k1gInSc3	posměch
<g/>
.	.	kIx.	.
</s>
<s>
Útoky	útok	k1gInPc4	útok
proti	proti	k7c3	proti
povrchnosti	povrchnost	k1gFnSc3	povrchnost
<g/>
,	,	kIx,	,
zbabělosti	zbabělost	k1gFnSc3	zbabělost
a	a	k8xC	a
všednosti	všednost	k1gFnSc3	všednost
společnosti	společnost	k1gFnSc2	společnost
postupně	postupně	k6eAd1	postupně
přerostly	přerůst	k5eAaPmAgInP	přerůst
v	v	k7c4	v
nenávistnou	nenávistný	k2eAgFnSc4d1	nenávistná
a	a	k8xC	a
totální	totální	k2eAgFnSc4d1	totální
revoltu	revolta	k1gFnSc4	revolta
proti	proti	k7c3	proti
všem	všecek	k3xTgFnPc3	všecek
hodnotám	hodnota	k1gFnPc3	hodnota
a	a	k8xC	a
tabu	tabu	k1gNnSc7	tabu
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Rimbaudovo	Rimbaudův	k2eAgNnSc4d1	Rimbaudovo
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
vycházející	vycházející	k2eAgNnSc4d1	vycházející
ze	z	k7c2	z
zloby	zloba	k1gFnSc2	zloba
<g/>
,	,	kIx,	,
z	z	k7c2	z
revolty	revolta	k1gFnSc2	revolta
a	a	k8xC	a
vnitřní	vnitřní	k2eAgFnSc2d1	vnitřní
síly	síla	k1gFnSc2	síla
a	a	k8xC	a
útočilo	útočit	k5eAaImAgNnS	útočit
na	na	k7c4	na
všechny	všechen	k3xTgFnPc4	všechen
formy	forma	k1gFnPc4	forma
tradice	tradice	k1gFnSc2	tradice
<g/>
,	,	kIx,	,
ovlivnilo	ovlivnit	k5eAaPmAgNnS	ovlivnit
moderní	moderní	k2eAgFnSc4d1	moderní
světovou	světový	k2eAgFnSc4d1	světová
poezii	poezie	k1gFnSc4	poezie
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
symbolisty	symbolista	k1gMnPc4	symbolista
a	a	k8xC	a
surrealisty	surrealista	k1gMnPc4	surrealista
<g/>
,	,	kIx,	,
beat	beat	k1gInSc1	beat
generation	generation	k1gInSc1	generation
a	a	k8xC	a
poetiku	poetika	k1gFnSc4	poetika
rockové	rockový	k2eAgFnSc2d1	rocková
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
jeho	on	k3xPp3gNnSc2	on
díla	dílo	k1gNnSc2	dílo
publikoval	publikovat	k5eAaBmAgMnS	publikovat
Paul	Paul	k1gMnSc1	Paul
Verlaine	Verlain	k1gInSc5	Verlain
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
asi	asi	k9	asi
osm	osm	k4xCc4	osm
jeho	jeho	k3xOp3gNnPc2	jeho
děl	dělo	k1gNnPc2	dělo
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
komunistické	komunistický	k2eAgFnSc2d1	komunistická
ústavy	ústava	k1gFnSc2	ústava
(	(	kIx(	(
<g/>
1871	[number]	k4	1871
<g/>
)	)	kIx)	)
Sezóna	sezóna	k1gFnSc1	sezóna
v	v	k7c6	v
pekle	peklo	k1gNnSc6	peklo
(	(	kIx(	(
<g/>
1873	[number]	k4	1873
<g/>
)	)	kIx)	)
-	-	kIx~	-
někdy	někdy	k6eAd1	někdy
též	též	k9	též
Pobyt	pobyt	k1gInSc1	pobyt
v	v	k7c6	v
pekle	peklo	k1gNnSc6	peklo
-	-	kIx~	-
básně	báseň	k1gFnPc1	báseň
v	v	k7c6	v
próze	próza	k1gFnSc6	próza
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
sbírka	sbírka	k1gFnSc1	sbírka
je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
jako	jako	k8xS	jako
vrchol	vrchol	k1gInSc1	vrchol
jeho	jeho	k3xOp3gFnSc2	jeho
tvorby	tvorba	k1gFnSc2	tvorba
<g/>
.	.	kIx.	.
</s>
<s>
Relikvier	Relikvier	k1gInSc1	Relikvier
(	(	kIx(	(
<g/>
1891	[number]	k4	1891
<g/>
)	)	kIx)	)
Dále	daleko	k6eAd2	daleko
pod	pod	k7c7	pod
jeho	jeho	k3xOp3gNnSc7	jeho
jménem	jméno	k1gNnSc7	jméno
vyšly	vyjít	k5eAaPmAgFnP	vyjít
dvě	dva	k4xCgFnPc1	dva
sbírky	sbírka	k1gFnPc1	sbírka
sebrané	sebraný	k2eAgFnPc1d1	sebraná
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gMnPc2	jeho
přátel	přítel	k1gMnPc2	přítel
<g/>
:	:	kIx,	:
Básně	báseň	k1gFnSc2	báseň
Iluminace	iluminace	k1gFnSc2	iluminace
-	-	kIx~	-
soubor	soubor	k1gInSc1	soubor
básní	báseň	k1gFnPc2	báseň
v	v	k7c6	v
próze	próza	k1gFnSc6	próza
i	i	k8xC	i
ve	v	k7c6	v
verších	verš	k1gInPc6	verš
Opilý	opilý	k2eAgInSc4d1	opilý
koráb	koráb	k1gInSc4	koráb
-	-	kIx~	-
básníkova	básníkův	k2eAgFnSc1d1	básníkova
nejznámější	známý	k2eAgFnSc1d3	nejznámější
básnická	básnický	k2eAgFnSc1d1	básnická
skladba	skladba	k1gFnSc1	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Básník	básník	k1gMnSc1	básník
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
přirovnává	přirovnávat	k5eAaImIp3nS	přirovnávat
ke	k	k7c3	k
korábu	koráb	k1gInSc3	koráb
<g/>
.	.	kIx.	.
</s>
<s>
Loď	loď	k1gFnSc1	loď
na	na	k7c6	na
rozbouřeném	rozbouřený	k2eAgNnSc6d1	rozbouřené
moři	moře	k1gNnSc6	moře
je	být	k5eAaImIp3nS	být
samotný	samotný	k2eAgMnSc1d1	samotný
autor	autor	k1gMnSc1	autor
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
pluje	plout	k5eAaImIp3nS	plout
mezi	mezi	k7c7	mezi
životem	život	k1gInSc7	život
a	a	k8xC	a
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Individuum	individuum	k1gNnSc1	individuum
se	se	k3xPyFc4	se
odklání	odklánět	k5eAaImIp3nS	odklánět
do	do	k7c2	do
říše	říš	k1gFnSc2	říš
fantazie	fantazie	k1gFnSc2	fantazie
<g/>
,	,	kIx,	,
putuje	putovat	k5eAaImIp3nS	putovat
říší	říš	k1gFnSc7	říš
<g/>
.	.	kIx.	.
</s>
<s>
Spáč	spáč	k1gMnSc1	spáč
v	v	k7c6	v
úvalu	úval	k1gInSc6	úval
-	-	kIx~	-
vylíčení	vylíčení	k1gNnSc1	vylíčení
přírody	příroda	k1gFnSc2	příroda
a	a	k8xC	a
mrtvého	mrtvý	k2eAgMnSc2d1	mrtvý
vojáka	voják	k1gMnSc2	voják
<g/>
,	,	kIx,	,
ležícího	ležící	k2eAgMnSc2d1	ležící
v	v	k7c6	v
úvalu	úval	k1gInSc6	úval
<g/>
,	,	kIx,	,
s	s	k7c7	s
prostřeleným	prostřelený	k2eAgInSc7d1	prostřelený
bokem	bok	k1gInSc7	bok
Cestou	cesta	k1gFnSc7	cesta
bez	bez	k7c2	bez
konce	konec	k1gInSc2	konec
Verše	verš	k1gInSc2	verš
<g/>
.	.	kIx.	.
</s>
<s>
Přebásnil	přebásnit	k5eAaPmAgMnS	přebásnit
Vítězslav	Vítězslav	k1gMnSc1	Vítězslav
Nezval	Nezval	k1gMnSc1	Nezval
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
1956	[number]	k4	1956
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
