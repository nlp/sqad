<s>
Jan	Jan	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bretaňský	bretaňský	k2eAgInSc1d1
</s>
<s>
Jan	Jan	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bretaňský	bretaňský	k2eAgInSc4d1
Narození	narození	k1gNnSc1
</s>
<s>
1339	#num#	k4
<g/>
Bretaňské	bretaňský	k2eAgFnPc4d1
vévodství	vévodství	k1gNnSc4
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1399	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
59	#num#	k4
<g/>
–	–	k?
<g/>
60	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Nantes	Nantes	k1gInSc1
Místo	místo	k7c2
pohřbení	pohřbení	k1gNnSc2
</s>
<s>
Katedrála	katedrála	k1gFnSc1
svatého	svatý	k2eAgMnSc2d1
Petra	Petr	k1gMnSc2
a	a	k8xC
Pavla	Pavel	k1gMnSc2
Povolání	povolání	k1gNnSc2
</s>
<s>
panovník	panovník	k1gMnSc1
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Marie	Marie	k1gFnSc1
Anglická	anglický	k2eAgFnSc1d1
(	(	kIx(
<g/>
1355	#num#	k4
<g/>
–	–	k?
<g/>
1361	#num#	k4
<g/>
)	)	kIx)
<g/>
Joan	Joan	k1gNnSc1
Holland	Hollanda	k1gFnPc2
<g/>
,	,	kIx,
Duchess	Duchess	k1gInSc4
of	of	k?
Brittany	Brittan	k1gInPc4
(	(	kIx(
<g/>
1366	#num#	k4
<g/>
–	–	k?
<g/>
1384	#num#	k4
<g/>
)	)	kIx)
<g/>
Jana	Jana	k1gFnSc1
Navarrská	navarrský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1386	#num#	k4
<g/>
–	–	k?
<g/>
1399	#num#	k4
<g/>
)	)	kIx)
Děti	dítě	k1gFnPc1
</s>
<s>
Jan	Jan	k1gMnSc1
V.	V.	kA
BretaňskýArthur	BretaňskýArthur	k1gMnSc1
III	III	kA
<g/>
,	,	kIx,
Duke	Duke	k1gNnSc2
of	of	k?
BrittanyRichard	BrittanyRichard	k1gMnSc1
BretaňskýMarie	BretaňskýMarie	k1gFnSc2
BretaňskáMargaret	BretaňskáMargaret	k1gMnSc1
of	of	k?
BritainBlanche	BritainBlanche	k1gInSc1
of	of	k?
BrittannyJana	BrittannyJan	k1gMnSc2
BretaňskáIsabelle	BretaňskáIsabell	k1gMnSc2
BretaňskáGilles	BretaňskáGilles	k1gMnSc1
Bretaňský	bretaňský	k2eAgMnSc1d1
Rodiče	rodič	k1gMnPc1
</s>
<s>
Jan	Jan	k1gMnSc1
z	z	k7c2
Montfortu	Montfort	k1gInSc2
a	a	k8xC
Johana	Johana	k1gFnSc1
Flanderská	flanderský	k2eAgFnSc1d1
Rod	rod	k1gInSc4
</s>
<s>
Montfort	Montfort	k1gInSc4
of	of	k?
Brittany	Brittan	k1gInPc4
Příbuzní	příbuzný	k1gMnPc1
</s>
<s>
Joan	Joan	k1gMnSc1
of	of	k?
Brittany	Brittan	k1gInPc1
<g/>
,	,	kIx,
Baroness	Baroness	k1gInSc1
of	of	k?
Drayton	Drayton	k1gInSc1
(	(	kIx(
<g/>
sourozenec	sourozenec	k1gMnSc1
<g/>
)	)	kIx)
<g/>
Isabela	Isabela	k1gFnSc1
Bretaňská	bretaňský	k2eAgFnSc1d1
<g/>
,	,	kIx,
František	František	k1gMnSc1
I.	I.	kA
Bretaňský	bretaňský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
II	II	kA
<g/>
,	,	kIx,
Duke	Duke	k1gFnSc1
of	of	k?
Brittany	Brittan	k1gInPc1
a	a	k8xC
Gilles	Gilles	k1gInSc1
de	de	k?
Bretagne	Bretagne	k1gFnPc1
(	(	kIx(
<g/>
vnoučata	vnouče	k1gNnPc1
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jan	Jan	k1gMnSc1
IV	IV	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bretaňský	bretaňský	k2eAgInSc1d1
(	(	kIx(
<g/>
bretonsky	bretonsky	k6eAd1
Yann	Yann	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
francouzsky	francouzsky	k6eAd1
Jean	Jean	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
1339	#num#	k4
-	-	kIx~
1	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
1399	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
od	od	k7c2
roku	rok	k1gInSc2
1345	#num#	k4
bretaňským	bretaňský	k2eAgMnSc7d1
vévodou	vévoda	k1gMnSc7
a	a	k8xC
hrabětem	hrabě	k1gMnSc7
z	z	k7c2
Montfort	Montfort	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1372	#num#	k4
hrabětem	hrabě	k1gMnSc7
z	z	k7c2
Richmondu	Richmond	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Počítání	počítání	k1gNnSc1
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS
se	se	k3xPyFc4
jako	jako	k9
syn	syn	k1gMnSc1
Jana	Jan	k1gMnSc2
z	z	k7c2
Montfortu	Montfort	k1gInSc2
a	a	k8xC
Johany	Johana	k1gFnSc2
Flanderské	flanderský	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Otec	otec	k1gMnSc1
si	se	k3xPyFc3
nárokoval	nárokovat	k5eAaImAgMnS
titul	titul	k1gInSc4
bretaňského	bretaňský	k2eAgMnSc2d1
vévody	vévoda	k1gMnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
nebyl	být	k5eNaImAgMnS
schopen	schopen	k2eAgMnSc1d1
si	se	k3xPyFc3
nárok	nárok	k1gInSc4
udržet	udržet	k5eAaPmF
po	po	k7c4
delší	dlouhý	k2eAgFnSc4d2
dobu	doba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
otcův	otcův	k2eAgInSc1d1
nárok	nárok	k1gInSc1
byl	být	k5eAaImAgInS
sporný	sporný	k2eAgInSc1d1
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
Jan	Jan	k1gMnSc1
počítán	počítat	k5eAaImNgMnS
jako	jako	k9
Jan	Jan	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
jeho	jeho	k3xOp3gMnSc1
otec	otec	k1gMnSc1
byl	být	k5eAaImAgMnS
jednoduše	jednoduše	k6eAd1
"	"	kIx"
<g/>
Jan	Jan	k1gMnSc1
z	z	k7c2
Montfortu	Montfort	k1gInSc2
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Angličtí	anglický	k2eAgMnPc1d1
historici	historik	k1gMnPc1
ho	on	k3xPp3gNnSc4
však	však	k9
označují	označovat	k5eAaImIp3nP
jako	jako	k8xS,k8xC
Jana	Jan	k1gMnSc4
V.	V.	kA
<g/>
,	,	kIx,
protože	protože	k8xS
anglický	anglický	k2eAgMnSc1d1
král	král	k1gMnSc1
titul	titul	k1gInSc4
jeho	jeho	k3xOp3gMnSc2,k3xPp3gMnSc2
otce	otec	k1gMnSc2
uznal	uznat	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
Francii	Francie	k1gFnSc6
je	být	k5eAaImIp3nS
znám	znám	k2eAgMnSc1d1
jako	jako	k8xS,k8xC
Jan	Jan	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
protože	protože	k8xS
francouzský	francouzský	k2eAgMnSc1d1
panovník	panovník	k1gMnSc1
<g/>
,	,	kIx,
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
anglického	anglický	k2eAgMnSc2d1
<g/>
,	,	kIx,
otcovy	otcův	k2eAgInPc4d1
tituly	titul	k1gInPc4
nikdy	nikdy	k6eAd1
neuznal	uznat	k5eNaPmAgInS
<g/>
.	.	kIx.
</s>
<s>
Dobytí	dobytí	k1gNnSc1
</s>
<s>
První	první	k4xOgFnSc1
část	část	k1gFnSc1
jeho	jeho	k3xOp3gFnSc2
vlády	vláda	k1gFnSc2
byla	být	k5eAaImAgFnS
poznamenána	poznamenat	k5eAaPmNgFnS
válkou	válka	k1gFnSc7
o	o	k7c4
bretaňské	bretaňský	k2eAgNnSc4d1
dědictví	dědictví	k1gNnSc4
<g/>
,	,	kIx,
boji	boj	k1gInSc6
jeho	on	k3xPp3gMnSc2,k3xOp3gMnSc2
otce	otec	k1gMnSc2
proti	proti	k7c3
sestřenici	sestřenice	k1gFnSc3
Johaně	Johana	k1gFnSc3
z	z	k7c2
Penthiè	Penthiè	k1gInSc5
a	a	k8xC
jejímu	její	k3xOp3gMnSc3
manželovi	manžel	k1gMnSc3
Karlovi	Karel	k1gMnSc3
z	z	k7c2
Blois	Blois	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
vojenskou	vojenský	k2eAgFnSc7d1
podporou	podpora	k1gFnSc7
Francie	Francie	k1gFnSc2
byl	být	k5eAaImAgMnS
Karel	Karel	k1gMnSc1
schopen	schopen	k2eAgMnSc1d1
řídit	řídit	k5eAaImF
většinu	většina	k1gFnSc4
Bretaně	Bretaň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
otcově	otcův	k2eAgFnSc6d1
smrti	smrt	k1gFnSc6
<g/>
,	,	kIx,
se	se	k3xPyFc4
Janova	Janův	k2eAgFnSc1d1
matka	matka	k1gFnSc1
Johana	Johana	k1gFnSc1
pokoušela	pokoušet	k5eAaImAgFnS
pokračovat	pokračovat	k5eAaImF
ve	v	k7c6
válce	válka	k1gFnSc6
ve	v	k7c6
jménu	jméno	k1gNnSc6
svého	své	k1gNnSc2
synka	synek	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stala	stát	k5eAaPmAgFnS
se	se	k3xPyFc4
známá	známý	k2eAgFnSc1d1
jako	jako	k8xS,k8xC
"	"	kIx"
<g/>
Jeanne	Jeann	k1gInSc5
La	la	k1gNnSc3
Flamme	Flamme	k1gFnSc1
<g/>
"	"	kIx"
(	(	kIx(
<g/>
Ohnivá	ohnivý	k2eAgFnSc1d1
Johana	Johana	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
pro	pro	k7c4
její	její	k3xOp3gFnSc4
ohnivou	ohnivý	k2eAgFnSc4d1
povahu	povaha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
nakonec	nakonec	k6eAd1
nucena	nutit	k5eAaImNgFnS
i	i	k9
se	s	k7c7
svým	svůj	k3xOyFgMnSc7
synem	syn	k1gMnSc7
ustoupit	ustoupit	k5eAaPmF
do	do	k7c2
Anglie	Anglie	k1gFnSc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
požádala	požádat	k5eAaPmAgFnS
o	o	k7c4
pomoc	pomoc	k1gFnSc4
Eduarda	Eduard	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
byla	být	k5eAaImAgFnS
prohlášena	prohlásit	k5eAaPmNgFnS
za	za	k7c7
duševně	duševně	k6eAd1
nemocnou	nemocný	k2eAgFnSc7d1,k2eNgFnSc7d1
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1343	#num#	k4
uvězněna	uvěznit	k5eAaPmNgFnS
na	na	k7c6
hradě	hrad	k1gInSc6
Tickhill	Tickhill	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jan	Jana	k1gFnPc2
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
sestra	sestra	k1gFnSc1
Johana	Johan	k1gMnSc4
byli	být	k5eAaImAgMnP
poté	poté	k6eAd1
přijati	přijmout	k5eAaPmNgMnP
do	do	k7c2
královy	králův	k2eAgFnSc2d1
domácnosti	domácnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jan	Jan	k1gMnSc1
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
do	do	k7c2
Bretaně	Bretaň	k1gFnSc2
prosadit	prosadit	k5eAaPmF
svůj	svůj	k3xOyFgInSc4
nárok	nárok	k1gInSc4
<g/>
,	,	kIx,
tentokrát	tentokrát	k6eAd1
s	s	k7c7
anglickou	anglický	k2eAgFnSc7d1
pomocí	pomoc	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1364	#num#	k4
<g/>
,	,	kIx,
se	se	k3xPyFc4
Janovi	Jan	k1gMnSc3
podařilo	podařit	k5eAaPmAgNnS
zvítězit	zvítězit	k5eAaPmF
proti	proti	k7c3
rodu	rod	k1gInSc3
Blois	Blois	k1gFnSc2
v	v	k7c6
bitvě	bitva	k1gFnSc6
u	u	k7c2
Auray	Auraa	k1gFnSc2
<g/>
,	,	kIx,
s	s	k7c7
podporou	podpora	k1gFnSc7
anglické	anglický	k2eAgFnSc2d1
armády	armáda	k1gFnSc2
vedené	vedený	k2eAgFnSc2d1
Janem	Jan	k1gMnSc7
Chandosem	Chandos	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
nepřítel	nepřítel	k1gMnSc1
Karel	Karel	k1gMnSc1
byl	být	k5eAaImAgMnS
zabit	zabít	k5eAaPmNgMnS
v	v	k7c6
bitvě	bitva	k1gFnSc6
a	a	k8xC
Karlova	Karlův	k2eAgFnSc1d1
vdova	vdova	k1gFnSc1
Johana	Johan	k1gMnSc2
byla	být	k5eAaImAgFnS
dne	den	k1gInSc2
12	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1365	#num#	k4
nucena	nucen	k2eAgFnSc1d1
podepsat	podepsat	k5eAaPmF
Smlouvu	smlouva	k1gFnSc4
u	u	k7c2
Guérande	Guérand	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
podmínkách	podmínka	k1gFnPc6
smlouvy	smlouva	k1gFnSc2
<g/>
,	,	kIx,
se	se	k3xPyFc4
Johana	Johana	k1gFnSc1
vzdala	vzdát	k5eAaPmAgFnS
práva	právo	k1gNnPc4
na	na	k7c4
Bretaň	Bretaň	k1gFnSc4
a	a	k8xC
Jan	Jan	k1gMnSc1
byl	být	k5eAaImAgMnS
uznán	uznat	k5eAaPmNgMnS
jako	jako	k8xC,k8xS
jediný	jediný	k2eAgMnSc1d1
vládce	vládce	k1gMnSc1
vévodství	vévodství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Boje	boj	k1gInPc1
o	o	k7c4
moc	moc	k1gFnSc4
</s>
<s>
Po	po	k7c6
dosažení	dosažení	k1gNnSc6
vítězství	vítězství	k1gNnSc2
s	s	k7c7
anglickou	anglický	k2eAgFnSc7d1
podporou	podpora	k1gFnSc7
(	(	kIx(
<g/>
a	a	k8xC
přiženil	přiženit	k5eAaPmAgMnS
se	se	k3xPyFc4
do	do	k7c2
anglické	anglický	k2eAgFnSc2d1
královské	královský	k2eAgFnSc2d1
rodiny	rodina	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
Jan	Jan	k1gMnSc1
nucen	nutit	k5eAaImNgMnS
potvrdit	potvrdit	k5eAaPmF
některým	některý	k3yIgMnPc3
anglickým	anglický	k2eAgMnPc3d1
baronům	baron	k1gMnPc3
mocné	mocný	k2eAgFnSc2d1
pozice	pozice	k1gFnSc2
v	v	k7c6
Bretani	Bretaň	k1gFnSc6
<g/>
,	,	kIx,
zejména	zejména	k9
jako	jako	k9
kontrolory	kontrolor	k1gMnPc4
strategicky	strategicky	k6eAd1
důležitých	důležitý	k2eAgFnPc2d1
pevností	pevnost	k1gFnPc2
v	v	k7c6
okolí	okolí	k1gNnSc6
přístavu	přístav	k1gInSc2
Brest	Brest	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yQgMnSc1,k3yRgMnSc1
umožnil	umožnit	k5eAaPmAgMnS
anglickým	anglický	k2eAgMnSc7d1
vojákům	voják	k1gMnPc3
přístup	přístup	k1gInSc4
k	k	k7c3
poloostrovu	poloostrov	k1gInSc3
a	a	k8xC
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
bral	brát	k5eAaImAgMnS
bretaňské	bretaňský	k2eAgInPc4d1
příjmy	příjem	k1gInPc4
a	a	k8xC
dával	dávat	k5eAaImAgMnS
je	být	k5eAaImIp3nS
anglické	anglický	k2eAgFnSc3d1
koruně	koruna	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
anglická	anglický	k2eAgFnSc1d1
mocenská	mocenský	k2eAgFnSc1d1
základna	základna	k1gFnSc1
v	v	k7c6
Bretani	Bretaň	k1gFnSc6
byla	být	k5eAaImAgFnS
odmítána	odmítán	k2eAgFnSc1d1
bretonskými	bretonský	k2eAgMnPc7d1
aristokraty	aristokrat	k1gMnPc7
i	i	k8xC
francouzskou	francouzský	k2eAgFnSc7d1
monarchií	monarchie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
se	se	k3xPyFc4
prohlásil	prohlásit	k5eAaPmAgMnS
vazalem	vazal	k1gMnSc7
Karla	Karel	k1gMnSc4
V.	V.	kA
Francouzského	francouzský	k2eAgMnSc4d1
<g/>
,	,	kIx,
a	a	k8xC
ne	ne	k9
Eduarda	Eduard	k1gMnSc2
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
gesto	gesto	k1gNnSc1
neuklidnilo	uklidnit	k5eNaPmAgNnS
jeho	jeho	k3xOp3gMnPc4
kritiky	kritik	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
viděli	vidět	k5eAaImAgMnP
ničemné	ničemný	k2eAgMnPc4d1
anglické	anglický	k2eAgMnPc4d1
vojáky	voják	k1gMnPc4
a	a	k8xC
lordy	lord	k1gMnPc4
jako	jako	k8xS,k8xC
destabilizující	destabilizující	k2eAgMnPc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tváří	tvář	k1gFnPc2
v	v	k7c4
tvář	tvář	k1gFnSc4
vzdoru	vzdor	k1gInSc2
bretoňské	bretoňský	k2eAgFnSc2d1
šlechty	šlechta	k1gFnSc2
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
nebyl	být	k5eNaImAgMnS
schopen	schopen	k2eAgMnSc1d1
shromáždit	shromáždit	k5eAaPmF
vojenskou	vojenský	k2eAgFnSc4d1
podporu	podpora	k1gFnSc4
proti	proti	k7c3
Karlu	Karel	k1gMnSc3
V.	V.	kA
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
využil	využít	k5eAaPmAgInS
příležitosti	příležitost	k1gFnPc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
vyvinul	vyvinout	k5eAaPmAgInS
tlak	tlak	k1gInSc1
na	na	k7c4
Bretaň	Bretaň	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bez	bez	k7c2
místní	místní	k2eAgFnSc2d1
podpory	podpora	k1gFnSc2
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
Jan	Jan	k1gMnSc1
nucen	nutit	k5eAaImNgMnS
v	v	k7c6
roce	rok	k1gInSc6
1373	#num#	k4
opět	opět	k6eAd1
odejít	odejít	k5eAaPmF
do	do	k7c2
anglického	anglický	k2eAgInSc2d1
exilu	exil	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Nicméně	nicméně	k8xC
<g/>
,	,	kIx,
Karel	Karel	k1gMnSc1
V.	V.	kA
udělal	udělat	k5eAaPmAgMnS
tu	tu	k6eAd1
chybu	chyba	k1gFnSc4
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
snažil	snažit	k5eAaImAgMnS
vévodství	vévodství	k1gNnSc2
zcela	zcela	k6eAd1
připojit	připojit	k5eAaPmF
k	k	k7c3
francouzské	francouzský	k2eAgFnSc3d1
koruně	koruna	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bertrand	Bertrand	k1gInSc1
de	de	k?
Guesclin	Guesclin	k1gInSc1
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1378	#num#	k4
poslán	poslat	k5eAaPmNgInS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
vévodství	vévodství	k1gNnSc4
podrobil	podrobit	k5eAaPmAgMnS
francouzskému	francouzský	k2eAgMnSc3d1
králi	král	k1gMnSc3
silou	síla	k1gFnSc7
zbraní	zbraň	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Baroni	baron	k1gMnPc1
se	se	k3xPyFc4
vzbouřili	vzbouřit	k5eAaPmAgMnP
proti	proti	k7c3
francouzské	francouzský	k2eAgFnSc3d1
anexi	anexe	k1gFnSc3
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1379	#num#	k4
vyzvali	vyzvat	k5eAaPmAgMnP
Jana	Jan	k1gMnSc2
IV	IV	kA
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
V.	V.	kA
k	k	k7c3
návratu	návrat	k1gInSc3
z	z	k7c2
vyhnanství	vyhnanství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přistál	přistát	k5eAaImAgMnS,k5eAaPmAgMnS
ve	v	k7c6
městě	město	k1gNnSc6
Dinard	Dinarda	k1gFnPc2
a	a	k8xC
s	s	k7c7
podporou	podpora	k1gFnSc7
místních	místní	k2eAgMnPc2d1
baronů	baron	k1gMnPc2
opět	opět	k6eAd1
získal	získat	k5eAaPmAgMnS
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
vévodstvím	vévodství	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anglická	anglický	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
pod	pod	k7c7
vedením	vedení	k1gNnSc7
Tomáše	Tomáš	k1gMnSc2
z	z	k7c2
Woodstocku	Woodstock	k1gInSc2
přistála	přistát	k5eAaPmAgFnS,k5eAaImAgFnS
u	u	k7c2
Calais	Calais	k1gNnSc2
a	a	k8xC
pochodovala	pochodovat	k5eAaImAgFnS
směrem	směr	k1gInSc7
k	k	k7c3
Nantes	Nantes	k1gInSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
převzala	převzít	k5eAaPmAgFnS
kontrolu	kontrola	k1gFnSc4
nad	nad	k7c7
městem	město	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
se	se	k3xPyFc4
smířil	smířit	k5eAaPmAgMnS
s	s	k7c7
novým	nový	k2eAgMnSc7d1
francouzským	francouzský	k2eAgMnSc7d1
králem	král	k1gMnSc7
Karlem	Karel	k1gMnSc7
VI	VI	kA
<g/>
.	.	kIx.
a	a	k8xC
vyplatil	vyplatit	k5eAaPmAgInS
anglické	anglický	k2eAgNnSc4d1
vojsko	vojsko	k1gNnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
vyhnul	vyhnout	k5eAaPmAgInS
konfrontaci	konfrontace	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Potom	potom	k6eAd1
vládl	vládnout	k5eAaImAgInS
vévodství	vévodství	k1gNnSc4
v	v	k7c6
míru	mír	k1gInSc6
s	s	k7c7
francouzskou	francouzský	k2eAgFnSc7d1
i	i	k8xC
anglickou	anglický	k2eAgFnSc7d1
korunou	koruna	k1gFnSc7
více	hodně	k6eAd2
než	než	k8xS
deset	deset	k4xCc4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
udržoval	udržovat	k5eAaImAgInS
s	s	k7c7
oběma	dva	k4xCgInPc7
kontakt	kontakt	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
minimalizoval	minimalizovat	k5eAaBmAgMnS
otevřené	otevřený	k2eAgNnSc4d1
spojení	spojení	k1gNnSc4
s	s	k7c7
Anglií	Anglie	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Také	také	k6eAd1
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
v	v	k7c6
roce	rok	k1gInSc6
1397	#num#	k4
podařilo	podařit	k5eAaPmAgNnS
vyprostit	vyprostit	k5eAaPmF
Brest	Brest	k1gInSc4
z	z	k7c2
anglické	anglický	k2eAgFnSc2d1
kontroly	kontrola	k1gFnSc2
pomocí	pomocí	k7c2
diplomatického	diplomatický	k2eAgInSc2d1
tlaku	tlak	k1gInSc2
a	a	k8xC
finančních	finanční	k2eAgInPc2d1
úplatků	úplatek	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Clissonská	Clissonský	k2eAgFnSc1d1
aféra	aféra	k1gFnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1392	#num#	k4
byl	být	k5eAaImAgInS
učiněn	učinit	k5eAaImNgInS,k5eAaPmNgInS
pokus	pokus	k1gInSc1
zabít	zabít	k5eAaPmF
Oliviera	Oliviera	k1gFnSc1
de	de	k?
Clisson	Clissona	k1gFnPc2
<g/>
,	,	kIx,
konstábla	konstábl	k1gMnSc2
Francie	Francie	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
byl	být	k5eAaImAgMnS
starým	starý	k2eAgMnSc7d1
vévodovým	vévodův	k2eAgMnSc7d1
nepřítelem	nepřítel	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Útočník	útočník	k1gMnSc1
<g/>
,	,	kIx,
Pierre	Pierr	k1gInSc5
de	de	k?
Craon	Craon	k1gInSc4
<g/>
,	,	kIx,
uprchl	uprchnout	k5eAaPmAgMnS
do	do	k7c2
Bretaně	Bretaň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jan	Jan	k1gMnSc1
byl	být	k5eAaImAgMnS
domnělým	domnělý	k2eAgMnSc7d1
strůjcem	strůjce	k1gMnSc7
spiknutí	spiknutí	k1gNnSc2
<g/>
,	,	kIx,
a	a	k8xC
Karel	Karel	k1gMnSc1
VI	VI	kA
<g/>
.	.	kIx.
využil	využít	k5eAaPmAgMnS
příležitosti	příležitost	k1gFnSc2
k	k	k7c3
útoku	útok	k1gInSc3
Bretaně	Bretaň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
doprovodu	doprovod	k1gInSc6
konstábla	konstábl	k1gMnSc2
pochodoval	pochodovat	k5eAaImAgMnS
do	do	k7c2
Bretaně	Bretaň	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
než	než	k8xS
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgMnS
k	k	k7c3
vévodství	vévodství	k1gNnSc3
<g/>
,	,	kIx,
byl	být	k5eAaImAgMnS
král	král	k1gMnSc1
postižen	postihnout	k5eAaPmNgMnS
záchvatem	záchvat	k1gInSc7
šílenství	šílenství	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příbuzní	příbuzný	k2eAgMnPc5d1
Karla	Karel	k1gMnSc2
VI	VI	kA
<g/>
.	.	kIx.
obviňovali	obviňovat	k5eAaImAgMnP
Clissona	Clissona	k1gFnSc1
<g/>
,	,	kIx,
a	a	k8xC
žaloby	žaloba	k1gFnSc2
proti	proti	k7c3
němu	on	k3xPp3gInSc3
oslabily	oslabit	k5eAaPmAgFnP
jeho	jeho	k3xOp3gFnSc4
politickou	politický	k2eAgFnSc4d1
pozici	pozice	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zbavený	zbavený	k2eAgInSc4d1
postavení	postavení	k1gNnSc1
konstábla	konstábl	k1gMnSc2
<g/>
,	,	kIx,
Clisson	Clisson	k1gInSc1
našel	najít	k5eAaPmAgInS
útočiště	útočiště	k1gNnSc4
v	v	k7c6
Bretani	Bretaň	k1gFnSc6
<g/>
,	,	kIx,
smířil	smířit	k5eAaPmAgInS
se	se	k3xPyFc4
s	s	k7c7
Janem	Jan	k1gMnSc7
<g/>
,	,	kIx,
a	a	k8xC
stal	stát	k5eAaPmAgMnS
se	se	k3xPyFc4
jeho	jeho	k3xOp3gMnSc7
blízkým	blízký	k2eAgMnSc7d1
poradcem	poradce	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Anglický	anglický	k2eAgInSc1d1
rytířský	rytířský	k2eAgInSc1d1
stav	stav	k1gInSc1
</s>
<s>
Jan	Jan	k1gMnSc1
byl	být	k5eAaImAgMnS
v	v	k7c6
roce	rok	k1gInSc6
1375	#num#	k4
-	-	kIx~
1376	#num#	k4
Eduardem	Eduard	k1gMnSc7
III	III	kA
<g/>
.	.	kIx.
pasován	pasován	k2eAgMnSc1d1
na	na	k7c4
rytíře	rytíř	k1gMnSc4
jako	jako	k8xS,k8xC
člen	člen	k1gMnSc1
Podvazkového	podvazkový	k2eAgInSc2d1
řádu	řád	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Věřil	věřit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
jediným	jediný	k2eAgMnSc7d1
bretaňským	bretaňský	k2eAgMnSc7d1
vévodou	vévoda	k1gMnSc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
dosáhl	dosáhnout	k5eAaPmAgMnS
tohoto	tento	k3xDgNnSc2
anglického	anglický	k2eAgNnSc2d1
vyznamenání	vyznamenání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Rodina	rodina	k1gFnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s>
Manželství	manželství	k1gNnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
se	se	k3xPyFc4
třikrát	třikrát	k6eAd1
oženil	oženit	k5eAaPmAgMnS
<g/>
:	:	kIx,
</s>
<s>
Marie	Marie	k1gFnSc1
Anglická	anglický	k2eAgFnSc1d1
</s>
<s>
Johana	Johana	k1gFnSc1
Holandská	holandský	k2eAgFnSc1d1
</s>
<s>
Jana	Jana	k1gFnSc1
Navarrská	navarrský	k2eAgFnSc1d1
</s>
<s>
Jana	Jana	k1gFnSc1
byla	být	k5eAaImAgFnS
matkou	matka	k1gFnSc7
všech	všecek	k3xTgFnPc2
Janových	Janových	k2eAgFnPc2d1
dětí	dítě	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
jeho	jeho	k3xOp3gFnSc6
smrti	smrt	k1gFnSc6
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
regentkou	regentka	k1gFnSc7
za	za	k7c4
jejich	jejich	k3xOp3gMnPc4
syna	syn	k1gMnSc4
Jana	Jan	k1gMnSc2
V.	V.	kA
<g/>
/	/	kIx~
<g/>
VI	VI	kA
<g/>
.	.	kIx.
a	a	k8xC
nakonec	nakonec	k6eAd1
se	se	k3xPyFc4
provdala	provdat	k5eAaPmAgFnS
za	za	k7c4
Jindřicha	Jindřich	k1gMnSc4
IV	Iva	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anglického	anglický	k2eAgInSc2d1
<g/>
.	.	kIx.
</s>
<s>
Potomci	potomek	k1gMnPc1
</s>
<s>
Jana	Jana	k1gFnSc1
Bretaňská	bretaňský	k2eAgFnSc1d1
</s>
<s>
Isabela	Isabela	k1gFnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
V.	V.	kA
Bretaňský	bretaňský	k2eAgInSc5d1
</s>
<s>
Marie	Marie	k1gFnSc1
Bretaňská	bretaňský	k2eAgFnSc1d1
(	(	kIx(
<g/>
1391	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Markéta	Markéta	k1gFnSc1
</s>
<s>
Artur	Artur	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bretaňský	bretaňský	k2eAgInSc1d1
</s>
<s>
Gilles	Gilles	k1gMnSc1
</s>
<s>
Richard	Richard	k1gMnSc1
Bretaňský	bretaňský	k2eAgMnSc1d1
</s>
<s>
Blanka	Blanka	k1gFnSc1
</s>
<s>
Vývod	vývod	k1gInSc1
z	z	k7c2
předků	předek	k1gInPc2
</s>
<s>
Jan	Jan	k1gMnSc1
I.	I.	kA
Bretaňský	bretaňský	k2eAgMnSc5d1
</s>
<s>
Jan	Jan	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bretaňský	bretaňský	k2eAgInSc1d1
</s>
<s>
Blanka	Blanka	k1gFnSc1
Navarrská	navarrský	k2eAgFnSc1d1
</s>
<s>
Artur	Artur	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bretaňský	bretaňský	k2eAgInSc1d1
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anglický	anglický	k2eAgInSc1d1
</s>
<s>
Beatrix	Beatrix	k1gInSc1
Anglická	anglický	k2eAgFnSc1d1
</s>
<s>
Eleonora	Eleonora	k1gFnSc1
Provensálská	provensálský	k2eAgFnSc1d1
</s>
<s>
Jan	Jan	k1gMnSc1
z	z	k7c2
Montfortu	Montfort	k1gInSc2
</s>
<s>
Jan	Jan	k1gMnSc1
I.	I.	kA
z	z	k7c2
Dreux	Dreux	k1gInSc1
</s>
<s>
Robert	Robert	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
z	z	k7c2
Dreux	Dreux	k1gInSc1
</s>
<s>
Marie	Marie	k1gFnSc1
Bourbonská	bourbonský	k2eAgFnSc1d1
</s>
<s>
Jolanda	Jolanda	k1gFnSc1
z	z	k7c2
Dreux	Dreux	k1gInSc4
</s>
<s>
Jan	Jan	k1gMnSc1
I.	I.	kA
z	z	k7c2
Montfortu	Montfort	k1gInSc2
</s>
<s>
Beatrix	Beatrix	k1gInSc1
z	z	k7c2
Montfortu	Montfort	k1gInSc2
</s>
<s>
Jana	Jana	k1gFnSc1
ze	z	k7c2
Châteaudun	Châteauduna	k1gFnPc2
</s>
<s>
'	'	kIx"
<g/>
Jan	Jan	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bretaňský	bretaňský	k2eAgInSc1d1
<g/>
'	'	kIx"
</s>
<s>
Ludvík	Ludvík	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bourbonský	bourbonský	k2eAgInSc1d1
</s>
<s>
Jan	Jan	k1gMnSc1
I.	I.	kA
Bourbonský	bourbonský	k2eAgInSc5d1
</s>
<s>
Anna	Anna	k1gFnSc1
z	z	k7c2
Auvergne	Auvergn	k1gMnSc5
</s>
<s>
Karel	Karel	k1gMnSc1
I.	I.	kA
Bourbonský	bourbonský	k2eAgInSc1d1
</s>
<s>
Jan	Jan	k1gMnSc1
z	z	k7c2
Berry	Berra	k1gFnSc2
</s>
<s>
Marie	Marie	k1gFnSc1
z	z	k7c2
Berry	Berra	k1gFnSc2
</s>
<s>
Jana	Jana	k1gFnSc1
z	z	k7c2
Armagnacu	Armagnacus	k1gInSc2
</s>
<s>
Johana	Johana	k1gFnSc1
Flanderská	flanderský	k2eAgFnSc1d1
</s>
<s>
Filip	Filip	k1gMnSc1
II	II	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Burgundský	burgundský	k2eAgInSc1d1
</s>
<s>
Jan	Jan	k1gMnSc1
I.	I.	kA
Burgundský	burgundský	k2eAgMnSc1d1
</s>
<s>
Markéta	Markéta	k1gFnSc1
III	III	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Flanderská	flanderský	k2eAgFnSc1d1
</s>
<s>
Anežka	Anežka	k1gFnSc1
Burgundská	burgundský	k2eAgFnSc1d1
</s>
<s>
Albrecht	Albrecht	k1gMnSc1
I.	I.	kA
Bavorský	bavorský	k2eAgInSc5d1
</s>
<s>
Markéta	Markéta	k1gFnSc1
Bavorská	bavorský	k2eAgFnSc1d1
</s>
<s>
Markéta	Markéta	k1gFnSc1
Břežská	Břežský	k2eAgFnSc1d1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
John	John	k1gMnSc1
IV	Iva	k1gFnPc2
<g/>
,	,	kIx,
Duke	Duke	k1gFnPc2
of	of	k?
Brittany	Brittan	k1gInPc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
10094776X	10094776X	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
27422292	#num#	k4
</s>
