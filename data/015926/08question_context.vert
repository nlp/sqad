<s desamb="1">
Bretaňský	bretaňský	k2eAgInSc1d1
(	(	kIx(
<g/>
bretonsky	bretonsky	k6eAd1
Yann	Yann	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
francouzsky	francouzsky	k6eAd1
Jean	Jean	k1gMnSc1
IV	IV	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
1339	#num#	k4
-	-	kIx~
1	#num#	k4
<g/>
.	.	kIx.
listopad	listopad	k1gInSc1
1399	#num#	k4
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
od	od	k7c2
roku	rok	k1gInSc2
1345	#num#	k4
bretaňským	bretaňský	k2eAgMnSc7d1
vévodou	vévoda	k1gMnSc7
a	a	k8xC
hrabětem	hrabě	k1gMnSc7
z	z	k7c2
Montfort	Montfort	k1gInSc4
<g/>
,	,	kIx,
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1372	#num#	k4
hrabětem	hrabě	k1gMnSc7
z	z	k7c2
Richmondu	Richmond	k1gInSc2
<g/>
.	.	kIx.
</s>