<s>
10	[number]	k4	10
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1898	[number]	k4	1898
její	její	k3xOp3gMnSc1	její
život	život	k1gInSc4	život
ukončil	ukončit	k5eAaPmAgMnS	ukončit
italský	italský	k2eAgMnSc1d1	italský
anarchista	anarchista	k1gMnSc1	anarchista
Luigi	Luigi	k1gNnSc2	Luigi
Lucheni	Luchen	k2eAgMnPc1d1	Luchen
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
ji	on	k3xPp3gFnSc4	on
probodl	probodnout	k5eAaPmAgMnS	probodnout
pilníkem	pilník	k1gInSc7	pilník
(	(	kIx(	(
<g/>
nástroj	nástroj	k1gInSc1	nástroj
pronikl	proniknout	k5eAaPmAgInS	proniknout
do	do	k7c2	do
hrudi	hruď	k1gFnSc2	hruď
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
8,5	[number]	k4	8,5
centimetru	centimetr	k1gInSc2	centimetr
<g/>
,	,	kIx,	,
zlomil	zlomit	k5eAaPmAgMnS	zlomit
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
žebro	žebro	k1gNnSc4	žebro
a	a	k8xC	a
pronikl	proniknout	k5eAaPmAgInS	proniknout
mezižeberním	mezižeberní	k2eAgInSc7d1	mezižeberní
prostorem	prostor	k1gInSc7	prostor
<g/>
,	,	kIx,	,
prorazil	prorazit	k5eAaPmAgInS	prorazit
dolní	dolní	k2eAgInSc1d1	dolní
kraj	kraj	k1gInSc1	kraj
plicního	plicní	k2eAgInSc2d1	plicní
laloku	lalok	k1gInSc2	lalok
a	a	k8xC	a
zasáhl	zasáhnout	k5eAaPmAgInS	zasáhnout
levou	levý	k2eAgFnSc4d1	levá
část	část	k1gFnSc4	část
srdeční	srdeční	k2eAgFnSc2d1	srdeční
komory	komora	k1gFnSc2	komora
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
když	když	k8xS	když
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
ženevského	ženevský	k2eAgInSc2d1	ženevský
přístavu	přístav	k1gInSc2	přístav
<g/>
.	.	kIx.	.
</s>
