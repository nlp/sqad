<p>
<s>
Diftongizace	diftongizace	k1gFnSc1	diftongizace
je	být	k5eAaImIp3nS	být
změna	změna	k1gFnSc1	změna
dlouhé	dlouhý	k2eAgFnSc2d1	dlouhá
samohlásky	samohláska	k1gFnSc2	samohláska
na	na	k7c4	na
dvojhlásku	dvojhláska	k1gFnSc4	dvojhláska
(	(	kIx(	(
<g/>
=	=	kIx~	=
<g/>
diftong	diftong	k1gInSc1	diftong
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
ve	v	k7c6	v
vývoji	vývoj	k1gInSc6	vývoj
jazyka	jazyk	k1gInSc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Opakem	opak	k1gInSc7	opak
je	být	k5eAaImIp3nS	být
monoftongizace	monoftongizace	k1gFnSc1	monoftongizace
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
dvojhláska	dvojhláska	k1gFnSc1	dvojhláska
mění	měnit	k5eAaImIp3nS	měnit
na	na	k7c4	na
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
samohlásku	samohláska	k1gFnSc4	samohláska
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Čeština	čeština	k1gFnSc1	čeština
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
diftongizaci	diftongizace	k1gFnSc6	diftongizace
ú	ú	k0	ú
na	na	k7c4	na
ou	ou	k0	ou
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
bylo	být	k5eAaImAgNnS	být
psáno	psát	k5eAaImNgNnS	psát
jako	jako	k9	jako
au	au	k0	au
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ze	z	k7c2	z
starého	starý	k2eAgNnSc2d1	staré
spadnúti	spadnúti	k1gNnSc2	spadnúti
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
dnešní	dnešní	k2eAgNnSc1d1	dnešní
spadnout	spadnout	k5eAaPmF	spadnout
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
slovenštině	slovenština	k1gFnSc6	slovenština
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
nedošlo	dojít	k5eNaPmAgNnS	dojít
(	(	kIx(	(
<g/>
spadnúť	spadnúť	k1gFnSc1	spadnúť
<g/>
)	)	kIx)	)
a	a	k8xC	a
staré	starý	k2eAgFnPc1d1	stará
ú	ú	k0	ú
je	být	k5eAaImIp3nS	být
zachováno	zachovat	k5eAaPmNgNnS	zachovat
i	i	k9	i
ve	v	k7c6	v
východomoravském	východomoravský	k2eAgNnSc6d1	východomoravské
nářečí	nářečí	k1gNnSc6	nářečí
(	(	kIx(	(
<g/>
spadnút	spadnút	k5eAaPmF	spadnút
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
v	v	k7c6	v
lašském	lašský	k2eAgNnSc6d1	Lašské
nářečí	nářečí	k1gNnSc6	nářečí
(	(	kIx(	(
<g/>
spadnuć	spadnuć	k?	spadnuć
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hanáckém	hanácký	k2eAgNnSc6d1	Hanácké
nářečí	nářečí	k1gNnSc6	nářečí
došlo	dojít	k5eAaPmAgNnS	dojít
nejprve	nejprve	k6eAd1	nejprve
k	k	k7c3	k
diftongizaci	diftongizace	k1gFnSc3	diftongizace
ú	ú	k0	ú
na	na	k7c4	na
ou	ou	k0	ou
(	(	kIx(	(
<g/>
tak	tak	k9	tak
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
ve	v	k7c6	v
spisovné	spisovný	k2eAgFnSc6d1	spisovná
češtině	čeština	k1gFnSc6	čeština
<g/>
)	)	kIx)	)
a	a	k8xC	a
potom	potom	k6eAd1	potom
k	k	k7c3	k
monoftongizaci	monoftongizace	k1gFnSc3	monoftongizace
na	na	k7c6	na
ó	ó	k0	ó
(	(	kIx(	(
<g/>
spadnót	spadnótum	k1gNnPc2	spadnótum
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
nářečí	nářečí	k1gNnSc6	nářečí
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
i	i	k9	i
diftongizace	diftongizace	k1gFnSc1	diftongizace
ý	ý	k?	ý
na	na	k7c4	na
ej	ej	k0	ej
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
psáno	psát	k5eAaImNgNnS	psát
jako	jako	k9	jako
ay	ay	k?	ay
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
bejt	bejt	k?	bejt
místo	místo	k6eAd1	místo
být	být	k5eAaImF	být
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
hanáckém	hanácký	k2eAgNnSc6d1	Hanácké
nářečí	nářečí	k1gNnSc6	nářečí
se	se	k3xPyFc4	se
ej	ej	k0	ej
monoftongizovalo	monoftongizovat	k5eAaImAgNnS	monoftongizovat
na	na	k7c4	na
é	é	k0	é
(	(	kIx(	(
<g/>
bét	bét	k?	bét
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
této	tento	k3xDgFnSc3	tento
změně	změna	k1gFnSc3	změna
nedošlo	dojít	k5eNaPmAgNnS	dojít
v	v	k7c6	v
lašském	lašský	k2eAgNnSc6d1	Lašské
nářečí	nářečí	k1gNnSc6	nářečí
(	(	kIx(	(
<g/>
być	być	k?	być
<g/>
)	)	kIx)	)
a	a	k8xC	a
ve	v	k7c6	v
východomoravském	východomoravský	k2eAgNnSc6d1	východomoravské
nářečí	nářečí	k1gNnSc6	nářečí
(	(	kIx(	(
<g/>
byt	byt	k1gInSc1	byt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
diftongizace	diftongizace	k1gFnSc2	diftongizace
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
