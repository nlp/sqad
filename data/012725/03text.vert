<p>
<s>
Jeffrey	Jeffrea	k1gFnPc1	Jeffrea
Preston	Preston	k1gInSc1	Preston
"	"	kIx"	"
<g/>
Jeff	Jeff	k1gMnSc1	Jeff
<g/>
"	"	kIx"	"
Bezos	Bezos	k1gMnSc1	Bezos
(	(	kIx(	(
<g/>
*	*	kIx~	*
12	[number]	k4	12
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
podnikatel	podnikatel	k1gMnSc1	podnikatel
<g/>
;	;	kIx,	;
zakladatel	zakladatel	k1gMnSc1	zakladatel
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
a	a	k8xC	a
výkonný	výkonný	k2eAgMnSc1d1	výkonný
ředitel	ředitel	k1gMnSc1	ředitel
(	(	kIx(	(
<g/>
CEO	CEO	kA	CEO
<g/>
)	)	kIx)	)
amerického	americký	k2eAgInSc2d1	americký
obchodního	obchodní	k2eAgInSc2d1	obchodní
portálu	portál	k1gInSc2	portál
Amazon	amazona	k1gFnPc2	amazona
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
října	říjen	k1gInSc2	říjen
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
drží	držet	k5eAaImIp3nS	držet
titul	titul	k1gInSc1	titul
nejbohatšího	bohatý	k2eAgMnSc2d3	nejbohatší
muže	muž	k1gMnSc2	muž
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
opět	opět	k6eAd1	opět
přeskočil	přeskočit	k5eAaPmAgMnS	přeskočit
Billa	Bill	k1gMnSc4	Bill
Gatese	Gatesa	k1gFnSc6	Gatesa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
jako	jako	k8xC	jako
Jeffrey	Jeffre	k2eAgFnPc1d1	Jeffre
Preston	Preston	k1gInSc4	Preston
Jorgensen	Jorgensna	k1gFnPc2	Jorgensna
v	v	k7c6	v
americkém	americký	k2eAgNnSc6d1	americké
Albuquerque	Albuquerque	k1gNnSc6	Albuquerque
<g/>
.	.	kIx.	.
</s>
<s>
Matka	matka	k1gFnSc1	matka
Jacklyn	Jacklyna	k1gFnPc2	Jacklyna
Gise	Gis	k1gInSc2	Gis
Jorgensen	Jorgensna	k1gFnPc2	Jorgensna
pocházela	pocházet	k5eAaImAgFnS	pocházet
z	z	k7c2	z
převážně	převážně	k6eAd1	převážně
farmářské	farmářský	k2eAgFnSc2d1	farmářská
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gMnSc1	její
dědeček	dědeček	k1gMnSc1	dědeček
přitom	přitom	k6eAd1	přitom
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
US	US	kA	US
Atomic	Atomic	k1gMnSc1	Atomic
Energy	Energ	k1gMnPc4	Energ
Commission	Commission	k1gInSc4	Commission
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
státní	státní	k2eAgFnSc1d1	státní
agentura	agentura	k1gFnSc1	agentura
pro	pro	k7c4	pro
atomovou	atomový	k2eAgFnSc4d1	atomová
energii	energie	k1gFnSc4	energie
<g/>
.	.	kIx.	.
</s>
<s>
Jeff	Jeff	k1gMnSc1	Jeff
Bezos	Bezos	k1gMnSc1	Bezos
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
jeho	jeho	k3xOp3gInSc6	jeho
ranči	ranč	k1gInSc6	ranč
často	často	k6eAd1	často
o	o	k7c6	o
prázdninách	prázdniny	k1gFnPc6	prázdniny
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
manželství	manželství	k1gNnSc1	manželství
s	s	k7c7	s
Bezosovým	Bezosový	k2eAgMnSc7d1	Bezosový
otcem	otec	k1gMnSc7	otec
<g/>
,	,	kIx,	,
Tedem	Ted	k1gMnSc7	Ted
Jorgensenen	Jorgensenna	k1gFnPc2	Jorgensenna
<g/>
,	,	kIx,	,
trvalo	trvat	k5eAaImAgNnS	trvat
pouze	pouze	k6eAd1	pouze
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c4	za
Miguela	Miguel	k1gMnSc4	Miguel
Bezose	Bezosa	k1gFnSc6	Bezosa
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
Jeffa	Jeff	k1gMnSc4	Jeff
Bezose	Bezosa	k1gFnSc3	Bezosa
adoptoval	adoptovat	k5eAaPmAgMnS	adoptovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Už	už	k6eAd1	už
od	od	k7c2	od
mala	mal	k1gInSc2	mal
měl	mít	k5eAaImAgMnS	mít
Bezos	Bezos	k1gMnSc1	Bezos
technické	technický	k2eAgNnSc4d1	technické
nadání	nadání	k1gNnSc4	nadání
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
střední	střední	k2eAgFnSc2d1	střední
školy	škola	k1gFnSc2	škola
šel	jít	k5eAaImAgMnS	jít
studovat	studovat	k5eAaImF	studovat
fyziku	fyzika	k1gFnSc4	fyzika
na	na	k7c4	na
Princetonskou	Princetonský	k2eAgFnSc4d1	Princetonská
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
,	,	kIx,	,
brzy	brzy	k6eAd1	brzy
však	však	k9	však
přešel	přejít	k5eAaPmAgInS	přejít
na	na	k7c4	na
informatiku	informatika	k1gFnSc4	informatika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
patřil	patřit	k5eAaImAgInS	patřit
k	k	k7c3	k
nejlepším	dobrý	k2eAgInPc3d3	nejlepší
v	v	k7c6	v
ročníku	ročník	k1gInSc6	ročník
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
studia	studio	k1gNnSc2	studio
byl	být	k5eAaImAgInS	být
členem	člen	k1gInSc7	člen
různých	různý	k2eAgFnPc2d1	různá
studentských	studentský	k2eAgFnPc2d1	studentská
organizací	organizace	k1gFnPc2	organizace
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
studentské	studentský	k2eAgFnSc2d1	studentská
organizace	organizace	k1gFnSc2	organizace
pro	pro	k7c4	pro
výzkum	výzkum	k1gInSc4	výzkum
vesmíru	vesmír	k1gInSc2	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dostudování	dostudování	k1gNnSc6	dostudování
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
informatik	informatik	k1gMnSc1	informatik
na	na	k7c4	na
Wall	Wall	k1gInSc4	Wall
Streetu	Street	k1gInSc2	Street
pro	pro	k7c4	pro
různé	různý	k2eAgFnPc4d1	různá
společnosti	společnost	k1gFnPc4	společnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
se	se	k3xPyFc4	se
Bezos	Bezos	k1gInSc1	Bezos
oženil	oženit	k5eAaPmAgInS	oženit
a	a	k8xC	a
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
roz	roz	k?	roz
<g/>
.	.	kIx.	.
</s>
<s>
Mackenzie	Mackenzie	k1gFnSc1	Mackenzie
má	mít	k5eAaImIp3nS	mít
čtyři	čtyři	k4xCgFnPc4	čtyři
děti	dítě	k1gFnPc4	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
však	však	k9	však
manželé	manžel	k1gMnPc1	manžel
Bezosovi	Bezosův	k2eAgMnPc1d1	Bezosův
požádali	požádat	k5eAaPmAgMnP	požádat
o	o	k7c4	o
rozvod	rozvod	k1gInSc4	rozvod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Bezos	Bezos	k1gMnSc1	Bezos
je	být	k5eAaImIp3nS	být
libertarián	libertarián	k1gMnSc1	libertarián
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
darovali	darovat	k5eAaPmAgMnP	darovat
2,5	[number]	k4	2,5
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
dolarů	dolar	k1gInPc2	dolar
na	na	k7c4	na
kampaň	kampaň	k1gFnSc4	kampaň
při	při	k7c6	při
referendu	referendum	k1gNnSc6	referendum
o	o	k7c6	o
stejnopohlavních	stejnopohlavní	k2eAgNnPc6d1	stejnopohlavní
manželstvích	manželství	k1gNnPc6	manželství
<g/>
,	,	kIx,	,
opakovaně	opakovaně	k6eAd1	opakovaně
také	také	k9	také
finančně	finančně	k6eAd1	finančně
podpořil	podpořit	k5eAaPmAgMnS	podpořit
Republikány	republikán	k1gMnPc4	republikán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
pak	pak	k6eAd1	pak
financoval	financovat	k5eAaBmAgMnS	financovat
expedici	expedice	k1gFnSc4	expedice
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ze	z	k7c2	z
dna	dno	k1gNnSc2	dno
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
vyzdvihla	vyzdvihnout	k5eAaPmAgFnS	vyzdvihnout
část	část	k1gFnSc1	část
rakety	raketa	k1gFnSc2	raketa
Saturn	Saturn	k1gInSc1	Saturn
V	V	kA	V
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
použita	použít	k5eAaPmNgFnS	použít
při	při	k7c6	při
misi	mise	k1gFnSc6	mise
Apollo	Apollo	k1gNnSc1	Apollo
11	[number]	k4	11
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
vložil	vložit	k5eAaPmAgMnS	vložit
42	[number]	k4	42
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
dolarů	dolar	k1gInPc2	dolar
do	do	k7c2	do
projektu	projekt	k1gInSc2	projekt
Clock	Clock	k1gMnSc1	Clock
of	of	k?	of
the	the	k?	the
Long	Long	k1gMnSc1	Long
Now	Now	k1gMnSc1	Now
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
cíl	cíl	k1gInSc4	cíl
výrobu	výrob	k1gInSc2	výrob
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
vydržet	vydržet	k5eAaPmF	vydržet
fungovat	fungovat	k5eAaImF	fungovat
min	min	kA	min
<g/>
.	.	kIx.	.
10	[number]	k4	10
000	[number]	k4	000
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Podnikání	podnikání	k1gNnSc1	podnikání
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
založil	založit	k5eAaPmAgMnS	založit
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
garáži	garáž	k1gFnSc6	garáž
společnost	společnost	k1gFnSc1	společnost
Amazon	amazona	k1gFnPc2	amazona
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
,	,	kIx,	,
zabývající	zabývající	k2eAgMnSc1d1	zabývající
se	se	k3xPyFc4	se
zpočátku	zpočátku	k6eAd1	zpočátku
pouze	pouze	k6eAd1	pouze
prodejem	prodej	k1gInSc7	prodej
knih	kniha	k1gFnPc2	kniha
<g/>
.	.	kIx.	.
</s>
<s>
Přestěhoval	přestěhovat	k5eAaPmAgInS	přestěhovat
se	se	k3xPyFc4	se
z	z	k7c2	z
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
do	do	k7c2	do
Seattlu	Seattl	k1gInSc2	Seattl
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
měl	mít	k5eAaImAgMnS	mít
lepší	dobrý	k2eAgNnSc4d2	lepší
technické	technický	k2eAgNnSc4d1	technické
zázemí	zázemí	k1gNnSc4	zázemí
<g/>
.	.	kIx.	.
</s>
<s>
Podnikatelský	podnikatelský	k2eAgInSc1d1	podnikatelský
záměr	záměr	k1gInSc1	záměr
Amazonu	amazona	k1gFnSc4	amazona
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
cestou	cestou	k7c2	cestou
autem	auto	k1gNnSc7	auto
do	do	k7c2	do
Texasu	Texas	k1gInSc2	Texas
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
co	co	k9	co
jeho	jeho	k3xOp3gFnSc1	jeho
manželka	manželka	k1gFnSc1	manželka
řídila	řídit	k5eAaImAgFnS	řídit
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
Amazonu	amazona	k1gFnSc4	amazona
se	se	k3xPyFc4	se
z	z	k7c2	z
Bezose	Bezosa	k1gFnSc6	Bezosa
stal	stát	k5eAaPmAgMnS	stát
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
předních	přední	k2eAgMnPc2d1	přední
internetových	internetový	k2eAgMnPc2d1	internetový
podnikatelů	podnikatel	k1gMnPc2	podnikatel
a	a	k8xC	a
miliardářů	miliardář	k1gMnPc2	miliardář
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
odhadl	odhadnout	k5eAaPmAgInS	odhadnout
časopis	časopis	k1gInSc1	časopis
Forbes	forbes	k1gInSc1	forbes
hodnotu	hodnota	k1gFnSc4	hodnota
majetku	majetek	k1gInSc2	majetek
Bezose	Bezosa	k1gFnSc3	Bezosa
na	na	k7c4	na
25,2	[number]	k4	25,2
mld.	mld.	k?	mld.
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
když	když	k8xS	když
většinu	většina	k1gFnSc4	většina
majetku	majetek	k1gInSc2	majetek
tvoří	tvořit	k5eAaImIp3nP	tvořit
právě	právě	k6eAd1	právě
akcie	akcie	k1gFnPc1	akcie
společnosti	společnost	k1gFnSc2	společnost
Amazon	amazona	k1gFnPc2	amazona
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
založil	založit	k5eAaPmAgMnS	založit
společnost	společnost	k1gFnSc4	společnost
Blue	Blu	k1gInSc2	Blu
Origin	Origina	k1gFnPc2	Origina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
podniká	podnikat	k5eAaImIp3nS	podnikat
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
vesmírné	vesmírný	k2eAgFnSc2d1	vesmírná
turistiky	turistika	k1gFnSc2	turistika
a	a	k8xC	a
vynášení	vynášení	k1gNnSc4	vynášení
nákladu	náklad	k1gInSc2	náklad
na	na	k7c4	na
oběžnou	oběžný	k2eAgFnSc4d1	oběžná
dráhu	dráha	k1gFnSc4	dráha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
zakoupil	zakoupit	k5eAaPmAgMnS	zakoupit
od	od	k7c2	od
společnosti	společnost	k1gFnSc2	společnost
Washington	Washington	k1gInSc1	Washington
Post	posta	k1gFnPc2	posta
Company	Compana	k1gFnPc1	Compana
deník	deník	k1gInSc4	deník
The	The	k1gMnPc2	The
Washington	Washington	k1gInSc1	Washington
Post	posta	k1gFnPc2	posta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
2016	[number]	k4	2016
a	a	k8xC	a
2017	[number]	k4	2017
získal	získat	k5eAaPmAgInS	získat
Bezos	Bezos	k1gInSc1	Bezos
(	(	kIx(	(
<g/>
potažmo	potažmo	k6eAd1	potažmo
Amazon	amazona	k1gFnPc2	amazona
<g/>
)	)	kIx)	)
600	[number]	k4	600
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
ve	v	k7c6	v
smlouvě	smlouva	k1gFnSc6	smlouva
se	se	k3xPyFc4	se
CIA	CIA	kA	CIA
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pro	pro	k7c4	pro
tuto	tento	k3xDgFnSc4	tento
organizaci	organizace	k1gFnSc4	organizace
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
cloudové	cloudový	k2eAgFnPc4d1	cloudová
služby	služba	k1gFnPc4	služba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Majetek	majetek	k1gInSc1	majetek
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
byl	být	k5eAaImAgMnS	být
Jeff	Jeff	k1gMnSc1	Jeff
Bezos	Bezos	k1gMnSc1	Bezos
časopisem	časopis	k1gInSc7	časopis
Forbes	forbes	k1gInSc1	forbes
vyhlášen	vyhlásit	k5eAaPmNgMnS	vyhlásit
nejbohatším	bohatý	k2eAgMnSc7d3	nejbohatší
člověkem	člověk	k1gMnSc7	člověk
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
podíl	podíl	k1gInSc1	podíl
17	[number]	k4	17
%	%	kIx~	%
na	na	k7c6	na
společnosti	společnost	k1gFnSc6	společnost
Amazon	amazona	k1gFnPc2	amazona
stoupl	stoupnout	k5eAaPmAgInS	stoupnout
díky	díky	k7c3	díky
nárůstu	nárůst	k1gInSc3	nárůst
ceny	cena	k1gFnSc2	cena
jejích	její	k3xOp3gFnPc2	její
akcií	akcie	k1gFnPc2	akcie
natolik	natolik	k6eAd1	natolik
v	v	k7c6	v
hodnotě	hodnota	k1gFnSc6	hodnota
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
Bezosovi	Bezos	k1gMnSc3	Bezos
přičítán	přičítán	k2eAgInSc4d1	přičítán
majetek	majetek	k1gInSc4	majetek
ve	v	k7c6	v
výši	výše	k1gFnSc6	výše
118,5	[number]	k4	118,5
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
Bezos	Bezos	k1gInSc1	Bezos
dostal	dostat	k5eAaPmAgInS	dostat
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
jediného	jediný	k2eAgMnSc2d1	jediný
člověka	člověk	k1gMnSc2	člověk
s	s	k7c7	s
"	"	kIx"	"
<g/>
dvanácticiferným	dvanácticiferný	k2eAgInSc7d1	dvanácticiferný
<g/>
"	"	kIx"	"
majetkem	majetek	k1gInSc7	majetek
a	a	k8xC	a
předstihl	předstihnout	k5eAaPmAgMnS	předstihnout
Billa	Bill	k1gMnSc4	Bill
Gatese	Gatesa	k1gFnSc6	Gatesa
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
hodnocen	hodnotit	k5eAaImNgInS	hodnotit
na	na	k7c4	na
90	[number]	k4	90
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgMnSc1	třetí
je	být	k5eAaImIp3nS	být
velkoakcionář	velkoakcionář	k1gMnSc1	velkoakcionář
společnosti	společnost	k1gFnSc2	společnost
Berkshire	Berkshir	k1gInSc5	Berkshir
Hathaway	Hathawaa	k1gFnPc4	Hathawaa
Warren	Warrno	k1gNnPc2	Warrno
Buffett	Buffetta	k1gFnPc2	Buffetta
s	s	k7c7	s
majetkem	majetek	k1gInSc7	majetek
84	[number]	k4	84
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
tři	tři	k4xCgMnPc1	tři
muži	muž	k1gMnPc1	muž
vlastní	vlastnit	k5eAaImIp3nP	vlastnit
víc	hodně	k6eAd2	hodně
<g/>
,	,	kIx,	,
než	než	k8xS	než
polovina	polovina	k1gFnSc1	polovina
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
160	[number]	k4	160
milionů	milion	k4xCgInPc2	milion
<g/>
)	)	kIx)	)
občanů	občan	k1gMnPc2	občan
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Herectví	herectví	k1gNnSc2	herectví
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
cameo	cameo	k6eAd1	cameo
roli	role	k1gFnSc6	role
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
objevil	objevit	k5eAaPmAgMnS	objevit
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Star	Star	kA	Star
Trek	Treka	k1gFnPc2	Treka
<g/>
:	:	kIx,	:
Do	do	k7c2	do
neznáma	neznámo	k1gNnSc2	neznámo
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
mimozemského	mimozemský	k2eAgMnSc4d1	mimozemský
důstojníka	důstojník	k1gMnSc4	důstojník
Hvězdné	hvězdný	k2eAgFnSc2d1	hvězdná
flotily	flotila	k1gFnSc2	flotila
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
