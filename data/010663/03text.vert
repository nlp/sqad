<p>
<s>
Osudy	osud	k1gInPc1	osud
dobrého	dobrý	k2eAgMnSc2d1	dobrý
vojáka	voják	k1gMnSc2	voják
Švejka	Švejk	k1gMnSc4	Švejk
za	za	k7c2	za
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
–	–	k?	–
<g/>
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
zkracováno	zkracován	k2eAgNnSc1d1	zkracováno
jako	jako	k8xC	jako
Osudy	osud	k1gInPc1	osud
dobrého	dobrý	k2eAgMnSc2d1	dobrý
vojáka	voják	k1gMnSc2	voják
Švejka	Švejk	k1gMnSc2	Švejk
či	či	k8xC	či
hovorově	hovorově	k6eAd1	hovorově
jen	jen	k9	jen
Švejk	Švejk	k1gMnSc1	Švejk
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
čtyřdílný	čtyřdílný	k2eAgInSc1d1	čtyřdílný
humoristický	humoristický	k2eAgInSc1d1	humoristický
román	román	k1gInSc1	román
českého	český	k2eAgMnSc2d1	český
spisovatele	spisovatel	k1gMnSc2	spisovatel
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Haška	Hašek	k1gMnSc2	Hašek
<g/>
,	,	kIx,	,
několikrát	několikrát	k6eAd1	několikrát
zfilmovaný	zfilmovaný	k2eAgMnSc1d1	zfilmovaný
i	i	k8xC	i
zdramatizovaný	zdramatizovaný	k2eAgMnSc1d1	zdramatizovaný
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nejpřekládanější	překládaný	k2eAgInSc1d3	nejpřekládanější
český	český	k2eAgInSc1d1	český
román	román	k1gInSc1	román
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
byl	být	k5eAaImAgMnS	být
přeložený	přeložený	k2eAgMnSc1d1	přeložený
do	do	k7c2	do
58	[number]	k4	58
jazyků	jazyk	k1gInPc2	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
nejvýznamnější	významný	k2eAgNnSc4d3	nejvýznamnější
Haškovo	Haškův	k2eAgNnSc4d1	Haškovo
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
mnoha	mnoho	k4c3	mnoho
lidmi	člověk	k1gMnPc7	člověk
spojováno	spojovat	k5eAaImNgNnS	spojovat
s	s	k7c7	s
kongeniálními	kongeniální	k2eAgFnPc7d1	kongeniální
ilustracemi	ilustrace	k1gFnPc7	ilustrace
Josefa	Josef	k1gMnSc2	Josef
Lady	lady	k1gFnSc6	lady
<g/>
.	.	kIx.	.
</s>
<s>
Smrt	smrt	k1gFnSc1	smrt
Haškovi	Hašek	k1gMnSc3	Hašek
nedovolila	dovolit	k5eNaPmAgFnS	dovolit
toto	tento	k3xDgNnSc4	tento
dílo	dílo	k1gNnSc4	dílo
dokončit	dokončit	k5eAaPmF	dokončit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
románu	román	k1gInSc2	román
==	==	k?	==
</s>
</p>
<p>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
díly	díl	k1gInPc1	díl
románu	román	k1gInSc2	román
mají	mít	k5eAaImIp3nP	mít
tyto	tento	k3xDgInPc4	tento
názvy	název	k1gInPc4	název
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
V	v	k7c6	v
zázemí	zázemí	k1gNnSc6	zázemí
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Na	na	k7c6	na
frontě	fronta	k1gFnSc6	fronta
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Slavný	slavný	k2eAgInSc1d1	slavný
výprask	výprask	k1gInSc1	výprask
(	(	kIx(	(
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Pokračování	pokračování	k1gNnSc1	pokračování
slavného	slavný	k2eAgInSc2d1	slavný
výprasku	výprask	k1gInSc2	výprask
(	(	kIx(	(
<g/>
1923	[number]	k4	1923
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
již	již	k9	již
první	první	k4xOgNnSc4	první
vydání	vydání	k1gNnSc4	vydání
tohoto	tento	k3xDgInSc2	tento
Haškem	Hašek	k1gMnSc7	Hašek
nedokončeného	dokončený	k2eNgInSc2d1	nedokončený
dílu	díl	k1gInSc2	díl
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
s	s	k7c7	s
pokračováním	pokračování	k1gNnSc7	pokračování
Karla	Karel	k1gMnSc2	Karel
Vaňka	Vaněk	k1gMnSc2	Vaněk
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
samém	samý	k3xTgInSc6	samý
roce	rok	k1gInSc6	rok
vydal	vydat	k5eAaPmAgInS	vydat
ještě	ještě	k9	ještě
díl	díl	k1gInSc1	díl
pátý	pátý	k4xOgMnSc1	pátý
a	a	k8xC	a
šestý	šestý	k4xOgMnSc1	šestý
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Vaňkovo	Vaňkův	k2eAgNnSc4d1	Vaňkovo
pokračování	pokračování	k1gNnSc4	pokračování
románu	román	k1gInSc2	román
Osudy	osud	k1gInPc1	osud
dobrého	dobrý	k2eAgMnSc2d1	dobrý
vojáka	voják	k1gMnSc2	voják
Švejka	Švejk	k1gMnSc2	Švejk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Haškovo	Haškův	k2eAgNnSc1d1	Haškovo
dílo	dílo	k1gNnSc1	dílo
není	být	k5eNaImIp3nS	být
snadné	snadný	k2eAgNnSc1d1	snadné
žánrově	žánrově	k6eAd1	žánrově
zařadit	zařadit	k5eAaPmF	zařadit
<g/>
.	.	kIx.	.
</s>
<s>
Zdaleka	zdaleka	k6eAd1	zdaleka
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
jen	jen	k9	jen
humoristický	humoristický	k2eAgInSc4d1	humoristický
román	román	k1gInSc4	román
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
zároveň	zároveň	k6eAd1	zároveň
satira	satira	k1gFnSc1	satira
líčící	líčící	k2eAgFnSc1d1	líčící
velice	velice	k6eAd1	velice
přesvědčivě	přesvědčivě	k6eAd1	přesvědčivě
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
všemožných	všemožný	k2eAgFnPc2d1	všemožná
karikatur	karikatura	k1gFnPc2	karikatura
<g/>
,	,	kIx,	,
podivných	podivný	k2eAgFnPc2d1	podivná
figurek	figurka	k1gFnPc2	figurka
a	a	k8xC	a
neobvyklých	obvyklý	k2eNgFnPc2d1	neobvyklá
situací	situace	k1gFnPc2	situace
žalostné	žalostný	k2eAgInPc1d1	žalostný
poměry	poměr	k1gInPc1	poměr
panující	panující	k2eAgInPc1d1	panující
v	v	k7c6	v
rozkládajícím	rozkládající	k2eAgInSc6d1	rozkládající
se	se	k3xPyFc4	se
Rakousku-Uhersku	Rakousku-Uhersek	k1gInSc3	Rakousku-Uhersek
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
Hašek	Hašek	k1gMnSc1	Hašek
využívá	využívat	k5eAaImIp3nS	využívat
i	i	k9	i
žánr	žánr	k1gInSc4	žánr
grotesky	groteska	k1gFnSc2	groteska
a	a	k8xC	a
frašky	fraška	k1gFnSc2	fraška
<g/>
,	,	kIx,	,
volí	volit	k5eAaImIp3nS	volit
jednoduché	jednoduchý	k2eAgInPc4d1	jednoduchý
a	a	k8xC	a
srozumitelné	srozumitelný	k2eAgInPc4d1	srozumitelný
výrazové	výrazový	k2eAgInPc4d1	výrazový
prostředky	prostředek	k1gInPc4	prostředek
<g/>
,	,	kIx,	,
píše	psát	k5eAaImIp3nS	psát
přehledně	přehledně	k6eAd1	přehledně
a	a	k8xC	a
jazykem	jazyk	k1gInSc7	jazyk
obyčejného	obyčejný	k2eAgMnSc2d1	obyčejný
člověka	člověk	k1gMnSc2	člověk
<g/>
,	,	kIx,	,
vše	všechen	k3xTgNnSc1	všechen
směřuje	směřovat	k5eAaImIp3nS	směřovat
k	k	k7c3	k
zesměšnění	zesměšnění	k1gNnSc3	zesměšnění
nesmyslného	smyslný	k2eNgNnSc2d1	nesmyslné
válčení	válčení	k1gNnSc2	válčení
a	a	k8xC	a
starého	starý	k2eAgInSc2d1	starý
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Kompozice	kompozice	k1gFnSc1	kompozice
románu	román	k1gInSc2	román
pak	pak	k6eAd1	pak
připomíná	připomínat	k5eAaImIp3nS	připomínat
pikareskní	pikareskní	k2eAgInSc4d1	pikareskní
román	román	k1gInSc4	román
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
hrdinou	hrdina	k1gMnSc7	hrdina
tzv.	tzv.	kA	tzv.
píkaro	píkara	k1gFnSc5	píkara
(	(	kIx(	(
<g/>
šibal	šibal	k1gMnSc1	šibal
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
zosobňuje	zosobňovat	k5eAaImIp3nS	zosobňovat
ideál	ideál	k1gInSc4	ideál
lidové	lidový	k2eAgFnSc2d1	lidová
podnikavosti	podnikavost	k1gFnSc2	podnikavost
a	a	k8xC	a
chytrosti	chytrost	k1gFnSc2	chytrost
<g/>
,	,	kIx,	,
prochází	procházet	k5eAaImIp3nS	procházet
celou	celý	k2eAgFnSc7d1	celá
řadou	řada	k1gFnSc7	řada
více	hodně	k6eAd2	hodně
či	či	k8xC	či
méně	málo	k6eAd2	málo
napínavých	napínavý	k2eAgFnPc2d1	napínavá
příhod	příhoda	k1gFnPc2	příhoda
a	a	k8xC	a
snaží	snažit	k5eAaImIp3nS	snažit
se	se	k3xPyFc4	se
za	za	k7c4	za
každou	každý	k3xTgFnSc4	každý
cenu	cena	k1gFnSc4	cena
přežít	přežít	k5eAaPmF	přežít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgMnSc1d1	hlavní
hrdina	hrdina	k1gMnSc1	hrdina
knihy	kniha	k1gFnSc2	kniha
Josef	Josef	k1gMnSc1	Josef
Švejk	Švejk	k1gMnSc1	Švejk
má	mít	k5eAaImIp3nS	mít
sochy	socha	k1gFnPc4	socha
a	a	k8xC	a
pomníky	pomník	k1gInPc4	pomník
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
řadě	řada	k1gFnSc6	řada
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
Humenném	Humenné	k1gNnSc6	Humenné
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
v	v	k7c6	v
polském	polský	k2eAgInSc6d1	polský
Přemyšlu	Přemyšl	k1gInSc6	Přemyšl
a	a	k8xC	a
Sanoku	Sanok	k1gInSc6	Sanok
<g/>
,	,	kIx,	,
v	v	k7c6	v
ruském	ruský	k2eAgMnSc6d1	ruský
Petrohradu	Petrohrad	k1gInSc3	Petrohrad
<g/>
,	,	kIx,	,
Omsku	Omsk	k1gInSc3	Omsk
a	a	k8xC	a
Bugulmě	Bugulma	k1gFnSc3	Bugulma
<g/>
,	,	kIx,	,
v	v	k7c6	v
ukrajinském	ukrajinský	k2eAgInSc6d1	ukrajinský
Kyjevě	Kyjev	k1gInSc6	Kyjev
<g/>
,	,	kIx,	,
Lvově	Lvov	k1gInSc6	Lvov
a	a	k8xC	a
Doněcku	Doněck	k1gInSc6	Doněck
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc4	první
sochu	socha	k1gFnSc4	socha
Švejka	Švejk	k1gMnSc2	Švejk
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
odhalili	odhalit	k5eAaPmAgMnP	odhalit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
v	v	k7c6	v
Putimi	Putim	k1gFnSc6	Putim
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
odehrávala	odehrávat	k5eAaImAgFnS	odehrávat
část	část	k1gFnSc1	část
příběhu	příběh	k1gInSc2	příběh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsah	obsah	k1gInSc1	obsah
románu	román	k1gInSc2	román
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
V	v	k7c6	v
zázemí	zázemí	k1gNnSc6	zázemí
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
knihy	kniha	k1gFnSc2	kniha
oznamuje	oznamovat	k5eAaImIp3nS	oznamovat
Švejkovi	Švejk	k1gMnSc6	Švejk
posluhovačka	posluhovačka	k1gFnSc1	posluhovačka
paní	paní	k1gFnSc1	paní
Müllerová	Müllerová	k1gFnSc1	Müllerová
slovy	slovo	k1gNnPc7	slovo
"	"	kIx"	"
<g/>
Tak	tak	k8xS	tak
nám	my	k3xPp1nPc3	my
zabili	zabít	k5eAaPmAgMnP	zabít
Ferdinanda	Ferdinand	k1gMnSc4	Ferdinand
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Sarajevu	Sarajevo	k1gNnSc6	Sarajevo
byl	být	k5eAaImAgMnS	být
zavražděn	zavražděn	k2eAgMnSc1d1	zavražděn
arcivévoda	arcivévoda	k1gMnSc1	arcivévoda
František	František	k1gMnSc1	František
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Este	Est	k1gMnSc5	Est
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pražském	pražský	k2eAgInSc6d1	pražský
hostinci	hostinec	k1gInSc6	hostinec
U	u	k7c2	u
Kalicha	kalich	k1gInSc2	kalich
se	se	k3xPyFc4	se
Švejk	Švejk	k1gMnSc1	Švejk
setkává	setkávat	k5eAaImIp3nS	setkávat
s	s	k7c7	s
tajným	tajný	k2eAgMnSc7d1	tajný
policistou	policista	k1gMnSc7	policista
Bretschneidrem	Bretschneidr	k1gMnSc7	Bretschneidr
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nechá	nechat	k5eAaPmIp3nS	nechat
zavřít	zavřít	k5eAaPmF	zavřít
sprostého	sprostý	k2eAgMnSc4d1	sprostý
hostinského	hostinský	k1gMnSc4	hostinský
Palivce	Palivec	k1gMnSc4	Palivec
do	do	k7c2	do
vězení	vězení	k1gNnSc2	vězení
pro	pro	k7c4	pro
velezradu	velezrada	k1gFnSc4	velezrada
za	za	k7c4	za
odpověď	odpověď	k1gFnSc4	odpověď
na	na	k7c4	na
otázku	otázka	k1gFnSc4	otázka
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
obraz	obraz	k1gInSc1	obraz
císaře	císař	k1gMnSc2	císař
pána	pán	k1gMnSc2	pán
(	(	kIx(	(
<g/>
hostinský	hostinský	k1gMnSc1	hostinský
řekl	říct	k5eAaPmAgMnS	říct
"	"	kIx"	"
<g/>
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
<g/>
,	,	kIx,	,
sraly	srát	k5eAaImAgFnP	srát
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
mouchy	moucha	k1gFnPc4	moucha
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zatčen	zatčen	k2eAgInSc1d1	zatčen
je	být	k5eAaImIp3nS	být
i	i	k9	i
Švejk	Švejk	k1gMnSc1	Švejk
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Ocitne	ocitnout	k5eAaPmIp3nS	ocitnout
se	se	k3xPyFc4	se
na	na	k7c6	na
policejním	policejní	k2eAgNnSc6d1	policejní
ředitelství	ředitelství	k1gNnSc6	ředitelství
a	a	k8xC	a
poté	poté	k6eAd1	poté
v	v	k7c6	v
blázinci	blázinec	k1gInSc6	blázinec
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
válka	válka	k1gFnSc1	válka
skutečně	skutečně	k6eAd1	skutečně
vypukne	vypuknout	k5eAaPmIp3nS	vypuknout
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Švejk	Švejk	k1gMnSc1	Švejk
povolán	povolat	k5eAaPmNgMnS	povolat
na	na	k7c4	na
vojnu	vojna	k1gFnSc4	vojna
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
trpí	trpět	k5eAaImIp3nP	trpět
silným	silný	k2eAgInSc7d1	silný
revmatismem	revmatismus	k1gInSc7	revmatismus
<g/>
,	,	kIx,	,
veze	vézt	k5eAaImIp3nS	vézt
ho	on	k3xPp3gMnSc4	on
k	k	k7c3	k
odvodu	odvod	k1gInSc3	odvod
paní	paní	k1gFnSc2	paní
Müllerová	Müllerová	k1gFnSc1	Müllerová
na	na	k7c6	na
kolečkovém	kolečkový	k2eAgNnSc6d1	kolečkové
křesle	křeslo	k1gNnSc6	křeslo
<g/>
.	.	kIx.	.
</s>
<s>
Švejk	Švejk	k1gMnSc1	Švejk
přitom	přitom	k6eAd1	přitom
mává	mávat	k5eAaImIp3nS	mávat
berlemi	berle	k1gFnPc7	berle
a	a	k8xC	a
křičí	křičet	k5eAaImIp3nS	křičet
"	"	kIx"	"
<g/>
Na	na	k7c4	na
Bělehrad	Bělehrad	k1gInSc4	Bělehrad
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
způsobí	způsobit	k5eAaPmIp3nS	způsobit
obrovské	obrovský	k2eAgNnSc1d1	obrovské
pozdvižení	pozdvižení	k1gNnSc1	pozdvižení
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
léčen	léčit	k5eAaImNgMnS	léčit
v	v	k7c6	v
nemocnici	nemocnice	k1gFnSc6	nemocnice
pro	pro	k7c4	pro
simulanty	simulant	k1gMnPc4	simulant
u	u	k7c2	u
doktora	doktor	k1gMnSc2	doktor
Grünsteina	Grünstein	k1gMnSc2	Grünstein
<g/>
,	,	kIx,	,
pobývá	pobývat	k5eAaImIp3nS	pobývat
na	na	k7c6	na
garnizóně	garnizóna	k1gFnSc6	garnizóna
(	(	kIx(	(
<g/>
vojenské	vojenský	k2eAgNnSc1d1	vojenské
vězení	vězení	k1gNnSc1	vězení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
u	u	k7c2	u
polního	polní	k2eAgMnSc2d1	polní
kuráta	kurát	k1gMnSc2	kurát
Katze	Katze	k1gFnSc2	Katze
a	a	k8xC	a
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
jej	on	k3xPp3gInSc4	on
věčně	věčně	k6eAd1	věčně
opilý	opilý	k2eAgInSc4d1	opilý
Katz	Katz	k1gInSc4	Katz
prohraje	prohrát	k5eAaPmIp3nS	prohrát
v	v	k7c6	v
kartách	karta	k1gFnPc6	karta
<g/>
,	,	kIx,	,
skončí	skončit	k5eAaPmIp3nS	skončit
jako	jako	k9	jako
vojenský	vojenský	k2eAgMnSc1d1	vojenský
sluha	sluha	k1gMnSc1	sluha
(	(	kIx(	(
<g/>
pucflek	pucflek	k1gMnSc1	pucflek
<g/>
)	)	kIx)	)
u	u	k7c2	u
nadporučíka	nadporučík	k1gMnSc2	nadporučík
Lukáše	Lukáš	k1gMnSc2	Lukáš
<g/>
,	,	kIx,	,
známého	známý	k2eAgMnSc2d1	známý
mnoha	mnoho	k4c2	mnoho
milostnými	milostný	k2eAgFnPc7d1	milostná
aférami	aféra	k1gFnPc7	aféra
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
nadporučík	nadporučík	k1gMnSc1	nadporučík
přikáže	přikázat	k5eAaPmIp3nS	přikázat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
mu	on	k3xPp3gMnSc3	on
sehnal	sehnat	k5eAaPmAgMnS	sehnat
stájového	stájový	k2eAgMnSc4d1	stájový
pinče	pinč	k1gMnSc4	pinč
<g/>
,	,	kIx,	,
Švejk	Švejk	k1gMnSc1	Švejk
jej	on	k3xPp3gMnSc4	on
pro	pro	k7c4	pro
něho	on	k3xPp3gMnSc4	on
ukradne	ukradnout	k5eAaPmIp3nS	ukradnout
<g/>
.	.	kIx.	.
</s>
<s>
Ukáže	ukázat	k5eAaPmIp3nS	ukázat
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
pes	pes	k1gMnSc1	pes
patřil	patřit	k5eAaImAgMnS	patřit
plukovníkovi	plukovník	k1gMnSc3	plukovník
Krausovi	Kraus	k1gMnSc3	Kraus
von	von	k1gInSc4	von
Zillergut	Zillergut	k1gMnSc1	Zillergut
a	a	k8xC	a
nadporučík	nadporučík	k1gMnSc1	nadporučík
Lukáš	Lukáš	k1gMnSc1	Lukáš
je	být	k5eAaImIp3nS	být
poslán	poslat	k5eAaPmNgMnS	poslat
do	do	k7c2	do
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
k	k	k7c3	k
91	[number]	k4	91
<g/>
.	.	kIx.	.
pěšímu	pěší	k1gMnSc3	pěší
pluku	pluk	k1gInSc2	pluk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
připravuje	připravovat	k5eAaImIp3nS	připravovat
k	k	k7c3	k
odjezdu	odjezd	k1gInSc3	odjezd
na	na	k7c4	na
frontu	fronta	k1gFnSc4	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Švejk	Švejk	k1gMnSc1	Švejk
jede	jet	k5eAaImIp3nS	jet
pochopitelně	pochopitelně	k6eAd1	pochopitelně
s	s	k7c7	s
ním	on	k3xPp3gMnSc7	on
a	a	k8xC	a
říká	říkat	k5eAaImIp3nS	říkat
nadporučíkovi	nadporučík	k1gMnSc3	nadporučík
<g/>
,	,	kIx,	,
že	že	k8xS	že
"	"	kIx"	"
<g/>
to	ten	k3xDgNnSc4	ten
bude	být	k5eAaImBp3nS	být
něco	něco	k6eAd1	něco
nádhernýho	nádhernýho	k?	nádhernýho
<g/>
,	,	kIx,	,
když	když	k8xS	když
voba	voba	k6eAd1	voba
padneme	padnout	k5eAaPmIp1nP	padnout
spolu	spolu	k6eAd1	spolu
za	za	k7c4	za
císaře	císař	k1gMnSc4	císař
pána	pán	k1gMnSc4	pán
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
rodinu	rodina	k1gFnSc4	rodina
<g/>
...	...	k?	...
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Na	na	k7c6	na
frontě	fronta	k1gFnSc6	fronta
===	===	k?	===
</s>
</p>
<p>
<s>
Do	do	k7c2	do
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
jede	jet	k5eAaImIp3nS	jet
nadporučík	nadporučík	k1gMnSc1	nadporučík
Lukáš	Lukáš	k1gMnSc1	Lukáš
se	s	k7c7	s
Švejkem	Švejk	k1gMnSc7	Švejk
rychlíkem	rychlík	k1gMnSc7	rychlík
a	a	k8xC	a
Švejkovi	Švejk	k1gMnSc3	Švejk
se	se	k3xPyFc4	se
přihodí	přihodit	k5eAaPmIp3nS	přihodit
nehoda	nehoda	k1gFnSc1	nehoda
<g/>
.	.	kIx.	.
</s>
<s>
Baví	bavit	k5eAaImIp3nS	bavit
se	se	k3xPyFc4	se
s	s	k7c7	s
železničním	železniční	k2eAgMnSc7d1	železniční
zaměstnancem	zaměstnanec	k1gMnSc7	zaměstnanec
o	o	k7c6	o
problematice	problematika	k1gFnSc6	problematika
záchranných	záchranný	k2eAgFnPc2d1	záchranná
brzd	brzda	k1gFnPc2	brzda
a	a	k8xC	a
možná	možná	k9	možná
on	on	k3xPp3gMnSc1	on
<g/>
,	,	kIx,	,
možná	možná	k9	možná
oba	dva	k4xCgInPc1	dva
za	za	k7c4	za
tuto	tento	k3xDgFnSc4	tento
brzdu	brzda	k1gFnSc4	brzda
zatáhnou	zatáhnout	k5eAaPmIp3nP	zatáhnout
a	a	k8xC	a
způsobí	způsobit	k5eAaPmIp3nS	způsobit
nenadálé	nenadálý	k2eAgNnSc4d1	nenadálé
zastavení	zastavení	k1gNnSc4	zastavení
vlaku	vlak	k1gInSc2	vlak
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Táboře	Tábor	k1gInSc6	Tábor
je	být	k5eAaImIp3nS	být
Švejk	Švejk	k1gMnSc1	Švejk
vyveden	vyvést	k5eAaPmNgMnS	vyvést
na	na	k7c4	na
nádraží	nádraží	k1gNnSc4	nádraží
kvůli	kvůli	k7c3	kvůli
sepsání	sepsání	k1gNnSc3	sepsání
protokolu	protokol	k1gInSc2	protokol
a	a	k8xC	a
vlak	vlak	k1gInSc1	vlak
mu	on	k3xPp3gMnSc3	on
mezitím	mezitím	k6eAd1	mezitím
ujede	ujet	k5eAaPmIp3nS	ujet
<g/>
.	.	kIx.	.
</s>
<s>
Velitel	velitel	k1gMnSc1	velitel
nádraží	nádraží	k1gNnSc2	nádraží
mu	on	k3xPp3gMnSc3	on
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
zjistí	zjistit	k5eAaPmIp3nS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Švejk	Švejk	k1gMnSc1	Švejk
nemá	mít	k5eNaImIp3nS	mít
peníze	peníz	k1gInPc4	peníz
na	na	k7c4	na
lístek	lístek	k1gInSc4	lístek
<g/>
,	,	kIx,	,
nařídí	nařídit	k5eAaPmIp3nS	nařídit
dojít	dojít	k5eAaPmF	dojít
do	do	k7c2	do
Českých	český	k2eAgInPc2d1	český
Budějovic	Budějovice	k1gInPc2	Budějovice
pěšky	pěšky	k6eAd1	pěšky
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
budějovické	budějovický	k2eAgFnSc6d1	Budějovická
anabázi	anabáze	k1gFnSc6	anabáze
Švejk	Švejk	k1gMnSc1	Švejk
pěkně	pěkně	k6eAd1	pěkně
zabloudí	zabloudit	k5eAaPmIp3nS	zabloudit
<g/>
,	,	kIx,	,
přihlouplým	přihlouplý	k2eAgMnSc7d1	přihlouplý
putimským	putimský	k2eAgMnSc7d1	putimský
strážmistrem	strážmistr	k1gMnSc7	strážmistr
Flanderkou	Flanderka	k1gFnSc7	Flanderka
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
ruského	ruský	k2eAgMnSc4d1	ruský
špióna	špión	k1gMnSc4	špión
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
je	být	k5eAaImIp3nS	být
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
pluku	pluk	k1gInSc3	pluk
doveden	doveden	k2eAgMnSc1d1	doveden
četníkem	četník	k1gMnSc7	četník
jako	jako	k8xC	jako
údajný	údajný	k2eAgMnSc1d1	údajný
zběh	zběh	k1gMnSc1	zběh
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
slovy	slovo	k1gNnPc7	slovo
"	"	kIx"	"
<g/>
Poslušně	poslušně	k6eAd1	poslušně
hlásím	hlásit	k5eAaImIp1nS	hlásit
<g/>
,	,	kIx,	,
pane	pan	k1gMnSc5	pan
obrlajtnant	obrlajtnant	k1gInSc4	obrlajtnant
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsem	být	k5eAaImIp1nS	být
opět	opět	k6eAd1	opět
zde	zde	k6eAd1	zde
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
radostně	radostně	k6eAd1	radostně
hlásí	hlásit	k5eAaImIp3nS	hlásit
u	u	k7c2	u
nadporučíka	nadporučík	k1gMnSc2	nadporučík
Lukáše	Lukáš	k1gMnSc2	Lukáš
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
pluku	pluk	k1gInSc2	pluk
se	se	k3xPyFc4	se
Švejk	Švejk	k1gMnSc1	Švejk
postupně	postupně	k6eAd1	postupně
seznamuje	seznamovat	k5eAaImIp3nS	seznamovat
s	s	k7c7	s
nezapomenutelnými	zapomenutelný	k2eNgFnPc7d1	nezapomenutelná
postavami	postava	k1gFnPc7	postava
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
je	on	k3xPp3gMnPc4	on
účetní	účetní	k1gMnPc4	účetní
šikovatel	šikovatel	k1gMnSc1	šikovatel
Vaněk	Vaněk	k1gMnSc1	Vaněk
<g/>
,	,	kIx,	,
kuchař	kuchař	k1gMnSc1	kuchař
okultista	okultista	k1gMnSc1	okultista
Jurajda	Jurajda	k1gMnSc1	Jurajda
<g/>
,	,	kIx,	,
jednoroční	jednoroční	k2eAgMnSc1d1	jednoroční
dobrovolník	dobrovolník	k1gMnSc1	dobrovolník
Marek	Marek	k1gMnSc1	Marek
nebo	nebo	k8xC	nebo
sapér	sapér	k?	sapér
Vodička	Vodička	k1gMnSc1	Vodička
<g/>
.	.	kIx.	.
</s>
<s>
Setkává	setkávat	k5eAaImIp3nS	setkávat
se	se	k3xPyFc4	se
i	i	k9	i
s	s	k7c7	s
karikaturami	karikatura	k1gFnPc7	karikatura
důstojníků	důstojník	k1gMnPc2	důstojník
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
přičinlivý	přičinlivý	k2eAgMnSc1d1	přičinlivý
až	až	k8xS	až
podlézavý	podlézavý	k2eAgMnSc1d1	podlézavý
kadet	kadet	k1gMnSc1	kadet
Biegler	Biegler	k1gMnSc1	Biegler
nebo	nebo	k8xC	nebo
omezený	omezený	k2eAgMnSc1d1	omezený
poručík	poručík	k1gMnSc1	poručík
Dub	Dub	k1gMnSc1	Dub
<g/>
,	,	kIx,	,
v	v	k7c6	v
civilu	civil	k1gMnSc3	civil
gymnaziální	gymnaziální	k2eAgMnSc1d1	gymnaziální
profesor	profesor	k1gMnSc1	profesor
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
koníčkem	koníček	k1gInSc7	koníček
je	být	k5eAaImIp3nS	být
buzerace	buzerace	k1gFnSc1	buzerace
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
který	který	k3yIgMnSc1	který
svými	svůj	k3xOyFgFnPc7	svůj
radami	rada	k1gFnPc7	rada
otravuje	otravovat	k5eAaImIp3nS	otravovat
život	život	k1gInSc1	život
i	i	k9	i
zkušeným	zkušený	k2eAgMnPc3d1	zkušený
aktivním	aktivní	k2eAgMnPc3d1	aktivní
důstojníkům	důstojník	k1gMnPc3	důstojník
<g/>
,	,	kIx,	,
jakými	jaký	k3yIgMnPc7	jaký
jsou	být	k5eAaImIp3nP	být
hejtman	hejtman	k1gMnSc1	hejtman
Ságner	Ságner	k1gMnSc1	Ságner
nebo	nebo	k8xC	nebo
plukovník	plukovník	k1gMnSc1	plukovník
Schröder	Schröder	k1gMnSc1	Schröder
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbeným	oblíbený	k2eAgNnSc7d1	oblíbené
Dubovým	Dubův	k2eAgNnSc7d1	Dubovo
rčením	rčení	k1gNnSc7	rčení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
ve	v	k7c6	v
vzteku	vztek	k1gInSc6	vztek
pokřikuje	pokřikovat	k5eAaImIp3nS	pokřikovat
na	na	k7c4	na
vojáky	voják	k1gMnPc4	voják
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
Ty	ty	k3xPp2nSc1	ty
mne	já	k3xPp1nSc4	já
ještě	ještě	k9	ještě
neznáš	neznat	k5eAaImIp2nS	neznat
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
až	až	k9	až
mne	já	k3xPp1nSc4	já
poznáš	poznat	k5eAaPmIp2nS	poznat
<g/>
,	,	kIx,	,
já	já	k3xPp1nSc1	já
tě	ty	k3xPp2nSc4	ty
přinutím	přinutit	k5eAaPmIp1nS	přinutit
až	až	k6eAd1	až
k	k	k7c3	k
pláči	pláč	k1gInSc3	pláč
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Švejkův	Švejkův	k2eAgInSc1d1	Švejkův
pluk	pluk	k1gInSc1	pluk
je	být	k5eAaImIp3nS	být
brzy	brzy	k6eAd1	brzy
poslán	poslat	k5eAaPmNgMnS	poslat
na	na	k7c4	na
frontu	fronta	k1gFnSc4	fronta
a	a	k8xC	a
přemístěn	přemístěn	k2eAgMnSc1d1	přemístěn
do	do	k7c2	do
města	město	k1gNnSc2	město
Királyhidy	Királyhida	k1gFnSc2	Királyhida
na	na	k7c6	na
rakousko-uherském	rakouskoherský	k2eAgNnSc6d1	rakousko-uherské
pomezí	pomezí	k1gNnSc6	pomezí
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
nadporučík	nadporučík	k1gMnSc1	nadporučík
Lukáš	Lukáš	k1gMnSc1	Lukáš
opět	opět	k6eAd1	opět
zaplete	zaplést	k5eAaPmIp3nS	zaplést
do	do	k7c2	do
dalšího	další	k2eAgNnSc2d1	další
milostného	milostný	k2eAgNnSc2d1	milostné
dobrodružství	dobrodružství	k1gNnSc2	dobrodružství
s	s	k7c7	s
vdanou	vdaný	k2eAgFnSc7d1	vdaná
paničkou	panička	k1gFnSc7	panička
a	a	k8xC	a
pošle	poslat	k5eAaPmIp3nS	poslat
Švejka	Švejk	k1gMnSc4	Švejk
s	s	k7c7	s
dopisem	dopis	k1gInSc7	dopis
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
vyvolené	vyvolená	k1gFnSc3	vyvolená
<g/>
.	.	kIx.	.
</s>
<s>
Švejk	Švejk	k1gMnSc1	Švejk
doprovázený	doprovázený	k2eAgMnSc1d1	doprovázený
sapérem	sapérem	k?	sapérem
Vodičkou	Vodička	k1gMnSc7	Vodička
se	se	k3xPyFc4	se
dostane	dostat	k5eAaPmIp3nS	dostat
do	do	k7c2	do
rvačky	rvačka	k1gFnSc2	rvačka
s	s	k7c7	s
jejím	její	k3xOp3gMnSc7	její
manželem	manžel	k1gMnSc7	manžel
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
skončí	skončit	k5eAaPmIp3nS	skončit
jako	jako	k9	jako
hromadná	hromadný	k2eAgFnSc1d1	hromadná
pouliční	pouliční	k2eAgFnSc1d1	pouliční
bitka	bitka	k1gFnSc1	bitka
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
jsou	být	k5eAaImIp3nP	být
oba	dva	k4xCgMnPc1	dva
zatčeni	zatčen	k2eAgMnPc1d1	zatčen
<g/>
.	.	kIx.	.
</s>
<s>
Nadporučík	nadporučík	k1gMnSc1	nadporučík
Lukáš	Lukáš	k1gMnSc1	Lukáš
si	se	k3xPyFc3	se
musí	muset	k5eAaImIp3nS	muset
najít	najít	k5eAaPmF	najít
nového	nový	k2eAgMnSc4d1	nový
sluhu	sluha	k1gMnSc4	sluha
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnSc7	který
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
věčně	věčně	k6eAd1	věčně
nenažraný	nažraný	k2eNgMnSc1d1	nenažraný
tlustý	tlustý	k2eAgMnSc1d1	tlustý
obr	obr	k1gMnSc1	obr
Baloun	Baloun	k1gMnSc1	Baloun
<g/>
,	,	kIx,	,
neustále	neustále	k6eAd1	neustále
ztrpčující	ztrpčující	k2eAgFnSc1d1	ztrpčující
svému	svůj	k1gMnSc3	svůj
pánovi	pán	k1gMnSc3	pán
život	život	k1gInSc4	život
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
mu	on	k3xPp3gMnSc3	on
všechno	všechen	k3xTgNnSc4	všechen
sní	snít	k5eAaImIp3nS	snít
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
propuštění	propuštění	k1gNnSc6	propuštění
se	se	k3xPyFc4	se
Švejk	Švejk	k1gMnSc1	Švejk
stane	stanout	k5eAaPmIp3nS	stanout
ordonancem	ordonancem	k?	ordonancem
(	(	kIx(	(
<g/>
=	=	kIx~	=
vojenským	vojenský	k2eAgMnSc7d1	vojenský
poslem	posel	k1gMnSc7	posel
<g/>
)	)	kIx)	)
své	své	k1gNnSc4	své
11	[number]	k4	11
<g/>
.	.	kIx.	.
marškumpanie	marškumpanie	k1gFnSc1	marškumpanie
a	a	k8xC	a
loučí	loučit	k5eAaImIp3nS	loučit
se	se	k3xPyFc4	se
se	s	k7c7	s
sapérem	sapérem	k?	sapérem
(	(	kIx(	(
<g/>
=	=	kIx~	=
ženistou	ženista	k1gMnSc7	ženista
<g/>
)	)	kIx)	)
Vodičkou	Vodička	k1gMnSc7	Vodička
slovy	slovo	k1gNnPc7	slovo
"	"	kIx"	"
<g/>
Až	až	k9	až
bude	být	k5eAaImBp3nS	být
po	po	k7c6	po
tý	tý	k0	tý
vojně	vojna	k1gFnSc6	vojna
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
mě	já	k3xPp1nSc4	já
přijeď	přijet	k5eAaPmRp2nS	přijet
navštívit	navštívit	k5eAaPmF	navštívit
<g/>
.	.	kIx.	.
</s>
<s>
Najdeš	najít	k5eAaPmIp2nS	najít
mě	já	k3xPp1nSc4	já
každej	každej	k?	každej
večer	večer	k6eAd1	večer
od	od	k7c2	od
šesti	šest	k4xCc2	šest
hodin	hodina	k1gFnPc2	hodina
u	u	k7c2	u
Kalicha	kalich	k1gInSc2	kalich
na	na	k7c6	na
Bojišti	bojiště	k1gNnSc6	bojiště
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
===	===	k?	===
Slavný	slavný	k2eAgInSc1d1	slavný
výprask	výprask	k1gInSc1	výprask
===	===	k?	===
</s>
</p>
<p>
<s>
91	[number]	k4	91
<g/>
.	.	kIx.	.
pěší	pěší	k2eAgInSc1d1	pěší
pluk	pluk	k1gInSc1	pluk
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
naložen	naložit	k5eAaPmNgInS	naložit
do	do	k7c2	do
vlaku	vlak	k1gInSc2	vlak
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
jej	on	k3xPp3gMnSc4	on
veze	vézt	k5eAaImIp3nS	vézt
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
novou	nový	k2eAgFnSc4d1	nová
skupinu	skupina	k1gFnSc4	skupina
lidí	člověk	k1gMnPc2	člověk
hnaných	hnaný	k2eAgMnPc2d1	hnaný
na	na	k7c4	na
jatky	jatka	k1gFnPc4	jatka
<g/>
"	"	kIx"	"
do	do	k7c2	do
Haliče	Halič	k1gFnSc2	Halič
na	na	k7c4	na
ruskou	ruský	k2eAgFnSc4d1	ruská
frontu	fronta	k1gFnSc4	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Hašek	Hašek	k1gMnSc1	Hašek
tuto	tento	k3xDgFnSc4	tento
pouť	pouť	k1gFnSc4	pouť
přes	přes	k7c4	přes
celé	celý	k2eAgFnPc4d1	celá
Uhry	Uhry	k1gFnPc4	Uhry
popisuje	popisovat	k5eAaImIp3nS	popisovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
román	román	k1gInSc1	román
stává	stávat	k5eAaImIp3nS	stávat
skutečnou	skutečný	k2eAgFnSc7d1	skutečná
obžalobou	obžaloba	k1gFnSc7	obžaloba
války	válka	k1gFnSc2	válka
a	a	k8xC	a
stupidních	stupidní	k2eAgMnPc2d1	stupidní
důstojníků	důstojník	k1gMnPc2	důstojník
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
nejde	jít	k5eNaImIp3nS	jít
o	o	k7c4	o
nic	nic	k3yNnSc4	nic
jiného	jiný	k2eAgNnSc2d1	jiné
než	než	k8xS	než
o	o	k7c4	o
vlastní	vlastní	k2eAgInSc4d1	vlastní
prospěch	prospěch	k1gInSc4	prospěch
bez	bez	k7c2	bez
ohledu	ohled	k1gInSc2	ohled
na	na	k7c4	na
osudy	osud	k1gInPc4	osud
vojáků	voják	k1gMnPc2	voják
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnPc3	který
mají	mít	k5eAaImIp3nP	mít
velet	velet	k5eAaImF	velet
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc1	jejich
schopnosti	schopnost	k1gFnSc2	schopnost
Hašek	Hašek	k1gMnSc1	Hašek
popisuje	popisovat	k5eAaImIp3nS	popisovat
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Byl	být	k5eAaImAgMnS	být
to	ten	k3xDgNnSc4	ten
pán	pán	k1gMnSc1	pán
velkých	velký	k2eAgFnPc2d1	velká
vojenských	vojenský	k2eAgFnPc2d1	vojenská
schopností	schopnost	k1gFnPc2	schopnost
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
vrazily	vrazit	k5eAaPmAgFnP	vrazit
do	do	k7c2	do
nohou	noha	k1gFnPc2	noha
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
podágry	podágry	k?	podágry
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Názory	názor	k1gInPc1	názor
prostých	prostý	k2eAgMnPc2d1	prostý
vojáků	voják	k1gMnPc2	voják
na	na	k7c4	na
válku	válka	k1gFnSc4	válka
charakterizuje	charakterizovat	k5eAaBmIp3nS	charakterizovat
Hašek	Hašek	k1gMnSc1	Hašek
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
železniční	železniční	k2eAgFnSc2d1	železniční
přepravy	přeprava	k1gFnSc2	přeprava
bataliónu	batalión	k1gInSc2	batalión
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
měl	mít	k5eAaImAgInS	mít
sklízet	sklízet	k5eAaImF	sklízet
válečnou	válečný	k2eAgFnSc4d1	válečná
slávu	sláva	k1gFnSc4	sláva
<g/>
,	,	kIx,	,
až	až	k6eAd1	až
projde	projít	k5eAaPmIp3nS	projít
pěšky	pěšky	k6eAd1	pěšky
od	od	k7c2	od
Laborce	laborka	k1gFnSc6	laborka
východní	východní	k2eAgFnSc6d1	východní
Haličí	Halič	k1gFnSc7	Halič
na	na	k7c4	na
front	front	k1gInSc4	front
<g/>
,	,	kIx,	,
vedly	vést	k5eAaImAgInP	vést
se	se	k3xPyFc4	se
ve	v	k7c6	v
vagónu	vagón	k1gInSc6	vagón
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
jednoroční	jednoroční	k2eAgMnSc1d1	jednoroční
dobrovolník	dobrovolník	k1gMnSc1	dobrovolník
a	a	k8xC	a
Švejk	Švejk	k1gMnSc1	Švejk
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
podivné	podivný	k2eAgFnSc2d1	podivná
řeči	řeč	k1gFnSc2	řeč
<g/>
,	,	kIx,	,
víceméně	víceméně	k9	víceméně
velezrádného	velezrádný	k2eAgInSc2d1	velezrádný
obsahu	obsah	k1gInSc2	obsah
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
konci	konec	k1gInSc6	konec
dílu	díl	k1gInSc2	díl
je	být	k5eAaImIp3nS	být
Švejk	Švejk	k1gMnSc1	Švejk
vyslán	vyslat	k5eAaPmNgMnS	vyslat
jako	jako	k9	jako
kvartýrmachr	kvartýrmachr	k1gMnSc1	kvartýrmachr
zajistit	zajistit	k5eAaPmF	zajistit
pro	pro	k7c4	pro
svou	svůj	k3xOyFgFnSc4	svůj
kumpanii	kumpanie	k1gFnSc4	kumpanie
nocleh	nocleh	k1gInSc4	nocleh
ve	v	k7c6	v
Felštýně	Felštýna	k1gFnSc6	Felštýna
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Švejk	Švejk	k1gMnSc1	Švejk
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
malému	malý	k2eAgInSc3d1	malý
rybníku	rybník	k1gInSc3	rybník
<g/>
,	,	kIx,	,
setká	setkat	k5eAaPmIp3nS	setkat
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
s	s	k7c7	s
uprchlým	uprchlý	k2eAgMnSc7d1	uprchlý
ruským	ruský	k2eAgMnSc7d1	ruský
zajatcem	zajatec	k1gMnSc7	zajatec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
tam	tam	k6eAd1	tam
koupal	koupat	k5eAaImAgMnS	koupat
a	a	k8xC	a
po	po	k7c6	po
spatření	spatření	k1gNnSc6	spatření
Švejka	Švejk	k1gMnSc2	Švejk
se	se	k3xPyFc4	se
dal	dát	k5eAaPmAgMnS	dát
nahý	nahý	k2eAgMnSc1d1	nahý
na	na	k7c4	na
útěk	útěk	k1gInSc4	útěk
<g/>
.	.	kIx.	.
</s>
<s>
Švejk	Švejk	k1gMnSc1	Švejk
si	se	k3xPyFc3	se
oblékne	obléknout	k5eAaPmIp3nS	obléknout
jeho	jeho	k3xOp3gFnSc4	jeho
uniformu	uniforma	k1gFnSc4	uniforma
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
zvědav	zvědav	k2eAgMnSc1d1	zvědav
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
by	by	kYmCp3nS	by
mu	on	k3xPp3gMnSc3	on
asi	asi	k9	asi
slušela	slušet	k5eAaImAgFnS	slušet
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
uniformě	uniforma	k1gFnSc6	uniforma
ho	on	k3xPp3gMnSc4	on
najde	najít	k5eAaPmIp3nS	najít
patrola	patrola	k?	patrola
polního	polní	k2eAgNnSc2d1	polní
četnictva	četnictvo	k1gNnSc2	četnictvo
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
tohoto	tento	k3xDgMnSc2	tento
ruského	ruský	k2eAgMnSc2d1	ruský
uprchlíka	uprchlík	k1gMnSc2	uprchlík
hledala	hledat	k5eAaImAgFnS	hledat
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
Maďaři	Maďar	k1gMnPc1	Maďar
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
Švejkovi	Švejkův	k2eAgMnPc1d1	Švejkův
nerozumí	rozumět	k5eNaImIp3nP	rozumět
a	a	k8xC	a
přes	přes	k7c4	přes
jeho	jeho	k3xOp3gInPc4	jeho
protesty	protest	k1gInPc4	protest
ho	on	k3xPp3gMnSc4	on
zařadí	zařadit	k5eAaPmIp3nS	zařadit
do	do	k7c2	do
transportu	transport	k1gInSc2	transport
ruských	ruský	k2eAgMnPc2d1	ruský
zajatců	zajatec	k1gMnPc2	zajatec
určených	určený	k2eAgMnPc2d1	určený
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
opravě	oprava	k1gFnSc6	oprava
železniční	železniční	k2eAgFnSc2d1	železniční
trati	trať	k1gFnSc2	trať
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
Švejk	Švejk	k1gMnSc1	Švejk
upadne	upadnout	k5eAaPmIp3nS	upadnout
omylem	omyl	k1gInSc7	omyl
do	do	k7c2	do
rakouského	rakouský	k2eAgNnSc2d1	rakouské
zajetí	zajetí	k1gNnSc2	zajetí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pokračování	pokračování	k1gNnSc1	pokračování
slavného	slavný	k2eAgInSc2d1	slavný
výprasku	výprask	k1gInSc2	výprask
===	===	k?	===
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
je	být	k5eAaImIp3nS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Švejk	Švejk	k1gMnSc1	Švejk
je	být	k5eAaImIp3nS	být
Čech	Čech	k1gMnSc1	Čech
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
něj	on	k3xPp3gNnSc4	on
pohlíženo	pohlížen	k2eAgNnSc4d1	pohlíženo
jako	jako	k8xS	jako
na	na	k7c4	na
zběha	zběh	k1gMnSc4	zběh
a	a	k8xC	a
polní	polní	k2eAgInSc4d1	polní
soud	soud	k1gInSc4	soud
ho	on	k3xPp3gMnSc4	on
odsoudí	odsoudit	k5eAaPmIp3nS	odsoudit
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Duchovní	duchovní	k2eAgFnSc4d1	duchovní
útěchu	útěcha	k1gFnSc4	útěcha
před	před	k7c7	před
popravou	poprava	k1gFnSc7	poprava
mu	on	k3xPp3gMnSc3	on
má	mít	k5eAaImIp3nS	mít
poskytnout	poskytnout	k5eAaPmF	poskytnout
polní	polní	k2eAgMnSc1d1	polní
kurát	kurát	k1gMnSc1	kurát
Martinec	Martinec	k1gMnSc1	Martinec
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
skutečně	skutečně	k6eAd1	skutečně
nábožensky	nábožensky	k6eAd1	nábožensky
založený	založený	k2eAgMnSc1d1	založený
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
stykem	styk	k1gInSc7	styk
s	s	k7c7	s
opilými	opilý	k2eAgMnPc7d1	opilý
důstojníky	důstojník	k1gMnPc7	důstojník
postupně	postupně	k6eAd1	postupně
mravně	mravně	k6eAd1	mravně
upadá	upadat	k5eAaImIp3nS	upadat
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
si	se	k3xPyFc3	se
nepřestává	přestávat	k5eNaImIp3nS	přestávat
v	v	k7c6	v
skrytu	skryt	k1gInSc6	skryt
duše	duše	k1gFnSc2	duše
neustále	neustále	k6eAd1	neustále
vyčítat	vyčítat	k5eAaImF	vyčítat
<g/>
.	.	kIx.	.
</s>
<s>
Švejk	Švejk	k1gMnSc1	Švejk
jej	on	k3xPp3gNnSc4	on
však	však	k9	však
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
dalšího	další	k2eAgMnSc4d1	další
vězně	vězeň	k1gMnSc4	vězeň
a	a	k8xC	a
svými	svůj	k3xOyFgInPc7	svůj
proslovy	proslov	k1gInPc7	proslov
jej	on	k3xPp3gMnSc4	on
tak	tak	k9	tak
vyděsí	vyděsit	k5eAaPmIp3nS	vyděsit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Martinec	Martinec	k1gMnSc1	Martinec
z	z	k7c2	z
cely	cela	k1gFnSc2	cela
prchne	prchnout	k5eAaPmIp3nS	prchnout
na	na	k7c4	na
důstojnický	důstojnický	k2eAgInSc4d1	důstojnický
večírek	večírek	k1gInSc4	večírek
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
zpije	zpít	k5eAaPmIp3nS	zpít
do	do	k7c2	do
bezvědomí	bezvědomí	k1gNnSc2	bezvědomí
"	"	kIx"	"
<g/>
s	s	k7c7	s
přesvědčením	přesvědčení	k1gNnSc7	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
hraje	hrát	k5eAaImIp3nS	hrát
o	o	k7c4	o
jeho	jeho	k3xOp3gFnSc4	jeho
duši	duše	k1gFnSc4	duše
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
mučedníkem	mučedník	k1gMnSc7	mučedník
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
vše	všechen	k3xTgNnSc1	všechen
vysvětlí	vysvětlit	k5eAaPmIp3nS	vysvětlit
a	a	k8xC	a
Švejk	Švejk	k1gMnSc1	Švejk
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
ocitne	ocitnout	k5eAaPmIp3nS	ocitnout
u	u	k7c2	u
své	svůj	k3xOyFgFnSc2	svůj
marškumpanie	marškumpanie	k1gFnSc2	marškumpanie
<g/>
.	.	kIx.	.
</s>
<s>
Haškův	Haškův	k2eAgInSc1d1	Haškův
text	text	k1gInSc1	text
pak	pak	k6eAd1	pak
končí	končit	k5eAaImIp3nS	končit
proslovem	proslov	k1gInSc7	proslov
poručíka	poručík	k1gMnSc2	poručík
Duba	Dub	k1gMnSc2	Dub
"	"	kIx"	"
<g/>
S	s	k7c7	s
okresním	okresní	k2eAgMnSc7d1	okresní
hejtmanem	hejtman	k1gMnSc7	hejtman
jsme	být	k5eAaImIp1nP	být
vždy	vždy	k6eAd1	vždy
říkávali	říkávat	k5eAaImAgMnP	říkávat
<g/>
:	:	kIx,	:
Patriotismus	patriotismus	k1gInSc1	patriotismus
<g/>
,	,	kIx,	,
věrnost	věrnost	k1gFnSc1	věrnost
k	k	k7c3	k
povinnosti	povinnost	k1gFnSc3	povinnost
<g/>
,	,	kIx,	,
sebepřekonání	sebepřekonání	k1gNnSc3	sebepřekonání
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc1	ten
jsou	být	k5eAaImIp3nP	být
ty	ten	k3xDgFnPc1	ten
pravé	pravý	k2eAgFnPc1d1	pravá
zbraně	zbraň	k1gFnPc1	zbraň
ve	v	k7c6	v
válce	válka	k1gFnSc6	válka
<g/>
!	!	kIx.	!
</s>
<s>
Připomínám	připomínat	k5eAaImIp1nS	připomínat
si	se	k3xPyFc3	se
to	ten	k3xDgNnSc4	ten
zejména	zejména	k9	zejména
dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
když	když	k8xS	když
naše	náš	k3xOp1gNnPc1	náš
vojska	vojsko	k1gNnPc1	vojsko
v	v	k7c6	v
dohledné	dohledný	k2eAgFnSc6d1	dohledná
době	doba	k1gFnSc6	doba
překročí	překročit	k5eAaPmIp3nS	překročit
hranice	hranice	k1gFnSc1	hranice
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Vše	všechen	k3xTgNnSc1	všechen
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
případně	případně	k6eAd1	případně
v	v	k7c6	v
knize	kniha	k1gFnSc6	kniha
následuje	následovat	k5eAaImIp3nS	následovat
za	za	k7c7	za
touto	tento	k3xDgFnSc7	tento
větou	věta	k1gFnSc7	věta
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pokračování	pokračování	k1gNnSc1	pokračování
dopsané	dopsaný	k2eAgNnSc1d1	dopsané
od	od	k7c2	od
Karla	Karel	k1gMnSc2	Karel
Vaňka	Vaněk	k1gMnSc2	Vaněk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
Švejkovy	Švejkův	k2eAgFnSc2d1	Švejkova
postavy	postava	k1gFnSc2	postava
==	==	k?	==
</s>
</p>
<p>
<s>
Nápad	nápad	k1gInSc1	nápad
napsat	napsat	k5eAaBmF	napsat
knihu	kniha	k1gFnSc4	kniha
o	o	k7c6	o
dobrovolníkovi	dobrovolník	k1gMnSc6	dobrovolník
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
je	být	k5eAaImIp3nS	být
lehce	lehko	k6eAd1	lehko
slabomyslný	slabomyslný	k2eAgMnSc1d1	slabomyslný
<g/>
,	,	kIx,	,
dostal	dostat	k5eAaPmAgMnS	dostat
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Hašek	Hašek	k1gMnSc1	Hašek
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1911	[number]	k4	1911
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
z	z	k7c2	z
hospody	hospody	k?	hospody
napsal	napsat	k5eAaPmAgMnS	napsat
na	na	k7c4	na
kousek	kousek	k1gInSc4	kousek
papíru	papír	k1gInSc2	papír
slova	slovo	k1gNnSc2	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Pitomec	pitomec	k1gMnSc1	pitomec
u	u	k7c2	u
kumpanie	kumpanie	k1gFnSc2	kumpanie
<g/>
.	.	kIx.	.
</s>
<s>
Dal	dát	k5eAaPmAgMnS	dát
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
vyzkoušet	vyzkoušet	k5eAaPmF	vyzkoušet
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vystupoval	vystupovat	k5eAaImAgMnS	vystupovat
jako	jako	k8xS	jako
řádný	řádný	k2eAgMnSc1d1	řádný
vojín	vojín	k1gMnSc1	vojín
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
V	v	k7c6	v
duchu	duch	k1gMnSc6	duch
svého	svůj	k3xOyFgNnSc2	svůj
přesvědčení	přesvědčení	k1gNnSc2	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
militantní	militantní	k2eAgNnSc4d1	militantní
Rakousko-Uhersko	Rakousko-Uhersko	k1gNnSc4	Rakousko-Uhersko
(	(	kIx(	(
<g/>
a	a	k8xC	a
na	na	k7c4	na
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
totalitní	totalitní	k2eAgInSc4d1	totalitní
systém	systém	k1gInSc4	systém
vůbec	vůbec	k9	vůbec
<g/>
)	)	kIx)	)
nejlépe	dobře	k6eAd3	dobře
platí	platit	k5eAaImIp3nS	platit
pitomec	pitomec	k1gMnSc1	pitomec
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
obrátí	obrátit	k5eAaPmIp3nS	obrátit
blbost	blbost	k1gFnSc4	blbost
představitelů	představitel	k1gMnPc2	představitel
takovéhoto	takovýto	k3xDgInSc2	takovýto
systému	systém	k1gInSc2	systém
proti	proti	k7c3	proti
nim	on	k3xPp3gMnPc3	on
samým	samý	k3xTgMnPc3	samý
<g/>
,	,	kIx,	,
vydal	vydat	k5eAaPmAgMnS	vydat
pak	pak	k6eAd1	pak
Hašek	Hašek	k1gMnSc1	Hašek
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
knihu	kniha	k1gFnSc4	kniha
Dobrý	dobrý	k2eAgMnSc1d1	dobrý
voják	voják	k1gMnSc1	voják
Švejk	Švejk	k1gMnSc1	Švejk
a	a	k8xC	a
jiné	jiný	k2eAgFnPc4d1	jiná
podivné	podivný	k2eAgFnPc4d1	podivná
historky	historka	k1gFnPc4	historka
<g/>
.	.	kIx.	.
</s>
<s>
Švejkova	Švejkův	k2eAgFnSc1d1	Švejkova
pitomost	pitomost	k1gFnSc1	pitomost
však	však	k9	však
již	již	k6eAd1	již
tehdy	tehdy	k6eAd1	tehdy
byla	být	k5eAaImAgFnS	být
poněkud	poněkud	k6eAd1	poněkud
odlišná	odlišný	k2eAgFnSc1d1	odlišná
od	od	k7c2	od
pitomosti	pitomost	k1gFnSc2	pitomost
běžného	běžný	k2eAgMnSc2d1	běžný
blbce	blbec	k1gMnSc2	blbec
<g/>
.	.	kIx.	.
</s>
<s>
Švejk	Švejk	k1gMnSc1	Švejk
dokázal	dokázat	k5eAaPmAgMnS	dokázat
vrhnout	vrhnout	k5eAaPmF	vrhnout
na	na	k7c4	na
důstojníka	důstojník	k1gMnSc4	důstojník
takový	takový	k3xDgInSc4	takový
"	"	kIx"	"
<g/>
dětský	dětský	k2eAgInSc4d1	dětský
jasný	jasný	k2eAgInSc4d1	jasný
pohled	pohled	k1gInSc4	pohled
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
že	že	k8xS	že
důstojník	důstojník	k1gMnSc1	důstojník
nevěděl	vědět	k5eNaImAgMnS	vědět
"	"	kIx"	"
<g/>
má	mít	k5eAaImIp3nS	mít
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
smát	smát	k5eAaImF	smát
či	či	k8xC	či
zlobit	zlobit	k5eAaImF	zlobit
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Švejkova	Švejkův	k2eAgFnSc1d1	Švejkova
blbost	blbost	k1gFnSc1	blbost
tak	tak	k6eAd1	tak
dokázala	dokázat	k5eAaPmAgFnS	dokázat
nejen	nejen	k6eAd1	nejen
odzbrojit	odzbrojit	k5eAaPmF	odzbrojit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přimět	přimět	k5eAaPmF	přimět
i	i	k9	i
k	k	k7c3	k
zamyšlení	zamyšlení	k1gNnSc3	zamyšlení
nad	nad	k7c7	nad
sebou	se	k3xPyFc7	se
samým	samý	k3xTgMnSc7	samý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Povídkový	povídkový	k2eAgInSc1d1	povídkový
náčrt	náčrt	k1gInSc1	náčrt
Švejkovy	Švejkův	k2eAgFnSc2d1	Švejkova
postavy	postava	k1gFnSc2	postava
Hašek	Hašek	k1gMnSc1	Hašek
hojně	hojně	k6eAd1	hojně
využíval	využívat	k5eAaImAgMnS	využívat
i	i	k9	i
při	při	k7c6	při
jiných	jiný	k2eAgFnPc6d1	jiná
příležitostech	příležitost	k1gFnPc6	příležitost
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
ve	v	k7c6	v
scénářích	scénář	k1gInPc6	scénář
ke	k	k7c3	k
kabaretním	kabaretní	k2eAgNnPc3d1	kabaretní
vystoupením	vystoupení	k1gNnPc3	vystoupení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
ke	k	k7c3	k
Švejkovi	Švejk	k1gMnSc3	Švejk
vrátil	vrátit	k5eAaPmAgMnS	vrátit
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
ruském	ruský	k2eAgNnSc6d1	ruské
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
tohoto	tento	k3xDgInSc2	tento
roku	rok	k1gInSc2	rok
vydal	vydat	k5eAaPmAgInS	vydat
v	v	k7c6	v
Kyjevě	Kyjev	k1gInSc6	Kyjev
v	v	k7c6	v
knihovničce	knihovnička	k1gFnSc6	knihovnička
časopisu	časopis	k1gInSc2	časopis
Čechoslovan	Čechoslovan	k?	Čechoslovan
novelu	novela	k1gFnSc4	novela
Dobrý	dobrý	k2eAgMnSc1d1	dobrý
voják	voják	k1gMnSc1	voják
Švejk	Švejk	k1gMnSc1	Švejk
v	v	k7c6	v
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
příběhu	příběh	k1gInSc6	příběh
se	se	k3xPyFc4	se
již	již	k6eAd1	již
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
zárodky	zárodek	k1gInPc4	zárodek
budoucího	budoucí	k2eAgInSc2d1	budoucí
románu	román	k1gInSc2	román
<g/>
.	.	kIx.	.
</s>
<s>
Líčí	líčit	k5eAaImIp3nS	líčit
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
války	válka	k1gFnSc2	válka
a	a	k8xC	a
Švejkovu	Švejkův	k2eAgFnSc4d1	Švejkova
snahu	snaha	k1gFnSc4	snaha
dostat	dostat	k5eAaPmF	dostat
se	se	k3xPyFc4	se
na	na	k7c4	na
vojnu	vojna	k1gFnSc4	vojna
<g/>
,	,	kIx,	,
pobyt	pobyt	k1gInSc4	pobyt
ve	v	k7c6	v
vojenském	vojenský	k2eAgNnSc6d1	vojenské
vězení	vězení	k1gNnSc6	vězení
a	a	k8xC	a
v	v	k7c6	v
blázinci	blázinec	k1gInSc6	blázinec
<g/>
,	,	kIx,	,
přidělení	přidělení	k1gNnSc6	přidělení
k	k	k7c3	k
91	[number]	k4	91
<g/>
.	.	kIx.	.
pěšímu	pěší	k2eAgInSc3d1	pěší
pluku	pluk	k1gInSc3	pluk
a	a	k8xC	a
služba	služba	k1gFnSc1	služba
vojenského	vojenský	k2eAgMnSc2d1	vojenský
sluhy	sluha	k1gMnSc2	sluha
u	u	k7c2	u
sadistického	sadistický	k2eAgMnSc2d1	sadistický
fenricha	fenrich	k1gMnSc2	fenrich
Dauerlinga	Dauerling	k1gMnSc2	Dauerling
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
nenávidí	návidět	k5eNaImIp3nP	návidět
Slovany	Slovan	k1gInPc1	Slovan
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
i	i	k9	i
pověstná	pověstný	k2eAgFnSc1d1	pověstná
příhoda	příhoda	k1gFnSc1	příhoda
s	s	k7c7	s
ukradeným	ukradený	k2eAgMnSc7d1	ukradený
psem	pes	k1gMnSc7	pes
<g/>
,	,	kIx,	,
po	po	k7c6	po
které	který	k3yIgFnSc6	který
následuje	následovat	k5eAaImIp3nS	následovat
odjezd	odjezd	k1gInSc1	odjezd
na	na	k7c4	na
frontu	fronta	k1gFnSc4	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
samém	samý	k3xTgInSc6	samý
konci	konec	k1gInSc6	konec
příběhu	příběh	k1gInSc2	příběh
pak	pak	k8xC	pak
Švejk	Švejk	k1gMnSc1	Švejk
Dauerlinga	Dauerlinga	k1gFnSc1	Dauerlinga
nechtěně	chtěně	k6eNd1	chtěně
zastřelí	zastřelit	k5eAaPmIp3nS	zastřelit
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
chce	chtít	k5eAaImIp3nS	chtít
jen	jen	k9	jen
prostřelit	prostřelit	k5eAaPmF	prostřelit
rameno	rameno	k1gNnSc4	rameno
<g/>
,	,	kIx,	,
o	o	k7c6	o
což	což	k3yRnSc1	což
jej	on	k3xPp3gInSc4	on
Dauerling	Dauerling	k1gInSc4	Dauerling
sám	sám	k3xTgInSc4	sám
požádal	požádat	k5eAaPmAgInS	požádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
dostal	dostat	k5eAaPmAgMnS	dostat
z	z	k7c2	z
bojiště	bojiště	k1gNnSc2	bojiště
<g/>
.	.	kIx.	.
</s>
<s>
Švejk	Švejk	k1gMnSc1	Švejk
pak	pak	k6eAd1	pak
přeběhne	přeběhnout	k5eAaPmIp3nS	přeběhnout
k	k	k7c3	k
Rusům	Rus	k1gMnPc3	Rus
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
vlasti	vlast	k1gFnSc6	vlast
obviněn	obvinit	k5eAaPmNgMnS	obvinit
z	z	k7c2	z
velezrady	velezrada	k1gFnSc2	velezrada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
románu	román	k1gInSc6	román
Osudy	osud	k1gInPc1	osud
dobrého	dobrý	k2eAgMnSc2d1	dobrý
vojáka	voják	k1gMnSc2	voják
Švejka	Švejk	k1gMnSc2	Švejk
se	se	k3xPyFc4	se
Švejkova	Švejkův	k2eAgFnSc1d1	Švejkova
postava	postava	k1gFnSc1	postava
dostala	dostat	k5eAaPmAgFnS	dostat
na	na	k7c4	na
úplně	úplně	k6eAd1	úplně
jinou	jiný	k2eAgFnSc4d1	jiná
<g/>
,	,	kIx,	,
podstatně	podstatně	k6eAd1	podstatně
hlubší	hluboký	k2eAgFnSc4d2	hlubší
úroveň	úroveň	k1gFnSc4	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Nejde	jít	k5eNaImIp3nS	jít
zde	zde	k6eAd1	zde
již	již	k6eAd1	již
vůbec	vůbec	k9	vůbec
o	o	k7c4	o
jednoznačného	jednoznačný	k2eAgMnSc4d1	jednoznačný
blba	blb	k1gMnSc4	blb
<g/>
.	.	kIx.	.
</s>
<s>
Švejk	Švejk	k1gMnSc1	Švejk
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
pozoruhodný	pozoruhodný	k2eAgInSc1d1	pozoruhodný
právě	právě	k9	právě
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
u	u	k7c2	u
něj	on	k3xPp3gMnSc2	on
instinktivní	instinktivní	k2eAgFnSc4d1	instinktivní
statečnost	statečnost	k1gFnSc4	statečnost
a	a	k8xC	a
jakýsi	jakýsi	k3yIgInSc4	jakýsi
smysl	smysl	k1gInSc4	smysl
pro	pro	k7c4	pro
pořádek	pořádek	k1gInSc4	pořádek
nepojí	pojíst	k5eNaPmIp3nS	pojíst
s	s	k7c7	s
očekávanou	očekávaný	k2eAgFnSc7d1	očekávaná
hloupostí	hloupost	k1gFnSc7	hloupost
nebo	nebo	k8xC	nebo
nedostatkem	nedostatek	k1gInSc7	nedostatek
smyslu	smysl	k1gInSc2	smysl
pro	pro	k7c4	pro
realitu	realita	k1gFnSc4	realita
<g/>
.	.	kIx.	.
</s>
<s>
Švejkova	Švejkův	k2eAgFnSc1d1	Švejkova
osobnost	osobnost	k1gFnSc1	osobnost
<g/>
,	,	kIx,	,
tak	tak	k9	tak
jak	jak	k8xS	jak
ji	on	k3xPp3gFnSc4	on
Hašek	Hašek	k1gMnSc1	Hašek
představuje	představovat	k5eAaImIp3nS	představovat
<g/>
,	,	kIx,	,
překvapuje	překvapovat	k5eAaImIp3nS	překvapovat
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
neutuchajícím	utuchající	k2eNgInSc7d1	neutuchající
životním	životní	k2eAgInSc7d1	životní
elánem	elán	k1gInSc7	elán
<g/>
,	,	kIx,	,
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
druhé	druhý	k4xOgFnSc6	druhý
jakousi	jakýsi	k3yIgFnSc7	jakýsi
životní	životní	k2eAgFnSc7d1	životní
rezignací	rezignace	k1gFnSc7	rezignace
<g/>
,	,	kIx,	,
odevzdaností	odevzdanost	k1gFnSc7	odevzdanost
a	a	k8xC	a
smířením	smíření	k1gNnSc7	smíření
se	se	k3xPyFc4	se
s	s	k7c7	s
osudem	osud	k1gInSc7	osud
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
by	by	kYmCp3nP	by
Švejkovi	Švejk	k1gMnSc3	Švejk
scházely	scházet	k5eAaImAgInP	scházet
ambice	ambice	k1gFnPc4	ambice
a	a	k8xC	a
životní	životní	k2eAgInPc4d1	životní
cíle	cíl	k1gInPc4	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
paradox	paradox	k1gInSc1	paradox
vyvolává	vyvolávat	k5eAaImIp3nS	vyvolávat
pochybnosti	pochybnost	k1gFnPc4	pochybnost
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
Švejk	Švejk	k1gMnSc1	Švejk
opravdu	opravdu	k6eAd1	opravdu
jedná	jednat	k5eAaImIp3nS	jednat
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
si	se	k3xPyFc3	se
myslí	myslet	k5eAaImIp3nS	myslet
<g/>
.	.	kIx.	.
</s>
<s>
Švejk	Švejk	k1gMnSc1	Švejk
se	se	k3xPyFc4	se
umí	umět	k5eAaImIp3nS	umět
vysmívat	vysmívat	k5eAaImF	vysmívat
<g/>
,	,	kIx,	,
dělá	dělat	k5eAaImIp3nS	dělat
to	ten	k3xDgNnSc1	ten
často	často	k6eAd1	často
a	a	k8xC	a
i	i	k9	i
velice	velice	k6eAd1	velice
inteligentně	inteligentně	k6eAd1	inteligentně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
motivací	motivace	k1gFnSc7	motivace
jeho	jeho	k3xOp3gNnSc2	jeho
doslovného	doslovný	k2eAgNnSc2d1	doslovné
plnění	plnění	k1gNnSc2	plnění
příkazů	příkaz	k1gInPc2	příkaz
svých	svůj	k3xOyFgMnPc2	svůj
nadřízených	nadřízený	k1gMnPc2	nadřízený
není	být	k5eNaImIp3nS	být
výsměch	výsměch	k1gInSc4	výsměch
<g/>
,	,	kIx,	,
alespoň	alespoň	k9	alespoň
ne	ne	k9	ne
jeho	jeho	k3xOp3gFnSc4	jeho
vlastní	vlastní	k2eAgFnSc4d1	vlastní
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
Hašek	Hašek	k1gMnSc1	Hašek
sám	sám	k3xTgMnSc1	sám
se	se	k3xPyFc4	se
směje	smát	k5eAaImIp3nS	smát
že	že	k8xS	že
postava	postava	k1gFnSc1	postava
tak	tak	k6eAd1	tak
ctnostná	ctnostný	k2eAgFnSc1d1	ctnostná
jako	jako	k8xS	jako
Švejk	Švejk	k1gMnSc1	Švejk
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
jak	jak	k6eAd1	jak
mimo	mimo	k7c4	mimo
dosah	dosah	k1gInSc4	dosah
pochopení	pochopení	k1gNnSc2	pochopení
svých	svůj	k3xOyFgMnPc2	svůj
současníků	současník	k1gMnPc2	současník
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
mimo	mimo	k7c4	mimo
konstrukční	konstrukční	k2eAgInPc4d1	konstrukční
parametry	parametr	k1gInPc4	parametr
dané	daný	k2eAgFnSc2d1	daná
třídící	třídící	k2eAgFnSc2d1	třídící
a	a	k8xC	a
jateční	jateční	k2eAgFnSc2d1	jateční
linky	linka	k1gFnSc2	linka
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
by	by	kYmCp3nS	by
paradoxně	paradoxně	k6eAd1	paradoxně
zlepšilo	zlepšit	k5eAaPmAgNnS	zlepšit
jeho	jeho	k3xOp3gFnPc4	jeho
šance	šance	k1gFnPc4	šance
válečnou	válečný	k2eAgFnSc4d1	válečná
katastrofu	katastrofa	k1gFnSc4	katastrofa
přežít	přežít	k5eAaPmF	přežít
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
hlavolam	hlavolam	k1gInSc4	hlavolam
autorského	autorský	k2eAgInSc2d1	autorský
záměru	záměr	k1gInSc2	záměr
je	být	k5eAaImIp3nS	být
drogou	droga	k1gFnSc7	droga
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
udržuje	udržovat	k5eAaImIp3nS	udržovat
milióny	milión	k4xCgInPc4	milión
příznivců	příznivec	k1gMnPc2	příznivec
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
ale	ale	k8xC	ale
běžné	běžný	k2eAgNnSc1d1	běžné
se	se	k3xPyFc4	se
setkat	setkat	k5eAaPmF	setkat
se	s	k7c7	s
slovní	slovní	k2eAgFnSc7d1	slovní
formulací	formulace	k1gFnSc7	formulace
jeho	on	k3xPp3gNnSc2	on
řešení	řešení	k1gNnSc2	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
omylem	omylem	k6eAd1	omylem
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
postava	postava	k1gFnSc1	postava
dobrého	dobrý	k2eAgMnSc2d1	dobrý
vojáka	voják	k1gMnSc2	voják
Švejka	Švejk	k1gMnSc2	Švejk
občas	občas	k6eAd1	občas
ukazována	ukazován	k2eAgFnSc1d1	ukazována
jako	jako	k8xS	jako
typický	typický	k2eAgInSc1d1	typický
příklad	příklad	k1gInSc1	příklad
české	český	k2eAgFnSc2d1	Česká
národní	národní	k2eAgFnSc2d1	národní
povahy	povaha	k1gFnSc2	povaha
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
velice	velice	k6eAd1	velice
těžké	těžký	k2eAgFnSc3d1	těžká
interpretaci	interpretace	k1gFnSc3	interpretace
své	svůj	k3xOyFgFnSc2	svůj
postavy	postava	k1gFnSc2	postava
sám	sám	k3xTgMnSc1	sám
Hašek	Hašek	k1gMnSc1	Hašek
píše	psát	k5eAaImIp3nS	psát
v	v	k7c6	v
doslovu	doslov	k1gInSc6	doslov
k	k	k7c3	k
prvnímu	první	k4xOgInSc3	první
dílu	díl	k1gInSc3	díl
románu	román	k1gInSc2	román
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nevím	vědět	k5eNaImIp1nS	vědět
<g/>
,	,	kIx,	,
podaří	podařit	k5eAaPmIp3nS	podařit
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
mi	já	k3xPp1nSc3	já
vystihnout	vystihnout	k5eAaPmF	vystihnout
touto	tento	k3xDgFnSc7	tento
knihou	kniha	k1gFnSc7	kniha
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
jsem	být	k5eAaImIp1nS	být
chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
.	.	kIx.	.
</s>
<s>
Již	jenž	k3xRgFnSc4	jenž
okolnost	okolnost	k1gFnSc4	okolnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
slyšel	slyšet	k5eAaImAgInS	slyšet
jsem	být	k5eAaImIp1nS	být
jednoho	jeden	k4xCgMnSc4	jeden
člověka	člověk	k1gMnSc4	člověk
nadávat	nadávat	k5eAaImF	nadávat
druhému	druhý	k4xOgNnSc3	druhý
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ty	ty	k3xPp2nSc1	ty
jsi	být	k5eAaImIp2nS	být
blbej	blbej	k?	blbej
jako	jako	k8xS	jako
Švejk	Švejk	k1gMnSc1	Švejk
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
právě	právě	k6eAd1	právě
tomu	ten	k3xDgMnSc3	ten
nenasvědčuje	nasvědčovat	k5eNaImIp3nS	nasvědčovat
<g/>
.	.	kIx.	.
</s>
<s>
Stane	stanout	k5eAaPmIp3nS	stanout
<g/>
-li	i	k?	-li
se	se	k3xPyFc4	se
však	však	k9	však
slovo	slovo	k1gNnSc1	slovo
Švejk	Švejk	k1gMnSc1	Švejk
novou	nový	k2eAgFnSc7d1	nová
nadávkou	nadávka	k1gFnSc7	nadávka
v	v	k7c6	v
květnatém	květnatý	k2eAgInSc6d1	květnatý
věnci	věnec	k1gInSc6	věnec
spílání	spílání	k1gNnSc2	spílání
<g/>
,	,	kIx,	,
musím	muset	k5eAaImIp1nS	muset
se	se	k3xPyFc4	se
spokojit	spokojit	k5eAaPmF	spokojit
tímto	tento	k3xDgNnSc7	tento
obohacením	obohacení	k1gNnSc7	obohacení
českého	český	k2eAgInSc2d1	český
jazyka	jazyk	k1gInSc2	jazyk
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Přinejmenším	přinejmenším	k6eAd1	přinejmenším
tuto	tento	k3xDgFnSc4	tento
zásluhu	zásluha	k1gFnSc4	zásluha
dnes	dnes	k6eAd1	dnes
Haškovi	Haškův	k2eAgMnPc1d1	Haškův
nelze	lze	k6eNd1	lze
upřít	upřít	k5eAaPmF	upřít
–	–	k?	–
slova	slovo	k1gNnPc1	slovo
švejk	švejk	k1gMnSc1	švejk
<g/>
,	,	kIx,	,
švejkovina	švejkovina	k1gFnSc1	švejkovina
nebo	nebo	k8xC	nebo
švejkovství	švejkovství	k1gNnSc4	švejkovství
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
(	(	kIx(	(
<g/>
v	v	k7c6	v
přeneseném	přenesený	k2eAgInSc6d1	přenesený
významu	význam	k1gInSc6	význam
<g/>
)	)	kIx)	)
běžnou	běžný	k2eAgFnSc7d1	běžná
součástí	součást	k1gFnSc7	součást
české	český	k2eAgFnSc2d1	Česká
slovní	slovní	k2eAgFnSc2d1	slovní
zásoby	zásoba	k1gFnSc2	zásoba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Těžko	těžko	k6eAd1	těžko
lze	lze	k6eAd1	lze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
Švejkova	Švejkův	k2eAgFnSc1d1	Švejkova
postava	postava	k1gFnSc1	postava
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
dál	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
kdyby	kdyby	kYmCp3nS	kdyby
Hašek	Hašek	k1gMnSc1	Hašek
svůj	svůj	k3xOyFgInSc4	svůj
román	román	k1gInSc4	román
dokončil	dokončit	k5eAaPmAgMnS	dokončit
<g/>
.	.	kIx.	.
</s>
<s>
Maličko	maličko	k6eAd1	maličko
soudit	soudit	k5eAaImF	soudit
lze	lze	k6eAd1	lze
snad	snad	k9	snad
jen	jen	k9	jen
podle	podle	k7c2	podle
autorova	autorův	k2eAgInSc2d1	autorův
spisu	spis	k1gInSc2	spis
Velitelem	velitel	k1gMnSc7	velitel
města	město	k1gNnSc2	město
Bugulmy	Bugulma	k1gFnSc2	Bugulma
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
popisuje	popisovat	k5eAaImIp3nS	popisovat
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
rudým	rudý	k2eAgMnSc7d1	rudý
komisařem	komisař	k1gMnSc7	komisař
v	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Adaptace	adaptace	k1gFnSc1	adaptace
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Pokračování	pokračování	k1gNnSc4	pokračování
od	od	k7c2	od
jiných	jiný	k2eAgMnPc2d1	jiný
autorů	autor	k1gMnPc2	autor
===	===	k?	===
</s>
</p>
<p>
<s>
Již	již	k9	již
první	první	k4xOgNnSc1	první
vydání	vydání	k1gNnSc1	vydání
Haškem	Hašek	k1gMnSc7	Hašek
nedokončeného	dokončený	k2eNgNnSc2d1	nedokončené
díla	dílo	k1gNnSc2	dílo
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
s	s	k7c7	s
pokračováním	pokračování	k1gNnSc7	pokračování
Karla	Karel	k1gMnSc2	Karel
Vaňka	Vaněk	k1gMnSc2	Vaněk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
dopsal	dopsat	k5eAaPmAgInS	dopsat
díl	díl	k1gInSc1	díl
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
a	a	k8xC	a
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
samém	samý	k3xTgInSc6	samý
roce	rok	k1gInSc6	rok
vydal	vydat	k5eAaPmAgInS	vydat
ještě	ještě	k9	ještě
díl	díl	k1gInSc1	díl
pátý	pátý	k4xOgMnSc1	pátý
a	a	k8xC	a
šestý	šestý	k4xOgMnSc1	šestý
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
Vaňkovo	Vaňkův	k2eAgNnSc4d1	Vaňkovo
pokračování	pokračování	k1gNnSc4	pokračování
románu	román	k1gInSc2	román
Osudy	osud	k1gInPc1	osud
dobrého	dobrý	k2eAgMnSc2d1	dobrý
vojáka	voják	k1gMnSc2	voják
Švejka	Švejk	k1gMnSc2	Švejk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Využití	využití	k1gNnSc1	využití
Švejkovy	Švejkův	k2eAgFnSc2d1	Švejkova
postavy	postava	k1gFnSc2	postava
ke	k	k7c3	k
kritice	kritika	k1gFnSc3	kritika
komunismu	komunismus	k1gInSc2	komunismus
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
kniha	kniha	k1gFnSc1	kniha
Osudy	osud	k1gInPc1	osud
dobrého	dobrý	k2eAgMnSc2d1	dobrý
vojáka	voják	k1gMnSc2	voják
Švejka	Švejk	k1gMnSc2	Švejk
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
od	od	k7c2	od
Josefa	Josef	k1gMnSc2	Josef
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Marka	Marek	k1gMnSc2	Marek
(	(	kIx(	(
<g/>
pseudonym	pseudonym	k1gInSc1	pseudonym
Martina	Martin	k1gMnSc2	Martin
Petišky	Petiška	k1gFnSc2	Petiška
<g/>
)	)	kIx)	)
napsaná	napsaný	k2eAgFnSc1d1	napsaná
v	v	k7c6	v
době	doba	k1gFnSc6	doba
normalizace	normalizace	k1gFnSc2	normalizace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Audioknihy	Audioknih	k1gInPc4	Audioknih
===	===	k?	===
</s>
</p>
<p>
<s>
Osudy	osud	k1gInPc1	osud
dobrého	dobrý	k2eAgMnSc2d1	dobrý
vojáka	voják	k1gMnSc2	voják
Švejka	Švejk	k1gMnSc2	Švejk
<g/>
,	,	kIx,	,
Supraphon	supraphon	k1gInSc1	supraphon
<g/>
,	,	kIx,	,
načetl	načíst	k5eAaPmAgMnS	načíst
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
</s>
</p>
<p>
<s>
Osudy	osud	k1gInPc1	osud
dobrého	dobrý	k2eAgMnSc2d1	dobrý
vojáka	voják	k1gMnSc2	voják
Švejka	Švejk	k1gMnSc4	Švejk
<g/>
,	,	kIx,	,
Radioservis	Radioservis	k1gFnSc4	Radioservis
<g/>
,	,	kIx,	,
načetl	načíst	k5eAaBmAgMnS	načíst
Oldřich	Oldřich	k1gMnSc1	Oldřich
Kaiser	Kaiser	k1gMnSc1	Kaiser
</s>
</p>
<p>
<s>
Osudy	osud	k1gInPc1	osud
dobrého	dobrý	k2eAgMnSc2d1	dobrý
vojáka	voják	k1gMnSc2	voják
Švejka	Švejk	k1gMnSc2	Švejk
<g/>
,	,	kIx,	,
Audiotéka	Audiotéek	k1gMnSc2	Audiotéek
<g/>
,	,	kIx,	,
203	[number]	k4	203
lidí	člověk	k1gMnPc2	člověk
za	za	k7c4	za
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
načetlo	načíst	k5eAaPmAgNnS	načíst
první	první	k4xOgInSc4	první
díl	díl	k1gInSc4	díl
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
fóra	fórum	k1gNnSc2	fórum
Meltingpot	Meltingpota	k1gFnPc2	Meltingpota
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bylo	být	k5eAaImAgNnS	být
součástí	součást	k1gFnSc7	součást
festivalu	festival	k1gInSc2	festival
Colours	Colours	k1gInSc1	Colours
of	of	k?	of
Ostrava	Ostrava	k1gFnSc1	Ostrava
<g/>
,	,	kIx,	,
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
audiokniha	audiokniha	k1gFnSc1	audiokniha
s	s	k7c7	s
nejvíce	hodně	k6eAd3	hodně
inteprety	intepret	k1gInPc1	intepret
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
a	a	k8xC	a
byla	být	k5eAaImAgNnP	být
zaznamenána	zaznamenat	k5eAaPmNgNnP	zaznamenat
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
knize	kniha	k1gFnSc6	kniha
rekordů	rekord	k1gInPc2	rekord
pod	pod	k7c7	pod
záštitou	záštita	k1gFnSc7	záštita
pelhřimovské	pelhřimovský	k2eAgFnSc2d1	pelhřimovská
agentury	agentura	k1gFnSc2	agentura
Dobrý	dobrý	k2eAgInSc1d1	dobrý
den	den	k1gInSc1	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Dramatizace	dramatizace	k1gFnSc2	dramatizace
===	===	k?	===
</s>
</p>
<p>
<s>
Emil	Emil	k1gMnSc1	Emil
Artur	Artur	k1gMnSc1	Artur
Longen	Longen	k1gInSc1	Longen
<g/>
:	:	kIx,	:
Dobrý	dobrý	k2eAgMnSc1d1	dobrý
voják	voják	k1gMnSc1	voják
Švejk	Švejk	k1gMnSc1	Švejk
ve	v	k7c6	v
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Felix	Felix	k1gMnSc1	Felix
Bartoš	Bartoš	k1gMnSc1	Bartoš
<g/>
:	:	kIx,	:
Švejk	Švejk	k1gMnSc1	Švejk
v	v	k7c6	v
zákopech	zákop	k1gInPc6	zákop
(	(	kIx(	(
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Felix	Felix	k1gMnSc1	Felix
Bartoš	Bartoš	k1gMnSc1	Bartoš
<g/>
:	:	kIx,	:
Švejk	Švejk	k1gMnSc1	Švejk
válčí	válčit	k5eAaImIp3nS	válčit
v	v	k7c6	v
civilu	civil	k1gMnSc6	civil
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
námětu	námět	k1gInSc2	námět
Karla	Karel	k1gMnSc2	Karel
Vaňka	Vaněk	k1gMnSc2	Vaněk
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Max	Max	k1gMnSc1	Max
Brod	Brod	k1gInSc1	Brod
a	a	k8xC	a
Erwin	Erwin	k1gMnSc1	Erwin
Piscator	Piscator	k1gMnSc1	Piscator
<g/>
:	:	kIx,	:
Abenteuer	Abenteuer	k1gInSc1	Abenteuer
des	des	k1gNnSc2	des
braven	braven	k2eAgInSc1d1	braven
Soldaten	Soldaten	k2eAgInSc1d1	Soldaten
Schwejk	Schwejk	k1gInSc1	Schwejk
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Felix	Felix	k1gMnSc1	Felix
Bartoš	Bartoš	k1gMnSc1	Bartoš
<g/>
:	:	kIx,	:
Švejk	Švejk	k1gMnSc1	Švejk
táhne	táhnout	k5eAaImIp3nS	táhnout
do	do	k7c2	do
boje	boj	k1gInSc2	boj
(	(	kIx(	(
<g/>
1929	[number]	k4	1929
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dle	dle	k7c2	dle
románu	román	k1gInSc2	román
Karla	Karel	k1gMnSc2	Karel
Vaňka	Vaněk	k1gMnSc2	Vaněk
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Emil	Emil	k1gMnSc1	Emil
František	František	k1gMnSc1	František
Burian	Burian	k1gMnSc1	Burian
<g/>
:	:	kIx,	:
Dobrý	dobrý	k2eAgMnSc1d1	dobrý
voják	voják	k1gMnSc1	voják
Švejk	Švejk	k1gMnSc1	Švejk
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Bertolt	Bertolt	k1gMnSc1	Bertolt
Brecht	Brecht	k1gMnSc1	Brecht
<g/>
:	:	kIx,	:
Schweyk	Schweyk	k1gInSc1	Schweyk
im	im	k?	im
Zweiten	Zweiten	k2eAgInSc1d1	Zweiten
Weltkrieg	Weltkrieg	k1gInSc1	Weltkrieg
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
Švejk	Švejk	k1gMnSc1	Švejk
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
)	)	kIx)	)
-	-	kIx~	-
Brechtova	Brechtův	k2eAgFnSc1d1	Brechtova
hra	hra	k1gFnSc1	hra
přesouvá	přesouvat	k5eAaImIp3nS	přesouvat
(	(	kIx(	(
<g/>
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
úpravami	úprava	k1gFnPc7	úprava
<g/>
)	)	kIx)	)
děj	děj	k1gInSc1	děj
Haškova	Haškův	k2eAgMnSc2d1	Haškův
Švejka	Švejk	k1gMnSc2	Švejk
do	do	k7c2	do
prostředí	prostředí	k1gNnSc2	prostředí
protektorátní	protektorátní	k2eAgFnSc2d1	protektorátní
Prahy	Praha	k1gFnSc2	Praha
<g/>
,	,	kIx,	,
nejde	jít	k5eNaImIp3nS	jít
tak	tak	k9	tak
o	o	k7c4	o
dramatizaci	dramatizace	k1gFnSc4	dramatizace
v	v	k7c6	v
pravém	pravý	k2eAgInSc6d1	pravý
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
<g/>
,	,	kIx,	,
spíše	spíše	k9	spíše
o	o	k7c4	o
jakousi	jakýsi	k3yIgFnSc4	jakýsi
osobitou	osobitý	k2eAgFnSc4d1	osobitá
parafrázi	parafráze	k1gFnSc4	parafráze
</s>
</p>
<p>
<s>
Pavel	Pavel	k1gMnSc1	Pavel
Kohout	Kohout	k1gMnSc1	Kohout
<g/>
:	:	kIx,	:
Josef	Josef	k1gMnSc1	Josef
Švejk	Švejk	k1gMnSc1	Švejk
aneb	aneb	k?	aneb
Tak	tak	k6eAd1	tak
nám	my	k3xPp1nPc3	my
zabili	zabít	k5eAaPmAgMnP	zabít
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
a	a	k8xC	a
jiné	jiný	k2eAgInPc4d1	jiný
citáty	citát	k1gInPc4	citát
z	z	k7c2	z
Osudů	osud	k1gInPc2	osud
dobrého	dobrý	k2eAgMnSc2d1	dobrý
vojáka	voják	k1gMnSc2	voják
Švejka	Švejk	k1gMnSc2	Švejk
(	(	kIx(	(
<g/>
1964	[number]	k4	1964
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Postránecký	Postránecký	k2eAgMnSc1d1	Postránecký
<g/>
:	:	kIx,	:
Muzikál	muzikál	k1gInSc1	muzikál
Švejk	Švejk	k1gMnSc1	Švejk
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
BBC	BBC	kA	BBC
Radio	radio	k1gNnSc1	radio
4	[number]	k4	4
vysílalo	vysílat	k5eAaImAgNnS	vysílat
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
dvoudílnou	dvoudílný	k2eAgFnSc4d1	dvoudílná
rozhlasovou	rozhlasový	k2eAgFnSc4d1	rozhlasová
adaptaci	adaptace	k1gFnSc4	adaptace
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Sam	Sam	k1gMnSc1	Sam
Kelly	Kella	k1gFnSc2	Kella
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Filmové	filmový	k2eAgFnSc2d1	filmová
adaptace	adaptace	k1gFnSc2	adaptace
===	===	k?	===
</s>
</p>
<p>
<s>
Pokud	pokud	k8xS	pokud
není	být	k5eNaImIp3nS	být
u	u	k7c2	u
filmu	film	k1gInSc2	film
uvedena	uvést	k5eAaPmNgFnS	uvést
země	země	k1gFnSc1	země
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
československý	československý	k2eAgInSc4d1	československý
snímek	snímek	k1gInSc4	snímek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dobrý	dobrý	k2eAgMnSc1d1	dobrý
voják	voják	k1gMnSc1	voják
Švejk	Švejk	k1gMnSc1	Švejk
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
černobílý	černobílý	k2eAgInSc1d1	černobílý
němý	němý	k2eAgInSc1d1	němý
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Karel	Karel	k1gMnSc1	Karel
Lamač	lamač	k1gMnSc1	lamač
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Karel	Karel	k1gMnSc1	Karel
Noll	Noll	k1gMnSc1	Noll
</s>
</p>
<p>
<s>
Švejk	Švejk	k1gMnSc1	Švejk
na	na	k7c6	na
frontě	fronta	k1gFnSc6	fronta
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
němý	němý	k2eAgInSc1d1	němý
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Karel	Karel	k1gMnSc1	Karel
Lamač	lamač	k1gMnSc1	lamač
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Karel	Karel	k1gMnSc1	Karel
Noll	Noll	k1gMnSc1	Noll
</s>
</p>
<p>
<s>
Švejk	Švejk	k1gMnSc1	Švejk
v	v	k7c6	v
ruském	ruský	k2eAgNnSc6d1	ruské
zajetí	zajetí	k1gNnSc6	zajetí
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1926	[number]	k4	1926
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
němý	němý	k2eAgInSc1d1	němý
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Svatopluk	Svatopluk	k1gMnSc1	Svatopluk
Innemann	Innemann	k1gMnSc1	Innemann
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Karel	Karel	k1gMnSc1	Karel
Noll	Noll	k1gMnSc1	Noll
</s>
</p>
<p>
<s>
Švejk	Švejk	k1gMnSc1	Švejk
v	v	k7c6	v
civilu	civil	k1gMnSc6	civil
(	(	kIx(	(
<g/>
1927	[number]	k4	1927
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
němý	němý	k2eAgInSc1d1	němý
film	film	k1gInSc1	film
podle	podle	k7c2	podle
původního	původní	k2eAgInSc2d1	původní
námětu	námět	k1gInSc2	námět
Karla	Karel	k1gMnSc2	Karel
Vaňka	Vaněk	k1gMnSc2	Vaněk
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Gustav	Gustav	k1gMnSc1	Gustav
Machatý	Machatý	k1gMnSc1	Machatý
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Karel	Karel	k1gMnSc1	Karel
Noll	Noll	k1gMnSc1	Noll
</s>
</p>
<p>
<s>
Dobrý	dobrý	k2eAgMnSc1d1	dobrý
voják	voják	k1gMnSc1	voják
Švejk	Švejk	k1gMnSc1	Švejk
(	(	kIx(	(
<g/>
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
černobílý	černobílý	k2eAgInSc1d1	černobílý
hraný	hraný	k2eAgInSc1d1	hraný
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Martin	Martin	k1gMnSc1	Martin
Frič	Frič	k1gMnSc1	Frič
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Saša	Saša	k1gFnSc1	Saša
Rašilov	Rašilov	k1gInSc4	Rašilov
</s>
</p>
<p>
<s>
Schweik	Schweik	k1gInSc1	Schweik
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
New	New	k1gFnSc7	New
Adventures	Adventures	k1gMnSc1	Adventures
(	(	kIx(	(
<g/>
1943	[number]	k4	1943
<g/>
,	,	kIx,	,
Švejk	Švejk	k1gMnSc1	Švejk
bourá	bourat	k5eAaImIp3nS	bourat
Německo	Německo	k1gNnSc4	Německo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Velká	velký	k2eAgFnSc1d1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
,	,	kIx,	,
černobílý	černobílý	k2eAgInSc1d1	černobílý
hraný	hraný	k2eAgInSc1d1	hraný
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Karel	Karel	k1gMnSc1	Karel
Lamač	lamač	k1gMnSc1	lamač
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Lloyd	Lloyd	k1gMnSc1	Lloyd
Pearson	Pearson	k1gMnSc1	Pearson
<g/>
,	,	kIx,	,
</s>
</p>
<p>
<s>
Dobrý	dobrý	k2eAgMnSc1d1	dobrý
voják	voják	k1gMnSc1	voják
Švejk	Švejk	k1gMnSc1	Švejk
(	(	kIx(	(
<g/>
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
barevný	barevný	k2eAgInSc1d1	barevný
loutkový	loutkový	k2eAgInSc1d1	loutkový
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Jiří	Jiří	k1gMnSc1	Jiří
Trnka	Trnka	k1gMnSc1	Trnka
<g/>
,	,	kIx,	,
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
Jan	Jan	k1gMnSc1	Jan
Werich	Werich	k1gMnSc1	Werich
</s>
</p>
<p>
<s>
Dobrý	dobrý	k2eAgMnSc1d1	dobrý
voják	voják	k1gMnSc1	voják
Švejk	Švejk	k1gMnSc1	Švejk
(	(	kIx(	(
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
barevný	barevný	k2eAgInSc1d1	barevný
hraný	hraný	k2eAgInSc1d1	hraný
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Karel	Karel	k1gMnSc1	Karel
Steklý	Steklý	k1gMnSc1	Steklý
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Rudolf	Rudolfa	k1gFnPc2	Rudolfa
Hrušínský	Hrušínský	k2eAgInSc1d1	Hrušínský
</s>
</p>
<p>
<s>
Poslušně	poslušně	k6eAd1	poslušně
hlásím	hlásit	k5eAaImIp1nS	hlásit
(	(	kIx(	(
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
barevný	barevný	k2eAgInSc4d1	barevný
hraný	hraný	k2eAgInSc4d1	hraný
film	film	k1gInSc4	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnPc4	režie
Karel	Karel	k1gMnSc1	Karel
Steklý	Steklý	k1gMnSc1	Steklý
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Rudolf	Rudolf	k1gMnSc1	Rudolf
Hrušínský	Hrušínský	k2eAgInSc4d1	Hrušínský
<g/>
,	,	kIx,	,
pokračování	pokračování	k1gNnSc3	pokračování
předcházejícího	předcházející	k2eAgInSc2d1	předcházející
snímku	snímek	k1gInSc2	snímek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
</s>
</p>
<p>
<s>
Der	drát	k5eAaImRp2nS	drát
brave	brav	k1gInSc5	brav
Soldat	Soldat	k1gMnSc7	Soldat
Schwejk	Schwejk	k1gMnSc1	Schwejk
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
,	,	kIx,	,
Dobrý	dobrý	k2eAgMnSc1d1	dobrý
voják	voják	k1gMnSc1	voják
Švejk	Švejk	k1gMnSc1	Švejk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
černobílý	černobílý	k2eAgInSc1d1	černobílý
film	film	k1gInSc1	film
ze	z	k7c2	z
Spolkové	spolkový	k2eAgFnSc2d1	spolková
republiky	republika	k1gFnSc2	republika
Německo	Německo	k1gNnSc1	Německo
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Axel	Axel	k1gInSc1	Axel
von	von	k1gInSc1	von
Ambesser	Ambesser	k1gInSc4	Ambesser
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Heinz	Heinz	k1gMnSc1	Heinz
Rühmann	Rühmann	k1gMnSc1	Rühmann
</s>
</p>
<p>
<s>
Schwejks	Schwejksit	k5eAaPmRp2nS	Schwejksit
Flegeljahre	Flegeljahr	k1gMnSc5	Flegeljahr
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
,	,	kIx,	,
Švejkova	Švejkův	k2eAgNnPc1d1	Švejkovo
klackovitá	klackovitý	k2eAgNnPc1d1	klackovité
léta	léto	k1gNnPc1	léto
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
černobílý	černobílý	k2eAgInSc1d1	černobílý
film	film	k1gInSc1	film
z	z	k7c2	z
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Liebeneiner	Liebeneiner	k1gMnSc1	Liebeneiner
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Peter	Peter	k1gMnSc1	Peter
Alexander	Alexandra	k1gFnPc2	Alexandra
</s>
</p>
<p>
<s>
Die	Die	k?	Die
Abenteuer	Abenteuer	k1gInSc1	Abenteuer
des	des	k1gNnSc2	des
braven	braven	k2eAgInSc1d1	braven
Soldaten	Soldaten	k2eAgInSc1d1	Soldaten
Schwejk	Schwejk	k1gInSc1	Schwejk
(	(	kIx(	(
<g/>
1972	[number]	k4	1972
<g/>
–	–	k?	–
<g/>
1976	[number]	k4	1976
<g/>
,	,	kIx,	,
Osudy	osud	k1gInPc1	osud
dobrého	dobrý	k2eAgMnSc2d1	dobrý
vojáka	voják	k1gMnSc2	voják
Švejka	Švejk	k1gMnSc2	Švejk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
13	[number]	k4	13
<g/>
dílný	dílný	k2eAgInSc1d1	dílný
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
Liebeneiner	Liebeneiner	k1gMnSc1	Liebeneiner
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Fritz	Fritz	k1gMnSc1	Fritz
Muliar	Muliar	k1gMnSc1	Muliar
</s>
</p>
<p>
<s>
Osudy	osud	k1gInPc1	osud
dobrého	dobrý	k2eAgMnSc2d1	dobrý
vojáka	voják	k1gMnSc2	voják
Švejka	Švejk	k1gMnSc4	Švejk
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1986	[number]	k4	1986
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
loutkový	loutkový	k2eAgInSc1d1	loutkový
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Stanislav	Stanislav	k1gMnSc1	Stanislav
Látal	Látal	k1gMnSc1	Látal
<g/>
,	,	kIx,	,
postavu	postava	k1gFnSc4	postava
Švejka	Švejk	k1gMnSc2	Švejk
mluví	mluvit	k5eAaImIp3nS	mluvit
Petr	Petr	k1gMnSc1	Petr
Haničinec	Haničinec	k1gMnSc1	Haničinec
</s>
</p>
<p>
<s>
Opowieść	Opowieść	k?	Opowieść
o	o	k7c4	o
Józefie	Józefie	k1gFnPc4	Józefie
Szwejku	Szwejka	k1gFnSc4	Szwejka
i	i	k8xC	i
jego	jego	k1gNnSc4	jego
najjaśniejszej	najjaśniejszat	k5eAaPmRp2nS	najjaśniejszat
epoce	epoce	k1gFnSc2	epoce
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
Povídka	povídka	k1gFnSc1	povídka
o	o	k7c4	o
Josefe	Josef	k1gMnSc5	Josef
Švejku	Švejk	k1gMnSc3	Švejk
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc7	jeho
císařskou	císařský	k2eAgFnSc7d1	císařská
době	doba	k1gFnSc6	doba
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Włodzimierz	Włodzimierza	k1gFnPc2	Włodzimierza
Gawroński	Gawrońsk	k1gFnSc2	Gawrońsk
<g/>
,	,	kIx,	,
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Jerzy	Jerza	k1gFnSc2	Jerza
Stuhr	Stuhra	k1gFnPc2	Stuhra
</s>
</p>
<p>
<s>
Dobrý	dobrý	k2eAgMnSc1d1	dobrý
voják	voják	k1gMnSc1	voják
Švejk	Švejk	k1gMnSc1	Švejk
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
<g/>
The	The	k1gMnSc1	The
Good	Good	k1gMnSc1	Good
Soldier	Soldier	k1gMnSc1	Soldier
Shweik	Shweik	k1gMnSc1	Shweik
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
a	a	k8xC	a
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Robert	Roberta	k1gFnPc2	Roberta
Crombie	Crombie	k1gFnSc2	Crombie
<g/>
,	,	kIx,	,
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
znění	znění	k1gNnSc6	znění
Ladislav	Ladislav	k1gMnSc1	Ladislav
Potměšil	potměšil	k1gMnSc1	potměšil
</s>
</p>
<p>
<s>
Pochožděnija	Pochožděnija	k6eAd1	Pochožděnija
bravogo	bravogo	k6eAd1	bravogo
soldata	soldat	k2eAgMnSc4d1	soldat
Švejka	Švejk	k1gMnSc4	Švejk
(	(	kIx(	(
<g/>
П	П	k?	П
Б	Б	k?	Б
С	С	k?	С
Ш	Ш	k?	Ш
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
Osudy	osud	k1gInPc1	osud
dobrého	dobrý	k2eAgMnSc2d1	dobrý
vojáka	voják	k1gMnSc2	voják
Švejka	Švejk	k1gMnSc2	Švejk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
animovaný	animovaný	k2eAgInSc1d1	animovaný
film	film	k1gInSc1	film
<g/>
,	,	kIx,	,
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
a	a	k8xC	a
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
režie	režie	k1gFnSc1	režie
Rinat	Rinat	k1gInSc1	Rinat
Gazizov	Gazizov	k1gInSc1	Gazizov
</s>
</p>
<p>
<s>
===	===	k?	===
Hudba	hudba	k1gFnSc1	hudba
===	===	k?	===
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
románu	román	k1gInSc2	román
napsal	napsat	k5eAaPmAgMnS	napsat
americký	americký	k2eAgMnSc1d1	americký
skladatel	skladatel	k1gMnSc1	skladatel
Robert	Robert	k1gMnSc1	Robert
Kurka	Kurka	k1gMnSc1	Kurka
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
–	–	k?	–
<g/>
1957	[number]	k4	1957
<g/>
)	)	kIx)	)
operu	oprat	k5eAaPmIp1nS	oprat
The	The	k1gMnSc1	The
Good	Good	k1gMnSc1	Good
Soldier	Soldier	k1gMnSc1	Soldier
Schweik	Schweik	k1gMnSc1	Schweik
(	(	kIx(	(
<g/>
Dobrý	dobrý	k2eAgMnSc1d1	dobrý
voják	voják	k1gMnSc1	voják
Švejk	Švejk	k1gMnSc1	Švejk
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
premiéru	premiéra	k1gFnSc4	premiéra
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
roku	rok	k1gInSc2	rok
1958	[number]	k4	1958
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Švejkova	Švejkův	k2eAgFnSc1d1	Švejkova
postava	postava	k1gFnSc1	postava
v	v	k7c6	v
opeře	opera	k1gFnSc6	opera
je	být	k5eAaImIp3nS	být
tenor	tenor	k1gInSc1	tenor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
2001	[number]	k4	2001
měl	mít	k5eAaImAgMnS	mít
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
premiéru	premiéra	k1gFnSc4	premiéra
německý	německý	k2eAgInSc4d1	německý
muzikál	muzikál	k1gInSc4	muzikál
Schweyk	Schweyka	k1gFnPc2	Schweyka
it	it	k?	it
Easy	Easy	k1gInPc1	Easy
s	s	k7c7	s
hudbou	hudba	k1gFnSc7	hudba
Konstantina	Konstantin	k1gMnSc2	Konstantin
Weckera	Wecker	k1gMnSc2	Wecker
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Švejk	Švejk	k1gMnSc1	Švejk
(	(	kIx(	(
<g/>
planetka	planetka	k1gFnSc1	planetka
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Hašek	Hašek	k1gMnSc1	Hašek
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Vaněk	Vaněk	k1gMnSc1	Vaněk
(	(	kIx(	(
<g/>
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Josef	Josef	k1gMnSc1	Josef
Lada	lado	k1gNnSc2	lado
</s>
</p>
<p>
<s>
Baloun	Baloun	k1gMnSc1	Baloun
</s>
</p>
<p>
<s>
Sapér	Sapér	k?	Sapér
Vodička	Vodička	k1gMnSc1	Vodička
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
COSENTINO	COSENTINO	kA	COSENTINO
<g/>
,	,	kIx,	,
Annalisa	Annalisa	k1gFnSc1	Annalisa
<g/>
.	.	kIx.	.
</s>
<s>
Chvála	chvála	k1gFnSc1	chvála
blbosti	blbost	k1gFnSc2	blbost
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
do	do	k7c2	do
Irkutska	Irkutsk	k1gInSc2	Irkutsk
a	a	k8xC	a
zpět	zpět	k6eAd1	zpět
v	v	k7c6	v
patnácti	patnáct	k4xCc6	patnáct
stech	sto	k4xCgNnPc6	sto
povídkách	povídka	k1gFnPc6	povídka
a	a	k8xC	a
jednom	jeden	k4xCgInSc6	jeden
románu	román	k1gInSc6	román
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Univerzita	univerzita	k1gFnSc1	univerzita
Karlova	Karlův	k2eAgFnSc1d1	Karlova
<g/>
,	,	kIx,	,
Filozofická	filozofický	k2eAgFnSc1d1	filozofická
fakulta	fakulta	k1gFnSc1	fakulta
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7308	[number]	k4	7308
<g/>
-	-	kIx~	-
<g/>
710	[number]	k4	710
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Osudy	osud	k1gInPc1	osud
dobrého	dobrý	k2eAgMnSc2d1	dobrý
vojáka	voják	k1gMnSc2	voják
Švejka	Švejk	k1gMnSc4	Švejk
za	za	k7c2	za
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Osudy	osud	k1gInPc1	osud
dobrého	dobrý	k2eAgMnSc2d1	dobrý
vojáka	voják	k1gMnSc2	voják
Švejka	Švejk	k1gMnSc4	Švejk
za	za	k7c2	za
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Dílo	dílo	k1gNnSc1	dílo
Osudy	osud	k1gInPc1	osud
dobrého	dobrý	k2eAgMnSc2d1	dobrý
vojáka	voják	k1gMnSc2	voják
Švejka	Švejk	k1gMnSc4	Švejk
za	za	k7c2	za
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
Nová	nový	k2eAgFnSc1d1	nová
podoba	podoba	k1gFnSc1	podoba
Švejka	Švejk	k1gMnSc2	Švejk
<g/>
,	,	kIx,	,
maloval	malovat	k5eAaImAgMnS	malovat
ho	on	k3xPp3gMnSc4	on
kreslíř	kreslíř	k1gMnSc1	kreslíř
Urban	Urban	k1gMnSc1	Urban
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Kdo	kdo	k3yQnSc1	kdo
je	být	k5eAaImIp3nS	být
Švejk	Švejk	k1gMnSc1	Švejk
in	in	k?	in
Z	z	k7c2	z
dějin	dějiny	k1gFnPc2	dějiny
českého	český	k2eAgNnSc2d1	české
myšlení	myšlení	k1gNnSc2	myšlení
o	o	k7c6	o
literatuře	literatura	k1gFnSc6	literatura
3	[number]	k4	3
(	(	kIx(	(
<g/>
1958	[number]	k4	1958
<g/>
-	-	kIx~	-
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
:	:	kIx,	:
Antologie	antologie	k1gFnSc1	antologie
k	k	k7c3	k
Dějinám	dějiny	k1gFnPc3	dějiny
české	český	k2eAgFnSc2d1	Česká
literatury	literatura	k1gFnSc2	literatura
1945	[number]	k4	1945
<g/>
-	-	kIx~	-
<g/>
1990	[number]	k4	1990
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
ÚČL	ÚČL	kA	ÚČL
AV	AV	kA	AV
<g/>
,	,	kIx,	,
2003	[number]	k4	2003
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
(	(	kIx(	(
<g/>
pdf	pdf	k?	pdf
<g/>
)	)	kIx)	)
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
články	článek	k1gInPc4	článek
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Kosík	Kosík	k1gMnSc1	Kosík
<g/>
:	:	kIx,	:
Hašek	Hašek	k1gMnSc1	Hašek
a	a	k8xC	a
Kafka	Kafka	k1gMnSc1	Kafka
neboli	neboli	k8xC	neboli
groteskní	groteskní	k2eAgInSc1d1	groteskní
svět	svět	k1gInSc1	svět
</s>
</p>
<p>
<s>
Václav	Václav	k1gMnSc1	Václav
Černý	Černý	k1gMnSc1	Černý
<g/>
:	:	kIx,	:
Král	Král	k1gMnSc1	Král
Ubu	Ubu	k1gMnSc1	Ubu
a	a	k8xC	a
pan	pan	k1gMnSc1	pan
Josef	Josef	k1gMnSc1	Josef
Švejk	Švejk	k1gMnSc1	Švejk
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
poddaný	poddaný	k1gMnSc1	poddaný
</s>
</p>
<p>
<s>
Bohumil	Bohumil	k1gMnSc1	Bohumil
Doležal	Doležal	k1gMnSc1	Doležal
<g/>
:	:	kIx,	:
Kdo	kdo	k3yQnSc1	kdo
je	být	k5eAaImIp3nS	být
Švejk	Švejk	k1gMnSc1	Švejk
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Švejk	Švejk	k1gMnSc1	Švejk
Central	Central	k1gMnSc1	Central
</s>
</p>
<p>
<s>
Eseje	esej	k1gFnPc1	esej
Josefa	Josef	k1gMnSc2	Josef
Musila	Musil	k1gMnSc2	Musil
o	o	k7c6	o
Švejkovi	Švejk	k1gMnSc6	Švejk
v	v	k7c6	v
časopise	časopis	k1gInSc6	časopis
TÉMA	téma	k1gNnSc2	téma
</s>
</p>
<p>
<s>
Nápověda	nápověda	k1gFnSc1	nápověda
a	a	k8xC	a
vysvětlivky	vysvětlivka	k1gFnPc1	vysvětlivka
aneb	aneb	k?	aneb
slovníček	slovníček	k1gInSc1	slovníček
Josefa	Josef	k1gMnSc2	Josef
Švejka	Švejk	k1gMnSc2	Švejk
</s>
</p>
<p>
<s>
Cesta	cesta	k1gFnSc1	cesta
Josefa	Josef	k1gMnSc2	Josef
Švejka	Švejk	k1gMnSc2	Švejk
</s>
</p>
<p>
<s>
Karel	Karel	k1gMnSc1	Karel
Vaněk	Vaněk	k1gMnSc1	Vaněk
<g/>
:	:	kIx,	:
Osudy	osud	k1gInPc1	osud
dobrého	dobrý	k2eAgMnSc2d1	dobrý
vojáka	voják	k1gMnSc2	voják
Švejka	Švejk	k1gMnSc2	Švejk
v	v	k7c6	v
ruském	ruský	k2eAgNnSc6d1	ruské
zajetí	zajetí	k1gNnSc6	zajetí
(	(	kIx(	(
<g/>
plný	plný	k2eAgInSc1d1	plný
text	text	k1gInSc1	text
dvoudílného	dvoudílný	k2eAgNnSc2d1	dvoudílné
pokračování	pokračování	k1gNnSc2	pokračování
Haškova	Haškův	k2eAgMnSc4d1	Haškův
Švejka	Švejk	k1gMnSc4	Švejk
<g/>
)	)	kIx)	)
</s>
</p>
