<s>
Osudy	osud	k1gInPc1	osud
dobrého	dobrý	k2eAgMnSc2d1	dobrý
vojáka	voják	k1gMnSc2	voják
Švejka	Švejk	k1gMnSc4	Švejk
za	za	k7c2	za
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
(	(	kIx(	(
<g/>
1921	[number]	k4	1921
<g/>
–	–	k?	–
<g/>
1923	[number]	k4	1923
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
zkracováno	zkracován	k2eAgNnSc1d1	zkracováno
jako	jako	k8xS	jako
Osudy	osud	k1gInPc1	osud
dobrého	dobrý	k2eAgMnSc2d1	dobrý
vojáka	voják	k1gMnSc2	voják
Švejka	Švejk	k1gMnSc2	Švejk
či	či	k8xC	či
hovorově	hovorově	k6eAd1	hovorově
jen	jen	k9	jen
Švejk	Švejk	k1gMnSc1	Švejk
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
čtyřdílný	čtyřdílný	k2eAgInSc1d1	čtyřdílný
humoristický	humoristický	k2eAgInSc1d1	humoristický
román	román	k1gInSc1	román
českého	český	k2eAgMnSc2d1	český
spisovatele	spisovatel	k1gMnSc2	spisovatel
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Haška	Hašek	k1gMnSc2	Hašek
<g/>
,	,	kIx,	,
několikrát	několikrát	k6eAd1	několikrát
zfilmovaný	zfilmovaný	k2eAgMnSc1d1	zfilmovaný
i	i	k8xC	i
zdramatizovaný	zdramatizovaný	k2eAgMnSc1d1	zdramatizovaný
<g/>
.	.	kIx.	.
</s>
