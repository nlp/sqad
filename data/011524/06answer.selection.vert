<s>
Časopis	časopis	k1gInSc1	časopis
je	být	k5eAaImIp3nS	být
periodická	periodický	k2eAgFnSc1d1	periodická
publikace	publikace	k1gFnSc1	publikace
(	(	kIx(	(
<g/>
opakovaně	opakovaně	k6eAd1	opakovaně
vycházející	vycházející	k2eAgFnSc1d1	vycházející
tiskovina	tiskovina	k1gFnSc1	tiskovina
<g/>
)	)	kIx)	)
určená	určený	k2eAgFnSc1d1	určená
pro	pro	k7c4	pro
skupinu	skupina	k1gFnSc4	skupina
čtenářů	čtenář	k1gMnPc2	čtenář
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
určitý	určitý	k2eAgInSc4d1	určitý
společný	společný	k2eAgInSc4d1	společný
zájem	zájem	k1gInSc4	zájem
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
takových	takový	k3xDgMnPc2	takový
zájmů	zájem	k1gInPc2	zájem
<g/>
.	.	kIx.	.
</s>
