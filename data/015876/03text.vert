<s>
Václav	Václav	k1gMnSc1
Vonášek	Vonášek	k1gMnSc1
</s>
<s>
Václav	Václav	k1gMnSc1
Vonášek	Vonášek	k1gMnSc1
Narození	narození	k1gNnSc4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1980	#num#	k4
(	(	kIx(
<g/>
40	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Blatná	blatný	k2eAgFnSc1d1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc2
</s>
<s>
Akademie	akademie	k1gFnSc1
múzických	múzický	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
Povolání	povolání	k1gNnSc2
</s>
<s>
fagotista	fagotista	k1gMnSc1
Manžel	manžel	k1gMnSc1
<g/>
(	(	kIx(
<g/>
ka	ka	k?
<g/>
)	)	kIx)
</s>
<s>
Jana	Jana	k1gFnSc1
Vonášková-Nováková	Vonášková-Nováková	k1gFnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
Chybí	chybit	k5eAaPmIp3nS,k5eAaImIp3nS
svobodný	svobodný	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Václav	Václav	k1gMnSc1
Vonášek	Vonášek	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
1980	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
český	český	k2eAgMnSc1d1
kontrafagotista	kontrafagotista	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
manželkou	manželka	k1gFnSc7
je	být	k5eAaImIp3nS
houslistka	houslistka	k1gFnSc1
Jana	Jana	k1gFnSc1
Vonášková-Nováková	Vonášková-Nováková	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Studoval	studovat	k5eAaImAgInS
na	na	k7c6
Konzervatoři	konzervatoř	k1gFnSc6
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
<g/>
,	,	kIx,
Hudební	hudební	k2eAgFnSc6d1
fakultě	fakulta	k1gFnSc6
AMU	AMU	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
a	a	k8xC
rok	rok	k1gInSc4
na	na	k7c4
Royal	Royal	k1gInSc4
College	Colleg	k1gFnSc2
of	of	k?
Music	Music	k1gMnSc1
v	v	k7c6
Londýně	Londýn	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
členem	člen	k1gMnSc7
České	český	k2eAgFnSc2d1
filharmonie	filharmonie	k1gFnSc2
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
spolupracuje	spolupracovat	k5eAaImIp3nS
se	s	k7c7
souborem	soubor	k1gInSc7
Barocco	Barocca	k1gMnSc5
sempre	sempr	k1gMnSc5
giovane	giovan	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
působí	působit	k5eAaImIp3nS
jako	jako	k9
kontrafagotista	kontrafagotista	k1gMnSc1
Berlínských	berlínský	k2eAgMnPc2d1
filharmoniků	filharmonik	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ocenění	ocenění	k1gNnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
cena	cena	k1gFnSc1
v	v	k7c4
Česko	Česko	k1gNnSc4
Slovenské	slovenský	k2eAgFnSc6d1
soutěži	soutěž	k1gFnSc6
Talent	talent	k1gInSc1
roku	rok	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc7
součástí	součást	k1gFnSc7
bylo	být	k5eAaImAgNnS
roční	roční	k2eAgNnSc1d1
stipendium	stipendium	k1gNnSc1
na	na	k7c4
Royal	Royal	k1gInSc4
College	Colleg	k1gFnSc2
of	of	k?
Music	Music	k1gMnSc1
v	v	k7c6
Londýně	Londýn	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
cena	cena	k1gFnSc1
a	a	k8xC
Cena	cena	k1gFnSc1
Českého	český	k2eAgInSc2d1
hudebního	hudební	k2eAgInSc2d1
fondu	fond	k1gInSc2
za	za	k7c4
nejlepší	dobrý	k2eAgNnSc4d3
provedení	provedení	k1gNnSc4
české	český	k2eAgFnSc2d1
soudobé	soudobý	k2eAgFnSc2d1
skladby	skladba	k1gFnSc2
na	na	k7c6
Pražském	pražský	k2eAgInSc6d1
jaru	jar	k1gInSc6
2002	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
cena	cena	k1gFnSc1
v	v	k7c6
soutěži	soutěž	k1gFnSc6
International	International	k1gFnSc2
Double	double	k2eAgInSc1d1
Reed	Reed	k1gInSc1
Society	societa	k1gFnSc2
v	v	k7c6
Melbourne	Melbourne	k1gNnSc6
2004	#num#	k4
</s>
<s>
Vítěz	vítěz	k1gMnSc1
soutěže	soutěž	k1gFnSc2
v	v	k7c6
polské	polský	k2eAgFnSc6d1
Łódźi	Łódź	k1gFnSc6
2005	#num#	k4
</s>
<s>
V	v	k7c6
listopadu	listopad	k1gInSc6
2005	#num#	k4
mu	on	k3xPp3gMnSc3
byla	být	k5eAaImAgFnS
udělena	udělen	k2eAgFnSc1d1
Cena	cena	k1gFnSc1
ministryně	ministryně	k1gFnSc2
školství	školství	k1gNnSc2
<g/>
,	,	kIx,
mládeže	mládež	k1gFnSc2
a	a	k8xC
tělovýchovy	tělovýchova	k1gFnSc2
za	za	k7c4
vynikající	vynikající	k2eAgInPc4d1
studijní	studijní	k2eAgInPc4d1
výsledky	výsledek	k1gInPc4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
cena	cena	k1gFnSc1
z	z	k7c2
mezinárodní	mezinárodní	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
ARD	ARD	kA
v	v	k7c6
Mnichově	Mnichov	k1gInSc6
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
cena	cena	k1gFnSc1
a	a	k8xC
titul	titul	k1gInSc1
laureáta	laureát	k1gMnSc2
z	z	k7c2
61	#num#	k4
<g/>
.	.	kIx.
ročníku	ročník	k1gInSc2
soutěže	soutěž	k1gFnSc2
Pražského	pražský	k2eAgNnSc2d1
jara	jaro	k1gNnSc2
2009	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
https://www.berliner-philharmoniker.de/orchester/	https://www.berliner-philharmoniker.de/orchester/	k?
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Hudební	hudební	k2eAgInPc1d1
rozhledy	rozhled	k1gInPc1
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
Životopis	životopis	k1gInSc1
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
Hudební	hudební	k2eAgInPc1d1
kurzy	kurz	k1gInPc1
</s>
<s>
Rozhlas	rozhlas	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Vítěz	vítěz	k1gMnSc1
Pražského	pražský	k2eAgNnSc2d1
jara	jaro	k1gNnSc2
v	v	k7c6
Pardubicích	Pardubice	k1gInPc6
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
pna	pnout	k5eAaImSgInS
<g/>
2005274575	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0003	#num#	k4
7439	#num#	k4
2900	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
n	n	k0
<g/>
2014000018	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
85083906	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-n	lccn-n	k1gInSc1
<g/>
2014000018	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
