<s>
Step	step	k1gFnSc1	step
(	(	kIx(	(
<g/>
z	z	k7c2	z
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
tance	tanec	k1gInSc2	tanec
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
tančí	tančit	k5eAaImIp3nP	tančit
jak	jak	k6eAd1	jak
sóloví	sólový	k2eAgMnPc1d1	sólový
tanečníci	tanečník	k1gMnPc1	tanečník
<g/>
,	,	kIx,	,
tak	tak	k9	tak
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
pouze	pouze	k6eAd1	pouze
nohama	noha	k1gFnPc7	noha
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc4	zbytek
těla	tělo	k1gNnSc2	tělo
jen	jen	k9	jen
vyvažuje	vyvažovat	k5eAaImIp3nS	vyvažovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
provádění	provádění	k1gNnSc4	provádění
tance	tanec	k1gInSc2	tanec
se	se	k3xPyFc4	se
neúčastní	účastnit	k5eNaImIp3nS	účastnit
<g/>
.	.	kIx.	.
</s>
