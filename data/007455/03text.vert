<s>
Step	step	k1gFnSc1	step
(	(	kIx(	(
<g/>
z	z	k7c2	z
angličtiny	angličtina	k1gFnSc2	angličtina
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
druh	druh	k1gInSc4	druh
tance	tanec	k1gInSc2	tanec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
tančí	tančit	k5eAaImIp3nP	tančit
jak	jak	k6eAd1	jak
sóloví	sólový	k2eAgMnPc1d1	sólový
tanečníci	tanečník	k1gMnPc1	tanečník
<g/>
,	,	kIx,	,
tak	tak	k9	tak
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
pouze	pouze	k6eAd1	pouze
nohama	noha	k1gFnPc7	noha
<g/>
,	,	kIx,	,
zbytek	zbytek	k1gInSc4	zbytek
těla	tělo	k1gNnSc2	tělo
jen	jen	k9	jen
vyvažuje	vyvažovat	k5eAaImIp3nS	vyvažovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
provádění	provádění	k1gNnSc4	provádění
tance	tanec	k1gInSc2	tanec
se	se	k3xPyFc4	se
neúčastní	účastnit	k5eNaImIp3nS	účastnit
<g/>
.	.	kIx.	.
</s>
<s>
Chodidla	chodidlo	k1gNnSc2	chodidlo
nohou	noha	k1gFnPc2	noha
vyklepávají	vyklepávat	k5eAaImIp3nP	vyklepávat
patami	pata	k1gFnPc7	pata
a	a	k8xC	a
špičkami	špička	k1gFnPc7	špička
hudební	hudební	k2eAgInSc4d1	hudební
rytmus	rytmus	k1gInSc4	rytmus
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomuto	tento	k3xDgInSc3	tento
účelu	účel	k1gInSc3	účel
se	se	k3xPyFc4	se
nosí	nosit	k5eAaImIp3nS	nosit
speciální	speciální	k2eAgFnSc1d1	speciální
obuv	obuv	k1gFnSc1	obuv
opatřená	opatřený	k2eAgFnSc1d1	opatřená
kovovými	kovový	k2eAgInPc7d1	kovový
plíšky	plíšek	k1gInPc7	plíšek
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
Češi	Čech	k1gMnPc1	Čech
znají	znát	k5eAaImIp3nP	znát
tento	tento	k3xDgInSc4	tento
tanec	tanec	k1gInSc4	tanec
pod	pod	k7c7	pod
pojmem	pojem	k1gInSc7	pojem
step	step	k1gFnSc1	step
<g/>
,	,	kIx,	,
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
převážně	převážně	k6eAd1	převážně
výraz	výraz	k1gInSc1	výraz
tap	tap	k?	tap
dance	dance	k1gFnSc2	dance
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
další	další	k2eAgInPc4d1	další
názvy	název	k1gInPc4	název
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Clogging	Clogging	k1gInSc1	Clogging
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
jiná	jiný	k2eAgFnSc1d1	jiná
varianta	varianta	k1gFnSc1	varianta
tance	tanec	k1gInSc2	tanec
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
americký	americký	k2eAgInSc1d1	americký
step	step	k1gInSc1	step
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
vzrůstá	vzrůstat	k5eAaImIp3nS	vzrůstat
popularita	popularita	k1gFnSc1	popularita
irského	irský	k2eAgInSc2d1	irský
stepu	step	k1gInSc2	step
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgInSc1d1	americký
step	step	k1gInSc1	step
Clogging	Clogging	k1gInSc1	Clogging
–	–	k?	–
westernové	westernový	k2eAgNnSc4d1	westernové
stepování	stepování	k1gNnSc4	stepování
Irský	irský	k2eAgInSc1d1	irský
step	step	k1gInSc1	step
Moderní	moderní	k2eAgFnSc1d1	moderní
step	step	k1gFnSc1	step
Street	Street	k1gMnSc1	Street
step	step	k1gInSc1	step
neboli	neboli	k8xC	neboli
pouliční	pouliční	k2eAgInSc1d1	pouliční
step	step	k1gInSc1	step
Step	step	k1gInSc1	step
vznikal	vznikat	k5eAaImAgInS	vznikat
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
New	New	k1gFnSc4	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
v	v	k7c6	v
tancích	tanec	k1gInPc6	tanec
soutěžili	soutěžit	k5eAaImAgMnP	soutěžit
irští	irský	k2eAgMnPc1d1	irský
<g/>
,	,	kIx,	,
skotští	skotský	k2eAgMnPc1d1	skotský
a	a	k8xC	a
afričtí	africký	k2eAgMnPc1d1	africký
přistěhovalci	přistěhovalec	k1gMnPc1	přistěhovalec
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
směsi	směs	k1gFnSc2	směs
"	"	kIx"	"
<g/>
nožních	nožní	k2eAgInPc2d1	nožní
tanců	tanec	k1gInPc2	tanec
<g/>
"	"	kIx"	"
z	z	k7c2	z
mnoha	mnoho	k4c2	mnoho
zdrojů	zdroj	k1gInPc2	zdroj
se	se	k3xPyFc4	se
vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
původní	původní	k2eAgInSc1d1	původní
tap	tap	k?	tap
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
byl	být	k5eAaImAgInS	být
tanec	tanec	k1gInSc1	tanec
znám	znám	k2eAgInSc1d1	znám
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
jazz	jazz	k1gInSc4	jazz
dance	danec	k1gInSc2	danec
<g/>
.	.	kIx.	.
</s>
<s>
Step	step	k1gInSc1	step
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
nový	nový	k2eAgInSc1d1	nový
hudební	hudební	k2eAgInSc1d1	hudební
žánr	žánr	k1gInSc1	žánr
jazz	jazz	k1gInSc1	jazz
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
se	se	k3xPyFc4	se
jazzová	jazzový	k2eAgFnSc1d1	jazzová
hudba	hudba	k1gFnSc1	hudba
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
nejvhodnější	vhodný	k2eAgInPc1d3	nejvhodnější
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
stepování	stepování	k1gNnSc4	stepování
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
však	však	k9	však
možné	možný	k2eAgNnSc1d1	možné
stepovat	stepovat	k5eAaImF	stepovat
i	i	k9	i
bez	bez	k7c2	bez
hudebního	hudební	k2eAgInSc2d1	hudební
doprovodu	doprovod	k1gInSc2	doprovod
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
tap	tap	k?	tap
dance	danko	k6eAd1	danko
převládajícím	převládající	k2eAgInSc7d1	převládající
druhem	druh	k1gInSc7	druh
tance	tanec	k1gInSc2	tanec
na	na	k7c6	na
tanečních	taneční	k2eAgNnPc6d1	taneční
vystoupeních	vystoupení	k1gNnPc6	vystoupení
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
všech	všecek	k3xTgNnPc6	všecek
městech	město	k1gNnPc6	město
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
byli	být	k5eAaImAgMnP	být
kvalifikovaní	kvalifikovaný	k2eAgMnPc1d1	kvalifikovaný
tanečníci	tanečník	k1gMnPc1	tanečník
<g/>
.	.	kIx.	.
</s>
<s>
Řetězce	řetězec	k1gInPc1	řetězec
sálů	sál	k1gInPc2	sál
Vaudeville	vaudeville	k1gInSc1	vaudeville
a	a	k8xC	a
T.O.B.A.	T.O.B.A.	k1gFnSc1	T.O.B.A.
(	(	kIx(	(
<g/>
Black	Black	k1gInSc1	Black
Vaudeville	vaudeville	k1gInSc1	vaudeville
<g/>
)	)	kIx)	)
najímaly	najímat	k5eAaImAgInP	najímat
mnoho	mnoho	k6eAd1	mnoho
profesionálních	profesionální	k2eAgMnPc2d1	profesionální
tanečníků	tanečník	k1gMnPc2	tanečník
<g/>
.	.	kIx.	.
</s>
<s>
Vaudevillští	Vaudevillský	k2eAgMnPc1d1	Vaudevillský
tanečníci	tanečník	k1gMnPc1	tanečník
jezdili	jezdit	k5eAaImAgMnP	jezdit
na	na	k7c4	na
turné	turné	k1gNnSc4	turné
po	po	k7c6	po
amerických	americký	k2eAgNnPc6d1	americké
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Putující	putující	k2eAgMnPc1d1	putující
umělci	umělec	k1gMnPc1	umělec
ovlivňovali	ovlivňovat	k5eAaImAgMnP	ovlivňovat
tance	tanec	k1gInPc4	tanec
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
například	například	k6eAd1	například
i	i	k9	i
lindy	linda	k1gFnSc2	linda
hop	hop	k0	hop
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
některé	některý	k3yIgInPc4	některý
prvky	prvek	k1gInPc4	prvek
stepu	stepat	k5eAaPmIp1nS	stepat
přejal	přejmout	k5eAaPmAgMnS	přejmout
<g/>
.	.	kIx.	.
</s>
<s>
Základem	základ	k1gInSc7	základ
stepu	step	k1gInSc2	step
jsou	být	k5eAaImIp3nP	být
nejjednodušší	jednoduchý	k2eAgInSc4d3	nejjednodušší
kroky	krok	k1gInPc4	krok
jako	jako	k8xC	jako
např.	např.	kA	např.
step	step	k1gInSc1	step
<g/>
,	,	kIx,	,
tap	tap	k?	tap
<g/>
,	,	kIx,	,
stamp	stamp	k1gMnSc1	stamp
<g/>
,	,	kIx,	,
pick-up	pickp	k1gMnSc1	pick-up
<g/>
,	,	kIx,	,
heel	heel	k1gMnSc1	heel
<g/>
...	...	k?	...
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgInPc2	který
jsou	být	k5eAaImIp3nP	být
složeny	složit	k5eAaPmNgInP	složit
složitější	složitý	k2eAgInPc1d2	složitější
<g/>
,	,	kIx,	,
např.	např.	kA	např.
rag	rag	k?	rag
time	time	k1gInSc1	time
<g/>
,	,	kIx,	,
crawling	crawling	k1gInSc1	crawling
<g/>
,	,	kIx,	,
the	the	k?	the
wizard	wizard	k1gMnSc1	wizard
of	of	k?	of
Oz	Oz	k1gMnSc1	Oz
<g/>
,	,	kIx,	,
...	...	k?	...
Tap	Tap	k1gFnSc1	Tap
–	–	k?	–
úder	úder	k1gInSc4	úder
bříškem	bříšek	k1gInSc7	bříšek
chodidla	chodidlo	k1gNnSc2	chodidlo
bez	bez	k7c2	bez
přenesení	přenesení	k1gNnSc2	přenesení
váhy	váha	k1gFnSc2	váha
Step	step	k1gInSc1	step
–	–	k?	–
úder	úder	k1gInSc4	úder
bříškem	bříško	k1gNnSc7	bříško
chodidla	chodidlo	k1gNnSc2	chodidlo
s	s	k7c7	s
přenesením	přenesení	k1gNnSc7	přenesení
váhy	váha	k1gFnSc2	váha
Brush	Brush	k1gMnSc1	Brush
–	–	k?	–
úder	úder	k1gInSc1	úder
s	s	k7c7	s
menším	malý	k2eAgInSc7d2	menší
švihem	švih	k1gInSc7	švih
chodidla	chodidlo	k1gNnSc2	chodidlo
(	(	kIx(	(
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
)	)	kIx)	)
vpřed	vpřed	k6eAd1	vpřed
Back	Back	k1gInSc1	Back
–	–	k?	–
úder	úder	k1gInSc1	úder
s	s	k7c7	s
menším	malý	k2eAgInSc7d2	menší
švihem	švih	k1gInSc7	švih
chodidla	chodidlo	k1gNnSc2	chodidlo
vzad	vzad	k6eAd1	vzad
Heel	Heel	k1gInSc1	Heel
–	–	k?	–
úder	úder	k1gInSc4	úder
stojné	stojný	k2eAgFnSc2d1	stojná
paty	pata	k1gFnSc2	pata
Stamp	Stamp	k1gMnSc1	Stamp
–	–	k?	–
úder	úder	k1gInSc1	úder
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
plochu	plocha	k1gFnSc4	plocha
nohy	noha	k1gFnSc2	noha
s	s	k7c7	s
přenesením	přenesení	k1gNnSc7	přenesení
váhy	váha	k1gFnSc2	váha
Stomp	Stomp	k1gMnSc1	Stomp
–	–	k?	–
úder	úder	k1gInSc1	úder
na	na	k7c4	na
celou	celý	k2eAgFnSc4d1	celá
plochu	plocha	k1gFnSc4	plocha
nohy	noha	k1gFnSc2	noha
bez	bez	k7c2	bez
přenesení	přenesení	k1gNnSc2	přenesení
váhy	váha	k1gFnSc2	váha
Dig	Dig	k1gFnSc1	Dig
–	–	k?	–
úder	úder	k1gInSc4	úder
připomínající	připomínající	k2eAgNnSc1d1	připomínající
kopnutí	kopnutí	k1gNnSc1	kopnutí
Crawl	Crawl	k1gInSc1	Crawl
–	–	k?	–
úder	úder	k1gInSc4	úder
špičkou	špička	k1gFnSc7	špička
boty	bota	k1gFnSc2	bota
nejčastěji	často	k6eAd3	často
za	za	k7c7	za
stojnou	stojný	k2eAgFnSc7d1	stojná
nohou	noha	k1gFnSc7	noha
<g />
.	.	kIx.	.
</s>
<s>
Scuff	Scuff	k1gInSc1	Scuff
–	–	k?	–
úder	úder	k1gInSc4	úder
druhou	druhý	k4xOgFnSc4	druhý
(	(	kIx(	(
<g/>
zadní	zadní	k2eAgFnSc4d1	zadní
<g/>
)	)	kIx)	)
polovinou	polovina	k1gFnSc7	polovina
paty	pata	k1gFnSc2	pata
bez	bez	k7c2	bez
přenesení	přenesení	k1gNnSc2	přenesení
váhy	váha	k1gFnSc2	váha
Pick-up	Pickp	k1gMnSc1	Pick-up
–	–	k?	–
úder	úder	k1gInSc1	úder
bříškem	bříšek	k1gInSc7	bříšek
chodidla	chodidlo	k1gNnSc2	chodidlo
ze	z	k7c2	z
zvednuté	zvednutý	k2eAgFnSc2d1	zvednutá
špičky	špička	k1gFnSc2	špička
Shuffle	Shuffle	k1gFnSc2	Shuffle
–	–	k?	–
brush	brush	k1gMnSc1	brush
a	a	k8xC	a
back	back	k1gMnSc1	back
Scuffle	Scuffle	k1gFnSc2	Scuffle
–	–	k?	–
scuff	scuff	k1gInSc1	scuff
a	a	k8xC	a
step	step	k1gFnSc1	step
Flap	Flap	k1gInSc1	Flap
–	–	k?	–
brush	brush	k1gInSc1	brush
a	a	k8xC	a
step	step	k1gInSc1	step
Bill	Bill	k1gMnSc1	Bill
Robinson	Robinson	k1gMnSc1	Robinson
(	(	kIx(	(
<g/>
známý	známý	k2eAgMnSc1d1	známý
také	také	k9	také
jako	jako	k9	jako
Bill	Bill	k1gMnSc1	Bill
Bojangles	Bojangles	k1gMnSc1	Bojangles
Robinson	Robinson	k1gMnSc1	Robinson
<g/>
,	,	kIx,	,
otec	otec	k1gMnSc1	otec
<g />
.	.	kIx.	.
</s>
<s>
moderního	moderní	k2eAgInSc2d1	moderní
stepu	step	k1gInSc2	step
<g/>
)	)	kIx)	)
Master	master	k1gMnSc1	master
Juba	Juba	k1gMnSc1	Juba
Ruby	rub	k1gInPc7	rub
Keeler	Keeler	k1gMnSc1	Keeler
Ann	Ann	k1gMnSc1	Ann
Miller	Miller	k1gMnSc1	Miller
Eleanor	Eleanor	k1gMnSc1	Eleanor
Powell	Powell	k1gMnSc1	Powell
The	The	k1gMnSc1	The
Nicholas	Nicholas	k1gMnSc1	Nicholas
Brothers	Brothers	k1gInSc1	Brothers
-	-	kIx~	-
bratří	bratřit	k5eAaImIp3nS	bratřit
Nicholasovi	Nicholasův	k2eAgMnPc1d1	Nicholasův
Fred	Fred	k1gMnSc1	Fred
Astaire	Astair	k1gInSc5	Astair
(	(	kIx(	(
<g/>
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
filmech	film	k1gInPc6	film
hrál	hrát	k5eAaImAgMnS	hrát
a	a	k8xC	a
stepoval	stepovat	k5eAaImAgMnS	stepovat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ginger	Gingra	k1gFnPc2	Gingra
Rogersovou	Rogersová	k1gFnSc7	Rogersová
<g/>
)	)	kIx)	)
Ginger	Ginger	k1gInSc1	Ginger
Rogersová	Rogersový	k2eAgFnSc1d1	Rogersová
Gene	gen	k1gInSc5	gen
Kelly	Kella	k1gMnSc2	Kella
Donald	Donald	k1gMnSc1	Donald
O	o	k7c6	o
<g/>
́	́	k?	́
<g/>
Connor	Connor	k1gInSc1	Connor
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
Genem	gen	k1gInSc7	gen
Kellym	Kellymum	k1gNnPc2	Kellymum
v	v	k7c6	v
<g />
.	.	kIx.	.
</s>
<s>
některých	některý	k3yIgFnPc6	některý
známých	známý	k2eAgFnPc6d1	známá
stepovacích	stepovací	k2eAgFnPc6d1	stepovací
scénách	scéna	k1gFnPc6	scéna
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Zpívání	zpívání	k1gNnSc2	zpívání
v	v	k7c6	v
dešti	dešť	k1gInSc6	dešť
-	-	kIx~	-
Singing	Singing	k1gInSc1	Singing
in	in	k?	in
the	the	k?	the
Rain	Rain	k1gInSc1	Rain
<g/>
)	)	kIx)	)
Vera	Vera	k1gFnSc1	Vera
Ellen	Ellna	k1gFnPc2	Ellna
(	(	kIx(	(
<g/>
ve	v	k7c6	v
filmu	film	k1gInSc6	film
On	on	k3xPp3gMnSc1	on
The	The	k1gMnSc1	The
Town	Town	k1gMnSc1	Town
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Genem	gen	k1gInSc7	gen
Kellym	Kellym	k1gInSc1	Kellym
<g/>
)	)	kIx)	)
Gregory	Gregor	k1gMnPc4	Gregor
Hines	Hinesa	k1gFnPc2	Hinesa
(	(	kIx(	(
<g/>
mezi	mezi	k7c7	mezi
jinými	jiný	k2eAgMnPc7d1	jiný
umělci	umělec	k1gMnPc7	umělec
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
filmu	film	k1gInSc6	film
Tap	Tap	k1gFnPc2	Tap
Dance	Danka	k1gFnSc3	Danka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
<g/>
)	)	kIx)	)
Maurice	Maurika	k1gFnSc6	Maurika
Hines	Hinesa	k1gFnPc2	Hinesa
Sammy	Samma	k1gFnSc2	Samma
Davis	Davis	k1gFnSc2	Davis
Jr	Jr	k1gFnSc2	Jr
<g/>
.	.	kIx.	.
</s>
<s>
Savion	Savion	k1gInSc1	Savion
Glover	Glovra	k1gFnPc2	Glovra
Paulette	Paulett	k1gInSc5	Paulett
Goddard	Goddarda	k1gFnPc2	Goddarda
William	William	k1gInSc4	William
Henri	Henre	k1gFnSc4	Henre
Lane	Lan	k1gMnSc2	Lan
alias	alias	k9	alias
Master	master	k1gMnSc1	master
Juba	Jub	k1gInSc2	Jub
Jimmy	Jimma	k1gFnSc2	Jimma
Slyde	Slyd	k1gInSc5	Slyd
John	John	k1gMnSc1	John
W.	W.	kA	W.
Bubbles	Bubbles	k1gMnSc1	Bubbles
Howard	Howard	k1gMnSc1	Howard
Sims	Sims	k1gInSc4	Sims
Bunny	Bunna	k1gFnSc2	Bunna
Briggs	Briggsa	k1gFnPc2	Briggsa
Henry	Henry	k1gMnSc1	Henry
Letang	Letang	k1gMnSc1	Letang
Steve	Steve	k1gMnSc1	Steve
Condos	Condos	k1gMnSc1	Condos
Charles	Charles	k1gMnSc1	Charles
Honi	Hon	k1gFnSc2	Hon
Coles	Coles	k1gMnSc1	Coles
Pat	pata	k1gFnPc2	pata
Rico	Rico	k1gMnSc1	Rico
Jeni	Jen	k1gFnSc2	Jen
LeGon	LeGon	k1gMnSc1	LeGon
Arthur	Arthur	k1gMnSc1	Arthur
Duncan	Duncana	k1gFnPc2	Duncana
Tommy	Tomma	k1gFnSc2	Tomma
Tune	Tun	k1gMnSc4	Tun
Brenda	Brend	k1gMnSc4	Brend
Bufalino	Bufalin	k2eAgNnSc1d1	Bufalino
Jay	Jay	k1gMnSc1	Jay
Fagan	Fagan	k?	Fagan
Ted	Ted	k1gMnSc1	Ted
Bebblejad	Bebblejad	k1gInSc1	Bebblejad
Peter	Petra	k1gFnPc2	Petra
Briansen	Briansna	k1gFnPc2	Briansna
Rita	rito	k1gNnSc2	rito
Hayworth	Hayworth	k1gMnSc1	Hayworth
Betty	Betty	k1gFnSc2	Betty
Grable	Grable	k1gMnSc1	Grable
Alfonso	Alfonso	k1gMnSc1	Alfonso
Ribeiro	Ribeiro	k1gNnSc1	Ribeiro
LaVaughn	LaVaughn	k1gMnSc1	LaVaughn
Robinson	Robinson	k1gMnSc1	Robinson
<g />
.	.	kIx.	.
</s>
<s>
Jason	Jason	k1gMnSc1	Jason
Samuels	Samuelsa	k1gFnPc2	Samuelsa
Smith	Smith	k1gMnSc1	Smith
Shirley	Shirlea	k1gFnSc2	Shirlea
Temple	templ	k1gInSc5	templ
Grant	grant	k1gInSc4	grant
Swift	Swift	k2eAgMnSc1d1	Swift
Joseph	Joseph	k1gMnSc1	Joseph
Wiggan	Wiggan	k1gMnSc1	Wiggan
Guillem	Guill	k1gMnSc7	Guill
Alonso	Alonsa	k1gFnSc5	Alonsa
Michael	Michael	k1gMnSc1	Michael
Flatley	Flatlea	k1gFnSc2	Flatlea
Ondřej	Ondřej	k1gMnSc1	Ondřej
Havelka	Havelka	k1gMnSc1	Havelka
Acia	Acia	k1gMnSc1	Acia
Gray	Graa	k1gFnSc2	Graa
Gumboot	Gumboot	k1gMnSc1	Gumboot
dance	dance	k1gMnSc1	dance
(	(	kIx(	(
<g/>
tanec	tanec	k1gInSc1	tanec
s	s	k7c7	s
gumou	guma	k1gFnSc7	guma
a	a	k8xC	a
botou	bota	k1gFnSc7	bota
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
step	step	k1gInSc1	step
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
step	step	k1gFnSc1	step
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
www.step.cz	www.step.cza	k1gFnPc2	www.step.cza
Základní	základní	k2eAgInPc1d1	základní
údery	úder	k1gInPc1	úder
Taneční	taneční	k2eAgInPc1d1	taneční
studio	studio	k1gNnSc4	studio
No	no	k9	no
Feet	Feet	k1gInSc1	Feet
Brno	Brno	k1gNnSc1	Brno
-	-	kIx~	-
několikanásobní	několikanásobný	k2eAgMnPc1d1	několikanásobný
mistři	mistr	k1gMnPc1	mistr
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
mistr	mistr	k1gMnSc1	mistr
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
účastník	účastník	k1gMnSc1	účastník
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
www.tapdance.org	www.tapdance.org	k1gInSc1	www.tapdance.org
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
www.tapdance.info	www.tapdance.info	k6eAd1	www.tapdance.info
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
www.germantap.de	www.germantap.de	k6eAd1	www.germantap.de
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
<g/>
)	)	kIx)	)
Terminoligia	Terminoligia	k1gFnSc1	Terminoligia
-	-	kIx~	-
www.offjazz.com/term-tp-it.htm	www.offjazz.com/termpt.htm	k1gInSc1	www.offjazz.com/term-tp-it.htm
</s>
