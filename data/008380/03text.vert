<p>
<s>
Bartolomeo	Bartolomeo	k1gMnSc1	Bartolomeo
Cristofori	Cristofor	k1gFnSc2	Cristofor
di	di	k?	di
Francesco	Francesco	k1gMnSc1	Francesco
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1655	[number]	k4	1655
Padova	Padova	k1gFnSc1	Padova
–	–	k?	–
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1731	[number]	k4	1731
Florencie	Florencie	k1gFnSc2	Florencie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
italský	italský	k2eAgMnSc1d1	italský
výrobce	výrobce	k1gMnSc1	výrobce
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
považovaný	považovaný	k2eAgInSc1d1	považovaný
za	za	k7c4	za
vynálezce	vynálezce	k1gMnSc4	vynálezce
klavíru	klavír	k1gInSc2	klavír
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c6	o
jeho	jeho	k3xOp3gNnSc6	jeho
mládí	mládí	k1gNnSc6	mládí
není	být	k5eNaImIp3nS	být
mnoho	mnoho	k6eAd1	mnoho
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1688	[number]	k4	1688
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
služeb	služba	k1gFnPc2	služba
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
de	de	k?	de
<g/>
'	'	kIx"	'
<g/>
Medici	medik	k1gMnPc1	medik
<g/>
,	,	kIx,	,
dědice	dědic	k1gMnPc4	dědic
toskánského	toskánský	k2eAgInSc2d1	toskánský
trůnu	trůn	k1gInSc2	trůn
a	a	k8xC	a
známého	známý	k2eAgMnSc2d1	známý
mecenáše	mecenáš	k1gMnSc2	mecenáš
umění	umění	k1gNnSc2	umění
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
zřejmě	zřejmě	k6eAd1	zřejmě
začal	začít	k5eAaPmAgInS	začít
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
klávesovou	klávesový	k2eAgFnSc4d1	klávesová
mechaniku	mechanika	k1gFnSc4	mechanika
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
umožnila	umožnit	k5eAaPmAgFnS	umožnit
dynamické	dynamický	k2eAgNnSc4d1	dynamické
odstupňování	odstupňování	k1gNnSc4	odstupňování
síly	síla	k1gFnSc2	síla
tónu	tón	k1gInSc2	tón
<g/>
.	.	kIx.	.
</s>
<s>
Dosavadní	dosavadní	k2eAgInPc1d1	dosavadní
klávesové	klávesový	k2eAgInPc1d1	klávesový
nástroje	nástroj	k1gInPc1	nástroj
totiž	totiž	k9	totiž
neumožňovaly	umožňovat	k5eNaImAgInP	umožňovat
ovládat	ovládat	k5eAaImF	ovládat
hlasitost	hlasitost	k1gFnSc4	hlasitost
silou	síla	k1gFnSc7	síla
úhozu	úhoz	k1gInSc2	úhoz
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
1700	[number]	k4	1700
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
na	na	k7c6	na
seznamu	seznam	k1gInSc6	seznam
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
knížete	kníže	k1gMnSc2	kníže
objevuje	objevovat	k5eAaImIp3nS	objevovat
arpicembalo	arpicembat	k5eAaPmAgNnS	arpicembat
che	che	k0	che
fà	fà	k?	fà
il	il	k?	il
piano	piano	k6eAd1	piano
e	e	k0	e
il	il	k?	il
forte	forte	k1gNnSc1	forte
(	(	kIx(	(
<g/>
cembalo	cembalo	k1gNnSc1	cembalo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
může	moct	k5eAaImIp3nS	moct
hrát	hrát	k5eAaImF	hrát
tiše	tiš	k1gFnPc4	tiš
i	i	k9	i
hlasitě	hlasitě	k6eAd1	hlasitě
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
nástroj	nástroj	k1gInSc1	nástroj
bývá	bývat	k5eAaImIp3nS	bývat
obvykle	obvykle	k6eAd1	obvykle
datován	datovat	k5eAaImNgInS	datovat
rokem	rok	k1gInSc7	rok
1698	[number]	k4	1698
a	a	k8xC	a
měl	mít	k5eAaImAgInS	mít
rozsah	rozsah	k1gInSc1	rozsah
4	[number]	k4	4
oktávy	oktáva	k1gFnSc2	oktáva
<g/>
.	.	kIx.	.
</s>
<s>
Cristofori	Cristofori	k6eAd1	Cristofori
zůstal	zůstat	k5eAaPmAgInS	zůstat
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
medicejského	medicejský	k2eAgInSc2d1	medicejský
dvora	dvůr	k1gInSc2	dvůr
ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
až	až	k9	až
do	do	k7c2	do
smrti	smrt	k1gFnSc2	smrt
a	a	k8xC	a
svůj	svůj	k3xOyFgInSc4	svůj
vynález	vynález	k1gInSc4	vynález
dále	daleko	k6eAd2	daleko
zdokonaloval	zdokonalovat	k5eAaImAgMnS	zdokonalovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Conny	Conn	k1gInPc1	Conn
Restle	Restle	k?	Restle
<g/>
:	:	kIx,	:
Bartolomeo	Bartolomeo	k1gMnSc1	Bartolomeo
Cristofori	Cristofor	k1gFnSc2	Cristofor
und	und	k?	und
die	die	k?	die
Anfänge	Anfänge	k1gInSc1	Anfänge
des	des	k1gNnSc1	des
Hammerclaviers	Hammerclaviers	k1gInSc1	Hammerclaviers
<g/>
,	,	kIx,	,
Editio	Editio	k6eAd1	Editio
Maris	Maris	k1gFnSc1	Maris
<g/>
,	,	kIx,	,
München	München	k1gInSc1	München
1991	[number]	k4	1991
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
925801	[number]	k4	925801
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
David	David	k1gMnSc1	David
Crombie	Crombie	k1gFnSc2	Crombie
<g/>
:	:	kIx,	:
Piano	piano	k1gNnSc1	piano
<g/>
.	.	kIx.	.
</s>
<s>
Evolution	Evolution	k1gInSc1	Evolution
<g/>
,	,	kIx,	,
Design	design	k1gInSc1	design
and	and	k?	and
Performance	performance	k1gFnSc2	performance
<g/>
.	.	kIx.	.
</s>
<s>
London	London	k1gMnSc1	London
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
871547	[number]	k4	871547
<g/>
-	-	kIx~	-
<g/>
99	[number]	k4	99
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Eszter	Eszter	k1gInSc1	Eszter
Fontana	Fontana	k1gFnSc1	Fontana
<g/>
,	,	kIx,	,
Kerstin	Kerstin	k1gMnSc1	Kerstin
Schwarz	Schwarz	k1gMnSc1	Schwarz
<g/>
,	,	kIx,	,
Stewart	Stewart	k1gMnSc1	Stewart
Pollens	Pollensa	k1gFnPc2	Pollensa
<g/>
,	,	kIx,	,
Gabriele	Gabriela	k1gFnSc6	Gabriela
Rossi-Rognoni	Rossi-Rognoň	k1gFnSc6	Rossi-Rognoň
<g/>
:	:	kIx,	:
Bartolomeo	Bartolomeo	k1gNnSc1	Bartolomeo
Cristofori	Cristofor	k1gFnSc2	Cristofor
–	–	k?	–
Hofinstrumentenbauer	Hofinstrumentenbauer	k1gMnSc1	Hofinstrumentenbauer
der	drát	k5eAaImRp2nS	drát
Medici	medik	k1gMnPc5	medik
/	/	kIx~	/
Strumentaio	Strumentaio	k1gNnSc4	Strumentaio
alla	alla	k6eAd1	alla
corte	corte	k5eAaPmIp2nP	corte
medicea	medicea	k6eAd1	medicea
/	/	kIx~	/
Court	Court	k1gInSc1	Court
Instrument	instrumentum	k1gNnPc2	instrumentum
Maker	makro	k1gNnPc2	makro
of	of	k?	of
the	the	k?	the
Medici	medik	k1gMnPc1	medik
<g/>
.	.	kIx.	.
</s>
<s>
Janos	Janos	k1gInSc1	Janos
Stekovics	Stekovics	k1gInSc1	Stekovics
<g/>
,	,	kIx,	,
Halle	Halle	k1gInSc1	Halle
<g/>
/	/	kIx~	/
<g/>
Saale	Saale	k1gInSc1	Saale
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
932863	[number]	k4	932863
<g/>
-	-	kIx~	-
<g/>
93	[number]	k4	93
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kerstin	Kerstin	k1gMnSc1	Kerstin
Schwarz	Schwarz	k1gMnSc1	Schwarz
(	(	kIx(	(
<g/>
Hrsg	Hrsg	k1gMnSc1	Hrsg
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Bartolomeo	Bartolomeo	k1gMnSc1	Bartolomeo
Cristofori	Cristofor	k1gFnSc2	Cristofor
–	–	k?	–
Hammerflügel	Hammerflügel	k1gMnSc1	Hammerflügel
und	und	k?	und
Cembali	Cembali	k1gMnSc1	Cembali
im	im	k?	im
Vergleich	Vergleich	k1gMnSc1	Vergleich
<g/>
.	.	kIx.	.
</s>
<s>
Mit	Mit	k?	Mit
Beiträgen	Beiträgen	k1gInSc1	Beiträgen
von	von	k1gInSc1	von
Rainer	Rainra	k1gFnPc2	Rainra
Behrends	Behrends	k1gInSc1	Behrends
<g/>
,	,	kIx,	,
Irmela	Irmela	k1gFnSc1	Irmela
Breidenstein	Breidenstein	k1gMnSc1	Breidenstein
und	und	k?	und
Klaus	Klaus	k1gMnSc1	Klaus
Gernhardt	Gernhardt	k1gMnSc1	Gernhardt
<g/>
.	.	kIx.	.
</s>
<s>
Halle	Halle	k1gInSc1	Halle
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
932863	[number]	k4	932863
<g/>
-	-	kIx~	-
<g/>
97	[number]	k4	97
<g/>
-	-	kIx~	-
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
=	=	kIx~	=
Scripta	Scripta	k1gFnSc1	Scripta
Artium	Artium	k1gNnSc1	Artium
<g/>
,	,	kIx,	,
Bd	Bd	k1gFnSc1	Bd
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
,	,	kIx,	,
Schriftenreihe	Schriftenreihe	k1gFnSc1	Schriftenreihe
der	drát	k5eAaImRp2nS	drát
Kunstsammlungen	Kunstsammlungen	k1gInSc1	Kunstsammlungen
der	drát	k5eAaImRp2nS	drát
Universität	Universität	k1gMnSc1	Universität
Leipzig	Leipzig	k1gMnSc1	Leipzig
<g/>
)	)	kIx)	)
</s>
</p>
