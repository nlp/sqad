<s>
Bartolomeo	Bartolomeo	k1gMnSc1	Bartolomeo
Cristofori	Cristofor	k1gFnSc2	Cristofor
di	di	k?	di
Francesco	Francesco	k1gMnSc1	Francesco
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1655	[number]	k4	1655
Padova	Padova	k1gFnSc1	Padova
–	–	k?	–
27	[number]	k4	27
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1731	[number]	k4	1731
Florencie	Florencie	k1gFnSc2	Florencie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
italský	italský	k2eAgMnSc1d1	italský
výrobce	výrobce	k1gMnSc1	výrobce
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
považovaný	považovaný	k2eAgInSc1d1	považovaný
za	za	k7c4	za
vynálezce	vynálezce	k1gMnSc4	vynálezce
klavíru	klavír	k1gInSc2	klavír
<g/>
.	.	kIx.	.
</s>
