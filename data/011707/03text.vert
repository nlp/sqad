<p>
<s>
Kunija	Kunija	k6eAd1	Kunija
Daini	Dain	k1gMnPc1	Dain
(	(	kIx(	(
<g/>
*	*	kIx~	*
12	[number]	k4	12
<g/>
.	.	kIx.	.
říjen	říjen	k1gInSc1	říjen
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
japonský	japonský	k2eAgMnSc1d1	japonský
fotbalista	fotbalista	k1gMnSc1	fotbalista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klubová	klubový	k2eAgFnSc1d1	klubová
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Hrával	hrávat	k5eAaImAgMnS	hrávat
za	za	k7c4	za
Mitsubishi	mitsubishi	k1gNnSc4	mitsubishi
Motors	Motorsa	k1gFnPc2	Motorsa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reprezentační	reprezentační	k2eAgFnSc1d1	reprezentační
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Kunija	Kunija	k6eAd1	Kunija
Daini	Daieň	k1gFnSc3	Daieň
odehrál	odehrát	k5eAaPmAgInS	odehrát
za	za	k7c4	za
japonský	japonský	k2eAgInSc4d1	japonský
národní	národní	k2eAgInSc4d1	národní
tým	tým	k1gInSc4	tým
v	v	k7c6	v
letech	let	k1gInPc6	let
1972	[number]	k4	1972
<g/>
–	–	k?	–
<g/>
1976	[number]	k4	1976
celkem	celkem	k6eAd1	celkem
44	[number]	k4	44
reprezentačních	reprezentační	k2eAgNnPc2d1	reprezentační
utkání	utkání	k1gNnPc2	utkání
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Statistiky	statistika	k1gFnPc1	statistika
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
National	Nationat	k5eAaPmAgInS	Nationat
Football	Football	k1gMnSc1	Football
Teams	Teamsa	k1gFnPc2	Teamsa
</s>
</p>
<p>
<s>
Japan	japan	k1gInSc1	japan
National	National	k1gFnSc2	National
Football	Footballa	k1gFnPc2	Footballa
Team	team	k1gInSc1	team
Database	Databasa	k1gFnSc6	Databasa
</s>
</p>
