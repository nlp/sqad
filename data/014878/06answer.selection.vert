<s desamb="1">
Vazal	vazal	k1gMnSc1
se	se	k3xPyFc4
zavazuje	zavazovat	k5eAaImIp3nS
k	k	k7c3
věrnosti	věrnost	k1gFnSc3
<g/>
,	,	kIx,
poslušnosti	poslušnost	k1gFnSc6
a	a	k8xC
službě	služba	k1gFnSc6
(	(	kIx(
<g/>
zejména	zejména	k9
vojenské	vojenský	k2eAgMnPc4d1
<g/>
)	)	kIx)
vůči	vůči	k7c3
pánovi	pán	k1gMnSc3
<g/>
,	,	kIx,
ten	ten	k3xDgMnSc1
mu	on	k3xPp3gMnSc3
za	za	k7c4
to	ten	k3xDgNnSc4
zaručuje	zaručovat	k5eAaImIp3nS
ochranu	ochrana	k1gFnSc4
a	a	k8xC
materiální	materiální	k2eAgNnSc4d1
zajištění	zajištění	k1gNnSc4
prostřednictvím	prostřednictvím	k7c2
léna	léno	k1gNnSc2
<g/>
.	.	kIx.
</s>