<s>
Feudalismus	feudalismus	k1gInSc1
</s>
<s>
Tři	tři	k4xCgInPc1
stavy	stav	k1gInPc1
na	na	k7c6
středověké	středověký	k2eAgFnSc6d1
miniatuře	miniatura	k1gFnSc6
</s>
<s>
Feudalismus	feudalismus	k1gInSc1
neboli	neboli	k8xC
feudální	feudální	k2eAgNnSc1d1
zřízení	zřízení	k1gNnSc1
je	být	k5eAaImIp3nS
systém	systém	k1gInSc4
lenních	lenní	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
charakteristický	charakteristický	k2eAgInSc1d1
pro	pro	k7c4
evropský	evropský	k2eAgInSc4d1
<g/>
,	,	kIx,
zejména	zejména	k9
vrcholný	vrcholný	k2eAgInSc4d1
středověk	středověk	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
širším	široký	k2eAgInSc6d2
smyslu	smysl	k1gInSc6
se	se	k3xPyFc4
tak	tak	k6eAd1
označuje	označovat	k5eAaImIp3nS
celá	celý	k2eAgFnSc1d1
tehdejší	tehdejší	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
nebo	nebo	k8xC
období	období	k1gNnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mimo	mimo	k7c4
něj	on	k3xPp3gNnSc4
se	se	k3xPyFc4
s	s	k7c7
ním	on	k3xPp3gMnSc7
lze	lze	k6eAd1
výjimečně	výjimečně	k6eAd1
setkat	setkat	k5eAaPmF
i	i	k9
jinde	jinde	k6eAd1
<g/>
,	,	kIx,
například	například	k6eAd1
výrazně	výrazně	k6eAd1
v	v	k7c6
císařském	císařský	k2eAgNnSc6d1
Japonsku	Japonsko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
marxistické	marxistický	k2eAgFnSc6d1
ideologii	ideologie	k1gFnSc6
je	být	k5eAaImIp3nS
tak	tak	k6eAd1
nazýváno	nazývat	k5eAaImNgNnS
uspořádání	uspořádání	k1gNnSc1
společnosti	společnost	k1gFnSc2
a	a	k8xC
celá	celý	k2eAgFnSc1d1
lidská	lidský	k2eAgFnSc1d1
epocha	epocha	k1gFnSc1
od	od	k7c2
zániku	zánik	k1gInSc2
otrokářského	otrokářský	k2eAgInSc2d1
starověku	starověk	k1gInSc2
do	do	k7c2
převládnutí	převládnutí	k1gNnSc2
kapitalismu	kapitalismus	k1gInSc2
<g/>
;	;	kIx,
charakteristická	charakteristický	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
půda	půda	k1gFnSc1
vlastněná	vlastněný	k2eAgFnSc1d1
feudály	feudál	k1gMnPc7
a	a	k8xC
obdělávaná	obdělávaný	k2eAgNnPc1d1
osobně	osobně	k6eAd1
závislými	závislý	k2eAgFnPc7d1
a	a	k8xC
ekonomicky	ekonomicky	k6eAd1
vykořisťovanými	vykořisťovaný	k2eAgMnPc7d1
poddanými	poddaný	k1gMnPc7
(	(	kIx(
<g/>
nevolníky	nevolník	k1gMnPc7
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Název	název	k1gInSc1
je	být	k5eAaImIp3nS
odvozen	odvodit	k5eAaPmNgInS
z	z	k7c2
latinského	latinský	k2eAgNnSc2d1
slova	slovo	k1gNnSc2
feudum	feudum	k1gNnSc4
<g/>
,	,	kIx,
česky	česky	k6eAd1
léno	léno	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Feudální	feudální	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
</s>
<s>
Tři	tři	k4xCgInPc1
stavy	stav	k1gInPc1
ve	v	k7c6
formě	forma	k1gFnSc6
diagramu	diagram	k1gInSc2
</s>
<s>
Královské	královský	k2eAgFnPc1d1
domény	doména	k1gFnPc1
a	a	k8xC
území	území	k1gNnSc1
feudálních	feudální	k2eAgMnPc2d1
pánů	pan	k1gMnPc2
ve	v	k7c6
Francii	Francie	k1gFnSc6
koncem	koncem	k7c2
15	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
</s>
<s>
Feudální	feudální	k2eAgInSc1d1
vztah	vztah	k1gInSc1
se	se	k3xPyFc4
vytváří	vytvářet	k5eAaImIp3nS,k5eAaPmIp3nS
na	na	k7c6
různých	různý	k2eAgFnPc6d1
úrovních	úroveň	k1gFnPc6
mezi	mezi	k7c7
svobodnými	svobodný	k2eAgMnPc7d1
lidmi	člověk	k1gMnPc7
<g/>
,	,	kIx,
vazalem	vazal	k1gMnSc7
na	na	k7c6
jedné	jeden	k4xCgFnSc6
straně	strana	k1gFnSc6
a	a	k8xC
lenním	lenní	k2eAgMnSc7d1
pánem	pán	k1gMnSc7
na	na	k7c6
druhé	druhý	k4xOgFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vazal	vazal	k1gMnSc1
se	se	k3xPyFc4
zavazuje	zavazovat	k5eAaImIp3nS
k	k	k7c3
věrnosti	věrnost	k1gFnSc3
<g/>
,	,	kIx,
poslušnosti	poslušnost	k1gFnSc6
a	a	k8xC
službě	služba	k1gFnSc6
(	(	kIx(
<g/>
zejména	zejména	k9
vojenské	vojenský	k2eAgMnPc4d1
<g/>
)	)	kIx)
vůči	vůči	k7c3
pánovi	pán	k1gMnSc3
<g/>
,	,	kIx,
ten	ten	k3xDgMnSc1
mu	on	k3xPp3gMnSc3
za	za	k7c4
to	ten	k3xDgNnSc4
zaručuje	zaručovat	k5eAaImIp3nS
ochranu	ochrana	k1gFnSc4
a	a	k8xC
materiální	materiální	k2eAgNnSc4d1
zajištění	zajištění	k1gNnSc4
prostřednictvím	prostřednictvím	k7c2
léna	léno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Léno	léno	k1gNnSc1
bývá	bývat	k5eAaImIp3nS
nejčastěji	často	k6eAd3
půda	půda	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
živí	živit	k5eAaImIp3nS
své	svůj	k3xOyFgMnPc4
držitele	držitel	k1gMnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
je	být	k5eAaImIp3nS
obhospodářována	obhospodářovat	k5eAaPmNgFnS,k5eAaImNgFnS,k5eAaBmNgFnS
venkovany	venkovan	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
To	ten	k3xDgNnSc1
také	také	k9
odpovídá	odpovídat	k5eAaImIp3nS
tehdejší	tehdejší	k2eAgInSc1d1
představě	představa	k1gFnSc3
o	o	k7c4
rozdělení	rozdělení	k1gNnSc4
společnosti	společnost	k1gFnSc2
na	na	k7c4
pracující	pracující	k1gMnPc4
<g/>
,	,	kIx,
bojující	bojující	k2eAgMnSc1d1
a	a	k8xC
modlící	modlící	k2eAgMnSc1d1
se	se	k3xPyFc4
<g/>
,	,	kIx,
čili	čili	k8xC
zemědělské	zemědělský	k2eAgNnSc1d1
obyvatelstvo	obyvatelstvo	k1gNnSc1
<g/>
,	,	kIx,
profesionální	profesionální	k2eAgInSc1d1
válečníky	válečník	k1gMnPc7
a	a	k8xC
příslušníky	příslušník	k1gMnPc7
církve	církev	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Systém	systém	k1gInSc1
lenních	lenní	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
sahal	sahat	k5eAaImAgInS
hierarchicky	hierarchicky	k6eAd1
od	od	k7c2
prostého	prostý	k2eAgMnSc2d1
rytíře	rytíř	k1gMnSc2
až	až	k9
po	po	k7c4
vrchního	vrchní	k1gMnSc4
lenního	lenní	k2eAgMnSc2d1
pána	pán	k1gMnSc2
<g/>
,	,	kIx,
panovníka	panovník	k1gMnSc2
monarchie	monarchie	k1gFnSc2
-	-	kIx~
například	například	k6eAd1
krále	král	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
ten	ten	k3xDgMnSc1
se	se	k3xPyFc4
však	však	k9
mohl	moct	k5eAaImAgInS
za	za	k7c2
určitých	určitý	k2eAgFnPc2d1
okolností	okolnost	k1gFnPc2
dostat	dostat	k5eAaPmF
do	do	k7c2
lenní	lenní	k2eAgFnSc2d1
závislosti	závislost	k1gFnSc2
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
o	o	k7c6
tom	ten	k3xDgNnSc6
svědčí	svědčit	k5eAaImIp3nS
formálně	formálně	k6eAd1
vazalské	vazalský	k2eAgNnSc4d1
postavení	postavení	k1gNnSc4
anglického	anglický	k2eAgMnSc2d1
krále	král	k1gMnSc2
vůči	vůči	k7c3
králi	král	k1gMnSc3
francouzskému	francouzský	k2eAgMnSc3d1
<g/>
,	,	kIx,
svému	svůj	k3xOyFgMnSc3
lennímu	lenní	k2eAgMnSc3d1
pánovi	pán	k1gMnSc3
ve	v	k7c6
vztahu	vztah	k1gInSc6
k	k	k7c3
rozsáhlému	rozsáhlý	k2eAgNnSc3d1
plantagenetovskému	plantagenetovský	k2eAgNnSc3d1
panství	panství	k1gNnSc3
na	na	k7c6
území	území	k1gNnSc6
francouzského	francouzský	k2eAgNnSc2d1
království	království	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následné	následný	k2eAgFnSc6d1
stoleté	stoletý	k2eAgFnSc6d1
válce	válka	k1gFnSc6
mezi	mezi	k7c7
oběma	dva	k4xCgInPc7
králi	král	k1gMnPc7
se	se	k3xPyFc4
rytířský	rytířský	k2eAgInSc1d1
způsob	způsob	k1gInSc1
boje	boj	k1gInSc2
ukázal	ukázat	k5eAaPmAgInS
jako	jako	k9
již	již	k6eAd1
neefektivní	efektivní	k2eNgMnSc1d1
<g/>
,	,	kIx,
zejména	zejména	k9
ve	v	k7c6
velkých	velký	k2eAgFnPc6d1
porážkách	porážka	k1gFnPc6
francouzského	francouzský	k2eAgInSc2d1
rytířstva	rytířstvo	k1gNnSc2
anglickými	anglický	k2eAgMnPc7d1
lukostřelci	lukostřelec	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Konec	konec	k1gInSc1
rytířského	rytířský	k2eAgNnSc2d1
období	období	k1gNnSc2
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
jako	jako	k9
uzavření	uzavření	k1gNnSc1
a	a	k8xC
rozdělení	rozdělení	k1gNnSc1
privilegované	privilegovaný	k2eAgFnSc2d1
vrstvy	vrstva	k1gFnSc2
na	na	k7c4
nižší	nízký	k2eAgFnSc4d2
a	a	k8xC
vyšší	vysoký	k2eAgFnSc4d2
šlechtu	šlechta	k1gFnSc4
nebo	nebo	k8xC
vzestup	vzestup	k1gInSc4
významu	význam	k1gInSc2
měst	město	k1gNnPc2
<g/>
,	,	kIx,
signalizoval	signalizovat	k5eAaImAgMnS
konec	konec	k1gInSc4
feudalismu	feudalismus	k1gInSc2
a	a	k8xC
postupný	postupný	k2eAgInSc4d1
přechod	přechod	k1gInSc4
ke	k	k7c3
stavovské	stavovský	k2eAgFnSc3d1
společnosti	společnost	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Marxistický	marxistický	k2eAgInSc1d1
termín	termín	k1gInSc1
</s>
<s>
Vazal	vazal	k1gMnSc1
obřadně	obřadně	k6eAd1
uznává	uznávat	k5eAaImIp3nS
svého	svůj	k3xOyFgMnSc4
lenního	lenní	k2eAgMnSc4d1
pána	pán	k1gMnSc4
<g/>
,	,	kIx,
sklonek	sklonek	k1gInSc4
feudalismu	feudalismus	k1gInSc2
(	(	kIx(
<g/>
asi	asi	k9
1469	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Feudalismus	feudalismus	k1gInSc1
v	v	k7c6
marxistické	marxistický	k2eAgFnSc6d1
literatuře	literatura	k1gFnSc6
nemá	mít	k5eNaImIp3nS
plnou	plný	k2eAgFnSc4d1
souvislost	souvislost	k1gFnSc4
s	s	k7c7
feudem	feudum	k1gNnSc7
(	(	kIx(
<g/>
lénem	léno	k1gNnSc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
bylo	být	k5eAaImAgNnS
vztahem	vztah	k1gInSc7
mezi	mezi	k7c7
šlechtou	šlechta	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
tak	tak	k6eAd1
nazýváno	nazývat	k5eAaImNgNnS
uspořádání	uspořádání	k1gNnSc1
společnosti	společnost	k1gFnSc2
a	a	k8xC
období	období	k1gNnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
společnost	společnost	k1gFnSc1
rozdělena	rozdělit	k5eAaPmNgFnS
na	na	k7c4
vrchnost	vrchnost	k1gFnSc4
a	a	k8xC
poddané	poddaná	k1gFnPc4
<g/>
,	,	kIx,
do	do	k7c2
značné	značný	k2eAgFnSc2d1
míry	míra	k1gFnSc2
souběžné	souběžný	k2eAgFnPc4d1
se	s	k7c7
středověkem	středověk	k1gInSc7
-	-	kIx~
zhruba	zhruba	k6eAd1
od	od	k7c2
konce	konec	k1gInSc2
antiky	antika	k1gFnSc2
do	do	k7c2
16	#num#	k4
<g/>
.	.	kIx.
až	až	k9
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
tím	ten	k3xDgNnSc7
je	být	k5eAaImIp3nS
nutno	nutno	k6eAd1
počítat	počítat	k5eAaImF
u	u	k7c2
naší	náš	k3xOp1gFnSc2
literatury	literatura	k1gFnSc2
z	z	k7c2
let	léto	k1gNnPc2
1948	#num#	k4
až	až	k6eAd1
1989	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
toto	tento	k3xDgNnSc1
marxistické	marxistický	k2eAgNnSc1d1
pojetí	pojetí	k1gNnSc1
bylo	být	k5eAaImAgNnS
povinné	povinný	k2eAgNnSc1d1
<g/>
,	,	kIx,
a	a	k8xC
někdy	někdy	k6eAd1
také	také	k9
<g/>
,	,	kIx,
ze	z	k7c2
setrvačnosti	setrvačnost	k1gFnSc2
<g/>
,	,	kIx,
i	i	k8xC
u	u	k7c2
té	ten	k3xDgFnSc2
pozdější	pozdní	k2eAgFnSc2d2
<g/>
.	.	kIx.
</s>
<s>
Odchýlení	odchýlení	k1gNnSc1
významu	význam	k1gInSc2
</s>
<s>
V	v	k7c6
17	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
už	už	k6eAd1
lenní	lenní	k2eAgInPc1d1
vztahy	vztah	k1gInPc1
nefungovaly	fungovat	k5eNaImAgInP
<g/>
,	,	kIx,
ale	ale	k8xC
vlastnictví	vlastnictví	k1gNnSc1
půdy	půda	k1gFnSc2
mělo	mít	k5eAaImAgNnS
ve	v	k7c6
feudalismu	feudalismus	k1gInSc6
svůj	svůj	k3xOyFgInSc4
původ	původ	k1gInSc4
<g/>
,	,	kIx,
se	se	k3xPyFc4
u	u	k7c2
anglických	anglický	k2eAgMnPc2d1
právníků	právník	k1gMnPc2
objevuje	objevovat	k5eAaImIp3nS
termín	termín	k1gInSc4
feudální	feudální	k2eAgInSc4d1
zřízení	zřízení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
tomu	ten	k3xDgInSc3
se	se	k3xPyFc4
tak	tak	k8xC,k8xS
začala	začít	k5eAaPmAgFnS
označovat	označovat	k5eAaImF
soustava	soustava	k1gFnSc1
pozemkového	pozemkový	k2eAgNnSc2d1
vlastnictví	vlastnictví	k1gNnSc2
a	a	k8xC
později	pozdě	k6eAd2
s	s	k7c7
ním	on	k3xPp3gMnSc7
spojená	spojený	k2eAgNnPc4d1
vrchnostenská	vrchnostenský	k2eAgNnPc4d1
práva	právo	k1gNnPc4
těžce	těžce	k6eAd1
doléhající	doléhající	k2eAgFnPc1d1
na	na	k7c4
venkovské	venkovský	k2eAgNnSc4d1
obyvatelstvo	obyvatelstvo	k1gNnSc4
<g/>
,	,	kIx,
ve	v	k7c6
Francii	Francie	k1gFnSc6
zrušená	zrušený	k2eAgFnSc1d1
za	za	k7c2
revoluce	revoluce	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1789	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Tím	ten	k3xDgNnSc7
výraz	výraz	k1gInSc4
feudální	feudální	k2eAgNnSc4d1
zřízení	zřízení	k1gNnSc4
získal	získat	k5eAaPmAgInS
politický	politický	k2eAgInSc1d1
význam	význam	k1gInSc1
a	a	k8xC
v	v	k7c6
tomto	tento	k3xDgInSc6
smyslu	smysl	k1gInSc6
jej	on	k3xPp3gInSc4
přijal	přijmout	k5eAaPmAgMnS
i	i	k8xC
Karel	Karel	k1gMnSc1
Marx	Marx	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Později	pozdě	k6eAd2
v	v	k7c6
devatenáctém	devatenáctý	k4xOgInSc6
století	století	k1gNnSc6
<g/>
,	,	kIx,
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
rozvojem	rozvoj	k1gInSc7
vědeckého	vědecký	k2eAgNnSc2d1
studia	studio	k1gNnSc2
středověku	středověk	k1gInSc2
<g/>
,	,	kIx,
slovo	slovo	k1gNnSc1
feudální	feudální	k2eAgNnSc1d1
získalo	získat	k5eAaPmAgNnS
zpět	zpět	k6eAd1
svůj	svůj	k3xOyFgInSc4
logický	logický	k2eAgInSc4d1
a	a	k8xC
odůvodněný	odůvodněný	k2eAgInSc4d1
význam	význam	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Marxisté	marxista	k1gMnPc1
zůstali	zůstat	k5eAaPmAgMnP
věrni	věren	k2eAgMnPc1d1
dobovému	dobový	k2eAgInSc3d1
významu	význam	k1gInSc3
podle	podle	k7c2
svého	svůj	k3xOyFgMnSc4
učitele	učitel	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s>
Marxistická	marxistický	k2eAgFnSc1d1
definice	definice	k1gFnSc1
</s>
<s>
V	v	k7c6
marxistickém	marxistický	k2eAgNnSc6d1
pojetí	pojetí	k1gNnSc6
historie	historie	k1gFnSc2
stupeň	stupeň	k1gInSc4
společenského	společenský	k2eAgInSc2d1
vývoje	vývoj	k1gInSc2
(	(	kIx(
<g/>
tzv.	tzv.	kA
společensko-ekonomická	společensko-ekonomický	k2eAgFnSc1d1
formace	formace	k1gFnSc1
<g/>
)	)	kIx)
předcházející	předcházející	k2eAgInSc1d1
kapitalismus	kapitalismus	k1gInSc1
<g/>
,	,	kIx,
v	v	k7c6
němž	jenž	k3xRgInSc6
vládnoucí	vládnoucí	k2eAgFnSc1d1
třída	třída	k1gFnSc1
(	(	kIx(
<g/>
feudálové	feudál	k1gMnPc1
<g/>
)	)	kIx)
vlastní	vlastnit	k5eAaImIp3nS
základní	základní	k2eAgInSc1d1
výrobní	výrobní	k2eAgInSc1d1
prostředek	prostředek	k1gInSc1
–	–	k?
půdu	půda	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
neúplném	úplný	k2eNgNnSc6d1
vlastnictví	vlastnictví	k1gNnSc6
-	-	kIx~
resp.	resp.	kA
trvalém	trvalý	k2eAgNnSc6d1
područí	područí	k1gNnSc1
-	-	kIx~
feudálů	feudál	k1gMnPc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
i	i	k9
zemědělské	zemědělský	k2eAgNnSc1d1
obyvatelstvo	obyvatelstvo	k1gNnSc1
-	-	kIx~
nevolníci	nevolník	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
feudálovi	feudálův	k2eAgMnPc1d1
odevzdávají	odevzdávat	k5eAaImIp3nP
část	část	k1gFnSc4
své	svůj	k3xOyFgFnSc2
produkce	produkce	k1gFnSc2
<g/>
,	,	kIx,
případně	případně	k6eAd1
vlastními	vlastní	k2eAgInPc7d1
nástroji	nástroj	k1gInPc7
obdělává	obdělávat	k5eAaImIp3nS
půdu	půda	k1gFnSc4
feudálem	feudál	k1gMnSc7
nepropůjčenou	propůjčený	k2eNgFnSc4d1
(	(	kIx(
<g/>
robota	robot	k1gMnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zatímco	zatímco	k8xS
ve	v	k7c6
starověku	starověk	k1gInSc6
převládala	převládat	k5eAaImAgFnS
ekonomika	ekonomika	k1gFnSc1
založená	založený	k2eAgFnSc1d1
na	na	k7c4
otrokářství	otrokářství	k1gNnSc4
<g/>
,	,	kIx,
feudalismus	feudalismus	k1gInSc1
znamená	znamenat	k5eAaImIp3nS
pokrok	pokrok	k1gInSc4
ve	v	k7c6
vývoji	vývoj	k1gInSc6
výrobních	výrobní	k2eAgInPc2d1
vztahů	vztah	k1gInPc2
<g/>
,	,	kIx,
protože	protože	k8xS
feudální	feudální	k2eAgFnSc1d1
půda	půda	k1gFnSc1
je	být	k5eAaImIp3nS
rozdělena	rozdělit	k5eAaPmNgFnS
mezi	mezi	k7c4
samostatněji	samostatně	k6eAd2
hospodařící	hospodařící	k2eAgMnPc4d1
drobné	drobný	k2eAgMnPc4d1
rolníky	rolník	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Feudalismus	feudalismus	k1gInSc1
kolísal	kolísat	k5eAaImAgInS
v	v	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
od	od	k7c2
rolnického	rolnický	k2eAgNnSc2d1
trvalého	trvalý	k2eAgNnSc2d1
vlastnictví	vlastnictví	k1gNnSc2
půdy	půda	k1gFnSc2
a	a	k8xC
relativní	relativní	k2eAgFnSc6d1
osobní	osobní	k2eAgFnSc6d1
svobodě	svoboda	k1gFnSc6
obyvatelstva	obyvatelstvo	k1gNnSc2
(	(	kIx(
<g/>
poddanství	poddanství	k1gNnSc2
<g/>
)	)	kIx)
k	k	k7c3
úplné	úplný	k2eAgFnSc3d1
závislosti	závislost	k1gFnSc3
a	a	k8xC
podřízenosti	podřízenost	k1gFnSc6
blízké	blízký	k2eAgNnSc1d1
otroctví	otroctví	k1gNnSc1
(	(	kIx(
<g/>
nevolnictví	nevolnictví	k1gNnSc1
<g/>
)	)	kIx)
a	a	k8xC
naopak	naopak	k6eAd1
<g/>
,	,	kIx,
vývoj	vývoj	k1gInSc1
nebyl	být	k5eNaImAgInS
jednoznačný	jednoznačný	k2eAgMnSc1d1
(	(	kIx(
<g/>
např.	např.	kA
druhé	druhý	k4xOgFnSc2
nevolnictví	nevolnictví	k1gNnPc2
v	v	k7c6
novověku	novověk	k1gInSc6
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Osobní	osobní	k2eAgFnSc1d1
svoboda	svoboda	k1gFnSc1
je	být	k5eAaImIp3nS
podmínkou	podmínka	k1gFnSc7
rozvoje	rozvoj	k1gInSc2
flexibilní	flexibilní	k2eAgFnSc2d1
námezdní	námezdní	k2eAgFnSc2d1
práce	práce	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
umožňuje	umožňovat	k5eAaImIp3nS
pokrok	pokrok	k1gInSc4
k	k	k7c3
vyšší	vysoký	k2eAgFnSc3d2
formě	forma	k1gFnSc3
společensko-ekonomické	společensko-ekonomický	k2eAgFnSc2d1
formace	formace	k1gFnSc2
<g/>
,	,	kIx,
kapitalismu	kapitalismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
naopak	naopak	k6eAd1
<g/>
,	,	kIx,
vývoj	vývoj	k1gInSc1
kapitalismu	kapitalismus	k1gInSc2
podporuje	podporovat	k5eAaImIp3nS
osvobozování	osvobozování	k1gNnSc2
nevolníků	nevolník	k1gMnPc2
resp.	resp.	kA
poddaných	poddaná	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Akademický	akademický	k2eAgInSc4d1
slovník	slovník	k1gInSc4
cizích	cizí	k2eAgNnPc2d1
slov	slovo	k1gNnPc2
<g/>
,	,	kIx,
Academia	academia	k1gFnSc1
<g/>
,	,	kIx,
2001	#num#	k4
(	(	kIx(
<g/>
ISBN	ISBN	kA
80	#num#	k4
<g/>
-	-	kIx~
<g/>
200	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
607	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Váša	Váša	k1gMnSc1
<g/>
;	;	kIx,
Trávníček	Trávníček	k1gMnSc1
<g/>
:	:	kIx,
Slovník	slovník	k1gInSc1
jazyka	jazyk	k1gInSc2
českého	český	k2eAgInSc2d1
<g/>
,	,	kIx,
Fr.	Fr.	k1gFnPc7
Borový	borový	k2eAgInSc1d1
Praha	Praha	k1gFnSc1
19371	#num#	k4
2	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
Klimeš	Klimeš	k1gMnSc1
<g/>
,	,	kIx,
Lumír	Lumír	k1gMnSc1
<g/>
:	:	kIx,
Slovník	slovník	k1gInSc1
cizích	cizí	k2eAgNnPc2d1
slov	slovo	k1gNnPc2
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
<g/>
vyd	vyd	k?
<g/>
.	.	kIx.
<g/>
,	,	kIx,
Státní	státní	k2eAgNnSc1d1
pedagogické	pedagogický	k2eAgNnSc1d1
nakladatelství	nakladatelství	k1gNnSc1
Praha	Praha	k1gFnSc1
19831	#num#	k4
2	#num#	k4
Le	Le	k1gMnSc1
Goff	Goff	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
<g/>
,	,	kIx,
Kultura	kultura	k1gFnSc1
středověké	středověký	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1991	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
<g/>
426	#num#	k4
<g/>
-	-	kIx~
<g/>
427	#num#	k4
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
Feudální	feudální	k2eAgNnSc1d1
zřízení	zřízení	k1gNnSc1
(	(	kIx(
<g/>
definice	definice	k1gFnSc1
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
Le	Le	k1gMnSc1
Goff	Goff	k1gMnSc1
<g/>
,	,	kIx,
J.	J.	kA
<g/>
,	,	kIx,
Kultura	kultura	k1gFnSc1
středověké	středověký	k2eAgFnSc2d1
Evropy	Evropa	k1gFnSc2
<g/>
,	,	kIx,
s.	s.	k?
<g/>
426	#num#	k4
heslo	heslo	k1gNnSc4
Feudální	feudální	k2eAgNnSc4d1
zřízení	zřízení	k1gNnSc4
(	(	kIx(
<g/>
ideologické	ideologický	k2eAgNnSc4d1
užití	užití	k1gNnSc4
termínu	termín	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Léno	léno	k1gNnSc1
</s>
<s>
Vazal	vazal	k1gMnSc1
</s>
<s>
Poddanství	poddanství	k1gNnSc1
</s>
<s>
Trojí	trojí	k4xRgInSc1
lid	lid	k1gInSc1
</s>
<s>
Historický	historický	k2eAgInSc1d1
materialismus	materialismus	k1gInSc1
</s>
<s>
Kapitalismus	kapitalismus	k1gInSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
feudalismus	feudalismus	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Feudalismus	feudalismus	k1gInSc1
-	-	kIx~
lenní	lenní	k2eAgInSc1d1
systém	systém	k1gInSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4131524-8	4131524-8	k4
</s>
