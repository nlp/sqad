<s>
Největší	veliký	k2eAgInPc4d3	veliký
současné	současný	k2eAgInPc4d1	současný
reflektory	reflektor	k1gInPc4	reflektor
mají	mít	k5eAaImIp3nP	mít
průměr	průměr	k1gInSc4	průměr
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
kolem	kolem	k7c2	kolem
10	[number]	k4	10
m	m	kA	m
<g/>
,	,	kIx,	,
největší	veliký	k2eAgInSc1d3	veliký
dalekohled	dalekohled	k1gInSc1	dalekohled
v	v	k7c6	v
ČR	ČR	kA	ČR
je	být	k5eAaImIp3nS	být
umístěn	umístit	k5eAaPmNgInS	umístit
v	v	k7c6	v
Ondřejově	Ondřejův	k2eAgFnSc6d1	Ondřejova
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
průměr	průměr	k1gInSc4	průměr
zrcadla	zrcadlo	k1gNnSc2	zrcadlo
2	[number]	k4	2
m.	m.	k?	m.
Pro	pro	k7c4	pro
ještě	ještě	k6eAd1	ještě
větší	veliký	k2eAgInPc4d2	veliký
projekty	projekt	k1gInPc4	projekt
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
automaticky	automaticky	k6eAd1	automaticky
koordinovaných	koordinovaný	k2eAgFnPc2d1	koordinovaná
soustav	soustava	k1gFnPc2	soustava
segmentovaných	segmentovaný	k2eAgNnPc2d1	segmentované
zrcadel	zrcadlo	k1gNnPc2	zrcadlo
<g/>
.	.	kIx.	.
</s>
