<s>
Bumerang	bumerang	k1gInSc1
</s>
<s>
Typický	typický	k2eAgMnSc1d1
vracející	vracející	k2eAgMnSc1d1
se	se	k3xPyFc4
bumerang	bumerang	k1gInSc1
</s>
<s>
Možná	možná	k9
hledáte	hledat	k5eAaImIp2nP
<g/>
:	:	kIx,
článek	článek	k1gInSc1
o	o	k7c6
mlhovině	mlhovina	k1gFnSc6
-	-	kIx~
viz	vidět	k5eAaImRp2nS
Mlhovina	mlhovina	k1gFnSc1
Bumerang	bumerang	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Bumerang	bumerang	k1gInSc1
je	být	k5eAaImIp3nS
jednoduchý	jednoduchý	k2eAgInSc1d1
dřevěný	dřevěný	k2eAgInSc1d1
nástroj	nástroj	k1gInSc1
užívaný	užívaný	k2eAgInSc1d1
k	k	k7c3
různým	různý	k2eAgInPc3d1
účelům	účel	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
prvotně	prvotně	k6eAd1
spojován	spojovat	k5eAaImNgMnS
s	s	k7c7
původními	původní	k2eAgMnPc7d1
obyvateli	obyvatel	k1gMnPc7
Austrálie	Austrálie	k1gFnSc2
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
ho	on	k3xPp3gMnSc4
lze	lze	k6eAd1
najít	najít	k5eAaPmF
i	i	k9
v	v	k7c6
různých	různý	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
Afriky	Afrika	k1gFnSc2
či	či	k8xC
u	u	k7c2
některých	některý	k3yIgInPc2
indiánských	indiánský	k2eAgInPc2d1
kmenů	kmen	k1gInPc2
v	v	k7c6
Americe	Amerika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
polské	polský	k2eAgFnSc6d1
straně	strana	k1gFnSc6
Tater	Tatra	k1gFnPc2
byl	být	k5eAaImAgInS
nalezen	nalézt	k5eAaBmNgInS,k5eAaPmNgInS
dosud	dosud	k6eAd1
nejstarší	starý	k2eAgInSc1d3
bumerang	bumerang	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gNnSc1
stáří	stáří	k1gNnSc1
je	být	k5eAaImIp3nS
odhadováno	odhadován	k2eAgNnSc1d1
na	na	k7c4
zhruba	zhruba	k6eAd1
20	#num#	k4
000	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
účelu	účel	k1gInSc2
bumerangu	bumerang	k1gInSc2
se	se	k3xPyFc4
liší	lišit	k5eAaImIp3nP
i	i	k9
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
tvar	tvar	k1gInSc4
<g/>
,	,	kIx,
nejznámější	známý	k2eAgMnSc1d3
je	být	k5eAaImIp3nS
vracející	vracející	k2eAgMnSc1d1
se	se	k3xPyFc4
bumerang	bumerang	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
v	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
správně	správně	k6eAd1
odhozen	odhozen	k2eAgInSc1d1
<g/>
,	,	kIx,
letí	letět	k5eAaImIp3nS
na	na	k7c6
zakřivené	zakřivený	k2eAgFnSc6d1
dráze	dráha	k1gFnSc6
a	a	k8xC
vrací	vracet	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
místo	místo	k1gNnSc4
vypuštění	vypuštění	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiné	jiný	k2eAgInPc1d1
bumerangy	bumerang	k1gInPc1
se	se	k3xPyFc4
ale	ale	k9
nevracejí	vracet	k5eNaImIp3nP
a	a	k8xC
některé	některý	k3yIgInPc1
se	se	k3xPyFc4
dokonce	dokonce	k9
vůbec	vůbec	k9
nevyhazují	vyhazovat	k5eNaImIp3nP
a	a	k8xC
původní	původní	k2eAgMnPc1d1
obyvatelé	obyvatel	k1gMnPc1
Austrálie	Austrálie	k1gFnSc2
je	on	k3xPp3gMnPc4
používali	používat	k5eAaImAgMnP
jako	jako	k8xC,k8xS
ruční	ruční	k2eAgFnSc4d1
zbraň	zbraň	k1gFnSc4
k	k	k7c3
boji	boj	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Bumerangům	bumerang	k1gInPc3
podobné	podobný	k2eAgInPc4d1
lovecké	lovecký	k2eAgInPc4d1
nástroje	nástroj	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
vrhaly	vrhat	k5eAaImAgFnP
po	po	k7c6
přímé	přímý	k2eAgFnSc6d1
dráze	dráha	k1gFnSc6
a	a	k8xC
nevracely	vracet	k5eNaImAgFnP
se	se	k3xPyFc4
zpět	zpět	k6eAd1
<g/>
,	,	kIx,
se	se	k3xPyFc4
nazývají	nazývat	k5eAaImIp3nP
kylie	kylie	k1gFnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Bumerangy	bumerang	k1gInPc1
se	se	k3xPyFc4
užívají	užívat	k5eAaImIp3nP
jako	jako	k9
nástroje	nástroj	k1gInPc1
na	na	k7c4
lov	lov	k1gInSc4
<g/>
,	,	kIx,
hudební	hudební	k2eAgInPc4d1
nástroje	nástroj	k1gInPc4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
zbraně	zbraň	k1gFnPc4
nebo	nebo	k8xC
hračky	hračka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gFnSc4
velikost	velikost	k1gFnSc4
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
v	v	k7c6
rozmezí	rozmezí	k1gNnSc6
od	od	k7c2
10	#num#	k4
centimetrů	centimetr	k1gInPc2
do	do	k7c2
2	#num#	k4
metrů	metr	k1gInPc2
délky	délka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Nástroje	nástroj	k1gInPc1
podobné	podobný	k2eAgInPc1d1
bumerangům	bumerang	k1gInPc3
<g/>
,	,	kIx,
různé	různý	k2eAgFnPc1d1
lovicí	lovicí	k2eAgFnPc1d1
hůlky	hůlka	k1gFnPc1
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
užívány	užívat	k5eAaImNgFnP
k	k	k7c3
lovu	lov	k1gInSc3
<g/>
,	,	kIx,
náboženským	náboženský	k2eAgInPc3d1
obřadům	obřad	k1gInPc3
i	i	k8xC
zábavě	zábava	k1gFnSc6
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejich	jejich	k3xOp3gInPc4
původ	původ	k1gInSc1
není	být	k5eNaImIp3nS
zcela	zcela	k6eAd1
jasný	jasný	k2eAgInSc1d1
<g/>
:	:	kIx,
objeveny	objeven	k2eAgFnPc1d1
byly	být	k5eAaImAgFnP
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
,	,	kIx,
Egyptě	Egypt	k1gInSc6
<g/>
,	,	kIx,
nejznámější	známý	k2eAgNnPc1d3
jsou	být	k5eAaImIp3nP
ale	ale	k9
bumerangy	bumerang	k1gInPc1
australské	australský	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tamní	tamní	k2eAgMnPc1d1
původní	původní	k2eAgMnPc1d1
obyvatelé	obyvatel	k1gMnPc1
je	on	k3xPp3gMnPc4
používali	používat	k5eAaImAgMnP
po	po	k7c4
tisíce	tisíc	k4xCgInSc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jméno	jméno	k1gNnSc1
se	se	k3xPyFc4
odvozuje	odvozovat	k5eAaImIp3nS
od	od	k7c2
slova	slovo	k1gNnSc2
bou-mar-rang	bou-mar-ranga	k1gFnPc2
kmene	kmen	k1gInSc2
Turuwal	Turuwal	k1gInSc4
<g/>
,	,	kIx,
sídlícího	sídlící	k2eAgNnSc2d1
v	v	k7c6
oblasti	oblast	k1gFnSc6
současného	současný	k2eAgInSc2d1
přístavu	přístav	k1gInSc2
v	v	k7c6
Sydney	Sydney	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tvar	tvar	k1gInSc1
</s>
<s>
Dráha	dráha	k1gFnSc1
letu	let	k1gInSc2
bumerangu	bumerang	k1gInSc2
</s>
<s>
Vracející	vracející	k2eAgMnSc1d1
se	se	k3xPyFc4
bumerang	bumerang	k1gInSc1
má	mít	k5eAaImIp3nS
profil	profil	k1gInSc4
křídla	křídlo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
je	být	k5eAaImIp3nS
vyhozen	vyhozen	k2eAgInSc1d1
s	s	k7c7
příslušnou	příslušný	k2eAgFnSc7d1
rotací	rotace	k1gFnSc7
<g/>
,	,	kIx,
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
se	se	k3xPyFc4
aerodynamický	aerodynamický	k2eAgInSc1d1
vztlak	vztlak	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
jeho	jeho	k3xOp3gFnSc4
dráhu	dráha	k1gFnSc4
zakřivuje	zakřivovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bumerangy	bumerang	k1gInPc4
k	k	k7c3
lovu	lov	k1gInSc3
jsou	být	k5eAaImIp3nP
obvykle	obvykle	k6eAd1
větší	veliký	k2eAgNnSc4d2
a	a	k8xC
těžší	těžký	k2eAgNnSc4d2
<g/>
,	,	kIx,
po	po	k7c6
zasažení	zasažení	k1gNnSc6
kořisti	kořist	k1gFnSc2
padají	padat	k5eAaImIp3nP
k	k	k7c3
zemi	zem	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sportovní	sportovní	k2eAgInPc1d1
bumerangy	bumerang	k1gInPc1
jsou	být	k5eAaImIp3nP
lehké	lehký	k2eAgFnPc1d1
a	a	k8xC
malé	malý	k2eAgFnPc1d1
<g/>
,	,	kIx,
váží	vážit	k5eAaImIp3nP
méně	málo	k6eAd2
než	než	k8xS
100	#num#	k4
gramů	gram	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Existují	existovat	k5eAaImIp3nP
bumerangy	bumerang	k1gInPc1
pravotočivé	pravotočivý	k2eAgFnSc2d1
a	a	k8xC
levotočivé	levotočivý	k2eAgFnSc2d1
<g/>
,	,	kIx,
pravotočivý	pravotočivý	k2eAgMnSc1d1
svou	svůj	k3xOyFgFnSc4
dráhu	dráha	k1gFnSc4
zakřivuje	zakřivovat	k5eAaImIp3nS
doprava	doprava	k1gFnSc1
a	a	k8xC
naopak	naopak	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oba	dva	k4xCgInPc1
je	on	k3xPp3gMnPc4
možné	možný	k2eAgNnSc1d1
házet	házet	k5eAaImF
oběma	dva	k4xCgFnPc7
rukama	ruka	k1gFnPc7
<g/>
,	,	kIx,
ale	ale	k8xC
směr	směr	k1gInSc1
letu	let	k1gInSc2
tím	ten	k3xDgNnSc7
nelze	lze	k6eNd1
ovlivnit	ovlivnit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
,	,	kIx,
je	být	k5eAaImIp3nS
nutné	nutný	k2eAgNnSc1d1
respektovat	respektovat	k5eAaImF
jeho	jeho	k3xOp3gInSc4,k3xPp3gInSc4
směr	směr	k1gInSc4
a	a	k8xC
házet	házet	k5eAaImF
zhruba	zhruba	k6eAd1
45	#num#	k4
<g/>
°	°	k?
oproti	oproti	k7c3
němu	on	k3xPp3gMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bumerangu	bumerang	k1gInSc2
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
udělit	udělit	k5eAaPmF
spíše	spíše	k9
větší	veliký	k2eAgFnSc4d2
rotaci	rotace	k1gFnSc4
než	než	k8xS
dopřednou	dopředný	k2eAgFnSc4d1
rychlost	rychlost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
je	být	k5eAaImIp3nS
správně	správně	k6eAd1
hozen	hodit	k5eAaPmNgMnS
<g/>
,	,	kIx,
ve	v	k7c6
vzduchu	vzduch	k1gInSc6
působením	působení	k1gNnSc7
fyzikálních	fyzikální	k2eAgFnPc2d1
sil	síla	k1gFnPc2
letí	letět	k5eAaImIp3nS
po	po	k7c6
obloukové	obloukový	k2eAgFnSc6d1
dráze	dráha	k1gFnSc6
a	a	k8xC
vrátí	vrátit	k5eAaPmIp3nS
se	se	k3xPyFc4
zpět	zpět	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
návratu	návrat	k1gInSc6
by	by	kYmCp3nS
se	se	k3xPyFc4
měl	mít	k5eAaImAgInS
snášet	snášet	k5eAaImF
ve	v	k7c6
vodorovné	vodorovný	k2eAgFnSc6d1
poloze	poloha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bezpečný	bezpečný	k2eAgInSc1d1
způsob	způsob	k1gInSc1
chytání	chytání	k1gNnSc2
je	být	k5eAaImIp3nS
mezi	mezi	k7c4
obě	dva	k4xCgFnPc4
ruce	ruka	k1gFnPc4
(	(	kIx(
<g/>
na	na	k7c4
způsob	způsob	k1gInSc4
sendviče	sendvič	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Bumerang	bumerang	k1gInSc1
se	se	k3xPyFc4
obvykle	obvykle	k6eAd1
hází	házet	k5eAaImIp3nS
s	s	k7c7
úhlem	úhel	k1gInSc7
naklonění	naklonění	k1gNnSc2
cca	cca	kA
45	#num#	k4
<g/>
°	°	k?
od	od	k7c2
vertikály	vertikála	k1gFnSc2
a	a	k8xC
směr	směr	k1gInSc1
hodu	hod	k1gInSc2
vpřed	vpřed	k6eAd1
je	být	k5eAaImIp3nS
mírně	mírně	k6eAd1
nad	nad	k7c4
vodorovnou	vodorovný	k2eAgFnSc4d1
úroveň	úroveň	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Speciální	speciální	k2eAgInPc1d1
typy	typ	k1gInPc1
sportovních	sportovní	k2eAgInPc2d1
bumerangů	bumerang	k1gInPc2
pro	pro	k7c4
'	'	kIx"
<g/>
LD	LD	kA
=	=	kIx~
Long	Long	k1gMnSc1
Distance	distance	k1gFnSc2
<g/>
'	'	kIx"
se	se	k3xPyFc4
hází	házet	k5eAaImIp3nS
s	s	k7c7
úhlem	úhel	k1gInSc7
naklonění	naklonění	k1gNnSc2
téměř	téměř	k6eAd1
vodorovně	vodorovně	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2008	#num#	k4
bumerang	bumerang	k1gInSc1
otestoval	otestovat	k5eAaPmAgInS
v	v	k7c6
kosmu	kosmos	k1gInSc6
japonský	japonský	k2eAgMnSc1d1
astronaut	astronaut	k1gMnSc1
Takao	Takao	k1gMnSc1
Doi	Doi	k1gMnSc1
a	a	k8xC
potvrdil	potvrdit	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
vrací	vracet	k5eAaImIp3nS
i	i	k9
ve	v	k7c6
stavu	stav	k1gInSc6
beztíže	beztíže	k1gFnSc2
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Sportovní	sportovní	k2eAgInSc1d1
bumerang	bumerang	k1gInSc1
</s>
<s>
Moderní	moderní	k2eAgInPc4d1
sportovní	sportovní	k2eAgInPc4d1
bumerangy	bumerang	k1gInPc4
</s>
<s>
Dnes	dnes	k6eAd1
jsou	být	k5eAaImIp3nP
bumerangy	bumerang	k1gInPc1
hlavně	hlavně	k6eAd1
náčiním	náčiní	k1gNnSc7
pro	pro	k7c4
zábavu	zábava	k1gFnSc4
a	a	k8xC
sport	sport	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sportovních	sportovní	k2eAgFnPc2d1
disciplín	disciplína	k1gFnPc2
pro	pro	k7c4
házení	házení	k1gNnSc6
bumerangem	bumerang	k1gInSc7
je	být	k5eAaImIp3nS
celá	celý	k2eAgFnSc1d1
řada	řada	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
výdrž	výdrž	k1gFnSc4
ve	v	k7c6
vzduchu	vzduch	k1gInSc6
<g/>
,	,	kIx,
</s>
<s>
triky	trik	k1gInPc1
při	při	k7c6
chytání	chytání	k1gNnSc6
<g/>
,	,	kIx,
</s>
<s>
rychlé	rychlý	k2eAgNnSc4d1
chytání	chytání	k1gNnSc4
<g/>
,	,	kIx,
</s>
<s>
žonglování	žonglování	k1gNnSc1
se	s	k7c7
dvěma	dva	k4xCgInPc7
bumerangy	bumerang	k1gInPc7
ad	ad	k7c4
<g/>
.	.	kIx.
</s>
<s>
Disciplíny	disciplína	k1gFnPc1
</s>
<s>
Aussie	Aussie	k1gFnSc1
Round	round	k1gInSc1
(	(	kIx(
<g/>
australské	australský	k2eAgNnSc1d1
kolo	kolo	k1gNnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bumerang	bumerang	k1gInSc1
má	mít	k5eAaImIp3nS
překonat	překonat	k5eAaPmF
vzdálenost	vzdálenost	k1gFnSc4
50	#num#	k4
metrů	metr	k1gInPc2
a	a	k8xC
být	být	k5eAaImF
chycen	chytit	k5eAaPmNgInS
v	v	k7c6
místě	místo	k1gNnSc6
odhodu	odhod	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Accuracy	Accuracy	k1gInPc1
=	=	kIx~
Přesnost	přesnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Body	bod	k1gInPc7
se	se	k3xPyFc4
udělují	udělovat	k5eAaImIp3nP
podle	podle	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
blízko	blízko	k6eAd1
bumerang	bumerang	k1gInSc1
přistane	přistat	k5eAaPmIp3nS
k	k	k7c3
místu	místo	k1gNnSc3
odhodu	odhod	k1gInSc2
(	(	kIx(
<g/>
nechytá	chytat	k5eNaImIp3nS
se	se	k3xPyFc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yInSc4,k3yQnSc4
letěl	letět	k5eAaImAgInS
min	min	kA
<g/>
.	.	kIx.
20	#num#	k4
m	m	kA
daleko	daleko	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Endurance	Endurance	k1gFnSc1
=	=	kIx~
Vytrvalost	vytrvalost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hodnotí	hodnotit	k5eAaImIp3nS
se	se	k3xPyFc4
počet	počet	k1gInSc1
chycení	chycení	k1gNnSc2
bumerangu	bumerang	k1gInSc2
ze	z	k7c2
vzduchu	vzduch	k1gInSc2
za	za	k7c4
dobu	doba	k1gFnSc4
pěti	pět	k4xCc2
minut	minuta	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bumerang	bumerang	k1gInSc1
musí	muset	k5eAaImIp3nS
pokaždé	pokaždé	k6eAd1
letět	letět	k5eAaImF
min	min	kA
<g/>
.	.	kIx.
20	#num#	k4
m	m	kA
daleko	daleko	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Fast	Fast	k2eAgInSc1d1
Catch	Catch	k1gInSc1
=	=	kIx~
Rychlé	Rychlé	k2eAgNnSc1d1
chytání	chytání	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hodnotí	hodnotit	k5eAaImIp3nS
se	se	k3xPyFc4
minimální	minimální	k2eAgInSc1d1
čas	čas	k1gInSc1
na	na	k7c4
pět	pět	k4xCc4
vyhození	vyhození	k1gNnPc2
a	a	k8xC
chycení	chycení	k1gNnSc2
bumerangu	bumerang	k1gInSc2
v	v	k7c6
řadě	řada	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bumerang	bumerang	k1gInSc1
musí	muset	k5eAaImIp3nS
pokaždé	pokaždé	k6eAd1
letět	letět	k5eAaImF
min	min	kA
<g/>
.	.	kIx.
20	#num#	k4
m	m	kA
daleko	daleko	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Trick	Trick	k1gMnSc1
Catch	Catch	k1gMnSc1
&	&	k?
Doubling	Doubling	k1gInSc1
=	=	kIx~
Trikové	trikový	k2eAgNnSc1d1
chytání	chytání	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Body	bod	k1gInPc7
se	se	k3xPyFc4
udělují	udělovat	k5eAaImIp3nP
za	za	k7c4
těžká	těžký	k2eAgNnPc4d1
chytání	chytání	k1gNnPc4
(	(	kIx(
<g/>
za	za	k7c4
zády	záda	k1gNnPc7
<g/>
,	,	kIx,
mezi	mezi	k7c7
nohama	noha	k1gFnPc7
<g/>
,	,	kIx,
vleže	vleže	k6eAd1
bez	bez	k7c2
pomoci	pomoc	k1gFnSc2
rukou	ruka	k1gFnPc2
<g/>
...	...	k?
<g/>
)	)	kIx)
Provádí	provádět	k5eAaImIp3nS
se	se	k3xPyFc4
nejprve	nejprve	k6eAd1
jedním	jeden	k4xCgInSc7
bumerangem	bumerang	k1gInSc7
(	(	kIx(
<g/>
trick	trick	k1gInSc1
catch	catch	k1gInSc1
<g/>
)	)	kIx)
a	a	k8xC
pak	pak	k6eAd1
dvěma	dva	k4xCgInPc7
bumerangy	bumerang	k1gInPc7
zároveň	zároveň	k6eAd1
(	(	kIx(
<g/>
doubling	doubling	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
LD	LD	kA
~	~	kIx~
Long	Long	k1gMnSc1
Distance	distance	k1gFnSc2
=	=	kIx~
Vzdálenost	vzdálenost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hodnotí	hodnotit	k5eAaImIp3nS
se	se	k3xPyFc4
maximální	maximální	k2eAgFnSc1d1
dosažená	dosažený	k2eAgFnSc1d1
vzdálenost	vzdálenost	k1gFnSc1
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
bumerang	bumerang	k1gInSc1
začne	začít	k5eAaPmIp3nS
vracet	vracet	k5eAaImF
<g/>
,	,	kIx,
při	při	k7c6
návratu	návrat	k1gInSc6
musí	muset	k5eAaImIp3nS
překonat	překonat	k5eAaPmF
čáru	čára	k1gFnSc4
odhodu	odhod	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
není	být	k5eNaImIp3nS
nutné	nutný	k2eAgNnSc1d1
ho	on	k3xPp3gMnSc4
chytit	chytit	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s>
MTA	mta	k0
~	~	kIx~
Maximum	maximum	k1gNnSc4
Time	Tim	k1gFnSc2
Aloft	Alofta	k1gFnPc2
=	=	kIx~
Maximální	maximální	k2eAgFnSc1d1
výdrž	výdrž	k1gFnSc1
ve	v	k7c6
vzduchu	vzduch	k1gInSc6
<g/>
.	.	kIx.
</s>
<s>
Juggling	Juggling	k1gInSc1
=	=	kIx~
Žonglování	žonglování	k1gNnSc1
se	s	k7c7
dvěma	dva	k4xCgInPc7
bumerangy	bumerang	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Vybrané	vybraný	k2eAgInPc4d1
světové	světový	k2eAgInPc4d1
rekordy	rekord	k1gInPc4
</s>
<s>
DisciplínaVýsledekJménoRok	DisciplínaVýsledekJménoRok	k1gInSc1
</s>
<s>
Vytrvalost	vytrvalost	k1gFnSc1
<g/>
81	#num#	k4
chyceníManuel	chyceníManuel	k1gMnSc1
Schütz	Schütz	k1gMnSc1
2006	#num#	k4
</s>
<s>
Rychlé	Rychlé	k2eAgNnPc4d1
chytání	chytání	k1gNnPc4
<g/>
14,60	14,60	k4
sChristian	sChristiany	k1gInPc2
Ruhf	Ruhf	k1gInSc1
1996	#num#	k4
</s>
<s>
Vzdálenost	vzdálenost	k1gFnSc1
<g/>
238	#num#	k4
mManuel	mManuel	k1gMnSc1
Schütz	Schütz	k1gMnSc1
1999	#num#	k4
</s>
<s>
Opakované	opakovaný	k2eAgNnSc1d1
chytání	chytání	k1gNnSc1
<g/>
1297	#num#	k4
chyceníManuel	chyceníManuet	k5eAaPmAgMnS,k5eAaImAgMnS,k5eAaBmAgMnS
Schütz	Schütz	k1gMnSc1
2005	#num#	k4
</s>
<s>
Pozn	pozn	kA
<g/>
.	.	kIx.
<g/>
:	:	kIx,
Rekordy	rekord	k1gInPc4
platné	platný	k2eAgInPc4d1
k	k	k7c3
červnu	červen	k1gInSc3
2007	#num#	k4
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Boomerang	Boomeranga	k1gFnPc2
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Boomerang	Boomerang	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cultur	Cultura	k1gFnPc2
Quest	Quest	k1gFnSc4
on	on	k3xPp3gMnSc1
Aboriginal	Aboriginal	k1gMnSc1
Tools	Toolsa	k1gFnPc2
and	and	k?
Culture	Cultur	k1gMnSc5
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
HARRIS	HARRIS	kA
<g/>
,	,	kIx,
Tom	Tom	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
How	How	k1gFnSc1
Boomerangs	Boomerangsa	k1gFnPc2
Work	Worka	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
HowStuffWorks	HowStuffWorks	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
What	What	k1gInSc1
is	is	k?
a	a	k8xC
boomerang	boomerang	k1gInSc1
and	and	k?
where	wher	k1gInSc5
does	does	k1gInSc1
it	it	k?
come	come	k6eAd1
from	from	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bumerang-Sport	Bumerang-Sport	k1gInSc1
<g/>
.	.	kIx.
<g/>
de	de	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Traditional	Traditional	k1gMnSc1
Aboriginal	Aboriginal	k1gMnSc1
Music	Music	k1gMnSc1
:	:	kIx,
Aboriginal	Aboriginal	k1gFnSc1
Musical	musical	k1gInSc1
Instruments	Instruments	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alice	Alice	k1gFnSc1
Springs	Springsa	k1gFnPc2
<g/>
:	:	kIx,
Aboriginal	Aboriginal	k1gMnSc5
Art	Art	k1gMnSc5
Culture	Cultur	k1gMnSc5
and	and	k?
Tourism	Touris	k1gNnSc7
Authority	Authorita	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
mapkou	mapka	k1gFnSc7
rozšíření	rozšíření	k1gNnSc2
původních	původní	k2eAgInPc2d1
australských	australský	k2eAgInPc2d1
hudebních	hudební	k2eAgInPc2d1
nástrojů	nástroj	k1gInPc2
včetně	včetně	k7c2
bumerangu	bumerang	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
BUTZ	BUTZ	kA
<g/>
,	,	kIx,
Tony	Tony	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Article	Article	k1gInSc1
-	-	kIx~
What	What	k1gInSc1
is	is	k?
a	a	k8xC
Boomerang	Boomerang	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
:	:	kIx,
An	An	k1gFnSc1
investigation	investigation	k1gInSc1
of	of	k?
the	the	k?
word	word	k1gMnSc1
boomerang	boomerang	k1gMnSc1
in	in	k?
Aboriginal	Aboriginal	k1gMnSc1
and	and	k?
English	English	k1gMnSc1
languages	languages	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Boomerang	Boomerang	k1gInSc1
Association	Association	k1gInSc4
of	of	k?
Australia	Australius	k1gMnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ATKINSON	ATKINSON	kA
<g/>
,	,	kIx,
Nancy	Nancy	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Does	Doesa	k1gFnPc2
a	a	k8xC
Boomerang	Boomeranga	k1gFnPc2
Work	Work	k1gMnSc1
in	in	k?
Space	Space	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Universe	Universe	k1gFnSc1
Today	Todaa	k1gFnSc2
<g/>
,	,	kIx,
2008-03-24	2008-03-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
nezamýšlené	zamýšlený	k2eNgInPc4d1
důsledky	důsledek	k1gInPc4
-	-	kIx~
bumerangový	bumerangový	k2eAgInSc4d1
efekt	efekt	k1gInSc4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Galerie	galerie	k1gFnSc1
bumerang	bumerang	k1gInSc4
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
SCHROTH	SCHROTH	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ne	ne	k9
všechny	všechen	k3xTgInPc1
bumerangy	bumerang	k1gInPc1
se	se	k3xPyFc4
vracejí	vracet	k5eAaImIp3nP
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4008880-7	4008880-7	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85015769	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85015769	#num#	k4
</s>
