<s>
Sýr	sýr	k1gInSc1	sýr
Niva	niva	k1gFnSc1	niva
je	být	k5eAaImIp3nS	být
sýr	sýr	k1gInSc1	sýr
s	s	k7c7	s
modrou	modrý	k2eAgFnSc7d1	modrá
plísní	plíseň	k1gFnSc7	plíseň
uvnitř	uvnitř	k7c2	uvnitř
hmoty	hmota	k1gFnSc2	hmota
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
kravský	kravský	k2eAgInSc1d1	kravský
sýr	sýr	k1gInSc1	sýr
je	být	k5eAaImIp3nS	být
napodobeninou	napodobenina	k1gFnSc7	napodobenina
francouzského	francouzský	k2eAgInSc2d1	francouzský
ovčího	ovčí	k2eAgInSc2d1	ovčí
sýra	sýr	k1gInSc2	sýr
Roquefortu	Roquefort	k1gInSc2	Roquefort
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
52	[number]	k4	52
%	%	kIx~	%
sušiny	sušina	k1gFnSc2	sušina
<g/>
,	,	kIx,	,
50	[number]	k4	50
%	%	kIx~	%
tuku	tuk	k1gInSc2	tuk
v	v	k7c6	v
sušině	sušina	k1gFnSc6	sušina
<g/>
,	,	kIx,	,
5	[number]	k4	5
%	%	kIx~	%
kuchyňské	kuchyňský	k2eAgFnSc2d1	kuchyňská
soli	sůl	k1gFnSc2	sůl
<g/>
,	,	kIx,	,
1,5	[number]	k4	1,5
%	%	kIx~	%
cukru	cukr	k1gInSc2	cukr
a	a	k8xC	a
1500	[number]	k4	1500
kJ	kJ	k?	kJ
využitelné	využitelný	k2eAgFnSc2d1	využitelná
energie	energie	k1gFnSc2	energie
na	na	k7c4	na
100	[number]	k4	100
gramů	gram	k1gInPc2	gram
sýra	sýr	k1gInSc2	sýr
<g/>
.	.	kIx.	.
</s>
<s>
Výslovnost	výslovnost	k1gFnSc1	výslovnost
slova	slovo	k1gNnSc2	slovo
niva	niva	k1gFnSc1	niva
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
pravopisu	pravopis	k1gInSc2	pravopis
a	a	k8xC	a
slovo	slovo	k1gNnSc1	slovo
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
měkce	měkko	k6eAd1	měkko
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
čtyřicátých	čtyřicátý	k4xOgNnPc6	čtyřicátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
název	název	k1gInSc1	název
Roquefort	Roquefort	k1gInSc1	Roquefort
stal	stát	k5eAaPmAgInS	stát
chráněným	chráněný	k2eAgInSc7d1	chráněný
názvem	název	k1gInSc7	název
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
tento	tento	k3xDgInSc1	tento
podobný	podobný	k2eAgInSc1d1	podobný
sýr	sýr	k1gInSc1	sýr
musel	muset	k5eAaImAgInS	muset
dostat	dostat	k5eAaPmF	dostat
jiné	jiný	k2eAgNnSc4d1	jiné
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Technologa	technolog	k1gMnSc4	technolog
<g/>
,	,	kIx,	,
ruského	ruský	k2eAgMnSc4d1	ruský
(	(	kIx(	(
<g/>
donského	donský	k2eAgMnSc2d1	donský
<g/>
)	)	kIx)	)
kozáka	kozák	k1gMnSc2	kozák
<g/>
,	,	kIx,	,
Charitonova	Charitonův	k2eAgMnSc2d1	Charitonův
z	z	k7c2	z
mlékárny	mlékárna	k1gFnSc2	mlékárna
v	v	k7c6	v
Českém	český	k2eAgInSc6d1	český
Krumlově	Krumlov	k1gInSc6	Krumlov
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
JIHOČESKÉ	jihočeský	k2eAgFnSc2d1	Jihočeská
MLÉKÁRNY	mlékárna	k1gFnSc2	mlékárna
(	(	kIx(	(
<g/>
JČM	JČM	kA	JČM
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Madeta	Madet	k2eAgFnSc1d1	Madeta
<g/>
)	)	kIx)	)
napadla	napadnout	k5eAaPmAgFnS	napadnout
niva	niva	k1gFnSc1	niva
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
modrozelená	modrozelený	k2eAgFnSc1d1	modrozelená
barva	barva	k1gFnSc1	barva
sýru	sýr	k1gInSc2	sýr
mu	on	k3xPp3gMnSc3	on
připomínala	připomínat	k5eAaImAgFnS	připomínat
úrodnou	úrodný	k2eAgFnSc4d1	úrodná
louku	louka	k1gFnSc4	louka
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Niva	niva	k1gFnSc1	niva
je	být	k5eAaImIp3nS	být
i	i	k9	i
název	název	k1gInSc1	název
obce	obec	k1gFnSc2	obec
sousedící	sousedící	k2eAgFnSc2d1	sousedící
s	s	k7c7	s
lokalitou	lokalita	k1gFnSc7	lokalita
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
tradičních	tradiční	k2eAgMnPc2d1	tradiční
výrobců	výrobce	k1gMnPc2	výrobce
-	-	kIx~	-
Mlékárnou	mlékárna	k1gFnSc7	mlékárna
v	v	k7c6	v
Otinovsi	Otinovse	k1gFnSc6	Otinovse
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
zkoušel	zkoušet	k5eAaImAgMnS	zkoušet
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
sýr	sýr	k1gInSc1	sýr
by	by	kYmCp3nP	by
byl	být	k5eAaImAgInS	být
vůbec	vůbec	k9	vůbec
nejvhodnější	vhodný	k2eAgNnSc4d3	nejvhodnější
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
v	v	k7c6	v
českokrumlovské	českokrumlovský	k2eAgFnSc6d1	českokrumlovská
pobočce	pobočka	k1gFnSc6	pobočka
<g/>
.	.	kIx.	.
</s>
<s>
Zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejvhodnější	vhodný	k2eAgMnSc1d3	nejvhodnější
bude	být	k5eAaImBp3nS	být
vyrábět	vyrábět	k5eAaImF	vyrábět
sýr	sýr	k1gInSc4	sýr
s	s	k7c7	s
modrozelenou	modrozelený	k2eAgFnSc7d1	modrozelená
plísní	plíseň	k1gFnSc7	plíseň
uvnitř	uvnitř	k6eAd1	uvnitř
obecně	obecně	k6eAd1	obecně
známý	známý	k2eAgMnSc1d1	známý
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
rokfór	rokfór	k1gInSc1	rokfór
<g/>
,	,	kIx,	,
Roquefort	Roquefort	k1gInSc1	Roquefort
<g/>
.	.	kIx.	.
</s>
<s>
Nivu	niva	k1gFnSc4	niva
jako	jako	k8xC	jako
zavedený	zavedený	k2eAgInSc4d1	zavedený
název	název	k1gInSc4	název
používají	používat	k5eAaImIp3nP	používat
všichni	všechen	k3xTgMnPc1	všechen
stávající	stávající	k2eAgMnPc1d1	stávající
výrobci	výrobce	k1gMnPc1	výrobce
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Pouze	pouze	k6eAd1	pouze
dvě	dva	k4xCgFnPc1	dva
značky	značka	k1gFnPc1	značka
<g/>
,	,	kIx,	,
Jihočeská	jihočeský	k2eAgFnSc1d1	Jihočeská
niva	niva	k1gFnSc1	niva
a	a	k8xC	a
Jihočeská	jihočeský	k2eAgFnSc1d1	Jihočeská
zlatá	zlatý	k2eAgFnSc1d1	zlatá
niva	niva	k1gFnSc1	niva
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
ochranné	ochranný	k2eAgNnSc4d1	ochranné
označení	označení	k1gNnSc4	označení
Evropské	evropský	k2eAgFnSc2d1	Evropská
Unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
veden	vést	k5eAaImNgInS	vést
spor	spor	k1gInSc1	spor
o	o	k7c4	o
užívání	užívání	k1gNnSc4	užívání
tohoto	tento	k3xDgInSc2	tento
názvu	název	k1gInSc2	název
se	s	k7c7	s
Slovenskem	Slovensko	k1gNnSc7	Slovensko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
spor	spor	k1gInSc4	spor
prohrálo	prohrát	k5eAaPmAgNnS	prohrát
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
spor	spor	k1gInSc1	spor
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
výhra	výhra	k1gFnSc1	výhra
vlastně	vlastně	k9	vlastně
dokazují	dokazovat	k5eAaImIp3nP	dokazovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pojmenování	pojmenování	k1gNnSc1	pojmenování
sýru	sýr	k1gInSc2	sýr
Niva	niva	k1gFnSc1	niva
<g/>
/	/	kIx~	/
<g/>
niva	niva	k1gFnSc1	niva
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
Slovensku	Slovensko	k1gNnSc6	Slovensko
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
předchozí	předchozí	k2eAgFnSc3d1	předchozí
historii	historie	k1gFnSc3	historie
ve	v	k7c6	v
společném	společný	k2eAgInSc6d1	společný
státě	stát	k1gInSc6	stát
a	a	k8xC	a
trhu	trh	k1gInSc6	trh
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
státech	stát	k1gInPc6	stát
EU	EU	kA	EU
není	být	k5eNaImIp3nS	být
vůbec	vůbec	k9	vůbec
tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
znám	znám	k2eAgInSc1d1	znám
<g/>
.	.	kIx.	.
</s>
<s>
Sýr	sýr	k1gInSc1	sýr
má	mít	k5eAaImIp3nS	mít
barvu	barva	k1gFnSc4	barva
smetanovou	smetanový	k2eAgFnSc4d1	smetanová
až	až	k9	až
sýrově	sýrově	k6eAd1	sýrově
žlutou	žlutý	k2eAgFnSc7d1	žlutá
<g/>
.	.	kIx.	.
</s>
<s>
Povrch	povrch	k1gInSc1	povrch
je	být	k5eAaImIp3nS	být
světle	světle	k6eAd1	světle
hnědý	hnědý	k2eAgInSc1d1	hnědý
a	a	k8xC	a
celistvý	celistvý	k2eAgInSc1d1	celistvý
se	s	k7c7	s
zřetelnými	zřetelný	k2eAgInPc7d1	zřetelný
vpichy	vpich	k1gInPc7	vpich
a	a	k8xC	a
na	na	k7c6	na
řezu	řez	k1gInSc6	řez
s	s	k7c7	s
mramorovitým	mramorovitý	k2eAgNnSc7d1	mramorovitý
prorostem	prorostem	k?	prorostem
světlé	světlý	k2eAgInPc1d1	světlý
až	až	k6eAd1	až
tmavě	tmavě	k6eAd1	tmavě
zelené	zelený	k2eAgFnPc1d1	zelená
ušlechtilé	ušlechtilý	k2eAgFnPc1d1	ušlechtilá
plísně	plíseň	k1gFnPc1	plíseň
<g/>
.	.	kIx.	.
</s>
<s>
Barva	barva	k1gFnSc1	barva
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
je	být	k5eAaImIp3nS	být
znakem	znak	k1gInSc7	znak
vyzrálosti	vyzrálost	k1gFnSc2	vyzrálost
sýrů	sýr	k1gInPc2	sýr
a	a	k8xC	a
neovlivňuje	ovlivňovat	k5eNaImIp3nS	ovlivňovat
jejich	jejich	k3xOp3gFnSc4	jejich
zdravotní	zdravotní	k2eAgFnSc4d1	zdravotní
nezávadnost	nezávadnost	k1gFnSc4	nezávadnost
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
prorost	prorůst	k5eAaPmDgInS	prorůst
plísní	plíseň	k1gFnPc2	plíseň
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
i	i	k9	i
mírně	mírně	k6eAd1	mírně
drobivý	drobivý	k2eAgInSc1d1	drobivý
nebo	nebo	k8xC	nebo
roztíratelný	roztíratelný	k2eAgInSc1d1	roztíratelný
podle	podle	k7c2	podle
stupně	stupeň	k1gInSc2	stupeň
zralosti	zralost	k1gFnSc2	zralost
sýra	sýr	k1gInSc2	sýr
<g/>
.	.	kIx.	.
</s>
<s>
Konzistence	konzistence	k1gFnSc1	konzistence
sýru	sýr	k1gInSc2	sýr
je	být	k5eAaImIp3nS	být
poloměkká	poloměkký	k2eAgFnSc1d1	poloměkká
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
trvanlivosti	trvanlivost	k1gFnSc2	trvanlivost
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
obalem	obal	k1gInSc7	obal
může	moct	k5eAaImIp3nS	moct
objevit	objevit	k5eAaPmF	objevit
slabě	slabě	k6eAd1	slabě
oranžový	oranžový	k2eAgInSc4d1	oranžový
maz	maz	k1gInSc4	maz
nebo	nebo	k8xC	nebo
prorost	prorůst	k5eAaPmDgInS	prorůst
plísně	plíseň	k1gFnPc4	plíseň
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
je	být	k5eAaImIp3nS	být
přirozený	přirozený	k2eAgInSc1d1	přirozený
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
závadný	závadný	k2eAgMnSc1d1	závadný
<g/>
.	.	kIx.	.
</s>
<s>
Chuť	chuť	k1gFnSc1	chuť
sýru	sýr	k1gInSc2	sýr
je	být	k5eAaImIp3nS	být
slaná	slaný	k2eAgFnSc1d1	slaná
<g/>
,	,	kIx,	,
výrazně	výrazně	k6eAd1	výrazně
pikantní	pikantní	k2eAgMnSc1d1	pikantní
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
příchuť	příchuť	k1gFnSc4	příchuť
po	po	k7c6	po
ušlechtilé	ušlechtilý	k2eAgFnSc6d1	ušlechtilá
plísni	plíseň	k1gFnSc6	plíseň
<g/>
.	.	kIx.	.
</s>
<s>
Sýr	sýr	k1gInSc1	sýr
se	se	k3xPyFc4	se
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
surovin	surovina	k1gFnPc2	surovina
<g/>
:	:	kIx,	:
syrové	syrový	k2eAgNnSc4d1	syrové
kravské	kravský	k2eAgNnSc4d1	kravské
mléko	mléko	k1gNnSc4	mléko
jedlá	jedlý	k2eAgFnSc1d1	jedlá
sůl	sůl	k1gFnSc1	sůl
–	–	k?	–
NaCl	NaCl	k1gInSc1	NaCl
chlorid	chlorid	k1gInSc1	chlorid
vápenatý	vápenatý	k2eAgInSc1d1	vápenatý
–	–	k?	–
CaCl	CaCl	k1gInSc1	CaCl
<g/>
2	[number]	k4	2
kultura	kultura	k1gFnSc1	kultura
smetanová	smetanový	k2eAgFnSc1d1	smetanová
(	(	kIx(	(
<g/>
smetanový	smetanový	k2eAgInSc1d1	smetanový
zákys	zákys	k1gInSc1	zákys
<g/>
)	)	kIx)	)
kultura	kultura	k1gFnSc1	kultura
Penicillium	Penicillium	k1gNnSc1	Penicillium
roqueforti	roquefort	k1gMnPc1	roquefort
syřidlo	syřidlo	k1gNnSc4	syřidlo
Výrobní	výrobní	k2eAgInSc1d1	výrobní
proces	proces	k1gInSc1	proces
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
pod	pod	k7c7	pod
stálým	stálý	k2eAgInSc7d1	stálý
hygienickým	hygienický	k2eAgInSc7d1	hygienický
a	a	k8xC	a
veterinárním	veterinární	k2eAgInSc7d1	veterinární
dohledem	dohled	k1gInSc7	dohled
a	a	k8xC	a
uvedené	uvedený	k2eAgFnPc1d1	uvedená
suroviny	surovina	k1gFnPc1	surovina
musí	muset	k5eAaImIp3nP	muset
odpovídat	odpovídat	k5eAaImF	odpovídat
platným	platný	k2eAgInSc7d1	platný
hygienickým	hygienický	k2eAgInSc7d1	hygienický
<g/>
,	,	kIx,	,
zdravotním	zdravotní	k2eAgInSc7d1	zdravotní
a	a	k8xC	a
veterinárním	veterinární	k2eAgInPc3d1	veterinární
zákonům	zákon	k1gInPc3	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Výrobní	výrobní	k2eAgInSc1d1	výrobní
proces	proces	k1gInSc1	proces
<g/>
:	:	kIx,	:
Odstředění	odstředění	k1gNnSc1	odstředění
syrového	syrový	k2eAgNnSc2d1	syrové
plnotučného	plnotučný	k2eAgNnSc2d1	plnotučné
kravského	kravský	k2eAgNnSc2d1	kravské
mléka	mléko	k1gNnSc2	mléko
<g/>
.	.	kIx.	.
</s>
<s>
Homogenizace	homogenizace	k1gFnSc1	homogenizace
<g/>
.	.	kIx.	.
</s>
<s>
Pasterizace	pasterizace	k1gFnSc1	pasterizace
<g/>
.	.	kIx.	.
</s>
<s>
Sýření	sýření	k1gNnSc1	sýření
probíhá	probíhat	k5eAaImIp3nS	probíhat
ve	v	k7c6	v
výrobníku	výrobník	k1gInSc6	výrobník
<g/>
.	.	kIx.	.
</s>
<s>
Sýřenina	sýřenina	k1gFnSc1	sýřenina
se	se	k3xPyFc4	se
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
na	na	k7c4	na
zrno	zrno	k1gNnSc4	zrno
–	–	k?	–
základní	základní	k2eAgFnSc4d1	základní
surovinu	surovina	k1gFnSc4	surovina
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
sýra	sýr	k1gInSc2	sýr
<g/>
.	.	kIx.	.
</s>
<s>
Vzniklá	vzniklý	k2eAgFnSc1d1	vzniklá
směs	směs	k1gFnSc1	směs
syrovátky	syrovátka	k1gFnSc2	syrovátka
a	a	k8xC	a
zrna	zrno	k1gNnSc2	zrno
se	se	k3xPyFc4	se
pouští	pouštět	k5eAaImIp3nS	pouštět
na	na	k7c4	na
pás	pás	k1gInSc4	pás
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
syrovátka	syrovátka	k1gFnSc1	syrovátka
odtéká	odtékat	k5eAaImIp3nS	odtékat
<g/>
.	.	kIx.	.
</s>
<s>
Solení	solení	k1gNnSc1	solení
zrna	zrno	k1gNnSc2	zrno
koncentrovaným	koncentrovaný	k2eAgMnSc7d1	koncentrovaný
solným	solný	k2eAgInSc7d1	solný
roztokem	roztok	k1gInSc7	roztok
a	a	k8xC	a
plnění	plnění	k1gNnSc1	plnění
do	do	k7c2	do
forem	forma	k1gFnPc2	forma
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
pravidelných	pravidelný	k2eAgInPc6d1	pravidelný
intervalech	interval	k1gInPc6	interval
obracejí	obracet	k5eAaImIp3nP	obracet
<g/>
.	.	kIx.	.
</s>
<s>
Hmota	hmota	k1gFnSc1	hmota
je	být	k5eAaImIp3nS	být
poté	poté	k6eAd1	poté
vyklápěna	vyklápět	k5eAaImNgFnS	vyklápět
na	na	k7c4	na
nerezové	rezový	k2eNgFnPc4d1	nerezová
palety	paleta	k1gFnPc4	paleta
a	a	k8xC	a
putuje	putovat	k5eAaImIp3nS	putovat
do	do	k7c2	do
solných	solný	k2eAgFnPc2d1	solná
lázní	lázeň	k1gFnPc2	lázeň
<g/>
.	.	kIx.	.
</s>
<s>
Obrovské	obrovský	k2eAgFnPc1d1	obrovská
kádě	káď	k1gFnPc1	káď
jsou	být	k5eAaImIp3nP	být
napouštěny	napouštěn	k2eAgMnPc4d1	napouštěn
solným	solný	k2eAgInSc7d1	solný
roztokem	roztok	k1gInSc7	roztok
s	s	k7c7	s
danou	daný	k2eAgFnSc7d1	daná
koncentrací	koncentrace	k1gFnSc7	koncentrace
<g/>
,	,	kIx,	,
kyselostí	kyselost	k1gFnSc7	kyselost
a	a	k8xC	a
teplotou	teplota	k1gFnSc7	teplota
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vyjmutí	vyjmutí	k1gNnSc6	vyjmutí
se	se	k3xPyFc4	se
bochníky	bochník	k1gInPc1	bochník
nechají	nechat	k5eAaPmIp3nP	nechat
den	den	k1gInSc4	den
okapat	okapat	k5eAaPmF	okapat
a	a	k8xC	a
ručně	ručně	k6eAd1	ručně
se	se	k3xPyFc4	se
solí	solit	k5eAaImIp3nS	solit
<g/>
.	.	kIx.	.
</s>
<s>
Propichování	propichování	k1gNnSc1	propichování
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
se	se	k3xPyFc4	se
vytvořily	vytvořit	k5eAaPmAgInP	vytvořit
průduchy	průduch	k1gInPc1	průduch
nezbytné	nezbytný	k2eAgInPc1d1	nezbytný
pro	pro	k7c4	pro
růst	růst	k1gInSc4	růst
plísně	plíseň	k1gFnSc2	plíseň
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
udělat	udělat	k5eAaPmF	udělat
asi	asi	k9	asi
35	[number]	k4	35
vpichů	vpich	k1gInPc2	vpich
z	z	k7c2	z
každé	každý	k3xTgFnSc2	každý
strany	strana	k1gFnSc2	strana
bochníku	bochník	k1gInSc2	bochník
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
dostatek	dostatek	k1gInSc4	dostatek
vzduchových	vzduchový	k2eAgFnPc2d1	vzduchová
bublin	bublina	k1gFnPc2	bublina
s	s	k7c7	s
dvojím	dvojí	k4xRgInSc7	dvojí
významem	význam	k1gInSc7	význam
–	–	k?	–
umožnit	umožnit	k5eAaPmF	umožnit
přístup	přístup	k1gInSc4	přístup
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
plíseň	plíseň	k1gFnSc4	plíseň
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
nechat	nechat	k5eAaPmF	nechat
zplodiny	zplodina	k1gFnPc4	zplodina
vznikající	vznikající	k2eAgFnPc4d1	vznikající
štěpením	štěpení	k1gNnSc7	štěpení
bílkovin	bílkovina	k1gFnPc2	bílkovina
a	a	k8xC	a
tuků	tuk	k1gInPc2	tuk
ze	z	k7c2	z
sýra	sýr	k1gInSc2	sýr
uniknout	uniknout	k5eAaPmF	uniknout
<g/>
.	.	kIx.	.
</s>
<s>
Plísňové	plísňový	k2eAgFnPc1d1	plísňová
kultury	kultura	k1gFnPc1	kultura
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
vysterilizovaných	vysterilizovaný	k2eAgFnPc2d1	vysterilizovaná
krup	kroupa	k1gFnPc2	kroupa
a	a	k8xC	a
do	do	k7c2	do
sýrové	sýrový	k2eAgFnSc2d1	sýrová
hmoty	hmota	k1gFnSc2	hmota
se	se	k3xPyFc4	se
splachují	splachovat	k5eAaImIp3nP	splachovat
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Zrání	zrání	k1gNnSc1	zrání
ve	v	k7c6	v
sklepích	sklep	k1gInPc6	sklep
při	při	k7c6	při
stálé	stálý	k2eAgFnSc6d1	stálá
teplotě	teplota	k1gFnSc6	teplota
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
a	a	k8xC	a
vlhkosti	vlhkost	k1gFnSc2	vlhkost
(	(	kIx(	(
<g/>
95	[number]	k4	95
%	%	kIx~	%
<g/>
)	)	kIx)	)
probíhá	probíhat	k5eAaImIp3nS	probíhat
5	[number]	k4	5
týdnů	týden	k1gInPc2	týden
<g/>
.	.	kIx.	.
</s>
<s>
Ošetření	ošetření	k1gNnSc1	ošetření
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
z	z	k7c2	z
bochníků	bochník	k1gInPc2	bochník
ručně	ručně	k6eAd1	ručně
seškrabují	seškrabovat	k5eAaImIp3nP	seškrabovat
nežádoucí	žádoucí	k2eNgFnPc4d1	nežádoucí
plísně	plíseň	k1gFnPc4	plíseň
<g/>
.	.	kIx.	.
</s>
<s>
Balení	balení	k1gNnSc1	balení
<g/>
.	.	kIx.	.
</s>
<s>
Skladování	skladování	k1gNnSc1	skladování
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
nivu	niva	k1gFnSc4	niva
<g/>
:	:	kIx,	:
MADETA	MADETA	kA	MADETA
a.	a.	k?	a.
s.	s.	k?	s.
(	(	kIx(	(
<g/>
výrobní	výrobní	k2eAgInSc1d1	výrobní
závod	závod	k1gInSc1	závod
Český	český	k2eAgInSc1d1	český
Krumlov	Krumlov	k1gInSc1	Krumlov
<g/>
)	)	kIx)	)
N	N	kA	N
I	i	k9	i
V	v	k7c6	v
A	A	kA	A
s.	s.	k?	s.
<g/>
r.	r.	kA	r.
<g/>
o.	o.	k?	o.
<g/>
,	,	kIx,	,
Dolní	dolní	k2eAgMnSc1d1	dolní
Přím	přít	k5eAaImIp1nS	přít
2	[number]	k4	2
Mlékárna	mlékárna	k1gFnSc1	mlékárna
Otinoves	Otinovesa	k1gFnPc2	Otinovesa
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
(	(	kIx(	(
<g/>
Obec	obec	k1gFnSc1	obec
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Niva	niva	k1gFnSc1	niva
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
jen	jen	k9	jen
1	[number]	k4	1
km	km	kA	km
od	od	k7c2	od
sídla	sídlo	k1gNnSc2	sídlo
tradičního	tradiční	k2eAgMnSc2d1	tradiční
výrobce	výrobce	k1gMnSc2	výrobce
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
dozrávání	dozrávání	k1gNnSc3	dozrávání
sýru	sýr	k1gInSc2	sýr
Niva	niva	k1gFnSc1	niva
používána	používán	k2eAgFnSc1d1	používána
jeskyně	jeskyně	k1gFnSc1	jeskyně
Michálka	Michálek	k1gMnSc2	Michálek
v	v	k7c6	v
Moravském	moravský	k2eAgInSc6d1	moravský
krasu	kras	k1gInSc6	kras
)	)	kIx)	)
Niva	niva	k1gFnSc1	niva
–	–	k?	–
válec	válec	k1gInSc1	válec
váží	vážit	k5eAaImIp3nS	vážit
průměrně	průměrně	k6eAd1	průměrně
2	[number]	k4	2
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Bývá	bývat	k5eAaImIp3nS	bývat
zabalen	zabalit	k5eAaPmNgMnS	zabalit
do	do	k7c2	do
aluminiové	aluminiový	k2eAgFnSc2d1	aluminiová
folie	folie	k1gFnSc2	folie
<g/>
.	.	kIx.	.
</s>
<s>
Niva	niva	k1gFnSc1	niva
–	–	k?	–
porce	porce	k1gFnSc1	porce
(	(	kIx(	(
<g/>
trojúhelníková	trojúhelníkový	k2eAgFnSc1d1	trojúhelníková
výseč	výseč	k1gFnSc1	výseč
<g/>
)	)	kIx)	)
váží	vážit	k5eAaImIp3nS	vážit
kolem	kolem	k7c2	kolem
100	[number]	k4	100
g	g	kA	g
(	(	kIx(	(
<g/>
kalibrovaná	kalibrovaný	k2eAgFnSc1d1	kalibrovaná
váha	váha	k1gFnSc1	váha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Niva	niva	k1gFnSc1	niva
–	–	k?	–
zlomky	zlomek	k1gInPc1	zlomek
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
požadavky	požadavek	k1gInPc4	požadavek
dalšího	další	k2eAgNnSc2d1	další
zpracování	zpracování	k1gNnSc2	zpracování
je	být	k5eAaImIp3nS	být
sýr	sýr	k1gInSc1	sýr
balen	balen	k2eAgInSc1d1	balen
do	do	k7c2	do
zdravotně	zdravotně	k6eAd1	zdravotně
nezávadných	závadný	k2eNgInPc2d1	nezávadný
pytlů	pytel	k1gInPc2	pytel
z	z	k7c2	z
polyethylenu	polyethylen	k1gInSc2	polyethylen
<g/>
.	.	kIx.	.
</s>
<s>
Sýr	sýr	k1gInSc1	sýr
ve	v	k7c6	v
stáří	stáří	k1gNnSc6	stáří
<g/>
:	:	kIx,	:
21	[number]	k4	21
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
dnů	den	k1gInPc2	den
–	–	k?	–
je	být	k5eAaImIp3nS	být
pevnější	pevný	k2eAgFnPc4d2	pevnější
tvarohovité	tvarohovitý	k2eAgFnPc4d1	tvarohovitá
konzistence	konzistence	k1gFnPc4	konzistence
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
vhodný	vhodný	k2eAgInSc4d1	vhodný
například	například	k6eAd1	například
k	k	k7c3	k
obalování	obalování	k1gNnSc3	obalování
či	či	k8xC	či
výrobě	výroba	k1gFnSc3	výroba
pomazánek	pomazánka	k1gFnPc2	pomazánka
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
–	–	k?	–
<g/>
55	[number]	k4	55
dnů	den	k1gInPc2	den
–	–	k?	–
s	s	k7c7	s
měkčí	měkčit	k5eAaImIp3nS	měkčit
prozrálou	prozrálý	k2eAgFnSc4d1	prozrálý
konsistenci	konsistence	k1gFnSc4	konsistence
a	a	k8xC	a
s	s	k7c7	s
pikantní	pikantní	k2eAgFnSc7d1	pikantní
výraznou	výrazný	k2eAgFnSc7d1	výrazná
chutí	chuť	k1gFnSc7	chuť
je	být	k5eAaImIp3nS	být
vhodný	vhodný	k2eAgInSc1d1	vhodný
k	k	k7c3	k
vínu	víno	k1gNnSc3	víno
<g/>
.	.	kIx.	.
</s>
<s>
Minimální	minimální	k2eAgFnSc1d1	minimální
trvanlivost	trvanlivost	k1gFnSc1	trvanlivost
sýra	sýr	k1gInSc2	sýr
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
21	[number]	k4	21
<g/>
–	–	k?	–
<g/>
24	[number]	k4	24
dní	den	k1gInPc2	den
od	od	k7c2	od
data	datum	k1gNnSc2	datum
balení	balení	k1gNnSc2	balení
při	při	k7c6	při
skladování	skladování	k1gNnSc6	skladování
při	při	k7c6	při
vhodné	vhodný	k2eAgFnSc6d1	vhodná
teplotě	teplota	k1gFnSc6	teplota
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
8	[number]	k4	8
°	°	k?	°
<g/>
C.	C.	kA	C.
</s>
