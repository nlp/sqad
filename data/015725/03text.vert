<s>
Life	Life	k6eAd1
thru	thru	k5eAaPmIp1nS
a	a	k8xC
Lens	Lens	k1gInSc1
</s>
<s>
Life	Life	k6eAd1
thru	thru	k6eAd1
a	a	k8xC
LensInterpretRobbie	LensInterpretRobbie	k1gFnSc1
WilliamsDruh	WilliamsDruha	k1gFnPc2
albastudiové	albastudiový	k2eAgNnSc4d1
albumVydáno	albumVydán	k2eAgNnSc4d1
<g/>
29	#num#	k4
<g/>
.	.	kIx.
září	zářit	k5eAaImIp3nS
1997	#num#	k4
<g/>
Nahráno	nahrát	k5eAaPmNgNnS,k5eAaBmNgNnS
<g/>
1997	#num#	k4
<g/>
Žánrybritpop	Žánrybritpop	k1gInSc1
<g/>
,	,	kIx,
alternativní	alternativní	k2eAgFnSc1d1
rockDélka	rockDélka	k1gFnSc1
<g/>
44	#num#	k4
<g/>
:	:	kIx,
<g/>
25	#num#	k4
<g/>
JazykangličtinaVydavatelstvíEMI	JazykangličtinaVydavatelstvíEMI	k1gFnSc7
RecordsProducentiGuy	RecordsProducentiGua	k1gFnSc2
Chambers	Chambers	k1gInSc1
<g/>
,	,	kIx,
Steve	Steve	k1gMnSc1
PowerProfesionální	PowerProfesionální	k2eAgFnSc2d1
kritika	kritik	k1gMnSc2
</s>
<s>
Allmusic	Allmusic	k1gMnSc1
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Robbie	Robbie	k1gFnSc1
Williams	Williamsa	k1gFnPc2
chronologicky	chronologicky	k6eAd1
</s>
<s>
Life	Life	k6eAd1
thru	thru	k5eAaPmIp1nS
a	a	k8xC
Lens	Lens	k1gInSc1
<g/>
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
I	i	k9
<g/>
'	'	kIx"
<g/>
ve	v	k7c4
Been	Been	k1gInSc4
Expecting	Expecting	k1gInSc1
You	You	k1gFnSc1
<g/>
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Life	Life	k6eAd1
thru	thru	k5eAaPmIp1nS
a	a	k8xC
Lens	Lens	k1gInSc1
je	být	k5eAaImIp3nS
debutové	debutový	k2eAgNnSc4d1
album	album	k1gNnSc4
anglického	anglický	k2eAgMnSc2d1
zpěváka	zpěvák	k1gMnSc2
Robbieho	Robbie	k1gMnSc2
Williamse	Williams	k1gMnSc2
<g/>
,	,	kIx,
vydané	vydaný	k2eAgInPc4d1
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
po	po	k7c6
rozpadu	rozpad	k1gInSc6
kapely	kapela	k1gFnSc2
Take	Take	k1gFnSc2
That	That	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
britské	britský	k2eAgFnSc6d1
hitparádě	hitparáda	k1gFnSc6
deska	deska	k1gFnSc1
dosáhla	dosáhnout	k5eAaPmAgFnS
první	první	k4xOgFnPc4
příčky	příčka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Celkem	celkem	k6eAd1
bylo	být	k5eAaImAgNnS
vydáno	vydat	k5eAaPmNgNnS
pět	pět	k4xCc1
singlů	singl	k1gInPc2
(	(	kIx(
<g/>
„	„	k?
<g/>
Old	Olda	k1gFnPc2
Before	Befor	k1gInSc5
Die	Die	k1gFnPc3
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
Lazy	Lazy	k?
Days	Days	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
South	South	k1gInSc1
of	of	k?
the	the	k?
Border	Border	k1gInSc1
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
Angels	Angels	k1gInSc1
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
Let	let	k1gInSc1
Me	Me	k1gMnSc1
Entertain	Entertain	k1gMnSc1
You	You	k1gMnSc1
<g/>
“	“	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Album	album	k1gNnSc1
bylo	být	k5eAaImAgNnS
ve	v	k7c6
Spojeném	spojený	k2eAgNnSc6d1
království	království	k1gNnSc6
oceněno	ocenit	k5eAaPmNgNnS
jako	jako	k9
8	#num#	k4
<g/>
x	x	k?
platinové	platinový	k2eAgInPc1d1
a	a	k8xC
prodalo	prodat	k5eAaPmAgNnS
se	se	k3xPyFc4
ho	on	k3xPp3gNnSc2
okolo	okolo	k7c2
2	#num#	k4
500	#num#	k4
000	#num#	k4
kusů	kus	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největším	veliký	k2eAgInSc7d3
hitem	hit	k1gInSc7
je	být	k5eAaImIp3nS
nejspíše	nejspíše	k9
skladba	skladba	k1gFnSc1
„	„	k?
<g/>
Angels	Angels	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Seznam	seznam	k1gInSc1
skladeb	skladba	k1gFnPc2
</s>
<s>
„	„	k?
<g/>
Lazy	Lazy	k?
Days	Days	k1gInSc1
<g/>
“	“	k?
–	–	k?
3	#num#	k4
<g/>
:	:	kIx,
<g/>
52	#num#	k4
</s>
<s>
„	„	k?
<g/>
Life	Life	k1gFnSc1
thru	thru	k5eAaPmIp1nS
a	a	k8xC
Lens	Lens	k1gInSc1
<g/>
“	“	k?
–	–	k?
3	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
6	#num#	k4
</s>
<s>
„	„	k?
<g/>
Ego	ego	k1gNnSc1
a	a	k8xC
Go	Go	k1gFnSc1
Go	Go	k1gFnSc2
<g/>
“	“	k?
–	–	k?
3	#num#	k4
<g/>
:	:	kIx,
<g/>
31	#num#	k4
</s>
<s>
„	„	k?
<g/>
Angels	Angels	k1gInSc1
<g/>
“	“	k?
–	–	k?
4	#num#	k4
<g/>
:	:	kIx,
<g/>
23	#num#	k4
</s>
<s>
„	„	k?
<g/>
South	South	k1gInSc1
of	of	k?
the	the	k?
Border	Border	k1gInSc1
<g/>
“	“	k?
–	–	k?
3	#num#	k4
<g/>
:	:	kIx,
<g/>
52	#num#	k4
</s>
<s>
„	„	k?
<g/>
Old	Olda	k1gFnPc2
Before	Befor	k1gInSc5
I	i	k9
Die	Die	k1gMnSc2
<g/>
“	“	k?
–	–	k?
3	#num#	k4
<g/>
:	:	kIx,
<g/>
52	#num#	k4
</s>
<s>
„	„	k?
<g/>
One	One	k1gMnSc1
of	of	k?
God	God	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
Better	Better	k1gMnSc1
People	People	k1gMnSc7
<g/>
“	“	k?
–	–	k?
3	#num#	k4
<g/>
:	:	kIx,
<g/>
33	#num#	k4
</s>
<s>
„	„	k?
<g/>
Let	let	k1gInSc1
Me	Me	k1gMnSc1
Entertain	Entertain	k1gMnSc1
You	You	k1gMnSc1
<g/>
“	“	k?
–	–	k?
4	#num#	k4
<g/>
:	:	kIx,
<g/>
20	#num#	k4
</s>
<s>
„	„	k?
<g/>
Killing	Killing	k1gInSc1
Me	Me	k1gFnSc1
<g/>
“	“	k?
–	–	k?
3	#num#	k4
<g/>
:	:	kIx,
<g/>
55	#num#	k4
</s>
<s>
„	„	k?
<g/>
Clean	Clean	k1gInSc1
<g/>
“	“	k?
–	–	k?
3	#num#	k4
<g/>
:	:	kIx,
<g/>
51	#num#	k4
</s>
<s>
„	„	k?
<g/>
Baby	baby	k1gNnSc1
Girl	girl	k1gFnSc1
Window	Window	k1gMnSc2
<g/>
“	“	k?
–	–	k?
6	#num#	k4
<g/>
:	:	kIx,
<g/>
10	#num#	k4
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
BUSH	Bush	k1gMnSc1
<g/>
,	,	kIx,
John	John	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Robbie	Robbie	k1gFnSc1
Williams	Williamsa	k1gFnPc2
<g/>
:	:	kIx,
Life	Life	k1gInSc1
Thru	Thrus	k1gInSc2
a	a	k8xC
Lens	Lensa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Allmusic	Allmusic	k1gMnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Robbie	Robbie	k1gFnSc1
Williams	Williams	k1gInSc4
Studiová	studiový	k2eAgNnPc4d1
alba	album	k1gNnPc4
</s>
<s>
Life	Life	k6eAd1
thru	thru	k5eAaPmIp1nS
a	a	k8xC
Lens	Lens	k1gInSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
•	•	k?
I	i	k9
<g/>
'	'	kIx"
<g/>
ve	v	k7c4
Been	Been	k1gInSc4
Expecting	Expecting	k1gInSc1
You	You	k1gFnSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sing	Sing	k1gMnSc1
When	When	k1gMnSc1
You	You	k1gMnSc1
<g/>
'	'	kIx"
<g/>
re	re	k9
Winning	Winning	k1gInSc1
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Swing	swing	k1gInSc1
When	When	k1gMnSc1
You	You	k1gMnSc1
<g/>
'	'	kIx"
<g/>
re	re	k9
Winning	Winning	k1gInSc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Escapology	Escapolog	k1gMnPc7
(	(	kIx(
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Intensive	Intensiv	k1gMnSc5
Care	car	k1gMnSc5
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Rudebox	Rudebox	k1gInSc1
(	(	kIx(
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Reality	realita	k1gFnSc2
Killed	Killed	k1gMnSc1
the	the	k?
Video	video	k1gNnSc1
Star	Star	kA
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Take	Take	k1gInSc1
the	the	k?
Crown	Crown	k1gInSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Swings	Swings	k1gInSc1
Both	Both	k1gMnSc1
Ways	Ways	k1gInSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
•	•	k?
The	The	k1gMnSc1
Heavy	Heava	k1gFnSc2
Entertainment	Entertainment	k1gMnSc1
Show	show	k1gFnSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Hudba	hudba	k1gFnSc1
</s>
