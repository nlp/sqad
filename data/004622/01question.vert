<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaImIp3nS	jmenovat
proces	proces	k1gInSc1	proces
zachycující	zachycující	k2eAgNnSc4d1	zachycující
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
v	v	k7c4	v
daný	daný	k2eAgInSc4d1	daný
okamžik	okamžik	k1gInSc4	okamžik
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
smysly	smysl	k1gInPc4	smysl
<g/>
,	,	kIx,	,
informuje	informovat	k5eAaBmIp3nS	informovat
o	o	k7c6	o
vnějším	vnější	k2eAgInSc6d1	vnější
i	i	k8xC	i
vnitřním	vnitřní	k2eAgInSc6d1	vnitřní
světě	svět	k1gInSc6	svět
<g/>
?	?	kIx.	?
</s>
