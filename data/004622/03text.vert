<s>
Vnímání	vnímání	k1gNnSc1	vnímání
(	(	kIx(	(
<g/>
též	též	k9	též
percepce	percepce	k1gFnSc1	percepce
<g/>
)	)	kIx)	)
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
v	v	k7c4	v
daný	daný	k2eAgInSc4d1	daný
okamžik	okamžik	k1gInSc4	okamžik
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
smysly	smysl	k1gInPc4	smysl
<g/>
,	,	kIx,	,
informuje	informovat	k5eAaBmIp3nS	informovat
o	o	k7c6	o
vnějším	vnější	k2eAgInSc6d1	vnější
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
chuť	chuť	k1gFnSc1	chuť
<g/>
)	)	kIx)	)
i	i	k8xC	i
vnitřním	vnitřní	k2eAgMnSc6d1	vnitřní
(	(	kIx(	(
<g/>
bolest	bolest	k1gFnSc1	bolest
<g/>
,	,	kIx,	,
zadýchání	zadýchání	k1gNnSc1	zadýchání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vnímání	vnímání	k1gNnSc1	vnímání
je	být	k5eAaImIp3nS	být
subjektivním	subjektivní	k2eAgInSc7d1	subjektivní
odrazem	odraz	k1gInSc7	odraz
objektivní	objektivní	k2eAgFnSc2d1	objektivní
reality	realita	k1gFnSc2	realita
v	v	k7c6	v
našem	náš	k3xOp1gNnSc6	náš
vědomí	vědomí	k1gNnSc6	vědomí
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
receptorů	receptor	k1gInPc2	receptor
<g/>
.	.	kIx.	.
</s>
<s>
Umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
základní	základní	k2eAgFnSc4d1	základní
orientaci	orientace	k1gFnSc4	orientace
v	v	k7c6	v
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
v	v	k7c6	v
aktuální	aktuální	k2eAgFnSc6d1	aktuální
situaci	situace	k1gFnSc6	situace
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
vnímání	vnímání	k1gNnSc2	vnímání
jsou	být	k5eAaImIp3nP	být
počitky	počitek	k1gInPc4	počitek
a	a	k8xC	a
vjemy	vjem	k1gInPc4	vjem
<g/>
.	.	kIx.	.
</s>
<s>
Počitek	počitek	k1gInSc1	počitek
je	být	k5eAaImIp3nS	být
nejjednodušším	jednoduchý	k2eAgInSc7d3	nejjednodušší
elementem	element	k1gInSc7	element
našeho	náš	k3xOp1gNnSc2	náš
vnímání	vnímání	k1gNnSc2	vnímání
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
základním	základní	k2eAgInSc7d1	základní
materiálem	materiál	k1gInSc7	materiál
pro	pro	k7c4	pro
složitější	složitý	k2eAgInPc4d2	složitější
procesy	proces	k1gInPc4	proces
<g/>
:	:	kIx,	:
paměť	paměť	k1gFnSc1	paměť
<g/>
,	,	kIx,	,
myšlení	myšlení	k1gNnSc1	myšlení
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
výsledný	výsledný	k2eAgInSc4d1	výsledný
prvek	prvek	k1gInSc4	prvek
jednoho	jeden	k4xCgMnSc2	jeden
analyzátoru	analyzátor	k1gInSc2	analyzátor
(	(	kIx(	(
<g/>
smyslu	smysl	k1gInSc2	smysl
<g/>
)	)	kIx)	)
–	–	k?	–
např.	např.	kA	např.
receptor	receptor	k1gInSc1	receptor
+	+	kIx~	+
nerv	nerv	k1gInSc1	nerv
+	+	kIx~	+
mozek	mozek	k1gInSc1	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nS	tvořit
obraz	obraz	k1gInSc1	obraz
jednoho	jeden	k4xCgInSc2	jeden
znaku	znak	k1gInSc2	znak
vnímaného	vnímaný	k2eAgInSc2d1	vnímaný
předmětu	předmět	k1gInSc2	předmět
(	(	kIx(	(
<g/>
např.	např.	kA	např.
modrá	modrý	k2eAgFnSc1d1	modrá
barva	barva	k1gFnSc1	barva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
většího	veliký	k2eAgNnSc2d2	veliký
množství	množství	k1gNnSc2	množství
počitků	počitek	k1gInPc2	počitek
je	být	k5eAaImIp3nS	být
vjem	vjem	k1gInSc1	vjem
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
zpracování	zpracování	k1gNnSc6	zpracování
počitků	počitek	k1gInPc2	počitek
do	do	k7c2	do
větších	veliký	k2eAgInPc2d2	veliký
celků	celek	k1gInPc2	celek
se	se	k3xPyFc4	se
uplatňuje	uplatňovat	k5eAaImIp3nS	uplatňovat
i	i	k9	i
myšlení	myšlení	k1gNnSc1	myšlení
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
výsledný	výsledný	k2eAgInSc1d1	výsledný
vjem	vjem	k1gInSc1	vjem
je	být	k5eAaImIp3nS	být
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
suma	suma	k1gFnSc1	suma
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Zákonitostmi	zákonitost	k1gFnPc7	zákonitost
vnímání	vnímání	k1gNnSc2	vnímání
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgInS	zabývat
zvláště	zvláště	k6eAd1	zvláště
gestaltismus	gestaltismus	k1gInSc1	gestaltismus
neboli	neboli	k8xC	neboli
tvarová	tvarový	k2eAgFnSc1d1	tvarová
psychologie	psychologie	k1gFnSc1	psychologie
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
gestaltisté	gestaltista	k1gMnPc1	gestaltista
byli	být	k5eAaImAgMnP	být
zastánci	zastánce	k1gMnPc7	zastánce
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
celek	celek	k1gInSc1	celek
je	být	k5eAaImIp3nS	být
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
jen	jen	k9	jen
souhrn	souhrn	k1gInSc4	souhrn
částí	část	k1gFnPc2	část
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
celkem	celek	k1gInSc7	celek
a	a	k8xC	a
částmi	část	k1gFnPc7	část
existuje	existovat	k5eAaImIp3nS	existovat
specifický	specifický	k2eAgInSc1d1	specifický
vztah	vztah	k1gInSc1	vztah
<g/>
.	.	kIx.	.
</s>
<s>
Celek	celek	k1gInSc1	celek
vytváří	vytvářit	k5eAaPmIp3nS	vytvářit
tzv.	tzv.	kA	tzv.
Gestalt	Gestalt	k2eAgInSc1d1	Gestalt
(	(	kIx(	(
<g/>
tvar	tvar	k1gInSc1	tvar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jenž	k3xRgMnSc4	jenž
uspořádání	uspořádání	k1gNnSc1	uspořádání
podléhá	podléhat	k5eAaImIp3nS	podléhat
určitým	určitý	k2eAgInSc7d1	určitý
pravidlům	pravidlo	k1gNnPc3	pravidlo
-	-	kIx~	-
Gestalt	Gestalt	k1gInSc1	Gestalt
zákonům	zákon	k1gInPc3	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
Gestalt	Gestalt	k1gInSc1	Gestalt
zákonem	zákon	k1gInSc7	zákon
je	být	k5eAaImIp3nS	být
tzv.	tzv.	kA	tzv.
zákon	zákon	k1gInSc4	zákon
pregnantnosti	pregnantnost	k1gFnSc2	pregnantnost
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
kterého	který	k3yQgMnSc2	který
Gestalt	Gestaltum	k1gNnPc2	Gestaltum
směřuje	směřovat	k5eAaImIp3nS	směřovat
vždy	vždy	k6eAd1	vždy
k	k	k7c3	k
co	co	k9	co
nejjednoduššímu	jednoduchý	k2eAgNnSc3d3	nejjednodušší
uspořádání	uspořádání	k1gNnSc3	uspořádání
prvků	prvek	k1gInPc2	prvek
do	do	k7c2	do
jednoznačně	jednoznačně	k6eAd1	jednoznačně
definovatelného	definovatelný	k2eAgInSc2d1	definovatelný
celku	celek	k1gInSc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
tohoto	tento	k3xDgInSc2	tento
zákona	zákon	k1gInSc2	zákon
jsou	být	k5eAaImIp3nP	být
odvozeny	odvodit	k5eAaPmNgFnP	odvodit
i	i	k9	i
ty	ten	k3xDgFnPc1	ten
ostatní	ostatní	k2eAgFnPc1d1	ostatní
<g/>
,	,	kIx,	,
např.	např.	kA	např.
zákon	zákon	k1gInSc1	zákon
doplnění	doplnění	k1gNnSc2	doplnění
dobrého	dobrý	k2eAgInSc2d1	dobrý
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
zákon	zákon	k1gInSc1	zákon
uzavřenosti	uzavřenost	k1gFnSc2	uzavřenost
<g/>
,	,	kIx,	,
zákon	zákon	k1gInSc4	zákon
blízkosti	blízkost	k1gFnSc2	blízkost
(	(	kIx(	(
<g/>
části	část	k1gFnSc2	část
lokalizované	lokalizovaný	k2eAgFnPc1d1	lokalizovaná
blízko	blízko	k6eAd1	blízko
sebe	sebe	k3xPyFc4	sebe
mají	mít	k5eAaImIp3nP	mít
tendenci	tendence	k1gFnSc4	tendence
se	se	k3xPyFc4	se
ve	v	k7c6	v
vjemu	vjem	k1gInSc6	vjem
spojovat	spojovat	k5eAaImF	spojovat
a	a	k8xC	a
vytvářet	vytvářet	k5eAaImF	vytvářet
celek	celek	k1gInSc4	celek
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zákon	zákon	k1gInSc1	zákon
figury	figura	k1gFnSc2	figura
a	a	k8xC	a
pozadí	pozadí	k1gNnSc2	pozadí
(	(	kIx(	(
<g/>
rozdělení	rozdělení	k1gNnSc1	rozdělení
vjemového	vjemový	k2eAgNnSc2d1	vjemové
pole	pole	k1gNnSc2	pole
na	na	k7c4	na
dominantní	dominantní	k2eAgInSc4d1	dominantní
obrazec	obrazec	k1gInSc4	obrazec
=	=	kIx~	=
figuru	figura	k1gFnSc4	figura
a	a	k8xC	a
pozadí	pozadí	k1gNnSc4	pozadí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vnímání	vnímání	k1gNnSc1	vnímání
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
zjednodušení	zjednodušení	k1gNnSc2	zjednodušení
a	a	k8xC	a
snadnější	snadný	k2eAgFnSc2d2	snazší
orientace	orientace	k1gFnSc2	orientace
člověka	člověk	k1gMnSc2	člověk
zkresleno	zkreslen	k2eAgNnSc1d1	zkresleno
<g/>
,	,	kIx,	,
toho	ten	k3xDgNnSc2	ten
využívají	využívat	k5eAaPmIp3nP	využívat
tzv.	tzv.	kA	tzv.
optické	optický	k2eAgInPc4d1	optický
klamy	klam	k1gInPc4	klam
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Kognitivní	kognitivní	k2eAgNnSc1d1	kognitivní
zkreslení	zkreslení	k1gNnSc1	zkreslení
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
hlavní	hlavní	k2eAgFnSc4d1	hlavní
příčinu	příčina	k1gFnSc4	příčina
iluzí	iluze	k1gFnPc2	iluze
patří	patřit	k5eAaImIp3nS	patřit
aktuální	aktuální	k2eAgNnSc1d1	aktuální
emocionální	emocionální	k2eAgNnSc1d1	emocionální
vyladění	vyladění	k1gNnSc1	vyladění
pozorovatele	pozorovatel	k1gMnSc2	pozorovatel
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
v	v	k7c6	v
parku	park	k1gInSc6	park
večer	večer	k6eAd1	večer
člověk	člověk	k1gMnSc1	člověk
v	v	k7c6	v
každém	každý	k3xTgInSc6	každý
stínu	stín	k1gInSc6	stín
"	"	kIx"	"
<g/>
vidí	vidět	k5eAaImIp3nS	vidět
<g/>
"	"	kIx"	"
plížící	plížící	k2eAgNnSc4d1	plížící
se	se	k3xPyFc4	se
postavu	postav	k1gInSc2	postav
<g/>
.	.	kIx.	.
</s>
<s>
Halucinace	halucinace	k1gFnSc1	halucinace
(	(	kIx(	(
<g/>
pokud	pokud	k8xS	pokud
nejsou	být	k5eNaImIp3nP	být
důsledkem	důsledek	k1gInSc7	důsledek
psychoaktivních	psychoaktivní	k2eAgFnPc2d1	psychoaktivní
látek	látka	k1gFnPc2	látka
<g/>
)	)	kIx)	)
vznikají	vznikat	k5eAaImIp3nP	vznikat
jako	jako	k8xS	jako
obranný	obranný	k2eAgInSc1d1	obranný
mechanismus	mechanismus	k1gInSc1	mechanismus
proti	proti	k7c3	proti
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
co	co	k9	co
člověka	člověk	k1gMnSc4	člověk
vnitřně	vnitřně	k6eAd1	vnitřně
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
totiž	totiž	k9	totiž
projekce	projekce	k1gFnSc1	projekce
něčeho	něco	k3yInSc2	něco
z	z	k7c2	z
nitra	nitro	k1gNnSc2	nitro
do	do	k7c2	do
vnějšího	vnější	k2eAgNnSc2d1	vnější
okolí	okolí	k1gNnSc2	okolí
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
osoby	osoba	k1gFnPc4	osoba
postižené	postižený	k2eAgFnPc4d1	postižená
halucinací	halucinace	k1gFnSc7	halucinace
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
je	on	k3xPp3gFnPc4	on
psychicky	psychicky	k6eAd1	psychicky
stravuje	stravovat	k5eAaImIp3nS	stravovat
<g/>
,	,	kIx,	,
už	už	k6eAd1	už
snesitelnější	snesitelný	k2eAgFnSc1d2	snesitelnější
ve	v	k7c6	v
vnější	vnější	k2eAgFnSc6d1	vnější
realitě	realita	k1gFnSc6	realita
<g/>
.	.	kIx.	.
</s>
<s>
Zkreslené	zkreslený	k2eAgNnSc1d1	zkreslené
vnímání	vnímání	k1gNnSc1	vnímání
nebo	nebo	k8xC	nebo
změněné	změněný	k2eAgNnSc1d1	změněné
vnímání	vnímání	k1gNnSc1	vnímání
je	být	k5eAaImIp3nS	být
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
změněným	změněný	k2eAgInSc7d1	změněný
chemickofyzikálním	chemickofyzikální	k2eAgInSc7d1	chemickofyzikální
procesem	proces	k1gInSc7	proces
v	v	k7c6	v
mozku	mozek	k1gInSc6	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Takový	takový	k3xDgInSc1	takový
proces	proces	k1gInSc1	proces
může	moct	k5eAaImIp3nS	moct
způsobovat	způsobovat	k5eAaImF	způsobovat
špatný	špatný	k2eAgInSc4d1	špatný
tělesný	tělesný	k2eAgInSc4d1	tělesný
metabolismus	metabolismus	k1gInSc4	metabolismus
nebo	nebo	k8xC	nebo
přijímání	přijímání	k1gNnSc4	přijímání
chemikálií	chemikálie	k1gFnPc2	chemikálie
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
přímo	přímo	k6eAd1	přímo
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
změněné	změněný	k2eAgNnSc4d1	změněné
vnímání	vnímání	k1gNnSc4	vnímání
<g/>
.	.	kIx.	.
</s>
<s>
Příkladem	příklad	k1gInSc7	příklad
takových	takový	k3xDgInPc2	takový
metabolitů	metabolit	k1gInPc2	metabolit
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
gliadinomorfin	gliadinomorfina	k1gFnPc2	gliadinomorfina
nebo	nebo	k8xC	nebo
kazomorfin	kazomorfina	k1gFnPc2	kazomorfina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
jsou	být	k5eAaImIp3nP	být
zařazeny	zařadit	k5eAaPmNgInP	zařadit
mezi	mezi	k7c7	mezi
opiáty	opiát	k1gInPc7	opiát
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
v	v	k7c6	v
potravě	potrava	k1gFnSc6	potrava
často	často	k6eAd1	často
se	se	k3xPyFc4	se
vyskytující	vyskytující	k2eAgInPc1d1	vyskytující
proteiny	protein	k1gInPc1	protein
jako	jako	k8xS	jako
lepky	lepek	k1gInPc1	lepek
<g/>
,	,	kIx,	,
kaseiny	kasein	k1gInPc1	kasein
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
vajíčkách	vajíčko	k1gNnPc6	vajíčko
či	či	k8xC	či
špenátu	špenát	k1gInSc2	špenát
obsažené	obsažený	k2eAgFnSc2d1	obsažená
bílkoviny	bílkovina	k1gFnSc2	bílkovina
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
zažívacím	zažívací	k2eAgInSc7d1	zažívací
procesem	proces	k1gInSc7	proces
přeměněny	přeměněn	k2eAgInPc4d1	přeměněn
na	na	k7c4	na
tzv.	tzv.	kA	tzv.
opioidní	opioidní	k2eAgInPc1d1	opioidní
peptidy	peptid	k1gInPc1	peptid
<g/>
.	.	kIx.	.
</s>
<s>
Takové	takový	k3xDgInPc1	takový
peptidy	peptid	k1gInPc1	peptid
na	na	k7c4	na
tělo	tělo	k1gNnSc4	tělo
působí	působit	k5eAaImIp3nS	působit
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
morfin	morfin	k1gInSc4	morfin
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
osob	osoba	k1gFnPc2	osoba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
nejsou	být	k5eNaImIp3nP	být
schopny	schopen	k2eAgInPc1d1	schopen
opioidní	opioidní	k2eAgInPc1d1	opioidní
peptidy	peptid	k1gInPc1	peptid
dále	daleko	k6eAd2	daleko
zažíváním	zažívání	k1gNnSc7	zažívání
proměnit	proměnit	k5eAaPmF	proměnit
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mohou	moct	k5eAaImIp3nP	moct
vyvinout	vyvinout	k5eAaPmF	vyvinout
příznaky	příznak	k1gInPc1	příznak
fyzické	fyzický	k2eAgFnSc2d1	fyzická
nebo	nebo	k8xC	nebo
duševní	duševní	k2eAgFnSc2d1	duševní
choroby	choroba	k1gFnSc2	choroba
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
zdrojem	zdroj	k1gInSc7	zdroj
neobvyklého	obvyklý	k2eNgNnSc2d1	neobvyklé
vnímání	vnímání	k1gNnSc2	vnímání
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
například	například	k6eAd1	například
drogy	droga	k1gFnPc4	droga
<g/>
,	,	kIx,	,
rtuť	rtuť	k1gFnSc1	rtuť
a	a	k8xC	a
jiné	jiný	k2eAgFnPc1d1	jiná
chemikálie	chemikálie	k1gFnPc1	chemikálie
<g/>
,	,	kIx,	,
ovlivňující	ovlivňující	k2eAgInPc1d1	ovlivňující
mozkovou	mozkový	k2eAgFnSc4d1	mozková
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
parazitární	parazitární	k2eAgFnSc2d1	parazitární
a	a	k8xC	a
bakteriální	bakteriální	k2eAgFnSc2d1	bakteriální
infekce	infekce	k1gFnSc2	infekce
postihující	postihující	k2eAgInSc1d1	postihující
mozek	mozek	k1gInSc1	mozek
<g/>
.	.	kIx.	.
</s>
<s>
Neobvyklé	obvyklý	k2eNgNnSc1d1	neobvyklé
vnímání	vnímání	k1gNnSc1	vnímání
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
projevovat	projevovat	k5eAaImF	projevovat
například	například	k6eAd1	například
jako	jako	k8xC	jako
derealizace	derealizace	k1gFnPc1	derealizace
<g/>
,	,	kIx,	,
depersonalizace	depersonalizace	k1gFnPc1	depersonalizace
<g/>
,	,	kIx,	,
bludy	blud	k1gInPc1	blud
či	či	k8xC	či
halucinace	halucinace	k1gFnPc1	halucinace
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
chemické	chemický	k2eAgFnPc4d1	chemická
látky	látka	k1gFnPc4	látka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
způsobují	způsobovat	k5eAaImIp3nP	způsobovat
abnormální	abnormální	k2eAgNnSc4d1	abnormální
vnímání	vnímání	k1gNnSc4	vnímání
<g/>
,	,	kIx,	,
způsobené	způsobený	k2eAgNnSc1d1	způsobené
chemickou	chemický	k2eAgFnSc7d1	chemická
změnou	změna	k1gFnSc7	změna
vědomí	vědomí	k1gNnSc2	vědomí
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
toluen	toluen	k1gInSc1	toluen
<g/>
,	,	kIx,	,
rtuť	rtuť	k1gFnSc1	rtuť
<g/>
,	,	kIx,	,
LSD	LSD	kA	LSD
<g/>
,	,	kIx,	,
marihuana	marihuana	k1gFnSc1	marihuana
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
různé	různý	k2eAgFnPc4d1	různá
potraviny	potravina	k1gFnPc4	potravina
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
špatný	špatný	k2eAgInSc1d1	špatný
metabolismus	metabolismus	k1gInSc1	metabolismus
dokáže	dokázat	k5eAaPmIp3nS	dokázat
vyrobit	vyrobit	k5eAaPmF	vyrobit
vědomí	vědomí	k1gNnSc4	vědomí
ovlivňující	ovlivňující	k2eAgFnSc2d1	ovlivňující
chemikálie	chemikálie	k1gFnSc2	chemikálie
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
vnímání	vnímání	k1gNnSc2	vnímání
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
