<s>
Vnímání	vnímání	k1gNnSc1	vnímání
(	(	kIx(	(
<g/>
též	též	k9	též
percepce	percepce	k1gFnSc1	percepce
<g/>
)	)	kIx)	)
zachycuje	zachycovat	k5eAaImIp3nS	zachycovat
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
v	v	k7c4	v
daný	daný	k2eAgInSc4d1	daný
okamžik	okamžik	k1gInSc4	okamžik
působí	působit	k5eAaImIp3nS	působit
na	na	k7c4	na
smysly	smysl	k1gInPc4	smysl
<g/>
,	,	kIx,	,
informuje	informovat	k5eAaBmIp3nS	informovat
o	o	k7c6	o
vnějším	vnější	k2eAgInSc6d1	vnější
světě	svět	k1gInSc6	svět
(	(	kIx(	(
<g/>
barva	barva	k1gFnSc1	barva
<g/>
,	,	kIx,	,
chuť	chuť	k1gFnSc1	chuť
<g/>
)	)	kIx)	)
i	i	k8xC	i
vnitřním	vnitřní	k2eAgMnSc6d1	vnitřní
(	(	kIx(	(
<g/>
bolest	bolest	k1gFnSc1	bolest
<g/>
,	,	kIx,	,
zadýchání	zadýchání	k1gNnSc1	zadýchání
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
