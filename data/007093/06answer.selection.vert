<s>
Poznatky	poznatek	k1gInPc1
získané	získaný	k2eAgInPc1d1
v	v	k7c6
projektu	projekt	k1gInSc6
Manhattan	Manhattan	k1gInSc1
posloužily	posloužit	k5eAaPmAgInP
k	k	k7c3
sestrojení	sestrojení	k1gNnSc3
pum	puma	k1gFnPc2
Fat	fatum	k1gNnPc2
Man	Man	k1gMnSc1
a	a	k8xC
Little	Little	k1gFnSc1
Boy	boa	k1gFnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc4
byly	být	k5eAaImAgFnP
6	[number]	k4
<g/>
.	.	kIx.
a	a	k8xC
9	[number]	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1945	[number]	k4
použity	použít	k5eAaPmNgInP
k	k	k7c3
útoku	útok	k1gInSc3
na	na	k7c4
japonská	japonský	k2eAgNnPc4d1
města	město	k1gNnPc4
Hirošimu	Hirošima	k1gFnSc4
a	a	k8xC
Nagasaki	Nagasaki	k1gNnSc4
<g/>
.	.	kIx.
</s>