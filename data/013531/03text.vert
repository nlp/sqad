<s>
José	José	k6eAd1
Cardozo	Cardoza	k1gFnSc5
</s>
<s>
José	José	k6eAd1
Cardozo	Cardoza	k1gFnSc5
Narození	narození	k1gNnSc5
</s>
<s>
19	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1971	#num#	k4
(	(	kIx(
<g/>
50	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
<g/>
Nueva	Nueva	k1gFnSc1
Italia	Italium	k1gNnSc2
Povolání	povolání	k1gNnSc2
</s>
<s>
fotbalista	fotbalista	k1gMnSc1
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc1
medailí	medaile	k1gFnPc2
</s>
<s>
Fotbal	fotbal	k1gInSc1
na	na	k7c4
LOH	LOH	kA
</s>
<s>
LOH	LOH	kA
2004	#num#	k4
</s>
<s>
Paraguay	Paraguay	k1gFnSc1
U23	U23	k1gFnSc2
</s>
<s>
José	José	k1gMnSc1
Saturnino	Saturnino	k1gMnSc1
Cardozo	Cardozo	k1gMnSc1
Otazú	Otazú	k1gMnSc1
(	(	kIx(
<g/>
*	*	kIx~
19	#num#	k4
<g/>
.	.	kIx.
březen	březen	k1gInSc1
1971	#num#	k4
<g/>
,	,	kIx,
Nueva	Nueva	k1gFnSc1
Italia	Italia	k1gFnSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
bývalý	bývalý	k2eAgMnSc1d1
paraguajský	paraguajský	k2eAgMnSc1d1
fotbalista	fotbalista	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrával	hrávat	k5eAaImAgMnS
na	na	k7c6
pozici	pozice	k1gFnSc6
útočníka	útočník	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Získal	získat	k5eAaPmAgInS
stříbrnou	stříbrný	k2eAgFnSc4d1
medaili	medaile	k1gFnSc4
na	na	k7c6
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
roku	rok	k1gInSc2
2004	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Hrál	hrát	k5eAaImAgInS
i	i	k9
na	na	k7c6
dvou	dva	k4xCgInPc6
světových	světový	k2eAgInPc6d1
šampionátech	šampionát	k1gInPc6
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Za	za	k7c4
paraguajskou	paraguajský	k2eAgFnSc4d1
reprezentaci	reprezentace	k1gFnSc4
celkem	celkem	k6eAd1
odehrál	odehrát	k5eAaPmAgMnS
82	#num#	k4
utkání	utkání	k1gNnSc2
a	a	k8xC
vstřelil	vstřelit	k5eAaPmAgMnS
25	#num#	k4
branek	branka	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Je	být	k5eAaImIp3nS
tak	tak	k6eAd1
nejlepším	dobrý	k2eAgMnSc7d3
střelcem	střelec	k1gMnSc7
v	v	k7c6
historii	historie	k1gFnSc6
národního	národní	k2eAgInSc2d1
týmu	tým	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
S	s	k7c7
mexickým	mexický	k2eAgInSc7d1
klubem	klub	k1gInSc7
Deportivo	Deportiva	k1gFnSc5
Toluca	Toluc	k1gInSc2
vyhrál	vyhrát	k5eAaPmAgMnS
roku	rok	k1gInSc2
2003	#num#	k4
Pohár	pohár	k1gInSc4
mistrů	mistr	k1gMnPc2
CONCACAF	CONCACAF	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Dvakrát	dvakrát	k6eAd1
též	též	k9
hrál	hrát	k5eAaImAgMnS
finále	finále	k1gNnSc2
Poháru	pohár	k1gInSc2
osvoboditelů	osvoboditel	k1gMnPc2
(	(	kIx(
<g/>
Copa	Copa	k1gMnSc1
Libertadores	Libertadores	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
roku	rok	k1gInSc2
1993	#num#	k4
s	s	k7c7
chilským	chilský	k2eAgInSc7d1
klubem	klub	k1gInSc7
Universidad	Universidad	k1gInSc1
Catolica	Catolic	k1gInSc2
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
roku	rok	k1gInSc2
2001	#num#	k4
s	s	k7c7
mexickým	mexický	k2eAgNnSc7d1
Cruz	Cruz	k1gInSc4
Azul	Azul	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Čtyřikrát	čtyřikrát	k6eAd1
byl	být	k5eAaImAgInS
nejlepším	dobrý	k2eAgMnSc7d3
střelcem	střelec	k1gMnSc7
mexické	mexický	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Drží	držet	k5eAaImIp3nS
v	v	k7c6
ní	on	k3xPp3gFnSc6
také	také	k9
rekord	rekord	k1gInSc4
v	v	k7c6
počtu	počet	k1gInSc6
gólů	gól	k1gInPc2
v	v	k7c6
jedné	jeden	k4xCgFnSc6
sezóně	sezóna	k1gFnSc6
(	(	kIx(
<g/>
58	#num#	k4
branek	branka	k1gFnPc2
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
a	a	k8xC
s	s	k7c7
252	#num#	k4
góly	gól	k1gInPc7
je	být	k5eAaImIp3nS
čtvrtým	čtvrtý	k4xOgMnSc7
nejlepším	dobrý	k2eAgMnSc7d3
střelcem	střelec	k1gMnSc7
její	její	k3xOp3gFnSc2
historie	historie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Třikrát	třikrát	k6eAd1
byl	být	k5eAaImAgMnS
vyhlášen	vyhlásit	k5eAaPmNgMnS
paraguajským	paraguajský	k2eAgMnSc7d1
fotbalistou	fotbalista	k1gMnSc7
roku	rok	k1gInSc2
(	(	kIx(
<g/>
2000	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
,	,	kIx,
2003	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
roku	rok	k1gInSc2
2002	#num#	k4
dokonce	dokonce	k9
nejlepším	dobrý	k2eAgMnSc7d3
fotbalistou	fotbalista	k1gMnSc7
Jižní	jižní	k2eAgFnSc2d1
Ameriky	Amerika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
skončení	skončení	k1gNnSc6
hráčské	hráčský	k2eAgFnSc2d1
kariéry	kariéra	k1gFnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
trenérem	trenér	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://www.transfermarkt.co.uk/en/jose-cardozo/profil/spieler_38788.html	http://www.transfermarkt.co.uk/en/jose-cardozo/profil/spieler_38788.html	k1gInSc1
<g/>
↑	↑	k?
http://www.rsssf.com/tableso/ol2004f-det.html	http://www.rsssf.com/tableso/ol2004f-det.html	k1gInSc1
<g/>
↑	↑	k?
http://www.fifa.com/worldfootball/statisticsandrecords/players/player=162010/	http://www.fifa.com/worldfootball/statisticsandrecords/players/player=162010/	k4
<g/>
↑	↑	k?
http://www.rsssf.com/miscellaneous/cardozo-intlg.html	http://www.rsssf.com/miscellaneous/cardozo-intlg.html	k1gMnSc1
<g/>
↑	↑	k?
http://www.rsssf.com/miscellaneous/para-recintlp.html#goals	http://www.rsssf.com/miscellaneous/para-recintlp.html#goals	k6eAd1
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
www.transfermarkt.com	www.transfermarkt.com	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://www.rsssf.com/sacups/copa93full.html	http://www.rsssf.com/sacups/copa93full.html	k1gMnSc1
<g/>
↑	↑	k?
http://www.rsssf.com/sacups/copa01.html	http://www.rsssf.com/sacups/copa01.html	k1gMnSc1
<g/>
↑	↑	k?
http://www.rsssf.com/tablesm/mextops.html	http://www.rsssf.com/tablesm/mextops.html	k1gMnSc1
<g/>
↑	↑	k?
http://www.rsssf.com/miscellaneous/para-recintlp.html#goals	http://www.rsssf.com/miscellaneous/para-recintlp.html#goals	k6eAd1
<g/>
↑	↑	k?
http://www.rsssf.com/tablesm/mextops-allt.html	http://www.rsssf.com/tablesm/mextops-allt.html	k1gMnSc1
<g/>
↑	↑	k?
http://www.rsssf.com/miscellaneous/para-poy.html	http://www.rsssf.com/miscellaneous/para-poy.html	k1gMnSc1
<g/>
↑	↑	k?
http://www.rsssf.com/miscellaneous/sam-poy.html	http://www.rsssf.com/miscellaneous/sam-poy.html	k1gMnSc1
<g/>
↑	↑	k?
http://www.transfermarkt.co.uk/en/jose-cardozo/aufeinenblick/trainer_11893.html	http://www.transfermarkt.co.uk/en/jose-cardozo/aufeinenblick/trainer_11893.html	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
José	José	k6eAd1
Cardozo	Cardoza	k1gFnSc5
v	v	k7c6
databázi	databáze	k1gFnSc6
Olympedia	Olympedium	k1gNnSc2
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Medailisté	medailista	k1gMnPc1
-	-	kIx~
fotbal	fotbal	k1gInSc1
na	na	k7c6
Letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
2004	#num#	k4
Argentina	Argentina	k1gFnSc1
</s>
<s>
Roberto	Roberta	k1gFnSc5
Ayala	Ayalo	k1gNnSc2
•	•	k?
Nicolás	Nicolása	k1gFnPc2
Burdisso	Burdissa	k1gFnSc5
•	•	k?
Wilfredo	Wilfredo	k1gNnSc1
Caballero	caballero	k1gMnSc1
•	•	k?
Fabricio	Fabricio	k1gMnSc1
Coloccini	Coloccin	k1gMnPc1
•	•	k?
César	César	k1gMnSc1
Delgado	Delgada	k1gFnSc5
•	•	k?
Andrés	Andrésa	k1gFnPc2
D	D	kA
<g/>
'	'	kIx"
<g/>
Alessandro	Alessandra	k1gFnSc5
•	•	k?
Leandro	Leandra	k1gFnSc5
Fernández	Fernández	k1gInSc1
•	•	k?
Luciano	Luciana	k1gFnSc5
Figueroa	Figueroum	k1gNnPc1
•	•	k?
Kily	kilo	k1gNnPc7
González	Gonzáleza	k1gFnPc2
•	•	k?
Luis	Luisa	k1gFnPc2
González	González	k1gMnSc1
•	•	k?
Mariano	Mariana	k1gFnSc5
González	González	k1gMnSc1
•	•	k?
Gabriel	Gabriel	k1gMnSc1
Heinze	Heinze	k1gFnSc2
•	•	k?
Germán	Germán	k1gMnSc1
Lux	Lux	k1gMnSc1
•	•	k?
Javier	Javier	k1gMnSc1
Mascherano	Mascherana	k1gFnSc5
•	•	k?
Nicolás	Nicolás	k1gInSc1
Medina	Medina	k1gFnSc1
•	•	k?
Clemente	Clement	k1gInSc5
Rodríguez	Rodrígueza	k1gFnPc2
•	•	k?
Mauro	Mauro	k1gNnSc1
Rosales	Rosales	k1gMnSc1
•	•	k?
Javier	Javier	k1gInSc1
Saviola	Saviola	k1gFnSc1
•	•	k?
Carlos	Carlos	k1gMnSc1
TévezTrenér	TévezTrenér	k1gMnSc1
<g/>
:	:	kIx,
Marcelo	Marcela	k1gFnSc5
Bielsa	Biels	k1gMnSc4
Paraguay	Paraguay	k1gFnSc1
</s>
<s>
Rodrigo	Rodrigo	k6eAd1
Romero	Romero	k1gNnSc1
•	•	k?
Emilio	Emilio	k1gMnSc1
Martínez	Martínez	k1gMnSc1
•	•	k?
Julio	Julio	k1gMnSc1
Manzur	Manzur	k1gMnSc1
•	•	k?
Carlos	Carlos	k1gMnSc1
Gamarra	Gamarra	k1gMnSc1
•	•	k?
José	Josá	k1gFnSc2
Devaca	Devaca	k1gMnSc1
•	•	k?
Celso	Celsa	k1gFnSc5
Esquivel	Esquivela	k1gFnPc2
•	•	k?
Pablo	Pablo	k1gNnSc1
Giménez	Giménez	k1gMnSc1
•	•	k?
Edgar	Edgar	k1gMnSc1
Barreto	Barreto	k1gNnSc1
•	•	k?
Fredy	Fredy	k1gMnSc1
Barreiro	Barreiro	k1gNnSc1
•	•	k?
Diego	Diego	k1gMnSc1
Figueredo	Figueredo	k1gNnSc1
•	•	k?
Aureliano	Aureliana	k1gFnSc5
Torres	Torresa	k1gFnPc2
•	•	k?
Pedro	Pedro	k1gNnSc1
Benítez	Benítez	k1gMnSc1
•	•	k?
Julio	Julio	k1gMnSc1
César	César	k1gMnSc1
Enciso	Encisa	k1gFnSc5
•	•	k?
Julio	Julio	k1gMnSc1
González	González	k1gMnSc1
•	•	k?
Ernesto	Ernesta	k1gMnSc5
Cristaldo	Cristalda	k1gMnSc5
•	•	k?
Osvaldo	Osvaldo	k1gNnSc1
Díaz	Díaz	k1gMnSc1
•	•	k?
José	Josá	k1gFnSc2
Cardozo	Cardoza	k1gFnSc5
•	•	k?
Diego	Diego	k1gMnSc1
BarretoTrenér	BarretoTrenér	k1gMnSc1
<g/>
:	:	kIx,
Carlos	Carlos	k1gMnSc1
Jara	Jara	k1gMnSc1
Saguier	Saguier	k1gMnSc1
Itálie	Itálie	k1gFnSc2
</s>
<s>
Marco	Marco	k1gMnSc1
Amelia	Amelium	k1gNnSc2
•	•	k?
Andrea	Andrea	k1gFnSc1
Barzagli	Barzagle	k1gFnSc4
•	•	k?
Daniele	Daniela	k1gFnSc3
Bonera	Bonero	k1gNnSc2
•	•	k?
Cesare	Cesar	k1gMnSc5
Bovo	Bova	k1gMnSc5
•	•	k?
Giorgio	Giorgio	k6eAd1
Chiellini	Chiellin	k2eAgMnPc1d1
•	•	k?
Daniele	Daniela	k1gFnSc6
De	De	k?
Rossi	Rosse	k1gFnSc6
•	•	k?
Simone	Simon	k1gMnSc5
Del	Del	k1gMnSc5
Nero	Nero	k1gMnSc1
•	•	k?
Marco	Marco	k1gMnSc1
Donadel	Donadlo	k1gNnPc2
•	•	k?
Matteo	Matteo	k1gMnSc1
Ferrari	Ferrari	k1gMnSc1
•	•	k?
Andrea	Andrea	k1gFnSc1
Gasbarroni	Gasbarron	k1gMnPc1
•	•	k?
Alberto	Alberta	k1gFnSc5
Gilardino	Gilardin	k2eAgNnSc1d1
•	•	k?
Emiliano	Emiliana	k1gFnSc5
Moretti	Moretti	k1gNnPc7
•	•	k?
Giandomenico	Giandomenico	k1gMnSc1
Mesto	Mesto	k1gNnSc1
•	•	k?
Angelo	Angela	k1gFnSc5
Palombo	Palomba	k1gFnSc5
•	•	k?
Ivan	Ivan	k1gMnSc1
Pelizzoli	Pelizzole	k1gFnSc3
•	•	k?
Giampiero	Giampiero	k1gNnSc1
Pinzi	Pinh	k1gMnPc1
•	•	k?
Andrea	Andrea	k1gFnSc1
Pirlo	Pirlo	k1gNnSc1
•	•	k?
Giuseppe	Giusepp	k1gInSc5
SculliTrenér	SculliTrenér	k1gInSc4
<g/>
:	:	kIx,
Claudio	Claudia	k1gMnSc5
Gentile	Gentil	k1gMnSc5
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
</s>
