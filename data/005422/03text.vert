<s>
A	a	k9	a
je	být	k5eAaImIp3nS	být
první	první	k4xOgNnSc1	první
písmeno	písmeno	k1gNnSc1	písmeno
většiny	většina	k1gFnSc2	většina
abeced	abeceda	k1gFnPc2	abeceda
<g/>
,	,	kIx,	,
latinské	latinský	k2eAgFnSc2d1	Latinská
i	i	k8xC	i
české	český	k2eAgFnSc2d1	Česká
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
samohláska	samohláska	k1gFnSc1	samohláska
<g/>
.	.	kIx.	.
</s>
<s>
Podoba	podoba	k1gFnSc1	podoba
písmene	písmeno	k1gNnSc2	písmeno
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
starověkých	starověký	k2eAgInPc2d1	starověký
pasteveckých	pastevecký	k2eAgInPc2d1	pastevecký
kultů	kult	k1gInPc2	kult
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
piktogramu	piktogram	k1gInSc2	piktogram
býčí	býčí	k2eAgFnSc2d1	býčí
hlavy	hlava	k1gFnSc2	hlava
se	se	k3xPyFc4	se
v	v	k7c6	v
protosinajském	protosinajský	k2eAgNnSc6d1	protosinajský
písmu	písmo	k1gNnSc6	písmo
vyvinula	vyvinout	k5eAaPmAgFnS	vyvinout
stylizovaná	stylizovaný	k2eAgFnSc1d1	stylizovaná
podoba	podoba	k1gFnSc1	podoba
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
postaveného	postavený	k2eAgInSc2d1	postavený
na	na	k7c4	na
špičku	špička	k1gFnSc4	špička
a	a	k8xC	a
zakončeného	zakončený	k2eAgInSc2d1	zakončený
rohy	roh	k1gInPc7	roh
<g/>
.	.	kIx.	.
</s>
<s>
Tu	ten	k3xDgFnSc4	ten
přejali	přejmout	k5eAaPmAgMnP	přejmout
Féničané	Féničan	k1gMnPc1	Féničan
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
pootočené	pootočený	k2eAgFnPc1d1	pootočená
o	o	k7c4	o
devadesát	devadesát	k4xCc4	devadesát
stupňů	stupeň	k1gInPc2	stupeň
<g/>
.	.	kIx.	.
</s>
<s>
Znak	znak	k1gInSc1	znak
dostal	dostat	k5eAaPmAgInS	dostat
název	název	k1gInSc4	název
aluph	alupha	k1gFnPc2	alupha
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
vládce	vládce	k1gMnSc4	vládce
<g/>
"	"	kIx"	"
a	a	k8xC	a
dokazuje	dokazovat	k5eAaImIp3nS	dokazovat
význam	význam	k1gInSc4	význam
hovězího	hovězí	k2eAgInSc2d1	hovězí
dobytka	dobytek	k1gInSc2	dobytek
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
společnosti	společnost	k1gFnSc6	společnost
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
také	také	k9	také
prvním	první	k4xOgNnSc7	první
písmenem	písmeno	k1gNnSc7	písmeno
abecedy	abeceda	k1gFnSc2	abeceda
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
něj	on	k3xPp3gNnSc2	on
se	se	k3xPyFc4	se
vyvinulo	vyvinout	k5eAaPmAgNnS	vyvinout
hebrejské	hebrejský	k2eAgNnSc1d1	hebrejské
písmeno	písmeno	k1gNnSc1	písmeno
alef	alef	k1gInSc1	alef
<g/>
,	,	kIx,	,
řecké	řecký	k2eAgNnSc1d1	řecké
alfa	alfa	k1gNnSc1	alfa
a	a	k8xC	a
latinské	latinský	k2eAgInPc1d1	latinský
A	A	kA	A
<g/>
,	,	kIx,	,
posunuté	posunutý	k2eAgFnSc2d1	posunutá
o	o	k7c4	o
dalších	další	k2eAgInPc2d1	další
devadesát	devadesát	k4xCc4	devadesát
stupňů	stupeň	k1gInPc2	stupeň
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
rohy	roh	k1gInPc1	roh
směřují	směřovat	k5eAaImIp3nP	směřovat
dolů	dolů	k6eAd1	dolů
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
je	být	k5eAaImIp3nS	být
jiný	jiný	k2eAgInSc1d1	jiný
znak	znak	k1gInSc1	znak
<g/>
,	,	kIx,	,
než	než	k8xS	než
A	a	k9	a
s	s	k7c7	s
kroužkem	kroužek	k1gInSc7	kroužek
Související	související	k2eAgFnSc2d1	související
informace	informace	k1gFnSc2	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Alfa	alfa	k1gFnSc1	alfa
<g/>
.	.	kIx.	.
řazeno	řazen	k2eAgNnSc1d1	řazeno
podle	podle	k7c2	podle
pořadí	pořadí	k1gNnSc2	pořadí
v	v	k7c6	v
Unicode	Unicod	k1gInSc5	Unicod
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
A	a	k8xC	a
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commons	k1gInSc4	Commons
Galerie	galerie	k1gFnSc2	galerie
A	a	k9	a
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc4d1	slovníkové
heslo	heslo	k1gNnSc4	heslo
a	a	k8xC	a
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
