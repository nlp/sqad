<s>
Rammstein	Rammstein	k1gInSc1	Rammstein
je	být	k5eAaImIp3nS	být
německá	německý	k2eAgFnSc1d1	německá
hudební	hudební	k2eAgFnSc1d1	hudební
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Členové	člen	k1gMnPc1	člen
skupiny	skupina	k1gFnSc2	skupina
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
bývalé	bývalý	k2eAgFnSc2d1	bývalá
NDR	NDR	kA	NDR
<g/>
.	.	kIx.	.
</s>
<s>
Rammstein	Rammstein	k2eAgMnSc1d1	Rammstein
bývají	bývat	k5eAaImIp3nP	bývat
zařazováni	zařazovat	k5eAaImNgMnP	zařazovat
do	do	k7c2	do
hudebních	hudební	k2eAgInPc2d1	hudební
stylů	styl	k1gInPc2	styl
Neue	Neuus	k1gMnSc5	Neuus
Deutsche	Deutschus	k1gMnSc5	Deutschus
Härte	Härt	k1gMnSc5	Härt
<g/>
,	,	kIx,	,
industrial	industriat	k5eAaPmAgInS	industriat
metal	metal	k1gInSc1	metal
s	s	k7c7	s
prvky	prvek	k1gInPc7	prvek
elektronické	elektronický	k2eAgFnSc2d1	elektronická
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
textů	text	k1gInPc2	text
skupiny	skupina	k1gFnSc2	skupina
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
3	[number]	k4	3
albech	album	k1gNnPc6	album
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
malé	malý	k2eAgFnSc6d1	malá
míře	míra	k1gFnSc6	míra
objevují	objevovat	k5eAaImIp3nP	objevovat
i	i	k9	i
další	další	k2eAgInPc1d1	další
jazyky	jazyk	k1gInPc1	jazyk
(	(	kIx(	(
<g/>
angličtina	angličtina	k1gFnSc1	angličtina
<g/>
,	,	kIx,	,
francouzština	francouzština	k1gFnSc1	francouzština
<g/>
,	,	kIx,	,
ruština	ruština	k1gFnSc1	ruština
<g/>
,	,	kIx,	,
jedna	jeden	k4xCgFnSc1	jeden
skladba	skladba	k1gFnSc1	skladba
je	být	k5eAaImIp3nS	být
celá	celý	k2eAgFnSc1d1	celá
ve	v	k7c6	v
španělštině	španělština	k1gFnSc6	španělština
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
zformovala	zformovat	k5eAaPmAgFnS	zformovat
počátkem	počátkem	k7c2	počátkem
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc4d1	významná
úlohu	úloha	k1gFnSc4	úloha
při	při	k7c6	při
jejím	její	k3xOp3gInSc6	její
zrodu	zrod	k1gInSc6	zrod
sehrál	sehrát	k5eAaPmAgMnS	sehrát
kytarista	kytarista	k1gMnSc1	kytarista
Richard	Richard	k1gMnSc1	Richard
Kruspe	Krusp	k1gMnSc5	Krusp
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
působil	působit	k5eAaImAgMnS	působit
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Paulem	Paul	k1gMnSc7	Paul
Landersem	Landers	k1gMnSc7	Landers
a	a	k8xC	a
Tillem	Till	k1gMnSc7	Till
Lindemannem	Lindemann	k1gMnSc7	Lindemann
ve	v	k7c6	v
Schwerinu	Schwerin	k1gInSc6	Schwerin
v	v	k7c6	v
punk-rockové	punkockový	k2eAgFnSc6d1	punk-rocková
skupině	skupina	k1gFnSc6	skupina
First	First	k1gMnSc1	First
Arsch	Arsch	k1gMnSc1	Arsch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
utekl	utéct	k5eAaPmAgMnS	utéct
do	do	k7c2	do
Rakouska	Rakousko	k1gNnSc2	Rakousko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
železné	železný	k2eAgFnSc2d1	železná
opony	opona	k1gFnSc2	opona
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgInS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Schwerinu	Schwerin	k1gInSc2	Schwerin
a	a	k8xC	a
bydlel	bydlet	k5eAaImAgMnS	bydlet
společně	společně	k6eAd1	společně
s	s	k7c7	s
bubeníkem	bubeník	k1gMnSc7	bubeník
Christophem	Christoph	k1gInSc7	Christoph
Schneiderem	Schneider	k1gMnSc7	Schneider
a	a	k8xC	a
baskytaristou	baskytarista	k1gMnSc7	baskytarista
Oliverem	Oliver	k1gMnSc7	Oliver
Riedelem	Riedel	k1gMnSc7	Riedel
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
tři	tři	k4xCgMnPc1	tři
hudebníci	hudebník	k1gMnPc1	hudebník
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
založí	založit	k5eAaPmIp3nP	založit
nový	nový	k2eAgInSc4d1	nový
hudební	hudební	k2eAgInSc4d1	hudební
projekt	projekt	k1gInSc4	projekt
<g/>
,	,	kIx,	,
a	a	k8xC	a
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
tak	tak	k6eAd1	tak
zárodek	zárodek	k1gInSc4	zárodek
skupiny	skupina	k1gFnSc2	skupina
Rammstein	Rammsteina	k1gFnPc2	Rammsteina
<g/>
.	.	kIx.	.
</s>
<s>
Kruspe	Kruspat	k5eAaPmIp3nS	Kruspat
brzy	brzy	k6eAd1	brzy
přivedl	přivést	k5eAaPmAgMnS	přivést
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
Lindemanna	Lindemanno	k1gNnSc2	Lindemanno
jako	jako	k8xS	jako
čtvrtého	čtvrtý	k4xOgMnSc2	čtvrtý
člena	člen	k1gMnSc2	člen
<g/>
.	.	kIx.	.
</s>
<s>
Lindemann	Lindemann	k1gMnSc1	Lindemann
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
jinak	jinak	k6eAd1	jinak
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
pletač	pletač	k1gMnSc1	pletač
košů	koš	k1gInPc2	koš
<g/>
,	,	kIx,	,
přijal	přijmout	k5eAaPmAgInS	přijmout
úlohu	úloha	k1gFnSc4	úloha
zpěváka	zpěvák	k1gMnSc2	zpěvák
a	a	k8xC	a
textaře	textař	k1gMnSc2	textař
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1995	[number]	k4	1995
se	se	k3xPyFc4	se
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
zúčastnili	zúčastnit	k5eAaPmAgMnP	zúčastnit
soutěže	soutěž	k1gFnPc4	soutěž
pro	pro	k7c4	pro
mladé	mladý	k1gMnPc4	mladý
hudební	hudební	k2eAgFnSc2d1	hudební
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
jako	jako	k9	jako
vítězům	vítěz	k1gMnPc3	vítěz
jim	on	k3xPp3gMnPc3	on
bylo	být	k5eAaImAgNnS	být
umožněno	umožnit	k5eAaPmNgNnS	umožnit
vytvořit	vytvořit	k5eAaPmF	vytvořit
demonahrávku	demonahrávka	k1gFnSc4	demonahrávka
o	o	k7c6	o
čtyřech	čtyři	k4xCgFnPc6	čtyři
skladbách	skladba	k1gFnPc6	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Rammstein	Rammstein	k1gMnSc1	Rammstein
tehdy	tehdy	k6eAd1	tehdy
nahráli	nahrát	k5eAaPmAgMnP	nahrát
Das	Das	k1gMnPc1	Das
alte	alt	k1gInSc5	alt
Leid	Leid	k1gMnSc1	Leid
<g/>
,	,	kIx,	,
Seemann	Seemann	k1gMnSc1	Seemann
<g/>
,	,	kIx,	,
Weißes	Weißes	k1gMnSc1	Weißes
Fleisch	Fleisch	k1gMnSc1	Fleisch
a	a	k8xC	a
Rammstein	Rammstein	k1gMnSc1	Rammstein
(	(	kIx(	(
<g/>
písně	píseň	k1gFnPc1	píseň
však	však	k9	však
nebyly	být	k5eNaImAgFnP	být
totožné	totožný	k2eAgFnPc1d1	totožná
s	s	k7c7	s
těmi	ten	k3xDgMnPc7	ten
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
stejným	stejný	k2eAgInSc7d1	stejný
názvem	název	k1gInSc7	název
objevily	objevit	k5eAaPmAgFnP	objevit
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
albu	album	k1gNnSc6	album
Herzeleid	Herzeleida	k1gFnPc2	Herzeleida
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
úspěchu	úspěch	k1gInSc6	úspěch
získala	získat	k5eAaPmAgFnS	získat
skupina	skupina	k1gFnSc1	skupina
další	další	k2eAgMnPc4d1	další
členy	člen	k1gMnPc4	člen
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
přidali	přidat	k5eAaPmAgMnP	přidat
Paul	Paul	k1gMnSc1	Paul
Landers	Landers	k1gInSc4	Landers
jako	jako	k8xS	jako
druhý	druhý	k4xOgMnSc1	druhý
kytarista	kytarista	k1gMnSc1	kytarista
a	a	k8xC	a
Christian	Christian	k1gMnSc1	Christian
Lorenz	Lorenz	k1gMnSc1	Lorenz
jako	jako	k8xC	jako
klávesista	klávesista	k1gMnSc1	klávesista
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgMnPc1	dva
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
hráli	hrát	k5eAaImAgMnP	hrát
ve	v	k7c6	v
skupině	skupina	k1gFnSc6	skupina
Feeling	Feeling	k1gInSc1	Feeling
B.	B.	kA	B.
Zatímco	zatímco	k8xS	zatímco
Paul	Paul	k1gMnSc1	Paul
Landers	Landersa	k1gFnPc2	Landersa
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgInS	připojit
hned	hned	k6eAd1	hned
po	po	k7c6	po
poslechu	poslech	k1gInSc6	poslech
nahrávky	nahrávka	k1gFnSc2	nahrávka
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
Lorenz	Lorenz	k1gMnSc1	Lorenz
zpočátku	zpočátku	k6eAd1	zpočátku
nebyl	být	k5eNaImAgMnS	být
tak	tak	k6eAd1	tak
nadšený	nadšený	k2eAgInSc4d1	nadšený
a	a	k8xC	a
nějakou	nějaký	k3yIgFnSc4	nějaký
dobu	doba	k1gFnSc4	doba
váhal	váhat	k5eAaImAgMnS	váhat
<g/>
.	.	kIx.	.
</s>
<s>
Složení	složení	k1gNnSc1	složení
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nezměnilo	změnit	k5eNaPmAgNnS	změnit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
vznikají	vznikat	k5eAaImIp3nP	vznikat
i	i	k9	i
další	další	k2eAgFnPc1d1	další
demonahrávky	demonahrávka	k1gFnPc1	demonahrávka
<g/>
,	,	kIx,	,
např.	např.	kA	např.
Schwarzes	Schwarzes	k1gInSc1	Schwarzes
Glas	Glas	k1gInSc1	Glas
<g/>
,	,	kIx,	,
Jeder	Jeder	k1gMnSc1	Jeder
lacht	lacht	k1gMnSc1	lacht
<g/>
,	,	kIx,	,
Feuerräder	Feuerräder	k1gMnSc1	Feuerräder
a	a	k8xC	a
Wilder	Wilder	k1gMnSc1	Wilder
Wein	Wein	k1gMnSc1	Wein
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1995	[number]	k4	1995
začala	začít	k5eAaPmAgFnS	začít
skupina	skupina	k1gFnSc1	skupina
ve	v	k7c6	v
stockholmském	stockholmský	k2eAgNnSc6d1	Stockholmské
studiu	studio	k1gNnSc6	studio
Polar	Polara	k1gFnPc2	Polara
Studio	studio	k1gNnSc4	studio
nahrávat	nahrávat	k5eAaImF	nahrávat
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
s	s	k7c7	s
producenty	producent	k1gMnPc7	producent
Jacobem	Jacob	k1gMnSc7	Jacob
Hellnerem	Hellner	k1gMnSc7	Hellner
a	a	k8xC	a
C.	C.	kA	C.
<g/>
-	-	kIx~	-
<g/>
M.	M.	kA	M.
Herlöffsonem	Herlöffson	k1gInSc7	Herlöffson
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
případě	případ	k1gInSc6	případ
Jacoba	Jacoba	k1gFnSc1	Jacoba
Hellnera	Hellner	k1gMnSc2	Hellner
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
počátek	počátek	k1gInSc4	počátek
dlouhodobé	dlouhodobý	k2eAgFnSc2d1	dlouhodobá
spolupráce	spolupráce	k1gFnSc2	spolupráce
<g/>
,	,	kIx,	,
podílel	podílet	k5eAaImAgMnS	podílet
se	se	k3xPyFc4	se
i	i	k9	i
na	na	k7c4	na
produkci	produkce	k1gFnSc4	produkce
všech	všecek	k3xTgNnPc2	všecek
následujících	následující	k2eAgNnPc2d1	následující
studiových	studiový	k2eAgNnPc2d1	studiové
alb	album	k1gNnPc2	album
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
byl	být	k5eAaImAgInS	být
uveřejněn	uveřejnit	k5eAaPmNgInS	uveřejnit
první	první	k4xOgInSc1	první
singl	singl	k1gInSc1	singl
<g/>
,	,	kIx,	,
Du	Du	k?	Du
riechst	riechst	k1gInSc1	riechst
so	so	k?	so
gut	gut	k?	gut
<g/>
,	,	kIx,	,
a	a	k8xC	a
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
celé	celý	k2eAgNnSc1d1	celé
debutové	debutový	k2eAgNnSc1d1	debutové
album	album	k1gNnSc1	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Herzeleid	Herzeleida	k1gFnPc2	Herzeleida
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
singl	singl	k1gInSc1	singl
z	z	k7c2	z
tohoto	tento	k3xDgNnSc2	tento
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
balada	balada	k1gFnSc1	balada
Seemann	Seemann	k1gInSc1	Seemann
<g/>
,	,	kIx,	,
vyšel	vyjít	k5eAaPmAgInS	vyjít
v	v	k7c6	v
příštím	příští	k2eAgInSc6d1	příští
roce	rok	k1gInSc6	rok
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
byl	být	k5eAaImAgInS	být
s	s	k7c7	s
odstupem	odstup	k1gInSc7	odstup
vydán	vydat	k5eAaPmNgInS	vydat
ještě	ještě	k9	ještě
singl	singl	k1gInSc1	singl
Asche	Asch	k1gMnSc2	Asch
zu	zu	k?	zu
Asche	Asch	k1gMnSc2	Asch
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
5	[number]	k4	5
živými	živý	k2eAgFnPc7d1	živá
nahrávkami	nahrávka	k1gFnPc7	nahrávka
z	z	k7c2	z
DVD	DVD	kA	DVD
Live	Live	k1gFnPc2	Live
aus	aus	k?	aus
Berlin	berlina	k1gFnPc2	berlina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Herzeleid	Herzeleida	k1gFnPc2	Herzeleida
se	se	k3xPyFc4	se
dostalo	dostat	k5eAaPmAgNnS	dostat
nejvýše	vysoce	k6eAd3	vysoce
na	na	k7c4	na
6	[number]	k4	6
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
hitparádě	hitparáda	k1gFnSc6	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
vydáním	vydání	k1gNnSc7	vydání
prvního	první	k4xOgNnSc2	první
alba	album	k1gNnSc2	album
začaly	začít	k5eAaPmAgFnP	začít
zaznívat	zaznívat	k5eAaImF	zaznívat
první	první	k4xOgInPc4	první
kritické	kritický	k2eAgInPc4d1	kritický
hlasy	hlas	k1gInPc4	hlas
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
skupinu	skupina	k1gFnSc4	skupina
fašisticky	fašisticky	k6eAd1	fašisticky
orientovanou	orientovaný	k2eAgFnSc4d1	orientovaná
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
důkaz	důkaz	k1gInSc1	důkaz
často	často	k6eAd1	často
sloužil	sloužit	k5eAaImAgInS	sloužit
zdánlivě	zdánlivě	k6eAd1	zdánlivě
nevinný	vinný	k2eNgInSc4d1	nevinný
obal	obal	k1gInSc4	obal
prvního	první	k4xOgInSc2	první
CD	CD	kA	CD
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
prý	prý	k9	prý
zobrazuje	zobrazovat	k5eAaImIp3nS	zobrazovat
postavy	postava	k1gFnPc4	postava
čisté	čistý	k2eAgFnSc2d1	čistá
árijské	árijský	k2eAgFnSc2d1	árijská
rasy	rasa	k1gFnSc2	rasa
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
pochopitelně	pochopitelně	k6eAd1	pochopitelně
o	o	k7c4	o
nesmysl	nesmysl	k1gInSc4	nesmysl
a	a	k8xC	a
sami	sám	k3xTgMnPc1	sám
Rammstein	Rammstein	k1gInSc4	Rammstein
uvedli	uvést	k5eAaPmAgMnP	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
rasismem	rasismus	k1gInSc7	rasismus
nemají	mít	k5eNaImIp3nP	mít
a	a	k8xC	a
nechtějí	chtít	k5eNaImIp3nP	chtít
mít	mít	k5eAaImF	mít
nic	nic	k3yNnSc1	nic
společného	společný	k2eAgNnSc2d1	společné
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
alba	album	k1gNnSc2	album
následovaly	následovat	k5eAaImAgInP	následovat
četné	četný	k2eAgInPc1d1	četný
koncerty	koncert	k1gInPc1	koncert
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
i	i	k8xC	i
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
absolvovali	absolvovat	k5eAaPmAgMnP	absolvovat
tour	tour	k1gMnSc1	tour
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Project	Project	k2eAgInSc1d1	Project
Pitchfork	Pitchfork	k1gInSc1	Pitchfork
<g/>
,	,	kIx,	,
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1995	[number]	k4	1995
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
Rammstein	Rammstein	k2eAgInSc4d1	Rammstein
ve	v	k7c6	v
Varšavě	Varšava	k1gFnSc6	Varšava
a	a	k8xC	a
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
jako	jako	k8xS	jako
předskupina	předskupina	k1gFnSc1	předskupina
švédských	švédský	k2eAgFnPc2d1	švédská
Clawfinger	Clawfingra	k1gFnPc2	Clawfingra
<g/>
,	,	kIx,	,
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
pak	pak	k6eAd1	pak
následovala	následovat	k5eAaImAgFnS	následovat
série	série	k1gFnSc1	série
17	[number]	k4	17
koncertů	koncert	k1gInPc2	koncert
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1996	[number]	k4	1996
odehráli	odehrát	k5eAaPmAgMnP	odehrát
další	další	k2eAgInSc4d1	další
koncert	koncert	k1gInSc4	koncert
s	s	k7c7	s
Clawfinger	Clawfingero	k1gNnPc2	Clawfingero
a	a	k8xC	a
poté	poté	k6eAd1	poté
i	i	k9	i
s	s	k7c7	s
Ramones	Ramones	k1gInSc1	Ramones
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
na	na	k7c6	na
koncertní	koncertní	k2eAgFnSc6d1	koncertní
šňůře	šňůra	k1gFnSc6	šňůra
s	s	k7c7	s
posledním	poslední	k2eAgNnSc7d1	poslední
albem	album	k1gNnSc7	album
¡	¡	k?	¡
<g/>
Adios	Adios	k1gMnSc1	Adios
Amigos	Amigos	k1gMnSc1	Amigos
<g/>
!	!	kIx.	!
</s>
<s>
a	a	k8xC	a
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
ukončili	ukončit	k5eAaPmAgMnP	ukončit
činnost	činnost	k1gFnSc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
absolvovali	absolvovat	k5eAaPmAgMnP	absolvovat
Rammstein	Rammstein	k1gInSc4	Rammstein
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
živý	živý	k2eAgInSc4d1	živý
vstup	vstup	k1gInSc4	vstup
na	na	k7c6	na
MTV	MTV	kA	MTV
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
září	září	k1gNnSc2	září
a	a	k8xC	a
října	říjen	k1gInSc2	říjen
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
série	série	k1gFnSc1	série
koncertů	koncert	k1gInPc2	koncert
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Rakousku	Rakousko	k1gNnSc6	Rakousko
a	a	k8xC	a
Švýcarsku	Švýcarsko	k1gNnSc6	Švýcarsko
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
skupiny	skupina	k1gFnSc2	skupina
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
také	také	k9	také
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Lost	Losta	k1gFnPc2	Losta
Highway	Highwaa	k1gFnSc2	Highwaa
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
David	David	k1gMnSc1	David
Lynch	Lynch	k1gMnSc1	Lynch
si	se	k3xPyFc3	se
z	z	k7c2	z
alba	album	k1gNnSc2	album
Herzeleid	Herzeleida	k1gFnPc2	Herzeleida
vybral	vybrat	k5eAaPmAgInS	vybrat
písně	píseň	k1gFnPc4	píseň
Heirate	Heirat	k1gInSc5	Heirat
mich	mich	k1gMnSc1	mich
a	a	k8xC	a
Rammstein	Rammstein	k1gMnSc1	Rammstein
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1997	[number]	k4	1997
vyšel	vyjít	k5eAaPmAgInS	vyjít
singl	singl	k1gInSc4	singl
Engel	Engela	k1gFnPc2	Engela
z	z	k7c2	z
nového	nový	k2eAgNnSc2d1	nové
alba	album	k1gNnSc2	album
Sehnsucht	Sehnsuchta	k1gFnPc2	Sehnsuchta
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
nahráno	nahrát	k5eAaPmNgNnS	nahrát
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
předchozího	předchozí	k2eAgInSc2d1	předchozí
roku	rok	k1gInSc2	rok
v	v	k7c6	v
Temple	templ	k1gInSc5	templ
Studios	Studios	k?	Studios
na	na	k7c6	na
Maltě	Malta	k1gFnSc6	Malta
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
23	[number]	k4	23
<g/>
.	.	kIx.	.
květnu	květen	k1gInSc6	květen
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
prodat	prodat	k5eAaPmF	prodat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
250	[number]	k4	250
000	[number]	k4	000
kopií	kopie	k1gFnPc2	kopie
singlu	singl	k1gInSc2	singl
a	a	k8xC	a
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
fanouškovská	fanouškovský	k2eAgFnSc1d1	fanouškovská
verze	verze	k1gFnSc1	verze
(	(	kIx(	(
<g/>
Engel-Fan-Edition	Engel-Fan-Edition	k1gInSc1	Engel-Fan-Edition
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
hitparádě	hitparáda	k1gFnSc6	hitparáda
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgInS	dostat
na	na	k7c4	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
nejvýše	nejvýše	k6eAd1	nejvýše
z	z	k7c2	z
dosud	dosud	k6eAd1	dosud
vydaných	vydaný	k2eAgInPc2d1	vydaný
singlů	singl	k1gInPc2	singl
skupiny	skupina	k1gFnSc2	skupina
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
následoval	následovat	k5eAaImAgInS	následovat
singl	singl	k1gInSc1	singl
Du	Du	k?	Du
hast	hast	k1gInSc1	hast
<g/>
.	.	kIx.	.
</s>
<s>
Text	text	k1gInSc1	text
této	tento	k3xDgFnSc2	tento
písně	píseň	k1gFnSc2	píseň
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
založen	založit	k5eAaPmNgInS	založit
na	na	k7c6	na
slovní	slovní	k2eAgFnSc6d1	slovní
hříčce	hříčka	k1gFnSc6	hříčka
se	s	k7c7	s
slovesem	sloveso	k1gNnSc7	sloveso
haben	habna	k1gFnPc2	habna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
byla	být	k5eAaImAgFnS	být
nominována	nominovat	k5eAaBmNgFnS	nominovat
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
MTV	MTV	kA	MTV
Europe	Europ	k1gInSc5	Europ
Music	Musice	k1gInPc2	Musice
Awards	Awards	k1gInSc4	Awards
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
rock	rock	k1gInSc1	rock
(	(	kIx(	(
<g/>
Best	Best	k2eAgInSc1d1	Best
Rock	rock	k1gInSc1	rock
Act	Act	k1gFnSc2	Act
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Best	Best	k2eAgInSc4d1	Best
Metal	metal	k1gInSc4	metal
Performance	performance	k1gFnSc2	performance
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
metalový	metalový	k2eAgInSc4d1	metalový
výkon	výkon	k1gInSc4	výkon
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
kategorii	kategorie	k1gFnSc6	kategorie
ovšem	ovšem	k9	ovšem
nezvítězila	zvítězit	k5eNaPmAgFnS	zvítězit
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Sehnsucht	Sehnsuchta	k1gFnPc2	Sehnsuchta
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
22	[number]	k4	22
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1997	[number]	k4	1997
a	a	k8xC	a
zaznamenalo	zaznamenat	k5eAaPmAgNnS	zaznamenat
nejvýraznější	výrazný	k2eAgInSc4d3	nejvýraznější
úspěch	úspěch	k1gInSc4	úspěch
v	v	k7c6	v
německy	německy	k6eAd1	německy
mluvících	mluvící	k2eAgFnPc6d1	mluvící
zemích	zem	k1gFnPc6	zem
(	(	kIx(	(
<g/>
ovládlo	ovládnout	k5eAaPmAgNnS	ovládnout
německou	německý	k2eAgFnSc4d1	německá
a	a	k8xC	a
rakouskou	rakouský	k2eAgFnSc4d1	rakouská
hitparádu	hitparáda	k1gFnSc4	hitparáda
<g/>
,	,	kIx,	,
ve	v	k7c6	v
švýcarské	švýcarský	k2eAgFnSc2d1	švýcarská
bylo	být	k5eAaImAgNnS	být
nejvýše	nejvýše	k6eAd1	nejvýše
na	na	k7c4	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
21	[number]	k4	21
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1997	[number]	k4	1997
uveřejňují	uveřejňovat	k5eAaImIp3nP	uveřejňovat
Rammstein	Rammstein	k2eAgInSc4d1	Rammstein
singl	singl	k1gInSc4	singl
Das	Das	k1gMnSc1	Das
Modell	Modell	k1gMnSc1	Modell
<g/>
,	,	kIx,	,
vlastní	vlastní	k2eAgFnSc4d1	vlastní
verzi	verze	k1gFnSc4	verze
písně	píseň	k1gFnSc2	píseň
Das	Das	k1gFnSc2	Das
Model	modla	k1gFnPc2	modla
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
původně	původně	k6eAd1	původně
nahrála	nahrát	k5eAaBmAgFnS	nahrát
skupina	skupina	k1gFnSc1	skupina
Kraftwerk	Kraftwerk	k1gInSc1	Kraftwerk
<g/>
,	,	kIx,	,
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
následuje	následovat	k5eAaImIp3nS	následovat
tour	tour	k1gInSc1	tour
v	v	k7c6	v
USA	USA	kA	USA
(	(	kIx(	(
<g/>
jako	jako	k8xC	jako
podpora	podpora	k1gFnSc1	podpora
skupiny	skupina	k1gFnSc2	skupina
KMFDM	KMFDM	kA	KMFDM
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
dubna	duben	k1gInSc2	duben
a	a	k8xC	a
května	květen	k1gInSc2	květen
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
uskutečňují	uskutečňovat	k5eAaImIp3nP	uskutečňovat
další	další	k2eAgInSc4d1	další
tour	tour	k1gInSc4	tour
v	v	k7c6	v
USA	USA	kA	USA
(	(	kIx(	(
<g/>
poprvé	poprvé	k6eAd1	poprvé
jako	jako	k8xS	jako
hlavní	hlavní	k2eAgFnSc1d1	hlavní
skupina	skupina	k1gFnSc1	skupina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
pak	pak	k6eAd1	pak
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1998	[number]	k4	1998
vychází	vycházet	k5eAaImIp3nS	vycházet
nová	nový	k2eAgFnSc1d1	nová
verze	verze	k1gFnSc1	verze
singlu	singl	k1gInSc2	singl
Du	Du	k?	Du
riechst	riechst	k1gMnSc1	riechst
so	so	k?	so
gut	gut	k?	gut
(	(	kIx(	(
<g/>
Du	Du	k?	Du
riechst	riechst	k1gInSc1	riechst
so	so	k?	so
gut	gut	k?	gut
'	'	kIx"	'
<g/>
98	[number]	k4	98
<g/>
)	)	kIx)	)
obsahující	obsahující	k2eAgInSc1d1	obsahující
také	také	k9	také
několik	několik	k4yIc4	několik
remixů	remix	k1gInPc2	remix
této	tento	k3xDgFnSc2	tento
písně	píseň	k1gFnSc2	píseň
od	od	k7c2	od
jiných	jiný	k2eAgMnPc2d1	jiný
interpretů	interpret	k1gMnPc2	interpret
<g/>
.	.	kIx.	.
27	[number]	k4	27
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
pak	pak	k6eAd1	pak
Rammstein	Rammstein	k1gInSc4	Rammstein
vydávají	vydávat	k5eAaPmIp3nP	vydávat
singl	singl	k1gInSc4	singl
Stripped	Stripped	k1gInSc1	Stripped
<g/>
,	,	kIx,	,
coververzi	coververze	k1gFnSc4	coververze
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
písně	píseň	k1gFnSc2	píseň
od	od	k7c2	od
Depeche	Depech	k1gInSc2	Depech
Mode	modus	k1gInSc5	modus
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
1998	[number]	k4	1998
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
účastní	účastnit	k5eAaImIp3nS	účastnit
Family	Famil	k1gMnPc4	Famil
Values	Values	k1gMnSc1	Values
Tour	Tour	k1gMnSc1	Tour
v	v	k7c6	v
USA	USA	kA	USA
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
interprety	interpret	k1gMnPc7	interpret
Korn	Korn	k1gInSc4	Korn
<g/>
,	,	kIx,	,
Ice	Ice	k1gFnSc4	Ice
Cube	Cube	k1gNnSc2	Cube
<g/>
,	,	kIx,	,
Orgy	Orga	k1gFnSc2	Orga
a	a	k8xC	a
Limp	limpa	k1gFnPc2	limpa
Bizkit	Bizkita	k1gFnPc2	Bizkita
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dubnu	duben	k1gInSc6	duben
1999	[number]	k4	1999
doprovázejí	doprovázet	k5eAaImIp3nP	doprovázet
Rammstein	Rammstein	k1gMnSc1	Rammstein
skupinu	skupina	k1gFnSc4	skupina
Kiss	Kissa	k1gFnPc2	Kissa
na	na	k7c6	na
koncertech	koncert	k1gInPc6	koncert
v	v	k7c6	v
Latinské	latinský	k2eAgFnSc6d1	Latinská
Americe	Amerika	k1gFnSc6	Amerika
a	a	k8xC	a
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
absolvují	absolvovat	k5eAaPmIp3nP	absolvovat
vlastní	vlastní	k2eAgInSc4d1	vlastní
tour	tour	k1gInSc4	tour
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
vydávají	vydávat	k5eAaPmIp3nP	vydávat
Rammstein	Rammstein	k2eAgInSc4d1	Rammstein
své	svůj	k3xOyFgNnSc4	svůj
první	první	k4xOgNnSc4	první
živé	živý	k2eAgNnSc4d1	živé
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
Live	Live	k1gFnSc4	Live
aus	aus	k?	aus
Berlin	berlina	k1gFnPc2	berlina
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
vytvořeno	vytvořit	k5eAaPmNgNnS	vytvořit
ze	z	k7c2	z
záznamů	záznam	k1gInPc2	záznam
koncertů	koncert	k1gInPc2	koncert
z	z	k7c2	z
22	[number]	k4	22
<g/>
.	.	kIx.	.
a	a	k8xC	a
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
odehrály	odehrát	k5eAaPmAgInP	odehrát
ve	v	k7c6	v
Wuhlheide	Wuhlheid	k1gInSc5	Wuhlheid
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
Šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
dosud	dosud	k6eAd1	dosud
největší	veliký	k2eAgNnSc4d3	veliký
vystoupení	vystoupení	k1gNnSc4	vystoupení
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
ho	on	k3xPp3gMnSc4	on
přes	přes	k7c4	přes
17	[number]	k4	17
000	[number]	k4	000
fanoušků	fanoušek	k1gMnPc2	fanoušek
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc4d1	původní
CD	CD	kA	CD
verzi	verze	k1gFnSc4	verze
doplnila	doplnit	k5eAaPmAgFnS	doplnit
v	v	k7c6	v
září	září	k1gNnSc6	září
videokazeta	videokazeta	k1gFnSc1	videokazeta
a	a	k8xC	a
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
DVD	DVD	kA	DVD
<g/>
.	.	kIx.	.
</s>
<s>
Videokazeta	videokazeta	k1gFnSc1	videokazeta
byla	být	k5eAaImAgFnS	být
k	k	k7c3	k
dostání	dostání	k1gNnSc3	dostání
v	v	k7c6	v
cenzurované	cenzurovaný	k2eAgFnSc6d1	cenzurovaná
a	a	k8xC	a
necenzurované	cenzurovaný	k2eNgFnSc6d1	necenzurovaná
verzi	verze	k1gFnSc6	verze
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
cenzurovaná	cenzurovaný	k2eAgFnSc1d1	cenzurovaná
verze	verze	k1gFnSc1	verze
neobsahovala	obsahovat	k5eNaImAgFnS	obsahovat
píseň	píseň	k1gFnSc4	píseň
Bück	Bücka	k1gFnPc2	Bücka
dich	dicha	k1gFnPc2	dicha
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
video	video	k1gNnSc1	video
natočené	natočený	k2eAgNnSc1d1	natočené
k	k	k7c3	k
této	tento	k3xDgFnSc3	tento
písni	píseň	k1gFnSc3	píseň
na	na	k7c6	na
koncertu	koncert	k1gInSc6	koncert
bylo	být	k5eAaImAgNnS	být
nevhodné	vhodný	k2eNgNnSc1d1	nevhodné
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
mladistvé	mladistvý	k1gMnPc4	mladistvý
(	(	kIx(	(
<g/>
na	na	k7c6	na
turné	turné	k1gNnSc6	turné
v	v	k7c6	v
USA	USA	kA	USA
toto	tento	k3xDgNnSc1	tento
vystoupení	vystoupení	k1gNnSc1	vystoupení
vedlo	vést	k5eAaImAgNnS	vést
dokonce	dokonce	k9	dokonce
k	k	k7c3	k
zatčení	zatčení	k1gNnSc3	zatčení
Lindemanna	Lindemann	k1gMnSc2	Lindemann
a	a	k8xC	a
Lorenze	Lorenz	k1gMnSc2	Lorenz
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Live	Live	k1gFnSc1	Live
aus	aus	k?	aus
Berlin	berlina	k1gFnPc2	berlina
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
dostalo	dostat	k5eAaPmAgNnS	dostat
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
hitparádě	hitparáda	k1gFnSc6	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
vydání	vydání	k1gNnSc6	vydání
začala	začít	k5eAaPmAgFnS	začít
příprava	příprava	k1gFnSc1	příprava
třetího	třetí	k4xOgNnSc2	třetí
studiového	studiový	k2eAgNnSc2d1	studiové
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
zatím	zatím	k6eAd1	zatím
nazpíval	nazpívat	k5eAaBmAgInS	nazpívat
Lindemann	Lindemann	k1gInSc1	Lindemann
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
Puhdys	Puhdysa	k1gFnPc2	Puhdysa
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Wut	Wut	k1gMnSc1	Wut
Will	Will	k1gMnSc1	Will
Nicht	Nicht	k1gMnSc1	Nicht
Sterben	Sterben	k2eAgMnSc1d1	Sterben
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Mutter	Muttra	k1gFnPc2	Muttra
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
ve	v	k7c6	v
studiu	studio	k1gNnSc6	studio
Miraval	Miraval	k1gFnSc2	Miraval
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Francii	Francie	k1gFnSc6	Francie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
a	a	k8xC	a
červnu	červen	k1gInSc6	červen
2000	[number]	k4	2000
provedeno	provést	k5eAaPmNgNnS	provést
nahrávání	nahrávání	k1gNnSc2	nahrávání
<g/>
,	,	kIx,	,
a	a	k8xC	a
ve	v	k7c6	v
Stockholmu	Stockholm	k1gInSc6	Stockholm
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
mixování	mixování	k1gNnSc1	mixování
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Rammstein	Rammsteina	k1gFnPc2	Rammsteina
ho	on	k3xPp3gMnSc4	on
produkovali	produkovat	k5eAaImAgMnP	produkovat
za	za	k7c4	za
pomoci	pomoct	k5eAaPmF	pomoct
Stefana	Stefan	k1gMnSc2	Stefan
Glaumanna	Glaumann	k1gMnSc2	Glaumann
a	a	k8xC	a
-	-	kIx~	-
již	již	k6eAd1	již
tradičně	tradičně	k6eAd1	tradičně
-	-	kIx~	-
Jacoba	Jacoba	k1gFnSc1	Jacoba
Hellnera	Hellner	k1gMnSc2	Hellner
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
zveřejnili	zveřejnit	k5eAaPmAgMnP	zveřejnit
Rammstein	Rammstein	k1gInSc4	Rammstein
první	první	k4xOgFnSc4	první
ukázku	ukázka	k1gFnSc4	ukázka
z	z	k7c2	z
nového	nový	k2eAgNnSc2d1	nové
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
píseň	píseň	k1gFnSc1	píseň
Links	Linksa	k1gFnPc2	Linksa
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
stáhnout	stáhnout	k5eAaPmF	stáhnout
na	na	k7c6	na
oficiálních	oficiální	k2eAgFnPc6d1	oficiální
stránkách	stránka	k1gFnPc6	stránka
skupiny	skupina	k1gFnSc2	skupina
ve	v	k7c6	v
formátu	formát	k1gInSc6	formát
MP	MP	kA	MP
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
a	a	k8xC	a
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
na	na	k7c6	na
festivalu	festival	k1gInSc6	festival
Big	Big	k1gMnSc2	Big
Day	Day	k1gMnSc2	Day
Out	Out	k1gMnSc2	Out
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
a	a	k8xC	a
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
singl	singl	k1gInSc1	singl
Sonne	Sonn	k1gInSc5	Sonn
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
12	[number]	k4	12
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
hitparádě	hitparáda	k1gFnSc6	hitparáda
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Mutter	Muttrum	k1gNnPc2	Muttrum
pak	pak	k6eAd1	pak
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
2	[number]	k4	2
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yRnSc1	co
se	se	k3xPyFc4	se
týká	týkat	k5eAaImIp3nS	týkat
počtu	počet	k1gInSc3	počet
vydaných	vydaný	k2eAgInPc2d1	vydaný
singlů	singl	k1gInPc2	singl
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Mutter	Muttra	k1gFnPc2	Muttra
zatím	zatím	k6eAd1	zatím
nejplodnějším	plodný	k2eAgNnSc7d3	nejplodnější
albem	album	k1gNnSc7	album
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Sonne	Sonn	k1gInSc5	Sonn
následovaly	následovat	k5eAaImAgInP	následovat
ještě	ještě	k6eAd1	ještě
Links	Links	k1gInSc4	Links
2-3-4	[number]	k4	2-3-4
(	(	kIx(	(
<g/>
květen	květen	k1gInSc4	květen
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ich	Ich	k1gFnSc1	Ich
will	willa	k1gFnPc2	willa
(	(	kIx(	(
<g/>
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mutter	Mutter	k1gInSc1	Mutter
(	(	kIx(	(
<g/>
březen	březen	k1gInSc1	březen
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
a	a	k8xC	a
Feuer	Feuer	k1gInSc4	Feuer
Frei	Fre	k1gFnSc2	Fre
<g/>
!	!	kIx.	!
</s>
<s>
(	(	kIx(	(
<g/>
říjen	říjen	k1gInSc1	říjen
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Posledně	posledně	k6eAd1	posledně
jmenovaný	jmenovaný	k2eAgInSc1d1	jmenovaný
singl	singl	k1gInSc1	singl
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
soundtracku	soundtrack	k1gInSc2	soundtrack
k	k	k7c3	k
filmu	film	k1gInSc3	film
xXx	xXx	k?	xXx
<g/>
;	;	kIx,	;
videoklip	videoklip	k1gInSc1	videoklip
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
se	s	k7c7	s
záběrů	záběr	k1gInPc2	záběr
z	z	k7c2	z
živého	živý	k2eAgNnSc2d1	živé
vystoupení	vystoupení	k1gNnSc2	vystoupení
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
z	z	k7c2	z
prostřihů	prostřih	k1gInPc2	prostřih
z	z	k7c2	z
filmu	film	k1gInSc2	film
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2002	[number]	k4	2002
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Rammstein	Rammstein	k1gInSc1	Rammstein
po	po	k7c6	po
delší	dlouhý	k2eAgFnSc6d2	delší
době	doba	k1gFnSc6	doba
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
–	–	k?	–
na	na	k7c6	na
Strahově	Strahov	k1gInSc6	Strahov
jako	jako	k9	jako
předkapela	předkapela	k1gFnSc1	předkapela
AC	AC	kA	AC
<g/>
/	/	kIx~	/
<g/>
DC	DC	kA	DC
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc4	tento
koncert	koncert	k1gInSc4	koncert
označili	označit	k5eAaPmAgMnP	označit
jako	jako	k8xS	jako
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejnepovedenějších	povedený	k2eNgInPc2d3	nejnepovedenější
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
navštívili	navštívit	k5eAaPmAgMnP	navštívit
Česko	Česko	k1gNnSc4	Česko
ještě	ještě	k6eAd1	ještě
jednou	jeden	k4xCgFnSc7	jeden
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
dne	den	k1gInSc2	den
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2001	[number]	k4	2001
v	v	k7c6	v
tehdejší	tehdejší	k2eAgFnSc6d1	tehdejší
T-Mobile	T-Mobila	k1gFnSc6	T-Mobila
aréně	aréna	k1gFnSc6	aréna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
června	červen	k1gInSc2	červen
až	až	k8xS	až
října	říjen	k1gInSc2	říjen
2001	[number]	k4	2001
odehráli	odehrát	k5eAaPmAgMnP	odehrát
Rammstein	Rammstein	k2eAgInSc4d1	Rammstein
sérii	série	k1gFnSc4	série
koncertů	koncert	k1gInPc2	koncert
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
(	(	kIx(	(
<g/>
Kanada	Kanada	k1gFnSc1	Kanada
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Mexiko	Mexiko	k1gNnSc1	Mexiko
<g/>
)	)	kIx)	)
a	a	k8xC	a
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
září	září	k1gNnSc2	září
a	a	k8xC	a
října	říjen	k1gInSc2	říjen
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
další	další	k2eAgInSc4d1	další
tour	tour	k1gInSc4	tour
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
listopadu	listopad	k1gInSc2	listopad
a	a	k8xC	a
prosince	prosinec	k1gInSc2	prosinec
2003	[number]	k4	2003
nahrává	nahrávat	k5eAaImIp3nS	nahrávat
skupina	skupina	k1gFnSc1	skupina
ve	v	k7c6	v
španělské	španělský	k2eAgFnSc6d1	španělská
Málaze	Málaha	k1gFnSc6	Málaha
své	svůj	k3xOyFgNnSc4	svůj
čtvrté	čtvrtý	k4xOgNnSc4	čtvrtý
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
alba	album	k1gNnSc2	album
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
staré	starý	k2eAgNnSc4d1	staré
námořnické	námořnický	k2eAgNnSc4d1	námořnické
heslo	heslo	k1gNnSc4	heslo
určené	určený	k2eAgNnSc4d1	určené
k	k	k7c3	k
mobilizaci	mobilizace	k1gFnSc3	mobilizace
ostatních	ostatní	k2eAgMnPc2d1	ostatní
námořníků	námořník	k1gMnPc2	námořník
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Povstaň	povstat	k5eAaPmRp2nS	povstat
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
(	(	kIx(	(
<g/>
die	die	k?	die
<g/>
)	)	kIx)	)
Reise	Reise	k1gFnSc1	Reise
jinak	jinak	k6eAd1	jinak
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
cesta	cesta	k1gFnSc1	cesta
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
zároveň	zároveň	k6eAd1	zároveň
vychází	vycházet	k5eAaImIp3nS	vycházet
druhé	druhý	k4xOgFnPc4	druhý
DVD	DVD	kA	DVD
skupiny	skupina	k1gFnPc1	skupina
<g/>
,	,	kIx,	,
Lichtspielhaus	Lichtspielhaus	k1gInSc1	Lichtspielhaus
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
všechny	všechen	k3xTgInPc4	všechen
dosud	dosud	k6eAd1	dosud
natočené	natočený	k2eAgInPc4d1	natočený
videoklipy	videoklip	k1gInPc4	videoklip
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
materiály	materiál	k1gInPc4	materiál
(	(	kIx(	(
<g/>
záběry	záběr	k1gInPc4	záběr
z	z	k7c2	z
přípravy	příprava	k1gFnSc2	příprava
alb	album	k1gNnPc2	album
<g/>
,	,	kIx,	,
reklamy	reklama	k1gFnPc4	reklama
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2004	[number]	k4	2004
vychází	vycházet	k5eAaImIp3nS	vycházet
první	první	k4xOgInSc1	první
singl	singl	k1gInSc1	singl
z	z	k7c2	z
nového	nový	k2eAgNnSc2d1	nové
alba	album	k1gNnSc2	album
<g/>
,	,	kIx,	,
Mein	Mein	k1gMnSc1	Mein
Teil	Teil	k1gMnSc1	Teil
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
nedávným	dávný	k2eNgInSc7d1	nedávný
případem	případ	k1gInSc7	případ
Armina	Armino	k1gNnSc2	Armino
Meiwese	Meiwese	k1gFnSc2	Meiwese
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
dopustil	dopustit	k5eAaPmAgMnS	dopustit
kanibalismu	kanibalismus	k1gInSc3	kanibalismus
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
se	se	k3xPyFc4	se
vyšplhala	vyšplhat	k5eAaPmAgFnS	vyšplhat
na	na	k7c4	na
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
hitparádě	hitparáda	k1gFnSc6	hitparáda
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
byla	být	k5eAaImAgFnS	být
nominována	nominovat	k5eAaBmNgFnS	nominovat
na	na	k7c4	na
cenu	cena	k1gFnSc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
(	(	kIx(	(
<g/>
kategorie	kategorie	k1gFnSc2	kategorie
Best	Best	k1gInSc1	Best
Metal	metal	k1gInSc1	metal
Performance	performance	k1gFnSc1	performance
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
stejně	stejně	k6eAd1	stejně
jako	jako	k8xS	jako
dříve	dříve	k6eAd2	dříve
píseň	píseň	k1gFnSc1	píseň
Du	Du	k?	Du
hast	hast	k5eAaPmF	hast
tuto	tento	k3xDgFnSc4	tento
cenu	cena	k1gFnSc4	cena
nevyhrála	vyhrát	k5eNaPmAgFnS	vyhrát
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
i	i	k9	i
druhý	druhý	k4xOgInSc4	druhý
singl	singl	k1gInSc4	singl
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
13	[number]	k4	13
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Text	text	k1gInSc1	text
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
celosvětovým	celosvětový	k2eAgInSc7d1	celosvětový
vlivem	vliv	k1gInSc7	vliv
kultury	kultura	k1gFnSc2	kultura
USA	USA	kA	USA
a	a	k8xC	a
část	část	k1gFnSc1	část
textu	text	k1gInSc2	text
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	září	k1gNnSc6	září
pak	pak	k6eAd1	pak
vychází	vycházet	k5eAaImIp3nS	vycházet
album	album	k1gNnSc4	album
Reise	Reis	k1gMnSc4	Reis
<g/>
,	,	kIx,	,
Reise	Reis	k1gMnSc4	Reis
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
magazínu	magazín	k1gInSc2	magazín
Billboard	billboard	k1gInSc1	billboard
se	se	k3xPyFc4	se
Rammstein	Rammstein	k2eAgMnSc1d1	Rammstein
na	na	k7c6	na
základě	základ	k1gInSc6	základ
výsledků	výsledek	k1gInPc2	výsledek
tohoto	tento	k3xDgNnSc2	tento
alba	album	k1gNnSc2	album
v	v	k7c6	v
hitparádách	hitparáda	k1gFnPc6	hitparáda
stali	stát	k5eAaPmAgMnP	stát
nejúspěšnější	úspěšný	k2eAgMnPc1d3	nejúspěšnější
německy	německy	k6eAd1	německy
zpívající	zpívající	k2eAgFnSc7d1	zpívající
skupinou	skupina	k1gFnSc7	skupina
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
a	a	k8xC	a
prosinci	prosinec	k1gInSc6	prosinec
následovalo	následovat	k5eAaImAgNnS	následovat
koncertní	koncertní	k2eAgNnSc1d1	koncertní
turné	turné	k1gNnSc1	turné
po	po	k7c6	po
Německu	Německo	k1gNnSc6	Německo
a	a	k8xC	a
dalších	další	k2eAgFnPc6d1	další
evropských	evropský	k2eAgFnPc6d1	Evropská
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
série	série	k1gFnSc1	série
vystoupení	vystoupení	k1gNnSc2	vystoupení
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
a	a	k8xC	a
také	také	k9	také
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
až	až	k8xS	až
červnu	červen	k1gInSc6	červen
následujícího	následující	k2eAgInSc2d1	následující
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
2005	[number]	k4	2005
hrála	hrát	k5eAaImAgFnS	hrát
skupina	skupina	k1gFnSc1	skupina
ve	v	k7c6	v
Wuhlheide	Wuhlheid	k1gInSc5	Wuhlheid
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
4	[number]	k4	4
dny	den	k1gInPc1	den
pro	pro	k7c4	pro
celkem	celkem	k6eAd1	celkem
zhruba	zhruba	k6eAd1	zhruba
70	[number]	k4	70
000	[number]	k4	000
diváků	divák	k1gMnPc2	divák
<g/>
,	,	kIx,	,
publikum	publikum	k1gNnSc1	publikum
si	se	k3xPyFc3	se
poslechlo	poslechnout	k5eAaPmAgNnS	poslechnout
i	i	k8xC	i
píseň	píseň	k1gFnSc4	píseň
Benzin	benzin	k1gInSc1	benzin
z	z	k7c2	z
připravovaného	připravovaný	k2eAgNnSc2d1	připravované
nového	nový	k2eAgNnSc2d1	nové
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
alba	album	k1gNnSc2	album
Reise	Reise	k1gFnSc2	Reise
<g/>
,	,	kIx,	,
Reise	Reise	k1gFnPc1	Reise
vzešly	vzejít	k5eAaPmAgFnP	vzejít
ještě	ještě	k6eAd1	ještě
2	[number]	k4	2
singly	singl	k1gInPc4	singl
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
baladická	baladický	k2eAgFnSc1d1	baladická
píseň	píseň	k1gFnSc1	píseň
Ohne	ohnout	k5eAaPmIp3nS	ohnout
dich	dich	k1gInSc1	dich
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2004	[number]	k4	2004
<g/>
)	)	kIx)	)
a	a	k8xC	a
Keine	Kein	k1gInSc5	Kein
Lust	Lust	k1gMnSc1	Lust
(	(	kIx(	(
<g/>
28	[number]	k4	28
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc1d1	další
studiové	studiový	k2eAgNnSc1d1	studiové
album	album	k1gNnSc1	album
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
tentokrát	tentokrát	k6eAd1	tentokrát
s	s	k7c7	s
menším	malý	k2eAgInSc7d2	menší
časovým	časový	k2eAgInSc7d1	časový
odstupem	odstup	k1gInSc7	odstup
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
nahráno	nahrát	k5eAaPmNgNnS	nahrát
opět	opět	k6eAd1	opět
v	v	k7c6	v
Málaze	Málaha	k1gFnSc6	Málaha
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
Benzin	benzin	k1gInSc1	benzin
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
singl	singl	k1gInSc1	singl
vydána	vydán	k2eAgFnSc1d1	vydána
7	[number]	k4	7
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
rok	rok	k1gInSc4	rok
po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
alba	album	k1gNnSc2	album
Reise	Reis	k1gMnSc2	Reis
<g/>
,	,	kIx,	,
Reise	Reis	k1gMnSc2	Reis
<g/>
.	.	kIx.	.
</s>
<s>
Benzin	benzin	k1gInSc1	benzin
obsadil	obsadit	k5eAaPmAgInS	obsadit
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
hitparádě	hitparáda	k1gFnSc6	hitparáda
6	[number]	k4	6
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Rosenrot	Rosenrota	k1gFnPc2	Rosenrota
spatřilo	spatřit	k5eAaPmAgNnS	spatřit
světlo	světlo	k1gNnSc1	světlo
světa	svět	k1gInSc2	svět
28	[number]	k4	28
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
vyšel	vyjít	k5eAaPmAgMnS	vyjít
singl	singl	k1gInSc4	singl
Rosenrot	Rosenrota	k1gFnPc2	Rosenrota
a	a	k8xC	a
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2006	[number]	k4	2006
singl	singl	k1gInSc1	singl
Mann	Mann	k1gMnSc1	Mann
gegen	gegna	k1gFnPc2	gegna
Mann	Mann	k1gMnSc1	Mann
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
na	na	k7c4	na
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
poslední	poslední	k2eAgMnSc1d1	poslední
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
vydává	vydávat	k5eAaImIp3nS	vydávat
skupina	skupina	k1gFnSc1	skupina
své	svůj	k3xOyFgFnPc4	svůj
třetí	třetí	k4xOgFnPc4	třetí
DVD	DVD	kA	DVD
<g/>
,	,	kIx,	,
Völkerball	Völkerball	k1gInSc1	Völkerball
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
videa	video	k1gNnPc4	video
z	z	k7c2	z
koncertů	koncert	k1gInPc2	koncert
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
Tokiu	Tokio	k1gNnSc6	Tokio
<g/>
,	,	kIx,	,
Moskvě	Moskva	k1gFnSc6	Moskva
a	a	k8xC	a
ve	v	k7c6	v
francouzském	francouzský	k2eAgInSc6d1	francouzský
Nîmes	Nîmes	k1gInSc1	Nîmes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2007	[number]	k4	2007
skupina	skupina	k1gFnSc1	skupina
uvažovala	uvažovat	k5eAaImAgFnS	uvažovat
o	o	k7c6	o
své	svůj	k3xOyFgFnSc6	svůj
další	další	k2eAgFnSc6d1	další
budoucnosti	budoucnost	k1gFnSc6	budoucnost
<g/>
.	.	kIx.	.
</s>
<s>
Objevila	objevit	k5eAaPmAgFnS	objevit
se	se	k3xPyFc4	se
informace	informace	k1gFnSc1	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
zpěvák	zpěvák	k1gMnSc1	zpěvák
Till	Till	k1gMnSc1	Till
Lindemann	Lindemann	k1gMnSc1	Lindemann
má	mít	k5eAaImIp3nS	mít
se	s	k7c7	s
skupinou	skupina	k1gFnSc7	skupina
natočit	natočit	k5eAaBmF	natočit
poslední	poslední	k2eAgNnSc4d1	poslední
album	album	k1gNnSc4	album
a	a	k8xC	a
pak	pak	k6eAd1	pak
odejít	odejít	k5eAaPmF	odejít
<g/>
,	,	kIx,	,
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
se	se	k3xPyFc4	se
ale	ale	k9	ale
<g/>
,	,	kIx,	,
že	že	k8xS	že
jde	jít	k5eAaImIp3nS	jít
jen	jen	k9	jen
o	o	k7c4	o
spekulaci	spekulace	k1gFnSc4	spekulace
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
roku	rok	k1gInSc2	rok
pracovat	pracovat	k5eAaImF	pracovat
na	na	k7c6	na
novém	nový	k2eAgNnSc6d1	nové
albu	album	k1gNnSc6	album
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc1	jehož
příprava	příprava	k1gFnSc1	příprava
se	se	k3xPyFc4	se
značně	značně	k6eAd1	značně
protáhla	protáhnout	k5eAaPmAgFnS	protáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávání	nahrávání	k1gNnSc1	nahrávání
začalo	začít	k5eAaPmAgNnS	začít
koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
první	první	k4xOgInSc4	první
singl	singl	k1gInSc4	singl
<g/>
,	,	kIx,	,
Pussy	Pussa	k1gFnPc1	Pussa
<g/>
,	,	kIx,	,
fanoušci	fanoušek	k1gMnPc1	fanoušek
čekali	čekat	k5eAaImAgMnP	čekat
až	až	k6eAd1	až
do	do	k7c2	do
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Pussy	Pussa	k1gFnSc2	Pussa
se	se	k3xPyFc4	se
věnuje	věnovat	k5eAaImIp3nS	věnovat
tématu	téma	k1gNnSc2	téma
sexuální	sexuální	k2eAgFnSc2d1	sexuální
turistiky	turistika	k1gFnSc2	turistika
a	a	k8xC	a
vzbudil	vzbudit	k5eAaPmAgInS	vzbudit
velkou	velký	k2eAgFnSc4d1	velká
pozornost	pozornost	k1gFnSc4	pozornost
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
značné	značný	k2eAgFnSc3d1	značná
míře	míra	k1gFnSc3	míra
erotiky	erotika	k1gFnSc2	erotika
(	(	kIx(	(
<g/>
klip	klip	k1gInSc1	klip
v	v	k7c6	v
necenzurované	cenzurovaný	k2eNgFnSc6d1	necenzurovaná
verzi	verze	k1gFnSc6	verze
pak	pak	k6eAd1	pak
bývá	bývat	k5eAaImIp3nS	bývat
označován	označovat	k5eAaImNgInS	označovat
za	za	k7c4	za
jasnou	jasný	k2eAgFnSc4d1	jasná
pornografii	pornografie	k1gFnSc4	pornografie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Singl	singl	k1gInSc1	singl
s	s	k7c7	s
anglicko	anglicko	k6eAd1	anglicko
<g/>
-	-	kIx~	-
<g/>
německým	německý	k2eAgInSc7d1	německý
textem	text	k1gInSc7	text
jednoznačně	jednoznačně	k6eAd1	jednoznačně
uspěl	uspět	k5eAaPmAgMnS	uspět
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
hitparádě	hitparáda	k1gFnSc6	hitparáda
(	(	kIx(	(
<g/>
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
příčka	příčka	k1gFnSc1	příčka
<g/>
,	,	kIx,	,
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
dosáhly	dosáhnout	k5eAaPmAgFnP	dosáhnout
3	[number]	k4	3
singly	singl	k1gInPc7	singl
skupiny	skupina	k1gFnSc2	skupina
na	na	k7c4	na
druhé	druhý	k4xOgNnSc4	druhý
místo	místo	k1gNnSc4	místo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Liebe	Lieb	k1gInSc5	Lieb
ist	ist	k?	ist
für	für	k?	für
alle	alle	k1gInSc1	alle
da	da	k?	da
vychází	vycházet	k5eAaImIp3nS	vycházet
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2009	[number]	k4	2009
a	a	k8xC	a
svou	svůj	k3xOyFgFnSc7	svůj
oblíbeností	oblíbenost	k1gFnSc7	oblíbenost
nezaostává	zaostávat	k5eNaImIp3nS	zaostávat
za	za	k7c7	za
předchozími	předchozí	k2eAgNnPc7d1	předchozí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
8	[number]	k4	8
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
prodejnosti	prodejnost	k1gFnSc6	prodejnost
alb	album	k1gNnPc2	album
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
obsazuje	obsazovat	k5eAaImIp3nS	obsazovat
13	[number]	k4	13
<g/>
.	.	kIx.	.
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
tak	tak	k6eAd1	tak
dosud	dosud	k6eAd1	dosud
nejúspěšnějším	úspěšný	k2eAgNnSc7d3	nejúspěšnější
albem	album	k1gNnSc7	album
skupiny	skupina	k1gFnSc2	skupina
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
bylo	být	k5eAaImAgNnS	být
album	album	k1gNnSc1	album
německým	německý	k2eAgInSc7d1	německý
státním	státní	k2eAgInSc7d1	státní
orgánem	orgán	k1gInSc7	orgán
Bundesprüfstelle	Bundesprüfstelle	k1gFnSc2	Bundesprüfstelle
für	für	k?	für
jugendgefährdende	jugendgefährdend	k1gInSc5	jugendgefährdend
Medien	Medien	k2eAgInSc4d1	Medien
(	(	kIx(	(
<g/>
orgán	orgán	k1gInSc4	orgán
ochraňující	ochraňující	k2eAgFnSc4d1	ochraňující
mládež	mládež	k1gFnSc4	mládež
před	před	k7c7	před
nebezpečnými	bezpečný	k2eNgInPc7d1	nebezpečný
vlivy	vliv	k1gInPc7	vliv
médií	médium	k1gNnPc2	médium
<g/>
)	)	kIx)	)
označeno	označit	k5eAaPmNgNnS	označit
za	za	k7c4	za
nebezpečné	bezpečný	k2eNgNnSc4d1	nebezpečné
pro	pro	k7c4	pro
mládež	mládež	k1gFnSc4	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Prodej	prodej	k1gInSc1	prodej
alba	album	k1gNnSc2	album
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
povolen	povolit	k5eAaPmNgInS	povolit
pouze	pouze	k6eAd1	pouze
plnoletým	plnoletý	k2eAgNnSc7d1	plnoleté
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
nesmělo	smět	k5eNaImAgNnS	smět
být	být	k5eAaImF	být
vystaveno	vystavit	k5eAaPmNgNnS	vystavit
v	v	k7c6	v
obchodech	obchod	k1gInPc6	obchod
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
mají	mít	k5eAaImIp3nP	mít
přístup	přístup	k1gInSc4	přístup
mladiství	mladiství	k1gNnSc2	mladiství
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
byl	být	k5eAaImAgInS	být
sadomasochistický	sadomasochistický	k2eAgInSc1d1	sadomasochistický
ráz	ráz	k1gInSc1	ráz
písně	píseň	k1gFnSc2	píseň
Ich	Ich	k1gFnSc2	Ich
tu	tu	k6eAd1	tu
dir	dir	k?	dir
weh	weh	k?	weh
<g/>
,	,	kIx,	,
skupina	skupina	k1gFnSc1	skupina
dostala	dostat	k5eAaPmAgFnS	dostat
zákaz	zákaz	k1gInSc4	zákaz
hrát	hrát	k5eAaImF	hrát
tuto	tento	k3xDgFnSc4	tento
píseň	píseň	k1gFnSc4	píseň
na	na	k7c6	na
veřejných	veřejný	k2eAgInPc6d1	veřejný
koncertech	koncert	k1gInPc6	koncert
<g/>
.	.	kIx.	.
16	[number]	k4	16
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
dokonce	dokonce	k9	dokonce
vydána	vydán	k2eAgFnSc1d1	vydána
nová	nový	k2eAgFnSc1d1	nová
verze	verze	k1gFnSc1	verze
alba	album	k1gNnSc2	album
bez	bez	k7c2	bez
této	tento	k3xDgFnSc2	tento
písně	píseň	k1gFnSc2	píseň
a	a	k8xC	a
bez	bez	k7c2	bez
jednoho	jeden	k4xCgInSc2	jeden
problémového	problémový	k2eAgInSc2d1	problémový
obrázku	obrázek	k1gInSc2	obrázek
z	z	k7c2	z
bookletu	booklet	k1gInSc2	booklet
<g/>
.	.	kIx.	.
</s>
<s>
Singl	singl	k1gInSc1	singl
Ich	Ich	k1gFnSc2	Ich
tu	tu	k6eAd1	tu
dir	dir	k?	dir
weh	weh	k?	weh
byl	být	k5eAaImAgInS	být
vydán	vydat	k5eAaPmNgInS	vydat
5	[number]	k4	5
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
2010	[number]	k4	2010
a	a	k8xC	a
singl	singl	k1gInSc1	singl
Haifisch	Haifisch	k1gInSc1	Haifisch
14	[number]	k4	14
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
skupina	skupina	k1gFnSc1	skupina
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
udělá	udělat	k5eAaPmIp3nS	udělat
šňůru	šňůra	k1gFnSc4	šňůra
koncertních	koncertní	k2eAgNnPc2d1	koncertní
turné	turné	k1gNnPc2	turné
po	po	k7c6	po
téměř	téměř	k6eAd1	téměř
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Turné	turné	k1gNnSc1	turné
s	s	k7c7	s
názvem	název	k1gInSc7	název
Made	Made	k1gFnSc1	Made
in	in	k?	in
Germany	German	k1gInPc7	German
1995-2011	[number]	k4	1995-2011
bylo	být	k5eAaImAgNnS	být
odstartováno	odstartovat	k5eAaPmNgNnS	odstartovat
6	[number]	k4	6
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
v	v	k7c6	v
Bratislavě	Bratislava	k1gFnSc6	Bratislava
a	a	k8xC	a
skončilo	skončit	k5eAaPmAgNnS	skončit
25	[number]	k4	25
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
v	v	k7c6	v
Houstonu	Houston	k1gInSc6	Houston
(	(	kIx(	(
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
zavítala	zavítat	k5eAaPmAgFnS	zavítat
skupina	skupina	k1gFnSc1	skupina
i	i	k9	i
do	do	k7c2	do
pražské	pražský	k2eAgFnSc2d1	Pražská
O2	O2	k1gFnSc2	O2
areny	arena	k1gFnSc2	arena
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
příležitosti	příležitost	k1gFnSc6	příležitost
vydali	vydat	k5eAaPmAgMnP	vydat
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
své	svůj	k3xOyFgNnSc4	svůj
sedmé	sedmý	k4xOgNnSc4	sedmý
album	album	k1gNnSc4	album
<g/>
,	,	kIx,	,
kompilaci	kompilace	k1gFnSc4	kompilace
Made	Made	k1gNnSc2	Made
in	in	k?	in
Germany	German	k1gInPc7	German
1995	[number]	k4	1995
<g/>
-	-	kIx~	-
<g/>
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
11	[number]	k4	11
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2011	[number]	k4	2011
vyšel	vyjít	k5eAaPmAgInS	vyjít
singl	singl	k1gInSc1	singl
s	s	k7c7	s
názvem	název	k1gInSc7	název
Mein	Mein	k1gMnSc1	Mein
Land	Land	k1gMnSc1	Land
ve	v	k7c6	v
verzi	verze	k1gFnSc6	verze
digipack	digipacka	k1gFnPc2	digipacka
a	a	k8xC	a
také	také	k9	také
jako	jako	k9	jako
7	[number]	k4	7
<g/>
"	"	kIx"	"
vinyl	vinyl	k1gInSc1	vinyl
<g/>
.	.	kIx.	.
</s>
<s>
Obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
zcela	zcela	k6eAd1	zcela
novou	nový	k2eAgFnSc4d1	nová
píseň	píseň	k1gFnSc4	píseň
s	s	k7c7	s
totožným	totožný	k2eAgInSc7d1	totožný
názvem	název	k1gInSc7	název
Mein	Mein	k1gNnSc4	Mein
Land	Landa	k1gFnPc2	Landa
<g/>
,	,	kIx,	,
dva	dva	k4xCgInPc1	dva
remixy	remix	k1gInPc1	remix
písně	píseň	k1gFnSc2	píseň
Mein	Mein	k1gMnSc1	Mein
Land	Land	k1gMnSc1	Land
a	a	k8xC	a
dosud	dosud	k6eAd1	dosud
nevydanou	vydaný	k2eNgFnSc4d1	nevydaná
píseň	píseň	k1gFnSc4	píseň
s	s	k7c7	s
názvem	název	k1gInSc7	název
Vergiss	Vergiss	k1gInSc4	Vergiss
uns	uns	k?	uns
nicht	nicht	k1gInSc1	nicht
<g/>
.	.	kIx.	.
</s>
<s>
Kapela	kapela	k1gFnSc1	kapela
také	také	k9	také
natočila	natočit	k5eAaBmAgFnS	natočit
klip	klip	k1gInSc4	klip
k	k	k7c3	k
písni	píseň	k1gFnSc3	píseň
"	"	kIx"	"
<g/>
Mein	Mein	k1gMnSc1	Mein
Land	Land	k1gMnSc1	Land
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
26	[number]	k4	26
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
na	na	k7c6	na
neoficiálních	neoficiální	k2eAgFnPc6d1	neoficiální
stránkách	stránka	k1gFnPc6	stránka
skupiny	skupina	k1gFnSc2	skupina
informace	informace	k1gFnSc2	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
chystá	chystat	k5eAaImIp3nS	chystat
vydání	vydání	k1gNnSc1	vydání
singlu	singl	k1gInSc2	singl
Mein	Mein	k1gMnSc1	Mein
Herz	Herz	k1gMnSc1	Herz
brennt	brennt	k1gMnSc1	brennt
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
píseň	píseň	k1gFnSc1	píseň
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
Made	Made	k1gFnSc6	Made
in	in	k?	in
Germany	German	k1gInPc7	German
1995	[number]	k4	1995
<g/>
-	-	kIx~	-
<g/>
2011	[number]	k4	2011
jediná	jediný	k2eAgFnSc1d1	jediná
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
nevyšla	vyjít	k5eNaPmAgFnS	vyjít
jako	jako	k9	jako
singl	singl	k1gInSc4	singl
<g/>
.	.	kIx.	.
</s>
<s>
Videoklip	videoklip	k1gInSc1	videoklip
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2011	[number]	k4	2011
v	v	k7c6	v
Beelitz-Heilstättenu	Beelitz-Heilstätten	k1gInSc6	Beelitz-Heilstätten
v	v	k7c6	v
opuštěném	opuštěný	k2eAgNnSc6d1	opuštěné
sanatoriu	sanatorium	k1gNnSc6	sanatorium
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1916	[number]	k4	1916
léčil	léčit	k5eAaImAgMnS	léčit
i	i	k9	i
Adolf	Adolf	k1gMnSc1	Adolf
Hitler	Hitler	k1gMnSc1	Hitler
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
byl	být	k5eAaImAgMnS	být
zraněn	zranit	k5eAaPmNgMnS	zranit
na	na	k7c6	na
noze	noha	k1gFnSc6	noha
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Sommě	Somma	k1gFnSc6	Somma
<g/>
.	.	kIx.	.
</s>
<s>
Singl	singl	k1gInSc1	singl
vyšel	vyjít	k5eAaPmAgInS	vyjít
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Singl	singl	k1gInSc1	singl
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
pianovou	pianový	k2eAgFnSc4d1	pianová
verzi	verze	k1gFnSc4	verze
písně	píseň	k1gFnSc2	píseň
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
novou	nový	k2eAgFnSc4d1	nová
píseň	píseň	k1gFnSc4	píseň
s	s	k7c7	s
názvem	název	k1gInSc7	název
Gib	Gib	k1gFnSc2	Gib
mir	mir	k1gInSc1	mir
deine	deinout	k5eAaPmIp3nS	deinout
Augen	Augen	k2eAgInSc1d1	Augen
<g/>
.	.	kIx.	.
</s>
<s>
Videoklip	videoklip	k1gInSc1	videoklip
k	k	k7c3	k
písni	píseň	k1gFnSc3	píseň
se	se	k3xPyFc4	se
zároveň	zároveň	k6eAd1	zároveň
objevil	objevit	k5eAaPmAgMnS	objevit
ve	v	k7c4	v
video	video	k1gNnSc4	video
kolekci	kolekce	k1gFnSc4	kolekce
s	s	k7c7	s
názvem	název	k1gInSc7	název
Videos	Videos	k1gInSc4	Videos
1995	[number]	k4	1995
<g/>
-	-	kIx~	-
<g/>
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Kolekce	kolekce	k1gFnSc1	kolekce
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
veškeré	veškerý	k3xTgInPc4	veškerý
natočené	natočený	k2eAgInPc4d1	natočený
videoklipy	videoklip	k1gInPc4	videoklip
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
<s>
Veškeré	veškerý	k3xTgInPc1	veškerý
klipy	klip	k1gInPc1	klip
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k9	rovněž
doplněny	doplnit	k5eAaPmNgInP	doplnit
o	o	k7c4	o
"	"	kIx"	"
<g/>
making	making	k1gInSc1	making
of	of	k?	of
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Kolekce	kolekce	k1gFnSc1	kolekce
vyšla	vyjít	k5eAaPmAgFnS	vyjít
na	na	k7c4	na
3	[number]	k4	3
DVD	DVD	kA	DVD
a	a	k8xC	a
také	také	k9	také
na	na	k7c4	na
2	[number]	k4	2
Blu-ray	Bluaa	k1gFnSc2	Blu-raa
discích	disk	k1gInPc6	disk
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
Rammstein	Rammstein	k1gMnSc1	Rammstein
použili	použít	k5eAaPmAgMnP	použít
vůbec	vůbec	k9	vůbec
poprvé	poprvé	k6eAd1	poprvé
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
vydala	vydat	k5eAaPmAgFnS	vydat
kapela	kapela	k1gFnSc1	kapela
Rammstein	Rammstein	k1gInSc4	Rammstein
své	své	k1gNnSc4	své
3	[number]	k4	3
<g/>
.	.	kIx.	.
kompilační	kompilační	k2eAgNnSc1d1	kompilační
album	album	k1gNnSc1	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
Rammstein	Rammstein	k1gMnSc1	Rammstein
In	In	k1gMnSc1	In
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
.	.	kIx.	.
2DVD	[number]	k4	2DVD
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
živý	živý	k2eAgInSc1d1	živý
koncert	koncert	k1gInSc1	koncert
z	z	k7c2	z
Madison	Madisona	k1gFnPc2	Madisona
Square	square	k1gInSc1	square
Garden	Gardno	k1gNnPc2	Gardno
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
11	[number]	k4	11
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
také	také	k9	také
video	video	k1gNnSc4	video
z	z	k7c2	z
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Making	Making	k1gInSc1	Making
of	of	k?	of
Liebe	Lieb	k1gInSc5	Lieb
ist	ist	k?	ist
für	für	k?	für
alle	alle	k1gInSc1	alle
da	da	k?	da
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
výrobu	výroba	k1gFnSc4	výroba
a	a	k8xC	a
natáčení	natáčení	k1gNnSc4	natáčení
zatím	zatím	k6eAd1	zatím
posledního	poslední	k2eAgNnSc2d1	poslední
alba	album	k1gNnSc2	album
skupiny	skupina	k1gFnSc2	skupina
a	a	k8xC	a
dokumentární	dokumentární	k2eAgInSc4d1	dokumentární
film	film	k1gInSc4	film
Rammstein	Rammstein	k1gMnSc1	Rammstein
in	in	k?	in
Amerika	Amerika	k1gFnSc1	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Dokument	dokument	k1gInSc1	dokument
pomáhal	pomáhat	k5eAaImAgInS	pomáhat
tvořit	tvořit	k5eAaImF	tvořit
známý	známý	k2eAgMnSc1d1	známý
rakouský	rakouský	k2eAgMnSc1d1	rakouský
režisér	režisér	k1gMnSc1	režisér
Hannes	Hannesa	k1gFnPc2	Hannesa
Rossacher	Rossachra	k1gFnPc2	Rossachra
<g/>
.	.	kIx.	.
</s>
<s>
Zmiňovaný	zmiňovaný	k2eAgInSc1d1	zmiňovaný
dvouhodinový	dvouhodinový	k2eAgInSc1d1	dvouhodinový
snímek	snímek	k1gInSc1	snímek
mapuje	mapovat	k5eAaImIp3nS	mapovat
téměř	téměř	k6eAd1	téměř
celou	celý	k2eAgFnSc4d1	celá
historii	historie	k1gFnSc4	historie
této	tento	k3xDgFnSc2	tento
industriální	industriální	k2eAgFnSc2d1	industriální
kapely	kapela	k1gFnSc2	kapela
<g/>
,	,	kIx,	,
a	a	k8xC	a
kromě	kromě	k7c2	kromě
doposud	doposud	k6eAd1	doposud
nikdy	nikdy	k6eAd1	nikdy
nezveřejněných	zveřejněný	k2eNgInPc2d1	nezveřejněný
rozhovorů	rozhovor	k1gInPc2	rozhovor
a	a	k8xC	a
záběrů	záběr	k1gInPc2	záběr
z	z	k7c2	z
koncertů	koncert	k1gInPc2	koncert
i	i	k8xC	i
zákulisí	zákulisí	k1gNnSc2	zákulisí
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
lze	lze	k6eAd1	lze
dočkat	dočkat	k5eAaPmF	dočkat
i	i	k9	i
soukromého	soukromý	k2eAgInSc2d1	soukromý
vizuálního	vizuální	k2eAgInSc2d1	vizuální
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnPc3	který
přispěli	přispět	k5eAaPmAgMnP	přispět
i	i	k9	i
samotní	samotný	k2eAgMnPc1d1	samotný
členové	člen	k1gMnPc1	člen
kapely	kapela	k1gFnSc2	kapela
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
dokumentu	dokument	k1gInSc2	dokument
dostali	dostat	k5eAaPmAgMnP	dostat
ale	ale	k8xC	ale
prostor	prostor	k1gInSc4	prostor
i	i	k8xC	i
jiní	jiný	k2eAgMnPc1d1	jiný
hudebníci	hudebník	k1gMnPc1	hudebník
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
na	na	k7c4	na
adresu	adresa	k1gFnSc4	adresa
kapely	kapela	k1gFnSc2	kapela
(	(	kIx(	(
<g/>
někdy	někdy	k6eAd1	někdy
i	i	k9	i
úplně	úplně	k6eAd1	úplně
poprvé	poprvé	k6eAd1	poprvé
<g/>
)	)	kIx)	)
otevřeně	otevřeně	k6eAd1	otevřeně
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
oslovené	oslovený	k2eAgMnPc4d1	oslovený
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Iggy	Igg	k1gMnPc4	Igg
Pop	pop	k1gInSc1	pop
<g/>
,	,	kIx,	,
Steven	Steven	k2eAgInSc1d1	Steven
Tyler	Tyler	k1gInSc1	Tyler
(	(	kIx(	(
<g/>
Aerosmith	Aerosmith	k1gInSc1	Aerosmith
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Marilyn	Marilyn	k1gFnSc1	Marilyn
Manson	Manson	k1gInSc1	Manson
<g/>
,	,	kIx,	,
Gene	gen	k1gInSc5	gen
Simmons	Simmons	k1gInSc1	Simmons
(	(	kIx(	(
<g/>
Kiss	Kiss	k1gInSc1	Kiss
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Paul	Paul	k1gMnSc1	Paul
Stanley	Stanlea	k1gFnSc2	Stanlea
(	(	kIx(	(
<g/>
Kiss	Kiss	k1gInSc1	Kiss
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Moby	Moba	k1gFnSc2	Moba
<g/>
,	,	kIx,	,
Scott	Scott	k1gMnSc1	Scott
Ian	Ian	k1gMnSc1	Ian
(	(	kIx(	(
<g/>
Anthrax	Anthrax	k1gInSc1	Anthrax
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Chad	Chad	k1gMnSc1	Chad
Smith	Smith	k1gMnSc1	Smith
(	(	kIx(	(
<g/>
Red	Red	k1gMnSc1	Red
Hot	hot	k0	hot
Chili	Chile	k1gFnSc3	Chile
Peppers	Peppers	k1gInSc1	Peppers
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Wes	Wes	k1gMnSc1	Wes
Borland	Borland	kA	Borland
(	(	kIx(	(
<g/>
Limp	limpa	k1gFnPc2	limpa
Bizkit	Bizkit	k1gInSc1	Bizkit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Dolmayan	Dolmayan	k1gMnSc1	Dolmayan
(	(	kIx(	(
<g/>
System	Syst	k1gMnSc7	Syst
of	of	k?	of
A	a	k8xC	a
Down	Downo	k1gNnPc2	Downo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Melissa	Melissa	k1gFnSc1	Melissa
Auf	Auf	k1gFnSc1	Auf
der	drát	k5eAaImRp2nS	drát
Maur	Maur	k1gMnSc1	Maur
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
Shaffer	Shaffer	k1gMnSc1	Shaffer
(	(	kIx(	(
<g/>
Korn	Korn	k1gMnSc1	Korn
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Shawn	Shawn	k1gMnSc1	Shawn
Crahan	Crahan	k1gMnSc1	Crahan
(	(	kIx(	(
<g/>
Slipknot	Slipknota	k1gFnPc2	Slipknota
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
herec	herec	k1gMnSc1	herec
Kiefer	Kiefer	k1gMnSc1	Kiefer
Sutherland	Sutherlando	k1gNnPc2	Sutherlando
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgMnSc7d1	hlavní
autorem	autor	k1gMnSc7	autor
textů	text	k1gInPc2	text
skupiny	skupina	k1gFnSc2	skupina
je	být	k5eAaImIp3nS	být
zpěvák	zpěvák	k1gMnSc1	zpěvák
Till	Till	k1gMnSc1	Till
Lindemann	Lindemann	k1gMnSc1	Lindemann
<g/>
,	,	kIx,	,
syn	syn	k1gMnSc1	syn
básníka	básník	k1gMnSc2	básník
a	a	k8xC	a
spisovatele	spisovatel	k1gMnSc2	spisovatel
Wernera	Werner	k1gMnSc2	Werner
Lindemanna	Lindemann	k1gMnSc2	Lindemann
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
většina	většina	k1gFnSc1	většina
textů	text	k1gInPc2	text
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
výhradně	výhradně	k6eAd1	výhradně
cizojazyčnou	cizojazyčný	k2eAgFnSc7d1	cizojazyčná
písní	píseň	k1gFnSc7	píseň
vydanou	vydaný	k2eAgFnSc7d1	vydaná
na	na	k7c6	na
studiovém	studiový	k2eAgNnSc6d1	studiové
albu	album	k1gNnSc6	album
je	být	k5eAaImIp3nS	být
Te	Te	k1gMnSc1	Te
quiero	quiero	k1gNnSc4	quiero
puta	puta	k1gFnSc1	puta
<g/>
!	!	kIx.	!
</s>
<s>
z	z	k7c2	z
alba	album	k1gNnSc2	album
Rosenrot	Rosenrota	k1gFnPc2	Rosenrota
<g/>
,	,	kIx,	,
skupina	skupina	k1gFnSc1	skupina
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
vytvořit	vytvořit	k5eAaPmF	vytvořit
skladbu	skladba	k1gFnSc4	skladba
ve	v	k7c6	v
španělštině	španělština	k1gFnSc6	španělština
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
projevila	projevit	k5eAaPmAgFnS	projevit
dík	dík	k7c3	dík
svým	svůj	k3xOyFgMnPc3	svůj
mexickým	mexický	k2eAgMnPc3d1	mexický
fanouškům	fanoušek	k1gMnPc3	fanoušek
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
předchozí	předchozí	k2eAgNnSc4d1	předchozí
album	album	k1gNnSc4	album
Reise	Reise	k1gFnSc2	Reise
<g/>
,	,	kIx,	,
Reise	Reise	k1gFnSc2	Reise
se	se	k3xPyFc4	se
v	v	k7c6	v
Mexiku	Mexiko	k1gNnSc6	Mexiko
dostalo	dostat	k5eAaPmAgNnS	dostat
na	na	k7c4	na
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
příčku	příčka	k1gFnSc4	příčka
hitparády	hitparáda	k1gFnSc2	hitparáda
<g/>
.	.	kIx.	.
</s>
<s>
Angličtina	angličtina	k1gFnSc1	angličtina
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
písních	píseň	k1gFnPc6	píseň
Amerika	Amerika	k1gFnSc1	Amerika
a	a	k8xC	a
Pussy	Pussa	k1gFnPc1	Pussa
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
také	také	k9	také
v	v	k7c4	v
cover	cover	k1gInSc4	cover
verzi	verze	k1gFnSc4	verze
písně	píseň	k1gFnSc2	píseň
Stripped	Stripped	k1gMnSc1	Stripped
od	od	k7c2	od
Depeche	Depech	k1gFnSc2	Depech
Mode	modus	k1gInSc5	modus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
Stirb	Stirba	k1gFnPc2	Stirba
nicht	nicht	k2eAgInSc1d1	nicht
vor	vor	k1gInSc1	vor
mir	mir	k1gInSc1	mir
zpívá	zpívat	k5eAaImIp3nS	zpívat
anglicky	anglicky	k6eAd1	anglicky
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Sharleen	Sharlena	k1gFnPc2	Sharlena
Spiteri	Spiter	k1gFnSc2	Spiter
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
refrénu	refrén	k1gInSc6	refrén
písně	píseň	k1gFnSc2	píseň
Frühling	Frühling	k1gInSc1	Frühling
in	in	k?	in
Paris	Paris	k1gMnSc1	Paris
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
francouzština	francouzština	k1gFnSc1	francouzština
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
odkaz	odkaz	k1gInSc4	odkaz
na	na	k7c4	na
píseň	píseň	k1gFnSc4	píseň
Non	Non	k1gMnPc2	Non
<g/>
,	,	kIx,	,
je	on	k3xPp3gMnPc4	on
ne	ne	k9	ne
regrette	regrette	k5eAaPmIp2nP	regrette
rien	rien	k1gInSc4	rien
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
zpívala	zpívat	k5eAaImAgFnS	zpívat
Édith	Édith	k1gInSc4	Édith
Piaf	Piaf	k1gFnSc2	Piaf
<g/>
.	.	kIx.	.
</s>
<s>
Rammstein	Rammstein	k1gInSc4	Rammstein
vyzkoušeli	vyzkoušet	k5eAaPmAgMnP	vyzkoušet
také	také	k9	také
ruštinu	ruština	k1gFnSc4	ruština
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
v	v	k7c6	v
písni	píseň	k1gFnSc6	píseň
Moskau	Moskaus	k1gInSc2	Moskaus
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
zpívá	zpívat	k5eAaImIp3nS	zpívat
rusky	rusky	k6eAd1	rusky
především	především	k9	především
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Viktoria	Viktoria	k1gFnSc1	Viktoria
Fersh	Fersh	k1gInSc4	Fersh
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
nahráli	nahrát	k5eAaPmAgMnP	nahrát
Schtiel	Schtiel	k1gInSc4	Schtiel
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
cover	cover	k1gInSc1	cover
verze	verze	k1gFnSc2	verze
písně	píseň	k1gFnSc2	píseň
Štil	Štila	k1gFnPc2	Štila
(	(	kIx(	(
<g/>
Ш	Ш	k?	Ш
<g/>
)	)	kIx)	)
od	od	k7c2	od
ruské	ruský	k2eAgFnSc2d1	ruská
heavy-metalové	heavyetal	k1gMnPc1	heavy-metal
skupiny	skupina	k1gFnSc2	skupina
Aria	Arium	k1gNnSc2	Arium
s	s	k7c7	s
originálním	originální	k2eAgInSc7d1	originální
textem	text	k1gInSc7	text
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
textech	text	k1gInPc6	text
skupiny	skupina	k1gFnPc1	skupina
mají	mít	k5eAaImIp3nP	mít
různé	různý	k2eAgInPc4d1	různý
společensky	společensky	k6eAd1	společensky
okrajové	okrajový	k2eAgInPc4d1	okrajový
či	či	k8xC	či
tabuizované	tabuizovaný	k2eAgInPc4d1	tabuizovaný
jevy	jev	k1gInPc4	jev
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
jmenovat	jmenovat	k5eAaBmF	jmenovat
například	například	k6eAd1	například
sadomasochismus	sadomasochismus	k1gInSc4	sadomasochismus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
je	být	k5eAaImIp3nS	být
tématem	téma	k1gNnSc7	téma
písní	píseň	k1gFnPc2	píseň
Bestrafe	Bestraf	k1gInSc5	Bestraf
mich	mich	k1gMnSc1	mich
<g/>
,	,	kIx,	,
Bück	Bück	k1gMnSc1	Bück
dich	dich	k1gMnSc1	dich
<g/>
,	,	kIx,	,
Rein	Rein	k2eAgInSc1d1	Rein
Raus	Raus	k1gInSc1	Raus
a	a	k8xC	a
Ich	Ich	k1gFnSc1	Ich
tu	tu	k6eAd1	tu
dir	dir	k?	dir
weh	weh	k?	weh
<g/>
,	,	kIx,	,
homosexualitu	homosexualita	k1gFnSc4	homosexualita
(	(	kIx(	(
<g/>
Mann	Mann	k1gMnSc1	Mann
gegen	gegna	k1gFnPc2	gegna
Mann	Mann	k1gMnSc1	Mann
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zneužívání	zneužívání	k1gNnSc1	zneužívání
a	a	k8xC	a
incest	incest	k1gInSc1	incest
(	(	kIx(	(
<g/>
Tier	Tier	k1gMnSc1	Tier
<g/>
,	,	kIx,	,
Laichzeit	Laichzeit	k1gMnSc1	Laichzeit
<g/>
,	,	kIx,	,
Spiel	Spiel	k1gMnSc1	Spiel
mit	mit	k?	mit
<g />
.	.	kIx.	.
</s>
<s>
mir	mir	k1gInSc1	mir
<g/>
,	,	kIx,	,
Halleluja	Halleluja	k1gMnSc1	Halleluja
a	a	k8xC	a
Wiener	Wiener	k1gMnSc1	Wiener
Blut	Blut	k1gMnSc1	Blut
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nekrofílii	nekrofílie	k1gFnSc4	nekrofílie
(	(	kIx(	(
<g/>
Heirate	Heirat	k1gMnSc5	Heirat
mich	micha	k1gFnPc2	micha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pyromanii	pyromanie	k1gFnSc4	pyromanie
(	(	kIx(	(
<g/>
Benzin	benzin	k1gInSc4	benzin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sexuální	sexuální	k2eAgFnSc4d1	sexuální
turistiku	turistika	k1gFnSc4	turistika
(	(	kIx(	(
<g/>
Pussy	Pussa	k1gFnSc2	Pussa
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hermafroditismus	hermafroditismus	k1gInSc1	hermafroditismus
(	(	kIx(	(
<g/>
Zwitter	Zwitter	k1gInSc1	Zwitter
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
drogy	droga	k1gFnPc4	droga
(	(	kIx(	(
<g/>
Adiós	Adiós	k1gInSc1	Adiós
<g/>
)	)	kIx)	)
a	a	k8xC	a
kanibalismus	kanibalismus	k1gInSc4	kanibalismus
(	(	kIx(	(
<g/>
Mein	Mein	k1gMnSc1	Mein
Teil	Teil	k1gMnSc1	Teil
a	a	k8xC	a
Eifersucht	Eifersucht	k1gMnSc1	Eifersucht
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Písně	píseň	k1gFnPc1	píseň
Asche	Asche	k1gNnSc2	Asche
zu	zu	k?	zu
Asche	Asche	k1gFnPc2	Asche
a	a	k8xC	a
Bestrafe	Bestraf	k1gInSc5	Bestraf
mich	mich	k1gInSc4	mich
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
náboženskou	náboženský	k2eAgFnSc4d1	náboženská
tematiku	tematika	k1gFnSc4	tematika
<g/>
.	.	kIx.	.
</s>
<s>
Motiv	motiv	k1gInSc1	motiv
tragédie	tragédie	k1gFnSc2	tragédie
či	či	k8xC	či
smrti	smrt	k1gFnSc2	smrt
obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
např.	např.	kA	např.
v	v	k7c6	v
písních	píseň	k1gFnPc6	píseň
Rammstein	Rammstein	k1gMnSc1	Rammstein
<g/>
,	,	kIx,	,
Alter	Alter	k1gMnSc1	Alter
Mann	Mann	k1gMnSc1	Mann
<g/>
,	,	kIx,	,
Spieluhr	Spieluhr	k1gMnSc1	Spieluhr
<g/>
,	,	kIx,	,
Ohne	ohnout	k5eAaPmIp3nS	ohnout
dich	dich	k1gMnSc1	dich
či	či	k8xC	či
Hilf	Hilf	k1gMnSc1	Hilf
mir	mir	k1gInSc1	mir
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
texty	text	k1gInPc1	text
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
věnují	věnovat	k5eAaImIp3nP	věnovat
mezilidským	mezilidský	k2eAgInPc3d1	mezilidský
vztahům	vztah	k1gInPc3	vztah
a	a	k8xC	a
lásce	láska	k1gFnSc6	láska
<g/>
,	,	kIx,	,
běžným	běžný	k2eAgInPc3d1	běžný
lidským	lidský	k2eAgInPc3d1	lidský
problémům	problém	k1gInPc3	problém
(	(	kIx(	(
<g/>
text	text	k1gInSc1	text
písně	píseň	k1gFnSc2	píseň
Morgenstern	Morgensterna	k1gFnPc2	Morgensterna
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
o	o	k7c6	o
samotě	samota	k1gFnSc6	samota
a	a	k8xC	a
smutku	smutek	k1gInSc6	smutek
ošklivé	ošklivý	k2eAgFnSc2d1	ošklivá
dívky	dívka	k1gFnSc2	dívka
<g/>
,	,	kIx,	,
píseň	píseň	k1gFnSc1	píseň
Mehr	Mehra	k1gFnPc2	Mehra
je	být	k5eAaImIp3nS	být
o	o	k7c6	o
lidské	lidský	k2eAgFnSc6d1	lidská
hrabivosti	hrabivost	k1gFnSc6	hrabivost
<g/>
,	,	kIx,	,
skladba	skladba	k1gFnSc1	skladba
Ich	Ich	k1gMnSc1	Ich
Will	Will	k1gMnSc1	Will
je	být	k5eAaImIp3nS	být
o	o	k7c6	o
touze	touha	k1gFnSc6	touha
vidět	vidět	k5eAaImF	vidět
a	a	k8xC	a
být	být	k5eAaImF	být
viděn	viděn	k2eAgMnSc1d1	viděn
<g/>
)	)	kIx)	)
a	a	k8xC	a
například	například	k6eAd1	například
píseň	píseň	k1gFnSc1	píseň
Amerika	Amerika	k1gFnSc1	Amerika
kritizuje	kritizovat	k5eAaImIp3nS	kritizovat
přílišný	přílišný	k2eAgInSc4d1	přílišný
vliv	vliv	k1gInSc4	vliv
americké	americký	k2eAgFnSc2d1	americká
kultury	kultura	k1gFnSc2	kultura
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
texty	text	k1gInPc1	text
jsou	být	k5eAaImIp3nP	být
inspirovány	inspirovat	k5eAaBmNgInP	inspirovat
skutečnými	skutečný	k2eAgFnPc7d1	skutečná
událostmi	událost	k1gFnPc7	událost
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
Rammstein	Rammsteina	k1gFnPc2	Rammsteina
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
na	na	k7c4	na
leteckou	letecký	k2eAgFnSc4d1	letecká
katastrofu	katastrofa	k1gFnSc4	katastrofa
na	na	k7c6	na
základně	základna	k1gFnSc6	základna
Ramstein	Ramstein	k1gInSc4	Ramstein
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
<g/>
,	,	kIx,	,
píseň	píseň	k1gFnSc1	píseň
Mein	Meina	k1gFnPc2	Meina
Teil	Teil	k1gInSc4	Teil
je	být	k5eAaImIp3nS	být
inspirována	inspirovat	k5eAaBmNgFnS	inspirovat
případem	případ	k1gInSc7	případ
kanibala	kanibal	k1gMnSc2	kanibal
Armina	Armin	k1gMnSc2	Armin
Meiwese	Meiwese	k1gFnSc2	Meiwese
a	a	k8xC	a
námětem	námět	k1gInSc7	námět
písně	píseň	k1gFnSc2	píseň
Wiener	Wiener	k1gInSc1	Wiener
Blut	Blut	k2eAgInSc1d1	Blut
je	být	k5eAaImIp3nS	být
případ	případ	k1gInSc1	případ
tyrana	tyran	k1gMnSc2	tyran
Josefa	Josef	k1gMnSc2	Josef
Fritzla	Fritzla	k1gMnSc2	Fritzla
<g/>
.	.	kIx.	.
</s>
<s>
Hudba	hudba	k1gFnSc1	hudba
skupiny	skupina	k1gFnSc2	skupina
Rammstein	Rammsteina	k1gFnPc2	Rammsteina
je	být	k5eAaImIp3nS	být
ovlivněna	ovlivnit	k5eAaPmNgFnS	ovlivnit
více	hodně	k6eAd2	hodně
hudebními	hudební	k2eAgInPc7d1	hudební
styly	styl	k1gInPc7	styl
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vydání	vydání	k1gNnSc6	vydání
jejího	její	k3xOp3gInSc2	její
prvního	první	k4xOgNnSc2	první
alba	album	k1gNnSc2	album
vymyslel	vymyslet	k5eAaPmAgInS	vymyslet
hudební	hudební	k2eAgInSc1d1	hudební
tisk	tisk	k1gInSc1	tisk
speciální	speciální	k2eAgInSc4d1	speciální
termín	termín	k1gInSc4	termín
Neue	Neuus	k1gMnSc5	Neuus
Deutsche	Deutschus	k1gMnSc5	Deutschus
Härte	Härt	k1gMnSc5	Härt
(	(	kIx(	(
<g/>
doslovně	doslovně	k6eAd1	doslovně
přeloženo	přeložen	k2eAgNnSc1d1	přeloženo
"	"	kIx"	"
<g/>
nová	nový	k2eAgFnSc1d1	nová
německá	německý	k2eAgFnSc1d1	německá
tvrdost	tvrdost	k1gFnSc1	tvrdost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
patrná	patrný	k2eAgFnSc1d1	patrná
je	být	k5eAaImIp3nS	být
narážka	narážka	k1gFnSc1	narážka
na	na	k7c4	na
termín	termín	k1gInSc4	termín
Neue	Neu	k1gMnSc2	Neu
Deutsche	Deutsch	k1gMnSc2	Deutsch
Welle	Well	k1gMnSc2	Well
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Styl	styl	k1gInSc1	styl
Neue	Neu	k1gMnSc2	Neu
Deutsche	Deutsch	k1gMnSc2	Deutsch
Härte	Härt	k1gInSc5	Härt
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
německou	německý	k2eAgFnSc4d1	německá
hudbu	hudba	k1gFnSc4	hudba
kombinující	kombinující	k2eAgInPc1d1	kombinující
prvky	prvek	k1gInPc1	prvek
hard	harda	k1gFnPc2	harda
rocku	rock	k1gInSc2	rock
<g/>
,	,	kIx,	,
heavy	heava	k1gFnSc2	heava
metalu	metal	k1gInSc2	metal
a	a	k8xC	a
industriální	industriální	k2eAgFnSc2d1	industriální
a	a	k8xC	a
elektronické	elektronický	k2eAgFnSc2d1	elektronická
hudby	hudba	k1gFnSc2	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Rammstein	Rammstein	k1gInSc4	Rammstein
jsou	být	k5eAaImIp3nP	být
nejúspěšnějším	úspěšný	k2eAgMnSc7d3	nejúspěšnější
zástupcem	zástupce	k1gMnSc7	zástupce
tohoto	tento	k3xDgInSc2	tento
stylu	styl	k1gInSc2	styl
rockové	rockový	k2eAgFnSc2d1	rocková
hudby	hudba	k1gFnSc2	hudba
<g/>
,	,	kIx,	,
dalším	další	k2eAgMnSc7d1	další
známým	známý	k2eAgMnSc7d1	známý
interpretem	interpret	k1gMnSc7	interpret
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
Oomph	Oomph	k1gInSc4	Oomph
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
</s>
<s>
Někdy	někdy	k6eAd1	někdy
jsou	být	k5eAaImIp3nP	být
Rammstein	Rammsteina	k1gFnPc2	Rammsteina
řazeni	řazen	k2eAgMnPc1d1	řazen
mezi	mezi	k7c4	mezi
industrial	industrial	k1gInSc4	industrial
metalové	metalový	k2eAgFnSc2d1	metalová
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
jejich	jejich	k3xOp3gFnSc1	jejich
hudba	hudba	k1gFnSc1	hudba
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
řadu	řada	k1gFnSc4	řada
typických	typický	k2eAgInPc2d1	typický
rysů	rys	k1gInPc2	rys
tohoto	tento	k3xDgInSc2	tento
stylu	styl	k1gInSc2	styl
(	(	kIx(	(
<g/>
opakující	opakující	k2eAgMnSc1d1	opakující
se	se	k3xPyFc4	se
tvrdé	tvrdý	k2eAgInPc1d1	tvrdý
kytarové	kytarový	k2eAgInPc1d1	kytarový
riffy	riff	k1gInPc1	riff
<g/>
,	,	kIx,	,
kombinace	kombinace	k1gFnPc1	kombinace
s	s	k7c7	s
elektronikou	elektronika	k1gFnSc7	elektronika
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
neuhlazený	uhlazený	k2eNgInSc4d1	neuhlazený
zkreslený	zkreslený	k2eAgInSc4d1	zkreslený
zvuk	zvuk	k1gInSc4	zvuk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
hudbu	hudba	k1gFnSc4	hudba
z	z	k7c2	z
alba	album	k1gNnSc2	album
Herzeleid	Herzeleida	k1gFnPc2	Herzeleida
byly	být	k5eAaImAgFnP	být
typické	typický	k2eAgNnSc1d1	typické
dokola	dokola	k6eAd1	dokola
se	se	k3xPyFc4	se
opakující	opakující	k2eAgInPc1d1	opakující
<g/>
,	,	kIx,	,
tvrdé	tvrdý	k2eAgInPc1d1	tvrdý
a	a	k8xC	a
často	často	k6eAd1	často
rychlé	rychlý	k2eAgInPc4d1	rychlý
kytarové	kytarový	k2eAgInPc4d1	kytarový
riffy	riff	k1gInPc4	riff
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
málo	málo	k6eAd1	málo
melodie	melodie	k1gFnPc4	melodie
a	a	k8xC	a
značný	značný	k2eAgInSc4d1	značný
podíl	podíl	k1gInSc4	podíl
elektroniky	elektronika	k1gFnSc2	elektronika
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
výrazným	výrazný	k2eAgInPc3d1	výrazný
a	a	k8xC	a
jednoduchým	jednoduchý	k2eAgInPc3d1	jednoduchý
rytmům	rytmus	k1gInPc3	rytmus
vymyslela	vymyslet	k5eAaPmAgFnS	vymyslet
skupina	skupina	k1gFnSc1	skupina
označení	označení	k1gNnSc2	označení
Tanzmetal	Tanzmetal	k1gFnSc1	Tanzmetal
(	(	kIx(	(
<g/>
taneční	taneční	k2eAgInSc1d1	taneční
metal	metal	k1gInSc1	metal
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Sehnsucht	Sehnsuchta	k1gFnPc2	Sehnsuchta
pokračovalo	pokračovat	k5eAaImAgNnS	pokračovat
v	v	k7c6	v
podobném	podobný	k2eAgInSc6d1	podobný
stylu	styl	k1gInSc6	styl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bylo	být	k5eAaImAgNnS	být
komerčně	komerčně	k6eAd1	komerčně
úspěšnější	úspěšný	k2eAgNnSc1d2	úspěšnější
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
Mutter	Muttrum	k1gNnPc2	Muttrum
se	se	k3xPyFc4	se
otevřelo	otevřít	k5eAaPmAgNnS	otevřít
širokému	široký	k2eAgNnSc3d1	široké
publiku	publikum	k1gNnSc3	publikum
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
hudba	hudba	k1gFnSc1	hudba
byla	být	k5eAaImAgFnS	být
oproti	oproti	k7c3	oproti
předchozím	předchozí	k2eAgNnPc3d1	předchozí
albům	album	k1gNnPc3	album
uhlazenější	uhlazený	k2eAgInSc5d2	uhlazenější
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
následujícím	následující	k2eAgNnSc6d1	následující
albu	album	k1gNnSc6	album
Reise	Reise	k1gFnSc2	Reise
<g/>
,	,	kIx,	,
Reise	Reise	k1gFnSc2	Reise
se	se	k3xPyFc4	se
skupina	skupina	k1gFnSc1	skupina
odklonila	odklonit	k5eAaPmAgFnS	odklonit
od	od	k7c2	od
elektroniky	elektronika	k1gFnSc2	elektronika
<g/>
.	.	kIx.	.
</s>
<s>
Skladby	skladba	k1gFnPc1	skladba
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
albu	album	k1gNnSc6	album
jsou	být	k5eAaImIp3nP	být
spíše	spíše	k9	spíše
pomalé	pomalý	k2eAgNnSc1d1	pomalé
<g/>
.	.	kIx.	.
</s>
<s>
Pomalejší	pomalý	k2eAgNnSc1d2	pomalejší
tempo	tempo	k1gNnSc1	tempo
je	být	k5eAaImIp3nS	být
patrné	patrný	k2eAgNnSc1d1	patrné
i	i	k9	i
na	na	k7c6	na
následujících	následující	k2eAgNnPc6d1	následující
albech	album	k1gNnPc6	album
<g/>
,	,	kIx,	,
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
více	hodně	k6eAd2	hodně
klidných	klidný	k2eAgFnPc2d1	klidná
písní	píseň	k1gFnPc2	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Ostrých	ostrý	k2eAgInPc2d1	ostrý
a	a	k8xC	a
rychlých	rychlý	k2eAgInPc2d1	rychlý
riffů	riff	k1gInPc2	riff
<g/>
,	,	kIx,	,
typických	typický	k2eAgFnPc2d1	typická
pro	pro	k7c4	pro
počátky	počátek	k1gInPc4	počátek
skupiny	skupina	k1gFnSc2	skupina
<g/>
,	,	kIx,	,
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
ubylo	ubýt	k5eAaPmAgNnS	ubýt
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
nejnovějším	nový	k2eAgNnSc6d3	nejnovější
albu	album	k1gNnSc6	album
Liebe	Lieb	k1gInSc5	Lieb
ist	ist	k?	ist
für	für	k?	für
alle	alle	k1gInSc1	alle
da	da	k?	da
je	být	k5eAaImIp3nS	být
patrný	patrný	k2eAgInSc1d1	patrný
určitý	určitý	k2eAgInSc1d1	určitý
návrat	návrat	k1gInSc1	návrat
k	k	k7c3	k
dřívější	dřívější	k2eAgFnSc3d1	dřívější
tvorbě	tvorba	k1gFnSc3	tvorba
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
syrovosti	syrovost	k1gFnSc2	syrovost
skladeb	skladba	k1gFnPc2	skladba
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
hudba	hudba	k1gFnSc1	hudba
skupiny	skupina	k1gFnSc2	skupina
je	být	k5eAaImIp3nS	být
také	také	k9	také
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
dřívější	dřívější	k2eAgFnSc7d1	dřívější
tvorbou	tvorba	k1gFnSc7	tvorba
melodičtější	melodický	k2eAgFnSc7d2	melodičtější
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
hraje	hrát	k5eAaImIp3nS	hrát
stále	stále	k6eAd1	stále
ve	v	k7c6	v
stejném	stejný	k2eAgNnSc6d1	stejné
složení	složení	k1gNnSc6	složení
<g/>
.	.	kIx.	.
</s>
<s>
Zpívá	zpívat	k5eAaImIp3nS	zpívat
Till	Till	k1gMnSc1	Till
Lindemann	Lindemann	k1gMnSc1	Lindemann
<g/>
,	,	kIx,	,
v	v	k7c6	v
pozadí	pozadí	k1gNnSc6	pozadí
občas	občas	k6eAd1	občas
zpívají	zpívat	k5eAaImIp3nP	zpívat
i	i	k9	i
hlavní	hlavní	k2eAgMnSc1d1	hlavní
kytarista	kytarista	k1gMnSc1	kytarista
Richard	Richard	k1gMnSc1	Richard
Kruspe	Krusp	k1gInSc5	Krusp
a	a	k8xC	a
doprovodný	doprovodný	k2eAgMnSc1d1	doprovodný
kytarista	kytarista	k1gMnSc1	kytarista
Paul	Paul	k1gMnSc1	Paul
Landers	Landers	k1gInSc4	Landers
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
baskytaru	baskytara	k1gFnSc4	baskytara
hraje	hrát	k5eAaImIp3nS	hrát
Oliver	Oliver	k1gInSc1	Oliver
Riedel	Riedlo	k1gNnPc2	Riedlo
<g/>
,	,	kIx,	,
na	na	k7c4	na
bicí	bicí	k2eAgInSc4d1	bicí
Christoph	Christoph	k1gInSc4	Christoph
Schneider	Schneider	k1gMnSc1	Schneider
a	a	k8xC	a
klávesy	klávesa	k1gFnSc2	klávesa
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
elektronickými	elektronický	k2eAgInPc7d1	elektronický
efekty	efekt	k1gInPc7	efekt
má	mít	k5eAaImIp3nS	mít
na	na	k7c4	na
starosti	starost	k1gFnPc4	starost
Christian	Christian	k1gMnSc1	Christian
Lorenz	Lorenz	k1gMnSc1	Lorenz
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
atributem	atribut	k1gInSc7	atribut
skupiny	skupina	k1gFnSc2	skupina
je	být	k5eAaImIp3nS	být
projev	projev	k1gInSc4	projev
zpěváka	zpěvák	k1gMnSc2	zpěvák
<g/>
.	.	kIx.	.
</s>
<s>
Typické	typický	k2eAgFnPc1d1	typická
jsou	být	k5eAaImIp3nP	být
frázované	frázovaný	k2eAgFnPc1d1	frázovaná
pasáže	pasáž	k1gFnPc1	pasáž
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
střídají	střídat	k5eAaImIp3nP	střídat
s	s	k7c7	s
melodickým	melodický	k2eAgInSc7d1	melodický
zpěvem	zpěv	k1gInSc7	zpěv
<g/>
.	.	kIx.	.
</s>
<s>
Lindemann	Lindemann	k1gInSc1	Lindemann
zpívá	zpívat	k5eAaImIp3nS	zpívat
většinu	většina	k1gFnSc4	většina
písní	píseň	k1gFnPc2	píseň
silným	silný	k2eAgInSc7d1	silný
hlasem	hlas	k1gInSc7	hlas
v	v	k7c6	v
hlubších	hluboký	k2eAgFnPc6d2	hlubší
polohách	poloha	k1gFnPc6	poloha
(	(	kIx(	(
<g/>
bas	bas	k1gInSc4	bas
<g/>
,	,	kIx,	,
baryton	baryton	k1gInSc4	baryton
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštností	zvláštnost	k1gFnSc7	zvláštnost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
Lindemann	Lindemann	k1gInSc1	Lindemann
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
hlásku	hláska	k1gFnSc4	hláska
"	"	kIx"	"
<g/>
r	r	kA	r
<g/>
"	"	kIx"	"
jako	jako	k8xS	jako
alveolární	alveolární	k2eAgFnSc4d1	alveolární
vibrantu	vibranta	k1gFnSc4	vibranta
(	(	kIx(	(
<g/>
tedy	tedy	k9	tedy
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
se	se	k3xPyFc4	se
hláska	hláska	k1gFnSc1	hláska
r	r	kA	r
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
málo	málo	k6eAd1	málo
obvyklé	obvyklý	k2eAgNnSc1d1	obvyklé
(	(	kIx(	(
<g/>
nejčastěji	často	k6eAd3	často
se	se	k3xPyFc4	se
r	r	kA	r
vyslovuje	vyslovovat	k5eAaImIp3nS	vyslovovat
jako	jako	k9	jako
znělá	znělý	k2eAgFnSc1d1	znělá
uvulární	uvulární	k2eAgFnSc1d1	uvulární
frikativa	frikativa	k1gFnSc1	frikativa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zpěvák	Zpěvák	k1gMnSc1	Zpěvák
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
způsob	způsob	k1gInSc1	způsob
výslovnosti	výslovnost	k1gFnSc2	výslovnost
přichází	přicházet	k5eAaImIp3nS	přicházet
v	v	k7c6	v
hlubokých	hluboký	k2eAgFnPc6d1	hluboká
polohách	poloha	k1gFnPc6	poloha
sám	sám	k3xTgMnSc1	sám
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
skupiny	skupina	k1gFnSc2	skupina
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
malé	malý	k2eAgFnSc2d1	malá
vesničky	vesnička	k1gFnSc2	vesnička
Ramstein-Miesenbach	Ramstein-Miesenbacha	k1gFnPc2	Ramstein-Miesenbacha
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
spolkové	spolkový	k2eAgFnSc6d1	spolková
zemi	zem	k1gFnSc6	zem
Porýní-Falc	Porýní-Falc	k1gFnSc4	Porýní-Falc
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
USAF	USAF	kA	USAF
jednu	jeden	k4xCgFnSc4	jeden
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
leteckých	letecký	k2eAgFnPc2d1	letecká
základen	základna	k1gFnPc2	základna
(	(	kIx(	(
<g/>
na	na	k7c6	na
ní	on	k3xPp3gFnSc6	on
je	být	k5eAaImIp3nS	být
momentálně	momentálně	k6eAd1	momentálně
umístěna	umístěn	k2eAgFnSc1d1	umístěna
435	[number]	k4	435
<g/>
.	.	kIx.	.
letka	letka	k1gFnSc1	letka
USAFE	USAFE	kA	USAFE
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Základna	základna	k1gFnSc1	základna
Ramstein	Ramsteina	k1gFnPc2	Ramsteina
byla	být	k5eAaImAgFnS	být
28	[number]	k4	28
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1988	[number]	k4	1988
dějištěm	dějiště	k1gNnSc7	dějiště
letecké	letecký	k2eAgFnSc2d1	letecká
přehlídky	přehlídka	k1gFnSc2	přehlídka
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
se	se	k3xPyFc4	se
dvě	dva	k4xCgNnPc1	dva
letadla	letadlo	k1gNnPc1	letadlo
typu	typ	k1gInSc2	typ
Aermacchi	Aermacch	k1gFnPc4	Aermacch
MB-339	MB-339	k1gFnPc2	MB-339
z	z	k7c2	z
italské	italský	k2eAgFnSc2d1	italská
akrobatické	akrobatický	k2eAgFnSc2d1	akrobatická
skupiny	skupina	k1gFnSc2	skupina
Frecce	Frecce	k1gFnSc2	Frecce
Tricolori	Tricolor	k1gFnSc2	Tricolor
při	při	k7c6	při
provádění	provádění	k1gNnSc6	provádění
manévru	manévr	k1gInSc2	manévr
Cardioide	Cardioid	k1gInSc5	Cardioid
(	(	kIx(	(
<g/>
lze	lze	k6eAd1	lze
přeložit	přeložit	k5eAaPmF	přeložit
jako	jako	k8xC	jako
probodnuté	probodnutý	k2eAgNnSc4d1	probodnuté
srdce	srdce	k1gNnSc4	srdce
<g/>
)	)	kIx)	)
srazila	srazit	k5eAaPmAgFnS	srazit
ve	v	k7c6	v
vzduchu	vzduch	k1gInSc6	vzduch
a	a	k8xC	a
následný	následný	k2eAgInSc1d1	následný
pád	pád	k1gInSc1	pád
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
do	do	k7c2	do
diváckého	divácký	k2eAgInSc2d1	divácký
prostoru	prostor	k1gInSc2	prostor
a	a	k8xC	a
požár	požár	k1gInSc1	požár
leteckého	letecký	k2eAgNnSc2d1	letecké
paliva	palivo	k1gNnSc2	palivo
způsobil	způsobit	k5eAaPmAgInS	způsobit
neštěstí	neštěstí	k1gNnSc4	neštěstí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
si	se	k3xPyFc3	se
vyžádalo	vyžádat	k5eAaPmAgNnS	vyžádat
bezprostředně	bezprostředně	k6eAd1	bezprostředně
po	po	k7c6	po
havárii	havárie	k1gFnSc6	havárie
40	[number]	k4	40
lidských	lidský	k2eAgInPc2d1	lidský
životů	život	k1gInPc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Stovky	stovka	k1gFnPc1	stovka
přihlížejících	přihlížející	k2eAgMnPc2d1	přihlížející
byly	být	k5eAaImAgFnP	být
zraněny	zranit	k5eAaPmNgFnP	zranit
a	a	k8xC	a
konečná	konečný	k2eAgFnSc1d1	konečná
bilance	bilance	k1gFnSc1	bilance
katastrofy	katastrofa	k1gFnSc2	katastrofa
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
vyšplhala	vyšplhat	k5eAaPmAgFnS	vyšplhat
na	na	k7c4	na
72	[number]	k4	72
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
události	událost	k1gFnSc6	událost
byla	být	k5eAaImAgFnS	být
přijata	přijat	k2eAgFnSc1d1	přijata
nová	nový	k2eAgFnSc1d1	nová
bezpečností	bezpečnost	k1gFnPc2	bezpečnost
opatření	opatření	k1gNnSc4	opatření
pro	pro	k7c4	pro
letecké	letecký	k2eAgFnPc4d1	letecká
přehlídky	přehlídka	k1gFnPc4	přehlídka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
skupiny	skupina	k1gFnPc1	skupina
létají	létat	k5eAaImIp3nP	létat
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
vymezeném	vymezený	k2eAgInSc6d1	vymezený
prostoru	prostor	k1gInSc6	prostor
mimo	mimo	k7c4	mimo
sektor	sektor	k1gInSc4	sektor
diváků	divák	k1gMnPc2	divák
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
předešlo	předejít	k5eAaPmAgNnS	předejít
opakování	opakování	k1gNnSc1	opakování
tragédie	tragédie	k1gFnSc2	tragédie
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
text	text	k1gInSc1	text
písně	píseň	k1gFnSc2	píseň
Rammstein	Rammsteina	k1gFnPc2	Rammsteina
z	z	k7c2	z
alba	album	k1gNnSc2	album
Herzeleid	Herzeleid	k1gInSc4	Herzeleid
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
katastrofu	katastrofa	k1gFnSc4	katastrofa
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
<g/>
.	.	kIx.	.
</s>
<s>
Richard	Richard	k1gMnSc1	Richard
(	(	kIx(	(
<g/>
kytarista	kytarista	k1gMnSc1	kytarista
kapely	kapela	k1gFnSc2	kapela
<g/>
)	)	kIx)	)
však	však	k9	však
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
název	název	k1gInSc4	název
vymyslel	vymyslet	k5eAaPmAgMnS	vymyslet
již	již	k6eAd1	již
před	před	k7c7	před
neštěstím	neštěstí	k1gNnSc7	neštěstí
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
do	do	k7c2	do
názvu	název	k1gInSc2	název
přidal	přidat	k5eAaPmAgMnS	přidat
druhé	druhý	k4xOgFnSc6	druhý
"	"	kIx"	"
<g/>
M	M	kA	M
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
od	od	k7c2	od
onoho	onen	k3xDgNnSc2	onen
místa	místo	k1gNnSc2	místo
odlišili	odlišit	k5eAaPmAgMnP	odlišit
(	(	kIx(	(
<g/>
přičemž	přičemž	k6eAd1	přičemž
"	"	kIx"	"
<g/>
Rammstein	Rammstein	k1gInSc1	Rammstein
<g/>
"	"	kIx"	"
v	v	k7c6	v
němčině	němčina	k1gFnSc6	němčina
znamená	znamenat	k5eAaImIp3nS	znamenat
beranidlo	beranidlo	k1gNnSc1	beranidlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
skupiny	skupina	k1gFnSc2	skupina
byla	být	k5eAaImAgFnS	být
pojmenována	pojmenovat	k5eAaPmNgFnS	pojmenovat
i	i	k9	i
planetka	planetka	k1gFnSc1	planetka
110393	[number]	k4	110393
Rammstein	Rammsteina	k1gFnPc2	Rammsteina
<g/>
,	,	kIx,	,
objevená	objevený	k2eAgFnSc1d1	objevená
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
Rammstein	Rammsteina	k1gFnPc2	Rammsteina
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
svými	svůj	k3xOyFgInPc7	svůj
pyrotechnickými	pyrotechnický	k2eAgInPc7d1	pyrotechnický
efekty	efekt	k1gInPc7	efekt
při	při	k7c6	při
koncertech	koncert	k1gInPc6	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Skoro	skoro	k6eAd1	skoro
ke	k	k7c3	k
každému	každý	k3xTgInSc3	každý
songu	song	k1gInSc3	song
mají	mít	k5eAaImIp3nP	mít
přiřazený	přiřazený	k2eAgInSc4d1	přiřazený
efekt	efekt	k1gInSc4	efekt
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
při	při	k7c6	při
songu	song	k1gInSc6	song
"	"	kIx"	"
<g/>
Asche	Asche	k1gNnSc1	Asche
zu	zu	k?	zu
Asche	Asch	k1gInSc2	Asch
<g/>
"	"	kIx"	"
hoří	hořet	k5eAaImIp3nP	hořet
mikrofóny	mikrofón	k1gInPc4	mikrofón
před	před	k7c7	před
Richardem	Richard	k1gMnSc7	Richard
a	a	k8xC	a
Paulem	Paul	k1gMnSc7	Paul
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
"	"	kIx"	"
<g/>
Feuer	Feuero	k1gNnPc2	Feuero
Frei	Frei	k1gNnSc2	Frei
<g/>
!	!	kIx.	!
</s>
<s>
<g/>
"	"	kIx"	"
si	se	k3xPyFc3	se
členové	člen	k1gMnPc1	člen
kapely	kapela	k1gFnSc2	kapela
nasadí	nasadit	k5eAaPmIp3nP	nasadit
náhubky	náhubka	k1gFnPc1	náhubka
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yQgMnPc2	který
následně	následně	k6eAd1	následně
"	"	kIx"	"
<g/>
plivou	plivou	k?	plivou
<g/>
"	"	kIx"	"
oheň	oheň	k1gInSc1	oheň
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Mein	Mein	k1gMnSc1	Mein
Teil	Teil	k1gMnSc1	Teil
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
Till	Till	k1gInSc1	Till
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
mikrofonu	mikrofon	k1gInSc6	mikrofon
připevněný	připevněný	k2eAgInSc4d1	připevněný
nůž	nůž	k1gInSc4	nůž
<g/>
.	.	kIx.	.
</s>
<s>
Přiveze	přivézt	k5eAaPmIp3nS	přivézt
si	se	k3xPyFc3	se
velký	velký	k2eAgInSc4d1	velký
kotel	kotel	k1gInSc4	kotel
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
Flake	Flake	k1gInSc1	Flake
<g/>
"	"	kIx"	"
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
klávesami	klávesa	k1gFnPc7	klávesa
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
si	se	k3xPyFc3	se
Till	Till	k1gInSc1	Till
vezme	vzít	k5eAaPmIp3nS	vzít
plamenomet	plamenomet	k1gInSc4	plamenomet
a	a	k8xC	a
začne	začít	k5eAaPmIp3nS	začít
"	"	kIx"	"
<g/>
zatápět	zatápět	k5eAaImF	zatápět
<g/>
"	"	kIx"	"
pod	pod	k7c7	pod
kotlem	kotel	k1gInSc7	kotel
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Du	Du	k?	Du
Riechst	Riechst	k1gMnSc1	Riechst
So	So	kA	So
Gut	Gut	k1gMnSc1	Gut
<g/>
"	"	kIx"	"
<g/>
:	:	kIx,	:
Till	Till	k1gInSc1	Till
přijde	přijít	k5eAaPmIp3nS	přijít
s	s	k7c7	s
lukem	luk	k1gInSc7	luk
,	,	kIx,	,
<g/>
ze	z	k7c2	z
kterého	který	k3yIgInSc2	který
začnou	začít	k5eAaPmIp3nP	začít
létat	létat	k5eAaImF	létat
jiskry	jiskra	k1gFnPc4	jiskra
na	na	k7c4	na
všechny	všechen	k3xTgFnPc4	všechen
strany	strana	k1gFnPc4	strana
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Till	Till	k1gMnSc1	Till
Lindemann	Lindemann	k1gMnSc1	Lindemann
vlastní	vlastní	k2eAgFnSc4d1	vlastní
pyrotechnickou	pyrotechnický	k2eAgFnSc4d1	pyrotechnická
licenci	licence	k1gFnSc4	licence
a	a	k8xC	a
zpočátku	zpočátku	k6eAd1	zpočátku
efekty	efekt	k1gInPc4	efekt
připravoval	připravovat	k5eAaImAgInS	připravovat
sám	sám	k3xTgInSc1	sám
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
od	od	k7c2	od
nehody	nehoda	k1gFnSc2	nehoda
na	na	k7c6	na
koncertu	koncert	k1gInSc6	koncert
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
27	[number]	k4	27
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
část	část	k1gFnSc1	část
hořících	hořící	k2eAgFnPc2d1	hořící
kulis	kulisa	k1gFnPc2	kulisa
spadla	spadnout	k5eAaPmAgFnS	spadnout
těsně	těsně	k6eAd1	těsně
vedle	vedle	k7c2	vedle
diváků	divák	k1gMnPc2	divák
(	(	kIx(	(
<g/>
nehoda	nehoda	k1gFnSc1	nehoda
se	s	k7c7	s
shodou	shoda	k1gFnSc7	shoda
okolností	okolnost	k1gFnPc2	okolnost
stala	stát	k5eAaPmAgFnS	stát
při	při	k7c6	při
jejich	jejich	k3xOp3gInSc6	jejich
stém	stý	k4xOgInSc6	stý
koncertu	koncert	k1gInSc6	koncert
100	[number]	k4	100
Jahre	Jahr	k1gInSc5	Jahr
Rammstein	Rammstein	k1gMnSc1	Rammstein
při	při	k7c6	při
<g />
.	.	kIx.	.
</s>
<s>
písni	píseň	k1gFnSc3	píseň
Heirate	Heirat	k1gInSc5	Heirat
Mich	Micha	k1gFnPc2	Micha
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Rammstein	Rammstein	k1gInSc4	Rammstein
najímají	najímat	k5eAaImIp3nP	najímat
specializovaný	specializovaný	k2eAgInSc4d1	specializovaný
tým	tým	k1gInSc4	tým
pyrotechniků	pyrotechnik	k1gMnPc2	pyrotechnik
na	na	k7c4	na
přípravu	příprava	k1gFnSc4	příprava
svých	svůj	k3xOyFgInPc2	svůj
efektů	efekt	k1gInPc2	efekt
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
-	-	kIx~	-
ECHO	echo	k1gNnSc1	echo
-	-	kIx~	-
Most	most	k1gInSc1	most
successful	successfula	k1gFnPc2	successfula
national	nationat	k5eAaImAgInS	nationat
Act	Act	k1gFnSc2	Act
abroad	abroad	k1gInSc1	abroad
2012	[number]	k4	2012
-	-	kIx~	-
ECHO	echo	k1gNnSc1	echo
-	-	kIx~	-
Best	Best	k2eAgInSc1d1	Best
Rock	rock	k1gInSc1	rock
<g/>
/	/	kIx~	/
<g/>
Alternative	Alternativ	k1gInSc5	Alternativ
national	nationat	k5eAaImAgInS	nationat
2011	[number]	k4	2011
-	-	kIx~	-
prestižní	prestižní	k2eAgNnSc1d1	prestižní
americké	americký	k2eAgNnSc1d1	americké
ocenění	ocenění	k1gNnSc1	ocenění
Golden	Goldna	k1gFnPc2	Goldna
Gods	Godsa	k1gFnPc2	Godsa
Award	Awardo	k1gNnPc2	Awardo
2011	[number]	k4	2011
-	-	kIx~	-
nejlepší	dobrý	k2eAgFnSc1d3	nejlepší
živá	živý	k2eAgFnSc1d1	živá
kapela	kapela	k1gFnSc1	kapela
2011	[number]	k4	2011
-	-	kIx~	-
ECHO	echo	k1gNnSc1	echo
-	-	kIx~	-
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
národní	národní	k2eAgNnSc1d1	národní
video	video	k1gNnSc1	video
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Ich	Ich	k1gMnPc1	Ich
Tu	ten	k3xDgFnSc4	ten
Dir	Dir	k1gFnSc4	Dir
Weh	Weh	k1gFnSc2	Weh
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
2010	[number]	k4	2010
-	-	kIx~	-
Kerrang	Kerranga	k1gFnPc2	Kerranga
<g/>
!	!	kIx.	!
</s>
<s>
Awards	Awards	k1gInSc1	Awards
2010	[number]	k4	2010
<g/>
:	:	kIx,	:
Inspirace	inspirace	k1gFnSc1	inspirace
Kerrang	Kerranga	k1gFnPc2	Kerranga
<g/>
!	!	kIx.	!
</s>
<s>
2006	[number]	k4	2006
-	-	kIx~	-
ECHO	echo	k1gNnSc1	echo
-	-	kIx~	-
Rock	rock	k1gInSc1	rock
<g/>
/	/	kIx~	/
<g/>
Alternative	Alternativ	k1gInSc5	Alternativ
national	nationat	k5eAaImAgInS	nationat
-	-	kIx~	-
videoklip	videoklip	k1gInSc1	videoklip
"	"	kIx"	"
<g/>
Rosenrot	Rosenrot	k1gInSc1	Rosenrot
<g/>
"	"	kIx"	"
2006	[number]	k4	2006
-	-	kIx~	-
Edison	Edison	k1gInSc1	Edison
Awards	Awards	k1gInSc1	Awards
:	:	kIx,	:
Best	Best	k1gInSc1	Best
alternative	alternativ	k1gInSc5	alternativ
music	music	k1gMnSc1	music
-	-	kIx~	-
"	"	kIx"	"
<g/>
Rosenrot	Rosenrot	k1gInSc1	Rosenrot
<g/>
"	"	kIx"	"
2006	[number]	k4	2006
-	-	kIx~	-
Emma	Emma	k1gFnSc1	Emma
Gaala	Gaala	k1gMnSc1	Gaala
:	:	kIx,	:
Best	Best	k1gMnSc1	Best
international	internationat	k5eAaPmAgMnS	internationat
artist	artist	k1gInSc4	artist
/	/	kIx~	/
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
mezinárodní	mezinárodní	k2eAgMnSc1d1	mezinárodní
umělec	umělec	k1gMnSc1	umělec
2005	[number]	k4	2005
-	-	kIx~	-
ECHO	echo	k1gNnSc1	echo
-	-	kIx~	-
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
živý	živý	k2eAgInSc1d1	živý
koncert	koncert	k1gInSc1	koncert
<g />
.	.	kIx.	.
</s>
<s>
2005	[number]	k4	2005
-	-	kIx~	-
ECHO	echo	k1gNnSc1	echo
-	-	kIx~	-
nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
album	album	k1gNnSc1	album
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Alternative	Alternativ	k1gInSc5	Alternativ
za	za	k7c4	za
Reise	Reis	k1gMnPc4	Reis
<g/>
,	,	kIx,	,
Reise	Reise	k1gFnSc1	Reise
2005	[number]	k4	2005
-	-	kIx~	-
Krone	Kron	k1gMnSc5	Kron
za	za	k7c4	za
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
živé	živý	k2eAgNnSc4d1	živé
vystoupení	vystoupení	k1gNnSc4	vystoupení
2005	[number]	k4	2005
-	-	kIx~	-
Comet	Comet	k1gMnSc1	Comet
(	(	kIx(	(
<g/>
viva	viva	k1gMnSc1	viva
<g/>
)	)	kIx)	)
za	za	k7c4	za
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
video	video	k1gNnSc4	video
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Keine	Kein	k1gMnSc5	Kein
Lust	Lust	k2eAgMnSc1d1	Lust
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
2005	[number]	k4	2005
-	-	kIx~	-
World	World	k1gInSc1	World
Music	Musice	k1gFnPc2	Musice
Awards	Awards	k1gInSc4	Awards
za	za	k7c7	za
nejlepší	dobrý	k2eAgFnSc7d3	nejlepší
německou	německý	k2eAgFnSc7d1	německá
<g />
.	.	kIx.	.
</s>
<s>
skupinu	skupina	k1gFnSc4	skupina
2005	[number]	k4	2005
-	-	kIx~	-
MTV	MTV	kA	MTV
Europe	Europ	k1gInSc5	Europ
Music	Musice	k1gInPc2	Musice
Awards	Awards	k1gInSc4	Awards
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
německou	německý	k2eAgFnSc4d1	německá
skupinu	skupina	k1gFnSc4	skupina
2005	[number]	k4	2005
-	-	kIx~	-
Echo	echo	k1gNnSc1	echo
za	za	k7c4	za
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
živé	živý	k2eAgNnSc4d1	živé
vystoupení	vystoupení	k1gNnSc4	vystoupení
2004	[number]	k4	2004
-	-	kIx~	-
MetalHammer	MetalHammer	k1gInSc1	MetalHammer
za	za	k7c4	za
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
video	video	k1gNnSc4	video
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Mein	Mein	k1gMnSc1	Mein
Teil	Teil	k1gMnSc1	Teil
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
2004	[number]	k4	2004
-	-	kIx~	-
MetalHammer	MetalHammer	k1gInSc1	MetalHammer
za	za	k7c4	za
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
album	album	k1gNnSc4	album
(	(	kIx(	(
<g/>
Reise	Reise	k1gFnSc1	Reise
<g/>
,	,	kIx,	,
Reise	Reise	k1gFnSc1	Reise
<g/>
)	)	kIx)	)
2004	[number]	k4	2004
-	-	kIx~	-
MetalHammer	MetalHammer	k1gInSc1	MetalHammer
za	za	k7c4	za
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
song	song	k1gInSc4	song
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Mein	Mein	k1gMnSc1	Mein
Teil	Teil	k1gMnSc1	Teil
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
2002	[number]	k4	2002
-	-	kIx~	-
Kerrang	Kerranga	k1gFnPc2	Kerranga
<g/>
!	!	kIx.	!
</s>
<s>
Awards	Awards	k1gInSc1	Awards
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
německou	německý	k2eAgFnSc4d1	německá
skupinu	skupina	k1gFnSc4	skupina
2002	[number]	k4	2002
-	-	kIx~	-
Echo	echo	k1gNnSc1	echo
Nu-Metal	Nu-Metal	k1gFnSc2	Nu-Metal
1999	[number]	k4	1999
-	-	kIx~	-
Echo	echo	k1gNnSc1	echo
za	za	k7c4	za
nejoblíbenější	oblíbený	k2eAgFnSc4d3	nejoblíbenější
německou	německý	k2eAgFnSc4d1	německá
skupinu	skupina	k1gFnSc4	skupina
1998	[number]	k4	1998
-	-	kIx~	-
Viva	Viv	k1gInSc2	Viv
Comet	Comet	k1gInSc1	Comet
za	za	k7c4	za
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
živé	živý	k2eAgNnSc4d1	živé
vystoupení	vystoupení	k1gNnSc4	vystoupení
1998	[number]	k4	1998
-	-	kIx~	-
Echo	echo	k1gNnSc1	echo
za	za	k7c4	za
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
video	video	k1gNnSc4	video
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Du	Du	k?	Du
hast	hast	k1gInSc1	hast
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
2005	[number]	k4	2005
-	-	kIx~	-
Kerrang	Kerrang	k1gInSc1	Kerrang
Awards	Awards	k1gInSc1	Awards
-	-	kIx~	-
nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
živé	živý	k2eAgNnSc1d1	živé
vystoupení	vystoupení	k1gNnSc1	vystoupení
2005	[number]	k4	2005
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
Neo	Neo	k?	Neo
Award	Award	k1gInSc4	Award
-	-	kIx~	-
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
stažené	stažený	k2eAgNnSc4d1	stažené
album	album	k1gNnSc4	album
2005	[number]	k4	2005
-	-	kIx~	-
Comet	Comet	k1gMnSc1	Comet
(	(	kIx(	(
<g/>
Viva	Viva	k1gMnSc1	Viva
<g/>
)	)	kIx)	)
-	-	kIx~	-
nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
živé	živý	k2eAgNnSc1d1	živé
vystoupení	vystoupení	k1gNnSc1	vystoupení
2002	[number]	k4	2002
-	-	kIx~	-
Nejlepší	dobrý	k2eAgNnSc1d3	nejlepší
video	video	k1gNnSc1	video
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Sonne	Sonn	k1gMnSc5	Sonn
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
1999	[number]	k4	1999
-	-	kIx~	-
42	[number]	k4	42
<g/>
.	.	kIx.	.
ročník	ročník	k1gInSc1	ročník
Grammy	Gramma	k1gFnSc2	Gramma
Awards	Awardsa	k1gFnPc2	Awardsa
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
metalovou	metalový	k2eAgFnSc4d1	metalová
skupinu	skupina	k1gFnSc4	skupina
1998	[number]	k4	1998
-	-	kIx~	-
MTV	MTV	kA	MTV
Europe	Europ	k1gInSc5	Europ
Music	Musice	k1gInPc2	Musice
Awards	Awards	k1gInSc4	Awards
za	za	k7c4	za
<g />
.	.	kIx.	.
</s>
<s>
nejlepší	dobrý	k2eAgInSc1d3	nejlepší
rockový	rockový	k2eAgInSc1d1	rockový
zpěv	zpěv	k1gInSc1	zpěv
album	album	k1gNnSc1	album
Liebe	Lieb	k1gInSc5	Lieb
Ist	Ist	k1gMnPc1	Ist
Für	Für	k1gFnSc1	Für
Alle	Alle	k1gFnSc1	Alle
Da	Da	k1gFnSc1	Da
6	[number]	k4	6
<g/>
×	×	k?	×
album	album	k1gNnSc4	album
Herzeleid	Herzeleida	k1gFnPc2	Herzeleida
2	[number]	k4	2
<g/>
×	×	k?	×
singl	singl	k1gInSc1	singl
Engel	Engel	k1gInSc1	Engel
2	[number]	k4	2
<g/>
×	×	k?	×
album	album	k1gNnSc1	album
Rosenrot	Rosenrota	k1gFnPc2	Rosenrota
album	album	k1gNnSc1	album
Liebe	Lieb	k1gInSc5	Lieb
Ist	Ist	k1gMnPc1	Ist
Für	Für	k1gFnSc1	Für
Alle	Alle	k1gFnSc1	Alle
Da	Da	k1gFnSc1	Da
8	[number]	k4	8
<g/>
×	×	k?	×
album	album	k1gNnSc4	album
Herzeleid	Herzeleida	k1gFnPc2	Herzeleida
2	[number]	k4	2
<g/>
×	×	k?	×
album	album	k1gNnSc4	album
Sehnsucht	Sehnsuchta	k1gFnPc2	Sehnsuchta
2	[number]	k4	2
<g/>
×	×	k?	×
album	album	k1gNnSc4	album
Mutter	Muttra	k1gFnPc2	Muttra
2	[number]	k4	2
<g/>
×	×	k?	×
album	album	k1gNnSc4	album
Reise	Reis	k1gMnSc2	Reis
<g/>
,	,	kIx,	,
Reise	Reis	k1gMnSc2	Reis
album	album	k1gNnSc1	album
Rosenrot	Rosenrot	k1gMnSc1	Rosenrot
</s>
