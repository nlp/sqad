<s>
Časové	časový	k2eAgNnSc1d1	časové
pásmo	pásmo	k1gNnSc1	pásmo
je	být	k5eAaImIp3nS	být
ta	ten	k3xDgFnSc1	ten
část	část	k1gFnSc1	část
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
používá	používat	k5eAaImIp3nS	používat
stejný	stejný	k2eAgInSc4d1	stejný
standardní	standardní	k2eAgInSc4d1	standardní
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
používali	používat	k5eAaImAgMnP	používat
lidé	člověk	k1gMnPc1	člověk
sluneční	sluneční	k2eAgFnSc7d1	sluneční
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
ovšem	ovšem	k9	ovšem
tu	ten	k3xDgFnSc4	ten
nevýhodu	nevýhoda	k1gFnSc4	nevýhoda
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
místa	místo	k1gNnSc2	místo
k	k	k7c3	k
místu	místo	k1gNnSc3	místo
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rozvojem	rozvoj	k1gInSc7	rozvoj
dopravy	doprava	k1gFnSc2	doprava
a	a	k8xC	a
komunikace	komunikace	k1gFnSc1	komunikace
byla	být	k5eAaImAgFnS	být
tato	tento	k3xDgFnSc1	tento
nevýhoda	nevýhoda	k1gFnSc1	nevýhoda
stále	stále	k6eAd1	stále
výraznější	výrazný	k2eAgFnSc1d2	výraznější
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
přešlo	přejít	k5eAaPmAgNnS	přejít
na	na	k7c4	na
pásmový	pásmový	k2eAgInSc4d1	pásmový
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
celá	celý	k2eAgFnSc1d1	celá
oblast	oblast	k1gFnSc1	oblast
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
15	[number]	k4	15
°	°	k?	°
kolem	kolem	k7c2	kolem
daného	daný	k2eAgInSc2d1	daný
poledníku	poledník	k1gInSc2	poledník
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
stejný	stejný	k2eAgInSc4d1	stejný
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
určen	určit	k5eAaPmNgInS	určit
svým	svůj	k3xOyFgInSc7	svůj
posunem	posun	k1gInSc7	posun
od	od	k7c2	od
UTC	UTC	kA	UTC
<g/>
,	,	kIx,	,
koordinovaného	koordinovaný	k2eAgInSc2d1	koordinovaný
světového	světový	k2eAgInSc2d1	světový
času	čas	k1gInSc2	čas
(	(	kIx(	(
<g/>
většinou	většinou	k6eAd1	většinou
je	být	k5eAaImIp3nS	být
posun	posun	k1gInSc1	posun
určen	určit	k5eAaPmNgInS	určit
celistvým	celistvý	k2eAgInSc7d1	celistvý
počtem	počet	k1gInSc7	počet
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
i	i	k9	i
výjimky	výjimka	k1gFnPc1	výjimka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgNnSc7d1	základní
časovým	časový	k2eAgNnSc7d1	časové
pásmem	pásmo	k1gNnSc7	pásmo
je	být	k5eAaImIp3nS	být
pásmo	pásmo	k1gNnSc1	pásmo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
platí	platit	k5eAaImIp3nP	platit
UTC	UTC	kA	UTC
a	a	k8xC	a
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
kolem	kolem	k7c2	kolem
nultého	nultý	k4xOgInSc2	nultý
poledníku	poledník	k1gInSc2	poledník
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
prochází	procházet	k5eAaImIp3nS	procházet
Královskou	královský	k2eAgFnSc7d1	královská
observatoří	observatoř	k1gFnSc7	observatoř
v	v	k7c6	v
Greenwichi	Greenwich	k1gInSc6	Greenwich
(	(	kIx(	(
<g/>
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
se	se	k3xPyFc4	se
pásmovému	pásmový	k2eAgInSc3d1	pásmový
času	čas	k1gInSc3	čas
odpovídajícímu	odpovídající	k2eAgInSc3d1	odpovídající
UTC	UTC	kA	UTC
někdy	někdy	k6eAd1	někdy
říká	říkat	k5eAaImIp3nS	říkat
Greenwichský	greenwichský	k2eAgInSc1d1	greenwichský
čas	čas	k1gInSc1	čas
(	(	kIx(	(
<g/>
GMT	GMT	kA	GMT
<g/>
,	,	kIx,	,
Greenwich	Greenwich	k1gInSc4	Greenwich
Mean	Mean	k1gNnSc4	Mean
Time	Tim	k1gFnSc2	Tim
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgNnPc1d1	ostatní
časová	časový	k2eAgNnPc1d1	časové
pásma	pásmo	k1gNnPc1	pásmo
jsou	být	k5eAaImIp3nP	být
popsána	popsat	k5eAaPmNgNnP	popsat
rozdílem	rozdíl	k1gInSc7	rozdíl
počtu	počet	k1gInSc2	počet
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
o	o	k7c4	o
které	který	k3yQgFnPc4	který
se	se	k3xPyFc4	se
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
platný	platný	k2eAgInSc4d1	platný
čas	čas	k1gInSc4	čas
liší	lišit	k5eAaImIp3nP	lišit
od	od	k7c2	od
UTC	UTC	kA	UTC
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
středoevropský	středoevropský	k2eAgInSc4d1	středoevropský
čas	čas	k1gInSc4	čas
(	(	kIx(	(
<g/>
SEČ	SEČ	kA	SEČ
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
označen	označit	k5eAaPmNgInS	označit
jako	jako	k9	jako
UTC	UTC	kA	UTC
<g/>
+	+	kIx~	+
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
vzhledem	vzhled	k1gInSc7	vzhled
k	k	k7c3	k
UTC	UTC	kA	UTC
posunut	posunut	k2eAgInSc1d1	posunut
o	o	k7c4	o
hodinu	hodina	k1gFnSc4	hodina
napřed	napřed	k6eAd1	napřed
(	(	kIx(	(
<g/>
tzn.	tzn.	kA	tzn.
ve	v	k7c6	v
chvíli	chvíle	k1gFnSc6	chvíle
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
12.00	[number]	k4	12.00
UTC	UTC	kA	UTC
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
Evropě	Evropa	k1gFnSc6	Evropa
13.00	[number]	k4	13.00
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
polokouli	polokoule	k1gFnSc6	polokoule
je	být	k5eAaImIp3nS	být
čas	čas	k1gInSc4	čas
oproti	oproti	k7c3	oproti
UTC	UTC	kA	UTC
posunut	posunout	k5eAaPmNgInS	posunout
zpět	zpět	k6eAd1	zpět
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
USA	USA	kA	USA
platí	platit	k5eAaImIp3nS	platit
časové	časový	k2eAgNnSc4d1	časové
pásmo	pásmo	k1gNnSc4	pásmo
UTC-	UTC-	k1gFnSc2	UTC-
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
polokouli	polokoule	k1gFnSc6	polokoule
platí	platit	k5eAaImIp3nS	platit
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
před	před	k7c7	před
UTC	UTC	kA	UTC
(	(	kIx(	(
<g/>
např.	např.	kA	např.
v	v	k7c6	v
Tokiu	Tokio	k1gNnSc6	Tokio
<g/>
,	,	kIx,	,
Japonsko	Japonsko	k1gNnSc1	Japonsko
je	být	k5eAaImIp3nS	být
časové	časový	k2eAgNnSc4d1	časové
pásmo	pásmo	k1gNnSc4	pásmo
UTC	UTC	kA	UTC
<g/>
+	+	kIx~	+
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ideální	ideální	k2eAgNnPc1d1	ideální
časová	časový	k2eAgNnPc1d1	časové
pásma	pásmo	k1gNnPc1	pásmo
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
lišila	lišit	k5eAaImAgFnS	lišit
o	o	k7c4	o
celý	celý	k2eAgInSc4d1	celý
počet	počet	k1gInSc4	počet
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
by	by	kYmCp3nS	by
Zemi	zem	k1gFnSc4	zem
rozdělila	rozdělit	k5eAaPmAgFnS	rozdělit
na	na	k7c4	na
přesné	přesný	k2eAgInPc4d1	přesný
pruhy	pruh	k1gInPc4	pruh
široké	široký	k2eAgNnSc4d1	široké
15	[number]	k4	15
°	°	k?	°
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
by	by	kYmCp3nS	by
však	však	k9	však
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
procházejí	procházet	k5eAaImIp3nP	procházet
hranicí	hranice	k1gFnSc7	hranice
takových	takový	k3xDgFnPc2	takový
teoretických	teoretický	k2eAgFnPc2d1	teoretická
pásem	pásmo	k1gNnPc2	pásmo
<g/>
,	,	kIx,	,
by	by	kYmCp3nP	by
musely	muset	k5eAaImAgFnP	muset
používat	používat	k5eAaImF	používat
dvě	dva	k4xCgNnPc4	dva
časová	časový	k2eAgNnPc4d1	časové
pásma	pásmo	k1gNnPc4	pásmo
<g/>
,	,	kIx,	,
jakkoli	jakkoli	k8xS	jakkoli
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
jejich	jejich	k3xOp3gFnSc1	jejich
rozloha	rozloha	k1gFnSc1	rozloha
malá	malý	k2eAgFnSc1d1	malá
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
praktických	praktický	k2eAgInPc2d1	praktický
důvodů	důvod	k1gInPc2	důvod
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
stanovily	stanovit	k5eAaPmAgInP	stanovit
takové	takový	k3xDgInPc1	takový
tvary	tvar	k1gInPc1	tvar
časových	časový	k2eAgNnPc2d1	časové
pásem	pásmo	k1gNnPc2	pásmo
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
přizpůsobují	přizpůsobovat	k5eAaImIp3nP	přizpůsobovat
hranicím	hranice	k1gFnPc3	hranice
států	stát	k1gInPc2	stát
či	či	k8xC	či
jiných	jiný	k2eAgInPc2d1	jiný
územních	územní	k2eAgInPc2d1	územní
celků	celek	k1gInPc2	celek
<g/>
.	.	kIx.	.
</s>
<s>
Některá	některý	k3yIgNnPc1	některý
časová	časový	k2eAgNnPc1d1	časové
pásma	pásmo	k1gNnPc1	pásmo
pak	pak	k6eAd1	pak
používají	používat	k5eAaImIp3nP	používat
čas	čas	k1gInSc4	čas
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
od	od	k7c2	od
UTC	UTC	kA	UTC
neliší	lišit	k5eNaImIp3nS	lišit
o	o	k7c4	o
celý	celý	k2eAgInSc4d1	celý
počet	počet	k1gInSc4	počet
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
o	o	k7c4	o
půlhodiny	půlhodina	k1gFnPc4	půlhodina
<g/>
,	,	kIx,	,
či	či	k8xC	či
dokonce	dokonce	k9	dokonce
čtvrthodiny	čtvrthodina	k1gFnSc2	čtvrthodina
<g/>
.	.	kIx.	.
</s>
<s>
Čas	čas	k1gInSc1	čas
aktuálně	aktuálně	k6eAd1	aktuálně
platný	platný	k2eAgInSc4d1	platný
na	na	k7c6	na
daném	daný	k2eAgNnSc6d1	dané
místě	místo	k1gNnSc6	místo
není	být	k5eNaImIp3nS	být
dán	dát	k5eAaPmNgInS	dát
pouze	pouze	k6eAd1	pouze
časovým	časový	k2eAgNnSc7d1	časové
pásmem	pásmo	k1gNnSc7	pásmo
<g/>
,	,	kIx,	,
do	do	k7c2	do
kterého	který	k3yQgNnSc2	který
místo	místo	k1gNnSc1	místo
patří	patřit	k5eAaImIp3nS	patřit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
daném	daný	k2eAgNnSc6d1	dané
místě	místo	k1gNnSc6	místo
používán	používat	k5eAaImNgInS	používat
letní	letní	k2eAgInSc1d1	letní
čas	čas	k1gInSc1	čas
<g/>
.	.	kIx.	.
</s>
<s>
Skutečná	skutečný	k2eAgFnSc1d1	skutečná
odchylka	odchylka	k1gFnSc1	odchylka
místně	místně	k6eAd1	místně
platného	platný	k2eAgInSc2d1	platný
času	čas	k1gInSc2	čas
od	od	k7c2	od
UTC	UTC	kA	UTC
se	se	k3xPyFc4	se
tedy	tedy	k9	tedy
může	moct	k5eAaImIp3nS	moct
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
měnit	měnit	k5eAaImF	měnit
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
po	po	k7c6	po
přičtení	přičtení	k1gNnSc6	přičtení
časového	časový	k2eAgInSc2d1	časový
rozdílu	rozdíl	k1gInSc2	rozdíl
k	k	k7c3	k
UTC	UTC	kA	UTC
překročí	překročit	k5eAaPmIp3nP	překročit
půlnoc	půlnoc	k1gFnSc1	půlnoc
<g/>
,	,	kIx,	,
změní	změnit	k5eAaPmIp3nS	změnit
se	se	k3xPyFc4	se
i	i	k9	i
datum	datum	k1gNnSc1	datum
platné	platný	k2eAgNnSc1d1	platné
na	na	k7c6	na
daném	daný	k2eAgNnSc6d1	dané
místě	místo	k1gNnSc6	místo
(	(	kIx(	(
<g/>
např.	např.	kA	např.
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
pátek	pátek	k1gInSc4	pátek
<g/>
,	,	kIx,	,
18.00	[number]	k4	18.00
UTC	UTC	kA	UTC
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
v	v	k7c6	v
pásmu	pásmo	k1gNnSc6	pásmo
UTC	UTC	kA	UTC
<g/>
+	+	kIx~	+
<g/>
8	[number]	k4	8
je	být	k5eAaImIp3nS	být
sobota	sobota	k1gFnSc1	sobota
<g/>
,	,	kIx,	,
0	[number]	k4	0
<g/>
2.00	[number]	k4	2.00
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Datum	datum	k1gNnSc1	datum
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nS	měnit
též	též	k9	též
při	při	k7c6	při
překročení	překročení	k1gNnSc6	překročení
tzv.	tzv.	kA	tzv.
datové	datový	k2eAgInPc1d1	datový
čáry	čár	k1gInPc1	čár
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
prochází	procházet	k5eAaImIp3nS	procházet
zhruba	zhruba	k6eAd1	zhruba
po	po	k7c6	po
180	[number]	k4	180
<g/>
.	.	kIx.	.
poledníku	poledník	k1gInSc2	poledník
a	a	k8xC	a
která	který	k3yQgFnSc1	který
tvoří	tvořit	k5eAaImIp3nS	tvořit
hranici	hranice	k1gFnSc4	hranice
mezi	mezi	k7c7	mezi
pásmy	pásmo	k1gNnPc7	pásmo
UTC	UTC	kA	UTC
<g/>
+	+	kIx~	+
<g/>
12	[number]	k4	12
a	a	k8xC	a
UTC-	UTC-	k1gMnSc1	UTC-
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
překročení	překročení	k1gNnSc6	překročení
této	tento	k3xDgFnSc2	tento
čáry	čára	k1gFnSc2	čára
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
západ	západ	k1gInSc4	západ
(	(	kIx(	(
<g/>
což	což	k3yQnSc1	což
ovšem	ovšem	k9	ovšem
znamená	znamenat	k5eAaImIp3nS	znamenat
ze	z	k7c2	z
západní	západní	k2eAgFnSc2d1	západní
polokoule	polokoule	k1gFnSc2	polokoule
na	na	k7c6	na
východní	východní	k2eAgFnSc6d1	východní
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
přechází	přecházet	k5eAaImIp3nS	přecházet
z	z	k7c2	z
pásma	pásmo	k1gNnSc2	pásmo
UTC-12	UTC-12	k1gFnSc2	UTC-12
do	do	k7c2	do
pásma	pásmo	k1gNnSc2	pásmo
UTC	UTC	kA	UTC
<g/>
+	+	kIx~	+
<g/>
12	[number]	k4	12
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
platný	platný	k2eAgInSc1d1	platný
čas	čas	k1gInSc1	čas
se	se	k3xPyFc4	se
zvýší	zvýšit	k5eAaPmIp3nS	zvýšit
o	o	k7c4	o
24	[number]	k4	24
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
tzn.	tzn.	kA	tzn.
čas	čas	k1gInSc1	čas
se	se	k3xPyFc4	se
nemění	měnit	k5eNaImIp3nS	měnit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
datum	datum	k1gNnSc1	datum
se	se	k3xPyFc4	se
posune	posunout	k5eAaPmIp3nS	posunout
o	o	k7c4	o
jeden	jeden	k4xCgInSc4	jeden
den	den	k1gInSc4	den
vpřed	vpřed	k6eAd1	vpřed
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
přechodu	přechod	k1gInSc6	přechod
směrem	směr	k1gInSc7	směr
na	na	k7c4	na
východ	východ	k1gInSc4	východ
opačně	opačně	k6eAd1	opačně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujícím	následující	k2eAgInSc6d1	následující
seznamu	seznam	k1gInSc6	seznam
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
každého	každý	k3xTgNnSc2	každý
časového	časový	k2eAgNnSc2d1	časové
pásma	pásmo	k1gNnSc2	pásmo
uvedena	uveden	k2eAgFnSc1d1	uvedena
odchylka	odchylka	k1gFnSc1	odchylka
od	od	k7c2	od
UTC	UTC	kA	UTC
<g/>
,	,	kIx,	,
jednopísmenné	jednopísmenný	k2eAgNnSc1d1	jednopísmenné
označení	označení	k1gNnSc1	označení
používané	používaný	k2eAgNnSc1d1	používané
námořníky	námořník	k1gMnPc7	námořník
(	(	kIx(	(
<g/>
které	který	k3yRgNnSc4	který
lze	lze	k6eAd1	lze
pomocí	pomocí	k7c2	pomocí
hláskovací	hláskovací	k2eAgFnSc2d1	hláskovací
tabulky	tabulka	k1gFnSc2	tabulka
převést	převést	k5eAaPmF	převést
na	na	k7c4	na
jednoslovný	jednoslovný	k2eAgInSc4d1	jednoslovný
název	název	k1gInSc4	název
<g/>
)	)	kIx)	)
a	a	k8xC	a
případná	případný	k2eAgNnPc1d1	případné
dále	daleko	k6eAd2	daleko
používaná	používaný	k2eAgNnPc1d1	používané
označení	označení	k1gNnPc1	označení
časového	časový	k2eAgNnSc2d1	časové
pásma	pásmo	k1gNnSc2	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
každého	každý	k3xTgNnSc2	každý
pásma	pásmo	k1gNnSc2	pásmo
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
uveden	uveden	k2eAgInSc1d1	uveden
seznam	seznam	k1gInSc1	seznam
států	stát	k1gInPc2	stát
či	či	k8xC	či
území	území	k1gNnPc2	území
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
pásmo	pásmo	k1gNnSc1	pásmo
používají	používat	k5eAaImIp3nP	používat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
seznamu	seznam	k1gInSc6	seznam
jsou	být	k5eAaImIp3nP	být
hvězdičkou	hvězdička	k1gFnSc7	hvězdička
(	(	kIx(	(
<g/>
*	*	kIx~	*
<g/>
)	)	kIx)	)
označena	označen	k2eAgNnPc4d1	označeno
území	území	k1gNnPc4	území
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgFnPc6	který
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
letní	letní	k2eAgInSc4d1	letní
čas	čas	k1gInSc4	čas
<g/>
.	.	kIx.	.
</s>
<s>
Bakerův	Bakerův	k2eAgInSc4d1	Bakerův
ostrov	ostrov	k1gInSc4	ostrov
(	(	kIx(	(
<g/>
neobydlený	obydlený	k2eNgInSc4d1	neobydlený
<g/>
)	)	kIx)	)
Howlandův	Howlandův	k2eAgInSc4d1	Howlandův
ostrov	ostrov	k1gInSc4	ostrov
<g/>
(	(	kIx(	(
<g/>
neobydlený	obydlený	k2eNgMnSc1d1	neobydlený
<g/>
)	)	kIx)	)
Americká	americký	k2eAgFnSc1d1	americká
Samoa	Samoa	k1gFnSc1	Samoa
Atol	atol	k1gInSc4	atol
Midway	Midwaa	k1gFnSc2	Midwaa
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
Niue	Niu	k1gInSc2	Niu
Cookovy	Cookův	k2eAgInPc4d1	Cookův
ostrovy	ostrov	k1gInPc4	ostrov
Části	část	k1gFnSc2	část
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
Polynésie	Polynésie	k1gFnSc2	Polynésie
Johnstonův	Johnstonův	k2eAgInSc1d1	Johnstonův
atol	atol	k1gInSc1	atol
Části	část	k1gFnSc2	část
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
Havaj	Havaj	k1gFnSc4	Havaj
<g/>
,	,	kIx,	,
Aleutské	aleutský	k2eAgInPc4d1	aleutský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
)	)	kIx)	)
<g/>
*	*	kIx~	*
Část	část	k1gFnSc1	část
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
Polynésie	Polynésie	k1gFnSc2	Polynésie
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Markézy	Markéza	k1gFnPc4	Markéza
<g/>
)	)	kIx)	)
Části	část	k1gFnPc4	část
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
Polynésie	Polynésie	k1gFnSc2	Polynésie
Část	část	k1gFnSc4	část
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
Aljaška	Aljaška	k1gFnSc1	Aljaška
<g/>
)	)	kIx)	)
<g/>
*	*	kIx~	*
Části	část	k1gFnSc2	část
Kanady	Kanada	k1gFnSc2	Kanada
(	(	kIx(	(
<g/>
Yukon	Yukon	k1gInSc1	Yukon
<g/>
,	,	kIx,	,
Britská	britský	k2eAgFnSc1d1	britská
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
<g/>
)	)	kIx)	)
<g/>
*	*	kIx~	*
Části	část	k1gFnSc3	část
Mexika	Mexiko	k1gNnSc2	Mexiko
(	(	kIx(	(
<g/>
Baja	Baj	k2eAgNnPc1d1	Baj
California	Californium	k1gNnPc1	Californium
<g/>
)	)	kIx)	)
<g/>
*	*	kIx~	*
Pitcairnovy	Pitcairnův	k2eAgInPc4d1	Pitcairnův
ostrovy	ostrov	k1gInPc4	ostrov
Části	část	k1gFnSc2	část
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Nevada	Nevada	k1gFnSc1	Nevada
<g/>
,	,	kIx,	,
Oregon	Oregon	k1gInSc1	Oregon
<g/>
,	,	kIx,	,
Washington	Washington	k1gInSc1	Washington
<g/>
)	)	kIx)	)
<g/>
*	*	kIx~	*
Části	část	k1gFnSc2	část
Kanady	Kanada	k1gFnSc2	Kanada
(	(	kIx(	(
<g/>
Alberta	Albert	k1gMnSc2	Albert
<g/>
)	)	kIx)	)
<g/>
*	*	kIx~	*
Části	část	k1gFnSc3	část
Mexika	Mexiko	k1gNnSc2	Mexiko
(	(	kIx(	(
<g/>
Chihuahua	Chihuahua	k1gFnSc1	Chihuahua
<g/>
,	,	kIx,	,
Sonora	sonora	k1gFnSc1	sonora
<g/>
,	,	kIx,	,
Baja	Baja	k1gMnSc1	Baja
California	Californium	k1gNnSc2	Californium
Sur	Sur	k1gMnSc1	Sur
<g/>
,	,	kIx,	,
Sinaloa	Sinaloa	k1gMnSc1	Sinaloa
<g/>
,	,	kIx,	,
Nayarit	Nayarit	k1gInSc1	Nayarit
<g/>
)	)	kIx)	)
<g/>
*	*	kIx~	*
Části	část	k1gFnPc4	část
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
Arizona	Arizona	k1gFnSc1	Arizona
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Colorado	Colorado	k1gNnSc1	Colorado
<g/>
)	)	kIx)	)
<g/>
*	*	kIx~	*
Belize	Belize	k1gFnSc1	Belize
Část	část	k1gFnSc1	část
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
(	(	kIx(	(
<g/>
Galapágy	Galapágy	k1gFnPc1	Galapágy
<g/>
)	)	kIx)	)
Salvador	Salvador	k1gInSc1	Salvador
Guatemala	Guatemala	k1gFnSc1	Guatemala
Honduras	Honduras	k1gInSc4	Honduras
Části	část	k1gFnSc2	část
Kanady	Kanada	k1gFnSc2	Kanada
(	(	kIx(	(
<g/>
Manitoba	Manitoba	k1gFnSc1	Manitoba
<g/>
,	,	kIx,	,
Saskatchewan	Saskatchewan	k1gInSc1	Saskatchewan
<g/>
)	)	kIx)	)
<g/>
*	*	kIx~	*
Kostarika	Kostarika	k1gFnSc1	Kostarika
Většina	většina	k1gFnSc1	většina
Mexika	Mexiko	k1gNnSc2	Mexiko
<g/>
*	*	kIx~	*
Nikaragua	Nikaragua	k1gFnSc1	Nikaragua
Části	část	k1gFnSc2	část
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
(	(	kIx(	(
<g/>
Illinois	Illinois	k1gInSc1	Illinois
<g/>
,	,	kIx,	,
Texas	Texas	k1gInSc1	Texas
<g/>
)	)	kIx)	)
<g/>
*	*	kIx~	*
Bahamy	Bahamy	k1gFnPc1	Bahamy
<g/>
<g />
.	.	kIx.	.
</s>
<s>
*	*	kIx~	*
Ekvádor	Ekvádor	k1gInSc1	Ekvádor
Haiti	Haiti	k1gNnSc2	Haiti
Část	část	k1gFnSc1	část
Chile	Chile	k1gNnPc7	Chile
(	(	kIx(	(
<g/>
Velikonoční	velikonoční	k2eAgInSc4d1	velikonoční
ostrov	ostrov	k1gInSc4	ostrov
<g/>
)	)	kIx)	)
<g/>
*	*	kIx~	*
Jamajka	Jamajka	k1gFnSc1	Jamajka
Části	část	k1gFnSc2	část
Kanady	Kanada	k1gFnSc2	Kanada
(	(	kIx(	(
<g/>
Ontario	Ontario	k1gNnSc1	Ontario
<g/>
,	,	kIx,	,
Québec	Québec	k1gInSc1	Québec
<g/>
)	)	kIx)	)
<g/>
*	*	kIx~	*
Kajmanské	Kajmanský	k2eAgInPc4d1	Kajmanský
ostrovy	ostrov	k1gInPc4	ostrov
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
Kuba	Kuba	k1gFnSc1	Kuba
<g/>
*	*	kIx~	*
Panama	panama	k2eAgNnSc1d1	panama
Peru	Peru	k1gNnSc1	Peru
Turks	Turksa	k1gFnPc2	Turksa
a	a	k8xC	a
Caicos	Caicosa	k1gFnPc2	Caicosa
<g/>
*	*	kIx~	*
část	část	k1gFnSc1	část
Mexika	Mexiko	k1gNnSc2	Mexiko
(	(	kIx(	(
<g/>
Quintana	Quintana	k1gFnSc1	Quintana
Roo	Roo	k1gFnSc2	Roo
<g/>
)	)	kIx)	)
Části	část	k1gFnSc2	část
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
východní	východní	k2eAgFnSc1d1	východní
pobřeží	pobřeží	k1gNnSc4	pobřeží
Washington	Washington	k1gInSc1	Washington
<g/>
,	,	kIx,	,
D.C.	D.C.	k1gMnSc1	D.C.
<g/>
,	,	kIx,	,
New	New	k1gMnSc1	New
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
<g/>
*	*	kIx~	*
Anguilla	Anguilla	k1gMnSc1	Anguilla
Americké	americký	k2eAgInPc4d1	americký
i	i	k8xC	i
Britské	britský	k2eAgInPc4d1	britský
Panenské	panenský	k2eAgInPc4d1	panenský
ostrovy	ostrov	k1gInPc4	ostrov
Antigua	Antigu	k1gInSc2	Antigu
a	a	k8xC	a
Barbuda	Barbuda	k1gMnSc1	Barbuda
Aruba	Aruba	k1gMnSc1	Aruba
Barbados	Barbados	k1gInSc4	Barbados
Bermudy	Bermudy	k1gFnPc4	Bermudy
<g/>
*	*	kIx~	*
Bolívie	Bolívie	k1gFnSc1	Bolívie
Části	část	k1gFnSc2	část
Brazílie	Brazílie	k1gFnSc2	Brazílie
(	(	kIx(	(
<g/>
Amazonas	Amazonas	k1gMnSc1	Amazonas
<g/>
)	)	kIx)	)
<g/>
*	*	kIx~	*
Curaçao	curaçao	k1gNnSc1	curaçao
Dominika	Dominik	k1gMnSc2	Dominik
Dominikánská	dominikánský	k2eAgFnSc1d1	Dominikánská
republika	republika	k1gFnSc1	republika
Falklandy	Falklanda	k1gFnSc2	Falklanda
<g/>
*	*	kIx~	*
Grenada	Grenada	k1gFnSc1	Grenada
Část	část	k1gFnSc1	část
Grónska	Grónsko	k1gNnSc2	Grónsko
<g />
.	.	kIx.	.
</s>
<s>
<g/>
*	*	kIx~	*
Guadeloupe	Guadeloupe	k1gMnPc4	Guadeloupe
Guyana	Guyana	k1gFnSc1	Guyana
Části	část	k1gFnSc2	část
Kanady	Kanada	k1gFnSc2	Kanada
(	(	kIx(	(
<g/>
Nové	Nové	k2eAgNnSc1d1	Nové
Skotsko	Skotsko	k1gNnSc1	Skotsko
<g/>
)	)	kIx)	)
<g/>
*	*	kIx~	*
Martinik	Martinik	k1gInSc1	Martinik
Montserrat	Montserrat	k1gInSc1	Montserrat
Paraguay	Paraguay	k1gFnSc1	Paraguay
<g/>
*	*	kIx~	*
Portoriko	Portoriko	k1gNnSc1	Portoriko
Sint	Sinta	k1gFnPc2	Sinta
Maarten	Maartno	k1gNnPc2	Maartno
Svatá	svatat	k5eAaImIp3nS	svatat
Lucie	Lucie	k1gFnSc1	Lucie
Svatý	svatý	k1gMnSc1	svatý
Kryštof	Kryštof	k1gMnSc1	Kryštof
a	a	k8xC	a
Nevis	viset	k5eNaImRp2nS	viset
Svatý	svatý	k1gMnSc1	svatý
Vincenc	Vincenc	k1gMnSc1	Vincenc
a	a	k8xC	a
Grenadiny	grenadina	k1gFnSc2	grenadina
Trinidad	Trinidad	k1gInSc1	Trinidad
a	a	k8xC	a
Tobago	Tobago	k6eAd1	Tobago
Venezuela	Venezuela	k1gFnSc1	Venezuela
Části	část	k1gFnSc2	část
Kanady	Kanada	k1gFnSc2	Kanada
(	(	kIx(	(
<g/>
Newfoundland	Newfoundland	k1gInSc1	Newfoundland
<g/>
)	)	kIx)	)
<g/>
*	*	kIx~	*
Argentina	Argentina	k1gFnSc1	Argentina
Části	část	k1gFnSc2	část
Brazílie	Brazílie	k1gFnSc2	Brazílie
Brasila	Brasila	k1gFnSc2	Brasila
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Rio	Rio	k1gFnPc1	Rio
de	de	k?	de
Janeiro	Janeiro	k1gNnSc1	Janeiro
<g/>
,	,	kIx,	,
Sã	Sã	k1gFnSc1	Sã
Paulo	Paula	k1gFnSc5	Paula
<g/>
*	*	kIx~	*
Francouzská	francouzský	k2eAgFnSc1d1	francouzská
Guyana	Guyana	k1gFnSc1	Guyana
Většina	většina	k1gFnSc1	většina
Grónska	Grónsko	k1gNnSc2	Grónsko
<g/>
*	*	kIx~	*
Chile	Chile	k1gNnSc2	Chile
(	(	kIx(	(
<g/>
kontinentální	kontinentální	k2eAgNnSc1d1	kontinentální
<g/>
)	)	kIx)	)
<g/>
*	*	kIx~	*
Saint-Pierre	Saint-Pierr	k1gMnSc5	Saint-Pierr
a	a	k8xC	a
Miquelon	Miquelon	k1gInSc1	Miquelon
<g/>
*	*	kIx~	*
Surinam	Surinam	k1gInSc1	Surinam
Uruguay	Uruguay	k1gFnSc1	Uruguay
Část	část	k1gFnSc1	část
Brazílie	Brazílie	k1gFnSc1	Brazílie
(	(	kIx(	(
<g/>
Fernando	Fernanda	k1gFnSc5	Fernanda
de	de	k?	de
Noronha	Noronha	k1gFnSc1	Noronha
<g/>
)	)	kIx)	)
Jižní	jižní	k2eAgFnSc1d1	jižní
Georgie	Georgie	k1gFnSc1	Georgie
a	a	k8xC	a
Jižní	jižní	k2eAgInPc1d1	jižní
Sandwichovy	Sandwichův	k2eAgInPc1d1	Sandwichův
ostrovy	ostrov	k1gInPc1	ostrov
Část	část	k1gFnSc1	část
Grónska	Grónsko	k1gNnSc2	Grónsko
<g/>
*	*	kIx~	*
<g />
.	.	kIx.	.
</s>
<s>
Kapverdy	Kapverdy	k6eAd1	Kapverdy
Část	část	k1gFnSc1	část
Portugalska	Portugalsko	k1gNnSc2	Portugalsko
(	(	kIx(	(
<g/>
Azorské	azorský	k2eAgInPc1d1	azorský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
)	)	kIx)	)
*	*	kIx~	*
Burkina	Burkina	k1gFnSc1	Burkina
Faso	Faso	k6eAd1	Faso
Faerské	Faerský	k2eAgInPc4d1	Faerský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
*	*	kIx~	*
Gambie	Gambie	k1gFnSc1	Gambie
Ghana	Ghana	k1gFnSc1	Ghana
Část	část	k1gFnSc1	část
Grónska	Grónsko	k1gNnSc2	Grónsko
Guinea	guinea	k1gFnSc2	guinea
Guinea-Bissau	Guinea-Bissaus	k1gInSc2	Guinea-Bissaus
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
*	*	kIx~	*
Island	Island	k1gInSc1	Island
Libérie	Libérie	k1gFnSc2	Libérie
Mali	Mali	k1gNnSc2	Mali
Maroko	Maroko	k1gNnSc1	Maroko
Mauritánie	Mauritánie	k1gFnSc2	Mauritánie
Pobřeží	pobřeží	k1gNnSc2	pobřeží
slonoviny	slonovina	k1gFnSc2	slonovina
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
(	(	kIx(	(
<g/>
kontinentální	kontinentální	k2eAgInSc1d1	kontinentální
<g/>
,	,	kIx,	,
Madeira	Madeira	k1gFnSc1	Madeira
<g/>
)	)	kIx)	)
<g/>
*	*	kIx~	*
Senegal	Senegal	k1gInSc1	Senegal
Sierra	Sierra	k1gFnSc1	Sierra
Leone	Leo	k1gMnSc5	Leo
Svatá	svatý	k2eAgFnSc1d1	svatá
Helena	Helena	k1gFnSc1	Helena
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Ascension	Ascension	k1gInSc1	Ascension
a	a	k8xC	a
Tristan	Tristan	k1gInSc1	Tristan
da	da	k?	da
Cunha	Cunha	k1gMnSc1	Cunha
Svatý	svatý	k1gMnSc1	svatý
Tomáš	Tomáš	k1gMnSc1	Tomáš
a	a	k8xC	a
Princův	princův	k2eAgInSc1d1	princův
ostrov	ostrov	k1gInSc1	ostrov
Část	část	k1gFnSc4	část
Španělska	Španělsko	k1gNnSc2	Španělsko
(	(	kIx(	(
<g/>
Kanárské	kanárský	k2eAgInPc4d1	kanárský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
)	)	kIx)	)
*	*	kIx~	*
Togo	Togo	k1gNnSc1	Togo
Velká	velká	k1gFnSc1	velká
Británie	Británie	k1gFnSc1	Británie
<g/>
*	*	kIx~	*
Albánie	Albánie	k1gFnSc1	Albánie
<g/>
*	*	kIx~	*
Alžírsko	Alžírsko	k1gNnSc1	Alžírsko
Andorra	Andorra	k1gFnSc1	Andorra
<g/>
*	*	kIx~	*
Angola	Angola	k1gFnSc1	Angola
Belgie	Belgie	k1gFnSc1	Belgie
<g/>
*	*	kIx~	*
Benin	Benin	k1gInSc1	Benin
Bosna	Bosna	k1gFnSc1	Bosna
a	a	k8xC	a
Hercegovina	Hercegovina	k1gFnSc1	Hercegovina
<g/>
*	*	kIx~	*
Čad	Čad	k1gInSc1	Čad
Černá	černý	k2eAgFnSc1d1	černá
Hora	hora	k1gFnSc1	hora
<g/>
*	*	kIx~	*
Česko	Česko	k1gNnSc1	Česko
<g/>
*	*	kIx~	*
Dánsko	Dánsko	k1gNnSc1	Dánsko
<g />
.	.	kIx.	.
</s>
<s>
<g/>
*	*	kIx~	*
Francie	Francie	k1gFnSc1	Francie
<g/>
*	*	kIx~	*
Gabon	Gabon	k1gInSc1	Gabon
Gibraltar	Gibraltar	k1gInSc1	Gibraltar
<g/>
*	*	kIx~	*
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
<g/>
*	*	kIx~	*
Itálie	Itálie	k1gFnSc1	Itálie
<g/>
*	*	kIx~	*
Libye	Libye	k1gFnSc1	Libye
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc4	Lichtenštejnsko
<g/>
*	*	kIx~	*
Lucembursko	Lucembursko	k1gNnSc1	Lucembursko
<g/>
*	*	kIx~	*
Kamerun	Kamerun	k1gInSc1	Kamerun
Kongo	Kongo	k1gNnSc1	Kongo
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
*	*	kIx~	*
Makedonie	Makedonie	k1gFnSc1	Makedonie
<g/>
*	*	kIx~	*
Malta	Malta	k1gFnSc1	Malta
<g/>
*	*	kIx~	*
Monako	Monako	k1gNnSc1	Monako
<g/>
*	*	kIx~	*
Namibie	Namibie	k1gFnSc1	Namibie
<g/>
*	*	kIx~	*
Německo	Německo	k1gNnSc1	Německo
<g/>
*	*	kIx~	*
Niger	Niger	k1gInSc1	Niger
Nigérie	Nigérie	k1gFnSc2	Nigérie
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
<g/>
*	*	kIx~	*
Norsko	Norsko	k1gNnSc1	Norsko
<g/>
*	*	kIx~	*
<g />
.	.	kIx.	.
</s>
<s>
Polsko	Polsko	k1gNnSc1	Polsko
<g/>
*	*	kIx~	*
Rakousko	Rakousko	k1gNnSc1	Rakousko
<g/>
*	*	kIx~	*
Rovníková	rovníkový	k2eAgFnSc1d1	Rovníková
Guinea	Guinea	k1gFnSc1	Guinea
San	San	k1gFnSc2	San
Marino	Marina	k1gFnSc5	Marina
<g/>
*	*	kIx~	*
Slovensko	Slovensko	k1gNnSc1	Slovensko
<g/>
*	*	kIx~	*
Slovinsko	Slovinsko	k1gNnSc1	Slovinsko
<g/>
*	*	kIx~	*
Srbsko	Srbsko	k1gNnSc1	Srbsko
<g/>
*	*	kIx~	*
Středoafrická	středoafrický	k2eAgFnSc1d1	Středoafrická
republika	republika	k1gFnSc1	republika
Svalbard	Svalbard	k1gInSc1	Svalbard
<g/>
*	*	kIx~	*
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
*	*	kIx~	*
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
*	*	kIx~	*
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
*	*	kIx~	*
Tunisko	Tunisko	k1gNnSc1	Tunisko
Vatikán	Vatikán	k1gInSc1	Vatikán
<g/>
*	*	kIx~	*
Botswana	Botswana	k1gFnSc1	Botswana
Bulharsko	Bulharsko	k1gNnSc4	Bulharsko
<g/>
*	*	kIx~	*
Burundi	Burundi	k1gNnSc4	Burundi
Demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
Kongo	Kongo	k1gNnSc1	Kongo
Egypt	Egypt	k1gInSc1	Egypt
Estonsko	Estonsko	k1gNnSc4	Estonsko
<g/>
<g />
.	.	kIx.	.
</s>
<s>
*	*	kIx~	*
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
*	*	kIx~	*
Pásmo	pásmo	k1gNnSc1	pásmo
Gazy	Gaza	k1gFnSc2	Gaza
<g/>
*	*	kIx~	*
Izrael	Izrael	k1gInSc1	Izrael
<g/>
*	*	kIx~	*
Jihoafrická	jihoafrický	k2eAgFnSc1d1	Jihoafrická
republika	republika	k1gFnSc1	republika
Jordánsko	Jordánsko	k1gNnSc4	Jordánsko
<g/>
*	*	kIx~	*
Kypr	Kypr	k1gInSc1	Kypr
<g/>
*	*	kIx~	*
Lotyšsko	Lotyšsko	k1gNnSc1	Lotyšsko
<g/>
*	*	kIx~	*
Lesotho	Lesot	k1gMnSc2	Lesot
Libanon	Libanon	k1gInSc1	Libanon
<g/>
*	*	kIx~	*
Litva	Litva	k1gFnSc1	Litva
<g/>
*	*	kIx~	*
Malawi	Malawi	k1gNnSc1	Malawi
Moldavsko	Moldavsko	k1gNnSc1	Moldavsko
<g/>
*	*	kIx~	*
Mosambik	Mosambik	k1gInSc1	Mosambik
Rumunsko	Rumunsko	k1gNnSc1	Rumunsko
<g/>
*	*	kIx~	*
Rwanda	Rwanda	k1gFnSc1	Rwanda
Řecko	Řecko	k1gNnSc4	Řecko
<g/>
*	*	kIx~	*
Svazijsko	Svazijsko	k1gNnSc1	Svazijsko
Sýrie	Sýrie	k1gFnSc1	Sýrie
<g/>
*	*	kIx~	*
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
*	*	kIx~	*
Ukrajina	Ukrajina	k1gFnSc1	Ukrajina
<g/>
<g />
.	.	kIx.	.
</s>
<s>
*	*	kIx~	*
Západní	západní	k2eAgInSc4d1	západní
břeh	břeh	k1gInSc4	břeh
Jordánu	Jordán	k1gInSc2	Jordán
<g/>
*	*	kIx~	*
Zambie	Zambie	k1gFnSc1	Zambie
Zimbabwe	Zimbabwe	k1gInSc4	Zimbabwe
Bahrajn	Bahrajn	k1gInSc1	Bahrajn
Bělorusko	Bělorusko	k1gNnSc1	Bělorusko
<g/>
*	*	kIx~	*
Džibutsko	Džibutsko	k1gNnSc1	Džibutsko
Eritrea	Eritrea	k1gFnSc1	Eritrea
Etiopie	Etiopie	k1gFnSc1	Etiopie
Irák	Irák	k1gInSc1	Irák
<g/>
*	*	kIx~	*
Jemen	Jemen	k1gInSc1	Jemen
Katar	katar	k1gMnSc1	katar
Keňa	Keňa	k1gFnSc1	Keňa
Komory	komora	k1gFnSc2	komora
Kuvajt	Kuvajt	k1gInSc1	Kuvajt
Madagaskar	Madagaskar	k1gInSc1	Madagaskar
Mayotte	Mayott	k1gInSc5	Mayott
Část	část	k1gFnSc1	část
Ruska	Rusko	k1gNnSc2	Rusko
(	(	kIx(	(
<g/>
Kaliningradská	kaliningradský	k2eAgFnSc1d1	Kaliningradská
oblast	oblast	k1gFnSc1	oblast
<g/>
)	)	kIx)	)
<g/>
*	*	kIx~	*
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
Somálsko	Somálsko	k1gNnSc1	Somálsko
Súdán	Súdán	k1gInSc4	Súdán
Tanzanie	Tanzanie	k1gFnSc2	Tanzanie
Uganda	Uganda	k1gFnSc1	Uganda
Írán	Írán	k1gInSc1	Írán
<g/>
*	*	kIx~	*
Arménie	Arménie	k1gFnSc1	Arménie
<g/>
*	*	kIx~	*
Ázerbájdžán	Ázerbájdžán	k1gInSc1	Ázerbájdžán
<g/>
*	*	kIx~	*
<g />
.	.	kIx.	.
</s>
<s>
Gruzie	Gruzie	k1gFnSc1	Gruzie
<g/>
*	*	kIx~	*
Mauricius	Mauricius	k1gInSc1	Mauricius
Omán	Omán	k1gInSc1	Omán
Réunion	Réunion	k1gInSc1	Réunion
Evropská	evropský	k2eAgFnSc1d1	Evropská
část	část	k1gFnSc1	část
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
Republika	republika	k1gFnSc1	republika
Krym	Krym	k1gInSc1	Krym
(	(	kIx(	(
<g/>
sporné	sporný	k2eAgNnSc1d1	sporné
území	území	k1gNnSc1	území
<g/>
)	)	kIx)	)
<g/>
*	*	kIx~	*
Seychely	Seychely	k1gFnPc1	Seychely
Spojené	spojený	k2eAgInPc4d1	spojený
arabské	arabský	k2eAgInPc4d1	arabský
emiráty	emirát	k1gInPc4	emirát
Afghánistán	Afghánistán	k1gInSc1	Afghánistán
Části	část	k1gFnSc2	část
Kazachstánu	Kazachstán	k1gInSc2	Kazachstán
<g/>
*	*	kIx~	*
Maledivy	Maledivy	k1gFnPc1	Maledivy
Pákistán	Pákistán	k1gInSc4	Pákistán
Části	část	k1gFnSc2	část
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
*	*	kIx~	*
Tádžikistán	Tádžikistán	k1gInSc1	Tádžikistán
Turkmenistán	Turkmenistán	k1gInSc1	Turkmenistán
Uzbekistán	Uzbekistán	k1gInSc4	Uzbekistán
Indie	Indie	k1gFnSc2	Indie
Šrí	Šrí	k1gFnSc2	Šrí
Lanka	lanko	k1gNnSc2	lanko
Nepál	Nepál	k1gInSc1	Nepál
Bangladéš	Bangladéš	k1gInSc1	Bangladéš
Bhútán	Bhútán	k1gInSc1	Bhútán
Kyrgyzstán	Kyrgyzstán	k1gInSc1	Kyrgyzstán
<g/>
*	*	kIx~	*
Části	část	k1gFnSc2	část
Kazachstánu	Kazachstán	k1gInSc2	Kazachstán
<g/>
<g />
.	.	kIx.	.
</s>
<s>
*	*	kIx~	*
Části	část	k1gFnSc3	část
Ruska	Rusko	k1gNnSc2	Rusko
(	(	kIx(	(
<g/>
Čeljabinská	čeljabinský	k2eAgFnSc1d1	Čeljabinská
oblast	oblast	k1gFnSc1	oblast
<g/>
,	,	kIx,	,
Sverdlovská	sverdlovský	k2eAgFnSc1d1	Sverdlovská
oblast	oblast	k1gFnSc1	oblast
<g/>
)	)	kIx)	)
<g/>
*	*	kIx~	*
Kokosové	kokosový	k2eAgInPc1d1	kokosový
ostrovy	ostrov	k1gInPc1	ostrov
Myanmar	Myanmara	k1gFnPc2	Myanmara
Část	část	k1gFnSc1	část
Indonésie	Indonésie	k1gFnSc1	Indonésie
(	(	kIx(	(
<g/>
Sumatra	Sumatra	k1gFnSc1	Sumatra
<g/>
,	,	kIx,	,
Jáva	Jáva	k1gFnSc1	Jáva
<g/>
,	,	kIx,	,
část	část	k1gFnSc1	část
Kalimantanu	Kalimantan	k1gInSc2	Kalimantan
<g/>
)	)	kIx)	)
Kambodža	Kambodža	k1gFnSc1	Kambodža
Laos	Laos	k1gInSc1	Laos
Část	část	k1gFnSc4	část
Ruska	Rusko	k1gNnSc2	Rusko
(	(	kIx(	(
<g/>
Novosibirská	novosibirský	k2eAgFnSc1d1	Novosibirská
oblast	oblast	k1gFnSc1	oblast
<g/>
)	)	kIx)	)
<g/>
*	*	kIx~	*
Thajsko	Thajsko	k1gNnSc1	Thajsko
Vietnam	Vietnam	k1gInSc1	Vietnam
Vánoční	vánoční	k2eAgInSc4d1	vánoční
ostrov	ostrov	k1gInSc4	ostrov
Část	část	k1gFnSc4	část
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Západní	západní	k2eAgFnSc2d1	západní
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
)	)	kIx)	)
Brunej	Brunej	k1gFnSc1	Brunej
Čína	Čína	k1gFnSc1	Čína
(	(	kIx(	(
<g/>
celá	celý	k2eAgFnSc1d1	celá
<g/>
)	)	kIx)	)
Hongkong	Hongkong	k1gInSc1	Hongkong
Část	část	k1gFnSc1	část
Indonésie	Indonésie	k1gFnSc1	Indonésie
(	(	kIx(	(
<g/>
Sulawesi	Sulawesi	k1gNnSc1	Sulawesi
<g/>
)	)	kIx)	)
Macao	Macao	k1gNnSc1	Macao
Malajsie	Malajsie	k1gFnSc2	Malajsie
Mongolsko	Mongolsko	k1gNnSc1	Mongolsko
(	(	kIx(	(
<g/>
většina	většina	k1gFnSc1	většina
<g/>
)	)	kIx)	)
<g/>
*	*	kIx~	*
Filipíny	Filipíny	k1gFnPc1	Filipíny
Části	část	k1gFnSc2	část
Ruska	Rusko	k1gNnSc2	Rusko
(	(	kIx(	(
<g/>
Krasnojarský	Krasnojarský	k2eAgInSc1d1	Krasnojarský
kraj	kraj	k1gInSc1	kraj
<g/>
)	)	kIx)	)
<g/>
*	*	kIx~	*
Singapur	Singapur	k1gInSc1	Singapur
Čínská	čínský	k2eAgFnSc1d1	čínská
republika	republika	k1gFnSc1	republika
Severní	severní	k2eAgFnSc1d1	severní
Korea	Korea	k1gFnSc1	Korea
Část	část	k1gFnSc1	část
Austrálie	Austrálie	k1gFnSc1	Austrálie
Východní	východní	k2eAgFnSc1d1	východní
<g />
.	.	kIx.	.
</s>
<s>
Timor	Timor	k1gInSc1	Timor
Část	část	k1gFnSc1	část
Indonésie	Indonésie	k1gFnSc1	Indonésie
(	(	kIx(	(
<g/>
Západní	západní	k2eAgFnSc1d1	západní
Papua	Papua	k1gFnSc1	Papua
<g/>
)	)	kIx)	)
Japonsko	Japonsko	k1gNnSc4	Japonsko
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
Palau	Palaus	k1gInSc2	Palaus
Části	část	k1gFnSc2	část
Ruska	Rusko	k1gNnSc2	Rusko
(	(	kIx(	(
<g/>
Irkutská	irkutský	k2eAgFnSc1d1	Irkutská
oblast	oblast	k1gFnSc1	oblast
<g/>
)	)	kIx)	)
<g/>
*	*	kIx~	*
Část	část	k1gFnSc1	část
Austrálie	Austrálie	k1gFnSc2	Austrálie
(	(	kIx(	(
<g/>
Jižní	jižní	k2eAgFnSc2d1	jižní
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
,	,	kIx,	,
Severní	severní	k2eAgNnSc1d1	severní
teritorium	teritorium	k1gNnSc1	teritorium
<g/>
)	)	kIx)	)
Část	část	k1gFnSc1	část
Austrálie	Austrálie	k1gFnSc2	Austrálie
(	(	kIx(	(
<g/>
Queensland	Queensland	k1gInSc1	Queensland
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
Jižní	jižní	k2eAgInSc1d1	jižní
Wales	Wales	k1gInSc1	Wales
<g/>
,	,	kIx,	,
Tasmánie	Tasmánie	k1gFnSc1	Tasmánie
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Victoria	Victorium	k1gNnPc1	Victorium
<g/>
)	)	kIx)	)
Guam	Guam	k1gInSc1	Guam
Části	část	k1gFnSc2	část
Mikronésie	Mikronésie	k1gFnSc2	Mikronésie
Papua-Nová	Papua-Nová	k1gFnSc1	Papua-Nová
Guinea	Guinea	k1gFnSc1	Guinea
(	(	kIx(	(
<g/>
většina	většina	k1gFnSc1	většina
<g/>
)	)	kIx)	)
Části	část	k1gFnPc1	část
Ruska	Rusko	k1gNnSc2	Rusko
(	(	kIx(	(
<g/>
Zabajkalský	zabajkalský	k2eAgInSc1d1	zabajkalský
kraj	kraj	k1gInSc1	kraj
<g/>
)	)	kIx)	)
<g/>
*	*	kIx~	*
Severní	severní	k2eAgFnSc2d1	severní
Mariany	Mariana	k1gFnSc2	Mariana
Nová	nový	k2eAgFnSc1d1	nová
Kaledonie	Kaledonie	k1gFnSc1	Kaledonie
Část	část	k1gFnSc1	část
Austrálie	Austrálie	k1gFnSc1	Austrálie
(	(	kIx(	(
<g/>
Ostrov	ostrov	k1gInSc1	ostrov
lorda	lord	k1gMnSc2	lord
Howa	Howus	k1gMnSc2	Howus
<g/>
)	)	kIx)	)
Části	část	k1gFnPc1	část
Mikronésie	Mikronésie	k1gFnSc2	Mikronésie
Nová	Nová	k1gFnSc1	Nová
Kaledonie	Kaledonie	k1gFnSc1	Kaledonie
Papua-Nová	Papua-Nová	k1gFnSc1	Papua-Nová
Guinea	Guinea	k1gFnSc1	Guinea
(	(	kIx(	(
<g/>
autonomní	autonomní	k2eAgFnSc1d1	autonomní
území	území	k1gNnSc4	území
Bougainville	Bougainville	k1gInSc1	Bougainville
<g/>
)	)	kIx)	)
Části	část	k1gFnSc2	část
<g />
.	.	kIx.	.
</s>
<s>
Ruska	Ruska	k1gFnSc1	Ruska
(	(	kIx(	(
<g/>
Přímořský	přímořský	k2eAgInSc1d1	přímořský
kraj	kraj	k1gInSc1	kraj
<g/>
)	)	kIx)	)
<g/>
*	*	kIx~	*
Šalamounovy	Šalamounův	k2eAgInPc4d1	Šalamounův
ostrovy	ostrov	k1gInPc4	ostrov
Vanuatu	Vanuat	k1gInSc2	Vanuat
Norfolk	Norfolko	k1gNnPc2	Norfolko
Fidži	Fidž	k1gFnSc3	Fidž
Část	část	k1gFnSc1	část
Kiribati	Kiribati	k1gMnSc1	Kiribati
Marshallovy	Marshallův	k2eAgInPc4d1	Marshallův
ostrovy	ostrov	k1gInPc4	ostrov
Nauru	Naur	k1gInSc2	Naur
Nový	nový	k2eAgInSc4d1	nový
Zéland	Zéland	k1gInSc4	Zéland
Části	část	k1gFnSc2	část
Ruska	Rusko	k1gNnSc2	Rusko
(	(	kIx(	(
<g/>
Kamčatský	kamčatský	k2eAgInSc1d1	kamčatský
kraj	kraj	k1gInSc1	kraj
<g/>
)	)	kIx)	)
<g/>
*	*	kIx~	*
Tuvalu	Tuval	k1gInSc6	Tuval
Wakeův	Wakeův	k2eAgInSc4d1	Wakeův
ostrov	ostrov	k1gInSc4	ostrov
Wallis	Wallis	k1gFnSc1	Wallis
a	a	k8xC	a
Futuna	Futuna	k1gFnSc1	Futuna
Část	část	k1gFnSc1	část
Nového	Nového	k2eAgInSc2d1	Nového
Zélandu	Zéland	k1gInSc2	Zéland
(	(	kIx(	(
<g/>
Chathamské	Chathamský	k2eAgInPc1d1	Chathamský
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
)	)	kIx)	)
Část	část	k1gFnSc1	část
Kiribati	Kiribati	k1gFnSc1	Kiribati
Samoa	Samoa	k1gFnSc1	Samoa
Tokelau	Tokelaus	k1gInSc2	Tokelaus
(	(	kIx(	(
<g/>
Nový	nový	k2eAgInSc1d1	nový
Zéland	Zéland	k1gInSc1	Zéland
<g/>
)	)	kIx)	)
Tonga	Tonga	k1gFnSc1	Tonga
Část	část	k1gFnSc1	část
Kiribati	Kiribati	k1gFnSc1	Kiribati
</s>
