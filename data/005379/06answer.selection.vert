<s>
Základním	základní	k2eAgNnSc7d1	základní
časovým	časový	k2eAgNnSc7d1	časové
pásmem	pásmo	k1gNnSc7	pásmo
je	být	k5eAaImIp3nS	být
pásmo	pásmo	k1gNnSc1	pásmo
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
platí	platit	k5eAaImIp3nP	platit
UTC	UTC	kA	UTC
a	a	k8xC	a
které	který	k3yIgNnSc1	který
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
kolem	kolem	k7c2	kolem
nultého	nultý	k4xOgInSc2	nultý
poledníku	poledník	k1gInSc2	poledník
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
prochází	procházet	k5eAaImIp3nS	procházet
Královskou	královský	k2eAgFnSc7d1	královská
observatoří	observatoř	k1gFnSc7	observatoř
v	v	k7c6	v
Greenwichi	Greenwich	k1gInSc6	Greenwich
(	(	kIx(	(
<g/>
Londýn	Londýn	k1gInSc1	Londýn
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc1	Anglie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
