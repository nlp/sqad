<s>
Krkavcovití	Krkavcovitý	k2eAgMnPc1d1	Krkavcovitý
(	(	kIx(	(
<g/>
Corvidae	Corvidae	k1gNnSc7	Corvidae
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
největší	veliký	k2eAgFnSc7d3	veliký
čeledí	čeleď	k1gFnSc7	čeleď
řádu	řád	k1gInSc2	řád
pěvců	pěvec	k1gMnPc2	pěvec
<g/>
.	.	kIx.	.
</s>
<s>
Patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
všežravce	všežravec	k1gMnSc4	všežravec
<g/>
,	,	kIx,	,
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
na	na	k7c6	na
stromech	strom	k1gInPc6	strom
<g/>
,	,	kIx,	,
skalách	skála	k1gFnPc6	skála
nebo	nebo	k8xC	nebo
ve	v	k7c6	v
skalních	skalní	k2eAgFnPc6d1	skalní
dutinách	dutina	k1gFnPc6	dutina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
vyskytujícími	vyskytující	k2eAgInPc7d1	vyskytující
známými	známý	k2eAgInPc7d1	známý
druhy	druh	k1gInPc7	druh
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
vrána	vrána	k1gFnSc1	vrána
obecná	obecná	k1gFnSc1	obecná
(	(	kIx(	(
<g/>
černá	černý	k2eAgFnSc1d1	černá
a	a	k8xC	a
šedá	šedý	k2eAgFnSc1d1	šedá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
havran	havran	k1gMnSc1	havran
polní	polní	k2eAgMnSc1d1	polní
<g/>
,	,	kIx,	,
krkavec	krkavec	k1gMnSc1	krkavec
velký	velký	k2eAgMnSc1d1	velký
<g/>
,	,	kIx,	,
kavka	kavka	k1gFnSc1	kavka
obecná	obecná	k1gFnSc1	obecná
<g/>
,	,	kIx,	,
sojka	sojka	k1gFnSc1	sojka
obecná	obecná	k1gFnSc1	obecná
<g/>
,	,	kIx,	,
straka	straka	k1gFnSc1	straka
obecná	obecná	k1gFnSc1	obecná
<g/>
,	,	kIx,	,
ořešník	ořešník	k1gInSc1	ořešník
kropenatý	kropenatý	k2eAgInSc1d1	kropenatý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
105	[number]	k4	105
(	(	kIx(	(
<g/>
115	[number]	k4	115
<g/>
)	)	kIx)	)
druhů	druh	k1gInPc2	druh
ve	v	k7c6	v
25	[number]	k4	25
rodech	rod	k1gInPc6	rod
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
předek	předek	k1gInSc1	předek
žil	žít	k5eAaImAgInS	žít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
počátkem	počátkem	k7c2	počátkem
třetihor	třetihory	k1gFnPc2	třetihory
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
tato	tento	k3xDgFnSc1	tento
čeleď	čeleď	k1gFnSc1	čeleď
osidluje	osidlovat	k5eAaImIp3nS	osidlovat
téměř	téměř	k6eAd1	téměř
celý	celý	k2eAgInSc1d1	celý
svět	svět	k1gInSc1	svět
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Antarktidy	Antarktida	k1gFnSc2	Antarktida
a	a	k8xC	a
nejsevernější	severní	k2eAgFnSc2d3	nejsevernější
části	část	k1gFnSc2	část
Arktidy	Arktida	k1gFnSc2	Arktida
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
krkavcovití	krkavcovitý	k2eAgMnPc1d1	krkavcovitý
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
inteligentní	inteligentní	k2eAgMnPc1d1	inteligentní
ptáci	pták	k1gMnPc1	pták
s	s	k7c7	s
vysokou	vysoký	k2eAgFnSc7d1	vysoká
přizpůsobivostí	přizpůsobivost	k1gFnSc7	přizpůsobivost
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
jedni	jeden	k4xCgMnPc1	jeden
z	z	k7c2	z
mála	málo	k1gNnSc2	málo
ptáků	pták	k1gMnPc2	pták
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
můžete	moct	k5eAaImIp2nP	moct
běžně	běžně	k6eAd1	běžně
sledovat	sledovat	k5eAaImF	sledovat
při	při	k7c6	při
hře	hra	k1gFnSc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
Krkavcovití	Krkavcovitý	k2eAgMnPc1d1	Krkavcovitý
mají	mít	k5eAaImIp3nP	mít
proměnlivou	proměnlivý	k2eAgFnSc4d1	proměnlivá
velikost	velikost	k1gFnSc4	velikost
těla	tělo	k1gNnSc2	tělo
(	(	kIx(	(
<g/>
16	[number]	k4	16
<g/>
-	-	kIx~	-
<g/>
68	[number]	k4	68
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hlava	hlava	k1gFnSc1	hlava
je	být	k5eAaImIp3nS	být
poměrně	poměrně	k6eAd1	poměrně
velká	velká	k1gFnSc1	velká
se	s	k7c7	s
silným	silný	k2eAgInSc7d1	silný
zobákem	zobák	k1gInSc7	zobák
a	a	k8xC	a
krátkýma	krátký	k2eAgFnPc7d1	krátká
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
silnýma	silný	k2eAgFnPc7d1	silná
nohama	noha	k1gFnPc7	noha
s	s	k7c7	s
mocnými	mocný	k2eAgInPc7d1	mocný
drápy	dráp	k1gInPc7	dráp
<g/>
.	.	kIx.	.
</s>
<s>
Křídla	křídlo	k1gNnPc1	křídlo
bývají	bývat	k5eAaImIp3nP	bývat
širší	široký	k2eAgNnPc1d2	širší
a	a	k8xC	a
zakulacená	zakulacený	k2eAgFnSc1d1	zakulacená
<g/>
,	,	kIx,	,
délka	délka	k1gFnSc1	délka
ocasu	ocas	k1gInSc2	ocas
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
čeledi	čeleď	k1gFnSc2	čeleď
značně	značně	k6eAd1	značně
proměnlivá	proměnlivý	k2eAgFnSc1d1	proměnlivá
<g/>
.	.	kIx.	.
</s>
<s>
Opeření	opeření	k1gNnSc1	opeření
bývá	bývat	k5eAaImIp3nS	bývat
mnohdy	mnohdy	k6eAd1	mnohdy
pestře	pestro	k6eAd1	pestro
zbarvené	zbarvený	k2eAgFnPc1d1	zbarvená
(	(	kIx(	(
<g/>
černé	černý	k2eAgFnPc1d1	černá
barvy	barva	k1gFnPc1	barva
rodu	rod	k1gInSc2	rod
Corvus	Corvus	k1gInSc1	Corvus
jsou	být	k5eAaImIp3nP	být
spíš	spíš	k9	spíš
výjimkou	výjimka	k1gFnSc7	výjimka
<g/>
)	)	kIx)	)
a	a	k8xC	a
s	s	k7c7	s
kovovým	kovový	k2eAgInSc7d1	kovový
leskem	lesk	k1gInSc7	lesk
<g/>
,	,	kIx,	,
u	u	k7c2	u
obou	dva	k4xCgNnPc2	dva
pohlaví	pohlaví	k1gNnPc2	pohlaví
bývá	bývat	k5eAaImIp3nS	bývat
stejné	stejný	k2eAgNnSc1d1	stejné
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
druhů	druh	k1gInPc2	druh
je	být	k5eAaImIp3nS	být
společenská	společenský	k2eAgFnSc1d1	společenská
a	a	k8xC	a
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
ve	v	k7c6	v
velkých	velký	k2eAgFnPc6d1	velká
koloniích	kolonie	k1gFnPc6	kolonie
<g/>
,	,	kIx,	,
staví	stavit	k5eAaPmIp3nS	stavit
pevná	pevný	k2eAgNnPc4d1	pevné
hnízda	hnízdo	k1gNnPc4	hnízdo
ve	v	k7c6	v
větvích	větev	k1gFnPc6	větev
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
silný	silný	k2eAgInSc4d1	silný
hlas	hlas	k1gInSc4	hlas
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
dobří	dobrý	k2eAgMnPc1d1	dobrý
imitátoři	imitátor	k1gMnPc1	imitátor
nejrůznějších	různý	k2eAgInPc2d3	nejrůznější
zvuků	zvuk	k1gInPc2	zvuk
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
se	se	k3xPyFc4	se
někteří	některý	k3yIgMnPc1	některý
mohou	moct	k5eAaImIp3nP	moct
naučit	naučit	k5eAaPmF	naučit
i	i	k8xC	i
mluvit	mluvit	k5eAaImF	mluvit
<g/>
.	.	kIx.	.
</s>
<s>
Potravu	potrava	k1gFnSc4	potrava
sbírají	sbírat	k5eAaImIp3nP	sbírat
často	často	k6eAd1	často
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
drobným	drobný	k2eAgInSc7d1	drobný
hmyzem	hmyz	k1gInSc7	hmyz
a	a	k8xC	a
masem	maso	k1gNnSc7	maso
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc1	ten
velmi	velmi	k6eAd1	velmi
dobří	dobřit	k5eAaImIp3nS	dobřit
letci	letec	k1gMnSc3	letec
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
skupiny	skupina	k1gFnSc2	skupina
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejinteligentnější	inteligentní	k2eAgMnPc4d3	nejinteligentnější
obratlovce	obratlovec	k1gMnPc4	obratlovec
vůbec	vůbec	k9	vůbec
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
novokaledonská	novokaledonský	k2eAgFnSc1d1	novokaledonská
vrána	vrána	k1gFnSc1	vrána
(	(	kIx(	(
<g/>
Corvus	Corvus	k1gMnSc1	Corvus
moneduloides	moneduloides	k1gMnSc1	moneduloides
<g/>
)	)	kIx)	)
dokáže	dokázat	k5eAaPmIp3nS	dokázat
vyrábět	vyrábět	k5eAaImF	vyrábět
i	i	k9	i
primitivní	primitivní	k2eAgInPc4d1	primitivní
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
savců	savec	k1gMnPc2	savec
<g/>
,	,	kIx,	,
především	především	k9	především
vyšších	vysoký	k2eAgMnPc2d2	vyšší
primátů	primát	k1gMnPc2	primát
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
u	u	k7c2	u
vyder	vydra	k1gFnPc2	vydra
mořských	mořský	k2eAgFnPc2d1	mořská
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
dokáží	dokázat	k5eAaPmIp3nP	dokázat
své	svůj	k3xOyFgFnPc4	svůj
akce	akce	k1gFnPc4	akce
a	a	k8xC	a
použití	použití	k1gNnSc4	použití
nástrojů	nástroj	k1gInPc2	nástroj
plánovat	plánovat	k5eAaImF	plánovat
na	na	k7c4	na
několik	několik	k4yIc4	několik
kroků	krok	k1gInPc2	krok
dopředu	dopředu	k6eAd1	dopředu
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hned	hned	k6eAd1	hned
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
nutnosti	nutnost	k1gFnSc2	nutnost
použít	použít	k5eAaPmF	použít
metodu	metoda	k1gFnSc4	metoda
pokusu	pokus	k1gInSc2	pokus
a	a	k8xC	a
omylu	omyl	k1gInSc2	omyl
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
díky	díky	k7c3	díky
inteligenci	inteligence	k1gFnSc3	inteligence
jejich	jejich	k3xOp3gFnSc1	jejich
pozornost	pozornost	k1gFnSc1	pozornost
upoutávají	upoutávat	k5eAaImIp3nP	upoutávat
v	v	k7c6	v
přírodě	příroda	k1gFnSc6	příroda
nezvyklé	zvyklý	k2eNgInPc1d1	nezvyklý
předměty	předmět	k1gInPc1	předmět
–	–	k?	–
odnášejí	odnášet	k5eAaImIp3nP	odnášet
si	se	k3xPyFc3	se
různé	různý	k2eAgFnPc1d1	různá
drobnosti	drobnost	k1gFnPc1	drobnost
do	do	k7c2	do
hnízda	hnízdo	k1gNnSc2	hnízdo
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k9	tak
lze	lze	k6eAd1	lze
někdy	někdy	k6eAd1	někdy
nalézt	nalézt	k5eAaBmF	nalézt
i	i	k8xC	i
šperky	šperk	k1gInPc4	šperk
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Známé	známý	k2eAgNnSc1d1	známé
je	být	k5eAaImIp3nS	být
přirovnání	přirovnání	k1gNnSc1	přirovnání
"	"	kIx"	"
<g/>
Krade	krást	k5eAaImIp3nS	krást
jako	jako	k9	jako
straka	straka	k1gFnSc1	straka
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Mohou	moct	k5eAaImIp3nP	moct
však	však	k9	však
nechtě	chtě	k6eNd1	chtě
uškodit	uškodit	k5eAaPmF	uškodit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
si	se	k3xPyFc3	se
do	do	k7c2	do
hnízda	hnízdo	k1gNnSc2	hnízdo
mohou	moct	k5eAaImIp3nP	moct
odnést	odnést	k5eAaPmF	odnést
hořící	hořící	k2eAgInSc4d1	hořící
nedopalek	nedopalek	k1gInSc4	nedopalek
cigarety	cigareta	k1gFnSc2	cigareta
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
způsobit	způsobit	k5eAaPmF	způsobit
"	"	kIx"	"
<g/>
nevysvětlitelný	vysvětlitelný	k2eNgInSc4d1	nevysvětlitelný
<g/>
"	"	kIx"	"
požár	požár	k1gInSc4	požár
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
metod	metoda	k1gFnPc2	metoda
vran	vrána	k1gFnPc2	vrána
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
pečovat	pečovat	k5eAaImF	pečovat
o	o	k7c4	o
své	svůj	k3xOyFgNnSc4	svůj
peří	peří	k1gNnSc4	peří
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
využití	využití	k1gNnSc1	využití
mravenců	mravenec	k1gMnPc2	mravenec
<g/>
:	:	kIx,	:
ty	ty	k3xPp2nSc1	ty
občas	občas	k6eAd1	občas
úmyslně	úmyslně	k6eAd1	úmyslně
vyprovokují	vyprovokovat	k5eAaPmIp3nP	vyprovokovat
rozhrabáním	rozhrabání	k1gNnSc7	rozhrabání
jejich	jejich	k3xOp3gNnSc2	jejich
mraveniště	mraveniště	k1gNnSc2	mraveniště
a	a	k8xC	a
přikryjí	přikrýt	k5eAaPmIp3nP	přikrýt
ho	on	k3xPp3gMnSc4	on
křídly	křídlo	k1gNnPc7	křídlo
<g/>
.	.	kIx.	.
</s>
<s>
Mravenci	mravenec	k1gMnPc1	mravenec
zalezou	zalézt	k5eAaPmIp3nP	zalézt
mezi	mezi	k7c4	mezi
pera	pero	k1gNnPc4	pero
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pak	pak	k6eAd1	pak
útočí	útočit	k5eAaImIp3nS	útočit
na	na	k7c4	na
ptačí	ptačí	k2eAgMnPc4d1	ptačí
parazity	parazit	k1gMnPc4	parazit
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
zvýšení	zvýšení	k1gNnSc4	zvýšení
účinku	účinek	k1gInSc2	účinek
si	se	k3xPyFc3	se
vrána	vrána	k1gFnSc1	vrána
některé	některý	k3yIgFnPc4	některý
ještě	ještě	k9	ještě
rozetře	rozetřít	k5eAaPmIp3nS	rozetřít
po	po	k7c6	po
křídlech	křídlo	k1gNnPc6	křídlo
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
kyselina	kyselina	k1gFnSc1	kyselina
mravenčí	mravenčí	k2eAgFnSc1d1	mravenčí
ještě	ještě	k6eAd1	ještě
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
účinek	účinek	k1gInSc4	účinek
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
.	.	kIx.	.
</s>
