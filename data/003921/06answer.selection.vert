<s>
Vatikán	Vatikán	k1gInSc1	Vatikán
má	mít	k5eAaImIp3nS	mít
přes	přes	k7c4	přes
800	[number]	k4	800
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
většinu	většina	k1gFnSc4	většina
tvoří	tvořit	k5eAaImIp3nP	tvořit
diplomaté	diplomat	k1gMnPc1	diplomat
(	(	kIx(	(
<g/>
nunciové	nuncius	k1gMnPc1	nuncius
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
žijí	žít	k5eAaImIp3nP	žít
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
<g/>
.	.	kIx.	.
</s>
