<s>
Vznik	vznik	k1gInSc1	vznik
tohoto	tento	k3xDgInSc2	tento
stylu	styl	k1gInSc2	styl
sahá	sahat	k5eAaImIp3nS	sahat
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
značně	značně	k6eAd1	značně
rozšířena	rozšířen	k2eAgFnSc1d1	rozšířena
secese	secese	k1gFnSc1	secese
–	–	k?	–
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
jednoduchými	jednoduchý	k2eAgFnPc7d1	jednoduchá
liniemi	linie	k1gFnPc7	linie
a	a	k8xC	a
křiklavou	křiklavý	k2eAgFnSc7d1	křiklavá
barevností	barevnost	k1gFnSc7	barevnost
<g/>
.	.	kIx.	.
</s>
