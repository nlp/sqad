<p>
<s>
Art	Art	k?	Art
deco	deco	k6eAd1	deco
je	být	k5eAaImIp3nS	být
univerzální	univerzální	k2eAgInSc4d1	univerzální
dekorativní	dekorativní	k2eAgInSc4d1	dekorativní
styl	styl	k1gInSc4	styl
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
především	především	k9	především
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
v	v	k7c6	v
USA	USA	kA	USA
ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
a	a	k8xC	a
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
dvacátého	dvacátý	k4xOgNnSc2	dvacátý
století	století	k1gNnSc2	století
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
a	a	k8xC	a
řemesle	řemeslo	k1gNnSc6	řemeslo
<g/>
.	.	kIx.	.
</s>
<s>
Nese	nést	k5eAaImIp3nS	nést
rysy	rys	k1gInPc4	rys
mnoha	mnoho	k4c2	mnoho
směrů	směr	k1gInPc2	směr
<g/>
,	,	kIx,	,
především	především	k9	především
kubismu	kubismus	k1gInSc2	kubismus
<g/>
,	,	kIx,	,
futurismu	futurismus	k1gInSc2	futurismus
a	a	k8xC	a
secese	secese	k1gFnSc2	secese
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vznik	vznik	k1gInSc1	vznik
==	==	k?	==
</s>
</p>
<p>
<s>
Vznik	vznik	k1gInSc1	vznik
tohoto	tento	k3xDgInSc2	tento
stylu	styl	k1gInSc2	styl
sahá	sahat	k5eAaImIp3nS	sahat
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
značně	značně	k6eAd1	značně
rozšířena	rozšířen	k2eAgFnSc1d1	rozšířena
secese	secese	k1gFnSc1	secese
–	–	k?	–
se	s	k7c7	s
svými	svůj	k3xOyFgFnPc7	svůj
jednoduchými	jednoduchý	k2eAgFnPc7d1	jednoduchá
liniemi	linie	k1gFnPc7	linie
a	a	k8xC	a
křiklavou	křiklavý	k2eAgFnSc7d1	křiklavá
barevností	barevnost	k1gFnSc7	barevnost
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
secese	secese	k1gFnSc2	secese
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
v	v	k7c6	v
architektuře	architektura	k1gFnSc6	architektura
prosazovat	prosazovat	k5eAaImF	prosazovat
modernistické	modernistický	k2eAgFnPc4d1	modernistická
tendence	tendence	k1gFnPc4	tendence
a	a	k8xC	a
v	v	k7c6	v
umění	umění	k1gNnSc6	umění
nové	nový	k2eAgInPc4d1	nový
moderní	moderní	k2eAgInPc4d1	moderní
styly	styl	k1gInPc4	styl
–	–	k?	–
především	především	k6eAd1	především
kubismus	kubismus	k1gInSc1	kubismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tyto	tento	k3xDgInPc1	tento
trendy	trend	k1gInPc1	trend
a	a	k8xC	a
jisté	jistý	k2eAgNnSc4d1	jisté
"	"	kIx"	"
<g/>
šílení	šílení	k1gNnSc4	šílení
<g/>
"	"	kIx"	"
společnosti	společnost	k1gFnSc2	společnost
vším	všecek	k3xTgInSc7	všecek
moderním	moderní	k2eAgInSc7d1	moderní
(	(	kIx(	(
<g/>
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
určité	určitý	k2eAgFnSc2d1	určitá
míry	míra	k1gFnSc2	míra
ovlivněno	ovlivnit	k5eAaPmNgNnS	ovlivnit
novými	nový	k2eAgInPc7d1	nový
vynálezy	vynález	k1gInPc7	vynález
a	a	k8xC	a
rozvojem	rozvoj	k1gInSc7	rozvoj
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
skloubily	skloubit	k5eAaPmAgFnP	skloubit
v	v	k7c6	v
novém	nový	k2eAgInSc6d1	nový
stylu	styl	k1gInSc6	styl
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
prezentoval	prezentovat	k5eAaBmAgMnS	prezentovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1925	[number]	k4	1925
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
,	,	kIx,	,
ve	v	k7c6	v
které	který	k3yIgFnSc6	který
se	se	k3xPyFc4	se
konala	konat	k5eAaImAgFnS	konat
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
výstava	výstava	k1gFnSc1	výstava
dekorativního	dekorativní	k2eAgNnSc2d1	dekorativní
umění	umění	k1gNnSc2	umění
(	(	kIx(	(
<g/>
Exposition	Exposition	k1gInSc4	Exposition
Internationale	Internationale	k1gFnSc2	Internationale
des	des	k1gNnSc7	des
Arts	Artsa	k1gFnPc2	Artsa
Dékoratifs	Dékoratifs	k1gInSc4	Dékoratifs
et	et	k?	et
industriels	industriels	k1gInSc1	industriels
Modernes	Modernes	k1gInSc1	Modernes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
výstavě	výstava	k1gFnSc6	výstava
byly	být	k5eAaImAgInP	být
vystavovány	vystavován	k2eAgInPc1d1	vystavován
užité	užitý	k2eAgInPc1d1	užitý
předměty	předmět	k1gInPc1	předmět
ovlivněné	ovlivněný	k2eAgInPc1d1	ovlivněný
modernismem	modernismus	k1gInSc7	modernismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vývoj	vývoj	k1gInSc1	vývoj
a	a	k8xC	a
rozšíření	rozšíření	k1gNnSc1	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Třebaže	třebaže	k8xS	třebaže
byl	být	k5eAaImAgInS	být
nový	nový	k2eAgInSc1d1	nový
styl	styl	k1gInSc1	styl
art	art	k?	art
deco	deco	k6eAd1	deco
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
počátku	počátek	k1gInSc6	počátek
v	v	k7c6	v
jistém	jistý	k2eAgInSc6d1	jistý
smyslu	smysl	k1gInSc6	smysl
styl	styl	k1gInSc4	styl
luxusní	luxusní	k2eAgInSc1d1	luxusní
a	a	k8xC	a
určený	určený	k2eAgInSc1d1	určený
především	především	k6eAd1	především
pro	pro	k7c4	pro
bohaté	bohatý	k2eAgMnPc4d1	bohatý
lidi	člověk	k1gMnPc4	člověk
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
si	se	k3xPyFc3	se
ohromnou	ohromný	k2eAgFnSc4d1	ohromná
oblibu	obliba	k1gFnSc4	obliba
v	v	k7c6	v
široké	široký	k2eAgFnSc6d1	široká
veřejnosti	veřejnost	k1gFnSc6	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
typické	typický	k2eAgInPc1d1	typický
rysy	rys	k1gInPc1	rys
tak	tak	k6eAd1	tak
byly	být	k5eAaImAgInP	být
reprodukovány	reprodukován	k2eAgInPc1d1	reprodukován
v	v	k7c6	v
masovém	masový	k2eAgNnSc6d1	masové
měřítku	měřítko	k1gNnSc6	měřítko
a	a	k8xC	a
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
oborech	obor	k1gInPc6	obor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Oblasti	oblast	k1gFnSc6	oblast
rozšíření	rozšíření	k1gNnSc4	rozšíření
==	==	k?	==
</s>
</p>
<p>
<s>
Hlavní	hlavní	k2eAgFnPc1d1	hlavní
oblasti	oblast	k1gFnPc1	oblast
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yIgNnPc6	který
se	se	k3xPyFc4	se
styl	styl	k1gInSc1	styl
art	art	k?	art
deco	deco	k6eAd1	deco
prosadil	prosadit	k5eAaPmAgMnS	prosadit
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
===	===	k?	===
Předměty	předmět	k1gInPc7	předmět
denní	denní	k2eAgFnSc2d1	denní
potřeby	potřeba	k1gFnSc2	potřeba
===	===	k?	===
</s>
</p>
<p>
<s>
Předměty	předmět	k1gInPc1	předmět
určené	určený	k2eAgInPc1d1	určený
ke	k	k7c3	k
každodenní	každodenní	k2eAgFnSc3d1	každodenní
činnosti	činnost	k1gFnSc3	činnost
se	se	k3xPyFc4	se
vyznačovaly	vyznačovat	k5eAaImAgInP	vyznačovat
vysokým	vysoký	k2eAgInSc7d1	vysoký
leskem	lesk	k1gInSc7	lesk
<g/>
,	,	kIx,	,
zjednodušenými	zjednodušený	k2eAgFnPc7d1	zjednodušená
liniemi	linie	k1gFnPc7	linie
a	a	k8xC	a
čistými	čistý	k2eAgFnPc7d1	čistá
barvami	barva	k1gFnPc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
takových	takový	k3xDgInPc2	takový
předmětů	předmět	k1gInPc2	předmět
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
pudřenky	pudřenka	k1gFnPc1	pudřenka
<g/>
,	,	kIx,	,
zrcátka	zrcátko	k1gNnPc1	zrcátko
<g/>
,	,	kIx,	,
náramkové	náramkový	k2eAgFnPc1d1	náramková
hodinky	hodinka	k1gFnPc1	hodinka
či	či	k8xC	či
osvětlovací	osvětlovací	k2eAgNnPc1d1	osvětlovací
tělesa	těleso	k1gNnPc1	těleso
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
hojně	hojně	k6eAd1	hojně
používat	používat	k5eAaImF	používat
materiály	materiál	k1gInPc4	materiál
jako	jako	k8xC	jako
<g/>
:	:	kIx,	:
sklo	sklo	k1gNnSc1	sklo
<g/>
,	,	kIx,	,
keramika	keramika	k1gFnSc1	keramika
<g/>
,	,	kIx,	,
moderní	moderní	k2eAgInSc1d1	moderní
bakelit	bakelit	k1gInSc1	bakelit
nebo	nebo	k8xC	nebo
leštěný	leštěný	k2eAgInSc1d1	leštěný
chrom	chrom	k1gInSc1	chrom
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Grafika	grafika	k1gFnSc1	grafika
===	===	k?	===
</s>
</p>
<p>
<s>
Plakáty	plakát	k1gInPc1	plakát
a	a	k8xC	a
obaly	obal	k1gInPc1	obal
knih	kniha	k1gFnPc2	kniha
se	se	k3xPyFc4	se
vyznačovaly	vyznačovat	k5eAaImAgFnP	vyznačovat
silnými	silný	k2eAgFnPc7d1	silná
konturami	kontura	k1gFnPc7	kontura
<g/>
,	,	kIx,	,
jasnými	jasný	k2eAgFnPc7d1	jasná
barvami	barva	k1gFnPc7	barva
a	a	k8xC	a
výraznými	výrazný	k2eAgNnPc7d1	výrazné
písmeny	písmeno	k1gNnPc7	písmeno
<g/>
.	.	kIx.	.
</s>
<s>
Dobrým	dobrý	k2eAgInSc7d1	dobrý
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
úvodní	úvodní	k2eAgFnSc1d1	úvodní
sekvence	sekvence	k1gFnSc1	sekvence
ve	v	k7c6	v
filmech	film	k1gInPc6	film
studia	studio	k1gNnSc2	studio
Universal	Universal	k1gMnSc1	Universal
Pictures	Pictures	k1gMnSc1	Pictures
–	–	k?	–
otáčející	otáčející	k2eAgFnSc1d1	otáčející
se	se	k3xPyFc4	se
planeta	planeta	k1gFnSc1	planeta
a	a	k8xC	a
zářící	zářící	k2eAgFnSc1d1	zářící
sluneční	sluneční	k2eAgFnSc1d1	sluneční
soustava	soustava	k1gFnSc1	soustava
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
universal	universat	k5eAaImAgMnS	universat
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Automobilismus	automobilismus	k1gInSc4	automobilismus
===	===	k?	===
</s>
</p>
<p>
<s>
Art	Art	k?	Art
deco	deco	k6eAd1	deco
výrazně	výrazně	k6eAd1	výrazně
ovlivnil	ovlivnit	k5eAaPmAgInS	ovlivnit
design	design	k1gInSc1	design
rodícího	rodící	k2eAgMnSc2d1	rodící
se	se	k3xPyFc4	se
automobilismu	automobilismus	k1gInSc2	automobilismus
<g/>
.	.	kIx.	.
</s>
<s>
Typické	typický	k2eAgInPc1d1	typický
jsou	být	k5eAaImIp3nP	být
tvarově	tvarově	k6eAd1	tvarově
jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
automobily	automobil	k1gInPc1	automobil
proudnicového	proudnicový	k2eAgInSc2d1	proudnicový
tvaru	tvar	k1gInSc2	tvar
s	s	k7c7	s
luxusním	luxusní	k2eAgInSc7d1	luxusní
interiérem	interiér	k1gInSc7	interiér
(	(	kIx(	(
<g/>
pohodlná	pohodlný	k2eAgFnSc1d1	pohodlná
sedačka	sedačka	k1gFnSc1	sedačka
nebo	nebo	k8xC	nebo
dřevěné	dřevěný	k2eAgInPc1d1	dřevěný
obklady	obklad	k1gInPc1	obklad
interiéru	interiér	k1gInSc2	interiér
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Architektura	architektura	k1gFnSc1	architektura
===	===	k?	===
</s>
</p>
<p>
<s>
Nejlepším	dobrý	k2eAgMnSc7d3	nejlepší
reprezentantem	reprezentant	k1gMnSc7	reprezentant
tohoto	tento	k3xDgInSc2	tento
stylu	styl	k1gInSc2	styl
v	v	k7c6	v
architektuře	architektura	k1gFnSc6	architektura
je	být	k5eAaImIp3nS	být
slavná	slavný	k2eAgFnSc1d1	slavná
budova	budova	k1gFnSc1	budova
Chrysler	Chrysler	k1gMnSc1	Chrysler
Building	Building	k1gInSc1	Building
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
;	;	kIx,	;
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
stavbě	stavba	k1gFnSc6	stavba
jsou	být	k5eAaImIp3nP	být
patrné	patrný	k2eAgInPc1d1	patrný
hlavní	hlavní	k2eAgInPc1d1	hlavní
rysy	rys	k1gInPc1	rys
<g/>
:	:	kIx,	:
pevně	pevně	k6eAd1	pevně
daná	daný	k2eAgFnSc1d1	daná
linie	linie	k1gFnSc1	linie
(	(	kIx(	(
<g/>
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
"	"	kIx"	"
<g/>
křivolaké	křivolaký	k2eAgFnSc2d1	křivolaká
<g/>
"	"	kIx"	"
secese	secese	k1gFnSc2	secese
<g/>
)	)	kIx)	)
a	a	k8xC	a
dekorativní	dekorativní	k2eAgInSc4d1	dekorativní
interiér	interiér	k1gInSc4	interiér
<g/>
.	.	kIx.	.
</s>
<s>
Českým	český	k2eAgMnSc7d1	český
představitelem	představitel	k1gMnSc7	představitel
stylu	styl	k1gInSc2	styl
art	art	k?	art
deco	deco	k6eAd1	deco
je	být	k5eAaImIp3nS	být
hotel	hotel	k1gInSc1	hotel
Alcron	Alcron	k1gInSc1	Alcron
postavený	postavený	k2eAgInSc1d1	postavený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1932	[number]	k4	1932
Aloisem	Alois	k1gMnSc7	Alois
Kroftou	Krofta	k1gMnSc7	Krofta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Galerie	galerie	k1gFnSc1	galerie
==	==	k?	==
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Secese	secese	k1gFnSc1	secese
</s>
</p>
<p>
<s>
Kubismus	kubismus	k1gInSc1	kubismus
</s>
</p>
<p>
<s>
Futurismus	futurismus	k1gInSc1	futurismus
</s>
</p>
<p>
<s>
České	český	k2eAgFnPc1d1	Česká
art	art	k?	art
deco	deco	k6eAd1	deco
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
Art	Art	k1gFnSc2	Art
deco	deco	k6eAd1	deco
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Art	Art	k1gFnPc2	Art
deco	deco	k6eAd1	deco
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
