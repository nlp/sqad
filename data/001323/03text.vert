<s>
George	George	k1gInSc1	George
Washington	Washington	k1gInSc1	Washington
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1732	[number]	k4	1732
<g/>
,	,	kIx,	,
Bridge	Bridge	k1gFnSc1	Bridge
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Creek	Creek	k1gInSc1	Creek
<g/>
,	,	kIx,	,
Virginie	Virginie	k1gFnSc1	Virginie
-	-	kIx~	-
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1799	[number]	k4	1799
<g/>
,	,	kIx,	,
Mount	Mount	k1gMnSc1	Mount
Vernon	Vernon	k1gMnSc1	Vernon
<g/>
,	,	kIx,	,
Virginie	Virginie	k1gFnSc1	Virginie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
voják	voják	k1gMnSc1	voják
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jediným	jediný	k2eAgMnSc7d1	jediný
americkým	americký	k2eAgMnSc7d1	americký
vojákem	voják	k1gMnSc7	voják
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
v	v	k7c6	v
hodnosti	hodnost	k1gFnSc6	hodnost
maršála	maršál	k1gMnSc2	maršál
<g/>
,	,	kIx,	,
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
hodnosti	hodnost	k1gFnSc2	hodnost
nebyl	být	k5eNaImAgInS	být
kromě	kromě	k7c2	kromě
něho	on	k3xPp3gMnSc2	on
nikdo	nikdo	k3yNnSc1	nikdo
jmenován	jmenován	k2eAgMnSc1d1	jmenován
a	a	k8xC	a
armáda	armáda	k1gFnSc1	armáda
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
tuto	tento	k3xDgFnSc4	tento
hodnost	hodnost	k1gFnSc4	hodnost
nikdy	nikdy	k6eAd1	nikdy
nezavedla	zavést	k5eNaPmAgFnS	zavést
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
žádný	žádný	k3yNgInSc4	žádný
americký	americký	k2eAgMnSc1d1	americký
vojenský	vojenský	k2eAgMnSc1d1	vojenský
velitel	velitel	k1gMnSc1	velitel
nemohl	moct	k5eNaImAgMnS	moct
získat	získat	k5eAaPmF	získat
vyšší	vysoký	k2eAgFnSc4d2	vyšší
hodnost	hodnost	k1gFnSc4	hodnost
než	než	k8xS	než
George	George	k1gInSc4	George
Washington	Washington	k1gInSc1	Washington
(	(	kIx(	(
<g/>
výjimkou	výjimka	k1gFnSc7	výjimka
byl	být	k5eAaImAgMnS	být
gen.	gen.	kA	gen.
Douglas	Douglas	k1gMnSc1	Douglas
MacArthur	MacArthur	k1gMnSc1	MacArthur
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
získal	získat	k5eAaPmAgInS	získat
hodnost	hodnost	k1gFnSc4	hodnost
polního	polní	k2eAgMnSc2d1	polní
maršála	maršál	k1gMnSc2	maršál
ve	v	k7c6	v
filipínské	filipínský	k2eAgFnSc6d1	filipínská
armádě	armáda	k1gFnSc6	armáda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
nejstarším	starý	k2eAgMnSc7d3	nejstarší
synem	syn	k1gMnSc7	syn
poměrně	poměrně	k6eAd1	poměrně
zámožného	zámožný	k2eAgMnSc2d1	zámožný
majitele	majitel	k1gMnSc2	majitel
plantáží	plantáž	k1gFnPc2	plantáž
ve	v	k7c6	v
Virginii	Virginie	k1gFnSc6	Virginie
a	a	k8xC	a
soudce	soudce	k1gMnSc2	soudce
Augustina	Augustin	k1gMnSc2	Augustin
Washingtona	Washington	k1gMnSc2	Washington
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
ženy	žena	k1gFnSc2	žena
Mary	Mary	k1gFnSc2	Mary
<g/>
,	,	kIx,	,
rozené	rozený	k2eAgFnSc2d1	rozená
Ballové	Ballová	k1gFnSc2	Ballová
<g/>
.	.	kIx.	.
</s>
<s>
Rodiny	rodina	k1gFnPc4	rodina
obou	dva	k4xCgMnPc2	dva
rodičů	rodič	k1gMnPc2	rodič
patřily	patřit	k5eAaImAgFnP	patřit
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
ke	k	k7c3	k
starousedlíkům	starousedlík	k1gMnPc3	starousedlík
-	-	kIx~	-
jejich	jejich	k3xOp3gMnPc1	jejich
předkové	předek	k1gMnPc1	předek
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
přistěhovali	přistěhovat	k5eAaPmAgMnP	přistěhovat
z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
již	již	k6eAd1	již
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
George	George	k6eAd1	George
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
nenavštěvoval	navštěvovat	k5eNaImAgInS	navštěvovat
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
měl	mít	k5eAaImAgMnS	mít
soukromé	soukromý	k2eAgMnPc4d1	soukromý
učitele	učitel	k1gMnPc4	učitel
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
sedmnácti	sedmnáct	k4xCc6	sedmnáct
letech	léto	k1gNnPc6	léto
odešel	odejít	k5eAaPmAgMnS	odejít
z	z	k7c2	z
rodinné	rodinný	k2eAgFnSc2d1	rodinná
usedlosti	usedlost	k1gFnSc2	usedlost
a	a	k8xC	a
začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
živit	živit	k5eAaImF	živit
jako	jako	k9	jako
zeměměřič	zeměměřič	k1gMnSc1	zeměměřič
<g/>
,	,	kIx,	,
průvodce	průvodce	k1gMnSc1	průvodce
na	na	k7c6	na
stezkách	stezka	k1gFnPc6	stezka
v	v	k7c6	v
méně	málo	k6eAd2	málo
prozkoumaných	prozkoumaný	k2eAgFnPc6d1	prozkoumaná
oblastech	oblast	k1gFnPc6	oblast
a	a	k8xC	a
osidlovací	osidlovací	k2eAgMnSc1d1	osidlovací
agent	agent	k1gMnSc1	agent
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
otce	otec	k1gMnSc2	otec
připadla	připadnout	k5eAaPmAgFnS	připadnout
většina	většina	k1gFnSc1	většina
majetku	majetek	k1gInSc2	majetek
synům	syn	k1gMnPc3	syn
z	z	k7c2	z
prvního	první	k4xOgNnSc2	první
manželství	manželství	k1gNnSc2	manželství
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
zdědil	zdědit	k5eAaPmAgInS	zdědit
pouze	pouze	k6eAd1	pouze
malou	malý	k2eAgFnSc4d1	malá
farmu	farma	k1gFnSc4	farma
u	u	k7c2	u
Fredericksburgu	Fredericksburg	k1gInSc2	Fredericksburg
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
práce	práce	k1gFnSc1	práce
zeměměřiče	zeměměřič	k1gMnSc2	zeměměřič
jej	on	k3xPp3gMnSc4	on
ale	ale	k8xC	ale
slušně	slušně	k6eAd1	slušně
živila	živit	k5eAaImAgFnS	živit
<g/>
,	,	kIx,	,
časem	časem	k6eAd1	časem
přikoupil	přikoupit	k5eAaPmAgMnS	přikoupit
další	další	k2eAgInPc4d1	další
pozemky	pozemek	k1gInPc4	pozemek
a	a	k8xC	a
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
svého	svůj	k3xOyFgMnSc2	svůj
nevlastního	vlastní	k2eNgMnSc2d1	nevlastní
bratra	bratr	k1gMnSc2	bratr
Lawrence	Lawrenec	k1gMnSc2	Lawrenec
si	se	k3xPyFc3	se
od	od	k7c2	od
jeho	jeho	k3xOp3gFnSc2	jeho
ženy	žena	k1gFnSc2	žena
pronajal	pronajmout	k5eAaPmAgMnS	pronajmout
usedlost	usedlost	k1gFnSc4	usedlost
Mount	Mount	k1gMnSc1	Mount
Vernon	Vernon	k1gMnSc1	Vernon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
trávil	trávit	k5eAaImAgMnS	trávit
mnoho	mnoho	k4c4	mnoho
času	čas	k1gInSc2	čas
už	už	k9	už
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
tomto	tento	k3xDgNnSc6	tento
oblíbeném	oblíbený	k2eAgNnSc6d1	oblíbené
místě	místo	k1gNnSc6	místo
pak	pak	k6eAd1	pak
prožil	prožít	k5eAaPmAgInS	prožít
většinu	většina	k1gFnSc4	většina
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgMnSc6	svůj
bratrovi	bratr	k1gMnSc6	bratr
získal	získat	k5eAaPmAgInS	získat
i	i	k9	i
místo	místo	k1gNnSc4	místo
u	u	k7c2	u
virginské	virginský	k2eAgFnSc2d1	virginská
milice	milice	k1gFnSc2	milice
<g/>
,	,	kIx,	,
hodnost	hodnost	k1gFnSc4	hodnost
majora	major	k1gMnSc2	major
a	a	k8xC	a
pevný	pevný	k2eAgInSc4d1	pevný
plat	plat	k1gInSc4	plat
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
i	i	k9	i
prvních	první	k4xOgFnPc2	první
válečných	válečný	k2eAgFnPc2d1	válečná
zkušeností	zkušenost	k1gFnPc2	zkušenost
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
tyto	tento	k3xDgFnPc1	tento
polovojenské	polovojenský	k2eAgFnPc1d1	polovojenská
<g/>
,	,	kIx,	,
dobrovolnické	dobrovolnický	k2eAgFnPc1d1	dobrovolnická
milice	milice	k1gFnPc1	milice
pomáhaly	pomáhat	k5eAaImAgFnP	pomáhat
v	v	k7c6	v
koloniích	kolonie	k1gFnPc6	kolonie
anglické	anglický	k2eAgFnSc3d1	anglická
armádě	armáda	k1gFnSc3	armáda
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
Indiánům	Indián	k1gMnPc3	Indián
<g/>
,	,	kIx,	,
Španělům	Španěl	k1gMnPc3	Španěl
a	a	k8xC	a
zejména	zejména	k9	zejména
severním	severní	k2eAgMnPc3d1	severní
sousedům	soused	k1gMnPc3	soused
-	-	kIx~	-
Francouzům	Francouz	k1gMnPc3	Francouz
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
kariéra	kariéra	k1gFnSc1	kariéra
v	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
sborech	sbor	k1gInPc6	sbor
strmě	strmě	k6eAd1	strmě
stoupala	stoupat	k5eAaImAgFnS	stoupat
a	a	k8xC	a
ve	v	k7c6	v
svých	svůj	k3xOyFgNnPc6	svůj
dvaadvaceti	dvaadvacet	k4xCc6	dvaadvacet
byl	být	k5eAaImAgInS	být
povýšen	povýšit	k5eAaPmNgMnS	povýšit
na	na	k7c4	na
plukovníka	plukovník	k1gMnSc4	plukovník
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
touží	toužit	k5eAaImIp3nS	toužit
být	být	k5eAaImF	být
vojákem	voják	k1gMnSc7	voják
z	z	k7c2	z
povolání	povolání	k1gNnSc2	povolání
v	v	k7c6	v
britské	britský	k2eAgFnSc6d1	britská
armádě	armáda	k1gFnSc6	armáda
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
ale	ale	k9	ale
nepodařilo	podařit	k5eNaPmAgNnS	podařit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jedné	jeden	k4xCgFnSc6	jeden
nezdařené	zdařený	k2eNgFnSc6d1	nezdařená
akci	akce	k1gFnSc6	akce
proti	proti	k7c3	proti
francouzským	francouzský	k2eAgNnPc3d1	francouzské
vojskům	vojsko	k1gNnPc3	vojsko
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
milic	milice	k1gFnPc2	milice
velel	velet	k5eAaImAgInS	velet
<g/>
,	,	kIx,	,
odešel	odejít	k5eAaPmAgMnS	odejít
dobrovolně	dobrovolně	k6eAd1	dobrovolně
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
Mount	Mount	k1gInSc4	Mount
Vernon	Vernona	k1gFnPc2	Vernona
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
ujal	ujmout	k5eAaPmAgInS	ujmout
role	role	k1gFnSc2	role
hospodáře	hospodář	k1gMnSc2	hospodář
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pronajaté	pronajatý	k2eAgFnSc6d1	pronajatá
farmě	farma	k1gFnSc6	farma
Mount	Mounta	k1gFnPc2	Mounta
Vernon	Vernona	k1gFnPc2	Vernona
a	a	k8xC	a
přilehlých	přilehlý	k2eAgInPc6d1	přilehlý
pozemcích	pozemek	k1gInPc6	pozemek
se	se	k3xPyFc4	se
věnoval	věnovat	k5eAaPmAgMnS	věnovat
pěstování	pěstování	k1gNnSc2	pěstování
tabáku	tabák	k1gInSc2	tabák
<g/>
.	.	kIx.	.
</s>
<s>
Měl	mít	k5eAaImAgMnS	mít
316	[number]	k4	316
(	(	kIx(	(
<g/>
421	[number]	k4	421
<g/>
)	)	kIx)	)
černošských	černošský	k2eAgMnPc2d1	černošský
otroků	otrok	k1gMnPc2	otrok
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgFnPc3	jenž
se	se	k3xPyFc4	se
na	na	k7c4	na
tehdejší	tehdejší	k2eAgInPc4d1	tehdejší
poměry	poměr	k1gInPc4	poměr
choval	chovat	k5eAaImAgMnS	chovat
údajně	údajně	k6eAd1	údajně
velmi	velmi	k6eAd1	velmi
slušně	slušně	k6eAd1	slušně
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1755	[number]	k4	1755
byl	být	k5eAaImAgMnS	být
opět	opět	k6eAd1	opět
požádán	požádat	k5eAaPmNgMnS	požádat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
britské	britský	k2eAgFnSc2d1	britská
vojenské	vojenský	k2eAgFnSc2d1	vojenská
výpravy	výprava	k1gFnSc2	výprava
proti	proti	k7c3	proti
Francouzům	Francouz	k1gMnPc3	Francouz
do	do	k7c2	do
údolí	údolí	k1gNnSc2	údolí
Ohia	Ohio	k1gNnSc2	Ohio
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
žádal	žádat	k5eAaImAgMnS	žádat
o	o	k7c4	o
zařazení	zařazení	k1gNnSc4	zařazení
do	do	k7c2	do
britské	britský	k2eAgFnSc2d1	britská
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
opět	opět	k6eAd1	opět
byl	být	k5eAaImAgInS	být
odmítnut	odmítnout	k5eAaPmNgInS	odmítnout
a	a	k8xC	a
stal	stát	k5eAaPmAgInS	stát
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
poradcem	poradce	k1gMnSc7	poradce
výpravy	výprava	k1gFnSc2	výprava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
nakonec	nakonec	k6eAd1	nakonec
skončila	skončit	k5eAaPmAgFnS	skončit
naprostou	naprostý	k2eAgFnSc7d1	naprostá
katastrofou	katastrofa	k1gFnSc7	katastrofa
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
neúspěch	neúspěch	k1gInSc4	neúspěch
akce	akce	k1gFnSc1	akce
byl	být	k5eAaImAgInS	být
pochválen	pochválen	k2eAgInSc1d1	pochválen
za	za	k7c4	za
svoji	svůj	k3xOyFgFnSc4	svůj
odvahu	odvaha	k1gFnSc4	odvaha
v	v	k7c6	v
bojích	boj	k1gInPc6	boj
a	a	k8xC	a
guvernérem	guvernér	k1gMnSc7	guvernér
Severní	severní	k2eAgFnSc2d1	severní
Karolíny	Karolína	k1gFnSc2	Karolína
dokonce	dokonce	k9	dokonce
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
Virginie	Virginie	k1gFnSc1	Virginie
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
mu	on	k3xPp3gNnSc3	on
znovu	znovu	k6eAd1	znovu
přiznána	přiznán	k2eAgFnSc1d1	přiznána
vojenská	vojenský	k2eAgFnSc1d1	vojenská
hodnost	hodnost	k1gFnSc1	hodnost
plukovníka	plukovník	k1gMnSc2	plukovník
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
ale	ale	k9	ale
platila	platit	k5eAaImAgFnS	platit
opět	opět	k6eAd1	opět
jen	jen	k9	jen
pro	pro	k7c4	pro
koloniální	koloniální	k2eAgFnSc4d1	koloniální
a	a	k8xC	a
nikoliv	nikoliv	k9	nikoliv
britskou	britský	k2eAgFnSc4d1	britská
armádu	armáda	k1gFnSc4	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Zúčastní	zúčastnit	k5eAaPmIp3nS	zúčastnit
se	se	k3xPyFc4	se
několika	několik	k4yIc2	několik
dalších	další	k2eAgMnPc2d1	další
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
již	již	k6eAd1	již
úspěšných	úspěšný	k2eAgNnPc2d1	úspěšné
tažení	tažení	k1gNnPc2	tažení
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
mu	on	k3xPp3gMnSc3	on
dokonce	dokonce	k9	dokonce
dočasně	dočasně	k6eAd1	dočasně
propůjčena	propůjčen	k2eAgFnSc1d1	propůjčena
hodnost	hodnost	k1gFnSc1	hodnost
brigádního	brigádní	k2eAgMnSc2d1	brigádní
generála	generál	k1gMnSc2	generál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1758	[number]	k4	1758
podruhé	podruhé	k6eAd1	podruhé
opustil	opustit	k5eAaPmAgMnS	opustit
vojsko	vojsko	k1gNnSc4	vojsko
<g/>
,	,	kIx,	,
vrátil	vrátit	k5eAaPmAgMnS	vrátit
se	se	k3xPyFc4	se
na	na	k7c4	na
své	svůj	k3xOyFgFnPc4	svůj
plantáže	plantáž	k1gFnPc4	plantáž
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
poslancem	poslanec	k1gMnSc7	poslanec
virginijského	virginijský	k2eAgNnSc2d1	virginijský
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
amerických	americký	k2eAgFnPc6d1	americká
koloniích	kolonie	k1gFnPc6	kolonie
zatím	zatím	k6eAd1	zatím
postupně	postupně	k6eAd1	postupně
sílilo	sílit	k5eAaImAgNnS	sílit
hospodářství	hospodářství	k1gNnSc1	hospodářství
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
závislost	závislost	k1gFnSc4	závislost
na	na	k7c6	na
vzdálené	vzdálený	k2eAgFnSc6d1	vzdálená
Anglii	Anglie	k1gFnSc6	Anglie
začala	začít	k5eAaPmAgFnS	začít
být	být	k5eAaImF	být
pociťována	pociťován	k2eAgFnSc1d1	pociťována
jako	jako	k8xS	jako
brzda	brzda	k1gFnSc1	brzda
rozvoje	rozvoj	k1gInSc2	rozvoj
<g/>
.	.	kIx.	.
</s>
<s>
Osadníci	osadník	k1gMnPc1	osadník
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
zbytečné	zbytečný	k2eAgNnSc4d1	zbytečné
odvádět	odvádět	k5eAaImF	odvádět
daně	daň	k1gFnSc2	daň
králi	král	k1gMnPc7	král
a	a	k8xC	a
vydržovat	vydržovat	k5eAaImF	vydržovat
britské	britský	k2eAgMnPc4d1	britský
úředníky	úředník	k1gMnPc4	úředník
<g/>
,	,	kIx,	,
guvernéry	guvernér	k1gMnPc4	guvernér
a	a	k8xC	a
armádu	armáda	k1gFnSc4	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Začala	začít	k5eAaPmAgFnS	začít
narůstat	narůstat	k5eAaImF	narůstat
revoluční	revoluční	k2eAgFnSc1d1	revoluční
nálada	nálada	k1gFnSc1	nálada
a	a	k8xC	a
nespokojenost	nespokojenost	k1gFnSc1	nespokojenost
některých	některý	k3yIgFnPc2	některý
vrstev	vrstva	k1gFnPc2	vrstva
americké	americký	k2eAgFnSc2d1	americká
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
zastával	zastávat	k5eAaImAgMnS	zastávat
zpočátku	zpočátku	k6eAd1	zpočátku
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
otázkách	otázka	k1gFnPc6	otázka
zdrženlivý	zdrženlivý	k2eAgInSc1d1	zdrženlivý
postoj	postoj	k1gInSc1	postoj
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1759	[number]	k4	1759
se	se	k3xPyFc4	se
oženil	oženit	k5eAaPmAgMnS	oženit
s	s	k7c7	s
Marthou	Martha	k1gFnSc7	Martha
Dandridge	Dandridg	k1gInSc2	Dandridg
Custisovou	Custisový	k2eAgFnSc7d1	Custisový
<g/>
,	,	kIx,	,
vdovou	vdova	k1gFnSc7	vdova
se	s	k7c7	s
dvěma	dva	k4xCgFnPc7	dva
malými	malý	k2eAgFnPc7d1	malá
dětmi	dítě	k1gFnPc7	dítě
<g/>
,	,	kIx,	,
které	který	k3yQgFnSc3	který
adoptoval	adoptovat	k5eAaPmAgMnS	adoptovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
sňatku	sňatek	k1gInSc6	sňatek
již	již	k6eAd1	již
vlastnil	vlastnit	k5eAaImAgInS	vlastnit
několik	několik	k4yIc4	několik
tisíc	tisíc	k4xCgInPc2	tisíc
hektarů	hektar	k1gInPc2	hektar
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Plantáže	plantáž	k1gFnPc1	plantáž
však	však	k9	však
byly	být	k5eAaImAgFnP	být
po	po	k7c6	po
mnoha	mnoho	k4c6	mnoho
letech	léto	k1gNnPc6	léto
zdecimovány	zdecimován	k2eAgInPc4d1	zdecimován
<g/>
,	,	kIx,	,
úrodnost	úrodnost	k1gFnSc1	úrodnost
klesala	klesat	k5eAaImAgFnS	klesat
a	a	k8xC	a
ceny	cena	k1gFnSc2	cena
tabáku	tabák	k1gInSc2	tabák
také	také	k9	také
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
odvádět	odvádět	k5eAaImF	odvádět
stále	stále	k6eAd1	stále
další	další	k2eAgFnPc1d1	další
a	a	k8xC	a
nové	nový	k2eAgFnPc1d1	nová
daně	daň	k1gFnPc1	daň
Anglii	Anglie	k1gFnSc4	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Farma	farma	k1gFnSc1	farma
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
zadlužovala	zadlužovat	k5eAaImAgFnS	zadlužovat
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
ani	ani	k8xC	ani
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
období	období	k1gNnSc6	období
<g/>
,	,	kIx,	,
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1770	[number]	k4	1770
<g/>
,	,	kIx,	,
nebyl	být	k5eNaImAgMnS	být
ještě	ještě	k6eAd1	ještě
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
se	s	k7c7	s
spory	spor	k1gInPc7	spor
mezi	mezi	k7c7	mezi
kolonisty	kolonista	k1gMnPc7	kolonista
a	a	k8xC	a
Anglií	Anglie	k1gFnSc7	Anglie
měly	mít	k5eAaImAgInP	mít
řešit	řešit	k5eAaImF	řešit
násilnou	násilný	k2eAgFnSc7d1	násilná
cestou	cesta	k1gFnSc7	cesta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1775	[number]	k4	1775
se	se	k3xPyFc4	se
však	však	k9	však
neshody	neshoda	k1gFnPc4	neshoda
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
zeměmi	zem	k1gFnPc7	zem
vyostřily	vyostřit	k5eAaPmAgInP	vyostřit
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
prvním	první	k4xOgInPc3	první
ozbrojeným	ozbrojený	k2eAgInPc3d1	ozbrojený
střetům	střet	k1gInPc3	střet
<g/>
.	.	kIx.	.
10	[number]	k4	10
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1775	[number]	k4	1775
se	se	k3xPyFc4	se
ve	v	k7c6	v
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
sešel	sejít	k5eAaPmAgInS	sejít
Druhý	druhý	k4xOgInSc1	druhý
kontinentální	kontinentální	k2eAgInSc1d1	kontinentální
kongres	kongres	k1gInSc1	kongres
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnPc1	jehož
účastníci	účastník	k1gMnPc1	účastník
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
něj	on	k3xPp3gNnSc2	on
<g/>
,	,	kIx,	,
zaslali	zaslat	k5eAaPmAgMnP	zaslat
anglickému	anglický	k2eAgMnSc3d1	anglický
králi	král	k1gMnSc3	král
Jiřímu	Jiří	k1gMnSc3	Jiří
III	III	kA	III
<g/>
.	.	kIx.	.
petici	petice	k1gFnSc4	petice
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
nabízeli	nabízet	k5eAaImAgMnP	nabízet
několik	několik	k4yIc4	několik
kompromisních	kompromisní	k2eAgNnPc2d1	kompromisní
řešení	řešení	k1gNnPc2	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
v	v	k7c6	v
očekávání	očekávání	k1gNnSc6	očekávání
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
bude	být	k5eAaImBp3nS	být
následovat	následovat	k5eAaImF	následovat
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
15	[number]	k4	15
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1775	[number]	k4	1775
jmenován	jmenovat	k5eAaImNgMnS	jmenovat
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
kontinentálních	kontinentální	k2eAgFnPc2d1	kontinentální
milic	milice	k1gFnPc2	milice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1775	[number]	k4	1775
britský	britský	k2eAgMnSc1d1	britský
král	král	k1gMnSc1	král
Jiří	Jiří	k1gMnSc1	Jiří
III	III	kA	III
<g/>
.	.	kIx.	.
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
petici	petice	k1gFnSc4	petice
Američanů	Američan	k1gMnPc2	Američan
a	a	k8xC	a
označil	označit	k5eAaPmAgMnS	označit
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
povstalce	povstalec	k1gMnPc4	povstalec
<g/>
,	,	kIx,	,
s	s	k7c7	s
nimiž	jenž	k3xRgMnPc7	jenž
zakázal	zakázat	k5eAaPmAgMnS	zakázat
jakékoli	jakýkoli	k3yIgNnSc4	jakýkoli
vyjednávání	vyjednávání	k1gNnSc4	vyjednávání
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
okamžikem	okamžik	k1gInSc7	okamžik
začala	začít	k5eAaPmAgFnS	začít
americká	americký	k2eAgFnSc1d1	americká
válka	válka	k1gFnSc1	válka
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
téměř	téměř	k6eAd1	téměř
dvaceti	dvacet	k4xCc6	dvacet
letech	léto	k1gNnPc6	léto
poklidného	poklidný	k2eAgInSc2d1	poklidný
života	život	k1gInSc2	život
na	na	k7c6	na
svých	svůj	k3xOyFgFnPc6	svůj
plantážích	plantáž	k1gFnPc6	plantáž
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
znovu	znovu	k6eAd1	znovu
vrátil	vrátit	k5eAaPmAgMnS	vrátit
k	k	k7c3	k
dobrovolnému	dobrovolný	k2eAgNnSc3d1	dobrovolné
koloniálnímu	koloniální	k2eAgNnSc3d1	koloniální
vojsku	vojsko	k1gNnSc3	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zděšen	zděsit	k5eAaPmNgMnS	zděsit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
v	v	k7c6	v
jakém	jaký	k3yIgInSc6	jaký
stavu	stav	k1gInSc6	stav
se	se	k3xPyFc4	se
milice	milice	k1gFnPc1	milice
nacházely	nacházet	k5eAaImAgFnP	nacházet
-	-	kIx~	-
stále	stále	k6eAd1	stále
neexistovala	existovat	k5eNaImAgFnS	existovat
jednotná	jednotný	k2eAgFnSc1d1	jednotná
uniforma	uniforma	k1gFnSc1	uniforma
<g/>
,	,	kIx,	,
dobrovolníci	dobrovolník	k1gMnPc1	dobrovolník
byli	být	k5eAaImAgMnP	být
velmi	velmi	k6eAd1	velmi
špatně	špatně	k6eAd1	špatně
vyzbrojeni	vyzbrojit	k5eAaPmNgMnP	vyzbrojit
a	a	k8xC	a
ve	v	k7c6	v
sborech	sbor	k1gInPc6	sbor
chyběla	chybět	k5eAaImAgFnS	chybět
kázeň	kázeň	k1gFnSc1	kázeň
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc1	první
střet	střet	k1gInSc1	střet
s	s	k7c7	s
anglickými	anglický	k2eAgInPc7d1	anglický
vojsky	vojsky	k6eAd1	vojsky
u	u	k7c2	u
Bunker	Bunkra	k1gFnPc2	Bunkra
Hillu	Hill	k1gInSc2	Hill
dopadl	dopadnout	k5eAaPmAgInS	dopadnout
pro	pro	k7c4	pro
koloniální	koloniální	k2eAgNnSc4d1	koloniální
vojsko	vojsko	k1gNnSc4	vojsko
špatně	špatně	k6eAd1	špatně
<g/>
.	.	kIx.	.
</s>
<s>
Jemu	on	k3xPp3gMnSc3	on
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
podařilo	podařit	k5eAaPmAgNnS	podařit
armádu	armáda	k1gFnSc4	armáda
rychle	rychle	k6eAd1	rychle
stabilizovat	stabilizovat	k5eAaBmF	stabilizovat
a	a	k8xC	a
v	v	k7c6	v
následné	následný	k2eAgFnSc6d1	následná
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c4	o
Boston	Boston	k1gInSc4	Boston
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1776	[number]	k4	1776
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
Britové	Brit	k1gMnPc1	Brit
poraženi	porazit	k5eAaPmNgMnP	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgMnS	vydat
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
si	se	k3xPyFc3	se
zvolil	zvolit	k5eAaPmAgMnS	zvolit
za	za	k7c4	za
hlavní	hlavní	k2eAgNnSc4d1	hlavní
vojenské	vojenský	k2eAgNnSc4d1	vojenské
stanoviště	stanoviště	k1gNnSc4	stanoviště
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
jej	on	k3xPp3gMnSc4	on
také	také	k9	také
zastihla	zastihnout	k5eAaPmAgFnS	zastihnout
informace	informace	k1gFnPc4	informace
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
4	[number]	k4	4
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1776	[number]	k4	1776
došlo	dojít	k5eAaPmAgNnS	dojít
ve	v	k7c6	v
Filadelfii	Filadelfie	k1gFnSc6	Filadelfie
k	k	k7c3	k
Prohlášení	prohlášení	k1gNnSc3	prohlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
a	a	k8xC	a
třináct	třináct	k4xCc4	třináct
vzbouřeneckých	vzbouřenecký	k2eAgFnPc2d1	vzbouřenecká
kolonií	kolonie	k1gFnPc2	kolonie
vyhlásilo	vyhlásit	k5eAaPmAgNnS	vyhlásit
svou	svůj	k3xOyFgFnSc4	svůj
samostatnost	samostatnost	k1gFnSc4	samostatnost
a	a	k8xC	a
vytvořilo	vytvořit	k5eAaPmAgNnS	vytvořit
unii	unie	k1gFnSc3	unie
nazvanou	nazvaný	k2eAgFnSc4d1	nazvaná
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgInPc1d1	americký
<g/>
.	.	kIx.	.
</s>
<s>
Boje	boj	k1gInPc1	boj
s	s	k7c7	s
Brity	Brit	k1gMnPc7	Brit
ovšem	ovšem	k9	ovšem
nadále	nadále	k6eAd1	nadále
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
a	a	k8xC	a
vítězství	vítězství	k1gNnSc6	vítězství
dosahovala	dosahovat	k5eAaImAgNnP	dosahovat
střídavě	střídavě	k6eAd1	střídavě
jedna	jeden	k4xCgNnPc1	jeden
a	a	k8xC	a
pak	pak	k6eAd1	pak
zase	zase	k9	zase
druhá	druhý	k4xOgFnSc1	druhý
strana	strana	k1gFnSc1	strana
<g/>
.	.	kIx.	.
</s>
<s>
Celkově	celkově	k6eAd1	celkově
se	se	k3xPyFc4	se
však	však	k9	však
zpočátku	zpočátku	k6eAd1	zpočátku
více	hodně	k6eAd2	hodně
dařilo	dařit	k5eAaImAgNnS	dařit
Britům	Brit	k1gMnPc3	Brit
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
dostávali	dostávat	k5eAaImAgMnP	dostávat
z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
pravidelné	pravidelný	k2eAgFnPc4d1	pravidelná
posily	posila	k1gFnPc4	posila
i	i	k8xC	i
zásoby	zásoba	k1gFnPc4	zásoba
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
on	on	k3xPp3gMnSc1	on
sám	sám	k3xTgInSc1	sám
opakovaně	opakovaně	k6eAd1	opakovaně
a	a	k8xC	a
marně	marně	k6eAd1	marně
žádal	žádat	k5eAaImAgInS	žádat
Kongres	kongres	k1gInSc1	kongres
o	o	k7c4	o
získání	získání	k1gNnSc4	získání
dalších	další	k2eAgMnPc2d1	další
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
<g/>
,	,	kIx,	,
o	o	k7c4	o
zbraně	zbraň	k1gFnPc4	zbraň
a	a	k8xC	a
peníze	peníz	k1gInPc4	peníz
<g/>
.	.	kIx.	.
</s>
<s>
Nové	Nové	k2eAgFnSc3d1	Nové
republice	republika	k1gFnSc3	republika
také	také	k9	také
hrozila	hrozit	k5eAaImAgFnS	hrozit
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
nejbohatší	bohatý	k2eAgMnPc1d3	nejbohatší
obyvatelé	obyvatel	k1gMnPc1	obyvatel
bývalých	bývalý	k2eAgFnPc2d1	bývalá
kolonií	kolonie	k1gFnPc2	kolonie
měli	mít	k5eAaImAgMnP	mít
stále	stále	k6eAd1	stále
zájem	zájem	k1gInSc4	zájem
na	na	k7c4	na
udržení	udržení	k1gNnSc4	udržení
obchodních	obchodní	k2eAgInPc2d1	obchodní
styků	styk	k1gInPc2	styk
s	s	k7c7	s
Anglií	Anglie	k1gFnSc7	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Krize	krize	k1gFnSc1	krize
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
vrcholu	vrchol	k1gInSc2	vrchol
26	[number]	k4	26
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
1777	[number]	k4	1777
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
britská	britský	k2eAgNnPc4d1	Britské
vojska	vojsko	k1gNnPc4	vojsko
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
triumfálně	triumfálně	k6eAd1	triumfálně
do	do	k7c2	do
Filadelfie	Filadelfie	k1gFnSc2	Filadelfie
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
došlo	dojít	k5eAaPmAgNnS	dojít
částečně	částečně	k6eAd1	částečně
k	k	k7c3	k
obratu	obrat	k1gInSc3	obrat
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
zapříčinilo	zapříčinit	k5eAaPmAgNnS	zapříčinit
i	i	k9	i
připlutí	připlutí	k1gNnSc4	připlutí
francouzské	francouzský	k2eAgFnSc2d1	francouzská
flotily	flotila	k1gFnSc2	flotila
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
francouzské	francouzský	k2eAgFnSc2d1	francouzská
armády	armáda	k1gFnSc2	armáda
přišla	přijít	k5eAaPmAgFnS	přijít
mladé	mladá	k1gFnSc3	mladá
republice	republika	k1gFnSc3	republika
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
i	i	k8xC	i
řada	řada	k1gFnSc1	řada
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
z	z	k7c2	z
Pruska	Prusko	k1gNnSc2	Prusko
<g/>
,	,	kIx,	,
Polska	Polsko	k1gNnSc2	Polsko
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
dalších	další	k2eAgNnPc2d1	další
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
karta	karta	k1gFnSc1	karta
pomalu	pomalu	k6eAd1	pomalu
obracet	obracet	k5eAaImF	obracet
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
Americké	americký	k2eAgFnSc2d1	americká
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
.	.	kIx.	.
</s>
<s>
Největšího	veliký	k2eAgMnSc2d3	veliký
a	a	k8xC	a
nejdůležitějšího	důležitý	k2eAgNnSc2d3	nejdůležitější
vítězství	vítězství	k1gNnSc2	vítězství
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1781	[number]	k4	1781
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c4	o
Yorktown	Yorktown	k1gInSc4	Yorktown
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
této	tento	k3xDgFnSc2	tento
zdrcující	zdrcující	k2eAgFnSc2d1	zdrcující
porážky	porážka	k1gFnSc2	porážka
se	se	k3xPyFc4	se
již	již	k9	již
britská	britský	k2eAgNnPc1d1	Britské
vojska	vojsko	k1gNnPc1	vojsko
nevzpamatovala	vzpamatovat	k5eNaPmAgNnP	vzpamatovat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1782	[number]	k4	1782
probíhaly	probíhat	k5eAaImAgFnP	probíhat
přípravy	příprava	k1gFnPc1	příprava
na	na	k7c4	na
další	další	k2eAgFnSc4d1	další
klíčovou	klíčový	k2eAgFnSc4d1	klíčová
bitvu	bitva	k1gFnSc4	bitva
-	-	kIx~	-
bitvu	bitva	k1gFnSc4	bitva
o	o	k7c4	o
New	New	k1gFnSc4	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
drželi	držet	k5eAaImAgMnP	držet
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
Angličané	Angličan	k1gMnPc1	Angličan
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
té	ten	k3xDgFnSc3	ten
už	už	k6eAd1	už
nedošlo	dojít	k5eNaPmAgNnS	dojít
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1782	[number]	k4	1782
vláda	vláda	k1gFnSc1	vláda
Jeho	jeho	k3xOp3gNnSc2	jeho
Veličenstva	veličenstvo	k1gNnSc2	veličenstvo
britského	britský	k2eAgMnSc2d1	britský
krále	král	k1gMnSc2	král
Jiřího	Jiří	k1gMnSc2	Jiří
III	III	kA	III
<g/>
.	.	kIx.	.
uznala	uznat	k5eAaPmAgFnS	uznat
nezávislost	nezávislost	k1gFnSc4	nezávislost
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálně	oficiálně	k6eAd1	oficiálně
byla	být	k5eAaImAgFnS	být
válka	válka	k1gFnSc1	válka
ukončena	ukončen	k2eAgFnSc1d1	ukončena
18	[number]	k4	18
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1783	[number]	k4	1783
příměřím	příměří	k1gNnPc3	příměří
vyhlášeným	vyhlášený	k2eAgMnSc7d1	vyhlášený
Georgem	Georg	k1gMnSc7	Georg
Washingtonem	Washington	k1gInSc7	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
Vánoce	Vánoce	k1gFnPc4	Vánoce
roku	rok	k1gInSc2	rok
1783	[number]	k4	1783
se	se	k3xPyFc4	se
po	po	k7c6	po
téměř	téměř	k6eAd1	téměř
devíti	devět	k4xCc6	devět
letech	léto	k1gNnPc6	léto
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgMnPc2d1	americký
vrátil	vrátit	k5eAaPmAgMnS	vrátit
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
svoji	svůj	k3xOyFgFnSc4	svůj
usedlost	usedlost	k1gFnSc4	usedlost
Mount	Mount	k1gMnSc1	Mount
Vernon	Vernon	k1gMnSc1	Vernon
<g/>
.	.	kIx.	.
</s>
<s>
Mladou	mladý	k2eAgFnSc4d1	mladá
republiku	republika	k1gFnSc4	republika
však	však	k9	však
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
čekala	čekat	k5eAaImAgFnS	čekat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgInPc2d1	další
problémů	problém	k1gInPc2	problém
-	-	kIx~	-
v	v	k7c6	v
krizi	krize	k1gFnSc6	krize
bylo	být	k5eAaImAgNnS	být
převážně	převážně	k6eAd1	převážně
na	na	k7c6	na
zemědělství	zemědělství	k1gNnSc6	zemědělství
založené	založený	k2eAgNnSc4d1	založené
hospodářství	hospodářství	k1gNnSc4	hospodářství
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgFnPc1d1	americká
neměly	mít	k5eNaImAgFnP	mít
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1780	[number]	k4	1780
vlastní	vlastní	k2eAgFnSc2d1	vlastní
banky	banka	k1gFnSc2	banka
<g/>
,	,	kIx,	,
měnu	měna	k1gFnSc4	měna
a	a	k8xC	a
finanční	finanční	k2eAgInSc4d1	finanční
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
prudce	prudko	k6eAd1	prudko
rostla	růst	k5eAaImAgFnS	růst
inflace	inflace	k1gFnSc1	inflace
a	a	k8xC	a
dluhy	dluh	k1gInPc1	dluh
soukromníků	soukromník	k1gMnPc2	soukromník
i	i	k8xC	i
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ochromen	ochromit	k5eAaPmNgInS	ochromit
byl	být	k5eAaImAgInS	být
i	i	k9	i
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
Anglií	Anglie	k1gFnSc7	Anglie
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
většina	většina	k1gFnSc1	většina
států	stát	k1gInPc2	stát
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
odmítala	odmítat	k5eAaImAgFnS	odmítat
uznat	uznat	k5eAaPmF	uznat
USA	USA	kA	USA
za	za	k7c4	za
samostatný	samostatný	k2eAgInSc4d1	samostatný
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1789	[number]	k4	1789
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
Ústava	ústava	k1gFnSc1	ústava
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
nejpokrokovější	pokrokový	k2eAgFnSc7d3	nejpokrokovější
ústavou	ústava	k1gFnSc7	ústava
své	svůj	k3xOyFgFnSc2	svůj
doby	doba	k1gFnSc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
předsedal	předsedat	k5eAaImAgMnS	předsedat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1787	[number]	k4	1787
Ústavnímu	ústavní	k2eAgInSc3d1	ústavní
konventu	konvent	k1gInSc3	konvent
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
na	na	k7c4	na
vypracování	vypracování	k1gNnSc4	vypracování
Ústavy	ústava	k1gFnSc2	ústava
USA	USA	kA	USA
pracoval	pracovat	k5eAaImAgMnS	pracovat
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
Ústava	ústava	k1gFnSc1	ústava
uzákonila	uzákonit	k5eAaPmAgFnS	uzákonit
republikánskou	republikánský	k2eAgFnSc4d1	republikánská
formu	forma	k1gFnSc4	forma
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
napomohla	napomoct	k5eAaPmAgFnS	napomoct
upevnění	upevnění	k1gNnSc4	upevnění
státního	státní	k2eAgNnSc2d1	státní
zřízení	zřízení	k1gNnSc2	zřízení
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
docházelo	docházet	k5eAaImAgNnS	docházet
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
Kongresu	kongres	k1gInSc2	kongres
k	k	k7c3	k
neustálým	neustálý	k2eAgInPc3d1	neustálý
sporům	spor	k1gInPc3	spor
a	a	k8xC	a
třenicím	třenice	k1gFnPc3	třenice
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
státy	stát	k1gInPc7	stát
unie	unie	k1gFnSc2	unie
a	a	k8xC	a
až	až	k9	až
po	po	k7c6	po
mnoha	mnoho	k4c6	mnoho
jednáních	jednání	k1gNnPc6	jednání
bylo	být	k5eAaImAgNnS	být
dohodnuto	dohodnout	k5eAaPmNgNnS	dohodnout
místo	místo	k1gNnSc1	místo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
stane	stanout	k5eAaPmIp3nS	stanout
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
USA	USA	kA	USA
a	a	k8xC	a
sídlem	sídlo	k1gNnSc7	sídlo
vrcholných	vrcholný	k2eAgInPc2d1	vrcholný
orgánů	orgán	k1gInPc2	orgán
správy	správa	k1gFnSc2	správa
a	a	k8xC	a
prezidenta	prezident	k1gMnSc2	prezident
-	-	kIx~	-
tím	ten	k3xDgNnSc7	ten
městem	město	k1gNnSc7	město
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
nakonec	nakonec	k6eAd1	nakonec
New	New	k1gFnSc4	New
York	York	k1gInSc1	York
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1789	[number]	k4	1789
kongresový	kongresový	k2eAgInSc1d1	kongresový
volební	volební	k2eAgInSc1d1	volební
výbor	výbor	k1gInSc1	výbor
doporučil	doporučit	k5eAaPmAgInS	doporučit
jeho	jeho	k3xOp3gFnSc4	jeho
kandidaturu	kandidatura	k1gFnSc4	kandidatura
na	na	k7c4	na
úřad	úřad	k1gInSc4	úřad
prezidenta	prezident	k1gMnSc2	prezident
<g/>
.	.	kIx.	.
</s>
<s>
Kongres	kongres	k1gInSc1	kongres
jej	on	k3xPp3gInSc4	on
pak	pak	k6eAd1	pak
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1789	[number]	k4	1789
jednomyslně	jednomyslně	k6eAd1	jednomyslně
zvolil	zvolit	k5eAaPmAgMnS	zvolit
prvním	první	k4xOgMnSc7	první
prezidentem	prezident	k1gMnSc7	prezident
USA	USA	kA	USA
<g/>
.	.	kIx.	.
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1789	[number]	k4	1789
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
newyorské	newyorský	k2eAgFnSc6d1	newyorská
Federal	Federal	k1gFnSc6	Federal
Hall	Hall	k1gInSc1	Hall
slavnostně	slavnostně	k6eAd1	slavnostně
uveden	uvést	k5eAaPmNgInS	uvést
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Viceprezidentem	viceprezident	k1gMnSc7	viceprezident
byl	být	k5eAaImAgMnS	být
zvolen	zvolen	k2eAgMnSc1d1	zvolen
John	John	k1gMnSc1	John
Adams	Adams	k1gInSc1	Adams
(	(	kIx(	(
<g/>
pozdější	pozdní	k2eAgMnSc1d2	pozdější
druhý	druhý	k4xOgMnSc1	druhý
prezident	prezident	k1gMnSc1	prezident
USA	USA	kA	USA
<g/>
)	)	kIx)	)
a	a	k8xC	a
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
ministrů	ministr	k1gMnPc2	ministr
-	-	kIx~	-
ministrem	ministr	k1gMnSc7	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
-	-	kIx~	-
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Thomas	Thomas	k1gMnSc1	Thomas
Jefferson	Jefferson	k1gMnSc1	Jefferson
(	(	kIx(	(
<g/>
třetí	třetí	k4xOgMnSc1	třetí
prezident	prezident	k1gMnSc1	prezident
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tentýž	týž	k3xTgInSc4	týž
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
<g/>
,	,	kIx,	,
přišla	přijít	k5eAaPmAgFnS	přijít
z	z	k7c2	z
Paříže	Paříž	k1gFnSc2	Paříž
radostná	radostný	k2eAgFnSc1d1	radostná
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
Velké	velký	k2eAgFnSc2d1	velká
francouzské	francouzský	k2eAgFnSc2d1	francouzská
revoluce	revoluce	k1gFnSc2	revoluce
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
tou	ten	k3xDgFnSc7	ten
americkou	americký	k2eAgFnSc7d1	americká
zčásti	zčásti	k6eAd1	zčásti
inspirována	inspirovat	k5eAaBmNgNnP	inspirovat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Francouzské	francouzský	k2eAgFnSc6d1	francouzská
revoluci	revoluce	k1gFnSc6	revoluce
se	se	k3xPyFc4	se
angažoval	angažovat	k5eAaBmAgMnS	angažovat
i	i	k9	i
jeho	jeho	k3xOp3gMnSc1	jeho
velmi	velmi	k6eAd1	velmi
dobrý	dobrý	k2eAgMnSc1d1	dobrý
přítel	přítel	k1gMnSc1	přítel
a	a	k8xC	a
spolubojovník	spolubojovník	k1gMnSc1	spolubojovník
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
americké	americký	k2eAgFnSc2d1	americká
revoluce	revoluce	k1gFnSc2	revoluce
markýz	markýza	k1gFnPc2	markýza
La	la	k1gNnSc1	la
Fayette	Fayett	k1gInSc5	Fayett
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
prvního	první	k4xOgNnSc2	první
prezidentského	prezidentský	k2eAgNnSc2d1	prezidentské
období	období	k1gNnSc2	období
vykonal	vykonat	k5eAaPmAgInS	vykonat
Washington	Washington	k1gInSc1	Washington
několik	několik	k4yIc1	několik
cest	cesta	k1gFnPc2	cesta
po	po	k7c6	po
USA	USA	kA	USA
<g/>
,	,	kIx,	,
zažil	zažít	k5eAaPmAgMnS	zažít
přijetí	přijetí	k1gNnSc4	přijetí
dalších	další	k2eAgInPc2d1	další
států	stát	k1gInPc2	stát
do	do	k7c2	do
Unie	unie	k1gFnSc2	unie
-	-	kIx~	-
Rhode	Rhodos	k1gInSc5	Rhodos
Islandu	Island	k1gInSc2	Island
<g/>
,	,	kIx,	,
Vermontu	Vermont	k1gInSc2	Vermont
a	a	k8xC	a
Kentucky	Kentucka	k1gFnSc2	Kentucka
a	a	k8xC	a
také	také	k9	také
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
nového	nový	k2eAgNnSc2d1	nové
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
USA	USA	kA	USA
Filadelfie	Filadelfie	k1gFnSc2	Filadelfie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
Kongres	kongres	k1gInSc1	kongres
rovněž	rovněž	k6eAd1	rovněž
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
o	o	k7c6	o
výstavbě	výstavba	k1gFnSc6	výstavba
zcela	zcela	k6eAd1	zcela
nového	nový	k2eAgNnSc2d1	nové
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
vybral	vybrat	k5eAaPmAgMnS	vybrat
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dokončení	dokončení	k1gNnSc1	dokončení
výstavby	výstavba	k1gFnSc2	výstavba
metropole	metropol	k1gFnSc2	metropol
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
nakonec	nakonec	k6eAd1	nakonec
získala	získat	k5eAaPmAgFnS	získat
i	i	k8xC	i
jeho	jeho	k3xOp3gNnSc4	jeho
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
již	již	k6eAd1	již
nedožil	dožít	k5eNaPmAgMnS	dožít
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
první	první	k4xOgNnSc1	první
prezidentské	prezidentský	k2eAgNnSc1d1	prezidentské
období	období	k1gNnSc1	období
je	být	k5eAaImIp3nS	být
také	také	k9	také
dobou	doba	k1gFnSc7	doba
velkých	velký	k2eAgInPc2d1	velký
politických	politický	k2eAgInPc2d1	politický
rozporů	rozpor	k1gInPc2	rozpor
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
proti	proti	k7c3	proti
sobě	se	k3xPyFc3	se
stáli	stát	k5eAaImAgMnP	stát
takzvaní	takzvaný	k2eAgMnPc1d1	takzvaný
federalisté	federalista	k1gMnPc1	federalista
a	a	k8xC	a
demokratičtí	demokratický	k2eAgMnPc1d1	demokratický
republikáni	republikán	k1gMnPc1	republikán
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
dvě	dva	k4xCgNnPc1	dva
politická	politický	k2eAgNnPc1d1	politické
uskupení	uskupení	k1gNnPc1	uskupení
se	se	k3xPyFc4	se
nemohla	moct	k5eNaImAgNnP	moct
téměř	téměř	k6eAd1	téměř
na	na	k7c6	na
ničem	nic	k3yNnSc6	nic
shodnout	shodnout	k5eAaBmF	shodnout
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
věci	věc	k1gFnSc6	věc
nalezli	nalézt	k5eAaBmAgMnP	nalézt
jejich	jejich	k3xOp3gMnPc1	jejich
členové	člen	k1gMnPc1	člen
společnou	společný	k2eAgFnSc4d1	společná
řeč	řeč	k1gFnSc4	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
prezidentské	prezidentský	k2eAgFnPc4d1	prezidentská
volby	volba	k1gFnPc4	volba
roku	rok	k1gInSc2	rok
1792	[number]	k4	1792
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
za	za	k7c2	za
kandidáta	kandidát	k1gMnSc2	kandidát
opět	opět	k6eAd1	opět
George	Georg	k1gMnSc4	Georg
Washingtona	Washington	k1gMnSc4	Washington
<g/>
.	.	kIx.	.
13	[number]	k4	13
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1793	[number]	k4	1793
byl	být	k5eAaImAgInS	být
jednomyslně	jednomyslně	k6eAd1	jednomyslně
zvolen	zvolit	k5eAaPmNgInS	zvolit
podruhé	podruhé	k6eAd1	podruhé
prezidentem	prezident	k1gMnSc7	prezident
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tohoto	tento	k3xDgNnSc2	tento
druhého	druhý	k4xOgNnSc2	druhý
prezidentského	prezidentský	k2eAgNnSc2d1	prezidentské
období	období	k1gNnSc2	období
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
nadále	nadále	k6eAd1	nadále
pokračovaly	pokračovat	k5eAaImAgInP	pokračovat
silné	silný	k2eAgInPc1d1	silný
politické	politický	k2eAgInPc1d1	politický
spory	spor	k1gInPc1	spor
<g/>
,	,	kIx,	,
Anglie	Anglie	k1gFnSc1	Anglie
navíc	navíc	k6eAd1	navíc
stále	stále	k6eAd1	stále
podněcovala	podněcovat	k5eAaImAgFnS	podněcovat
Indiány	Indián	k1gMnPc4	Indián
k	k	k7c3	k
útokům	útok	k1gInPc3	útok
na	na	k7c4	na
americké	americký	k2eAgMnPc4d1	americký
osady	osada	k1gFnSc2	osada
a	a	k8xC	a
farmy	farma	k1gFnSc2	farma
a	a	k8xC	a
odmítala	odmítat	k5eAaImAgFnS	odmítat
stáhnout	stáhnout	k5eAaPmF	stáhnout
své	svůj	k3xOyFgMnPc4	svůj
vojáky	voják	k1gMnPc4	voják
z	z	k7c2	z
území	území	k1gNnSc2	území
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
On	on	k3xPp3gMnSc1	on
sám	sám	k3xTgMnSc1	sám
již	již	k9	již
také	také	k9	také
nebyl	být	k5eNaImAgInS	být
tak	tak	k6eAd1	tak
bezmezně	bezmezně	k6eAd1	bezmezně
obdivován	obdivován	k2eAgInSc1d1	obdivován
jako	jako	k8xS	jako
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
byl	být	k5eAaImAgMnS	být
kritizován	kritizovat	k5eAaImNgMnS	kritizovat
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
názorově	názorově	k6eAd1	názorově
sbližuje	sbližovat	k5eAaImIp3nS	sbližovat
s	s	k7c7	s
federalisty	federalista	k1gMnPc7	federalista
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
ve	v	k7c6	v
sporech	spor	k1gInPc6	spor
nestranný	nestranný	k2eAgInSc1d1	nestranný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1796	[number]	k4	1796
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
do	do	k7c2	do
Unie	unie	k1gFnSc2	unie
další	další	k2eAgInSc4d1	další
stát	stát	k1gInSc4	stát
Tennessee	Tennesse	k1gInSc2	Tennesse
<g/>
,	,	kIx,	,
národní	národní	k2eAgNnSc1d1	národní
hospodářství	hospodářství	k1gNnSc1	hospodářství
narůstalo	narůstat	k5eAaImAgNnS	narůstat
<g/>
,	,	kIx,	,
životní	životní	k2eAgFnSc1d1	životní
úroveň	úroveň	k1gFnSc1	úroveň
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
zvyšovala	zvyšovat	k5eAaImAgFnS	zvyšovat
<g/>
,	,	kIx,	,
18	[number]	k4	18
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1793	[number]	k4	1793
byl	být	k5eAaImAgInS	být
položen	položit	k5eAaPmNgInS	položit
základní	základní	k2eAgInSc1d1	základní
kámen	kámen	k1gInSc1	kámen
nového	nový	k2eAgNnSc2d1	nové
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Pensylvánii	Pensylvánie	k1gFnSc6	Pensylvánie
byla	být	k5eAaImAgFnS	být
objevena	objeven	k2eAgFnSc1d1	objevena
velká	velká	k1gFnSc1	velká
naleziště	naleziště	k1gNnSc2	naleziště
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
byl	být	k5eAaImAgMnS	být
příslib	příslib	k1gInSc4	příslib
do	do	k7c2	do
budoucna	budoucno	k1gNnSc2	budoucno
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
dosud	dosud	k6eAd1	dosud
zemědělsky	zemědělsky	k6eAd1	zemědělsky
zaměřená	zaměřený	k2eAgFnSc1d1	zaměřená
republika	republika	k1gFnSc1	republika
může	moct	k5eAaImIp3nS	moct
orientovat	orientovat	k5eAaBmF	orientovat
i	i	k9	i
na	na	k7c4	na
průmysl	průmysl	k1gInSc4	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
slavnostním	slavnostní	k2eAgNnSc6d1	slavnostní
uvedení	uvedení	k1gNnSc6	uvedení
Johna	John	k1gMnSc2	John
Adamse	Adams	k1gMnSc2	Adams
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
amerického	americký	k2eAgMnSc2d1	americký
prezidenta	prezident	k1gMnSc2	prezident
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
1797	[number]	k4	1797
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
rodné	rodný	k2eAgFnSc2d1	rodná
Virginie	Virginie	k1gFnSc2	Virginie
a	a	k8xC	a
na	na	k7c4	na
svůj	svůj	k3xOyFgInSc4	svůj
milovaný	milovaný	k2eAgInSc4d1	milovaný
Mount	Mount	k1gInSc4	Mount
Vernon	Vernona	k1gFnPc2	Vernona
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
věnovat	věnovat	k5eAaImF	věnovat
rodině	rodina	k1gFnSc3	rodina
a	a	k8xC	a
řízení	řízení	k1gNnSc3	řízení
svých	svůj	k3xOyFgFnPc2	svůj
farem	farma	k1gFnPc2	farma
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1798	[number]	k4	1798
ale	ale	k8xC	ale
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
možného	možný	k2eAgInSc2d1	možný
válečného	válečný	k2eAgInSc2d1	válečný
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
Francií	Francie	k1gFnSc7	Francie
a	a	k8xC	a
generál	generál	k1gMnSc1	generál
George	Georg	k1gFnSc2	Georg
Washington	Washington	k1gInSc1	Washington
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
šestašedesáti	šestašedesát	k4xCc6	šestašedesát
letech	léto	k1gNnPc6	léto
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
však	však	k9	však
nebezpečí	nebezpečí	k1gNnSc1	nebezpečí
pominulo	pominout	k5eAaPmAgNnS	pominout
a	a	k8xC	a
on	on	k3xPp3gMnSc1	on
se	se	k3xPyFc4	se
již	již	k6eAd1	již
navždy	navždy	k6eAd1	navždy
vrátil	vrátit	k5eAaPmAgMnS	vrátit
na	na	k7c4	na
Mount	Mount	k1gInSc4	Mount
Vernon	Vernona	k1gFnPc2	Vernona
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1799	[number]	k4	1799
během	během	k7c2	během
projížďky	projížďka	k1gFnSc2	projížďka
na	na	k7c6	na
koni	kůň	k1gMnSc6	kůň
prochladl	prochladnout	k5eAaPmAgInS	prochladnout
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1799	[number]	k4	1799
na	na	k7c4	na
následky	následek	k1gInPc4	následek
infekčního	infekční	k2eAgInSc2d1	infekční
zánětu	zánět	k1gInSc2	zánět
hrdla	hrdlo	k1gNnSc2	hrdlo
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
počest	počest	k1gFnSc6	počest
bylo	být	k5eAaImAgNnS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
nově	nově	k6eAd1	nově
založené	založený	k2eAgNnSc1d1	založené
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
USA	USA	kA	USA
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
amerických	americký	k2eAgInPc2d1	americký
států	stát	k1gInPc2	stát
a	a	k8xC	a
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
historie	historie	k1gFnSc2	historie
také	také	k9	také
čtyři	čtyři	k4xCgNnPc4	čtyři
plavidla	plavidlo	k1gNnPc4	plavidlo
amerického	americký	k2eAgNnSc2d1	americké
námořnictva	námořnictvo	k1gNnSc2	námořnictvo
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
George	Georg	k1gInSc2	Georg
Washington	Washington	k1gInSc1	Washington
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
George	George	k1gInSc1	George
Washington	Washington	k1gInSc1	Washington
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
