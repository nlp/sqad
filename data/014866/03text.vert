<s>
Nakládalova	Nakládalův	k2eAgFnSc1d1
vila	vila	k1gFnSc1
</s>
<s>
Nakládalova	Nakládalův	k2eAgFnSc1d1
vila	vila	k1gFnSc1
Uliční	uliční	k2eAgNnSc4d1
průčelí	průčelí	k1gNnSc4
vily	vila	k1gFnSc2
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
Polívkova	Polívkův	k2eAgFnSc1d1
35	#num#	k4
<g/>
,	,	kIx,
Nová	nový	k2eAgFnSc1d1
Ulice	ulice	k1gFnSc1
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Ulice	ulice	k1gFnSc2
</s>
<s>
Polívkova	Polívkův	k2eAgFnSc1d1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
35	#num#	k4
<g/>
′	′	k?
<g/>
25,8	25,8	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
17	#num#	k4
<g/>
°	°	k?
<g/>
14	#num#	k4
<g/>
′	′	k?
<g/>
30,12	30,12	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Rejstříkové	rejstříkový	k2eAgFnPc1d1
číslo	číslo	k1gNnSc4
památky	památka	k1gFnSc2
</s>
<s>
34327	#num#	k4
<g/>
/	/	kIx~
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
2266	#num#	k4
(	(	kIx(
<g/>
Pk	Pk	k1gFnSc1
<g/>
•	•	k?
<g/>
MIS	mísa	k1gFnPc2
<g/>
•	•	k?
<g/>
Sez	Sez	k1gMnSc1
<g/>
•	•	k?
<g/>
Obr	obr	k1gMnSc1
<g/>
)	)	kIx)
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Nakládalova	Nakládalův	k2eAgFnSc1d1
Vila	vila	k1gFnSc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
také	také	k9
Vila	vila	k1gFnSc1
Stanislava	Stanislav	k1gMnSc2
Nakládala	nakládat	k5eAaImAgFnS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
výjimečně	výjimečně	k6eAd1
dobře	dobře	k6eAd1
dochovaným	dochovaný	k2eAgInSc7d1
příkladem	příklad	k1gInSc7
meziválečné	meziválečný	k2eAgFnSc2d1
přísně	přísně	k6eAd1
funkcionalistické	funkcionalistický	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
nejcennější	cenný	k2eAgFnSc1d3
funkcionalistická	funkcionalistický	k2eAgFnSc1d1
památka	památka	k1gFnSc1
v	v	k7c6
Olomouci	Olomouc	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejím	její	k3xOp3gMnSc7
autorem	autor	k1gMnSc7
je	být	k5eAaImIp3nS
architekt	architekt	k1gMnSc1
Lubomír	Lubomír	k1gMnSc1
Šlapeta	Šlapeta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
postavena	postaven	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1936	#num#	k4
v	v	k7c6
Polívkově	Polívkův	k2eAgFnSc6d1
ulici	ulice	k1gFnSc6
35	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vila	vila	k1gFnSc1
je	být	k5eAaImIp3nS
přístupná	přístupný	k2eAgFnSc1d1
veřejnosti	veřejnost	k1gFnSc3
<g/>
,	,	kIx,
protože	protože	k8xS
v	v	k7c6
současnosti	současnost	k1gFnSc6
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
ve	v	k7c6
vile	vila	k1gFnSc6
lékařská	lékařský	k2eAgFnSc1d1
ordinace	ordinace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Architektura	architektura	k1gFnSc1
vily	vila	k1gFnSc2
</s>
<s>
Vila	vila	k1gFnSc1
je	být	k5eAaImIp3nS
výjimečným	výjimečný	k2eAgInSc7d1
příkladem	příklad	k1gInSc7
typu	typ	k1gInSc2
tzv.	tzv.	kA
bílých	bílý	k2eAgFnPc2d1
vil	vila	k1gFnPc2
<g/>
,	,	kIx,
jednoduchých	jednoduchý	k2eAgFnPc2d1
(	(	kIx(
<g/>
ale	ale	k8xC
luxusních	luxusní	k2eAgFnPc2d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kubických	kubický	k2eAgInPc2d1
<g/>
,	,	kIx,
bíle	bíle	k6eAd1
omítnutých	omítnutý	k2eAgFnPc2d1
staveb	stavba	k1gFnPc2
s	s	k7c7
plochou	plochý	k2eAgFnSc7d1
střechou	střecha	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průčelí	průčelí	k1gNnPc1
do	do	k7c2
ulice	ulice	k1gFnSc2
je	být	k5eAaImIp3nS
velmi	velmi	k6eAd1
jednoduše	jednoduše	k6eAd1
členěno	členit	k5eAaImNgNnS
pouze	pouze	k6eAd1
základními	základní	k2eAgInPc7d1
geometrickými	geometrický	k2eAgInPc7d1
tvary	tvar	k1gInPc7
oken	okno	k1gNnPc2
a	a	k8xC
dveří	dveře	k1gFnPc2
<g/>
,	,	kIx,
jediným	jediný	k2eAgInSc7d1
výstupkem	výstupek	k1gInSc7
na	na	k7c6
fasádě	fasáda	k1gFnSc6
je	být	k5eAaImIp3nS
plochá	plochý	k2eAgFnSc1d1
stříška	stříška	k1gFnSc1
nad	nad	k7c7
vchodem	vchod	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zadní	zadní	k2eAgInPc1d1
<g/>
,	,	kIx,
zahradní	zahradní	k2eAgNnSc1d1
průčelí	průčelí	k1gNnSc1
je	být	k5eAaImIp3nS
členěno	členit	k5eAaImNgNnS
více	hodně	k6eAd2
<g/>
,	,	kIx,
kromě	kromě	k7c2
schodiště	schodiště	k1gNnSc2
do	do	k7c2
zahrady	zahrada	k1gFnSc2
se	se	k3xPyFc4
zde	zde	k6eAd1
nachází	nacházet	k5eAaImIp3nS
nižší	nízký	k2eAgInSc4d2
kubus	kubus	k1gInSc4
někdejší	někdejší	k2eAgFnSc2d1
jídelny	jídelna	k1gFnSc2
a	a	k8xC
zimní	zimní	k2eAgFnSc2d1
zahrady	zahrada	k1gFnSc2
se	s	k7c7
střešní	střešní	k2eAgFnSc7d1
terasou	terasa	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Oplocení	oplocení	k1gNnSc1
pozemku	pozemek	k1gInSc2
kolem	kolem	k7c2
vily	vila	k1gFnSc2
a	a	k8xC
prvky	prvek	k1gInPc1
zábradlí	zábradlí	k1gNnSc2
odkazují	odkazovat	k5eAaImIp3nP
na	na	k7c4
dílo	dílo	k1gNnSc4
Šlapetova	Šlapetův	k2eAgMnSc2d1
učitele	učitel	k1gMnSc2
<g/>
,	,	kIx,
vratislavského	vratislavský	k2eAgMnSc2d1
architekta	architekt	k1gMnSc2
Hanse	Hans	k1gMnSc2
Scharouna	Scharoun	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
vile	vila	k1gFnSc6
se	se	k3xPyFc4
částečně	částečně	k6eAd1
zachovalo	zachovat	k5eAaPmAgNnS
původní	původní	k2eAgNnSc4d1
vybavení	vybavení	k1gNnSc4
podle	podle	k7c2
návrhu	návrh	k1gInSc2
Lubomíra	Lubomír	k1gMnSc2
Šlapety	Šlapeta	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Černoušek	černoušek	k1gMnSc1
<g/>
,	,	kIx,
<g/>
Vladimír	Vladimír	k1gMnSc1
Šlapeta	Šlapeto	k1gNnSc2
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
Zatloukal	Zatloukal	k1gMnSc1
<g/>
:	:	kIx,
Olomoucká	olomoucký	k2eAgFnSc1d1
architektura	architektura	k1gFnSc1
1900	#num#	k4
<g/>
-	-	kIx~
<g/>
1950	#num#	k4
<g/>
,	,	kIx,
Olomouc	Olomouc	k1gFnSc1
1981	#num#	k4
</s>
<s>
Petr	Petr	k1gMnSc1
Pelčák	Pelčák	k1gMnSc1
<g/>
,	,	kIx,
Vladimír	Vladimír	k1gMnSc1
Šlapeta	Šlapeto	k1gNnSc2
<g/>
,	,	kIx,
Pavel	Pavel	k1gMnSc1
Zatloukal	Zatloukal	k1gMnSc1
<g/>
:	:	kIx,
Lubomír	Lubomír	k1gMnSc1
Šlapeta	Šlapet	k1gMnSc2
-	-	kIx~
Čestmír	Čestmír	k1gMnSc1
Šlapeta	Šlapet	k1gMnSc2
-	-	kIx~
Architektonické	architektonický	k2eAgNnSc1d1
dílo	dílo	k1gNnSc1
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
2003	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
80-85227-56-8	80-85227-56-8	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Vila	vila	k1gFnSc1
Stanislava	Stanislav	k1gMnSc2
Nakládala	nakládat	k5eAaImAgFnS
na	na	k7c6
serveru	server	k1gInSc6
slavnevily	slavnevit	k5eAaPmAgFnP,k5eAaImAgFnP
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
