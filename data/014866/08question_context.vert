<s>
Nakládalova	Nakládalův	k2eAgFnSc1d1
Vila	vila	k1gFnSc1
<g/>
,	,	kIx,
nebo	nebo	k8xC
také	také	k9
Vila	vila	k1gFnSc1
Stanislava	Stanislav	k1gMnSc2
Nakládala	nakládat	k5eAaImAgFnS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
výjimečně	výjimečně	k6eAd1
dobře	dobře	k6eAd1
dochovaným	dochovaný	k2eAgInSc7d1
příkladem	příklad	k1gInSc7
meziválečné	meziválečný	k2eAgFnSc2d1
přísně	přísně	k6eAd1
funkcionalistické	funkcionalistický	k2eAgFnSc2d1
architektury	architektura	k1gFnSc2
<g/>
.	.	kIx.
</s>