<s>
Filament	Filament	k1gInSc1	Filament
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
textilní	textilní	k2eAgFnSc6d1	textilní
terminologii	terminologie	k1gFnSc6	terminologie
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
označení	označení	k1gNnSc4	označení
pro	pro	k7c4	pro
všechna	všechen	k3xTgNnPc1	všechen
vlákna	vlákno	k1gNnPc1	vlákno
neomezené	omezený	k2eNgFnSc2d1	neomezená
délky	délka	k1gFnSc2	délka
<g/>
.	.	kIx.	.
</s>
<s>
Filament	Filament	k1gInSc1	Filament
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
souhrnné	souhrnný	k2eAgNnSc1d1	souhrnné
pojmenování	pojmenování	k1gNnSc1	pojmenování
pro	pro	k7c4	pro
všechna	všechen	k3xTgNnPc4	všechen
umělá	umělý	k2eAgNnPc4d1	umělé
textilní	textilní	k2eAgNnPc4d1	textilní
vlákna	vlákno	k1gNnPc4	vlákno
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
vláken	vlákno	k1gNnPc2	vlákno
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
stříhána	stříhán	k2eAgMnSc4d1	stříhán
<g/>
,	,	kIx,	,
řezána	řezán	k2eAgMnSc4d1	řezán
nebo	nebo	k8xC	nebo
trhána	trhat	k5eAaImNgFnS	trhat
na	na	k7c4	na
určitou	určitý	k2eAgFnSc4d1	určitá
staplovou	staplový	k2eAgFnSc4d1	staplová
délku	délka	k1gFnSc4	délka
a	a	k8xC	a
přírodní	přírodní	k2eAgNnSc4d1	přírodní
hedvábí	hedvábí	k1gNnSc4	hedvábí
</s>
<s>
V	v	k7c6	v
odborné	odborný	k2eAgFnSc6d1	odborná
češtině	čeština	k1gFnSc6	čeština
se	se	k3xPyFc4	se
však	však	k9	však
(	(	kIx(	(
<g/>
odlišně	odlišně	k6eAd1	odlišně
od	od	k7c2	od
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
standardu	standard	k1gInSc2	standard
<g/>
)	)	kIx)	)
filament	filament	k1gInSc1	filament
definuje	definovat	k5eAaBmIp3nS	definovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
jedničné	jedničný	k2eAgNnSc1d1	jedničný
nekonečné	konečný	k2eNgNnSc1d1	nekonečné
vlákno	vlákno	k1gNnSc1	vlákno
z	z	k7c2	z
chemického	chemický	k2eAgNnSc2d1	chemické
hedvábí	hedvábí	k1gNnSc2	hedvábí
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Například	například	k6eAd1	například
podle	podle	k7c2	podle
německého	německý	k2eAgInSc2d1	německý
Zákona	zákon	k1gInSc2	zákon
o	o	k7c6	o
označování	označování	k1gNnSc6	označování
textilií	textilie	k1gFnPc2	textilie
se	se	k3xPyFc4	se
jako	jako	k9	jako
hedvábí	hedvábí	k1gNnSc1	hedvábí
(	(	kIx(	(
<g/>
Seide	Seid	k1gMnSc5	Seid
<g/>
)	)	kIx)	)
smějí	smát	k5eAaImIp3nP	smát
označovat	označovat	k5eAaImF	označovat
pouze	pouze	k6eAd1	pouze
výrobky	výrobek	k1gInPc4	výrobek
z	z	k7c2	z
přírodního	přírodní	k2eAgNnSc2d1	přírodní
hedvábí	hedvábí	k1gNnSc2	hedvábí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Chemické	chemický	k2eAgNnSc1d1	chemické
vlákno	vlákno	k1gNnSc1	vlákno
bez	bez	k7c2	bez
omezení	omezení	k1gNnSc2	omezení
délky	délka	k1gFnSc2	délka
se	se	k3xPyFc4	se
dodává	dodávat	k5eAaImIp3nS	dodávat
k	k	k7c3	k
textilnímu	textilní	k2eAgNnSc3d1	textilní
zpracování	zpracování	k1gNnSc3	zpracování
nejčastěji	často	k6eAd3	často
jako	jako	k8xS	jako
svazek	svazek	k1gInSc4	svazek
elementárních	elementární	k2eAgFnPc2d1	elementární
vláken	vlákna	k1gFnPc2	vlákna
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
sdružených	sdružený	k2eAgMnPc2d1	sdružený
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
otvorů	otvor	k1gInPc2	otvor
jedné	jeden	k4xCgFnSc2	jeden
zvlákňovací	zvlákňovací	k2eAgFnSc2d1	zvlákňovací
trysky	tryska	k1gFnSc2	tryska
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Těmto	tento	k3xDgMnPc3	tento
elementárním	elementární	k2eAgMnPc3d1	elementární
vláknům	vlákno	k1gNnPc3	vlákno
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
říkalo	říkat	k5eAaImAgNnS	říkat
kapiláry	kapilára	k1gFnSc2	kapilára
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
často	často	k6eAd1	často
používá	používat	k5eAaImIp3nS	používat
také	také	k9	také
označení	označení	k1gNnSc2	označení
filamenty	filament	k1gInPc1	filament
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
elementárních	elementární	k2eAgNnPc2d1	elementární
vláken	vlákno	k1gNnPc2	vlákno
ve	v	k7c6	v
filamentové	filamentový	k2eAgFnSc6d1	filamentový
přízi	příz	k1gFnSc6	příz
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
písmenem	písmeno	k1gNnSc7	písmeno
f	f	k?	f
<g/>
,	,	kIx,	,
např.	např.	kA	např.
polyesterová	polyesterový	k2eAgFnSc1d1	polyesterová
příze	příze	k1gFnSc1	příze
s	s	k7c7	s
jemností	jemnost	k1gFnSc7	jemnost
"	"	kIx"	"
<g/>
5	[number]	k4	5
tex	tex	k?	tex
f	f	k?	f
18	[number]	k4	18
<g/>
"	"	kIx"	"
sestává	sestávat	k5eAaImIp3nS	sestávat
z	z	k7c2	z
18	[number]	k4	18
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
vláken	vlákno	k1gNnPc2	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Filamenty	Filament	k1gInPc1	Filament
pro	pro	k7c4	pro
určité	určitý	k2eAgInPc4d1	určitý
účely	účel	k1gInPc4	účel
(	(	kIx(	(
<g/>
rybářské	rybářský	k2eAgFnPc4d1	rybářská
šňůry	šňůra	k1gFnPc4	šňůra
<g/>
,	,	kIx,	,
štětiny	štětina	k1gFnPc4	štětina
kartáčů	kartáč	k1gInPc2	kartáč
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
zvlákňují	zvlákňovat	k5eAaImIp3nP	zvlákňovat
přes	přes	k7c4	přes
trysku	tryska	k1gFnSc4	tryska
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
otvorem	otvor	k1gInSc7	otvor
a	a	k8xC	a
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
jako	jako	k9	jako
jednoduchá	jednoduchý	k2eAgNnPc1d1	jednoduché
(	(	kIx(	(
<g/>
poměrně	poměrně	k6eAd1	poměrně
tlustá	tlustý	k2eAgFnSc1d1	tlustá
<g/>
)	)	kIx)	)
nit	nit	k1gFnSc1	nit
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgNnPc1	tento
vlákna	vlákno	k1gNnPc1	vlákno
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
monofily	monofil	k1gInPc1	monofil
<g/>
.	.	kIx.	.
</s>
<s>
Svazku	svazek	k1gInSc2	svazek
jednotlivých	jednotlivý	k2eAgNnPc2d1	jednotlivé
vláken	vlákno	k1gNnPc2	vlákno
se	se	k3xPyFc4	se
říká	říkat	k5eAaImIp3nS	říkat
multifil	multifil	k1gInSc1	multifil
<g/>
.	.	kIx.	.
</s>
<s>
Světová	světový	k2eAgFnSc1d1	světová
výroba	výroba	k1gFnSc1	výroba
filamentů	filament	k1gInPc2	filament
se	se	k3xPyFc4	se
v	v	k7c6	v
1	[number]	k4	1
<g/>
.	.	kIx.	.
dekádě	dekáda	k1gFnSc6	dekáda
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
zvýšila	zvýšit	k5eAaPmAgFnS	zvýšit
asi	asi	k9	asi
o	o	k7c4	o
70	[number]	k4	70
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
obnášela	obnášet	k5eAaImAgFnS	obnášet
celková	celkový	k2eAgFnSc1d1	celková
produkce	produkce	k1gFnSc1	produkce
44	[number]	k4	44
miliony	milion	k4xCgInPc4	milion
tun	tuna	k1gFnPc2	tuna
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
většina	většina	k1gFnSc1	většina
polyesterových	polyesterový	k2eAgInPc2d1	polyesterový
a	a	k8xC	a
polyamidových	polyamidový	k2eAgNnPc2d1	polyamidové
vláken	vlákno	k1gNnPc2	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
filament	filament	k1gInSc1	filament
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
v	v	k7c6	v
několika	několik	k4yIc6	několik
jiných	jiný	k2eAgInPc6d1	jiný
oborech	obor	k1gInPc6	obor
(	(	kIx(	(
<g/>
elektrotechnika	elektrotechnika	k1gFnSc1	elektrotechnika
<g/>
,	,	kIx,	,
biologie	biologie	k1gFnSc1	biologie
<g/>
,	,	kIx,	,
astronomie	astronomie	k1gFnSc1	astronomie
<g/>
)	)	kIx)	)
bez	bez	k7c2	bez
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
souvislosti	souvislost	k1gFnSc2	souvislost
s	s	k7c7	s
textilními	textilní	k2eAgNnPc7d1	textilní
vlákny	vlákno	k1gNnPc7	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Schenek	Schenka	k1gFnPc2	Schenka
<g/>
:	:	kIx,	:
Lexikon	lexikon	k1gInSc1	lexikon
Garne	Garn	k1gInSc5	Garn
und	und	k?	und
Zwirne	Zwirn	k1gInSc5	Zwirn
str	str	kA	str
<g/>
.	.	kIx.	.
156	[number]	k4	156
<g/>
-	-	kIx~	-
<g/>
157	[number]	k4	157
<g/>
,	,	kIx,	,
Deutscher	Deutschra	k1gFnPc2	Deutschra
Fachverlag	Fachverlaga	k1gFnPc2	Fachverlaga
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3-87150-810-1	[number]	k4	3-87150-810-1
Alfons	Alfons	k1gMnSc1	Alfons
Hofer	Hofer	k?	Hofer
<g/>
,	,	kIx,	,
Stoffe	Stoff	k1gMnSc5	Stoff
:	:	kIx,	:
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Textilrohstoffe	Textilrohstoff	k1gMnSc5	Textilrohstoff
<g/>
,	,	kIx,	,
Garne	Garn	k1gMnSc5	Garn
<g/>
,	,	kIx,	,
Effekte	Effekt	k1gInSc5	Effekt
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.	.	kIx.	.
38	[number]	k4	38
a	a	k8xC	a
469	[number]	k4	469
,	,	kIx,	,
7	[number]	k4	7
<g/>
.	.	kIx.	.
völlig	völlig	k1gInSc1	völlig
überarbeitetet	überarbeiteteta	k1gFnPc2	überarbeiteteta
Auflage	Auflage	k1gInSc1	Auflage
<g/>
,	,	kIx,	,
Frankfurt	Frankfurt	k1gInSc1	Frankfurt
am	am	k?	am
Main	Maina	k1gFnPc2	Maina
:	:	kIx,	:
Deutscher	Deutschra	k1gFnPc2	Deutschra
Fachverlag	Fachverlag	k1gInSc1	Fachverlag
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
3-87150-366-5	[number]	k4	3-87150-366-5
</s>
