<s>
Kolibříci	kolibřík	k1gMnPc1	kolibřík
jsou	být	k5eAaImIp3nP	být
ve	v	k7c6	v
velikosti	velikost	k1gFnSc6	velikost
viditelně	viditelně	k6eAd1	viditelně
odlišní	odlišný	k2eAgMnPc1d1	odlišný
<g/>
,	,	kIx,	,
nejmenší	malý	k2eAgMnPc4d3	nejmenší
zástupce	zástupce	k1gMnPc4	zástupce
celé	celý	k2eAgFnSc2d1	celá
čeledi	čeleď	k1gFnSc2	čeleď
<g/>
,	,	kIx,	,
kalypta	kalypto	k1gNnPc4	kalypto
nejmenší	malý	k2eAgNnPc4d3	nejmenší
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
nejmenším	malý	k2eAgMnSc7d3	nejmenší
ptákem	pták	k1gMnSc7	pták
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
největším	veliký	k2eAgMnSc7d3	veliký
zástupcem	zástupce	k1gMnSc7	zástupce
je	být	k5eAaImIp3nS	být
naopak	naopak	k6eAd1	naopak
kolibřík	kolibřík	k1gMnSc1	kolibřík
velký	velký	k2eAgMnSc1d1	velký
<g/>
.	.	kIx.	.
</s>
