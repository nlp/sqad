<s>
Nedostatek	nedostatek	k1gInSc1
hořčíku	hořčík	k1gInSc2
u	u	k7c2
rostlin	rostlina	k1gFnPc2
</s>
<s>
Krušina	krušina	k1gFnSc1
olšová	olšový	k2eAgFnSc1d1
s	s	k7c7
nedostatkem	nedostatek	k1gInSc7
hořčíku	hořčík	k1gInSc2
-	-	kIx~
chloróza	chloróza	k1gFnSc1
mezi	mezi	k7c7
cévními	cévní	k2eAgInPc7d1
svazky	svazek	k1gInPc7
<g/>
.	.	kIx.
</s>
<s>
Nedostatek	nedostatek	k1gInSc1
hořčíku	hořčík	k1gInSc2
u	u	k7c2
rostlin	rostlina	k1gFnPc2
je	být	k5eAaImIp3nS
fyziologická	fyziologický	k2eAgFnSc1d1
porucha	porucha	k1gFnSc1
rostlin	rostlina	k1gFnPc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
způsobena	způsobit	k5eAaPmNgFnS
nedostatkem	nedostatek	k1gInSc7
příjmu	příjem	k1gInSc2
hořčíku	hořčík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgMnSc7
příznakem	příznak	k1gInSc7
jsou	být	k5eAaImIp3nP
chlorózy	chloróza	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
nedostatku	nedostatek	k1gInSc6
draslíku	draslík	k1gInSc2
se	se	k3xPyFc4
příznaky	příznak	k1gInPc1
zesilují	zesilovat	k5eAaImIp3nP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
L	L	kA
1	#num#	k4
<g/>
]	]	kIx)
Hořčík	hořčík	k1gInSc1
hraje	hrát	k5eAaImIp3nS
důležitou	důležitý	k2eAgFnSc4d1
roli	role	k1gFnSc4
ve	v	k7c6
fotosyntéze	fotosyntéza	k1gFnSc6
<g/>
,	,	kIx,
protože	protože	k8xS
tvoří	tvořit	k5eAaImIp3nP
centrální	centrální	k2eAgInSc4d1
atom	atom	k1gInSc4
chlorofylu	chlorofyl	k1gInSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
chelátově	chelátově	k6eAd1
vázán	vázat	k5eAaImNgMnS
v	v	k7c6
porfyrinovém	porfyrinový	k2eAgNnSc6d1
jádře	jádro	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Bez	bez	k7c2
dostatečného	dostatečný	k2eAgNnSc2d1
množství	množství	k1gNnSc2
hořčíku	hořčík	k1gInSc2
proto	proto	k8xC
pro	pro	k7c4
pokrytí	pokrytí	k1gNnSc4
potřeby	potřeba	k1gFnSc2
hořčíku	hořčík	k1gInSc2
v	v	k7c6
mladých	mladý	k2eAgInPc6d1
listech	list	k1gInPc6
<g/>
,	,	kIx,
rostliny	rostlina	k1gFnPc1
začnou	začít	k5eAaPmIp3nP
odbourávat	odbourávat	k5eAaImF
chlorofyl	chlorofyl	k1gInSc4
ve	v	k7c6
starých	starý	k2eAgInPc6d1
listech	list	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
způsobí	způsobit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
hlavním	hlavní	k2eAgInSc7d1
příznakem	příznak	k1gInSc7
nedostatku	nedostatek	k1gInSc2
hořčíku	hořčík	k1gInSc2
je	být	k5eAaImIp3nS
chloróza	chloróza	k1gFnSc1
nebo	nebo	k8xC
žloutnutí	žloutnutí	k1gNnSc1
listů	list	k1gInPc2
mezi	mezi	k7c7
nervaturou	nervatura	k1gFnSc7
která	který	k3yIgFnSc1,k3yRgFnSc1,k3yQgFnSc1
zůstává	zůstávat	k5eAaImIp3nS
zelená	zelený	k2eAgFnSc1d1
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
listům	list	k1gInPc3
rostlin	rostlina	k1gFnPc2
dodává	dodávat	k5eAaImIp3nS
mramorovaný	mramorovaný	k2eAgInSc4d1
vzhled	vzhled	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hořčík	hořčík	k1gInSc1
je	být	k5eAaImIp3nS
také	také	k9
nutný	nutný	k2eAgInSc1d1
aktivátor	aktivátor	k1gInSc1
mnoha	mnoho	k4c2
důležitých	důležitý	k2eAgInPc2d1
enzymů	enzym	k1gInPc2
a	a	k8xC
to	ten	k3xDgNnSc1
i	i	k9
enzymů	enzym	k1gInPc2
fotosyntézy	fotosyntéza	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hořčík	hořčík	k1gInSc1
je	být	k5eAaImIp3nS
také	také	k9
velmi	velmi	k6eAd1
důležitý	důležitý	k2eAgInSc1d1
při	při	k7c6
stabilizaci	stabilizace	k1gFnSc6
struktury	struktura	k1gFnSc2
ribozomu	ribozom	k1gInSc2
<g/>
,	,	kIx,
takže	takže	k8xS
nedostatek	nedostatek	k1gInSc1
hořčíku	hořčík	k1gInSc2
způsobuje	způsobovat	k5eAaImIp3nS
depolymerizaci	depolymerizace	k1gFnSc4
ribozomů	ribozom	k1gInPc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vede	vést	k5eAaImIp3nS
k	k	k7c3
předčasnému	předčasný	k2eAgNnSc3d1
stárnutí	stárnutí	k1gNnSc3
rostlin	rostlina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
delší	dlouhý	k2eAgFnSc6d2
době	doba	k1gFnSc6
nedostatku	nedostatek	k1gInSc2
hořčíku	hořčík	k1gInSc2
dochází	docházet	k5eAaImIp3nS
k	k	k7c3
nekrózám	nekróza	k1gFnPc3
listů	list	k1gInPc2
a	a	k8xC
shazování	shazování	k1gNnSc2
starších	starý	k2eAgInPc2d2
listů	list	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rostliny	rostlina	k1gFnPc1
s	s	k7c7
nedostatkem	nedostatek	k1gInSc7
hořčíku	hořčík	k1gInSc2
také	také	k9
vytváří	vytvářit	k5eAaPmIp3nS,k5eAaImIp3nS
menší	malý	k2eAgNnSc1d2
<g/>
,	,	kIx,
dřevnatější	dřevnatý	k2eAgNnSc1d2
ovoce	ovoce	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Výskyt	výskyt	k1gInSc1
</s>
<s>
Vyskytuje	vyskytovat	k5eAaImIp3nS
nejčastěji	často	k6eAd3
v	v	k7c4
silně	silně	k6eAd1
kyselých	kyselý	k2eAgInPc2d1
<g/>
,	,	kIx,
písčitých	písčitý	k2eAgInPc2d1
<g/>
,	,	kIx,
písčitohlinitých	písčitohlinitý	k2eAgInPc2d1
<g/>
,	,	kIx,
lehkých	lehký	k2eAgInPc2d1
<g/>
,	,	kIx,
drnopodzolových	drnopodzolový	k2eAgFnPc6d1
půdách	půda	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
hořčík	hořčík	k1gInSc1
snadno	snadno	k6eAd1
vyplaven	vyplavit	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Objevuje	objevovat	k5eAaImIp3nS
se	se	k3xPyFc4
i	i	k9
při	při	k7c6
hnojení	hnojení	k1gNnSc6
kyselými	kyselý	k2eAgNnPc7d1
hnojivy	hnojivo	k1gNnPc7
<g/>
,	,	kIx,
při	při	k7c6
nadměrném	nadměrný	k2eAgNnSc6d1
vápnění	vápnění	k1gNnSc6
a	a	k8xC
po	po	k7c6
delších	dlouhý	k2eAgInPc6d2
deštích	dešť	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Příznaky	příznak	k1gInPc1
</s>
<s>
Chlorózy	chloróza	k1gFnPc1
často	často	k6eAd1
připomínají	připomínat	k5eAaImIp3nP
mramorování	mramorování	k1gNnPc1
<g/>
,	,	kIx,
blednutí	blednutí	k1gNnPc1
listů	list	k1gInPc2
od	od	k7c2
okrajů	okraj	k1gInPc2
a	a	k8xC
mezi	mezi	k7c7
cévními	cévní	k2eAgInPc7d1
svazky	svazek	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
ještě	ještě	k9
zůstávají	zůstávat	k5eAaImIp3nP
zelené	zelený	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Listy	lista	k1gFnPc1
bývají	bývat	k5eAaImIp3nP
často	často	k6eAd1
lámavé	lámavý	k2eAgFnPc1d1
a	a	k8xC
křehké	křehký	k2eAgFnPc1d1
<g/>
,	,	kIx,
kořeny	kořen	k1gInPc1
slabě	slabě	k6eAd1
vyvinuté	vyvinutý	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
řepy	řepa	k1gFnSc2
se	se	k3xPyFc4
porucha	porucha	k1gFnSc1
projevuje	projevovat	k5eAaImIp3nS
hnědou	hnědý	k2eAgFnSc7d1
kropenatostí	kropenatost	k1gFnSc7
<g/>
,	,	kIx,
u	u	k7c2
brambor	brambora	k1gFnPc2
čárkami	čárka	k1gFnPc7
připomínajícími	připomínající	k2eAgFnPc7d1
Y-virus	Y-virus	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
L	L	kA
1	#num#	k4
<g/>
]	]	kIx)
Při	při	k7c6
nedostatku	nedostatek	k1gInSc6
Mg	mg	kA
je	být	k5eAaImIp3nS
ochuzena	ochudit	k5eAaPmNgFnS
nejdříve	dříve	k6eAd3
řada	řada	k1gFnSc1
biologicky	biologicky	k6eAd1
významných	významný	k2eAgFnPc2d1
soustav	soustava	k1gFnPc2
než	než	k8xS
dojde	dojít	k5eAaPmIp3nS
na	na	k7c4
změny	změna	k1gFnPc4
u	u	k7c2
chlorofyl	chlorofyl	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Projevům	projev	k1gInPc3
chlorózy	chloróza	k1gFnSc2
tak	tak	k6eAd1
předchází	předcházet	k5eAaImIp3nS
hluboký	hluboký	k2eAgInSc1d1
metabolický	metabolický	k2eAgInSc1d1
rozvrat	rozvrat	k1gInSc1
často	často	k6eAd1
doprovázený	doprovázený	k2eAgInSc1d1
nekrózami	nekróza	k1gFnPc7
pletiv	pletivo	k1gNnPc2
(	(	kIx(
<g/>
korálková	korálkový	k2eAgFnSc1d1
mozaika	mozaika	k1gFnSc1
nebo	nebo	k8xC
pruhovitost	pruhovitost	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
U	u	k7c2
jehličin	jehličina	k1gFnPc2
se	se	k3xPyFc4
projevuje	projevovat	k5eAaImIp3nS
zasycháním	zasychání	k1gNnSc7
špiček	špička	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Lze	lze	k6eAd1
rozlišit	rozlišit	k5eAaPmF
tři	tři	k4xCgInPc4
druhy	druh	k1gInPc4
chloróz	chloróza	k1gFnPc2
z	z	k7c2
nedostatku	nedostatek	k1gInSc2
hořčíku	hořčík	k1gInSc2
<g/>
:	:	kIx,
</s>
<s>
chloróza	chloróza	k1gFnSc1
začíná	začínat	k5eAaImIp3nS
od	od	k7c2
špiček	špička	k1gFnPc2
listů	list	k1gInPc2
a	a	k8xC
od	od	k7c2
okrajů	okraj	k1gInPc2
pokračuje	pokračovat	k5eAaImIp3nS
ke	k	k7c3
středu	střed	k1gInSc3
listů	list	k1gInPc2
(	(	kIx(
<g/>
tabák	tabák	k1gInSc1
<g/>
,	,	kIx,
celer	celer	k1gInSc1
<g/>
,	,	kIx,
hrušeň	hrušeň	k1gFnSc1
<g/>
,	,	kIx,
jabloň	jabloň	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
chloróza	chloróza	k1gFnSc1
vzniká	vznikat	k5eAaImIp3nS
mezi	mezi	k7c4
nervy	nerv	k1gInPc4
v	v	k7c6
blízkosti	blízkost	k1gFnSc6
středního	střední	k2eAgNnSc2d1
žebra	žebro	k1gNnSc2
a	a	k8xC
odtud	odtud	k6eAd1
se	se	k3xPyFc4
rozšiřuje	rozšiřovat	k5eAaImIp3nS
směrem	směr	k1gInSc7
k	k	k7c3
okrajům	okraj	k1gInPc3
(	(	kIx(
<g/>
jabloň	jabloň	k1gFnSc1
<g/>
,	,	kIx,
brambory	brambor	k1gInPc1
<g/>
,	,	kIx,
rajčata	rajče	k1gNnPc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
</s>
<s>
u	u	k7c2
trav	tráva	k1gFnPc2
probíhají	probíhat	k5eAaImIp3nP
chlorózy	chloróza	k1gFnPc1
v	v	k7c6
důsledku	důsledek	k1gInSc6
vedení	vedení	k1gNnSc2
cévních	cévní	k2eAgMnPc2d1
svazků	svazek	k1gInPc2
listy	list	k1gInPc4
pruhovitě	pruhovitě	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obiloviny	obilovina	k1gFnPc1
vykazují	vykazovat	k5eAaImIp3nP
tygrovité	tygrovitý	k2eAgNnSc4d1
zbarvení	zbarvení	k1gNnSc4
čepele	čepel	k1gInPc1
listové	listový	k2eAgInPc1d1
jako	jako	k8xC,k8xS
důsledek	důsledek	k1gInSc1
nahromadění	nahromadění	k1gNnSc2
chlorofylu	chlorofyl	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yRgNnPc4,k3yIgNnPc4,k3yQgNnPc4
předcházejí	předcházet	k5eAaImIp3nP
chloróze	chloróza	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nedostatek	nedostatek	k1gInSc1
hořčíku	hořčík	k1gInSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
zaměněn	zaměnit	k5eAaPmNgInS
s	s	k7c7
nedostatkem	nedostatek	k1gInSc7
zinku	zinek	k1gInSc2
nebo	nebo	k8xC
nedostatkem	nedostatek	k1gInSc7
chlóru	chlór	k1gInSc2
<g/>
,	,	kIx,
virovými	virový	k2eAgFnPc7d1
chorobami	choroba	k1gFnPc7
<g/>
,	,	kIx,
nebo	nebo	k8xC
přirozeným	přirozený	k2eAgNnSc7d1
stárnutím	stárnutí	k1gNnSc7
rostlin	rostlina	k1gFnPc2
<g/>
,	,	kIx,
protože	protože	k8xS
mají	mít	k5eAaImIp3nP
podobné	podobný	k2eAgInPc4d1
příznaky	příznak	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Ochrana	ochrana	k1gFnSc1
rostlin	rostlina	k1gFnPc2
</s>
<s>
Prevence	prevence	k1gFnSc1
</s>
<s>
Používání	používání	k1gNnSc1
hnojiv	hnojivo	k1gNnPc2
s	s	k7c7
obsahem	obsah	k1gInSc7
hořčíku	hořčík	k1gInSc2
<g/>
,	,	kIx,
hnojení	hnojení	k1gNnSc1
do	do	k7c2
zásoby	zásoba	k1gFnSc2
dolomitickým	dolomitický	k2eAgInSc7d1
vápencem	vápenec	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
nadměrných	nadměrný	k2eAgFnPc6d1
srážkách	srážka	k1gFnPc6
poskytovat	poskytovat	k5eAaImF
rostlinám	rostlina	k1gFnPc3
dostatečné	dostatečná	k1gFnSc2
množství	množství	k1gNnSc1
živin	živina	k1gFnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
hořčíku	hořčík	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Ošetření	ošetření	k1gNnSc1
při	při	k7c6
výskytu	výskyt	k1gInSc6
</s>
<s>
Použití	použití	k1gNnSc1
roztoku	roztok	k1gInSc2
hořečnatých	hořečnatý	k2eAgFnPc2d1
solí	sůl	k1gFnPc2
<g/>
,	,	kIx,
nejlépe	dobře	k6eAd3
v	v	k7c6
aplikaci	aplikace	k1gFnSc6
jako	jako	k9
hnojení	hnojení	k1gNnSc1
na	na	k7c4
list	list	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
význam	význam	k1gInSc1
hořčíku	hořčík	k1gInSc2
<g/>
,	,	kIx,
mendelu	mendel	k1gInSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
web	web	k1gInSc1
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
mendelu	mendel	k1gInSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
1	#num#	k4
2	#num#	k4
BARTOŠ	Bartoš	k1gMnSc1
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ochrana	ochrana	k1gFnSc1
rostlin	rostlina	k1gFnPc2
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
SZN	SZN	kA
<g/>
,	,	kIx,
1968	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
fyziologické	fyziologický	k2eAgFnPc1d1
poruchy	porucha	k1gFnPc1
jikl	jikla	k1gFnPc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
<g/>
Deficit	deficit	k1gInSc1
hořčíku	hořčík	k1gInSc2
a	a	k8xC
ochrana	ochrana	k1gFnSc1
na	na	k7c4
gardenersworld	gardenersworld	k1gInSc4
<g/>
.	.	kIx.
<g/>
com	com	k?
</s>
