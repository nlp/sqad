<s>
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
<g/>
,	,	kIx,
a.s.	a.s.	k?
Název	název	k1gInSc1
</s>
<s>
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
<g/>
,	,	kIx,
akciová	akciový	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
Přezdívka	přezdívka	k1gFnSc1
</s>
<s>
„	„	k?
<g/>
Chachaři	chachar	k1gMnPc1
<g/>
“	“	k?
<g/>
,	,	kIx,
„	„	k?
<g/>
Baníček	Baníček	k1gInSc1
<g/>
“	“	k?
Země	zem	k1gFnSc2
</s>
<s>
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
Město	město	k1gNnSc1
</s>
<s>
Ostrava	Ostrava	k1gFnSc1
Založen	založen	k2eAgInSc1d1
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1922	#num#	k4
Asociace	asociace	k1gFnSc2
</s>
<s>
FAČR	FAČR	kA
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_mlada	_mlada	k1gFnSc1
<g/>
1617	#num#	k4
<g/>
h	h	k?
<g/>
|	|	kIx~
<g/>
link	link	k1gMnSc1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Domácí	domácí	k2eAgInSc1d1
dres	dres	k1gInSc1
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
_whiteborder	_whiteborder	k1gMnSc1
<g/>
|	|	kIx~
<g/>
link	link	k1gMnSc1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Venkovní	venkovní	k2eAgInSc4d1
dres	dres	k1gInSc4
</s>
<s>
[[	[[	k?
<g/>
Soubor	soubor	k1gInSc1
<g/>
:	:	kIx,
<g/>
Kit	kit	k1gInSc1
left	left	k1gInSc1
arm	arm	k?
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
top	topit	k5eAaImRp2nS
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
|	|	kIx~
<g/>
link	link	k6eAd1
<g/>
=	=	kIx~
<g/>
|	|	kIx~
<g/>
alt	alt	k1gInSc1
<g/>
=	=	kIx~
<g/>
]]	]]	k?
</s>
<s>
Alternativní	alternativní	k2eAgInPc1d1
</s>
<s>
Soutěž	soutěž	k1gFnSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
česká	český	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc1
Stadion	stadion	k1gNnSc1
</s>
<s>
Městský	městský	k2eAgInSc1d1
stadion	stadion	k1gInSc1
v	v	k7c6
Ostravě-Vítkovicích	Ostravě-Vítkovice	k1gFnPc6
<g/>
,	,	kIx,
Ostrava	Ostrava	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
49	#num#	k4
<g/>
°	°	k?
<g/>
50	#num#	k4
<g/>
′	′	k?
<g/>
42	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
18	#num#	k4
<g/>
°	°	k?
<g/>
17	#num#	k4
<g/>
′	′	k?
<g/>
58	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Kapacita	kapacita	k1gFnSc1
</s>
<s>
15	#num#	k4
123	#num#	k4
Vedení	vedení	k1gNnPc2
Vlastník	vlastník	k1gMnSc1
</s>
<s>
Václav	Václav	k1gMnSc1
Brabec	Brabec	k1gMnSc1
Trenér	trenér	k1gMnSc1
</s>
<s>
Ondřej	Ondřej	k1gMnSc1
Smetana	Smetana	k1gMnSc1
</s>
<s>
Oficiální	oficiální	k2eAgFnSc1d1
webová	webový	k2eAgFnSc1d1
stránka	stránka	k1gFnSc1
Největší	veliký	k2eAgInPc1d3
úspěchy	úspěch	k1gInPc1
Ligové	ligový	k2eAgInPc1d1
tituly	titul	k1gInPc1
</s>
<s>
3	#num#	k4
<g/>
×	×	k?
mistr	mistr	k1gMnSc1
Československa	Československo	k1gNnSc2
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
<g/>
,	,	kIx,
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
<g/>
,	,	kIx,
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
<g/>
)	)	kIx)
1	#num#	k4
<g/>
×	×	k?
mistr	mistr	k1gMnSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
(	(	kIx(
<g/>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
)	)	kIx)
Domácí	domácí	k2eAgFnSc2d1
trofeje	trofej	k1gFnSc2
</s>
<s>
5	#num#	k4
<g/>
×	×	k?
Český	český	k2eAgInSc1d1
pohár	pohár	k1gInSc1
Mezinárodní	mezinárodní	k2eAgFnSc2d1
trofeje	trofej	k1gFnSc2
</s>
<s>
1	#num#	k4
<g/>
×	×	k?
Středoevropský	středoevropský	k2eAgInSc1d1
pohár	pohár	k1gInSc1
</s>
<s>
Fortuna	Fortuna	k1gFnSc1
<g/>
:	:	kIx,
<g/>
Liga	liga	k1gFnSc1
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
</s>
<s>
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
<g/>
,	,	kIx,
a.s.	a.s.	k?
(	(	kIx(
<g/>
zkráceně	zkráceně	k6eAd1
FCB	FCB	kA
<g/>
,	,	kIx,
celým	celý	k2eAgInSc7d1
názvem	název	k1gInSc7
<g/>
:	:	kIx,
Football	Football	k1gInSc1
Club	club	k1gInSc1
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
<g/>
,	,	kIx,
a.s.	a.s.	k?
<g/>
))	))	k?
je	být	k5eAaImIp3nS
český	český	k2eAgInSc1d1
profesionální	profesionální	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
sídlí	sídlet	k5eAaImIp3nS
v	v	k7c6
Ostravě	Ostrava	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Založen	založen	k2eAgMnSc1d1
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
1922	#num#	k4
pod	pod	k7c7
názvem	název	k1gInSc7
SK	Sk	kA
Slezská	slezský	k2eAgFnSc1d1
Ostrava	Ostrava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Baník	Baník	k1gInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
klubovém	klubový	k2eAgInSc6d1
názvu	název	k1gInSc6
nepřetržitě	přetržitě	k6eNd1
obsažen	obsáhnout	k5eAaPmNgInS
od	od	k7c2
roku	rok	k1gInSc2
1952	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Klubovými	klubový	k2eAgFnPc7d1
barvami	barva	k1gFnPc7
jsou	být	k5eAaImIp3nP
modrá	modrý	k2eAgFnSc1d1
a	a	k8xC
bílá	bílý	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Největšími	veliký	k2eAgMnPc7d3
rivaly	rival	k1gMnPc7
Baníku	Baník	k1gInSc2
jsou	být	k5eAaImIp3nP
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
a	a	k8xC
Slezský	slezský	k2eAgInSc1d1
FC	FC	kA
Opava	Opava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Své	svůj	k3xOyFgInPc4
domácí	domácí	k2eAgInPc4d1
zápasy	zápas	k1gInPc4
odehrává	odehrávat	k5eAaImIp3nS
na	na	k7c6
Městském	městský	k2eAgInSc6d1
stadionu	stadion	k1gInSc6
v	v	k7c6
Ostravě-Vítkovicích	Ostravě-Vítkovice	k1gFnPc6
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
má	mít	k5eAaImIp3nS
kapacitu	kapacita	k1gFnSc4
15	#num#	k4
123	#num#	k4
diváků	divák	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historické	historický	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1922	#num#	k4
–	–	k?
SK	Sk	kA
Slezská	slezský	k2eAgFnSc1d1
Ostrava	Ostrava	k1gFnSc1
(	(	kIx(
<g/>
Sportovní	sportovní	k2eAgInSc1d1
klub	klub	k1gInSc1
Slezská	slezský	k2eAgFnSc1d1
Ostrava	Ostrava	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1945	#num#	k4
–	–	k?
SK	Sk	kA
Ostrava	Ostrava	k1gFnSc1
(	(	kIx(
<g/>
Sportovní	sportovní	k2eAgInSc1d1
klub	klub	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1948	#num#	k4
–	–	k?
JTO	JTO	kA
Sokol	Sokol	k1gMnSc1
Trojice	trojice	k1gFnSc2
Ostrava	Ostrava	k1gFnSc1
(	(	kIx(
<g/>
Jednotná	jednotný	k2eAgFnSc1d1
tělovýchovná	tělovýchovný	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
Sokol	Sokol	k1gInSc1
Trojice	trojice	k1gFnSc1
Ostrava	Ostrava	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1951	#num#	k4
–	–	k?
JTO	JTO	kA
Sokol	Sokol	k1gMnSc1
OKD	OKD	kA
Ostrava	Ostrava	k1gFnSc1
(	(	kIx(
<g/>
Jednotná	jednotný	k2eAgFnSc1d1
tělovýchovná	tělovýchovný	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
Sokol	Sokol	k1gMnSc1
Ostravsko-karvinské	ostravsko-karvinský	k2eAgInPc4d1
doly	dol	k1gInPc4
Ostrava	Ostrava	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1952	#num#	k4
–	–	k?
DSO	DSO	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
(	(	kIx(
<g/>
Dobrovolná	dobrovolný	k2eAgFnSc1d1
sportovní	sportovní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1961	#num#	k4
–	–	k?
TJ	tj	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
(	(	kIx(
<g/>
Tělovýchovná	tělovýchovný	k2eAgFnSc1d1
jednota	jednota	k1gFnSc1
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1970	#num#	k4
–	–	k?
TJ	tj	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
OKD	OKD	kA
(	(	kIx(
<g/>
Tělovýchovná	tělovýchovný	k2eAgFnSc1d1
jednota	jednota	k1gFnSc1
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
Ostravsko-karvinské	ostravsko-karvinský	k2eAgInPc1d1
doly	dol	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
1990	#num#	k4
–	–	k?
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
(	(	kIx(
<g/>
Football	Football	k1gInSc1
Club	club	k1gInSc1
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
<g/>
,	,	kIx,
a.s.	a.s.	k?
<g/>
)	)	kIx)
</s>
<s>
1994	#num#	k4
–	–	k?
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
Tango	tango	k1gNnSc4
(	(	kIx(
<g/>
Football	Football	k1gInSc1
Club	club	k1gInSc1
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
Tango	tango	k1gNnSc1
<g/>
,	,	kIx,
a.s.	a.s.	k?
<g/>
)	)	kIx)
</s>
<s>
1995	#num#	k4
–	–	k?
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
(	(	kIx(
<g/>
Football	Football	k1gInSc1
Club	club	k1gInSc1
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
<g/>
,	,	kIx,
a.s.	a.s.	k?
<g/>
)	)	kIx)
</s>
<s>
2003	#num#	k4
–	–	k?
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
Ispat	Ispat	k1gInSc1
(	(	kIx(
<g/>
Football	Football	k1gInSc1
Club	club	k1gInSc1
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
Ispat	Ispat	k1gInSc1
<g/>
,	,	kIx,
a.s.	a.s.	k?
<g/>
)	)	kIx)
</s>
<s>
2005	#num#	k4
–	–	k?
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
(	(	kIx(
<g/>
Football	Football	k1gInSc1
Club	club	k1gInSc1
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
<g/>
,	,	kIx,
akciová	akciový	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Vznik	vznik	k1gInSc1
klubu	klub	k1gInSc2
a	a	k8xC
historie	historie	k1gFnSc2
od	od	k7c2
roku	rok	k1gInSc2
1922	#num#	k4
do	do	k7c2
1937	#num#	k4
</s>
<s>
Klub	klub	k1gInSc1
vznikl	vzniknout	k5eAaPmAgInS
8	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
1922	#num#	k4
jako	jako	k8xC,k8xS
SK	Sk	kA
Slezská	Slezská	k1gFnSc1
Ostrava	Ostrava	k1gFnSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
20	#num#	k4
signatářů	signatář	k1gMnPc2
podepsalo	podepsat	k5eAaPmAgNnS
prezenční	prezenční	k2eAgFnSc4d1
listinu	listina	k1gFnSc4
v	v	k7c6
restauraci	restaurace	k1gFnSc6
U	u	k7c2
Dubu	dub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Signatáři	signatář	k1gMnPc7
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
podepsali	podepsat	k5eAaPmAgMnP
prezenční	prezenční	k2eAgFnSc4d1
listinu	listina	k1gFnSc4
<g/>
,	,	kIx,
však	však	k9
byli	být	k5eAaImAgMnP
nemajetní	majetný	k2eNgMnPc1d1
horníci	horník	k1gMnPc1
bydlící	bydlící	k2eAgMnPc1d1
v	v	k7c6
kolonii	kolonie	k1gFnSc6
Na	na	k7c6
kamenci	kamenec	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Organizátory	organizátor	k1gInPc4
byli	být	k5eAaImAgMnP
Karel	Karel	k1gMnSc1
Aniol	Aniol	k1gInSc4
<g/>
,	,	kIx,
Arnošt	Arnošt	k1gMnSc1
Haberkiewicz	Haberkiewicz	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
Křižák	křižák	k1gMnSc1
<g/>
,	,	kIx,
František	František	k1gMnSc1
Mruzek	Mruzek	k1gInSc1
a	a	k8xC
Jaroslav	Jaroslav	k1gMnSc1
Horák	Horák	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tým	tým	k1gInSc1
byl	být	k5eAaImAgInS
oficiálně	oficiálně	k6eAd1
registrovaný	registrovaný	k2eAgInSc1d1
14	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
1922	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prvním	první	k4xOgMnSc7
předsedou	předseda	k1gMnSc7
klubu	klub	k1gInSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Karel	Karel	k1gMnSc1
Aniol	Aniola	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Své	svůj	k3xOyFgInPc4
první	první	k4xOgInPc4
zápasy	zápas	k1gInPc4
hrávala	hrávat	k5eAaImAgFnS
Slezská	slezský	k2eAgFnSc1d1
v	v	k7c4
červeno-bílo	červeno-bílo	k1gNnSc4
pruhovaných	pruhovaný	k2eAgFnPc2d1
dresech	dres	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
dubna	duben	k1gInSc2
1923	#num#	k4
hrál	hrát	k5eAaImAgInS
s	s	k7c7
bílými	bílý	k2eAgInPc7d1
dresy	dres	k1gInPc7
a	a	k8xC
modrými	modrý	k2eAgFnPc7d1
trenýrkami	trenýrky	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc1
tradiční	tradiční	k2eAgFnPc1d1
barvy	barva	k1gFnPc1
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
prvnímu	první	k4xOgInSc3
zápasu	zápas	k1gInSc3
došlo	dojít	k5eAaPmAgNnS
4	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1923	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
SK	Sk	kA
Slezská	Slezská	k1gFnSc1
nastoupila	nastoupit	k5eAaPmAgFnS
proti	proti	k7c3
rezervě	rezerva	k1gFnSc3
tehdy	tehdy	k6eAd1
silného	silný	k2eAgInSc2d1
Slovanu	Slovan	k1gInSc2
Ostrava	Ostrava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
SK	Sk	kA
Slezská	Slezská	k1gFnSc1
Ostrava	Ostrava	k1gFnSc1
byl	být	k5eAaImAgInS
tehdy	tehdy	k6eAd1
chudý	chudý	k2eAgInSc1d1
tým	tým	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
neměl	mít	k5eNaImAgInS
ani	ani	k9
své	svůj	k3xOyFgNnSc4
hřiště	hřiště	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
si	se	k3xPyFc3
obvykle	obvykle	k6eAd1
půjčoval	půjčovat	k5eAaImAgMnS
od	od	k7c2
svých	svůj	k3xOyFgMnPc2
městských	městský	k2eAgMnPc2d1
rivalů	rival	k1gMnPc2
za	za	k7c4
stokorunu	stokoruna	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Své	svůj	k3xOyFgNnSc4
vlastní	vlastní	k2eAgNnSc4d1
hřiště	hřiště	k1gNnSc4
si	se	k3xPyFc3
otevřel	otevřít	k5eAaPmAgMnS
na	na	k7c4
podzim	podzim	k1gInSc4
roku	rok	k1gInSc2
1925	#num#	k4
Na	na	k7c6
kamenci	kamenec	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hřiště	hřiště	k1gNnSc1
bylo	být	k5eAaImAgNnS
však	však	k9
kamenité	kamenitý	k2eAgNnSc1d1
a	a	k8xC
sotva	sotva	k6eAd1
regulérní	regulérní	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1934	#num#	k4
se	se	k3xPyFc4
klubu	klub	k1gInSc6
podařilo	podařit	k5eAaPmAgNnS
získat	získat	k5eAaPmF
pozemky	pozemek	k1gInPc4
ve	v	k7c6
Staré	Staré	k2eAgFnSc6d1
střelnici	střelnice	k1gFnSc6
od	od	k7c2
bohatého	bohatý	k2eAgMnSc2d1
průmyslníka	průmyslník	k1gMnSc2
Wilczka	Wilczka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
léta	léto	k1gNnSc2
tam	tam	k6eAd1
bylo	být	k5eAaImAgNnS
postaveno	postavit	k5eAaPmNgNnS
nové	nový	k2eAgNnSc1d1
hřiště	hřiště	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnozí	mnohý	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
zde	zde	k6eAd1
chodili	chodit	k5eAaImAgMnP
bez	bez	k7c2
nároku	nárok	k1gInSc2
na	na	k7c4
odměnu	odměna	k1gFnSc4
pracovat	pracovat	k5eAaImF
na	na	k7c4
stavbu	stavba	k1gFnSc4
přímo	přímo	k6eAd1
ze	z	k7c2
směny	směna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
SK	Sk	kA
Slezská	slezský	k2eAgFnSc1d1
Ostrava	Ostrava	k1gFnSc1
začala	začít	k5eAaPmAgFnS
hrát	hrát	k5eAaImF
roku	rok	k1gInSc2
1923	#num#	k4
ve	v	k7c6
III	III	kA
<g/>
.	.	kIx.
třídě	třída	k1gFnSc3
župy	župa	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
byla	být	k5eAaImAgFnS
nejnižší	nízký	k2eAgFnSc4d3
soutěž	soutěž	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
postoupil	postoupit	k5eAaPmAgInS
do	do	k7c2
divize	divize	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trvalo	trvat	k5eAaImAgNnS
to	ten	k3xDgNnSc1
však	však	k9
ještě	ještě	k9
nějakou	nějaký	k3yIgFnSc4
dobu	doba	k1gFnSc4
než	než	k8xS
se	se	k3xPyFc4
tým	tým	k1gInSc1
dostal	dostat	k5eAaPmAgInS
do	do	k7c2
nejvyšší	vysoký	k2eAgFnSc2d3
soutěže	soutěž	k1gFnSc2
v	v	k7c6
Československu	Československo	k1gNnSc6
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1934	#num#	k4
tým	tým	k1gInSc1
postoupil	postoupit	k5eAaPmAgInS
do	do	k7c2
moravskoslezské	moravskoslezský	k2eAgFnSc2d1
divize	divize	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ze	z	k7c2
Slezské	Slezská	k1gFnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
populární	populární	k2eAgInSc1d1
tým	tým	k1gInSc1
a	a	k8xC
zvedl	zvednout	k5eAaPmAgMnS
se	se	k3xPyFc4
o	o	k7c4
něj	on	k3xPp3gInSc4
veřejný	veřejný	k2eAgInSc4d1
zájem	zájem	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Derby	derby	k1gNnPc1
proti	proti	k7c3
Slovanu	Slovan	k1gMnSc6
Ostrava	Ostrava	k1gFnSc1
sledovalo	sledovat	k5eAaImAgNnS
na	na	k7c6
Staré	Staré	k2eAgFnSc6d1
střelnici	střelnice	k1gFnSc6
5	#num#	k4
400	#num#	k4
diváků	divák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1934	#num#	k4
se	se	k3xPyFc4
klub	klub	k1gInSc1
začal	začít	k5eAaPmAgInS
věnovat	věnovat	k5eAaImF,k5eAaPmF
placení	placení	k1gNnSc4
hráčů	hráč	k1gMnPc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
ve	v	k7c6
stejném	stejný	k2eAgInSc6d1
roce	rok	k1gInSc6
povolil	povolit	k5eAaPmAgInS
Československý	československý	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
svaz	svaz	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
divizi	divize	k1gFnSc6
se	se	k3xPyFc4
tým	tým	k1gInSc1
držel	držet	k5eAaImAgInS
ve	v	k7c6
středu	střed	k1gInSc6
tabulky	tabulka	k1gFnSc2
a	a	k8xC
čelil	čelit	k5eAaImAgInS
týmům	tým	k1gInPc3
jako	jako	k9
byli	být	k5eAaImAgMnP
např.	např.	kA
SK	Sk	kA
Baťa	Baťa	k1gMnSc1
Zlín	Zlín	k1gInSc1
a	a	k8xC
Polonia	polonium	k1gNnSc2
Karviná	Karviná	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
sezóně	sezóna	k1gFnSc6
1936-1937	1936-1937	k4
Slezská	Slezská	k1gFnSc1
divizi	divize	k1gFnSc4
vyhrála	vyhrát	k5eAaPmAgFnS
a	a	k8xC
postoupila	postoupit	k5eAaPmAgFnS
do	do	k7c2
kvalifikačního	kvalifikační	k2eAgInSc2d1
turnaje	turnaj	k1gInSc2
o	o	k7c4
první	první	k4xOgFnSc4
ligu	liga	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
SK	Sk	kA
Slezská	Slezská	k1gFnSc1
čelila	čelit	k5eAaImAgFnS
několika	několik	k4yIc3
známým	známý	k2eAgInPc3d1
týmům	tým	k1gInPc3
jako	jako	k8xC,k8xS
třeba	třeba	k9
DFC	DFC	kA
Prag	Prag	k1gInSc1
nebo	nebo	k8xC
Čechie	Čechie	k1gFnSc1
Karlín	Karlín	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvalifikaci	kvalifikace	k1gFnSc6
se	se	k3xPyFc4
ji	on	k3xPp3gFnSc4
podařilo	podařit	k5eAaPmAgNnS
vyhrát	vyhrát	k5eAaPmF
a	a	k8xC
poprvé	poprvé	k6eAd1
v	v	k7c6
historii	historie	k1gFnSc6
postoupila	postoupit	k5eAaPmAgFnS
do	do	k7c2
první	první	k4xOgFnSc2
ligy	liga	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Období	období	k1gNnSc1
od	od	k7c2
roku	rok	k1gInSc2
1937	#num#	k4
do	do	k7c2
1952	#num#	k4
</s>
<s>
V	v	k7c6
té	ten	k3xDgFnSc6
době	doba	k1gFnSc6
hrály	hrát	k5eAaImAgFnP
v	v	k7c6
1	#num#	k4
<g/>
.	.	kIx.
československé	československý	k2eAgFnSc6d1
lize	liga	k1gFnSc6
týmy	tým	k1gInPc1
převážně	převážně	k6eAd1
z	z	k7c2
Prahy	Praha	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
byly	být	k5eAaImAgFnP
pokročilé	pokročilý	k2eAgFnPc1d1
ve	v	k7c6
všech	všecek	k3xTgInPc6
směrech	směr	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postup	postup	k1gInSc4
do	do	k7c2
nejvyšší	vysoký	k2eAgFnSc2d3
československé	československý	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
byl	být	k5eAaImAgInS
pro	pro	k7c4
SK	Sk	kA
Slezská	slezský	k2eAgFnSc1d1
obrovský	obrovský	k2eAgInSc4d1
úspěch	úspěch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
své	svůj	k3xOyFgFnSc2
patnáctileté	patnáctiletý	k2eAgFnSc2d1
historie	historie	k1gFnSc2
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
podařilo	podařit	k5eAaPmAgNnS
postoupit	postoupit	k5eAaPmF
z	z	k7c2
nejnižší	nízký	k2eAgFnSc2d3
soutěže	soutěž	k1gFnSc2
až	až	k9
do	do	k7c2
nejvyšší	vysoký	k2eAgFnSc2d3
ligy	liga	k1gFnSc2
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc4
ligový	ligový	k2eAgInSc4d1
zápas	zápas	k1gInSc4
se	se	k3xPyFc4
hrál	hrát	k5eAaImAgMnS
na	na	k7c6
Staré	Staré	k2eAgFnSc6d1
střelnici	střelnice	k1gFnSc6
22	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1937	#num#	k4
proti	proti	k7c3
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČsŠK	ČsŠK	k1gFnSc1
Bratislava	Bratislava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
druhém	druhý	k4xOgInSc6
zápase	zápas	k1gInSc6
se	se	k3xPyFc4
v	v	k7c6
Praze	Praha	k1gFnSc6
střetl	střetnout	k5eAaPmAgMnS
ze	z	k7c2
Spartou	Sparta	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
ve	v	k7c6
Spartě	Sparta	k1gFnSc6
hrála	hrát	k5eAaImAgFnS
většina	většina	k1gFnSc1
reprezentačních	reprezentační	k2eAgMnPc2d1
hráčů	hráč	k1gMnPc2
<g/>
,	,	kIx,
Baník	Baník	k1gInSc1
vyhrál	vyhrát	k5eAaPmAgInS
3	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
byla	být	k5eAaImAgFnS
obrovská	obrovský	k2eAgFnSc1d1
senzace	senzace	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
nejvyšší	vysoký	k2eAgFnSc6d3
soutěži	soutěž	k1gFnSc6
se	se	k3xPyFc4
Slezská	Slezská	k1gFnSc1
udržela	udržet	k5eAaPmAgFnS
3	#num#	k4
roky	rok	k1gInPc4
<g/>
,	,	kIx,
poté	poté	k6eAd1
roku	rok	k1gInSc2
1940	#num#	k4
sestoupila	sestoupit	k5eAaPmAgFnS
do	do	k7c2
divize	divize	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Slezská	slezský	k2eAgFnSc1d1
Ostrava	Ostrava	k1gFnSc1
hrála	hrát	k5eAaImAgFnS
divizi	divize	k1gFnSc4
až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1943	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
opět	opět	k6eAd1
postoupila	postoupit	k5eAaPmAgFnS
do	do	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ligy	liga	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Postupem	postup	k1gInSc7
do	do	k7c2
nejvyšší	vysoký	k2eAgFnSc2d3
soutěže	soutěž	k1gFnSc2
se	se	k3xPyFc4
zvýšil	zvýšit	k5eAaPmAgInS
u	u	k7c2
místních	místní	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
zájem	zájem	k1gInSc4
o	o	k7c4
fotbal	fotbal	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novou	nový	k2eAgFnSc4d1
hymnu	hymna	k1gFnSc4
Slezské	Slezská	k1gFnSc2
složil	složit	k5eAaPmAgMnS
Vladimír	Vladimír	k1gMnSc1
Brázda	Brázda	k1gMnSc1
a	a	k8xC
na	na	k7c4
gramofonovu	gramofonův	k2eAgFnSc4d1
desku	deska	k1gFnSc4
ji	on	k3xPp3gFnSc4
nazpíval	nazpívat	k5eAaBmAgMnS,k5eAaPmAgMnS
Rudolf	Rudolf	k1gMnSc1
Asmus	Asmus	k1gMnSc1
<g/>
,	,	kIx,
později	pozdě	k6eAd2
slavný	slavný	k2eAgMnSc1d1
operní	operní	k2eAgMnSc1d1
pěvec	pěvec	k1gMnSc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
působil	působit	k5eAaImAgMnS
v	v	k7c6
Národním	národní	k2eAgNnSc6d1
divadle	divadlo	k1gNnSc6
a	a	k8xC
léta	léto	k1gNnPc4
byl	být	k5eAaImAgMnS
členem	člen	k1gMnSc7
berlínské	berlínský	k2eAgFnSc2d1
Komické	komický	k2eAgFnSc2d1
opery	opera	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
sezóně	sezóna	k1gFnSc6
1943-1944	1943-1944	k4
se	se	k3xPyFc4
rapidně	rapidně	k6eAd1
zvýšila	zvýšit	k5eAaPmAgFnS
domácí	domácí	k2eAgFnSc1d1
návštěvnost	návštěvnost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zápas	zápas	k1gInSc1
se	se	k3xPyFc4
Slávii	Slávia	k1gFnSc3
Praha	Praha	k1gFnSc1
sledovalo	sledovat	k5eAaImAgNnS
33	#num#	k4
000	#num#	k4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ligový	ligový	k2eAgInSc1d1
ročník	ročník	k1gInSc1
1944-45	1944-45	k4
se	se	k3xPyFc4
neuskutečnil	uskutečnit	k5eNaPmAgInS
<g/>
,	,	kIx,
úřady	úřad	k1gInPc1
jen	jen	k6eAd1
občas	občas	k6eAd1
dovolily	dovolit	k5eAaPmAgFnP
sehrát	sehrát	k5eAaPmF
nějaké	nějaký	k3yIgNnSc4
přátelské	přátelský	k2eAgNnSc4d1
utkání	utkání	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
poválečných	poválečný	k2eAgInPc6d1
měsících	měsíc	k1gInPc6
si	se	k3xPyFc3
starší	starý	k2eAgMnPc1d2
fotbalisté	fotbalista	k1gMnPc1
odbývali	odbývat	k5eAaImAgMnP
vojenskou	vojenský	k2eAgFnSc4d1
službu	služba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1949	#num#	k4
se	se	k3xPyFc4
klub	klub	k1gInSc1
vrátil	vrátit	k5eAaPmAgInS
do	do	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ligy	liga	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
změnil	změnit	k5eAaPmAgInS
své	svůj	k3xOyFgNnSc4
jméno	jméno	k1gNnSc4
v	v	k7c6
roce	rok	k1gInSc6
1947	#num#	k4
na	na	k7c4
Sokol	Sokol	k1gInSc4
Trojice	trojice	k1gFnSc1
Slezská	Slezská	k1gFnSc1
a	a	k8xC
znovu	znovu	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1948	#num#	k4
na	na	k7c6
OKD	OKD	kA
Ostrava	Ostrava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	nový	k2eAgInSc1d1
stadion	stadion	k1gInSc1
Bazaly	Bazala	k1gFnSc2
byl	být	k5eAaImAgInS
otevřen	otevřít	k5eAaPmNgInS
19	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
1959	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Období	období	k1gNnSc1
od	od	k7c2
roku	rok	k1gInSc2
1952	#num#	k4
po	po	k7c4
rok	rok	k1gInSc4
1967	#num#	k4
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1952	#num#	k4
klub	klub	k1gInSc1
přijal	přijmout	k5eAaPmAgInS
název	název	k1gInSc1
DSO	DSO	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
se	se	k3xPyFc4
název	název	k1gInSc1
příliš	příliš	k6eAd1
nezměnil	změnit	k5eNaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
sezóně	sezóna	k1gFnSc6
1954	#num#	k4
dosáhl	dosáhnout	k5eAaPmAgMnS
tehdy	tehdy	k6eAd1
svého	svůj	k3xOyFgInSc2
nejlepšího	dobrý	k2eAgInSc2d3
úspěchu	úspěch	k1gInSc2
<g/>
,	,	kIx,
skončil	skončit	k5eAaPmAgInS
druhý	druhý	k4xOgInSc4
hned	hned	k6eAd1
za	za	k7c7
Spartou	Sparta	k1gFnSc7
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
<g/>
V	v	k7c6
sezóně	sezóna	k1gFnSc6
1959	#num#	k4
hrál	hrát	k5eAaImAgMnS
Baník	baník	k1gMnSc1
naposledy	naposledy	k6eAd1
na	na	k7c6
Staré	Staré	k2eAgFnSc6d1
střelnici	střelnice	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stará	starý	k2eAgFnSc1d1
střelnice	střelnice	k1gFnSc1
přestala	přestat	k5eAaPmAgFnS
splňovat	splňovat	k5eAaImF
podmínky	podmínka	k1gFnPc4
fotbalového	fotbalový	k2eAgInSc2d1
svazu	svaz	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hřiště	hřiště	k1gNnSc1
nebylo	být	k5eNaImAgNnS
travnaté	travnatý	k2eAgNnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
škvárové	škvárový	k2eAgNnSc1d1
a	a	k8xC
to	ten	k3xDgNnSc1
byl	být	k5eAaImAgInS
také	také	k9
důvod	důvod	k1gInSc1
k	k	k7c3
jeho	jeho	k3xOp3gNnSc3
uzavření	uzavření	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
sezóně	sezóna	k1gFnSc6
1965-1966	1965-1966	k4
byl	být	k5eAaImAgInS
Baník	Baník	k1gInSc1
oslaben	oslabit	k5eAaPmNgInS
po	po	k7c6
generační	generační	k2eAgFnSc6d1
výměně	výměna	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Skončil	skončit	k5eAaPmAgMnS
13	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
sestoupil	sestoupit	k5eAaPmAgMnS
do	do	k7c2
druhé	druhý	k4xOgFnSc2
ligy	liga	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c4
rok	rok	k1gInSc4
později	pozdě	k6eAd2
Baník	Baník	k1gInSc1
opět	opět	k6eAd1
postoupil	postoupit	k5eAaPmAgInS
do	do	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ligy	liga	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1
éra	éra	k1gFnSc1
(	(	kIx(
<g/>
70	#num#	k4
<g/>
.	.	kIx.
až	až	k9
80	#num#	k4
<g/>
.	.	kIx.
léta	léto	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
sezóně	sezóna	k1gFnSc6
1972-1973	1972-1973	k4
Baník	baník	k1gMnSc1
vyhrál	vyhrát	k5eAaPmAgMnS
Československý	československý	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
vítěz	vítěz	k1gMnSc1
domácího	domácí	k2eAgInSc2d1
poháru	pohár	k1gInSc2
postoupil	postoupit	k5eAaPmAgInS
do	do	k7c2
Poháru	pohár	k1gInSc2
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
první	první	k4xOgNnSc4
ostravské	ostravský	k2eAgNnSc4d1
vystoupení	vystoupení	k1gNnSc4
v	v	k7c6
evropských	evropský	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tam	tam	k6eAd1
Baník	Baník	k1gInSc1
porazil	porazit	k5eAaPmAgInS
v	v	k7c6
prvním	první	k4xOgNnSc6
kole	kolo	k1gNnSc6
irský	irský	k2eAgInSc1d1
Cork	Cork	k1gInSc1
Hibernians	Hiberniansa	k1gFnPc2
FC	FC	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
druhém	druhý	k4xOgInSc6
kole	kolo	k1gNnSc6
byl	být	k5eAaImAgMnS
ale	ale	k9
vyřazen	vyřadit	k5eAaPmNgMnS
německým	německý	k2eAgMnPc3d1
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Magdeburg	Magdeburg	k1gInSc1
<g/>
,	,	kIx,
pozdějším	pozdní	k2eAgMnSc7d2
vítězem	vítěz	k1gMnSc7
poháru	pohár	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
sezóně	sezóna	k1gFnSc6
1974-1975	1974-1975	k4
Postoupil	postoupit	k5eAaPmAgMnS
do	do	k7c2
Poháru	pohár	k1gInSc2
UEFA	UEFA	kA
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
probojoval	probojovat	k5eAaPmAgMnS
přes	přes	k7c4
španělský	španělský	k2eAgInSc4d1
Real	Real	k1gInSc4
Sociedad	Sociedad	k1gInSc1
San	San	k1gFnSc1
Sebastian	Sebastian	k1gMnSc1
<g/>
,	,	kIx,
francouzské	francouzský	k2eAgInPc4d1
FC	FC	kA
Nantes	Nantesa	k1gFnPc2
a	a	k8xC
italské	italský	k2eAgFnSc2d1
SSC	SSC	kA
Napoli	Napole	k1gFnSc4
až	až	k8xS
do	do	k7c2
čtvrtfinále	čtvrtfinále	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
byl	být	k5eAaImAgMnS
vyřazen	vyřadit	k5eAaPmNgMnS
německou	německý	k2eAgFnSc7d1
Borussií	Borussie	k1gFnSc7
Mönchengladbach	Mönchengladbacha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
sezóně	sezóna	k1gFnSc6
1974-1975	1974-1975	k4
skončil	skončit	k5eAaPmAgMnS
Baník	baník	k1gMnSc1
v	v	k7c6
první	první	k4xOgFnSc6
lize	liga	k1gFnSc6
až	až	k9
na	na	k7c4
13	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
a	a	k8xC
příliš	příliš	k6eAd1
se	se	k3xPyFc4
nečekalo	čekat	k5eNaImAgNnS
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
dokázal	dokázat	k5eAaPmAgMnS
nějaký	nějaký	k3yIgInSc4
zázračný	zázračný	k2eAgInSc4d1
výsledek	výsledek	k1gInSc4
v	v	k7c6
další	další	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
další	další	k2eAgFnSc4d1
sezónu	sezóna	k1gFnSc4
byl	být	k5eAaImAgMnS
novým	nový	k2eAgMnSc7d1
manažerem	manažer	k1gMnSc7
ostravského	ostravský	k2eAgInSc2d1
Baníku	Baník	k1gInSc2
jmenován	jmenován	k2eAgMnSc1d1
Pražák	Pražák	k1gMnSc1
Jiří	Jiří	k1gMnSc1
Rubáš	rubat	k5eAaImIp2nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Baníku	Baník	k1gInSc2
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
odrazit	odrazit	k5eAaPmF
se	se	k3xPyFc4
ze	z	k7c2
dna	dno	k1gNnSc2
a	a	k8xC
v	v	k7c6
druhé	druhý	k4xOgFnSc6
polovině	polovina	k1gFnSc6
sezóny	sezóna	k1gFnSc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
poprvé	poprvé	k6eAd1
v	v	k7c6
historii	historie	k1gFnSc6
mistrem	mistr	k1gMnSc7
Československé	československý	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
posledním	poslední	k2eAgNnSc7d1
utkáním	utkání	k1gNnSc7
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
byl	být	k5eAaImAgInS
Baník	Baník	k1gInSc4
na	na	k7c6
druhém	druhý	k4xOgNnSc6
místě	místo	k1gNnSc6
tabulky	tabulka	k1gFnSc2
o	o	k7c4
jeden	jeden	k4xCgInSc4
bod	bod	k1gInSc4
za	za	k7c2
Slávií	Slávia	k1gFnPc2
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Baník	baník	k1gMnSc1
však	však	k9
vyhrál	vyhrát	k5eAaPmAgMnS
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
Slavii	slavie	k1gFnSc4
porazil	porazit	k5eAaPmAgInS
Slovan	Slovan	k1gInSc1
Bratislava	Bratislava	k1gFnSc1
také	také	k9
1	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
těmto	tento	k3xDgInPc3
výsledkům	výsledek	k1gInPc3
se	se	k3xPyFc4
Baník	Baník	k1gInSc1
mohl	moct	k5eAaImAgInS
poprvé	poprvé	k6eAd1
v	v	k7c6
historii	historie	k1gFnSc6
radovat	radovat	k5eAaImF
z	z	k7c2
ligového	ligový	k2eAgInSc2d1
titulu	titul	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgNnSc7
také	také	k9
postoupil	postoupit	k5eAaPmAgMnS
do	do	k7c2
poháru	pohár	k1gInSc2
mistrů	mistr	k1gMnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
jej	on	k3xPp3gInSc4
ve	v	k7c6
druhém	druhý	k4xOgNnSc6
kole	kolo	k1gNnSc6
vyřadil	vyřadit	k5eAaPmAgInS
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
<g/>
,	,	kIx,
nad	nad	k7c7
kterým	který	k3yRgMnSc7,k3yQgMnSc7,k3yIgMnSc7
ale	ale	k8xC
vyhrál	vyhrát	k5eAaPmAgMnS
doma	doma	k6eAd1
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Skutečná	skutečný	k2eAgFnSc1d1
zlatá	zlatý	k2eAgFnSc1d1
éra	éra	k1gFnSc1
v	v	k7c6
historii	historie	k1gFnSc6
klubu	klub	k1gInSc2
však	však	k9
teprve	teprve	k6eAd1
začala	začít	k5eAaPmAgFnS
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
novým	nový	k2eAgMnSc7d1
trenérem	trenér	k1gMnSc7
FCB	FCB	kA
stal	stát	k5eAaPmAgMnS
Evžen	Evžen	k1gMnSc1
Hadamczik	Hadamczik	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
to	ten	k3xDgNnSc1
mladý	mladý	k2eAgMnSc1d1
a	a	k8xC
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
široké	široký	k2eAgFnSc2d1
veřejnosti	veřejnost	k1gFnSc2
neznámý	známý	k2eNgMnSc1d1
trenér	trenér	k1gMnSc1
bez	bez	k7c2
ligové	ligový	k2eAgFnSc2d1
zkušenosti	zkušenost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1978	#num#	k4
s	s	k7c7
ním	on	k3xPp3gMnSc7
Baník	baník	k1gMnSc1
podruhé	podruhé	k6eAd1
v	v	k7c6
historii	historie	k1gFnSc6
vyhrál	vyhrát	k5eAaPmAgInS
Československý	československý	k2eAgInSc1d1
pohár	pohár	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
sezóně	sezóna	k1gFnSc6
1978-1979	1978-1979	k4
skončili	skončit	k5eAaPmAgMnP
Ostravané	Ostravan	k1gMnPc1
druzí	druhý	k4xOgMnPc1
těsně	těsně	k6eAd1
za	za	k7c7
Duklou	Dukla	k1gFnSc7
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
Baník	Baník	k1gInSc1
neprohrál	prohrát	k5eNaPmAgInS
74	#num#	k4
domácích	domácí	k2eAgInPc2d1
zápasů	zápas	k1gInPc2
v	v	k7c6
řadě	řada	k1gFnSc6
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
téměř	téměř	k6eAd1
pět	pět	k4xCc4
roků	rok	k1gInPc2
nebyl	být	k5eNaImAgMnS
na	na	k7c6
domácí	domácí	k2eAgFnSc6d1
půdě	půda	k1gFnSc6
poražen	porazit	k5eAaPmNgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
sezóně	sezóna	k1gFnSc6
Poháru	pohár	k1gInSc2
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
se	se	k3xPyFc4
Baník	Baník	k1gInSc1
dostal	dostat	k5eAaPmAgInS
až	až	k9
do	do	k7c2
semifinále	semifinále	k1gNnSc2
přes	přes	k7c4
Sporting	Sporting	k1gInSc4
Lisabon	Lisabon	k1gInSc1
<g/>
,	,	kIx,
Shamrock	Shamrock	k1gInSc1
Rovers	Roversa	k1gFnPc2
FC	FC	kA
a	a	k8xC
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Magdeburg	Magdeburg	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tam	tam	k6eAd1
jej	on	k3xPp3gNnSc4
vyřadila	vyřadit	k5eAaPmAgFnS
Fortuna	Fortuna	k1gFnSc1
Düsseldorf	Düsseldorf	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Zlatá	zlatý	k2eAgFnSc1d1
éra	éra	k1gFnSc1
pokračovala	pokračovat	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejlepší	dobrý	k2eAgMnPc1d3
hráči	hráč	k1gMnPc1
jako	jako	k9
např.	např.	kA
Verner	Verner	k1gMnSc1
Lička	lička	k1gFnSc1
a	a	k8xC
Rostislav	Rostislav	k1gMnSc1
Vojáček	Vojáček	k1gMnSc1
hráli	hrát	k5eAaImAgMnP
pravidelně	pravidelně	k6eAd1
za	za	k7c4
národní	národní	k2eAgInSc4d1
tým	tým	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiní	jiný	k1gMnPc1
hráli	hrát	k5eAaImAgMnP
také	také	k9
pro	pro	k7c4
olympijský	olympijský	k2eAgInSc4d1
tým	tým	k1gInSc4
např.	např.	kA
<g/>
:	:	kIx,
Petr	Petr	k1gMnSc1
Němec	Němec	k1gMnSc1
<g/>
,	,	kIx,
Libor	Libor	k1gMnSc1
Radimec	Radimec	k1gMnSc1
<g/>
,	,	kIx,
Zdeněk	Zdeněk	k1gMnSc1
Šreiner	Šreiner	k1gMnSc1
a	a	k8xC
Zdeněk	Zdeněk	k1gMnSc1
Rygel	Rygel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
létě	léto	k1gNnSc6
roku	rok	k1gInSc2
1976	#num#	k4
přilákal	přilákat	k5eAaPmAgInS
až	až	k9
35	#num#	k4
000	#num#	k4
diváků	divák	k1gMnPc2
mezinárodní	mezinárodní	k2eAgInSc4d1
souboj	souboj	k1gInSc4
tehdy	tehdy	k6eAd1
mistrovského	mistrovský	k2eAgInSc2d1
Baníku	Baník	k1gInSc2
s	s	k7c7
výběrem	výběr	k1gInSc7
Brazílie	Brazílie	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
ladil	ladit	k5eAaImAgInS
formu	forma	k1gFnSc4
před	před	k7c7
olympijskými	olympijský	k2eAgFnPc7d1
hrami	hra	k1gFnPc7
v	v	k7c6
kanadském	kanadský	k2eAgInSc6d1
Montrealu	Montreal	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Baník	Baník	k1gInSc1
zvítězil	zvítězit	k5eAaPmAgInS
gólem	gól	k1gInSc7
z	z	k7c2
penalty	penalta	k1gFnSc2
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
proměnil	proměnit	k5eAaPmAgMnS
Petr	Petr	k1gMnSc1
Slaný	Slaný	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Archivováno	archivován	k2eAgNnSc4d1
4	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2020	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
</s>
<s>
V	v	k7c6
sezóně	sezóna	k1gFnSc6
1979-1980	1979-1980	k4
dosáhl	dosáhnout	k5eAaPmAgInS
Baník	Baník	k1gInSc1
svého	svůj	k3xOyFgInSc2
druhého	druhý	k4xOgInSc2
mistrovského	mistrovský	k2eAgInSc2d1
titulu	titul	k1gInSc2
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhá	druhý	k4xOgFnSc1
Zbrojovka	zbrojovka	k1gFnSc1
Brno	Brno	k1gNnSc4
na	na	k7c4
něj	on	k3xPp3gNnSc4
ztrácela	ztrácet	k5eAaImAgFnS
5	#num#	k4
bodů	bod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
další	další	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
vyhrál	vyhrát	k5eAaPmAgMnS
Baník	Baník	k1gInSc4
titul	titul	k1gInSc4
znovu	znovu	k6eAd1
a	a	k8xC
postoupil	postoupit	k5eAaPmAgMnS
až	až	k6eAd1
do	do	k7c2
čtvrtfinále	čtvrtfinále	k1gNnSc2
poháru	pohár	k1gInSc2
UEFA	UEFA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tam	tam	k6eAd1
ho	on	k3xPp3gMnSc4
vyřadil	vyřadit	k5eAaPmAgInS
Bayern	Bayern	k1gInSc1
Mnichov	Mnichov	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
následujících	následující	k2eAgFnPc6d1
dvou	dva	k4xCgFnPc6
sezónách	sezóna	k1gFnPc6
Baník	baník	k1gMnSc1
skončil	skončit	k5eAaPmAgMnS
druhý	druhý	k4xOgMnSc1
v	v	k7c6
ligové	ligový	k2eAgFnSc6d1
tabulce	tabulka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
sezóně	sezóna	k1gFnSc6
1982	#num#	k4
<g/>
-	-	kIx~
<g/>
1983	#num#	k4
<g/>
,	,	kIx,
trenér	trenér	k1gMnSc1
Hadamczik	Hadamczik	k1gMnSc1
odstoupil	odstoupit	k5eAaPmAgMnS
a	a	k8xC
tím	ten	k3xDgNnSc7
symbolicky	symbolicky	k6eAd1
končí	končit	k5eAaImIp3nS
zlatá	zlatý	k2eAgFnSc1d1
éra	éra	k1gFnSc1
klubu	klub	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Konec	konec	k1gInSc1
zlaté	zlatý	k2eAgFnSc2d1
éry	éra	k1gFnSc2
(	(	kIx(
<g/>
90	#num#	k4
<g/>
.	.	kIx.
léta	léto	k1gNnSc2
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
následujících	následující	k2eAgNnPc6d1
letech	léto	k1gNnPc6
se	se	k3xPyFc4
Baník	Baník	k1gInSc1
stáhl	stáhnout	k5eAaPmAgInS
z	z	k7c2
hrotu	hrot	k1gInSc2
tabulky	tabulka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prošel	projít	k5eAaPmAgMnS
další	další	k2eAgFnSc7d1
generační	generační	k2eAgFnSc7d1
změnou	změna	k1gFnSc7
a	a	k8xC
týmu	tým	k1gInSc2
<g/>
,	,	kIx,
složeným	složený	k2eAgFnPc3d1
z	z	k7c2
většiny	většina	k1gFnSc2
mladými	mladý	k2eAgMnPc7d1
hráči	hráč	k1gMnPc1
se	se	k3xPyFc4
v	v	k7c6
lize	liga	k1gFnSc6
nedařilo	dařit	k5eNaImAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přesto	přesto	k8xC
figuroval	figurovat	k5eAaImAgInS
v	v	k7c6
horní	horní	k2eAgFnSc6d1
části	část	k1gFnSc6
tabulky	tabulka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
sezónách	sezóna	k1gFnPc6
1988-1989	1988-1989	k4
a	a	k8xC
1989-1990	1989-1990	k4
skončil	skončit	k5eAaPmAgMnS
druhý	druhý	k4xOgMnSc1
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1991	#num#	k4
naposledy	naposledy	k6eAd1
vyhrál	vyhrát	k5eAaPmAgMnS
Československý	československý	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
v	v	k7c6
posledním	poslední	k2eAgInSc6d1
zápase	zápas	k1gInSc6
porazil	porazit	k5eAaPmAgInS
Spartak	Spartak	k1gInSc1
Trnava	Trnava	k1gFnSc1
6	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
byl	být	k5eAaImAgInS
na	na	k7c4
dlouhou	dlouhý	k2eAgFnSc4d1
dobu	doba	k1gFnSc4
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
poslední	poslední	k2eAgInSc4d1
úspěch	úspěch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Období	období	k1gNnSc1
od	od	k7c2
roku	rok	k1gInSc2
2000	#num#	k4
do	do	k7c2
2015	#num#	k4
</s>
<s>
Od	od	k7c2
roku	rok	k1gInSc2
1991	#num#	k4
se	se	k3xPyFc4
klub	klub	k1gInSc1
stále	stále	k6eAd1
propadal	propadat	k5eAaImAgInS,k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
finančním	finanční	k2eAgInPc3d1
problémům	problém	k1gInPc3
se	se	k3xPyFc4
připojili	připojit	k5eAaPmAgMnP
i	i	k9
sponzoři	sponzor	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
procházeli	procházet	k5eAaImAgMnP
restrukturalizací	restrukturalizace	k1gFnSc7
po	po	k7c6
pádu	pád	k1gInSc6
komunismu	komunismus	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výsledky	výsledek	k1gInPc7
byly	být	k5eAaImAgInP
všelijaké	všelijaký	k3yIgMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejlepším	dobrý	k2eAgNnSc7d3
umístěním	umístění	k1gNnSc7
bylo	být	k5eAaImAgNnS
třetí	třetí	k4xOgNnSc1
místo	místo	k1gNnSc1
v	v	k7c6
sezoně	sezona	k1gFnSc6
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
však	však	k9
bojoval	bojovat	k5eAaImAgInS
o	o	k7c4
ligové	ligový	k2eAgNnSc4d1
přežití	přežití	k1gNnSc4
v	v	k7c6
období	období	k1gNnSc6
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
skončil	skončit	k5eAaPmAgInS
14	#num#	k4
<g/>
.	.	kIx.
v	v	k7c6
tabulce	tabulka	k1gFnSc6
<g/>
,	,	kIx,
pouhé	pouhý	k2eAgInPc4d1
čtyři	čtyři	k4xCgInPc4
body	bod	k1gInPc4
před	před	k7c7
sestupem	sestup	k1gInSc7
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
dlouhém	dlouhý	k2eAgNnSc6d1
trápení	trápení	k1gNnSc6
však	však	k9
přišel	přijít	k5eAaPmAgInS
úspěch	úspěch	k1gInSc1
v	v	k7c6
sezóně	sezóna	k1gFnSc6
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
dosáhl	dosáhnout	k5eAaPmAgMnS
ligového	ligový	k2eAgInSc2d1
titulu	titul	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Baník	Baník	k1gInSc1
celou	celý	k2eAgFnSc4d1
sezónu	sezóna	k1gFnSc4
suverénně	suverénně	k6eAd1
vedl	vést	k5eAaImAgMnS
tabulku	tabulka	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
sezóně	sezóna	k1gFnSc6
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
nejlepším	dobrý	k2eAgMnSc7d3
střelcem	střelec	k1gMnSc7
Marek	Marek	k1gMnSc1
Heinz	Heinz	k1gMnSc1
s	s	k7c7
19	#num#	k4
vstřelenými	vstřelený	k2eAgFnPc7d1
brankami	branka	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
této	tento	k3xDgFnSc6
sezóně	sezóna	k1gFnSc6
však	však	k9
tým	tým	k1gInSc1
prošel	projít	k5eAaPmAgInS
dramatickou	dramatický	k2eAgFnSc7d1
změnou	změna	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Trenéra	trenér	k1gMnSc4
Františka	František	k1gMnSc4
Komňackého	Komňacký	k1gMnSc4
a	a	k8xC
většinu	většina	k1gFnSc4
hlavních	hlavní	k2eAgMnPc2d1
hráčů	hráč	k1gMnPc2
vedení	vedení	k1gNnSc2
Baníku	Baník	k1gInSc2
prodalo	prodat	k5eAaPmAgNnS
<g/>
:	:	kIx,
René	René	k1gFnSc1
Bolfa	Bolf	k1gMnSc2
<g/>
,	,	kIx,
Jana	Jana	k1gFnSc1
Laštůvku	laštůvka	k1gFnSc4
<g/>
,	,	kIx,
Miroslava	Miroslav	k1gMnSc4
Matušoviče	Matušovič	k1gMnSc4
i	i	k8xC
Marka	Marek	k1gMnSc4
Heinze	Heinze	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
další	další	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
skončil	skončit	k5eAaPmAgMnS
sedmý	sedmý	k4xOgMnSc1
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
,	,	kIx,
přesto	přesto	k8xC
se	se	k3xPyFc4
mu	on	k3xPp3gMnSc3
ale	ale	k9
podařilo	podařit	k5eAaPmAgNnS
vyhrát	vyhrát	k5eAaPmF
Pohár	pohár	k1gInSc4
ČMFS	ČMFS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
se	se	k3xPyFc4
Baník	Baník	k1gInSc1
objevoval	objevovat	k5eAaImAgInS
pravidelně	pravidelně	k6eAd1
v	v	k7c6
horní	horní	k2eAgFnSc6d1
části	část	k1gFnSc6
ligové	ligový	k2eAgFnSc2d1
tabulky	tabulka	k1gFnSc2
a	a	k8xC
skončil	skončit	k5eAaPmAgInS
dvakrát	dvakrát	k6eAd1
na	na	k7c6
třetím	třetí	k4xOgInSc6
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Klub	klub	k1gInSc1
se	se	k3xPyFc4
i	i	k9
přes	přes	k7c4
své	svůj	k3xOyFgInPc4
sportovní	sportovní	k2eAgInPc4d1
úspěchy	úspěch	k1gInPc4
nacházel	nacházet	k5eAaImAgMnS
ve	v	k7c6
finančních	finanční	k2eAgInPc6d1
problémech	problém	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
rozprodávání	rozprodávání	k1gNnSc3
hráčského	hráčský	k2eAgInSc2d1
kádru	kádr	k1gInSc2
<g/>
,	,	kIx,
v	v	k7c6
prosinci	prosinec	k1gInSc6
2011	#num#	k4
dokonce	dokonce	k9
odprodal	odprodat	k5eAaPmAgInS
i	i	k9
stadión	stadión	k1gInSc1
na	na	k7c6
Bazalech	Bazal	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
únoru	únor	k1gInSc6
2012	#num#	k4
změnil	změnit	k5eAaPmAgInS
klub	klub	k1gInSc1
majitele	majitel	k1gMnSc4
<g/>
,	,	kIx,
když	když	k8xS
Tomáš	Tomáš	k1gMnSc1
Petera	Petera	k1gMnSc1
prodal	prodat	k5eAaPmAgMnS
svůj	svůj	k3xOyFgMnSc1
95	#num#	k4
<g/>
%	%	kIx~
podíl	podíl	k1gInSc1
společnostem	společnost	k1gFnPc3
SMK	SMK	kA
Reality	realita	k1gFnSc2
Invest	Invest	k1gMnSc1
(	(	kIx(
<g/>
Libor	Libor	k1gMnSc1
Adámek	Adámek	k1gMnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
již	již	k6eAd1
vlastnila	vlastnit	k5eAaImAgFnS
Bazaly	Bazala	k1gFnPc4
<g/>
,	,	kIx,
a	a	k8xC
PAM	PAM	kA
market	market	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
sezóně	sezóna	k1gFnSc6
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
2012	#num#	k4
skončil	skončit	k5eAaPmAgInS
Baník	Baník	k1gInSc1
rovněž	rovněž	k9
na	na	k7c4
14	#num#	k4
<g/>
.	.	kIx.
pozici	pozice	k1gFnSc6
v	v	k7c6
tabulce	tabulka	k1gFnSc6
a	a	k8xC
vyhnul	vyhnout	k5eAaPmAgMnS
se	se	k3xPyFc4
tak	tak	k6eAd1
sestupu	sestup	k1gInSc2
na	na	k7c6
poslední	poslední	k2eAgFnSc6d1
chvíli	chvíle	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vzhledem	vzhledem	k7c3
ke	k	k7c3
dluhům	dluh	k1gInPc3
se	se	k3xPyFc4
musel	muset	k5eAaImAgInS
klub	klub	k1gInSc1
uskromnit	uskromnit	k5eAaPmF
a	a	k8xC
trenér	trenér	k1gMnSc1
Radoslav	Radoslav	k1gMnSc1
Látal	Látal	k1gMnSc1
dostal	dostat	k5eAaPmAgMnS
za	za	k7c4
úkol	úkol	k1gInSc4
připravit	připravit	k5eAaPmF
mužstvo	mužstvo	k1gNnSc4
na	na	k7c4
novou	nový	k2eAgFnSc4d1
sezónu	sezóna	k1gFnSc4
s	s	k7c7
mnohem	mnohem	k6eAd1
menším	malý	k2eAgInSc7d2
finančním	finanční	k2eAgInSc7d1
rozpočtem	rozpočet	k1gInSc7
pro	pro	k7c4
A	A	kA
tým	tým	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Klub	klub	k1gInSc1
získal	získat	k5eAaPmAgInS
dotaci	dotace	k1gFnSc4
od	od	k7c2
města	město	k1gNnSc2
ve	v	k7c6
výši	výše	k1gFnSc6,k1gFnSc6wB
30	#num#	k4
milionů	milion	k4xCgInPc2
korun	koruna	k1gFnPc2
na	na	k7c4
rekonstrukci	rekonstrukce	k1gFnSc4
stadionu	stadion	k1gInSc2
Bazalů	Bazal	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rekonstrukce	rekonstrukce	k1gFnSc1
hrací	hrací	k2eAgFnSc2d1
plochy	plocha	k1gFnSc2
se	se	k3xPyFc4
platila	platit	k5eAaImAgFnS
z	z	k7c2
peněz	peníze	k1gInPc2
fanoušků	fanoušek	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
prostřednictvím	prostřednictvím	k7c2
transparentního	transparentní	k2eAgInSc2d1
účtu	účet	k1gInSc2
Zachraňme	zachránit	k5eAaPmRp1nP
Baníček	Baníček	k1gInSc4
<g/>
!!!	!!!	k?
zaslali	zaslat	k5eAaPmAgMnP
své	svůj	k3xOyFgInPc4
dary	dar	k1gInPc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
sezóně	sezóna	k1gFnSc6
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
velké	velký	k2eAgFnSc3d1
události	událost	k1gFnSc3
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
Baník	Baník	k1gInSc1
přestěhoval	přestěhovat	k5eAaPmAgInS
na	na	k7c4
Městský	městský	k2eAgInSc4d1
stadion	stadion	k1gInSc4
v	v	k7c6
Ostravě-Vítkovicích	Ostravě-Vítkovice	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
hraje	hrát	k5eAaImIp3nS
svá	svůj	k3xOyFgNnPc4
domácí	domácí	k2eAgNnPc4d1
utkání	utkání	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s>
Pád	Pád	k1gInSc1
do	do	k7c2
druhé	druhý	k4xOgFnSc2
ligy	liga	k1gFnSc2
a	a	k8xC
návrat	návrat	k1gInSc4
zpět	zpět	k6eAd1
(	(	kIx(
<g/>
2015	#num#	k4
až	až	k9
2018	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sezóna	sezóna	k1gFnSc1
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
</s>
<s>
Městský	městský	k2eAgInSc1d1
stadion	stadion	k1gInSc1
v	v	k7c6
Ostravě-Vítkovicích	Ostravě-Vítkovice	k1gFnPc6
<g/>
,	,	kIx,
azylový	azylový	k2eAgInSc1d1
domovský	domovský	k2eAgInSc1d1
stadion	stadion	k1gInSc1
Baníku	Baník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
sezoně	sezona	k1gFnSc6
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
2016	#num#	k4
byl	být	k5eAaImAgInS
pro	pro	k7c4
Baník	Baník	k1gInSc4
konečnou	konečná	k1gFnSc4
v	v	k7c6
1	#num#	k4
<g/>
.	.	kIx.
české	český	k2eAgFnSc6d1
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hned	hned	k6eAd1
na	na	k7c6
začátku	začátek	k1gInSc6
sezony	sezona	k1gFnSc2
opustilo	opustit	k5eAaPmAgNnS
tým	tým	k1gInSc4
několik	několik	k4yIc1
hráčů	hráč	k1gMnPc2
základní	základní	k2eAgFnSc2d1
jedenáctky	jedenáctka	k1gFnSc2
(	(	kIx(
<g/>
např.	např.	kA
Michal	Michal	k1gMnSc1
Frydrych	Frydrych	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
zamířil	zamířit	k5eAaPmAgMnS
do	do	k7c2
Slavie	slavie	k1gFnSc2
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
Baníku	Baník	k1gInSc2
však	však	k9
přišel	přijít	k5eAaPmAgMnS
třeba	třeba	k6eAd1
odchovanec	odchovanec	k1gMnSc1
Tomáš	Tomáš	k1gMnSc1
Mičola	Mičola	k1gFnSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yRgInSc1,k3yIgInSc1
však	však	k9
vinou	vina	k1gFnSc7
zranění	zranění	k1gNnSc2
odehrál	odehrát	k5eAaPmAgInS
v	v	k7c6
sezóně	sezóna	k1gFnSc6
pouze	pouze	k6eAd1
6	#num#	k4
utkání	utkání	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkově	celkově	k6eAd1
však	však	k9
oslabený	oslabený	k2eAgInSc4d1
tým	tým	k1gInSc4
pak	pak	k6eAd1
na	na	k7c4
podzim	podzim	k1gInSc4
získal	získat	k5eAaPmAgMnS
jen	jen	k9
4	#num#	k4
body	bod	k1gInPc4
a	a	k8xC
byl	být	k5eAaImAgInS
považován	považován	k2eAgInSc1d1
za	za	k7c4
již	již	k6eAd1
jistý	jistý	k2eAgInSc4d1
sestupující	sestupující	k2eAgInSc4d1
tým	tým	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
v	v	k7c6
zimě	zima	k1gFnSc6
klub	klub	k1gInSc1
přivedl	přivést	k5eAaPmAgInS
na	na	k7c4
post	post	k1gInSc4
trenéra	trenér	k1gMnSc2
zkušeného	zkušený	k2eAgMnSc2d1
kouče	kouč	k1gMnSc2
Vlastimila	Vlastimil	k1gMnSc2
Petrželu	Petržel	k1gInSc2
a	a	k8xC
klub	klub	k1gInSc4
převzal	převzít	k5eAaPmAgMnS
nový	nový	k2eAgMnSc1d1
majitel	majitel	k1gMnSc1
Václav	Václav	k1gMnSc1
Brabec	Brabec	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tým	tým	k1gInSc1
se	se	k3xPyFc4
sice	sice	k8xC
do	do	k7c2
začátku	začátek	k1gInSc2
jarní	jarní	k2eAgFnSc2d1
části	část	k1gFnSc2
sezony	sezona	k1gFnSc2
postavil	postavit	k5eAaPmAgMnS
na	na	k7c4
nohy	noha	k1gFnPc4
s	s	k7c7
novými	nový	k2eAgMnPc7d1
hráči	hráč	k1gMnPc7
<g/>
,	,	kIx,
získal	získat	k5eAaPmAgInS
však	však	k9
jen	jen	k9
dalších	další	k2eAgInPc2d1
deset	deset	k4xCc4
bodů	bod	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
společně	společně	k6eAd1
se	se	k3xPyFc4
Sigmou	Sigmý	k2eAgFnSc4d1
Olomouc	Olomouc	k1gFnSc4
sestoupil	sestoupit	k5eAaPmAgMnS
do	do	k7c2
Fotbalové	fotbalový	k2eAgFnSc2d1
národní	národní	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Sezóna	sezóna	k1gFnSc1
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
</s>
<s>
Sezonu	sezona	k1gFnSc4
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
odehrál	odehrát	k5eAaPmAgMnS
Baník	baník	k1gMnSc1
Ostrava	Ostrava	k1gFnSc1
ve	v	k7c6
druhé	druhý	k4xOgFnSc6
lize	liga	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavním	hlavní	k2eAgInSc7d1
cílem	cíl	k1gInSc7
klubu	klub	k1gInSc2
byl	být	k5eAaImAgInS
okamžitý	okamžitý	k2eAgInSc1d1
návrat	návrat	k1gInSc1
do	do	k7c2
nejvyšší	vysoký	k2eAgFnSc2d3
soutěže	soutěž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
I	i	k8xC
přes	přes	k7c4
nevyrovnané	vyrovnaný	k2eNgInPc4d1
výsledky	výsledek	k1gInPc4
se	se	k3xPyFc4
nakonec	nakonec	k6eAd1
Baník	baník	k1gMnSc1
umístil	umístit	k5eAaPmAgMnS
na	na	k7c6
druhém	druhý	k4xOgNnSc6
postupovém	postupový	k2eAgNnSc6d1
místě	místo	k1gNnSc6
za	za	k7c4
Sigmou	Sigmý	k2eAgFnSc4d1
Olomouc	Olomouc	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
postupu	postup	k1gInSc6
<g/>
,	,	kIx,
na	na	k7c4
úkor	úkor	k1gInSc4
Opavy	Opava	k1gFnSc2
<g/>
,	,	kIx,
však	však	k9
rozhodl	rozhodnout	k5eAaPmAgInS
až	až	k6eAd1
výhrou	výhra	k1gFnSc7
proti	proti	k7c3
Znojmu	Znojmo	k1gNnSc3
v	v	k7c6
posledním	poslední	k2eAgNnSc6d1
kole	kolo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Statisticky	statisticky	k6eAd1
nejlepším	dobrý	k2eAgMnSc7d3
střelcem	střelec	k1gMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Alexander	Alexandra	k1gFnPc2
Jakubov	Jakubov	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
však	však	k9
10	#num#	k4
z	z	k7c2
11	#num#	k4
gólů	gól	k1gInPc2
vstřelil	vstřelit	k5eAaPmAgMnS
ještě	ještě	k6eAd1
na	na	k7c4
podzim	podzim	k1gInSc4
v	v	k7c6
dresu	dres	k1gInSc6
Varnsdorfu	Varnsdorf	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhým	druhý	k4xOgInSc7
nejlepším	dobrý	k2eAgMnSc7d3
střelcem	střelec	k1gMnSc7
byl	být	k5eAaImAgMnS
Tomáš	Tomáš	k1gMnSc1
Mičola	Mičola	k1gFnSc1
se	s	k7c7
sedmi	sedm	k4xCc7
góly	gól	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc7d1
oporou	opora	k1gFnSc7
týmu	tým	k1gInSc2
tak	tak	k6eAd1
byl	být	k5eAaImAgMnS
brankář	brankář	k1gMnSc1
Petr	Petr	k1gMnSc1
Vašek	Vašek	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yIgMnSc1,k3yRgMnSc1,k3yQgMnSc1
vychytal	vychytat	k5eAaPmAgMnS
18	#num#	k4
nul	nula	k1gFnPc2
a	a	k8xC
byl	být	k5eAaImAgInS
hlavním	hlavní	k2eAgMnSc7d1
strůjcem	strůjce	k1gMnSc7
pevné	pevný	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
Baníku	Baník	k1gInSc2
<g/>
,	,	kIx,
když	když	k8xS
tým	tým	k1gInSc1
obdržel	obdržet	k5eAaPmAgInS
pouze	pouze	k6eAd1
20	#num#	k4
gólů	gól	k1gInPc2
ve	v	k7c6
třiceti	třicet	k4xCc6
zápasech	zápas	k1gInPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pozitivem	pozitiv	k1gMnSc7
sezony	sezona	k1gFnSc2
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
pak	pak	k9
byla	být	k5eAaImAgFnS
neutuchající	utuchající	k2eNgFnSc1d1
podpora	podpora	k1gFnSc1
fanoušků	fanoušek	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dvakrát	dvakrát	k6eAd1
<g/>
,	,	kIx,
v	v	k7c6
zápasech	zápas	k1gInPc6
s	s	k7c7
Opavou	Opava	k1gFnSc7
a	a	k8xC
Znojmem	Znojmo	k1gNnSc7
<g/>
,	,	kIx,
se	se	k3xPyFc4
dokonce	dokonce	k9
podařilo	podařit	k5eAaPmAgNnS
vyprodat	vyprodat	k5eAaPmF
Městský	městský	k2eAgInSc4d1
stadion	stadion	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Sezóna	sezóna	k1gFnSc1
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
</s>
<s>
V	v	k7c6
letní	letní	k2eAgFnSc6d1
pauze	pauza	k1gFnSc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
velkému	velký	k2eAgInSc3d1
návratu	návrat	k1gInSc3
<g/>
,	,	kIx,
když	když	k8xS
ostravská	ostravský	k2eAgFnSc1d1
legenda	legenda	k1gFnSc1
Milan	Milan	k1gMnSc1
Baroš	Baroš	k1gMnSc1
znovu	znovu	k6eAd1
oblékla	obléct	k5eAaPmAgFnS
modrý	modrý	k2eAgInSc4d1
dres	dres	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jako	jako	k8xS,k8xC
další	další	k2eAgFnPc4d1
posily	posila	k1gFnPc4
přišel	přijít	k5eAaPmAgMnS
třeba	třeba	k6eAd1
pravý	pravý	k2eAgMnSc1d1
bek	bek	k1gMnSc1
Lukáš	Lukáš	k1gMnSc1
Pazdera	Pazdera	k1gMnSc1
<g/>
,	,	kIx,
ukrajinský	ukrajinský	k2eAgMnSc1d1
stoper	stoper	k1gMnSc1
Oleksandr	Oleksandr	k1gInSc1
Azackyj	Azackyj	k1gInSc4
<g/>
,	,	kIx,
defenzivní	defenzivní	k2eAgInSc4d1
štít	štít	k1gInSc4
Martin	Martin	k1gMnSc1
Šindelář	Šindelář	k1gMnSc1
či	či	k8xC
na	na	k7c6
hostování	hostování	k1gNnSc6
plzeňský	plzeňský	k2eAgMnSc1d1
útočník	útočník	k1gMnSc1
Tomáš	Tomáš	k1gMnSc1
Poznar	Poznar	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
kádru	kádr	k1gInSc6
došlo	dojít	k5eAaPmAgNnS
však	však	k8xC
došlo	dojít	k5eAaPmAgNnS
i	i	k9
k	k	k7c3
odchodů	odchod	k1gInPc2
<g/>
,	,	kIx,
když	když	k8xS
v	v	k7c6
Baníku	Baník	k1gInSc6
skončil	skončit	k5eAaPmAgMnS
lídr	lídr	k1gMnSc1
obrany	obrana	k1gFnSc2
minulé	minulý	k2eAgFnSc2d1
sezóny	sezóna	k1gFnSc2
Tomáš	Tomáš	k1gMnSc1
Zápotočný	Zápotočný	k2eAgMnSc1d1
či	či	k8xC
krajní	krajní	k2eAgMnSc1d1
záložník	záložník	k1gMnSc1
Karol	Karola	k1gFnPc2
Mondek	Mondka	k1gFnPc2
<g/>
,	,	kIx,
několika	několik	k4yIc3
hráčům	hráč	k1gMnPc3
skončilo	skončit	k5eAaPmAgNnS
hostování	hostování	k1gNnSc4
v	v	k7c6
Baníku	Baník	k1gInSc6
(	(	kIx(
<g/>
Petr	Petr	k1gMnSc1
Nerad	nerad	k2eAgMnSc1d1
<g/>
,	,	kIx,
Štefan	Štefan	k1gMnSc1
Pekár	Pekár	k1gMnSc1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
Hučko	Hučko	k1gMnSc1
<g/>
,	,	kIx,
Ľubomír	Ľubomír	k1gMnSc1
Urgela	Urgela	k1gFnSc1
<g/>
)	)	kIx)
a	a	k8xC
někteří	některý	k3yIgMnPc1
hráči	hráč	k1gMnPc1
hostovat	hostovat	k5eAaImF
odešli	odejít	k5eAaPmAgMnP
(	(	kIx(
<g/>
např.	např.	kA
Josef	Josef	k1gMnSc1
Celba	Celba	k1gMnSc1
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
Chvěja	Chvěja	k1gMnSc1
<g/>
,	,	kIx,
Matěj	Matěj	k1gMnSc1
Helešic	Helešic	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Sezónu	sezóna	k1gFnSc4
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
2018	#num#	k4
zahájil	zahájit	k5eAaPmAgInS
Baník	Baník	k1gInSc1
slibně	slibně	k6eAd1
venkovní	venkovní	k2eAgFnSc7d1
výhrou	výhra	k1gFnSc7
proti	proti	k7c3
Brnu	Brno	k1gNnSc3
a	a	k8xC
domácí	domácí	k2eAgFnSc7d1
remízou	remíza	k1gFnSc7
s	s	k7c7
úřadujícím	úřadující	k2eAgMnSc7d1
mistrem	mistr	k1gMnSc7
Slavií	slavie	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poté	poté	k6eAd1
však	však	k9
přišla	přijít	k5eAaPmAgFnS
série	série	k1gFnSc1
nepřesvědčivých	přesvědčivý	k2eNgInPc2d1
výsledků	výsledek	k1gInPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
Baník	baník	k1gMnSc1
prohrál	prohrát	k5eAaPmAgMnS
všechna	všechen	k3xTgNnPc4
ostatní	ostatní	k2eAgNnPc4d1
venkovní	venkovní	k2eAgNnPc4d1
utkání	utkání	k1gNnPc4
<g/>
,	,	kIx,
doma	doma	k6eAd1
dokázal	dokázat	k5eAaPmAgInS
porazit	porazit	k5eAaPmF
pouze	pouze	k6eAd1
Karvinou	Karviná	k1gFnSc7
a	a	k8xC
pětkrát	pětkrát	k6eAd1
remizovat	remizovat	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Baník	Baník	k1gInSc1
tak	tak	k6eAd1
měl	mít	k5eAaImAgInS
na	na	k7c4
podzim	podzim	k1gInSc4
na	na	k7c6
kontě	konto	k1gNnSc6
2	#num#	k4
výhry	výhra	k1gFnPc4
<g/>
,	,	kIx,
5	#num#	k4
remíz	remíza	k1gFnPc2
a	a	k8xC
9	#num#	k4
porážek	porážka	k1gFnPc2
<g/>
,	,	kIx,
podzimní	podzimní	k2eAgFnSc4d1
část	část	k1gFnSc4
sezóny	sezóna	k1gFnSc2
tak	tak	k6eAd1
zakončil	zakončit	k5eAaPmAgMnS
na	na	k7c6
předposledním	předposlední	k2eAgNnSc6d1
místě	místo	k1gNnSc6
tabulky	tabulka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
zimní	zimní	k2eAgFnSc6d1
pauze	pauza	k1gFnSc6
však	však	k9
Baník	Baník	k1gInSc1
začal	začít	k5eAaPmAgInS
výrazným	výrazný	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
posilovat	posilovat	k5eAaImF
<g/>
,	,	kIx,
když	když	k8xS
získal	získat	k5eAaPmAgMnS
celkem	celkem	k6eAd1
8	#num#	k4
nových	nový	k2eAgFnPc2d1
posil	posila	k1gFnPc2
díky	díky	k7c3
přestupům	přestup	k1gInPc3
-	-	kIx~
ze	z	k7c2
Zlína	Zlín	k1gInSc2
přišel	přijít	k5eAaPmAgInS
Dame	Dame	k1gFnSc3
Diop	Diop	k1gInSc1
<g/>
,	,	kIx,
z	z	k7c2
Teplic	Teplice	k1gFnPc2
Martin	Martin	k2eAgMnSc5d1
Fillo	Filla	k1gMnSc5
<g/>
,	,	kIx,
z	z	k7c2
Mladé	mladý	k2eAgFnSc2d1
Boleslavi	Boleslaev	k1gFnSc6
Jiří	Jiří	k1gMnSc1
Fleišman	Fleišman	k1gMnSc1
<g/>
,	,	kIx,
z	z	k7c2
Norska	Norsko	k1gNnSc2
Christophe	Christoph	k1gFnSc2
Psyché	psyché	k1gFnSc2
<g/>
,	,	kIx,
ze	z	k7c2
Slavie	slavie	k1gFnSc2
Jan	Jan	k1gMnSc1
Laštůvka	laštůvka	k1gFnSc1
<g/>
,	,	kIx,
z	z	k7c2
Bohemians	Bohemiansa	k1gFnPc2
Milan	Milan	k1gMnSc1
Jirásek	Jirásek	k1gMnSc1
<g/>
,	,	kIx,
z	z	k7c2
Turecka	Turecko	k1gNnSc2
Václav	Václav	k1gMnSc1
Procházka	Procházka	k1gMnSc1
a	a	k8xC
ze	z	k7c2
Sparty	Sparta	k1gFnSc2
Viktor	Viktor	k1gMnSc1
Budinský	Budinský	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
jaře	jaro	k1gNnSc6
však	však	k8xC
tým	tým	k1gInSc1
pokračoval	pokračovat	k5eAaImAgInS
v	v	k7c6
nepřesvědčivých	přesvědčivý	k2eNgInPc6d1
výkonech	výkon	k1gInPc6
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
po	po	k7c6
zisku	zisk	k1gInSc6
jednoho	jeden	k4xCgInSc2
bodu	bod	k1gInSc2
ze	z	k7c2
tří	tři	k4xCgNnPc2
utkání	utkání	k1gNnPc2
se	s	k7c7
Slováckem	Slovácko	k1gNnSc7
<g/>
,	,	kIx,
Teplicemi	Teplice	k1gFnPc7
a	a	k8xC
Jabloncem	Jablonec	k1gInSc7
stálo	stát	k5eAaImAgNnS
v	v	k7c6
březnu	březen	k1gInSc6
post	posta	k1gFnPc2
trenéra	trenér	k1gMnSc4
Radima	Radim	k1gMnSc4
Kučeru	Kučera	k1gMnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
Postu	post	k1gInSc2
trenéra	trenér	k1gMnSc2
se	se	k3xPyFc4
ujmul	ujmout	k5eAaPmAgMnS
bývalý	bývalý	k2eAgMnSc1d1
trenér	trenér	k1gMnSc1
Zlína	Zlín	k1gInSc2
Bohumil	Bohumil	k1gMnSc1
Páník	Páník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
čtvrtfinále	čtvrtfinále	k1gNnSc6
skončil	skončit	k5eAaPmAgInS
postup	postup	k1gInSc4
Baníku	Baník	k1gInSc2
v	v	k7c4
MOL	mol	k1gInSc4
Cupu	cup	k1gInSc2
<g/>
,	,	kIx,
když	když	k8xS
na	na	k7c4
penalty	penalta	k1gFnPc4
vypadl	vypadnout	k5eAaPmAgMnS
s	s	k7c7
Mladou	mladá	k1gFnSc7
Boleslaví	Boleslavý	k2eAgMnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
dalších	další	k2eAgInPc6d1
bojích	boj	k1gInPc6
o	o	k7c4
záchranu	záchrana	k1gFnSc4
se	se	k3xPyFc4
však	však	k9
Baník	Baník	k1gInSc1
výkonnostně	výkonnostně	k6eAd1
výrazně	výrazně	k6eAd1
zlepšil	zlepšit	k5eAaPmAgMnS
a	a	k8xC
díky	díky	k7c3
výhrám	výhra	k1gFnPc3
nad	nad	k7c7
Jihlavou	Jihlava	k1gFnSc7
<g/>
,	,	kIx,
Zlínem	Zlín	k1gInSc7
<g/>
,	,	kIx,
Bohemians	Bohemians	k1gInSc1
<g/>
,	,	kIx,
Spartou	Sparta	k1gFnSc7
a	a	k8xC
Zbrojovkou	zbrojovka	k1gFnSc7
Brno	Brno	k1gNnSc1
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
díky	díky	k7c3
remízám	remíza	k1gFnPc3
s	s	k7c7
Plzní	Plzeň	k1gFnSc7
<g/>
,	,	kIx,
Mladou	mladá	k1gFnSc7
Boleslaví	Boleslavý	k2eAgMnPc1d1
<g/>
,	,	kIx,
Libercem	Liberec	k1gInSc7
a	a	k8xC
Karvinou	Karviná	k1gFnSc7
skončil	skončit	k5eAaPmAgMnS
nakonec	nakonec	k6eAd1
na	na	k7c4
13	#num#	k4
<g/>
.	.	kIx.
místě	místo	k1gNnSc6
v	v	k7c6
tabulce	tabulka	k1gFnSc6
<g/>
,	,	kIx,
bod	bod	k1gInSc1
nad	nad	k7c7
pásmem	pásmo	k1gNnSc7
sestupu	sestup	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sezóna	sezóna	k1gFnSc1
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
</s>
<s>
V	v	k7c6
letní	letní	k2eAgFnSc6d1
pauze	pauza	k1gFnSc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
určité	určitý	k2eAgFnSc3d1
obměně	obměna	k1gFnSc3
kádru	kádr	k1gInSc2
<g/>
,	,	kIx,
když	když	k8xS
skončil	skončit	k5eAaPmAgMnS
Marek	Marek	k1gMnSc1
Hlinka	Hlinka	k1gMnSc1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
Poznar	Poznar	k1gMnSc1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
Mičola	Mičola	k1gFnSc1
či	či	k8xC
Dyjan	Dyjan	k1gMnSc1
Carlos	Carlos	k1gMnSc1
de	de	k?
Azevedo	Azevedo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
se	se	k3xPyFc4
vrátili	vrátit	k5eAaPmAgMnP
bývalí	bývalý	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
Baníku	Baník	k1gInSc2
Patrizio	Patrizio	k1gNnSc1
Stronati	Stronati	k1gMnSc1
a	a	k8xC
Daniel	Daniel	k1gMnSc1
Holzer	Holzer	k1gMnSc1
<g/>
,	,	kIx,
navíc	navíc	k6eAd1
doplnění	doplnění	k1gNnSc1
další	další	k2eAgNnSc1d1
posilou	posila	k1gFnSc7
Adamem	Adam	k1gMnSc7
Jánošem	Jánoš	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
zimě	zima	k1gFnSc6
se	se	k3xPyFc4
pak	pak	k6eAd1
přidal	přidat	k5eAaPmAgMnS
i	i	k9
Nemanja	Nemanja	k1gMnSc1
Kuzmanovič	Kuzmanovič	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
sezoně	sezona	k1gFnSc6
se	se	k3xPyFc4
Baníku	baník	k1gMnSc3
podařilo	podařit	k5eAaPmAgNnS
obsadit	obsadit	k5eAaPmF
celkově	celkově	k6eAd1
5	#num#	k4
<g/>
.	.	kIx.
místo	místo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Sezóna	sezóna	k1gFnSc1
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
</s>
<s>
I	i	k9
v	v	k7c6
této	tento	k3xDgFnSc6
sezóně	sezóna	k1gFnSc6
se	se	k3xPyFc4
odehrál	odehrát	k5eAaPmAgMnS
dost	dost	k6eAd1
pikantní	pikantní	k2eAgInSc4d1
přestup	přestup	k1gInSc4
<g/>
,	,	kIx,
tentokrát	tentokrát	k6eAd1
z	z	k7c2
Opavy	Opava	k1gFnSc2
přišel	přijít	k5eAaPmAgMnS
Tomáš	Tomáš	k1gMnSc1
Smola	Smola	k1gMnSc1
<g/>
,	,	kIx,
dále	daleko	k6eAd2
pak	pak	k6eAd1
Rudolf	Rudolf	k1gMnSc1
Reiter	Reiter	k1gMnSc1
z	z	k7c2
Bohemians	Bohemiansa	k1gFnPc2
1905	#num#	k4
a	a	k8xC
Milan	Milan	k1gMnSc1
Lalkovič	Lalkovič	k1gMnSc1
z	z	k7c2
Olomouce	Olomouc	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
hostování	hostování	k1gNnSc2
se	se	k3xPyFc4
vrátil	vrátit	k5eAaPmAgMnS
Dyjan	Dyjan	k1gMnSc1
Carlos	Carlos	k1gMnSc1
de	de	k?
Azevedo	Azevedo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
na	na	k7c6
hostování	hostování	k1gNnSc6
odešli	odejít	k5eAaPmAgMnP
bratři	bratr	k1gMnPc1
Šašinkové	Šašinkový	k2eAgFnPc1d1
(	(	kIx(
<g/>
Jakub	Jakub	k1gMnSc1
do	do	k7c2
Karviné	Karviná	k1gFnSc2
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
na	na	k7c4
Slovácko	Slovácko	k1gNnSc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Oleksandr	Oleksandr	k1gInSc1
Azackyj	Azackyj	k1gFnSc4
do	do	k7c2
Zlína	Zlín	k1gInSc2
a	a	k8xC
Arťom	Arťom	k1gInSc4
Mešaninov	Mešaninovo	k1gNnPc2
do	do	k7c2
FK	FK	kA
Baltika	Baltika	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Úspěchy	úspěch	k1gInPc1
A	a	k8xC
<g/>
–	–	k?
<g/>
týmu	tým	k1gInSc2
</s>
<s>
Kontinentální	kontinentální	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
</s>
<s>
Středoevropský	středoevropský	k2eAgInSc1d1
pohár	pohár	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
TJ	tj	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
OKD	OKD	kA
<g/>
:	:	kIx,
1989	#num#	k4
</s>
<s>
Domácí	domácí	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
</s>
<s>
Československá	československý	k2eAgFnSc1d1
nejvyšší	vysoký	k2eAgFnSc1d3
fotbalová	fotbalový	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
TJ	tj	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
OKD	OKD	kA
<g/>
:	:	kIx,
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
<g/>
,	,	kIx,
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
<g/>
,	,	kIx,
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
–	–	k?
DSO	DSO	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
<g/>
:	:	kIx,
1954	#num#	k4
<g/>
;	;	kIx,
TJ	tj	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
OKD	OKD	kA
<g/>
:	:	kIx,
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
<g/>
,	,	kIx,
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
<g/>
,	,	kIx,
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
<g/>
,	,	kIx,
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
<g/>
,	,	kIx,
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
</s>
<s>
–	–	k?
TJ	tj	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
<g/>
:	:	kIx,
1962	#num#	k4
<g/>
/	/	kIx~
<g/>
63	#num#	k4
</s>
<s>
Česká	český	k2eAgFnSc1d1
nejvyšší	vysoký	k2eAgFnSc1d3
fotbalová	fotbalový	k2eAgFnSc1d1
soutěž	soutěž	k1gFnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
Ispat	Ispat	k1gInSc1
<g/>
:	:	kIx,
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
</s>
<s>
–	–	k?
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
<g/>
:	:	kIx,
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
</s>
<s>
Československý	československý	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
TJ	tj	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
OKD	OKD	kA
<g/>
:	:	kIx,
1973	#num#	k4
<g/>
,	,	kIx,
1978	#num#	k4
<g/>
;	;	kIx,
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
<g/>
:	:	kIx,
1991	#num#	k4
</s>
<s>
Český	český	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
×	×	k?
vítěz	vítěz	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
–	–	k?
TJ	tj	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
OKD	OKD	kA
<g/>
:	:	kIx,
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
<g/>
,	,	kIx,
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
<g/>
,	,	kIx,
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
<g/>
;	;	kIx,
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
<g/>
:	:	kIx,
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
<g/>
;	;	kIx,
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
Ispat	Ispat	k1gInSc1
<g/>
:	:	kIx,
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
</s>
<s>
Fanoušci	Fanoušek	k1gMnPc1
</s>
<s>
Baníkovští	Baníkovský	k2eAgMnPc1d1
"	"	kIx"
<g/>
chachaři	chachar	k1gMnPc1
<g/>
"	"	kIx"
při	při	k7c6
zápasu	zápas	k1gInSc6
s	s	k7c7
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Brno	Brno	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s>
Domácí	domácí	k2eAgInPc1d1
zápasy	zápas	k1gInPc1
Baníku	Baník	k1gInSc2
patří	patřit	k5eAaImIp3nP
dlouhodobě	dlouhodobě	k6eAd1
mezi	mezi	k7c4
nejnavštěvovanější	navštěvovaný	k2eAgNnSc4d3
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvyšší	vysoký	k2eAgFnSc1d3
návštěvnost	návštěvnost	k1gFnSc1
Baník	Baník	k1gInSc1
zaznamenal	zaznamenat	k5eAaPmAgMnS
v	v	k7c6
mistrovské	mistrovský	k2eAgFnSc6d1
sezóně	sezóna	k1gFnSc6
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
domácí	domácí	k2eAgInPc1d1
zápasy	zápas	k1gInPc1
průměrně	průměrně	k6eAd1
navštěvovalo	navštěvovat	k5eAaImAgNnS
přes	přes	k7c4
15	#num#	k4
tisíc	tisíc	k4xCgInPc2
diváků	divák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Průměrná	průměrný	k2eAgFnSc1d1
návštěvnost	návštěvnost	k1gFnSc1
dnes	dnes	k6eAd1
<g/>
,	,	kIx,
se	se	k3xPyFc4
pohybuje	pohybovat	k5eAaImIp3nS
okolo	okolo	k7c2
10000	#num#	k4
diváků	divák	k1gMnPc2
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
velkých	velký	k2eAgNnPc2d1
utkání	utkání	k1gNnPc2
s	s	k7c7
rivaly	rival	k1gMnPc7
<g/>
,	,	kIx,
nebo	nebo	k8xC
atraktivními	atraktivní	k2eAgInPc7d1
kluby	klub	k1gInPc7
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
je	být	k5eAaImIp3nS
většinou	většinou	k6eAd1
vyprodáno	vyprodat	k5eAaPmNgNnS
<g/>
,	,	kIx,
popřípadě	popřípadě	k6eAd1
návštěva	návštěva	k1gFnSc1
minimálně	minimálně	k6eAd1
přesahuje	přesahovat	k5eAaImIp3nS
hranici	hranice	k1gFnSc4
10	#num#	k4
tisíc	tisíc	k4xCgInPc2
diváků	divák	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ač	ač	k8xS
číselně	číselně	k6eAd1
nejde	jít	k5eNaImIp3nS
o	o	k7c4
nejvyšší	vysoký	k2eAgNnPc4d3
čísla	číslo	k1gNnPc4
<g/>
,	,	kIx,
domácí	domácí	k2eAgInPc4d1
zápasy	zápas	k1gInPc4
Baníku	Baník	k1gInSc2
jsou	být	k5eAaImIp3nP
proslulé	proslulý	k2eAgFnPc1d1
atraktivní	atraktivní	k2eAgFnPc1d1
a	a	k8xC
bouřlivou	bouřlivý	k2eAgFnSc7d1
diváckou	divácký	k2eAgFnSc7d1
kulisou	kulisa	k1gFnSc7
a	a	k8xC
samotní	samotný	k2eAgMnPc1d1
fanoušci	fanoušek	k1gMnPc1
Ultras	Ultrasa	k1gFnPc2
Baníku	Baník	k1gInSc2
se	se	k3xPyFc4
řadí	řadit	k5eAaImIp3nS
mezi	mezi	k7c4
nejvěrnější	věrný	k2eAgNnSc4d3
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c6
začátku	začátek	k1gInSc6
každého	každý	k3xTgInSc2
zápasu	zápas	k1gInSc2
ostravští	ostravský	k2eAgMnPc1d1
fanoušci	fanoušek	k1gMnPc1
<g/>
,	,	kIx,
zvaní	zvaný	k2eAgMnPc1d1
„	„	k?
<g/>
Chachaři	chachar	k1gMnPc1
<g/>
“	“	k?
<g/>
,	,	kIx,
zpívají	zpívat	k5eAaImIp3nP
klubovou	klubový	k2eAgFnSc4d1
hymnu	hymna	k1gFnSc4
„	„	k?
<g/>
Baníčku	Baníček	k1gInSc2
<g/>
,	,	kIx,
my	my	k3xPp1nPc1
jsme	být	k5eAaImIp1nP
s	s	k7c7
tebou	ty	k3xPp2nSc7
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Mezi	mezi	k7c4
tradiční	tradiční	k2eAgInPc4d1
pokřiky	pokřik	k1gInPc4
fandů	fandů	k?
Baníku	Baník	k1gInSc2
patři	patřit	k5eAaImRp2nS
„	„	k?
<g/>
Baník	baník	k1gMnSc1
<g/>
,	,	kIx,
pičo	pičo	k1gMnSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
repertoár	repertoár	k1gInSc1
chorálů	chorál	k1gInPc2
však	však	k9
čítá	čítat	k5eAaImIp3nS
množství	množství	k1gNnSc1
dalších	další	k2eAgInPc2d1
popěvků	popěvek	k1gInPc2
a	a	k8xC
je	být	k5eAaImIp3nS
pravidelně	pravidelně	k6eAd1
doplňován	doplňovat	k5eAaImNgInS
<g/>
.	.	kIx.
</s>
<s>
Fanoušci	Fanoušek	k1gMnPc1
Baníku	Baník	k1gInSc2
podnikají	podnikat	k5eAaImIp3nP
<g/>
,	,	kIx,
také	také	k9
výjezdy	výjezd	k1gInPc1
na	na	k7c4
všechny	všechen	k3xTgInPc4
venkovní	venkovní	k2eAgInPc4d1
zápasy	zápas	k1gInPc4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
jejich	jejich	k3xOp3gFnSc4
početní	početní	k2eAgFnSc4d1
účast	účast	k1gFnSc4
na	na	k7c6
venkovních	venkovní	k2eAgInPc6d1
zápasech	zápas	k1gInPc6
dlouhodobě	dlouhodobě	k6eAd1
patří	patřit	k5eAaImIp3nS
k	k	k7c3
vůbec	vůbec	k9
těm	ten	k3xDgInPc3
nejlepším	dobrý	k2eAgInPc3d3
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Baníkovští	Baníkovský	k2eAgMnPc1d1
fanoušci	fanoušek	k1gMnPc1
se	se	k3xPyFc4
přátelí	přátelit	k5eAaImIp3nP
s	s	k7c7
polským	polský	k2eAgNnSc7d1
GKS	GKS	kA
Katowice	Katowika	k1gFnSc6
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
oslavili	oslavit	k5eAaPmAgMnP
již	jenž	k3xRgMnPc1
10	#num#	k4
let	léto	k1gNnPc2
této	tento	k3xDgFnSc2
družby	družba	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInSc1d1
klub	klub	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
měl	mít	k5eAaImAgInS
vazby	vazba	k1gFnPc4
s	s	k7c7
ostravským	ostravský	k2eAgMnSc7d1
Baníkem	baník	k1gMnSc7
<g/>
,	,	kIx,
byl	být	k5eAaImAgInS
Spartak	Spartak	k1gInSc1
Trnava	Trnava	k1gFnSc1
<g/>
,	,	kIx,
družba	družba	k1gFnSc1
však	však	k9
již	již	k6eAd1
skončila	skončit	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s>
Fanoušci	Fanoušek	k1gMnPc1
Baníku	Baník	k1gInSc2
mají	mít	k5eAaImIp3nP
však	však	k9
i	i	k9
určitou	určitý	k2eAgFnSc4d1
negativní	negativní	k2eAgFnSc4d1
reputaci	reputace	k1gFnSc4
kvůli	kvůli	k7c3
konfliktnímu	konfliktní	k2eAgNnSc3d1
chování	chování	k1gNnSc3
<g/>
,	,	kIx,
rvačkách	rvačka	k1gFnPc6
na	na	k7c6
stadionech	stadion	k1gInPc6
i	i	k9
mimo	mimo	k7c4
ně	on	k3xPp3gNnSc4
<g/>
,	,	kIx,
to	ten	k3xDgNnSc1
se	se	k3xPyFc4
však	však	k9
týká	týkat	k5eAaImIp3nS
radikálního	radikální	k2eAgNnSc2d1
jádra	jádro	k1gNnSc2
Baníku	Baník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Toto	tento	k3xDgNnSc1
nejtvrdší	tvrdý	k2eAgNnSc1d3
jádro	jádro	k1gNnSc1
je	být	k5eAaImIp3nS
<g/>
,	,	kIx,
také	také	k9
mnohdy	mnohdy	k6eAd1
tvořeno	tvořit	k5eAaImNgNnS
i	i	k8xC
polskými	polský	k2eAgMnPc7d1
příznivci	příznivec	k1gMnPc7
družebního	družební	k2eAgInSc2d1
klubu	klub	k1gInSc2
GKS	GKS	kA
Katowice	Katowice	k1gInPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Přestupy	přestup	k1gInPc1
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Galásek	Galásek	k1gMnSc1
<g/>
,	,	kIx,
jeden	jeden	k4xCgMnSc1
ze	z	k7c2
známých	známý	k2eAgMnPc2d1
ostravských	ostravský	k2eAgMnPc2d1
odchovanců	odchovanec	k1gMnPc2
<g/>
.	.	kIx.
<g/>
Baník	baník	k1gMnSc1
patří	patřit	k5eAaImIp3nS
k	k	k7c3
největším	veliký	k2eAgMnPc3d3
dodavatelům	dodavatel	k1gMnPc3
reprezentačních	reprezentační	k2eAgInPc2d1
hráčů	hráč	k1gMnPc2
v	v	k7c6
zemi	zem	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
posledních	poslední	k2eAgNnPc2d1
15	#num#	k4
let	léto	k1gNnPc2
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
stabilnímu	stabilní	k2eAgInSc3d1
nárůstu	nárůst	k1gInSc3
mladých	mladý	k2eAgMnPc2d1
hráčů	hráč	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yRgMnPc1,k3yQgMnPc1
se	se	k3xPyFc4
po	po	k7c6
odchodu	odchod	k1gInSc6
pohybují	pohybovat	k5eAaImIp3nP
v	v	k7c6
zahraničních	zahraniční	k2eAgFnPc6d1
soutěžích	soutěž	k1gFnPc6
<g/>
,	,	kIx,
ať	ať	k8xC,k8xS
už	už	k6eAd1
jde	jít	k5eAaImIp3nS
o	o	k7c4
belgickou	belgický	k2eAgFnSc4d1
ligu	liga	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
Bundesligu	bundesliga	k1gFnSc4
či	či	k8xC
Serii	serie	k1gFnSc4
A.	A.	kA
</s>
<s>
Mezi	mezi	k7c4
největší	veliký	k2eAgInPc4d3
přestupy	přestup	k1gInPc4
patří	patřit	k5eAaImIp3nS
přestup	přestup	k1gInSc1
Milana	Milan	k1gMnSc2
Baroše	Baroš	k1gMnSc2
do	do	k7c2
Liverpool	Liverpool	k1gInSc1
FC	FC	kA
za	za	k7c4
180	#num#	k4
milionů	milion	k4xCgInPc2
korun	koruna	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
přestup	přestup	k1gInSc1
Matěje	Matěj	k1gMnSc2
Vydry	Vydra	k1gMnSc2
do	do	k7c2
Udinese	Udinese	k1gFnSc2
Calcio	Calcio	k6eAd1
za	za	k7c4
100	#num#	k4
milionů	milion	k4xCgInPc2
korun	koruna	k1gFnPc2
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
či	či	k8xC
transfer	transfer	k1gInSc4
Václava	Václav	k1gMnSc2
Svěrkoše	Svěrkoš	k1gMnSc2
do	do	k7c2
francouzského	francouzský	k2eAgInSc2d1
FC	FC	kA
Sochaux	Sochaux	k1gInSc1
za	za	k7c4
nezveřejněnou	zveřejněný	k2eNgFnSc4d1
částku	částka	k1gFnSc4
(	(	kIx(
<g/>
odhadem	odhad	k1gInSc7
za	za	k7c4
65	#num#	k4
milionů	milion	k4xCgInPc2
korun	koruna	k1gFnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Několik	několik	k4yIc1
hráčů	hráč	k1gMnPc2
přestoupilo	přestoupit	k5eAaPmAgNnS
v	v	k7c6
minulosti	minulost	k1gFnSc6
do	do	k7c2
Sparty	Sparta	k1gFnSc2
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
např.	např.	kA
Zdeněk	Zdeněk	k1gMnSc1
Pospěch	pospěch	k1gInSc1
<g/>
,	,	kIx,
Miroslav	Miroslav	k1gMnSc1
Matušovič	Matušovič	k1gMnSc1
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
Řepka	řepka	k1gFnSc1
<g/>
,	,	kIx,
Libor	Libor	k1gMnSc1
Sionko	Sionko	k1gNnSc1
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
Holzer	Holzer	k1gMnSc1
a	a	k8xC
další	další	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Zajímavosti	zajímavost	k1gFnPc1
</s>
<s>
Do	do	k7c2
Baníku	Baník	k1gInSc2
se	se	k3xPyFc4
vrátili	vrátit	k5eAaPmAgMnP
jeho	jeho	k3xOp3gMnPc1
slavní	slavný	k2eAgMnPc1d1
odchovanci	odchovanec	k1gMnPc1
Václav	Václav	k1gMnSc1
Svěrkoš	Svěrkoš	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
si	se	k3xPyFc3
dvakrát	dvakrát	k6eAd1
utrhl	utrhnout	k5eAaPmAgMnS
vazy	vaz	k1gInPc4
a	a	k8xC
Marek	Marek	k1gMnSc1
Jankulovski	Jankulovsk	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
pro	pro	k7c4
stejné	stejný	k2eAgNnSc4d1
zranění	zranění	k1gNnPc4
odehrál	odehrát	k5eAaPmAgInS
za	za	k7c4
Baník	Baník	k1gInSc4
po	po	k7c6
svém	svůj	k3xOyFgInSc6
návratu	návrat	k1gInSc6
pouhých	pouhý	k2eAgInPc2d1
7	#num#	k4
minut	minuta	k1gFnPc2
a	a	k8xC
ukončil	ukončit	k5eAaPmAgInS
hráčskou	hráčský	k2eAgFnSc4d1
kariéru	kariéra	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Zatím	zatím	k6eAd1
posledním	poslední	k2eAgMnSc7d1
slavným	slavný	k2eAgMnSc7d1
ostravským	ostravský	k2eAgMnSc7d1
navrátilcem	navrátilec	k1gMnSc7
je	být	k5eAaImIp3nS
Milan	Milan	k1gMnSc1
Baroš	Baroš	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
ukončil	ukončit	k5eAaPmAgInS
své	svůj	k3xOyFgNnSc4
angažmá	angažmá	k1gNnSc4
v	v	k7c6
FC	FC	kA
Slovan	Slovan	k1gInSc1
Liberec	Liberec	k1gInSc1
a	a	k8xC
vrátil	vrátit	k5eAaPmAgInS
se	se	k3xPyFc4
již	již	k9
potřetí	potřetí	k4xO
domů	dům	k1gInPc2
na	na	k7c4
sezóny	sezóna	k1gFnPc4
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
-	-	kIx~
<g/>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Soupiska	soupiska	k1gFnSc1
</s>
<s>
Aktuální	aktuální	k2eAgInPc1d1
k	k	k7c3
25	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
2021	#num#	k4
</s>
<s>
#	#	kIx~
<g/>
PoziceHráč	PoziceHráč	k1gInSc1
</s>
<s>
1	#num#	k4
</s>
<s>
B	B	kA
</s>
<s>
Radovan	Radovan	k1gMnSc1
Murin	Murin	k1gMnSc1
</s>
<s>
5	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Adam	Adam	k1gMnSc1
Jánoš	Jánoš	k1gMnSc1
</s>
<s>
6	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Daniel	Daniel	k1gMnSc1
Tetour	Tetour	k1gMnSc1
</s>
<s>
7	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Martin	Martin	k1gMnSc1
Fillo	Filla	k1gMnSc5
</s>
<s>
8	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Jakub	Jakub	k1gMnSc1
Drozd	Drozd	k1gMnSc1
</s>
<s>
9	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
David	David	k1gMnSc1
Buchta	Buchta	k1gMnSc1
</s>
<s>
11	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Nemanja	Nemanj	k2eAgFnSc1d1
Kuzmanović	Kuzmanović	k1gFnSc1
</s>
<s>
14	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
José	José	k6eAd1
Mena	Mena	k1gMnSc1
Rodríguez	Rodríguez	k1gMnSc1
</s>
<s>
15	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Patrizio	Patrizio	k1gMnSc1
Stronati	Stronati	k1gMnSc1
</s>
<s>
16	#num#	k4
</s>
<s>
B	B	kA
</s>
<s>
Jan	Jan	k1gMnSc1
Laštůvka	laštůvka	k1gFnSc1
</s>
<s>
17	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Muhammed	Muhammed	k1gMnSc1
Sanneh	Sanneh	k1gMnSc1
</s>
<s>
18	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Zajíc	Zajíc	k1gMnSc1
</s>
<s>
#	#	kIx~
<g/>
PoziceHráč	PoziceHráč	k1gInSc1
</s>
<s>
20	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Jakub	Jakub	k1gMnSc1
Pokorný	Pokorný	k1gMnSc1
</s>
<s>
21	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Daniel	Daniel	k1gMnSc1
Holzer	Holzer	k1gMnSc1
</s>
<s>
22	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Filip	Filip	k1gMnSc1
Kaloč	Kaloč	k1gMnSc1
</s>
<s>
23	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Svozil	Svozil	k1gMnSc1
</s>
<s>
24	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Jan	Jan	k1gMnSc1
Juroška	Jurošek	k1gMnSc2
</s>
<s>
25	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Jiří	Jiří	k1gMnSc1
Fleišman	Fleišman	k1gMnSc1
</s>
<s>
30	#num#	k4
</s>
<s>
B	B	kA
</s>
<s>
Viktor	Viktor	k1gMnSc1
Budinský	Budinský	k2eAgMnSc1d1
</s>
<s>
31	#num#	k4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Ondřej	Ondřej	k1gMnSc1
Šašinka	Šašinka	k1gFnSc1
</s>
<s>
77	#num#	k4
</s>
<s>
O	o	k7c6
</s>
<s>
Gigli	Gigl	k1gMnSc5
Ndefe	Ndef	k1gMnSc5
</s>
<s>
91	#num#	k4
</s>
<s>
Z	z	k7c2
</s>
<s>
Dyjan	Dyjan	k1gMnSc1
Carlos	Carlos	k1gMnSc1
de	de	k?
Azevedo	Azevedo	k1gNnSc4
</s>
<s>
Z	z	k7c2
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Hykel	Hykel	k1gMnSc1
</s>
<s>
Ú	Ú	kA
</s>
<s>
Jakub	Jakub	k1gMnSc1
Šašinka	Šašinka	k1gFnSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
Yira	Yir	k2eAgFnSc1d1
Sor	Sor	k1gFnSc1
</s>
<s>
Změny	změna	k1gFnPc1
v	v	k7c6
kádru	kádr	k1gInSc6
v	v	k7c6
zimním	zimní	k2eAgNnSc6d1
přestupovém	přestupový	k2eAgNnSc6d1
období	období	k1gNnSc6
2021	#num#	k4
</s>
<s>
Příchody	příchod	k1gInPc1
</s>
<s>
Poz	Poz	k?
<g/>
.	.	kIx.
</s>
<s>
Nár	Nár	k?
<g/>
.	.	kIx.
</s>
<s>
Hráč	hráč	k1gMnSc1
</s>
<s>
Věk	věk	k1gInSc1
</s>
<s>
Odkud	odkud	k6eAd1
přišel	přijít	k5eAaPmAgMnS
</s>
<s>
Typ	typ	k1gInSc1
</s>
<s>
ZTomáš	ZTomat	k5eAaPmIp2nS,k5eAaBmIp2nS,k5eAaImIp2nS
Hykel	Hykel	k1gInSc4
<g/>
24	#num#	k4
FC	FC	kA
Frýdek-Místeknávrat	Frýdek-Místeknávrat	k1gInSc1
z	z	k7c2
hostování	hostování	k1gNnSc2
</s>
<s>
ÚJakub	ÚJakub	k1gInSc1
Šašinka	Šašinka	k1gFnSc1
<g/>
25	#num#	k4
FK	FK	kA
Blanskonávrat	Blanskonávrat	k1gInSc1
z	z	k7c2
hostování	hostování	k1gNnSc2
</s>
<s>
BFrantišek	BFrantišek	k1gMnSc1
Chmiel	Chmiel	k1gMnSc1
<g/>
23	#num#	k4
TJL	TJL	kA
Petrovicenávrat	Petrovicenávrat	k1gInSc1
z	z	k7c2
hostování	hostování	k1gNnSc2
</s>
<s>
ZLukáš	ZLukat	k5eAaImIp2nS,k5eAaPmIp2nS,k5eAaBmIp2nS
Kania	Kania	k1gFnSc1
<g/>
23	#num#	k4
FK	FK	kA
Blanskonávrat	Blanskonávrat	k1gInSc1
z	z	k7c2
hostování	hostování	k1gNnSc2
</s>
<s>
OMuhammed	OMuhammed	k1gMnSc1
Sanneh	Sanneh	k1gMnSc1
<g/>
20	#num#	k4
Paide	Paid	k1gInSc5
Linnameeskondhostování	Linnameeskondhostování	k1gNnSc1
</s>
<s>
OGigli	OGigl	k1gMnSc5
Ndefe	Ndef	k1gMnSc5
<g/>
26	#num#	k4
MFK	MFK	kA
Karviná	Karviná	k1gFnSc1
</s>
<s>
ZYira	ZYira	k1gFnSc1
Sol	sol	k1gInSc1
<g/>
20	#num#	k4
36	#num#	k4
Lion	Lion	k1gMnSc1
FC	FC	kA
</s>
<s>
Odchody	odchod	k1gInPc4
</s>
<s>
Poz	Poz	k?
<g/>
.	.	kIx.
</s>
<s>
Nár	Nár	k?
<g/>
.	.	kIx.
</s>
<s>
Hráč	hráč	k1gMnSc1
</s>
<s>
Věk	věk	k1gInSc1
</s>
<s>
Kam	kam	k6eAd1
odešel	odejít	k5eAaPmAgMnS
</s>
<s>
Typ	typ	k1gInSc1
</s>
<s>
ÚMartin	ÚMartin	k2eAgInSc1d1
Macej	Macej	k1gInSc1
<g/>
23	#num#	k4
FK	FK	kA
Teplicehostování	Teplicehostování	k1gNnSc2
</s>
<s>
ÚRudolf	ÚRudolf	k1gMnSc1
Reiter	Reiter	k1gMnSc1
<g/>
26	#num#	k4
FC	FC	kA
Zbrojovka	zbrojovka	k1gFnSc1
Brnopokračování	Brnopokračování	k1gNnPc2
hostování	hostování	k1gNnSc2
</s>
<s>
ZMilan	ZMilan	k1gMnSc1
Lalkovič	Lalkovič	k1gMnSc1
<g/>
28	#num#	k4
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FK	FK	kA
Příbramkonec	Příbramkonec	k1gInSc4
smlouvy	smlouva	k1gFnSc2
</s>
<s>
ZMilan	ZMilan	k1gMnSc1
Jirásek	Jirásek	k1gMnSc1
<g/>
28	#num#	k4
FK	FK	kA
Mladá	mladá	k1gFnSc1
Boleslav	Boleslav	k1gMnSc1
</s>
<s>
ZLukáš	ZLukat	k5eAaPmIp2nS,k5eAaBmIp2nS,k5eAaImIp2nS
Kania	Kania	k1gFnSc1
<g/>
23	#num#	k4
SFC	SFC	kA
Opavahostování	Opavahostování	k1gNnSc2
</s>
<s>
OOleksandr	OOleksandr	k1gInSc1
Azackyj	Azackyj	k1gFnSc1
<g/>
26	#num#	k4
Dinamo	Dinama	k1gFnSc5
Batumipřestup	Batumipřestup	k1gInSc4
</s>
<s>
OOndřej	OOndřet	k5eAaImRp2nS,k5eAaPmRp2nS
Chvěja	Chvěja	k1gFnSc1
<g/>
22	#num#	k4
FK	FK	kA
Pohronie	Pohronie	k1gFnSc2
</s>
<s>
BFrantišek	BFrantišek	k1gMnSc1
Chmiel	Chmiel	k1gMnSc1
<g/>
23	#num#	k4
FK	FK	kA
Blanskohostování	Blanskohostování	k1gNnSc2
</s>
<s>
ÚRoman	ÚRoman	k1gMnSc1
Potočný	Potočný	k?
<g/>
29	#num#	k4
FC	FC	kA
Fastav	Fastav	k1gFnSc4
Zlínhostování	Zlínhostování	k1gNnSc3
</s>
<s>
ZLukáš	ZLukat	k5eAaImIp2nS,k5eAaBmIp2nS,k5eAaPmIp2nS
Cienciala	Cienciala	k1gFnSc1
<g/>
20	#num#	k4
FK	FK	kA
Fotbal	fotbal	k1gInSc1
Třinechostování	Třinechostování	k1gNnSc1
</s>
<s>
ÚMuhamed	ÚMuhamed	k1gMnSc1
Tijani	Tijaň	k1gFnSc3
<g/>
20	#num#	k4
FK	FK	kA
Fotbal	fotbal	k1gInSc1
Třinechostování	Třinechostování	k1gNnSc1
</s>
<s>
B-tým	B-té	k1gNnSc7
</s>
<s>
Soupiska	soupiska	k1gFnSc1
rezervního	rezervní	k2eAgInSc2d1
týmu	tým	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
nastupuje	nastupovat	k5eAaImIp3nS
v	v	k7c6
MSFL	MSFL	kA
</s>
<s>
Aktuální	aktuální	k2eAgInPc1d1
k	k	k7c3
23	#num#	k4
<g/>
.	.	kIx.
srpen	srpen	k1gInSc1
2020	#num#	k4
</s>
<s>
#	#	kIx~
<g/>
PoziceHráč	PoziceHráč	k1gInSc4
</s>
<s>
B	B	kA
</s>
<s>
Michael	Michael	k1gMnSc1
Gergela	Gergel	k1gMnSc2
</s>
<s>
B	B	kA
</s>
<s>
Patrik	Patrik	k1gMnSc1
Jílovec	jílovec	k1gInSc4
</s>
<s>
B	B	kA
</s>
<s>
Radovan	Radovan	k1gMnSc1
Murin	Murin	k1gMnSc1
</s>
<s>
B	B	kA
</s>
<s>
Ladislav	Ladislav	k1gMnSc1
Kovařík	Kovařík	k1gMnSc1
</s>
<s>
O	o	k7c6
</s>
<s>
Jarmil	Jarmila	k1gFnPc2
Blahuta	Blahut	k2eAgMnSc4d1
</s>
<s>
O	o	k7c6
</s>
<s>
Jan	Jan	k1gMnSc1
Fulnek	Fulnek	k1gMnSc1
</s>
<s>
O	o	k7c6
</s>
<s>
Dominik	Dominik	k1gMnSc1
Hasala	Hasal	k1gMnSc2
</s>
<s>
O	o	k7c6
</s>
<s>
Lukáš	Lukáš	k1gMnSc1
Lehnert	Lehnert	k1gMnSc1
</s>
<s>
O	o	k7c6
</s>
<s>
Jakub	Jakub	k1gMnSc1
Tamajka	Tamajka	k1gFnSc1
</s>
<s>
O	o	k7c6
</s>
<s>
Michal	Michal	k1gMnSc1
Velner	Velner	k1gMnSc1
</s>
<s>
O	o	k7c6
</s>
<s>
Roman	Roman	k1gMnSc1
Zálešák	Zálešák	k1gMnSc1
</s>
<s>
#	#	kIx~
<g/>
PoziceHráč	PoziceHráč	k1gInSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
Dyjan	Dyjan	k1gMnSc1
Carlos	Carlos	k1gMnSc1
de	de	k?
Azevedo	Azevedo	k1gNnSc4
</s>
<s>
Z	z	k7c2
</s>
<s>
Petr	Petr	k1gMnSc1
Jaroň	Jaroň	k1gMnSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
Marek	Marek	k1gMnSc1
Kostka	Kostka	k1gMnSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
Samuel	Samuel	k1gMnSc1
Kulig	Kulig	k1gMnSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
Jan	Jan	k1gMnSc1
Martiník	Martiník	k1gMnSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
Aleš	Aleš	k1gMnSc1
Mooc	Mooc	k1gInSc4
</s>
<s>
Z	z	k7c2
</s>
<s>
Milan	Milan	k1gMnSc1
Urban	Urban	k1gMnSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
Adam	Adam	k1gMnSc1
Zajíček	Zajíček	k1gMnSc1
</s>
<s>
Ú	Ú	kA
</s>
<s>
Timofej	Timofat	k5eAaPmRp2nS
Barkov	Barkov	k1gInSc4
</s>
<s>
Ú	Ú	kA
</s>
<s>
Radovan	Radovan	k1gMnSc1
Lokša	Lokša	k1gMnSc1
</s>
<s>
Ú	Ú	kA
</s>
<s>
Vojtěch	Vojtěch	k1gMnSc1
Wojatschke	Wojatschk	k1gFnSc2
</s>
<s>
Umístění	umístění	k1gNnSc1
v	v	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
sezonách	sezona	k1gFnPc6
</s>
<s>
Stručný	stručný	k2eAgInSc1d1
přehled	přehled	k1gInSc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1923	#num#	k4
<g/>
:	:	kIx,
III	III	kA
<g/>
.	.	kIx.
třída	třída	k1gFnSc1
Moravskoslezské	moravskoslezský	k2eAgFnSc2d1
župy	župa	k1gFnSc2
</s>
<s>
1924	#num#	k4
<g/>
:	:	kIx,
II	II	kA
<g/>
.	.	kIx.
třída	třída	k1gFnSc1
Moravskoslezské	moravskoslezský	k2eAgFnSc2d1
župy	župa	k1gFnSc2
</s>
<s>
1925	#num#	k4
<g/>
–	–	k?
<g/>
1930	#num#	k4
<g/>
:	:	kIx,
I.	I.	kA
třída	třída	k1gFnSc1
Moravskoslezské	moravskoslezský	k2eAgFnPc1d1
župy	župa	k1gFnPc1
</s>
<s>
1931	#num#	k4
<g/>
–	–	k?
<g/>
1932	#num#	k4
<g/>
:	:	kIx,
II	II	kA
<g/>
.	.	kIx.
třída	třída	k1gFnSc1
Moravskoslezské	moravskoslezský	k2eAgFnSc2d1
župy	župa	k1gFnSc2
</s>
<s>
1932	#num#	k4
<g/>
–	–	k?
<g/>
1934	#num#	k4
<g/>
:	:	kIx,
I.	I.	kA
třída	třída	k1gFnSc1
Moravskoslezské	moravskoslezský	k2eAgFnPc1d1
župy	župa	k1gFnPc1
</s>
<s>
1934	#num#	k4
<g/>
–	–	k?
<g/>
1937	#num#	k4
<g/>
:	:	kIx,
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
divize	divize	k1gFnSc1
</s>
<s>
1937	#num#	k4
<g/>
–	–	k?
<g/>
1938	#num#	k4
<g/>
:	:	kIx,
Státní	státní	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
1939	#num#	k4
<g/>
–	–	k?
<g/>
1940	#num#	k4
<g/>
:	:	kIx,
Národní	národní	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
1940	#num#	k4
<g/>
–	–	k?
<g/>
1943	#num#	k4
<g/>
:	:	kIx,
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
divize	divize	k1gFnSc1
</s>
<s>
1943	#num#	k4
<g/>
–	–	k?
<g/>
1944	#num#	k4
<g/>
:	:	kIx,
Národní	národní	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
1945	#num#	k4
<g/>
–	–	k?
<g/>
1946	#num#	k4
<g/>
:	:	kIx,
Státní	státní	k2eAgFnSc1d1
liga	liga	k1gFnSc1
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
</s>
<s>
1946	#num#	k4
<g/>
–	–	k?
<g/>
1948	#num#	k4
<g/>
:	:	kIx,
Státní	státní	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
1949	#num#	k4
<g/>
:	:	kIx,
Celostátní	celostátní	k2eAgNnSc4d1
československé	československý	k2eAgNnSc4d1
mistrovství	mistrovství	k1gNnSc4
</s>
<s>
1950	#num#	k4
<g/>
:	:	kIx,
Celostátní	celostátní	k2eAgNnSc4d1
československé	československý	k2eAgNnSc4d1
mistrovství	mistrovství	k1gNnSc4
II	II	kA
</s>
<s>
1951	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
<g/>
:	:	kIx,
Mistrovství	mistrovství	k1gNnSc4
československé	československý	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
1953	#num#	k4
<g/>
–	–	k?
<g/>
1955	#num#	k4
<g/>
:	:	kIx,
Přebor	přebor	k1gInSc1
československé	československý	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
1956	#num#	k4
<g/>
–	–	k?
<g/>
1966	#num#	k4
<g/>
:	:	kIx,
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1966	#num#	k4
<g/>
–	–	k?
<g/>
1967	#num#	k4
<g/>
:	:	kIx,
2	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
</s>
<s>
1967	#num#	k4
<g/>
–	–	k?
<g/>
1993	#num#	k4
<g/>
:	:	kIx,
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
(	(	kIx(
<g/>
ČSR	ČSR	kA
<g/>
)	)	kIx)
</s>
<s>
1993	#num#	k4
<g/>
–	–	k?
<g/>
2016	#num#	k4
<g/>
:	:	kIx,
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
(	(	kIx(
<g/>
ČR	ČR	kA
<g/>
)	)	kIx)
</s>
<s>
2016	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
:	:	kIx,
Fotbalová	fotbalový	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
2017	#num#	k4
<g/>
–	–	k?
:	:	kIx,
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1
ročníky	ročník	k1gInPc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
Z	Z	kA
-	-	kIx~
zápasy	zápas	k1gInPc1
<g/>
,	,	kIx,
V	v	k7c6
-	-	kIx~
výhry	výhra	k1gFnPc4
<g/>
,	,	kIx,
R	R	kA
-	-	kIx~
remízy	remíz	k1gInPc1
<g/>
,	,	kIx,
P	P	kA
-	-	kIx~
porážky	porážka	k1gFnSc2
<g/>
,	,	kIx,
VG	VG	kA
-	-	kIx~
vstřelené	vstřelený	k2eAgInPc1d1
góly	gól	k1gInPc1
<g/>
,	,	kIx,
OG	OG	kA
-	-	kIx~
obdržené	obdržený	k2eAgInPc4d1
góly	gól	k1gInPc4
<g/>
,	,	kIx,
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
-	-	kIx~
rozdíl	rozdíl	k1gInSc1
skóre	skóre	k1gNnSc2
<g/>
,	,	kIx,
B	B	kA
-	-	kIx~
body	bod	k1gInPc1
<g/>
,	,	kIx,
červené	červený	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
-	-	kIx~
sestup	sestup	k1gInSc1
<g/>
,	,	kIx,
zelené	zelený	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
-	-	kIx~
postup	postup	k1gInSc1
<g/>
,	,	kIx,
fialové	fialový	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
-	-	kIx~
reorganizace	reorganizace	k1gFnSc1
<g/>
,	,	kIx,
změna	změna	k1gFnSc1
skupiny	skupina	k1gFnSc2
či	či	k8xC
soutěže	soutěž	k1gFnSc2
</s>
<s>
Československo	Československo	k1gNnSc1
(	(	kIx(
<g/>
1923	#num#	k4
–	–	k?
1938	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sezóna	sezóna	k1gFnSc1
</s>
<s>
Liga	liga	k1gFnSc1
</s>
<s>
Úroveň	úroveň	k1gFnSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
V	v	k7c6
</s>
<s>
R	R	kA
</s>
<s>
P	P	kA
</s>
<s>
VG	VG	kA
</s>
<s>
OG	OG	kA
</s>
<s>
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
</s>
<s>
B	B	kA
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
1923	#num#	k4
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
III	III	kA
<g/>
.	.	kIx.
třída	třída	k1gFnSc1
Moravskoslezské	moravskoslezský	k2eAgFnSc2d1
župy	župa	k1gFnSc2
</s>
<s>
5	#num#	k4
</s>
<s>
...	...	k?
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1924	#num#	k4
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
II	II	kA
<g/>
.	.	kIx.
třída	třída	k1gFnSc1
Moravskoslezské	moravskoslezský	k2eAgFnSc2d1
župy	župa	k1gFnSc2
</s>
<s>
4	#num#	k4
</s>
<s>
...	...	k?
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1925	#num#	k4
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
I.	I.	kA
třída	třída	k1gFnSc1
Moravskoslezské	moravskoslezský	k2eAgFnPc1d1
župy	župa	k1gFnPc1
</s>
<s>
3	#num#	k4
</s>
<s>
...	...	k?
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1926	#num#	k4
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
I.	I.	kA
třída	třída	k1gFnSc1
Moravskoslezské	moravskoslezský	k2eAgFnPc1d1
župy	župa	k1gFnPc1
</s>
<s>
3	#num#	k4
</s>
<s>
...	...	k?
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1927	#num#	k4
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
I.	I.	kA
třída	třída	k1gFnSc1
Moravskoslezské	moravskoslezský	k2eAgFnPc1d1
župy	župa	k1gFnPc1
</s>
<s>
3	#num#	k4
</s>
<s>
...	...	k?
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1928	#num#	k4
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
I.	I.	kA
třída	třída	k1gFnSc1
Moravskoslezské	moravskoslezský	k2eAgFnPc1d1
župy	župa	k1gFnPc1
</s>
<s>
3	#num#	k4
</s>
<s>
...	...	k?
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1929	#num#	k4
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
I.	I.	kA
třída	třída	k1gFnSc1
Moravskoslezské	moravskoslezský	k2eAgFnPc1d1
župy	župa	k1gFnPc1
</s>
<s>
3	#num#	k4
</s>
<s>
...	...	k?
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1930	#num#	k4
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
I.	I.	kA
třída	třída	k1gFnSc1
Moravskoslezské	moravskoslezský	k2eAgFnPc1d1
župy	župa	k1gFnPc1
</s>
<s>
3	#num#	k4
</s>
<s>
...	...	k?
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1931	#num#	k4
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
II	II	kA
<g/>
.	.	kIx.
třída	třída	k1gFnSc1
Moravskoslezské	moravskoslezský	k2eAgFnSc2d1
župy	župa	k1gFnSc2
</s>
<s>
4	#num#	k4
</s>
<s>
...	...	k?
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1932	#num#	k4
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
II	II	kA
<g/>
.	.	kIx.
třída	třída	k1gFnSc1
Moravskoslezské	moravskoslezský	k2eAgFnSc2d1
župy	župa	k1gFnSc2
</s>
<s>
4	#num#	k4
</s>
<s>
...	...	k?
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1932	#num#	k4
<g/>
/	/	kIx~
<g/>
33	#num#	k4
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
I.	I.	kA
třída	třída	k1gFnSc1
Moravskoslezské	moravskoslezský	k2eAgFnPc1d1
župy	župa	k1gFnPc1
</s>
<s>
3	#num#	k4
</s>
<s>
...	...	k?
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1933	#num#	k4
<g/>
/	/	kIx~
<g/>
34	#num#	k4
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
I.	I.	kA
třída	třída	k1gFnSc1
Moravskoslezské	moravskoslezský	k2eAgFnPc1d1
župy	župa	k1gFnPc1
</s>
<s>
3	#num#	k4
</s>
<s>
...	...	k?
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1934	#num#	k4
<g/>
/	/	kIx~
<g/>
35	#num#	k4
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
divize	divize	k1gFnSc1
</s>
<s>
2	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
32	#num#	k4
</s>
<s>
29	#num#	k4
</s>
<s>
+3	+3	k4
</s>
<s>
25	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1935	#num#	k4
<g/>
/	/	kIx~
<g/>
36	#num#	k4
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
divize	divize	k1gFnSc1
</s>
<s>
2	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
60	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
+30	+30	k4
</s>
<s>
33	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1936	#num#	k4
<g/>
/	/	kIx~
<g/>
37	#num#	k4
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
divize	divize	k1gFnSc1
</s>
<s>
2	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
75	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
+56	+56	k4
</s>
<s>
39	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1937	#num#	k4
<g/>
/	/	kIx~
<g/>
38	#num#	k4
</s>
<s>
Státní	státní	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
43	#num#	k4
</s>
<s>
52	#num#	k4
</s>
<s>
-9	-9	k4
</s>
<s>
18	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1938	#num#	k4
<g/>
/	/	kIx~
<g/>
39	#num#	k4
</s>
<s>
Státní	státní	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
29	#num#	k4
</s>
<s>
39	#num#	k4
</s>
<s>
-10	-10	k4
</s>
<s>
18	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Protektorát	protektorát	k1gInSc1
Čechy	Čechy	k1gFnPc1
a	a	k8xC
Morava	Morava	k1gFnSc1
(	(	kIx(
<g/>
1939	#num#	k4
–	–	k?
1944	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sezóna	sezóna	k1gFnSc1
</s>
<s>
Liga	liga	k1gFnSc1
</s>
<s>
Úroveň	úroveň	k1gFnSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
V	v	k7c6
</s>
<s>
R	R	kA
</s>
<s>
P	P	kA
</s>
<s>
VG	VG	kA
</s>
<s>
OG	OG	kA
</s>
<s>
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
</s>
<s>
B	B	kA
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
1939	#num#	k4
<g/>
/	/	kIx~
<g/>
40	#num#	k4
</s>
<s>
Národní	národní	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
38	#num#	k4
</s>
<s>
68	#num#	k4
</s>
<s>
-30	-30	k4
</s>
<s>
13	#num#	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1940	#num#	k4
<g/>
/	/	kIx~
<g/>
41	#num#	k4
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
divize	divize	k1gFnSc1
</s>
<s>
2	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
96	#num#	k4
</s>
<s>
42	#num#	k4
</s>
<s>
+54	+54	k4
</s>
<s>
39	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1941	#num#	k4
<g/>
/	/	kIx~
<g/>
42	#num#	k4
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
divize	divize	k1gFnSc1
</s>
<s>
2	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
86	#num#	k4
</s>
<s>
40	#num#	k4
</s>
<s>
+46	+46	k4
</s>
<s>
38	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1942	#num#	k4
<g/>
/	/	kIx~
<g/>
43	#num#	k4
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
divize	divize	k1gFnSc1
</s>
<s>
2	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
100	#num#	k4
</s>
<s>
45	#num#	k4
</s>
<s>
+55	+55	k4
</s>
<s>
39	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1943	#num#	k4
<g/>
/	/	kIx~
<g/>
44	#num#	k4
</s>
<s>
Národní	národní	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
61	#num#	k4
</s>
<s>
83	#num#	k4
</s>
<s>
-22	-22	k4
</s>
<s>
25	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1944	#num#	k4
<g/>
/	/	kIx~
<g/>
45	#num#	k4
</s>
<s>
Fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
se	se	k3xPyFc4
nehrála	hrát	k5eNaImAgFnS
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
tak	tak	k9
i	i	k9
nižší	nízký	k2eAgFnSc2d2
fotbalové	fotbalový	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Československo	Československo	k1gNnSc1
(	(	kIx(
<g/>
1945	#num#	k4
–	–	k?
1993	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sezóna	sezóna	k1gFnSc1
</s>
<s>
Liga	liga	k1gFnSc1
</s>
<s>
Úroveň	úroveň	k1gFnSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
V	v	k7c6
</s>
<s>
R	R	kA
</s>
<s>
P	P	kA
</s>
<s>
VG	VG	kA
</s>
<s>
OG	OG	kA
</s>
<s>
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
</s>
<s>
B	B	kA
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
1945	#num#	k4
<g/>
/	/	kIx~
<g/>
46	#num#	k4
</s>
<s>
Státní	státní	k2eAgFnSc1d1
liga	liga	k1gFnSc1
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
A	a	k8xC
</s>
<s>
1	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
55	#num#	k4
</s>
<s>
57	#num#	k4
</s>
<s>
-2	-2	k4
</s>
<s>
19	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1946	#num#	k4
<g/>
/	/	kIx~
<g/>
47	#num#	k4
</s>
<s>
Státní	státní	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
62	#num#	k4
</s>
<s>
57	#num#	k4
</s>
<s>
+5	+5	k4
</s>
<s>
27	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1947	#num#	k4
<g/>
/	/	kIx~
<g/>
48	#num#	k4
</s>
<s>
Státní	státní	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
48	#num#	k4
</s>
<s>
56	#num#	k4
</s>
<s>
-8	-8	k4
</s>
<s>
19	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1949	#num#	k4
</s>
<s>
Celostátní	celostátní	k2eAgNnSc4d1
československé	československý	k2eAgNnSc4d1
mistrovství	mistrovství	k1gNnSc4
</s>
<s>
1	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
38	#num#	k4
</s>
<s>
55	#num#	k4
</s>
<s>
-17	-17	k4
</s>
<s>
22	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1950	#num#	k4
</s>
<s>
Celostátní	celostátní	k2eAgNnSc4d1
československé	československý	k2eAgNnSc4d1
mistrovství	mistrovství	k1gNnSc4
II	II	kA
</s>
<s>
2	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
73	#num#	k4
</s>
<s>
35	#num#	k4
</s>
<s>
+38	+38	k4
</s>
<s>
34	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1951	#num#	k4
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
československé	československý	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
1	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
42	#num#	k4
</s>
<s>
41	#num#	k4
</s>
<s>
+1	+1	k4
</s>
<s>
27	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1952	#num#	k4
</s>
<s>
Mistrovství	mistrovství	k1gNnSc1
československé	československý	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
1	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
43	#num#	k4
</s>
<s>
47	#num#	k4
</s>
<s>
-4	-4	k4
</s>
<s>
24	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1953	#num#	k4
</s>
<s>
Přebor	přebor	k1gInSc1
československé	československý	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
1	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
25	#num#	k4
</s>
<s>
-10	-10	k4
</s>
<s>
10	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1954	#num#	k4
</s>
<s>
Přebor	přebor	k1gInSc1
československé	československý	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
1	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
45	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
+19	+19	k4
</s>
<s>
28	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1955	#num#	k4
</s>
<s>
Přebor	přebor	k1gInSc1
československé	československý	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
1	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
29	#num#	k4
</s>
<s>
37	#num#	k4
</s>
<s>
-8	-8	k4
</s>
<s>
19	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1956	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
53	#num#	k4
</s>
<s>
48	#num#	k4
</s>
<s>
+5	+5	k4
</s>
<s>
25	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1957	#num#	k4
<g/>
/	/	kIx~
<g/>
58	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
33	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
58	#num#	k4
</s>
<s>
63	#num#	k4
</s>
<s>
-5	-5	k4
</s>
<s>
28	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1958	#num#	k4
<g/>
/	/	kIx~
<g/>
59	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
44	#num#	k4
</s>
<s>
40	#num#	k4
</s>
<s>
+4	+4	k4
</s>
<s>
26	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1959	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
29	#num#	k4
</s>
<s>
34	#num#	k4
</s>
<s>
-5	-5	k4
</s>
<s>
28	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1960	#num#	k4
<g/>
/	/	kIx~
<g/>
61	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
50	#num#	k4
</s>
<s>
46	#num#	k4
</s>
<s>
+4	+4	k4
</s>
<s>
29	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
49	#num#	k4
</s>
<s>
35	#num#	k4
</s>
<s>
+14	+14	k4
</s>
<s>
28	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1962	#num#	k4
<g/>
/	/	kIx~
<g/>
63	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
47	#num#	k4
</s>
<s>
33	#num#	k4
</s>
<s>
+14	+14	k4
</s>
<s>
31	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
51	#num#	k4
</s>
<s>
36	#num#	k4
</s>
<s>
+15	+15	k4
</s>
<s>
30	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
44	#num#	k4
</s>
<s>
33	#num#	k4
</s>
<s>
+11	+11	k4
</s>
<s>
28	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
36	#num#	k4
</s>
<s>
38	#num#	k4
</s>
<s>
-2	-2	k4
</s>
<s>
21	#num#	k4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
</s>
<s>
2	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
53	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
+36	+36	k4
</s>
<s>
37	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
27	#num#	k4
</s>
<s>
31	#num#	k4
</s>
<s>
-4	-4	k4
</s>
<s>
22	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
37	#num#	k4
</s>
<s>
-15	-15	k4
</s>
<s>
24	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
32	#num#	k4
</s>
<s>
-6	-6	k4
</s>
<s>
26	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
71	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
39	#num#	k4
</s>
<s>
32	#num#	k4
</s>
<s>
+7	+7	k4
</s>
<s>
34	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
39	#num#	k4
</s>
<s>
41	#num#	k4
</s>
<s>
-2	-2	k4
</s>
<s>
26	#num#	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
32	#num#	k4
</s>
<s>
34	#num#	k4
</s>
<s>
-2	-2	k4
</s>
<s>
30	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
40	#num#	k4
</s>
<s>
42	#num#	k4
</s>
<s>
-2	-2	k4
</s>
<s>
34	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
32	#num#	k4
</s>
<s>
36	#num#	k4
</s>
<s>
-4	-4	k4
</s>
<s>
27	#num#	k4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
37	#num#	k4
</s>
<s>
29	#num#	k4
</s>
<s>
+8	+8	k4
</s>
<s>
37	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
36	#num#	k4
</s>
<s>
33	#num#	k4
</s>
<s>
+3	+3	k4
</s>
<s>
30	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
35	#num#	k4
</s>
<s>
41	#num#	k4
</s>
<s>
-6	-6	k4
</s>
<s>
28	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
44	#num#	k4
</s>
<s>
22	#num#	k4
</s>
<s>
+22	+22	k4
</s>
<s>
41	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
47	#num#	k4
</s>
<s>
23	#num#	k4
</s>
<s>
+24	+24	k4
</s>
<s>
41	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
44	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
+25	+25	k4
</s>
<s>
40	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
82	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
53	#num#	k4
</s>
<s>
24	#num#	k4
</s>
<s>
+29	+29	k4
</s>
<s>
38	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
48	#num#	k4
</s>
<s>
31	#num#	k4
</s>
<s>
+17	+17	k4
</s>
<s>
40	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
45	#num#	k4
</s>
<s>
28	#num#	k4
</s>
<s>
+17	+17	k4
</s>
<s>
35	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
41	#num#	k4
</s>
<s>
23	#num#	k4
</s>
<s>
+18	+18	k4
</s>
<s>
39	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
41	#num#	k4
</s>
<s>
34	#num#	k4
</s>
<s>
+7	+7	k4
</s>
<s>
30	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
55	#num#	k4
</s>
<s>
39	#num#	k4
</s>
<s>
+16	+16	k4
</s>
<s>
33	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
49	#num#	k4
</s>
<s>
40	#num#	k4
</s>
<s>
+9	+9	k4
</s>
<s>
34	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
19	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
54	#num#	k4
</s>
<s>
34	#num#	k4
</s>
<s>
+20	+20	k4
</s>
<s>
42	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
50	#num#	k4
</s>
<s>
24	#num#	k4
</s>
<s>
+26	+26	k4
</s>
<s>
41	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
50	#num#	k4
</s>
<s>
34	#num#	k4
</s>
<s>
+16	+16	k4
</s>
<s>
32	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
50	#num#	k4
</s>
<s>
36	#num#	k4
</s>
<s>
+14	+14	k4
</s>
<s>
35	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
47	#num#	k4
</s>
<s>
38	#num#	k4
</s>
<s>
+9	+9	k4
</s>
<s>
31	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
1993	#num#	k4
–	–	k?
)	)	kIx)
</s>
<s>
Sezóna	sezóna	k1gFnSc1
</s>
<s>
Liga	liga	k1gFnSc1
</s>
<s>
Úroveň	úroveň	k1gFnSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
V	v	k7c6
</s>
<s>
R	R	kA
</s>
<s>
P	P	kA
</s>
<s>
VG	VG	kA
</s>
<s>
OG	OG	kA
</s>
<s>
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
</s>
<s>
B	B	kA
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
52	#num#	k4
</s>
<s>
25	#num#	k4
</s>
<s>
+27	+27	k4
</s>
<s>
36	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
36	#num#	k4
</s>
<s>
41	#num#	k4
</s>
<s>
-5	-5	k4
</s>
<s>
38	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
40	#num#	k4
</s>
<s>
46	#num#	k4
</s>
<s>
-6	-6	k4
</s>
<s>
35	#num#	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
33	#num#	k4
</s>
<s>
35	#num#	k4
</s>
<s>
-2	-2	k4
</s>
<s>
37	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
51	#num#	k4
</s>
<s>
35	#num#	k4
</s>
<s>
+16	+16	k4
</s>
<s>
52	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
39	#num#	k4
</s>
<s>
26	#num#	k4
</s>
<s>
+13	+13	k4
</s>
<s>
45	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
43	#num#	k4
</s>
<s>
45	#num#	k4
</s>
<s>
-2	-2	k4
</s>
<s>
35	#num#	k4
</s>
<s>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
28	#num#	k4
</s>
<s>
45	#num#	k4
</s>
<s>
-17	-17	k4
</s>
<s>
30	#num#	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
43	#num#	k4
</s>
<s>
36	#num#	k4
</s>
<s>
+7	+7	k4
</s>
<s>
44	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
41	#num#	k4
</s>
<s>
38	#num#	k4
</s>
<s>
+3	+3	k4
</s>
<s>
45	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
3	#num#	k4
</s>
<s>
60	#num#	k4
</s>
<s>
25	#num#	k4
</s>
<s>
+35	+35	k4
</s>
<s>
63	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
33	#num#	k4
</s>
<s>
36	#num#	k4
</s>
<s>
-3	-3	k4
</s>
<s>
37	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
35	#num#	k4
</s>
<s>
32	#num#	k4
</s>
<s>
+3	+3	k4
</s>
<s>
40	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
43	#num#	k4
</s>
<s>
33	#num#	k4
</s>
<s>
+10	+10	k4
</s>
<s>
46	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
51	#num#	k4
</s>
<s>
28	#num#	k4
</s>
<s>
+23	+23	k4
</s>
<s>
55	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
6	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
38	#num#	k4
</s>
<s>
36	#num#	k4
</s>
<s>
+2	+2	k4
</s>
<s>
39	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
17	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
47	#num#	k4
</s>
<s>
25	#num#	k4
</s>
<s>
+22	+22	k4
</s>
<s>
60	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
14	#num#	k4
</s>
<s>
31	#num#	k4
</s>
<s>
46	#num#	k4
</s>
<s>
-15	-15	k4
</s>
<s>
30	#num#	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
16	#num#	k4
</s>
<s>
31	#num#	k4
</s>
<s>
48	#num#	k4
</s>
<s>
-17	-17	k4
</s>
<s>
28	#num#	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
15	#num#	k4
</s>
<s>
34	#num#	k4
</s>
<s>
44	#num#	k4
</s>
<s>
-10	-10	k4
</s>
<s>
29	#num#	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
</s>
<s>
Gambrinus	gambrinus	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
33	#num#	k4
</s>
<s>
43	#num#	k4
</s>
<s>
-10	-10	k4
</s>
<s>
35	#num#	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
</s>
<s>
Synot	Synot	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
9	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
23	#num#	k4
</s>
<s>
41	#num#	k4
</s>
<s>
-18	-18	k4
</s>
<s>
33	#num#	k4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
</s>
<s>
Synot	Synot	k1gInSc1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
24	#num#	k4
</s>
<s>
27	#num#	k4
</s>
<s>
65	#num#	k4
</s>
<s>
-38	-38	k4
</s>
<s>
14	#num#	k4
</s>
<s>
16	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
</s>
<s>
Fotbalová	fotbalový	k2eAgFnSc1d1
národní	národní	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
2	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
18	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
48	#num#	k4
</s>
<s>
20	#num#	k4
</s>
<s>
+28	+28	k4
</s>
<s>
64	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
</s>
<s>
HET	HET	kA
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
7	#num#	k4
</s>
<s>
10	#num#	k4
</s>
<s>
13	#num#	k4
</s>
<s>
36	#num#	k4
</s>
<s>
43	#num#	k4
</s>
<s>
-7	-7	k4
</s>
<s>
31	#num#	k4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
</s>
<s>
Fortuna	Fortuna	k1gFnSc1
<g/>
:	:	kIx,
<g/>
Liga	liga	k1gFnSc1
</s>
<s>
135138143943-447	135138143943-447	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
</s>
<s>
Fortuna	Fortuna	k1gFnSc1
<g/>
:	:	kIx,
<g/>
Liga	liga	k1gFnSc1
</s>
<s>
1351211	#num#	k4
</s>
<s>
12	#num#	k4
</s>
<s>
47	#num#	k4
</s>
<s>
43	#num#	k4
</s>
<s>
+4	+4	k4
</s>
<s>
47	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
</s>
<s>
Fortuna	Fortuna	k1gFnSc1
<g/>
:	:	kIx,
<g/>
Liga	liga	k1gFnSc1
</s>
<s>
130	#num#	k4
</s>
<s>
Účast	účast	k1gFnSc1
v	v	k7c6
evropských	evropský	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
v	v	k7c6
článku	článek	k1gInSc6
FC	FC	kA
Baník	baník	k1gMnSc1
Ostrava	Ostrava	k1gFnSc1
v	v	k7c6
evropských	evropský	k2eAgInPc6d1
fotbalových	fotbalový	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
SEP	SEP	kA
–	–	k?
Středoevropský	středoevropský	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
,	,	kIx,
VP	VP	kA
–	–	k?
Veletržní	veletržní	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
,	,	kIx,
PMEZ	PMEZ	kA
–	–	k?
Pohár	pohár	k1gInSc1
mistrů	mistr	k1gMnPc2
evropských	evropský	k2eAgFnPc2d1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
PVP	PVP	kA
–	–	k?
Pohár	pohár	k1gInSc4
vítězů	vítěz	k1gMnPc2
pohárů	pohár	k1gInPc2
<g/>
,	,	kIx,
LM	LM	kA
–	–	k?
Liga	liga	k1gFnSc1
mistrů	mistr	k1gMnPc2
UEFA	UEFA	kA
<g/>
,	,	kIx,
UEFA	UEFA	kA
–	–	k?
Pohár	pohár	k1gInSc1
UEFA	UEFA	kA
<g/>
,	,	kIx,
EL	Ela	k1gFnPc2
–	–	k?
Evropská	evropský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
UEFA	UEFA	kA
<g/>
,	,	kIx,
SP	SP	kA
–	–	k?
Superpohár	superpohár	k1gInSc1
UEFA	UEFA	kA
<g/>
,	,	kIx,
PI	pi	k0
–	–	k?
Pohár	pohár	k1gInSc4
Intertoto	Intertota	k1gFnSc5
<g/>
,	,	kIx,
IP	IP	kA
–	–	k?
Interkontinentální	interkontinentální	k2eAgInSc4d1
pohár	pohár	k1gInSc4
<g/>
,	,	kIx,
MS	MS	kA
–	–	k?
Mistrovství	mistrovství	k1gNnSc1
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
klubů	klub	k1gInPc2
</s>
<s>
VP	VP	kA
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
1970	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
</s>
<s>
PVP	PVP	kA
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
1974	#num#	k4
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
</s>
<s>
UEFA	UEFA	kA
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
1975	#num#	k4
–	–	k?
Čtvrtfinále	čtvrtfinále	k1gNnSc1
</s>
<s>
PMEZ	PMEZ	kA
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
1977	#num#	k4
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
</s>
<s>
PVP	PVP	kA
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
1979	#num#	k4
–	–	k?
Semifinále	semifinále	k1gNnSc1
</s>
<s>
UEFA	UEFA	kA
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
1980	#num#	k4
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
</s>
<s>
PMEZ	PMEZ	kA
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
1981	#num#	k4
–	–	k?
Čtvrtfinále	čtvrtfinále	k1gNnSc1
</s>
<s>
PMEZ	PMEZ	kA
1981	#num#	k4
<g/>
/	/	kIx~
<g/>
1982	#num#	k4
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
</s>
<s>
UEFA	UEFA	kA
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
1983	#num#	k4
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
</s>
<s>
UEFA	UEFA	kA
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
1984	#num#	k4
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
</s>
<s>
UEFA	UEFA	kA
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
1986	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
</s>
<s>
UEFA	UEFA	kA
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
1990	#num#	k4
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
</s>
<s>
UEFA	UEFA	kA
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
1991	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
</s>
<s>
PVP	PVP	kA
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
1992	#num#	k4
–	–	k?
2	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
</s>
<s>
LM	LM	kA
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
2005	#num#	k4
–	–	k?
3	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
</s>
<s>
UEFA	UEFA	kA
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
2005	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
</s>
<s>
UEFA	UEFA	kA
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
2006	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
</s>
<s>
UEFA	UEFA	kA
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
kolo	kolo	k1gNnSc1
</s>
<s>
EL	Ela	k1gFnPc2
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
2011	#num#	k4
–	–	k?
3	#num#	k4
<g/>
.	.	kIx.
předkolo	předkolo	k1gNnSc1
</s>
<s>
Slavní	slavný	k2eAgMnPc1d1
hráči	hráč	k1gMnPc1
</s>
<s>
Milan	Milan	k1gMnSc1
Baroš	Baroš	k1gMnSc1
</s>
<s>
Marek	Marek	k1gMnSc1
Jankulovski	Jankulovsk	k1gFnSc2
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Galásek	Galásek	k1gMnSc1
</s>
<s>
Václav	Václav	k1gMnSc1
Svěrkoš	Svěrkoš	k1gMnSc1
</s>
<s>
Radoslav	Radoslav	k1gMnSc1
Látal	Látal	k1gMnSc1
</s>
<s>
Radek	Radek	k1gMnSc1
Slončík	Slončík	k1gMnSc1
</s>
<s>
Martin	Martin	k1gMnSc1
Lukeš	Lukeš	k1gMnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Laštůvka	laštůvka	k1gFnSc1
</s>
<s>
Marek	Marek	k1gMnSc1
Heinz	Heinz	k1gMnSc1
</s>
<s>
Mario	Mario	k1gMnSc1
Lička	lička	k1gFnSc1
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1
Pospěch	pospěch	k1gInSc4
</s>
<s>
Maroš	Maroš	k1gMnSc1
Klimpl	Klimpl	k1gMnSc1
</s>
<s>
Martin	Martin	k1gMnSc1
Raška	Raška	k1gMnSc1
</s>
<s>
Lukáš	Lukáš	k1gMnSc1
Magera	Magero	k1gNnSc2
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
Otepka	Otepek	k1gMnSc2
</s>
<s>
Martin	Martin	k1gMnSc1
Čížek	Čížek	k1gMnSc1
</s>
<s>
Peter	Peter	k1gMnSc1
Drozd	Drozd	k1gMnSc1
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Řepka	řepka	k1gFnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Pavlenka	Pavlenka	k1gFnSc1
</s>
<s>
René	René	k1gMnSc1
Bolf	Bolf	k1gMnSc1
</s>
<s>
Libor	Libor	k1gMnSc1
Sionko	Sionko	k1gNnSc1
</s>
<s>
Marek	Marek	k1gMnSc1
Čech	Čech	k1gMnSc1
</s>
<s>
Legendy	legenda	k1gFnPc1
klubu	klub	k1gInSc2
</s>
<s>
Václav	Václav	k1gMnSc1
Daněk	Daněk	k1gMnSc1
</s>
<s>
Evžen	Evžen	k1gMnSc1
Hadamczik	Hadamczik	k1gMnSc1
</s>
<s>
Viliam	Viliam	k1gMnSc1
Hýravý	Hýravý	k2eAgMnSc1d1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Křižák	křižák	k1gMnSc1
</s>
<s>
Karel	Karel	k1gMnSc1
Kula	kula	k1gFnSc1
</s>
<s>
Verner	Verner	k1gMnSc1
Lička	lička	k1gFnSc1
</s>
<s>
Pavol	Pavol	k1gInSc1
Michalík	Michalík	k1gMnSc1
</s>
<s>
Luděk	Luděk	k1gMnSc1
Mikloško	Mikloška	k1gFnSc5
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Pospíchal	Pospíchal	k1gMnSc1
</s>
<s>
Libor	Libor	k1gMnSc1
Radimec	Radimec	k1gMnSc1
</s>
<s>
František	František	k1gMnSc1
Schmucker	Schmucker	k1gMnSc1
</s>
<s>
František	František	k1gMnSc1
Valošek	Valošek	k1gMnSc1
</s>
<s>
Rostislav	Rostislav	k1gMnSc1
Vojáček	Vojáček	k1gMnSc1
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
Wiecek	Wiecka	k1gFnPc2
</s>
<s>
Petr	Petr	k1gMnSc1
Samec	samec	k1gMnSc1
</s>
<s>
Pavel	Pavel	k1gMnSc1
Srniček	srnička	k1gFnPc2
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1
Rygel	Rygel	k1gMnSc1
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1
Šreiner	Šreiner	k1gMnSc1
</s>
<s>
Milan	Milan	k1gMnSc1
Baroš	Baroš	k1gMnSc1
</s>
<s>
Seznam	seznam	k1gInSc1
trenérů	trenér	k1gMnPc2
</s>
<s>
Glass	Glass	k1gInSc1
(	(	kIx(
<g/>
1923	#num#	k4
<g/>
-	-	kIx~
<g/>
1935	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Karel	Karel	k1gMnSc1
Nenál	Nenál	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
1936	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
1936	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Karel	Karel	k1gMnSc1
Böhm	Böhm	k1gMnSc1
(	(	kIx(
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
1936	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
1937	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Karel	Karel	k1gMnSc1
Hromadník	Hromadník	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
1937	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
1937	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ladislav	Ladislav	k1gMnSc1
Holeček	Holeček	k1gMnSc1
(	(	kIx(
<g/>
10	#num#	k4
<g/>
/	/	kIx~
<g/>
1937	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
1937	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vilém	Vilém	k1gMnSc1
Lugr	Lugr	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
1938	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Karel	Karel	k1gMnSc1
Böhm	Böhm	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
1938	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
/	/	kIx~
<g/>
1938	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Karel	Karel	k1gMnSc1
Texa	Texa	k1gMnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
1938	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
1938	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Karel	Karel	k1gMnSc1
Böhm	Böhm	k1gMnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
1938	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
1938	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1
Stefflik	Stefflik	k1gMnSc1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
1938	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
1939	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Antonín	Antonín	k1gMnSc1
Křišťál	křišťál	k1gInSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
1939	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
1940	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Karel	Karel	k1gMnSc1
Böhm	Böhm	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
1940	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
1941	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Evžen	Evžen	k1gMnSc1
Šenovský	šenovský	k2eAgMnSc1d1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
1941	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
1941	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Antonín	Antonín	k1gMnSc1
Rumler	Rumler	k1gMnSc1
(	(	kIx(
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
1941	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
1942	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Václav	Václav	k1gMnSc1
Horák	Horák	k1gMnSc1
(	(	kIx(
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
1942	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
1943	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
František	František	k1gMnSc1
Jurek	Jurek	k1gMnSc1
(	(	kIx(
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
1943	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
1945	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
František	František	k1gMnSc1
Bělík	Bělík	k1gMnSc1
(	(	kIx(
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
1945	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
1946	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
František	František	k1gMnSc1
Kuchta	Kuchta	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
1946	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
1946	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Josef	Josef	k1gMnSc1
Kuchynka	Kuchynka	k1gMnSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
1946	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
1948	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jan	Jan	k1gMnSc1
Gavač	Gavač	k1gMnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
1948	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
1948	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Václav	Václav	k1gMnSc1
Horák	Horák	k1gMnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
1948	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
1949	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
Bartoš	Bartoš	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
1949	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
/	/	kIx~
<g/>
1949	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
František	František	k1gMnSc1
Bičiště	bičiště	k1gNnSc2
(	(	kIx(
<g/>
10	#num#	k4
<g/>
/	/	kIx~
<g/>
1949	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
1950	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Šimonek	Šimonka	k1gFnPc2
(	(	kIx(
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
1950	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
1951	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rudolf	Rudolf	k1gMnSc1
Vytlačil	Vytlačil	k1gMnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
1951	#num#	k4
<g/>
-	-	kIx~
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
1952	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Bedřich	Bedřich	k1gMnSc1
Šafl	Šafl	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
1952	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
/	/	kIx~
<g/>
1952	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Šimonek	Šimonka	k1gFnPc2
(	(	kIx(
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
1952	#num#	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
1956	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ferenc	Ferenc	k1gMnSc1
Szedlacsek	Szedlacsek	k1gMnSc1
(	(	kIx(
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
1956	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
1957	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Antonín	Antonín	k1gMnSc1
Honál	Honál	k1gMnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
1957	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
1957	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
František	František	k1gMnSc1
Bičiště	bičiště	k1gNnSc2
(	(	kIx(
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
1957	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
1958	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Vejvoda	Vejvoda	k1gMnSc1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
1958	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
1960	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
František	František	k1gMnSc1
Bufka	Bufka	k1gMnSc1
(	(	kIx(
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
1960	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
1964	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1
Šajer	Šajer	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
1965	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
1965	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
František	František	k1gMnSc1
Bičiště	bičiště	k1gNnSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
1966	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
1966	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
Křižák	křižák	k1gMnSc1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
1966	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
1966	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jozef	Jozef	k1gMnSc1
Čurgaly	Čurgala	k1gFnSc2
(	(	kIx(
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
1967	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
1967	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Oldřich	Oldřich	k1gMnSc1
Šubrt	Šubrt	k1gMnSc1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
1967	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
1969	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
Rubáš	rubáš	k1gInSc1
(	(	kIx(
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
1969	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
1970	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
František	František	k1gMnSc1
Ipser	Ipser	k1gMnSc1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
1970	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
1971	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1
Stanczo	Stancza	k1gFnSc5
(	(	kIx(
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
1971	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
1971	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Karol	Karol	k1gInSc1
Bučko	Bučko	k1gNnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
1972	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
1972	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
František	František	k1gMnSc1
Šindelář	Šindelář	k1gMnSc1
(	(	kIx(
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
1972	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
/	/	kIx~
<g/>
1972	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Pospíchal	Pospíchal	k1gMnSc1
(	(	kIx(
<g/>
10	#num#	k4
<g/>
/	/	kIx~
<g/>
1972	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
1975	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
Rubáš	rubáš	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
1976	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
1977	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Evžen	Evžen	k1gMnSc1
Hadamczik	Hadamczik	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
1978	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Stanislav	Stanislav	k1gMnSc1
Jarábek	Jarábek	k1gMnSc1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
1983	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
1984	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Josef	Josef	k1gMnSc1
Kolečko	kolečko	k1gNnSc4
(	(	kIx(
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
1984	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
1986	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Milan	Milan	k1gMnSc1
Máčala	Máčal	k1gMnSc2
(	(	kIx(
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
1986	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
1990	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Gürtler	Gürtler	k1gMnSc1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
1990	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ivan	Ivan	k1gMnSc1
Kopecký	Kopecký	k1gMnSc1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
1992	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
/	/	kIx~
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Janoš	Janoš	k1gMnSc1
(	(	kIx(
<g/>
11	#num#	k4
<g/>
/	/	kIx~
<g/>
1992	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
1992	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Verner	Verner	k1gMnSc1
Lička	lička	k1gFnSc1
(	(	kIx(
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
1992	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Janoš	Janoš	k1gMnSc1
(	(	kIx(
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
1995	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ján	Ján	k1gMnSc1
Zachar	Zachar	k1gMnSc1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Janoš	Janoš	k1gMnSc1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
1995	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
1995	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ján	Ján	k1gMnSc1
Zachar	Zachar	k1gMnSc1
(	(	kIx(
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
1995	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
1996	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Petr	Petr	k1gMnSc1
Uličný	Uličný	k2eAgMnSc1d1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
1996	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
1997	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Verner	Verner	k1gMnSc1
Lička	lička	k1gFnSc1
(	(	kIx(
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
1997	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Rostislav	Rostislav	k1gMnSc1
Vojáček	Vojáček	k1gMnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Milan	Milan	k1gMnSc1
Bokša	Bokša	k1gMnSc1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1
Gürtler	Gürtler	k1gMnSc1
(	(	kIx(
<g/>
11	#num#	k4
<g/>
/	/	kIx~
<g/>
2000	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Verner	Verner	k1gMnSc1
Lička	lička	k1gFnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
2001	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jozef	Jozef	k1gMnSc1
Jarabinský	Jarabinský	k2eAgMnSc1d1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2001	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
2002	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Erich	Erich	k1gMnSc1
Cviertna	Cviertn	k1gMnSc2
(	(	kIx(
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2002	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pavel	Pavel	k1gMnSc1
Vrba	Vrba	k1gMnSc1
(	(	kIx(
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
2003	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
František	František	k1gMnSc1
Komňacký	Komňacký	k1gMnSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2003	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
/	/	kIx~
<g/>
2004	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jozef	Jozef	k1gMnSc1
Jarabinský	Jarabinský	k2eAgMnSc1d1
(	(	kIx(
<g/>
10	#num#	k4
<g/>
/	/	kIx~
<g/>
2004	#num#	k4
<g/>
-	-	kIx~
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
2005	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pavel	Pavel	k1gMnSc1
Hapal	Hapal	k1gMnSc1
(	(	kIx(
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
2005	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2006	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Karel	Karel	k1gMnSc1
Večeřa	Večeřa	k1gMnSc1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
2006	#num#	k4
<g/>
-	-	kIx~
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Miroslav	Miroslav	k1gMnSc1
Koubek	Koubek	k1gMnSc1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g/>
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
/	/	kIx~
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Verner	Verner	k1gMnSc1
Lička	lička	k1gFnSc1
(	(	kIx(
<g/>
10	#num#	k4
<g/>
/	/	kIx~
<g/>
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
/	/	kIx~
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Karol	Karol	k1gInSc1
Marko	Marko	k1gMnSc1
(	(	kIx(
<g/>
11	#num#	k4
<g/>
/	/	kIx~
<g/>
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Pavel	Pavel	k1gMnSc1
Malura	Malur	k1gMnSc2
(	(	kIx(
<g/>
8	#num#	k4
<g/>
/	/	kIx~
<g/>
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Radoslav	Radoslav	k1gMnSc1
Látal	Látal	k1gMnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
/	/	kIx~
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Martin	Martin	k1gMnSc1
Pulpit	pulpit	k1gInSc1
(	(	kIx(
<g/>
11	#num#	k4
<g/>
/	/	kIx~
<g/>
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Martin	Martin	k1gMnSc1
Svědík	Svědík	k1gMnSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
František	František	k1gMnSc1
Komňacký	Komňacký	k1gMnSc1
(	(	kIx(
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Bernady	Bernada	k1gFnSc2
(	(	kIx(
<g/>
4	#num#	k4
<g/>
/	/	kIx~
<g/>
2014	#num#	k4
<g/>
–	–	k?
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Petr	Petr	k1gMnSc1
Frňka	Frňka	k1gMnSc1
(	(	kIx(
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
2014	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Radomír	Radomír	k1gMnSc1
Korytář	korytář	k1gMnSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2015	#num#	k4
<g/>
–	–	k?
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vlastimil	Vlastimil	k1gMnSc1
Petržela	Petržela	k1gMnSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
2016	#num#	k4
<g/>
–	–	k?
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Radim	Radim	k1gMnSc1
Kučera	Kučera	k1gMnSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bohumil	Bohumil	k1gMnSc1
Páník	Páník	k1gMnSc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
2018	#num#	k4
<g/>
–	–	k?
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Luboš	Luboš	k1gMnSc1
Kozel	Kozel	k1gMnSc1
(	(	kIx(
<g/>
12	#num#	k4
<g/>
/	/	kIx~
<g/>
2019	#num#	k4
<g/>
–	–	k?
<g/>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
2021	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
„	„	k?
<g/>
B	B	kA
<g/>
“	“	k?
</s>
<s>
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
„	„	k?
<g/>
B	B	kA
<g/>
“	“	k?
je	být	k5eAaImIp3nS
rezervní	rezervní	k2eAgInSc4d1
tým	tým	k1gInSc4
ostravského	ostravský	k2eAgInSc2d1
Baníku	Baník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
sezoně	sezona	k1gFnSc6
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
se	s	k7c7
přechodem	přechod	k1gInSc7
z	z	k7c2
Moravskoslezské	moravskoslezský	k2eAgFnSc2d1
fotbalové	fotbalový	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
do	do	k7c2
juniorské	juniorský	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
vyčlenil	vyčlenit	k5eAaPmAgInS
z	z	k7c2
mužských	mužský	k2eAgFnPc2d1
soutěží	soutěž	k1gFnPc2
FAČR	FAČR	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Znovuobnoven	Znovuobnovna	k1gFnPc2
byl	být	k5eAaImAgInS
v	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Umístění	umístění	k1gNnSc1
v	v	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
sezonách	sezona	k1gFnPc6
</s>
<s>
Stručný	stručný	k2eAgInSc1d1
přehled	přehled	k1gInSc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1961	#num#	k4
<g/>
–	–	k?
<g/>
1964	#num#	k4
<g/>
:	:	kIx,
Severomoravský	severomoravský	k2eAgInSc1d1
krajský	krajský	k2eAgInSc1d1
přebor	přebor	k1gInSc1
</s>
<s>
1964	#num#	k4
<g/>
–	–	k?
<g/>
1966	#num#	k4
<g/>
:	:	kIx,
2	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
</s>
<s>
1966	#num#	k4
<g/>
–	–	k?
<g/>
1967	#num#	k4
<g/>
:	:	kIx,
Divize	divize	k1gFnSc2
D	D	kA
</s>
<s>
1967	#num#	k4
<g/>
–	–	k?
<g/>
1968	#num#	k4
<g/>
:	:	kIx,
2	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
</s>
<s>
1968	#num#	k4
<g/>
–	–	k?
<g/>
1969	#num#	k4
<g/>
:	:	kIx,
Divize	divize	k1gFnSc2
D	D	kA
</s>
<s>
1969	#num#	k4
<g/>
–	–	k?
<g/>
1968	#num#	k4
<g/>
:	:	kIx,
3	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
</s>
<s>
1977	#num#	k4
<g/>
–	–	k?
<g/>
1984	#num#	k4
<g/>
:	:	kIx,
bez	bez	k7c2
soutěže	soutěž	k1gFnSc2
</s>
<s>
1984	#num#	k4
<g/>
–	–	k?
<g/>
1985	#num#	k4
<g/>
:	:	kIx,
Divize	divize	k1gFnSc2
D	D	kA
</s>
<s>
1985	#num#	k4
<g/>
–	–	k?
<g/>
1991	#num#	k4
<g/>
:	:	kIx,
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČNFL	ČNFL	kA
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
</s>
<s>
1991	#num#	k4
<g/>
–	–	k?
<g/>
2007	#num#	k4
<g/>
:	:	kIx,
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
2007	#num#	k4
<g/>
–	–	k?
<g/>
2008	#num#	k4
<g/>
:	:	kIx,
Divize	divize	k1gFnSc2
E	E	kA
</s>
<s>
2008	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
<g/>
:	:	kIx,
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
2010	#num#	k4
<g/>
–	–	k?
<g/>
2011	#num#	k4
<g/>
:	:	kIx,
Divize	divize	k1gFnSc2
E	E	kA
</s>
<s>
2011	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
:	:	kIx,
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
2013	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
<g/>
:	:	kIx,
bez	bez	k7c2
soutěže	soutěž	k1gFnSc2
</s>
<s>
2019	#num#	k4
<g/>
–	–	k?
:	:	kIx,
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1
ročníky	ročník	k1gInPc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
Z	Z	kA
-	-	kIx~
zápasy	zápas	k1gInPc1
<g/>
,	,	kIx,
V	v	k7c6
-	-	kIx~
výhry	výhra	k1gFnPc4
<g/>
,	,	kIx,
R	R	kA
-	-	kIx~
remízy	remíz	k1gInPc1
<g/>
,	,	kIx,
P	P	kA
-	-	kIx~
porážky	porážka	k1gFnSc2
<g/>
,	,	kIx,
VG	VG	kA
-	-	kIx~
vstřelené	vstřelený	k2eAgInPc1d1
góly	gól	k1gInPc1
<g/>
,	,	kIx,
OG	OG	kA
-	-	kIx~
obdržené	obdržený	k2eAgInPc4d1
góly	gól	k1gInPc4
<g/>
,	,	kIx,
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
-	-	kIx~
rozdíl	rozdíl	k1gInSc1
skóre	skóre	k1gNnSc2
<g/>
,	,	kIx,
B	B	kA
-	-	kIx~
body	bod	k1gInPc1
<g/>
,	,	kIx,
červené	červený	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
-	-	kIx~
sestup	sestup	k1gInSc1
<g/>
,	,	kIx,
zelené	zelený	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
-	-	kIx~
postup	postup	k1gInSc1
<g/>
,	,	kIx,
fialové	fialový	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
-	-	kIx~
reorganizace	reorganizace	k1gFnSc1
<g/>
,	,	kIx,
změna	změna	k1gFnSc1
skupiny	skupina	k1gFnSc2
či	či	k8xC
soutěže	soutěž	k1gFnSc2
</s>
<s>
Československo	Československo	k1gNnSc1
(	(	kIx(
<g/>
1961	#num#	k4
–	–	k?
1993	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sezóny	sezóna	k1gFnPc1
</s>
<s>
Liga	liga	k1gFnSc1
</s>
<s>
Úroveň	úroveň	k1gFnSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
V	v	k7c6
</s>
<s>
R	R	kA
</s>
<s>
P	P	kA
</s>
<s>
VG	VG	kA
</s>
<s>
OG	OG	kA
</s>
<s>
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
</s>
<s>
B	B	kA
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
</s>
<s>
Severomoravský	severomoravský	k2eAgInSc1d1
krajský	krajský	k2eAgInSc1d1
přebor	přebor	k1gInSc1
</s>
<s>
32613496041	#num#	k4
<g/>
+	+	kIx~
<g/>
1930	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1962	#num#	k4
<g/>
/	/	kIx~
<g/>
63	#num#	k4
</s>
<s>
Severomoravský	severomoravský	k2eAgInSc1d1
krajský	krajský	k2eAgInSc1d1
přebor	přebor	k1gInSc1
</s>
<s>
32617368730	#num#	k4
<g/>
+	+	kIx~
<g/>
5737	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
</s>
<s>
Severomoravský	severomoravský	k2eAgInSc1d1
krajský	krajský	k2eAgInSc1d1
přebor	přebor	k1gInSc1
</s>
<s>
32621417811	#num#	k4
<g/>
+	+	kIx~
<g/>
6746	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
</s>
<s>
226112133942-324	226112133942-324	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
</s>
<s>
22676131934-1520	22676131934-1520	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
</s>
<s>
Divize	divize	k1gFnSc1
D	D	kA
</s>
<s>
32617546823	#num#	k4
<g/>
+	+	kIx~
<g/>
4539	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
</s>
<s>
22694133343-1022	22694133343-1022	k4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
</s>
<s>
Divize	divize	k1gFnSc1
D	D	kA
</s>
<s>
32610793527	#num#	k4
<g/>
+	+	kIx~
<g/>
827	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
</s>
<s>
33016665131	#num#	k4
<g/>
+	+	kIx~
<g/>
2038	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
71	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
</s>
<s>
3301010104241	#num#	k4
<g/>
+	+	kIx~
<g/>
130	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
</s>
<s>
330135125143	#num#	k4
<g/>
+	+	kIx~
<g/>
831	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
</s>
<s>
330155183828	#num#	k4
<g/>
+	+	kIx~
<g/>
1035	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
</s>
<s>
330117123331	#num#	k4
<g/>
+	+	kIx~
<g/>
229	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
</s>
<s>
330108124937	#num#	k4
<g/>
+	+	kIx~
<g/>
1228	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
</s>
<s>
33091293026	#num#	k4
<g/>
+	+	kIx~
<g/>
430	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
liga	liga	k1gFnSc1
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
</s>
<s>
33016684026	#num#	k4
<g/>
+	+	kIx~
<g/>
1438	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1977	#num#	k4
<g/>
–	–	k?
<g/>
1984	#num#	k4
</s>
<s>
B-mužstvo	B-mužstvo	k1gNnSc1
nebylo	být	k5eNaImAgNnS
přihlášeno	přihlásit	k5eAaPmNgNnS
v	v	k7c6
žádné	žádný	k3yNgFnSc6
soutěži	soutěž	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
</s>
<s>
Divize	divize	k1gFnSc1
D	D	kA
</s>
<s>
43021457323	#num#	k4
<g/>
+	+	kIx~
<g/>
5046	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČNFL	ČNFL	kA
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
</s>
<s>
33015964825	#num#	k4
<g/>
+	+	kIx~
<g/>
2339	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČNFL	ČNFL	kA
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
</s>
<s>
330153124336	#num#	k4
<g/>
+	+	kIx~
<g/>
742	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČNFL	ČNFL	kA
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
</s>
<s>
328108104041-128	328108104041-128	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČNFL	ČNFL	kA
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
</s>
<s>
330146105041	#num#	k4
<g/>
+	+	kIx~
<g/>
934	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1989	#num#	k4
<g/>
/	/	kIx~
<g/>
90	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČNFL	ČNFL	kA
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
</s>
<s>
330102183946-722	330102183946-722	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČNFL	ČNFL	kA
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
</s>
<s>
330111185339	#num#	k4
<g/>
+	+	kIx~
<g/>
1433	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
330109115133	#num#	k4
<g/>
+	+	kIx~
<g/>
1829	#num#	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
33016686134	#num#	k4
<g/>
+	+	kIx~
<g/>
3038	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
1993	#num#	k4
–	–	k?
)	)	kIx)
</s>
<s>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
330155104337	#num#	k4
<g/>
+	+	kIx~
<g/>
635	#num#	k4
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
33015784731	#num#	k4
<g/>
+	+	kIx~
<g/>
1652	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
32888124147-632	32888124147-632	k4
</s>
<s>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
32810994543	#num#	k4
<g/>
+	+	kIx~
<g/>
239	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
330163116141	#num#	k4
<g/>
+	+	kIx~
<g/>
2051	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
330136113619	#num#	k4
<g/>
+	+	kIx~
<g/>
1745	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
328111164529	#num#	k4
<g/>
+	+	kIx~
<g/>
1644	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
330111094534	#num#	k4
<g/>
+	+	kIx~
<g/>
1143	#num#	k4
</s>
<s>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
33013985035	#num#	k4
<g/>
+	+	kIx~
<g/>
1548	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
33017676926	#num#	k4
<g/>
+	+	kIx~
<g/>
4357	#num#	k4
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
33015875023	#num#	k4
<g/>
+	+	kIx~
<g/>
2753	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2004	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
33013983827	#num#	k4
<g/>
+	+	kIx~
<g/>
1148	#num#	k4
</s>
<s>
5	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
330117123641-540	330117123641-540	k4
</s>
<s>
9	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
33097143134-534	33097143134-534	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Divize	divize	k1gFnSc1
E	E	kA
</s>
<s>
43020917019	#num#	k4
<g/>
+	+	kIx~
<g/>
5169	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
330125132834-641	330125132834-641	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
32874172746-1925	32874172746-1925	k4
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Divize	divize	k1gFnSc1
E	E	kA
</s>
<s>
42817836422	#num#	k4
<g/>
+	+	kIx~
<g/>
4259	#num#	k4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
33098133346-1335	33098133346-1335	k4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
330116134148-739	330116134148-739	k4
</s>
<s>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2013	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
</s>
<s>
B-mužstvo	B-mužstvo	k1gNnSc1
nebylo	být	k5eNaImAgNnS
přihlášeno	přihlásit	k5eAaPmNgNnS
v	v	k7c6
žádné	žádný	k3yNgFnSc6
soutěži	soutěž	k1gFnSc6
</s>
<s>
**	**	k?
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
3189	#num#	k4
</s>
<s>
4	#num#	k4
</s>
<s>
5	#num#	k4
</s>
<s>
3919	#num#	k4
</s>
<s>
+20	+20	k4
</s>
<s>
31	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
<g/>
:	:	kIx,
</s>
<s>
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
<g/>
:	:	kIx,
Po	po	k7c6
sezóně	sezóna	k1gFnSc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
reorganizaci	reorganizace	k1gFnSc3
soutěží	soutěž	k1gFnPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
Divize	divize	k1gFnSc1
D	D	kA
stala	stát	k5eAaPmAgFnS
jednou	jeden	k4xCgFnSc7
ze	z	k7c2
skupin	skupina	k1gFnPc2
4	#num#	k4
<g/>
.	.	kIx.
nejvyšší	vysoký	k2eAgFnSc2d3
soutěže	soutěž	k1gFnSc2
-	-	kIx~
prvních	první	k4xOgNnPc6
9	#num#	k4
mužstev	mužstvo	k1gNnPc2
postoupilo	postoupit	k5eAaPmAgNnS
do	do	k7c2
3	#num#	k4
<g/>
.	.	kIx.
ligy	liga	k1gFnSc2
–	–	k?
sk	sk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
B	B	kA
</s>
<s>
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
<g/>
:	:	kIx,
Po	po	k7c6
sezoně	sezona	k1gFnSc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
plošnému	plošný	k2eAgNnSc3d1
rušení	rušení	k1gNnSc3
rezervních	rezervní	k2eAgInPc2d1
týmů	tým	k1gInPc2
(	(	kIx(
<g/>
B-mužstev	B-mužstvo	k1gNnPc2
<g/>
)	)	kIx)
prvoligových	prvoligový	k2eAgInPc2d1
týmů	tým	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
**	**	k?
<g/>
=	=	kIx~
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
<g/>
:	:	kIx,
Sezona	sezona	k1gFnSc1
byla	být	k5eAaImAgFnS
předčasně	předčasně	k6eAd1
ukončena	ukončit	k5eAaPmNgFnS
kvůli	kvůli	k7c3
pandemii	pandemie	k1gFnSc3
Covid-	Covid-	k1gFnSc1
<g/>
19	#num#	k4
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
–	–	k?
juniorský	juniorský	k2eAgInSc1d1
tým	tým	k1gInSc1
</s>
<s>
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
–	–	k?
juniorský	juniorský	k2eAgInSc1d1
tým	tým	k1gInSc1
byl	být	k5eAaImAgInS
juniorský	juniorský	k2eAgInSc1d1
tým	tým	k1gInSc1
ostravského	ostravský	k2eAgInSc2d1
Baníku	Baník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2016	#num#	k4
byl	být	k5eAaImAgMnS
po	po	k7c6
odhlášení	odhlášení	k1gNnSc6
z	z	k7c2
juniorské	juniorský	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
dočasně	dočasně	k6eAd1
neaktivní	aktivní	k2eNgInSc1d1
<g/>
,	,	kIx,
obnoven	obnovit	k5eAaPmNgInS
byl	být	k5eAaImAgInS
na	na	k7c4
novou	nový	k2eAgFnSc4d1
sezónu	sezóna	k1gFnSc4
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
sezóně	sezóna	k1gFnSc6
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
byl	být	k5eAaImAgInS
juniorský	juniorský	k2eAgInSc1d1
tým	tým	k1gInSc1
zrušen	zrušit	k5eAaPmNgInS
společně	společně	k6eAd1
s	s	k7c7
Juniorskou	juniorský	k2eAgFnSc7d1
ligou	liga	k1gFnSc7
<g/>
,	,	kIx,
došlo	dojít	k5eAaPmAgNnS
tak	tak	k9
k	k	k7c3
znovuobnovení	znovuobnovení	k1gNnSc3
rezervních	rezervní	k2eAgInPc2d1
týmů	tým	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Umístění	umístění	k1gNnSc1
v	v	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
sezonách	sezona	k1gFnPc6
</s>
<s>
Stručný	stručný	k2eAgInSc1d1
přehled	přehled	k1gInSc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2012	#num#	k4
<g/>
–	–	k?
<g/>
2016	#num#	k4
<g/>
:	:	kIx,
Juniorská	juniorský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
2016	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
:	:	kIx,
bez	bez	k7c2
soutěže	soutěž	k1gFnSc2
</s>
<s>
2016	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
<g/>
:	:	kIx,
Juniorská	juniorský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1
ročníky	ročník	k1gInPc1
</s>
<s>
Zdroj	zdroj	k1gInSc1
<g/>
:	:	kIx,
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Legenda	legenda	k1gFnSc1
<g/>
:	:	kIx,
Z	Z	kA
-	-	kIx~
zápasy	zápas	k1gInPc1
<g/>
,	,	kIx,
V	v	k7c6
-	-	kIx~
výhry	výhra	k1gFnPc4
<g/>
,	,	kIx,
R	R	kA
-	-	kIx~
remízy	remíz	k1gInPc1
<g/>
,	,	kIx,
P	P	kA
-	-	kIx~
porážky	porážka	k1gFnSc2
<g/>
,	,	kIx,
VG	VG	kA
-	-	kIx~
vstřelené	vstřelený	k2eAgInPc1d1
góly	gól	k1gInPc1
<g/>
,	,	kIx,
OG	OG	kA
-	-	kIx~
obdržené	obdržený	k2eAgInPc4d1
góly	gól	k1gInPc4
<g/>
,	,	kIx,
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
-	-	kIx~
rozdíl	rozdíl	k1gInSc1
skóre	skóre	k1gNnSc2
<g/>
,	,	kIx,
B	B	kA
-	-	kIx~
body	bod	k1gInPc1
<g/>
,	,	kIx,
červené	červený	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
-	-	kIx~
sestup	sestup	k1gInSc1
<g/>
,	,	kIx,
zelené	zelený	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
-	-	kIx~
postup	postup	k1gInSc1
<g/>
,	,	kIx,
fialové	fialový	k2eAgNnSc1d1
podbarvení	podbarvení	k1gNnSc1
-	-	kIx~
reorganizace	reorganizace	k1gFnSc1
<g/>
,	,	kIx,
změna	změna	k1gFnSc1
skupiny	skupina	k1gFnSc2
či	či	k8xC
soutěže	soutěž	k1gFnSc2
</s>
<s>
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
(	(	kIx(
<g/>
2012	#num#	k4
–	–	k?
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Sezóny	sezóna	k1gFnPc1
</s>
<s>
Liga	liga	k1gFnSc1
</s>
<s>
Úroveň	úroveň	k1gFnSc1
</s>
<s>
Z	z	k7c2
</s>
<s>
V	v	k7c6
</s>
<s>
R	R	kA
</s>
<s>
P	P	kA
</s>
<s>
VG	VG	kA
</s>
<s>
OG	OG	kA
</s>
<s>
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
</s>
<s>
B	B	kA
</s>
<s>
Pozice	pozice	k1gFnSc1
</s>
<s>
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
</s>
<s>
Juniorská	juniorský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
13465234375-2823	13465234375-2823	k4
</s>
<s>
18	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
</s>
<s>
Juniorská	juniorský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
140234138360	#num#	k4
<g/>
+	+	kIx~
<g/>
2373	#num#	k4
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
</s>
<s>
Juniorská	juniorský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
13819497954	#num#	k4
<g/>
+	+	kIx~
<g/>
2561	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
</s>
<s>
Juniorská	juniorský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
1321610128161	#num#	k4
<g/>
+	+	kIx~
<g/>
2058	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
</s>
<s>
Juniorský	juniorský	k2eAgInSc1d1
tým	tým	k1gInSc1
nebyl	být	k5eNaImAgInS
přihlášen	přihlásit	k5eAaPmNgInS
v	v	k7c6
žádné	žádný	k3yNgFnSc6
soutěži	soutěž	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
</s>
<s>
Juniorská	juniorský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
</s>
<s>
1	#num#	k4
</s>
<s>
30	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
8	#num#	k4
</s>
<s>
11	#num#	k4
</s>
<s>
53	#num#	k4
</s>
<s>
54	#num#	k4
</s>
<s>
-1	-1	k4
</s>
<s>
41	#num#	k4
</s>
<s>
8	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
</s>
<s>
Juniorská	juniorský	k2eAgFnSc1d1
liga	liga	k1gFnSc1
(	(	kIx(
<g/>
Finále	finále	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
19117931-224	19117931-224	k4
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
1	#num#	k4
2	#num#	k4
"	"	kIx"
<g/>
Historie	historie	k1gFnSc1
klubu	klub	k1gInSc2
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
fcb	fcb	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Stadion	stadion	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Baník	Baník	k1gInSc1
změnil	změnit	k5eAaPmAgMnS
majitele	majitel	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Peterův	Peterův	k2eAgInSc4d1
podíl	podíl	k1gInSc4
získala	získat	k5eAaPmAgFnS
SMK	SMK	kA
Reality	realita	k1gFnSc2
Invest	Invest	k1gInSc4
na	na	k7c4
Sport	sport	k1gInSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Transparentní	transparentní	k2eAgInSc1d1
účet	účet	k1gInSc1
vydal	vydat	k5eAaPmAgInS
první	první	k4xOgFnSc4
část	část	k1gFnSc4
financí	finance	k1gFnPc2
za	za	k7c4
rekonstrukci	rekonstrukce	k1gFnSc4
hřiště	hřiště	k1gNnSc2
Archivováno	archivovat	k5eAaBmNgNnS
21	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2012	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
Michal	Michal	k1gMnSc1
Frydrych	Frydrych	k1gMnSc1
přestoupil	přestoupit	k5eAaPmAgMnS
do	do	k7c2
Slavie	slavie	k1gFnSc2
Praha	Praha	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Statistiky	statistika	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fortuna	Fortuna	k1gFnSc1
národní	národní	k2eAgFnSc1d1
liga	liga	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Baník	Baník	k1gInSc1
pod	pod	k7c7
lupou	lupa	k1gFnSc7
<g/>
:	:	kIx,
Vytáhnout	vytáhnout	k5eAaPmF
tým	tým	k1gInSc4
z	z	k7c2
bryndy	brynda	k1gFnSc2
mají	mít	k5eAaImIp3nP
Fillo	Filla	k1gMnSc5
s	s	k7c7
Laštůvkou	laštůvka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
HET	HET	kA
liga	liga	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Trenér	trenér	k1gMnSc1
Radim	Radim	k1gMnSc1
Kučera	Kučera	k1gMnSc1
u	u	k7c2
mužstva	mužstvo	k1gNnSc2
skončil	skončit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
FK	FK	kA
Mladá	mladá	k1gFnSc1
Boleslav	Boleslav	k1gMnSc1
–	–	k?
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
2	#num#	k4
<g/>
:	:	kIx,
<g/>
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
–	–	k?
FC	FC	kA
Zbrojovka	zbrojovka	k1gFnSc1
Brno	Brno	k1gNnSc1
2	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
http://hymna.cz/fotbalovy-klub/fc-banik-ostrava/	http://hymna.cz/fotbalovy-klub/fc-banik-ostrava/	k?
<g/>
↑	↑	k?
Tažení	tažení	k1gNnSc4
za	za	k7c4
slušné	slušný	k2eAgMnPc4d1
fanoušky	fanoušek	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Baník	Baník	k1gInSc1
pi	pi	k0
<g/>
*	*	kIx~
<g/>
o	o	k7c6
se	se	k3xPyFc4
snad	snad	k9
může	moct	k5eAaImIp3nS
<g/>
,	,	kIx,
zní	znět	k5eAaImIp3nS
z	z	k7c2
Bazalů	Bazal	k1gInPc2
<g/>
,	,	kIx,
iSport	iSport	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
cit	cit	k1gInSc1
<g/>
.	.	kIx.
20	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
↑	↑	k?
Historia	Historium	k1gNnPc1
śląsko-czeskiej	śląsko-czeskiej	k1gInSc1
sztamy	sztam	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Banik	Banik	k1gMnSc1
Ostrawa	Ostrawa	k1gMnSc1
i	i	k8xC
GKS	GKS	kA
Katowice	Katowice	k1gFnSc2
<g/>
,	,	kIx,
dziennikzachodni	dziennikzachodnit	k5eAaPmRp2nS
<g/>
.	.	kIx.
<g/>
pl	pl	k?
<g/>
,	,	kIx,
citováno	citován	k2eAgNnSc4d1
25	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2014	#num#	k4
(	(	kIx(
<g/>
polsky	polsky	k6eAd1
<g/>
)	)	kIx)
<g/>
↑	↑	k?
Přísnější	přísný	k2eAgInPc4d2
tresty	trest	k1gInPc4
pro	pro	k7c4
násilnické	násilnický	k2eAgMnPc4d1
sportovní	sportovní	k2eAgMnPc4d1
fanoušky	fanoušek	k1gMnPc4
<g/>
↑	↑	k?
Za	za	k7c4
masakr	masakr	k1gInSc4
na	na	k7c6
Baníku	Baník	k1gInSc6
můžou	můžou	k?
Poláci	Polák	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neumíme	umět	k5eNaImIp1nP
jim	on	k3xPp3gMnPc3
fotbal	fotbal	k1gInSc4
zakázat	zakázat	k5eAaPmF
<g/>
,	,	kIx,
říká	říkat	k5eAaImIp3nS
FAČR	FAČR	kA
-	-	kIx~
iSport	iSport	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
iSport	iSport	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Příští	příští	k2eAgFnSc1d1
sezona	sezona	k1gFnSc1
bude	být	k5eAaImBp3nS
lepší	dobrý	k2eAgFnSc1d2
<g/>
,	,	kIx,
slyšel	slyšet	k5eAaImAgMnS
Baroš	Baroš	k1gMnSc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2002-05-14	2002-05-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Osmnáctiletý	osmnáctiletý	k2eAgMnSc1d1
fotbalový	fotbalový	k2eAgMnSc1d1
útočník	útočník	k1gMnSc1
Vydra	Vydra	k1gMnSc1
jde	jít	k5eAaImIp3nS
z	z	k7c2
Ostravy	Ostrava	k1gFnSc2
za	za	k7c4
sto	sto	k4xCgNnSc4
milionů	milion	k4xCgInPc2
do	do	k7c2
Udine	Udin	k1gInSc5
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-06-16	2010-06-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
SEZNAM	seznam	k1gInSc1
<g/>
.	.	kIx.
<g/>
CZ	CZ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svěrkoš	Svěrkoš	k1gMnSc1
už	už	k9
trénuje	trénovat	k5eAaImIp3nS
se	se	k3xPyFc4
Sochaux	Sochaux	k1gInSc4
<g/>
,	,	kIx,
smlouvu	smlouva	k1gFnSc4
podepíše	podepsat	k5eAaPmIp3nS
na	na	k7c4
Nový	nový	k2eAgInSc4d1
rok	rok	k1gInSc4
<g/>
.	.	kIx.
www.sport.cz	www.sport.cz	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
www.fcb.cz	www.fcb.cz	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
18	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
2013	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
24	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
2012	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://www.ceskatelevize.cz/sport/fotbal/358431-banik-se-do-ligy-vratil-vitezstvim-v-brne-rozhodl-navratilec-baros/	http://www.ceskatelevize.cz/sport/fotbal/358431-banik-se-do-ligy-vratil-vitezstvim-v-brne-rozhodl-navratilec-baros/	k4
<g/>
↑	↑	k?
Barošův	Barošův	k2eAgInSc1d1
konec	konec	k1gInSc1
<g/>
:	:	kIx,
překvapení	překvapení	k1gNnSc1
<g/>
,	,	kIx,
slzy	slza	k1gFnPc1
i	i	k8xC
ohromná	ohromný	k2eAgFnSc1d1
šance	šance	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sluníčko	sluníčko	k1gNnSc1
<g/>
,	,	kIx,
usmál	usmát	k5eAaPmAgMnS
se	se	k3xPyFc4
<g/>
.	.	kIx.
iSport	iSport	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
Archiv	archiv	k1gInSc1
Rudého	rudý	k2eAgNnSc2d1
práva	právo	k1gNnSc2
<g/>
,	,	kIx,
1965-19891	1965-19891	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
Nižší	nízký	k2eAgFnSc2d2
soutěže	soutěž	k1gFnSc2
<g/>
,	,	kIx,
František	František	k1gMnSc1
Kopecký	Kopecký	k1gMnSc1
(	(	kIx(
<g/>
jfk-fotbal	jfk-fotbal	k1gInSc1
<g/>
)	)	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
Jindřich	Jindřich	k1gMnSc1
Horák	Horák	k1gMnSc1
<g/>
,	,	kIx,
Lubomír	Lubomír	k1gMnSc1
Král	Král	k1gMnSc1
<g/>
:	:	kIx,
Encyklopedie	encyklopedie	k1gFnSc1
našeho	náš	k3xOp1gInSc2
fotbalu	fotbal	k1gInSc2
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
1896	#num#	k4
–	–	k?
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
Libri	Libre	k1gFnSc4
1997.1	1997.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
Archiv	archiv	k1gInSc1
soutěží	soutěžit	k5eAaImIp3nS
<g/>
,	,	kIx,
výsledkový	výsledkový	k2eAgInSc1d1
servis	servis	k1gInSc1
Lidových	lidový	k2eAgFnPc2d1
novin	novina	k1gFnPc2
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
11	#num#	k4
12	#num#	k4
13	#num#	k4
14	#num#	k4
15	#num#	k4
"	"	kIx"
<g/>
Přehled	přehled	k1gInSc1
umístění	umístění	k1gNnSc2
v	v	k7c6
lize	liga	k1gFnSc6
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
fcb	fcb	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
"	"	kIx"
<g/>
SK	Sk	kA
Vsetín	Vsetín	k1gInSc1
do	do	k7c2
Moravskoslezské	moravskoslezský	k2eAgFnSc2d1
divize	divize	k1gFnSc2
<g/>
"	"	kIx"
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
fcvsetin	fcvsetin	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
17.06	17.06	k4
<g/>
.2013	.2013	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Fotbalová	fotbalový	k2eAgFnSc1d1
Ostrava	Ostrava	k1gFnSc1
má	mít	k5eAaImIp3nS
nového	nový	k2eAgMnSc4d1
kouče	kouč	k1gMnSc4
<g/>
,	,	kIx,
klub	klub	k1gInSc1
do	do	k7c2
funkce	funkce	k1gFnSc2
potvrdil	potvrdit	k5eAaPmAgMnS
Svědíka	Svědík	k1gMnSc4
<g/>
,	,	kIx,
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
cit	cit	k1gInSc1
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
↑	↑	k?
Komňacký	Komňacký	k1gMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
hlavním	hlavní	k2eAgMnSc7d1
trenérem	trenér	k1gMnSc7
Baníku	Baník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Baník	Baník	k1gInSc1
ukončil	ukončit	k5eAaPmAgInS
spolupráci	spolupráce	k1gFnSc4
s	s	k7c7
trenérem	trenér	k1gMnSc7
Komňackým	Komňacký	k1gMnSc7
Archivováno	archivován	k2eAgNnSc4d1
17	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gMnSc5
<g/>
,	,	kIx,
fcb	fcb	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Baník	Baník	k1gInSc1
má	mít	k5eAaImIp3nS
širší	široký	k2eAgInSc1d2
realizační	realizační	k2eAgInSc1d1
tým	tým	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Shodneme	shodnout	k5eAaPmIp1nP,k5eAaBmIp1nP
se	se	k3xPyFc4
<g/>
,	,	kIx,
tvrdí	tvrdit	k5eAaImIp3nS
Slončík	Slončík	k1gMnSc1
<g/>
↑	↑	k?
Je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
překonávat	překonávat	k5eAaImF
klišé	klišé	k1gNnSc4
<g/>
,	,	kIx,
říká	říkat	k5eAaImIp3nS
nový	nový	k2eAgMnSc1d1
kouč	kouč	k1gMnSc1
Ostravy	Ostrava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obavy	obava	k1gFnPc1
nemá	mít	k5eNaImIp3nS
<g/>
↑	↑	k?
Baník	Baník	k1gInSc1
místo	místo	k7c2
trenéra	trenér	k1gMnSc2
Frňky	Frňka	k1gMnSc2
povede	vést	k5eAaImIp3nS,k5eAaPmIp3nS
Korytář	korytář	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Fotbalisty	fotbalista	k1gMnSc2
Baníku	Baník	k1gInSc2
Ostrava	Ostrava	k1gFnSc1
povede	povést	k5eAaPmIp3nS,k5eAaImIp3nS
na	na	k7c6
jaře	jaro	k1gNnSc6
Vlastimil	Vlastimil	k1gMnSc1
Petržela	Petržela	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Moravskoslezský	moravskoslezský	k2eAgInSc1d1
deník	deník	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Petržela	Petržela	k1gMnSc1
<g/>
:	:	kIx,
Záchrana	záchrana	k1gFnSc1
je	být	k5eAaImIp3nS
nemožná	možný	k2eNgFnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
věřím	věřit	k5eAaImIp1nS
v	v	k7c6
obrodu	obrod	k1gInSc6
Baníku	Baník	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sportovní	sportovní	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
(	(	kIx(
<g/>
ČTK	ČTK	kA
<g/>
)	)	kIx)
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-12-28	2015-12-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Baník	Baník	k1gInSc1
nemá	mít	k5eNaImIp3nS
na	na	k7c4
záchranu	záchrana	k1gFnSc4
šanci	šance	k1gFnSc4
<g/>
,	,	kIx,
soudí	soudit	k5eAaImIp3nS
Petržela	Petržela	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Druhou	druhý	k4xOgFnSc7
ligou	liga	k1gFnSc7
chce	chtít	k5eAaImIp3nS
prolétnout	prolétnout	k5eAaPmF
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2015-12-29	2015-12-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Petržela	Petržela	k1gMnSc1
už	už	k6eAd1
trénovat	trénovat	k5eAaImF
Baník	Baník	k1gInSc4
nebude	být	k5eNaImBp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
jeho	jeho	k3xOp3gMnSc6
nástupci	nástupce	k1gMnSc6
klub	klub	k1gInSc1
intenzivně	intenzivně	k6eAd1
jedná	jednat	k5eAaImIp3nS
<g/>
↑	↑	k?
Podepsáno	podepsán	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Baník	baník	k1gMnSc1
povede	vést	k5eAaImIp3nS,k5eAaPmIp3nS
Radim	Radim	k1gMnSc1
Kučera	Kučera	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Trenér	trenér	k1gMnSc1
Radim	Radim	k1gMnSc1
Kučera	Kučera	k1gMnSc1
skončil	skončit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Baník	Baník	k1gInSc1
přebírá	přebírat	k5eAaImIp3nS
Bohumil	Bohumil	k1gMnSc1
Páník	Páník	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Divize	divize	k1gFnSc1
E	E	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
eurofotbal	eurofotbal	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2015	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
https://tv.isport.blesk.cz/video/4667451/juniorska-liga-konci-od-sezony-2019-2020-se-vrati-becka.html	https://tv.isport.blesk.cz/video/4667451/juniorska-liga-konci-od-sezony-2019-2020-se-vrati-becka.html	k1gInSc1
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
Jindřich	Jindřich	k1gMnSc1
Horák	Horák	k1gMnSc1
<g/>
,	,	kIx,
Lubomír	Lubomír	k1gMnSc1
Král	Král	k1gMnSc1
<g/>
:	:	kIx,
Encyklopedie	encyklopedie	k1gFnSc1
našeho	náš	k3xOp1gInSc2
fotbalu	fotbal	k1gInSc2
(	(	kIx(
<g/>
1896	#num#	k4
–	–	k?
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Libri	Libri	k1gNnSc7
1997	#num#	k4
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
FC	FC	kA
Baník	Baník	k1gInSc4
Ostrava	Ostrava	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
klubu	klub	k1gInSc2
</s>
<s>
Klubové	klubový	k2eAgInPc1d1
rekordy	rekord	k1gInPc1
</s>
<s>
Stránky	stránka	k1gFnPc1
fanoušků	fanoušek	k1gMnPc2
klubu	klub	k1gInSc2
</s>
<s>
CHACHAŘI	chachar	k1gMnPc1
<g/>
,	,	kIx,
oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
ULTRAS	ULTRAS	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
</s>
<s>
Baník	baník	k1gMnSc1
Ostrava	Ostrava	k1gFnSc1
v	v	k7c6
evropských	evropský	k2eAgInPc6d1
fotbalových	fotbalový	k2eAgInPc6d1
pohárech	pohár	k1gInPc6
</s>
<s>
Lexikon	lexikon	k1gInSc1
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
<g/>
/	/	kIx~
<g/>
Zahlen	Zahlen	k2eAgInSc1d1
und	und	k?
Fakten	Faktno	k1gNnPc2
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
1	#num#	k4
<g/>
.	.	kIx.
česká	český	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
–	–	k?
česká	český	k2eAgFnSc1d1
nejvyšší	vysoký	k2eAgFnSc1d3
soutěž	soutěž	k1gFnSc1
ve	v	k7c6
fotbale	fotbal	k1gInSc6
(	(	kIx(
<g/>
ročníky	ročník	k1gInPc1
<g/>
,	,	kIx,
týmy	tým	k1gInPc1
<g/>
,	,	kIx,
historie	historie	k1gFnPc1
atd.	atd.	kA
<g/>
)	)	kIx)
Kluby	klub	k1gInPc7
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
(	(	kIx(
<g/>
18	#num#	k4
klubů	klub	k1gInPc2
<g/>
)	)	kIx)
</s>
<s>
SK	Sk	kA
Dynamo	dynamo	k1gNnSc1
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
•	•	k?
FK	FK	kA
Jablonec	Jablonec	k1gInSc1
•	•	k?
MFK	MFK	kA
Karviná	Karviná	k1gFnSc1
•	•	k?
FC	FC	kA
Slovan	Slovan	k1gInSc1
Liberec	Liberec	k1gInSc1
•	•	k?
FK	FK	kA
Mladá	mladá	k1gFnSc1
Boleslav	Boleslav	k1gMnSc1
•	•	k?
SK	Sk	kA
Sigma	sigma	k1gNnSc2
Olomouc	Olomouc	k1gFnSc1
•	•	k?
SFC	SFC	kA
Opava	Opava	k1gFnSc1
•	•	k?
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
•	•	k?
FC	FC	kA
Viktoria	Viktoria	k1gFnSc1
Plzeň	Plzeň	k1gFnSc1
•	•	k?
Bohemians	Bohemians	k1gInSc1
Praha	Praha	k1gFnSc1
1905	#num#	k4
•	•	k?
SK	Sk	kA
Slavia	Slavia	k1gFnSc1
Praha	Praha	k1gFnSc1
•	•	k?
AC	AC	kA
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
•	•	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FK	FK	kA
Příbram	Příbram	k1gFnSc1
•	•	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Slovácko	Slovácko	k1gNnSc1
•	•	k?
FK	FK	kA
Teplice	teplice	k1gFnSc2
•	•	k?
FC	FC	kA
Fastav	Fastav	k1gFnSc2
Zlín	Zlín	k1gInSc1
•	•	k?
FK	FK	kA
Pardubice	Pardubice	k1gInPc1
•	•	k?
FC	FC	kA
Zbrojovka	zbrojovka	k1gFnSc1
Brno	Brno	k1gNnSc1
Stadiony	stadion	k1gInPc1
</s>
<s>
České	český	k2eAgInPc1d1
Budějovice	Budějovice	k1gInPc1
•	•	k?
Jablonec	Jablonec	k1gInSc1
•	•	k?
Karviná	Karviná	k1gFnSc1
•	•	k?
Liberec	Liberec	k1gInSc1
•	•	k?
Mladá	mladá	k1gFnSc1
Boleslav	Boleslav	k1gMnSc1
•	•	k?
Olomouc	Olomouc	k1gFnSc1
•	•	k?
Opava	Opava	k1gFnSc1
•	•	k?
Ostrava	Ostrava	k1gFnSc1
•	•	k?
Plzeň	Plzeň	k1gFnSc1
•	•	k?
Bohemians	Bohemians	k1gInSc1
Praha	Praha	k1gFnSc1
•	•	k?
Slavia	Slavia	k1gFnSc1
Praha	Praha	k1gFnSc1
•	•	k?
Sparta	Sparta	k1gFnSc1
Praha	Praha	k1gFnSc1
•	•	k?
Příbram	Příbram	k1gFnSc1
•	•	k?
Slovácko	Slovácko	k1gNnSc1
•	•	k?
Teplice	teplice	k1gFnSc2
•	•	k?
Zlín	Zlín	k1gInSc1
•	•	k?
Pardubice	Pardubice	k1gInPc1
•	•	k?
Brno	Brno	k1gNnSc1
Sezóny	sezóna	k1gFnSc2
(	(	kIx(
<g/>
historický	historický	k2eAgInSc4d1
přehled	přehled	k1gInSc4
<g/>
)	)	kIx)
</s>
<s>
Československo	Československo	k1gNnSc1
<g/>
:	:	kIx,
1925	#num#	k4
•	•	k?
1925	#num#	k4
<g/>
/	/	kIx~
<g/>
26	#num#	k4
•	•	k?
1927	#num#	k4
•	•	k?
1927	#num#	k4
<g/>
/	/	kIx~
<g/>
28	#num#	k4
•	•	k?
1928	#num#	k4
<g/>
/	/	kIx~
<g/>
29	#num#	k4
•	•	k?
1929	#num#	k4
<g/>
/	/	kIx~
<g/>
30	#num#	k4
•	•	k?
1930	#num#	k4
<g/>
/	/	kIx~
<g/>
31	#num#	k4
•	•	k?
1931	#num#	k4
<g/>
/	/	kIx~
<g/>
32	#num#	k4
•	•	k?
1932	#num#	k4
<g/>
/	/	kIx~
<g/>
33	#num#	k4
•	•	k?
1933	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
/	/	kIx~
<g/>
34	#num#	k4
•	•	k?
1934	#num#	k4
<g/>
/	/	kIx~
<g/>
35	#num#	k4
•	•	k?
1935	#num#	k4
<g/>
/	/	kIx~
<g/>
36	#num#	k4
•	•	k?
1936	#num#	k4
<g/>
/	/	kIx~
<g/>
37	#num#	k4
•	•	k?
1937	#num#	k4
<g/>
/	/	kIx~
<g/>
38	#num#	k4
•	•	k?
1938	#num#	k4
<g/>
/	/	kIx~
<g/>
39	#num#	k4
Protektorát	protektorát	k1gInSc1
Čechy	Čechy	k1gFnPc1
a	a	k8xC
Morava	Morava	k1gFnSc1
<g/>
:	:	kIx,
1939	#num#	k4
<g/>
/	/	kIx~
<g/>
40	#num#	k4
•	•	k?
1940	#num#	k4
<g/>
/	/	kIx~
<g/>
41	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1941	#num#	k4
<g/>
/	/	kIx~
<g/>
42	#num#	k4
•	•	k?
1942	#num#	k4
<g/>
/	/	kIx~
<g/>
43	#num#	k4
•	•	k?
1943	#num#	k4
<g/>
/	/	kIx~
<g/>
44	#num#	k4
Československo	Československo	k1gNnSc1
<g/>
:	:	kIx,
1945	#num#	k4
<g/>
/	/	kIx~
<g/>
46	#num#	k4
•	•	k?
1946	#num#	k4
<g/>
/	/	kIx~
<g/>
47	#num#	k4
•	•	k?
1947	#num#	k4
<g/>
/	/	kIx~
<g/>
48	#num#	k4
•	•	k?
1949	#num#	k4
•	•	k?
1950	#num#	k4
•	•	k?
1951	#num#	k4
•	•	k?
1952	#num#	k4
•	•	k?
1953	#num#	k4
•	•	k?
1954	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1955	#num#	k4
•	•	k?
1956	#num#	k4
•	•	k?
1957	#num#	k4
<g/>
/	/	kIx~
<g/>
58	#num#	k4
•	•	k?
1958	#num#	k4
<g/>
/	/	kIx~
<g/>
59	#num#	k4
•	•	k?
1959	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
•	•	k?
1960	#num#	k4
<g/>
/	/	kIx~
<g/>
61	#num#	k4
•	•	k?
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
•	•	k?
1962	#num#	k4
<g/>
/	/	kIx~
<g/>
63	#num#	k4
•	•	k?
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
•	•	k?
1964	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
65	#num#	k4
•	•	k?
1965	#num#	k4
<g/>
/	/	kIx~
<g/>
66	#num#	k4
•	•	k?
1966	#num#	k4
<g/>
/	/	kIx~
<g/>
67	#num#	k4
•	•	k?
1967	#num#	k4
<g/>
/	/	kIx~
<g/>
68	#num#	k4
•	•	k?
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
•	•	k?
1969	#num#	k4
<g/>
/	/	kIx~
<g/>
70	#num#	k4
•	•	k?
1970	#num#	k4
<g/>
/	/	kIx~
<g/>
71	#num#	k4
•	•	k?
1971	#num#	k4
<g/>
/	/	kIx~
<g/>
72	#num#	k4
•	•	k?
1972	#num#	k4
<g/>
/	/	kIx~
<g/>
73	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
1973	#num#	k4
<g/>
/	/	kIx~
<g/>
74	#num#	k4
•	•	k?
1974	#num#	k4
<g/>
/	/	kIx~
<g/>
75	#num#	k4
•	•	k?
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
•	•	k?
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
•	•	k?
1977	#num#	k4
<g/>
/	/	kIx~
<g/>
78	#num#	k4
•	•	k?
1978	#num#	k4
<g/>
/	/	kIx~
<g/>
79	#num#	k4
•	•	k?
1979	#num#	k4
<g/>
/	/	kIx~
<g/>
80	#num#	k4
•	•	k?
1980	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
•	•	k?
1981	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
82	#num#	k4
•	•	k?
1982	#num#	k4
<g/>
/	/	kIx~
<g/>
83	#num#	k4
•	•	k?
1983	#num#	k4
<g/>
/	/	kIx~
<g/>
84	#num#	k4
•	•	k?
1984	#num#	k4
<g/>
/	/	kIx~
<g/>
85	#num#	k4
•	•	k?
1985	#num#	k4
<g/>
/	/	kIx~
<g/>
86	#num#	k4
•	•	k?
1986	#num#	k4
<g/>
/	/	kIx~
<g/>
87	#num#	k4
•	•	k?
1987	#num#	k4
<g/>
/	/	kIx~
<g/>
88	#num#	k4
•	•	k?
1988	#num#	k4
<g/>
/	/	kIx~
<g/>
89	#num#	k4
•	•	k?
1989	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
90	#num#	k4
•	•	k?
1990	#num#	k4
<g/>
/	/	kIx~
<g/>
91	#num#	k4
•	•	k?
1991	#num#	k4
<g/>
/	/	kIx~
<g/>
92	#num#	k4
•	•	k?
1992	#num#	k4
<g/>
/	/	kIx~
<g/>
93	#num#	k4
Česko	Česko	k1gNnSc1
<g/>
:	:	kIx,
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
•	•	k?
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
•	•	k?
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
•	•	k?
1996	#num#	k4
<g/>
/	/	kIx~
<g/>
97	#num#	k4
•	•	k?
1997	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
98	#num#	k4
•	•	k?
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
•	•	k?
1999	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
0	#num#	k4
•	•	k?
2000	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
•	•	k?
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
•	•	k?
2002	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
•	•	k?
2003	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
•	•	k?
2004	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
•	•	k?
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
•	•	k?
2006	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
•	•	k?
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
•	•	k?
2008	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
•	•	k?
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
•	•	k?
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
•	•	k?
2012	#num#	k4
<g/>
/	/	kIx~
<g/>
13	#num#	k4
•	•	k?
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
•	•	k?
2014	#num#	k4
<g/>
/	/	kIx~
<g/>
15	#num#	k4
•	•	k?
2015	#num#	k4
<g/>
/	/	kIx~
<g/>
16	#num#	k4
•	•	k?
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
•	•	k?
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
•	•	k?
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
•	•	k?
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
•	•	k?
2020	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
Dřívější	dřívější	k2eAgMnPc1d1
účastníci	účastník	k1gMnPc1
(	(	kIx(
<g/>
poslední	poslední	k2eAgFnSc1d1
sezóna	sezóna	k1gFnSc1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
pozn	pozn	kA
<g/>
.	.	kIx.
<g/>
:	:	kIx,
v	v	k7c6
ČSR	ČSR	kA
éře	éra	k1gFnSc6
pouze	pouze	k6eAd1
české	český	k2eAgInPc1d1
kluby	klub	k1gInPc1
<g/>
)	)	kIx)
</s>
<s>
SK	Sk	kA
Čechie	Čechie	k1gFnSc1
Praha	Praha	k1gFnSc1
VIII	VIII	kA
(	(	kIx(
<g/>
1925	#num#	k4
<g/>
/	/	kIx~
<g/>
26	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Slavoj	Slavoj	k1gInSc1
Žižkov	Žižkov	k1gInSc1
(	(	kIx(
<g/>
1925	#num#	k4
<g/>
/	/	kIx~
<g/>
26	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Nuselský	nuselský	k2eAgInSc4d1
SK	Sk	kA
(	(	kIx(
<g/>
1927	#num#	k4
<g/>
)	)	kIx)
•	•	k?
ČAFC	ČAFC	kA
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
1929	#num#	k4
<g/>
/	/	kIx~
<g/>
30	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FK	FK	kA
Meteor	meteor	k1gInSc1
Praha	Praha	k1gFnSc1
VIII	VIII	kA
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
1930	#num#	k4
<g/>
/	/	kIx~
<g/>
31	#num#	k4
<g/>
)	)	kIx)
•	•	k?
AFK	AFK	kA
Kolín	Kolín	k1gInSc1
(	(	kIx(
<g/>
1935	#num#	k4
<g/>
/	/	kIx~
<g/>
36	#num#	k4
<g/>
)	)	kIx)
•	•	k?
DFC	DFC	kA
Prag	Prag	k1gInSc1
(	(	kIx(
<g/>
1935	#num#	k4
<g/>
/	/	kIx~
<g/>
36	#num#	k4
<g/>
)	)	kIx)
•	•	k?
DSV	DSV	kA
Saaz	Saaz	k1gInSc1
(	(	kIx(
<g/>
1935	#num#	k4
<g/>
/	/	kIx~
<g/>
36	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Teplitzer	Teplitzer	k1gInSc1
FK	FK	kA
(	(	kIx(
<g/>
1935	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
36	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SK	Sk	kA
Moravská	moravský	k2eAgFnSc1d1
Slavia	Slavia	k1gFnSc1
Brno	Brno	k1gNnSc4
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
/	/	kIx~
<g/>
37	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SK	Sk	kA
Rusj	Rusj	k1gInSc1
Užhorod	Užhorod	k1gInSc1
(	(	kIx(
<g/>
1936	#num#	k4
<g/>
/	/	kIx~
<g/>
37	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FK	FK	kA
Náchod	Náchod	k1gInSc1
(	(	kIx(
<g/>
1939	#num#	k4
<g/>
/	/	kIx~
<g/>
40	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SK	Sk	kA
Nusle	Nusle	k1gFnPc1
(	(	kIx(
<g/>
1943	#num#	k4
<g/>
/	/	kIx~
<g/>
44	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SK	Sk	kA
Polaban	Polaban	k1gMnSc1
Nymburk	Nymburk	k1gInSc1
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
/	/	kIx~
<g/>
46	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SK	Sk	kA
Pardubice	Pardubice	k1gInPc1
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
/	/	kIx~
<g/>
46	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
SK	Sk	kA
Prostějov	Prostějov	k1gInSc1
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
/	/	kIx~
<g/>
46	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SK	Sk	kA
Rakovník	Rakovník	k1gInSc1
(	(	kIx(
<g/>
1945	#num#	k4
<g/>
/	/	kIx~
<g/>
46	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SK	Sk	kA
Libeň	Libeň	k1gFnSc1
(	(	kIx(
<g/>
1946	#num#	k4
<g/>
/	/	kIx~
<g/>
47	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SK	Sk	kA
Olomouc	Olomouc	k1gFnSc1
ASO	ASO	kA
(	(	kIx(
<g/>
1946	#num#	k4
<g/>
/	/	kIx~
<g/>
47	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SK	Sk	kA
<g />
.	.	kIx.
</s>
<s hack="1">
Plzeň	Plzeň	k1gFnSc1
(	(	kIx(
<g/>
1950	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SK	Sk	kA
Čechie	Čechie	k1gFnSc1
Karlín	Karlín	k1gInSc1
(	(	kIx(
<g/>
1951	#num#	k4
<g/>
)	)	kIx)
•	•	k?
ČAFC	ČAFC	kA
Židenice	Židenice	k1gFnSc2
2011	#num#	k4
(	(	kIx(
<g/>
1952	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Slavoj	Slavoj	k1gInSc1
Liberec	Liberec	k1gInSc1
(	(	kIx(
<g/>
1953	#num#	k4
<g/>
)	)	kIx)
•	•	k?
VTJ	VTJ	kA
Dukla	Dukla	k1gFnSc1
Olomouc	Olomouc	k1gFnSc1
(	(	kIx(
<g/>
1954	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Jiskra	jiskra	k1gFnSc1
Liberec	Liberec	k1gInSc1
(	(	kIx(
<g/>
1955	#num#	k4
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Dukla	Dukla	k1gFnSc1
Pardubice	Pardubice	k1gInPc1
(	(	kIx(
<g/>
1959	#num#	k4
<g/>
/	/	kIx~
<g/>
60	#num#	k4
<g/>
)	)	kIx)
•	•	k?
TJ	tj	kA
Rudá	rudý	k2eAgFnSc1d1
hvězda	hvězda	k1gFnSc1
Brno	Brno	k1gNnSc4
(	(	kIx(
<g/>
1960	#num#	k4
<g/>
/	/	kIx~
<g/>
61	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FC	FC	kA
LeRK	LeRK	k1gFnSc7
Brno	Brno	k1gNnSc1
(	(	kIx(
<g/>
1961	#num#	k4
<g/>
/	/	kIx~
<g/>
62	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FK	FK	kA
Motorlet	motorlet	k1gInSc1
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
1963	#num#	k4
<g/>
/	/	kIx~
<g/>
64	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
SK	Sk	kA
Baťov	Baťov	k1gInSc4
1930	#num#	k4
(	(	kIx(
<g/>
1964	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FK	FK	kA
Pardubice	Pardubice	k1gInPc1
1899	#num#	k4
(	(	kIx(
<g/>
1968	#num#	k4
<g/>
/	/	kIx~
<g/>
69	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FK	FK	kA
Fotbal	fotbal	k1gInSc1
Třinec	Třinec	k1gInSc1
(	(	kIx(
<g/>
1975	#num#	k4
<g/>
/	/	kIx~
<g/>
76	#num#	k4
<g/>
)	)	kIx)
•	•	k?
MFK	MFK	kA
Frýdek-Místek	Frýdek-Místek	k1gInSc1
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
/	/	kIx~
<g/>
77	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
•	•	k?
MFK	MFK	kA
Vítkovice	Vítkovice	k1gInPc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
/	/	kIx~
<g/>
94	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SK	Sk	kA
Benešov	Benešov	k1gInSc1
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
/	/	kIx~
<g/>
95	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FC	FC	kA
Union	union	k1gInSc1
Cheb	Cheb	k1gInSc1
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FC	FC	kA
SYNOT	SYNOT	kA
Slovácká	slovácký	k2eAgFnSc1d1
Slavia	Slavia	k1gFnSc1
Uherské	uherský	k2eAgNnSc1d1
Hradiště	Hradiště	k1gNnSc1
(	(	kIx(
<g/>
1995	#num#	k4
<g/>
/	/	kIx~
<g/>
96	#num#	k4
<g/>
)	)	kIx)
•	•	k?
AFK	AFK	kA
Atlantic	Atlantice	k1gFnPc2
Lázně	lázeň	k1gFnSc2
Bohdaneč	Bohdaneč	k1gMnSc1
(	(	kIx(
<g/>
1997	#num#	k4
<g/>
/	/	kIx~
<g/>
98	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FC	FC	kA
Karviná	Karviná	k1gFnSc1
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
/	/	kIx~
<g/>
99	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FK	FK	kA
Drnovice	Drnovice	k1gInPc1
(	(	kIx(
<g/>
2001	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FK	FK	kA
Chmel	chmel	k1gInSc4
Blšany	Blšana	k1gFnSc2
(	(	kIx(
<g/>
2005	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FK	FK	kA
Baník	Baník	k1gInSc1
Most	most	k1gInSc1
1909	#num#	k4
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
/	/	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
)	)	kIx)
•	•	k?
SK	Sk	kA
Kladno	Kladno	k1gNnSc4
(	(	kIx(
<g/>
2009	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FK	FK	kA
Bohemians	Bohemians	k1gInSc1
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
/	/	kIx~
<g/>
10	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FK	FK	kA
Ústí	ústit	k5eAaImIp3nS
nad	nad	k7c7
Labem	Labe	k1gNnSc7
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
/	/	kIx~
<g/>
11	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FK	FK	kA
Viktoria	Viktoria	k1gFnSc1
Žižkov	Žižkov	k1gInSc1
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
/	/	kIx~
<g/>
12	#num#	k4
<g/>
)	)	kIx)
•	•	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
SC	SC	kA
Znojmo	Znojmo	k1gNnSc1
FK	FK	kA
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
/	/	kIx~
<g/>
14	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FC	FC	kA
Hradec	Hradec	k1gInSc4
Králové	Králová	k1gFnSc2
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
/	/	kIx~
<g/>
17	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FC	FC	kA
Vysočina	vysočina	k1gFnSc1
Jihlava	Jihlava	k1gFnSc1
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
/	/	kIx~
<g/>
18	#num#	k4
<g/>
)	)	kIx)
•	•	k?
FK	FK	kA
Dukla	Dukla	k1gFnSc1
Praha	Praha	k1gFnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
/	/	kIx~
<g/>
19	#num#	k4
<g/>
)	)	kIx)
Ocenění	ocenění	k1gNnSc1
</s>
<s>
Hráč	hráč	k1gMnSc1
a	a	k8xC
trenér	trenér	k1gMnSc1
měsíce	měsíc	k1gInSc2
1	#num#	k4
<g/>
.	.	kIx.
české	český	k2eAgFnSc2d1
fotbalové	fotbalový	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
•	•	k?
Přihraj	přihrát	k5eAaPmRp2nS
<g/>
:	:	kIx,
<g/>
Král	Král	k1gMnSc1
asistencí	asistence	k1gFnPc2
Jiné	jiný	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Český	český	k2eAgInSc1d1
fotbalový	fotbalový	k2eAgInSc1d1
pohár	pohár	k1gInSc1
•	•	k?
Český	český	k2eAgInSc1d1
Superpohár	superpohár	k1gInSc1
•	•	k?
Česko-slovenský	česko-slovenský	k2eAgInSc1d1
Superpohár	superpohár	k1gInSc1
•	•	k?
Fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
Československa	Československo	k1gNnSc2
•	•	k?
Kluby	klub	k1gInPc7
1	#num#	k4
<g/>
.	.	kIx.
české	český	k2eAgFnSc2d1
fotbalové	fotbalový	k2eAgFnSc2d1
ligy	liga	k1gFnSc2
•	•	k?
Pražské	pražský	k2eAgNnSc1d1
derby	derby	k1gNnSc1
Fotbal	fotbal	k1gInSc4
ženy	žena	k1gFnSc2
<g/>
:	:	kIx,
I.	I.	kA
liga	liga	k1gFnSc1
žen	žena	k1gFnPc2
•	•	k?
Česká	český	k2eAgFnSc1d1
ženská	ženský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
reprezentace	reprezentace	k1gFnSc1
</s>
<s>
Moravskoslezská	moravskoslezský	k2eAgFnSc1d1
fotbalová	fotbalový	k2eAgFnSc1d1
liga	liga	k1gFnSc1
2019	#num#	k4
<g/>
/	/	kIx~
<g/>
20	#num#	k4
</s>
<s>
FK	FK	kA
Blansko	Blansko	k1gNnSc1
•	•	k?
FC	FC	kA
Dolní	dolní	k2eAgInSc1d1
Benešov	Benešov	k1gInSc1
•	•	k?
MFK	MFK	kA
Frýdek-Místek	Frýdek-Místek	k1gInSc1
•	•	k?
FC	FC	kA
Hlučín	Hlučín	k1gInSc1
•	•	k?
SK	Sk	kA
Hanácká	hanácký	k2eAgFnSc1d1
Slavia	Slavia	k1gFnSc1
Kroměříž	Kroměříž	k1gFnSc1
•	•	k?
SK	Sk	kA
Sigma	sigma	k1gNnSc2
Olomouc	Olomouc	k1gFnSc1
„	„	k?
<g/>
B	B	kA
<g/>
“	“	k?
•	•	k?
FC	FC	kA
Baník	Baník	k1gInSc1
Ostrava	Ostrava	k1gFnSc1
„	„	k?
<g/>
B	B	kA
<g/>
“	“	k?
•	•	k?
FC	FC	kA
Viktoria	Viktoria	k1gFnSc1
Otrokovice	Otrokovice	k1gFnPc1
•	•	k?
FC	FC	kA
Odra	Odra	k1gFnSc1
Petřkovice	Petřkovice	k1gFnPc1
•	•	k?
FC	FC	kA
Slovan	Slovan	k1gMnSc1
Rosice	Rosice	k1gFnPc4
•	•	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
FC	FC	kA
Slovácko	Slovácko	k1gNnSc1
„	„	k?
<g/>
B	B	kA
<g/>
“	“	k?
•	•	k?
ČSK	ČSK	kA
Uherský	uherský	k2eAgInSc1d1
Brod	Brod	k1gInSc1
•	•	k?
SK	Sk	kA
Uničov	Uničov	k1gInSc1
•	•	k?
FC	FC	kA
Velké	velký	k2eAgNnSc1d1
Meziříčí	Meziříčí	k1gNnSc1
•	•	k?
SFK	SFK	kA
Vrchovina	vrchovina	k1gFnSc1
Nové	Nová	k1gFnSc2
Město	město	k1gNnSc1
na	na	k7c6
Moravě	Morava	k1gFnSc6
•	•	k?
MFK	MFK	kA
Vyškov	Vyškov	k1gInSc1
•	•	k?
FC	FC	kA
Fastav	Fastav	k1gFnSc2
Zlín	Zlín	k1gInSc1
„	„	k?
<g/>
B	B	kA
<g/>
“	“	k?
•	•	k?
1	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
SC	SC	kA
Znojmo	Znojmo	k1gNnSc1
FK	FK	kA
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
xx	xx	k?
<g/>
0	#num#	k4
<g/>
0	#num#	k4
<g/>
15979	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
125998175	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
|	|	kIx~
Ostrava	Ostrava	k1gFnSc1
</s>
