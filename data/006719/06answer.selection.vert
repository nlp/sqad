<s>
Vietnam	Vietnam	k1gInSc1	Vietnam
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
63	[number]	k4	63
administrativních	administrativní	k2eAgFnPc2d1	administrativní
oblastí	oblast	k1gFnPc2	oblast
<g/>
;	;	kIx,	;
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
58	[number]	k4	58
provincií	provincie	k1gFnPc2	provincie
(	(	kIx(	(
<g/>
vietnamsky	vietnamsky	k6eAd1	vietnamsky
tỉ	tỉ	k?	tỉ
<g/>
)	)	kIx)	)
a	a	k8xC	a
5	[number]	k4	5
měst	město	k1gNnPc2	město
"	"	kIx"	"
<g/>
pod	pod	k7c7	pod
ústřední	ústřední	k2eAgFnSc7d1	ústřední
správou	správa	k1gFnSc7	správa
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
viet	viet	k1gInSc1	viet
<g/>
.	.	kIx.	.
thà	thà	k?	thà
phố	phố	k?	phố
trự	trự	k?	trự
thuộ	thuộ	k?	thuộ
trung	trung	k1gInSc1	trung
ư	ư	k?	ư
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
