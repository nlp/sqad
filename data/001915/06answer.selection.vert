<s>
Prvním	první	k4xOgMnSc7	první
vědcem	vědec	k1gMnSc7	vědec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
použil	použít	k5eAaPmAgMnS	použít
slovo	slovo	k1gNnSc4	slovo
"	"	kIx"	"
<g/>
organela	organela	k1gFnSc1	organela
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
zdrobnělina	zdrobnělina	k1gFnSc1	zdrobnělina
ze	z	k7c2	z
slova	slovo	k1gNnSc2	slovo
orgán	orgán	k1gInSc1	orgán
<g/>
,	,	kIx,	,
jakoby	jakoby	k8xS	jakoby
"	"	kIx"	"
<g/>
malý	malý	k2eAgInSc1d1	malý
orgán	orgán	k1gInSc1	orgán
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
buněčných	buněčný	k2eAgFnPc2d1	buněčná
struktur	struktura	k1gFnPc2	struktura
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
německý	německý	k2eAgMnSc1d1	německý
zoolog	zoolog	k1gMnSc1	zoolog
Karl	Karl	k1gMnSc1	Karl
August	August	k1gMnSc1	August
Möbius	Möbius	k1gMnSc1	Möbius
(	(	kIx(	(
<g/>
používá	používat	k5eAaImIp3nS	používat
výraz	výraz	k1gInSc1	výraz
"	"	kIx"	"
<g/>
organula	organout	k5eAaPmAgFnS	organout
<g/>
"	"	kIx"	"
jako	jako	k8xC	jako
množné	množný	k2eAgNnSc1d1	množné
číslo	číslo	k1gNnSc1	číslo
od	od	k7c2	od
lat.	lat.	k?	lat.
"	"	kIx"	"
<g/>
organulum	organulum	k1gInSc1	organulum
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
