<s>
Organely	organela	k1gFnPc1	organela
jsou	být	k5eAaImIp3nP	být
drobné	drobný	k2eAgInPc4d1	drobný
mikroskopické	mikroskopický	k2eAgInPc4d1	mikroskopický
útvary	útvar	k1gInPc4	útvar
uvnitř	uvnitř	k7c2	uvnitř
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
se	s	k7c7	s
specifickou	specifický	k2eAgFnSc7d1	specifická
funkcí	funkce	k1gFnSc7	funkce
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
funkční	funkční	k2eAgFnSc7d1	funkční
obdobou	obdoba	k1gFnSc7	obdoba
orgánů	orgán	k1gMnPc2	orgán
u	u	k7c2	u
živočichů	živočich	k1gMnPc2	živočich
<g/>
.	.	kIx.	.
</s>
<s>
Nejde	jít	k5eNaImIp3nS	jít
však	však	k9	však
o	o	k7c4	o
skutečné	skutečný	k2eAgInPc4d1	skutečný
orgány	orgán	k1gInPc4	orgán
<g/>
,	,	kIx,	,
ty	ten	k3xDgInPc1	ten
jsou	být	k5eAaImIp3nP	být
totiž	totiž	k9	totiž
tvořeny	tvořit	k5eAaImNgInP	tvořit
tkáněmi	tkáň	k1gFnPc7	tkáň
(	(	kIx(	(
<g/>
u	u	k7c2	u
živočichů	živočich	k1gMnPc2	živočich
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
pletivy	pletivo	k1gNnPc7	pletivo
(	(	kIx(	(
<g/>
u	u	k7c2	u
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
složenými	složený	k2eAgInPc7d1	složený
z	z	k7c2	z
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Buňka	buňka	k1gFnSc1	buňka
je	být	k5eAaImIp3nS	být
rozčleněna	rozčlenit	k5eAaPmNgFnS	rozčlenit
(	(	kIx(	(
<g/>
kompartmentována	kompartmentovat	k5eAaPmNgFnS	kompartmentovat
<g/>
)	)	kIx)	)
na	na	k7c6	na
mnoho	mnoho	k4c1	mnoho
takových	takový	k3xDgFnPc2	takový
organel	organela	k1gFnPc2	organela
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
vždy	vždy	k6eAd1	vždy
svou	svůj	k3xOyFgFnSc4	svůj
specifickou	specifický	k2eAgFnSc4d1	specifická
úlohu	úloha	k1gFnSc4	úloha
<g/>
.	.	kIx.	.
</s>
<s>
Organely	organela	k1gFnPc1	organela
jsou	být	k5eAaImIp3nP	být
dle	dle	k7c2	dle
převažující	převažující	k2eAgFnSc2d1	převažující
definice	definice	k1gFnSc2	definice
takové	takový	k3xDgFnSc2	takový
buněčné	buněčný	k2eAgFnSc2d1	buněčná
struktury	struktura	k1gFnSc2	struktura
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
jsou	být	k5eAaImIp3nP	být
obalené	obalený	k2eAgInPc1d1	obalený
vlastní	vlastní	k2eAgFnSc7d1	vlastní
membránou	membrána	k1gFnSc7	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Organely	organela	k1gFnPc1	organela
jsou	být	k5eAaImIp3nP	být
dle	dle	k7c2	dle
takové	takový	k3xDgFnSc2	takový
definice	definice	k1gFnSc2	definice
převážně	převážně	k6eAd1	převážně
výsadou	výsada	k1gFnSc7	výsada
tzv.	tzv.	kA	tzv.
eukaryotických	eukaryotický	k2eAgFnPc2d1	eukaryotická
buněk	buňka	k1gFnPc2	buňka
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
jsou	být	k5eAaImIp3nP	být
buňky	buňka	k1gFnPc1	buňka
tvořící	tvořící	k2eAgNnSc1d1	tvořící
tělo	tělo	k1gNnSc1	tělo
živočichů	živočich	k1gMnPc2	živočich
<g/>
,	,	kIx,	,
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
hub	houba	k1gFnPc2	houba
a	a	k8xC	a
prvoků	prvok	k1gMnPc2	prvok
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
bakterie	bakterie	k1gFnSc2	bakterie
a	a	k8xC	a
archebakterie	archebakterie	k1gFnSc2	archebakterie
obvykle	obvykle	k6eAd1	obvykle
membránové	membránový	k2eAgInPc1d1	membránový
útvary	útvar	k1gInPc1	útvar
uvnitř	uvnitř	k7c2	uvnitř
svých	svůj	k3xOyFgFnPc2	svůj
buněk	buňka	k1gFnPc2	buňka
nemají	mít	k5eNaImIp3nP	mít
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
mesozomy	mesozom	k1gInPc1	mesozom
jsou	být	k5eAaImIp3nP	být
zřejmě	zřejmě	k6eAd1	zřejmě
artefakty	artefakt	k1gInPc1	artefakt
vzniklé	vzniklý	k2eAgInPc1d1	vzniklý
při	při	k7c6	při
přípravě	příprava	k1gFnSc6	příprava
buněk	buňka	k1gFnPc2	buňka
na	na	k7c4	na
elektronovou	elektronový	k2eAgFnSc4d1	elektronová
mikroskopii	mikroskopie	k1gFnSc4	mikroskopie
<g/>
,	,	kIx,	,
určitými	určitý	k2eAgFnPc7d1	určitá
výjimkami	výjimka	k1gFnPc7	výjimka
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
neobvyklé	obvyklý	k2eNgInPc4d1	neobvyklý
karboxyzomy	karboxyzom	k1gInPc4	karboxyzom
<g/>
,	,	kIx,	,
magnetozomy	magnetozom	k1gInPc4	magnetozom
a	a	k8xC	a
jádra	jádro	k1gNnPc4	jádro
připomínající	připomínající	k2eAgInSc4d1	připomínající
útvar	útvar	k1gInSc4	útvar
planktomycet	planktomycet	k5eAaImF	planktomycet
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgMnSc7	první
vědcem	vědec	k1gMnSc7	vědec
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
použil	použít	k5eAaPmAgMnS	použít
slovo	slovo	k1gNnSc4	slovo
"	"	kIx"	"
<g/>
organela	organela	k1gFnSc1	organela
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
zdrobnělina	zdrobnělina	k1gFnSc1	zdrobnělina
ze	z	k7c2	z
slova	slovo	k1gNnSc2	slovo
orgán	orgán	k1gInSc1	orgán
<g/>
,	,	kIx,	,
jakoby	jakoby	k8xS	jakoby
"	"	kIx"	"
<g/>
malý	malý	k2eAgInSc1d1	malý
orgán	orgán	k1gInSc1	orgán
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
buněčných	buněčný	k2eAgFnPc2d1	buněčná
struktur	struktura	k1gFnPc2	struktura
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
německý	německý	k2eAgMnSc1d1	německý
zoolog	zoolog	k1gMnSc1	zoolog
Karl	Karl	k1gMnSc1	Karl
August	August	k1gMnSc1	August
Möbius	Möbius	k1gMnSc1	Möbius
(	(	kIx(	(
<g/>
používá	používat	k5eAaImIp3nS	používat
výraz	výraz	k1gInSc1	výraz
"	"	kIx"	"
<g/>
organula	organout	k5eAaPmAgFnS	organout
<g/>
"	"	kIx"	"
jako	jako	k8xC	jako
množné	množný	k2eAgNnSc1d1	množné
číslo	číslo	k1gNnSc1	číslo
od	od	k7c2	od
lat.	lat.	k?	lat.
"	"	kIx"	"
<g/>
organulum	organulum	k1gInSc1	organulum
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
kontextu	kontext	k1gInSc2	kontext
je	být	k5eAaImIp3nS	být
zřejmé	zřejmý	k2eAgNnSc1d1	zřejmé
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
výraz	výraz	k1gInSc1	výraz
používá	používat	k5eAaImIp3nS	používat
pro	pro	k7c4	pro
rozmnožovací	rozmnožovací	k2eAgInPc4d1	rozmnožovací
útvary	útvar	k1gInPc4	útvar
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
jednobuněčných	jednobuněčný	k2eAgMnPc2d1	jednobuněčný
prvoků	prvok	k1gMnPc2	prvok
(	(	kIx(	(
<g/>
aby	aby	kYmCp3nP	aby
tak	tak	k6eAd1	tak
zdůraznil	zdůraznit	k5eAaPmAgMnS	zdůraznit
rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
orgány	orgán	k1gInPc7	orgán
mnohobuněčných	mnohobuněčný	k2eAgInPc2d1	mnohobuněčný
a	a	k8xC	a
organelami	organela	k1gFnPc7	organela
jednobuněčných	jednobuněčný	k2eAgNnPc2d1	jednobuněčné
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Trvalo	trvat	k5eAaImAgNnS	trvat
několik	několik	k4yIc1	několik
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
termín	termín	k1gInSc1	termín
organulum	organulum	k1gInSc1	organulum
uchytil	uchytit	k5eAaPmAgInS	uchytit
pro	pro	k7c4	pro
subcelulární	subcelulární	k2eAgInSc4d1	subcelulární
(	(	kIx(	(
<g/>
podbuněčné	podbuněčný	k2eAgFnPc1d1	podbuněčný
<g/>
,	,	kIx,	,
menší	malý	k2eAgFnPc1d2	menší
než	než	k8xS	než
buňka	buňka	k1gFnSc1	buňka
<g/>
)	)	kIx)	)
struktury	struktura	k1gFnPc1	struktura
i	i	k9	i
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
mnohobuněčných	mnohobuněčný	k2eAgMnPc2d1	mnohobuněčný
organizmů	organizmus	k1gInPc2	organizmus
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
lidských	lidský	k2eAgFnPc6d1	lidská
buňkách	buňka	k1gFnPc6	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Knihy	kniha	k1gFnPc4	kniha
napsané	napsaný	k2eAgFnPc4d1	napsaná
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
stále	stále	k6eAd1	stále
používají	používat	k5eAaImIp3nP	používat
označení	označení	k1gNnSc1	označení
"	"	kIx"	"
<g/>
buněčné	buněčný	k2eAgInPc1d1	buněčný
orgány	orgán	k1gInPc1	orgán
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
organela	organela	k1gFnSc1	organela
stávala	stávat	k5eAaImAgFnS	stávat
známějším	známý	k2eAgNnSc7d2	známější
a	a	k8xC	a
známějším	známý	k2eAgNnSc7d2	známější
označením	označení	k1gNnSc7	označení
a	a	k8xC	a
kolem	kolo	k1gNnSc7	kolo
roku	rok	k1gInSc2	rok
1920	[number]	k4	1920
už	už	k6eAd1	už
takto	takto	k6eAd1	takto
byly	být	k5eAaImAgFnP	být
označovány	označován	k2eAgFnPc4d1	označována
struktury	struktura	k1gFnPc4	struktura
umožňující	umožňující	k2eAgFnSc1d1	umožňující
pohyb	pohyb	k1gInSc1	pohyb
(	(	kIx(	(
<g/>
např.	např.	kA	např.
bičík	bičík	k1gInSc1	bičík
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
struktury	struktura	k1gFnPc1	struktura
na	na	k7c6	na
buňkách	buňka	k1gFnPc6	buňka
např.	např.	kA	např.
nálevníků	nálevník	k1gMnPc2	nálevník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
použil	použít	k5eAaPmAgMnS	použít
Alfred	Alfred	k1gMnSc1	Alfred
Kühn	Kühn	k1gInSc4	Kühn
označení	označení	k1gNnSc2	označení
"	"	kIx"	"
<g/>
organela	organela	k1gFnSc1	organela
<g/>
"	"	kIx"	"
také	také	k9	také
pro	pro	k7c4	pro
centriolu	centriola	k1gFnSc4	centriola
<g/>
.	.	kIx.	.
</s>
<s>
Současná	současný	k2eAgFnSc1d1	současná
definice	definice	k1gFnSc1	definice
organely	organela	k1gFnSc2	organela
vykrystalizovala	vykrystalizovat	k5eAaPmAgFnS	vykrystalizovat
až	až	k9	až
poměrně	poměrně	k6eAd1	poměrně
pozdě	pozdě	k6eAd1	pozdě
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
byly	být	k5eAaImAgFnP	být
za	za	k7c2	za
organely	organela	k1gFnSc2	organela
považovány	považován	k2eAgFnPc1d1	považována
ty	ten	k3xDgFnPc1	ten
buněčné	buněčný	k2eAgFnPc1d1	buněčná
struktury	struktura	k1gFnPc1	struktura
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
obklopeny	obklopit	k5eAaPmNgFnP	obklopit
membránou	membrána	k1gFnSc7	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
práce	práce	k1gFnPc1	práce
však	však	k9	však
stále	stále	k6eAd1	stále
používají	používat	k5eAaImIp3nP	používat
starší	starý	k2eAgFnSc4d2	starší
definici	definice	k1gFnSc4	definice
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
je	být	k5eAaImIp3nS	být
organela	organela	k1gFnSc1	organela
jakákoliv	jakýkoliv	k3yIgFnSc1	jakýkoliv
funkční	funkční	k2eAgFnSc1d1	funkční
jednotka	jednotka	k1gFnSc1	jednotka
uvnitř	uvnitř	k7c2	uvnitř
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Organely	organela	k1gFnPc4	organela
s	s	k7c7	s
DNA	DNA	kA	DNA
neboli	neboli	k8xC	neboli
semiautonomní	semiautonomní	k2eAgFnSc2d1	semiautonomní
organely	organela	k1gFnSc2	organela
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc2	smysl
jsou	být	k5eAaImIp3nP	být
obklopeny	obklopit	k5eAaPmNgFnP	obklopit
dvěma	dva	k4xCgFnPc7	dva
nebo	nebo	k8xC	nebo
více	hodně	k6eAd2	hodně
membránami	membrána	k1gFnPc7	membrána
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
obecně	obecně	k6eAd1	obecně
uznává	uznávat	k5eAaImIp3nS	uznávat
jejich	jejich	k3xOp3gInSc1	jejich
vznik	vznik	k1gInSc1	vznik
procesem	proces	k1gInSc7	proces
endosymbiózy	endosymbióza	k1gFnSc2	endosymbióza
a	a	k8xC	a
existují	existovat	k5eAaImIp3nP	existovat
pozorování	pozorování	k1gNnSc4	pozorování
nově	nově	k6eAd1	nově
vznikajících	vznikající	k2eAgFnPc2d1	vznikající
semiautonomních	semiautonomní	k2eAgFnPc2d1	semiautonomní
organel	organela	k1gFnPc2	organela
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
nový	nový	k2eAgInSc4d1	nový
vznik	vznik	k1gInSc4	vznik
chloroplastů	chloroplast	k1gInPc2	chloroplast
u	u	k7c2	u
prvoka	prvok	k1gMnSc2	prvok
Paulinella	Paulinell	k1gMnSc2	Paulinell
chromatophora	chromatophor	k1gMnSc2	chromatophor
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
(	(	kIx(	(
<g/>
karyon	karyon	k1gInSc1	karyon
<g/>
,	,	kIx,	,
nucleus	nucleus	k1gInSc1	nucleus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
kryto	krýt	k5eAaImNgNnS	krýt
dvouvrstvou	dvouvrstvý	k2eAgFnSc7d1	dvouvrstvá
jadernou	jaderný	k2eAgFnSc7d1	jaderná
membránou	membrána	k1gFnSc7	membrána
<g/>
,	,	kIx,	,
s	s	k7c7	s
póry	pór	k1gInPc7	pór
tvořenými	tvořený	k2eAgFnPc7d1	tvořená
speciálními	speciální	k2eAgFnPc7d1	speciální
bílkovinami	bílkovina	k1gFnPc7	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
usnadnit	usnadnit	k5eAaPmF	usnadnit
transport	transport	k1gInSc1	transport
makromolekul	makromolekula	k1gFnPc2	makromolekula
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
RNA	RNA	kA	RNA
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jádře	jádro	k1gNnSc6	jádro
je	být	k5eAaImIp3nS	být
uložen	uložen	k2eAgInSc1d1	uložen
chromatin	chromatin	k1gInSc1	chromatin
(	(	kIx(	(
<g/>
DNA	DNA	kA	DNA
<g/>
)	)	kIx)	)
-	-	kIx~	-
v	v	k7c6	v
době	doba	k1gFnSc6	doba
dělení	dělení	k1gNnSc2	dělení
buňky	buňka	k1gFnSc2	buňka
se	se	k3xPyFc4	se
organizuje	organizovat	k5eAaBmIp3nS	organizovat
do	do	k7c2	do
formy	forma	k1gFnSc2	forma
chromozomů	chromozom	k1gInPc2	chromozom
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
jadérko	jadérko	k1gNnSc1	jadérko
<g/>
,	,	kIx,	,
ribozomy	ribozom	k1gInPc1	ribozom
a	a	k8xC	a
karyolymfa	karyolymfa	k1gFnSc1	karyolymfa
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
funkcí	funkce	k1gFnSc7	funkce
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
uchování	uchování	k1gNnSc4	uchování
genetické	genetický	k2eAgFnSc2d1	genetická
informace	informace	k1gFnSc2	informace
a	a	k8xC	a
na	na	k7c6	na
jejím	její	k3xOp3gInSc6	její
základě	základ	k1gInSc6	základ
řízení	řízení	k1gNnSc2	řízení
funkcí	funkce	k1gFnPc2	funkce
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Jádro	jádro	k1gNnSc1	jádro
lze	lze	k6eAd1	lze
najít	najít	k5eAaPmF	najít
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
eukaryot	eukaryota	k1gFnPc2	eukaryota
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
v	v	k7c6	v
něm	on	k3xPp3gMnSc6	on
najít	najít	k5eAaPmF	najít
jadérko	jadérko	k1gNnSc1	jadérko
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
funkcí	funkce	k1gFnSc7	funkce
je	být	k5eAaImIp3nS	být
tvorba	tvorba	k1gFnSc1	tvorba
rRNA	rRNA	k?	rRNA
(	(	kIx(	(
<g/>
ribozomální	ribozomální	k2eAgFnSc1d1	ribozomální
RNA	RNA	kA	RNA
<g/>
)	)	kIx)	)
a	a	k8xC	a
účast	účast	k1gFnSc4	účast
na	na	k7c4	na
regulaci	regulace	k1gFnSc4	regulace
buněčného	buněčný	k2eAgNnSc2d1	buněčné
dělení	dělení	k1gNnSc2	dělení
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Semiautonomní	Semiautonomní	k2eAgFnSc1d1	Semiautonomní
organela	organela	k1gFnSc1	organela
<g/>
.	.	kIx.	.
</s>
<s>
Semiautonomní	Semiautonomní	k2eAgFnPc1d1	Semiautonomní
organely	organela	k1gFnPc1	organela
v	v	k7c6	v
užším	úzký	k2eAgInSc6d2	užší
smyslu	smysl	k1gInSc6	smysl
jsou	být	k5eAaImIp3nP	být
buněčné	buněčný	k2eAgInPc1d1	buněčný
kompartmenty	kompartment	k1gInPc1	kompartment
s	s	k7c7	s
vlastní	vlastní	k2eAgFnSc7d1	vlastní
genetickou	genetický	k2eAgFnSc7d1	genetická
informací	informace	k1gFnSc7	informace
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
DNA	dno	k1gNnSc2	dno
prokaryotního	prokaryotní	k2eAgNnSc2d1	prokaryotní
uspořádání	uspořádání	k1gNnSc2	uspořádání
(	(	kIx(	(
<g/>
DNA	DNA	kA	DNA
v	v	k7c6	v
kruhovém	kruhový	k2eAgNnSc6d1	kruhové
uspořádání	uspořádání	k1gNnSc6	uspořádání
a	a	k8xC	a
v	v	k7c6	v
plazmidech	plazmid	k1gInPc6	plazmid
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
endosymbiotické	endosymbiotický	k2eAgFnSc2d1	endosymbiotická
teorie	teorie	k1gFnSc2	teorie
se	se	k3xPyFc4	se
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
má	mít	k5eAaImIp3nS	mít
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc1	tento
organely	organela	k1gFnPc1	organela
jsou	být	k5eAaImIp3nP	být
přímými	přímý	k2eAgInPc7d1	přímý
potomky	potomek	k1gMnPc4	potomek
bakterií	bakterie	k1gFnPc2	bakterie
(	(	kIx(	(
<g/>
mitochondrie	mitochondrie	k1gFnSc1	mitochondrie
<g/>
)	)	kIx)	)
a	a	k8xC	a
sinic	sinice	k1gFnPc2	sinice
(	(	kIx(	(
<g/>
chloroplasty	chloroplast	k1gInPc1	chloroplast
a	a	k8xC	a
ostatní	ostatní	k2eAgInPc1d1	ostatní
plastidy	plastid	k1gInPc1	plastid
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
prošly	projít	k5eAaPmAgFnP	projít
intenzivní	intenzivní	k2eAgInPc4d1	intenzivní
symbiózou	symbióza	k1gFnSc7	symbióza
s	s	k7c7	s
hostitelskou	hostitelský	k2eAgFnSc7d1	hostitelská
buňkou	buňka	k1gFnSc7	buňka
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
Archea	Arche	k2eAgFnSc1d1	Arche
<g/>
)	)	kIx)	)
a	a	k8xC	a
staly	stát	k5eAaPmAgFnP	stát
se	se	k3xPyFc4	se
na	na	k7c6	na
buňce	buňka	k1gFnSc6	buňka
plně	plně	k6eAd1	plně
závislou	závislý	k2eAgFnSc7d1	závislá
strukturou	struktura	k1gFnSc7	struktura
<g/>
.	.	kIx.	.
</s>
<s>
Semiautonomními	Semiautonomní	k2eAgMnPc7d1	Semiautonomní
jsou	být	k5eAaImIp3nP	být
nazývány	nazývat	k5eAaImNgFnP	nazývat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jejich	jejich	k3xOp3gFnSc1	jejich
existence	existence	k1gFnSc1	existence
již	již	k6eAd1	již
není	být	k5eNaImIp3nS	být
možná	možná	k9	možná
mimo	mimo	k7c4	mimo
prostředí	prostředí	k1gNnSc4	prostředí
buňky	buňka	k1gFnSc2	buňka
<g/>
,	,	kIx,	,
s	s	k7c7	s
jejímiž	jejíž	k3xOyRp3gFnPc7	jejíž
signálními	signální	k2eAgFnPc7d1	signální
drahami	draha	k1gFnPc7	draha
interagují	interagovat	k5eAaBmIp3nP	interagovat
a	a	k8xC	a
jimiž	jenž	k3xRgFnPc7	jenž
jsou	být	k5eAaImIp3nP	být
usměrňovány	usměrňován	k2eAgFnPc1d1	usměrňována
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zachovaly	zachovat	k5eAaPmAgFnP	zachovat
si	se	k3xPyFc3	se
ještě	ještě	k9	ještě
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
původní	původní	k2eAgFnSc2d1	původní
genetické	genetický	k2eAgFnPc4d1	genetická
informace	informace	k1gFnPc4	informace
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnPc4	jejíž
realizace	realizace	k1gFnSc1	realizace
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
existenci	existence	k1gFnSc4	existence
a	a	k8xC	a
funkci	funkce	k1gFnSc4	funkce
této	tento	k3xDgFnSc2	tento
organely	organela	k1gFnSc2	organela
stále	stále	k6eAd1	stále
nezbytná	zbytný	k2eNgFnSc1d1	zbytný
<g/>
.	.	kIx.	.
</s>
<s>
Semiautonomní	Semiautonomní	k2eAgFnPc1d1	Semiautonomní
organely	organela	k1gFnPc1	organela
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
Eukaryot	Eukaryota	k1gFnPc2	Eukaryota
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
získání	získání	k1gNnSc2	získání
představuje	představovat	k5eAaImIp3nS	představovat
základní	základní	k2eAgInSc4d1	základní
krok	krok	k1gInSc4	krok
v	v	k7c6	v
evoluci	evoluce	k1gFnSc6	evoluce
od	od	k7c2	od
Prokaryot	Prokaryota	k1gFnPc2	Prokaryota
k	k	k7c3	k
Eukaryotám	Eukaryota	k1gFnPc3	Eukaryota
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
semiautonomní	semiautonomní	k2eAgFnSc4d1	semiautonomní
organelu	organela	k1gFnSc4	organela
je	být	k5eAaImIp3nS	být
také	také	k9	také
typické	typický	k2eAgNnSc1d1	typické
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
obklopena	obklopit	k5eAaPmNgFnS	obklopit
dvojitou	dvojitý	k2eAgFnSc7d1	dvojitá
membránou	membrána	k1gFnSc7	membrána
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
membrána	membrána	k1gFnSc1	membrána
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
tzv	tzv	kA	tzv
lipidovou	lipidový	k2eAgFnSc7d1	lipidová
dvouvrstvou	dvouvrstva	k1gFnSc7	dvouvrstva
<g/>
,	,	kIx,	,
jde	jít	k5eAaImIp3nS	jít
tedy	tedy	k9	tedy
o	o	k7c4	o
dvojici	dvojice	k1gFnSc4	dvojice
takovýchto	takovýto	k3xDgNnPc2	takovýto
již	již	k6eAd1	již
"	"	kIx"	"
<g/>
dvojitých	dvojitý	k2eAgFnPc2d1	dvojitá
<g/>
"	"	kIx"	"
membrán	membrána	k1gFnPc2	membrána
(	(	kIx(	(
<g/>
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
membrána	membrána	k1gFnSc1	membrána
z	z	k7c2	z
dvojice	dvojice	k1gFnSc2	dvojice
představuje	představovat	k5eAaImIp3nS	představovat
původní	původní	k2eAgFnSc4d1	původní
cytoplasmatickou	cytoplasmatický	k2eAgFnSc4d1	cytoplasmatická
membránu	membrána	k1gFnSc4	membrána
pohlcené	pohlcený	k2eAgFnSc2d1	pohlcená
bakterie	bakterie	k1gFnSc2	bakterie
a	a	k8xC	a
vnější	vnější	k2eAgFnSc1d1	vnější
membrána	membrána	k1gFnSc1	membrána
představuje	představovat	k5eAaImIp3nS	představovat
původní	původní	k2eAgFnSc4d1	původní
cytoplasmatickou	cytoplasmatický	k2eAgFnSc4d1	cytoplasmatická
membránu	membrána	k1gFnSc4	membrána
hostitelské	hostitelský	k2eAgFnSc2d1	hostitelská
buňky	buňka	k1gFnSc2	buňka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obklopila	obklopit	k5eAaPmAgFnS	obklopit
pronikající	pronikající	k2eAgFnSc4d1	pronikající
bakterii	bakterie	k1gFnSc4	bakterie
při	při	k7c6	při
endocytóze	endocytóza	k1gFnSc6	endocytóza
-	-	kIx~	-
průniku	průnik	k1gInSc6	průnik
bakterie	bakterie	k1gFnSc2	bakterie
dovnitř	dovnitř	k6eAd1	dovnitř
host	host	k1gMnSc1	host
<g/>
.	.	kIx.	.
buňky	buňka	k1gFnPc1	buňka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
membrány	membrána	k1gFnPc1	membrána
mají	mít	k5eAaImIp3nP	mít
částečně	částečně	k6eAd1	částečně
odlišné	odlišný	k2eAgFnPc1d1	odlišná
vlastnosti	vlastnost	k1gFnPc1	vlastnost
<g/>
,	,	kIx,	,
ať	ať	k9	ať
v	v	k7c4	v
již	již	k6eAd1	již
zastoupení	zastoupení	k1gNnSc4	zastoupení
membránových	membránový	k2eAgInPc2d1	membránový
proteinů	protein	k1gInPc2	protein
<g/>
,	,	kIx,	,
zastoupení	zastoupení	k1gNnSc3	zastoupení
odlišných	odlišný	k2eAgInPc2d1	odlišný
lipidů	lipid	k1gInPc2	lipid
a	a	k8xC	a
často	často	k6eAd1	často
také	také	k9	také
odlišnou	odlišný	k2eAgFnSc7d1	odlišná
hodnotou	hodnota	k1gFnSc7	hodnota
gradientu	gradient	k1gInSc2	gradient
redoxního	redoxní	k2eAgInSc2d1	redoxní
potenciálu	potenciál	k1gInSc2	potenciál
<g/>
.	.	kIx.	.
</s>
<s>
Mitochondrie	mitochondrie	k1gFnSc1	mitochondrie
je	být	k5eAaImIp3nS	být
zdrojem	zdroj	k1gInSc7	zdroj
velké	velký	k2eAgFnSc2d1	velká
většiny	většina	k1gFnSc2	většina
buněčného	buněčný	k2eAgNnSc2d1	buněčné
ATP	atp	kA	atp
a	a	k8xC	a
představuje	představovat	k5eAaImIp3nS	představovat
tak	tak	k6eAd1	tak
základní	základní	k2eAgInSc1d1	základní
zdroj	zdroj	k1gInSc1	zdroj
energie	energie	k1gFnSc2	energie
eukaryotické	eukaryotický	k2eAgFnSc2d1	eukaryotická
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
mitochondriích	mitochondrie	k1gFnPc6	mitochondrie
probíhá	probíhat	k5eAaImIp3nS	probíhat
oxidativní	oxidativní	k2eAgFnSc1d1	oxidativní
fosforylace	fosforylace	k1gFnSc1	fosforylace
-	-	kIx~	-
velmi	velmi	k6eAd1	velmi
efektivní	efektivní	k2eAgInSc1d1	efektivní
způsob	způsob	k1gInSc1	způsob
získávání	získávání	k1gNnSc2	získávání
energie	energie	k1gFnSc2	energie
štěpením	štěpení	k1gNnSc7	štěpení
cukrů	cukr	k1gInPc2	cukr
až	až	k9	až
na	na	k7c4	na
oxid	oxid	k1gInSc4	oxid
uhličitý	uhličitý	k2eAgInSc4d1	uhličitý
a	a	k8xC	a
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
membrána	membrána	k1gFnSc1	membrána
je	být	k5eAaImIp3nS	být
místem	místo	k1gNnSc7	místo
vlastní	vlastní	k2eAgFnSc2d1	vlastní
oxidace	oxidace	k1gFnSc2	oxidace
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
všelijak	všelijak	k6eAd1	všelijak
zprohýbaná	zprohýbaný	k2eAgFnSc1d1	zprohýbaná
v	v	k7c4	v
kristy	krist	k1gMnPc4	krist
a	a	k8xC	a
tvoří	tvořit	k5eAaImIp3nP	tvořit
tak	tak	k6eAd1	tak
oddělené	oddělený	k2eAgInPc4d1	oddělený
prostory	prostor	k1gInPc4	prostor
(	(	kIx(	(
<g/>
kompartmenty	kompartment	k1gInPc4	kompartment
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
různé	různý	k2eAgFnPc4d1	různá
chemické	chemický	k2eAgFnPc4d1	chemická
reakce	reakce	k1gFnPc4	reakce
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
tím	ten	k3xDgNnSc7	ten
zvětšuje	zvětšovat	k5eAaImIp3nS	zvětšovat
svou	svůj	k3xOyFgFnSc4	svůj
reakční	reakční	k2eAgFnSc4d1	reakční
plochu	plocha	k1gFnSc4	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Vnitřní	vnitřní	k2eAgInSc1d1	vnitřní
prostor	prostor	k1gInSc1	prostor
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
matrix	matrix	k1gInSc1	matrix
<g/>
.	.	kIx.	.
</s>
<s>
Mitochondrie	mitochondrie	k1gFnPc1	mitochondrie
jsou	být	k5eAaImIp3nP	být
přítomné	přítomný	k2eAgFnPc1d1	přítomná
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
většiny	většina	k1gFnSc2	většina
eukaryotních	eukaryotní	k2eAgInPc2d1	eukaryotní
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Stejnou	stejný	k2eAgFnSc4d1	stejná
funkci	funkce	k1gFnSc4	funkce
jako	jako	k8xS	jako
mitochondrie	mitochondrie	k1gFnSc1	mitochondrie
plní	plnit	k5eAaImIp3nS	plnit
u	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
eukaryot	eukaryot	k1gInSc4	eukaryot
hydrogenozomy	hydrogenozom	k1gInPc7	hydrogenozom
<g/>
,	,	kIx,	,
mající	mající	k2eAgFnPc4d1	mající
i	i	k8xC	i
obdobné	obdobný	k2eAgFnPc4d1	obdobná
strukturní	strukturní	k2eAgFnPc4d1	strukturní
součásti	součást	k1gFnPc4	součást
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
vyvinuly	vyvinout	k5eAaPmAgFnP	vyvinout
z	z	k7c2	z
mitochondrií	mitochondrie	k1gFnPc2	mitochondrie
<g/>
.	.	kIx.	.
</s>
<s>
Plastidy	plastid	k1gInPc1	plastid
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
mitochondrie	mitochondrie	k1gFnPc1	mitochondrie
mají	mít	k5eAaImIp3nP	mít
dvojvrstvou	dvojvrstvý	k2eAgFnSc4d1	dvojvrstvá
membránu	membrána	k1gFnSc4	membrána
a	a	k8xC	a
vlastní	vlastní	k2eAgNnPc4d1	vlastní
DNA	dno	k1gNnPc4	dno
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
z	z	k7c2	z
endosymbiotických	endosymbiotický	k2eAgFnPc2d1	endosymbiotická
sinic	sinice	k1gFnPc2	sinice
<g/>
.	.	kIx.	.
</s>
<s>
Sinice	sinice	k1gFnPc1	sinice
jsou	být	k5eAaImIp3nP	být
eubakterie	eubakterie	k1gFnPc1	eubakterie
schopné	schopný	k2eAgFnPc1d1	schopná
využít	využít	k5eAaPmF	využít
energie	energie	k1gFnSc1	energie
světla	světlo	k1gNnSc2	světlo
v	v	k7c6	v
procesu	proces	k1gInSc6	proces
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
<g/>
,	,	kIx,	,
symbiózou	symbióza	k1gFnSc7	symbióza
a	a	k8xC	a
pozdějším	pozdní	k2eAgNnSc7d2	pozdější
přisvojením	přisvojení	k1gNnSc7	přisvojení
si	se	k3xPyFc3	se
sinic	sinice	k1gFnPc2	sinice
tak	tak	k6eAd1	tak
eukaryotní	eukaryotní	k2eAgFnPc1d1	eukaryotní
buňky	buňka	k1gFnPc1	buňka
mohly	moct	k5eAaImAgFnP	moct
získat	získat	k5eAaPmF	získat
schopnost	schopnost	k1gFnSc4	schopnost
vlastní	vlastní	k2eAgFnSc2d1	vlastní
fotosyntézy	fotosyntéza	k1gFnSc2	fotosyntéza
a	a	k8xC	a
mohly	moct	k5eAaImAgFnP	moct
se	se	k3xPyFc4	se
orientovat	orientovat	k5eAaBmF	orientovat
na	na	k7c4	na
zásadně	zásadně	k6eAd1	zásadně
odlišnou	odlišný	k2eAgFnSc4d1	odlišná
strategii	strategie	k1gFnSc4	strategie
získávání	získávání	k1gNnSc2	získávání
energie	energie	k1gFnSc2	energie
a	a	k8xC	a
následně	následně	k6eAd1	následně
i	i	k9	i
odlišnou	odlišný	k2eAgFnSc4d1	odlišná
životní	životní	k2eAgFnSc4d1	životní
strategii	strategie	k1gFnSc4	strategie
<g/>
.	.	kIx.	.
</s>
<s>
Plastidy	plastid	k1gInPc1	plastid
se	se	k3xPyFc4	se
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
u	u	k7c2	u
organismů	organismus	k1gInPc2	organismus
náležejících	náležející	k2eAgInPc2d1	náležející
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
Archaeplastida	Archaeplastida	k1gFnSc1	Archaeplastida
(	(	kIx(	(
<g/>
tedy	tedy	k8xC	tedy
rostliny	rostlina	k1gFnPc1	rostlina
v	v	k7c6	v
širším	široký	k2eAgInSc6d2	širší
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
tři	tři	k4xCgFnPc1	tři
linie	linie	k1gFnPc1	linie
fotosyntetizujících	fotosyntetizující	k2eAgFnPc2d1	fotosyntetizující
eukaryot	eukaryota	k1gFnPc2	eukaryota
<g/>
:	:	kIx,	:
glaukofyty	glaukofyt	k1gInPc1	glaukofyt
<g/>
,	,	kIx,	,
červené	červený	k2eAgFnPc1d1	červená
řasy	řasa	k1gFnPc1	řasa
<g/>
,	,	kIx,	,
zelené	zelený	k2eAgFnPc1d1	zelená
řasy	řasa	k1gFnPc1	řasa
a	a	k8xC	a
ze	z	k7c2	z
zelených	zelený	k2eAgFnPc2d1	zelená
řas	řasa	k1gFnPc2	řasa
vyvinuvší	vyvinuvší	k2eAgFnSc2d1	vyvinuvší
se	se	k3xPyFc4	se
rostliny	rostlina	k1gFnSc2	rostlina
(	(	kIx(	(
<g/>
Plantae	Plantae	k1gNnSc1	Plantae
sensu	sens	k1gInSc2	sens
stricto	strict	k2eAgNnSc1d1	stricto
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
bioinformatického	bioinformatický	k2eAgInSc2d1	bioinformatický
rozboru	rozbor	k1gInSc2	rozbor
plastidové	plastidový	k2eAgFnSc2d1	plastidová
DNA	DNA	kA	DNA
se	se	k3xPyFc4	se
usuzuje	usuzovat	k5eAaImIp3nS	usuzovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
plastidy	plastid	k1gInPc1	plastid
obsažené	obsažený	k2eAgInPc1d1	obsažený
v	v	k7c6	v
jakémkoliv	jakýkoliv	k3yIgInSc6	jakýkoliv
z	z	k7c2	z
eukaryotních	eukaryotní	k2eAgInPc2d1	eukaryotní
organismů	organismus	k1gInPc2	organismus
(	(	kIx(	(
<g/>
mimo	mimo	k6eAd1	mimo
rostlin	rostlina	k1gFnPc2	rostlina
jsou	být	k5eAaImIp3nP	být
plastidy	plastid	k1gInPc1	plastid
přítomné	přítomný	k2eAgInPc1d1	přítomný
také	také	k9	také
v	v	k7c6	v
buňkách	buňka	k1gFnPc6	buňka
všech	všecek	k3xTgInPc2	všecek
typů	typ	k1gInPc2	typ
řas	řasa	k1gFnPc2	řasa
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
jednoho	jeden	k4xCgMnSc4	jeden
původu	původa	k1gMnSc4	původa
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k7c4	mimo
výše	výše	k1gFnPc4	výše
zmíněných	zmíněný	k2eAgFnPc2d1	zmíněná
linií	linie	k1gFnPc2	linie
však	však	k9	však
existují	existovat	k5eAaImIp3nP	existovat
ještě	ještě	k9	ještě
některé	některý	k3yIgFnPc1	některý
řasy	řasa	k1gFnPc1	řasa
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
plastidy	plastid	k1gInPc1	plastid
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
sekundární	sekundární	k2eAgFnPc4d1	sekundární
endosymbiózou	endosymbióza	k1gFnSc7	endosymbióza
(	(	kIx(	(
<g/>
Rhizaria	Rhizarium	k1gNnSc2	Rhizarium
<g/>
,	,	kIx,	,
Excavata	Excavata	k1gFnSc1	Excavata
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hostitelská	hostitelský	k2eAgFnSc1d1	hostitelská
buňka	buňka	k1gFnSc1	buňka
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
pohltila	pohltit	k5eAaPmAgFnS	pohltit
jednobuněčnou	jednobuněčný	k2eAgFnSc4d1	jednobuněčná
řasu	řasa	k1gFnSc4	řasa
již	již	k6eAd1	již
i	i	k9	i
s	s	k7c7	s
plastidem	plastid	k1gInSc7	plastid
<g/>
,	,	kIx,	,
tyto	tento	k3xDgInPc1	tento
plastidy	plastid	k1gInPc1	plastid
pak	pak	k6eAd1	pak
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
tři	tři	k4xCgMnPc1	tři
i	i	k9	i
čtyři	čtyři	k4xCgFnPc4	čtyři
membrány	membrána	k1gFnPc4	membrána
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
zelených	zelený	k2eAgFnPc2d1	zelená
řas	řasa	k1gFnPc2	řasa
se	se	k3xPyFc4	se
v	v	k7c6	v
buňce	buňka	k1gFnSc6	buňka
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
jen	jen	k9	jen
jediný	jediný	k2eAgInSc1d1	jediný
plastid	plastid	k1gInSc1	plastid
<g/>
,	,	kIx,	,
u	u	k7c2	u
vyšších	vysoký	k2eAgFnPc2d2	vyšší
rostlin	rostlina	k1gFnPc2	rostlina
počet	počet	k1gInSc1	počet
plastidů	plastid	k1gInPc2	plastid
na	na	k7c4	na
buňku	buňka	k1gFnSc4	buňka
není	být	k5eNaImIp3nS	být
vázaný	vázaný	k2eAgInSc1d1	vázaný
<g/>
,	,	kIx,	,
dělí	dělit	k5eAaImIp3nS	dělit
se	se	k3xPyFc4	se
nezávisle	závisle	k6eNd1	závisle
na	na	k7c6	na
buněčném	buněčný	k2eAgNnSc6d1	buněčné
dělení	dělení	k1gNnSc6	dělení
a	a	k8xC	a
bývá	bývat	k5eAaImIp3nS	bývat
jich	on	k3xPp3gFnPc2	on
v	v	k7c6	v
jediné	jediný	k2eAgFnSc6d1	jediná
buňce	buňka	k1gFnSc6	buňka
mnoho	mnoho	k4c1	mnoho
<g/>
.	.	kIx.	.
</s>
<s>
Plastidy	plastid	k1gInPc1	plastid
prodělávají	prodělávat	k5eAaImIp3nP	prodělávat
po	po	k7c6	po
rozdělení	rozdělení	k1gNnSc6	rozdělení
různý	různý	k2eAgInSc4d1	různý
vývoj	vývoj	k1gInSc4	vývoj
a	a	k8xC	a
můžeme	moct	k5eAaImIp1nP	moct
je	on	k3xPp3gFnPc4	on
proto	proto	k8xC	proto
dále	daleko	k6eAd2	daleko
dělit	dělit	k5eAaImF	dělit
na	na	k7c4	na
<g/>
:	:	kIx,	:
Chloroplasty	chloroplast	k1gInPc4	chloroplast
-	-	kIx~	-
Jsou	být	k5eAaImIp3nP	být
plastidy	plastid	k1gInPc4	plastid
s	s	k7c7	s
plně	plně	k6eAd1	plně
vyvinutým	vyvinutý	k2eAgInSc7d1	vyvinutý
fotosyntetickým	fotosyntetický	k2eAgInSc7d1	fotosyntetický
aparátem	aparát	k1gInSc7	aparát
<g/>
,	,	kIx,	,
představují	představovat	k5eAaImIp3nP	představovat
jedinou	jediný	k2eAgFnSc4d1	jediná
strukturu	struktura	k1gFnSc4	struktura
eukaryotní	eukaryotní	k2eAgFnSc2d1	eukaryotní
buňky	buňka	k1gFnSc2	buňka
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
může	moct	k5eAaImIp3nS	moct
fotosyntéza	fotosyntéza	k1gFnSc1	fotosyntéza
probíhat	probíhat	k5eAaImF	probíhat
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zelené	zelený	k2eAgInPc1d1	zelený
<g/>
,	,	kIx,	,
dávají	dávat	k5eAaImIp3nP	dávat
barvu	barva	k1gFnSc4	barva
celé	celý	k2eAgFnSc3d1	celá
rostlině	rostlina	k1gFnSc3	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Proplastidy	Proplastida	k1gFnPc1	Proplastida
-	-	kIx~	-
Dosud	dosud	k6eAd1	dosud
nedozrálé	dozrálý	k2eNgInPc1d1	nedozrálý
plastidy	plastid	k1gInPc1	plastid
<g/>
,	,	kIx,	,
Chromoplasty	chromoplast	k1gInPc1	chromoplast
-	-	kIx~	-
Obsahují	obsahovat	k5eAaImIp3nP	obsahovat
nechlorofylová	chlorofylový	k2eNgNnPc1d1	chlorofylový
barviva	barvivo	k1gNnPc1	barvivo
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
různé	různý	k2eAgFnPc4d1	různá
barvy	barva	k1gFnPc4	barva
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
ochranou	ochrana	k1gFnSc7	ochrana
funkci	funkce	k1gFnSc4	funkce
a	a	k8xC	a
také	také	k9	také
zabarvují	zabarvovat	k5eAaImIp3nP	zabarvovat
rostlinu	rostlina	k1gFnSc4	rostlina
(	(	kIx(	(
<g/>
např.	např.	kA	např.
podzimní	podzimní	k2eAgNnSc1d1	podzimní
listí	listí	k1gNnSc1	listí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Leukoplasty	leukoplast	k1gInPc1	leukoplast
-	-	kIx~	-
Mají	mít	k5eAaImIp3nP	mít
zásobní	zásobní	k2eAgFnSc4d1	zásobní
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
především	především	k9	především
škrob	škrob	k1gInSc4	škrob
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Endomembránový	Endomembránový	k2eAgInSc4d1	Endomembránový
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
membránové	membránový	k2eAgInPc1d1	membránový
útvary	útvar	k1gInPc1	útvar
se	se	k3xPyFc4	se
označují	označovat	k5eAaImIp3nP	označovat
další	další	k2eAgInPc1d1	další
buněčné	buněčný	k2eAgInPc1d1	buněčný
kompartmenty	kompartment	k1gInPc1	kompartment
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
ohraničené	ohraničený	k2eAgInPc1d1	ohraničený
a	a	k8xC	a
od	od	k7c2	od
svého	svůj	k3xOyFgNnSc2	svůj
okolí	okolí	k1gNnSc2	okolí
oddělené	oddělený	k2eAgNnSc1d1	oddělené
buněčnou	buněčný	k2eAgFnSc7d1	buněčná
membránou	membrána	k1gFnSc7	membrána
<g/>
,	,	kIx,	,
a	a	k8xC	a
sama	sám	k3xTgFnSc1	sám
vnější	vnější	k2eAgFnSc1d1	vnější
cytoplazmatická	cytoplazmatický	k2eAgFnSc1d1	cytoplazmatická
membrána	membrána	k1gFnSc1	membrána
buňky	buňka	k1gFnSc2	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
semiautonomních	semiautonomní	k2eAgFnPc2d1	semiautonomní
organel	organela	k1gFnPc2	organela
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
vždy	vždy	k6eAd1	vždy
obklopeny	obklopit	k5eAaPmNgFnP	obklopit
dvojicí	dvojice	k1gFnSc7	dvojice
membrán	membrána	k1gFnPc2	membrána
<g/>
,	,	kIx,	,
tyto	tento	k3xDgInPc1	tento
kompartmenty	kompartment	k1gInPc1	kompartment
mají	mít	k5eAaImIp3nP	mít
vždy	vždy	k6eAd1	vždy
jen	jen	k9	jen
jednu	jeden	k4xCgFnSc4	jeden
membránovou	membránový	k2eAgFnSc4d1	membránová
vrstvu	vrstva	k1gFnSc4	vrstva
<g/>
.	.	kIx.	.
</s>
<s>
Komplikovanější	komplikovaný	k2eAgFnPc1d2	komplikovanější
vnitřní	vnitřní	k2eAgFnPc1d1	vnitřní
membránové	membránový	k2eAgFnPc1d1	membránová
struktury	struktura	k1gFnPc1	struktura
jsou	být	k5eAaImIp3nP	být
charakteristické	charakteristický	k2eAgFnPc1d1	charakteristická
pro	pro	k7c4	pro
evolučně	evolučně	k6eAd1	evolučně
rozvinutější	rozvinutý	k2eAgFnPc4d2	rozvinutější
eukaryotické	eukaryotický	k2eAgFnPc4d1	eukaryotická
buňky	buňka	k1gFnPc4	buňka
<g/>
,	,	kIx,	,
prokaryota	prokaryota	k1gFnSc1	prokaryota
má	mít	k5eAaImIp3nS	mít
svou	svůj	k3xOyFgFnSc4	svůj
vnitřní	vnitřní	k2eAgFnSc4d1	vnitřní
strukturu	struktura	k1gFnSc4	struktura
prostou	prostý	k2eAgFnSc4d1	prostá
<g/>
,	,	kIx,	,
membránami	membrána	k1gFnPc7	membrána
dále	daleko	k6eAd2	daleko
již	již	k6eAd1	již
nečleněnou	členěný	k2eNgFnSc4d1	nečleněná
<g/>
.	.	kIx.	.
</s>
<s>
Cytoplazmatická	Cytoplazmatický	k2eAgFnSc1d1	Cytoplazmatická
membrána	membrána	k1gFnSc1	membrána
(	(	kIx(	(
<g/>
též	též	k9	též
plazmalema	plazmalema	k1gNnSc1	plazmalema
nebo	nebo	k8xC	nebo
plasmalema	plasmalema	k1gNnSc1	plasmalema
<g/>
;	;	kIx,	;
plazmatická	plazmatický	k2eAgFnSc1d1	plazmatická
membrána	membrána	k1gFnSc1	membrána
<g/>
,	,	kIx,	,
zkr.	zkr.	kA	zkr.
PM	PM	kA	PM
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
semipermeabilní	semipermeabilní	k2eAgFnSc1d1	semipermeabilní
(	(	kIx(	(
<g/>
polopropustná	polopropustný	k2eAgFnSc1d1	polopropustná
<g/>
)	)	kIx)	)
membrána	membrána	k1gFnSc1	membrána
uzavírající	uzavírající	k2eAgFnSc1d1	uzavírající
obsah	obsah	k1gInSc4	obsah
buňky	buňka	k1gFnPc4	buňka
<g/>
.	.	kIx.	.
</s>
<s>
Zprostředkovává	zprostředkovávat	k5eAaImIp3nS	zprostředkovávat
kontakt	kontakt	k1gInSc4	kontakt
buňky	buňka	k1gFnSc2	buňka
s	s	k7c7	s
okolím	okolí	k1gNnSc7	okolí
(	(	kIx(	(
<g/>
ať	ať	k9	ať
už	už	k6eAd1	už
aktivním	aktivní	k2eAgInSc7d1	aktivní
transportem	transport	k1gInSc7	transport
či	či	k8xC	či
osmózou	osmóza	k1gFnSc7	osmóza
<g/>
)	)	kIx)	)
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
ji	on	k3xPp3gFnSc4	on
před	před	k7c7	před
okolím	okolí	k1gNnSc7	okolí
chrání	chránit	k5eAaImIp3nP	chránit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
lipidovou	lipidový	k2eAgFnSc7d1	lipidová
dvouvrstvou	dvouvrstva	k1gFnSc7	dvouvrstva
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yQgFnSc2	který
jsou	být	k5eAaImIp3nP	být
zabudovány	zabudovat	k5eAaPmNgFnP	zabudovat
různé	různý	k2eAgFnPc1d1	různá
bílkoviny	bílkovina	k1gFnPc1	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
nezbytnou	zbytný	k2eNgFnSc7d1	zbytný
součástí	součást	k1gFnSc7	součást
buněk	buňka	k1gFnPc2	buňka
všech	všecek	k3xTgInPc2	všecek
typů	typ	k1gInPc2	typ
<g/>
.	.	kIx.	.
</s>
<s>
Endoplazmatické	Endoplazmatický	k2eAgNnSc1d1	Endoplazmatické
retikulum	retikulum	k1gNnSc1	retikulum
(	(	kIx(	(
<g/>
ER	ER	kA	ER
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
složitá	složitý	k2eAgFnSc1d1	složitá
membránová	membránový	k2eAgFnSc1d1	membránová
struktura	struktura	k1gFnSc1	struktura
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
jádra	jádro	k1gNnSc2	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Lze	lze	k6eAd1	lze
rozlišit	rozlišit	k5eAaPmF	rozlišit
dva	dva	k4xCgInPc4	dva
typy	typ	k1gInPc4	typ
-	-	kIx~	-
hladké	hladký	k2eAgNnSc4d1	hladké
endoplazmatické	endoplazmatický	k2eAgNnSc4d1	endoplazmatické
retikulum	retikulum	k1gNnSc4	retikulum
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
přisedlých	přisedlý	k2eAgInPc2d1	přisedlý
ribozómů	ribozóm	k1gInPc2	ribozóm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
funkcí	funkce	k1gFnSc7	funkce
je	být	k5eAaImIp3nS	být
syntéza	syntéza	k1gFnSc1	syntéza
tuků	tuk	k1gInPc2	tuk
a	a	k8xC	a
glykogenu	glykogen	k1gInSc2	glykogen
a	a	k8xC	a
hrubé	hrubý	k2eAgNnSc1d1	hrubé
endoplazmatické	endoplazmatický	k2eAgNnSc1d1	endoplazmatické
retikulum	retikulum	k1gNnSc1	retikulum
(	(	kIx(	(
<g/>
s	s	k7c7	s
přisedlými	přisedlý	k2eAgInPc7d1	přisedlý
ribozómy	ribozóm	k1gInPc7	ribozóm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gFnSc7	jehož
funkcí	funkce	k1gFnSc7	funkce
je	být	k5eAaImIp3nS	být
syntéza	syntéza	k1gFnSc1	syntéza
bílkovin	bílkovina	k1gFnPc2	bílkovina
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
eukaryot	eukaryota	k1gFnPc2	eukaryota
<g/>
.	.	kIx.	.
</s>
<s>
Golgiho	Golgize	k6eAd1	Golgize
aparát	aparát	k1gInSc1	aparát
(	(	kIx(	(
<g/>
GA	GA	kA	GA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
složitá	složitý	k2eAgFnSc1d1	složitá
membránová	membránový	k2eAgFnSc1d1	membránová
struktura	struktura	k1gFnSc1	struktura
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
funkcí	funkce	k1gFnSc7	funkce
je	být	k5eAaImIp3nS	být
shromažďovat	shromažďovat	k5eAaImF	shromažďovat
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
zpracovávat	zpracovávat	k5eAaImF	zpracovávat
produkty	produkt	k1gInPc4	produkt
endoplazmatického	endoplazmatický	k2eAgNnSc2d1	endoplazmatické
retikula	retikulum	k1gNnSc2	retikulum
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterým	který	k3yRgInSc7	který
sousedí	sousedit	k5eAaImIp3nP	sousedit
a	a	k8xC	a
probíhá	probíhat	k5eAaImIp3nS	probíhat
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
váčkový	váčkový	k2eAgInSc4d1	váčkový
-	-	kIx~	-
vezikulární	vezikulární	k2eAgInSc4d1	vezikulární
transport	transport	k1gInSc4	transport
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
eukaryot	eukaryota	k1gFnPc2	eukaryota
<g/>
.	.	kIx.	.
</s>
<s>
Lysosomy	Lysosom	k1gInPc1	Lysosom
jsou	být	k5eAaImIp3nP	být
typické	typický	k2eAgInPc1d1	typický
pro	pro	k7c4	pro
živočišné	živočišný	k2eAgFnPc4d1	živočišná
buňky	buňka	k1gFnPc4	buňka
<g/>
,	,	kIx,	,
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
proteolytické	proteolytický	k2eAgInPc1d1	proteolytický
a	a	k8xC	a
hydrolytické	hydrolytický	k2eAgInPc1d1	hydrolytický
enzymy	enzym	k1gInPc1	enzym
<g/>
,	,	kIx,	,
slouží	sloužit	k5eAaImIp3nS	sloužit
k	k	k7c3	k
degradaci	degradace	k1gFnSc3	degradace
látek	látka	k1gFnPc2	látka
buňkou	buňka	k1gFnSc7	buňka
fagocytovaných	fagocytovaný	k2eAgFnPc2d1	fagocytovaný
nebo	nebo	k8xC	nebo
do	do	k7c2	do
buňky	buňka	k1gFnSc2	buňka
proniklých	proniklý	k2eAgFnPc2d1	proniklá
pinocytózou	pinocytóza	k1gFnSc7	pinocytóza
<g/>
.	.	kIx.	.
</s>
<s>
Vakuolu	vakuola	k1gFnSc4	vakuola
nalezneme	naleznout	k5eAaPmIp1nP	naleznout
především	především	k9	především
u	u	k7c2	u
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
plní	plnit	k5eAaImIp3nS	plnit
zde	zde	k6eAd1	zde
funkci	funkce	k1gFnSc4	funkce
zásobního	zásobní	k2eAgInSc2d1	zásobní
orgánu	orgán	k1gInSc2	orgán
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
u	u	k7c2	u
dospělých	dospělý	k2eAgFnPc2d1	dospělá
buněk	buňka	k1gFnPc2	buňka
vyplňuje	vyplňovat	k5eAaImIp3nS	vyplňovat
velkou	velký	k2eAgFnSc4d1	velká
většinu	většina	k1gFnSc4	většina
buněčného	buněčný	k2eAgInSc2d1	buněčný
obsahu	obsah	k1gInSc2	obsah
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
některá	některý	k3yIgNnPc1	některý
protista	protista	k1gMnSc1	protista
mají	mít	k5eAaImIp3nP	mít
vyvinutou	vyvinutý	k2eAgFnSc4d1	vyvinutá
vakuolu	vakuola	k1gFnSc4	vakuola
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
může	moct	k5eAaImIp3nS	moct
plnit	plnit	k5eAaImF	plnit
mnoho	mnoho	k4c4	mnoho
funkcí	funkce	k1gFnPc2	funkce
(	(	kIx(	(
<g/>
potravní	potravní	k2eAgInPc4d1	potravní
<g/>
,	,	kIx,	,
pulsující	pulsující	k2eAgInSc4d1	pulsující
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Thylakoidy	Thylakoid	k1gInPc1	Thylakoid
jsou	být	k5eAaImIp3nP	být
jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
membránové	membránový	k2eAgInPc1d1	membránový
váčky	váček	k1gInPc1	váček
v	v	k7c6	v
prokaryotických	prokaryotický	k2eAgFnPc6d1	prokaryotická
buňkách	buňka	k1gFnPc6	buňka
a	a	k8xC	a
semiautonomních	semiautonomní	k2eAgFnPc6d1	semiautonomní
organelách	organela	k1gFnPc6	organela
<g/>
.	.	kIx.	.
</s>
<s>
Mikrotělíska	Mikrotělíska	k1gFnSc1	Mikrotělíska
(	(	kIx(	(
<g/>
peroxizómy	peroxizóm	k1gInPc1	peroxizóm
a	a	k8xC	a
glyoxizómy	glyoxizóm	k1gInPc1	glyoxizóm
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
přeměnu	přeměna	k1gFnSc4	přeměna
aminokyselin	aminokyselina	k1gFnPc2	aminokyselina
a	a	k8xC	a
tuků	tuk	k1gInPc2	tuk
na	na	k7c4	na
cukry	cukr	k1gInPc4	cukr
<g/>
.	.	kIx.	.
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
organela	organela	k1gFnSc1	organela
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
organela	organela	k1gFnSc1	organela
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Organelle	Organelle	k1gFnSc2	Organelle
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
Alberts	Alberts	k1gInSc1	Alberts
<g/>
,	,	kIx,	,
Bruce	Bruce	k1gFnSc1	Bruce
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Molecular	Molecular	k1gInSc1	Molecular
Biology	biolog	k1gMnPc4	biolog
of	of	k?	of
the	the	k?	the
Cell	cello	k1gNnPc2	cello
<g/>
,	,	kIx,	,
4	[number]	k4	4
<g/>
th	th	k?	th
ed	ed	k?	ed
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Garland	Garland	k1gInSc1	Garland
Science	Science	k1gFnSc1	Science
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
8153	[number]	k4	8153
<g/>
-	-	kIx~	-
<g/>
3218	[number]	k4	3218
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Kerfeld	Kerfeld	k1gInSc1	Kerfeld
<g/>
,	,	kIx,	,
Cheryl	Cheryl	k1gInSc1	Cheryl
A	a	k8xC	a
et	et	k?	et
al	ala	k1gFnPc2	ala
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Protein	protein	k1gInSc1	protein
Structures	Structures	k1gInSc1	Structures
Forming	Forming	k1gInSc1	Forming
the	the	k?	the
Shell	Shell	k1gInSc1	Shell
of	of	k?	of
Primitive	primitiv	k1gMnSc5	primitiv
Bacterial	Bacterial	k1gMnSc1	Bacterial
Organelles	Organelles	k1gInSc1	Organelles
<g/>
,	,	kIx,	,
Science	Science	k1gFnSc1	Science
309	[number]	k4	309
<g/>
:	:	kIx,	:
<g/>
936	[number]	k4	936
<g/>
-	-	kIx~	-
<g/>
938	[number]	k4	938
(	(	kIx(	(
<g/>
5	[number]	k4	5
August	August	k1gMnSc1	August
2005	[number]	k4	2005
<g/>
)	)	kIx)	)
Pubmed	Pubmed	k1gInSc1	Pubmed
</s>
