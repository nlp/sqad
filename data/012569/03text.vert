<p>
<s>
Ragtime	ragtime	k1gInSc1	ragtime
(	(	kIx(	(
<g/>
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
anglického	anglický	k2eAgInSc2d1	anglický
to	ten	k3xDgNnSc4	ten
rag	rag	k?	rag
–	–	k?	–
roztrhat	roztrhat	k5eAaPmF	roztrhat
<g/>
,	,	kIx,	,
rozervat	rozervat	k5eAaPmF	rozervat
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
osobitý	osobitý	k2eAgMnSc1d1	osobitý
a	a	k8xC	a
specifický	specifický	k2eAgInSc1d1	specifický
způsob	způsob	k1gInSc1	způsob
klavírní	klavírní	k2eAgFnSc2d1	klavírní
hry	hra	k1gFnSc2	hra
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
poslední	poslední	k2eAgInSc4d1	poslední
mezistupeň	mezistupeň	k1gInSc4	mezistupeň
mezi	mezi	k7c7	mezi
tradiční	tradiční	k2eAgFnSc7d1	tradiční
černošskou	černošský	k2eAgFnSc7d1	černošská
lidovou	lidový	k2eAgFnSc7d1	lidová
hudbou	hudba	k1gFnSc7	hudba
a	a	k8xC	a
jazzem	jazz	k1gInSc7	jazz
<g/>
.	.	kIx.	.
</s>
<s>
Začal	začít	k5eAaPmAgMnS	začít
se	se	k3xPyFc4	se
formovat	formovat	k5eAaImF	formovat
koncem	koncem	k7c2	koncem
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
také	také	k9	také
nazýval	nazývat	k5eAaImAgInS	nazývat
dobovým	dobový	k2eAgInSc7d1	dobový
pojmem	pojem	k1gInSc7	pojem
cake-walk	cakealka	k1gFnPc2	cake-walka
nebo	nebo	k8xC	nebo
coon-song	coononga	k1gFnPc2	coon-songa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Základní	základní	k2eAgInPc4d1	základní
hudební	hudební	k2eAgInPc4d1	hudební
prvky	prvek	k1gInPc4	prvek
převzal	převzít	k5eAaPmAgInS	převzít
ragtime	ragtime	k1gInSc1	ragtime
z	z	k7c2	z
tzv.	tzv.	kA	tzv.
minstrel	minstrel	k1gMnSc1	minstrel
shows	shows	k6eAd1	shows
<g/>
.	.	kIx.	.
</s>
<s>
To	to	k9	to
byla	být	k5eAaImAgFnS	být
představení	představení	k1gNnSc4	představení
bělošských	bělošský	k2eAgInPc2d1	bělošský
kabaretů	kabaret	k1gInPc2	kabaret
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
parodovala	parodovat	k5eAaImAgFnS	parodovat
hudbu	hudba	k1gFnSc4	hudba
a	a	k8xC	a
život	život	k1gInSc1	život
amerických	americký	k2eAgMnPc2d1	americký
černochů	černoch	k1gMnPc2	černoch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
představeních	představení	k1gNnPc6	představení
hráli	hrát	k5eAaImAgMnP	hrát
běloši	běloch	k1gMnPc1	běloch
s	s	k7c7	s
načerněnými	načerněný	k2eAgInPc7d1	načerněný
obličeji	obličej	k1gInPc7	obličej
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zrušení	zrušení	k1gNnSc6	zrušení
otroctví	otroctví	k1gNnSc2	otroctví
začali	začít	k5eAaPmAgMnP	začít
hudbu	hudba	k1gFnSc4	hudba
těchto	tento	k3xDgInPc2	tento
kabaretů	kabaret	k1gInPc2	kabaret
oficiálně	oficiálně	k6eAd1	oficiálně
hrát	hrát	k5eAaImF	hrát
i	i	k9	i
černoši	černoch	k1gMnPc1	černoch
<g/>
.	.	kIx.	.
</s>
<s>
Původním	původní	k2eAgInSc7d1	původní
nástrojem	nástroj	k1gInSc7	nástroj
tohoto	tento	k3xDgInSc2	tento
stylu	styl	k1gInSc2	styl
hry	hra	k1gFnSc2	hra
bylo	být	k5eAaImAgNnS	být
bendžo	bendžo	k1gNnSc1	bendžo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
ale	ale	k9	ale
movitější	movitý	k2eAgMnPc1d2	movitější
a	a	k8xC	a
vzdělanější	vzdělaný	k2eAgMnPc1d2	vzdělanější
černošští	černošský	k2eAgMnPc1d1	černošský
hudebníci	hudebník	k1gMnPc1	hudebník
začali	začít	k5eAaPmAgMnP	začít
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
sami	sám	k3xTgMnPc1	sám
opouštět	opouštět	k5eAaImF	opouštět
a	a	k8xC	a
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
používali	používat	k5eAaImAgMnP	používat
klasické	klasický	k2eAgNnSc4d1	klasické
fortepiano	fortepiano	k1gNnSc4	fortepiano
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ragtime	ragtime	k1gInSc1	ragtime
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
rytmus	rytmus	k1gInSc1	rytmus
v	v	k7c6	v
polkovém	polkový	k2eAgNnSc6d1	polkový
či	či	k8xC	či
pochodovém	pochodový	k2eAgNnSc6d1	pochodové
tempu	tempo	k1gNnSc6	tempo
s	s	k7c7	s
oktávovým	oktávový	k2eAgInSc7d1	oktávový
basem	bas	k1gInSc7	bas
na	na	k7c6	na
těžkých	těžký	k2eAgFnPc6d1	těžká
dobách	doba	k1gFnPc6	doba
s	s	k7c7	s
bohatě	bohatě	k6eAd1	bohatě
synkopovanou	synkopovaný	k2eAgFnSc7d1	synkopovaná
melodií	melodie	k1gFnSc7	melodie
<g/>
.	.	kIx.	.
</s>
<s>
Melodie	melodie	k1gFnSc1	melodie
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
jakoby	jakoby	k8xS	jakoby
rytmicky	rytmicky	k6eAd1	rytmicky
roztrhána	roztrhán	k2eAgFnSc1d1	roztrhána
či	či	k8xC	či
rozcupována	rozcupován	k2eAgFnSc1d1	rozcupován
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc4	ten
vše	všechen	k3xTgNnSc4	všechen
v	v	k7c6	v
jediném	jediný	k2eAgInSc6d1	jediný
přísném	přísný	k2eAgInSc6d1	přísný
základním	základní	k2eAgInSc6d1	základní
dvoučvrťovém	dvoučvrťový	k2eAgInSc6d1	dvoučvrťový
metru	metr	k1gInSc6	metr
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
toho	ten	k3xDgNnSc2	ten
také	také	k6eAd1	také
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
název	název	k1gInSc1	název
tohoto	tento	k3xDgInSc2	tento
hudebního	hudební	k2eAgInSc2d1	hudební
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rytmu	rytmus	k1gInSc6	rytmus
ještě	ještě	k6eAd1	ještě
není	být	k5eNaImIp3nS	být
patrná	patrný	k2eAgFnSc1d1	patrná
žádná	žádný	k3yNgFnSc1	žádný
nepravidelnost	nepravidelnost	k1gFnSc1	nepravidelnost
ani	ani	k8xC	ani
náznaky	náznak	k1gInPc1	náznak
mnohem	mnohem	k6eAd1	mnohem
pozdějšího	pozdní	k2eAgInSc2d2	pozdější
swingu	swing	k1gInSc2	swing
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mnoho	mnoho	k4c1	mnoho
černošských	černošský	k2eAgMnPc2d1	černošský
pianistů	pianista	k1gMnPc2	pianista
považovalo	považovat	k5eAaImAgNnS	považovat
ragtime	ragtime	k1gInSc4	ragtime
za	za	k7c4	za
vážnou	vážný	k2eAgFnSc4d1	vážná
hudbu	hudba	k1gFnSc4	hudba
a	a	k8xC	a
doufali	doufat	k5eAaImAgMnP	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ho	on	k3xPp3gNnSc4	on
tak	tak	k6eAd1	tak
bude	být	k5eAaImBp3nS	být
veřejnost	veřejnost	k1gFnSc1	veřejnost
přijímat	přijímat	k5eAaImF	přijímat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
byla	být	k5eAaImAgFnS	být
taková	takový	k3xDgFnSc1	takový
představa	představa	k1gFnSc1	představa
poněkud	poněkud	k6eAd1	poněkud
nereálná	reálný	k2eNgFnSc1d1	nereálná
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
si	se	k3xPyFc3	se
veřejnost	veřejnost	k1gFnSc4	veřejnost
na	na	k7c4	na
ragtime	ragtime	k1gInSc4	ragtime
opět	opět	k6eAd1	opět
vzpomněla	vzpomnít	k5eAaPmAgFnS	vzpomnít
–	–	k?	–
jeho	jeho	k3xOp3gFnPc4	jeho
desky	deska	k1gFnPc4	deska
na	na	k7c4	na
nějaký	nějaký	k3yIgInSc4	nějaký
čas	čas	k1gInSc4	čas
obsadily	obsadit	k5eAaPmAgFnP	obsadit
špičková	špičkový	k2eAgNnPc4d1	špičkové
místa	místo	k1gNnPc4	místo
na	na	k7c6	na
žebříčcích	žebříček	k1gInPc6	žebříček
nejprodávanější	prodávaný	k2eAgFnSc2d3	nejprodávanější
vážné	vážný	k2eAgFnSc2d1	vážná
hudby	hudba	k1gFnSc2	hudba
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
všechno	všechen	k3xTgNnSc4	všechen
mohl	moct	k5eAaImAgInS	moct
film	film	k1gInSc1	film
Podraz	podraz	k1gInSc1	podraz
(	(	kIx(	(
<g/>
The	The	k1gMnSc1	The
Sting	Sting	k1gMnSc1	Sting
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
zazněl	zaznět	k5eAaImAgInS	zaznět
asi	asi	k9	asi
nejslavnější	slavný	k2eAgInSc1d3	nejslavnější
ragtime	ragtime	k1gInSc1	ragtime
Scotta	Scotto	k1gNnSc2	Scotto
Joplina	Joplina	k1gFnSc1	Joplina
–	–	k?	–
česky	česky	k6eAd1	česky
s	s	k7c7	s
názvem	název	k1gInSc7	název
Komika	komik	k1gMnSc2	komik
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
známější	známý	k2eAgInSc1d2	známější
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gInSc1	jeho
původní	původní	k2eAgInSc1d1	původní
název	název	k1gInSc1	název
The	The	k1gMnSc1	The
Entertainer	Entertainer	k1gMnSc1	Entertainer
(	(	kIx(	(
<g/>
1902	[number]	k4	1902
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Klasikové	klasik	k1gMnPc1	klasik
stylu	styl	k1gInSc2	styl
==	==	k?	==
</s>
</p>
<p>
<s>
Scott	Scott	k2eAgInSc1d1	Scott
Joplin	Joplin	k1gInSc1	Joplin
(	(	kIx(	(
<g/>
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1868	[number]	k4	1868
až	až	k9	až
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1917	[number]	k4	1917
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
James	James	k1gMnSc1	James
Scott	Scott	k1gMnSc1	Scott
(	(	kIx(	(
<g/>
1886	[number]	k4	1886
<g/>
–	–	k?	–
<g/>
1938	[number]	k4	1938
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Louis	Louis	k1gMnSc1	Louis
Chauvin	Chauvina	k1gFnPc2	Chauvina
(	(	kIx(	(
<g/>
1881	[number]	k4	1881
<g/>
–	–	k?	–
<g/>
1908	[number]	k4	1908
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Tom	Tom	k1gMnSc1	Tom
Turpin	Turpin	k1gMnSc1	Turpin
(	(	kIx(	(
<g/>
1873	[number]	k4	1873
<g/>
–	–	k?	–
<g/>
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Joseph	Joseph	k1gMnSc1	Joseph
F.	F.	kA	F.
Lamb	Lamb	k1gMnSc1	Lamb
(	(	kIx(	(
<g/>
1887	[number]	k4	1887
<g/>
–	–	k?	–
<g/>
1960	[number]	k4	1960
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Jelly	Jella	k1gFnSc2	Jella
Roll	Rolla	k1gFnPc2	Rolla
Morton	Morton	k1gInSc1	Morton
(	(	kIx(	(
<g/>
1885	[number]	k4	1885
<g/>
–	–	k?	–
<g/>
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
–	–	k?	–
což	což	k3yRnSc4	což
byl	být	k5eAaImAgInS	být
také	také	k9	také
pozdější	pozdní	k2eAgMnSc1d2	pozdější
jazzman	jazzman	k1gMnSc1	jazzman
</s>
</p>
<p>
<s>
Winifred	Winifred	k1gInSc1	Winifred
Atwellová	Atwellová	k1gFnSc1	Atwellová
(	(	kIx(	(
<g/>
1910	[number]	k4	1910
<g/>
–	–	k?	–
<g/>
1983	[number]	k4	1983
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Domácí	domácí	k2eAgMnPc1d1	domácí
interpreti	interpret	k1gMnPc1	interpret
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
našich	náš	k3xOp1gMnPc2	náš
interpretů	interpret	k1gMnPc2	interpret
se	se	k3xPyFc4	se
tomuto	tento	k3xDgInSc3	tento
stylu	styl	k1gInSc3	styl
hry	hra	k1gFnSc2	hra
velice	velice	k6eAd1	velice
dobře	dobře	k6eAd1	dobře
věnoval	věnovat	k5eAaImAgMnS	věnovat
Vladimír	Vladimír	k1gMnSc1	Vladimír
Klusák	Klusák	k1gMnSc1	Klusák
<g/>
,	,	kIx,	,
umí	umět	k5eAaImIp3nS	umět
jej	on	k3xPp3gMnSc4	on
velice	velice	k6eAd1	velice
dobře	dobře	k6eAd1	dobře
hrát	hrát	k5eAaImF	hrát
třeba	třeba	k6eAd1	třeba
paní	paní	k1gFnSc1	paní
Eva	Eva	k1gFnSc1	Eva
Mládková	Mládková	k1gFnSc1	Mládková
(	(	kIx(	(
<g/>
manželka	manželka	k1gFnSc1	manželka
Ivana	Ivan	k1gMnSc2	Ivan
Mládka	Mládek	k1gMnSc2	Mládek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Klasickou	klasický	k2eAgFnSc7d1	klasická
skladbou	skladba	k1gFnSc7	skladba
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
stylu	styl	k1gInSc6	styl
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
klavírní	klavírní	k2eAgFnSc1d1	klavírní
skladba	skladba	k1gFnSc1	skladba
Jaroslava	Jaroslav	k1gMnSc2	Jaroslav
Ježka	Ježek	k1gMnSc2	Ježek
Bugatti	Bugatť	k1gFnSc2	Bugatť
Step	step	k1gFnSc1	step
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Ragtime	ragtime	k1gInSc1	ragtime
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
–	–	k?	–
filmová	filmový	k2eAgFnSc1d1	filmová
adaptace	adaptace	k1gFnSc1	adaptace
režiséra	režisér	k1gMnSc2	režisér
Miloše	Miloš	k1gMnSc2	Miloš
Formana	Forman	k1gMnSc2	Forman
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použité	použitý	k2eAgInPc1d1	použitý
zdroje	zdroj	k1gInPc1	zdroj
==	==	k?	==
</s>
</p>
<p>
<s>
Lubomír	Lubomír	k1gMnSc1	Lubomír
Dorůžka	Dorůžka	k1gFnSc1	Dorůžka
<g/>
,	,	kIx,	,
Panoráma	panoráma	k1gFnSc1	panoráma
populární	populární	k2eAgFnSc2d1	populární
hudby	hudba	k1gFnSc2	hudba
1918	[number]	k4	1918
<g/>
/	/	kIx~	/
<g/>
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1981	[number]	k4	1981
<g/>
,	,	kIx,	,
katalogové	katalogový	k2eAgNnSc1d1	Katalogové
číslo	číslo	k1gNnSc1	číslo
23-068-81	[number]	k4	23-068-81
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
/	/	kIx~	/
<g/>
21	[number]	k4	21
</s>
</p>
<p>
<s>
Československá	československý	k2eAgFnSc1d1	Československá
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
<g/>
:	:	kIx,	:
Malá	malý	k2eAgFnSc1d1	malá
československá	československý	k2eAgFnSc1d1	Československá
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
1986	[number]	k4	1986
</s>
</p>
<p>
<s>
Liz	liz	k1gInSc1	liz
and	and	k?	and
John	John	k1gMnSc1	John
Soars	Soars	k1gInSc1	Soars
<g/>
:	:	kIx,	:
New	New	k1gFnSc1	New
Headway	Headwaa	k1gFnSc2	Headwaa
English	Englisha	k1gFnPc2	Englisha
Course	Course	k1gFnSc2	Course
–	–	k?	–
Intermediate	Intermediat	k1gMnSc5	Intermediat
<g/>
,	,	kIx,	,
Student	student	k1gMnSc1	student
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Book	Book	k1gInSc1	Book
<g/>
,	,	kIx,	,
Oxford	Oxford	k1gInSc1	Oxford
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
1996	[number]	k4	1996
</s>
</p>
<p>
<s>
CD	CD	kA	CD
deska	deska	k1gFnSc1	deska
Classic	Classic	k1gMnSc1	Classic
Ragtime	ragtime	k1gInSc1	ragtime
Piano	piano	k6eAd1	piano
–	–	k?	–
Eva	Eva	k1gFnSc1	Eva
Mládková	Mládková	k1gFnSc1	Mládková
–	–	k?	–
vydal	vydat	k5eAaPmAgMnS	vydat
Platon	Platon	k1gMnSc1	Platon
</s>
</p>
<p>
<s>
CD	CD	kA	CD
deska	deska	k1gFnSc1	deska
Ragtime	ragtime	k1gInSc1	ragtime
guitar	guitar	k1gInSc1	guitar
(	(	kIx(	(
<g/>
hraje	hrát	k5eAaImIp3nS	hrát
Stanislav	Stanislav	k1gMnSc1	Stanislav
Barek	Barka	k1gFnPc2	Barka
a	a	k8xC	a
Adib	Adiba	k1gFnPc2	Adiba
Ghali	Ghal	k1gMnPc1	Ghal
<g/>
)	)	kIx)	)
–	–	k?	–
vydaly	vydat	k5eAaPmAgFnP	vydat
Levné	levný	k2eAgFnPc1d1	levná
knihy	kniha	k1gFnPc1	kniha
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
text	text	k1gInSc1	text
na	na	k7c6	na
obalu	obal	k1gInSc6	obal
napsal	napsat	k5eAaPmAgMnS	napsat
Petr	Petr	k1gMnSc1	Petr
Dorůžka	Dorůžka	k1gFnSc1	Dorůžka
</s>
</p>
