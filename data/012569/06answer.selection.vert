<s>
Ragtime	ragtime	k1gInSc1	ragtime
je	být	k5eAaImIp3nS	být
vlastně	vlastně	k9	vlastně
pravidelný	pravidelný	k2eAgInSc1d1	pravidelný
rytmus	rytmus	k1gInSc1	rytmus
v	v	k7c6	v
polkovém	polkový	k2eAgNnSc6d1	polkový
či	či	k8xC	či
pochodovém	pochodový	k2eAgNnSc6d1	pochodové
tempu	tempo	k1gNnSc6	tempo
s	s	k7c7	s
oktávovým	oktávový	k2eAgInSc7d1	oktávový
basem	bas	k1gInSc7	bas
na	na	k7c6	na
těžkých	těžký	k2eAgFnPc6d1	těžká
dobách	doba	k1gFnPc6	doba
s	s	k7c7	s
bohatě	bohatě	k6eAd1	bohatě
synkopovanou	synkopovaný	k2eAgFnSc7d1	synkopovaná
melodií	melodie	k1gFnSc7	melodie
<g/>
.	.	kIx.	.
</s>
