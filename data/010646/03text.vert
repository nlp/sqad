<p>
<s>
Nevelké	velký	k2eNgInPc1d1	nevelký
Jihlavské	jihlavský	k2eAgInPc1d1	jihlavský
vrchy	vrch	k1gInPc1	vrch
jsou	být	k5eAaImIp3nP	být
nejvyšším	vysoký	k2eAgNnSc7d3	nejvyšší
pohořím	pohoří	k1gNnSc7	pohoří
Českomoravské	českomoravský	k2eAgFnSc2d1	Českomoravská
vysočiny	vysočina	k1gFnSc2	vysočina
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
geomorfologický	geomorfologický	k2eAgInSc4d1	geomorfologický
podcelek	podcelek	k1gInSc4	podcelek
<g/>
,	,	kIx,	,
náležící	náležící	k2eAgInSc4d1	náležící
do	do	k7c2	do
Javořické	Javořický	k2eAgFnSc2d1	Javořická
vrchoviny	vrchovina	k1gFnSc2	vrchovina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
podsoustavy	podsoustava	k1gFnSc2	podsoustava
Českomoravské	českomoravský	k2eAgFnSc2d1	Českomoravská
vrchoviny	vrchovina	k1gFnSc2	vrchovina
<g/>
.	.	kIx.	.
</s>
<s>
Vrchy	vrch	k1gInPc1	vrch
se	se	k3xPyFc4	se
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
10	[number]	k4	10
km	km	kA	km
severozápadně	severozápadně	k6eAd1	severozápadně
od	od	k7c2	od
Telče	Telč	k1gFnSc2	Telč
<g/>
,	,	kIx,	,
či	či	k8xC	či
10	[number]	k4	10
km	km	kA	km
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Třeště	třeštit	k5eAaImSgMnS	třeštit
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc1d3	veliký
část	část	k1gFnSc1	část
Jihlavských	jihlavský	k2eAgInPc2d1	jihlavský
vrchů	vrch	k1gInPc2	vrch
je	být	k5eAaImIp3nS	být
souvisle	souvisle	k6eAd1	souvisle
zalesněna	zalesněn	k2eAgFnSc1d1	zalesněna
(	(	kIx(	(
<g/>
8	[number]	k4	8
x	x	k?	x
8	[number]	k4	8
km	km	kA	km
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
vrchů	vrch	k1gInPc2	vrch
i	i	k8xC	i
celé	celý	k2eAgFnSc2d1	celá
Českomoravské	českomoravský	k2eAgFnSc2d1	Českomoravská
vysočiny	vysočina	k1gFnSc2	vysočina
je	být	k5eAaImIp3nS	být
Javořice	Javořice	k1gFnSc1	Javořice
(	(	kIx(	(
<g/>
837	[number]	k4	837
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
)	)	kIx)	)
na	na	k7c6	na
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
vrcholu	vrchol	k1gInSc6	vrchol
je	být	k5eAaImIp3nS	být
zdaleka	zdaleka	k6eAd1	zdaleka
viditelný	viditelný	k2eAgInSc1d1	viditelný
166	[number]	k4	166
m	m	kA	m
vysoký	vysoký	k2eAgInSc1d1	vysoký
televizní	televizní	k2eAgInSc1d1	televizní
vysílač	vysílač	k1gInSc1	vysílač
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
hrad	hrad	k1gInSc1	hrad
Roštejn	Roštejna	k1gFnPc2	Roštejna
či	či	k8xC	či
zřícena	zřícen	k2eAgFnSc1d1	zřícena
hradu	hrad	k1gInSc2	hrad
Šternberku	Šternberk	k1gInSc2	Šternberk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
části	část	k1gFnSc6	část
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
mnoho	mnoho	k4c1	mnoho
starých	starý	k2eAgInPc2d1	starý
lomů	lom	k1gInPc2	lom
na	na	k7c4	na
žulu	žula	k1gFnSc4	žula
<g/>
,	,	kIx,	,
v	v	k7c6	v
Mrákotíně	Mrákotín	k1gInSc6	Mrákotín
se	se	k3xPyFc4	se
žula	žula	k1gFnSc1	žula
doposud	doposud	k6eAd1	doposud
těží	těžet	k5eAaImIp3nS	těžet
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
obce	obec	k1gFnSc2	obec
Řásná	řásnat	k5eAaImIp3nS	řásnat
<g/>
,	,	kIx,	,
poblíž	poblíž	k6eAd1	poblíž
Velkého	velký	k2eAgInSc2d1	velký
pařezitého	pařezitý	k2eAgInSc2d1	pařezitý
rybníka	rybník	k1gInSc2	rybník
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
autokemp	autokemp	k1gInSc1	autokemp
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jihlavské	jihlavský	k2eAgInPc4d1	jihlavský
vrchy	vrch	k1gInPc4	vrch
se	se	k3xPyFc4	se
člení	členit	k5eAaImIp3nS	členit
na	na	k7c4	na
3	[number]	k4	3
geomorfologické	geomorfologický	k2eAgInPc4d1	geomorfologický
okrsky	okrsek	k1gInPc4	okrsek
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
masivní	masivní	k2eAgFnSc1d1	masivní
severní	severní	k2eAgFnSc1d1	severní
část	část	k1gFnSc1	část
<g/>
:	:	kIx,	:
Řásenská	Řásenský	k2eAgFnSc1d1	Řásenský
vrchovina	vrchovina	k1gFnSc1	vrchovina
s	s	k7c7	s
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
Javořice	Javořice	k1gFnSc2	Javořice
</s>
</p>
<p>
<s>
střední	střední	k2eAgFnSc1d1	střední
část	část	k1gFnSc1	část
<g/>
:	:	kIx,	:
Mrákotínská	Mrákotínský	k2eAgFnSc1d1	Mrákotínská
sníženina	sníženina	k1gFnSc1	sníženina
s	s	k7c7	s
velkými	velký	k2eAgInPc7d1	velký
žulovými	žulový	k2eAgInPc7d1	žulový
lomy	lom	k1gInPc7	lom
u	u	k7c2	u
Mrákotína	Mrákotín	k1gInSc2	Mrákotín
</s>
</p>
<p>
<s>
jižní	jižní	k2eAgFnSc1d1	jižní
část	část	k1gFnSc1	část
Pivničky	pivnička	k1gFnSc2	pivnička
(	(	kIx(	(
<g/>
nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bod	bod	k1gInSc1	bod
<g/>
:	:	kIx,	:
Pivničky	pivnička	k1gFnSc2	pivnička
760	[number]	k4	760
m	m	kA	m
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jihlavské	jihlavský	k2eAgInPc1d1	jihlavský
vrchy	vrch	k1gInPc1	vrch
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
