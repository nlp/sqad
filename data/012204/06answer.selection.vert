<s>
Finských	finský	k2eAgInPc2d1	finský
šampionátů	šampionát	k1gInPc2	šampionát
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
na	na	k7c6	na
mezinárodních	mezinárodní	k2eAgFnPc6d1	mezinárodní
akcích	akce	k1gFnPc6	akce
startoval	startovat	k5eAaBmAgInS	startovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
