<p>
<s>
Gunnar	Gunnar	k1gInSc1	Gunnar
Valfrid	Valfrid	k1gInSc1	Valfrid
Strömstén	Strömstén	k1gInSc1	Strömstén
(	(	kIx(	(
<g/>
23	[number]	k4	23
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1885	[number]	k4	1885
–	–	k?	–
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
finský	finský	k2eAgMnSc1d1	finský
rychlobruslař	rychlobruslař	k1gMnSc1	rychlobruslař
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Finských	finský	k2eAgInPc2d1	finský
šampionátů	šampionát	k1gInPc2	šampionát
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgInS	účastnit
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1904	[number]	k4	1904
<g/>
,	,	kIx,	,
na	na	k7c6	na
mezinárodních	mezinárodní	k2eAgFnPc6d1	mezinárodní
akcích	akce	k1gFnPc6	akce
startoval	startovat	k5eAaBmAgInS	startovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1906	[number]	k4	1906
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
Mistrovství	mistrovství	k1gNnSc4	mistrovství
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
MS	MS	kA	MS
1908	[number]	k4	1908
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c6	na
čtvrté	čtvrtý	k4xOgFnSc6	čtvrtý
příčce	příčka	k1gFnSc6	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Největších	veliký	k2eAgInPc2d3	veliký
úspěchů	úspěch	k1gInPc2	úspěch
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
roku	rok	k1gInSc2	rok
1912	[number]	k4	1912
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
získal	získat	k5eAaPmAgMnS	získat
stříbrné	stříbrný	k2eAgFnPc4d1	stříbrná
medaile	medaile	k1gFnPc4	medaile
jak	jak	k8xS	jak
na	na	k7c6	na
kontinentálním	kontinentální	k2eAgInSc6d1	kontinentální
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
na	na	k7c6	na
světovém	světový	k2eAgInSc6d1	světový
šampionátu	šampionát	k1gInSc6	šampionát
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInPc4d1	poslední
závody	závod	k1gInPc4	závod
absolvoval	absolvovat	k5eAaPmAgMnS	absolvovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1915	[number]	k4	1915
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
se	se	k3xPyFc4	se
po	po	k7c6	po
obnovení	obnovení	k1gNnSc6	obnovení
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
soutěží	soutěž	k1gFnPc2	soutěž
krátce	krátce	k6eAd1	krátce
vrátil	vrátit	k5eAaPmAgMnS	vrátit
k	k	k7c3	k
rychlobruslení	rychlobruslení	k1gNnSc3	rychlobruslení
a	a	k8xC	a
startoval	startovat	k5eAaBmAgInS	startovat
na	na	k7c4	na
Mistrovství	mistrovství	k1gNnSc4	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Gunnar	Gunnar	k1gInSc1	Gunnar
Strömstén	Strömstén	k1gInSc1	Strömstén
na	na	k7c4	na
speedskatingnews	speedskatingnews	k1gInSc4	speedskatingnews
<g/>
.	.	kIx.	.
<g/>
info	info	k6eAd1	info
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
