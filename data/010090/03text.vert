<p>
<s>
Hora	hora	k1gFnSc1	hora
je	být	k5eAaImIp3nS	být
výrazná	výrazný	k2eAgFnSc1d1	výrazná
vyvýšenina	vyvýšenina	k1gFnSc1	vyvýšenina
kupovitého	kupovitý	k2eAgMnSc2d1	kupovitý
<g/>
,	,	kIx,	,
kuželovitého	kuželovitý	k2eAgInSc2d1	kuželovitý
nebo	nebo	k8xC	nebo
tabulovitého	tabulovitý	k2eAgInSc2d1	tabulovitý
tvaru	tvar	k1gInSc2	tvar
s	s	k7c7	s
relativní	relativní	k2eAgFnSc7d1	relativní
výškou	výška	k1gFnSc7	výška
nad	nad	k7c7	nad
okolním	okolní	k2eAgInSc7d1	okolní
terénem	terén	k1gInSc7	terén
300	[number]	k4	300
<g/>
–	–	k?	–
<g/>
600	[number]	k4	600
m.	m.	k?	m.
Pro	pro	k7c4	pro
vyšší	vysoký	k2eAgFnSc4d2	vyšší
vyvýšeninu	vyvýšenina	k1gFnSc4	vyvýšenina
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
pojem	pojem	k1gInSc1	pojem
velehora	velehora	k1gFnSc1	velehora
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
ale	ale	k9	ale
také	také	k9	také
hora	hora	k1gFnSc1	hora
<g/>
.	.	kIx.	.
</s>
<s>
Více	hodně	k6eAd2	hodně
hor	hora	k1gFnPc2	hora
na	na	k7c6	na
jednom	jeden	k4xCgNnSc6	jeden
místě	místo	k1gNnSc6	místo
může	moct	k5eAaImIp3nS	moct
tvořit	tvořit	k5eAaImF	tvořit
pohoří	pohoří	k1gNnSc4	pohoří
<g/>
.	.	kIx.	.
</s>
<s>
Stáří	stáří	k1gNnSc1	stáří
hor	hora	k1gFnPc2	hora
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
rozmanité	rozmanitý	k2eAgNnSc1d1	rozmanité
a	a	k8xC	a
souvisí	souviset	k5eAaImIp3nS	souviset
s	s	k7c7	s
geologickými	geologický	k2eAgNnPc7d1	geologické
obdobími	období	k1gNnPc7	období
na	na	k7c6	na
Zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Některé	některý	k3yIgFnPc1	některý
hory	hora	k1gFnPc1	hora
byly	být	k5eAaImAgFnP	být
také	také	k9	také
od	od	k7c2	od
pradávna	pradávno	k1gNnSc2	pradávno
považovány	považován	k2eAgInPc1d1	považován
za	za	k7c4	za
sídla	sídlo	k1gNnPc4	sídlo
bohů	bůh	k1gMnPc2	bůh
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Olymp	Olymp	k1gInSc1	Olymp
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
vrcholky	vrcholek	k1gInPc4	vrcholek
mnoha	mnoho	k4c2	mnoho
hor	hora	k1gFnPc2	hora
lidé	člověk	k1gMnPc1	člověk
vystoupili	vystoupit	k5eAaPmAgMnP	vystoupit
až	až	k6eAd1	až
během	během	k7c2	během
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
slézáním	slézání	k1gNnSc7	slézání
skal	skála	k1gFnPc2	skála
a	a	k8xC	a
hor	hora	k1gFnPc2	hora
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
lidská	lidský	k2eAgFnSc1d1	lidská
činnost	činnost	k1gFnSc1	činnost
zvaná	zvaný	k2eAgNnPc1d1	zvané
horolezectví	horolezectví	k1gNnPc1	horolezectví
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pojmenování	pojmenování	k1gNnSc1	pojmenování
pochází	pocházet	k5eAaImIp3nS	pocházet
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
z	z	k7c2	z
hebrejského	hebrejský	k2eAgInSc2d1	hebrejský
har	har	k?	har
(	(	kIx(	(
<g/>
hora	hora	k1gFnSc1	hora
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výška	výška	k1gFnSc1	výška
hor	hora	k1gFnPc2	hora
==	==	k?	==
</s>
</p>
<p>
<s>
Výška	výška	k1gFnSc1	výška
hor	hora	k1gFnPc2	hora
je	být	k5eAaImIp3nS	být
určována	určovat	k5eAaImNgFnS	určovat
jako	jako	k8xS	jako
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
jejich	jejich	k3xOp3gInSc2	jejich
vrcholu	vrchol	k1gInSc2	vrchol
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
je	být	k5eAaImIp3nS	být
Sněžka	Sněžka	k1gFnSc1	Sněžka
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Evropy	Evropa	k1gFnSc2	Evropa
je	být	k5eAaImIp3nS	být
tradičně	tradičně	k6eAd1	tradičně
Mont	Mont	k2eAgMnSc1d1	Mont
Blanc	Blanc	k1gMnSc1	Blanc
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
poznámka	poznámka	k1gFnSc1	poznámka
níže	níže	k1gFnSc1	níže
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
světa	svět	k1gInSc2	svět
Mount	Mounta	k1gFnPc2	Mounta
Everest	Everest	k1gInSc1	Everest
a	a	k8xC	a
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
hora	hora	k1gFnSc1	hora
Sluneční	sluneční	k2eAgFnSc2d1	sluneční
soustavy	soustava	k1gFnSc2	soustava
Olympus	Olympus	k1gMnSc1	Olympus
Mons	Mons	k1gInSc1	Mons
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
povrchu	povrch	k1gInSc6	povrch
Marsu	Mars	k1gInSc2	Mars
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
určení	určení	k1gNnSc6	určení
jiných	jiný	k2eAgNnPc2d1	jiné
kritérií	kritérion	k1gNnPc2	kritérion
výšky	výška	k1gFnSc2	výška
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
vrcholu	vrchol	k1gInSc2	vrchol
od	od	k7c2	od
středu	střed	k1gInSc2	střed
Země	zem	k1gFnSc2	zem
nebo	nebo	k8xC	nebo
převýšení	převýšení	k1gNnSc2	převýšení
mezi	mezi	k7c7	mezi
vrcholem	vrchol	k1gInSc7	vrchol
a	a	k8xC	a
patou	pata	k1gFnSc7	pata
hory	hora	k1gFnSc2	hora
<g/>
)	)	kIx)	)
mohou	moct	k5eAaImIp3nP	moct
jako	jako	k9	jako
nejvyšší	vysoký	k2eAgMnPc1d3	nejvyšší
vyjít	vyjít	k5eAaPmF	vyjít
i	i	k9	i
jiné	jiný	k2eAgFnPc4d1	jiná
hory	hora	k1gFnPc4	hora
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
ale	ale	k9	ale
obvykle	obvykle	k6eAd1	obvykle
uvádí	uvádět	k5eAaImIp3nS	uvádět
pouze	pouze	k6eAd1	pouze
jako	jako	k9	jako
kuriozita	kuriozita	k1gFnSc1	kuriozita
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Spor	spor	k1gInSc1	spor
o	o	k7c4	o
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
horu	hora	k1gFnSc4	hora
Evropy	Evropa	k1gFnSc2	Evropa
===	===	k?	===
</s>
</p>
<p>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
horou	hora	k1gFnSc7	hora
Evropy	Evropa	k1gFnSc2	Evropa
není	být	k5eNaImIp3nS	být
alpský	alpský	k2eAgMnSc1d1	alpský
Mont	Mont	k1gMnSc1	Mont
Blanc	Blanc	k1gMnSc1	Blanc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kavkazský	kavkazský	k2eAgMnSc1d1	kavkazský
Elbrus	Elbrus	k1gMnSc1	Elbrus
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
fakticky	fakticky	k6eAd1	fakticky
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Prameny	pramen	k1gInPc1	pramen
a	a	k8xC	a
zdroje	zdroj	k1gInPc1	zdroj
se	se	k3xPyFc4	se
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
liší	lišit	k5eAaImIp3nS	lišit
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
přesný	přesný	k2eAgInSc1d1	přesný
průběh	průběh	k1gInSc1	průběh
evropské	evropský	k2eAgFnSc2d1	Evropská
hranice	hranice	k1gFnSc2	hranice
na	na	k7c6	na
Kavkazu	Kavkaz	k1gInSc6	Kavkaz
není	být	k5eNaImIp3nS	být
z	z	k7c2	z
geografického	geografický	k2eAgNnSc2d1	geografické
hlediska	hledisko	k1gNnSc2	hledisko
ujednocen	ujednotit	k5eAaPmNgInS	ujednotit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hory	hora	k1gFnSc2	hora
ve	v	k7c6	v
Fennoskandinávii	Fennoskandinávie	k1gFnSc6	Fennoskandinávie
==	==	k?	==
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
Fennoskandinávii	Fennoskandinávie	k1gFnSc6	Fennoskandinávie
se	se	k3xPyFc4	se
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
přesahuje	přesahovat	k5eAaImIp3nS	přesahovat
výškou	výška	k1gFnSc7	výška
horní	horní	k2eAgFnSc4d1	horní
hranici	hranice	k1gFnSc4	hranice
lesa	les	k1gInSc2	les
někdy	někdy	k6eAd1	někdy
nazývá	nazývat	k5eAaImIp3nS	nazývat
fell	fell	k1gInSc1	fell
(	(	kIx(	(
<g/>
švédsky	švédsky	k6eAd1	švédsky
fjäll	fjälnout	k5eAaPmAgMnS	fjälnout
<g/>
,	,	kIx,	,
norsky	norsky	k6eAd1	norsky
fjell	fjell	k1gInSc1	fjell
<g/>
,	,	kIx,	,
finsky	finsky	k6eAd1	finsky
tunturi	tunturi	k6eAd1	tunturi
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
hor	hora	k1gFnPc2	hora
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
států	stát	k1gInPc2	stát
světa	svět	k1gInSc2	svět
podle	podle	k7c2	podle
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
hor	hora	k1gFnPc2	hora
</s>
</p>
<p>
<s>
Kopec	kopec	k1gInSc1	kopec
</s>
</p>
<p>
<s>
Fell	Fell	k1gInSc1	Fell
(	(	kIx(	(
<g/>
hora	hora	k1gFnSc1	hora
<g/>
,	,	kIx,	,
kopec	kopec	k1gInSc1	kopec
ve	v	k7c6	v
Skandinávii	Skandinávie	k1gFnSc6	Skandinávie
na	na	k7c6	na
Britských	britský	k2eAgInPc6d1	britský
ostrovech	ostrov	k1gInPc6	ostrov
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
hora	hora	k1gFnSc1	hora
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
hora	hora	k1gFnSc1	hora
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Hory	hora	k1gFnSc2	hora
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Téma	téma	k1gNnSc1	téma
Hora	hora	k1gFnSc1	hora
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
</p>
