<s>
Tereza	Tereza	k1gFnSc1	Tereza
Maxová	Maxová	k1gFnSc1	Maxová
(	(	kIx(	(
<g/>
*	*	kIx~	*
31	[number]	k4	31
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1971	[number]	k4	1971
Pardubice	Pardubice	k1gInPc4	Pardubice
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
česká	český	k2eAgFnSc1d1	Česká
modelka	modelka	k1gFnSc1	modelka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
žije	žít	k5eAaImIp3nS	žít
střídavě	střídavě	k6eAd1	střídavě
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
a	a	k8xC	a
Monaku	Monako	k1gNnSc6	Monako
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1975	[number]	k4	1975
se	se	k3xPyFc4	se
s	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
Ústí	ústí	k1gNnSc2	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
ukončila	ukončit	k5eAaPmAgFnS	ukončit
základní	základní	k2eAgFnSc4d1	základní
školu	škola	k1gFnSc4	škola
se	s	k7c7	s
sportovně-atletickým	sportovnětletický	k2eAgNnSc7d1	sportovně-atletický
zaměřením	zaměření	k1gNnSc7	zaměření
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
úspěšných	úspěšný	k2eAgFnPc6d1	úspěšná
přijímacích	přijímací	k2eAgFnPc6d1	přijímací
zkouškách	zkouška	k1gFnPc6	zkouška
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
na	na	k7c4	na
Gymnázium	gymnázium	k1gNnSc4	gymnázium
Budějovická	budějovický	k2eAgFnSc1d1	Budějovická
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
maturitě	maturita	k1gFnSc6	maturita
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
byla	být	k5eAaImAgFnS	být
přijata	přijmout	k5eAaPmNgFnS	přijmout
ke	k	k7c3	k
studiu	studio	k1gNnSc3	studio
na	na	k7c6	na
Právnické	právnický	k2eAgFnSc6d1	právnická
fakultě	fakulta	k1gFnSc6	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Karlovy	Karlův	k2eAgFnSc2d1	Karlova
<g/>
.	.	kIx.	.
</s>
<s>
Tu	tu	k6eAd1	tu
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
letech	léto	k1gNnPc6	léto
individuálního	individuální	k2eAgNnSc2d1	individuální
studia	studio	k1gNnSc2	studio
ukončit	ukončit	k5eAaPmF	ukončit
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2000	[number]	k4	2000
se	se	k3xPyFc4	se
v	v	k7c6	v
Dánsku	Dánsko	k1gNnSc6	Dánsko
provdala	provdat	k5eAaPmAgFnS	provdat
za	za	k7c4	za
tenistu	tenista	k1gMnSc4	tenista
Frederika	Frederik	k1gMnSc4	Frederik
Fetterleina	Fetterlein	k1gMnSc4	Fetterlein
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
manželství	manželství	k1gNnSc2	manželství
se	s	k7c7	s
9	[number]	k4	9
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2000	[number]	k4	2000
narodil	narodit	k5eAaPmAgMnS	narodit
syn	syn	k1gMnSc1	syn
<g/>
,	,	kIx,	,
Tobias	Tobias	k1gMnSc1	Tobias
Joshua	Joshua	k1gMnSc1	Joshua
Maxa	Maxa	k1gMnSc1	Maxa
Fetterlein	Fetterlein	k1gMnSc1	Fetterlein
<g/>
.	.	kIx.	.
</s>
<s>
Manželství	manželství	k1gNnSc1	manželství
se	se	k3xPyFc4	se
však	však	k9	však
rozpadlo	rozpadnout	k5eAaPmAgNnS	rozpadnout
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
Monaku	Monako	k1gNnSc6	Monako
s	s	k7c7	s
Burakem	Burak	k1gMnSc7	Burak
Oymenem	Oymen	k1gMnSc7	Oymen
<g/>
,	,	kIx,	,
tureckým	turecký	k2eAgMnSc7d1	turecký
podnikatelem	podnikatel	k1gMnSc7	podnikatel
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
má	mít	k5eAaImIp3nS	mít
dceru	dcera	k1gFnSc4	dcera
Mínu	Mína	k1gFnSc4	Mína
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
a	a	k8xC	a
syna	syn	k1gMnSc4	syn
Aidena	Aiden	k1gMnSc4	Aiden
(	(	kIx(	(
<g/>
nar	nar	kA	nar
<g/>
.	.	kIx.	.
v	v	k7c6	v
září	září	k1gNnSc6	září
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Provdala	provdat	k5eAaPmAgFnS	provdat
se	se	k3xPyFc4	se
za	za	k7c2	za
něho	on	k3xPp3gNnSc2	on
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2016	[number]	k4	2016
v	v	k7c6	v
tureckém	turecký	k2eAgInSc6d1	turecký
Bodrumu	Bodrum	k1gInSc6	Bodrum
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žije	žít	k5eAaImIp3nS	žít
manželova	manželův	k2eAgFnSc1d1	manželova
rodina	rodina	k1gFnSc1	rodina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
sedmnácti	sedmnáct	k4xCc6	sedmnáct
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
zúčastnila	zúčastnit	k5eAaPmAgFnS	zúčastnit
s	s	k7c7	s
kamarádkou	kamarádka	k1gFnSc7	kamarádka
Evou	Eva	k1gFnSc7	Eva
Herzigovou	Herzigův	k2eAgFnSc7d1	Herzigova
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
stále	stále	k6eAd1	stále
panujícího	panující	k2eAgInSc2d1	panující
komunismu	komunismus	k1gInSc2	komunismus
<g/>
,	,	kIx,	,
konkurzu	konkurz	k1gInSc2	konkurz
francouzské	francouzský	k2eAgFnSc2d1	francouzská
modelingové	modelingový	k2eAgFnSc2d1	modelingová
agentury	agentura	k1gFnSc2	agentura
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
profesionální	profesionální	k2eAgFnSc1d1	profesionální
kariéra	kariéra	k1gFnSc1	kariéra
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
sametovou	sametový	k2eAgFnSc7d1	sametová
revolucí	revoluce	k1gFnSc7	revoluce
odjela	odjet	k5eAaPmAgFnS	odjet
do	do	k7c2	do
Paříže	Paříž	k1gFnSc2	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
modelingové	modelingový	k2eAgFnSc2d1	modelingová
kariéry	kariéra	k1gFnSc2	kariéra
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
s	s	k7c7	s
řadou	řada	k1gFnSc7	řada
známých	známý	k2eAgMnPc2d1	známý
fotografů	fotograf	k1gMnPc2	fotograf
od	od	k7c2	od
Patrika	Patrik	k1gMnSc2	Patrik
Demarcheliera	Demarchelier	k1gMnSc2	Demarchelier
<g/>
,	,	kIx,	,
až	až	k9	až
po	po	k7c4	po
Maria	Mario	k1gMnSc4	Mario
Testina	testin	k2eAgMnSc4d1	testin
a	a	k8xC	a
Petera	Peter	k1gMnSc4	Peter
Lindberga	Lindberg	k1gMnSc4	Lindberg
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
tvář	tvář	k1gFnSc1	tvář
byla	být	k5eAaImAgFnS	být
titulní	titulní	k2eAgFnSc4d1	titulní
straně	strana	k1gFnSc6	strana
britského	britský	k2eAgMnSc2d1	britský
Vogue	Vogu	k1gMnSc2	Vogu
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
na	na	k7c6	na
předních	přední	k2eAgFnPc6d1	přední
stranách	strana	k1gFnPc6	strana
Elle	Ell	k1gFnSc2	Ell
<g/>
,	,	kIx,	,
Marie	Maria	k1gFnSc2	Maria
Claire	Clair	k1gInSc5	Clair
a	a	k8xC	a
Glamouru	Glamour	k1gInSc6	Glamour
<g/>
.	.	kIx.	.
</s>
<s>
Prošla	projít	k5eAaPmAgFnS	projít
po	po	k7c6	po
významných	významný	k2eAgNnPc6d1	významné
přehlídkových	přehlídkový	k2eAgNnPc6d1	přehlídkové
molech	molo	k1gNnPc6	molo
–	–	k?	–
Dior	Dior	k1gMnSc1	Dior
<g/>
,	,	kIx,	,
Chanel	Chanel	k1gMnSc1	Chanel
<g/>
,	,	kIx,	,
Prada	Prada	k1gMnSc1	Prada
<g/>
,	,	kIx,	,
Gucci	Gucce	k1gMnPc1	Gucce
<g/>
,	,	kIx,	,
Yves	Yves	k1gInSc1	Yves
Saint-Laurent	Saint-Laurent	k1gInSc1	Saint-Laurent
<g/>
,	,	kIx,	,
Ralph	Ralph	k1gInSc1	Ralph
Lauren	Laurna	k1gFnPc2	Laurna
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
tváří	tvář	k1gFnSc7	tvář
pro	pro	k7c4	pro
reklamní	reklamní	k2eAgFnPc4d1	reklamní
kampaně	kampaň	k1gFnPc4	kampaň
značek	značka	k1gFnPc2	značka
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
Karl	Karl	k1gMnSc1	Karl
Lagerfeld	Lagerfeld	k1gMnSc1	Lagerfeld
<g/>
,	,	kIx,	,
Donna	donna	k1gFnSc1	donna
Karan	Karan	k1gInSc1	Karan
<g/>
,	,	kIx,	,
Hermés	Hermés	k1gInSc1	Hermés
<g/>
,	,	kIx,	,
La	la	k1gNnPc1	la
Perla	perla	k1gFnSc1	perla
<g/>
,	,	kIx,	,
Vichy	Vicha	k1gFnPc1	Vicha
<g/>
,	,	kIx,	,
Oriflame	Oriflam	k1gInSc5	Oriflam
a	a	k8xC	a
L	L	kA	L
<g/>
́	́	k?	́
<g/>
Oréal	Oréal	k1gInSc1	Oréal
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
návštěva	návštěva	k1gFnSc1	návštěva
pražského	pražský	k2eAgInSc2d1	pražský
kojeneckého	kojenecký	k2eAgInSc2d1	kojenecký
ústavu	ústav	k1gInSc2	ústav
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1996	[number]	k4	1996
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgFnS	změnit
v	v	k7c4	v
dlouhotrvající	dlouhotrvající	k2eAgFnSc4d1	dlouhotrvající
pomoc	pomoc	k1gFnSc4	pomoc
opuštěným	opuštěný	k2eAgFnPc3d1	opuštěná
dětem	dítě	k1gFnPc3	dítě
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
začala	začít	k5eAaPmAgFnS	začít
se	se	k3xPyFc4	se
svojí	svůj	k3xOyFgFnSc7	svůj
charitativní	charitativní	k2eAgFnSc7d1	charitativní
činností	činnost	k1gFnSc7	činnost
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Nadace	nadace	k1gFnSc2	nadace
Terezy	Tereza	k1gFnSc2	Tereza
Maxové	Maxová	k1gFnSc2	Maxová
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
opuštěným	opuštěný	k2eAgFnPc3d1	opuštěná
dětem	dítě	k1gFnPc3	dítě
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
200	[number]	k4	200
dětských	dětský	k2eAgFnPc2d1	dětská
domovech	domov	k1gInPc6	domov
<g/>
,	,	kIx,	,
kojeneckých	kojenecký	k2eAgInPc6d1	kojenecký
ústavech	ústav	k1gInPc6	ústav
a	a	k8xC	a
dětských	dětský	k2eAgNnPc6d1	dětské
centrech	centrum	k1gNnPc6	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Nadace	nadace	k1gFnSc1	nadace
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
asociace	asociace	k1gFnSc2	asociace
nadací	nadace	k1gFnPc2	nadace
<g/>
,	,	kIx,	,
Fóra	fórum	k1gNnPc4	fórum
dárců	dárce	k1gMnPc2	dárce
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2013	[number]	k4	2013
přerozdělila	přerozdělit	k5eAaPmAgFnS	přerozdělit
a	a	k8xC	a
utratila	utratit	k5eAaPmAgFnS	utratit
okolo	okolo	k7c2	okolo
220	[number]	k4	220
miliónů	milión	k4xCgInPc2	milión
korun	koruna	k1gFnPc2	koruna
<g/>
.	.	kIx.	.
</s>
