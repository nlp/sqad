<s>
Spánková	spánkový	k2eAgFnSc1d1	spánková
deprivace	deprivace	k1gFnSc1	deprivace
je	být	k5eAaImIp3nS	být
nedostatek	nedostatek	k1gInSc4	nedostatek
normálního	normální	k2eAgNnSc2d1	normální
množství	množství	k1gNnSc2	množství
spánku	spánek	k1gInSc2	spánek
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
používaný	používaný	k2eAgInSc4d1	používaný
jako	jako	k8xC	jako
druh	druh	k1gInSc4	druh
mučení	mučení	k1gNnPc2	mučení
<g/>
.	.	kIx.	.
</s>
<s>
Způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
ospalost	ospalost	k1gFnSc1	ospalost
<g/>
,	,	kIx,	,
nemožnost	nemožnost	k1gFnSc1	nemožnost
soustředění	soustředění	k1gNnSc2	soustředění
a	a	k8xC	a
halucinace	halucinace	k1gFnSc2	halucinace
<g/>
.	.	kIx.	.
</s>
<s>
Asi	asi	k9	asi
90	[number]	k4	90
%	%	kIx~	%
dospělých	dospělí	k1gMnPc2	dospělí
spí	spát	k5eAaImIp3nS	spát
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Spí	spát	k5eAaImIp3nS	spát
<g/>
-li	i	k?	-li
člověk	člověk	k1gMnSc1	člověk
kratší	krátký	k2eAgFnSc4d2	kratší
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
optimum	optimum	k1gNnSc1	optimum
<g/>
,	,	kIx,	,
jeví	jevit	k5eAaImIp3nS	jevit
během	během	k7c2	během
dne	den	k1gInSc2	den
známky	známka	k1gFnSc2	známka
ospalosti	ospalost	k1gFnSc2	ospalost
<g/>
.	.	kIx.	.
</s>
<s>
Každodenní	každodenní	k2eAgFnSc1d1	každodenní
ztráta	ztráta	k1gFnSc1	ztráta
jedné	jeden	k4xCgFnSc2	jeden
hodiny	hodina	k1gFnSc2	hodina
spánku	spánek	k1gInSc2	spánek
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
jednoho	jeden	k4xCgInSc2	jeden
týdne	týden	k1gInSc2	týden
se	se	k3xPyFc4	se
tak	tak	k9	tak
vlastně	vlastně	k9	vlastně
rovná	rovnat	k5eAaImIp3nS	rovnat
jedné	jeden	k4xCgFnSc6	jeden
probdělé	probdělý	k2eAgFnSc6d1	probdělá
noci	noc	k1gFnSc6	noc
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
si	se	k3xPyFc3	se
osmihodinový	osmihodinový	k2eAgInSc4d1	osmihodinový
spánek	spánek	k1gInSc4	spánek
prodloužíme	prodloužit	k5eAaPmIp1nP	prodloužit
o	o	k7c4	o
další	další	k2eAgFnPc4d1	další
dvě	dva	k4xCgFnPc4	dva
hodiny	hodina	k1gFnPc4	hodina
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
u	u	k7c2	u
nás	my	k3xPp1nPc2	my
dojít	dojít	k5eAaPmF	dojít
k	k	k7c3	k
zásadnímu	zásadní	k2eAgNnSc3d1	zásadní
zvýšení	zvýšení	k1gNnSc3	zvýšení
bdělosti	bdělost	k1gFnSc2	bdělost
<g/>
.	.	kIx.	.
</s>
<s>
Spánek	spánek	k1gInSc1	spánek
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
ze	z	k7c2	z
základních	základní	k2eAgFnPc2d1	základní
potřeb	potřeba	k1gFnPc2	potřeba
<g/>
,	,	kIx,	,
člověk	člověk	k1gMnSc1	člověk
bez	bez	k7c2	bez
spánku	spánek	k1gInSc2	spánek
dlouho	dlouho	k6eAd1	dlouho
nevydrží	vydržet	k5eNaPmIp3nP	vydržet
<g/>
.	.	kIx.	.
</s>
<s>
Uvádí	uvádět	k5eAaImIp3nS	uvádět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
rekord	rekord	k1gInSc1	rekord
v	v	k7c6	v
nespaní	nespaní	k1gNnSc6	nespaní
drží	držet	k5eAaImIp3nS	držet
americký	americký	k2eAgMnSc1d1	americký
student	student	k1gMnSc1	student
Randy	rand	k1gInPc4	rand
Gardner	Gardnero	k1gNnPc2	Gardnero
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
roku	rok	k1gInSc2	rok
1965	[number]	k4	1965
v	v	k7c6	v
17	[number]	k4	17
letech	léto	k1gNnPc6	léto
vydržel	vydržet	k5eAaPmAgMnS	vydržet
11	[number]	k4	11
dnů	den	k1gInPc2	den
za	za	k7c7	za
sebou	se	k3xPyFc7	se
nespat	spát	k5eNaImF	spát
<g/>
.	.	kIx.	.
</s>
<s>
Ke	k	k7c3	k
konci	konec	k1gInSc3	konec
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc2	období
měl	mít	k5eAaImAgInS	mít
už	už	k6eAd1	už
velmi	velmi	k6eAd1	velmi
útržkovité	útržkovitý	k2eAgNnSc1d1	útržkovité
myšlení	myšlení	k1gNnSc1	myšlení
<g/>
,	,	kIx,	,
poruchy	porucha	k1gFnPc1	porucha
paměti	paměť	k1gFnPc1	paměť
<g/>
,	,	kIx,	,
halucinace	halucinace	k1gFnPc1	halucinace
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
experiment	experiment	k1gInSc1	experiment
byl	být	k5eAaImAgInS	být
nakonec	nakonec	k6eAd1	nakonec
z	z	k7c2	z
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
lékařů	lékař	k1gMnPc2	lékař
přerušen	přerušen	k2eAgInSc1d1	přerušen
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
hrozilo	hrozit	k5eAaImAgNnS	hrozit
naprosté	naprostý	k2eAgNnSc1d1	naprosté
vyčerpání	vyčerpání	k1gNnSc1	vyčerpání
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
spánková	spánkový	k2eAgFnSc1d1	spánková
deprivace	deprivace	k1gFnSc1	deprivace
nezanechala	zanechat	k5eNaPmAgFnS	zanechat
na	na	k7c6	na
Gardnerovi	Gardner	k1gMnSc6	Gardner
žádné	žádný	k3yNgInPc4	žádný
trvalé	trvalý	k2eAgInPc4d1	trvalý
následky	následek	k1gInPc4	následek
<g/>
.	.	kIx.	.
</s>
<s>
Spánková	spánkový	k2eAgFnSc1d1	spánková
deprivace	deprivace	k1gFnSc1	deprivace
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
následující	následující	k2eAgInPc1d1	následující
problémy	problém	k1gInPc1	problém
<g/>
:	:	kIx,	:
ztráty	ztráta	k1gFnPc1	ztráta
bdělosti	bdělost	k1gFnSc2	bdělost
a	a	k8xC	a
pozornosti	pozornost	k1gFnSc2	pozornost
během	během	k7c2	během
dne	den	k1gInSc2	den
–	–	k?	–
to	ten	k3xDgNnSc4	ten
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
nebezpečné	bezpečný	k2eNgNnSc1d1	nebezpečné
zejména	zejména	k9	zejména
u	u	k7c2	u
řidičů	řidič	k1gMnPc2	řidič
<g/>
,	,	kIx,	,
lékařů	lékař	k1gMnPc2	lékař
a	a	k8xC	a
dalších	další	k2eAgNnPc2d1	další
povolání	povolání	k1gNnPc2	povolání
<g/>
.	.	kIx.	.
20	[number]	k4	20
%	%	kIx~	%
řidičů	řidič	k1gMnPc2	řidič
a	a	k8xC	a
56	[number]	k4	56
%	%	kIx~	%
dálkových	dálkový	k2eAgMnPc2d1	dálkový
řidičů	řidič	k1gMnPc2	řidič
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
alespoň	alespoň	k9	alespoň
jednou	jeden	k4xCgFnSc7	jeden
během	během	k7c2	během
řízení	řízení	k1gNnSc2	řízení
upadli	upadnout	k5eAaPmAgMnP	upadnout
do	do	k7c2	do
krátkého	krátký	k2eAgInSc2d1	krátký
spánku	spánek	k1gInSc2	spánek
<g />
.	.	kIx.	.
</s>
<s>
<g/>
nespavost	nespavost	k1gFnSc1	nespavost
–	–	k?	–
člověk	člověk	k1gMnSc1	člověk
má	mít	k5eAaImIp3nS	mít
narušený	narušený	k2eAgInSc1d1	narušený
spánkový	spánkový	k2eAgInSc1d1	spánkový
rytmus	rytmus	k1gInSc1	rytmus
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
obtížným	obtížný	k2eAgNnSc7d1	obtížné
usínáním	usínání	k1gNnSc7	usínání
a	a	k8xC	a
přerušovaným	přerušovaný	k2eAgInSc7d1	přerušovaný
spánkem	spánek	k1gInSc7	spánek
u	u	k7c2	u
těžkých	těžký	k2eAgFnPc2d1	těžká
spánkových	spánkový	k2eAgFnPc2d1	spánková
deprivací	deprivace	k1gFnPc2	deprivace
se	se	k3xPyFc4	se
dostavují	dostavovat	k5eAaImIp3nP	dostavovat
<g/>
:	:	kIx,	:
halucinace	halucinace	k1gFnPc1	halucinace
<g/>
,	,	kIx,	,
paranoie	paranoia	k1gFnPc1	paranoia
<g/>
,	,	kIx,	,
odbývání	odbývání	k1gNnSc1	odbývání
životních	životní	k2eAgFnPc2d1	životní
potřeb	potřeba	k1gFnPc2	potřeba
<g/>
,	,	kIx,	,
ztráta	ztráta	k1gFnSc1	ztráta
myšlenkové	myšlenkový	k2eAgFnSc2d1	myšlenková
kontinuity	kontinuita	k1gFnSc2	kontinuita
Z	z	k7c2	z
pokusů	pokus	k1gInPc2	pokus
na	na	k7c6	na
dospělých	dospělý	k2eAgFnPc6d1	dospělá
kočkách	kočka	k1gFnPc6	kočka
a	a	k8xC	a
psech	pes	k1gMnPc6	pes
je	být	k5eAaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
15	[number]	k4	15
dnech	den	k1gInPc6	den
nepřetržitého	přetržitý	k2eNgNnSc2d1	nepřetržité
bdění	bdění	k1gNnSc2	bdění
zvířata	zvíře	k1gNnPc4	zvíře
hynula	hynout	k5eAaImAgFnS	hynout
na	na	k7c4	na
celkové	celkový	k2eAgNnSc4d1	celkové
vyčerpání	vyčerpání	k1gNnSc4	vyčerpání
organismu	organismus	k1gInSc2	organismus
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgMnSc1d1	americký
filozof	filozof	k1gMnSc1	filozof
Allen	Allen	k1gMnSc1	Allen
Rechtschaffen	Rechtschaffno	k1gNnPc2	Rechtschaffno
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
zabraňoval	zabraňovat	k5eAaImAgInS	zabraňovat
krysám	krysa	k1gFnPc3	krysa
ve	v	k7c6	v
spánku	spánek	k1gInSc6	spánek
<g/>
.	.	kIx.	.
</s>
<s>
Experimentální	experimentální	k2eAgMnPc1d1	experimentální
hlodavci	hlodavec	k1gMnPc1	hlodavec
vykazovali	vykazovat	k5eAaImAgMnP	vykazovat
za	za	k7c4	za
několik	několik	k4yIc4	několik
dnů	den	k1gInPc2	den
známky	známka	k1gFnSc2	známka
celkového	celkový	k2eAgNnSc2d1	celkové
oslabení	oslabení	k1gNnSc2	oslabení
<g/>
.	.	kIx.	.
</s>
<s>
Krysy	krysa	k1gFnPc1	krysa
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
chovat	chovat	k5eAaImF	chovat
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
kdyby	kdyby	kYmCp3nP	kdyby
byly	být	k5eAaImAgFnP	být
vystaveny	vystavit	k5eAaPmNgFnP	vystavit
nepřetržitě	přetržitě	k6eNd1	přetržitě
působícímu	působící	k2eAgInSc3d1	působící
stresu	stres	k1gInSc3	stres
<g/>
.	.	kIx.	.
</s>
<s>
Objevily	objevit	k5eAaPmAgFnP	objevit
se	se	k3xPyFc4	se
poruchy	porucha	k1gFnSc2	porucha
pohyblivosti	pohyblivost	k1gFnSc2	pohyblivost
<g/>
,	,	kIx,	,
úbytek	úbytek	k1gInSc4	úbytek
na	na	k7c6	na
váze	váha	k1gFnSc6	váha
(	(	kIx(	(
<g/>
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
navzdory	navzdory	k7c3	navzdory
zvýšenému	zvýšený	k2eAgNnSc3d1	zvýšené
příjmu	příjem	k1gInSc2	příjem
potravy	potrava	k1gFnSc2	potrava
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
znatelně	znatelně	k6eAd1	znatelně
se	se	k3xPyFc4	se
snížila	snížit	k5eAaPmAgFnS	snížit
průměrná	průměrný	k2eAgFnSc1d1	průměrná
EEG	EEG	kA	EEG
amplituda	amplituda	k1gFnSc1	amplituda
<g/>
,	,	kIx,	,
docházelo	docházet	k5eAaImAgNnS	docházet
ke	k	k7c3	k
zduření	zduření	k1gNnSc3	zduření
žláz	žláza	k1gFnPc2	žláza
s	s	k7c7	s
vnitřní	vnitřní	k2eAgFnSc7d1	vnitřní
sekrecí	sekrece	k1gFnSc7	sekrece
<g/>
,	,	kIx,	,
objevily	objevit	k5eAaPmAgInP	objevit
se	se	k3xPyFc4	se
žaludeční	žaludeční	k2eAgInPc1d1	žaludeční
vředy	vřed	k1gInPc1	vřed
<g/>
.	.	kIx.	.
</s>
<s>
Deprivované	Deprivovaný	k2eAgFnPc1d1	Deprivovaný
krysy	krysa	k1gFnPc1	krysa
hynuly	hynout	k5eAaImAgFnP	hynout
–	–	k?	–
některé	některý	k3yIgFnPc4	některý
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
již	již	k6eAd1	již
po	po	k7c6	po
5	[number]	k4	5
dnech	den	k1gInPc6	den
beze	beze	k7c2	beze
spánku	spánek	k1gInSc6	spánek
<g/>
,	,	kIx,	,
jiné	jiný	k2eAgFnPc1d1	jiná
vydržely	vydržet	k5eAaPmAgFnP	vydržet
až	až	k9	až
30	[number]	k4	30
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
HUGHES	HUGHES	kA	HUGHES
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
obrazová	obrazový	k2eAgFnSc1d1	obrazová
všeobecná	všeobecný	k2eAgFnSc1d1	všeobecná
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
:	:	kIx,	:
Svojtka	Svojtka	k1gFnSc1	Svojtka
&	&	k?	&
Co	co	k9	co
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
1999	[number]	k4	1999
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7237	[number]	k4	7237
<g/>
-	-	kIx~	-
<g/>
256	[number]	k4	256
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
Kapitola	kapitola	k1gFnSc1	kapitola
Potraviny	potravina	k1gFnSc2	potravina
a	a	k8xC	a
výživa	výživa	k1gFnSc1	výživa
-	-	kIx~	-
spánek	spánek	k1gInSc1	spánek
a	a	k8xC	a
sny	sen	k1gInPc1	sen
<g/>
,	,	kIx,	,
s.	s.	k?	s.
168	[number]	k4	168
<g/>
.	.	kIx.	.
</s>
<s>
KASSIN	KASSIN	kA	KASSIN
<g/>
,	,	kIx,	,
Saul	Saul	k1gMnSc1	Saul
M.	M.	kA	M.
<g/>
.	.	kIx.	.
</s>
<s>
Psychologie	psychologie	k1gFnSc1	psychologie
<g/>
.	.	kIx.	.
</s>
<s>
Brno	Brno	k1gNnSc1	Brno
:	:	kIx,	:
Computer	computer	k1gInSc1	computer
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
251	[number]	k4	251
<g/>
-	-	kIx~	-
<g/>
1716	[number]	k4	1716
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
143	[number]	k4	143
-	-	kIx~	-
144	[number]	k4	144
<g/>
.	.	kIx.	.
</s>
<s>
Spánek	spánek	k1gInSc1	spánek
Bdění	bdění	k1gNnSc2	bdění
Deprivace	deprivace	k1gFnSc2	deprivace
Pásmová	pásmový	k2eAgFnSc1d1	pásmová
nemoc	nemoc	k1gFnSc1	nemoc
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Spánková	spánkový	k2eAgFnSc1d1	spánková
deprivace	deprivace	k1gFnSc1	deprivace
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Psychologie	psychologie	k1gFnSc1	psychologie
–	–	k?	–
web	web	k1gInSc4	web
o	o	k7c4	o
psychologii	psychologie	k1gFnSc4	psychologie
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
</s>
