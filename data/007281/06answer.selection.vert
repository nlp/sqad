<s>
Spánková	spánkový	k2eAgFnSc1d1	spánková
deprivace	deprivace	k1gFnSc1	deprivace
je	být	k5eAaImIp3nS	být
nedostatek	nedostatek	k1gInSc4	nedostatek
normálního	normální	k2eAgNnSc2d1	normální
množství	množství	k1gNnSc2	množství
spánku	spánek	k1gInSc2	spánek
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
používaný	používaný	k2eAgInSc4d1	používaný
jako	jako	k8xS	jako
druh	druh	k1gInSc4	druh
mučení	mučení	k1gNnPc2	mučení
<g/>
.	.	kIx.	.
</s>
