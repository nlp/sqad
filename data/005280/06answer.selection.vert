<s>
Platónovy	Platónův	k2eAgInPc1d1
písemné	písemný	k2eAgInPc1d1
dialogy	dialog	k1gInPc1
Timaios	Timaiosa	k1gFnPc2
a	a	k8xC
Kritias	Kritiasa	k1gFnPc2
<g/>
,	,	kIx,
napsané	napsaný	k2eAgInPc1d1
v	v	k7c6
roce	rok	k1gInSc6
360	[number]	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
jediným	jediný	k2eAgInSc7d1
zdrojem	zdroj	k1gInSc7
informací	informace	k1gFnPc2
o	o	k7c6
Atlantidě	Atlantida	k1gFnSc6
<g/>
.	.	kIx.
</s>