<s>
Atlantida	Atlantida	k1gFnSc1	Atlantida
je	být	k5eAaImIp3nS	být
legendární	legendární	k2eAgInSc4d1	legendární
kontinent	kontinent	k1gInSc4	kontinent
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yIgInSc6	který
měla	mít	k5eAaImAgFnS	mít
sídlit	sídlit	k5eAaImF	sídlit
vysoce	vysoce	k6eAd1	vysoce
vyvinutá	vyvinutý	k2eAgFnSc1d1	vyvinutá
civilizace	civilizace	k1gFnSc1	civilizace
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
zničila	zničit	k5eAaPmAgFnS	zničit
mohutná	mohutný	k2eAgFnSc1d1	mohutná
přírodní	přírodní	k2eAgFnSc1d1	přírodní
katastrofa	katastrofa	k1gFnSc1	katastrofa
přibližně	přibližně	k6eAd1	přibližně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
9600	[number]	k4	9600
př.n.l.	př.n.l.	k?	př.n.l.
Přestože	přestože	k8xS	přestože
existence	existence	k1gFnSc1	existence
tohoto	tento	k3xDgInSc2	tento
kontinentu	kontinent	k1gInSc2	kontinent
nebyla	být	k5eNaImAgNnP	být
nikdy	nikdy	k6eAd1	nikdy
přesvědčivě	přesvědčivě	k6eAd1	přesvědčivě
potvrzena	potvrdit	k5eAaPmNgFnS	potvrdit
<g/>
,	,	kIx,	,
vedou	vést	k5eAaImIp3nP	vést
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
debaty	debata	k1gFnSc2	debata
o	o	k7c6	o
její	její	k3xOp3gFnSc6	její
existenci	existence	k1gFnSc6	existence
a	a	k8xC	a
možné	možný	k2eAgFnSc3d1	možná
poloze	poloha	k1gFnSc3	poloha
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgInSc7d1	jediný
zdrojem	zdroj	k1gInSc7	zdroj
informací	informace	k1gFnPc2	informace
jsou	být	k5eAaImIp3nP	být
Platónovy	Platónův	k2eAgInPc1d1	Platónův
spisy	spis	k1gInPc1	spis
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
popisuje	popisovat	k5eAaImIp3nS	popisovat
atlantskou	atlantský	k2eAgFnSc4d1	Atlantská
kulturu	kultura	k1gFnSc4	kultura
<g/>
,	,	kIx,	,
státní	státní	k2eAgNnSc4d1	státní
zřízení	zřízení	k1gNnSc4	zřízení
a	a	k8xC	a
polohu	poloha	k1gFnSc4	poloha
<g/>
;	;	kIx,	;
měla	mít	k5eAaImAgFnS	mít
ležet	ležet	k5eAaImF	ležet
někde	někde	k6eAd1	někde
za	za	k7c7	za
Héraklovými	Héraklův	k2eAgInPc7d1	Héraklův
sloupy	sloup	k1gInPc7	sloup
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc1d1	dnešní
Gibraltarský	gibraltarský	k2eAgInSc1d1	gibraltarský
průliv	průliv	k1gInSc1	průliv
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Platónovy	Platónův	k2eAgInPc1d1	Platónův
písemné	písemný	k2eAgInPc1d1	písemný
dialogy	dialog	k1gInPc1	dialog
Timaios	Timaiosa	k1gFnPc2	Timaiosa
a	a	k8xC	a
Kritias	Kritiasa	k1gFnPc2	Kritiasa
<g/>
,	,	kIx,	,
napsané	napsaný	k2eAgInPc1d1	napsaný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
360	[number]	k4	360
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
jediným	jediný	k2eAgInSc7d1	jediný
zdrojem	zdroj	k1gInSc7	zdroj
informací	informace	k1gFnPc2	informace
o	o	k7c6	o
Atlantidě	Atlantida	k1gFnSc6	Atlantida
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
těchto	tento	k3xDgInPc6	tento
dialozích	dialog	k1gInPc6	dialog
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
Platón	platón	k1gInSc1	platón
na	na	k7c4	na
vyprávění	vyprávění	k1gNnSc4	vyprávění
řeckého	řecký	k2eAgMnSc2d1	řecký
státníka	státník	k1gMnSc2	státník
Solóna	Solón	k1gMnSc2	Solón
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
prý	prý	k9	prý
vše	všechen	k3xTgNnSc1	všechen
o	o	k7c6	o
Atlantidě	Atlantida	k1gFnSc6	Atlantida
dozvěděl	dozvědět	k5eAaPmAgMnS	dozvědět
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
cestě	cesta	k1gFnSc6	cesta
do	do	k7c2	do
Egypta	Egypt	k1gInSc2	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Platóna	Platón	k1gMnSc2	Platón
existoval	existovat	k5eAaImAgInS	existovat
9000	[number]	k4	9000
let	léto	k1gNnPc2	léto
před	před	k7c7	před
dobou	doba	k1gFnSc7	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
žil	žít	k5eAaImAgMnS	žít
Solón	Solón	k1gMnSc1	Solón
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
9600	[number]	k4	9600
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
za	za	k7c7	za
Herkulovými	Herkulův	k2eAgInPc7d1	Herkulův
sloupy	sloup	k1gInPc7	sloup
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc1d1	dnešní
Gibraltarský	gibraltarský	k2eAgInSc1d1	gibraltarský
průliv	průliv	k1gInSc1	průliv
<g/>
)	)	kIx)	)
kontinent	kontinent	k1gInSc1	kontinent
o	o	k7c6	o
velikosti	velikost	k1gFnSc6	velikost
Asie	Asie	k1gFnSc2	Asie
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Malé	Malé	k2eAgFnSc2d1	Malé
Asie	Asie	k1gFnSc2	Asie
<g/>
)	)	kIx)	)
a	a	k8xC	a
Libye	Libye	k1gFnSc1	Libye
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
severní	severní	k2eAgFnSc2d1	severní
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
panuje	panovat	k5eAaImIp3nS	panovat
Poseidón	Poseidón	k1gMnSc1	Poseidón
<g/>
,	,	kIx,	,
bůh	bůh	k1gMnSc1	bůh
moří	mořit	k5eAaImIp3nS	mořit
<g/>
;	;	kIx,	;
název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Atlantida	Atlantida	k1gFnSc1	Atlantida
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
jména	jméno	k1gNnSc2	jméno
jeho	jeho	k3xOp3gMnSc2	jeho
nejstaršího	starý	k2eAgMnSc2d3	nejstarší
syna	syn	k1gMnSc2	syn
Atlanta	Atlanta	k1gFnSc1	Atlanta
(	(	kIx(	(
<g/>
pozor	pozor	k1gInSc1	pozor
na	na	k7c4	na
záměnu	záměna	k1gFnSc4	záměna
s	s	k7c7	s
titánem	titán	k1gMnSc7	titán
Atlantem	Atlant	k1gMnSc7	Atlant
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
něhož	jenž	k3xRgMnSc2	jenž
byl	být	k5eAaImAgInS	být
pojmenován	pojmenován	k2eAgInSc1d1	pojmenován
Atlantský	atlantský	k2eAgInSc1d1	atlantský
oceán	oceán	k1gInSc1	oceán
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poseidón	Poseidón	k1gMnSc1	Poseidón
měl	mít	k5eAaImAgMnS	mít
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
manželkou	manželka	k1gFnSc7	manželka
Kleitó	Kleitó	k1gFnSc1	Kleitó
celkem	celkem	k6eAd1	celkem
deset	deset	k4xCc1	deset
synů	syn	k1gMnPc2	syn
(	(	kIx(	(
<g/>
patery	patera	k1gFnPc1	patera
dvojčata	dvojče	k1gNnPc1	dvojče
<g/>
)	)	kIx)	)
a	a	k8xC	a
Atlantida	Atlantida	k1gFnSc1	Atlantida
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
administrativně	administrativně	k6eAd1	administrativně
dělila	dělit	k5eAaImAgFnS	dělit
do	do	k7c2	do
deseti	deset	k4xCc2	deset
provincií	provincie	k1gFnPc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Atlantida	Atlantida	k1gFnSc1	Atlantida
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
velice	velice	k6eAd1	velice
úrodná	úrodný	k2eAgFnSc1d1	úrodná
a	a	k8xC	a
hustě	hustě	k6eAd1	hustě
zalidněná	zalidněný	k2eAgFnSc1d1	zalidněná
země	země	k1gFnSc1	země
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
měřilo	měřit	k5eAaImAgNnS	měřit
na	na	k7c6	na
obvodu	obvod	k1gInSc6	obvod
3000	[number]	k4	3000
krát	krát	k6eAd1	krát
2000	[number]	k4	2000
stadií	stadion	k1gNnPc2	stadion
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
centrálního	centrální	k2eAgInSc2d1	centrální
chrámového	chrámový	k2eAgInSc2d1	chrámový
pahorku	pahorek	k1gInSc2	pahorek
byly	být	k5eAaImAgInP	být
vystavěny	vystavěn	k2eAgInPc1d1	vystavěn
tři	tři	k4xCgInPc1	tři
soustředné	soustředný	k2eAgInPc1d1	soustředný
kruhové	kruhový	k2eAgInPc1d1	kruhový
vodní	vodní	k2eAgInPc1d1	vodní
kanály	kanál	k1gInPc1	kanál
spojené	spojený	k2eAgInPc1d1	spojený
s	s	k7c7	s
mořem	moře	k1gNnSc7	moře
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
sloužily	sloužit	k5eAaImAgInP	sloužit
jako	jako	k8xC	jako
přístav	přístav	k1gInSc1	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Duchovním	duchovní	k2eAgNnSc7d1	duchovní
centrem	centrum	k1gNnSc7	centrum
Atlantidy	Atlantida	k1gFnSc2	Atlantida
byl	být	k5eAaImAgInS	být
Poseidónův	Poseidónův	k2eAgInSc1d1	Poseidónův
chrám	chrám	k1gInSc1	chrám
uprostřed	uprostřed	k7c2	uprostřed
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
stavební	stavební	k2eAgInSc1d1	stavební
materiál	materiál	k1gInSc1	materiál
sloužil	sloužit	k5eAaImAgInS	sloužit
přírodní	přírodní	k2eAgInSc4d1	přírodní
kámen	kámen	k1gInSc4	kámen
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
vyskytoval	vyskytovat	k5eAaImAgMnS	vyskytovat
v	v	k7c6	v
bílé	bílý	k2eAgFnSc6d1	bílá
<g/>
,	,	kIx,	,
černé	černý	k2eAgFnSc3d1	černá
a	a	k8xC	a
červené	červený	k2eAgFnSc3d1	červená
barvě	barva	k1gFnSc3	barva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Atlantidě	Atlantida	k1gFnSc6	Atlantida
byly	být	k5eAaImAgInP	být
dva	dva	k4xCgInPc1	dva
prameny	pramen	k1gInPc1	pramen
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
s	s	k7c7	s
horkou	horka	k1gFnSc7	horka
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
se	se	k3xPyFc4	se
studenou	studený	k2eAgFnSc7d1	studená
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
zřídit	zřídit	k5eAaPmF	zřídit
pro	pro	k7c4	pro
všechny	všechen	k3xTgFnPc4	všechen
vrstvy	vrstva	k1gFnPc4	vrstva
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
početné	početný	k2eAgFnPc4d1	početná
lázně	lázeň	k1gFnPc4	lázeň
<g/>
.	.	kIx.	.
</s>
<s>
Vzkvétal	vzkvétat	k5eAaImAgInS	vzkvétat
také	také	k9	také
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
vyspělými	vyspělý	k2eAgFnPc7d1	vyspělá
civilizacemi	civilizace	k1gFnPc7	civilizace
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Platón	Platón	k1gMnSc1	Platón
se	se	k3xPyFc4	se
také	také	k9	také
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
o	o	k7c6	o
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
propukla	propuknout	k5eAaPmAgFnS	propuknout
mezi	mezi	k7c7	mezi
zeměmi	zem	k1gFnPc7	zem
před	před	k7c7	před
a	a	k8xC	a
za	za	k7c7	za
Héraklovými	Héraklův	k2eAgInPc7d1	Héraklův
sloupy	sloup	k1gInPc7	sloup
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
Atlantida	Atlantida	k1gFnSc1	Atlantida
bojovala	bojovat	k5eAaImAgFnS	bojovat
proti	proti	k7c3	proti
Egyptu	Egypt	k1gInSc3	Egypt
<g/>
,	,	kIx,	,
pra-Athénám	pra-Athéna	k1gFnPc3	pra-Athéna
a	a	k8xC	a
ostatním	ostatní	k2eAgInPc3d1	ostatní
státům	stát	k1gInPc3	stát
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
začátkem	začátek	k1gInSc7	začátek
této	tento	k3xDgFnSc2	tento
války	válka	k1gFnSc2	válka
si	se	k3xPyFc3	se
Atlanťané	Atlanťan	k1gMnPc1	Atlanťan
podmanili	podmanit	k5eAaPmAgMnP	podmanit
velkou	velký	k2eAgFnSc4d1	velká
část	část	k1gFnSc4	část
Středomoří	středomoří	k1gNnPc2	středomoří
až	až	k9	až
k	k	k7c3	k
Egyptu	Egypt	k1gInSc3	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Mnohem	mnohem	k6eAd1	mnohem
menším	menšit	k5eAaImIp1nS	menšit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
početnějším	početní	k2eAgFnPc3d2	početnější
pra-Athénám	pra-Athéna	k1gFnPc3	pra-Athéna
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
Atlanťany	Atlanťan	k1gMnPc4	Atlanťan
porazit	porazit	k5eAaPmF	porazit
a	a	k8xC	a
osvobodit	osvobodit	k5eAaPmF	osvobodit
země	zem	k1gFnSc2	zem
až	až	k9	až
k	k	k7c3	k
Herkulovým	Herkulův	k2eAgInPc3d1	Herkulův
sloupům	sloup	k1gInPc3	sloup
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
<g/>
,	,	kIx,	,
po	po	k7c6	po
mohutném	mohutný	k2eAgNnSc6d1	mohutné
zemětřesení	zemětřesení	k1gNnSc6	zemětřesení
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
celý	celý	k2eAgInSc1d1	celý
atlantský	atlantský	k2eAgInSc1d1	atlantský
kontinent	kontinent	k1gInSc1	kontinent
potopil	potopit	k5eAaPmAgInS	potopit
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Atlantida	Atlantida	k1gFnSc1	Atlantida
se	se	k3xPyFc4	se
měla	mít	k5eAaImAgFnS	mít
nacházet	nacházet	k5eAaImF	nacházet
ve	v	k7c6	v
vnějším	vnější	k2eAgInSc6d1	vnější
oceánu	oceán	k1gInSc6	oceán
za	za	k7c7	za
Herkulovými	Herkulův	k2eAgInPc7d1	Herkulův
sloupy	sloup	k1gInPc7	sloup
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgInSc1d1	dnešní
Gibraltarský	gibraltarský	k2eAgInSc1d1	gibraltarský
průliv	průliv	k1gInSc1	průliv
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Řekové	Řek	k1gMnPc1	Řek
Atlantský	atlantský	k2eAgInSc4d1	atlantský
oceán	oceán	k1gInSc4	oceán
považovali	považovat	k5eAaImAgMnP	považovat
za	za	k7c4	za
světový	světový	k2eAgInSc4d1	světový
oceán	oceán	k1gInSc4	oceán
obepínající	obepínající	k2eAgInPc4d1	obepínající
tři	tři	k4xCgInPc4	tři
kontinenty	kontinent	k1gInPc4	kontinent
-	-	kIx~	-
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
Afriku	Afrika	k1gFnSc4	Afrika
a	a	k8xC	a
Asii	Asie	k1gFnSc4	Asie
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
definice	definice	k1gFnSc2	definice
tedy	tedy	k8xC	tedy
Atlantida	Atlantida	k1gFnSc1	Atlantida
mohla	moct	k5eAaImAgFnS	moct
ležet	ležet	k5eAaImF	ležet
kdekoliv	kdekoliv	k6eAd1	kdekoliv
mimo	mimo	k7c4	mimo
Středozemní	středozemní	k2eAgNnSc4d1	středozemní
moře	moře	k1gNnSc4	moře
<g/>
,	,	kIx,	,
od	od	k7c2	od
Švédska	Švédsko	k1gNnSc2	Švédsko
po	po	k7c6	po
Indii	Indie	k1gFnSc6	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Hypotéz	hypotéza	k1gFnPc2	hypotéza
o	o	k7c6	o
možné	možný	k2eAgFnSc6d1	možná
poloze	poloha	k1gFnSc6	poloha
Atlantidy	Atlantida	k1gFnSc2	Atlantida
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
nepřeberné	přeberný	k2eNgNnSc1d1	nepřeberné
množství	množství	k1gNnSc1	množství
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
badatelé	badatel	k1gMnPc1	badatel
se	se	k3xPyFc4	se
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Solón	Solón	k1gMnSc1	Solón
mohl	moct	k5eAaImAgMnS	moct
při	při	k7c6	při
překladu	překlad	k1gInSc6	překlad
egyptských	egyptský	k2eAgInPc2d1	egyptský
rukopisů	rukopis	k1gInPc2	rukopis
zaměnit	zaměnit	k5eAaPmF	zaměnit
číslovku	číslovka	k1gFnSc4	číslovka
"	"	kIx"	"
<g/>
1000	[number]	k4	1000
<g/>
"	"	kIx"	"
za	za	k7c2	za
"	"	kIx"	"
<g/>
100	[number]	k4	100
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
by	by	kYmCp3nS	by
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Atlantida	Atlantida	k1gFnSc1	Atlantida
zanikla	zaniknout	k5eAaPmAgFnS	zaniknout
v	v	k7c6	v
období	období	k1gNnSc6	období
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1500	[number]	k4	1500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
a	a	k8xC	a
měla	mít	k5eAaImAgFnS	mít
by	by	k9	by
10	[number]	k4	10
<g/>
×	×	k?	×
menší	malý	k2eAgInPc4d2	menší
rozměry	rozměr	k1gInPc4	rozměr
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
badatelé	badatel	k1gMnPc1	badatel
(	(	kIx(	(
<g/>
James	James	k1gMnSc1	James
Mavor	Mavor	k1gMnSc1	Mavor
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
Baikie	Baikie	k1gFnSc2	Baikie
aj.	aj.	kA	aj.
<g/>
)	)	kIx)	)
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zánik	zánik	k1gInSc1	zánik
mínojské	mínojský	k2eAgFnSc2d1	mínojská
civilizace	civilizace	k1gFnSc2	civilizace
je	být	k5eAaImIp3nS	být
identický	identický	k2eAgMnSc1d1	identický
se	s	k7c7	s
zánikem	zánik	k1gInSc7	zánik
Atlantidy	Atlantida	k1gFnSc2	Atlantida
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Krétě	Kréta	k1gFnSc6	Kréta
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
našly	najít	k5eAaPmAgFnP	najít
mnohé	mnohý	k2eAgInPc4d1	mnohý
reliéfy	reliéf	k1gInPc4	reliéf
a	a	k8xC	a
nástěnné	nástěnný	k2eAgFnPc4d1	nástěnná
malby	malba	k1gFnPc4	malba
znázorňující	znázorňující	k2eAgInSc4d1	znázorňující
lov	lov	k1gInSc4	lov
býků	býk	k1gMnPc2	býk
a	a	k8xC	a
býčí	býčí	k2eAgInPc1d1	býčí
zápasy	zápas	k1gInPc1	zápas
<g/>
,	,	kIx,	,
býčí	býčí	k2eAgInSc1d1	býčí
kult	kult	k1gInSc1	kult
byl	být	k5eAaImAgInS	být
podle	podle	k7c2	podle
Platóna	Platón	k1gMnSc2	Platón
také	také	k6eAd1	také
vyznáván	vyznáván	k2eAgInSc1d1	vyznáván
na	na	k7c6	na
Atlantidě	Atlantida	k1gFnSc6	Atlantida
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nejnovějších	nový	k2eAgInPc2d3	nejnovější
výzkumů	výzkum	k1gInPc2	výzkum
však	však	k9	však
mínojská	mínojský	k2eAgFnSc1d1	mínojská
civilizace	civilizace	k1gFnSc1	civilizace
neskončila	skončit	k5eNaPmAgFnS	skončit
kvůli	kvůli	k7c3	kvůli
výbuchu	výbuch	k1gInSc3	výbuch
sopky	sopka	k1gFnSc2	sopka
na	na	k7c6	na
vulkanickém	vulkanický	k2eAgInSc6d1	vulkanický
ostrově	ostrov	k1gInSc6	ostrov
Thera	Thero	k1gNnSc2	Thero
blízko	blízko	k7c2	blízko
Kréty	Kréta	k1gFnSc2	Kréta
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
se	se	k3xPyFc4	se
dříve	dříve	k6eAd2	dříve
předpokládalo	předpokládat	k5eAaImAgNnS	předpokládat
(	(	kIx(	(
<g/>
podobnost	podobnost	k1gFnSc1	podobnost
se	s	k7c7	s
zánikem	zánik	k1gInSc7	zánik
Atlantidy	Atlantida	k1gFnSc2	Atlantida
tak	tak	k6eAd1	tak
odpadá	odpadat	k5eAaImIp3nS	odpadat
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
existovala	existovat	k5eAaImAgFnS	existovat
ještě	ještě	k9	ještě
přibližně	přibližně	k6eAd1	přibližně
200	[number]	k4	200
let	léto	k1gNnPc2	léto
po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
katastrofě	katastrofa	k1gFnSc6	katastrofa
<g/>
.	.	kIx.	.
</s>
<s>
Kréta	Kréta	k1gFnSc1	Kréta
se	se	k3xPyFc4	se
navíc	navíc	k6eAd1	navíc
samozřejmě	samozřejmě	k6eAd1	samozřejmě
nenachází	nacházet	k5eNaImIp3nS	nacházet
v	v	k7c6	v
oceánu	oceán	k1gInSc6	oceán
za	za	k7c7	za
Gibraltarským	gibraltarský	k2eAgInSc7d1	gibraltarský
průlivem	průliv	k1gInSc7	průliv
<g/>
.	.	kIx.	.
</s>
<s>
Jiní	jiný	k2eAgMnPc1d1	jiný
autoři	autor	k1gMnPc1	autor
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Eberhard	Eberhard	k1gMnSc1	Eberhard
Zangger	Zangger	k1gMnSc1	Zangger
<g/>
)	)	kIx)	)
předpokládají	předpokládat	k5eAaImIp3nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
celá	celý	k2eAgFnSc1d1	celá
historie	historie	k1gFnSc1	historie
Atlantidy	Atlantida	k1gFnSc2	Atlantida
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
egyptskou	egyptský	k2eAgFnSc7d1	egyptská
verzí	verze	k1gFnSc7	verze
Iliady	Iliada	k1gFnSc2	Iliada
zpětně	zpětně	k6eAd1	zpětně
přenesenou	přenesený	k2eAgFnSc4d1	přenesená
do	do	k7c2	do
Řecka	Řecko	k1gNnSc2	Řecko
Solónem	Solón	k1gMnSc7	Solón
<g/>
.	.	kIx.	.
</s>
<s>
Herkulovy	Herkulův	k2eAgInPc1d1	Herkulův
sloupy	sloup	k1gInPc1	sloup
by	by	kYmCp3nP	by
tedy	tedy	k9	tedy
znamenaly	znamenat	k5eAaImAgFnP	znamenat
průliv	průliv	k1gInSc4	průliv
Dardanely	Dardanely	k1gFnPc4	Dardanely
<g/>
.	.	kIx.	.
</s>
<s>
Atlantida	Atlantida	k1gFnSc1	Atlantida
měla	mít	k5eAaImAgFnS	mít
ležet	ležet	k5eAaImF	ležet
za	za	k7c7	za
Herkulovými	Herkulův	k2eAgInPc7d1	Herkulův
sloupy	sloup	k1gInPc7	sloup
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Trója	Trója	k1gFnSc1	Trója
z	z	k7c2	z
egyptského	egyptský	k2eAgInSc2d1	egyptský
pohledu	pohled	k1gInSc2	pohled
leží	ležet	k5eAaImIp3nS	ležet
před	před	k7c7	před
Dardanelami	Dardanely	k1gFnPc7	Dardanely
<g/>
.	.	kIx.	.
</s>
<s>
Trója	Trója	k1gFnSc1	Trója
také	také	k6eAd1	také
nezanikla	zaniknout	k5eNaPmAgFnS	zaniknout
zemětřesením	zemětřesení	k1gNnSc7	zemětřesení
a	a	k8xC	a
následným	následný	k2eAgNnSc7d1	následné
zatopením	zatopení	k1gNnSc7	zatopení
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
teorie	teorie	k1gFnSc2	teorie
atlantská	atlantský	k2eAgFnSc1d1	Atlantská
civilizace	civilizace	k1gFnSc1	civilizace
existovala	existovat	k5eAaImAgFnS	existovat
přibližně	přibližně	k6eAd1	přibližně
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
5500	[number]	k4	5500
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
na	na	k7c6	na
severozápadním	severozápadní	k2eAgNnSc6d1	severozápadní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
ještě	ještě	k9	ještě
sladkovodního	sladkovodní	k2eAgNnSc2d1	sladkovodní
jezera	jezero	k1gNnSc2	jezero
<g/>
)	)	kIx)	)
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
při	při	k7c6	při
protržení	protržení	k1gNnSc6	protržení
přírodní	přírodní	k2eAgFnSc2d1	přírodní
hráze	hráz	k1gFnSc2	hráz
v	v	k7c6	v
místě	místo	k1gNnSc6	místo
dnešního	dnešní	k2eAgInSc2d1	dnešní
Istanbulu	Istanbul	k1gInSc2	Istanbul
a	a	k8xC	a
následné	následný	k2eAgFnSc6d1	následná
potopě	potopa	k1gFnSc6	potopa
a	a	k8xC	a
propojení	propojení	k1gNnSc6	propojení
Černého	černé	k1gNnSc2	černé
a	a	k8xC	a
Středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Herkulovy	Herkulův	k2eAgInPc1d1	Herkulův
sloupy	sloup	k1gInPc1	sloup
by	by	kYmCp3nP	by
tedy	tedy	k9	tedy
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
znamenaly	znamenat	k5eAaImAgFnP	znamenat
Bosporský	bosporský	k2eAgInSc4d1	bosporský
průliv	průliv	k1gInSc4	průliv
<g/>
.	.	kIx.	.
</s>
<s>
Skupina	skupina	k1gFnSc1	skupina
francouzských	francouzský	k2eAgMnPc2d1	francouzský
vědců	vědec	k1gMnPc2	vědec
se	se	k3xPyFc4	se
domnívá	domnívat	k5eAaImIp3nS	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Atlantida	Atlantida	k1gFnSc1	Atlantida
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
nacházet	nacházet	k5eAaImF	nacházet
na	na	k7c6	na
zatopeném	zatopený	k2eAgInSc6d1	zatopený
ostrově	ostrov	k1gInSc6	ostrov
Spartel	Spartel	k1gInSc1	Spartel
v	v	k7c6	v
Cádizském	cádizský	k2eAgInSc6d1	cádizský
zálivu	záliv	k1gInSc6	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jejich	jejich	k3xOp3gFnPc2	jejich
analýz	analýza	k1gFnPc2	analýza
mořského	mořský	k2eAgNnSc2d1	mořské
dna	dno	k1gNnSc2	dno
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
Spartel	Spartel	k1gInSc1	Spartel
potopil	potopit	k5eAaPmAgInS	potopit
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
mnoha	mnoho	k4c2	mnoho
zemětřesení	zemětřesení	k1gNnPc2	zemětřesení
a	a	k8xC	a
vln	vlna	k1gFnPc2	vlna
tsunami	tsunami	k1gNnSc4	tsunami
<g/>
.	.	kIx.	.
</s>
<s>
Italský	italský	k2eAgMnSc1d1	italský
novinář	novinář	k1gMnSc1	novinář
Sergio	Sergio	k1gMnSc1	Sergio
Frau	Fraa	k1gFnSc4	Fraa
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
článku	článek	k1gInSc6	článek
"	"	kIx"	"
<g/>
Le	Le	k1gFnSc1	Le
colonne	colonnout	k5eAaPmIp3nS	colonnout
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Ercole	Ercole	k1gFnSc1	Ercole
<g/>
"	"	kIx"	"
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
době	doba	k1gFnSc6	doba
před	před	k7c7	před
Alexandrem	Alexandr	k1gMnSc7	Alexandr
Velikým	veliký	k2eAgMnSc7d1	veliký
znamenaly	znamenat	k5eAaImAgFnP	znamenat
Herkulovy	Herkulův	k2eAgInPc4d1	Herkulův
sloupy	sloup	k1gInPc4	sloup
Sicilský	sicilský	k2eAgInSc4d1	sicilský
průliv	průliv	k1gInSc4	průliv
<g/>
.	.	kIx.	.
</s>
<s>
Atlantida	Atlantida	k1gFnSc1	Atlantida
by	by	kYmCp3nS	by
tedy	tedy	k9	tedy
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
totožná	totožný	k2eAgFnSc1d1	totožná
se	s	k7c7	s
Sardinií	Sardinie	k1gFnSc7	Sardinie
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
postihla	postihnout	k5eAaPmAgFnS	postihnout
přírodní	přírodní	k2eAgFnSc1d1	přírodní
katastrofa	katastrofa	k1gFnSc1	katastrofa
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
část	část	k1gFnSc1	část
uprchlých	uprchlá	k1gFnPc2	uprchlá
obyvatel	obyvatel	k1gMnPc2	obyvatel
následně	následně	k6eAd1	následně
založila	založit	k5eAaPmAgFnS	založit
etruskou	etruský	k2eAgFnSc4d1	etruská
civilizaci	civilizace	k1gFnSc4	civilizace
na	na	k7c6	na
Apeninském	apeninský	k2eAgInSc6d1	apeninský
poloostrově	poloostrov	k1gInSc6	poloostrov
a	a	k8xC	a
zbytek	zbytek	k1gInSc1	zbytek
přeživších	přeživší	k2eAgMnPc2d1	přeživší
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
připojil	připojit	k5eAaPmAgInS	připojit
k	k	k7c3	k
ostatním	ostatní	k2eAgInPc3d1	ostatní
mořským	mořský	k2eAgInPc3d1	mořský
národům	národ	k1gInPc3	národ
při	při	k7c6	při
útoku	útok	k1gInSc6	útok
na	na	k7c4	na
Egypt	Egypt	k1gInSc4	Egypt
(	(	kIx(	(
<g/>
výrazná	výrazný	k2eAgFnSc1d1	výrazná
podobnost	podobnost	k1gFnSc1	podobnost
s	s	k7c7	s
popisovanou	popisovaný	k2eAgFnSc7d1	popisovaná
válkou	válka	k1gFnSc7	válka
o	o	k7c6	o
Středomoří	středomoří	k1gNnSc6	středomoří
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
žhavých	žhavý	k2eAgMnPc2d1	žhavý
kandidátů	kandidát	k1gMnPc2	kandidát
je	být	k5eAaImIp3nS	být
ostrov	ostrov	k1gInSc4	ostrov
Santorini	Santorin	k2eAgMnPc1d1	Santorin
patřící	patřící	k2eAgMnPc1d1	patřící
k	k	k7c3	k
nejjižnějším	jižní	k2eAgMnPc3d3	nejjižnější
z	z	k7c2	z
Kykladských	kykladský	k2eAgInPc2d1	kykladský
ostrovů	ostrov	k1gInPc2	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Výbuch	výbuch	k1gInSc1	výbuch
podmořské	podmořský	k2eAgFnSc2d1	podmořská
sopky	sopka	k1gFnSc2	sopka
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterému	který	k3yQgNnSc3	který
došlo	dojít	k5eAaPmAgNnS	dojít
podle	podle	k7c2	podle
nejnovějších	nový	k2eAgInPc2d3	nejnovější
výzkumů	výzkum	k1gInPc2	výzkum
před	před	k7c7	před
3	[number]	k4	3
600	[number]	k4	600
lety	léto	k1gNnPc7	léto
<g/>
,	,	kIx,	,
způsobil	způsobit	k5eAaPmAgInS	způsobit
<g/>
,	,	kIx,	,
že	že	k8xS	že
část	část	k1gFnSc1	část
ostrova	ostrov	k1gInSc2	ostrov
se	se	k3xPyFc4	se
doslova	doslova	k6eAd1	doslova
propadla	propadnout	k5eAaPmAgFnS	propadnout
do	do	k7c2	do
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Nánosy	nános	k1gInPc1	nános
po	po	k7c6	po
vlně	vlna	k1gFnSc6	vlna
tsunami	tsunami	k1gNnPc2	tsunami
byly	být	k5eAaImAgFnP	být
nalezeny	nalézt	k5eAaBmNgFnP	nalézt
až	až	k9	až
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Vykopávky	vykopávka	k1gFnSc2	vykopávka
odhalily	odhalit	k5eAaPmAgInP	odhalit
osídlení	osídlení	k1gNnSc4	osídlení
již	již	k9	již
z	z	k7c2	z
pozdního	pozdní	k2eAgInSc2d1	pozdní
neolitu	neolit	k1gInSc2	neolit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
exploze	exploze	k1gFnSc2	exploze
tu	tu	k6eAd1	tu
stálo	stát	k5eAaImAgNnS	stát
prosperující	prosperující	k2eAgNnSc1d1	prosperující
město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Pompeje	Pompeje	k1gInPc1	Pompeje
zachovalo	zachovat	k5eAaPmAgNnS	zachovat
pod	pod	k7c7	pod
příkrovem	příkrov	k1gInSc7	příkrov
lávy	láva	k1gFnSc2	láva
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
nebyly	být	k5eNaImAgInP	být
nalezeny	naleznout	k5eAaPmNgInP	naleznout
žádné	žádný	k3yNgInPc1	žádný
lidské	lidský	k2eAgInPc1d1	lidský
ostatky	ostatek	k1gInPc1	ostatek
ani	ani	k8xC	ani
předměty	předmět	k1gInPc1	předmět
z	z	k7c2	z
drahých	drahý	k2eAgInPc2d1	drahý
kovů	kov	k1gInPc2	kov
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
úspěšné	úspěšný	k2eAgFnSc6d1	úspěšná
evakuaci	evakuace	k1gFnSc6	evakuace
<g/>
.	.	kIx.	.
</s>
<s>
Archeologové	archeolog	k1gMnPc1	archeolog
pokládají	pokládat	k5eAaImIp3nP	pokládat
erupci	erupce	k1gFnSc4	erupce
na	na	k7c6	na
Théře	Théra	k1gFnSc6	Théra
za	za	k7c4	za
příčinu	příčina	k1gFnSc4	příčina
náhlého	náhlý	k2eAgInSc2d1	náhlý
zániku	zánik	k1gInSc2	zánik
mínojské	mínojský	k2eAgFnSc2d1	mínojská
civilizace	civilizace	k1gFnSc2	civilizace
a	a	k8xC	a
nevylučují	vylučovat	k5eNaImIp3nP	vylučovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
byla	být	k5eAaImAgFnS	být
inspirací	inspirace	k1gFnSc7	inspirace
bájí	báj	k1gFnPc2	báj
o	o	k7c6	o
Atlantidě	Atlantida	k1gFnSc6	Atlantida
a	a	k8xC	a
nejen	nejen	k6eAd1	nejen
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
možná	možný	k2eAgFnSc1d1	možná
poloha	poloha	k1gFnSc1	poloha
bájného	bájný	k2eAgNnSc2d1	bájné
města	město	k1gNnSc2	město
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
v	v	k7c6	v
oblastni	oblastnout	k5eAaPmRp2nS	oblastnout
tzv.	tzv.	kA	tzv.
Bermudského	bermudský	k2eAgInSc2d1	bermudský
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
nedaleko	nedaleko	k7c2	nedaleko
Kuby	Kuba	k1gFnSc2	Kuba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
objevili	objevit	k5eAaPmAgMnP	objevit
manželé	manžel	k1gMnPc1	manžel
Paul	Paul	k1gMnSc1	Paul
Weinzweig	Weinzweig	k1gMnSc1	Weinzweig
a	a	k8xC	a
Pauline	Paulin	k1gInSc5	Paulin
Zalitzky	Zalitzek	k1gInPc4	Zalitzek
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
týmem	tým	k1gInSc7	tým
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
moře	moře	k1gNnSc2	moře
obrovské	obrovský	k2eAgFnPc1d1	obrovská
trosky	troska	k1gFnPc1	troska
mrtvého	mrtvý	k2eAgNnSc2d1	mrtvé
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Zjistili	zjistit	k5eAaPmAgMnP	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
dně	dno	k1gNnSc6	dno
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
několik	několik	k4yIc1	několik
sfing	sfinga	k1gFnPc2	sfinga
<g/>
,	,	kIx,	,
pyramid	pyramida	k1gFnPc2	pyramida
<g/>
,	,	kIx,	,
dlážděných	dlážděný	k2eAgFnPc2d1	dlážděná
ulic	ulice	k1gFnPc2	ulice
a	a	k8xC	a
zbytků	zbytek	k1gInPc2	zbytek
domů	domů	k6eAd1	domů
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
vyrytými	vyrytý	k2eAgInPc7d1	vyrytý
nápisy	nápis	k1gInPc7	nápis
na	na	k7c6	na
zdech	zeď	k1gFnPc6	zeď
<g/>
.	.	kIx.	.
</s>
<s>
Vědecká	vědecký	k2eAgFnSc1d1	vědecká
obec	obec	k1gFnSc1	obec
však	však	k9	však
o	o	k7c6	o
jejich	jejich	k3xOp3gInSc6	jejich
nálezu	nález	k1gInSc6	nález
pochybuje	pochybovat	k5eAaImIp3nS	pochybovat
a	a	k8xC	a
kamerovým	kamerový	k2eAgInPc3d1	kamerový
záznamům	záznam	k1gInPc3	záznam
příliš	příliš	k6eAd1	příliš
nevěří	věřit	k5eNaImIp3nS	věřit
<g/>
.	.	kIx.	.
</s>
<s>
Manželé	manžel	k1gMnPc1	manžel
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
o	o	k7c6	o
svém	svůj	k3xOyFgInSc6	svůj
objevu	objev	k1gInSc6	objev
starověkého	starověký	k2eAgNnSc2d1	starověké
města	město	k1gNnSc2	město
přesvědčeni	přesvědčit	k5eAaPmNgMnP	přesvědčit
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
uvažované	uvažovaný	k2eAgFnPc4d1	uvažovaná
lokality	lokalita	k1gFnPc4	lokalita
patří	patřit	k5eAaImIp3nS	patřit
ve	v	k7c6	v
Středomoří	středomoří	k1gNnSc6	středomoří
Malta	Malta	k1gFnSc1	Malta
(	(	kIx(	(
<g/>
zde	zde	k6eAd1	zde
na	na	k7c6	na
základě	základ	k1gInSc6	základ
rýh	rýha	k1gFnPc2	rýha
u	u	k7c2	u
Clapham	Clapham	k1gInSc4	Clapham
Junction	Junction	k1gInSc1	Junction
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kypr	Kypr	k1gInSc1	Kypr
<g/>
;	;	kIx,	;
v	v	k7c6	v
Atlantském	atlantský	k2eAgInSc6d1	atlantský
oceánu	oceán	k1gInSc6	oceán
pak	pak	k6eAd1	pak
například	například	k6eAd1	například
Irsko	Irsko	k1gNnSc1	Irsko
<g/>
,	,	kIx,	,
ostrovy	ostrov	k1gInPc1	ostrov
Karibiku	Karibik	k1gInSc2	Karibik
nebo	nebo	k8xC	nebo
Azorské	azorský	k2eAgInPc4d1	azorský
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Legendární	legendární	k2eAgFnSc1d1	legendární
Atlantida	Atlantida	k1gFnSc1	Atlantida
nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
inspirovala	inspirovat	k5eAaBmAgFnS	inspirovat
jméno	jméno	k1gNnSc4	jméno
pro	pro	k7c4	pro
vulkanickou	vulkanický	k2eAgFnSc4d1	vulkanická
podmořskou	podmořský	k2eAgFnSc4d1	podmořská
horu	hora	k1gFnSc4	hora
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Azorských	azorský	k2eAgInPc2d1	azorský
ostrovů	ostrov	k1gInPc2	ostrov
s	s	k7c7	s
anglickým	anglický	k2eAgInSc7d1	anglický
názvem	název	k1gInSc7	název
Atlantis	Atlantis	k1gFnSc1	Atlantis
Seamount	Seamount	k1gMnSc1	Seamount
(	(	kIx(	(
<g/>
podmořáská	podmořáská	k1gFnSc1	podmořáská
hora	hora	k1gFnSc1	hora
Atlantis	Atlantis	k1gFnSc1	Atlantis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rusky	rusky	k6eAd1	rusky
Г	Г	k?	Г
А	А	k?	А
(	(	kIx(	(
<g/>
Gora	Gor	k2eAgFnSc1d1	Gora
Atlantis	Atlantis	k1gFnSc1	Atlantis
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
samotný	samotný	k2eAgMnSc1d1	samotný
autor	autor	k1gMnSc1	autor
báje	báj	k1gFnSc2	báj
o	o	k7c6	o
Atlantidě	Atlantida	k1gFnSc6	Atlantida
se	se	k3xPyFc4	se
dočkal	dočkat	k5eAaPmAgInS	dočkat
zvěčnění	zvěčnění	k1gNnSc1	zvěčnění
pojmenováním	pojmenování	k1gNnSc7	pojmenování
další	další	k2eAgFnSc2d1	další
podmořské	podmořský	k2eAgFnSc2d1	podmořská
hory	hora	k1gFnSc2	hora
sopečného	sopečný	k2eAgInSc2d1	sopečný
původu	původ	k1gInSc2	původ
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Azor	Azor	k1gMnSc1	Azor
-	-	kIx~	-
Plato	Plato	k1gMnSc1	Plato
Seamount	Seamount	k1gMnSc1	Seamount
(	(	kIx(	(
<g/>
Platónova	Platónův	k2eAgFnSc1d1	Platónova
podmořská	podmořský	k2eAgFnSc1d1	podmořská
hora	hora	k1gFnSc1	hora
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
pozornost	pozornost	k1gFnSc1	pozornost
si	se	k3xPyFc3	se
zaslouží	zasloužit	k5eAaPmIp3nS	zasloužit
Richatská	Richatský	k2eAgFnSc1d1	Richatský
Struktura	struktura	k1gFnSc1	struktura
v	v	k7c6	v
Mauritánii	Mauritánie	k1gFnSc6	Mauritánie
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgFnPc2d1	ostatní
lokalit	lokalita	k1gFnPc2	lokalita
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
rozměrům	rozměr	k1gInPc3	rozměr
města	město	k1gNnSc2	město
popisovaném	popisovaný	k2eAgInSc6d1	popisovaný
Platónem	platón	k1gInSc7	platón
(	(	kIx(	(
<g/>
Jekaterina	Jekaterin	k2eAgMnSc2d1	Jekaterin
Andrejeva	Andrejev	k1gMnSc2	Andrejev
<g/>
:	:	kIx,	:
ATLANTIDA	Atlantida	k1gFnSc1	Atlantida
hledání	hledání	k1gNnSc2	hledání
ztraceného	ztracený	k2eAgInSc2d1	ztracený
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
33	[number]	k4	33
<g/>
-	-	kIx~	-
<g/>
203	[number]	k4	203
<g/>
-	-	kIx~	-
<g/>
66	[number]	k4	66
<g/>
,	,	kIx,	,
str	str	kA	str
<g/>
.11	.11	k4	.11
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
GoogleEarth	GoogleEarth	k1gInSc1	GoogleEarth
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Manželé	manžel	k1gMnPc1	manžel
Rand	rand	k1gInSc4	rand
a	a	k8xC	a
Rose	Rose	k1gMnSc1	Rose
Flem-Athovi	Flem-Ath	k1gMnSc3	Flem-Ath
se	se	k3xPyFc4	se
na	na	k7c6	na
základě	základ	k1gInSc6	základ
prací	práce	k1gFnPc2	práce
profesora	profesor	k1gMnSc2	profesor
Charlese	Charles	k1gMnSc2	Charles
Hapgooda	Hapgood	k1gMnSc2	Hapgood
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
teorii	teorie	k1gFnSc4	teorie
o	o	k7c6	o
posuvu	posuv	k1gInSc6	posuv
zemské	zemský	k2eAgFnSc2d1	zemská
kůry	kůra	k1gFnSc2	kůra
<g/>
,	,	kIx,	,
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Antarktida	Antarktida	k1gFnSc1	Antarktida
je	být	k5eAaImIp3nS	být
ztracená	ztracený	k2eAgFnSc1d1	ztracená
Atlantida	Atlantida	k1gFnSc1	Atlantida
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Platónova	Platónův	k2eAgNnSc2d1	Platónovo
vyprávění	vyprávění	k1gNnSc2	vyprávění
existovala	existovat	k5eAaImAgFnS	existovat
Atlantida	Atlantida	k1gFnSc1	Atlantida
9000	[number]	k4	9000
let	léto	k1gNnPc2	léto
před	před	k7c7	před
dobou	doba	k1gFnSc7	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
žil	žít	k5eAaImAgMnS	žít
Solón	Solón	k1gMnSc1	Solón
(	(	kIx(	(
<g/>
asi	asi	k9	asi
638	[number]	k4	638
-	-	kIx~	-
555	[number]	k4	555
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
přibližně	přibližně	k6eAd1	přibližně
9600	[number]	k4	9600
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vědeckých	vědecký	k2eAgInPc2d1	vědecký
poznatků	poznatek	k1gInPc2	poznatek
se	se	k3xPyFc4	se
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
už	už	k6eAd1	už
nezadržitelně	zadržitelně	k6eNd1	zadržitelně
blížil	blížit	k5eAaImAgInS	blížit
konec	konec	k1gInSc1	konec
poslední	poslední	k2eAgFnSc2d1	poslední
doby	doba	k1gFnSc2	doba
ledové	ledový	k2eAgFnSc2d1	ledová
(	(	kIx(	(
<g/>
glaciálu	glaciál	k1gInSc2	glaciál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
blížila	blížit	k5eAaImAgFnS	blížit
se	se	k3xPyFc4	se
současná	současný	k2eAgFnSc1d1	současná
doba	doba	k1gFnSc1	doba
meziledová	meziledový	k2eAgFnSc1d1	meziledová
(	(	kIx(	(
<g/>
interglaciál	interglaciál	k1gInSc1	interglaciál
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
kontinentální	kontinentální	k2eAgInPc1d1	kontinentální
ledovce	ledovec	k1gInPc1	ledovec
stále	stále	k6eAd1	stále
vázaly	vázat	k5eAaImAgFnP	vázat
obrovské	obrovský	k2eAgNnSc4d1	obrovské
množství	množství	k1gNnSc4	množství
vody	voda	k1gFnSc2	voda
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
ledu	led	k1gInSc2	led
a	a	k8xC	a
úrověň	úrověň	k1gFnSc4	úrověň
hladiny	hladina	k1gFnSc2	hladina
světového	světový	k2eAgInSc2d1	světový
oceánu	oceán	k1gInSc2	oceán
byla	být	k5eAaImAgFnS	být
<g />
.	.	kIx.	.
</s>
<s>
v	v	k7c6	v
době	doba	k1gFnSc6	doba
9600	[number]	k4	9600
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
o	o	k7c4	o
48	[number]	k4	48
metrů	metr	k1gInPc2	metr
níže	nízce	k6eAd2	nízce
<g/>
,	,	kIx,	,
než	než	k8xS	než
je	být	k5eAaImIp3nS	být
tomu	ten	k3xDgNnSc3	ten
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
Pro	pro	k7c4	pro
srovnání	srovnání	k1gNnSc4	srovnání
<g/>
,	,	kIx,	,
při	při	k7c6	při
posledním	poslední	k2eAgNnSc6d1	poslední
glaciálním	glaciální	k2eAgNnSc6d1	glaciální
maximu	maximum	k1gNnSc6	maximum
-	-	kIx~	-
vrcholu	vrchol	k1gInSc2	vrchol
doby	doba	k1gFnSc2	doba
ledové	ledový	k2eAgFnSc2d1	ledová
-	-	kIx~	-
v	v	k7c4	v
období	období	k1gNnSc4	období
přibližně	přibližně	k6eAd1	přibližně
22	[number]	k4	22
000	[number]	k4	000
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
až	až	k9	až
20	[number]	k4	20
000	[number]	k4	000
př	př	kA	př
<g />
.	.	kIx.	.
</s>
<s>
<g/>
n.	n.	k?	n.
l.	l.	k?	l.
byla	být	k5eAaImAgFnS	být
úrověň	úrověň	k1gFnSc4	úrověň
hladiny	hladina	k1gFnSc2	hladina
o	o	k7c4	o
135	[number]	k4	135
metrů	metr	k1gInPc2	metr
níže	nízce	k6eAd2	nízce
než	než	k8xS	než
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Pro	pro	k7c4	pro
ilustraci	ilustrace	k1gFnSc4	ilustrace
situace	situace	k1gFnSc2	situace
9600	[number]	k4	9600
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
:	:	kIx,	:
Z	z	k7c2	z
území	území	k1gNnSc2	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Anglie	Anglie	k1gFnSc2	Anglie
se	se	k3xPyFc4	se
dalo	dát	k5eAaPmAgNnS	dát
suchou	suchý	k2eAgFnSc7d1	suchá
nohou	noha	k1gFnSc7	noha
přejít	přejít	k5eAaPmF	přejít
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
Německa	Německo	k1gNnSc2	Německo
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
oblast	oblast	k1gFnSc1	oblast
geology	geolog	k1gMnPc4	geolog
pojmenovaná	pojmenovaný	k2eAgFnSc1d1	pojmenovaná
pracovně	pracovně	k6eAd1	pracovně
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Doggerland	Doggerland	k1gInSc1	Doggerland
<g/>
"	"	kIx"	"
byla	být	k5eAaImAgFnS	být
tehdy	tehdy	k6eAd1	tehdy
souší	souš	k1gFnSc7	souš
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
prakticky	prakticky	k6eAd1	prakticky
celou	celý	k2eAgFnSc4d1	celá
Skandinávii	Skandinávie	k1gFnSc4	Skandinávie
<g/>
,	,	kIx,	,
Island	Island	k1gInSc4	Island
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
větší	veliký	k2eAgFnSc1d2	veliký
část	část	k1gFnSc1	část
Kanady	Kanada	k1gFnSc2	Kanada
ještě	ještě	k9	ještě
pokrýval	pokrývat	k5eAaImAgInS	pokrývat
pevninský	pevninský	k2eAgInSc1d1	pevninský
ledovec	ledovec	k1gInSc1	ledovec
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
úrovní	úroveň	k1gFnSc7	úroveň
hladiny	hladina	k1gFnSc2	hladina
byl	být	k5eAaImAgInS	být
pevninský	pevninský	k2eAgInSc1d1	pevninský
most	most	k1gInSc1	most
mezi	mezi	k7c7	mezi
Indií	Indie	k1gFnSc7	Indie
a	a	k8xC	a
Srí	Srí	k1gFnSc7	Srí
Lankou	Lanký	k2eAgFnSc7d1	Lanký
<g/>
,	,	kIx,	,
suchou	suchý	k2eAgFnSc7d1	suchá
nohou	noha	k1gFnSc7	noha
bylo	být	k5eAaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
přejít	přejít	k5eAaPmF	přejít
ze	z	k7c2	z
severozápadního	severozápadní	k2eAgInSc2d1	severozápadní
cípu	cíp	k1gInSc2	cíp
Austrálie	Austrálie	k1gFnSc2	Austrálie
na	na	k7c4	na
Papuu	Papu	k2eAgFnSc4d1	Papua
Novou	nový	k2eAgFnSc4d1	nová
Guineu	Guinea	k1gFnSc4	Guinea
<g/>
,	,	kIx,	,
suché	suchý	k2eAgNnSc1d1	suché
spojení	spojení	k1gNnSc1	spojení
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
mezi	mezi	k7c7	mezi
Austrálií	Austrálie	k1gFnSc7	Austrálie
a	a	k8xC	a
Tasmánií	Tasmánie	k1gFnSc7	Tasmánie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Beringova	Beringův	k2eAgInSc2d1	Beringův
průlivu	průliv	k1gInSc2	průliv
existoval	existovat	k5eAaImAgInS	existovat
v	v	k7c6	v
době	doba	k1gFnSc6	doba
ledové	ledový	k2eAgFnSc2d1	ledová
Beringův	Beringův	k2eAgInSc4d1	Beringův
most	most	k1gInSc4	most
spojující	spojující	k2eAgFnSc4d1	spojující
Auroasii	Auroasie	k1gFnSc4	Auroasie
se	s	k7c7	s
Severní	severní	k2eAgFnSc7d1	severní
Amerikou	Amerika	k1gFnSc7	Amerika
<g/>
,	,	kIx,	,
v	v	k7c6	v
době	doba	k1gFnSc6	doba
před	před	k7c4	před
9600	[number]	k4	9600
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
se	se	k3xPyFc4	se
však	však	k9	však
už	už	k6eAd1	už
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
těsně	těsně	k6eAd1	těsně
rozpojil	rozpojit	k5eAaPmAgMnS	rozpojit
vlivem	vlivem	k7c2	vlivem
postupujícího	postupující	k2eAgNnSc2d1	postupující
tání	tání	k1gNnSc2	tání
pevninských	pevninský	k2eAgInPc2d1	pevninský
ledovců	ledovec	k1gInPc2	ledovec
a	a	k8xC	a
stoupáním	stoupání	k1gNnSc7	stoupání
hladiny	hladina	k1gFnSc2	hladina
světového	světový	k2eAgInSc2d1	světový
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
kontinenty	kontinent	k1gInPc1	kontinent
měly	mít	k5eAaImAgInP	mít
větší	veliký	k2eAgFnSc4d2	veliký
suchozemskou	suchozemský	k2eAgFnSc4d1	suchozemská
plochu	plocha	k1gFnSc4	plocha
než	než	k8xS	než
dnes	dnes	k6eAd1	dnes
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
mělkých	mělký	k2eAgFnPc6d1	mělká
částech	část	k1gFnPc6	část
kontinentálních	kontinentální	k2eAgInPc2d1	kontinentální
šelfů	šelf	k1gInPc2	šelf
<g/>
,	,	kIx,	,
analogicky	analogicky	k6eAd1	analogicky
měly	mít	k5eAaImAgFnP	mít
větší	veliký	k2eAgFnSc4d2	veliký
rozlohu	rozloha	k1gFnSc4	rozloha
i	i	k9	i
některé	některý	k3yIgInPc4	některý
ostrovy	ostrov	k1gInPc4	ostrov
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
historiků	historik	k1gMnPc2	historik
se	se	k3xPyFc4	se
shoduje	shodovat	k5eAaImIp3nS	shodovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Atlantida	Atlantida	k1gFnSc1	Atlantida
je	být	k5eAaImIp3nS	být
pouze	pouze	k6eAd1	pouze
Platónova	Platónův	k2eAgFnSc1d1	Platónova
fikce	fikce	k1gFnSc1	fikce
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
pro	pro	k7c4	pro
to	ten	k3xDgNnSc4	ten
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
období	období	k1gNnSc6	období
10	[number]	k4	10
000	[number]	k4	000
let	léto	k1gNnPc2	léto
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
je	být	k5eAaImIp3nS	být
existence	existence	k1gFnSc1	existence
takto	takto	k6eAd1	takto
vyspělých	vyspělý	k2eAgFnPc2d1	vyspělá
civilizací	civilizace	k1gFnPc2	civilizace
prakticky	prakticky	k6eAd1	prakticky
vyloučena	vyloučit	k5eAaPmNgFnS	vyloučit
<g/>
,	,	kIx,	,
neexistují	existovat	k5eNaImIp3nP	existovat
také	také	k9	také
jiné	jiný	k2eAgInPc4d1	jiný
písemné	písemný	k2eAgInPc4d1	písemný
prameny	pramen	k1gInPc4	pramen
než	než	k8xS	než
Platónovy	Platónův	k2eAgFnPc4d1	Platónova
(	(	kIx(	(
<g/>
o	o	k7c6	o
Atlantidě	Atlantida	k1gFnSc6	Atlantida
se	se	k3xPyFc4	se
nezmiňuje	zmiňovat	k5eNaImIp3nS	zmiňovat
Herodotos	Herodotos	k1gMnSc1	Herodotos
ani	ani	k8xC	ani
žádný	žádný	k3yNgMnSc1	žádný
jiný	jiný	k2eAgMnSc1d1	jiný
řecký	řecký	k2eAgMnSc1d1	řecký
autor	autor	k1gMnSc1	autor
<g/>
,	,	kIx,	,
i	i	k9	i
když	když	k8xS	když
by	by	kYmCp3nS	by
jim	on	k3xPp3gMnPc3	on
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
musela	muset	k5eAaImAgFnS	muset
historie	historie	k1gFnSc1	historie
Atlantidy	Atlantida	k1gFnSc2	Atlantida
být	být	k5eAaImF	být
známa	známo	k1gNnPc4	známo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Platón	platón	k1gInSc1	platón
nejspíše	nejspíše	k9	nejspíše
použil	použít	k5eAaPmAgInS	použít
příběh	příběh	k1gInSc1	příběh
Atlantidy	Atlantida	k1gFnSc2	Atlantida
jako	jako	k8xC	jako
demonstrace	demonstrace	k1gFnSc1	demonstrace
ideálního	ideální	k2eAgInSc2d1	ideální
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
měli	mít	k5eAaImAgMnP	mít
Atlanťané	Atlanťan	k1gMnPc1	Atlanťan
podle	podle	k7c2	podle
Platóna	Platón	k1gMnSc2	Platón
žít	žít	k5eAaImF	žít
spořádaně	spořádaně	k6eAd1	spořádaně
a	a	k8xC	a
v	v	k7c6	v
souladu	soulad	k1gInSc6	soulad
se	s	k7c7	s
zákony	zákon	k1gInPc7	zákon
bohů	bůh	k1gMnPc2	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
ale	ale	k9	ale
začali	začít	k5eAaPmAgMnP	začít
toužit	toužit	k5eAaImF	toužit
po	po	k7c6	po
větším	veliký	k2eAgNnSc6d2	veliký
bohatství	bohatství	k1gNnSc6	bohatství
a	a	k8xC	a
moci	moc	k1gFnSc3	moc
<g/>
,	,	kIx,	,
ztratili	ztratit	k5eAaPmAgMnP	ztratit
důvěru	důvěra	k1gFnSc4	důvěra
bohů	bůh	k1gMnPc2	bůh
a	a	k8xC	a
Zeus	Zeusa	k1gFnPc2	Zeusa
se	se	k3xPyFc4	se
nakonec	nakonec	k6eAd1	nakonec
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
celý	celý	k2eAgInSc4d1	celý
kontinent	kontinent	k1gInSc4	kontinent
zničit	zničit	k5eAaPmF	zničit
mohutným	mohutný	k2eAgNnSc7d1	mohutné
zemětřesením	zemětřesení	k1gNnSc7	zemětřesení
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
:	:	kIx,	:
Atlantida	Atlantida	k1gFnSc1	Atlantida
<g/>
.	.	kIx.	.
</s>
<s>
Legenda	legenda	k1gFnSc1	legenda
o	o	k7c6	o
Atlantidě	Atlantida	k1gFnSc6	Atlantida
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
knihách	kniha	k1gFnPc6	kniha
<g/>
,	,	kIx,	,
filmech	film	k1gInPc6	film
<g/>
,	,	kIx,	,
televizních	televizní	k2eAgInPc6d1	televizní
seriálech	seriál	k1gInPc6	seriál
<g/>
,	,	kIx,	,
hrách	hra	k1gFnPc6	hra
a	a	k8xC	a
písních	píseň	k1gFnPc6	píseň
<g/>
.	.	kIx.	.
</s>
<s>
Jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
posledních	poslední	k2eAgNnPc2d1	poslední
vyobrazení	vyobrazení	k1gNnPc2	vyobrazení
Atlantidy	Atlantida	k1gFnSc2	Atlantida
v	v	k7c6	v
médiích	médium	k1gNnPc6	médium
je	být	k5eAaImIp3nS	být
televizní	televizní	k2eAgInSc4d1	televizní
seriál	seriál	k1gInSc4	seriál
Stargate	Stargat	k1gInSc5	Stargat
Atlantis	Atlantis	k1gFnSc1	Atlantis
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
navazuje	navazovat	k5eAaImIp3nS	navazovat
na	na	k7c4	na
televizní	televizní	k2eAgInSc4d1	televizní
seriál	seriál	k1gInSc4	seriál
Hvězdná	hvězdný	k2eAgFnSc1d1	hvězdná
brána	brána	k1gFnSc1	brána
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
něm	on	k3xPp3gInSc6	on
je	být	k5eAaImIp3nS	být
Atlantida	Atlantida	k1gFnSc1	Atlantida
bájné	bájný	k2eAgNnSc1d1	bájné
město	město	k1gNnSc1	město
mimozemské	mimozemský	k2eAgFnSc2d1	mimozemská
rasy	rasa	k1gFnSc2	rasa
<g/>
,	,	kIx,	,
zvané	zvaný	k2eAgFnSc2d1	zvaná
Antikové	Antikový	k2eAgFnSc2d1	Antikový
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
Antikové	Antikový	k2eAgNnSc1d1	Antikové
celé	celý	k2eAgNnSc1d1	celé
město	město	k1gNnSc1	město
přesunuli	přesunout	k5eAaPmAgMnP	přesunout
do	do	k7c2	do
galaxie	galaxie	k1gFnSc2	galaxie
Pegasus	Pegasus	k1gMnSc1	Pegasus
<g/>
.	.	kIx.	.
</s>
<s>
Pendragon	Pendragon	k1gMnSc1	Pendragon
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
díl	díl	k1gInSc1	díl
<g/>
:	:	kIx,	:
Taliesin	Taliesin	k1gInSc1	Taliesin
<g/>
,	,	kIx,	,
od	od	k7c2	od
Stephena	Stephen	k2eAgFnSc1d1	Stephena
Lawheada	Lawheada	k1gFnSc1	Lawheada
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
New	New	k1gFnSc1	New
Atlantis	Atlantis	k1gFnSc1	Atlantis
(	(	kIx(	(
<g/>
Nová	nový	k2eAgFnSc1d1	nová
Atlantida	Atlantida	k1gFnSc1	Atlantida
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
utopie	utopie	k1gFnSc1	utopie
Frencise	Frencise	k1gFnSc1	Frencise
Bacona	Bacona	k1gFnSc1	Bacona
<g/>
.	.	kIx.	.
</s>
<s>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Atlantide	Atlantid	k1gInSc5	Atlantid
(	(	kIx(	(
<g/>
Atlantida	Atlantida	k1gFnSc1	Atlantida
<g/>
)	)	kIx)	)
od	od	k7c2	od
Pierra	Pierro	k1gNnSc2	Pierro
Benoita	Benoita	k1gFnSc1	Benoita
se	se	k3xPyFc4	se
osvobodila	osvobodit	k5eAaPmAgFnS	osvobodit
od	od	k7c2	od
platónské	platónský	k2eAgFnSc2d1	platónská
tradice	tradice	k1gFnSc2	tradice
a	a	k8xC	a
umisťuje	umisťovat	k5eAaImIp3nS	umisťovat
Atlantidu	Atlantida	k1gFnSc4	Atlantida
do	do	k7c2	do
srdce	srdce	k1gNnSc2	srdce
Sahary	Sahara	k1gFnSc2	Sahara
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
románu	román	k1gInSc6	román
Dvacet	dvacet	k4xCc4	dvacet
tisíc	tisíc	k4xCgInPc2	tisíc
mil	míle	k1gFnPc2	míle
pod	pod	k7c7	pod
mořem	moře	k1gNnSc7	moře
Julesa	Julesa	k1gFnSc1	Julesa
Verna	Verno	k1gNnSc2	Verno
najde	najít	k5eAaPmIp3nS	najít
výprava	výprava	k1gFnSc1	výprava
kapitána	kapitán	k1gMnSc2	kapitán
Nema	Nemus	k1gMnSc2	Nemus
ruiny	ruina	k1gFnSc2	ruina
Atlantidy	Atlantida	k1gFnSc2	Atlantida
<g/>
.	.	kIx.	.
</s>
<s>
Opération	Opération	k1gInSc1	Opération
Atlantide	Atlantid	k1gInSc5	Atlantid
(	(	kIx(	(
<g/>
Operace	operace	k1gFnSc1	operace
Atlantida	Atlantida	k1gFnSc1	Atlantida
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
název	název	k1gInSc1	název
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
příběhů	příběh	k1gInPc2	příběh
Boba	Bob	k1gMnSc2	Bob
Morana	Morana	k1gFnSc1	Morana
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
napsal	napsat	k5eAaPmAgMnS	napsat
belgický	belgický	k2eAgMnSc1d1	belgický
spisovatel	spisovatel	k1gMnSc1	spisovatel
Henri	Henr	k1gFnSc2	Henr
Vernes	Vernes	k1gMnSc1	Vernes
<g/>
.	.	kIx.	.
</s>
<s>
Tolkien	Tolkien	k1gInSc1	Tolkien
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
fiktivní	fiktivní	k2eAgInSc4d1	fiktivní
ostrov	ostrov	k1gInSc4	ostrov
Númenor	Númenora	k1gFnPc2	Númenora
podle	podle	k7c2	podle
mýtu	mýtus	k1gInSc2	mýtus
o	o	k7c6	o
Atlantidě	Atlantida	k1gFnSc6	Atlantida
<g/>
.	.	kIx.	.
</s>
<s>
Atlantis	Atlantis	k1gFnSc1	Atlantis
je	být	k5eAaImIp3nS	být
název	název	k1gInSc4	název
časopisu	časopis	k1gInSc2	časopis
a	a	k8xC	a
badatelské	badatelský	k2eAgFnSc2d1	badatelská
esoterické	esoterický	k2eAgFnSc2d1	esoterická
skupiny	skupina	k1gFnSc2	skupina
vytvořené	vytvořený	k2eAgFnSc2d1	vytvořená
Paulem	Paul	k1gMnSc7	Paul
Le	Le	k1gMnSc2	Le
Cour	Cour	k?	Cour
<g/>
.	.	kIx.	.
</s>
<s>
Civilisations	Civilisations	k6eAd1	Civilisations
englouties	englouties	k1gInSc1	englouties
<g/>
,	,	kIx,	,
kniha	kniha	k1gFnSc1	kniha
Grahama	Graham	k1gMnSc2	Graham
Hancocka	Hancocka	k1gFnSc1	Hancocka
(	(	kIx(	(
<g/>
2002	[number]	k4	2002
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Empreinte	Empreint	k1gInSc5	Empreint
des	des	k1gNnPc2	des
Dieux	Dieux	k1gInSc1	Dieux
(	(	kIx(	(
<g/>
Stopa	stopa	k1gFnSc1	stopa
bohů	bůh	k1gMnPc2	bůh
<g/>
)	)	kIx)	)
od	od	k7c2	od
Grahama	Graham	k1gMnSc2	Graham
Hancocka	Hancocko	k1gNnSc2	Hancocko
(	(	kIx(	(
<g/>
1996	[number]	k4	1996
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
énigme	énigmat	k5eAaPmIp3nS	énigmat
de	de	k?	de
l	l	kA	l
<g/>
'	'	kIx"	'
<g/>
Atlantide	Atlantid	k1gInSc5	Atlantid
(	(	kIx(	(
<g/>
Záhada	záhada	k1gFnSc1	záhada
Atlantidy	Atlantida	k1gFnSc2	Atlantida
<g/>
)	)	kIx)	)
od	od	k7c2	od
Edouarda	Edouard	k1gMnSc2	Edouard
Braseye	Brasey	k1gMnSc2	Brasey
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Atlantide	Atlantid	k1gMnSc5	Atlantid
amerického	americký	k2eAgMnSc2d1	americký
romanopisce	romanopisec	k1gMnSc2	romanopisec
Clivea	Cliveus	k1gMnSc2	Cliveus
Cusslera	Cussler	k1gMnSc2	Cussler
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Atlantide	Atlantid	k1gMnSc5	Atlantid
<g/>
,	,	kIx,	,
la	la	k1gNnSc1	la
solution	solution	k1gInSc1	solution
oubliée	oubliée	k1gFnSc1	oubliée
(	(	kIx(	(
<g/>
Atlantida	Atlantida	k1gFnSc1	Atlantida
<g/>
,	,	kIx,	,
zapomenuté	zapomenutý	k2eAgNnSc1d1	zapomenuté
řešení	řešení	k1gNnSc1	řešení
<g/>
)	)	kIx)	)
Jacquesa	Jacques	k1gMnSc4	Jacques
Héberta	Hébert	k1gMnSc4	Hébert
(	(	kIx(	(
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
V	v	k7c6	v
sérii	série	k1gFnSc6	série
románů	román	k1gInPc2	román
les	les	k1gInSc1	les
Dossiers	Dossiers	k1gInSc1	Dossiers
du	du	k?	du
futur	futurum	k1gNnPc2	futurum
(	(	kIx(	(
<g/>
Záznamy	záznam	k1gInPc7	záznam
o	o	k7c6	o
budoucnosti	budoucnost	k1gFnSc6	budoucnost
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
egyptský	egyptský	k2eAgMnSc1d1	egyptský
vědeckofantastický	vědeckofantastický	k2eAgMnSc1d1	vědeckofantastický
spisovatel	spisovatel	k1gMnSc1	spisovatel
Nabil	nabít	k5eAaPmAgInS	nabít
Farouk	Farouk	k1gInSc4	Farouk
vypráví	vyprávět	k5eAaImIp3nS	vyprávět
o	o	k7c6	o
mýtu	mýtus	k1gInSc6	mýtus
o	o	k7c6	o
Atlantidě	Atlantida	k1gFnSc6	Atlantida
v	v	k7c6	v
románech	román	k1gInPc6	román
číslo	číslo	k1gNnSc1	číslo
47	[number]	k4	47
<g/>
:	:	kIx,	:
le	le	k?	le
Dernier	Dernier	k1gMnSc1	Dernier
Guerrier	Guerrier	k1gMnSc1	Guerrier
(	(	kIx(	(
<g/>
Poslední	poslední	k2eAgMnSc1d1	poslední
voják	voják	k1gMnSc1	voják
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
89	[number]	k4	89
až	až	k9	až
93	[number]	k4	93
<g/>
:	:	kIx,	:
Volkano	Volkana	k1gFnSc5	Volkana
(	(	kIx(	(
<g/>
Vulkán	vulkán	k1gInSc4	vulkán
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prezentuje	prezentovat	k5eAaBmIp3nS	prezentovat
Atlantidu	Atlantida	k1gFnSc4	Atlantida
jako	jako	k9	jako
vysoce	vysoce	k6eAd1	vysoce
vyspělou	vyspělý	k2eAgFnSc4d1	vyspělá
civilizaci	civilizace	k1gFnSc4	civilizace
ve	v	k7c6	v
spojení	spojení	k1gNnSc6	spojení
s	s	k7c7	s
Egyptem	Egypt	k1gInSc7	Egypt
před	před	k7c4	před
10	[number]	k4	10
000	[number]	k4	000
lety	léto	k1gNnPc7	léto
<g/>
.	.	kIx.	.
</s>
<s>
Atlantides	Atlantides	k1gInSc1	Atlantides
<g/>
,	,	kIx,	,
les	les	k1gInSc1	les
Îles	Îles	k1gInSc1	Îles
Englouties	Englouties	k1gInSc1	Englouties
(	(	kIx(	(
<g/>
Atlantida	Atlantida	k1gFnSc1	Atlantida
<g/>
,	,	kIx,	,
potopené	potopený	k2eAgInPc1d1	potopený
ostrovy	ostrov	k1gInPc1	ostrov
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
soubor	soubor	k1gInSc4	soubor
novel	novela	k1gFnPc2	novela
a	a	k8xC	a
románů	román	k1gInPc2	román
o	o	k7c6	o
Atlantidě	Atlantida	k1gFnSc6	Atlantida
<g/>
.	.	kIx.	.
</s>
<s>
Nacházejí	nacházet	k5eAaImIp3nP	nacházet
se	se	k3xPyFc4	se
v	v	k7c6	v
něm	on	k3xPp3gInSc6	on
díla	dílo	k1gNnPc4	dílo
Cutliffa	Cutliff	k1gMnSc2	Cutliff
Hyna	hynout	k5eAaImSgInS	hynout
<g/>
,	,	kIx,	,
Julesa	Julesa	k1gFnSc1	Julesa
Verna	Verna	k1gFnSc1	Verna
<g/>
,	,	kIx,	,
H.	H.	kA	H.
Ridera	Rider	k1gMnSc2	Rider
Haggarda	Haggard	k1gMnSc2	Haggard
<g/>
,	,	kIx,	,
Jeana	Jean	k1gMnSc2	Jean
Carrè	Carrè	k1gFnSc2	Carrè
a	a	k8xC	a
mnoha	mnoho	k4c2	mnoho
dalších	další	k2eAgFnPc2d1	další
<g/>
.	.	kIx.	.
</s>
