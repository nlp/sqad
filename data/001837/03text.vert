<s>
Megalosaurus	Megalosaurus	k1gInSc1	Megalosaurus
(	(	kIx(	(
<g/>
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
velký	velký	k2eAgInSc1d1	velký
plaz	plaz	k1gInSc1	plaz
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
z	z	k7c2	z
řeckého	řecký	k2eAgInSc2d1	řecký
<g/>
,	,	kIx,	,
μ	μ	k?	μ
<g/>
/	/	kIx~	/
<g/>
megalo-	megalo-	k?	megalo-
'	'	kIx"	'
<g/>
velký	velký	k2eAgInSc1d1	velký
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
'	'	kIx"	'
<g/>
vysoký	vysoký	k2eAgMnSc1d1	vysoký
<g/>
'	'	kIx"	'
<g/>
,	,	kIx,	,
a	a	k8xC	a
σ	σ	k?	σ
<g/>
/	/	kIx~	/
<g/>
sauros	saurosa	k1gFnPc2	saurosa
'	'	kIx"	'
<g/>
plaz	plaz	k1gInSc1	plaz
<g/>
'	'	kIx"	'
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vědeckým	vědecký	k2eAgInSc7d1	vědecký
názvem	název	k1gInSc7	název
M.	M.	kA	M.
(	(	kIx(	(
<g/>
=	=	kIx~	=
<g/>
Megalosaurus	Megalosaurus	k1gInSc4	Megalosaurus
<g/>
)	)	kIx)	)
bucklandii	bucklandie	k1gFnSc4	bucklandie
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
rod	rod	k1gInSc1	rod
velkých	velký	k2eAgMnPc2d1	velký
masožravých	masožraví	k1gMnPc2	masožraví
teropodních	teropodní	k2eAgInPc2d1	teropodní
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
středního	střední	k1gMnSc2	střední
až	až	k8xS	až
svrchního	svrchní	k2eAgNnSc2d1	svrchní
jurského	jurský	k2eAgNnSc2d1	jurské
období	období	k1gNnSc2	období
(	(	kIx(	(
<g/>
stupeň	stupeň	k1gInSc1	stupeň
bath	bath	k1gInSc1	bath
<g/>
;	;	kIx,	;
podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
stupně	stupeň	k1gInSc2	stupeň
aalen-oxford	aalenxforda	k1gFnPc2	aalen-oxforda
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
<g/>
)	)	kIx)	)
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
(	(	kIx(	(
<g/>
jižní	jižní	k2eAgFnSc2d1	jižní
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc2	Francie
<g/>
,	,	kIx,	,
Portugalsko	Portugalsko	k1gNnSc1	Portugalsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
významný	významný	k2eAgMnSc1d1	významný
jako	jako	k8xC	jako
vůbec	vůbec	k9	vůbec
první	první	k4xOgInSc1	první
rod	rod	k1gInSc1	rod
dinosaurů	dinosaurus	k1gMnPc2	dinosaurus
(	(	kIx(	(
<g/>
vyjma	vyjma	k7c2	vyjma
ptáků	pták	k1gMnPc2	pták
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
byl	být	k5eAaImAgInS	být
vědecky	vědecky	k6eAd1	vědecky
popsán	popsat	k5eAaPmNgInS	popsat
a	a	k8xC	a
pojmenován	pojmenovat	k5eAaPmNgInS	pojmenovat
<g/>
.	.	kIx.	.
</s>
<s>
Megalosaurus	Megalosaurus	k1gInSc1	Megalosaurus
byl	být	k5eAaImAgInS	být
prvním	první	k4xOgNnSc7	první
(	(	kIx(	(
<g/>
neptačím	ptačí	k2eNgMnSc7d1	neptačí
<g/>
)	)	kIx)	)
dinosaurem	dinosaurus	k1gMnSc7	dinosaurus
popsaným	popsaný	k2eAgInSc7d1	popsaný
formálně	formálně	k6eAd1	formálně
ve	v	k7c6	v
vědecké	vědecký	k2eAgFnSc6d1	vědecká
literatuře	literatura	k1gFnSc6	literatura
(	(	kIx(	(
<g/>
1824	[number]	k4	1824
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
jeho	jeho	k3xOp3gFnSc2	jeho
kosti	kost	k1gFnSc2	kost
byla	být	k5eAaImAgFnS	být
poprvé	poprvé	k6eAd1	poprvé
nalezena	naleznout	k5eAaPmNgFnS	naleznout
ve	v	k7c6	v
vápencovém	vápencový	k2eAgInSc6d1	vápencový
lomu	lom	k1gInSc6	lom
u	u	k7c2	u
Cornwellu	Cornwell	k1gInSc2	Cornwell
<g/>
,	,	kIx,	,
nedaleko	daleko	k6eNd1	daleko
Chipping	Chipping	k1gInSc1	Chipping
Nortonu	Norton	k1gInSc2	Norton
<g/>
,	,	kIx,	,
v	v	k7c6	v
Oxfordském	oxfordský	k2eAgNnSc6d1	Oxfordské
hrabství	hrabství	k1gNnSc6	hrabství
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1676	[number]	k4	1676
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
nešlo	jít	k5eNaImAgNnS	jít
o	o	k7c4	o
první	první	k4xOgInSc4	první
evropský	evropský	k2eAgInSc4d1	evropský
objev	objev	k1gInSc4	objev
dinosauří	dinosauří	k2eAgFnSc2d1	dinosauří
kosti	kost	k1gFnSc2	kost
<g/>
,	,	kIx,	,
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
došlo	dojít	k5eAaPmAgNnS	dojít
zřejmě	zřejmě	k6eAd1	zřejmě
již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1614	[number]	k4	1614
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
Cornwellská	Cornwellský	k2eAgFnSc1d1	Cornwellský
kost	kost	k1gFnSc1	kost
byla	být	k5eAaImAgFnS	být
popsána	popsat	k5eAaPmNgFnS	popsat
nejprve	nejprve	k6eAd1	nejprve
ředitelem	ředitel	k1gMnSc7	ředitel
Ashmolean	Ashmolean	k1gMnSc1	Ashmolean
museum	museum	k1gNnSc1	museum
Robertem	Robert	k1gMnSc7	Robert
Plotem	plot	k1gInSc7	plot
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
její	její	k3xOp3gInSc1	její
popis	popis	k1gInSc1	popis
publikoval	publikovat	k5eAaBmAgInS	publikovat
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
Natural	Natural	k?	Natural
History	Histor	k1gInPc4	Histor
of	of	k?	of
Oxfordshire	Oxfordshir	k1gInSc5	Oxfordshir
(	(	kIx(	(
<g/>
Vývoj	vývoj	k1gInSc1	vývoj
přírody	příroda	k1gFnSc2	příroda
oxfordského	oxfordský	k2eAgNnSc2d1	Oxfordské
hrabství	hrabství	k1gNnSc2	hrabství
<g/>
)	)	kIx)	)
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1677	[number]	k4	1677
<g/>
.	.	kIx.	.
</s>
<s>
Plot	plot	k1gInSc1	plot
fragment	fragment	k1gInSc1	fragment
sice	sice	k8xC	sice
správně	správně	k6eAd1	správně
určil	určit	k5eAaPmAgInS	určit
jako	jako	k9	jako
spodní	spodní	k2eAgInSc1d1	spodní
výběžek	výběžek	k1gInSc1	výběžek
stehenní	stehenní	k2eAgFnSc2d1	stehenní
kosti	kost	k1gFnSc2	kost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ale	ale	k9	ale
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
předpokládané	předpokládaný	k2eAgFnSc3d1	předpokládaná
velikosti	velikost	k1gFnSc3	velikost
nemohla	moct	k5eNaImAgFnS	moct
patřit	patřit	k5eAaImF	patřit
žádnému	žádný	k3yNgMnSc3	žádný
známému	známý	k1gMnSc3	známý
tvoru	tvor	k1gMnSc3	tvor
<g/>
.	.	kIx.	.
</s>
<s>
Usoudil	usoudit	k5eAaPmAgMnS	usoudit
tedy	tedy	k9	tedy
<g/>
,	,	kIx,	,
že	že	k8xS	že
patřila	patřit	k5eAaImAgFnS	patřit
patrně	patrně	k6eAd1	patrně
obrům	obr	k1gMnPc3	obr
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
takovým	takový	k3xDgMnPc3	takový
<g/>
,	,	kIx,	,
jací	jaký	k3yQgMnPc1	jaký
jsou	být	k5eAaImIp3nP	být
popsáni	popsat	k5eAaPmNgMnP	popsat
v	v	k7c6	v
Bibli	bible	k1gFnSc6	bible
(	(	kIx(	(
<g/>
Gn	Gn	k1gFnSc1	Gn
6,4	[number]	k4	6,4
<g/>
;	;	kIx,	;
Nu	nu	k9	nu
13	[number]	k4	13
<g/>
,	,	kIx,	,
33	[number]	k4	33
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Znovu	znovu	k6eAd1	znovu
byla	být	k5eAaImAgFnS	být
zkamenělina	zkamenělina	k1gFnSc1	zkamenělina
popsána	popsat	k5eAaPmNgFnS	popsat
Richardem	Richard	k1gMnSc7	Richard
Brookesem	Brookes	k1gMnSc7	Brookes
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1763	[number]	k4	1763
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
ji	on	k3xPp3gFnSc4	on
pro	pro	k7c4	pro
její	její	k3xOp3gFnSc4	její
podobnost	podobnost	k1gFnSc4	podobnost
s	s	k7c7	s
lidskými	lidský	k2eAgNnPc7d1	lidské
varlaty	varle	k1gNnPc7	varle
nazval	nazvat	k5eAaBmAgMnS	nazvat
"	"	kIx"	"
<g/>
Scrotum	Scrotum	k1gNnSc1	Scrotum
humanum	humanum	k1gNnSc1	humanum
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
šourek	šourek	k1gInSc4	šourek
lidský	lidský	k2eAgInSc4d1	lidský
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Megalosaurus	Megalosaurus	k1gMnSc1	Megalosaurus
byl	být	k5eAaImAgMnS	být
štíhle	štíhle	k6eAd1	štíhle
stavěný	stavěný	k2eAgMnSc1d1	stavěný
dvounohý	dvounohý	k2eAgMnSc1d1	dvounohý
dravec	dravec	k1gMnSc1	dravec
<g/>
,	,	kIx,	,
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
přibližně	přibližně	k6eAd1	přibližně
9	[number]	k4	9
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
vážící	vážící	k2eAgMnSc1d1	vážící
kolem	kolem	k7c2	kolem
1	[number]	k4	1
tuny	tuna	k1gFnSc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
ostré	ostrý	k2eAgInPc1d1	ostrý
a	a	k8xC	a
mírně	mírně	k6eAd1	mírně
vroubkované	vroubkovaný	k2eAgInPc1d1	vroubkovaný
zuby	zub	k1gInPc1	zub
měly	mít	k5eAaImAgInP	mít
dlouhé	dlouhý	k2eAgInPc4d1	dlouhý
kořeny	kořen	k1gInPc4	kořen
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
byly	být	k5eAaImAgInP	být
pevně	pevně	k6eAd1	pevně
zasazené	zasazený	k2eAgInPc1d1	zasazený
v	v	k7c6	v
čelisti	čelist	k1gFnSc6	čelist
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnPc1	jejich
korunky	korunka	k1gFnPc1	korunka
byly	být	k5eAaImAgFnP	být
ze	z	k7c2	z
stran	strana	k1gFnPc2	strana
zploštělé	zploštělý	k2eAgFnSc2d1	zploštělá
a	a	k8xC	a
ohýbaly	ohýbat	k5eAaImAgInP	ohýbat
se	s	k7c7	s
směrem	směr	k1gInSc7	směr
vzad	vzad	k6eAd1	vzad
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
předních	přední	k2eAgFnPc6d1	přední
končetinách	končetina	k1gFnPc6	končetina
měl	mít	k5eAaImAgInS	mít
tři	tři	k4xCgInPc4	tři
prsty	prst	k1gInPc4	prst
<g/>
,	,	kIx,	,
na	na	k7c6	na
zadních	zadní	k2eAgFnPc6d1	zadní
pak	pak	k6eAd1	pak
čtyři	čtyři	k4xCgFnPc4	čtyři
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgInSc4	jeden
drobný	drobný	k2eAgInSc4d1	drobný
<g/>
)	)	kIx)	)
vybavené	vybavený	k2eAgInPc4d1	vybavený
silnými	silný	k2eAgInPc7d1	silný
a	a	k8xC	a
ostrými	ostrý	k2eAgInPc7d1	ostrý
drápy	dráp	k1gInPc7	dráp
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
rodovým	rodový	k2eAgInSc7d1	rodový
názvem	název	k1gInSc7	název
Megalosaurus	Megalosaurus	k1gMnSc1	Megalosaurus
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
doby	doba	k1gFnSc2	doba
popsána	popsat	k5eAaPmNgFnS	popsat
řada	řada	k1gFnSc1	řada
velmi	velmi	k6eAd1	velmi
různých	různý	k2eAgInPc2d1	různý
teropodů	teropod	k1gInPc2	teropod
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jediným	jediný	k2eAgInSc7d1	jediný
validním	validní	k2eAgInSc7d1	validní
(	(	kIx(	(
<g/>
platným	platný	k2eAgInSc7d1	platný
<g/>
)	)	kIx)	)
druhem	druh	k1gInSc7	druh
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
zdá	zdát	k5eAaPmIp3nS	zdát
být	být	k5eAaImF	být
pouze	pouze	k6eAd1	pouze
Megalosaurus	Megalosaurus	k1gInSc1	Megalosaurus
bucklandii	bucklandie	k1gFnSc4	bucklandie
z	z	k7c2	z
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
</s>
<s>
Teropodní	Teropodní	k2eAgMnPc1d1	Teropodní
dinosauři	dinosaurus	k1gMnPc1	dinosaurus
<g/>
:	:	kIx,	:
Metriacanthosaurus	Metriacanthosaurus	k1gMnSc1	Metriacanthosaurus
<g/>
,	,	kIx,	,
Nuthetes	Nuthetes	k1gMnSc1	Nuthetes
<g/>
,	,	kIx,	,
Dryptosaurus	Dryptosaurus	k1gMnSc1	Dryptosaurus
<g/>
,	,	kIx,	,
Dilophosaurus	Dilophosaurus	k1gMnSc1	Dilophosaurus
<g/>
,	,	kIx,	,
Ceratosaurus	Ceratosaurus	k1gMnSc1	Ceratosaurus
<g/>
,	,	kIx,	,
Magnosaurus	Magnosaurus	k1gMnSc1	Magnosaurus
<g/>
,	,	kIx,	,
Carcharodontosaurus	Carcharodontosaurus	k1gMnSc1	Carcharodontosaurus
<g/>
,	,	kIx,	,
Eustreptospondylus	Eustreptospondylus	k1gMnSc1	Eustreptospondylus
<g/>
,	,	kIx,	,
Proceratosaurus	Proceratosaurus	k1gMnSc1	Proceratosaurus
<g/>
,	,	kIx,	,
Majungasaurus	Majungasaurus	k1gMnSc1	Majungasaurus
<g/>
,	,	kIx,	,
Altispinax	Altispinax	k1gInSc1	Altispinax
<g/>
,	,	kIx,	,
Deinodon	Deinodon	k1gMnSc1	Deinodon
<g/>
,	,	kIx,	,
Zanclodon	Zanclodon	k1gMnSc1	Zanclodon
<g/>
,	,	kIx,	,
Duriavenator	Duriavenator	k1gMnSc1	Duriavenator
<g/>
,	,	kIx,	,
Sarcosaurus	Sarcosaurus	k1gMnSc1	Sarcosaurus
<g/>
,	,	kIx,	,
Poekilopleuron	Poekilopleuron	k1gMnSc1	Poekilopleuron
<g/>
,	,	kIx,	,
Streptospondylus	Streptospondylus	k1gMnSc1	Streptospondylus
<g/>
,	,	kIx,	,
Newtonsaurus	Newtonsaurus	k1gInSc4	Newtonsaurus
Jiní	jiný	k2eAgMnPc1d1	jiný
dinosauři	dinosaurus	k1gMnPc1	dinosaurus
zaměňovaní	zaměňovaný	k2eAgMnPc1d1	zaměňovaný
s	s	k7c7	s
megalosaurem	megalosaur	k1gInSc7	megalosaur
<g/>
:	:	kIx,	:
prosauropod	prosauropod	k1gInSc1	prosauropod
Plateosaurus	Plateosaurus	k1gInSc1	Plateosaurus
</s>
