<s>
Megalosaurus	Megalosaurus	k1gMnSc1	Megalosaurus
byl	být	k5eAaImAgMnS	být
štíhle	štíhle	k6eAd1	štíhle
stavěný	stavěný	k2eAgMnSc1d1	stavěný
dvounohý	dvounohý	k2eAgMnSc1d1	dvounohý
dravec	dravec	k1gMnSc1	dravec
<g/>
,	,	kIx,	,
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
přibližně	přibližně	k6eAd1	přibližně
9	[number]	k4	9
metrů	metr	k1gInPc2	metr
a	a	k8xC	a
vážící	vážící	k2eAgMnSc1d1	vážící
kolem	kolem	k7c2	kolem
1	[number]	k4	1
tuny	tuna	k1gFnSc2	tuna
<g/>
.	.	kIx.	.
</s>
