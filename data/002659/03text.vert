<s>
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
(	(	kIx(	(
<g/>
srbskou	srbský	k2eAgFnSc7d1	Srbská
cyrilicí	cyrilice	k1gFnSc7	cyrilice
С	С	k?	С
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jde	jít	k5eAaImIp3nS	jít
také	také	k9	také
sídlem	sídlo	k1gNnSc7	sídlo
samosprávné	samosprávný	k2eAgFnSc2d1	samosprávná
entity	entita	k1gFnSc2	entita
Federace	federace	k1gFnSc2	federace
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
(	(	kIx(	(
<g/>
FBaH	FBaH	k1gFnSc1	FBaH
<g/>
)	)	kIx)	)
a	a	k8xC	a
jedné	jeden	k4xCgFnSc3	jeden
ze	z	k7c2	z
samosprávních	samosprávní	k2eAgFnPc2d1	samosprávní
jednotek	jednotka	k1gFnPc2	jednotka
Federace	federace	k1gFnSc2	federace
BaH	bah	k0	bah
-	-	kIx~	-
kantonu	kanton	k1gInSc2	kanton
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
historické	historický	k2eAgFnSc2d1	historická
části	část	k1gFnSc2	část
Sarajeva	Sarajevo	k1gNnSc2	Sarajevo
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
odděleno	oddělen	k2eAgNnSc4d1	odděleno
Východní	východní	k2eAgNnSc4d1	východní
Sarajevo	Sarajevo	k1gNnSc4	Sarajevo
(	(	kIx(	(
<g/>
Istočno	Istočno	k1gNnSc1	Istočno
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgFnSc1d1	ležící
v	v	k7c6	v
Republice	republika	k1gFnSc6	republika
srbské	srbský	k2eAgFnSc6d1	Srbská
<g/>
.	.	kIx.	.
</s>
<s>
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
je	být	k5eAaImIp3nS	být
přirozeným	přirozený	k2eAgInSc7d1	přirozený
politickým	politický	k2eAgInSc7d1	politický
<g/>
,	,	kIx,	,
ekonomickým	ekonomický	k2eAgInSc7d1	ekonomický
<g/>
,	,	kIx,	,
kulturním	kulturní	k2eAgInSc7d1	kulturní
a	a	k8xC	a
dopravním	dopravní	k2eAgInSc7d1	dopravní
centrem	centr	k1gInSc7	centr
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
275	[number]	k4	275
524	[number]	k4	524
obyvateli	obyvatel	k1gMnPc7	obyvatel
(	(	kIx(	(
<g/>
2013	[number]	k4	2013
<g/>
)	)	kIx)	)
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
141,5	[number]	k4	141,5
km2	km2	k4	km2
(	(	kIx(	(
<g/>
s	s	k7c7	s
předměstskými	předměstský	k2eAgFnPc7d1	předměstská
obcemi	obec	k1gFnPc7	obec
přes	přes	k7c4	přes
395	[number]	k4	395
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
i	i	k9	i
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
v	v	k7c6	v
něm	on	k3xPp3gNnSc6	on
Předsednictvo	předsednictvo	k1gNnSc1	předsednictvo
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
<g/>
,	,	kIx,	,
dvoukomorový	dvoukomorový	k2eAgInSc1d1	dvoukomorový
Parlament	parlament	k1gInSc1	parlament
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
<g/>
,	,	kIx,	,
Rada	rada	k1gFnSc1	rada
ministrů	ministr	k1gMnPc2	ministr
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
<g/>
,	,	kIx,	,
Ústavní	ústavní	k2eAgInSc1d1	ústavní
soud	soud	k1gInSc1	soud
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
<g/>
,	,	kIx,	,
Centrální	centrální	k2eAgFnSc1d1	centrální
banka	banka	k1gFnSc1	banka
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
<g/>
,	,	kIx,	,
nejvyšší	vysoký	k2eAgInPc4d3	Nejvyšší
orgány	orgán	k1gInPc4	orgán
Federace	federace	k1gFnSc2	federace
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
a	a	k8xC	a
Kantonu	Kanton	k1gInSc2	Kanton
Sarajevo	Sarajevo	k1gNnSc4	Sarajevo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
byl	být	k5eAaImAgInS	být
prostor	prostor	k1gInSc1	prostor
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Sarajeva	Sarajevo	k1gNnSc2	Sarajevo
tvořen	tvořit	k5eAaImNgInS	tvořit
shlukem	shluk	k1gInSc7	shluk
vesnic	vesnice	k1gFnPc2	vesnice
(	(	kIx(	(
<g/>
se	s	k7c7	s
společným	společný	k2eAgNnSc7d1	společné
tržištěm	tržiště	k1gNnSc7	tržiště
<g/>
)	)	kIx)	)
a	a	k8xC	a
pevností	pevnost	k1gFnSc7	pevnost
Hodidjed	Hodidjed	k1gMnSc1	Hodidjed
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
nacházela	nacházet	k5eAaImAgFnS	nacházet
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gInSc1	jejíž
název	název	k1gInSc1	název
pochází	pocházet	k5eAaImIp3nS	pocházet
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
ze	z	k7c2	z
spojení	spojení	k1gNnSc2	spojení
slov	slovo	k1gNnPc2	slovo
"	"	kIx"	"
<g/>
vrh	vrh	k1gInSc1	vrh
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
kopec	kopec	k1gInSc1	kopec
<g/>
,	,	kIx,	,
hora	hora	k1gFnSc1	hora
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
Bosna	Bosna	k1gFnSc1	Bosna
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
řeka	řeka	k1gFnSc1	řeka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
údolí	údolí	k1gNnSc2	údolí
pramení	pramenit	k5eAaImIp3nS	pramenit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
roku	rok	k1gInSc2	rok
1239	[number]	k4	1239
zde	zde	k6eAd1	zde
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
katedrála	katedrála	k1gFnSc1	katedrála
zasvěcená	zasvěcený	k2eAgFnSc1d1	zasvěcená
sv.	sv.	kA	sv.
Petrovi	Petr	k1gMnSc3	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Vrhbosna	Vrhbosn	k1gInSc2	Vrhbosn
dodnes	dodnes	k6eAd1	dodnes
používá	používat	k5eAaImIp3nS	používat
zdejší	zdejší	k2eAgFnSc1d1	zdejší
katolická	katolický	k2eAgFnSc1d1	katolická
diecéze	diecéze	k1gFnSc1	diecéze
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1263	[number]	k4	1263
tu	ten	k3xDgFnSc4	ten
existovala	existovat	k5eAaImAgFnS	existovat
i	i	k9	i
citadela	citadela	k1gFnSc1	citadela
<g/>
,	,	kIx,	,
zničena	zničit	k5eAaPmNgFnS	zničit
byla	být	k5eAaImAgFnS	být
však	však	k9	však
při	při	k7c6	při
okupaci	okupace	k1gFnSc6	okupace
osmanskými	osmanský	k2eAgNnPc7d1	osmanské
vojsky	vojsko	k1gNnPc7	vojsko
v	v	k7c4	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Osmané	Osman	k1gMnPc1	Osman
získali	získat	k5eAaPmAgMnP	získat
město	město	k1gNnSc4	město
roku	rok	k1gInSc2	rok
1435	[number]	k4	1435
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
turecký	turecký	k2eAgMnSc1d1	turecký
paša	paša	k1gMnSc1	paša
obsadil	obsadit	k5eAaPmAgMnS	obsadit
hrad	hrad	k1gInSc4	hrad
Hođiđed	Hođiđed	k1gInSc4	Hođiđed
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
se	se	k3xPyFc4	se
začíná	začínat	k5eAaImIp3nS	začínat
projevovat	projevovat	k5eAaImF	projevovat
vliv	vliv	k1gInSc4	vliv
z	z	k7c2	z
Istanbulu	Istanbul	k1gInSc2	Istanbul
a	a	k8xC	a
šíří	šířit	k5eAaImIp3nS	šířit
se	se	k3xPyFc4	se
islám	islám	k1gInSc1	islám
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
vzniká	vznikat	k5eAaImIp3nS	vznikat
z	z	k7c2	z
podnětu	podnět	k1gInSc2	podnět
správce	správce	k1gMnSc2	správce
města	město	k1gNnSc2	město
Ishak-bega	Ishakeg	k1gMnSc2	Ishak-beg
Ishakoviće	Ishaković	k1gMnSc2	Ishaković
kryté	krytý	k2eAgNnSc1d1	kryté
tržiště	tržiště	k1gNnSc1	tržiště
(	(	kIx(	(
<g/>
bezistan	bezistan	k1gInSc1	bezistan
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vodovod	vodovod	k1gInSc1	vodovod
<g/>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
vybudovány	vybudovat	k5eAaPmNgInP	vybudovat
i	i	k8xC	i
objekty	objekt	k1gInPc1	objekt
muslimské	muslimský	k2eAgFnSc2d1	muslimská
vakufské	vakufský	k2eAgFnSc2d1	vakufský
nadace	nadace	k1gFnSc2	nadace
a	a	k8xC	a
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1477	[number]	k4	1477
jsou	být	k5eAaImIp3nP	být
přístupné	přístupný	k2eAgInPc1d1	přístupný
i	i	k8xC	i
veřejnosti	veřejnost	k1gFnPc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
tedy	tedy	k9	tedy
základ	základ	k1gInSc1	základ
současného	současný	k2eAgNnSc2d1	současné
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Začátkem	začátkem	k7c2	začátkem
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1507	[number]	k4	1507
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
-	-	kIx~	-
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
i	i	k9	i
doložený	doložený	k2eAgInSc1d1	doložený
-	-	kIx~	-
současný	současný	k2eAgInSc1d1	současný
název	název	k1gInSc1	název
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
<g/>
.	.	kIx.	.
</s>
<s>
Vyvinul	vyvinout	k5eAaPmAgInS	vyvinout
se	se	k3xPyFc4	se
z	z	k7c2	z
tureckých	turecký	k2eAgNnPc2d1	turecké
jmen	jméno	k1gNnPc2	jméno
jako	jako	k9	jako
Saraj-ovasi	Sarajvas	k1gMnPc1	Saraj-ovas
<g/>
,	,	kIx,	,
Bosna-saraj	Bosnaaraj	k1gMnSc1	Bosna-saraj
a	a	k8xC	a
Sarajbosna	Sarajbosna	k1gFnSc1	Sarajbosna
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
slovo	slovo	k1gNnSc1	slovo
saraj	saraj	k1gInSc1	saraj
znamená	znamenat	k5eAaImIp3nS	znamenat
palác	palác	k1gInSc4	palác
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgFnSc1d2	pozdější
slovanská	slovanský	k2eAgFnSc1d1	Slovanská
zkomolenina	zkomolenina	k1gFnSc1	zkomolenina
tohoto	tento	k3xDgInSc2	tento
názvu	název	k1gInSc2	název
vytvořila	vytvořit	k5eAaPmAgFnS	vytvořit
dnešní	dnešní	k2eAgFnSc4d1	dnešní
podobu	podoba	k1gFnSc4	podoba
Sarajevo	Sarajevo	k1gNnSc4	Sarajevo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc4	století
se	s	k7c7	s
správcem	správce	k1gMnSc7	správce
města	město	k1gNnSc2	město
stal	stát	k5eAaPmAgMnS	stát
Gazi	Gaze	k1gFnSc4	Gaze
Husrev-beg	Husreveg	k1gMnSc1	Husrev-beg
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
nechal	nechat	k5eAaPmAgMnS	nechat
postavit	postavit	k5eAaPmF	postavit
mešitu	mešita	k1gFnSc4	mešita
<g/>
,	,	kIx,	,
medresu	medrést	k5eAaPmIp1nS	medrést
(	(	kIx(	(
<g/>
muslimskou	muslimský	k2eAgFnSc4d1	muslimská
střední	střední	k2eAgFnSc4d1	střední
školu	škola	k1gFnSc4	škola
<g/>
)	)	kIx)	)
a	a	k8xC	a
za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
dosáhlo	dosáhnout	k5eAaPmAgNnS	dosáhnout
město	město	k1gNnSc1	město
hranice	hranice	k1gFnSc2	hranice
50	[number]	k4	50
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
Sarajeva	Sarajevo	k1gNnSc2	Sarajevo
přemístěno	přemístěn	k2eAgNnSc1d1	přemístěno
i	i	k8xC	i
sídlo	sídlo	k1gNnSc1	sídlo
osmanského	osmanský	k2eAgMnSc2d1	osmanský
správce	správce	k1gMnSc2	správce
(	(	kIx(	(
<g/>
vezíra	vezír	k1gMnSc2	vezír
<g/>
,	,	kIx,	,
beglerbega	beglerbeg	k1gMnSc2	beglerbeg
<g/>
,	,	kIx,	,
valího	valí	k1gMnSc2	valí
<g/>
)	)	kIx)	)
z	z	k7c2	z
Travniku	Travnik	k1gInSc2	Travnik
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1697	[number]	k4	1697
však	však	k9	však
rozvoj	rozvoj	k1gInSc1	rozvoj
rychle	rychle	k6eAd1	rychle
ustal	ustat	k5eAaPmAgInS	ustat
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
vyplenila	vyplenit	k5eAaPmAgNnP	vyplenit
rakouská	rakouský	k2eAgNnPc1d1	rakouské
vojska	vojsko	k1gNnPc1	vojsko
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Evžena	Evžen	k1gMnSc2	Evžen
Savojského	savojský	k2eAgMnSc2d1	savojský
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
bylo	být	k5eAaImAgNnS	být
obnoveno	obnovit	k5eAaPmNgNnS	obnovit
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
nikdy	nikdy	k6eAd1	nikdy
nedosáhlo	dosáhnout	k5eNaPmAgNnS	dosáhnout
takové	takový	k3xDgFnPc4	takový
prosperity	prosperita	k1gFnPc4	prosperita
jako	jako	k9	jako
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
ztratilo	ztratit	k5eAaPmAgNnS	ztratit
statut	statut	k1gInSc4	statut
centra	centrum	k1gNnSc2	centrum
ejáletu	ejálet	k1gInSc2	ejálet
-	-	kIx~	-
jež	jenž	k3xRgNnSc1	jenž
bylo	být	k5eAaImAgNnS	být
přesunuto	přesunout	k5eAaPmNgNnS	přesunout
do	do	k7c2	do
Travniku	Travnik	k1gInSc2	Travnik
<g/>
.	.	kIx.	.
</s>
<s>
Osmané	Osman	k1gMnPc1	Osman
vládnoucí	vládnoucí	k2eAgFnSc2d1	vládnoucí
městu	město	k1gNnSc3	město
si	se	k3xPyFc3	se
však	však	k9	však
uvědomovali	uvědomovat	k5eAaImAgMnP	uvědomovat
blízkost	blízkost	k1gFnSc4	blízkost
nedalekého	daleký	k2eNgNnSc2d1	nedaleké
Rakouska	Rakousko	k1gNnSc2	Rakousko
a	a	k8xC	a
Uher	Uhry	k1gFnPc2	Uhry
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
nechali	nechat	k5eAaPmAgMnP	nechat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1729	[number]	k4	1729
vybudovat	vybudovat	k5eAaPmF	vybudovat
městské	městský	k2eAgNnSc1d1	Městské
opevnění	opevnění	k1gNnSc1	opevnění
a	a	k8xC	a
na	na	k7c6	na
kopci	kopec	k1gInSc6	kopec
nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
pevnost	pevnost	k1gFnSc1	pevnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
vládě	vláda	k1gFnSc6	vláda
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
pomalu	pomalu	k6eAd1	pomalu
začaly	začít	k5eAaPmAgFnP	začít
objevovat	objevovat	k5eAaImF	objevovat
první	první	k4xOgFnPc1	první
trhliny	trhlina	k1gFnPc1	trhlina
a	a	k8xC	a
náznaky	náznak	k1gInPc1	náznak
úpadku	úpadek	k1gInSc2	úpadek
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1827	[number]	k4	1827
se	se	k3xPyFc4	se
vzbouřili	vzbouřit	k5eAaPmAgMnP	vzbouřit
janičáři	janičár	k1gMnPc1	janičár
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
objevovaly	objevovat	k5eAaImAgFnP	objevovat
i	i	k9	i
další	další	k2eAgFnPc1d1	další
vzpoury	vzpoura	k1gFnPc1	vzpoura
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Huseina-kapetana	Huseinaapetan	k1gMnSc2	Huseina-kapetan
Gradaščeviće	Gradaščević	k1gMnSc2	Gradaščević
(	(	kIx(	(
<g/>
1831	[number]	k4	1831
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
požadoval	požadovat	k5eAaImAgMnS	požadovat
pro	pro	k7c4	pro
Bosnu	Bosna	k1gFnSc4	Bosna
autonomii	autonomie	k1gFnSc4	autonomie
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1842	[number]	k4	1842
tu	ten	k3xDgFnSc4	ten
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
i	i	k9	i
první	první	k4xOgFnSc1	první
pošta	pošta	k1gFnSc1	pošta
a	a	k8xC	a
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1850	[number]	k4	1850
a	a	k8xC	a
1878	[number]	k4	1878
zde	zde	k6eAd1	zde
sídlil	sídlit	k5eAaImAgInS	sídlit
též	též	k9	též
i	i	k9	i
turecký	turecký	k2eAgMnSc1d1	turecký
místodržitel	místodržitel	k1gMnSc1	místodržitel
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1878	[number]	k4	1878
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
Berlínském	berlínský	k2eAgInSc6d1	berlínský
kongresu	kongres	k1gInSc6	kongres
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bosna	Bosna	k1gFnSc1	Bosna
a	a	k8xC	a
Hercegovina	Hercegovina	k1gFnSc1	Hercegovina
bude	být	k5eAaImBp3nS	být
obsazena	obsadit	k5eAaPmNgFnS	obsadit
Rakousko-Uherskem	Rakousko-Uhersek	k1gMnSc7	Rakousko-Uhersek
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
také	také	k9	také
i	i	k9	i
stalo	stát	k5eAaPmAgNnS	stát
<g/>
;	;	kIx,	;
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
do	do	k7c2	do
města	město	k1gNnSc2	město
vstoupily	vstoupit	k5eAaPmAgFnP	vstoupit
rakouské	rakouský	k2eAgFnPc1d1	rakouská
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Bosna	Bosna	k1gFnSc1	Bosna
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
protektorátem	protektorát	k1gInSc7	protektorát
císařství	císařství	k1gNnSc1	císařství
a	a	k8xC	a
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
její	její	k3xOp3gFnSc2	její
metropolí	metropol	k1gFnPc2	metropol
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
Sarajeva	Sarajevo	k1gNnSc2	Sarajevo
vnímalo	vnímat	k5eAaImAgNnS	vnímat
novou	nový	k2eAgFnSc4d1	nová
nadvládu	nadvláda	k1gFnSc4	nadvláda
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
nelibostí	nelibost	k1gFnSc7	nelibost
<g/>
;	;	kIx,	;
především	především	k9	především
díky	díky	k7c3	díky
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rakousko	Rakousko	k1gNnSc1	Rakousko
bylo	být	k5eAaImAgNnS	být
dlouhodobým	dlouhodobý	k2eAgMnSc7d1	dlouhodobý
nepřítelem	nepřítel	k1gMnSc7	nepřítel
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
vnímané	vnímaný	k2eAgFnSc6d1	vnímaná
navíc	navíc	k6eAd1	navíc
jako	jako	k9	jako
centrum	centrum	k1gNnSc1	centrum
katolického	katolický	k2eAgInSc2d1	katolický
vlivu	vliv	k1gInSc2	vliv
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
obsazení	obsazení	k1gNnSc6	obsazení
Rakousko-Uherskem	Rakousko-Uhersek	k1gInSc7	Rakousko-Uhersek
se	se	k3xPyFc4	se
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
začalo	začít	k5eAaPmAgNnS	začít
výrazně	výrazně	k6eAd1	výrazně
architektonicky	architektonicky	k6eAd1	architektonicky
měnit	měnit	k5eAaImF	měnit
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
města	město	k1gNnSc2	město
orientálního	orientální	k2eAgNnSc2d1	orientální
v	v	k7c4	v
město	město	k1gNnSc4	město
evropského	evropský	k2eAgInSc2d1	evropský
stylu	styl	k1gInSc2	styl
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
zásluhu	zásluha	k1gFnSc4	zásluha
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
přeměně	přeměna	k1gFnSc6	přeměna
měl	mít	k5eAaImAgMnS	mít
architekt	architekt	k1gMnSc1	architekt
českého	český	k2eAgInSc2d1	český
původu	původ	k1gInSc2	původ
Karel	Karel	k1gMnSc1	Karel
Pařík	Pařík	k1gMnSc1	Pařík
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1884	[number]	k4	1884
<g/>
-	-	kIx~	-
<g/>
1939	[number]	k4	1939
vyprojektoval	vyprojektovat	k5eAaPmAgInS	vyprojektovat
většinu	většina	k1gFnSc4	většina
městotvorných	městotvorný	k2eAgFnPc2d1	městotvorná
staveb	stavba	k1gFnPc2	stavba
v	v	k7c6	v
Sarajevu	Sarajevo	k1gNnSc6	Sarajevo
-	-	kIx~	-
Sarajevskou	sarajevský	k2eAgFnSc4d1	Sarajevská
knihovnu	knihovna	k1gFnSc4	knihovna
(	(	kIx(	(
<g/>
původně	původně	k6eAd1	původně
radnici	radnice	k1gFnSc4	radnice
<g/>
,	,	kIx,	,
známou	známý	k2eAgFnSc4d1	známá
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Vijećnica	Vijećnic	k1gInSc2	Vijećnic
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Zemské	zemský	k2eAgNnSc4d1	zemské
muzeum	muzeum	k1gNnSc4	muzeum
<g/>
,	,	kIx,	,
Katedrálu	katedrála	k1gFnSc4	katedrála
srdce	srdce	k1gNnSc2	srdce
Ježíšova	Ježíšův	k2eAgNnSc2d1	Ježíšovo
(	(	kIx(	(
<g/>
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Josipem	Josip	k1gMnSc7	Josip
Vancašem	Vancaš	k1gMnSc7	Vancaš
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
věznici	věznice	k1gFnSc4	věznice
<g/>
,	,	kIx,	,
Aškenázskou	Aškenázský	k2eAgFnSc4d1	Aškenázská
synagogu	synagoga	k1gFnSc4	synagoga
<g/>
,	,	kIx,	,
Šeriatskou	Šeriatský	k2eAgFnSc4d1	Šeriatský
školu	škola	k1gFnSc4	škola
a	a	k8xC	a
desítky	desítka	k1gFnPc4	desítka
dalších	další	k2eAgFnPc2d1	další
staveb	stavba	k1gFnPc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vzhledu	vzhled	k1gInSc6	vzhled
moderního	moderní	k2eAgNnSc2d1	moderní
Sarajeva	Sarajevo	k1gNnSc2	Sarajevo
se	se	k3xPyFc4	se
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
19	[number]	k4	19
<g/>
.	.	kIx.	.
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
výrazně	výrazně	k6eAd1	výrazně
podepsalo	podepsat	k5eAaPmAgNnS	podepsat
i	i	k9	i
několik	několik	k4yIc1	několik
dalších	další	k2eAgMnPc2d1	další
českých	český	k2eAgMnPc2d1	český
architektů	architekt	k1gMnPc2	architekt
-	-	kIx~	-
např.	např.	kA	např.
František	František	k1gMnSc1	František
Blažek	Blažek	k1gMnSc1	Blažek
<g/>
,	,	kIx,	,
Karel	Karel	k1gMnSc1	Karel
Pánek	Pánek	k1gMnSc1	Pánek
<g/>
,	,	kIx,	,
Marian	Marian	k1gMnSc1	Marian
Pařík	Pařík	k1gMnSc1	Pařík
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
Karla	Karel	k1gMnSc2	Karel
Paříka	Pařík	k1gMnSc2	Pařík
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Alexander	Alexandra	k1gFnPc2	Alexandra
Wittek	Wittka	k1gFnPc2	Wittka
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Kotěra	Kotěra	k1gFnSc1	Kotěra
<g/>
,	,	kIx,	,
Bedřich	Bedřich	k1gMnSc1	Bedřich
Hacar	Hacar	k1gMnSc1	Hacar
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Pospíšil	Pospíšil	k1gMnSc1	Pospíšil
a	a	k8xC	a
další	další	k2eAgMnSc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
rakousko-uherského	rakouskoherský	k2eAgInSc2d1	rakousko-uherský
protektorátu	protektorát	k1gInSc2	protektorát
nastal	nastat	k5eAaPmAgInS	nastat
oproti	oproti	k7c3	oproti
předchozí	předchozí	k2eAgFnSc3d1	předchozí
stagnaci	stagnace	k1gFnSc3	stagnace
rychlý	rychlý	k2eAgInSc4d1	rychlý
rozvoj	rozvoj	k1gInSc4	rozvoj
-	-	kIx~	-
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
továrny	továrna	k1gFnPc1	továrna
<g/>
,	,	kIx,	,
školy	škola	k1gFnPc1	škola
<g/>
,	,	kIx,	,
železnice	železnice	k1gFnSc1	železnice
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
zavedena	zavést	k5eAaPmNgFnS	zavést
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Začalo	začít	k5eAaPmAgNnS	začít
se	se	k3xPyFc4	se
psát	psát	k5eAaImF	psát
latinkou	latinka	k1gFnSc7	latinka
namísto	namísto	k7c2	namísto
arabicí	arabice	k1gFnPc2	arabice
(	(	kIx(	(
<g/>
bosenskou	bosenský	k2eAgFnSc7d1	bosenská
variantou	varianta	k1gFnSc7	varianta
arabského	arabský	k2eAgNnSc2d1	arabské
písma	písmo	k1gNnSc2	písmo
<g/>
)	)	kIx)	)
a	a	k8xC	a
cyrilice	cyrilice	k1gFnSc1	cyrilice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1882	[number]	k4	1882
získalo	získat	k5eAaPmAgNnS	získat
město	město	k1gNnSc1	město
železniční	železniční	k2eAgNnSc1d1	železniční
spojení	spojení	k1gNnSc1	spojení
<g/>
.	.	kIx.	.
</s>
<s>
Růst	růst	k1gInSc1	růst
města	město	k1gNnSc2	město
byl	být	k5eAaImAgInS	být
natolik	natolik	k6eAd1	natolik
dramatický	dramatický	k2eAgInSc4d1	dramatický
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1904	[number]	k4	1904
fungovalo	fungovat	k5eAaImAgNnS	fungovat
v	v	k7c6	v
Sarajevu	Sarajevo	k1gNnSc6	Sarajevo
celkem	celkem	k6eAd1	celkem
9	[number]	k4	9
cihelen	cihelna	k1gFnPc2	cihelna
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
začal	začít	k5eAaPmAgInS	začít
pozvolna	pozvolna	k6eAd1	pozvolna
růst	růst	k1gInSc1	růst
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1879	[number]	k4	1879
mělo	mít	k5eAaImAgNnS	mít
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
21	[number]	k4	21
337	[number]	k4	337
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1895	[number]	k4	1895
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
již	již	k6eAd1	již
38	[number]	k4	38
083	[number]	k4	083
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
51	[number]	k4	51
919	[number]	k4	919
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
nárůst	nárůst	k1gInSc1	nárůst
však	však	k9	však
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
odrážel	odrážet	k5eAaImAgMnS	odrážet
nemalý	malý	k2eNgInSc4d1	nemalý
počet	počet	k1gInSc4	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
zbytku	zbytek	k1gInSc2	zbytek
Rakousko-Uherska	Rakousko-Uhersk	k1gInSc2	Rakousko-Uhersk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
cestovali	cestovat	k5eAaImAgMnP	cestovat
do	do	k7c2	do
Bosny	Bosna	k1gFnSc2	Bosna
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
zemi	zem	k1gFnSc6	zem
zmodernizovat	zmodernizovat	k5eAaPmF	zmodernizovat
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1879	[number]	k4	1879
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
většinově	většinově	k6eAd1	většinově
muslimské	muslimský	k2eAgFnPc4d1	muslimská
<g/>
;	;	kIx,	;
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1910	[number]	k4	1910
byl	být	k5eAaImAgInS	být
podíl	podíl	k1gInSc1	podíl
muslimského	muslimský	k2eAgNnSc2d1	muslimské
a	a	k8xC	a
katolického	katolický	k2eAgNnSc2d1	katolické
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
navštívil	navštívit	k5eAaPmAgInS	navštívit
roku	rok	k1gInSc2	rok
1910	[number]	k4	1910
i	i	k8xC	i
císař	císař	k1gMnSc1	císař
František	František	k1gMnSc1	František
Josef	Josef	k1gMnSc1	Josef
I.	I.	kA	I.
V	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
také	také	k6eAd1	také
zasedal	zasedat	k5eAaImAgInS	zasedat
i	i	k9	i
první	první	k4xOgInSc1	první
bosenský	bosenský	k2eAgInSc1d1	bosenský
parlament	parlament	k1gInSc1	parlament
-	-	kIx~	-
Sabor	sabor	k1gInSc1	sabor
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
další	další	k2eAgInPc4d1	další
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
tu	tu	k6eAd1	tu
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
atentátu	atentát	k1gInSc3	atentát
na	na	k7c4	na
Františka	František	k1gMnSc4	František
Ferdinanda	Ferdinand	k1gMnSc4	Ferdinand
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Este	Est	k1gMnSc5	Est
<g/>
,	,	kIx,	,
historické	historický	k2eAgFnPc4d1	historická
události	událost	k1gFnPc4	událost
mezinárodního	mezinárodní	k2eAgInSc2d1	mezinárodní
významu	význam	k1gInSc2	význam
<g/>
.	.	kIx.	.
28	[number]	k4	28
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1914	[number]	k4	1914
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
konci	konec	k1gInSc6	konec
mostu	most	k1gInSc2	most
Latinska	Latinsko	k1gNnSc2	Latinsko
ćuprija	ćuprij	k1gInSc2	ćuprij
u	u	k7c2	u
budovy	budova	k1gFnSc2	budova
knihovny	knihovna	k1gFnSc2	knihovna
postřelen	postřelen	k2eAgMnSc1d1	postřelen
následník	následník	k1gMnSc1	následník
Rakousko-Uherského	rakouskoherský	k2eAgInSc2d1	rakousko-uherský
trůnu	trůn	k1gInSc2	trůn
František	František	k1gMnSc1	František
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Este	Est	k1gMnSc5	Est
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
týž	týž	k3xTgInSc4	týž
den	den	k1gInSc4	den
zraněním	zranění	k1gNnSc7	zranění
podlehl	podlehnout	k5eAaPmAgInS	podlehnout
<g/>
.	.	kIx.	.
</s>
<s>
Útočníkem	útočník	k1gMnSc7	útočník
byl	být	k5eAaImAgMnS	být
srbský	srbský	k2eAgMnSc1d1	srbský
nacionalista	nacionalista	k1gMnSc1	nacionalista
Gavrilo	Gavrila	k1gFnSc5	Gavrila
Princip	princip	k1gInSc1	princip
<g/>
.	.	kIx.	.
</s>
<s>
Událost	událost	k1gFnSc1	událost
dala	dát	k5eAaPmAgFnS	dát
podnět	podnět	k1gInSc4	podnět
k	k	k7c3	k
první	první	k4xOgFnSc3	první
světové	světový	k2eAgFnSc3d1	světová
válce	válka	k1gFnSc3	válka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
městě	město	k1gNnSc6	město
začaly	začít	k5eAaPmAgFnP	začít
první	první	k4xOgFnSc7	první
okupační	okupační	k2eAgFnSc7d1	okupační
správou	správa	k1gFnSc7	správa
řízené	řízený	k2eAgFnSc2d1	řízená
násilnosti	násilnost	k1gFnSc2	násilnost
mezi	mezi	k7c7	mezi
tamějšími	tamější	k2eAgMnPc7d1	tamější
Srby	Srb	k1gMnPc7	Srb
<g/>
,	,	kIx,	,
Bosňáky	Bosňáky	k?	Bosňáky
a	a	k8xC	a
Chorvaty	Chorvat	k1gMnPc7	Chorvat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
obzvláště	obzvláště	k6eAd1	obzvláště
brutální	brutální	k2eAgFnSc1d1	brutální
<g/>
,	,	kIx,	,
a	a	k8xC	a
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
Království	království	k1gNnSc1	království
Srbů	Srb	k1gMnPc2	Srb
<g/>
,	,	kIx,	,
Chorvatů	Chorvat	k1gMnPc2	Chorvat
a	a	k8xC	a
Slovinců	Slovinec	k1gMnPc2	Slovinec
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
uklidnila	uklidnit	k5eAaPmAgFnS	uklidnit
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
i	i	k8xC	i
centrem	centrum	k1gNnSc7	centrum
tzv.	tzv.	kA	tzv.
Drinské	Drinská	k1gFnSc2	Drinská
bánoviny	bánovina	k1gFnSc2	bánovina
<g/>
,	,	kIx,	,
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
devíti	devět	k4xCc2	devět
administrativních	administrativní	k2eAgFnPc2d1	administrativní
jednotek	jednotka	k1gFnPc2	jednotka
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
královské	královský	k2eAgFnSc2d1	královská
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
nerespektovaly	respektovat	k5eNaImAgFnP	respektovat
historické	historický	k2eAgFnPc1d1	historická
a	a	k8xC	a
národnostní	národnostní	k2eAgFnPc1d1	národnostní
hranice	hranice	k1gFnPc1	hranice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
mezi	mezi	k7c7	mezi
60	[number]	k4	60
a	a	k8xC	a
70	[number]	k4	70
tisíci	tisíc	k4xCgInPc7	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Radikální	radikální	k2eAgFnSc4d1	radikální
změnu	změna	k1gFnSc4	změna
politického	politický	k2eAgNnSc2d1	politické
uspořádání	uspořádání	k1gNnSc2	uspořádání
přinesla	přinést	k5eAaPmAgFnS	přinést
až	až	k9	až
Druhá	druhý	k4xOgFnSc1	druhý
světová	světový	k2eAgFnSc1d1	světová
válka	válka	k1gFnSc1	válka
<g/>
.	.	kIx.	.
</s>
<s>
Království	království	k1gNnSc1	království
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
se	se	k3xPyFc4	se
rozpadlo	rozpadnout	k5eAaPmAgNnS	rozpadnout
během	během	k7c2	během
invaze	invaze	k1gFnSc2	invaze
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
na	na	k7c6	na
území	území	k1gNnSc6	území
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
a	a	k8xC	a
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
fašistický	fašistický	k2eAgInSc1d1	fašistický
Nezávislý	závislý	k2eNgInSc1d1	nezávislý
stát	stát	k1gInSc1	stát
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
tzv.	tzv.	kA	tzv.
<g/>
Ustašovců	ustašovec	k1gMnPc2	ustašovec
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
systematickému	systematický	k2eAgNnSc3d1	systematické
vyvražďování	vyvražďování	k1gNnSc3	vyvražďování
hlavně	hlavně	k6eAd1	hlavně
Srbů	Srb	k1gMnPc2	Srb
a	a	k8xC	a
Židů	Žid	k1gMnPc2	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
Židé	Žid	k1gMnPc1	Žid
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
též	též	k9	též
Romové	Rom	k1gMnPc1	Rom
a	a	k8xC	a
další	další	k2eAgMnPc1d1	další
označovaní	označovaný	k2eAgMnPc1d1	označovaný
jako	jako	k8xC	jako
podlidé	podčlověk	k1gMnPc1	podčlověk
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
odváženi	odvážit	k5eAaPmNgMnP	odvážit
do	do	k7c2	do
koncentračních	koncentrační	k2eAgInPc2d1	koncentrační
táborů	tábor	k1gInPc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
Chorvatští	chorvatský	k2eAgMnPc1d1	chorvatský
fašisté	fašista	k1gMnPc1	fašista
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
muslimské	muslimský	k2eAgFnSc3d1	muslimská
Bosňáky	Bosňáky	k?	Bosňáky
zahrnout	zahrnout	k5eAaPmF	zahrnout
do	do	k7c2	do
svého	svůj	k3xOyFgInSc2	svůj
národního	národní	k2eAgInSc2d1	národní
korpusu	korpus	k1gInSc2	korpus
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
tak	tak	k6eAd1	tak
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
navýšení	navýšení	k1gNnSc4	navýšení
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
svého	svůj	k3xOyFgInSc2	svůj
vlastního	vlastní	k2eAgInSc2d1	vlastní
národa	národ	k1gInSc2	národ
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
formou	forma	k1gFnSc7	forma
provokací	provokace	k1gFnPc2	provokace
snažili	snažit	k5eAaImAgMnP	snažit
vyostřit	vyostřit	k5eAaPmF	vyostřit
jejich	jejich	k3xOp3gInPc4	jejich
vztahy	vztah	k1gInPc4	vztah
s	s	k7c7	s
bosenskými	bosenský	k2eAgMnPc7d1	bosenský
Srby	Srb	k1gMnPc7	Srb
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
procházela	procházet	k5eAaImAgFnS	procházet
Sarajevem	Sarajevo	k1gNnSc7	Sarajevo
hranice	hranice	k1gFnSc2	hranice
sfér	sféra	k1gFnPc2	sféra
vlivu	vliv	k1gInSc2	vliv
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
Itálie	Itálie	k1gFnSc2	Itálie
v	v	k7c6	v
Nezávislém	závislý	k2eNgInSc6d1	nezávislý
státu	stát	k1gInSc6	stát
Chorvatsko	Chorvatsko	k1gNnSc1	Chorvatsko
<g/>
.	.	kIx.	.
</s>
<s>
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
bylo	být	k5eAaImAgNnS	být
osvobozeno	osvobodit	k5eAaPmNgNnS	osvobodit
komunistickým	komunistický	k2eAgInSc7d1	komunistický
odbojem	odboj	k1gInSc7	odboj
5	[number]	k4	5
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1945	[number]	k4	1945
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
osvobození	osvobození	k1gNnSc6	osvobození
města	město	k1gNnSc2	město
jugoslávskými	jugoslávský	k2eAgInPc7d1	jugoslávský
partyzány	partyzána	k1gFnPc1	partyzána
a	a	k8xC	a
ustanovení	ustanovení	k1gNnPc1	ustanovení
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
Josipem	Josip	k1gMnSc7	Josip
Brozem	Broz	k1gMnSc7	Broz
Titem	Tit	k1gMnSc7	Tit
jako	jako	k8xS	jako
socialistického	socialistický	k2eAgInSc2d1	socialistický
federativního	federativní	k2eAgInSc2d1	federativní
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
opět	opět	k6eAd1	opět
centrem	centrum	k1gNnSc7	centrum
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
mělo	mít	k5eAaImAgNnS	mít
108	[number]	k4	108
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
příchodů	příchod	k1gInPc2	příchod
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
museli	muset	k5eAaImAgMnP	muset
město	město	k1gNnSc4	město
kvůli	kvůli	k7c3	kvůli
válce	válka	k1gFnSc3	válka
opustit	opustit	k5eAaPmF	opustit
přitahovalo	přitahovat	k5eAaImAgNnS	přitahovat
Sarajevo	Sarajevo	k1gNnSc4	Sarajevo
další	další	k2eAgNnSc4d1	další
obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
především	především	k9	především
díky	díky	k7c3	díky
překotné	překotný	k2eAgFnSc3d1	překotná
industrializaci	industrializace	k1gFnSc3	industrializace
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterou	který	k3yQgFnSc7	který
počítal	počítat	k5eAaImAgInS	počítat
první	první	k4xOgInSc1	první
pětiletý	pětiletý	k2eAgInSc1d1	pětiletý
plán	plán	k1gInSc1	plán
a	a	k8xC	a
výstavba	výstavba	k1gFnSc1	výstavba
nových	nový	k2eAgFnPc2d1	nová
továren	továrna	k1gFnPc2	továrna
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c7	za
pomocí	pomoc	k1gFnSc7	pomoc
československých	československý	k2eAgMnPc2d1	československý
odborníků	odborník	k1gMnPc2	odborník
byl	být	k5eAaImAgInS	být
vypracován	vypracován	k2eAgInSc1d1	vypracován
nový	nový	k2eAgInSc4d1	nový
urbanistický	urbanistický	k2eAgInSc4d1	urbanistický
plán	plán	k1gInSc4	plán
metropole	metropol	k1gFnSc2	metropol
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
pozici	pozice	k1gFnSc6	pozice
metropole	metropol	k1gFnSc2	metropol
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
méně	málo	k6eAd2	málo
rozvinutých	rozvinutý	k2eAgFnPc2d1	rozvinutá
republik	republika	k1gFnPc2	republika
Jugoslávie	Jugoslávie	k1gFnSc1	Jugoslávie
bylo	být	k5eAaImAgNnS	být
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
dotováno	dotovat	k5eAaBmNgNnS	dotovat
federálními	federální	k2eAgInPc7d1	federální
zdroji	zdroj	k1gInPc7	zdroj
<g/>
,	,	kIx,	,
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
i	i	k9	i
zastavěná	zastavěný	k2eAgFnSc1d1	zastavěná
plocha	plocha	k1gFnSc1	plocha
rostly	růst	k5eAaImAgInP	růst
<g/>
.	.	kIx.	.
</s>
<s>
Vystavěna	vystavěn	k2eAgFnSc1d1	vystavěna
byla	být	k5eAaImAgNnP	být
panelová	panelový	k2eAgNnPc1d1	panelové
sídliště	sídliště	k1gNnPc1	sídliště
<g/>
,	,	kIx,	,
továrny	továrna	k1gFnPc1	továrna
<g/>
,	,	kIx,	,
muzea	muzeum	k1gNnPc1	muzeum
a	a	k8xC	a
školy	škola	k1gFnPc1	škola
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1961	[number]	k4	1961
bylo	být	k5eAaImAgNnS	být
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
domovem	domov	k1gInSc7	domov
pro	pro	k7c4	pro
142	[number]	k4	142
423	[number]	k4	423
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
o	o	k7c4	o
dvacet	dvacet	k4xCc4	dvacet
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
již	již	k6eAd1	již
448	[number]	k4	448
500	[number]	k4	500
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
provinčního	provinční	k2eAgNnSc2d1	provinční
města	město	k1gNnSc2	město
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
stalo	stát	k5eAaPmAgNnS	stát
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
významných	významný	k2eAgNnPc2d1	významné
sídel	sídlo	k1gNnPc2	sídlo
na	na	k7c6	na
Balkáně	Balkán	k1gInSc6	Balkán
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
událostí	událost	k1gFnSc7	událost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
udávala	udávat	k5eAaImAgFnS	udávat
tón	tón	k1gInSc4	tón
v	v	k7c6	v
Sarajevu	Sarajevo	k1gNnSc6	Sarajevo
přelomu	přelom	k1gInSc2	přelom
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
první	první	k4xOgFnSc2	první
poloviny	polovina	k1gFnSc2	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
</s>
<s>
Zimní	zimní	k2eAgFnSc2d1	zimní
olympijské	olympijský	k2eAgFnSc2d1	olympijská
hry	hra	k1gFnSc2	hra
zahájené	zahájený	k2eAgFnSc2d1	zahájená
15	[number]	k4	15
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
nim	on	k3xPp3gFnPc3	on
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
Sarajeva	Sarajevo	k1gNnSc2	Sarajevo
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
okolí	okolí	k1gNnSc4	okolí
investováno	investován	k2eAgNnSc4d1	investováno
nemalé	malý	k2eNgNnSc4d1	nemalé
množství	množství	k1gNnSc4	množství
finančních	finanční	k2eAgInPc2d1	finanční
prostředků	prostředek	k1gInPc2	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Především	především	k6eAd1	především
byly	být	k5eAaImAgInP	být
dokončeny	dokončit	k5eAaPmNgInP	dokončit
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
rozestavěné	rozestavěný	k2eAgInPc1d1	rozestavěný
projekty	projekt	k1gInPc1	projekt
a	a	k8xC	a
zmodernizována	zmodernizován	k2eAgFnSc1d1	zmodernizována
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
80	[number]	k4	80
<g/>
.	.	kIx.	.
a	a	k8xC	a
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
tu	tu	k6eAd1	tu
žilo	žít	k5eAaImAgNnS	žít
již	již	k6eAd1	již
podle	podle	k7c2	podle
sčítání	sčítání	k1gNnSc2	sčítání
lidu	lid	k1gInSc2	lid
429	[number]	k4	429
672	[number]	k4	672
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Existovaly	existovat	k5eAaImAgInP	existovat
i	i	k9	i
plány	plán	k1gInPc1	plán
na	na	k7c4	na
výstavbu	výstavba	k1gFnSc4	výstavba
podzemní	podzemní	k2eAgFnSc2d1	podzemní
dráhy	dráha	k1gFnSc2	dráha
<g/>
.	.	kIx.	.
</s>
<s>
Avšak	avšak	k8xC	avšak
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
Jugoslávie	Jugoslávie	k1gFnSc1	Jugoslávie
spíše	spíše	k9	spíše
ve	v	k7c6	v
stagnaci	stagnace	k1gFnSc6	stagnace
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
hlavní	hlavní	k2eAgNnSc1d1	hlavní
centrum	centrum	k1gNnSc1	centrum
průmyslové	průmyslový	k2eAgFnSc2d1	průmyslová
výroby	výroba	k1gFnSc2	výroba
republiky	republika	k1gFnSc2	republika
bylo	být	k5eAaImAgNnS	být
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
postiženo	postihnout	k5eAaPmNgNnS	postihnout
ekonomickou	ekonomický	k2eAgFnSc7d1	ekonomická
krizí	krize	k1gFnSc7	krize
značně	značně	k6eAd1	značně
<g/>
.	.	kIx.	.
</s>
<s>
Jen	jen	k9	jen
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
došlo	dojít	k5eAaPmAgNnS	dojít
masové	masový	k2eAgFnSc3d1	masová
stávce	stávka	k1gFnSc3	stávka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
do	do	k7c2	do
Sarajeva	Sarajevo	k1gNnSc2	Sarajevo
sjeli	sjet	k5eAaPmAgMnP	sjet
dělníci	dělník	k1gMnPc1	dělník
z	z	k7c2	z
několika	několik	k4yIc2	několik
dalších	další	k2eAgNnPc2d1	další
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
Protiválečná	protiválečný	k2eAgFnSc1d1	protiválečná
demonstrace	demonstrace	k1gFnSc1	demonstrace
v	v	k7c6	v
Sarajevu	Sarajevo	k1gNnSc6	Sarajevo
1992	[number]	k4	1992
a	a	k8xC	a
Obléhání	obléhání	k1gNnSc4	obléhání
Sarajeva	Sarajevo	k1gNnSc2	Sarajevo
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
vnitřních	vnitřní	k2eAgInPc2d1	vnitřní
i	i	k8xC	i
vnějších	vnější	k2eAgInPc2d1	vnější
vlivů	vliv	k1gInPc2	vliv
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
vyhlášení	vyhlášení	k1gNnSc3	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
3	[number]	k4	3
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1992	[number]	k4	1992
a	a	k8xC	a
vytvoření	vytvoření	k1gNnSc4	vytvoření
nezávislého	závislý	k2eNgInSc2d1	nezávislý
státu	stát	k1gInSc2	stát
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
Chorvatska	Chorvatsko	k1gNnSc2	Chorvatsko
<g/>
.	.	kIx.	.
</s>
<s>
Bělehrad	Bělehrad	k1gInSc4	Bělehrad
referendum	referendum	k1gNnSc1	referendum
o	o	k7c6	o
nezávislosti	nezávislost	k1gFnSc6	nezávislost
neuznal	uznat	k5eNaPmAgMnS	uznat
a	a	k8xC	a
odvolal	odvolat	k5eAaPmAgMnS	odvolat
se	se	k3xPyFc4	se
na	na	k7c6	na
ústavu	ústav	k1gInSc6	ústav
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
území	území	k1gNnSc4	území
socialistické	socialistický	k2eAgFnSc2d1	socialistická
republiky	republika	k1gFnSc2	republika
v	v	k7c6	v
případě	případ	k1gInSc6	případ
ohrožení	ohrožení	k1gNnSc4	ohrožení
bráněno	bránit	k5eAaImNgNnS	bránit
silou	síla	k1gFnSc7	síla
(	(	kIx(	(
<g/>
toto	tento	k3xDgNnSc1	tento
stanovisko	stanovisko	k1gNnSc1	stanovisko
získalo	získat	k5eAaPmAgNnS	získat
rychle	rychle	k6eAd1	rychle
podporu	podpora	k1gFnSc4	podpora
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
bosenských	bosenský	k2eAgMnPc2d1	bosenský
Srbů	Srb	k1gMnPc2	Srb
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
probíhající	probíhající	k2eAgFnSc1d1	probíhající
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Chorvatsku	Chorvatsko	k1gNnSc6	Chorvatsko
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
přenesla	přenést	k5eAaPmAgFnS	přenést
i	i	k9	i
na	na	k7c6	na
území	území	k1gNnSc6	území
BaH	bah	k0	bah
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
do	do	k7c2	do
Sarajeva	Sarajevo	k1gNnSc2	Sarajevo
<g/>
.	.	kIx.	.
</s>
<s>
Okamžitě	okamžitě	k6eAd1	okamžitě
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
vznikaly	vznikat	k5eAaImAgFnP	vznikat
první	první	k4xOgFnPc4	první
paravojenské	paravojenský	k2eAgFnPc4d1	paravojenská
jednotky	jednotka	k1gFnPc4	jednotka
<g/>
.	.	kIx.	.
</s>
<s>
Situaci	situace	k1gFnSc4	situace
vyostřovaly	vyostřovat	k5eAaImAgFnP	vyostřovat
především	především	k9	především
události	událost	k1gFnPc1	událost
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Bosně	Bosna	k1gFnSc6	Bosna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
prvním	první	k4xOgNnPc3	první
případům	případ	k1gInPc3	případ
násilí	násilí	k1gNnSc2	násilí
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
padly	padnout	k5eAaImAgInP	padnout
první	první	k4xOgInPc1	první
výstřely	výstřel	k1gInPc1	výstřel
v	v	k7c6	v
Bijeljině	Bijeljina	k1gFnSc6	Bijeljina
<g/>
,	,	kIx,	,
začaly	začít	k5eAaPmAgInP	začít
tisíce	tisíc	k4xCgInPc1	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
největšího	veliký	k2eAgNnSc2d3	veliký
města	město	k1gNnSc2	město
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
protestovat	protestovat	k5eAaBmF	protestovat
proti	proti	k7c3	proti
válce	válka	k1gFnSc3	válka
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnSc1	jejich
shromáždění	shromáždění	k1gNnSc1	shromáždění
bylo	být	k5eAaImAgNnS	být
ukončeno	ukončit	k5eAaPmNgNnS	ukončit
až	až	k9	až
odstřelováním	odstřelování	k1gNnSc7	odstřelování
srbské	srbský	k2eAgFnSc2d1	Srbská
polovojenské	polovojenský	k2eAgFnSc2d1	polovojenská
jednotky	jednotka	k1gFnSc2	jednotka
z	z	k7c2	z
hotelu	hotel	k1gInSc2	hotel
Holiday	Holidaa	k1gMnSc2	Holidaa
Inn	Inn	k1gMnSc2	Inn
<g/>
.	.	kIx.	.
<g/>
;	;	kIx,	;
k	k	k7c3	k
zastavení	zastavení	k1gNnSc3	zastavení
násilí	násilí	k1gNnSc2	násilí
vyzval	vyzvat	k5eAaPmAgMnS	vyzvat
i	i	k9	i
Alija	Alija	k1gMnSc1	Alija
Izetbegović	Izetbegović	k1gMnSc1	Izetbegović
<g/>
,	,	kIx,	,
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
prezident	prezident	k1gMnSc1	prezident
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
také	také	k9	také
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
výjimečný	výjimečný	k2eAgInSc4d1	výjimečný
stav	stav	k1gInSc4	stav
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
výzvy	výzva	k1gFnPc4	výzva
však	však	k9	však
nakonec	nakonec	k6eAd1	nakonec
byly	být	k5eAaImAgInP	být
plané	planý	k2eAgInPc1d1	planý
<g/>
;	;	kIx,	;
srbští	srbský	k2eAgMnPc1d1	srbský
představitelé	představitel	k1gMnPc1	představitel
odmítli	odmítnout	k5eAaPmAgMnP	odmítnout
uznat	uznat	k5eAaPmF	uznat
nezávislý	závislý	k2eNgInSc1d1	nezávislý
stát	stát	k1gInSc1	stát
v	v	k7c6	v
jakékoliv	jakýkoliv	k3yIgFnSc6	jakýkoliv
jeho	jeho	k3xOp3gFnSc6	jeho
podobě	podoba	k1gFnSc6	podoba
a	a	k8xC	a
spíše	spíše	k9	spíše
se	se	k3xPyFc4	se
snažili	snažit	k5eAaImAgMnP	snažit
zemi	zem	k1gFnSc3	zem
rozdělit	rozdělit	k5eAaPmF	rozdělit
s	s	k7c7	s
Chorvaty	Chorvat	k1gMnPc7	Chorvat
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
následně	následně	k6eAd1	následně
od	od	k7c2	od
května	květen	k1gInSc2	květen
1992	[number]	k4	1992
začala	začít	k5eAaPmAgFnS	začít
bombardovat	bombardovat	k5eAaImF	bombardovat
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
obléhat	obléhat	k5eAaImF	obléhat
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
nyní	nyní	k6eAd1	nyní
výhradně	výhradně	k6eAd1	výhradně
srbská	srbský	k2eAgFnSc1d1	Srbská
<g/>
,	,	kIx,	,
Jugoslávská	jugoslávský	k2eAgFnSc1d1	jugoslávská
lidová	lidový	k2eAgFnSc1d1	lidová
armáda	armáda	k1gFnSc1	armáda
(	(	kIx(	(
<g/>
JNA	JNA	kA	JNA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
po	po	k7c4	po
celé	celý	k2eAgInPc4d1	celý
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Zničeno	zničen	k2eAgNnSc1d1	zničeno
bylo	být	k5eAaImAgNnS	být
mnoho	mnoho	k4c1	mnoho
významných	významný	k2eAgFnPc2d1	významná
památek	památka	k1gFnPc2	památka
<g/>
,	,	kIx,	,
celé	celý	k2eAgNnSc1d1	celé
město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
silně	silně	k6eAd1	silně
poškozeno	poškodit	k5eAaPmNgNnS	poškodit
<g/>
.	.	kIx.	.
</s>
<s>
Dodávky	dodávka	k1gFnPc1	dodávka
elektřiny	elektřina	k1gFnSc2	elektřina
<g/>
,	,	kIx,	,
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
tepla	teplo	k1gNnSc2	teplo
a	a	k8xC	a
lékařské	lékařský	k2eAgFnSc2d1	lékařská
péče	péče	k1gFnSc2	péče
ustaly	ustat	k5eAaPmAgFnP	ustat
a	a	k8xC	a
místní	místní	k2eAgMnPc1d1	místní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
se	se	k3xPyFc4	se
ocitli	ocitnout	k5eAaPmAgMnP	ocitnout
v	v	k7c6	v
nesnesitelných	snesitelný	k2eNgFnPc6d1	nesnesitelná
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Dařilo	dařit	k5eAaImAgNnS	dařit
se	se	k3xPyFc4	se
černému	černé	k1gNnSc3	černé
trhu	trh	k1gInSc2	trh
a	a	k8xC	a
kvetl	kvést	k5eAaImAgInS	kvést
prodej	prodej	k1gInSc1	prodej
předražovaných	předražovaný	k2eAgFnPc2d1	předražovaný
potravin	potravina	k1gFnPc2	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Jediný	jediný	k2eAgInSc4d1	jediný
koridor	koridor	k1gInSc4	koridor
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
němuž	jenž	k3xRgNnSc3	jenž
bylo	být	k5eAaImAgNnS	být
město	město	k1gNnSc1	město
přístupné	přístupný	k2eAgNnSc1d1	přístupné
<g/>
,	,	kIx,	,
vedl	vést	k5eAaImAgMnS	vést
přes	přes	k7c4	přes
letiště	letiště	k1gNnSc4	letiště
<g/>
;	;	kIx,	;
tudy	tudy	k6eAd1	tudy
také	také	k9	také
přinášely	přinášet	k5eAaImAgInP	přinášet
pomoc	pomoc	k1gFnSc4	pomoc
konvoje	konvoj	k1gInPc1	konvoj
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Civilisté	civilista	k1gMnPc1	civilista
pod	pod	k7c7	pod
letištěm	letiště	k1gNnSc7	letiště
vybudovali	vybudovat	k5eAaPmAgMnP	vybudovat
cca	cca	kA	cca
800	[number]	k4	800
m	m	kA	m
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
tunel	tunel	k1gInSc1	tunel
<g/>
,	,	kIx,	,
kterým	který	k3yRgMnSc7	který
se	se	k3xPyFc4	se
pokoušeli	pokoušet	k5eAaImAgMnP	pokoušet
zajistit	zajistit	k5eAaPmF	zajistit
si	se	k3xPyFc3	se
základní	základní	k2eAgFnPc4d1	základní
potřeby	potřeba	k1gFnPc4	potřeba
<g/>
;	;	kIx,	;
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
slouží	sloužit	k5eAaImIp3nS	sloužit
část	část	k1gFnSc1	část
tunelu	tunel	k1gInSc2	tunel
jako	jako	k8xC	jako
muzeum	muzeum	k1gNnSc1	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
si	se	k3xPyFc3	se
podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
v	v	k7c6	v
Sarajevu	Sarajevo	k1gNnSc6	Sarajevo
12	[number]	k4	12
000	[number]	k4	000
mrtvých	mrtvý	k1gMnPc2	mrtvý
a	a	k8xC	a
50	[number]	k4	50
000	[number]	k4	000
zraněných	zraněný	k1gMnPc2	zraněný
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
připomínají	připomínat	k5eAaImIp3nP	připomínat
relativně	relativně	k6eAd1	relativně
nové	nový	k2eAgInPc4d1	nový
hřbitovy	hřbitov	k1gInPc4	hřbitov
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
křesťanské	křesťanský	k2eAgFnPc1d1	křesťanská
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
muslimské	muslimský	k2eAgFnPc1d1	muslimská
<g/>
.	.	kIx.	.
</s>
<s>
Válku	válka	k1gFnSc4	válka
ukončila	ukončit	k5eAaPmAgFnS	ukončit
až	až	k9	až
mírová	mírový	k2eAgFnSc1d1	mírová
smlouva	smlouva	k1gFnSc1	smlouva
z	z	k7c2	z
amerického	americký	k2eAgInSc2d1	americký
Daytonu	Dayton	k1gInSc2	Dayton
podepsaná	podepsaný	k2eAgFnSc1d1	podepsaná
19	[number]	k4	19
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1995	[number]	k4	1995
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
smlouvy	smlouva	k1gFnSc2	smlouva
také	také	k9	také
bylo	být	k5eAaImAgNnS	být
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
budoucí	budoucí	k2eAgNnSc1d1	budoucí
uspořádání	uspořádání	k1gNnSc1	uspořádání
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgFnPc1	dva
znepřátelené	znepřátelený	k2eAgFnPc1d1	znepřátelená
strany	strana	k1gFnPc1	strana
-	-	kIx~	-
tedy	tedy	k9	tedy
Srbové	Srb	k1gMnPc1	Srb
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
jedné	jeden	k4xCgFnSc2	jeden
a	a	k8xC	a
Chorvaté	Chorvat	k1gMnPc1	Chorvat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Bosňáky	Bosňáky	k?	Bosňáky
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
druhé	druhý	k4xOgFnSc6	druhý
-	-	kIx~	-
dostali	dostat	k5eAaPmAgMnP	dostat
území	území	k1gNnSc2	území
země	zem	k1gFnSc2	zem
rozdělené	rozdělená	k1gFnSc2	rozdělená
půl	půl	k1xP	půl
na	na	k7c6	na
půl	půl	k1xP	půl
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
však	však	k9	však
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
několika	několik	k4yIc2	několik
menších	malý	k2eAgNnPc2d2	menší
předměstí	předměstí	k1gNnPc2	předměstí
na	na	k7c6	na
východě	východ	k1gInSc6	východ
připadlo	připadnout	k5eAaPmAgNnS	připadnout
Federaci	federace	k1gFnSc4	federace
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
-	-	kIx~	-
nově	nově	k6eAd1	nově
ustanovené	ustanovený	k2eAgFnSc6d1	ustanovená
entitě	entita	k1gFnSc6	entita
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
spravuje	spravovat	k5eAaImIp3nS	spravovat
tu	ten	k3xDgFnSc4	ten
oblast	oblast	k1gFnSc4	oblast
BaH	bah	k0	bah
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
žijí	žít	k5eAaImIp3nP	žít
převážně	převážně	k6eAd1	převážně
Bosňáci	Bosňáci	k?	Bosňáci
a	a	k8xC	a
Chorvaté	Chorvat	k1gMnPc1	Chorvat
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
strachu	strach	k1gInSc2	strach
před	před	k7c7	před
možnou	možný	k2eAgFnSc7d1	možná
odplatou	odplata	k1gFnSc7	odplata
a	a	k8xC	a
za	za	k7c2	za
masivní	masivní	k2eAgFnSc2d1	masivní
propagandy	propaganda	k1gFnSc2	propaganda
bosenskosrbského	bosenskosrbský	k2eAgNnSc2d1	bosenskosrbský
vedení	vedení	k1gNnSc2	vedení
se	se	k3xPyFc4	se
z	z	k7c2	z
města	město	k1gNnSc2	město
vystěhovaly	vystěhovat	k5eAaPmAgInP	vystěhovat
další	další	k2eAgInPc1d1	další
tisíce	tisíc	k4xCgInPc1	tisíc
Srbů	Srb	k1gMnPc2	Srb
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
dnes	dnes	k6eAd1	dnes
žijí	žít	k5eAaImIp3nP	žít
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Bosně	Bosna	k1gFnSc6	Bosna
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
srbských	srbský	k2eAgMnPc2d1	srbský
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
snížil	snížit	k5eAaPmAgInS	snížit
ze	z	k7c2	z
157	[number]	k4	157
193	[number]	k4	193
osob	osoba	k1gFnPc2	osoba
na	na	k7c4	na
cca	cca	kA	cca
18	[number]	k4	18
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Sarajeva	Sarajevo	k1gNnSc2	Sarajevo
naopak	naopak	k6eAd1	naopak
začali	začít	k5eAaPmAgMnP	začít
přicházet	přicházet	k5eAaImF	přicházet
uprchlíci	uprchlík	k1gMnPc1	uprchlík
(	(	kIx(	(
<g/>
především	především	k9	především
Bosňáci	Bosňáci	k?	Bosňáci
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
z	z	k7c2	z
východní	východní	k2eAgFnSc2d1	východní
Bosny	Bosna	k1gFnSc2	Bosna
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
dosud	dosud	k6eAd1	dosud
pobývali	pobývat	k5eAaImAgMnP	pobývat
v	v	k7c6	v
dalších	další	k2eAgNnPc6d1	další
velkých	velký	k2eAgNnPc6d1	velké
městech	město	k1gNnPc6	město
-	-	kIx~	-
Tuzle	Tuzle	k1gInSc1	Tuzle
a	a	k8xC	a
Zenici	zenice	k1gFnSc3	zenice
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
neúspěšně	úspěšně	k6eNd1	úspěšně
kandidovalo	kandidovat	k5eAaImAgNnS	kandidovat
na	na	k7c6	na
konání	konání	k1gNnSc6	konání
Zimních	zimní	k2eAgFnPc2d1	zimní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
a	a	k8xC	a
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
tu	ten	k3xDgFnSc4	ten
kandidaturu	kandidatura	k1gFnSc4	kandidatura
stáhlo	stáhnout	k5eAaPmAgNnS	stáhnout
<g/>
.	.	kIx.	.
</s>
<s>
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
se	se	k3xPyFc4	se
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
jihovýchodním	jihovýchodní	k2eAgInSc7d1	jihovýchodní
směrem	směr	k1gInSc7	směr
od	od	k7c2	od
pomyslného	pomyslný	k2eAgInSc2d1	pomyslný
středu	střed	k1gInSc2	střed
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
<g/>
,	,	kIx,	,
v	v	k7c6	v
relativně	relativně	k6eAd1	relativně
rozsáhlém	rozsáhlý	k2eAgNnSc6d1	rozsáhlé
<g/>
,	,	kIx,	,
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
západu	západ	k1gInSc3	západ
rozšiřujícím	rozšiřující	k2eAgInSc7d1	rozšiřující
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
údolí	údolí	k1gNnSc4	údolí
řeky	řeka	k1gFnSc2	řeka
Miljacky	Miljacka	k1gFnSc2	Miljacka
a	a	k8xC	a
částečně	částečně	k6eAd1	částečně
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
i	i	k9	i
do	do	k7c2	do
velkého	velký	k2eAgNnSc2d1	velké
údolí	údolí	k1gNnSc2	údolí
řeky	řeka	k1gFnSc2	řeka
Bosny	Bosna	k1gFnSc2	Bosna
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
vyvěračka	vyvěračka	k1gFnSc1	vyvěračka
(	(	kIx(	(
<g/>
Vrelo	Vrela	k1gMnSc5	Vrela
Bosne	Bosn	k1gMnSc5	Bosn
<g/>
)	)	kIx)	)
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
významným	významný	k2eAgFnPc3d1	významná
přírodním	přírodní	k2eAgFnPc3d1	přírodní
památkám	památka	k1gFnPc3	památka
okolí	okolí	k1gNnSc2	okolí
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Úpatí	úpatí	k1gNnSc1	úpatí
údolí	údolí	k1gNnSc2	údolí
Miljacky	Miljacka	k1gFnSc2	Miljacka
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
strmé	strmý	k2eAgNnSc1d1	strmé
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
lze	lze	k6eAd1	lze
vidět	vidět	k5eAaImF	vidět
na	na	k7c6	na
místní	místní	k2eAgFnSc6d1	místní
zástavbě	zástavba	k1gFnSc6	zástavba
a	a	k8xC	a
uličkách	ulička	k1gFnPc6	ulička
s	s	k7c7	s
velmi	velmi	k6eAd1	velmi
prudkým	prudký	k2eAgNnSc7d1	prudké
klesáním	klesání	k1gNnSc7	klesání
se	s	k7c7	s
serpentinami	serpentina	k1gFnPc7	serpentina
<g/>
.	.	kIx.	.
</s>
<s>
Okolní	okolní	k2eAgFnSc1d1	okolní
krajina	krajina	k1gFnSc1	krajina
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
hornatá	hornatý	k2eAgFnSc1d1	hornatá
<g/>
;	;	kIx,	;
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
je	být	k5eAaImIp3nS	být
Treskavica	Treskavica	k1gFnSc1	Treskavica
s	s	k7c7	s
2	[number]	k4	2
088	[number]	k4	088
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
má	mít	k5eAaImIp3nS	mít
mírné	mírný	k2eAgNnSc1d1	mírné
podnebí	podnebí	k1gNnSc1	podnebí
<g/>
,	,	kIx,	,
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
nedaleko	nedaleko	k7c2	nedaleko
rozhraní	rozhraní	k1gNnSc2	rozhraní
mírného	mírný	k2eAgNnSc2d1	mírné
kontinentálního	kontinentální	k2eAgNnSc2d1	kontinentální
a	a	k8xC	a
teplého	teplý	k2eAgNnSc2d1	teplé
středomořského	středomořský	k2eAgNnSc2d1	středomořské
podnebí	podnebí	k1gNnSc2	podnebí
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgInSc1d1	roční
průměr	průměr	k1gInSc1	průměr
teplot	teplota	k1gFnPc2	teplota
zde	zde	k6eAd1	zde
činí	činit	k5eAaImIp3nS	činit
10	[number]	k4	10
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
léta	léto	k1gNnPc1	léto
jsou	být	k5eAaImIp3nP	být
mírná	mírný	k2eAgNnPc1d1	mírné
(	(	kIx(	(
<g/>
průměr	průměr	k1gInSc1	průměr
18,1	[number]	k4	18,1
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zimy	zima	k1gFnSc2	zima
studené	studený	k2eAgFnSc2d1	studená
(	(	kIx(	(
<g/>
0,3	[number]	k4	0,3
°	°	k?	°
<g/>
C	C	kA	C
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nejteplejším	teplý	k2eAgInSc7d3	nejteplejší
měsícem	měsíc	k1gInSc7	měsíc
v	v	k7c6	v
roce	rok	k1gInSc6	rok
je	být	k5eAaImIp3nS	být
červenec	červenec	k1gInSc1	červenec
<g/>
,	,	kIx,	,
nejchladnějším	chladný	k2eAgInSc7d3	nejchladnější
pak	pak	k6eAd1	pak
leden	leden	k1gInSc4	leden
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
zaznamenaná	zaznamenaný	k2eAgFnSc1d1	zaznamenaná
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
Sarajevu	Sarajevo	k1gNnSc6	Sarajevo
byla	být	k5eAaImAgFnS	být
40	[number]	k4	40
<g/>
°	°	k?	°
C	C	kA	C
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1946	[number]	k4	1946
<g/>
,	,	kIx,	,
nejníže	nízce	k6eAd3	nízce
pak	pak	k6eAd1	pak
rtuť	rtuť	k1gFnSc1	rtuť
teploměru	teploměr	k1gInSc2	teploměr
klesla	klesnout	k5eAaPmAgFnS	klesnout
24	[number]	k4	24
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1942	[number]	k4	1942
na	na	k7c4	na
-	-	kIx~	-
<g/>
26,4	[number]	k4	26,4
°	°	k?	°
<g/>
C.	C.	kA	C.
Průměrný	průměrný	k2eAgInSc1d1	průměrný
úhrn	úhrn	k1gInSc1	úhrn
srážek	srážka	k1gFnPc2	srážka
za	za	k7c4	za
rok	rok	k1gInSc4	rok
tu	tu	k6eAd1	tu
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
932	[number]	k4	932
mm	mm	kA	mm
<g/>
;	;	kIx,	;
nejdeštivějším	deštivý	k2eAgInSc7d3	nejdeštivější
měsícem	měsíc	k1gInSc7	měsíc
je	být	k5eAaImIp3nS	být
říjen	říjen	k1gInSc4	říjen
a	a	k8xC	a
nejsušším	suchý	k2eAgInSc7d3	nejsušší
březen	březen	k1gInSc4	březen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
počet	počet	k1gInSc1	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
Sarajeva	Sarajevo	k1gNnSc2	Sarajevo
292	[number]	k4	292
399	[number]	k4	399
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
oproti	oproti	k7c3	oproti
429	[number]	k4	429
672	[number]	k4	672
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
značný	značný	k2eAgInSc4d1	značný
pokles	pokles	k1gInSc4	pokles
<g/>
.	.	kIx.	.
</s>
<s>
Podílely	podílet	k5eAaImAgInP	podílet
se	se	k3xPyFc4	se
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
především	především	k9	především
události	událost	k1gFnPc1	událost
spojené	spojený	k2eAgFnPc1d1	spojená
s	s	k7c7	s
válkou	válka	k1gFnSc7	válka
z	z	k7c2	z
let	léto	k1gNnPc2	léto
1992	[number]	k4	1992
<g/>
-	-	kIx~	-
<g/>
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
město	město	k1gNnSc1	město
opustilo	opustit	k5eAaPmAgNnS	opustit
přes	přes	k7c4	přes
100	[number]	k4	100
000	[number]	k4	000
jeho	jeho	k3xOp3gMnPc2	jeho
původních	původní	k2eAgMnPc2d1	původní
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Válečný	válečný	k2eAgInSc1d1	válečný
konflikt	konflikt	k1gInSc1	konflikt
pozměnil	pozměnit	k5eAaPmAgInS	pozměnit
také	také	k9	také
etnické	etnický	k2eAgNnSc4d1	etnické
složení	složení	k1gNnSc4	složení
sarajevské	sarajevský	k2eAgFnSc2d1	Sarajevská
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
ve	v	k7c6	v
městě	město	k1gNnSc6	město
(	(	kIx(	(
<g/>
současných	současný	k2eAgInPc6d1	současný
4	[number]	k4	4
obvodech	obvod	k1gInPc6	obvod
a	a	k8xC	a
předměstí	předměstí	k1gNnSc6	předměstí
Ilidže	Ilidž	k1gFnSc2	Ilidž
<g/>
)	)	kIx)	)
žilo	žít	k5eAaImAgNnS	žít
259	[number]	k4	259
470	[number]	k4	470
(	(	kIx(	(
<g/>
49,23	[number]	k4	49,23
%	%	kIx~	%
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
Bosňáků	Bosňáků	k?	Bosňáků
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
157	[number]	k4	157
143	[number]	k4	143
(	(	kIx(	(
<g/>
29,82	[number]	k4	29,82
%	%	kIx~	%
<g/>
)	)	kIx)	)
Srbů	Srb	k1gMnPc2	Srb
<g/>
,	,	kIx,	,
34	[number]	k4	34
873	[number]	k4	873
(	(	kIx(	(
<g/>
6,62	[number]	k4	6,62
%	%	kIx~	%
<g/>
)	)	kIx)	)
Chorvatů	Chorvat	k1gMnPc2	Chorvat
a	a	k8xC	a
56	[number]	k4	56
460	[number]	k4	460
(	(	kIx(	(
<g/>
10,71	[number]	k4	10,71
%	%	kIx~	%
<g/>
)	)	kIx)	)
Jugoslávců	Jugoslávec	k1gMnPc2	Jugoslávec
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k6eAd1	ještě
před	před	k7c7	před
vypuknutím	vypuknutí	k1gNnSc7	vypuknutí
bojů	boj	k1gInPc2	boj
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1992	[number]	k4	1992
z	z	k7c2	z
města	město	k1gNnSc2	město
odešla	odejít	k5eAaPmAgFnS	odejít
značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
současných	současný	k2eAgFnPc2d1	současná
poválečných	poválečný	k2eAgFnPc2d1	poválečná
hranic	hranice	k1gFnPc2	hranice
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
žilo	žít	k5eAaImAgNnS	žít
98,5	[number]	k4	98,5
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
Sarajeva	Sarajevo	k1gNnSc2	Sarajevo
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
Federací	federace	k1gFnSc7	federace
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
a	a	k8xC	a
1,5	[number]	k4	1,5
%	%	kIx~	%
na	na	k7c6	na
území	území	k1gNnSc6	území
současné	současný	k2eAgFnSc2d1	současná
Republiky	republika	k1gFnSc2	republika
srbské	srbský	k2eAgFnSc2d1	Srbská
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2002	[number]	k4	2002
žilo	žít	k5eAaImAgNnS	žít
ve	v	k7c6	v
čtyřech	čtyři	k4xCgInPc6	čtyři
městských	městský	k2eAgInPc6d1	městský
obvodech	obvod	k1gInPc6	obvod
230	[number]	k4	230
130	[number]	k4	130
(	(	kIx(	(
<g/>
77,4	[number]	k4	77,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
Bosňáků	Bosňáků	k?	Bosňáků
<g/>
,	,	kIx,	,
35	[number]	k4	35
606	[number]	k4	606
(	(	kIx(	(
<g/>
12,0	[number]	k4	12,0
%	%	kIx~	%
<g/>
)	)	kIx)	)
Srbů	Srb	k1gMnPc2	Srb
a	a	k8xC	a
22	[number]	k4	22
380	[number]	k4	380
(	(	kIx(	(
<g/>
7,5	[number]	k4	7,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
Chorvatů	Chorvat	k1gMnPc2	Chorvat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
tvoří	tvořit	k5eAaImIp3nS	tvořit
52	[number]	k4	52
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
tu	ten	k3xDgFnSc4	ten
část	část	k1gFnSc4	část
Sarajevanů	Sarajevan	k1gMnPc2	Sarajevan
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
usídlili	usídlit	k5eAaPmAgMnP	usídlit
během	během	k7c2	během
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Přistěhovalectví	přistěhovalectví	k1gNnSc1	přistěhovalectví
ze	z	k7c2	z
zbytku	zbytek	k1gInSc2	zbytek
země	zem	k1gFnSc2	zem
do	do	k7c2	do
metropole	metropol	k1gFnSc2	metropol
stále	stále	k6eAd1	stále
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
a	a	k8xC	a
tak	tak	k6eAd1	tak
není	být	k5eNaImIp3nS	být
vyloučeno	vyloučit	k5eAaPmNgNnS	vyloučit
<g/>
,	,	kIx,	,
že	že	k8xS	že
město	město	k1gNnSc1	město
by	by	kYmCp3nP	by
mohlo	moct	k5eAaImAgNnS	moct
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
překročit	překročit	k5eAaPmF	překročit
metu	meta	k1gFnSc4	meta
jednoho	jeden	k4xCgInSc2	jeden
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
je	být	k5eAaImIp3nS	být
centrem	centr	k1gInSc7	centr
jak	jak	k6eAd1	jak
bosenského	bosenský	k2eAgInSc2d1	bosenský
islámu	islám	k1gInSc2	islám
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
římskokatolické	římskokatolický	k2eAgFnSc2d1	Římskokatolická
a	a	k8xC	a
pravoslavné	pravoslavný	k2eAgFnSc2d1	pravoslavná
křesťanské	křesťanský	k2eAgFnSc2d1	křesťanská
církve	církev	k1gFnSc2	církev
<g/>
;	;	kIx,	;
zastoupeni	zastoupit	k5eAaPmNgMnP	zastoupit
jsou	být	k5eAaImIp3nP	být
rovněž	rovněž	k6eAd1	rovněž
i	i	k9	i
Židé	Žid	k1gMnPc1	Žid
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
ostatních	ostatní	k2eAgNnPc2d1	ostatní
měst	město	k1gNnPc2	město
je	být	k5eAaImIp3nS	být
tak	tak	k9	tak
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
nábožensky	nábožensky	k6eAd1	nábožensky
velmi	velmi	k6eAd1	velmi
heterogenní	heterogenní	k2eAgFnSc1d1	heterogenní
<g/>
;	;	kIx,	;
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
kostely	kostel	k1gInPc7	kostel
<g/>
,	,	kIx,	,
synagogy	synagoga	k1gFnPc4	synagoga
i	i	k8xC	i
mešity	mešita	k1gFnPc4	mešita
(	(	kIx(	(
<g/>
celkem	celkem	k6eAd1	celkem
86	[number]	k4	86
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byl	být	k5eAaImAgInS	být
poměr	poměr	k1gInSc1	poměr
mezi	mezi	k7c7	mezi
věřícími	věřící	k1gMnPc7	věřící
relativně	relativně	k6eAd1	relativně
vyrovnaný	vyrovnaný	k2eAgInSc1d1	vyrovnaný
<g/>
;	;	kIx,	;
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
bylo	být	k5eAaImAgNnS	být
velmi	velmi	k6eAd1	velmi
multikulturním	multikulturní	k2eAgNnSc7d1	multikulturní
městem	město	k1gNnSc7	město
-	-	kIx~	-
přezdívalo	přezdívat	k5eAaImAgNnS	přezdívat
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
evropský	evropský	k2eAgInSc4d1	evropský
Jeruzalém	Jeruzalém	k1gInSc4	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
však	však	k9	však
začal	začít	k5eAaPmAgMnS	začít
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
národnostnímu	národnostní	k2eAgNnSc3d1	národnostní
složení	složení	k1gNnSc3	složení
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
podle	podle	k7c2	podle
poválečných	poválečný	k2eAgFnPc2d1	poválečná
vnitrostátních	vnitrostátní	k2eAgFnPc2d1	vnitrostátní
hranic	hranice	k1gFnPc2	hranice
<g/>
,	,	kIx,	,
získávat	získávat	k5eAaImF	získávat
převahu	převaha	k1gFnSc4	převaha
islám	islám	k1gInSc1	islám
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
tvořili	tvořit	k5eAaImAgMnP	tvořit
již	již	k6eAd1	již
87	[number]	k4	87
%	%	kIx~	%
všech	všecek	k3xTgMnPc2	všecek
Sarajevanů	Sarajevan	k1gMnPc2	Sarajevan
Bosňáci	Bosňáci	k?	Bosňáci
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
to	ten	k3xDgNnSc1	ten
bývá	bývat	k5eAaImIp3nS	bývat
kritizováno	kritizovat	k5eAaImNgNnS	kritizovat
převážně	převážně	k6eAd1	převážně
srbskými	srbský	k2eAgInPc7d1	srbský
kruhy	kruh	k1gInPc7	kruh
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
administrativně	administrativně	k6eAd1	administrativně
na	na	k7c4	na
4	[number]	k4	4
opčiny	opčina	k1gFnSc2	opčina
(	(	kIx(	(
<g/>
Centar	Centar	k1gInSc1	Centar
<g/>
,	,	kIx,	,
Novi	Novi	k1gNnSc1	Novi
Grad	grad	k1gInSc1	grad
<g/>
,	,	kIx,	,
Novo	nova	k1gFnSc5	nova
Sarajevo	Sarajevo	k1gNnSc4	Sarajevo
a	a	k8xC	a
Stari	Stari	k1gNnSc4	Stari
Grad	grad	k1gInSc1	grad
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
opčinami	opčina	k1gFnPc7	opčina
Ilidža	Ilidžum	k1gNnSc2	Ilidžum
a	a	k8xC	a
Vogošća	Vogošćum	k1gNnSc2	Vogošćum
tvoří	tvořit	k5eAaImIp3nP	tvořit
zmíněné	zmíněný	k2eAgInPc1d1	zmíněný
čtyři	čtyři	k4xCgInPc1	čtyři
tzv.	tzv.	kA	tzv.
Větší	veliký	k2eAgNnSc1d2	veliký
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
<g/>
.	.	kIx.	.
</s>
<s>
Před	před	k7c7	před
válkou	válka	k1gFnSc7	válka
bylo	být	k5eAaImAgNnS	být
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
mnohem	mnohem	k6eAd1	mnohem
rozsáhlejší	rozsáhlý	k2eAgFnSc2d2	rozsáhlejší
<g/>
;	;	kIx,	;
východní	východní	k2eAgFnSc2d1	východní
části	část	k1gFnSc2	část
však	však	k9	však
připadly	připadnout	k5eAaPmAgFnP	připadnout
Republice	republika	k1gFnSc6	republika
srbské	srbský	k2eAgFnSc6d1	Srbská
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
reorganizovány	reorganizován	k2eAgInPc1d1	reorganizován
jako	jako	k8xC	jako
bosensky	bosensky	k6eAd1	bosensky
Srpsko	Srpsko	k1gNnSc1	Srpsko
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
Istočno	Istočno	k1gNnSc1	Istočno
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
-	-	kIx~	-
východní	východní	k2eAgNnSc1d1	východní
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
největší	veliký	k2eAgFnSc1d3	veliký
takovou	takový	k3xDgFnSc7	takový
bývalou	bývalý	k2eAgFnSc7d1	bývalá
částí	část	k1gFnSc7	část
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
Pale	pal	k1gInSc5	pal
<g/>
.	.	kIx.	.
</s>
<s>
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
patří	patřit	k5eAaImIp3nS	patřit
vzhledem	vzhled	k1gInSc7	vzhled
ke	k	k7c3	k
svému	svůj	k3xOyFgInSc3	svůj
významu	význam	k1gInSc3	význam
a	a	k8xC	a
centralistickému	centralistický	k2eAgInSc3d1	centralistický
charakteru	charakter	k1gInSc3	charakter
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
k	k	k7c3	k
hospodářsky	hospodářsky	k6eAd1	hospodářsky
nejsilnějším	silný	k2eAgFnPc3d3	nejsilnější
oblastem	oblast	k1gFnPc3	oblast
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
ekonomická	ekonomický	k2eAgFnSc1d1	ekonomická
prosperita	prosperita	k1gFnSc1	prosperita
stojí	stát	k5eAaImIp3nS	stát
jak	jak	k6eAd1	jak
na	na	k7c6	na
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
i	i	k9	i
na	na	k7c6	na
službách	služba	k1gFnPc6	služba
a	a	k8xC	a
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
i	i	k8xC	i
na	na	k7c6	na
turistice	turistika	k1gFnSc6	turistika
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
zde	zde	k6eAd1	zde
bosenská	bosenský	k2eAgFnSc1d1	bosenská
centrální	centrální	k2eAgFnSc1d1	centrální
banka	banka	k1gFnSc1	banka
a	a	k8xC	a
sarajevská	sarajevský	k2eAgFnSc1d1	Sarajevská
burza	burza	k1gFnSc1	burza
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
má	mít	k5eAaImIp3nS	mít
své	svůj	k3xOyFgInPc4	svůj
závody	závod	k1gInPc4	závod
mnoho	mnoho	k6eAd1	mnoho
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Harris	Harris	k1gFnSc1	Harris
Communications	Communications	k1gInSc1	Communications
<g/>
,	,	kIx,	,
Brown	Brown	k1gMnSc1	Brown
<g/>
&	&	k?	&
<g/>
Root	Root	k1gMnSc1	Root
<g/>
,	,	kIx,	,
Škoda	škoda	k1gFnSc1	škoda
Auto	auto	k1gNnSc1	auto
<g/>
,	,	kIx,	,
či	či	k8xC	či
Coca-Cola	cocaola	k1gFnSc1	coca-cola
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
staví	stavit	k5eAaImIp3nS	stavit
velká	velký	k2eAgNnPc4d1	velké
obchodní	obchodní	k2eAgNnPc4d1	obchodní
centra	centrum	k1gNnPc4	centrum
Bosmal	Bosmal	k1gInSc1	Bosmal
City	city	k1gNnSc1	city
Centar	Centar	k1gInSc1	Centar
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc7	jehož
vlastníkem	vlastník	k1gMnSc7	vlastník
je	být	k5eAaImIp3nS	být
bosensko-malajsijská	bosenskoalajsijský	k2eAgFnSc1d1	bosensko-malajsijský
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
Mercator	Mercator	k1gInSc1	Mercator
<g/>
,	,	kIx,	,
Merkur	Merkur	k1gInSc1	Merkur
aj.	aj.	kA	aj.
Ekonomický	ekonomický	k2eAgInSc1d1	ekonomický
vzestup	vzestup	k1gInSc1	vzestup
města	město	k1gNnSc2	město
začal	začít	k5eAaPmAgInS	začít
již	již	k6eAd1	již
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
<g/>
;	;	kIx,	;
zatímco	zatímco	k8xS	zatímco
pro	pro	k7c4	pro
celou	celý	k2eAgFnSc4d1	celá
SFRJ	SFRJ	kA	SFRJ
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
období	období	k1gNnSc4	období
jisté	jistý	k2eAgFnSc2d1	jistá
stagnace	stagnace	k1gFnSc2	stagnace
<g/>
,	,	kIx,	,
bosenská	bosenský	k2eAgFnSc1d1	bosenská
metropole	metropole	k1gFnSc1	metropole
zažívala	zažívat	k5eAaImAgFnS	zažívat
růst	růst	k1gInSc4	růst
díky	díky	k7c3	díky
přípravě	příprava	k1gFnSc3	příprava
na	na	k7c4	na
zimní	zimní	k2eAgFnPc4d1	zimní
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
ji	on	k3xPp3gFnSc4	on
přivedlo	přivést	k5eAaPmAgNnS	přivést
k	k	k7c3	k
zájmu	zájem	k1gInSc3	zájem
mnohých	mnohý	k2eAgMnPc2d1	mnohý
investorů	investor	k1gMnPc2	investor
<g/>
,	,	kIx,	,
jež	jenž	k3xRgMnPc4	jenž
sem	sem	k6eAd1	sem
přesunuli	přesunout	k5eAaPmAgMnP	přesunout
následně	následně	k6eAd1	následně
některé	některý	k3yIgInPc4	některý
své	svůj	k3xOyFgInPc4	svůj
výrobní	výrobní	k2eAgInPc4d1	výrobní
a	a	k8xC	a
obchodní	obchodní	k2eAgFnPc4d1	obchodní
kapacity	kapacita	k1gFnPc4	kapacita
<g/>
.	.	kIx.	.
</s>
<s>
Velkým	velký	k2eAgInSc7d1	velký
šokem	šok	k1gInSc7	šok
se	se	k3xPyFc4	se
ale	ale	k9	ale
stala	stát	k5eAaPmAgFnS	stát
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
během	během	k7c2	během
níž	jenž	k3xRgFnSc2	jenž
Srbové	Srbová	k1gFnSc2	Srbová
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
město	město	k1gNnSc4	město
obléhali	obléhat	k5eAaImAgMnP	obléhat
<g/>
,	,	kIx,	,
útočili	útočit	k5eAaImAgMnP	útočit
na	na	k7c4	na
významné	významný	k2eAgInPc4d1	významný
průmyslové	průmyslový	k2eAgInPc4d1	průmyslový
závody	závod	k1gInPc4	závod
<g/>
;	;	kIx,	;
ustala	ustat	k5eAaPmAgFnS	ustat
tehdy	tehdy	k6eAd1	tehdy
i	i	k9	i
jakákoliv	jakýkoliv	k3yIgFnSc1	jakýkoliv
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
činnost	činnost	k1gFnSc1	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
bojů	boj	k1gInPc2	boj
<g/>
,	,	kIx,	,
podepsání	podepsání	k1gNnSc4	podepsání
míru	mír	k1gInSc2	mír
<g/>
,	,	kIx,	,
začíná	začínat	k5eAaImIp3nS	začínat
poválečná	poválečný	k2eAgFnSc1d1	poválečná
obnova	obnova	k1gFnSc1	obnova
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Situaci	situace	k1gFnSc4	situace
zhoršilo	zhoršit	k5eAaPmAgNnS	zhoršit
vystěhování	vystěhování	k1gNnSc1	vystěhování
Srbů	Srb	k1gMnPc2	Srb
a	a	k8xC	a
příliv	příliv	k1gInSc4	příliv
Bosňáků	Bosňáků	k?	Bosňáků
z	z	k7c2	z
jiných	jiný	k2eAgFnPc2d1	jiná
částí	část	k1gFnPc2	část
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
většina	většina	k1gFnSc1	většina
práceschopných	práceschopný	k2eAgMnPc2d1	práceschopný
obyvatel	obyvatel	k1gMnPc2	obyvatel
zaměstnána	zaměstnat	k5eAaPmNgFnS	zaměstnat
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
(	(	kIx(	(
<g/>
19,4	[number]	k4	19,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
následuje	následovat	k5eAaImIp3nS	následovat
obchod	obchod	k1gInSc4	obchod
a	a	k8xC	a
služby	služba	k1gFnPc4	služba
(	(	kIx(	(
<g/>
12,9	[number]	k4	12,9
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pak	pak	k6eAd1	pak
doprava	doprava	k1gFnSc1	doprava
(	(	kIx(	(
<g/>
9,9	[number]	k4	9,9
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
školství	školství	k1gNnSc2	školství
(	(	kIx(	(
<g/>
8,7	[number]	k4	8,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Nejvíce	nejvíce	k6eAd1	nejvíce
pracovních	pracovní	k2eAgNnPc2d1	pracovní
míst	místo	k1gNnPc2	místo
je	být	k5eAaImIp3nS	být
koncentrováno	koncentrovat	k5eAaBmNgNnS	koncentrovat
v	v	k7c6	v
městských	městský	k2eAgInPc6d1	městský
obvodech	obvod	k1gInPc6	obvod
Centar	Centara	k1gFnPc2	Centara
a	a	k8xC	a
Novo	nova	k1gFnSc5	nova
Sarajevo	Sarajevo	k1gNnSc4	Sarajevo
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
centrum	centrum	k1gNnSc1	centrum
celé	celý	k2eAgFnSc2d1	celá
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
rovněž	rovněž	k9	rovněž
i	i	k9	i
střediskem	středisko	k1gNnSc7	středisko
veškerých	veškerý	k3xTgFnPc2	veškerý
telekomunikačních	telekomunikační	k2eAgFnPc2d1	telekomunikační
služeb	služba	k1gFnPc2	služba
a	a	k8xC	a
masmédií	masmédium	k1gNnPc2	masmédium
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
jsou	být	k5eAaImIp3nP	být
provozovány	provozovat	k5eAaImNgInP	provozovat
na	na	k7c6	na
celostátní	celostátní	k2eAgFnSc6d1	celostátní
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Sídlí	sídlet	k5eAaImIp3nS	sídlet
tu	tu	k6eAd1	tu
státní	státní	k2eAgFnSc1d1	státní
televize	televize	k1gFnSc1	televize
(	(	kIx(	(
<g/>
kanál	kanál	k1gInSc1	kanál
BHRT	BHRT	kA	BHRT
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
televize	televize	k1gFnSc1	televize
Federace	federace	k1gFnSc2	federace
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
(	(	kIx(	(
<g/>
též	též	k9	též
součást	součást	k1gFnSc4	součást
té	ten	k3xDgFnSc2	ten
státní	státní	k2eAgFnSc2d1	státní
<g/>
)	)	kIx)	)
s	s	k7c7	s
názvem	název	k1gInSc7	název
FTV	FTV	kA	FTV
(	(	kIx(	(
<g/>
RTVFBiH	RTVFBiH	k1gMnSc1	RTVFBiH
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vysílá	vysílat	k5eAaImIp3nS	vysílat
rovněž	rovněž	k9	rovněž
i	i	k9	i
kantonální	kantonální	k2eAgFnPc4d1	kantonální
stanice	stanice	k1gFnPc4	stanice
<g/>
,	,	kIx,	,
v	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
pak	pak	k6eAd1	pak
i	i	k9	i
mnoho	mnoho	k4c4	mnoho
komerčních	komerční	k2eAgInPc2d1	komerční
kanálů	kanál	k1gInPc2	kanál
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
NTV	NTV	kA	NTV
Hayat	Hayat	k1gInSc1	Hayat
<g/>
,	,	kIx,	,
TV	TV	kA	TV
Pink	pink	k6eAd1	pink
nebo	nebo	k8xC	nebo
TV	TV	kA	TV
OBN	OBN	kA	OBN
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Sarajevu	Sarajevo	k1gNnSc6	Sarajevo
též	též	k9	též
mají	mít	k5eAaImIp3nP	mít
své	svůj	k3xOyFgNnSc4	svůj
sídlo	sídlo	k1gNnSc4	sídlo
také	také	k9	také
celostátní	celostátní	k2eAgInPc1d1	celostátní
deníky	deník	k1gInPc1	deník
Oslobođenije	Oslobođenije	k1gFnSc2	Oslobođenije
a	a	k8xC	a
Dnevni	Dnevni	k1gMnSc1	Dnevni
Avaz	Avaz	k1gMnSc1	Avaz
<g/>
.	.	kIx.	.
</s>
<s>
Oba	dva	k4xCgInPc1	dva
dva	dva	k4xCgInPc1	dva
sídlí	sídlet	k5eAaImIp3nP	sídlet
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
velkém	velký	k2eAgInSc6d1	velký
komplexu	komplex	k1gInSc6	komplex
ve	v	k7c6	v
čtvrti	čtvrt	k1gFnSc6	čtvrt
Novo	nova	k1gFnSc5	nova
Sarajevo	Sarajevo	k1gNnSc4	Sarajevo
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
mediálním	mediální	k2eAgNnSc7d1	mediální
centrem	centrum	k1gNnSc7	centrum
Sarajeva	Sarajevo	k1gNnSc2	Sarajevo
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
tituly	titul	k1gInPc4	titul
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
tak	tak	k6eAd1	tak
významné	významný	k2eAgFnPc1d1	významná
<g/>
,	,	kIx,	,
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Slobodna	Slobodna	k1gFnSc1	Slobodna
Bosna	Bosna	k1gFnSc1	Bosna
<g/>
,	,	kIx,	,
Jutarnje	Jutarnje	k1gFnSc1	Jutarnje
novine	novinout	k5eAaPmIp3nS	novinout
či	či	k8xC	či
chorvatskojazyčný	chorvatskojazyčný	k2eAgInSc1d1	chorvatskojazyčný
Hrvatska	Hrvatsek	k1gMnSc4	Hrvatsek
riječ	riječ	k1gInSc4	riječ
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Sarajevu	Sarajevo	k1gNnSc6	Sarajevo
<g/>
.	.	kIx.	.
</s>
<s>
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
je	být	k5eAaImIp3nS	být
dopravním	dopravní	k2eAgInSc7d1	dopravní
uzlem	uzel	k1gInSc7	uzel
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
podlouhlého	podlouhlý	k2eAgInSc2d1	podlouhlý
východo-západního	východoápadní	k2eAgInSc2d1	východo-západní
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
v	v	k7c6	v
městské	městský	k2eAgFnSc6d1	městská
i	i	k8xC	i
meziměstské	meziměstský	k2eAgFnSc6d1	meziměstská
dopravě	doprava	k1gFnSc6	doprava
klade	klást	k5eAaImIp3nS	klást
nároky	nárok	k1gInPc4	nárok
právě	právě	k9	právě
na	na	k7c4	na
spojení	spojení	k1gNnSc4	spojení
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgFnPc7	tento
dvěma	dva	k4xCgFnPc7	dva
stranami	strana	k1gFnPc7	strana
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
tu	tu	k6eAd1	tu
hlavní	hlavní	k2eAgNnSc1d1	hlavní
železniční	železniční	k2eAgNnSc1d1	železniční
nádraží	nádraží	k1gNnSc1	nádraží
i	i	k8xC	i
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
<g/>
;	;	kIx,	;
místním	místní	k2eAgMnPc3d1	místní
obyvatelům	obyvatel	k1gMnPc3	obyvatel
slouží	sloužit	k5eAaImIp3nP	sloužit
tři	tři	k4xCgInPc1	tři
druhy	druh	k1gInPc1	druh
veřejné	veřejný	k2eAgFnSc2d1	veřejná
dopravy	doprava	k1gFnSc2	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
silničních	silniční	k2eAgFnPc2d1	silniční
dopravních	dopravní	k2eAgFnPc2d1	dopravní
tepen	tepna	k1gFnPc2	tepna
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
vedena	vést	k5eAaImNgFnS	vést
mezi	mezi	k7c7	mezi
východem	východ	k1gInSc7	východ
a	a	k8xC	a
západem	západ	k1gInSc7	západ
<g/>
;	;	kIx,	;
na	na	k7c6	na
západě	západ	k1gInSc6	západ
vycházejí	vycházet	k5eAaImIp3nP	vycházet
k	k	k7c3	k
Dálnici	dálnice	k1gFnSc3	dálnice
A	a	k9	a
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
vedoucí	vedoucí	k2eAgFnSc4d1	vedoucí
údolím	údolí	k1gNnSc7	údolí
řeky	řeka	k1gFnSc2	řeka
Bosny	Bosna	k1gFnSc2	Bosna
na	na	k7c4	na
sever	sever	k1gInSc4	sever
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Severo-jižní	Severoižní	k2eAgInPc1d1	Severo-jižní
spoje	spoj	k1gInPc1	spoj
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
značně	značně	k6eAd1	značně
dopravně	dopravně	k6eAd1	dopravně
vytíženy	vytížit	k5eAaPmNgInP	vytížit
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
ulice	ulice	k1gFnPc4	ulice
s	s	k7c7	s
největší	veliký	k2eAgFnSc7d3	veliký
mírou	míra	k1gFnSc7	míra
dopravního	dopravní	k2eAgInSc2d1	dopravní
ruchu	ruch	k1gInSc2	ruch
patří	patřit	k5eAaImIp3nS	patřit
Titova	Titův	k2eAgFnSc1d1	Titova
ulica	ulica	k1gFnSc1	ulica
a	a	k8xC	a
třída	třída	k1gFnSc1	třída
Zmaj	zmaj	k1gMnSc1	zmaj
od	od	k7c2	od
Bosne	Bosn	k1gInSc5	Bosn
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
nejmenší	malý	k2eAgNnSc4d3	nejmenší
vytížení	vytížení	k1gNnSc4	vytížení
a	a	k8xC	a
nejhorší	zlý	k2eAgFnPc4d3	nejhorší
dopravní	dopravní	k2eAgFnPc4d1	dopravní
podmínky	podmínka	k1gFnPc4	podmínka
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
svazích	svah	k1gInPc6	svah
okolo	okolo	k7c2	okolo
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
převážně	převážně	k6eAd1	převážně
vilová	vilový	k2eAgFnSc1d1	vilová
zástavba	zástavba	k1gFnSc1	zástavba
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
tři	tři	k4xCgInPc4	tři
již	již	k6eAd1	již
uvedené	uvedený	k2eAgInPc4d1	uvedený
druhy	druh	k1gInPc4	druh
dopravy	doprava	k1gFnSc2	doprava
patří	patřit	k5eAaImIp3nP	patřit
tramvaje	tramvaj	k1gFnPc1	tramvaj
(	(	kIx(	(
<g/>
tramvajová	tramvajový	k2eAgFnSc1d1	tramvajová
síť	síť	k1gFnSc1	síť
spojuje	spojovat	k5eAaImIp3nS	spojovat
právě	právě	k9	právě
staré	starý	k2eAgNnSc1d1	staré
město	město	k1gNnSc1	město
na	na	k7c6	na
východě	východ	k1gInSc6	východ
se	s	k7c7	s
sídlišti	sídliště	k1gNnPc7	sídliště
na	na	k7c6	na
západě	západ	k1gInSc6	západ
města	město	k1gNnSc2	město
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
páteřní	páteřní	k2eAgInSc4d1	páteřní
charakter	charakter	k1gInSc4	charakter
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
doplňované	doplňovaný	k2eAgInPc1d1	doplňovaný
trolejbusy	trolejbus	k1gInPc1	trolejbus
a	a	k8xC	a
autobusy	autobus	k1gInPc1	autobus
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
obsluhují	obsluhovat	k5eAaImIp3nP	obsluhovat
například	například	k6eAd1	například
strmé	strmý	k2eAgInPc1d1	strmý
svahy	svah	k1gInPc1	svah
údolí	údolí	k1gNnSc2	údolí
řeky	řeka	k1gFnSc2	řeka
Milajcky	Milajcka	k1gFnSc2	Milajcka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
jezdí	jezdit	k5eAaImIp3nP	jezdit
československé	československý	k2eAgFnPc1d1	Československá
a	a	k8xC	a
české	český	k2eAgFnPc1d1	Česká
tramvaje	tramvaj	k1gFnPc1	tramvaj
a	a	k8xC	a
trolejbusy	trolejbus	k1gInPc1	trolejbus
<g/>
.	.	kIx.	.
</s>
<s>
Sarajevské	sarajevský	k2eAgNnSc1d1	Sarajevské
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
(	(	kIx(	(
<g/>
IATA	IATA	kA	IATA
<g/>
:	:	kIx,	:
<g/>
SJJ	SJJ	kA	SJJ
<g/>
,	,	kIx,	,
do	do	k7c2	do
rozpadu	rozpad	k1gInSc2	rozpad
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
zvané	zvaný	k2eAgInPc1d1	zvaný
Butmir	Butmir	k1gInSc1	Butmir
<g/>
,	,	kIx,	,
známé	známý	k2eAgNnSc1d1	známé
z	z	k7c2	z
časů	čas	k1gInPc2	čas
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
jediným	jediný	k2eAgInSc7d1	jediný
koridorem	koridor	k1gInSc7	koridor
k	k	k7c3	k
obléhanému	obléhaný	k2eAgNnSc3d1	obléhané
městu	město	k1gNnSc3	město
využívaným	využívaný	k2eAgNnSc7d1	využívané
jednotkami	jednotka	k1gFnPc7	jednotka
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
na	na	k7c6	na
jihozápadě	jihozápad	k1gInSc6	jihozápad
metropole	metropol	k1gFnSc2	metropol
nedaleko	nedaleko	k7c2	nedaleko
řeky	řeka	k1gFnSc2	řeka
Bosny	Bosna	k1gFnSc2	Bosna
<g/>
.	.	kIx.	.
</s>
<s>
Letiště	letiště	k1gNnSc1	letiště
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
celostátního	celostátní	k2eAgNnSc2d1	celostátní
(	(	kIx(	(
<g/>
ne	ne	k9	ne
však	však	k9	však
z	z	k7c2	z
celoevropského	celoevropský	k2eAgMnSc2d1	celoevropský
<g/>
)	)	kIx)	)
hlediska	hledisko	k1gNnSc2	hledisko
velmi	velmi	k6eAd1	velmi
významné	významný	k2eAgNnSc4d1	významné
<g/>
;	;	kIx,	;
dopravu	doprava	k1gFnSc4	doprava
sem	sem	k6eAd1	sem
zajišťuje	zajišťovat	k5eAaImIp3nS	zajišťovat
mnoho	mnoho	k4c1	mnoho
dopravců	dopravce	k1gMnPc2	dopravce
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
B	B	kA	B
<g/>
&	&	k?	&
<g/>
H	H	kA	H
Airlines	Airlines	k1gMnSc1	Airlines
<g/>
,	,	kIx,	,
Austrian	Austrian	k1gMnSc1	Austrian
Airlines	Airlines	k1gMnSc1	Airlines
<g/>
,	,	kIx,	,
Alitalia	Alitalia	k1gFnSc1	Alitalia
<g/>
,	,	kIx,	,
Aero	aero	k1gNnSc1	aero
Flight	Flighta	k1gFnPc2	Flighta
<g/>
,	,	kIx,	,
Lufthansa	Lufthansa	k1gFnSc1	Lufthansa
<g/>
,	,	kIx,	,
Jat	jat	k2eAgInSc1d1	jat
Airways	Airways	k1gInSc1	Airways
(	(	kIx(	(
<g/>
srbské	srbský	k2eAgFnSc2d1	Srbská
aerolinie	aerolinie	k1gFnSc2	aerolinie
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Croatia	Croatia	k1gFnSc1	Croatia
Airlines	Airlines	k1gMnSc1	Airlines
a	a	k8xC	a
mnozí	mnohý	k2eAgMnPc1d1	mnohý
další	další	k2eAgMnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
bylo	být	k5eAaImAgNnS	být
na	na	k7c6	na
letišti	letiště	k1gNnSc6	letiště
odbaveno	odbaven	k2eAgNnSc4d1	odbaveno
397	[number]	k4	397
000	[number]	k4	000
cestujících	cestující	k1gMnPc2	cestující
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgInSc1d1	velký
rozdíl	rozdíl	k1gInSc1	rozdíl
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
to	ten	k3xDgNnSc1	ten
bylo	být	k5eAaImAgNnS	být
pouhých	pouhý	k2eAgInPc2d1	pouhý
25	[number]	k4	25
000	[number]	k4	000
Kulturní	kulturní	k2eAgInSc1d1	kulturní
život	život	k1gInSc1	život
ve	v	k7c6	v
městě	město	k1gNnSc6	město
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
tradic	tradice	k1gFnPc2	tradice
zdejších	zdejší	k2eAgMnPc2d1	zdejší
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
;	;	kIx,	;
pevně	pevně	k6eAd1	pevně
je	být	k5eAaImIp3nS	být
spjat	spjat	k2eAgMnSc1d1	spjat
s	s	k7c7	s
náboženským	náboženský	k2eAgMnSc7d1	náboženský
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
je	být	k5eAaImIp3nS	být
de	de	k?	de
facto	facto	k1gNnSc1	facto
symbolem	symbol	k1gInSc7	symbol
všech	všecek	k3xTgInPc2	všecek
tří	tři	k4xCgInPc2	tři
majoritních	majoritní	k2eAgInPc2d1	majoritní
národů	národ	k1gInPc2	národ
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
převažují	převažovat	k5eAaImIp3nP	převažovat
muslimští	muslimský	k2eAgMnPc1d1	muslimský
Bosňáci	Bosňáci	k?	Bosňáci
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
islám	islám	k1gInSc1	islám
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
tradice	tradice	k1gFnSc2	tradice
nezastupitelný	zastupitelný	k2eNgInSc4d1	nezastupitelný
vliv	vliv	k1gInSc4	vliv
na	na	k7c4	na
život	život	k1gInSc4	život
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
však	však	k9	však
neporovnatelně	porovnatelně	k6eNd1	porovnatelně
nižší	nízký	k2eAgMnSc1d2	nižší
než	než	k8xS	než
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
většinu	většina	k1gFnSc4	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
tvoří	tvořit	k5eAaImIp3nP	tvořit
právě	právě	k9	právě
muslimové	muslim	k1gMnPc1	muslim
<g/>
;	;	kIx,	;
lze	lze	k6eAd1	lze
tedy	tedy	k9	tedy
Sarajevo	Sarajevo	k1gNnSc4	Sarajevo
označit	označit	k5eAaPmF	označit
jako	jako	k8xS	jako
relativně	relativně	k6eAd1	relativně
multikulturní	multikulturní	k2eAgNnSc4d1	multikulturní
město	město	k1gNnSc4	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
kulturní	kulturní	k2eAgFnSc2d1	kulturní
vybavenosti	vybavenost	k1gFnSc2	vybavenost
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
disponuje	disponovat	k5eAaBmIp3nS	disponovat
všemi	všecek	k3xTgFnPc7	všecek
institucemi	instituce	k1gFnPc7	instituce
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
sídlo	sídlo	k1gNnSc4	sídlo
s	s	k7c7	s
několika	několik	k4yIc7	několik
sty	sto	k4xCgNnPc7	sto
tisíci	tisíc	k4xCgInPc7	tisíc
obyvateli	obyvatel	k1gMnPc7	obyvatel
nezbytné	nezbytný	k2eAgNnSc1d1	nezbytný
<g/>
.	.	kIx.	.
</s>
<s>
Sarajevanům	Sarajevan	k1gMnPc3	Sarajevan
slouží	sloužit	k5eAaImIp3nP	sloužit
několik	několik	k4yIc4	několik
muzeí	muzeum	k1gNnPc2	muzeum
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
zemského	zemský	k2eAgNnSc2d1	zemské
Zemaljski	Zemaljski	k1gNnSc2	Zemaljski
muzej	muzej	k1gInSc1	muzej
a	a	k8xC	a
městského	městský	k2eAgNnSc2d1	Městské
muzea	muzeum	k1gNnSc2	muzeum
Muzej	Muzej	k1gInSc4	Muzej
Sarajeva	Sarajevo	k1gNnSc2	Sarajevo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
škol	škola	k1gFnPc2	škola
i	i	k8xC	i
náboženských	náboženský	k2eAgInPc2d1	náboženský
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
starých	starý	k2eAgInPc2d1	starý
i	i	k8xC	i
několik	několik	k4yIc4	několik
set	sto	k4xCgNnPc2	sto
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
neposlední	poslední	k2eNgFnSc6d1	neposlední
řadě	řada	k1gFnSc6	řada
tu	tu	k6eAd1	tu
sídlí	sídlet	k5eAaImIp3nS	sídlet
také	také	k9	také
spousta	spousta	k1gFnSc1	spousta
divadel	divadlo	k1gNnPc2	divadlo
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yRgInPc2	který
je	být	k5eAaImIp3nS	být
nejvýznamnější	významný	k2eAgInSc1d3	nejvýznamnější
Národní	národní	k2eAgInSc1d1	národní
(	(	kIx(	(
<g/>
bosensky	bosensky	k6eAd1	bosensky
Narodno	Narodno	k1gNnSc4	Narodno
pozorište	pozorišit	k5eAaPmRp2nP	pozorišit
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
stojící	stojící	k2eAgMnSc1d1	stojící
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
<g/>
.	.	kIx.	.
</s>
<s>
Počínaje	počínaje	k7c7	počínaje
rokem	rok	k1gInSc7	rok
1949	[number]	k4	1949
má	mít	k5eAaImIp3nS	mít
město	město	k1gNnSc4	město
i	i	k8xC	i
svoji	svůj	k3xOyFgFnSc4	svůj
operu	opera	k1gFnSc4	opera
(	(	kIx(	(
<g/>
bosensky	bosensky	k6eAd1	bosensky
Sarajevska	Sarajevsko	k1gNnSc2	Sarajevsko
opera	opera	k1gFnSc1	opera
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
zahájila	zahájit	k5eAaPmAgFnS	zahájit
svoji	svůj	k3xOyFgFnSc4	svůj
činnost	činnost	k1gFnSc4	činnost
Prodanou	prodaný	k2eAgFnSc7d1	prodaná
nevěstou	nevěsta	k1gFnSc7	nevěsta
od	od	k7c2	od
Bedřicha	Bedřich	k1gMnSc2	Bedřich
Smetany	Smetana	k1gMnSc2	Smetana
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
každého	každý	k3xTgInSc2	každý
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
různé	různý	k2eAgInPc1d1	různý
festivaly	festival	k1gInPc1	festival
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
Baščaršijske	Baščaršijske	k1gFnSc1	Baščaršijske
noći	noć	k1gFnSc2	noć
(	(	kIx(	(
<g/>
Baščašijské	Baščašijský	k2eAgFnPc1d1	Baščašijský
noci	noc	k1gFnPc1	noc
<g/>
;	;	kIx,	;
hudební	hudební	k2eAgNnPc1d1	hudební
<g/>
,	,	kIx,	,
taneční	taneční	k2eAgNnPc1d1	taneční
a	a	k8xC	a
jiná	jiný	k2eAgNnPc1d1	jiné
kulturní	kulturní	k2eAgNnPc1d1	kulturní
představení	představení	k1gNnPc1	představení
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Sarajevska	Sarajevsko	k1gNnSc2	Sarajevsko
zima	zima	k1gFnSc1	zima
(	(	kIx(	(
<g/>
převážně	převážně	k6eAd1	převážně
výstavy	výstava	k1gFnPc4	výstava
<g/>
)	)	kIx)	)
či	či	k8xC	či
Sarajevský	sarajevský	k2eAgInSc1d1	sarajevský
jazzový	jazzový	k2eAgInSc1d1	jazzový
festival	festival	k1gInSc1	festival
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
se	se	k3xPyFc4	se
koná	konat	k5eAaImIp3nS	konat
každoročně	každoročně	k6eAd1	každoročně
také	také	k9	také
i	i	k9	i
festival	festival	k1gInSc1	festival
filmový	filmový	k2eAgInSc1d1	filmový
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
dob	doba	k1gFnPc2	doba
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
bylo	být	k5eAaImAgNnS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
známé	známá	k1gFnSc2	známá
také	také	k9	také
jako	jako	k9	jako
centrum	centrum	k1gNnSc4	centrum
pop	pop	k1gMnSc1	pop
rocku	rock	k1gInSc2	rock
<g/>
;	;	kIx,	;
některé	některý	k3yIgNnSc4	některý
dnes	dnes	k6eAd1	dnes
úspěšné	úspěšný	k2eAgFnPc1d1	úspěšná
místní	místní	k2eAgFnPc1d1	místní
skupiny	skupina	k1gFnPc1	skupina
pocházejí	pocházet	k5eAaImIp3nP	pocházet
právě	právě	k9	právě
ze	z	k7c2	z
Sarajeva	Sarajevo	k1gNnSc2	Sarajevo
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Sarajevské	sarajevský	k2eAgFnSc2d1	Sarajevská
památky	památka	k1gFnSc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
dlouhé	dlouhý	k2eAgFnSc3d1	dlouhá
historii	historie	k1gFnSc3	historie
zahrnující	zahrnující	k2eAgFnSc1d1	zahrnující
mnoho	mnoho	k6eAd1	mnoho
období	období	k1gNnSc3	období
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
střídaly	střídat	k5eAaImAgFnP	střídat
různé	různý	k2eAgFnPc1d1	různá
kultury	kultura	k1gFnPc1	kultura
i	i	k8xC	i
náboženství	náboženství	k1gNnPc1	náboženství
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Bosny	Bosna	k1gFnSc2	Bosna
a	a	k8xC	a
Hercegoviny	Hercegovina	k1gFnSc2	Hercegovina
relativně	relativně	k6eAd1	relativně
bohaté	bohatý	k2eAgInPc1d1	bohatý
na	na	k7c4	na
množství	množství	k1gNnSc4	množství
různých	různý	k2eAgFnPc2d1	různá
památek	památka	k1gFnPc2	památka
<g/>
,	,	kIx,	,
jak	jak	k6eAd1	jak
křesťanských	křesťanský	k2eAgNnPc2d1	křesťanské
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
muslimských	muslimský	k2eAgMnPc2d1	muslimský
<g/>
,	,	kIx,	,
či	či	k8xC	či
židovských	židovská	k1gFnPc2	židovská
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
území	území	k1gNnSc6	území
Sarajeva	Sarajevo	k1gNnSc2	Sarajevo
se	se	k3xPyFc4	se
nacházely	nacházet	k5eAaImAgFnP	nacházet
různé	různý	k2eAgFnPc1d1	různá
muslimské	muslimský	k2eAgFnPc1d1	muslimská
sakrální	sakrální	k2eAgFnPc1d1	sakrální
stavby	stavba	k1gFnPc1	stavba
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
které	který	k3yIgInPc4	který
lze	lze	k6eAd1	lze
zařadit	zařadit	k5eAaPmF	zařadit
například	například	k6eAd1	například
mešitu	mešita	k1gFnSc4	mešita
Mehmed-bega	Mehmedega	k1gFnSc1	Mehmed-bega
Minetoviće	Minetoviće	k1gFnSc1	Minetoviće
(	(	kIx(	(
<g/>
stála	stát	k5eAaImAgFnS	stát
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1463	[number]	k4	1463
<g/>
-	-	kIx~	-
<g/>
1464	[number]	k4	1464
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prvního	první	k4xOgMnSc2	první
bosenského	bosenský	k2eAgMnSc2d1	bosenský
sandžakbega	sandžakbeg	k1gMnSc2	sandžakbeg
<g/>
,	,	kIx,	,
mešitu	mešita	k1gFnSc4	mešita
Bali-bega	Balieg	k1gMnSc2	Bali-beg
Malkočeviće	Malkočević	k1gMnSc2	Malkočević
(	(	kIx(	(
<g/>
1475	[number]	k4	1475
<g/>
-	-	kIx~	-
<g/>
1476	[number]	k4	1476
<g/>
)	)	kIx)	)
na	na	k7c4	na
Bistriku	Bistrika	k1gFnSc4	Bistrika
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
sem	sem	k6eAd1	sem
také	také	k9	také
patří	patřit	k5eAaImIp3nS	patřit
mešita	mešita	k1gFnSc1	mešita
Ajas-bega	Ajasega	k1gFnSc1	Ajas-bega
<g/>
,	,	kIx,	,
Jahja-pašova	Jahjaašův	k2eAgFnSc1d1	Jahja-pašův
mešita	mešita	k1gFnSc1	mešita
<g/>
,	,	kIx,	,
mešita	mešita	k1gFnSc1	mešita
Sarače-Ismaila	Sarače-Ismaila	k1gFnSc1	Sarače-Ismaila
a	a	k8xC	a
mnoho	mnoho	k4c1	mnoho
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
však	však	k9	však
nedochovala	dochovat	k5eNaPmAgFnS	dochovat
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Totéž	týž	k3xTgNnSc1	týž
i	i	k9	i
platí	platit	k5eAaImIp3nS	platit
o	o	k7c6	o
křesťanských	křesťanský	k2eAgFnPc6d1	křesťanská
stavbách	stavba	k1gFnPc6	stavba
<g/>
;	;	kIx,	;
původní	původní	k2eAgFnSc2d1	původní
předosmanské	předosmanský	k2eAgFnSc2d1	předosmanský
stavby	stavba	k1gFnSc2	stavba
bosenské	bosenský	k2eAgFnSc2d1	bosenská
církve	církev	k1gFnSc2	církev
byly	být	k5eAaImAgFnP	být
buď	buď	k8xC	buď
přebudovány	přebudován	k2eAgFnPc1d1	přebudována
<g/>
,	,	kIx,	,
či	či	k8xC	či
zničeny	zničit	k5eAaPmNgFnP	zničit
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Osmanů	Osman	k1gMnPc2	Osman
<g/>
.	.	kIx.	.
</s>
<s>
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
má	mít	k5eAaImIp3nS	mít
také	také	k9	také
ale	ale	k9	ale
i	i	k9	i
pamětihodnosti	pamětihodnost	k1gFnPc1	pamětihodnost
z	z	k7c2	z
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
jeho	jeho	k3xOp3gInPc4	jeho
konce	konec	k1gInPc4	konec
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
Rakousko-Uherské	rakouskoherský	k2eAgFnSc2d1	rakousko-uherská
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
a	a	k8xC	a
též	též	k9	též
i	i	k9	i
ze	z	k7c2	z
století	století	k1gNnSc2	století
dvacátého	dvacátý	k4xOgMnSc2	dvacátý
<g/>
,	,	kIx,	,
z	z	k7c2	z
časů	čas	k1gInPc2	čas
Království	království	k1gNnSc2	království
SHS	SHS	kA	SHS
a	a	k8xC	a
Jugoslávie	Jugoslávie	k1gFnSc2	Jugoslávie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
tyto	tento	k3xDgFnPc4	tento
budovy	budova	k1gFnPc4	budova
lze	lze	k6eAd1	lze
zařadit	zařadit	k5eAaPmF	zařadit
především	především	k9	především
stavby	stavba	k1gFnPc1	stavba
architekta	architekt	k1gMnSc2	architekt
Karla	Karel	k1gMnSc2	Karel
Paříka	Pařík	k1gMnSc2	Pařík
-	-	kIx~	-
knihovnu	knihovna	k1gFnSc4	knihovna
<g/>
,	,	kIx,	,
Katedrálu	katedrála	k1gFnSc4	katedrála
srdce	srdce	k1gNnSc2	srdce
Ježíšova	Ježíšův	k2eAgNnSc2d1	Ježíšovo
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
katedrálu	katedrála	k1gFnSc4	katedrála
Vrhbosenské	Vrhbosenský	k2eAgFnSc2d1	Vrhbosenský
arcidiecéze	arcidiecéze	k1gFnSc2	arcidiecéze
<g/>
,	,	kIx,	,
Zemské	zemský	k2eAgNnSc4d1	zemské
muzeum	muzeum	k1gNnSc4	muzeum
a	a	k8xC	a
Aškenazskou	Aškenazský	k2eAgFnSc4d1	Aškenazský
synagogu	synagoga	k1gFnSc4	synagoga
(	(	kIx(	(
<g/>
Sarajevo	Sarajevo	k1gNnSc4	Sarajevo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgFnPc4d1	významná
stavby	stavba	k1gFnPc4	stavba
tohoto	tento	k3xDgNnSc2	tento
období	období	k1gNnSc1	období
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
Národní	národní	k2eAgNnSc1d1	národní
divadlo	divadlo	k1gNnSc1	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Sarajevo	Sarajevo	k1gNnSc1	Sarajevo
má	mít	k5eAaImIp3nS	mít
ve	v	k7c6	v
školství	školství	k1gNnSc6	školství
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
tradici	tradice	k1gFnSc4	tradice
<g/>
;	;	kIx,	;
první	první	k4xOgFnSc1	první
univerzita	univerzita	k1gFnSc1	univerzita
tu	tu	k6eAd1	tu
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
roku	rok	k1gInSc2	rok
1531	[number]	k4	1531
<g/>
,	,	kIx,	,
založil	založit	k5eAaPmAgMnS	založit
ji	on	k3xPp3gFnSc4	on
Gazi	Gaze	k1gFnSc4	Gaze
Husrev-beg	Husreveg	k1gMnSc1	Husrev-beg
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
správce	správce	k1gMnSc1	správce
Bosny	Bosna	k1gFnSc2	Bosna
za	za	k7c2	za
Osmanské	osmanský	k2eAgFnSc2d1	Osmanská
nadvlády	nadvláda	k1gFnSc2	nadvláda
<g/>
.	.	kIx.	.
</s>
<s>
Jmenovala	jmenovat	k5eAaImAgFnS	jmenovat
se	se	k3xPyFc4	se
Chaniká	Chaniká	k1gFnSc1	Chaniká
<g/>
,	,	kIx,	,
a	a	k8xC	a
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
súfijskou	súfijský	k2eAgFnSc4d1	súfijská
školu	škola	k1gFnSc4	škola
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
šest	šest	k4xCc4	šest
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
zde	zde	k6eAd1	zde
přibyly	přibýt	k5eAaPmAgInP	přibýt
i	i	k9	i
další	další	k2eAgInPc1d1	další
obory	obor	k1gInPc1	obor
<g/>
.	.	kIx.	.
</s>
<s>
Vyučovalo	vyučovat	k5eAaImAgNnS	vyučovat
se	se	k3xPyFc4	se
právo	právo	k1gNnSc1	právo
(	(	kIx(	(
<g/>
šaría	šaría	k1gFnSc1	šaría
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teologie	teologie	k1gFnSc1	teologie
<g/>
,	,	kIx,	,
filozofie	filozofie	k1gFnSc1	filozofie
<g/>
,	,	kIx,	,
existovala	existovat	k5eAaImAgFnS	existovat
také	také	k9	také
i	i	k9	i
univerzitní	univerzitní	k2eAgFnSc1d1	univerzitní
knihovna	knihovna	k1gFnSc1	knihovna
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
nejvýznamnější	významný	k2eAgFnSc4d3	nejvýznamnější
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Bosně	Bosna	k1gFnSc6	Bosna
a	a	k8xC	a
Hercegovině	Hercegovina	k1gFnSc6	Hercegovina
<g/>
;	;	kIx,	;
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
s	s	k7c7	s
tehdejší	tehdejší	k2eAgFnSc7d1	tehdejší
istanbulskou	istanbulský	k2eAgFnSc7d1	Istanbulská
medresou	medresa	k1gFnSc7	medresa
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
zcela	zcela	k6eAd1	zcela
sekulární	sekulární	k2eAgFnSc1d1	sekulární
škola	škola	k1gFnSc1	škola
(	(	kIx(	(
<g/>
Sarajevská	sarajevský	k2eAgFnSc1d1	Sarajevská
univerzita	univerzita	k1gFnSc1	univerzita
Univerzitet	Univerziteta	k1gFnPc2	Univerziteta
u	u	k7c2	u
Sarajevu	Sarajevo	k1gNnSc3	Sarajevo
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
otevřena	otevřít	k5eAaPmNgFnS	otevřít
až	až	k9	až
roku	rok	k1gInSc2	rok
1940	[number]	k4	1940
(	(	kIx(	(
<g/>
svůj	svůj	k3xOyFgInSc4	svůj
současný	současný	k2eAgInSc4d1	současný
název	název	k1gInSc4	název
ale	ale	k8xC	ale
získala	získat	k5eAaPmAgFnS	získat
až	až	k9	až
o	o	k7c4	o
dalších	další	k2eAgNnPc2d1	další
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
později	pozdě	k6eAd2	pozdě
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
během	během	k7c2	během
celých	celý	k2eAgNnPc2d1	celé
let	léto	k1gNnPc2	léto
čtyřicátých	čtyřicátý	k4xOgNnPc6	čtyřicátý
vznikalo	vznikat	k5eAaImAgNnS	vznikat
postupně	postupně	k6eAd1	postupně
několik	několik	k4yIc1	několik
fakult	fakulta	k1gFnPc2	fakulta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
univerzita	univerzita	k1gFnSc1	univerzita
dále	daleko	k6eAd2	daleko
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
a	a	k8xC	a
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
vytvářet	vytvářet	k5eAaImF	vytvářet
nové	nový	k2eAgFnPc4d1	nová
školy	škola	k1gFnPc4	škola
podobného	podobný	k2eAgInSc2d1	podobný
typu	typ	k1gInSc2	typ
v	v	k7c4	v
Banja	banjo	k1gNnPc4	banjo
Luce	Luc	k1gFnSc2	Luc
<g/>
,	,	kIx,	,
Mostaru	Mostar	k1gInSc2	Mostar
a	a	k8xC	a
Tuzle	Tuzle	k1gFnSc2	Tuzle
<g/>
.	.	kIx.	.
70	[number]	k4	70
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
léta	léto	k1gNnSc2	léto
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
ve	v	k7c6	v
znamení	znamení	k1gNnSc6	znamení
oddělování	oddělování	k1gNnSc2	oddělování
některých	některý	k3yIgFnPc2	některý
fakult	fakulta	k1gFnPc2	fakulta
od	od	k7c2	od
univerzity	univerzita	k1gFnSc2	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Přibývají	přibývat	k5eAaImIp3nP	přibývat
studijní	studijní	k2eAgInPc4d1	studijní
obory	obor	k1gInPc4	obor
(	(	kIx(	(
<g/>
nové	nový	k2eAgInPc4d1	nový
technické	technický	k2eAgInPc4d1	technický
obory	obor	k1gInPc4	obor
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
stagnaci	stagnace	k1gFnSc3	stagnace
na	na	k7c6	na
konci	konec	k1gInSc6	konec
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
však	však	k9	však
situace	situace	k1gFnSc1	situace
ve	v	k7c6	v
školství	školství	k1gNnSc6	školství
celkově	celkově	k6eAd1	celkově
zhoršuje	zhoršovat	k5eAaImIp3nS	zhoršovat
<g/>
.	.	kIx.	.
</s>
<s>
Sarajevská	sarajevský	k2eAgFnSc1d1	Sarajevská
univerzita	univerzita	k1gFnSc1	univerzita
obnovila	obnovit	k5eAaPmAgFnS	obnovit
činnost	činnost	k1gFnSc4	činnost
první	první	k4xOgInSc4	první
rok	rok	k1gInSc4	rok
po	po	k7c6	po
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešní	dnešní	k2eAgFnSc6d1	dnešní
době	doba	k1gFnSc6	doba
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
s	s	k7c7	s
dalšími	další	k2eAgFnPc7d1	další
vysokými	vysoký	k2eAgFnPc7d1	vysoká
školami	škola	k1gFnPc7	škola
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
v	v	k7c6	v
USA	USA	kA	USA
a	a	k8xC	a
v	v	k7c6	v
arabském	arabský	k2eAgInSc6d1	arabský
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Sarajevanům	Sarajevan	k1gMnPc3	Sarajevan
slouží	sloužit	k5eAaImIp3nS	sloužit
kromě	kromě	k7c2	kromě
vysokého	vysoký	k2eAgNnSc2d1	vysoké
školství	školství	k1gNnSc2	školství
též	též	k9	též
kolem	kolem	k7c2	kolem
čtyřiceti	čtyřicet	k4xCc2	čtyřicet
základních	základní	k2eAgMnPc2d1	základní
a	a	k8xC	a
třiceti	třicet	k4xCc2	třicet
středních	střední	k2eAgFnPc2d1	střední
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
</s>
<s>
Sarajevo	Sarajevo	k1gNnSc4	Sarajevo
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
upoutalo	upoutat	k5eAaPmAgNnS	upoutat
pozornost	pozornost	k1gFnSc4	pozornost
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
pořádáním	pořádání	k1gNnSc7	pořádání
zimních	zimní	k2eAgFnPc2d1	zimní
olympijských	olympijský	k2eAgFnPc2d1	olympijská
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ty	k3xPp2nSc1	ty
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
konaly	konat	k5eAaImAgFnP	konat
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
Jugoslávii	Jugoslávie	k1gFnSc6	Jugoslávie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
i	i	k9	i
socialistické	socialistický	k2eAgFnSc6d1	socialistická
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
země	země	k1gFnSc1	země
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
o	o	k7c6	o
nejlepší	dobrý	k2eAgFnSc6d3	nejlepší
prezentaci	prezentace	k1gFnSc6	prezentace
<g/>
,	,	kIx,	,
paradoxně	paradoxně	k6eAd1	paradoxně
v	v	k7c6	v
situaci	situace	k1gFnSc6	situace
hospodářské	hospodářský	k2eAgFnSc2d1	hospodářská
krize	krize	k1gFnSc2	krize
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
a	a	k8xC	a
znepokojující	znepokojující	k2eAgInPc4d1	znepokojující
problémy	problém	k1gInPc4	problém
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
jejích	její	k3xOp3gFnPc6	její
částech	část	k1gFnPc6	část
<g/>
.	.	kIx.	.
</s>
<s>
Investovalo	investovat	k5eAaBmAgNnS	investovat
se	se	k3xPyFc4	se
do	do	k7c2	do
mnohých	mnohý	k2eAgNnPc2d1	mnohé
sportovních	sportovní	k2eAgNnPc2d1	sportovní
zařízení	zařízení	k1gNnPc2	zařízení
a	a	k8xC	a
areálů	areál	k1gInPc2	areál
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
původně	původně	k6eAd1	původně
patřičné	patřičný	k2eAgNnSc4d1	patřičné
zázemí	zázemí	k1gNnSc4	zázemí
neměla	mít	k5eNaImAgFnS	mít
a	a	k8xC	a
kde	kde	k6eAd1	kde
nebyla	být	k5eNaImAgFnS	být
rozvinuta	rozvinut	k2eAgFnSc1d1	rozvinuta
sportovní	sportovní	k2eAgFnSc1d1	sportovní
tradice	tradice	k1gFnSc1	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ještě	ještě	k9	ještě
dnes	dnes	k6eAd1	dnes
mnoho	mnoho	k4c4	mnoho
komplexů	komplex	k1gInPc2	komplex
pro	pro	k7c4	pro
zimní	zimní	k2eAgFnPc4d1	zimní
aktivity	aktivita	k1gFnPc4	aktivita
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
byla	být	k5eAaImAgFnS	být
během	během	k7c2	během
olympiády	olympiáda	k1gFnSc2	olympiáda
využívána	využívat	k5eAaPmNgFnS	využívat
a	a	k8xC	a
byly	být	k5eAaImAgFnP	být
ušetřeny	ušetřit	k5eAaPmNgFnP	ušetřit
válečných	válečný	k2eAgFnPc2d1	válečná
událostí	událost	k1gFnSc7	událost
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
stadion	stadion	k1gNnSc1	stadion
Koševo	Koševo	k1gNnSc1	Koševo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgInSc1d1	populární
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
fotbal	fotbal	k1gInSc4	fotbal
<g/>
;	;	kIx,	;
ve	v	k7c6	v
městě	město	k1gNnSc6	město
sídlí	sídlet	k5eAaImIp3nP	sídlet
dva	dva	k4xCgMnPc1	dva
prvoligoví	prvoligový	k2eAgMnPc1d1	prvoligový
rivalové	rival	k1gMnPc1	rival
(	(	kIx(	(
<g/>
FK	FK	kA	FK
Željezničar	Željezničara	k1gFnPc2	Željezničara
(	(	kIx(	(
<g/>
od	od	k7c2	od
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
a	a	k8xC	a
FK	FK	kA	FK
Sarajevo	Sarajevo	k1gNnSc4	Sarajevo
(	(	kIx(	(
<g/>
od	od	k7c2	od
1946	[number]	k4	1946
<g/>
))	))	k?	))
<g/>
,	,	kIx,	,
další	další	k2eAgInPc1d1	další
kluby	klub	k1gInPc1	klub
se	se	k3xPyFc4	se
účastní	účastnit	k5eAaImIp3nP	účastnit
nižších	nízký	k2eAgFnPc2d2	nižší
soutěží	soutěž	k1gFnPc2	soutěž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dalších	další	k2eAgInPc6d1	další
sportech	sport	k1gInPc6	sport
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
basketbalu	basketbal	k1gInSc2	basketbal
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
úspěšný	úspěšný	k2eAgInSc1d1	úspěšný
tým	tým	k1gInSc1	tým
KK	KK	kA	KK
Bosna	Bosna	k1gFnSc1	Bosna
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
proslavil	proslavit	k5eAaPmAgMnS	proslavit
hlavně	hlavně	k9	hlavně
vítězstvím	vítězství	k1gNnSc7	vítězství
v	v	k7c6	v
evropském	evropský	k2eAgInSc6d1	evropský
poháru	pohár	k1gInSc6	pohár
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1979	[number]	k4	1979
<g/>
.	.	kIx.	.
</s>
