<s>
CLAMP	CLAMP	kA	CLAMP
(	(	kIx(	(
<g/>
ク	ク	k?	ク
Kuranpu	Kuranp	k1gInSc2	Kuranp
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
japonská	japonský	k2eAgFnSc1d1	japonská
ženská	ženský	k2eAgFnSc1d1	ženská
kreativní	kreativní	k2eAgFnSc1d1	kreativní
skupina	skupina	k1gFnSc1	skupina
<g/>
,	,	kIx,	,
soustřeďující	soustřeďující	k2eAgFnSc1d1	soustřeďující
se	se	k3xPyFc4	se
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
tvorbu	tvorba	k1gFnSc4	tvorba
mangy	mango	k1gNnPc7	mango
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gNnPc1	jejich
manga	mango	k1gNnPc1	mango
byla	být	k5eAaImAgNnP	být
po	po	k7c4	po
vypuštění	vypuštění	k1gNnSc4	vypuštění
na	na	k7c4	na
trh	trh	k1gInSc4	trh
častokrát	častokrát	k6eAd1	častokrát
přetvořena	přetvořen	k2eAgFnSc1d1	přetvořena
v	v	k7c6	v
anime	animat	k5eAaPmIp3nS	animat
<g/>
.	.	kIx.	.
</s>
<s>
CLAMP	CLAMP	kA	CLAMP
prodal	prodat	k5eAaPmAgInS	prodat
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
více	hodně	k6eAd2	hodně
než	než	k8xS	než
90	[number]	k4	90
milionů	milion	k4xCgInPc2	milion
tankóbonů	tankóbon	k1gInPc2	tankóbon
(	(	kIx(	(
<g/>
dílů	díl	k1gInPc2	díl
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
sérií	série	k1gFnPc2	série
mangy	mango	k1gNnPc7	mango
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
CLAMP	CLAMP	kA	CLAMP
byl	být	k5eAaImAgInS	být
založen	založit	k5eAaPmNgInS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
jako	jako	k8xC	jako
dvanáctičlenný	dvanáctičlenný	k2eAgInSc1d1	dvanáctičlenný
kruh	kruh	k1gInSc1	kruh
doudžinši	doudžinsat	k5eAaPmIp1nSwK	doudžinsat
(	(	kIx(	(
<g/>
autoři	autor	k1gMnPc1	autor
mangy	mango	k1gNnPc7	mango
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejšími	tehdejší	k2eAgInPc7d1	tehdejší
členy	člen	k1gInPc7	člen
byly	být	k5eAaImAgInP	být
<g/>
:	:	kIx,	:
Ageha	Ageh	k1gMnSc4	Ageh
Ohkawa	Ohkawus	k1gMnSc4	Ohkawus
<g/>
,	,	kIx,	,
Tsubaki	Tsubaki	k1gNnPc7	Tsubaki
Nekoi	Neko	k1gFnSc2	Neko
<g/>
,	,	kIx,	,
Mokona	Mokona	k1gFnSc1	Mokona
<g/>
,	,	kIx,	,
Satsuki	Satsuki	k1gNnSc1	Satsuki
Igarashi	Igarash	k1gFnSc2	Igarash
<g/>
,	,	kIx,	,
Tamayo	Tamayo	k6eAd1	Tamayo
Akiyama	Akiyama	k1gFnSc1	Akiyama
<g/>
,	,	kIx,	,
Soushi	Soushi	k1gNnSc1	Soushi
Hisagi	Hisag	k1gFnSc2	Hisag
<g/>
,	,	kIx,	,
O-Kyon	O-Kyon	k1gNnSc1	O-Kyon
<g/>
,	,	kIx,	,
Kazue	Kazue	k1gNnSc1	Kazue
Nakamori	Nakamor	k1gFnSc2	Nakamor
<g/>
,	,	kIx,	,
Inoue	Inou	k1gFnSc2	Inou
Yuzuru	Yuzur	k1gInSc2	Yuzur
<g/>
,	,	kIx,	,
Sei	Sei	k1gMnSc1	Sei
Nanao	Nanao	k1gMnSc1	Nanao
<g/>
,	,	kIx,	,
Shinya	Shinya	k1gMnSc1	Shinya
Ohmi	Ohm	k1gFnSc2	Ohm
a	a	k8xC	a
Leeza	Leez	k1gMnSc2	Leez
Sei	Sei	k1gMnSc2	Sei
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
kruh	kruh	k1gInSc1	kruh
zredukoval	zredukovat	k5eAaPmAgInS	zredukovat
z	z	k7c2	z
dvanácti	dvanáct	k4xCc2	dvanáct
na	na	k7c4	na
sedm	sedm	k4xCc4	sedm
členů	člen	k1gInPc2	člen
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
až	až	k9	až
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
(	(	kIx(	(
<g/>
když	když	k8xS	když
během	během	k7c2	během
prací	práce	k1gFnPc2	práce
na	na	k7c6	na
RG	RG	kA	RG
Veda	vést	k5eAaImSgInS	vést
odešly	odejít	k5eAaPmAgFnP	odejít
Tamayo	Tamayo	k1gNnSc4	Tamayo
Akiyama	Akiyamum	k1gNnSc2	Akiyamum
<g/>
,	,	kIx,	,
Sei	Sei	k1gMnSc1	Sei
Nanao	Nanao	k1gMnSc1	Nanao
a	a	k8xC	a
Leeza	Leeza	k1gFnSc1	Leeza
Sei	Sei	k1gFnSc2	Sei
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Současnými	současný	k2eAgFnPc7d1	současná
členkami	členka	k1gFnPc7	členka
jsou	být	k5eAaImIp3nP	být
Ageha	Ageh	k1gMnSc2	Ageh
Ohkawa	Ohkawus	k1gMnSc2	Ohkawus
<g/>
,	,	kIx,	,
Tsubaki	Tsubak	k1gFnSc2	Tsubak
Nekoi	Neko	k1gFnSc2	Neko
<g/>
,	,	kIx,	,
Mokona	Mokona	k1gFnSc1	Mokona
a	a	k8xC	a
Satsuki	Satsuki	k1gNnSc1	Satsuki
Igarashi	Igarash	k1gFnSc2	Igarash
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
skupiny	skupina	k1gFnSc2	skupina
má	mít	k5eAaImIp3nS	mít
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
původ	původ	k1gInSc4	původ
v	v	k7c6	v
anglickém	anglický	k2eAgNnSc6d1	anglické
slově	slovo	k1gNnSc6	slovo
"	"	kIx"	"
<g/>
clump	clump	k1gMnSc1	clump
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamená	znamenat	k5eAaImIp3nS	znamenat
hromada	hromada	k1gFnSc1	hromada
brambor	brambora	k1gFnPc2	brambora
<g/>
.	.	kIx.	.
</s>
<s>
Narozena	narozen	k2eAgFnSc1d1	narozena
<g/>
:	:	kIx,	:
2	[number]	k4	2
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1967	[number]	k4	1967
v	v	k7c6	v
Osace	Osaka	k1gFnSc6	Osaka
Krevní	krevní	k2eAgFnSc1d1	krevní
skupina	skupina	k1gFnSc1	skupina
<g/>
:	:	kIx,	:
A	a	k8xC	a
Ohkawa	Ohkawa	k1gFnSc1	Ohkawa
je	být	k5eAaImIp3nS	být
coby	coby	k?	coby
spisovatelka	spisovatelka	k1gFnSc1	spisovatelka
vůdcem	vůdce	k1gMnSc7	vůdce
celého	celý	k2eAgInSc2d1	celý
týmu	tým	k1gInSc2	tým
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
prací	práce	k1gFnSc7	práce
je	být	k5eAaImIp3nS	být
také	také	k9	také
vyjednávání	vyjednávání	k1gNnSc4	vyjednávání
s	s	k7c7	s
jejich	jejich	k3xOp3gMnPc7	jejich
vydavateli	vydavatel	k1gMnPc7	vydavatel
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
dostává	dostávat	k5eAaImIp3nS	dostávat
i	i	k9	i
do	do	k7c2	do
role	role	k1gFnSc2	role
scenáristky	scenáristka	k1gFnSc2	scenáristka
a	a	k8xC	a
sice	sice	k8xC	sice
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
<g/>
-li	i	k?	-li
jejich	jejich	k3xOp3gFnPc7	jejich
mangy	mango	k1gNnPc7	mango
převáděny	převáděn	k2eAgInPc1d1	převáděn
na	na	k7c4	na
filmové	filmový	k2eAgNnSc4d1	filmové
plátno	plátno	k1gNnSc4	plátno
<g/>
.	.	kIx.	.
</s>
<s>
Narozena	narozen	k2eAgFnSc1d1	narozena
<g/>
:	:	kIx,	:
16	[number]	k4	16
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1968	[number]	k4	1968
v	v	k7c6	v
Kjótu	Kjóto	k1gNnSc6	Kjóto
Krevní	krevní	k2eAgFnSc1d1	krevní
skupina	skupina	k1gFnSc1	skupina
<g/>
:	:	kIx,	:
A	a	k8xC	a
Mokona	Mokona	k1gFnSc1	Mokona
je	být	k5eAaImIp3nS	být
hlavním	hlavní	k2eAgMnSc7d1	hlavní
návrhářem	návrhář	k1gMnSc7	návrhář
většiny	většina	k1gFnSc2	většina
příběhů	příběh	k1gInPc2	příběh
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
jako	jako	k9	jako
výtvarnicí	výtvarnice	k1gFnSc7	výtvarnice
některých	některý	k3yIgInPc2	některý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnPc1	její
díla	dílo	k1gNnPc1	dílo
jsou	být	k5eAaImIp3nP	být
obchodní	obchodní	k2eAgFnSc7d1	obchodní
značkou	značka	k1gFnSc7	značka
celého	celý	k2eAgNnSc2d1	celé
studia	studio	k1gNnSc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Narozena	narozen	k2eAgFnSc1d1	narozena
<g/>
:	:	kIx,	:
21	[number]	k4	21
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1969	[number]	k4	1969
v	v	k7c6	v
Kjótu	Kjóto	k1gNnSc6	Kjóto
Krevní	krevní	k2eAgFnSc1d1	krevní
skupina	skupina	k1gFnSc1	skupina
<g/>
:	:	kIx,	:
O	o	k7c6	o
Nekoi	Neko	k1gInSc6	Neko
je	být	k5eAaImIp3nS	být
povětšinou	povětšinou	k6eAd1	povětšinou
Mokoninou	Mokonin	k2eAgFnSc7d1	Mokonin
asistentkou	asistentka	k1gFnSc7	asistentka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
stojí	stát	k5eAaImIp3nS	stát
také	také	k9	také
za	za	k7c7	za
některými	některý	k3yIgFnPc7	některý
z	z	k7c2	z
děl	dělo	k1gNnPc2	dělo
(	(	kIx(	(
<g/>
Watashi	Watashi	k1gNnPc2	Watashi
no	no	k9	no
Suki	Suki	k1gNnSc7	Suki
na	na	k7c4	na
Hito	Hito	k1gNnSc4	Hito
<g/>
,	,	kIx,	,
Wish	Wish	k1gInSc1	Wish
<g/>
,	,	kIx,	,
Suki	Suki	k1gNnSc1	Suki
<g/>
:	:	kIx,	:
Dakara	Dakara	k1gFnSc1	Dakara
Suki	Suk	k1gFnSc2	Suk
<g/>
,	,	kIx,	,
Góhó	Góhó	k1gMnSc1	Góhó
Drug	Drug	k1gMnSc1	Drug
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc7	její
hlavní	hlavní	k2eAgFnSc7d1	hlavní
prací	práce	k1gFnSc7	práce
je	být	k5eAaImIp3nS	být
superdeformace	superdeformace	k1gFnSc1	superdeformace
hlavních	hlavní	k2eAgFnPc2d1	hlavní
postav	postava	k1gFnPc2	postava
a	a	k8xC	a
maskotů	maskot	k1gInPc2	maskot
a	a	k8xC	a
společně	společně	k6eAd1	společně
s	s	k7c7	s
Mokonou	Mokona	k1gFnSc7	Mokona
i	i	k8xC	i
vytváření	vytváření	k1gNnSc2	vytváření
jejich	jejich	k3xOp3gInPc2	jejich
normálních	normální	k2eAgInPc2d1	normální
protějšků	protějšek	k1gInPc2	protějšek
<g/>
.	.	kIx.	.
</s>
<s>
Narozena	narozen	k2eAgFnSc1d1	narozena
<g/>
:	:	kIx,	:
8	[number]	k4	8
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1969	[number]	k4	1969
v	v	k7c6	v
Kjótu	Kjóto	k1gNnSc6	Kjóto
Krevní	krevní	k2eAgFnSc1d1	krevní
skupina	skupina	k1gFnSc1	skupina
<g/>
:	:	kIx,	:
A	a	k8xC	a
Satsuki	Satsuki	k1gNnSc1	Satsuki
je	být	k5eAaImIp3nS	být
asistentkou	asistentka	k1gFnSc7	asistentka
Mokony	Mokona	k1gFnSc2	Mokona
a	a	k8xC	a
Nekoi	Neko	k1gFnSc2	Neko
v	v	k7c6	v
jejich	jejich	k3xOp3gFnSc6	jejich
práci	práce	k1gFnSc6	práce
a	a	k8xC	a
také	také	k9	také
dohlíží	dohlížet	k5eAaImIp3nS	dohlížet
na	na	k7c4	na
design	design	k1gInSc4	design
tankóbun	tankóbuna	k1gFnPc2	tankóbuna
<g/>
.	.	kIx.	.
</s>
<s>
Mimo	mimo	k6eAd1	mimo
to	ten	k3xDgNnSc1	ten
píše	psát	k5eAaImIp3nS	psát
každý	každý	k3xTgInSc4	každý
měsíc	měsíc	k1gInSc4	měsíc
do	do	k7c2	do
časopisu	časopis	k1gInSc2	časopis
Kadokawa	Kadokaw	k1gInSc2	Kadokaw
Newtype	Newtyp	k1gInSc5	Newtyp
Magazine	Magazin	k1gInSc5	Magazin
(	(	kIx(	(
<g/>
magazín	magazín	k1gInSc4	magazín
specializující	specializující	k2eAgInSc4d1	specializující
se	se	k3xPyFc4	se
na	na	k7c6	na
události	událost	k1gFnSc6	událost
anime	animat	k5eAaPmIp3nS	animat
a	a	k8xC	a
mangy	mango	k1gNnPc7	mango
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
si	se	k3xPyFc3	se
členky	členka	k1gFnSc2	členka
k	k	k7c3	k
15	[number]	k4	15
<g/>
.	.	kIx.	.
<g/>
výročí	výročí	k1gNnSc3	výročí
CLAMPu	CLAMPus	k1gInSc2	CLAMPus
změnily	změnit	k5eAaPmAgFnP	změnit
jména	jméno	k1gNnPc1	jméno
(	(	kIx(	(
<g/>
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
uváděna	uváděn	k2eAgNnPc1d1	uváděno
jména	jméno	k1gNnPc1	jméno
dřívější	dřívější	k2eAgNnPc1d1	dřívější
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Srpnové	srpnový	k2eAgNnSc1d1	srpnové
číslo	číslo	k1gNnSc1	číslo
Newtype	Newtyp	k1gInSc5	Newtyp
USA	USA	kA	USA
napsalo	napsat	k5eAaPmAgNnS	napsat
<g/>
,	,	kIx,	,
že	že	k8xS	že
členky	členka	k1gFnPc1	členka
CLAMPu	CLAMPus	k1gInSc2	CLAMPus
si	se	k3xPyFc3	se
prostě	prostě	k9	prostě
chtěli	chtít	k5eAaImAgMnP	chtít
vyzkoušet	vyzkoušet	k5eAaPmF	vyzkoušet
nová	nový	k2eAgNnPc4d1	nové
jména	jméno	k1gNnPc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
Pozdější	pozdní	k2eAgNnSc1d2	pozdější
interview	interview	k1gNnSc1	interview
s	s	k7c7	s
Ohkawou	Ohkawa	k1gFnSc7	Ohkawa
prozradilo	prozradit	k5eAaPmAgNnS	prozradit
<g/>
,	,	kIx,	,
že	že	k8xS	že
iniciátorka	iniciátorka	k1gFnSc1	iniciátorka
Mokona	Mokona	k1gFnSc1	Mokona
chtěla	chtít	k5eAaImAgFnS	chtít
vypustit	vypustit	k5eAaPmF	vypustit
své	svůj	k3xOyFgNnSc4	svůj
příjmení	příjmení	k1gNnSc4	příjmení
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
znělo	znět	k5eAaImAgNnS	znět
příliš	příliš	k6eAd1	příliš
nedospěle	dospěle	k6eNd1	dospěle
<g/>
.	.	kIx.	.
</s>
<s>
Nekoi	Nekoi	k6eAd1	Nekoi
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
změnu	změna	k1gFnSc4	změna
nelíbilo	líbit	k5eNaImAgNnS	líbit
<g/>
,	,	kIx,	,
když	když	k8xS	když
jí	on	k3xPp3gFnSc3	on
lidé	člověk	k1gMnPc1	člověk
říkali	říkat	k5eAaImAgMnP	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
má	mít	k5eAaImIp3nS	mít
stejné	stejný	k2eAgNnSc4d1	stejné
jméno	jméno	k1gNnSc4	jméno
jako	jako	k8xS	jako
Mick	Mick	k1gMnSc1	Mick
Jagger	Jagger	k1gMnSc1	Jagger
<g/>
,	,	kIx,	,
Ohkawa	Ohkawa	k1gMnSc1	Ohkawa
a	a	k8xC	a
Igarashi	Igarash	k1gMnPc1	Igarash
se	se	k3xPyFc4	se
už	už	k6eAd1	už
jen	jen	k6eAd1	jen
připojily	připojit	k5eAaPmAgInP	připojit
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgInSc1d1	následující
mangy	mango	k1gNnPc7	mango
buď	buď	k8xC	buď
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
vydávané	vydávaný	k2eAgFnPc1d1	vydávaná
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
z	z	k7c2	z
nějakého	nějaký	k3yIgInSc2	nějaký
důvodu	důvod	k1gInSc2	důvod
pozastavilo	pozastavit	k5eAaPmAgNnS	pozastavit
jejich	jejich	k3xOp3gMnPc1	jejich
vydávaní	vydávaný	k2eAgMnPc1d1	vydávaný
<g/>
.	.	kIx.	.
1992	[number]	k4	1992
-	-	kIx~	-
X	X	kA	X
manga	mango	k1gNnPc4	mango
1997	[number]	k4	1997
-	-	kIx~	-
Clover	Clover	k1gInSc1	Clover
2000	[number]	k4	2000
-	-	kIx~	-
Legal	Legal	k1gInSc1	Legal
Drug	Drug	k1gInSc1	Drug
2010	[number]	k4	2010
-	-	kIx~	-
Gate	Gat	k1gInSc2	Gat
7	[number]	k4	7
1989	[number]	k4	1989
–	–	k?	–
1996	[number]	k4	1996
-	-	kIx~	-
RG	RG	kA	RG
Veda	vést	k5eAaImSgInS	vést
1990	[number]	k4	1990
–	–	k?	–
1991	[number]	k4	1991
-	-	kIx~	-
20	[number]	k4	20
Mensou	mensa	k1gFnSc7	mensa
ni	on	k3xPp3gFnSc4	on
Onegai	Onegae	k1gFnSc4	Onegae
1990	[number]	k4	1990
–	–	k?	–
1993	[number]	k4	1993
-	-	kIx~	-
Tokyo	Tokyo	k6eAd1	Tokyo
<g />
.	.	kIx.	.
</s>
<s>
Babylon	Babylon	k1gInSc1	Babylon
1992	[number]	k4	1992
–	–	k?	–
1993	[number]	k4	1993
-	-	kIx~	-
CLAMP	CLAMP	kA	CLAMP
School	School	k1gInSc1	School
Detectives	Detectives	k1gInSc1	Detectives
1992	[number]	k4	1992
–	–	k?	–
1993	[number]	k4	1993
-	-	kIx~	-
Duklyon	Duklyon	k1gMnSc1	Duklyon
<g/>
:	:	kIx,	:
CLAMP	CLAMP	kA	CLAMP
School	School	k1gInSc1	School
Defenders	Defenders	k1gInSc1	Defenders
1992	[number]	k4	1992
-	-	kIx~	-
Shirahime-Syo	Shirahime-Syo	k1gNnSc1	Shirahime-Syo
<g/>
:	:	kIx,	:
Snow	Snow	k1gFnSc1	Snow
Goddess	Goddess	k1gInSc4	Goddess
Tales	Tales	k1gInSc1	Tales
1993	[number]	k4	1993
–	–	k?	–
1995	[number]	k4	1995
-	-	kIx~	-
Magic	Magic	k1gMnSc1	Magic
Knight	Knight	k1gMnSc1	Knight
Rayearth	Rayearth	k1gMnSc1	Rayearth
1993	[number]	k4	1993
–	–	k?	–
1995	[number]	k4	1995
-	-	kIx~	-
Miyuki-chan	Miyukihan	k1gInSc1	Miyuki-chan
in	in	k?	in
Wonderland	Wonderland	k1gInSc1	Wonderland
1995	[number]	k4	1995
-	-	kIx~	-
Watashi	Watashi	k1gNnPc2	Watashi
no	no	k9	no
Suki-na	Suki	k1gInSc2	Suki-n
Hito	Hito	k6eAd1	Hito
1995	[number]	k4	1995
–	–	k?	–
1996	[number]	k4	1996
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
Magic	Magic	k1gMnSc1	Magic
Knight	Knight	k1gMnSc1	Knight
Rayearth	Rayearth	k1gMnSc1	Rayearth
2	[number]	k4	2
1996	[number]	k4	1996
-	-	kIx~	-
Shin	Shin	k1gInSc1	Shin
Shunkaden	Shunkaden	k2eAgInSc1d1	Shunkaden
1996	[number]	k4	1996
–	–	k?	–
1998	[number]	k4	1998
-	-	kIx~	-
Wish	Wish	k1gInSc1	Wish
1996	[number]	k4	1996
–	–	k?	–
2000	[number]	k4	2000
-	-	kIx~	-
Cardcaptor	Cardcaptor	k1gInSc1	Cardcaptor
Sakura	sakura	k1gFnSc1	sakura
1999	[number]	k4	1999
–	–	k?	–
2000	[number]	k4	2000
-	-	kIx~	-
Suki	Suki	k1gNnSc2	Suki
<g/>
:	:	kIx,	:
Dakara	Dakara	k1gFnSc1	Dakara
Suki	Suki	k1gNnSc1	Suki
1999	[number]	k4	1999
–	–	k?	–
2001	[number]	k4	2001
-	-	kIx~	-
Angelic	Angelice	k1gFnPc2	Angelice
Layer	Layero	k1gNnPc2	Layero
2001	[number]	k4	2001
–	–	k?	–
2002	[number]	k4	2002
-	-	kIx~	-
Chobits	Chobits	k1gInSc1	Chobits
2003	[number]	k4	2003
–	–	k?	–
2009	[number]	k4	2009
-	-	kIx~	-
Tsubasa	Tsubasa	k1gFnSc1	Tsubasa
<g/>
:	:	kIx,	:
RESERVoir	RESERVoir	k1gInSc1	RESERVoir
CHRoNiCLE	CHRoNiCLE	k1gFnSc2	CHRoNiCLE
2003	[number]	k4	2003
–	–	k?	–
2011	[number]	k4	2011
-	-	kIx~	-
×××	×××	k?	×××
<g/>
HOLiC	Holice	k1gFnPc2	Holice
2005	[number]	k4	2005
–	–	k?	–
2011	[number]	k4	2011
-	-	kIx~	-
Kobato	Kobat	k2eAgNnSc1d1	Kobato
<g/>
.	.	kIx.	.
</s>
<s>
CLAMP	CLAMP	kA	CLAMP
vydalo	vydat	k5eAaPmAgNnS	vydat
též	též	k6eAd1	též
ke	k	k7c3	k
svému	své	k1gNnSc3	své
15	[number]	k4	15
<g/>
.	.	kIx.	.
<g/>
výročí	výročí	k1gNnSc2	výročí
dvanácti	dvanáct	k4xCc2	dvanáct
sborníků	sborník	k1gInPc2	sborník
CLAMP	CLAMP	kA	CLAMP
no	no	k9	no
Kiseki	Kisek	k1gMnPc1	Kisek
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
obsahovaly	obsahovat	k5eAaImAgInP	obsahovat
rozhovory	rozhovor	k1gInPc1	rozhovor
s	s	k7c7	s
autory	autor	k1gMnPc7	autor
<g/>
,	,	kIx,	,
nové	nový	k2eAgNnSc1d1	nové
manga	mango	k1gNnPc1	mango
povídky	povídka	k1gFnSc2	povídka
atd.	atd.	kA	atd.
1989	[number]	k4	1989
-	-	kIx~	-
Tenshi	Tenshi	k1gNnPc7	Tenshi
no	no	k9	no
Bodyguard	bodyguard	k1gMnSc1	bodyguard
1990	[number]	k4	1990
-	-	kIx~	-
Shiawase	Shiawasa	k1gFnSc6	Shiawasa
ni	on	k3xPp3gFnSc4	on
Naritai	Naritae	k1gFnSc4	Naritae
1990	[number]	k4	1990
-	-	kIx~	-
Tenku	Tenk	k1gInSc2	Tenk
Senki	Senki	k1gNnSc1	Senki
Shurato	Shurat	k2eAgNnSc1d1	Shurat
Original	Original	k1gFnSc7	Original
Memory	Memora	k1gFnSc2	Memora
<g/>
:	:	kIx,	:
Dreamer	Dreamer	k1gInSc1	Dreamer
1990	[number]	k4	1990
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
Koi	Koi	k?	Koi
wa	wa	k?	wa
Tenka	Tenek	k1gInSc2	Tenek
no	no	k9	no
Mawarimono	Mawarimona	k1gFnSc5	Mawarimona
1994	[number]	k4	1994
-	-	kIx~	-
Hidari	Hidari	k1gNnSc2	Hidari
Te	Te	k1gFnSc2	Te
1994	[number]	k4	1994
-	-	kIx~	-
Sohryuden	Sohryudna	k1gFnPc2	Sohryudna
<g/>
:	:	kIx,	:
Legend	legenda	k1gFnPc2	legenda
of	of	k?	of
the	the	k?	the
Dragon	Dragon	k1gMnSc1	Dragon
Kings	Kings	k1gInSc4	Kings
1996	[number]	k4	1996
-	-	kIx~	-
Yumegari	Yumegar	k1gInSc3	Yumegar
2002	[number]	k4	2002
-	-	kIx~	-
Ano	ano	k9	ano
hi	hi	k0	hi
wo	wo	k?	wo
shiru	shira	k1gFnSc4	shira
mono	mono	k2eAgNnPc2d1	mono
wa	wa	k?	wa
saiwai	saiwai	k1gNnPc2	saiwai
de	de	k?	de
Aru	ar	k1gInSc6	ar
2002	[number]	k4	2002
-	-	kIx~	-
Murikuri	Murikur	k1gInSc3	Murikur
1993	[number]	k4	1993
-	-	kIx~	-
Koi	Koi	k1gFnSc1	Koi
1993	[number]	k4	1993
-	-	kIx~	-
Rex	Rex	k1gMnPc2	Rex
Kyouryuu	Kyouryu	k2eAgFnSc4d1	Kyouryu
Monogatari	Monogatare	k1gFnSc4	Monogatare
2004	[number]	k4	2004
-	-	kIx~	-
Sweet	Sweet	k1gInSc1	Sweet
Valerian	Valerian	k1gInSc1	Valerian
2006	[number]	k4	2006
-	-	kIx~	-
<g />
.	.	kIx.	.
</s>
<s>
Code	Code	k1gInSc1	Code
Geass	Geass	k1gInSc1	Geass
<g/>
:	:	kIx,	:
Lelouch	Lelouch	k1gInSc1	Lelouch
of	of	k?	of
the	the	k?	the
Rebellion	Rebellion	k?	Rebellion
2006	[number]	k4	2006
-	-	kIx~	-
Night	Night	k2eAgInSc1d1	Night
Head	Head	k1gInSc1	Head
Genesis	Genesis	k1gFnSc1	Genesis
2008	[number]	k4	2008
-	-	kIx~	-
Mouryou	Mouryá	k1gFnSc4	Mouryá
no	no	k9	no
Hako	Hako	k6eAd1	Hako
???	???	k?	???
-	-	kIx~	-
Sohryuden	Sohryudna	k1gFnPc2	Sohryudna
<g/>
:	:	kIx,	:
Legend	legenda	k1gFnPc2	legenda
of	of	k?	of
the	the	k?	the
Dragon	Dragon	k1gMnSc1	Dragon
Kings	Kings	k1gInSc1	Kings
(	(	kIx(	(
<g/>
kniha	kniha	k1gFnSc1	kniha
<g/>
)	)	kIx)	)
???	???	k?	???
-	-	kIx~	-
Oshiroi	Oshiroe	k1gFnSc4	Oshiroe
Chouchou	Chouchý	k2eAgFnSc4d1	Chouchý
???	???	k?	???
-	-	kIx~	-
CLAMP	CLAMP	kA	CLAMP
School	School	k1gInSc1	School
Paranormal	Paranormal	k1gInSc1	Paranormal
Investigators	Investigators	k1gInSc4	Investigators
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
CLAMP	CLAMP	kA	CLAMP
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc1	Commons
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
CLAMPu	CLAMPus	k1gInSc2	CLAMPus
Anime	Anim	k1gInSc5	Anim
News	News	k1gInSc4	News
Network	network	k1gInSc1	network
-	-	kIx~	-
informace	informace	k1gFnPc1	informace
o	o	k7c6	o
CLAMPu	CLAMPus	k1gInSc6	CLAMPus
v	v	k7c6	v
angličtině	angličtina	k1gFnSc6	angličtina
</s>
