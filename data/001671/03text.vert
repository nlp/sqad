<s>
Hannes	Hannes	k1gMnSc1	Hannes
Olof	Olof	k1gMnSc1	Olof
Gösta	Gösta	k1gMnSc1	Gösta
Alfvén	Alfvén	k1gInSc1	Alfvén
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1908	[number]	k4	1908
<g/>
,	,	kIx,	,
Norrköping	Norrköping	k1gInSc1	Norrköping
<g/>
,	,	kIx,	,
Švédsko	Švédsko	k1gNnSc1	Švédsko
-	-	kIx~	-
2	[number]	k4	2
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
Djursholm	Djursholm	k1gInSc1	Djursholm
<g/>
,	,	kIx,	,
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
fyzik	fyzik	k1gMnSc1	fyzik
a	a	k8xC	a
astrofyzik	astrofyzik	k1gMnSc1	astrofyzik
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
zejména	zejména	k9	zejména
kosmickou	kosmický	k2eAgFnSc7d1	kosmická
elektrodynamikou	elektrodynamika	k1gFnSc7	elektrodynamika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
publikoval	publikovat	k5eAaBmAgMnS	publikovat
Alfvén	Alfvén	k1gInSc4	Alfvén
teorii	teorie	k1gFnSc3	teorie
magnetických	magnetický	k2eAgFnPc2d1	magnetická
bouří	bouř	k1gFnPc2	bouř
a	a	k8xC	a
polárních	polární	k2eAgFnPc2d1	polární
září	zář	k1gFnPc2	zář
<g/>
.	.	kIx.	.
</s>
<s>
Významnou	významný	k2eAgFnSc7d1	významná
mírou	míra	k1gFnSc7	míra
přispěl	přispět	k5eAaPmAgInS	přispět
k	k	k7c3	k
moderní	moderní	k2eAgFnSc3d1	moderní
planetární	planetární	k2eAgFnSc3d1	planetární
kosmologii	kosmologie	k1gFnSc3	kosmologie
hlavně	hlavně	k9	hlavně
pracemi	práce	k1gFnPc7	práce
o	o	k7c6	o
přenosu	přenos	k1gInSc6	přenos
momentu	moment	k1gInSc2	moment
hybnosti	hybnost	k1gFnSc2	hybnost
z	z	k7c2	z
rotujícího	rotující	k2eAgInSc2d1	rotující
praslunce	praslunka	k1gFnSc3	praslunka
na	na	k7c6	na
planety	planeta	k1gFnSc2	planeta
<g/>
,	,	kIx,	,
účinku	účinek	k1gInSc2	účinek
magnetických	magnetický	k2eAgFnPc2d1	magnetická
polí	pole	k1gFnPc2	pole
na	na	k7c4	na
pramlhovinu	pramlhovina	k1gFnSc4	pramlhovina
a	a	k8xC	a
pracemi	práce	k1gFnPc7	práce
o	o	k7c6	o
magnetosféře	magnetosféra	k1gFnSc6	magnetosféra
Země	zem	k1gFnSc2	zem
a	a	k8xC	a
jiných	jiný	k2eAgNnPc2d1	jiné
těles	těleso	k1gNnPc2	těleso
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
dostal	dostat	k5eAaPmAgMnS	dostat
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
práce	práce	k1gFnPc4	práce
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
fyziky	fyzika	k1gFnSc2	fyzika
plazmatu	plazma	k1gNnSc2	plazma
a	a	k8xC	a
její	její	k3xOp3gFnSc2	její
interakce	interakce	k1gFnSc2	interakce
s	s	k7c7	s
magnetickým	magnetický	k2eAgNnSc7d1	magnetické
polem	pole	k1gNnSc7	pole
<g/>
.	.	kIx.	.
</s>
<s>
Zavadí	zavadit	k5eAaPmIp3nS	zavadit
také	také	k9	také
alternativní	alternativní	k2eAgInSc4d1	alternativní
model	model	k1gInSc4	model
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
plazmová	plazmový	k2eAgFnSc1d1	plazmová
kosmologie	kosmologie	k1gFnSc1	kosmologie
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Hannes	Hannes	k1gInSc1	Hannes
Alfvén	Alfvén	k1gInSc1	Alfvén
Lubomír	Lubomír	k1gMnSc1	Lubomír
Sodomka	Sodomka	k1gFnSc1	Sodomka
<g/>
,	,	kIx,	,
Magdalena	Magdalena	k1gFnSc1	Magdalena
Sodomková	Sodomková	k1gFnSc1	Sodomková
<g/>
,	,	kIx,	,
Nobelovy	Nobelův	k2eAgFnSc2d1	Nobelova
ceny	cena	k1gFnSc2	cena
za	za	k7c4	za
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
SET	set	k1gInSc1	set
OUT	OUT	kA	OUT
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-902058-5-2	[number]	k4	80-902058-5-2
</s>
