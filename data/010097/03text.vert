<p>
<s>
Carlos	Carlos	k1gMnSc1	Carlos
Alberto	Alberta	k1gFnSc5	Alberta
de	de	k?	de
Sousa	Sous	k1gMnSc4	Sous
Lopes	Lopes	k1gMnSc1	Lopes
(	(	kIx(	(
<g/>
*	*	kIx~	*
18	[number]	k4	18
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1947	[number]	k4	1947
Vildemoinhos	Vildemoinhos	k1gInSc1	Vildemoinhos
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalý	bývalý	k2eAgMnSc1d1	bývalý
portugalský	portugalský	k2eAgMnSc1d1	portugalský
atlet	atlet	k1gMnSc1	atlet
<g/>
,	,	kIx,	,
běžec	běžec	k1gMnSc1	běžec
na	na	k7c6	na
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
tratě	trata	k1gFnSc6	trata
<g/>
,	,	kIx,	,
olympijský	olympijský	k2eAgMnSc1d1	olympijský
vítěz	vítěz	k1gMnSc1	vítěz
v	v	k7c6	v
maratonu	maraton	k1gInSc6	maraton
na	na	k7c4	na
OH	OH	kA	OH
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angeles	k1gInSc4	Angeles
1984	[number]	k4	1984
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sportovní	sportovní	k2eAgFnSc1d1	sportovní
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Poprvé	poprvé	k6eAd1	poprvé
startoval	startovat	k5eAaBmAgInS	startovat
na	na	k7c6	na
mezinárodních	mezinárodní	k2eAgFnPc6d1	mezinárodní
soutěžích	soutěž	k1gFnPc6	soutěž
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1971	[number]	k4	1971
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
Evropy	Evropa	k1gFnSc2	Evropa
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
(	(	kIx(	(
<g/>
bez	bez	k7c2	bez
velkého	velký	k2eAgInSc2d1	velký
úspěchu	úspěch	k1gInSc2	úspěch
<g/>
)	)	kIx)	)
–	–	k?	–
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
10	[number]	k4	10
000	[number]	k4	000
metrů	metr	k1gInPc2	metr
doběhl	doběhnout	k5eAaPmAgMnS	doběhnout
poslední	poslední	k2eAgMnSc1d1	poslední
(	(	kIx(	(
<g/>
33	[number]	k4	33
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
3	[number]	k4	3
000	[number]	k4	000
metrů	metr	k1gInPc2	metr
překážek	překážka	k1gFnPc2	překážka
se	se	k3xPyFc4	se
neprobojoval	probojovat	k5eNaPmAgInS	probojovat
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
olympiádě	olympiáda	k1gFnSc6	olympiáda
v	v	k7c6	v
Mnichově	Mnichov	k1gInSc6	Mnichov
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
startoval	startovat	k5eAaBmAgMnS	startovat
v	v	k7c6	v
bězích	běh	k1gInPc6	běh
na	na	k7c4	na
5	[number]	k4	5
a	a	k8xC	a
10	[number]	k4	10
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ani	ani	k8xC	ani
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
disciplíně	disciplína	k1gFnSc6	disciplína
nepostoupil	postoupit	k5eNaPmAgMnS	postoupit
do	do	k7c2	do
finále	finále	k1gNnSc2	finále
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgInSc1	první
výrazný	výrazný	k2eAgInSc1d1	výrazný
úspěch	úspěch	k1gInSc1	úspěch
zaznamenal	zaznamenat	k5eAaPmAgInS	zaznamenat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1976	[number]	k4	1976
<g/>
.	.	kIx.	.
</s>
<s>
Nejprve	nejprve	k6eAd1	nejprve
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
v	v	k7c6	v
přespolním	přespolní	k2eAgInSc6d1	přespolní
běhu	běh	k1gInSc6	běh
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Montrealu	Montreal	k1gInSc6	Montreal
v	v	k7c6	v
červenci	červenec	k1gInSc6	červenec
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
startoval	startovat	k5eAaBmAgInS	startovat
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
na	na	k7c4	na
10	[number]	k4	10
000	[number]	k4	000
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Udával	udávat	k5eAaImAgMnS	udávat
tempo	tempo	k1gNnSc4	tempo
peletonu	peleton	k1gInSc2	peleton
běžců	běžec	k1gMnPc2	běžec
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
rychlosti	rychlost	k1gFnSc2	rychlost
stačil	stačit	k5eAaBmAgMnS	stačit
pouze	pouze	k6eAd1	pouze
pozdější	pozdní	k2eAgMnSc1d2	pozdější
vítěz	vítěz	k1gMnSc1	vítěz
Lasse	Lasse	k1gFnSc2	Lasse
Virén	Virén	k1gInSc1	Virén
<g/>
.	.	kIx.	.
</s>
<s>
Lopes	Lopes	k1gMnSc1	Lopes
si	se	k3xPyFc3	se
doběhl	doběhnout	k5eAaPmAgMnS	doběhnout
pro	pro	k7c4	pro
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
světovém	světový	k2eAgInSc6d1	světový
šampionátu	šampionát	k1gInSc6	šampionát
v	v	k7c6	v
přespolním	přespolní	k2eAgInSc6d1	přespolní
běhu	běh	k1gInSc6	běh
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1977	[number]	k4	1977
skončil	skončit	k5eAaPmAgMnS	skončit
druhý	druhý	k4xOgMnSc1	druhý
za	za	k7c7	za
Léonem	Léon	k1gMnSc7	Léon
Schotsem	Schots	k1gMnSc7	Schots
z	z	k7c2	z
Belgie	Belgie	k1gFnSc2	Belgie
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
několik	několik	k4yIc4	několik
roků	rok	k1gInPc2	rok
jeho	jeho	k3xOp3gNnSc3	jeho
poslední	poslední	k2eAgInSc4d1	poslední
výrazný	výrazný	k2eAgInSc4d1	výrazný
úspěch	úspěch	k1gInSc4	úspěch
<g/>
.	.	kIx.	.
</s>
<s>
Návrat	návrat	k1gInSc1	návrat
k	k	k7c3	k
původní	původní	k2eAgFnSc3d1	původní
formě	forma	k1gFnSc3	forma
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařil	podařit	k5eAaPmAgInS	podařit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1982	[number]	k4	1982
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Oslo	Oslo	k1gNnSc6	Oslo
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
evropský	evropský	k2eAgInSc4d1	evropský
rekord	rekord	k1gInSc4	rekord
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
10	[number]	k4	10
000	[number]	k4	000
metrů	metr	k1gInPc2	metr
časem	časem	k6eAd1	časem
27	[number]	k4	27
<g/>
:	:	kIx,	:
<g/>
24,39	[number]	k4	24,39
<g/>
,	,	kIx,	,
na	na	k7c6	na
evropském	evropský	k2eAgInSc6d1	evropský
šampionátu	šampionát	k1gInSc6	šampionát
v	v	k7c6	v
Aténách	Atény	k1gFnPc6	Atény
skončil	skončit	k5eAaPmAgMnS	skončit
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
disciplíně	disciplína	k1gFnSc6	disciplína
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následující	následující	k2eAgFnSc6d1	následující
sezóně	sezóna	k1gFnSc6	sezóna
získal	získat	k5eAaPmAgMnS	získat
stříbrnou	stříbrný	k2eAgFnSc4d1	stříbrná
medaili	medaile	k1gFnSc4	medaile
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
přespolním	přespolní	k2eAgInSc6d1	přespolní
běhu	běh	k1gInSc6	běh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
premiéře	premiéra	k1gFnSc6	premiéra
mistrovství	mistrovství	k1gNnSc2	mistrovství
světa	svět	k1gInSc2	svět
na	na	k7c6	na
dráze	dráha	k1gFnSc6	dráha
v	v	k7c6	v
Helsinkách	Helsinky	k1gFnPc6	Helsinky
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
doběhl	doběhnout	k5eAaPmAgMnS	doběhnout
ve	v	k7c6	v
finále	finále	k1gNnSc6	finále
závodu	závod	k1gInSc2	závod
na	na	k7c4	na
10	[number]	k4	10
000	[number]	k4	000
metrů	metr	k1gInPc2	metr
na	na	k7c6	na
šestém	šestý	k4xOgInSc6	šestý
místě	místo	k1gNnSc6	místo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c4	na
start	start	k1gInSc1	start
prvního	první	k4xOgInSc2	první
maratonu	maraton	k1gInSc2	maraton
v	v	k7c6	v
kariéře	kariéra	k1gFnSc6	kariéra
se	se	k3xPyFc4	se
postavil	postavit	k5eAaPmAgMnS	postavit
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
roku	rok	k1gInSc2	rok
1982	[number]	k4	1982
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
Závod	závod	k1gInSc1	závod
po	po	k7c6	po
kolizi	kolize	k1gFnSc6	kolize
s	s	k7c7	s
divákem	divák	k1gMnSc7	divák
nedokončil	dokončit	k5eNaPmAgMnS	dokončit
a	a	k8xC	a
odstoupil	odstoupit	k5eAaPmAgMnS	odstoupit
na	na	k7c4	na
30	[number]	k4	30
<g/>
.	.	kIx.	.
kilometru	kilometr	k1gInSc2	kilometr
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
1983	[number]	k4	1983
při	při	k7c6	při
maratonu	maraton	k1gInSc6	maraton
v	v	k7c6	v
Rotterdamu	Rotterdam	k1gInSc6	Rotterdam
doběhl	doběhnout	k5eAaPmAgMnS	doběhnout
druhý	druhý	k4xOgMnSc1	druhý
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
časem	čas	k1gInSc7	čas
2.08	[number]	k4	2.08
<g/>
:	:	kIx,	:
<g/>
39	[number]	k4	39
nový	nový	k2eAgInSc4d1	nový
evropský	evropský	k2eAgInSc4d1	evropský
rekord	rekord	k1gInSc4	rekord
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
olympijské	olympijský	k2eAgFnSc6d1	olympijská
sezoně	sezona	k1gFnSc6	sezona
1984	[number]	k4	1984
zvítězil	zvítězit	k5eAaPmAgMnS	zvítězit
na	na	k7c6	na
mistrovství	mistrovství	k1gNnSc6	mistrovství
světa	svět	k1gInSc2	svět
v	v	k7c6	v
přespolním	přespolní	k2eAgInSc6d1	přespolní
běhu	běh	k1gInSc6	běh
a	a	k8xC	a
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
družstev	družstvo	k1gNnPc2	družstvo
získal	získat	k5eAaPmAgMnS	získat
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1984	[number]	k4	1984
pomohl	pomoct	k5eAaPmAgInS	pomoct
svému	svůj	k3xOyFgMnSc3	svůj
krajanovi	krajan	k1gMnSc3	krajan
Fernando	Fernanda	k1gFnSc5	Fernanda
Mamedemu	Mamedem	k1gMnSc3	Mamedem
vytvořit	vytvořit	k5eAaPmF	vytvořit
nový	nový	k2eAgInSc4d1	nový
světový	světový	k2eAgInSc4d1	světový
rekord	rekord	k1gInSc4	rekord
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
na	na	k7c4	na
10	[number]	k4	10
000	[number]	k4	000
metrů	metr	k1gInPc2	metr
časem	časem	k6eAd1	časem
27	[number]	k4	27
<g/>
:	:	kIx,	:
<g/>
13,81	[number]	k4	13,81
(	(	kIx(	(
<g/>
Lopes	Lopes	k1gMnSc1	Lopes
doběhl	doběhnout	k5eAaPmAgMnS	doběhnout
v	v	k7c6	v
závodě	závod	k1gInSc6	závod
druhý	druhý	k4xOgMnSc1	druhý
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
největším	veliký	k2eAgInSc7d3	veliký
úspěchem	úspěch	k1gInSc7	úspěch
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
vítězství	vítězství	k1gNnSc1	vítězství
v	v	k7c6	v
maratonu	maraton	k1gInSc6	maraton
na	na	k7c6	na
olympiádě	olympiáda	k1gFnSc6	olympiáda
v	v	k7c4	v
Los	los	k1gInSc4	los
Angeles	Angelesa	k1gFnPc2	Angelesa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1984	[number]	k4	1984
Je	být	k5eAaImIp3nS	být
čas	čas	k1gInSc4	čas
2.09	[number]	k4	2.09
<g/>
:	:	kIx,	:
<g/>
21	[number]	k4	21
znamenal	znamenat	k5eAaImAgInS	znamenat
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
olympijský	olympijský	k2eAgInSc1d1	olympijský
rekord	rekord	k1gInSc1	rekord
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prvním	první	k4xOgMnSc7	první
portugalským	portugalský	k2eAgMnSc7d1	portugalský
olympijským	olympijský	k2eAgMnSc7d1	olympijský
vítězem	vítěz	k1gMnSc7	vítěz
<g/>
.	.	kIx.	.
</s>
<s>
Zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
ve	v	k7c4	v
svých	svůj	k3xOyFgNnPc2	svůj
37	[number]	k4	37
letech	léto	k1gNnPc6	léto
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
nejstarším	starý	k2eAgMnSc7d3	nejstarší
olympijským	olympijský	k2eAgMnSc7d1	olympijský
vítězem	vítěz	k1gMnSc7	vítěz
v	v	k7c6	v
běžeckých	běžecký	k2eAgFnPc6d1	běžecká
disciplínách	disciplína	k1gFnPc6	disciplína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1985	[number]	k4	1985
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
potřetí	potřetí	k4xO	potřetí
mistrem	mistr	k1gMnSc7	mistr
světa	svět	k1gInSc2	svět
v	v	k7c6	v
přespolním	přespolní	k2eAgInSc6d1	přespolní
běhu	běh	k1gInSc6	běh
(	(	kIx(	(
<g/>
tentokrát	tentokrát	k6eAd1	tentokrát
na	na	k7c6	na
domácí	domácí	k2eAgFnSc6d1	domácí
půdě	půda	k1gFnSc6	půda
v	v	k7c6	v
Lisabonu	Lisabon	k1gInSc6	Lisabon
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
v	v	k7c6	v
Rotterdamu	Rotterdam	k1gInSc6	Rotterdam
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
světový	světový	k2eAgInSc4d1	světový
výkon	výkon	k1gInSc4	výkon
v	v	k7c6	v
maratonu	maraton	k1gInSc6	maraton
časem	čas	k1gInSc7	čas
2.07	[number]	k4	2.07
<g/>
:	:	kIx,	:
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
tak	tak	k9	tak
prvním	první	k4xOgMnSc7	první
člověkem	člověk	k1gMnSc7	člověk
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
42	[number]	k4	42
195	[number]	k4	195
metrů	metr	k1gInPc2	metr
uběhl	uběhnout	k5eAaPmAgInS	uběhnout
pod	pod	k7c4	pod
2	[number]	k4	2
hodiny	hodina	k1gFnSc2	hodina
a	a	k8xC	a
8	[number]	k4	8
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
roce	rok	k1gInSc6	rok
ukončil	ukončit	k5eAaPmAgInS	ukončit
svoji	svůj	k3xOyFgFnSc4	svůj
sportovní	sportovní	k2eAgFnSc4d1	sportovní
kariéru	kariéra	k1gFnSc4	kariéra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Carlos	Carlosa	k1gFnPc2	Carlosa
Lopes	Lopesa	k1gFnPc2	Lopesa
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Carlos	Carlos	k1gMnSc1	Carlos
Lopes	Lopes	k1gMnSc1	Lopes
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
IAAF	IAAF	kA	IAAF
</s>
</p>
<p>
<s>
Profil	profil	k1gInSc1	profil
na	na	k7c6	na
sports-reference	sportseferenka	k1gFnSc6	sports-referenka
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
