<s>
Klášter	klášter	k1gInSc1	klášter
Sedlec	Sedlec	k1gInSc1	Sedlec
býval	bývat	k5eAaImAgInS	bývat
nejstarším	starý	k2eAgNnSc7d3	nejstarší
cisterciáckým	cisterciácký	k2eAgNnSc7d1	cisterciácké
opatstvím	opatství	k1gNnSc7	opatství
v	v	k7c6	v
českém	český	k2eAgNnSc6d1	české
království	království	k1gNnSc6	království
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
založen	založen	k2eAgInSc1d1	založen
roku	rok	k1gInSc2	rok
1142	[number]	k4	1142
šlechticem	šlechtic	k1gMnSc7	šlechtic
Miroslavem	Miroslav	k1gMnSc7	Miroslav
zřejmě	zřejmě	k6eAd1	zřejmě
na	na	k7c4	na
radu	rada	k1gFnSc4	rada
olomouckého	olomoucký	k2eAgMnSc2d1	olomoucký
biskupa	biskup	k1gMnSc2	biskup
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Zdíka	Zdík	k1gMnSc2	Zdík
v	v	k7c4	v
dnešní	dnešní	k2eAgInSc4d1	dnešní
Sedlci	Sedlec	k1gInSc3	Sedlec
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vypálení	vypálení	k1gNnSc6	vypálení
husity	husita	k1gMnPc7	husita
se	se	k3xPyFc4	se
dochoval	dochovat	k5eAaPmAgInS	dochovat
konventní	konventní	k2eAgInSc1d1	konventní
Kostel	kostel	k1gInSc1	kostel
Nanebevzetí	nanebevzetí	k1gNnSc2	nanebevzetí
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
a	a	k8xC	a
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
areálu	areál	k1gInSc6	areál
někdejšího	někdejší	k2eAgInSc2d1	někdejší
kláštera	klášter	k1gInSc2	klášter
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
sídlí	sídlet	k5eAaImIp3nS	sídlet
cigaretová	cigaretový	k2eAgFnSc1d1	cigaretová
továrna	továrna	k1gFnSc1	továrna
provozovaná	provozovaný	k2eAgFnSc1d1	provozovaná
společností	společnost	k1gFnSc7	společnost
Philip	Philip	k1gMnSc1	Philip
Morris	Morris	k1gFnSc2	Morris
ČR	ČR	kA	ČR
a.	a.	k?	a.
s.	s.	k?	s.
Prvotní	prvotní	k2eAgInSc1d1	prvotní
konvent	konvent	k1gInSc1	konvent
v	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
nového	nový	k2eAgInSc2d1	nový
řádu	řád	k1gInSc2	řád
přišel	přijít	k5eAaPmAgInS	přijít
z	z	k7c2	z
bavorského	bavorský	k2eAgInSc2d1	bavorský
kláštera	klášter	k1gInSc2	klášter
Valdsasy	Valdsasa	k1gFnSc2	Valdsasa
roku	rok	k1gInSc2	rok
1142	[number]	k4	1142
a	a	k8xC	a
nové	nový	k2eAgNnSc4d1	nové
založení	založení	k1gNnSc4	založení
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
napojeno	napojit	k5eAaPmNgNnS	napojit
na	na	k7c4	na
morimondskou	morimondský	k2eAgFnSc4d1	morimondský
větev	větev	k1gFnSc4	větev
řádu	řád	k1gInSc2	řád
<g/>
.	.	kIx.	.
</s>
<s>
Řeholníci	řeholník	k1gMnPc1	řeholník
získali	získat	k5eAaPmAgMnP	získat
od	od	k7c2	od
fundátora	fundátor	k1gMnSc2	fundátor
zřejmě	zřejmě	k6eAd1	zřejmě
většinu	většina	k1gFnSc4	většina
jeho	on	k3xPp3gInSc2	on
majetku	majetek	k1gInSc2	majetek
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
tehdejším	tehdejší	k2eAgNnSc6d1	tehdejší
území	území	k1gNnSc6	území
okolo	okolo	k7c2	okolo
Malína	Malín	k1gInSc2	Malín
<g/>
.	.	kIx.	.
</s>
<s>
Klášter	klášter	k1gInSc1	klášter
zpočátku	zpočátku	k6eAd1	zpočátku
nenabyl	nabýt	k5eNaPmAgInS	nabýt
příliš	příliš	k6eAd1	příliš
velkého	velký	k2eAgInSc2d1	velký
významu	význam	k1gInSc2	význam
a	a	k8xC	a
až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
posledních	poslední	k2eAgMnPc2d1	poslední
Přemyslovců	Přemyslovec	k1gMnPc2	Přemyslovec
se	se	k3xPyFc4	se
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
v	v	k7c6	v
písemnostech	písemnost	k1gFnPc6	písemnost
příliš	příliš	k6eAd1	příliš
často	často	k6eAd1	často
nesetkáváme	setkávat	k5eNaImIp1nP	setkávat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
hladomoru	hladomor	k1gInSc2	hladomor
v	v	k7c6	v
době	doba	k1gFnSc6	doba
po	po	k7c6	po
bitvě	bitva	k1gFnSc6	bitva
na	na	k7c6	na
Moravském	moravský	k2eAgNnSc6d1	Moravské
poli	pole	k1gNnSc6	pole
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1281	[number]	k4	1281
zhoršil	zhoršit	k5eAaPmAgInS	zhoršit
stav	stav	k1gInSc1	stav
hospodářství	hospodářství	k1gNnSc2	hospodářství
sedleckých	sedlecký	k2eAgMnPc2d1	sedlecký
mnichů	mnich	k1gMnPc2	mnich
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
bylo	být	k5eAaImAgNnS	být
nutné	nutný	k2eAgNnSc1d1	nutné
požádat	požádat	k5eAaPmF	požádat
generální	generální	k2eAgFnSc4d1	generální
kapitulu	kapitula	k1gFnSc4	kapitula
o	o	k7c4	o
svolení	svolení	k1gNnSc4	svolení
k	k	k7c3	k
rozptýlení	rozptýlení	k1gNnSc3	rozptýlení
konventu	konvent	k1gInSc2	konvent
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
znamenalo	znamenat	k5eAaImAgNnS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
mniši	mnich	k1gMnPc1	mnich
dočasně	dočasně	k6eAd1	dočasně
odejdou	odejít	k5eAaPmIp3nP	odejít
na	na	k7c4	na
grangie	grangie	k1gFnPc4	grangie
či	či	k8xC	či
do	do	k7c2	do
jiných	jiný	k2eAgInPc2d1	jiný
řádových	řádový	k2eAgInPc2d1	řádový
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
v	v	k7c4	v
tuto	tento	k3xDgFnSc4	tento
dobu	doba	k1gFnSc4	doba
byl	být	k5eAaImAgMnS	být
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
opata	opat	k1gMnSc2	opat
zvolen	zvolen	k2eAgInSc4d1	zvolen
Heidenreich	Heidenreich	k1gInSc4	Heidenreich
<g/>
,	,	kIx,	,
během	během	k7c2	během
jehož	jenž	k3xRgInSc2	jenž
úřadu	úřad	k1gInSc2	úřad
klášteru	klášter	k1gInSc3	klášter
přibylo	přibýt	k5eAaPmAgNnS	přibýt
na	na	k7c6	na
významnosti	významnost	k1gFnSc6	významnost
a	a	k8xC	a
také	také	k9	také
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
první	první	k4xOgFnSc1	první
fundace	fundace	k1gFnSc1	fundace
na	na	k7c6	na
Zbraslavi	Zbraslav	k1gFnSc6	Zbraslav
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
sedmdesátých	sedmdesátý	k4xOgNnPc2	sedmdesátý
let	léto	k1gNnPc2	léto
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
také	také	k9	také
datuje	datovat	k5eAaImIp3nS	datovat
objev	objev	k1gInSc1	objev
ložisek	ložisko	k1gNnPc2	ložisko
stříbrné	stříbrný	k2eAgFnSc2d1	stříbrná
rudy	ruda	k1gFnSc2	ruda
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c7	za
vznikem	vznik	k1gInSc7	vznik
Kutné	kutný	k2eAgFnSc2d1	Kutná
Hory	hora	k1gFnSc2	hora
<g/>
.	.	kIx.	.
</s>
<s>
Klášter	klášter	k1gInSc1	klášter
měl	mít	k5eAaImAgInS	mít
z	z	k7c2	z
dolů	dol	k1gInPc2	dol
otevřených	otevřený	k2eAgInPc2d1	otevřený
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
území	území	k1gNnSc6	území
nemalé	malý	k2eNgInPc4d1	nemalý
zisky	zisk	k1gInPc4	zisk
a	a	k8xC	a
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
vybíral	vybírat	k5eAaImAgMnS	vybírat
poplatky	poplatek	k1gInPc4	poplatek
z	z	k7c2	z
domů	dům	k1gInPc2	dům
<g/>
,	,	kIx,	,
lázní	lázeň	k1gFnPc2	lázeň
a	a	k8xC	a
mlýnů	mlýn	k1gInPc2	mlýn
v	v	k7c6	v
Kutné	kutný	k2eAgFnSc6d1	Kutná
Hoře	hora	k1gFnSc6	hora
<g/>
.	.	kIx.	.
</s>
<s>
Heidenreich	Heidenreich	k1gMnSc1	Heidenreich
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
rádců	rádce	k1gMnPc2	rádce
mladého	mladý	k2eAgMnSc2d1	mladý
krále	král	k1gMnSc2	král
Václava	Václav	k1gMnSc2	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
právě	právě	k9	právě
ten	ten	k3xDgMnSc1	ten
jej	on	k3xPp3gMnSc4	on
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
nový	nový	k2eAgInSc4d1	nový
konvent	konvent	k1gInSc4	konvent
pro	pro	k7c4	pro
královský	královský	k2eAgInSc4d1	královský
klášter	klášter	k1gInSc4	klášter
na	na	k7c6	na
Zbraslavi	Zbraslav	k1gFnSc6	Zbraslav
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1304	[number]	k4	1304
doplatil	doplatit	k5eAaPmAgInS	doplatit
klášter	klášter	k1gInSc1	klášter
na	na	k7c4	na
blízkost	blízkost	k1gFnSc4	blízkost
Kutné	kutný	k2eAgFnSc2d1	Kutná
Hory	hora	k1gFnSc2	hora
při	při	k7c6	při
obléhání	obléhání	k1gNnSc6	obléhání
vojsky	vojsky	k6eAd1	vojsky
Albrechta	Albrecht	k1gMnSc2	Albrecht
Habsburského	habsburský	k2eAgMnSc2d1	habsburský
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
mniši	mnich	k1gMnPc1	mnich
raději	rád	k6eAd2	rád
opustili	opustit	k5eAaPmAgMnP	opustit
klášterní	klášterní	k2eAgFnPc4d1	klášterní
budovy	budova	k1gFnPc4	budova
a	a	k8xC	a
nechali	nechat	k5eAaPmAgMnP	nechat
je	být	k5eAaImIp3nS	být
vojákům	voják	k1gMnPc3	voják
napospas	napospas	k6eAd1	napospas
<g/>
.	.	kIx.	.
</s>
<s>
Heidenreichova	Heidenreichův	k2eAgFnSc1d1	Heidenreichův
účast	účast	k1gFnSc1	účast
na	na	k7c6	na
politických	politický	k2eAgFnPc6d1	politická
námluvách	námluva	k1gFnPc6	námluva
s	s	k7c7	s
novopečeným	novopečený	k2eAgMnSc7d1	novopečený
římským	římský	k2eAgMnSc7d1	římský
králem	král	k1gMnSc7	král
Jindřichem	Jindřich	k1gMnSc7	Jindřich
Lucemburským	lucemburský	k2eAgMnSc7d1	lucemburský
o	o	k7c4	o
dosazení	dosazení	k1gNnSc4	dosazení
jeho	jeho	k3xOp3gMnSc2	jeho
syna	syn	k1gMnSc2	syn
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
českých	český	k2eAgMnPc2d1	český
králů	král	k1gMnPc2	král
<g/>
,	,	kIx,	,
zapříčinila	zapříčinit	k5eAaPmAgFnS	zapříčinit
útok	útok	k1gInSc4	útok
kutnohorských	kutnohorský	k2eAgMnPc2d1	kutnohorský
měšťanů	měšťan	k1gMnPc2	měšťan
vyprovokovaný	vyprovokovaný	k2eAgInSc4d1	vyprovokovaný
maršálkem	maršálek	k1gMnSc7	maršálek
Jindřicha	Jindřich	k1gMnSc4	Jindřich
Korutanského	korutanský	k2eAgMnSc4d1	korutanský
<g/>
.	.	kIx.	.
</s>
<s>
Osazenstvu	osazenstvo	k1gNnSc3	osazenstvo
kláštera	klášter	k1gInSc2	klášter
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
útok	útok	k1gInSc4	útok
odrazit	odrazit	k5eAaPmF	odrazit
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Jana	Jan	k1gMnSc2	Jan
Lucemburského	lucemburský	k2eAgMnSc2d1	lucemburský
byl	být	k5eAaImAgInS	být
klášter	klášter	k1gInSc1	klášter
zatížen	zatížen	k2eAgInSc1d1	zatížen
půjčkami	půjčka	k1gFnPc7	půjčka
svému	svůj	k3xOyFgInSc3	svůj
králi	král	k1gMnSc6	král
a	a	k8xC	a
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
situace	situace	k1gFnSc1	situace
donutila	donutit	k5eAaPmAgFnS	donutit
mnichy	mnich	k1gMnPc4	mnich
opět	opět	k6eAd1	opět
k	k	k7c3	k
rozptýlení	rozptýlení	k1gNnSc3	rozptýlení
konventu	konvent	k1gInSc2	konvent
<g/>
.	.	kIx.	.
</s>
<s>
Ani	ani	k8xC	ani
finanční	finanční	k2eAgFnSc4d1	finanční
pomoc	pomoc	k1gFnSc4	pomoc
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
a	a	k8xC	a
poté	poté	k6eAd1	poté
i	i	k9	i
jeho	jeho	k3xOp3gMnSc4	jeho
syna	syn	k1gMnSc4	syn
Václava	Václav	k1gMnSc4	Václav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
nedokázala	dokázat	k5eNaPmAgFnS	dokázat
zastavit	zastavit	k5eAaPmF	zastavit
prohlubující	prohlubující	k2eAgFnSc1d1	prohlubující
se	se	k3xPyFc4	se
finanční	finanční	k2eAgInSc1d1	finanční
propad	propad	k1gInSc1	propad
sedleckého	sedlecký	k2eAgInSc2d1	sedlecký
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1357	[number]	k4	1357
založen	založen	k2eAgInSc1d1	založen
filiální	filiální	k2eAgInSc1d1	filiální
klášter	klášter	k1gInSc1	klášter
ve	v	k7c6	v
Skalici	Skalice	k1gFnSc6	Skalice
a	a	k8xC	a
osazen	osadit	k5eAaPmNgInS	osadit
12	[number]	k4	12
mnichy	mnich	k1gInPc7	mnich
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
novým	nový	k2eAgMnSc7d1	nový
opatem	opat	k1gMnSc7	opat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1412	[number]	k4	1412
kutnohorští	kutnohorský	k2eAgMnPc1d1	kutnohorský
vyvraždili	vyvraždit	k5eAaPmAgMnP	vyvraždit
obyvatele	obyvatel	k1gMnSc4	obyvatel
klášterní	klášterní	k2eAgFnSc2d1	klášterní
vsi	ves	k1gFnSc2	ves
Malín	Malína	k1gFnPc2	Malína
a	a	k8xC	a
husitské	husitský	k2eAgInPc4d1	husitský
houfy	houf	k1gInPc4	houf
klášter	klášter	k1gInSc1	klášter
dobyly	dobýt	k5eAaPmAgFnP	dobýt
a	a	k8xC	a
vypálily	vypálit	k5eAaPmAgFnP	vypálit
24	[number]	k4	24
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1421	[number]	k4	1421
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
husitském	husitský	k2eAgInSc6d1	husitský
vpádu	vpád	k1gInSc6	vpád
zůstal	zůstat	k5eAaPmAgInS	zůstat
klášter	klášter	k1gInSc1	klášter
v	v	k7c6	v
troskách	troska	k1gFnPc6	troska
<g/>
,	,	kIx,	,
klášterní	klášterní	k2eAgNnSc1d1	klášterní
zboží	zboží	k1gNnSc1	zboží
bylo	být	k5eAaImAgNnS	být
rozkradeno	rozkrást	k5eAaPmNgNnS	rozkrást
a	a	k8xC	a
přeživší	přeživší	k2eAgMnPc1d1	přeživší
mniši	mnich	k1gMnPc1	mnich
se	se	k3xPyFc4	se
uchýlili	uchýlit	k5eAaPmAgMnP	uchýlit
do	do	k7c2	do
jiných	jiný	k2eAgInPc2d1	jiný
klášterů	klášter	k1gInPc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Prozíraví	prozíravý	k2eAgMnPc1d1	prozíravý
cisterciáci	cisterciák	k1gMnPc1	cisterciák
sice	sice	k8xC	sice
archiv	archiv	k1gInSc1	archiv
a	a	k8xC	a
cennosti	cennost	k1gFnSc3	cennost
včas	včas	k6eAd1	včas
odvezli	odvézt	k5eAaPmAgMnP	odvézt
do	do	k7c2	do
rakouského	rakouský	k2eAgInSc2d1	rakouský
Klosterneuburgu	Klosterneuburg	k1gInSc2	Klosterneuburg
a	a	k8xC	a
do	do	k7c2	do
Jihlavy	Jihlava	k1gFnSc2	Jihlava
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
vše	všechen	k3xTgNnSc1	všechen
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
vrátilo	vrátit	k5eAaPmAgNnS	vrátit
nazpět	nazpět	k6eAd1	nazpět
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
obnově	obnova	k1gFnSc3	obnova
došlo	dojít	k5eAaPmAgNnS	dojít
1454	[number]	k4	1454
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
v	v	k7c6	v
polorozbořeném	polorozbořený	k2eAgInSc6d1	polorozbořený
klášteře	klášter	k1gInSc6	klášter
usídlilo	usídlit	k5eAaPmAgNnS	usídlit
pár	pár	k4xCyI	pár
mnichů	mnich	k1gMnPc2	mnich
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ubohý	ubohý	k2eAgInSc4d1	ubohý
stav	stav	k1gInSc4	stav
klášterního	klášterní	k2eAgInSc2d1	klášterní
majetku	majetek	k1gInSc2	majetek
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
spojení	spojení	k1gNnSc3	spojení
s	s	k7c7	s
klášterem	klášter	k1gInSc7	klášter
ve	v	k7c6	v
Skalici	Skalice	k1gFnSc6	Skalice
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vydrželo	vydržet	k5eAaPmAgNnS	vydržet
až	až	k6eAd1	až
do	do	k7c2	do
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
situace	situace	k1gFnSc1	situace
se	se	k3xPyFc4	se
srovnala	srovnat	k5eAaPmAgFnS	srovnat
až	až	k9	až
koncem	koncem	k7c2	koncem
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
roku	rok	k1gInSc2	rok
1783	[number]	k4	1783
byl	být	k5eAaImAgInS	být
klášter	klášter	k1gInSc4	klášter
císařským	císařský	k2eAgInSc7d1	císařský
dekretem	dekret	k1gInSc7	dekret
zrušen	zrušit	k5eAaPmNgInS	zrušit
<g/>
.	.	kIx.	.
</s>
<s>
Soudí	soudit	k5eAaImIp3nS	soudit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
zpočátku	zpočátku	k6eAd1	zpočátku
žili	žít	k5eAaImAgMnP	žít
cisterciáci	cisterciák	k1gMnPc1	cisterciák
v	v	k7c6	v
jakýchsi	jakýsi	k3yIgFnPc6	jakýsi
provizorních	provizorní	k2eAgFnPc6d1	provizorní
budovách	budova	k1gFnPc6	budova
a	a	k8xC	a
až	až	k9	až
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
začalo	začít	k5eAaPmAgNnS	začít
se	s	k7c7	s
stavbou	stavba	k1gFnSc7	stavba
kláštera	klášter	k1gInSc2	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
polovině	polovina	k1gFnSc6	polovina
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
v	v	k7c6	v
klášterní	klášterní	k2eAgFnSc6d1	klášterní
bráně	brána	k1gFnSc6	brána
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
dvojlodní	dvojlodní	k2eAgInSc1d1	dvojlodní
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Filipa	Filip	k1gMnSc2	Filip
a	a	k8xC	a
Jakuba	Jakub	k1gMnSc2	Jakub
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
sloužil	sloužit	k5eAaImAgInS	sloužit
i	i	k9	i
laickým	laický	k2eAgMnPc3d1	laický
návštěvníkům	návštěvník	k1gMnPc3	návštěvník
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
zbořen	zbořit	k5eAaPmNgInS	zbořit
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
musel	muset	k5eAaImAgMnS	muset
ustoupit	ustoupit	k5eAaPmF	ustoupit
stavbě	stavba	k1gFnSc3	stavba
silnice	silnice	k1gFnSc2	silnice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
13	[number]	k4	13
<g/>
.	.	kIx.	.
a	a	k8xC	a
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
půdorysu	půdorys	k1gInSc6	půdorys
latinského	latinský	k2eAgInSc2d1	latinský
kříže	kříž	k1gInSc2	kříž
postaven	postavit	k5eAaPmNgInS	postavit
opatem	opat	k1gMnSc7	opat
Heidenreichem	Heidenreich	k1gMnSc7	Heidenreich
pětilodní	pětilodní	k2eAgInPc4d1	pětilodní
konventní	konventní	k2eAgInPc4d1	konventní
kostel	kostel	k1gInSc4	kostel
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
o	o	k7c6	o
délce	délka	k1gFnSc6	délka
87	[number]	k4	87
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Architektonicky	architektonicky	k6eAd1	architektonicky
navazoval	navazovat	k5eAaImAgInS	navazovat
na	na	k7c4	na
severofrancouzské	severofrancouzský	k2eAgFnPc4d1	severofrancouzský
katedrály	katedrála	k1gFnPc4	katedrála
<g/>
,	,	kIx,	,
na	na	k7c6	na
pětilodí	pětilodí	k1gNnSc6	pětilodí
navazoval	navazovat	k5eAaImAgInS	navazovat
trojlodní	trojlodní	k2eAgInSc1d1	trojlodní
transept	transept	k1gInSc1	transept
a	a	k8xC	a
polygonální	polygonální	k2eAgInSc1d1	polygonální
ochoz	ochoz	k1gInSc1	ochoz
s	s	k7c7	s
věncem	věnec	k1gInSc7	věnec
kaplí	kaple	k1gFnSc7	kaple
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
klášterním	klášterní	k2eAgInSc6d1	klášterní
areálu	areál	k1gInSc6	areál
byl	být	k5eAaImAgInS	být
opatský	opatský	k2eAgInSc1d1	opatský
dům	dům	k1gInSc1	dům
s	s	k7c7	s
kaplí	kaple	k1gFnSc7	kaple
<g/>
,	,	kIx,	,
kaple	kaple	k1gFnSc1	kaple
sv.	sv.	kA	sv.
Kosmy	Kosma	k1gMnSc2	Kosma
a	a	k8xC	a
Damiána	Damián	k1gMnSc2	Damián
<g/>
,	,	kIx,	,
hostinský	hostinský	k2eAgInSc1d1	hostinský
dům	dům	k1gInSc1	dům
<g/>
,	,	kIx,	,
špitál	špitál	k1gInSc1	špitál
<g/>
,	,	kIx,	,
hospodářský	hospodářský	k2eAgInSc1d1	hospodářský
dvůr	dvůr	k1gInSc1	dvůr
se	s	k7c7	s
sýpkou	sýpka	k1gFnSc7	sýpka
a	a	k8xC	a
hřbitov	hřbitov	k1gInSc1	hřbitov
Všech	všecek	k3xTgFnPc2	všecek
svatých	svatá	k1gFnPc2	svatá
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
kritickému	kritický	k2eAgInSc3d1	kritický
nedostatku	nedostatek	k1gInSc2	nedostatek
financí	finance	k1gFnPc2	finance
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
husitském	husitský	k2eAgNnSc6d1	husitské
řádění	řádění	k1gNnSc6	řádění
obnovena	obnovit	k5eAaPmNgNnP	obnovit
pouze	pouze	k6eAd1	pouze
nejnutnější	nutný	k2eAgNnPc1d3	nejnutnější
stavení	stavení	k1gNnPc1	stavení
a	a	k8xC	a
k	k	k7c3	k
zásadní	zásadní	k2eAgFnSc3d1	zásadní
přestavbě	přestavba	k1gFnSc3	přestavba
došlo	dojít	k5eAaPmAgNnS	dojít
až	až	k9	až
koncem	koncem	k7c2	koncem
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
z	z	k7c2	z
iniciativy	iniciativa	k1gFnSc2	iniciativa
opata	opat	k1gMnSc2	opat
Jindřicha	Jindřich	k1gMnSc2	Jindřich
Snopka	Snopek	k1gMnSc2	Snopek
(	(	kIx(	(
<g/>
1651	[number]	k4	1651
<g/>
-	-	kIx~	-
<g/>
1709	[number]	k4	1709
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
svěřil	svěřit	k5eAaPmAgInS	svěřit
přestavbu	přestavba	k1gFnSc4	přestavba
konventního	konventní	k2eAgInSc2d1	konventní
kostela	kostel	k1gInSc2	kostel
nejprve	nejprve	k6eAd1	nejprve
Pavlu	Pavel	k1gMnSc3	Pavel
Ignáci	Ignác	k1gMnSc3	Ignác
Bayerovi	Bayer	k1gMnSc3	Bayer
a	a	k8xC	a
poté	poté	k6eAd1	poté
mladému	mladý	k2eAgMnSc3d1	mladý
staviteli	stavitel	k1gMnSc3	stavitel
J.	J.	kA	J.
B.	B.	kA	B.
Santinimu	Santinim	k1gInSc2	Santinim
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
dokončil	dokončit	k5eAaPmAgMnS	dokončit
velkolepou	velkolepý	k2eAgFnSc4d1	velkolepá
přestavbu	přestavba	k1gFnSc4	přestavba
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
barokní	barokní	k2eAgFnSc2d1	barokní
gotiky	gotika	k1gFnPc1	gotika
<g/>
.	.	kIx.	.
</s>
<s>
Kostelní	kostelní	k2eAgFnSc1d1	kostelní
klenba	klenba	k1gFnSc1	klenba
byla	být	k5eAaImAgFnS	být
dokončena	dokončen	k2eAgFnSc1d1	dokončena
roku	rok	k1gInSc2	rok
1705	[number]	k4	1705
a	a	k8xC	a
o	o	k7c4	o
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
byl	být	k5eAaImAgInS	být
kostel	kostel	k1gInSc1	kostel
znovu	znovu	k6eAd1	znovu
vysvěcen	vysvětit	k5eAaPmNgInS	vysvětit
<g/>
.	.	kIx.	.
</s>
<s>
Interiér	interiér	k1gInSc1	interiér
byl	být	k5eAaImAgInS	být
zařízen	zařídit	k5eAaPmNgInS	zařídit
dřevěnou	dřevěný	k2eAgFnSc7d1	dřevěná
architekturou	architektura	k1gFnSc7	architektura
oltářů	oltář	k1gInPc2	oltář
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
barokní	barokní	k2eAgFnSc2d1	barokní
gotiky	gotika	k1gFnSc2	gotika
podle	podle	k7c2	podle
návrhů	návrh	k1gInPc2	návrh
J.	J.	kA	J.
B.	B.	kA	B.
Santiniho	Santini	k1gMnSc2	Santini
Aichla	Aichla	k1gMnSc2	Aichla
<g/>
,	,	kIx,	,
tři	tři	k4xCgInPc1	tři
oltářní	oltářní	k2eAgInPc1d1	oltářní
obrazy	obraz	k1gInPc1	obraz
dodal	dodat	k5eAaPmAgMnS	dodat
Petr	Petr	k1gMnSc1	Petr
Brandl	Brandl	k1gMnSc1	Brandl
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
i	i	k9	i
většina	většina	k1gFnSc1	většina
konventních	konventní	k2eAgFnPc2d1	konventní
budov	budova	k1gFnPc2	budova
novostavbami	novostavba	k1gFnPc7	novostavba
v	v	k7c6	v
barokním	barokní	k2eAgInSc6d1	barokní
slohu	sloh	k1gInSc6	sloh
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
proměnilo	proměnit	k5eAaPmAgNnS	proměnit
středověký	středověký	k2eAgInSc4d1	středověký
ráz	ráz	k1gInSc4	ráz
klášterního	klášterní	k2eAgInSc2d1	klášterní
areálu	areál	k1gInSc2	areál
<g/>
.	.	kIx.	.
</s>
<s>
Pozoruhodný	pozoruhodný	k2eAgMnSc1d1	pozoruhodný
je	být	k5eAaImIp3nS	být
rozlehlý	rozlehlý	k2eAgInSc1d1	rozlehlý
refektář	refektář	k1gInSc1	refektář
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc4	jehož
ochozy	ochoz	k1gInPc4	ochoz
vyzdobil	vyzdobit	k5eAaPmAgInS	vyzdobit
freskami	freska	k1gFnPc7	freska
malíř	malíř	k1gMnSc1	malíř
Juda	judo	k1gNnSc2	judo
Tadeáš	Tadeáš	k1gMnSc1	Tadeáš
Supper	Supper	k1gMnSc1	Supper
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zrušení	zrušení	k1gNnSc6	zrušení
kláštera	klášter	k1gInSc2	klášter
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1783	[number]	k4	1783
se	se	k3xPyFc4	se
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
otevřela	otevřít	k5eAaPmAgFnS	otevřít
roku	rok	k1gInSc2	rok
1812	[number]	k4	1812
továrna	továrna	k1gFnSc1	továrna
na	na	k7c4	na
tabák	tabák	k1gInSc4	tabák
(	(	kIx(	(
<g/>
přenesena	přenést	k5eAaPmNgFnS	přenést
z	z	k7c2	z
Golčova	Golčův	k2eAgNnSc2d1	Golčův
Jeníkova	Jeníkův	k2eAgNnSc2d1	Jeníkovo
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
firma	firma	k1gFnSc1	firma
Philip	Philip	k1gInSc4	Philip
Morris	Morris	k1gFnSc2	Morris
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
gotické	gotický	k2eAgFnSc6d1	gotická
kapli	kaple	k1gFnSc6	kaple
Všech	všecek	k3xTgMnPc2	všecek
svatých	svatý	k1gMnPc2	svatý
byla	být	k5eAaImAgFnS	být
od	od	k7c2	od
morových	morový	k2eAgFnPc2d1	morová
epidemií	epidemie	k1gFnPc2	epidemie
zavedena	zaveden	k2eAgFnSc1d1	zavedena
kostnice	kostnice	k1gFnSc1	kostnice
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
ji	on	k3xPp3gFnSc4	on
na	na	k7c6	na
západní	západní	k2eAgFnSc6d1	západní
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
straně	strana	k1gFnSc6	strana
obklopoval	obklopovat	k5eAaImAgMnS	obklopovat
velký	velký	k2eAgInSc4d1	velký
hřbitov	hřbitov	k1gInSc4	hřbitov
<g/>
,	,	kIx,	,
zrušený	zrušený	k2eAgInSc4d1	zrušený
během	během	k7c2	během
19	[number]	k4	19
<g/>
.	.	kIx.	.
<g/>
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1753	[number]	k4	1753
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
opatem	opat	k1gMnSc7	opat
Sedleckého	sedlecký	k2eAgInSc2d1	sedlecký
kláštera	klášter	k1gInSc2	klášter
Jakub	Jakub	k1gMnSc1	Jakub
Růžička	Růžička	k1gMnSc1	Růžička
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
začal	začít	k5eAaPmAgMnS	začít
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
s	s	k7c7	s
budováním	budování	k1gNnSc7	budování
nové	nový	k2eAgFnSc2d1	nová
konventní	konventní	k2eAgFnSc2d1	konventní
budovy	budova	k1gFnSc2	budova
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
starý	starý	k2eAgInSc1d1	starý
konvent	konvent	k1gInSc1	konvent
při	při	k7c6	při
kostele	kostel	k1gInSc6	kostel
sv.	sv.	kA	sv.
Filipa	Filip	k1gMnSc2	Filip
a	a	k8xC	a
Jakuba	Jakub	k1gMnSc2	Jakub
byl	být	k5eAaImAgInS	být
již	již	k6eAd1	již
neobyvatelný	obyvatelný	k2eNgInSc1d1	neobyvatelný
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1753	[number]	k4	1753
<g/>
-	-	kIx~	-
<g/>
1759	[number]	k4	1759
<g/>
,	,	kIx,	,
dokončovací	dokončovací	k2eAgFnSc2d1	dokončovací
práce	práce	k1gFnSc2	práce
pobíhaly	pobíhat	k5eAaImAgFnP	pobíhat
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1763	[number]	k4	1763
<g/>
.	.	kIx.	.
</s>
<s>
Celkové	celkový	k2eAgInPc1d1	celkový
náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
stavbu	stavba	k1gFnSc4	stavba
činily	činit	k5eAaImAgInP	činit
82	[number]	k4	82
410	[number]	k4	410
zlatých	zlatá	k1gFnPc2	zlatá
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
přibližně	přibližně	k6eAd1	přibližně
27	[number]	k4	27
000	[number]	k4	000
stál	stát	k5eAaImAgInS	stát
materiál	materiál	k1gInSc1	materiál
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Zbylé	zbylý	k2eAgFnPc1d1	zbylá
finance	finance	k1gFnPc1	finance
pokryly	pokrýt	k5eAaPmAgFnP	pokrýt
mzdy	mzda	k1gFnPc1	mzda
zedníků	zedník	k1gMnPc2	zedník
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
22	[number]	k4	22
000	[number]	k4	000
zl	zl	k?	zl
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
ostatní	ostatní	k2eAgFnPc4d1	ostatní
práce	práce	k1gFnPc4	práce
(	(	kIx(	(
<g/>
přibližně	přibližně	k6eAd1	přibližně
33	[number]	k4	33
000	[number]	k4	000
zl	zl	k?	zl
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
z	z	k7c2	z
čehož	což	k3yQnSc2	což
bylo	být	k5eAaImAgNnS	být
4	[number]	k4	4
996	[number]	k4	996
zl	zl	k?	zl
<g/>
.	.	kIx.	.
za	za	k7c2	za
práce	práce	k1gFnSc2	práce
zámečnické	zámečnický	k2eAgInPc1d1	zámečnický
<g/>
,	,	kIx,	,
4	[number]	k4	4
042	[number]	k4	042
zl	zl	k?	zl
<g/>
.	.	kIx.	.
za	za	k7c2	za
práce	práce	k1gFnSc2	práce
kamenické	kamenický	k2eAgFnSc2d1	kamenická
a	a	k8xC	a
3	[number]	k4	3
662	[number]	k4	662
zl	zl	k?	zl
<g/>
.	.	kIx.	.
za	za	k7c4	za
malířskou	malířský	k2eAgFnSc4d1	malířská
výzdobu	výzdoba	k1gFnSc4	výzdoba
od	od	k7c2	od
Judy	judo	k1gNnPc7	judo
Tadeáše	Tadeáš	k1gMnSc2	Tadeáš
Supera	Super	k1gMnSc2	Super
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
platy	plat	k1gInPc4	plat
tesařům	tesař	k1gMnPc3	tesař
a	a	k8xC	a
sochařům	sochař	k1gMnPc3	sochař
<g/>
)	)	kIx)	)
</s>
