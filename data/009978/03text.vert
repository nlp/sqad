<p>
<s>
Sv.	sv.	kA	sv.
Evaristus	Evaristus	k1gMnSc1	Evaristus
(	(	kIx(	(
<g/>
Aristus	Aristus	k1gMnSc1	Aristus
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
5	[number]	k4	5
<g/>
.	.	kIx.	.
papežem	papež	k1gMnSc7	papež
katolické	katolický	k2eAgFnSc2d1	katolická
církve	církev	k1gFnSc2	církev
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
pontifikát	pontifikát	k1gInSc1	pontifikát
se	se	k3xPyFc4	se
datuje	datovat	k5eAaImIp3nS	datovat
do	do	k7c2	do
let	léto	k1gNnPc2	léto
98	[number]	k4	98
<g/>
/	/	kIx~	/
<g/>
101	[number]	k4	101
–	–	k?	–
105	[number]	k4	105
<g/>
/	/	kIx~	/
<g/>
107	[number]	k4	107
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
zemřel	zemřít	k5eAaPmAgMnS	zemřít
jako	jako	k9	jako
mučedník	mučedník	k1gMnSc1	mučedník
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Život	život	k1gInSc1	život
==	==	k?	==
</s>
</p>
<p>
<s>
Datum	datum	k1gNnSc1	datum
narození	narození	k1gNnSc2	narození
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
.	.	kIx.	.
</s>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
107	[number]	k4	107
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Liberiově	Liberiově	k1gFnSc6	Liberiově
katalogu	katalog	k1gInSc2	katalog
(	(	kIx(	(
<g/>
Catalogus	Catalogus	k1gMnSc1	Catalogus
Liberianus	Liberianus	k1gMnSc1	Liberianus
–	–	k?	–
seznam	seznam	k1gInSc1	seznam
prvních	první	k4xOgMnPc2	první
papežů	papež	k1gMnPc2	papež
zpracovaný	zpracovaný	k2eAgInSc1d1	zpracovaný
papežem	papež	k1gMnSc7	papež
Liberiem	Liberium	k1gNnSc7	Liberium
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
350	[number]	k4	350
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
jméno	jméno	k1gNnSc1	jméno
uvedeno	uvést	k5eAaPmNgNnS	uvést
jako	jako	k8xC	jako
Aristus	Aristus	k1gInSc1	Aristus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
papežských	papežský	k2eAgInPc6d1	papežský
seznamech	seznam	k1gInPc6	seznam
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yIgMnPc2	který
čerpali	čerpat	k5eAaImAgMnP	čerpat
církevní	církevní	k2eAgMnPc1d1	církevní
historici	historik	k1gMnPc1	historik
2	[number]	k4	2
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
sv.	sv.	kA	sv.
Irenaeus	Irenaeus	k1gMnSc1	Irenaeus
a	a	k8xC	a
Hippolytus	Hippolytus	k1gMnSc1	Hippolytus
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
uváděn	uvádět	k5eAaImNgMnS	uvádět
jako	jako	k9	jako
čtvrtý	čtvrtý	k4xOgMnSc1	čtvrtý
následník	následník	k1gMnSc1	následník
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc4	Petr
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
papežem	papež	k1gMnSc7	papež
po	po	k7c6	po
sv.	sv.	kA	sv.
Klementovi	Klementův	k2eAgMnPc1d1	Klementův
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
prameny	pramen	k1gInPc1	pramen
rovněž	rovněž	k9	rovněž
uvádějí	uvádět	k5eAaImIp3nP	uvádět
dobu	doba	k1gFnSc4	doba
pontifikátu	pontifikát	k1gInSc2	pontifikát
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
osmi	osm	k4xCc2	osm
let	léto	k1gNnPc2	léto
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
od	od	k7c2	od
r.	r.	kA	r.
98	[number]	k4	98
nebo	nebo	k8xC	nebo
99	[number]	k4	99
do	do	k7c2	do
roku	rok	k1gInSc2	rok
106	[number]	k4	106
nebo	nebo	k8xC	nebo
107	[number]	k4	107
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Raně	raně	k6eAd1	raně
křesťanské	křesťanský	k2eAgInPc4d1	křesťanský
zdroje	zdroj	k1gInPc4	zdroj
nám	my	k3xPp1nPc3	my
nenabízejí	nabízet	k5eNaImIp3nP	nabízet
žádné	žádný	k3yNgInPc4	žádný
autentické	autentický	k2eAgInPc4d1	autentický
údaje	údaj	k1gInPc4	údaj
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
životě	život	k1gInSc6	život
<g/>
.	.	kIx.	.
</s>
<s>
V	V	kA	V
"	"	kIx"	"
<g/>
Liber	libra	k1gFnPc2	libra
Pontificalis	Pontificalis	k1gInSc1	Pontificalis
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
řecké	řecký	k2eAgFnSc2d1	řecká
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
byl	být	k5eAaImAgMnS	být
synem	syn	k1gMnSc7	syn
Žida	Žid	k1gMnSc2	Žid
z	z	k7c2	z
Betléma	Betlém	k1gInSc2	Betlém
<g/>
.	.	kIx.	.
</s>
<s>
Připisuje	připisovat	k5eAaImIp3nS	připisovat
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
nařízení	nařízení	k1gNnSc3	nařízení
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nejen	nejen	k6eAd1	nejen
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
každém	každý	k3xTgNnSc6	každý
biskupském	biskupský	k2eAgNnSc6d1	Biskupské
sídle	sídlo	k1gNnSc6	sídlo
bylo	být	k5eAaImAgNnS	být
sedm	sedm	k4xCc1	sedm
jáhnů	jáhen	k1gMnPc2	jáhen
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
údajně	údajně	k6eAd1	údajně
z	z	k7c2	z
jeho	jeho	k3xOp3gNnSc2	jeho
rozhodnutí	rozhodnutí	k1gNnSc2	rozhodnutí
pochází	pocházet	k5eAaImIp3nS	pocházet
i	i	k9	i
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
sňatky	sňatek	k1gInPc1	sňatek
mají	mít	k5eAaImIp3nP	mít
být	být	k5eAaImF	být
veřejně	veřejně	k6eAd1	veřejně
posvěcovány	posvěcovat	k5eAaImNgInP	posvěcovat
knězem	kněz	k1gMnSc7	kněz
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgMnS	být
mučedníkem	mučedník	k1gMnSc7	mučedník
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
tato	tento	k3xDgFnSc1	tento
informace	informace	k1gFnSc1	informace
není	být	k5eNaImIp3nS	být
ničím	nic	k3yNnSc7	nic
podložena	podložit	k5eAaPmNgFnS	podložit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zemřel	zemřít	k5eAaPmAgMnS	zemřít
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
107	[number]	k4	107
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
pochován	pochován	k2eAgMnSc1d1	pochován
ve	v	k7c6	v
Vatikánu	Vatikán	k1gInSc6	Vatikán
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
hrobky	hrobka	k1gFnSc2	hrobka
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
památku	památka	k1gFnSc4	památka
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
připomíná	připomínat	k5eAaImIp3nS	připomínat
27	[number]	k4	27
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
SCHAUBER	SCHAUBER	kA	SCHAUBER
<g/>
,	,	kIx,	,
Vera	Vera	k1gMnSc1	Vera
<g/>
;	;	kIx,	;
SCHINDLER	Schindler	k1gMnSc1	Schindler
<g/>
,	,	kIx,	,
Hanns	Hanns	k1gInSc1	Hanns
Michael	Michaela	k1gFnPc2	Michaela
<g/>
.	.	kIx.	.
</s>
<s>
Rok	rok	k1gInSc1	rok
se	s	k7c7	s
svatými	svatá	k1gFnPc7	svatá
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Kostelní	kostelní	k2eAgNnSc1d1	kostelní
Vydří	vydří	k2eAgNnSc1d1	vydří
<g/>
:	:	kIx,	:
Karmelitánské	karmelitánský	k2eAgNnSc1d1	Karmelitánské
nakladatelství	nakladatelství	k1gNnSc1	nakladatelství
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
702	[number]	k4	702
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7192	[number]	k4	7192
<g/>
-	-	kIx~	-
<g/>
304	[number]	k4	304
<g/>
-	-	kIx~	-
<g/>
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
GELMI	GELMI	kA	GELMI
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
<g/>
.	.	kIx.	.
</s>
<s>
Papežové	Papež	k1gMnPc1	Papež
:	:	kIx,	:
Od	od	k7c2	od
svatého	svatý	k2eAgMnSc2d1	svatý
Petra	Petr	k1gMnSc2	Petr
po	po	k7c4	po
Jana	Jan	k1gMnSc4	Jan
Pavla	Pavel	k1gMnSc2	Pavel
II	II	kA	II
<g/>
..	..	k?	..
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Mladá	mladý	k2eAgFnSc1d1	mladá
fronta	fronta	k1gFnSc1	fronta
<g/>
,	,	kIx,	,
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
328	[number]	k4	328
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
204	[number]	k4	204
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
457	[number]	k4	457
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MAXWELL-STUART	MAXWELL-STUART	k?	MAXWELL-STUART
<g/>
,	,	kIx,	,
P.	P.	kA	P.
<g/>
G.	G.	kA	G.
Papežové	Papež	k1gMnPc1	Papež
<g/>
,	,	kIx,	,
život	život	k1gInSc1	život
a	a	k8xC	a
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
sv.	sv.	kA	sv.
Petra	Petr	k1gMnSc2	Petr
k	k	k7c3	k
Janu	Jan	k1gMnSc3	Jan
Pavlu	Pavel	k1gMnSc3	Pavel
II	II	kA	II
<g/>
..	..	k?	..
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
(	(	kIx(	(
<g/>
servis	servis	k1gInSc1	servis
<g/>
)	)	kIx)	)
240	[number]	k4	240
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
902300	[number]	k4	902300
<g/>
-	-	kIx~	-
<g/>
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RENDINA	RENDINA	kA	RENDINA
<g/>
,	,	kIx,	,
Claudio	Claudio	k6eAd1	Claudio
<g/>
.	.	kIx.	.
</s>
<s>
Příběhy	příběh	k1gInPc1	příběh
papežů	papež	k1gMnPc2	papež
:	:	kIx,	:
dějiny	dějiny	k1gFnPc1	dějiny
a	a	k8xC	a
tajemství	tajemství	k1gNnSc1	tajemství
:	:	kIx,	:
životopisy	životopis	k1gInPc4	životopis
265	[number]	k4	265
římských	římský	k2eAgMnPc2d1	římský
papežů	papež	k1gMnPc2	papež
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Volvox	Volvox	k1gInSc1	Volvox
Globator	Globator	k1gInSc1	Globator
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
714	[number]	k4	714
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7207	[number]	k4	7207
<g/>
-	-	kIx~	-
<g/>
574	[number]	k4	574
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Evaristus	Evaristus	k1gInSc1	Evaristus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
The	The	k1gFnSc3	The
Catholic	Catholice	k1gFnPc2	Catholice
Encyclopedia	Encyclopedium	k1gNnSc2	Encyclopedium
-	-	kIx~	-
Pope	pop	k1gInSc5	pop
St.	st.	kA	st.
Evaristus	Evaristus	k1gInSc1	Evaristus
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Pope	pop	k1gInSc5	pop
St.	st.	kA	st.
Evaristus	Evaristus	k1gInSc1	Evaristus
-	-	kIx~	-
saints	saints	k1gInSc1	saints
<g/>
.	.	kIx.	.
<g/>
sqpn	sqpn	k1gInSc1	sqpn
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
