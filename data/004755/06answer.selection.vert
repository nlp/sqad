<s>
Představovaly	představovat	k5eAaImAgFnP	představovat
německé	německý	k2eAgFnPc1d1	německá
zvláštní	zvláštní	k2eAgFnPc1d1	zvláštní
a	a	k8xC	a
cizinecké	cizinecký	k2eAgFnPc1d1	cizinecká
jednotky	jednotka	k1gFnPc1	jednotka
nacistické	nacistický	k2eAgFnSc2d1	nacistická
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
na	na	k7c6	na
konci	konec	k1gInSc6	konec
války	válka	k1gFnSc2	válka
zahrnovaly	zahrnovat	k5eAaImAgInP	zahrnovat
přibližně	přibližně	k6eAd1	přibližně
950	[number]	k4	950
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
dobrovolníků	dobrovolník	k1gMnPc2	dobrovolník
<g/>
.	.	kIx.	.
</s>
