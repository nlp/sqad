<s>
Lineární	lineární	k2eAgInSc1d1
přenos	přenos	k1gInSc1
energie	energie	k1gFnSc2
(	(	kIx(
<g/>
LET	let	k1gInSc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
fyzikální	fyzikální	k2eAgFnSc1d1
veličina	veličina	k1gFnSc1
popisující	popisující	k2eAgFnSc1d1
hustotu	hustota	k1gFnSc4
předávání	předávání	k1gNnSc2
energie	energie	k1gFnSc2
prostředí	prostředí	k1gNnSc4
ionizujícím	ionizující	k2eAgNnSc7d1
zářením	záření	k1gNnSc7
<g/>
.	.	kIx.
</s>