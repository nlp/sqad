<p>
<s>
Lineární	lineární	k2eAgInSc1d1	lineární
přenos	přenos	k1gInSc1	přenos
energie	energie	k1gFnSc2	energie
(	(	kIx(	(
<g/>
LET	let	k1gInSc1	let
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
fyzikální	fyzikální	k2eAgFnSc1d1	fyzikální
veličina	veličina	k1gFnSc1	veličina
popisující	popisující	k2eAgFnSc1d1	popisující
hustotu	hustota	k1gFnSc4	hustota
předávání	předávání	k1gNnSc2	předávání
energie	energie	k1gFnSc2	energie
prostředí	prostředí	k1gNnSc4	prostředí
ionizujícím	ionizující	k2eAgNnSc7d1	ionizující
zářením	záření	k1gNnSc7	záření
<g/>
.	.	kIx.	.
</s>
<s>
Používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
popis	popis	k1gInSc4	popis
vlastností	vlastnost	k1gFnPc2	vlastnost
ionizujícího	ionizující	k2eAgNnSc2d1	ionizující
záření	záření	k1gNnSc2	záření
v	v	k7c6	v
dozimetrii	dozimetrie	k1gFnSc6	dozimetrie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
LET	let	k1gInSc1	let
je	být	k5eAaImIp3nS	být
definován	definovat	k5eAaBmNgInS	definovat
pouze	pouze	k6eAd1	pouze
pro	pro	k7c4	pro
nabité	nabitý	k2eAgFnPc4d1	nabitá
částice	částice	k1gFnPc4	částice
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jako	jako	k9	jako
poměr	poměr	k1gInSc1	poměr
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
</p>
<p>
<s>
Δ	Δ	k?	Δ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
<s>
l	l	kA	l
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Δ	Δ	k?	Δ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L_	L_	k1gMnPc6	L_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
left	left	k5eAaPmF	left
<g/>
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k1gFnSc1	frac
{	{	kIx(	{
<g/>
dE	dE	k?	dE
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
dl	dl	k?	dl
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
right	right	k5eAaPmF	right
<g/>
)	)	kIx)	)
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
}}	}}	k?	}}
</s>
</p>
<p>
</p>
<p>
<s>
přitom	přitom	k6eAd1	přitom
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
<s>
l	l	kA	l
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
dl	dl	k?	dl
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
tu	tu	k6eAd1	tu
je	být	k5eAaImIp3nS	být
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
částice	částice	k1gFnSc1	částice
prošla	projít	k5eAaPmAgFnS	projít
<g/>
,	,	kIx,	,
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
<s>
E	E	kA	E
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
dE	dE	k?	dE
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
energie	energie	k1gFnSc1	energie
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
po	po	k7c6	po
této	tento	k3xDgFnSc6	tento
dráze	dráha	k1gFnSc6	dráha
předala	předat	k5eAaPmAgFnS	předat
prostředí	prostředí	k1gNnSc4	prostředí
pomocí	pomocí	k7c2	pomocí
nepružných	pružný	k2eNgFnPc2d1	nepružná
srážek	srážka	k1gFnPc2	srážka
doprovázených	doprovázený	k2eAgFnPc2d1	doprovázená
přenosem	přenos	k1gInSc7	přenos
energie	energie	k1gFnPc4	energie
nižším	nízký	k2eAgFnPc3d2	nižší
jak	jak	k8xC	jak
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Δ	Δ	k?	Δ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
LET	let	k1gInSc1	let
představuje	představovat	k5eAaImIp3nS	představovat
přechodovou	přechodový	k2eAgFnSc4d1	přechodová
veličinu	veličina	k1gFnSc4	veličina
mezi	mezi	k7c7	mezi
dozimetrií	dozimetrie	k1gFnSc7	dozimetrie
a	a	k8xC	a
mikrodozimetrií	mikrodozimetrie	k1gFnSc7	mikrodozimetrie
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
nestochastická	stochastický	k2eNgFnSc1d1	stochastický
veličina	veličina	k1gFnSc1	veličina
jako	jako	k8xS	jako
třeba	třeba	k6eAd1	třeba
dávka	dávka	k1gFnSc1	dávka
<g/>
,	,	kIx,	,
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
ale	ale	k9	ale
díky	díky	k7c3	díky
omezení	omezení	k1gNnSc3	omezení
předávané	předávaný	k2eAgFnSc2d1	předávaná
energie	energie	k1gFnSc2	energie
zohledňuje	zohledňovat	k5eAaImIp3nS	zohledňovat
lokální	lokální	k2eAgFnSc3d1	lokální
depozici	depozice	k1gFnSc3	depozice
energie	energie	k1gFnSc2	energie
-	-	kIx~	-
čím	co	k3yInSc7	co
nižší	nízký	k2eAgFnSc1d2	nižší
je	být	k5eAaImIp3nS	být
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Δ	Δ	k?	Δ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
blíže	blízce	k6eAd2	blízce
k	k	k7c3	k
místu	místo	k1gNnSc3	místo
primární	primární	k2eAgFnSc2d1	primární
interakce	interakce	k1gFnSc2	interakce
je	být	k5eAaImIp3nS	být
energie	energie	k1gFnSc1	energie
nakonec	nakonec	k6eAd1	nakonec
deponována	deponován	k2eAgFnSc1d1	deponována
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Při	při	k7c6	při
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Δ	Δ	k?	Δ
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
<s>
∞	∞	k?	∞
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Delta	delta	k1gFnSc1	delta
\	\	kIx~	\
<g/>
rightarrow	rightarrow	k?	rightarrow
\	\	kIx~	\
<g/>
infty	infta	k1gMnSc2	infta
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
LET	let	k1gInSc1	let
číselně	číselně	k6eAd1	číselně
roven	roven	k2eAgInSc1d1	roven
brzdné	brzdný	k2eAgFnSc3d1	brzdná
schopnosti	schopnost	k1gFnSc3	schopnost
nabité	nabitý	k2eAgFnPc1d1	nabitá
částice	částice	k1gFnPc1	částice
pro	pro	k7c4	pro
nepružné	pružný	k2eNgFnPc4d1	nepružná
srážky	srážka	k1gFnPc4	srážka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Koncept	koncept	k1gInSc1	koncept
LET	let	k1gInSc1	let
byl	být	k5eAaImAgInS	být
zaveden	zavést	k5eAaPmNgInS	zavést
v	v	k7c6	v
padesátých	padesátý	k4xOgNnPc6	padesátý
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
a	a	k8xC	a
literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
ICRU	ICRU	kA	ICRU
report	report	k1gInSc1	report
36	[number]	k4	36
<g/>
,	,	kIx,	,
1983	[number]	k4	1983
</s>
</p>
<p>
<s>
Principy	princip	k1gInPc1	princip
a	a	k8xC	a
praxe	praxe	k1gFnPc1	praxe
radiační	radiační	k2eAgFnSc2d1	radiační
ochrany	ochrana	k1gFnSc2	ochrana
<g/>
,	,	kIx,	,
editor	editor	k1gMnSc1	editor
Vladislav	Vladislav	k1gMnSc1	Vladislav
Klener	Klener	k1gMnSc1	Klener
<g/>
.	.	kIx.	.
</s>
<s>
Azin	Azin	k1gInSc1	Azin
CZ	CZ	kA	CZ
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
</s>
</p>
