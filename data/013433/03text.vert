<p>
<s>
Amyotrofická	amyotrofický	k2eAgFnSc1d1	amyotrofická
laterální	laterální	k2eAgFnSc1d1	laterální
skleróza	skleróza	k1gFnSc1	skleróza
(	(	kIx(	(
<g/>
zkratka	zkratka	k1gFnSc1	zkratka
ALS	ALS	kA	ALS
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
též	též	k9	též
jako	jako	k8xS	jako
Lou	Lou	k1gFnSc1	Lou
Gehrigova	Gehrigův	k2eAgFnSc1d1	Gehrigova
choroba	choroba	k1gFnSc1	choroba
či	či	k8xC	či
onemocnění	onemocnění	k1gNnSc1	onemocnění
motoneuronu	motoneuron	k1gInSc2	motoneuron
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
progresivní	progresivní	k2eAgMnSc1d1	progresivní
<g/>
,	,	kIx,	,
fatální	fatální	k2eAgMnSc1d1	fatální
<g/>
,	,	kIx,	,
neurodegenerativní	urodegenerativní	k2eNgNnSc1d1	neurodegenerativní
onemocnění	onemocnění	k1gNnSc1	onemocnění
motorických	motorický	k2eAgInPc2d1	motorický
neuronů	neuron	k1gInPc2	neuron
mozku	mozek	k1gInSc2	mozek
a	a	k8xC	a
míchy	mícha	k1gFnSc2	mícha
<g/>
,	,	kIx,	,
způsobující	způsobující	k2eAgFnSc4d1	způsobující
degeneraci	degenerace	k1gFnSc4	degenerace
a	a	k8xC	a
ztrátu	ztráta	k1gFnSc4	ztráta
mozkových	mozkový	k2eAgFnPc2d1	mozková
(	(	kIx(	(
<g/>
horní	horní	k2eAgFnSc7d1	horní
<g/>
)	)	kIx)	)
a	a	k8xC	a
spinálních	spinální	k2eAgFnPc6d1	spinální
(	(	kIx(	(
<g/>
dolní	dolní	k2eAgFnSc1d1	dolní
<g/>
)	)	kIx)	)
motoneuronů	motoneuron	k1gInPc2	motoneuron
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
buněk	buňka	k1gFnPc2	buňka
centrální	centrální	k2eAgFnSc2d1	centrální
nervové	nervový	k2eAgFnSc2d1	nervová
soustavy	soustava	k1gFnSc2	soustava
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
ovládají	ovládat	k5eAaImIp3nP	ovládat
vůlí	vůle	k1gFnSc7	vůle
ovlivnitelné	ovlivnitelný	k2eAgInPc1d1	ovlivnitelný
svalové	svalový	k2eAgInPc1d1	svalový
pohyby	pohyb	k1gInPc1	pohyb
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
tomu	ten	k3xDgNnSc3	ten
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
postupné	postupný	k2eAgFnSc3d1	postupná
svalové	svalový	k2eAgFnSc3d1	svalová
slabosti	slabost	k1gFnSc3	slabost
až	až	k8xS	až
atrofii	atrofie	k1gFnSc3	atrofie
<g/>
.	.	kIx.	.
</s>
<s>
Mozek	mozek	k1gInSc1	mozek
nakonec	nakonec	k6eAd1	nakonec
není	být	k5eNaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
ovládat	ovládat	k5eAaImF	ovládat
většinu	většina	k1gFnSc4	většina
svalů	sval	k1gInPc2	sval
a	a	k8xC	a
pacient	pacient	k1gMnSc1	pacient
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
paralyzován	paralyzovat	k5eAaBmNgMnS	paralyzovat
<g/>
,	,	kIx,	,
při	při	k7c6	při
zachování	zachování	k1gNnSc6	zachování
psychických	psychický	k2eAgFnPc2d1	psychická
a	a	k8xC	a
mentálních	mentální	k2eAgFnPc2d1	mentální
schopností	schopnost	k1gFnPc2	schopnost
<g/>
.	.	kIx.	.
</s>
<s>
Příčiny	příčina	k1gFnPc1	příčina
onemocnění	onemocnění	k1gNnSc2	onemocnění
nebyly	být	k5eNaImAgFnP	být
zatím	zatím	k6eAd1	zatím
dostatečně	dostatečně	k6eAd1	dostatečně
objasněny	objasněn	k2eAgFnPc1d1	objasněna
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
Lou	Lou	k1gFnSc2	Lou
Gehrigova	Gehrigův	k2eAgFnSc1d1	Gehrigova
choroba	choroba	k1gFnSc1	choroba
toto	tento	k3xDgNnSc4	tento
onemocnění	onemocnění	k1gNnSc1	onemocnění
získalo	získat	k5eAaPmAgNnS	získat
po	po	k7c6	po
slavném	slavný	k2eAgMnSc6d1	slavný
baseballovém	baseballový	k2eAgMnSc6d1	baseballový
hráči	hráč	k1gMnSc6	hráč
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Yankees	Yankees	k1gInSc1	Yankees
<g/>
,	,	kIx,	,
Lou	Lou	k1gMnSc3	Lou
Gehrigovi	Gehrig	k1gMnSc3	Gehrig
<g/>
,	,	kIx,	,
u	u	k7c2	u
něhož	jenž	k3xRgInSc2	jenž
byla	být	k5eAaImAgFnS	být
ALS	ALS	kA	ALS
diagnostikována	diagnostikovat	k5eAaBmNgNnP	diagnostikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
současné	současný	k2eAgFnPc4d1	současná
osobnosti	osobnost	k1gFnPc4	osobnost
postižené	postižený	k2eAgFnPc4d1	postižená
ALS	ALS	kA	ALS
patří	patřit	k5eAaImIp3nS	patřit
kytarista	kytarista	k1gMnSc1	kytarista
Jason	Jason	k1gMnSc1	Jason
Becker	Becker	k1gMnSc1	Becker
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
chorobou	choroba	k1gFnSc7	choroba
trpěli	trpět	k5eAaImAgMnP	trpět
např.	např.	kA	např.
astrofyzik	astrofyzika	k1gFnPc2	astrofyzika
Stephen	Stephna	k1gFnPc2	Stephna
Hawking	Hawking	k1gInSc1	Hawking
<g/>
,	,	kIx,	,
česká	český	k2eAgFnSc1d1	Česká
překladatelka	překladatelka	k1gFnSc1	překladatelka
Dana	Dana	k1gFnSc1	Dana
Gálová	Gálová	k1gFnSc1	Gálová
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
dabér	dabér	k1gMnSc1	dabér
Martin	Martin	k1gMnSc1	Martin
Kolár	Kolár	k1gMnSc1	Kolár
či	či	k8xC	či
bývalý	bývalý	k2eAgMnSc1d1	bývalý
český	český	k2eAgMnSc1d1	český
premiér	premiér	k1gMnSc1	premiér
Stanislav	Stanislav	k1gMnSc1	Stanislav
Gross	Gross	k1gMnSc1	Gross
<g/>
.	.	kIx.	.
</s>
<s>
Onemocněli	onemocnět	k5eAaPmAgMnP	onemocnět
jí	on	k3xPp3gFnSc3	on
také	také	k9	také
fotbalista	fotbalista	k1gMnSc1	fotbalista
Marián	Marián	k1gMnSc1	Marián
Čišovský	Čišovský	k1gMnSc1	Čišovský
nebo	nebo	k8xC	nebo
masér	masér	k1gMnSc1	masér
české	český	k2eAgFnSc2d1	Česká
fotbalové	fotbalový	k2eAgFnSc2d1	fotbalová
i	i	k8xC	i
futsalové	futsalové	k2eAgFnSc2d1	futsalové
reprezentace	reprezentace	k1gFnSc2	reprezentace
Vladimír	Vladimír	k1gMnSc1	Vladimír
Mikuláš	Mikuláš	k1gMnSc1	Mikuláš
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc1	popis
onemocnění	onemocnění	k1gNnSc2	onemocnění
==	==	k?	==
</s>
</p>
<p>
<s>
Rozlišujeme	rozlišovat	k5eAaImIp1nP	rozlišovat
tři	tři	k4xCgFnPc4	tři
formy	forma	k1gFnPc4	forma
ALS	ALS	kA	ALS
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc4	ten
sporadickou	sporadický	k2eAgFnSc4d1	sporadická
<g/>
,	,	kIx,	,
familiární	familiární	k2eAgFnSc4d1	familiární
a	a	k8xC	a
guamskou	guamský	k2eAgFnSc4d1	guamská
<g/>
.	.	kIx.	.
</s>
<s>
Nejčastější	častý	k2eAgFnSc7d3	nejčastější
formou	forma	k1gFnSc7	forma
je	být	k5eAaImIp3nS	být
forma	forma	k1gFnSc1	forma
sporadická	sporadický	k2eAgFnSc1d1	sporadická
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
familiární	familiární	k2eAgFnSc1d1	familiární
forma	forma	k1gFnSc1	forma
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
–	–	k?	–
<g/>
10	[number]	k4	10
%	%	kIx~	%
<g/>
)	)	kIx)	)
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
dřívější	dřívější	k2eAgInSc4d1	dřívější
výskyt	výskyt	k1gInSc4	výskyt
v	v	k7c6	v
rodině	rodina	k1gFnSc6	rodina
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
dědičný	dědičný	k2eAgInSc4d1	dědičný
původ	původ	k1gInSc4	původ
<g/>
.	.	kIx.	.
</s>
<s>
Třetí	třetí	k4xOgFnSc1	třetí
zmíněná	zmíněný	k2eAgFnSc1d1	zmíněná
forma	forma	k1gFnSc1	forma
odkazuje	odkazovat	k5eAaImIp3nS	odkazovat
k	k	k7c3	k
mimořádnému	mimořádný	k2eAgInSc3d1	mimořádný
výskytu	výskyt	k1gInSc3	výskyt
onemocnění	onemocnění	k1gNnPc2	onemocnění
na	na	k7c6	na
tichomořském	tichomořský	k2eAgInSc6d1	tichomořský
ostrově	ostrov	k1gInSc6	ostrov
Guam	Guam	k1gInSc1	Guam
v	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rané	raný	k2eAgNnSc1d1	rané
stádium	stádium	k1gNnSc1	stádium
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c7	mezi
příznaky	příznak	k1gInPc7	příznak
rané	raný	k2eAgFnSc2d1	raná
formy	forma	k1gFnSc2	forma
ALS	ALS	kA	ALS
patří	patřit	k5eAaImIp3nS	patřit
zvýšená	zvýšený	k2eAgFnSc1d1	zvýšená
svalová	svalový	k2eAgFnSc1d1	svalová
slabost	slabost	k1gFnSc1	slabost
a	a	k8xC	a
atrofie	atrofie	k1gFnSc1	atrofie
(	(	kIx(	(
<g/>
75	[number]	k4	75
%	%	kIx~	%
případů	případ	k1gInPc2	případ
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
a	a	k8xC	a
nohách	noha	k1gFnPc6	noha
<g/>
,	,	kIx,	,
a	a	k8xC	a
postižení	postižení	k1gNnSc1	postižení
řeči	řeč	k1gFnSc2	řeč
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
případech	případ	k1gInPc6	případ
polykání	polykání	k1gNnSc2	polykání
<g/>
,	,	kIx,	,
dýchání	dýchání	k1gNnSc2	dýchání
<g/>
,	,	kIx,	,
křeče	křeč	k1gFnPc4	křeč
nebo	nebo	k8xC	nebo
ztuhlost	ztuhlost	k1gFnSc4	ztuhlost
postižených	postižený	k2eAgInPc2d1	postižený
svalů	sval	k1gInPc2	sval
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
ztuhlost	ztuhlost	k1gFnSc1	ztuhlost
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
projevovat	projevovat	k5eAaImF	projevovat
neslyšitelnosti	neslyšitelnost	k1gFnSc2	neslyšitelnost
řeči	řeč	k1gFnSc2	řeč
<g/>
.	.	kIx.	.
</s>
<s>
Projevem	projev	k1gInSc7	projev
bývá	bývat	k5eAaImIp3nS	bývat
neschopnost	neschopnost	k1gFnSc1	neschopnost
zapnout	zapnout	k5eAaPmF	zapnout
knoflík	knoflík	k1gInSc4	knoflík
od	od	k7c2	od
košile	košile	k1gFnSc2	košile
<g/>
,	,	kIx,	,
psát	psát	k5eAaImF	psát
nebo	nebo	k8xC	nebo
otočení	otočení	k1gNnSc4	otočení
klíče	klíč	k1gInSc2	klíč
v	v	k7c6	v
zámku	zámek	k1gInSc6	zámek
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
pacientů	pacient	k1gMnPc2	pacient
se	se	k3xPyFc4	se
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
pouze	pouze	k6eAd1	pouze
chronická	chronický	k2eAgNnPc4d1	chronické
postižení	postižení	k1gNnPc4	postižení
pár	pár	k4xCyI	pár
svalů	sval	k1gInPc2	sval
(	(	kIx(	(
<g/>
monomelická	monomelický	k2eAgFnSc1d1	monomelický
amyotrofie	amyotrofie	k1gFnSc1	amyotrofie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
dysfagie	dysfagie	k1gFnSc1	dysfagie
a	a	k8xC	a
dysartrie	dysartrie	k1gFnSc1	dysartrie
<g/>
.	.	kIx.	.
</s>
<s>
Postižení	postižení	k1gNnSc1	postižení
horních	horní	k2eAgFnPc2d1	horní
partií	partie	k1gFnPc2	partie
těla	tělo	k1gNnSc2	tělo
postupně	postupně	k6eAd1	postupně
směřuje	směřovat	k5eAaImIp3nS	směřovat
ke	k	k7c3	k
spasticitě	spasticita	k1gFnSc3	spasticita
a	a	k8xC	a
hyperreflexi	hyperreflexe	k1gFnSc3	hyperreflexe
včetně	včetně	k7c2	včetně
Babinského	Babinský	k2eAgInSc2d1	Babinský
reflexu	reflex	k1gInSc2	reflex
<g/>
.	.	kIx.	.
</s>
<s>
Postižení	postižení	k1gNnSc1	postižení
horních	horní	k2eAgFnPc2d1	horní
partií	partie	k1gFnPc2	partie
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
svalové	svalový	k2eAgFnSc3d1	svalová
slabosti	slabost	k1gFnSc3	slabost
<g/>
,	,	kIx,	,
atrofii	atrofie	k1gFnSc6	atrofie
<g/>
,	,	kIx,	,
křečím	křeč	k1gFnPc3	křeč
a	a	k8xC	a
viditelným	viditelný	k2eAgInPc3d1	viditelný
záškubům	záškub	k1gInPc3	záškub
(	(	kIx(	(
<g/>
fascikulace	fascikulace	k1gFnSc2	fascikulace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Přibližně	přibližně	k6eAd1	přibližně
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
45	[number]	k4	45
%	%	kIx~	%
pacientů	pacient	k1gMnPc2	pacient
trpí	trpět	k5eAaImIp3nS	trpět
psychickými	psychický	k2eAgFnPc7d1	psychická
poruchami	porucha	k1gFnPc7	porucha
např.	např.	kA	např.
nekontrolovaný	kontrolovaný	k2eNgInSc4d1	nekontrolovaný
smích	smích	k1gInSc4	smích
<g/>
,	,	kIx,	,
úsměv	úsměv	k1gInSc4	úsměv
<g/>
,	,	kIx,	,
pláč	pláč	k1gInSc4	pláč
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
demence	demence	k1gFnSc1	demence
frontálního	frontální	k2eAgInSc2d1	frontální
laloku	lalok	k1gInSc2	lalok
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
diagnostiku	diagnostika	k1gFnSc4	diagnostika
ALS	ALS	kA	ALS
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
postižení	postižení	k1gNnSc1	postižení
spodních	spodní	k2eAgFnPc2d1	spodní
i	i	k8xC	i
horních	horní	k2eAgFnPc2d1	horní
partií	partie	k1gFnPc2	partie
<g/>
.	.	kIx.	.
</s>
<s>
Obecným	obecný	k2eAgInSc7d1	obecný
znakem	znak	k1gInSc7	znak
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
smíšená	smíšený	k2eAgFnSc1d1	smíšená
paréza	paréza	k1gFnSc1	paréza
s	s	k7c7	s
fascikulacemi	fascikulace	k1gFnPc7	fascikulace
<g/>
,	,	kIx,	,
chybí	chybit	k5eAaPmIp3nS	chybit
senzitivní	senzitivní	k2eAgInSc4d1	senzitivní
deficit	deficit	k1gInSc4	deficit
a	a	k8xC	a
nejsou	být	k5eNaImIp3nP	být
přítomny	přítomen	k2eAgFnPc1d1	přítomna
žádné	žádný	k3yNgFnPc1	žádný
bolesti	bolest	k1gFnPc1	bolest
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Pokročilé	pokročilý	k2eAgNnSc1d1	pokročilé
stádium	stádium	k1gNnSc1	stádium
===	===	k?	===
</s>
</p>
<p>
<s>
Většina	většina	k1gFnSc1	většina
pacientů	pacient	k1gMnPc2	pacient
je	být	k5eAaImIp3nS	být
postupem	postup	k1gInSc7	postup
imobilní	imobilní	k2eAgFnSc1d1	imobilní
<g/>
,	,	kIx,	,
neschopná	schopný	k2eNgFnSc1d1	neschopná
ovládat	ovládat	k5eAaImF	ovládat
ruce	ruka	k1gFnPc4	ruka
a	a	k8xC	a
lokty	loket	k1gInPc4	loket
<g/>
.	.	kIx.	.
</s>
<s>
Ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
také	také	k9	také
schopnost	schopnost	k1gFnSc4	schopnost
mluvit	mluvit	k5eAaImF	mluvit
a	a	k8xC	a
polykat	polykat	k5eAaImF	polykat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
užít	užít	k5eAaPmF	užít
plicní	plicní	k2eAgFnPc4d1	plicní
ventilace	ventilace	k1gFnPc4	ventilace
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
popisu	popis	k1gInSc3	popis
progrese	progrese	k1gFnSc2	progrese
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
ALSFRS-R	ALSFRS-R	k1gMnSc1	ALSFRS-R
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
má	mít	k5eAaImIp3nS	mít
škálu	škála	k1gFnSc4	škála
od	od	k7c2	od
48	[number]	k4	48
do	do	k7c2	do
0	[number]	k4	0
bodů	bod	k1gInPc2	bod
(	(	kIx(	(
<g/>
úplná	úplný	k2eAgFnSc1d1	úplná
ztuhlost	ztuhlost	k1gFnSc1	ztuhlost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pacenti	Pacent	k1gMnPc1	Pacent
průměrně	průměrně	k6eAd1	průměrně
ztrácí	ztrácet	k5eAaImIp3nP	ztrácet
0,9	[number]	k4	0,9
bodu	bod	k1gInSc2	bod
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
měsíc	měsíc	k1gInSc4	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
limbární	limbární	k2eAgFnSc6d1	limbární
variantě	varianta	k1gFnSc6	varianta
postihuje	postihovat	k5eAaImIp3nS	postihovat
ALS	ALS	kA	ALS
nejprve	nejprve	k6eAd1	nejprve
jednu	jeden	k4xCgFnSc4	jeden
končetinu	končetina	k1gFnSc4	končetina
a	a	k8xC	a
následně	následně	k6eAd1	následně
druhou	druhý	k4xOgFnSc7	druhý
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
rozšíří	rozšířit	k5eAaPmIp3nS	rozšířit
dál	daleko	k6eAd2	daleko
<g/>
,	,	kIx,	,
v	v	k7c4	v
bulbární	bulbární	k2eAgInSc4d1	bulbární
(	(	kIx(	(
<g/>
prodloužená	prodloužená	k1gFnSc1	prodloužená
mícha	mícha	k1gFnSc1	mícha
<g/>
)	)	kIx)	)
variantě	varianta	k1gFnSc6	varianta
příznaky	příznak	k1gInPc1	příznak
začínají	začínat	k5eAaImIp3nP	začínat
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
a	a	k8xC	a
poté	poté	k6eAd1	poté
směřují	směřovat	k5eAaImIp3nP	směřovat
k	k	k7c3	k
nohám	noha	k1gFnPc3	noha
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
postižených	postižený	k1gMnPc2	postižený
umírá	umírat	k5eAaImIp3nS	umírat
na	na	k7c4	na
plicní	plicní	k2eAgNnSc4d1	plicní
selhání	selhání	k1gNnSc4	selhání
nebo	nebo	k8xC	nebo
zápal	zápal	k1gInSc4	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
se	se	k3xPyFc4	se
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
fází	fáze	k1gFnSc7	fáze
vyskytuje	vyskytovat	k5eAaImIp3nS	vyskytovat
stav	stav	k1gInSc1	stav
připomínající	připomínající	k2eAgInSc1d1	připomínající
tzv.	tzv.	kA	tzv.
syndrom	syndrom	k1gInSc1	syndrom
uzamčení	uzamčení	k1gNnSc2	uzamčení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Incidence	incidence	k1gFnSc1	incidence
onemocnění	onemocnění	k1gNnPc2	onemocnění
je	být	k5eAaImIp3nS	být
2,6	[number]	k4	2,6
na	na	k7c4	na
100	[number]	k4	100
000	[number]	k4	000
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
prevalence	prevalence	k1gFnSc1	prevalence
je	být	k5eAaImIp3nS	být
7-9	[number]	k4	7-9
na	na	k7c4	na
100	[number]	k4	100
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
.	.	kIx.	.
</s>
<s>
Rychlost	rychlost	k1gFnSc1	rychlost
<g/>
,	,	kIx,	,
jakou	jaký	k3yRgFnSc4	jaký
nemoc	nemoc	k1gFnSc4	nemoc
postupuje	postupovat	k5eAaImIp3nS	postupovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
individuální	individuální	k2eAgMnSc1d1	individuální
<g/>
.	.	kIx.	.
</s>
<s>
Průměrná	průměrný	k2eAgFnSc1d1	průměrná
doba	doba	k1gFnSc1	doba
nemoci	nemoc	k1gFnSc2	nemoc
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
4,3	[number]	k4	4,3
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
zhruba	zhruba	k6eAd1	zhruba
25	[number]	k4	25
<g/>
%	%	kIx~	%
pacientů	pacient	k1gMnPc2	pacient
přežije	přežít	k5eAaPmIp3nS	přežít
5	[number]	k4	5
let	léto	k1gNnPc2	léto
a	a	k8xC	a
8	[number]	k4	8
<g/>
–	–	k?	–
<g/>
16	[number]	k4	16
<g/>
%	%	kIx~	%
10	[number]	k4	10
let	léto	k1gNnPc2	léto
a	a	k8xC	a
5	[number]	k4	5
<g/>
%	%	kIx~	%
žije	žít	k5eAaImIp3nS	žít
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ALS	ALS	kA	ALS
neexistuje	existovat	k5eNaImIp3nS	existovat
žádná	žádný	k3yNgFnSc1	žádný
známá	známý	k2eAgFnSc1d1	známá
kauzální	kauzální	k2eAgFnSc1d1	kauzální
léčba	léčba	k1gFnSc1	léčba
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgInPc1d1	dostupný
léky	lék	k1gInPc1	lék
se	se	k3xPyFc4	se
zaměřují	zaměřovat	k5eAaImIp3nP	zaměřovat
především	především	k9	především
na	na	k7c4	na
zmírnění	zmírnění	k1gNnSc4	zmírnění
obtíží	obtíž	k1gFnPc2	obtíž
a	a	k8xC	a
prevenci	prevence	k1gFnSc4	prevence
komplikací	komplikace	k1gFnPc2	komplikace
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kauzalita	kauzalita	k1gFnSc1	kauzalita
==	==	k?	==
</s>
</p>
<p>
<s>
Výzkum	výzkum	k1gInSc1	výzkum
biologie	biologie	k1gFnSc2	biologie
a	a	k8xC	a
terapie	terapie	k1gFnSc2	terapie
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
limitován	limitovat	k5eAaBmNgMnS	limitovat
jednak	jednak	k8xC	jednak
ne	ne	k9	ne
zcela	zcela	k6eAd1	zcela
vhodným	vhodný	k2eAgInSc7d1	vhodný
zvířecím	zvířecí	k2eAgInSc7d1	zvířecí
modelem	model	k1gInSc7	model
(	(	kIx(	(
<g/>
potkani	potkan	k1gMnPc1	potkan
s	s	k7c7	s
mutací	mutace	k1gFnSc7	mutace
superoxiddismutázy	superoxiddismutáza	k1gFnSc2	superoxiddismutáza
SOD	soda	k1gFnPc2	soda
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
a	a	k8xC	a
jednak	jednak	k8xC	jednak
problémy	problém	k1gInPc4	problém
při	při	k7c6	při
organizaci	organizace	k1gFnSc6	organizace
klinických	klinický	k2eAgNnPc2d1	klinické
studií	studio	k1gNnPc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
není	být	k5eNaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
známá	známý	k2eAgFnSc1d1	známá
ani	ani	k8xC	ani
přesná	přesný	k2eAgFnSc1d1	přesná
příčina	příčina	k1gFnSc1	příčina
onemocnění	onemocnění	k1gNnSc1	onemocnění
ani	ani	k8xC	ani
vyvolávající	vyvolávající	k2eAgInPc1d1	vyvolávající
faktory	faktor	k1gInPc1	faktor
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
,	,	kIx,	,
zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
však	však	k9	však
<g/>
,	,	kIx,	,
že	že	k8xS	že
primární	primární	k2eAgFnSc7d1	primární
poruchou	porucha	k1gFnSc7	porucha
je	být	k5eAaImIp3nS	být
porucha	porucha	k1gFnSc1	porucha
degradace	degradace	k1gFnSc2	degradace
některých	některý	k3yIgInPc2	některý
proteinů	protein	k1gInPc2	protein
<g/>
,	,	kIx,	,
na	na	k7c6	na
rozvoji	rozvoj	k1gInSc6	rozvoj
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
i	i	k9	i
řada	řada	k1gFnSc1	řada
faktorů	faktor	k1gInPc2	faktor
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
např.	např.	kA	např.
nadprodukce	nadprodukce	k1gFnSc1	nadprodukce
volných	volný	k2eAgMnPc2d1	volný
radikálů	radikál	k1gMnPc2	radikál
nebo	nebo	k8xC	nebo
tzv.	tzv.	kA	tzv.
excitotoxicita	excitotoxicita	k1gFnSc1	excitotoxicita
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
vyloučit	vyloučit	k5eAaPmF	vyloučit
ani	ani	k8xC	ani
heterogenitu	heterogenita	k1gFnSc4	heterogenita
onemocnění	onemocnění	k1gNnPc2	onemocnění
<g/>
,	,	kIx,	,
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
dědičných	dědičný	k2eAgFnPc2d1	dědičná
poruch	porucha	k1gFnPc2	porucha
se	se	k3xPyFc4	se
podílí	podílet	k5eAaImIp3nS	podílet
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
funkčně	funkčně	k6eAd1	funkčně
poměrně	poměrně	k6eAd1	poměrně
odlišných	odlišný	k2eAgInPc2d1	odlišný
genů	gen	k1gInPc2	gen
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
terapii	terapie	k1gFnSc6	terapie
představuje	představovat	k5eAaImIp3nS	představovat
největší	veliký	k2eAgInSc4d3	veliký
přínos	přínos	k1gInSc4	přínos
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
prodloužení	prodloužení	k1gNnSc2	prodloužení
kvality	kvalita	k1gFnSc2	kvalita
i	i	k8xC	i
délky	délka	k1gFnSc2	délka
života	život	k1gInSc2	život
pacienta	pacient	k1gMnSc2	pacient
kvalitní	kvalitní	k2eAgFnSc1d1	kvalitní
multioborová	multioborový	k2eAgFnSc1d1	multioborová
symptomatická	symptomatický	k2eAgFnSc1d1	symptomatická
péče	péče	k1gFnSc1	péče
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Léčba	léčba	k1gFnSc1	léčba
==	==	k?	==
</s>
</p>
<p>
<s>
Z	z	k7c2	z
terapeutických	terapeutický	k2eAgInPc2d1	terapeutický
postupů	postup	k1gInPc2	postup
vykazuje	vykazovat	k5eAaImIp3nS	vykazovat
jistou	jistý	k2eAgFnSc4d1	jistá
efektivitu	efektivita	k1gFnSc4	efektivita
riluzol	riluzola	k1gFnPc2	riluzola
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
statisticky	statisticky	k6eAd1	statisticky
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
přežití	přežití	k1gNnSc1	přežití
nemocných	nemocný	k1gMnPc2	nemocný
pouze	pouze	k6eAd1	pouze
o	o	k7c4	o
2	[number]	k4	2
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
měsíců	měsíc	k1gInPc2	měsíc
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
účinek	účinek	k1gInSc1	účinek
je	být	k5eAaImIp3nS	být
omezen	omezit	k5eAaPmNgInS	omezit
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
bulbární	bulbární	k2eAgFnSc4d1	bulbární
formu	forma	k1gFnSc4	forma
onemocnění	onemocnění	k1gNnSc2	onemocnění
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výzkum	výzkum	k1gInSc1	výzkum
==	==	k?	==
</s>
</p>
<p>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
ukončené	ukončený	k2eAgFnSc2d1	ukončená
třetí	třetí	k4xOgFnSc2	třetí
fáze	fáze	k1gFnSc2	fáze
klinických	klinický	k2eAgFnPc2d1	klinická
zkoušek	zkouška	k1gFnPc2	zkouška
požádala	požádat	k5eAaPmAgFnS	požádat
japonská	japonský	k2eAgFnSc1d1	japonská
společnost	společnost	k1gFnSc1	společnost
Eisai	Eisai	k1gNnPc4	Eisai
Co	co	k9	co
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
Ltd	ltd	kA	ltd
<g/>
.	.	kIx.	.
o	o	k7c4	o
registraci	registrace	k1gFnSc4	registrace
vysokých	vysoký	k2eAgFnPc2d1	vysoká
dávek	dávka	k1gFnPc2	dávka
methylcobalaminu	methylcobalamin	k1gInSc2	methylcobalamin
(	(	kIx(	(
<g/>
50	[number]	k4	50
<g/>
mg	mg	kA	mg
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
x	x	k?	x
týdně	týdně	k6eAd1	týdně
<g/>
,	,	kIx,	,
do	do	k7c2	do
svalu	sval	k1gInSc2	sval
<g/>
)	)	kIx)	)
pod	pod	k7c7	pod
obchodním	obchodní	k2eAgInSc7d1	obchodní
názvem	název	k1gInSc7	název
E0302	E0302	k1gFnSc2	E0302
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2016	[number]	k4	2016
u	u	k7c2	u
Japonského	japonský	k2eAgInSc2d1	japonský
úřadu	úřad	k1gInSc2	úřad
<g />
.	.	kIx.	.
</s>
<s>
pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
léčiv	léčivo	k1gNnPc2	léčivo
jako	jako	k8xC	jako
léčiva	léčivo	k1gNnPc1	léčivo
pro	pro	k7c4	pro
ALS	ALS	kA	ALS
zvyšujícího	zvyšující	k2eAgNnSc2d1	zvyšující
přežití	přežití	k1gNnSc2	přežití
v	v	k7c6	v
mediánu	medián	k1gInSc6	medián
o	o	k7c4	o
600	[number]	k4	600
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
<g/>
Látka	látka	k1gFnSc1	látka
Edaravone	Edaravon	k1gInSc5	Edaravon
byla	být	k5eAaImAgNnP	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
registrována	registrován	k2eAgFnSc1d1	registrována
v	v	k7c6	v
Japonsku	Japonsko	k1gNnSc6	Japonsko
jako	jako	k8xC	jako
lék	lék	k1gInSc1	lék
se	se	k3xPyFc4	se
statisticky	statisticky	k6eAd1	statisticky
prokazatelným	prokazatelný	k2eAgNnSc7d1	prokazatelné
zpomalením	zpomalení	k1gNnSc7	zpomalení
progrese	progrese	k1gFnSc2	progrese
o	o	k7c4	o
30	[number]	k4	30
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
<g/>
Látka	látka	k1gFnSc1	látka
Masitinib	Masitiniba	k1gFnPc2	Masitiniba
ukončila	ukončit	k5eAaPmAgFnS	ukončit
fázi	fáze	k1gFnSc4	fáze
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
klinických	klinický	k2eAgFnPc2d1	klinická
zkoušek	zkouška	k1gFnPc2	zkouška
a	a	k8xC	a
v	v	k7c6	v
dávce	dávka	k1gFnSc6	dávka
4,5	[number]	k4	4,5
<g/>
mg	mg	kA	mg
<g/>
/	/	kIx~	/
<g/>
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
den	den	k1gInSc4	den
prokázala	prokázat	k5eAaPmAgFnS	prokázat
schopnost	schopnost	k1gFnSc1	schopnost
zpomalit	zpomalit	k5eAaPmF	zpomalit
progresi	progrese	k1gFnSc4	progrese
onemocnění	onemocnění	k1gNnSc2	onemocnění
o	o	k7c4	o
minimálně	minimálně	k6eAd1	minimálně
27	[number]	k4	27
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
začne	začít	k5eAaPmIp3nS	začít
podávat	podávat	k5eAaImF	podávat
ne	ne	k9	ne
později	pozdě	k6eAd2	pozdě
než	než	k8xS	než
22-24	[number]	k4	22-24
měsíců	měsíc	k1gInPc2	měsíc
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
onemocnění	onemocnění	k1gNnSc2	onemocnění
tak	tak	k8xC	tak
efekt	efekt	k1gInSc1	efekt
zpomalení	zpomalení	k1gNnSc2	zpomalení
je	být	k5eAaImIp3nS	být
minimálně	minimálně	k6eAd1	minimálně
42	[number]	k4	42
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
září	září	k1gNnSc2	září
2016	[number]	k4	2016
probíhá	probíhat	k5eAaImIp3nS	probíhat
první	první	k4xOgFnPc1	první
fáze	fáze	k1gFnPc1	fáze
klinických	klinický	k2eAgFnPc2d1	klinická
zkoušek	zkouška	k1gFnPc2	zkouška
na	na	k7c6	na
látce	látka	k1gFnSc6	látka
Cu-ATSM	Cu-ATSM	k1gFnSc2	Cu-ATSM
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
ukazuje	ukazovat	k5eAaImIp3nS	ukazovat
jako	jako	k9	jako
vhodný	vhodný	k2eAgMnSc1d1	vhodný
kandidát	kandidát	k1gMnSc1	kandidát
pro	pro	k7c4	pro
léčbu	léčba	k1gFnSc4	léčba
ALS	ALS	kA	ALS
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
podkladu	podklad	k1gInSc6	podklad
výzkumné	výzkumný	k2eAgFnSc2d1	výzkumná
práce	práce	k1gFnSc2	práce
<g />
.	.	kIx.	.
</s>
<s>
vědců	vědec	k1gMnPc2	vědec
z	z	k7c2	z
University	universita	k1gFnSc2	universita
of	of	k?	of
Tokyo	Tokyo	k6eAd1	Tokyo
je	být	k5eAaImIp3nS	být
látka	látka	k1gFnSc1	látka
Perampanel	Perampanel	k1gMnSc1	Perampanel
(	(	kIx(	(
<g/>
selektivní	selektivní	k2eAgMnSc1d1	selektivní
antagonista	antagonista	k1gMnSc1	antagonista
AMPA	AMPA	kA	AMPA
receptoru	receptor	k1gInSc3	receptor
<g/>
)	)	kIx)	)
kandidátem	kandidát	k1gMnSc7	kandidát
pro	pro	k7c4	pro
otestování	otestování	k1gNnSc4	otestování
v	v	k7c6	v
klinické	klinický	k2eAgFnSc6d1	klinická
studii	studie	k1gFnSc6	studie
na	na	k7c6	na
lidech	lid	k1gInPc6	lid
pro	pro	k7c4	pro
léčbu	léčba	k1gFnSc4	léčba
ALS	ALS	kA	ALS
<g/>
.	.	kIx.	.
<g/>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
testované	testovaný	k2eAgFnPc4d1	testovaná
terapie	terapie	k1gFnPc4	terapie
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
terapie	terapie	k1gFnSc1	terapie
kmenovými	kmenový	k2eAgFnPc7d1	kmenová
buňkami	buňka	k1gFnPc7	buňka
<g/>
;	;	kIx,	;
použití	použití	k1gNnSc4	použití
těchto	tento	k3xDgFnPc2	tento
terapií	terapie	k1gFnPc2	terapie
v	v	k7c6	v
ČR	ČR	kA	ČR
spadá	spadat	k5eAaImIp3nS	spadat
dle	dle	k7c2	dle
zákona	zákon	k1gInSc2	zákon
č.	č.	k?	č.
378	[number]	k4	378
<g/>
/	/	kIx~	/
<g/>
2007	[number]	k4	2007
Sb	sb	kA	sb
<g/>
.	.	kIx.	.
o	o	k7c6	o
léčivech	léčivo	k1gNnPc6	léčivo
pod	pod	k7c4	pod
užití	užití	k1gNnSc4	užití
tzv.	tzv.	kA	tzv.
neregistrovaných	registrovaný	k2eNgInPc2d1	neregistrovaný
léčivých	léčivý	k2eAgInPc2d1	léčivý
přípravků	přípravek	k1gInPc2	přípravek
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
u	u	k7c2	u
terapie	terapie	k1gFnSc2	terapie
kmenovými	kmenový	k2eAgFnPc7d1	kmenová
buňkami	buňka	k1gFnPc7	buňka
stále	stále	k6eAd1	stále
probíhají	probíhat	k5eAaImIp3nP	probíhat
klinické	klinický	k2eAgFnPc1d1	klinická
studie	studie	k1gFnPc1	studie
s	s	k7c7	s
cílem	cíl	k1gInSc7	cíl
určit	určit	k5eAaPmF	určit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
jejich	jejich	k3xOp3gFnSc1	jejich
aplikace	aplikace	k1gFnSc1	aplikace
skutečně	skutečně	k6eAd1	skutečně
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
zpomalení	zpomalení	k1gNnSc3	zpomalení
patologického	patologický	k2eAgInSc2d1	patologický
procesu	proces	k1gInSc2	proces
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
některé	některý	k3yIgFnPc1	některý
práce	práce	k1gFnPc1	práce
<g/>
,	,	kIx,	,
např.	např.	kA	např.
retrospektivní	retrospektivní	k2eAgFnSc1d1	retrospektivní
analýza	analýza	k1gFnSc1	analýza
několika	několik	k4yIc2	několik
případů	případ	k1gInPc2	případ
Alok	Aloka	k1gFnPc2	Aloka
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
vycházejí	vycházet	k5eAaImIp3nP	vycházet
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
aplikace	aplikace	k1gFnPc1	aplikace
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
medián	medián	k1gInSc1	medián
přežití	přežití	k1gNnSc2	přežití
<g/>
,	,	kIx,	,
sumarizační	sumarizační	k2eAgFnSc2d1	sumarizační
práce	práce	k1gFnSc2	práce
hodnotící	hodnotící	k2eAgInSc4d1	hodnotící
více	hodně	k6eAd2	hodně
dosud	dosud	k6eAd1	dosud
publikovaných	publikovaný	k2eAgFnPc2d1	publikovaná
studií	studie	k1gFnPc2	studie
efektivitu	efektivita	k1gFnSc4	efektivita
terapie	terapie	k1gFnSc2	terapie
nepotvrzují	potvrzovat	k5eNaImIp3nP	potvrzovat
.	.	kIx.	.
</s>
<s>
Motor	motor	k1gInSc1	motor
neuron	neuron	k1gInSc1	neuron
disease	disease	k6eAd1	disease
association	association	k1gInSc1	association
<g/>
,	,	kIx,	,
charitativní	charitativní	k2eAgFnSc1d1	charitativní
organizace	organizace	k1gFnSc1	organizace
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
,	,	kIx,	,
Walesu	Wales	k1gInSc6	Wales
a	a	k8xC	a
Severním	severní	k2eAgNnSc6d1	severní
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
zaměřena	zaměřit	k5eAaPmNgFnS	zaměřit
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
nemocných	nemocný	k1gMnPc2	nemocný
i	i	k8xC	i
na	na	k7c4	na
podporu	podpora	k1gFnSc4	podpora
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
,	,	kIx,	,
zastává	zastávat	k5eAaImIp3nS	zastávat
podobné	podobný	k2eAgNnSc1d1	podobné
stanovisko	stanovisko	k1gNnSc1	stanovisko
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
že	že	k8xS	že
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
nebyl	být	k5eNaImAgInS	být
podán	podat	k5eAaPmNgInS	podat
věrohodný	věrohodný	k2eAgInSc1d1	věrohodný
důkaz	důkaz	k1gInSc1	důkaz
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
byla	být	k5eAaImAgFnS	být
terapie	terapie	k1gFnSc1	terapie
kmenovými	kmenový	k2eAgInPc7d1	kmenový
buňkami	buňka	k1gFnPc7	buňka
efektivní	efektivní	k2eAgFnSc3d1	efektivní
v	v	k7c6	v
léčbě	léčba	k1gFnSc6	léčba
onemocnění	onemocnění	k1gNnSc2	onemocnění
motoneuronu	motoneuron	k1gInSc2	motoneuron
vč.	vč.	k?	vč.
amyotrofické	amyotrofický	k2eAgFnPc4d1	amyotrofická
laterální	laterální	k2eAgFnPc4d1	laterální
sklerózy	skleróza	k1gFnPc4	skleróza
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
fázi	fáze	k1gFnSc6	fáze
II	II	kA	II
klinických	klinický	k2eAgFnPc2d1	klinická
studií	studie	k1gFnPc2	studie
buněčná	buněčný	k2eAgFnSc1d1	buněčná
terapie	terapie	k1gFnSc1	terapie
od	od	k7c2	od
BrainStorm	BrainStorm	k1gInSc4	BrainStorm
Cell	cello	k1gNnPc2	cello
Therapeutics	Therapeuticsa	k1gFnPc2	Therapeuticsa
v	v	k7c4	v
Hadassah	Hadassah	k1gInSc4	Hadassah
Medical	Medical	k1gFnSc1	Medical
Center	centrum	k1gNnPc2	centrum
(	(	kIx(	(
<g/>
Izrael	Izrael	k1gInSc1	Izrael
<g/>
)	)	kIx)	)
s	s	k7c7	s
předběžným	předběžný	k2eAgInSc7d1	předběžný
výsledkem	výsledek	k1gInSc7	výsledek
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
"	"	kIx"	"
<g/>
ukázala	ukázat	k5eAaPmAgFnS	ukázat
tendenci	tendence	k1gFnSc4	tendence
ke	k	k7c3	k
stabilizaci	stabilizace	k1gFnSc3	stabilizace
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
parametrech	parametr	k1gInPc6	parametr
v	v	k7c6	v
ALS	ALS	kA	ALS
FRS	FRS	kA	FRS
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Započetí	započetí	k1gNnSc4	započetí
druhé	druhý	k4xOgFnSc2	druhý
fáze	fáze	k1gFnSc2	fáze
se	se	k3xPyFc4	se
očekává	očekávat	k5eAaImIp3nS	očekávat
také	také	k9	také
v	v	k7c6	v
několika	několik	k4yIc6	několik
institucích	instituce	k1gFnPc6	instituce
USA	USA	kA	USA
včetně	včetně	k7c2	včetně
Mayo	Mayo	k6eAd1	Mayo
Clinic	Clinice	k1gFnPc2	Clinice
<g/>
.	.	kIx.	.
<g/>
Několik	několik	k4yIc4	několik
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
studií	studie	k1gFnPc2	studie
našlo	najít	k5eAaPmAgNnS	najít
statistické	statistický	k2eAgFnPc4d1	statistická
korelace	korelace	k1gFnPc4	korelace
mezi	mezi	k7c7	mezi
ALS	ALS	kA	ALS
a	a	k8xC	a
některými	některý	k3yIgInPc7	některý
zemědělskými	zemědělský	k2eAgInPc7d1	zemědělský
pesticidy	pesticid	k1gInPc7	pesticid
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Protože	protože	k8xS	protože
je	být	k5eAaImIp3nS	být
však	však	k9	však
pacientů	pacient	k1gMnPc2	pacient
poměrně	poměrně	k6eAd1	poměrně
málo	málo	k6eAd1	málo
a	a	k8xC	a
diagnostika	diagnostika	k1gFnSc1	diagnostika
se	se	k3xPyFc4	se
zpřesňuje	zpřesňovat	k5eAaImIp3nS	zpřesňovat
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
problémem	problém	k1gInSc7	problém
<g/>
,	,	kIx,	,
kolik	kolik	k4yIc1	kolik
pacientů	pacient	k1gMnPc2	pacient
bylo	být	k5eAaImAgNnS	být
vůbec	vůbec	k9	vůbec
vyšetřeno	vyšetřen	k2eAgNnSc1d1	vyšetřeno
<g/>
.	.	kIx.	.
</s>
<s>
Sama	sám	k3xTgFnSc1	sám
vzácnost	vzácnost	k1gFnSc1	vzácnost
onemocnění	onemocnění	k1gNnPc2	onemocnění
pak	pak	k6eAd1	pak
nahrává	nahrávat	k5eAaImIp3nS	nahrávat
statisticky	statisticky	k6eAd1	statisticky
průkazným	průkazný	k2eAgFnPc3d1	průkazná
asociacím	asociace	k1gFnPc3	asociace
s	s	k7c7	s
faktory	faktor	k1gInPc7	faktor
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc4	jejichž
relevance	relevance	k1gFnSc1	relevance
je	být	k5eAaImIp3nS	být
sporná	sporný	k2eAgFnSc1d1	sporná
<g/>
.	.	kIx.	.
</s>
<s>
Rizikovými	rizikový	k2eAgInPc7d1	rizikový
faktory	faktor	k1gInPc7	faktor
pro	pro	k7c4	pro
rozvoj	rozvoj	k1gInSc4	rozvoj
ALS	ALS	kA	ALS
jsou	být	k5eAaImIp3nP	být
např.	např.	kA	např.
aktivní	aktivní	k2eAgFnSc1d1	aktivní
služba	služba	k1gFnSc1	služba
v	v	k7c6	v
armádě	armáda	k1gFnSc6	armáda
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
<g/>
,	,	kIx,	,
povolání	povolání	k1gNnSc2	povolání
profesionálního	profesionální	k2eAgMnSc2d1	profesionální
fotbalisty	fotbalista	k1gMnSc2	fotbalista
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
zejm.	zejm.	k?	zejm.
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
záložníka	záložník	k1gMnSc2	záložník
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zapojení	zapojení	k1gNnSc1	zapojení
se	se	k3xPyFc4	se
do	do	k7c2	do
amatérských	amatérský	k2eAgFnPc2d1	amatérská
fotbalových	fotbalový	k2eAgFnPc2d1	fotbalová
soutěží	soutěž	k1gFnPc2	soutěž
v	v	k7c6	v
Anglii	Anglie	k1gFnSc6	Anglie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Medializace	medializace	k1gFnSc1	medializace
onemocnění	onemocnění	k1gNnSc2	onemocnění
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
na	na	k7c6	na
internetu	internet	k1gInSc6	internet
velmi	velmi	k6eAd1	velmi
masivní	masivní	k2eAgFnSc1d1	masivní
kbelíková	kbelíkový	k2eAgFnSc1d1	kbelíková
kampaň	kampaň	k1gFnSc1	kampaň
Ice	Ice	k1gFnSc2	Ice
Bucket	Bucketa	k1gFnPc2	Bucketa
Challenge	Challenge	k1gNnSc7	Challenge
za	za	k7c4	za
osvětu	osvěta	k1gFnSc4	osvěta
a	a	k8xC	a
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
tomuto	tento	k3xDgNnSc3	tento
neurodegenerativnímu	urodegenerativní	k2eNgNnSc3d1	neurodegenerativní
onemocnění	onemocnění	k1gNnSc3	onemocnění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
uvedla	uvést	k5eAaPmAgFnS	uvést
reportáž	reportáž	k1gFnSc4	reportáž
Obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
nadějí	naděje	k1gFnSc7	naděje
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Reportéři	reportér	k1gMnPc1	reportér
ČT	ČT	kA	ČT
ohledně	ohledně	k7c2	ohledně
aplikace	aplikace	k1gFnSc2	aplikace
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
mimo	mimo	k7c4	mimo
schválenou	schválený	k2eAgFnSc4d1	schválená
klinickou	klinický	k2eAgFnSc4d1	klinická
studii	studie	k1gFnSc4	studie
AMSC-ALS-	AMSC-ALS-	k1gFnSc2	AMSC-ALS-
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vyjádření	vyjádření	k1gNnSc2	vyjádření
rady	rada	k1gFnSc2	rada
AV	AV	kA	AV
nebylo	být	k5eNaImAgNnS	být
podávání	podávání	k1gNnSc1	podávání
kmenových	kmenový	k2eAgFnPc2d1	kmenová
buněk	buňka	k1gFnPc2	buňka
pacientům	pacient	k1gMnPc3	pacient
mimo	mimo	k7c4	mimo
klinickou	klinický	k2eAgFnSc4d1	klinická
studii	studie	k1gFnSc4	studie
dostatečně	dostatečně	k6eAd1	dostatečně
odůvodněno	odůvodněn	k2eAgNnSc1d1	odůvodněno
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
nekriticky	kriticky	k6eNd1	kriticky
prezentováno	prezentovat	k5eAaBmNgNnS	prezentovat
na	na	k7c6	na
webových	webový	k2eAgFnPc6d1	webová
stránkách	stránka	k1gFnPc6	stránka
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vyjádření	vyjádření	k1gNnSc2	vyjádření
UEM	UEM	kA	UEM
výsledky	výsledek	k1gInPc4	výsledek
klinické	klinický	k2eAgFnSc2d1	klinická
studie	studie	k1gFnSc2	studie
FN	FN	kA	FN
Motol	Motol	k1gInSc4	Motol
prokázaly	prokázat	k5eAaPmAgFnP	prokázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
u	u	k7c2	u
téměř	téměř	k6eAd1	téměř
70	[number]	k4	70
%	%	kIx~	%
pacientů	pacient	k1gMnPc2	pacient
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
statisticky	statisticky	k6eAd1	statisticky
významnému	významný	k2eAgNnSc3d1	významné
zpomalení	zpomalení	k1gNnSc3	zpomalení
progrese	progrese	k1gFnSc2	progrese
neurologického	urologický	k2eNgInSc2d1	urologický
deficitu	deficit	k1gInSc2	deficit
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
přetrvávalo	přetrvávat	k5eAaImAgNnS	přetrvávat
3	[number]	k4	3
až	až	k8xS	až
12	[number]	k4	12
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
z	z	k7c2	z
těchto	tento	k3xDgMnPc2	tento
pacientů	pacient	k1gMnPc2	pacient
vykazovala	vykazovat	k5eAaImAgFnS	vykazovat
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
podání	podání	k1gNnSc6	podání
KB	kb	kA	kb
stabilní	stabilní	k2eAgFnSc2d1	stabilní
hodnoty	hodnota	k1gFnSc2	hodnota
v	v	k7c6	v
testech	test	k1gInPc6	test
svalové	svalový	k2eAgFnSc2d1	svalová
síly	síla	k1gFnSc2	síla
končetin	končetina	k1gFnPc2	končetina
a	a	k8xC	a
nedošlo	dojít	k5eNaPmAgNnS	dojít
u	u	k7c2	u
nich	on	k3xPp3gMnPc2	on
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
12	[number]	k4	12
měsíců	měsíc	k1gInPc2	měsíc
k	k	k7c3	k
významnému	významný	k2eAgNnSc3d1	významné
zhoršení	zhoršení	k1gNnSc3	zhoršení
ventilačních	ventilační	k2eAgInPc2d1	ventilační
parametrů	parametr	k1gInPc2	parametr
(	(	kIx(	(
<g/>
FVC	FVC	kA	FVC
<g/>
>	>	kIx)	>
<g/>
65	[number]	k4	65
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
několika	několik	k4yIc2	několik
pacientů	pacient	k1gMnPc2	pacient
došlo	dojít	k5eAaPmAgNnS	dojít
dokonce	dokonce	k9	dokonce
ke	k	k7c3	k
zlepšení	zlepšení	k1gNnSc3	zlepšení
některých	některý	k3yIgFnPc2	některý
testovaných	testovaný	k2eAgFnPc2d1	testovaná
funkcí	funkce	k1gFnPc2	funkce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Etymologie	etymologie	k1gFnSc2	etymologie
==	==	k?	==
</s>
</p>
<p>
<s>
Název	název	k1gInSc1	název
nemoci	nemoc	k1gFnSc2	nemoc
pochází	pocházet	k5eAaImIp3nS	pocházet
od	od	k7c2	od
řeckých	řecký	k2eAgNnPc2d1	řecké
slov	slovo	k1gNnPc2	slovo
amyotrophia	amyotrophia	k1gFnSc1	amyotrophia
<g/>
:	:	kIx,	:
a-	a-	k?	a-
"	"	kIx"	"
<g/>
ne	ne	k9	ne
<g/>
/	/	kIx~	/
<g/>
bez	bez	k7c2	bez
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
myo	myo	k?	myo
"	"	kIx"	"
<g/>
sval	sval	k1gInSc1	sval
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
and	and	k?	and
trophia	trophia	k1gFnSc1	trophia
"	"	kIx"	"
<g/>
výživa	výživa	k1gFnSc1	výživa
<g/>
"	"	kIx"	"
<g/>
;	;	kIx,	;
Lateralní	Lateralný	k2eAgMnPc1d1	Lateralný
(	(	kIx(	(
<g/>
boční	boční	k2eAgNnSc1d1	boční
<g/>
,	,	kIx,	,
postranní	postranní	k2eAgFnSc1d1	postranní
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
k	k	k7c3	k
umístění	umístění	k1gNnSc3	umístění
postižených	postižený	k2eAgInPc2d1	postižený
nervů	nerv	k1gInPc2	nerv
<g/>
.	.	kIx.	.
</s>
<s>
Nemoc	nemoc	k1gFnSc1	nemoc
vede	vést	k5eAaImIp3nS	vést
ke	k	k7c3	k
ztvrdnutí	ztvrdnutí	k1gNnSc3	ztvrdnutí
tkáně	tkáň	k1gFnSc2	tkáň
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
skleróza	skleróza	k1gFnSc1	skleróza
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
také	také	k9	také
jinak	jinak	k6eAd1	jinak
obecnějšího	obecní	k2eAgInSc2d2	obecní
názvu	název	k1gInSc2	název
"	"	kIx"	"
<g/>
motor	motor	k1gInSc1	motor
neurone	neuron	k1gInSc5	neuron
disease	diseasa	k1gFnSc6	diseasa
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
MND	MND	kA	MND
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Amyotrophic	Amyotrophic	k1gMnSc1	Amyotrophic
lateral	laterat	k5eAaPmAgMnS	laterat
sclerosis	sclerosis	k1gFnSc4	sclerosis
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Degenerativní	degenerativní	k2eAgFnSc1d1	degenerativní
myelopatie	myelopatie	k1gFnSc1	myelopatie
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
amyotrofická	amyotrofický	k2eAgFnSc1d1	amyotrofická
laterální	laterální	k2eAgFnSc1d1	laterální
skleróza	skleróza	k1gFnSc1	skleróza
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Info	Info	k6eAd1	Info
na	na	k7c6	na
stránkách	stránka	k1gFnPc6	stránka
ereska	eresk	k1gInSc2	eresk
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
ALS	ALS	kA	ALS
Association	Association	k1gInSc1	Association
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
MedlinePlus	MedlinePlus	k1gMnSc1	MedlinePlus
–	–	k?	–
Amyotrophic	Amyotrophic	k1gMnSc1	Amyotrophic
Lateral	Lateral	k1gMnSc2	Lateral
Sclerosis	Sclerosis	k1gFnSc2	Sclerosis
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
National	National	k1gFnSc1	National
Institute	institut	k1gInSc5	institut
of	of	k?	of
Neurological	Neurological	k1gFnSc3	Neurological
Disorders	Disordersa	k1gFnPc2	Disordersa
and	and	k?	and
Stroke	Stroke	k1gNnSc4	Stroke
(	(	kIx(	(
<g/>
NINDS	NINDS	kA	NINDS
<g/>
)	)	kIx)	)
–	–	k?	–
Amyotrophic	Amyotrophic	k1gMnSc1	Amyotrophic
Lateral	Lateral	k1gMnSc1	Lateral
Sclerosis	Sclerosis	k1gFnPc2	Sclerosis
Fact	Fact	k1gMnSc1	Fact
Sheet	Sheet	k1gMnSc1	Sheet
</s>
</p>
