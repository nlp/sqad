<s>
Název	název	k1gInSc1	název
Lou	Lou	k1gFnSc2	Lou
Gehrigova	Gehrigův	k2eAgFnSc1d1	Gehrigova
choroba	choroba	k1gFnSc1	choroba
toto	tento	k3xDgNnSc4	tento
onemocnění	onemocnění	k1gNnSc1	onemocnění
získalo	získat	k5eAaPmAgNnS	získat
po	po	k7c6	po
slavném	slavný	k2eAgMnSc6d1	slavný
baseballovém	baseballový	k2eAgMnSc6d1	baseballový
hráči	hráč	k1gMnSc6	hráč
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
Yankees	Yankees	k1gInSc1	Yankees
<g/>
,	,	kIx,	,
Lou	Lou	k1gMnSc3	Lou
Gehrigovi	Gehrig	k1gMnSc3	Gehrig
<g/>
,	,	kIx,	,
u	u	k7c2	u
něhož	jenž	k3xRgInSc2	jenž
byla	být	k5eAaImAgFnS	být
ALS	ALS	kA	ALS
diagnostikována	diagnostikovat	k5eAaBmNgNnP	diagnostikovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
<g/>
.	.	kIx.	.
</s>
