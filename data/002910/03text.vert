<s>
Mimika	mimika	k1gFnSc1	mimika
(	(	kIx(	(
<g/>
z	z	k7c2	z
řeckého	řecký	k2eAgNnSc2d1	řecké
mimeomai	mimeomai	k1gNnSc2	mimeomai
<g/>
,	,	kIx,	,
napodobovat	napodobovat	k5eAaImF	napodobovat
<g/>
,	,	kIx,	,
představovat	představovat	k5eAaImF	představovat
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
vědomé	vědomý	k2eAgNnSc1d1	vědomé
vyjadřování	vyjadřování	k1gNnSc1	vyjadřování
výrazem	výraz	k1gInSc7	výraz
tváře	tvář	k1gFnPc1	tvář
<g/>
,	,	kIx,	,
způsobené	způsobený	k2eAgInPc1d1	způsobený
stahy	stah	k1gInPc1	stah
obličejových	obličejový	k2eAgInPc2d1	obličejový
svalů	sval	k1gInPc2	sval
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
-	-	kIx~	-
vedle	vedle	k7c2	vedle
gestikulace	gestikulace	k1gFnSc2	gestikulace
-	-	kIx~	-
důležitou	důležitý	k2eAgFnSc7d1	důležitá
složkou	složka	k1gFnSc7	složka
nonverbální	nonverbální	k2eAgFnSc2d1	nonverbální
komunikace	komunikace	k1gFnSc2	komunikace
<g/>
,	,	kIx,	,
hereckého	herecký	k2eAgNnSc2d1	herecké
umění	umění	k1gNnSc2	umění
<g/>
,	,	kIx,	,
pantomimy	pantomima	k1gFnSc2	pantomima
i	i	k8xC	i
živého	živý	k2eAgNnSc2d1	živé
vyjadřování	vyjadřování	k1gNnSc2	vyjadřování
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
úzce	úzko	k6eAd1	úzko
spjata	spjat	k2eAgFnSc1d1	spjata
s	s	k7c7	s
očním	oční	k2eAgInSc7d1	oční
kontaktem	kontakt	k1gInSc7	kontakt
<g/>
.	.	kIx.	.
</s>
<s>
Mimické	mimický	k2eAgFnSc2d1	mimická
schopnosti	schopnost	k1gFnSc2	schopnost
člověka	člověk	k1gMnSc2	člověk
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
jinými	jiný	k2eAgMnPc7d1	jiný
živočichy	živočich	k1gMnPc7	živočich
silně	silně	k6eAd1	silně
podporuje	podporovat	k5eAaImIp3nS	podporovat
plochý	plochý	k2eAgInSc4d1	plochý
obličej	obličej	k1gInSc4	obličej
bez	bez	k7c2	bez
srsti	srst	k1gFnSc2	srst
i	i	k8xC	i
bohatá	bohatý	k2eAgFnSc1d1	bohatá
muskulatura	muskulatura	k1gFnSc1	muskulatura
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
výrazu	výraz	k1gInSc6	výraz
tváře	tvář	k1gFnSc2	tvář
se	se	k3xPyFc4	se
nejvíce	hodně	k6eAd3	hodně
podílejí	podílet	k5eAaImIp3nP	podílet
oči	oko	k1gNnPc4	oko
a	a	k8xC	a
ústa	ústa	k1gNnPc4	ústa
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
si	se	k3xPyFc3	se
je	on	k3xPp3gInPc4	on
herci	herec	k1gMnPc1	herec
zvýrazňují	zvýrazňovat	k5eAaImIp3nP	zvýrazňovat
maskováním	maskování	k1gNnSc7	maskování
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
svraštěné	svraštěný	k2eAgNnSc1d1	svraštěné
čelo	čelo	k1gNnSc1	čelo
<g/>
,	,	kIx,	,
přimhouřené	přimhouřený	k2eAgNnSc1d1	přimhouřené
oko	oko	k1gNnSc1	oko
<g/>
,	,	kIx,	,
ohrnutý	ohrnutý	k2eAgInSc4d1	ohrnutý
ret	ret	k1gInSc4	ret
nebo	nebo	k8xC	nebo
nos	nos	k1gInSc4	nos
jsou	být	k5eAaImIp3nP	být
jednoduché	jednoduchý	k2eAgInPc1d1	jednoduchý
prostředky	prostředek	k1gInPc1	prostředek
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgFnPc7	jenž
rychle	rychle	k6eAd1	rychle
a	a	k8xC	a
úsporně	úsporně	k6eAd1	úsporně
vyjadřujeme	vyjadřovat	k5eAaImIp1nP	vyjadřovat
své	svůj	k3xOyFgInPc4	svůj
soudy	soud	k1gInPc4	soud
a	a	k8xC	a
hodnocení	hodnocení	k1gNnPc4	hodnocení
<g/>
.	.	kIx.	.
</s>
<s>
Mimické	mimický	k2eAgInPc1d1	mimický
výrazy	výraz	k1gInPc1	výraz
jsou	být	k5eAaImIp3nP	být
sice	sice	k8xC	sice
individuálně	individuálně	k6eAd1	individuálně
proměnlivé	proměnlivý	k2eAgNnSc1d1	proměnlivé
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
mohou	moct	k5eAaImIp3nP	moct
sloužit	sloužit	k5eAaImF	sloužit
k	k	k7c3	k
dorozumívání	dorozumívání	k1gNnSc3	dorozumívání
i	i	k9	i
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
si	se	k3xPyFc3	se
lidé	člověk	k1gMnPc1	člověk
jazykově	jazykově	k6eAd1	jazykově
nerozumějí	rozumět	k5eNaImIp3nP	rozumět
<g/>
.	.	kIx.	.
</s>
<s>
Nejsou	být	k5eNaImIp3nP	být
však	však	k9	však
univerzální	univerzální	k2eAgInPc1d1	univerzální
<g/>
.	.	kIx.	.
</s>
<s>
Lidskou	lidský	k2eAgFnSc7d1	lidská
mimikou	mimika	k1gFnSc7	mimika
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
primáty	primát	k1gMnPc7	primát
se	se	k3xPyFc4	se
důkladně	důkladně	k6eAd1	důkladně
zabýval	zabývat	k5eAaImAgMnS	zabývat
Charles	Charles	k1gMnSc1	Charles
Darwin	Darwin	k1gMnSc1	Darwin
(	(	kIx(	(
<g/>
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
knize	kniha	k1gFnSc6	kniha
Vyjádření	vyjádření	k1gNnSc1	vyjádření
emocí	emoce	k1gFnPc2	emoce
u	u	k7c2	u
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
u	u	k7c2	u
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
The	The	k1gFnSc1	The
Expression	Expression	k1gInSc1	Expression
of	of	k?	of
the	the	k?	the
Emotions	Emotionsa	k1gFnPc2	Emotionsa
in	in	k?	in
Man	Man	k1gMnSc1	Man
and	and	k?	and
Animals	Animals	k1gInSc1	Animals
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pokusy	pokus	k1gInPc1	pokus
s	s	k7c7	s
mimikou	mimika	k1gFnSc7	mimika
dělal	dělat	k5eAaImAgMnS	dělat
také	také	k6eAd1	také
Jan	Jan	k1gMnSc1	Jan
Evangelista	evangelista	k1gMnSc1	evangelista
Purkyně	Purkyně	k1gFnSc1	Purkyně
<g/>
.	.	kIx.	.
</s>
<s>
M.	M.	kA	M.
L.	L.	kA	L.
Knapp	Knapp	k1gMnSc1	Knapp
definoval	definovat	k5eAaBmAgInS	definovat
výraz	výraz	k1gInSc4	výraz
tváře	tvář	k1gFnSc2	tvář
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Lidská	lidský	k2eAgFnSc1d1	lidská
tvář	tvář	k1gFnSc1	tvář
má	mít	k5eAaImIp3nS	mít
velice	velice	k6eAd1	velice
bohatý	bohatý	k2eAgInSc4d1	bohatý
komunikační	komunikační	k2eAgInSc4d1	komunikační
potenciál	potenciál	k1gInSc4	potenciál
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
prvořadě	prvořadě	k6eAd1	prvořadě
důležitým	důležitý	k2eAgInSc7d1	důležitý
sdělovačem	sdělovač	k1gInSc7	sdělovač
emocionálních	emocionální	k2eAgInPc2d1	emocionální
stavů	stav	k1gInPc2	stav
<g/>
.	.	kIx.	.
</s>
<s>
Odráží	odrážet	k5eAaImIp3nP	odrážet
vzájemné	vzájemný	k2eAgInPc1d1	vzájemný
postoje	postoj	k1gInPc1	postoj
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
spolu	spolu	k6eAd1	spolu
jednají	jednat	k5eAaImIp3nP	jednat
<g/>
,	,	kIx,	,
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
zpětnou	zpětný	k2eAgFnSc4d1	zpětná
vazbu	vazba	k1gFnSc4	vazba
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
odpověď	odpověď	k1gFnSc4	odpověď
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
jsem	být	k5eAaImIp1nS	být
druhému	druhý	k4xOgMnSc3	druhý
člověku	člověk	k1gMnSc3	člověk
řekli	říct	k5eAaPmAgMnP	říct
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
badatelé	badatel	k1gMnPc1	badatel
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
domnívají	domnívat	k5eAaImIp3nP	domnívat
<g/>
,	,	kIx,	,
že	že	k8xS	že
z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
sociální	sociální	k2eAgFnSc2d1	sociální
komunikace	komunikace	k1gFnSc2	komunikace
je	být	k5eAaImIp3nS	být
tvář	tvář	k1gFnSc4	tvář
vedle	vedle	k7c2	vedle
slova	slovo	k1gNnSc2	slovo
druhým	druhý	k4xOgInSc7	druhý
nejdůležitějším	důležitý	k2eAgInSc7d3	nejdůležitější
sdělovacím	sdělovací	k2eAgInSc7d1	sdělovací
prostředkem	prostředek	k1gInSc7	prostředek
v	v	k7c6	v
mezilidském	mezilidský	k2eAgInSc6d1	mezilidský
styku	styk	k1gInSc6	styk
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Paul	Paul	k1gMnSc1	Paul
Ekman	Ekman	k1gMnSc1	Ekman
napsal	napsat	k5eAaBmAgMnS	napsat
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
I	i	k9	i
když	když	k8xS	když
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
našem	náš	k3xOp1gInSc6	náš
jazyce	jazyk	k1gInSc6	jazyk
poměrně	poměrně	k6eAd1	poměrně
omezený	omezený	k2eAgInSc1d1	omezený
počet	počet	k1gInSc1	počet
slovních	slovní	k2eAgInPc2d1	slovní
výrazů	výraz	k1gInPc2	výraz
pro	pro	k7c4	pro
popis	popis	k1gInSc4	popis
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
děje	dít	k5eAaImIp3nS	dít
v	v	k7c6	v
lidské	lidský	k2eAgFnSc6d1	lidská
tváři	tvář	k1gFnSc6	tvář
(	(	kIx(	(
<g/>
např.	např.	kA	např.
úsměv	úsměv	k1gInSc1	úsměv
<g/>
,	,	kIx,	,
výsměch	výsměch	k1gInSc1	výsměch
<g/>
,	,	kIx,	,
zakabonění	zakabonění	k1gNnSc1	zakabonění
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
přece	přece	k9	přece
jen	jen	k9	jen
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
konstatovat	konstatovat	k5eAaBmF	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
svaly	sval	k1gInPc1	sval
v	v	k7c6	v
našem	náš	k3xOp1gInSc6	náš
obličeji	obličej	k1gInSc6	obličej
jsou	být	k5eAaImIp3nP	být
natolik	natolik	k6eAd1	natolik
složitým	složitý	k2eAgNnSc7d1	složité
předivem	předivo	k1gNnSc7	předivo
<g/>
,	,	kIx,	,
že	že	k8xS	že
nám	my	k3xPp1nPc3	my
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1000	[number]	k4	1000
různých	různý	k2eAgInPc2d1	různý
výrazů	výraz	k1gInPc2	výraz
obličeje	obličej	k1gInSc2	obličej
<g/>
.	.	kIx.	.
</s>
<s>
Činnost	činnost	k1gFnSc1	činnost
tohoto	tento	k3xDgNnSc2	tento
přediva	předivo	k1gNnSc2	předivo
svalů	sval	k1gInPc2	sval
v	v	k7c6	v
našem	náš	k3xOp1gInSc6	náš
obličeji	obličej	k1gInSc6	obličej
je	být	k5eAaImIp3nS	být
přitom	přitom	k6eAd1	přitom
tak	tak	k6eAd1	tak
hbitá	hbitý	k2eAgFnSc1d1	hbitá
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgInPc1	tento
rozmanité	rozmanitý	k2eAgInPc1d1	rozmanitý
výrazy	výraz	k1gInPc1	výraz
mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
produkovány	produkovat	k5eAaImNgInP	produkovat
v	v	k7c6	v
minimálním	minimální	k2eAgInSc6d1	minimální
čase	čas	k1gInSc6	čas
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Sociální	sociální	k2eAgMnPc1d1	sociální
psycholog	psycholog	k1gMnSc1	psycholog
Paul	Paul	k1gMnSc1	Paul
Ekman	Ekman	k1gMnSc1	Ekman
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgInSc7	svůj
týmem	tým	k1gInSc7	tým
provedl	provést	k5eAaPmAgInS	provést
výzkum	výzkum	k1gInSc4	výzkum
a	a	k8xC	a
identifikoval	identifikovat	k5eAaBmAgMnS	identifikovat
7	[number]	k4	7
tzv.	tzv.	kA	tzv.
primárních	primární	k2eAgFnPc2d1	primární
emocí	emoce	k1gFnPc2	emoce
ve	v	k7c6	v
výrazech	výraz	k1gInPc6	výraz
obličeje	obličej	k1gInSc2	obličej
<g/>
:	:	kIx,	:
štěstí	štěstí	k1gNnSc4	štěstí
-	-	kIx~	-
neštěstí	štěstit	k5eNaImIp3nS	štěstit
neočekávané	očekávaný	k2eNgNnSc4d1	neočekávané
překvapení	překvapení	k1gNnSc4	překvapení
-	-	kIx~	-
splněné	splněný	k2eAgNnSc4d1	splněné
očekávání	očekávání	k1gNnSc4	očekávání
strach	strach	k1gInSc1	strach
a	a	k8xC	a
bázeň	bázeň	k1gFnSc1	bázeň
-	-	kIx~	-
pocit	pocit	k1gInSc1	pocit
jistoty	jistota	k1gFnSc2	jistota
radost	radost	k1gFnSc1	radost
-	-	kIx~	-
smutek	smutek	k1gInSc1	smutek
klid	klid	k1gInSc1	klid
-	-	kIx~	-
rozčilení	rozčilení	k1gNnSc1	rozčilení
spokojenost	spokojenost	k1gFnSc1	spokojenost
-	-	kIx~	-
nespokojenost	nespokojenost	k1gFnSc1	nespokojenost
až	až	k8xS	až
znechucení	znechucení	k1gNnSc1	znechucení
zájem	zájem	k1gInSc1	zájem
-	-	kIx~	-
nezájem	nezájem	k1gInSc1	nezájem
Mimo	mimo	k7c4	mimo
tyto	tento	k3xDgFnPc4	tento
primární	primární	k2eAgFnPc4d1	primární
emoce	emoce	k1gFnPc4	emoce
existují	existovat	k5eAaImIp3nP	existovat
i	i	k9	i
emoce	emoce	k1gFnPc1	emoce
sekundární	sekundární	k2eAgFnPc1d1	sekundární
<g/>
,	,	kIx,	,
odvozené	odvozený	k2eAgInPc1d1	odvozený
výrazy	výraz	k1gInPc1	výraz
obličeje	obličej	k1gInSc2	obličej
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obličeji	obličej	k1gInSc6	obličej
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
zrcadlit	zrcadlit	k5eAaImF	zrcadlit
i	i	k9	i
několik	několik	k4yIc4	několik
emocí	emoce	k1gFnPc2	emoce
naráz	naráz	k6eAd1	naráz
(	(	kIx(	(
<g/>
např.	např.	kA	např.
úžas	úžas	k1gInSc4	úžas
a	a	k8xC	a
radost	radost	k1gFnSc4	radost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obličej	obličej	k1gInSc1	obličej
lze	lze	k6eAd1	lze
považovat	považovat	k5eAaImF	považovat
za	za	k7c4	za
prvotní	prvotní	k2eAgInSc4d1	prvotní
sdělovač	sdělovač	k1gInSc4	sdělovač
emocí	emoce	k1gFnPc2	emoce
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
emoce	emoce	k1gFnPc1	emoce
(	(	kIx(	(
<g/>
např.	např.	kA	např.
strach	strach	k1gInSc1	strach
<g/>
,	,	kIx,	,
smutek	smutek	k1gInSc1	smutek
<g/>
,	,	kIx,	,
štěstí	štěstí	k1gNnSc1	štěstí
<g/>
,	,	kIx,	,
překvapení	překvapení	k1gNnSc1	překvapení
<g/>
)	)	kIx)	)
dokáže	dokázat	k5eAaPmIp3nS	dokázat
člověk	člověk	k1gMnSc1	člověk
určit	určit	k5eAaPmF	určit
mimořádně	mimořádně	k6eAd1	mimořádně
přesně	přesně	k6eAd1	přesně
<g/>
,	,	kIx,	,
schopnost	schopnost	k1gFnSc1	schopnost
správně	správně	k6eAd1	správně
identifikovat	identifikovat	k5eAaBmF	identifikovat
emoci	emoce	k1gFnSc4	emoce
z	z	k7c2	z
tváře	tvář	k1gFnSc2	tvář
nebo	nebo	k8xC	nebo
očí	oko	k1gNnPc2	oko
mají	mít	k5eAaImIp3nP	mít
někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
(	(	kIx(	(
<g/>
např.	např.	kA	např.
introverti	introvert	k1gMnPc1	introvert
<g/>
)	)	kIx)	)
navíc	navíc	k6eAd1	navíc
výrazně	výrazně	k6eAd1	výrazně
posílenu	posílen	k2eAgFnSc4d1	posílena
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc1	dva
z	z	k7c2	z
nejvýraznějších	výrazný	k2eAgInPc2d3	nejvýraznější
mimických	mimický	k2eAgInPc2d1	mimický
projevů	projev	k1gInPc2	projev
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
pláč	pláč	k1gInSc1	pláč
smích	smích	k1gInSc1	smích
</s>
