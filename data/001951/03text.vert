<s>
Empire	empir	k1gInSc5	empir
State	status	k1gInSc5	status
Building	Building	k1gInSc1	Building
je	být	k5eAaImIp3nS	být
102	[number]	k4	102
patrová	patrový	k2eAgFnSc1d1	patrová
budova	budova	k1gFnSc1	budova
postavená	postavený	k2eAgFnSc1d1	postavená
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
art	art	k?	art
deco	deco	k1gMnSc1	deco
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
v	v	k7c6	v
USA	USA	kA	USA
na	na	k7c6	na
křižovatce	křižovatka	k1gFnSc6	křižovatka
Páté	pátá	k1gFnSc2	pátá
Avenue	avenue	k1gFnSc2	avenue
a	a	k8xC	a
West	West	k1gInSc1	West
34	[number]	k4	34
<g/>
th	th	k?	th
Street	Street	k1gMnSc1	Street
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
dokončení	dokončení	k1gNnSc6	dokončení
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1931	[number]	k4	1931
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
na	na	k7c4	na
více	hodně	k6eAd2	hodně
než	než	k8xS	než
40	[number]	k4	40
let	léto	k1gNnPc2	léto
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
budovou	budova	k1gFnSc7	budova
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1972	[number]	k4	1972
ji	on	k3xPp3gFnSc4	on
překonala	překonat	k5eAaPmAgFnS	překonat
severní	severní	k2eAgFnSc1d1	severní
věž	věž	k1gFnSc1	věž
Světového	světový	k2eAgNnSc2d1	světové
obchodního	obchodní	k2eAgNnSc2d1	obchodní
centra	centrum	k1gNnSc2	centrum
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
zřícení	zřícení	k1gNnSc6	zřícení
budov	budova	k1gFnPc2	budova
při	při	k7c6	při
teroristických	teroristický	k2eAgInPc6d1	teroristický
útocích	útok	k1gInPc6	útok
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2001	[number]	k4	2001
se	se	k3xPyFc4	se
Empire	empir	k1gInSc5	empir
State	status	k1gInSc5	status
Building	Building	k1gInSc4	Building
stala	stát	k5eAaPmAgFnS	stát
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
budovou	budova	k1gFnSc7	budova
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
třetí	třetí	k4xOgFnSc1	třetí
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
v	v	k7c6	v
USA	USA	kA	USA
(	(	kIx(	(
<g/>
hned	hned	k6eAd1	hned
po	po	k7c6	po
Willis	Willis	k1gFnSc6	Willis
Tower	Tower	k1gMnSc1	Tower
a	a	k8xC	a
Trump	Trump	k1gMnSc1	Trump
International	International	k1gFnSc2	International
Hotel	hotel	k1gInSc1	hotel
and	and	k?	and
Tower	Tower	k1gInSc1	Tower
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
však	však	k9	však
byla	být	k5eAaImAgFnS	být
dostavěna	dostavěn	k2eAgFnSc1d1	dostavěna
náhrada	náhrada	k1gFnSc1	náhrada
za	za	k7c4	za
"	"	kIx"	"
<g/>
dvojčata	dvojče	k1gNnPc1	dvojče
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
nové	nový	k2eAgNnSc4d1	nové
světové	světový	k2eAgNnSc4d1	světové
obchodní	obchodní	k2eAgNnSc4d1	obchodní
centrum	centrum	k1gNnSc4	centrum
nazvané	nazvaný	k2eAgFnSc2d1	nazvaná
One	One	k1gFnSc2	One
World	World	k1gMnSc1	World
Trade	Trad	k1gInSc5	Trad
Center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
svou	svůj	k3xOyFgFnSc4	svůj
výškou	výška	k1gFnSc7	výška
Empire	empir	k1gInSc5	empir
State	status	k1gInSc5	status
Building	Building	k1gInSc4	Building
opět	opět	k6eAd1	opět
překonalo	překonat	k5eAaPmAgNnS	překonat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1986	[number]	k4	1986
se	se	k3xPyFc4	se
budova	budova	k1gFnSc1	budova
zařadila	zařadit	k5eAaPmAgFnS	zařadit
na	na	k7c4	na
seznam	seznam	k1gInSc4	seznam
národních	národní	k2eAgFnPc2d1	národní
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
je	být	k5eAaImIp3nS	být
vlastněna	vlastnit	k5eAaImNgFnS	vlastnit
a	a	k8xC	a
řízena	řídit	k5eAaImNgFnS	řídit
společností	společnost	k1gFnSc7	společnost
W	W	kA	W
&	&	k?	&
H	H	kA	H
Properties	Properties	k1gInSc1	Properties
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
neodmyslitelnou	odmyslitelný	k2eNgFnSc7d1	neodmyslitelná
součástí	součást	k1gFnSc7	součást
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
Eiffelova	Eiffelův	k2eAgFnSc1d1	Eiffelova
věž	věž	k1gFnSc1	věž
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
Paříži	Paříž	k1gFnSc3	Paříž
<g/>
.	.	kIx.	.
</s>
<s>
Empire	empir	k1gInSc5	empir
State	status	k1gInSc5	status
Building	Building	k1gInSc4	Building
navrhl	navrhnout	k5eAaPmAgInS	navrhnout
William	William	k1gInSc1	William
F.	F.	kA	F.
Lamb	Lamb	k1gInSc1	Lamb
z	z	k7c2	z
architektonické	architektonický	k2eAgFnSc2d1	architektonická
firmy	firma	k1gFnSc2	firma
Shreve	Shreev	k1gFnSc2	Shreev
<g/>
,	,	kIx,	,
Lamb	Lamba	k1gFnPc2	Lamba
a	a	k8xC	a
Harmon	Harmona	k1gFnPc2	Harmona
<g/>
,	,	kIx,	,
stavební	stavební	k2eAgInPc1d1	stavební
výkresy	výkres	k1gInPc1	výkres
byly	být	k5eAaImAgInP	být
navrženy	navrhnout	k5eAaPmNgInP	navrhnout
za	za	k7c4	za
pouhé	pouhý	k2eAgInPc4d1	pouhý
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
ovšem	ovšem	k9	ovšem
inspirovány	inspirován	k2eAgInPc4d1	inspirován
předchozími	předchozí	k2eAgInPc7d1	předchozí
plány	plán	k1gInPc7	plán
pro	pro	k7c4	pro
Reynolds	Reynolds	k1gInSc4	Reynolds
Building	Building	k1gInSc4	Building
ve	v	k7c6	v
Winston-Salem	Winston-Sal	k1gMnSc7	Winston-Sal
v	v	k7c6	v
Severní	severní	k2eAgFnSc6d1	severní
Karolíně	Karolína	k1gFnSc6	Karolína
a	a	k8xC	a
Carew	Carew	k1gFnSc6	Carew
Tower	Tower	k1gInSc1	Tower
v	v	k7c6	v
Cincinnati	Cincinnati	k1gFnPc6	Cincinnati
<g/>
,	,	kIx,	,
stát	stát	k5eAaImF	stát
Ohio	Ohio	k1gNnSc4	Ohio
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
návrh	návrh	k1gInSc1	návrh
značil	značit	k5eAaImAgInS	značit
pouze	pouze	k6eAd1	pouze
80	[number]	k4	80
<g/>
patrovou	patrový	k2eAgFnSc4d1	patrová
budovu	budova	k1gFnSc4	budova
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poté	poté	k6eAd1	poté
co	co	k9	co
se	se	k3xPyFc4	se
navýšila	navýšit	k5eAaPmAgFnS	navýšit
plánovaná	plánovaný	k2eAgFnSc1d1	plánovaná
výška	výška	k1gFnSc1	výška
Chrysler	Chrysler	k1gMnSc1	Chrysler
Building	Building	k1gInSc1	Building
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
podstatně	podstatně	k6eAd1	podstatně
zvednuta	zvednout	k5eAaPmNgFnS	zvednout
i	i	k9	i
výška	výška	k1gFnSc1	výška
Empire	empir	k1gInSc5	empir
State	status	k1gInSc5	status
Building	Building	k1gInSc4	Building
<g/>
.	.	kIx.	.
</s>
<s>
Generální	generální	k2eAgMnSc1d1	generální
dodavatelé	dodavatel	k1gMnPc1	dodavatel
byli	být	k5eAaImAgMnP	být
Starrett	Starrett	k2eAgInSc4d1	Starrett
Brothers	Brothers	k1gInSc4	Brothers
and	and	k?	and
Eken	Eken	k1gInSc1	Eken
(	(	kIx(	(
<g/>
Starrettovi	Starrettův	k2eAgMnPc1d1	Starrettův
bratři	bratr	k1gMnPc1	bratr
a	a	k8xC	a
Eken	Eken	k1gInSc1	Eken
<g/>
)	)	kIx)	)
a	a	k8xC	a
projekt	projekt	k1gInSc4	projekt
financovali	financovat	k5eAaBmAgMnP	financovat
především	především	k9	především
John	John	k1gMnSc1	John
J.	J.	kA	J.
Raskob	Raskoba	k1gFnPc2	Raskoba
a	a	k8xC	a
Pierre	Pierr	k1gInSc5	Pierr
S.	S.	kA	S.
du	du	k?	du
Pont	Pont	k1gInSc1	Pont
<g/>
.	.	kIx.	.
</s>
<s>
Stavební	stavební	k2eAgFnSc3d1	stavební
firmě	firma	k1gFnSc3	firma
předsedal	předsedat	k5eAaImAgMnS	předsedat
Alfred	Alfred	k1gMnSc1	Alfred
E.	E.	kA	E.
Smith	Smith	k1gMnSc1	Smith
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
guvernér	guvernér	k1gMnSc1	guvernér
státu	stát	k1gInSc2	stát
New	New	k1gFnSc2	New
York	York	k1gInSc1	York
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
James	Jamesa	k1gFnPc2	Jamesa
Farley	Farlea	k1gFnSc2	Farlea
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
General	General	k1gFnSc7	General
Builders	Buildersa	k1gFnPc2	Buildersa
Supply	Supply	k1gFnSc2	Supply
Corporation	Corporation	k1gInSc1	Corporation
dodala	dodat	k5eAaPmAgFnS	dodat
stavební	stavební	k2eAgInPc4d1	stavební
materiály	materiál	k1gInPc4	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Pozemky	pozemka	k1gFnPc1	pozemka
byly	být	k5eAaImAgFnP	být
koupeny	koupit	k5eAaPmNgFnP	koupit
od	od	k7c2	od
Williama	Williamum	k1gNnSc2	Williamum
Waldorfa	Waldorf	k1gMnSc2	Waldorf
Astora	Astor	k1gMnSc2	Astor
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
na	na	k7c6	na
daném	daný	k2eAgNnSc6d1	dané
místě	místo	k1gNnSc6	místo
předtím	předtím	k6eAd1	předtím
postavil	postavit	k5eAaPmAgInS	postavit
původní	původní	k2eAgInSc1d1	původní
hotel	hotel	k1gInSc1	hotel
Waldorf	Waldorf	k1gInSc1	Waldorf
Astoria	Astorium	k1gNnSc2	Astorium
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
musel	muset	k5eAaImAgInS	muset
být	být	k5eAaImF	být
zbourán	zbourat	k5eAaPmNgInS	zbourat
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
výkopové	výkopový	k2eAgFnPc1d1	výkopová
práce	práce	k1gFnPc1	práce
začaly	začít	k5eAaPmAgFnP	začít
22	[number]	k4	22
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1930	[number]	k4	1930
<g/>
,	,	kIx,	,
stavba	stavba	k1gFnSc1	stavba
základů	základ	k1gInPc2	základ
budovy	budova	k1gFnSc2	budova
začala	začít	k5eAaPmAgFnS	začít
symbolicky	symbolicky	k6eAd1	symbolicky
17	[number]	k4	17
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
,	,	kIx,	,
na	na	k7c4	na
Den	den	k1gInSc4	den
svatého	svatý	k2eAgMnSc2d1	svatý
Patrika	Patrik	k1gMnSc2	Patrik
<g/>
,	,	kIx,	,
patrona	patron	k1gMnSc2	patron
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
,	,	kIx,	,
před	před	k7c7	před
davem	dav	k1gInSc7	dav
5	[number]	k4	5
tisíc	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
pracovalo	pracovat	k5eAaImAgNnS	pracovat
asi	asi	k9	asi
3400	[number]	k4	3400
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
přistěhovalců	přistěhovalec	k1gMnPc2	přistěhovalec
z	z	k7c2	z
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
oficiální	oficiální	k2eAgFnSc2d1	oficiální
zprávy	zpráva	k1gFnSc2	zpráva
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
během	během	k7c2	během
stavby	stavba	k1gFnSc2	stavba
5	[number]	k4	5
pracovníků	pracovník	k1gMnPc2	pracovník
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
následkem	následkem	k7c2	následkem
pádu	pád	k1gInSc2	pád
z	z	k7c2	z
výšky	výška	k1gFnSc2	výška
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
tehdy	tehdy	k6eAd1	tehdy
pracovali	pracovat	k5eAaImAgMnP	pracovat
bez	bez	k7c2	bez
lanového	lanový	k2eAgNnSc2d1	lanové
zajištění	zajištění	k1gNnSc2	zajištění
<g/>
.	.	kIx.	.
</s>
<s>
Stavební	stavební	k2eAgNnSc1d1	stavební
tempo	tempo	k1gNnSc1	tempo
bylo	být	k5eAaImAgNnS	být
obrovské	obrovský	k2eAgNnSc1d1	obrovské
<g/>
,	,	kIx,	,
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
4	[number]	k4	4
a	a	k8xC	a
půl	půl	k1xP	půl
patra	patro	k1gNnSc2	patro
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
týden	týden	k1gInSc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
byla	být	k5eAaImAgFnS	být
slavnostně	slavnostně	k6eAd1	slavnostně
otevřena	otevřen	k2eAgFnSc1d1	otevřena
1	[number]	k4	1
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1931	[number]	k4	1931
<g/>
,	,	kIx,	,
tehdejší	tehdejší	k2eAgMnSc1d1	tehdejší
prezident	prezident	k1gMnSc1	prezident
Herbert	Herbert	k1gMnSc1	Herbert
Hoover	Hoover	k1gInSc1	Hoover
pouhým	pouhý	k2eAgNnSc7d1	pouhé
stisknutím	stisknutí	k1gNnSc7	stisknutí
tlačítka	tlačítko	k1gNnSc2	tlačítko
rozsvítil	rozsvítit	k5eAaPmAgMnS	rozsvítit
osvětlení	osvětlení	k1gNnSc3	osvětlení
mrakodrapu	mrakodrap	k1gInSc2	mrakodrap
přímo	přímo	k6eAd1	přímo
z	z	k7c2	z
Washingtonu	Washington	k1gInSc2	Washington
<g/>
,	,	kIx,	,
vzdáleného	vzdálený	k2eAgInSc2d1	vzdálený
od	od	k7c2	od
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
asi	asi	k9	asi
430	[number]	k4	430
km	km	kA	km
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
otevření	otevření	k1gNnSc2	otevření
probíhala	probíhat	k5eAaImAgFnS	probíhat
Velká	velký	k2eAgFnSc1d1	velká
hospodářská	hospodářský	k2eAgFnSc1d1	hospodářská
krize	krize	k1gFnSc1	krize
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
které	který	k3yRgFnSc3	který
nebyla	být	k5eNaImAgFnS	být
pronajata	pronajat	k2eAgFnSc1d1	pronajata
velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
kancelářských	kancelářský	k2eAgFnPc2d1	kancelářská
prostor	prostora	k1gFnPc2	prostora
<g/>
.	.	kIx.	.
</s>
<s>
Nízké	nízký	k2eAgFnSc3d1	nízká
poptávce	poptávka	k1gFnSc3	poptávka
po	po	k7c6	po
kancelářích	kancelář	k1gFnPc6	kancelář
také	také	k9	také
napomohl	napomoct	k5eAaPmAgInS	napomoct
fakt	fakt	k1gInSc1	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
budova	budova	k1gFnSc1	budova
byla	být	k5eAaImAgFnS	být
tehdy	tehdy	k6eAd1	tehdy
postavena	postavit	k5eAaPmNgFnS	postavit
na	na	k7c6	na
nepříliš	příliš	k6eNd1	příliš
vhodném	vhodný	k2eAgNnSc6d1	vhodné
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
daleko	daleko	k6eAd1	daleko
od	od	k7c2	od
dopravního	dopravní	k2eAgNnSc2d1	dopravní
spojení	spojení	k1gNnSc2	spojení
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
nízkému	nízký	k2eAgInSc3d1	nízký
počtu	počet	k1gInSc3	počet
nájemníků	nájemník	k1gMnPc2	nájemník
se	se	k3xPyFc4	se
budově	budova	k1gFnSc3	budova
začalo	začít	k5eAaPmAgNnS	začít
přezdívat	přezdívat	k5eAaImF	přezdívat
Empty	Empt	k1gInPc4	Empt
State	status	k1gInSc5	status
Building	Building	k1gInSc1	Building
(	(	kIx(	(
<g/>
empty	empt	k1gInPc1	empt
=	=	kIx~	=
prázdný	prázdný	k2eAgInSc1d1	prázdný
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
budova	budova	k1gFnSc1	budova
kvůli	kvůli	k7c3	kvůli
finančním	finanční	k2eAgFnPc3d1	finanční
potížím	potíž	k1gFnPc3	potíž
prodána	prodat	k5eAaPmNgFnS	prodat
za	za	k7c4	za
34	[number]	k4	34
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Empire	empir	k1gInSc5	empir
State	status	k1gInSc5	status
Building	Building	k1gInSc4	Building
byla	být	k5eAaImAgFnS	být
23	[number]	k4	23
let	léto	k1gNnPc2	léto
nejvyšší	vysoký	k2eAgFnSc2d3	nejvyšší
člověkem	člověk	k1gMnSc7	člověk
postavená	postavený	k2eAgFnSc1d1	postavená
stavba	stavba	k1gFnSc1	stavba
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
až	až	k9	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1954	[number]	k4	1954
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
ji	on	k3xPp3gFnSc4	on
překonal	překonat	k5eAaPmAgInS	překonat
vysílač	vysílač	k1gInSc1	vysílač
Griffin	Griffin	k2eAgInSc1d1	Griffin
Television	Television	k1gInSc4	Television
Tower	Towero	k1gNnPc2	Towero
Oklahoma	Oklahom	k1gMnSc2	Oklahom
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
také	také	k9	také
36	[number]	k4	36
let	léto	k1gNnPc2	léto
nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
volně	volně	k6eAd1	volně
stojící	stojící	k2eAgFnSc1d1	stojící
stavba	stavba	k1gFnSc1	stavba
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
ji	on	k3xPp3gFnSc4	on
překonala	překonat	k5eAaPmAgFnS	překonat
Televizní	televizní	k2eAgFnSc1d1	televizní
věž	věž	k1gFnSc1	věž
Ostankino	Ostankino	k1gNnSc4	Ostankino
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
držela	držet	k5eAaImAgFnS	držet
rekord	rekord	k1gInSc4	rekord
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
42	[number]	k4	42
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
dokud	dokud	k8xS	dokud
jej	on	k3xPp3gInSc4	on
nepřerušila	přerušit	k5eNaPmAgFnS	přerušit
dostavba	dostavba	k1gFnSc1	dostavba
severní	severní	k2eAgFnSc2d1	severní
věže	věž	k1gFnSc2	věž
Světového	světový	k2eAgNnSc2d1	světové
obchodního	obchodní	k2eAgNnSc2d1	obchodní
centra	centrum	k1gNnSc2	centrum
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1973	[number]	k4	1973
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
teroristických	teroristický	k2eAgInPc6d1	teroristický
útocích	útok	k1gInPc6	útok
z	z	k7c2	z
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2001	[number]	k4	2001
a	a	k8xC	a
následném	následný	k2eAgNnSc6d1	následné
zřícení	zřícení	k1gNnSc6	zřícení
obou	dva	k4xCgFnPc2	dva
věží	věž	k1gFnPc2	věž
WTC	WTC	kA	WTC
se	se	k3xPyFc4	se
Empire	empir	k1gInSc5	empir
State	status	k1gInSc5	status
Building	Building	k1gInSc4	Building
opět	opět	k6eAd1	opět
stal	stát	k5eAaPmAgMnS	stát
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
stavbou	stavba	k1gFnSc7	stavba
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
a	a	k8xC	a
druhou	druhý	k4xOgFnSc4	druhý
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
v	v	k7c6	v
USA	USA	kA	USA
(	(	kIx(	(
<g/>
hned	hned	k6eAd1	hned
po	po	k7c6	po
Willis	Willis	k1gFnSc6	Willis
Tower	Towra	k1gFnPc2	Towra
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
čtvrtou	čtvrtý	k4xOgFnSc4	čtvrtý
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
budovu	budova	k1gFnSc4	budova
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
byl	být	k5eAaImAgInS	být
dostavěn	dostavěn	k2eAgInSc1d1	dostavěn
mrakodrap	mrakodrap	k1gInSc1	mrakodrap
v	v	k7c6	v
Chicagu	Chicago	k1gNnSc6	Chicago
s	s	k7c7	s
názvem	název	k1gInSc7	název
Trump	Trump	k1gInSc1	Trump
International	International	k1gFnPc2	International
Hotel	hotel	k1gInSc1	hotel
and	and	k?	and
Tower	Tower	k1gInSc1	Tower
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
byl	být	k5eAaImAgInS	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
také	také	k9	také
newyorský	newyorský	k2eAgMnSc1d1	newyorský
One	One	k1gMnSc1	One
World	World	k1gMnSc1	World
Trade	Trad	k1gInSc5	Trad
Center	centrum	k1gNnPc2	centrum
v	v	k7c6	v
bezprostřední	bezprostřední	k2eAgFnSc6d1	bezprostřední
blízkosti	blízkost	k1gFnSc6	blízkost
památníku	památník	k1gInSc2	památník
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
zvaném	zvaný	k2eAgNnSc6d1	zvané
Ground	Ground	k1gInSc1	Ground
Zero	Zero	k1gNnSc1	Zero
(	(	kIx(	(
<g/>
oficiální	oficiální	k2eAgInSc1d1	oficiální
název	název	k1gInSc1	název
je	být	k5eAaImIp3nS	být
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
Memorial	Memorial	k1gInSc1	Memorial
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budoucnosti	budoucnost	k1gFnSc6	budoucnost
si	se	k3xPyFc3	se
Empire	empir	k1gInSc5	empir
State	status	k1gInSc5	status
Building	Building	k1gInSc1	Building
ve	v	k7c6	v
výškovém	výškový	k2eAgNnSc6d1	výškové
srovnání	srovnání	k1gNnSc6	srovnání
ještě	ještě	k9	ještě
pohorší	pohoršit	k5eAaPmIp3nS	pohoršit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
počítá	počítat	k5eAaImIp3nS	počítat
s	s	k7c7	s
výstavbou	výstavba	k1gFnSc7	výstavba
mrakodrapu	mrakodrap	k1gInSc2	mrakodrap
Chicago	Chicago	k1gNnSc1	Chicago
Spire	Spir	k1gMnSc5	Spir
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
tato	tento	k3xDgFnSc1	tento
budova	budova	k1gFnSc1	budova
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
přesáhnout	přesáhnout	k5eAaPmF	přesáhnout
hranici	hranice	k1gFnSc4	hranice
500	[number]	k4	500
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
existence	existence	k1gFnSc2	existence
budovy	budova	k1gFnSc2	budova
více	hodně	k6eAd2	hodně
než	než	k8xS	než
30	[number]	k4	30
lidí	člověk	k1gMnPc2	člověk
spáchalo	spáchat	k5eAaPmAgNnS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
skokem	skokem	k6eAd1	skokem
z	z	k7c2	z
vrchních	vrchní	k2eAgFnPc2d1	vrchní
pater	patro	k1gNnPc2	patro
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
první	první	k4xOgFnSc3	první
sebevraždě	sebevražda	k1gFnSc3	sebevražda
došlo	dojít	k5eAaPmAgNnS	dojít
ještě	ještě	k9	ještě
před	před	k7c7	před
dokončením	dokončení	k1gNnSc7	dokončení
stavby	stavba	k1gFnSc2	stavba
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
spáchal	spáchat	k5eAaPmAgMnS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
propuštěný	propuštěný	k2eAgMnSc1d1	propuštěný
zaměstnanec	zaměstnanec	k1gMnSc1	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
byl	být	k5eAaImAgMnS	být
na	na	k7c6	na
vyhlídkové	vyhlídkový	k2eAgFnSc6d1	vyhlídková
terase	terasa	k1gFnSc6	terasa
namontován	namontován	k2eAgInSc4d1	namontován
plot	plot	k1gInSc4	plot
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
během	během	k7c2	během
jednoho	jeden	k4xCgInSc2	jeden
týdne	týden	k1gInSc2	týden
pokusilo	pokusit	k5eAaPmAgNnS	pokusit
skočit	skočit	k5eAaPmF	skočit
pět	pět	k4xCc4	pět
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Vinou	vinou	k7c2	vinou
chybné	chybný	k2eAgFnSc2d1	chybná
navigace	navigace	k1gFnSc2	navigace
v	v	k7c6	v
mlze	mlha	k1gFnSc6	mlha
narazil	narazit	k5eAaPmAgMnS	narazit
do	do	k7c2	do
severní	severní	k2eAgFnSc2d1	severní
strany	strana	k1gFnSc2	strana
budovy	budova	k1gFnSc2	budova
mezi	mezi	k7c7	mezi
79	[number]	k4	79
<g/>
.	.	kIx.	.
a	a	k8xC	a
80	[number]	k4	80
<g/>
.	.	kIx.	.
patrem	patro	k1gNnSc7	patro
28	[number]	k4	28
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1945	[number]	k4	1945
dopoledne	dopoledne	k1gNnSc4	dopoledne
americký	americký	k2eAgInSc1d1	americký
bombardér	bombardér	k1gInSc1	bombardér
B-	B-	k1gFnSc1	B-
<g/>
25	[number]	k4	25
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
pilotoval	pilotovat	k5eAaImAgInS	pilotovat
William	William	k1gInSc1	William
F.	F.	kA	F.
Smith	Smith	k1gMnSc1	Smith
junior	junior	k1gMnSc1	junior
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
neštěstí	neštěstí	k1gNnSc6	neštěstí
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
11	[number]	k4	11
lidí	člověk	k1gMnPc2	člověk
pracujících	pracující	k2eAgMnPc2d1	pracující
v	v	k7c4	v
79	[number]	k4	79
<g/>
.	.	kIx.	.
patře	patro	k1gNnSc6	patro
a	a	k8xC	a
tři	tři	k4xCgMnPc1	tři
členové	člen	k1gMnPc1	člen
posádky	posádka	k1gFnSc2	posádka
letounu	letoun	k1gInSc2	letoun
<g/>
.	.	kIx.	.
</s>
<s>
Betty	Betty	k1gFnSc1	Betty
Lou	Lou	k1gFnSc2	Lou
Oliver	Olivra	k1gFnPc2	Olivra
tehdy	tehdy	k6eAd1	tehdy
obsluhovala	obsluhovat	k5eAaImAgFnS	obsluhovat
výtah	výtah	k1gInSc4	výtah
<g/>
,	,	kIx,	,
ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
kvůli	kvůli	k7c3	kvůli
nárazu	náraz	k1gInSc2	náraz
zřítil	zřítit	k5eAaPmAgInS	zřítit
o	o	k7c4	o
75	[number]	k4	75
pater	patro	k1gNnPc2	patro
<g/>
,	,	kIx,	,
pád	pád	k1gInSc1	pád
ale	ale	k8xC	ale
přežila	přežít	k5eAaPmAgFnS	přežít
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
dodnes	dodnes	k6eAd1	dodnes
zapsána	zapsat	k5eAaPmNgFnS	zapsat
v	v	k7c6	v
Guinnessově	Guinnessův	k2eAgFnSc6d1	Guinnessova
knize	kniha	k1gFnSc6	kniha
rekordů	rekord	k1gInPc2	rekord
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
nárazu	náraz	k1gInSc6	náraz
jeden	jeden	k4xCgInSc1	jeden
motor	motor	k1gInSc1	motor
spadl	spadnout	k5eAaPmAgInS	spadnout
do	do	k7c2	do
výtahové	výtahový	k2eAgFnSc2d1	výtahová
šachty	šachta	k1gFnSc2	šachta
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
způsobil	způsobit	k5eAaPmAgInS	způsobit
požár	požár	k1gInSc1	požár
<g/>
,	,	kIx,	,
ten	ten	k3xDgInSc1	ten
byl	být	k5eAaImAgInS	být
uhašen	uhasit	k5eAaPmNgInS	uhasit
po	po	k7c6	po
40	[number]	k4	40
minutách	minuta	k1gFnPc6	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
motor	motor	k1gInSc1	motor
proletěl	proletět	k5eAaPmAgInS	proletět
budovou	budova	k1gFnSc7	budova
a	a	k8xC	a
skončil	skončit	k5eAaPmAgInS	skončit
jeden	jeden	k4xCgInSc4	jeden
blok	blok	k1gInSc4	blok
za	za	k7c7	za
ní	on	k3xPp3gFnSc7	on
<g/>
,	,	kIx,	,
na	na	k7c6	na
střeše	střecha	k1gFnSc6	střecha
jiné	jiný	k2eAgFnSc2d1	jiná
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Nárazem	náraz	k1gInSc7	náraz
ani	ani	k8xC	ani
následným	následný	k2eAgInSc7d1	následný
výbuchem	výbuch	k1gInSc7	výbuch
nebyla	být	k5eNaImAgFnS	být
narušena	narušen	k2eAgFnSc1d1	narušena
statika	statika	k1gFnSc1	statika
budovy	budova	k1gFnSc2	budova
a	a	k8xC	a
škody	škoda	k1gFnSc2	škoda
byly	být	k5eAaImAgFnP	být
vyčísleny	vyčíslen	k2eAgFnPc1d1	vyčíslena
na	na	k7c4	na
1	[number]	k4	1
milion	milion	k4xCgInSc4	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
byla	být	k5eAaImAgFnS	být
opět	opět	k6eAd1	opět
otevřena	otevřít	k5eAaPmNgFnS	otevřít
hned	hned	k6eAd1	hned
následující	následující	k2eAgInSc4d1	následující
týden	týden	k1gInSc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc1d3	nejvyšší
plocha	plocha	k1gFnSc1	plocha
budovy	budova	k1gFnSc2	budova
byla	být	k5eAaImAgFnS	být
původně	původně	k6eAd1	původně
navržena	navrhnout	k5eAaPmNgFnS	navrhnout
pro	pro	k7c4	pro
kotvení	kotvení	k1gNnSc4	kotvení
vzducholodí	vzducholoď	k1gFnPc2	vzducholoď
<g/>
.	.	kIx.	.
102	[number]	k4	102
<g/>
.	.	kIx.	.
patro	patro	k1gNnSc1	patro
mělo	mít	k5eAaImAgNnS	mít
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
přistávací	přistávací	k2eAgFnSc1d1	přistávací
plocha	plocha	k1gFnSc1	plocha
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
102	[number]	k4	102
<g/>
.	.	kIx.	.
a	a	k8xC	a
86	[number]	k4	86
<g/>
.	.	kIx.	.
patrem	patro	k1gNnSc7	patro
byl	být	k5eAaImAgInS	být
vybudován	vybudován	k2eAgInSc1d1	vybudován
výtah	výtah	k1gInSc1	výtah
pro	pro	k7c4	pro
cestující	cestující	k1gMnPc4	cestující
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
měli	mít	k5eAaImAgMnP	mít
být	být	k5eAaImF	být
v	v	k7c6	v
86	[number]	k4	86
<g/>
.	.	kIx.	.
patře	patro	k1gNnSc6	patro
odbavováni	odbavovat	k5eAaImNgMnP	odbavovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
pokusech	pokus	k1gInPc6	pokus
se	s	k7c7	s
vzducholoďmi	vzducholoď	k1gFnPc7	vzducholoď
se	se	k3xPyFc4	se
od	od	k7c2	od
plánu	plán	k1gInSc2	plán
upustilo	upustit	k5eAaPmAgNnS	upustit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
ukázal	ukázat	k5eAaPmAgInS	ukázat
jako	jako	k9	jako
nepraktický	praktický	k2eNgInSc1d1	nepraktický
a	a	k8xC	a
krajně	krajně	k6eAd1	krajně
nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
v	v	k7c6	v
budově	budova	k1gFnSc6	budova
krátce	krátce	k6eAd1	krátce
sídlilo	sídlit	k5eAaImAgNnS	sídlit
nově	nově	k6eAd1	nově
vzniklé	vzniklý	k2eAgNnSc4d1	vzniklé
československé	československý	k2eAgNnSc4d1	Československé
vysílání	vysílání	k1gNnSc4	vysílání
rozhlasové	rozhlasový	k2eAgFnSc2d1	rozhlasová
stanice	stanice	k1gFnSc2	stanice
Rádio	rádio	k1gNnSc4	rádio
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
Evropa	Evropa	k1gFnSc1	Evropa
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
Peroutky	Peroutka	k1gMnSc2	Peroutka
<g/>
.	.	kIx.	.
24	[number]	k4	24
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1997	[number]	k4	1997
postřelil	postřelit	k5eAaPmAgMnS	postřelit
palestinský	palestinský	k2eAgMnSc1d1	palestinský
ozbrojenec	ozbrojenec	k1gMnSc1	ozbrojenec
sedm	sedm	k4xCc4	sedm
lidí	člověk	k1gMnPc2	člověk
na	na	k7c6	na
vyhlídce	vyhlídka	k1gFnSc6	vyhlídka
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
jednoho	jeden	k4xCgMnSc4	jeden
zabil	zabít	k5eAaPmAgMnS	zabít
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
zastřelil	zastřelit	k5eAaPmAgMnS	zastřelit
i	i	k9	i
sám	sám	k3xTgMnSc1	sám
sebe	sebe	k3xPyFc4	sebe
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
102	[number]	k4	102
<g/>
patrová	patrový	k2eAgFnSc1d1	patrová
budova	budova	k1gFnSc1	budova
se	se	k3xPyFc4	se
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
výšky	výška	k1gFnSc2	výška
448	[number]	k4	448
metrů	metr	k1gInPc2	metr
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gFnSc1	jeho
uznávaná	uznávaný	k2eAgFnSc1d1	uznávaná
výška	výška	k1gFnSc1	výška
je	být	k5eAaImIp3nS	být
381	[number]	k4	381
m	m	kA	m
<g/>
,	,	kIx,	,
anténa	anténa	k1gFnSc1	anténa
se	se	k3xPyFc4	se
do	do	k7c2	do
ní	on	k3xPp3gFnSc2	on
nepočítá	počítat	k5eNaImIp3nS	počítat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byla	být	k5eAaImAgFnS	být
přidělaná	přidělaný	k2eAgFnSc1d1	přidělaná
až	až	k6eAd1	až
delší	dlouhý	k2eAgFnSc4d2	delší
dobu	doba	k1gFnSc4	doba
po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
stavby	stavba	k1gFnSc2	stavba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budově	budova	k1gFnSc6	budova
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
85	[number]	k4	85
pater	patro	k1gNnPc2	patro
využívaných	využívaný	k2eAgFnPc2d1	využívaná
pro	pro	k7c4	pro
kancelářské	kancelářský	k2eAgInPc4d1	kancelářský
a	a	k8xC	a
obchodní	obchodní	k2eAgInPc4d1	obchodní
účely	účel	k1gInPc4	účel
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
86	[number]	k4	86
<g/>
.	.	kIx.	.
patře	patro	k1gNnSc6	patro
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
vnitřní	vnitřní	k2eAgFnSc1d1	vnitřní
i	i	k8xC	i
venkovní	venkovní	k2eAgFnSc1d1	venkovní
vyhlídková	vyhlídkový	k2eAgFnSc1d1	vyhlídková
terasa	terasa	k1gFnSc1	terasa
<g/>
.	.	kIx.	.
</s>
<s>
Zbylých	zbylý	k2eAgNnPc2d1	zbylé
16	[number]	k4	16
pater	patro	k1gNnPc2	patro
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
úzké	úzký	k2eAgFnSc6d1	úzká
věži	věž	k1gFnSc6	věž
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
končí	končit	k5eAaImIp3nS	končit
102	[number]	k4	102
<g/>
.	.	kIx.	.
patrem	patro	k1gNnSc7	patro
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterém	který	k3yQgInSc6	který
je	být	k5eAaImIp3nS	být
vyhlídka	vyhlídka	k1gFnSc1	vyhlídka
a	a	k8xC	a
nad	nad	k7c7	nad
ní	on	k3xPp3gFnSc7	on
se	se	k3xPyFc4	se
tyčí	tyčit	k5eAaImIp3nS	tyčit
67	[number]	k4	67
m	m	kA	m
vysoká	vysoký	k2eAgFnSc1d1	vysoká
anténa	anténa	k1gFnSc1	anténa
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
samotném	samotný	k2eAgInSc6d1	samotný
vrcholu	vrchol	k1gInSc6	vrchol
je	být	k5eAaImIp3nS	být
hromosvod	hromosvod	k1gInSc1	hromosvod
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
bývá	bývat	k5eAaImIp3nS	bývat
40	[number]	k4	40
<g/>
×	×	k?	×
za	za	k7c4	za
rok	rok	k1gInSc4	rok
zasažen	zasáhnout	k5eAaPmNgMnS	zasáhnout
bleskem	blesk	k1gInSc7	blesk
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
to	ten	k3xDgNnSc1	ten
byla	být	k5eAaImAgFnS	být
první	první	k4xOgFnSc1	první
stavba	stavba	k1gFnSc1	stavba
na	na	k7c6	na
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
100	[number]	k4	100
pater	patro	k1gNnPc2	patro
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
6500	[number]	k4	6500
oken	okno	k1gNnPc2	okno
a	a	k8xC	a
73	[number]	k4	73
výtahů	výtah	k1gInPc2	výtah
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
celková	celkový	k2eAgFnSc1d1	celková
podlahová	podlahový	k2eAgFnSc1d1	podlahová
plocha	plocha	k1gFnSc1	plocha
je	být	k5eAaImIp3nS	být
257.211	[number]	k4	257.211
m2	m2	k4	m2
<g/>
.	.	kIx.	.
</s>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
budovy	budova	k1gFnSc2	budova
je	být	k5eAaImIp3nS	být
přibližně	přibližně	k6eAd1	přibližně
370.000	[number]	k4	370.000
tun	tuna	k1gFnPc2	tuna
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budově	budova	k1gFnSc6	budova
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
113	[number]	k4	113
km	km	kA	km
potrubí	potrubí	k1gNnSc2	potrubí
a	a	k8xC	a
760	[number]	k4	760
km	km	kA	km
elektrických	elektrický	k2eAgInPc2d1	elektrický
drátů	drát	k1gInPc2	drát
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
je	být	k5eAaImIp3nS	být
postavena	postaven	k2eAgFnSc1d1	postavena
z	z	k7c2	z
ocelové	ocelový	k2eAgFnSc2d1	ocelová
konstrukce	konstrukce	k1gFnSc2	konstrukce
a	a	k8xC	a
zdivo	zdivo	k1gNnSc1	zdivo
z	z	k7c2	z
Bedfordského	Bedfordský	k2eAgInSc2d1	Bedfordský
vápence	vápenec	k1gInSc2	vápenec
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
použito	použít	k5eAaPmNgNnS	použít
60	[number]	k4	60
000	[number]	k4	000
tun	tuna	k1gFnPc2	tuna
oceli	ocel	k1gFnSc2	ocel
<g/>
,	,	kIx,	,
28	[number]	k4	28
700	[number]	k4	700
m2	m2	k4	m2
mramoru	mramor	k1gInSc2	mramor
pro	pro	k7c4	pro
interiér	interiér	k1gInSc4	interiér
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
budově	budova	k1gFnSc6	budova
sídlí	sídlet	k5eAaImIp3nS	sídlet
asi	asi	k9	asi
1000	[number]	k4	1000
firem	firma	k1gFnPc2	firma
a	a	k8xC	a
denně	denně	k6eAd1	denně
zde	zde	k6eAd1	zde
pracuje	pracovat	k5eAaImIp3nS	pracovat
asi	asi	k9	asi
21	[number]	k4	21
000	[number]	k4	000
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
dokonce	dokonce	k9	dokonce
své	svůj	k3xOyFgNnSc4	svůj
vlastní	vlastní	k2eAgNnSc4d1	vlastní
směrovací	směrovací	k2eAgNnSc4d1	směrovací
číslo	číslo	k1gNnSc4	číslo
(	(	kIx(	(
<g/>
10118	[number]	k4	10118
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hned	hned	k6eAd1	hned
po	po	k7c6	po
Pentagonu	Pentagon	k1gInSc6	Pentagon
to	ten	k3xDgNnSc1	ten
je	být	k5eAaImIp3nS	být
druhý	druhý	k4xOgInSc1	druhý
největší	veliký	k2eAgInSc1d3	veliký
kancelářský	kancelářský	k2eAgInSc1d1	kancelářský
komplex	komplex	k1gInSc1	komplex
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
trvala	trvat	k5eAaImAgFnS	trvat
1	[number]	k4	1
rok	rok	k1gInSc4	rok
a	a	k8xC	a
45	[number]	k4	45
dní	den	k1gInPc2	den
s	s	k7c7	s
rychlostí	rychlost	k1gFnSc7	rychlost
4,5	[number]	k4	4,5
patra	patro	k1gNnSc2	patro
za	za	k7c4	za
týden	týden	k1gInSc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
postavena	postaven	k2eAgFnSc1d1	postavena
ve	v	k7c6	v
stylu	styl	k1gInSc6	styl
art	art	k?	art
deco	deco	k1gMnSc1	deco
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
pro	pro	k7c4	pro
30	[number]	k4	30
<g/>
.	.	kIx.	.
léta	léto	k1gNnPc4	léto
typický	typický	k2eAgInSc1d1	typický
<g/>
.	.	kIx.	.
</s>
<s>
Náklady	náklad	k1gInPc1	náklad
na	na	k7c4	na
budovu	budova	k1gFnSc4	budova
činily	činit	k5eAaImAgInP	činit
40.950.000	[number]	k4	40.950.000
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Samotné	samotný	k2eAgInPc1d1	samotný
stavební	stavební	k2eAgInPc1d1	stavební
náklady	náklad	k1gInPc1	náklad
byly	být	k5eAaImAgInP	být
24.718.000	[number]	k4	24.718.000
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Vstupní	vstupní	k2eAgFnSc1d1	vstupní
hala	hala	k1gFnSc1	hala
budovy	budova	k1gFnSc2	budova
s	s	k7c7	s
délkou	délka	k1gFnSc7	délka
30	[number]	k4	30
m	m	kA	m
zaujímá	zaujímat	k5eAaImIp3nS	zaujímat
výšku	výška	k1gFnSc4	výška
celých	celý	k2eAgNnPc2d1	celé
tří	tři	k4xCgNnPc2	tři
pater	patro	k1gNnPc2	patro
a	a	k8xC	a
nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
kolem	kolem	k7c2	kolem
výtahů	výtah	k1gInPc2	výtah
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
interiér	interiér	k1gInSc1	interiér
je	být	k5eAaImIp3nS	být
proveden	provést	k5eAaPmNgInS	provést
převážně	převážně	k6eAd1	převážně
z	z	k7c2	z
mramoru	mramor	k1gInSc2	mramor
<g/>
,	,	kIx,	,
zdobeného	zdobený	k2eAgInSc2d1	zdobený
motivy	motiv	k1gInPc7	motiv
sedmi	sedm	k4xCc2	sedm
divů	div	k1gInPc2	div
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
osmý	osmý	k4xOgInSc4	osmý
div	div	k1gInSc4	div
je	být	k5eAaImIp3nS	být
prezentována	prezentovat	k5eAaBmNgFnS	prezentovat
samotná	samotný	k2eAgFnSc1d1	samotná
Empire	empir	k1gInSc5	empir
State	status	k1gInSc5	status
Building	Building	k1gInSc1	Building
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
vchodům	vchod	k1gInPc3	vchod
z	z	k7c2	z
33	[number]	k4	33
<g/>
.	.	kIx.	.
a	a	k8xC	a
34	[number]	k4	34
<g/>
.	.	kIx.	.
ulice	ulice	k1gFnPc1	ulice
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nP	nacházet
markýzy	markýza	k1gFnPc1	markýza
z	z	k7c2	z
oceli	ocel	k1gFnSc2	ocel
a	a	k8xC	a
hliníku	hliník	k1gInSc2	hliník
<g/>
.	.	kIx.	.
</s>
<s>
Základy	základ	k1gInPc1	základ
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
17	[number]	k4	17
m	m	kA	m
pod	pod	k7c4	pod
povrch	povrch	k1gInSc4	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
nyní	nyní	k6eAd1	nyní
prochází	procházet	k5eAaImIp3nS	procházet
renovací	renovace	k1gFnSc7	renovace
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
energeticky	energeticky	k6eAd1	energeticky
a	a	k8xC	a
ekologicky	ekologicky	k6eAd1	ekologicky
úspornější	úsporný	k2eAgFnSc1d2	úspornější
<g/>
,	,	kIx,	,
tato	tento	k3xDgFnSc1	tento
renovace	renovace	k1gFnSc1	renovace
si	se	k3xPyFc3	se
vyžádala	vyžádat	k5eAaPmAgFnS	vyžádat
120	[number]	k4	120
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Odhadované	odhadovaný	k2eAgInPc1d1	odhadovaný
celkové	celkový	k2eAgInPc1d1	celkový
náklady	náklad	k1gInPc1	náklad
mají	mít	k5eAaImIp3nP	mít
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
0,5	[number]	k4	0,5
miliard	miliarda	k4xCgFnPc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
byly	být	k5eAaImAgFnP	být
přidány	přidat	k5eAaPmNgInP	přidat
reflektory	reflektor	k1gInPc1	reflektor
osvětlující	osvětlující	k2eAgFnSc2d1	osvětlující
vrchní	vrchní	k1gFnSc2	vrchní
část	část	k1gFnSc1	část
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Osvětlení	osvětlení	k1gNnSc1	osvětlení
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
různými	různý	k2eAgFnPc7d1	různá
barvami	barva	k1gFnPc7	barva
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
závisí	záviset	k5eAaImIp3nS	záviset
a	a	k8xC	a
různě	různě	k6eAd1	různě
se	se	k3xPyFc4	se
střídají	střídat	k5eAaImIp3nP	střídat
podle	podle	k7c2	podle
daných	daný	k2eAgFnPc2d1	daná
kalendářních	kalendářní	k2eAgFnPc2d1	kalendářní
událostí	událost	k1gFnPc2	událost
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Den	den	k1gInSc1	den
svatého	svatý	k2eAgMnSc2d1	svatý
Patrika	Patrik	k1gMnSc2	Patrik
<g/>
,	,	kIx,	,
Vánoce	Vánoce	k1gFnPc1	Vánoce
<g/>
,	,	kIx,	,
Silvestr	Silvestr	k1gInSc1	Silvestr
<g/>
,	,	kIx,	,
Den	den	k1gInSc1	den
nezávislosti	nezávislost	k1gFnSc2	nezávislost
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
měsíců	měsíc	k1gInPc2	měsíc
po	po	k7c6	po
zničení	zničení	k1gNnSc6	zničení
Světového	světový	k2eAgNnSc2d1	světové
obchodního	obchodní	k2eAgNnSc2d1	obchodní
centra	centrum	k1gNnSc2	centrum
byl	být	k5eAaImAgInS	být
mrakodrap	mrakodrap	k1gInSc1	mrakodrap
osvětlen	osvětlit	k5eAaPmNgInS	osvětlit
národními	národní	k2eAgFnPc7d1	národní
barvami	barva	k1gFnPc7	barva
(	(	kIx(	(
<g/>
červenou	červený	k2eAgFnSc7d1	červená
<g/>
,	,	kIx,	,
bílou	bílý	k2eAgFnSc7d1	bílá
a	a	k8xC	a
modrou	modrý	k2eAgFnSc7d1	modrá
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Barvy	barva	k1gFnPc1	barva
se	se	k3xPyFc4	se
mění	měnit	k5eAaImIp3nP	měnit
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
má	mít	k5eAaImIp3nS	mít
některý	některý	k3yIgInSc1	některý
newyorský	newyorský	k2eAgInSc1d1	newyorský
sportovní	sportovní	k2eAgInSc1d1	sportovní
klub	klub	k1gInSc1	klub
domácí	domácí	k2eAgInSc4d1	domácí
zápas	zápas	k1gInSc4	zápas
<g/>
,	,	kIx,	,
mrakodrap	mrakodrap	k1gInSc4	mrakodrap
pak	pak	k6eAd1	pak
svítí	svítit	k5eAaImIp3nP	svítit
klubovými	klubový	k2eAgFnPc7d1	klubová
barvami	barva	k1gFnPc7	barva
<g/>
.	.	kIx.	.
</s>
<s>
Měnily	měnit	k5eAaImAgInP	měnit
se	se	k3xPyFc4	se
i	i	k9	i
například	například	k6eAd1	například
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1995	[number]	k4	1995
při	při	k7c6	při
uvolnění	uvolnění	k1gNnSc6	uvolnění
Microsoftu	Microsoft	k1gInSc2	Microsoft
Windows	Windows	kA	Windows
95	[number]	k4	95
(	(	kIx(	(
<g/>
modrá	modrý	k2eAgFnSc1d1	modrá
<g/>
,	,	kIx,	,
červená	červený	k2eAgFnSc1d1	červená
<g/>
,	,	kIx,	,
žlutá	žlutý	k2eAgFnSc1d1	žlutá
a	a	k8xC	a
zelená	zelený	k2eAgFnSc1d1	zelená
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
při	při	k7c6	při
probíhajícím	probíhající	k2eAgInSc6d1	probíhající
tenisovém	tenisový	k2eAgInSc6d1	tenisový
turnaji	turnaj	k1gInSc6	turnaj
US	US	kA	US
Open	Opena	k1gFnPc2	Opena
(	(	kIx(	(
<g/>
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
žlutě	žlutě	k6eAd1	žlutě
také	také	k9	také
svítila	svítit	k5eAaImAgFnS	svítit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
při	při	k7c6	při
uvedení	uvedení	k1gNnSc6	uvedení
filmu	film	k1gInSc2	film
Simpsonovi	Simpsonův	k2eAgMnPc1d1	Simpsonův
ve	v	k7c6	v
filmu	film	k1gInSc6	film
do	do	k7c2	do
kin	kino	k1gNnPc2	kino
a	a	k8xC	a
mnoha	mnoho	k4c6	mnoho
dalších	další	k2eAgFnPc6d1	další
akcích	akce	k1gFnPc6	akce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
zhasla	zhasnout	k5eAaPmAgFnS	zhasnout
všechna	všechen	k3xTgNnPc4	všechen
světla	světlo	k1gNnPc4	světlo
budovy	budova	k1gFnSc2	budova
na	na	k7c4	na
dobu	doba	k1gFnSc4	doba
15	[number]	k4	15
minut	minuta	k1gFnPc2	minuta
kvůli	kvůli	k7c3	kvůli
smrti	smrt	k1gFnSc3	smrt
herečky	herečka	k1gFnSc2	herečka
Fay	Fay	k1gFnSc2	Fay
Wray	Wraa	k1gFnSc2	Wraa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
v	v	k7c6	v
legendárním	legendární	k2eAgInSc6d1	legendární
filmu	film	k1gInSc6	film
King	King	k1gMnSc1	King
Kong	Kongo	k1gNnPc2	Kongo
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
na	na	k7c6	na
této	tento	k3xDgFnSc6	tento
budově	budova	k1gFnSc6	budova
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Světla	světlo	k1gNnPc1	světlo
zhasla	zhasnout	k5eAaPmAgNnP	zhasnout
společně	společně	k6eAd1	společně
s	s	k7c7	s
celým	celý	k2eAgNnSc7d1	celé
městem	město	k1gNnSc7	město
i	i	k9	i
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
při	při	k7c6	při
akci	akce	k1gFnSc6	akce
Hodina	hodina	k1gFnSc1	hodina
Země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
byla	být	k5eAaImAgFnS	být
vypnutá	vypnutý	k2eAgFnSc1d1	vypnutá
přesně	přesně	k6eAd1	přesně
jednu	jeden	k4xCgFnSc4	jeden
hodinu	hodina	k1gFnSc4	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Empire	empir	k1gInSc5	empir
State	status	k1gInSc5	status
Building	Building	k1gInSc1	Building
je	být	k5eAaImIp3nS	být
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nejpopulárnějších	populární	k2eAgFnPc2d3	nejpopulárnější
venkovních	venkovní	k2eAgFnPc2d1	venkovní
vyhlídek	vyhlídka	k1gFnPc2	vyhlídka
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
nabízí	nabízet	k5eAaImIp3nS	nabízet
pohled	pohled	k1gInSc4	pohled
na	na	k7c4	na
celý	celý	k2eAgInSc4d1	celý
New	New	k1gFnPc7	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
New	New	k1gFnPc4	New
Jersey	Jersea	k1gFnPc4	Jersea
a	a	k8xC	a
další	další	k2eAgNnPc4d1	další
města	město	k1gNnPc4	město
<g/>
.	.	kIx.	.
</s>
<s>
Navštívilo	navštívit	k5eAaPmAgNnS	navštívit
ji	on	k3xPp3gFnSc4	on
doposud	doposud	k6eAd1	doposud
více	hodně	k6eAd2	hodně
než	než	k8xS	než
110	[number]	k4	110
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
vyhlídka	vyhlídka	k1gFnSc1	vyhlídka
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
86	[number]	k4	86
<g/>
.	.	kIx.	.
patře	patro	k1gNnSc6	patro
<g/>
,	,	kIx,	,
druhá	druhý	k4xOgFnSc1	druhý
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
ve	v	k7c6	v
102	[number]	k4	102
<g/>
.	.	kIx.	.
patře	patro	k1gNnSc6	patro
<g/>
.	.	kIx.	.
</s>
<s>
Cesta	cesta	k1gFnSc1	cesta
výtahem	výtah	k1gInSc7	výtah
z	z	k7c2	z
lobby	lobby	k1gFnSc2	lobby
budovy	budova	k1gFnSc2	budova
do	do	k7c2	do
86	[number]	k4	86
<g/>
.	.	kIx.	.
patra	patro	k1gNnSc2	patro
trvá	trvat	k5eAaImIp3nS	trvat
méně	málo	k6eAd2	málo
než	než	k8xS	než
jednu	jeden	k4xCgFnSc4	jeden
minutu	minuta	k1gFnSc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Vyhlídka	vyhlídka	k1gFnSc1	vyhlídka
je	být	k5eAaImIp3nS	být
otevřená	otevřený	k2eAgFnSc1d1	otevřená
denně	denně	k6eAd1	denně
od	od	k7c2	od
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
23	[number]	k4	23
<g/>
:	:	kIx,	:
<g/>
15	[number]	k4	15
hod	hod	k1gInSc1	hod
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
ve	v	k7c4	v
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
<g/>
,	,	kIx,	,
pátek	pátek	k1gInSc4	pátek
a	a	k8xC	a
v	v	k7c4	v
sobotu	sobota	k1gFnSc4	sobota
se	se	k3xPyFc4	se
zavírá	zavírat	k5eAaImIp3nS	zavírat
až	až	k9	až
ve	v	k7c4	v
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
hod	hod	k1gInSc1	hod
<g/>
.	.	kIx.	.
</s>
<s>
Cena	cena	k1gFnSc1	cena
vstupenky	vstupenka	k1gFnSc2	vstupenka
je	být	k5eAaImIp3nS	být
27	[number]	k4	27
dolarů	dolar	k1gInPc2	dolar
(	(	kIx(	(
<g/>
rok	rok	k1gInSc1	rok
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
seniory	senior	k1gMnPc4	senior
24	[number]	k4	24
dolarů	dolar	k1gInPc2	dolar
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
první	první	k4xOgInSc4	první
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
provozu	provoz	k1gInSc2	provoz
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
)	)	kIx)	)
vydělala	vydělat	k5eAaPmAgFnS	vydělat
terasa	terasa	k1gFnSc1	terasa
2	[number]	k4	2
miliony	milion	k4xCgInPc4	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byla	být	k5eAaImAgFnS	být
-	-	kIx~	-
kvůli	kvůli	k7c3	kvůli
nedostatku	nedostatek	k1gInSc3	nedostatek
nájemníků	nájemník	k1gMnPc2	nájemník
-	-	kIx~	-
stejná	stejný	k2eAgFnSc1d1	stejná
částka	částka	k1gFnSc1	částka
jako	jako	k8xC	jako
z	z	k7c2	z
nájmu	nájem	k1gInSc2	nájem
z	z	k7c2	z
prvního	první	k4xOgInSc2	první
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc4d3	veliký
mediální	mediální	k2eAgInSc4d1	mediální
trh	trh	k1gInSc4	trh
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
zřícení	zřícení	k1gNnSc4	zřícení
věží	věž	k1gFnPc2	věž
Světového	světový	k2eAgNnSc2d1	světové
obchodního	obchodní	k2eAgNnSc2d1	obchodní
centra	centrum	k1gNnSc2	centrum
se	se	k3xPyFc4	se
téměř	téměř	k6eAd1	téměř
všechny	všechen	k3xTgInPc4	všechen
newyorské	newyorský	k2eAgInPc4d1	newyorský
televizní	televizní	k2eAgInPc4d1	televizní
kanály	kanál	k1gInPc4	kanál
a	a	k8xC	a
rádiové	rádiový	k2eAgFnPc4d1	rádiová
stanice	stanice	k1gFnPc4	stanice
přesunuly	přesunout	k5eAaPmAgInP	přesunout
na	na	k7c4	na
Empire	empir	k1gInSc5	empir
State	status	k1gInSc5	status
Building	Building	k1gInSc1	Building
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
rozhlasové	rozhlasový	k2eAgFnPc1d1	rozhlasová
AM	AM	kA	AM
vysílání	vysílání	k1gNnSc1	vysílání
zde	zde	k6eAd1	zde
začalo	začít	k5eAaPmAgNnS	začít
v	v	k7c6	v
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1931	[number]	k4	1931
<g/>
.	.	kIx.	.
</s>
<s>
Vysílat	vysílat	k5eAaImF	vysílat
začala	začít	k5eAaPmAgFnS	začít
stanice	stanice	k1gFnPc4	stanice
RCA	RCA	kA	RCA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1934	[number]	k4	1934
bylo	být	k5eAaImAgNnS	být
nainstalováno	nainstalován	k2eAgNnSc1d1	nainstalováno
zařízení	zařízení	k1gNnSc1	zařízení
pro	pro	k7c4	pro
FM	FM	kA	FM
vysílání	vysílání	k1gNnSc4	vysílání
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
bylo	být	k5eAaImAgNnS	být
odstraněno	odstranit	k5eAaPmNgNnS	odstranit
a	a	k8xC	a
začala	začít	k5eAaPmAgNnP	začít
zde	zde	k6eAd1	zde
vysílat	vysílat	k5eAaImF	vysílat
první	první	k4xOgFnSc2	první
televize	televize	k1gFnSc2	televize
W2XBS	W2XBS	k1gFnSc2	W2XBS
kanál	kanál	k1gInSc1	kanál
1	[number]	k4	1
(	(	kIx(	(
<g/>
později	pozdě	k6eAd2	pozdě
WNBT	WNBT	kA	WNBT
kanál	kanál	k1gInSc4	kanál
1	[number]	k4	1
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
WNBT-TV	WNBT-TV	k1gFnSc1	WNBT-TV
kanál	kanál	k1gInSc1	kanál
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgFnSc1d1	dnešní
FM	FM	kA	FM
stanice	stanice	k1gFnSc1	stanice
WQHT	WQHT	kA	WQHT
začala	začít	k5eAaPmAgFnS	začít
vysílat	vysílat	k5eAaImF	vysílat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1940	[number]	k4	1940
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
přibývaly	přibývat	k5eAaImAgFnP	přibývat
další	další	k2eAgFnPc4d1	další
stanice	stanice	k1gFnPc4	stanice
a	a	k8xC	a
byla	být	k5eAaImAgFnS	být
potřeba	potřeba	k1gFnSc1	potřeba
výstavba	výstavba	k1gFnSc1	výstavba
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
začala	začít	k5eAaPmAgFnS	začít
využívat	využívat	k5eAaImF	využívat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1953	[number]	k4	1953
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dostavění	dostavění	k1gNnSc6	dostavění
Světového	světový	k2eAgNnSc2d1	světové
obchodního	obchodní	k2eAgNnSc2d1	obchodní
centra	centrum	k1gNnSc2	centrum
se	se	k3xPyFc4	se
na	na	k7c4	na
něj	on	k3xPp3gMnSc4	on
přesunula	přesunout	k5eAaPmAgFnS	přesunout
většina	většina	k1gFnSc1	většina
stanic	stanice	k1gFnPc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Anténa	anténa	k1gFnSc1	anténa
na	na	k7c4	na
Empire	empir	k1gInSc5	empir
State	status	k1gInSc5	status
Building	Building	k1gInSc4	Building
tak	tak	k9	tak
byla	být	k5eAaImAgFnS	být
méně	málo	k6eAd2	málo
využívána	využívat	k5eAaPmNgFnS	využívat
a	a	k8xC	a
mohla	moct	k5eAaImAgFnS	moct
začít	začít	k5eAaPmF	začít
její	její	k3xOp3gFnSc1	její
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2009	[number]	k4	2009
vysílaly	vysílat	k5eAaImAgFnP	vysílat
z	z	k7c2	z
budovy	budova	k1gFnSc2	budova
tyto	tento	k3xDgFnPc4	tento
stanice	stanice	k1gFnPc4	stanice
<g/>
:	:	kIx,	:
TV	TV	kA	TV
<g/>
:	:	kIx,	:
WCBS-TV	WCBS-TV	k1gFnSc1	WCBS-TV
2	[number]	k4	2
<g/>
,	,	kIx,	,
WNBC-TV	WNBC-TV	k1gFnSc1	WNBC-TV
4	[number]	k4	4
<g/>
,	,	kIx,	,
WNYW	WNYW	kA	WNYW
5	[number]	k4	5
<g/>
,	,	kIx,	,
WABC-TV	WABC-TV	k1gFnSc1	WABC-TV
7	[number]	k4	7
<g/>
,	,	kIx,	,
WWOR-TV	WWOR-TV	k1gFnSc1	WWOR-TV
9	[number]	k4	9
Secaucus	Secaucus	k1gMnSc1	Secaucus
<g/>
,	,	kIx,	,
WPIX-TV	WPIX-TV	k1gFnSc1	WPIX-TV
11	[number]	k4	11
<g/>
,	,	kIx,	,
WNET	WNET	kA	WNET
13	[number]	k4	13
Newark	Newark	k1gInSc1	Newark
<g/>
,	,	kIx,	,
WNYE-TV	WNYE-TV	k1gFnSc1	WNYE-TV
25	[number]	k4	25
<g/>
,	,	kIx,	,
WPXN-TV	WPXN-TV	k1gFnSc1	WPXN-TV
31	[number]	k4	31
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
WXTV	WXTV	kA	WXTV
41	[number]	k4	41
Paterson	Patersona	k1gFnPc2	Patersona
<g/>
,	,	kIx,	,
WNJU	WNJU	kA	WNJU
47	[number]	k4	47
Linden	Lindno	k1gNnPc2	Lindno
<g/>
,	,	kIx,	,
a	a	k8xC	a
WFUT-TV	WFUT-TV	k1gMnSc1	WFUT-TV
68	[number]	k4	68
Newark	Newark	k1gInSc1	Newark
FM	FM	kA	FM
<g/>
:	:	kIx,	:
WXRK	WXRK	kA	WXRK
92,3	[number]	k4	92,3
<g/>
,	,	kIx,	,
WPAT-FM	WPAT-FM	k1gFnSc1	WPAT-FM
93.1	[number]	k4	93.1
Paterson	Patersona	k1gFnPc2	Patersona
<g/>
,	,	kIx,	,
WNYC-FM	WNYC-FM	k1gFnSc1	WNYC-FM
93.9	[number]	k4	93.9
<g/>
,	,	kIx,	,
WPLJ	WPLJ	kA	WPLJ
95,5	[number]	k4	95,5
<g/>
,	,	kIx,	,
WXNY	WXNY	kA	WXNY
96,3	[number]	k4	96,3
<g/>
,	,	kIx,	,
WQHT-FM	WQHT-FM	k1gFnSc1	WQHT-FM
97.1	[number]	k4	97.1
<g/>
,	,	kIx,	,
WSKQ-FM	WSKQ-FM	k1gFnSc1	WSKQ-FM
97.9	[number]	k4	97.9
<g/>
,	,	kIx,	,
WRKS-FM	WRKS-FM	k1gFnSc1	WRKS-FM
98.7	[number]	k4	98.7
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
WBAI	WBAI	kA	WBAI
99,5	[number]	k4	99,5
<g/>
,	,	kIx,	,
WHTZ	WHTZ	kA	WHTZ
100,3	[number]	k4	100,3
Newark	Newark	k1gInSc1	Newark
<g/>
,	,	kIx,	,
WCBS-FM	WCBS-FM	k1gFnSc1	WCBS-FM
101.1	[number]	k4	101.1
<g/>
,	,	kIx,	,
WRXP	WRXP	kA	WRXP
101.9	[number]	k4	101.9
<g/>
,	,	kIx,	,
WWFS	WWFS	kA	WWFS
102,7	[number]	k4	102,7
<g/>
,	,	kIx,	,
WKTU	WKTU	kA	WKTU
103,5	[number]	k4	103,5
Lake	Lak	k1gFnSc2	Lak
Success	Successa	k1gFnPc2	Successa
<g/>
,	,	kIx,	,
WAXQ	WAXQ	kA	WAXQ
104,3	[number]	k4	104,3
<g/>
,	,	kIx,	,
WWPR-FM	WWPR-FM	k1gFnSc1	WWPR-FM
105.1	[number]	k4	105.1
<g/>
,	,	kIx,	,
WQXR-FM	WQXR-FM	k1gFnSc1	WQXR-FM
105,9	[number]	k4	105,9
Newark	Newark	k1gInSc1	Newark
<g/>
,	,	kIx,	,
WLTW	WLTW	kA	WLTW
106,7	[number]	k4	106,7
<g/>
,	,	kIx,	,
a	a	k8xC	a
WBLS	WBLS	kA	WBLS
107.5	[number]	k4	107.5
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1978	[number]	k4	1978
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
každoročně	každoročně	k6eAd1	každoročně
koná	konat	k5eAaImIp3nS	konat
závod	závod	k1gInSc4	závod
v	v	k7c6	v
běhu	běh	k1gInSc6	běh
po	po	k7c6	po
schodech	schod	k1gInPc6	schod
z	z	k7c2	z
přízemí	přízemí	k1gNnSc2	přízemí
do	do	k7c2	do
86	[number]	k4	86
<g/>
.	.	kIx.	.
patra	patro	k1gNnSc2	patro
<g/>
.	.	kIx.	.
</s>
<s>
Závodníci	Závodník	k1gMnPc1	Závodník
musí	muset	k5eAaImIp3nP	muset
překonat	překonat	k5eAaPmF	překonat
výškový	výškový	k2eAgInSc4d1	výškový
rozdíl	rozdíl	k1gInSc4	rozdíl
320	[number]	k4	320
m	m	kA	m
a	a	k8xC	a
vyběhnout	vyběhnout	k5eAaPmF	vyběhnout
1576	[number]	k4	1576
schodů	schod	k1gInPc2	schod
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
dosáhli	dosáhnout	k5eAaPmAgMnP	dosáhnout
cíle	cíl	k1gInPc4	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Rekord	rekord	k1gInSc1	rekord
drží	držet	k5eAaImIp3nS	držet
australský	australský	k2eAgMnSc1d1	australský
profesionální	profesionální	k2eAgMnSc1d1	profesionální
cyklista	cyklista	k1gMnSc1	cyklista
Pavel	Pavel	k1gMnSc1	Pavel
Crake	Crake	k1gInSc4	Crake
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
zdolal	zdolat	k5eAaPmAgMnS	zdolat
toto	tento	k3xDgNnSc4	tento
schodiště	schodiště	k1gNnSc4	schodiště
za	za	k7c4	za
9	[number]	k4	9
minut	minuta	k1gFnPc2	minuta
a	a	k8xC	a
33	[number]	k4	33
sekund	sekunda	k1gFnPc2	sekunda
<g/>
.	.	kIx.	.
</s>
<s>
Snad	snad	k9	snad
nejznámější	známý	k2eAgFnSc4d3	nejznámější
roli	role	k1gFnSc4	role
má	mít	k5eAaImIp3nS	mít
mrakodrap	mrakodrap	k1gInSc1	mrakodrap
ve	v	k7c6	v
filmu	film	k1gInSc6	film
King	King	k1gInSc4	King
Kong	Kongo	k1gNnPc2	Kongo
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1933	[number]	k4	1933
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
šplhá	šplhat	k5eAaImIp3nS	šplhat
a	a	k8xC	a
kde	kde	k6eAd1	kde
umírá	umírat	k5eAaImIp3nS	umírat
obrovský	obrovský	k2eAgMnSc1d1	obrovský
op	op	k1gMnSc1	op
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
50	[number]	k4	50
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc2	výročí
tohoto	tento	k3xDgInSc2	tento
filmu	film	k1gInSc2	film
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1983	[number]	k4	1983
byl	být	k5eAaImAgInS	být
obří	obří	k2eAgInSc1d1	obří
nafukovací	nafukovací	k2eAgInSc1d1	nafukovací
King	King	k1gInSc1	King
Kong	Kongo	k1gNnPc2	Kongo
umístěn	umístit	k5eAaPmNgInS	umístit
na	na	k7c4	na
střechu	střecha	k1gFnSc4	střecha
budovy	budova	k1gFnSc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1939	[number]	k4	1939
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Milostný	milostný	k2eAgInSc1d1	milostný
románek	románek	k1gInSc1	románek
(	(	kIx(	(
<g/>
Love	lov	k1gInSc5	lov
Affair	Affaira	k1gFnPc2	Affaira
<g/>
)	)	kIx)	)
posloužila	posloužit	k5eAaPmAgFnS	posloužit
vyhlídková	vyhlídkový	k2eAgFnSc1d1	vyhlídková
terasa	terasa	k1gFnSc1	terasa
jako	jako	k8xS	jako
místo	místo	k1gNnSc1	místo
pro	pro	k7c4	pro
velké	velký	k2eAgNnSc4d1	velké
romantické	romantický	k2eAgNnSc4d1	romantické
setkání	setkání	k1gNnSc4	setkání
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
Samotář	samotář	k1gMnSc1	samotář
v	v	k7c6	v
Seattlu	Seattl	k1gInSc6	Seattl
měla	mít	k5eAaImAgFnS	mít
stejnou	stejný	k2eAgFnSc4d1	stejná
funkci	funkce	k1gFnSc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
Den	dna	k1gFnPc2	dna
nezávislosti	nezávislost	k1gFnSc2	nezávislost
útočí	útočit	k5eAaImIp3nP	útočit
vetřelci	vetřelec	k1gMnPc1	vetřelec
na	na	k7c4	na
New	New	k1gFnSc4	New
York	York	k1gInSc1	York
a	a	k8xC	a
budova	budova	k1gFnSc1	budova
je	být	k5eAaImIp3nS	být
centrem	centrum	k1gNnSc7	centrum
útoku	útok	k1gInSc2	útok
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
je	být	k5eAaImIp3nS	být
po	po	k7c6	po
zásahu	zásah	k1gInSc6	zásah
zbraní	zbraň	k1gFnPc2	zbraň
roztříštěna	roztříštěn	k2eAgFnSc1d1	roztříštěna
a	a	k8xC	a
touto	tento	k3xDgFnSc7	tento
ránou	rána	k1gFnSc7	rána
mimozemšťané	mimozemšťan	k1gMnPc1	mimozemšťan
srovnají	srovnat	k5eAaPmIp3nP	srovnat
skoro	skoro	k6eAd1	skoro
celé	celý	k2eAgNnSc4d1	celé
město	město	k1gNnSc4	město
se	s	k7c7	s
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
<s>
Kanál	kanál	k1gInSc1	kanál
Discovery	Discovera	k1gFnSc2	Discovera
Channel	Channel	k1gMnSc1	Channel
vyzkoušel	vyzkoušet	k5eAaPmAgMnS	vyzkoušet
v	v	k7c6	v
pořadu	pořad	k1gInSc6	pořad
Bořiči	bořič	k1gMnPc1	bořič
mýtů	mýtus	k1gInPc2	mýtus
pravdivost	pravdivost	k1gFnSc4	pravdivost
mýtu	mýtus	k1gInSc2	mýtus
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
hodíme	hodit	k5eAaImIp1nP	hodit
z	z	k7c2	z
vrcholu	vrchol	k1gInSc2	vrchol
budovy	budova	k1gFnSc2	budova
minci	mince	k1gFnSc4	mince
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
dole	dole	k6eAd1	dole
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
někoho	někdo	k3yInSc4	někdo
zabít	zabít	k5eAaPmF	zabít
nebo	nebo	k8xC	nebo
udělat	udělat	k5eAaPmF	udělat
díru	díra	k1gFnSc4	díra
v	v	k7c6	v
chodníku	chodník	k1gInSc6	chodník
<g/>
.	.	kIx.	.
</s>
<s>
Zkouška	zkouška	k1gFnSc1	zkouška
ukázala	ukázat	k5eAaPmAgFnS	ukázat
že	že	k8xS	že
padající	padající	k2eAgFnSc1d1	padající
mince	mince	k1gFnSc1	mince
má	mít	k5eAaImIp3nS	mít
rychlost	rychlost	k1gFnSc4	rychlost
105	[number]	k4	105
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
není	být	k5eNaImIp3nS	být
rychlost	rychlost	k1gFnSc1	rychlost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgFnS	moct
poškodit	poškodit	k5eAaPmF	poškodit
chodník	chodník	k1gInSc4	chodník
nebo	nebo	k8xC	nebo
způsobit	způsobit	k5eAaPmF	způsobit
smrtelné	smrtelný	k2eAgNnSc4d1	smrtelné
zranění	zranění	k1gNnSc4	zranění
<g/>
.	.	kIx.	.
</s>
<s>
Mýtus	mýtus	k1gInSc1	mýtus
tak	tak	k9	tak
byl	být	k5eAaImAgInS	být
vyvrácen	vyvrácen	k2eAgInSc1d1	vyvrácen
<g/>
.	.	kIx.	.
</s>
<s>
Mrakodrap	mrakodrap	k1gInSc1	mrakodrap
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
rovněž	rovněž	k9	rovněž
ústředním	ústřední	k2eAgInSc7d1	ústřední
bodem	bod	k1gInSc7	bod
jednoho	jeden	k4xCgMnSc4	jeden
dílu	dílo	k1gNnSc3	dílo
britského	britský	k2eAgNnSc2d1	Britské
sci-fi	scii	k1gNnSc2	sci-fi
seriálu	seriál	k1gInSc2	seriál
Doctor	Doctor	k1gMnSc1	Doctor
Who	Who	k1gMnSc1	Who
Mrakodrap	mrakodrap	k1gInSc4	mrakodrap
byl	být	k5eAaImAgMnS	být
rovněž	rovněž	k9	rovněž
natočen	natočen	k2eAgMnSc1d1	natočen
Andy	Anda	k1gFnPc4	Anda
Warholem	Warhol	k1gInSc7	Warhol
v	v	k7c6	v
osmihodinovém	osmihodinový	k2eAgInSc6d1	osmihodinový
filmu	film	k1gInSc6	film
Empire	empir	k1gInSc5	empir
<g/>
.	.	kIx.	.
</s>
<s>
Objevil	objevit	k5eAaPmAgInS	objevit
se	se	k3xPyFc4	se
také	také	k9	také
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Percy	Perca	k1gFnSc2	Perca
Jackson-	Jackson-	k1gMnPc2	Jackson-
Zloděj	zloděj	k1gMnSc1	zloděj
blesku	blesk	k1gInSc2	blesk
jako	jako	k8xC	jako
vstup	vstup	k1gInSc4	vstup
na	na	k7c4	na
Olymp	Olymp	k1gInSc4	Olymp
v	v	k7c6	v
šestistém	šestistý	k2eAgNnSc6d1	šestistý
patře	patro	k1gNnSc6	patro
<g/>
.	.	kIx.	.
</s>
<s>
Empire	empir	k1gInSc5	empir
State	status	k1gInSc5	status
Building	Building	k1gInSc4	Building
se	se	k3xPyFc4	se
také	také	k9	také
objevila	objevit	k5eAaPmAgFnS	objevit
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
How	How	k1gFnSc2	How
I	i	k9	i
Met	met	k1gInSc1	met
Your	Youra	k1gFnPc2	Youra
Mother	Mothra	k1gFnPc2	Mothra
(	(	kIx(	(
<g/>
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Jak	jak	k6eAd1	jak
jsem	být	k5eAaImIp1nS	být
poznal	poznat	k5eAaPmAgInS	poznat
vaši	váš	k3xOp2gFnSc4	váš
matku	matka	k1gFnSc4	matka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
David	David	k1gMnSc1	David
Macaulay	Macaulaa	k1gFnSc2	Macaulaa
popisuje	popisovat	k5eAaImIp3nS	popisovat
v	v	k7c6	v
ilustrované	ilustrovaný	k2eAgFnSc6d1	ilustrovaná
knize	kniha	k1gFnSc6	kniha
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
si	se	k3xPyFc3	se
budovu	budova	k1gFnSc4	budova
koupil	koupit	k5eAaPmAgMnS	koupit
miliardář	miliardář	k1gMnSc1	miliardář
z	z	k7c2	z
blízkého	blízký	k2eAgInSc2d1	blízký
východu	východ	k1gInSc2	východ
a	a	k8xC	a
kousek	kousek	k1gInSc4	kousek
po	po	k7c6	po
kousku	kousek	k1gInSc6	kousek
ji	on	k3xPp3gFnSc4	on
rozebírá	rozebírat	k5eAaImIp3nS	rozebírat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
mohl	moct	k5eAaImAgMnS	moct
převézt	převézt	k5eAaPmF	převézt
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
vlasti	vlast	k1gFnSc2	vlast
a	a	k8xC	a
tam	tam	k6eAd1	tam
ji	on	k3xPp3gFnSc4	on
znovu	znovu	k6eAd1	znovu
postavit	postavit	k5eAaPmF	postavit
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
se	se	k3xPyFc4	se
objevila	objevit	k5eAaPmAgFnS	objevit
i	i	k9	i
ve	v	k7c6	v
známé	známý	k2eAgFnSc6d1	známá
počítačové	počítačový	k2eAgFnSc6d1	počítačová
hře	hra	k1gFnSc6	hra
Grand	grand	k1gMnSc1	grand
Theft	Theft	k1gMnSc1	Theft
Auto	auto	k1gNnSc4	auto
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
tam	tam	k6eAd1	tam
vypadá	vypadat	k5eAaPmIp3nS	vypadat
vzhledově	vzhledově	k6eAd1	vzhledově
stejně	stejně	k6eAd1	stejně
<g/>
,	,	kIx,	,
jen	jen	k9	jen
má	mít	k5eAaImIp3nS	mít
jiné	jiný	k2eAgNnSc4d1	jiné
jméno	jméno	k1gNnSc4	jméno
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
Rotterdam	Rotterdam	k1gInSc4	Rotterdam
Tower	Towra	k1gFnPc2	Towra
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
vidění	vidění	k1gNnSc3	vidění
je	být	k5eAaImIp3nS	být
i	i	k9	i
ve	v	k7c6	v
hře	hra	k1gFnSc6	hra
Mafia	Mafium	k1gNnSc2	Mafium
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Alitalia	Alitalia	k1gFnSc1	Alitalia
<g/>
,	,	kIx,	,
Croatian	Croatian	k1gMnSc1	Croatian
National	National	k1gFnSc2	National
Tourist	Tourist	k1gMnSc1	Tourist
Board	Board	k1gMnSc1	Board
<g/>
,	,	kIx,	,
Filipino	Filipin	k2eAgNnSc1d1	Filipino
Reporter	Reporter	k1gMnSc1	Reporter
<g/>
,	,	kIx,	,
Human	Human	k1gMnSc1	Human
Rights	Rightsa	k1gFnPc2	Rightsa
Watch	Watch	k1gMnSc1	Watch
<g/>
,	,	kIx,	,
Polish	Polish	k1gMnSc1	Polish
Cultural	Cultural	k1gMnSc1	Cultural
Institute	institut	k1gInSc5	institut
in	in	k?	in
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
Senegal	Senegal	k1gInSc1	Senegal
Tourist	Tourist	k1gInSc1	Tourist
Office	Office	kA	Office
<g/>
,	,	kIx,	,
TAROM	TAROM	kA	TAROM
<g/>
,	,	kIx,	,
The	The	k1gMnSc1	The
King	King	k1gMnSc1	King
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
College	College	k1gFnSc7	College
China	China	k1gFnSc1	China
National	National	k1gMnSc1	National
Tourist	Tourist	k1gMnSc1	Tourist
<g/>
,	,	kIx,	,
Zeman	Zeman	k1gMnSc1	Zeman
tour	tour	k1gMnSc1	tour
</s>
