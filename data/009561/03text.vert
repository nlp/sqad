<p>
<s>
Kos	Kos	k1gMnSc1	Kos
černý	černý	k1gMnSc1	černý
(	(	kIx(	(
<g/>
Turdus	Turdus	k1gMnSc1	Turdus
merula	merul	k1gMnSc2	merul
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
pták	pták	k1gMnSc1	pták
žijící	žijící	k2eAgMnSc1d1	žijící
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
v	v	k7c6	v
jižní	jižní	k2eAgFnSc6d1	jižní
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
byl	být	k5eAaImAgInS	být
introdukován	introdukovat	k5eAaImNgInS	introdukovat
i	i	k9	i
do	do	k7c2	do
Austrálie	Austrálie	k1gFnSc2	Austrálie
a	a	k8xC	a
na	na	k7c4	na
Nový	nový	k2eAgInSc4d1	nový
Zéland	Zéland	k1gInSc4	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
přizpůsobivosti	přizpůsobivost	k1gFnSc3	přizpůsobivost
se	se	k3xPyFc4	se
adaptoval	adaptovat	k5eAaBmAgMnS	adaptovat
na	na	k7c4	na
život	život	k1gInSc4	život
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
úspěšně	úspěšně	k6eAd1	úspěšně
žije	žít	k5eAaImIp3nS	žít
a	a	k8xC	a
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
i	i	k9	i
v	v	k7c6	v
těsném	těsný	k2eAgNnSc6d1	těsné
sousedství	sousedství	k1gNnSc6	sousedství
lidských	lidský	k2eAgNnPc2d1	lidské
sídel	sídlo	k1gNnPc2	sídlo
<g/>
.	.	kIx.	.
</s>
<s>
Samci	Samek	k1gMnPc1	Samek
jsou	být	k5eAaImIp3nP	být
nepřehlédnutelní	přehlédnutelný	k2eNgMnPc1d1	nepřehlédnutelný
svým	svůj	k3xOyFgNnSc7	svůj
charakteristickým	charakteristický	k2eAgNnSc7d1	charakteristické
černým	černý	k2eAgNnSc7d1	černé
peřím	peří	k1gNnSc7	peří
a	a	k8xC	a
žlutým	žlutý	k2eAgInSc7d1	žlutý
až	až	k8xS	až
oranžovým	oranžový	k2eAgInSc7d1	oranžový
zobákem	zobák	k1gInSc7	zobák
<g/>
,	,	kIx,	,
upozorňují	upozorňovat	k5eAaImIp3nP	upozorňovat
na	na	k7c4	na
sebe	sebe	k3xPyFc4	sebe
také	také	k6eAd1	také
melodickým	melodický	k2eAgInSc7d1	melodický
zpěvem	zpěv	k1gInSc7	zpěv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Délka	délka	k1gFnSc1	délka
těla	tělo	k1gNnSc2	tělo
<g/>
:	:	kIx,	:
23	[number]	k4	23
<g/>
–	–	k?	–
<g/>
29	[number]	k4	29
cm	cm	kA	cm
</s>
</p>
<p>
<s>
Hmotnost	hmotnost	k1gFnSc1	hmotnost
těla	tělo	k1gNnSc2	tělo
<g/>
:	:	kIx,	:
75	[number]	k4	75
<g/>
–	–	k?	–
<g/>
135	[number]	k4	135
g	g	kA	g
</s>
</p>
<p>
<s>
Rozpětí	rozpětí	k1gNnSc1	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
<g/>
:	:	kIx,	:
34	[number]	k4	34
<g/>
–	–	k?	–
<g/>
39	[number]	k4	39
cmKos	cmKos	k1gInSc1	cmKos
černý	černý	k2eAgInSc1d1	černý
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velký	velký	k2eAgMnSc1d1	velký
pták	pták	k1gMnSc1	pták
<g/>
,	,	kIx,	,
o	o	k7c4	o
něco	něco	k3yInSc4	něco
menší	malý	k2eAgFnSc1d2	menší
než	než	k8xS	než
hrdlička	hrdlička	k1gFnSc1	hrdlička
zahradní	zahradní	k2eAgFnSc1d1	zahradní
<g/>
.	.	kIx.	.
</s>
<s>
Dospělý	dospělý	k2eAgMnSc1d1	dospělý
samec	samec	k1gMnSc1	samec
je	být	k5eAaImIp3nS	být
matně	matně	k6eAd1	matně
černý	černý	k2eAgInSc1d1	černý
s	s	k7c7	s
oranžově	oranžově	k6eAd1	oranžově
žlutým	žlutý	k2eAgInSc7d1	žlutý
zobákem	zobák	k1gInSc7	zobák
a	a	k8xC	a
žlutým	žlutý	k2eAgInSc7d1	žlutý
kroužkem	kroužek	k1gInSc7	kroužek
okolo	okolo	k7c2	okolo
očí	oko	k1gNnPc2	oko
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
je	být	k5eAaImIp3nS	být
hnědavá	hnědavý	k2eAgFnSc1d1	hnědavá
s	s	k7c7	s
bělavějším	bělavý	k2eAgNnSc7d2	bělavý
hrdlem	hrdlo	k1gNnSc7	hrdlo
a	a	k8xC	a
nezřetelně	zřetelně	k6eNd1	zřetelně
skvrnitou	skvrnitý	k2eAgFnSc7d1	skvrnitá
hrudí	hruď	k1gFnSc7	hruď
<g/>
,	,	kIx,	,
nohy	noha	k1gFnPc1	noha
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
černé	černý	k2eAgInPc1d1	černý
<g/>
,	,	kIx,	,
zobák	zobák	k1gInSc1	zobák
má	mít	k5eAaImIp3nS	mít
tmavý	tmavý	k2eAgMnSc1d1	tmavý
<g/>
,	,	kIx,	,
jen	jen	k9	jen
částečně	částečně	k6eAd1	částečně
oranžový	oranžový	k2eAgMnSc1d1	oranžový
<g/>
.	.	kIx.	.
</s>
<s>
Mláďata	mládě	k1gNnPc1	mládě
jsou	být	k5eAaImIp3nP	být
podobná	podobný	k2eAgFnSc1d1	podobná
samici	samice	k1gFnSc3	samice
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
světlejší	světlý	k2eAgMnPc1d2	světlejší
a	a	k8xC	a
mohou	moct	k5eAaImIp3nP	moct
mít	mít	k5eAaImF	mít
hnědě	hnědě	k6eAd1	hnědě
tečkovanou	tečkovaný	k2eAgFnSc4d1	tečkovaná
hruď	hruď	k1gFnSc4	hruď
a	a	k8xC	a
světlé	světlý	k2eAgInPc4d1	světlý
podélné	podélný	k2eAgInPc4d1	podélný
proužky	proužek	k1gInPc4	proužek
na	na	k7c6	na
zádech	záda	k1gNnPc6	záda
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
ani	ani	k8xC	ani
mláďata	mládě	k1gNnPc1	mládě
nemají	mít	k5eNaImIp3nP	mít
oční	oční	k2eAgInSc4d1	oční
kroužek	kroužek	k1gInSc4	kroužek
<g/>
.	.	kIx.	.
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
<g/>
,	,	kIx,	,
roční	roční	k2eAgMnPc1d1	roční
samci	samec	k1gMnPc1	samec
nemají	mít	k5eNaImIp3nP	mít
ještě	ještě	k9	ještě
vybarvený	vybarvený	k2eAgInSc4d1	vybarvený
zobák	zobák	k1gInSc4	zobák
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
černavý	černavý	k2eAgInSc1d1	černavý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
objevují	objevovat	k5eAaImIp3nP	objevovat
i	i	k9	i
jedinci	jedinec	k1gMnPc1	jedinec
s	s	k7c7	s
větším	veliký	k2eAgInSc7d2	veliký
či	či	k8xC	či
menším	malý	k2eAgInSc7d2	menší
podílem	podíl	k1gInSc7	podíl
bílého	bílý	k2eAgNnSc2d1	bílé
peří	peří	k1gNnSc2	peří
(	(	kIx(	(
<g/>
leucismus	leucismus	k1gInSc1	leucismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzácnější	vzácný	k2eAgFnSc1d2	vzácnější
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
ne	ne	k9	ne
neznámí	známý	k2eNgMnPc1d1	neznámý
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
úplně	úplně	k6eAd1	úplně
bílí	bílý	k2eAgMnPc1d1	bílý
ptáci	pták	k1gMnPc1	pták
s	s	k7c7	s
červenýma	červený	k2eAgNnPc7d1	červené
očima	oko	k1gNnPc7	oko
(	(	kIx(	(
<g/>
albinismus	albinismus	k1gInSc1	albinismus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozšíření	rozšíření	k1gNnPc1	rozšíření
a	a	k8xC	a
stanoviště	stanoviště	k1gNnPc1	stanoviště
==	==	k?	==
</s>
</p>
<p>
<s>
Původní	původní	k2eAgInSc1d1	původní
areál	areál	k1gInSc1	areál
kosa	kos	k1gMnSc2	kos
černého	černé	k1gNnSc2	černé
zahrnoval	zahrnovat	k5eAaImAgMnS	zahrnovat
celou	celý	k2eAgFnSc4d1	celá
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
severní	severní	k2eAgFnSc4d1	severní
Afriku	Afrika	k1gFnSc4	Afrika
a	a	k8xC	a
jižní	jižní	k2eAgFnSc6d1	jižní
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
během	během	k7c2	během
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byl	být	k5eAaImAgMnS	být
introdukován	introdukován	k2eAgMnSc1d1	introdukován
i	i	k9	i
do	do	k7c2	do
Austrálie	Austrálie	k1gFnSc2	Austrálie
a	a	k8xC	a
na	na	k7c4	na
Nový	nový	k2eAgInSc4d1	nový
Zéland	Zéland	k1gInSc4	Zéland
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
rychle	rychle	k6eAd1	rychle
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
a	a	k8xC	a
na	na	k7c6	na
některých	některý	k3yIgNnPc6	některý
místech	místo	k1gNnPc6	místo
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
škůdce	škůdce	k1gMnSc4	škůdce
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
konkuruje	konkurovat	k5eAaImIp3nS	konkurovat
místním	místní	k2eAgInPc3d1	místní
druhům	druh	k1gInPc3	druh
ptáků	pták	k1gMnPc2	pták
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kos	Kos	k1gMnSc1	Kos
černý	černý	k1gMnSc1	černý
je	být	k5eAaImIp3nS	být
pták	pták	k1gMnSc1	pták
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
před	před	k7c7	před
svým	svůj	k3xOyFgNnSc7	svůj
přistěhováním	přistěhování	k1gNnSc7	přistěhování
se	se	k3xPyFc4	se
do	do	k7c2	do
blízkosti	blízkost	k1gFnSc2	blízkost
člověka	člověk	k1gMnSc2	člověk
žil	žíla	k1gFnPc2	žíla
a	a	k8xC	a
hnízdil	hnízdit	k5eAaImAgInS	hnízdit
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
přízemním	přízemní	k2eAgNnSc6d1	přízemní
patru	patro	k1gNnSc6	patro
lesa	les	k1gInSc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městech	město	k1gNnPc6	město
a	a	k8xC	a
vesnicích	vesnice	k1gFnPc6	vesnice
vyhledává	vyhledávat	k5eAaImIp3nS	vyhledávat
podobné	podobný	k2eAgNnSc4d1	podobné
prostředí	prostředí	k1gNnSc4	prostředí
<g/>
,	,	kIx,	,
sady	sada	k1gFnPc4	sada
<g/>
,	,	kIx,	,
vinice	vinice	k1gFnPc4	vinice
<g/>
,	,	kIx,	,
zahrady	zahrada	k1gFnPc4	zahrada
<g/>
,	,	kIx,	,
parky	park	k1gInPc4	park
nebo	nebo	k8xC	nebo
hřbitovy	hřbitov	k1gInPc4	hřbitov
<g/>
,	,	kIx,	,
obecně	obecně	k6eAd1	obecně
je	být	k5eAaImIp3nS	být
kos	kos	k1gMnSc1	kos
velmi	velmi	k6eAd1	velmi
přizpůsobivý	přizpůsobivý	k2eAgMnSc1d1	přizpůsobivý
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
schopen	schopen	k2eAgMnSc1d1	schopen
žít	žít	k5eAaImF	žít
a	a	k8xC	a
úspěšně	úspěšně	k6eAd1	úspěšně
vyvádět	vyvádět	k5eAaImF	vyvádět
mláďata	mládě	k1gNnPc4	mládě
všude	všude	k6eAd1	všude
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nachází	nacházet	k5eAaImIp3nS	nacházet
alespoň	alespoň	k9	alespoň
nějakou	nějaký	k3yIgFnSc4	nějaký
potravu	potrava	k1gFnSc4	potrava
a	a	k8xC	a
úkryt	úkryt	k1gInSc4	úkryt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1959	[number]	k4	1959
byl	být	k5eAaImAgMnS	být
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
poprvé	poprvé	k6eAd1	poprvé
zjištěn	zjistit	k5eAaPmNgInS	zjistit
virus	virus	k1gInSc1	virus
Usutu	Usut	k1gInSc2	Usut
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1996	[number]	k4	1996
rozšířil	rozšířit	k5eAaPmAgInS	rozšířit
do	do	k7c2	do
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
způsobil	způsobit	k5eAaPmAgMnS	způsobit
hromadné	hromadný	k2eAgInPc4d1	hromadný
úhyny	úhyn	k1gInPc4	úhyn
kosů	kos	k1gMnPc2	kos
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
byl	být	k5eAaImAgMnS	být
uhynulý	uhynulý	k2eAgMnSc1d1	uhynulý
kos	kos	k1gMnSc1	kos
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
virem	vir	k1gInSc7	vir
nalezen	nalezen	k2eAgMnSc1d1	nalezen
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
se	se	k3xPyFc4	se
virus	virus	k1gInSc1	virus
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
a	a	k8xC	a
uhynulo	uhynout	k5eAaPmAgNnS	uhynout
několik	několik	k4yIc4	několik
desítek	desítka	k1gFnPc2	desítka
procent	procento	k1gNnPc2	procento
kosů	kos	k1gMnPc2	kos
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Biologie	biologie	k1gFnSc2	biologie
==	==	k?	==
</s>
</p>
<p>
<s>
Po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
Evropě	Evropa	k1gFnSc6	Evropa
je	být	k5eAaImIp3nS	být
populace	populace	k1gFnSc1	populace
kosa	kos	k1gMnSc2	kos
černého	černý	k2eAgInSc2d1	černý
rozdělena	rozdělen	k2eAgNnPc4d1	rozděleno
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
oddělené	oddělený	k2eAgFnPc4d1	oddělená
subpopulace	subpopulace	k1gFnPc4	subpopulace
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
způsobem	způsob	k1gInSc7	způsob
života	život	k1gInSc2	život
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
člověku	člověk	k1gMnSc3	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgFnSc1d1	původní
<g/>
,	,	kIx,	,
lesní	lesní	k2eAgFnSc1d1	lesní
populace	populace	k1gFnSc1	populace
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
žije	žít	k5eAaImIp3nS	žít
skrytým	skrytý	k2eAgInSc7d1	skrytý
způsobem	způsob	k1gInSc7	způsob
života	život	k1gInSc2	život
v	v	k7c6	v
jehličnatých	jehličnatý	k2eAgInPc6d1	jehličnatý
i	i	k8xC	i
listnatých	listnatý	k2eAgInPc6d1	listnatý
lesích	les	k1gInPc6	les
<g/>
,	,	kIx,	,
populace	populace	k1gFnSc1	populace
městská	městský	k2eAgFnSc1d1	městská
<g/>
,	,	kIx,	,
urbánní	urbánní	k2eAgFnSc1d1	urbánní
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
přizpůsobila	přizpůsobit	k5eAaPmAgFnS	přizpůsobit
životu	život	k1gInSc3	život
v	v	k7c6	v
intravilánech	intravilán	k1gInPc6	intravilán
obcí	obec	k1gFnPc2	obec
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
početně	početně	k6eAd1	početně
převyšuje	převyšovat	k5eAaImIp3nS	převyšovat
lesní	lesní	k2eAgFnPc4d1	lesní
kosy	kosa	k1gFnPc4	kosa
<g/>
.	.	kIx.	.
</s>
<s>
Městští	městský	k2eAgMnPc1d1	městský
kosové	kos	k1gMnPc1	kos
jsou	být	k5eAaImIp3nP	být
právě	právě	k9	právě
ti	ten	k3xDgMnPc1	ten
<g/>
,	,	kIx,	,
se	s	k7c7	s
kterými	který	k3yQgInPc7	který
člověk	člověk	k1gMnSc1	člověk
běžně	běžně	k6eAd1	běžně
přichází	přicházet	k5eAaImIp3nS	přicházet
do	do	k7c2	do
kontaktu	kontakt	k1gInSc2	kontakt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Lesní	lesní	k2eAgMnPc1d1	lesní
kosi	kos	k1gMnPc1	kos
===	===	k?	===
</s>
</p>
<p>
<s>
Lesní	lesní	k2eAgMnPc1d1	lesní
kosi	kos	k1gMnPc1	kos
černí	čerň	k1gFnPc2	čerň
jsou	být	k5eAaImIp3nP	být
plaší	plachý	k2eAgMnPc1d1	plachý
ptáci	pták	k1gMnPc1	pták
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
obývají	obývat	k5eAaImIp3nP	obývat
přízemní	přízemní	k2eAgNnSc1d1	přízemní
patro	patro	k1gNnSc1	patro
lesa	les	k1gInSc2	les
<g/>
.	.	kIx.	.
</s>
<s>
Potravu	potrava	k1gFnSc4	potrava
si	se	k3xPyFc3	se
hledají	hledat	k5eAaImIp3nP	hledat
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
a	a	k8xC	a
živí	živit	k5eAaImIp3nP	živit
se	se	k3xPyFc4	se
hlavně	hlavně	k6eAd1	hlavně
hmyzem	hmyz	k1gInSc7	hmyz
<g/>
,	,	kIx,	,
žížalami	žížala	k1gFnPc7	žížala
nebo	nebo	k8xC	nebo
plži	plž	k1gMnPc7	plž
<g/>
,	,	kIx,	,
a	a	k8xC	a
drobnými	drobný	k2eAgInPc7d1	drobný
lesními	lesní	k2eAgInPc7d1	lesní
plody	plod	k1gInPc7	plod
(	(	kIx(	(
<g/>
jahody	jahoda	k1gFnPc4	jahoda
<g/>
,	,	kIx,	,
borůvky	borůvka	k1gFnPc4	borůvka
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Lesní	lesní	k2eAgFnSc1d1	lesní
populace	populace	k1gFnSc1	populace
kosů	kos	k1gMnPc2	kos
je	být	k5eAaImIp3nS	být
tažná	tažný	k2eAgFnSc1d1	tažná
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Městští	městský	k2eAgMnPc1d1	městský
kosi	kos	k1gMnPc1	kos
===	===	k?	===
</s>
</p>
<p>
<s>
Synurbanizace	Synurbanizace	k1gFnSc1	Synurbanizace
kosa	kos	k1gMnSc2	kos
černého	černý	k1gMnSc2	černý
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
Evropě	Evropa	k1gFnSc6	Evropa
už	už	k6eAd1	už
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
19	[number]	k4	19
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
<g/>
,	,	kIx,	,
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
pronikají	pronikat	k5eAaImIp3nP	pronikat
první	první	k4xOgMnPc1	první
kosové	kos	k1gMnPc1	kos
do	do	k7c2	do
měst	město	k1gNnPc2	město
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
stol.	stol.	k?	stol.
Městští	městský	k2eAgMnPc1d1	městský
kosi	kos	k1gMnPc1	kos
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
lesními	lesní	k2eAgMnPc7d1	lesní
příbuznými	příbuzný	k1gMnPc7	příbuzný
mnohem	mnohem	k6eAd1	mnohem
drzejší	drzý	k2eAgInSc1d2	drzejší
a	a	k8xC	a
i	i	k9	i
když	když	k8xS	když
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
opatrní	opatrný	k2eAgMnPc1d1	opatrný
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
snáší	snášet	k5eAaImIp3nS	snášet
ruch	ruch	k1gInSc1	ruch
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
jejich	jejich	k3xOp3gInSc2	jejich
jídelníčku	jídelníček	k1gInSc2	jídelníček
pak	pak	k6eAd1	pak
kromě	kromě	k7c2	kromě
bezobratlých	bezobratlý	k2eAgMnPc2d1	bezobratlý
živočichů	živočich	k1gMnPc2	živočich
a	a	k8xC	a
bobulí	bobule	k1gFnPc2	bobule
patří	patřit	k5eAaImIp3nS	patřit
také	také	k9	také
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
najdou	najít	k5eAaPmIp3nP	najít
na	na	k7c6	na
ulicích	ulice	k1gFnPc6	ulice
<g/>
,	,	kIx,	,
dvorcích	dvorec	k1gInPc6	dvorec
nebo	nebo	k8xC	nebo
sadech	sad	k1gInPc6	sad
<g/>
,	,	kIx,	,
snadno	snadno	k6eAd1	snadno
také	také	k9	také
obsazují	obsazovat	k5eAaImIp3nP	obsazovat
krmítka	krmítko	k1gNnPc4	krmítko
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
odhání	odhánět	k5eAaImIp3nS	odhánět
jiné	jiný	k2eAgMnPc4d1	jiný
ptáky	pták	k1gMnPc4	pták
<g/>
.	.	kIx.	.
</s>
<s>
Městský	městský	k2eAgMnSc1d1	městský
kos	kos	k1gMnSc1	kos
se	se	k3xPyFc4	se
stává	stávat	k5eAaImIp3nS	stávat
všežravcem	všežravec	k1gMnSc7	všežravec
a	a	k8xC	a
díky	díky	k7c3	díky
široké	široký	k2eAgFnSc3d1	široká
potravní	potravní	k2eAgFnSc3d1	potravní
nabídce	nabídka	k1gFnSc3	nabídka
má	mít	k5eAaImIp3nS	mít
větší	veliký	k2eAgFnSc4d2	veliký
populační	populační	k2eAgFnSc4d1	populační
hustotu	hustota	k1gFnSc4	hustota
než	než	k8xS	než
lesní	lesní	k2eAgFnSc2d1	lesní
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Nabídka	nabídka	k1gFnSc1	nabídka
potravy	potrava	k1gFnSc2	potrava
navíc	navíc	k6eAd1	navíc
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
i	i	k9	i
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
příznivějším	příznivý	k2eAgNnSc7d2	příznivější
mikroklimatem	mikroklima	k1gNnSc7	mikroklima
města	město	k1gNnSc2	město
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
kosům	kos	k1gMnPc3	kos
přezimovat	přezimovat	k5eAaBmF	přezimovat
<g/>
.	.	kIx.	.
</s>
<s>
Městský	městský	k2eAgMnSc1d1	městský
kos	kos	k1gMnSc1	kos
je	být	k5eAaImIp3nS	být
částečně	částečně	k6eAd1	částečně
tažný	tažný	k2eAgMnSc1d1	tažný
pták	pták	k1gMnSc1	pták
<g/>
,	,	kIx,	,
častěji	často	k6eAd2	často
migrují	migrovat	k5eAaImIp3nP	migrovat
mladí	mladý	k2eAgMnPc1d1	mladý
ptáci	pták	k1gMnPc1	pták
a	a	k8xC	a
samice	samice	k1gFnPc1	samice
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
zjištěno	zjistit	k5eAaPmNgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
osvětlení	osvětlení	k1gNnSc1	osvětlení
ve	v	k7c6	v
městech	město	k1gNnPc6	město
urychluje	urychlovat	k5eAaImIp3nS	urychlovat
sexuální	sexuální	k2eAgFnSc4d1	sexuální
vyspělost	vyspělost	k1gFnSc4	vyspělost
kosů	kos	k1gMnPc2	kos
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
až	až	k9	až
o	o	k7c4	o
26	[number]	k4	26
dnů	den	k1gInPc2	den
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
jejich	jejich	k3xOp3gMnPc1	jejich
venkovští	venkovský	k2eAgMnPc1d1	venkovský
kolegové	kolega	k1gMnPc1	kolega
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Hnízdění	hnízdění	k1gNnSc2	hnízdění
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
kos	kos	k1gMnSc1	kos
černý	černý	k2eAgMnSc1d1	černý
v	v	k7c6	v
dubnu	duben	k1gInSc6	duben
až	až	k8xS	až
červenci	červenec	k1gInSc6	červenec
dvakrát	dvakrát	k6eAd1	dvakrát
až	až	k9	až
čtyřikrát	čtyřikrát	k6eAd1	čtyřikrát
ročně	ročně	k6eAd1	ročně
<g/>
,	,	kIx,	,
městští	městský	k2eAgMnPc1d1	městský
kosi	kos	k1gMnPc1	kos
pak	pak	k6eAd1	pak
hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
o	o	k7c4	o
10	[number]	k4	10
dnů	den	k1gInPc2	den
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
lesní	lesní	k2eAgMnPc1d1	lesní
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdo	hnízdo	k1gNnSc4	hnízdo
je	být	k5eAaImIp3nS	být
hluboká	hluboký	k2eAgFnSc1d1	hluboká
<g/>
,	,	kIx,	,
polokulovitá	polokulovitý	k2eAgFnSc1d1	polokulovitá
miska	miska	k1gFnSc1	miska
vybudovaná	vybudovaný	k2eAgFnSc1d1	vybudovaná
ze	z	k7c2	z
suchých	suchý	k2eAgNnPc2d1	suché
stébel	stéblo	k1gNnPc2	stéblo
a	a	k8xC	a
kořínků	kořínek	k1gInPc2	kořínek
slepených	slepený	k2eAgInPc2d1	slepený
hlínou	hlína	k1gFnSc7	hlína
<g/>
,	,	kIx,	,
lesní	lesní	k2eAgMnPc1d1	lesní
kosi	kos	k1gMnPc1	kos
je	on	k3xPp3gMnPc4	on
umísťují	umísťovat	k5eAaImIp3nP	umísťovat
do	do	k7c2	do
vidlic	vidlice	k1gFnPc2	vidlice
stromů	strom	k1gInPc2	strom
a	a	k8xC	a
keřů	keř	k1gInPc2	keř
<g/>
,	,	kIx,	,
městští	městský	k2eAgMnPc1d1	městský
kosi	kos	k1gMnPc1	kos
hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
i	i	k9	i
na	na	k7c6	na
budovách	budova	k1gFnPc6	budova
(	(	kIx(	(
<g/>
balkóny	balkón	k1gInPc1	balkón
<g/>
,	,	kIx,	,
verandy	veranda	k1gFnPc1	veranda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdo	hnízdo	k1gNnSc1	hnízdo
nikdy	nikdy	k6eAd1	nikdy
nebývá	bývat	k5eNaImIp3nS	bývat
umístěno	umístit	k5eAaPmNgNnS	umístit
vysoko	vysoko	k6eAd1	vysoko
nad	nad	k7c7	nad
zemí	zem	k1gFnSc7	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Hnízdní	hnízdní	k2eAgInSc1d1	hnízdní
revír	revír	k1gInSc1	revír
je	být	k5eAaImIp3nS	být
obsazen	obsadit	k5eAaPmNgInS	obsadit
jediným	jediný	k2eAgInSc7d1	jediný
párem	pár	k1gInSc7	pár
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
území	území	k1gNnSc6	území
nestrpí	strpět	k5eNaPmIp3nS	strpět
žádného	žádný	k3yNgMnSc4	žádný
jiného	jiný	k2eAgMnSc4d1	jiný
kosa	kos	k1gMnSc4	kos
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnSc1	samice
snáší	snášet	k5eAaImIp3nS	snášet
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
6	[number]	k4	6
modrozelených	modrozelený	k2eAgNnPc2d1	modrozelené
vajec	vejce	k1gNnPc2	vejce
s	s	k7c7	s
rezavými	rezavý	k2eAgFnPc7d1	rezavá
skvrnami	skvrna	k1gFnPc7	skvrna
<g/>
,	,	kIx,	,
na	na	k7c6	na
kterých	který	k3yQgFnPc6	který
sedí	sedit	k5eAaImIp3nP	sedit
sama	sám	k3xTgMnSc4	sám
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
13	[number]	k4	13
-	-	kIx~	-
14	[number]	k4	14
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
mláďata	mládě	k1gNnPc4	mládě
se	se	k3xPyFc4	se
starají	starat	k5eAaImIp3nP	starat
oba	dva	k4xCgMnPc1	dva
rodiče	rodič	k1gMnPc1	rodič
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
12	[number]	k4	12
<g/>
–	–	k?	–
<g/>
15	[number]	k4	15
dnů	den	k1gInPc2	den
na	na	k7c6	na
hnízdě	hnízdo	k1gNnSc6	hnízdo
a	a	k8xC	a
po	po	k7c6	po
vylétnutí	vylétnutí	k1gNnSc6	vylétnutí
je	být	k5eAaImIp3nS	být
ještě	ještě	k9	ještě
asi	asi	k9	asi
dva	dva	k4xCgInPc4	dva
týdny	týden	k1gInPc4	týden
přikrmují	přikrmovat	k5eAaImIp3nP	přikrmovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kos	Kos	k1gMnSc1	Kos
černý	černý	k1gMnSc1	černý
se	se	k3xPyFc4	se
dožívá	dožívat	k5eAaImIp3nS	dožívat
až	až	k9	až
20	[number]	k4	20
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tah	tah	k1gInSc1	tah
===	===	k?	===
</s>
</p>
<p>
<s>
Kos	Kos	k1gMnSc1	Kos
černý	černý	k1gMnSc1	černý
je	být	k5eAaImIp3nS	být
částečně	částečně	k6eAd1	částečně
tažný	tažný	k2eAgMnSc1d1	tažný
pták	pták	k1gMnSc1	pták
<g/>
,	,	kIx,	,
na	na	k7c4	na
zimu	zima	k1gFnSc4	zima
odlétají	odlétat	k5eAaPmIp3nP	odlétat
lesní	lesní	k2eAgMnPc1d1	lesní
kosi	kos	k1gMnPc1	kos
a	a	k8xC	a
jen	jen	k6eAd1	jen
část	část	k1gFnSc1	část
kosů	kos	k1gMnPc2	kos
městských	městský	k2eAgMnPc2d1	městský
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
z	z	k7c2	z
Čech	Čechy	k1gFnPc2	Čechy
a	a	k8xC	a
severní	severní	k2eAgFnSc2d1	severní
Moravy	Morava	k1gFnSc2	Morava
zimují	zimovat	k5eAaImIp3nP	zimovat
u	u	k7c2	u
Atlantiku	Atlantik	k1gInSc2	Atlantik
<g/>
,	,	kIx,	,
jihomoravští	jihomoravský	k2eAgMnPc1d1	jihomoravský
kosi	kos	k1gMnPc1	kos
pak	pak	k6eAd1	pak
létají	létat	k5eAaImIp3nP	létat
do	do	k7c2	do
Středomoří	středomoří	k1gNnSc2	středomoří
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zimovišť	zimoviště	k1gNnPc2	zimoviště
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
března	březen	k1gInSc2	březen
a	a	k8xC	a
dubna	duben	k1gInSc2	duben
a	a	k8xC	a
odlétá	odlétat	k5eAaImIp3nS	odlétat
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zvukové	zvukový	k2eAgInPc1d1	zvukový
projevy	projev	k1gInPc1	projev
a	a	k8xC	a
zpěv	zpěv	k1gInSc1	zpěv
===	===	k?	===
</s>
</p>
<p>
<s>
Vábení	vábení	k1gNnSc1	vábení
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
dukdukduk	dukdukduk	k6eAd1	dukdukduk
<g/>
"	"	kIx"	"
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
srííí	srííí	k1gFnSc1	srííí
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
varování	varování	k1gNnSc1	varování
pak	pak	k6eAd1	pak
rámusivé	rámusivý	k2eAgNnSc1d1	rámusivý
"	"	kIx"	"
<g/>
tikstikstikstiks	tikstikstikstiks	k6eAd1	tikstikstikstiks
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Samec	samec	k1gInSc1	samec
kosa	kosa	k1gFnSc1	kosa
začíná	začínat	k5eAaImIp3nS	začínat
zpívat	zpívat	k5eAaImF	zpívat
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
jara	jaro	k1gNnSc2	jaro
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
za	za	k7c2	za
ranního	ranní	k2eAgInSc2d1	ranní
a	a	k8xC	a
večerního	večerní	k2eAgInSc2d1	večerní
soumraku	soumrak	k1gInSc2	soumrak
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
vyvýšených	vyvýšený	k2eAgNnPc2d1	vyvýšené
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
Zpěv	zpěv	k1gInSc1	zpěv
je	být	k5eAaImIp3nS	být
hlasitý	hlasitý	k2eAgMnSc1d1	hlasitý
<g/>
,	,	kIx,	,
flétnový	flétnový	k2eAgMnSc1d1	flétnový
a	a	k8xC	a
melodický	melodický	k2eAgMnSc1d1	melodický
<g/>
,	,	kIx,	,
kos	kos	k1gMnSc1	kos
navíc	navíc	k6eAd1	navíc
ovládá	ovládat	k5eAaImIp3nS	ovládat
více	hodně	k6eAd2	hodně
melodií	melodie	k1gFnPc2	melodie
a	a	k8xC	a
harmonií	harmonie	k1gFnPc2	harmonie
než	než	k8xS	než
slavík	slavík	k1gInSc1	slavík
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavé	zajímavý	k2eAgNnSc1d1	zajímavé
na	na	k7c6	na
zpěvu	zpěv	k1gInSc6	zpěv
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
jednotlivých	jednotlivý	k2eAgFnPc2d1	jednotlivá
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
kos	kos	k1gMnSc1	kos
po	po	k7c6	po
dohrání	dohrání	k1gNnSc6	dohrání
celého	celý	k2eAgNnSc2d1	celé
svého	své	k1gNnSc2	své
repertoáru	repertoár	k1gInSc2	repertoár
neustále	neustále	k6eAd1	neustále
opakuje	opakovat	k5eAaImIp3nS	opakovat
dokola	dokola	k6eAd1	dokola
<g/>
.	.	kIx.	.
</s>
<s>
Složitost	složitost	k1gFnSc1	složitost
a	a	k8xC	a
počet	počet	k1gInSc1	počet
skladeb	skladba	k1gFnPc2	skladba
vzrůstá	vzrůstat	k5eAaImIp3nS	vzrůstat
se	s	k7c7	s
stářím	stáří	k1gNnSc7	stáří
a	a	k8xC	a
zkušenostmi	zkušenost	k1gFnPc7	zkušenost
zpěváka	zpěvák	k1gMnSc2	zpěvák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
WITT	WITT	kA	WITT
<g/>
,	,	kIx,	,
Reinhart	Reinhart	k1gInSc1	Reinhart
<g/>
.	.	kIx.	.
</s>
<s>
Ptáci	pták	k1gMnPc1	pták
<g/>
.	.	kIx.	.
</s>
<s>
Steinbachův	Steinbachův	k2eAgMnSc1d1	Steinbachův
velký	velký	k2eAgMnSc1d1	velký
průvodce	průvodce	k1gMnSc1	průvodce
přírodou	příroda	k1gFnSc7	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Překlad	překlad	k1gInSc1	překlad
Helena	Helena	k1gFnSc1	Helena
Kholová	Kholová	k1gFnSc1	Kholová
<g/>
.	.	kIx.	.
</s>
<s>
Košice	Košice	k1gInPc1	Košice
<g/>
:	:	kIx,	:
Geocenter	Geocenter	k1gMnSc1	Geocenter
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
159	[number]	k4	159
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
3	[number]	k4	3
<g/>
-	-	kIx~	-
<g/>
576	[number]	k4	576
<g/>
-	-	kIx~	-
<g/>
10014	[number]	k4	10014
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kos	kos	k1gMnSc1	kos
černý	černý	k2eAgMnSc1d1	černý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
kos	kos	k1gMnSc1	kos
černý	černý	k2eAgMnSc1d1	černý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
kos	kos	k1gMnSc1	kos
černý	černý	k1gMnSc1	černý
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Hlasová	hlasový	k2eAgFnSc1d1	hlasová
ukázka	ukázka	k1gFnSc1	ukázka
z	z	k7c2	z
projektu	projekt	k1gInSc2	projekt
Českého	český	k2eAgInSc2d1	český
rozhlasu	rozhlas	k1gInSc2	rozhlas
Hlas	hlas	k1gInSc1	hlas
na	na	k7c4	na
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
</s>
</p>
<p>
<s>
www	www	k?	www
<g/>
.	.	kIx.	.
<g/>
PŘÍRODA	příroda	k1gFnSc1	příroda
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Kos	Kos	k1gMnSc1	Kos
černý	černý	k1gMnSc1	černý
-	-	kIx~	-
Turdus	Turdus	k1gMnSc1	Turdus
merula	merul	k1gMnSc2	merul
</s>
</p>
<p>
<s>
Změny	změna	k1gFnPc1	změna
v	v	k7c6	v
denzitách	denzita	k1gFnPc6	denzita
a	a	k8xC	a
hnízdní	hnízdní	k2eAgFnSc6d1	hnízdní
úspěšnosti	úspěšnost	k1gFnSc6	úspěšnost
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
synurbanizace	synurbanizace	k1gFnSc2	synurbanizace
kosa	kos	k1gMnSc2	kos
černého	černý	k1gMnSc2	černý
(	(	kIx(	(
<g/>
Turdus	Turdus	k1gInSc1	Turdus
merula	merulum	k1gNnSc2	merulum
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Birds	Birds	k1gInSc1	Birds
in	in	k?	in
Backyards	Backyards	k1gInSc1	Backyards
-	-	kIx~	-
Common	Common	k1gMnSc1	Common
Blackbird	Blackbird	k1gMnSc1	Blackbird
(	(	kIx(	(
<g/>
Turdus	Turdus	k1gMnSc1	Turdus
merula	merul	k1gMnSc2	merul
<g/>
)	)	kIx)	)
fact	fact	k2eAgInSc1d1	fact
sheet	sheet	k1gInSc1	sheet
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
