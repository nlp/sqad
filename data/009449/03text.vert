<p>
<s>
Cinemax	Cinemax	k1gInSc4	Cinemax
je	být	k5eAaImIp3nS	být
české	český	k2eAgNnSc1d1	české
nezávislé	závislý	k2eNgNnSc1d1	nezávislé
vývojářské	vývojářský	k2eAgNnSc1d1	vývojářské
studio	studio	k1gNnSc1	studio
sídlící	sídlící	k2eAgFnSc1d1	sídlící
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
založeno	založen	k2eAgNnSc1d1	založeno
Lukášem	Lukáš	k1gMnSc7	Lukáš
Macurou	Macura	k1gMnSc7	Macura
a	a	k8xC	a
Markem	Marek	k1gMnSc7	Marek
Nepožitkem	Nepožitek	k1gInSc7	Nepožitek
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
<s>
Cinemax	Cinemax	k1gInSc1	Cinemax
funguje	fungovat	k5eAaImIp3nS	fungovat
také	také	k9	také
jako	jako	k9	jako
distributor	distributor	k1gMnSc1	distributor
her	hra	k1gFnPc2	hra
<g/>
.	.	kIx.	.
</s>
<s>
Distribuoval	distribuovat	k5eAaBmAgMnS	distribuovat
například	například	k6eAd1	například
hru	hra	k1gFnSc4	hra
Ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
havrana	havran	k1gMnSc2	havran
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hry	hra	k1gFnPc1	hra
==	==	k?	==
</s>
</p>
<p>
<s>
1997	[number]	k4	1997
-	-	kIx~	-
Gooka	Gooek	k1gInSc2	Gooek
</s>
</p>
<p>
<s>
1998	[number]	k4	1998
-	-	kIx~	-
Husita	husita	k1gMnSc1	husita
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
-	-	kIx~	-
Ve	v	k7c6	v
stínu	stín	k1gInSc6	stín
havrana	havran	k1gMnSc2	havran
</s>
</p>
<p>
<s>
2001	[number]	k4	2001
-	-	kIx~	-
State	status	k1gInSc5	status
of	of	k?	of
War	War	k1gFnSc7	War
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
-	-	kIx~	-
Žhavé	žhavý	k2eAgNnSc1d1	žhavé
Léto	léto	k1gNnSc1	léto
3	[number]	k4	3
a	a	k8xC	a
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
-	-	kIx~	-
Necromania	Necromanium	k1gNnSc2	Necromanium
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
-	-	kIx~	-
Daemonica	Daemonic	k1gInSc2	Daemonic
</s>
</p>
<p>
<s>
2006	[number]	k4	2006
-	-	kIx~	-
Gumboy	Gumbo	k2eAgInPc4d1	Gumbo
<g/>
:	:	kIx,	:
Crazy	Craz	k1gInPc4	Craz
Adventures	Adventures	k1gInSc1	Adventures
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
-	-	kIx~	-
Gumboy	Gumbo	k2eAgInPc4d1	Gumbo
<g/>
:	:	kIx,	:
Crazy	Craz	k1gInPc4	Craz
Features	Featuresa	k1gFnPc2	Featuresa
</s>
</p>
<p>
<s>
2007	[number]	k4	2007
-	-	kIx~	-
State	status	k1gInSc5	status
of	of	k?	of
War	War	k1gFnSc7	War
2	[number]	k4	2
<g/>
:	:	kIx,	:
Arcon	Arcon	k1gInSc1	Arcon
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
-	-	kIx~	-
Gumboy	Gumboa	k1gFnSc2	Gumboa
<g/>
:	:	kIx,	:
<g/>
Tournament	Tournament	k1gInSc1	Tournament
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
-	-	kIx~	-
Puzzle	puzzle	k1gNnSc1	puzzle
Rocks	Rocksa	k1gFnPc2	Rocksa
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
-	-	kIx~	-
Numen	Numen	k1gInSc1	Numen
<g/>
:	:	kIx,	:
Contest	Contest	k1gInSc1	Contest
of	of	k?	of
Heroes	Heroes	k1gInSc1	Heroes
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
-	-	kIx~	-
Inquisitor	Inquisitor	k1gInSc1	Inquisitor
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
-	-	kIx~	-
Sokomania	Sokomanium	k1gNnSc2	Sokomanium
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
-	-	kIx~	-
Snakenoid	Snakenoid	k1gInSc1	Snakenoid
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
-	-	kIx~	-
Space	Spaec	k1gInSc2	Spaec
Revenge	Revenge	k1gNnSc2	Revenge
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
-	-	kIx~	-
Rytmik	rytmika	k1gFnPc2	rytmika
</s>
</p>
<p>
<s>
2011	[number]	k4	2011
-	-	kIx~	-
Commando	Commanda	k1gFnSc5	Commanda
<g/>
:	:	kIx,	:
Steel	Steel	k1gMnSc1	Steel
Disaster	Disaster	k1gMnSc1	Disaster
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
-	-	kIx~	-
Decathlon	Decathlon	k1gInSc1	Decathlon
2012	[number]	k4	2012
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
-	-	kIx~	-
Retro	retro	k1gNnSc2	retro
Decathlon	Decathlon	k1gInSc4	Decathlon
2012	[number]	k4	2012
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
-	-	kIx~	-
Gyro	Gyro	k6eAd1	Gyro
<g/>
13	[number]	k4	13
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
-	-	kIx~	-
247	[number]	k4	247
Missiles	Missilesa	k1gFnPc2	Missilesa
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
-	-	kIx~	-
hexee	hexe	k1gInSc2	hexe
<g/>
–	–	k?	–
<g/>
smash	smash	k1gMnSc1	smash
the	the	k?	the
match	match	k1gMnSc1	match
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
-	-	kIx~	-
Wormi	Wor	k1gFnPc7	Wor
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
-	-	kIx~	-
Sokomania	Sokomanium	k1gNnSc2	Sokomanium
2	[number]	k4	2
<g/>
:	:	kIx,	:
Cool	Cool	k1gMnSc1	Cool
Job	Job	k1gMnSc1	Job
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
-	-	kIx~	-
The	The	k1gMnSc1	The
Keep	Keep	k1gMnSc1	Keep
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Stránky	stránka	k1gFnPc1	stránka
o	o	k7c6	o
hrách	hra	k1gFnPc6	hra
od	od	k7c2	od
Cinemaxu	Cinemax	k1gInSc2	Cinemax
</s>
</p>
<p>
<s>
Cinemax	Cinemax	k1gInSc1	Cinemax
na	na	k7c6	na
abcher	abchra	k1gFnPc2	abchra
<g/>
.	.	kIx.	.
<g/>
sk	sk	k?	sk
</s>
</p>
<p>
<s>
Cinemax	Cinemax	k1gInSc1	Cinemax
na	na	k7c4	na
databázi	databáze	k1gFnSc4	databáze
her	hra	k1gFnPc2	hra
</s>
</p>
