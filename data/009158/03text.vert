<p>
<s>
Kilometr	kilometr	k1gInSc1	kilometr
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
(	(	kIx(	(
<g/>
značka	značka	k1gFnSc1	značka
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
nebo	nebo	k8xC	nebo
km	km	kA	km
<g/>
·	·	k?	·
<g/>
h	h	k?	h
<g/>
−	−	k?	−
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednotka	jednotka	k1gFnSc1	jednotka
rychlosti	rychlost	k1gFnSc2	rychlost
<g/>
.	.	kIx.	.
</s>
<s>
Udává	udávat	k5eAaImIp3nS	udávat
<g/>
,	,	kIx,	,
kolik	kolik	k4yIc4	kolik
kilometrů	kilometr	k1gInPc2	kilometr
urazí	urazit	k5eAaPmIp3nS	urazit
za	za	k7c4	za
jednu	jeden	k4xCgFnSc4	jeden
hodinu	hodina	k1gFnSc4	hodina
objekt	objekt	k1gInSc4	objekt
pohybující	pohybující	k2eAgInSc4d1	pohybující
se	se	k3xPyFc4	se
konstantní	konstantní	k2eAgFnSc7d1	konstantní
rychlostí	rychlost	k1gFnSc7	rychlost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Převod	převod	k1gInSc1	převod
na	na	k7c4	na
jiné	jiný	k2eAgFnPc4d1	jiná
jednotky	jednotka	k1gFnPc4	jednotka
==	==	k?	==
</s>
</p>
<p>
<s>
Metr	metr	k1gInSc1	metr
za	za	k7c4	za
sekundu	sekunda	k1gFnSc4	sekunda
<g/>
:	:	kIx,	:
1	[number]	k4	1
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
=	=	kIx~	=
3,6	[number]	k4	3,6
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
1	[number]	k4	1
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
=	=	kIx~	=
0,2777	[number]	k4	0,2777
<g/>
...	...	k?	...
m	m	kA	m
<g/>
/	/	kIx~	/
<g/>
s	s	k7c7	s
</s>
</p>
<p>
<s>
Míle	míle	k1gFnSc1	míle
za	za	k7c4	za
hodinu	hodina	k1gFnSc4	hodina
<g/>
:	:	kIx,	:
1	[number]	k4	1
mph	mph	k?	mph
=	=	kIx~	=
1,609	[number]	k4	1,609
<g/>
4	[number]	k4	4
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
1	[number]	k4	1
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
≈	≈	k?	≈
0,621	[number]	k4	0,621
mph	mph	k?	mph
</s>
</p>
<p>
<s>
Uzel	uzel	k1gInSc1	uzel
<g/>
:	:	kIx,	:
1	[number]	k4	1
kt	kt	k?	kt
=	=	kIx~	=
1,852	[number]	k4	1,852
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
<g/>
,	,	kIx,	,
1	[number]	k4	1
km	km	kA	km
<g/>
/	/	kIx~	/
<g/>
h	h	k?	h
≈	≈	k?	≈
0,54	[number]	k4	0,54
kt	kt	k?	kt
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Tachometr	tachometr	k1gInSc1	tachometr
</s>
</p>
<p>
<s>
Tachograf	tachograf	k1gInSc1	tachograf
</s>
</p>
