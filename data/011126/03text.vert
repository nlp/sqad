<p>
<s>
Čong	Čong	k1gMnSc1	Čong
Kjong-mi	Kjong-	k1gFnPc7	Kjong-
(	(	kIx(	(
<g/>
*	*	kIx~	*
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1985	[number]	k4	1985
Kunsan	Kunsana	k1gFnPc2	Kunsana
<g/>
,	,	kIx,	,
Jižní	jižní	k2eAgFnSc1d1	jižní
Korea	Korea	k1gFnSc1	Korea
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
bývalá	bývalý	k2eAgFnSc1d1	bývalá
korejská	korejský	k2eAgFnSc1d1	Korejská
zápasnice-judistka	zápasniceudistka	k1gFnSc1	zápasnice-judistka
<g/>
,	,	kIx,	,
bronzová	bronzový	k2eAgFnSc1d1	bronzová
olympijská	olympijský	k2eAgFnSc1d1	olympijská
medailistka	medailistka	k1gFnSc1	medailistka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sportovní	sportovní	k2eAgFnSc1d1	sportovní
kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
jihokorejské	jihokorejský	k2eAgFnSc6d1	jihokorejská
reprezentaci	reprezentace	k1gFnSc6	reprezentace
se	se	k3xPyFc4	se
pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
od	od	k7c2	od
roku	rok	k1gInSc2	rok
2005	[number]	k4	2005
v	v	k7c6	v
polotěžké	polotěžký	k2eAgFnSc6d1	polotěžká
váze	váha	k1gFnSc6	váha
do	do	k7c2	do
78	[number]	k4	78
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Připravovala	připravovat	k5eAaImAgFnS	připravovat
se	se	k3xPyFc4	se
na	na	k7c6	na
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Jonginu	Jongin	k1gInSc6	Jongin
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
vybojovala	vybojovat	k5eAaPmAgFnS	vybojovat
třetím	třetí	k4xOgNnSc7	třetí
místem	místo	k1gNnSc7	místo
na	na	k7c6	na
mistrovstí	mistrovst	k1gFnPc2	mistrovst
světa	svět	k1gInSc2	svět
v	v	k7c6	v
Riu	Riu	k1gFnSc6	Riu
přímou	přímý	k2eAgFnSc4d1	přímá
kvalifikaci	kvalifikace	k1gFnSc4	kvalifikace
na	na	k7c4	na
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
v	v	k7c6	v
Pekingu	Peking	k1gInSc6	Peking
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
vyladila	vyladit	k5eAaPmAgFnS	vyladit
formu	forma	k1gFnSc4	forma
<g/>
,	,	kIx,	,
ve	v	k7c6	v
čtvrtfinále	čtvrtfinále	k1gNnSc6	čtvrtfinále
porazila	porazit	k5eAaPmAgFnS	porazit
na	na	k7c4	na
ippon	ippon	k1gInSc4	ippon
svojí	svojit	k5eAaImIp3nP	svojit
osobní	osobní	k2eAgFnSc7d1	osobní
technikou	technika	k1gFnSc7	technika
ippon-seoi-nage	ipponeoiagat	k5eAaPmIp3nS	ippon-seoi-nagat
Němku	Němka	k1gFnSc4	Němka
Heide	Heid	k1gInSc5	Heid
Wollertovou	Wollertová	k1gFnSc4	Wollertová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
semifinále	semifinále	k1gNnSc6	semifinále
však	však	k9	však
nezvládla	zvládnout	k5eNaPmAgFnS	zvládnout
hru	hra	k1gFnSc4	hra
nervů	nerv	k1gInPc2	nerv
s	s	k7c7	s
dobře	dobře	k6eAd1	dobře
připravenou	připravený	k2eAgFnSc7d1	připravená
Kubánkou	Kubánka	k1gFnSc7	Kubánka
Yalennis	Yalennis	k1gFnSc7	Yalennis
Castillovou	Castillová	k1gFnSc7	Castillová
a	a	k8xC	a
prohrála	prohrát	k5eAaPmAgFnS	prohrát
na	na	k7c4	na
šido	šido	k1gNnSc4	šido
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
boji	boj	k1gInSc6	boj
o	o	k7c4	o
třetí	třetí	k4xOgNnSc4	třetí
místo	místo	k1gNnSc4	místo
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
proti	proti	k7c3	proti
Brazilce	Brazilka	k1gFnSc3	Brazilka
Edinanci	Edinance	k1gFnSc3	Edinance
Silvaové	Silvaová	k1gFnSc3	Silvaová
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
zápasu	zápas	k1gInSc2	zápas
poslala	poslat	k5eAaPmAgFnS	poslat
soupeřku	soupeřka	k1gFnSc4	soupeřka
technikou	technika	k1gFnSc7	technika
harai-makikomi	haraiakiko	k1gFnPc7	harai-makiko
na	na	k7c4	na
yuko	yuko	k1gNnSc4	yuko
a	a	k8xC	a
následně	následně	k6eAd1	následně
jí	jíst	k5eAaImIp3nS	jíst
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
držení	držení	k1gNnSc2	držení
<g/>
.	.	kIx.	.
</s>
<s>
Získala	získat	k5eAaPmAgFnS	získat
bronzovou	bronzový	k2eAgFnSc4d1	bronzová
olympijskou	olympijský	k2eAgFnSc4d1	olympijská
medaili	medaile	k1gFnSc4	medaile
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
získala	získat	k5eAaPmAgFnS	získat
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
na	na	k7c6	na
Asijských	asijský	k2eAgFnPc6d1	asijská
hrách	hra	k1gFnPc6	hra
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
konkurenci	konkurence	k1gFnSc6	konkurence
evropských	evropský	k2eAgFnPc2d1	Evropská
a	a	k8xC	a
amerických	americký	k2eAgFnPc2d1	americká
polotěžkých	polotěžký	k2eAgFnPc2d1	polotěžká
vah	váha	k1gFnPc2	váha
se	se	k3xPyFc4	se
jí	on	k3xPp3gFnSc7	on
přestalo	přestat	k5eAaPmAgNnS	přestat
dařit	dařit	k5eAaImF	dařit
<g/>
.	.	kIx.	.
</s>
<s>
Zápasy	zápas	k1gInPc4	zápas
nezvládala	zvládat	k5eNaImAgFnS	zvládat
takticky	takticky	k6eAd1	takticky
a	a	k8xC	a
pravidelně	pravidelně	k6eAd1	pravidelně
končila	končit	k5eAaImAgNnP	končit
v	v	k7c6	v
prvních	první	k4xOgFnPc6	první
kole	kolo	k1gNnSc6	kolo
na	na	k7c4	na
větší	veliký	k2eAgInSc4d2	veliký
počet	počet	k1gInSc4	počet
napomenutí	napomenutí	k1gNnSc2	napomenutí
<g/>
.	.	kIx.	.
</s>
<s>
Ne	ne	k9	ne
jinak	jinak	k6eAd1	jinak
tomu	ten	k3xDgNnSc3	ten
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
na	na	k7c6	na
olympijských	olympijský	k2eAgFnPc6d1	olympijská
hrách	hra	k1gFnPc6	hra
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vypadla	vypadnout	k5eAaPmAgFnS	vypadnout
v	v	k7c6	v
úvodním	úvodní	k2eAgNnSc6d1	úvodní
kole	kolo	k1gNnSc6	kolo
s	s	k7c7	s
Japonkou	Japonka	k1gFnSc7	Japonka
Akari	Akar	k1gFnSc2	Akar
Ogataovou	Ogataová	k1gFnSc4	Ogataová
po	po	k7c6	po
dvou	dva	k4xCgNnPc6	dva
napomenutích	napomenutí	k1gNnPc6	napomenutí
na	na	k7c6	na
juko	juka	k1gFnSc5	juka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
obhájila	obhájit	k5eAaPmAgFnS	obhájit
zlatou	zlatý	k2eAgFnSc4d1	zlatá
medaili	medaile	k1gFnSc4	medaile
na	na	k7c6	na
Asijských	asijský	k2eAgFnPc6d1	asijská
hrách	hra	k1gFnPc6	hra
<g/>
.	.	kIx.	.
</s>
<s>
Sportovní	sportovní	k2eAgFnSc4d1	sportovní
kariéru	kariéra	k1gFnSc4	kariéra
ukončila	ukončit	k5eAaPmAgFnS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k9	jako
policistka	policistka	k1gFnSc1	policistka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vítězství	vítězství	k1gNnSc4	vítězství
na	na	k7c6	na
mezinárodních	mezinárodní	k2eAgInPc6d1	mezinárodní
turnajích	turnaj	k1gInPc6	turnaj
===	===	k?	===
</s>
</p>
<p>
<s>
2005	[number]	k4	2005
-	-	kIx~	-
1	[number]	k4	1
<g/>
x	x	k?	x
světový	světový	k2eAgInSc4d1	světový
pohár	pohár	k1gInSc4	pohár
(	(	kIx(	(
<g/>
Čedžu	Čedžu	k1gFnSc4	Čedžu
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2008	[number]	k4	2008
-	-	kIx~	-
1	[number]	k4	1
<g/>
x	x	k?	x
světový	světový	k2eAgInSc4d1	světový
pohár	pohár	k1gInSc4	pohár
(	(	kIx(	(
<g/>
Čedžu	Čedžu	k1gFnSc4	Čedžu
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2009	[number]	k4	2009
-	-	kIx~	-
1	[number]	k4	1
<g/>
x	x	k?	x
světový	světový	k2eAgInSc4d1	světový
pohár	pohár	k1gInSc4	pohár
(	(	kIx(	(
<g/>
Ulánbátar	Ulánbátar	k1gInSc4	Ulánbátar
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2010	[number]	k4	2010
-	-	kIx~	-
2	[number]	k4	2
<g/>
x	x	k?	x
světový	světový	k2eAgInSc4d1	světový
pohár	pohár	k1gInSc4	pohár
(	(	kIx(	(
<g/>
Taškent	Taškent	k1gInSc1	Taškent
<g/>
,	,	kIx,	,
Suwon	Suwon	k1gInSc1	Suwon
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2012	[number]	k4	2012
-	-	kIx~	-
3	[number]	k4	3
<g/>
x	x	k?	x
světový	světový	k2eAgInSc4d1	světový
pohár	pohár	k1gInSc4	pohár
(	(	kIx(	(
<g/>
Sofie	Sofie	k1gFnSc1	Sofie
<g/>
,	,	kIx,	,
Apia	Apia	k1gFnSc1	Apia
<g/>
,	,	kIx,	,
Čedžu	Čedžu	k1gFnSc1	Čedžu
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2013	[number]	k4	2013
-	-	kIx~	-
2	[number]	k4	2
<g/>
x	x	k?	x
světový	světový	k2eAgInSc4d1	světový
pohár	pohár	k1gInSc4	pohár
(	(	kIx(	(
<g/>
Oberwart	Oberwart	k1gInSc1	Oberwart
<g/>
,	,	kIx,	,
Čedžu	Čedžu	k1gFnSc1	Čedžu
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
2014	[number]	k4	2014
-	-	kIx~	-
1	[number]	k4	1
<g/>
x	x	k?	x
světový	světový	k2eAgInSc4d1	světový
pohár	pohár	k1gInSc4	pohár
(	(	kIx(	(
<g/>
Wollongong	Wollongong	k1gInSc4	Wollongong
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
==	==	k?	==
Výsledky	výsledek	k1gInPc1	výsledek
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Sport	sport	k1gInSc1	sport
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
a	a	k8xC	a
novinky	novinka	k1gFnPc1	novinka
Čong	Čonga	k1gFnPc2	Čonga
Kjong-mi	Kjong-	k1gFnPc7	Kjong-
na	na	k7c6	na
judoinside	judoinsid	k1gInSc5	judoinsid
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
