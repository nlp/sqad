<s>
Kdo	kdo	k3yInSc1	kdo
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
komediální	komediální	k2eAgMnSc1d1	komediální
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
producent	producent	k1gMnSc1	producent
a	a	k8xC	a
scenárista	scenárista	k1gMnSc1	scenárista
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
letech	let	k1gInPc6	let
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
stal	stát	k5eAaPmAgInS	stát
známým	známý	k2eAgMnSc7d1	známý
jako	jako	k8xC	jako
korespondent	korespondent	k1gMnSc1	korespondent
televizní	televizní	k2eAgFnSc2d1	televizní
show	show	k1gFnSc2	show
The	The	k1gFnSc2	The
Daily	Daila	k1gFnSc2	Daila
Show	show	k1gFnSc2	show
with	with	k1gMnSc1	with
Jon	Jon	k1gMnSc1	Jon
Stewart	Stewart	k1gMnSc1	Stewart
<g/>
?	?	kIx.	?
</s>
