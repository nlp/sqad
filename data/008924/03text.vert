<p>
<s>
Steven	Steven	k2eAgMnSc1d1	Steven
John	John	k1gMnSc1	John
"	"	kIx"	"
<g/>
Steve	Steve	k1gMnSc1	Steve
<g/>
"	"	kIx"	"
Carell	Carell	k1gMnSc1	Carell
(	(	kIx(	(
<g/>
*	*	kIx~	*
16	[number]	k4	16
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1962	[number]	k4	1962
<g/>
,	,	kIx,	,
Concord	Concord	k1gInSc1	Concord
<g/>
,	,	kIx,	,
Massachusetts	Massachusetts	k1gNnSc1	Massachusetts
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
komediální	komediální	k2eAgMnSc1d1	komediální
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
producent	producent	k1gMnSc1	producent
a	a	k8xC	a
scenárista	scenárista	k1gMnSc1	scenárista
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
letech	let	k1gInPc6	let
1999	[number]	k4	1999
<g/>
–	–	k?	–
<g/>
2004	[number]	k4	2004
stal	stát	k5eAaPmAgInS	stát
známým	známý	k2eAgMnSc7d1	známý
jako	jako	k8xS	jako
korespondent	korespondent	k1gMnSc1	korespondent
televizní	televizní	k2eAgFnSc2d1	televizní
show	show	k1gFnSc2	show
The	The	k1gFnSc2	The
Daily	Daila	k1gFnSc2	Daila
Show	show	k1gFnSc2	show
with	with	k1gMnSc1	with
Jon	Jon	k1gMnSc1	Jon
Stewart	Stewart	k1gMnSc1	Stewart
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2005	[number]	k4	2005
získal	získat	k5eAaPmAgMnS	získat
hlavní	hlavní	k2eAgFnSc4d1	hlavní
roli	role	k1gFnSc4	role
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
adaptaci	adaptace	k1gFnSc6	adaptace
britského	britský	k2eAgInSc2d1	britský
seriálu	seriál	k1gInSc2	seriál
Kancl	Kancl	k?	Kancl
<g/>
.	.	kIx.	.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS	následovat
několik	několik	k4yIc1	několik
úspěšných	úspěšný	k2eAgInPc2d1	úspěšný
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
Božský	božský	k2eAgMnSc1d1	božský
Bruce	Bruce	k1gMnSc1	Bruce
<g/>
,	,	kIx,	,
Božský	božský	k2eAgMnSc1d1	božský
Evan	Evan	k1gMnSc1	Evan
<g/>
,	,	kIx,	,
Zprávař	Zprávař	k1gMnSc1	Zprávař
<g/>
,	,	kIx,	,
40	[number]	k4	40
let	léto	k1gNnPc2	léto
panic	panice	k1gFnPc2	panice
<g/>
,	,	kIx,	,
Dan	Dan	k1gMnSc1	Dan
in	in	k?	in
Real	Real	k1gInSc1	Real
Life	Life	k1gInSc1	Life
<g/>
,	,	kIx,	,
Horton	Horton	k1gInSc1	Horton
<g/>
,	,	kIx,	,
Malá	malý	k2eAgFnSc1d1	malá
Miss	miss	k1gFnSc1	miss
Sunshine	Sunshin	k1gInSc5	Sunshin
a	a	k8xC	a
Dostaňte	dostat	k5eAaPmRp2nP	dostat
agenta	agent	k1gMnSc4	agent
Smarta	Smarta	k1gFnSc1	Smarta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Časopisem	časopis	k1gInSc7	časopis
Life	Lif	k1gFnSc2	Lif
Magazine	Magazin	k1gInSc5	Magazin
byl	být	k5eAaImAgInS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
nejzábavnějšího	zábavní	k2eAgMnSc4d3	nejzábavnější
muže	muž	k1gMnSc4	muž
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
získal	získat	k5eAaPmAgMnS	získat
cenu	cena	k1gFnSc4	cena
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
nejlepší	dobrý	k2eAgMnSc1d3	nejlepší
herec	herec	k1gMnSc1	herec
v	v	k7c6	v
televizní	televizní	k2eAgFnSc6d1	televizní
komedii	komedie	k1gFnSc6	komedie
za	za	k7c4	za
svoji	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
Michaela	Michael	k1gMnSc2	Michael
Scotta	Scott	k1gMnSc2	Scott
v	v	k7c6	v
seriálu	seriál	k1gInSc6	seriál
Kancl	Kancl	k?	Kancl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Carellovo	Carellův	k2eAgNnSc4d1	Carellův
mládí	mládí	k1gNnSc4	mládí
==	==	k?	==
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
nejmladším	mladý	k2eAgMnPc3d3	nejmladší
ze	z	k7c2	z
čtyř	čtyři	k4xCgMnPc2	čtyři
synů	syn	k1gMnPc2	syn
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
Concordu	Concordo	k1gNnSc6	Concordo
v	v	k7c6	v
Massachusetts	Massachusetts	k1gNnSc6	Massachusetts
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
brzo	brzo	k6eAd1	brzo
po	po	k7c6	po
narození	narození	k1gNnSc6	narození
se	se	k3xPyFc4	se
s	s	k7c7	s
rodiči	rodič	k1gMnPc7	rodič
přestěhovali	přestěhovat	k5eAaPmAgMnP	přestěhovat
do	do	k7c2	do
města	město	k1gNnSc2	město
Acton	Actona	k1gFnPc2	Actona
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
Harriet	Harriet	k1gInSc1	Harriet
T.	T.	kA	T.
(	(	kIx(	(
<g/>
rozená	rozený	k2eAgFnSc1d1	rozená
Kochová	Kochová	k1gFnSc1	Kochová
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
ošetřovatelka	ošetřovatelka	k1gFnSc1	ošetřovatelka
na	na	k7c6	na
psychiatrii	psychiatrie	k1gFnSc6	psychiatrie
<g/>
,	,	kIx,	,
a	a	k8xC	a
jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
<g/>
,	,	kIx,	,
Edwin	Edwin	k1gMnSc1	Edwin
A.	A.	kA	A.
Carell	Carell	k1gMnSc1	Carell
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
elektrotechnik	elektrotechnik	k1gMnSc1	elektrotechnik
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
dědeček	dědeček	k1gMnSc1	dědeček
z	z	k7c2	z
otcovy	otcův	k2eAgFnSc2d1	otcova
strany	strana	k1gFnSc2	strana
byl	být	k5eAaImAgMnS	být
Ital	Ital	k1gMnSc1	Ital
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
s	s	k7c7	s
příjmením	příjmení	k1gNnSc7	příjmení
"	"	kIx"	"
<g/>
Caroselli	Caroselle	k1gFnSc4	Caroselle
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
si	se	k3xPyFc3	se
poté	poté	k6eAd1	poté
nechal	nechat	k5eAaPmAgMnS	nechat
zkrátit	zkrátit	k5eAaPmF	zkrátit
na	na	k7c4	na
"	"	kIx"	"
<g/>
Carell	Carell	k1gInSc4	Carell
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Studoval	studovat	k5eAaImAgInS	studovat
Fenn	Fenn	k1gInSc1	Fenn
School	School	k1gInSc1	School
a	a	k8xC	a
Middlesex	Middlesex	k1gInSc1	Middlesex
School	Schoola	k1gFnPc2	Schoola
a	a	k8xC	a
navštěvoval	navštěvovat	k5eAaImAgMnS	navštěvovat
univerzitu	univerzita	k1gFnSc4	univerzita
Denison	Denisona	k1gFnPc2	Denisona
v	v	k7c6	v
Granville	Granvilla	k1gFnSc6	Granvilla
<g/>
,	,	kIx,	,
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
Ohio	Ohio	k1gNnSc1	Ohio
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
chtěl	chtít	k5eAaImAgMnS	chtít
stát	stát	k5eAaPmF	stát
hlasatelem	hlasatel	k1gMnSc7	hlasatel
v	v	k7c6	v
WDUB	WDUB	kA	WDUB
<g/>
,	,	kIx,	,
rozhlasové	rozhlasový	k2eAgFnSc6d1	rozhlasová
stanici	stanice	k1gFnSc6	stanice
v	v	k7c6	v
Granvillu	Granvill	k1gInSc6	Granvill
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kariéra	kariéra	k1gFnSc1	kariéra
==	==	k?	==
</s>
</p>
<p>
<s>
Tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
než	než	k8xS	než
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
hercem	herec	k1gMnSc7	herec
<g/>
,	,	kIx,	,
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
pošťák	pošťák	k1gMnSc1	pošťák
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Littleton	Littleton	k1gInSc1	Littleton
v	v	k7c6	v
Massachusetts	Massachusetts	k1gNnSc6	Massachusetts
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
si	se	k3xPyFc3	se
však	však	k9	však
odporuje	odporovat	k5eAaImIp3nS	odporovat
s	s	k7c7	s
tvrzením	tvrzení	k1gNnSc7	tvrzení
zaměstnanců	zaměstnanec	k1gMnPc2	zaměstnanec
littletonského	littletonský	k2eAgInSc2d1	littletonský
poštovního	poštovní	k2eAgInSc2d1	poštovní
úřadu	úřad	k1gInSc2	úřad
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
zaměstnavatel	zaměstnavatel	k1gMnSc1	zaměstnavatel
mu	on	k3xPp3gMnSc3	on
však	však	k9	však
údajně	údajně	k6eAd1	údajně
řekl	říct	k5eAaPmAgMnS	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
dobrým	dobrý	k2eAgMnSc7d1	dobrý
pošťákem	pošťák	k1gMnSc7	pošťák
<g/>
,	,	kIx,	,
a	a	k8xC	a
tak	tak	k9	tak
tuto	tento	k3xDgFnSc4	tento
práci	práce	k1gFnSc4	práce
opustil	opustit	k5eAaPmAgMnS	opustit
<g/>
.	.	kIx.	.
</s>
<s>
Hodlal	hodlat	k5eAaImAgMnS	hodlat
absolvovat	absolvovat	k5eAaPmF	absolvovat
právnickou	právnický	k2eAgFnSc4d1	právnická
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
protože	protože	k8xS	protože
nevěděl	vědět	k5eNaImAgMnS	vědět
<g/>
,	,	kIx,	,
jaké	jaký	k3yIgInPc1	jaký
důvody	důvod	k1gInPc1	důvod
ho	on	k3xPp3gMnSc4	on
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
vedou	vést	k5eAaImIp3nP	vést
(	(	kIx(	(
<g/>
a	a	k8xC	a
neměl	mít	k5eNaImAgMnS	mít
tedy	tedy	k9	tedy
co	co	k9	co
napsat	napsat	k5eAaBmF	napsat
do	do	k7c2	do
přihlášky	přihláška	k1gFnSc2	přihláška
pod	pod	k7c4	pod
tuto	tento	k3xDgFnSc4	tento
otázku	otázka	k1gFnSc4	otázka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
vzdal	vzdát	k5eAaPmAgMnS	vzdát
i	i	k9	i
toto	tento	k3xDgNnSc4	tento
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
své	svůj	k3xOyFgFnSc2	svůj
herecké	herecký	k2eAgFnSc2d1	herecká
kariéry	kariéra	k1gFnSc2	kariéra
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
putovní	putovní	k2eAgFnSc6d1	putovní
dětské	dětský	k2eAgFnSc6d1	dětská
divadelní	divadelní	k2eAgFnSc6d1	divadelní
společnosti	společnost	k1gFnSc6	společnost
<g/>
,	,	kIx,	,
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
komediálním	komediální	k2eAgInSc6d1	komediální
muzikálu	muzikál	k1gInSc6	muzikál
Knat	Knat	k2eAgInSc4d1	Knat
Scat	scat	k1gInSc4	scat
Private	Privat	k1gMnSc5	Privat
Eye	Eye	k1gMnSc5	Eye
(	(	kIx(	(
<g/>
Soukromé	soukromý	k2eAgNnSc4d1	soukromé
očko	očko	k1gNnSc4	očko
Knat	Knat	k2eAgInSc1d1	Knat
Scat	scat	k1gInSc1	scat
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
získal	získat	k5eAaPmAgMnS	získat
svou	svůj	k3xOyFgFnSc4	svůj
první	první	k4xOgFnSc4	první
malou	malý	k2eAgFnSc4d1	malá
roli	role	k1gFnSc4	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Kudrnatá	kudrnatý	k2eAgFnSc1d1	kudrnatá
Sue	Sue	k1gFnSc1	Sue
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ztvárnil	ztvárnit	k5eAaPmAgInS	ztvárnit
postavu	postava	k1gFnSc4	postava
Tesia	Tesium	k1gNnSc2	Tesium
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Carell	Carell	k1gMnSc1	Carell
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
povědomí	povědomí	k1gNnSc2	povědomí
veřejnosti	veřejnost	k1gFnSc2	veřejnost
jako	jako	k8xC	jako
korespondent	korespondent	k1gMnSc1	korespondent
pro	pro	k7c4	pro
The	The	k1gFnSc4	The
Daily	Daila	k1gFnSc2	Daila
Show	show	k1gFnSc2	show
with	with	k1gMnSc1	with
Jon	Jon	k1gMnSc1	Jon
Stewart	Stewart	k1gMnSc1	Stewart
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
1999	[number]	k4	1999
a	a	k8xC	a
2005	[number]	k4	2005
<g/>
,	,	kIx,	,
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
pravidelnými	pravidelný	k2eAgInPc7d1	pravidelný
segmenty	segment	k1gInPc7	segment
jako	jako	k8xS	jako
například	například	k6eAd1	například
Even	Even	k1gInSc1	Even
Stevphen	Stevphen	k1gInSc1	Stevphen
se	s	k7c7	s
Stephenem	Stephen	k1gMnSc7	Stephen
Colbertem	Colbert	k1gMnSc7	Colbert
<g/>
,	,	kIx,	,
či	či	k8xC	či
Produce	Produce	k1gFnSc1	Produce
Pete	Pete	k1gFnSc1	Pete
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
v	v	k7c6	v
americké	americký	k2eAgFnSc6d1	americká
adaptaci	adaptace	k1gFnSc6	adaptace
britského	britský	k2eAgInSc2d1	britský
seriálu	seriál	k1gInSc2	seriál
Kancl	Kancl	k?	Kancl
regionálního	regionální	k2eAgMnSc2d1	regionální
manažera	manažer	k1gMnSc2	manažer
v	v	k7c6	v
pobočce	pobočka	k1gFnSc6	pobočka
fiktivní	fiktivní	k2eAgFnSc2d1	fiktivní
společnosti	společnost	k1gFnSc2	společnost
Dunder	Dunder	k1gMnSc1	Dunder
Mifflin	Mifflina	k1gFnPc2	Mifflina
Paper	Paper	k1gMnSc1	Paper
Company	Compana	k1gFnSc2	Compana
Michaela	Michael	k1gMnSc2	Michael
Scotta	Scott	k1gMnSc2	Scott
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
účinkoval	účinkovat	k5eAaImAgMnS	účinkovat
do	do	k7c2	do
konce	konec	k1gInSc2	konec
7	[number]	k4	7
řady	řada	k1gFnSc2	řada
<g/>
,	,	kIx,	,
odvysílané	odvysílaný	k2eAgFnPc1d1	odvysílaná
v	v	k7c6	v
letech	let	k1gInPc6	let
2010	[number]	k4	2010
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
menších	malý	k2eAgFnPc6d2	menší
filmových	filmový	k2eAgFnPc6d1	filmová
rolích	role	k1gFnPc6	role
zazářil	zazářit	k5eAaPmAgMnS	zazářit
v	v	k7c6	v
úspěšné	úspěšný	k2eAgFnSc6d1	úspěšná
komedii	komedie	k1gFnSc6	komedie
40	[number]	k4	40
let	léto	k1gNnPc2	léto
panic	panice	k1gFnPc2	panice
<g/>
.	.	kIx.	.
</s>
<s>
Následovaly	následovat	k5eAaImAgInP	následovat
Malá	malý	k2eAgFnSc1d1	malá
Miss	miss	k1gFnSc1	miss
Sunshine	Sunshin	k1gInSc5	Sunshin
<g/>
,	,	kIx,	,
Božský	božský	k2eAgMnSc1d1	božský
Evan	Evan	k1gMnSc1	Evan
<g/>
,	,	kIx,	,
Dan	Dan	k1gMnSc1	Dan
in	in	k?	in
Real	Real	k1gInSc1	Real
Life	Lif	k1gFnSc2	Lif
nebo	nebo	k8xC	nebo
Dostaňte	dostat	k5eAaPmRp2nP	dostat
agenta	agent	k1gMnSc4	agent
Smarta	Smarta	k1gFnSc1	Smarta
<g/>
.	.	kIx.	.
</s>
<s>
Mezitím	mezitím	k6eAd1	mezitím
také	také	k9	také
zazněl	zaznít	k5eAaPmAgInS	zaznít
jeho	on	k3xPp3gInSc4	on
hlas	hlas	k1gInSc4	hlas
ve	v	k7c6	v
dvou	dva	k4xCgInPc6	dva
počítačově	počítačově	k6eAd1	počítačově
animovaných	animovaný	k2eAgInPc6d1	animovaný
filmech	film	k1gInPc6	film
<g/>
,	,	kIx,	,
Za	za	k7c7	za
plotem	plot	k1gInSc7	plot
a	a	k8xC	a
Horton	Horton	k1gInSc4	Horton
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgInS	zahrát
<g/>
,	,	kIx,	,
<g/>
společně	společně	k6eAd1	společně
z	z	k7c2	z
Meryl	Meryl	k1gInSc4	Meryl
Streep	Streep	k1gInSc1	Streep
a	a	k8xC	a
Tommy	Tomma	k1gFnSc2	Tomma
Lee	Lea	k1gFnSc3	Lea
Jonesem	Jones	k1gMnSc7	Jones
<g/>
,	,	kIx,	,
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Druhá	druhý	k4xOgFnSc1	druhý
šance	šance	k1gFnSc1	šance
<g/>
.	.	kIx.	.
</s>
<s>
Kde	kde	k6eAd1	kde
ztvárnil	ztvárnit	k5eAaPmAgMnS	ztvárnit
manželského	manželský	k2eAgMnSc4d1	manželský
poradce	poradce	k1gMnSc4	poradce
doktora	doktor	k1gMnSc4	doktor
Bernarda	Bernard	k1gMnSc4	Bernard
Felba	Felb	k1gMnSc4	Felb
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc1	svůj
hlas	hlas	k1gInSc1	hlas
propůjčil	propůjčit	k5eAaPmAgInS	propůjčit
filmům	film	k1gInPc3	film
Já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
padouch	padouch	k1gMnSc1	padouch
a	a	k8xC	a
Já	já	k3xPp1nSc1	já
<g/>
,	,	kIx,	,
padouch	padouch	k1gMnSc1	padouch
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
získal	získat	k5eAaPmAgMnS	získat
roli	role	k1gFnSc4	role
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Hon	hon	k1gInSc4	hon
na	na	k7c4	na
lišku	liška	k1gFnSc4	liška
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
měl	mít	k5eAaImAgInS	mít
premiéru	premiéra	k1gFnSc4	premiéra
na	na	k7c6	na
Filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Cannes	Cannes	k1gNnSc6	Cannes
a	a	k8xC	a
získal	získat	k5eAaPmAgMnS	získat
pozitivní	pozitivní	k2eAgFnSc4d1	pozitivní
kritiku	kritika	k1gFnSc4	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
roli	role	k1gFnSc4	role
získal	získat	k5eAaPmAgMnS	získat
nominace	nominace	k1gFnSc2	nominace
na	na	k7c4	na
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
a	a	k8xC	a
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgInS	zahrát
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Emmy	Emma	k1gFnSc2	Emma
Stone	ston	k1gInSc5	ston
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Souboj	souboj	k1gInSc1	souboj
pohlaví	pohlaví	k1gNnSc2	pohlaví
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
si	se	k3xPyFc3	se
zahrál	zahrát	k5eAaPmAgInS	zahrát
Davida	David	k1gMnSc4	David
Sheffa	Sheff	k1gMnSc4	Sheff
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Beautiful	Beautifula	k1gFnPc2	Beautifula
Boy	boa	k1gFnSc2	boa
<g/>
,	,	kIx,	,
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Timothée	Timothé	k1gMnSc2	Timothé
Chalameta	Chalamet	k1gMnSc2	Chalamet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Filmografie	filmografie	k1gFnSc2	filmografie
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Film	film	k1gInSc1	film
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Televize	televize	k1gFnSc1	televize
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Videohry	videohra	k1gFnSc2	videohra
===	===	k?	===
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Steve	Steve	k1gMnSc1	Steve
Carell	Carell	k1gMnSc1	Carell
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Steve	Steve	k1gMnSc1	Steve
Carell	Carella	k1gFnPc2	Carella
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Steve	Steve	k1gMnSc1	Steve
Carell	Carell	k1gMnSc1	Carell
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Steve	Steve	k1gMnSc1	Steve
Carell	Carell	k1gMnSc1	Carell
na	na	k7c4	na
IMDB	IMDB	kA	IMDB
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Steve	Steve	k1gMnSc1	Steve
Carell	Carell	k1gMnSc1	Carell
On-line	Onin	k1gInSc5	On-lin
</s>
</p>
