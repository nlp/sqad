<s>
Garota	Garota	k1gFnSc1	Garota
<g/>
,	,	kIx,	,
též	též	k9	též
garotta	garotta	k1gMnSc1	garotta
nebo	nebo	k8xC	nebo
garote	garot	k1gInSc5	garot
<g/>
,	,	kIx,	,
ze	z	k7c2	z
šp	šp	k?	šp
<g/>
.	.	kIx.	.
garrote	garrot	k1gInSc5	garrot
je	být	k5eAaImIp3nS	být
popravčí	popravčí	k2eAgInSc4d1	popravčí
nástroj	nástroj	k1gInSc4	nástroj
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
v	v	k7c6	v
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
<g/>
,	,	kIx,	,
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejich	jejich	k3xOp3gFnPc6	jejich
koloniích	kolonie	k1gFnPc6	kolonie
a	a	k8xC	a
zemích	zem	k1gFnPc6	zem
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
byly	být	k5eAaImAgFnP	být
kdysi	kdysi	k6eAd1	kdysi
pod	pod	k7c7	pod
jejich	jejich	k3xOp3gInSc7	jejich
vlivem	vliv	k1gInSc7	vliv
<g/>
.	.	kIx.	.
</s>
