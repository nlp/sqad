<s>
Létající	létající	k2eAgMnSc1d1	létající
jaguár	jaguár	k1gMnSc1	jaguár
je	být	k5eAaImIp3nS	být
novela	novela	k1gFnSc1	novela
spisovatele	spisovatel	k1gMnSc2	spisovatel
Josefa	Josef	k1gMnSc2	Josef
Formánka	Formánek	k1gMnSc2	Formánek
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
Formánkova	Formánkův	k2eAgFnSc1d1	Formánkova
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
druhá	druhý	k4xOgFnSc1	druhý
kniha	kniha	k1gFnSc1	kniha
má	mít	k5eAaImIp3nS	mít
sice	sice	k8xC	sice
mnoho	mnoho	k4c4	mnoho
typických	typický	k2eAgInPc2d1	typický
znaků	znak	k1gInPc2	znak
autorova	autorův	k2eAgInSc2d1	autorův
stylu	styl	k1gInSc2	styl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
přece	přece	k9	přece
jen	jen	k9	jen
se	se	k3xPyFc4	se
od	od	k7c2	od
jeho	jeho	k3xOp3gFnSc2	jeho
tvorby	tvorba	k1gFnSc2	tvorba
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Rozdílná	rozdílný	k2eAgFnSc1d1	rozdílná
je	být	k5eAaImIp3nS	být
zejména	zejména	k9	zejména
kvůli	kvůli	k7c3	kvůli
časovému	časový	k2eAgInSc3d1	časový
horizontu	horizont	k1gInSc3	horizont
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Formánek	Formánek	k1gMnSc1	Formánek
své	svůj	k3xOyFgFnSc2	svůj
knihy	kniha	k1gFnSc2	kniha
tvoří	tvořit	k5eAaImIp3nP	tvořit
ne	ne	k9	ne
několik	několik	k4yIc4	několik
měsíců	měsíc	k1gInPc2	měsíc
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zpravidla	zpravidla	k6eAd1	zpravidla
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Svůj	svůj	k3xOyFgInSc4	svůj
debut	debut	k1gInSc4	debut
Prsatý	prsatý	k2eAgMnSc1d1	prsatý
muž	muž	k1gMnSc1	muž
a	a	k8xC	a
zloděj	zloděj	k1gMnSc1	zloděj
příběhů	příběh	k1gInPc2	příběh
psal	psát	k5eAaImAgMnS	psát
sedm	sedm	k4xCc1	sedm
let	léto	k1gNnPc2	léto
a	a	k8xC	a
zatím	zatím	k6eAd1	zatím
poslední	poslední	k2eAgInSc4d1	poslední
román	román	k1gInSc4	román
Mluviti	mluvit	k5eAaImF	mluvit
pravdu	pravda	k1gFnSc4	pravda
vznikal	vznikat	k5eAaImAgInS	vznikat
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Létající	létající	k2eAgMnSc1d1	létající
jaguár	jaguár	k1gMnSc1	jaguár
byl	být	k5eAaImAgMnS	být
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
Formánkových	Formánkových	k2eAgFnPc2d1	Formánkových
dalších	další	k2eAgFnPc2d1	další
knih	kniha	k1gFnPc2	kniha
ale	ale	k8xC	ale
dílem	dílo	k1gNnSc7	dílo
okamžiku	okamžik	k1gInSc2	okamžik
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
necelých	celý	k2eNgInPc2d1	necelý
dvaceti	dvacet	k4xCc2	dvacet
čtyř	čtyři	k4xCgInPc2	čtyři
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Nápad	nápad	k1gInSc1	nápad
napsat	napsat	k5eAaPmF	napsat
knihu	kniha	k1gFnSc4	kniha
v	v	k7c6	v
takto	takto	k6eAd1	takto
krátkém	krátký	k2eAgInSc6d1	krátký
čase	čas	k1gInSc6	čas
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
jako	jako	k9	jako
sázka	sázka	k1gFnSc1	sázka
mezi	mezi	k7c7	mezi
autorem	autor	k1gMnSc7	autor
a	a	k8xC	a
nakladatelem	nakladatel	k1gMnSc7	nakladatel
a	a	k8xC	a
umožnil	umožnit	k5eAaPmAgInS	umožnit
tak	tak	k6eAd1	tak
zapsání	zapsání	k1gNnSc4	zapsání
hned	hned	k6eAd1	hned
dvou	dva	k4xCgInPc2	dva
světových	světový	k2eAgInPc2d1	světový
rekordů	rekord	k1gInPc2	rekord
<g/>
.	.	kIx.	.
</s>
<s>
Prvního	první	k4xOgMnSc2	první
za	za	k7c4	za
nejrychleji	rychle	k6eAd3	rychle
napsanou	napsaný	k2eAgFnSc4d1	napsaná
knihu	kniha	k1gFnSc4	kniha
<g/>
,	,	kIx,	,
v	v	k7c6	v
čase	čas	k1gInSc6	čas
11	[number]	k4	11
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
19	[number]	k4	19
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc4	druhý
rekord	rekord	k1gInSc4	rekord
autor	autor	k1gMnSc1	autor
a	a	k8xC	a
nakladatel	nakladatel	k1gMnSc1	nakladatel
získali	získat	k5eAaPmAgMnP	získat
za	za	k7c4	za
nejrychlejší	rychlý	k2eAgNnSc4d3	nejrychlejší
napsání	napsání	k1gNnSc4	napsání
<g/>
,	,	kIx,	,
vydání	vydání	k1gNnSc4	vydání
a	a	k8xC	a
distribuci	distribuce	k1gFnSc4	distribuce
knihy	kniha	k1gFnSc2	kniha
<g/>
,	,	kIx,	,
v	v	k7c6	v
čase	čas	k1gInSc6	čas
23	[number]	k4	23
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
24	[number]	k4	24
minut	minuta	k1gFnPc2	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Sázka	sázka	k1gFnSc1	sázka
<g/>
,	,	kIx,	,
z	z	k7c2	z
níž	jenž	k3xRgFnSc2	jenž
vyústily	vyústit	k5eAaPmAgFnP	vyústit
zmíněné	zmíněný	k2eAgInPc4d1	zmíněný
rekordy	rekord	k1gInPc4	rekord
<g/>
,	,	kIx,	,
začala	začít	k5eAaPmAgFnS	začít
provokací	provokace	k1gFnSc7	provokace
nakladatele	nakladatel	k1gMnSc2	nakladatel
Iva	Ivo	k1gMnSc2	Ivo
Železného	Železný	k1gMnSc2	Železný
<g/>
.	.	kIx.	.
</s>
<s>
Formánek	Formánek	k1gMnSc1	Formánek
není	být	k5eNaImIp3nS	být
podle	podle	k7c2	podle
něho	on	k3xPp3gMnSc2	on
žádný	žádný	k1gMnSc1	žádný
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
když	když	k8xS	když
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
psát	psát	k5eAaImF	psát
knihy	kniha	k1gFnPc4	kniha
rychleji	rychle	k6eAd2	rychle
než	než	k8xS	než
v	v	k7c6	v
horizontu	horizont	k1gInSc6	horizont
několika	několik	k4yIc2	několik
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Formánek	Formánek	k1gMnSc1	Formánek
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
obranu	obrana	k1gFnSc4	obrana
uvedl	uvést	k5eAaPmAgMnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
knihu	kniha	k1gFnSc4	kniha
napíše	napsat	k5eAaBmIp3nS	napsat
klidně	klidně	k6eAd1	klidně
za	za	k7c4	za
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Nakladatel	nakladatel	k1gMnSc1	nakladatel
si	se	k3xPyFc3	se
ale	ale	k9	ale
přisadil	přisadit	k5eAaPmAgMnS	přisadit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
Formánek	Formánek	k1gMnSc1	Formánek
knihu	kniha	k1gFnSc4	kniha
napíše	napsat	k5eAaPmIp3nS	napsat
za	za	k7c4	za
dvanáct	dvanáct	k4xCc4	dvanáct
hodin	hodina	k1gFnPc2	hodina
<g/>
,	,	kIx,	,
on	on	k3xPp3gMnSc1	on
mu	on	k3xPp3gMnSc3	on
ji	on	k3xPp3gFnSc4	on
vydá	vydat	k5eAaPmIp3nS	vydat
za	za	k7c2	za
dalších	další	k2eAgFnPc2d1	další
dvanáct	dvanáct	k4xCc1	dvanáct
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Sázka	sázka	k1gFnSc1	sázka
o	o	k7c4	o
Formánkovy	Formánkův	k2eAgInPc4d1	Formánkův
vousy	vous	k1gInPc4	vous
a	a	k8xC	a
Železného	Železný	k1gMnSc4	Železný
vlasy	vlas	k1gInPc1	vlas
však	však	k9	však
skončila	skončit	k5eAaPmAgFnS	skončit
oboustranným	oboustranný	k2eAgNnSc7d1	oboustranné
vítězstvím	vítězství	k1gNnSc7	vítězství
<g/>
.	.	kIx.	.
</s>
<s>
Výtěžek	výtěžek	k1gInSc1	výtěžek
z	z	k7c2	z
prodaných	prodaný	k2eAgFnPc2d1	prodaná
knih	kniha	k1gFnPc2	kniha
byl	být	k5eAaImAgInS	být
navíc	navíc	k6eAd1	navíc
věnován	věnovat	k5eAaPmNgInS	věnovat
na	na	k7c4	na
dobrou	dobrý	k2eAgFnSc4d1	dobrá
věc	věc	k1gFnSc4	věc
-	-	kIx~	-
nově	nově	k6eAd1	nově
otevřený	otevřený	k2eAgInSc1d1	otevřený
pavilon	pavilon	k1gInSc1	pavilon
pro	pro	k7c4	pro
slony	slon	k1gMnPc4	slon
v	v	k7c6	v
ZOO	zoo	k1gFnSc6	zoo
v	v	k7c6	v
Ústí	ústí	k1gNnSc6	ústí
nad	nad	k7c7	nad
Labem	Labe	k1gNnSc7	Labe
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
je	být	k5eAaImIp3nS	být
originální	originální	k2eAgInPc4d1	originální
také	také	k9	také
způsobem	způsob	k1gInSc7	způsob
vzniku	vznik	k1gInSc2	vznik
a	a	k8xC	a
námětem	námět	k1gInSc7	námět
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgNnSc3	jenž
přispěli	přispět	k5eAaPmAgMnP	přispět
samotní	samotný	k2eAgMnPc1d1	samotný
čtenáři	čtenář	k1gMnPc1	čtenář
<g/>
.	.	kIx.	.
</s>
<s>
Poslali	poslat	k5eAaPmAgMnP	poslat
téměř	téměř	k6eAd1	téměř
čtyři	čtyři	k4xCgNnPc4	čtyři
sta	sto	k4xCgNnPc4	sto
návrhů	návrh	k1gInPc2	návrh
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yQgFnPc2	který
náhodně	náhodně	k6eAd1	náhodně
vybraní	vybraný	k2eAgMnPc1d1	vybraný
lidé	člověk	k1gMnPc1	člověk
vylosovali	vylosovat	k5eAaPmAgMnP	vylosovat
tři	tři	k4xCgInPc4	tři
hlavní	hlavní	k2eAgInPc4d1	hlavní
motivy	motiv	k1gInPc4	motiv
knihy	kniha	k1gFnSc2	kniha
<g/>
:	:	kIx,	:
Marnost	marnost	k1gFnSc1	marnost
<g/>
,	,	kIx,	,
Největší	veliký	k2eAgFnSc1d3	veliký
láska	láska	k1gFnSc1	láska
a	a	k8xC	a
Cestování	cestování	k1gNnSc1	cestování
a	a	k8xC	a
hromadná	hromadný	k2eAgFnSc1d1	hromadná
městská	městský	k2eAgFnSc1d1	městská
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Spisovatel	spisovatel	k1gMnSc1	spisovatel
knihu	kniha	k1gFnSc4	kniha
tvořil	tvořit	k5eAaImAgMnS	tvořit
<g/>
,	,	kIx,	,
sedíce	sedit	k5eAaImSgFnP	sedit
společně	společně	k6eAd1	společně
se	se	k3xPyFc4	se
svým	svůj	k3xOyFgMnSc7	svůj
dvorním	dvorní	k2eAgMnSc7d1	dvorní
ilustrátorem	ilustrátor	k1gMnSc7	ilustrátor
Daliborem	Dalibor	k1gMnSc7	Dalibor
Nesnídalem	Nesnídal	k1gMnSc7	Nesnídal
a	a	k8xC	a
odpovědnou	odpovědný	k2eAgFnSc7d1	odpovědná
redaktorkou	redaktorka	k1gFnSc7	redaktorka
Janou	Jana	k1gFnSc7	Jana
Žofkovou	Žofková	k1gFnSc7	Žofková
přímo	přímo	k6eAd1	přímo
za	za	k7c7	za
výlohou	výloha	k1gFnSc7	výloha
pražského	pražský	k2eAgNnSc2d1	Pražské
knihkupectví	knihkupectví	k1gNnSc2	knihkupectví
Luxor	Luxora	k1gFnPc2	Luxora
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
]	]	kIx)	]
Postavami	postava	k1gFnPc7	postava
novely	novela	k1gFnPc4	novela
jsou	být	k5eAaImIp3nP	být
vypravěč	vypravěč	k1gMnSc1	vypravěč
-	-	kIx~	-
autor	autor	k1gMnSc1	autor
Josef	Josef	k1gMnSc1	Josef
Formánek	Formánek	k1gMnSc1	Formánek
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc1	jeho
kamarád	kamarád	k1gMnSc1	kamarád
dobrodruh	dobrodruh	k1gMnSc1	dobrodruh
Jorge	Jorge	k1gFnPc2	Jorge
Kill	Kill	k1gMnSc1	Kill
<g/>
,	,	kIx,	,
medička	medička	k1gFnSc1	medička
<g/>
,	,	kIx,	,
taxikář	taxikář	k1gMnSc1	taxikář
<g/>
,	,	kIx,	,
heroinista	heroinista	k1gMnSc1	heroinista
a	a	k8xC	a
anděl	anděl	k1gMnSc1	anděl
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
společně	společně	k6eAd1	společně
s	s	k7c7	s
létajícím	létající	k2eAgMnSc7d1	létající
jaguárem	jaguár	k1gMnSc7	jaguár
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
mystično	mystično	k1gNnSc1	mystično
díla	dílo	k1gNnSc2	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Osudy	osud	k1gInPc1	osud
postav	postava	k1gFnPc2	postava
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
proplétají	proplétat	k5eAaImIp3nP	proplétat
a	a	k8xC	a
vrcholí	vrcholit	k5eAaImIp3nP	vrcholit
téměř	téměř	k6eAd1	téměř
ve	v	k7c6	v
stejném	stejný	k2eAgInSc6d1	stejný
momentě	moment	k1gInSc6	moment
prozření	prozření	k1gNnSc2	prozření
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
k	k	k7c3	k
propletení	propletení	k1gNnSc3	propletení
nedochází	docházet	k5eNaImIp3nS	docházet
jen	jen	k9	jen
náhodou	náhoda	k1gFnSc7	náhoda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vlivem	vliv	k1gInSc7	vliv
anděla	anděl	k1gMnSc2	anděl
strážného	strážný	k1gMnSc2	strážný
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
byl	být	k5eAaImAgInS	být
seslán	seslat	k5eAaPmNgMnS	seslat
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
létajícím	létající	k2eAgMnSc7d1	létající
jaguárem	jaguár	k1gMnSc7	jaguár
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
lze	lze	k6eAd1	lze
rozdělit	rozdělit	k5eAaPmF	rozdělit
na	na	k7c4	na
2	[number]	k4	2
příběhy	příběh	k1gInPc4	příběh
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
knihy	kniha	k1gFnSc2	kniha
spojí	spojit	k5eAaPmIp3nS	spojit
v	v	k7c4	v
jeden	jeden	k4xCgInSc4	jeden
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
<g/>
,	,	kIx,	,
vycházející	vycházející	k2eAgFnSc1d1	vycházející
z	z	k7c2	z
Formánkových	Formánkových	k2eAgFnPc2d1	Formánkových
cest	cesta	k1gFnPc2	cesta
<g/>
,	,	kIx,	,
popisuje	popisovat	k5eAaImIp3nS	popisovat
autorovo	autorův	k2eAgNnSc4d1	autorovo
přátelství	přátelství	k1gNnSc4	přátelství
s	s	k7c7	s
dobrodruhem	dobrodruh	k1gMnSc7	dobrodruh
Jorgem	Jorg	k1gMnSc7	Jorg
Killem	Kill	k1gMnSc7	Kill
a	a	k8xC	a
jejich	jejich	k3xOp3gInPc4	jejich
společné	společný	k2eAgInPc4d1	společný
zážitky	zážitek	k1gInPc4	zážitek
v	v	k7c6	v
Amazonii	Amazonie	k1gFnSc6	Amazonie
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
poprvé	poprvé	k6eAd1	poprvé
setkáváme	setkávat	k5eAaImIp1nP	setkávat
také	také	k9	také
s	s	k7c7	s
létajícím	létající	k2eAgMnSc7d1	létající
jaguárem	jaguár	k1gMnSc7	jaguár
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
nadpozemskou	nadpozemský	k2eAgFnSc4d1	nadpozemská
sílu	síla	k1gFnSc4	síla
ho	on	k3xPp3gInSc2	on
přestavuje	přestavovat	k5eAaImIp3nS	přestavovat
Jorge	Jorge	k1gInSc1	Jorge
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
neuvěřitelných	uvěřitelný	k2eNgFnPc2d1	neuvěřitelná
historek	historka	k1gFnPc2	historka
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Nesmál	smát	k5eNaImAgMnS	smát
bych	by	kYmCp1nS	by
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
létající	létající	k2eAgMnSc1d1	létající
jaguár	jaguár	k1gMnSc1	jaguár
potrestá	potrestat	k5eAaPmIp3nS	potrestat
každého	každý	k3xTgMnSc2	každý
posměváčka	posměváček	k1gMnSc2	posměváček
<g/>
.	.	kIx.	.
</s>
<s>
Nesnáší	snášet	k5eNaImIp3nS	snášet
výsměch	výsměch	k1gInSc4	výsměch
<g/>
,	,	kIx,	,
neúctu	neúcta	k1gFnSc4	neúcta
a	a	k8xC	a
lež	lež	k1gFnSc4	lež
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
varoval	varovat	k5eAaImAgMnS	varovat
mě	já	k3xPp1nSc4	já
Jorge	Jorg	k1gMnSc4	Jorg
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Druhý	druhý	k4xOgInSc1	druhý
příběh	příběh	k1gInSc1	příběh
spojuje	spojovat	k5eAaImIp3nS	spojovat
osudy	osud	k1gInPc4	osud
všech	všecek	k3xTgFnPc2	všecek
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Medičky	medička	k1gFnPc1	medička
<g/>
,	,	kIx,	,
hledající	hledající	k2eAgInPc1d1	hledající
smysl	smysl	k1gInSc4	smysl
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Jorgeho	Jorge	k1gMnSc2	Jorge
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
medičce	medička	k1gFnSc3	medička
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
žít	žít	k5eAaImF	žít
<g/>
.	.	kIx.	.
</s>
<s>
Feťáka	Feťáka	k?	Feťáka
<g/>
,	,	kIx,	,
shánějícího	shánějící	k2eAgNnSc2d1	shánějící
zoufale	zoufale	k6eAd1	zoufale
peníze	peníz	k1gInPc4	peníz
na	na	k7c4	na
dávku	dávka	k1gFnSc4	dávka
<g/>
.	.	kIx.	.
</s>
<s>
Taxikáře	taxikář	k1gMnSc2	taxikář
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
je	být	k5eAaImIp3nS	být
feťákem	feťákem	k?	feťákem
pobodán	pobodat	k5eAaPmNgMnS	pobodat
a	a	k8xC	a
medičkou	medička	k1gFnSc7	medička
zachráněn	zachránit	k5eAaPmNgMnS	zachránit
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
anděla	anděl	k1gMnSc4	anděl
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
seslán	seslat	k5eAaPmNgMnS	seslat
létajícím	létající	k2eAgMnSc7d1	létající
jaguárem	jaguár	k1gMnSc7	jaguár
<g/>
,	,	kIx,	,
a	a	k8xC	a
jenž	jenž	k3xRgMnSc1	jenž
všem	všecek	k3xTgMnPc3	všecek
pomůže	pomoct	k5eAaPmIp3nS	pomoct
najít	najít	k5eAaPmF	najít
<g/>
,	,	kIx,	,
to	ten	k3xDgNnSc4	ten
co	co	k9	co
hledali	hledat	k5eAaImAgMnP	hledat
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
novelu	novela	k1gFnSc4	novela
otevírá	otevírat	k5eAaImIp3nS	otevírat
zážitky	zážitek	k1gInPc4	zážitek
z	z	k7c2	z
jeho	jeho	k3xOp3gNnSc2	jeho
cestování	cestování	k1gNnSc2	cestování
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
důvtipně	důvtipně	k6eAd1	důvtipně
obchází	obcházet	k5eAaImIp3nS	obcházet
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
vylosovaných	vylosovaný	k2eAgNnPc2d1	vylosované
témat	téma	k1gNnPc2	téma
-	-	kIx~	-
"	"	kIx"	"
<g/>
největší	veliký	k2eAgFnSc1d3	veliký
láska	láska	k1gFnSc1	láska
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yIgFnSc4	který
považuje	považovat	k5eAaImIp3nS	považovat
právě	právě	k9	právě
cestování	cestování	k1gNnSc1	cestování
<g/>
.	.	kIx.	.
</s>
<s>
Popisuje	popisovat	k5eAaImIp3nS	popisovat
historky	historka	k1gFnPc4	historka
z	z	k7c2	z
Amazonského	amazonský	k2eAgInSc2d1	amazonský
pralesa	prales	k1gInSc2	prales
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
potkal	potkat	k5eAaPmAgInS	potkat
Jorge	Jorge	k1gFnSc4	Jorge
Killa	Killo	k1gNnSc2	Killo
<g/>
.	.	kIx.	.
</s>
<s>
Čecha	Čech	k1gMnSc2	Čech
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
Sovětském	sovětský	k2eAgInSc6d1	sovětský
svazu	svaz	k1gInSc6	svaz
a	a	k8xC	a
pak	pak	k6eAd1	pak
coby	coby	k?	coby
ruský	ruský	k2eAgMnSc1d1	ruský
agent	agent	k1gMnSc1	agent
na	na	k7c6	na
Antarktidě	Antarktida	k1gFnSc6	Antarktida
<g/>
.	.	kIx.	.
</s>
<s>
Odtud	odtud	k6eAd1	odtud
se	se	k3xPyFc4	se
přes	přes	k7c4	přes
chilskou	chilský	k2eAgFnSc4d1	chilská
polární	polární	k2eAgFnSc4d1	polární
stanici	stanice	k1gFnSc4	stanice
dostal	dostat	k5eAaPmAgMnS	dostat
do	do	k7c2	do
Jižní	jižní	k2eAgFnSc2d1	jižní
Ameriky	Amerika	k1gFnSc2	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Žil	žít	k5eAaImAgMnS	žít
v	v	k7c6	v
Peru	Peru	k1gNnSc6	Peru
<g/>
,	,	kIx,	,
Brazílii	Brazílie	k1gFnSc6	Brazílie
<g/>
,	,	kIx,	,
Venezuele	Venezuela	k1gFnSc6	Venezuela
a	a	k8xC	a
Kolumbii	Kolumbie	k1gFnSc6	Kolumbie
<g/>
.	.	kIx.	.
</s>
<s>
Formánek	Formánek	k1gMnSc1	Formánek
tomuto	tento	k3xDgMnSc3	tento
dobrodruhovi	dobrodruh	k1gMnSc3	dobrodruh
samozřejmě	samozřejmě	k6eAd1	samozřejmě
nevěří	věřit	k5eNaImIp3nS	věřit
ani	ani	k8xC	ani
polovinu	polovina	k1gFnSc4	polovina
z	z	k7c2	z
jeho	jeho	k3xOp3gFnPc2	jeho
historek	historka	k1gFnPc2	historka
<g/>
,	,	kIx,	,
Jorge	Jorge	k1gNnSc1	Jorge
ho	on	k3xPp3gMnSc4	on
častým	častý	k2eAgNnPc3d1	časté
poučováním	poučování	k1gNnPc3	poučování
rozčiluje	rozčilovat	k5eAaImIp3nS	rozčilovat
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
ale	ale	k9	ale
Formánek	Formánek	k1gMnSc1	Formánek
<g/>
,	,	kIx,	,
po	po	k7c6	po
pár	pár	k4xCyI	pár
společných	společný	k2eAgInPc6d1	společný
zážitcích	zážitek	k1gInPc6	zážitek
<g/>
,	,	kIx,	,
uznává	uznávat	k5eAaImIp3nS	uznávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Jorge	Jorge	k1gInSc1	Jorge
vážně	vážně	k6eAd1	vážně
nepřeháněl	přehánět	k5eNaImAgInS	přehánět
<g/>
.	.	kIx.	.
</s>
<s>
Neváhal	váhat	k5eNaImAgMnS	váhat
například	například	k6eAd1	například
plavat	plavat	k5eAaImF	plavat
za	za	k7c7	za
anakondou	anakonda	k1gFnSc7	anakonda
a	a	k8xC	a
vytáhnout	vytáhnout	k5eAaPmF	vytáhnout
její	její	k3xOp3gInSc4	její
ocas	ocas	k1gInSc4	ocas
z	z	k7c2	z
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ji	on	k3xPp3gFnSc4	on
Formánek	Formánek	k1gMnSc1	Formánek
mohl	moct	k5eAaImAgMnS	moct
natočit	natočit	k5eAaBmF	natočit
na	na	k7c4	na
kameru	kamera	k1gFnSc4	kamera
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgMnSc1d1	poslední
z	z	k7c2	z
neuvěřitelných	uvěřitelný	k2eNgInPc2d1	neuvěřitelný
Jorgeho	Jorgeha	k1gFnSc5	Jorgeha
historek	historka	k1gFnPc2	historka
vyprávěla	vyprávět	k5eAaImAgFnS	vyprávět
o	o	k7c6	o
létajícím	létající	k2eAgMnSc6d1	létající
jaguárovi	jaguár	k1gMnSc6	jaguár
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
pomohl	pomoct	k5eAaPmAgMnS	pomoct
jednomu	jeden	k4xCgMnSc3	jeden
z	z	k7c2	z
indiánů	indián	k1gMnPc2	indián
<g/>
.	.	kIx.	.
</s>
<s>
Nikdo	nikdo	k3yNnSc1	nikdo
se	se	k3xPyFc4	se
ale	ale	k9	ale
jaguárovi	jaguár	k1gMnSc3	jaguár
nemá	mít	k5eNaImIp3nS	mít
posmívat	posmívat	k5eAaImF	posmívat
<g/>
,	,	kIx,	,
jinak	jinak	k6eAd1	jinak
ho	on	k3xPp3gMnSc4	on
stihne	stihnout	k5eAaPmIp3nS	stihnout
trest	trest	k1gInSc1	trest
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Čechách	Čechy	k1gFnPc6	Čechy
jaguár	jaguár	k1gMnSc1	jaguár
sesílá	sesílat	k5eAaImIp3nS	sesílat
na	na	k7c4	na
zem	zem	k1gFnSc4	zem
anděla	anděl	k1gMnSc2	anděl
strážného	strážný	k1gMnSc2	strážný
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
a	a	k8xC	a
trestá	trestat	k5eAaImIp3nS	trestat
ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
tímto	tento	k3xDgMnSc7	tento
andělem	anděl	k1gMnSc7	anděl
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
setkávají	setkávat	k5eAaImIp3nP	setkávat
další	další	k2eAgFnPc1d1	další
postavy	postava	k1gFnPc1	postava
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
novely	novela	k1gFnSc2	novela
<g/>
.	.	kIx.	.
</s>
<s>
Děj	děj	k1gInSc1	děj
už	už	k6eAd1	už
je	být	k5eAaImIp3nS	být
zasazen	zasadit	k5eAaPmNgInS	zasadit
do	do	k7c2	do
českého	český	k2eAgNnSc2d1	české
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
konkrétně	konkrétně	k6eAd1	konkrétně
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Jorge	Jorge	k6eAd1	Jorge
se	se	k3xPyFc4	se
vrací	vracet	k5eAaImIp3nS	vracet
do	do	k7c2	do
Čech	Čechy	k1gFnPc2	Čechy
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
cest	cesta	k1gFnPc2	cesta
a	a	k8xC	a
setkává	setkávat	k5eAaImIp3nS	setkávat
se	se	k3xPyFc4	se
s	s	k7c7	s
Formánkem	Formánek	k1gMnSc7	Formánek
<g/>
.	.	kIx.	.
</s>
<s>
Opilý	opilý	k2eAgInSc1d1	opilý
Jorge	Jorge	k1gInSc1	Jorge
pozná	poznat	k5eAaPmIp3nS	poznat
smutnou	smutný	k2eAgFnSc4d1	smutná
medičku	medička	k1gFnSc4	medička
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
postrádá	postrádat	k5eAaImIp3nS	postrádat
motivaci	motivace	k1gFnSc4	motivace
k	k	k7c3	k
životu	život	k1gInSc3	život
<g/>
,	,	kIx,	,
prožívá	prožívat	k5eAaImIp3nS	prožívat
jen	jen	k9	jen
marnost	marnost	k1gFnSc1	marnost
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
příběhu	příběh	k1gInSc2	příběh
tedy	tedy	k9	tedy
vstupuje	vstupovat	k5eAaImIp3nS	vstupovat
druhé	druhý	k4xOgNnSc1	druhý
vylosované	vylosovaný	k2eAgNnSc1d1	vylosované
téma	téma	k1gNnSc1	téma
-	-	kIx~	-
marnost	marnost	k1gFnSc1	marnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
se	se	k3xPyFc4	se
medička	medička	k1gFnSc1	medička
Jorgemu	Jorgem	k1gInSc2	Jorgem
svěří	svěřit	k5eAaPmIp3nS	svěřit
<g/>
,	,	kIx,	,
že	že	k8xS	že
přemýšlí	přemýšlet	k5eAaImIp3nS	přemýšlet
nad	nad	k7c7	nad
sebevraždou	sebevražda	k1gFnSc7	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
Jorge	Jorge	k1gFnSc1	Jorge
jí	on	k3xPp3gFnSc3	on
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tím	ten	k3xDgNnSc7	ten
rozzlobí	rozzlobit	k5eAaPmIp3nS	rozzlobit
létajícího	létající	k2eAgMnSc4d1	létající
jaguára	jaguár	k1gMnSc4	jaguár
<g/>
.	.	kIx.	.
</s>
<s>
Pak	pak	k6eAd1	pak
si	se	k3xPyFc3	se
uvědomuje	uvědomovat	k5eAaImIp3nS	uvědomovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
když	když	k8xS	když
ji	on	k3xPp3gFnSc4	on
takto	takto	k6eAd1	takto
slyšel	slyšet	k5eAaImAgMnS	slyšet
mluvit	mluvit	k5eAaImF	mluvit
<g/>
,	,	kIx,	,
určitě	určitě	k6eAd1	určitě
jí	on	k3xPp3gFnSc3	on
pošle	poslat	k5eAaPmIp3nS	poslat
jejího	její	k3xOp3gMnSc2	její
anděla	anděl	k1gMnSc2	anděl
<g/>
.	.	kIx.	.
</s>
<s>
Anděl	Anděl	k1gMnSc1	Anděl
zasáhne	zasáhnout	k5eAaPmIp3nS	zasáhnout
do	do	k7c2	do
jejího	její	k3xOp3gInSc2	její
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
i	i	k8xC	i
života	život	k1gInSc2	život
dalších	další	k2eAgFnPc2d1	další
postav	postava	k1gFnPc2	postava
<g/>
.	.	kIx.	.
</s>
<s>
Těmi	ten	k3xDgInPc7	ten
jsou	být	k5eAaImIp3nP	být
taxikář	taxikář	k1gMnSc1	taxikář
(	(	kIx(	(
<g/>
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
společně	společně	k6eAd1	společně
s	s	k7c7	s
tramvají	tramvaj	k1gFnSc7	tramvaj
další	další	k2eAgNnSc1d1	další
vylosované	vylosovaný	k2eAgNnSc1d1	vylosované
téma	téma	k1gNnSc1	téma
-	-	kIx~	-
MHD	MHD	kA	MHD
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
)	)	kIx)	)
a	a	k8xC	a
feťák	feťák	k?	feťák
<g/>
.	.	kIx.	.
</s>
<s>
Heroinista	Heroinista	k1gMnSc1	Heroinista
zoufale	zoufale	k6eAd1	zoufale
potřebuje	potřebovat	k5eAaImIp3nS	potřebovat
dávku	dávka	k1gFnSc4	dávka
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yRgFnSc4	který
nemá	mít	k5eNaImIp3nS	mít
peníze	peníz	k1gInPc1	peníz
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
pobodá	pobodat	k5eAaPmIp3nS	pobodat
a	a	k8xC	a
okrade	okrást	k5eAaPmIp3nS	okrást
taxikáře	taxikář	k1gMnPc4	taxikář
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
již	již	k6eAd1	již
předtím	předtím	k6eAd1	předtím
setkal	setkat	k5eAaPmAgMnS	setkat
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
andělem	anděl	k1gMnSc7	anděl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nevěnoval	věnovat	k5eNaImAgMnS	věnovat
jeho	jeho	k3xOp3gFnPc3	jeho
radám	rada	k1gFnPc3	rada
pozornost	pozornost	k1gFnSc4	pozornost
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pobodání	pobodání	k1gNnSc6	pobodání
si	se	k3xPyFc3	se
na	na	k7c4	na
ně	on	k3xPp3gNnSc4	on
vzpomene	vzpomenout	k5eAaPmIp3nS	vzpomenout
a	a	k8xC	a
z	z	k7c2	z
posledních	poslední	k2eAgFnPc2d1	poslední
sil	síla	k1gFnPc2	síla
dojede	dojet	k5eAaPmIp3nS	dojet
na	na	k7c4	na
místo	místo	k1gNnSc4	místo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mu	on	k3xPp3gMnSc3	on
anděl	anděl	k1gMnSc1	anděl
radil	radit	k5eAaImAgInS	radit
<g/>
.	.	kIx.	.
</s>
<s>
Tam	tam	k6eAd1	tam
jako	jako	k8xS	jako
náhodou	náhodou	k6eAd1	náhodou
čeká	čekat	k5eAaImIp3nS	čekat
na	na	k7c4	na
tramvaj	tramvaj	k1gFnSc4	tramvaj
medička	medička	k1gFnSc1	medička
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mu	on	k3xPp3gMnSc3	on
zachrání	zachránit	k5eAaPmIp3nS	zachránit
život	život	k1gInSc4	život
<g/>
.	.	kIx.	.
</s>
<s>
Potažmo	potažmo	k6eAd1	potažmo
i	i	k8xC	i
sobě	se	k3xPyFc3	se
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
najde	najít	k5eAaPmIp3nS	najít
smysl	smysl	k1gInSc4	smysl
života	život	k1gInSc2	život
a	a	k8xC	a
v	v	k7c6	v
taxikáři	taxikář	k1gMnSc6	taxikář
také	také	k9	také
partnera	partner	k1gMnSc2	partner
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
potká	potkat	k5eAaPmIp3nS	potkat
anděla	anděl	k1gMnSc4	anděl
i	i	k8xC	i
feťák	feťák	k?	feťák
<g/>
,	,	kIx,	,
poslechne	poslechnout	k5eAaPmIp3nS	poslechnout
jeho	jeho	k3xOp3gFnSc4	jeho
radu	rada	k1gFnSc4	rada
<g/>
,	,	kIx,	,
skočí	skočit	k5eAaPmIp3nS	skočit
dolů	dolů	k6eAd1	dolů
z	z	k7c2	z
vysoké	vysoký	k2eAgFnSc2d1	vysoká
terasy	terasa	k1gFnSc2	terasa
a	a	k8xC	a
zemře	zemřít	k5eAaPmIp3nS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Spravedlnosti	spravedlnost	k1gFnSc2	spravedlnost
je	být	k5eAaImIp3nS	být
učiněno	učinit	k5eAaImNgNnS	učinit
zadost	zadost	k6eAd1	zadost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
knihy	kniha	k1gFnSc2	kniha
se	se	k3xPyFc4	se
opět	opět	k6eAd1	opět
vracíme	vracet	k5eAaImIp1nP	vracet
k	k	k7c3	k
Jorgemu	Jorgem	k1gInSc3	Jorgem
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
Formánkovi	Formánek	k1gMnSc3	Formánek
posílá	posílat	k5eAaImIp3nS	posílat
k	k	k7c3	k
Vánocům	Vánoce	k1gFnPc3	Vánoce
pohled	pohled	k1gInSc4	pohled
a	a	k8xC	a
dárky	dárek	k1gInPc4	dárek
z	z	k7c2	z
Venezuely	Venezuela	k1gFnSc2	Venezuela
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
díky	díky	k7c3	díky
svému	svůj	k3xOyFgMnSc3	svůj
andělu	anděl	k1gMnSc3	anděl
strážnému	strážný	k1gMnSc3	strážný
<g/>
.	.	kIx.	.
</s>
