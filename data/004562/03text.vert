<s>
Žebro	žebro	k1gNnSc1	žebro
(	(	kIx(	(
<g/>
latinsky	latinsky	k6eAd1	latinsky
costa	costa	k1gFnSc1	costa
<g/>
,	,	kIx,	,
-ae	e	k?	-ae
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
obloukovitá	obloukovitý	k2eAgFnSc1d1	obloukovitá
kost	kost	k1gFnSc1	kost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
na	na	k7c6	na
zádech	záda	k1gNnPc6	záda
připojuje	připojovat	k5eAaImIp3nS	připojovat
k	k	k7c3	k
hrudním	hrudní	k2eAgInPc3d1	hrudní
obratlům	obratel	k1gInPc3	obratel
a	a	k8xC	a
v	v	k7c6	v
přední	přední	k2eAgFnSc6d1	přední
části	část	k1gFnSc6	část
je	být	k5eAaImIp3nS	být
chrupavkou	chrupavka	k1gFnSc7	chrupavka
připojena	připojit	k5eAaPmNgFnS	připojit
k	k	k7c3	k
hrudní	hrudní	k2eAgFnSc3d1	hrudní
kosti	kost	k1gFnSc3	kost
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
pro	pro	k7c4	pro
pokročilejší	pokročilý	k2eAgMnPc4d2	pokročilejší
obratlovce	obratlovec	k1gMnPc4	obratlovec
<g/>
;	;	kIx,	;
mihule	mihule	k1gFnSc1	mihule
žádná	žádný	k3yNgNnPc1	žádný
žebra	žebro	k1gNnPc1	žebro
nemají	mít	k5eNaImIp3nP	mít
<g/>
,	,	kIx,	,
paryby	paryba	k1gFnPc1	paryba
mají	mít	k5eAaImIp3nP	mít
jen	jen	k9	jen
velmi	velmi	k6eAd1	velmi
drobná	drobný	k2eAgNnPc1d1	drobné
žebra	žebro	k1gNnPc1	žebro
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
ryb	ryba	k1gFnPc2	ryba
je	být	k5eAaImIp3nS	být
již	již	k6eAd1	již
má	mít	k5eAaImIp3nS	mít
dobře	dobře	k6eAd1	dobře
vyvinuta	vyvinout	k5eAaPmNgFnS	vyvinout
<g/>
.	.	kIx.	.
</s>
<s>
Člověk	člověk	k1gMnSc1	člověk
má	mít	k5eAaImIp3nS	mít
běžně	běžně	k6eAd1	běžně
12	[number]	k4	12
párů	pár	k1gInPc2	pár
žeber	žebro	k1gNnPc2	žebro
<g/>
,	,	kIx,	,
občas	občas	k6eAd1	občas
se	se	k3xPyFc4	se
ale	ale	k9	ale
narodí	narodit	k5eAaPmIp3nP	narodit
jedinci	jedinec	k1gMnPc1	jedinec
s	s	k7c7	s
11	[number]	k4	11
nebo	nebo	k8xC	nebo
13	[number]	k4	13
páry	pár	k1gInPc1	pár
<g/>
.	.	kIx.	.
</s>
<s>
Žebra	žebro	k1gNnPc1	žebro
chrání	chránit	k5eAaImIp3nP	chránit
před	před	k7c7	před
mechanickým	mechanický	k2eAgNnSc7d1	mechanické
poškozením	poškození	k1gNnSc7	poškození
životně	životně	k6eAd1	životně
důležité	důležitý	k2eAgInPc1d1	důležitý
orgány	orgán	k1gInPc1	orgán
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
je	on	k3xPp3gNnPc4	on
srdce	srdce	k1gNnPc4	srdce
či	či	k8xC	či
plíce	plíce	k1gFnPc4	plíce
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
prvních	první	k4xOgInPc2	první
7	[number]	k4	7
párů	pár	k1gInPc2	pár
žeber	žebro	k1gNnPc2	žebro
připojuje	připojovat	k5eAaImIp3nS	připojovat
chrupavka	chrupavka	k1gFnSc1	chrupavka
žebra	žebro	k1gNnSc2	žebro
přímo	přímo	k6eAd1	přímo
k	k	k7c3	k
hrudní	hrudní	k2eAgFnSc3d1	hrudní
kosti	kost	k1gFnSc3	kost
–	–	k?	–
jde	jít	k5eAaImIp3nS	jít
proto	proto	k8xC	proto
o	o	k7c6	o
tzv.	tzv.	kA	tzv.
pravá	pravý	k2eAgFnSc1d1	pravá
žebra	žebro	k1gNnPc1	žebro
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
costae	costae	k1gInSc1	costae
verae	verae	k1gInSc1	verae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
3	[number]	k4	3
páry	pára	k1gFnPc1	pára
jsou	být	k5eAaImIp3nP	být
chrupavkami	chrupavka	k1gFnPc7	chrupavka
spojeny	spojit	k5eAaPmNgFnP	spojit
s	s	k7c7	s
výše	vysoce	k6eAd2	vysoce
uloženými	uložený	k2eAgInPc7d1	uložený
pravými	pravý	k2eAgInPc7d1	pravý
žebry	žebr	k1gInPc7	žebr
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
3	[number]	k4	3
páry	pára	k1gFnPc1	pára
tvoří	tvořit	k5eAaImIp3nP	tvořit
tzv.	tzv.	kA	tzv.
nepravá	pravý	k2eNgNnPc1d1	nepravé
žebra	žebro	k1gNnPc1	žebro
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
costae	costae	k1gInSc1	costae
spuriae	spuriae	k1gInSc1	spuriae
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInPc1d1	poslední
dva	dva	k4xCgInPc1	dva
páry	pár	k1gInPc1	pár
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
nespojeny	spojit	k5eNaPmNgInP	spojit
–	–	k?	–
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
volná	volný	k2eAgNnPc4d1	volné
žebra	žebro	k1gNnPc4	žebro
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
costae	costae	k1gInSc1	costae
fluctuantes	fluctuantes	k1gInSc1	fluctuantes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
končící	končící	k2eAgInSc1d1	končící
mezi	mezi	k7c4	mezi
svaly	sval	k1gInPc4	sval
břišní	břišní	k2eAgFnSc2d1	břišní
stěny	stěna	k1gFnSc2	stěna
<g/>
.	.	kIx.	.
</s>
<s>
Dohromady	dohromady	k6eAd1	dohromady
tvoří	tvořit	k5eAaImIp3nP	tvořit
hrudník	hrudník	k1gInSc4	hrudník
<g/>
.	.	kIx.	.
</s>
<s>
Žebra	žebro	k1gNnPc1	žebro
jsou	být	k5eAaImIp3nP	být
náchylná	náchylný	k2eAgNnPc1d1	náchylné
ke	k	k7c3	k
zlomeninám	zlomenina	k1gFnPc3	zlomenina
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
pádu	pád	k1gInSc2	pád
nebo	nebo	k8xC	nebo
úderu	úder	k1gInSc2	úder
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
někdy	někdy	k6eAd1	někdy
i	i	k9	i
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
dlouhotrvajícího	dlouhotrvající	k2eAgInSc2d1	dlouhotrvající
kašle	kašel	k1gInSc2	kašel
či	či	k8xC	či
smíchu	smích	k1gInSc2	smích
<g/>
.	.	kIx.	.
</s>
<s>
Fraktura	fraktura	k1gFnSc1	fraktura
bývá	bývat	k5eAaImIp3nS	bývat
bolestivá	bolestivý	k2eAgFnSc1d1	bolestivá
při	při	k7c6	při
nadechování	nadechování	k1gNnSc6	nadechování
a	a	k8xC	a
vydechování	vydechování	k1gNnSc6	vydechování
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
mohou	moct	k5eAaImIp3nP	moct
žebra	žebro	k1gNnPc4	žebro
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
benigní	benigní	k2eAgInPc4d1	benigní
nádory	nádor	k1gInPc4	nádor
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
metastázující	metastázující	k2eAgInPc4d1	metastázující
maligní	maligní	k2eAgInPc4d1	maligní
nádory	nádor	k1gInPc4	nádor
<g/>
.	.	kIx.	.
</s>
<s>
Hrudní	hrudní	k2eAgInSc1d1	hrudní
koš	koš	k1gInSc1	koš
tvoří	tvořit	k5eAaImIp3nS	tvořit
<g/>
:	:	kIx,	:
12	[number]	k4	12
párů	pár	k1gInPc2	pár
žeber	žebro	k1gNnPc2	žebro
<g/>
,	,	kIx,	,
hrudní	hrudní	k2eAgFnSc4d1	hrudní
kost	kost	k1gFnSc4	kost
a	a	k8xC	a
hrudní	hrudní	k2eAgInPc4d1	hrudní
obratle	obratel	k1gInPc4	obratel
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
žebro	žebro	k1gNnSc4	žebro
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
žebro	žebro	k1gNnSc4	žebro
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
