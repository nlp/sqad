<p>
<s>
Suomi	Suo	k1gFnPc7	Suo
Hloubětín	Hloubětína	k1gFnPc2	Hloubětína
je	být	k5eAaImIp3nS	být
vznikající	vznikající	k2eAgFnSc4d1	vznikající
rezidenční	rezidenční	k2eAgFnSc4d1	rezidenční
čtvrť	čtvrť	k1gFnSc4	čtvrť
s	s	k7c7	s
domy	dům	k1gInPc7	dům
ve	v	k7c6	v
finském	finský	k2eAgInSc6d1	finský
stylu	styl	k1gInSc6	styl
v	v	k7c6	v
katastrálním	katastrální	k2eAgNnSc6d1	katastrální
území	území	k1gNnSc6	území
Hloubětín	Hloubětína	k1gFnPc2	Hloubětína
na	na	k7c6	na
Praze	Praha	k1gFnSc6	Praha
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
Nachází	nacházet	k5eAaImIp3nS	nacházet
se	se	k3xPyFc4	se
mezi	mezi	k7c7	mezi
ulicí	ulice	k1gFnSc7	ulice
Kolbenovou	Kolbenový	k2eAgFnSc7d1	Kolbenový
<g/>
,	,	kIx,	,
tramvajovou	tramvajový	k2eAgFnSc7d1	tramvajová
tratí	trať	k1gFnSc7	trať
vedle	vedle	k7c2	vedle
vozovny	vozovna	k1gFnSc2	vozovna
Hloubětín	Hloubětín	k1gInSc1	Hloubětín
<g/>
,	,	kIx,	,
jižní	jižní	k2eAgInSc1d1	jižní
hranici	hranice	k1gFnSc4	hranice
tvoří	tvořit	k5eAaImIp3nS	tvořit
říčka	říčka	k1gFnSc1	říčka
Rokytka	Rokytka	k1gFnSc1	Rokytka
<g/>
.	.	kIx.	.
</s>
<s>
Čtvrť	čtvrť	k1gFnSc1	čtvrť
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
původního	původní	k2eAgInSc2d1	původní
brownfieldu	brownfield	k1gInSc2	brownfield
na	na	k7c6	na
ploše	plocha	k1gFnSc6	plocha
devíti	devět	k4xCc2	devět
hektarů	hektar	k1gInPc2	hektar
pro	pro	k7c4	pro
přibližně	přibližně	k6eAd1	přibližně
2500	[number]	k4	2500
obyvatel	obyvatel	k1gMnPc2	obyvatel
buduje	budovat	k5eAaImIp3nS	budovat
developerská	developerský	k2eAgFnSc1d1	developerská
společnost	společnost	k1gFnSc1	společnost
YIT	YIT	kA	YIT
Česká	český	k2eAgFnSc1d1	Česká
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vstoupila	vstoupit	k5eAaPmAgFnS	vstoupit
na	na	k7c4	na
český	český	k2eAgInSc4d1	český
trh	trh	k1gInSc4	trh
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
akvizicí	akvizice	k1gFnPc2	akvizice
získala	získat	k5eAaPmAgFnS	získat
českou	český	k2eAgFnSc4d1	Česká
firmu	firma	k1gFnSc4	firma
Euro	euro	k1gNnSc4	euro
Stavokonsult	Stavokonsulta	k1gFnPc2	Stavokonsulta
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
Suomi	Suo	k1gFnPc7	Suo
znamená	znamenat	k5eAaImIp3nS	znamenat
ve	v	k7c6	v
finštině	finština	k1gFnSc6	finština
Finsko	Finsko	k1gNnSc1	Finsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
zahrne	zahrnout	k5eAaPmIp3nS	zahrnout
přibližně	přibližně	k6eAd1	přibližně
900	[number]	k4	900
nebo	nebo	k8xC	nebo
1000	[number]	k4	1000
bytů	byt	k1gInPc2	byt
a	a	k8xC	a
celý	celý	k2eAgInSc4d1	celý
komplex	komplex	k1gInSc4	komplex
včetně	včetně	k7c2	včetně
parku	park	k1gInSc2	park
a	a	k8xC	a
jeho	on	k3xPp3gNnSc2	on
vybavení	vybavení	k1gNnSc2	vybavení
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
2021	[number]	k4	2021
a	a	k8xC	a
2022	[number]	k4	2022
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
do	do	k7c2	do
10	[number]	k4	10
fází	fáze	k1gFnPc2	fáze
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
nazvané	nazvaný	k2eAgInPc1d1	nazvaný
po	po	k7c6	po
finských	finský	k2eAgNnPc6d1	finské
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Finský	finský	k2eAgInSc4d1	finský
charakter	charakter	k1gInSc4	charakter
připomínají	připomínat	k5eAaImIp3nP	připomínat
i	i	k9	i
názvy	název	k1gInPc1	název
ulic	ulice	k1gFnPc2	ulice
a	a	k8xC	a
veřejných	veřejný	k2eAgNnPc2d1	veřejné
prostranství	prostranství	k1gNnPc2	prostranství
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
odkazují	odkazovat	k5eAaImIp3nP	odkazovat
na	na	k7c4	na
významné	významný	k2eAgFnPc4d1	významná
finské	finský	k2eAgFnPc4d1	finská
osobnosti	osobnost	k1gFnPc4	osobnost
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
architektury	architektura	k1gFnSc2	architektura
a	a	k8xC	a
vědy	věda	k1gFnSc2	věda
(	(	kIx(	(
<g/>
např.	např.	kA	např.
ulice	ulice	k1gFnSc1	ulice
Waltariho	Waltari	k1gMnSc2	Waltari
<g/>
,	,	kIx,	,
Saarinenova	Saarinenův	k2eAgFnSc1d1	Saarinenův
<g/>
,	,	kIx,	,
náměstí	náměstí	k1gNnSc1	náměstí
Alvara	Alvara	k1gFnSc1	Alvara
Alta	Alta	k1gFnSc1	Alta
<g/>
,	,	kIx,	,
park	park	k1gInSc1	park
Tove	Tov	k1gFnSc2	Tov
Janssonové	Janssonová	k1gFnSc2	Janssonová
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Čtvrť	čtvrť	k1gFnSc1	čtvrť
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
širokou	široký	k2eAgFnSc4d1	široká
občanskou	občanský	k2eAgFnSc4d1	občanská
vybavenost	vybavenost	k1gFnSc4	vybavenost
<g/>
,	,	kIx,	,
v	v	k7c6	v
ústředním	ústřední	k2eAgNnSc6d1	ústřední
prostranství	prostranství	k1gNnSc6	prostranství
v	v	k7c6	v
parterech	parter	k1gInPc6	parter
domů	domů	k6eAd1	domů
budou	být	k5eAaImBp3nP	být
komerční	komerční	k2eAgFnPc1d1	komerční
prostory	prostora	k1gFnPc1	prostora
pro	pro	k7c4	pro
obchody	obchod	k1gInPc4	obchod
a	a	k8xC	a
služby	služba	k1gFnPc4	služba
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
s	s	k7c7	s
rozlehlou	rozlehlý	k2eAgFnSc7d1	rozlehlá
zahradou	zahrada	k1gFnSc7	zahrada
<g/>
,	,	kIx,	,
ve	v	k7c6	v
svahu	svah	k1gInSc6	svah
nad	nad	k7c7	nad
Rokytkou	Rokytka	k1gFnSc7	Rokytka
bude	být	k5eAaImBp3nS	být
restaurace	restaurace	k1gFnSc1	restaurace
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
předností	přednost	k1gFnSc7	přednost
budou	být	k5eAaImBp3nP	být
terasy	terasa	k1gFnPc1	terasa
s	s	k7c7	s
výhledem	výhled	k1gInSc7	výhled
do	do	k7c2	do
zeleně	zeleň	k1gFnSc2	zeleň
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k7c2	poblíž
Rokytky	Rokytka	k1gMnSc2	Rokytka
bude	být	k5eAaImBp3nS	být
postaven	postavit	k5eAaPmNgInS	postavit
přírodní	přírodní	k2eAgInSc1d1	přírodní
amfiteátr	amfiteátr	k1gInSc1	amfiteátr
a	a	k8xC	a
sportovištěm	sportoviště	k1gNnSc7	sportoviště
i	i	k9	i
venkovní	venkovní	k2eAgMnSc1d1	venkovní
workout	workout	k5eAaPmF	workout
prvky	prvek	k1gInPc4	prvek
<g/>
.	.	kIx.	.
</s>
<s>
Komplex	komplex	k1gInSc1	komplex
je	být	k5eAaImIp3nS	být
napojen	napojit	k5eAaPmNgInS	napojit
na	na	k7c4	na
cyklostezku	cyklostezka	k1gFnSc4	cyklostezka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
bude	být	k5eAaImBp3nS	být
psí	psí	k2eAgFnSc1d1	psí
park	park	k1gInSc1	park
<g/>
,	,	kIx,	,
v	v	k7c6	v
jihovýchodní	jihovýchodní	k2eAgFnSc6d1	jihovýchodní
části	část	k1gFnSc6	část
komunitní	komunitní	k2eAgFnSc2d1	komunitní
zahrádky	zahrádka	k1gFnSc2	zahrádka
<g/>
.	.	kIx.	.
</s>
<s>
Příjezd	příjezd	k1gInSc1	příjezd
do	do	k7c2	do
čtvrti	čtvrt	k1gFnSc2	čtvrt
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
ulice	ulice	k1gFnSc2	ulice
Kolbenova	Kolbenův	k2eAgFnSc1d1	Kolbenova
(	(	kIx(	(
<g/>
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
těchto	tento	k3xDgNnPc6	tento
místech	místo	k1gNnPc6	místo
na	na	k7c4	na
jih	jih	k1gInSc4	jih
od	od	k7c2	od
Kolbenovy	Kolbenův	k2eAgFnSc2d1	Kolbenova
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nouzová	nouzový	k2eAgFnSc1d1	nouzová
kolonie	kolonie	k1gFnSc1	kolonie
Čína	Čína	k1gFnSc1	Čína
nebo	nebo	k8xC	nebo
také	také	k9	také
V	v	k7c6	v
Číně	Čína	k1gFnSc6	Čína
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
Indočína	Indočína	k1gFnSc1	Indočína
a	a	k8xC	a
následně	následně	k6eAd1	následně
ještě	ještě	k9	ještě
jižněji	jižně	k6eAd2	jižně
zahrádkářská	zahrádkářský	k2eAgFnSc1d1	zahrádkářská
osada	osada	k1gFnSc1	osada
Rajská	rajský	k2eAgFnSc1d1	rajská
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
domky	domek	k1gInPc1	domek
zde	zde	k6eAd1	zde
stály	stát	k5eAaImAgInP	stát
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
bylo	být	k5eAaImAgNnS	být
toto	tento	k3xDgNnSc1	tento
území	území	k1gNnSc1	území
využíváno	využívat	k5eAaPmNgNnS	využívat
podnikem	podnik	k1gInSc7	podnik
ČKD	ČKD	kA	ČKD
Praha	Praha	k1gFnSc1	Praha
jako	jako	k8xS	jako
železniční	železniční	k2eAgFnSc1d1	železniční
dopravní	dopravní	k2eAgFnSc1d1	dopravní
cesta	cesta	k1gFnSc1	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
oblouku	oblouk	k1gInSc2	oblouk
původní	původní	k2eAgFnSc2d1	původní
vlečky	vlečka	k1gFnSc2	vlečka
kopíruje	kopírovat	k5eAaImIp3nS	kopírovat
ulice	ulice	k1gFnSc1	ulice
Granitova	Granitův	k2eAgFnSc1d1	Granitův
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
1	[number]	k4	1
<g/>
.	.	kIx.	.
etapa	etapa	k1gFnSc1	etapa
Espoo	Espoo	k1gMnSc1	Espoo
===	===	k?	===
</s>
</p>
<p>
<s>
Etapa	etapa	k1gFnSc1	etapa
je	být	k5eAaImIp3nS	být
nazvána	nazvat	k5eAaBmNgFnS	nazvat
podle	podle	k7c2	podle
druhého	druhý	k4xOgNnSc2	druhý
nejlidnatějšího	lidnatý	k2eAgNnSc2d3	nejlidnatější
finského	finský	k2eAgNnSc2d1	finské
města	město	k1gNnSc2	město
Espoo	Espoo	k1gNnSc4	Espoo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
jižním	jižní	k2eAgNnSc6d1	jižní
pobřeží	pobřeží	k1gNnSc6	pobřeží
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
je	být	k5eAaImIp3nS	být
ohraničen	ohraničit	k5eAaPmNgInS	ohraničit
ulicemi	ulice	k1gFnPc7	ulice
Granitova	Granitův	k2eAgNnPc1d1	Granitův
na	na	k7c6	na
severovýchodě	severovýchod	k1gInSc6	severovýchod
<g/>
,	,	kIx,	,
Laponská	laponský	k2eAgNnPc1d1	laponské
na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
Saarinenova	Saarinenův	k2eAgFnSc1d1	Saarinenův
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
půdorys	půdorys	k1gInSc1	půdorys
připomíná	připomínat	k5eAaImIp3nS	připomínat
svým	svůj	k3xOyFgInSc7	svůj
tvarem	tvar	k1gInSc7	tvar
rohlík	rohlík	k1gInSc1	rohlík
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
ho	on	k3xPp3gInSc4	on
dvě	dva	k4xCgFnPc1	dva
samostatné	samostatný	k2eAgFnPc1d1	samostatná
budovy	budova	k1gFnPc1	budova
označené	označený	k2eAgFnSc2d1	označená
jako	jako	k8xC	jako
D1	D1	k1gFnSc2	D1
(	(	kIx(	(
<g/>
severní	severní	k2eAgFnSc2d1	severní
<g/>
)	)	kIx)	)
a	a	k8xC	a
D2	D2	k1gFnSc1	D2
(	(	kIx(	(
<g/>
jižní	jižní	k2eAgFnSc1d1	jižní
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
šest	šest	k4xCc4	šest
nadzemních	nadzemní	k2eAgNnPc2d1	nadzemní
bytových	bytový	k2eAgNnPc2d1	bytové
podlaží	podlaží	k1gNnPc2	podlaží
a	a	k8xC	a
dvě	dva	k4xCgNnPc4	dva
podzemní	podzemní	k2eAgNnPc4d1	podzemní
podlaží	podlaží	k1gNnPc4	podlaží
<g/>
.	.	kIx.	.
</s>
<s>
Každá	každý	k3xTgFnSc1	každý
z	z	k7c2	z
budov	budova	k1gFnPc2	budova
má	mít	k5eAaImIp3nS	mít
2	[number]	k4	2
samostatné	samostatný	k2eAgInPc4d1	samostatný
vchody	vchod	k1gInPc4	vchod
<g/>
.	.	kIx.	.
</s>
<s>
Střechy	střecha	k1gFnPc1	střecha
objektů	objekt	k1gInPc2	objekt
jsou	být	k5eAaImIp3nP	být
ploché	plochý	k2eAgFnPc1d1	plochá
<g/>
.	.	kIx.	.
</s>
<s>
Obytný	obytný	k2eAgInSc1d1	obytný
soubor	soubor	k1gInSc1	soubor
čp.	čp.	k?	čp.
1115	[number]	k4	1115
nabízí	nabízet	k5eAaImIp3nS	nabízet
celkem	celkem	k6eAd1	celkem
149	[number]	k4	149
bytů	byt	k1gInPc2	byt
(	(	kIx(	(
<g/>
severní	severní	k2eAgInSc4d1	severní
dům	dům	k1gInSc4	dům
76	[number]	k4	76
bytů	byt	k1gInPc2	byt
a	a	k8xC	a
jižní	jižní	k2eAgInSc1d1	jižní
dům	dům	k1gInSc1	dům
73	[number]	k4	73
bytů	byt	k1gInPc2	byt
<g/>
)	)	kIx)	)
v	v	k7c6	v
kategoriích	kategorie	k1gFnPc6	kategorie
od	od	k7c2	od
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
kk	kk	k?	kk
po	po	k7c6	po
5	[number]	k4	5
<g/>
+	+	kIx~	+
<g/>
kk	kk	k?	kk
o	o	k7c6	o
velikostech	velikost	k1gFnPc6	velikost
31	[number]	k4	31
až	až	k6eAd1	až
130	[number]	k4	130
metrů	metr	k1gInPc2	metr
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
<g/>
.	.	kIx.	.
</s>
<s>
Hromadné	hromadný	k2eAgFnPc1d1	hromadná
garáže	garáž	k1gFnPc1	garáž
v	v	k7c6	v
suterénu	suterén	k1gInSc6	suterén
mají	mít	k5eAaImIp3nP	mít
kapacitu	kapacita	k1gFnSc4	kapacita
146	[number]	k4	146
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
,	,	kIx,	,
venkovních	venkovní	k2eAgNnPc2d1	venkovní
stání	stání	k1gNnPc2	stání
je	být	k5eAaImIp3nS	být
28	[number]	k4	28
<g/>
.	.	kIx.	.
</s>
<s>
Domy	dům	k1gInPc1	dům
navrhlo	navrhnout	k5eAaPmAgNnS	navrhnout
studio	studio	k1gNnSc1	studio
ABM	ABM	kA	ABM
architekti	architekt	k1gMnPc1	architekt
<g/>
.	.	kIx.	.
<g/>
Domy	dům	k1gInPc1	dům
byly	být	k5eAaImAgInP	být
zkolaudovány	zkolaudovat	k5eAaPmNgInP	zkolaudovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
2	[number]	k4	2
<g/>
.	.	kIx.	.
etapa	etapa	k1gFnSc1	etapa
Oulu	Oulus	k1gInSc2	Oulus
===	===	k?	===
</s>
</p>
<p>
<s>
Etapa	etapa	k1gFnSc1	etapa
je	být	k5eAaImIp3nS	být
nazvána	nazvat	k5eAaBmNgFnS	nazvat
podle	podle	k7c2	podle
města	město	k1gNnSc2	město
Oulu	Oulus	k1gInSc2	Oulus
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgInSc1d3	veliký
v	v	k7c6	v
severním	severní	k2eAgNnSc6d1	severní
Finsku	Finsko	k1gNnSc6	Finsko
a	a	k8xC	a
šesté	šestý	k4xOgFnSc3	šestý
nejlidnatější	lidnatý	k2eAgFnSc3d3	nejlidnatější
v	v	k7c6	v
celé	celý	k2eAgFnSc6d1	celá
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
severovýchodním	severovýchodní	k2eAgNnSc6d1	severovýchodní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Botnického	botnický	k2eAgInSc2d1	botnický
zálivu	záliv	k1gInSc2	záliv
Baltského	baltský	k2eAgNnSc2d1	Baltské
moře	moře	k1gNnSc2	moře
při	při	k7c6	při
ústí	ústí	k1gNnSc6	ústí
řeky	řeka	k1gFnSc2	řeka
Oulu	Oulus	k1gInSc2	Oulus
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
je	být	k5eAaImIp3nS	být
ohraničen	ohraničit	k5eAaPmNgInS	ohraničit
ulicí	ulice	k1gFnSc7	ulice
Saarinenovou	Saarinenová	k1gFnSc4	Saarinenová
na	na	k7c6	na
severu	sever	k1gInSc6	sever
a	a	k8xC	a
východě	východ	k1gInSc6	východ
a	a	k8xC	a
ulicí	ulice	k1gFnSc7	ulice
Revellovou	Revellová	k1gFnSc7	Revellová
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
a	a	k8xC	a
západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
ho	on	k3xPp3gInSc4	on
tři	tři	k4xCgFnPc1	tři
samostatné	samostatný	k2eAgFnPc1d1	samostatná
budovy	budova	k1gFnPc1	budova
<g/>
.	.	kIx.	.
</s>
<s>
Severozápadní	severozápadní	k2eAgNnSc1d1	severozápadní
je	být	k5eAaImIp3nS	být
označen	označit	k5eAaPmNgInS	označit
jako	jako	k9	jako
F4	F4	k1gFnSc7	F4
(	(	kIx(	(
<g/>
čp.	čp.	k?	čp.
1129	[number]	k4	1129
<g/>
/	/	kIx~	/
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
severovýchodní	severovýchodní	k2eAgFnSc1d1	severovýchodní
F5	F5	k1gFnSc1	F5
(	(	kIx(	(
<g/>
čp.	čp.	k?	čp.
1129	[number]	k4	1129
<g/>
/	/	kIx~	/
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
a	a	k8xC	a
jižní	jižní	k2eAgFnSc1d1	jižní
W	W	kA	W
(	(	kIx(	(
<g/>
čp.	čp.	k?	čp.
1129	[number]	k4	1129
<g/>
/	/	kIx~	/
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Devadesát	devadesát	k4xCc1	devadesát
bytů	byt	k1gInPc2	byt
bylo	být	k5eAaImAgNnS	být
postaveno	postavit	k5eAaPmNgNnS	postavit
jako	jako	k8xC	jako
družstevní	družstevní	k2eAgNnSc1d1	družstevní
bydlení	bydlení	k1gNnSc1	bydlení
pro	pro	k7c4	pro
investora	investor	k1gMnSc4	investor
stavební	stavební	k2eAgFnSc4d1	stavební
bytové	bytový	k2eAgNnSc1d1	bytové
družstvo	družstvo	k1gNnSc1	družstvo
Stavbař	stavbař	k1gMnSc1	stavbař
<g/>
.	.	kIx.	.
<g/>
Domy	dům	k1gInPc1	dům
byly	být	k5eAaImAgInP	být
zkolaudovány	zkolaudovat	k5eAaPmNgInP	zkolaudovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
3	[number]	k4	3
<g/>
.	.	kIx.	.
etapa	etapa	k1gFnSc1	etapa
Turku	turek	k1gInSc5	turek
===	===	k?	===
</s>
</p>
<p>
<s>
Etapa	etapa	k1gFnSc1	etapa
je	být	k5eAaImIp3nS	být
nazvána	nazvat	k5eAaPmNgFnS	nazvat
podle	podle	k7c2	podle
pátého	pátý	k4xOgNnSc2	pátý
nejlidnatějšího	lidnatý	k2eAgNnSc2d3	nejlidnatější
finského	finský	k2eAgNnSc2d1	finské
města	město	k1gNnSc2	město
Turku	turek	k1gInSc6	turek
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
jihozápadním	jihozápadní	k2eAgNnSc6d1	jihozápadní
pobřeží	pobřeží	k1gNnSc6	pobřeží
země	zem	k1gFnSc2	zem
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1812	[number]	k4	1812
bylo	být	k5eAaImAgNnS	být
správním	správní	k2eAgInSc7d1	správní
centrem	centr	k1gInSc7	centr
Finska	Finsko	k1gNnSc2	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
je	být	k5eAaImIp3nS	být
ohraničen	ohraničit	k5eAaPmNgInS	ohraničit
ulicí	ulice	k1gFnSc7	ulice
Saarinenovou	Saarinenová	k1gFnSc4	Saarinenová
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
Laponskou	laponský	k2eAgFnSc4d1	laponská
na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
Revellovou	Revellová	k1gFnSc4	Revellová
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
a	a	k8xC	a
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Tvoří	tvořit	k5eAaImIp3nP	tvořit
ho	on	k3xPp3gInSc4	on
čtyři	čtyři	k4xCgInPc1	čtyři
samostatné	samostatný	k2eAgInPc1d1	samostatný
nízkoenergetické	nízkoenergetický	k2eAgInPc1d1	nízkoenergetický
domy	dům	k1gInPc1	dům
ve	v	k7c6	v
střídmém	střídmý	k2eAgInSc6d1	střídmý
stylu	styl	k1gInSc6	styl
inspirovaném	inspirovaný	k2eAgInSc6d1	inspirovaný
klasickou	klasický	k2eAgFnSc7d1	klasická
severskou	severský	k2eAgFnSc7d1	severská
architekturou	architektura	k1gFnSc7	architektura
dle	dle	k7c2	dle
návrhu	návrh	k1gInSc2	návrh
ateliéru	ateliér	k1gInSc2	ateliér
Loxia	Loxium	k1gNnSc2	Loxium
Architectes	Architectesa	k1gFnPc2	Architectesa
Ingenierie	Ingenierie	k1gFnSc2	Ingenierie
<g/>
.	.	kIx.	.
</s>
<s>
Severozápadní	severozápadní	k2eAgInSc1d1	severozápadní
dům	dům	k1gInSc1	dům
je	být	k5eAaImIp3nS	být
označen	označit	k5eAaPmNgInS	označit
jako	jako	k9	jako
F1	F1	k1gFnSc7	F1
(	(	kIx(	(
<g/>
čp.	čp.	k?	čp.
1130	[number]	k4	1130
<g/>
/	/	kIx~	/
<g/>
13	[number]	k4	13
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
severovýchodní	severovýchodní	k2eAgFnSc1d1	severovýchodní
F3	F3	k1gFnSc1	F3
(	(	kIx(	(
<g/>
čp.	čp.	k?	čp.
1130	[number]	k4	1130
<g/>
/	/	kIx~	/
<g/>
7	[number]	k4	7
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jihozápadní	jihozápadní	k2eAgFnSc2d1	jihozápadní
E1	E1	k1gFnSc2	E1
(	(	kIx(	(
<g/>
čp.	čp.	k?	čp.
1130	[number]	k4	1130
<g/>
/	/	kIx~	/
<g/>
11	[number]	k4	11
<g/>
)	)	kIx)	)
a	a	k8xC	a
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
E3	E3	k1gFnSc2	E3
(	(	kIx(	(
<g/>
čp.	čp.	k?	čp.
<g />
.	.	kIx.	.
</s>
<s>
1130	[number]	k4	1130
<g/>
/	/	kIx~	/
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
106	[number]	k4	106
bytů	byt	k1gInPc2	byt
o	o	k7c6	o
velikostech	velikost	k1gFnPc6	velikost
32	[number]	k4	32
až	až	k6eAd1	až
142	[number]	k4	142
metrů	metr	k1gInPc2	metr
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
je	být	k5eAaImIp3nS	být
určených	určený	k2eAgFnPc2d1	určená
do	do	k7c2	do
osobního	osobní	k2eAgNnSc2d1	osobní
vlastnictví	vlastnictví	k1gNnSc2	vlastnictví
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
dispozici	dispozice	k1gFnSc3	dispozice
od	od	k7c2	od
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
kk	kk	k?	kk
po	po	k7c6	po
5	[number]	k4	5
<g/>
+	+	kIx~	+
<g/>
kk	kk	k?	kk
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
vybaveny	vybavit	k5eAaPmNgInP	vybavit
polozapuštěnými	polozapuštěný	k2eAgInPc7d1	polozapuštěný
balkony	balkon	k1gInPc7	balkon
<g/>
,	,	kIx,	,
terasami	terasa	k1gFnPc7	terasa
nebo	nebo	k8xC	nebo
předzahrádkami	předzahrádka	k1gFnPc7	předzahrádka
<g/>
.	.	kIx.	.
<g/>
Domy	dům	k1gInPc1	dům
byly	být	k5eAaImAgInP	být
zkolaudovány	zkolaudovat	k5eAaPmNgInP	zkolaudovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
4	[number]	k4	4
<g/>
.	.	kIx.	.
etapa	etapa	k1gFnSc1	etapa
Lahti	Lahť	k1gFnSc2	Lahť
===	===	k?	===
</s>
</p>
<p>
<s>
Etapa	etapa	k1gFnSc1	etapa
je	být	k5eAaImIp3nS	být
nazvána	nazvat	k5eAaBmNgFnS	nazvat
podle	podle	k7c2	podle
města	město	k1gNnSc2	město
Lahti	Lahť	k1gFnSc2	Lahť
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
je	být	k5eAaImIp3nS	být
významných	významný	k2eAgFnPc2d1	významná
zimním	zimní	k2eAgNnSc7d1	zimní
a	a	k8xC	a
horským	horský	k2eAgNnSc7d1	horské
střediskem	středisko	k1gNnSc7	středisko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
konají	konat	k5eAaImIp3nP	konat
mistrovství	mistrovství	k1gNnSc3	mistrovství
světa	svět	k1gInSc2	svět
ve	v	k7c6	v
skocích	skok	k1gInPc6	skok
na	na	k7c6	na
lyžích	lyže	k1gFnPc6	lyže
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
je	být	k5eAaImIp3nS	být
ohraničen	ohraničit	k5eAaPmNgInS	ohraničit
ulicí	ulice	k1gFnSc7	ulice
Granitovou	granitový	k2eAgFnSc4d1	granitová
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
Laponskou	laponský	k2eAgFnSc4d1	laponská
na	na	k7c6	na
západě	západ	k1gInSc6	západ
a	a	k8xC	a
Saarinenovou	Saarinenová	k1gFnSc4	Saarinenová
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
</s>
<s>
Etapa	etapa	k1gFnSc1	etapa
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
tři	tři	k4xCgInPc4	tři
šestipodlažní	šestipodlažní	k2eAgInPc4d1	šestipodlažní
domy	dům	k1gInPc4	dům
se	s	k7c7	s
společným	společný	k2eAgInSc7d1	společný
jednopodlažním	jednopodlažní	k2eAgInSc7d1	jednopodlažní
suterénem	suterén	k1gInSc7	suterén
a	a	k8xC	a
parterem	parter	k1gInSc7	parter
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
mají	mít	k5eAaImIp3nP	mít
domy	dům	k1gInPc4	dům
společné	společný	k2eAgNnSc4d1	společné
podnoží	podnoží	k1gNnSc4	podnoží
<g/>
,	,	kIx,	,
právně	právně	k6eAd1	právně
tvoří	tvořit	k5eAaImIp3nS	tvořit
jednu	jeden	k4xCgFnSc4	jeden
budovu	budova	k1gFnSc4	budova
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
následně	následně	k6eAd1	následně
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
jedno	jeden	k4xCgNnSc1	jeden
společenství	společenství	k1gNnSc1	společenství
vlastníků	vlastník	k1gMnPc2	vlastník
<g/>
.	.	kIx.	.
</s>
<s>
Nabídne	nabídnout	k5eAaPmIp3nS	nabídnout
104	[number]	k4	104
bytů	byt	k1gInPc2	byt
v	v	k7c6	v
dispozicích	dispozice	k1gFnPc6	dispozice
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
kk	kk	k?	kk
až	až	k9	až
5	[number]	k4	5
<g/>
+	+	kIx~	+
<g/>
kk	kk	k?	kk
o	o	k7c6	o
velikostech	velikost	k1gFnPc6	velikost
31	[number]	k4	31
až	až	k6eAd1	až
132	[number]	k4	132
metrů	metr	k1gInPc2	metr
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
budově	budova	k1gFnSc6	budova
označené	označený	k2eAgFnSc2d1	označená
C1	C1	k1gFnSc2	C1
je	být	k5eAaImIp3nS	být
39	[number]	k4	39
bytů	byt	k1gInPc2	byt
<g/>
,	,	kIx,	,
ve	v	k7c6	v
střední	střední	k2eAgFnSc6d1	střední
C2	C2	k1gFnSc6	C2
je	být	k5eAaImIp3nS	být
33	[number]	k4	33
bytů	byt	k1gInPc2	byt
a	a	k8xC	a
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
C3	C3	k1gFnSc6	C3
32	[number]	k4	32
bytů	byt	k1gInPc2	byt
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
obytných	obytný	k2eAgFnPc2d1	obytná
prostor	prostora	k1gFnPc2	prostora
budou	být	k5eAaImBp3nP	být
v	v	k7c6	v
přízemí	přízemí	k1gNnSc6	přízemí
objektu	objekt	k1gInSc2	objekt
začleněny	začleněn	k2eAgInPc1d1	začleněn
4	[number]	k4	4
komerční	komerční	k2eAgFnPc1d1	komerční
jednotky	jednotka	k1gFnPc1	jednotka
situované	situovaný	k2eAgFnPc1d1	situovaná
do	do	k7c2	do
ulice	ulice	k1gFnSc2	ulice
<g/>
.	.	kIx.	.
</s>
<s>
Architektonické	architektonický	k2eAgNnSc4d1	architektonické
řešení	řešení	k1gNnSc4	řešení
domů	dům	k1gInPc2	dům
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
dílny	dílna	k1gFnSc2	dílna
studia	studio	k1gNnSc2	studio
Loxia	Loxium	k1gNnSc2	Loxium
Architects	Architects	k1gInSc4	Architects
Ingenierie	Ingenierie	k1gFnSc2	Ingenierie
<g/>
.	.	kIx.	.
</s>
<s>
Domy	dům	k1gInPc1	dům
mají	mít	k5eAaImIp3nP	mít
průkaz	průkaz	k1gInSc4	průkaz
energetické	energetický	k2eAgFnSc2d1	energetická
náročnosti	náročnost	k1gFnSc2	náročnost
budovy	budova	k1gFnSc2	budova
B.	B.	kA	B.
Součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
také	také	k9	také
101	[number]	k4	101
parkovacích	parkovací	k2eAgNnPc2d1	parkovací
garážových	garážový	k2eAgNnPc2d1	garážové
stání	stání	k1gNnPc2	stání
<g/>
,	,	kIx,	,
místnost	místnost	k1gFnSc1	místnost
pro	pro	k7c4	pro
mytí	mytí	k1gNnSc4	mytí
kola	kolo	k1gNnSc2	kolo
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
<g/>
Výstavba	výstavba	k1gFnSc1	výstavba
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
2017	[number]	k4	2017
<g/>
,	,	kIx,	,
hrubá	hrubý	k2eAgFnSc1d1	hrubá
stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
dokončena	dokončit	k5eAaPmNgFnS	dokončit
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
Kolaudace	kolaudace	k1gFnSc1	kolaudace
se	se	k3xPyFc4	se
plánuje	plánovat	k5eAaImIp3nS	plánovat
na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
polovinu	polovina	k1gFnSc4	polovina
2019	[number]	k4	2019
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
5	[number]	k4	5
<g/>
.	.	kIx.	.
etapa	etapa	k1gFnSc1	etapa
Salo	Salo	k1gMnSc1	Salo
===	===	k?	===
</s>
</p>
<p>
<s>
Etapa	etapa	k1gFnSc1	etapa
je	být	k5eAaImIp3nS	být
nazvána	nazvat	k5eAaPmNgFnS	nazvat
podle	podle	k7c2	podle
jihofinského	jihofinský	k2eAgNnSc2d1	jihofinský
města	město	k1gNnSc2	město
Salo	Salo	k1gNnSc4	Salo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
leží	ležet	k5eAaImIp3nS	ležet
přibližně	přibližně	k6eAd1	přibližně
52	[number]	k4	52
km	km	kA	km
od	od	k7c2	od
města	město	k1gNnSc2	město
Turku	turek	k1gInSc3	turek
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Vlastní	vlastní	k2eAgNnSc4d1	vlastní
Finsko	Finsko	k1gNnSc4	Finsko
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
Baltskému	baltský	k2eAgNnSc3d1	Baltské
moři	moře	k1gNnSc3	moře
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
zahrne	zahrnout	k5eAaPmIp3nS	zahrnout
dva	dva	k4xCgInPc4	dva
osmipodlažní	osmipodlažní	k2eAgInPc4d1	osmipodlažní
bytové	bytový	k2eAgInPc4d1	bytový
domy	dům	k1gInPc4	dům
s	s	k7c7	s
celkem	celek	k1gInSc7	celek
101	[number]	k4	101
jednotkami	jednotka	k1gFnPc7	jednotka
v	v	k7c6	v
dispozicích	dispozice	k1gFnPc6	dispozice
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
kk	kk	k?	kk
až	až	k9	až
5	[number]	k4	5
<g/>
+	+	kIx~	+
<g/>
kk	kk	k?	kk
o	o	k7c6	o
velikostech	velikost	k1gFnPc6	velikost
32	[number]	k4	32
až	až	k6eAd1	až
120	[number]	k4	120
metrů	metr	k1gInPc2	metr
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
<g/>
.	.	kIx.	.
</s>
<s>
Západní	západní	k2eAgInSc1d1	západní
dům	dům	k1gInSc1	dům
je	být	k5eAaImIp3nS	být
označen	označit	k5eAaPmNgInS	označit
jako	jako	k9	jako
B2	B2	k1gFnSc7	B2
(	(	kIx(	(
<g/>
53	[number]	k4	53
bytů	byt	k1gInPc2	byt
<g/>
)	)	kIx)	)
a	a	k8xC	a
východní	východní	k2eAgFnSc2d1	východní
B1	B1	k1gFnSc2	B1
(	(	kIx(	(
<g/>
48	[number]	k4	48
bytů	byt	k1gInPc2	byt
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
byt	byt	k1gInSc1	byt
bude	být	k5eAaImBp3nS	být
disponovat	disponovat	k5eAaBmF	disponovat
vlastním	vlastní	k2eAgInSc7d1	vlastní
balkonem	balkon	k1gInSc7	balkon
<g/>
,	,	kIx,	,
terasou	terasa	k1gFnSc7	terasa
nebo	nebo	k8xC	nebo
předzahrádkou	předzahrádka	k1gFnSc7	předzahrádka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
domech	dům	k1gInPc6	dům
jsou	být	k5eAaImIp3nP	být
dvě	dva	k4xCgNnPc1	dva
podzemní	podzemní	k2eAgNnPc1d1	podzemní
podlaží	podlaží	k1gNnPc1	podlaží
určené	určený	k2eAgInPc4d1	určený
pro	pro	k7c4	pro
98	[number]	k4	98
garážových	garážový	k2eAgNnPc2d1	garážové
stání	stání	k1gNnPc2	stání
<g/>
,	,	kIx,	,
sklepní	sklepní	k2eAgFnSc1d1	sklepní
kóje	kóje	k1gFnSc1	kóje
<g/>
,	,	kIx,	,
kočárkárnu	kočárkárna	k1gFnSc4	kočárkárna
a	a	k8xC	a
místnost	místnost	k1gFnSc4	místnost
pro	pro	k7c4	pro
mytí	mytí	k1gNnSc4	mytí
kol	kolo	k1gNnPc2	kolo
a	a	k8xC	a
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
projektu	projekt	k1gInSc2	projekt
Salo	Salo	k6eAd1	Salo
navíc	navíc	k6eAd1	navíc
budou	být	k5eAaImBp3nP	být
i	i	k9	i
čtyři	čtyři	k4xCgInPc4	čtyři
komerční	komerční	k2eAgInPc4d1	komerční
prostory	prostor	k1gInPc4	prostor
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc4	dům
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
architekti	architekt	k1gMnPc1	architekt
ze	z	k7c2	z
studia	studio	k1gNnSc2	studio
Loxia	Loxium	k1gNnSc2	Loxium
Architects	Architects	k1gInSc4	Architects
Ingenierie	Ingenierie	k1gFnSc2	Ingenierie
<g/>
.	.	kIx.	.
</s>
<s>
Domy	dům	k1gInPc1	dům
mají	mít	k5eAaImIp3nP	mít
průkaz	průkaz	k1gInSc4	průkaz
energetické	energetický	k2eAgFnSc2d1	energetická
náročnosti	náročnost	k1gFnSc2	náročnost
budovy	budova	k1gFnSc2	budova
B.	B.	kA	B.
Protože	protože	k8xS	protože
bytové	bytový	k2eAgInPc1d1	bytový
domy	dům	k1gInPc1	dům
etap	etapa	k1gFnPc2	etapa
Salo	Salo	k6eAd1	Salo
a	a	k8xC	a
Porvoo	Porvoo	k6eAd1	Porvoo
budou	být	k5eAaImBp3nP	být
mít	mít	k5eAaImF	mít
společný	společný	k2eAgInSc4d1	společný
suterénní	suterénní	k2eAgInSc4d1	suterénní
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
budou	být	k5eAaImBp3nP	být
právně	právně	k6eAd1	právně
tvořit	tvořit	k5eAaImF	tvořit
jednu	jeden	k4xCgFnSc4	jeden
budovu	budova	k1gFnSc4	budova
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
posléze	posléze	k6eAd1	posléze
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
jedno	jeden	k4xCgNnSc1	jeden
společenství	společenství	k1gNnSc1	společenství
vlastníků	vlastník	k1gMnPc2	vlastník
<g/>
.	.	kIx.	.
<g/>
Stavba	stavba	k1gFnSc1	stavba
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
plánovaná	plánovaný	k2eAgFnSc1d1	plánovaná
kolaudace	kolaudace	k1gFnSc1	kolaudace
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2020	[number]	k4	2020
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
6	[number]	k4	6
<g/>
.	.	kIx.	.
etapa	etapa	k1gFnSc1	etapa
MŠ	MŠ	kA	MŠ
===	===	k?	===
</s>
</p>
<p>
<s>
Projekt	projekt	k1gInSc1	projekt
představuje	představovat	k5eAaImIp3nS	představovat
mateřskou	mateřský	k2eAgFnSc4d1	mateřská
školu	škola	k1gFnSc4	škola
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
finský	finský	k2eAgMnSc1d1	finský
architekt	architekt	k1gMnSc1	architekt
Jyrki	Jyrk	k1gFnSc2	Jyrk
Tasa	Tasa	k1gMnSc1	Tasa
(	(	kIx(	(
<g/>
*	*	kIx~	*
1944	[number]	k4	1944
Turku	Turek	k1gMnSc6	Turek
<g/>
)	)	kIx)	)
a	a	k8xC	a
jejíž	jejíž	k3xOyRp3gFnSc7	jejíž
součástí	součást	k1gFnSc7	součást
je	být	k5eAaImIp3nS	být
rozlehlá	rozlehlý	k2eAgFnSc1d1	rozlehlá
zahrada	zahrada	k1gFnSc1	zahrada
<g/>
.	.	kIx.	.
</s>
<s>
Bude	být	k5eAaImBp3nS	být
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
charakteristický	charakteristický	k2eAgInSc1d1	charakteristický
oblý	oblý	k2eAgInSc4d1	oblý
organický	organický	k2eAgInSc4d1	organický
tvar	tvar	k1gInSc4	tvar
<g/>
,	,	kIx,	,
střešní	střešní	k2eAgNnSc4d1	střešní
okno	okno	k1gNnSc4	okno
<g/>
,	,	kIx,	,
z	z	k7c2	z
materiálů	materiál	k1gInPc2	materiál
sklo	sklo	k1gNnSc1	sklo
<g/>
,	,	kIx,	,
beton	beton	k1gInSc1	beton
a	a	k8xC	a
kov	kov	k1gInSc1	kov
<g/>
.	.	kIx.	.
</s>
<s>
Bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
provedení	provedení	k1gNnSc3	provedení
ve	v	k7c6	v
smetanové	smetanový	k2eAgFnSc6d1	smetanová
barvě	barva	k1gFnSc6	barva
<g/>
.	.	kIx.	.
<g/>
Termín	termín	k1gInSc1	termín
dokončení	dokončení	k1gNnSc1	dokončení
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovina	polovina	k1gFnSc1	polovina
2020	[number]	k4	2020
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
7	[number]	k4	7
<g/>
.	.	kIx.	.
etapa	etapa	k1gFnSc1	etapa
Porvoo	Porvoo	k1gMnSc1	Porvoo
===	===	k?	===
</s>
</p>
<p>
<s>
Etapa	etapa	k1gFnSc1	etapa
je	být	k5eAaImIp3nS	být
nazvána	nazvat	k5eAaBmNgFnS	nazvat
podle	podle	k7c2	podle
jihofinského	jihofinský	k2eAgNnSc2d1	jihofinský
města	město	k1gNnSc2	město
a	a	k8xC	a
přístavu	přístav	k1gInSc2	přístav
Porvoo	Porvoo	k6eAd1	Porvoo
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
Finského	finský	k2eAgInSc2d1	finský
zálivu	záliv	k1gInSc2	záliv
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
tvoří	tvořit	k5eAaImIp3nS	tvořit
jedna	jeden	k4xCgFnSc1	jeden
budova	budova	k1gFnSc1	budova
označená	označený	k2eAgFnSc1d1	označená
jako	jako	k8xC	jako
A	A	kA	A
<g/>
6	[number]	k4	6
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
6	[number]	k4	6
nadzemních	nadzemní	k2eAgMnPc2d1	nadzemní
a	a	k8xC	a
2	[number]	k4	2
podzemní	podzemní	k2eAgFnSc7d1	podzemní
podlaží	podlaží	k1gNnSc4	podlaží
a	a	k8xC	a
nabídne	nabídnout	k5eAaPmIp3nS	nabídnout
60	[number]	k4	60
bytů	byt	k1gInPc2	byt
o	o	k7c6	o
dispozici	dispozice	k1gFnSc6	dispozice
od	od	k7c2	od
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
kk	kk	k?	kk
do	do	k7c2	do
5	[number]	k4	5
<g/>
+	+	kIx~	+
<g/>
kk	kk	k?	kk
s	s	k7c7	s
výměrami	výměra	k1gFnPc7	výměra
od	od	k7c2	od
33	[number]	k4	33
do	do	k7c2	do
120	[number]	k4	120
metrů	metr	k1gInPc2	metr
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
<g/>
.	.	kIx.	.
</s>
<s>
Šest	šest	k4xCc1	šest
přízemních	přízemní	k2eAgInPc2d1	přízemní
bytů	byt	k1gInPc2	byt
bude	být	k5eAaImBp3nS	být
mít	mít	k5eAaImF	mít
předzahrádky	předzahrádka	k1gFnPc4	předzahrádka
<g/>
,	,	kIx,	,
v	v	k7c6	v
posledním	poslední	k2eAgNnSc6d1	poslední
podlaží	podlaží	k1gNnSc6	podlaží
budou	být	k5eAaImBp3nP	být
čtyři	čtyři	k4xCgInPc4	čtyři
byty	byt	k1gInPc4	byt
s	s	k7c7	s
terasami	terasa	k1gFnPc7	terasa
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
ostatních	ostatní	k2eAgNnPc6d1	ostatní
patrech	patro	k1gNnPc6	patro
bude	být	k5eAaImBp3nS	být
po	po	k7c6	po
deseti	deset	k4xCc6	deset
bytech	byt	k1gInPc6	byt
s	s	k7c7	s
balkony	balkon	k1gInPc7	balkon
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgNnPc1	dva
podzemní	podzemní	k2eAgNnPc1d1	podzemní
podlaží	podlaží	k1gNnPc1	podlaží
jsou	být	k5eAaImIp3nP	být
určené	určený	k2eAgFnPc1d1	určená
pro	pro	k7c4	pro
56	[number]	k4	56
garážových	garážový	k2eAgNnPc2d1	garážové
stání	stání	k1gNnPc2	stání
<g/>
,	,	kIx,	,
sklepní	sklepní	k2eAgFnSc1d1	sklepní
kóje	kóje	k1gFnSc1	kóje
<g/>
,	,	kIx,	,
kočárkárnu	kočárkárna	k1gFnSc4	kočárkárna
a	a	k8xC	a
místnost	místnost	k1gFnSc4	místnost
pro	pro	k7c4	pro
mytí	mytí	k1gNnSc4	mytí
kol	kolo	k1gNnPc2	kolo
a	a	k8xC	a
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc4	dům
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
architekti	architekt	k1gMnPc1	architekt
ze	z	k7c2	z
studia	studio	k1gNnSc2	studio
Loxia	Loxium	k1gNnSc2	Loxium
Architects	Architects	k1gInSc4	Architects
Ingenierie	Ingenierie	k1gFnSc2	Ingenierie
<g/>
.	.	kIx.	.
</s>
<s>
Dům	dům	k1gInSc1	dům
má	mít	k5eAaImIp3nS	mít
průkaz	průkaz	k1gInSc4	průkaz
energetické	energetický	k2eAgFnSc2d1	energetická
náročnosti	náročnost	k1gFnSc2	náročnost
budovy	budova	k1gFnSc2	budova
B.	B.	kA	B.
Protože	protože	k8xS	protože
bytové	bytový	k2eAgInPc1d1	bytový
domy	dům	k1gInPc1	dům
etap	etapa	k1gFnPc2	etapa
Salo	Salo	k6eAd1	Salo
a	a	k8xC	a
Porvoo	Porvoo	k6eAd1	Porvoo
budou	být	k5eAaImBp3nP	být
mít	mít	k5eAaImF	mít
společný	společný	k2eAgInSc4d1	společný
suterénní	suterénní	k2eAgInSc4d1	suterénní
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
budou	být	k5eAaImBp3nP	být
právně	právně	k6eAd1	právně
tvořit	tvořit	k5eAaImF	tvořit
jednu	jeden	k4xCgFnSc4	jeden
budovu	budova	k1gFnSc4	budova
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
posléze	posléze	k6eAd1	posléze
vznikne	vzniknout	k5eAaPmIp3nS	vzniknout
jedno	jeden	k4xCgNnSc1	jeden
společenství	společenství	k1gNnSc1	společenství
vlastníků	vlastník	k1gMnPc2	vlastník
<g/>
.	.	kIx.	.
<g/>
Výstavba	výstavba	k1gFnSc1	výstavba
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
kolaudace	kolaudace	k1gFnSc1	kolaudace
je	být	k5eAaImIp3nS	být
plánována	plánovat	k5eAaImNgFnS	plánovat
na	na	k7c4	na
říjen	říjen	k1gInSc4	říjen
2020	[number]	k4	2020
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
8	[number]	k4	8
<g/>
.	.	kIx.	.
etapa	etapa	k1gFnSc1	etapa
Pori	Por	k1gFnSc2	Por
===	===	k?	===
</s>
</p>
<p>
<s>
Etapa	etapa	k1gFnSc1	etapa
je	být	k5eAaImIp3nS	být
nazvána	nazvat	k5eAaPmNgFnS	nazvat
podle	podle	k7c2	podle
města	město	k1gNnSc2	město
Pori	Por	k1gFnSc2	Por
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
leží	ležet	k5eAaImIp3nP	ležet
na	na	k7c6	na
západním	západní	k2eAgNnSc6d1	západní
pobřeží	pobřeží	k1gNnSc6	pobřeží
Finska	Finsko	k1gNnSc2	Finsko
<g/>
.	.	kIx.	.
</s>
<s>
Projekt	projekt	k1gInSc1	projekt
tvoří	tvořit	k5eAaImIp3nS	tvořit
3	[number]	k4	3
bytové	bytový	k2eAgInPc4d1	bytový
domy	dům	k1gInPc4	dům
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
je	být	k5eAaImIp3nS	být
82	[number]	k4	82
bytů	byt	k1gInPc2	byt
o	o	k7c4	o
dispozici	dispozice	k1gFnSc4	dispozice
1	[number]	k4	1
<g/>
+	+	kIx~	+
<g/>
kk	kk	k?	kk
až	až	k9	až
5	[number]	k4	5
<g/>
+	+	kIx~	+
<g/>
kk	kk	k?	kk
a	a	k8xC	a
velikostech	velikost	k1gFnPc6	velikost
33	[number]	k4	33
až	až	k6eAd1	až
140	[number]	k4	140
metrů	metr	k1gInPc2	metr
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
<g/>
.	.	kIx.	.
</s>
<s>
Domy	dům	k1gInPc1	dům
A9	A9	k1gFnSc2	A9
(	(	kIx(	(
<g/>
30	[number]	k4	30
bytů	byt	k1gInPc2	byt
<g/>
)	)	kIx)	)
a	a	k8xC	a
B5	B5	k1gMnSc1	B5
(	(	kIx(	(
<g/>
23	[number]	k4	23
bytů	byt	k1gInPc2	byt
<g/>
)	)	kIx)	)
budou	být	k5eAaImBp3nP	být
mít	mít	k5eAaImF	mít
5	[number]	k4	5
nadzemních	nadzemní	k2eAgNnPc2d1	nadzemní
podlaží	podlaží	k1gNnPc2	podlaží
<g/>
,	,	kIx,	,
dům	dům	k1gInSc1	dům
B6	B6	k1gFnSc2	B6
(	(	kIx(	(
<g/>
29	[number]	k4	29
bytů	byt	k1gInPc2	byt
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
6	[number]	k4	6
nadzemních	nadzemní	k2eAgNnPc2d1	nadzemní
podlaží	podlaží	k1gNnPc2	podlaží
<g/>
,	,	kIx,	,
všechny	všechen	k3xTgInPc4	všechen
tři	tři	k4xCgInPc4	tři
domy	dům	k1gInPc4	dům
budou	být	k5eAaImBp3nP	být
mít	mít	k5eAaImF	mít
2	[number]	k4	2
společná	společný	k2eAgNnPc1d1	společné
podzemní	podzemní	k2eAgNnPc1d1	podzemní
podlaží	podlaží	k1gNnPc1	podlaží
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
bude	být	k5eAaImBp3nS	být
81	[number]	k4	81
garážových	garážový	k2eAgNnPc2d1	garážové
stání	stání	k1gNnPc2	stání
<g/>
,	,	kIx,	,
82	[number]	k4	82
sklepních	sklepní	k2eAgFnPc2d1	sklepní
kójí	kóje	k1gFnPc2	kóje
<g/>
,	,	kIx,	,
kočárkárna	kočárkárna	k1gFnSc1	kočárkárna
a	a	k8xC	a
místnost	místnost	k1gFnSc1	místnost
pro	pro	k7c4	pro
mytí	mytí	k1gNnSc4	mytí
kol	kolo	k1gNnPc2	kolo
a	a	k8xC	a
psů	pes	k1gMnPc2	pes
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
architektonických	architektonický	k2eAgFnPc2d1	architektonická
řešením	řešení	k1gNnSc7	řešení
stojí	stát	k5eAaImIp3nS	stát
studio	studio	k1gNnSc1	studio
LOXIA	LOXIA	kA	LOXIA
Architectes	Architectesa	k1gFnPc2	Architectesa
Ingenierie	Ingenierie	k1gFnSc1	Ingenierie
s.	s.	k?	s.
r.	r.	kA	r.
o.	o.	k?	o.
Dům	dům	k1gInSc1	dům
má	mít	k5eAaImIp3nS	mít
průkaz	průkaz	k1gInSc4	průkaz
nízké	nízký	k2eAgFnSc2d1	nízká
energetické	energetický	k2eAgFnSc2d1	energetická
náročnosti	náročnost	k1gFnSc2	náročnost
budovy	budova	k1gFnSc2	budova
B.	B.	kA	B.
<g/>
Výstavba	výstavba	k1gFnSc1	výstavba
byla	být	k5eAaImAgFnS	být
zahájena	zahájit	k5eAaPmNgFnS	zahájit
v	v	k7c6	v
prvním	první	k4xOgNnSc6	první
čtvrtletí	čtvrtletí	k1gNnSc6	čtvrtletí
roku	rok	k1gInSc2	rok
2019	[number]	k4	2019
<g/>
,	,	kIx,	,
plánovaná	plánovaný	k2eAgFnSc1d1	plánovaná
kolaudace	kolaudace	k1gFnSc1	kolaudace
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
leden	leden	k1gInSc4	leden
2021	[number]	k4	2021
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
9	[number]	k4	9
<g/>
.	.	kIx.	.
etapa	etapa	k1gFnSc1	etapa
Vantaa	Vantaa	k1gFnSc1	Vantaa
===	===	k?	===
</s>
</p>
<p>
<s>
Etapa	etapa	k1gFnSc1	etapa
je	být	k5eAaImIp3nS	být
nazvána	nazvat	k5eAaBmNgFnS	nazvat
podle	podle	k7c2	podle
čtvrtého	čtvrtý	k4xOgNnSc2	čtvrtý
nejlidnatějšího	lidnatý	k2eAgNnSc2d3	nejlidnatější
finského	finský	k2eAgNnSc2d1	finské
města	město	k1gNnSc2	město
Vantaa	Vantaum	k1gNnSc2	Vantaum
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
země	zem	k1gFnSc2	zem
a	a	k8xC	a
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
území	území	k1gNnSc6	území
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
hlavní	hlavní	k2eAgNnSc1d1	hlavní
helsinské	helsinský	k2eAgNnSc1d1	Helsinské
letiště	letiště	k1gNnSc1	letiště
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
104	[number]	k4	104
bytů	byt	k1gInPc2	byt
<g/>
,	,	kIx,	,
zahájení	zahájení	k1gNnSc2	zahájení
3	[number]	k4	3
<g/>
.	.	kIx.	.
čtvrtletí	čtvrtletí	k1gNnSc4	čtvrtletí
2019	[number]	k4	2019
<g/>
,	,	kIx,	,
plánovaná	plánovaný	k2eAgFnSc1d1	plánovaná
kolaudace	kolaudace	k1gFnSc1	kolaudace
srpen	srpen	k1gInSc1	srpen
2021	[number]	k4	2021
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
10	[number]	k4	10
<g/>
.	.	kIx.	.
etapa	etapa	k1gFnSc1	etapa
Tampere	Tamper	k1gMnSc5	Tamper
===	===	k?	===
</s>
</p>
<p>
<s>
Etapa	etapa	k1gFnSc1	etapa
je	být	k5eAaImIp3nS	být
nazvána	nazvat	k5eAaBmNgFnS	nazvat
podle	podle	k7c2	podle
třetí	třetí	k4xOgFnSc2	třetí
největšího	veliký	k2eAgNnSc2d3	veliký
finského	finský	k2eAgNnSc2d1	finské
města	město	k1gNnSc2	město
Tampere	Tamper	k1gMnSc5	Tamper
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
největším	veliký	k2eAgNnSc7d3	veliký
vnitrozemským	vnitrozemský	k2eAgNnSc7d1	vnitrozemské
městem	město	k1gNnSc7	město
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
68	[number]	k4	68
bytů	byt	k1gInPc2	byt
<g/>
,	,	kIx,	,
zahájení	zahájení	k1gNnSc2	zahájení
konec	konec	k1gInSc4	konec
roku	rok	k1gInSc2	rok
2019	[number]	k4	2019
<g/>
,	,	kIx,	,
plánovaná	plánovaný	k2eAgFnSc1d1	plánovaná
kolaudace	kolaudace	k1gFnSc1	kolaudace
srpen	srpen	k1gInSc1	srpen
2021	[number]	k4	2021
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Lappi	Lappi	k6eAd1	Lappi
Hloubětín	Hloubětín	k1gInSc1	Hloubětín
</s>
</p>
