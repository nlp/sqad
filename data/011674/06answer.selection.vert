<s>
Výstavba	výstavba	k1gFnSc1	výstavba
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
zahrne	zahrnout	k5eAaPmIp3nS	zahrnout
přibližně	přibližně	k6eAd1	přibližně
900	[number]	k4	900
nebo	nebo	k8xC	nebo
1000	[number]	k4	1000
bytů	byt	k1gInPc2	byt
a	a	k8xC	a
celý	celý	k2eAgInSc4d1	celý
komplex	komplex	k1gInSc4	komplex
včetně	včetně	k7c2	včetně
parku	park	k1gInSc2	park
a	a	k8xC	a
jeho	on	k3xPp3gNnSc2	on
vybavení	vybavení	k1gNnSc2	vybavení
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
dokončen	dokončit	k5eAaPmNgInS	dokončit
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
2021	[number]	k4	2021
a	a	k8xC	a
2022	[number]	k4	2022
<g/>
.	.	kIx.	.
</s>
