<s>
Volodymyr	Volodymyr	k1gMnSc1	Volodymyr
Olexandrovyč	Olexandrovyč	k1gMnSc1	Olexandrovyč
Javorivskyj	Javorivskyj	k1gMnSc1	Javorivskyj
(	(	kIx(	(
<g/>
ukrajinsky	ukrajinsky	k6eAd1	ukrajinsky
Я	Я	k?	Я
В	В	k?	В
О	О	k?	О
<g/>
;	;	kIx,	;
*	*	kIx~	*
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1942	[number]	k4	1942
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
současný	současný	k2eAgMnSc1d1	současný
ukrajinský	ukrajinský	k2eAgMnSc1d1	ukrajinský
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
politik	politik	k1gMnSc1	politik
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	s	k7c7	s
11	[number]	k4	11
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1942	[number]	k4	1942
ve	v	k7c6	v
vesnici	vesnice	k1gFnSc6	vesnice
Teklivka	Teklivka	k1gFnSc1	Teklivka
ve	v	k7c6	v
Vinnycké	Vinnycký	k2eAgFnSc6d1	Vinnycký
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
absolvoval	absolvovat	k5eAaPmAgInS	absolvovat
obor	obor	k1gInSc1	obor
"	"	kIx"	"
<g/>
Ukrajinský	ukrajinský	k2eAgInSc1d1	ukrajinský
jazyk	jazyk	k1gInSc1	jazyk
a	a	k8xC	a
literatura	literatura	k1gFnSc1	literatura
<g/>
"	"	kIx"	"
na	na	k7c6	na
Oděské	oděský	k2eAgFnSc6d1	Oděská
národní	národní	k2eAgFnSc6d1	národní
univerzitě	univerzita	k1gFnSc6	univerzita
I.	I.	kA	I.
I.	I.	kA	I.
Mečnikova	Mečnikův	k2eAgInSc2d1	Mečnikův
<g/>
.	.	kIx.	.
</s>
<s>
Pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
redaktor	redaktor	k1gMnSc1	redaktor
oděského	oděský	k2eAgInSc2d1	oděský
rozhlasu	rozhlas	k1gInSc2	rozhlas
<g/>
,	,	kIx,	,
korespondent	korespondent	k1gMnSc1	korespondent
redakce	redakce	k1gFnSc2	redakce
několika	několik	k4yIc2	několik
novin	novina	k1gFnPc2	novina
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Záporožská	záporožský	k2eAgFnSc1d1	Záporožská
pravda	pravda	k1gFnSc1	pravda
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
scenárista	scenárista	k1gMnSc1	scenárista
lvovské	lvovský	k2eAgFnSc2d1	Lvovská
televize	televize	k1gFnSc2	televize
<g/>
,	,	kIx,	,
literární	literární	k2eAgMnSc1d1	literární
konzultant	konzultant	k1gMnSc1	konzultant
<g/>
,	,	kIx,	,
referent	referent	k1gMnSc1	referent
Svazu	svaz	k1gInSc2	svaz
spisovatelů	spisovatel	k1gMnPc2	spisovatel
Ukrajinské	ukrajinský	k2eAgFnSc2d1	ukrajinská
SSR	SSR	kA	SSR
<g/>
,	,	kIx,	,
zástupce	zástupce	k1gMnSc1	zástupce
šéfredaktora	šéfredaktor	k1gMnSc2	šéfredaktor
časopisu	časopis	k1gInSc2	časopis
"	"	kIx"	"
<g/>
Vitčyzna	Vitčyzna	k1gFnSc1	Vitčyzna
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Vlast	vlast	k1gFnSc1	vlast
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Příslušník	příslušník	k1gMnSc1	příslušník
Svazu	svaz	k1gInSc2	svaz
spisovatelů	spisovatel	k1gMnPc2	spisovatel
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
a	a	k8xC	a
Svazu	svaz	k1gInSc2	svaz
novinářů	novinář	k1gMnPc2	novinář
Ukrajiny	Ukrajina	k1gFnSc2	Ukrajina
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1981	[number]	k4	1981
až	až	k9	až
1990	[number]	k4	1990
byl	být	k5eAaImAgInS	být
členem	člen	k1gMnSc7	člen
KSSS	KSSS	kA	KSSS
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgMnS	být
jedním	jeden	k4xCgMnSc7	jeden
ze	z	k7c2	z
zakladatelů	zakladatel	k1gMnPc2	zakladatel
ukrajinského	ukrajinský	k2eAgNnSc2d1	ukrajinské
opozičního	opoziční	k2eAgNnSc2d1	opoziční
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
RUCHu	ruch	k1gInSc2	ruch
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1989	[number]	k4	1989
stal	stát	k5eAaPmAgMnS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
Kyjevské	kyjevský	k2eAgFnSc2d1	Kyjevská
koordinační	koordinační	k2eAgFnSc2d1	koordinační
rady	rada	k1gFnSc2	rada
RUCHu	ruch	k1gInSc2	ruch
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
získaní	získaný	k2eAgMnPc1d1	získaný
nezávislosti	nezávislost	k1gFnSc3	nezávislost
několikrát	několikrát	k6eAd1	několikrát
kandidoval	kandidovat	k5eAaImAgMnS	kandidovat
do	do	k7c2	do
ukrajinského	ukrajinský	k2eAgInSc2d1	ukrajinský
parlamentu	parlament	k1gInSc2	parlament
za	za	k7c2	za
různé	různý	k2eAgFnSc2d1	různá
demokraticky	demokraticky	k6eAd1	demokraticky
orientované	orientovaný	k2eAgFnSc2d1	orientovaná
politické	politický	k2eAgFnSc2d1	politická
strany	strana	k1gFnSc2	strana
<g/>
.	.	kIx.	.
</s>
<s>
Nyní	nyní	k6eAd1	nyní
je	být	k5eAaImIp3nS	být
členem	člen	k1gMnSc7	člen
Bloku	blok	k1gInSc2	blok
Julie	Julie	k1gFnSc1	Julie
Tymošenko	Tymošenka	k1gFnSc5	Tymošenka
<g/>
.	.	kIx.	.
</s>
<s>
Stojí	stát	k5eAaImIp3nS	stát
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
parlamentního	parlamentní	k2eAgInSc2d1	parlamentní
výboru	výbor	k1gInSc2	výbor
pro	pro	k7c4	pro
kulturu	kultura	k1gFnSc4	kultura
a	a	k8xC	a
duchovnost	duchovnost	k1gFnSc4	duchovnost
<g/>
.	.	kIx.	.
</s>
<s>
Seznam	seznam	k1gInSc1	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Volodymyr	Volodymyr	k1gInSc1	Volodymyr
Javorivskyj	Javorivskyj	k1gFnSc4	Javorivskyj
1968	[number]	k4	1968
А	А	k?	А
я	я	k?	я
п	п	k?	п
(	(	kIx(	(
<g/>
A	a	k8xC	a
jablka	jablko	k1gNnPc1	jablko
padají	padat	k5eAaImIp3nP	padat
<g/>
)	)	kIx)	)
1971	[number]	k4	1971
Г	Г	k?	Г
с	с	k?	с
в	в	k?	в
(	(	kIx(	(
<g/>
Hrozen	hrozen	k1gInSc4	hrozen
zralého	zralý	k2eAgNnSc2d1	zralé
vína	víno	k1gNnSc2	víno
<g/>
)	)	kIx)	)
1975	[number]	k4	1975
К	К	k?	К
в	в	k?	в
н	н	k?	н
(	(	kIx(	(
<g/>
Křídla	křídlo	k1gNnPc1	křídlo
nabroušená	nabroušený	k2eAgFnSc1d1	nabroušená
nebem	nebe	k1gNnSc7	nebe
<g/>
)	)	kIx)	)
1977	[number]	k4	1977
Т	Т	k?	Т
<g/>
,	,	kIx,	,
н	н	k?	н
з	з	k?	з
(	(	kIx(	(
<g/>
Tady	tady	k6eAd1	tady
<g/>
,	,	kIx,	,
na	na	k7c6	na
zemí	zem	k1gFnPc2	zem
<g/>
)	)	kIx)	)
1980	[number]	k4	1980
І	І	k?	І
в	в	k?	в
м	м	k?	м
п	п	k?	п
<g/>
'	'	kIx"	'
<g/>
я	я	k?	я
д	д	k?	д
1985	[number]	k4	1985
П	П	k?	П
в	в	k?	в
і	і	k?	і
(	(	kIx(	(
<g/>
Právo	právo	k1gNnSc1	právo
na	na	k7c4	na
vlastní	vlastní	k2eAgNnSc4d1	vlastní
jméno	jméno	k1gNnSc4	jméno
<g/>
)	)	kIx)	)
2002	[number]	k4	2002
Щ	Щ	k?	Щ
ж	ж	k?	ж
м	м	k?	м
з	з	k?	з
н	н	k?	н
т	т	k?	т
<g/>
?	?	kIx.	?
</s>
<s>
(	(	kIx(	(
<g/>
Co	co	k3yQnSc4	co
jsme	být	k5eAaImIp1nP	být
to	ten	k3xDgNnSc4	ten
za	za	k7c4	za
divný	divný	k2eAgInSc4d1	divný
národ	národ	k1gInSc4	národ
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
1984	[number]	k4	1984
З	З	k?	З
в	в	k?	в
в	в	k?	в
(	(	kIx(	(
<g/>
Z	z	k7c2	z
výšky	výška	k1gFnSc2	výška
září	září	k1gNnSc2	září
<g/>
)	)	kIx)	)
1984	[number]	k4	1984
В	В	k?	В
К	К	k?	К
(	(	kIx(	(
<g/>
Věčné	věčný	k2eAgInPc1d1	věčný
Kortelisy	Kortelis	k1gInPc1	Kortelis
–	–	k?	–
oceněn	oceněn	k2eAgInSc1d1	oceněn
státním	státní	k2eAgNnSc7d1	státní
vyznamenáním	vyznamenání	k1gNnSc7	vyznamenání
T.	T.	kA	T.
H.	H.	kA	H.
Ševčenka	Ševčenka	k1gFnSc1	Ševčenka
<g/>
)	)	kIx)	)
1978	[number]	k4	1978
Л	Л	k?	Л
р	р	k?	р
(	(	kIx(	(
<g/>
Řetězová	řetězový	k2eAgFnSc1d1	řetězová
reakce	reakce	k1gFnSc1	reakce
–	–	k?	–
o	o	k7c6	o
stavbě	stavba	k1gFnSc6	stavba
Černobylské	černobylský	k2eAgFnSc2d1	Černobylská
atomové	atomový	k2eAgFnSc2d1	atomová
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
)	)	kIx)	)
1979	[number]	k4	1979
О	О	k?	О
з	з	k?	з
о	о	k?	о
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Ohlédni	ohlédnout	k5eAaPmRp2nS	ohlédnout
se	se	k3xPyFc4	se
z	z	k7c2	z
podzimu	podzim	k1gInSc2	podzim
<g/>
)	)	kIx)	)
1980	[number]	k4	1980
А	А	k?	А
з	з	k?	з
у	у	k?	у
(	(	kIx(	(
<g/>
Autoportrét	autoportrét	k1gInSc1	autoportrét
podle	podle	k7c2	podle
představy	představa	k1gFnSc2	představa
<g/>
)	)	kIx)	)
1988	[number]	k4	1988
М	М	k?	М
з	з	k?	з
п	п	k?	п
у	у	k?	у
к	к	k?	к
с	с	k?	с
(	(	kIx(	(
<g/>
Marie	Maria	k1gFnSc2	Maria
z	z	k7c2	z
Černobylu	Černobyl	k1gInSc2	Černobyl
<g/>
)	)	kIx)	)
2000	[number]	k4	2000
К	К	k?	К
(	(	kIx(	(
<g/>
Krize	krize	k1gFnSc1	krize
<g/>
)	)	kIx)	)
Volodymyr	Volodymyr	k1gMnSc1	Volodymyr
Javorivskyj	Javorivskyj	k1gMnSc1	Javorivskyj
<g/>
:	:	kIx,	:
Marie	Marie	k1gFnSc1	Marie
z	z	k7c2	z
Černobylu	Černobyl	k1gInSc2	Černobyl
<g/>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1989	[number]	k4	1989
<g/>
;	;	kIx,	;
přeložila	přeložit	k5eAaPmAgFnS	přeložit
Dagmar	Dagmar	k1gFnSc1	Dagmar
Dvořáková	Dvořáková	k1gFnSc1	Dvořáková
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Я	Я	k?	Я
В	В	k?	В
О	О	k?	О
na	na	k7c6	na
ukrajinské	ukrajinský	k2eAgFnSc6d1	ukrajinská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
