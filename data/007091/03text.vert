<s>
Leó	Leó	k?	Leó
Szilárd	Szilárd	k1gInSc1	Szilárd
(	(	kIx(	(
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1898	[number]	k4	1898
<g/>
,	,	kIx,	,
Budapešť	Budapešť	k1gFnSc1	Budapešť
–	–	k?	–
30	[number]	k4	30
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1964	[number]	k4	1964
<g/>
,	,	kIx,	,
La	la	k1gNnSc1	la
Jolla	Jolla	k1gFnSc1	Jolla
<g/>
,	,	kIx,	,
Kalifornie	Kalifornie	k1gFnSc1	Kalifornie
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
americký	americký	k2eAgMnSc1d1	americký
fyzik	fyzik	k1gMnSc1	fyzik
maďarsko-židovského	maďarsko-židovský	k2eAgInSc2d1	maďarsko-židovský
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
první	první	k4xOgFnSc7	první
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
řetězová	řetězový	k2eAgFnSc1d1	řetězová
reakce	reakce	k1gFnSc1	reakce
(	(	kIx(	(
<g/>
i	i	k9	i
atomová	atomový	k2eAgFnSc1d1	atomová
bomba	bomba	k1gFnSc1	bomba
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
uskutečnitelná	uskutečnitelný	k2eAgFnSc1d1	uskutečnitelná
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
obával	obávat	k5eAaImAgMnS	obávat
<g/>
,	,	kIx,	,
že	že	k8xS	že
Němci	Němec	k1gMnPc1	Němec
jí	on	k3xPp3gFnSc2	on
brzy	brzy	k6eAd1	brzy
získají	získat	k5eAaPmIp3nP	získat
<g/>
,	,	kIx,	,
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Albertem	Albert	k1gMnSc7	Albert
Einsteinem	Einstein	k1gMnSc7	Einstein
napsal	napsat	k5eAaBmAgMnS	napsat
list	list	k1gInSc4	list
americkému	americký	k2eAgMnSc3d1	americký
presidentu	president	k1gMnSc3	president
Franklinovi	Franklin	k1gMnSc3	Franklin
D.	D.	kA	D.
Rooseveltovi	Roosevelt	k1gMnSc3	Roosevelt
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
zúčastnil	zúčastnit	k5eAaPmAgMnS	zúčastnit
projektu	projekt	k1gInSc2	projekt
Manhattan	Manhattan	k1gInSc1	Manhattan
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
autorem	autor	k1gMnSc7	autor
mnohých	mnohý	k2eAgInPc2d1	mnohý
patentů	patent	k1gInPc2	patent
(	(	kIx(	(
<g/>
například	například	k6eAd1	například
elektronový	elektronový	k2eAgInSc1d1	elektronový
mikroskop	mikroskop	k1gInSc1	mikroskop
a	a	k8xC	a
urychlovač	urychlovač	k1gInSc1	urychlovač
částic	částice	k1gFnPc2	částice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Výrazně	výrazně	k6eAd1	výrazně
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
zkonstruování	zkonstruování	k1gNnSc6	zkonstruování
prvního	první	k4xOgInSc2	první
atomového	atomový	k2eAgInSc2d1	atomový
reaktoru	reaktor	k1gInSc2	reaktor
(	(	kIx(	(
<g/>
patent	patent	k1gInSc1	patent
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Enricem	Enric	k1gMnSc7	Enric
Fermim	Fermim	k1gMnSc1	Fermim
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Szilárd	Szilárd	k1gInSc1	Szilárd
patřil	patřit	k5eAaImAgInS	patřit
do	do	k7c2	do
skupiny	skupina	k1gFnSc2	skupina
známých	známý	k2eAgMnPc2d1	známý
maďarsko-židovských	maďarsko-židovský	k2eAgMnPc2d1	maďarsko-židovský
fyziků	fyzik	k1gMnPc2	fyzik
a	a	k8xC	a
matematiků	matematik	k1gMnPc2	matematik
z	z	k7c2	z
Budapešti	Budapešť	k1gFnSc2	Budapešť
<g/>
.	.	kIx.	.
</s>
<s>
Patřili	patřit	k5eAaImAgMnP	patřit
sem	sem	k6eAd1	sem
i	i	k9	i
Paul	Paul	k1gMnSc1	Paul
Erdős	Erdős	k1gInSc1	Erdős
<g/>
,	,	kIx,	,
Edward	Edward	k1gMnSc1	Edward
Teller	Teller	k1gMnSc1	Teller
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
von	von	k1gInSc4	von
Neumann	Neumann	k1gMnSc1	Neumann
a	a	k8xC	a
Eugene	Eugen	k1gInSc5	Eugen
Paul	Paul	k1gMnSc1	Paul
Wigner	Wignero	k1gNnPc2	Wignero
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gMnPc1	jejich
američtí	americký	k2eAgMnPc1d1	americký
kolegové	kolega	k1gMnPc1	kolega
je	on	k3xPp3gMnPc4	on
kvůli	kvůli	k7c3	kvůli
jakoby	jakoby	k8xS	jakoby
"	"	kIx"	"
<g/>
nadpozemským	nadpozemský	k2eAgFnPc3d1	nadpozemská
<g/>
"	"	kIx"	"
schopnostem	schopnost	k1gFnPc3	schopnost
nazývali	nazývat	k5eAaImAgMnP	nazývat
"	"	kIx"	"
<g/>
The	The	k1gFnSc1	The
Martians	Martiansa	k1gFnPc2	Martiansa
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Marťani	marťan	k1gMnPc1	marťan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Leó	Leó	k?	Leó
Szilárd	Szilárd	k1gMnSc1	Szilárd
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgMnS	narodit
jako	jako	k9	jako
nejstarší	starý	k2eAgMnSc1d3	nejstarší
syn	syn	k1gMnSc1	syn
úspěšného	úspěšný	k2eAgMnSc2d1	úspěšný
židovského	židovský	k2eAgMnSc2d1	židovský
inženýra	inženýr	k1gMnSc2	inženýr
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
rodina	rodina	k1gFnSc1	rodina
jmenovala	jmenovat	k5eAaBmAgFnS	jmenovat
Spitz	Spitz	k1gInSc4	Spitz
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1900	[number]	k4	1900
si	se	k3xPyFc3	se
jméno	jméno	k1gNnSc4	jméno
změnila	změnit	k5eAaPmAgFnS	změnit
na	na	k7c4	na
Szilárd	Szilárd	k1gInSc4	Szilárd
<g/>
.	.	kIx.	.
</s>
<s>
Fyzikou	fyzika	k1gFnSc7	fyzika
se	se	k3xPyFc4	se
Leó	Leó	k1gMnSc1	Leó
začal	začít	k5eAaPmAgMnS	začít
zabývat	zabývat	k5eAaImF	zabývat
už	už	k9	už
ve	v	k7c6	v
třinácti	třináct	k4xCc6	třináct
letech	léto	k1gNnPc6	léto
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
studium	studium	k1gNnSc4	studium
započal	započnout	k5eAaPmAgInS	započnout
na	na	k7c6	na
Technické	technický	k2eAgFnSc6d1	technická
škole	škola	k1gFnSc6	škola
v	v	k7c6	v
Budapešti	Budapešť	k1gFnSc6	Budapešť
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
kvůli	kvůli	k7c3	kvůli
politickému	politický	k2eAgNnSc3d1	politické
napětí	napětí	k1gNnSc3	napětí
a	a	k8xC	a
nevhodným	vhodný	k2eNgFnPc3d1	nevhodná
studijním	studijní	k2eAgFnPc3d1	studijní
podmínkám	podmínka	k1gFnPc3	podmínka
odchází	odcházet	k5eAaImIp3nS	odcházet
roku	rok	k1gInSc2	rok
1919	[number]	k4	1919
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
mladším	mladý	k2eAgMnSc7d2	mladší
bratrem	bratr	k1gMnSc7	bratr
do	do	k7c2	do
Berlína	Berlín	k1gInSc2	Berlín
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c4	na
Technische	Technische	k1gNnSc4	Technische
Hochschule	Hochschule	k1gFnSc2	Hochschule
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vlivem	vliv	k1gInSc7	vliv
Alberta	Albert	k1gMnSc2	Albert
Einsteina	Einstein	k1gMnSc2	Einstein
<g/>
,	,	kIx,	,
Max	max	kA	max
von	von	k1gInSc1	von
Laueho	Laue	k1gMnSc2	Laue
<g/>
,	,	kIx,	,
Erwina	Erwin	k1gMnSc2	Erwin
Schrödingera	Schrödinger	k1gMnSc2	Schrödinger
<g/>
,	,	kIx,	,
Waltera	Walter	k1gMnSc2	Walter
Nernsta	Nernst	k1gMnSc2	Nernst
a	a	k8xC	a
Fritze	Fritze	k1gFnSc1	Fritze
Habera	Habera	k1gFnSc1	Habera
(	(	kIx(	(
<g/>
kteří	který	k3yRgMnPc1	který
tehdy	tehdy	k6eAd1	tehdy
vyučovali	vyučovat	k5eAaImAgMnP	vyučovat
v	v	k7c6	v
Berlíně	Berlín	k1gInSc6	Berlín
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
více	hodně	k6eAd2	hodně
zaměřil	zaměřit	k5eAaPmAgMnS	zaměřit
na	na	k7c4	na
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
ukončil	ukončit	k5eAaPmAgInS	ukončit
studium	studium	k1gNnSc4	studium
v	v	k7c6	v
Technische	Technische	k1gFnSc6	Technische
Hochschule	Hochschule	k1gFnSc2	Hochschule
a	a	k8xC	a
zapsal	zapsat	k5eAaPmAgMnS	zapsat
se	se	k3xPyFc4	se
na	na	k7c4	na
Berlínskou	berlínský	k2eAgFnSc4d1	Berlínská
univerzitu	univerzita	k1gFnSc4	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
přednášek	přednáška	k1gFnPc2	přednáška
ukázal	ukázat	k5eAaPmAgMnS	ukázat
Leó	Leó	k1gMnSc1	Leó
Szilárd	Szilárd	k1gMnSc1	Szilárd
své	svůj	k3xOyFgFnSc2	svůj
práce	práce	k1gFnSc2	práce
Albertu	Albert	k1gMnSc3	Albert
Einsteinovi	Einstein	k1gMnSc3	Einstein
<g/>
,	,	kIx,	,
kterého	který	k3yRgMnSc2	který
zaujaly	zaujmout	k5eAaPmAgInP	zaujmout
<g/>
,	,	kIx,	,
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
ho	on	k3xPp3gMnSc4	on
Leó	Leó	k1gMnSc4	Leó
často	často	k6eAd1	často
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
získal	získat	k5eAaPmAgInS	získat
doktorát	doktorát	k1gInSc4	doktorát
"	"	kIx"	"
<g/>
cum	cum	k?	cum
laude	laude	k6eAd1	laude
<g/>
"	"	kIx"	"
po	po	k7c6	po
předložení	předložení	k1gNnSc6	předložení
své	svůj	k3xOyFgFnSc2	svůj
dizertační	dizertační	k2eAgFnSc2d1	dizertační
práce	práce	k1gFnSc2	práce
"	"	kIx"	"
<g/>
Pokles	pokles	k1gInSc1	pokles
entropie	entropie	k1gFnSc2	entropie
v	v	k7c6	v
termodynamické	termodynamický	k2eAgFnSc6d1	termodynamická
soustavě	soustava	k1gFnSc6	soustava
vlivem	vlivem	k7c2	vlivem
inteligentní	inteligentní	k2eAgFnSc2d1	inteligentní
bytosti	bytost	k1gFnSc2	bytost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
vzbudila	vzbudit	k5eAaPmAgFnS	vzbudit
v	v	k7c6	v
odborných	odborný	k2eAgInPc6d1	odborný
kruzích	kruh	k1gInPc6	kruh
velký	velký	k2eAgInSc4d1	velký
ohlas	ohlas	k1gInSc4	ohlas
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
prací	práce	k1gFnSc7	práce
dalece	dalece	k?	dalece
předběhl	předběhnout	k5eAaPmAgInS	předběhnout
svou	svůj	k3xOyFgFnSc4	svůj
dobu	doba	k1gFnSc4	doba
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
zkoumá	zkoumat	k5eAaImIp3nS	zkoumat
a	a	k8xC	a
v	v	k7c6	v
podstatě	podstata	k1gFnSc6	podstata
i	i	k9	i
popisuje	popisovat	k5eAaImIp3nS	popisovat
souvislost	souvislost	k1gFnSc4	souvislost
mezi	mezi	k7c7	mezi
informací	informace	k1gFnSc7	informace
a	a	k8xC	a
druhým	druhý	k4xOgInSc7	druhý
termodynamickým	termodynamický	k2eAgInSc7d1	termodynamický
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
lety	let	k1gInPc7	let
1925	[number]	k4	1925
až	až	k9	až
1933	[number]	k4	1933
získal	získat	k5eAaPmAgInS	získat
několik	několik	k4yIc4	několik
patentů	patent	k1gInPc2	patent
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
osm	osm	k4xCc1	osm
společně	společně	k6eAd1	společně
s	s	k7c7	s
Albertem	Albert	k1gMnSc7	Albert
Einsteinem	Einstein	k1gMnSc7	Einstein
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
si	se	k3xPyFc3	se
dává	dávat	k5eAaImIp3nS	dávat
patentovat	patentovat	k5eAaBmF	patentovat
urychlovač	urychlovač	k1gInSc4	urychlovač
částic	částice	k1gFnPc2	částice
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1931	[number]	k4	1931
elektronový	elektronový	k2eAgInSc1d1	elektronový
mikroskop	mikroskop	k1gInSc1	mikroskop
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1931	[number]	k4	1931
podal	podat	k5eAaPmAgInS	podat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Einsteinem	Einstein	k1gMnSc7	Einstein
patent	patent	k1gInSc4	patent
na	na	k7c4	na
nový	nový	k2eAgInSc4d1	nový
typ	typ	k1gInSc4	typ
chladničky	chladnička	k1gFnSc2	chladnička
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
když	když	k8xS	když
v	v	k7c6	v
běžném	běžný	k2eAgInSc6d1	běžný
životě	život	k1gInSc6	život
se	se	k3xPyFc4	se
princip	princip	k1gInSc1	princip
vynálezu	vynález	k1gInSc2	vynález
neujal	ujmout	k5eNaPmAgInS	ujmout
<g/>
,	,	kIx,	,
stále	stále	k6eAd1	stále
se	se	k3xPyFc4	se
užívá	užívat	k5eAaImIp3nS	užívat
na	na	k7c4	na
chlazení	chlazení	k1gNnSc4	chlazení
atomových	atomový	k2eAgInPc2d1	atomový
reaktorů	reaktor	k1gInPc2	reaktor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgNnSc6	tento
období	období	k1gNnSc6	období
působil	působit	k5eAaImAgMnS	působit
Leó	Leó	k1gMnSc1	Leó
Szilárd	Szilárd	k1gMnSc1	Szilárd
jako	jako	k8xC	jako
pomocný	pomocný	k2eAgMnSc1d1	pomocný
profesor	profesor	k1gMnSc1	profesor
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
i	i	k9	i
několikrát	několikrát	k6eAd1	několikrát
přednášel	přednášet	k5eAaImAgMnS	přednášet
teoretickou	teoretický	k2eAgFnSc4d1	teoretická
fyziku	fyzika	k1gFnSc4	fyzika
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Ervinem	Ervin	k1gMnSc7	Ervin
Schrödingerem	Schrödinger	k1gMnSc7	Schrödinger
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
přednášel	přednášet	k5eAaImAgMnS	přednášet
společně	společně	k6eAd1	společně
s	s	k7c7	s
Lisou	Lisa	k1gFnSc7	Lisa
Meitnerovou	Meitnerův	k2eAgFnSc7d1	Meitnerův
chemii	chemie	k1gFnSc4	chemie
a	a	k8xC	a
jadernou	jaderný	k2eAgFnSc4d1	jaderná
fyziku	fyzika	k1gFnSc4	fyzika
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1933	[number]	k4	1933
dostali	dostat	k5eAaPmAgMnP	dostat
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
nacisté	nacista	k1gMnPc1	nacista
<g/>
,	,	kIx,	,
vycítil	vycítit	k5eAaPmAgMnS	vycítit
Szilárd	Szilárd	k1gMnSc1	Szilárd
příchod	příchod	k1gInSc4	příchod
nových	nový	k2eAgInPc2d1	nový
časů	čas	k1gInPc2	čas
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Sbalil	sbalit	k5eAaPmAgMnS	sbalit
jsem	být	k5eAaImIp1nS	být
si	se	k3xPyFc3	se
dva	dva	k4xCgInPc4	dva
kufry	kufr	k1gInPc4	kufr
<g/>
,	,	kIx,	,
abych	aby	kYmCp1nS	aby
mohl	moct	k5eAaImAgMnS	moct
v	v	k7c6	v
kterémkoliv	kterýkoliv	k3yIgInSc6	kterýkoliv
okamžiku	okamžik	k1gInSc6	okamžik
opustit	opustit	k5eAaPmF	opustit
zemi	zem	k1gFnSc4	zem
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
požáru	požár	k1gInSc6	požár
Reichstagu	Reichstag	k1gInSc2	Reichstag
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
rychlík	rychlík	k1gInSc4	rychlík
do	do	k7c2	do
Vídně	Vídeň	k1gFnSc2	Vídeň
<g/>
.	.	kIx.	.
</s>
<s>
Druhého	druhý	k4xOgInSc2	druhý
dne	den	k1gInSc2	den
tentýž	týž	k3xTgInSc1	týž
vlak	vlak	k1gInSc1	vlak
(	(	kIx(	(
<g/>
a	a	k8xC	a
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
předchozího	předchozí	k2eAgInSc2d1	předchozí
dne	den	k1gInSc2	den
zcela	zcela	k6eAd1	zcela
plný	plný	k2eAgInSc1d1	plný
<g/>
)	)	kIx)	)
zastavili	zastavit	k5eAaPmAgMnP	zastavit
nacisté	nacista	k1gMnPc1	nacista
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
a	a	k8xC	a
přinutili	přinutit	k5eAaPmAgMnP	přinutit
všechny	všechen	k3xTgMnPc4	všechen
Židy	Žid	k1gMnPc4	Žid
vystoupit	vystoupit	k5eAaPmF	vystoupit
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
března	březen	k1gInSc2	březen
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
Anglie	Anglie	k1gFnSc2	Anglie
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
V	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
mě	já	k3xPp1nSc2	já
zajímaly	zajímat	k5eAaImAgFnP	zajímat
pouze	pouze	k6eAd1	pouze
dvě	dva	k4xCgFnPc4	dva
věci	věc	k1gFnPc4	věc
<g/>
:	:	kIx,	:
fyzika	fyzika	k1gFnSc1	fyzika
a	a	k8xC	a
politika	politika	k1gFnSc1	politika
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nikdy	nikdy	k6eAd1	nikdy
bych	by	kYmCp1nS	by
si	se	k3xPyFc3	se
nemyslel	myslet	k5eNaImAgMnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgFnPc4	tento
dvě	dva	k4xCgFnPc4	dva
věci	věc	k1gFnPc4	věc
spolu	spolu	k6eAd1	spolu
budou	být	k5eAaImBp3nP	být
někdy	někdy	k6eAd1	někdy
souviset	souviset	k5eAaImF	souviset
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svůj	svůj	k3xOyFgInSc4	svůj
život	život	k1gInSc4	život
vděčím	vděčit	k5eAaImIp1nS	vděčit
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
svému	svůj	k3xOyFgInSc3	svůj
politickému	politický	k2eAgInSc3d1	politický
rozhledu	rozhled	k1gInSc3	rozhled
a	a	k8xC	a
fyzice	fyzika	k1gFnSc3	fyzika
vděčím	vděčit	k5eAaImIp1nS	vděčit
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
můj	můj	k3xOp1gInSc1	můj
život	život	k1gInSc1	život
zajímavý	zajímavý	k2eAgInSc1d1	zajímavý
<g/>
"	"	kIx"	"
vzpomínal	vzpomínat	k5eAaImAgInS	vzpomínat
později	pozdě	k6eAd2	pozdě
Szilárd	Szilárd	k1gInSc1	Szilárd
<g/>
.	.	kIx.	.
</s>
<s>
Szilárd	Szilárd	k6eAd1	Szilárd
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Hansem	Hans	k1gMnSc7	Hans
Bethem	Beth	k1gInSc7	Beth
umožnil	umožnit	k5eAaPmAgInS	umožnit
zaměstnání	zaměstnání	k1gNnSc4	zaměstnání
mnohým	mnohý	k2eAgMnPc3d1	mnohý
uprchlým	uprchlý	k2eAgMnPc3d1	uprchlý
vědcům	vědec	k1gMnPc3	vědec
na	na	k7c6	na
anglických	anglický	k2eAgFnPc6d1	anglická
univerzitách	univerzita	k1gFnPc6	univerzita
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
sira	sir	k1gMnSc4	sir
Wiliama	Wiliam	k1gMnSc4	Wiliam
Beveridge	Beveridg	k1gMnSc4	Beveridg
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
založil	založit	k5eAaPmAgMnS	založit
organizaci	organizace	k1gFnSc4	organizace
Academic	Academic	k1gMnSc1	Academic
Assistance	Assistanec	k1gInSc2	Assistanec
Council	Council	k1gInSc1	Council
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
pomáhat	pomáhat	k5eAaImF	pomáhat
vědcům	vědec	k1gMnPc3	vědec
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
uprchli	uprchnout	k5eAaPmAgMnP	uprchnout
před	před	k7c7	před
nacismem	nacismus	k1gInSc7	nacismus
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
<g/>
,	,	kIx,	,
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
měl	mít	k5eAaImAgMnS	mít
na	na	k7c6	na
půdě	půda	k1gFnSc6	půda
Royal	Royal	k1gMnSc1	Royal
Society	societa	k1gFnSc2	societa
přednášku	přednáška	k1gFnSc4	přednáška
Ernest	Ernest	k1gMnSc1	Ernest
Rutherford	Rutherford	k1gMnSc1	Rutherford
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
hovořil	hovořit	k5eAaImAgMnS	hovořit
o	o	k7c6	o
obrovské	obrovský	k2eAgFnSc6d1	obrovská
energii	energie	k1gFnSc6	energie
ukryté	ukrytý	k2eAgFnSc6d1	ukrytá
v	v	k7c6	v
atomovém	atomový	k2eAgNnSc6d1	atomové
jádru	jádro	k1gNnSc6	jádro
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
praktické	praktický	k2eAgNnSc1d1	praktické
využití	využití	k1gNnSc1	využití
atomové	atomový	k2eAgFnSc2d1	atomová
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
na	na	k7c4	na
Zemi	zem	k1gFnSc4	zem
nelze	lze	k6eNd1	lze
vytvořit	vytvořit	k5eAaPmF	vytvořit
potřebný	potřebný	k2eAgInSc4d1	potřebný
proces	proces	k1gInSc4	proces
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
jeho	jeho	k3xOp3gInPc7	jeho
závěry	závěr	k1gInPc7	závěr
Leó	Leó	k1gMnSc1	Leó
Szilard	Szilard	k1gMnSc1	Szilard
nesouhlasil	souhlasit	k5eNaImAgMnS	souhlasit
a	a	k8xC	a
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
existoval	existovat	k5eAaImAgInS	existovat
dostatečně	dostatečně	k6eAd1	dostatečně
nestabilní	stabilní	k2eNgInSc1d1	nestabilní
prvek	prvek	k1gInSc1	prvek
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
by	by	kYmCp3nS	by
uvolnil	uvolnit	k5eAaPmAgInS	uvolnit
dva	dva	k4xCgInPc4	dva
neutrony	neutron	k1gInPc4	neutron
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
jeden	jeden	k4xCgMnSc1	jeden
pohltí	pohltit	k5eAaPmIp3nS	pohltit
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
by	by	kYmCp3nS	by
možné	možný	k2eAgNnSc1d1	možné
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
prvkem	prvek	k1gInSc7	prvek
vytvořit	vytvořit	k5eAaPmF	vytvořit
řetězovou	řetězový	k2eAgFnSc4d1	řetězová
reakci	reakce	k1gFnSc4	reakce
<g/>
,	,	kIx,	,
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
ho	on	k3xPp3gMnSc4	on
bylo	být	k5eAaImAgNnS	být
kritické	kritický	k2eAgNnSc1d1	kritické
množství	množství	k1gNnSc1	množství
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
byl	být	k5eAaImAgInS	být
princip	princip	k1gInSc1	princip
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1934	[number]	k4	1934
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
podal	podat	k5eAaPmAgMnS	podat
dva	dva	k4xCgInPc4	dva
patenty	patent	k1gInPc4	patent
do	do	k7c2	do
British	Britisha	k1gFnPc2	Britisha
Admirality	admiralita	k1gFnSc2	admiralita
týkající	týkající	k2eAgFnSc2d1	týkající
se	se	k3xPyFc4	se
dané	daný	k2eAgFnSc2d1	daná
problematiky	problematika	k1gFnSc2	problematika
<g/>
.	.	kIx.	.
</s>
<s>
Jeden	jeden	k4xCgInSc1	jeden
byl	být	k5eAaImAgInS	být
veřejný	veřejný	k2eAgMnSc1d1	veřejný
a	a	k8xC	a
druhý	druhý	k4xOgMnSc1	druhý
tajný	tajný	k1gMnSc1	tajný
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
nevěděl	vědět	k5eNaImAgMnS	vědět
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterým	který	k3yRgInSc7	který
prvkem	prvek	k1gInSc7	prvek
je	být	k5eAaImIp3nS	být
reakce	reakce	k1gFnSc1	reakce
uskutečnitelná	uskutečnitelný	k2eAgFnSc1d1	uskutečnitelná
<g/>
,	,	kIx,	,
doporučil	doporučit	k5eAaPmAgInS	doporučit
beryllium	beryllium	k1gNnSc4	beryllium
<g/>
,	,	kIx,	,
brom	brom	k1gInSc4	brom
a	a	k8xC	a
uran	uran	k1gInSc4	uran
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
žádost	žádost	k1gFnSc4	žádost
na	na	k7c4	na
8	[number]	k4	8
tisíc	tisíc	k4xCgInPc2	tisíc
liber	libra	k1gFnPc2	libra
pro	pro	k7c4	pro
další	další	k2eAgInPc4d1	další
výzkumy	výzkum	k1gInPc4	výzkum
byla	být	k5eAaImAgFnS	být
zamítnuta	zamítnout	k5eAaPmNgFnS	zamítnout
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
obdržet	obdržet	k5eAaPmF	obdržet
Nobelovu	Nobelův	k2eAgFnSc4d1	Nobelova
cenu	cena	k1gFnSc4	cena
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
výzkum	výzkum	k1gInSc1	výzkum
proběhl	proběhnout	k5eAaPmAgInS	proběhnout
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
by	by	kYmCp3nS	by
atomová	atomový	k2eAgFnSc1d1	atomová
bomba	bomba	k1gFnSc1	bomba
objevena	objevit	k5eAaPmNgFnS	objevit
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
by	by	kYmCp3nS	by
změnilo	změnit	k5eAaPmAgNnS	změnit
dějiny	dějiny	k1gFnPc4	dějiny
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1935	[number]	k4	1935
Szilárd	Szilárd	k1gMnSc1	Szilárd
získal	získat	k5eAaPmAgMnS	získat
status	status	k1gInSc4	status
uprchlíka	uprchlík	k1gMnSc2	uprchlík
a	a	k8xC	a
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
ve	v	k7c6	v
výzkumu	výzkum	k1gInSc6	výzkum
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
jaderné	jaderný	k2eAgFnSc2d1	jaderná
fyziky	fyzika	k1gFnSc2	fyzika
v	v	k7c4	v
Clarendon	Clarendon	k1gInSc4	Clarendon
Laboratory	Laborator	k1gInPc1	Laborator
<g/>
.	.	kIx.	.
</s>
<s>
Soustředil	soustředit	k5eAaPmAgMnS	soustředit
se	se	k3xPyFc4	se
na	na	k7c4	na
výzkum	výzkum	k1gInSc4	výzkum
iridia	iridium	k1gNnSc2	iridium
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
vykazovalo	vykazovat	k5eAaImAgNnS	vykazovat
zvláštní	zvláštní	k2eAgFnSc4d1	zvláštní
izotopovou	izotopový	k2eAgFnSc4d1	izotopová
aktivitu	aktivita	k1gFnSc4	aktivita
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgMnPc4	svůj
kolegy	kolega	k1gMnPc4	kolega
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
přesvědčit	přesvědčit	k5eAaPmF	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
atomová	atomový	k2eAgFnSc1d1	atomová
energie	energie	k1gFnSc1	energie
má	mít	k5eAaImIp3nS	mít
praktické	praktický	k2eAgNnSc4d1	praktické
využití	využití	k1gNnSc4	využití
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
zároveň	zároveň	k6eAd1	zároveň
představuje	představovat	k5eAaImIp3nS	představovat
hrozbu	hrozba	k1gFnSc4	hrozba
a	a	k8xC	a
proto	proto	k8xC	proto
by	by	kYmCp3nS	by
měl	mít	k5eAaImAgInS	mít
výzkum	výzkum	k1gInSc1	výzkum
probíhat	probíhat	k5eAaImF	probíhat
pod	pod	k7c7	pod
určitým	určitý	k2eAgInSc7d1	určitý
dohledem	dohled	k1gInSc7	dohled
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1937	[number]	k4	1937
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
ve	v	k7c6	v
výzkumech	výzkum	k1gInPc6	výzkum
v	v	k7c6	v
Oxfordu	Oxford	k1gInSc6	Oxford
s	s	k7c7	s
Jamesem	James	k1gMnSc7	James
Tuckem	Tucek	k1gMnSc7	Tucek
a	a	k8xC	a
naplánoval	naplánovat	k5eAaBmAgMnS	naplánovat
elektronový	elektronový	k2eAgInSc4d1	elektronový
urychlovač	urychlovač	k1gInSc4	urychlovač
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c4	v
období	období	k1gNnSc4	období
schválení	schválení	k1gNnSc2	schválení
Mnichovské	mnichovský	k2eAgFnSc2d1	Mnichovská
dohody	dohoda	k1gFnSc2	dohoda
byl	být	k5eAaImAgMnS	být
Leó	Leó	k1gMnSc1	Leó
Szilard	Szilard	k1gMnSc1	Szilard
na	na	k7c6	na
přednášce	přednáška	k1gFnSc6	přednáška
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
<g/>
,	,	kIx,	,
a	a	k8xC	a
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
britským	britský	k2eAgInPc3d1	britský
ústupkům	ústupek	k1gInPc3	ústupek
vůči	vůči	k7c3	vůči
nacistům	nacista	k1gMnPc3	nacista
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
odstěhovat	odstěhovat	k5eAaPmF	odstěhovat
do	do	k7c2	do
New	New	k1gFnSc2	New
Yorku	York	k1gInSc2	York
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
New	New	k1gFnSc6	New
Yorku	York	k1gInSc2	York
na	na	k7c6	na
Kolumbijské	kolumbijský	k2eAgFnSc6d1	kolumbijská
univerzitě	univerzita	k1gFnSc6	univerzita
spolupracoval	spolupracovat	k5eAaImAgMnS	spolupracovat
s	s	k7c7	s
Walterem	Walter	k1gMnSc7	Walter
Zinnem	Zinn	k1gMnSc7	Zinn
na	na	k7c6	na
neutronových	neutronový	k2eAgFnPc6d1	neutronová
emisích	emise	k1gFnPc6	emise
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
zjistil	zjistit	k5eAaPmAgMnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
iridium	iridium	k1gNnSc1	iridium
není	být	k5eNaImIp3nS	být
vhodné	vhodný	k2eAgNnSc1d1	vhodné
na	na	k7c4	na
uskutečnění	uskutečnění	k1gNnSc4	uskutečnění
řetězové	řetězový	k2eAgFnSc2d1	řetězová
reakce	reakce	k1gFnSc2	reakce
<g/>
,	,	kIx,	,
začal	začít	k5eAaPmAgMnS	začít
se	s	k7c7	s
zkoumáním	zkoumání	k1gNnSc7	zkoumání
uranu	uran	k1gInSc2	uran
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
1939	[number]	k4	1939
ho	on	k3xPp3gMnSc4	on
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
zpráva	zpráva	k1gFnSc1	zpráva
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
došlo	dojít	k5eAaPmAgNnS	dojít
ke	k	k7c3	k
štěpení	štěpení	k1gNnSc3	štěpení
uranu	uran	k1gInSc2	uran
a	a	k8xC	a
že	že	k8xS	že
se	se	k3xPyFc4	se
rozpadá	rozpadat	k5eAaImIp3nS	rozpadat
na	na	k7c4	na
dvě	dva	k4xCgNnPc4	dva
jádra	jádro	k1gNnPc4	jádro
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
s	s	k7c7	s
menším	malý	k2eAgInSc7d2	menší
počtem	počet	k1gInSc7	počet
neutronů	neutron	k1gInPc2	neutron
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
blížící	blížící	k2eAgFnSc7d1	blížící
se	se	k3xPyFc4	se
válkou	válka	k1gFnSc7	válka
se	se	k3xPyFc4	se
Szilárd	Szilárd	k1gMnSc1	Szilárd
stále	stále	k6eAd1	stále
více	hodně	k6eAd2	hodně
zabýval	zabývat	k5eAaImAgMnS	zabývat
myšlenkou	myšlenka	k1gFnSc7	myšlenka
na	na	k7c4	na
sestrojení	sestrojení	k1gNnSc4	sestrojení
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
<g/>
.	.	kIx.	.
</s>
<s>
Vědělo	vědět	k5eAaImAgNnS	vědět
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
Němci	Němec	k1gMnPc1	Němec
pokračují	pokračovat	k5eAaImIp3nP	pokračovat
v	v	k7c6	v
atomovém	atomový	k2eAgInSc6d1	atomový
výzkumu	výzkum	k1gInSc6	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
s	s	k7c7	s
Albertem	Albert	k1gMnSc7	Albert
Einsteinem	Einstein	k1gMnSc7	Einstein
proto	proto	k8xC	proto
sepsal	sepsat	k5eAaPmAgMnS	sepsat
dopis	dopis	k1gInSc4	dopis
prezidentu	prezident	k1gMnSc3	prezident
USA	USA	kA	USA
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgNnSc6	který
informovali	informovat	k5eAaBmAgMnP	informovat
o	o	k7c6	o
možném	možný	k2eAgNnSc6d1	možné
využití	využití	k1gNnSc6	využití
atomové	atomový	k2eAgFnSc2d1	atomová
energie	energie	k1gFnSc2	energie
pro	pro	k7c4	pro
vojenské	vojenský	k2eAgInPc4d1	vojenský
účely	účel	k1gInPc4	účel
a	a	k8xC	a
varovali	varovat	k5eAaImAgMnP	varovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nP	by
Němci	Němec	k1gMnPc1	Němec
mohli	moct	k5eAaImAgMnP	moct
získat	získat	k5eAaPmF	získat
ničivou	ničivý	k2eAgFnSc4d1	ničivá
zbraň	zbraň	k1gFnSc4	zbraň
dříve	dříve	k6eAd2	dříve
než	než	k8xS	než
spojenci	spojenec	k1gMnPc1	spojenec
<g/>
.	.	kIx.	.
</s>
<s>
Dopis	dopis	k1gInSc1	dopis
bývá	bývat	k5eAaImIp3nS	bývat
označován	označovat	k5eAaImNgInS	označovat
jako	jako	k8xC	jako
Einsteinův	Einsteinův	k2eAgInSc1d1	Einsteinův
dopis	dopis	k1gInSc1	dopis
či	či	k8xC	či
jako	jako	k9	jako
Einstein-Szilárdův	Einstein-Szilárdův	k2eAgInSc4d1	Einstein-Szilárdův
dopis	dopis	k1gInSc4	dopis
<g/>
.	.	kIx.	.
</s>
<s>
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
na	na	k7c4	na
přezkoumání	přezkoumání	k1gNnSc4	přezkoumání
informací	informace	k1gFnPc2	informace
ustavil	ustavit	k5eAaPmAgMnS	ustavit
komisi	komise	k1gFnSc4	komise
<g/>
.	.	kIx.	.
</s>
<s>
Szilárd	Szilárd	k1gMnSc1	Szilárd
<g/>
,	,	kIx,	,
Wigner	Wigner	k1gMnSc1	Wigner
a	a	k8xC	a
Teller	Teller	k1gMnSc1	Teller
byli	být	k5eAaImAgMnP	být
pozváni	pozvat	k5eAaPmNgMnP	pozvat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
vysvětlili	vysvětlit	k5eAaPmAgMnP	vysvětlit
svůj	svůj	k3xOyFgInSc4	svůj
postoj	postoj	k1gInSc4	postoj
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
se	se	k3xPyFc4	se
komise	komise	k1gFnSc1	komise
zeptala	zeptat	k5eAaPmAgFnS	zeptat
<g/>
,	,	kIx,	,
kolik	kolik	k4yIc1	kolik
peněz	peníze	k1gInPc2	peníze
by	by	kYmCp3nP	by
potřebovali	potřebovat	k5eAaImAgMnP	potřebovat
na	na	k7c4	na
výzkum	výzkum	k1gInSc4	výzkum
<g/>
,	,	kIx,	,
Wigner	Wigner	k1gInSc4	Wigner
odpověděl	odpovědět	k5eAaPmAgMnS	odpovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
bude	být	k5eAaImBp3nS	být
stačit	stačit	k5eAaBmF	stačit
6000	[number]	k4	6000
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
které	který	k3yIgMnPc4	který
poté	poté	k6eAd1	poté
obdrželi	obdržet	k5eAaPmAgMnP	obdržet
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
výzkum	výzkum	k1gInSc1	výzkum
posléze	posléze	k6eAd1	posléze
přerostl	přerůst	k5eAaPmAgInS	přerůst
v	v	k7c4	v
projekt	projekt	k1gInSc4	projekt
Manhattan	Manhattan	k1gInSc1	Manhattan
<g/>
.	.	kIx.	.
</s>
<s>
Zkonstruování	zkonstruování	k1gNnSc4	zkonstruování
reaktoru	reaktor	k1gInSc2	reaktor
měl	mít	k5eAaImAgMnS	mít
na	na	k7c6	na
starosti	starost	k1gFnSc6	starost
Enrico	Enrico	k6eAd1	Enrico
Fermi	Fer	k1gFnPc7	Fer
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
Szilárdem	Szilárd	k1gInSc7	Szilárd
<g/>
,	,	kIx,	,
chemické	chemický	k2eAgInPc4d1	chemický
postupy	postup	k1gInPc4	postup
měl	mít	k5eAaImAgMnS	mít
na	na	k7c4	na
starosti	starost	k1gFnPc4	starost
Wigner	Wignra	k1gFnPc2	Wignra
a	a	k8xC	a
matematické	matematický	k2eAgInPc4d1	matematický
výpočty	výpočet	k1gInPc4	výpočet
John	John	k1gMnSc1	John
von	von	k1gInSc4	von
Neumann	Neumann	k1gMnSc1	Neumann
<g/>
.	.	kIx.	.
</s>
<s>
Projektu	projekt	k1gInSc3	projekt
Manhattan	Manhattan	k1gInSc1	Manhattan
se	se	k3xPyFc4	se
zúčastnilo	zúčastnit	k5eAaPmAgNnS	zúčastnit
mnoho	mnoho	k4c1	mnoho
vědců	vědec	k1gMnPc2	vědec
z	z	k7c2	z
různých	různý	k2eAgFnPc2d1	různá
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
stál	stát	k5eAaImAgInS	stát
více	hodně	k6eAd2	hodně
než	než	k8xS	než
2	[number]	k4	2
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
uranem	uran	k1gInSc7	uran
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
nedařilo	dařit	k5eNaImAgNnS	dařit
provést	provést	k5eAaPmF	provést
řetězovou	řetězový	k2eAgFnSc4d1	řetězová
reakci	reakce	k1gFnSc4	reakce
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
neutrony	neutron	k1gInPc1	neutron
štěpily	štěpit	k5eAaImAgInP	štěpit
jen	jen	k6eAd1	jen
lehký	lehký	k2eAgInSc4d1	lehký
uran	uran	k1gInSc4	uran
U	u	k7c2	u
<g/>
235	[number]	k4	235
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
atomy	atom	k1gInPc1	atom
ale	ale	k8xC	ale
neutrony	neutron	k1gInPc1	neutron
pohlcovaly	pohlcovat	k5eAaImAgInP	pohlcovat
<g/>
,	,	kIx,	,
zpomalily	zpomalit	k5eAaPmAgFnP	zpomalit
a	a	k8xC	a
nakonec	nakonec	k6eAd1	nakonec
i	i	k9	i
zastavily	zastavit	k5eAaPmAgFnP	zastavit
řetězovou	řetězový	k2eAgFnSc4d1	řetězová
reakci	reakce	k1gFnSc4	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byl	být	k5eAaImAgMnS	být
okolo	okolo	k7c2	okolo
uranu	uran	k1gInSc2	uran
rozmístěn	rozmístěn	k2eAgInSc4d1	rozmístěn
grafit	grafit	k1gInSc4	grafit
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
zpomaloval	zpomalovat	k5eAaImAgInS	zpomalovat
rychlost	rychlost	k1gFnSc4	rychlost
neutronů	neutron	k1gInPc2	neutron
a	a	k8xC	a
těžký	těžký	k2eAgInSc1d1	těžký
uran	uran	k1gInSc1	uran
je	být	k5eAaImIp3nS	být
už	už	k6eAd1	už
nepohlcoval	pohlcovat	k5eNaImAgMnS	pohlcovat
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
Szilárdovým	Szilárdův	k2eAgNnSc7d1	Szilárdův
vedením	vedení	k1gNnSc7	vedení
podařilo	podařit	k5eAaPmAgNnS	podařit
v	v	k7c6	v
Oak	Oak	k1gMnSc6	Oak
Ridge	Ridge	k1gFnPc2	Ridge
vyrobit	vyrobit	k5eAaPmF	vyrobit
grafit	grafit	k1gInSc4	grafit
s	s	k7c7	s
dosud	dosud	k6eAd1	dosud
nevídanou	vídaný	k2eNgFnSc7d1	nevídaná
čistotou	čistota	k1gFnSc7	čistota
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
se	se	k3xPyFc4	se
ale	ale	k9	ale
zjistilo	zjistit	k5eAaPmAgNnS	zjistit
<g/>
,	,	kIx,	,
že	že	k8xS	že
čistý	čistý	k2eAgInSc1d1	čistý
grafit	grafit	k1gInSc1	grafit
už	už	k6eAd1	už
nepohlcuje	pohlcovat	k5eNaImIp3nS	pohlcovat
neutrony	neutron	k1gInPc4	neutron
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zpomaluje	zpomalovat	k5eAaImIp3nS	zpomalovat
proces	proces	k1gInSc4	proces
natolik	natolik	k6eAd1	natolik
<g/>
,	,	kIx,	,
že	že	k8xS	že
není	být	k5eNaImIp3nS	být
vhodný	vhodný	k2eAgInSc1d1	vhodný
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
byly	být	k5eAaImAgInP	být
vypracovány	vypracován	k2eAgInPc1d1	vypracován
nové	nový	k2eAgInPc1d1	nový
postupy	postup	k1gInPc1	postup
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc7	první
možností	možnost	k1gFnSc7	možnost
bylo	být	k5eAaImAgNnS	být
oddělit	oddělit	k5eAaPmF	oddělit
od	od	k7c2	od
sebe	sebe	k3xPyFc4	sebe
lehký	lehký	k2eAgInSc1d1	lehký
a	a	k8xC	a
těžký	těžký	k2eAgInSc1d1	těžký
uran	uran	k1gInSc1	uran
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
následně	následně	k6eAd1	následně
stal	stát	k5eAaPmAgMnS	stát
vhodným	vhodný	k2eAgNnSc7d1	vhodné
na	na	k7c4	na
uskutečnění	uskutečnění	k1gNnSc4	uskutečnění
řetězové	řetězový	k2eAgFnSc2d1	řetězová
reakce	reakce	k1gFnSc2	reakce
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
postup	postup	k1gInSc1	postup
byl	být	k5eAaImAgInS	být
však	však	k9	však
velmi	velmi	k6eAd1	velmi
obtížný	obtížný	k2eAgInSc1d1	obtížný
<g/>
.	.	kIx.	.
</s>
<s>
Druhá	druhý	k4xOgFnSc1	druhý
možnost	možnost	k1gFnSc1	možnost
počítala	počítat	k5eAaImAgFnS	počítat
s	s	k7c7	s
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
během	během	k7c2	během
pomalé	pomalý	k2eAgFnSc2d1	pomalá
řetězové	řetězový	k2eAgFnSc2d1	řetězová
reakce	reakce	k1gFnSc2	reakce
těžký	těžký	k2eAgInSc1d1	těžký
uran	uran	k1gInSc1	uran
pohltí	pohltit	k5eAaPmIp3nS	pohltit
neutrony	neutron	k1gInPc4	neutron
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
během	během	k7c2	během
několika	několik	k4yIc2	několik
dní	den	k1gInPc2	den
přemění	přeměnit	k5eAaPmIp3nS	přeměnit
uran	uran	k1gInSc1	uran
na	na	k7c4	na
plutonium	plutonium	k1gNnSc4	plutonium
<g/>
.	.	kIx.	.
</s>
<s>
Szilárd	Szilárd	k6eAd1	Szilárd
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
přestěhoval	přestěhovat	k5eAaPmAgMnS	přestěhovat
do	do	k7c2	do
Chicaga	Chicago	k1gNnSc2	Chicago
a	a	k8xC	a
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1946	[number]	k4	1946
pracoval	pracovat	k5eAaImAgMnS	pracovat
jako	jako	k9	jako
vedoucí	vedoucí	k1gMnSc1	vedoucí
fyzik	fyzik	k1gMnSc1	fyzik
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Arthurem	Arthur	k1gMnSc7	Arthur
Comptonem	Compton	k1gInSc7	Compton
na	na	k7c6	na
Chicagské	chicagský	k2eAgFnSc6d1	Chicagská
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
Metallurgical	Metallurgical	k1gFnSc6	Metallurgical
Laboratory	Laborator	k1gInPc4	Laborator
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
laboratoř	laboratoř	k1gFnSc1	laboratoř
byla	být	k5eAaImAgFnS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
vedoucích	vedoucí	k2eAgNnPc2d1	vedoucí
středisek	středisko	k1gNnPc2	středisko
projektu	projekt	k1gInSc2	projekt
Manhattan	Manhattan	k1gInSc1	Manhattan
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1942	[number]	k4	1942
započala	započnout	k5eAaPmAgFnS	započnout
v	v	k7c6	v
údolí	údolí	k1gNnSc6	údolí
Oak	Oak	k1gFnSc2	Oak
Ridge	Ridge	k1gFnPc2	Ridge
výroba	výroba	k1gFnSc1	výroba
zařízení	zařízení	k1gNnSc2	zařízení
potřebných	potřebné	k1gNnPc2	potřebné
na	na	k7c6	na
zkonstruování	zkonstruování	k1gNnSc6	zkonstruování
atomové	atomový	k2eAgFnSc2d1	atomová
bomby	bomba	k1gFnSc2	bomba
<g/>
,	,	kIx,	,
toto	tento	k3xDgNnSc4	tento
místo	místo	k1gNnSc4	místo
byla	být	k5eAaImAgFnS	být
označováno	označovat	k5eAaImNgNnS	označovat
jako	jako	k8xC	jako
Manhattan	Manhattan	k1gInSc1	Manhattan
District	Districta	k1gFnPc2	Districta
<g/>
.	.	kIx.	.
2	[number]	k4	2
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1942	[number]	k4	1942
Fermi	Fer	k1gFnPc7	Fer
a	a	k8xC	a
Szilárd	Szilárd	k1gInSc1	Szilárd
společně	společně	k6eAd1	společně
s	s	k7c7	s
dalšími	další	k2eAgMnPc7d1	další
představili	představit	k5eAaPmAgMnP	představit
první	první	k4xOgFnSc4	první
nukleární	nukleární	k2eAgFnSc4d1	nukleární
řetězovou	řetězový	k2eAgFnSc4d1	řetězová
reakci	reakce	k1gFnSc4	reakce
na	na	k7c6	na
Chicagské	chicagský	k2eAgFnSc6d1	Chicagská
univerzitě	univerzita	k1gFnSc6	univerzita
v	v	k7c6	v
reaktoru	reaktor	k1gInSc6	reaktor
postaveném	postavený	k2eAgInSc6d1	postavený
v	v	k7c6	v
podzemí	podzemí	k1gNnSc6	podzemí
stadionu	stadion	k1gInSc2	stadion
Stagg	Stagg	k1gMnSc1	Stagg
Field	Field	k1gMnSc1	Field
<g/>
.	.	kIx.	.
</s>
<s>
Rozměry	rozměra	k1gFnPc1	rozměra
reaktoru	reaktor	k1gInSc2	reaktor
byly	být	k5eAaImAgFnP	být
obrovské	obrovský	k2eAgFnPc1d1	obrovská
<g/>
,	,	kIx,	,
skládal	skládat	k5eAaImAgMnS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
45	[number]	k4	45
000	[number]	k4	000
grafitových	grafitový	k2eAgInPc2d1	grafitový
bloků	blok	k1gInPc2	blok
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
pokus	pokus	k1gInSc1	pokus
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
Szilárdovu	Szilárdův	k2eAgFnSc4d1	Szilárdův
myšlenku	myšlenka	k1gFnSc4	myšlenka
<g/>
,	,	kIx,	,
že	že	k8xS	že
atomovou	atomový	k2eAgFnSc4d1	atomová
energii	energie	k1gFnSc4	energie
lze	lze	k6eAd1	lze
využít	využít	k5eAaPmF	využít
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
kapitulaci	kapitulace	k1gFnSc6	kapitulace
Německa	Německo	k1gNnSc2	Německo
Szilárd	Szilárda	k1gFnPc2	Szilárda
napsal	napsat	k5eAaPmAgInS	napsat
další	další	k2eAgInSc1d1	další
dopis	dopis	k1gInSc1	dopis
prezidentu	prezident	k1gMnSc3	prezident
Rooseveltovi	Roosevelt	k1gMnSc3	Roosevelt
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
doporučil	doporučit	k5eAaPmAgInS	doporučit
stanovení	stanovení	k1gNnSc4	stanovení
mezí	mez	k1gFnPc2	mez
na	na	k7c4	na
použití	použití	k1gNnSc4	použití
atomových	atomový	k2eAgFnPc2d1	atomová
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Roosevelt	Roosevelt	k1gMnSc1	Roosevelt
ale	ale	k8xC	ale
zemřel	zemřít	k5eAaPmAgMnS	zemřít
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
dopis	dopis	k1gInSc4	dopis
doručili	doručit	k5eAaPmAgMnP	doručit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
byl	být	k5eAaImAgInS	být
patent	patent	k1gInSc4	patent
na	na	k7c4	na
jaderný	jaderný	k2eAgInSc4d1	jaderný
reaktor	reaktor	k1gInSc4	reaktor
udělen	udělen	k2eAgInSc4d1	udělen
společně	společně	k6eAd1	společně
Fermimu	Fermima	k1gFnSc4	Fermima
a	a	k8xC	a
Szilárdovi	Szilárda	k1gMnSc3	Szilárda
<g/>
,	,	kIx,	,
následně	následně	k6eAd1	následně
ho	on	k3xPp3gInSc4	on
vláda	vláda	k1gFnSc1	vláda
USA	USA	kA	USA
odkoupila	odkoupit	k5eAaPmAgFnS	odkoupit
za	za	k7c4	za
symbolický	symbolický	k2eAgInSc4d1	symbolický
jeden	jeden	k4xCgInSc4	jeden
dolar	dolar	k1gInSc4	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
se	se	k3xPyFc4	se
Szilárd	Szilárd	k1gMnSc1	Szilárd
zaměřil	zaměřit	k5eAaPmAgMnS	zaměřit
na	na	k7c6	na
nukleární	nukleární	k2eAgFnSc6d1	nukleární
biologii	biologie	k1gFnSc6	biologie
a	a	k8xC	a
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Aaronem	Aaron	k1gMnSc7	Aaron
Novickem	Novick	k1gInSc7	Novick
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
1950	[number]	k4	1950
Szilárd	Szilárd	k1gMnSc1	Szilárd
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
kobaltovou	kobaltový	k2eAgFnSc4d1	kobaltová
bombu	bomba	k1gFnSc4	bomba
<g/>
,	,	kIx,	,
nový	nový	k2eAgInSc4d1	nový
typ	typ	k1gInSc4	typ
nukleární	nukleární	k2eAgFnSc2d1	nukleární
zbraně	zbraň	k1gFnSc2	zbraň
používající	používající	k2eAgInSc1d1	používající
kobalt	kobalt	k1gInSc1	kobalt
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgMnS	moct
zničit	zničit	k5eAaPmF	zničit
veškerý	veškerý	k3xTgInSc4	veškerý
život	život	k1gInSc4	život
na	na	k7c6	na
planetě	planeta	k1gFnSc6	planeta
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Leó	Leó	k1gFnSc2	Leó
Szilárd	Szilárd	k1gInSc1	Szilárd
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Leó	Leó	k1gFnSc2	Leó
Szilárd	Szilárda	k1gFnPc2	Szilárda
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Informace	informace	k1gFnPc1	informace
Leo	Leo	k1gMnSc1	Leo
Szilárd	Szilárd	k1gMnSc1	Szilárd
Online	Onlin	k1gInSc5	Onlin
Leo	Leo	k1gMnSc1	Leo
Szilárd	Szilárda	k1gFnPc2	Szilárda
na	na	k7c4	na
atomicarchive	atomicarchiev	k1gFnPc4	atomicarchiev
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
Leo	Leo	k1gMnSc1	Leo
Szilárd	Szilárd	k1gMnSc1	Szilárd
Tvrdohlavý	tvrdohlavý	k2eAgInSc4d1	tvrdohlavý
vizionár	vizionár	k1gInSc4	vizionár
na	na	k7c4	na
sme	sme	k?	sme
<g/>
.	.	kIx.	.
<g/>
sk	sk	k?	sk
Einstein-Szilárdov	Einstein-Szilárdov	k1gInSc1	Einstein-Szilárdov
list	list	k1gInSc1	list
prezidentovi	prezident	k1gMnSc3	prezident
Rooseveltovi	Roosevelt	k1gMnSc3	Roosevelt
–	–	k?	–
1939	[number]	k4	1939
Dr	dr	kA	dr
<g/>
.	.	kIx.	.
Bödők	Bödők	k1gMnSc1	Bödők
Zsigmond	Zsigmond	k1gMnSc1	Zsigmond
<g/>
:	:	kIx,	:
A	A	kA	A
legsokoldalúbb	legsokoldalúbb	k1gMnSc1	legsokoldalúbb
marslakó	marslakó	k?	marslakó
<g/>
,	,	kIx,	,
Szilárd	Szilárd	k1gMnSc1	Szilárd
Leó	Leó	k1gMnSc1	Leó
<g/>
,	,	kIx,	,
Élet	Élet	k1gMnSc1	Élet
és	és	k?	és
Tudomány	Tudomán	k1gMnPc7	Tudomán
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
Patenty	patent	k1gInPc1	patent
Espacenet	Espacenet	k1gMnSc1	Espacenet
USPTO	USPTO	kA	USPTO
</s>
