<s>
Adele	Adele	k1gFnSc1	Adele
Laurie	Laurie	k1gFnSc2	Laurie
Blue	Blue	k1gFnPc2	Blue
Adkins	Adkins	k1gInSc1	Adkins
<g/>
,	,	kIx,	,
MBE	MBE	kA	MBE
(	(	kIx(	(
<g/>
*	*	kIx~	*
5	[number]	k4	5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1988	[number]	k4	1988
Londýn	Londýn	k1gInSc1	Londýn
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
britská	britský	k2eAgFnSc1d1	britská
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
a	a	k8xC	a
skladatelka	skladatelka	k1gFnSc1	skladatelka
známá	známý	k2eAgFnSc1d1	známá
jen	jen	k9	jen
jako	jako	k8xS	jako
Adele	Adele	k1gFnSc1	Adele
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
převzala	převzít	k5eAaPmAgFnS	převzít
cenu	cena	k1gFnSc4	cena
BRIT	Brit	k1gMnSc1	Brit
Awards	Awardsa	k1gFnPc2	Awardsa
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Cena	cena	k1gFnSc1	cena
kritiků	kritik	k1gMnPc2	kritik
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
udílená	udílený	k2eAgFnSc1d1	udílená
umělcům	umělec	k1gMnPc3	umělec
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
ještě	ještě	k6eAd1	ještě
nevydali	vydat	k5eNaPmAgMnP	vydat
album	album	k1gNnSc4	album
<g/>
.	.	kIx.	.
</s>
<s>
Debutová	debutový	k2eAgFnSc1d1	debutová
deska	deska	k1gFnSc1	deska
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
19	[number]	k4	19
vyšla	vyjít	k5eAaPmAgFnS	vyjít
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2008	[number]	k4	2008
a	a	k8xC	a
dostala	dostat	k5eAaPmAgFnS	dostat
se	se	k3xPyFc4	se
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
prodejního	prodejní	k2eAgInSc2d1	prodejní
žebříčku	žebříček	k1gInSc2	žebříček
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Druhé	druhý	k4xOgNnSc1	druhý
album	album	k1gNnSc1	album
s	s	k7c7	s
názvem	název	k1gInSc7	název
21	[number]	k4	21
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
nejprodávanějším	prodávaný	k2eAgFnPc3d3	nejprodávanější
albem	album	k1gNnSc7	album
tohoto	tento	k3xDgNnSc2	tento
století	století	k1gNnSc2	století
a	a	k8xC	a
prestižní	prestižní	k2eAgInSc1d1	prestižní
hudební	hudební	k2eAgInSc1d1	hudební
magazín	magazín	k1gInSc1	magazín
Billboard	billboard	k1gInSc1	billboard
ho	on	k3xPp3gMnSc4	on
označil	označit	k5eAaPmAgInS	označit
za	za	k7c4	za
nejlepší	dobrý	k2eAgNnSc4d3	nejlepší
album	album	k1gNnSc4	album
všech	všecek	k3xTgFnPc2	všecek
dob	doba	k1gFnPc2	doba
<g/>
.	.	kIx.	.
</s>
<s>
Celosvětové	celosvětový	k2eAgNnSc1d1	celosvětové
vydání	vydání	k1gNnSc1	vydání
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
19	[number]	k4	19
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
desky	deska	k1gFnSc2	deska
vyšel	vyjít	k5eAaPmAgInS	vyjít
také	také	k9	také
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Rolling	Rolling	k1gInSc1	Rolling
in	in	k?	in
the	the	k?	the
Deep	Deep	k1gInSc1	Deep
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
přednesla	přednést	k5eAaPmAgFnS	přednést
svojí	svojit	k5eAaImIp3nP	svojit
píseň	píseň	k1gFnSc4	píseň
"	"	kIx"	"
<g/>
Skyfall	Skyfall	k1gInSc4	Skyfall
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
za	za	k7c4	za
kterou	který	k3yIgFnSc4	který
obdržela	obdržet	k5eAaPmAgFnS	obdržet
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
a	a	k8xC	a
Oscara	Oscar	k1gMnSc4	Oscar
<g/>
.	.	kIx.	.
</s>
<s>
Alba	alba	k1gFnSc1	alba
její	její	k3xOp3gFnSc2	její
diskografie	diskografie	k1gFnSc2	diskografie
se	se	k3xPyFc4	se
na	na	k7c6	na
prvním	první	k4xOgNnSc6	první
místě	místo	k1gNnSc6	místo
nejprodávanějších	prodávaný	k2eAgFnPc2d3	nejprodávanější
desek	deska	k1gFnPc2	deska
udržely	udržet	k5eAaPmAgFnP	udržet
nejdéle	dlouho	k6eAd3	dlouho
v	v	k7c6	v
historii	historie	k1gFnSc6	historie
<g/>
.	.	kIx.	.
</s>
<s>
Překonala	překonat	k5eAaPmAgFnS	překonat
tak	tak	k9	tak
Rolling	Rolling	k1gInSc4	Rolling
Stones	Stonesa	k1gFnPc2	Stonesa
i	i	k8xC	i
Madonnu	Madonn	k1gInSc2	Madonn
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
tříleté	tříletý	k2eAgFnSc6d1	tříletá
odmlce	odmlka	k1gFnSc6	odmlka
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
vrátila	vrátit	k5eAaPmAgFnS	vrátit
s	s	k7c7	s
novým	nový	k2eAgInSc7d1	nový
<g/>
,	,	kIx,	,
již	již	k9	již
třetím	třetí	k4xOgMnSc7	třetí
studiovým	studiový	k2eAgInSc7d1	studiový
albem	album	k1gNnSc7	album
25	[number]	k4	25
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
ji	on	k3xPp3gFnSc4	on
ihned	ihned	k6eAd1	ihned
dostalo	dostat	k5eAaPmAgNnS	dostat
zpět	zpět	k6eAd1	zpět
na	na	k7c4	na
vrchol	vrchol	k1gInSc4	vrchol
hudebního	hudební	k2eAgInSc2d1	hudební
průmyslu	průmysl	k1gInSc2	průmysl
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Hello	Hello	k1gNnSc1	Hello
<g/>
"	"	kIx"	"
okamžitě	okamžitě	k6eAd1	okamžitě
obsadila	obsadit	k5eAaPmAgFnS	obsadit
první	první	k4xOgNnPc4	první
místa	místo	k1gNnPc4	místo
hudebních	hudební	k2eAgInPc2d1	hudební
žebříčků	žebříček	k1gInPc2	žebříček
a	a	k8xC	a
na	na	k7c4	na
YouTube	YouTub	k1gInSc5	YouTub
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
za	za	k7c4	za
pouhé	pouhý	k2eAgInPc4d1	pouhý
3	[number]	k4	3
měsíce	měsíc	k1gInPc4	měsíc
1	[number]	k4	1
miliardu	miliarda	k4xCgFnSc4	miliarda
zhlédnutí	zhlédnutí	k1gNnSc6	zhlédnutí
<g/>
.	.	kIx.	.
</s>
<s>
Videoklip	videoklip	k1gInSc1	videoklip
ke	k	k7c3	k
skladbě	skladba	k1gFnSc3	skladba
také	také	k9	také
drží	držet	k5eAaImIp3nS	držet
rekord	rekord	k1gInSc4	rekord
v	v	k7c6	v
největším	veliký	k2eAgInSc6d3	veliký
počtu	počet	k1gInSc6	počet
zhlédnutí	zhlédnutý	k2eAgMnPc1d1	zhlédnutý
na	na	k7c4	na
YouTube	YouTub	k1gInSc5	YouTub
v	v	k7c4	v
prvních	první	k4xOgInPc2	první
24	[number]	k4	24
hodinách	hodina	k1gFnPc6	hodina
od	od	k7c2	od
zveřejnění	zveřejnění	k1gNnSc2	zveřejnění
–	–	k?	–
27,7	[number]	k4	27,7
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
přibyl	přibýt	k5eAaPmAgInS	přibýt
i	i	k9	i
rekord	rekord	k1gInSc4	rekord
za	za	k7c4	za
nejrychlejší	rychlý	k2eAgNnSc4d3	nejrychlejší
dosažení	dosažení	k1gNnSc4	dosažení
1	[number]	k4	1
miliardy	miliarda	k4xCgFnSc2	miliarda
zhlédnutí	zhlédnutí	k1gNnPc2	zhlédnutí
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
za	za	k7c4	za
87	[number]	k4	87
dní	den	k1gInPc2	den
od	od	k7c2	od
zveřejnění	zveřejnění	k1gNnSc2	zveřejnění
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2007	[number]	k4	2007
vydala	vydat	k5eAaPmAgFnS	vydat
svůj	svůj	k3xOyFgInSc4	svůj
první	první	k4xOgInSc4	první
singl	singl	k1gInSc4	singl
"	"	kIx"	"
<g/>
Hometown	Hometown	k1gInSc1	Hometown
Glory	Glora	k1gFnSc2	Glora
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Umístil	umístit	k5eAaPmAgMnS	umístit
se	se	k3xPyFc4	se
na	na	k7c4	na
19	[number]	k4	19
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
britského	britský	k2eAgInSc2d1	britský
žebříčku	žebříček	k1gInSc2	žebříček
UK	UK	kA	UK
Singles	Singlesa	k1gFnPc2	Singlesa
Chart	charta	k1gFnPc2	charta
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
vedoucím	vedoucí	k2eAgInSc7d1	vedoucí
singlem	singl	k1gInSc7	singl
jejího	její	k3xOp3gNnSc2	její
debutového	debutový	k2eAgNnSc2d1	debutové
alba	album	k1gNnSc2	album
19	[number]	k4	19
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
pojmenované	pojmenovaný	k2eAgNnSc1d1	pojmenované
po	po	k7c6	po
věku	věk	k1gInSc6	věk
Adele	Adele	k1gFnSc2	Adele
<g/>
,	,	kIx,	,
když	když	k8xS	když
ho	on	k3xPp3gInSc4	on
začala	začít	k5eAaPmAgFnS	začít
nahrávat	nahrávat	k5eAaImF	nahrávat
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2008	[number]	k4	2008
u	u	k7c2	u
společnosti	společnost	k1gFnSc2	společnost
XL	XL	kA	XL
Recordings	Recordings	k1gInSc1	Recordings
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
získala	získat	k5eAaPmAgFnS	získat
smlouvu	smlouva	k1gFnSc4	smlouva
v	v	k7c6	v
červnu	červen	k1gInSc6	červen
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
Vcelku	vcelku	k6eAd1	vcelku
nevýrazný	výrazný	k2eNgInSc4d1	nevýrazný
první	první	k4xOgInSc4	první
singl	singl	k1gInSc4	singl
následovala	následovat	k5eAaImAgFnS	následovat
píseň	píseň	k1gFnSc1	píseň
"	"	kIx"	"
<g/>
Chasing	Chasing	k1gInSc1	Chasing
Pavements	Pavements	k1gInSc1	Pavements
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
umístila	umístit	k5eAaPmAgFnS	umístit
na	na	k7c4	na
2	[number]	k4	2
<g/>
.	.	kIx.	.
místě	místo	k1gNnSc6	místo
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
a	a	k8xC	a
na	na	k7c6	na
21	[number]	k4	21
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
domácí	domácí	k2eAgFnSc6d1	domácí
půdě	půda	k1gFnSc6	půda
uspěly	uspět	k5eAaPmAgInP	uspět
i	i	k9	i
singly	singl	k1gInPc1	singl
"	"	kIx"	"
<g/>
Cold	Cold	k1gMnSc1	Cold
Shoulder	Shoulder	k1gMnSc1	Shoulder
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Make	Make	k1gFnSc1	Make
You	You	k1gMnSc1	You
Feel	Feel	k1gMnSc1	Feel
My	my	k3xPp1nPc1	my
Love	lov	k1gInSc5	lov
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
debutovalo	debutovat	k5eAaBmAgNnS	debutovat
na	na	k7c4	na
1	[number]	k4	1
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
žebříčku	žebříček	k1gInSc2	žebříček
UK	UK	kA	UK
Albums	Albumsa	k1gFnPc2	Albumsa
Chart	charta	k1gFnPc2	charta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
původně	původně	k6eAd1	původně
na	na	k7c4	na
54	[number]	k4	54
<g/>
.	.	kIx.	.
příčce	příčka	k1gFnSc6	příčka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
úspěchu	úspěch	k1gInSc6	úspěch
alba	alba	k1gFnSc1	alba
21	[number]	k4	21
se	se	k3xPyFc4	se
vyšplhalo	vyšplhat	k5eAaPmAgNnS	vyšplhat
až	až	k9	až
na	na	k7c4	na
4	[number]	k4	4
<g/>
.	.	kIx.	.
příčku	příčka	k1gFnSc4	příčka
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
se	se	k3xPyFc4	se
ho	on	k3xPp3gMnSc4	on
prodalo	prodat	k5eAaPmAgNnS	prodat
2	[number]	k4	2
miliony	milion	k4xCgInPc4	milion
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
stejný	stejný	k2eAgInSc4d1	stejný
počet	počet	k1gInSc4	počet
i	i	k9	i
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
celosvětově	celosvětově	k6eAd1	celosvětově
poté	poté	k6eAd1	poté
10	[number]	k4	10
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
album	album	k1gNnSc1	album
ji	on	k3xPp3gFnSc4	on
vyneslo	vynést	k5eAaPmAgNnS	vynést
jednu	jeden	k4xCgFnSc4	jeden
cenu	cena	k1gFnSc4	cena
z	z	k7c2	z
BRIT	Brit	k1gMnSc1	Brit
Awards	Awards	k1gInSc1	Awards
a	a	k8xC	a
dvě	dva	k4xCgFnPc4	dva
ceny	cena	k1gFnPc4	cena
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgNnSc4	svůj
druhé	druhý	k4xOgNnSc4	druhý
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
začala	začít	k5eAaPmAgFnS	začít
nahrávat	nahrávat	k5eAaImF	nahrávat
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
zvolila	zvolit	k5eAaPmAgFnS	zvolit
název	název	k1gInSc4	název
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
Vydání	vydání	k1gNnSc4	vydání
alba	album	k1gNnSc2	album
předcházel	předcházet	k5eAaImAgInS	předcházet
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Rolling	Rolling	k1gInSc1	Rolling
in	in	k?	in
the	the	k?	the
Deep	Deep	k1gInSc1	Deep
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
obsadil	obsadit	k5eAaPmAgMnS	obsadit
2	[number]	k4	2
<g/>
.	.	kIx.	.
příčku	příčka	k1gFnSc4	příčka
v	v	k7c6	v
UK	UK	kA	UK
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
příčku	příčka	k1gFnSc4	příčka
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Belgii	Belgie	k1gFnSc6	Belgie
<g/>
,	,	kIx,	,
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
Německu	Německo	k1gNnSc6	Německo
<g/>
,	,	kIx,	,
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
Finsku	Finsko	k1gNnSc6	Finsko
nebo	nebo	k8xC	nebo
i	i	k9	i
v	v	k7c6	v
Jižní	jižní	k2eAgFnSc6d1	jižní
Koreji	Korea	k1gFnSc6	Korea
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
USA	USA	kA	USA
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
6	[number]	k4	6
<g/>
x	x	k?	x
platinový	platinový	k2eAgMnSc1d1	platinový
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
v	v	k7c6	v
UK	UK	kA	UK
jen	jen	k6eAd1	jen
zlatý	zlatý	k2eAgInSc1d1	zlatý
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
bylo	být	k5eAaImAgNnS	být
vydáno	vydat	k5eAaPmNgNnS	vydat
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2011	[number]	k4	2011
a	a	k8xC	a
obsadilo	obsadit	k5eAaPmAgNnS	obsadit
celou	celá	k1gFnSc4	celá
řadu	řad	k1gInSc2	řad
prvních	první	k4xOgNnPc2	první
míst	místo	k1gNnPc2	místo
světových	světový	k2eAgInPc2d1	světový
žebříčků	žebříček	k1gInPc2	žebříček
(	(	kIx(	(
<g/>
minimálně	minimálně	k6eAd1	minimálně
26	[number]	k4	26
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
UK	UK	kA	UK
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Česku	Česko	k1gNnSc6	Česko
<g/>
,	,	kIx,	,
Kanadě	Kanada	k1gFnSc6	Kanada
<g/>
,	,	kIx,	,
Mexiku	Mexiko	k1gNnSc6	Mexiko
<g/>
,	,	kIx,	,
Austrálii	Austrálie	k1gFnSc6	Austrálie
atd.	atd.	kA	atd.
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
16	[number]	k4	16
<g/>
x	x	k?	x
platinovým	platinový	k2eAgMnSc7d1	platinový
ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
<g/>
,	,	kIx,	,
12	[number]	k4	12
<g/>
x	x	k?	x
platinovým	platinový	k2eAgInPc3d1	platinový
v	v	k7c6	v
Irsku	Irsko	k1gNnSc6	Irsko
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
x	x	k?	x
platinovým	platinový	k2eAgMnSc7d1	platinový
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
<g/>
,	,	kIx,	,
diamantovým	diamantový	k2eAgMnSc7d1	diamantový
v	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
či	či	k8xC	či
11	[number]	k4	11
<g/>
x	x	k?	x
platinovým	platinový	k2eAgFnPc3d1	platinová
v	v	k7c6	v
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Tj.	tj.	kA	tj.
4,8	[number]	k4	4,8
milionů	milion	k4xCgInPc2	milion
prodaných	prodaný	k2eAgInPc2d1	prodaný
kusů	kus	k1gInPc2	kus
v	v	k7c6	v
UK	UK	kA	UK
<g/>
,	,	kIx,	,
11	[number]	k4	11
milionů	milion	k4xCgInPc2	milion
v	v	k7c6	v
USA	USA	kA	USA
<g/>
,	,	kIx,	,
celosvětově	celosvětově	k6eAd1	celosvětově
poté	poté	k6eAd1	poté
na	na	k7c4	na
30	[number]	k4	30
milionů	milion	k4xCgInPc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
alba	album	k1gNnSc2	album
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
úspěšnými	úspěšný	k2eAgFnPc7d1	úspěšná
ještě	ještě	k9	ještě
singly	singl	k1gInPc1	singl
"	"	kIx"	"
<g/>
Someone	Someon	k1gMnSc5	Someon
Like	Likus	k1gMnSc5	Likus
You	You	k1gMnSc5	You
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
konečně	konečně	k6eAd1	konečně
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
i	i	k9	i
na	na	k7c4	na
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
v	v	k7c6	v
britském	britský	k2eAgInSc6d1	britský
žebříčku	žebříček	k1gInSc6	žebříček
<g/>
,	,	kIx,	,
a	a	k8xC	a
"	"	kIx"	"
<g/>
Set	set	k1gInSc1	set
Fire	Fir	k1gFnSc2	Fir
to	ten	k3xDgNnSc1	ten
the	the	k?	the
Rain	Raino	k1gNnPc2	Raino
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
oba	dva	k4xCgMnPc1	dva
v	v	k7c6	v
USA	USA	kA	USA
obsadily	obsadit	k5eAaPmAgInP	obsadit
první	první	k4xOgNnSc4	první
místo	místo	k1gNnSc4	místo
a	a	k8xC	a
staly	stát	k5eAaPmAgFnP	stát
se	se	k3xPyFc4	se
multi-platinovými	multilatinový	k2eAgNnPc7d1	multi-platinové
<g/>
.	.	kIx.	.
</s>
<s>
Poslední	poslední	k2eAgInSc1d1	poslední
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Rumour	Rumour	k1gMnSc1	Rumour
Has	hasit	k5eAaImRp2nS	hasit
It	It	k1gFnSc1	It
<g/>
"	"	kIx"	"
již	již	k9	již
takový	takový	k3xDgInSc4	takový
úspěch	úspěch	k1gInSc4	úspěch
nezopakoval	zopakovat	k5eNaPmAgMnS	zopakovat
<g/>
.	.	kIx.	.
</s>
<s>
Úspěch	úspěch	k1gInSc1	úspěch
alba	album	k1gNnSc2	album
znamenal	znamenat	k5eAaImAgInS	znamenat
i	i	k9	i
zisk	zisk	k1gInSc1	zisk
mnoha	mnoho	k4c2	mnoho
ocenění	ocenění	k1gNnPc2	ocenění
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
dvě	dva	k4xCgFnPc1	dva
ceny	cena	k1gFnPc1	cena
z	z	k7c2	z
Brit	Brit	k1gMnSc1	Brit
Awards	Awards	k1gInSc1	Awards
nebo	nebo	k8xC	nebo
6	[number]	k4	6
cen	cena	k1gFnPc2	cena
Grammy	Gramma	k1gFnSc2	Gramma
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
pro	pro	k7c4	pro
Adele	Adele	k1gNnSc4	Adele
znamenal	znamenat	k5eAaImAgInS	znamenat
překonání	překonání	k1gNnSc4	překonání
mnoha	mnoho	k4c2	mnoho
hudebních	hudební	k2eAgInPc2d1	hudební
rekordů	rekord	k1gInPc2	rekord
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
se	s	k7c7	s
skladatelem	skladatel	k1gMnSc7	skladatel
Paulem	Paul	k1gMnSc7	Paul
Epworthem	Epworth	k1gInSc7	Epworth
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
složila	složit	k5eAaPmAgFnS	složit
a	a	k8xC	a
nazpívala	nazpívat	k5eAaPmAgFnS	nazpívat
eponymní	eponymní	k2eAgFnSc4d1	eponymní
titulní	titulní	k2eAgFnSc4d1	titulní
skladbu	skladba	k1gFnSc4	skladba
k	k	k7c3	k
23	[number]	k4	23
<g/>
.	.	kIx.	.
filmové	filmový	k2eAgFnSc2d1	filmová
bondovce	bondovce	k?	bondovce
Skyfall	Skyfall	k1gMnSc1	Skyfall
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
vydána	vydán	k2eAgFnSc1d1	vydána
5	[number]	k4	5
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
píseň	píseň	k1gFnSc1	píseň
získala	získat	k5eAaPmAgFnS	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
Oscara	Oscara	k1gFnSc1	Oscara
za	za	k7c4	za
Nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
původní	původní	k2eAgFnSc4d1	původní
píseň	píseň	k1gFnSc4	píseň
<g/>
,	,	kIx,	,
Zlatý	zlatý	k2eAgInSc4d1	zlatý
glóbus	glóbus	k1gInSc4	glóbus
za	za	k7c4	za
nejlepší	dobrý	k2eAgFnSc4d3	nejlepší
filmovou	filmový	k2eAgFnSc4d1	filmová
píseň	píseň	k1gFnSc4	píseň
a	a	k8xC	a
z	z	k7c2	z
BRIT	Brit	k1gMnSc1	Brit
Awards	Awardsa	k1gFnPc2	Awardsa
si	se	k3xPyFc3	se
odnesla	odnést	k5eAaPmAgFnS	odnést
vítězství	vítězství	k1gNnSc4	vítězství
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
Nejlepší	dobrý	k2eAgInSc4d3	nejlepší
britský	britský	k2eAgInSc4d1	britský
singl	singl	k1gInSc4	singl
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2013	[number]	k4	2013
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Buckinghamském	buckinghamský	k2eAgInSc6d1	buckinghamský
paláci	palác	k1gInSc6	palác
oceněna	ocenit	k5eAaPmNgFnS	ocenit
Řádem	řád	k1gInSc7	řád
britského	britský	k2eAgNnSc2d1	Britské
impéria	impérium	k1gNnSc2	impérium
za	za	k7c4	za
přínos	přínos	k1gInSc4	přínos
hudbě	hudba	k1gFnSc3	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
srpna	srpen	k1gInSc2	srpen
2015	[number]	k4	2015
začala	začít	k5eAaPmAgFnS	začít
nahrávací	nahrávací	k2eAgFnSc1d1	nahrávací
společnost	společnost	k1gFnSc1	společnost
XL	XL	kA	XL
propagovat	propagovat	k5eAaImF	propagovat
třetí	třetí	k4xOgNnSc4	třetí
studiové	studiový	k2eAgNnSc4d1	studiové
album	album	k1gNnSc4	album
Adele	Adele	k1gFnSc2	Adele
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
byl	být	k5eAaImAgInS	být
zveřejněn	zveřejněn	k2eAgInSc1d1	zveřejněn
název	název	k1gInSc1	název
alba	alba	k1gFnSc1	alba
25	[number]	k4	25
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
stanoveno	stanovit	k5eAaPmNgNnS	stanovit
datum	datum	k1gNnSc4	datum
vydání	vydání	k1gNnSc2	vydání
na	na	k7c4	na
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
byl	být	k5eAaImAgInS	být
také	také	k9	také
vydán	vydat	k5eAaPmNgInS	vydat
první	první	k4xOgInSc1	první
singl	singl	k1gInSc1	singl
"	"	kIx"	"
<g/>
Hello	Hello	k1gNnSc1	Hello
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
umístil	umístit	k5eAaPmAgMnS	umístit
na	na	k7c6	na
prvních	první	k4xOgFnPc6	první
příčkách	příčka	k1gFnPc6	příčka
mnoha	mnoho	k4c2	mnoho
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
žebříčků	žebříček	k1gInPc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
písni	píseň	k1gFnSc3	píseň
byl	být	k5eAaImAgInS	být
okamžitě	okamžitě	k6eAd1	okamžitě
vydán	vydat	k5eAaPmNgInS	vydat
i	i	k9	i
videoklip	videoklip	k1gInSc1	videoklip
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
na	na	k7c6	na
YouTube	YouTub	k1gInSc5	YouTub
během	během	k7c2	během
prvního	první	k4xOgInSc2	první
dne	den	k1gInSc2	den
zhlédlo	zhlédnout	k5eAaPmAgNnS	zhlédnout
27,7	[number]	k4	27,7
milionů	milion	k4xCgInPc2	milion
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
zlomil	zlomit	k5eAaPmAgMnS	zlomit
rekord	rekord	k1gInSc4	rekord
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
pouhý	pouhý	k2eAgInSc4d1	pouhý
jeden	jeden	k4xCgInSc4	jeden
měsíc	měsíc	k1gInSc4	měsíc
videoklip	videoklip	k1gInSc4	videoklip
zhlédlo	zhlédnout	k5eAaPmAgNnS	zhlédnout
452	[number]	k4	452
milionů	milion	k4xCgInPc2	milion
uživatelů	uživatel	k1gMnPc2	uživatel
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Hello	Hello	k1gNnSc1	Hello
<g/>
"	"	kIx"	"
za	za	k7c4	za
87	[number]	k4	87
dní	den	k1gInPc2	den
zhlédla	zhlédnout	k5eAaPmAgFnS	zhlédnout
1	[number]	k4	1
miliarda	miliarda	k4xCgFnSc1	miliarda
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
byl	být	k5eAaImAgMnS	být
pokořen	pokořen	k2eAgInSc4d1	pokořen
světový	světový	k2eAgInSc4d1	světový
rekord	rekord	k1gInSc4	rekord
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
se	se	k3xPyFc4	se
písně	píseň	k1gFnPc4	píseň
prodalo	prodat	k5eAaPmAgNnS	prodat
330	[number]	k4	330
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
během	během	k7c2	během
prvního	první	k4xOgInSc2	první
týdne	týden	k1gInSc2	týden
prodeje	prodej	k1gInSc2	prodej
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
nejprodávanějším	prodávaný	k2eAgInSc7d3	nejprodávanější
singlem	singl	k1gInSc7	singl
za	za	k7c4	za
poslední	poslední	k2eAgInPc4d1	poslední
tři	tři	k4xCgInPc4	tři
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
listopadu	listopad	k1gInSc2	listopad
singl	singl	k1gInSc1	singl
debutoval	debutovat	k5eAaBmAgInS	debutovat
v	v	k7c6	v
US	US	kA	US
žebříčku	žebříček	k1gInSc2	žebříček
na	na	k7c6	na
první	první	k4xOgFnSc6	první
příčce	příčka	k1gFnSc6	příčka
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	s	k7c7	s
prvním	první	k4xOgInSc7	první
digitálním	digitální	k2eAgInSc7d1	digitální
singlem	singl	k1gInSc7	singl
<g/>
,	,	kIx,	,
kterého	který	k3yRgNnSc2	který
se	se	k3xPyFc4	se
během	během	k7c2	během
prvního	první	k4xOgInSc2	první
týdne	týden	k1gInSc2	týden
prodalo	prodat	k5eAaPmAgNnS	prodat
(	(	kIx(	(
<g/>
placeně	placeně	k6eAd1	placeně
stáhlo	stáhnout	k5eAaPmAgNnS	stáhnout
<g/>
)	)	kIx)	)
přes	přes	k7c4	přes
milion	milion	k4xCgInSc4	milion
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
byl	být	k5eAaImAgInS	být
natočen	natočit	k5eAaBmNgInS	natočit
a	a	k8xC	a
na	na	k7c4	na
stanici	stanice	k1gFnSc4	stanice
BBC	BBC	kA	BBC
One	One	k1gMnSc2	One
odvysílán	odvysílat	k5eAaPmNgInS	odvysílat
i	i	k9	i
televizní	televizní	k2eAgInSc1d1	televizní
speciál	speciál	k1gInSc1	speciál
Adele	Adele	k1gFnSc2	Adele
at	at	k?	at
the	the	k?	the
BBC	BBC	kA	BBC
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
také	také	k9	také
sloužil	sloužit	k5eAaImAgInS	sloužit
k	k	k7c3	k
propagaci	propagace	k1gFnSc3	propagace
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
propagaci	propagace	k1gFnSc4	propagace
alba	album	k1gNnSc2	album
dále	daleko	k6eAd2	daleko
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
v	v	k7c6	v
americkém	americký	k2eAgInSc6d1	americký
pořadu	pořad	k1gInSc6	pořad
Saturday	Saturdaa	k1gFnSc2	Saturdaa
Night	Night	k2eAgInSc4d1	Night
Live	Live	k1gInSc4	Live
a	a	k8xC	a
vystoupila	vystoupit	k5eAaPmAgFnS	vystoupit
v	v	k7c4	v
Radio	radio	k1gNnSc4	radio
City	City	k1gFnSc2	City
Music	Music	k1gMnSc1	Music
Hall	Hall	k1gMnSc1	Hall
<g/>
;	;	kIx,	;
tento	tento	k3xDgInSc4	tento
koncert	koncert	k1gInSc4	koncert
později	pozdě	k6eAd2	pozdě
odvysílala	odvysílat	k5eAaPmAgFnS	odvysílat
stanice	stanice	k1gFnPc4	stanice
NBC	NBC	kA	NBC
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
25	[number]	k4	25
debutovalo	debutovat	k5eAaBmAgNnS	debutovat
na	na	k7c6	na
prvních	první	k4xOgFnPc6	první
příčkách	příčka	k1gFnPc6	příčka
mnoha	mnoho	k4c2	mnoho
světových	světový	k2eAgInPc2d1	světový
žebříčků	žebříček	k1gInPc2	žebříček
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
odhadů	odhad	k1gInPc2	odhad
společnosti	společnost	k1gFnSc2	společnost
Nielsen	Nielsna	k1gFnPc2	Nielsna
Music	Musice	k1gInPc2	Musice
<g/>
,	,	kIx,	,
založených	založený	k2eAgInPc2d1	založený
na	na	k7c6	na
prodeji	prodej	k1gInSc6	prodej
prvního	první	k4xOgInSc2	první
dne	den	k1gInSc2	den
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
v	v	k7c6	v
USA	USA	kA	USA
prodat	prodat	k5eAaPmF	prodat
2,5	[number]	k4	2,5
milionů	milion	k4xCgInPc2	milion
kusů	kus	k1gInPc2	kus
alba	album	k1gNnSc2	album
za	za	k7c4	za
první	první	k4xOgInSc4	první
týden	týden	k1gInSc4	týden
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
by	by	kYmCp3nP	by
stanovilo	stanovit	k5eAaPmAgNnS	stanovit
absolutní	absolutní	k2eAgInSc4d1	absolutní
rekord	rekord	k1gInSc4	rekord
<g/>
.	.	kIx.	.
</s>
<s>
Nahrávací	nahrávací	k2eAgFnSc1d1	nahrávací
společnost	společnost	k1gFnSc1	společnost
se	se	k3xPyFc4	se
na	na	k7c4	na
velký	velký	k2eAgInSc4d1	velký
zájem	zájem	k1gInSc4	zájem
připravila	připravit	k5eAaPmAgFnS	připravit
a	a	k8xC	a
do	do	k7c2	do
distribuce	distribuce	k1gFnSc2	distribuce
poslala	poslat	k5eAaPmAgFnS	poslat
3,6	[number]	k4	3,6
milionů	milion	k4xCgInPc2	milion
kusů	kus	k1gInPc2	kus
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
25	[number]	k4	25
se	se	k3xPyFc4	se
tím	ten	k3xDgNnSc7	ten
mělo	mít	k5eAaImAgNnS	mít
okamžitě	okamžitě	k6eAd1	okamžitě
stát	stát	k5eAaImF	stát
nejprodávanějším	prodávaný	k2eAgNnSc7d3	nejprodávanější
albem	album	k1gNnSc7	album
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
v	v	k7c6	v
USA	USA	kA	USA
prodalo	prodat	k5eAaPmAgNnS	prodat
za	za	k7c4	za
pouhé	pouhý	k2eAgInPc4d1	pouhý
tři	tři	k4xCgInPc4	tři
dny	den	k1gInPc4	den
prodeje	prodej	k1gInSc2	prodej
2,3	[number]	k4	2,3
milionů	milion	k4xCgInPc2	milion
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
nejrychleji	rychle	k6eAd3	rychle
se	s	k7c7	s
prodávaným	prodávaný	k2eAgNnSc7d1	prodávané
albem	album	k1gNnSc7	album
historie	historie	k1gFnSc2	historie
a	a	k8xC	a
nejprodávanějším	prodávaný	k2eAgNnSc7d3	nejprodávanější
albem	album	k1gNnSc7	album
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
první	první	k4xOgInSc4	první
týden	týden	k1gInSc4	týden
se	se	k3xPyFc4	se
celkově	celkově	k6eAd1	celkově
v	v	k7c6	v
USA	USA	kA	USA
prodalo	prodat	k5eAaPmAgNnS	prodat
3,38	[number]	k4	3,38
milionů	milion	k4xCgInPc2	milion
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
prvním	první	k4xOgNnSc7	první
albem	album	k1gNnSc7	album
historie	historie	k1gFnSc2	historie
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gNnSc1	jehož
se	se	k3xPyFc4	se
prodalo	prodat	k5eAaPmAgNnS	prodat
přes	přes	k7c4	přes
tři	tři	k4xCgInPc4	tři
miliony	milion	k4xCgInPc4	milion
kusů	kus	k1gInPc2	kus
za	za	k7c4	za
pouhý	pouhý	k2eAgInSc4d1	pouhý
týden	týden	k1gInSc4	týden
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
ledna	leden	k1gInSc2	leden
2016	[number]	k4	2016
se	se	k3xPyFc4	se
v	v	k7c6	v
USA	USA	kA	USA
prodalo	prodat	k5eAaPmAgNnS	prodat
7,6	[number]	k4	7,6
milionů	milion	k4xCgInPc2	milion
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Album	album	k1gNnSc1	album
se	se	k3xPyFc4	se
drželo	držet	k5eAaImAgNnS	držet
na	na	k7c6	na
první	první	k4xOgFnSc6	první
příčce	příčka	k1gFnSc6	příčka
žebříčku	žebříček	k1gInSc2	žebříček
Billboard	billboard	k1gInSc4	billboard
po	po	k7c4	po
sedm	sedm	k4xCc4	sedm
týdnů	týden	k1gInPc2	týden
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
se	se	k3xPyFc4	se
naposledy	naposledy	k6eAd1	naposledy
podařilo	podařit	k5eAaPmAgNnS	podařit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1987	[number]	k4	1987
Whitney	Whitnea	k1gMnSc2	Whitnea
Houston	Houston	k1gInSc4	Houston
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Kanadě	Kanada	k1gFnSc6	Kanada
se	se	k3xPyFc4	se
první	první	k4xOgInSc4	první
týden	týden	k1gInSc4	týden
prodalo	prodat	k5eAaPmAgNnS	prodat
306	[number]	k4	306
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
byl	být	k5eAaImAgMnS	být
zlomen	zlomen	k2eAgMnSc1d1	zlomen
kanadský	kanadský	k2eAgMnSc1d1	kanadský
rekord	rekord	k1gInSc4	rekord
Céline	Célin	k1gInSc5	Célin
Dion	Dion	k1gInSc1	Dion
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
domácím	domácí	k2eAgNnSc6d1	domácí
Spojeném	spojený	k2eAgNnSc6d1	spojené
království	království	k1gNnSc6	království
se	se	k3xPyFc4	se
alba	album	k1gNnSc2	album
prodalo	prodat	k5eAaPmAgNnS	prodat
800	[number]	k4	800
000	[number]	k4	000
kusů	kus	k1gInPc2	kus
během	během	k7c2	během
prvního	první	k4xOgInSc2	první
týdne	týden	k1gInSc2	týden
<g/>
,	,	kIx,	,
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
byl	být	k5eAaImAgInS	být
zlomen	zlomen	k2eAgInSc4d1	zlomen
historický	historický	k2eAgInSc4d1	historický
rekord	rekord	k1gInSc4	rekord
nejrychleji	rychle	k6eAd3	rychle
se	se	k3xPyFc4	se
prodávaného	prodávaný	k2eAgNnSc2d1	prodávané
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
čtyři	čtyři	k4xCgInPc4	čtyři
týdny	týden	k1gInPc4	týden
prodeje	prodej	k1gInSc2	prodej
se	se	k3xPyFc4	se
prodaly	prodat	k5eAaPmAgFnP	prodat
2	[number]	k4	2
miliony	milion	k4xCgInPc4	milion
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Celkový	celkový	k2eAgInSc1d1	celkový
prodej	prodej	k1gInSc1	prodej
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2016	[number]	k4	2016
činil	činit	k5eAaImAgInS	činit
2,5	[number]	k4	2,5
milionů	milion	k4xCgInPc2	milion
kusů	kus	k1gInPc2	kus
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
ledna	leden	k1gInSc2	leden
se	se	k3xPyFc4	se
celosvětově	celosvětově	k6eAd1	celosvětově
prodalo	prodat	k5eAaPmAgNnS	prodat
přes	přes	k7c4	přes
15	[number]	k4	15
milionů	milion	k4xCgInPc2	milion
kusů	kus	k1gInPc2	kus
alba	album	k1gNnSc2	album
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
února	únor	k1gInSc2	únor
do	do	k7c2	do
listopadu	listopad	k1gInSc2	listopad
2016	[number]	k4	2016
se	se	k3xPyFc4	se
Adele	Adele	k1gFnSc1	Adele
vydala	vydat	k5eAaPmAgFnS	vydat
na	na	k7c4	na
koncertní	koncertní	k2eAgFnSc4d1	koncertní
šňůru	šňůra	k1gFnSc4	šňůra
po	po	k7c6	po
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
Evropě	Evropa	k1gFnSc6	Evropa
a	a	k8xC	a
Severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
proběhlo	proběhnout	k5eAaPmAgNnS	proběhnout
celkem	celkem	k6eAd1	celkem
52	[number]	k4	52
koncertů	koncert	k1gInPc2	koncert
a	a	k8xC	a
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
58	[number]	k4	58
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2017	[number]	k4	2017
Adele	Adele	k1gFnSc1	Adele
odzpívala	odzpívat	k5eAaPmAgFnS	odzpívat
11	[number]	k4	11
koncertů	koncert	k1gInPc2	koncert
v	v	k7c6	v
Austrálii	Austrálie	k1gFnSc6	Austrálie
a	a	k8xC	a
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
Zélandu	Zéland	k1gInSc6	Zéland
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
její	její	k3xOp3gNnSc4	její
světové	světový	k2eAgNnSc4d1	světové
turné	turné	k1gNnSc4	turné
čítá	čítat	k5eAaImIp3nS	čítat
123	[number]	k4	123
koncertů	koncert	k1gInPc2	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Závěrem	závěr	k1gInSc7	závěr
velkolepého	velkolepý	k2eAgNnSc2d1	velkolepé
turné	turné	k1gNnSc2	turné
byly	být	k5eAaImAgFnP	být
koncerty	koncert	k1gInPc4	koncert
v	v	k7c6	v
prestižní	prestižní	k2eAgFnSc6d1	prestižní
londýnské	londýnský	k2eAgFnSc6d1	londýnská
aréně	aréna	k1gFnSc6	aréna
Wembley	Wemblea	k1gFnSc2	Wemblea
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yQgFnPc2	který
bylo	být	k5eAaImAgNnS	být
natočeno	natočit	k5eAaBmNgNnS	natočit
DVD	DVD	kA	DVD
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInSc4	první
vztah	vztah	k1gInSc4	vztah
prožila	prožít	k5eAaPmAgFnS	prožít
s	s	k7c7	s
bisexuálem	bisexuál	k1gMnSc7	bisexuál
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
ji	on	k3xPp3gFnSc4	on
inspiroval	inspirovat	k5eAaBmAgInS	inspirovat
k	k	k7c3	k
hudebním	hudební	k2eAgInPc3d1	hudební
textům	text	k1gInPc3	text
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2012	[number]	k4	2012
bylo	být	k5eAaImAgNnS	být
oznámeno	oznámit	k5eAaPmNgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Adele	Adele	k1gFnSc1	Adele
udržuje	udržovat	k5eAaImIp3nS	udržovat
partnerský	partnerský	k2eAgInSc4d1	partnerský
vztah	vztah	k1gInSc4	vztah
s	s	k7c7	s
ředitelem	ředitel	k1gMnSc7	ředitel
charitativní	charitativní	k2eAgFnSc2d1	charitativní
organizace	organizace	k1gFnSc2	organizace
Drop	drop	k1gMnSc1	drop
<g/>
4	[number]	k4	4
<g/>
Drop	drop	k1gMnSc1	drop
Simonem	Simon	k1gMnSc7	Simon
Koneckim	Koneckima	k1gFnPc2	Koneckima
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
sdělila	sdělit	k5eAaPmAgFnS	sdělit
<g/>
,	,	kIx,	,
že	že	k8xS	že
s	s	k7c7	s
přítelem	přítel	k1gMnSc7	přítel
čekají	čekat	k5eAaImIp3nP	čekat
narození	narození	k1gNnSc6	narození
potomka	potomek	k1gMnSc4	potomek
a	a	k8xC	a
19	[number]	k4	19
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
porodila	porodit	k5eAaPmAgFnS	porodit
syna	syn	k1gMnSc4	syn
Angelo	Angela	k1gFnSc5	Angela
Jamese	Jamese	k1gFnSc2	Jamese
<g/>
.	.	kIx.	.
</s>
<s>
Konecki	Konecki	k6eAd1	Konecki
již	již	k6eAd1	již
má	mít	k5eAaImIp3nS	mít
dceru	dcera	k1gFnSc4	dcera
z	z	k7c2	z
bývalého	bývalý	k2eAgNnSc2d1	bývalé
manželství	manželství	k1gNnSc2	manželství
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2008	[number]	k4	2008
koupila	koupit	k5eAaPmAgFnS	koupit
byt	byt	k1gInSc4	byt
v	v	k7c6	v
londýnském	londýnský	k2eAgNnSc6d1	Londýnské
Notting	Notting	k1gInSc1	Notting
Hillu	Hill	k1gInSc6	Hill
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2011	[number]	k4	2011
se	se	k3xPyFc4	se
s	s	k7c7	s
partnerem	partner	k1gMnSc7	partner
přestěhovala	přestěhovat	k5eAaPmAgFnS	přestěhovat
do	do	k7c2	do
desetihektarového	desetihektarový	k2eAgNnSc2d1	desetihektarové
sídla	sídlo	k1gNnSc2	sídlo
s	s	k7c7	s
deseti	deset	k4xCc7	deset
ložnicemi	ložnice	k1gFnPc7	ložnice
v	v	k7c6	v
anglickém	anglický	k2eAgNnSc6d1	anglické
hrabství	hrabství	k1gNnSc6	hrabství
Západní	západní	k2eAgInSc1d1	západní
Sussex	Sussex	k1gInSc1	Sussex
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Diskografie	diskografie	k1gFnSc2	diskografie
Adele	Adele	k1gFnSc2	Adele
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Hometown	Hometown	k1gInSc1	Hometown
Glory	Glora	k1gFnSc2	Glora
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Chasing	Chasing	k1gInSc1	Chasing
Pavements	Pavements	k1gInSc1	Pavements
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Cold	Cold	k1gMnSc1	Cold
Shoulder	Shoulder	k1gMnSc1	Shoulder
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Rolling	Rolling	k1gInSc1	Rolling
in	in	k?	in
the	the	k?	the
Deep	Deep	k1gInSc1	Deep
<g/>
"	"	kIx"	"
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Someone	Someon	k1gMnSc5	Someon
Like	Likus	k1gMnSc5	Likus
You	You	k1gMnSc5	You
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Set	set	k1gInSc1	set
Fire	Fire	k1gFnPc2	Fire
To	to	k9	to
The	The	k1gMnSc1	The
Rain	Rain	k1gMnSc1	Rain
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Turning	Turning	k1gInSc1	Turning
Tables	Tables	k1gInSc1	Tables
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Skyfall	Skyfall	k1gInSc1	Skyfall
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2012	[number]	k4	2012
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Hello	Hello	k1gNnSc1	Hello
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
When	When	k1gInSc1	When
We	We	k1gMnSc2	We
Were	Wer	k1gMnSc2	Wer
Young	Young	k1gMnSc1	Young
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
"	"	kIx"	"
<g/>
Send	Send	k1gInSc1	Send
My	my	k3xPp1nPc1	my
Love	lov	k1gInSc5	lov
(	(	kIx(	(
<g/>
To	to	k9	to
Your	Your	k1gMnSc1	Your
New	New	k1gMnSc1	New
Lover	Lover	k1gMnSc1	Lover
<g/>
)	)	kIx)	)
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
2015	[number]	k4	2015
<g/>
)	)	kIx)	)
An	An	k1gFnPc2	An
Evening	Evening	k1gInSc1	Evening
with	with	k1gMnSc1	with
Adele	Adele	k1gInSc1	Adele
(	(	kIx(	(
<g/>
2008	[number]	k4	2008
<g/>
–	–	k?	–
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
)	)	kIx)	)
Adele	Adele	k1gNnSc3	Adele
Live	Live	k1gNnSc3	Live
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Adele	Adele	k1gInSc1	Adele
Live	Live	k1gNnSc1	Live
2016	[number]	k4	2016
(	(	kIx(	(
<g/>
2016	[number]	k4	2016
<g/>
)	)	kIx)	)
</s>
