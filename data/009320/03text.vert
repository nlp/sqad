<p>
<s>
Etiopie	Etiopie	k1gFnSc1	Etiopie
(	(	kIx(	(
<g/>
amharsky	amharsky	k6eAd1	amharsky
ኢ	ኢ	k?	ኢ
<g/>
,	,	kIx,	,
Itjopja	Itjopja	k1gFnSc1	Itjopja
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
plným	plný	k2eAgInSc7d1	plný
názvem	název	k1gInSc7	název
Etiopská	etiopský	k2eAgFnSc1d1	etiopská
federativní	federativní	k2eAgFnSc1d1	federativní
demokratická	demokratický	k2eAgFnSc1d1	demokratická
republika	republika	k1gFnSc1	republika
(	(	kIx(	(
<g/>
amharsky	amharsky	k6eAd1	amharsky
የ	የ	k?	የ
ፈ	ፈ	k?	ፈ
ዲ	ዲ	k?	ዲ
ሪ	ሪ	k?	ሪ
<g/>
,	,	kIx,	,
Je	být	k5eAaImIp3nS	být
<g/>
'	'	kIx"	'
<g/>
Itjop	Itjop	k1gInSc1	Itjop
<g/>
'	'	kIx"	'
<g/>
ja	ja	k?	ja
federalawi	federalawi	k6eAd1	federalawi
dimokrasijawi	dimokrasijawi	k6eAd1	dimokrasijawi
ripeblik	ripeblika	k1gFnPc2	ripeblika
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
též	též	k9	též
Habeš	Habeš	k1gFnSc1	Habeš
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vnitrozemský	vnitrozemský	k2eAgInSc1d1	vnitrozemský
stát	stát	k1gInSc1	stát
v	v	k7c6	v
Africkém	africký	k2eAgInSc6d1	africký
rohu	roh	k1gInSc6	roh
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Sousedí	sousedit	k5eAaImIp3nS	sousedit
na	na	k7c6	na
severu	sever	k1gInSc6	sever
s	s	k7c7	s
Eritrejí	Eritrea	k1gFnSc7	Eritrea
<g/>
,	,	kIx,	,
na	na	k7c6	na
východě	východ	k1gInSc6	východ
s	s	k7c7	s
Džibutskem	Džibutsko	k1gNnSc7	Džibutsko
a	a	k8xC	a
Somálskem	Somálsko	k1gNnSc7	Somálsko
<g/>
,	,	kIx,	,
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
s	s	k7c7	s
Keňou	Keňa	k1gFnSc7	Keňa
a	a	k8xC	a
se	s	k7c7	s
Súdánem	Súdán	k1gInSc7	Súdán
a	a	k8xC	a
Jižním	jižní	k2eAgInSc7d1	jižní
Súdánem	Súdán	k1gInSc7	Súdán
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
82	[number]	k4	82
101	[number]	k4	101
998	[number]	k4	998
a	a	k8xC	a
v	v	k7c6	v
r.	r.	kA	r.
2017	[number]	k4	2017
už	už	k6eAd1	už
105	[number]	k4	105
350	[number]	k4	350
020	[number]	k4	020
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Addis	Addis	k1gFnSc1	Addis
Abeba	Abeba	k1gFnSc1	Abeba
<g/>
,	,	kIx,	,
úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
federace	federace	k1gFnSc2	federace
je	být	k5eAaImIp3nS	být
amharština	amharština	k1gFnSc1	amharština
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
etiopském	etiopský	k2eAgNnSc6d1	etiopské
území	území	k1gNnSc6	území
se	se	k3xPyFc4	se
dochovaly	dochovat	k5eAaPmAgFnP	dochovat
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
jednoho	jeden	k4xCgInSc2	jeden
z	z	k7c2	z
nejstarších	starý	k2eAgInPc2d3	nejstarší
předků	předek	k1gInPc2	předek
člověka	člověk	k1gMnSc2	člověk
z	z	k7c2	z
rodu	rod	k1gInSc2	rod
Australopithecus	Australopithecus	k1gInSc1	Australopithecus
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgMnSc1	první
lépe	dobře	k6eAd2	dobře
zdokumentovaný	zdokumentovaný	k2eAgInSc1d1	zdokumentovaný
historický	historický	k2eAgInSc1d1	historický
státní	státní	k2eAgInSc1d1	státní
útvar	útvar	k1gInSc1	útvar
na	na	k7c6	na
etiopském	etiopský	k2eAgNnSc6d1	etiopské
území	území	k1gNnSc6	území
představuje	představovat	k5eAaImIp3nS	představovat
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
aksumská	aksumský	k2eAgFnSc1d1	aksumská
říše	říše	k1gFnSc1	říše
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
vládla	vládnout	k5eAaImAgFnS	vládnout
šalomounská	šalomounský	k2eAgFnSc1d1	šalomounská
dynastie	dynastie	k1gFnSc1	dynastie
založená	založený	k2eAgFnSc1d1	založená
podle	podle	k7c2	podle
legend	legenda	k1gFnPc2	legenda
potomkem	potomek	k1gMnSc7	potomek
krále	král	k1gMnSc2	král
Šalomouna	Šalomoun	k1gMnSc2	Šalomoun
a	a	k8xC	a
královny	královna	k1gFnSc2	královna
ze	z	k7c2	z
Sáby	Sába	k1gFnSc2	Sába
<g/>
.	.	kIx.	.
</s>
<s>
Aksumští	Aksumský	k2eAgMnPc1d1	Aksumský
vládci	vládce	k1gMnPc1	vládce
přijali	přijmout	k5eAaPmAgMnP	přijmout
ve	v	k7c6	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Následující	následující	k2eAgNnSc1d1	následující
staletí	staletí	k1gNnSc1	staletí
–	–	k?	–
především	především	k6eAd1	především
v	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
ztráty	ztráta	k1gFnSc2	ztráta
obchodních	obchodní	k2eAgInPc2d1	obchodní
kontaktů	kontakt	k1gInPc2	kontakt
a	a	k8xC	a
islámské	islámský	k2eAgFnSc2d1	islámská
expanze	expanze	k1gFnSc2	expanze
–	–	k?	–
znamenala	znamenat	k5eAaImAgFnS	znamenat
úpadek	úpadek	k1gInSc4	úpadek
a	a	k8xC	a
do	do	k7c2	do
značné	značný	k2eAgFnSc2d1	značná
míry	míra	k1gFnSc2	míra
i	i	k8xC	i
izolaci	izolace	k1gFnSc4	izolace
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
započala	započnout	k5eAaPmAgFnS	započnout
postupná	postupný	k2eAgFnSc1d1	postupná
modernizace	modernizace	k1gFnSc1	modernizace
a	a	k8xC	a
soupeření	soupeření	k1gNnSc1	soupeření
s	s	k7c7	s
evropským	evropský	k2eAgInSc7d1	evropský
kolonialismem	kolonialismus	k1gInSc7	kolonialismus
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
s	s	k7c7	s
pětiletou	pětiletý	k2eAgFnSc7d1	pětiletá
přestávkou	přestávka	k1gFnSc7	přestávka
v	v	k7c6	v
době	doba	k1gFnSc6	doba
italské	italský	k2eAgFnSc2d1	italská
okupace	okupace	k1gFnSc2	okupace
<g/>
,	,	kIx,	,
etiopským	etiopský	k2eAgMnSc7d1	etiopský
císařem	císař	k1gMnSc7	císař
Haile	Hail	k1gMnSc2	Hail
Selassie	Selassie	k1gFnSc2	Selassie
I.	I.	kA	I.
Ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgInS	být
poté	poté	k6eAd1	poté
svržen	svrhnout	k5eAaPmNgInS	svrhnout
Mengistu	Mengist	k1gInSc3	Mengist
Haile	Haile	k1gFnSc2	Haile
Mariamem	Mariam	k1gInSc7	Mariam
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
nastolil	nastolit	k5eAaPmAgInS	nastolit
komunistický	komunistický	k2eAgInSc1d1	komunistický
režim	režim	k1gInSc1	režim
<g/>
.	.	kIx.	.
</s>
<s>
Konec	konec	k1gInSc1	konec
tohoto	tento	k3xDgInSc2	tento
režimu	režim	k1gInSc2	režim
přišel	přijít	k5eAaPmAgInS	přijít
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
zhroucením	zhroucení	k1gNnSc7	zhroucení
sovětského	sovětský	k2eAgInSc2d1	sovětský
bloku	blok	k1gInSc2	blok
a	a	k8xC	a
Mengistu	Mengist	k1gInSc2	Mengist
musel	muset	k5eAaImAgInS	muset
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
uprchnout	uprchnout	k5eAaPmF	uprchnout
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Etiopie	Etiopie	k1gFnSc1	Etiopie
se	se	k3xPyFc4	se
federalizovala	federalizovat	k5eAaBmAgFnS	federalizovat
a	a	k8xC	a
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
nové	nový	k2eAgFnSc2d1	nová
vlády	vláda	k1gFnSc2	vláda
pod	pod	k7c7	pod
vedením	vedení	k1gNnSc7	vedení
Etiopské	etiopský	k2eAgFnSc2d1	etiopská
lidové	lidový	k2eAgFnSc2d1	lidová
revoluční	revoluční	k2eAgFnSc2d1	revoluční
demokratické	demokratický	k2eAgFnSc2d1	demokratická
fronty	fronta	k1gFnSc2	fronta
stanul	stanout	k5eAaPmAgMnS	stanout
Meles	Meles	k1gMnSc1	Meles
Zenawi	Zenaw	k1gFnSc2	Zenaw
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
postupem	postupem	k7c2	postupem
času	čas	k1gInSc2	čas
zavedl	zavést	k5eAaPmAgInS	zavést
autoritářský	autoritářský	k2eAgInSc1d1	autoritářský
režim	režim	k1gInSc1	režim
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
moci	moc	k1gFnSc2	moc
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Etiopský	etiopský	k2eAgInSc1d1	etiopský
stát	stát	k1gInSc1	stát
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejchudší	chudý	k2eAgFnPc4d3	nejchudší
země	zem	k1gFnPc4	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělství	zemědělství	k1gNnSc1	zemědělství
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
závislé	závislý	k2eAgNnSc1d1	závislé
na	na	k7c6	na
podnebí	podnebí	k1gNnSc6	podnebí
a	a	k8xC	a
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
k	k	k7c3	k
rozvinutým	rozvinutý	k2eAgInPc3d1	rozvinutý
státům	stát	k1gInPc3	stát
málo	málo	k6eAd1	málo
modernizované	modernizovaný	k2eAgInPc1d1	modernizovaný
<g/>
.	.	kIx.	.
</s>
<s>
Průmysl	průmysl	k1gInSc1	průmysl
je	být	k5eAaImIp3nS	být
nevyvinutý	vyvinutý	k2eNgInSc1d1	nevyvinutý
a	a	k8xC	a
služby	služba	k1gFnPc1	služba
nepříliš	příliš	k6eNd1	příliš
kvalitní	kvalitní	k2eAgFnPc1d1	kvalitní
<g/>
.	.	kIx.	.
</s>
<s>
Dosud	dosud	k6eAd1	dosud
špatný	špatný	k2eAgInSc1d1	špatný
stav	stav	k1gInSc1	stav
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
vládním	vládní	k2eAgFnPc3d1	vládní
investicím	investice	k1gFnPc3	investice
postupně	postupně	k6eAd1	postupně
zlepšuje	zlepšovat	k5eAaImIp3nS	zlepšovat
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
v	v	k7c6	v
jinak	jinak	k6eAd1	jinak
špatné	špatný	k2eAgFnSc6d1	špatná
etiopské	etiopský	k2eAgFnSc6d1	etiopská
dopravě	doprava	k1gFnSc6	doprava
tvoří	tvořit	k5eAaImIp3nS	tvořit
Ethiopian	Ethiopian	k1gInSc4	Ethiopian
Airlines	Airlinesa	k1gFnPc2	Airlinesa
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
tři	tři	k4xCgFnPc4	tři
nejdůležitější	důležitý	k2eAgFnPc4d3	nejdůležitější
letecké	letecký	k2eAgFnPc4d1	letecká
společnosti	společnost	k1gFnPc4	společnost
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
značné	značný	k2eAgFnSc3d1	značná
korupci	korupce	k1gFnSc3	korupce
a	a	k8xC	a
nedostatečným	dostatečný	k2eNgFnPc3d1	nedostatečná
službám	služba	k1gFnPc3	služba
se	se	k3xPyFc4	se
ekonomika	ekonomika	k1gFnSc1	ekonomika
Etiopie	Etiopie	k1gFnSc1	Etiopie
rychle	rychle	k6eAd1	rychle
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
<g/>
;	;	kIx,	;
země	země	k1gFnSc1	země
je	být	k5eAaImIp3nS	být
však	však	k9	však
přesto	přesto	k8xC	přesto
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
zahraniční	zahraniční	k2eAgFnSc6d1	zahraniční
pomoci	pomoc	k1gFnSc6	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Etiopská	etiopský	k2eAgFnSc1d1	etiopská
armáda	armáda	k1gFnSc1	armáda
představuje	představovat	k5eAaImIp3nS	představovat
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejsilnějších	silný	k2eAgFnPc2d3	nejsilnější
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
úzce	úzko	k6eAd1	úzko
vojensky	vojensky	k6eAd1	vojensky
spolupracuje	spolupracovat	k5eAaImIp3nS	spolupracovat
se	s	k7c7	s
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zahraničních	zahraniční	k2eAgInPc2d1	zahraniční
vztahů	vztah	k1gInPc2	vztah
udržuje	udržovat	k5eAaImIp3nS	udržovat
Etiopie	Etiopie	k1gFnSc1	Etiopie
dobré	dobrý	k2eAgInPc1d1	dobrý
vztahy	vztah	k1gInPc1	vztah
zvláště	zvláště	k9	zvláště
s	s	k7c7	s
okolními	okolní	k2eAgInPc7d1	okolní
státy	stát	k1gInPc7	stát
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Eritreje	Eritrea	k1gFnSc2	Eritrea
a	a	k8xC	a
Somálska	Somálsko	k1gNnSc2	Somálsko
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
investorů	investor	k1gMnPc2	investor
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
nejvíce	nejvíce	k6eAd1	nejvíce
prosazuje	prosazovat	k5eAaImIp3nS	prosazovat
Indie	Indie	k1gFnSc1	Indie
a	a	k8xC	a
Čína	Čína	k1gFnSc1	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Etiopie	Etiopie	k1gFnSc1	Etiopie
má	mít	k5eAaImIp3nS	mít
rovněž	rovněž	k9	rovněž
bohatou	bohatý	k2eAgFnSc4d1	bohatá
kulturu	kultura	k1gFnSc4	kultura
založenou	založený	k2eAgFnSc4d1	založená
na	na	k7c6	na
dlouhé	dlouhý	k2eAgFnSc6d1	dlouhá
historické	historický	k2eAgFnSc6d1	historická
tradici	tradice	k1gFnSc6	tradice
i	i	k8xC	i
kontaktech	kontakt	k1gInPc6	kontakt
s	s	k7c7	s
okolním	okolní	k2eAgInSc7d1	okolní
světem	svět	k1gInSc7	svět
a	a	k8xC	a
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
množství	množství	k1gNnSc1	množství
kulturních	kulturní	k2eAgFnPc2d1	kulturní
i	i	k8xC	i
přírodních	přírodní	k2eAgFnPc2d1	přírodní
památek	památka	k1gFnPc2	památka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Etymologie	etymologie	k1gFnSc2	etymologie
==	==	k?	==
</s>
</p>
<p>
<s>
Slovo	slovo	k1gNnSc1	slovo
Etiopie	Etiopie	k1gFnSc2	Etiopie
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řečtiny	řečtina	k1gFnSc2	řečtina
(	(	kIx(	(
<g/>
Α	Α	k?	Α
<g/>
,	,	kIx,	,
Aethiopia	Aethiopia	k1gFnSc1	Aethiopia
<g/>
)	)	kIx)	)
a	a	k8xC	a
obecně	obecně	k6eAd1	obecně
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
přeložit	přeložit	k5eAaPmF	přeložit
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
země	země	k1gFnSc1	země
opálených	opálený	k2eAgMnPc2d1	opálený
lidí	člověk	k1gMnPc2	člověk
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
výraz	výraz	k1gInSc1	výraz
se	se	k3xPyFc4	se
nicméně	nicméně	k8xC	nicméně
nevztahoval	vztahovat	k5eNaImAgMnS	vztahovat
pouze	pouze	k6eAd1	pouze
na	na	k7c4	na
dnešní	dnešní	k2eAgNnSc4d1	dnešní
etiopské	etiopský	k2eAgNnSc4d1	etiopské
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
souhrnně	souhrnně	k6eAd1	souhrnně
označoval	označovat	k5eAaImAgInS	označovat
země	zem	k1gFnPc4	zem
s	s	k7c7	s
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
tmavší	tmavý	k2eAgFnSc2d2	tmavší
pleti	pleť	k1gFnSc2	pleť
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
též	též	k9	též
užíval	užívat	k5eAaImAgInS	užívat
pro	pro	k7c4	pro
označení	označení	k1gNnSc4	označení
země	zem	k1gFnSc2	zem
Kuš	kuš	k0	kuš
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
Bible	bible	k1gFnSc2	bible
se	se	k3xPyFc4	se
slovo	slovo	k1gNnSc1	slovo
Etiopie	Etiopie	k1gFnSc2	Etiopie
objevuje	objevovat	k5eAaImIp3nS	objevovat
například	například	k6eAd1	například
v	v	k7c6	v
Homérově	Homérův	k2eAgFnSc6d1	Homérova
Iliadě	Iliada	k1gFnSc6	Iliada
a	a	k8xC	a
Odyssei	Odysse	k1gFnSc6	Odysse
<g/>
.	.	kIx.	.
</s>
<s>
Druhý	druhý	k4xOgInSc1	druhý
název	název	k1gInSc1	název
<g/>
,	,	kIx,	,
Habeš	Habeš	k1gFnSc1	Habeš
nebo	nebo	k8xC	nebo
též	též	k9	též
Abyssinia	Abyssinium	k1gNnPc1	Abyssinium
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
pojmenování	pojmenování	k1gNnSc2	pojmenování
kmenové	kmenový	k2eAgFnSc2d1	kmenová
skupiny	skupina	k1gFnSc2	skupina
Habašat	Habašat	k1gInSc1	Habašat
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
později	pozdě	k6eAd2	pozdě
přejali	přejmout	k5eAaPmAgMnP	přejmout
Arabové	Arab	k1gMnPc1	Arab
a	a	k8xC	a
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
al	ala	k1gFnPc2	ala
Habaša	Habašum	k1gNnSc2	Habašum
tím	ten	k3xDgMnSc7	ten
označovali	označovat	k5eAaImAgMnP	označovat
zdejší	zdejší	k2eAgMnPc4d1	zdejší
obyvatele	obyvatel	k1gMnPc4	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Termín	termín	k1gInSc1	termín
Habeša	Habeš	k1gInSc2	Habeš
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
dodnes	dodnes	k6eAd1	dodnes
<g/>
,	,	kIx,	,
většinou	většinou	k6eAd1	většinou
pro	pro	k7c4	pro
vymezení	vymezení	k1gNnSc4	vymezení
etiopské	etiopský	k2eAgFnSc2d1	etiopská
rasové	rasový	k2eAgFnSc2d1	rasová
skupiny	skupina	k1gFnSc2	skupina
vůči	vůči	k7c3	vůči
černochům	černoch	k1gMnPc3	černoch
i	i	k8xC	i
bělochům	běloch	k1gMnPc3	běloch
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
jím	on	k3xPp3gNnSc7	on
nazývají	nazývat	k5eAaImIp3nP	nazývat
tradiční	tradiční	k2eAgInPc4d1	tradiční
etiopské	etiopský	k2eAgInPc4d1	etiopský
výrobky	výrobek	k1gInPc4	výrobek
<g/>
,	,	kIx,	,
pokrmy	pokrm	k1gInPc4	pokrm
a	a	k8xC	a
nápoje	nápoj	k1gInPc4	nápoj
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Pravěk	pravěk	k1gInSc1	pravěk
a	a	k8xC	a
starověk	starověk	k1gInSc1	starověk
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
etiopském	etiopský	k2eAgNnSc6d1	etiopské
území	území	k1gNnSc6	území
žijí	žít	k5eAaImIp3nP	žít
lidé	člověk	k1gMnPc1	člověk
již	již	k6eAd1	již
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
několika	několik	k4yIc2	několik
milionů	milion	k4xCgInPc2	milion
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
zde	zde	k6eAd1	zde
byly	být	k5eAaImAgInP	být
také	také	k6eAd1	také
nalezeny	naleznout	k5eAaPmNgInP	naleznout
početné	početný	k2eAgInPc1d1	početný
kosterní	kosterní	k2eAgInPc1d1	kosterní
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
lidských	lidský	k2eAgMnPc2d1	lidský
předků	předek	k1gMnPc2	předek
rodu	rod	k1gInSc2	rod
Ardipithecus	Ardipithecus	k1gMnSc1	Ardipithecus
a	a	k8xC	a
Australopithecus	Australopithecus	k1gMnSc1	Australopithecus
<g/>
.	.	kIx.	.
</s>
<s>
Nepřetržitá	přetržitý	k2eNgFnSc1d1	nepřetržitá
přítomnost	přítomnost	k1gFnSc1	přítomnost
člověka	člověk	k1gMnSc2	člověk
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
prokázána	prokázat	k5eAaPmNgFnS	prokázat
zhruba	zhruba	k6eAd1	zhruba
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1	[number]	k4	1
000	[number]	k4	000
000	[number]	k4	000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
do	do	k7c2	do
období	období	k1gNnSc2	období
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
4000	[number]	k4	4000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Ze	z	k7c2	z
starších	starý	k2eAgFnPc2d2	starší
etiopských	etiopský	k2eAgFnPc2d1	etiopská
legend	legenda	k1gFnPc2	legenda
je	být	k5eAaImIp3nS	být
nejvýznamnější	významný	k2eAgFnSc4d3	nejvýznamnější
pověst	pověst	k1gFnSc4	pověst
o	o	k7c6	o
královně	královna	k1gFnSc6	královna
ze	z	k7c2	z
Sáby	Sába	k1gFnSc2	Sába
a	a	k8xC	a
králi	král	k1gMnSc3	král
Šalomounovi	Šalomoun	k1gMnSc3	Šalomoun
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc1	jejichž
syn	syn	k1gMnSc1	syn
Menelik	Menelika	k1gFnPc2	Menelika
I.	I.	kA	I.
měl	mít	k5eAaImAgInS	mít
nastoupit	nastoupit	k5eAaPmF	nastoupit
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
roku	rok	k1gInSc2	rok
982	[number]	k4	982
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l	l	kA	l
a	a	k8xC	a
být	být	k5eAaImF	být
zakladatelem	zakladatel	k1gMnSc7	zakladatel
šalomounovské	šalomounovský	k2eAgFnSc2d1	šalomounovský
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
dynastie	dynastie	k1gFnSc1	dynastie
pak	pak	k6eAd1	pak
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
s	s	k7c7	s
přestávkami	přestávka	k1gFnPc7	přestávka
vládla	vládnout	k5eAaImAgFnS	vládnout
téměř	téměř	k6eAd1	téměř
dvě	dva	k4xCgNnPc4	dva
tisíciletí	tisíciletí	k1gNnPc4	tisíciletí
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
monarchie	monarchie	k1gFnSc2	monarchie
svržena	svrhnout	k5eAaPmNgFnS	svrhnout
komunistickým	komunistický	k2eAgInSc7d1	komunistický
převratem	převrat	k1gInSc7	převrat
<g/>
.	.	kIx.	.
<g/>
Prvním	první	k4xOgMnSc7	první
lépe	dobře	k6eAd2	dobře
zdokumentovaným	zdokumentovaný	k2eAgInSc7d1	zdokumentovaný
historickým	historický	k2eAgInSc7d1	historický
státním	státní	k2eAgInSc7d1	státní
útvarem	útvar	k1gInSc7	útvar
zde	zde	k6eAd1	zde
byla	být	k5eAaImAgFnS	být
aksumská	aksumský	k2eAgFnSc1d1	aksumská
říše	říše	k1gFnSc1	říše
<g/>
,	,	kIx,	,
rozkládající	rozkládající	k2eAgFnSc1d1	rozkládající
se	se	k3xPyFc4	se
vedle	vedle	k7c2	vedle
etiopského	etiopský	k2eAgNnSc2d1	etiopské
území	území	k1gNnSc2	území
také	také	k9	také
v	v	k7c6	v
jihozápadní	jihozápadní	k2eAgFnSc6d1	jihozápadní
Arábii	Arábie	k1gFnSc6	Arábie
<g/>
.	.	kIx.	.
<g/>
Zhruba	zhruba	k6eAd1	zhruba
někdy	někdy	k6eAd1	někdy
po	po	k7c6	po
roce	rok	k1gInSc6	rok
325	[number]	k4	325
přijal	přijmout	k5eAaPmAgMnS	přijmout
aksumský	aksumský	k2eAgMnSc1d1	aksumský
vládce	vládce	k1gMnSc1	vládce
Enzana	Enzan	k1gMnSc2	Enzan
zásluhou	zásluhou	k7c2	zásluhou
svatého	svatý	k2eAgNnSc2d1	svaté
Frumentia	Frumentium	k1gNnSc2	Frumentium
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
udržovala	udržovat	k5eAaImAgFnS	udržovat
kontakty	kontakt	k1gInPc4	kontakt
s	s	k7c7	s
antickým	antický	k2eAgInSc7d1	antický
světem	svět	k1gInSc7	svět
<g/>
,	,	kIx,	,
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
se	se	k3xPyFc4	se
architektura	architektura	k1gFnSc1	architektura
<g/>
,	,	kIx,	,
umění	umění	k1gNnSc1	umění
i	i	k8xC	i
řemeslná	řemeslný	k2eAgFnSc1d1	řemeslná
výroba	výroba	k1gFnSc1	výroba
<g/>
,	,	kIx,	,
razily	razit	k5eAaImAgInP	razit
se	se	k3xPyFc4	se
také	také	k9	také
mince	mince	k1gFnSc1	mince
<g/>
.	.	kIx.	.
</s>
<s>
Vrcholu	vrchol	k1gInSc2	vrchol
svého	svůj	k3xOyFgInSc2	svůj
vývoje	vývoj	k1gInSc2	vývoj
se	se	k3xPyFc4	se
říše	říše	k1gFnSc1	říše
dočkala	dočkat	k5eAaPmAgFnS	dočkat
v	v	k7c6	v
první	první	k4xOgFnSc6	první
polovině	polovina	k1gFnSc6	polovina
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
císaře	císař	k1gMnSc2	císař
Kaleba	Kaleb	k1gMnSc2	Kaleb
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
velkou	velká	k1gFnSc4	velká
část	část	k1gFnSc1	část
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
strávil	strávit	k5eAaPmAgMnS	strávit
boji	boj	k1gInSc6	boj
s	s	k7c7	s
židovskými	židovský	k2eAgMnPc7d1	židovský
povstalci	povstalec	k1gMnPc7	povstalec
v	v	k7c6	v
arabské	arabský	k2eAgFnSc6d1	arabská
části	část	k1gFnSc6	část
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
nastal	nastat	k5eAaPmAgInS	nastat
úpadek	úpadek	k1gInSc1	úpadek
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
říše	říše	k1gFnSc1	říše
o	o	k7c4	o
arabská	arabský	k2eAgNnPc4d1	arabské
území	území	k1gNnPc4	území
přišla	přijít	k5eAaPmAgFnS	přijít
a	a	k8xC	a
oblast	oblast	k1gFnSc1	oblast
v	v	k7c6	v
druhé	druhý	k4xOgFnSc6	druhý
polovině	polovina	k1gFnSc6	polovina
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
sásánovská	sásánovský	k2eAgFnSc1d1	sásánovská
Persie	Persie	k1gFnSc1	Persie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
aksumské	aksumský	k2eAgFnSc6d1	aksumská
říši	říš	k1gFnSc6	říš
se	se	k3xPyFc4	se
používal	používat	k5eAaImAgInS	používat
zejména	zejména	k9	zejména
jazyk	jazyk	k1gInSc1	jazyk
ge	ge	k?	ge
<g/>
'	'	kIx"	'
<g/>
ez	ez	k?	ez
a	a	k8xC	a
v	v	k7c6	v
obchodních	obchodní	k2eAgInPc6d1	obchodní
stycích	styk	k1gInPc6	styk
se	s	k7c7	s
zahraničím	zahraničí	k1gNnSc7	zahraničí
se	se	k3xPyFc4	se
používala	používat	k5eAaImAgFnS	používat
i	i	k9	i
řečtina	řečtina	k1gFnSc1	řečtina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Období	období	k1gNnSc1	období
izolace	izolace	k1gFnSc2	izolace
a	a	k8xC	a
bojů	boj	k1gInPc2	boj
s	s	k7c7	s
muslimy	muslim	k1gMnPc7	muslim
===	===	k?	===
</s>
</p>
<p>
<s>
Kvůli	kvůli	k7c3	kvůli
územním	územní	k2eAgFnPc3d1	územní
ztrátám	ztráta	k1gFnPc3	ztráta
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
také	také	k9	také
expanzi	expanze	k1gFnSc3	expanze
muslimů	muslim	k1gMnPc2	muslim
se	se	k3xPyFc4	se
aksumská	aksumský	k2eAgFnSc1d1	aksumská
říše	říše	k1gFnSc1	říše
dostala	dostat	k5eAaPmAgFnS	dostat
do	do	k7c2	do
izolace	izolace	k1gFnSc2	izolace
<g/>
.	.	kIx.	.
</s>
<s>
Upadl	upadnout	k5eAaPmAgInS	upadnout
obchod	obchod	k1gInSc1	obchod
a	a	k8xC	a
řemesla	řemeslo	k1gNnPc1	řemeslo
a	a	k8xC	a
namísto	namísto	k7c2	namísto
peněz	peníze	k1gInPc2	peníze
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
znovu	znovu	k6eAd1	znovu
používat	používat	k5eAaImF	používat
směnný	směnný	k2eAgInSc4d1	směnný
systém	systém	k1gInSc4	systém
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
až	až	k9	až
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
rozsáhlý	rozsáhlý	k2eAgInSc1d1	rozsáhlý
úpadek	úpadek	k1gInSc1	úpadek
vyústil	vyústit	k5eAaPmAgInS	vyústit
v	v	k7c6	v
povstání	povstání	k1gNnSc6	povstání
židovského	židovský	k2eAgInSc2d1	židovský
kmene	kmen	k1gInSc2	kmen
Falašů	Falaš	k1gMnPc2	Falaš
vedeného	vedený	k2eAgInSc2d1	vedený
královnou	královna	k1gFnSc7	královna
Yodit	Yodit	k1gInSc1	Yodit
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jeho	jeho	k3xOp3gNnSc6	jeho
potlačení	potlačení	k1gNnSc6	potlačení
se	se	k3xPyFc4	se
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
v	v	k7c6	v
regionu	region	k1gInSc6	region
dostala	dostat	k5eAaPmAgFnS	dostat
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
dynastie	dynastie	k1gFnSc2	dynastie
Zagwe	Zagw	k1gFnSc2	Zagw
<g/>
,	,	kIx,	,
za	za	k7c2	za
níž	jenž	k3xRgFnSc2	jenž
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
vrcholu	vrchol	k1gInSc6	vrchol
klášterní	klášterní	k2eAgFnSc1d1	klášterní
architektura	architektura	k1gFnSc1	architektura
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
zaniklé	zaniklý	k2eAgFnSc6d1	zaniklá
aksumské	aksumský	k2eAgFnSc6d1	aksumská
říši	říš	k1gFnSc6	říš
převzali	převzít	k5eAaPmAgMnP	převzít
tito	tento	k3xDgMnPc1	tento
noví	nový	k2eAgMnPc1d1	nový
vládci	vládce	k1gMnPc1	vládce
křesťanství	křesťanství	k1gNnSc2	křesťanství
<g/>
,	,	kIx,	,
jazyk	jazyk	k1gInSc1	jazyk
ge	ge	k?	ge
<g/>
'	'	kIx"	'
<g/>
ez	ez	k?	ez
a	a	k8xC	a
státotvornou	státotvorný	k2eAgFnSc4d1	státotvorná
tradici	tradice	k1gFnSc4	tradice
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1270	[number]	k4	1270
se	se	k3xPyFc4	se
zagewská	zagewský	k2eAgFnSc1d1	zagewský
dynastie	dynastie	k1gFnSc1	dynastie
dobrovolně	dobrovolně	k6eAd1	dobrovolně
vzdala	vzdát	k5eAaPmAgFnS	vzdát
moci	moct	k5eAaImF	moct
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
původní	původní	k2eAgFnSc2d1	původní
dynastie	dynastie	k1gFnSc2	dynastie
Šalomounů	Šalomoun	k1gMnPc2	Šalomoun
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
přijala	přijmout	k5eAaPmAgFnS	přijmout
titul	titul	k1gInSc4	titul
císařů	císař	k1gMnPc2	císař
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
trval	trvat	k5eAaImAgInS	trvat
rozkvět	rozkvět	k1gInSc1	rozkvět
a	a	k8xC	a
upevnilo	upevnit	k5eAaPmAgNnS	upevnit
se	se	k3xPyFc4	se
i	i	k9	i
feudální	feudální	k2eAgNnSc1d1	feudální
zřízení	zřízení	k1gNnSc1	zřízení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
staletích	staletí	k1gNnPc6	staletí
se	se	k3xPyFc4	se
množily	množit	k5eAaImAgInP	množit
ozbrojené	ozbrojený	k2eAgInPc1d1	ozbrojený
střety	střet	k1gInPc1	střet
zejména	zejména	k9	zejména
s	s	k7c7	s
muslimy	muslim	k1gMnPc7	muslim
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Afrického	africký	k2eAgInSc2d1	africký
rohu	roh	k1gInSc2	roh
<g/>
,	,	kIx,	,
předně	předně	k6eAd1	předně
s	s	k7c7	s
Adalským	Adalský	k2eAgInSc7d1	Adalský
sultanátem	sultanát	k1gInSc7	sultanát
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
dílčí	dílčí	k2eAgInPc4d1	dílčí
etiopské	etiopský	k2eAgInPc4d1	etiopský
úspěchy	úspěch	k1gInPc4	úspěch
moc	moc	k6eAd1	moc
muslimů	muslim	k1gMnPc2	muslim
v	v	k7c6	v
regionu	region	k1gInSc6	region
nadále	nadále	k6eAd1	nadále
sílila	sílit	k5eAaImAgFnS	sílit
<g/>
.	.	kIx.	.
</s>
<s>
Kritická	kritický	k2eAgFnSc1d1	kritická
situace	situace	k1gFnSc1	situace
nastala	nastat	k5eAaPmAgFnS	nastat
roku	rok	k1gInSc2	rok
1529	[number]	k4	1529
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
muslimský	muslimský	k2eAgMnSc1d1	muslimský
vládce	vládce	k1gMnSc1	vládce
Amhad	Amhad	k1gInSc1	Amhad
Graňň	Graňň	k1gFnSc4	Graňň
zaútočil	zaútočit	k5eAaPmAgInS	zaútočit
na	na	k7c4	na
etiopské	etiopský	k2eAgNnSc4d1	etiopské
území	území	k1gNnSc4	území
a	a	k8xC	a
téměř	téměř	k6eAd1	téměř
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
podařilo	podařit	k5eAaPmAgNnS	podařit
etiopský	etiopský	k2eAgInSc4d1	etiopský
stát	stát	k1gInSc4	stát
vyvrátit	vyvrátit	k5eAaPmF	vyvrátit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1541	[number]	k4	1541
však	však	k9	však
připluli	připlout	k5eAaPmAgMnP	připlout
Etiopanům	Etiopan	k1gInPc3	Etiopan
na	na	k7c4	na
pomoc	pomoc	k1gFnSc4	pomoc
Portugalci	Portugalec	k1gMnPc1	Portugalec
<g/>
,	,	kIx,	,
s	s	k7c7	s
jejichž	jejichž	k3xOyRp3gFnSc7	jejichž
pomocí	pomoc	k1gFnSc7	pomoc
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
muslimy	muslim	k1gMnPc4	muslim
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
jezera	jezero	k1gNnSc2	jezero
Tana	tanout	k5eAaImSgInS	tanout
porazit	porazit	k5eAaPmF	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
ve	v	k7c6	v
stejné	stejný	k2eAgFnSc6d1	stejná
době	doba	k1gFnSc6	doba
začali	začít	k5eAaPmAgMnP	začít
z	z	k7c2	z
jihu	jih	k1gInSc2	jih
pronikat	pronikat	k5eAaImF	pronikat
do	do	k7c2	do
Etiopie	Etiopie	k1gFnSc2	Etiopie
Oromové	Oromová	k1gFnSc2	Oromová
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
poničené	poničený	k2eAgFnSc6d1	poničená
válkou	válka	k1gFnSc7	válka
usazovali	usazovat	k5eAaImAgMnP	usazovat
<g/>
,	,	kIx,	,
přejímali	přejímat	k5eAaImAgMnP	přejímat
zdejší	zdejší	k2eAgInSc4d1	zdejší
způsob	způsob	k1gInSc4	způsob
života	život	k1gInSc2	život
a	a	k8xC	a
náboženství	náboženství	k1gNnSc2	náboženství
a	a	k8xC	a
navždy	navždy	k6eAd1	navždy
tak	tak	k6eAd1	tak
změnili	změnit	k5eAaPmAgMnP	změnit
etnické	etnický	k2eAgNnSc4d1	etnické
složení	složení	k1gNnSc4	složení
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
Oromů	Orom	k1gMnPc2	Orom
poté	poté	k6eAd1	poté
zastávalo	zastávat	k5eAaImAgNnS	zastávat
významné	významný	k2eAgFnPc4d1	významná
pozice	pozice	k1gFnPc4	pozice
ve	v	k7c6	v
státní	státní	k2eAgFnSc6d1	státní
správě	správa	k1gFnSc6	správa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Nové	Nové	k2eAgInPc1d1	Nové
kontakty	kontakt	k1gInPc1	kontakt
se	s	k7c7	s
zahraničím	zahraničí	k1gNnSc7	zahraničí
a	a	k8xC	a
boj	boj	k1gInSc1	boj
s	s	k7c7	s
kolonialismem	kolonialismus	k1gInSc7	kolonialismus
===	===	k?	===
</s>
</p>
<p>
<s>
Nový	nový	k2eAgInSc4d1	nový
problém	problém	k1gInSc4	problém
představovali	představovat	k5eAaImAgMnP	představovat
Portugalci	Portugalec	k1gMnPc1	Portugalec
a	a	k8xC	a
zejména	zejména	k9	zejména
jezuitští	jezuitský	k2eAgMnPc1d1	jezuitský
misionáři	misionář	k1gMnPc1	misionář
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
sem	sem	k6eAd1	sem
s	s	k7c7	s
nimi	on	k3xPp3gInPc7	on
přišli	přijít	k5eAaPmAgMnP	přijít
<g/>
.	.	kIx.	.
</s>
<s>
Etiopští	etiopský	k2eAgMnPc1d1	etiopský
císaři	císař	k1gMnPc1	císař
se	se	k3xPyFc4	se
ocitli	ocitnout	k5eAaPmAgMnP	ocitnout
pod	pod	k7c7	pod
silným	silný	k2eAgInSc7d1	silný
tlakem	tlak	k1gInSc7	tlak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
přestoupili	přestoupit	k5eAaPmAgMnP	přestoupit
na	na	k7c4	na
katolickou	katolický	k2eAgFnSc4d1	katolická
víru	víra	k1gFnSc4	víra
<g/>
,	,	kIx,	,
a	a	k8xC	a
přestože	přestože	k8xS	přestože
se	se	k3xPyFc4	se
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
pokusili	pokusit	k5eAaPmAgMnP	pokusit
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Ze	z	k7c2	z
Dingil	Dingila	k1gFnPc2	Dingila
či	či	k8xC	či
Susneos	Susneosa	k1gFnPc2	Susneosa
<g/>
,	,	kIx,	,
lidový	lidový	k2eAgInSc1d1	lidový
odpor	odpor	k1gInSc1	odpor
byl	být	k5eAaImAgInS	být
tak	tak	k6eAd1	tak
silný	silný	k2eAgInSc1d1	silný
<g/>
,	,	kIx,	,
že	že	k8xS	že
oba	dva	k4xCgMnPc1	dva
později	pozdě	k6eAd2	pozdě
přišli	přijít	k5eAaPmAgMnP	přijít
o	o	k7c4	o
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Susneův	Susneův	k2eAgMnSc1d1	Susneův
syn	syn	k1gMnSc1	syn
Fasiladas	Fasiladas	k1gMnSc1	Fasiladas
posléze	posléze	k6eAd1	posléze
misionáře	misionář	k1gMnPc4	misionář
ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
17	[number]	k4	17
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
vyhnal	vyhnat	k5eAaPmAgMnS	vyhnat
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
Fasilova	Fasilův	k2eAgMnSc4d1	Fasilův
nástupce	nástupce	k1gMnSc4	nástupce
Ijasua	Ijasu	k1gInSc2	Ijasu
I.	I.	kA	I.
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
obnovení	obnovení	k1gNnSc3	obnovení
silného	silný	k2eAgNnSc2d1	silné
postavení	postavení	k1gNnSc2	postavení
Etiopie	Etiopie	k1gFnSc2	Etiopie
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
1636	[number]	k4	1636
bylo	být	k5eAaImAgNnS	být
také	také	k6eAd1	také
založeno	založen	k2eAgNnSc1d1	založeno
nové	nový	k2eAgNnSc1d1	nové
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
jménem	jméno	k1gNnSc7	jméno
Gondar	Gondar	k1gMnSc1	Gondar
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
tohoto	tento	k3xDgMnSc2	tento
císaře	císař	k1gMnSc2	císař
ale	ale	k8xC	ale
nastal	nastat	k5eAaPmAgInS	nastat
značný	značný	k2eAgInSc4d1	značný
úpadek	úpadek	k1gInSc4	úpadek
císařské	císařský	k2eAgFnSc2d1	císařská
moci	moc	k1gFnSc2	moc
a	a	k8xC	a
na	na	k7c4	na
dění	dění	k1gNnSc4	dění
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
měli	mít	k5eAaImAgMnP	mít
stále	stále	k6eAd1	stále
větší	veliký	k2eAgInSc4d2	veliký
vliv	vliv	k1gInSc4	vliv
feudální	feudální	k2eAgMnPc1d1	feudální
šlechtici	šlechtic	k1gMnPc1	šlechtic
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
vyvrcholilo	vyvrcholit	k5eAaPmAgNnS	vyvrcholit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1784	[number]	k4	1784
až	až	k9	až
1855	[number]	k4	1855
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
etiopští	etiopský	k2eAgMnPc1d1	etiopský
vládci	vládce	k1gMnPc1	vládce
panovali	panovat	k5eAaImAgMnP	panovat
pouze	pouze	k6eAd1	pouze
nominálně	nominálně	k6eAd1	nominálně
a	a	k8xC	a
namísto	namísto	k7c2	namísto
nich	on	k3xPp3gInPc2	on
rozhodovali	rozhodovat	k5eAaImAgMnP	rozhodovat
aristokraté	aristokrat	k1gMnPc1	aristokrat
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
historie	historie	k1gFnSc2	historie
se	se	k3xPyFc4	se
zapsalo	zapsat	k5eAaPmAgNnS	zapsat
jako	jako	k9	jako
Zemene	Zemen	k1gInSc5	Zemen
Mes	Mes	k1gMnPc6	Mes
<g/>
'	'	kIx"	'
<g/>
afint	afint	k1gInSc1	afint
<g/>
,	,	kIx,	,
doba	doba	k1gFnSc1	doba
princů	princ	k1gMnPc2	princ
<g/>
.	.	kIx.	.
<g/>
Úpadek	úpadek	k1gInSc1	úpadek
panovnické	panovnický	k2eAgFnSc2d1	panovnická
moci	moc	k1gFnSc2	moc
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
zastavit	zastavit	k5eAaPmF	zastavit
císař	císař	k1gMnSc1	císař
Tewedros	Tewedrosa	k1gFnPc2	Tewedrosa
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
panující	panující	k2eAgFnSc2d1	panující
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1855	[number]	k4	1855
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
krutý	krutý	k2eAgInSc1d1	krutý
a	a	k8xC	a
úspěšný	úspěšný	k2eAgMnSc1d1	úspěšný
panovník	panovník	k1gMnSc1	panovník
byl	být	k5eAaImAgMnS	být
talentovaný	talentovaný	k2eAgMnSc1d1	talentovaný
vojevůdce	vojevůdce	k1gMnSc1	vojevůdce
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
dospěl	dochvít	k5eAaPmAgMnS	dochvít
k	k	k7c3	k
mnoha	mnoho	k4c3	mnoho
vojenským	vojenský	k2eAgInPc3d1	vojenský
úspěchům	úspěch	k1gInPc3	úspěch
a	a	k8xC	a
pokoušel	pokoušet	k5eAaImAgMnS	pokoušet
se	se	k3xPyFc4	se
o	o	k7c4	o
modernizaci	modernizace	k1gFnSc4	modernizace
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
nepodařilo	podařit	k5eNaPmAgNnS	podařit
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
získat	získat	k5eAaPmF	získat
nutnou	nutný	k2eAgFnSc4d1	nutná
podporu	podpora	k1gFnSc4	podpora
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
mocností	mocnost	k1gFnPc2	mocnost
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
konfliktu	konflikt	k1gInSc3	konflikt
s	s	k7c7	s
britským	britský	k2eAgMnSc7d1	britský
konzulem	konzul	k1gMnSc7	konzul
Cameronem	Cameron	k1gMnSc7	Cameron
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gMnSc1	jehož
nechal	nechat	k5eAaPmAgMnS	nechat
paranoidní	paranoidní	k2eAgMnSc1d1	paranoidní
Tewedros	Tewedrosa	k1gFnPc2	Tewedrosa
uvěznit	uvěznit	k5eAaPmF	uvěznit
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
znepřátelil	znepřátelit	k5eAaPmAgMnS	znepřátelit
Velkou	velký	k2eAgFnSc4d1	velká
Británii	Británie	k1gFnSc4	Británie
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
nakonec	nakonec	k6eAd1	nakonec
k	k	k7c3	k
intervenci	intervence	k1gFnSc3	intervence
britských	britský	k2eAgNnPc2d1	Britské
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
způsobila	způsobit	k5eAaPmAgFnS	způsobit
císařovo	císařův	k2eAgNnSc4d1	císařovo
sesazení	sesazení	k1gNnSc4	sesazení
<g/>
,	,	kIx,	,
po	po	k7c6	po
němž	jenž	k3xRgInSc6	jenž
spáchal	spáchat	k5eAaPmAgInS	spáchat
sebevraždu	sebevražda	k1gFnSc4	sebevražda
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
nový	nový	k2eAgInSc4d1	nový
pokus	pokus	k1gInSc4	pokus
o	o	k7c6	o
centralizaci	centralizace	k1gFnSc6	centralizace
moci	moc	k1gFnSc2	moc
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
Johannes	Johannes	k1gMnSc1	Johannes
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
roku	rok	k1gInSc2	rok
1871	[number]	k4	1871
<g/>
.	.	kIx.	.
</s>
<s>
Úspěšně	úspěšně	k6eAd1	úspěšně
se	se	k3xPyFc4	se
ubránil	ubránit	k5eAaPmAgInS	ubránit
proti	proti	k7c3	proti
egyptskému	egyptský	k2eAgInSc3d1	egyptský
vpádu	vpád	k1gInSc3	vpád
a	a	k8xC	a
bojoval	bojovat	k5eAaImAgMnS	bojovat
na	na	k7c4	na
popud	popud	k1gInSc4	popud
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
také	také	k9	také
se	s	k7c7	s
súdánskými	súdánský	k2eAgMnPc7d1	súdánský
mahdisty	mahdista	k1gMnPc7	mahdista
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgNnPc7	jenž
však	však	k9	však
byl	být	k5eAaImAgMnS	být
poražen	porazit	k5eAaPmNgMnS	porazit
<g/>
.	.	kIx.	.
</s>
<s>
Dostal	dostat	k5eAaPmAgMnS	dostat
se	se	k3xPyFc4	se
též	též	k9	též
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
s	s	k7c7	s
Itálií	Itálie	k1gFnSc7	Itálie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
si	se	k3xPyFc3	se
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Eritreje	Eritrea	k1gFnSc2	Eritrea
vytvářela	vytvářet	k5eAaImAgFnS	vytvářet
zázemí	zázemí	k1gNnSc4	zázemí
pro	pro	k7c4	pro
další	další	k2eAgInSc4d1	další
postup	postup	k1gInSc4	postup
v	v	k7c6	v
regionu	region	k1gInSc6	region
<g/>
.	.	kIx.	.
</s>
<s>
Italy	Ital	k1gMnPc4	Ital
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
zastavit	zastavit	k5eAaPmF	zastavit
až	až	k9	až
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
dalšího	další	k2eAgMnSc2d1	další
císaře	císař	k1gMnSc2	císař
<g/>
,	,	kIx,	,
Menelika	Menelik	k1gMnSc2	Menelik
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
rozdrtil	rozdrtit	k5eAaPmAgInS	rozdrtit
italská	italský	k2eAgNnPc4d1	italské
vojska	vojsko	k1gNnPc4	vojsko
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Adwy	Adwa	k1gFnSc2	Adwa
(	(	kIx(	(
<g/>
Aduy	Adua	k1gFnSc2	Adua
<g/>
)	)	kIx)	)
roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
proběhla	proběhnout	k5eAaPmAgFnS	proběhnout
radikální	radikální	k2eAgFnSc1d1	radikální
expanze	expanze	k1gFnSc1	expanze
na	na	k7c4	na
jih	jih	k1gInSc4	jih
a	a	k8xC	a
jihovýchod	jihovýchod	k1gInSc4	jihovýchod
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
posílení	posílení	k1gNnSc3	posílení
státních	státní	k2eAgFnPc2d1	státní
financí	finance	k1gFnPc2	finance
a	a	k8xC	a
obranyschopnosti	obranyschopnost	k1gFnSc2	obranyschopnost
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Začala	začít	k5eAaPmAgFnS	začít
se	se	k3xPyFc4	se
stavět	stavět	k5eAaImF	stavět
první	první	k4xOgFnPc1	první
železnice	železnice	k1gFnPc1	železnice
mezi	mezi	k7c7	mezi
Addis	Addis	k1gFnSc7	Addis
Abebou	Abeba	k1gFnSc7	Abeba
<g/>
,	,	kIx,	,
novým	nový	k2eAgNnSc7d1	nové
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
<g/>
,	,	kIx,	,
a	a	k8xC	a
tehdy	tehdy	k6eAd1	tehdy
francouzským	francouzský	k2eAgNnSc7d1	francouzské
Džíbútí	Džíbúť	k1gFnSc7	Džíbúť
<g/>
,	,	kIx,	,
budovaly	budovat	k5eAaImAgFnP	budovat
se	se	k3xPyFc4	se
též	též	k9	též
nové	nový	k2eAgInPc1d1	nový
mosty	most	k1gInPc1	most
a	a	k8xC	a
silnice	silnice	k1gFnPc1	silnice
<g/>
.	.	kIx.	.
</s>
<s>
Menelik	Menelik	k1gMnSc1	Menelik
vládl	vládnout	k5eAaImAgMnS	vládnout
až	až	k6eAd1	až
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
<g/>
,	,	kIx,	,
po	po	k7c6	po
jeho	jeho	k3xOp3gFnSc6	jeho
smrti	smrt	k1gFnSc6	smrt
se	se	k3xPyFc4	se
dostal	dostat	k5eAaPmAgMnS	dostat
k	k	k7c3	k
moci	moc	k1gFnSc3	moc
Menelikův	Menelikův	k2eAgMnSc1d1	Menelikův
vnuk	vnuk	k1gMnSc1	vnuk
Ijasu	Ijas	k1gInSc2	Ijas
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgInS	být
svržen	svrhnout	k5eAaPmNgMnS	svrhnout
a	a	k8xC	a
moci	moc	k1gFnSc3	moc
se	se	k3xPyFc4	se
chopil	chopit	k5eAaPmAgMnS	chopit
šlechtic	šlechtic	k1gMnSc1	šlechtic
<g/>
,	,	kIx,	,
neboli	neboli	k8xC	neboli
ras	rasa	k1gFnPc2	rasa
<g/>
,	,	kIx,	,
Tafari	Tafari	k1gNnSc1	Tafari
Makonen	Makonna	k1gFnPc2	Makonna
<g/>
,	,	kIx,	,
nejdříve	dříve	k6eAd3	dříve
jako	jako	k9	jako
regent	regent	k1gMnSc1	regent
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
jako	jako	k9	jako
svrchovaný	svrchovaný	k2eAgMnSc1d1	svrchovaný
panovník	panovník	k1gMnSc1	panovník
Haile	Haile	k1gNnSc2	Haile
Selasie	Selasie	k1gFnSc2	Selasie
I.	I.	kA	I.
</s>
</p>
<p>
<s>
===	===	k?	===
Moderní	moderní	k2eAgFnPc1d1	moderní
dějiny	dějiny	k1gFnPc1	dějiny
===	===	k?	===
</s>
</p>
<p>
<s>
Haile	Haile	k1gFnSc1	Haile
Selassie	Selassie	k1gFnSc2	Selassie
I.	I.	kA	I.
se	se	k3xPyFc4	se
pokoušel	pokoušet	k5eAaImAgInS	pokoušet
o	o	k7c6	o
soustředění	soustředění	k1gNnSc6	soustředění
moci	moc	k1gFnSc2	moc
ve	v	k7c6	v
svých	svůj	k3xOyFgFnPc6	svůj
rukou	ruka	k1gFnPc6	ruka
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
dokázal	dokázat	k5eAaPmAgInS	dokázat
provádět	provádět	k5eAaImF	provádět
další	další	k2eAgFnPc4d1	další
reformy	reforma	k1gFnPc4	reforma
<g/>
,	,	kIx,	,
jimž	jenž	k3xRgFnPc3	jenž
se	se	k3xPyFc4	se
mnohé	mnohý	k2eAgFnPc1d1	mnohá
složky	složka	k1gFnPc1	složka
společnosti	společnost	k1gFnSc2	společnost
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
šlechta	šlechta	k1gFnSc1	šlechta
a	a	k8xC	a
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
urputně	urputně	k6eAd1	urputně
bránily	bránit	k5eAaImAgFnP	bránit
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc4	jeho
snahu	snaha	k1gFnSc4	snaha
přerušila	přerušit	k5eAaPmAgFnS	přerušit
etiopsko-italská	etiopskotalský	k2eAgFnSc1d1	etiopsko-italský
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
v	v	k7c6	v
jejímž	jejíž	k3xOyRp3gInSc6	jejíž
důsledku	důsledek	k1gInSc6	důsledek
byl	být	k5eAaImAgInS	být
roku	rok	k1gInSc2	rok
1936	[number]	k4	1936
císař	císař	k1gMnSc1	císař
nucen	nutit	k5eAaImNgMnS	nutit
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Etiopii	Etiopie	k1gFnSc3	Etiopie
dobyla	dobýt	k5eAaPmAgFnS	dobýt
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
i	i	k9	i
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
chemických	chemický	k2eAgFnPc2d1	chemická
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
a	a	k8xC	a
okupovala	okupovat	k5eAaBmAgFnS	okupovat
italská	italský	k2eAgFnSc1d1	italská
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
se	se	k3xPyFc4	se
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
Britů	Brit	k1gMnPc2	Brit
podařilo	podařit	k5eAaPmAgNnS	podařit
roku	rok	k1gInSc2	rok
1941	[number]	k4	1941
dobýt	dobýt	k5eAaPmF	dobýt
etiopské	etiopský	k2eAgNnSc4d1	etiopské
území	území	k1gNnSc4	území
nazpět	nazpět	k6eAd1	nazpět
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
se	se	k3xPyFc4	se
díky	díky	k7c3	díky
úspěšnému	úspěšný	k2eAgNnSc3d1	úspěšné
diplomatickému	diplomatický	k2eAgNnSc3d1	diplomatické
jednání	jednání	k1gNnSc3	jednání
zdařilo	zdařit	k5eAaPmAgNnS	zdařit
získat	získat	k5eAaPmF	získat
Haile	Haile	k1gNnSc1	Haile
Selassiemu	Selassiem	k1gInSc2	Selassiem
pod	pod	k7c4	pod
svou	svůj	k3xOyFgFnSc4	svůj
kontrolu	kontrola	k1gFnSc4	kontrola
také	také	k9	také
původní	původní	k2eAgFnSc4d1	původní
italskou	italský	k2eAgFnSc4d1	italská
kolonii	kolonie	k1gFnSc4	kolonie
Eritreu	Eritrea	k1gFnSc4	Eritrea
<g/>
.	.	kIx.	.
</s>
<s>
Proti	proti	k7c3	proti
vládě	vláda	k1gFnSc3	vláda
císaře	císař	k1gMnSc2	císař
a	a	k8xC	a
pomalému	pomalý	k2eAgNnSc3d1	pomalé
tempu	tempo	k1gNnSc3	tempo
jeho	jeho	k3xOp3gFnPc2	jeho
reforem	reforma	k1gFnPc2	reforma
postupně	postupně	k6eAd1	postupně
rostl	růst	k5eAaImAgInS	růst
odpor	odpor	k1gInSc1	odpor
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
vyvrcholil	vyvrcholit	k5eAaPmAgInS	vyvrcholit
roku	rok	k1gInSc2	rok
1960	[number]	k4	1960
pokusem	pokus	k1gInSc7	pokus
o	o	k7c4	o
puč	puč	k1gInSc4	puč
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
byl	být	k5eAaImAgInS	být
pokus	pokus	k1gInSc1	pokus
o	o	k7c4	o
převrat	převrat	k1gInSc4	převrat
potlačen	potlačen	k2eAgInSc4d1	potlačen
<g/>
,	,	kIx,	,
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
nepřestávalo	přestávat	k5eNaImAgNnS	přestávat
působit	působit	k5eAaImF	působit
sociální	sociální	k2eAgNnSc4d1	sociální
pnutí	pnutí	k1gNnSc4	pnutí
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
si	se	k3xPyFc3	se
postupně	postupně	k6eAd1	postupně
znepřátelil	znepřátelit	k5eAaPmAgMnS	znepřátelit
reformám	reforma	k1gFnPc3	reforma
nakloněnou	nakloněný	k2eAgFnSc4d1	nakloněná
střední	střední	k2eAgFnSc4d1	střední
vrstvu	vrstva	k1gFnSc4	vrstva
<g/>
,	,	kIx,	,
studentstvo	studentstvo	k1gNnSc4	studentstvo
i	i	k8xC	i
nižší	nízký	k2eAgMnPc4d2	nižší
armádní	armádní	k2eAgMnPc4d1	armádní
důstojníky	důstojník	k1gMnPc4	důstojník
<g/>
.	.	kIx.	.
</s>
<s>
Právě	právě	k9	právě
tito	tento	k3xDgMnPc1	tento
příslušníci	příslušník	k1gMnPc1	příslušník
armády	armáda	k1gFnSc2	armáda
vinili	vinit	k5eAaImAgMnP	vinit
císaře	císař	k1gMnSc4	císař
a	a	k8xC	a
přežívající	přežívající	k2eAgInSc4d1	přežívající
feudální	feudální	k2eAgInSc4d1	feudální
systém	systém	k1gInSc4	systém
ze	z	k7c2	z
společenských	společenský	k2eAgInPc2d1	společenský
problémů	problém	k1gInPc2	problém
a	a	k8xC	a
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
vlastní	vlastní	k2eAgFnSc2d1	vlastní
chudoby	chudoba	k1gFnSc2	chudoba
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
Šalomounovců	Šalomounovec	k1gMnPc2	Šalomounovec
postupně	postupně	k6eAd1	postupně
ztratila	ztratit	k5eAaPmAgFnS	ztratit
důvěru	důvěra	k1gFnSc4	důvěra
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
se	se	k3xPyFc4	se
projevilo	projevit	k5eAaPmAgNnS	projevit
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgMnS	být
etiopský	etiopský	k2eAgMnSc1d1	etiopský
císař	císař	k1gMnSc1	císař
svržen	svrhnout	k5eAaPmNgMnS	svrhnout
a	a	k8xC	a
monarchie	monarchie	k1gFnSc1	monarchie
zrušena	zrušit	k5eAaPmNgFnS	zrušit
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
následujících	následující	k2eAgNnPc6d1	následující
letech	léto	k1gNnPc6	léto
vládl	vládnout	k5eAaImAgInS	vládnout
v	v	k7c4	v
zemi	zem	k1gFnSc4	zem
takzvaný	takzvaný	k2eAgInSc1d1	takzvaný
derg	derg	k1gInSc1	derg
(	(	kIx(	(
<g/>
rada	rada	k1gFnSc1	rada
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gInSc6	jehož
čele	čelo	k1gNnSc6	čelo
stanul	stanout	k5eAaPmAgMnS	stanout
armádní	armádní	k2eAgMnSc1d1	armádní
důstojník	důstojník	k1gMnSc1	důstojník
Mengistu	Mengist	k1gInSc2	Mengist
Haile	Haile	k1gFnSc2	Haile
Mariam	Mariam	k1gInSc1	Mariam
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
inspirován	inspirovat	k5eAaBmNgInS	inspirovat
několika	několik	k4yIc2	několik
konkurenčními	konkurenční	k2eAgFnPc7d1	konkurenční
marxistickými	marxistický	k2eAgFnPc7d1	marxistická
stranami	strana	k1gFnPc7	strana
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
ze	z	k7c2	z
studentských	studentský	k2eAgNnPc2d1	studentské
hnutí	hnutí	k1gNnPc2	hnutí
<g/>
,	,	kIx,	,
zavedl	zavést	k5eAaPmAgInS	zavést
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
pozemkovou	pozemkový	k2eAgFnSc4d1	pozemková
reformu	reforma	k1gFnSc4	reforma
<g/>
,	,	kIx,	,
zestátnění	zestátnění	k1gNnSc4	zestátnění
podniků	podnik	k1gInPc2	podnik
a	a	k8xC	a
násilný	násilný	k2eAgInSc1d1	násilný
teror	teror	k1gInSc1	teror
proti	proti	k7c3	proti
domnělým	domnělý	k2eAgFnPc3d1	domnělá
i	i	k8xC	i
skutečným	skutečný	k2eAgFnPc3d1	skutečná
opozičním	opoziční	k2eAgFnPc3d1	opoziční
silám	síla	k1gFnPc3	síla
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
vedl	vést	k5eAaImAgMnS	vést
ke	k	k7c3	k
statisícům	statisíce	k1gInPc3	statisíce
zavražděných	zavražděný	k2eAgFnPc2d1	zavražděná
či	či	k8xC	či
umučených	umučený	k2eAgMnPc2d1	umučený
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
k	k	k7c3	k
nuceným	nucený	k2eAgInPc3d1	nucený
přesunům	přesun	k1gInPc3	přesun
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
sucha	sucho	k1gNnSc2	sucho
v	v	k7c6	v
80	[number]	k4	80
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
ekonomika	ekonomika	k1gFnSc1	ekonomika
státu	stát	k1gInSc2	stát
prakticky	prakticky	k6eAd1	prakticky
zhroutila	zhroutit	k5eAaPmAgFnS	zhroutit
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
vyústilo	vyústit	k5eAaPmAgNnS	vyústit
v	v	k7c4	v
posílení	posílení	k1gNnSc4	posílení
opozičních	opoziční	k2eAgFnPc2d1	opoziční
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
vedly	vést	k5eAaImAgFnP	vést
proti	proti	k7c3	proti
vládě	vláda	k1gFnSc3	vláda
neustálý	neustálý	k2eAgInSc4d1	neustálý
ozbrojený	ozbrojený	k2eAgInSc4d1	ozbrojený
konflikt	konflikt	k1gInSc4	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
situaci	situace	k1gFnSc3	situace
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
nepřispěla	přispět	k5eNaPmAgFnS	přispět
ani	ani	k8xC	ani
etiopsko-somálská	etiopskoomálský	k2eAgFnSc1d1	etiopsko-somálský
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1977	[number]	k4	1977
až	až	k9	až
1978	[number]	k4	1978
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc7	který
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
vyhrát	vyhrát	k5eAaPmF	vyhrát
díky	díky	k7c3	díky
podpoře	podpora	k1gFnSc3	podpora
Sovětského	sovětský	k2eAgInSc2d1	sovětský
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Sovětské	sovětský	k2eAgInPc1d1	sovětský
zájmy	zájem	k1gInPc1	zájem
a	a	k8xC	a
podpora	podpora	k1gFnSc1	podpora
režimu	režim	k1gInSc2	režim
v	v	k7c4	v
Etiopii	Etiopie	k1gFnSc4	Etiopie
byla	být	k5eAaImAgFnS	být
součástí	součást	k1gFnSc7	součást
velmocenského	velmocenský	k2eAgInSc2d1	velmocenský
zápasu	zápas	k1gInSc2	zápas
studené	studený	k2eAgFnSc2d1	studená
války	válka	k1gFnSc2	válka
v	v	k7c6	v
zemích	zem	k1gFnPc6	zem
tzv.	tzv.	kA	tzv.
třetího	třetí	k4xOgInSc2	třetí
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
byl	být	k5eAaImAgInS	být
totiž	totiž	k9	totiž
původně	původně	k6eAd1	původně
Sovětský	sovětský	k2eAgInSc1d1	sovětský
svaz	svaz	k1gInSc1	svaz
spojencem	spojenec	k1gMnSc7	spojenec
Somálska	Somálsko	k1gNnSc2	Somálsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
držela	držet	k5eAaImAgFnS	držet
moc	moc	k6eAd1	moc
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
levicová	levicový	k2eAgFnSc1d1	levicová
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
sovětskou	sovětský	k2eAgFnSc4d1	sovětská
vládu	vláda	k1gFnSc4	vláda
bylo	být	k5eAaImAgNnS	být
výhodnější	výhodný	k2eAgNnSc1d2	výhodnější
podporovat	podporovat	k5eAaImF	podporovat
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
Etiopii	Etiopie	k1gFnSc4	Etiopie
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
kvůli	kvůli	k7c3	kvůli
jejímu	její	k3xOp3gNnSc3	její
většímu	veliký	k2eAgNnSc3d2	veliký
materiálnímu	materiální	k2eAgNnSc3d1	materiální
zajištění	zajištění	k1gNnSc3	zajištění
a	a	k8xC	a
ideologicky	ideologicky	k6eAd1	ideologicky
bližšímu	blízký	k2eAgInSc3d2	bližší
režimu	režim	k1gInSc3	režim
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
podpora	podpora	k1gFnSc1	podpora
východního	východní	k2eAgInSc2d1	východní
bloku	blok	k1gInSc2	blok
ale	ale	k8xC	ale
koncem	koncem	k7c2	koncem
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
skončila	skončit	k5eAaPmAgFnS	skončit
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
jeho	jeho	k3xOp3gInSc3	jeho
postupnému	postupný	k2eAgInSc3d1	postupný
úpadku	úpadek	k1gInSc3	úpadek
<g/>
.	.	kIx.	.
</s>
<s>
Mengistova	Mengistův	k2eAgFnSc1d1	Mengistův
vláda	vláda	k1gFnSc1	vláda
ztratila	ztratit	k5eAaPmAgFnS	ztratit
domácí	domácí	k2eAgFnSc7d1	domácí
i	i	k8xC	i
zahraniční	zahraniční	k2eAgFnSc4d1	zahraniční
podporu	podpora	k1gFnSc4	podpora
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
následně	následně	k6eAd1	následně
roku	rok	k1gInSc2	rok
1991	[number]	k4	1991
svržena	svržen	k2eAgFnSc1d1	svržena
a	a	k8xC	a
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
Eritrea	Eritrea	k1gFnSc1	Eritrea
<g/>
,	,	kIx,	,
dosud	dosud	k6eAd1	dosud
pod	pod	k7c7	pod
etiopskou	etiopský	k2eAgFnSc7d1	etiopská
nadvládou	nadvláda	k1gFnSc7	nadvláda
<g/>
,	,	kIx,	,
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
nové	nový	k2eAgFnSc2d1	nová
ústavy	ústava	k1gFnSc2	ústava
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1994	[number]	k4	1994
se	se	k3xPyFc4	se
Etiopie	Etiopie	k1gFnSc1	Etiopie
stala	stát	k5eAaPmAgFnS	stát
federativní	federativní	k2eAgFnSc7d1	federativní
republikou	republika	k1gFnSc7	republika
rozdělenou	rozdělený	k2eAgFnSc7d1	rozdělená
na	na	k7c6	na
etnickém	etnický	k2eAgInSc6d1	etnický
základě	základ	k1gInSc6	základ
<g/>
.	.	kIx.	.
</s>
<s>
Volby	volba	k1gFnPc1	volba
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
Etiopská	etiopský	k2eAgFnSc1d1	etiopská
lidová	lidový	k2eAgFnSc1d1	lidová
revoluční	revoluční	k2eAgFnSc1d1	revoluční
demokratická	demokratický	k2eAgFnSc1d1	demokratická
fronta	fronta	k1gFnSc1	fronta
(	(	kIx(	(
<g/>
EPRDF	EPRDF	kA	EPRDF
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
Etiopii	Etiopie	k1gFnSc4	Etiopie
vládne	vládnout	k5eAaImIp3nS	vládnout
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Této	tento	k3xDgFnSc3	tento
straně	strana	k1gFnSc3	strana
se	se	k3xPyFc4	se
během	během	k7c2	během
posledních	poslední	k2eAgNnPc2d1	poslední
dvaceti	dvacet	k4xCc2	dvacet
let	léto	k1gNnPc2	léto
podařilo	podařit	k5eAaPmAgNnS	podařit
v	v	k7c4	v
zemi	zem	k1gFnSc4	zem
vytvořit	vytvořit	k5eAaPmF	vytvořit
autoritářský	autoritářský	k2eAgInSc4d1	autoritářský
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
udržuje	udržovat	k5eAaImIp3nS	udržovat
s	s	k7c7	s
pomocí	pomoc	k1gFnSc7	pomoc
represí	represe	k1gFnPc2	represe
a	a	k8xC	a
nestandardního	standardní	k2eNgNnSc2d1	nestandardní
vedení	vedení	k1gNnSc2	vedení
voleb	volba	k1gFnPc2	volba
<g/>
.	.	kIx.	.
</s>
<s>
Premiérem	premiér	k1gMnSc7	premiér
a	a	k8xC	a
vůdcem	vůdce	k1gMnSc7	vůdce
EPRDF	EPRDF	kA	EPRDF
byl	být	k5eAaImAgInS	být
do	do	k7c2	do
své	svůj	k3xOyFgFnSc2	svůj
smrti	smrt	k1gFnSc2	smrt
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
Meles	Meles	k1gInSc4	Meles
Zenawi	Zenaw	k1gFnSc2	Zenaw
<g/>
.	.	kIx.	.
</s>
<s>
Etiopie	Etiopie	k1gFnSc1	Etiopie
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejchudších	chudý	k2eAgFnPc2d3	nejchudší
zemí	zem	k1gFnPc2	zem
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
pozitivnímu	pozitivní	k2eAgInSc3d1	pozitivní
vývoji	vývoj	k1gInSc3	vývoj
nepomohla	pomoct	k5eNaPmAgFnS	pomoct
také	také	k9	také
etiopsko-eritrejská	etiopskoritrejský	k2eAgFnSc1d1	etiopsko-eritrejský
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1998	[number]	k4	1998
až	až	k9	až
2001	[number]	k4	2001
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vedla	vést	k5eAaImAgFnS	vést
ke	k	k7c3	k
100	[number]	k4	100
000	[number]	k4	000
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Válka	válka	k1gFnSc1	válka
ekonomicky	ekonomicky	k6eAd1	ekonomicky
vysílila	vysílit	k5eAaPmAgFnS	vysílit
obě	dva	k4xCgFnPc4	dva
země	zem	k1gFnPc4	zem
<g/>
,	,	kIx,	,
většina	většina	k1gFnSc1	většina
rozvojových	rozvojový	k2eAgInPc2d1	rozvojový
projektů	projekt	k1gInPc2	projekt
byla	být	k5eAaImAgFnS	být
přerušena	přerušen	k2eAgFnSc1d1	přerušena
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
vývoj	vývoj	k1gInSc1	vývoj
vržen	vržen	k2eAgInSc1d1	vržen
o	o	k7c4	o
několik	několik	k4yIc4	několik
let	léto	k1gNnPc2	léto
nazpátek	nazpátek	k6eAd1	nazpátek
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2018	[number]	k4	2018
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
kmenové	kmenový	k2eAgNnSc1d1	kmenové
násilí	násilí	k1gNnSc1	násilí
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
ze	z	k7c2	z
svých	svůj	k3xOyFgInPc2	svůj
domovů	domov	k1gInPc2	domov
vyhnalo	vyhnat	k5eAaPmAgNnS	vyhnat
téměř	téměř	k6eAd1	téměř
milion	milion	k4xCgInSc1	milion
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Geografie	geografie	k1gFnSc2	geografie
==	==	k?	==
</s>
</p>
<p>
<s>
Etiopský	etiopský	k2eAgInSc1d1	etiopský
stát	stát	k1gInSc1	stát
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
ve	v	k7c6	v
východní	východní	k2eAgFnSc6d1	východní
Africe	Afrika	k1gFnSc6	Afrika
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zvané	zvaný	k2eAgFnSc2d1	zvaná
Africký	africký	k2eAgInSc4d1	africký
roh	roh	k1gInSc4	roh
<g/>
,	,	kIx,	,
poblíž	poblíž	k6eAd1	poblíž
jižní	jižní	k2eAgFnSc2d1	jižní
části	část	k1gFnSc2	část
Rudého	rudý	k2eAgNnSc2d1	Rudé
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
stát	stát	k1gInSc1	stát
vnitrozemský	vnitrozemský	k2eAgInSc1d1	vnitrozemský
<g/>
,	,	kIx,	,
o	o	k7c6	o
pobřeží	pobřeží	k1gNnSc6	pobřeží
přišel	přijít	k5eAaPmAgMnS	přijít
po	po	k7c6	po
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Eritreje	Eritrea	k1gFnSc2	Eritrea
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Celková	celkový	k2eAgFnSc1d1	celková
rozloha	rozloha	k1gFnSc1	rozloha
činí	činit	k5eAaImIp3nS	činit
1	[number]	k4	1
127	[number]	k4	127
127	[number]	k4	127
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Sousedí	sousedit	k5eAaImIp3nS	sousedit
se	s	k7c7	s
Súdánem	Súdán	k1gInSc7	Súdán
a	a	k8xC	a
Jižním	jižní	k2eAgInSc7d1	jižní
Súdánem	Súdán	k1gInSc7	Súdán
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
Eritrejí	Eritrea	k1gFnSc7	Eritrea
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
Džibutskem	Džibutsko	k1gNnSc7	Džibutsko
a	a	k8xC	a
Somálskem	Somálsko	k1gNnSc7	Somálsko
na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
Keňou	Keňa	k1gFnSc7	Keňa
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
<g/>
.	.	kIx.	.
<g/>
Etiopii	Etiopie	k1gFnSc6	Etiopie
dominuje	dominovat	k5eAaImIp3nS	dominovat
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
náhorní	náhorní	k2eAgFnSc1d1	náhorní
plošina	plošina	k1gFnSc1	plošina
nacházející	nacházející	k2eAgFnSc1d1	nacházející
se	se	k3xPyFc4	se
ve	v	k7c6	v
středu	střed	k1gInSc6	střed
země	zem	k1gFnSc2	zem
<g/>
,	,	kIx,	,
rozdělená	rozdělený	k2eAgFnSc1d1	rozdělená
na	na	k7c4	na
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
Velkou	velký	k2eAgFnSc7d1	velká
příkopovou	příkopový	k2eAgFnSc7d1	příkopová
propadlinou	propadlina	k1gFnSc7	propadlina
<g/>
.	.	kIx.	.
</s>
<s>
Výška	výška	k1gFnSc1	výška
plošiny	plošina	k1gFnSc2	plošina
se	se	k3xPyFc4	se
průměrně	průměrně	k6eAd1	průměrně
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
1500	[number]	k4	1500
a	a	k8xC	a
3000	[number]	k4	3000
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
<g/>
,	,	kIx,	,
celý	celý	k2eAgInSc1d1	celý
masiv	masiv	k1gInSc1	masiv
je	být	k5eAaImIp3nS	být
charakterizován	charakterizovat	k5eAaBmNgInS	charakterizovat
horskými	horský	k2eAgInPc7d1	horský
hřebeny	hřeben	k1gInPc7	hřeben
rozdělenými	rozdělený	k2eAgFnPc7d1	rozdělená
hlubokými	hluboký	k2eAgFnPc7d1	hluboká
roklemi	rokle	k1gFnPc7	rokle
a	a	k8xC	a
říčními	říční	k2eAgNnPc7d1	říční
údolími	údolí	k1gNnPc7	údolí
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
převážně	převážně	k6eAd1	převážně
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
části	část	k1gFnSc6	část
<g/>
.	.	kIx.	.
</s>
<s>
Plošina	plošina	k1gFnSc1	plošina
se	se	k3xPyFc4	se
pomalu	pomalu	k6eAd1	pomalu
svažuje	svažovat	k5eAaImIp3nS	svažovat
do	do	k7c2	do
nížin	nížina	k1gFnPc2	nížina
na	na	k7c4	na
západ	západ	k1gInSc4	západ
do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
Súdánu	Súdán	k1gInSc2	Súdán
a	a	k8xC	a
do	do	k7c2	do
jihovýchodní	jihovýchodní	k2eAgFnSc2d1	jihovýchodní
části	část	k1gFnSc2	část
Etiopie	Etiopie	k1gFnSc2	Etiopie
obývané	obývaný	k2eAgFnSc2d1	obývaná
Somálci	Somálec	k1gMnSc3	Somálec
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
bodem	bod	k1gInSc7	bod
je	být	k5eAaImIp3nS	být
Ras	ras	k1gMnSc1	ras
Dašen	Dašen	k2eAgMnSc1d1	Dašen
(	(	kIx(	(
<g/>
Bejeda	Bejeda	k1gMnSc1	Bejeda
<g/>
)	)	kIx)	)
s	s	k7c7	s
4620	[number]	k4	4620
metry	metr	k1gInPc4	metr
nad	nad	k7c7	nad
mořem	moře	k1gNnSc7	moře
<g/>
.	.	kIx.	.
</s>
<s>
Naopak	naopak	k6eAd1	naopak
nejnižším	nízký	k2eAgInSc7d3	nejnižší
bodem	bod	k1gInSc7	bod
je	být	k5eAaImIp3nS	být
Danakilská	Danakilský	k2eAgFnSc1d1	Danakilská
proláklina	proláklina	k1gFnSc1	proláklina
dosahující	dosahující	k2eAgFnSc1d1	dosahující
115	[number]	k4	115
metrů	metr	k1gInPc2	metr
pod	pod	k7c7	pod
hladinou	hladina	k1gFnSc7	hladina
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
také	také	k9	také
o	o	k7c4	o
jedno	jeden	k4xCgNnSc4	jeden
z	z	k7c2	z
nejteplejších	teplý	k2eAgNnPc2d3	nejteplejší
míst	místo	k1gNnPc2	místo
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
geologické	geologický	k2eAgFnSc3d1	geologická
aktivitě	aktivita	k1gFnSc3	aktivita
Velké	velký	k2eAgFnSc2d1	velká
příkopové	příkopový	k2eAgFnSc2d1	příkopová
propadliny	propadlina	k1gFnSc2	propadlina
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
její	její	k3xOp3gFnSc6	její
blízkosti	blízkost	k1gFnSc6	blízkost
možný	možný	k2eAgInSc1d1	možný
častější	častý	k2eAgInSc1d2	častější
výskyt	výskyt	k1gInSc1	výskyt
zemětřesení	zemětřesení	k1gNnSc2	zemětřesení
a	a	k8xC	a
sopečné	sopečný	k2eAgFnSc2d1	sopečná
činnosti	činnost	k1gFnSc2	činnost
<g/>
.	.	kIx.	.
<g/>
Obdělává	obdělávat	k5eAaImIp3nS	obdělávat
se	se	k3xPyFc4	se
zhruba	zhruba	k6eAd1	zhruba
17	[number]	k4	17
%	%	kIx~	%
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
přestože	přestože	k8xS	přestože
podíl	podíl	k1gInSc1	podíl
půdy	půda	k1gFnSc2	půda
použitelné	použitelný	k2eAgFnSc2d1	použitelná
k	k	k7c3	k
orbě	orba	k1gFnSc3	orba
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgInSc1d2	vyšší
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
intenzivní	intenzivní	k2eAgFnSc2d1	intenzivní
deforestace	deforestace	k1gFnSc2	deforestace
posledních	poslední	k2eAgInPc2d1	poslední
30	[number]	k4	30
let	léto	k1gNnPc2	léto
je	být	k5eAaImIp3nS	být
zalesněno	zalesnit	k5eAaPmNgNnS	zalesnit
pouze	pouze	k6eAd1	pouze
zhruba	zhruba	k6eAd1	zhruba
10	[number]	k4	10
až	až	k9	až
15	[number]	k4	15
%	%	kIx~	%
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Zbytek	zbytek	k1gInSc1	zbytek
je	být	k5eAaImIp3nS	být
využíván	využívat	k5eAaImNgInS	využívat
pro	pro	k7c4	pro
pastvu	pastva	k1gFnSc4	pastva
<g/>
,	,	kIx,	,
pakliže	pakliže	k8xS	pakliže
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
drsný	drsný	k2eAgInSc4d1	drsný
<g/>
,	,	kIx,	,
suchý	suchý	k2eAgInSc4d1	suchý
či	či	k8xC	či
neúrodný	úrodný	k2eNgInSc4d1	neúrodný
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
dal	dát	k5eAaPmAgInS	dát
využít	využít	k5eAaPmF	využít
k	k	k7c3	k
obhospodařování	obhospodařování	k1gNnSc3	obhospodařování
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
silně	silně	k6eAd1	silně
trpí	trpět	k5eAaImIp3nS	trpět
nadměrným	nadměrný	k2eAgNnSc7d1	nadměrné
využíváním	využívání	k1gNnSc7	využívání
pastvin	pastvina	k1gFnPc2	pastvina
<g/>
,	,	kIx,	,
erozí	eroze	k1gFnPc2	eroze
<g/>
,	,	kIx,	,
desertifikací	desertifikace	k1gFnPc2	desertifikace
a	a	k8xC	a
častými	častý	k2eAgNnPc7d1	časté
suchy	sucho	k1gNnPc7	sucho
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
oblastech	oblast	k1gFnPc6	oblast
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
v	v	k7c6	v
suchých	suchý	k2eAgNnPc6d1	suché
obdobích	období	k1gNnPc6	období
nedostatek	nedostatek	k1gInSc4	nedostatek
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
Velké	velký	k2eAgFnSc2d1	velká
příkopové	příkopový	k2eAgFnSc2d1	příkopová
propadliny	propadlina	k1gFnSc2	propadlina
se	se	k3xPyFc4	se
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
rozsáhlá	rozsáhlý	k2eAgFnSc1d1	rozsáhlá
síť	síť	k1gFnSc1	síť
jezer	jezero	k1gNnPc2	jezero
<g/>
,	,	kIx,	,
největší	veliký	k2eAgFnSc1d3	veliký
přírodní	přírodní	k2eAgFnSc1d1	přírodní
vodní	vodní	k2eAgFnSc1d1	vodní
nádrž	nádrž	k1gFnSc1	nádrž
je	být	k5eAaImIp3nS	být
však	však	k9	však
jezero	jezero	k1gNnSc1	jezero
Tana	tanout	k5eAaImSgInS	tanout
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
<g/>
.	.	kIx.	.
</s>
<s>
Nejvodnější	Nejvodný	k2eAgFnSc7d2	Nejvodný
etiopskou	etiopský	k2eAgFnSc7d1	etiopská
řekou	řeka	k1gFnSc7	řeka
je	být	k5eAaImIp3nS	být
Modrý	modrý	k2eAgInSc1d1	modrý
Nil	Nil	k1gInSc1	Nil
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Tekeze	Tekeze	k1gFnSc2	Tekeze
a	a	k8xC	a
Barem	Barma	k1gFnPc2	Barma
vtéká	vtékat	k5eAaImIp3nS	vtékat
na	na	k7c6	na
západě	západ	k1gInSc6	západ
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
na	na	k7c6	na
území	území	k1gNnSc6	území
Súdánu	Súdán	k1gInSc2	Súdán
<g/>
,	,	kIx,	,
do	do	k7c2	do
Nilu	Nil	k1gInSc2	Nil
<g/>
.	.	kIx.	.
</s>
<s>
Řeka	Řek	k1gMnSc2	Řek
Awaš	Awaš	k1gInSc1	Awaš
naopak	naopak	k6eAd1	naopak
teče	téct	k5eAaImIp3nS	téct
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
vlévá	vlévat	k5eAaImIp3nS	vlévat
do	do	k7c2	do
slaných	slaný	k2eAgNnPc2d1	slané
jezer	jezero	k1gNnPc2	jezero
v	v	k7c6	v
Danakilské	Danakilský	k2eAgFnSc6d1	Danakilská
proláklině	proláklina	k1gFnSc6	proláklina
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
velkých	velký	k2eAgFnPc2d1	velká
řek	řeka	k1gFnPc2	řeka
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
zmínit	zmínit	k5eAaPmF	zmínit
také	také	k9	také
Genali	Genali	k1gFnPc7	Genali
a	a	k8xC	a
Šebeli	Šebel	k1gInPc7	Šebel
<g/>
,	,	kIx,	,
tekoucí	tekoucí	k2eAgNnSc1d1	tekoucí
na	na	k7c4	na
jihovýchod	jihovýchod	k1gInSc4	jihovýchod
do	do	k7c2	do
Somálska	Somálsko	k1gNnSc2	Somálsko
<g/>
,	,	kIx,	,
a	a	k8xC	a
Omo	Omo	k1gFnSc1	Omo
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
ústí	ústit	k5eAaImIp3nS	ústit
do	do	k7c2	do
jezera	jezero	k1gNnSc2	jezero
Turkana	Turkan	k1gMnSc2	Turkan
na	na	k7c6	na
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
Keňou	Keňa	k1gFnSc7	Keňa
<g/>
.	.	kIx.	.
<g/>
Etiopie	Etiopie	k1gFnSc1	Etiopie
je	být	k5eAaImIp3nS	být
domovem	domov	k1gInSc7	domov
rozličných	rozličný	k2eAgInPc2d1	rozličný
živočišných	živočišný	k2eAgInPc2d1	živočišný
druhů	druh	k1gInPc2	druh
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgInPc4	jenž
patří	patřit	k5eAaImIp3nP	patřit
typičtí	typický	k2eAgMnPc1d1	typický
zástupci	zástupce	k1gMnPc1	zástupce
africké	africký	k2eAgFnSc2d1	africká
fauny	fauna	k1gFnSc2	fauna
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
lev	lev	k1gMnSc1	lev
<g/>
,	,	kIx,	,
leopard	leopard	k1gMnSc1	leopard
<g/>
,	,	kIx,	,
gepard	gepard	k1gMnSc1	gepard
<g/>
,	,	kIx,	,
hroch	hroch	k1gMnSc1	hroch
<g/>
,	,	kIx,	,
slon	slon	k1gMnSc1	slon
<g/>
,	,	kIx,	,
zebra	zebra	k1gFnSc1	zebra
<g/>
,	,	kIx,	,
různé	různý	k2eAgInPc1d1	různý
druhy	druh	k1gInPc1	druh
opic	opice	k1gFnPc2	opice
a	a	k8xC	a
gazel	gazela	k1gFnPc2	gazela
<g/>
,	,	kIx,	,
žiraf	žirafa	k1gFnPc2	žirafa
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
i	i	k9	i
ptáci	pták	k1gMnPc1	pták
jako	jako	k8xC	jako
plameňák	plameňák	k1gMnSc1	plameňák
<g/>
,	,	kIx,	,
pelikán	pelikán	k1gMnSc1	pelikán
<g/>
,	,	kIx,	,
husa	husa	k1gFnSc1	husa
<g/>
,	,	kIx,	,
čáp	čáp	k1gMnSc1	čáp
<g/>
,	,	kIx,	,
ibis	ibis	k1gMnSc1	ibis
či	či	k8xC	či
marabu	marabu	k1gMnSc1	marabu
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
se	se	k3xPyFc4	se
také	také	k9	také
nacházejí	nacházet	k5eAaImIp3nP	nacházet
i	i	k9	i
druhy	druh	k1gInPc1	druh
endemické	endemický	k2eAgInPc1d1	endemický
<g/>
,	,	kIx,	,
omezené	omezený	k2eAgFnPc1d1	omezená
na	na	k7c4	na
jedno	jeden	k4xCgNnSc4	jeden
menší	malý	k2eAgNnSc4d2	menší
území	území	k1gNnSc4	území
–	–	k?	–
těmi	ten	k3xDgInPc7	ten
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
nyala	nyala	k6eAd1	nyala
horská	horský	k2eAgFnSc1d1	horská
<g/>
,	,	kIx,	,
vlček	vlček	k1gInSc1	vlček
etiopský	etiopský	k2eAgInSc1d1	etiopský
nebo	nebo	k8xC	nebo
dželada	dželada	k1gFnSc1	dželada
<g/>
.	.	kIx.	.
</s>
<s>
Rozmanité	rozmanitý	k2eAgNnSc1d1	rozmanité
je	být	k5eAaImIp3nS	být
také	také	k9	také
rostlinstvo	rostlinstvo	k1gNnSc1	rostlinstvo
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vysokohorských	vysokohorský	k2eAgFnPc6d1	vysokohorská
oblastech	oblast	k1gFnPc6	oblast
převládají	převládat	k5eAaImIp3nP	převládat
jehličnaté	jehličnatý	k2eAgInPc1d1	jehličnatý
lesy	les	k1gInPc1	les
<g/>
,	,	kIx,	,
vřesy	vřes	k1gInPc1	vřes
a	a	k8xC	a
mlžné	mlžný	k2eAgInPc1d1	mlžný
pralesy	prales	k1gInPc1	prales
<g/>
,	,	kIx,	,
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
se	se	k3xPyFc4	se
rozkládají	rozkládat	k5eAaImIp3nP	rozkládat
spíše	spíše	k9	spíše
listnaté	listnatý	k2eAgInPc1d1	listnatý
stromy	strom	k1gInPc1	strom
<g/>
,	,	kIx,	,
deštné	deštný	k2eAgInPc1d1	deštný
pralesy	prales	k1gInPc1	prales
<g/>
,	,	kIx,	,
rostou	růst	k5eAaImIp3nP	růst
zde	zde	k6eAd1	zde
často	často	k6eAd1	často
liány	liána	k1gFnSc2	liána
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
pláních	pláň	k1gFnPc6	pláň
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
ve	v	k7c6	v
velkém	velký	k2eAgInSc6d1	velký
množství	množství	k1gNnSc2	množství
kaktusy	kaktus	k1gInPc4	kaktus
<g/>
,	,	kIx,	,
sukulenty	sukulent	k1gInPc4	sukulent
a	a	k8xC	a
velké	velký	k2eAgFnPc4d1	velká
sykomory	sykomora	k1gFnPc4	sykomora
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
teplých	teplý	k2eAgFnPc6d1	teplá
oblastech	oblast	k1gFnPc6	oblast
je	být	k5eAaImIp3nS	být
možno	možno	k6eAd1	možno
nalézt	nalézt	k5eAaBmF	nalézt
nepravý	pravý	k2eNgInSc4d1	nepravý
banán	banán	k1gInSc4	banán
známý	známý	k1gMnSc1	známý
jako	jako	k8xS	jako
inset	inset	k1gMnSc1	inset
<g/>
.	.	kIx.	.
</s>
<s>
Poblíž	poblíž	k7c2	poblíž
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
roste	růst	k5eAaImIp3nS	růst
v	v	k7c6	v
hojném	hojný	k2eAgNnSc6d1	hojné
množství	množství	k1gNnSc6	množství
eukalyptus	eukalyptus	k1gMnSc1	eukalyptus
<g/>
,	,	kIx,	,
dovezený	dovezený	k2eAgMnSc1d1	dovezený
do	do	k7c2	do
země	zem	k1gFnSc2	zem
z	z	k7c2	z
Austrálie	Austrálie	k1gFnSc2	Austrálie
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejznámějších	známý	k2eAgFnPc2d3	nejznámější
a	a	k8xC	a
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
etiopských	etiopský	k2eAgFnPc2d1	etiopská
plodin	plodina	k1gFnPc2	plodina
je	být	k5eAaImIp3nS	být
káva	káva	k1gFnSc1	káva
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
z	z	k7c2	z
Etiopie	Etiopie	k1gFnSc2	Etiopie
také	také	k9	také
původně	původně	k6eAd1	původně
pochází	pocházet	k5eAaImIp3nS	pocházet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Podnebí	podnebí	k1gNnSc2	podnebí
===	===	k?	===
</s>
</p>
<p>
<s>
Území	území	k1gNnSc1	území
Etiopie	Etiopie	k1gFnSc2	Etiopie
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
tropické	tropický	k2eAgFnSc6d1	tropická
monzunové	monzunový	k2eAgFnSc6d1	monzunová
oblasti	oblast	k1gFnSc6	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gNnSc1	její
podnebí	podnebí	k1gNnSc1	podnebí
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
rozmanité	rozmanitý	k2eAgNnSc1d1	rozmanité
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
rozdílům	rozdíl	k1gInPc3	rozdíl
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblastech	oblast	k1gFnPc6	oblast
nad	nad	k7c7	nad
1500	[number]	k4	1500
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
převažují	převažovat	k5eAaImIp3nP	převažovat
denní	denní	k2eAgFnPc1d1	denní
teploty	teplota	k1gFnPc1	teplota
mezi	mezi	k7c7	mezi
16	[number]	k4	16
a	a	k8xC	a
30	[number]	k4	30
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
střídané	střídaný	k2eAgInPc1d1	střídaný
chladnějšími	chladný	k2eAgFnPc7d2	chladnější
nocemi	noc	k1gFnPc7	noc
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
níže	nízce	k6eAd2	nízce
položených	položený	k2eAgFnPc6d1	položená
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
v	v	k7c6	v
Dire	Dire	k1gFnSc6	Dire
Dawě	Dawa	k1gFnSc6	Dawa
nebo	nebo	k8xC	nebo
Afarsku	Afarsko	k1gNnSc6	Afarsko
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
teploty	teplota	k1gFnPc1	teplota
výrazně	výrazně	k6eAd1	výrazně
vyšší	vysoký	k2eAgFnPc1d2	vyšší
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Danakilské	Danakilský	k2eAgFnSc2d1	Danakilská
pouště	poušť	k1gFnSc2	poušť
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
teploty	teplota	k1gFnPc1	teplota
40	[number]	k4	40
°	°	k?	°
<g/>
C.	C.	kA	C.
Někdy	někdy	k6eAd1	někdy
se	se	k3xPyFc4	se
nížiny	nížina	k1gFnPc1	nížina
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
též	též	k9	též
vysokou	vysoký	k2eAgFnSc7d1	vysoká
vlhkostí	vlhkost	k1gFnSc7	vlhkost
vzduchu	vzduch	k1gInSc2	vzduch
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
se	se	k3xPyFc4	se
střídají	střídat	k5eAaImIp3nP	střídat
období	období	k1gNnSc4	období
sucha	sucho	k1gNnSc2	sucho
a	a	k8xC	a
období	období	k1gNnSc2	období
dešťů	dešť	k1gInPc2	dešť
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
ledna	leden	k1gInSc2	leden
a	a	k8xC	a
února	únor	k1gInSc2	únor
převažují	převažovat	k5eAaImIp3nP	převažovat
občasné	občasný	k2eAgInPc1d1	občasný
deště	dešť	k1gInPc1	dešť
–	–	k?	–
pokud	pokud	k8xS	pokud
tyto	tento	k3xDgInPc1	tento
nejsou	být	k5eNaImIp3nP	být
dostatečné	dostatečný	k2eAgInPc1d1	dostatečný
<g/>
,	,	kIx,	,
hrozí	hrozit	k5eAaImIp3nS	hrozit
hladomory	hladomor	k1gInPc4	hladomor
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
konce	konec	k1gInSc2	konec
února	únor	k1gInSc2	únor
až	až	k6eAd1	až
do	do	k7c2	do
června	červen	k1gInSc2	červen
trvá	trvat	k5eAaImIp3nS	trvat
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
suché	suchý	k2eAgNnSc1d1	suché
období	období	k1gNnSc1	období
<g/>
,	,	kIx,	,
po	po	k7c6	po
němž	jenž	k3xRgNnSc6	jenž
přicházejí	přicházet	k5eAaImIp3nP	přicházet
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
června	červen	k1gInSc2	červen
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
září	zářit	k5eAaImIp3nP	zářit
silné	silný	k2eAgInPc1d1	silný
deště	dešť	k1gInPc1	dešť
<g/>
.	.	kIx.	.
</s>
<s>
Intenzivní	intenzivní	k2eAgFnPc1d1	intenzivní
srážky	srážka	k1gFnPc1	srážka
zanechají	zanechat	k5eAaPmIp3nP	zanechat
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
dostatek	dostatek	k1gInSc4	dostatek
vláhy	vláha	k1gFnSc2	vláha
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
vydrží	vydržet	k5eAaPmIp3nS	vydržet
zhruba	zhruba	k6eAd1	zhruba
do	do	k7c2	do
prosince	prosinec	k1gInSc2	prosinec
a	a	k8xC	a
ledna	leden	k1gInSc2	leden
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
opět	opět	k6eAd1	opět
přicházejí	přicházet	k5eAaImIp3nP	přicházet
sucha	sucho	k1gNnPc1	sucho
<g/>
,	,	kIx,	,
následovaném	následovaný	k2eAgInSc6d1	následovaný
od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
ledna	leden	k1gInSc2	leden
občasnými	občasný	k2eAgInPc7d1	občasný
dešti	dešť	k1gInPc7	dešť
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
politika	politika	k1gFnSc1	politika
==	==	k?	==
</s>
</p>
<p>
<s>
Etiopie	Etiopie	k1gFnSc1	Etiopie
je	být	k5eAaImIp3nS	být
federativní	federativní	k2eAgFnSc7d1	federativní
parlamentní	parlamentní	k2eAgFnSc7d1	parlamentní
republikou	republika	k1gFnSc7	republika
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
státy	stát	k1gInPc1	stát
federace	federace	k1gFnSc2	federace
<g/>
,	,	kIx,	,
vytvořené	vytvořený	k2eAgFnSc2d1	vytvořená
na	na	k7c6	na
etnickém	etnický	k2eAgInSc6d1	etnický
základu	základ	k1gInSc6	základ
<g/>
,	,	kIx,	,
mají	mít	k5eAaImIp3nP	mít
právo	právo	k1gNnSc4	právo
ratifikovat	ratifikovat	k5eAaBmF	ratifikovat
vlastní	vlastní	k2eAgInPc4d1	vlastní
regionální	regionální	k2eAgInSc4d1	regionální
ústavy	ústav	k1gInPc4	ústav
<g/>
,	,	kIx,	,
uvádět	uvádět	k5eAaImF	uvádět
v	v	k7c4	v
platnost	platnost	k1gFnSc4	platnost
zákony	zákon	k1gInPc4	zákon
a	a	k8xC	a
vytvářet	vytvářet	k5eAaImF	vytvářet
i	i	k8xC	i
spravovat	spravovat	k5eAaImF	spravovat
vládní	vládní	k2eAgFnPc4d1	vládní
funkce	funkce	k1gFnPc4	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
též	též	k9	též
od	od	k7c2	od
mateřské	mateřský	k2eAgFnSc2d1	mateřská
země	zem	k1gFnSc2	zem
odtrhnout	odtrhnout	k5eAaPmF	odtrhnout
<g/>
.	.	kIx.	.
</s>
<s>
Zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
moc	moc	k1gFnSc4	moc
představuje	představovat	k5eAaImIp3nS	představovat
dvoukomorový	dvoukomorový	k2eAgInSc1d1	dvoukomorový
parlament	parlament	k1gInSc1	parlament
skládající	skládající	k2eAgFnSc2d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
horní	horní	k2eAgFnSc2d1	horní
Komory	komora	k1gFnSc2	komora
federace	federace	k1gFnSc2	federace
mající	mající	k2eAgInPc1d1	mající
108	[number]	k4	108
křesel	křeslo	k1gNnPc2	křeslo
a	a	k8xC	a
dolní	dolní	k2eAgFnSc2d1	dolní
Komory	komora	k1gFnSc2	komora
lidových	lidový	k2eAgMnPc2d1	lidový
zástupců	zástupce	k1gMnPc2	zástupce
skládající	skládající	k2eAgInPc4d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
548	[number]	k4	548
křesel	křeslo	k1gNnPc2	křeslo
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
členové	člen	k1gMnPc1	člen
horní	horní	k2eAgFnSc2d1	horní
komory	komora	k1gFnSc2	komora
jsou	být	k5eAaImIp3nP	být
voleni	volit	k5eAaImNgMnP	volit
parlamentními	parlamentní	k2eAgFnPc7d1	parlamentní
shromážděními	shromáždění	k1gNnPc7	shromáždění
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
států	stát	k1gInPc2	stát
federace	federace	k1gFnSc2	federace
<g/>
,	,	kIx,	,
dolní	dolní	k2eAgFnSc1d1	dolní
komora	komora	k1gFnSc1	komora
je	být	k5eAaImIp3nS	být
volena	volit	k5eAaImNgFnS	volit
ve	v	k7c6	v
všeobecném	všeobecný	k2eAgNnSc6d1	všeobecné
hlasování	hlasování	k1gNnSc6	hlasování
<g/>
.	.	kIx.	.
</s>
<s>
Volební	volební	k2eAgNnSc4d1	volební
právo	právo	k1gNnSc4	právo
mají	mít	k5eAaImIp3nP	mít
všichni	všechen	k3xTgMnPc1	všechen
občané	občan	k1gMnPc1	občan
od	od	k7c2	od
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
prezident	prezident	k1gMnSc1	prezident
<g/>
,	,	kIx,	,
volený	volený	k2eAgMnSc1d1	volený
oběma	dva	k4xCgFnPc7	dva
komorami	komora	k1gFnPc7	komora
parlamentu	parlament	k1gInSc2	parlament
na	na	k7c4	na
šestileté	šestiletý	k2eAgNnSc4d1	šestileté
období	období	k1gNnSc4	období
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgMnSc7d1	současný
prezidentem	prezident	k1gMnSc7	prezident
Etiopie	Etiopie	k1gFnSc2	Etiopie
a	a	k8xC	a
hlavou	hlava	k1gFnSc7	hlava
státu	stát	k1gInSc2	stát
je	být	k5eAaImIp3nS	být
Mulatu	mulat	k1gMnSc3	mulat
Teshome	Teshom	k1gInSc5	Teshom
Wirtu	Wirto	k1gNnSc3	Wirto
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
představitel	představitel	k1gMnSc1	představitel
výkonné	výkonný	k2eAgFnSc2d1	výkonná
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
volen	volit	k5eAaImNgMnS	volit
z	z	k7c2	z
členů	člen	k1gMnPc2	člen
Komory	komora	k1gFnSc2	komora
lidových	lidový	k2eAgMnPc2d1	lidový
zástupců	zástupce	k1gMnPc2	zástupce
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
zároveň	zároveň	k6eAd1	zároveň
vrchním	vrchní	k2eAgMnSc7d1	vrchní
velitelem	velitel	k1gMnSc7	velitel
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
post	post	k1gInSc1	post
nyní	nyní	k6eAd1	nyní
zastává	zastávat	k5eAaImIp3nS	zastávat
zastupující	zastupující	k2eAgMnSc1d1	zastupující
předseda	předseda	k1gMnSc1	předseda
vlády	vláda	k1gFnSc2	vláda
Hailemariam	Hailemariam	k1gInSc1	Hailemariam
Desalegn	Desalegn	k1gInSc1	Desalegn
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
nastoupil	nastoupit	k5eAaPmAgInS	nastoupit
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Melese	Melese	k1gFnSc2	Melese
Zenawiho	Zenawi	k1gMnSc2	Zenawi
v	v	k7c6	v
srpnu	srpen	k1gInSc6	srpen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
soudní	soudní	k2eAgFnSc7d1	soudní
autoritou	autorita	k1gFnSc7	autorita
je	být	k5eAaImIp3nS	být
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
federální	federální	k2eAgInSc1d1	federální
soud	soud	k1gInSc1	soud
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc4	jeho
členové	člen	k1gMnPc1	člen
jsou	být	k5eAaImIp3nP	být
navrhováni	navrhován	k2eAgMnPc1d1	navrhován
předsedou	předseda	k1gMnSc7	předseda
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
schvalováni	schvalovat	k5eAaImNgMnP	schvalovat
dolní	dolní	k2eAgFnSc7d1	dolní
komorou	komora	k1gFnSc7	komora
parlamentu	parlament	k1gInSc2	parlament
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ústava	ústava	k1gFnSc1	ústava
<g/>
,	,	kIx,	,
ukotvující	ukotvující	k2eAgFnSc4d1	ukotvující
oddělenou	oddělený	k2eAgFnSc4d1	oddělená
moc	moc	k1gFnSc4	moc
zákonodárnou	zákonodárný	k2eAgFnSc4d1	zákonodárná
<g/>
,	,	kIx,	,
soudní	soudní	k2eAgFnSc4d1	soudní
a	a	k8xC	a
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
ratifikována	ratifikovat	k5eAaBmNgFnS	ratifikovat
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
existuje	existovat	k5eAaImIp3nS	existovat
systém	systém	k1gInSc1	systém
více	hodně	k6eAd2	hodně
politických	politický	k2eAgFnPc2d1	politická
stran	strana	k1gFnPc2	strana
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
dle	dle	k7c2	dle
organizace	organizace	k1gFnSc2	organizace
Freedom	Freedom	k1gInSc4	Freedom
House	house	k1gNnSc1	house
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
principy	princip	k1gInPc4	princip
demokratického	demokratický	k2eAgNnSc2d1	demokratické
zastoupení	zastoupení	k1gNnSc2	zastoupení
nefungují	fungovat	k5eNaImIp3nP	fungovat
–	–	k?	–
v	v	k7c6	v
posledních	poslední	k2eAgFnPc6d1	poslední
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2015	[number]	k4	2015
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
vládnoucí	vládnoucí	k2eAgFnSc1d1	vládnoucí
strana	strana	k1gFnSc1	strana
Etiopská	etiopský	k2eAgFnSc1d1	etiopská
lidová	lidový	k2eAgFnSc1d1	lidová
revoluční	revoluční	k2eAgFnSc1d1	revoluční
demokratická	demokratický	k2eAgFnSc1d1	demokratická
fronta	fronta	k1gFnSc1	fronta
(	(	kIx(	(
<g/>
EPRDF	EPRDF	kA	EPRDF
<g/>
)	)	kIx)	)
100	[number]	k4	100
%	%	kIx~	%
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
si	se	k3xPyFc3	se
k	k	k7c3	k
podobnému	podobný	k2eAgInSc3d1	podobný
volebnímu	volební	k2eAgInSc3d1	volební
výsledku	výsledek	k1gInSc3	výsledek
vládnoucí	vládnoucí	k2eAgInSc1d1	vládnoucí
režim	režim	k1gInSc1	režim
dopomohl	dopomoct	k5eAaPmAgInS	dopomoct
zejména	zejména	k9	zejména
díky	díky	k7c3	díky
nestandardním	standardní	k2eNgInPc3d1	nestandardní
zákrokům	zákrok	k1gInPc3	zákrok
během	během	k7c2	během
voleb	volba	k1gFnPc2	volba
<g/>
,	,	kIx,	,
opakovanému	opakovaný	k2eAgNnSc3d1	opakované
pronásledování	pronásledování	k1gNnSc3	pronásledování
odpůrců	odpůrce	k1gMnPc2	odpůrce
<g/>
,	,	kIx,	,
represím	represe	k1gFnPc3	represe
<g/>
,	,	kIx,	,
porušování	porušování	k1gNnSc1	porušování
práva	právo	k1gNnSc2	právo
na	na	k7c6	na
shromažďování	shromažďování	k1gNnSc6	shromažďování
a	a	k8xC	a
spolčování	spolčování	k1gNnSc6	spolčování
a	a	k8xC	a
takřka	takřka	k6eAd1	takřka
absenci	absence	k1gFnSc4	absence
nezávislých	závislý	k2eNgNnPc2d1	nezávislé
médií	médium	k1gNnPc2	médium
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Nadace	nadace	k1gFnSc2	nadace
Bertelsmann	Bertelsmann	k1gInSc1	Bertelsmann
federální	federální	k2eAgInSc1d1	federální
systém	systém	k1gInSc1	systém
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
téměř	téměř	k6eAd1	téměř
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
papíře	papír	k1gInSc6	papír
–	–	k?	–
EPRDF	EPRDF	kA	EPRDF
udržuje	udržovat	k5eAaImIp3nS	udržovat
nad	nad	k7c7	nad
jednotlivými	jednotlivý	k2eAgFnPc7d1	jednotlivá
regionálními	regionální	k2eAgFnPc7d1	regionální
vládami	vláda	k1gFnPc7	vláda
přísnou	přísný	k2eAgFnSc4d1	přísná
kontrolu	kontrola	k1gFnSc4	kontrola
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
nepokojům	nepokoj	k1gInPc3	nepokoj
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
premiér	premiér	k1gMnSc1	premiér
Desalegn	Desalegn	k1gMnSc1	Desalegn
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2016	[number]	k4	2016
výjimečný	výjimečný	k2eAgInSc4d1	výjimečný
stav	stav	k1gInSc4	stav
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
trval	trvat	k5eAaImAgInS	trvat
do	do	k7c2	do
srpna	srpen	k1gInSc2	srpen
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc1	co
v	v	k7c6	v
únoru	únor	k1gInSc6	únor
2018	[number]	k4	2018
premiér	premiér	k1gMnSc1	premiér
rezignoval	rezignovat	k5eAaBmAgMnS	rezignovat
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
funkci	funkce	k1gFnSc4	funkce
<g/>
,	,	kIx,	,
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
etiopská	etiopský	k2eAgFnSc1d1	etiopská
vláda	vláda	k1gFnSc1	vláda
opakovaně	opakovaně	k6eAd1	opakovaně
výjimečný	výjimečný	k2eAgInSc4d1	výjimečný
stav	stav	k1gInSc4	stav
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
prohlášení	prohlášení	k1gNnSc6	prohlášení
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
důvodem	důvod	k1gInSc7	důvod
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
druhého	druhý	k4xOgMnSc2	druhý
výjimečného	výjimečný	k2eAgMnSc2d1	výjimečný
za	za	k7c4	za
poslední	poslední	k2eAgInSc4d1	poslední
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
protesty	protest	k1gInPc4	protest
<g/>
,	,	kIx,	,
které	který	k3yQgInPc4	který
si	se	k3xPyFc3	se
vyžádaly	vyžádat	k5eAaPmAgFnP	vyžádat
oběti	oběť	k1gFnPc1	oběť
na	na	k7c6	na
životech	život	k1gInPc6	život
i	i	k8xC	i
zranění	zranění	k1gNnSc6	zranění
<g/>
,	,	kIx,	,
vyhnaly	vyhnat	k5eAaPmAgInP	vyhnat
část	část	k1gFnSc4	část
obyvatel	obyvatel	k1gMnPc2	obyvatel
z	z	k7c2	z
domovů	domov	k1gInPc2	domov
a	a	k8xC	a
připravily	připravit	k5eAaPmAgFnP	připravit
je	on	k3xPp3gFnPc4	on
o	o	k7c4	o
majetek	majetek	k1gInSc4	majetek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
centrální	centrální	k2eAgFnSc6d1	centrální
části	část	k1gFnSc6	část
země	zem	k1gFnSc2	zem
má	mít	k5eAaImIp3nS	mít
vláda	vláda	k1gFnSc1	vláda
situaci	situace	k1gFnSc3	situace
pevně	pevně	k6eAd1	pevně
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
<g/>
,	,	kIx,	,
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
odlehlejších	odlehlý	k2eAgFnPc6d2	odlehlejší
oblastech	oblast	k1gFnPc6	oblast
proti	proti	k7c3	proti
ní	on	k3xPp3gFnSc3	on
bojují	bojovat	k5eAaImIp3nP	bojovat
různá	různý	k2eAgNnPc4d1	různé
osvobozenecká	osvobozenecký	k2eAgNnPc4d1	osvobozenecké
hnutí	hnutí	k1gNnPc4	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
jinými	jiný	k1gMnPc7	jiný
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
Oromskou	Oromský	k2eAgFnSc4d1	Oromský
osvobozeneckou	osvobozenecký	k2eAgFnSc4d1	osvobozenecká
frontu	fronta	k1gFnSc4	fronta
(	(	kIx(	(
<g/>
OLF	OLF	kA	OLF
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Ogadenskou	Ogadenský	k2eAgFnSc4d1	Ogadenský
národně	národně	k6eAd1	národně
osvobozeneckou	osvobozenecký	k2eAgFnSc4d1	osvobozenecká
frontu	fronta	k1gFnSc4	fronta
(	(	kIx(	(
<g/>
ONLF	ONLF	kA	ONLF
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
Afarskou	Afarský	k2eAgFnSc4d1	Afarská
národně	národně	k6eAd1	národně
osvobozeneckou	osvobozenecký	k2eAgFnSc4d1	osvobozenecká
frontu	fronta	k1gFnSc4	fronta
<g/>
,	,	kIx,	,
doprovázené	doprovázený	k2eAgInPc4d1	doprovázený
dalšími	další	k2eAgFnPc7d1	další
menšími	malý	k2eAgFnPc7d2	menší
organizacemi	organizace	k1gFnPc7	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Neklidnou	klidný	k2eNgFnSc4d1	neklidná
situaci	situace	k1gFnSc4	situace
dokreslují	dokreslovat	k5eAaImIp3nP	dokreslovat
časté	častý	k2eAgFnPc4d1	častá
krvavé	krvavý	k2eAgFnPc4d1	krvavá
kmenové	kmenový	k2eAgFnPc4d1	kmenová
srážky	srážka	k1gFnPc4	srážka
o	o	k7c4	o
vodu	voda	k1gFnSc4	voda
a	a	k8xC	a
půdu	půda	k1gFnSc4	půda
v	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
částech	část	k1gFnPc6	část
Etiopie	Etiopie	k1gFnSc2	Etiopie
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
mezi	mezi	k7c7	mezi
lety	léto	k1gNnPc7	léto
2006	[number]	k4	2006
a	a	k8xC	a
2008	[number]	k4	2008
explodovalo	explodovat	k5eAaBmAgNnS	explodovat
v	v	k7c6	v
Addis	Addis	k1gFnSc6	Addis
Abebě	Abeba	k1gFnSc6	Abeba
a	a	k8xC	a
dalších	další	k2eAgNnPc6d1	další
městech	město	k1gNnPc6	město
několik	několik	k4yIc4	několik
bomb	bomba	k1gFnPc2	bomba
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
zabily	zabít	k5eAaPmAgFnP	zabít
zhruba	zhruba	k6eAd1	zhruba
20	[number]	k4	20
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
za	za	k7c7	za
těmito	tento	k3xDgFnPc7	tento
činy	čin	k1gInPc4	čin
stál	stát	k5eAaImAgInS	stát
<g/>
.	.	kIx.	.
</s>
<s>
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
zahraničí	zahraničí	k1gNnSc2	zahraničí
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
amerických	americký	k2eAgInPc2d1	americký
a	a	k8xC	a
organizace	organizace	k1gFnSc2	organizace
Human	Humana	k1gFnPc2	Humana
Rights	Rights	k1gInSc1	Rights
Watch	Watch	k1gMnSc1	Watch
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
lidé	člověk	k1gMnPc1	člověk
podezřelí	podezřelý	k2eAgMnPc1d1	podezřelý
ze	z	k7c2	z
sympatií	sympatie	k1gFnPc2	sympatie
k	k	k7c3	k
politické	politický	k2eAgFnSc3d1	politická
opozici	opozice	k1gFnSc3	opozice
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
osvobozeneckým	osvobozenecký	k2eAgNnPc3d1	osvobozenecké
hnutím	hnutí	k1gNnPc3	hnutí
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
oběťmi	oběť	k1gFnPc7	oběť
porušování	porušování	k1gNnPc4	porušování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgMnPc4	jenž
často	často	k6eAd1	často
patří	patřit	k5eAaImIp3nP	patřit
popravy	poprava	k1gFnPc4	poprava
bez	bez	k7c2	bez
soudu	soud	k1gInSc2	soud
<g/>
,	,	kIx,	,
znásilňování	znásilňování	k1gNnSc1	znásilňování
<g/>
,	,	kIx,	,
mučení	mučení	k1gNnSc1	mučení
či	či	k8xC	či
svévolné	svévolný	k2eAgNnSc1d1	svévolné
zatčení	zatčení	k1gNnSc1	zatčení
<g/>
.	.	kIx.	.
</s>
<s>
Řada	řada	k1gFnSc1	řada
umělců	umělec	k1gMnPc2	umělec
<g/>
,	,	kIx,	,
sportovců	sportovec	k1gMnPc2	sportovec
či	či	k8xC	či
diplomatů	diplomat	k1gMnPc2	diplomat
utíká	utíkat	k5eAaImIp3nS	utíkat
do	do	k7c2	do
zahraničí	zahraničí	k1gNnSc2	zahraničí
a	a	k8xC	a
žádá	žádat	k5eAaImIp3nS	žádat
o	o	k7c4	o
azyl	azyl	k1gInSc4	azyl
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
má	mít	k5eAaImIp3nS	mít
silné	silný	k2eAgInPc4d1	silný
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
korupcí	korupce	k1gFnSc7	korupce
<g/>
.	.	kIx.	.
</s>
<s>
Nižší	nízký	k2eAgFnPc1d2	nižší
úrovně	úroveň	k1gFnPc1	úroveň
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
trpí	trpět	k5eAaImIp3nP	trpět
úplatkářstvím	úplatkářství	k1gNnSc7	úplatkářství
a	a	k8xC	a
inkompetencí	inkompetence	k1gFnPc2	inkompetence
<g/>
,	,	kIx,	,
vyšší	vysoký	k2eAgFnPc1d2	vyšší
politické	politický	k2eAgFnPc1d1	politická
špičky	špička	k1gFnPc1	špička
používají	používat	k5eAaImIp3nP	používat
úplatky	úplatek	k1gInPc4	úplatek
k	k	k7c3	k
udržení	udržení	k1gNnSc3	udržení
loajality	loajalita	k1gFnSc2	loajalita
ve	v	k7c6	v
vlastní	vlastní	k2eAgFnSc6d1	vlastní
straně	strana	k1gFnSc6	strana
i	i	k8xC	i
vládě	vláda	k1gFnSc6	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Silně	silně	k6eAd1	silně
zkorumpovaná	zkorumpovaný	k2eAgFnSc1d1	zkorumpovaná
je	být	k5eAaImIp3nS	být
i	i	k9	i
justice	justice	k1gFnSc1	justice
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ani	ani	k8xC	ani
plně	plně	k6eAd1	plně
nefunguje	fungovat	k5eNaImIp3nS	fungovat
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
oblastech	oblast	k1gFnPc6	oblast
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
je	být	k5eAaImIp3nS	být
soudní	soudní	k2eAgFnSc1d1	soudní
moc	moc	k1gFnSc1	moc
pod	pod	k7c7	pod
silným	silný	k2eAgInSc7d1	silný
vlivem	vliv	k1gInSc7	vliv
moci	moc	k1gFnSc2	moc
výkonné	výkonný	k2eAgFnSc2d1	výkonná
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
upozorňuje	upozorňovat	k5eAaImIp3nS	upozorňovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
přes	přes	k7c4	přes
určité	určitý	k2eAgFnPc4d1	určitá
korupční	korupční	k2eAgFnPc4d1	korupční
praktiky	praktika	k1gFnPc4	praktika
samotné	samotný	k2eAgFnSc2d1	samotná
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
ve	v	k7c6	v
vládnoucí	vládnoucí	k2eAgFnSc6d1	vládnoucí
třídě	třída	k1gFnSc6	třída
snaha	snaha	k1gFnSc1	snaha
tyto	tento	k3xDgInPc4	tento
problémy	problém	k1gInPc4	problém
řešit	řešit	k5eAaImF	řešit
a	a	k8xC	a
korupční	korupční	k2eAgFnPc1d1	korupční
aktivity	aktivita	k1gFnPc1	aktivita
omezit	omezit	k5eAaPmF	omezit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Administrativní	administrativní	k2eAgInSc1d1	administrativní
systém	systém	k1gInSc1	systém
===	===	k?	===
</s>
</p>
<p>
<s>
Území	území	k1gNnSc1	území
Etiopie	Etiopie	k1gFnSc2	Etiopie
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
9	[number]	k4	9
etnicky	etnicky	k6eAd1	etnicky
založených	založený	k2eAgInPc2d1	založený
federálních	federální	k2eAgInPc2d1	federální
států	stát	k1gInPc2	stát
a	a	k8xC	a
2	[number]	k4	2
samosprávné	samosprávný	k2eAgFnSc2d1	samosprávná
městské	městský	k2eAgFnSc2d1	městská
oblasti	oblast	k1gFnSc2	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Addis	Addis	k1gFnSc1	Addis
Abeba	Abeba	k1gFnSc1	Abeba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
sídlem	sídlo	k1gNnSc7	sídlo
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
městské	městský	k2eAgFnSc2d1	městská
samosprávné	samosprávný	k2eAgFnSc2d1	samosprávná
oblasti	oblast	k1gFnSc2	oblast
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
státu	stát	k1gInSc3	stát
Oromie	Oromie	k1gFnSc2	Oromie
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgInPc4d1	další
státy	stát	k1gInPc4	stát
patří	patřit	k5eAaImIp3nS	patřit
Tigraj	Tigraj	k1gFnSc1	Tigraj
<g/>
,	,	kIx,	,
Stát	stát	k1gInSc1	stát
jižních	jižní	k2eAgInPc2d1	jižní
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
národností	národnost	k1gFnPc2	národnost
a	a	k8xC	a
lidu	lid	k1gInSc2	lid
<g/>
,	,	kIx,	,
Afarsko	Afarsko	k1gNnSc1	Afarsko
<g/>
,	,	kIx,	,
Somalia	Somalia	k1gFnSc1	Somalia
<g/>
,	,	kIx,	,
Beningšangul-Gumuz	Beningšangul-Gumuz	k1gInSc1	Beningšangul-Gumuz
<g/>
,	,	kIx,	,
Gambella	Gambella	k1gFnSc1	Gambella
<g/>
,	,	kIx,	,
Hararský	Hararský	k2eAgInSc1d1	Hararský
stát	stát	k1gInSc1	stát
a	a	k8xC	a
Amharsko	Amharsko	k1gNnSc1	Amharsko
<g/>
.	.	kIx.	.
</s>
<s>
Druhým	druhý	k4xOgInSc7	druhý
samosprávným	samosprávný	k2eAgInSc7d1	samosprávný
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Dire	Dire	k1gFnSc1	Dire
Dawa	Dawa	k1gFnSc1	Dawa
<g/>
.	.	kIx.	.
</s>
<s>
Státy	stát	k1gInPc1	stát
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
okresy	okres	k1gInPc4	okres
(	(	kIx(	(
<g/>
wereda	wereda	k1gMnSc1	wereda
<g/>
)	)	kIx)	)
a	a	k8xC	a
obce	obec	k1gFnSc2	obec
(	(	kIx(	(
<g/>
kebele	kebele	k6eAd1	kebele
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
nejvýznamnější	významný	k2eAgNnPc4d3	nejvýznamnější
města	město	k1gNnPc4	město
patří	patřit	k5eAaImIp3nS	patřit
Addis	Addis	k1gFnSc1	Addis
Abeba	Abeba	k1gFnSc1	Abeba
(	(	kIx(	(
<g/>
zhruba	zhruba	k6eAd1	zhruba
3	[number]	k4	3
147	[number]	k4	147
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
pak	pak	k6eAd1	pak
Dire	Dire	k1gFnSc1	Dire
Dawa	Dawa	k1gFnSc1	Dawa
(	(	kIx(	(
<g/>
306	[number]	k4	306
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Adama	Adam	k1gMnSc4	Adam
(	(	kIx(	(
<g/>
dříve	dříve	k6eAd2	dříve
Nazareth	Nazareth	k1gInSc1	Nazareth
<g/>
;	;	kIx,	;
251	[number]	k4	251
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gondar	Gondar	k1gMnSc1	Gondar
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
213	[number]	k4	213
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Džima	Džima	k1gFnSc1	Džima
(	(	kIx(	(
<g/>
174	[number]	k4	174
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Desje	Desje	k1gFnSc1	Desje
(	(	kIx(	(
<g/>
186	[number]	k4	186
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Mekelle	Mekelle	k1gFnSc1	Mekelle
(	(	kIx(	(
<g/>
184	[number]	k4	184
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Bahir	Bahir	k1gInSc1	Bahir
Dar	dar	k1gInSc1	dar
(	(	kIx(	(
<g/>
183	[number]	k4	183
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Awassa	Awassa	k1gFnSc1	Awassa
(	(	kIx(	(
<g/>
137	[number]	k4	137
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Harar	Harar	k1gMnSc1	Harar
(	(	kIx(	(
<g/>
131	[number]	k4	131
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
a	a	k8xC	a
Gambella	Gambella	k1gMnSc1	Gambella
(	(	kIx(	(
<g/>
33	[number]	k4	33
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zahraniční	zahraniční	k2eAgInPc1d1	zahraniční
vztahy	vztah	k1gInPc1	vztah
===	===	k?	===
</s>
</p>
<p>
<s>
Etiopie	Etiopie	k1gFnSc1	Etiopie
po	po	k7c4	po
dobu	doba	k1gFnSc4	doba
své	svůj	k3xOyFgFnSc2	svůj
existence	existence	k1gFnSc2	existence
udržovala	udržovat	k5eAaImAgFnS	udržovat
kontakty	kontakt	k1gInPc4	kontakt
s	s	k7c7	s
mnoha	mnoho	k4c7	mnoho
okolními	okolní	k2eAgInPc7d1	okolní
národy	národ	k1gInPc7	národ
i	i	k9	i
s	s	k7c7	s
evropskými	evropský	k2eAgInPc7d1	evropský
křesťanskými	křesťanský	k2eAgInPc7d1	křesťanský
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
významněji	významně	k6eAd2	významně
projevila	projevit	k5eAaPmAgFnS	projevit
na	na	k7c6	na
světové	světový	k2eAgFnSc6d1	světová
scéně	scéna	k1gFnSc6	scéna
roku	rok	k1gInSc2	rok
1896	[number]	k4	1896
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
etiopskému	etiopský	k2eAgMnSc3d1	etiopský
císaři	císař	k1gMnSc3	císař
Menelikovi	Menelik	k1gMnSc3	Menelik
II	II	kA	II
<g/>
.	.	kIx.	.
podařilo	podařit	k5eAaPmAgNnS	podařit
porazit	porazit	k5eAaPmF	porazit
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Adwy	Adwa	k1gFnSc2	Adwa
italská	italský	k2eAgNnPc1d1	italské
vojska	vojsko	k1gNnPc1	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1923	[number]	k4	1923
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
členem	člen	k1gMnSc7	člen
Společnosti	společnost	k1gFnSc2	společnost
národů	národ	k1gInPc2	národ
<g/>
,	,	kIx,	,
po	po	k7c6	po
druhé	druhý	k4xOgFnSc6	druhý
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
dění	dění	k1gNnSc6	dění
hrát	hrát	k5eAaImF	hrát
i	i	k9	i
významnější	významný	k2eAgFnSc4d2	významnější
pozici	pozice	k1gFnSc4	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
mimo	mimo	k7c4	mimo
jiné	jiné	k1gNnSc4	jiné
zakládajícím	zakládající	k2eAgMnSc7d1	zakládající
členem	člen	k1gMnSc7	člen
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
a	a	k8xC	a
etiopský	etiopský	k2eAgMnSc1d1	etiopský
císař	císař	k1gMnSc1	císař
Haile	Haile	k1gNnSc2	Haile
Selassie	Selassie	k1gFnSc2	Selassie
I.	I.	kA	I.
stál	stát	k5eAaImAgInS	stát
u	u	k7c2	u
zrodu	zrod	k1gInSc2	zrod
Organizace	organizace	k1gFnSc2	organizace
africké	africký	k2eAgFnSc2d1	africká
jednoty	jednota	k1gFnSc2	jednota
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
Africké	africký	k2eAgFnSc2d1	africká
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Etiopský	etiopský	k2eAgInSc1d1	etiopský
stát	stát	k1gInSc1	stát
se	se	k3xPyFc4	se
také	také	k9	také
účastnil	účastnit	k5eAaImAgInS	účastnit
misí	mise	k1gFnSc7	mise
OSN	OSN	kA	OSN
v	v	k7c6	v
Koreji	Korea	k1gFnSc6	Korea
a	a	k8xC	a
Kongu	Kongo	k1gNnSc6	Kongo
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
též	též	k9	též
v	v	k7c6	v
Burundi	Burundi	k1gNnSc6	Burundi
<g/>
,	,	kIx,	,
Libérii	Libérie	k1gFnSc6	Libérie
a	a	k8xC	a
Rwandě	Rwanda	k1gFnSc6	Rwanda
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
etiopský	etiopský	k2eAgMnSc1d1	etiopský
panovník	panovník	k1gMnSc1	panovník
se	se	k3xPyFc4	se
orientoval	orientovat	k5eAaBmAgMnS	orientovat
zejména	zejména	k9	zejména
na	na	k7c4	na
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
<g/>
,	,	kIx,	,
vláda	vláda	k1gFnSc1	vláda
dergu	derg	k1gInSc2	derg
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
po	po	k7c6	po
převratu	převrat	k1gInSc6	převrat
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
<g/>
,	,	kIx,	,
orientovala	orientovat	k5eAaBmAgFnS	orientovat
celou	celý	k2eAgFnSc4d1	celá
zemi	zem	k1gFnSc4	zem
směrem	směr	k1gInSc7	směr
k	k	k7c3	k
východnímu	východní	k2eAgInSc3d1	východní
bloku	blok	k1gInSc3	blok
komunistických	komunistický	k2eAgFnPc2d1	komunistická
zemí	zem	k1gFnPc2	zem
a	a	k8xC	a
Sovětskému	sovětský	k2eAgInSc3d1	sovětský
svazu	svaz	k1gInSc3	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1991	[number]	k4	1991
obnovila	obnovit	k5eAaPmAgFnS	obnovit
nová	nový	k2eAgFnSc1d1	nová
etiopská	etiopský	k2eAgFnSc1d1	etiopská
vláda	vláda	k1gFnSc1	vláda
kontakty	kontakt	k1gInPc4	kontakt
se	s	k7c7	s
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
a	a	k8xC	a
obecně	obecně	k6eAd1	obecně
udržuje	udržovat	k5eAaImIp3nS	udržovat
mírové	mírový	k2eAgInPc4d1	mírový
kontakty	kontakt	k1gInPc4	kontakt
s	s	k7c7	s
většinou	většina	k1gFnSc7	většina
afrických	africký	k2eAgFnPc2d1	africká
a	a	k8xC	a
evropských	evropský	k2eAgFnPc2d1	Evropská
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Výjimku	výjimka	k1gFnSc4	výjimka
představuje	představovat	k5eAaImIp3nS	představovat
Eritrea	Eritrea	k1gFnSc1	Eritrea
<g/>
,	,	kIx,	,
s	s	k7c7	s
níž	jenž	k3xRgFnSc7	jenž
dosud	dosud	k6eAd1	dosud
nevyřešené	vyřešený	k2eNgInPc1d1	nevyřešený
spory	spor	k1gInPc1	spor
ohledně	ohledně	k7c2	ohledně
hranic	hranice	k1gFnPc2	hranice
vedly	vést	k5eAaImAgFnP	vést
k	k	k7c3	k
vypuknutí	vypuknutí	k1gNnSc3	vypuknutí
eritrejsko-etiopské	eritrejskotiopský	k2eAgFnSc2d1	eritrejsko-etiopský
války	válka	k1gFnSc2	válka
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1998	[number]	k4	1998
až	až	k9	až
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
Vztahy	vztah	k1gInPc1	vztah
mezi	mezi	k7c7	mezi
těmito	tento	k3xDgInPc7	tento
dvěma	dva	k4xCgInPc7	dva
státy	stát	k1gInPc7	stát
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
nadále	nadále	k6eAd1	nadále
chladné	chladný	k2eAgFnPc1d1	chladná
<g/>
,	,	kIx,	,
po	po	k7c6	po
obou	dva	k4xCgFnPc6	dva
stranách	strana	k1gFnPc6	strana
hranice	hranice	k1gFnSc2	hranice
udržují	udržovat	k5eAaImIp3nP	udržovat
obě	dva	k4xCgFnPc1	dva
země	zem	k1gFnPc1	zem
vojska	vojsko	k1gNnSc2	vojsko
o	o	k7c6	o
síle	síla	k1gFnSc6	síla
asi	asi	k9	asi
100	[number]	k4	100
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
.	.	kIx.	.
</s>
<s>
Etiopie	Etiopie	k1gFnSc1	Etiopie
se	se	k3xPyFc4	se
také	také	k9	také
snaží	snažit	k5eAaImIp3nS	snažit
svého	svůj	k3xOyFgMnSc4	svůj
souseda	soused	k1gMnSc4	soused
konstantně	konstantně	k6eAd1	konstantně
držet	držet	k5eAaImF	držet
v	v	k7c6	v
diplomatické	diplomatický	k2eAgFnSc6d1	diplomatická
izolaci	izolace	k1gFnSc6	izolace
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
těchto	tento	k3xDgFnPc2	tento
událostí	událost	k1gFnPc2	událost
udržuje	udržovat	k5eAaImIp3nS	udržovat
Etiopie	Etiopie	k1gFnSc1	Etiopie
dobré	dobrá	k1gFnSc2	dobrá
vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
Džibutskem	Džibutsek	k1gInSc7	Džibutsek
a	a	k8xC	a
Súdánem	Súdán	k1gInSc7	Súdán
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
si	se	k3xPyFc3	se
tak	tak	k6eAd1	tak
zajistila	zajistit	k5eAaPmAgFnS	zajistit
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
mořským	mořský	k2eAgInPc3d1	mořský
přístavům	přístav	k1gInPc3	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
zdrojem	zdroj	k1gInSc7	zdroj
napětí	napětí	k1gNnSc2	napětí
je	být	k5eAaImIp3nS	být
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Somálsku	Somálsko	k1gNnSc6	Somálsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Etiopie	Etiopie	k1gFnSc1	Etiopie
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
během	během	k7c2	během
roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
proti	proti	k7c3	proti
Svazu	svaz	k1gInSc3	svaz
islámských	islámský	k2eAgInPc2d1	islámský
soudů	soud	k1gInPc2	soud
a	a	k8xC	a
do	do	k7c2	do
ledna	leden	k1gInSc2	leden
2009	[number]	k4	2009
prováděla	provádět	k5eAaImAgFnS	provádět
v	v	k7c6	v
somálské	somálský	k2eAgFnSc6d1	Somálská
oblasti	oblast	k1gFnSc6	oblast
vojenské	vojenský	k2eAgFnSc2d1	vojenská
operace	operace	k1gFnSc2	operace
<g/>
,	,	kIx,	,
během	během	k7c2	během
nichž	jenž	k3xRgMnPc2	jenž
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
i	i	k9	i
proti	proti	k7c3	proti
etiopským	etiopský	k2eAgMnPc3d1	etiopský
disidentům	disident	k1gMnPc3	disident
skrývajícím	skrývající	k2eAgMnPc3d1	skrývající
se	se	k3xPyFc4	se
na	na	k7c6	na
somálském	somálský	k2eAgNnSc6d1	somálské
území	území	k1gNnSc6	území
<g/>
.	.	kIx.	.
</s>
<s>
Etiopie	Etiopie	k1gFnSc1	Etiopie
nadále	nadále	k6eAd1	nadále
považuje	považovat	k5eAaImIp3nS	považovat
probíhající	probíhající	k2eAgInSc4d1	probíhající
konflikt	konflikt	k1gInSc4	konflikt
v	v	k7c6	v
Somálsku	Somálsko	k1gNnSc6	Somálsko
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
z	z	k7c2	z
důležitých	důležitý	k2eAgInPc2d1	důležitý
bezpečnostních	bezpečnostní	k2eAgInPc2d1	bezpečnostní
faktorů	faktor	k1gInPc2	faktor
v	v	k7c6	v
regionu	region	k1gInSc6	region
<g/>
.	.	kIx.	.
</s>
<s>
Udržuje	udržovat	k5eAaImIp3nS	udržovat
také	také	k9	také
vztahy	vztah	k1gInPc4	vztah
se	s	k7c7	s
Somalilandem	Somaliland	k1gInSc7	Somaliland
<g/>
,	,	kIx,	,
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
odtržených	odtržený	k2eAgFnPc2d1	odtržená
částí	část	k1gFnPc2	část
Somálska	Somálsko	k1gNnSc2	Somálsko
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
Etiopii	Etiopie	k1gFnSc4	Etiopie
využívat	využívat	k5eAaPmF	využívat
svých	svůj	k3xOyFgInPc2	svůj
přístavů	přístav	k1gInPc2	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Počátek	počátek	k1gInSc1	počátek
války	válka	k1gFnSc2	válka
proti	proti	k7c3	proti
teroru	teror	k1gInSc3	teror
znamenal	znamenat	k5eAaImAgInS	znamenat
upevnění	upevnění	k1gNnSc4	upevnění
vztahů	vztah	k1gInPc2	vztah
s	s	k7c7	s
USA	USA	kA	USA
i	i	k8xC	i
západoevropskými	západoevropský	k2eAgInPc7d1	západoevropský
státy	stát	k1gInPc7	stát
–	–	k?	–
Etiopie	Etiopie	k1gFnSc1	Etiopie
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
považována	považován	k2eAgNnPc1d1	považováno
za	za	k7c2	za
důležitého	důležitý	k2eAgNnSc2d1	důležité
spojence	spojenec	k1gMnPc4	spojenec
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
islámskému	islámský	k2eAgInSc3d1	islámský
fundamentalismu	fundamentalismus	k1gInSc3	fundamentalismus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Vztahy	vztah	k1gInPc1	vztah
s	s	k7c7	s
Československem	Československo	k1gNnSc7	Československo
a	a	k8xC	a
Českem	Česko	k1gNnSc7	Česko
====	====	k?	====
</s>
</p>
<p>
<s>
Diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
vztahy	vztah	k1gInPc4	vztah
navázala	navázat	k5eAaPmAgFnS	navázat
Etiopie	Etiopie	k1gFnSc1	Etiopie
s	s	k7c7	s
Československem	Československo	k1gNnSc7	Československo
po	po	k7c6	po
první	první	k4xOgFnSc6	první
světové	světový	k2eAgFnSc6d1	světová
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
Zájmy	zájem	k1gInPc1	zájem
Československa	Československo	k1gNnSc2	Československo
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
zastupovalo	zastupovat	k5eAaImAgNnS	zastupovat
francouzské	francouzský	k2eAgNnSc1d1	francouzské
velvyslanectví	velvyslanectví	k1gNnSc1	velvyslanectví
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yQnSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
obchodní	obchodní	k2eAgFnSc2d1	obchodní
výměny	výměna	k1gFnSc2	výměna
<g/>
,	,	kIx,	,
v	v	k7c6	v
meziválečném	meziválečný	k2eAgNnSc6d1	meziválečné
období	období	k1gNnSc6	období
byly	být	k5eAaImAgFnP	být
do	do	k7c2	do
Etiopie	Etiopie	k1gFnSc2	Etiopie
dodávány	dodáván	k2eAgFnPc4d1	dodávána
především	především	k6eAd1	především
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
Československo	Československo	k1gNnSc1	Československo
zřídilo	zřídit	k5eAaPmAgNnS	zřídit
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
vyslanectví	vyslanectví	k1gNnSc2	vyslanectví
<g/>
,	,	kIx,	,
o	o	k7c4	o
čtyři	čtyři	k4xCgInPc4	čtyři
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
velvyslanectví	velvyslanectví	k1gNnSc1	velvyslanectví
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1959	[number]	k4	1959
byla	být	k5eAaImAgFnS	být
rovněž	rovněž	k9	rovněž
podepsána	podepsán	k2eAgFnSc1d1	podepsána
Smlouva	smlouva	k1gFnSc1	smlouva
o	o	k7c4	o
přátelství	přátelství	k1gNnSc4	přátelství
a	a	k8xC	a
Dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
kulturní	kulturní	k2eAgFnSc6d1	kulturní
<g/>
,	,	kIx,	,
vědecké	vědecký	k2eAgFnSc3d1	vědecká
a	a	k8xC	a
technické	technický	k2eAgFnSc3d1	technická
pomoci	pomoc	k1gFnSc3	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
země	zem	k1gFnSc2	zem
poté	poté	k6eAd1	poté
přišli	přijít	k5eAaPmAgMnP	přijít
českoslovenští	československý	k2eAgMnPc1d1	československý
odborníci	odborník	k1gMnPc1	odborník
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
spolupráci	spolupráce	k1gFnSc3	spolupráce
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
státy	stát	k1gInPc7	stát
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
několik	několik	k4yIc1	několik
továrních	tovární	k2eAgInPc2d1	tovární
podniků	podnik	k1gInPc2	podnik
a	a	k8xC	a
v	v	k7c6	v
Hararu	Harar	k1gInSc6	Harar
byla	být	k5eAaImAgFnS	být
postavena	postaven	k2eAgFnSc1d1	postavena
s	s	k7c7	s
československou	československý	k2eAgFnSc7d1	Československá
pomocí	pomoc	k1gFnSc7	pomoc
také	také	k9	také
nemocnice	nemocnice	k1gFnSc1	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2004	[number]	k4	2004
zde	zde	k6eAd1	zde
probíhá	probíhat	k5eAaImIp3nS	probíhat
projekt	projekt	k1gInSc1	projekt
Postavme	postavit	k5eAaPmRp1nP	postavit
školu	škola	k1gFnSc4	škola
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
české	český	k2eAgFnSc2d1	Česká
humanitární	humanitární	k2eAgFnSc2d1	humanitární
organizace	organizace	k1gFnSc2	organizace
Člověk	člověk	k1gMnSc1	člověk
v	v	k7c6	v
tísni	tíseň	k1gFnSc6	tíseň
a	a	k8xC	a
skautské	skautský	k2eAgFnSc2d1	skautská
organizace	organizace	k1gFnSc2	organizace
Junák	junák	k1gMnSc1	junák
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2007	[number]	k4	2007
byla	být	k5eAaImAgFnS	být
mezi	mezi	k7c7	mezi
Českem	Česko	k1gNnSc7	Česko
a	a	k8xC	a
Etiopií	Etiopie	k1gFnSc7	Etiopie
podepsána	podepsán	k2eAgFnSc1d1	podepsána
Dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c4	o
zamezení	zamezení	k1gNnSc4	zamezení
dvojímu	dvojí	k4xRgInSc3	dvojí
zdanění	zdanění	k1gNnSc3	zdanění
a	a	k8xC	a
prevenci	prevence	k1gFnSc3	prevence
daňových	daňový	k2eAgInPc2d1	daňový
úniků	únik	k1gInPc2	únik
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
rozdíly	rozdíl	k1gInPc4	rozdíl
v	v	k7c6	v
hospodářské	hospodářský	k2eAgFnSc6d1	hospodářská
politice	politika	k1gFnSc6	politika
obou	dva	k4xCgInPc2	dva
států	stát	k1gInPc2	stát
však	však	k9	však
dosud	dosud	k6eAd1	dosud
nebyla	být	k5eNaImAgFnS	být
podepsána	podepsat	k5eAaPmNgFnS	podepsat
již	již	k6eAd1	již
připravená	připravený	k2eAgFnSc1d1	připravená
Dohoda	dohoda	k1gFnSc1	dohoda
o	o	k7c6	o
podpoře	podpora	k1gFnSc6	podpora
a	a	k8xC	a
vzájemné	vzájemný	k2eAgFnSc3d1	vzájemná
ochraně	ochrana	k1gFnSc3	ochrana
investic	investice	k1gFnPc2	investice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
českých	český	k2eAgFnPc6d1	Česká
vysokých	vysoký	k2eAgFnPc6d1	vysoká
školách	škola	k1gFnPc6	škola
také	také	k6eAd1	také
studovalo	studovat	k5eAaImAgNnS	studovat
několik	několik	k4yIc1	několik
generací	generace	k1gFnPc2	generace
etiopských	etiopský	k2eAgMnPc2d1	etiopský
studentů	student	k1gMnPc2	student
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
vzájemné	vzájemný	k2eAgInPc4d1	vzájemný
kulturní	kulturní	k2eAgInPc4d1	kulturní
a	a	k8xC	a
vědecké	vědecký	k2eAgInPc4d1	vědecký
styky	styk	k1gInPc4	styk
nejsou	být	k5eNaImIp3nP	být
velmi	velmi	k6eAd1	velmi
rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
občany	občan	k1gMnPc4	občan
ČR	ČR	kA	ČR
platí	platit	k5eAaImIp3nS	platit
vízová	vízový	k2eAgFnSc1d1	vízová
povinnost	povinnost	k1gFnSc1	povinnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Obrana	obrana	k1gFnSc1	obrana
a	a	k8xC	a
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
===	===	k?	===
</s>
</p>
<p>
<s>
Etiopská	etiopský	k2eAgFnSc1d1	etiopská
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
pod	pod	k7c7	pod
souhrnným	souhrnný	k2eAgInSc7d1	souhrnný
názvem	název	k1gInSc7	název
Etiopské	etiopský	k2eAgFnSc2d1	etiopská
národní	národní	k2eAgFnSc2d1	národní
obranné	obranný	k2eAgFnSc2d1	obranná
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
sestává	sestávat	k5eAaImIp3nS	sestávat
zhruba	zhruba	k6eAd1	zhruba
z	z	k7c2	z
200	[number]	k4	200
000	[number]	k4	000
mužů	muž	k1gMnPc2	muž
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ji	on	k3xPp3gFnSc4	on
činí	činit	k5eAaImIp3nS	činit
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
armád	armáda	k1gFnPc2	armáda
na	na	k7c6	na
africkém	africký	k2eAgInSc6d1	africký
kontinentu	kontinent	k1gInSc6	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Skládá	skládat	k5eAaImIp3nS	skládat
se	se	k3xPyFc4	se
z	z	k7c2	z
pozemní	pozemní	k2eAgFnSc2d1	pozemní
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
letectva	letectvo	k1gNnSc2	letectvo
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
etiopské	etiopský	k2eAgNnSc1d1	etiopské
loďstvo	loďstvo	k1gNnSc1	loďstvo
bylo	být	k5eAaImAgNnS	být
přenecháno	přenechat	k5eAaPmNgNnS	přenechat
Eritreji	Eritrea	k1gFnSc6	Eritrea
po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
odtržení	odtržení	k1gNnSc6	odtržení
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
pádu	pád	k1gInSc6	pád
komunistického	komunistický	k2eAgInSc2d1	komunistický
režimu	režim	k1gInSc2	režim
byla	být	k5eAaImAgFnS	být
původní	původní	k2eAgFnSc1d1	původní
armáda	armáda	k1gFnSc1	armáda
rozpuštěna	rozpustit	k5eAaPmNgFnS	rozpustit
a	a	k8xC	a
nahrazena	nahradit	k5eAaPmNgFnS	nahradit
guerillovými	guerillový	k2eAgFnPc7d1	guerillová
jednotkami	jednotka	k1gFnPc7	jednotka
povstalců	povstalec	k1gMnPc2	povstalec
<g/>
.	.	kIx.	.
</s>
<s>
Nová	nový	k2eAgFnSc1d1	nová
armáda	armáda	k1gFnSc1	armáda
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
prochází	procházet	k5eAaImIp3nS	procházet
s	s	k7c7	s
americkou	americký	k2eAgFnSc7d1	americká
pomocí	pomoc	k1gFnSc7	pomoc
postupnou	postupný	k2eAgFnSc7d1	postupná
transformací	transformace	k1gFnSc7	transformace
v	v	k7c4	v
moderní	moderní	k2eAgNnSc4d1	moderní
vojsko	vojsko	k1gNnSc4	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Plánem	plán	k1gInSc7	plán
vlády	vláda	k1gFnSc2	vláda
bylo	být	k5eAaImAgNnS	být
vytvořit	vytvořit	k5eAaPmF	vytvořit
multietnickou	multietnický	k2eAgFnSc4d1	multietnická
armádu	armáda	k1gFnSc4	armáda
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
té	ten	k3xDgFnSc3	ten
stále	stále	k6eAd1	stále
dominují	dominovat	k5eAaImIp3nP	dominovat
Tigrajové	Tigraj	k1gMnPc1	Tigraj
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
coby	coby	k?	coby
členové	člen	k1gMnPc1	člen
Tigrajské	Tigrajský	k2eAgFnSc2d1	Tigrajský
lidově	lidově	k6eAd1	lidově
osvobozenecké	osvobozenecký	k2eAgFnSc2d1	osvobozenecká
fronty	fronta	k1gFnSc2	fronta
spojenci	spojenec	k1gMnPc7	spojenec
EPRDF	EPRDF	kA	EPRDF
<g/>
.	.	kIx.	.
</s>
<s>
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
služba	služba	k1gFnSc1	služba
je	být	k5eAaImIp3nS	být
postavena	postaven	k2eAgFnSc1d1	postavena
na	na	k7c6	na
dobrovolnickém	dobrovolnický	k2eAgInSc6d1	dobrovolnický
principu	princip	k1gInSc6	princip
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nutnosti	nutnost	k1gFnSc2	nutnost
je	být	k5eAaImIp3nS	být
nastoupení	nastoupení	k1gNnSc1	nastoupení
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
povinné	povinný	k2eAgNnSc1d1	povinné
<g/>
.	.	kIx.	.
</s>
<s>
Minimální	minimální	k2eAgInSc1d1	minimální
věk	věk	k1gInSc1	věk
pro	pro	k7c4	pro
vstup	vstup	k1gInSc4	vstup
do	do	k7c2	do
armády	armáda	k1gFnSc2	armáda
je	být	k5eAaImIp3nS	být
18	[number]	k4	18
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Armáda	armáda	k1gFnSc1	armáda
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
loajální	loajální	k2eAgFnSc3d1	loajální
civilní	civilní	k2eAgFnSc3d1	civilní
správě	správa	k1gFnSc3	správa
<g/>
,	,	kIx,	,
proti	proti	k7c3	proti
lidovým	lidový	k2eAgInPc3d1	lidový
nepokojům	nepokoj	k1gInPc3	nepokoj
či	či	k8xC	či
povstalcům	povstalec	k1gMnPc3	povstalec
postupuje	postupovat	k5eAaImIp3nS	postupovat
často	často	k6eAd1	často
brutálním	brutální	k2eAgInSc7d1	brutální
způsobem	způsob	k1gInSc7	způsob
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
obvinění	obvinění	k1gNnSc3	obvinění
armádních	armádní	k2eAgFnPc2d1	armádní
složek	složka	k1gFnPc2	složka
ze	z	k7c2	z
zločinů	zločin	k1gInPc2	zločin
proti	proti	k7c3	proti
lidskosti	lidskost	k1gFnSc3	lidskost
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
společným	společný	k2eAgInPc3d1	společný
zájmům	zájem	k1gInPc3	zájem
Etiopie	Etiopie	k1gFnSc2	Etiopie
a	a	k8xC	a
USA	USA	kA	USA
prohlubují	prohlubovat	k5eAaImIp3nP	prohlubovat
tyto	tento	k3xDgInPc1	tento
dva	dva	k4xCgInPc1	dva
státy	stát	k1gInPc1	stát
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
spolupráci	spolupráce	k1gFnSc4	spolupráce
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
vynaložila	vynaložit	k5eAaPmAgFnS	vynaložit
Etiopie	Etiopie	k1gFnSc1	Etiopie
na	na	k7c4	na
vojenské	vojenský	k2eAgFnPc4d1	vojenská
potřeby	potřeba	k1gFnPc4	potřeba
1,2	[number]	k4	1,2
%	%	kIx~	%
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2005	[number]	k4	2005
Etiopie	Etiopie	k1gFnSc1	Etiopie
vlastnila	vlastnit	k5eAaImAgFnS	vlastnit
250	[number]	k4	250
tanků	tank	k1gInPc2	tank
<g/>
,	,	kIx,	,
400	[number]	k4	400
vozidel	vozidlo	k1gNnPc2	vozidlo
<g/>
,	,	kIx,	,
400	[number]	k4	400
kusů	kus	k1gInPc2	kus
mobilního	mobilní	k2eAgNnSc2d1	mobilní
dělostřelectva	dělostřelectvo	k1gNnSc2	dělostřelectvo
<g/>
,	,	kIx,	,
50	[number]	k4	50
raketometů	raketomet	k1gInPc2	raketomet
a	a	k8xC	a
48	[number]	k4	48
letadel	letadlo	k1gNnPc2	letadlo
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgMnPc4	jenž
patřila	patřit	k5eAaImAgFnS	patřit
letadla	letadlo	k1gNnSc2	letadlo
Su-	Su-	k1gFnSc2	Su-
<g/>
27	[number]	k4	27
a	a	k8xC	a
MiG-	MiG-	k1gMnSc1	MiG-
<g/>
21	[number]	k4	21
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
armádní	armádní	k2eAgNnSc1d1	armádní
vybavení	vybavení	k1gNnSc1	vybavení
bylo	být	k5eAaImAgNnS	být
většinou	většinou	k6eAd1	většinou
zakoupeno	zakoupit	k5eAaPmNgNnS	zakoupit
od	od	k7c2	od
ruských	ruský	k2eAgMnPc2d1	ruský
dodavatelů	dodavatel	k1gMnPc2	dodavatel
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
1999	[number]	k4	1999
a	a	k8xC	a
2000	[number]	k4	2000
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2011	[number]	k4	2011
podepsala	podepsat	k5eAaPmAgFnS	podepsat
Etiopie	Etiopie	k1gFnSc1	Etiopie
obchodní	obchodní	k2eAgFnSc4d1	obchodní
smlouvu	smlouva	k1gFnSc4	smlouva
s	s	k7c7	s
Ukrajinou	Ukrajina	k1gFnSc7	Ukrajina
na	na	k7c4	na
nákup	nákup	k1gInSc4	nákup
dalších	další	k2eAgInPc2d1	další
200	[number]	k4	200
tanků	tank	k1gInPc2	tank
T-	T-	k1gFnSc2	T-
<g/>
72	[number]	k4	72
<g/>
.	.	kIx.	.
</s>
<s>
Téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
získala	získat	k5eAaPmAgFnS	získat
izraelská	izraelský	k2eAgFnSc1d1	izraelská
firma	firma	k1gFnSc1	firma
BlueBird	BlueBirda	k1gFnPc2	BlueBirda
od	od	k7c2	od
etiopské	etiopský	k2eAgFnSc2d1	etiopská
vlády	vláda	k1gFnSc2	vláda
kontrakt	kontrakt	k1gInSc1	kontrakt
na	na	k7c6	na
dodání	dodání	k1gNnSc6	dodání
bezpilotních	bezpilotní	k2eAgInPc2d1	bezpilotní
letounů	letoun	k1gInPc2	letoun
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Policie	policie	k1gFnSc1	policie
je	být	k5eAaImIp3nS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c6	na
federálním	federální	k2eAgInSc6d1	federální
základě	základ	k1gInSc6	základ
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
však	však	k9	však
její	její	k3xOp3gMnPc1	její
činitelé	činitel	k1gMnPc1	činitel
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
regionech	region	k1gInPc6	region
místním	místní	k2eAgFnPc3d1	místní
institucím	instituce	k1gFnPc3	instituce
nezodpovídají	zodpovídat	k5eNaImIp3nP	zodpovídat
<g/>
.	.	kIx.	.
</s>
<s>
Etiopské	etiopský	k2eAgFnPc1d1	etiopská
policejní	policejní	k2eAgFnPc1d1	policejní
síly	síla	k1gFnPc1	síla
také	také	k9	také
postrádají	postrádat	k5eAaImIp3nP	postrádat
profesionalitu	profesionalita	k1gFnSc4	profesionalita
a	a	k8xC	a
trpí	trpět	k5eAaImIp3nP	trpět
silnou	silný	k2eAgFnSc7d1	silná
korupcí	korupce	k1gFnSc7	korupce
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
armáda	armáda	k1gFnSc1	armáda
se	se	k3xPyFc4	se
policie	policie	k1gFnSc1	policie
často	často	k6eAd1	často
uchyluje	uchylovat	k5eAaImIp3nS	uchylovat
k	k	k7c3	k
nelegálním	legální	k2eNgFnPc3d1	nelegální
praktikám	praktika	k1gFnPc3	praktika
jako	jako	k8xS	jako
je	být	k5eAaImIp3nS	být
svévolné	svévolný	k2eAgNnSc4d1	svévolné
zatčení	zatčení	k1gNnSc4	zatčení
<g/>
,	,	kIx,	,
týrání	týrání	k1gNnSc4	týrání
<g/>
,	,	kIx,	,
vraždy	vražda	k1gFnPc4	vražda
<g/>
,	,	kIx,	,
nepovolené	povolený	k2eNgNnSc4d1	nepovolené
prohledávání	prohledávání	k1gNnSc4	prohledávání
a	a	k8xC	a
podobně	podobně	k6eAd1	podobně
<g/>
.	.	kIx.	.
<g/>
Za	za	k7c4	za
boj	boj	k1gInSc4	boj
s	s	k7c7	s
terorismem	terorismus	k1gInSc7	terorismus
<g/>
,	,	kIx,	,
výzvědnou	výzvědný	k2eAgFnSc7d1	výzvědná
činnost	činnost	k1gFnSc4	činnost
<g/>
,	,	kIx,	,
bezpečnost	bezpečnost	k1gFnSc4	bezpečnost
hranic	hranice	k1gFnPc2	hranice
a	a	k8xC	a
kriminální	kriminální	k2eAgNnSc4d1	kriminální
vyšetřování	vyšetřování	k1gNnSc4	vyšetřování
zodpovídá	zodpovídat	k5eAaImIp3nS	zodpovídat
etiopská	etiopský	k2eAgFnSc1d1	etiopská
Národní	národní	k2eAgFnSc1d1	národní
výzvědná	výzvědný	k2eAgFnSc1d1	výzvědná
a	a	k8xC	a
bezpečnostní	bezpečnostní	k2eAgFnSc1d1	bezpečnostní
služba	služba	k1gFnSc1	služba
(	(	kIx(	(
<g/>
NISS	NISS	kA	NISS
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
zahraničí	zahraničí	k1gNnSc2	zahraničí
USA	USA	kA	USA
si	se	k3xPyFc3	se
etiopská	etiopský	k2eAgFnSc1d1	etiopská
vláda	vláda	k1gFnSc1	vláda
většinou	většina	k1gFnSc7	většina
udržuje	udržovat	k5eAaImIp3nS	udržovat
dohled	dohled	k1gInSc4	dohled
i	i	k9	i
nad	nad	k7c7	nad
touto	tento	k3xDgFnSc7	tento
organizací	organizace	k1gFnSc7	organizace
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
vyskytly	vyskytnout	k5eAaPmAgFnP	vyskytnout
se	s	k7c7	s
případy	případ	k1gInPc7	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bezpečnostní	bezpečnostní	k2eAgFnPc1d1	bezpečnostní
síly	síla	k1gFnPc1	síla
jednaly	jednat	k5eAaImAgFnP	jednat
na	na	k7c4	na
vlastní	vlastní	k2eAgFnSc4d1	vlastní
pěst	pěst	k1gFnSc4	pěst
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
některých	některý	k3yIgInPc6	některý
regionech	region	k1gInPc6	region
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Oromii	Oromie	k1gFnSc4	Oromie
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
bezpečnostní	bezpečnostní	k2eAgFnPc1d1	bezpečnostní
složky	složka	k1gFnPc1	složka
chovaly	chovat	k5eAaImAgFnP	chovat
podle	podle	k7c2	podle
zprávy	zpráva	k1gFnSc2	zpráva
organizace	organizace	k1gFnSc2	organizace
Mezinárodní	mezinárodní	k2eAgFnSc2d1	mezinárodní
krizové	krizový	k2eAgFnSc2d1	krizová
skupiny	skupina	k1gFnSc2	skupina
jako	jako	k8xC	jako
stát	stát	k5eAaImF	stát
ve	v	k7c6	v
státě	stát	k1gInSc6	stát
<g/>
.	.	kIx.	.
</s>
<s>
Human	Human	k1gInSc1	Human
Rights	Rights	k1gInSc1	Rights
Watch	Watch	k1gInSc4	Watch
také	také	k9	také
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
zprávě	zpráva	k1gFnSc6	zpráva
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
vládní	vládní	k2eAgFnPc1d1	vládní
složky	složka	k1gFnPc1	složka
využívaly	využívat	k5eAaImAgFnP	využívat
několikrát	několikrát	k6eAd1	několikrát
NISS	NISS	kA	NISS
ke	k	k7c3	k
sledování	sledování	k1gNnSc3	sledování
<g/>
,	,	kIx,	,
nátlaku	nátlak	k1gInSc3	nátlak
a	a	k8xC	a
zastrašování	zastrašování	k1gNnSc4	zastrašování
proti	proti	k7c3	proti
lidem	člověk	k1gMnPc3	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
považováni	považován	k2eAgMnPc1d1	považován
za	za	k7c4	za
nesympatizující	sympatizující	k2eNgNnSc4d1	sympatizující
s	s	k7c7	s
režimem	režim	k1gInSc7	režim
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
proti	proti	k7c3	proti
případné	případný	k2eAgFnSc3d1	případná
opozici	opozice	k1gFnSc3	opozice
využívána	využíván	k2eAgFnSc1d1	využívána
síť	síť	k1gFnSc1	síť
placených	placený	k2eAgMnPc2d1	placený
informátorů	informátor	k1gMnPc2	informátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ekonomika	ekonomika	k1gFnSc1	ekonomika
==	==	k?	==
</s>
</p>
<p>
<s>
Ekonomika	ekonomika	k1gFnSc1	ekonomika
etiopského	etiopský	k2eAgInSc2d1	etiopský
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
jedny	jeden	k4xCgFnPc4	jeden
z	z	k7c2	z
nejchudších	chudý	k2eAgFnPc2d3	nejchudší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
činil	činit	k5eAaImAgInS	činit
hrubý	hrubý	k2eAgInSc1d1	hrubý
domácí	domácí	k2eAgInSc1d1	domácí
produkt	produkt	k1gInSc1	produkt
(	(	kIx(	(
<g/>
HDP	HDP	kA	HDP
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
parity	parita	k1gFnSc2	parita
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
86,12	[number]	k4	86,12
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
si	se	k3xPyFc3	se
udržuje	udržovat	k5eAaImIp3nS	udržovat
vysoký	vysoký	k2eAgInSc4d1	vysoký
ekonomický	ekonomický	k2eAgInSc4d1	ekonomický
růst	růst	k1gInSc4	růst
–	–	k?	–
roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
činil	činit	k5eAaImAgInS	činit
11,2	[number]	k4	11,2
%	%	kIx~	%
HDP	HDP	kA	HDP
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
10	[number]	k4	10
%	%	kIx~	%
<g/>
,	,	kIx,	,
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
klesl	klesnout	k5eAaPmAgInS	klesnout
na	na	k7c4	na
8	[number]	k4	8
%	%	kIx~	%
zřejmě	zřejmě	k6eAd1	zřejmě
kvůli	kvůli	k7c3	kvůli
světové	světový	k2eAgFnSc3d1	světová
ekonomické	ekonomický	k2eAgFnSc3d1	ekonomická
krizi	krize	k1gFnSc3	krize
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
do	do	k7c2	do
HDP	HDP	kA	HDP
na	na	k7c4	na
obyvatele	obyvatel	k1gMnPc4	obyvatel
v	v	k7c6	v
paritě	parita	k1gFnSc6	parita
kupní	kupní	k2eAgFnSc2d1	kupní
síly	síla	k1gFnSc2	síla
<g/>
,	,	kIx,	,
Etiopie	Etiopie	k1gFnSc1	Etiopie
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
s	s	k7c7	s
1000	[number]	k4	1000
dolarů	dolar	k1gInPc2	dolar
na	na	k7c4	na
hlavu	hlava	k1gFnSc4	hlava
213	[number]	k4	213
<g/>
.	.	kIx.	.
státem	stát	k1gInSc7	stát
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
85	[number]	k4	85
%	%	kIx~	%
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
pracuje	pracovat	k5eAaImIp3nS	pracovat
v	v	k7c6	v
zemědělství	zemědělství	k1gNnSc6	zemědělství
<g/>
,	,	kIx,	,
5	[number]	k4	5
%	%	kIx~	%
v	v	k7c6	v
průmyslu	průmysl	k1gInSc6	průmysl
a	a	k8xC	a
10	[number]	k4	10
%	%	kIx~	%
ve	v	k7c6	v
službách	služba	k1gFnPc6	služba
<g/>
.	.	kIx.	.
</s>
<s>
Vládnoucí	vládnoucí	k2eAgFnSc1d1	vládnoucí
strana	strana	k1gFnSc1	strana
si	se	k3xPyFc3	se
udržuje	udržovat	k5eAaImIp3nS	udržovat
silný	silný	k2eAgInSc4d1	silný
vliv	vliv	k1gInSc4	vliv
na	na	k7c6	na
hospodářství	hospodářství	k1gNnSc6	hospodářství
–	–	k?	–
veškerá	veškerý	k3xTgFnSc1	veškerý
půda	půda	k1gFnSc1	půda
patří	patřit	k5eAaImIp3nS	patřit
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ji	on	k3xPp3gFnSc4	on
pronajímá	pronajímat	k5eAaImIp3nS	pronajímat
<g/>
,	,	kIx,	,
soukromý	soukromý	k2eAgInSc1d1	soukromý
sektor	sektor	k1gInSc1	sektor
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgInSc4d1	malý
a	a	k8xC	a
bankovní	bankovní	k2eAgFnPc4d1	bankovní
instituce	instituce	k1gFnPc4	instituce
slabé	slabý	k2eAgFnPc4d1	slabá
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
Etiopii	Etiopie	k1gFnSc3	Etiopie
dominuje	dominovat	k5eAaImIp3nS	dominovat
malý	malý	k2eAgInSc1d1	malý
počet	počet	k1gInSc1	počet
firem	firma	k1gFnPc2	firma
se	s	k7c7	s
silnými	silný	k2eAgFnPc7d1	silná
vazbami	vazba	k1gFnPc7	vazba
na	na	k7c4	na
vládnoucí	vládnoucí	k2eAgFnSc4d1	vládnoucí
stranu	strana	k1gFnSc4	strana
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
EPRDF	EPRDF	kA	EPRDF
sama	sám	k3xTgFnSc1	sám
některé	některý	k3yIgFnPc4	některý
firmy	firma	k1gFnPc4	firma
vlastní	vlastní	k2eAgFnPc4d1	vlastní
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
mezinárodní	mezinárodní	k2eAgFnSc6d1	mezinárodní
ekonomické	ekonomický	k2eAgFnSc6d1	ekonomická
a	a	k8xC	a
humanitární	humanitární	k2eAgFnSc6d1	humanitární
pomoci	pomoc	k1gFnSc6	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
též	též	k9	též
využívá	využívat	k5eAaImIp3nS	využívat
světovou	světový	k2eAgFnSc4d1	světová
ekonomickou	ekonomický	k2eAgFnSc4d1	ekonomická
krizi	krize	k1gFnSc4	krize
k	k	k7c3	k
zdůvodnění	zdůvodnění	k1gNnSc3	zdůvodnění
zpomalení	zpomalení	k1gNnSc2	zpomalení
liberálních	liberální	k2eAgFnPc2d1	liberální
hospodářských	hospodářský	k2eAgFnPc2d1	hospodářská
a	a	k8xC	a
politických	politický	k2eAgFnPc2d1	politická
reforem	reforma	k1gFnPc2	reforma
<g/>
.	.	kIx.	.
</s>
<s>
Měnou	měna	k1gFnSc7	měna
je	být	k5eAaImIp3nS	být
etiopský	etiopský	k2eAgInSc1d1	etiopský
birr	birr	k1gInSc1	birr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
není	být	k5eNaImIp3nS	být
volně	volně	k6eAd1	volně
směnitelný	směnitelný	k2eAgMnSc1d1	směnitelný
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
vázán	vázat	k5eAaImNgMnS	vázat
na	na	k7c4	na
americký	americký	k2eAgInSc4d1	americký
dolar	dolar	k1gInSc4	dolar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
byl	být	k5eAaImAgInS	být
směnný	směnný	k2eAgInSc1d1	směnný
kurs	kurs	k1gInSc1	kurs
14,4	[number]	k4	14,4
birru	birr	k1gInSc2	birr
za	za	k7c4	za
dolar	dolar	k1gInSc4	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Inflace	inflace	k1gFnSc1	inflace
dosahovala	dosahovat	k5eAaImAgFnS	dosahovat
k	k	k7c3	k
roku	rok	k1gInSc3	rok
2010	[number]	k4	2010
celkem	celek	k1gInSc7	celek
8	[number]	k4	8
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
době	doba	k1gFnSc6	doba
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
investovaly	investovat	k5eAaBmAgInP	investovat
nejvýrazněji	výrazně	k6eAd3	výrazně
zejména	zejména	k9	zejména
Indie	Indie	k1gFnSc1	Indie
a	a	k8xC	a
Čína	Čína	k1gFnSc1	Čína
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Číny	Čína	k1gFnSc2	Čína
také	také	k9	také
směřuje	směřovat	k5eAaImIp3nS	směřovat
nejvíce	nejvíce	k6eAd1	nejvíce
etiopského	etiopský	k2eAgInSc2d1	etiopský
exportu	export	k1gInSc2	export
(	(	kIx(	(
<g/>
13,3	[number]	k4	13,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Následuje	následovat	k5eAaImIp3nS	následovat
Německo	Německo	k1gNnSc1	Německo
(	(	kIx(	(
<g/>
10	[number]	k4	10
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
(	(	kIx(	(
<g/>
7,6	[number]	k4	7,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgFnSc2d1	americká
(	(	kIx(	(
<g/>
7,4	[number]	k4	7,4
<g/>
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Nizozemsko	Nizozemsko	k1gNnSc1	Nizozemsko
(	(	kIx(	(
<g/>
6,5	[number]	k4	6,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Súdán	Súdán	k1gInSc1	Súdán
(	(	kIx(	(
<g/>
4,5	[number]	k4	4,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Belgie	Belgie	k1gFnSc1	Belgie
(	(	kIx(	(
<g/>
4,1	[number]	k4	4,1
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
vývozním	vývozní	k2eAgInSc7d1	vývozní
artiklem	artikl	k1gInSc7	artikl
je	být	k5eAaImIp3nS	být
káva	káva	k1gFnSc1	káva
<g/>
,	,	kIx,	,
vyváží	vyvážet	k5eAaImIp3nS	vyvážet
se	se	k3xPyFc4	se
také	také	k9	také
kůže	kůže	k1gFnSc2	kůže
a	a	k8xC	a
kožené	kožený	k2eAgInPc4d1	kožený
výrobky	výrobek	k1gInPc4	výrobek
<g/>
,	,	kIx,	,
živá	živý	k2eAgNnPc4d1	živé
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
olejnice	olejnice	k1gFnPc4	olejnice
<g/>
,	,	kIx,	,
zlato	zlato	k1gNnSc4	zlato
a	a	k8xC	a
rostlina	rostlina	k1gFnSc1	rostlina
khat	khat	k1gInSc1	khat
<g/>
,	,	kIx,	,
mírná	mírný	k2eAgFnSc1d1	mírná
droga	droga	k1gFnSc1	droga
oblíbená	oblíbený	k2eAgFnSc1d1	oblíbená
zejména	zejména	k9	zejména
v	v	k7c6	v
Džibutsku	Džibutsek	k1gInSc6	Džibutsek
<g/>
,	,	kIx,	,
Jemenu	Jemen	k1gInSc6	Jemen
a	a	k8xC	a
Somálsku	Somálsko	k1gNnSc6	Somálsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
importu	import	k1gInSc6	import
taktéž	taktéž	k?	taktéž
převažuje	převažovat	k5eAaImIp3nS	převažovat
Čína	Čína	k1gFnSc1	Čína
s	s	k7c7	s
podílem	podíl	k1gInSc7	podíl
19,8	[number]	k4	19,8
%	%	kIx~	%
<g/>
,	,	kIx,	,
další	další	k2eAgMnPc1d1	další
významní	významný	k2eAgMnPc1d1	významný
dovozci	dovozce	k1gMnPc1	dovozce
jsou	být	k5eAaImIp3nP	být
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
(	(	kIx(	(
<g/>
8,5	[number]	k4	8,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Indie	Indie	k1gFnSc1	Indie
(	(	kIx(	(
<g/>
4,4	[number]	k4	4,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
americké	americký	k2eAgFnSc2d1	americká
(	(	kIx(	(
<g/>
4,4	[number]	k4	4,4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejčastěji	často	k6eAd3	často
dovážené	dovážený	k2eAgNnSc4d1	dovážené
zboží	zboží	k1gNnSc4	zboží
patří	patřit	k5eAaImIp3nS	patřit
potraviny	potravina	k1gFnPc4	potravina
a	a	k8xC	a
živá	živý	k2eAgNnPc4d1	živé
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
ropa	ropa	k1gFnSc1	ropa
a	a	k8xC	a
ropné	ropný	k2eAgInPc1d1	ropný
výrobky	výrobek	k1gInPc1	výrobek
<g/>
,	,	kIx,	,
chemikálie	chemikálie	k1gFnPc1	chemikálie
<g/>
,	,	kIx,	,
stroje	stroj	k1gInPc1	stroj
<g/>
,	,	kIx,	,
výrobní	výrobní	k2eAgNnPc1d1	výrobní
zařízení	zařízení	k1gNnPc1	zařízení
<g/>
,	,	kIx,	,
motorová	motorový	k2eAgNnPc1d1	motorové
vozidla	vozidlo	k1gNnPc1	vozidlo
<g/>
,	,	kIx,	,
letadla	letadlo	k1gNnPc1	letadlo
a	a	k8xC	a
tkané	tkaný	k2eAgFnPc1d1	tkaná
látky	látka	k1gFnPc1	látka
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Etiopie	Etiopie	k1gFnSc1	Etiopie
je	být	k5eAaImIp3nS	být
nucena	nucen	k2eAgFnSc1d1	nucena
dovážet	dovážet	k5eAaImF	dovážet
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
jídla	jídlo	k1gNnSc2	jídlo
a	a	k8xC	a
neprodukuje	produkovat	k5eNaImIp3nS	produkovat
drahé	drahý	k2eAgNnSc4d1	drahé
exportní	exportní	k2eAgNnSc4d1	exportní
zboží	zboží	k1gNnSc4	zboží
jako	jako	k8xS	jako
minerály	minerál	k1gInPc4	minerál
a	a	k8xC	a
ropu	ropa	k1gFnSc4	ropa
<g/>
,	,	kIx,	,
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
obchodní	obchodní	k2eAgFnSc1d1	obchodní
bilance	bilance	k1gFnSc1	bilance
trvale	trvale	k6eAd1	trvale
záporná	záporný	k2eAgFnSc1d1	záporná
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
měl	mít	k5eAaImAgInS	mít
export	export	k1gInSc1	export
hodnotu	hodnota	k1gFnSc4	hodnota
1,729	[number]	k4	1,729
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
import	import	k1gInSc1	import
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
hodnoty	hodnota	k1gFnSc2	hodnota
7,517	[number]	k4	7,517
miliardy	miliarda	k4xCgFnSc2	miliarda
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
<g/>
Z	z	k7c2	z
nerostných	nerostný	k2eAgFnPc2d1	nerostná
surovin	surovina	k1gFnPc2	surovina
má	mít	k5eAaImIp3nS	mít
Etiopie	Etiopie	k1gFnSc1	Etiopie
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
naleziště	naleziště	k1gNnSc2	naleziště
soli	sůl	k1gFnSc2	sůl
<g/>
,	,	kIx,	,
zlata	zlato	k1gNnSc2	zlato
<g/>
,	,	kIx,	,
platiny	platina	k1gFnSc2	platina
<g/>
,	,	kIx,	,
železné	železný	k2eAgFnSc2d1	železná
rudy	ruda	k1gFnSc2	ruda
<g/>
,	,	kIx,	,
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
,	,	kIx,	,
drahých	drahý	k2eAgInPc2d1	drahý
kamenů	kámen	k1gInPc2	kámen
<g/>
,	,	kIx,	,
uhličitanu	uhličitan	k1gInSc2	uhličitan
sodného	sodný	k2eAgInSc2d1	sodný
<g/>
,	,	kIx,	,
tantalu	tantal	k1gInSc2	tantal
<g/>
,	,	kIx,	,
potaše	potaš	k1gFnSc2	potaš
<g/>
,	,	kIx,	,
mědi	měď	k1gFnSc2	měď
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
nevyužitá	využitý	k2eNgNnPc1d1	nevyužité
ložiska	ložisko	k1gNnPc1	ložisko
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
,	,	kIx,	,
niklu	nikl	k1gInSc2	nikl
<g/>
,	,	kIx,	,
mramoru	mramor	k1gInSc2	mramor
a	a	k8xC	a
žuly	žula	k1gFnSc2	žula
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
větším	veliký	k2eAgNnSc6d2	veliký
množství	množství	k1gNnSc6	množství
se	se	k3xPyFc4	se
zatím	zatím	k6eAd1	zatím
těží	těžet	k5eAaImIp3nS	těžet
pouze	pouze	k6eAd1	pouze
zlato	zlato	k1gNnSc1	zlato
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Zemědělství	zemědělství	k1gNnSc1	zemědělství
===	===	k?	===
</s>
</p>
<p>
<s>
Zemědělství	zemědělství	k1gNnSc1	zemědělství
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
vyspělými	vyspělý	k2eAgFnPc7d1	vyspělá
ekonomikami	ekonomika	k1gFnPc7	ekonomika
silně	silně	k6eAd1	silně
zaostalé	zaostalý	k2eAgFnPc1d1	zaostalá
<g/>
.	.	kIx.	.
</s>
<s>
Veškerou	veškerý	k3xTgFnSc4	veškerý
půdu	půda	k1gFnSc4	půda
vlastní	vlastnit	k5eAaImIp3nS	vlastnit
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
ji	on	k3xPp3gFnSc4	on
zájemcům	zájemce	k1gMnPc3	zájemce
pronajímá	pronajímat	k5eAaImIp3nS	pronajímat
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
k	k	k7c3	k
zemědělské	zemědělský	k2eAgFnSc3d1	zemědělská
činnosti	činnost	k1gFnSc3	činnost
využívá	využívat	k5eAaImIp3nS	využívat
17	[number]	k4	17
%	%	kIx~	%
půdy	půda	k1gFnSc2	půda
<g/>
,	,	kIx,	,
množství	množství	k1gNnSc1	množství
půdy	půda	k1gFnSc2	půda
vhodné	vhodný	k2eAgFnSc2d1	vhodná
k	k	k7c3	k
obdělávání	obdělávání	k1gNnSc3	obdělávání
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
nevyužito	využit	k2eNgNnSc1d1	nevyužito
<g/>
,	,	kIx,	,
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
výroba	výroba	k1gFnSc1	výroba
je	být	k5eAaImIp3nS	být
minimálně	minimálně	k6eAd1	minimálně
mechanizovaná	mechanizovaný	k2eAgFnSc1d1	mechanizovaná
a	a	k8xC	a
úroda	úroda	k1gFnSc1	úroda
je	být	k5eAaImIp3nS	být
silně	silně	k6eAd1	silně
závislá	závislý	k2eAgFnSc1d1	závislá
na	na	k7c6	na
dešťových	dešťový	k2eAgFnPc6d1	dešťová
srážkách	srážka	k1gFnPc6	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
90	[number]	k4	90
%	%	kIx~	%
zemědělsky	zemědělsky	k6eAd1	zemědělsky
užívané	užívaný	k2eAgFnSc2d1	užívaná
půdy	půda	k1gFnSc2	půda
obdělávají	obdělávat	k5eAaImIp3nP	obdělávat
drobní	drobný	k2eAgMnPc1d1	drobný
rolníci	rolník	k1gMnPc1	rolník
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
zemědělské	zemědělský	k2eAgFnPc1d1	zemědělská
techniky	technika	k1gFnPc1	technika
se	se	k3xPyFc4	se
za	za	k7c2	za
posledních	poslední	k2eAgNnPc2d1	poslední
dva	dva	k4xCgInPc4	dva
tisíce	tisíc	k4xCgInSc2	tisíc
let	léto	k1gNnPc2	léto
takřka	takřka	k6eAd1	takřka
nezměnily	změnit	k5eNaPmAgFnP	změnit
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
vrchovině	vrchovina	k1gFnSc6	vrchovina
se	se	k3xPyFc4	se
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
obilniny	obilnina	k1gFnPc1	obilnina
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
teff	teff	k1gInSc1	teff
<g/>
,	,	kIx,	,
endemická	endemický	k2eAgFnSc1d1	endemická
plodina	plodina	k1gFnSc1	plodina
užívaná	užívaný	k2eAgFnSc1d1	užívaná
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
etiopského	etiopský	k2eAgInSc2d1	etiopský
chleba	chléb	k1gInSc2	chléb
<g/>
,	,	kIx,	,
ječmen	ječmen	k1gInSc1	ječmen
<g/>
,	,	kIx,	,
pšenice	pšenice	k1gFnSc1	pšenice
<g/>
,	,	kIx,	,
proso	proso	k1gNnSc1	proso
<g/>
,	,	kIx,	,
luštěniny	luštěnina	k1gFnPc1	luštěnina
a	a	k8xC	a
olejniny	olejnina	k1gFnPc1	olejnina
<g/>
,	,	kIx,	,
v	v	k7c6	v
nížinách	nížina	k1gFnPc6	nížina
jsou	být	k5eAaImIp3nP	být
oblíbenými	oblíbený	k2eAgFnPc7d1	oblíbená
plodinami	plodina	k1gFnPc7	plodina
zejména	zejména	k9	zejména
čirok	čirok	k1gInSc4	čirok
a	a	k8xC	a
cukrová	cukrový	k2eAgFnSc1d1	cukrová
třtina	třtina	k1gFnSc1	třtina
<g/>
.	.	kIx.	.
</s>
<s>
Běžné	běžný	k2eAgInPc1d1	běžný
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
brambory	brambora	k1gFnPc1	brambora
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jihu	jih	k1gInSc6	jih
se	se	k3xPyFc4	se
pěstují	pěstovat	k5eAaImIp3nP	pěstovat
dvě	dva	k4xCgFnPc1	dva
významné	významný	k2eAgFnPc1d1	významná
exportní	exportní	k2eAgFnPc1d1	exportní
plodiny	plodina	k1gFnPc1	plodina
<g/>
:	:	kIx,	:
první	první	k4xOgFnSc1	první
je	být	k5eAaImIp3nS	být
káva	káva	k1gFnSc1	káva
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
také	také	k9	také
z	z	k7c2	z
oblasti	oblast	k1gFnSc2	oblast
Etiopie	Etiopie	k1gFnSc2	Etiopie
původně	původně	k6eAd1	původně
pochází	pocházet	k5eAaImIp3nS	pocházet
a	a	k8xC	a
jež	jenž	k3xRgFnSc1	jenž
se	se	k3xPyFc4	se
například	například	k6eAd1	například
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2007	[number]	k4	2007
podílela	podílet	k5eAaImAgFnS	podílet
36	[number]	k4	36
%	%	kIx~	%
na	na	k7c6	na
celkovém	celkový	k2eAgInSc6d1	celkový
výnosu	výnos	k1gInSc6	výnos
exportu	export	k1gInSc2	export
<g/>
;	;	kIx,	;
druhou	druhý	k4xOgFnSc4	druhý
je	být	k5eAaImIp3nS	být
lehké	lehký	k2eAgNnSc4d1	lehké
narkotikum	narkotikum	k1gNnSc4	narkotikum
khat	khata	k1gFnPc2	khata
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
pronajímá	pronajímat	k5eAaImIp3nS	pronajímat
vláda	vláda	k1gFnSc1	vláda
půdu	půda	k1gFnSc4	půda
ve	v	k7c6	v
stále	stále	k6eAd1	stále
větší	veliký	k2eAgFnSc6d2	veliký
míře	míra	k1gFnSc6	míra
zahraničním	zahraniční	k2eAgMnPc3d1	zahraniční
investorům	investor	k1gMnPc3	investor
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
zakládají	zakládat	k5eAaImIp3nP	zakládat
rozsáhlé	rozsáhlý	k2eAgFnPc1d1	rozsáhlá
plantáže	plantáž	k1gFnPc1	plantáž
na	na	k7c4	na
pěstování	pěstování	k1gNnSc4	pěstování
květin	květina	k1gFnPc2	květina
<g/>
,	,	kIx,	,
zemědělských	zemědělský	k2eAgFnPc2d1	zemědělská
biopaliv	biopalit	k5eAaPmDgInS	biopalit
<g/>
,	,	kIx,	,
kávy	káva	k1gFnSc2	káva
a	a	k8xC	a
čaje	čaj	k1gInSc2	čaj
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
praktiky	praktika	k1gFnPc1	praktika
často	často	k6eAd1	často
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
vyhánění	vyhánění	k1gNnSc3	vyhánění
rolníků	rolník	k1gMnPc2	rolník
z	z	k7c2	z
jimi	on	k3xPp3gMnPc7	on
obdělávané	obdělávaný	k2eAgFnPc4d1	obdělávaná
půdy	půda	k1gFnSc2	půda
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k9	tak
častá	častý	k2eAgFnSc1d1	častá
neúroda	neúroda	k1gFnSc1	neúroda
způsobená	způsobený	k2eAgFnSc1d1	způsobená
suchy	sucho	k1gNnPc7	sucho
vede	vést	k5eAaImIp3nS	vést
rolníky	rolník	k1gMnPc4	rolník
k	k	k7c3	k
opouštění	opouštění	k1gNnSc3	opouštění
polí	pole	k1gFnPc2	pole
a	a	k8xC	a
stěhování	stěhování	k1gNnSc1	stěhování
se	se	k3xPyFc4	se
do	do	k7c2	do
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
rozšiřují	rozšiřovat	k5eAaImIp3nP	rozšiřovat
řady	řada	k1gFnPc1	řada
městské	městský	k2eAgFnSc2d1	městská
chudiny	chudina	k1gFnSc2	chudina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
také	také	k9	také
žije	žít	k5eAaImIp3nS	žít
na	na	k7c4	na
7	[number]	k4	7
milionů	milion	k4xCgInPc2	milion
pastevců	pastevec	k1gMnPc2	pastevec
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
průzkumu	průzkum	k1gInSc2	průzkum
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
se	se	k3xPyFc4	se
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
nacházelo	nacházet	k5eAaImAgNnS	nacházet
35	[number]	k4	35
milionů	milion	k4xCgInPc2	milion
skotu	skot	k1gInSc2	skot
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
největší	veliký	k2eAgNnSc4d3	veliký
stádo	stádo	k1gNnSc4	stádo
tohoto	tento	k3xDgInSc2	tento
zvířecího	zvířecí	k2eAgInSc2d1	zvířecí
druhu	druh	k1gInSc2	druh
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
také	také	k9	také
25	[number]	k4	25
milionů	milion	k4xCgInPc2	milion
ovcí	ovce	k1gFnPc2	ovce
a	a	k8xC	a
18	[number]	k4	18
milionů	milion	k4xCgInPc2	milion
koz	koza	k1gFnPc2	koza
<g/>
.	.	kIx.	.
</s>
<s>
Začíná	začínat	k5eAaImIp3nS	začínat
se	se	k3xPyFc4	se
rozvíjet	rozvíjet	k5eAaImF	rozvíjet
sladkovodní	sladkovodní	k2eAgNnSc1d1	sladkovodní
rybářství	rybářství	k1gNnSc1	rybářství
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Průmysl	průmysl	k1gInSc1	průmysl
===	===	k?	===
</s>
</p>
<p>
<s>
Rovněž	rovněž	k9	rovněž
průmysl	průmysl	k1gInSc1	průmysl
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
zaostalý	zaostalý	k2eAgMnSc1d1	zaostalý
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
toto	tento	k3xDgNnSc1	tento
odvětví	odvětví	k1gNnSc1	odvětví
začíná	začínat	k5eAaImIp3nS	začínat
vykazovat	vykazovat	k5eAaImF	vykazovat
silnější	silný	k2eAgInSc1d2	silnější
růst	růst	k1gInSc1	růst
–	–	k?	–
zatímco	zatímco	k8xS	zatímco
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgInS	podílet
na	na	k7c6	na
HDP	HDP	kA	HDP
asi	asi	k9	asi
5,1	[number]	k4	5,1
%	%	kIx~	%
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
již	již	k6eAd1	již
13	[number]	k4	13
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
podíl	podíl	k1gInSc1	podíl
ve	v	k7c6	v
výrobě	výroba	k1gFnSc6	výroba
má	mít	k5eAaImIp3nS	mít
průmysl	průmysl	k1gInSc4	průmysl
potravinářský	potravinářský	k2eAgInSc4d1	potravinářský
<g/>
,	,	kIx,	,
následovaný	následovaný	k2eAgInSc4d1	následovaný
kožedělným	kožedělný	k2eAgInSc7d1	kožedělný
a	a	k8xC	a
textilním	textilní	k2eAgInSc7d1	textilní
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
dalších	další	k2eAgNnPc2d1	další
odvětví	odvětví	k1gNnPc2	odvětví
jsou	být	k5eAaImIp3nP	být
významné	významný	k2eAgInPc1d1	významný
výroba	výroba	k1gFnSc1	výroba
chemikálií	chemikálie	k1gFnPc2	chemikálie
<g/>
,	,	kIx,	,
léčiv	léčivo	k1gNnPc2	léčivo
<g/>
,	,	kIx,	,
stavebních	stavební	k2eAgFnPc2d1	stavební
hmot	hmota	k1gFnPc2	hmota
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
kovozpracující	kovozpracující	k2eAgInSc1d1	kovozpracující
průmysl	průmysl	k1gInSc1	průmysl
<g/>
,	,	kIx,	,
dřevozpracující	dřevozpracující	k2eAgInSc1d1	dřevozpracující
průmysl	průmysl	k1gInSc1	průmysl
a	a	k8xC	a
výroba	výroba	k1gFnSc1	výroba
traktorů	traktor	k1gInPc2	traktor
<g/>
,	,	kIx,	,
nákladních	nákladní	k2eAgInPc2d1	nákladní
automobilů	automobil	k1gInPc2	automobil
a	a	k8xC	a
ocelových	ocelový	k2eAgFnPc2d1	ocelová
konstrukcí	konstrukce	k1gFnPc2	konstrukce
<g/>
.	.	kIx.	.
</s>
<s>
Takřka	takřka	k6eAd1	takřka
veškeré	veškerý	k3xTgInPc4	veškerý
průmyslové	průmyslový	k2eAgInPc4d1	průmyslový
podniky	podnik	k1gInPc4	podnik
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc6	jeho
okolí	okolí	k1gNnSc6	okolí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Služby	služba	k1gFnPc1	služba
a	a	k8xC	a
cestovní	cestovní	k2eAgInSc1d1	cestovní
ruch	ruch	k1gInSc1	ruch
===	===	k?	===
</s>
</p>
<p>
<s>
Služby	služba	k1gFnPc1	služba
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
nejsou	být	k5eNaImIp3nP	být
až	až	k9	až
na	na	k7c4	na
výjimky	výjimka	k1gFnPc4	výjimka
na	na	k7c6	na
vysoké	vysoký	k2eAgFnSc6d1	vysoká
úrovni	úroveň	k1gFnSc6	úroveň
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
dominují	dominovat	k5eAaImIp3nP	dominovat
převážně	převážně	k6eAd1	převážně
muslimové	muslim	k1gMnPc1	muslim
<g/>
,	,	kIx,	,
dopravy	doprava	k1gFnSc2	doprava
a	a	k8xC	a
komunikací	komunikace	k1gFnPc2	komunikace
představuje	představovat	k5eAaImIp3nS	představovat
význačné	význačný	k2eAgNnSc4d1	význačné
odvětví	odvětví	k1gNnSc4	odvětví
cestovní	cestovní	k2eAgInSc1d1	cestovní
ruch	ruch	k1gInSc1	ruch
těšící	těšící	k2eAgFnSc2d1	těšící
se	se	k3xPyFc4	se
podpoře	podpora	k1gFnSc3	podpora
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
jej	on	k3xPp3gMnSc4	on
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
jeden	jeden	k4xCgInSc4	jeden
ze	z	k7c2	z
základních	základní	k2eAgInPc2d1	základní
kamenů	kámen	k1gInPc2	kámen
budoucnosti	budoucnost	k1gFnSc2	budoucnost
Etiopie	Etiopie	k1gFnSc1	Etiopie
<g/>
.	.	kIx.	.
</s>
<s>
Převážnou	převážný	k2eAgFnSc4d1	převážná
část	část	k1gFnSc4	část
turistů	turist	k1gMnPc2	turist
<g/>
,	,	kIx,	,
kterých	který	k3yRgInPc2	který
do	do	k7c2	do
Etiopie	Etiopie	k1gFnSc2	Etiopie
přijede	přijet	k5eAaPmIp3nS	přijet
ročně	ročně	k6eAd1	ročně
zhruba	zhruba	k6eAd1	zhruba
100	[number]	k4	100
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
0	[number]	k4	0
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
Etiopané	Etiopaný	k2eAgFnPc1d1	Etiopaný
žijící	žijící	k2eAgFnPc1d1	žijící
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
turistiky	turistika	k1gFnSc2	turistika
přichází	přicházet	k5eAaImIp3nS	přicházet
ročně	ročně	k6eAd1	ročně
státu	stát	k1gInSc2	stát
zhruba	zhruba	k6eAd1	zhruba
20	[number]	k4	20
milionů	milion	k4xCgInPc2	milion
dolarů	dolar	k1gInPc2	dolar
<g/>
.	.	kIx.	.
</s>
<s>
Potenciál	potenciál	k1gInSc1	potenciál
země	zem	k1gFnSc2	zem
však	však	k9	však
zdaleka	zdaleka	k6eAd1	zdaleka
není	být	k5eNaImIp3nS	být
dostatečně	dostatečně	k6eAd1	dostatečně
využitý	využitý	k2eAgInSc1d1	využitý
<g/>
.	.	kIx.	.
<g/>
Etiopie	Etiopie	k1gFnSc1	Etiopie
nabízí	nabízet	k5eAaImIp3nS	nabízet
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
historických	historický	k2eAgFnPc2d1	historická
i	i	k8xC	i
přírodních	přírodní	k2eAgFnPc2d1	přírodní
památek	památka	k1gFnPc2	památka
<g/>
.	.	kIx.	.
</s>
<s>
Nejnavštěvovanějšími	navštěvovaný	k2eAgNnPc7d3	nejnavštěvovanější
místy	místo	k1gNnPc7	místo
jsou	být	k5eAaImIp3nP	být
starobylé	starobylý	k2eAgInPc4d1	starobylý
chrámy	chrám	k1gInPc4	chrám
a	a	k8xC	a
obelisky	obelisk	k1gInPc4	obelisk
v	v	k7c6	v
Aksumu	Aksum	k1gInSc6	Aksum
<g/>
,	,	kIx,	,
hrady	hrad	k1gInPc1	hrad
a	a	k8xC	a
zámky	zámek	k1gInPc1	zámek
v	v	k7c4	v
Gondaru	Gondara	k1gFnSc4	Gondara
a	a	k8xC	a
skalní	skalní	k2eAgInPc1d1	skalní
chrámy	chrám	k1gInPc1	chrám
v	v	k7c6	v
Lalibele	Lalibela	k1gFnSc6	Lalibela
<g/>
.	.	kIx.	.
</s>
<s>
Pozornosti	pozornost	k1gFnSc3	pozornost
se	se	k3xPyFc4	se
také	také	k9	také
těší	těšit	k5eAaImIp3nS	těšit
město	město	k1gNnSc1	město
Harer	Harra	k1gFnPc2	Harra
<g/>
,	,	kIx,	,
kláštery	klášter	k1gInPc1	klášter
nacházející	nacházející	k2eAgInPc1d1	nacházející
se	se	k3xPyFc4	se
na	na	k7c6	na
ostrovech	ostrov	k1gInPc6	ostrov
jezera	jezero	k1gNnSc2	jezero
Tana	tanout	k5eAaImSgInS	tanout
i	i	k9	i
jinde	jinde	k6eAd1	jinde
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Debre	Debr	k1gInSc5	Debr
Libanos	Libanosa	k1gFnPc2	Libanosa
a	a	k8xC	a
Debre	Debr	k1gMnSc5	Debr
Damo	Dama	k1gMnSc5	Dama
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
přírodních	přírodní	k2eAgFnPc2d1	přírodní
památek	památka	k1gFnPc2	památka
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
jmenovat	jmenovat	k5eAaImF	jmenovat
samotné	samotný	k2eAgNnSc4d1	samotné
jezero	jezero	k1gNnSc4	jezero
Tana	tanout	k5eAaImSgInS	tanout
<g/>
,	,	kIx,	,
vodopád	vodopád	k1gInSc1	vodopád
Tis	tis	k1gInSc1	tis
Issat	Issat	k2eAgInSc1d1	Issat
<g/>
,	,	kIx,	,
jeskynní	jeskynní	k2eAgInSc1d1	jeskynní
systém	systém	k1gInSc1	systém
Sof	sofa	k1gFnPc2	sofa
Omar	Omara	k1gFnPc2	Omara
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
například	například	k6eAd1	například
jezera	jezero	k1gNnSc2	jezero
Abiyata	Abiyat	k1gMnSc4	Abiyat
<g/>
,	,	kIx,	,
Shala	Shal	k1gMnSc4	Shal
<g/>
,	,	kIx,	,
Langano	Langana	k1gFnSc5	Langana
<g/>
,	,	kIx,	,
termální	termální	k2eAgInPc1d1	termální
prameny	pramen	k1gInPc1	pramen
v	v	k7c6	v
Sodere	Soder	k1gInSc5	Soder
<g/>
,	,	kIx,	,
Wellisu	Wellis	k1gInSc2	Wellis
či	či	k8xC	či
Awase	Awase	k1gFnSc2	Awase
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
také	také	k9	také
12	[number]	k4	12
národních	národní	k2eAgInPc2d1	národní
parků	park	k1gInPc2	park
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
Národní	národní	k2eAgInSc1d1	národní
park	park	k1gInSc1	park
Awaš	Awaš	k1gInSc1	Awaš
<g/>
,	,	kIx,	,
NP	NP	kA	NP
Bale	bal	k1gInSc5	bal
Moutains	Moutains	k1gInSc1	Moutains
<g/>
,	,	kIx,	,
NP	NP	kA	NP
Gambella	Gambella	k1gFnSc1	Gambella
<g/>
,	,	kIx,	,
NP	NP	kA	NP
Simien	Simien	k2eAgInSc1d1	Simien
Mountains	Mountains	k1gInSc1	Mountains
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Ochrana	ochrana	k1gFnSc1	ochrana
prostředí	prostředí	k1gNnSc2	prostředí
je	být	k5eAaImIp3nS	být
však	však	k9	však
silně	silně	k6eAd1	silně
zanedbána	zanedbat	k5eAaPmNgFnS	zanedbat
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
tak	tak	k6eAd1	tak
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
problém	problém	k1gInSc1	problém
s	s	k7c7	s
nedostatečnou	dostatečný	k2eNgFnSc7d1	nedostatečná
úrovní	úroveň	k1gFnSc7	úroveň
dopravní	dopravní	k2eAgFnSc2d1	dopravní
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
a	a	k8xC	a
ubytování	ubytování	k1gNnSc2	ubytování
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc4d1	další
možnosti	možnost	k1gFnPc4	možnost
představuje	představovat	k5eAaImIp3nS	představovat
horská	horský	k2eAgFnSc1d1	horská
turistika	turistika	k1gFnSc1	turistika
či	či	k8xC	či
kontakty	kontakt	k1gInPc1	kontakt
s	s	k7c7	s
domorodými	domorodý	k2eAgInPc7d1	domorodý
kmeny	kmen	k1gInPc7	kmen
žijícími	žijící	k2eAgInPc7d1	žijící
původním	původní	k2eAgInSc7d1	původní
způsobem	způsob	k1gInSc7	způsob
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
===	===	k?	===
</s>
</p>
<p>
<s>
Celkově	celkově	k6eAd1	celkově
se	se	k3xPyFc4	se
etiopská	etiopský	k2eAgFnSc1d1	etiopská
infrastruktura	infrastruktura	k1gFnSc1	infrastruktura
rychle	rychle	k6eAd1	rychle
rozvíjí	rozvíjet	k5eAaImIp3nS	rozvíjet
<g/>
.	.	kIx.	.
</s>
<s>
Etiopie	Etiopie	k1gFnSc1	Etiopie
vykládá	vykládat	k5eAaImIp3nS	vykládat
na	na	k7c4	na
její	její	k3xOp3gFnSc4	její
výstavbu	výstavba	k1gFnSc4	výstavba
zhruba	zhruba	k6eAd1	zhruba
10	[number]	k4	10
%	%	kIx~	%
HDP	HDP	kA	HDP
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
nejvyšších	vysoký	k2eAgMnPc2d3	nejvyšší
podílů	podíl	k1gInPc2	podíl
HDP	HDP	kA	HDP
určených	určený	k2eAgInPc2d1	určený
na	na	k7c4	na
rozvoj	rozvoj	k1gInSc4	rozvoj
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Budování	budování	k1gNnSc4	budování
dopravní	dopravní	k2eAgFnSc2d1	dopravní
sítě	síť	k1gFnSc2	síť
je	být	k5eAaImIp3nS	být
soustředěno	soustředit	k5eAaPmNgNnS	soustředit
zejména	zejména	k9	zejména
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
Addis	Addis	k1gFnSc2	Addis
Abeby	Abeba	k1gFnSc2	Abeba
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
4100	[number]	k4	4100
km	km	kA	km
asfaltových	asfaltový	k2eAgFnPc2d1	asfaltová
silnic	silnice	k1gFnPc2	silnice
<g/>
,	,	kIx,	,
spojujících	spojující	k2eAgFnPc2d1	spojující
především	především	k6eAd1	především
významná	významný	k2eAgNnPc1d1	významné
městská	městský	k2eAgNnPc1d1	Městské
centra	centrum	k1gNnPc1	centrum
<g/>
,	,	kIx,	,
dobrá	dobrý	k2eAgFnSc1d1	dobrá
silniční	silniční	k2eAgFnSc1d1	silniční
síť	síť	k1gFnSc1	síť
funguje	fungovat	k5eAaImIp3nS	fungovat
i	i	k9	i
při	při	k7c6	při
hranicích	hranice	k1gFnPc6	hranice
s	s	k7c7	s
Džibutskem	Džibutsek	k1gInSc7	Džibutsek
a	a	k8xC	a
Keňou	Keňa	k1gFnSc7	Keňa
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgInPc2d1	další
19	[number]	k4	19
tisíc	tisíc	k4xCgInPc2	tisíc
km	km	kA	km
silnic	silnice	k1gFnPc2	silnice
je	být	k5eAaImIp3nS	být
prašných	prašný	k2eAgFnPc2d1	prašná
nebo	nebo	k8xC	nebo
štěrkových	štěrkový	k2eAgFnPc2d1	štěrková
<g/>
.	.	kIx.	.
</s>
<s>
Jedinou	jediný	k2eAgFnSc7d1	jediná
existující	existující	k2eAgFnSc7d1	existující
železnicí	železnice	k1gFnSc7	železnice
je	být	k5eAaImIp3nS	být
trať	trať	k1gFnSc1	trať
mezi	mezi	k7c7	mezi
Addis	Addis	k1gFnSc7	Addis
Abebou	Abebý	k2eAgFnSc7d1	Abebý
přes	přes	k7c4	přes
Dire	Dire	k1gNnSc4	Dire
Dawu	Dawus	k1gInSc2	Dawus
s	s	k7c7	s
Džibutskem	Džibutsek	k1gInSc7	Džibutsek
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
781	[number]	k4	781
km	km	kA	km
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gNnSc1	jejíž
vlastnictví	vlastnictví	k1gNnSc1	vlastnictví
vyjasnilo	vyjasnit	k5eAaPmAgNnS	vyjasnit
několik	několik	k4yIc1	několik
bilaterálních	bilaterální	k2eAgFnPc2d1	bilaterální
smluv	smlouva	k1gFnPc2	smlouva
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgInPc7	dva
státy	stát	k1gInPc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
byla	být	k5eAaImAgFnS	být
koncese	koncese	k1gFnSc1	koncese
na	na	k7c4	na
tuto	tento	k3xDgFnSc4	tento
železnici	železnice	k1gFnSc4	železnice
prodána	prodat	k5eAaPmNgFnS	prodat
jihoafrické	jihoafrický	k2eAgFnSc3d1	Jihoafrická
firmě	firma	k1gFnSc3	firma
Comazar	Comazar	k1gInSc4	Comazar
<g/>
.	.	kIx.	.
</s>
<s>
Plánovaná	plánovaný	k2eAgFnSc1d1	plánovaná
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
existujícího	existující	k2eAgNnSc2d1	existující
železničního	železniční	k2eAgNnSc2d1	železniční
spojení	spojení	k1gNnSc2	spojení
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc4	jeho
další	další	k2eAgNnSc4d1	další
rozšíření	rozšíření	k1gNnSc4	rozšíření
na	na	k7c4	na
5000	[number]	k4	5000
km	km	kA	km
byla	být	k5eAaImAgFnS	být
započata	započnout	k5eAaPmNgFnS	započnout
v	v	k7c6	v
září	září	k1gNnSc6	září
2010	[number]	k4	2010
za	za	k7c7	za
pomoci	pomoc	k1gFnSc2	pomoc
čínských	čínský	k2eAgFnPc2d1	čínská
investic	investice	k1gFnPc2	investice
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
africké	africký	k2eAgInPc4d1	africký
poměry	poměr	k1gInPc4	poměr
má	mít	k5eAaImIp3nS	mít
vysokou	vysoký	k2eAgFnSc4d1	vysoká
úroveň	úroveň	k1gFnSc4	úroveň
letecká	letecký	k2eAgFnSc1d1	letecká
společnost	společnost	k1gFnSc1	společnost
Ethiopian	Ethiopian	k1gMnSc1	Ethiopian
Airlines	Airlines	k1gMnSc1	Airlines
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
řadí	řadit	k5eAaImIp3nS	řadit
mezi	mezi	k7c4	mezi
tři	tři	k4xCgMnPc4	tři
nejlepší	dobrý	k2eAgMnPc4d3	nejlepší
poskytovatele	poskytovatel	k1gMnPc4	poskytovatel
letecké	letecký	k2eAgFnSc2d1	letecká
dopravy	doprava	k1gFnSc2	doprava
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
a	a	k8xC	a
udržuje	udržovat	k5eAaImIp3nS	udržovat
množství	množství	k1gNnSc1	množství
linek	linka	k1gFnPc2	linka
do	do	k7c2	do
afrických	africký	k2eAgFnPc2d1	africká
a	a	k8xC	a
arabských	arabský	k2eAgFnPc2d1	arabská
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
Evropy	Evropa	k1gFnSc2	Evropa
či	či	k8xC	či
Indie	Indie	k1gFnSc2	Indie
<g/>
.	.	kIx.	.
</s>
<s>
Letiště	letiště	k1gNnSc4	letiště
Bole	bola	k1gFnSc3	bola
v	v	k7c4	v
Addis	Addis	k1gFnSc4	Addis
Abebě	Abeba	k1gFnSc3	Abeba
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
nejvýznamnějších	významný	k2eAgInPc2d3	nejvýznamnější
mezinárodních	mezinárodní	k2eAgInPc2d1	mezinárodní
afrických	africký	k2eAgInPc2d1	africký
leteckých	letecký	k2eAgInPc2d1	letecký
uzlů	uzel	k1gInPc2	uzel
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
zhruba	zhruba	k6eAd1	zhruba
80	[number]	k4	80
letišť	letiště	k1gNnPc2	letiště
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
30	[number]	k4	30
je	být	k5eAaImIp3nS	být
užíváno	užívat	k5eAaImNgNnS	užívat
pravidelně	pravidelně	k6eAd1	pravidelně
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
případů	případ	k1gInPc2	případ
pro	pro	k7c4	pro
domácí	domácí	k2eAgFnPc4d1	domácí
linky	linka	k1gFnPc4	linka
<g/>
.	.	kIx.	.
</s>
<s>
Země	země	k1gFnSc1	země
přišla	přijít	k5eAaPmAgFnS	přijít
o	o	k7c6	o
pobřeží	pobřeží	k1gNnSc6	pobřeží
i	i	k8xC	i
o	o	k7c4	o
všechny	všechen	k3xTgInPc4	všechen
přístavy	přístav	k1gInPc4	přístav
důsledkem	důsledek	k1gInSc7	důsledek
vyhlášení	vyhlášení	k1gNnSc1	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
Eritreje	Eritrea	k1gFnSc2	Eritrea
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1993	[number]	k4	1993
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
etiopsko-eritrejské	etiopskoritrejský	k2eAgFnSc6d1	etiopsko-eritrejský
válce	válka	k1gFnSc6	válka
byla	být	k5eAaImAgFnS	být
přesunuta	přesunout	k5eAaPmNgFnS	přesunout
námořní	námořní	k2eAgFnSc1d1	námořní
doprava	doprava	k1gFnSc1	doprava
do	do	k7c2	do
přístavu	přístav	k1gInSc2	přístav
Džíbútí	Džíbútí	k1gNnSc2	Džíbútí
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
také	také	k9	také
do	do	k7c2	do
somalilandské	somalilandský	k2eAgMnPc4d1	somalilandský
Berbery	Berber	k1gMnPc4	Berber
<g/>
.	.	kIx.	.
</s>
<s>
Momentálně	momentálně	k6eAd1	momentálně
má	mít	k5eAaImIp3nS	mít
etiopské	etiopský	k2eAgNnSc1d1	etiopské
obchodní	obchodní	k2eAgNnSc1d1	obchodní
loďstvo	loďstvo	k1gNnSc1	loďstvo
devět	devět	k4xCc4	devět
lodí	loď	k1gFnPc2	loď
<g/>
.	.	kIx.	.
<g/>
Zdrojem	zdroj	k1gInSc7	zdroj
elektřiny	elektřina	k1gFnSc2	elektřina
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
97	[number]	k4	97
%	%	kIx~	%
vodní	vodní	k2eAgFnSc2d1	vodní
elektrárny	elektrárna	k1gFnSc2	elektrárna
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
zemědělství	zemědělství	k1gNnSc4	zemědělství
<g/>
,	,	kIx,	,
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
dešťových	dešťový	k2eAgFnPc6d1	dešťová
srážkách	srážka	k1gFnPc6	srážka
<g/>
.	.	kIx.	.
</s>
<s>
Přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
elektřině	elektřina	k1gFnSc3	elektřina
má	mít	k5eAaImIp3nS	mít
asi	asi	k9	asi
86	[number]	k4	86
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
měst	město	k1gNnPc2	město
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
elektrifikace	elektrifikace	k1gFnSc1	elektrifikace
ve	v	k7c6	v
vesnických	vesnický	k2eAgFnPc6d1	vesnická
oblastech	oblast	k1gFnPc6	oblast
je	být	k5eAaImIp3nS	být
prakticky	prakticky	k6eAd1	prakticky
minimální	minimální	k2eAgMnSc1d1	minimální
<g/>
.	.	kIx.	.
</s>
<s>
Ropa	ropa	k1gFnSc1	ropa
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
dovážet	dovážet	k5eAaImF	dovážet
<g/>
;	;	kIx,	;
plánovány	plánován	k2eAgInPc1d1	plánován
jsou	být	k5eAaImIp3nP	být
průzkumy	průzkum	k1gInPc1	průzkum
nalezišť	naleziště	k1gNnPc2	naleziště
jak	jak	k8xS	jak
ropy	ropa	k1gFnSc2	ropa
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
možnost	možnost	k1gFnSc1	možnost
představuje	představovat	k5eAaImIp3nS	představovat
využití	využití	k1gNnSc4	využití
geotermální	geotermální	k2eAgFnSc2d1	geotermální
energie	energie	k1gFnSc2	energie
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
populace	populace	k1gFnSc2	populace
nadále	nadále	k6eAd1	nadále
závisí	záviset	k5eAaImIp3nS	záviset
na	na	k7c6	na
dřevě	dřevo	k1gNnSc6	dřevo
jakožto	jakožto	k8xS	jakožto
hlavní	hlavní	k2eAgFnSc6d1	hlavní
látce	látka	k1gFnSc6	látka
pro	pro	k7c4	pro
topení	topení	k1gNnSc4	topení
i	i	k8xC	i
stavbu	stavba	k1gFnSc4	stavba
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
vede	vést	k5eAaImIp3nS	vést
k	k	k7c3	k
prohlubující	prohlubující	k2eAgFnSc3d1	prohlubující
se	se	k3xPyFc4	se
deforestaci	deforestace	k1gFnSc3	deforestace
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
telekomunikací	telekomunikace	k1gFnPc2	telekomunikace
si	se	k3xPyFc3	se
udržuje	udržovat	k5eAaImIp3nS	udržovat
monopolní	monopolní	k2eAgNnSc4d1	monopolní
postavení	postavení	k1gNnSc4	postavení
Ethiopian	Ethiopiana	k1gFnPc2	Ethiopiana
Telecommunications	Telecommunicationsa	k1gFnPc2	Telecommunicationsa
Corporation	Corporation	k1gInSc1	Corporation
<g/>
.	.	kIx.	.
</s>
<s>
Přístup	přístup	k1gInSc1	přístup
k	k	k7c3	k
telefonu	telefon	k1gInSc3	telefon
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
zhruba	zhruba	k6eAd1	zhruba
5	[number]	k4	5
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1999	[number]	k4	1999
funguje	fungovat	k5eAaImIp3nS	fungovat
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
mobilní	mobilní	k2eAgFnSc1d1	mobilní
síť	síť	k1gFnSc1	síť
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zaveden	zavést	k5eAaPmNgInS	zavést
i	i	k9	i
internet	internet	k1gInSc1	internet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
==	==	k?	==
</s>
</p>
<p>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2010	[number]	k4	2010
čítala	čítat	k5eAaImAgFnS	čítat
etiopská	etiopský	k2eAgFnSc1d1	etiopská
populace	populace	k1gFnSc1	populace
podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
zhruba	zhruba	k6eAd1	zhruba
82	[number]	k4	82
101	[number]	k4	101
998	[number]	k4	998
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
v	v	k7c6	v
r.	r.	kA	r.
2017	[number]	k4	2017
už	už	k6eAd1	už
105	[number]	k4	105
350	[number]	k4	350
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Roční	roční	k2eAgInSc1d1	roční
přírůstek	přírůstek	k1gInSc1	přírůstek
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
činí	činit	k5eAaImIp3nS	činit
2,6	[number]	k4	2,6
%	%	kIx~	%
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
16	[number]	k4	16
<g/>
–	–	k?	–
<g/>
17	[number]	k4	17
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
žije	žít	k5eAaImIp3nS	žít
ve	v	k7c6	v
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
existuje	existovat	k5eAaImIp3nS	existovat
množství	množství	k1gNnSc1	množství
rozličných	rozličný	k2eAgFnPc2d1	rozličná
etnických	etnický	k2eAgFnPc2d1	etnická
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
největšími	veliký	k2eAgInPc7d3	veliký
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
jsou	být	k5eAaImIp3nP	být
Oromové	Oromová	k1gFnPc1	Oromová
(	(	kIx(	(
<g/>
34,5	[number]	k4	34,5
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Amharové	Amharové	k2eAgFnSc1d1	Amharové
(	(	kIx(	(
<g/>
26,9	[number]	k4	26,9
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Somálci	Somálec	k1gMnPc1	Somálec
(	(	kIx(	(
<g/>
6,2	[number]	k4	6,2
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Tigrajové	Tigraj	k1gMnPc1	Tigraj
(	(	kIx(	(
<g/>
6,1	[number]	k4	6,1
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
Sidamové	Sidamový	k2eAgInPc4d1	Sidamový
(	(	kIx(	(
<g/>
4	[number]	k4	4
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Guragové	Gurag	k1gMnPc1	Gurag
(	(	kIx(	(
<g/>
2,5	[number]	k4	2,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Welaitové	Welait	k1gMnPc1	Welait
(	(	kIx(	(
<g/>
2,3	[number]	k4	2,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Hadicové	hadicový	k2eAgFnSc2d1	hadicová
(	(	kIx(	(
<g/>
1,7	[number]	k4	1,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Afarové	Afarové	k2eAgFnSc1d1	Afarové
(	(	kIx(	(
<g/>
1,7	[number]	k4	1,7
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Gamové	Gamové	k2eAgFnSc1d1	Gamové
(	(	kIx(	(
<g/>
1,5	[number]	k4	1,5
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
Gedeové	Gedeové	k2eAgFnSc1d1	Gedeové
(	(	kIx(	(
<g/>
1,3	[number]	k4	1,3
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgFnPc4d1	ostatní
etnické	etnický	k2eAgFnPc4d1	etnická
skupiny	skupina	k1gFnPc4	skupina
tvoří	tvořit	k5eAaImIp3nS	tvořit
zbylých	zbylý	k2eAgNnPc2d1	zbylé
11,3	[number]	k4	11,3
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
městech	město	k1gNnPc6	město
žijí	žít	k5eAaImIp3nP	žít
i	i	k8xC	i
menšiny	menšina	k1gFnPc1	menšina
Italů	Ital	k1gMnPc2	Ital
<g/>
,	,	kIx,	,
Řeků	Řek	k1gMnPc2	Řek
<g/>
,	,	kIx,	,
Indů	Ind	k1gMnPc2	Ind
a	a	k8xC	a
Arménů	Armén	k1gMnPc2	Armén
<g/>
.	.	kIx.	.
</s>
<s>
Užívá	užívat	k5eAaImIp3nS	užívat
se	se	k3xPyFc4	se
zhruba	zhruba	k6eAd1	zhruba
83	[number]	k4	83
různých	různý	k2eAgInPc2d1	různý
jazyků	jazyk	k1gInPc2	jazyk
a	a	k8xC	a
200	[number]	k4	200
dialektů	dialekt	k1gInPc2	dialekt
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
patří	patřit	k5eAaImIp3nP	patřit
do	do	k7c2	do
semitské	semitský	k2eAgFnSc2d1	semitská
<g/>
,	,	kIx,	,
kušitské	kušitský	k2eAgFnSc2d1	kušitský
či	či	k8xC	či
omotské	omotský	k2eAgFnSc2d1	omotský
jazykové	jazykový	k2eAgFnSc2d1	jazyková
rodiny	rodina	k1gFnSc2	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Oficiálním	oficiální	k2eAgInSc7d1	oficiální
jazykem	jazyk	k1gInSc7	jazyk
celé	celý	k2eAgFnSc2d1	celá
federace	federace	k1gFnSc2	federace
je	být	k5eAaImIp3nS	být
amharština	amharština	k1gFnSc1	amharština
<g/>
,	,	kIx,	,
vedle	vedle	k7c2	vedle
ní	on	k3xPp3gFnSc2	on
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
jednotlivých	jednotlivý	k2eAgInPc6d1	jednotlivý
státech	stát	k1gInPc6	stát
statut	statut	k1gInSc4	statut
oficiálního	oficiální	k2eAgInSc2d1	oficiální
jazyka	jazyk	k1gInSc2	jazyk
tigrajština	tigrajština	k1gFnSc1	tigrajština
<g/>
,	,	kIx,	,
oromština	oromština	k1gFnSc1	oromština
<g/>
,	,	kIx,	,
harerština	harerština	k1gFnSc1	harerština
<g/>
,	,	kIx,	,
afarština	afarština	k1gFnSc1	afarština
<g/>
,	,	kIx,	,
somálština	somálština	k1gFnSc1	somálština
<g/>
,	,	kIx,	,
angličtina	angličtina	k1gFnSc1	angličtina
a	a	k8xC	a
arabština	arabština	k1gFnSc1	arabština
<g/>
.	.	kIx.	.
</s>
<s>
Angličtina	angličtina	k1gFnSc1	angličtina
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
etiopském	etiopský	k2eAgNnSc6d1	etiopské
školství	školství	k1gNnSc6	školství
nejčastěji	často	k6eAd3	často
vyučovaným	vyučovaný	k2eAgMnSc7d1	vyučovaný
zahraničním	zahraniční	k2eAgMnSc7d1	zahraniční
jazykem	jazyk	k1gMnSc7	jazyk
<g/>
.	.	kIx.	.
46	[number]	k4	46
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
0	[number]	k4	0
<g/>
–	–	k?	–
<g/>
14	[number]	k4	14
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
51	[number]	k4	51
%	%	kIx~	%
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
15	[number]	k4	15
<g/>
–	–	k?	–
<g/>
64	[number]	k4	64
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
zbývajících	zbývající	k2eAgMnPc2d1	zbývající
lidí	člověk	k1gMnPc2	člověk
nad	nad	k7c4	nad
64	[number]	k4	64
let	léto	k1gNnPc2	léto
je	být	k5eAaImIp3nS	být
2,7	[number]	k4	2,7
%	%	kIx~	%
<g/>
.	.	kIx.	.
<g/>
Takřka	takřka	k6eAd1	takřka
polovina	polovina	k1gFnSc1	polovina
Etiopanů	Etiopan	k1gMnPc2	Etiopan
žije	žít	k5eAaImIp3nS	žít
pod	pod	k7c7	pod
hranicí	hranice	k1gFnSc7	hranice
chudoby	chudoba	k1gFnSc2	chudoba
<g/>
.	.	kIx.	.
</s>
<s>
Zdravotnický	zdravotnický	k2eAgInSc1d1	zdravotnický
systém	systém	k1gInSc1	systém
je	být	k5eAaImIp3nS	být
nedostačující	dostačující	k2eNgInSc1d1	nedostačující
<g/>
,	,	kIx,	,
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
vysoká	vysoký	k2eAgFnSc1d1	vysoká
dětská	dětský	k2eAgFnSc1d1	dětská
úmrtnost	úmrtnost	k1gFnSc1	úmrtnost
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgFnSc1d1	střední
délka	délka	k1gFnSc1	délka
života	život	k1gInSc2	život
je	být	k5eAaImIp3nS	být
zhruba	zhruba	k6eAd1	zhruba
56	[number]	k4	56
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
čisté	čistý	k2eAgFnSc3d1	čistá
vodě	voda	k1gFnSc3	voda
má	mít	k5eAaImIp3nS	mít
přístup	přístup	k1gInSc1	přístup
98	[number]	k4	98
%	%	kIx~	%
obyvatel	obyvatel	k1gMnSc1	obyvatel
ve	v	k7c6	v
městech	město	k1gNnPc6	město
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
pouze	pouze	k6eAd1	pouze
26	[number]	k4	26
%	%	kIx~	%
na	na	k7c6	na
vesnici	vesnice	k1gFnSc6	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
Průměrně	průměrně	k6eAd1	průměrně
5	[number]	k4	5
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
je	on	k3xPp3gMnPc4	on
závislých	závislý	k2eAgInPc2d1	závislý
na	na	k7c6	na
dodávkách	dodávka	k1gFnPc6	dodávka
jídla	jídlo	k1gNnSc2	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Somalie	Somalie	k1gFnSc2	Somalie
je	být	k5eAaImIp3nS	být
také	také	k9	také
často	často	k6eAd1	často
kritický	kritický	k2eAgInSc1d1	kritický
nedostatek	nedostatek	k1gInSc1	nedostatek
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
Zhruba	zhruba	k6eAd1	zhruba
2,3	[number]	k4	2,3
%	%	kIx~	%
lidí	člověk	k1gMnPc2	člověk
je	být	k5eAaImIp3nS	být
nakaženo	nakazit	k5eAaPmNgNnS	nakazit
virem	vir	k1gInSc7	vir
HIV	HIV	kA	HIV
<g/>
,	,	kIx,	,
u	u	k7c2	u
infikovaných	infikovaný	k2eAgMnPc2d1	infikovaný
převažují	převažovat	k5eAaImIp3nP	převažovat
obyvatelé	obyvatel	k1gMnPc1	obyvatel
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Dalšími	další	k2eAgFnPc7d1	další
rozšířenými	rozšířený	k2eAgFnPc7d1	rozšířená
chorobami	choroba	k1gFnPc7	choroba
jsou	být	k5eAaImIp3nP	být
například	například	k6eAd1	například
žloutenka	žloutenka	k1gFnSc1	žloutenka
typu	typ	k1gInSc2	typ
A	A	kA	A
a	a	k8xC	a
E	E	kA	E
<g/>
,	,	kIx,	,
břišní	břišní	k2eAgInSc1d1	břišní
tyfus	tyfus	k1gInSc1	tyfus
<g/>
,	,	kIx,	,
malárie	malárie	k1gFnSc1	malárie
<g/>
,	,	kIx,	,
meningitida	meningitida	k1gFnSc1	meningitida
<g/>
,	,	kIx,	,
cholera	cholera	k1gFnSc1	cholera
<g/>
,	,	kIx,	,
žlutá	žlutý	k2eAgFnSc1d1	žlutá
zimnice	zimnice	k1gFnSc1	zimnice
<g/>
,	,	kIx,	,
vzteklina	vzteklina	k1gFnSc1	vzteklina
a	a	k8xC	a
schistosomóza	schistosomóza	k1gFnSc1	schistosomóza
<g/>
.	.	kIx.	.
</s>
<s>
Vláda	vláda	k1gFnSc1	vláda
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
současnou	současný	k2eAgFnSc4d1	současná
situaci	situace	k1gFnSc4	situace
řešit	řešit	k5eAaImF	řešit
a	a	k8xC	a
za	za	k7c4	za
posledních	poslední	k2eAgNnPc2d1	poslední
deset	deset	k4xCc4	deset
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
situace	situace	k1gFnSc1	situace
ve	v	k7c6	v
zdravotnictví	zdravotnictví	k1gNnSc6	zdravotnictví
mírně	mírně	k6eAd1	mírně
zlepšila	zlepšit	k5eAaPmAgFnS	zlepšit
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
etiopské	etiopský	k2eAgNnSc1d1	etiopské
zdravotnictví	zdravotnictví	k1gNnSc1	zdravotnictví
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
podle	podle	k7c2	podle
Center	centrum	k1gNnPc2	centrum
for	forum	k1gNnPc2	forum
National	National	k1gMnSc1	National
Health	Health	k1gMnSc1	Health
Development	Development	k1gMnSc1	Development
při	při	k7c6	při
Columbia	Columbia	k1gFnSc1	Columbia
University	universita	k1gFnSc2	universita
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejméně	málo	k6eAd3	málo
vyvinutých	vyvinutý	k2eAgNnPc2d1	vyvinuté
v	v	k7c6	v
subsaharské	subsaharský	k2eAgFnSc6d1	subsaharská
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Human	Human	k1gInSc1	Human
Rights	Rightsa	k1gFnPc2	Rightsa
Watch	Watcha	k1gFnPc2	Watcha
dále	daleko	k6eAd2	daleko
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
vládnoucí	vládnoucí	k2eAgFnSc1d1	vládnoucí
strana	strana	k1gFnSc1	strana
někdy	někdy	k6eAd1	někdy
zneužívá	zneužívat	k5eAaImIp3nS	zneužívat
mezinárodních	mezinárodní	k2eAgFnPc2d1	mezinárodní
potravinových	potravinový	k2eAgFnPc2d1	potravinová
zásob	zásoba	k1gFnPc2	zásoba
a	a	k8xC	a
nedistribuuje	distribuovat	k5eNaBmIp3nS	distribuovat
je	on	k3xPp3gInPc4	on
do	do	k7c2	do
rebely	rebel	k1gMnPc7	rebel
držených	držený	k2eAgFnPc2d1	držená
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
tak	tak	k6eAd1	tak
vyhladověla	vyhladovět	k5eAaPmAgNnP	vyhladovět
odbojová	odbojový	k2eAgNnPc1d1	odbojové
hnutí	hnutí	k1gNnPc1	hnutí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
přetrvává	přetrvávat	k5eAaImIp3nS	přetrvávat
diskriminační	diskriminační	k2eAgInSc4d1	diskriminační
přístup	přístup	k1gInSc4	přístup
k	k	k7c3	k
ženám	žena	k1gFnPc3	žena
a	a	k8xC	a
ačkoliv	ačkoliv	k8xS	ačkoliv
zákony	zákon	k1gInPc1	zákon
země	zem	k1gFnSc2	zem
považují	považovat	k5eAaImIp3nP	považovat
ženy	žena	k1gFnPc1	žena
a	a	k8xC	a
muže	muž	k1gMnPc4	muž
za	za	k7c4	za
sobě	se	k3xPyFc3	se
rovné	rovný	k2eAgFnPc4d1	rovná
<g/>
,	,	kIx,	,
tato	tento	k3xDgNnPc1	tento
ustanovení	ustanovení	k1gNnPc1	ustanovení
se	se	k3xPyFc4	se
v	v	k7c6	v
praxi	praxe	k1gFnSc6	praxe
podle	podle	k7c2	podle
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
zahraničí	zahraničí	k1gNnSc2	zahraničí
USA	USA	kA	USA
často	často	k6eAd1	často
nedodržují	dodržovat	k5eNaImIp3nP	dodržovat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
zprávě	zpráva	k1gFnSc6	zpráva
OECD	OECD	kA	OECD
Development	Development	k1gInSc1	Development
Centre	centr	k1gInSc5	centr
se	se	k3xPyFc4	se
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
žena	žena	k1gFnSc1	žena
je	být	k5eAaImIp3nS	být
stále	stále	k6eAd1	stále
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
vnímána	vnímán	k2eAgFnSc1d1	vnímána
jako	jako	k8xS	jako
podřízená	podřízený	k2eAgFnSc1d1	podřízená
muži	muž	k1gMnPc7	muž
a	a	k8xC	a
přes	přes	k7c4	přes
všechna	všechen	k3xTgNnPc4	všechen
opatření	opatření	k1gNnPc4	opatření
přijatá	přijatý	k2eAgFnSc1d1	přijatá
vládou	vláda	k1gFnSc7	vláda
je	být	k5eAaImIp3nS	být
Etiopie	Etiopie	k1gFnSc1	Etiopie
jednou	jednou	k6eAd1	jednou
z	z	k7c2	z
nejvíce	hodně	k6eAd3	hodně
tradicí	tradice	k1gFnSc7	tradice
svázaných	svázaný	k2eAgFnPc2d1	svázaná
zemí	zem	k1gFnPc2	zem
v	v	k7c6	v
Africe	Afrika	k1gFnSc6	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
Ženy	žena	k1gFnPc1	žena
tak	tak	k9	tak
mají	mít	k5eAaImIp3nP	mít
stíženou	stížený	k2eAgFnSc4d1	stížená
možnost	možnost	k1gFnSc4	možnost
vlastnit	vlastnit	k5eAaImF	vlastnit
půdu	půda	k1gFnSc4	půda
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
obvyklé	obvyklý	k2eAgFnPc1d1	obvyklá
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
regionech	region	k1gInPc6	region
nelegální	legální	k2eNgInPc1d1	nelegální
sňatky	sňatek	k1gInPc1	sňatek
s	s	k7c7	s
dívkami	dívka	k1gFnPc7	dívka
mladšími	mladý	k2eAgFnPc7d2	mladší
18	[number]	k4	18
či	či	k8xC	či
15	[number]	k4	15
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
sňatek	sňatek	k1gInSc1	sňatek
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
únosu	únos	k1gInSc2	únos
<g/>
,	,	kIx,	,
rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
je	být	k5eAaImIp3nS	být
prostituce	prostituce	k1gFnSc1	prostituce
<g/>
,	,	kIx,	,
násilí	násilí	k1gNnSc1	násilí
mezi	mezi	k7c7	mezi
partnery	partner	k1gMnPc7	partner
<g/>
,	,	kIx,	,
častá	častý	k2eAgNnPc4d1	časté
znásilnění	znásilnění	k1gNnPc4	znásilnění
<g/>
,	,	kIx,	,
ženská	ženská	k1gFnSc1	ženská
obřízka	obřízka	k1gFnSc1	obřízka
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
formy	forma	k1gFnPc1	forma
diskriminace	diskriminace	k1gFnSc2	diskriminace
<g/>
.	.	kIx.	.
</s>
<s>
Uvedené	uvedený	k2eAgNnSc1d1	uvedené
platí	platit	k5eAaImIp3nS	platit
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
venkovské	venkovský	k2eAgFnPc4d1	venkovská
oblasti	oblast	k1gFnPc4	oblast
<g/>
.	.	kIx.	.
</s>
<s>
Polygamie	polygamie	k1gFnSc1	polygamie
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
zakázaná	zakázaný	k2eAgFnSc1d1	zakázaná
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
však	však	k9	však
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
jestli	jestli	k8xS	jestli
je	být	k5eAaImIp3nS	být
nadále	nadále	k6eAd1	nadále
praktikovaná	praktikovaný	k2eAgFnSc1d1	praktikovaná
či	či	k8xC	či
případně	případně	k6eAd1	případně
ve	v	k7c6	v
společnosti	společnost	k1gFnSc2	společnost
nějakým	nějaký	k3yIgInSc7	nějaký
způsobem	způsob	k1gInSc7	způsob
přijímaná	přijímaný	k2eAgFnSc1d1	přijímaná
<g/>
.	.	kIx.	.
</s>
<s>
Organizace	organizace	k1gFnSc1	organizace
jako	jako	k9	jako
je	být	k5eAaImIp3nS	být
Dětský	dětský	k2eAgInSc1d1	dětský
fond	fond	k1gInSc1	fond
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
(	(	kIx(	(
<g/>
UNICEF	UNICEF	kA	UNICEF
<g/>
)	)	kIx)	)
či	či	k8xC	či
Save	Save	k1gFnSc1	Save
the	the	k?	the
Children	Childrna	k1gFnPc2	Childrna
uvádějí	uvádět	k5eAaImIp3nP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
nedospělých	nedospělý	k1gMnPc2	nedospělý
je	být	k5eAaImIp3nS	být
také	také	k9	také
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
špatné	špatný	k2eAgFnSc6d1	špatná
situaci	situace	k1gFnSc6	situace
–	–	k?	–
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
500	[number]	k4	500
000	[number]	k4	000
až	až	k9	až
700	[number]	k4	700
000	[number]	k4	000
dětí	dítě	k1gFnPc2	dítě
žijících	žijící	k2eAgFnPc2d1	žijící
na	na	k7c6	na
ulici	ulice	k1gFnSc6	ulice
<g/>
,	,	kIx,	,
18	[number]	k4	18
%	%	kIx~	%
etiopské	etiopský	k2eAgFnSc2d1	etiopská
dětské	dětský	k2eAgFnSc2d1	dětská
populace	populace	k1gFnSc2	populace
přišlo	přijít	k5eAaPmAgNnS	přijít
alespoň	alespoň	k9	alespoň
o	o	k7c4	o
jednoho	jeden	k4xCgMnSc4	jeden
rodiče	rodič	k1gMnSc4	rodič
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
70	[number]	k4	70
%	%	kIx~	%
dětí	dítě	k1gFnPc2	dítě
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
7	[number]	k4	7
až	až	k9	až
15	[number]	k4	15
let	léto	k1gNnPc2	léto
pracuje	pracovat	k5eAaImIp3nS	pracovat
<g/>
,	,	kIx,	,
40	[number]	k4	40
%	%	kIx~	%
z	z	k7c2	z
pracujících	pracující	k2eAgFnPc2d1	pracující
dětí	dítě	k1gFnPc2	dítě
je	být	k5eAaImIp3nS	být
mladších	mladý	k2eAgNnPc2d2	mladší
6	[number]	k4	6
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Častý	častý	k2eAgInSc1d1	častý
je	být	k5eAaImIp3nS	být
obchod	obchod	k1gInSc1	obchod
s	s	k7c7	s
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
většinou	většina	k1gFnSc7	většina
z	z	k7c2	z
vesnických	vesnický	k2eAgFnPc2d1	vesnická
oblastí	oblast	k1gFnPc2	oblast
do	do	k7c2	do
měst	město	k1gNnPc2	město
na	na	k7c4	na
různé	různý	k2eAgInPc4d1	různý
druhy	druh	k1gInPc4	druh
nucené	nucený	k2eAgFnSc2d1	nucená
práce	práce	k1gFnSc2	práce
<g/>
,	,	kIx,	,
ženy	žena	k1gFnPc1	žena
jsou	být	k5eAaImIp3nP	být
případně	případně	k6eAd1	případně
prodávány	prodávat	k5eAaImNgInP	prodávat
do	do	k7c2	do
arabských	arabský	k2eAgFnPc2d1	arabská
zemí	zem	k1gFnPc2	zem
jako	jako	k8xC	jako
domácí	domácí	k2eAgFnSc2d1	domácí
otrokyně	otrokyně	k1gFnSc2	otrokyně
nebo	nebo	k8xC	nebo
prostitutky	prostitutka	k1gFnSc2	prostitutka
<g/>
.	.	kIx.	.
</s>
<s>
Co	co	k3yInSc1	co
se	se	k3xPyFc4	se
týče	týkat	k5eAaImIp3nS	týkat
homosexuality	homosexualita	k1gFnSc2	homosexualita
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
ilegální	ilegální	k2eAgMnSc1d1	ilegální
a	a	k8xC	a
v	v	k7c6	v
etiopské	etiopský	k2eAgFnSc6d1	etiopská
společnosti	společnost	k1gFnSc6	společnost
je	být	k5eAaImIp3nS	být
brána	brána	k1gFnSc1	brána
jako	jako	k8xS	jako
tabu	tabu	k1gNnSc1	tabu
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Náboženství	náboženství	k1gNnSc2	náboženství
===	===	k?	===
</s>
</p>
<p>
<s>
Etiopie	Etiopie	k1gFnSc1	Etiopie
je	být	k5eAaImIp3nS	být
sekulárním	sekulární	k2eAgInSc7d1	sekulární
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
nejrozšířenější	rozšířený	k2eAgNnSc4d3	nejrozšířenější
náboženství	náboženství	k1gNnSc4	náboženství
patří	patřit	k5eAaImIp3nS	patřit
koptské	koptský	k2eAgNnSc4d1	Koptské
křesťanství	křesťanství	k1gNnSc4	křesťanství
miafyzitního	miafyzitní	k2eAgNnSc2d1	miafyzitní
vyznání	vyznání	k1gNnSc2	vyznání
reprezentované	reprezentovaný	k2eAgNnSc1d1	reprezentované
etiopskou	etiopský	k2eAgFnSc7d1	etiopská
pravoslavnou	pravoslavný	k2eAgFnSc7d1	pravoslavná
církví	církev	k1gFnSc7	církev
(	(	kIx(	(
<g/>
43,5	[number]	k4	43,5
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
islám	islám	k1gInSc1	islám
(	(	kIx(	(
<g/>
33,9	[number]	k4	33,9
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
protestantismus	protestantismus	k1gInSc1	protestantismus
(	(	kIx(	(
<g/>
18	[number]	k4	18
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tradiční	tradiční	k2eAgNnPc1d1	tradiční
náboženství	náboženství	k1gNnPc1	náboženství
(	(	kIx(	(
<g/>
2,6	[number]	k4	2,6
%	%	kIx~	%
<g/>
)	)	kIx)	)
a	a	k8xC	a
katolictví	katolictví	k1gNnSc4	katolictví
(	(	kIx(	(
<g/>
0,7	[number]	k4	0,7
%	%	kIx~	%
<g/>
,	,	kIx,	,
římští	římský	k2eAgMnPc1d1	římský
katolíci	katolík	k1gMnPc1	katolík
+	+	kIx~	+
Etiopská	etiopský	k2eAgFnSc1d1	etiopská
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
-	-	kIx~	-
má	mít	k5eAaImIp3nS	mít
etiopský	etiopský	k2eAgMnSc1d1	etiopský
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
koptský	koptský	k2eAgInSc4d1	koptský
ritus	ritus	k1gInSc4	ritus
-	-	kIx~	-
ten	ten	k3xDgMnSc1	ten
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
alexandrijského	alexandrijský	k2eAgInSc2d1	alexandrijský
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ostatní	ostatní	k2eAgInPc1d1	ostatní
náboženské	náboženský	k2eAgInPc1d1	náboženský
směry	směr	k1gInPc1	směr
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
0,7	[number]	k4	0,7
%	%	kIx~	%
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Stoupenci	stoupenec	k1gMnPc1	stoupenec
Etiopské	etiopský	k2eAgFnSc2d1	etiopská
církve	církev	k1gFnSc2	církev
převažují	převažovat	k5eAaImIp3nP	převažovat
zejména	zejména	k9	zejména
u	u	k7c2	u
Tigrajů	Tigraj	k1gMnPc2	Tigraj
a	a	k8xC	a
Amharů	Amhar	k1gMnPc2	Amhar
<g/>
,	,	kIx,	,
islám	islám	k1gInSc4	islám
vyznávají	vyznávat	k5eAaImIp3nP	vyznávat
naopak	naopak	k6eAd1	naopak
zejména	zejména	k9	zejména
Somálci	Somálec	k1gMnPc1	Somálec
<g/>
,	,	kIx,	,
Afarové	Afar	k1gMnPc1	Afar
<g/>
,	,	kIx,	,
Oromové	Orom	k1gMnPc1	Orom
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
ti	ten	k3xDgMnPc1	ten
žijící	žijící	k2eAgMnPc1d1	žijící
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
vrchovině	vrchovina	k1gFnSc6	vrchovina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Guragové	Guragový	k2eAgFnPc4d1	Guragový
a	a	k8xC	a
Sidamové	Sidamový	k2eAgFnPc4d1	Sidamový
na	na	k7c6	na
jihovýchodě	jihovýchod	k1gInSc6	jihovýchod
<g/>
.	.	kIx.	.
</s>
<s>
Obchodníci	obchodník	k1gMnPc1	obchodník
žijící	žijící	k2eAgMnPc1d1	žijící
ve	v	k7c6	v
velkých	velký	k2eAgNnPc6d1	velké
městech	město	k1gNnPc6	město
také	také	k9	také
často	často	k6eAd1	často
bývají	bývat	k5eAaImIp3nP	bývat
muslimy	muslim	k1gMnPc4	muslim
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jižních	jižní	k2eAgFnPc6d1	jižní
a	a	k8xC	a
jihovýchodních	jihovýchodní	k2eAgFnPc6d1	jihovýchodní
hranicích	hranice	k1gFnPc6	hranice
je	být	k5eAaImIp3nS	být
přítomno	přítomen	k2eAgNnSc1d1	přítomno
značné	značný	k2eAgNnSc1d1	značné
množství	množství	k1gNnSc1	množství
zahraničních	zahraniční	k2eAgMnPc2d1	zahraniční
misionářů	misionář	k1gMnPc2	misionář
<g/>
,	,	kIx,	,
mj.	mj.	kA	mj.
i	i	k8xC	i
římskokatolických	římskokatolický	k2eAgFnPc6d1	Římskokatolická
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
stoupají	stoupat	k5eAaImIp3nP	stoupat
v	v	k7c4	v
Etiopii	Etiopie	k1gFnSc4	Etiopie
náboženské	náboženský	k2eAgFnSc2d1	náboženská
rozepře	rozepře	k1gFnSc2	rozepře
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
si	se	k3xPyFc3	se
vyžádaly	vyžádat	k5eAaPmAgFnP	vyžádat
i	i	k8xC	i
několik	několik	k4yIc1	několik
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tradice	tradice	k1gFnSc1	tradice
etiopské	etiopský	k2eAgFnSc2d1	etiopská
pravoslavné	pravoslavný	k2eAgFnSc2d1	pravoslavná
církve	církev	k1gFnSc2	církev
sahá	sahat	k5eAaImIp3nS	sahat
do	do	k7c2	do
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
bylo	být	k5eAaImAgNnS	být
křesťanství	křesťanství	k1gNnSc1	křesťanství
přijato	přijat	k2eAgNnSc1d1	přijato
králem	král	k1gMnSc7	král
Ezanou	Ezana	k1gMnSc7	Ezana
<g/>
.	.	kIx.	.
</s>
<s>
Značnou	značný	k2eAgFnSc4d1	značná
zásluhu	zásluha	k1gFnSc4	zásluha
na	na	k7c4	na
rozšíření	rozšíření	k1gNnSc4	rozšíření
křesťanství	křesťanství	k1gNnSc2	křesťanství
měl	mít	k5eAaImAgMnS	mít
zejména	zejména	k9	zejména
Ezanův	Ezanův	k2eAgMnSc1d1	Ezanův
vychovatel	vychovatel	k1gMnSc1	vychovatel
svatý	svatý	k1gMnSc1	svatý
Frumentius	Frumentius	k1gMnSc1	Frumentius
a	a	k8xC	a
dále	daleko	k6eAd2	daleko
devět	devět	k4xCc1	devět
misionářů	misionář	k1gMnPc2	misionář
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
působili	působit	k5eAaImAgMnP	působit
v	v	k7c6	v
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgMnSc7d1	další
významným	významný	k2eAgMnSc7d1	významný
světcem	světec	k1gMnSc7	světec
je	být	k5eAaImIp3nS	být
Tekle	Tekla	k1gFnSc3	Tekla
Haymanot	Haymanota	k1gFnPc2	Haymanota
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
o	o	k7c6	o
znovunastoupení	znovunastoupení	k1gNnSc6	znovunastoupení
šalomounské	šalomounský	k2eAgFnSc2d1	šalomounská
dynastie	dynastie	k1gFnSc2	dynastie
po	po	k7c6	po
r.	r.	kA	r.
1270	[number]	k4	1270
<g/>
.	.	kIx.	.
</s>
<s>
Etiopská	etiopský	k2eAgFnSc1d1	etiopská
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
neuznává	uznávat	k5eNaImIp3nS	uznávat
Chalkedonský	Chalkedonský	k2eAgMnSc1d1	Chalkedonský
(	(	kIx(	(
<g/>
451	[number]	k4	451
<g/>
)	)	kIx)	)
a	a	k8xC	a
Druhý	druhý	k4xOgInSc1	druhý
konstantinopolský	konstantinopolský	k2eAgInSc1d1	konstantinopolský
koncil	koncil	k1gInSc1	koncil
(	(	kIx(	(
<g/>
553	[number]	k4	553
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
uzákonily	uzákonit	k5eAaPmAgFnP	uzákonit
dvojí	dvojí	k4xRgFnSc4	dvojí
přirozenost	přirozenost	k1gFnSc4	přirozenost
Kristovu	Kristův	k2eAgFnSc4d1	Kristova
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
miafyzitská	miafyzitský	k2eAgFnSc1d1	miafyzitský
/	/	kIx~	/
<g/>
původně	původně	k6eAd1	původně
monofyzitská	monofyzitský	k2eAgFnSc1d1	monofyzitská
<g/>
/	/	kIx~	/
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
Koptská	koptský	k2eAgFnSc1d1	koptská
církev	církev	k1gFnSc1	církev
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
Syrská	syrský	k2eAgFnSc1d1	Syrská
c.	c.	k?	c.
Antiochie	Antiochie	k1gFnSc1	Antiochie
(	(	kIx(	(
<g/>
čili	čili	k8xC	čili
syrsko-pravoslavná	syrskoravoslavný	k2eAgFnSc1d1	syrsko-pravoslavný
<g/>
,	,	kIx,	,
zvaná	zvaný	k2eAgFnSc1d1	zvaná
též	též	k9	též
jakobitská	jakobitský	k2eAgFnSc1d1	jakobitská
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Arménská	arménský	k2eAgFnSc1d1	arménská
apoštolská	apoštolský	k2eAgFnSc1d1	apoštolská
církev	církev	k1gFnSc1	církev
<g/>
,	,	kIx,	,
Eritrejská	eritrejský	k2eAgFnSc1d1	eritrejská
pravoslavná	pravoslavný	k2eAgFnSc1d1	pravoslavná
církev	církev	k1gFnSc1	církev
a	a	k8xC	a
dvě	dva	k4xCgFnPc1	dva
jakobitské	jakobitský	k2eAgFnPc1d1	jakobitská
církve	církev	k1gFnPc1	církev
v	v	k7c6	v
Indii	Indie	k1gFnSc6	Indie
(	(	kIx(	(
<g/>
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
je	být	k5eAaImIp3nS	být
autonomní	autonomní	k2eAgFnSc7d1	autonomní
součástí	součást	k1gFnSc7	součást
Syrské	syrský	k2eAgFnSc2d1	Syrská
c.	c.	k?	c.
Antiochie	Antiochie	k1gFnSc2	Antiochie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
izolaci	izolace	k1gFnSc4	izolace
Etiopie	Etiopie	k1gFnSc2	Etiopie
se	se	k3xPyFc4	se
církev	církev	k1gFnSc1	církev
po	po	k7c6	po
úpadku	úpadek	k1gInSc6	úpadek
Aksumu	Aksum	k1gInSc2	Aksum
<g/>
(	(	kIx(	(
<g/>
v	v	k7c6	v
8	[number]	k4	8
<g/>
.	.	kIx.	.
<g/>
stol.	stol.	k?	stol.
<g/>
)	)	kIx)	)
vrátila	vrátit	k5eAaPmAgFnS	vrátit
k	k	k7c3	k
některým	některý	k3yIgFnPc3	některý
starozákonním	starozákonní	k2eAgFnPc3d1	starozákonní
tradicím	tradice	k1gFnPc3	tradice
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
obřízce	obřízka	k1gFnSc3	obřízka
<g/>
,	,	kIx,	,
rituální	rituální	k2eAgFnSc3d1	rituální
porážce	porážka	k1gFnSc3	porážka
<g/>
,	,	kIx,	,
činění	činění	k1gNnSc4	činění
rozdílů	rozdíl	k1gInPc2	rozdíl
mezi	mezi	k7c7	mezi
čistým	čistý	k2eAgNnSc7d1	čisté
a	a	k8xC	a
nečistým	čistý	k2eNgNnSc7d1	nečisté
masem	maso	k1gNnSc7	maso
<g/>
,	,	kIx,	,
uznáváním	uznávání	k1gNnSc7	uznávání
sabatu	sabat	k1gInSc2	sabat
jako	jako	k8xS	jako
svátečního	sváteční	k2eAgInSc2d1	sváteční
dne	den	k1gInSc2	den
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Etiopská	etiopský	k2eAgFnSc1d1	etiopská
bible	bible	k1gFnSc1	bible
také	také	k9	také
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
některé	některý	k3yIgInPc4	některý
apokryfní	apokryfní	k2eAgInPc4d1	apokryfní
texty	text	k1gInPc4	text
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
bibli	bible	k1gFnSc6	bible
obvyklé	obvyklý	k2eAgFnPc1d1	obvyklá
pro	pro	k7c4	pro
západní	západní	k2eAgNnSc4d1	západní
křesťanství	křesťanství	k1gNnSc4	křesťanství
nenacházejí	nacházet	k5eNaImIp3nP	nacházet
<g/>
.	.	kIx.	.
</s>
<s>
Liturgickým	liturgický	k2eAgInSc7d1	liturgický
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
původní	původní	k2eAgInSc1d1	původní
jazyk	jazyk	k1gInSc1	jazyk
aksumské	aksumský	k2eAgFnSc2d1	aksumská
říše	říš	k1gFnSc2	říš
zvaný	zvaný	k2eAgInSc1d1	zvaný
ge	ge	k?	ge
<g/>
'	'	kIx"	'
<g/>
ez	ez	k?	ez
<g/>
.	.	kIx.	.
</s>
<s>
Církevní	církevní	k2eAgFnSc1d1	církevní
organizace	organizace	k1gFnSc1	organizace
byla	být	k5eAaImAgFnS	být
až	až	k9	až
do	do	k7c2	do
r.	r.	kA	r.
1951	[number]	k4	1951
podřízena	podřídit	k5eAaPmNgFnS	podřídit
alexandrijskému	alexandrijský	k2eAgMnSc3d1	alexandrijský
patriarchovi	patriarcha	k1gMnSc3	patriarcha
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
papeži	papež	k1gMnPc7	papež
Koptské	koptský	k2eAgFnSc2d1	koptská
církve	církev	k1gFnSc2	církev
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
původně	původně	k6eAd1	původně
vybíral	vybírat	k5eAaImAgMnS	vybírat
biskupy	biskup	k1gInPc4	biskup
<g/>
/	/	kIx~	/
poté	poté	k6eAd1	poté
metropolity	metropolita	k1gMnSc2	metropolita
<g/>
/	/	kIx~	/
etiopské	etiopský	k2eAgFnSc2d1	etiopská
církve	církev	k1gFnSc2	církev
užívajícího	užívající	k2eAgInSc2d1	užívající
titul	titul	k1gInSc4	titul
Abun	Abun	k1gInSc1	Abun
<g/>
/	/	kIx~	/
<g/>
Abuna	Abuna	k1gFnSc1	Abuna
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1951	[number]	k4	1951
je	být	k5eAaImIp3nS	být
abuna	abuna	k6eAd1	abuna
volen	volit	k5eAaImNgInS	volit
etiopskými	etiopský	k2eAgInPc7d1	etiopský
biskupy	biskup	k1gInPc7	biskup
jakožto	jakožto	k8xS	jakožto
patriarcha	patriarcha	k1gMnSc1	patriarcha
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Abuna	Abuno	k1gNnSc2	Abuno
Basilios	Basilios	k1gInSc1	Basilios
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
r.	r.	kA	r.
1958	[number]	k4	1958
je	být	k5eAaImIp3nS	být
Etiopská	etiopský	k2eAgFnSc1d1	etiopská
církev	církev	k1gFnSc1	církev
autokefální	autokefální	k2eAgFnSc1d1	autokefální
=	=	kIx~	=
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
<g/>
.	.	kIx.	.
<g/>
Její	její	k3xOp3gFnSc4	její
autokefalitu	autokefalita	k1gFnSc4	autokefalita
uznala	uznat	k5eAaPmAgFnS	uznat
Koptská	koptský	k2eAgFnSc1d1	koptská
církev	církev	k1gFnSc1	církev
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
už	už	k9	už
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
1959	[number]	k4	1959
<g/>
)	)	kIx)	)
protože	protože	k8xS	protože
Etiopská	etiopský	k2eAgFnSc1d1	etiopská
církev	církev	k1gFnSc1	církev
přiznala	přiznat	k5eAaPmAgFnS	přiznat
<g/>
(	(	kIx(	(
<g/>
a	a	k8xC	a
přiznává	přiznávat	k5eAaImIp3nS	přiznávat
dosud	dosud	k6eAd1	dosud
<g/>
)	)	kIx)	)
koptskému	koptský	k2eAgMnSc3d1	koptský
papeži	papež	k1gMnSc3	papež
čestné	čestný	k2eAgNnSc1d1	čestné
postavení	postavení	k1gNnSc1	postavení
"	"	kIx"	"
<g/>
primus	primus	k1gInSc1	primus
inter	intra	k1gFnPc2	intra
pares	paresa	k1gFnPc2	paresa
<g/>
"	"	kIx"	"
,	,	kIx,	,
tj.	tj.	kA	tj.
<g/>
první	první	k4xOgFnSc6	první
mezi	mezi	k7c7	mezi
sobě	se	k3xPyFc3	se
rovnými	rovný	k2eAgFnPc7d1	rovná
<g/>
.	.	kIx.	.
</s>
<s>
Tehdejší	tehdejší	k2eAgFnSc1d1	tehdejší
etiopská	etiopský	k2eAgFnSc1d1	etiopská
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
císařem	císař	k1gMnSc7	císař
Haile	Haile	k1gNnSc2	Haile
Selassie	Selassie	k1gFnSc2	Selassie
I.	I.	kA	I.
toto	tento	k3xDgNnSc4	tento
osamostatnění	osamostatnění	k1gNnSc4	osamostatnění
iniciovala	iniciovat	k5eAaBmAgFnS	iniciovat
a	a	k8xC	a
diplomaticky	diplomaticky	k6eAd1	diplomaticky
podporovala	podporovat	k5eAaImAgFnS	podporovat
<g/>
.	.	kIx.	.
</s>
<s>
Současným	současný	k2eAgInSc7d1	současný
abunem	abun	k1gInSc7	abun
je	být	k5eAaImIp3nS	být
P	P	kA	P
<g/>
'	'	kIx"	'
<g/>
awlos	awlos	k1gInSc1	awlos
<g/>
.	.	kIx.	.
<g/>
Etiopští	etiopský	k2eAgMnPc1d1	etiopský
muslimové	muslim	k1gMnPc1	muslim
vyznávají	vyznávat	k5eAaImIp3nP	vyznávat
sunnitskou	sunnitský	k2eAgFnSc4d1	sunnitská
formu	forma	k1gFnSc4	forma
islámu	islám	k1gInSc2	islám
<g/>
,	,	kIx,	,
převážná	převážný	k2eAgFnSc1d1	převážná
část	část	k1gFnSc1	část
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
jsou	být	k5eAaImIp3nP	být
stoupenci	stoupenec	k1gMnPc7	stoupenec
mystického	mystický	k2eAgNnSc2d1	mystické
islámského	islámský	k2eAgNnSc2d1	islámské
hnutí	hnutí	k1gNnSc2	hnutí
súfismu	súfismus	k1gInSc2	súfismus
<g/>
.	.	kIx.	.
</s>
<s>
Islám	islám	k1gInSc1	islám
se	s	k7c7	s
vzhledem	vzhled	k1gInSc7	vzhled
ke	k	k7c3	k
své	svůj	k3xOyFgFnSc3	svůj
jednotnosti	jednotnost	k1gFnSc3	jednotnost
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
nevyznačuje	vyznačovat	k5eNaImIp3nS	vyznačovat
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
výraznými	výrazný	k2eAgFnPc7d1	výrazná
odchylkami	odchylka	k1gFnPc7	odchylka
od	od	k7c2	od
oficiálního	oficiální	k2eAgNnSc2d1	oficiální
učení	učení	k1gNnSc2	učení
<g/>
.	.	kIx.	.
</s>
<s>
Rostoucí	rostoucí	k2eAgNnSc4d1	rostoucí
náboženské	náboženský	k2eAgNnSc4d1	náboženské
napětí	napětí	k1gNnSc4	napětí
mezi	mezi	k7c4	mezi
muslimy	muslim	k1gMnPc4	muslim
a	a	k8xC	a
křesťany	křesťan	k1gMnPc4	křesťan
by	by	kYmCp3nS	by
mohlo	moct	k5eAaImAgNnS	moct
být	být	k5eAaImF	být
podle	podle	k7c2	podle
etiopského	etiopský	k2eAgInSc2d1	etiopský
Nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
pro	pro	k7c4	pro
muslimské	muslimský	k2eAgFnPc4d1	muslimská
záležitosti	záležitost	k1gFnPc4	záležitost
způsobeno	způsobit	k5eAaPmNgNnS	způsobit
stoupajícím	stoupající	k2eAgInSc7d1	stoupající
vlivem	vliv	k1gInSc7	vliv
některých	některý	k3yIgFnPc2	některý
muslimských	muslimský	k2eAgFnPc2d1	muslimská
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
jsou	být	k5eAaImIp3nP	být
podporovány	podporovat	k5eAaImNgFnP	podporovat
ze	z	k7c2	z
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Židovská	židovský	k2eAgFnSc1d1	židovská
komunita	komunita	k1gFnSc1	komunita
====	====	k?	====
</s>
</p>
<p>
<s>
Samostatnou	samostatný	k2eAgFnSc4d1	samostatná
kapitolu	kapitola	k1gFnSc4	kapitola
tvoří	tvořit	k5eAaImIp3nS	tvořit
etiopská	etiopský	k2eAgFnSc1d1	etiopská
komunita	komunita	k1gFnSc1	komunita
Falašů	Falaš	k1gMnPc2	Falaš
<g/>
,	,	kIx,	,
vyznávajících	vyznávající	k2eAgMnPc2d1	vyznávající
primitivní	primitivní	k2eAgFnSc4d1	primitivní
formu	forma	k1gFnSc4	forma
judaismu	judaismus	k1gInSc2	judaismus
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
Falaš	Falaš	k1gMnSc1	Falaš
či	či	k8xC	či
Felaš	Felaš	k1gMnSc1	Felaš
je	být	k5eAaImIp3nS	být
označení	označení	k1gNnSc4	označení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
etiopským	etiopský	k2eAgMnPc3d1	etiopský
židům	žid	k1gMnPc3	žid
dali	dát	k5eAaPmAgMnP	dát
jejich	jejich	k3xOp3gMnPc1	jejich
sousedé	soused	k1gMnPc1	soused
<g/>
.	.	kIx.	.
</s>
<s>
Pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
amharštiny	amharština	k1gFnSc2	amharština
a	a	k8xC	a
znamená	znamenat	k5eAaImIp3nS	znamenat
"	"	kIx"	"
<g/>
zbavení	zbavení	k1gNnSc1	zbavení
kořenů	kořen	k1gInPc2	kořen
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
odtržení	odtržení	k1gNnSc2	odtržení
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
"	"	kIx"	"
<g/>
cizinci	cizinec	k1gMnPc1	cizinec
<g/>
–	–	k?	–
<g/>
nájezdníci	nájezdník	k1gMnPc1	nájezdník
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Falašové	Falaš	k1gMnPc1	Falaš
si	se	k3xPyFc3	se
sami	sám	k3xTgMnPc1	sám
říkali	říkat	k5eAaImAgMnP	říkat
Bejt	Bejt	k?	Bejt
Jisra	Jisr	k1gInSc2	Jisr
<g/>
'	'	kIx"	'
<g/>
el	ela	k1gFnPc2	ela
neboli	neboli	k8xC	neboli
Dům	dům	k1gInSc1	dům
Izraele	Izrael	k1gInSc2	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
do	do	k7c2	do
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
si	se	k3xPyFc3	se
Falašové	Falaš	k1gMnPc1	Falaš
zachovali	zachovat	k5eAaPmAgMnP	zachovat
silnou	silný	k2eAgFnSc4d1	silná
míru	míra	k1gFnSc4	míra
autonomie	autonomie	k1gFnSc2	autonomie
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
po	po	k7c4	po
znovunastoupení	znovunastoupení	k1gNnPc4	znovunastoupení
Šalomounovců	Šalomounovec	k1gMnPc2	Šalomounovec
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1270	[number]	k4	1270
se	se	k3xPyFc4	se
etiopští	etiopský	k2eAgMnPc1d1	etiopský
panovníci	panovník	k1gMnPc1	panovník
snažili	snažit	k5eAaImAgMnP	snažit
jejich	jejich	k3xOp3gFnSc4	jejich
nezávislost	nezávislost	k1gFnSc4	nezávislost
omezit	omezit	k5eAaPmF	omezit
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
se	se	k3xPyFc4	se
vůči	vůči	k7c3	vůči
nim	on	k3xPp3gMnPc3	on
začalo	začít	k5eAaPmAgNnS	začít
postupovat	postupovat	k5eAaImF	postupovat
se	s	k7c7	s
sílící	sílící	k2eAgFnSc7d1	sílící
intenzitou	intenzita	k1gFnSc7	intenzita
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
byli	být	k5eAaImAgMnP	být
viněni	vinit	k5eAaImNgMnP	vinit
z	z	k7c2	z
úpadku	úpadek	k1gInSc2	úpadek
aksumského	aksumský	k2eAgInSc2d1	aksumský
státu	stát	k1gInSc2	stát
za	za	k7c4	za
panování	panování	k1gNnSc4	panování
židovské	židovský	k2eAgFnSc2d1	židovská
královny	královna	k1gFnSc2	královna
Yodit	Yodita	k1gFnPc2	Yodita
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
načas	načas	k6eAd1	načas
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
etiopské	etiopský	k2eAgNnSc4d1	etiopské
území	území	k1gNnSc4	území
a	a	k8xC	a
perzekvovala	perzekvovat	k5eAaImAgFnS	perzekvovat
křesťany	křesťan	k1gMnPc4	křesťan
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
podpory	podpora	k1gFnSc2	podpora
Portugalců	Portugalec	k1gMnPc2	Portugalec
byli	být	k5eAaImAgMnP	být
definitivně	definitivně	k6eAd1	definitivně
poraženi	porazit	k5eAaPmNgMnP	porazit
etiopskou	etiopský	k2eAgFnSc7d1	etiopská
mocí	moc	k1gFnSc7	moc
roku	rok	k1gInSc2	rok
1624	[number]	k4	1624
<g/>
,	,	kIx,	,
následovalo	následovat	k5eAaImAgNnS	následovat
rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
prodávání	prodávání	k1gNnSc1	prodávání
do	do	k7c2	do
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
,	,	kIx,	,
nucené	nucený	k2eAgInPc4d1	nucený
křty	křest	k1gInPc4	křest
<g/>
,	,	kIx,	,
odmítnutí	odmítnutí	k1gNnSc4	odmítnutí
práva	právo	k1gNnSc2	právo
na	na	k7c4	na
půdu	půda	k1gFnSc4	půda
a	a	k8xC	a
konfiskace	konfiskace	k1gFnPc4	konfiskace
majetku	majetek	k1gInSc2	majetek
<g/>
.	.	kIx.	.
</s>
<s>
Byly	být	k5eAaImAgInP	být
páleny	pálit	k5eAaImNgInP	pálit
židovské	židovský	k2eAgInPc1d1	židovský
náboženské	náboženský	k2eAgInPc1d1	náboženský
texty	text	k1gInPc1	text
a	a	k8xC	a
historické	historický	k2eAgInPc1d1	historický
záznamy	záznam	k1gInPc1	záznam
ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
zahladit	zahladit	k5eAaPmF	zahladit
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
falašskou	falašský	k2eAgFnSc4d1	falašský
minulost	minulost	k1gFnSc4	minulost
<g/>
.	.	kIx.	.
</s>
<s>
Falašové	Falaš	k1gMnPc1	Falaš
se	se	k3xPyFc4	se
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
povětšinou	povětšinou	k6eAd1	povětšinou
zabývali	zabývat	k5eAaImAgMnP	zabývat
řemesly	řemeslo	k1gNnPc7	řemeslo
<g/>
,	,	kIx,	,
kterými	který	k3yRgFnPc7	který
zbytek	zbytek	k1gInSc1	zbytek
společnosti	společnost	k1gFnSc3	společnost
opovrhoval	opovrhovat	k5eAaImAgInS	opovrhovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
byli	být	k5eAaImAgMnP	být
vystaveni	vystaven	k2eAgMnPc1d1	vystaven
nucené	nucený	k2eAgFnSc2d1	nucená
separaci	separace	k1gFnSc3	separace
od	od	k7c2	od
zbytku	zbytek	k1gInSc2	zbytek
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
svoje	svůj	k3xOyFgFnPc4	svůj
náboženské	náboženský	k2eAgFnPc4d1	náboženská
potřeby	potřeba	k1gFnPc4	potřeba
užívali	užívat	k5eAaImAgMnP	užívat
jazyk	jazyk	k1gInSc4	jazyk
ge	ge	k?	ge
<g/>
'	'	kIx"	'
<g/>
ez	ez	k?	ez
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ani	ani	k8xC	ani
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
postavení	postavení	k1gNnSc1	postavení
etiopských	etiopský	k2eAgMnPc2d1	etiopský
židů	žid	k1gMnPc2	žid
příliš	příliš	k6eAd1	příliš
nezlepšilo	zlepšit	k5eNaPmAgNnS	zlepšit
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1955	[number]	k4	1955
se	se	k3xPyFc4	se
nevládní	vládní	k2eNgFnSc1d1	nevládní
Židovská	židovský	k2eAgFnSc1d1	židovská
agentura	agentura	k1gFnSc1	agentura
podílela	podílet	k5eAaImAgFnS	podílet
na	na	k7c6	na
stavbě	stavba	k1gFnSc6	stavba
škol	škola	k1gFnPc2	škola
a	a	k8xC	a
výuce	výuka	k1gFnSc3	výuka
budoucích	budoucí	k2eAgMnPc2d1	budoucí
učitelů	učitel	k1gMnPc2	učitel
Falašů	Falaš	k1gInPc2	Falaš
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1956	[number]	k4	1956
udržovala	udržovat	k5eAaImAgFnS	udržovat
Etiopie	Etiopie	k1gFnSc1	Etiopie
s	s	k7c7	s
Izraelem	Izrael	k1gInSc7	Izrael
diplomatické	diplomatický	k2eAgInPc4d1	diplomatický
styky	styk	k1gInPc4	styk
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
však	však	k9	však
byly	být	k5eAaImAgFnP	být
přerušeny	přerušit	k5eAaPmNgInP	přerušit
roku	rok	k1gInSc2	rok
1973	[number]	k4	1973
před	před	k7c7	před
vypuknutím	vypuknutí	k1gNnSc7	vypuknutí
jomkipurské	jomkipurský	k2eAgFnSc2d1	jomkipurská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Sílící	sílící	k2eAgNnSc1d1	sílící
ohrožení	ohrožení	k1gNnSc1	ohrožení
etiopských	etiopský	k2eAgMnPc2d1	etiopský
židů	žid	k1gMnPc2	žid
během	během	k7c2	během
nástupu	nástup	k1gInSc2	nástup
komunistické	komunistický	k2eAgFnSc2d1	komunistická
diktatury	diktatura	k1gFnSc2	diktatura
o	o	k7c4	o
rok	rok	k1gInSc4	rok
později	pozdě	k6eAd2	pozdě
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
jednáním	jednání	k1gNnPc3	jednání
mezi	mezi	k7c7	mezi
etiopskou	etiopský	k2eAgFnSc7d1	etiopská
a	a	k8xC	a
izraelskou	izraelský	k2eAgFnSc7d1	izraelská
vládou	vláda	k1gFnSc7	vláda
o	o	k7c6	o
zaslání	zaslání	k1gNnSc6	zaslání
prvních	první	k4xOgFnPc2	první
200	[number]	k4	200
židů	žid	k1gMnPc2	žid
do	do	k7c2	do
Izraele	Izrael	k1gInSc2	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Mengistu	Mengista	k1gMnSc4	Mengista
<g/>
,	,	kIx,	,
tehdejší	tehdejší	k2eAgMnPc4d1	tehdejší
vůdce	vůdce	k1gMnPc4	vůdce
socialistického	socialistický	k2eAgInSc2d1	socialistický
režimu	režim	k1gInSc2	režim
<g/>
,	,	kIx,	,
svolil	svolit	k5eAaPmAgMnS	svolit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
souvislosti	souvislost	k1gFnSc6	souvislost
se	s	k7c7	s
zákazem	zákaz	k1gInSc7	zákaz
praktikování	praktikování	k1gNnSc2	praktikování
judaismu	judaismus	k1gInSc2	judaismus
roku	rok	k1gInSc2	rok
1980	[number]	k4	1980
a	a	k8xC	a
opakovanými	opakovaný	k2eAgInPc7d1	opakovaný
hladomory	hladomor	k1gInPc7	hladomor
během	během	k7c2	během
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
Izrael	Izrael	k1gInSc1	Izrael
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
přesunout	přesunout	k5eAaPmF	přesunout
na	na	k7c4	na
svou	svůj	k3xOyFgFnSc4	svůj
půdu	půda	k1gFnSc4	půda
co	co	k8xS	co
největší	veliký	k2eAgNnSc4d3	veliký
množství	množství	k1gNnSc4	množství
Falašů	Falaš	k1gInPc2	Falaš
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1985	[number]	k4	1985
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1990	[number]	k4	1990
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
přestěhovat	přestěhovat	k5eAaPmF	přestěhovat
z	z	k7c2	z
Etiopie	Etiopie	k1gFnSc2	Etiopie
během	během	k7c2	během
operací	operace	k1gFnPc2	operace
Mojžíš	Mojžíš	k1gMnSc1	Mojžíš
<g/>
,	,	kIx,	,
Jozue	Jozue	k1gMnSc1	Jozue
a	a	k8xC	a
Šalomoun	Šalomoun	k1gMnSc1	Šalomoun
přes	přes	k7c4	přes
36	[number]	k4	36
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
stále	stále	k6eAd1	stále
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
zhruba	zhruba	k6eAd1	zhruba
2	[number]	k4	2
100	[number]	k4	100
židů	žid	k1gMnPc2	žid
čekajících	čekající	k2eAgMnPc2d1	čekající
na	na	k7c4	na
přesun	přesun	k1gInSc4	přesun
do	do	k7c2	do
Izraele	Izrael	k1gInSc2	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc4d1	velký
problém	problém	k1gInSc4	problém
představující	představující	k2eAgMnPc1d1	představující
původní	původní	k2eAgMnPc1d1	původní
židé	žid	k1gMnPc1	žid
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
konvertovali	konvertovat	k5eAaBmAgMnP	konvertovat
ke	k	k7c3	k
křesťanství	křesťanství	k1gNnSc3	křesťanství
<g/>
,	,	kIx,	,
zvaní	zvaní	k1gNnSc3	zvaní
Falaša	Falaš	k2eAgFnSc1d1	Falaš
Mura	mura	k1gFnSc1	mura
<g/>
,	,	kIx,	,
a	a	k8xC	a
čítající	čítající	k2eAgMnSc1d1	čítající
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
asi	asi	k9	asi
18	[number]	k4	18
000	[number]	k4	000
až	až	k9	až
26	[number]	k4	26
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
BBC	BBC	kA	BBC
uvedla	uvést	k5eAaPmAgFnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
velké	velký	k2eAgNnSc1d1	velké
množství	množství	k1gNnSc1	množství
Falašů	Falaš	k1gInPc2	Falaš
Murů	mur	k1gInPc2	mur
se	se	k3xPyFc4	se
přesunulo	přesunout	k5eAaPmAgNnS	přesunout
k	k	k7c3	k
Addis	Addis	k1gFnSc3	Addis
Abebě	Abeba	k1gFnSc3	Abeba
v	v	k7c4	v
domnění	domnění	k1gNnSc4	domnění
<g/>
,	,	kIx,	,
že	že	k8xS	že
budou	být	k5eAaImBp3nP	být
také	také	k9	také
evakuováni	evakuován	k2eAgMnPc1d1	evakuován
<g/>
.	.	kIx.	.
</s>
<s>
Izrael	Izrael	k1gInSc1	Izrael
ale	ale	k8xC	ale
jejich	jejich	k3xOp3gNnSc1	jejich
židovství	židovství	k1gNnSc1	židovství
neuznává	uznávat	k5eNaImIp3nS	uznávat
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
bídě	bída	k1gFnSc6	bída
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgNnPc2d1	další
8	[number]	k4	8
000	[number]	k4	000
až	až	k9	až
16	[number]	k4	16
000	[number]	k4	000
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
táborech	tábor	k1gInPc6	tábor
nevládních	vládní	k2eNgFnPc2d1	nevládní
organizací	organizace	k1gFnPc2	organizace
v	v	k7c6	v
Gondaru	Gondar	k1gInSc6	Gondar
bez	bez	k7c2	bez
přístupu	přístup	k1gInSc2	přístup
k	k	k7c3	k
tekoucí	tekoucí	k2eAgFnSc3d1	tekoucí
vodě	voda	k1gFnSc3	voda
a	a	k8xC	a
závislých	závislý	k2eAgInPc2d1	závislý
na	na	k7c6	na
dodávkách	dodávka	k1gFnPc6	dodávka
jídla	jídlo	k1gNnSc2	jídlo
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2008	[number]	k4	2008
izraelské	izraelský	k2eAgFnSc2d1	izraelská
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
oznámilo	oznámit	k5eAaPmAgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
prověřuje	prověřovat	k5eAaImIp3nS	prověřovat
práva	právo	k1gNnPc4	právo
na	na	k7c4	na
přesun	přesun	k1gInSc4	přesun
dalších	další	k2eAgInPc2d1	další
zhruba	zhruba	k6eAd1	zhruba
3	[number]	k4	3
000	[number]	k4	000
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kultura	kultura	k1gFnSc1	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Etiopie	Etiopie	k1gFnSc1	Etiopie
má	mít	k5eAaImIp3nS	mít
dlouho	dlouho	k6eAd1	dlouho
trvající	trvající	k2eAgFnSc4d1	trvající
kulturu	kultura	k1gFnSc4	kultura
<g/>
,	,	kIx,	,
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
ohledech	ohled	k1gInPc6	ohled
odlišnou	odlišný	k2eAgFnSc4d1	odlišná
od	od	k7c2	od
zbytku	zbytek	k1gInSc2	zbytek
Afriky	Afrika	k1gFnSc2	Afrika
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
dlouhé	dlouhý	k2eAgFnSc3d1	dlouhá
tradici	tradice	k1gFnSc3	tradice
křesťanství	křesťanství	k1gNnSc2	křesťanství
i	i	k8xC	i
takřka	takřka	k6eAd1	takřka
nepřerušenému	přerušený	k2eNgNnSc3d1	nepřerušené
státnímu	státní	k2eAgNnSc3d1	státní
vývoji	vývoj	k1gInSc3	vývoj
trvajícímu	trvající	k2eAgNnSc3d1	trvající
několik	několik	k4yIc4	několik
staletí	staletí	k1gNnPc2	staletí
<g/>
.	.	kIx.	.
</s>
<s>
Etiopané	Etiopaný	k2eAgInPc1d1	Etiopaný
užívají	užívat	k5eAaImIp3nP	užívat
vlastní	vlastní	k2eAgInPc4d1	vlastní
kalendář	kalendář	k1gInSc4	kalendář
<g/>
,	,	kIx,	,
mající	mající	k2eAgInSc4d1	mající
12	[number]	k4	12
měsíců	měsíc	k1gInPc2	měsíc
o	o	k7c6	o
30	[number]	k4	30
dnech	den	k1gInPc6	den
a	a	k8xC	a
13	[number]	k4	13
<g/>
.	.	kIx.	.
měsíc	měsíc	k1gInSc4	měsíc
trvající	trvající	k2eAgInSc4d1	trvající
pět	pět	k4xCc4	pět
nebo	nebo	k8xC	nebo
šest	šest	k4xCc4	šest
dní	den	k1gInPc2	den
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jedná	jednat	k5eAaImIp3nS	jednat
<g/>
–	–	k?	–
<g/>
li	li	k8xS	li
se	se	k3xPyFc4	se
o	o	k7c4	o
rok	rok	k1gInSc4	rok
normální	normální	k2eAgInSc4d1	normální
či	či	k8xC	či
přestupný	přestupný	k2eAgInSc4d1	přestupný
<g/>
.	.	kIx.	.
</s>
<s>
Etiopský	etiopský	k2eAgInSc1d1	etiopský
rok	rok	k1gInSc1	rok
začíná	začínat	k5eAaImIp3nS	začínat
11	[number]	k4	11
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
a	a	k8xC	a
vůči	vůči	k7c3	vůči
gregoriánskému	gregoriánský	k2eAgNnSc3d1	gregoriánské
je	být	k5eAaImIp3nS	být
zpožděn	zpozdit	k5eAaPmNgInS	zpozdit
o	o	k7c4	o
zhruba	zhruba	k6eAd1	zhruba
sedm	sedm	k4xCc4	sedm
let	léto	k1gNnPc2	léto
a	a	k8xC	a
osm	osm	k4xCc4	osm
měsíců	měsíc	k1gInPc2	měsíc
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
zvláštností	zvláštnost	k1gFnSc7	zvláštnost
je	být	k5eAaImIp3nS	být
etiopské	etiopský	k2eAgNnSc4d1	etiopské
písmo	písmo	k1gNnSc4	písmo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
z	z	k7c2	z
písma	písmo	k1gNnSc2	písmo
jihoarabských	jihoarabský	k2eAgMnPc2d1	jihoarabský
Sabejců	Sabejec	k1gMnPc2	Sabejec
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
počátky	počátek	k1gInPc1	počátek
tkví	tkvět	k5eAaImIp3nP	tkvět
v	v	k7c6	v
písmu	písmo	k1gNnSc6	písmo
Féničanů	Féničan	k1gMnPc2	Féničan
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nese	nést	k5eAaImIp3nS	nést
podobnosti	podobnost	k1gFnPc4	podobnost
s	s	k7c7	s
například	například	k6eAd1	například
písmem	písmo	k1gNnSc7	písmo
řeckým	řecký	k2eAgNnSc7d1	řecké
<g/>
,	,	kIx,	,
latinským	latinský	k2eAgNnSc7d1	latinské
či	či	k8xC	či
hebrejským	hebrejský	k2eAgNnSc7d1	hebrejské
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
jediné	jediný	k2eAgNnSc1d1	jediné
semitské	semitský	k2eAgNnSc1d1	semitské
písmo	písmo	k1gNnSc1	písmo
se	se	k3xPyFc4	se
čte	číst	k5eAaImIp3nS	číst
zleva	zleva	k6eAd1	zleva
doprava	doprava	k1gFnSc1	doprava
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
také	také	k9	také
o	o	k7c4	o
jediné	jediný	k2eAgNnSc4d1	jediné
původní	původní	k2eAgNnSc4d1	původní
africké	africký	k2eAgNnSc4d1	africké
písmo	písmo	k1gNnSc4	písmo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
dodnes	dodnes	k6eAd1	dodnes
používá	používat	k5eAaImIp3nS	používat
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
například	například	k6eAd1	například
Evropanům	Evropan	k1gMnPc3	Evropan
nemají	mít	k5eNaImIp3nP	mít
Etiopané	Etiopaný	k2eAgNnSc4d1	Etiopané
příjmení	příjmení	k1gNnSc4	příjmení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
pro	pro	k7c4	pro
lepší	dobrý	k2eAgFnSc4d2	lepší
identifikaci	identifikace	k1gFnSc4	identifikace
přidávají	přidávat	k5eAaImIp3nP	přidávat
za	za	k7c4	za
své	svůj	k3xOyFgNnSc4	svůj
jméno	jméno	k1gNnSc4	jméno
křestní	křestní	k2eAgNnSc4d1	křestní
jméno	jméno	k1gNnSc4	jméno
svého	svůj	k3xOyFgMnSc2	svůj
otce	otec	k1gMnSc2	otec
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
také	také	k9	také
dědečka	dědeček	k1gMnSc4	dědeček
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Etiopská	etiopský	k2eAgFnSc1d1	etiopská
literatura	literatura	k1gFnSc1	literatura
je	být	k5eAaImIp3nS	být
psaná	psaný	k2eAgFnSc1d1	psaná
většinou	většinou	k6eAd1	většinou
v	v	k7c6	v
jazyku	jazyk	k1gInSc6	jazyk
ge	ge	k?	ge
<g/>
'	'	kIx"	'
<g/>
ez	ez	k?	ez
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
starší	starý	k2eAgFnSc2d2	starší
literatury	literatura	k1gFnSc2	literatura
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
v	v	k7c6	v
amharštině	amharština	k1gFnSc6	amharština
(	(	kIx(	(
<g/>
v	v	k7c6	v
případě	případ	k1gInSc6	případ
literatury	literatura	k1gFnSc2	literatura
novější	nový	k2eAgFnSc2d2	novější
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jazyku	jazyk	k1gInSc6	jazyk
ge	ge	k?	ge
<g/>
'	'	kIx"	'
<g/>
ez	ez	k?	ez
je	být	k5eAaImIp3nS	být
napsána	napsat	k5eAaBmNgFnS	napsat
i	i	k9	i
etiopská	etiopský	k2eAgFnSc1d1	etiopská
bible	bible	k1gFnSc1	bible
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
ohledech	ohled	k1gInPc6	ohled
odlišuje	odlišovat	k5eAaImIp3nS	odlišovat
od	od	k7c2	od
bible	bible	k1gFnSc2	bible
západní	západní	k2eAgFnSc2d1	západní
<g/>
.	.	kIx.	.
</s>
<s>
Jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejvýznamnějších	významný	k2eAgFnPc2d3	nejvýznamnější
literárních	literární	k2eAgFnPc2d1	literární
prací	práce	k1gFnPc2	práce
představuje	představovat	k5eAaImIp3nS	představovat
kniha	kniha	k1gFnSc1	kniha
Kibre	Kibr	k1gInSc5	Kibr
negest	gest	k5eNaPmF	gest
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Velikost	velikost	k1gFnSc1	velikost
králů	král	k1gMnPc2	král
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
pocházející	pocházející	k2eAgFnSc1d1	pocházející
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ceněna	ceněn	k2eAgFnSc1d1	ceněna
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
také	také	k9	také
ge	ge	k?	ge
<g/>
'	'	kIx"	'
<g/>
ezská	ezský	k2eAgFnSc1d1	ezský
poezie	poezie	k1gFnSc1	poezie
<g/>
,	,	kIx,	,
vzkvétající	vzkvétající	k2eAgFnSc1d1	vzkvétající
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
čtená	čtený	k2eAgFnSc1d1	čtená
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
klášterech	klášter	k1gInPc6	klášter
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
užívat	užívat	k5eAaImF	užívat
pro	pro	k7c4	pro
literární	literární	k2eAgInPc4d1	literární
účely	účel	k1gInPc4	účel
amharský	amharský	k2eAgInSc4d1	amharský
jazyk	jazyk	k1gInSc4	jazyk
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
převážil	převážit	k5eAaPmAgInS	převážit
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
významné	významný	k2eAgFnPc4d1	významná
autory	autor	k1gMnPc7	autor
novodobé	novodobý	k2eAgFnSc2d1	novodobá
Etiopie	Etiopie	k1gFnSc2	Etiopie
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Makonnen	Makonnen	k2eAgMnSc1d1	Makonnen
Endalkachew	Endalkachew	k1gMnSc1	Endalkachew
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
alegorických	alegorický	k2eAgFnPc2d1	alegorická
novel	novela	k1gFnPc2	novela
a	a	k8xC	a
divadelních	divadelní	k2eAgFnPc2d1	divadelní
her	hra	k1gFnPc2	hra
<g/>
,	,	kIx,	,
Kebede	Kebed	k1gMnSc5	Kebed
Mikael	Mikael	k1gMnSc1	Mikael
<g/>
,	,	kIx,	,
tvůrce	tvůrce	k1gMnSc1	tvůrce
veršovaných	veršovaný	k2eAgFnPc2d1	veršovaná
dramat	drama	k1gNnPc2	drama
<g/>
,	,	kIx,	,
a	a	k8xC	a
Tekle	Tekla	k1gFnSc3	Tekla
Tsodek	Tsodka	k1gFnPc2	Tsodka
Makuria	Makurium	k1gNnSc2	Makurium
<g/>
,	,	kIx,	,
zaměřující	zaměřující	k2eAgFnSc2d1	zaměřující
se	se	k3xPyFc4	se
na	na	k7c4	na
kratší	krátký	k2eAgInPc4d2	kratší
příběhy	příběh	k1gInPc4	příběh
<g/>
.	.	kIx.	.
<g/>
Původní	původní	k2eAgFnSc1d1	původní
etiopská	etiopský	k2eAgFnSc1d1	etiopská
architektura	architektura	k1gFnSc1	architektura
se	se	k3xPyFc4	se
taktéž	taktéž	k?	taktéž
vyvíjela	vyvíjet	k5eAaImAgFnS	vyvíjet
po	po	k7c6	po
staletí	staletí	k1gNnSc6	staletí
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
doby	doba	k1gFnSc2	doba
aksumské	aksumský	k2eAgFnSc2d1	aksumská
říše	říš	k1gFnSc2	říš
se	se	k3xPyFc4	se
například	například	k6eAd1	například
v	v	k7c6	v
jejím	její	k3xOp3gNnSc6	její
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
Aksumu	Aksum	k1gInSc6	Aksum
<g/>
,	,	kIx,	,
dochovaly	dochovat	k5eAaPmAgInP	dochovat
chrámy	chrám	k1gInPc1	chrám
<g/>
,	,	kIx,	,
paláce	palác	k1gInPc1	palác
či	či	k8xC	či
obelisky	obelisk	k1gInPc1	obelisk
<g/>
,	,	kIx,	,
za	za	k7c2	za
dynastie	dynastie	k1gFnSc2	dynastie
Zagwe	Zagwe	k1gNnSc2	Zagwe
dosáhla	dosáhnout	k5eAaPmAgFnS	dosáhnout
vrcholu	vrchol	k1gInSc2	vrchol
klášterní	klášterní	k2eAgFnSc1d1	klášterní
architektura	architektura	k1gFnSc1	architektura
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
tesané	tesaný	k2eAgInPc1d1	tesaný
křesťanské	křesťanský	k2eAgInPc1d1	křesťanský
chrámy	chrám	k1gInPc1	chrám
v	v	k7c6	v
Lalibele	Lalibela	k1gFnSc6	Lalibela
a	a	k8xC	a
další	další	k2eAgNnPc4d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
palácový	palácový	k2eAgInSc4d1	palácový
komplex	komplex	k1gInSc4	komplex
v	v	k7c6	v
Gondaru	Gondar	k1gInSc6	Gondar
svědčí	svědčit	k5eAaImIp3nS	svědčit
o	o	k7c6	o
etiopském	etiopský	k2eAgNnSc6d1	etiopské
stavitelském	stavitelský	k2eAgNnSc6d1	stavitelské
umění	umění	k1gNnSc6	umění
v	v	k7c6	v
době	doba	k1gFnSc6	doba
druhého	druhý	k4xOgNnSc2	druhý
panování	panování	k1gNnSc2	panování
šalomounské	šalomounský	k2eAgFnSc2d1	šalomounská
dynastie	dynastie	k1gFnSc2	dynastie
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
památky	památka	k1gFnPc1	památka
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
součástí	součást	k1gFnPc2	součást
světového	světový	k2eAgNnSc2d1	světové
dědictví	dědictví	k1gNnSc2	dědictví
UNESCO	UNESCO	kA	UNESCO
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
hudby	hudba	k1gFnSc2	hudba
se	se	k3xPyFc4	se
nejstarší	starý	k2eAgFnSc1d3	nejstarší
tradice	tradice	k1gFnSc1	tradice
váže	vázat	k5eAaImIp3nS	vázat
na	na	k7c4	na
práci	práce	k1gFnSc4	práce
svatého	svatý	k2eAgMnSc2d1	svatý
Yareda	Yared	k1gMnSc2	Yared
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zavedl	zavést	k5eAaPmAgMnS	zavést
vlastní	vlastní	k2eAgInSc4d1	vlastní
notační	notační	k2eAgInSc4d1	notační
systém	systém	k1gInSc4	systém
<g/>
,	,	kIx,	,
s	s	k7c7	s
jehož	jehož	k3xOyRp3gFnSc7	jehož
pomocí	pomoc	k1gFnSc7	pomoc
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
a	a	k8xC	a
zaznamenal	zaznamenat	k5eAaPmAgMnS	zaznamenat
mnoho	mnoho	k4c4	mnoho
liturgických	liturgický	k2eAgFnPc2d1	liturgická
skladeb	skladba	k1gFnPc2	skladba
<g/>
,	,	kIx,	,
dochovala	dochovat	k5eAaPmAgFnS	dochovat
se	se	k3xPyFc4	se
po	po	k7c6	po
něm	on	k3xPp3gMnSc6	on
i	i	k9	i
řada	řada	k1gFnSc1	řada
básní	báseň	k1gFnPc2	báseň
<g/>
.	.	kIx.	.
</s>
<s>
Typická	typický	k2eAgFnSc1d1	typická
moderní	moderní	k2eAgFnSc1d1	moderní
etiopská	etiopský	k2eAgFnSc1d1	etiopská
hudba	hudba	k1gFnSc1	hudba
mívá	mívat	k5eAaImIp3nS	mívat
často	často	k6eAd1	často
dvojí	dvojí	k4xRgInSc4	dvojí
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Píseň	píseň	k1gFnSc1	píseň
se	se	k3xPyFc4	se
na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
úrovni	úroveň	k1gFnSc6	úroveň
zabývá	zabývat	k5eAaImIp3nS	zabývat
spirituální	spirituální	k2eAgFnSc7d1	spirituální
tematikou	tematika	k1gFnSc7	tematika
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
její	její	k3xOp3gInSc1	její
druhý	druhý	k4xOgInSc1	druhý
význam	význam	k1gInSc1	význam
má	mít	k5eAaImIp3nS	mít
povahu	povaha	k1gFnSc4	povaha
spíše	spíše	k9	spíše
osobní	osobní	k2eAgInPc1d1	osobní
<g/>
.	.	kIx.	.
</s>
<s>
Známá	známá	k1gFnSc1	známá
v	v	k7c4	v
užití	užití	k1gNnSc4	užití
tohoto	tento	k3xDgInSc2	tento
stylu	styl	k1gInSc2	styl
je	být	k5eAaImIp3nS	být
například	například	k6eAd1	například
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
Mary	Mary	k1gFnSc1	Mary
Armede	Armed	k1gMnSc5	Armed
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zahraničních	zahraniční	k2eAgInPc2d1	zahraniční
vlivů	vliv	k1gInPc2	vliv
se	se	k3xPyFc4	se
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
v	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
nejvíce	hodně	k6eAd3	hodně
uplatnily	uplatnit	k5eAaPmAgInP	uplatnit
styly	styl	k1gInPc1	styl
užívající	užívající	k2eAgInPc1d1	užívající
ve	v	k7c6	v
velké	velký	k2eAgFnSc6d1	velká
míře	míra	k1gFnSc6	míra
žesťové	žesťový	k2eAgInPc4d1	žesťový
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
soulová	soulový	k2eAgFnSc1d1	soulová
hudba	hudba	k1gFnSc1	hudba
<g/>
.	.	kIx.	.
</s>
<s>
Nejvýznamnějšími	významný	k2eAgMnPc7d3	nejvýznamnější
současnými	současný	k2eAgMnPc7d1	současný
zpěváky	zpěvák	k1gMnPc7	zpěvák
jsou	být	k5eAaImIp3nP	být
Neway	Newaa	k1gFnPc1	Newaa
Debebe	Debeb	k1gInSc5	Debeb
a	a	k8xC	a
Netsanet	Netsanet	k1gInSc4	Netsanet
Mellesse	Mellesse	k1gFnSc2	Mellesse
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
skupiny	skupina	k1gFnPc1	skupina
Wallias	Walliasa	k1gFnPc2	Walliasa
a	a	k8xC	a
Roha	Roh	k1gInSc2	Roh
<g/>
.	.	kIx.	.
<g/>
Pro	pro	k7c4	pro
etiopskou	etiopský	k2eAgFnSc4d1	etiopská
kuchyni	kuchyně	k1gFnSc4	kuchyně
jsou	být	k5eAaImIp3nP	být
typická	typický	k2eAgNnPc1d1	typické
silně	silně	k6eAd1	silně
kořeněná	kořeněný	k2eAgNnPc1d1	kořeněné
jídla	jídlo	k1gNnPc1	jídlo
<g/>
,	,	kIx,	,
časté	častý	k2eAgInPc1d1	častý
jsou	být	k5eAaImIp3nP	být
masné	masný	k2eAgInPc4d1	masný
pokrmy	pokrm	k1gInPc4	pokrm
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
vepřového	vepřové	k1gNnSc2	vepřové
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
nejí	jíst	k5eNaImIp3nS	jíst
většina	většina	k1gFnSc1	většina
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Konzumaci	konzumace	k1gFnSc4	konzumace
masa	maso	k1gNnSc2	maso
také	také	k9	také
omezují	omezovat	k5eAaImIp3nP	omezovat
křesťanské	křesťanský	k2eAgInPc1d1	křesťanský
i	i	k8xC	i
muslimské	muslimský	k2eAgInPc1d1	muslimský
půsty	půst	k1gInPc1	půst
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jsou	být	k5eAaImIp3nP	být
obvyklé	obvyklý	k2eAgInPc1d1	obvyklý
i	i	k8xC	i
zeleninové	zeleninový	k2eAgInPc1d1	zeleninový
pokrmy	pokrm	k1gInPc1	pokrm
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
luštěnina	luštěnina	k1gFnSc1	luštěnina
<g/>
.	.	kIx.	.
</s>
<s>
Etiopský	etiopský	k2eAgInSc1d1	etiopský
chléb	chléb	k1gInSc1	chléb
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
indžera	indžera	k1gFnSc1	indžera
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
tenkou	tenký	k2eAgFnSc4d1	tenká
kvašenou	kvašený	k2eAgFnSc4d1	kvašená
chlebovou	chlebový	k2eAgFnSc4d1	chlebová
placku	placka	k1gFnSc4	placka
vyráběnou	vyráběný	k2eAgFnSc4d1	vyráběná
z	z	k7c2	z
plodiny	plodina	k1gFnSc2	plodina
teff	teff	k1gInSc1	teff
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
se	se	k3xPyFc4	se
často	často	k6eAd1	často
jí	on	k3xPp3gFnSc3	on
masová	masový	k2eAgFnSc1d1	masová
směs	směs	k1gFnSc1	směs
podobná	podobný	k2eAgFnSc1d1	podobná
guláši	guláš	k1gInSc6	guláš
zvaná	zvaný	k2eAgFnSc1d1	zvaná
wet	wet	k?	wet
<g/>
.	.	kIx.	.
</s>
<s>
Pálivá	pálivý	k2eAgNnPc1d1	pálivé
jídla	jídlo	k1gNnPc1	jídlo
jsou	být	k5eAaImIp3nP	být
často	často	k6eAd1	často
doplněna	doplnit	k5eAaPmNgFnS	doplnit
kysanou	kysaný	k2eAgFnSc7d1	kysaná
smetanou	smetana	k1gFnSc7	smetana
nebo	nebo	k8xC	nebo
sýrem	sýr	k1gInSc7	sýr
ajib	ajiba	k1gFnPc2	ajiba
na	na	k7c4	na
zmírnění	zmírnění	k1gNnSc4	zmírnění
ostré	ostrý	k2eAgFnSc2d1	ostrá
chuti	chuť	k1gFnSc2	chuť
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
jídlu	jídlo	k1gNnSc3	jídlo
se	se	k3xPyFc4	se
podává	podávat	k5eAaImIp3nS	podávat
voda	voda	k1gFnSc1	voda
<g/>
,	,	kIx,	,
pivo	pivo	k1gNnSc1	pivo
či	či	k8xC	či
etiopská	etiopský	k2eAgFnSc1d1	etiopská
medovina	medovina	k1gFnSc1	medovina
(	(	kIx(	(
<g/>
medž	medž	k1gFnSc1	medž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
stolování	stolování	k1gNnSc6	stolování
se	se	k3xPyFc4	se
nepoužívají	používat	k5eNaImIp3nP	používat
příbory	příbor	k1gInPc7	příbor
<g/>
,	,	kIx,	,
pokrm	pokrm	k1gInSc1	pokrm
se	se	k3xPyFc4	se
většinou	většina	k1gFnSc7	většina
zabalí	zabalit	k5eAaPmIp3nP	zabalit
do	do	k7c2	do
chlebové	chlebový	k2eAgFnSc2d1	chlebová
placky	placka	k1gFnSc2	placka
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
jídle	jídlo	k1gNnSc6	jídlo
se	se	k3xPyFc4	se
často	často	k6eAd1	často
připravuje	připravovat	k5eAaImIp3nS	připravovat
káva	káva	k1gFnSc1	káva
s	s	k7c7	s
hřebíčkem	hřebíček	k1gInSc7	hřebíček
<g/>
,	,	kIx,	,
kardamonem	kardamon	k1gInSc7	kardamon
a	a	k8xC	a
skořicí	skořice	k1gFnSc7	skořice
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
oslazená	oslazená	k1gFnSc1	oslazená
medem	med	k1gInSc7	med
<g/>
.	.	kIx.	.
</s>
<s>
Oblíbeným	oblíbený	k2eAgInSc7d1	oblíbený
nápojem	nápoj	k1gInSc7	nápoj
je	být	k5eAaImIp3nS	být
také	také	k9	také
čaj	čaj	k1gInSc1	čaj
<g/>
.	.	kIx.	.
<g/>
Etiopie	Etiopie	k1gFnSc1	Etiopie
je	být	k5eAaImIp3nS	být
známá	známý	k2eAgFnSc1d1	známá
pro	pro	k7c4	pro
mnoho	mnoho	k4c4	mnoho
význačných	význačný	k2eAgMnPc2d1	význačný
sportovců	sportovec	k1gMnPc2	sportovec
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
běžce	běžec	k1gMnPc4	běžec
Abebe	Abeb	k1gInSc5	Abeb
Bikilu	Bikil	k1gInSc3	Bikil
<g/>
,	,	kIx,	,
Mama	mama	k1gFnSc1	mama
Wolde	Wold	k1gInSc5	Wold
<g/>
,	,	kIx,	,
Haile	Haile	k1gNnPc7	Haile
Gebrselassie	Gebrselassie	k1gFnSc2	Gebrselassie
či	či	k8xC	či
Kenenisu	Kenenis	k1gInSc2	Kenenis
Bekele	Bekel	k1gInSc2	Bekel
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
získali	získat	k5eAaPmAgMnP	získat
i	i	k9	i
několik	několik	k4yIc4	několik
zlatých	zlatý	k2eAgFnPc2d1	zlatá
olympijských	olympijský	k2eAgFnPc2d1	olympijská
medailí	medaile	k1gFnPc2	medaile
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
významným	významný	k2eAgInSc7d1	významný
sportem	sport	k1gInSc7	sport
byl	být	k5eAaImAgInS	být
dříve	dříve	k6eAd2	dříve
fotbal	fotbal	k1gInSc1	fotbal
a	a	k8xC	a
Etiopie	Etiopie	k1gFnSc1	Etiopie
se	se	k3xPyFc4	se
významně	významně	k6eAd1	významně
podílela	podílet	k5eAaImAgFnS	podílet
i	i	k9	i
na	na	k7c6	na
vzniku	vznik	k1gInSc6	vznik
Afrického	africký	k2eAgInSc2d1	africký
fotbalového	fotbalový	k2eAgInSc2d1	fotbalový
svazu	svaz	k1gInSc2	svaz
<g/>
.	.	kIx.	.
</s>
<s>
Známým	známý	k2eAgMnSc7d1	známý
fotbalistou	fotbalista	k1gMnSc7	fotbalista
a	a	k8xC	a
národním	národní	k2eAgMnSc7d1	národní
hrdinou	hrdina	k1gMnSc7	hrdina
byl	být	k5eAaImAgMnS	být
například	například	k6eAd1	například
Worku	Worka	k1gMnSc4	Worka
Mengistu	Mengista	k1gMnSc4	Mengista
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
roce	rok	k1gInSc6	rok
1970	[number]	k4	1970
však	však	k9	však
etiopský	etiopský	k2eAgInSc1d1	etiopský
fotbal	fotbal	k1gInSc1	fotbal
začal	začít	k5eAaPmAgInS	začít
upadat	upadat	k5eAaPmF	upadat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Vzdělání	vzdělání	k1gNnSc1	vzdělání
a	a	k8xC	a
věda	věda	k1gFnSc1	věda
===	===	k?	===
</s>
</p>
<p>
<s>
Vzdělání	vzdělání	k1gNnSc1	vzdělání
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
základního	základní	k2eAgInSc2d1	základní
po	po	k7c4	po
vysokoškolské	vysokoškolský	k2eAgNnSc4d1	vysokoškolské
bezplatné	bezplatný	k2eAgNnSc4d1	bezplatné
<g/>
.	.	kIx.	.
</s>
<s>
Povinná	povinný	k2eAgFnSc1d1	povinná
školní	školní	k2eAgFnSc1d1	školní
docházka	docházka	k1gFnSc1	docházka
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
sedmi	sedm	k4xCc2	sedm
do	do	k7c2	do
třinácti	třináct	k4xCc2	třináct
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Střední	střední	k2eAgNnSc1d1	střední
a	a	k8xC	a
vyšší	vysoký	k2eAgNnSc1d2	vyšší
vzdělání	vzdělání	k1gNnSc1	vzdělání
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
získat	získat	k5eAaPmF	získat
pouze	pouze	k6eAd1	pouze
ve	v	k7c6	v
větších	veliký	k2eAgNnPc6d2	veliký
městech	město	k1gNnPc6	město
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
roku	rok	k1gInSc3	rok
2005	[number]	k4	2005
bylo	být	k5eAaImAgNnS	být
zhruba	zhruba	k6eAd1	zhruba
35,9	[number]	k4	35,9
%	%	kIx~	%
gramotných	gramotný	k2eAgFnPc2d1	gramotná
u	u	k7c2	u
dospělých	dospělý	k2eAgMnPc2d1	dospělý
a	a	k8xC	a
49,9	[number]	k4	49,9
%	%	kIx~	%
u	u	k7c2	u
mladší	mladý	k2eAgFnSc2d2	mladší
generace	generace	k1gFnSc2	generace
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
začíná	začínat	k5eAaImIp3nS	začínat
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
ve	v	k7c6	v
věku	věk	k1gInSc6	věk
sedmi	sedm	k4xCc2	sedm
let	léto	k1gNnPc2	léto
<g/>
,	,	kIx,	,
střední	střední	k2eAgFnSc7d1	střední
pro	pro	k7c4	pro
patnáctileté	patnáctiletý	k2eAgNnSc4d1	patnáctileté
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
10	[number]	k4	10
<g/>
.	.	kIx.	.
ročníku	ročník	k1gInSc2	ročník
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
střední	střední	k2eAgFnSc2d1	střední
školy	škola	k1gFnSc2	škola
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
žáci	žák	k1gMnPc1	žák
rozděleni	rozdělen	k2eAgMnPc1d1	rozdělen
na	na	k7c4	na
budoucí	budoucí	k2eAgMnPc4d1	budoucí
studenty	student	k1gMnPc4	student
univerzit	univerzita	k1gFnPc2	univerzita
a	a	k8xC	a
na	na	k7c4	na
ty	ten	k3xDgMnPc4	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
získají	získat	k5eAaPmIp3nP	získat
vyučení	vyučení	k1gNnSc4	vyučení
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
polytechniky	polytechnika	k1gFnSc2	polytechnika
<g/>
,	,	kIx,	,
pedagogiky	pedagogika	k1gFnSc2	pedagogika
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc7d1	hlavní
univerzitou	univerzita	k1gFnSc7	univerzita
je	být	k5eAaImIp3nS	být
Addis	Addis	k1gFnSc1	Addis
Abebská	Abebský	k2eAgFnSc1d1	Abebský
univerzita	univerzita	k1gFnSc1	univerzita
<g/>
,	,	kIx,	,
další	další	k2eAgFnSc1d1	další
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
některých	některý	k3yIgNnPc6	některý
hlavních	hlavní	k2eAgNnPc6d1	hlavní
městech	město	k1gNnPc6	město
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
federálních	federální	k2eAgInPc2d1	federální
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
v	v	k7c6	v
Addis	Addis	k1gFnSc6	Addis
Abebě	Abeba	k1gFnSc6	Abeba
existuje	existovat	k5eAaImIp3nS	existovat
také	také	k9	také
několik	několik	k4yIc4	několik
dalších	další	k2eAgMnPc2d1	další
<g/>
,	,	kIx,	,
převážně	převážně	k6eAd1	převážně
soukromých	soukromý	k2eAgFnPc2d1	soukromá
<g/>
,	,	kIx,	,
vysokých	vysoký	k2eAgFnPc2d1	vysoká
škol	škola	k1gFnPc2	škola
<g/>
.	.	kIx.	.
<g/>
Většina	většina	k1gFnSc1	většina
významných	významný	k2eAgFnPc2d1	významná
kulturních	kulturní	k2eAgFnPc2d1	kulturní
a	a	k8xC	a
vědeckých	vědecký	k2eAgFnPc2d1	vědecká
institucí	instituce	k1gFnPc2	instituce
je	být	k5eAaImIp3nS	být
situována	situovat	k5eAaBmNgFnS	situovat
v	v	k7c6	v
Addis	Addis	k1gFnSc6	Addis
Abebě	Abeba	k1gFnSc6	Abeba
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
ně	on	k3xPp3gFnPc4	on
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
Národní	národní	k2eAgNnSc1d1	národní
muzeum	muzeum	k1gNnSc1	muzeum
Etiopie	Etiopie	k1gFnSc1	Etiopie
<g/>
,	,	kIx,	,
Národní	národní	k2eAgInSc1d1	národní
archiv	archiv	k1gInSc1	archiv
a	a	k8xC	a
knihovna	knihovna	k1gFnSc1	knihovna
Etiopie	Etiopie	k1gFnSc1	Etiopie
a	a	k8xC	a
Muzeum	muzeum	k1gNnSc1	muzeum
institutu	institut	k1gInSc2	institut
etiopských	etiopský	k2eAgNnPc2d1	etiopské
studií	studio	k1gNnPc2	studio
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c4	po
dlouhou	dlouhý	k2eAgFnSc4d1	dlouhá
dobu	doba	k1gFnSc4	doba
neexistovala	existovat	k5eNaImAgFnS	existovat
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
akademie	akademie	k1gFnSc2	akademie
věd	věda	k1gFnPc2	věda
<g/>
.	.	kIx.	.
</s>
<s>
Poprvé	poprvé	k6eAd1	poprvé
byla	být	k5eAaImAgFnS	být
myšlenka	myšlenka	k1gFnSc1	myšlenka
založení	založení	k1gNnSc2	založení
takové	takový	k3xDgFnSc2	takový
organizace	organizace	k1gFnSc2	organizace
pojata	pojmout	k5eAaPmNgFnS	pojmout
již	již	k6eAd1	již
v	v	k7c6	v
šedesátých	šedesátý	k4xOgNnPc6	šedesátý
letech	léto	k1gNnPc6	léto
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
následné	následný	k2eAgInPc1d1	následný
politické	politický	k2eAgInPc1d1	politický
otřesy	otřes	k1gInPc1	otřes
jakýkoliv	jakýkoliv	k3yIgInSc4	jakýkoliv
rozvoj	rozvoj	k1gInSc4	rozvoj
znemožnily	znemožnit	k5eAaPmAgInP	znemožnit
<g/>
.	.	kIx.	.
</s>
<s>
Etiopská	etiopský	k2eAgFnSc1d1	etiopská
akademie	akademie	k1gFnSc1	akademie
věd	věda	k1gFnPc2	věda
tak	tak	k9	tak
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
až	až	k9	až
roku	rok	k1gInSc2	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Poznámky	poznámka	k1gFnSc2	poznámka
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Country	country	k2eAgInSc1d1	country
presentation	presentation	k1gInSc1	presentation
by	by	kYmCp3nS	by
the	the	k?	the
government	government	k1gInSc1	government
of	of	k?	of
Ethiopia	Ethiopia	k1gFnSc1	Ethiopia
<g/>
.	.	kIx.	.
</s>
<s>
Geneva	Geneva	k1gFnSc1	Geneva
<g/>
:	:	kIx,	:
United	United	k1gInSc1	United
Nations	Nationsa	k1gFnPc2	Nationsa
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Etiopie	Etiopie	k1gFnSc1	Etiopie
<g/>
:	:	kIx,	:
bulletin	bulletin	k1gInSc1	bulletin
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Společnost	společnost	k1gFnSc1	společnost
přátel	přítel	k1gMnPc2	přítel
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
1992	[number]	k4	1992
<g/>
-	-	kIx~	-
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Etiopská	etiopský	k2eAgFnSc1d1	etiopská
kniha	kniha	k1gFnSc1	kniha
mrtvých	mrtvý	k1gMnPc2	mrtvý
a	a	k8xC	a
obsahově	obsahově	k6eAd1	obsahově
spřízněná	spřízněný	k2eAgNnPc1d1	spřízněné
díla	dílo	k1gNnPc1	dílo
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Jaromír	Jaromír	k1gMnSc1	Jaromír
Kozák	Kozák	k1gMnSc1	Kozák
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Eminent	Eminent	k1gMnSc1	Eminent
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7281	[number]	k4	7281
<g/>
-	-	kIx~	-
<g/>
151	[number]	k4	151
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Moudrost	moudrost	k1gFnSc1	moudrost
Etiopie	Etiopie	k1gFnSc2	Etiopie
:	:	kIx,	:
amharská	amharský	k2eAgNnPc4d1	amharský
přísloví	přísloví	k1gNnPc4	přísloví
<g/>
.	.	kIx.	.
</s>
<s>
Příprava	příprava	k1gFnSc1	příprava
vydání	vydání	k1gNnSc2	vydání
Pavel	Pavel	k1gMnSc1	Pavel
Mikeš	Mikeš	k1gMnSc1	Mikeš
<g/>
,	,	kIx,	,
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Poláček	Poláček	k1gMnSc1	Poláček
<g/>
.	.	kIx.	.
</s>
<s>
Červený	červený	k2eAgInSc1d1	červený
Kostelec	Kostelec	k1gInSc1	Kostelec
<g/>
:	:	kIx,	:
Pavel	Pavel	k1gMnSc1	Pavel
Mervart	Mervarta	k1gFnPc2	Mervarta
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
86818	[number]	k4	86818
<g/>
-	-	kIx~	-
<g/>
79	[number]	k4	79
<g/>
-	-	kIx~	-
<g/>
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ethiopia	Ethiopia	k1gFnSc1	Ethiopia
/	/	kIx~	/
United	United	k1gInSc1	United
Nations	Nationsa	k1gFnPc2	Nationsa
Conference	Conferenec	k1gInSc2	Conferenec
on	on	k3xPp3gMnSc1	on
Trade	Trad	k1gInSc5	Trad
and	and	k?	and
Development	Development	k1gInSc1	Development
<g/>
.	.	kIx.	.
</s>
<s>
Investment	Investment	k1gInSc1	Investment
and	and	k?	and
innovation	innovation	k1gInSc1	innovation
policy	polica	k1gFnSc2	polica
review	review	k?	review
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
United	United	k1gInSc1	United
Nations	Nationsa	k1gFnPc2	Nationsa
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FIALA	Fiala	k1gMnSc1	Fiala
<g/>
,	,	kIx,	,
Vlastimil	Vlastimil	k1gMnSc1	Vlastimil
<g/>
;	;	kIx,	;
PROUZA	PROUZA	kA	PROUZA
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
;	;	kIx,	;
ZÁHOŘÍK	ZÁHOŘÍK	kA	ZÁHOŘÍK
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Politické	politický	k2eAgNnSc1d1	politické
stranictví	stranictví	k1gNnSc1	stranictví
v	v	k7c6	v
subsaharské	subsaharský	k2eAgFnSc6d1	subsaharská
Africe	Afrika	k1gFnSc6	Afrika
:	:	kIx,	:
Botswana	Botswana	k1gFnSc1	Botswana
<g/>
,	,	kIx,	,
Malawi	Malawi	k1gNnSc1	Malawi
<g/>
,	,	kIx,	,
Ghana	Ghana	k1gFnSc1	Ghana
<g/>
,	,	kIx,	,
Etiopie	Etiopie	k1gFnSc1	Etiopie
a	a	k8xC	a
Eritrea	Eritrea	k1gFnSc1	Eritrea
<g/>
.	.	kIx.	.
</s>
<s>
Olomouc	Olomouc	k1gFnSc1	Olomouc
<g/>
:	:	kIx,	:
Iuridicum	Iuridicum	k1gInSc1	Iuridicum
Olomoucense	Olomoucense	k1gFnSc2	Olomoucense
ve	v	k7c6	v
spolupráci	spolupráce	k1gFnSc6	spolupráce
s	s	k7c7	s
Právnickou	právnický	k2eAgFnSc7d1	právnická
fakultou	fakulta	k1gFnSc7	fakulta
Univerzity	univerzita	k1gFnSc2	univerzita
Palackého	Palacký	k1gMnSc2	Palacký
v	v	k7c6	v
Olomouci	Olomouc	k1gFnSc6	Olomouc
<g/>
,	,	kIx,	,
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
87382	[number]	k4	87382
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
FRIIS	FRIIS	kA	FRIIS
<g/>
,	,	kIx,	,
Ib	Ib	k1gFnSc1	Ib
<g/>
;	;	kIx,	;
DEMISSEW	DEMISSEW	kA	DEMISSEW
<g/>
,	,	kIx,	,
Sebsebe	Sebseb	k1gMnSc5	Sebseb
<g/>
;	;	kIx,	;
BREUGEL	BREUGEL	kA	BREUGEL
<g/>
,	,	kIx,	,
Paulo	Paula	k1gFnSc5	Paula
van	vana	k1gFnPc2	vana
<g/>
.	.	kIx.	.
</s>
<s>
Atlas	Atlas	k1gInSc1	Atlas
of	of	k?	of
the	the	k?	the
potential	potentiat	k5eAaImAgInS	potentiat
vegetation	vegetation	k1gInSc1	vegetation
of	of	k?	of
Ethiopia	Ethiopia	k1gFnSc1	Ethiopia
<g/>
.	.	kIx.	.
</s>
<s>
Copenhagen	Copenhagen	k1gInSc1	Copenhagen
<g/>
:	:	kIx,	:
Kongelige	Kongelige	k1gNnSc1	Kongelige
Danske	Dansk	k1gFnPc1	Dansk
Videnskabernes	Videnskabernes	k1gMnSc1	Videnskabernes
Selskab	Selskab	k1gMnSc1	Selskab
<g/>
,	,	kIx,	,
c	c	k0	c
<g/>
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
87	[number]	k4	87
<g/>
-	-	kIx~	-
<g/>
7304	[number]	k4	7304
<g/>
-	-	kIx~	-
<g/>
347	[number]	k4	347
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
KEELEY	KEELEY	kA	KEELEY
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
<g/>
;	;	kIx,	;
SCOONES	SCOONES	kA	SCOONES
<g/>
,	,	kIx,	,
Ian	Ian	k1gMnSc1	Ian
<g/>
.	.	kIx.	.
</s>
<s>
Understanding	Understanding	k1gInSc1	Understanding
Policy	Polica	k1gFnSc2	Polica
Processes	Processes	k1gMnSc1	Processes
in	in	k?	in
Ethiopia	Ethiopia	k1gFnSc1	Ethiopia
<g/>
:	:	kIx,	:
A	a	k9	a
Response	response	k1gFnSc1	response
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Journal	Journal	k1gMnSc1	Journal
of	of	k?	of
Modern	Modern	k1gMnSc1	Modern
African	African	k1gMnSc1	African
Studies	Studies	k1gMnSc1	Studies
<g/>
.	.	kIx.	.
</s>
<s>
March	March	k1gInSc1	March
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
42	[number]	k4	42
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
,	,	kIx,	,
s.	s.	k?	s.
149	[number]	k4	149
<g/>
-	-	kIx~	-
<g/>
153	[number]	k4	153
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MARCUS	MARCUS	kA	MARCUS
<g/>
,	,	kIx,	,
Harold	Harold	k1gMnSc1	Harold
G.	G.	kA	G.
A	a	k8xC	a
history	histor	k1gInPc1	histor
of	of	k?	of
Ethiopia	Ethiopius	k1gMnSc4	Ethiopius
<g/>
.	.	kIx.	.
</s>
<s>
Berkeley	Berkelea	k1gFnPc1	Berkelea
<g/>
:	:	kIx,	:
University	universita	k1gFnPc1	universita
of	of	k?	of
California	Californium	k1gNnSc2	Californium
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
c	c	k0	c
<g/>
1994	[number]	k4	1994
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
520	[number]	k4	520
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8121	[number]	k4	8121
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
MOJDL	MOJDL	kA	MOJDL
<g/>
,	,	kIx,	,
Lubor	Lubor	k1gMnSc1	Lubor
<g/>
.	.	kIx.	.
</s>
<s>
Etiopie	Etiopie	k1gFnSc1	Etiopie
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Libri	Libri	k1gNnSc1	Libri
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7277	[number]	k4	7277
<g/>
-	-	kIx~	-
<g/>
221	[number]	k4	221
<g/>
-	-	kIx~	-
<g/>
X.	X.	kA	X.
</s>
</p>
<p>
<s>
MUSIL	Musil	k1gMnSc1	Musil
<g/>
,	,	kIx,	,
Alois	Alois	k1gMnSc1	Alois
<g/>
.	.	kIx.	.
</s>
<s>
Lev	Lev	k1gMnSc1	Lev
ze	z	k7c2	z
kmene	kmen	k1gInSc2	kmen
Judova	Judov	k1gInSc2	Judov
:	:	kIx,	:
Nová	nový	k2eAgFnSc1d1	nová
Habeš	Habeš	k1gFnSc1	Habeš
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Melantrich	Melantrich	k1gMnSc1	Melantrich
<g/>
,	,	kIx,	,
1934	[number]	k4	1934
<g/>
.	.	kIx.	.
120	[number]	k4	120
s.	s.	k?	s.
Dostupné	dostupný	k2eAgMnPc4d1	dostupný
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
TIBEBU	TIBEBU	kA	TIBEBU
<g/>
,	,	kIx,	,
Teshale	Teshala	k1gFnSc3	Teshala
<g/>
.	.	kIx.	.
</s>
<s>
Ethiopia	Ethiopia	k1gFnSc1	Ethiopia
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
"	"	kIx"	"
<g/>
Anomaly	Anomaly	k1gFnSc1	Anomaly
<g/>
"	"	kIx"	"
and	and	k?	and
"	"	kIx"	"
<g/>
Paradox	paradox	k1gInSc1	paradox
<g/>
"	"	kIx"	"
of	of	k?	of
Africa	Africa	k1gFnSc1	Africa
<g/>
.	.	kIx.	.
</s>
<s>
Journal	Journat	k5eAaImAgMnS	Journat
of	of	k?	of
Black	Black	k1gMnSc1	Black
Studies	Studies	k1gMnSc1	Studies
<g/>
.	.	kIx.	.
</s>
<s>
March	March	k1gInSc1	March
1996	[number]	k4	1996
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
26	[number]	k4	26
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
,	,	kIx,	,
s.	s.	k?	s.
414	[number]	k4	414
<g/>
-	-	kIx~	-
<g/>
430	[number]	k4	430
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
RYVOLOVÁ	RYVOLOVÁ	kA	RYVOLOVÁ
<g/>
,	,	kIx,	,
Saša	Saša	k1gFnSc1	Saša
<g/>
.	.	kIx.	.
</s>
<s>
Tváře	tvář	k1gFnPc1	tvář
v	v	k7c6	v
prachu	prach	k1gInSc6	prach
Etiopie	Etiopie	k1gFnSc2	Etiopie
<g/>
.	.	kIx.	.
</s>
<s>
Třebíč	Třebíč	k1gFnSc1	Třebíč
<g/>
:	:	kIx,	:
Akcent	akcent	k1gInSc1	akcent
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
7268	[number]	k4	7268
<g/>
-	-	kIx~	-
<g/>
360	[number]	k4	360
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ZEWDE	ZEWDE	kA	ZEWDE
<g/>
,	,	kIx,	,
Bahru	Bahra	k1gFnSc4	Bahra
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
history	histor	k1gInPc1	histor
of	of	k?	of
modern	modern	k1gInSc4	modern
Ethiopia	Ethiopius	k1gMnSc2	Ethiopius
<g/>
,	,	kIx,	,
1855	[number]	k4	1855
<g/>
-	-	kIx~	-
<g/>
1991	[number]	k4	1991
<g/>
.	.	kIx.	.
</s>
<s>
Oxford	Oxford	k1gInSc1	Oxford
<g/>
:	:	kIx,	:
James	James	k1gInSc1	James
Curry	Curra	k1gFnSc2	Curra
<g/>
,	,	kIx,	,
2001	[number]	k4	2001
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
85255	[number]	k4	85255
<g/>
-	-	kIx~	-
<g/>
786	[number]	k4	786
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Etiopie	Etiopie	k1gFnSc2	Etiopie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Etiopie	Etiopie	k1gFnSc2	Etiopie
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
Encyklopedické	encyklopedický	k2eAgNnSc1d1	encyklopedické
heslo	heslo	k1gNnSc1	heslo
Aethiópia	Aethiópium	k1gNnSc2	Aethiópium
v	v	k7c6	v
Ottově	Ottův	k2eAgInSc6d1	Ottův
slovníku	slovník	k1gInSc6	slovník
naučném	naučný	k2eAgInSc6d1	naučný
ve	v	k7c6	v
Wikizdrojích	Wikizdroj	k1gInPc6	Wikizdroj
</s>
</p>
<p>
<s>
Kategorie	kategorie	k1gFnSc1	kategorie
Etiopie	Etiopie	k1gFnSc1	Etiopie
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
Ethiopia	Ethiopia	k1gFnSc1	Ethiopia
-	-	kIx~	-
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc1	International
Report	report	k1gInSc1	report
2011	[number]	k4	2011
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
25	[number]	k4	25
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Ethiopia	Ethiopia	k1gFnSc1	Ethiopia
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc1	Freedom
House	house	k1gNnSc1	house
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
.	.	kIx.	.
</s>
<s>
BTI	BTI	kA	BTI
2010	[number]	k4	2010
–	–	k?	–
Ethiopia	Ethiopius	k1gMnSc2	Ethiopius
Country	country	k2eAgInSc4d1	country
Report	report	k1gInSc4	report
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Gütersloh	Gütersloh	k1gMnSc1	Gütersloh
<g/>
:	:	kIx,	:
Bertelsmann	Bertelsmann	k1gMnSc1	Bertelsmann
Stiftung	Stiftung	k1gMnSc1	Stiftung
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
5	[number]	k4	5
<g/>
-	-	kIx~	-
<g/>
20	[number]	k4	20
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Bureau	Bureau	k6eAd1	Bureau
of	of	k?	of
African	African	k1gInSc1	African
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gInSc1	Note
<g/>
:	:	kIx,	:
Ethiopia	Ethiopia	k1gFnSc1	Ethiopia
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011-03-23	[number]	k4	2011-03-23
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2010	[number]	k4	2010
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
Ethiopia	Ethiopia	k1gFnSc1	Ethiopia
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Rev	Rev	k?	Rev
<g/>
.	.	kIx.	.
2010-07-07	[number]	k4	2010-07-07
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Country	country	k2eAgInSc1d1	country
of	of	k?	of
Origin	Origin	k2eAgInSc1d1	Origin
Research	Research	k1gInSc1	Research
and	and	k?	and
Information	Information	k1gInSc1	Information
(	(	kIx(	(
<g/>
CORI	CORI	kA	CORI
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
CORI	CORI	kA	CORI
Country	country	k2eAgInSc1d1	country
Report	report	k1gInSc1	report
<g/>
:	:	kIx,	:
Ethiopia	Ethiopia	k1gFnSc1	Ethiopia
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2010-01	[number]	k4	2010-01
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Human	Human	k1gMnSc1	Human
Rights	Rightsa	k1gFnPc2	Rightsa
Watch	Watch	k1gMnSc1	Watch
<g/>
.	.	kIx.	.
</s>
<s>
World	World	k1gInSc1	World
Report	report	k1gInSc1	report
2012	[number]	k4	2012
-	-	kIx~	-
Ethiopia	Ethiopium	k1gNnSc2	Ethiopium
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2012-01-22	[number]	k4	2012-01-22
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Library	Librar	k1gInPc1	Librar
of	of	k?	of
Congress	Congress	k1gInSc1	Congress
<g/>
.	.	kIx.	.
</s>
<s>
Country	country	k2eAgFnPc1d1	country
Profile	profil	k1gInSc5	profil
<g/>
:	:	kIx,	:
Ethiopia	Ethiopium	k1gNnSc2	Ethiopium
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
2005-04-20	[number]	k4	2005-04-20
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Refworld	Refworld	k1gMnSc1	Refworld
<g/>
:	:	kIx,	:
The	The	k1gMnSc1	The
Leader	leader	k1gMnSc1	leader
in	in	k?	in
Refugee	Refugee	k1gInSc1	Refugee
Decision	Decision	k1gInSc1	Decision
Support	support	k1gInSc1	support
<g/>
.	.	kIx.	.
</s>
<s>
Ethiopia	Ethiopia	k1gFnSc1	Ethiopia
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
United	United	k1gMnSc1	United
Nations	Nationsa	k1gFnPc2	Nationsa
High	High	k1gMnSc1	High
Commissioner	Commissioner	k1gMnSc1	Commissioner
for	forum	k1gNnPc2	forum
Refugees	Refugees	k1gInSc1	Refugees
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2012	[number]	k4	2012
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
v	v	k7c6	v
Addis	Addis	k1gFnSc6	Addis
Abebě	Abeba	k1gFnSc6	Abeba
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
Etiopie	Etiopie	k1gFnSc1	Etiopie
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2009-10-04	[number]	k4	2009-10-04
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
25	[number]	k4	25
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
BEKELE	BEKELE	kA	BEKELE
<g/>
,	,	kIx,	,
Million	Million	k1gInSc1	Million
<g/>
.	.	kIx.	.
</s>
<s>
Forestry	Forestra	k1gFnPc1	Forestra
Outlook	Outlook	k1gInSc1	Outlook
Studies	Studies	k1gMnSc1	Studies
in	in	k?	in
Africa	Africa	k1gMnSc1	Africa
(	(	kIx(	(
<g/>
FOSA	fosa	k1gFnSc1	fosa
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
Ethiopia	Ethiopia	k1gFnSc1	Ethiopia
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
Food	Food	k1gInSc1	Food
and	and	k?	and
Agriculture	Agricultur	k1gMnSc5	Agricultur
Organization	Organization	k1gInSc1	Organization
of	of	k?	of
the	the	k?	the
United	United	k1gInSc1	United
Nations	Nationsa	k1gFnPc2	Nationsa
<g/>
,	,	kIx,	,
2010-06	[number]	k4	2010-06
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
nedostupný	dostupný	k2eNgInSc1d1	nedostupný
zdroj	zdroj	k1gInSc1	zdroj
<g/>
]	]	kIx)	]
</s>
</p>
<p>
<s>
CRUMMEY	CRUMMEY	kA	CRUMMEY
<g/>
,	,	kIx,	,
Donald	Donald	k1gMnSc1	Donald
Edward	Edward	k1gMnSc1	Edward
<g/>
,	,	kIx,	,
a	a	k8xC	a
kol	kolo	k1gNnPc2	kolo
<g/>
.	.	kIx.	.
</s>
<s>
Ethiopia	Ethiopia	k1gFnSc1	Ethiopia
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
29	[number]	k4	29
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
FOSTER	FOSTER	kA	FOSTER
<g/>
,	,	kIx,	,
Vivien	Vivien	k1gInSc1	Vivien
<g/>
;	;	kIx,	;
MORELLA	MORELLA	kA	MORELLA
<g/>
,	,	kIx,	,
Elvira	Elviro	k1gNnSc2	Elviro
<g/>
.	.	kIx.	.
</s>
<s>
Ethiopia	Ethiopia	k1gFnSc1	Ethiopia
<g/>
'	'	kIx"	'
<g/>
s	s	k7c7	s
Infrastructure	Infrastructur	k1gMnSc5	Infrastructur
<g/>
:	:	kIx,	:
A	a	k9	a
Continental	Continental	k1gMnSc1	Continental
Perspective	Perspectiv	k1gInSc5	Perspectiv
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
New	New	k?	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
World	World	k1gMnSc1	World
Bank	bank	k1gInSc1	bank
<g/>
,	,	kIx,	,
2010-03	[number]	k4	2010-03
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2010	[number]	k4	2010
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
16	[number]	k4	16
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
v	v	k7c6	v
archivu	archiv	k1gInSc6	archiv
pořízeném	pořízený	k2eAgInSc6d1	pořízený
dne	den	k1gInSc2	den
2014	[number]	k4	2014
<g/>
-	-	kIx~	-
<g/>
12	[number]	k4	12
<g/>
-	-	kIx~	-
<g/>
18	[number]	k4	18
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
