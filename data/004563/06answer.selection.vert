<s>
Anatomie	anatomie	k1gFnSc1	anatomie
je	být	k5eAaImIp3nS	být
obor	obor	k1gInSc4	obor
biologie	biologie	k1gFnSc2	biologie
nebo	nebo	k8xC	nebo
medicíny	medicína	k1gFnSc2	medicína
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
makroskopickou	makroskopický	k2eAgFnSc7d1	makroskopická
a	a	k8xC	a
mikroskopickou	mikroskopický	k2eAgFnSc7d1	mikroskopická
stavbou	stavba	k1gFnSc7	stavba
organismů	organismus	k1gInPc2	organismus
<g/>
.	.	kIx.	.
</s>
