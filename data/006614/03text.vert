<s>
Chanel	Chanel	k1gMnSc1	Chanel
N	N	kA	N
<g/>
°	°	k?	°
5	[number]	k4	5
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
nejslavnější	slavný	k2eAgInSc4d3	nejslavnější
parfém	parfém	k1gInSc4	parfém
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
na	na	k7c4	na
trh	trh	k1gInSc4	trh
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1921	[number]	k4	1921
francouzskou	francouzský	k2eAgFnSc7d1	francouzská
módní	módní	k2eAgFnSc7d1	módní
návrhářkou	návrhářka	k1gFnSc7	návrhářka
Gabrielle	Gabrielle	k1gFnSc2	Gabrielle
"	"	kIx"	"
<g/>
Coco	Coco	k1gMnSc1	Coco
<g/>
"	"	kIx"	"
Chanel	Chanel	k1gMnSc1	Chanel
<g/>
.	.	kIx.	.
</s>
<s>
Parfém	parfém	k1gInSc1	parfém
tvoří	tvořit	k5eAaImIp3nS	tvořit
kompozice	kompozice	k1gFnPc4	kompozice
více	hodně	k6eAd2	hodně
než	než	k8xS	než
80	[number]	k4	80
složek	složka	k1gFnPc2	složka
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
hlavní	hlavní	k2eAgFnSc7d1	hlavní
jsou	být	k5eAaImIp3nP	být
jasmín	jasmín	k1gInSc1	jasmín
<g/>
,	,	kIx,	,
růže	růže	k1gFnSc1	růže
<g/>
,	,	kIx,	,
kosatec	kosatec	k1gInSc1	kosatec
<g/>
,	,	kIx,	,
ylang	ylang	k1gMnSc1	ylang
ylang	ylang	k1gMnSc1	ylang
<g/>
,	,	kIx,	,
vanilka	vanilka	k1gFnSc1	vanilka
<g/>
,	,	kIx,	,
ambra	ambra	k1gFnSc1	ambra
a	a	k8xC	a
santalový	santalový	k2eAgInSc1d1	santalový
olej	olej	k1gInSc1	olej
<g/>
.	.	kIx.	.
</s>
<s>
Parfém	parfém	k1gInSc4	parfém
vyvinul	vyvinout	k5eAaPmAgMnS	vyvinout
chemik	chemik	k1gMnSc1	chemik
Ernest	Ernest	k1gMnSc1	Ernest
Beaux	Beaux	k1gInSc4	Beaux
<g/>
.	.	kIx.	.
</s>
<s>
Beaux	Beaux	k1gInSc4	Beaux
byl	být	k5eAaImAgMnS	být
Francouz	Francouz	k1gMnSc1	Francouz
<g/>
,	,	kIx,	,
potomek	potomek	k1gMnSc1	potomek
alchymistů	alchymista	k1gMnPc2	alchymista
na	na	k7c6	na
carském	carský	k2eAgInSc6d1	carský
dvoře	dvůr	k1gInSc6	dvůr
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
po	po	k7c6	po
revoluci	revoluce	k1gFnSc6	revoluce
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1917	[number]	k4	1917
emigroval	emigrovat	k5eAaBmAgMnS	emigrovat
z	z	k7c2	z
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
odpovídala	odpovídat	k5eAaImAgFnS	odpovídat
za	za	k7c4	za
parfémy	parfém	k1gInPc4	parfém
na	na	k7c6	na
carském	carský	k2eAgInSc6d1	carský
dvoře	dvůr	k1gInSc6	dvůr
<g/>
.	.	kIx.	.
</s>
<s>
Parfém	parfém	k1gInSc1	parfém
byl	být	k5eAaImAgInS	být
výsledkem	výsledek	k1gInSc7	výsledek
spolupráce	spolupráce	k1gFnSc2	spolupráce
Beauxe	Beauxe	k1gFnSc2	Beauxe
a	a	k8xC	a
Coco	Coco	k1gMnSc1	Coco
Chanel	Chanel	k1gMnSc1	Chanel
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
ho	on	k3xPp3gMnSc4	on
oslovila	oslovit	k5eAaPmAgFnS	oslovit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
-	-	kIx~	-
jako	jako	k9	jako
již	již	k6eAd1	již
proslulá	proslulý	k2eAgFnSc1d1	proslulá
módní	módní	k2eAgFnSc1d1	módní
návrhářka	návrhářka	k1gFnSc1	návrhářka
-	-	kIx~	-
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
expandovat	expandovat	k5eAaImF	expandovat
do	do	k7c2	do
obchodu	obchod	k1gInSc2	obchod
s	s	k7c7	s
parfémy	parfém	k1gInPc7	parfém
a	a	k8xC	a
kosmetikou	kosmetika	k1gFnSc7	kosmetika
<g/>
.	.	kIx.	.
</s>
<s>
Jejím	její	k3xOp3gInSc7	její
záměrem	záměr	k1gInSc7	záměr
bylo	být	k5eAaImAgNnS	být
vytvořit	vytvořit	k5eAaPmF	vytvořit
vůni	vůně	k1gFnSc4	vůně
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
ztělesňovala	ztělesňovat	k5eAaImAgFnS	ztělesňovat
moderní	moderní	k2eAgInSc4d1	moderní
styl	styl	k1gInSc4	styl
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
spolupráce	spolupráce	k1gFnSc2	spolupráce
chemika	chemik	k1gMnSc2	chemik
a	a	k8xC	a
módní	módní	k2eAgFnSc2d1	módní
návrhářky	návrhářka	k1gFnSc2	návrhářka
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
čistá	čistý	k2eAgFnSc1d1	čistá
a	a	k8xC	a
svěží	svěží	k2eAgFnSc1d1	svěží
vůně	vůně	k1gFnSc1	vůně
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
je	být	k5eAaImIp3nS	být
cítit	cítit	k5eAaImF	cítit
citrony	citron	k1gInPc4	citron
<g/>
,	,	kIx,	,
mýdlo	mýdlo	k1gNnSc4	mýdlo
<g/>
,	,	kIx,	,
květiny	květina	k1gFnPc4	květina
a	a	k8xC	a
květy	květ	k1gInPc4	květ
třešní	třešeň	k1gFnPc2	třešeň
<g/>
;	;	kIx,	;
podle	podle	k7c2	podle
Coco	Coco	k6eAd1	Coco
Chanel	Chanel	k1gInSc4	Chanel
vůně	vůně	k1gFnSc2	vůně
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
ji	on	k3xPp3gFnSc4	on
provázely	provázet	k5eAaImAgFnP	provázet
v	v	k7c6	v
dětství	dětství	k1gNnSc6	dětství
a	a	k8xC	a
mládí	mládí	k1gNnSc6	mládí
v	v	k7c6	v
klášterním	klášterní	k2eAgInSc6d1	klášterní
sirotčinci	sirotčinec	k1gInSc6	sirotčinec
ve	v	k7c6	v
středofrancouzském	středofrancouzský	k2eAgInSc6d1	středofrancouzský
Aubazine	Aubazin	k1gInSc5	Aubazin
<g/>
.	.	kIx.	.
</s>
<s>
Vzpomínala	vzpomínat	k5eAaImAgFnS	vzpomínat
na	na	k7c4	na
vůni	vůně	k1gFnSc4	vůně
"	"	kIx"	"
<g/>
povlečení	povlečení	k1gNnSc2	povlečení
vyvařovaného	vyvařovaný	k2eAgNnSc2d1	vyvařovaný
v	v	k7c6	v
měděných	měděný	k2eAgInPc6d1	měděný
kotlích	kotel	k1gInPc6	kotel
<g/>
,	,	kIx,	,
navoněných	navoněný	k2eAgFnPc2d1	navoněná
sušenými	sušený	k2eAgInPc7d1	sušený
kořeny	kořen	k1gInPc7	kořen
kosatců	kosatec	k1gInPc2	kosatec
<g/>
,	,	kIx,	,
skříní	skříň	k1gFnPc2	skříň
s	s	k7c7	s
prádlem	prádlo	k1gNnSc7	prádlo
s	s	k7c7	s
růžovým	růžový	k2eAgNnSc7d1	růžové
dřevem	dřevo	k1gNnSc7	dřevo
a	a	k8xC	a
verbenou	verbena	k1gFnSc7	verbena
<g/>
,	,	kIx,	,
a	a	k8xC	a
podlah	podlaha	k1gFnPc2	podlaha
<g/>
,	,	kIx,	,
zdí	zeď	k1gFnPc2	zeď
a	a	k8xC	a
těl	tělo	k1gNnPc2	tělo
vydrhnutých	vydrhnutý	k2eAgInPc2d1	vydrhnutý
hrubým	hrubý	k2eAgNnSc7d1	hrubé
lojovým	lojový	k2eAgNnSc7d1	lojový
mýdlem	mýdlo	k1gNnSc7	mýdlo
<g/>
,	,	kIx,	,
i	i	k8xC	i
jasmínů	jasmín	k1gInPc2	jasmín
<g/>
,	,	kIx,	,
levandulí	levandule	k1gFnPc2	levandule
a	a	k8xC	a
růží	růž	k1gFnPc2	růž
z	z	k7c2	z
místních	místní	k2eAgFnPc2d1	místní
zahrad	zahrada	k1gFnPc2	zahrada
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
těchto	tento	k3xDgFnPc2	tento
jejích	její	k3xOp3gFnPc2	její
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
vyrobil	vyrobit	k5eAaPmAgInS	vyrobit
Beaux	Beaux	k1gInSc1	Beaux
několik	několik	k4yIc1	několik
směsí	směs	k1gFnPc2	směs
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
Coco	Coco	k6eAd1	Coco
vybrala	vybrat	k5eAaPmAgFnS	vybrat
tu	tu	k6eAd1	tu
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
nejlépe	dobře	k6eAd3	dobře
odpovídala	odpovídat	k5eAaImAgFnS	odpovídat
jejím	její	k3xOp3gMnSc7	její
představám	představa	k1gFnPc3	představa
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
"	"	kIx"	"
<g/>
Chanel	Chanel	k1gInSc1	Chanel
N	N	kA	N
<g/>
°	°	k?	°
5	[number]	k4	5
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
"	"	kIx"	"
<g/>
Chanel	Chanel	k1gInSc1	Chanel
nr	nr	k?	nr
5	[number]	k4	5
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
slepé	slepý	k2eAgFnSc2d1	slepá
zkoušky	zkouška	k1gFnSc2	zkouška
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
při	při	k7c6	při
prezentaci	prezentace	k1gFnSc6	prezentace
očíslovaných	očíslovaný	k2eAgFnPc2d1	očíslovaná
lahviček	lahvička	k1gFnPc2	lahvička
se	s	k7c7	s
vzorky	vzorek	k1gInPc7	vzorek
parfémů	parfém	k1gInPc2	parfém
návrhářka	návrhářka	k1gFnSc1	návrhářka
vybrala	vybrat	k5eAaPmAgFnS	vybrat
vzorek	vzorek	k1gInSc4	vzorek
s	s	k7c7	s
číslem	číslo	k1gNnSc7	číslo
5	[number]	k4	5
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
číslo	číslo	k1gNnSc1	číslo
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
její	její	k3xOp3gNnSc1	její
oblíbené	oblíbený	k2eAgNnSc1d1	oblíbené
a	a	k8xC	a
mělo	mít	k5eAaImAgNnS	mít
pro	pro	k7c4	pro
ni	on	k3xPp3gFnSc4	on
i	i	k8xC	i
symbolickou	symbolický	k2eAgFnSc4d1	symbolická
hodnotu	hodnota	k1gFnSc4	hodnota
<g/>
:	:	kIx,	:
sirotčinec	sirotčinec	k1gInSc4	sirotčinec
v	v	k7c6	v
Aubauzine	Aubauzin	k1gInSc5	Aubauzin
založili	založit	k5eAaPmAgMnP	založit
cisterciáci	cisterciák	k1gMnPc1	cisterciák
-	-	kIx~	-
řád	řád	k1gInSc1	řád
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
kladl	klást	k5eAaImAgInS	klást
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
numerologii	numerologie	k1gFnSc4	numerologie
a	a	k8xC	a
číslo	číslo	k1gNnSc4	číslo
pět	pět	k4xCc4	pět
mělo	mít	k5eAaImAgNnS	mít
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
mystický	mystický	k2eAgInSc1d1	mystický
význam	význam	k1gInSc1	význam
<g/>
;	;	kIx,	;
cesta	cesta	k1gFnSc1	cesta
<g/>
,	,	kIx,	,
po	po	k7c6	po
níž	jenž	k3xRgFnSc6	jenž
Coco	Coco	k6eAd1	Coco
denně	denně	k6eAd1	denně
chodila	chodit	k5eAaImAgFnS	chodit
do	do	k7c2	do
kostela	kostel	k1gInSc2	kostel
k	k	k7c3	k
modlitbám	modlitba	k1gFnPc3	modlitba
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
vydlážděna	vydláždit	k5eAaPmNgFnS	vydláždit
obrazci	obrazec	k1gInPc7	obrazec
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
se	se	k3xPyFc4	se
opakovala	opakovat	k5eAaImAgFnS	opakovat
číslice	číslice	k1gFnSc1	číslice
pět	pět	k4xCc4	pět
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgFnPc1	všechen
tyto	tento	k3xDgFnPc1	tento
skutečnosti	skutečnost	k1gFnPc1	skutečnost
vedly	vést	k5eAaImAgFnP	vést
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
jednoduchého	jednoduchý	k2eAgNnSc2d1	jednoduché
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
originálního	originální	k2eAgInSc2d1	originální
názvu	název	k1gInSc2	název
<g/>
.	.	kIx.	.
</s>
<s>
Původní	původní	k2eAgInSc1d1	původní
flakon	flakon	k1gInSc1	flakon
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
byl	být	k5eAaImAgInS	být
parfém	parfém	k1gInSc1	parfém
expedován	expedován	k2eAgInSc1d1	expedován
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
malý	malý	k2eAgInSc1d1	malý
a	a	k8xC	a
kulatý	kulatý	k2eAgInSc1d1	kulatý
a	a	k8xC	a
výrazně	výrazně	k6eAd1	výrazně
se	se	k3xPyFc4	se
lišil	lišit	k5eAaImAgMnS	lišit
od	od	k7c2	od
typické	typický	k2eAgFnSc2d1	typická
podoby	podoba	k1gFnSc2	podoba
flakonu	flakon	k1gInSc2	flakon
<g/>
,	,	kIx,	,
jak	jak	k8xS	jak
ji	on	k3xPp3gFnSc4	on
známe	znát	k5eAaImIp1nP	znát
dnes	dnes	k6eAd1	dnes
<g/>
.	.	kIx.	.
</s>
<s>
Záhy	záhy	k6eAd1	záhy
se	se	k3xPyFc4	se
však	však	k9	však
ukázalo	ukázat	k5eAaPmAgNnS	ukázat
<g/>
,	,	kIx,	,
že	že	k8xS	že
stěny	stěna	k1gFnPc1	stěna
lahvičky	lahvička	k1gFnSc2	lahvička
jsou	být	k5eAaImIp3nP	být
příliš	příliš	k6eAd1	příliš
tenké	tenký	k2eAgNnSc1d1	tenké
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
flakon	flakon	k1gInSc4	flakon
vydržel	vydržet	k5eAaPmAgMnS	vydržet
zasílání	zasílání	k1gNnSc4	zasílání
<g/>
;	;	kIx,	;
to	ten	k3xDgNnSc1	ten
vedlo	vést	k5eAaImAgNnS	vést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1924	[number]	k4	1924
po	po	k7c6	po
založení	založení	k1gNnSc6	založení
formy	forma	k1gFnSc2	forma
Parfémy	parfém	k1gInPc1	parfém
Chanel	Chanela	k1gFnPc2	Chanela
ke	k	k7c3	k
změně	změna	k1gFnSc3	změna
v	v	k7c4	v
klasický	klasický	k2eAgInSc4d1	klasický
hranatý	hranatý	k2eAgInSc4d1	hranatý
tvar	tvar	k1gInSc4	tvar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
minulých	minulý	k2eAgNnPc2d1	Minulé
70	[number]	k4	70
let	léto	k1gNnPc2	léto
byla	být	k5eAaImAgFnS	být
podoba	podoba	k1gFnSc1	podoba
flakonu	flakon	k1gInSc2	flakon
několikrát	několikrát	k6eAd1	několikrát
pozměněna	pozměněn	k2eAgFnSc1d1	pozměněna
-	-	kIx~	-
zhruba	zhruba	k6eAd1	zhruba
každých	každý	k3xTgInPc2	každý
20	[number]	k4	20
let	léto	k1gNnPc2	léto
je	být	k5eAaImIp3nS	být
nepatrně	patrně	k6eNd1	patrně
modifikována	modifikovat	k5eAaBmNgFnS	modifikovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
odpovídala	odpovídat	k5eAaImAgFnS	odpovídat
současné	současný	k2eAgFnSc6d1	současná
epoše	epocha	k1gFnSc6	epocha
<g/>
.	.	kIx.	.
</s>
<s>
Parfém	parfém	k1gInSc1	parfém
byl	být	k5eAaImAgInS	být
poprvé	poprvé	k6eAd1	poprvé
veřejnosti	veřejnost	k1gFnSc2	veřejnost
představen	představit	k5eAaPmNgInS	představit
na	na	k7c4	na
vánoce	vánoce	k1gFnPc4	vánoce
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
<g/>
;	;	kIx,	;
prvních	první	k4xOgInPc2	první
100	[number]	k4	100
flakonů	flakon	k1gInPc2	flakon
se	se	k3xPyFc4	se
Coco	Coco	k6eAd1	Coco
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
dát	dát	k5eAaPmF	dát
svým	svůj	k3xOyFgMnPc3	svůj
nejlepším	dobrý	k2eAgMnPc3d3	nejlepší
zákazníkům	zákazník	k1gMnPc3	zákazník
jako	jako	k8xS	jako
sváteční	sváteční	k2eAgInSc4d1	sváteční
dárek	dárek	k1gInSc4	dárek
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
skutečný	skutečný	k2eAgInSc4d1	skutečný
počátek	počátek	k1gInSc4	počátek
prodeje	prodej	k1gInSc2	prodej
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
považuje	považovat	k5eAaImIp3nS	považovat
polovina	polovina	k1gFnSc1	polovina
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byl	být	k5eAaImAgInS	být
parfém	parfém	k1gInSc1	parfém
prodáván	prodávat	k5eAaImNgInS	prodávat
jen	jen	k9	jen
v	v	k7c6	v
obchodech	obchod	k1gInPc6	obchod
Chanel	Chanel	k1gInSc1	Chanel
vybraným	vybraný	k2eAgMnPc3d1	vybraný
zákazníkům	zákazník	k1gMnPc3	zákazník
<g/>
.	.	kIx.	.
</s>
<s>
Získal	získat	k5eAaPmAgInS	získat
si	se	k3xPyFc3	se
obrovskou	obrovský	k2eAgFnSc4d1	obrovská
oblibu	obliba	k1gFnSc4	obliba
a	a	k8xC	a
věhlas	věhlas	k1gInSc4	věhlas
a	a	k8xC	a
dodnes	dodnes	k6eAd1	dodnes
náleží	náležet	k5eAaImIp3nS	náležet
k	k	k7c3	k
nejznámějším	známý	k2eAgInPc3d3	nejznámější
a	a	k8xC	a
nejprodávanějším	prodávaný	k2eAgInPc3d3	nejprodávanější
luxusním	luxusní	k2eAgInPc3d1	luxusní
parfémům	parfém	k1gInPc3	parfém
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Chanel	Chanel	k1gInSc1	Chanel
N	N	kA	N
<g/>
°	°	k?	°
5	[number]	k4	5
<g/>
"	"	kIx"	"
si	se	k3xPyFc3	se
oblíbila	oblíbit	k5eAaPmAgFnS	oblíbit
Marilyn	Marilyn	k1gFnSc1	Marilyn
Monroe	Monroe	k1gFnSc1	Monroe
<g/>
,	,	kIx,	,
propagovaly	propagovat	k5eAaImAgInP	propagovat
jej	on	k3xPp3gInSc4	on
Catherine	Catherin	k1gInSc5	Catherin
Deneuve	Deneuev	k1gFnPc1	Deneuev
<g/>
,	,	kIx,	,
Carole	Carole	k1gFnPc1	Carole
Bouquet	bouquet	k1gNnSc1	bouquet
<g/>
,	,	kIx,	,
Nicole	Nicole	k1gFnSc1	Nicole
Kidman	Kidman	k1gMnSc1	Kidman
<g/>
,	,	kIx,	,
Estella	Estella	k1gMnSc1	Estella
Warren	Warrna	k1gFnPc2	Warrna
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
vzniku	vznik	k1gInSc6	vznik
tohoto	tento	k3xDgInSc2	tento
světoznámého	světoznámý	k2eAgInSc2d1	světoznámý
parfému	parfém	k1gInSc2	parfém
přímo	přímo	k6eAd1	přímo
pojednává	pojednávat	k5eAaImIp3nS	pojednávat
také	také	k9	také
životopisný	životopisný	k2eAgInSc1d1	životopisný
francouzský	francouzský	k2eAgInSc1d1	francouzský
film	film	k1gInSc1	film
Coco	Coco	k1gMnSc1	Coco
Chanel	Chanel	k1gMnSc1	Chanel
&	&	k?	&
Igor	Igor	k1gMnSc1	Igor
Stravinskij	Stravinskij	k1gMnSc1	Stravinskij
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2009	[number]	k4	2009
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc7d1	další
tváří	tvář	k1gFnSc7	tvář
parfému	parfém	k1gInSc2	parfém
je	být	k5eAaImIp3nS	být
Audrey	Audre	k2eAgFnPc4d1	Audre
Tautou	Tautá	k1gFnSc4	Tautá
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
ztvárnila	ztvárnit	k5eAaPmAgFnS	ztvárnit
roli	role	k1gFnSc4	role
Coco	Coco	k6eAd1	Coco
Chanel	Chanel	k1gInSc1	Chanel
ve	v	k7c6	v
filmu	film	k1gInSc6	film
Coco	Coco	k1gMnSc1	Coco
Chanel	Chanel	k1gMnSc1	Chanel
<g/>
,	,	kIx,	,
tento	tento	k3xDgMnSc1	tento
snímek	snímek	k1gInSc4	snímek
však	však	k9	však
o	o	k7c6	o
tomto	tento	k3xDgInSc6	tento
parfému	parfém	k1gInSc6	parfém
přímo	přímo	k6eAd1	přímo
nepojednává	pojednávat	k5eNaImIp3nS	pojednávat
<g/>
.	.	kIx.	.
</s>
<s>
Coco	Coco	k1gMnSc1	Coco
Chanel	Chanel	k1gMnSc1	Chanel
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Chanel	Chanela	k1gFnPc2	Chanela
No	no	k9	no
<g/>
.	.	kIx.	.
5	[number]	k4	5
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Chanel	Chanela	k1gFnPc2	Chanela
N	N	kA	N
<g/>
°	°	k?	°
5	[number]	k4	5
Reklama	reklama	k1gFnSc1	reklama
Chanel	Chanela	k1gFnPc2	Chanela
N	N	kA	N
<g/>
°	°	k?	°
5	[number]	k4	5
Historie	historie	k1gFnSc1	historie
Chanel	Chanela	k1gFnPc2	Chanela
N	N	kA	N
<g/>
°	°	k?	°
5	[number]	k4	5
</s>
