<s>
Letiště	letiště	k1gNnSc1	letiště
je	být	k5eAaImIp3nS	být
stavba	stavba	k1gFnSc1	stavba
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
nebo	nebo	k8xC	nebo
na	na	k7c6	na
vodě	voda	k1gFnSc6	voda
určená	určený	k2eAgFnSc1d1	určená
pro	pro	k7c4	pro
vzlety	vzlet	k1gInPc4	vzlet
<g/>
,	,	kIx,	,
přistání	přistání	k1gNnSc4	přistání
(	(	kIx(	(
<g/>
vzletová	vzletový	k2eAgFnSc1d1	vzletová
a	a	k8xC	a
přistávací	přistávací	k2eAgFnSc1d1	přistávací
dráha	dráha	k1gFnSc1	dráha
<g/>
)	)	kIx)	)
a	a	k8xC	a
pozemní	pozemní	k2eAgInPc4d1	pozemní
pohyby	pohyb	k1gInPc4	pohyb
letadel	letadlo	k1gNnPc2	letadlo
po	po	k7c6	po
pojezdových	pojezdový	k2eAgFnPc6d1	pojezdová
drahách	draha	k1gFnPc6	draha
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
k	k	k7c3	k
ní	on	k3xPp3gFnSc3	on
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
další	další	k2eAgNnSc4d1	další
technické	technický	k2eAgNnSc4d1	technické
a	a	k8xC	a
logistické	logistický	k2eAgNnSc4d1	logistické
zázemí	zázemí	k1gNnSc4	zázemí
–	–	k?	–
hangáry	hangár	k1gInPc1	hangár
<g/>
,	,	kIx,	,
řídicí	řídicí	k2eAgFnSc1d1	řídicí
věž	věž	k1gFnSc1	věž
<g/>
,	,	kIx,	,
letištní	letištní	k2eAgInPc1d1	letištní
terminály	terminál	k1gInPc1	terminál
<g/>
,	,	kIx,	,
sklady	sklad	k1gInPc1	sklad
leteckého	letecký	k2eAgNnSc2d1	letecké
paliva	palivo	k1gNnSc2	palivo
a	a	k8xC	a
stavby	stavba	k1gFnSc2	stavba
pro	pro	k7c4	pro
logistické	logistický	k2eAgNnSc4d1	logistické
zabezpečení	zabezpečení	k1gNnSc4	zabezpečení
leteckého	letecký	k2eAgInSc2d1	letecký
provozu	provoz	k1gInSc2	provoz
<g/>
.	.	kIx.	.
</s>
<s>
Letiště	letiště	k1gNnPc1	letiště
můžeme	moct	k5eAaImIp1nP	moct
dělit	dělit	k5eAaImF	dělit
na	na	k7c4	na
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
letiště	letiště	k1gNnSc1	letiště
které	který	k3yIgNnSc1	který
může	moct	k5eAaImIp3nS	moct
přijmout	přijmout	k5eAaPmF	přijmout
mezinárodní	mezinárodní	k2eAgFnPc4d1	mezinárodní
cestující	cestující	k1gFnPc4	cestující
z	z	k7c2	z
jiných	jiný	k2eAgInPc2d1	jiný
států	stát	k1gInPc2	stát
a	a	k8xC	a
vnitrostátní	vnitrostátní	k2eAgFnSc1d1	vnitrostátní
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
se	se	k3xPyFc4	se
nemusí	muset	k5eNaImIp3nP	muset
zabývat	zabývat	k5eAaImF	zabývat
imigrací	imigrace	k1gFnSc7	imigrace
a	a	k8xC	a
clem	clo	k1gNnSc7	clo
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
letišť	letiště	k1gNnPc2	letiště
je	být	k5eAaImIp3nS	být
označena	označit	k5eAaPmNgFnS	označit
s	s	k7c7	s
kódem	kód	k1gInSc7	kód
ICAO	ICAO	kA	ICAO
a	a	k8xC	a
mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
s	s	k7c7	s
pravidelným	pravidelný	k2eAgInSc7d1	pravidelný
provozem	provoz	k1gInSc7	provoz
mívají	mívat	k5eAaImIp3nP	mívat
také	také	k9	také
kód	kód	k1gInSc4	kód
IATA	IATA	kA	IATA
<g/>
.	.	kIx.	.
</s>
<s>
Raná	raný	k2eAgNnPc1d1	rané
letiště	letiště	k1gNnPc1	letiště
byla	být	k5eAaImAgNnP	být
budována	budovat	k5eAaImNgNnP	budovat
během	během	k7c2	během
první	první	k4xOgFnSc2	první
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
pro	pro	k7c4	pro
vojenské	vojenský	k2eAgFnPc4d1	vojenská
potřeby	potřeba	k1gFnPc4	potřeba
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
válce	válka	k1gFnSc6	válka
byly	být	k5eAaImAgInP	být
k	k	k7c3	k
některým	některý	k3yIgInPc3	některý
dostavěny	dostavěn	k2eAgFnPc1d1	dostavěna
další	další	k2eAgFnPc1d1	další
budovy	budova	k1gFnPc1	budova
pro	pro	k7c4	pro
podporu	podpora	k1gFnSc4	podpora
civilní	civilní	k2eAgFnSc2d1	civilní
přepravy	přeprava	k1gFnSc2	přeprava
osob	osoba	k1gFnPc2	osoba
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
letiště	letiště	k1gNnSc1	letiště
určené	určený	k2eAgNnSc1d1	určené
výhradně	výhradně	k6eAd1	výhradně
pro	pro	k7c4	pro
civilní	civilní	k2eAgInPc4d1	civilní
účely	účel	k1gInPc4	účel
bylo	být	k5eAaImAgNnS	být
otevřeno	otevřít	k5eAaPmNgNnS	otevřít
roku	rok	k1gInSc2	rok
1922	[number]	k4	1922
v	v	k7c6	v
tehdy	tehdy	k6eAd1	tehdy
německém	německý	k2eAgInSc6d1	německý
Königsbergu	Königsberg	k1gInSc6	Königsberg
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgNnSc1d3	veliký
dopravní	dopravní	k2eAgNnSc1d1	dopravní
letiště	letiště	k1gNnSc1	letiště
světa	svět	k1gInSc2	svět
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Atlantě	Atlanta	k1gFnSc6	Atlanta
<g/>
,	,	kIx,	,
Georgia	Georgia	k1gFnSc1	Georgia
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
počet	počet	k1gInSc1	počet
přepravených	přepravený	k2eAgMnPc2d1	přepravený
cestujících	cestující	k1gMnPc2	cestující
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
díky	díky	k7c3	díky
většímu	veliký	k2eAgInSc3d2	veliký
počtu	počet	k1gInSc3	počet
letišť	letiště	k1gNnPc2	letiště
<g/>
:	:	kIx,	:
London	London	k1gMnSc1	London
Heathrow	Heathrow	k1gMnSc1	Heathrow
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
Gatwick	Gatwick	k1gMnSc1	Gatwick
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
Luton	Luton	k1gMnSc1	Luton
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
Stansted	Stansted	k1gMnSc1	Stansted
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
City	City	k1gFnSc2	City
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
Biggin	Biggin	k1gMnSc1	Biggin
Hill	Hill	k1gMnSc1	Hill
<g/>
.	.	kIx.	.
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2	podrobnější
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
letišť	letiště	k1gNnPc2	letiště
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
<g/>
.	.	kIx.	.
</s>
<s>
Letiště	letiště	k1gNnSc1	letiště
Brno-Tuřany	Brno-Tuřan	k1gMnPc7	Brno-Tuřan
(	(	kIx(	(
<g/>
LKTB	LKTB	kA	LKTB
<g/>
)	)	kIx)	)
Letiště	letiště	k1gNnSc1	letiště
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
(	(	kIx(	(
<g/>
LKKV	LKKV	kA	LKKV
<g/>
)	)	kIx)	)
Letiště	letiště	k1gNnSc1	letiště
Leoše	Leoš	k1gMnSc2	Leoš
Janáčka	Janáček	k1gMnSc2	Janáček
Ostrava	Ostrava	k1gFnSc1	Ostrava
(	(	kIx(	(
<g/>
LKMT	LKMT	kA	LKMT
<g/>
)	)	kIx)	)
Letiště	letiště	k1gNnSc1	letiště
Pardubice	Pardubice	k1gInPc1	Pardubice
(	(	kIx(	(
<g/>
LKPD	LKPD	kA	LKPD
<g/>
)	)	kIx)	)
Letiště	letiště	k1gNnSc1	letiště
Václava	Václav	k1gMnSc2	Václav
Havla	Havel	k1gMnSc2	Havel
Praha	Praha	k1gFnSc1	Praha
(	(	kIx(	(
<g/>
LKPR	LKPR	kA	LKPR
<g/>
)	)	kIx)	)
Letiště	letiště	k1gNnSc1	letiště
České	český	k2eAgInPc1d1	český
Budějovice	Budějovice	k1gInPc1	Budějovice
(	(	kIx(	(
<g/>
LKCS	LKCS	kA	LKCS
<g/>
)	)	kIx)	)
Letiště	letiště	k1gNnSc1	letiště
Benešov	Benešov	k1gInSc1	Benešov
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
LKBE	LKBE	kA	LKBE
<g/>
)	)	kIx)	)
Letiště	letiště	k1gNnSc1	letiště
Zbraslavice	Zbraslavice	k1gFnSc2	Zbraslavice
(	(	kIx(	(
<g/>
LKZB	LKZB	kA	LKZB
<g/>
)	)	kIx)	)
Letiště	letiště	k1gNnSc1	letiště
Brno-Medlánky	Brno-Medlánka	k1gFnSc2	Brno-Medlánka
(	(	kIx(	(
<g/>
LKCM	LKCM	kA	LKCM
<g/>
)	)	kIx)	)
Letiště	letiště	k1gNnSc1	letiště
Kladno	Kladno	k1gNnSc1	Kladno
(	(	kIx(	(
<g/>
LKKL	LKKL	kA	LKKL
<g/>
)	)	kIx)	)
Letiště	letiště	k1gNnSc1	letiště
Praha-Kbely	Praha-Kbela	k1gFnSc2	Praha-Kbela
(	(	kIx(	(
<g/>
LKKB	LKKB	kA	LKKB
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgInSc1d1	další
ICAO	ICAO	kA	ICAO
kód	kód	k1gInSc1	kód
letiště	letiště	k1gNnSc2	letiště
IATA	IATA	kA	IATA
kód	kód	k1gInSc1	kód
letiště	letiště	k1gNnSc2	letiště
Mezinárodní	mezinárodní	k2eAgNnSc1d1	mezinárodní
letiště	letiště	k1gNnSc1	letiště
Vnitrostátní	vnitrostátní	k2eAgNnSc1d1	vnitrostátní
letiště	letiště	k1gNnSc2	letiště
Uzlové	uzlový	k2eAgNnSc4d1	uzlové
letiště	letiště	k1gNnSc4	letiště
Vzletová	vzletový	k2eAgFnSc1d1	vzletová
a	a	k8xC	a
přistávací	přistávací	k2eAgFnSc1d1	přistávací
dráha	dráha	k1gFnSc1	dráha
Pojezdová	pojezdový	k2eAgFnSc1d1	pojezdová
dráha	dráha	k1gFnSc1	dráha
Biologická	biologický	k2eAgFnSc1d1	biologická
ochrana	ochrana	k1gFnSc1	ochrana
letišť	letiště	k1gNnPc2	letiště
Nástupní	nástupní	k2eAgInSc1d1	nástupní
most	most	k1gInSc1	most
Letištní	letištní	k2eAgInSc1d1	letištní
okruh	okruh	k1gInSc1	okruh
Heliport	heliport	k1gInSc1	heliport
Helipad	Helipad	k1gInSc4	Helipad
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
letiště	letiště	k1gNnSc2	letiště
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
letiště	letiště	k1gNnSc2	letiště
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
