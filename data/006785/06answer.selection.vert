<s>
Největší	veliký	k2eAgNnSc1d3	veliký
dopravní	dopravní	k2eAgNnSc1d1	dopravní
letiště	letiště	k1gNnSc1	letiště
světa	svět	k1gInSc2	svět
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Atlantě	Atlanta	k1gFnSc6	Atlanta
<g/>
,	,	kIx,	,
Georgia	Georgia	k1gFnSc1	Georgia
(	(	kIx(	(
<g/>
USA	USA	kA	USA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
počet	počet	k1gInSc1	počet
přepravených	přepravený	k2eAgMnPc2d1	přepravený
cestujících	cestující	k1gMnPc2	cestující
je	být	k5eAaImIp3nS	být
vyšší	vysoký	k2eAgFnSc1d2	vyšší
v	v	k7c6	v
Londýně	Londýn	k1gInSc6	Londýn
díky	díky	k7c3	díky
většímu	veliký	k2eAgInSc3d2	veliký
počtu	počet	k1gInSc3	počet
letišť	letiště	k1gNnPc2	letiště
<g/>
:	:	kIx,	:
London	London	k1gMnSc1	London
Heathrow	Heathrow	k1gMnSc1	Heathrow
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
Gatwick	Gatwick	k1gMnSc1	Gatwick
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
Luton	Luton	k1gMnSc1	Luton
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
Stansted	Stansted	k1gMnSc1	Stansted
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
City	City	k1gFnSc2	City
<g/>
,	,	kIx,	,
London	London	k1gMnSc1	London
Biggin	Biggin	k1gMnSc1	Biggin
Hill	Hill	k1gMnSc1	Hill
<g/>
.	.	kIx.	.
</s>
