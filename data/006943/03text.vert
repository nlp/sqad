<s>
Trutnov	Trutnov	k1gInSc1	Trutnov
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Trautenau	Trautenaus	k1gInSc3	Trautenaus
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
město	město	k1gNnSc1	město
v	v	k7c6	v
Královéhradeckém	královéhradecký	k2eAgInSc6d1	královéhradecký
kraji	kraj	k1gInSc6	kraj
v	v	k7c6	v
severovýchodních	severovýchodní	k2eAgFnPc6d1	severovýchodní
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
Trutnov	Trutnov	k1gInSc1	Trutnov
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Krkonošském	krkonošský	k2eAgNnSc6d1	Krkonošské
podhůří	podhůří	k1gNnSc6	podhůří
na	na	k7c6	na
řece	řeka	k1gFnSc6	řeka
Úpě	Úpa	k1gFnSc6	Úpa
<g/>
.	.	kIx.	.
</s>
<s>
Žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
téměř	téměř	k6eAd1	téměř
31	[number]	k4	31
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
rozlohu	rozloha	k1gFnSc4	rozloha
10	[number]	k4	10
336	[number]	k4	336
ha	ha	kA	ha
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnPc2	obyvatel
je	být	k5eAaImIp3nS	být
35	[number]	k4	35
<g/>
.	.	kIx.	.
největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
České	český	k2eAgFnSc2d1	Česká
republiky	republika	k1gFnSc2	republika
a	a	k8xC	a
podle	podle	k7c2	podle
katastrální	katastrální	k2eAgFnSc2d1	katastrální
rozlohy	rozloha	k1gFnSc2	rozloha
dokonce	dokonce	k9	dokonce
13	[number]	k4	13
<g/>
.	.	kIx.	.
největším	veliký	k2eAgInSc7d3	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Historické	historický	k2eAgNnSc1d1	historické
jádro	jádro	k1gNnSc1	jádro
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
městskou	městský	k2eAgFnSc7d1	městská
památkovou	památkový	k2eAgFnSc7d1	památková
zónou	zóna	k1gFnSc7	zóna
<g/>
.	.	kIx.	.
</s>
<s>
Trutnov	Trutnov	k1gInSc1	Trutnov
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
z	z	k7c2	z
21	[number]	k4	21
místních	místní	k2eAgFnPc2d1	místní
částí	část	k1gFnPc2	část
<g/>
:	:	kIx,	:
Adamov	Adamov	k1gInSc1	Adamov
<g/>
,	,	kIx,	,
Babí	babí	k2eAgFnPc1d1	babí
<g/>
,	,	kIx,	,
Bohuslavice	Bohuslavice	k1gFnPc1	Bohuslavice
<g/>
,	,	kIx,	,
Bojiště	bojiště	k1gNnSc1	bojiště
<g/>
,	,	kIx,	,
Dolní	dolní	k2eAgNnSc1d1	dolní
Předměstí	předměstí	k1gNnSc1	předměstí
<g/>
,	,	kIx,	,
Dolní	dolní	k2eAgNnSc1d1	dolní
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
Horní	horní	k2eAgNnSc1d1	horní
Předměstí	předměstí	k1gNnSc1	předměstí
<g/>
,	,	kIx,	,
Horní	horní	k2eAgNnSc1d1	horní
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
Kryblice	Kryblice	k1gFnSc1	Kryblice
<g/>
,	,	kIx,	,
Lhota	Lhota	k1gFnSc1	Lhota
<g/>
,	,	kIx,	,
Libeč	Libeč	k1gInSc1	Libeč
<g/>
,	,	kIx,	,
Nový	nový	k2eAgInSc1d1	nový
Rokytník	Rokytník	k1gInSc1	Rokytník
<g/>
,	,	kIx,	,
Oblanov	Oblanov	k1gInSc1	Oblanov
<g/>
,	,	kIx,	,
Poříčí	poříčí	k1gNnPc1	poříčí
<g/>
,	,	kIx,	,
Starý	starý	k2eAgInSc1d1	starý
Rokytník	Rokytník	k1gInSc1	Rokytník
<g/>
,	,	kIx,	,
Střední	střední	k2eAgNnSc1d1	střední
Předměstí	předměstí	k1gNnSc1	předměstí
<g/>
,	,	kIx,	,
Střítež	Střítež	k1gFnSc1	Střítež
<g/>
,	,	kIx,	,
Studenec	Studenec	k1gInSc1	Studenec
<g/>
,	,	kIx,	,
Vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
Volanov	Volanov	k1gInSc1	Volanov
a	a	k8xC	a
Voletiny	Voletin	k2eAgFnPc1d1	Voletin
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
k	k	k7c3	k
Trutnovu	Trutnov	k1gInSc3	Trutnov
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
osady	osada	k1gFnSc2	osada
a	a	k8xC	a
čtvrtě	čtvrt	k1gFnSc2	čtvrt
<g/>
:	:	kIx,	:
Bezděkov	Bezděkov	k1gInSc1	Bezděkov
<g/>
,	,	kIx,	,
Debrné	Debrný	k2eAgFnPc1d1	Debrný
<g/>
,	,	kIx,	,
Dolce	dolec	k1gInPc1	dolec
<g/>
,	,	kIx,	,
Dolníky	Dolník	k1gInPc1	Dolník
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kacíř	kacíř	k1gMnSc1	kacíř
<g/>
,	,	kIx,	,
Kalná	kalný	k2eAgFnSc1d1	kalná
Voda	voda	k1gFnSc1	voda
(	(	kIx(	(
<g/>
část	část	k1gFnSc1	část
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Kouty	kout	k1gInPc1	kout
<g/>
,	,	kIx,	,
Luční	luční	k2eAgInPc1d1	luční
Domky	domek	k1gInPc1	domek
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgInPc1d1	Nové
Dvory	Dvůr	k1gInPc1	Dvůr
<g/>
,	,	kIx,	,
Nové	Nové	k2eAgFnPc1d1	Nové
Voletiny	Voletin	k2eAgFnPc1d1	Voletin
<g/>
,	,	kIx,	,
Peklo	peklo	k1gNnSc1	peklo
<g/>
,	,	kIx,	,
Rubínovice	Rubínovice	k1gFnSc1	Rubínovice
<g/>
,	,	kIx,	,
Zelená	zelený	k2eAgFnSc1d1	zelená
Louka	louka	k1gFnSc1	louka
aj.	aj.	kA	aj.
Trutnov	Trutnov	k1gInSc1	Trutnov
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
Trutnovské	trutnovský	k2eAgFnSc6d1	trutnovská
pahorkatině	pahorkatina	k1gFnSc6	pahorkatina
na	na	k7c6	na
východě	východ	k1gInSc6	východ
Podkrkonošské	podkrkonošský	k2eAgFnSc2d1	Podkrkonošská
pahorkatiny	pahorkatina	k1gFnSc2	pahorkatina
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
součástí	součást	k1gFnSc7	součást
celku	celek	k1gInSc2	celek
Krkonošské	krkonošský	k2eAgNnSc4d1	Krkonošské
podhůří	podhůří	k1gNnSc4	podhůří
<g/>
.	.	kIx.	.
</s>
<s>
Nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
centra	centrum	k1gNnSc2	centrum
Trutnova	Trutnov	k1gInSc2	Trutnov
se	se	k3xPyFc4	se
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
kolem	kolem	k7c2	kolem
430	[number]	k4	430
m.	m.	k?	m.
Trutnovem	Trutnov	k1gInSc7	Trutnov
protéká	protékat	k5eAaImIp3nS	protékat
řeka	řeka	k1gFnSc1	řeka
Úpa	Úpa	k1gFnSc1	Úpa
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
v	v	k7c6	v
Poříčí	Poříčí	k1gNnSc6	Poříčí
přibírá	přibírat	k5eAaImIp3nS	přibírat
zleva	zleva	k6eAd1	zleva
Ličnou	Ličná	k1gFnSc4	Ličná
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
volbách	volba	k1gFnPc6	volba
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
2014	[number]	k4	2014
se	se	k3xPyFc4	se
do	do	k7c2	do
trutnovského	trutnovský	k2eAgNnSc2d1	trutnovské
zastupitelstva	zastupitelstvo	k1gNnSc2	zastupitelstvo
dostalo	dostat	k5eAaPmAgNnS	dostat
celkem	celkem	k6eAd1	celkem
osm	osm	k4xCc1	osm
z	z	k7c2	z
devíti	devět	k4xCc2	devět
kandidujících	kandidující	k2eAgFnPc2d1	kandidující
stran	strana	k1gFnPc2	strana
a	a	k8xC	a
hnutí	hnutí	k1gNnPc2	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
Koalici	koalice	k1gFnSc4	koalice
následně	následně	k6eAd1	následně
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
ODS	ODS	kA	ODS
<g/>
,	,	kIx,	,
Volba	volba	k1gFnSc1	volba
pro	pro	k7c4	pro
město	město	k1gNnSc4	město
Trutnov	Trutnov	k1gInSc1	Trutnov
<g/>
,	,	kIx,	,
Sdružení	sdružení	k1gNnSc1	sdružení
pro	pro	k7c4	pro
Trutnov	Trutnov	k1gInSc4	Trutnov
a	a	k8xC	a
TOP	topit	k5eAaImRp2nS	topit
0	[number]	k4	0
<g/>
9	[number]	k4	9
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
většinu	většina	k1gFnSc4	většina
18	[number]	k4	18
z	z	k7c2	z
33	[number]	k4	33
hlasů	hlas	k1gInPc2	hlas
<g/>
.	.	kIx.	.
</s>
<s>
Zdroj	zdroj	k1gInSc1	zdroj
<g/>
:	:	kIx,	:
volby	volba	k1gFnPc1	volba
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Související	související	k2eAgFnPc4d1	související
informace	informace	k1gFnPc4	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Dějiny	dějiny	k1gFnPc1	dějiny
Trutnova	Trutnov	k1gInSc2	Trutnov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
místech	místo	k1gNnPc6	místo
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Horního	horní	k2eAgNnSc2d1	horní
Starého	Starého	k2eAgNnSc2d1	Starého
Města	město	k1gNnSc2	město
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
osada	osada	k1gFnSc1	osada
Úpa	Úpa	k1gFnSc1	Úpa
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
patrně	patrně	k6eAd1	patrně
kvůli	kvůli	k7c3	kvůli
opakujícím	opakující	k2eAgInPc3d1	opakující
se	se	k3xPyFc4	se
povodním	povodeň	k1gFnPc3	povodeň
přesunuta	přesunout	k5eAaPmNgFnS	přesunout
do	do	k7c2	do
míst	místo	k1gNnPc2	místo
dnešního	dnešní	k2eAgInSc2d1	dnešní
Trutnova	Trutnov	k1gInSc2	Trutnov
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
přesnost	přesnost	k1gFnSc4	přesnost
je	být	k5eAaImIp3nS	být
dobré	dobrý	k2eAgNnSc1d1	dobré
poznamenat	poznamenat	k5eAaPmF	poznamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
jiná	jiný	k2eAgFnSc1d1	jiná
obec	obec	k1gFnSc1	obec
s	s	k7c7	s
původním	původní	k2eAgInSc7d1	původní
názvem	název	k1gInSc7	název
Grose	gros	k1gInSc5	gros
Aupe	Aup	k1gFnPc1	Aup
(	(	kIx(	(
<g/>
Velká	velký	k2eAgFnSc1d1	velká
Úpa	Úpa	k1gFnSc1	Úpa
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
rozdělena	rozdělit	k5eAaPmNgFnS	rozdělit
na	na	k7c4	na
dnešní	dnešní	k2eAgFnPc4d1	dnešní
dvě	dva	k4xCgFnPc4	dva
části	část	k1gFnPc4	část
Pec	Pec	k1gFnSc4	Pec
pod	pod	k7c7	pod
Sněžkou	Sněžka	k1gFnSc7	Sněžka
1	[number]	k4	1
a	a	k8xC	a
Velká	velký	k2eAgFnSc1d1	velká
Úpa	Úpa	k1gFnSc1	Úpa
(	(	kIx(	(
<g/>
Pec	Pec	k1gFnSc1	Pec
p.	p.	k?	p.
S.	S.	kA	S.
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
písemná	písemný	k2eAgFnSc1d1	písemná
zmínka	zmínka	k1gFnSc1	zmínka
o	o	k7c6	o
Trutnově	Trutnov	k1gInSc6	Trutnov
je	být	k5eAaImIp3nS	být
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1260	[number]	k4	1260
<g/>
,	,	kIx,	,
Úpa	Úpa	k1gFnSc1	Úpa
získala	získat	k5eAaPmAgFnS	získat
práva	práv	k2eAgFnSc1d1	práva
tržní	tržní	k2eAgFnSc1d1	tržní
<g/>
,	,	kIx,	,
práva	práv	k2eAgFnSc1d1	práva
vykonávat	vykonávat	k5eAaImF	vykonávat
nižší	nízký	k2eAgNnSc4d2	nižší
soudnictví	soudnictví	k1gNnSc4	soudnictví
a	a	k8xC	a
právo	právo	k1gNnSc4	právo
vařit	vařit	k5eAaImF	vařit
pivo	pivo	k1gNnSc4	pivo
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
města	město	k1gNnSc2	město
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
německého	německý	k2eAgInSc2d1	německý
truten	truten	k2eAgMnSc1d1	truten
ouwe	ouwe	k1gFnSc7	ouwe
–	–	k?	–
utěšená	utěšený	k2eAgFnSc1d1	utěšená
niva	niva	k1gFnSc1	niva
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
pověsti	pověst	k1gFnSc2	pověst
bylo	být	k5eAaImAgNnS	být
nazváno	nazvat	k5eAaPmNgNnS	nazvat
po	po	k7c6	po
rytíři	rytíř	k1gMnSc6	rytíř
Trutovi	Trut	k1gMnSc6	Trut
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
práce	práce	k1gFnSc2	práce
předního	přední	k2eAgMnSc2d1	přední
znalce	znalec	k1gMnSc2	znalec
doby	doba	k1gFnSc2	doba
přemyslovských	přemyslovský	k2eAgMnPc2d1	přemyslovský
králů	král	k1gMnPc2	král
profesora	profesor	k1gMnSc2	profesor
Josefa	Josef	k1gMnSc2	Josef
Žemličky	Žemlička	k1gMnSc2	Žemlička
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
Trutnov	Trutnov	k1gInSc1	Trutnov
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
spojen	spojit	k5eAaPmNgInS	spojit
s	s	k7c7	s
fundacemi	fundace	k1gFnPc7	fundace
zakládanými	zakládaný	k2eAgFnPc7d1	zakládaná
Přemyslem	Přemysl	k1gMnSc7	Přemysl
Otakarem	Otakar	k1gMnSc7	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
</s>
<s>
Josef	Josef	k1gMnSc1	Josef
Žemlička	Žemlička	k1gMnSc1	Žemlička
píše	psát	k5eAaImIp3nS	psát
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
práci	práce	k1gFnSc6	práce
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
král	král	k1gMnSc1	král
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
věků	věk	k1gInPc2	věk
(	(	kIx(	(
<g/>
str	str	kA	str
<g/>
.	.	kIx.	.
222	[number]	k4	222
-	-	kIx~	-
223	[number]	k4	223
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Ani	ani	k8xC	ani
počátky	počátek	k1gInPc1	počátek
zdejšího	zdejší	k2eAgNnSc2d1	zdejší
manství	manství	k1gNnSc2	manství
nejsou	být	k5eNaImIp3nP	být
bez	bez	k7c2	bez
otazníků	otazník	k1gInPc2	otazník
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
krajinu	krajina	k1gFnSc4	krajina
na	na	k7c6	na
horní	horní	k2eAgFnSc6d1	horní
Úpě	Úpa	k1gFnSc6	Úpa
poznamenaly	poznamenat	k5eAaPmAgFnP	poznamenat
dynamické	dynamický	k2eAgFnPc4d1	dynamická
změny	změna	k1gFnPc4	změna
provázené	provázený	k2eAgFnPc4d1	provázená
usazováním	usazování	k1gNnSc7	usazování
německých	německý	k2eAgMnPc2d1	německý
osadníků	osadník	k1gMnPc2	osadník
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
<g/>
,	,	kIx,	,
již	již	k6eAd1	již
v	v	k7c6	v
Přemyslovské	přemyslovský	k2eAgFnSc6d1	Přemyslovská
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
rýsoval	rýsovat	k5eAaImAgInS	rýsovat
nevelký	velký	k2eNgInSc1d1	nevelký
zeměpanský	zeměpanský	k2eAgInSc1d1	zeměpanský
manský	manský	k2eAgInSc1d1	manský
okruh	okruh	k1gInSc1	okruh
kolem	kolem	k7c2	kolem
Dvora	Dvůr	k1gInSc2	Dvůr
(	(	kIx(	(
<g/>
Králové	Králová	k1gFnSc2	Králová
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
kolonizace	kolonizace	k1gFnSc1	kolonizace
Úpska	Úpsek	k1gInSc2	Úpsek
<g/>
/	/	kIx~	/
<g/>
Trutnovska	Trutnovsko	k1gNnPc1	Trutnovsko
probíhala	probíhat	k5eAaImAgNnP	probíhat
pod	pod	k7c7	pod
taktovkou	taktovka	k1gFnSc7	taktovka
Švábeniců	Švábeniec	k1gInPc2	Švábeniec
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
v	v	k7c4	v
ně	on	k3xPp3gFnPc4	on
uvázali	uvázat	k5eAaPmAgMnP	uvázat
<g/>
,	,	kIx,	,
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
nejasné	jasný	k2eNgNnSc1d1	nejasné
(	(	kIx(	(
<g/>
dar	dar	k1gInSc1	dar
<g/>
,	,	kIx,	,
správa	správa	k1gFnSc1	správa
<g/>
,	,	kIx,	,
propůjčení	propůjčení	k1gNnSc1	propůjčení
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k6eAd1	až
koncem	koncem	k7c2	koncem
13	[number]	k4	13
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
Švábenicové	Švábenicový	k2eAgNnSc1d1	Švábenicový
uprostřed	uprostřed	k7c2	uprostřed
načatého	načatý	k2eAgNnSc2d1	načaté
díla	dílo	k1gNnSc2	dílo
zmizeli	zmizet	k5eAaPmAgMnP	zmizet
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
bychom	by	kYmCp1nP	by
znali	znát	k5eAaImAgMnP	znát
důvody	důvod	k1gInPc4	důvod
(	(	kIx(	(
<g/>
prodej	prodej	k1gInSc1	prodej
<g/>
,	,	kIx,	,
směna	směna	k1gFnSc1	směna
<g/>
,	,	kIx,	,
zabavení	zabavení	k1gNnSc1	zabavení
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Brzy	brzy	k6eAd1	brzy
se	se	k3xPyFc4	se
Dvorsko	Dvorsko	k1gNnSc1	Dvorsko
spojilo	spojit	k5eAaPmAgNnS	spojit
s	s	k7c7	s
Trutnovskem	Trutnovsko	k1gNnSc7	Trutnovsko
a	a	k8xC	a
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
manský	manský	k2eAgInSc1d1	manský
kraj	kraj	k1gInSc1	kraj
<g/>
,	,	kIx,	,
nazvaný	nazvaný	k2eAgInSc1d1	nazvaný
po	po	k7c6	po
Trutnovsku	Trutnovsko	k1gNnSc6	Trutnovsko
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
přestal	přestat	k5eAaPmAgInS	přestat
náležet	náležet	k5eAaImF	náležet
k	k	k7c3	k
hradecké	hradecký	k2eAgFnSc3d1	hradecká
provincii	provincie	k1gFnSc3	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
druhotně	druhotně	k6eAd1	druhotně
si	se	k3xPyFc3	se
vymohl	vymoct	k5eAaPmAgMnS	vymoct
svébytné	svébytný	k2eAgNnSc4d1	svébytné
postavení	postavení	k1gNnSc4	postavení
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Josef	Josef	k1gMnSc1	Josef
Žemlička	Žemlička	k1gMnSc1	Žemlička
dále	daleko	k6eAd2	daleko
uvádí	uvádět	k5eAaImIp3nS	uvádět
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
výše	vysoce	k6eAd2	vysoce
jmenované	jmenovaný	k2eAgFnSc6d1	jmenovaná
práci	práce	k1gFnSc6	práce
na	na	k7c4	na
str	str	kA	str
<g/>
.	.	kIx.	.
223	[number]	k4	223
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
r.	r.	kA	r.
1340	[number]	k4	1340
vydává	vydávat	k5eAaPmIp3nS	vydávat
Jan	Jan	k1gMnSc1	Jan
Lucemburský	lucemburský	k2eAgMnSc1d1	lucemburský
listinu	listina	k1gFnSc4	listina
určenou	určený	k2eAgFnSc4d1	určená
pro	pro	k7c4	pro
obyvatele	obyvatel	k1gMnSc4	obyvatel
měst	město	k1gNnPc2	město
Trutnova	Trutnov	k1gInSc2	Trutnov
a	a	k8xC	a
Dvora	Dvůr	k1gInSc2	Dvůr
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
král	král	k1gMnSc1	král
osvobozuje	osvobozovat	k5eAaImIp3nS	osvobozovat
tyto	tento	k3xDgMnPc4	tento
obyvatele	obyvatel	k1gMnPc4	obyvatel
z	z	k7c2	z
provinciálních	provinciální	k2eAgInPc2d1	provinciální
soudů	soud	k1gInPc2	soud
s	s	k7c7	s
odvolávkou	odvolávka	k1gFnSc7	odvolávka
na	na	k7c4	na
předchůdce	předchůdce	k1gMnSc4	předchůdce
<g/>
.	.	kIx.	.
</s>
<s>
Myšleni	myšlen	k2eAgMnPc1d1	myšlen
jsou	být	k5eAaImIp3nP	být
jistě	jistě	k9	jistě
jeho	jeho	k3xOp3gMnPc1	jeho
předchůdci	předchůdce	k1gMnPc1	předchůdce
<g/>
,	,	kIx,	,
tedy	tedy	k8xC	tedy
především	především	k9	především
Přemyslovci	Přemyslovec	k1gMnPc1	Přemyslovec
(	(	kIx(	(
<g/>
Václav	Václav	k1gMnSc1	Václav
II	II	kA	II
<g/>
.	.	kIx.	.
a	a	k8xC	a
Přemysl	Přemysl	k1gMnSc1	Přemysl
Otakar	Otakar	k1gMnSc1	Otakar
II	II	kA	II
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1421	[number]	k4	1421
byl	být	k5eAaImAgInS	být
Trutnov	Trutnov	k1gInSc1	Trutnov
dobyt	dobyt	k2eAgInSc1d1	dobyt
husitským	husitský	k2eAgNnSc7d1	husitské
vojskem	vojsko	k1gNnSc7	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1582	[number]	k4	1582
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
měšťanský	měšťanský	k2eAgInSc1d1	měšťanský
pivovar	pivovar	k1gInSc1	pivovar
<g/>
,	,	kIx,	,
který	který	k3yQgInSc4	který
byl	být	k5eAaImAgMnS	být
majetkem	majetek	k1gInSc7	majetek
149	[number]	k4	149
pravovárečných	pravovárečný	k2eAgInPc2d1	pravovárečný
domů	dům	k1gInPc2	dům
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byly	být	k5eAaImAgFnP	být
zbudovány	zbudován	k2eAgFnPc1d1	zbudována
kamenné	kamenný	k2eAgFnPc1d1	kamenná
hradby	hradba	k1gFnPc1	hradba
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1642	[number]	k4	1642
byl	být	k5eAaImAgInS	být
Trutnov	Trutnov	k1gInSc1	Trutnov
dobyt	dobyt	k2eAgInSc1d1	dobyt
švédským	švédský	k2eAgNnSc7d1	švédské
vojskem	vojsko	k1gNnSc7	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1745	[number]	k4	1745
byl	být	k5eAaImAgInS	být
Trutnov	Trutnov	k1gInSc1	Trutnov
postižen	postihnout	k5eAaPmNgInS	postihnout
požárem	požár	k1gInSc7	požár
a	a	k8xC	a
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1861	[number]	k4	1861
opět	opět	k6eAd1	opět
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1866	[number]	k4	1866
zde	zde	k6eAd1	zde
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
bitvě	bitva	k1gFnSc3	bitva
mezi	mezi	k7c7	mezi
rakouskými	rakouský	k2eAgNnPc7d1	rakouské
a	a	k8xC	a
pruskými	pruský	k2eAgNnPc7d1	pruské
vojsky	vojsko	k1gNnPc7	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
29	[number]	k4	29
<g/>
.	.	kIx.	.
na	na	k7c4	na
30	[number]	k4	30
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1897	[number]	k4	1897
byl	být	k5eAaImAgInS	být
Trutnov	Trutnov	k1gInSc1	Trutnov
silně	silně	k6eAd1	silně
zasažen	zasažen	k2eAgMnSc1d1	zasažen
povodní	povodní	k2eAgMnSc1d1	povodní
<g/>
.	.	kIx.	.
</s>
<s>
Budova	budova	k1gFnSc1	budova
gymnázia	gymnázium	k1gNnPc4	gymnázium
na	na	k7c6	na
Jiráskově	Jiráskův	k2eAgNnSc6d1	Jiráskovo
náměstí	náměstí	k1gNnSc6	náměstí
byla	být	k5eAaImAgNnP	být
postavena	postavit	k5eAaPmNgNnP	postavit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1920	[number]	k4	1920
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	naleznout	k5eAaPmIp2nP	naleznout
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Pověst	pověst	k1gFnSc1	pověst
o	o	k7c6	o
založení	založení	k1gNnSc6	založení
Trutnova	Trutnov	k1gInSc2	Trutnov
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jedné	jeden	k4xCgFnSc2	jeden
verze	verze	k1gFnSc2	verze
založil	založit	k5eAaPmAgMnS	založit
město	město	k1gNnSc4	město
pán	pán	k1gMnSc1	pán
z	z	k7c2	z
Trautenbergu	Trautenberg	k1gInSc2	Trautenberg
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jiné	jiný	k2eAgFnSc2d1	jiná
rytíř	rytíř	k1gMnSc1	rytíř
Trut	trout	k5eAaImNgMnS	trout
<g/>
,	,	kIx,	,
když	když	k8xS	když
svedli	svést	k5eAaPmAgMnP	svést
vítězný	vítězný	k2eAgInSc4d1	vítězný
souboj	souboj	k1gInSc4	souboj
se	s	k7c7	s
zde	zde	k6eAd1	zde
sídlícím	sídlící	k2eAgInSc7d1	sídlící
drakem	drak	k1gInSc7	drak
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
paměť	paměť	k1gFnSc4	paměť
mýtické	mýtický	k2eAgFnSc2d1	mýtická
události	událost	k1gFnSc2	událost
se	se	k3xPyFc4	se
ve	v	k7c6	v
městě	město	k1gNnSc6	město
každoročně	každoročně	k6eAd1	každoročně
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
konají	konat	k5eAaImIp3nP	konat
slavnosti	slavnost	k1gFnPc1	slavnost
draka	drak	k1gMnSc2	drak
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
pořádá	pořádat	k5eAaImIp3nS	pořádat
spolek	spolek	k1gInSc1	spolek
Trutnov	Trutnov	k1gInSc1	Trutnov
-	-	kIx~	-
město	město	k1gNnSc1	město
draka	drak	k1gMnSc2	drak
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnPc2	sčítání
1921	[number]	k4	1921
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c4	v
1	[number]	k4	1
019	[number]	k4	019
domech	dům	k1gInPc6	dům
14	[number]	k4	14
584	[number]	k4	584
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
bylo	být	k5eAaImAgNnS	být
7	[number]	k4	7
665	[number]	k4	665
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
2	[number]	k4	2
791	[number]	k4	791
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
československé	československý	k2eAgFnSc3d1	Československá
národnosti	národnost	k1gFnSc3	národnost
<g/>
,	,	kIx,	,
11	[number]	k4	11
412	[number]	k4	412
k	k	k7c3	k
německé	německý	k2eAgFnSc3d1	německá
a	a	k8xC	a
53	[number]	k4	53
k	k	k7c3	k
židovské	židovská	k1gFnSc3	židovská
<g/>
.	.	kIx.	.
</s>
<s>
Žilo	žít	k5eAaImAgNnS	žít
zde	zde	k6eAd1	zde
13	[number]	k4	13
065	[number]	k4	065
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
448	[number]	k4	448
evangelíků	evangelík	k1gMnPc2	evangelík
<g/>
,	,	kIx,	,
115	[number]	k4	115
příslušníků	příslušník	k1gMnPc2	příslušník
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
a	a	k8xC	a
397	[number]	k4	397
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
sčítání	sčítání	k1gNnPc2	sčítání
1930	[number]	k4	1930
zde	zde	k6eAd1	zde
žilo	žít	k5eAaImAgNnS	žít
v	v	k7c4	v
1	[number]	k4	1
321	[number]	k4	321
domech	dům	k1gInPc6	dům
15	[number]	k4	15
923	[number]	k4	923
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
3	[number]	k4	3
879	[number]	k4	879
obyvatel	obyvatel	k1gMnPc2	obyvatel
se	se	k3xPyFc4	se
hlásilo	hlásit	k5eAaImAgNnS	hlásit
k	k	k7c3	k
československé	československý	k2eAgFnSc3d1	Československá
národnosti	národnost	k1gFnSc3	národnost
a	a	k8xC	a
11	[number]	k4	11
619	[number]	k4	619
k	k	k7c3	k
německé	německý	k2eAgFnSc2d1	německá
<g/>
.	.	kIx.	.
</s>
<s>
Žilo	žít	k5eAaImAgNnS	žít
zde	zde	k6eAd1	zde
13	[number]	k4	13
396	[number]	k4	396
římských	římský	k2eAgMnPc2d1	římský
katolíků	katolík	k1gMnPc2	katolík
<g/>
,	,	kIx,	,
632	[number]	k4	632
evangelíků	evangelík	k1gMnPc2	evangelík
<g/>
,	,	kIx,	,
621	[number]	k4	621
příslušníků	příslušník	k1gMnPc2	příslušník
Církve	církev	k1gFnSc2	církev
československé	československý	k2eAgFnSc2d1	Československá
husitské	husitský	k2eAgFnSc2d1	husitská
a	a	k8xC	a
369	[number]	k4	369
židů	žid	k1gMnPc2	žid
<g/>
.	.	kIx.	.
</s>
<s>
Trutnov	Trutnov	k1gInSc1	Trutnov
je	být	k5eAaImIp3nS	být
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
zastoupen	zastoupen	k2eAgInSc1d1	zastoupen
<g/>
:	:	kIx,	:
elektrotechnický	elektrotechnický	k2eAgInSc1d1	elektrotechnický
průmysl	průmysl	k1gInSc1	průmysl
–	–	k?	–
ZPA	ZPA	kA	ZPA
Trutnov	Trutnov	k1gInSc1	Trutnov
<g/>
,	,	kIx,	,
ABB	ABB	kA	ABB
<g/>
,	,	kIx,	,
Tyco	Tyco	k1gNnSc1	Tyco
<g/>
,	,	kIx,	,
Siemens	siemens	k1gInSc1	siemens
<g/>
,	,	kIx,	,
TE	TE	kA	TE
Connectivity	Connectivita	k1gFnPc1	Connectivita
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Bean	Bean	k1gMnSc1	Bean
Technologies	Technologies	k1gMnSc1	Technologies
potravinářský	potravinářský	k2eAgInSc4d1	potravinářský
průmysl	průmysl	k1gInSc4	průmysl
–	–	k?	–
ZZN	ZZN	kA	ZZN
Trutnov	Trutnov	k1gInSc1	Trutnov
<g/>
,	,	kIx,	,
pivovar	pivovar	k1gInSc1	pivovar
Krakonoš	Krakonoš	k1gMnSc1	Krakonoš
<g/>
,	,	kIx,	,
pekárna	pekárna	k1gFnSc1	pekárna
energetický	energetický	k2eAgInSc4d1	energetický
průmysl	průmysl	k1gInSc4	průmysl
–	–	k?	–
Elektrárny	elektrárna	k1gFnSc2	elektrárna
<g />
.	.	kIx.	.
</s>
<s>
Poříčí	Poříčí	k1gNnSc1	Poříčí
(	(	kIx(	(
<g/>
EPO	EPO	kA	EPO
<g/>
)	)	kIx)	)
kožešnický	kožešnický	k2eAgInSc1d1	kožešnický
průmysl	průmysl	k1gInSc1	průmysl
–	–	k?	–
Kara	kara	k1gFnSc1	kara
Trutnov	Trutnov	k1gInSc4	Trutnov
textilní	textilní	k2eAgInSc1d1	textilní
průmysl	průmysl	k1gInSc1	průmysl
–	–	k?	–
TEXLEN	TEXLEN	kA	TEXLEN
Trutnov	Trutnov	k1gInSc1	Trutnov
(	(	kIx(	(
<g/>
zkrachovalý	zkrachovalý	k2eAgMnSc1d1	zkrachovalý
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
GRUND	GRUND	kA	GRUND
strojírenský	strojírenský	k2eAgInSc1d1	strojírenský
průmysl	průmysl	k1gInSc1	průmysl
–	–	k?	–
Kasper	Kasper	k1gInSc1	Kasper
KOVO	kovo	k1gNnSc1	kovo
automobilový	automobilový	k2eAgMnSc1d1	automobilový
-	-	kIx~	-
Continental	Continental	k1gMnSc1	Continental
Trutnov	Trutnov	k1gInSc4	Trutnov
je	být	k5eAaImIp3nS	být
významným	významný	k2eAgInSc7d1	významný
železničním	železniční	k2eAgInSc7d1	železniční
uzlem	uzel	k1gInSc7	uzel
<g/>
,	,	kIx,	,
v	v	k7c6	v
Trutnově	Trutnov	k1gInSc6	Trutnov
se	se	k3xPyFc4	se
kříží	křížit	k5eAaImIp3nS	křížit
trati	trať	k1gFnSc2	trať
032	[number]	k4	032
Jaroměř	Jaroměř	k1gFnSc1	Jaroměř
–	–	k?	–
Trutnov	Trutnov	k1gInSc1	Trutnov
<g/>
;	;	kIx,	;
047	[number]	k4	047
Trutnov	Trutnov	k1gInSc1	Trutnov
–	–	k?	–
Teplice	teplice	k1gFnSc1	teplice
nad	nad	k7c7	nad
Metují	Metuje	k1gFnSc7	Metuje
<g/>
;	;	kIx,	;
045	[number]	k4	045
Trutnov	Trutnov	k1gInSc1	Trutnov
–	–	k?	–
Svoboda	svoboda	k1gFnSc1	svoboda
nad	nad	k7c7	nad
Úpou	Úpa	k1gFnSc7	Úpa
<g/>
;	;	kIx,	;
040	[number]	k4	040
Trutnov	Trutnov	k1gInSc1	Trutnov
–	–	k?	–
Stará	starat	k5eAaImIp3nS	starat
Paka	Paka	k1gFnSc1	Paka
–	–	k?	–
Chlumec	Chlumec	k1gInSc1	Chlumec
nad	nad	k7c7	nad
Cidlinou	Cidlina	k1gFnSc7	Cidlina
<g/>
;	;	kIx,	;
043	[number]	k4	043
Trutnov	Trutnov	k1gInSc1	Trutnov
–	–	k?	–
Žacléř	Žacléř	k1gInSc1	Žacléř
<g/>
.	.	kIx.	.
</s>
<s>
Železnice	železnice	k1gFnSc1	železnice
dorazila	dorazit	k5eAaPmAgFnS	dorazit
do	do	k7c2	do
Trutnova	Trutnov	k1gInSc2	Trutnov
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1871	[number]	k4	1871
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
byla	být	k5eAaImAgFnS	být
zprovozněna	zprovozněn	k2eAgFnSc1d1	zprovozněna
trať	trať	k1gFnSc1	trať
z	z	k7c2	z
Poříčí	Poříčí	k1gNnSc2	Poříčí
<g/>
.	.	kIx.	.
</s>
<s>
Trutnov	Trutnov	k1gInSc1	Trutnov
je	být	k5eAaImIp3nS	být
také	také	k9	také
významným	významný	k2eAgInSc7d1	významný
silničním	silniční	k2eAgInSc7d1	silniční
uzlem	uzel	k1gInSc7	uzel
<g/>
,	,	kIx,	,
kříží	křížit	k5eAaImIp3nP	křížit
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
silnice	silnice	k1gFnPc1	silnice
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
16	[number]	k4	16
<g/>
,	,	kIx,	,
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
14	[number]	k4	14
a	a	k8xC	a
I	I	kA	I
<g/>
/	/	kIx~	/
<g/>
37	[number]	k4	37
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
300	[number]	k4	300
a	a	k8xC	a
II	II	kA	II
<g/>
/	/	kIx~	/
<g/>
301	[number]	k4	301
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
Trutnov	Trutnov	k1gInSc4	Trutnov
je	být	k5eAaImIp3nS	být
naplánována	naplánován	k2eAgFnSc1d1	naplánována
dálnice	dálnice	k1gFnSc1	dálnice
D11	D11	k1gFnSc1	D11
z	z	k7c2	z
Prahy	Praha	k1gFnSc2	Praha
na	na	k7c4	na
hranice	hranice	k1gFnPc4	hranice
s	s	k7c7	s
Polskem	Polsko	k1gNnSc7	Polsko
v	v	k7c6	v
Královci	Královec	k1gInSc6	Královec
<g/>
.	.	kIx.	.
</s>
<s>
Městská	městský	k2eAgFnSc1d1	městská
autobusová	autobusový	k2eAgFnSc1d1	autobusová
doprava	doprava	k1gFnSc1	doprava
v	v	k7c6	v
Trutnově	Trutnov	k1gInSc6	Trutnov
je	být	k5eAaImIp3nS	být
tvořena	tvořen	k2eAgFnSc1d1	tvořena
6	[number]	k4	6
městskými	městský	k2eAgMnPc7d1	městský
a	a	k8xC	a
několika	několik	k4yIc7	několik
integrovanými	integrovaný	k2eAgFnPc7d1	integrovaná
autobusovými	autobusový	k2eAgFnPc7d1	autobusová
linkami	linka	k1gFnPc7	linka
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
MHD	MHD	kA	MHD
a	a	k8xC	a
krajského	krajský	k2eAgInSc2d1	krajský
systému	systém	k1gInSc2	systém
IREDO	IREDO	kA	IREDO
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
Mládežnická	mládežnický	k2eAgFnSc1d1	mládežnická
<g/>
,	,	kIx,	,
Mládežnická	mládežnický	k2eAgFnSc1d1	mládežnická
536	[number]	k4	536
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
Mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
při	při	k7c6	při
nemocnici	nemocnice	k1gFnSc6	nemocnice
<g/>
,	,	kIx,	,
Gorkého	Gorkého	k2eAgFnSc1d1	Gorkého
77	[number]	k4	77
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
V	v	k7c6	v
Domcích	domek	k1gInPc6	domek
<g/>
,	,	kIx,	,
V	v	k7c6	v
Domcích	domek	k1gInPc6	domek
488	[number]	k4	488
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
Speciální	speciální	k2eAgFnSc1d1	speciální
základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
Mateřská	mateřský	k2eAgFnSc1d1	mateřská
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
Horská	Horská	k1gFnSc1	Horská
160	[number]	k4	160
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
kpt.	kpt.	k?	kpt.
Jaroše	Jaroš	k1gMnSc2	Jaroš
<g/>
,	,	kIx,	,
Gorkého	Gorkého	k2eAgNnPc2d1	Gorkého
38	[number]	k4	38
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
Náchodská	náchodský	k2eAgFnSc1d1	Náchodská
<g/>
,	,	kIx,	,
Náchodská	náchodský	k2eAgFnSc1d1	Náchodská
18	[number]	k4	18
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
Rudolfa	Rudolf	k1gMnSc2	Rudolf
Frimla	Friml	k1gMnSc2	Friml
<g/>
,	,	kIx,	,
R.	R.	kA	R.
Frimla	Frimla	k1gFnSc1	Frimla
816	[number]	k4	816
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
Základní	základní	k2eAgFnSc1d1	základní
umělecká	umělecký	k2eAgFnSc1d1	umělecká
škola	škola	k1gFnSc1	škola
Trutnov	Trutnov	k1gInSc1	Trutnov
<g/>
,	,	kIx,	,
Krakonošovo	Krakonošův	k2eAgNnSc1d1	Krakonošovo
nám.	nám.	k?	nám.
73	[number]	k4	73
<g />
.	.	kIx.	.
</s>
<s>
–	–	k?	–
ZUŠ	ZUŠ	kA	ZUŠ
Trutnov	Trutnov	k1gInSc1	Trutnov
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
Základní	základní	k2eAgFnSc1d1	základní
škola	škola	k1gFnSc1	škola
Komenského	Komenský	k1gMnSc2	Komenský
<g/>
,	,	kIx,	,
Komenského	Komenský	k1gMnSc2	Komenský
399	[number]	k4	399
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
CZECH	CZECH	kA	CZECH
SALES	SALES	kA	SALES
ACADEMY	ACADEMY	kA	ACADEMY
SOŠ	SOŠ	kA	SOŠ
a	a	k8xC	a
VOŠ	VOŠ	kA	VOŠ
Gymnázium	gymnázium	k1gNnSc4	gymnázium
Trutnov	Trutnov	k1gInSc1	Trutnov
Střední	střední	k2eAgFnSc1d1	střední
průmyslová	průmyslový	k2eAgFnSc1d1	průmyslová
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
Trutnov	Trutnov	k1gInSc1	Trutnov
<g/>
,	,	kIx,	,
Školní	školní	k2eAgFnSc1d1	školní
101	[number]	k4	101
–	–	k?	–
SPŠ	SPŠ	kA	SPŠ
Trutnov	Trutnov	k1gInSc1	Trutnov
Obchodní	obchodní	k2eAgFnSc2d1	obchodní
akademie	akademie	k1gFnSc2	akademie
Trutnov	Trutnov	k1gInSc1	Trutnov
<g/>
,	,	kIx,	,
Malé	Malá	k1gFnPc1	Malá
náměstí	náměstí	k1gNnSc1	náměstí
158	[number]	k4	158
<g/>
<g />
.	.	kIx.	.
</s>
<s>
/	/	kIx~	/
<g/>
1	[number]	k4	1
–	–	k?	–
OA	OA	kA	OA
Trutnov	Trutnov	k1gInSc1	Trutnov
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
Vyšší	vysoký	k2eAgFnSc1d2	vyšší
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
a	a	k8xC	a
Střední	střední	k2eAgFnSc1d1	střední
zdravotnická	zdravotnický	k2eAgFnSc1d1	zdravotnická
škola	škola	k1gFnSc1	škola
<g/>
,	,	kIx,	,
Procházkova	Procházkův	k2eAgFnSc1d1	Procházkova
303	[number]	k4	303
<g/>
]	]	kIx)	]
–	–	k?	–
VOŠZ	VOŠZ	kA	VOŠZ
a	a	k8xC	a
SZŠ	SZŠ	kA	SZŠ
Trutnov	Trutnov	k1gInSc1	Trutnov
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
Česká	český	k2eAgFnSc1d1	Česká
lesnická	lesnický	k2eAgFnSc1d1	lesnická
akademie	akademie	k1gFnSc1	akademie
<g/>
,	,	kIx,	,
Lesnická	lesnický	k2eAgFnSc1d1	lesnická
9	[number]	k4	9
–	–	k?	–
ČLA	ČLA	kA	ČLA
Trutnov	Trutnov	k1gInSc1	Trutnov
(	(	kIx(	(
<g/>
web	web	k1gInSc1	web
<g/>
)	)	kIx)	)
Střední	střední	k2eAgFnSc1d1	střední
odborná	odborný	k2eAgFnSc1d1	odborná
škola	škola	k1gFnSc1	škola
a	a	k8xC	a
Střední	střední	k2eAgNnSc1d1	střední
odborné	odborný	k2eAgNnSc1d1	odborné
učiliště	učiliště	k1gNnSc1	učiliště
<g/>
,	,	kIx,	,
Volanovská	Volanovský	k2eAgFnSc1d1	Volanovská
243	[number]	k4	243
–	–	k?	–
SOŠ	SOŠ	kA	SOŠ
a	a	k8xC	a
SOU	sou	k1gInSc1	sou
Trutnov	Trutnov	k1gInSc1	Trutnov
Trutnovskou	trutnovský	k2eAgFnSc4d1	trutnovská
kulturu	kultura	k1gFnSc4	kultura
zastupují	zastupovat	k5eAaImIp3nP	zastupovat
tradiční	tradiční	k2eAgFnPc4d1	tradiční
instituce	instituce	k1gFnPc4	instituce
Galerie	galerie	k1gFnSc2	galerie
města	město	k1gNnSc2	město
Trutnova	Trutnov	k1gInSc2	Trutnov
a	a	k8xC	a
Muzeum	muzeum	k1gNnSc1	muzeum
Podkrkonoší	Podkrkonoší	k1gNnSc2	Podkrkonoší
v	v	k7c6	v
Trutnově	Trutnov	k1gInSc6	Trutnov
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
zde	zde	k6eAd1	zde
působí	působit	k5eAaImIp3nS	působit
moderní	moderní	k2eAgNnSc1d1	moderní
polyfunkční	polyfunkční	k2eAgNnSc1d1	polyfunkční
kulturní	kulturní	k2eAgNnSc1d1	kulturní
centrum	centrum	k1gNnSc1	centrum
a	a	k8xC	a
divadlo	divadlo	k1gNnSc1	divadlo
Uffo	Uffo	k1gNnSc1	Uffo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
vybudováno	vybudovat	k5eAaPmNgNnS	vybudovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
<g/>
.	.	kIx.	.
</s>
<s>
Kino	kino	k1gNnSc1	kino
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
už	už	k6eAd1	už
pouze	pouze	k6eAd1	pouze
jediné	jediný	k2eAgNnSc4d1	jediné
–	–	k?	–
Vesmír	vesmír	k1gInSc4	vesmír
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
hudby	hudba	k1gFnSc2	hudba
se	se	k3xPyFc4	se
vedle	vedle	k7c2	vedle
celé	celý	k2eAgFnSc2d1	celá
plejády	plejáda	k1gFnSc2	plejáda
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
koncertů	koncert	k1gInPc2	koncert
v	v	k7c6	v
Trutnově	Trutnov	k1gInSc6	Trutnov
každoročně	každoročně	k6eAd1	každoročně
koná	konat	k5eAaImIp3nS	konat
celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
pravidelných	pravidelný	k2eAgFnPc2d1	pravidelná
hudebních	hudební	k2eAgFnPc2d1	hudební
akcí	akce	k1gFnPc2	akce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
i	i	k9	i
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
dvoudílný	dvoudílný	k2eAgInSc4d1	dvoudílný
jazz	jazz	k1gInSc4	jazz
a	a	k8xC	a
funky	funk	k1gInPc4	funk
festival	festival	k1gInSc1	festival
Jazzinec	Jazzinec	k1gInSc4	Jazzinec
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
podzim	podzim	k1gInSc4	podzim
má	mít	k5eAaImIp3nS	mít
pak	pak	k6eAd1	pak
vyhrazen	vyhradit	k5eAaPmNgInS	vyhradit
festival	festival	k1gInSc1	festival
komorní	komorní	k2eAgFnSc2d1	komorní
hudby	hudba	k1gFnSc2	hudba
Trutnovský	trutnovský	k2eAgInSc4d1	trutnovský
podzim	podzim	k1gInSc4	podzim
a	a	k8xC	a
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
konají	konat	k5eAaImIp3nP	konat
varhanní	varhanní	k2eAgInPc4d1	varhanní
koncerty	koncert	k1gInPc4	koncert
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíce	hodně	k6eAd3	hodně
hudby	hudba	k1gFnSc2	hudba
však	však	k9	však
přináší	přinášet	k5eAaImIp3nS	přinášet
léto	léto	k1gNnSc1	léto
–	–	k?	–
koná	konat	k5eAaImIp3nS	konat
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
rockový	rockový	k2eAgInSc1d1	rockový
hudební	hudební	k2eAgInSc1d1	hudební
festival	festival	k1gInSc1	festival
Trutnov	Trutnov	k1gInSc1	Trutnov
Open	Opena	k1gFnPc2	Opena
Air	Air	k1gFnPc2	Air
Festival	festival	k1gInSc1	festival
<g/>
,	,	kIx,	,
známý	známý	k2eAgInSc1d1	známý
také	také	k9	také
jako	jako	k9	jako
"	"	kIx"	"
<g/>
Východočeský	východočeský	k2eAgInSc1d1	východočeský
Woodstock	Woodstock	k1gInSc1	Woodstock
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
nejstarším	starý	k2eAgInSc7d3	nejstarší
festivalem	festival	k1gInSc7	festival
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
něj	on	k3xPp3gMnSc4	on
festival	festival	k1gInSc4	festival
extrémní	extrémní	k2eAgInSc4d1	extrémní
(	(	kIx(	(
<g/>
metal	metal	k1gInSc4	metal
<g/>
/	/	kIx~	/
<g/>
grindcore	grindcor	k1gMnSc5	grindcor
<g/>
)	)	kIx)	)
hudby	hudba	k1gFnSc2	hudba
Obsene	Obsen	k1gInSc5	Obsen
Extreme	Extrem	k1gInSc5	Extrem
Festival	festival	k1gInSc1	festival
a	a	k8xC	a
také	také	k9	také
volně	volně	k6eAd1	volně
přístupná	přístupný	k2eAgFnSc1d1	přístupná
série	série	k1gFnSc1	série
koncertů	koncert	k1gInPc2	koncert
místních	místní	k2eAgFnPc2d1	místní
kapel	kapela	k1gFnPc2	kapela
všech	všecek	k3xTgInPc2	všecek
typů	typ	k1gInPc2	typ
a	a	k8xC	a
žánrů	žánr	k1gInPc2	žánr
Trutnovské	trutnovský	k2eAgNnSc4d1	trutnovské
hudební	hudební	k2eAgNnSc4d1	hudební
léto	léto	k1gNnSc4	léto
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
další	další	k2eAgFnPc4d1	další
každoroční	každoroční	k2eAgFnPc4d1	každoroční
kulturní	kulturní	k2eAgFnPc4d1	kulturní
a	a	k8xC	a
sportovní	sportovní	k2eAgFnPc4d1	sportovní
akce	akce	k1gFnPc4	akce
patří	patřit	k5eAaImIp3nP	patřit
Pivofest	Pivofest	k1gMnSc1	Pivofest
<g/>
,	,	kIx,	,
Trutnovský	trutnovský	k2eAgMnSc1d1	trutnovský
jarmark	jarmark	k?	jarmark
<g/>
,	,	kIx,	,
Trutnovské	trutnovský	k2eAgFnPc1d1	trutnovská
vinařské	vinařský	k2eAgFnPc1d1	vinařská
slavnosti	slavnost	k1gFnPc1	slavnost
<g/>
,	,	kIx,	,
Trutnovské	trutnovský	k2eAgFnPc1d1	trutnovská
dračí	dračí	k2eAgFnPc1d1	dračí
slavnosti	slavnost	k1gFnPc1	slavnost
<g/>
,	,	kIx,	,
Den	den	k1gInSc1	den
bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Trutnova	Trutnov	k1gInSc2	Trutnov
<g/>
,	,	kIx,	,
Rallye	rallye	k1gFnSc2	rallye
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1	mezinárodní
přesah	přesah	k1gInSc1	přesah
má	mít	k5eAaImIp3nS	mít
festival	festival	k1gInSc4	festival
nového	nový	k2eAgInSc2d1	nový
cirkusu	cirkus	k1gInSc2	cirkus
Cirk-UFF	Cirk-UFF	k1gFnSc2	Cirk-UFF
Trutnov	Trutnov	k1gInSc1	Trutnov
<g/>
.	.	kIx.	.
</s>
<s>
Edvard	Edvard	k1gMnSc1	Edvard
Beneš	Beneš	k1gMnSc1	Beneš
(	(	kIx(	(
<g/>
1884	[number]	k4	1884
<g/>
–	–	k?	–
<g/>
1948	[number]	k4	1948
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
diplomat	diplomat	k1gMnSc1	diplomat
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
ČSR	ČSR	kA	ČSR
<g/>
,	,	kIx,	,
čestný	čestný	k2eAgMnSc1d1	čestný
občan	občan	k1gMnSc1	občan
města	město	k1gNnSc2	město
Trutnova	Trutnov	k1gInSc2	Trutnov
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
Lumír	Lumír	k1gMnSc1	Lumír
Čivrný	Čivrný	k2eAgMnSc1d1	Čivrný
(	(	kIx(	(
<g/>
1915	[number]	k4	1915
<g/>
–	–	k?	–
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
poslanec	poslanec	k1gMnSc1	poslanec
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
absolvent	absolvent	k1gMnSc1	absolvent
Gymnázia	gymnázium	k1gNnSc2	gymnázium
Trutnov	Trutnov	k1gInSc1	Trutnov
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
David	David	k1gMnSc1	David
(	(	kIx(	(
<g/>
1955	[number]	k4	1955
<g/>
–	–	k?	–
<g/>
2009	[number]	k4	2009
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
tanečník	tanečník	k1gMnSc1	tanečník
a	a	k8xC	a
zpěvák	zpěvák	k1gMnSc1	zpěvák
Marie	Maria	k1gFnSc2	Maria
Doležalová	Doležalová	k1gFnSc1	Doležalová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1987	[number]	k4	1987
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Dudek	Dudek	k1gMnSc1	Dudek
(	(	kIx(	(
<g/>
1932	[number]	k4	1932
<g/>
–	–	k?	–
<g/>
2000	[number]	k4	2000
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
český	český	k2eAgMnSc1d1	český
divadelní	divadelní	k2eAgMnSc1d1	divadelní
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
televizní	televizní	k2eAgMnSc1d1	televizní
režisér	režisér	k1gMnSc1	režisér
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Dvorský	Dvorský	k1gMnSc1	Dvorský
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
učitel	učitel	k1gMnSc1	učitel
při	při	k7c6	při
GTU	GTU	kA	GTU
<g/>
,	,	kIx,	,
vyznamenán	vyznamenán	k2eAgInSc1d1	vyznamenán
kulturní	kulturní	k2eAgFnSc7d1	kulturní
cenou	cena	k1gFnSc7	cena
města	město	k1gNnSc2	město
Trutnova	Trutnov	k1gInSc2	Trutnov
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2007	[number]	k4	2007
Igo	Igo	k1gMnSc1	Igo
Etrich	Etrich	k1gMnSc1	Etrich
(	(	kIx(	(
<g/>
1879	[number]	k4	1879
<g/>
–	–	k?	–
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
průkopník	průkopník	k1gMnSc1	průkopník
letectví	letectví	k1gNnSc2	letectví
Jan	Jan	k1gMnSc1	Jan
Faltis	Faltis	k1gFnSc2	Faltis
<g/>
,	,	kIx,	,
továrník	továrník	k1gMnSc1	továrník
Samuel	Samuel	k1gMnSc1	Samuel
Fritz	Fritz	k1gMnSc1	Fritz
(	(	kIx(	(
<g/>
1654	[number]	k4	1654
<g/>
–	–	k?	–
<g/>
1725	[number]	k4	1725
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
<g/>
,	,	kIx,	,
cestovatel	cestovatel	k1gMnSc1	cestovatel
<g/>
,	,	kIx,	,
kartograf	kartograf	k1gMnSc1	kartograf
a	a	k8xC	a
misionář	misionář	k1gMnSc1	misionář
<g/>
,	,	kIx,	,
autor	autor	k1gMnSc1	autor
první	první	k4xOgFnSc2	první
podrobné	podrobný	k2eAgFnSc2d1	podrobná
mapy	mapa	k1gFnSc2	mapa
Amazonky	Amazonka	k1gFnSc2	Amazonka
Ludwig	Ludwig	k1gInSc1	Ludwig
von	von	k1gInSc1	von
Gablenz	Gablenz	k1gInSc1	Gablenz
<g/>
,	,	kIx,	,
generál	generál	k1gMnSc1	generál
a	a	k8xC	a
diplomat	diplomat	k1gMnSc1	diplomat
Jan	Jan	k1gMnSc1	Jan
Haas	Haas	k1gInSc1	Haas
<g/>
,	,	kIx,	,
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Kulturní	kulturní	k2eAgFnSc2d1	kulturní
ceny	cena	k1gFnSc2	cena
města	město	k1gNnSc2	město
Trutnova	Trutnov	k1gInSc2	Trutnov
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1995	[number]	k4	1995
Aloys	Aloys	k1gInSc4	Aloys
Haase	Haase	k1gFnSc2	Haase
<g/>
,	,	kIx,	,
továrník	továrník	k1gMnSc1	továrník
a	a	k8xC	a
starosta	starosta	k1gMnSc1	starosta
města	město	k1gNnSc2	město
Jan	Jan	k1gMnSc1	Jan
Hančil	Hančil	k1gMnSc1	Hančil
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
*	*	kIx~	*
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgMnSc1d1	divadelní
vědec	vědec	k1gMnSc1	vědec
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
<g/>
,	,	kIx,	,
dramaturg	dramaturg	k1gMnSc1	dramaturg
činohry	činohra	k1gFnSc2	činohra
Národního	národní	k2eAgNnSc2d1	národní
divadla	divadlo	k1gNnSc2	divadlo
<g/>
,	,	kIx,	,
děkan	děkan	k1gMnSc1	děkan
DAMU	DAMU	kA	DAMU
<g/>
,	,	kIx,	,
rektor	rektor	k1gMnSc1	rektor
AMU	AMU	kA	AMU
Petr	Petr	k1gMnSc1	Petr
Haničinec	Haničinec	k1gMnSc1	Haničinec
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
student	student	k1gMnSc1	student
trutnovského	trutnovský	k2eAgNnSc2d1	trutnovské
gymnázia	gymnázium	k1gNnSc2	gymnázium
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1946	[number]	k4	1946
<g/>
–	–	k?	–
<g/>
1949	[number]	k4	1949
<g/>
)	)	kIx)	)
Michael	Michael	k1gMnSc1	Michael
Hauser	Hauser	k1gMnSc1	Hauser
(	(	kIx(	(
<g/>
*	*	kIx~	*
1972	[number]	k4	1972
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filozof	filozof	k1gMnSc1	filozof
<g/>
,	,	kIx,	,
překladatel	překladatel	k1gMnSc1	překladatel
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
občanského	občanský	k2eAgNnSc2d1	občanské
sdružení	sdružení	k1gNnSc2	sdružení
Socialistický	socialistický	k2eAgInSc4d1	socialistický
kruh	kruh	k1gInSc4	kruh
Jiří	Jiří	k1gMnSc1	Jiří
Havel	Havel	k1gMnSc1	Havel
(	(	kIx(	(
<g/>
*	*	kIx~	*
1931	[number]	k4	1931
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
fotograf	fotograf	k1gMnSc1	fotograf
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Kulturní	kulturní	k2eAgFnSc2d1	kulturní
ceny	cena	k1gFnSc2	cena
města	město	k1gNnSc2	město
Trutnova	Trutnov	k1gInSc2	Trutnov
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1997	[number]	k4	1997
Václav	Václav	k1gMnSc1	Václav
<g />
.	.	kIx.	.
</s>
<s>
Havel	Havel	k1gMnSc1	Havel
(	(	kIx(	(
<g/>
1936	[number]	k4	1936
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
prezident	prezident	k1gMnSc1	prezident
ČSSR	ČSSR	kA	ČSSR
<g/>
,	,	kIx,	,
ČSFR	ČSFR	kA	ČSFR
a	a	k8xC	a
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
pracoval	pracovat	k5eAaImAgMnS	pracovat
ve	v	k7c6	v
zdejším	zdejší	k2eAgInSc6d1	zdejší
pivovaru	pivovar	k1gInSc6	pivovar
Krakonoš	Krakonoš	k1gMnSc1	Krakonoš
<g/>
,	,	kIx,	,
vyznamenán	vyznamenán	k2eAgInSc1d1	vyznamenán
Kulturní	kulturní	k2eAgFnSc7d1	kulturní
cenou	cena	k1gFnSc7	cena
města	město	k1gNnSc2	město
Trutnova	Trutnov	k1gInSc2	Trutnov
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
čestný	čestný	k2eAgMnSc1d1	čestný
občan	občan	k1gMnSc1	občan
města	město	k1gNnSc2	město
Trutnova	Trutnov	k1gInSc2	Trutnov
(	(	kIx(	(
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
Gustav	Gustav	k1gMnSc1	Gustav
Hillebrand	Hillebranda	k1gFnPc2	Hillebranda
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
starosta	starosta	k1gMnSc1	starosta
Trutnova	Trutnov	k1gInSc2	Trutnov
1990	[number]	k4	1990
<g/>
–	–	k?	–
<g/>
1998	[number]	k4	1998
<g/>
,	,	kIx,	,
čestný	čestný	k2eAgMnSc1d1	čestný
občan	občan	k1gMnSc1	občan
města	město	k1gNnSc2	město
Trutnova	Trutnov	k1gInSc2	Trutnov
(	(	kIx(	(
<g/>
1999	[number]	k4	1999
<g/>
)	)	kIx)	)
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Hofman	Hofman	k1gMnSc1	Hofman
<g/>
,	,	kIx,	,
držitel	držitel	k1gMnSc1	držitel
Kulturní	kulturní	k2eAgFnSc2d1	kulturní
ceny	cena	k1gFnSc2	cena
města	město	k1gNnSc2	město
Trutnova	Trutnov	k1gInSc2	Trutnov
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2005	[number]	k4	2005
Dana	Dana	k1gFnSc1	Dana
Holá	Holá	k1gFnSc1	Holá
<g/>
,	,	kIx,	,
malířka	malířka	k1gFnSc1	malířka
<g/>
,	,	kIx,	,
nositelka	nositelka	k1gFnSc1	nositelka
Kulturní	kulturní	k2eAgFnSc2d1	kulturní
ceny	cena	k1gFnSc2	cena
města	město	k1gNnSc2	město
Trutnova	Trutnov	k1gInSc2	Trutnov
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1994	[number]	k4	1994
Bohdan	Bohdan	k1gMnSc1	Bohdan
Holomíček	Holomíček	k1gMnSc1	Holomíček
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
*	*	kIx~	*
1943	[number]	k4	1943
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Kulturní	kulturní	k2eAgFnSc2d1	kulturní
ceny	cena	k1gFnSc2	cena
města	město	k1gNnSc2	město
Trutnova	Trutnov	k1gInSc2	Trutnov
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2014	[number]	k4	2014
Uffo	Uffo	k1gMnSc1	Uffo
Horn	Horn	k1gMnSc1	Horn
(	(	kIx(	(
<g/>
1817	[number]	k4	1817
<g/>
–	–	k?	–
<g/>
1860	[number]	k4	1860
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
básník	básník	k1gMnSc1	básník
<g/>
,	,	kIx,	,
dramatik	dramatik	k1gMnSc1	dramatik
a	a	k8xC	a
revolucionář	revolucionář	k1gMnSc1	revolucionář
Simon	Simon	k1gMnSc1	Simon
Hüttel	Hüttel	k1gMnSc1	Hüttel
(	(	kIx(	(
<g/>
1530	[number]	k4	1530
<g/>
–	–	k?	–
<g/>
1601	[number]	k4	1601
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kronikář	kronikář	k1gMnSc1	kronikář
<g/>
,	,	kIx,	,
topograf	topograf	k1gMnSc1	topograf
<g />
.	.	kIx.	.
</s>
<s>
a	a	k8xC	a
geometr	geometr	k1gMnSc1	geometr
<g/>
,	,	kIx,	,
purkmistr	purkmistr	k1gMnSc1	purkmistr
Karel	Karel	k1gMnSc1	Karel
Hybner	Hybner	k1gMnSc1	Hybner
<g/>
,	,	kIx,	,
fotokronikář	fotokronikář	k1gMnSc1	fotokronikář
města	město	k1gNnSc2	město
Trutnova	Trutnov	k1gInSc2	Trutnov
<g/>
,	,	kIx,	,
čestný	čestný	k2eAgMnSc1d1	čestný
občan	občan	k1gMnSc1	občan
města	město	k1gNnSc2	město
Trutnova	Trutnov	k1gInSc2	Trutnov
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
a	a	k8xC	a
nositel	nositel	k1gMnSc1	nositel
Kulturní	kulturní	k2eAgFnPc4d1	kulturní
ceny	cena	k1gFnPc4	cena
města	město	k1gNnSc2	město
Trutnova	Trutnov	k1gInSc2	Trutnov
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2002	[number]	k4	2002
Jiří	Jiří	k1gMnSc1	Jiří
Jahoda	Jahoda	k1gMnSc1	Jahoda
<g/>
,	,	kIx,	,
fotograf	fotograf	k1gMnSc1	fotograf
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Kulturní	kulturní	k2eAgFnSc2d1	kulturní
ceny	cena	k1gFnSc2	cena
města	město	k1gNnSc2	město
Trutnova	Trutnov	k1gInSc2	Trutnov
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2004	[number]	k4	2004
Robert	Robert	k1gMnSc1	Robert
Jašków	Jašków	k1gMnSc1	Jašków
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
*	*	kIx~	*
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
Miroslav	Miroslav	k1gMnSc1	Miroslav
Janek	Janek	k1gMnSc1	Janek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1954	[number]	k4	1954
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
kameraman	kameraman	k1gMnSc1	kameraman
a	a	k8xC	a
střihač	střihač	k1gMnSc1	střihač
<g/>
,	,	kIx,	,
absolvent	absolvent	k1gMnSc1	absolvent
Gymnázia	gymnázium	k1gNnSc2	gymnázium
Trutnov	Trutnov	k1gInSc1	Trutnov
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
–	–	k?	–
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
plk.	plk.	kA	plk.
Michail	Michail	k1gMnSc1	Michail
Michailovič	Michailovič	k1gMnSc1	Michailovič
Jerzunov	Jerzunov	k1gInSc1	Jerzunov
<g/>
,	,	kIx,	,
voják	voják	k1gMnSc1	voják
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
čestný	čestný	k2eAgMnSc1d1	čestný
<g />
.	.	kIx.	.
</s>
<s>
občan	občan	k1gMnSc1	občan
města	město	k1gNnSc2	město
Trutnova	Trutnov	k1gInSc2	Trutnov
(	(	kIx(	(
<g/>
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
Václav	Václav	k1gMnSc1	Václav
Jirásek	Jirásek	k1gMnSc1	Jirásek
<g/>
,	,	kIx,	,
držitel	držitel	k1gMnSc1	držitel
Kulturní	kulturní	k2eAgFnSc2d1	kulturní
ceny	cena	k1gFnSc2	cena
města	město	k1gNnSc2	město
Trutnova	Trutnov	k1gInSc2	Trutnov
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2006	[number]	k4	2006
Tomáš	Tomáš	k1gMnSc1	Tomáš
Jirman	Jirman	k1gMnSc1	Jirman
<g/>
,	,	kIx,	,
divadelní	divadelní	k2eAgMnSc1d1	divadelní
herec	herec	k1gMnSc1	herec
Antonín	Antonín	k1gMnSc1	Antonín
Just	just	k6eAd1	just
(	(	kIx(	(
<g/>
*	*	kIx~	*
1921	[number]	k4	1921
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
kronikář	kronikář	k1gMnSc1	kronikář
Trutnova	Trutnov	k1gInSc2	Trutnov
<g/>
,	,	kIx,	,
první	první	k4xOgMnSc1	první
nositel	nositel	k1gMnSc1	nositel
Kulturní	kulturní	k2eAgFnSc2d1	kulturní
ceny	cena	k1gFnSc2	cena
města	město	k1gNnSc2	město
Trutnova	Trutnov	k1gInSc2	Trutnov
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1991	[number]	k4	1991
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čestný	čestný	k2eAgMnSc1d1	čestný
občan	občan	k1gMnSc1	občan
města	město	k1gNnSc2	město
(	(	kIx(	(
<g/>
2001	[number]	k4	2001
<g/>
)	)	kIx)	)
Tomáš	Tomáš	k1gMnSc1	Tomáš
Katschner	Katschner	k1gMnSc1	Katschner
<g/>
,	,	kIx,	,
hudebník	hudebník	k1gMnSc1	hudebník
<g/>
,	,	kIx,	,
pořadatel	pořadatel	k1gMnSc1	pořadatel
jazzového	jazzový	k2eAgInSc2d1	jazzový
festivalu	festival	k1gInSc2	festival
<g/>
,	,	kIx,	,
vyznamenán	vyznamenán	k2eAgInSc1d1	vyznamenán
Kulturní	kulturní	k2eAgFnSc7d1	kulturní
cenou	cena	k1gFnSc7	cena
města	město	k1gNnSc2	město
Trutnova	Trutnov	k1gInSc2	Trutnov
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2008	[number]	k4	2008
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Kiesewetter	Kiesewetter	k1gMnSc1	Kiesewetter
(	(	kIx(	(
<g/>
1853	[number]	k4	1853
<g/>
–	–	k?	–
<g/>
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
novinář	novinář	k1gMnSc1	novinář
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
politik	politik	k1gMnSc1	politik
Jan	Jan	k1gMnSc1	Jan
Adam	Adam	k1gMnSc1	Adam
Kluge	Kluge	k1gFnSc1	Kluge
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
textiláckého	textilácký	k2eAgInSc2d1	textilácký
rodu	rod	k1gInSc2	rod
Uršula	Uršula	k1gFnSc1	Uršula
Kluková	Kluková	k1gFnSc1	Kluková
(	(	kIx(	(
<g/>
*	*	kIx~	*
1941	[number]	k4	1941
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
absolventka	absolventka	k1gFnSc1	absolventka
Střední	střední	k2eAgFnSc2d1	střední
zdravotnické	zdravotnický	k2eAgFnSc2d1	zdravotnická
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Trutnově	Trutnov	k1gInSc6	Trutnov
Tereza	Tereza	k1gFnSc1	Tereza
Komárková	Komárková	k1gFnSc1	Komárková
<g/>
,	,	kIx,	,
nositelka	nositelka	k1gFnSc1	nositelka
Kulturní	kulturní	k2eAgFnSc2d1	kulturní
ceny	cena	k1gFnSc2	cena
města	město	k1gNnSc2	město
Trutnova	Trutnov	k1gInSc2	Trutnov
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2011	[number]	k4	2011
Ctibor	Ctibor	k1gMnSc1	Ctibor
Košťál	Košťál	k1gMnSc1	Košťál
<g/>
,	,	kIx,	,
fotograf	fotograf	k1gMnSc1	fotograf
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Kulturní	kulturní	k2eAgFnSc2d1	kulturní
ceny	cena	k1gFnSc2	cena
<g />
.	.	kIx.	.
</s>
<s>
města	město	k1gNnPc4	město
Trutnova	Trutnov	k1gInSc2	Trutnov
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2015	[number]	k4	2015
Milan	Milan	k1gMnSc1	Milan
Kout	kout	k1gInSc1	kout
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Kulturní	kulturní	k2eAgFnSc2d1	kulturní
ceny	cena	k1gFnSc2	cena
města	město	k1gNnSc2	město
Trutnova	Trutnov	k1gInSc2	Trutnov
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1995	[number]	k4	1995
Petr	Petr	k1gMnSc1	Petr
Kracík	Kracík	k1gMnSc1	Kracík
(	(	kIx(	(
<g/>
*	*	kIx~	*
1958	[number]	k4	1958
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
<g/>
,	,	kIx,	,
absolvent	absolvent	k1gMnSc1	absolvent
Gymnázia	gymnázium	k1gNnSc2	gymnázium
Trutnov	Trutnov	k1gInSc1	Trutnov
Anita	Anita	k1gMnSc1	Anita
Krausová	Krausová	k1gFnSc1	Krausová
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
<g/>
,	,	kIx,	,
rozhlasová	rozhlasový	k2eAgFnSc1d1	rozhlasová
moderátorka	moderátorka	k1gFnSc1	moderátorka
Max	Max	k1gMnSc1	Max
Kühn	Kühn	k1gMnSc1	Kühn
(	(	kIx(	(
<g/>
1877	[number]	k4	1877
<g/>
–	–	k?	–
<g/>
<g />
.	.	kIx.	.
</s>
<s>
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
architekt	architekt	k1gMnSc1	architekt
a	a	k8xC	a
pedagog	pedagog	k1gMnSc1	pedagog
Miloslav	Miloslav	k1gMnSc1	Miloslav
Lhotský	Lhotský	k1gMnSc1	Lhotský
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Kulturní	kulturní	k2eAgFnSc2d1	kulturní
ceny	cena	k1gFnSc2	cena
města	město	k1gNnSc2	město
Trutnova	Trutnov	k1gInSc2	Trutnov
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2001	[number]	k4	2001
Milan	Milan	k1gMnSc1	Milan
Lipovský	Lipovský	k1gMnSc1	Lipovský
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Kulturní	kulturní	k2eAgFnSc2d1	kulturní
ceny	cena	k1gFnSc2	cena
města	město	k1gNnSc2	město
Trutnova	Trutnov	k1gInSc2	Trutnov
za	za	k7c4	za
rok	rok	k1gInSc4	rok
1998	[number]	k4	1998
Václav	Václav	k1gMnSc1	Václav
Lohniský	Lohniský	k1gMnSc1	Lohniský
(	(	kIx(	(
<g/>
1920	[number]	k4	1920
<g/>
–	–	k?	–
<g/>
1980	[number]	k4	1980
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
absolvent	absolvent	k1gMnSc1	absolvent
Gymnázia	gymnázium	k1gNnSc2	gymnázium
<g />
.	.	kIx.	.
</s>
<s>
Trutnov	Trutnov	k1gInSc1	Trutnov
Josef	Josef	k1gMnSc1	Josef
Malejovský	Malejovský	k1gMnSc1	Malejovský
(	(	kIx(	(
<g/>
1914	[number]	k4	1914
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sochař	sochař	k1gMnSc1	sochař
<g/>
,	,	kIx,	,
řezbář	řezbář	k1gMnSc1	řezbář
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
a	a	k8xC	a
poslanec	poslanec	k1gMnSc1	poslanec
Carmen	Carmen	k2eAgInSc4d1	Carmen
Carin	Carin	k1gInSc4	Carin
Mayerová	Mayerová	k1gFnSc1	Mayerová
(	(	kIx(	(
<g/>
*	*	kIx~	*
1944	[number]	k4	1944
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herečka	herečka	k1gFnSc1	herečka
plk.	plk.	kA	plk.
Grigorij	Grigorij	k1gFnSc1	Grigorij
Arsentjevič	Arsentjevič	k1gMnSc1	Arsentjevič
Melnikov	Melnikov	k1gInSc1	Melnikov
<g/>
,	,	kIx,	,
voják	voják	k1gMnSc1	voják
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
čestný	čestný	k2eAgMnSc1d1	čestný
občan	občan	k1gMnSc1	občan
města	město	k1gNnSc2	město
Trutnova	Trutnov	k1gInSc2	Trutnov
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
1978	[number]	k4	1978
<g/>
)	)	kIx)	)
Pavel	Pavel	k1gMnSc1	Pavel
Milko	Milka	k1gFnSc5	Milka
(	(	kIx(	(
<g/>
*	*	kIx~	*
1971	[number]	k4	1971
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
teolog	teolog	k1gMnSc1	teolog
<g/>
,	,	kIx,	,
filosof	filosof	k1gMnSc1	filosof
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
<g/>
,	,	kIx,	,
pravoslavný	pravoslavný	k2eAgMnSc1d1	pravoslavný
kněz	kněz	k1gMnSc1	kněz
Josef	Josef	k1gMnSc1	Josef
Mühlberger	Mühlberger	k1gMnSc1	Mühlberger
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
plk.	plk.	kA	plk.
Jan	Jan	k1gMnSc1	Jan
Plovajko	Plovajka	k1gFnSc5	Plovajka
(	(	kIx(	(
<g/>
*	*	kIx~	*
1922	[number]	k4	1922
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
voják	voják	k1gMnSc1	voják
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Řádu	řád	k1gInSc2	řád
Bílého	bílý	k2eAgInSc2d1	bílý
lva	lev	k1gInSc2	lev
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
2010	[number]	k4	2010
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
čestný	čestný	k2eAgMnSc1d1	čestný
občan	občan	k1gMnSc1	občan
města	město	k1gNnSc2	město
Trutnova	Trutnov	k1gInSc2	Trutnov
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
Antonín	Antonín	k1gMnSc1	Antonín
Porák	Porák	k1gMnSc1	Porák
(	(	kIx(	(
<g/>
1815	[number]	k4	1815
<g/>
–	–	k?	–
<g/>
1892	[number]	k4	1892
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
lékař	lékař	k1gMnSc1	lékař
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
<g/>
,	,	kIx,	,
starosta	starosta	k1gMnSc1	starosta
Trutnova	Trutnov	k1gInSc2	Trutnov
Arnošt	Arnošt	k1gMnSc1	Arnošt
Porák	Porák	k1gMnSc1	Porák
(	(	kIx(	(
<g/>
1849	[number]	k4	1849
<g/>
–	–	k?	–
<g/>
1918	[number]	k4	1918
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
průmyslník	průmyslník	k1gMnSc1	průmyslník
<g />
.	.	kIx.	.
</s>
<s>
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Procházka	Procházka	k1gMnSc1	Procházka
<g/>
,	,	kIx,	,
profesor	profesor	k1gMnSc1	profesor
gymnázia	gymnázium	k1gNnSc2	gymnázium
<g/>
,	,	kIx,	,
tvůrce	tvůrce	k1gMnSc1	tvůrce
českého	český	k2eAgNnSc2d1	české
názvosloví	názvosloví	k1gNnSc2	názvosloví
na	na	k7c6	na
Trutnovsku	Trutnovsko	k1gNnSc6	Trutnovsko
<g/>
,	,	kIx,	,
čestný	čestný	k2eAgMnSc1d1	čestný
občan	občan	k1gMnSc1	občan
města	město	k1gNnSc2	město
Trutnova	Trutnov	k1gInSc2	Trutnov
(	(	kIx(	(
<g/>
1969	[number]	k4	1969
<g/>
)	)	kIx)	)
Robert	Robert	k1gMnSc1	Robert
Rosenberg	Rosenberg	k1gMnSc1	Rosenberg
(	(	kIx(	(
<g/>
*	*	kIx~	*
1975	[number]	k4	1975
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pornoherec	pornoherec	k1gMnSc1	pornoherec
Aleš	Aleš	k1gMnSc1	Aleš
Říha	Říha	k1gMnSc1	Říha
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g/>
,	,	kIx,	,
cestovatel	cestovatel	k1gMnSc1	cestovatel
Emil	Emil	k1gMnSc1	Emil
Schwantner	Schwantner	k1gMnSc1	Schwantner
(	(	kIx(	(
<g/>
1880	[number]	k4	1880
<g/>
–	–	k?	–
<g />
.	.	kIx.	.
</s>
<s>
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sochař	sochař	k1gMnSc1	sochař
Jiří	Jiří	k1gMnSc1	Jiří
Šebánek	Šebánek	k1gMnSc1	Šebánek
(	(	kIx(	(
<g/>
1930	[number]	k4	1930
<g/>
–	–	k?	–
<g/>
2007	[number]	k4	2007
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
spisovatel	spisovatel	k1gMnSc1	spisovatel
a	a	k8xC	a
scenárista	scenárista	k1gMnSc1	scenárista
<g/>
,	,	kIx,	,
bývalý	bývalý	k2eAgMnSc1d1	bývalý
člen	člen	k1gMnSc1	člen
Divadla	divadlo	k1gNnSc2	divadlo
Járy	Jára	k1gMnSc2	Jára
Cimrmana	Cimrman	k1gMnSc2	Cimrman
Jan	Jan	k1gMnSc1	Jan
Šrámek	Šrámek	k1gMnSc1	Šrámek
(	(	kIx(	(
<g/>
1870	[number]	k4	1870
<g/>
–	–	k?	–
<g/>
1956	[number]	k4	1956
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
politik	politik	k1gMnSc1	politik
a	a	k8xC	a
bývalý	bývalý	k2eAgMnSc1d1	bývalý
místopředseda	místopředseda	k1gMnSc1	místopředseda
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
odbojář	odbojář	k1gMnSc1	odbojář
<g/>
,	,	kIx,	,
čestný	čestný	k2eAgMnSc1d1	čestný
občan	občan	k1gMnSc1	občan
města	město	k1gNnSc2	město
Trutnova	Trutnov	k1gInSc2	Trutnov
(	(	kIx(	(
<g/>
1946	[number]	k4	1946
<g/>
)	)	kIx)	)
Otto	Otto	k1gMnSc1	Otto
Štemberka	Štemberka	k1gFnSc1	Štemberka
<g/>
,	,	kIx,	,
patriot	patriot	k1gMnSc1	patriot
<g/>
,	,	kIx,	,
spoluzakladatel	spoluzakladatel	k1gMnSc1	spoluzakladatel
sdružení	sdružení	k1gNnSc2	sdružení
Trutnov	Trutnov	k1gInSc1	Trutnov
–	–	k?	–
město	město	k1gNnSc1	město
draka	drak	k1gMnSc2	drak
<g/>
,	,	kIx,	,
vyznamenán	vyznamenán	k2eAgInSc1d1	vyznamenán
Kulturní	kulturní	k2eAgFnSc7d1	kulturní
cenou	cena	k1gFnSc7	cena
města	město	k1gNnSc2	město
Trutnova	Trutnov	k1gInSc2	Trutnov
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2012	[number]	k4	2012
Martin	Martin	k1gMnSc1	Martin
Štěpánek	Štěpánek	k1gMnSc1	Štěpánek
(	(	kIx(	(
<g/>
*	*	kIx~	*
1977	[number]	k4	1977
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sportovní	sportovní	k2eAgMnSc1d1	sportovní
potápěč	potápěč	k1gMnSc1	potápěč
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Trnka	Trnka	k1gMnSc1	Trnka
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
*	*	kIx~	*
1925	[number]	k4	1925
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pedagog	pedagog	k1gMnSc1	pedagog
a	a	k8xC	a
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Kulturní	kulturní	k2eAgFnSc2d1	kulturní
ceny	cena	k1gFnSc2	cena
města	město	k1gNnSc2	město
Trutnova	Trutnov	k1gInSc2	Trutnov
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2013	[number]	k4	2013
Miloš	Miloš	k1gMnSc1	Miloš
Trýzna	Trýzna	k1gFnSc1	Trýzna
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
a	a	k8xC	a
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Kulturní	kulturní	k2eAgFnSc2d1	kulturní
ceny	cena	k1gFnSc2	cena
města	město	k1gNnSc2	město
Trutnova	Trutnov	k1gInSc2	Trutnov
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2000	[number]	k4	2000
Jan	Jan	k1gMnSc1	Jan
Tuna	tuna	k1gFnSc1	tuna
(	(	kIx(	(
<g/>
*	*	kIx~	*
1973	[number]	k4	1973
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
herec	herec	k1gMnSc1	herec
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Tuzar	Tuzar	k1gMnSc1	Tuzar
(	(	kIx(	(
<g/>
1908	[number]	k4	1908
<g/>
–	–	k?	–
<g/>
1988	[number]	k4	1988
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kameraman	kameraman	k1gMnSc1	kameraman
Naďa	Naďa	k1gFnSc1	Naďa
Urbánková	Urbánková	k1gFnSc1	Urbánková
(	(	kIx(	(
<g/>
*	*	kIx~	*
1939	[number]	k4	1939
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zpěvačka	zpěvačka	k1gFnSc1	zpěvačka
<g/>
,	,	kIx,	,
absolventka	absolventka	k1gFnSc1	absolventka
Střední	střední	k2eAgFnSc2d1	střední
zdravotnické	zdravotnický	k2eAgFnSc2d1	zdravotnická
školy	škola	k1gFnSc2	škola
v	v	k7c6	v
Trutnově	Trutnov	k1gInSc6	Trutnov
Zdeněk	Zdeněk	k1gMnSc1	Zdeněk
Vašíček	Vašíček	k1gMnSc1	Vašíček
(	(	kIx(	(
<g/>
1933	[number]	k4	1933
<g/>
–	–	k?	–
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
filozof	filozof	k1gMnSc1	filozof
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
muzeolog	muzeolog	k1gMnSc1	muzeolog
<g/>
,	,	kIx,	,
ředitel	ředitel	k1gMnSc1	ředitel
trutnovského	trutnovský	k2eAgNnSc2d1	trutnovské
Muzea	muzeum	k1gNnSc2	muzeum
Podkrkonoší	Podkrkonoší	k1gNnSc2	Podkrkonoší
(	(	kIx(	(
<g/>
1960	[number]	k4	1960
<g/>
–	–	k?	–
<g/>
1970	[number]	k4	1970
<g/>
)	)	kIx)	)
Josef	Josef	k1gMnSc1	Josef
Vik	Vik	k1gMnSc1	Vik
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Kulturní	kulturní	k2eAgFnSc2d1	kulturní
ceny	cena	k1gFnSc2	cena
města	město	k1gNnSc2	město
Trutnova	Trutnov	k1gInSc2	Trutnov
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2007	[number]	k4	2007
Miroslav	Miroslav	k1gMnSc1	Miroslav
Vobořil	Vobořil	k1gMnSc1	Vobořil
(	(	kIx(	(
<g/>
*	*	kIx~	*
1966	[number]	k4	1966
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
filmové	filmový	k2eAgFnSc2d1	filmová
a	a	k8xC	a
scénické	scénický	k2eAgFnSc2d1	scénická
hudby	hudba	k1gFnSc2	hudba
Anton	Anton	k1gMnSc1	Anton
Antonovič	Antonovič	k1gMnSc1	Antonovič
Vojiščin	Vojiščin	k2eAgMnSc1d1	Vojiščin
<g/>
,	,	kIx,	,
voják	voják	k1gMnSc1	voják
Rudé	rudý	k2eAgFnSc2d1	rudá
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
čestný	čestný	k2eAgMnSc1d1	čestný
občan	občan	k1gMnSc1	občan
města	město	k1gNnSc2	město
Trutnova	Trutnov	k1gInSc2	Trutnov
(	(	kIx(	(
<g/>
1965	[number]	k4	1965
<g/>
)	)	kIx)	)
Vladimír	Vladimír	k1gMnSc1	Vladimír
Wolf	Wolf	k1gMnSc1	Wolf
<g/>
,	,	kIx,	,
historik	historik	k1gMnSc1	historik
a	a	k8xC	a
učitel	učitel	k1gMnSc1	učitel
<g/>
,	,	kIx,	,
nositel	nositel	k1gMnSc1	nositel
Kulturní	kulturní	k2eAgFnSc2d1	kulturní
ceny	cena	k1gFnSc2	cena
města	město	k1gNnSc2	město
Trutnova	Trutnov	k1gInSc2	Trutnov
za	za	k7c4	za
rok	rok	k1gInSc4	rok
2011	[number]	k4	2011
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Seznam	seznam	k1gInSc1	seznam
kulturních	kulturní	k2eAgFnPc2d1	kulturní
památek	památka	k1gFnPc2	památka
v	v	k7c6	v
Trutnově	Trutnov	k1gInSc6	Trutnov
<g/>
.	.	kIx.	.
</s>
<s>
Dělostřelecká	dělostřelecký	k2eAgFnSc1d1	dělostřelecká
tvrz	tvrz	k1gFnSc1	tvrz
Stachelberg	Stachelberg	k1gMnSc1	Stachelberg
(	(	kIx(	(
<g/>
Babí	babí	k2eAgFnSc1d1	babí
<g/>
)	)	kIx)	)
Rozhledna	rozhledna	k1gFnSc1	rozhledna
Eliška	Eliška	k1gFnSc1	Eliška
na	na	k7c6	na
Stachelbergu	Stachelberg	k1gInSc6	Stachelberg
Pozdně	pozdně	k6eAd1	pozdně
barokní	barokní	k2eAgInSc1d1	barokní
kostel	kostel	k1gInSc1	kostel
Narození	narození	k1gNnSc2	narození
Panny	Panna	k1gFnSc2	Panna
Marie	Maria	k1gFnSc2	Maria
(	(	kIx(	(
<g/>
1792	[number]	k4	1792
<g/>
)	)	kIx)	)
Renesanční	renesanční	k2eAgInSc1d1	renesanční
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Václava	Václav	k1gMnSc2	Václav
(	(	kIx(	(
<g/>
Horní	horní	k2eAgNnSc1d1	horní
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
<g/>
)	)	kIx)	)
Novogotický	novogotický	k2eAgInSc1d1	novogotický
kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Petra	Petra	k1gFnSc1	Petra
a	a	k8xC	a
Pavla	Pavla	k1gFnSc1	Pavla
(	(	kIx(	(
<g/>
Poříčí	Poříčí	k1gNnSc1	Poříčí
<g/>
)	)	kIx)	)
Kostel	kostel	k1gInSc1	kostel
sv.	sv.	kA	sv.
Jana	Jan	k1gMnSc2	Jan
Nepomuckého	Nepomucký	k1gMnSc2	Nepomucký
(	(	kIx(	(
<g />
.	.	kIx.	.
</s>
<s>
<g/>
Libeč	Libeč	k1gInSc1	Libeč
<g/>
)	)	kIx)	)
Novogotický	novogotický	k2eAgInSc1d1	novogotický
evangelický	evangelický	k2eAgInSc1d1	evangelický
kostel	kostel	k1gInSc1	kostel
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
koncertní	koncertní	k2eAgFnSc1d1	koncertní
síň	síň	k1gFnSc1	síň
Bohuslava	Bohuslava	k1gFnSc1	Bohuslava
Martinů	Martinů	k1gFnSc1	Martinů
(	(	kIx(	(
<g/>
Městský	městský	k2eAgInSc1d1	městský
park	park	k1gInSc1	park
<g/>
)	)	kIx)	)
Kaple	kaple	k1gFnSc1	kaple
svatého	svatý	k2eAgMnSc2d1	svatý
Jana	Jan	k1gMnSc2	Jan
Křtitele	křtitel	k1gMnSc2	křtitel
(	(	kIx(	(
<g/>
zvané	zvaný	k2eAgNnSc1d1	zvané
též	též	k9	též
Jánská	jánský	k2eAgFnSc1d1	Jánská
kaple	kaple	k1gFnSc1	kaple
<g/>
)	)	kIx)	)
v	v	k7c6	v
městském	městský	k2eAgInSc6d1	městský
parku	park	k1gInSc6	park
Arboretum	arboretum	k1gNnSc4	arboretum
Naučná	naučný	k2eAgFnSc1d1	naučná
stezka	stezka	k1gFnSc1	stezka
Bitvy	bitva	k1gFnSc2	bitva
u	u	k7c2	u
Trutnova	Trutnov	k1gInSc2	Trutnov
(	(	kIx(	(
<g/>
Městský	městský	k2eAgInSc1d1	městský
park	park	k1gInSc1	park
a	a	k8xC	a
okolí	okolí	k1gNnSc1	okolí
města	město	k1gNnSc2	město
<g/>
)	)	kIx)	)
<g />
.	.	kIx.	.
</s>
<s>
Muzeum	muzeum	k1gNnSc1	muzeum
Podkrkonoší	Podkrkonoší	k1gNnSc2	Podkrkonoší
<g/>
,	,	kIx,	,
původně	původně	k6eAd1	původně
hrad	hrad	k1gInSc1	hrad
(	(	kIx(	(
<g/>
centrum	centrum	k1gNnSc1	centrum
<g/>
)	)	kIx)	)
Původně	původně	k6eAd1	původně
renesanční	renesanční	k2eAgFnSc1d1	renesanční
radnice	radnice	k1gFnSc1	radnice
z	z	k7c2	z
konce	konec	k1gInSc2	konec
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
po	po	k7c6	po
požáru	požár	k1gInSc6	požár
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1861	[number]	k4	1861
novogoticky	novogoticky	k6eAd1	novogoticky
přestavěna	přestavět	k5eAaPmNgFnS	přestavět
(	(	kIx(	(
<g/>
centrum	centrum	k1gNnSc1	centrum
<g/>
)	)	kIx)	)
Novorenesanční	novorenesanční	k2eAgFnSc1d1	novorenesanční
budova	budova	k1gFnSc1	budova
Haasova	Haasův	k2eAgInSc2d1	Haasův
paláce	palác	k1gInSc2	palác
z	z	k7c2	z
80	[number]	k4	80
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
(	(	kIx(	(
<g/>
centrum	centrum	k1gNnSc1	centrum
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
Komplex	komplex	k1gInSc1	komplex
budov	budova	k1gFnPc2	budova
hlavního	hlavní	k2eAgNnSc2d1	hlavní
vlakového	vlakový	k2eAgNnSc2d1	vlakové
nádraží	nádraží	k1gNnSc2	nádraží
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1870	[number]	k4	1870
s	s	k7c7	s
blízkým	blízký	k2eAgInSc7d1	blízký
kamenným	kamenný	k2eAgInSc7d1	kamenný
mostem	most	k1gInSc7	most
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1884	[number]	k4	1884
(	(	kIx(	(
<g/>
centrum	centrum	k1gNnSc1	centrum
<g/>
)	)	kIx)	)
Renesanční	renesanční	k2eAgFnSc1d1	renesanční
budova	budova	k1gFnSc1	budova
dnešní	dnešní	k2eAgFnSc1d1	dnešní
"	"	kIx"	"
<g/>
Galerie	galerie	k1gFnSc1	galerie
Města	město	k1gNnSc2	město
Trutnova	Trutnov	k1gInSc2	Trutnov
<g/>
"	"	kIx"	"
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1599	[number]	k4	1599
(	(	kIx(	(
<g/>
centrum	centrum	k1gNnSc1	centrum
<g/>
)	)	kIx)	)
Krakonošova	Krakonošův	k2eAgFnSc1d1	Krakonošova
kašna	kašna	k1gFnSc1	kašna
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1892	[number]	k4	1892
se	s	k7c7	s
sochou	socha	k1gFnSc7	socha
Krakonoše	Krakonoš	k1gMnSc2	Krakonoš
(	(	kIx(	(
<g/>
centrum	centrum	k1gNnSc1	centrum
<g/>
)	)	kIx)	)
Národní	národní	k2eAgInSc1d1	národní
dům	dům	k1gInSc1	dům
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1900	[number]	k4	1900
(	(	kIx(	(
<g/>
Česká	český	k2eAgFnSc1d1	Česká
čtvrť	čtvrť	k1gFnSc1	čtvrť
<g/>
)	)	kIx)	)
Areál	areál	k1gInSc1	areál
českých	český	k2eAgFnPc2d1	Česká
škol	škola	k1gFnPc2	škola
na	na	k7c6	na
Středním	střední	k2eAgNnSc6d1	střední
Předměstí	předměstí	k1gNnSc6	předměstí
z	z	k7c2	z
20	[number]	k4	20
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
od	od	k7c2	od
arch	archa	k1gFnPc2	archa
<g/>
.	.	kIx.	.
</s>
<s>
Mezery	mezera	k1gFnPc1	mezera
(	(	kIx(	(
<g/>
Česká	český	k2eAgFnSc1d1	Česká
čtvrť	čtvrť	k1gFnSc1	čtvrť
<g/>
)	)	kIx)	)
Budova	budova	k1gFnSc1	budova
městského	městský	k2eAgNnSc2d1	Městské
kina	kino	k1gNnSc2	kino
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1929	[number]	k4	1929
od	od	k7c2	od
arch	archa	k1gFnPc2	archa
<g/>
.	.	kIx.	.
</s>
<s>
Brože	brož	k1gFnPc1	brož
(	(	kIx(	(
<g/>
centrum	centrum	k1gNnSc1	centrum
<g/>
)	)	kIx)	)
Bloky	blok	k1gInPc1	blok
secesních	secesní	k2eAgInPc2d1	secesní
domů	dům	k1gInPc2	dům
v	v	k7c6	v
prostoru	prostor	k1gInSc6	prostor
"	"	kIx"	"
<g/>
Na	na	k7c6	na
Záduší	záduší	k1gNnSc6	záduší
<g/>
"	"	kIx"	"
a	a	k8xC	a
dnešního	dnešní	k2eAgMnSc2d1	dnešní
Jiráskova	Jiráskův	k2eAgMnSc2d1	Jiráskův
nám.	nám.	k?	nám.
Česká	český	k2eAgFnSc1d1	Česká
čtvrť	čtvrť	k1gFnSc1	čtvrť
<g/>
)	)	kIx)	)
Sídliště	sídliště	k1gNnSc1	sídliště
"	"	kIx"	"
<g/>
Křižík	Křižík	k1gMnSc1	Křižík
<g/>
"	"	kIx"	"
z	z	k7c2	z
50	[number]	k4	50
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
postavené	postavený	k2eAgNnSc1d1	postavené
v	v	k7c6	v
duchu	duch	k1gMnSc6	duch
"	"	kIx"	"
<g/>
socialistického	socialistický	k2eAgInSc2d1	socialistický
realismu	realismus	k1gInSc2	realismus
<g/>
"	"	kIx"	"
Rozlehlé	rozlehlý	k2eAgInPc1d1	rozlehlý
<g />
.	.	kIx.	.
</s>
<s>
městské	městský	k2eAgFnPc1d1	městská
sady	sada	k1gFnPc1	sada
založené	založený	k2eAgFnPc1d1	založená
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Synagoga	synagoga	k1gFnSc1	synagoga
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1885	[number]	k4	1885
vypálena	vypálen	k2eAgFnSc1d1	vypálena
v	v	k7c6	v
listopadu	listopad	k1gInSc6	listopad
1938	[number]	k4	1938
místními	místní	k2eAgMnPc7d1	místní
nacisty	nacista	k1gMnPc7	nacista
při	při	k7c6	při
tzv.	tzv.	kA	tzv.
"	"	kIx"	"
<g/>
křišťálové	křišťálový	k2eAgFnSc2d1	Křišťálová
noci	noc	k1gFnSc2	noc
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Městský	městský	k2eAgInSc1d1	městský
park	park	k1gInSc1	park
<g/>
)	)	kIx)	)
Trutnov	Trutnov	k1gInSc1	Trutnov
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgNnSc7	jeden
z	z	k7c2	z
turistických	turistický	k2eAgNnPc2d1	turistické
center	centrum	k1gNnPc2	centrum
Královéhradeckého	královéhradecký	k2eAgInSc2d1	královéhradecký
kraje	kraj	k1gInSc2	kraj
a	a	k8xC	a
navštěvují	navštěvovat	k5eAaImIp3nP	navštěvovat
ho	on	k3xPp3gInSc4	on
nejen	nejen	k6eAd1	nejen
čeští	český	k2eAgMnPc1d1	český
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
zahraniční	zahraniční	k2eAgMnPc1d1	zahraniční
turisté	turist	k1gMnPc1	turist
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
Němci	Němec	k1gMnPc1	Němec
a	a	k8xC	a
Poláci	Polák	k1gMnPc1	Polák
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
jeho	jeho	k3xOp3gFnSc4	jeho
výhodnou	výhodný	k2eAgFnSc4d1	výhodná
polohu	poloha	k1gFnSc4	poloha
je	být	k5eAaImIp3nS	být
často	často	k6eAd1	často
vyhledáván	vyhledáván	k2eAgInSc1d1	vyhledáván
jako	jako	k8xS	jako
základna	základna	k1gFnSc1	základna
pro	pro	k7c4	pro
podnikání	podnikání	k1gNnSc4	podnikání
výletů	výlet	k1gInPc2	výlet
do	do	k7c2	do
okolí	okolí	k1gNnSc2	okolí
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
též	též	k9	též
důležitým	důležitý	k2eAgInSc7d1	důležitý
uzlem	uzel	k1gInSc7	uzel
turistů	turist	k1gMnPc2	turist
pokračujícím	pokračující	k2eAgFnPc3d1	pokračující
za	za	k7c7	za
zimními	zimní	k2eAgInPc7d1	zimní
sporty	sport	k1gInPc7	sport
<g/>
.	.	kIx.	.
</s>
<s>
Nedaleko	daleko	k6eNd1	daleko
od	od	k7c2	od
něj	on	k3xPp3gNnSc2	on
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
nachází	nacházet	k5eAaImIp3nS	nacházet
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
pohoří	pohoří	k1gNnSc1	pohoří
Krkonoše	Krkonoše	k1gFnPc1	Krkonoše
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
díky	díky	k7c3	díky
velkému	velký	k2eAgNnSc3d1	velké
množství	množství	k1gNnSc3	množství
turistů	turist	k1gMnPc2	turist
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
městě	město	k1gNnSc6	město
několik	několik	k4yIc4	několik
hotelů	hotel	k1gInPc2	hotel
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
stojí	stát	k5eAaImIp3nS	stát
za	za	k7c4	za
připomenutí	připomenutí	k1gNnSc4	připomenutí
Penzion	penzion	k1gInSc4	penzion
Pohoda	pohoda	k1gFnSc1	pohoda
na	na	k7c6	na
pěší	pěší	k2eAgFnSc6d1	pěší
zóně	zóna	k1gFnSc6	zóna
pod	pod	k7c7	pod
náměstím	náměstí	k1gNnSc7	náměstí
<g/>
,	,	kIx,	,
hotel	hotel	k1gInSc1	hotel
Patria	Patrium	k1gNnSc2	Patrium
na	na	k7c6	na
Dolním	dolní	k2eAgNnSc6d1	dolní
předměstí	předměstí	k1gNnSc6	předměstí
<g/>
,	,	kIx,	,
hotel	hotel	k1gInSc1	hotel
Adam	Adam	k1gMnSc1	Adam
na	na	k7c6	na
náměstí	náměstí	k1gNnSc6	náměstí
<g/>
,	,	kIx,	,
či	či	k8xC	či
hotel	hotel	k1gInSc4	hotel
Krakonoš	Krakonoš	k1gMnSc1	Krakonoš
u	u	k7c2	u
Trutnovského	trutnovský	k2eAgInSc2d1	trutnovský
pivovaru	pivovar	k1gInSc2	pivovar
<g/>
.	.	kIx.	.
</s>
<s>
Trutnov	Trutnov	k1gInSc1	Trutnov
si	se	k3xPyFc3	se
spousta	spousta	k1gFnSc1	spousta
lidí	člověk	k1gMnPc2	člověk
plete	plést	k5eAaImIp3nS	plést
s	s	k7c7	s
Turnovem	Turnov	k1gInSc7	Turnov
a	a	k8xC	a
denně	denně	k6eAd1	denně
dochází	docházet	k5eAaImIp3nS	docházet
k	k	k7c3	k
desítkám	desítka	k1gFnPc3	desítka
omylů	omyl	k1gInPc2	omyl
v	v	k7c6	v
poště	pošta	k1gFnSc6	pošta
a	a	k8xC	a
dopravě	doprava	k1gFnSc6	doprava
<g/>
.	.	kIx.	.
řeka	řeka	k1gFnSc1	řeka
Úpa	Úpa	k1gFnSc1	Úpa
protéká	protékat	k5eAaImIp3nS	protékat
městem	město	k1gNnSc7	město
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
16	[number]	k4	16
km	km	kA	km
a	a	k8xC	a
její	její	k3xOp3gNnSc1	její
převýšení	převýšení	k1gNnSc1	převýšení
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
činí	činit	k5eAaImIp3nS	činit
100	[number]	k4	100
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
přes	přes	k7c4	přes
řeku	řeka	k1gFnSc4	řeka
Úpu	Úpa	k1gFnSc4	Úpa
vede	vést	k5eAaImIp3nS	vést
19	[number]	k4	19
mostů	most	k1gInPc2	most
<g/>
,	,	kIx,	,
4	[number]	k4	4
lávky	lávka	k1gFnSc2	lávka
a	a	k8xC	a
7	[number]	k4	7
<g />
.	.	kIx.	.
</s>
<s>
jezů	jez	k1gInPc2	jez
<g/>
.	.	kIx.	.
město	město	k1gNnSc1	město
má	mít	k5eAaImIp3nS	mít
přes	přes	k7c4	přes
600	[number]	k4	600
názvů	název	k1gInPc2	název
ulic	ulice	k1gFnPc2	ulice
<g/>
.	.	kIx.	.
nejvyšším	vysoký	k2eAgInSc7d3	Nejvyšší
vrcholem	vrchol	k1gInSc7	vrchol
na	na	k7c6	na
území	území	k1gNnSc6	území
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
hora	hora	k1gFnSc1	hora
Kámen	kámen	k1gInSc1	kámen
865	[number]	k4	865
m	m	kA	m
n.	n.	k?	n.
<g/>
m.	m.	k?	m.
v	v	k7c6	v
KRNAP	KRNAP	kA	KRNAP
<g/>
.	.	kIx.	.
nejvyšší	vysoký	k2eAgNnSc1d3	nejvyšší
místo	místo	k1gNnSc1	místo
ve	v	k7c6	v
městě	město	k1gNnSc6	město
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
968	[number]	k4	968
metrů	metr	k1gInPc2	metr
na	na	k7c6	na
svahu	svah	k1gInSc6	svah
Dvorského	Dvorského	k2eAgInSc2d1	Dvorského
lesa	les	k1gInSc2	les
(	(	kIx(	(
<g/>
1033	[number]	k4	1033
<g/>
)	)	kIx)	)
v	v	k7c6	v
Krkonoších	Krkonoše	k1gFnPc6	Krkonoše
<g/>
<g />
.	.	kIx.	.
</s>
<s>
nejnižší	nízký	k2eAgNnSc4d3	nejnižší
místo	místo	k1gNnSc4	místo
ve	v	k7c6	v
městě	město	k1gNnSc6	město
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
nadmořské	nadmořský	k2eAgFnSc6d1	nadmořská
výšce	výška	k1gFnSc6	výška
350	[number]	k4	350
metrů	metr	k1gInPc2	metr
v	v	k7c6	v
korytě	koryto	k1gNnSc6	koryto
řeky	řeka	k1gFnSc2	řeka
Úpy	Úpa	k1gFnSc2	Úpa
pod	pod	k7c7	pod
Adamovem	Adamov	k1gInSc7	Adamov
<g/>
.	.	kIx.	.
nejobydlenější	obydlený	k2eAgFnSc7d3	nejobydlenější
částí	část	k1gFnSc7	část
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
Horní	horní	k2eAgNnSc1d1	horní
Staré	Staré	k2eAgNnSc1d1	Staré
Město	město	k1gNnSc1	město
s	s	k7c7	s
9.000	[number]	k4	9.000
obyvateli	obyvatel	k1gMnPc7	obyvatel
(	(	kIx(	(
<g/>
téměř	téměř	k6eAd1	téměř
třetina	třetina	k1gFnSc1	třetina
města	město	k1gNnSc2	město
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
nejméně	málo	k6eAd3	málo
obyvatel	obyvatel	k1gMnSc1	obyvatel
má	mít	k5eAaImIp3nS	mít
naopak	naopak	k6eAd1	naopak
Nový	nový	k2eAgInSc1d1	nový
Rokytník	Rokytník	k1gInSc1	Rokytník
s	s	k7c7	s
pouhými	pouhý	k2eAgFnPc7d1	pouhá
44	[number]	k4	44
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
<g />
.	.	kIx.	.
</s>
<s>
kvůli	kvůli	k7c3	kvůli
velké	velký	k2eAgFnSc3d1	velká
rozloze	rozloha	k1gFnSc3	rozloha
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
hustota	hustota	k1gFnSc1	hustota
obyvatel	obyvatel	k1gMnPc2	obyvatel
jen	jen	k6eAd1	jen
308	[number]	k4	308
osob	osoba	k1gFnPc2	osoba
na	na	k7c4	na
km	km	kA	km
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
nejvíce	hodně	k6eAd3	hodně
domů	dům	k1gInPc2	dům
stojí	stát	k5eAaImIp3nS	stát
na	na	k7c6	na
Středním	střední	k2eAgNnSc6d1	střední
Předměstí	předměstí	k1gNnSc6	předměstí
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
přes	přes	k7c4	přes
1000	[number]	k4	1000
č.	č.	k?	č.
p.	p.	k?	p.
nejrozsáhlejší	rozsáhlý	k2eAgFnSc7d3	nejrozsáhlejší
částí	část	k1gFnSc7	část
města	město	k1gNnSc2	město
je	být	k5eAaImIp3nS	být
Starý	starý	k2eAgInSc1d1	starý
Rokytník	Rokytník	k1gInSc1	Rokytník
s	s	k7c7	s
rozlohou	rozloha	k1gFnSc7	rozloha
1	[number]	k4	1
477.82	[number]	k4	477.82
ha	ha	kA	ha
avšak	avšak	k8xC	avšak
jen	jen	k9	jen
s	s	k7c7	s
82	[number]	k4	82
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
nejmenší	malý	k2eAgInSc4d3	nejmenší
katastr	katastr	k1gInSc4	katastr
ve	v	k7c6	v
městě	město	k1gNnSc6	město
188.02	[number]	k4	188.02
ha	ha	kA	ha
zabírá	zabírat	k5eAaImIp3nS	zabírat
Oblanov	Oblanov	k1gInSc1	Oblanov
se	s	k7c7	s
60	[number]	k4	60
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
,	,	kIx,	,
ještě	ještě	k9	ještě
menší	malý	k2eAgFnSc1d2	menší
je	být	k5eAaImIp3nS	být
však	však	k9	však
historické	historický	k2eAgNnSc1d1	historické
Vnitřní	vnitřní	k2eAgNnSc1d1	vnitřní
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
608	[number]	k4	608
obyvateli	obyvatel	k1gMnPc7	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Trutnov	Trutnov	k1gInSc1	Trutnov
přímo	přímo	k6eAd1	přímo
sousedí	sousedit	k5eAaImIp3nS	sousedit
s	s	k7c7	s
3	[number]	k4	3
městy	město	k1gNnPc7	město
<g/>
,	,	kIx,	,
1	[number]	k4	1
městysem	městys	k1gInSc7	městys
<g/>
,	,	kIx,	,
9	[number]	k4	9
obcemi	obec	k1gFnPc7	obec
a	a	k8xC	a
jednou	jeden	k4xCgFnSc7	jeden
obcí	obec	k1gFnSc7	obec
v	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
<g/>
.	.	kIx.	.
průměrná	průměrný	k2eAgFnSc1d1	průměrná
roční	roční	k2eAgFnSc1d1	roční
teplota	teplota	k1gFnSc1	teplota
v	v	k7c6	v
Trutnově	Trutnov	k1gInSc6	Trutnov
je	být	k5eAaImIp3nS	být
pouhých	pouhý	k2eAgNnPc2d1	pouhé
6,8	[number]	k4	6,8
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
podhorských	podhorský	k2eAgFnPc6d1	podhorská
částech	část	k1gFnPc6	část
je	být	k5eAaImIp3nS	být
ještě	ještě	k6eAd1	ještě
nižší	nízký	k2eAgFnSc1d2	nižší
<g/>
.	.	kIx.	.
</s>
<s>
Sport	sport	k1gInSc1	sport
v	v	k7c6	v
Trutnově	Trutnov	k1gInSc6	Trutnov
má	mít	k5eAaImIp3nS	mít
velkou	velký	k2eAgFnSc4d1	velká
tradici	tradice	k1gFnSc4	tradice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
hrají	hrát	k5eAaImIp3nP	hrát
tyto	tento	k3xDgFnPc1	tento
seniorské	seniorský	k2eAgFnPc1d1	seniorská
soutěže	soutěž	k1gFnPc1	soutěž
<g/>
:	:	kIx,	:
Basketbal	basketbal	k1gInSc1	basketbal
BK	BK	kA	BK
Kara	kara	k1gFnSc1	kara
Trutnov	Trutnov	k1gInSc1	Trutnov
(	(	kIx(	(
<g/>
ženy	žena	k1gFnPc1	žena
<g/>
)	)	kIx)	)
BK	BK	kA	BK
Kara	kara	k1gFnSc1	kara
Trutnov	Trutnov	k1gInSc1	Trutnov
(	(	kIx(	(
<g/>
muži	muž	k1gMnPc1	muž
<g/>
)	)	kIx)	)
Fotbal	fotbal	k1gInSc1	fotbal
MFK	MFK	kA	MFK
Trutnov	Trutnov	k1gInSc1	Trutnov
(	(	kIx(	(
<g/>
Divize	divize	k1gFnSc1	divize
C	C	kA	C
<g/>
)	)	kIx)	)
Hokej	hokej	k1gInSc1	hokej
HC	HC	kA	HC
Trutnov	Trutnov	k1gInSc1	Trutnov
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
národní	národní	k2eAgFnSc1d1	národní
hokejová	hokejový	k2eAgFnSc1d1	hokejová
liga	liga	k1gFnSc1	liga
<g/>
)	)	kIx)	)
Atletika	atletika	k1gFnSc1	atletika
<g />
.	.	kIx.	.
</s>
<s>
Loko	Loko	k6eAd1	Loko
Trutnov	Trutnov	k1gInSc1	Trutnov
Tenis	tenis	k1gInSc1	tenis
Loko	Loko	k1gNnSc1	Loko
Trutnov	Trutnov	k1gInSc1	Trutnov
(	(	kIx(	(
<g/>
Východočeský	východočeský	k2eAgInSc1d1	východočeský
přebor	přebor	k1gInSc1	přebor
<g/>
)	)	kIx)	)
Volejbal	volejbal	k1gInSc1	volejbal
Loko	Loko	k1gNnSc1	Loko
Trutnov	Trutnov	k1gInSc1	Trutnov
-	-	kIx~	-
Krajská	krajský	k2eAgFnSc1d1	krajská
soutěž	soutěž	k1gFnSc1	soutěž
žen	žena	k1gFnPc2	žena
<g/>
,	,	kIx,	,
1	[number]	k4	1
<g/>
.	.	kIx.	.
<g/>
třída	třída	k1gFnSc1	třída
Americký	americký	k2eAgInSc1d1	americký
fotbal	fotbal	k1gInSc1	fotbal
Trutnov	Trutnov	k1gInSc1	Trutnov
Rangers	Rangers	k1gInSc1	Rangers
-	-	kIx~	-
ČLAF	ČLAF	kA	ČLAF
B	B	kA	B
Kępno	Kępno	k1gNnSc4	Kępno
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc4	Polsko
Kamienna	Kamienn	k1gMnSc2	Kamienn
Góra	Górus	k1gMnSc2	Górus
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
Lohfelden	Lohfeldna	k1gFnPc2	Lohfeldna
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
Würzburg	Würzburg	k1gInSc1	Würzburg
<g/>
,	,	kIx,	,
Německo	Německo	k1gNnSc1	Německo
Świdnica	Świdnic	k1gInSc2	Świdnic
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
Strzelin	Strzelina	k1gFnPc2	Strzelina
<g/>
,	,	kIx,	,
Polsko	Polsko	k1gNnSc1	Polsko
Senica	Senic	k1gInSc2	Senic
<g/>
,	,	kIx,	,
Slovensko	Slovensko	k1gNnSc4	Slovensko
Uffo	Uffo	k1gNnSc1	Uffo
-	-	kIx~	-
Společenské	společenský	k2eAgNnSc1d1	společenské
centrum	centrum	k1gNnSc1	centrum
Trutnov	Trutnov	k1gInSc4	Trutnov
Galerie	galerie	k1gFnSc2	galerie
Trutnov	Trutnov	k1gInSc4	Trutnov
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commons	k1gInSc4	Commons
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Trutnov	Trutnov	k1gInSc1	Trutnov
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
Trutnov	Trutnov	k1gInSc1	Trutnov
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Oficiální	oficiální	k2eAgFnSc2d1	oficiální
stránky	stránka	k1gFnSc2	stránka
města	město	k1gNnSc2	město
Trutnov	Trutnov	k1gInSc1	Trutnov
Klub	klub	k1gInSc1	klub
vojenské	vojenský	k2eAgFnSc2d1	vojenská
historie	historie	k1gFnSc2	historie
Trutnov	Trutnov	k1gInSc4	Trutnov
Webová	webový	k2eAgFnSc1d1	webová
kamera	kamera	k1gFnSc1	kamera
(	(	kIx(	(
<g/>
<g />
.	.	kIx.	.
</s>
<s>
Krakonošovo	Krakonošův	k2eAgNnSc1d1	Krakonošovo
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
)	)	kIx)	)
Webová	webový	k2eAgFnSc1d1	webová
kamera	kamera	k1gFnSc1	kamera
(	(	kIx(	(
<g/>
Pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
Trutnov	Trutnov	k1gInSc4	Trutnov
z	z	k7c2	z
vrchu	vrch	k1gInSc2	vrch
Šibeník	šibeník	k1gMnSc1	šibeník
-	-	kIx~	-
SV	sv	kA	sv
směr	směr	k1gInSc1	směr
<g/>
)	)	kIx)	)
Webová	webový	k2eAgFnSc1d1	webová
kamera	kamera	k1gFnSc1	kamera
(	(	kIx(	(
<g/>
Pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
Trutnov	Trutnov	k1gInSc4	Trutnov
z	z	k7c2	z
vrchu	vrch	k1gInSc2	vrch
Šibeník	šibeník	k1gMnSc1	šibeník
-	-	kIx~	-
SZ	SZ	kA	SZ
směr	směr	k1gInSc1	směr
<g/>
)	)	kIx)	)
Webová	webový	k2eAgFnSc1d1	webová
kamera	kamera	k1gFnSc1	kamera
(	(	kIx(	(
<g/>
Panorámatický	Panorámatický	k2eAgInSc1d1	Panorámatický
pohled	pohled	k1gInSc1	pohled
na	na	k7c4	na
město	město	k1gNnSc4	město
<g/>
)	)	kIx)	)
Webová	webový	k2eAgFnSc1d1	webová
kamera	kamera	k1gFnSc1	kamera
(	(	kIx(	(
<g/>
Slovanské	slovanský	k2eAgNnSc1d1	slovanské
náměstí	náměstí	k1gNnSc1	náměstí
<g/>
)	)	kIx)	)
Meteorologická	meteorologický	k2eAgFnSc1d1	meteorologická
stanice	stanice	k1gFnSc1	stanice
Trutnov	Trutnov	k1gInSc1	Trutnov
Trutnov	Trutnov	k1gInSc1	Trutnov
-	-	kIx~	-
město	město	k1gNnSc1	město
draka	drak	k1gMnSc2	drak
Hasiči	hasič	k1gMnSc3	hasič
HSM	HSM	kA	HSM
Klub	klub	k1gInSc1	klub
filatelistů	filatelista	k1gMnPc2	filatelista
Trutnov	Trutnov	k1gInSc1	Trutnov
</s>
