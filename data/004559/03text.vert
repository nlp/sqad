<s>
Converse	Converse	k1gFnSc1	Converse
(	(	kIx(	(
<g/>
vysl	vysl	k1gInSc1	vysl
<g/>
.	.	kIx.	.
konvers	konvers	k1gInSc1	konvers
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
americká	americký	k2eAgFnSc1d1	americká
společnost	společnost	k1gFnSc1	společnost
vyrábějící	vyrábějící	k2eAgFnSc1d1	vyrábějící
především	především	k6eAd1	především
sportovní	sportovní	k2eAgFnSc1d1	sportovní
obuv	obuv	k1gFnSc1	obuv
a	a	k8xC	a
oblečení	oblečení	k1gNnSc1	oblečení
<g/>
.	.	kIx.	.
</s>
<s>
Boty	bota	k1gFnPc1	bota
Converse	Converse	k1gFnSc2	Converse
nesou	nést	k5eAaImIp3nP	nést
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
logo	logo	k1gNnSc4	logo
ve	v	k7c6	v
tvaru	tvar	k1gInSc6	tvar
pěticípé	pěticípý	k2eAgFnSc2d1	pěticípá
hvězdy	hvězda	k1gFnSc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
prodává	prodávat	k5eAaImIp3nS	prodávat
své	svůj	k3xOyFgNnSc4	svůj
zboží	zboží	k1gNnSc4	zboží
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
prodejců	prodejce	k1gMnPc2	prodejce
ve	v	k7c6	v
více	hodně	k6eAd2	hodně
než	než	k8xS	než
160	[number]	k4	160
zemích	zem	k1gFnPc6	zem
světa	svět	k1gInSc2	svět
<g/>
.	.	kIx.	.
</s>
<s>
Firma	firma	k1gFnSc1	firma
byla	být	k5eAaImAgFnS	být
založena	založen	k2eAgFnSc1d1	založena
roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
2003	[number]	k4	2003
byla	být	k5eAaImAgFnS	být
koupena	koupit	k5eAaPmNgFnS	koupit
firmou	firma	k1gFnSc7	firma
Nike	Nike	k1gFnSc2	Nike
za	za	k7c4	za
305	[number]	k4	305
mil	míle	k1gFnPc2	míle
<g/>
.	.	kIx.	.
amerických	americký	k2eAgInPc2d1	americký
dolarů	dolar	k1gInPc2	dolar
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
její	její	k3xOp3gFnSc7	její
dceřinou	dceřin	k2eAgFnSc7d1	dceřina
firmou	firma	k1gFnSc7	firma
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1908	[number]	k4	1908
Marquis	Marquis	k1gInSc1	Marquis
Mills	Mills	k1gInSc4	Mills
Converse	Converse	k1gFnSc2	Converse
založil	založit	k5eAaPmAgMnS	založit
společnost	společnost	k1gFnSc4	společnost
Converse	Converse	k1gFnSc1	Converse
Rubber	Rubber	k1gMnSc1	Rubber
Corporation	Corporation	k1gInSc1	Corporation
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
se	se	k3xPyFc4	se
firma	firma	k1gFnSc1	firma
věnovala	věnovat	k5eAaPmAgFnS	věnovat
výrobě	výroba	k1gFnSc3	výroba
galoší	galoše	k1gFnPc2	galoše
a	a	k8xC	a
další	další	k2eAgFnSc2d1	další
pracovní	pracovní	k2eAgFnSc2d1	pracovní
gumové	gumový	k2eAgFnSc2d1	gumová
obuvi	obuv	k1gFnSc2	obuv
<g/>
.	.	kIx.	.
</s>
<s>
Poněvadž	poněvadž	k8xS	poněvadž
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
jen	jen	k9	jen
o	o	k7c4	o
sezónní	sezónní	k2eAgNnSc4d1	sezónní
zboží	zboží	k1gNnSc4	zboží
<g/>
,	,	kIx,	,
hledala	hledat	k5eAaImAgFnS	hledat
firma	firma	k1gFnSc1	firma
způsob	způsob	k1gInSc1	způsob
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
zajistit	zajistit	k5eAaPmF	zajistit
celoroční	celoroční	k2eAgInSc4d1	celoroční
odbyt	odbyt	k1gInSc4	odbyt
gumové	gumový	k2eAgFnSc2d1	gumová
obuvi	obuv	k1gFnSc2	obuv
<g/>
.	.	kIx.	.
</s>
<s>
Rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
se	se	k3xPyFc4	se
proto	proto	k8xC	proto
začít	začít	k5eAaPmF	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
sportovní	sportovní	k2eAgFnSc4d1	sportovní
obuv	obuv	k1gFnSc4	obuv
<g/>
,	,	kIx,	,
o	o	k7c4	o
kterou	který	k3yQgFnSc4	který
bude	být	k5eAaImBp3nS	být
zájem	zájem	k1gInSc1	zájem
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
rok	rok	k1gInSc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
v	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
začínal	začínat	k5eAaImAgMnS	začínat
být	být	k5eAaImF	být
v	v	k7c6	v
Americe	Amerika	k1gFnSc6	Amerika
velmi	velmi	k6eAd1	velmi
populární	populární	k2eAgInSc4d1	populární
basketbal	basketbal	k1gInSc4	basketbal
<g/>
,	,	kIx,	,
spatřila	spatřit	k5eAaPmAgFnS	spatřit
firma	firma	k1gFnSc1	firma
příležitost	příležitost	k1gFnSc4	příležitost
nabídnout	nabídnout	k5eAaPmF	nabídnout
hráčům	hráč	k1gMnPc3	hráč
speciální	speciální	k2eAgFnPc4d1	speciální
basketbalové	basketbalový	k2eAgFnPc4d1	basketbalová
boty	bota	k1gFnPc4	bota
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dlouhém	dlouhý	k2eAgInSc6d1	dlouhý
výzkumu	výzkum	k1gInSc6	výzkum
a	a	k8xC	a
vývoji	vývoj	k1gInSc3	vývoj
byla	být	k5eAaImAgFnS	být
roku	rok	k1gInSc2	rok
1917	[number]	k4	1917
vyrobena	vyroben	k2eAgFnSc1d1	vyrobena
první	první	k4xOgFnSc1	první
verze	verze	k1gFnSc1	verze
basketbalové	basketbalový	k2eAgFnSc2d1	basketbalová
boty	bota	k1gFnSc2	bota
All	All	k1gFnSc2	All
Star	Star	kA	Star
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnPc1	první
boty	bota	k1gFnPc1	bota
All	All	k1gFnPc1	All
Star	Star	kA	Star
byly	být	k5eAaImAgFnP	být
v	v	k7c6	v
přírodní	přírodní	k2eAgFnSc6d1	přírodní
hnědé	hnědý	k2eAgFnSc6d1	hnědá
barvě	barva	k1gFnSc6	barva
s	s	k7c7	s
černým	černý	k2eAgNnSc7d1	černé
lemováním	lemování	k1gNnSc7	lemování
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
20	[number]	k4	20
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
All	All	k1gFnSc2	All
Star	star	k1gFnPc2	star
vyráběly	vyrábět	k5eAaImAgFnP	vyrábět
v	v	k7c6	v
černé	černá	k1gFnSc6	černá
plátěné	plátěný	k2eAgFnSc6d1	plátěná
nebo	nebo	k8xC	nebo
kožené	kožený	k2eAgFnSc6d1	kožená
verzi	verze	k1gFnSc6	verze
<g/>
.	.	kIx.	.
</s>
<s>
Boty	bota	k1gFnPc1	bota
All	All	k1gFnSc2	All
Star	star	k1gFnSc2	star
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgInP	stát
první	první	k4xOgFnSc2	první
masově	masově	k6eAd1	masově
vyráběnou	vyráběný	k2eAgFnSc7d1	vyráběná
basketbalovou	basketbalový	k2eAgFnSc7d1	basketbalová
obuví	obuv	k1gFnSc7	obuv
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Americe	Amerika	k1gFnSc6	Amerika
<g/>
.	.	kIx.	.
</s>
<s>
Bota	bota	k1gFnSc1	bota
se	se	k3xPyFc4	se
skládala	skládat	k5eAaImAgFnS	skládat
z	z	k7c2	z
velmi	velmi	k6eAd1	velmi
tlusté	tlustý	k2eAgFnSc2d1	tlustá
gumové	gumový	k2eAgFnSc2d1	gumová
podrážky	podrážka	k1gFnSc2	podrážka
a	a	k8xC	a
plátěného	plátěný	k2eAgNnSc2d1	plátěné
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
koženého	kožený	k2eAgInSc2d1	kožený
svršku	svršek	k1gInSc2	svršek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
kryl	krýt	k5eAaImAgInS	krýt
kotník	kotník	k1gInSc4	kotník
<g/>
.	.	kIx.	.
</s>
<s>
Prodeje	prodej	k1gFnPc4	prodej
prvních	první	k4xOgFnPc2	první
bot	bota	k1gFnPc2	bota
All	All	k1gMnPc2	All
Star	Star	kA	Star
byly	být	k5eAaImAgInP	být
zpočátku	zpočátku	k6eAd1	zpočátku
velmi	velmi	k6eAd1	velmi
nízké	nízký	k2eAgNnSc1d1	nízké
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
změnilo	změnit	k5eAaPmAgNnS	změnit
roku	rok	k1gInSc2	rok
1921	[number]	k4	1921
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
do	do	k7c2	do
odbytového	odbytový	k2eAgNnSc2d1	odbytové
oddělení	oddělení	k1gNnSc2	oddělení
společnosti	společnost	k1gFnSc2	společnost
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
Charles	Charles	k1gMnSc1	Charles
H.	H.	kA	H.
Taylor	Taylor	k1gMnSc1	Taylor
<g/>
.	.	kIx.	.
</s>
<s>
Charles	Charles	k1gMnSc1	Charles
"	"	kIx"	"
<g/>
Chuck	Chuck	k1gMnSc1	Chuck
<g/>
"	"	kIx"	"
Taylor	Taylor	k1gMnSc1	Taylor
byl	být	k5eAaImAgMnS	být
dříve	dříve	k6eAd2	dříve
hráčem	hráč	k1gMnSc7	hráč
basketbalového	basketbalový	k2eAgInSc2d1	basketbalový
týmu	tým	k1gInSc2	tým
Akron	Akron	k1gMnSc1	Akron
Firestones	Firestones	k1gMnSc1	Firestones
<g/>
.	.	kIx.	.
</s>
<s>
Tenisky	teniska	k1gFnPc1	teniska
All	All	k1gFnSc2	All
Star	star	k1gFnSc2	star
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
zamlouvaly	zamlouvat	k5eAaImAgFnP	zamlouvat
a	a	k8xC	a
spatřoval	spatřovat	k5eAaImAgMnS	spatřovat
v	v	k7c6	v
nich	on	k3xPp3gInPc6	on
velký	velký	k2eAgInSc4d1	velký
potenciál	potenciál	k1gInSc4	potenciál
a	a	k8xC	a
přínos	přínos	k1gInSc4	přínos
pro	pro	k7c4	pro
hráče	hráč	k1gMnPc4	hráč
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
nastoupit	nastoupit	k5eAaPmF	nastoupit
k	k	k7c3	k
firmě	firma	k1gFnSc3	firma
Converse	Converse	k1gFnSc2	Converse
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
u	u	k7c2	u
firmy	firma	k1gFnSc2	firma
Chuck	Chuck	k1gMnSc1	Chuck
Taylor	Taylor	k1gMnSc1	Taylor
jezdil	jezdit	k5eAaImAgMnS	jezdit
po	po	k7c6	po
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
jako	jako	k9	jako
obchodní	obchodní	k2eAgMnPc1d1	obchodní
cestující	cestující	k1gMnPc1	cestující
a	a	k8xC	a
propagoval	propagovat	k5eAaImAgMnS	propagovat
basketbalové	basketbalový	k2eAgFnPc4d1	basketbalová
boty	bota	k1gFnPc4	bota
All	All	k1gFnSc2	All
Star	Star	kA	Star
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
vlastního	vlastní	k2eAgInSc2d1	vlastní
prodeje	prodej	k1gInSc2	prodej
také	také	k6eAd1	také
využíval	využívat	k5eAaImAgInS	využívat
různé	různý	k2eAgInPc4d1	různý
chytré	chytrý	k2eAgInPc4d1	chytrý
marketingové	marketingový	k2eAgInPc4d1	marketingový
prostředky	prostředek	k1gInPc4	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
nejúspěšnějším	úspěšný	k2eAgMnSc7d3	nejúspěšnější
obchodníkem	obchodník	k1gMnSc7	obchodník
firmy	firma	k1gFnSc2	firma
Converse	Converse	k1gFnSc2	Converse
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc7	jeho
přičiněním	přičinění	k1gNnSc7	přičinění
výrazně	výrazně	k6eAd1	výrazně
vzrostly	vzrůst	k5eAaPmAgFnP	vzrůst
prodeje	prodej	k1gFnPc1	prodej
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
Taylor	Taylor	k1gMnSc1	Taylor
stal	stát	k5eAaPmAgMnS	stát
hráčem	hráč	k1gMnSc7	hráč
basketbalového	basketbalový	k2eAgInSc2d1	basketbalový
týmu	tým	k1gInSc2	tým
Converse	Converse	k1gFnSc2	Converse
<g/>
.	.	kIx.	.
</s>
<s>
Chuck	Chuck	k1gMnSc1	Chuck
Taylor	Taylor	k1gMnSc1	Taylor
se	se	k3xPyFc4	se
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
také	také	k9	také
podílel	podílet	k5eAaImAgMnS	podílet
na	na	k7c6	na
dalším	další	k2eAgInSc6d1	další
vývoji	vývoj	k1gInSc6	vývoj
a	a	k8xC	a
vylepšování	vylepšování	k1gNnSc6	vylepšování
modelu	model	k1gInSc2	model
bot	bota	k1gFnPc2	bota
All	All	k1gFnSc2	All
Star	Star	kA	Star
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
byly	být	k5eAaImAgFnP	být
nové	nový	k2eAgFnPc1d1	nová
kotníkové	kotníkový	k2eAgFnPc1d1	kotníková
basketbalové	basketbalový	k2eAgFnPc1d1	basketbalová
boty	bota	k1gFnPc1	bota
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
firma	firma	k1gFnSc1	firma
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
pojmenovat	pojmenovat	k5eAaPmF	pojmenovat
Taylorovým	Taylorův	k2eAgNnSc7d1	Taylorovo
jménem	jméno	k1gNnSc7	jméno
–	–	k?	–
"	"	kIx"	"
<g/>
All	All	k1gMnSc1	All
Star	Star	kA	Star
Chuck	Chuck	k1gMnSc1	Chuck
Taylor	Taylor	k1gMnSc1	Taylor
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
tenisky	teniska	k1gFnPc1	teniska
se	se	k3xPyFc4	se
začaly	začít	k5eAaPmAgFnP	začít
vyrábět	vyrábět	k5eAaImF	vyrábět
roku	rok	k1gInSc2	rok
1932	[number]	k4	1932
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
kotníkovém	kotníkový	k2eAgInSc6d1	kotníkový
štítku	štítek	k1gInSc6	štítek
nesly	nést	k5eAaImAgFnP	nést
kromě	kromě	k7c2	kromě
pěticípé	pěticípý	k2eAgFnSc2d1	pěticípá
hvězdy	hvězda	k1gFnSc2	hvězda
a	a	k8xC	a
nápisu	nápis	k1gInSc2	nápis
All	All	k1gFnSc2	All
Star	Star	kA	Star
také	také	k9	také
jméno	jméno	k1gNnSc4	jméno
Chuck	Chuck	k1gMnSc1	Chuck
Taylor	Taylor	k1gMnSc1	Taylor
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
model	model	k1gInSc1	model
bot	bota	k1gFnPc2	bota
se	se	k3xPyFc4	se
v	v	k7c6	v
téměř	téměř	k6eAd1	téměř
nezměněné	změněný	k2eNgFnSc6d1	nezměněná
podobě	podoba	k1gFnSc6	podoba
vyrábí	vyrábět	k5eAaImIp3nS	vyrábět
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
30	[number]	k4	30
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Chuck	Chuck	k1gMnSc1	Chuck
Taylor	Taylor	k1gMnSc1	Taylor
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
bílou	bílý	k2eAgFnSc4d1	bílá
verzi	verze	k1gFnSc4	verze
těchto	tento	k3xDgFnPc2	tento
bot	bota	k1gFnPc2	bota
určenou	určený	k2eAgFnSc4d1	určená
pro	pro	k7c4	pro
Olympijské	olympijský	k2eAgFnPc4d1	olympijská
hry	hra	k1gFnPc4	hra
1936	[number]	k4	1936
<g/>
.	.	kIx.	.
</s>
<s>
Boty	bota	k1gFnPc1	bota
mají	mít	k5eAaImIp3nP	mít
bílý	bílý	k2eAgInSc4d1	bílý
svršek	svršek	k1gInSc4	svršek
a	a	k8xC	a
bílou	bílý	k2eAgFnSc4d1	bílá
podrážku	podrážka	k1gFnSc4	podrážka
s	s	k7c7	s
červeným	červený	k2eAgNnSc7d1	červené
a	a	k8xC	a
modrým	modrý	k2eAgNnSc7d1	modré
lemováním	lemování	k1gNnSc7	lemování
<g/>
,	,	kIx,	,
v	v	k7c6	v
amerických	americký	k2eAgFnPc6d1	americká
národních	národní	k2eAgFnPc6d1	národní
barvách	barva	k1gFnPc6	barva
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
druhé	druhý	k4xOgFnSc2	druhý
světové	světový	k2eAgFnSc2d1	světová
války	válka	k1gFnSc2	válka
sloužil	sloužit	k5eAaImAgMnS	sloužit
Chuck	Chuck	k1gMnSc1	Chuck
Taylor	Taylor	k1gMnSc1	Taylor
jako	jako	k8xC	jako
kapitán	kapitán	k1gMnSc1	kapitán
v	v	k7c6	v
amerických	americký	k2eAgFnPc6d1	americká
vzdušných	vzdušný	k2eAgFnPc6d1	vzdušná
silách	síla	k1gFnPc6	síla
a	a	k8xC	a
trénoval	trénovat	k5eAaImAgMnS	trénovat
v	v	k7c6	v
místních	místní	k2eAgInPc6d1	místní
basketbalových	basketbalový	k2eAgInPc6d1	basketbalový
týmech	tým	k1gInPc6	tým
<g/>
.	.	kIx.	.
</s>
<s>
Tenisky	teniska	k1gFnPc1	teniska
All	All	k1gMnSc2	All
Star	Star	kA	Star
Chuck	Chuck	k1gMnSc1	Chuck
Taylor	Taylor	k1gMnSc1	Taylor
v	v	k7c6	v
olympijské	olympijský	k2eAgFnSc6d1	olympijská
bílé	bílý	k2eAgFnSc6d1	bílá
verzi	verze	k1gFnSc6	verze
se	se	k3xPyFc4	se
také	také	k9	také
během	během	k7c2	během
války	válka	k1gFnSc2	válka
staly	stát	k5eAaPmAgInP	stát
oficiální	oficiální	k2eAgInPc1d1	oficiální
sportovní	sportovní	k2eAgFnSc7d1	sportovní
obuví	obuv	k1gFnSc7	obuv
americké	americký	k2eAgFnSc2d1	americká
armády	armáda	k1gFnSc2	armáda
a	a	k8xC	a
byly	být	k5eAaImAgInP	být
využívány	využívat	k5eAaPmNgInP	využívat
při	při	k7c6	při
vojenském	vojenský	k2eAgInSc6d1	vojenský
výcviku	výcvik	k1gInSc6	výcvik
<g/>
.	.	kIx.	.
</s>
