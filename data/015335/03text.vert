<s>
Goodison	Goodison	k1gInSc1
Park	park	k1gInSc1
</s>
<s>
Goodison	Goodison	k1gMnSc1
ParkThe	ParkTh	k1gFnSc2
Grand	grand	k1gMnSc1
Old	Olda	k1gFnPc2
Lady	Lada	k1gFnSc2
Ocenění	ocenění	k1gNnSc1
</s>
<s>
UEFA	UEFA	kA
Poloha	poloha	k1gFnSc1
</s>
<s>
Liverpool	Liverpool	k1gInSc1
<g/>
,	,	kIx,
Anglie	Anglie	k1gFnSc1
Souřadnice	souřadnice	k1gFnSc1
</s>
<s>
53	#num#	k4
<g/>
°	°	k?
<g/>
26	#num#	k4
<g/>
′	′	k?
<g/>
20	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
2	#num#	k4
<g/>
°	°	k?
<g/>
57	#num#	k4
<g/>
′	′	k?
<g/>
59	#num#	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
Otevření	otevření	k1gNnSc2
</s>
<s>
24	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
1892	#num#	k4
Vlastník	vlastník	k1gMnSc1
</s>
<s>
Everton	Everton	k1gInSc1
FC	FC	kA
Architekt	architekt	k1gMnSc1
</s>
<s>
Kelly	Kella	k1gFnPc1
Brothers	Brothersa	k1gFnPc2
Bývalé	bývalý	k2eAgInPc1d1
názvy	název	k1gInPc1
</s>
<s>
Mere	Mere	k6eAd1
Green	Green	k2eAgInSc1d1
field	field	k1gInSc1
Události	událost	k1gFnSc2
</s>
<s>
•	•	k?
finále	finále	k1gNnSc1
FA	fa	k1gNnPc2
Cupu	cup	k1gInSc2
(	(	kIx(
<g/>
1894	#num#	k4
<g/>
,	,	kIx,
1910	#num#	k4
<g/>
)	)	kIx)
•	•	k?
MS	MS	kA
ve	v	k7c6
fotbale	fotbal	k1gInSc6
1966	#num#	k4
Týmy	tým	k1gInPc1
</s>
<s>
Everton	Everton	k1gInSc1
FC	FC	kA
(	(	kIx(
<g/>
1892	#num#	k4
<g/>
–	–	k?
)	)	kIx)
Kapacita	kapacita	k1gFnSc1
</s>
<s>
39	#num#	k4
572	#num#	k4
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Rozměry	rozměra	k1gFnSc2
</s>
<s>
102.4	102.4	k4
×	×	k?
71.3	71.3	k4
m	m	kA
Kód	kód	k1gInSc4
památky	památka	k1gFnSc2
</s>
<s>
1462465	#num#	k4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Goodison	Goodison	k1gInSc1
Park	park	k1gInSc1
je	být	k5eAaImIp3nS
fotbalový	fotbalový	k2eAgInSc4d1
stadion	stadion	k1gInSc4
ležící	ležící	k2eAgInSc4d1
v	v	k7c6
anglickém	anglický	k2eAgNnSc6d1
městě	město	k1gNnSc6
Liverpool	Liverpool	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svá	svůj	k3xOyFgNnPc4
domácí	domácí	k2eAgNnPc4d1
utkání	utkání	k1gNnPc4
zde	zde	k6eAd1
odehrává	odehrávat	k5eAaImIp3nS
ligový	ligový	k2eAgInSc4d1
klub	klub	k1gInSc4
Everton	Everton	k1gInSc1
FC	FC	kA
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
na	na	k7c6
stadionu	stadion	k1gInSc6
působí	působit	k5eAaImIp3nS
od	od	k7c2
jeho	on	k3xPp3gNnSc2,k3xOp3gNnSc2
založení	založení	k1gNnSc2
v	v	k7c6
roce	rok	k1gInSc6
1892	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Goodison	Goodison	k1gInSc1
Park	park	k1gInSc4
v	v	k7c6
současnosti	současnost	k1gFnSc6
pojme	pojmout	k5eAaPmIp3nS
39	#num#	k4
572	#num#	k4
diváků	divák	k1gMnPc2
<g/>
,	,	kIx,
všechna	všechen	k3xTgNnPc1
místa	místo	k1gNnPc1
jsou	být	k5eAaImIp3nP
k	k	k7c3
sezení	sezení	k1gNnSc3
<g/>
.	.	kIx.
</s>
<s>
Stadion	stadion	k1gInSc1
hostil	hostit	k5eAaImAgInS
nejvíce	nejvíce	k6eAd1,k6eAd3
prvoligových	prvoligový	k2eAgInPc2d1
zápasů	zápas	k1gInPc2
ze	z	k7c2
všech	všecek	k3xTgInPc2
dosud	dosud	k6eAd1
stojících	stojící	k2eAgInPc2d1
stadionů	stadion	k1gInPc2
v	v	k7c6
celé	celý	k2eAgFnSc6d1
Anglii	Anglie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
založení	založení	k1gNnSc2
Premier	Premier	k1gInSc1
League	League	k1gFnSc4
v	v	k7c6
roce	rok	k1gInSc6
1992	#num#	k4
pak	pak	k6eAd1
také	také	k9
hostil	hostit	k5eAaImAgInS
maximální	maximální	k2eAgInSc4d1
počet	počet	k1gInSc4
domácích	domácí	k2eAgInPc2d1
ligových	ligový	k2eAgInPc2d1
zápasů	zápas	k1gInPc2
<g/>
,	,	kIx,
protože	protože	k8xS
samotný	samotný	k2eAgInSc1d1
Everton	Everton	k1gInSc1
je	být	k5eAaImIp3nS
členem	člen	k1gInSc7
nejvyšší	vysoký	k2eAgFnSc2d3
soutěže	soutěž	k1gFnSc2
nepřetržitě	přetržitě	k6eNd1
od	od	k7c2
roku	rok	k1gInSc2
1954	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkově	celkově	k6eAd1
byl	být	k5eAaImAgInS
klub	klub	k1gInSc1
mimo	mimo	k7c4
anglickou	anglický	k2eAgFnSc4d1
nejvyšší	vysoký	k2eAgFnSc4d3
soutěže	soutěž	k1gFnPc4
pouze	pouze	k6eAd1
ve	v	k7c6
čtyřech	čtyři	k4xCgFnPc6
sezónách	sezóna	k1gFnPc6
<g/>
,	,	kIx,
s	s	k7c7
tím	ten	k3xDgMnSc7
že	že	k8xS
sestoupil	sestoupit	k5eAaPmAgMnS
pouze	pouze	k6eAd1
ve	v	k7c6
dvou	dva	k4xCgInPc6
případech	případ	k1gInPc6
(	(	kIx(
<g/>
v	v	k7c6
roce	rok	k1gInSc6
1930	#num#	k4
a	a	k8xC
1951	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Mimo	mimo	k7c4
domácí	domácí	k2eAgInPc4d1
zápasy	zápas	k1gInPc4
Evertonu	Everton	k1gInSc2
stadion	stadion	k1gInSc4
dále	daleko	k6eAd2
hostil	hostit	k5eAaImAgMnS
dvě	dva	k4xCgFnPc4
finále	finále	k1gNnSc6
FA	fa	k1gNnSc6
Cupu	cupat	k5eAaImIp1nS
<g/>
,	,	kIx,
několik	několik	k4yIc1
mezinárodních	mezinárodní	k2eAgFnPc2d1
reprezentačních	reprezentační	k2eAgFnPc2d1
utkání	utkání	k1gNnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
několika	několik	k4yIc2
zápasů	zápas	k1gInPc2
na	na	k7c4
Mistrovství	mistrovství	k1gNnSc4
světa	svět	k1gInSc2
ve	v	k7c6
fotbale	fotbal	k1gInSc6
v	v	k7c6
roce	rok	k1gInSc6
1966	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stadionu	stadion	k1gInSc2
patřil	patřit	k5eAaImAgMnS
několik	několik	k4yIc1
desetiletí	desetiletí	k1gNnPc2
rekord	rekord	k1gInSc4
v	v	k7c6
počtu	počet	k1gInSc6
platících	platící	k2eAgMnPc2d1
diváků	divák	k1gMnPc2
příchozích	příchozí	k1gMnPc2
na	na	k7c4
zápas	zápas	k1gInSc4
v	v	k7c6
ženském	ženský	k2eAgInSc6d1
fotbale	fotbal	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yRgInSc1,k3yQgInSc1
byl	být	k5eAaImAgInS
odehrán	odehrát	k5eAaPmNgInS
v	v	k7c6
roce	rok	k1gInSc6
1920	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
byl	být	k5eAaImAgInS
překonán	překonat	k5eAaPmNgInS
až	až	k9
po	po	k7c6
92	#num#	k4
letech	léto	k1gNnPc6
na	na	k7c6
Letních	letní	k2eAgFnPc6d1
olympijských	olympijský	k2eAgFnPc6d1
hrách	hra	k1gFnPc6
v	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Goodison	Goodison	k1gInSc1
Park	park	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
History	Histor	k1gInPc1
of	of	k?
Goodison	Goodison	k1gInSc1
Park	park	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Everton	Everton	k1gInSc1
F.	F.	kA
<g/>
C.	C.	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Goodison	Goodison	k1gInSc1
Park	park	k1gInSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Goodison	Goodison	k1gInSc1
Park	park	k1gInSc1
na	na	k7c6
stránkách	stránka	k1gFnPc6
worldofstadiums	worldofstadiums	k1gInSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Fotbal	fotbal	k1gInSc1
|	|	kIx~
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
</s>
