<s>
Giulio	Giulio	k6eAd1	Giulio
Caccini	Caccin	k2eAgMnPc1d1	Caccin
<g/>
(	(	kIx(	(
<g/>
Tivoli	Tivole	k1gFnSc6	Tivole
nebo	nebo	k8xC	nebo
Řím	Řím	k1gInSc1	Řím
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
1545	[number]	k4	1545
-	-	kIx~	-
Florencie	Florencie	k1gFnSc2	Florencie
<g/>
,	,	kIx,	,
1618	[number]	k4	1618
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
italský	italský	k2eAgMnSc1d1	italský
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
pěvec	pěvec	k1gMnSc1	pěvec
a	a	k8xC	a
učitel	učitel	k1gMnSc1	učitel
zpěvu	zpěv	k1gInSc2	zpěv
<g/>
,	,	kIx,	,
hráč	hráč	k1gMnSc1	hráč
na	na	k7c4	na
loutnu	loutna	k1gFnSc4	loutna
a	a	k8xC	a
harfu	harfa	k1gFnSc4	harfa
<g/>
.	.	kIx.	.
</s>
