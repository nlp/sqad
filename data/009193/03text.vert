<p>
<s>
Giulio	Giulio	k6eAd1	Giulio
Caccini	Caccin	k2eAgMnPc1d1	Caccin
<g/>
(	(	kIx(	(
<g/>
Tivoli	Tivole	k1gFnSc6	Tivole
nebo	nebo	k8xC	nebo
Řím	Řím	k1gInSc1	Řím
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
1545	[number]	k4	1545
-	-	kIx~	-
Florencie	Florencie	k1gFnSc2	Florencie
<g/>
,	,	kIx,	,
1618	[number]	k4	1618
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
italský	italský	k2eAgMnSc1d1	italský
hudební	hudební	k2eAgMnSc1d1	hudební
skladatel	skladatel	k1gMnSc1	skladatel
<g/>
,	,	kIx,	,
pěvec	pěvec	k1gMnSc1	pěvec
a	a	k8xC	a
učitel	učitel	k1gMnSc1	učitel
zpěvu	zpěv	k1gInSc2	zpěv
<g/>
,	,	kIx,	,
hráč	hráč	k1gMnSc1	hráč
na	na	k7c4	na
loutnu	loutna	k1gFnSc4	loutna
a	a	k8xC	a
harfu	harfa	k1gFnSc4	harfa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Životopis	životopis	k1gInSc4	životopis
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c4	v
mládí	mládí	k1gNnSc4	mládí
(	(	kIx(	(
<g/>
místo	místo	k1gNnSc4	místo
a	a	k8xC	a
datum	datum	k1gNnSc4	datum
narození	narození	k1gNnSc2	narození
jsou	být	k5eAaImIp3nP	být
nejisté	jistý	k2eNgNnSc4d1	nejisté
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
členem	člen	k1gMnSc7	člen
Julské	Julský	k2eAgFnSc2d1	Julský
kapely	kapela	k1gFnSc2	kapela
(	(	kIx(	(
<g/>
Cappella	Cappella	k1gFnSc1	Cappella
Giulia	Giulia	k1gFnSc1	Giulia
<g/>
)	)	kIx)	)
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Dlouho	dlouho	k6eAd1	dlouho
cestoval	cestovat	k5eAaImAgMnS	cestovat
mezi	mezi	k7c7	mezi
Římem	Řím	k1gInSc7	Řím
<g/>
,	,	kIx,	,
Ferrarou	Ferrara	k1gFnSc7	Ferrara
a	a	k8xC	a
Paříží	Paříž	k1gFnSc7	Paříž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
usídlil	usídlit	k5eAaPmAgMnS	usídlit
ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
<g/>
,	,	kIx,	,
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
členem	člen	k1gInSc7	člen
Spolku	spolek	k1gInSc2	spolek
bardů	bard	k1gMnPc2	bard
(	(	kIx(	(
<g/>
Camerata	Camerata	k1gFnSc1	Camerata
de	de	k?	de
<g/>
'	'	kIx"	'
Bardi	bard	k1gMnPc1	bard
<g/>
,	,	kIx,	,
známé	známý	k2eAgFnPc1d1	známá
také	také	k9	také
jako	jako	k9	jako
Florentská	florentský	k2eAgFnSc1d1	florentská
camerata	camerata	k1gFnSc1	camerata
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
akademie	akademie	k1gFnSc1	akademie
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
na	na	k7c6	na
konci	konec	k1gInSc6	konec
16	[number]	k4	16
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
položila	položit	k5eAaPmAgFnS	položit
základ	základ	k1gInSc4	základ
moderního	moderní	k2eAgNnSc2d1	moderní
melodramatu	melodramatu	k?	melodramatu
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc4	který
Caccini	Caccin	k1gMnPc1	Caccin
sepsal	sepsat	k5eAaPmAgMnS	sepsat
v	v	k7c6	v
díle	dílo	k1gNnSc6	dílo
"	"	kIx"	"
<g/>
Le	Le	k1gMnSc1	Le
nuove	nuovat	k5eAaPmIp3nS	nuovat
musiche	musiche	k6eAd1	musiche
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1601	[number]	k4	1601
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Deset	deset	k4xCc1	deset
let	léto	k1gNnPc2	léto
po	po	k7c6	po
svém	svůj	k3xOyFgInSc6	svůj
prvním	první	k4xOgInSc6	první
pěveckém	pěvecký	k2eAgInSc6d1	pěvecký
výstupu	výstup	k1gInSc6	výstup
(	(	kIx(	(
<g/>
1579	[number]	k4	1579
<g/>
,	,	kIx,	,
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
sňatku	sňatek	k1gInSc2	sňatek
Františka	František	k1gMnSc2	František
I.	I.	kA	I.
Medicejského	Medicejský	k2eAgMnSc2d1	Medicejský
s	s	k7c7	s
Biancou	Bianca	k1gFnSc7	Bianca
Capello	Capello	k1gNnSc1	Capello
<g/>
,	,	kIx,	,
spatřilo	spatřit	k5eAaPmAgNnS	spatřit
(	(	kIx(	(
<g/>
roku	rok	k1gInSc2	rok
1589	[number]	k4	1589
<g/>
)	)	kIx)	)
světlo	světlo	k1gNnSc1	světlo
světa	svět	k1gInSc2	svět
jeho	jeho	k3xOp3gNnSc4	jeho
první	první	k4xOgNnSc4	první
skladatelské	skladatelský	k2eAgNnSc4d1	skladatelské
dílo	dílo	k1gNnSc4	dílo
<g/>
:	:	kIx,	:
šlo	jít	k5eAaImAgNnS	jít
o	o	k7c4	o
intermedio	intermedio	k1gNnSc4	intermedio
-	-	kIx~	-
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
nazývaly	nazývat	k5eAaImAgFnP	nazývat
krátké	krátký	k2eAgFnPc1d1	krátká
hudební	hudební	k2eAgFnPc1d1	hudební
vsuvky	vsuvka	k1gFnPc1	vsuvka
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
se	se	k3xPyFc4	se
prolínaly	prolínat	k5eAaImAgFnP	prolínat
divadelními	divadelní	k2eAgNnPc7d1	divadelní
představeními	představení	k1gNnPc7	představení
-	-	kIx~	-
ke	k	k7c3	k
komedii	komedie	k1gFnSc3	komedie
"	"	kIx"	"
<g/>
La	la	k1gNnSc1	la
pellegrina	pellegrina	k1gFnSc1	pellegrina
<g/>
"	"	kIx"	"
-	-	kIx~	-
čes.	čes.	k?	čes.
"	"	kIx"	"
<g/>
Poutnice	poutnice	k1gFnSc1	poutnice
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
objednávku	objednávka	k1gFnSc4	objednávka
pro	pro	k7c4	pro
sňatek	sňatek	k1gInSc4	sňatek
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
I.	I.	kA	I.
Medicejského	Medicejský	k2eAgMnSc2d1	Medicejský
s	s	k7c7	s
Kristinou	Kristina	k1gFnSc7	Kristina
Lotrinskou	lotrinský	k2eAgFnSc7d1	lotrinská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
žánr	žánr	k1gInSc4	žánr
divadlo	divadlo	k1gNnSc4	divadlo
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
-	-	kIx~	-
jež	jenž	k3xRgFnSc1	jenž
zejména	zejména	k9	zejména
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
mezi	mezi	k7c4	mezi
16	[number]	k4	16
<g/>
.	.	kIx.	.
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
stoletím	století	k1gNnSc7	století
začala	začít	k5eAaPmAgFnS	začít
podnikat	podnikat	k5eAaImF	podnikat
své	svůj	k3xOyFgInPc4	svůj
první	první	k4xOgInPc4	první
krůčky	krůček	k1gInPc4	krůček
-	-	kIx~	-
zkomponoval	zkomponovat	k5eAaPmAgMnS	zkomponovat
hudbu	hudba	k1gFnSc4	hudba
"	"	kIx"	"
<g/>
Il	Il	k1gFnSc1	Il
rapimento	rapimento	k1gNnSc1	rapimento
di	di	k?	di
Cefalo	Cefala	k1gFnSc5	Cefala
<g/>
"	"	kIx"	"
na	na	k7c4	na
text	text	k1gInSc4	text
Gabriella	Gabriello	k1gNnSc2	Gabriello
Chiabrery	Chiabrera	k1gFnSc2	Chiabrera
(	(	kIx(	(
<g/>
téměř	téměř	k6eAd1	téměř
ztracené	ztracený	k2eAgFnPc1d1	ztracená
<g/>
,	,	kIx,	,
s	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
závěrečného	závěrečný	k2eAgInSc2d1	závěrečný
sboru	sbor	k1gInSc2	sbor
<g/>
)	)	kIx)	)
a	a	k8xC	a
"	"	kIx"	"
<g/>
Euridice	Euridice	k1gFnSc1	Euridice
<g/>
"	"	kIx"	"
od	od	k7c2	od
Ottavia	Ottavius	k1gMnSc2	Ottavius
Rinucciniho	Rinuccini	k1gMnSc2	Rinuccini
(	(	kIx(	(
<g/>
text	text	k1gInSc1	text
byl	být	k5eAaImAgInS	být
zhudebněn	zhudebnit	k5eAaPmNgInS	zhudebnit
a	a	k8xC	a
skladba	skladba	k1gFnSc1	skladba
provedena	provést	k5eAaPmNgFnS	provést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1600	[number]	k4	1600
také	také	k9	také
Jacopem	Jacop	k1gInSc7	Jacop
Perim	Perima	k1gFnPc2	Perima
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Významným	významný	k2eAgInSc7d1	významný
dílem	díl	k1gInSc7	díl
jsou	být	k5eAaImIp3nP	být
také	také	k9	také
dvě	dva	k4xCgFnPc4	dva
sbírky	sbírka	k1gFnPc4	sbírka
árií	árie	k1gFnPc2	árie
a	a	k8xC	a
madrigalů	madrigal	k1gInPc2	madrigal
pro	pro	k7c4	pro
sólový	sólový	k2eAgInSc4d1	sólový
hlas	hlas	k1gInSc4	hlas
<g/>
,	,	kIx,	,
napsaných	napsaný	k2eAgFnPc2d1	napsaná
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1602	[number]	k4	1602
až	až	k9	až
1614	[number]	k4	1614
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
představují	představovat	k5eAaImIp3nP	představovat
cestu	cesta	k1gFnSc4	cesta
od	od	k7c2	od
přísně	přísně	k6eAd1	přísně
polyfonního	polyfonní	k2eAgInSc2d1	polyfonní
madrigalu	madrigal	k1gInSc2	madrigal
k	k	k7c3	k
monodickému	monodický	k2eAgNnSc3d1	monodický
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Caccini	Caccin	k1gMnPc1	Caccin
se	se	k3xPyFc4	se
roku	rok	k1gInSc2	rok
1604	[number]	k4	1604
definitivně	definitivně	k6eAd1	definitivně
usídlil	usídlit	k5eAaPmAgMnS	usídlit
ve	v	k7c6	v
Florencii	Florencie	k1gFnSc6	Florencie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
také	také	k9	také
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1618	[number]	k4	1618
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnPc6	jeho
stopách	stopa	k1gFnPc6	stopa
šla	jít	k5eAaImAgFnS	jít
jeho	jeho	k3xOp3gFnSc1	jeho
dcera	dcera	k1gFnSc1	dcera
<g/>
,	,	kIx,	,
Francesca	Francesca	k1gFnSc1	Francesca
<g/>
,	,	kIx,	,
přezdívaná	přezdívaný	k2eAgFnSc1d1	přezdívaná
"	"	kIx"	"
<g/>
La	la	k1gNnPc2	la
Cecchina	Cecchino	k1gNnSc2	Cecchino
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Giulio	Giulio	k6eAd1	Giulio
Caccini	Caccin	k2eAgMnPc1d1	Caccin
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
