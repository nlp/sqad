<s>
Jonathan	Jonathan	k1gMnSc1	Jonathan
Littel	Littel	k1gMnSc1	Littel
(	(	kIx(	(
<g/>
*	*	kIx~	*
10	[number]	k4	10
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1967	[number]	k4	1967
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
francouzsky	francouzsky	k6eAd1	francouzsky
píšící	píšící	k2eAgMnSc1d1	píšící
americký	americký	k2eAgMnSc1d1	americký
spisovatel	spisovatel	k1gMnSc1	spisovatel
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
žijící	žijící	k2eAgMnSc1d1	žijící
v	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
<g/>
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Ve	v	k7c6	v
Francii	Francie	k1gFnSc6	Francie
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgMnSc1d1	považován
za	za	k7c4	za
francouzského	francouzský	k2eAgMnSc4d1	francouzský
spisovatele	spisovatel	k1gMnSc4	spisovatel
amerického	americký	k2eAgInSc2d1	americký
původu	původ	k1gInSc2	původ
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
</s>
<s>
Jeho	jeho	k3xOp3gInSc7	jeho
nejznámějším	známý	k2eAgInSc7d3	nejznámější
dílem	díl	k1gInSc7	díl
je	být	k5eAaImIp3nS	být
román	román	k1gInSc1	román
Laskavé	laskavý	k2eAgFnSc2d1	laskavá
bohyně	bohyně	k1gFnSc2	bohyně
<g/>
.	.	kIx.	.
</s>
<s>
Narodil	narodit	k5eAaPmAgMnS	narodit
se	se	k3xPyFc4	se
v	v	k7c6	v
židovské	židovský	k2eAgFnSc6d1	židovská
rodině	rodina	k1gFnSc6	rodina
(	(	kIx(	(
<g/>
sám	sám	k3xTgInSc1	sám
se	se	k3xPyFc4	se
však	však	k9	však
za	za	k7c4	za
žida	žid	k1gMnSc4	žid
"	"	kIx"	"
<g/>
vůbec	vůbec	k9	vůbec
nepovažuje	považovat	k5eNaImIp3nS	považovat
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
emigrovala	emigrovat	k5eAaBmAgFnS	emigrovat
z	z	k7c2	z
Polska	Polsko	k1gNnSc2	Polsko
do	do	k7c2	do
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
na	na	k7c6	na
konci	konec	k1gInSc6	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gMnSc7	jeho
otcem	otec	k1gMnSc7	otec
je	být	k5eAaImIp3nS	být
spisovatel	spisovatel	k1gMnSc1	spisovatel
Robert	Robert	k1gMnSc1	Robert
Littell	Littell	k1gMnSc1	Littell
<g/>
.	.	kIx.	.
</s>
<s>
Vystudoval	vystudovat	k5eAaPmAgMnS	vystudovat
pařížské	pařížský	k2eAgNnSc4d1	pařížské
lycée	lycée	k1gNnSc4	lycée
Fenelon	Fenelon	k1gInSc1	Fenelon
a	a	k8xC	a
odjel	odjet	k5eAaPmAgMnS	odjet
do	do	k7c2	do
Ameriky	Amerika	k1gFnSc2	Amerika
studovat	studovat	k5eAaImF	studovat
na	na	k7c4	na
Yale	Yale	k1gFnSc4	Yale
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
třech	tři	k4xCgNnPc6	tři
letech	léto	k1gNnPc6	léto
na	na	k7c6	na
Yale	Yale	k1gNnSc6	Yale
odjel	odjet	k5eAaPmAgInS	odjet
na	na	k7c4	na
tehdy	tehdy	k6eAd1	tehdy
válčící	válčící	k2eAgInSc4d1	válčící
Balkán	Balkán	k1gInSc4	Balkán
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
sedmi	sedm	k4xCc2	sedm
let	léto	k1gNnPc2	léto
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
humanitárních	humanitární	k2eAgFnPc2d1	humanitární
akcí	akce	k1gFnPc2	akce
neziskové	ziskový	k2eNgFnSc2d1	nezisková
organizaceAction	organizaceAction	k1gInSc1	organizaceAction
contre	contr	k1gInSc5	contr
la	la	k0	la
faim	faim	k6eAd1	faim
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Bosně	Bosna	k1gFnSc6	Bosna
a	a	k8xC	a
Hercegovině	Hercegovina	k1gFnSc6	Hercegovina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
na	na	k7c6	na
různých	různý	k2eAgNnPc6d1	různé
místech	místo	k1gNnPc6	místo
po	po	k7c6	po
celém	celý	k2eAgInSc6d1	celý
světě	svět	k1gInSc6	svět
jako	jako	k8xC	jako
např.	např.	kA	např.
v	v	k7c6	v
Čečensku	Čečensko	k1gNnSc6	Čečensko
<g/>
,	,	kIx,	,
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
<g/>
,	,	kIx,	,
Kongu	Kongo	k1gNnSc6	Kongo
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgInS	rozhodnout
ukončit	ukončit	k5eAaPmF	ukončit
humanitární	humanitární	k2eAgFnSc4d1	humanitární
činnost	činnost	k1gFnSc4	činnost
a	a	k8xC	a
začal	začít	k5eAaPmAgInS	začít
psát	psát	k5eAaImF	psát
svůj	svůj	k3xOyFgInSc4	svůj
nejznámější	známý	k2eAgInSc4d3	nejznámější
román	román	k1gInSc4	román
Laskavé	laskavý	k2eAgFnSc2d1	laskavá
bohyně	bohyně	k1gFnSc2	bohyně
<g/>
,	,	kIx,	,
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
fresku	freska	k1gFnSc4	freska
vykreslující	vykreslující	k2eAgFnSc4d1	vykreslující
pomocí	pomocí	k7c2	pomocí
fiktivních	fiktivní	k2eAgFnPc2d1	fiktivní
vzpomínek	vzpomínka	k1gFnPc2	vzpomínka
vzdělaného	vzdělaný	k2eAgMnSc2d1	vzdělaný
důstojníka	důstojník	k1gMnSc2	důstojník
SS	SS	kA	SS
Maxmiliana	Maxmiliana	k1gFnSc1	Maxmiliana
Aue	Aue	k1gFnSc3	Aue
druhou	druhý	k4xOgFnSc4	druhý
světovou	světový	k2eAgFnSc4d1	světová
válku	válka	k1gFnSc4	válka
a	a	k8xC	a
východní	východní	k2eAgFnSc4d1	východní
frontu	fronta	k1gFnSc4	fronta
<g/>
.	.	kIx.	.
</s>
<s>
Laskavé	laskavý	k2eAgFnPc1d1	laskavá
bohyně	bohyně	k1gFnPc1	bohyně
získaly	získat	k5eAaPmAgFnP	získat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
Goncourtovu	Goncourtův	k2eAgFnSc4d1	Goncourtova
cenu	cena	k1gFnSc4	cena
a	a	k8xC	a
Velkou	velký	k2eAgFnSc4d1	velká
cenu	cena	k1gFnSc4	cena
Francouzské	francouzský	k2eAgFnSc2d1	francouzská
akademie	akademie	k1gFnSc2	akademie
za	za	k7c4	za
román	román	k1gInSc4	román
<g/>
.	.	kIx.	.
</s>
<s>
Jorge	Jorg	k1gMnSc2	Jorg
Semprún	Semprún	k1gNnSc1	Semprún
tuto	tento	k3xDgFnSc4	tento
knihu	kniha	k1gFnSc4	kniha
označil	označit	k5eAaPmAgMnS	označit
za	za	k7c4	za
"	"	kIx"	"
<g/>
událost	událost	k1gFnSc4	událost
století	století	k1gNnSc2	století
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jediné	jediný	k2eAgNnSc1d1	jediné
jeho	jeho	k3xOp3gNnSc4	jeho
předchozí	předchozí	k2eAgNnSc4d1	předchozí
vydané	vydaný	k2eAgNnSc4d1	vydané
dílo	dílo	k1gNnSc4	dílo
<g/>
,	,	kIx,	,
kyberpunkový	kyberpunkový	k2eAgInSc4d1	kyberpunkový
román	román	k1gInSc4	román
Bad	Bad	k1gMnSc2	Bad
Voltage	Voltag	k1gMnSc2	Voltag
<g/>
,	,	kIx,	,
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1989	[number]	k4	1989
ve	v	k7c6	v
vydavatelství	vydavatelství	k1gNnSc6	vydavatelství
Signet	signet	k1gInSc4	signet
Book	Booka	k1gFnPc2	Booka
<g/>
.	.	kIx.	.
</s>
<s>
Kniha	kniha	k1gFnSc1	kniha
sice	sice	k8xC	sice
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
žádné	žádný	k3yNgFnPc4	žádný
zmínky	zmínka	k1gFnPc4	zmínka
o	o	k7c6	o
autorovi	autor	k1gMnSc6	autor
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
časté	častý	k2eAgInPc1d1	častý
odkazy	odkaz	k1gInPc1	odkaz
na	na	k7c6	na
Francii	Francie	k1gFnSc6	Francie
a	a	k8xC	a
autory	autor	k1gMnPc4	autor
jako	jako	k8xS	jako
Jean	Jean	k1gMnSc1	Jean
Genet	Genet	k1gMnSc1	Genet
a	a	k8xC	a
Charles	Charles	k1gMnSc1	Charles
Baudelaire	Baudelair	k1gInSc5	Baudelair
a	a	k8xC	a
na	na	k7c4	na
Paříž	Paříž	k1gFnSc4	Paříž
bezpochyby	bezpochyby	k6eAd1	bezpochyby
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
na	na	k7c4	na
téhož	týž	k3xTgMnSc4	týž
autora	autor	k1gMnSc4	autor
<g/>
.	.	kIx.	.
</s>
<s>
Několikrát	několikrát	k6eAd1	několikrát
zde	zde	k6eAd1	zde
ostatně	ostatně	k6eAd1	ostatně
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
pařížské	pařížský	k2eAgNnSc4d1	pařížské
podzemí	podzemí	k1gNnSc4	podzemí
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
děj	děj	k1gInSc1	děj
(	(	kIx(	(
<g/>
často	často	k6eAd1	často
<g/>
)	)	kIx)	)
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
(	(	kIx(	(
<g/>
Jonathan	Jonathan	k1gMnSc1	Jonathan
Littell	Littell	k1gMnSc1	Littell
byl	být	k5eAaImAgMnS	být
během	během	k7c2	během
svých	svůj	k3xOyFgNnPc2	svůj
studií	studio	k1gNnPc2	studio
na	na	k7c6	na
lycée	lycée	k1gFnSc6	lycée
Fenelon	Fenelon	k1gInSc4	Fenelon
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
tzv.	tzv.	kA	tzv.
katafilem	katafil	k1gMnSc7	katafil
–	–	k?	–
milovníkem	milovník	k1gMnSc7	milovník
nezákonných	zákonný	k2eNgFnPc2d1	nezákonná
výprav	výprava	k1gFnPc2	výprava
do	do	k7c2	do
(	(	kIx(	(
<g/>
nejen	nejen	k6eAd1	nejen
<g/>
)	)	kIx)	)
pařížského	pařížský	k2eAgNnSc2d1	pařížské
podzemí	podzemí	k1gNnSc2	podzemí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
obsáhlou	obsáhlý	k2eAgFnSc4d1	obsáhlá
a	a	k8xC	a
podrobnou	podrobný	k2eAgFnSc4d1	podrobná
zprávu	zpráva	k1gFnSc4	zpráva
o	o	k7c6	o
ruských	ruský	k2eAgFnPc6d1	ruská
tajných	tajný	k2eAgFnPc6d1	tajná
službách	služba	k1gFnPc6	služba
v	v	k7c6	v
letech	let	k1gInPc6	let
1991	[number]	k4	1991
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
The	The	k1gFnSc2	The
Security	Securita	k1gFnSc2	Securita
Organs	Organs	k1gInSc1	Organs
of	of	k?	of
the	the	k?	the
Russian	Russian	k1gInSc1	Russian
Federation	Federation	k1gInSc1	Federation
-	-	kIx~	-
A	a	k8xC	a
Brief	Brief	k1gInSc4	Brief
History	Histor	k1gInPc1	Histor
1991	[number]	k4	1991
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
žije	žít	k5eAaImIp3nS	žít
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
a	a	k8xC	a
dvěma	dva	k4xCgFnPc7	dva
dcerami	dcera	k1gFnPc7	dcera
v	v	k7c6	v
Barceloně	Barcelona	k1gFnSc6	Barcelona
<g/>
.	.	kIx.	.
</s>
<s>
Bad	Bad	k?	Bad
Voltage	Voltage	k1gInSc1	Voltage
–	–	k?	–
Signet	signet	k1gInSc1	signet
Books	Booksa	k1gFnPc2	Booksa
<g/>
,	,	kIx,	,
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-451-16014-2	[number]	k4	0-451-16014-2
Laskavé	laskavý	k2eAgFnSc2d1	laskavá
bohyně	bohyně	k1gFnSc2	bohyně
(	(	kIx(	(
<g/>
Les	les	k1gInSc1	les
Bienveillantes	Bienveillantes	k1gInSc1	Bienveillantes
<g/>
)	)	kIx)	)
–	–	k?	–
Gallimard	Gallimard	k1gInSc1	Gallimard
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
2	[number]	k4	2
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
78097	[number]	k4	78097
<g/>
-X	-X	k?	-X
<g/>
,	,	kIx,	,
český	český	k2eAgInSc4d1	český
překlad	překlad	k1gInSc4	překlad
Michala	Michala	k1gFnSc1	Michala
Marková	Marková	k1gFnSc1	Marková
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
Odeon	odeon	k1gInSc1	odeon
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
978-80-207-1278-3	[number]	k4	978-80-207-1278-3
The	The	k1gFnSc2	The
Security	Securita	k1gFnSc2	Securita
Organs	Organs	k1gInSc1	Organs
of	of	k?	of
the	the	k?	the
Russian	Russian	k1gInSc1	Russian
Federation	Federation	k1gInSc1	Federation
–	–	k?	–
A	a	k8xC	a
Brief	Brief	k1gInSc4	Brief
History	Histor	k1gInPc1	Histor
1991	[number]	k4	1991
<g/>
–	–	k?	–
<g/>
2005	[number]	k4	2005
–	–	k?	–
Publishing	Publishing	k1gInSc1	Publishing
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
(	(	kIx(	(
<g/>
dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
)	)	kIx)	)
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Jonathan	Jonathan	k1gMnSc1	Jonathan
Littell	Littella	k1gFnPc2	Littella
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Osoba	osoba	k1gFnSc1	osoba
Jonathan	Jonathan	k1gMnSc1	Jonathan
Littell	Littell	k1gMnSc1	Littell
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
Jonathan	Jonathan	k1gMnSc1	Jonathan
Littell	Littell	k1gMnSc1	Littell
</s>
