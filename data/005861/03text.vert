<s>
Slovo	slovo	k1gNnSc4	slovo
bikiny	bikiny	k1gFnPc1	bikiny
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
bikini	bikin	k2eAgMnPc1d1	bikin
<g/>
)	)	kIx)	)
označuje	označovat	k5eAaImIp3nS	označovat
dvoudílné	dvoudílný	k2eAgFnPc4d1	dvoudílná
dámské	dámský	k2eAgFnPc4d1	dámská
plavky	plavka	k1gFnPc4	plavka
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něž	jenž	k3xRgFnPc4	jenž
je	být	k5eAaImIp3nS	být
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
rozdělení	rozdělení	k1gNnSc1	rozdělení
na	na	k7c4	na
horní	horní	k2eAgFnSc4d1	horní
část	část	k1gFnSc4	část
zakrývající	zakrývající	k2eAgInSc1d1	zakrývající
ňadra	ňadro	k1gNnSc2	ňadro
a	a	k8xC	a
spodní	spodní	k2eAgFnSc4d1	spodní
část	část	k1gFnSc4	část
zakrývající	zakrývající	k2eAgInSc4d1	zakrývající
klín	klín	k1gInSc4	klín
(	(	kIx(	(
<g/>
oblast	oblast	k1gFnSc4	oblast
třísel	tříslo	k1gNnPc2	tříslo
<g/>
)	)	kIx)	)
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
nebo	nebo	k8xC	nebo
jenom	jenom	k6eAd1	jenom
částečně	částečně	k6eAd1	částečně
hýždě	hýždě	k1gFnSc2	hýždě
<g/>
.	.	kIx.	.
</s>
<s>
Tvary	tvar	k1gInPc1	tvar
a	a	k8xC	a
střihy	střih	k1gInPc1	střih
obou	dva	k4xCgFnPc2	dva
částí	část	k1gFnPc2	část
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgInPc1d1	podobný
tvarům	tvar	k1gInPc3	tvar
podprsenek	podprsenka	k1gFnPc2	podprsenka
a	a	k8xC	a
kalhotek	kalhotky	k1gFnPc2	kalhotky
u	u	k7c2	u
běžného	běžný	k2eAgNnSc2d1	běžné
spodního	spodní	k2eAgNnSc2d1	spodní
prádla	prádlo	k1gNnSc2	prádlo
<g/>
.	.	kIx.	.
</s>
<s>
Rozdílné	rozdílný	k2eAgInPc1d1	rozdílný
jsou	být	k5eAaImIp3nP	být
použité	použitý	k2eAgInPc1d1	použitý
materiály	materiál	k1gInPc1	materiál
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
reflektují	reflektovat	k5eAaImIp3nP	reflektovat
odlišné	odlišný	k2eAgInPc1d1	odlišný
nároky	nárok	k1gInPc1	nárok
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
plavky	plavka	k1gFnPc4	plavka
kladeny	kladen	k2eAgFnPc4d1	kladena
ve	v	k7c6	v
vodním	vodní	k2eAgNnSc6d1	vodní
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
<s>
Jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
prvních	první	k4xOgFnPc2	první
zmínek	zmínka	k1gFnPc2	zmínka
o	o	k7c6	o
dvoudílných	dvoudílný	k2eAgInPc6d1	dvoudílný
ženských	ženský	k2eAgInPc6d1	ženský
oděvech	oděv	k1gInPc6	oděv
používaných	používaný	k2eAgInPc6d1	používaný
pro	pro	k7c4	pro
atletické	atletický	k2eAgInPc4d1	atletický
účely	účel	k1gInPc4	účel
pochází	pocházet	k5eAaImIp3nS	pocházet
již	již	k6eAd1	již
z	z	k7c2	z
antického	antický	k2eAgNnSc2d1	antické
Řecka	Řecko	k1gNnSc2	Řecko
z	z	k7c2	z
období	období	k1gNnSc2	období
okolo	okolo	k7c2	okolo
roku	rok	k1gInSc2	rok
1400	[number]	k4	1400
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Ženy	žena	k1gFnPc1	žena
ve	v	k7c6	v
dvoudílném	dvoudílný	k2eAgMnSc6d1	dvoudílný
"	"	kIx"	"
<g/>
sportovním	sportovní	k2eAgInSc6d1	sportovní
<g/>
"	"	kIx"	"
obleku	oblek	k1gInSc6	oblek
jsou	být	k5eAaImIp3nP	být
zobrazeny	zobrazit	k5eAaPmNgInP	zobrazit
také	také	k9	také
na	na	k7c6	na
řadě	řada	k1gFnSc6	řada
římských	římský	k2eAgFnPc2d1	římská
mozaik	mozaika	k1gFnPc2	mozaika
z	z	k7c2	z
3	[number]	k4	3
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
<g/>
,	,	kIx,	,
odkrytých	odkrytý	k2eAgFnPc2d1	odkrytá
při	při	k7c6	při
výzkumu	výzkum	k1gInSc6	výzkum
římské	římský	k2eAgFnSc2d1	římská
vily	vila	k1gFnSc2	vila
nedaleko	nedaleko	k7c2	nedaleko
města	město	k1gNnSc2	město
Piazza	Piazz	k1gMnSc2	Piazz
Armerina	Armerin	k1gMnSc2	Armerin
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
vynálezce	vynálezce	k1gMnSc2	vynálezce
bikin	bikiny	k1gFnPc2	bikiny
v	v	k7c6	v
moderním	moderní	k2eAgNnSc6d1	moderní
pojetí	pojetí	k1gNnSc6	pojetí
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
dvojice	dvojice	k1gFnSc1	dvojice
Francouzů	Francouz	k1gMnPc2	Francouz
-	-	kIx~	-
inženýr	inženýr	k1gMnSc1	inženýr
Louis	Louis	k1gMnSc1	Louis
Réard	Réard	k1gMnSc1	Réard
(	(	kIx(	(
<g/>
1897	[number]	k4	1897
<g/>
–	–	k?	–
<g/>
1984	[number]	k4	1984
<g/>
)	)	kIx)	)
a	a	k8xC	a
návrhář	návrhář	k1gMnSc1	návrhář
Jacques	Jacques	k1gMnSc1	Jacques
Heim	Heim	k1gMnSc1	Heim
(	(	kIx(	(
<g/>
1899	[number]	k4	1899
<g/>
–	–	k?	–
<g/>
1967	[number]	k4	1967
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
první	první	k4xOgMnSc1	první
představil	představit	k5eAaPmAgInS	představit
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
1946	[number]	k4	1946
v	v	k7c6	v
Cannes	Cannes	k1gNnSc6	Cannes
svoje	svůj	k3xOyFgFnPc4	svůj
plavky	plavka	k1gFnPc4	plavka
s	s	k7c7	s
názvem	název	k1gInSc7	název
"	"	kIx"	"
<g/>
Atome	atom	k1gInSc5	atom
<g/>
"	"	kIx"	"
a	a	k8xC	a
sloganem	slogan	k1gInSc7	slogan
"	"	kIx"	"
<g/>
nejmenší	malý	k2eAgFnPc4d3	nejmenší
plavky	plavka	k1gFnPc4	plavka
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
"	"	kIx"	"
Jacques	Jacques	k1gMnSc1	Jacques
Heim	Heim	k1gMnSc1	Heim
<g/>
.	.	kIx.	.
</s>
<s>
Známější	známý	k2eAgFnSc1d2	známější
je	být	k5eAaImIp3nS	být
však	však	k9	však
prezentace	prezentace	k1gFnSc1	prezentace
ještě	ještě	k6eAd1	ještě
menšího	malý	k2eAgInSc2d2	menší
modelu	model	k1gInSc2	model
inženýra	inženýr	k1gMnSc4	inženýr
Réarda	Réard	k1gMnSc4	Réard
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
představil	představit	k5eAaPmAgMnS	představit
svojí	svojit	k5eAaImIp3nS	svojit
verzi	verze	k1gFnSc4	verze
plavek	plavka	k1gFnPc2	plavka
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1946	[number]	k4	1946
na	na	k7c6	na
módní	módní	k2eAgFnSc6d1	módní
přehlídce	přehlídka	k1gFnSc6	přehlídka
v	v	k7c6	v
pařížských	pařížský	k2eAgFnPc6d1	Pařížská
lázních	lázeň	k1gFnPc6	lázeň
Piscine	Piscin	k1gInSc5	Piscin
Molitor	Molitor	k1gInSc1	Molitor
se	s	k7c7	s
sloganem	slogan	k1gInSc7	slogan
"	"	kIx"	"
<g/>
bikiny	bikiny	k1gFnPc1	bikiny
-	-	kIx~	-
menší	malý	k2eAgFnPc1d2	menší
než	než	k8xS	než
nejmenší	malý	k2eAgFnPc1d3	nejmenší
plavky	plavka	k1gFnPc1	plavka
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
model	model	k1gInSc4	model
s	s	k7c7	s
nevyztuženými	vyztužený	k2eNgInPc7d1	nevyztužený
trojúhelníkovými	trojúhelníkový	k2eAgInPc7d1	trojúhelníkový
díly	díl	k1gInPc7	díl
podprsenky	podprsenka	k1gFnSc2	podprsenka
a	a	k8xC	a
string	string	k1gInSc1	string
(	(	kIx(	(
<g/>
šňůrkovými	šňůrkový	k2eAgFnPc7d1	šňůrková
<g/>
)	)	kIx)	)
kalhotkami	kalhotky	k1gFnPc7	kalhotky
se	s	k7c7	s
zadní	zadní	k2eAgFnSc7d1	zadní
částí	část	k1gFnSc7	část
připomínající	připomínající	k2eAgInSc1d1	připomínající
styl	styl	k1gInSc1	styl
tanga	tango	k1gNnSc2	tango
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc4	tento
plavky	plavka	k1gFnPc4	plavka
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
podle	podle	k7c2	podle
atolu	atol	k1gInSc2	atol
Bikini	Bikini	k1gNnSc2	Bikini
<g/>
,	,	kIx,	,
části	část	k1gFnSc2	část
Marshallových	Marshallův	k2eAgInPc2d1	Marshallův
ostrovů	ostrov	k1gInPc2	ostrov
v	v	k7c6	v
Tichém	tichý	k2eAgInSc6d1	tichý
oceánu	oceán	k1gInSc6	oceán
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
čtyři	čtyři	k4xCgInPc4	čtyři
dny	den	k1gInPc4	den
před	před	k7c7	před
přehlídkou	přehlídka	k1gFnSc7	přehlídka
proběhl	proběhnout	k5eAaPmAgMnS	proběhnout
americký	americký	k2eAgInSc4d1	americký
jaderný	jaderný	k2eAgInSc4d1	jaderný
pokus	pokus	k1gInSc4	pokus
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
měl	mít	k5eAaImAgInS	mít
symbolizovat	symbolizovat	k5eAaImF	symbolizovat
"	"	kIx"	"
<g/>
atomový	atomový	k2eAgInSc1d1	atomový
výbuch	výbuch	k1gInSc1	výbuch
<g/>
"	"	kIx"	"
ve	v	k7c6	v
světě	svět	k1gInSc6	svět
dámské	dámský	k2eAgFnSc2d1	dámská
plážové	plážový	k2eAgFnSc2d1	plážová
módy	móda	k1gFnSc2	móda
a	a	k8xC	a
v	v	k7c6	v
přeneseném	přenesený	k2eAgInSc6d1	přenesený
slova	slovo	k1gNnSc2	slovo
smyslu	smysl	k1gInSc6	smysl
i	i	k8xC	i
množství	množství	k1gNnSc6	množství
použitého	použitý	k2eAgInSc2d1	použitý
materiálu	materiál	k1gInSc2	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
střihu	střih	k1gInSc3	střih
plavek	plavka	k1gFnPc2	plavka
nenalezl	naleznout	k5eNaPmAgMnS	naleznout
Louis	Louis	k1gMnSc1	Louis
Réard	Réard	k1gMnSc1	Réard
žádnou	žádný	k3yNgFnSc4	žádný
modelku	modelka	k1gFnSc4	modelka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
by	by	kYmCp3nP	by
nové	nový	k2eAgFnPc4d1	nová
bikiny	bikiny	k1gFnPc4	bikiny
chtěla	chtít	k5eAaImAgFnS	chtít
předvádět	předvádět	k5eAaImF	předvádět
<g/>
.	.	kIx.	.
</s>
<s>
Bikiny	bikiny	k1gFnPc1	bikiny
proto	proto	k8xC	proto
v	v	k7c6	v
lázních	lázeň	k1gFnPc6	lázeň
Molitor	Molitor	k1gInSc4	Molitor
předvedla	předvést	k5eAaPmAgFnS	předvést
striptýzová	striptýzový	k2eAgFnSc1d1	striptýzová
tanečnice	tanečnice	k1gFnSc1	tanečnice
Micheline	Michelin	k1gInSc5	Michelin
Bernardiniová	Bernardiniový	k2eAgFnSc5d1	Bernardiniový
<g/>
.	.	kIx.	.
</s>
<s>
Představení	představení	k1gNnSc1	představení
nových	nový	k2eAgFnPc2d1	nová
plavek	plavka	k1gFnPc2	plavka
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
mezi	mezi	k7c7	mezi
novináři	novinář	k1gMnPc7	novinář
i	i	k8xC	i
diváky	divák	k1gMnPc7	divák
nebývalý	bývalý	k2eNgInSc4d1	bývalý
rozruch	rozruch	k1gInSc4	rozruch
a	a	k8xC	a
vlnu	vlna	k1gFnSc4	vlna
kritiky	kritika	k1gFnSc2	kritika
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
40	[number]	k4	40
<g/>
.	.	kIx.	.
let	let	k1gInSc1	let
byly	být	k5eAaImAgFnP	být
bikiny	bikiny	k1gFnPc1	bikiny
poněkud	poněkud	k6eAd1	poněkud
stranou	stranou	k6eAd1	stranou
zájmu	zájem	k1gInSc2	zájem
světové	světový	k2eAgFnSc2d1	světová
módy	móda	k1gFnSc2	móda
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc2	jejich
rozšíření	rozšíření	k1gNnSc2	rozšíření
bránily	bránit	k5eAaImAgFnP	bránit
i	i	k9	i
zákazy	zákaz	k1gInPc4	zákaz
jejich	jejich	k3xOp3gNnSc2	jejich
nošení	nošení	k1gNnSc2	nošení
na	na	k7c6	na
plážích	pláž	k1gFnPc6	pláž
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
platily	platit	k5eAaImAgFnP	platit
např.	např.	kA	např.
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
,	,	kIx,	,
Portugalsku	Portugalsko	k1gNnSc6	Portugalsko
nebo	nebo	k8xC	nebo
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
50	[number]	k4	50
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
byly	být	k5eAaImAgFnP	být
bikiny	bikiny	k1gFnPc1	bikiny
dokonce	dokonce	k9	dokonce
vyloučeny	vyloučit	k5eAaPmNgFnP	vyloučit
ze	z	k7c2	z
soutěže	soutěž	k1gFnSc2	soutěž
Miss	miss	k1gFnSc1	miss
World	World	k1gMnSc1	World
-	-	kIx~	-
údajně	údajně	k6eAd1	údajně
proto	proto	k8xC	proto
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
nezvýhodňovaly	zvýhodňovat	k5eNaImAgFnP	zvýhodňovat
odvážnější	odvážný	k2eAgFnPc1d2	odvážnější
soutěžící	soutěžící	k1gFnPc1	soutěžící
před	před	k7c7	před
těmi	ten	k3xDgMnPc7	ten
stydlivějšími	stydlivý	k2eAgMnPc7d2	stydlivější
<g/>
.	.	kIx.	.
</s>
<s>
Přelomovou	přelomový	k2eAgFnSc7d1	přelomová
událostí	událost	k1gFnSc7	událost
pro	pro	k7c4	pro
masivní	masivní	k2eAgNnSc4d1	masivní
rozšíření	rozšíření	k1gNnSc4	rozšíření
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
plavek	plavka	k1gFnPc2	plavka
nejenom	nejenom	k6eAd1	nejenom
ve	v	k7c6	v
Spojených	spojený	k2eAgInPc6d1	spojený
státech	stát	k1gInPc6	stát
amerických	americký	k2eAgInPc6d1	americký
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1957	[number]	k4	1957
natočení	natočení	k1gNnSc1	natočení
filmu	film	k1gInSc2	film
"	"	kIx"	"
<g/>
A	a	k9	a
bůh	bůh	k1gMnSc1	bůh
stvořil	stvořit	k5eAaPmAgMnS	stvořit
ženu	žena	k1gFnSc4	žena
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
v	v	k7c6	v
bikinách	bikiny	k1gFnPc6	bikiny
ukázala	ukázat	k5eAaPmAgFnS	ukázat
francouzská	francouzský	k2eAgFnSc1d1	francouzská
filmová	filmový	k2eAgFnSc1d1	filmová
hvězda	hvězda	k1gFnSc1	hvězda
Brigitte	Brigitte	k1gFnSc2	Brigitte
Bardotová	Bardotový	k2eAgFnSc1d1	Bardotová
<g/>
.	.	kIx.	.
</s>
<s>
Podobný	podobný	k2eAgInSc4d1	podobný
účinek	účinek	k1gInSc4	účinek
měla	mít	k5eAaImAgFnS	mít
i	i	k9	i
scéna	scéna	k1gFnSc1	scéna
z	z	k7c2	z
"	"	kIx"	"
<g/>
bondovky	bondovky	k?	bondovky
<g/>
"	"	kIx"	"
'	'	kIx"	'
<g/>
"	"	kIx"	"
<g/>
Doktor	doktor	k1gMnSc1	doktor
No	no	k9	no
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1962	[number]	k4	1962
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
Ursula	Ursula	k1gFnSc1	Ursula
Andressová	Andressová	k1gFnSc1	Andressová
vystupuje	vystupovat	k5eAaImIp3nS	vystupovat
z	z	k7c2	z
moře	moře	k1gNnSc2	moře
v	v	k7c6	v
bílých	bílý	k2eAgFnPc6d1	bílá
bikinách	bikiny	k1gFnPc6	bikiny
s	s	k7c7	s
nožem	nůž	k1gInSc7	nůž
za	za	k7c7	za
opaskem	opasek	k1gInSc7	opasek
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
scény	scéna	k1gFnPc1	scéna
z	z	k7c2	z
amerického	americký	k2eAgInSc2d1	americký
filmu	film	k1gInSc2	film
"	"	kIx"	"
<g/>
Beach	Beach	k1gInSc1	Beach
Party	parta	k1gFnSc2	parta
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1963	[number]	k4	1963
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
se	se	k3xPyFc4	se
móda	móda	k1gFnSc1	móda
bikin	bikiny	k1gFnPc2	bikiny
postupně	postupně	k6eAd1	postupně
rozšiřuje	rozšiřovat	k5eAaImIp3nS	rozšiřovat
téměř	téměř	k6eAd1	téměř
do	do	k7c2	do
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
a	a	k8xC	a
pro	pro	k7c4	pro
tvůrce	tvůrce	k1gMnSc4	tvůrce
i	i	k9	i
výrobce	výrobce	k1gMnSc1	výrobce
se	se	k3xPyFc4	se
bikiny	bikiny	k1gFnPc1	bikiny
stávají	stávat	k5eAaImIp3nP	stávat
zajímavým	zajímavý	k2eAgInSc7d1	zajímavý
obchodním	obchodní	k2eAgInSc7d1	obchodní
artiklem	artikl	k1gInSc7	artikl
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1964	[number]	k4	1964
přišel	přijít	k5eAaPmAgMnS	přijít
Američan	Američan	k1gMnSc1	Američan
rakouského	rakouský	k2eAgInSc2d1	rakouský
původu	původ	k1gInSc2	původ
Rudi	ruď	k1gFnSc2	ruď
Gernreich	Gernreicha	k1gFnPc2	Gernreicha
s	s	k7c7	s
prvními	první	k4xOgFnPc7	první
monokinami	monokina	k1gFnPc7	monokina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
zcela	zcela	k6eAd1	zcela
postrádaly	postrádat	k5eAaImAgFnP	postrádat
horní	horní	k2eAgInSc4d1	horní
díl	díl	k1gInSc4	díl
<g/>
,	,	kIx,	,
patřily	patřit	k5eAaImAgInP	patřit
střihy	střih	k1gInPc4	střih
bikin	bikiny	k1gFnPc2	bikiny
z	z	k7c2	z
období	období	k1gNnPc2	období
50	[number]	k4	50
<g/>
.	.	kIx.	.
a	a	k8xC	a
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
k	k	k7c3	k
nejkonzervativnějším	konzervativní	k2eAgFnPc3d3	nejkonzervativnější
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
70	[number]	k4	70
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
dochazí	dochazit	k5eAaPmIp3nS	dochazit
k	k	k7c3	k
postupné	postupný	k2eAgFnSc3d1	postupná
změně	změna	k1gFnSc3	změna
střihů	střih	k1gInPc2	střih
i	i	k8xC	i
použitých	použitý	k2eAgInPc2d1	použitý
materiálů	materiál	k1gInPc2	materiál
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
především	především	k6eAd1	především
polyamidová	polyamidový	k2eAgNnPc1d1	polyamidové
<g/>
,	,	kIx,	,
polyesterová	polyesterový	k2eAgNnPc1d1	polyesterové
a	a	k8xC	a
polyuretanová	polyuretanový	k2eAgNnPc1d1	polyuretanové
vlákna	vlákno	k1gNnPc1	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Populárními	populární	k2eAgInPc7d1	populární
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
string	string	k1gInSc4	string
bikiny	bikiny	k1gFnPc1	bikiny
trojúhelníkového	trojúhelníkový	k2eAgInSc2d1	trojúhelníkový
tvaru	tvar	k1gInSc2	tvar
s	s	k7c7	s
úzkým	úzký	k2eAgInSc7d1	úzký
nebo	nebo	k8xC	nebo
šňůrkovým	šňůrkový	k2eAgInSc7d1	šňůrkový
bokem	bok	k1gInSc7	bok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
dekády	dekáda	k1gFnSc2	dekáda
se	se	k3xPyFc4	se
v	v	k7c6	v
Brazílii	Brazílie	k1gFnSc6	Brazílie
objevují	objevovat	k5eAaImIp3nP	objevovat
první	první	k4xOgNnPc1	první
moderní	moderní	k2eAgNnPc1d1	moderní
tanga	tango	k1gNnPc1	tango
-	-	kIx~	-
plavky	plavka	k1gFnSc2	plavka
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
vznik	vznik	k1gInSc1	vznik
byl	být	k5eAaImAgInS	být
údajně	údajně	k6eAd1	údajně
inspirován	inspirován	k2eAgInSc1d1	inspirován
tradičním	tradiční	k2eAgNnSc7d1	tradiční
oblečením	oblečení	k1gNnSc7	oblečení
indiánských	indiánský	k2eAgInPc2d1	indiánský
kmenů	kmen	k1gInPc2	kmen
v	v	k7c6	v
brazilské	brazilský	k2eAgFnSc6d1	brazilská
Amazonii	Amazonie	k1gFnSc6	Amazonie
<g/>
.	.	kIx.	.
</s>
<s>
Vývoj	vývoj	k1gInSc1	vývoj
bikin	bikiny	k1gFnPc2	bikiny
v	v	k7c6	v
období	období	k1gNnSc6	období
80	[number]	k4	80
<g/>
.	.	kIx.	.
a	a	k8xC	a
počátku	počátek	k1gInSc2	počátek
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
byl	být	k5eAaImAgInS	být
poněkud	poněkud	k6eAd1	poněkud
utlumen	utlumit	k5eAaPmNgInS	utlumit
nástupem	nástup	k1gInSc7	nástup
módy	móda	k1gFnSc2	móda
jednodílných	jednodílný	k2eAgFnPc2d1	jednodílná
plavek	plavka	k1gFnPc2	plavka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
reflektovala	reflektovat	k5eAaImAgFnS	reflektovat
tehdejší	tehdejší	k2eAgInSc4d1	tehdejší
rozmach	rozmach	k1gInSc4	rozmach
aerobiku	aerobic	k1gInSc2	aerobic
a	a	k8xC	a
rekreačního	rekreační	k2eAgInSc2d1	rekreační
sportu	sport	k1gInSc2	sport
obecně	obecně	k6eAd1	obecně
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
období	období	k1gNnSc1	období
však	však	k9	však
položilo	položit	k5eAaPmAgNnS	položit
základ	základ	k1gInSc4	základ
obliby	obliba	k1gFnSc2	obliba
široké	široký	k2eAgFnSc2d1	široká
škály	škála	k1gFnSc2	škála
střihů	střih	k1gInPc2	střih
od	od	k7c2	od
velmi	velmi	k6eAd1	velmi
konzervativních	konzervativní	k2eAgNnPc2d1	konzervativní
až	až	k9	až
po	po	k7c4	po
velmi	velmi	k6eAd1	velmi
odvážné	odvážný	k2eAgInPc4d1	odvážný
střihy	střih	k1gInPc4	střih
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgInPc2	jenž
se	se	k3xPyFc4	se
v	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
vyvinuly	vyvinout	k5eAaPmAgInP	vyvinout
tzv.	tzv.	kA	tzv.
mikrokiny	mikrokin	k2eAgInPc1d1	mikrokin
jež	jenž	k3xRgFnSc1	jenž
jsou	být	k5eAaImIp3nP	být
charakteristické	charakteristický	k2eAgFnPc1d1	charakteristická
použitím	použití	k1gNnSc7	použití
minimálního	minimální	k2eAgNnSc2d1	minimální
množství	množství	k1gNnSc2	množství
materiálu	materiál	k1gInSc2	materiál
a	a	k8xC	a
využitím	využití	k1gNnSc7	využití
mnoha	mnoho	k4c2	mnoho
typů	typ	k1gInPc2	typ
moderních	moderní	k2eAgNnPc2d1	moderní
umělých	umělý	k2eAgNnPc2d1	umělé
vláken	vlákno	k1gNnPc2	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Počátek	počátek	k1gInSc1	počátek
první	první	k4xOgFnSc2	první
dekády	dekáda	k1gFnSc2	dekáda
21	[number]	k4	21
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
znamená	znamenat	k5eAaImIp3nS	znamenat
nový	nový	k2eAgInSc1d1	nový
rozmach	rozmach	k1gInSc1	rozmach
vývoje	vývoj	k1gInSc2	vývoj
těchto	tento	k3xDgFnPc2	tento
plavek	plavka	k1gFnPc2	plavka
a	a	k8xC	a
bikiny	bikiny	k1gFnPc1	bikiny
dominují	dominovat	k5eAaImIp3nP	dominovat
dámské	dámský	k2eAgFnSc3d1	dámská
plážové	plážový	k2eAgFnSc3d1	plážová
módě	móda	k1gFnSc3	móda
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
soudobé	soudobý	k2eAgInPc4d1	soudobý
trendy	trend	k1gInPc4	trend
patří	patřit	k5eAaImIp3nS	patřit
např.	např.	kA	např.
retro	retro	k1gNnSc4	retro
styl	styl	k1gInSc1	styl
60	[number]	k4	60
<g/>
.	.	kIx.	.
a	a	k8xC	a
70	[number]	k4	70
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
;	;	kIx,	;
použití	použití	k1gNnSc1	použití
flitrů	flitr	k1gInPc2	flitr
<g/>
,	,	kIx,	,
stříbra	stříbro	k1gNnSc2	stříbro
<g/>
,	,	kIx,	,
zlata	zlato	k1gNnSc2	zlato
a	a	k8xC	a
nových	nový	k2eAgInPc2d1	nový
způsobů	způsob	k1gInPc2	způsob
potisků	potisk	k1gInPc2	potisk
<g/>
;	;	kIx,	;
objevují	objevovat	k5eAaImIp3nP	objevovat
se	se	k3xPyFc4	se
nové	nový	k2eAgFnPc1d1	nová
barevné	barevný	k2eAgFnPc1d1	barevná
variace	variace	k1gFnPc1	variace
a	a	k8xC	a
materiály	materiál	k1gInPc1	materiál
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
umožňují	umožňovat	k5eAaImIp3nP	umožňovat
např.	např.	kA	např.
výrobu	výroba	k1gFnSc4	výroba
velmi	velmi	k6eAd1	velmi
tenkých	tenký	k2eAgFnPc2d1	tenká
(	(	kIx(	(
<g/>
téměř	téměř	k6eAd1	téměř
průhledných	průhledný	k2eAgFnPc2d1	průhledná
<g/>
)	)	kIx)	)
bikin	bikiny	k1gFnPc2	bikiny
nebo	nebo	k8xC	nebo
tkanin	tkanina	k1gFnPc2	tkanina
propouštějících	propouštějící	k2eAgInPc2d1	propouštějící
UV	UV	kA	UV
záření	záření	k1gNnSc2	záření
<g/>
.	.	kIx.	.
</s>
<s>
Nabídka	nabídka	k1gFnSc1	nabídka
některých	některý	k3yIgMnPc2	některý
výrobců	výrobce	k1gMnPc2	výrobce
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
také	také	k9	také
plavky	plavka	k1gFnPc4	plavka
vyrobené	vyrobený	k2eAgFnPc4d1	vyrobená
z	z	k7c2	z
neoprenu	neopren	k1gInSc2	neopren
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
modely	model	k1gInPc4	model
háčkované	háčkovaný	k2eAgInPc4d1	háčkovaný
z	z	k7c2	z
bavlny	bavlna	k1gFnSc2	bavlna
a	a	k8xC	a
polyamidových	polyamidový	k2eAgNnPc2d1	polyamidové
vláken	vlákno	k1gNnPc2	vlákno
<g/>
.	.	kIx.	.
</s>
<s>
Monokiny	Monokina	k1gFnPc1	Monokina
-	-	kIx~	-
Jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
pouze	pouze	k6eAd1	pouze
spodním	spodní	k2eAgNnSc7d1	spodní
dílem	dílo	k1gNnSc7	dílo
plavek	plavka	k1gFnPc2	plavka
<g/>
,	,	kIx,	,
obvykle	obvykle	k6eAd1	obvykle
typu	typa	k1gFnSc4	typa
tanga	tango	k1gNnSc2	tango
nebo	nebo	k8xC	nebo
string	string	k1gInSc1	string
(	(	kIx(	(
<g/>
jejich	jejich	k3xOp3gFnSc1	jejich
inovativní	inovativní	k2eAgFnSc1d1	inovativní
varianta	varianta	k1gFnSc1	varianta
se	se	k3xPyFc4	se
jmenuje	jmenovat	k5eAaBmIp3nS	jmenovat
krakaténky	krakatének	k1gInPc7	krakatének
a	a	k8xC	a
inspirovali	inspirovat	k5eAaBmAgMnP	inspirovat
ji	on	k3xPp3gFnSc4	on
čeští	český	k2eAgMnPc1d1	český
kreativci	kreativec	k1gMnPc1	kreativec
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tankiny	Tankina	k1gFnPc1	Tankina
-	-	kIx~	-
Jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgFnP	tvořit
spodním	spodní	k2eAgNnSc7d1	spodní
dílem	dílo	k1gNnSc7	dílo
plavek	plavka	k1gFnPc2	plavka
a	a	k8xC	a
horním	horní	k2eAgInSc7d1	horní
dílem	díl	k1gInSc7	díl
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
oproti	oproti	k7c3	oproti
běžným	běžný	k2eAgFnPc3d1	běžná
bikinám	bikiny	k1gFnPc3	bikiny
pod	pod	k7c7	pod
košíčky	košíček	k1gInPc7	košíček
rozšířený	rozšířený	k2eAgInSc4d1	rozšířený
o	o	k7c4	o
pruh	pruh	k1gInSc4	pruh
plavkové	plavkový	k2eAgFnSc2d1	plavková
tkaniny	tkanina	k1gFnSc2	tkanina
různé	různý	k2eAgFnSc2d1	různá
šířky	šířka	k1gFnSc2	šířka
dosahující	dosahující	k2eAgFnPc1d1	dosahující
někdy	někdy	k6eAd1	někdy
až	až	k9	až
k	k	k7c3	k
úrovni	úroveň	k1gFnSc3	úroveň
břicha	břicho	k1gNnSc2	břicho
<g/>
.	.	kIx.	.
</s>
<s>
String	String	k1gInSc4	String
bikiny	bikiny	k1gFnPc4	bikiny
-	-	kIx~	-
Pro	pro	k7c4	pro
spodní	spodní	k2eAgInSc4d1	spodní
i	i	k8xC	i
horní	horní	k2eAgInSc4d1	horní
díl	díl	k1gInSc4	díl
plavek	plavka	k1gFnPc2	plavka
je	být	k5eAaImIp3nS	být
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
použití	použití	k1gNnSc1	použití
úzkých	úzký	k2eAgFnPc2d1	úzká
pruženek	pruženka	k1gFnPc2	pruženka
(	(	kIx(	(
<g/>
pružných	pružný	k2eAgInPc2d1	pružný
pásků	pásek	k1gInPc2	pásek
obšitých	obšitý	k2eAgInPc2d1	obšitý
plavkovou	plavkový	k2eAgFnSc7d1	plavková
tkaninou	tkanina	k1gFnSc7	tkanina
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zejména	zejména	k9	zejména
u	u	k7c2	u
zavazování	zavazování	k1gNnSc2	zavazování
horního	horní	k2eAgInSc2d1	horní
dílu	díl	k1gInSc2	díl
a	a	k8xC	a
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
kolem	kolem	k7c2	kolem
pasu	pas	k1gInSc2	pas
u	u	k7c2	u
spodního	spodní	k2eAgInSc2d1	spodní
dílu	díl	k1gInSc2	díl
plavek	plavka	k1gFnPc2	plavka
<g/>
.	.	kIx.	.
</s>
<s>
Součásti	součást	k1gFnPc1	součást
obou	dva	k4xCgInPc2	dva
dílů	díl	k1gInPc2	díl
jsou	být	k5eAaImIp3nP	být
většinou	většinou	k6eAd1	většinou
tvarovány	tvarovat	k5eAaImNgInP	tvarovat
do	do	k7c2	do
podoby	podoba	k1gFnSc2	podoba
trojúhelníků	trojúhelník	k1gInPc2	trojúhelník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
setkáme	setkat	k5eAaPmIp1nP	setkat
s	s	k7c7	s
označením	označení	k1gNnSc7	označení
"	"	kIx"	"
<g/>
šňůrkové	šňůrkový	k2eAgFnPc4d1	šňůrková
<g/>
"	"	kIx"	"
bikiny	bikiny	k1gFnPc4	bikiny
(	(	kIx(	(
<g/>
plavky	plavka	k1gFnPc4	plavka
<g/>
)	)	kIx)	)
Tanga	tango	k1gNnSc2	tango
-	-	kIx~	-
Spodní	spodní	k2eAgInSc4d1	spodní
díl	díl	k1gInSc4	díl
plavek	plavka	k1gFnPc2	plavka
tvořený	tvořený	k2eAgInSc4d1	tvořený
v	v	k7c6	v
zadní	zadní	k2eAgFnSc6d1	zadní
části	část	k1gFnSc6	část
pouze	pouze	k6eAd1	pouze
malým	malý	k2eAgInSc7d1	malý
trojúhelníkem	trojúhelník	k1gInSc7	trojúhelník
tkaniny	tkanina	k1gFnSc2	tkanina
<g/>
.	.	kIx.	.
</s>
<s>
Nezakrývá	zakrývat	k5eNaImIp3nS	zakrývat
hýždě	hýždě	k1gFnSc1	hýždě
a	a	k8xC	a
mezi	mezi	k7c7	mezi
pruženkami	pruženka	k1gFnPc7	pruženka
kolem	kolem	k7c2	kolem
pasu	pas	k1gInSc2	pas
i	i	k9	i
mezi	mezi	k7c7	mezi
hýžděmi	hýždě	k1gFnPc7	hýždě
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
pouze	pouze	k6eAd1	pouze
úzký	úzký	k2eAgInSc1d1	úzký
pruh	pruh	k1gInSc1	pruh
tkaniny	tkanina	k1gFnSc2	tkanina
nebo	nebo	k8xC	nebo
pružné	pružný	k2eAgFnSc2d1	pružná
šňůrky	šňůrka	k1gFnSc2	šňůrka
T-back	Tacka	k1gFnPc2	T-backa
-	-	kIx~	-
Spodní	spodní	k2eAgInSc1d1	spodní
díl	díl	k1gInSc1	díl
plavek	plavka	k1gFnPc2	plavka
je	být	k5eAaImIp3nS	být
podobný	podobný	k2eAgInSc1d1	podobný
typu	typ	k1gInSc3	typ
tanga	tango	k1gNnSc2	tango
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
bez	bez	k7c2	bez
trojúhelníku	trojúhelník	k1gInSc2	trojúhelník
plavkové	plavkový	k2eAgFnSc2d1	plavková
tkaniny	tkanina	k1gFnSc2	tkanina
v	v	k7c6	v
zadní	zadní	k2eAgFnSc6d1	zadní
části	část	k1gFnSc6	část
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
tvořena	tvořen	k2eAgNnPc4d1	tvořeno
pouze	pouze	k6eAd1	pouze
úzkou	úzký	k2eAgFnSc7d1	úzká
pruženkou	pruženka	k1gFnSc7	pruženka
kolem	kolem	k7c2	kolem
pasu	pas	k1gInSc2	pas
a	a	k8xC	a
na	na	k7c6	na
ní	on	k3xPp3gFnSc7	on
navazující	navazující	k2eAgFnSc7d1	navazující
pruženkou	pruženka	k1gFnSc7	pruženka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
mezi	mezi	k7c7	mezi
hýžděmi	hýždě	k1gFnPc7	hýždě
spojuje	spojovat	k5eAaImIp3nS	spojovat
zadní	zadní	k2eAgFnSc1d1	zadní
část	část	k1gFnSc1	část
s	s	k7c7	s
předním	přední	k2eAgInSc7d1	přední
trojúhelníkovým	trojúhelníkový	k2eAgInSc7d1	trojúhelníkový
dílem	díl	k1gInSc7	díl
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
těchto	tento	k3xDgFnPc2	tento
pruženek	pruženka	k1gFnPc2	pruženka
připomíná	připomínat	k5eAaImIp3nS	připomínat
v	v	k7c6	v
zadní	zadní	k2eAgFnSc6d1	zadní
části	část	k1gFnSc6	část
písmeno	písmeno	k1gNnSc1	písmeno
T.	T.	kA	T.
V-kiny	Vina	k1gFnSc2	V-kina
(	(	kIx(	(
<g/>
výslovnost	výslovnost	k1gFnSc1	výslovnost
<g/>
:	:	kIx,	:
ví	vědět	k5eAaImIp3nS	vědět
kiny	kino	k1gNnPc7	kino
<g/>
)	)	kIx)	)
-	-	kIx~	-
Spodní	spodní	k2eAgInSc1d1	spodní
díl	díl	k1gInSc1	díl
plavek	plavka	k1gFnPc2	plavka
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc1	jehož
přední	přední	k2eAgInSc1d1	přední
díl	díl	k1gInSc1	díl
připomíná	připomínat	k5eAaImIp3nS	připomínat
svým	svůj	k3xOyFgInSc7	svůj
tvarem	tvar	k1gInSc7	tvar
písmeno	písmeno	k1gNnSc4	písmeno
V.	V.	kA	V.
Někdy	někdy	k6eAd1	někdy
bývá	bývat	k5eAaImIp3nS	bývat
tímto	tento	k3xDgInSc7	tento
termínem	termín	k1gInSc7	termín
označen	označit	k5eAaPmNgInS	označit
i	i	k9	i
G-string	Gtring	k1gInSc1	G-string
díl	díl	k1gInSc1	díl
<g/>
,	,	kIx,	,
u	u	k7c2	u
nějž	jenž	k3xRgNnSc2	jenž
spojení	spojení	k1gNnSc2	spojení
pruženek	pruženka	k1gFnPc2	pruženka
v	v	k7c6	v
zadní	zadní	k2eAgFnSc6d1	zadní
části	část	k1gFnSc6	část
připomíná	připomínat	k5eAaImIp3nS	připomínat
písmeno	písmeno	k1gNnSc1	písmeno
V.	V.	kA	V.
Mikrokiny	Mikrokina	k1gFnSc2	Mikrokina
-	-	kIx~	-
Bikiny	bikiny	k1gFnPc1	bikiny
s	s	k7c7	s
minimálním	minimální	k2eAgNnSc7d1	minimální
využitím	využití	k1gNnSc7	využití
plavkové	plavkový	k2eAgFnSc2d1	plavková
tkaniny	tkanina	k1gFnSc2	tkanina
<g/>
.	.	kIx.	.
</s>
<s>
Obvykle	obvykle	k6eAd1	obvykle
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
horní	horní	k2eAgInSc4d1	horní
díl	díl	k1gInSc4	díl
s	s	k7c7	s
nevyztuženými	vyztužený	k2eNgInPc7d1	nevyztužený
trojúhelníkovými	trojúhelníkový	k2eAgInPc7d1	trojúhelníkový
košíčky	košíček	k1gInPc7	košíček
typu	typ	k1gInSc2	typ
string	string	k1gInSc1	string
a	a	k8xC	a
spodní	spodní	k2eAgInSc4d1	spodní
díl	díl	k1gInSc4	díl
typu	typ	k1gInSc2	typ
G-string	Gtring	k1gInSc1	G-string
nebo	nebo	k8xC	nebo
V-kini	Vin	k1gMnPc1	V-kin
<g/>
.	.	kIx.	.
</s>
<s>
Šortky	šortky	k1gFnPc1	šortky
-	-	kIx~	-
Spodní	spodní	k2eAgFnSc1d1	spodní
část	část	k1gFnSc1	část
plavek	plavka	k1gFnPc2	plavka
připomíná	připomínat	k5eAaImIp3nS	připomínat
velmi	velmi	k6eAd1	velmi
krátké	krátký	k2eAgFnPc4d1	krátká
chlapecké	chlapecký	k2eAgFnPc4d1	chlapecká
kalhoty	kalhoty	k1gFnPc4	kalhoty
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
šortky	šortky	k1gFnPc1	šortky
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
pro	pro	k7c4	pro
šití	šití	k1gNnSc4	šití
plavek	plavka	k1gFnPc2	plavka
typu	typ	k1gInSc2	typ
bikiny	bikiny	k1gFnPc1	bikiny
používají	používat	k5eAaImIp3nP	používat
především	především	k6eAd1	především
materiály	materiál	k1gInPc4	materiál
z	z	k7c2	z
umělých	umělý	k2eAgNnPc2d1	umělé
vláken	vlákno	k1gNnPc2	vlákno
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInSc1	jejichž
základ	základ	k1gInSc1	základ
tvoří	tvořit	k5eAaImIp3nS	tvořit
polyamid	polyamid	k1gInSc1	polyamid
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Nylon	nylon	k1gInSc1	nylon
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
polyester	polyester	k1gInSc1	polyester
nebo	nebo	k8xC	nebo
polyuretan	polyuretan	k1gInSc1	polyuretan
(	(	kIx(	(
<g/>
např.	např.	kA	např.
Lycra	lycra	k1gFnSc1	lycra
<g/>
,	,	kIx,	,
Spandex	Spandex	k1gInSc1	Spandex
<g/>
,	,	kIx,	,
Elastan	Elastan	k1gInSc1	Elastan
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
dochází	docházet	k5eAaImIp3nS	docházet
ke	k	k7c3	k
kombinaci	kombinace	k1gFnSc3	kombinace
těchto	tento	k3xDgInPc2	tento
typů	typ	k1gInPc2	typ
vláken	vlákna	k1gFnPc2	vlákna
–	–	k?	–
např.	např.	kA	např.
80	[number]	k4	80
<g/>
%	%	kIx~	%
polyamidu	polyamid	k1gInSc2	polyamid
a	a	k8xC	a
20	[number]	k4	20
<g/>
%	%	kIx~	%
vlákna	vlákna	k1gFnSc1	vlákna
Lycra	lycra	k1gFnSc1	lycra
apod.	apod.	kA	apod.
V	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
se	se	k3xPyFc4	se
používají	používat	k5eAaImIp3nP	používat
i	i	k9	i
vlákna	vlákna	k1gFnSc1	vlákna
odvozená	odvozený	k2eAgFnSc1d1	odvozená
ze	z	k7c2	z
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
bavlny	bavlna	k1gFnSc2	bavlna
či	či	k8xC	či
dřevnatých	dřevnatý	k2eAgFnPc2d1	dřevnatá
rostlin	rostlina	k1gFnPc2	rostlina
(	(	kIx(	(
<g/>
označené	označený	k2eAgNnSc1d1	označené
obchodním	obchodní	k2eAgInSc7d1	obchodní
názvem	název	k1gInSc7	název
Rayon	Rayona	k1gFnPc2	Rayona
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
bikiny	bikiny	k1gFnPc1	bikiny
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
bikiny	bikiny	k1gFnPc1	bikiny
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Bikiny	bikiny	k1gFnPc1	bikiny
–	–	k?	–
podrobný	podrobný	k2eAgInSc4d1	podrobný
historický	historický	k2eAgInSc4d1	historický
vývoj	vývoj	k1gInSc4	vývoj
</s>
