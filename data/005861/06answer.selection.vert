<s>
Slovo	slovo	k1gNnSc4	slovo
bikiny	bikiny	k1gFnPc1	bikiny
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
:	:	kIx,	:
bikini	bikin	k2eAgMnPc1d1	bikin
<g/>
)	)	kIx)	)
označuje	označovat	k5eAaImIp3nS	označovat
dvoudílné	dvoudílný	k2eAgFnPc4d1	dvoudílná
dámské	dámský	k2eAgFnPc4d1	dámská
plavky	plavka	k1gFnPc4	plavka
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
něž	jenž	k3xRgFnPc4	jenž
je	být	k5eAaImIp3nS	být
charakteristické	charakteristický	k2eAgNnSc1d1	charakteristické
rozdělení	rozdělení	k1gNnSc1	rozdělení
na	na	k7c4	na
horní	horní	k2eAgFnSc4d1	horní
část	část	k1gFnSc4	část
zakrývající	zakrývající	k2eAgInSc1d1	zakrývající
ňadra	ňadro	k1gNnSc2	ňadro
a	a	k8xC	a
spodní	spodní	k2eAgFnSc4d1	spodní
část	část	k1gFnSc4	část
zakrývající	zakrývající	k2eAgInSc4d1	zakrývající
klín	klín	k1gInSc4	klín
(	(	kIx(	(
<g/>
oblast	oblast	k1gFnSc4	oblast
třísel	tříslo	k1gNnPc2	tříslo
<g/>
)	)	kIx)	)
a	a	k8xC	a
zcela	zcela	k6eAd1	zcela
nebo	nebo	k8xC	nebo
jenom	jenom	k6eAd1	jenom
částečně	částečně	k6eAd1	částečně
hýždě	hýždě	k1gFnSc2	hýždě
<g/>
.	.	kIx.	.
</s>
