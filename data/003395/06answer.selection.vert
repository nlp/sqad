<s>
Ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
specializovanou	specializovaný	k2eAgFnSc7d1	specializovaná
vývojovou	vývojový	k2eAgFnSc7d1	vývojová
větví	větev	k1gFnSc7	větev
plazů	plaz	k1gInPc2	plaz
skupiny	skupina	k1gFnSc2	skupina
Archosauria	Archosaurium	k1gNnSc2	Archosaurium
<g/>
,	,	kIx,	,
od	od	k7c2	od
kterých	který	k3yIgInPc2	který
se	se	k3xPyFc4	se
odlišili	odlišit	k5eAaPmAgMnP	odlišit
schopností	schopnost	k1gFnSc7	schopnost
endotermní	endotermní	k2eAgFnSc2d1	endotermní
termoregulace	termoregulace	k1gFnSc2	termoregulace
(	(	kIx(	(
<g/>
homoitermie	homoitermie	k1gFnSc1	homoitermie
<g/>
)	)	kIx)	)
a	a	k8xC	a
vytvořením	vytvoření	k1gNnSc7	vytvoření
unikátního	unikátní	k2eAgInSc2d1	unikátní
způsobu	způsob	k1gInSc2	způsob
létání	létání	k1gNnSc2	létání
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
zcela	zcela	k6eAd1	zcela
nové	nový	k2eAgFnSc2d1	nová
struktury	struktura	k1gFnSc2	struktura
–	–	k?	–
peří	peří	k1gNnSc1	peří
<g/>
.	.	kIx.	.
</s>
