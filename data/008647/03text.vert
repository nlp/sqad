<p>
<s>
Pensionat	Pensionat	k5eAaImF	Pensionat
Oskar	Oskar	k1gMnSc1	Oskar
je	být	k5eAaImIp3nS	být
dánsko-švédský	dánsko-švédský	k2eAgInSc1d1	dánsko-švédský
hraný	hraný	k2eAgInSc1d1	hraný
film	film	k1gInSc1	film
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
<g/>
,	,	kIx,	,
který	který	k3yIgInSc4	který
režírovala	režírovat	k5eAaImAgFnS	režírovat
Susanne	Susann	k1gInSc5	Susann
Bierová	Bierový	k2eAgFnSc5d1	Bierový
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
popisuje	popisovat	k5eAaImIp3nS	popisovat
středostavovskou	středostavovský	k2eAgFnSc4d1	středostavovská
rodinu	rodina	k1gFnSc4	rodina
v	v	k7c6	v
krizi	krize	k1gFnSc6	krize
<g/>
.	.	kIx.	.
</s>
<s>
Snímek	snímek	k1gInSc1	snímek
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
ČR	ČR	kA	ČR
byl	být	k5eAaImAgInS	být
uveden	uvést	k5eAaPmNgInS	uvést
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2003	[number]	k4	2003
na	na	k7c6	na
filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
Febiofest	Febiofest	k1gInSc1	Febiofest
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Děj	děj	k1gInSc1	děj
==	==	k?	==
</s>
</p>
<p>
<s>
Rune	run	k1gInSc5	run
pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k8xS	jako
filmový	filmový	k2eAgMnSc1d1	filmový
producent	producent	k1gMnSc1	producent
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
rodinou	rodina	k1gFnSc7	rodina
žije	žít	k5eAaImIp3nS	žít
v	v	k7c6	v
domku	domek	k1gInSc6	domek
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
pocit	pocit	k1gInSc4	pocit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
životě	život	k1gInSc6	život
moc	moc	k6eAd1	moc
nedaří	dařit	k5eNaImIp3nS	dařit
<g/>
.	.	kIx.	.
</s>
<s>
Nestal	stát	k5eNaPmAgMnS	stát
se	se	k3xPyFc4	se
hercem	herc	k1gInSc7	herc
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
snil	snít	k5eAaImAgMnS	snít
<g/>
,	,	kIx,	,
v	v	k7c6	v
povýšení	povýšení	k1gNnSc6	povýšení
ho	on	k3xPp3gNnSc4	on
přeskočil	přeskočit	k5eAaPmAgMnS	přeskočit
mladší	mladý	k2eAgMnSc1d2	mladší
kolega	kolega	k1gMnSc1	kolega
<g/>
,	,	kIx,	,
s	s	k7c7	s
manželkou	manželka	k1gFnSc7	manželka
Gunnel	Gunnlo	k1gNnPc2	Gunnlo
si	se	k3xPyFc3	se
už	už	k6eAd1	už
také	také	k9	také
moc	moc	k6eAd1	moc
nerozumí	rozumět	k5eNaImIp3nS	rozumět
<g/>
.	.	kIx.	.
</s>
<s>
Doufá	doufat	k5eAaImIp3nS	doufat
<g/>
,	,	kIx,	,
že	že	k8xS	že
letní	letní	k2eAgFnSc1d1	letní
dovolená	dovolená	k1gFnSc1	dovolená
s	s	k7c7	s
dětmi	dítě	k1gFnPc7	dítě
a	a	k8xC	a
manželkou	manželka	k1gFnSc7	manželka
přinese	přinést	k5eAaPmIp3nS	přinést
do	do	k7c2	do
jejich	jejich	k3xOp3gInSc2	jejich
rodinného	rodinný	k2eAgInSc2d1	rodinný
života	život	k1gInSc2	život
opět	opět	k6eAd1	opět
harmonii	harmonie	k1gFnSc4	harmonie
<g/>
.	.	kIx.	.
</s>
<s>
Ubytují	ubytovat	k5eAaPmIp3nP	ubytovat
se	se	k3xPyFc4	se
v	v	k7c6	v
penzionu	penzion	k1gInSc6	penzion
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
moře	moře	k1gNnSc2	moře
a	a	k8xC	a
hned	hned	k6eAd1	hned
zkraje	zkraje	k7c2	zkraje
dovolené	dovolená	k1gFnSc2	dovolená
se	se	k3xPyFc4	se
seznámí	seznámit	k5eAaPmIp3nS	seznámit
s	s	k7c7	s
Petrusem	Petrus	k1gInSc7	Petrus
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
zde	zde	k6eAd1	zde
pracuje	pracovat	k5eAaImIp3nS	pracovat
jako	jako	k9	jako
údržbář	údržbář	k1gMnSc1	údržbář
<g/>
.	.	kIx.	.
</s>
<s>
Rune	run	k1gInSc5	run
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
Petrusovi	Petrus	k1gMnSc3	Petrus
fyzicky	fyzicky	k6eAd1	fyzicky
přitahován	přitahovat	k5eAaImNgMnS	přitahovat
a	a	k8xC	a
nedokáže	dokázat	k5eNaPmIp3nS	dokázat
si	se	k3xPyFc3	se
své	svůj	k3xOyFgInPc4	svůj
pocity	pocit	k1gInPc4	pocit
vysvětlit	vysvětlit	k5eAaPmF	vysvětlit
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
dojde	dojít	k5eAaPmIp3nS	dojít
k	k	k7c3	k
během	během	k7c2	během
pobytu	pobyt	k1gInSc2	pobyt
k	k	k7c3	k
jejich	jejich	k3xOp3gNnSc3	jejich
intimnímu	intimní	k2eAgNnSc3d1	intimní
sblížení	sblížení	k1gNnSc3	sblížení
<g/>
,	,	kIx,	,
Rune	run	k1gInSc5	run
se	se	k3xPyFc4	se
rozhodne	rozhodnout	k5eAaPmIp3nS	rozhodnout
předčasně	předčasně	k6eAd1	předčasně
ukončit	ukončit	k5eAaPmF	ukončit
dovolenou	dovolená	k1gFnSc4	dovolená
a	a	k8xC	a
odjet	odjet	k5eAaPmF	odjet
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
by	by	kYmCp3nS	by
rodině	rodina	k1gFnSc3	rodina
sdělil	sdělit	k5eAaPmAgMnS	sdělit
pravé	pravý	k2eAgInPc4d1	pravý
důvody	důvod	k1gInPc4	důvod
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
s	s	k7c7	s
odstupem	odstup	k1gInSc7	odstup
času	čas	k1gInSc2	čas
manželce	manželka	k1gFnSc3	manželka
oznámí	oznámit	k5eAaPmIp3nS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
odchází	odcházet	k5eAaImIp3nS	odcházet
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
zamiloval	zamilovat	k5eAaPmAgMnS	zamilovat
do	do	k7c2	do
muže	muž	k1gMnSc2	muž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Obsazení	obsazení	k1gNnSc1	obsazení
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
