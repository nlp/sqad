<s>
Newton	Newton	k1gMnSc1
zavedl	zavést	k5eAaPmAgMnS
celkem	celkem	k6eAd1
tři	tři	k4xCgInPc1
pohybové	pohybový	k2eAgInPc1d1
zákony	zákon	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
tvoří	tvořit	k5eAaImIp3nP
základ	základ	k1gInSc4
klasické	klasický	k2eAgFnSc2d1
mechaniky	mechanika	k1gFnSc2
a	a	k8xC
zejména	zejména	k9
dynamiky	dynamika	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
zkoumá	zkoumat	k5eAaImIp3nS
příčiny	příčina	k1gFnPc1
pohybu	pohyb	k1gInSc2
<g/>
.	.	kIx.
</s>