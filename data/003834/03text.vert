<s>
Obecně	obecně	k6eAd1	obecně
a	a	k8xC	a
v	v	k7c6	v
populárně	populárně	k6eAd1	populárně
naučné	naučný	k2eAgFnSc6d1	naučná
literatuře	literatura	k1gFnSc6	literatura
výraz	výraz	k1gInSc1	výraz
červ	červ	k1gMnSc1	červ
označuje	označovat	k5eAaImIp3nS	označovat
živočichy	živočich	k1gMnPc4	živočich
drobného	drobný	k2eAgInSc2d1	drobný
vzrůstu	vzrůst	k1gInSc2	vzrůst
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
mají	mít	k5eAaImIp3nP	mít
takzvané	takzvaný	k2eAgNnSc4d1	takzvané
červovité	červovitý	k2eAgNnSc4d1	červovitý
tělo	tělo	k1gNnSc4	tělo
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
tělo	tělo	k1gNnSc1	tělo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
protáhlého	protáhlý	k2eAgInSc2d1	protáhlý
tvaru	tvar	k1gInSc2	tvar
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
bez	bez	k7c2	bez
kostí	kost	k1gFnPc2	kost
a	a	k8xC	a
bez	bez	k7c2	bez
končetin	končetina	k1gFnPc2	končetina
<g/>
.	.	kIx.	.
</s>
<s>
Výraz	výraz	k1gInSc1	výraz
bývá	bývat	k5eAaImIp3nS	bývat
používán	používat	k5eAaImNgInS	používat
např.	např.	kA	např.
ve	v	k7c4	v
spojení	spojení	k1gNnSc4	spojení
"	"	kIx"	"
<g/>
mnohoštětinatí	mnohoštětinatý	k2eAgMnPc1d1	mnohoštětinatý
červi	červ	k1gMnPc1	červ
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
běžné	běžný	k2eAgFnSc6d1	běžná
češtině	čeština	k1gFnSc6	čeština
se	s	k7c7	s
slovem	slovo	k1gNnSc7	slovo
červ	červ	k1gMnSc1	červ
označují	označovat	k5eAaImIp3nP	označovat
i	i	k9	i
červovité	červovitý	k2eAgFnPc1d1	červovitá
larvy	larva	k1gFnPc1	larva
některých	některý	k3yIgInPc2	některý
druhů	druh	k1gInPc2	druh
hmyzu	hmyz	k1gInSc2	hmyz
s	s	k7c7	s
proměnou	proměna	k1gFnSc7	proměna
dokonalou	dokonalý	k2eAgFnSc7d1	dokonalá
(	(	kIx(	(
<g/>
typicky	typicky	k6eAd1	typicky
larvy	larva	k1gFnPc1	larva
žijící	žijící	k2eAgFnPc1d1	žijící
v	v	k7c6	v
ovoci	ovoce	k1gNnSc6	ovoce
<g/>
,	,	kIx,	,
houbách	houba	k1gFnPc6	houba
<g/>
,	,	kIx,	,
mase	maso	k1gNnSc6	maso
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Biologický	biologický	k2eAgInSc1d1	biologický
význam	význam	k1gInSc1	význam
je	být	k5eAaImIp3nS	být
však	však	k9	však
užší	úzký	k2eAgNnSc1d2	užší
<g/>
.	.	kIx.	.
</s>
<s>
Červi	červ	k1gMnPc1	červ
(	(	kIx(	(
<g/>
Vermes	Vermes	k1gMnSc1	Vermes
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
biologický	biologický	k2eAgInSc4d1	biologický
taxon	taxon	k1gInSc4	taxon
(	(	kIx(	(
<g/>
zpravidla	zpravidla	k6eAd1	zpravidla
kmen	kmen	k1gInSc1	kmen
<g/>
)	)	kIx)	)
zahrnující	zahrnující	k2eAgMnSc1d1	zahrnující
takřka	takřka	k6eAd1	takřka
všechny	všechen	k3xTgMnPc4	všechen
mnohobuněčné	mnohobuněčný	k2eAgMnPc4d1	mnohobuněčný
živočichy	živočich	k1gMnPc4	živočich
nepatřící	patřící	k2eNgInSc1d1	nepatřící
mezi	mezi	k7c4	mezi
houbovce	houbovec	k1gInPc4	houbovec
<g/>
,	,	kIx,	,
láčkovce	láčkovec	k1gMnPc4	láčkovec
<g/>
,	,	kIx,	,
měkkýše	měkkýš	k1gMnPc4	měkkýš
<g/>
,	,	kIx,	,
členovce	členovec	k1gMnPc4	členovec
a	a	k8xC	a
obratlovce	obratlovec	k1gMnPc4	obratlovec
(	(	kIx(	(
<g/>
vše	všechen	k3xTgNnSc1	všechen
v	v	k7c6	v
dřívějším	dřívější	k2eAgNnSc6d1	dřívější
pojetí	pojetí	k1gNnSc6	pojetí
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Taxon	taxon	k1gInSc4	taxon
zavedl	zavést	k5eAaPmAgMnS	zavést
Carl	Carl	k1gMnSc1	Carl
Linné	Linná	k1gFnSc2	Linná
ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
Systema	System	k1gMnSc4	System
naturae	natura	k1gMnSc4	natura
v	v	k7c6	v
r.	r.	kA	r.
1735	[number]	k4	1735
pro	pro	k7c4	pro
všechny	všechen	k3xTgMnPc4	všechen
bezobratlé	bezobratlí	k1gMnPc4	bezobratlí
kromě	kromě	k7c2	kromě
členovců	členovec	k1gMnPc2	členovec
<g/>
;	;	kIx,	;
tehdy	tehdy	k6eAd1	tehdy
tedy	tedy	k9	tedy
ještě	ještě	k6eAd1	ještě
obsahoval	obsahovat	k5eAaImAgInS	obsahovat
i	i	k9	i
měkkýše	měkkýš	k1gMnPc4	měkkýš
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
z	z	k7c2	z
taxonu	taxon	k1gInSc2	taxon
začaly	začít	k5eAaPmAgFnP	začít
být	být	k5eAaImF	být
vyčleňovány	vyčleňován	k2eAgInPc4d1	vyčleňován
samostatné	samostatný	k2eAgInPc4d1	samostatný
kmeny	kmen	k1gInPc4	kmen
(	(	kIx(	(
<g/>
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
např.	např.	kA	např.
Jean-Baptiste	Jean-Baptist	k1gMnSc5	Jean-Baptist
Lamarck	Lamarck	k1gMnSc1	Lamarck
vyčlenil	vyčlenit	k5eAaPmAgMnS	vyčlenit
ostnokožce	ostnokožec	k1gMnPc4	ostnokožec
a	a	k8xC	a
kroužkovce	kroužkovec	k1gMnPc4	kroužkovec
v	v	k7c6	v
tehdejším	tehdejší	k2eAgInSc6d1	tehdejší
smyslu	smysl	k1gInSc6	smysl
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
dvacátém	dvacátý	k4xOgNnSc6	dvacátý
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
díky	díky	k7c3	díky
objevu	objev	k1gInSc3	objev
nových	nový	k2eAgInPc2d1	nový
živočišných	živočišný	k2eAgInPc2d1	živočišný
druhů	druh	k1gInPc2	druh
a	a	k8xC	a
srovnávacím	srovnávací	k2eAgFnPc3d1	srovnávací
fylogenetickým	fylogenetický	k2eAgFnPc3d1	fylogenetická
studiím	studie	k1gFnPc3	studie
(	(	kIx(	(
<g/>
ještě	ještě	k9	ještě
dávno	dávno	k6eAd1	dávno
před	před	k7c7	před
molekulárními	molekulární	k2eAgFnPc7d1	molekulární
metodami	metoda	k1gFnPc7	metoda
<g/>
)	)	kIx)	)
odhalena	odhalen	k2eAgFnSc1d1	odhalena
nepřirozenost	nepřirozenost	k1gFnSc1	nepřirozenost
i	i	k9	i
takto	takto	k6eAd1	takto
zúženého	zúžený	k2eAgNnSc2d1	zúžené
seskupení	seskupení	k1gNnSc2	seskupení
a	a	k8xC	a
taxon	taxon	k1gInSc1	taxon
přestal	přestat	k5eAaPmAgInS	přestat
být	být	k5eAaImF	být
systematickými	systematický	k2eAgFnPc7d1	systematická
biology	biolog	k1gMnPc4	biolog
používán	používán	k2eAgMnSc1d1	používán
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
druhé	druhý	k4xOgFnSc2	druhý
poloviny	polovina	k1gFnSc2	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
tak	tak	k6eAd1	tak
přetrval	přetrvat	k5eAaPmAgInS	přetrvat
jen	jen	k9	jen
výjimečně	výjimečně	k6eAd1	výjimečně
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
z	z	k7c2	z
pedagogických	pedagogický	k2eAgInPc2d1	pedagogický
důvodů	důvod	k1gInPc2	důvod
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
učebnicích	učebnice	k1gFnPc6	učebnice
se	s	k7c7	s
zjednodušujícím	zjednodušující	k2eAgNnSc7d1	zjednodušující
podáním	podání	k1gNnSc7	podání
systému	systém	k1gInSc2	systém
živočišné	živočišný	k2eAgFnSc2d1	živočišná
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
dnešním	dnešní	k2eAgInSc6d1	dnešní
systému	systém	k1gInSc6	systém
spadají	spadat	k5eAaPmIp3nP	spadat
zástupci	zástupce	k1gMnPc7	zástupce
bývalých	bývalý	k2eAgMnPc2d1	bývalý
červů	červ	k1gMnPc2	červ
(	(	kIx(	(
<g/>
Vermes	Vermes	k1gMnSc1	Vermes
<g/>
)	)	kIx)	)
do	do	k7c2	do
kmenů	kmen	k1gInPc2	kmen
<g/>
:	:	kIx,	:
čelistovky	čelistovka	k1gFnSc2	čelistovka
(	(	kIx(	(
<g/>
Gnathostomulida	Gnathostomulida	k1gFnSc1	Gnathostomulida
<g/>
)	)	kIx)	)
hlavatci	hlavatec	k1gInPc7	hlavatec
(	(	kIx(	(
<g/>
Priapulida	Priapulida	k1gFnSc1	Priapulida
<g/>
)	)	kIx)	)
hlístice	hlístice	k1gFnSc1	hlístice
(	(	kIx(	(
<g/>
Nematoda	Nematoda	k1gFnSc1	Nematoda
<g/>
)	)	kIx)	)
chapadlovky	chapadlovka	k1gFnPc1	chapadlovka
(	(	kIx(	(
<g/>
Phoronida	Phoronida	k1gFnSc1	Phoronida
<g/>
)	)	kIx)	)
kroužkovci	kroužkovec	k1gMnPc1	kroužkovec
(	(	kIx(	(
<g/>
Annelida	Annelida	k1gFnSc1	Annelida
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
lilijicovců	lilijicovec	k1gInPc2	lilijicovec
<g />
.	.	kIx.	.
</s>
<s>
(	(	kIx(	(
<g/>
Myzostomida	Myzostomida	k1gFnSc1	Myzostomida
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
rypohlavců	rypohlavec	k1gMnPc2	rypohlavec
(	(	kIx(	(
<g/>
Echiura	Echiura	k1gFnSc1	Echiura
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
sumýšovců	sumýšovec	k1gInPc2	sumýšovec
(	(	kIx(	(
<g/>
Sipunculida	Sipunculida	k1gFnSc1	Sipunculida
<g/>
)	)	kIx)	)
a	a	k8xC	a
vláknonošců	vláknonošce	k1gMnPc2	vláknonošce
(	(	kIx(	(
<g/>
Pogonophora	Pogonophora	k1gFnSc1	Pogonophora
<g/>
))	))	k?	))
pásnice	pásnice	k1gFnSc1	pásnice
(	(	kIx(	(
<g/>
Nemertini	Nemertin	k2eAgMnPc1d1	Nemertin
=	=	kIx~	=
Nemertea	Nemertea	k1gMnSc1	Nemertea
<g/>
)	)	kIx)	)
ploštěnci	ploštěnec	k1gMnPc1	ploštěnec
(	(	kIx(	(
<g/>
Platyhelminthes	Platyhelminthes	k1gInSc1	Platyhelminthes
<g/>
)	)	kIx)	)
ploutvenky	ploutvenka	k1gFnPc1	ploutvenka
(	(	kIx(	(
<g/>
Chaetognatha	Chaetognatha	k1gFnSc1	Chaetognatha
<g/>
)	)	kIx)	)
polostrunatci	polostrunatec	k1gMnPc1	polostrunatec
(	(	kIx(	(
<g/>
Hemichordata	Hemichordata	k1gFnSc1	Hemichordata
<g/>
)	)	kIx)	)
strunovci	strunovec	k1gMnPc1	strunovec
(	(	kIx(	(
<g/>
Nematomorpha	Nematomorpha	k1gMnSc1	Nematomorpha
<g/>
)	)	kIx)	)
a	a	k8xC	a
vrtejši	vrtejsat	k5eAaPmIp1nSwK	vrtejsat
(	(	kIx(	(
<g/>
Acanthocephala	Acanthocephal	k1gMnSc4	Acanthocephal
<g/>
)	)	kIx)	)
Vedle	vedle	k7c2	vedle
toho	ten	k3xDgNnSc2	ten
se	se	k3xPyFc4	se
ustálil	ustálit	k5eAaPmAgMnS	ustálit
užší	úzký	k2eAgInSc4d2	užší
smysl	smysl	k1gInSc4	smysl
pojmu	pojmout	k5eAaPmIp1nS	pojmout
červi	červ	k1gMnPc1	červ
(	(	kIx(	(
<g/>
Helminthes	Helminthes	k1gInSc1	Helminthes
<g/>
)	)	kIx)	)
pro	pro	k7c4	pro
červy	červ	k1gMnPc4	červ
parazitující	parazitující	k2eAgMnPc4d1	parazitující
na	na	k7c6	na
zvířatech	zvíře	k1gNnPc6	zvíře
(	(	kIx(	(
<g/>
včetně	včetně	k7c2	včetně
člověka	člověk	k1gMnSc2	člověk
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
smyslu	smysl	k1gInSc6	smysl
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
i	i	k9	i
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
medicíně	medicína	k1gFnSc6	medicína
a	a	k8xC	a
parazitologii	parazitologie	k1gFnSc6	parazitologie
<g/>
.	.	kIx.	.
</s>
<s>
Červ	červ	k1gMnSc1	červ
neboli	neboli	k8xC	neboli
helmint	helmint	k1gMnSc1	helmint
je	být	k5eAaImIp3nS	být
mnohobuněčný	mnohobuněčný	k2eAgInSc4d1	mnohobuněčný
organismus	organismus	k1gInSc4	organismus
patřící	patřící	k2eAgInSc4d1	patřící
do	do	k7c2	do
kmenů	kmen	k1gInPc2	kmen
ploštěnci	ploštěnec	k1gMnPc1	ploštěnec
(	(	kIx(	(
<g/>
Platyhelminthes	Platyhelminthes	k1gMnSc1	Platyhelminthes
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hlístice	hlístice	k1gFnSc1	hlístice
(	(	kIx(	(
<g/>
Nematoda	Nematoda	k1gFnSc1	Nematoda
<g/>
)	)	kIx)	)
a	a	k8xC	a
vrtejši	vrtejsat	k5eAaPmIp1nSwK	vrtejsat
(	(	kIx(	(
<g/>
Acanthocephala	Acanthocephal	k1gMnSc4	Acanthocephal
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
především	především	k9	především
o	o	k7c4	o
parazity	parazit	k1gMnPc4	parazit
zvířat	zvíře	k1gNnPc2	zvíře
<g/>
,	,	kIx,	,
člověka	člověk	k1gMnSc2	člověk
a	a	k8xC	a
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
druhy	druh	k1gInPc1	druh
žijí	žít	k5eAaImIp3nP	žít
saprofytickým	saprofytický	k2eAgInSc7d1	saprofytický
způsobem	způsob	k1gInSc7	způsob
v	v	k7c6	v
půdě	půda	k1gFnSc6	půda
či	či	k8xC	či
ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
<g/>
.	.	kIx.	.
</s>
<s>
Věda	věda	k1gFnSc1	věda
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
zabývá	zabývat	k5eAaImIp3nS	zabývat
studiem	studio	k1gNnSc7	studio
helmintů	helmint	k1gInPc2	helmint
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
helmintologie	helmintologie	k1gFnSc1	helmintologie
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
některých	některý	k3yIgMnPc2	některý
autorů	autor	k1gMnPc2	autor
lze	lze	k6eAd1	lze
mezi	mezi	k7c7	mezi
helminty	helmint	k1gMnPc7	helmint
zařadit	zařadit	k5eAaPmF	zařadit
i	i	k9	i
parazitické	parazitický	k2eAgInPc1d1	parazitický
viřníky	viřník	k1gInPc1	viřník
(	(	kIx(	(
<g/>
Rotifera	Rotifera	k1gFnSc1	Rotifera
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
strunovce	strunovec	k1gMnPc4	strunovec
(	(	kIx(	(
<g/>
Nematomorpha	Nematomorph	k1gMnSc4	Nematomorph
<g/>
)	)	kIx)	)
či	či	k8xC	či
pijavky	pijavka	k1gFnSc2	pijavka
(	(	kIx(	(
<g/>
Hirudinea	Hirudinea	k1gMnSc1	Hirudinea
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Červivost	červivost	k1gFnSc1	červivost
ovoce	ovoce	k1gNnSc2	ovoce
–	–	k?	–
červivost	červivost	k1gFnSc1	červivost
jablek	jablko	k1gNnPc2	jablko
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
nejčastěji	často	k6eAd3	často
obaleč	obaleč	k1gMnSc1	obaleč
jablečný	jablečný	k2eAgMnSc1d1	jablečný
<g/>
,	,	kIx,	,
červivost	červivost	k1gFnSc1	červivost
třešní	třešeň	k1gFnPc2	třešeň
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
nejčastěji	často	k6eAd3	často
vrtule	vrtule	k1gFnSc1	vrtule
třešňová	třešňový	k2eAgFnSc1d1	třešňová
<g/>
.	.	kIx.	.
</s>
<s>
Plicní	plicní	k2eAgFnSc1d1	plicní
červivost	červivost	k1gFnSc1	červivost
Helmintózy	Helmintóza	k1gFnSc2	Helmintóza
ptáků	pták	k1gMnPc2	pták
Obrázky	obrázek	k1gInPc4	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc4	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
červ	červ	k1gMnSc1	červ
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
červ	červ	k1gMnSc1	červ
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
Téma	téma	k1gNnSc1	téma
Červ	červ	k1gMnSc1	červ
ve	v	k7c6	v
Wikicitátech	Wikicitát	k1gInPc6	Wikicitát
</s>
