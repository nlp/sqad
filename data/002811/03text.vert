<s>
František	František	k1gMnSc1	František
Vláčil	vláčit	k5eAaImAgMnS	vláčit
(	(	kIx(	(
<g/>
19	[number]	k4	19
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1924	[number]	k4	1924
Český	český	k2eAgInSc1d1	český
Těšín	Těšín	k1gInSc1	Těšín
-	-	kIx~	-
28	[number]	k4	28
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
1999	[number]	k4	1999
Praha	Praha	k1gFnSc1	Praha
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
český	český	k2eAgMnSc1d1	český
malíř	malíř	k1gMnSc1	malíř
<g/>
,	,	kIx,	,
grafik	grafik	k1gMnSc1	grafik
<g/>
,	,	kIx,	,
filmový	filmový	k2eAgMnSc1d1	filmový
scenárista	scenárista	k1gMnSc1	scenárista
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc4	jeho
filmy	film	k1gInPc4	film
Marketa	Market	k2eAgFnSc1d1	Marketa
Lazarová	Lazarová	k1gFnSc1	Lazarová
a	a	k8xC	a
Údolí	údolí	k1gNnSc1	údolí
včel	včela	k1gFnPc2	včela
jsou	být	k5eAaImIp3nP	být
dnes	dnes	k6eAd1	dnes
filmovými	filmový	k2eAgInPc7d1	filmový
odborníky	odborník	k1gMnPc4	odborník
považovány	považován	k2eAgMnPc4d1	považován
za	za	k7c4	za
vrcholná	vrcholný	k2eAgNnPc4d1	vrcholné
díla	dílo	k1gNnPc4	dílo
české	český	k2eAgFnSc2d1	Česká
kinematografie	kinematografie	k1gFnSc2	kinematografie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
MFF	MFF	kA	MFF
Karlovy	Karlův	k2eAgInPc1d1	Karlův
Vary	Vary	k1gInPc1	Vary
byl	být	k5eAaImAgInS	být
oceněn	ocenit	k5eAaPmNgInS	ocenit
za	za	k7c4	za
přínos	přínos	k1gInSc4	přínos
světové	světový	k2eAgFnSc3d1	světová
kinematografii	kinematografie	k1gFnSc3	kinematografie
<g/>
.	.	kIx.	.
</s>
<s>
Dětství	dětství	k1gNnSc4	dětství
strávil	strávit	k5eAaPmAgMnS	strávit
na	na	k7c6	na
severní	severní	k2eAgFnSc6d1	severní
Moravě	Morava	k1gFnSc6	Morava
a	a	k8xC	a
pak	pak	k6eAd1	pak
krátce	krátce	k6eAd1	krátce
studoval	studovat	k5eAaImAgMnS	studovat
na	na	k7c4	na
UMPRUM	umprum	k1gInSc4	umprum
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
přestoupil	přestoupit	k5eAaPmAgMnS	přestoupit
na	na	k7c4	na
obor	obor	k1gInSc4	obor
dějiny	dějiny	k1gFnPc4	dějiny
umění	umění	k1gNnSc1	umění
a	a	k8xC	a
estetika	estetika	k1gFnSc1	estetika
na	na	k7c6	na
Filozofické	filozofický	k2eAgFnSc6d1	filozofická
fakultě	fakulta	k1gFnSc6	fakulta
Masarykovy	Masarykův	k2eAgFnSc2d1	Masarykova
univerzity	univerzita	k1gFnSc2	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Studium	studium	k1gNnSc1	studium
ukončil	ukončit	k5eAaPmAgInS	ukončit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1951	[number]	k4	1951
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c4	o
film	film	k1gInSc4	film
se	se	k3xPyFc4	se
zajímal	zajímat	k5eAaImAgMnS	zajímat
již	již	k6eAd1	již
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
studia	studio	k1gNnSc2	studio
a	a	k8xC	a
jako	jako	k9	jako
scénárista	scénárista	k1gMnSc1	scénárista
a	a	k8xC	a
režisér	režisér	k1gMnSc1	režisér
pracoval	pracovat	k5eAaImAgMnS	pracovat
v	v	k7c6	v
brněnském	brněnský	k2eAgNnSc6d1	brněnské
studiu	studio	k1gNnSc6	studio
kresleného	kreslený	k2eAgInSc2d1	kreslený
a	a	k8xC	a
loutkového	loutkový	k2eAgInSc2d1	loutkový
filmu	film	k1gInSc2	film
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
nově	nově	k6eAd1	nově
zřízeného	zřízený	k2eAgNnSc2d1	zřízené
Studia	studio	k1gNnSc2	studio
populárně	populárně	k6eAd1	populárně
vědeckých	vědecký	k2eAgInPc2d1	vědecký
a	a	k8xC	a
naučných	naučný	k2eAgInPc2d1	naučný
filmů	film	k1gInPc2	film
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
povinné	povinný	k2eAgFnSc2d1	povinná
vojenské	vojenský	k2eAgFnSc2d1	vojenská
služby	služba	k1gFnSc2	služba
ve	v	k7c6	v
filmovém	filmový	k2eAgNnSc6d1	filmové
studiu	studio	k1gNnSc6	studio
Československého	československý	k2eAgInSc2d1	československý
armádního	armádní	k2eAgInSc2d1	armádní
filmu	film	k1gInSc2	film
točil	točit	k5eAaImAgMnS	točit
různé	různý	k2eAgFnPc4d1	různá
instruktážní	instruktážní	k2eAgFnPc4d1	instruktážní
a	a	k8xC	a
propagandistické	propagandistický	k2eAgFnPc4d1	propagandistická
snímky	snímka	k1gFnPc4	snímka
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Barrandově	Barrandov	k1gInSc6	Barrandov
debutoval	debutovat	k5eAaBmAgMnS	debutovat
až	až	k6eAd1	až
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1960	[number]	k4	1960
filmem	film	k1gInSc7	film
Holubice	holubice	k1gFnSc2	holubice
<g/>
,	,	kIx,	,
kterým	který	k3yIgMnSc7	který
se	se	k3xPyFc4	se
zařadil	zařadit	k5eAaPmAgMnS	zařadit
mezi	mezi	k7c4	mezi
nejpozoruhodnější	pozoruhodný	k2eAgMnPc4d3	nejpozoruhodnější
filmové	filmový	k2eAgMnPc4d1	filmový
tvůrce	tvůrce	k1gMnPc4	tvůrce
tzv.	tzv.	kA	tzv.
československé	československý	k2eAgFnSc2d1	Československá
nové	nový	k2eAgFnSc2d1	nová
vlny	vlna	k1gFnSc2	vlna
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
oceněn	ocenit	k5eAaPmNgInS	ocenit
na	na	k7c6	na
MFF	MFF	kA	MFF
v	v	k7c6	v
Benátkách	Benátky	k1gFnPc6	Benátky
<g/>
.	.	kIx.	.
</s>
<s>
Následovala	následovat	k5eAaImAgFnS	následovat
historická	historický	k2eAgFnSc1d1	historická
balada	balada	k1gFnSc1	balada
Ďáblova	ďáblův	k2eAgFnSc1d1	Ďáblova
past	past	k1gFnSc1	past
(	(	kIx(	(
<g/>
1961	[number]	k4	1961
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgInSc1d3	veliký
ohlas	ohlas	k1gInSc1	ohlas
mu	on	k3xPp3gMnSc3	on
ale	ale	k9	ale
přinesl	přinést	k5eAaPmAgInS	přinést
dlouho	dlouho	k6eAd1	dlouho
připravovaný	připravovaný	k2eAgInSc1d1	připravovaný
<g/>
,	,	kIx,	,
dlouho	dlouho	k6eAd1	dlouho
natáčený	natáčený	k2eAgInSc4d1	natáčený
a	a	k8xC	a
upravovaný	upravovaný	k2eAgInSc4d1	upravovaný
film	film	k1gInSc4	film
Marketa	Market	k2eAgFnSc1d1	Marketa
Lazarová	Lazarová	k1gFnSc1	Lazarová
<g/>
.	.	kIx.	.
</s>
<s>
Složitá	složitý	k2eAgFnSc1d1	složitá
historická	historický	k2eAgFnSc1d1	historická
filmová	filmový	k2eAgFnSc1d1	filmová
freska	freska	k1gFnSc1	freska
s	s	k7c7	s
tehdy	tehdy	k6eAd1	tehdy
debutující	debutující	k2eAgFnSc7d1	debutující
mladičkou	mladičký	k2eAgFnSc7d1	mladičká
slovenskou	slovenský	k2eAgFnSc7d1	slovenská
herečkou	herečka	k1gFnSc7	herečka
Magdou	Magda	k1gFnSc7	Magda
Vašáryovou	Vašáryová	k1gFnSc7	Vašáryová
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
Markety	market	k1gInPc4	market
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
úchvatný	úchvatný	k2eAgInSc4d1	úchvatný
psychologický	psychologický	k2eAgInSc4d1	psychologický
film	film	k1gInSc4	film
plný	plný	k2eAgInSc4d1	plný
obrazových	obrazový	k2eAgFnPc2d1	obrazová
metafor	metafora	k1gFnPc2	metafora
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Vláčilem	Vláčil	k1gInSc7	Vláčil
na	na	k7c6	na
něm	on	k3xPp3gNnSc6	on
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
přední	přední	k2eAgMnPc1d1	přední
čeští	český	k2eAgMnPc1d1	český
výtvarníci	výtvarník	k1gMnPc1	výtvarník
jako	jako	k8xC	jako
tvůrci	tvůrce	k1gMnPc1	tvůrce
kostýmů	kostým	k1gInPc2	kostým
a	a	k8xC	a
dekorací	dekorace	k1gFnPc2	dekorace
(	(	kIx(	(
<g/>
Theodor	Theodor	k1gMnSc1	Theodor
Pištěk	Pištěk	k1gMnSc1	Pištěk
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Koblasa	Koblas	k1gMnSc2	Koblas
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
anketě	anketa	k1gFnSc6	anketa
kritiků	kritik	k1gMnPc2	kritik
byl	být	k5eAaImAgInS	být
film	film	k1gInSc1	film
Markéta	Markéta	k1gFnSc1	Markéta
Lazarová	Lazarová	k1gFnSc1	Lazarová
prohlášen	prohlásit	k5eAaPmNgMnS	prohlásit
za	za	k7c4	za
nejvýznamnější	významný	k2eAgInSc4d3	nejvýznamnější
film	film	k1gInSc4	film
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
i	i	k9	i
na	na	k7c4	na
tehdejší	tehdejší	k2eAgInPc4d1	tehdejší
poměry	poměr	k1gInPc4	poměr
velice	velice	k6eAd1	velice
nákladný	nákladný	k2eAgMnSc1d1	nákladný
(	(	kIx(	(
<g/>
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
o	o	k7c4	o
vůbec	vůbec	k9	vůbec
nejdražší	drahý	k2eAgInSc4d3	nejdražší
český	český	k2eAgInSc4d1	český
film	film	k1gInSc4	film
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
část	část	k1gFnSc1	část
nákladů	náklad	k1gInPc2	náklad
vrátila	vrátit	k5eAaPmAgFnS	vrátit
zpět	zpět	k6eAd1	zpět
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
stejných	stejný	k2eAgFnPc6d1	stejná
kulisách	kulisa	k1gFnPc6	kulisa
a	a	k8xC	a
dekoracích	dekorace	k1gFnPc6	dekorace
natočen	natočen	k2eAgInSc1d1	natočen
další	další	k2eAgInSc1d1	další
historický	historický	k2eAgInSc1d1	historický
film	film	k1gInSc1	film
Údolí	údolí	k1gNnSc1	údolí
včel	včela	k1gFnPc2	včela
s	s	k7c7	s
Petrem	Petr	k1gMnSc7	Petr
Čepkem	Čepek	k1gMnSc7	Čepek
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
<s>
Čepek	Čepek	k1gMnSc1	Čepek
zazářil	zazářit	k5eAaPmAgMnS	zazářit
i	i	k9	i
v	v	k7c6	v
dalším	další	k2eAgInSc6d1	další
barevném	barevný	k2eAgInSc6d1	barevný
snímku	snímek	k1gInSc6	snímek
Adelheid	Adelheid	k1gInSc1	Adelheid
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
období	období	k1gNnSc6	období
normalizace	normalizace	k1gFnSc2	normalizace
František	František	k1gMnSc1	František
Vláčil	vláčit	k5eAaImAgMnS	vláčit
natočil	natočit	k5eAaBmAgMnS	natočit
několik	několik	k4yIc4	několik
krátkometrážních	krátkometrážní	k2eAgInPc2d1	krátkometrážní
filmů	film	k1gInPc2	film
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
patrně	patrně	k6eAd1	patrně
vůbec	vůbec	k9	vůbec
nejpozoruhodnější	pozoruhodný	k2eAgInSc1d3	nejpozoruhodnější
je	být	k5eAaImIp3nS	být
film	film	k1gInSc1	film
Praha	Praha	k1gFnSc1	Praha
secesní	secesní	k2eAgFnSc1d1	secesní
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
<g/>
.	.	kIx.	.
</s>
<s>
Něžně	něžně	k6eAd1	něžně
poetický	poetický	k2eAgMnSc1d1	poetický
byl	být	k5eAaImAgInS	být
i	i	k9	i
vánočně	vánočně	k6eAd1	vánočně
laděný	laděný	k2eAgInSc4d1	laděný
dětský	dětský	k2eAgInSc4d1	dětský
film	film	k1gInSc4	film
Pověst	pověst	k1gFnSc1	pověst
o	o	k7c6	o
stříbrné	stříbrný	k2eAgFnSc6d1	stříbrná
jedli	jedle	k1gFnSc6	jedle
<g/>
.	.	kIx.	.
</s>
<s>
Následoval	následovat	k5eAaImAgInS	následovat
smutný	smutný	k2eAgInSc1d1	smutný
<g/>
,	,	kIx,	,
tísnivý	tísnivý	k2eAgInSc1d1	tísnivý
a	a	k8xC	a
psychologicky	psychologicky	k6eAd1	psychologicky
těžký	těžký	k2eAgInSc1d1	těžký
příběh	příběh	k1gInSc1	příběh
Dým	dým	k1gInSc1	dým
bramborové	bramborový	k2eAgFnSc2d1	bramborová
natě	nať	k1gFnSc2	nať
s	s	k7c7	s
Rudolfem	Rudolf	k1gMnSc7	Rudolf
Hrušínským	Hrušínský	k2eAgMnSc7d1	Hrušínský
v	v	k7c6	v
hlavní	hlavní	k2eAgFnSc6d1	hlavní
roli	role	k1gFnSc6	role
<g/>
.	.	kIx.	.
</s>
<s>
Zaujal	zaujmout	k5eAaPmAgInS	zaujmout
i	i	k9	i
baladický	baladický	k2eAgInSc1d1	baladický
příběh	příběh	k1gInSc1	příběh
Stíny	stín	k1gInPc1	stín
horkého	horký	k2eAgNnSc2d1	horké
léta	léto	k1gNnSc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Výrazný	výrazný	k2eAgInSc1d1	výrazný
režisérský	režisérský	k2eAgInSc1d1	režisérský
rukopis	rukopis	k1gInSc1	rukopis
nese	nést	k5eAaImIp3nS	nést
i	i	k9	i
biografický	biografický	k2eAgInSc1d1	biografický
film	film	k1gInSc1	film
o	o	k7c4	o
skladateli	skladatel	k1gMnSc3	skladatel
Antonínu	Antonín	k1gMnSc3	Antonín
Dvořákovi	Dvořák	k1gMnSc3	Dvořák
Koncert	koncert	k1gInSc1	koncert
na	na	k7c6	na
konci	konec	k1gInSc6	konec
léta	léto	k1gNnSc2	léto
<g/>
,	,	kIx,	,
natočený	natočený	k2eAgInSc1d1	natočený
podle	podle	k7c2	podle
scénáře	scénář	k1gInSc2	scénář
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Mahlera	Mahler	k1gMnSc2	Mahler
<g/>
.	.	kIx.	.
</s>
<s>
Velmi	velmi	k6eAd1	velmi
výrazné	výrazný	k2eAgFnPc4d1	výrazná
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
jeho	jeho	k3xOp3gInPc4	jeho
poslední	poslední	k2eAgInPc4d1	poslední
snímky	snímek	k1gInPc4	snímek
Hadí	hadit	k5eAaImIp3nS	hadit
jed	jed	k1gInSc1	jed
<g/>
,	,	kIx,	,
Stín	stín	k1gInSc1	stín
kapradiny	kapradina	k1gFnSc2	kapradina
a	a	k8xC	a
Mág	mág	k1gMnSc1	mág
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1994	[number]	k4	1994
obdržel	obdržet	k5eAaPmAgInS	obdržet
ocenění	ocenění	k1gNnSc4	ocenění
Český	český	k2eAgInSc1d1	český
lev	lev	k1gInSc1	lev
za	za	k7c4	za
celoživotní	celoživotní	k2eAgInSc4d1	celoživotní
umělecký	umělecký	k2eAgInSc4d1	umělecký
přínos	přínos	k1gInSc4	přínos
českému	český	k2eAgInSc3d1	český
filmu	film	k1gInSc3	film
<g/>
.	.	kIx.	.
</s>
<s>
Působil	působit	k5eAaImAgMnS	působit
také	také	k9	také
jako	jako	k9	jako
prezident	prezident	k1gMnSc1	prezident
České	český	k2eAgFnSc2d1	Česká
filmové	filmový	k2eAgFnSc2d1	filmová
a	a	k8xC	a
televizní	televizní	k2eAgFnSc2d1	televizní
akademie	akademie	k1gFnSc2	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Mezinárodním	mezinárodní	k2eAgInSc6d1	mezinárodní
filmovém	filmový	k2eAgInSc6d1	filmový
festivalu	festival	k1gInSc6	festival
v	v	k7c6	v
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
roku	rok	k1gInSc2	rok
1998	[number]	k4	1998
obdržel	obdržet	k5eAaPmAgMnS	obdržet
hlavní	hlavní	k2eAgFnSc4d1	hlavní
cenu	cena	k1gFnSc4	cena
za	za	k7c4	za
mimořádný	mimořádný	k2eAgInSc4d1	mimořádný
umělecký	umělecký	k2eAgInSc4d1	umělecký
přínos	přínos	k1gInSc4	přínos
světové	světový	k2eAgFnSc3d1	světová
kinematografii	kinematografie	k1gFnSc3	kinematografie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2013	[number]	k4	2013
získal	získat	k5eAaPmAgInS	získat
in	in	k?	in
memoriam	memoriam	k6eAd1	memoriam
Cenu	cena	k1gFnSc4	cena
Ministerstva	ministerstvo	k1gNnSc2	ministerstvo
kultury	kultura	k1gFnSc2	kultura
za	za	k7c4	za
přínos	přínos	k1gInSc4	přínos
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
kinematografie	kinematografie	k1gFnSc2	kinematografie
a	a	k8xC	a
audiovize	audiovize	k1gFnSc2	audiovize
<g/>
.	.	kIx.	.
</s>
<s>
František	František	k1gMnSc1	František
Vláčil	vláčit	k5eAaImAgMnS	vláčit
byl	být	k5eAaImAgInS	být
bezesporu	bezesporu	k9	bezesporu
výjimečný	výjimečný	k2eAgInSc1d1	výjimečný
a	a	k8xC	a
všemi	všecek	k3xTgFnPc7	všecek
múzami	múza	k1gFnPc7	múza
políbený	políbený	k2eAgMnSc1d1	políbený
filmař	filmař	k1gMnSc1	filmař
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
jednalo	jednat	k5eAaImAgNnS	jednat
se	se	k3xPyFc4	se
o	o	k7c6	o
vnitřně	vnitřně	k6eAd1	vnitřně
velmi	velmi	k6eAd1	velmi
rozpolcenou	rozpolcený	k2eAgFnSc4d1	rozpolcená
a	a	k8xC	a
složitou	složitý	k2eAgFnSc4d1	složitá
osobnost	osobnost	k1gFnSc4	osobnost
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
nevyhýbaly	vyhýbat	k5eNaImAgFnP	vyhýbat
deprese	deprese	k1gFnPc1	deprese
a	a	k8xC	a
velké	velký	k2eAgInPc1d1	velký
problémy	problém	k1gInPc1	problém
s	s	k7c7	s
alkoholem	alkohol	k1gInSc7	alkohol
<g/>
.	.	kIx.	.
</s>
<s>
Mnohé	mnohé	k1gNnSc1	mnohé
jeho	jeho	k3xOp3gFnSc2	jeho
snímky	snímka	k1gFnSc2	snímka
byly	být	k5eAaImAgFnP	být
složitě	složitě	k6eAd1	složitě
dotvořeny	dotvořen	k2eAgFnPc1d1	dotvořena
jen	jen	k9	jen
díky	díky	k7c3	díky
podpoře	podpora	k1gFnSc3	podpora
jeho	on	k3xPp3gInSc2	on
filmového	filmový	k2eAgInSc2d1	filmový
štábu	štáb	k1gInSc2	štáb
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
však	však	k9	však
zůstává	zůstávat	k5eAaImIp3nS	zůstávat
jeho	jeho	k3xOp3gNnSc1	jeho
filmové	filmový	k2eAgNnSc1d1	filmové
dílo	dílo	k1gNnSc1	dílo
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
kontextu	kontext	k1gInSc6	kontext
české	český	k2eAgFnSc2d1	Česká
kinematografie	kinematografie	k1gFnSc2	kinematografie
zcela	zcela	k6eAd1	zcela
unikátní	unikátní	k2eAgMnSc1d1	unikátní
a	a	k8xC	a
naprosto	naprosto	k6eAd1	naprosto
mimořádné	mimořádný	k2eAgNnSc1d1	mimořádné
<g/>
.	.	kIx.	.
1960	[number]	k4	1960
Holubice	holubice	k1gFnSc1	holubice
1961	[number]	k4	1961
Ďáblova	ďáblův	k2eAgFnSc1d1	Ďáblova
past	past	k1gFnSc1	past
1967	[number]	k4	1967
Marketa	Marketa	k1gFnSc1	Marketa
Lazarová	Lazarová	k1gFnSc1	Lazarová
(	(	kIx(	(
<g/>
natočen	natočit	k5eAaBmNgInS	natočit
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
románu	román	k1gInSc2	román
Vladislava	Vladislav	k1gMnSc2	Vladislav
Vančury	Vančura	k1gMnSc2	Vančura
<g/>
<g />
.	.	kIx.	.
</s>
<s>
)	)	kIx)	)
1967	[number]	k4	1967
Údolí	údolí	k1gNnPc2	údolí
včel	včela	k1gFnPc2	včela
(	(	kIx(	(
<g/>
natočen	natočit	k5eAaBmNgInS	natočit
na	na	k7c4	na
motivy	motiv	k1gInPc4	motiv
stejnojmenného	stejnojmenný	k2eAgInSc2d1	stejnojmenný
románu	román	k1gInSc2	román
Vladimíra	Vladimír	k1gMnSc2	Vladimír
Körnera	Körner	k1gMnSc2	Körner
<g/>
)	)	kIx)	)
1969	[number]	k4	1969
Adelheid	Adelheida	k1gFnPc2	Adelheida
1973	[number]	k4	1973
Pověst	pověst	k1gFnSc4	pověst
o	o	k7c6	o
stříbrné	stříbrný	k2eAgFnSc6d1	stříbrná
jedli	jedle	k1gFnSc3	jedle
1974	[number]	k4	1974
Sirius	Sirius	k1gInSc1	Sirius
(	(	kIx(	(
<g/>
krátkometrážní	krátkometrážní	k2eAgInSc1d1	krátkometrážní
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
1974	[number]	k4	1974
Praha	Praha	k1gFnSc1	Praha
secesní	secesní	k2eAgFnSc1d1	secesní
(	(	kIx(	(
<g/>
krátkometrážní	krátkometrážní	k2eAgFnSc1d1	krátkometrážní
film	film	k1gInSc1	film
<g/>
)	)	kIx)	)
1976	[number]	k4	1976
Dým	dým	k1gInSc1	dým
bramborové	bramborový	k2eAgFnSc2d1	bramborová
natě	nať	k1gFnSc2	nať
1977	[number]	k4	1977
Stíny	stín	k1gInPc4	stín
horkého	horký	k2eAgNnSc2d1	horké
léta	léto	k1gNnSc2	léto
1975	[number]	k4	1975
Koncert	koncert	k1gInSc1	koncert
na	na	k7c4	na
<g />
.	.	kIx.	.
</s>
<s>
konci	konec	k1gInSc3	konec
léta	léto	k1gNnSc2	léto
(	(	kIx(	(
<g/>
film	film	k1gInSc1	film
o	o	k7c6	o
hudebním	hudební	k2eAgMnSc6d1	hudební
skladateli	skladatel	k1gMnSc6	skladatel
Antonínu	Antonín	k1gMnSc6	Antonín
Dvořákovi	Dvořák	k1gMnSc6	Dvořák
podle	podle	k7c2	podle
scénáře	scénář	k1gInSc2	scénář
Zdeňka	Zdeněk	k1gMnSc2	Zdeněk
Mahlera	Mahler	k1gMnSc2	Mahler
<g/>
)	)	kIx)	)
1981	[number]	k4	1981
Hadí	hadit	k5eAaImIp3nS	hadit
jed	jed	k1gInSc4	jed
1983	[number]	k4	1983
Pasáček	pasáček	k1gMnSc1	pasáček
z	z	k7c2	z
doliny	dolina	k1gFnSc2	dolina
1984	[number]	k4	1984
Stín	stín	k1gInSc1	stín
kapradiny	kapradina	k1gFnSc2	kapradina
1987	[number]	k4	1987
Mág	mág	k1gMnSc1	mág
1985	[number]	k4	1985
Albert	Alberta	k1gFnPc2	Alberta
Seznam	seznam	k1gInSc4	seznam
děl	dělo	k1gNnPc2	dělo
v	v	k7c6	v
Souborném	souborný	k2eAgInSc6d1	souborný
katalogu	katalog	k1gInSc6	katalog
ČR	ČR	kA	ČR
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gMnSc7	jejichž
autorem	autor	k1gMnSc7	autor
nebo	nebo	k8xC	nebo
tématem	téma	k1gNnSc7	téma
je	být	k5eAaImIp3nS	být
František	František	k1gMnSc1	František
Vláčil	vláčit	k5eAaImAgMnS	vláčit
František	František	k1gMnSc1	František
Vláčil	vláčit	k5eAaImAgMnS	vláčit
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc6d1	česko-slovenská
filmové	filmový	k2eAgFnSc6d1	filmová
databázi	databáze	k1gFnSc6	databáze
František	František	k1gMnSc1	František
Vláčil	vláčit	k5eAaImAgMnS	vláčit
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc6	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
František	František	k1gMnSc1	František
Vláčil	vláčit	k5eAaImAgMnS	vláčit
v	v	k7c4	v
Internet	Internet	k1gInSc4	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
František	František	k1gMnSc1	František
Vláčil	vláčit	k5eAaImAgMnS	vláčit
Kdo	kdo	k3yInSc1	kdo
byl	být	k5eAaImAgMnS	být
kdo	kdo	k3yInSc1	kdo
v	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
Článek	článek	k1gInSc1	článek
o	o	k7c6	o
výstavě	výstava	k1gFnSc6	výstava
věnované	věnovaný	k2eAgFnSc6d1	věnovaná
Františku	František	k1gMnSc6	František
Vláčilovi	Vláčilův	k2eAgMnPc1d1	Vláčilův
v	v	k7c6	v
Císařské	císařský	k2eAgFnSc6d1	císařská
konírně	konírna	k1gFnSc6	konírna
Pražského	pražský	k2eAgInSc2d1	pražský
hradu	hrad	k1gInSc2	hrad
Připravuje	připravovat	k5eAaImIp3nS	připravovat
se	se	k3xPyFc4	se
vydání	vydání	k1gNnSc1	vydání
knihy	kniha	k1gFnSc2	kniha
o	o	k7c6	o
životě	život	k1gInSc6	život
a	a	k8xC	a
díle	díl	k1gInSc6	díl
Františka	Františka	k1gFnSc1	Františka
Vláčila	vláčit	k5eAaImAgFnS	vláčit
</s>
