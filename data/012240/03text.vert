<p>
<s>
Kolpík	kolpík	k1gMnSc1	kolpík
bílý	bílý	k1gMnSc1	bílý
(	(	kIx(	(
<g/>
Platalea	Platalea	k1gFnSc1	Platalea
leucorodia	leucorodium	k1gNnSc2	leucorodium
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
středně	středně	k6eAd1	středně
velký	velký	k2eAgInSc1d1	velký
druh	druh	k1gInSc1	druh
brodivého	brodivý	k2eAgMnSc2d1	brodivý
ptáka	pták	k1gMnSc2	pták
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
ibisovitých	ibisovitý	k2eAgMnPc2d1	ibisovitý
<g/>
.	.	kIx.	.
</s>
<s>
Vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
se	se	k3xPyFc4	se
neobvyklým	obvyklý	k2eNgInSc7d1	neobvyklý
tvarem	tvar	k1gInSc7	tvar
zobáku	zobák	k1gInSc2	zobák
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
špičce	špička	k1gFnSc6	špička
lžícovitě	lžícovitě	k6eAd1	lžícovitě
rozšířený	rozšířený	k2eAgInSc1d1	rozšířený
<g/>
.	.	kIx.	.
</s>
<s>
Dospělí	dospělý	k2eAgMnPc1d1	dospělý
ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
celí	celý	k2eAgMnPc1d1	celý
bílí	bílý	k1gMnPc1	bílý
<g/>
,	,	kIx,	,
s	s	k7c7	s
žlutohnědou	žlutohnědý	k2eAgFnSc7d1	žlutohnědá
skvrnou	skvrna	k1gFnSc7	skvrna
na	na	k7c6	na
hrudi	hruď	k1gFnSc6	hruď
a	a	k8xC	a
chocholkou	chocholka	k1gFnSc7	chocholka
v	v	k7c6	v
týle	týl	k1gInSc6	týl
<g/>
.	.	kIx.	.
</s>
<s>
Nohy	noha	k1gFnPc1	noha
a	a	k8xC	a
zobák	zobák	k1gInSc1	zobák
jsou	být	k5eAaImIp3nP	být
černavé	černavý	k2eAgFnPc1d1	černavá
<g/>
.	.	kIx.	.
</s>
<s>
Mladí	mladý	k2eAgMnPc1d1	mladý
ptáci	pták	k1gMnPc1	pták
se	se	k3xPyFc4	se
liší	lišit	k5eAaImIp3nP	lišit
masově	masově	k6eAd1	masově
zbarveným	zbarvený	k2eAgInSc7d1	zbarvený
zobákem	zobák	k1gInSc7	zobák
a	a	k8xC	a
černými	černý	k2eAgFnPc7d1	černá
špičkami	špička	k1gFnPc7	špička
letek	letka	k1gFnPc2	letka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Kolpík	kolpík	k1gMnSc1	kolpík
bílý	bílý	k1gMnSc1	bílý
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
mírném	mírný	k2eAgNnSc6d1	mírné
pásmu	pásmo	k1gNnSc6	pásmo
Evropy	Evropa	k1gFnSc2	Evropa
od	od	k7c2	od
Nizozemí	Nizozemí	k1gNnSc2	Nizozemí
na	na	k7c4	na
východ	východ	k1gInSc4	východ
po	po	k7c6	po
jižní	jižní	k2eAgFnSc6d1	jižní
a	a	k8xC	a
střední	střední	k2eAgFnSc6d1	střední
Asii	Asie	k1gFnSc6	Asie
<g/>
,	,	kIx,	,
izolované	izolovaný	k2eAgFnPc1d1	izolovaná
malé	malý	k2eAgFnPc1d1	malá
populace	populace	k1gFnPc1	populace
hnízdí	hnízdit	k5eAaImIp3nP	hnízdit
také	také	k9	také
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
polovině	polovina	k1gFnSc6	polovina
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
pravidelně	pravidelně	k6eAd1	pravidelně
objevovat	objevovat	k5eAaImF	objevovat
až	až	k9	až
po	po	k7c6	po
roce	rok	k1gInSc6	rok
1945	[number]	k4	1945
<g/>
,	,	kIx,	,
poprvé	poprvé	k6eAd1	poprvé
zahnízdil	zahnízdit	k5eAaPmAgMnS	zahnízdit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1949	[number]	k4	1949
na	na	k7c6	na
jižní	jižní	k2eAgFnSc6d1	jižní
Moravě	Morava	k1gFnSc6	Morava
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
pravidelně	pravidelně	k6eAd1	pravidelně
v	v	k7c6	v
jižních	jižní	k2eAgFnPc6d1	jižní
Čechách	Čechy	k1gFnPc6	Čechy
<g/>
,	,	kIx,	,
populace	populace	k1gFnPc1	populace
se	se	k3xPyFc4	se
v	v	k7c6	v
letech	let	k1gInPc6	let
2001	[number]	k4	2001
<g/>
–	–	k?	–
<g/>
2003	[number]	k4	2003
pohybovala	pohybovat	k5eAaImAgFnS	pohybovat
mezi	mezi	k7c4	mezi
4	[number]	k4	4
<g/>
–	–	k?	–
<g/>
11	[number]	k4	11
páry	pára	k1gFnSc2	pára
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Seznam	seznam	k1gInSc1	seznam
ptáků	pták	k1gMnPc2	pták
v	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
</s>
</p>
<p>
<s>
Kolpík	kolpík	k1gMnSc1	kolpík
</s>
</p>
<p>
<s>
==	==	k?	==
Video	video	k1gNnSc1	video
==	==	k?	==
</s>
</p>
<p>
<s>
Jarmila	Jarmila	k1gFnSc1	Jarmila
Kačírková	Kačírková	k1gFnSc1	Kačírková
<g/>
:	:	kIx,	:
Kolpík	kolpík	k1gMnSc1	kolpík
bílý	bílý	k1gMnSc1	bílý
(	(	kIx(	(
<g/>
Platalea	Platalea	k1gFnSc1	Platalea
leucorodia	leucorodium	k1gNnSc2	leucorodium
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Maďarsko	Maďarsko	k1gNnSc1	Maďarsko
<g/>
,	,	kIx,	,
Hortobágy	Hortobág	k1gInPc1	Hortobág
29.4	[number]	k4	29.4
<g/>
.2019	.2019	k4	.2019
</s>
</p>
<p>
<s>
https://youtu.be/BNBoiWQhASQ	[url]	k?	https://youtu.be/BNBoiWQhASQ
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
kolpík	kolpík	k1gMnSc1	kolpík
bílý	bílý	k2eAgMnSc1d1	bílý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Galerie	galerie	k1gFnSc1	galerie
kolpík	kolpík	k1gMnSc1	kolpík
bílý	bílý	k2eAgMnSc1d1	bílý
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Platalea	Platale	k1gInSc2	Platale
leucorodia	leucorodium	k1gNnSc2	leucorodium
ve	v	k7c6	v
Wikidruzích	Wikidruh	k1gInPc6	Wikidruh
</s>
</p>
