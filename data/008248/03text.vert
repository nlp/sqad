<p>
<s>
Belo	Bela	k1gFnSc5	Bela
Kapolka	Kapolka	k1gFnSc1	Kapolka
(	(	kIx(	(
<g/>
22	[number]	k4	22
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1935	[number]	k4	1935
<g/>
,	,	kIx,	,
Hostie	hostie	k1gFnSc1	hostie
−	−	k?	−
18	[number]	k4	18
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1994	[number]	k4	1994
<g/>
,	,	kIx,	,
Vysoké	vysoký	k2eAgFnSc2d1	vysoká
Tatry	Tatra	k1gFnSc2	Tatra
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
slovenský	slovenský	k2eAgMnSc1d1	slovenský
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
,	,	kIx,	,
chatař	chatař	k1gMnSc1	chatař
<g/>
,	,	kIx,	,
nosič	nosič	k1gMnSc1	nosič
a	a	k8xC	a
horolezec	horolezec	k1gMnSc1	horolezec
působící	působící	k2eAgFnSc2d1	působící
ve	v	k7c6	v
Vysokých	vysoký	k2eAgFnPc6d1	vysoká
Tatrách	Tatra	k1gFnPc6	Tatra
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pocházel	pocházet	k5eAaImAgMnS	pocházet
z	z	k7c2	z
učitelské	učitelský	k2eAgFnSc2d1	učitelská
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
vzdělání	vzdělání	k1gNnSc2	vzdělání
získával	získávat	k5eAaImAgInS	získávat
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
na	na	k7c6	na
Vojenské	vojenský	k2eAgFnSc6d1	vojenská
akademii	akademie	k1gFnSc6	akademie
<g/>
.	.	kIx.	.
</s>
<s>
Vystřídal	vystřídat	k5eAaPmAgMnS	vystřídat
mnoho	mnoho	k4c4	mnoho
zaměstnání	zaměstnání	k1gNnPc2	zaměstnání
<g/>
.	.	kIx.	.
</s>
<s>
Nosič	nosič	k1gMnSc1	nosič
<g/>
,	,	kIx,	,
lyžař	lyžař	k1gMnSc1	lyžař
<g/>
,	,	kIx,	,
člen	člen	k1gMnSc1	člen
Horské	Horské	k2eAgFnSc2d1	Horské
služby	služba	k1gFnSc2	služba
<g/>
,	,	kIx,	,
chatař	chatař	k1gMnSc1	chatař
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
především	především	k9	především
spisovatel	spisovatel	k1gMnSc1	spisovatel
<g/>
.	.	kIx.	.
</s>
<s>
Tragicky	tragicky	k6eAd1	tragicky
zahynul	zahynout	k5eAaPmAgMnS	zahynout
na	na	k7c4	na
Hrebienku	Hrebienka	k1gFnSc4	Hrebienka
<g/>
,	,	kIx,	,
když	když	k8xS	když
si	se	k3xPyFc3	se
při	při	k7c6	při
dosednutí	dosednutí	k1gNnSc6	dosednutí
v	v	k7c6	v
autě	auto	k1gNnSc6	auto
prostřelil	prostřelit	k5eAaPmAgInS	prostřelit
tepnu	tepna	k1gFnSc4	tepna
ve	v	k7c6	v
slabinách	slabina	k1gFnPc6	slabina
devítkou	devítka	k1gFnSc7	devítka
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
měl	mít	k5eAaImAgMnS	mít
za	za	k7c7	za
opaskem	opasek	k1gInSc7	opasek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Tvorba	tvorba	k1gFnSc1	tvorba
==	==	k?	==
</s>
</p>
<p>
<s>
První	první	k4xOgNnPc4	první
literární	literární	k2eAgNnPc4d1	literární
díla	dílo	k1gNnPc4	dílo
začal	začít	k5eAaPmAgInS	začít
uveřejňovat	uveřejňovat	k5eAaImF	uveřejňovat
již	již	k6eAd1	již
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
60	[number]	k4	60
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
<g/>
,	,	kIx,	,
knižně	knižně	k6eAd1	knižně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1967	[number]	k4	1967
sbírkou	sbírka	k1gFnSc7	sbírka
povídek	povídka	k1gFnPc2	povídka
Kanadské	kanadský	k2eAgInPc1d1	kanadský
smrky	smrk	k1gInPc1	smrk
<g/>
.	.	kIx.	.
</s>
<s>
Věnoval	věnovat	k5eAaImAgInS	věnovat
se	se	k3xPyFc4	se
psaní	psaní	k1gNnSc2	psaní
zejména	zejména	k6eAd1	zejména
krátkých	krátký	k2eAgInPc2d1	krátký
prozaických	prozaický	k2eAgInPc2d1	prozaický
útvarů	útvar	k1gInPc2	útvar
(	(	kIx(	(
<g/>
beletrizované	beletrizovaný	k2eAgFnPc4d1	beletrizovaná
reportáže	reportáž	k1gFnPc4	reportáž
<g/>
,	,	kIx,	,
úvahový	úvahový	k2eAgInSc4d1	úvahový
rysy	rys	k1gInPc4	rys
<g/>
,	,	kIx,	,
povídky	povídka	k1gFnPc4	povídka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
čerpal	čerpat	k5eAaImAgInS	čerpat
z	z	k7c2	z
prostředí	prostředí	k1gNnSc2	prostředí
Vysokých	vysoký	k2eAgFnPc2d1	vysoká
Tater	Tatra	k1gFnPc2	Tatra
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgMnPc6	jenž
roky	rok	k1gInPc4	rok
žil	žít	k5eAaImAgMnS	žít
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
popisů	popis	k1gInPc2	popis
tatranské	tatranský	k2eAgFnSc2d1	Tatranská
přírody	příroda	k1gFnSc2	příroda
se	se	k3xPyFc4	se
zabýval	zabývat	k5eAaImAgMnS	zabývat
i	i	k9	i
ztvárněním	ztvárnění	k1gNnSc7	ztvárnění
života	život	k1gInSc2	život
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
horách	hora	k1gFnPc6	hora
pohybovali	pohybovat	k5eAaImAgMnP	pohybovat
<g/>
,	,	kIx,	,
zdůrazňoval	zdůrazňovat	k5eAaImAgMnS	zdůrazňovat
potřebu	potřeba	k1gFnSc4	potřeba
domova	domov	k1gInSc2	domov
a	a	k8xC	a
rodinné	rodinný	k2eAgFnSc2d1	rodinná
harmonie	harmonie	k1gFnSc2	harmonie
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc2	jeho
díla	dílo	k1gNnSc2	dílo
měla	mít	k5eAaImAgFnS	mít
poutavé	poutavý	k2eAgInPc4d1	poutavý
dějové	dějový	k2eAgInPc4d1	dějový
zápletky	zápletek	k1gInPc4	zápletek
i	i	k8xC	i
rozuzlení	rozuzlení	k1gNnSc4	rozuzlení
a	a	k8xC	a
nepostrádala	postrádat	k5eNaImAgFnS	postrádat
dramatičnost	dramatičnost	k1gFnSc4	dramatičnost
<g/>
,	,	kIx,	,
snažil	snažit	k5eAaImAgMnS	snažit
se	se	k3xPyFc4	se
o	o	k7c4	o
demolici	demolice	k1gFnSc4	demolice
mýtů	mýtus	k1gInPc2	mýtus
o	o	k7c6	o
horských	horský	k2eAgInPc6d1	horský
nosičích	nosič	k1gInPc6	nosič
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
se	se	k3xPyFc4	se
vyjadřoval	vyjadřovat	k5eAaImAgMnS	vyjadřovat
k	k	k7c3	k
problematice	problematika	k1gFnSc3	problematika
turistiky	turistika	k1gFnSc2	turistika
<g/>
,	,	kIx,	,
zimních	zimní	k2eAgInPc2d1	zimní
sportů	sport	k1gInPc2	sport
a	a	k8xC	a
problémům	problém	k1gInPc3	problém
tatranského	tatranský	k2eAgInSc2d1	tatranský
regionu	region	k1gInSc2	region
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nevyhýbal	vyhýbat	k5eNaImAgMnS	vyhýbat
se	se	k3xPyFc4	se
ani	ani	k8xC	ani
vztahům	vztah	k1gInPc3	vztah
a	a	k8xC	a
konfliktům	konflikt	k1gInPc3	konflikt
mezi	mezi	k7c7	mezi
rodiči	rodič	k1gMnPc7	rodič
a	a	k8xC	a
dětmi	dítě	k1gFnPc7	dítě
<g/>
,	,	kIx,	,
zobrazování	zobrazování	k1gNnSc4	zobrazování
světa	svět	k1gInSc2	svět
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
mladých	mladý	k2eAgMnPc2d1	mladý
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gInPc3	jejich
problémům	problém	k1gInPc3	problém
a	a	k8xC	a
specifikám	specifika	k1gFnPc3	specifika
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
na	na	k7c4	na
ně	on	k3xPp3gMnPc4	on
klade	klást	k5eAaImIp3nS	klást
život	život	k1gInSc1	život
v	v	k7c6	v
horském	horský	k2eAgNnSc6d1	horské
prostředí	prostředí	k1gNnSc6	prostředí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dílo	dílo	k1gNnSc1	dílo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Díla	dílo	k1gNnPc4	dílo
pro	pro	k7c4	pro
dospělé	dospělý	k2eAgInPc4d1	dospělý
===	===	k?	===
</s>
</p>
<p>
<s>
1967	[number]	k4	1967
–	–	k?	–
Kanadské	kanadský	k2eAgInPc1d1	kanadský
smreky	smrek	k1gInPc1	smrek
</s>
</p>
<p>
<s>
1999	[number]	k4	1999
–	–	k?	–
Strecha	Strecha	k1gMnSc1	Strecha
</s>
</p>
<p>
<s>
1975	[number]	k4	1975
–	–	k?	–
Chodníky	chodník	k1gInPc1	chodník
bez	bez	k7c2	bez
značiek	značiky	k1gFnPc2	značiky
</s>
</p>
<p>
<s>
1971	[number]	k4	1971
–	–	k?	–
Sivá	sivý	k2eAgFnSc1d1	sivá
hmla	hmla	k1gFnSc1	hmla
kryje	krýt	k5eAaImIp3nS	krýt
sýteho	sýteze	k6eAd1	sýteze
sysľa	sysľa	k6eAd1	sysľa
</s>
</p>
<p>
<s>
1936	[number]	k4	1936
–	–	k?	–
Aby	aby	kYmCp3nS	aby
nám	my	k3xPp1nPc3	my
neodišlo	odišnout	k5eNaPmAgNnS	odišnout
srdce	srdce	k1gNnSc1	srdce
</s>
</p>
<p>
<s>
1991	[number]	k4	1991
–	–	k?	–
Kapolkov	Kapolkov	k1gInSc1	Kapolkov
chodník	chodník	k1gInSc1	chodník
<g/>
,	,	kIx,	,
soubor	soubor	k1gInSc1	soubor
nepublikovaných	publikovaný	k2eNgFnPc2d1	nepublikovaná
esejí	esej	k1gFnPc2	esej
a	a	k8xC	a
úvah	úvaha	k1gFnPc2	úvaha
(	(	kIx(	(
<g/>
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
posmrtně	posmrtně	k6eAd1	posmrtně
<g/>
,	,	kIx,	,
sestavili	sestavit	k5eAaPmAgMnP	sestavit
Drahoslav	Drahoslava	k1gFnPc2	Drahoslava
Machala	Machala	k1gMnSc1	Machala
a	a	k8xC	a
Dušan	Dušan	k1gMnSc1	Dušan
Mikolaj	Mikolaj	k1gMnSc1	Mikolaj
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Díla	dílo	k1gNnPc4	dílo
pro	pro	k7c4	pro
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
mládež	mládež	k1gFnSc4	mládež
===	===	k?	===
</s>
</p>
<p>
<s>
1984	[number]	k4	1984
–	–	k?	–
Mesiac	Mesiac	k1gFnSc1	Mesiac
nad	nad	k7c7	nad
Prostredným	Prostredný	k2eAgInSc7d1	Prostredný
hrotom	hrotom	k1gInSc1	hrotom
(	(	kIx(	(
<g/>
zfilmováno	zfilmován	k2eAgNnSc1d1	zfilmováno
jako	jako	k8xC	jako
šestidílný	šestidílný	k2eAgInSc1d1	šestidílný
televizní	televizní	k2eAgInSc1d1	televizní
seriál	seriál	k1gInSc1	seriál
"	"	kIx"	"
<g/>
Chlapec	chlapec	k1gMnSc1	chlapec
a	a	k8xC	a
pes	pes	k1gMnSc1	pes
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
1989	[number]	k4	1989
<g/>
,	,	kIx,	,
režisér	režisér	k1gMnSc1	režisér
Ľubomír	Ľubomír	k1gMnSc1	Ľubomír
Fifík	Fifík	k1gMnSc1	Fifík
<g/>
))	))	k?	))
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Belo	Bela	k1gFnSc5	Bela
Kapolka	Kapolka	k1gFnSc1	Kapolka
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
