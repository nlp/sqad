<s>
Sturm	Sturm	k1gInSc1	Sturm
und	und	k?	und
Drang	Drang	k1gInSc1	Drang
(	(	kIx(	(
<g/>
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
tradičně	tradičně	k6eAd1	tradičně
překládáno	překládán	k2eAgNnSc1d1	překládáno
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Bouře	bouře	k1gFnSc1	bouře
a	a	k8xC	a
vzdor	vzdor	k1gInSc1	vzdor
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
německé	německý	k2eAgNnSc1d1	německé
preromantické	preromantický	k2eAgNnSc1d1	preromantický
literární	literární	k2eAgNnSc1d1	literární
hnutí	hnutí	k1gNnSc1	hnutí
<g/>
,	,	kIx,	,
působící	působící	k2eAgInSc1d1	působící
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
letech	let	k1gInPc6	let
1767	[number]	k4	1767
<g/>
–	–	k?	–
<g/>
1785	[number]	k4	1785
<g/>
.	.	kIx.	.
</s>
<s>
Sturm	Sturm	k1gInSc1	Sturm
und	und	k?	und
Drang	Dranga	k1gFnPc2	Dranga
získalo	získat	k5eAaPmAgNnS	získat
své	svůj	k3xOyFgNnSc1	svůj
jméno	jméno	k1gNnSc1	jméno
podle	podle	k7c2	podle
stejnojmenné	stejnojmenný	k2eAgFnSc2d1	stejnojmenná
hry	hra	k1gFnSc2	hra
Friedricha	Friedrich	k1gMnSc2	Friedrich
Maximiliana	Maximilian	k1gMnSc2	Maximilian
Klingera	Klinger	k1gMnSc2	Klinger
<g/>
.	.	kIx.	.
</s>
<s>
Název	název	k1gInSc1	název
se	se	k3xPyFc4	se
obtížně	obtížně	k6eAd1	obtížně
překládá	překládat	k5eAaImIp3nS	překládat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
Sturm	Sturm	k1gInSc1	Sturm
může	moct	k5eAaImIp3nS	moct
znamenat	znamenat	k5eAaImF	znamenat
jak	jak	k8xC	jak
bouři	bouře	k1gFnSc4	bouře
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
také	také	k9	také
útok	útok	k1gInSc1	útok
<g/>
,	,	kIx,	,
a	a	k8xC	a
Drang	Drang	k1gInSc1	Drang
je	být	k5eAaImIp3nS	být
také	také	k9	také
touha	touha	k1gFnSc1	touha
<g/>
,	,	kIx,	,
pud	pud	k1gInSc1	pud
<g/>
,	,	kIx,	,
nápor	nápor	k1gInSc1	nápor
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
svého	svůj	k3xOyFgInSc2	svůj
programu	program	k1gInSc2	program
se	se	k3xPyFc4	se
někdy	někdy	k6eAd1	někdy
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
éra	éra	k1gFnSc1	éra
géniů	génius	k1gMnPc2	génius
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
Geniezeit	Geniezeit	k2eAgInSc1d1	Geniezeit
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Rysy	rys	k1gInPc1	rys
jsou	být	k5eAaImIp3nP	být
téměř	téměř	k6eAd1	téměř
stejné	stejný	k2eAgFnPc1d1	stejná
jako	jako	k8xS	jako
u	u	k7c2	u
preromantismu	preromantismus	k1gInSc2	preromantismus
<g/>
.	.	kIx.	.
</s>
<s>
Idealizace	idealizace	k1gFnSc1	idealizace
individuální	individuální	k2eAgFnSc2d1	individuální
vzpoury	vzpoura	k1gFnSc2	vzpoura
jedince	jedinec	k1gMnSc2	jedinec
(	(	kIx(	(
<g/>
hrdina	hrdina	k1gMnSc1	hrdina
většinou	většina	k1gFnSc7	většina
končí	končit	k5eAaImIp3nS	končit
tragicky	tragicky	k6eAd1	tragicky
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
hnutí	hnutí	k1gNnSc1	hnutí
působilo	působit	k5eAaImAgNnS	působit
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
německé	německý	k2eAgFnSc6d1	německá
literatuře	literatura	k1gFnSc6	literatura
<g/>
,	,	kIx,	,
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
jako	jako	k8xS	jako
určitá	určitý	k2eAgFnSc1d1	určitá
reakce	reakce	k1gFnSc1	reakce
na	na	k7c4	na
éru	éra	k1gFnSc4	éra
racionalismu	racionalismus	k1gInSc2	racionalismus
a	a	k8xC	a
osvícenství	osvícenství	k1gNnSc2	osvícenství
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
tehdejších	tehdejší	k2eAgMnPc2d1	tehdejší
trendů	trend	k1gInPc2	trend
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
vyzdvihovaly	vyzdvihovat	k5eAaImAgFnP	vyzdvihovat
rozum	rozum	k1gInSc4	rozum
<g/>
,	,	kIx,	,
přišlo	přijít	k5eAaPmAgNnS	přijít
Sturm	Sturm	k1gInSc4	Sturm
und	und	k?	und
Drang	Drang	k1gInSc1	Drang
s	s	k7c7	s
návratem	návrat	k1gInSc7	návrat
k	k	k7c3	k
emocím	emoce	k1gFnPc3	emoce
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
snaha	snaha	k1gFnSc1	snaha
o	o	k7c4	o
emotivnost	emotivnost	k1gFnSc4	emotivnost
vyústila	vyústit	k5eAaPmAgFnS	vyústit
ve	v	k7c4	v
snahu	snaha	k1gFnSc4	snaha
o	o	k7c4	o
rozvoj	rozvoj	k1gInSc4	rozvoj
geniální	geniální	k2eAgFnSc2d1	geniální
osobnosti	osobnost	k1gFnSc2	osobnost
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
by	by	kYmCp3nS	by
nejednala	jednat	k5eNaImAgFnS	jednat
racionálně	racionálně	k6eAd1	racionálně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
právě	právě	k9	právě
naopak	naopak	k6eAd1	naopak
rozvíjela	rozvíjet	k5eAaImAgFnS	rozvíjet
svoji	svůj	k3xOyFgFnSc4	svůj
emocionalitu	emocionalita	k1gFnSc4	emocionalita
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svých	svůj	k3xOyFgInPc6	svůj
dílech	díl	k1gInPc6	díl
tak	tak	k9	tak
tito	tento	k3xDgMnPc1	tento
literáti	literát	k1gMnPc1	literát
kladli	klást	k5eAaImAgMnP	klást
velký	velký	k2eAgInSc4d1	velký
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
cit	cit	k1gInSc4	cit
a	a	k8xC	a
fantazii	fantazie	k1gFnSc4	fantazie
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
nutně	nutně	k6eAd1	nutně
vedlo	vést	k5eAaImAgNnS	vést
ke	k	k7c3	k
zdůrazňování	zdůrazňování	k1gNnSc3	zdůrazňování
tvůrčí	tvůrčí	k2eAgFnSc2d1	tvůrčí
i	i	k8xC	i
osobní	osobní	k2eAgFnSc2d1	osobní
svobody	svoboda	k1gFnSc2	svoboda
a	a	k8xC	a
nadřazenosti	nadřazenost	k1gFnSc2	nadřazenost
osobnosti	osobnost	k1gFnSc2	osobnost
nad	nad	k7c7	nad
pravidly	pravidlo	k1gNnPc7	pravidlo
a	a	k8xC	a
autoritami	autorita	k1gFnPc7	autorita
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
myšlenek	myšlenka	k1gFnPc2	myšlenka
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
požadavek	požadavek	k1gInSc1	požadavek
na	na	k7c4	na
originalitu	originalita	k1gFnSc4	originalita
<g/>
,	,	kIx,	,
inspirací	inspirace	k1gFnPc2	inspirace
k	k	k7c3	k
jednotlivým	jednotlivý	k2eAgInPc3d1	jednotlivý
dílům	díl	k1gInPc3	díl
byla	být	k5eAaImAgFnS	být
především	především	k9	především
příroda	příroda	k1gFnSc1	příroda
a	a	k8xC	a
lidová	lidový	k2eAgFnSc1d1	lidová
slovesnost	slovesnost	k1gFnSc1	slovesnost
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgFnPc1	tento
myšlenky	myšlenka	k1gFnPc1	myšlenka
vyústily	vyústit	k5eAaPmAgFnP	vyústit
ve	v	k7c4	v
vzpouru	vzpoura	k1gFnSc4	vzpoura
mladé	mladý	k2eAgFnSc2d1	mladá
generace	generace	k1gFnSc2	generace
proti	proti	k7c3	proti
konvencím	konvence	k1gFnPc3	konvence
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
nejen	nejen	k6eAd1	nejen
literárním	literární	k2eAgInSc7d1	literární
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
postupně	postupně	k6eAd1	postupně
i	i	k8xC	i
společenským	společenský	k2eAgNnPc3d1	společenské
<g/>
.	.	kIx.	.
</s>
<s>
Nejpříhodnějším	příhodný	k2eAgInSc7d3	nejpříhodnější
literárním	literární	k2eAgInSc7d1	literární
útvarem	útvar	k1gInSc7	útvar
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnSc4	jejich
díla	dílo	k1gNnSc2	dílo
bylo	být	k5eAaImAgNnS	být
drama	drama	k1gNnSc1	drama
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
nejvíce	nejvíce	k6eAd1	nejvíce
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
naplnit	naplnit	k5eAaPmF	naplnit
tyto	tento	k3xDgInPc4	tento
ideály	ideál	k1gInPc4	ideál
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
omezené	omezený	k2eAgFnSc6d1	omezená
míře	míra	k1gFnSc6	míra
se	se	k3xPyFc4	se
toto	tento	k3xDgNnSc1	tento
hnutí	hnutí	k1gNnSc1	hnutí
odrazilo	odrazit	k5eAaPmAgNnS	odrazit
i	i	k9	i
v	v	k7c6	v
hudbě	hudba	k1gFnSc6	hudba
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
však	však	k9	však
nedosáhlo	dosáhnout	k5eNaPmAgNnS	dosáhnout
takového	takový	k3xDgInSc2	takový
významu	význam	k1gInSc2	význam
jako	jako	k9	jako
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
<g/>
.	.	kIx.	.
</s>
<s>
Myšlenky	myšlenka	k1gFnPc1	myšlenka
hnutí	hnutí	k1gNnSc2	hnutí
silně	silně	k6eAd1	silně
ovlivnily	ovlivnit	k5eAaPmAgFnP	ovlivnit
německý	německý	k2eAgInSc4d1	německý
romantismus	romantismus	k1gInSc4	romantismus
<g/>
.	.	kIx.	.
</s>
<s>
Patrný	patrný	k2eAgInSc1d1	patrný
byl	být	k5eAaImAgInS	být
zejména	zejména	k9	zejména
vliv	vliv	k1gInSc1	vliv
Schillera	Schiller	k1gMnSc2	Schiller
a	a	k8xC	a
důraz	důraz	k1gInSc4	důraz
na	na	k7c4	na
iracionalismus	iracionalismus	k1gInSc4	iracionalismus
<g/>
.	.	kIx.	.
</s>
<s>
Johann	Johann	k1gMnSc1	Johann
Wolfgang	Wolfgang	k1gMnSc1	Wolfgang
von	von	k1gInSc4	von
Goethe	Goeth	k1gInSc2	Goeth
Friedrich	Friedrich	k1gMnSc1	Friedrich
Schiller	Schiller	k1gMnSc1	Schiller
(	(	kIx(	(
<g/>
v	v	k7c6	v
mladším	mladý	k2eAgInSc6d2	mladší
věku	věk	k1gInSc6	věk
<g/>
)	)	kIx)	)
Friedrich	Friedrich	k1gMnSc1	Friedrich
Maximilian	Maximilian	k1gMnSc1	Maximilian
Klinger	Klinger	k1gMnSc1	Klinger
Jakob	Jakob	k1gMnSc1	Jakob
Michael	Michael	k1gMnSc1	Michael
Reinhold	Reinhold	k1gMnSc1	Reinhold
Lenz	Lenz	k1gMnSc1	Lenz
Gottfried	Gottfried	k1gMnSc1	Gottfried
August	August	k1gMnSc1	August
Bürger	Bürger	k1gMnSc1	Bürger
Heinrich	Heinrich	k1gMnSc1	Heinrich
Wilhelm	Wilhelma	k1gFnPc2	Wilhelma
von	von	k1gInSc1	von
Gerstenberg	Gerstenberg	k1gMnSc1	Gerstenberg
Johann	Johann	k1gMnSc1	Johann
Georg	Georg	k1gMnSc1	Georg
Hamann	Hamann	k1gMnSc1	Hamann
Johann	Johann	k1gMnSc1	Johann
Jakob	Jakob	k1gMnSc1	Jakob
Wilhelm	Wilhelm	k1gMnSc1	Wilhelm
Heinse	Heinse	k1gFnSc2	Heinse
Johann	Johann	k1gMnSc1	Johann
Gottfried	Gottfried	k1gMnSc1	Gottfried
Herder	Herder	k1gMnSc1	Herder
</s>
