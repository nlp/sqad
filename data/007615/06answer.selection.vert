<s>
Sturm	Sturm	k1gInSc1	Sturm
und	und	k?	und
Drang	Drang	k1gInSc1	Drang
(	(	kIx(	(
<g/>
do	do	k7c2	do
češtiny	čeština	k1gFnSc2	čeština
tradičně	tradičně	k6eAd1	tradičně
překládáno	překládán	k2eAgNnSc1d1	překládáno
jako	jako	k8xC	jako
"	"	kIx"	"
<g/>
Bouře	bouře	k1gFnSc1	bouře
a	a	k8xC	a
vzdor	vzdor	k1gInSc1	vzdor
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
bylo	být	k5eAaImAgNnS	být
německé	německý	k2eAgNnSc1d1	německé
preromantické	preromantický	k2eAgNnSc1d1	preromantický
literární	literární	k2eAgNnSc1d1	literární
hnutí	hnutí	k1gNnSc1	hnutí
<g/>
,	,	kIx,	,
působící	působící	k2eAgInSc1d1	působící
v	v	k7c6	v
18	[number]	k4	18
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
v	v	k7c6	v
letech	let	k1gInPc6	let
1767	[number]	k4	1767
<g/>
–	–	k?	–
<g/>
1785	[number]	k4	1785
<g/>
.	.	kIx.	.
</s>
