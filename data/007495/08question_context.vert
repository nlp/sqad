<s>
Od	od	k7c2
roku	rok	k1gInSc2
1999	[number]	k4
je	být	k5eAaImIp3nS
hrad	hrad	k1gInSc4
Veveří	veveří	k2eAgInSc4d1
ve	v	k7c6
správě	správa	k1gFnSc6
Národního	národní	k2eAgInSc2d1
památkového	památkový	k2eAgInSc2d1
ústavu	ústav	k1gInSc2
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
,	,	kIx,
který	který	k3yRgInSc1
zde	zde	k6eAd1
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
provádí	provádět	k5eAaImIp3nS
postupnou	postupný	k2eAgFnSc4d1
rekonstrukci	rekonstrukce	k1gFnSc4
celého	celý	k2eAgInSc2d1
značně	značně	k6eAd1
zdevastovaného	zdevastovaný	k2eAgInSc2d1
areálu	areál	k1gInSc2
<g/>
.	.	kIx.
</s>