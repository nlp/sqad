<p>
<s>
V	v	k7c6	v
polské	polský	k2eAgFnSc6d1	polská
Vratislavi	Vratislav	k1gFnSc6	Vratislav
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
začátku	začátek	k1gInSc6	začátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
krátký	krátký	k2eAgInSc4d1	krátký
čas	čas	k1gInSc4	čas
v	v	k7c6	v
provozu	provoz	k1gInSc6	provoz
první	první	k4xOgFnSc1	první
trolejbusová	trolejbusový	k2eAgFnSc1d1	trolejbusová
síť	síť	k1gFnSc1	síť
na	na	k7c6	na
území	území	k1gNnSc6	území
dnešního	dnešní	k2eAgNnSc2d1	dnešní
Polska	Polsko	k1gNnSc2	Polsko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
První	první	k4xOgMnSc1	první
<g/>
,	,	kIx,	,
a	a	k8xC	a
také	také	k9	také
jediná	jediný	k2eAgFnSc1d1	jediná
vratislavská	vratislavský	k2eAgFnSc1d1	Vratislavská
trolejbusová	trolejbusový	k2eAgFnSc1d1	trolejbusová
trať	trať	k1gFnSc1	trať
(	(	kIx(	(
<g/>
systému	systém	k1gInSc2	systém
Lloyd	Lloyd	k1gMnSc1	Lloyd
–	–	k?	–
Kohler	Kohler	k1gMnSc1	Kohler
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
zprovozněna	zprovozněn	k2eAgFnSc1d1	zprovozněna
20	[number]	k4	20
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1912	[number]	k4	1912
společností	společnost	k1gFnPc2	společnost
Gleislose	Gleislosa	k1gFnSc3	Gleislosa
Bahn	Bahn	k1gNnSc1	Bahn
Brockau	Brockaus	k1gInSc2	Brockaus
GmbH	GmbH	k1gFnSc2	GmbH
(	(	kIx(	(
<g/>
Bezkolejová	bezkolejový	k2eAgFnSc1d1	bezkolejový
dráha	dráha	k1gFnSc1	dráha
Brochów	Brochów	k1gFnSc2	Brochów
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Linka	linka	k1gFnSc1	linka
byla	být	k5eAaImAgFnS	být
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
4,4	[number]	k4	4,4
km	km	kA	km
a	a	k8xC	a
vedla	vést	k5eAaImAgFnS	vést
od	od	k7c2	od
tramvajové	tramvajový	k2eAgFnSc2d1	tramvajová
vozovny	vozovna	k1gFnSc2	vozovna
ve	v	k7c4	v
Wilczym	Wilczym	k1gInSc4	Wilczym
Kątu	Kąta	k1gFnSc4	Kąta
do	do	k7c2	do
tehdy	tehdy	k6eAd1	tehdy
samostatného	samostatný	k2eAgNnSc2d1	samostatné
města	město	k1gNnSc2	město
Brochów	Brochów	k1gFnSc2	Brochów
(	(	kIx(	(
<g/>
nyní	nyní	k6eAd1	nyní
část	část	k1gFnSc1	část
Vratislavi	Vratislav	k1gFnSc2	Vratislav
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Kvůli	kvůli	k7c3	kvůli
technickým	technický	k2eAgInPc3d1	technický
problémům	problém	k1gInPc3	problém
ale	ale	k8xC	ale
byla	být	k5eAaImAgNnP	být
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1913	[number]	k4	1913
zrušena	zrušit	k5eAaPmNgFnS	zrušit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Provoz	provoz	k1gInSc1	provoz
obstarávaly	obstarávat	k5eAaImAgInP	obstarávat
čtyři	čtyři	k4xCgInPc1	čtyři
trolejbusy	trolejbus	k1gInPc1	trolejbus
a	a	k8xC	a
dva	dva	k4xCgInPc4	dva
vlečné	vlečný	k2eAgInPc4d1	vlečný
vozy	vůz	k1gInPc4	vůz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1947	[number]	k4	1947
se	se	k3xPyFc4	se
ve	v	k7c6	v
Vratislavi	Vratislav	k1gFnSc6	Vratislav
vážně	vážně	k6eAd1	vážně
uvažovalo	uvažovat	k5eAaImAgNnS	uvažovat
o	o	k7c6	o
zprovoznění	zprovoznění	k1gNnSc6	zprovoznění
několika	několik	k4yIc2	několik
trolejbusových	trolejbusový	k2eAgFnPc2d1	trolejbusová
linek	linka	k1gFnPc2	linka
<g/>
.	.	kIx.	.
</s>
<s>
Dokonce	dokonce	k9	dokonce
bylo	být	k5eAaImAgNnS	být
do	do	k7c2	do
města	město	k1gNnSc2	město
dovezeno	dovézt	k5eAaPmNgNnS	dovézt
sedm	sedm	k4xCc1	sedm
vozů	vůz	k1gInPc2	vůz
Fiat	fiat	k1gInSc4	fiat
z	z	k7c2	z
Wałbrzychu	Wałbrzych	k1gInSc2	Wałbrzych
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
ale	ale	k9	ale
byly	být	k5eAaImAgFnP	být
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
později	pozdě	k6eAd2	pozdě
předány	předat	k5eAaPmNgFnP	předat
do	do	k7c2	do
Gdyně	Gdyně	k1gFnSc2	Gdyně
a	a	k8xC	a
trolejbusy	trolejbus	k1gInPc1	trolejbus
se	se	k3xPyFc4	se
tak	tak	k9	tak
po	po	k7c6	po
Vratislavi	Vratislav	k1gFnSc6	Vratislav
již	již	k6eAd1	již
nikdy	nikdy	k6eAd1	nikdy
nerozjely	rozjet	k5eNaPmAgFnP	rozjet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
polsky	polsky	k6eAd1	polsky
<g/>
)	)	kIx)	)
Krátký	krátký	k2eAgInSc1d1	krátký
popis	popis	k1gInSc1	popis
s	s	k7c7	s
fotkou	fotka	k1gFnSc7	fotka
vratislavských	vratislavský	k2eAgFnPc2d1	Vratislavská
trolejbusůV	trolejbusůV	k?	trolejbusůV
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Trolejbusy	trolejbus	k1gInPc4	trolejbus
we	we	k?	we
Wrocławiu	Wrocławium	k1gNnSc6	Wrocławium
na	na	k7c6	na
polské	polský	k2eAgFnSc6d1	polská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
