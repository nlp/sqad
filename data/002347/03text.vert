<s>
Epiglotitida	Epiglotitida	k1gFnSc1	Epiglotitida
je	být	k5eAaImIp3nS	být
zánět	zánět	k1gInSc4	zánět
epiglottis	epiglottis	k1gFnSc2	epiglottis
-	-	kIx~	-
příklopky	příklopka	k1gFnSc2	příklopka
hrtanové	hrtanový	k2eAgFnSc2d1	hrtanová
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
otok	otok	k1gInSc1	otok
může	moct	k5eAaImIp3nS	moct
způsobit	způsobit	k5eAaPmF	způsobit
problémy	problém	k1gInPc4	problém
s	s	k7c7	s
dýcháním	dýchání	k1gNnSc7	dýchání
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
úplné	úplný	k2eAgNnSc1d1	úplné
ucpání	ucpání	k1gNnSc1	ucpání
průdušnice	průdušnice	k1gFnSc2	průdušnice
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
je	být	k5eAaImIp3nS	být
nutné	nutný	k2eAgNnSc1d1	nutné
ihned	ihned	k6eAd1	ihned
vyhledat	vyhledat	k5eAaPmF	vyhledat
lékařskou	lékařský	k2eAgFnSc4d1	lékařská
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
epiglotitidu	epiglotitida	k1gFnSc4	epiglotitida
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
zemřel	zemřít	k5eAaPmAgMnS	zemřít
americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
George	Georg	k1gFnSc2	Georg
Washington	Washington	k1gInSc1	Washington
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
zánět	zánět	k1gInSc1	zánět
je	být	k5eAaImIp3nS	být
nejčastěji	často	k6eAd3	často
způsoben	způsobit	k5eAaPmNgInS	způsobit
bakterií	bakterie	k1gFnSc7	bakterie
Haemophilus	Haemophilus	k1gMnSc1	Haemophilus
influenzae	influenza	k1gFnSc2	influenza
typu	typ	k1gInSc2	typ
B.	B.	kA	B.
Jsou	být	k5eAaImIp3nP	být
ale	ale	k9	ale
známy	znám	k2eAgInPc1d1	znám
i	i	k8xC	i
případy	případ	k1gInPc1	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
bakterie	bakterie	k1gFnPc4	bakterie
Streptococcus	Streptococcus	k1gInSc4	Streptococcus
pneumoniae	pneumoniae	k1gNnSc1	pneumoniae
<g/>
,	,	kIx,	,
Streptococcus	Streptococcus	k1gInSc1	Streptococcus
agalactiae	agalactia	k1gFnSc2	agalactia
<g/>
,	,	kIx,	,
Staphylococcus	Staphylococcus	k1gMnSc1	Staphylococcus
aureus	aureus	k1gMnSc1	aureus
<g/>
,	,	kIx,	,
Streptococcus	Streptococcus	k1gMnSc1	Streptococcus
pyogenes	pyogenes	k1gMnSc1	pyogenes
<g/>
,	,	kIx,	,
Haemophilus	Haemophilus	k1gMnSc1	Haemophilus
influenzae	influenzaat	k5eAaPmIp3nS	influenzaat
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
Moraxella	Moraxella	k1gFnSc1	Moraxella
catarrhalis	catarrhalis	k1gFnSc1	catarrhalis
<g/>
.	.	kIx.	.
</s>
<s>
Zánět	zánět	k1gInSc1	zánět
postihuje	postihovat	k5eAaImIp3nS	postihovat
nejčastěji	často	k6eAd3	často
děti	dítě	k1gFnPc4	dítě
<g/>
,	,	kIx,	,
projevuje	projevovat	k5eAaImIp3nS	projevovat
se	se	k3xPyFc4	se
horečkou	horečka	k1gFnSc7	horečka
<g/>
,	,	kIx,	,
obtížným	obtížný	k2eAgNnSc7d1	obtížné
polykáním	polykání	k1gNnSc7	polykání
<g/>
,	,	kIx,	,
slintáním	slintání	k1gNnSc7	slintání
<g/>
,	,	kIx,	,
chrapotem	chrapot	k1gInSc7	chrapot
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
typickým	typický	k2eAgNnSc7d1	typické
chrčením	chrčení	k1gNnSc7	chrčení
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
způsobuje	způsobovat	k5eAaImIp3nS	způsobovat
obstrukce	obstrukce	k1gFnSc1	obstrukce
horních	horní	k2eAgFnPc2d1	horní
cest	cesta	k1gFnPc2	cesta
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
<g/>
.	.	kIx.	.
</s>
<s>
Dýchání	dýchání	k1gNnSc1	dýchání
je	být	k5eAaImIp3nS	být
povrchní	povrchní	k2eAgNnSc1d1	povrchní
<g/>
,	,	kIx,	,
s	s	k7c7	s
nepřirozeně	přirozeně	k6eNd1	přirozeně
natáhnutým	natáhnutý	k2eAgInSc7d1	natáhnutý
krkem	krk	k1gInSc7	krk
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc1	první
symptomy	symptom	k1gInPc1	symptom
jsou	být	k5eAaImIp3nP	být
nenápadné	nápadný	k2eNgInPc1d1	nenápadný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
rychle	rychle	k6eAd1	rychle
se	se	k3xPyFc4	se
vyvíjí	vyvíjet	k5eAaImIp3nS	vyvíjet
a	a	k8xC	a
otok	otok	k1gInSc1	otok
může	moct	k5eAaImIp3nS	moct
vést	vést	k5eAaImF	vést
až	až	k9	až
k	k	k7c3	k
udušení	udušení	k1gNnSc3	udušení
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
akutním	akutní	k2eAgInSc6d1	akutní
stavu	stav	k1gInSc6	stav
je	být	k5eAaImIp3nS	být
nutná	nutný	k2eAgFnSc1d1	nutná
intubace	intubace	k1gFnSc1	intubace
<g/>
.	.	kIx.	.
</s>
<s>
Nemoc	nemoc	k1gFnSc1	nemoc
se	se	k3xPyFc4	se
diagnostikuje	diagnostikovat	k5eAaBmIp3nS	diagnostikovat
pomocí	pomocí	k7c2	pomocí
laryngoskopického	laryngoskopický	k2eAgNnSc2d1	laryngoskopický
vyšetření	vyšetření	k1gNnSc2	vyšetření
<g/>
,	,	kIx,	,
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
něhož	jenž	k3xRgNnSc2	jenž
může	moct	k5eAaImIp3nS	moct
při	při	k7c6	při
akutní	akutní	k2eAgFnSc6d1	akutní
epiglotitidě	epiglotitida	k1gFnSc6	epiglotitida
dojít	dojít	k5eAaPmF	dojít
ke	k	k7c3	k
stahu	stah	k1gInSc3	stah
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
cest	cesta	k1gFnPc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
se	se	k3xPyFc4	se
využívá	využívat	k5eAaPmIp3nS	využívat
také	také	k9	také
CT	CT	kA	CT
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
němuž	jenž	k3xRgNnSc3	jenž
lze	lze	k6eAd1	lze
podle	podle	k7c2	podle
velikosti	velikost	k1gFnSc2	velikost
příklopky	příklopka	k1gFnSc2	příklopka
zjistit	zjistit	k5eAaPmF	zjistit
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
akutní	akutní	k2eAgFnSc4d1	akutní
epiglottidu	epiglottida	k1gFnSc4	epiglottida
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
některých	některý	k3yIgMnPc2	některý
pacientů	pacient	k1gMnPc2	pacient
se	se	k3xPyFc4	se
objeví	objevit	k5eAaPmIp3nP	objevit
komplikace	komplikace	k1gFnPc1	komplikace
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
zápalu	zápal	k1gInSc2	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
lymfadenopatie	lymfadenopatie	k1gFnSc2	lymfadenopatie
nebo	nebo	k8xC	nebo
septické	septický	k2eAgFnSc2d1	septická
artritidy	artritida	k1gFnSc2	artritida
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
objevení	objevení	k1gNnSc6	objevení
Hib	Hib	k1gFnSc2	Hib
vakcíny	vakcína	k1gFnSc2	vakcína
Haemophilus	Haemophilus	k1gMnSc1	Haemophilus
influenzae	influenzae	k1gNnSc2	influenzae
typu	typ	k1gInSc2	typ
B	B	kA	B
počet	počet	k1gInSc1	počet
postižených	postižený	k1gMnPc2	postižený
výrazně	výrazně	k6eAd1	výrazně
klesl	klesnout	k5eAaPmAgInS	klesnout
<g/>
.	.	kIx.	.
antibiotikum	antibiotikum	k1gNnSc4	antibiotikum
tracheální	tracheální	k2eAgFnSc2d1	tracheální
intubace	intubace	k1gFnSc2	intubace
chirurgické	chirurgický	k2eAgNnSc4d1	chirurgické
uvolnění	uvolnění	k1gNnSc4	uvolnění
dýchacích	dýchací	k2eAgFnPc2d1	dýchací
cest	cesta	k1gFnPc2	cesta
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Epiglotitida	Epiglotitida	k1gFnSc1	Epiglotitida
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
Jordana	Jordan	k1gMnSc2	Jordan
Marinoff	Marinoff	k1gInSc4	Marinoff
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Bacteria	Bacterium	k1gNnPc1	Bacterium
Grab	Graba	k1gFnPc2	Graba
a	a	k8xC	a
Windpipe	Windpip	k1gInSc5	Windpip
and	and	k?	and
Hold	hold	k1gInSc1	hold
it	it	k?	it
Hostage	Hostage	k1gInSc1	Hostage
<g/>
,	,	kIx,	,
<g/>
"	"	kIx"	"
Boston	Boston	k1gInSc1	Boston
Globe	globus	k1gInSc5	globus
<g/>
,	,	kIx,	,
10	[number]	k4	10
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
2006	[number]	k4	2006
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
Informace	informace	k1gFnPc1	informace
z	z	k7c2	z
Medscape	Medscap	k1gInSc5	Medscap
</s>
