<p>
<s>
Pivot	pivot	k1gInSc1	pivot
(	(	kIx(	(
<g/>
také	také	k9	také
podkošový	podkošový	k2eAgMnSc1d1	podkošový
hráč	hráč	k1gMnSc1	hráč
<g/>
,	,	kIx,	,
pivotman	pivotman	k1gMnSc1	pivotman
nebo	nebo	k8xC	nebo
centr	centr	k1gMnSc1	centr
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
basketbalový	basketbalový	k2eAgInSc4d1	basketbalový
výraz	výraz	k1gInSc4	výraz
pro	pro	k7c4	pro
postavení	postavení	k1gNnSc4	postavení
hráče	hráč	k1gMnSc2	hráč
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnPc4	charakteristikon
pozice	pozice	k1gFnSc2	pozice
==	==	k?	==
</s>
</p>
<p>
<s>
Pivot	pivot	k1gInSc1	pivot
hraje	hrát	k5eAaImIp3nS	hrát
v	v	k7c6	v
útoku	útok	k1gInSc6	útok
obvykle	obvykle	k6eAd1	obvykle
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
koše	koš	k1gInSc2	koš
<g/>
.	.	kIx.	.
</s>
<s>
Střílí	střílet	k5eAaImIp3nS	střílet
z	z	k7c2	z
malé	malý	k2eAgFnSc2d1	malá
nebo	nebo	k8xC	nebo
střední	střední	k2eAgFnSc2d1	střední
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
od	od	k7c2	od
koše	koš	k1gInSc2	koš
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gInSc7	jeho
důležitým	důležitý	k2eAgInSc7d1	důležitý
úkolem	úkol	k1gInSc7	úkol
je	být	k5eAaImIp3nS	být
získávání	získávání	k1gNnSc1	získávání
míče	míč	k1gInSc2	míč
odraženého	odražený	k2eAgInSc2d1	odražený
po	po	k7c6	po
nepřesné	přesný	k2eNgFnSc6d1	nepřesná
střele	střela	k1gFnSc6	střela
–	–	k?	–
tzv.	tzv.	kA	tzv.
doskok	doskok	k1gInSc1	doskok
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
obraně	obrana	k1gFnSc6	obrana
jsou	být	k5eAaImIp3nP	být
pivoti	pivot	k1gMnPc1	pivot
cenní	cenný	k2eAgMnPc1d1	cenný
hlavně	hlavně	k6eAd1	hlavně
svojí	svůj	k3xOyFgFnSc7	svůj
schopností	schopnost	k1gFnSc7	schopnost
blokovat	blokovat	k5eAaImF	blokovat
střely	střel	k1gInPc4	střel
a	a	k8xC	a
doskakováním	doskakování	k1gNnSc7	doskakování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
pozici	pozice	k1gFnSc4	pozice
pivota	pivot	k1gMnSc2	pivot
jsou	být	k5eAaImIp3nP	být
obvykle	obvykle	k6eAd1	obvykle
vybíráni	vybírán	k2eAgMnPc1d1	vybírán
nejvyšší	vysoký	k2eAgMnPc1d3	nejvyšší
hráči	hráč	k1gMnPc1	hráč
v	v	k7c6	v
mužstvu	mužstvo	k1gNnSc6	mužstvo
–	–	k?	–
dalo	dát	k5eAaPmAgNnS	dát
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
čím	co	k3yRnSc7	co
vyšší	vysoký	k2eAgInSc1d2	vyšší
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
lepší	dobrý	k2eAgMnSc1d2	lepší
<g/>
.	.	kIx.	.
</s>
<s>
Typickým	typický	k2eAgInSc7d1	typický
příkladem	příklad	k1gInSc7	příklad
klasického	klasický	k2eAgMnSc2d1	klasický
pivota	pivot	k1gMnSc2	pivot
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
byl	být	k5eAaImAgInS	být
se	s	k7c7	s
svými	svůj	k3xOyFgInPc7	svůj
218	[number]	k4	218
centimetry	centimetr	k1gInPc7	centimetr
a	a	k8xC	a
velmi	velmi	k6eAd1	velmi
dobrou	dobrý	k2eAgFnSc7d1	dobrá
pohyblivostí	pohyblivost	k1gFnSc7	pohyblivost
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
velké	velký	k2eAgFnSc3d1	velká
výšce	výška	k1gFnSc3	výška
Kareem	Kareum	k1gNnSc7	Kareum
Abdul-Jabbar	Abdul-Jabbar	k1gMnSc1	Abdul-Jabbar
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Druhou	druhý	k4xOgFnSc7	druhý
možností	možnost	k1gFnSc7	možnost
jsou	být	k5eAaImIp3nP	být
byť	byť	k8xS	byť
i	i	k9	i
menší	malý	k2eAgMnPc1d2	menší
hráči	hráč	k1gMnPc1	hráč
s	s	k7c7	s
velkou	velký	k2eAgFnSc7d1	velká
silou	síla	k1gFnSc7	síla
<g/>
,	,	kIx,	,
schopní	schopný	k2eAgMnPc1d1	schopný
"	"	kIx"	"
<g/>
přetlačit	přetlačit	k5eAaPmF	přetlačit
<g/>
"	"	kIx"	"
tělem	tělo	k1gNnSc7	tělo
soupeře	soupeř	k1gMnSc4	soupeř
a	a	k8xC	a
vytvořit	vytvořit	k5eAaPmF	vytvořit
si	se	k3xPyFc3	se
prostor	prostor	k1gInSc4	prostor
při	při	k7c6	při
doskakování	doskakování	k1gNnSc6	doskakování
–	–	k?	–
klasickým	klasický	k2eAgInSc7d1	klasický
příkladem	příklad	k1gInSc7	příklad
tohoto	tento	k3xDgInSc2	tento
typu	typ	k1gInSc2	typ
pivota	pivot	k1gMnSc2	pivot
byl	být	k5eAaImAgMnS	být
Moses	Moses	k1gMnSc1	Moses
Malone	Malon	k1gMnSc5	Malon
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
sem	sem	k6eAd1	sem
dal	dát	k5eAaPmAgInS	dát
zařadit	zařadit	k5eAaPmF	zařadit
i	i	k9	i
silový	silový	k2eAgInSc4d1	silový
křídelník	křídelník	k1gInSc4	křídelník
Charles	Charles	k1gMnSc1	Charles
Barkley	Barklea	k1gMnSc2	Barklea
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
pětičlenné	pětičlenný	k2eAgFnSc6d1	pětičlenná
basketbalové	basketbalový	k2eAgFnSc6d1	basketbalová
sestavě	sestava	k1gFnSc6	sestava
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
jeden	jeden	k4xCgInSc4	jeden
pivot	pivot	k1gInSc4	pivot
<g/>
,	,	kIx,	,
v	v	k7c6	v
některých	některý	k3yIgFnPc6	některý
herních	herní	k2eAgFnPc6d1	herní
variantách	varianta	k1gFnPc6	varianta
dva	dva	k4xCgInPc4	dva
<g/>
.	.	kIx.	.
</s>
<s>
Pivot	pivot	k1gInSc1	pivot
schopný	schopný	k2eAgInSc1d1	schopný
rychlého	rychlý	k2eAgInSc2d1	rychlý
pohybu	pohyb	k1gInSc2	pohyb
a	a	k8xC	a
střelby	střelba	k1gFnSc2	střelba
z	z	k7c2	z
větší	veliký	k2eAgFnSc2d2	veliký
vzdálenosti	vzdálenost	k1gFnSc2	vzdálenost
může	moct	k5eAaImIp3nS	moct
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
kombinovat	kombinovat	k5eAaImF	kombinovat
při	při	k7c6	při
hře	hra	k1gFnSc6	hra
funkci	funkce	k1gFnSc6	funkce
pivota	pivot	k1gMnSc4	pivot
a	a	k8xC	a
křídla	křídlo	k1gNnPc4	křídlo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Americké	americký	k2eAgNnSc1d1	americké
členění	členění	k1gNnSc1	členění
basketbalových	basketbalový	k2eAgFnPc2d1	basketbalová
pozic	pozice	k1gFnPc2	pozice
==	==	k?	==
</s>
</p>
<p>
<s>
Americké	americký	k2eAgNnSc1d1	americké
členění	členění	k1gNnSc1	členění
hráčských	hráčský	k2eAgFnPc2d1	hráčská
pozic	pozice	k1gFnPc2	pozice
se	se	k3xPyFc4	se
od	od	k7c2	od
evropského	evropský	k2eAgInSc2d1	evropský
(	(	kIx(	(
<g/>
rozehrávač	rozehrávač	k1gMnSc1	rozehrávač
–	–	k?	–
dvě	dva	k4xCgNnPc4	dva
křídla	křídlo	k1gNnPc4	křídlo
–	–	k?	–
dva	dva	k4xCgMnPc1	dva
pivoti	pivot	k1gMnPc1	pivot
<g/>
)	)	kIx)	)
do	do	k7c2	do
jisté	jistý	k2eAgFnSc2d1	jistá
míry	míra	k1gFnSc2	míra
liší	lišit	k5eAaImIp3nP	lišit
<g/>
.	.	kIx.	.
</s>
<s>
Českému	český	k2eAgInSc3d1	český
pojmu	pojem	k1gInSc3	pojem
"	"	kIx"	"
<g/>
pivot	pivot	k1gInSc1	pivot
<g/>
"	"	kIx"	"
odpovídá	odpovídat	k5eAaImIp3nS	odpovídat
nejlépe	dobře	k6eAd3	dobře
termín	termín	k1gInSc1	termín
center	centrum	k1gNnPc2	centrum
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
ale	ale	k9	ale
i	i	k9	i
power	power	k1gMnSc1	power
forward	forward	k1gMnSc1	forward
(	(	kIx(	(
<g/>
pivot	pivot	k1gMnSc1	pivot
s	s	k7c7	s
větší	veliký	k2eAgFnSc7d2	veliký
pohyblivostí	pohyblivost	k1gFnSc7	pohyblivost
<g/>
,	,	kIx,	,
typově	typově	k6eAd1	typově
bližší	blízký	k2eAgMnPc1d2	bližší
křídlu	křídlo	k1gNnSc3	křídlo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
americkém	americký	k2eAgNnSc6d1	americké
členění	členění	k1gNnSc6	členění
se	se	k3xPyFc4	se
pozice	pozice	k1gFnSc1	pozice
také	také	k9	také
číslují	číslovat	k5eAaImIp3nP	číslovat
<g/>
.	.	kIx.	.
</s>
<s>
Center	centrum	k1gNnPc2	centrum
se	se	k3xPyFc4	se
označuje	označovat	k5eAaImIp3nS	označovat
jako	jako	k8xC	jako
hráč	hráč	k1gMnSc1	hráč
číslo	číslo	k1gNnSc1	číslo
pět	pět	k4xCc1	pět
<g/>
,	,	kIx,	,
power	power	k1gMnSc1	power
forward	forward	k1gMnSc1	forward
jako	jako	k8xC	jako
pozice	pozice	k1gFnSc1	pozice
číslo	číslo	k1gNnSc1	číslo
čtyři	čtyři	k4xCgNnPc1	čtyři
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
==	==	k?	==
</s>
</p>
<p>
<s>
Rozehrávač	rozehrávač	k1gMnSc1	rozehrávač
</s>
</p>
<p>
<s>
Křídlo	křídlo	k1gNnSc1	křídlo
</s>
</p>
