<s>
Number	Number	k1gInSc1
One	One	k1gFnSc2
Observatory	Observator	k1gInPc1
Circle	Circle	k1gFnSc2
</s>
<s>
Number	Number	k1gInSc1
One	One	k1gFnSc1
Observatory	Observator	k1gInPc4
Circle	Circle	k1gFnSc2
Účel	účel	k1gInSc1
stavby	stavba	k1gFnSc2
</s>
<s>
oficiální	oficiální	k2eAgNnSc4d1
sídlo	sídlo	k1gNnSc4
</s>
<s>
Základní	základní	k2eAgFnSc1d1
informace	informace	k1gFnSc1
Architekt	architekt	k1gMnSc1
</s>
<s>
Leon	Leona	k1gFnPc2
E.	E.	kA
Dessez	Dessez	k1gInSc1
Výstavba	výstavba	k1gFnSc1
</s>
<s>
1893	#num#	k4
Poloha	poloha	k1gFnSc1
Adresa	adresa	k1gFnSc1
</s>
<s>
1	#num#	k4
Observatory	Observator	k1gMnPc7
Circle	Circle	k1gFnSc2
NW	NW	kA
<g/>
,	,	kIx,
Washington	Washington	k1gInSc1
<g/>
,	,	kIx,
DC	DC	kA
20008	#num#	k4
<g/>
-	-	kIx~
<g/>
3619	#num#	k4
<g/>
,	,	kIx,
Washington	Washington	k1gInSc1
<g/>
,	,	kIx,
D.C.	D.C.	k1gFnPc1
<g/>
,	,	kIx,
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgFnSc2d1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
38	#num#	k4
<g/>
°	°	k?
<g/>
55	#num#	k4
<g/>
′	′	k?
<g/>
22,64	22,64	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
77	#num#	k4
<g/>
°	°	k?
<g/>
3	#num#	k4
<g/>
′	′	k?
<g/>
55,53	55,53	k4
<g/>
″	″	k?
z.	z.	k?
d.	d.	k?
</s>
<s>
Další	další	k2eAgFnPc1d1
informace	informace	k1gFnPc1
Web	web	k1gInSc4
</s>
<s>
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Odcházející	odcházející	k2eAgMnSc1d1
viceprezident	viceprezident	k1gMnSc1
Cheney	Chenea	k1gFnSc2
a	a	k8xC
nastupují	nastupovat	k5eAaImIp3nP
viceprezident	viceprezident	k1gMnSc1
Biden	Biden	k1gInSc4
s	s	k7c7
manželkami	manželka	k1gFnPc7
(	(	kIx(
<g/>
listopad	listopad	k1gInSc1
2008	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Number	Number	k1gInSc1
One	One	k1gFnSc2
Observatory	Observator	k1gInPc1
Circle	Circle	k1gFnSc2
je	být	k5eAaImIp3nS
oficiální	oficiální	k2eAgFnSc1d1
rezidence	rezidence	k1gFnSc1
Viceprezidenta	viceprezident	k1gMnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
komplexu	komplex	k1gInSc2
United	United	k1gMnSc1
States	States	k1gMnSc1
Naval	navalit	k5eAaPmRp2nS
Observatory	Observator	k1gMnPc7
ve	v	k7c6
Washingtonu	Washington	k1gInSc6
D.C.	D.C.	k1gFnSc2
</s>
<s>
Stavba	stavba	k1gFnSc1
byla	být	k5eAaImAgFnS
postavena	postaven	k2eAgFnSc1d1
roku	rok	k1gInSc2
1893	#num#	k4
pro	pro	k7c4
superintendenta	superintendent	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1923	#num#	k4
byla	být	k5eAaImAgFnS
rezidencí	rezidence	k1gFnSc7
The	The	k1gMnSc2
Chief	Chief	k1gInSc1
of	of	k?
Naval	navalit	k5eAaPmRp2nS
Operations	Operations	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Kongres	kongres	k1gInSc1
USA	USA	kA
rozhodl	rozhodnout	k5eAaPmAgInS
roku	rok	k1gInSc2
1974	#num#	k4
<g/>
,	,	kIx,
že	že	k8xS
Number	Number	k1gMnSc1
One	One	k1gMnSc1
Observatory	Observator	k1gInPc7
Circle	Circle	k1gNnSc2
bude	být	k5eAaImBp3nS
oficiální	oficiální	k2eAgFnSc1d1
rezidencí	rezidence	k1gFnSc7
Viceprezidenta	viceprezident	k1gMnSc2
Spojených	spojený	k2eAgInPc2d1
států	stát	k1gInPc2
amerických	americký	k2eAgInPc2d1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc2
rodiny	rodina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Od	od	k7c2
tohoto	tento	k3xDgInSc2
roku	rok	k1gInSc2
zde	zde	k6eAd1
bydleli	bydlet	k5eAaImAgMnP
všichni	všechen	k3xTgMnPc1
Viceprezidenti	viceprezident	k1gMnPc1
USA	USA	kA
<g/>
,	,	kIx,
první	první	k4xOgMnSc1
byl	být	k5eAaImAgMnS
Nelson	Nelson	k1gMnSc1
Rockefeller	Rockefeller	k1gMnSc1
<g/>
,	,	kIx,
doposud	doposud	k6eAd1
poslední	poslední	k2eAgFnSc1d1
je	být	k5eAaImIp3nS
současná	současný	k2eAgFnSc1d1
viceprezidentka	viceprezidentka	k1gFnSc1
Kamala	Kamala	k1gFnSc1
Harrisová	Harrisový	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Roku	rok	k1gInSc2
2002	#num#	k4
<g/>
,	,	kIx,
tedy	tedy	k8xC
krátce	krátce	k6eAd1
po	po	k7c6
11	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc4
2001	#num#	k4
byl	být	k5eAaImAgInS
postaven	postavit	k5eAaPmNgInS
bezpečnostní	bezpečnostní	k2eAgInSc1d1
bunkr	bunkr	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Number	Numbra	k1gFnPc2
One	One	k1gFnSc2
Observatory	Observator	k1gInPc1
Circle	Circl	k1gMnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Pahýl	pahýl	k1gMnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
příliš	příliš	k6eAd1
stručný	stručný	k2eAgInSc4d1
nebo	nebo	k8xC
postrádá	postrádat	k5eAaImIp3nS
důležité	důležitý	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
Pomozte	pomoct	k5eAaPmRp2nPwC
Wikipedii	Wikipedie	k1gFnSc4
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
jej	on	k3xPp3gMnSc4
vhodně	vhodně	k6eAd1
rozšíříte	rozšířit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nevkládejte	vkládat	k5eNaImRp2nP
však	však	k9
bez	bez	k7c2
oprávnění	oprávnění	k1gNnSc2
cizí	cizí	k2eAgInPc4d1
texty	text	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Architektura	architektura	k1gFnSc1
a	a	k8xC
stavebnictví	stavebnictví	k1gNnSc1
|	|	kIx~
Spojené	spojený	k2eAgInPc1d1
státy	stát	k1gInPc1
americké	americký	k2eAgInPc1d1
</s>
