<s>
Carl	Carl	k1gMnSc1	Carl
Gustav	Gustav	k1gMnSc1	Gustav
Jung	Jung	k1gMnSc1	Jung
(	(	kIx(	(
<g/>
26	[number]	k4	26
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1875	[number]	k4	1875
Kesswil	Kesswila	k1gFnPc2	Kesswila
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
–	–	k?	–
6	[number]	k4	6
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1961	[number]	k4	1961
Küsnacht	Küsnachta	k1gFnPc2	Küsnachta
<g/>
,	,	kIx,	,
Švýcarsko	Švýcarsko	k1gNnSc1	Švýcarsko
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgMnS	být
švýcarský	švýcarský	k2eAgMnSc1d1	švýcarský
lékař	lékař	k1gMnSc1	lékař
a	a	k8xC	a
psychoterapeut	psychoterapeut	k1gMnSc1	psychoterapeut
<g/>
,	,	kIx,	,
zakladatel	zakladatel	k1gMnSc1	zakladatel
analytické	analytický	k2eAgFnSc2d1	analytická
psychologie	psychologie	k1gFnSc2	psychologie
<g/>
.	.	kIx.	.
</s>
