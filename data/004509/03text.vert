<s>
Žižkovský	žižkovský	k2eAgInSc1d1	žižkovský
vysílač	vysílač	k1gInSc1	vysílač
(	(	kIx(	(
<g/>
též	též	k9	též
Žižkovská	žižkovský	k2eAgFnSc1d1	Žižkovská
televizní	televizní	k2eAgFnSc1d1	televizní
věž	věž	k1gFnSc1	věž
<g/>
,	,	kIx,	,
Žižkovská	žižkovský	k2eAgFnSc1d1	Žižkovská
věž	věž	k1gFnSc1	věž
či	či	k8xC	či
TVPM	TVPM	kA	TVPM
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
pražských	pražský	k2eAgFnPc2d1	Pražská
dominant	dominanta	k1gFnPc2	dominanta
a	a	k8xC	a
současně	současně	k6eAd1	současně
nejvyšší	vysoký	k2eAgFnSc7d3	nejvyšší
stavbou	stavba	k1gFnSc7	stavba
(	(	kIx(	(
<g/>
216	[number]	k4	216
m	m	kA	m
<g/>
)	)	kIx)	)
ve	v	k7c6	v
městě	město	k1gNnSc6	město
<g/>
,	,	kIx,	,
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
tedy	tedy	k9	tedy
i	i	k9	i
nejvyšší	vysoký	k2eAgInSc4d3	Nejvyšší
stabilní	stabilní	k2eAgInSc4d1	stabilní
bod	bod	k1gInSc4	bod
ve	v	k7c6	v
městě	město	k1gNnSc6	město
–	–	k?	–
kótu	kóta	k1gFnSc4	kóta
474	[number]	k4	474
m	m	kA	m
n.	n.	k?	n.
m.	m.	k?	m.
Leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
rozhraní	rozhraní	k1gNnSc6	rozhraní
Žižkova	Žižkov	k1gInSc2	Žižkov
a	a	k8xC	a
Vinohrad	Vinohrady	k1gInPc2	Vinohrady
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Mahlerových	Mahlerův	k2eAgInPc2d1	Mahlerův
sadů	sad	k1gInPc2	sad
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
postavena	postavit	k5eAaPmNgFnS	postavit
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
1985	[number]	k4	1985
až	až	k9	až
1992	[number]	k4	1992
Inženýrskými	inženýrský	k2eAgFnPc7d1	inženýrská
a	a	k8xC	a
průmyslovými	průmyslový	k2eAgFnPc7d1	průmyslová
stavbami	stavba	k1gFnPc7	stavba
Ostrava	Ostrava	k1gFnSc1	Ostrava
podle	podle	k7c2	podle
návrhu	návrh	k1gInSc2	návrh
architekta	architekt	k1gMnSc2	architekt
Václava	Václav	k1gMnSc2	Václav
Aulického	Aulický	k2eAgMnSc2d1	Aulický
<g/>
,	,	kIx,	,
statika	statik	k1gMnSc2	statik
Jiřího	Jiří	k1gMnSc2	Jiří
Kozáka	Kozák	k1gMnSc2	Kozák
a	a	k8xC	a
Alexe	Alex	k1gMnSc5	Alex
Béma	Bémum	k1gNnPc5	Bémum
<g/>
.	.	kIx.	.
</s>
<s>
Majitelem	majitel	k1gMnSc7	majitel
věže	věž	k1gFnSc2	věž
a	a	k8xC	a
provozovatelem	provozovatel	k1gMnSc7	provozovatel
vysílací	vysílací	k2eAgFnSc2d1	vysílací
části	část	k1gFnSc2	část
je	být	k5eAaImIp3nS	být
společnost	společnost	k1gFnSc1	společnost
České	český	k2eAgFnSc2d1	Česká
Radiokomunikace	radiokomunikace	k1gFnSc2	radiokomunikace
<g/>
,	,	kIx,	,
a.s.	a.s.	k?	a.s.
Samotné	samotný	k2eAgNnSc1d1	samotné
technické	technický	k2eAgNnSc1d1	technické
řešení	řešení	k1gNnSc1	řešení
přineslo	přinést	k5eAaPmAgNnS	přinést
několik	několik	k4yIc4	několik
novinek	novinka	k1gFnPc2	novinka
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
i	i	k9	i
patentů	patent	k1gInPc2	patent
<g/>
.	.	kIx.	.
</s>
<s>
Základová	základový	k2eAgFnSc1d1	základová
železobetonová	železobetonový	k2eAgFnSc1d1	železobetonová
deska	deska	k1gFnSc1	deska
<g/>
,	,	kIx,	,
o	o	k7c6	o
průměru	průměr	k1gInSc6	průměr
30	[number]	k4	30
m	m	kA	m
a	a	k8xC	a
tloušťce	tloušťka	k1gFnSc6	tloušťka
4	[number]	k4	4
m	m	kA	m
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
nalézá	nalézat	k5eAaImIp3nS	nalézat
v	v	k7c6	v
hloubce	hloubka	k1gFnSc6	hloubka
15	[number]	k4	15
m	m	kA	m
pod	pod	k7c7	pod
původním	původní	k2eAgInSc7d1	původní
povrchem	povrch	k1gInSc7	povrch
<g/>
.	.	kIx.	.
</s>
<s>
Svislá	svislý	k2eAgFnSc1d1	svislá
konstrukce	konstrukce	k1gFnSc1	konstrukce
se	se	k3xPyFc4	se
skládá	skládat	k5eAaImIp3nS	skládat
ze	z	k7c2	z
tří	tři	k4xCgInPc2	tři
válcových	válcový	k2eAgInPc2d1	válcový
ocelových	ocelový	k2eAgInPc2d1	ocelový
tubusů	tubus	k1gInPc2	tubus
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgFnPc2	jenž
dva	dva	k4xCgMnPc4	dva
vedlejší	vedlejší	k2eAgMnPc1d1	vedlejší
(	(	kIx(	(
<g/>
jeden	jeden	k4xCgMnSc1	jeden
s	s	k7c7	s
nouzovým	nouzový	k2eAgNnSc7d1	nouzové
schodištěm	schodiště	k1gNnSc7	schodiště
<g/>
,	,	kIx,	,
druhý	druhý	k4xOgMnSc1	druhý
se	se	k3xPyFc4	se
služebním	služební	k2eAgInSc7d1	služební
výtahem	výtah	k1gInSc7	výtah
<g/>
)	)	kIx)	)
mají	mít	k5eAaImIp3nP	mít
průměr	průměr	k1gInSc4	průměr
4,8	[number]	k4	4,8
m	m	kA	m
a	a	k8xC	a
dosahují	dosahovat	k5eAaImIp3nP	dosahovat
společně	společně	k6eAd1	společně
výšky	výška	k1gFnPc1	výška
134	[number]	k4	134
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgInSc1d1	hlavní
tubus	tubus	k1gInSc1	tubus
(	(	kIx(	(
<g/>
se	s	k7c7	s
dvěma	dva	k4xCgInPc7	dva
rychlovýtahy	rychlovýtah	k1gInPc7	rychlovýtah
<g/>
)	)	kIx)	)
má	mít	k5eAaImIp3nS	mít
průměr	průměr	k1gInSc4	průměr
6,4	[number]	k4	6,4
m	m	kA	m
a	a	k8xC	a
přechází	přecházet	k5eAaImIp3nS	přecházet
do	do	k7c2	do
anténního	anténní	k2eAgInSc2d1	anténní
nástavce	nástavec	k1gInSc2	nástavec
sahajícího	sahající	k2eAgInSc2d1	sahající
do	do	k7c2	do
výšky	výška	k1gFnSc2	výška
216	[number]	k4	216
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Tubusy	tubus	k1gInPc1	tubus
jsou	být	k5eAaImIp3nP	být
tvořeny	tvořit	k5eAaImNgInP	tvořit
soustřednými	soustředný	k2eAgFnPc7d1	soustředná
ocelovými	ocelový	k2eAgFnPc7d1	ocelová
rourami	roura	k1gFnPc7	roura
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gFnPc1	jejichž
meziprostor	meziprostor	k1gInSc4	meziprostor
tloušťky	tloušťka	k1gFnSc2	tloušťka
cca	cca	kA	cca
30	[number]	k4	30
cm	cm	kA	cm
je	být	k5eAaImIp3nS	být
vylit	vylít	k5eAaPmNgInS	vylít
betonem	beton	k1gInSc7	beton
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
tyto	tento	k3xDgInPc4	tento
svislé	svislý	k2eAgInPc4d1	svislý
tubusy	tubus	k1gInPc4	tubus
jsou	být	k5eAaImIp3nP	být
pak	pak	k6eAd1	pak
zavěšeny	zavěšen	k2eAgFnPc4d1	zavěšena
tři	tři	k4xCgFnPc4	tři
kabiny	kabina	k1gFnPc4	kabina
s	s	k7c7	s
trojramenným	trojramenný	k2eAgInSc7d1	trojramenný
půdorysem	půdorys	k1gInSc7	půdorys
<g/>
:	:	kIx,	:
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
66	[number]	k4	66
metrů	metr	k1gInPc2	metr
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
panoramatická	panoramatický	k2eAgFnSc1d1	panoramatická
restaurace	restaurace	k1gFnSc1	restaurace
"	"	kIx"	"
<g/>
Oblaca	Oblaca	k1gFnSc1	Oblaca
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
s	s	k7c7	s
barem	bar	k1gInSc7	bar
a	a	k8xC	a
bistrem	bistr	k1gInSc7	bistr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
93	[number]	k4	93
metrů	metr	k1gInPc2	metr
vyhlídková	vyhlídkový	k2eAgFnSc1d1	vyhlídková
kabina	kabina	k1gFnSc1	kabina
s	s	k7c7	s
dohledností	dohlednost	k1gFnSc7	dohlednost
až	až	k9	až
100	[number]	k4	100
km	km	kA	km
a	a	k8xC	a
nejvýše	nejvýše	k6eAd1	nejvýše
je	být	k5eAaImIp3nS	být
umístěna	umístit	k5eAaPmNgFnS	umístit
(	(	kIx(	(
<g/>
veřejnosti	veřejnost	k1gFnSc6	veřejnost
běžně	běžně	k6eAd1	běžně
nepřístupná	přístupný	k2eNgFnSc1d1	nepřístupná
<g/>
)	)	kIx)	)
kabina	kabina	k1gFnSc1	kabina
s	s	k7c7	s
vysílací	vysílací	k2eAgFnSc7d1	vysílací
technikou	technika	k1gFnSc7	technika
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
v	v	k7c6	v
letech	let	k1gInPc6	let
2011	[number]	k4	2011
<g/>
–	–	k?	–
<g/>
2012	[number]	k4	2012
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
přímo	přímo	k6eAd1	přímo
nad	nad	k7c7	nad
restaurací	restaurace	k1gFnSc7	restaurace
luxusní	luxusní	k2eAgNnSc1d1	luxusní
hotelové	hotelový	k2eAgNnSc1d1	hotelové
apartmá	apartmá	k1gNnSc1	apartmá
<g/>
,	,	kIx,	,
změny	změna	k1gFnPc1	změna
se	se	k3xPyFc4	se
dotkly	dotknout	k5eAaPmAgFnP	dotknout
i	i	k9	i
Mahlerových	Mahlerův	k2eAgInPc2d1	Mahlerův
sadů	sad	k1gInPc2	sad
v	v	k7c6	v
blízkém	blízký	k2eAgNnSc6d1	blízké
okolí	okolí	k1gNnSc6	okolí
věže	věž	k1gFnSc2	věž
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
anténního	anténní	k2eAgInSc2d1	anténní
nástavce	nástavec	k1gInSc2	nástavec
vysílá	vysílat	k5eAaImIp3nS	vysílat
digitálně	digitálně	k6eAd1	digitálně
jedenáct	jedenáct	k4xCc1	jedenáct
televizních	televizní	k2eAgFnPc2d1	televizní
a	a	k8xC	a
osm	osm	k4xCc4	osm
rozhlasových	rozhlasový	k2eAgFnPc2d1	rozhlasová
stanic	stanice	k1gFnPc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Analogové	analogový	k2eAgNnSc1d1	analogové
televizní	televizní	k2eAgNnSc1d1	televizní
vysílání	vysílání	k1gNnSc1	vysílání
bylo	být	k5eAaImAgNnS	být
definitivně	definitivně	k6eAd1	definitivně
ukončeno	ukončit	k5eAaPmNgNnS	ukončit
30	[number]	k4	30
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2009	[number]	k4	2009
<g/>
,	,	kIx,	,
analogově	analogově	k6eAd1	analogově
v	v	k7c6	v
pásmu	pásmo	k1gNnSc6	pásmo
VKV	VKV	kA	VKV
odtud	odtud	k6eAd1	odtud
vysílá	vysílat	k5eAaImIp3nS	vysílat
šest	šest	k4xCc1	šest
rozhlasových	rozhlasový	k2eAgFnPc2d1	rozhlasová
stanic	stanice	k1gFnPc2	stanice
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
jsou	být	k5eAaImIp3nP	být
zde	zde	k6eAd1	zde
vysílače	vysílač	k1gInPc1	vysílač
mobilních	mobilní	k2eAgInPc2d1	mobilní
operátorů	operátor	k1gInPc2	operátor
a	a	k8xC	a
také	také	k9	také
se	se	k3xPyFc4	se
zde	zde	k6eAd1	zde
měří	měřit	k5eAaImIp3nS	měřit
kvalita	kvalita	k1gFnSc1	kvalita
ovzduší	ovzduší	k1gNnSc2	ovzduší
v	v	k7c6	v
Praze	Praha	k1gFnSc6	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
1974	[number]	k4	1974
se	se	k3xPyFc4	se
datovala	datovat	k5eAaImAgFnS	datovat
potřeba	potřeba	k1gFnSc1	potřeba
výkonného	výkonný	k2eAgInSc2d1	výkonný
vysílače	vysílač	k1gInSc2	vysílač
rozhlasového	rozhlasový	k2eAgInSc2d1	rozhlasový
a	a	k8xC	a
televizního	televizní	k2eAgInSc2d1	televizní
signálu	signál	k1gInSc2	signál
pro	pro	k7c4	pro
obyvatele	obyvatel	k1gMnPc4	obyvatel
Prahy	Praha	k1gFnSc2	Praha
<g/>
.	.	kIx.	.
</s>
<s>
Osazení	osazení	k1gNnSc1	osazení
vysílače	vysílač	k1gInSc2	vysílač
do	do	k7c2	do
Petřínské	petřínský	k2eAgFnSc2d1	Petřínská
rozhledny	rozhledna	k1gFnSc2	rozhledna
bylo	být	k5eAaImAgNnS	být
nutno	nutno	k6eAd1	nutno
chápat	chápat	k5eAaImF	chápat
jako	jako	k9	jako
provizorium	provizorium	k1gNnSc4	provizorium
a	a	k8xC	a
tak	tak	k6eAd1	tak
tehdejší	tehdejší	k2eAgNnSc1d1	tehdejší
Ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
spojů	spoj	k1gInPc2	spoj
už	už	k9	už
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1975	[number]	k4	1975
prosazovalo	prosazovat	k5eAaImAgNnS	prosazovat
stavbu	stavba	k1gFnSc4	stavba
160	[number]	k4	160
m	m	kA	m
vysokého	vysoký	k2eAgInSc2d1	vysoký
stožáru	stožár	k1gInSc2	stožár
–	–	k?	–
také	také	k9	také
na	na	k7c6	na
Petříně	Petřín	k1gInSc6	Petřín
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
podnik	podnik	k1gInSc4	podnik
Kovoslužba	kovoslužba	k1gFnSc1	kovoslužba
nebyl	být	k5eNaImAgInS	být
nucen	nucen	k2eAgMnSc1d1	nucen
přesměrovávat	přesměrovávat	k5eAaPmF	přesměrovávat
antény	anténa	k1gFnPc4	anténa
desetitisícům	desetitisíce	k1gInPc3	desetitisíce
domácností	domácnost	k1gFnPc2	domácnost
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgInSc3	tento
nápadu	nápad	k1gInSc3	nápad
se	se	k3xPyFc4	se
však	však	k9	však
silně	silně	k6eAd1	silně
a	a	k8xC	a
účinně	účinně	k6eAd1	účinně
bránili	bránit	k5eAaImAgMnP	bránit
památkáři	památkář	k1gMnPc1	památkář
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
definitivně	definitivně	k6eAd1	definitivně
uspěli	uspět	k5eAaPmAgMnP	uspět
a	a	k8xC	a
po	po	k7c6	po
delších	dlouhý	k2eAgFnPc6d2	delší
debatách	debata	k1gFnPc6	debata
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
vysílač	vysílač	k1gInSc4	vysílač
vybrána	vybrat	k5eAaPmNgFnS	vybrat
lokalita	lokalita	k1gFnSc1	lokalita
(	(	kIx(	(
<g/>
tehdy	tehdy	k6eAd1	tehdy
zpustlých	zpustlý	k2eAgInPc2d1	zpustlý
<g/>
)	)	kIx)	)
Mahlerových	Mahlerův	k2eAgInPc2d1	Mahlerův
sadů	sad	k1gInPc2	sad
<g/>
.	.	kIx.	.
</s>
<s>
Tehdy	tehdy	k6eAd1	tehdy
bylo	být	k5eAaImAgNnS	být
vypočteno	vypočten	k2eAgNnSc1d1	vypočteno
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
dokonalé	dokonalý	k2eAgNnSc4d1	dokonalé
pokrytí	pokrytí	k1gNnSc4	pokrytí
Prahy	Praha	k1gFnSc2	Praha
signálem	signál	k1gInSc7	signál
ze	z	k7c2	z
Žižkova	Žižkov	k1gInSc2	Žižkov
musí	muset	k5eAaImIp3nS	muset
být	být	k5eAaImF	být
anténa	anténa	k1gFnSc1	anténa
nejméně	málo	k6eAd3	málo
ve	v	k7c6	v
výšce	výška	k1gFnSc6	výška
200	[number]	k4	200
m	m	kA	m
nad	nad	k7c7	nad
terénem	terén	k1gInSc7	terén
<g/>
.	.	kIx.	.
</s>
<s>
Architekt	architekt	k1gMnSc1	architekt
Václav	Václav	k1gMnSc1	Václav
Aulický	Aulický	k2eAgMnSc1d1	Aulický
vypracoval	vypracovat	k5eAaPmAgMnS	vypracovat
zhruba	zhruba	k6eAd1	zhruba
dvacet	dvacet	k4xCc4	dvacet
různých	různý	k2eAgInPc2d1	různý
návrhů	návrh	k1gInPc2	návrh
<g/>
,	,	kIx,	,
např.	např.	kA	např.
štíhlý	štíhlý	k2eAgInSc1d1	štíhlý
hyperboloid	hyperboloid	k1gInSc1	hyperboloid
nebo	nebo	k8xC	nebo
"	"	kIx"	"
<g/>
jehlu	jehla	k1gFnSc4	jehla
s	s	k7c7	s
jablkem	jablko	k1gNnSc7	jablko
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
tlak	tlak	k1gInSc4	tlak
radiokomunikací	radiokomunikace	k1gFnPc2	radiokomunikace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
spokojily	spokojit	k5eAaPmAgFnP	spokojit
s	s	k7c7	s
obyčejným	obyčejný	k2eAgInSc7d1	obyčejný
železobetonovým	železobetonový	k2eAgInSc7d1	železobetonový
stožárem	stožár	k1gInSc7	stožár
a	a	k8xC	a
kabinou	kabina	k1gFnSc7	kabina
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
roku	rok	k1gInSc2	rok
1984	[number]	k4	1984
zvítězila	zvítězit	k5eAaPmAgFnS	zvítězit
světově	světově	k6eAd1	světově
unikátní	unikátní	k2eAgFnSc1d1	unikátní
futuristická	futuristický	k2eAgFnSc1d1	futuristická
varianta	varianta	k1gFnSc1	varianta
subtilní	subtilní	k2eAgFnSc1d1	subtilní
a	a	k8xC	a
přitom	přitom	k6eAd1	přitom
stabilní	stabilní	k2eAgFnSc2d1	stabilní
třísloupové	třísloupový	k2eAgFnSc2d1	třísloupový
konstrukce	konstrukce	k1gFnSc2	konstrukce
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
připomíná	připomínat	k5eAaImIp3nS	připomínat
startující	startující	k2eAgFnSc4d1	startující
raketu	raketa	k1gFnSc4	raketa
<g/>
.	.	kIx.	.
</s>
<s>
Autor	autor	k1gMnSc1	autor
tehdy	tehdy	k6eAd1	tehdy
úspěšně	úspěšně	k6eAd1	úspěšně
argumentoval	argumentovat	k5eAaImAgMnS	argumentovat
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
jednostožárový	jednostožárový	k2eAgInSc1d1	jednostožárový
železobetonový	železobetonový	k2eAgInSc1d1	železobetonový
vysílač	vysílač	k1gInSc1	vysílač
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
příliš	příliš	k6eAd1	příliš
masivní	masivní	k2eAgMnSc1d1	masivní
a	a	k8xC	a
v	v	k7c6	v
pražském	pražský	k2eAgNnSc6d1	Pražské
ovzduší	ovzduší	k1gNnSc6	ovzduší
by	by	kYmCp3nS	by
materiál	materiál	k1gInSc1	materiál
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
degradoval	degradovat	k5eAaBmAgMnS	degradovat
<g/>
.	.	kIx.	.
</s>
<s>
Třísloupová	třísloupový	k2eAgFnSc1d1	třísloupový
ocelobetonová	ocelobetonový	k2eAgFnSc1d1	ocelobetonová
konstrukce	konstrukce	k1gFnSc1	konstrukce
tak	tak	k9	tak
znamená	znamenat	k5eAaImIp3nS	znamenat
mnohem	mnohem	k6eAd1	mnohem
menší	malý	k2eAgInSc1d2	menší
zásah	zásah	k1gInSc1	zásah
do	do	k7c2	do
pražského	pražský	k2eAgNnSc2d1	Pražské
panoramatu	panorama	k1gNnSc2	panorama
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
vyšší	vysoký	k2eAgFnSc4d2	vyšší
aerodynamickou	aerodynamický	k2eAgFnSc4d1	aerodynamická
stabilitu	stabilita	k1gFnSc4	stabilita
a	a	k8xC	a
její	její	k3xOp3gNnSc4	její
technické	technický	k2eAgNnSc4d1	technické
provedení	provedení	k1gNnSc4	provedení
<g/>
,	,	kIx,	,
s	s	k7c7	s
ochrannými	ochranný	k2eAgFnPc7d1	ochranná
laminátovými	laminátový	k2eAgFnPc7d1	laminátová
vrstvami	vrstva	k1gFnPc7	vrstva
<g/>
,	,	kIx,	,
přinese	přinést	k5eAaPmIp3nS	přinést
i	i	k9	i
vyšší	vysoký	k2eAgFnSc4d2	vyšší
užitnou	užitný	k2eAgFnSc4d1	užitná
hodnotu	hodnota	k1gFnSc4	hodnota
a	a	k8xC	a
trvanlivost	trvanlivost	k1gFnSc4	trvanlivost
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgInSc1d1	základní
kámen	kámen	k1gInSc1	kámen
vysílače	vysílač	k1gInSc2	vysílač
byl	být	k5eAaImAgInS	být
položen	položit	k5eAaPmNgInS	položit
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
1985	[number]	k4	1985
<g/>
.	.	kIx.	.
</s>
<s>
Výstavba	výstavba	k1gFnSc1	výstavba
neprobíhala	probíhat	k5eNaImAgFnS	probíhat
nijak	nijak	k6eAd1	nijak
rychle	rychle	k6eAd1	rychle
<g/>
,	,	kIx,	,
navíc	navíc	k6eAd1	navíc
ji	on	k3xPp3gFnSc4	on
doprovázely	doprovázet	k5eAaImAgInP	doprovázet
mnohé	mnohý	k2eAgInPc1d1	mnohý
spory	spor	k1gInPc1	spor
a	a	k8xC	a
negativní	negativní	k2eAgNnSc4d1	negativní
hodnocení	hodnocení	k1gNnSc4	hodnocení
<g/>
:	:	kIx,	:
například	například	k6eAd1	například
situování	situování	k1gNnSc1	situování
stavby	stavba	k1gFnSc2	stavba
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
bývalého	bývalý	k2eAgInSc2d1	bývalý
židovského	židovský	k2eAgInSc2d1	židovský
hřbitova	hřbitov	k1gInSc2	hřbitov
si	se	k3xPyFc3	se
vynutilo	vynutit	k5eAaPmAgNnS	vynutit
dohled	dohled	k1gInSc4	dohled
představitelů	představitel	k1gMnPc2	představitel
Židovské	židovský	k2eAgFnSc2d1	židovská
obce	obec	k1gFnSc2	obec
při	při	k7c6	při
výkopových	výkopový	k2eAgFnPc6d1	výkopová
pracích	práce	k1gFnPc6	práce
a	a	k8xC	a
nalezené	nalezený	k2eAgInPc1d1	nalezený
ostatky	ostatek	k1gInPc1	ostatek
byly	být	k5eAaImAgInP	být
posléze	posléze	k6eAd1	posléze
pohřbívány	pohřbívat	k5eAaImNgInP	pohřbívat
na	na	k7c6	na
Novém	nový	k2eAgInSc6d1	nový
židovském	židovský	k2eAgInSc6d1	židovský
hřbitově	hřbitov	k1gInSc6	hřbitov
na	na	k7c6	na
Olšanech	Olšany	k1gInPc6	Olšany
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
ovšem	ovšem	k9	ovšem
nebylo	být	k5eNaImAgNnS	být
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
stavitele	stavitel	k1gMnPc4	stavitel
dodržováno	dodržován	k2eAgNnSc1d1	dodržováno
a	a	k8xC	a
došlo	dojít	k5eAaPmAgNnS	dojít
tak	tak	k9	tak
k	k	k7c3	k
definitivnímu	definitivní	k2eAgNnSc3d1	definitivní
zničení	zničení	k1gNnSc3	zničení
cenných	cenný	k2eAgInPc2d1	cenný
hrobů	hrob	k1gInPc2	hrob
a	a	k8xC	a
náhrobků	náhrobek	k1gInPc2	náhrobek
bez	bez	k7c2	bez
jakékoliv	jakýkoliv	k3yIgFnSc2	jakýkoliv
možn	možn	k1gInSc1	možn
osti	osti	k1gNnSc6	osti
dokumentace	dokumentace	k1gFnSc2	dokumentace
Památkovým	památkový	k2eAgInSc7d1	památkový
ústavem	ústav	k1gInSc7	ústav
či	či	k8xC	či
židovským	židovský	k2eAgNnSc7d1	Židovské
muzeem	muzeum	k1gNnSc7	muzeum
<g/>
.	.	kIx.	.
</s>
<s>
Projednávala	projednávat	k5eAaImAgFnS	projednávat
se	se	k3xPyFc4	se
také	také	k9	také
hrozba	hrozba	k1gFnSc1	hrozba
elektromagnetického	elektromagnetický	k2eAgNnSc2d1	elektromagnetické
zatížení	zatížení	k1gNnSc2	zatížení
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
však	však	k9	však
zvláštní	zvláštní	k2eAgFnSc1d1	zvláštní
komise	komise	k1gFnSc1	komise
vyvrátila	vyvrátit	k5eAaPmAgFnS	vyvrátit
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
výkon	výkon	k1gInSc1	výkon
vysílače	vysílač	k1gInSc2	vysílač
byl	být	k5eAaImAgInS	být
hluboko	hluboko	k6eAd1	hluboko
pod	pod	k7c7	pod
přísnými	přísný	k2eAgInPc7d1	přísný
hygienickými	hygienický	k2eAgInPc7d1	hygienický
limity	limit	k1gInPc7	limit
<g/>
.	.	kIx.	.
</s>
<s>
Zněly	znět	k5eAaImAgInP	znět
i	i	k9	i
nekompromisní	kompromisní	k2eNgInPc1d1	nekompromisní
hlasy	hlas	k1gInPc1	hlas
požadující	požadující	k2eAgInPc1d1	požadující
zbourání	zbourání	k1gNnSc4	zbourání
vysílače	vysílač	k1gInSc2	vysílač
<g/>
,	,	kIx,	,
ty	ten	k3xDgFnPc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
neprosadily	prosadit	k5eNaPmAgFnP	prosadit
a	a	k8xC	a
tak	tak	k6eAd1	tak
mohla	moct	k5eAaImAgFnS	moct
být	být	k5eAaImF	být
postupně	postupně	k6eAd1	postupně
osazována	osazován	k2eAgFnSc1d1	osazována
vysílací	vysílací	k2eAgFnSc1d1	vysílací
technika	technika	k1gFnSc1	technika
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
1991	[number]	k4	1991
započal	započnout	k5eAaPmAgInS	započnout
zkušební	zkušební	k2eAgInSc1d1	zkušební
vysílací	vysílací	k2eAgInSc1d1	vysílací
provoz	provoz	k1gInSc1	provoz
<g/>
,	,	kIx,	,
od	od	k7c2	od
3	[number]	k4	3
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1991	[number]	k4	1991
(	(	kIx(	(
<g/>
38	[number]	k4	38
let	léto	k1gNnPc2	léto
a	a	k8xC	a
2	[number]	k4	2
dny	den	k1gInPc7	den
po	po	k7c6	po
oficiálním	oficiální	k2eAgNnSc6d1	oficiální
zahájení	zahájení	k1gNnSc6	zahájení
TV	TV	kA	TV
vysílání	vysílání	k1gNnSc4	vysílání
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
<g/>
)	)	kIx)	)
přešel	přejít	k5eAaPmAgInS	přejít
do	do	k7c2	do
standardního	standardní	k2eAgInSc2d1	standardní
režimu	režim	k1gInSc2	režim
a	a	k8xC	a
17	[number]	k4	17
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1992	[number]	k4	1992
byl	být	k5eAaImAgInS	být
vysílač	vysílač	k1gInSc1	vysílač
<g/>
,	,	kIx,	,
resp.	resp.	kA	resp.
jeho	jeho	k3xOp3gMnPc6	jeho
restaurační	restaurační	k2eAgFnSc1d1	restaurační
a	a	k8xC	a
vyhlídková	vyhlídkový	k2eAgFnSc1d1	vyhlídková
část	část	k1gFnSc1	část
<g/>
,	,	kIx,	,
oficiálně	oficiálně	k6eAd1	oficiálně
otevřen	otevřít	k5eAaPmNgInS	otevřít
pro	pro	k7c4	pro
veřejnost	veřejnost	k1gFnSc4	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2000	[number]	k4	2000
bylo	být	k5eAaImAgNnS	být
na	na	k7c4	na
pilíře	pilíř	k1gInPc4	pilíř
umístěno	umístěn	k2eAgNnSc4d1	umístěno
dílo	dílo	k1gNnSc4	dílo
Davida	David	k1gMnSc2	David
Černého	Černý	k1gMnSc2	Černý
zvané	zvaný	k2eAgFnPc1d1	zvaná
"	"	kIx"	"
<g/>
Miminka	miminko	k1gNnPc4	miminko
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
"	"	kIx"	"
<g/>
Babies	Babies	k1gInSc1	Babies
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
v	v	k7c6	v
počtu	počet	k1gInSc6	počet
10	[number]	k4	10
miminek	miminko	k1gNnPc2	miminko
<g/>
,	,	kIx,	,
znázorňující	znázorňující	k2eAgNnPc1d1	znázorňující
batolata	batole	k1gNnPc1	batole
lezoucí	lezoucí	k2eAgNnPc1d1	lezoucí
nahoru	nahoru	k6eAd1	nahoru
a	a	k8xC	a
dolů	dolů	k6eAd1	dolů
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
se	se	k3xPyFc4	se
údajně	údajně	k6eAd1	údajně
setkalo	setkat	k5eAaPmAgNnS	setkat
s	s	k7c7	s
pozitivním	pozitivní	k2eAgInSc7d1	pozitivní
ohlasem	ohlas	k1gInSc7	ohlas
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2001	[number]	k4	2001
nainstalováno	nainstalovat	k5eAaPmNgNnS	nainstalovat
natrvalo	natrvalo	k6eAd1	natrvalo
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
jediný	jediný	k2eAgInSc1d1	jediný
umělecký	umělecký	k2eAgInSc1d1	umělecký
počin	počin	k1gInSc1	počin
<g/>
,	,	kIx,	,
který	který	k3yRgInSc4	který
věž	věž	k1gFnSc1	věž
vyprovokovala	vyprovokovat	k5eAaPmAgFnS	vyprovokovat
<g/>
:	:	kIx,	:
režisér	režisér	k1gMnSc1	režisér
Jan	Jan	k1gMnSc1	Jan
Němec	Němec	k1gMnSc1	Němec
ve	v	k7c6	v
vysílači	vysílač	k1gInSc6	vysílač
natočil	natočit	k5eAaBmAgInS	natočit
některé	některý	k3yIgFnPc4	některý
scény	scéna	k1gFnPc4	scéna
filmu	film	k1gInSc2	film
V	v	k7c6	v
žáru	žár	k1gInSc6	žár
královské	královský	k2eAgFnSc2d1	královská
lásky	láska	k1gFnSc2	láska
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
22	[number]	k4	22
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2006	[number]	k4	2006
<g/>
,	,	kIx,	,
u	u	k7c2	u
příležitosti	příležitost	k1gFnSc2	příležitost
125	[number]	k4	125
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
povýšení	povýšení	k1gNnSc2	povýšení
Žižkova	Žižkov	k1gInSc2	Žižkov
na	na	k7c4	na
město	město	k1gNnSc4	město
a	a	k8xC	a
15	[number]	k4	15
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
zprovoznění	zprovoznění	k1gNnSc2	zprovoznění
vysílače	vysílač	k1gInSc2	vysílač
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
vysílač	vysílač	k1gInSc1	vysílač
každý	každý	k3xTgInSc4	každý
den	den	k1gInSc4	den
různobarevně	různobarevně	k6eAd1	různobarevně
nasvícen	nasvícen	k2eAgInSc4d1	nasvícen
ve	v	k7c6	v
spodních	spodní	k2eAgFnPc6d1	spodní
částech	část	k1gFnPc6	část
plošin	plošina	k1gFnPc2	plošina
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zpravidla	zpravidla	k6eAd1	zpravidla
v	v	k7c6	v
barvách	barva	k1gFnPc6	barva
státní	státní	k2eAgFnSc2d1	státní
trikolóry	trikolóra	k1gFnSc2	trikolóra
(	(	kIx(	(
<g/>
které	který	k3yRgNnSc1	který
v	v	k7c6	v
době	doba	k1gFnSc6	doba
instalace	instalace	k1gFnSc2	instalace
byly	být	k5eAaImAgInP	být
zároveň	zároveň	k6eAd1	zároveň
i	i	k8xC	i
firemními	firemní	k2eAgFnPc7d1	firemní
barvami	barva	k1gFnPc7	barva
Českých	český	k2eAgFnPc2d1	Česká
Radiokomunikací	radiokomunikace	k1gFnPc2	radiokomunikace
coby	coby	k?	coby
provozovatele	provozovatel	k1gMnSc2	provozovatel
a	a	k8xC	a
majitele	majitel	k1gMnSc2	majitel
věže	věž	k1gFnSc2	věž
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
2011	[number]	k4	2011
až	až	k9	až
2012	[number]	k4	2012
prošla	projít	k5eAaPmAgFnS	projít
Žižkovská	žižkovský	k2eAgFnSc1d1	Žižkovská
věž	věž	k1gFnSc1	věž
velkou	velká	k1gFnSc7	velká
rekonstrukcí	rekonstrukce	k1gFnSc7	rekonstrukce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2010	[number]	k4	2010
si	se	k3xPyFc3	se
objekt	objekt	k1gInSc4	objekt
pronajala	pronajmout	k5eAaPmAgFnS	pronajmout
společnost	společnost	k1gFnSc1	společnost
Oreathea	Oreathea	k1gFnSc1	Oreathea
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
se	se	k3xPyFc4	se
rozhodla	rozhodnout	k5eAaPmAgFnS	rozhodnout
přestavět	přestavět	k5eAaPmF	přestavět
interiéry	interiér	k1gInPc4	interiér
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
2011	[number]	k4	2011
začala	začít	k5eAaPmAgFnS	začít
rekonstrukce	rekonstrukce	k1gFnSc1	rekonstrukce
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
odstraněny	odstranit	k5eAaPmNgFnP	odstranit
zbytečné	zbytečný	k2eAgFnPc1d1	zbytečná
místnosti	místnost	k1gFnPc1	místnost
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
modernizaci	modernizace	k1gFnSc3	modernizace
všech	všecek	k3xTgFnPc2	všecek
vnitřních	vnitřní	k2eAgFnPc2d1	vnitřní
prostor	prostora	k1gFnPc2	prostora
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
restaurace	restaurace	k1gFnPc4	restaurace
a	a	k8xC	a
vyhlídky	vyhlídka	k1gFnPc4	vyhlídka
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
věže	věž	k1gFnSc2	věž
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
luxusní	luxusní	k2eAgInSc1d1	luxusní
hotel	hotel	k1gInSc1	hotel
s	s	k7c7	s
jedním	jeden	k4xCgInSc7	jeden
pokojem	pokoj	k1gInSc7	pokoj
a	a	k8xC	a
dvěma	dva	k4xCgInPc7	dva
lůžky	lůžko	k1gNnPc7	lůžko
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
rekonstrukci	rekonstrukce	k1gFnSc6	rekonstrukce
věže	věž	k1gFnSc2	věž
došlo	dojít	k5eAaPmAgNnS	dojít
také	také	k9	také
k	k	k7c3	k
revitalizaci	revitalizace	k1gFnSc3	revitalizace
jejího	její	k3xOp3gNnSc2	její
okolí	okolí	k1gNnSc2	okolí
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
přilehlého	přilehlý	k2eAgInSc2d1	přilehlý
parku	park	k1gInSc2	park
<g/>
.	.	kIx.	.
</s>
<s>
Stavba	stavba	k1gFnSc1	stavba
je	být	k5eAaImIp3nS	být
kontroverzní	kontroverzní	k2eAgMnSc1d1	kontroverzní
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
kvůli	kvůli	k7c3	kvůli
ní	on	k3xPp3gFnSc3	on
byl	být	k5eAaImAgInS	být
zničen	zničen	k2eAgInSc1d1	zničen
Starý	starý	k2eAgInSc4d1	starý
olšanský	olšanský	k2eAgInSc4d1	olšanský
židovský	židovský	k2eAgInSc4d1	židovský
hřbitov	hřbitov	k1gInSc4	hřbitov
<g/>
,	,	kIx,	,
na	na	k7c6	na
jehož	jehož	k3xOyRp3gInSc6	jehož
místě	místo	k1gNnSc6	místo
stojí	stát	k5eAaImIp3nS	stát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
revolučním	revoluční	k2eAgInSc6d1	revoluční
kvasu	kvas	k1gInSc6	kvas
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
80	[number]	k4	80
<g/>
.	.	kIx.	.
a	a	k8xC	a
90	[number]	k4	90
<g/>
.	.	kIx.	.
let	léto	k1gNnPc2	léto
přišel	přijít	k5eAaPmAgMnS	přijít
publicista	publicista	k1gMnSc1	publicista
Jaroslav	Jaroslav	k1gMnSc1	Jaroslav
Veis	Veis	k1gInSc4	Veis
s	s	k7c7	s
populárním	populární	k2eAgNnSc7d1	populární
řešením	řešení	k1gNnSc7	řešení
dvou	dva	k4xCgFnPc2	dva
kontroverzních	kontroverzní	k2eAgFnPc2d1	kontroverzní
a	a	k8xC	a
vlekoucích	vlekoucí	k2eAgFnPc2d1	vlekoucí
se	se	k3xPyFc4	se
staveb	stavba	k1gFnPc2	stavba
socialismu	socialismus	k1gInSc2	socialismus
<g/>
,	,	kIx,	,
Strahovského	strahovský	k2eAgInSc2d1	strahovský
tunelu	tunel	k1gInSc2	tunel
a	a	k8xC	a
žižkovské	žižkovský	k2eAgFnSc2d1	Žižkovská
televizní	televizní	k2eAgFnSc2d1	televizní
věže	věž	k1gFnSc2	věž
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
fejetonu	fejeton	k1gInSc6	fejeton
pro	pro	k7c4	pro
obnovené	obnovený	k2eAgFnPc4d1	obnovená
Lidové	lidový	k2eAgFnPc4d1	lidová
noviny	novina	k1gFnPc4	novina
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
žižkovskou	žižkovský	k2eAgFnSc4d1	Žižkovská
věž	věž	k1gFnSc4	věž
podříznout	podříznout	k5eAaPmF	podříznout
a	a	k8xC	a
poté	poté	k6eAd1	poté
její	její	k3xOp3gInSc4	její
tubus	tubus	k1gInSc4	tubus
strčit	strčit	k5eAaPmF	strčit
do	do	k7c2	do
tunelu	tunel	k1gInSc2	tunel
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
letech	léto	k1gNnPc6	léto
bylo	být	k5eAaImAgNnS	být
autorství	autorství	k1gNnSc1	autorství
této	tento	k3xDgFnSc2	tento
myšlenky	myšlenka	k1gFnSc2	myšlenka
přisuzováno	přisuzován	k2eAgNnSc1d1	přisuzováno
Ludvíku	Ludvík	k1gMnSc6	Ludvík
Vaculíkovi	Vaculík	k1gMnSc6	Vaculík
<g/>
.	.	kIx.	.
</s>
<s>
Pozitivní	pozitivní	k2eAgInSc1d1	pozitivní
ohlas	ohlas	k1gInSc1	ohlas
nebyl	být	k5eNaImAgInS	být
všeobecný	všeobecný	k2eAgInSc1d1	všeobecný
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
australského	australský	k2eAgInSc2d1	australský
serveru	server	k1gInSc2	server
VirtualTourist	VirtualTourist	k1gInSc1	VirtualTourist
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
je	být	k5eAaImIp3nS	být
žižkovský	žižkovský	k2eAgInSc1d1	žižkovský
vysílač	vysílač	k1gInSc1	vysílač
druhou	druhý	k4xOgFnSc7	druhý
nejošklivější	ošklivý	k2eAgFnSc7d3	nejošklivější
stavbou	stavba	k1gFnSc7	stavba
světa	svět	k1gInSc2	svět
(	(	kIx(	(
<g/>
hned	hned	k6eAd1	hned
po	po	k7c6	po
baltimorském	baltimorský	k2eAgInSc6d1	baltimorský
divadlu	divadlo	k1gNnSc3	divadlo
<g/>
)	)	kIx)	)
a	a	k8xC	a
batolata	batole	k1gNnPc1	batole
Davida	David	k1gMnSc2	David
Černého	Černý	k1gMnSc2	Černý
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
popisu	popis	k1gInSc6	popis
výslovně	výslovně	k6eAd1	výslovně
zmíněna	zmínit	k5eAaPmNgFnS	zmínit
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Je	být	k5eAaImIp3nS	být
už	už	k9	už
sama	sám	k3xTgFnSc1	sám
o	o	k7c6	o
sobě	sebe	k3xPyFc6	sebe
dost	dost	k6eAd1	dost
ošklivá	ošklivý	k2eAgFnSc1d1	ošklivá
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
malé	malý	k2eAgFnPc1d1	malá
lezoucí	lezoucí	k2eAgFnPc1d1	lezoucí
děti	dítě	k1gFnPc1	dítě
od	od	k7c2	od
výtvarníka	výtvarník	k1gMnSc2	výtvarník
Davida	David	k1gMnSc2	David
Černého	Černý	k1gMnSc2	Černý
ji	on	k3xPp3gFnSc4	on
z	z	k7c2	z
hrůzného	hrůzný	k2eAgInSc2d1	hrůzný
pohledu	pohled	k1gInSc2	pohled
proměnily	proměnit	k5eAaPmAgInP	proměnit
v	v	k7c4	v
něco	něco	k3yInSc4	něco
<g/>
,	,	kIx,	,
nad	nad	k7c7	nad
čím	co	k3yQnSc7	co
už	už	k6eAd1	už
opravdu	opravdu	k6eAd1	opravdu
nevěřícně	věřícně	k6eNd1	věřícně
kroutíte	kroutit	k5eAaImIp2nP	kroutit
hlavou	hlava	k1gFnSc7	hlava
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
Černý	Černý	k1gMnSc1	Černý
považuje	považovat	k5eAaImIp3nS	považovat
vysílač	vysílač	k1gInSc1	vysílač
za	za	k7c4	za
něco	něco	k3yInSc4	něco
divného	divný	k2eAgMnSc4d1	divný
<g/>
,	,	kIx,	,
protlačeného	protlačený	k2eAgMnSc4d1	protlačený
bolševikem	bolševik	k1gMnSc7	bolševik
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
Aulický	Aulický	k2eAgMnSc1d1	Aulický
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vysílač	vysílač	k1gInSc1	vysílač
na	na	k7c6	na
Žižkově	Žižkov	k1gInSc6	Žižkov
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
komunistům	komunista	k1gMnPc3	komunista
navzdory	navzdory	k6eAd1	navzdory
<g/>
.	.	kIx.	.
</s>
<s>
Tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
byl	být	k5eAaImAgInS	být
vysílač	vysílač	k1gInSc1	vysílač
stavěn	stavit	k5eAaImNgInS	stavit
na	na	k7c6	na
konci	konec	k1gInSc6	konec
éry	éra	k1gFnSc2	éra
reálného	reálný	k2eAgInSc2d1	reálný
socialismu	socialismus	k1gInSc2	socialismus
<g/>
,	,	kIx,	,
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
si	se	k3xPyFc3	se
mezi	mezi	k7c7	mezi
veřejností	veřejnost	k1gFnSc7	veřejnost
nejméně	málo	k6eAd3	málo
dvě	dva	k4xCgFnPc4	dva
posměšné	posměšný	k2eAgFnPc4d1	posměšná
přezdívky	přezdívka	k1gFnPc4	přezdívka
narážející	narážející	k2eAgFnPc4d1	narážející
na	na	k7c4	na
představitele	představitel	k1gMnPc4	představitel
strany	strana	k1gFnSc2	strana
a	a	k8xC	a
vlády	vláda	k1gFnSc2	vláda
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Jakešův	Jakešův	k2eAgInSc1d1	Jakešův
prst	prst	k1gInSc1	prst
<g/>
"	"	kIx"	"
se	se	k3xPyFc4	se
vztahoval	vztahovat	k5eAaImAgInS	vztahovat
na	na	k7c4	na
generálního	generální	k2eAgMnSc2d1	generální
tajemníka	tajemník	k1gMnSc2	tajemník
ÚV	ÚV	kA	ÚV
KSČ	KSČ	kA	KSČ
Miloše	Miloš	k1gMnSc2	Miloš
Jakeše	Jakeš	k1gMnSc2	Jakeš
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
Biľakova	Biľakův	k2eAgFnSc1d1	Biľakova
jehla	jehla	k1gFnSc1	jehla
<g/>
"	"	kIx"	"
zase	zase	k9	zase
na	na	k7c4	na
člena	člen	k1gMnSc4	člen
předsednictva	předsednictvo	k1gNnSc2	předsednictvo
FS	FS	kA	FS
ČSSR	ČSSR	kA	ČSSR
Vasila	Vasil	k1gMnSc2	Vasil
Biľaka	Biľak	k1gMnSc2	Biľak
<g/>
,	,	kIx,	,
původem	původ	k1gInSc7	původ
vyučeného	vyučený	k2eAgMnSc4d1	vyučený
krejčího	krejčí	k1gMnSc4	krejčí
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
přezdívky	přezdívka	k1gFnPc1	přezdívka
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
raketa	raketa	k1gFnSc1	raketa
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
Bajkonur	Bajkonur	k1gMnSc1	Bajkonur
<g/>
"	"	kIx"	"
pak	pak	k6eAd1	pak
už	už	k9	už
jen	jen	k9	jen
odkazovaly	odkazovat	k5eAaImAgFnP	odkazovat
na	na	k7c4	na
štíhlý	štíhlý	k2eAgInSc4d1	štíhlý
vysoký	vysoký	k2eAgInSc4d1	vysoký
tvar	tvar	k1gInSc4	tvar
tubusů	tubus	k1gInPc2	tubus
<g/>
,	,	kIx,	,
připomínající	připomínající	k2eAgFnSc4d1	připomínající
kosmickou	kosmický	k2eAgFnSc4d1	kosmická
raketu	raketa	k1gFnSc4	raketa
před	před	k7c7	před
startem	start	k1gInSc7	start
<g/>
.	.	kIx.	.
</s>
<s>
Přehled	přehled	k1gInSc1	přehled
televizních	televizní	k2eAgInPc2d1	televizní
multiplexů	multiplex	k1gInPc2	multiplex
vysílaných	vysílaný	k2eAgInPc2d1	vysílaný
z	z	k7c2	z
Žižkovského	žižkovský	k2eAgInSc2d1	žižkovský
vysílače	vysílač	k1gInSc2	vysílač
<g/>
:	:	kIx,	:
</s>
