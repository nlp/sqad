<s>
Jezero	jezero	k1gNnSc1	jezero
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
"	"	kIx"	"
<g/>
Mrtvé	mrtvý	k2eAgNnSc1d1	mrtvé
moře	moře	k1gNnSc1	moře
<g/>
"	"	kIx"	"
kvůli	kvůli	k7c3	kvůli
své	svůj	k3xOyFgFnSc3	svůj
vysoké	vysoký	k2eAgFnSc3d1	vysoká
salinitě	salinita	k1gFnSc3	salinita
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
brání	bránit	k5eAaImIp3nS	bránit
životu	život	k1gInSc3	život
vodních	vodní	k2eAgInPc2d1	vodní
organismů	organismus	k1gInPc2	organismus
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jsou	být	k5eAaImIp3nP	být
ryby	ryba	k1gFnPc1	ryba
a	a	k8xC	a
vodní	vodní	k2eAgFnPc1d1	vodní
rostliny	rostlina	k1gFnPc1	rostlina
<g/>
.	.	kIx.	.
</s>
