<s>
Jiřina	Jiřina	k1gFnSc1
Adamcová	Adamcová	k1gFnSc1
</s>
<s>
Jiřina	Jiřina	k1gFnSc1
Adamcová	Adamcová	k1gFnSc1
Jiřina	Jiřina	k1gFnSc1
Adamcová	Adamcová	k1gFnSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
Narození	narození	k1gNnSc2
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1927	#num#	k4
Rudlice	Rudlice	k1gFnSc1
u	u	k7c2
Znojma	Znojmo	k1gNnSc2
</s>
<s>
Československo	Československo	k1gNnSc1
Československo	Československo	k1gNnSc1
Úmrtí	úmrtí	k1gNnPc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2019	#num#	k4
(	(	kIx(
<g/>
ve	v	k7c6
věku	věk	k1gInSc6
91	#num#	k4
let	léto	k1gNnPc2
<g/>
)	)	kIx)
Praha	Praha	k1gFnSc1
Česko	Česko	k1gNnSc1
Česko	Česko	k1gNnSc1
Alma	alma	k1gFnSc1
mater	mater	k1gFnSc1
</s>
<s>
Akademie	akademie	k1gFnSc1
výtvarných	výtvarný	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
Povolání	povolání	k1gNnSc2
</s>
<s>
malířka	malířka	k1gFnSc1
Ocenění	ocenění	k1gNnSc2
</s>
<s>
Čestné	čestný	k2eAgNnSc1d1
občanství	občanství	k1gNnSc1
města	město	k1gNnSc2
Kyjova	Kyjov	k1gInSc2
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
databázi	databáze	k1gFnSc6
Národní	národní	k2eAgFnSc2d1
knihovny	knihovna	k1gFnSc2
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Jiřina	Jiřina	k1gFnSc1
Adamcová	Adamcová	k1gFnSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
1927	#num#	k4
Rudlice	Rudlice	k1gFnSc1
u	u	k7c2
Znojma	Znojmo	k1gNnSc2
–	–	k?
1	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2019	#num#	k4
Praha	Praha	k1gFnSc1
<g/>
)	)	kIx)
byla	být	k5eAaImAgFnS
česká	český	k2eAgFnSc1d1
akademická	akademický	k2eAgFnSc1d1
malířka	malířka	k1gFnSc1
a	a	k8xC
grafička	grafička	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Životopis	životopis	k1gInSc1
</s>
<s>
Mládí	mládí	k1gNnSc1
prožila	prožít	k5eAaPmAgFnS
v	v	k7c6
Kyjově	Kyjov	k1gInSc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
navštěvovala	navštěvovat	k5eAaImAgFnS
reálné	reálný	k2eAgNnSc4d1
gymnázium	gymnázium	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1946	#num#	k4
<g/>
–	–	k?
<g/>
1950	#num#	k4
studovala	studovat	k5eAaImAgFnS
na	na	k7c4
Akademii	akademie	k1gFnSc4
výtvarných	výtvarný	k2eAgNnPc2d1
umění	umění	k1gNnPc2
v	v	k7c6
Praze	Praha	k1gFnSc6
(	(	kIx(
<g/>
Grafická	grafický	k2eAgFnSc1d1
škola	škola	k1gFnSc1
prof.	prof.	kA
Vladimíra	Vladimíra	k1gFnSc1
Pukla	puknout	k5eAaPmAgFnS
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byla	být	k5eAaImAgFnS
členkou	členka	k1gFnSc7
Svazu	svaz	k1gInSc2
čs	čs	kA
<g/>
.	.	kIx.
výtvarných	výtvarný	k2eAgMnPc2d1
umělců	umělec	k1gMnPc2
(	(	kIx(
<g/>
od	od	k7c2
roku	rok	k1gInSc2
1952	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Unie	unie	k1gFnSc1
výtvarných	výtvarný	k2eAgMnPc2d1
umělců	umělec	k1gMnPc2
a	a	k8xC
Asociace	asociace	k1gFnSc2
volné	volný	k2eAgFnSc2d1
grafiky	grafika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
letech	léto	k1gNnPc6
1950	#num#	k4
<g/>
–	–	k?
<g/>
1952	#num#	k4
byla	být	k5eAaImAgFnS
výtvarnou	výtvarný	k2eAgFnSc7d1
redaktorkou	redaktorka	k1gFnSc7
časopisu	časopis	k1gInSc2
Květy	Květa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pracovala	pracovat	k5eAaImAgFnS
v	v	k7c6
oboru	obor	k1gInSc6
grafiky	grafika	k1gFnSc2
a	a	k8xC
ilustrace	ilustrace	k1gFnSc2
<g/>
,	,	kIx,
věnovala	věnovat	k5eAaImAgFnS,k5eAaPmAgFnS
se	se	k3xPyFc4
zejména	zejména	k9
technice	technika	k1gFnSc3
volně	volně	k6eAd1
řezaného	řezaný	k2eAgInSc2d1
plošného	plošný	k2eAgInSc2d1
linorytu	linoryt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žila	žít	k5eAaImAgFnS
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
Dílo	dílo	k1gNnSc1
</s>
<s>
Na	na	k7c6
počátku	počátek	k1gInSc6
své	svůj	k3xOyFgFnSc2
tvorby	tvorba	k1gFnSc2
se	se	k3xPyFc4
kromě	kromě	k7c2
grafiky	grafika	k1gFnSc2
věnovala	věnovat	k5eAaPmAgFnS,k5eAaImAgFnS
ilustrační	ilustrační	k2eAgFnSc1d1
tvorbě	tvorba	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
grafických	grafický	k2eAgFnPc6d1
technikách	technika	k1gFnPc6
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
zpočátku	zpočátku	k6eAd1
o	o	k7c4
dřevořez	dřevořez	k1gInSc4
a	a	k8xC
dřevoryt	dřevoryt	k1gInSc4
<g/>
,	,	kIx,
posléze	posléze	k6eAd1
o	o	k7c4
linoryt	linoryt	k1gInSc4
<g/>
,	,	kIx,
kterému	který	k3yIgMnSc3,k3yRgMnSc3,k3yQgMnSc3
zůstala	zůstat	k5eAaPmAgFnS
věrná	věrný	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
roce	rok	k1gInSc6
1960	#num#	k4
se	se	k3xPyFc4
zaměřila	zaměřit	k5eAaPmAgFnS
na	na	k7c4
monumentální	monumentální	k2eAgFnSc4d1
tvorbu	tvorba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
rozmezí	rozmezí	k1gNnSc6
dvaceti	dvacet	k4xCc2
let	léto	k1gNnPc2
realizovala	realizovat	k5eAaBmAgFnS
přes	přes	k7c4
třicet	třicet	k4xCc4
děl	dělo	k1gNnPc2
pro	pro	k7c4
interiéry	interiér	k1gInPc4
i	i	k8xC
exteriéry	exteriér	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikly	vzniknout	k5eAaPmAgInP
realizace	realizace	k1gFnSc2
v	v	k7c6
různých	různý	k2eAgInPc6d1
materiálech	materiál	k1gInPc6
<g/>
,	,	kIx,
převážně	převážně	k6eAd1
v	v	k7c4
kamenné	kamenný	k2eAgNnSc4d1
a	a	k8xC
sklo	sklo	k1gNnSc4
mozaice	mozaika	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zároveň	zároveň	k6eAd1
vypovídá	vypovídat	k5eAaPmIp3nS,k5eAaImIp3nS
o	o	k7c6
světě	svět	k1gInSc6
v	v	k7c6
rozměrných	rozměrný	k2eAgInPc6d1
cyklech	cyklus	k1gInPc6
linorytů	linoryt	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
začátku	začátek	k1gInSc2
let	léto	k1gNnPc2
osmdesátých	osmdesátý	k4xOgNnPc2
se	se	k3xPyFc4
obrátila	obrátit	k5eAaPmAgFnS
ke	k	k7c3
sdělení	sdělení	k1gNnSc3
duchovního	duchovní	k2eAgNnSc2d1
zaměření	zaměření	k1gNnSc2
<g/>
,	,	kIx,
k	k	k7c3
inspiraci	inspirace	k1gFnSc3
z	z	k7c2
Bible	bible	k1gFnSc2
<g/>
,	,	kIx,
Starého	Starého	k2eAgInSc2d1
a	a	k8xC
Nového	Nového	k2eAgInSc2d1
zákona	zákon	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
tomto	tento	k3xDgInSc6
světě	svět	k1gInSc6
nalezla	naleznout	k5eAaPmAgFnS,k5eAaBmAgFnS
své	svůj	k3xOyFgNnSc4
celoživotní	celoživotní	k2eAgNnSc4d1
poselství	poselství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikly	vzniknout	k5eAaPmAgInP
grafické	grafický	k2eAgInPc1d1
cykly	cyklus	k1gInPc1
<g/>
:	:	kIx,
Žalmy	žalm	k1gInPc1
<g/>
,	,	kIx,
Zázraky	zázrak	k1gInPc1
<g/>
,	,	kIx,
Kázání	kázání	k1gNnPc1
na	na	k7c6
hoře	hora	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vytvořila	vytvořit	k5eAaPmAgFnS
vlastní	vlastní	k2eAgFnSc4d1
grafickou	grafický	k2eAgFnSc4d1
techniku	technika	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
směřuje	směřovat	k5eAaImIp3nS
k	k	k7c3
monumentálním	monumentální	k2eAgInPc3d1
formátům	formát	k1gInPc3
<g/>
,	,	kIx,
realizovaným	realizovaný	k2eAgNnSc7d1
v	v	k7c6
cyklu	cyklus	k1gInSc6
Žalmy	žalm	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vystavovala	vystavovat	k5eAaImAgFnS
v	v	k7c6
Památníku	památník	k1gInSc6
Terezín	Terezín	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
jejím	její	k3xOp3gNnSc7
magickým	magický	k2eAgNnSc7d1
místem	místo	k1gNnSc7
po	po	k7c4
celou	celý	k2eAgFnSc4d1
etapu	etapa	k1gFnSc4
života	život	k1gInSc2
a	a	k8xC
kam	kam	k6eAd1
se	se	k3xPyFc4
vracela	vracet	k5eAaImAgFnS
s	s	k7c7
dalšími	další	k2eAgFnPc7d1
velkými	velký	k2eAgFnPc7d1
výstavami	výstava	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
se	se	k3xPyFc4
věnovala	věnovat	k5eAaPmAgFnS,k5eAaImAgFnS
převážně	převážně	k6eAd1
malbě	malba	k1gFnSc3
a	a	k8xC
kombinovaným	kombinovaný	k2eAgFnPc3d1
technikám	technika	k1gFnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Inspirována	inspirován	k2eAgFnSc1d1
mnohými	mnohý	k2eAgFnPc7d1
cestami	cesta	k1gFnPc7
a	a	k8xC
setkáními	setkání	k1gNnPc7
s	s	k7c7
francouzskými	francouzský	k2eAgFnPc7d1
katedrálami	katedrála	k1gFnPc7
namalovala	namalovat	k5eAaPmAgFnS
rozsáhlé	rozsáhlý	k2eAgInPc4d1
cykly	cyklus	k1gInPc4
Okna	okno	k1gNnSc2
k	k	k7c3
naději	naděje	k1gFnSc3
a	a	k8xC
Pocta	pocta	k1gFnSc1
katedrále	katedrála	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
tomto	tento	k3xDgInSc6
cyklu	cyklus	k1gInSc6
pracovala	pracovat	k5eAaImAgFnS
několik	několik	k4yIc4
let	léto	k1gNnPc2
a	a	k8xC
vystavovala	vystavovat	k5eAaImAgFnS
ho	on	k3xPp3gMnSc4
na	na	k7c6
mnoha	mnoho	k4c6
výstavách	výstava	k1gFnPc6
doma	doma	k6eAd1
i	i	k8xC
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vystavovala	vystavovat	k5eAaImAgFnS
další	další	k2eAgInPc4d1
cykly	cyklus	k1gInPc4
maleb	malba	k1gFnPc2
<g/>
:	:	kIx,
Všichni	všechen	k3xTgMnPc1
jsme	být	k5eAaImIp1nP
poutníci	poutník	k1gMnPc1
<g/>
,	,	kIx,
Legendy	legenda	k1gFnPc1
o	o	k7c4
sv.	sv.	kA
Františkovi	František	k1gMnSc3
z	z	k7c2
Asissi	Asisse	k1gFnSc6
<g/>
,	,	kIx,
Andělé	anděl	k1gMnPc1
a	a	k8xC
Piety	pieta	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
letech	let	k1gInPc6
1958	#num#	k4
<g/>
–	–	k?
<g/>
2012	#num#	k4
realizovala	realizovat	k5eAaBmAgFnS
přes	přes	k7c4
čtyřicet	čtyřicet	k4xCc4
samostatných	samostatný	k2eAgFnPc2d1
výstav	výstava	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gFnPc1
grafika	grafika	k1gFnSc1
a	a	k8xC
malby	malba	k1gFnPc1
byly	být	k5eAaImAgFnP
zastoupeny	zastoupit	k5eAaPmNgFnP
na	na	k7c6
mnoha	mnoho	k4c6
výstavách	výstava	k1gFnPc6
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
Zúčastnila	zúčastnit	k5eAaPmAgFnS
se	se	k3xPyFc4
řady	řada	k1gFnSc2
mezinárodních	mezinárodní	k2eAgFnPc2d1
výstav	výstava	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gNnPc1
díla	dílo	k1gNnPc1
jsou	být	k5eAaImIp3nP
zastoupena	zastoupen	k2eAgNnPc1d1
ve	v	k7c6
sbírkách	sbírka	k1gFnPc6
Národní	národní	k2eAgFnSc2d1
galerie	galerie	k1gFnSc2
<g/>
,	,	kIx,
krajských	krajský	k2eAgFnPc6d1
galeriích	galerie	k1gFnPc6
a	a	k8xC
mnoha	mnoho	k4c2
institucích	instituce	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s>
Přehled	přehled	k1gInSc4
děl	dělo	k1gNnPc2
</s>
<s>
Mozaika	mozaika	k1gFnSc1
Jan	Jana	k1gFnPc2
Želivský	želivský	k2eAgInSc1d1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
doba	doba	k1gFnSc1
ve	v	k7c6
vestibulu	vestibul	k1gInSc6
stanice	stanice	k1gFnSc2
metra	metro	k1gNnSc2
Želivského	želivský	k2eAgNnSc2d1
</s>
<s>
Kamenná	kamenný	k2eAgFnSc1d1
mozaika	mozaika	k1gFnSc1
Jan	Jan	k1gMnSc1
Želivský	želivský	k2eAgMnSc1d1
a	a	k8xC
jeho	jeho	k3xOp3gFnSc1
doba	doba	k1gFnSc1
–	–	k?
stanice	stanice	k1gFnSc2
metra	metro	k1gNnSc2
Želivského	želivský	k2eAgNnSc2d1
<g/>
,	,	kIx,
1980	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
její	její	k3xOp3gFnSc7
relizací	relizace	k1gFnSc7
pomáhal	pomáhat	k5eAaImAgMnS
také	také	k6eAd1
její	její	k3xOp3gMnSc1
syn	syn	k1gMnSc1
<g/>
,	,	kIx,
akademický	akademický	k2eAgMnSc1d1
malíř	malíř	k1gMnSc1
a	a	k8xC
grafik	grafik	k1gMnSc1
<g/>
,	,	kIx,
Ing.	ing.	kA
arch	arch	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jiří	Jiří	k1gMnSc1
Němeček	Němeček	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Obřadní	obřadní	k2eAgFnSc1d1
síň	síň	k1gFnSc1
Praha-Dejvice	Praha-Dejvice	k1gFnSc2
</s>
<s>
Poštovní	poštovní	k2eAgFnSc1d1
přepravní	přepravní	k2eAgFnSc1d1
budova	budova	k1gFnSc1
Praha	Praha	k1gFnSc1
</s>
<s>
Dům	dům	k1gInSc1
odborů	odbor	k1gInPc2
Svitavy	Svitava	k1gFnSc2
–	–	k?
vestibul	vestibul	k1gInSc1
</s>
<s>
Kamenná	kamenný	k2eAgFnSc1d1
mozaika	mozaika	k1gFnSc1
v	v	k7c6
obřadní	obřadní	k2eAgFnSc6d1
síni	síň	k1gFnSc6
v	v	k7c6
Dejvicích	Dejvice	k1gFnPc6
</s>
<s>
Skleněná	skleněný	k2eAgFnSc1d1
mozaika	mozaika	k1gFnSc1
v	v	k7c6
nemocnici	nemocnice	k1gFnSc6
Na	na	k7c6
Homolce	homolka	k1gFnSc6
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
Skleněná	skleněný	k2eAgFnSc1d1
mozaika	mozaika	k1gFnSc1
–	–	k?
sanatorium	sanatorium	k1gNnSc1
Na	na	k7c6
Homolce	homolka	k1gFnSc6
</s>
<s>
Skleněná	skleněný	k2eAgFnSc1d1
mozaika	mozaika	k1gFnSc1
na	na	k7c6
stadionu	stadion	k1gInSc6
Rošického	Rošický	k2eAgInSc2d1
na	na	k7c6
Strahově	Strahov	k1gInSc6
</s>
<s>
V	v	k7c6
Praze	Praha	k1gFnSc6
realizovala	realizovat	k5eAaBmAgFnS
přes	přes	k7c4
třicet	třicet	k4xCc4
děl	dělo	k1gNnPc2
v	v	k7c6
architektuře	architektura	k1gFnSc6
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1
R.	R.	kA
Moric	Moric	k1gMnSc1
–	–	k?
Našiel	Našiel	k1gInSc1
som	soma	k1gFnPc2
vám	vy	k3xPp2nPc3
kamarátov	kamarátovo	k1gNnPc2
(	(	kIx(
<g/>
1960	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
učebnice	učebnice	k1gFnPc4
pro	pro	k7c4
základní	základní	k2eAgFnPc4d1
školy	škola	k1gFnPc4
aj.	aj.	kA
</s>
<s>
Obrazy	obraz	k1gInPc1
v	v	k7c6
evangelickém	evangelický	k2eAgInSc6d1
kostele	kostel	k1gInSc6
Jana	Jan	k1gMnSc2
Milíče	Milíč	k1gFnSc2
v	v	k7c6
Praze-Chodově	Praze-Chodův	k2eAgFnSc6d1
</s>
<s>
Obrazy	obraz	k1gInPc1
v	v	k7c6
evangelickém	evangelický	k2eAgInSc6d1
kostele	kostel	k1gInSc6
v	v	k7c6
Praze-Braníku	Praze-Braník	k1gInSc6
</s>
<s>
Obrazy	obraz	k1gInPc1
v	v	k7c6
kostele	kostel	k1gInSc6
v	v	k7c6
Hořovicích	Hořovice	k1gFnPc6
</s>
<s>
Obrazy	obraz	k1gInPc1
v	v	k7c6
kostele	kostel	k1gInSc6
Nejsvětějšího	nejsvětější	k2eAgMnSc2d1
Salvátora	Salvátor	k1gMnSc2
v	v	k7c6
Praze	Praha	k1gFnSc6
u	u	k7c2
Karlova	Karlův	k2eAgInSc2d1
mostu	most	k1gInSc2
a	a	k8xC
další	další	k2eAgInPc4d1
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1
řady	řada	k1gFnSc2
učebnic	učebnice	k1gFnPc2
pro	pro	k7c4
základní	základní	k2eAgFnPc4d1
školy	škola	k1gFnPc4
<g/>
,	,	kIx,
beletrii	beletrie	k1gFnSc4
pro	pro	k7c4
děti	dítě	k1gFnPc4
<g/>
,	,	kIx,
knihy	kniha	k1gFnPc4
poezie	poezie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Návrhy	návrh	k1gInPc1
monumentálních	monumentální	k2eAgFnPc2d1
vitráží	vitráž	k1gFnPc2
<g/>
,	,	kIx,
mozaik	mozaika	k1gFnPc2
a	a	k8xC
intarzií	intarzie	k1gFnPc2
do	do	k7c2
architektury	architektura	k1gFnSc2
–	–	k?
více	hodně	k6eAd2
než	než	k8xS
30	#num#	k4
děl	dělo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Věnovala	věnovat	k5eAaPmAgFnS,k5eAaImAgFnS
se	se	k3xPyFc4
volné	volný	k2eAgFnSc3d1
tvorbě	tvorba	k1gFnSc3
(	(	kIx(
<g/>
cykly	cyklus	k1gInPc4
Z	z	k7c2
rodného	rodný	k2eAgInSc2d1
kraje	kraj	k1gInSc2
<g/>
,	,	kIx,
1980	#num#	k4
<g/>
–	–	k?
<g/>
1981	#num#	k4
<g/>
)	)	kIx)
inspirované	inspirovaný	k2eAgInPc1d1
biblickými	biblický	k2eAgInPc7d1
náměty	námět	k1gInPc7
(	(	kIx(
<g/>
Žalmy	žalm	k1gInPc7
<g/>
)	)	kIx)
a	a	k8xC
vztahem	vztah	k1gInSc7
člověka	člověk	k1gMnSc2
a	a	k8xC
kosmu	kosmos	k1gInSc2
(	(	kIx(
<g/>
cyklus	cyklus	k1gInSc1
Planeta	planeta	k1gFnSc1
Země	země	k1gFnSc1
<g/>
,	,	kIx,
1981	#num#	k4
<g/>
–	–	k?
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Výstavy	výstava	k1gFnPc1
</s>
<s>
Samostatné	samostatný	k2eAgFnPc1d1
výstavy	výstava	k1gFnPc1
</s>
<s>
1958	#num#	k4
Galerie	galerie	k1gFnSc1
Luhačovice	Luhačovice	k1gFnPc1
</s>
<s>
1961	#num#	k4
Galerie	galerie	k1gFnSc1
U	u	k7c2
Řečických	řečický	k2eAgMnPc2d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
1962	#num#	k4
Galerie	galerie	k1gFnSc1
Chrudim	Chrudim	k1gFnSc1
</s>
<s>
1963	#num#	k4
Galerie	galerie	k1gFnSc1
Fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
1967	#num#	k4
Galerie	galerie	k1gFnSc1
muzea	muzeum	k1gNnSc2
<g/>
,	,	kIx,
Svitavy	Svitava	k1gFnSc2
</s>
<s>
1970	#num#	k4
Galerie	galerie	k1gFnSc1
Letná	Letná	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
1977	#num#	k4
Galerie	galerie	k1gFnSc1
Fronta	fronta	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
1977	#num#	k4
Krajská	krajský	k2eAgFnSc1d1
galerie	galerie	k1gFnSc1
<g/>
,	,	kIx,
Hodonín	Hodonín	k1gInSc1
</s>
<s>
1978	#num#	k4
Kulturní	kulturní	k2eAgInSc1d1
dům	dům	k1gInSc1
<g/>
,	,	kIx,
Svitavy	Svitava	k1gFnPc1
</s>
<s>
1982	#num#	k4
Galerie	galerie	k1gFnSc1
Karolina	Karolinum	k1gNnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
1983	#num#	k4
Galerie	galerie	k1gFnSc1
Ústí	ústí	k1gNnSc2
nad	nad	k7c7
Labem	Labe	k1gNnSc7
</s>
<s>
1984	#num#	k4
Galerie	galerie	k1gFnSc2
Český	český	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
1988	#num#	k4
Galerie	galerie	k1gFnSc1
Václava	Václav	k1gMnSc2
Špály	Špála	k1gMnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
1990	#num#	k4
Okresní	okresní	k2eAgInSc1d1
muzeum	muzeum	k1gNnSc4
<g/>
,	,	kIx,
Svitavy	Svitava	k1gFnPc4
</s>
<s>
1990	#num#	k4
Galerie	galerie	k1gFnSc1
Platýz	Platýz	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
1992	#num#	k4
Klub	klub	k1gInSc4
architektů	architekt	k1gMnPc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
1995	#num#	k4
Památník	památník	k1gInSc1
Terezín	Terezín	k1gInSc4
</s>
<s>
1996	#num#	k4
Galerie	galerie	k1gFnSc1
U	u	k7c2
Křižovníků	křižovník	k1gMnPc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
1997	#num#	k4
Galerie	galerie	k1gFnSc1
Blansko	Blansko	k1gNnSc1
</s>
<s>
1999	#num#	k4
Památník	památník	k1gInSc1
Terezín	Terezín	k1gInSc4
</s>
<s>
2000	#num#	k4
Galerie	galerie	k1gFnSc1
U	u	k7c2
Křižovníků	křižovník	k1gMnPc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
2002	#num#	k4
Chodovská	chodovský	k2eAgFnSc1d1
tvrz	tvrz	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
2003	#num#	k4
Chodovská	chodovský	k2eAgFnSc1d1
tvrz	tvrz	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
2004	#num#	k4
Domov	domov	k1gInSc1
Sue	Sue	k1gFnSc2
Ryder	Rydra	k1gFnPc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
2005	#num#	k4
Kostel	kostel	k1gInSc1
sv.	sv.	kA
Salvátora	Salvátor	k1gMnSc2
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
2005	#num#	k4
Muzeum	muzeum	k1gNnSc1
gheta	gheto	k1gNnSc2
<g/>
,	,	kIx,
Terezín	Terezín	k1gInSc1
</s>
<s>
2006	#num#	k4
Galerie	galerie	k1gFnSc1
Vyšehrad	Vyšehrad	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
2006	#num#	k4
Kaple	kaple	k1gFnSc1
sv.	sv.	kA
Máří	Máří	k?
Magdaleny	Magdalena	k1gFnSc2
<g/>
,	,	kIx,
Mníšek	mníšek	k1gMnSc1
</s>
<s>
2007	#num#	k4
Jihomoravské	jihomoravský	k2eAgFnPc4d1
muzeum	muzeum	k1gNnSc1
Znojmo	Znojmo	k1gNnSc1
</s>
<s>
2007	#num#	k4
Galerie	galerie	k1gFnSc1
Kyjov	Kyjov	k1gInSc4
</s>
<s>
2008	#num#	k4
Klášter	klášter	k1gInSc1
u	u	k7c2
Panny	Panna	k1gFnSc2
Marie	Maria	k1gFnSc2
Sněžné	sněžný	k2eAgFnPc1d1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
</s>
<s>
2009	#num#	k4
Galerie	galerie	k1gFnSc1
KD	KD	kA
Dobříš	dobřit	k5eAaImIp2nS
</s>
<s>
2009	#num#	k4
Kostel	kostel	k1gInSc1
ČCE	ČCE	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
-	-	kIx~
Chodov	Chodov	k1gInSc1
</s>
<s>
2009	#num#	k4
Kostel	kostel	k1gInSc1
ČCE	ČCE	kA
<g/>
,	,	kIx,
Hořovice	Hořovice	k1gFnPc1
</s>
<s>
2010	#num#	k4
Galerie	galerie	k1gFnSc1
Kulturního	kulturní	k2eAgInSc2d1
domu	dům	k1gInSc2
Dobříš	dobřit	k5eAaImIp2nS
(	(	kIx(
<g/>
s	s	k7c7
Ericem	Erice	k1gMnSc7
Wilsonem	Wilson	k1gMnSc7
<g/>
)	)	kIx)
</s>
<s>
2012	#num#	k4
Galerie	galerie	k1gFnSc1
Kyjov	Kyjov	k1gInSc1
</s>
<s>
2012	#num#	k4
Památník	památník	k1gInSc1
Terezín	Terezín	k1gInSc4
</s>
<s>
2018	#num#	k4
Kostel	kostel	k1gInSc1
U	u	k7c2
Jákobova	Jákobův	k2eAgInSc2d1
žebříku	žebřík	k1gInSc2
<g/>
,	,	kIx,
Farní	farní	k2eAgInSc1d1
sbor	sbor	k1gInSc1
Českobratrské	českobratrský	k2eAgFnSc2d1
církve	církev	k1gFnSc2
evangelické	evangelický	k2eAgFnSc2d1
v	v	k7c6
Praze	Praha	k1gFnSc6
8	#num#	k4
–	–	k?
Kobylisy	Kobylisy	k1gInPc1
</s>
<s>
Mezinárodní	mezinárodní	k2eAgFnSc2d1
výstavy	výstava	k1gFnSc2
skupinové	skupinový	k2eAgFnSc2d1
</s>
<s>
1976	#num#	k4
Bienale	Bienale	k1gFnSc6
Banská	banský	k2eAgFnSc1d1
Bystrica	Bystrica	k1gFnSc1
(	(	kIx(
<g/>
Slovensko	Slovensko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
1976	#num#	k4
Intergraphic	Intergraphice	k1gInPc2
Berlin	berlina	k1gFnPc2
(	(	kIx(
<g/>
Německo	Německo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
1976	#num#	k4
Trienale	Trienale	k1gMnSc1
Lublin	Lublin	k1gInSc1
(	(	kIx(
<g/>
Polsko	Polsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
1984	#num#	k4
Bienale	Bienale	k1gFnSc6
Banská	banský	k2eAgFnSc1d1
Bystrica	Bystrica	k1gFnSc1
(	(	kIx(
<g/>
Slovensko	Slovensko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
1985	#num#	k4
Trienale	Trienale	k1gMnSc1
Lublin	Lublin	k1gInSc1
(	(	kIx(
<g/>
Polsko	Polsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
1988	#num#	k4
Bienale	Bienale	k1gFnSc6
Banská	banský	k2eAgFnSc1d1
Bystrica	Bystrica	k1gFnSc1
(	(	kIx(
<g/>
Slovensko	Slovensko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
1990	#num#	k4
Bienale	Bienale	k1gFnSc6
Banská	banský	k2eAgFnSc1d1
Bystrica	Bystrica	k1gFnSc1
(	(	kIx(
<g/>
Slovensko	Slovensko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
1990	#num#	k4
Xylon	Xylon	k1gMnSc1
Schweitzinger	Schweitzinger	k1gMnSc1
(	(	kIx(
<g/>
Německo	Německo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
1991	#num#	k4
Xylon	Xylon	k1gMnSc1
Winterthur	Winterthur	k1gMnSc1
(	(	kIx(
<g/>
Švýcarsko	Švýcarsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
1991	#num#	k4
Trienale	Trienale	k1gMnSc1
Lublin	Lublin	k1gInSc1
(	(	kIx(
<g/>
Polsko	Polsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
1993	#num#	k4
Xylon	Xylon	k1gNnSc1
Montreal	Montreal	k1gInSc1
Kanada	Kanada	k1gFnSc1
(	(	kIx(
<g/>
Kanada	Kanada	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
1993	#num#	k4
Inter-Contact	Inter-Contact	k1gMnSc1
Graphic	Graphic	k1gMnSc1
Praha-Berlín	Praha-Berlín	k1gInSc1
(	(	kIx(
<g/>
Německo	Německo	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
1997	#num#	k4
Inter	Inter	k1gMnSc1
Print	Print	k1gMnSc1
Trienal	Trienal	k1gMnSc1
Krakow	Krakow	k1gMnSc1
(	(	kIx(
<g/>
Polsko	Polsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
2000	#num#	k4
Inter	Inter	k1gMnSc1
Bienal	Bienal	k1gMnSc1
Bitola	Bitola	k1gFnSc1
(	(	kIx(
<g/>
Republika	republika	k1gFnSc1
Makedonie	Makedonie	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
2000	#num#	k4
Inter	Inter	k1gInSc1
Trienal	Trienal	k1gFnSc2
Majdanek	Majdanka	k1gFnPc2
(	(	kIx(
<g/>
Polsko	Polsko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Samostatné	samostatný	k2eAgFnPc1d1
výstavy	výstava	k1gFnPc1
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
</s>
<s>
2007	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
,	,	kIx,
2009	#num#	k4
Showspace	Showspace	k1gFnPc1
Gallery	Galler	k1gInPc4
<g/>
,	,	kIx,
Flagstaff	Flagstaff	k1gInSc1
<g/>
,	,	kIx,
Arizona	Arizona	k1gFnSc1
(	(	kIx(
<g/>
USA	USA	kA
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Jiřina	Jiřina	k1gFnSc1
Adamcová	Adamcová	k1gFnSc1
</s>
<s>
Jiřina	Jiřina	k1gFnSc1
Adamcová	Adamcová	k1gFnSc1
</s>
<s>
Okna	okno	k1gNnSc2
k	k	k7c3
naději	naděje	k1gFnSc3
–	–	k?
Pocta	pocta	k1gFnSc1
katedrále	katedrála	k1gFnSc6
</s>
<s>
Výstava	výstava	k1gFnSc1
akademické	akademický	k2eAgFnSc2d1
malířky	malířka	k1gFnSc2
Jiřiny	Jiřina	k1gFnSc2
Adamcové	Adamcová	k1gFnSc2
<g/>
:	:	kIx,
Návrat	návrat	k1gInSc1
</s>
<s>
Jiřina	Jiřina	k1gFnSc1
Adamcová	Adamcová	k1gFnSc1
v	v	k7c6
informačním	informační	k2eAgInSc6d1
systému	systém	k1gInSc6
abART	abART	k?
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
jn	jn	k?
<g/>
19990209002	#num#	k4
|	|	kIx~
ISNI	ISNI	kA
<g/>
:	:	kIx,
0000	#num#	k4
0000	#num#	k4
7896	#num#	k4
3297	#num#	k4
|	|	kIx~
ULAN	ULAN	kA
<g/>
:	:	kIx,
500128244	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
96629962	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Lidé	člověk	k1gMnPc1
|	|	kIx~
umění	umění	k1gNnSc1
</s>
