<s>
Support	support	k1gInSc1	support
vector	vector	k1gMnSc1	vector
machines	machines	k1gMnSc1	machines
(	(	kIx(	(
<g/>
SVM	SVM	kA	SVM
<g/>
)	)	kIx)	)
neboli	neboli	k8xC	neboli
metoda	metoda	k1gFnSc1	metoda
podpůrných	podpůrný	k2eAgInPc2d1	podpůrný
vektorů	vektor	k1gInPc2	vektor
je	být	k5eAaImIp3nS	být
metoda	metoda	k1gFnSc1	metoda
strojového	strojový	k2eAgNnSc2d1	strojové
učení	učení	k1gNnSc2	učení
s	s	k7c7	s
učitelem	učitel	k1gMnSc7	učitel
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgNnSc1d1	sloužící
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
klasifikaci	klasifikace	k1gFnSc4	klasifikace
a	a	k8xC	a
také	také	k9	také
pro	pro	k7c4	pro
regresní	regresní	k2eAgFnSc4d1	regresní
analýzu	analýza	k1gFnSc4	analýza
<g/>
.	.	kIx.	.
</s>
