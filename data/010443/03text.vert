<p>
<s>
Support	support	k1gInSc1	support
vector	vector	k1gMnSc1	vector
machines	machines	k1gMnSc1	machines
(	(	kIx(	(
<g/>
SVM	SVM	kA	SVM
<g/>
)	)	kIx)	)
neboli	neboli	k8xC	neboli
metoda	metoda	k1gFnSc1	metoda
podpůrných	podpůrný	k2eAgInPc2d1	podpůrný
vektorů	vektor	k1gInPc2	vektor
je	být	k5eAaImIp3nS	být
metoda	metoda	k1gFnSc1	metoda
strojového	strojový	k2eAgNnSc2d1	strojové
učení	učení	k1gNnSc2	učení
s	s	k7c7	s
učitelem	učitel	k1gMnSc7	učitel
<g/>
,	,	kIx,	,
sloužící	sloužící	k2eAgNnSc1d1	sloužící
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
klasifikaci	klasifikace	k1gFnSc4	klasifikace
a	a	k8xC	a
také	také	k9	také
pro	pro	k7c4	pro
regresní	regresní	k2eAgFnSc4d1	regresní
analýzu	analýza	k1gFnSc4	analýza
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejím	její	k3xOp3gNnSc6	její
vynalezení	vynalezení	k1gNnSc6	vynalezení
se	se	k3xPyFc4	se
podílel	podílet	k5eAaImAgMnS	podílet
zejména	zejména	k9	zejména
Vladimir	Vladimir	k1gMnSc1	Vladimir
Vapnik	Vapnik	k1gMnSc1	Vapnik
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Základem	základ	k1gInSc7	základ
metody	metoda	k1gFnSc2	metoda
SVM	SVM	kA	SVM
je	být	k5eAaImIp3nS	být
lineární	lineární	k2eAgInSc4d1	lineární
klasifikátor	klasifikátor	k1gInSc4	klasifikátor
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
tříd	třída	k1gFnPc2	třída
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
úlohy	úloha	k1gFnSc2	úloha
je	být	k5eAaImIp3nS	být
nalézt	nalézt	k5eAaPmF	nalézt
nadrovinu	nadrovina	k1gFnSc4	nadrovina
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
prostor	prostor	k1gInSc4	prostor
příznaků	příznak	k1gInPc2	příznak
optimálně	optimálně	k6eAd1	optimálně
rozděluje	rozdělovat	k5eAaImIp3nS	rozdělovat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
trénovací	trénovací	k2eAgNnPc1d1	trénovací
data	datum	k1gNnPc1	datum
náležející	náležející	k2eAgMnSc1d1	náležející
odlišným	odlišný	k2eAgFnPc3d1	odlišná
třídám	třída	k1gFnPc3	třída
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
opačných	opačný	k2eAgInPc6d1	opačný
poloprostorech	poloprostor	k1gInPc6	poloprostor
<g/>
.	.	kIx.	.
</s>
<s>
Optimální	optimální	k2eAgFnSc1d1	optimální
nadrovina	nadrovina	k1gFnSc1	nadrovina
je	být	k5eAaImIp3nS	být
taková	takový	k3xDgFnSc1	takový
<g/>
,	,	kIx,	,
že	že	k8xS	že
hodnota	hodnota	k1gFnSc1	hodnota
minima	minimum	k1gNnSc2	minimum
vzdáleností	vzdálenost	k1gFnPc2	vzdálenost
bodů	bod	k1gInPc2	bod
od	od	k7c2	od
roviny	rovina	k1gFnSc2	rovina
je	být	k5eAaImIp3nS	být
co	co	k9	co
největší	veliký	k2eAgFnSc1d3	veliký
<g/>
.	.	kIx.	.
</s>
<s>
Jinými	jiný	k2eAgNnPc7d1	jiné
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
nadroviny	nadrovina	k1gFnSc2	nadrovina
je	být	k5eAaImIp3nS	být
na	na	k7c4	na
obě	dva	k4xCgFnPc4	dva
strany	strana	k1gFnPc4	strana
co	co	k9	co
nejširší	široký	k2eAgInSc1d3	nejširší
pruh	pruh	k1gInSc1	pruh
bez	bez	k7c2	bez
bodů	bod	k1gInPc2	bod
(	(	kIx(	(
<g/>
maximální	maximální	k2eAgInSc4d1	maximální
odstup	odstup	k1gInSc4	odstup
<g/>
,	,	kIx,	,
angl.	angl.	k?	angl.
maximal	maximal	k1gMnSc1	maximal
margin	margin	k1gMnSc1	margin
<g/>
,	,	kIx,	,
česky	česky	k6eAd1	česky
je	být	k5eAaImIp3nS	být
tento	tento	k3xDgInSc1	tento
pruh	pruh	k1gInSc1	pruh
někdy	někdy	k6eAd1	někdy
nazýván	nazývat	k5eAaImNgInS	nazývat
také	také	k9	také
pásmo	pásmo	k1gNnSc1	pásmo
necitlivosti	necitlivost	k1gFnSc2	necitlivost
nebo	nebo	k8xC	nebo
hraniční	hraniční	k2eAgNnSc1d1	hraniční
pásmo	pásmo	k1gNnSc1	pásmo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
popis	popis	k1gInSc4	popis
nadroviny	nadrovina	k1gFnSc2	nadrovina
stačí	stačit	k5eAaBmIp3nS	stačit
pouze	pouze	k6eAd1	pouze
body	bod	k1gInPc4	bod
ležící	ležící	k2eAgInPc4d1	ležící
na	na	k7c6	na
okraji	okraj	k1gInSc6	okraj
tohoto	tento	k3xDgNnSc2	tento
pásma	pásmo	k1gNnSc2	pásmo
a	a	k8xC	a
těch	ten	k3xDgInPc2	ten
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
málo	málo	k1gNnSc1	málo
-	-	kIx~	-
tyto	tento	k3xDgInPc1	tento
body	bod	k1gInPc1	bod
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
podpůrné	podpůrný	k2eAgInPc1d1	podpůrný
vektory	vektor	k1gInPc1	vektor
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
support	support	k1gInSc1	support
vectors	vectors	k1gInSc1	vectors
<g/>
)	)	kIx)	)
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
název	název	k1gInSc4	název
metody	metoda	k1gFnSc2	metoda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
součástí	součást	k1gFnSc7	součást
techniky	technika	k1gFnSc2	technika
Support	support	k1gInSc1	support
vector	vector	k1gMnSc1	vector
machines	machinesa	k1gFnPc2	machinesa
je	být	k5eAaImIp3nS	být
jádrová	jádrový	k2eAgFnSc1d1	jádrová
transformace	transformace	k1gFnSc1	transformace
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
kernel	kernel	k1gInSc1	kernel
transformation	transformation	k1gInSc1	transformation
<g/>
)	)	kIx)	)
prostoru	prostor	k1gInSc2	prostor
příznaků	příznak	k1gInPc2	příznak
dat	datum	k1gNnPc2	datum
do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
transformovaných	transformovaný	k2eAgInPc2d1	transformovaný
příznaků	příznak	k1gInPc2	příznak
typicky	typicky	k6eAd1	typicky
vyšší	vysoký	k2eAgFnSc2d2	vyšší
dimenze	dimenze	k1gFnSc2	dimenze
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
jádrová	jádrový	k2eAgFnSc1d1	jádrová
transformace	transformace	k1gFnSc1	transformace
umožňuje	umožňovat	k5eAaImIp3nS	umožňovat
převést	převést	k5eAaPmF	převést
původně	původně	k6eAd1	původně
lineárně	lineárně	k6eAd1	lineárně
neseparovatelnou	separovatelný	k2eNgFnSc4d1	separovatelný
úlohu	úloha	k1gFnSc4	úloha
na	na	k7c4	na
úlohu	úloha	k1gFnSc4	úloha
lineárně	lineárně	k6eAd1	lineárně
separovatelnou	separovatelný	k2eAgFnSc4d1	separovatelná
<g/>
,	,	kIx,	,
na	na	k7c4	na
kterou	který	k3yIgFnSc4	který
lze	lze	k6eAd1	lze
dále	daleko	k6eAd2	daleko
aplikovat	aplikovat	k5eAaBmF	aplikovat
optimalizační	optimalizační	k2eAgInSc4d1	optimalizační
algoritmus	algoritmus	k1gInSc4	algoritmus
pro	pro	k7c4	pro
nalezení	nalezení	k1gNnSc4	nalezení
rozdělující	rozdělující	k2eAgFnSc2d1	rozdělující
nadroviny	nadrovina	k1gFnSc2	nadrovina
<g/>
.	.	kIx.	.
</s>
<s>
Trik	trik	k1gInSc1	trik
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
nadrovina	nadrovina	k1gFnSc1	nadrovina
je	být	k5eAaImIp3nS	být
popsána	popsat	k5eAaPmNgFnS	popsat
a	a	k8xC	a
při	při	k7c6	při
výpočtech	výpočet	k1gInPc6	výpočet
je	být	k5eAaImIp3nS	být
použitý	použitý	k2eAgInSc4d1	použitý
pouze	pouze	k6eAd1	pouze
skalární	skalární	k2eAgInSc4d1	skalární
součin	součin	k1gInSc4	součin
<g/>
,	,	kIx,	,
a	a	k8xC	a
skalární	skalární	k2eAgInSc1d1	skalární
součin	součin	k1gInSc1	součin
transformovaných	transformovaný	k2eAgNnPc2d1	transformované
dat	datum	k1gNnPc2	datum
<	<	kIx(	<
<g/>
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
f	f	k?	f
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
>	>	kIx)	>
ve	v	k7c6	v
vysokorozměrném	vysokorozměrný	k2eAgInSc6d1	vysokorozměrný
prostoru	prostor	k1gInSc6	prostor
se	se	k3xPyFc4	se
nepočítá	počítat	k5eNaImIp3nS	počítat
explicitně	explicitně	k6eAd1	explicitně
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
počítá	počítat	k5eAaImIp3nS	počítat
se	se	k3xPyFc4	se
K	k	k7c3	k
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
x	x	k?	x
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
hodnota	hodnota	k1gFnSc1	hodnota
jádrové	jádrový	k2eAgFnSc2d1	jádrová
funkce	funkce	k1gFnSc2	funkce
K	k	k7c3	k
na	na	k7c6	na
datech	datum	k1gNnPc6	datum
v	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
prostoru	prostor	k1gInSc6	prostor
parametrů	parametr	k1gInPc2	parametr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Používají	používat	k5eAaImIp3nP	používat
se	se	k3xPyFc4	se
různé	různý	k2eAgFnPc1d1	různá
kernelové	kernelový	k2eAgFnPc1d1	kernelový
transformace	transformace	k1gFnPc1	transformace
<g/>
.	.	kIx.	.
</s>
<s>
Intuitivně	intuitivně	k6eAd1	intuitivně
<g/>
,	,	kIx,	,
vyjadřují	vyjadřovat	k5eAaImIp3nP	vyjadřovat
podobnost	podobnost	k1gFnSc4	podobnost
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
tj.	tj.	kA	tj.
svých	svůj	k3xOyFgInPc2	svůj
dvou	dva	k4xCgInPc2	dva
vstupních	vstupní	k2eAgInPc2d1	vstupní
argumentů	argument	k1gInPc2	argument
<g/>
.	.	kIx.	.
</s>
<s>
Výhodou	výhoda	k1gFnSc7	výhoda
této	tento	k3xDgFnSc2	tento
metody	metoda	k1gFnSc2	metoda
(	(	kIx(	(
<g/>
a	a	k8xC	a
jiných	jiný	k2eAgFnPc2d1	jiná
metod	metoda	k1gFnPc2	metoda
založených	založený	k2eAgFnPc2d1	založená
na	na	k7c6	na
jádrové	jádrový	k2eAgFnSc6d1	jádrová
transformaci	transformace	k1gFnSc6	transformace
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
transformace	transformace	k1gFnSc1	transformace
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
definovat	definovat	k5eAaBmF	definovat
pro	pro	k7c4	pro
různé	různý	k2eAgInPc4d1	různý
typy	typ	k1gInPc4	typ
objektů	objekt	k1gInPc2	objekt
<g/>
,	,	kIx,	,
nejen	nejen	k6eAd1	nejen
body	bod	k1gInPc4	bod
v	v	k7c6	v
Rn	Rn	k1gFnSc6	Rn
<g/>
.	.	kIx.	.
</s>
<s>
Např.	např.	kA	např.
pro	pro	k7c4	pro
grafy	graf	k1gInPc4	graf
<g/>
,	,	kIx,	,
stromy	strom	k1gInPc4	strom
<g/>
,	,	kIx,	,
posloupnosti	posloupnost	k1gFnPc4	posloupnost
DNA	DNA	kA	DNA
...	...	k?	...
</s>
</p>
<p>
<s>
==	==	k?	==
Lineární	lineární	k2eAgMnSc1d1	lineární
SVM	SVM	kA	SVM
==	==	k?	==
</s>
</p>
<p>
<s>
Lineární	lineární	k2eAgInSc1d1	lineární
SVM	SVM	kA	SVM
je	být	k5eAaImIp3nS	být
jednodušší	jednoduchý	k2eAgFnSc1d2	jednodušší
varianta	varianta	k1gFnSc1	varianta
SVM	SVM	kA	SVM
metody	metoda	k1gFnPc1	metoda
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yQgFnSc6	který
zůstáváme	zůstávat	k5eAaImIp1nP	zůstávat
v	v	k7c6	v
původním	původní	k2eAgInSc6d1	původní
prostoru	prostor	k1gInSc6	prostor
příznaků	příznak	k1gInPc2	příznak
a	a	k8xC	a
nedochází	docházet	k5eNaImIp3nS	docházet
k	k	k7c3	k
žádné	žádný	k3yNgFnSc3	žádný
jádrové	jádrový	k2eAgFnSc3d1	jádrová
transformaci	transformace	k1gFnSc3	transformace
<g/>
.	.	kIx.	.
</s>
<s>
Výsledkem	výsledek	k1gInSc7	výsledek
potom	potom	k6eAd1	potom
je	být	k5eAaImIp3nS	být
čistě	čistě	k6eAd1	čistě
lineární	lineární	k2eAgInSc1d1	lineární
klasifikátor	klasifikátor	k1gInSc1	klasifikátor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Separabilní	Separabilní	k2eAgInSc1d1	Separabilní
případ	případ	k1gInSc1	případ
===	===	k?	===
</s>
</p>
<p>
<s>
Nejjednodušším	jednoduchý	k2eAgInSc7d3	nejjednodušší
případem	případ	k1gInSc7	případ
použití	použití	k1gNnSc2	použití
SVM	SVM	kA	SVM
je	být	k5eAaImIp3nS	být
lineární	lineární	k2eAgInSc1d1	lineární
klasifikátor	klasifikátor	k1gInSc1	klasifikátor
pro	pro	k7c4	pro
lineárně	lineárně	k6eAd1	lineárně
separabilní	separabilní	k2eAgNnPc4d1	separabilní
data	datum	k1gNnPc4	datum
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
data	datum	k1gNnPc1	datum
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
lze	lze	k6eAd1	lze
obě	dva	k4xCgFnPc1	dva
třídy	třída	k1gFnPc1	třída
od	od	k7c2	od
sebe	se	k3xPyFc2	se
rozdělit	rozdělit	k5eAaPmF	rozdělit
nadrovinou	nadrovina	k1gFnSc7	nadrovina
<g/>
.	.	kIx.	.
</s>
<s>
Mějme	mít	k5eAaImRp1nP	mít
množinu	množina	k1gFnSc4	množina
trénovacích	trénovací	k2eAgNnPc2d1	trénovací
dat	datum	k1gNnPc2	datum
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
dvojic	dvojice	k1gFnPc2	dvojice
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
vektor	vektor	k1gInSc4	vektor
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
je	on	k3xPp3gFnPc4	on
informace	informace	k1gFnPc4	informace
od	od	k7c2	od
učitele	učitel	k1gMnSc2	učitel
nabývající	nabývající	k2eAgFnSc2d1	nabývající
hodnoty	hodnota	k1gFnSc2	hodnota
+1	+1	k4	+1
nebo	nebo	k8xC	nebo
-1	-1	k4	-1
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nadrovinu	Nadrovina	k1gFnSc4	Nadrovina
<g/>
,	,	kIx,	,
rozdělující	rozdělující	k2eAgInPc1d1	rozdělující
body	bod	k1gInPc1	bod
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
tříd	třída	k1gFnPc2	třída
<g/>
,	,	kIx,	,
lze	lze	k6eAd1	lze
popsat	popsat	k5eAaPmF	popsat
rovnicí	rovnice	k1gFnSc7	rovnice
</s>
</p>
<p>
<s>
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
w	w	k?	w
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
normála	normála	k1gFnSc1	normála
nadroviny	nadrovina	k1gFnSc2	nadrovina
<g/>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
||	||	k?	||
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
w	w	k?	w
<g/>
}}	}}	k?	}}
<g/>
||	||	k?	||
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
její	její	k3xOp3gFnSc1	její
Eukleidovská	eukleidovský	k2eAgFnSc1d1	eukleidovská
norma	norma	k1gFnSc1	norma
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
|	|	kIx~	|
<g/>
b	b	k?	b
<g/>
|	|	kIx~	|
<g/>
/	/	kIx~	/
<g/>
||	||	k?	||
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
w	w	k?	w
<g/>
}}	}}	k?	}}
<g/>
||	||	k?	||
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
nadroviny	nadrovina	k1gFnSc2	nadrovina
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
souřadnic	souřadnice	k1gFnPc2	souřadnice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejkratší	krátký	k2eAgFnPc4d3	nejkratší
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
nejbližších	blízký	k2eAgMnPc2d3	nejbližší
bodů	bod	k1gInPc2	bod
na	na	k7c6	na
každé	každý	k3xTgFnSc6	každý
straně	strana	k1gFnSc6	strana
nadroviny	nadrovina	k1gFnSc2	nadrovina
od	od	k7c2	od
ní	on	k3xPp3gFnSc2	on
značíme	značit	k5eAaImIp1nP	značit
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
d_	d_	k?	d_
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
d_	d_	k?	d_
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
Součet	součet	k1gInSc1	součet
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
d_	d_	k?	d_
<g/>
{	{	kIx(	{
<g/>
+	+	kIx~	+
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
d_	d_	k?	d_
<g/>
{	{	kIx(	{
<g/>
-	-	kIx~	-
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
pak	pak	k6eAd1	pak
představuje	představovat	k5eAaImIp3nS	představovat
šířku	šířka	k1gFnSc4	šířka
hraničního	hraniční	k2eAgNnSc2d1	hraniční
pásma	pásmo	k1gNnSc2	pásmo
<g/>
.	.	kIx.	.
</s>
<s>
Cílem	cíl	k1gInSc7	cíl
trénování	trénování	k1gNnSc2	trénování
SVM	SVM	kA	SVM
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
nalézt	nalézt	k5eAaBmF	nalézt
takovou	takový	k3xDgFnSc4	takový
rozdělující	rozdělující	k2eAgFnSc4d1	rozdělující
nadrovinu	nadrovina	k1gFnSc4	nadrovina
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
hraniční	hraniční	k2eAgNnSc1d1	hraniční
pásmo	pásmo	k1gNnSc1	pásmo
bylo	být	k5eAaImAgNnS	být
nejširší	široký	k2eAgNnSc1d3	nejširší
možné	možný	k2eAgNnSc1d1	možné
<g/>
.	.	kIx.	.
</s>
<s>
Body	bod	k1gInPc1	bod
<g/>
,	,	kIx,	,
ležící	ležící	k2eAgNnPc1d1	ležící
na	na	k7c4	na
hranici	hranice	k1gFnSc4	hranice
tohoto	tento	k3xDgNnSc2	tento
pásma	pásmo	k1gNnSc2	pásmo
a	a	k8xC	a
jejichž	jejichž	k3xOyRp3gNnSc7	jejichž
odebráním	odebrání	k1gNnSc7	odebrání
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
poloha	poloha	k1gFnSc1	poloha
optimální	optimální	k2eAgFnSc2d1	optimální
nadroviny	nadrovina	k1gFnSc2	nadrovina
změnila	změnit	k5eAaPmAgFnS	změnit
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
podpůrné	podpůrný	k2eAgInPc4d1	podpůrný
vektory	vektor	k1gInPc4	vektor
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
support	support	k1gInSc1	support
vectors	vectors	k1gInSc1	vectors
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Formulace	formulace	k1gFnSc2	formulace
úlohy	úloha	k1gFnSc2	úloha
====	====	k?	====
</s>
</p>
<p>
<s>
Předpokládejme	předpokládat	k5eAaImRp1nP	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
body	bod	k1gInPc4	bod
platí	platit	k5eAaImIp3nS	platit
</s>
</p>
<p>
<s>
což	což	k3yRnSc1	což
lze	lze	k6eAd1	lze
sloučit	sloučit	k5eAaPmF	sloučit
do	do	k7c2	do
jediné	jediný	k2eAgFnSc2d1	jediná
nerovnice	nerovnice	k1gFnSc2	nerovnice
</s>
</p>
<p>
<s>
Body	bod	k1gInPc4	bod
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yIgFnPc4	který
platí	platit	k5eAaImIp3nS	platit
rovnost	rovnost	k1gFnSc1	rovnost
v	v	k7c6	v
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
nadrovině	nadrovina	k1gFnSc6	nadrovina
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
H_	H_	k1gMnSc6	H_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
<g/>
:	:	kIx,	:
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
w	w	k?	w
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
b	b	k?	b
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Body	bod	k1gInPc4	bod
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yQgFnPc4	který
platí	platit	k5eAaImIp3nS	platit
rovnost	rovnost	k1gFnSc1	rovnost
v	v	k7c6	v
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
nadrovině	nadrovina	k1gFnSc6	nadrovina
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
H_	H_	k1gMnSc6	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
:	:	kIx,	:
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
w	w	k?	w
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
b	b	k?	b
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Nadroviny	Nadrovina	k1gFnPc1	Nadrovina
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
H_	H_	k1gMnSc6	H_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
H_	H_	k1gMnSc6	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
jsou	být	k5eAaImIp3nP	být
kolmé	kolmý	k2eAgFnPc1d1	kolmá
na	na	k7c4	na
stejný	stejný	k2eAgInSc4d1	stejný
normálový	normálový	k2eAgInSc4d1	normálový
vektor	vektor	k1gInSc4	vektor
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
w	w	k?	w
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
a	a	k8xC	a
jejich	jejich	k3xOp3gFnSc4	jejich
vzdálenost	vzdálenost	k1gFnSc4	vzdálenost
od	od	k7c2	od
rozdělující	rozdělující	k2eAgFnSc2d1	rozdělující
nadroviny	nadrovina	k1gFnSc2	nadrovina
je	být	k5eAaImIp3nS	být
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
1	[number]	k4	1
<g/>
/	/	kIx~	/
<g/>
||	||	k?	||
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
w	w	k?	w
<g/>
}}	}}	k?	}}
<g/>
||	||	k?	||
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Šířka	šířka	k1gFnSc1	šířka
hraničního	hraniční	k2eAgNnSc2d1	hraniční
pásma	pásmo	k1gNnSc2	pásmo
tedy	tedy	k9	tedy
je	být	k5eAaImIp3nS	být
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
2	[number]	k4	2
<g/>
/	/	kIx~	/
<g/>
||	||	k?	||
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
w	w	k?	w
<g/>
}}	}}	k?	}}
<g/>
||	||	k?	||
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
nadroviny	nadrovina	k1gFnPc1	nadrovina
s	s	k7c7	s
nejširším	široký	k2eAgNnSc7d3	nejširší
pásmem	pásmo	k1gNnSc7	pásmo
můžeme	moct	k5eAaImIp1nP	moct
určit	určit	k5eAaPmF	určit
minimalizací	minimalizace	k1gFnSc7	minimalizace
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
||	||	k?	||
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
w	w	k?	w
<g/>
}}	}}	k?	}}
<g/>
||	||	k?	||
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
podmínkám	podmínka	k1gFnPc3	podmínka
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pozn	pozn	kA	pozn
<g/>
.	.	kIx.	.
<g/>
:	:	kIx,	:
Z	z	k7c2	z
praktických	praktický	k2eAgInPc2d1	praktický
důvodů	důvod	k1gInPc2	důvod
se	se	k3xPyFc4	se
místo	místo	k7c2	místo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
||	||	k?	||
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
w	w	k?	w
<g/>
}}	}}	k?	}}
<g/>
||	||	k?	||
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
minimalizuje	minimalizovat	k5eAaBmIp3nS	minimalizovat
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
||	||	k?	||
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
w	w	k?	w
<g/>
}}	}}	k?	}}
<g/>
||	||	k?	||
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
====	====	k?	====
Lagrangeovská	Lagrangeovský	k2eAgFnSc1d1	Lagrangeovský
formulace	formulace	k1gFnSc1	formulace
====	====	k?	====
</s>
</p>
<p>
<s>
Lagrangeovská	Lagrangeovský	k2eAgFnSc1d1	Lagrangeovský
formulace	formulace	k1gFnSc1	formulace
úlohy	úloha	k1gFnSc2	úloha
má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
hlavní	hlavní	k2eAgFnPc4d1	hlavní
výhody	výhoda	k1gFnPc4	výhoda
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Namísto	namísto	k7c2	namísto
podmínky	podmínka	k1gFnSc2	podmínka
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
omezení	omezení	k1gNnSc4	omezení
na	na	k7c4	na
samotné	samotný	k2eAgMnPc4d1	samotný
Lagrangeovy	Lagrangeův	k2eAgMnPc4d1	Lagrangeův
multiplikátory	multiplikátor	k1gMnPc4	multiplikátor
<g/>
,	,	kIx,	,
s	s	k7c7	s
čímž	což	k3yQnSc7	což
se	se	k3xPyFc4	se
lépe	dobře	k6eAd2	dobře
pracuje	pracovat	k5eAaImIp3nS	pracovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Trénovací	trénovací	k2eAgNnPc1d1	trénovací
data	datum	k1gNnPc1	datum
se	se	k3xPyFc4	se
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
výrazech	výraz	k1gInPc6	výraz
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
jen	jen	k9	jen
ve	v	k7c6	v
skalárních	skalární	k2eAgInPc6d1	skalární
součinech	součin	k1gInPc6	součin
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
důležité	důležitý	k2eAgNnSc1d1	důležité
u	u	k7c2	u
nelineárního	lineární	k2eNgInSc2d1	nelineární
případu	případ	k1gInSc2	případ
<g/>
.	.	kIx.	.
<g/>
Zavedené	zavedený	k2eAgInPc1d1	zavedený
nezáporné	záporný	k2eNgInPc1d1	nezáporný
Lagrangeovy	Lagrangeův	k2eAgInPc1d1	Lagrangeův
multiplikátory	multiplikátor	k1gInPc1	multiplikátor
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
λ	λ	k?	λ
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
...	...	k?	...
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
l	l	kA	l
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
i	i	k9	i
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
dots	dots	k1gInSc1	dots
,	,	kIx,	,
<g/>
l	l	kA	l
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
jeden	jeden	k4xCgInSc1	jeden
pro	pro	k7c4	pro
každou	každý	k3xTgFnSc4	každý
nerovnost	nerovnost	k1gFnSc4	nerovnost
v	v	k7c6	v
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Lagrangeova	Lagrangeův	k2eAgFnSc1d1	Lagrangeova
funkce	funkce	k1gFnSc1	funkce
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
minimalizovat	minimalizovat	k5eAaBmF	minimalizovat
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
</p>
<p>
<s>
P	P	kA	P
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L_	L_	k1gMnSc1	L_
<g/>
{	{	kIx(	{
<g/>
P	P	kA	P
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
w	w	k?	w
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
a	a	k8xC	a
derivace	derivace	k1gFnSc1	derivace
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
</p>
<p>
<s>
P	P	kA	P
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L_	L_	k1gMnSc1	L_
<g/>
{	{	kIx(	{
<g/>
P	P	kA	P
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
všem	všecek	k3xTgFnPc3	všecek
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
λ	λ	k?	λ
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
musejí	muset	k5eAaImIp3nP	muset
být	být	k5eAaImF	být
nulové	nulový	k2eAgInPc1d1	nulový
<g/>
,	,	kIx,	,
vše	všechen	k3xTgNnSc1	všechen
za	za	k7c4	za
omezení	omezení	k1gNnSc4	omezení
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
λ	λ	k?	λ
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
≥	≥	k?	≥
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
geq	geq	k?	geq
0	[number]	k4	0
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
konvexní	konvexní	k2eAgInSc4d1	konvexní
problém	problém	k1gInSc4	problém
kvadratického	kvadratický	k2eAgNnSc2d1	kvadratické
programování	programování	k1gNnSc2	programování
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
lze	lze	k6eAd1	lze
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
řešit	řešit	k5eAaImF	řešit
duální	duální	k2eAgInSc1d1	duální
problém	problém	k1gInSc1	problém
-	-	kIx~	-
hledání	hledání	k1gNnSc1	hledání
maxima	maximum	k1gNnSc2	maximum
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
</p>
<p>
<s>
P	P	kA	P
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L_	L_	k1gMnSc1	L_
<g/>
{	{	kIx(	{
<g/>
P	P	kA	P
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
za	za	k7c2	za
následujících	následující	k2eAgFnPc2d1	následující
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
:	:	kIx,	:
že	že	k8xS	že
gradient	gradient	k1gInSc1	gradient
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
</p>
<p>
<s>
P	P	kA	P
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L_	L_	k1gMnSc1	L_
<g/>
{	{	kIx(	{
<g/>
P	P	kA	P
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
w	w	k?	w
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
b	b	k?	b
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
nulový	nulový	k2eAgMnSc1d1	nulový
<g/>
,	,	kIx,	,
a	a	k8xC	a
že	že	k8xS	že
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
λ	λ	k?	λ
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
≥	≥	k?	≥
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
geq	geq	k?	geq
0	[number]	k4	0
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Aby	aby	kYmCp3nP	aby
gradient	gradient	k1gInSc1	gradient
podle	podle	k7c2	podle
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
w	w	k?	w
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
b	b	k?	b
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
byl	být	k5eAaImAgInS	být
nulový	nulový	k2eAgInSc1d1	nulový
<g/>
,	,	kIx,	,
musí	muset	k5eAaImIp3nS	muset
platit	platit	k5eAaImF	platit
</s>
</p>
<p>
<s>
Dosazením	dosazení	k1gNnSc7	dosazení
do	do	k7c2	do
(	(	kIx(	(
<g/>
5	[number]	k4	5
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
získá	získat	k5eAaPmIp3nS	získat
duální	duální	k2eAgFnSc1d1	duální
Lagrangeova	Lagrangeův	k2eAgFnSc1d1	Lagrangeova
funkce	funkce	k1gFnSc1	funkce
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
</p>
<p>
<s>
P	P	kA	P
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L_	L_	k1gMnSc1	L_
<g/>
{	{	kIx(	{
<g/>
P	P	kA	P
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
-	-	kIx~	-
primární	primární	k2eAgFnSc1d1	primární
<g/>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
</p>
<p>
<s>
D	D	kA	D
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L_	L_	k1gMnSc1	L_
<g/>
{	{	kIx(	{
<g/>
D	D	kA	D
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
-	-	kIx~	-
duální	duální	k2eAgFnSc4d1	duální
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Trénování	trénování	k1gNnSc1	trénování
SVM	SVM	kA	SVM
spočívá	spočívat	k5eAaImIp3nS	spočívat
v	v	k7c6	v
maximalizaci	maximalizace	k1gFnSc6	maximalizace
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
</p>
<p>
<s>
D	D	kA	D
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
L_	L_	k1gMnSc1	L_
<g/>
{	{	kIx(	{
<g/>
D	D	kA	D
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
λ	λ	k?	λ
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
při	při	k7c6	při
omezení	omezení	k1gNnSc6	omezení
(	(	kIx(	(
<g/>
8	[number]	k4	8
<g/>
)	)	kIx)	)
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
λ	λ	k?	λ
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
≥	≥	k?	≥
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
geq	geq	k?	geq
0	[number]	k4	0
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
s	s	k7c7	s
řešením	řešení	k1gNnSc7	řešení
daným	daný	k2eAgInSc7d1	daný
(	(	kIx(	(
<g/>
6	[number]	k4	6
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Máme	mít	k5eAaImIp1nP	mít
jedno	jeden	k4xCgNnSc4	jeden
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
λ	λ	k?	λ
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
pro	pro	k7c4	pro
každý	každý	k3xTgInSc4	každý
trénovací	trénovací	k2eAgInSc4d1	trénovací
bod	bod	k1gInSc4	bod
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
body	bod	k1gInPc4	bod
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yIgFnPc4	který
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
λ	λ	k?	λ
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
>	>	kIx)	>
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
>	>	kIx)	>
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
jsou	být	k5eAaImIp3nP	být
podpůrné	podpůrný	k2eAgInPc4d1	podpůrný
vektory	vektor	k1gInPc4	vektor
a	a	k8xC	a
leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
nadrovinách	nadrovina	k1gFnPc6	nadrovina
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
H_	H_	k1gMnSc6	H_
<g/>
{	{	kIx(	{
<g/>
1	[number]	k4	1
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
nebo	nebo	k8xC	nebo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
H_	H_	k1gMnSc6	H_
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
všechny	všechen	k3xTgMnPc4	všechen
ostatní	ostatní	k2eAgMnPc4d1	ostatní
mají	mít	k5eAaImIp3nP	mít
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
λ	λ	k?	λ
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
leží	ležet	k5eAaImIp3nS	ležet
buď	buď	k8xC	buď
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
z	z	k7c2	z
nadrovin	nadrovina	k1gFnPc2	nadrovina
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
na	na	k7c6	na
té	ten	k3xDgFnSc6	ten
straně	strana	k1gFnSc6	strana
od	od	k7c2	od
nich	on	k3xPp3gFnPc2	on
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
kterou	který	k3yQgFnSc4	který
platí	platit	k5eAaImIp3nS	platit
ostrá	ostrý	k2eAgFnSc1d1	ostrá
nerovnost	nerovnost	k1gFnSc1	nerovnost
v	v	k7c6	v
(	(	kIx(	(
<g/>
4	[number]	k4	4
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Podpůrné	podpůrný	k2eAgInPc1d1	podpůrný
vektory	vektor	k1gInPc1	vektor
jsou	být	k5eAaImIp3nP	být
nejdůležitějšími	důležitý	k2eAgInPc7d3	nejdůležitější
body	bod	k1gInPc7	bod
trénovací	trénovací	k2eAgFnSc2d1	trénovací
množiny	množina	k1gFnSc2	množina
-	-	kIx~	-
pokud	pokud	k8xS	pokud
by	by	kYmCp3nP	by
se	se	k3xPyFc4	se
všechny	všechen	k3xTgInPc1	všechen
ostatní	ostatní	k2eAgInPc1d1	ostatní
body	bod	k1gInPc1	bod
odstranily	odstranit	k5eAaPmAgInP	odstranit
<g/>
,	,	kIx,	,
novým	nový	k2eAgNnSc7d1	nové
natrénováním	natrénování	k1gNnSc7	natrénování
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
získala	získat	k5eAaPmAgFnS	získat
tatáž	týž	k3xTgFnSc1	týž
rozdělující	rozdělující	k2eAgFnSc1d1	rozdělující
nadrovina	nadrovina	k1gFnSc1	nadrovina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Karushovy-Kuhnovy-Tuckerovy	Karushovy-Kuhnovy-Tuckerův	k2eAgFnPc4d1	Karushovy-Kuhnovy-Tuckerův
podmínky	podmínka	k1gFnPc4	podmínka
====	====	k?	====
</s>
</p>
<p>
<s>
Karushovy-Kuhnovy-Tuckerovy	Karushovy-Kuhnovy-Tuckerův	k2eAgInPc1d1	Karushovy-Kuhnovy-Tuckerův
(	(	kIx(	(
<g/>
KKT	KKT	kA	KKT
<g/>
)	)	kIx)	)
podmínky	podmínka	k1gFnPc1	podmínka
jsou	být	k5eAaImIp3nP	být
nutné	nutný	k2eAgFnPc1d1	nutná
podmínky	podmínka	k1gFnPc1	podmínka
pro	pro	k7c4	pro
optimální	optimální	k2eAgNnSc4d1	optimální
řešení	řešení	k1gNnSc4	řešení
v	v	k7c6	v
úloze	úloha	k1gFnSc6	úloha
nelineárního	lineární	k2eNgNnSc2d1	nelineární
programování	programování	k1gNnSc2	programování
za	za	k7c2	za
předpokladu	předpoklad	k1gInSc2	předpoklad
splnění	splnění	k1gNnSc4	splnění
určitých	určitý	k2eAgFnPc2d1	určitá
podmínek	podmínka	k1gFnPc2	podmínka
regularity	regularita	k1gFnSc2	regularita
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
konvexní	konvexní	k2eAgInPc4d1	konvexní
problémy	problém	k1gInPc4	problém
jsou	být	k5eAaImIp3nP	být
zároveň	zároveň	k6eAd1	zároveň
i	i	k8xC	i
postačujícími	postačující	k2eAgFnPc7d1	postačující
podmínkami	podmínka	k1gFnPc7	podmínka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SVM	SVM	kA	SVM
představují	představovat	k5eAaImIp3nP	představovat
konvexní	konvexní	k2eAgInSc4d1	konvexní
problém	problém	k1gInSc4	problém
a	a	k8xC	a
podmínky	podmínka	k1gFnPc4	podmínka
regularity	regularita	k1gFnSc2	regularita
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
ně	on	k3xPp3gMnPc4	on
splněné	splněný	k2eAgMnPc4d1	splněný
vždy	vždy	k6eAd1	vždy
<g/>
.	.	kIx.	.
</s>
<s>
KKT	KKT	kA	KKT
podmínky	podmínka	k1gFnPc1	podmínka
jsou	být	k5eAaImIp3nP	být
tím	ten	k3xDgInSc7	ten
pádem	pád	k1gInSc7	pád
nutné	nutný	k2eAgNnSc4d1	nutné
a	a	k8xC	a
postačující	postačující	k2eAgNnSc4d1	postačující
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
λ	λ	k?	λ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
w	w	k?	w
<g/>
}}	}}	k?	}}
<g/>
,	,	kIx,	,
<g/>
b	b	k?	b
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc2	lambda
}	}	kIx)	}
</s>
</p>
<p>
<s>
byly	být	k5eAaImAgFnP	být
řešením	řešení	k1gNnPc3	řešení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
primární	primární	k2eAgInSc4d1	primární
problém	problém	k1gInSc4	problém
KKT	KKT	kA	KKT
podmínky	podmínka	k1gFnPc1	podmínka
vypadají	vypadat	k5eAaPmIp3nP	vypadat
následovně	následovně	k6eAd1	následovně
<g/>
:	:	kIx,	:
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
</p>
<p>
<s>
ν	ν	k?	ν
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
</p>
<p>
<s>
P	P	kA	P
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
</p>
<p>
<s>
ν	ν	k?	ν
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
<s>
∑	∑	k?	∑
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
λ	λ	k?	λ
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
<s>
ν	ν	k?	ν
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ν	ν	k?	ν
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
...	...	k?	...
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
d	d	k?	d
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
</p>
<p>
<s>
∂	∂	k?	∂
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
</p>
<p>
<s>
P	P	kA	P
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
<s>
∑	∑	k?	∑
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
λ	λ	k?	λ
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
≥	≥	k?	≥
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
...	...	k?	...
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
l	l	kA	l
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
λ	λ	k?	λ
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
≥	≥	k?	≥
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∀	∀	k?	∀
</s>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
λ	λ	k?	λ
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
∀	∀	k?	∀
</s>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
begin	begin	k1gMnSc1	begin
<g/>
{	{	kIx(	{
<g/>
aligned	aligned	k1gMnSc1	aligned
<g/>
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
w_	w_	k?	w_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}}}	}}}	k?	}}}
<g/>
L_	L_	k1gMnSc1	L_
<g/>
{	{	kIx(	{
<g/>
P	P	kA	P
<g/>
}	}	kIx)	}
<g/>
<g />
.	.	kIx.	.
</s>
<s>
=	=	kIx~	=
<g/>
w_	w_	k?	w_
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
\	\	kIx~	\
<g />
.	.	kIx.	.
</s>
<s>
<g/>
nu	nu	k9	nu
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
qquad	qquad	k1gInSc1	qquad
\	\	kIx~	\
<g/>
nu	nu	k9	nu
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
dots	dots	k1gInSc1	dots
,	,	kIx,	,
<g/>
d	d	k?	d
<g/>
\\	\\	k?	\\
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
frac	frac	k6eAd1	frac
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
}	}	kIx)	}
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
partial	partial	k1gInSc1	partial
b	b	k?	b
<g/>
<g />
.	.	kIx.	.
</s>
<s>
}}	}}	k?	}}
<g/>
L_	L_	k1gMnSc1	L_
<g/>
{	{	kIx(	{
<g/>
P	P	kA	P
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
\\	\\	k?	\\
<g/>
<g />
.	.	kIx.	.
</s>
<s>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
w	w	k?	w
<g/>
}	}	kIx)	}
+	+	kIx~	+
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
\	\	kIx~	\
<g/>
geq	geq	k?	geq
<g />
.	.	kIx.	.
</s>
<s>
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
qquad	qquad	k6eAd1	qquad
i	i	k9	i
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
dots	dots	k1gInSc1	dots
,	,	kIx,	,
<g/>
l	l	kA	l
<g/>
\\\	\\\	k?	\\\
<g/>
lambda	lambda	k1gNnSc1	lambda
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
geq	geq	k?	geq
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
qquad	qquad	k1gInSc1	qquad
\	\	kIx~	\
<g/>
forall	foralnout	k5eAaPmAgInS	foralnout
i	i	k9	i
<g/>
\\\	\\\	k?	\\\
<g/>
<g />
.	.	kIx.	.
</s>
<s>
lambda	lambda	k1gNnSc1	lambda
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
y_	y_	k?	y_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
(	(	kIx(	(
<g/>
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gInSc1	mathbf
{	{	kIx(	{
<g/>
w	w	k?	w
<g/>
}	}	kIx)	}
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
mathbf	mathbf	k1gMnSc1	mathbf
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}	}	kIx)	}
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
+	+	kIx~	+
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
<g/>
-	-	kIx~	-
<g/>
1	[number]	k4	1
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
0	[number]	k4	0
<g/>
&	&	k?	&
<g/>
\	\	kIx~	\
<g/>
qquad	qquad	k1gInSc1	qquad
\	\	kIx~	\
<g/>
forall	foralnout	k5eAaPmAgInS	foralnout
i	i	k9	i
<g/>
\	\	kIx~	\
<g/>
end	end	k?	end
<g/>
{	{	kIx(	{
<g/>
aligned	aligned	k1gMnSc1	aligned
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
KKT	KKT	kA	KKT
podmínky	podmínka	k1gFnPc1	podmínka
hrají	hrát	k5eAaImIp3nP	hrát
roli	role	k1gFnSc4	role
při	při	k7c6	při
numerickém	numerický	k2eAgNnSc6d1	numerické
řešení	řešení	k1gNnSc6	řešení
SVM	SVM	kA	SVM
úlohy	úloha	k1gFnSc2	úloha
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
sekce	sekce	k1gFnSc2	sekce
Metody	metoda	k1gFnPc1	metoda
řešení	řešení	k1gNnSc1	řešení
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Testovací	testovací	k2eAgFnSc1d1	testovací
fáze	fáze	k1gFnSc1	fáze
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
testovací	testovací	k2eAgFnSc6d1	testovací
fázi	fáze	k1gFnSc6	fáze
klasifikujeme	klasifikovat	k5eAaImIp1nP	klasifikovat
neznámé	známý	k2eNgInPc4d1	neznámý
body	bod	k1gInPc4	bod
<g/>
,	,	kIx,	,
u	u	k7c2	u
nichž	jenž	k3xRgFnPc2	jenž
nevíme	vědět	k5eNaImIp1nP	vědět
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yQgFnSc2	který
třídy	třída	k1gFnSc2	třída
skutečně	skutečně	k6eAd1	skutečně
náležejí	náležet	k5eAaImIp3nP	náležet
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc4	tento
zjistíme	zjistit	k5eAaPmIp1nP	zjistit
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
spočítáme	spočítat	k5eAaPmIp1nP	spočítat
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
sgn	sgn	k?	sgn
</s>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
operatorname	operatornam	k1gInSc5	operatornam
{	{	kIx(	{
<g/>
sgn	sgn	k?	sgn
<g/>
}	}	kIx)	}
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
w	w	k?	w
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x	x	k?	x
<g/>
}}	}}	k?	}}
<g/>
+	+	kIx~	+
<g/>
b	b	k?	b
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
výsledné	výsledný	k2eAgNnSc1d1	výsledné
znaménko	znaménko	k1gNnSc1	znaménko
nám	my	k3xPp1nPc3	my
řekne	říct	k5eAaPmIp3nS	říct
<g/>
,	,	kIx,	,
na	na	k7c6	na
které	který	k3yIgFnSc6	který
straně	strana	k1gFnSc6	strana
rozdělující	rozdělující	k2eAgFnSc2d1	rozdělující
nadroviny	nadrovina	k1gFnSc2	nadrovina
daný	daný	k2eAgInSc4d1	daný
bod	bod	k1gInSc4	bod
leží	ležet	k5eAaImIp3nS	ležet
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
ho	on	k3xPp3gInSc4	on
přiřadíme	přiřadit	k5eAaPmIp1nP	přiřadit
do	do	k7c2	do
odpovídající	odpovídající	k2eAgFnSc2d1	odpovídající
třídy	třída	k1gFnSc2	třída
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Neseparabilní	Neseparabilní	k2eAgInSc1d1	Neseparabilní
případ	případ	k1gInSc1	případ
===	===	k?	===
</s>
</p>
<p>
<s>
Složitější	složitý	k2eAgFnSc7d2	složitější
variantou	varianta	k1gFnSc7	varianta
lineárních	lineární	k2eAgInPc2d1	lineární
SVM	SVM	kA	SVM
je	být	k5eAaImIp3nS	být
případ	případ	k1gInSc4	případ
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
se	se	k3xPyFc4	se
snažíme	snažit	k5eAaImIp1nP	snažit
lineárně	lineárně	k6eAd1	lineárně
oddělit	oddělit	k5eAaPmF	oddělit
data	datum	k1gNnPc4	datum
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
nejsou	být	k5eNaImIp3nP	být
plně	plně	k6eAd1	plně
lineárně	lineárně	k6eAd1	lineárně
separovatelná	separovatelný	k2eAgFnSc1d1	separovatelná
<g/>
.	.	kIx.	.
</s>
<s>
Například	například	k6eAd1	například
se	se	k3xPyFc4	se
může	moct	k5eAaImIp3nS	moct
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
zašuměná	zašuměný	k2eAgNnPc4d1	zašuměné
data	datum	k1gNnPc4	datum
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
třídy	třída	k1gFnPc1	třída
částečně	částečně	k6eAd1	částečně
překrývají	překrývat	k5eAaImIp3nP	překrývat
a	a	k8xC	a
není	být	k5eNaImIp3nS	být
proto	proto	k8xC	proto
možné	možný	k2eAgNnSc1d1	možné
najít	najít	k5eAaPmF	najít
jednoznačnou	jednoznačný	k2eAgFnSc4d1	jednoznačná
hranici	hranice	k1gFnSc4	hranice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
takovémto	takovýto	k3xDgInSc6	takovýto
případě	případ	k1gInSc6	případ
chceme	chtít	k5eAaImIp1nP	chtít
najít	najít	k5eAaPmF	najít
takovou	takový	k3xDgFnSc4	takový
rozdělující	rozdělující	k2eAgFnSc4d1	rozdělující
nadrovinu	nadrovina	k1gFnSc4	nadrovina
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
k	k	k7c3	k
chybné	chybný	k2eAgFnSc3d1	chybná
klasifikaci	klasifikace	k1gFnSc3	klasifikace
docházelo	docházet	k5eAaImAgNnS	docházet
co	co	k9	co
nejméně	málo	k6eAd3	málo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
takovéto	takovýto	k3xDgFnSc6	takovýto
situaci	situace	k1gFnSc6	situace
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
zmírnění	zmírnění	k1gNnSc1	zmírnění
podmínek	podmínka	k1gFnPc2	podmínka
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
a	a	k8xC	a
(	(	kIx(	(
<g/>
3	[number]	k4	3
<g/>
)	)	kIx)	)
s	s	k7c7	s
přidanou	přidaný	k2eAgFnSc7d1	přidaná
cenou	cena	k1gFnSc7	cena
za	za	k7c4	za
jejich	jejich	k3xOp3gNnSc4	jejich
porušení	porušení	k1gNnSc4	porušení
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
zavedením	zavedení	k1gNnSc7	zavedení
přídavné	přídavný	k2eAgFnSc2d1	přídavná
proměnné	proměnná	k1gFnSc2	proměnná
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ξ	ξ	k?	ξ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
xi	xi	k?	xi
}	}	kIx)	}
</s>
</p>
<p>
<s>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
přiřazení	přiřazení	k1gNnSc4	přiřazení
ceny	cena	k1gFnSc2	cena
navíc	navíc	k6eAd1	navíc
za	za	k7c4	za
chyby	chyba	k1gFnPc4	chyba
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
minimalizovaná	minimalizovaný	k2eAgFnSc1d1	minimalizovaná
funkce	funkce	k1gFnSc1	funkce
stanoví	stanovit	k5eAaPmIp3nS	stanovit
místo	místo	k1gNnSc4	místo
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
||	||	k?	||
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
w	w	k?	w
<g/>
}}	}}	k?	}}
<g/>
||	||	k?	||
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
jako	jako	k8xS	jako
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
/	/	kIx~	/
</s>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
<s>
∑	∑	k?	∑
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ξ	ξ	k?	ξ
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
||	||	k?	||
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
w	w	k?	w
<g/>
}}	}}	k?	}}
<g/>
||	||	k?	||
<g/>
^	^	kIx~	^
<g/>
{	{	kIx(	{
<g/>
2	[number]	k4	2
<g/>
}	}	kIx)	}
<g/>
/	/	kIx~	/
<g/>
2	[number]	k4	2
<g/>
+	+	kIx~	+
<g/>
C	C	kA	C
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
sum	suma	k1gFnPc2	suma
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
xi	xi	k?	xi
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
C	C	kA	C
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
volitelný	volitelný	k2eAgInSc1d1	volitelný
parametr	parametr	k1gInSc1	parametr
a	a	k8xC	a
větší	veliký	k2eAgFnSc1d2	veliký
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
C	C	kA	C
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
znamená	znamenat	k5eAaImIp3nS	znamenat
větší	veliký	k2eAgFnSc4d2	veliký
penalizaci	penalizace	k1gFnSc4	penalizace
chyb	chyba	k1gFnPc2	chyba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Duální	duální	k2eAgInSc1d1	duální
problém	problém	k1gInSc1	problém
pak	pak	k6eAd1	pak
je	být	k5eAaImIp3nS	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Maximalizovat	maximalizovat	k5eAaBmF	maximalizovat
</s>
</p>
<p>
<s>
za	za	k7c2	za
podmínek	podmínka	k1gFnPc2	podmínka
</s>
</p>
<p>
<s>
Řešení	řešení	k1gNnSc1	řešení
je	být	k5eAaImIp3nS	být
opět	opět	k6eAd1	opět
</s>
</p>
<p>
<s>
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
N_	N_	k1gFnPc1	N_
<g/>
{	{	kIx(	{
<g/>
S	s	k7c7	s
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
počet	počet	k1gInSc1	počet
podpůrných	podpůrný	k2eAgInPc2d1	podpůrný
vektorů	vektor	k1gInPc2	vektor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jediný	jediný	k2eAgInSc1d1	jediný
rozdíl	rozdíl	k1gInSc1	rozdíl
oproti	oproti	k7c3	oproti
předchozímu	předchozí	k2eAgInSc3d1	předchozí
případu	případ	k1gInSc3	případ
je	být	k5eAaImIp3nS	být
tedy	tedy	k9	tedy
ten	ten	k3xDgMnSc1	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
λ	λ	k?	λ
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
lambda	lambda	k1gNnSc1	lambda
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
nyní	nyní	k6eAd1	nyní
jsou	být	k5eAaImIp3nP	být
shora	shora	k6eAd1	shora
omezené	omezený	k2eAgFnPc1d1	omezená
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
C	C	kA	C
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Primární	primární	k2eAgFnSc1d1	primární
Lagrangeova	Lagrangeův	k2eAgFnSc1d1	Lagrangeova
funkce	funkce	k1gFnSc1	funkce
je	být	k5eAaImIp3nS	být
</s>
</p>
<p>
<s>
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
μ	μ	k?	μ
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
mu	on	k3xPp3gMnSc3	on
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
jsou	být	k5eAaImIp3nP	být
Lagrangeovy	Lagrangeův	k2eAgInPc1d1	Lagrangeův
multiplikátory	multiplikátor	k1gInPc1	multiplikátor
zavedené	zavedený	k2eAgInPc1d1	zavedený
pro	pro	k7c4	pro
zajištění	zajištění	k1gNnSc4	zajištění
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ξ	ξ	k?	ξ
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
≥	≥	k?	≥
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
xi	xi	k?	xi
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
geq	geq	k?	geq
0	[number]	k4	0
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
KKT	KKT	kA	KKT
podmínky	podmínka	k1gFnSc2	podmínka
pro	pro	k7c4	pro
primární	primární	k2eAgInSc4d1	primární
problém	problém	k1gInSc4	problém
pak	pak	k6eAd1	pak
jsou	být	k5eAaImIp3nP	být
</s>
</p>
<p>
<s>
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
i	i	k9	i
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
jde	jít	k5eAaImIp3nS	jít
od	od	k7c2	od
1	[number]	k4	1
do	do	k7c2	do
počtu	počet	k1gInSc2	počet
trénovacích	trénovací	k2eAgInPc2d1	trénovací
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ν	ν	k?	ν
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
nu	nu	k9	nu
}	}	kIx)	}
</s>
</p>
<p>
<s>
jde	jít	k5eAaImIp3nS	jít
od	od	k7c2	od
1	[number]	k4	1
do	do	k7c2	do
dimenze	dimenze	k1gFnSc2	dimenze
dat	datum	k1gNnPc2	datum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Práh	práh	k1gInSc1	práh
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
b	b	k?	b
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
lze	lze	k6eAd1	lze
určit	určit	k5eAaPmF	určit
na	na	k7c6	na
základě	základ	k1gInSc6	základ
podmínek	podmínka	k1gFnPc2	podmínka
(	(	kIx(	(
<g/>
29	[number]	k4	29
<g/>
)	)	kIx)	)
a	a	k8xC	a
(	(	kIx(	(
<g/>
30	[number]	k4	30
<g/>
)	)	kIx)	)
dosazením	dosazení	k1gNnSc7	dosazení
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
trénovacích	trénovací	k2eAgMnPc2d1	trénovací
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Nelineární	lineární	k2eNgMnSc1d1	nelineární
SVM	SVM	kA	SVM
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
lineárním	lineární	k2eAgInSc6d1	lineární
případě	případ	k1gInSc6	případ
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
data	datum	k1gNnPc4	datum
<g/>
,	,	kIx,	,
která	který	k3yIgNnPc1	který
byla	být	k5eAaImAgNnP	být
plně	plně	k6eAd1	plně
nebo	nebo	k8xC	nebo
téměř	téměř	k6eAd1	téměř
lineárně	lineárně	k6eAd1	lineárně
separovatelná	separovatelný	k2eAgFnSc1d1	separovatelná
<g/>
.	.	kIx.	.
</s>
<s>
Obecně	obecně	k6eAd1	obecně
toto	tento	k3xDgNnSc1	tento
však	však	k9	však
často	často	k6eAd1	často
neplatí	platit	k5eNaImIp3nS	platit
a	a	k8xC	a
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
byly	být	k5eAaImAgFnP	být
zavedeny	zavést	k5eAaPmNgFnP	zavést
nelineární	lineární	k2eNgFnSc7d1	nelineární
SVM	SVM	kA	SVM
<g/>
.	.	kIx.	.
</s>
<s>
Základní	základní	k2eAgFnSc7d1	základní
myšlenkou	myšlenka	k1gFnSc7	myšlenka
je	být	k5eAaImIp3nS	být
použití	použití	k1gNnSc1	použití
tzv.	tzv.	kA	tzv.
jádrového	jádrový	k2eAgInSc2d1	jádrový
triku	trik	k1gInSc2	trik
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
kernel	kernel	k1gInSc1	kernel
trick	tricka	k1gFnPc2	tricka
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
s	s	k7c7	s
jehož	jehož	k3xOyRp3gFnSc7	jehož
pomocí	pomoc	k1gFnSc7	pomoc
se	se	k3xPyFc4	se
provádí	provádět	k5eAaImIp3nS	provádět
transformace	transformace	k1gFnSc2	transformace
dat	datum	k1gNnPc2	datum
z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
prostoru	prostor	k1gInSc2	prostor
příznaků	příznak	k1gInPc2	příznak
do	do	k7c2	do
prostoru	prostor	k1gInSc2	prostor
vyšší	vysoký	k2eAgFnSc2d2	vyšší
dimenze	dimenze	k1gFnSc2	dimenze
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yIgInSc6	který
již	již	k6eAd1	již
jsou	být	k5eAaImIp3nP	být
lineárně	lineárně	k6eAd1	lineárně
separabilní	separabilní	k2eAgFnPc1d1	separabilní
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Jinými	jiný	k2eAgNnPc7d1	jiné
slovy	slovo	k1gNnPc7	slovo
<g/>
,	,	kIx,	,
provádíme	provádět	k5eAaImIp1nP	provádět
zobrazení	zobrazení	k1gNnSc4	zobrazení
trénovacích	trénovací	k2eAgNnPc2d1	trénovací
dat	datum	k1gNnPc2	datum
z	z	k7c2	z
původního	původní	k2eAgInSc2d1	původní
prostoru	prostor	k1gInSc2	prostor
do	do	k7c2	do
jiného	jiný	k2eAgInSc2d1	jiný
eukleidovského	eukleidovský	k2eAgInSc2d1	eukleidovský
prostoru	prostor	k1gInSc2	prostor
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathcal	mathcat	k5eAaPmAgInS	mathcat
{	{	kIx(	{
<g/>
H	H	kA	H
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
toto	tento	k3xDgNnSc4	tento
už	už	k6eAd1	už
platit	platit	k5eAaImF	platit
bude	být	k5eAaImBp3nS	být
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Prostor	prostor	k1gInSc1	prostor
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
H	H	kA	H
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathcal	mathcat	k5eAaPmAgInS	mathcat
{	{	kIx(	{
<g/>
H	H	kA	H
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
mnohem	mnohem	k6eAd1	mnohem
větší	veliký	k2eAgFnSc1d2	veliký
<g/>
,	,	kIx,	,
i	i	k8xC	i
až	až	k9	až
nekonečné	konečný	k2eNgFnSc2d1	nekonečná
<g/>
,	,	kIx,	,
dimenze	dimenze	k1gFnSc2	dimenze
než	než	k8xS	než
původní	původní	k2eAgInSc4d1	původní
prostor	prostor	k1gInSc4	prostor
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
značený	značený	k2eAgInSc1d1	značený
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathcal	mathcat	k5eAaPmAgInS	mathcat
{	{	kIx(	{
<g/>
L	L	kA	L
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
,	,	kIx,	,
a	a	k8xC	a
v	v	k7c6	v
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
L	L	kA	L
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
mathcal	mathcat	k5eAaPmAgInS	mathcat
{	{	kIx(	{
<g/>
L	L	kA	L
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
většinou	většina	k1gFnSc7	většina
neexistuje	existovat	k5eNaImIp3nS	existovat
vektor	vektor	k1gInSc1	vektor
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
obrazem	obraz	k1gInSc7	obraz
by	by	kYmCp3nS	by
byl	být	k5eAaImAgInS	být
vektor	vektor	k1gInSc1	vektor
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
w	w	k?	w
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Jelikož	jelikož	k8xS	jelikož
v	v	k7c6	v
rovnicích	rovnice	k1gFnPc6	rovnice
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
)	)	kIx)	)
se	se	k3xPyFc4	se
trénovací	trénovací	k2eAgNnPc1d1	trénovací
data	datum	k1gNnPc1	datum
vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
jen	jen	k9	jen
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
skalárních	skalární	k2eAgInPc2d1	skalární
součinů	součin	k1gInPc2	součin
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
<g/>
,	,	kIx,	,
nemusíme	muset	k5eNaImIp1nP	muset
vůbec	vůbec	k9	vůbec
znát	znát	k5eAaImF	znát
zobrazení	zobrazení	k1gNnSc4	zobrazení
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Φ	Φ	k?	Φ
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Phi	Phi	k1gMnPc5	Phi
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
stačí	stačit	k5eAaBmIp3nS	stačit
pouze	pouze	k6eAd1	pouze
znát	znát	k5eAaImF	znát
vztah	vztah	k1gInSc4	vztah
pro	pro	k7c4	pro
skalární	skalární	k2eAgInSc4d1	skalární
součin	součin	k1gInSc4	součin
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Φ	Φ	k?	Φ
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
Φ	Φ	k?	Φ
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
j	j	k?	j
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
Phi	Phi	k1gFnSc6	Phi
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
Phi	Phi	k1gMnSc1	Phi
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Využívá	využívat	k5eAaImIp3nS	využívat
se	se	k3xPyFc4	se
tzv.	tzv.	kA	tzv.
jádrová	jádrový	k2eAgFnSc1d1	jádrová
funkce	funkce	k1gFnSc1	funkce
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
kernel	kernel	k1gInSc1	kernel
function	function	k1gInSc1	function
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
K	k	k7c3	k
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
j	j	k?	j
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
Φ	Φ	k?	Φ
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
<s>
Φ	Φ	k?	Φ
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
j	j	k?	j
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
K	k	k7c3	k
<g/>
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
=	=	kIx~	=
<g/>
\	\	kIx~	\
<g/>
Phi	Phi	k1gFnSc1	Phi
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k1gMnSc1	cdot
\	\	kIx~	\
<g/>
Phi	Phi	k1gMnSc1	Phi
(	(	kIx(	(
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
j	j	k?	j
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
V	v	k7c6	v
testovací	testovací	k2eAgFnSc6d1	testovací
fázi	fáze	k1gFnSc6	fáze
lze	lze	k6eAd1	lze
použití	použití	k1gNnSc1	použití
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
w	w	k?	w
<g/>
}}}	}}}	k?	}}}
</s>
</p>
<p>
<s>
také	také	k9	také
obejít	obejít	k5eAaPmF	obejít
-	-	kIx~	-
pro	pro	k7c4	pro
určení	určení	k1gNnSc4	určení
<g/>
,	,	kIx,	,
do	do	k7c2	do
které	který	k3yIgFnSc2	který
třídy	třída	k1gFnSc2	třída
bod	bod	k1gInSc1	bod
spadá	spadat	k5eAaImIp3nS	spadat
<g/>
,	,	kIx,	,
počítáme	počítat	k5eAaImIp1nP	počítat
funkci	funkce	k1gFnSc4	funkce
signum	signum	k1gNnSc1	signum
z	z	k7c2	z
výrazu	výraz	k1gInSc2	výraz
</s>
</p>
<p>
<s>
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
s	s	k7c7	s
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
s	s	k7c7	s
<g/>
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
jsou	být	k5eAaImIp3nP	být
podpůrné	podpůrný	k2eAgInPc4d1	podpůrný
vektory	vektor	k1gInPc4	vektor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Příklady	příklad	k1gInPc1	příklad
nelineárních	lineární	k2eNgInPc2d1	nelineární
SVM	SVM	kA	SVM
===	===	k?	===
</s>
</p>
<p>
<s>
Mezi	mezi	k7c4	mezi
často	často	k6eAd1	často
používané	používaný	k2eAgFnPc4d1	používaná
jádrové	jádrový	k2eAgFnPc4d1	jádrová
funkce	funkce	k1gFnPc4	funkce
patří	patřit	k5eAaImIp3nS	patřit
například	například	k6eAd1	například
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Polynom	polynom	k1gInSc1	polynom
stupně	stupeň	k1gInSc2	stupeň
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
p	p	k?	p
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
p	p	k?	p
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
:	:	kIx,	:
<g/>
Radiální	radiální	k2eAgFnPc1d1	radiální
bázové	bázový	k2eAgFnPc1d1	bázová
funkce	funkce	k1gFnPc1	funkce
<g/>
:	:	kIx,	:
<g/>
Dvouvrstvá	Dvouvrstvý	k2eAgFnSc1d1	Dvouvrstvá
neuronová	neuronový	k2eAgFnSc1d1	neuronová
síť	síť	k1gFnSc1	síť
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
==	==	k?	==
Metody	metoda	k1gFnPc1	metoda
řešení	řešení	k1gNnSc2	řešení
==	==	k?	==
</s>
</p>
<p>
<s>
Problém	problém	k1gInSc1	problém
optimalizace	optimalizace	k1gFnSc2	optimalizace
pomocí	pomocí	k7c2	pomocí
podpůrných	podpůrný	k2eAgInPc2d1	podpůrný
vektorů	vektor	k1gInPc2	vektor
lze	lze	k6eAd1	lze
řešit	řešit	k5eAaImF	řešit
analyticky	analyticky	k6eAd1	analyticky
jen	jen	k9	jen
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
množství	množství	k1gNnSc1	množství
trénovacích	trénovací	k2eAgNnPc2d1	trénovací
dat	datum	k1gNnPc2	datum
velmi	velmi	k6eAd1	velmi
malé	malý	k2eAgInPc1d1	malý
nebo	nebo	k8xC	nebo
v	v	k7c6	v
separovatelném	separovatelný	k2eAgInSc6d1	separovatelný
případě	případ	k1gInSc6	případ
pokud	pokud	k8xS	pokud
je	být	k5eAaImIp3nS	být
předem	předem	k6eAd1	předem
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
která	který	k3yQgNnPc1	který
data	datum	k1gNnPc1	datum
se	se	k3xPyFc4	se
stanou	stanout	k5eAaPmIp3nP	stanout
podpůrnými	podpůrný	k2eAgInPc7d1	podpůrný
vektory	vektor	k1gInPc7	vektor
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
většině	většina	k1gFnSc6	většina
reálných	reálný	k2eAgNnPc2d1	reálné
použití	použití	k1gNnPc2	použití
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
rovnice	rovnice	k1gFnSc1	rovnice
(	(	kIx(	(
<g/>
17	[number]	k4	17
<g/>
-	-	kIx~	-
<g/>
19	[number]	k4	19
<g/>
)	)	kIx)	)
řešit	řešit	k5eAaImF	řešit
numericky	numericky	k6eAd1	numericky
(	(	kIx(	(
<g/>
se	s	k7c7	s
skalárními	skalární	k2eAgInPc7d1	skalární
součiny	součin	k1gInPc7	součin
nahrazenými	nahrazený	k2eAgInPc7d1	nahrazený
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
K	k	k7c3	k
</s>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
K	k	k7c3	k
<g/>
(	(	kIx(	(
<g/>
x	x	k?	x
<g/>
,	,	kIx,	,
<g/>
y	y	k?	y
<g/>
)	)	kIx)	)
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
jednodušší	jednoduchý	k2eAgFnPc4d2	jednodušší
úlohy	úloha	k1gFnPc4	úloha
postačují	postačovat	k5eAaImIp3nP	postačovat
standardní	standardní	k2eAgFnPc4d1	standardní
metody	metoda	k1gFnPc4	metoda
řešení	řešení	k1gNnSc2	řešení
konvexních	konvexní	k2eAgFnPc2d1	konvexní
úloh	úloha	k1gFnPc2	úloha
kvadratického	kvadratický	k2eAgNnSc2d1	kvadratické
programování	programování	k1gNnSc2	programování
s	s	k7c7	s
lineárními	lineární	k2eAgFnPc7d1	lineární
vazbovými	vazbový	k2eAgFnPc7d1	vazbová
podmínkami	podmínka	k1gFnPc7	podmínka
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
složitější	složitý	k2eAgFnPc4d2	složitější
úlohy	úloha	k1gFnPc4	úloha
pak	pak	k6eAd1	pak
existují	existovat	k5eAaImIp3nP	existovat
speciální	speciální	k2eAgFnPc4d1	speciální
techniky	technika	k1gFnPc4	technika
řešení	řešení	k1gNnSc2	řešení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Základní	základní	k2eAgInSc1d1	základní
postup	postup	k1gInSc1	postup
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Volba	volba	k1gFnSc1	volba
jádrové	jádrový	k2eAgFnSc2d1	jádrová
funkce	funkce	k1gFnSc2	funkce
a	a	k8xC	a
jejích	její	k3xOp3gInPc2	její
parametrů	parametr	k1gInPc2	parametr
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Volba	volba	k1gFnSc1	volba
parametru	parametr	k1gInSc2	parametr
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
C	C	kA	C
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
C	C	kA	C
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
reprezentujícího	reprezentující	k2eAgInSc2d1	reprezentující
kompromis	kompromis	k1gInSc4	kompromis
mezi	mezi	k7c7	mezi
minimalizací	minimalizace	k1gFnSc7	minimalizace
chyby	chyba	k1gFnSc2	chyba
na	na	k7c6	na
trénovací	trénovací	k2eAgFnSc6d1	trénovací
množině	množina	k1gFnSc6	množina
a	a	k8xC	a
maximalizací	maximalizace	k1gFnSc7	maximalizace
šířky	šířka	k1gFnSc2	šířka
hraničního	hraniční	k2eAgNnSc2d1	hraniční
pásma	pásmo	k1gNnSc2	pásmo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vyřešení	vyřešení	k1gNnSc1	vyřešení
duálního	duální	k2eAgInSc2d1	duální
problému	problém	k1gInSc2	problém
pomocí	pomocí	k7c2	pomocí
vhodné	vhodný	k2eAgFnSc2d1	vhodná
techniky	technika	k1gFnSc2	technika
kvadratického	kvadratický	k2eAgNnSc2d1	kvadratické
programování	programování	k1gNnSc2	programování
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Určení	určení	k1gNnSc1	určení
podmínek	podmínka	k1gFnPc2	podmínka
optimality	optimalita	k1gFnSc2	optimalita
(	(	kIx(	(
<g/>
KKT	KKT	kA	KKT
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
musí	muset	k5eAaImIp3nP	muset
řešení	řešení	k1gNnSc4	řešení
splňovat	splňovat	k5eAaImF	splňovat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Definování	definování	k1gNnSc1	definování
strategie	strategie	k1gFnSc2	strategie
dosažení	dosažení	k1gNnSc2	dosažení
optimality	optimalita	k1gFnSc2	optimalita
zvyšováním	zvyšování	k1gNnSc7	zvyšování
duální	duální	k2eAgFnSc2d1	duální
váhové	váhový	k2eAgFnSc2d1	váhová
funkce	funkce	k1gFnSc2	funkce
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
vazbovým	vazbový	k2eAgFnPc3d1	vazbová
podmínkám	podmínka	k1gFnPc3	podmínka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
rozsáhlejších	rozsáhlý	k2eAgFnPc2d2	rozsáhlejší
úloh	úloha	k1gFnPc2	úloha
<g/>
:	:	kIx,	:
volba	volba	k1gFnSc1	volba
algoritmu	algoritmus	k1gInSc2	algoritmus
dekompozice	dekompozice	k1gFnSc2	dekompozice
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
pracovat	pracovat	k5eAaImF	pracovat
jen	jen	k9	jen
s	s	k7c7	s
částí	část	k1gFnSc7	část
dat	datum	k1gNnPc2	datum
najednou	najednou	k6eAd1	najednou
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výpočet	výpočet	k1gInSc1	výpočet
parametru	parametr	k1gInSc2	parametr
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
b	b	k?	b
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
na	na	k7c6	na
základě	základ	k1gInSc6	základ
podpůrných	podpůrný	k2eAgInPc2d1	podpůrný
vektorů	vektor	k1gInPc2	vektor
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Klasifikace	klasifikace	k1gFnSc1	klasifikace
nových	nový	k2eAgInPc2d1	nový
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
<g/>
Algoritmy	algoritmus	k1gInPc1	algoritmus
dekompozice	dekompozice	k1gFnSc2	dekompozice
pro	pro	k7c4	pro
rozměrné	rozměrný	k2eAgFnPc4d1	rozměrná
úlohy	úloha	k1gFnPc4	úloha
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
"	"	kIx"	"
<g/>
hroudová	hroudový	k2eAgFnSc1d1	hroudový
<g/>
"	"	kIx"	"
metoda	metoda	k1gFnSc1	metoda
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
"	"	kIx"	"
<g/>
chunking	chunking	k1gInSc1	chunking
<g/>
"	"	kIx"	"
method	method	k1gInSc1	method
<g/>
)	)	kIx)	)
-	-	kIx~	-
SVM	SVM	kA	SVM
se	se	k3xPyFc4	se
nejdříve	dříve	k6eAd3	dříve
natrénuje	natrénovat	k5eAaPmIp3nS	natrénovat
na	na	k7c6	na
malé	malý	k2eAgFnSc6d1	malá
podmnožině	podmnožina	k1gFnSc6	podmnožina
dat	datum	k1gNnPc2	datum
<g/>
,	,	kIx,	,
na	na	k7c6	na
zbytku	zbytek	k1gInSc6	zbytek
se	se	k3xPyFc4	se
otestuje	otestovat	k5eAaPmIp3nS	otestovat
a	a	k8xC	a
udělá	udělat	k5eAaPmIp3nS	udělat
se	se	k3xPyFc4	se
seznam	seznam	k1gInSc1	seznam
chybných	chybný	k2eAgNnPc2d1	chybné
rozhodnutí	rozhodnutí	k1gNnPc2	rozhodnutí
seřazený	seřazený	k2eAgInSc1d1	seřazený
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
moc	moc	k6eAd1	moc
byly	být	k5eAaImAgInP	být
porušeny	porušit	k5eAaPmNgInP	porušit
KKT	KKT	kA	KKT
podmínky	podmínka	k1gFnPc4	podmínka
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
trénovací	trénovací	k2eAgFnSc1d1	trénovací
množina	množina	k1gFnSc1	množina
se	se	k3xPyFc4	se
vytvoří	vytvořit	k5eAaPmIp3nS	vytvořit
z	z	k7c2	z
prvních	první	k4xOgInPc2	první
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
N	N	kA	N
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
bodů	bod	k1gInPc2	bod
ze	z	k7c2	z
seznamu	seznam	k1gInSc2	seznam
a	a	k8xC	a
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
N_	N_	k1gFnPc1	N_
<g/>
{	{	kIx(	{
<g/>
S	s	k7c7	s
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
už	už	k6eAd1	už
nalezených	nalezený	k2eAgInPc2d1	nalezený
podpůrných	podpůrný	k2eAgInPc2d1	podpůrný
vektorů	vektor	k1gInPc2	vektor
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
N	N	kA	N
<g/>
+	+	kIx~	+
<g/>
N_	N_	k1gMnSc1	N_
<g/>
{	{	kIx(	{
<g/>
S	s	k7c7	s
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
určeno	určit	k5eAaPmNgNnS	určit
heuristicky	heuristicky	k6eAd1	heuristicky
<g/>
.	.	kIx.	.
</s>
<s>
Proces	proces	k1gInSc1	proces
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
dokud	dokud	k6eAd1	dokud
pro	pro	k7c4	pro
všechny	všechen	k3xTgInPc4	všechen
body	bod	k1gInPc4	bod
není	být	k5eNaImIp3nS	být
rozhodnuto	rozhodnout	k5eAaPmNgNnS	rozhodnout
správně	správně	k6eAd1	správně
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Tato	tento	k3xDgFnSc1	tento
metoda	metoda	k1gFnSc1	metoda
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
počet	počet	k1gInSc1	počet
podpůrných	podpůrný	k2eAgInPc2d1	podpůrný
vektorů	vektor	k1gInPc2	vektor
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
N_	N_	k1gFnPc1	N_
<g/>
{	{	kIx(	{
<g/>
S	s	k7c7	s
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
byl	být	k5eAaImAgInS	být
dostatečně	dostatečně	k6eAd1	dostatečně
malý	malý	k2eAgInSc1d1	malý
na	na	k7c4	na
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
Hessova	Hessův	k2eAgFnSc1d1	Hessova
matice	matice	k1gFnSc1	matice
o	o	k7c6	o
rozměru	rozměr	k1gInSc6	rozměr
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
×	×	k?	×
</s>
</p>
<p>
</p>
<p>
<s>
N	N	kA	N
</s>
</p>
<p>
</p>
<p>
<s>
S	s	k7c7	s
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
N_	N_	k1gFnPc1	N_
<g/>
{	{	kIx(	{
<g/>
S	s	k7c7	s
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
times	times	k1gMnSc1	times
N_	N_	k1gMnSc1	N_
<g/>
{	{	kIx(	{
<g/>
S	s	k7c7	s
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
vešla	vejít	k5eAaPmAgFnS	vejít
do	do	k7c2	do
paměti	paměť	k1gFnSc2	paměť
počítače	počítač	k1gInSc2	počítač
<g/>
.	.	kIx.	.
</s>
<s>
Pro	pro	k7c4	pro
ještě	ještě	k6eAd1	ještě
rozměrnější	rozměrný	k2eAgFnPc4d2	rozměrnější
úlohy	úloha	k1gFnPc4	úloha
existují	existovat	k5eAaImIp3nP	existovat
jiné	jiný	k2eAgInPc1d1	jiný
algoritmy	algoritmus	k1gInPc1	algoritmus
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
SVM	SVM	kA	SVM
regrese	regrese	k1gFnSc1	regrese
==	==	k?	==
</s>
</p>
<p>
<s>
Kromě	kromě	k7c2	kromě
klasifikace	klasifikace	k1gFnSc2	klasifikace
se	se	k3xPyFc4	se
SVM	SVM	kA	SVM
využívají	využívat	k5eAaImIp3nP	využívat
také	také	k9	také
pro	pro	k7c4	pro
regresní	regresní	k2eAgFnSc4d1	regresní
analýzu	analýza	k1gFnSc4	analýza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
SVM	SVM	kA	SVM
regrese	regrese	k1gFnSc1	regrese
využívá	využívat	k5eAaPmIp3nS	využívat
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ε	ε	k?	ε
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc4	varepsilon
}	}	kIx)	}
</s>
</p>
<p>
<s>
-necitlivou	ecitlivý	k2eAgFnSc4d1	-necitlivý
ztrátovou	ztrátový	k2eAgFnSc4d1	ztrátová
funkci	funkce	k1gFnSc4	funkce
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ε	ε	k?	ε
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc4	varepsilon
}	}	kIx)	}
</s>
</p>
<p>
<s>
-insensitive	nsensitiv	k1gInSc5	-insensitiv
loss	lossit	k5eAaPmRp2nS	lossit
function	function	k1gInSc1	function
<g/>
)	)	kIx)	)
-	-	kIx~	-
pokud	pokud	k8xS	pokud
rozdíl	rozdíl	k1gInSc4	rozdíl
mezi	mezi	k7c7	mezi
očekávanou	očekávaný	k2eAgFnSc7d1	očekávaná
a	a	k8xC	a
skutečnou	skutečný	k2eAgFnSc7d1	skutečná
hodnotou	hodnota	k1gFnSc7	hodnota
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
než	než	k8xS	než
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ε	ε	k?	ε
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc4	varepsilon
}	}	kIx)	}
</s>
</p>
<p>
<s>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
to	ten	k3xDgNnSc1	ten
považováno	považován	k2eAgNnSc1d1	považováno
za	za	k7c4	za
chybu	chyba	k1gFnSc4	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Tedy	tedy	k9	tedy
chceme	chtít	k5eAaImIp1nP	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
platilo	platit	k5eAaImAgNnS	platit
</s>
</p>
<p>
<s>
Opět	opět	k6eAd1	opět
se	se	k3xPyFc4	se
minimalizuje	minimalizovat	k5eAaBmIp3nS	minimalizovat
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
|	|	kIx~	|
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
||	||	k?	||
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
w	w	k?	w
<g/>
}}	}}	k?	}}
<g/>
||	||	k?	||
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
zavedené	zavedený	k2eAgFnPc4d1	zavedená
přídavné	přídavný	k2eAgFnPc4d1	přídavná
proměnné	proměnná	k1gFnPc4	proměnná
(	(	kIx(	(
<g/>
angl.	angl.	k?	angl.
slack	slack	k1gMnSc1	slack
variables	variables	k1gMnSc1	variables
<g/>
)	)	kIx)	)
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ξ	ξ	k?	ξ
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
xi	xi	k?	xi
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
a	a	k8xC	a
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ξ	ξ	k?	ξ
</s>
</p>
<p>
<s>
^	^	kIx~	^
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
hat	hat	k0	hat
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
xi	xi	k?	xi
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}	}}	k?	}}
</s>
</p>
<p>
<s>
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
lineární	lineární	k2eAgInSc4d1	lineární
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ε	ε	k?	ε
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc4	varepsilon
}	}	kIx)	}
</s>
</p>
<p>
<s>
-necitlivou	ecitlivý	k2eAgFnSc4d1	-necitlivý
funkci	funkce	k1gFnSc4	funkce
je	být	k5eAaImIp3nS	být
pak	pak	k6eAd1	pak
úkolem	úkol	k1gInSc7	úkol
minimalizovat	minimalizovat	k5eAaBmF	minimalizovat
</s>
</p>
<p>
<s>
za	za	k7c2	za
podmínek	podmínka	k1gFnPc2	podmínka
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
+	+	kIx~	+
</s>
</p>
<p>
</p>
<p>
<s>
ξ	ξ	k?	ξ
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
≥	≥	k?	≥
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
ε	ε	k?	ε
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
w	w	k?	w
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}}	}}}	k?	}}}
<g/>
-b-y_	_	k?	-b-y_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
+	+	kIx~	+
<g/>
\	\	kIx~	\
<g/>
xi	xi	k?	xi
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
geq	geq	k?	geq
-	-	kIx~	-
<g/>
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
(	(	kIx(	(
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
w	w	k?	w
</s>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
⋅	⋅	k?	⋅
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
x	x	k?	x
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
→	→	k?	→
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
<s>
b	b	k?	b
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
<s>
y	y	k?	y
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
)	)	kIx)	)
</s>
</p>
<p>
<s>
−	−	k?	−
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ξ	ξ	k?	ξ
</s>
</p>
<p>
<s>
^	^	kIx~	^
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
≤	≤	k?	≤
</s>
</p>
<p>
<s>
ε	ε	k?	ε
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
(	(	kIx(	(
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
w	w	k?	w
<g/>
}}	}}	k?	}}
<g/>
\	\	kIx~	\
<g/>
cdot	cdot	k5eAaPmF	cdot
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
vec	vec	k?	vec
{	{	kIx(	{
<g/>
x_	x_	k?	x_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}}}	}}}	k?	}}}
<g/>
-b-y_	_	k?	-b-y_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
)	)	kIx)	)
<g/>
-	-	kIx~	-
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
hat	hat	k0	hat
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
xi	xi	k?	xi
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
leq	leq	k?	leq
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc1	varepsilon
}	}	kIx)	}
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ξ	ξ	k?	ξ
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ξ	ξ	k?	ξ
</s>
</p>
<p>
<s>
^	^	kIx~	^
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
≥	≥	k?	≥
</s>
</p>
<p>
<s>
0	[number]	k4	0
</s>
</p>
<p>
</p>
<p>
<s>
i	i	k9	i
</s>
</p>
<p>
<s>
=	=	kIx~	=
</s>
</p>
<p>
<s>
1	[number]	k4	1
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
...	...	k?	...
</s>
</p>
<p>
<s>
,	,	kIx,	,
</s>
</p>
<p>
<s>
m	m	kA	m
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
xi	xi	k?	xi
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
,	,	kIx,	,
<g/>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
hat	hat	k0	hat
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
xi	xi	k?	xi
}}	}}	k?	}}
<g/>
_	_	kIx~	_
<g/>
{	{	kIx(	{
<g/>
i	i	k9	i
<g/>
}	}	kIx)	}
<g/>
\	\	kIx~	\
<g/>
geq	geq	k?	geq
0	[number]	k4	0
<g/>
\	\	kIx~	\
<g/>
quad	quad	k6eAd1	quad
i	i	k9	i
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
<g/>
\	\	kIx~	\
<g/>
dots	dots	k1gInSc1	dots
,	,	kIx,	,
<g/>
m	m	kA	m
<g/>
}	}	kIx)	}
</s>
</p>
<p>
<s>
Pro	pro	k7c4	pro
nelineární	lineární	k2eNgFnPc4d1	nelineární
regresní	regresní	k2eAgFnPc4d1	regresní
funkce	funkce	k1gFnPc4	funkce
se	se	k3xPyFc4	se
používá	používat	k5eAaImIp3nS	používat
podobný	podobný	k2eAgInSc1d1	podobný
postup	postup	k1gInSc1	postup
jako	jako	k8xC	jako
u	u	k7c2	u
klasifikace	klasifikace	k1gFnSc2	klasifikace
-	-	kIx~	-
duální	duální	k2eAgFnSc1d1	duální
Lagrangeova	Lagrangeův	k2eAgFnSc1d1	Lagrangeova
funkce	funkce	k1gFnSc1	funkce
<g/>
,	,	kIx,	,
jádrová	jádrový	k2eAgFnSc1d1	jádrová
transformace	transformace	k1gFnSc1	transformace
<g/>
,	,	kIx,	,
atd.	atd.	kA	atd.
</s>
</p>
<p>
<s>
Namísto	namísto	k7c2	namísto
pevně	pevně	k6eAd1	pevně
daného	daný	k2eAgInSc2d1	daný
parametru	parametr	k1gInSc2	parametr
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
ε	ε	k?	ε
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
{	{	kIx(	{
<g/>
\	\	kIx~	\
<g/>
displaystyle	displaystyl	k1gInSc5	displaystyl
\	\	kIx~	\
<g/>
varepsilon	varepsilon	k1gInSc4	varepsilon
}	}	kIx)	}
</s>
</p>
<p>
<s>
je	být	k5eAaImIp3nS	být
také	také	k9	také
možné	možný	k2eAgNnSc1d1	možné
stanovit	stanovit	k5eAaPmF	stanovit
maximální	maximální	k2eAgInSc4d1	maximální
podíl	podíl	k1gInSc4	podíl
bodů	bod	k1gInPc2	bod
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
které	který	k3yRgInPc4	který
nebude	být	k5eNaImBp3nS	být
platit	platit	k5eAaImF	platit
vztah	vztah	k1gInSc4	vztah
(	(	kIx(	(
<g/>
36	[number]	k4	36
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Uplatnění	uplatnění	k1gNnSc1	uplatnění
SVM	SVM	kA	SVM
==	==	k?	==
</s>
</p>
<p>
<s>
Support	support	k1gInSc1	support
Vector	Vector	k1gMnSc1	Vector
Machines	Machines	k1gMnSc1	Machines
mají	mít	k5eAaImIp3nP	mít
široké	široký	k2eAgNnSc4d1	široké
uplatnění	uplatnění	k1gNnSc4	uplatnění
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
pro	pro	k7c4	pro
klasifikaci	klasifikace	k1gFnSc4	klasifikace
<g/>
.	.	kIx.	.
</s>
<s>
Reálné	reálný	k2eAgFnPc1d1	reálná
aplikace	aplikace	k1gFnPc1	aplikace
zahrnují	zahrnovat	k5eAaImIp3nP	zahrnovat
například	například	k6eAd1	například
oblasti	oblast	k1gFnPc4	oblast
zpracování	zpracování	k1gNnSc2	zpracování
textu	text	k1gInSc2	text
<g/>
,	,	kIx,	,
počítačového	počítačový	k2eAgNnSc2d1	počítačové
vidění	vidění	k1gNnSc2	vidění
(	(	kIx(	(
<g/>
např.	např.	kA	např.
rozpoznávání	rozpoznávání	k1gNnSc1	rozpoznávání
obličejů	obličej	k1gInPc2	obličej
<g/>
,	,	kIx,	,
rozpoznávání	rozpoznávání	k1gNnPc2	rozpoznávání
ručně	ručně	k6eAd1	ručně
psaného	psaný	k2eAgInSc2d1	psaný
textu	text	k1gInSc2	text
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
bioinformatiky	bioinformatika	k1gFnSc2	bioinformatika
(	(	kIx(	(
<g/>
např.	např.	kA	např.
klasifikace	klasifikace	k1gFnSc1	klasifikace
proteinů	protein	k1gInPc2	protein
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Oproti	oproti	k7c3	oproti
některým	některý	k3yIgInPc3	některý
srovnatelným	srovnatelný	k2eAgInPc3d1	srovnatelný
typům	typ	k1gInPc3	typ
klasifikátorů	klasifikátor	k1gInPc2	klasifikátor
mají	mít	k5eAaImIp3nP	mít
SVM	SVM	kA	SVM
několik	několik	k4yIc4	několik
výhod	výhoda	k1gFnPc2	výhoda
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Trénování	trénování	k1gNnSc1	trénování
vždy	vždy	k6eAd1	vždy
najde	najít	k5eAaPmIp3nS	najít
globální	globální	k2eAgNnSc4d1	globální
řešení	řešení	k1gNnSc4	řešení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
například	například	k6eAd1	například
neuronových	neuronový	k2eAgFnPc2d1	neuronová
sítí	síť	k1gFnPc2	síť
u	u	k7c2	u
SVM	SVM	kA	SVM
neexistují	existovat	k5eNaImIp3nP	existovat
žádná	žádný	k3yNgNnPc1	žádný
lokální	lokální	k2eAgNnPc1d1	lokální
minima	minimum	k1gNnPc1	minimum
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
by	by	kYmCp3nS	by
nebyla	být	k5eNaImAgFnS	být
zároveň	zároveň	k6eAd1	zároveň
globálním	globální	k2eAgNnSc7d1	globální
minimem	minimum	k1gNnSc7	minimum
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Reprodukovatelné	reprodukovatelný	k2eAgInPc1d1	reprodukovatelný
výsledky	výsledek	k1gInPc1	výsledek
<g/>
,	,	kIx,	,
nezávislé	závislý	k2eNgInPc1d1	nezávislý
na	na	k7c6	na
volbě	volba	k1gFnSc6	volba
konkrétního	konkrétní	k2eAgInSc2d1	konkrétní
algoritmu	algoritmus	k1gInSc2	algoritmus
nebo	nebo	k8xC	nebo
např.	např.	kA	např.
počátečního	počáteční	k2eAgInSc2d1	počáteční
bodu	bod	k1gInSc2	bod
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Malý	malý	k2eAgInSc1d1	malý
počet	počet	k1gInSc1	počet
parametrů	parametr	k1gInPc2	parametr
modelu	model	k1gInSc2	model
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
je	být	k5eAaImIp3nS	být
třeba	třeba	k6eAd1	třeba
určit	určit	k5eAaPmF	určit
po	po	k7c6	po
výběru	výběr	k1gInSc6	výběr
typu	typ	k1gInSc2	typ
jádrové	jádrový	k2eAgFnSc2d1	jádrová
funkce	funkce	k1gFnSc2	funkce
(	(	kIx(	(
<g/>
může	moct	k5eAaImIp3nS	moct
být	být	k5eAaImF	být
zároveň	zároveň	k6eAd1	zároveň
i	i	k9	i
nevýhodou	nevýhoda	k1gFnSc7	nevýhoda
-	-	kIx~	-
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Jsou	být	k5eAaImIp3nP	být
tu	tu	k6eAd1	tu
však	však	k9	však
i	i	k9	i
jisté	jistý	k2eAgInPc4d1	jistý
problémy	problém	k1gInPc4	problém
a	a	k8xC	a
omezení	omezení	k1gNnSc4	omezení
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
Správná	správný	k2eAgFnSc1d1	správná
funkce	funkce	k1gFnSc1	funkce
SVM	SVM	kA	SVM
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
záleží	záležet	k5eAaImIp3nS	záležet
na	na	k7c6	na
vhodné	vhodný	k2eAgFnSc6d1	vhodná
volbě	volba	k1gFnSc6	volba
jádrové	jádrový	k2eAgFnSc2d1	jádrová
funkce	funkce	k1gFnSc2	funkce
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
ji	on	k3xPp3gFnSc4	on
stanovíme	stanovit	k5eAaPmIp1nP	stanovit
<g/>
,	,	kIx,	,
máme	mít	k5eAaImIp1nP	mít
jen	jen	k6eAd1	jen
jeden	jeden	k4xCgInSc4	jeden
parametr	parametr	k1gInSc4	parametr
-	-	kIx~	-
penaltu	penalta	k1gFnSc4	penalta
chyb	chyba	k1gFnPc2	chyba
<g/>
.	.	kIx.	.
</s>
<s>
Správnou	správný	k2eAgFnSc4d1	správná
jádrovou	jádrový	k2eAgFnSc4d1	jádrová
funkci	funkce	k1gFnSc4	funkce
ale	ale	k8xC	ale
nemusí	muset	k5eNaImIp3nP	muset
být	být	k5eAaImF	být
snadné	snadný	k2eAgFnPc1d1	snadná
najít	najít	k5eAaPmF	najít
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
U	u	k7c2	u
velmi	velmi	k6eAd1	velmi
rozsáhlých	rozsáhlý	k2eAgFnPc2d1	rozsáhlá
úloh	úloha	k1gFnPc2	úloha
(	(	kIx(	(
<g/>
miliony	milion	k4xCgInPc4	milion
podpůrných	podpůrný	k2eAgInPc2d1	podpůrný
vektorů	vektor	k1gInPc2	vektor
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
problém	problém	k1gInSc1	problém
s	s	k7c7	s
rychlostí	rychlost	k1gFnSc7	rychlost
a	a	k8xC	a
paměťovými	paměťový	k2eAgInPc7d1	paměťový
nároky	nárok	k1gInPc7	nárok
<g/>
.	.	kIx.	.
</s>
<s>
Vhodné	vhodný	k2eAgInPc1d1	vhodný
algoritmy	algoritmus	k1gInPc1	algoritmus
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gNnSc4	jejich
řešení	řešení	k1gNnSc4	řešení
jsou	být	k5eAaImIp3nP	být
stále	stále	k6eAd1	stále
ještě	ještě	k6eAd1	ještě
předmětem	předmět	k1gInSc7	předmět
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
.	.	kIx.	.
<g/>
Myšlenky	myšlenka	k1gFnPc1	myšlenka
vycházející	vycházející	k2eAgFnSc1d1	vycházející
ze	z	k7c2	z
SVM	SVM	kA	SVM
(	(	kIx(	(
<g/>
málo	málo	k4c4	málo
podpůrných	podpůrný	k2eAgInPc2d1	podpůrný
vektorů	vektor	k1gInPc2	vektor
<g/>
,	,	kIx,	,
maximální	maximální	k2eAgInSc4d1	maximální
odstup	odstup	k1gInSc4	odstup
a	a	k8xC	a
jádrové	jádrový	k2eAgFnSc2d1	jádrová
transformace	transformace	k1gFnSc2	transformace
<g/>
)	)	kIx)	)
byly	být	k5eAaImAgFnP	být
také	také	k9	také
použity	použit	k2eAgFnPc1d1	použita
i	i	k9	i
pro	pro	k7c4	pro
návrh	návrh	k1gInSc4	návrh
algoritmů	algoritmus	k1gInPc2	algoritmus
pro	pro	k7c4	pro
další	další	k2eAgFnPc4d1	další
úlohy	úloha	k1gFnPc4	úloha
strojového	strojový	k2eAgNnSc2d1	strojové
učení	učení	k1gNnSc2	učení
<g/>
,	,	kIx,	,
např.	např.	kA	např.
úlohy	úloha	k1gFnSc2	úloha
binární	binární	k2eAgFnSc2d1	binární
klasifikace	klasifikace	k1gFnSc2	klasifikace
na	na	k7c6	na
<g />
.	.	kIx.	.
</s>
<s>
zašuměných	zašuměný	k2eAgNnPc6d1	zašuměné
datech	datum	k1gNnPc6	datum
(	(	kIx(	(
<g/>
tzv.	tzv.	kA	tzv.
soft	soft	k?	soft
margin	margin	k1gInSc1	margin
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
diskrétní	diskrétní	k2eAgFnSc1d1	diskrétní
klasifikace	klasifikace	k1gFnSc1	klasifikace
(	(	kIx(	(
<g/>
do	do	k7c2	do
více	hodně	k6eAd2	hodně
tříd	třída	k1gFnPc2	třída
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
regrese	regrese	k1gFnSc1	regrese
<g/>
,	,	kIx,	,
jádrová	jádrový	k2eAgFnSc1d1	jádrová
analýza	analýza	k1gFnSc1	analýza
hlavních	hlavní	k2eAgFnPc2d1	hlavní
komponent	komponenta	k1gFnPc2	komponenta
(	(	kIx(	(
<g/>
PCA	PCA	kA	PCA
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
ranking	ranking	k1gInSc1	ranking
<g/>
,	,	kIx,	,
strukturované	strukturovaný	k2eAgNnSc1d1	strukturované
učení	učení	k1gNnSc1	učení
<g/>
,	,	kIx,	,
učení	učení	k1gNnSc1	učení
z	z	k7c2	z
jedné	jeden	k4xCgFnSc2	jeden
třídy	třída	k1gFnSc2	třída
(	(	kIx(	(
<g/>
one	one	k?	one
class	class	k1gInSc1	class
support	support	k1gInSc1	support
vector	vector	k1gInSc4	vector
<g/>
,	,	kIx,	,
single	singl	k1gInSc5	singl
class	class	k1gInSc4	class
data	datum	k1gNnSc2	datum
description	description	k1gInSc1	description
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Literatura	literatura	k1gFnSc1	literatura
==	==	k?	==
</s>
</p>
<p>
<s>
Bernhard	Bernhard	k1gMnSc1	Bernhard
Schölkopf	Schölkopf	k1gMnSc1	Schölkopf
<g/>
,	,	kIx,	,
Alex	Alex	k1gMnSc1	Alex
Smola	Smola	k1gMnSc1	Smola
<g/>
:	:	kIx,	:
Learning	Learning	k1gInSc1	Learning
with	with	k1gMnSc1	with
Kernels	Kernels	k1gInSc1	Kernels
<g/>
:	:	kIx,	:
Support	support	k1gInSc1	support
Vector	Vector	k1gMnSc1	Vector
Machines	Machines	k1gMnSc1	Machines
<g/>
,	,	kIx,	,
Regularization	Regularization	k1gInSc1	Regularization
<g/>
,	,	kIx,	,
Optimization	Optimization	k1gInSc1	Optimization
<g/>
,	,	kIx,	,
and	and	k?	and
Beyond	Beyond	k1gInSc1	Beyond
(	(	kIx(	(
<g/>
Adaptive	Adaptiv	k1gInSc5	Adaptiv
Computation	Computation	k1gInSc1	Computation
and	and	k?	and
Machine	Machin	k1gInSc5	Machin
Learning	Learning	k1gInSc1	Learning
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
MIT	MIT	kA	MIT
Press	Press	k1gInSc1	Press
<g/>
,	,	kIx,	,
Cambridge	Cambridge	k1gFnSc1	Cambridge
<g/>
,	,	kIx,	,
MA	MA	kA	MA
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
262	[number]	k4	262
<g/>
-	-	kIx~	-
<g/>
19475	[number]	k4	19475
<g/>
-	-	kIx~	-
<g/>
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Ingo	Ingo	k6eAd1	Ingo
Steinwart	Steinwart	k1gInSc1	Steinwart
<g/>
,	,	kIx,	,
Andreas	Andreas	k1gMnSc1	Andreas
Christmann	Christmann	k1gMnSc1	Christmann
<g/>
:	:	kIx,	:
Support	support	k1gInSc1	support
Vector	Vector	k1gMnSc1	Vector
Machines	Machines	k1gMnSc1	Machines
<g/>
,	,	kIx,	,
Springer	Springer	k1gMnSc1	Springer
<g/>
,	,	kIx,	,
New	New	k1gMnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
2008	[number]	k4	2008
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
-	-	kIx~	-
<g/>
387	[number]	k4	387
<g/>
-	-	kIx~	-
<g/>
77241	[number]	k4	77241
<g/>
-	-	kIx~	-
<g/>
7	[number]	k4	7
<g/>
.	.	kIx.	.
602	[number]	k4	602
pp	pp	k?	pp
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nello	Nello	k1gNnSc4	Nello
Cristianini	Cristianin	k2eAgMnPc1d1	Cristianin
<g/>
,	,	kIx,	,
John	John	k1gMnSc1	John
Shawe-Taylor	Shawe-Taylor	k1gMnSc1	Shawe-Taylor
<g/>
:	:	kIx,	:
Kernel	kernel	k1gInSc1	kernel
Methods	Methodsa	k1gFnPc2	Methodsa
for	forum	k1gNnPc2	forum
Pattern	Pattern	k1gMnSc1	Pattern
Analysis	Analysis	k1gFnSc2	Analysis
<g/>
,	,	kIx,	,
Cambridge	Cambridge	k1gFnSc2	Cambridge
University	universita	k1gFnSc2	universita
Press	Pressa	k1gFnPc2	Pressa
<g/>
,	,	kIx,	,
Cambridge	Cambridge	k1gFnSc2	Cambridge
<g/>
,	,	kIx,	,
2004	[number]	k4	2004
<g/>
,	,	kIx,	,
ISBN	ISBN	kA	ISBN
0-521-81397-2	[number]	k4	0-521-81397-2
</s>
</p>
<p>
<s>
Christopher	Christophra	k1gFnPc2	Christophra
J.	J.	kA	J.
C.	C.	kA	C.
Burges	Burgesa	k1gFnPc2	Burgesa
<g/>
:	:	kIx,	:
A	a	k9	a
Tutorial	Tutorial	k1gMnSc1	Tutorial
on	on	k3xPp3gMnSc1	on
Support	support	k1gInSc4	support
Vector	Vector	k1gInSc1	Vector
Machines	Machinesa	k1gFnPc2	Machinesa
for	forum	k1gNnPc2	forum
Pattern	Pattern	k1gMnSc1	Pattern
Recognition	Recognition	k1gInSc1	Recognition
<g/>
,	,	kIx,	,
Data	datum	k1gNnPc1	datum
Mining	Mining	k1gInSc4	Mining
and	and	k?	and
Knowledge	Knowledg	k1gFnSc2	Knowledg
Discovery	Discovera	k1gFnSc2	Discovera
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
<g/>
121	[number]	k4	121
<g/>
-	-	kIx~	-
<g/>
167	[number]	k4	167
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vladimir	Vladimir	k1gMnSc1	Vladimir
Vapnik	Vapnik	k1gMnSc1	Vapnik
<g/>
:	:	kIx,	:
Statistical	Statistical	k1gFnSc1	Statistical
Learning	Learning	k1gInSc4	Learning
Theory	Theora	k1gFnSc2	Theora
<g/>
,	,	kIx,	,
Wiley	Wilea	k1gFnSc2	Wilea
<g/>
,	,	kIx,	,
Chichester	Chichester	k1gMnSc1	Chichester
<g/>
,	,	kIx,	,
GB	GB	kA	GB
<g/>
,	,	kIx,	,
1998	[number]	k4	1998
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vladimir	Vladimir	k1gMnSc1	Vladimir
Vapnik	Vapnik	k1gMnSc1	Vapnik
<g/>
:	:	kIx,	:
The	The	k1gMnSc5	The
Nature	Natur	k1gMnSc5	Natur
of	of	k?	of
Statistical	Statistical	k1gFnSc3	Statistical
Learning	Learning	k1gInSc4	Learning
Theory	Theora	k1gFnSc2	Theora
<g/>
,	,	kIx,	,
Springer	Springer	k1gMnSc1	Springer
Verlag	Verlag	k1gMnSc1	Verlag
<g/>
,	,	kIx,	,
New	New	k1gMnSc1	New
York	York	k1gInSc1	York
<g/>
,	,	kIx,	,
NY	NY	kA	NY
<g/>
,	,	kIx,	,
USA	USA	kA	USA
<g/>
,	,	kIx,	,
1995	[number]	k4	1995
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
K.	K.	kA	K.
Bennett	Bennett	k1gMnSc1	Bennett
<g/>
,	,	kIx,	,
C.	C.	kA	C.
Campbell	Campbell	k1gInSc1	Campbell
<g/>
:	:	kIx,	:
Support	support	k1gInSc1	support
Vector	Vector	k1gMnSc1	Vector
Machines	Machines	k1gMnSc1	Machines
<g/>
:	:	kIx,	:
Hype	Hype	k1gInSc1	Hype
or	or	k?	or
Hallelujah	Hallelujah	k1gInSc1	Hallelujah
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
,	,	kIx,	,
ACM	ACM	kA	ACM
SIGKDD	SIGKDD	kA	SIGKDD
Explorations	Explorationsa	k1gFnPc2	Explorationsa
Newsletter	Newslettra	k1gFnPc2	Newslettra
<g/>
,	,	kIx,	,
2	[number]	k4	2
<g/>
(	(	kIx(	(
<g/>
2	[number]	k4	2
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
13	[number]	k4	13
<g/>
,	,	kIx,	,
New	New	k1gFnSc1	New
York	York	k1gInSc1	York
<g/>
:	:	kIx,	:
ACM	ACM	kA	ACM
<g/>
,	,	kIx,	,
2000	[number]	k4	2000
</s>
</p>
<p>
<s>
R.	R.	kA	R.
Freund	Freund	k1gMnSc1	Freund
<g/>
,	,	kIx,	,
F.	F.	kA	F.
Girosi	Girose	k1gFnSc4	Girose
<g/>
,	,	kIx,	,
E.	E.	kA	E.
Osuna	Osuna	k1gFnSc1	Osuna
<g/>
:	:	kIx,	:
An	An	k1gFnSc1	An
Improved	Improved	k1gInSc4	Improved
Training	Training	k1gInSc1	Training
Algorithm	Algorithma	k1gFnPc2	Algorithma
for	forum	k1gNnPc2	forum
Support	support	k1gInSc1	support
Vector	Vector	k1gMnSc1	Vector
Machines	Machines	k1gMnSc1	Machines
<g/>
,	,	kIx,	,
Neural	Neural	k1gMnPc1	Neural
Networks	Networksa	k1gFnPc2	Networksa
for	forum	k1gNnPc2	forum
Signal	Signal	k1gMnSc1	Signal
Processing	Processing	k1gInSc1	Processing
[	[	kIx(	[
<g/>
1997	[number]	k4	1997
<g/>
]	]	kIx)	]
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Proceedings	Proceedings	k1gInSc1	Proceedings
of	of	k?	of
the	the	k?	the
1997	[number]	k4	1997
IEEE	IEEE	kA	IEEE
Workshop	workshop	k1gInSc1	workshop
<g/>
,	,	kIx,	,
pp	pp	k?	pp
<g/>
.	.	kIx.	.
276	[number]	k4	276
<g/>
-	-	kIx~	-
<g/>
285	[number]	k4	285
<g/>
,	,	kIx,	,
IEEE	IEEE	kA	IEEE
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
</s>
</p>
