<s>
Šónen	Šónen	k1gInSc1	Šónen
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
少	少	k?	少
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
shō	shō	k?	shō
<g/>
,	,	kIx,	,
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
"	"	kIx"	"
<g/>
chlapec	chlapec	k1gMnSc1	chlapec
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
japonské	japonský	k2eAgNnSc1d1	Japonské
slovo	slovo	k1gNnSc1	slovo
užívané	užívaný	k2eAgNnSc1d1	užívané
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
mangy	mango	k1gNnPc7	mango
<g/>
,	,	kIx,	,
anime	animat	k5eAaPmIp3nS	animat
či	či	k8xC	či
doramy	doram	k1gInPc4	doram
určené	určený	k2eAgInPc4d1	určený
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
teenagery	teenager	k1gMnPc4	teenager
a	a	k8xC	a
starší	starý	k2eAgMnPc4d2	starší
chlapce	chlapec	k1gMnPc4	chlapec
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
šódžo	šódžo	k6eAd1	šódžo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
je	být	k5eAaImIp3nS	být
zaměřeno	zaměřit	k5eAaPmNgNnS	zaměřit
na	na	k7c4	na
dívky	dívka	k1gFnPc4	dívka
<g/>
.	.	kIx.	.
</s>
