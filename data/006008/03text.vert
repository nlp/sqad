<s>
Šónen	Šónen	k1gInSc1	Šónen
(	(	kIx(	(
<g/>
japonsky	japonsky	k6eAd1	japonsky
少	少	k?	少
<g/>
,	,	kIx,	,
anglicky	anglicky	k6eAd1	anglicky
shō	shō	k?	shō
<g/>
,	,	kIx,	,
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
"	"	kIx"	"
<g/>
chlapec	chlapec	k1gMnSc1	chlapec
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
japonské	japonský	k2eAgNnSc1d1	Japonské
slovo	slovo	k1gNnSc1	slovo
užívané	užívaný	k2eAgNnSc1d1	užívané
k	k	k7c3	k
označení	označení	k1gNnSc3	označení
mangy	mango	k1gNnPc7	mango
<g/>
,	,	kIx,	,
anime	animat	k5eAaPmIp3nS	animat
či	či	k8xC	či
doramy	doram	k1gInPc4	doram
určené	určený	k2eAgInPc4d1	určený
hlavně	hlavně	k9	hlavně
pro	pro	k7c4	pro
teenagery	teenager	k1gMnPc4	teenager
a	a	k8xC	a
starší	starý	k2eAgMnPc4d2	starší
chlapce	chlapec	k1gMnPc4	chlapec
<g/>
,	,	kIx,	,
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
šódžo	šódžo	k6eAd1	šódžo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
je	být	k5eAaImIp3nS	být
zaměřeno	zaměřit	k5eAaPmNgNnS	zaměřit
na	na	k7c4	na
dívky	dívka	k1gFnPc4	dívka
<g/>
.	.	kIx.	.
</s>
<s>
Díla	dílo	k1gNnPc1	dílo
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnPc1	jenž
typicky	typicky	k6eAd1	typicky
spadají	spadat	k5eAaPmIp3nP	spadat
pod	pod	k7c4	pod
označení	označení	k1gNnSc4	označení
šónen	šónna	k1gFnPc2	šónna
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zpravidla	zpravidla	k6eAd1	zpravidla
akční	akční	k2eAgFnSc1d1	akční
<g/>
,	,	kIx,	,
plná	plný	k2eAgFnSc1d1	plná
soubojů	souboj	k1gInPc2	souboj
a	a	k8xC	a
se	s	k7c7	s
zaměřením	zaměření	k1gNnSc7	zaměření
na	na	k7c4	na
vývoj	vývoj	k1gInSc4	vývoj
hlavního	hlavní	k2eAgMnSc2d1	hlavní
hrdiny	hrdina	k1gMnSc2	hrdina
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
se	se	k3xPyFc4	se
odehrávat	odehrávat	k5eAaImF	odehrávat
v	v	k7c6	v
mezích	mez	k1gFnPc6	mez
jakéhokoliv	jakýkoliv	k3yIgInSc2	jakýkoliv
žánru	žánr	k1gInSc2	žánr
či	či	k8xC	či
časového	časový	k2eAgNnSc2d1	časové
období	období	k1gNnSc2	období
<g/>
.	.	kIx.	.
</s>
<s>
Akame	Akamat	k5eAaPmIp3nS	Akamat
Ga	Ga	k1gMnSc1	Ga
Kill	Kill	k1gMnSc1	Kill
Appleseed	Appleseed	k1gMnSc1	Appleseed
Astro	astra	k1gFnSc5	astra
Bleach	Bleach	k1gMnSc1	Bleach
D.	D.	kA	D.
<g/>
Gray-man	Grayan	k1gMnSc1	Gray-man
Deadman	Deadman	k1gMnSc1	Deadman
Wonderland	Wonderlanda	k1gFnPc2	Wonderlanda
Death	Death	k1gMnSc1	Death
Note	Not	k1gFnSc2	Not
Detektiv	detektiv	k1gMnSc1	detektiv
Conan	Conan	k1gMnSc1	Conan
Dragon	Dragon	k1gMnSc1	Dragon
Ball	Ball	k1gMnSc1	Ball
Elfen	Elfna	k1gFnPc2	Elfna
Lied	Lied	k1gMnSc1	Lied
Erementar	Erementar	k1gMnSc1	Erementar
Gerad	Gerad	k1gInSc4	Gerad
Fairy	Faira	k1gFnSc2	Faira
Tail	Taila	k1gFnPc2	Taila
Fullmetal	Fullmetal	k1gMnSc1	Fullmetal
Alchemist	Alchemist	k1gMnSc1	Alchemist
Fullmetal	Fullmetal	k1gMnSc1	Fullmetal
Alchemist	Alchemist	k1gMnSc1	Alchemist
<g/>
:	:	kIx,	:
Bratrství	bratrství	k1gNnSc1	bratrství
Hikaru	Hikar	k1gInSc2	Hikar
no	no	k9	no
go	go	k?	go
Jakitate	Jakitat	k1gInSc5	Jakitat
<g/>
!!	!!	k?	!!
Japan	japan	k1gInSc4	japan
Kannazuki	Kannazuk	k1gFnSc2	Kannazuk
no	no	k9	no
miko	miko	k1gMnSc1	miko
Kiba	Kiba	k1gMnSc1	Kiba
Král	Král	k1gMnSc1	Král
šamanů	šaman	k1gMnPc2	šaman
Muteki	Mutek	k1gFnSc2	Mutek
kanban	kanban	k1gMnSc1	kanban
musume	musum	k1gInSc5	musum
Naruto	Narut	k2eAgNnSc1d1	Naruto
Naruto	Narut	k2eAgNnSc1d1	Naruto
<g/>
:	:	kIx,	:
Šippúden	Šippúdno	k1gNnPc2	Šippúdno
Saint	Sainta	k1gFnPc2	Sainta
Seija	Seija	k1gFnSc1	Seija
Sajonara	Sajonara	k1gFnSc1	Sajonara
zecubó	zecubó	k?	zecubó
sensei	sense	k1gFnSc2	sense
Tenkú	Tenkú	k1gFnSc2	Tenkú
no	no	k9	no
Escaflowne	Escaflown	k1gInSc5	Escaflown
Útok	útok	k1gInSc1	útok
titánů	titán	k1gMnPc2	titán
Yu-Gi-Oh	Yu-Gi-Oha	k1gFnPc2	Yu-Gi-Oha
<g/>
!	!	kIx.	!
</s>
<s>
Yu	Yu	k?	Yu
Yu	Yu	k1gMnSc1	Yu
Hakusho	Hakus	k1gMnSc2	Hakus
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Šónen	Šónna	k1gFnPc2	Šónna
na	na	k7c6	na
slovenské	slovenský	k2eAgFnSc6d1	slovenská
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
