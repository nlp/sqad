<p>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
Venezuely	Venezuela	k1gFnSc2	Venezuela
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
listem	list	k1gInSc7	list
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
3	[number]	k4	3
<g/>
,	,	kIx,	,
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
vodorovnými	vodorovný	k2eAgInPc7d1	vodorovný
pruhy	pruh	k1gInPc7	pruh
stejné	stejný	k2eAgFnSc2d1	stejná
šířky	šířka	k1gFnSc2	šířka
v	v	k7c6	v
barvách	barva	k1gFnPc6	barva
<g/>
:	:	kIx,	:
žlutá	žlutý	k2eAgFnSc1d1	žlutá
<g/>
,	,	kIx,	,
modra	modro	k1gNnSc2	modro
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
středu	střed	k1gInSc6	střed
modrého	modrý	k2eAgInSc2d1	modrý
pruhu	pruh	k1gInSc2	pruh
je	být	k5eAaImIp3nS	být
do	do	k7c2	do
oblouku	oblouk	k1gInSc2	oblouk
uspořádáno	uspořádat	k5eAaPmNgNnS	uspořádat
osm	osm	k4xCc1	osm
bílých	bílý	k2eAgFnPc2d1	bílá
pěticípých	pěticípý	k2eAgFnPc2d1	pěticípá
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
jsou	být	k5eAaImIp3nP	být
uspořádány	uspořádat	k5eAaPmNgFnP	uspořádat
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
jejich	jejich	k3xOp3gFnPc1	jejich
osy	osa	k1gFnPc1	osa
jsou	být	k5eAaImIp3nP	být
kolmé	kolmý	k2eAgFnPc1d1	kolmá
k	k	k7c3	k
tečně	tečna	k1gFnSc3	tečna
na	na	k7c6	na
pomyslném	pomyslný	k2eAgInSc6d1	pomyslný
půlkruhu	půlkruh	k1gInSc6	půlkruh
a	a	k8xC	a
svírají	svírat	k5eAaImIp3nP	svírat
navzájem	navzájem	k6eAd1	navzájem
úhel	úhel	k1gInSc4	úhel
20	[number]	k4	20
<g/>
°	°	k?	°
<g/>
.	.	kIx.	.
<g/>
Barvy	barva	k1gFnPc1	barva
vlajky	vlajka	k1gFnPc1	vlajka
jsou	být	k5eAaImIp3nP	být
odvozeny	odvodit	k5eAaPmNgFnP	odvodit
z	z	k7c2	z
vlajky	vlajka	k1gFnSc2	vlajka
Francisca	Franciscus	k1gMnSc2	Franciscus
de	de	k?	de
Mirandy	Miranda	k1gFnSc2	Miranda
<g/>
,	,	kIx,	,
kreolského	kreolský	k2eAgMnSc2d1	kreolský
důstojníka	důstojník	k1gMnSc2	důstojník
<g/>
,	,	kIx,	,
francouzského	francouzský	k2eAgMnSc2d1	francouzský
generála	generál	k1gMnSc2	generál
a	a	k8xC	a
vůdce	vůdce	k1gMnSc2	vůdce
osvobozeneckého	osvobozenecký	k2eAgNnSc2d1	osvobozenecké
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1806	[number]	k4	1806
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
byly	být	k5eAaImAgFnP	být
tyto	tento	k3xDgFnPc1	tento
barvy	barva	k1gFnPc1	barva
užívány	užíván	k2eAgFnPc1d1	užívána
i	i	k9	i
konfederací	konfederace	k1gFnPc2	konfederace
Velké	velký	k2eAgFnSc2d1	velká
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
<g/>
,	,	kIx,	,
ke	k	k7c3	k
které	který	k3yIgFnPc1	který
Venezuela	Venezuela	k1gFnSc1	Venezuela
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Kolumbií	Kolumbie	k1gFnSc7	Kolumbie
<g/>
,	,	kIx,	,
Ekvádorem	Ekvádor	k1gInSc7	Ekvádor
<g/>
,	,	kIx,	,
Panamou	Panama	k1gFnSc7	Panama
a	a	k8xC	a
západní	západní	k2eAgFnSc7d1	západní
částí	část	k1gFnSc7	část
Guyany	Guyana	k1gFnSc2	Guyana
patřila	patřit	k5eAaImAgFnS	patřit
<g/>
.	.	kIx.	.
<g/>
Hvězdy	hvězda	k1gFnPc4	hvězda
jsou	být	k5eAaImIp3nP	být
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
(	(	kIx(	(
<g/>
v	v	k7c6	v
měnícím	měnící	k2eAgMnSc6d1	měnící
se	se	k3xPyFc4	se
seskupení	seskupení	k1gNnSc1	seskupení
<g/>
)	)	kIx)	)
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1814	[number]	k4	1814
<g/>
.	.	kIx.	.
</s>
<s>
Sedm	sedm	k4xCc1	sedm
hvězd	hvězda	k1gFnPc2	hvězda
reprezentuje	reprezentovat	k5eAaImIp3nS	reprezentovat
sedm	sedm	k4xCc1	sedm
tehdejších	tehdejší	k2eAgFnPc2d1	tehdejší
provincií	provincie	k1gFnPc2	provincie
-	-	kIx~	-
signatářů	signatář	k1gMnPc2	signatář
vyhlášení	vyhlášení	k1gNnSc2	vyhlášení
venezuelské	venezuelský	k2eAgFnSc2d1	venezuelská
nezávislosti	nezávislost	k1gFnSc2	nezávislost
(	(	kIx(	(
<g/>
Caracas	Caracas	k1gInSc1	Caracas
<g/>
,	,	kIx,	,
Cumaná	Cumaná	k1gFnSc1	Cumaná
<g/>
,	,	kIx,	,
Barcelona	Barcelona	k1gFnSc1	Barcelona
<g/>
,	,	kIx,	,
Barinas	Barinas	k1gInSc1	Barinas
<g/>
,	,	kIx,	,
Margarita	Margarita	k1gFnSc1	Margarita
<g/>
,	,	kIx,	,
Mérida	Mérida	k1gFnSc1	Mérida
a	a	k8xC	a
Trujillo	Trujillo	k1gNnSc1	Trujillo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Osmá	osmý	k4xOgFnSc1	osmý
hvězda	hvězda	k1gFnSc1	hvězda
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
vlajku	vlajka	k1gFnSc4	vlajka
přidaná	přidaný	k2eAgFnSc1d1	přidaná
při	při	k7c6	při
poslední	poslední	k2eAgFnSc6d1	poslední
změně	změna	k1gFnSc6	změna
vlajky	vlajka	k1gFnSc2	vlajka
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
(	(	kIx(	(
<g/>
za	za	k7c2	za
prezidenta	prezident	k1gMnSc2	prezident
Cháveze	Cháveze	k1gFnSc2	Cháveze
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
teritorium	teritorium	k1gNnSc4	teritorium
Esequiba	Esequiba	k1gMnSc1	Esequiba
nacházející	nacházející	k2eAgMnSc1d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Guyany	Guyana	k1gFnSc2	Guyana
(	(	kIx(	(
<g/>
více	hodně	k6eAd2	hodně
než	než	k8xS	než
70	[number]	k4	70
%	%	kIx~	%
jejího	její	k3xOp3gNnSc2	její
území	území	k1gNnSc2	území
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Essequibo	Essequiba	k1gMnSc5	Essequiba
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yRgFnPc4	který
si	se	k3xPyFc3	se
Venezuela	Venezuela	k1gFnSc1	Venezuela
činí	činit	k5eAaImIp3nS	činit
nárok	nárok	k1gInSc4	nárok
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgInPc1	některý
zdroje	zdroj	k1gInPc1	zdroj
z	z	k7c2	z
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
uváděly	uvádět	k5eAaImAgInP	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
osmá	osmý	k4xOgFnSc1	osmý
hvězda	hvězda	k1gFnSc1	hvězda
byla	být	k5eAaImAgFnS	být
přidána	přidat	k5eAaPmNgFnS	přidat
na	na	k7c4	na
památku	památka	k1gFnSc4	památka
Simóna	Simón	k1gMnSc2	Simón
Bolívara	Bolívar	k1gMnSc2	Bolívar
<g/>
.	.	kIx.	.
<g/>
Žlutá	žlutý	k2eAgFnSc1d1	žlutá
barva	barva	k1gFnSc1	barva
symbolizuje	symbolizovat	k5eAaImIp3nS	symbolizovat
nové	nový	k2eAgFnPc4d1	nová
příležitosti	příležitost	k1gFnPc4	příležitost
a	a	k8xC	a
možnosti	možnost	k1gFnPc4	možnost
pro	pro	k7c4	pro
Ameriku	Amerika	k1gFnSc4	Amerika
po	po	k7c6	po
osvobození	osvobození	k1gNnSc6	osvobození
<g/>
.	.	kIx.	.
</s>
<s>
Červená	červenat	k5eAaImIp3nS	červenat
Španělsko	Španělsko	k1gNnSc1	Španělsko
<g/>
,	,	kIx,	,
modrá	modrat	k5eAaImIp3nS	modrat
vody	voda	k1gFnSc2	voda
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
mezi	mezi	k7c7	mezi
oběma	dva	k4xCgFnPc7	dva
zeměmi	zem	k1gFnPc7	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
horním	horní	k2eAgInSc6d1	horní
rohu	roh	k1gInSc6	roh
státní	státní	k2eAgFnSc2d1	státní
vlajky	vlajka	k1gFnSc2	vlajka
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
žlutém	žlutý	k2eAgInSc6d1	žlutý
pruhu	pruh	k1gInSc6	pruh
v	v	k7c6	v
žerďovém	žerďový	k2eAgInSc6d1	žerďový
rohu	roh	k1gInSc6	roh
venezuelský	venezuelský	k2eAgInSc4d1	venezuelský
státní	státní	k2eAgInSc4d1	státní
znak	znak	k1gInSc4	znak
<g/>
,	,	kIx,	,
naposledy	naposledy	k6eAd1	naposledy
změněný	změněný	k2eAgMnSc1d1	změněný
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Území	území	k1gNnSc1	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Venezuely	Venezuela	k1gFnSc2	Venezuela
bylo	být	k5eAaImAgNnS	být
původně	původně	k6eAd1	původně
osídleno	osídlit	k5eAaPmNgNnS	osídlit
Arawaky	Arawak	k1gMnPc7	Arawak
a	a	k8xC	a
Kariby	Karib	k1gMnPc7	Karib
<g/>
.	.	kIx.	.
</s>
<s>
Ústí	ústí	k1gNnSc1	ústí
řeky	řeka	k1gFnSc2	řeka
Orinoko	Orinoko	k1gNnSc1	Orinoko
objevil	objevit	k5eAaPmAgInS	objevit
1	[number]	k4	1
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1498	[number]	k4	1498
<g/>
,	,	kIx,	,
při	při	k7c6	při
své	svůj	k3xOyFgFnSc6	svůj
třetí	třetí	k4xOgFnSc6	třetí
výpravě	výprava	k1gFnSc6	výprava
<g/>
,	,	kIx,	,
Kryštof	Kryštof	k1gMnSc1	Kryštof
Kolumbus	Kolumbus	k1gMnSc1	Kolumbus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1499	[number]	k4	1499
pojmenoval	pojmenovat	k5eAaPmAgMnS	pojmenovat
Alonso	Alonsa	k1gFnSc5	Alonsa
de	de	k?	de
Hojeda	Hojeda	k1gMnSc1	Hojeda
indiánskou	indiánský	k2eAgFnSc4d1	indiánská
vesnici	vesnice	k1gFnSc4	vesnice
<g/>
,	,	kIx,	,
v	v	k7c6	v
zálivu	záliv	k1gInSc6	záliv
u	u	k7c2	u
dnešního	dnešní	k2eAgNnSc2d1	dnešní
města	město	k1gNnSc2	město
Maracaibo	Maracaiba	k1gFnSc5	Maracaiba
<g/>
,	,	kIx,	,
Venezuela	Venezuela	k1gFnSc1	Venezuela
(	(	kIx(	(
<g/>
v	v	k7c6	v
překladu	překlad	k1gInSc6	překlad
Malé	Malé	k2eAgFnPc1d1	Malé
Benátky	Benátky	k1gFnPc1	Benátky
<g/>
)	)	kIx)	)
podle	podle	k7c2	podle
podobnosti	podobnost	k1gFnSc2	podobnost
staveb	stavba	k1gFnPc2	stavba
s	s	k7c7	s
italským	italský	k2eAgNnSc7d1	italské
městem	město	k1gNnSc7	město
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
název	název	k1gInSc1	název
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
rozšiřovalo	rozšiřovat	k5eAaImAgNnS	rozšiřovat
o	o	k7c4	o
další	další	k2eAgNnSc4d1	další
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
až	až	k9	až
na	na	k7c4	na
dnešní	dnešní	k2eAgInSc4d1	dnešní
název	název	k1gInSc4	název
jihoamerického	jihoamerický	k2eAgInSc2d1	jihoamerický
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1717	[number]	k4	1717
se	se	k3xPyFc4	se
venezuelské	venezuelský	k2eAgFnPc1d1	venezuelská
provincie	provincie	k1gFnPc1	provincie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
kolonizace	kolonizace	k1gFnSc2	kolonizace
vznikly	vzniknout	k5eAaPmAgFnP	vzniknout
<g/>
,	,	kIx,	,
staly	stát	k5eAaPmAgFnP	stát
součástí	součást	k1gFnSc7	součást
Místokrálovství	Místokrálovství	k1gNnSc2	Místokrálovství
Nová	nový	k2eAgFnSc1d1	nová
Granada	Granada	k1gFnSc1	Granada
<g/>
,	,	kIx,	,
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1777	[number]	k4	1777
součástí	součást	k1gFnPc2	součást
generálního	generální	k2eAgInSc2d1	generální
kapitanátu	kapitanát	k1gInSc2	kapitanát
v	v	k7c6	v
Caracasu	Caracas	k1gInSc6	Caracas
(	(	kIx(	(
<g/>
současném	současný	k2eAgNnSc6d1	současné
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Venezuely	Venezuela	k1gFnSc2	Venezuela
<g/>
)	)	kIx)	)
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1783	[number]	k4	1783
byla	být	k5eAaImAgFnS	být
(	(	kIx(	(
<g/>
v	v	k7c6	v
přibližně	přibližně	k6eAd1	přibližně
současných	současný	k2eAgFnPc6d1	současná
hranicích	hranice	k1gFnPc6	hranice
<g/>
)	)	kIx)	)
založena	založen	k2eAgFnSc1d1	založena
superintendence	superintendence	k1gFnSc1	superintendence
Venezuela	Venezuela	k1gFnSc1	Venezuela
<g/>
.	.	kIx.	.
</s>
<s>
Prvními	první	k4xOgFnPc7	první
vlajkami	vlajka	k1gFnPc7	vlajka
<g/>
,	,	kIx,	,
vyvěšovanými	vyvěšovaný	k2eAgFnPc7d1	vyvěšovaná
na	na	k7c4	na
území	území	k1gNnSc4	území
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Venezuely	Venezuela	k1gFnSc2	Venezuela
<g/>
,	,	kIx,	,
tak	tak	k9	tak
byly	být	k5eAaImAgFnP	být
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1785	[number]	k4	1785
vlajky	vlajka	k1gFnSc2	vlajka
španělské	španělský	k2eAgFnSc2d1	španělská
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Po	po	k7c6	po
Velké	velká	k1gFnSc6	velká
francouzské	francouzský	k2eAgNnSc1d1	francouzské
(	(	kIx(	(
<g/>
1789	[number]	k4	1789
<g/>
–	–	k?	–
<g/>
1799	[number]	k4	1799
<g/>
)	)	kIx)	)
a	a	k8xC	a
haitské	haitský	k2eAgFnSc6d1	Haitská
revoluci	revoluce	k1gFnSc6	revoluce
(	(	kIx(	(
<g/>
1791	[number]	k4	1791
<g/>
–	–	k?	–
<g/>
1804	[number]	k4	1804
<g/>
)	)	kIx)	)
začalo	začít	k5eAaPmAgNnS	začít
i	i	k9	i
ve	v	k7c6	v
španělských	španělský	k2eAgFnPc6d1	španělská
koloniích	kolonie	k1gFnPc6	kolonie
sílit	sílit	k5eAaImF	sílit
osvobozenecké	osvobozenecký	k2eAgNnSc4d1	osvobozenecké
hnutí	hnutí	k1gNnSc4	hnutí
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
souvislosti	souvislost	k1gFnSc6	souvislost
vzniklo	vzniknout	k5eAaPmAgNnS	vzniknout
několik	několik	k4yIc1	několik
vlajek	vlajka	k1gFnPc2	vlajka
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
symbolů	symbol	k1gInPc2	symbol
odporu	odpor	k1gInSc2	odpor
proti	proti	k7c3	proti
španělskému	španělský	k2eAgInSc3d1	španělský
koloniálnímu	koloniální	k2eAgInSc3d1	koloniální
režimu	režim	k1gInSc3	režim
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1797	[number]	k4	1797
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
Juan	Juan	k1gMnSc1	Juan
Bautista	Bautista	k1gMnSc1	Bautista
Picornell	Picornell	k1gMnSc1	Picornell
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
narozený	narozený	k2eAgMnSc1d1	narozený
pedagog	pedagog	k1gMnSc1	pedagog
a	a	k8xC	a
přívrženec	přívrženec	k1gMnSc1	přívrženec
povstání	povstání	k1gNnSc2	povstání
proti	proti	k7c3	proti
španělskému	španělský	k2eAgInSc3d1	španělský
kolonialismu	kolonialismus	k1gInSc3	kolonialismus
<g/>
,	,	kIx,	,
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
,	,	kIx,	,
tvořenou	tvořený	k2eAgFnSc4d1	tvořená
čtyřmi	čtyři	k4xCgInPc7	čtyři
svislými	svislý	k2eAgInPc7d1	svislý
pruhy	pruh	k1gInPc7	pruh
<g/>
:	:	kIx,	:
žlutým	žlutý	k2eAgInSc7d1	žlutý
<g/>
,	,	kIx,	,
červeným	červený	k2eAgInSc7d1	červený
<g/>
,	,	kIx,	,
bílým	bílý	k2eAgNnSc7d1	bílé
a	a	k8xC	a
modrým	modré	k1gNnSc7	modré
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pruhy	pruh	k1gInPc1	pruh
měly	mít	k5eAaImAgFnP	mít
trojí	trojit	k5eAaImIp3nP	trojit
význam	význam	k1gInSc4	význam
<g/>
,	,	kIx,	,
symbolizovaly	symbolizovat	k5eAaImAgInP	symbolizovat
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
4	[number]	k4	4
etnické	etnický	k2eAgFnPc4d1	etnická
skupiny	skupina	k1gFnPc4	skupina
(	(	kIx(	(
<g/>
indiáni	indián	k1gMnPc1	indián
<g/>
,	,	kIx,	,
mulati	mulat	k1gMnPc1	mulat
<g/>
,	,	kIx,	,
běloši	běloch	k1gMnPc1	běloch
a	a	k8xC	a
černoši	černoch	k1gMnPc1	černoch
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
4	[number]	k4	4
provincie	provincie	k1gFnSc1	provincie
(	(	kIx(	(
<g/>
Caracas	Caracas	k1gInSc1	Caracas
<g/>
,	,	kIx,	,
Maracaibo	Maracaiba	k1gFnSc5	Maracaiba
<g/>
,	,	kIx,	,
Cumaná	Cumaná	k1gFnSc1	Cumaná
a	a	k8xC	a
Guyana	Guyana	k1gFnSc1	Guyana
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
4	[number]	k4	4
revoluční	revoluční	k2eAgFnPc4d1	revoluční
zásady	zásada	k1gFnPc4	zásada
(	(	kIx(	(
<g/>
rovnost	rovnost	k1gFnSc1	rovnost
<g/>
,	,	kIx,	,
svoboda	svoboda	k1gFnSc1	svoboda
<g/>
,	,	kIx,	,
majetek	majetek	k1gInSc1	majetek
a	a	k8xC	a
bezpečnost	bezpečnost	k1gFnSc1	bezpečnost
<g/>
)	)	kIx)	)
<g/>
José	Josá	k1gFnSc2	Josá
María	María	k1gMnSc1	María
Españ	Españ	k1gMnSc1	Españ
a	a	k8xC	a
Manuel	Manuel	k1gMnSc1	Manuel
Gual	Gual	k1gMnSc1	Gual
<g/>
,	,	kIx,	,
vůdci	vůdce	k1gMnPc1	vůdce
nezdařeného	zdařený	k2eNgNnSc2d1	nezdařené
povstání	povstání	k1gNnSc2	povstání
ze	z	k7c2	z
13	[number]	k4	13
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1797	[number]	k4	1797
<g/>
,	,	kIx,	,
navrhli	navrhnout	k5eAaPmAgMnP	navrhnout
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
měla	mít	k5eAaImAgFnS	mít
symbolizovat	symbolizovat	k5eAaImF	symbolizovat
boj	boj	k1gInSc4	boj
za	za	k7c4	za
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
měla	mít	k5eAaImAgFnS	mít
2	[number]	k4	2
vodorovné	vodorovný	k2eAgInPc4d1	vodorovný
pruhy	pruh	k1gInPc4	pruh
<g/>
:	:	kIx,	:
bílý	bílý	k2eAgInSc4d1	bílý
a	a	k8xC	a
modrý	modrý	k2eAgInSc4d1	modrý
s	s	k7c7	s
poměrem	poměr	k1gInSc7	poměr
šířek	šířka	k1gFnPc2	šířka
asi	asi	k9	asi
4	[number]	k4	4
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vlající	vlající	k2eAgFnSc6d1	vlající
části	část	k1gFnSc6	část
pak	pak	k6eAd1	pak
byly	být	k5eAaImAgFnP	být
čtyři	čtyři	k4xCgFnPc1	čtyři
úzké	úzký	k2eAgFnPc1d1	úzká
proužky	proužka	k1gFnPc1	proužka
<g/>
,	,	kIx,	,
stejných	stejný	k2eAgFnPc2d1	stejná
barev	barva	k1gFnPc2	barva
jako	jako	k8xS	jako
v	v	k7c6	v
předcházejícím	předcházející	k2eAgInSc6d1	předcházející
návrhu	návrh	k1gInSc6	návrh
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bílém	bílý	k2eAgInSc6d1	bílý
pruhu	pruh	k1gInSc6	pruh
bylo	být	k5eAaImAgNnS	být
žluté	žlutý	k2eAgNnSc1d1	žluté
slunce	slunce	k1gNnSc1	slunce
s	s	k7c7	s
lidskou	lidský	k2eAgFnSc7d1	lidská
tváří	tvář	k1gFnSc7	tvář
<g/>
,	,	kIx,	,
v	v	k7c6	v
modrém	modrý	k2eAgNnSc6d1	modré
čtyři	čtyři	k4xCgFnPc4	čtyři
bílé	bílý	k2eAgFnPc4d1	bílá
<g/>
,	,	kIx,	,
pěticípé	pěticípý	k2eAgFnPc4d1	pěticípá
hvězdy	hvězda	k1gFnPc4	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Symbolika	symbolika	k1gFnSc1	symbolika
byla	být	k5eAaImAgFnS	být
stejná	stejný	k2eAgFnSc1d1	stejná
jako	jako	k8xS	jako
u	u	k7c2	u
předcházející	předcházející	k2eAgFnSc2d1	předcházející
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
,	,	kIx,	,
slunce	slunce	k1gNnSc1	slunce
představovalo	představovat	k5eAaImAgNnS	představovat
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
,	,	kIx,	,
čtyři	čtyři	k4xCgInPc1	čtyři
prvky	prvek	k1gInPc1	prvek
také	také	k9	také
spravedlnost	spravedlnost	k1gFnSc1	spravedlnost
<g/>
,	,	kIx,	,
samosprávu	samospráva	k1gFnSc4	samospráva
<g/>
,	,	kIx,	,
svornost	svornost	k1gFnSc4	svornost
a	a	k8xC	a
jednotu	jednota	k1gFnSc4	jednota
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1800	[number]	k4	1800
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
Francisco	Francisco	k1gMnSc1	Francisco
de	de	k?	de
Miranda	Miranda	k1gFnSc1	Miranda
<g/>
,	,	kIx,	,
kreolský	kreolský	k2eAgMnSc1d1	kreolský
důstojník	důstojník	k1gMnSc1	důstojník
<g/>
,	,	kIx,	,
francouzský	francouzský	k2eAgMnSc1d1	francouzský
generál	generál	k1gMnSc1	generál
a	a	k8xC	a
vůdce	vůdce	k1gMnSc1	vůdce
osvobozeneckého	osvobozenecký	k2eAgNnSc2d1	osvobozenecké
hnutí	hnutí	k1gNnSc2	hnutí
<g/>
,	,	kIx,	,
vlajku	vlajka	k1gFnSc4	vlajka
se	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
vodorovnými	vodorovný	k2eAgInPc7d1	vodorovný
pruhy	pruh	k1gInPc7	pruh
<g/>
:	:	kIx,	:
černým	černý	k2eAgInSc7d1	černý
<g/>
,	,	kIx,	,
červeným	červený	k2eAgInSc7d1	červený
a	a	k8xC	a
žlutým	žlutý	k2eAgInSc7d1	žlutý
<g/>
.	.	kIx.	.
</s>
<s>
Barvy	barva	k1gFnPc1	barva
symbolizovaly	symbolizovat	k5eAaImAgFnP	symbolizovat
černochy	černoch	k1gMnPc4	černoch
<g/>
,	,	kIx,	,
indiány	indián	k1gMnPc4	indián
a	a	k8xC	a
mulaty	mulat	k1gMnPc4	mulat
<g/>
.	.	kIx.	.
<g/>
Miranda	Miranda	k1gFnSc1	Miranda
je	být	k5eAaImIp3nS	být
též	též	k9	též
autorem	autor	k1gMnSc7	autor
námořní	námořní	k2eAgFnSc2d1	námořní
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
(	(	kIx(	(
<g/>
údajně	údajně	k6eAd1	údajně
<g/>
)	)	kIx)	)
vztyčil	vztyčit	k5eAaPmAgMnS	vztyčit
na	na	k7c6	na
svém	svůj	k3xOyFgInSc6	svůj
škuneru	škuner	k1gInSc6	škuner
Leandro	Leandra	k1gFnSc5	Leandra
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
vztyčená	vztyčený	k2eAgFnSc1d1	vztyčená
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
roku	rok	k1gInSc2	rok
1806	[number]	k4	1806
byla	být	k5eAaImAgFnS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
modrým	modrý	k2eAgInSc7d1	modrý
listem	list	k1gInSc7	list
s	s	k7c7	s
polovinou	polovina	k1gFnSc7	polovina
žlutého	žlutý	k2eAgMnSc2d1	žlutý
<g/>
,	,	kIx,	,
vycházejícího	vycházející	k2eAgNnSc2d1	vycházející
slunce	slunce	k1gNnSc2	slunce
s	s	k7c7	s
lidskou	lidský	k2eAgFnSc7d1	lidská
tváří	tvář	k1gFnSc7	tvář
v	v	k7c6	v
horním	horní	k2eAgInSc6d1	horní
<g/>
,	,	kIx,	,
žerďovém	žerďový	k2eAgInSc6d1	žerďový
rohu	roh	k1gInSc6	roh
a	a	k8xC	a
s	s	k7c7	s
bílým	bílý	k2eAgInSc7d1	bílý
Měsícem	měsíc	k1gInSc7	měsíc
v	v	k7c6	v
úplňku	úplněk	k1gInSc6	úplněk
(	(	kIx(	(
<g/>
také	také	k9	také
s	s	k7c7	s
lidskou	lidský	k2eAgFnSc7d1	lidská
tváří	tvář	k1gFnSc7	tvář
<g/>
)	)	kIx)	)
v	v	k7c6	v
dolním	dolní	k2eAgInSc6d1	dolní
cípu	cíp	k1gInSc6	cíp
<g/>
.	.	kIx.	.
</s>
<s>
Nad	nad	k7c7	nad
vlajkou	vlajka	k1gFnSc7	vlajka
byl	být	k5eAaImAgInS	být
rudý	rudý	k2eAgInSc1d1	rudý
fábor	fábor	k1gInSc1	fábor
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
MUERA	MUERA	kA	MUERA
LA	la	k1gNnSc2	la
TIRANÍA	TIRANÍA	kA	TIRANÍA
Y	Y	kA	Y
VIVA	VIVA	kA	VIVA
LA	la	k1gNnPc2	la
LIBERTAD	LIBERTAD	kA	LIBERTAD
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Ať	ať	k9	ať
zhyne	zhynout	k5eAaPmIp3nS	zhynout
tyranie	tyranie	k1gFnSc1	tyranie
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
svoboda	svoboda	k1gFnSc1	svoboda
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Měsíc	měsíc	k1gInSc1	měsíc
symbolizoval	symbolizovat	k5eAaImAgInS	symbolizovat
Španělsko	Španělsko	k1gNnSc4	Španělsko
<g/>
,	,	kIx,	,
slunce	slunce	k1gNnSc4	slunce
Ameriku	Amerika	k1gFnSc4	Amerika
a	a	k8xC	a
modrá	modrý	k2eAgFnSc1d1	modrá
barva	barva	k1gFnSc1	barva
Atlantský	atlantský	k2eAgInSc1d1	atlantský
oceán	oceán	k1gInSc1	oceán
mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
<g/>
.	.	kIx.	.
</s>
<s>
Vlajka	vlajka	k1gFnSc1	vlajka
na	na	k7c6	na
obrázku	obrázek	k1gInSc6	obrázek
je	být	k5eAaImIp3nS	být
mírně	mírně	k6eAd1	mírně
odlišná	odlišný	k2eAgFnSc1d1	odlišná
<g/>
.12	.12	k4	.12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1806	[number]	k4	1806
se	se	k3xPyFc4	se
Miranda	Mirando	k1gNnSc2	Mirando
neúspěšně	úspěšně	k6eNd1	úspěšně
pokusil	pokusit	k5eAaPmAgMnS	pokusit
vylodit	vylodit	k5eAaPmF	vylodit
u	u	k7c2	u
La	la	k1gNnSc2	la
Vela	velo	k1gNnSc2	velo
de	de	k?	de
Coro	Coro	k1gMnSc1	Coro
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Ocumare	Ocumar	k1gMnSc5	Ocumar
v	v	k7c6	v
západní	západní	k2eAgFnSc6d1	západní
části	část	k1gFnSc6	část
Venezuely	Venezuela	k1gFnSc2	Venezuela
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
vztyčil	vztyčit	k5eAaPmAgMnS	vztyčit
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
své	svůj	k3xOyFgFnSc2	svůj
lodi	loď	k1gFnSc2	loď
žluto-modro-červenou	žlutoodro-červený	k2eAgFnSc4d1	žluto-modro-červený
trikolóru	trikolóra	k1gFnSc4	trikolóra
s	s	k7c7	s
vodorovnými	vodorovný	k2eAgInPc7d1	vodorovný
pruhy	pruh	k1gInPc7	pruh
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
je	být	k5eAaImIp3nS	být
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
první	první	k4xOgFnSc4	první
venezuelskou	venezuelský	k2eAgFnSc4d1	venezuelská
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
3	[number]	k4	3
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
se	se	k3xPyFc4	se
Mirandovi	Mirandův	k2eAgMnPc1d1	Mirandův
(	(	kIx(	(
<g/>
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
britského	britský	k2eAgMnSc4d1	britský
guvernéra	guvernér	k1gMnSc4	guvernér
Trinidadu	Trinidad	k1gInSc2	Trinidad
<g/>
)	)	kIx)	)
podařilo	podařit	k5eAaPmAgNnS	podařit
vylodit	vylodit	k5eAaPmF	vylodit
na	na	k7c6	na
venezuelském	venezuelský	k2eAgNnSc6d1	venezuelské
území	území	k1gNnSc6	území
a	a	k8xC	a
vztyčit	vztyčit	k5eAaPmF	vztyčit
ji	on	k3xPp3gFnSc4	on
na	na	k7c6	na
věži	věž	k1gFnSc6	věž
farního	farní	k2eAgInSc2d1	farní
kostela	kostel	k1gInSc2	kostel
v	v	k7c6	v
Santa	Sant	k1gInSc2	Sant
Ana	Ana	k1gFnPc2	Ana
de	de	k?	de
Coro	Coro	k1gNnSc4	Coro
(	(	kIx(	(
<g/>
sousední	sousední	k2eAgNnSc4d1	sousední
město	město	k1gNnSc4	město
La	la	k1gNnSc2	la
Vela	velo	k1gNnSc2	velo
de	de	k?	de
Coro	Coro	k1gMnSc1	Coro
<g/>
,	,	kIx,	,
s	s	k7c7	s
kterým	který	k3yQgInSc7	který
dnes	dnes	k6eAd1	dnes
tvoří	tvořit	k5eAaImIp3nP	tvořit
aglomeraci	aglomerace	k1gFnSc4	aglomerace
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
akt	akt	k1gInSc1	akt
na	na	k7c6	na
území	území	k1gNnSc6	území
superintendence	superintendence	k1gFnSc2	superintendence
je	být	k5eAaImIp3nS	být
považován	považován	k2eAgInSc1d1	považován
za	za	k7c4	za
finální	finální	k2eAgNnSc4d1	finální
zahájení	zahájení	k1gNnSc4	zahájení
boje	boj	k1gInSc2	boj
za	za	k7c4	za
venezuelskou	venezuelský	k2eAgFnSc4d1	venezuelská
nezávislost	nezávislost	k1gFnSc4	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Symbolika	symbolika	k1gFnSc1	symbolika
barev	barva	k1gFnPc2	barva
je	být	k5eAaImIp3nS	být
nejasná	jasný	k2eNgFnSc1d1	nejasná
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
jednoho	jeden	k4xCgInSc2	jeden
zdroje	zdroj	k1gInSc2	zdroj
byly	být	k5eAaImAgFnP	být
barvy	barva	k1gFnPc1	barva
odvozeny	odvozen	k2eAgFnPc1d1	odvozena
z	z	k7c2	z
barev	barva	k1gFnPc2	barva
hamburského	hamburský	k2eAgInSc2d1	hamburský
pluku	pluk	k1gInSc2	pluk
<g/>
,	,	kIx,	,
v	v	k7c6	v
kterém	který	k3yIgInSc6	který
Miranda	Mirando	k1gNnPc4	Mirando
bojoval	bojovat	k5eAaImAgMnS	bojovat
za	za	k7c2	za
svého	svůj	k3xOyFgNnSc2	svůj
působení	působení	k1gNnSc2	působení
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgInSc1d1	další
zdroj	zdroj	k1gInSc1	zdroj
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
inspiraci	inspirace	k1gFnSc4	inspirace
návrh	návrh	k1gInSc4	návrh
vlajky	vlajka	k1gFnSc2	vlajka
Españ	Españ	k1gFnSc2	Españ
a	a	k8xC	a
Guala	Gualo	k1gNnSc2	Gualo
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1797	[number]	k4	1797
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1808	[number]	k4	1808
abdikoval	abdikovat	k5eAaBmAgMnS	abdikovat
španělský	španělský	k2eAgMnSc1d1	španělský
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
a	a	k8xC	a
o	o	k7c4	o
trůn	trůn	k1gInSc4	trůn
svedli	svést	k5eAaPmAgMnP	svést
boj	boj	k1gInSc1	boj
Ferdinand	Ferdinand	k1gMnSc1	Ferdinand
VII	VII	kA	VII
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
syn	syn	k1gMnSc1	syn
Karla	Karel	k1gMnSc2	Karel
IV	IV	kA	IV
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
a	a	k8xC	a
Josef	Josef	k1gMnSc1	Josef
Bonaparte	bonapart	k1gInSc5	bonapart
(	(	kIx(	(
<g/>
Napoleonův	Napoleonův	k2eAgMnSc1d1	Napoleonův
bratr	bratr	k1gMnSc1	bratr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1810	[number]	k4	1810
sesadila	sesadit	k5eAaPmAgFnS	sesadit
městská	městský	k2eAgFnSc1d1	městská
rada	rada	k1gFnSc1	rada
v	v	k7c6	v
Caracasu	Caracas	k1gInSc6	Caracas
generálního	generální	k2eAgMnSc2d1	generální
kapitána	kapitán	k1gMnSc2	kapitán
a	a	k8xC	a
<g />
.	.	kIx.	.
</s>
<s>
předala	předat	k5eAaPmAgFnS	předat
moc	moc	k6eAd1	moc
kreolské	kreolský	k2eAgFnSc3d1	kreolská
juntě	junta	k1gFnSc3	junta
(	(	kIx(	(
<g/>
podporující	podporující	k2eAgNnPc1d1	podporující
formálně	formálně	k6eAd1	formálně
Ferdinanda	Ferdinand	k1gMnSc2	Ferdinand
VII	VII	kA	VII
<g/>
.	.	kIx.	.
<g/>
)	)	kIx)	)
Junta	junta	k1gFnSc1	junta
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
Spojené	spojený	k2eAgFnPc4d1	spojená
provincie	provincie	k1gFnPc4	provincie
Venezuely	Venezuela	k1gFnSc2	Venezuela
a	a	k8xC	a
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
většinu	většina	k1gFnSc4	většina
venezuelského	venezuelský	k2eAgNnSc2d1	venezuelské
území	území	k1gNnSc2	území
<g/>
.	.	kIx.	.
4	[number]	k4	4
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
byla	být	k5eAaImAgFnS	být
schválena	schválit	k5eAaPmNgFnS	schválit
pro	pro	k7c4	pro
přívržence	přívrženec	k1gMnSc4	přívrženec
nového	nový	k2eAgInSc2d1	nový
útvaru	útvar	k1gInSc2	útvar
červeno-žluto-černá	červeno-žluto-černý	k2eAgFnSc1d1	červeno-žluto-černý
kokarda	kokarda	k1gFnSc1	kokarda
s	s	k7c7	s
iniciály	iniciála	k1gFnPc1	iniciála
F	F	kA	F
VII	VII	kA	VII
<g/>
.	.	kIx.	.
nebo	nebo	k8xC	nebo
s	s	k7c7	s
portrétem	portrét	k1gInSc7	portrét
krále	král	k1gMnSc2	král
nad	nad	k7c7	nad
zkratkou	zkratka	k1gFnSc7	zkratka
FDO	FDO	kA	FDO
VII	VII	kA	VII
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
kokard	kokarda	k1gFnPc2	kokarda
byly	být	k5eAaImAgInP	být
později	pozdě	k6eAd2	pozdě
odvozeny	odvozen	k2eAgFnPc1d1	odvozena
vlajky	vlajka	k1gFnPc1	vlajka
(	(	kIx(	(
<g/>
obrázek	obrázek	k1gInSc1	obrázek
druhé	druhý	k4xOgFnSc2	druhý
však	však	k9	však
není	být	k5eNaImIp3nS	být
k	k	k7c3	k
dispozici	dispozice	k1gFnSc3	dispozice
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
O	o	k7c4	o
úplnou	úplný	k2eAgFnSc4d1	úplná
nezávislost	nezávislost	k1gFnSc4	nezávislost
se	se	k3xPyFc4	se
snažila	snažit	k5eAaImAgFnS	snažit
tzv.	tzv.	kA	tzv.
Vlastenecká	vlastenecký	k2eAgFnSc1d1	vlastenecká
společnost	společnost	k1gFnSc1	společnost
<g/>
,	,	kIx,	,
vedená	vedený	k2eAgFnSc1d1	vedená
Francisco	Francisco	k6eAd1	Francisco
de	de	k?	de
Mirandou	Miranda	k1gFnSc7	Miranda
a	a	k8xC	a
Simónem	Simón	k1gMnSc7	Simón
Bolívarem	Bolívar	k1gInSc7	Bolívar
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
jejich	jejich	k3xOp3gInSc7	jejich
vlivem	vliv	k1gInSc7	vliv
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
15	[number]	k4	15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1811	[number]	k4	1811
junta	junta	k1gFnSc1	junta
v	v	k7c6	v
Caracasu	Caracas	k1gInSc6	Caracas
Venezuelskou	venezuelský	k2eAgFnSc4d1	venezuelská
republiku	republika	k1gFnSc4	republika
<g/>
,	,	kIx,	,
kterou	který	k3yQgFnSc4	který
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
oficiálně	oficiálně	k6eAd1	oficiálně
potvrdil	potvrdit	k5eAaPmAgInS	potvrdit
Národní	národní	k2eAgInSc1d1	národní
kongres	kongres	k1gInSc1	kongres
pod	pod	k7c7	pod
názvem	název	k1gInSc7	název
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
venezuelské	venezuelský	k2eAgInPc4d1	venezuelský
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
</s>
<s>
Venezuelská	venezuelský	k2eAgFnSc1d1	venezuelská
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
jmenoval	jmenovat	k5eAaBmAgInS	jmenovat
kongres	kongres	k1gInSc4	kongres
komisi	komise	k1gFnSc4	komise
pro	pro	k7c4	pro
návrh	návrh	k1gInSc4	návrh
nové	nový	k2eAgFnSc2d1	nová
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
,	,	kIx,	,
ve	v	k7c6	v
složení	složení	k1gNnSc6	složení
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
generálporučík	generálporučík	k1gMnSc1	generálporučík
Francisco	Francisco	k1gMnSc1	Francisco
de	de	k?	de
Miranda	Miranda	k1gFnSc1	Miranda
</s>
</p>
<p>
<s>
fregatní	fregatní	k2eAgMnSc1d1	fregatní
kapitán	kapitán	k1gMnSc1	kapitán
Lido	Lido	k1gNnSc1	Lido
de	de	k?	de
Clemente	Clement	k1gMnSc5	Clement
</s>
</p>
<p>
<s>
kapitán	kapitán	k1gMnSc1	kapitán
ženijních	ženijní	k2eAgFnPc2d1	ženijní
vojsk	vojsko	k1gNnPc2	vojsko
José	Josá	k1gFnSc2	Josá
Sata	Sata	k1gFnSc1	Sata
y	y	k?	y
BussiKomise	BussiKomise	k1gFnSc1	BussiKomise
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
okamžitě	okamžitě	k6eAd1	okamžitě
přijata	přijmout	k5eAaPmNgFnS	přijmout
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
vlajka	vlajka	k1gFnSc1	vlajka
s	s	k7c7	s
třemi	tři	k4xCgInPc7	tři
vodorovnými	vodorovný	k2eAgInPc7d1	vodorovný
pruhy	pruh	k1gInPc7	pruh
<g/>
:	:	kIx,	:
žlutým	žlutý	k2eAgInSc7d1	žlutý
<g/>
,	,	kIx,	,
modrým	modrý	k2eAgInSc7d1	modrý
a	a	k8xC	a
červeným	červený	k2eAgInSc7d1	červený
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
šířek	šířka	k1gFnPc2	šířka
2	[number]	k4	2
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
doplněná	doplněný	k2eAgFnSc1d1	doplněná
ale	ale	k9	ale
o	o	k7c4	o
kanton	kanton	k1gInSc4	kanton
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
Caracasu	Caracas	k1gInSc6	Caracas
vyvěšena	vyvěšen	k2eAgFnSc1d1	vyvěšena
14	[number]	k4	14
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1811	[number]	k4	1811
při	při	k7c6	při
slavnostním	slavnostní	k2eAgNnSc6d1	slavnostní
vyhlášení	vyhlášení	k1gNnSc6	vyhlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
kantonu	kanton	k1gInSc6	kanton
byla	být	k5eAaImAgFnS	být
vyobrazena	vyobrazen	k2eAgFnSc1d1	vyobrazena
indiánka	indiánka	k1gFnSc1	indiánka
s	s	k7c7	s
kopím	kopí	k1gNnSc7	kopí
v	v	k7c6	v
ruce	ruka	k1gFnSc6	ruka
<g/>
,	,	kIx,	,
sedící	sedící	k2eAgFnSc6d1	sedící
na	na	k7c6	na
kameni	kámen	k1gInSc6	kámen
na	na	k7c6	na
pobřeží	pobřeží	k1gNnSc6	pobřeží
s	s	k7c7	s
vycházejícím	vycházející	k2eAgNnSc7d1	vycházející
sluncem	slunce	k1gNnSc7	slunce
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
špici	špice	k1gFnSc6	špice
kopí	kopí	k1gNnSc2	kopí
byla	být	k5eAaImAgFnS	být
frygická	frygický	k2eAgFnSc1d1	frygická
čapka	čapka	k1gFnSc1	čapka
<g/>
,	,	kIx,	,
v	v	k7c6	v
horním	horní	k2eAgInSc6d1	horní
rohu	roh	k1gInSc6	roh
byla	být	k5eAaImAgFnS	být
duha	duha	k1gFnSc1	duha
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
VENEZUELA	Venezuela	k1gFnSc1	Venezuela
LIBRE	LIBRE	kA	LIBRE
(	(	kIx(	(
<g/>
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
Venezuela	Venezuela	k1gFnSc1	Venezuela
<g/>
)	)	kIx)	)
a	a	k8xC	a
u	u	k7c2	u
nohou	noha	k1gFnPc2	noha
indiánky	indiánka	k1gFnSc2	indiánka
stuha	stuha	k1gFnSc1	stuha
s	s	k7c7	s
nápisem	nápis	k1gInSc7	nápis
COLOMBIA	COLOMBIA	kA	COLOMBIA
<g/>
.	.	kIx.	.
</s>
<s>
Indiánka	Indiánka	k1gFnSc1	Indiánka
symbolizovala	symbolizovat	k5eAaImAgFnS	symbolizovat
americkou	americký	k2eAgFnSc4d1	americká
svobodu	svoboda	k1gFnSc4	svoboda
<g/>
,	,	kIx,	,
frygická	frygický	k2eAgFnSc1d1	frygická
čapka	čapka	k1gFnSc1	čapka
republikánské	republikánský	k2eAgNnSc1d1	republikánské
zřízení	zřízení	k1gNnSc1	zřízení
<g/>
,	,	kIx,	,
nápisy	nápis	k1gInPc1	nápis
souvisí	souviset	k5eAaImIp3nP	souviset
s	s	k7c7	s
osvobozením	osvobození	k1gNnSc7	osvobození
od	od	k7c2	od
španělské	španělský	k2eAgFnSc2d1	španělská
nadvlády	nadvláda	k1gFnSc2	nadvláda
a	a	k8xC	a
s	s	k7c7	s
Kryštofem	Kryštof	k1gMnSc7	Kryštof
Kolumbem	Kolumbus	k1gMnSc7	Kolumbus
<g/>
.25	.25	k4	.25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1812	[number]	k4	1812
byla	být	k5eAaImAgFnS	být
republikánská	republikánský	k2eAgFnSc1d1	republikánská
armáda	armáda	k1gFnSc1	armáda
po	po	k7c6	po
ofenzívě	ofenzíva	k1gFnSc6	ofenzíva
španělské	španělský	k2eAgFnSc6d1	španělská
poražena	poražen	k2eAgFnSc1d1	poražena
a	a	k8xC	a
Miranda	Miranda	k1gFnSc1	Miranda
byl	být	k5eAaImAgInS	být
zajat	zajat	k2eAgMnSc1d1	zajat
(	(	kIx(	(
<g/>
zemřel	zemřít	k5eAaPmAgMnS	zemřít
roku	rok	k1gInSc2	rok
1816	[number]	k4	1816
ve	v	k7c6	v
vězení	vězení	k1gNnSc6	vězení
v	v	k7c6	v
Cádizu	Cádiz	k1gInSc6	Cádiz
ve	v	k7c6	v
Španělsku	Španělsko	k1gNnSc6	Španělsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Bolívar	Bolívar	k1gInSc1	Bolívar
odešel	odejít	k5eAaPmAgInS	odejít
do	do	k7c2	do
Bogoty	Bogota	k1gFnSc2	Bogota
<g/>
,	,	kIx,	,
<g/>
kde	kde	k6eAd1	kde
přesvědčil	přesvědčit	k5eAaPmAgMnS	přesvědčit
představitele	představitel	k1gMnPc4	představitel
Cundinamarky	Cundinamarka	k1gFnSc2	Cundinamarka
a	a	k8xC	a
Konfederace	konfederace	k1gFnSc2	konfederace
Nové	Nové	k2eAgFnSc2d1	Nové
Granady	Granada	k1gFnSc2	Granada
o	o	k7c6	o
protišpanělském	protišpanělský	k2eAgNnSc6d1	protišpanělský
povstání	povstání	k1gNnSc6	povstání
<g/>
.	.	kIx.	.
6	[number]	k4	6
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
1813	[number]	k4	1813
dobyl	dobýt	k5eAaPmAgInS	dobýt
Caracas	Caracas	k1gInSc1	Caracas
a	a	k8xC	a
8	[number]	k4	8
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
byla	být	k5eAaImAgFnS	být
ustanovena	ustanoven	k2eAgFnSc1d1	ustanovena
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
Venezuelská	venezuelský	k2eAgFnSc1d1	venezuelská
republika	republika	k1gFnSc1	republika
<g/>
.	.	kIx.	.
</s>
<s>
Symbolem	symbol	k1gInSc7	symbol
státu	stát	k1gInSc2	stát
se	se	k3xPyFc4	se
opětovně	opětovně	k6eAd1	opětovně
stala	stát	k5eAaPmAgFnS	stát
vlajka	vlajka	k1gFnSc1	vlajka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1811.9	[number]	k4	1811.9
<g/>
.	.	kIx.	.
září	září	k1gNnSc4	září
1814	[number]	k4	1814
byl	být	k5eAaImAgInS	být
kanton	kanton	k1gInSc1	kanton
s	s	k7c7	s
indiánkou	indiánka	k1gFnSc7	indiánka
z	z	k7c2	z
vlajky	vlajka	k1gFnSc2	vlajka
odstraněn	odstraněn	k2eAgMnSc1d1	odstraněn
a	a	k8xC	a
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
bylo	být	k5eAaImAgNnS	být
doprostřed	doprostřed	k7c2	doprostřed
žlutého	žlutý	k2eAgInSc2d1	žlutý
pruhu	pruh	k1gInSc2	pruh
umístěno	umístěn	k2eAgNnSc1d1	umístěno
vedle	vedle	k6eAd1	vedle
sebe	sebe	k3xPyFc4	sebe
sedm	sedm	k4xCc4	sedm
modrých	modrý	k2eAgFnPc2d1	modrá
<g/>
,	,	kIx,	,
pěticípých	pěticípý	k2eAgFnPc2d1	pěticípá
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
.	.	kIx.	.
</s>
<s>
Ještě	ještě	k9	ještě
v	v	k7c6	v
témže	týž	k3xTgInSc6	týž
roce	rok	k1gInSc6	rok
bylo	být	k5eAaImAgNnS	být
povstání	povstání	k1gNnSc1	povstání
poraženo	porazit	k5eAaPmNgNnS	porazit
a	a	k8xC	a
Bolívar	Bolívar	k1gInSc1	Bolívar
byl	být	k5eAaImAgInS	být
poslán	poslat	k5eAaPmNgInS	poslat
do	do	k7c2	do
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
symbolizovaly	symbolizovat	k5eAaImAgFnP	symbolizovat
sedm	sedm	k4xCc4	sedm
provincií	provincie	k1gFnPc2	provincie
konfederace	konfederace	k1gFnSc2	konfederace
(	(	kIx(	(
<g/>
Barcelona	Barcelona	k1gFnSc1	Barcelona
<g/>
,	,	kIx,	,
Barinas	Barinas	k1gInSc1	Barinas
<g/>
,	,	kIx,	,
Caracas	Caracas	k1gInSc1	Caracas
<g/>
,	,	kIx,	,
Cumaná	Cumaná	k1gFnSc1	Cumaná
<g/>
,	,	kIx,	,
Margarita	Margarita	k1gFnSc1	Margarita
<g/>
,	,	kIx,	,
Mérida	Mérida	k1gFnSc1	Mérida
a	a	k8xC	a
Trujillo	Trujillo	k1gNnSc1	Trujillo
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
1816	[number]	k4	1816
se	se	k3xPyFc4	se
se	s	k7c7	s
svým	svůj	k3xOyFgInSc7	svůj
expedičním	expediční	k2eAgInSc7d1	expediční
sborem	sbor	k1gInSc7	sbor
uchytil	uchytit	k5eAaPmAgMnS	uchytit
Bolívar	Bolívar	k1gInSc4	Bolívar
v	v	k7c6	v
deltě	delta	k1gFnSc6	delta
Orinoka	Orinoko	k1gNnSc2	Orinoko
<g/>
,	,	kIx,	,
za	za	k7c2	za
pomoci	pomoc	k1gFnSc2	pomoc
Britů	Brit	k1gMnPc2	Brit
rozšířil	rozšířit	k5eAaPmAgMnS	rozšířit
osvobozené	osvobozený	k2eAgNnSc4d1	osvobozené
území	území	k1gNnSc4	území
a	a	k8xC	a
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1918	[number]	k4	1918
vyhlásil	vyhlásit	k5eAaPmAgInS	vyhlásit
obnovení	obnovení	k1gNnSc4	obnovení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
12	[number]	k4	12
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1817	[number]	k4	1817
přijala	přijmout	k5eAaPmAgFnS	přijmout
federální	federální	k2eAgFnSc1d1	federální
vláda	vláda	k1gFnSc1	vláda
vlajku	vlajka	k1gFnSc4	vlajka
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1814	[number]	k4	1814
<g/>
.	.	kIx.	.
20	[number]	k4	20
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
1917	[number]	k4	1917
přidal	přidat	k5eAaPmAgInS	přidat
Bolívar	Bolívar	k1gInSc1	Bolívar
svým	svůj	k3xOyFgInSc7	svůj
dekretem	dekret	k1gInSc7	dekret
na	na	k7c4	na
vlajku	vlajka	k1gFnSc4	vlajka
osmou	osmý	k4xOgFnSc4	osmý
hvězdu	hvězda	k1gFnSc4	hvězda
symbolizující	symbolizující	k2eAgFnSc4d1	symbolizující
Guyanu	Guyana	k1gFnSc4	Guyana
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Bolívarova	Bolívarův	k2eAgFnSc1d1	Bolívarova
armáda	armáda	k1gFnSc1	armáda
(	(	kIx(	(
<g/>
používající	používající	k2eAgFnSc4d1	používající
nejčastěji	často	k6eAd3	často
Mirandovu	Mirandův	k2eAgFnSc4d1	Mirandův
trikolóru	trikolóra	k1gFnSc4	trikolóra
<g/>
,	,	kIx,	,
nazývanou	nazývaný	k2eAgFnSc4d1	nazývaná
bolívariana	bolívarian	k1gMnSc2	bolívarian
<g/>
)	)	kIx)	)
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1819	[number]	k4	1819
osvobodila	osvobodit	k5eAaPmAgFnS	osvobodit
většinu	většina	k1gFnSc4	většina
území	území	k1gNnSc2	území
Místokrálovství	Místokrálovství	k1gNnSc2	Místokrálovství
Nová	Nová	k1gFnSc1	Nová
Granada	Granada	k1gFnSc1	Granada
<g/>
.	.	kIx.	.
17	[number]	k4	17
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
1819	[number]	k4	1819
byla	být	k5eAaImAgFnS	být
vyhlášena	vyhlásit	k5eAaPmNgFnS	vyhlásit
Velká	velký	k2eAgFnSc1d1	velká
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
<g/>
,	,	kIx,	,
ústava	ústava	k1gFnSc1	ústava
přijatá	přijatý	k2eAgFnSc1d1	přijatá
25	[number]	k4	25
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
určovala	určovat	k5eAaImAgFnS	určovat
vlajku	vlajka	k1gFnSc4	vlajka
federace	federace	k1gFnSc2	federace
jako	jako	k8xS	jako
žluto-modro-červená	žlutoodro-červený	k2eAgFnSc1d1	žluto-modro-červený
trikolóra	trikolóra	k1gFnSc1	trikolóra
s	s	k7c7	s
poměrem	poměr	k1gInSc7	poměr
pruhů	pruh	k1gInPc2	pruh
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
6.6	[number]	k4	6.6
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1821	[number]	k4	1821
zavedl	zavést	k5eAaPmAgInS	zavést
Generální	generální	k2eAgInSc1d1	generální
kongres	kongres	k1gInSc1	kongres
Velké	velký	k2eAgFnSc2d1	velká
Kolumbie	Kolumbie	k1gFnSc2	Kolumbie
novou	nový	k2eAgFnSc4d1	nová
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Byl	být	k5eAaImAgInS	být
to	ten	k3xDgNnSc1	ten
stejný	stejný	k2eAgInSc1d1	stejný
list	list	k1gInSc1	list
jako	jako	k8xC	jako
v	v	k7c6	v
předchozí	předchozí	k2eAgFnSc6d1	předchozí
vlajce	vlajka	k1gFnSc6	vlajka
<g/>
,	,	kIx,	,
doplněný	doplněný	k2eAgInSc1d1	doplněný
o	o	k7c4	o
nový	nový	k2eAgInSc4d1	nový
státní	státní	k2eAgInSc4d1	státní
znak	znak	k1gInSc4	znak
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
některých	některý	k3yIgInPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
byl	být	k5eAaImAgInS	být
znak	znak	k1gInSc1	znak
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
v	v	k7c6	v
oválném	oválný	k2eAgNnSc6d1	oválné
mezikruží	mezikruží	k1gNnSc6	mezikruží
<g/>
,	,	kIx,	,
v	v	k7c6	v
jehož	jehož	k3xOyRp3gFnSc6	jehož
horní	horní	k2eAgFnSc6d1	horní
části	část	k1gFnSc6	část
byl	být	k5eAaImAgInS	být
španělský	španělský	k2eAgInSc1d1	španělský
název	název	k1gInSc1	název
státu	stát	k1gInSc2	stát
REPÚBLICA	REPÚBLICA	kA	REPÚBLICA
DE	DE	k?	DE
COLOMBIA	COLOMBIA	kA	COLOMBIA
a	a	k8xC	a
dole	dole	k6eAd1	dole
malá	malý	k2eAgFnSc1d1	malá
<g/>
,	,	kIx,	,
černá	černý	k2eAgFnSc1d1	černá
<g/>
,	,	kIx,	,
pěticípá	pěticípý	k2eAgFnSc1d1	pěticípá
hvězda	hvězda	k1gFnSc1	hvězda
<g/>
.	.	kIx.	.
<g/>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Bolívara	Bolívar	k1gMnSc4	Bolívar
a	a	k8xC	a
pod	pod	k7c7	pod
vlivem	vliv	k1gInSc7	vliv
separatistických	separatistický	k2eAgFnPc2d1	separatistická
tendencí	tendence	k1gFnPc2	tendence
se	se	k3xPyFc4	se
Velká	velký	k2eAgFnSc1d1	velká
Kolumbie	Kolumbie	k1gFnSc1	Kolumbie
rozpadla	rozpadnout	k5eAaPmAgFnS	rozpadnout
a	a	k8xC	a
6	[number]	k4	6
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
1930	[number]	k4	1930
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
José	José	k1gNnSc4	José
Antonio	Antonio	k1gMnSc1	Antonio
Páez	Páez	k1gMnSc1	Páez
<g/>
,	,	kIx,	,
vůdce	vůdce	k1gMnSc1	vůdce
separatistů	separatista	k1gMnPc2	separatista
<g/>
,	,	kIx,	,
nezávislost	nezávislost	k1gFnSc4	nezávislost
Venezuelské	venezuelský	k2eAgFnSc2d1	venezuelská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
</s>
<s>
Vlajkou	vlajka	k1gFnSc7	vlajka
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
opět	opět	k6eAd1	opět
trikolóra	trikolóra	k1gFnSc1	trikolóra
o	o	k7c6	o
poměru	poměr	k1gInSc6	poměr
šířek	šířka	k1gFnPc2	šířka
pruhů	pruh	k1gInPc2	pruh
8	[number]	k4	8
<g/>
:	:	kIx,	:
<g/>
7	[number]	k4	7
<g/>
:	:	kIx,	:
<g/>
6	[number]	k4	6
s	s	k7c7	s
provizorním	provizorní	k2eAgInSc7d1	provizorní
znakem	znak	k1gInSc7	znak
uprostřed	uprostřed	k7c2	uprostřed
listu	list	k1gInSc2	list
(	(	kIx(	(
<g/>
pod	pod	k7c7	pod
znakem	znak	k1gInSc7	znak
byl	být	k5eAaImAgInS	být
původně	původně	k6eAd1	původně
nápis	nápis	k1gInSc1	nápis
ESTADO	ESTADO	kA	ESTADO
DE	DE	k?	DE
VENEZUELA	Venezuela	k1gFnSc1	Venezuela
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Stát	stát	k1gInSc1	stát
Venezuela	Venezuela	k1gFnSc1	Venezuela
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
kdy	kdy	k6eAd1	kdy
<g/>
)	)	kIx)	)
REPÚBLICA	REPÚBLICA	kA	REPÚBLICA
DE	DE	k?	DE
VENEZUELA	Venezuela	k1gFnSc1	Venezuela
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Venezuelská	venezuelský	k2eAgFnSc1d1	venezuelská
republika	republika	k1gFnSc1	republika
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
<g/>
.20	.20	k4	.20
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
1836	[number]	k4	1836
byly	být	k5eAaImAgFnP	být
dekretem	dekret	k1gInSc7	dekret
č.	č.	k?	č.
1353	[number]	k4	1353
zavedeny	zaveden	k2eAgInPc4d1	zaveden
nové	nový	k2eAgInPc4d1	nový
státní	státní	k2eAgInPc4d1	státní
symboly	symbol	k1gInPc4	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Žluto-modro-červená	Žlutoodro-červený	k2eAgFnSc1d1	Žluto-modro-červený
trikolóra	trikolóra	k1gFnSc1	trikolóra
se	s	k7c7	s
stejně	stejně	k6eAd1	stejně
širokými	široký	k2eAgInPc7d1	široký
<g/>
,	,	kIx,	,
vodorovnými	vodorovný	k2eAgInPc7d1	vodorovný
pruhy	pruh	k1gInPc7	pruh
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
válečných	válečný	k2eAgFnPc6d1	válečná
lodích	loď	k1gFnPc6	loď
<g/>
,	,	kIx,	,
pevnostech	pevnost	k1gFnPc6	pevnost
<g/>
,	,	kIx,	,
veřejných	veřejný	k2eAgNnPc6d1	veřejné
místech	místo	k1gNnPc6	místo
a	a	k8xC	a
při	při	k7c6	při
reprezentaci	reprezentace	k1gFnSc6	reprezentace
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
doplněna	doplnit	k5eAaPmNgFnS	doplnit
o	o	k7c4	o
nový	nový	k2eAgInSc4d1	nový
státní	státní	k2eAgInSc4d1	státní
znak	znak	k1gInSc4	znak
v	v	k7c6	v
žerďovém	žerďový	k2eAgInSc6d1	žerďový
rohu	roh	k1gInSc6	roh
žlutého	žlutý	k2eAgInSc2d1	žlutý
pruhu	pruh	k1gInSc2	pruh
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1856	[number]	k4	1856
se	se	k3xPyFc4	se
znak	znak	k1gInSc1	znak
(	(	kIx(	(
<g/>
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k8xC	i
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
)	)	kIx)	)
rozhodnutím	rozhodnutí	k1gNnSc7	rozhodnutí
prezidenta	prezident	k1gMnSc2	prezident
José	Josá	k1gFnSc2	Josá
Tadea	Tadea	k1gFnSc1	Tadea
Monagase	Monagasa	k1gFnSc3	Monagasa
mírně	mírně	k6eAd1	mírně
změnil	změnit	k5eAaPmAgMnS	změnit
<g/>
:	:	kIx,	:
zobrazený	zobrazený	k2eAgMnSc1d1	zobrazený
kůň	kůň	k1gMnSc1	kůň
běžel	běžet	k5eAaImAgMnS	běžet
tryskem	tryskem	k6eAd1	tryskem
a	a	k8xC	a
ohlížel	ohlížet	k5eAaImAgInS	ohlížet
se	se	k3xPyFc4	se
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
Při	při	k7c6	při
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
proti	proti	k7c3	proti
unitárnímu	unitární	k2eAgNnSc3d1	unitární
uspořádání	uspořádání	k1gNnSc3	uspořádání
Venezuely	Venezuela	k1gFnSc2	Venezuela
vyhlásili	vyhlásit	k5eAaPmAgMnP	vyhlásit
federalisté	federalista	k1gMnPc1	federalista
20	[number]	k4	20
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
venezuelské	venezuelský	k2eAgInPc4d1	venezuelský
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1859	[number]	k4	1859
vyhlásila	vyhlásit	k5eAaPmAgFnS	vyhlásit
provizorní	provizorní	k2eAgFnSc1d1	provizorní
vláda	vláda	k1gFnSc1	vláda
sídlící	sídlící	k2eAgFnSc1d1	sídlící
v	v	k7c4	v
Coro	Coro	k6eAd1	Coro
novou	nový	k2eAgFnSc4d1	nová
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
tradiční	tradiční	k2eAgFnSc1d1	tradiční
trikolóra	trikolóra	k1gFnSc1	trikolóra
byla	být	k5eAaImAgFnS	být
ve	v	k7c6	v
žlutém	žlutý	k2eAgInSc6d1	žlutý
pruhu	pruh	k1gInSc6	pruh
doplněna	doplnit	k5eAaPmNgFnS	doplnit
o	o	k7c4	o
sedm	sedm	k4xCc4	sedm
modrých	modrý	k2eAgFnPc2d1	modrá
hvězd	hvězda	k1gFnPc2	hvězda
v	v	k7c6	v
řadě	řada	k1gFnSc6	řada
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
hvězd	hvězda	k1gFnPc2	hvězda
symbolizoval	symbolizovat	k5eAaImAgInS	symbolizovat
provincie	provincie	k1gFnPc4	provincie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1811	[number]	k4	1811
vytvořily	vytvořit	k5eAaPmAgFnP	vytvořit
venezuelskou	venezuelský	k2eAgFnSc4d1	venezuelská
federaci	federace	k1gFnSc4	federace
<g/>
.	.	kIx.	.
<g/>
Dle	dle	k7c2	dle
jiných	jiný	k2eAgInPc2d1	jiný
pramenů	pramen	k1gInPc2	pramen
byly	být	k5eAaImAgFnP	být
hvězdy	hvězda	k1gFnPc1	hvězda
uspořádány	uspořádat	k5eAaPmNgFnP	uspořádat
do	do	k7c2	do
kruhu	kruh	k1gInSc2	kruh
v	v	k7c6	v
žerďovém	žerďový	k2eAgInSc6d1	žerďový
rohu	roh	k1gInSc6	roh
(	(	kIx(	(
<g/>
šest	šest	k4xCc1	šest
hvězd	hvězda	k1gFnPc2	hvězda
<g/>
)	)	kIx)	)
se	s	k7c7	s
sedmou	sedma	k1gFnSc7	sedma
uprostřed	uprostřed	k7c2	uprostřed
tohoto	tento	k3xDgInSc2	tento
kruhu	kruh	k1gInSc2	kruh
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
červnu	červen	k1gInSc6	červen
roku	rok	k1gInSc2	rok
1859	[number]	k4	1859
změnil	změnit	k5eAaPmAgMnS	změnit
generál	generál	k1gMnSc1	generál
Ezequiel	Ezequiel	k1gMnSc1	Ezequiel
Zamora	Zamora	k1gFnSc1	Zamora
počet	počet	k1gInSc1	počet
hvězd	hvězda	k1gFnPc2	hvězda
na	na	k7c4	na
20	[number]	k4	20
<g/>
,	,	kIx,	,
uspořádaných	uspořádaný	k2eAgFnPc2d1	uspořádaná
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
řad	řada	k1gFnPc2	řada
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
<s>
Po	po	k7c6	po
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
(	(	kIx(	(
<g/>
při	při	k7c6	při
které	který	k3yRgFnSc6	který
zůstávala	zůstávat	k5eAaImAgFnS	zůstávat
centralistická	centralistický	k2eAgFnSc1d1	centralistická
vláda	vláda	k1gFnSc1	vláda
u	u	k7c2	u
symbolů	symbol	k1gInPc2	symbol
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1836	[number]	k4	1836
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
trvající	trvající	k2eAgInSc1d1	trvající
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1863	[number]	k4	1863
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
prezidentem	prezident	k1gMnSc7	prezident
Juan	Juan	k1gMnSc1	Juan
Crisóstomo	Crisóstoma	k1gFnSc5	Crisóstoma
Falcón	Falcón	k1gInSc1	Falcón
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgInSc1	ten
zavedl	zavést	k5eAaPmAgInS	zavést
29	[number]	k4	29
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1863	[number]	k4	1863
(	(	kIx(	(
<g/>
dekretem	dekret	k1gInSc7	dekret
č.	č.	k?	č.
1352	[number]	k4	1352
<g/>
)	)	kIx)	)
novou	nový	k2eAgFnSc4d1	nová
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
modrého	modrý	k2eAgInSc2d1	modrý
pruhu	pruh	k1gInSc2	pruh
tradiční	tradiční	k2eAgFnSc2d1	tradiční
trikolóry	trikolóra	k1gFnSc2	trikolóra
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
variantě	varianta	k1gFnSc6	varianta
přidán	přidán	k2eAgInSc4d1	přidán
kruh	kruh	k1gInSc4	kruh
šesti	šest	k4xCc2	šest
hvězd	hvězda	k1gFnPc2	hvězda
se	se	k3xPyFc4	se
sedmou	sedma	k1gFnSc7	sedma
uprostřed	uprostřed	k6eAd1	uprostřed
<g/>
.	.	kIx.	.
</s>
<s>
Ústava	ústava	k1gFnSc1	ústava
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
vlajku	vlajka	k1gFnSc4	vlajka
a	a	k8xC	a
název	název	k1gInSc4	název
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
venezuelské	venezuelský	k2eAgInPc1d1	venezuelský
zavedla	zavést	k5eAaPmAgFnS	zavést
<g/>
/	/	kIx~	/
<g/>
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
schválena	schválit	k5eAaPmNgFnS	schválit
až	až	k6eAd1	až
28	[number]	k4	28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1864	[number]	k4	1864
<g/>
.	.	kIx.	.
</s>
<s>
Zobrazená	zobrazený	k2eAgFnSc1d1	zobrazená
vlajka	vlajka	k1gFnSc1	vlajka
má	mít	k5eAaImIp3nS	mít
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
zdroji	zdroj	k1gInSc3	zdroj
<g/>
,	,	kIx,	,
hvězdy	hvězda	k1gFnPc4	hvězda
uspořádány	uspořádán	k2eAgFnPc1d1	uspořádána
odlišně	odlišně	k6eAd1	odlišně
<g/>
.	.	kIx.	.
<g/>
Stejná	stejný	k2eAgFnSc1d1	stejná
vlajka	vlajka	k1gFnSc1	vlajka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
se	s	k7c7	s
státním	státní	k2eAgInSc7d1	státní
znakem	znak	k1gInSc7	znak
v	v	k7c6	v
žerďovém	žerďový	k2eAgInSc6d1	žerďový
rohu	roh	k1gInSc6	roh
žlutého	žlutý	k2eAgInSc2d1	žlutý
pruhu	pruh	k1gInSc2	pruh
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
označována	označovat	k5eAaImNgFnS	označovat
za	za	k7c4	za
vlajku	vlajka	k1gFnSc4	vlajka
válečnou	válečná	k1gFnSc4	válečná
<g/>
.28	.28	k4	.28
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
1905	[number]	k4	1905
vydal	vydat	k5eAaPmAgMnS	vydat
prezident	prezident	k1gMnSc1	prezident
José	Josá	k1gFnSc2	Josá
Cipriano	Cipriana	k1gFnSc5	Cipriana
Castro	Castro	k1gNnSc1	Castro
dekret	dekret	k1gInSc4	dekret
<g/>
,	,	kIx,	,
kterým	který	k3yIgInSc7	který
<g />
.	.	kIx.	.
</s>
<s>
se	se	k3xPyFc4	se
změnila	změnit	k5eAaPmAgFnS	změnit
dosud	dosud	k6eAd1	dosud
užívaná	užívaný	k2eAgFnSc1d1	užívaná
vlajka	vlajka	k1gFnSc1	vlajka
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
že	že	k8xS	že
všech	všecek	k3xTgFnPc2	všecek
sedm	sedm	k4xCc1	sedm
hvězd	hvězda	k1gFnPc2	hvězda
bylo	být	k5eAaImAgNnS	být
uspořádáno	uspořádat	k5eAaPmNgNnS	uspořádat
do	do	k7c2	do
kruhu	kruh	k1gInSc2	kruh
a	a	k8xC	a
změnil	změnit	k5eAaPmAgInS	změnit
též	též	k9	též
státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
<g/>
.	.	kIx.	.
<g/>
Dekret	dekret	k1gInSc1	dekret
zároveň	zároveň	k6eAd1	zároveň
vymezil	vymezit	k5eAaPmAgInS	vymezit
v	v	k7c6	v
článcích	článek	k1gInPc6	článek
2	[number]	k4	2
a	a	k8xC	a
4	[number]	k4	4
užívání	užívání	k1gNnPc2	užívání
vlajky	vlajka	k1gFnSc2	vlajka
se	s	k7c7	s
státním	státní	k2eAgInSc7d1	státní
znakem	znak	k1gInSc7	znak
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
1911	[number]	k4	1911
navrhl	navrhnout	k5eAaPmAgMnS	navrhnout
senátor	senátor	k1gMnSc1	senátor
José	Josý	k2eAgFnSc2d1	Josý
Gil	Gil	k1gFnSc2	Gil
Fortoul	Fortoula	k1gFnPc2	Fortoula
změnu	změna	k1gFnSc4	změna
státního	státní	k2eAgInSc2d1	státní
znaku	znak	k1gInSc2	znak
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
považoval	považovat	k5eAaImAgMnS	považovat
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
kolegy	kolega	k1gMnPc7	kolega
za	za	k7c2	za
heraldicky	heraldicky	k6eAd1	heraldicky
nesprávný	správný	k2eNgMnSc1d1	nesprávný
<g/>
.	.	kIx.	.
</s>
<s>
Návrh	návrh	k1gInSc1	návrh
byl	být	k5eAaImAgInS	být
přijat	přijmout	k5eAaPmNgInS	přijmout
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
nebyl	být	k5eNaImAgInS	být
stanoven	stanovit	k5eAaPmNgInS	stanovit
zákonem	zákon	k1gInSc7	zákon
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
návrh	návrh	k1gInSc1	návrh
projevil	projevit	k5eAaPmAgInS	projevit
i	i	k9	i
na	na	k7c6	na
vlajce	vlajka	k1gFnSc6	vlajka
se	s	k7c7	s
znakem	znak	k1gInSc7	znak
(	(	kIx(	(
<g/>
není	být	k5eNaImIp3nS	být
obrázek	obrázek	k1gInSc1	obrázek
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
15	[number]	k4	15
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1930	[number]	k4	1930
přijal	přijmout	k5eAaPmAgInS	přijmout
venezuelský	venezuelský	k2eAgInSc1d1	venezuelský
Národní	národní	k2eAgInSc1d1	národní
kongres	kongres	k1gInSc1	kongres
první	první	k4xOgInSc4	první
zákon	zákon	k1gInSc4	zákon
o	o	k7c6	o
státních	státní	k2eAgInPc6d1	státní
symbolech	symbol	k1gInPc6	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Sedm	sedm	k4xCc1	sedm
hvězd	hvězda	k1gFnPc2	hvězda
v	v	k7c6	v
modrém	modrý	k2eAgInSc6d1	modrý
pruhu	pruh	k1gInSc6	pruh
bylo	být	k5eAaImAgNnS	být
nově	nově	k6eAd1	nově
uspořádáno	uspořádat	k5eAaPmNgNnS	uspořádat
do	do	k7c2	do
oblouku	oblouk	k1gInSc2	oblouk
<g/>
.	.	kIx.	.
</s>
<s>
Hvězdy	hvězda	k1gFnPc1	hvězda
opět	opět	k6eAd1	opět
symbolizovaly	symbolizovat	k5eAaImAgFnP	symbolizovat
provincie	provincie	k1gFnPc4	provincie
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
5	[number]	k4	5
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
1811	[number]	k4	1811
podepsaly	podepsat	k5eAaPmAgFnP	podepsat
prohlášení	prohlášení	k1gNnSc4	prohlášení
nezávislosti	nezávislost	k1gFnSc2	nezávislost
a	a	k8xC	a
v	v	k7c6	v
zákoně	zákon	k1gInSc6	zákon
byla	být	k5eAaImAgFnS	být
také	také	k9	také
stanovena	stanovit	k5eAaPmNgFnS	stanovit
i	i	k9	i
symbolika	symbolika	k1gFnSc1	symbolika
barev	barva	k1gFnPc2	barva
vlajky	vlajka	k1gFnSc2	vlajka
<g/>
:	:	kIx,	:
žlutá	žlutý	k2eAgFnSc1d1	žlutá
symbolizovala	symbolizovat	k5eAaImAgFnS	symbolizovat
šlechetnost	šlechetnost	k1gFnSc1	šlechetnost
<g/>
,	,	kIx,	,
lásku	láska	k1gFnSc4	láska
<g/>
,	,	kIx,	,
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
svrchovanost	svrchovanost	k1gFnSc4	svrchovanost
a	a	k8xC	a
bohatství	bohatství	k1gNnSc4	bohatství
Venezuely	Venezuela	k1gFnSc2	Venezuela
<g/>
,	,	kIx,	,
modrá	modrý	k2eAgFnSc1d1	modrá
oddanost	oddanost	k1gFnSc1	oddanost
<g/>
,	,	kIx,	,
spravedlnost	spravedlnost	k1gFnSc1	spravedlnost
<g/>
<g />
.	.	kIx.	.
</s>
<s>
,	,	kIx,	,
vytrvalost	vytrvalost	k1gFnSc4	vytrvalost
a	a	k8xC	a
oceán	oceán	k1gInSc4	oceán
oddělující	oddělující	k2eAgFnSc4d1	oddělující
Venezuelu	Venezuela	k1gFnSc4	Venezuela
od	od	k7c2	od
Španělska	Španělsko	k1gNnSc2	Španělsko
a	a	k8xC	a
červená	červený	k2eAgFnSc1d1	červená
barva	barva	k1gFnSc1	barva
představovala	představovat	k5eAaImAgFnS	představovat
čest	čest	k1gFnSc4	čest
<g/>
,	,	kIx,	,
odvahu	odvaha	k1gFnSc4	odvaha
<g/>
,	,	kIx,	,
sílu	síla	k1gFnSc4	síla
<g/>
,	,	kIx,	,
velkolepost	velkolepost	k1gFnSc1	velkolepost
a	a	k8xC	a
krev	krev	k1gFnSc1	krev
bojovníků	bojovník	k1gMnPc2	bojovník
za	za	k7c4	za
osvobození	osvobození	k1gNnSc4	osvobození
Venezuely	Venezuela	k1gFnSc2	Venezuela
<g/>
.	.	kIx.	.
<g/>
Zákon	zákon	k1gInSc1	zákon
také	také	k9	také
stanovil	stanovit	k5eAaPmAgInS	stanovit
použití	použití	k1gNnSc4	použití
vlajky	vlajka	k1gFnSc2	vlajka
se	s	k7c7	s
státním	státní	k2eAgInSc7d1	státní
znakem	znak	k1gInSc7	znak
v	v	k7c6	v
žerďovém	žerďový	k2eAgInSc6d1	žerďový
rohu	roh	k1gInSc6	roh
ve	v	k7c6	v
žlutém	žlutý	k2eAgInSc6d1	žlutý
pruhu	pruh	k1gInSc6	pruh
pro	pro	k7c4	pro
úřední	úřední	k2eAgFnPc4d1	úřední
budovy	budova	k1gFnPc4	budova
<g/>
,	,	kIx,	,
<g />
.	.	kIx.	.
</s>
<s>
válečné	válečný	k2eAgFnPc1d1	válečná
lodě	loď	k1gFnPc1	loď
a	a	k8xC	a
vojenské	vojenský	k2eAgFnPc1d1	vojenská
jednotky	jednotka	k1gFnPc1	jednotka
<g/>
.10	.10	k4	.10
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1954	[number]	k4	1954
upravil	upravit	k5eAaPmAgInS	upravit
parlament	parlament	k1gInSc1	parlament
název	název	k1gInSc1	název
státu	stát	k1gInSc3	stát
na	na	k7c4	na
Venezuelská	venezuelský	k2eAgFnSc1d1	venezuelská
republika	republika	k1gFnSc1	republika
a	a	k8xC	a
zákonem	zákon	k1gInSc7	zákon
o	o	k7c6	o
státních	státní	k2eAgInPc6d1	státní
symbolech	symbol	k1gInPc6	symbol
(	(	kIx(	(
<g/>
Ley	Lea	k1gFnPc1	Lea
de	de	k?	de
Bandera	Bandera	k1gFnSc1	Bandera
<g/>
,	,	kIx,	,
Escudo	escudo	k1gNnSc1	escudo
e	e	k0	e
Himno	Himno	k1gNnSc1	Himno
Nacionales	Nacionales	k1gInSc1	Nacionales
<g/>
)	)	kIx)	)
znak	znak	k1gInSc1	znak
a	a	k8xC	a
tím	ten	k3xDgNnSc7	ten
i	i	k9	i
státní	státní	k2eAgFnSc4d1	státní
vlajku	vlajka	k1gFnSc4	vlajka
<g/>
.13	.13	k4	.13
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1984	[number]	k4	1984
publikovalo	publikovat	k5eAaBmAgNnS	publikovat
ministerstvo	ministerstvo	k1gNnSc1	ministerstvo
vnitra	vnitro	k1gNnSc2	vnitro
novou	nový	k2eAgFnSc4d1	nová
verzi	verze	k1gFnSc4	verze
státního	státní	k2eAgInSc2d1	státní
znaku	znak	k1gInSc2	znak
<g/>
,	,	kIx,	,
uzákoněna	uzákoněn	k2eAgFnSc1d1	uzákoněna
však	však	k9	však
nebyla	být	k5eNaImAgFnS	být
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
2	[number]	k4	2
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
1999	[number]	k4	1999
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
do	do	k7c2	do
prezidentského	prezidentský	k2eAgInSc2d1	prezidentský
úřadu	úřad	k1gInSc2	úřad
Hugo	Hugo	k1gMnSc1	Hugo
Chávez	Chávez	k1gMnSc1	Chávez
<g/>
,	,	kIx,	,
15	[number]	k4	15
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
byl	být	k5eAaImAgInS	být
název	název	k1gInSc1	název
země	zem	k1gFnSc2	zem
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c6	na
Venezuelská	venezuelský	k2eAgFnSc1d1	venezuelská
bolívarovská	bolívarovský	k2eAgFnSc1d1	bolívarovská
republika	republika	k1gFnSc1	republika
a	a	k8xC	a
7	[number]	k4	7
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2006	[number]	k4	2006
schválilo	schválit	k5eAaPmAgNnS	schválit
Národní	národní	k2eAgNnSc1d1	národní
shromáždění	shromáždění	k1gNnSc1	shromáždění
zákon	zákon	k1gInSc1	zákon
č.	č.	k?	č.
423	[number]	k4	423
o	o	k7c6	o
státních	státní	k2eAgInPc6d1	státní
symbolech	symbol	k1gInPc6	symbol
<g/>
.	.	kIx.	.
</s>
<s>
Zákon	zákon	k1gInSc1	zákon
byl	být	k5eAaImAgInS	být
9	[number]	k4	9
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
uveřejněn	uveřejněn	k2eAgMnSc1d1	uveřejněn
ve	v	k7c6	v
věstníku	věstník	k1gInSc6	věstník
č.	č.	k?	č.
38.394	[number]	k4	38.394
a	a	k8xC	a
nabyl	nabýt	k5eAaPmAgInS	nabýt
účinnosti	účinnost	k1gFnSc2	účinnost
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
200	[number]	k4	200
<g/>
.	.	kIx.	.
výročí	výročí	k1gNnSc3	výročí
vztyčení	vztyčení	k1gNnSc2	vztyčení
první	první	k4xOgFnSc2	první
venezuelské	venezuelský	k2eAgFnSc2d1	venezuelská
vlajky	vlajka	k1gFnSc2	vlajka
na	na	k7c6	na
lodi	loď	k1gFnSc6	loď
generála	generál	k1gMnSc2	generál
Mirandy	Miranda	k1gFnSc2	Miranda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Den	den	k1gInSc4	den
vlajky	vlajka	k1gFnSc2	vlajka
==	==	k?	==
</s>
</p>
<p>
<s>
Do	do	k7c2	do
roku	rok	k1gInSc2	rok
2006	[number]	k4	2006
se	se	k3xPyFc4	se
slavil	slavit	k5eAaImAgInS	slavit
den	den	k1gInSc1	den
vlajky	vlajka	k1gFnSc2	vlajka
ve	v	k7c6	v
Venezuele	Venezuela	k1gFnSc6	Venezuela
12	[number]	k4	12
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
vztyčení	vztyčení	k1gNnSc2	vztyčení
první	první	k4xOgInPc1	první
venezuelské	venezuelský	k2eAgInPc1d1	venezuelský
vlaky	vlak	k1gInPc1	vlak
Franciscem	Francisce	k1gNnSc7	Francisce
de	de	k?	de
Mirandou	Miranda	k1gFnSc7	Miranda
na	na	k7c6	na
palubě	paluba	k1gFnSc6	paluba
své	svůj	k3xOyFgFnSc2	svůj
lodi	loď	k1gFnSc2	loď
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byl	být	k5eAaImAgInS	být
tento	tento	k3xDgInSc1	tento
svátek	svátek	k1gInSc1	svátek
změněn	změnit	k5eAaPmNgInS	změnit
na	na	k7c4	na
3	[number]	k4	3
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
<g/>
,	,	kIx,	,
v	v	k7c4	v
den	den	k1gInSc4	den
vztyčení	vztyčení	k1gNnSc2	vztyčení
vlajky	vlajka	k1gFnSc2	vlajka
na	na	k7c6	na
venezuelském	venezuelský	k2eAgNnSc6d1	venezuelské
území	území	k1gNnSc6	území
<g/>
,	,	kIx,	,
na	na	k7c6	na
věži	věž	k1gFnSc6	věž
farního	farní	k2eAgInSc2d1	farní
kostela	kostel	k1gInSc2	kostel
v	v	k7c4	v
Santa	Sant	k1gMnSc4	Sant
Ana	Ana	k1gMnSc4	Ana
de	de	k?	de
Coro	Coro	k1gMnSc1	Coro
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vlajky	vlajka	k1gFnPc4	vlajka
venezuelských	venezuelský	k2eAgInPc2d1	venezuelský
států	stát	k1gInPc2	stát
==	==	k?	==
</s>
</p>
<p>
<s>
Venezuela	Venezuela	k1gFnSc1	Venezuela
je	být	k5eAaImIp3nS	být
federativní	federativní	k2eAgFnSc1d1	federativní
republika	republika	k1gFnSc1	republika
<g/>
,	,	kIx,	,
skládající	skládající	k2eAgInPc4d1	skládající
se	se	k3xPyFc4	se
z	z	k7c2	z
distriktu	distrikt	k1gInSc2	distrikt
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
federálních	federální	k2eAgFnPc2d1	federální
dependencí	dependence	k1gFnPc2	dependence
(	(	kIx(	(
<g/>
12	[number]	k4	12
ostrovů	ostrov	k1gInPc2	ostrov
a	a	k8xC	a
ostrovních	ostrovní	k2eAgFnPc2d1	ostrovní
skupin	skupina	k1gFnPc2	skupina
pod	pod	k7c7	pod
federální	federální	k2eAgFnSc7d1	federální
správou	správa	k1gFnSc7	správa
<g/>
)	)	kIx)	)
a	a	k8xC	a
23	[number]	k4	23
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Státní	státní	k2eAgInSc1d1	státní
znak	znak	k1gInSc1	znak
Venezuely	Venezuela	k1gFnSc2	Venezuela
</s>
</p>
<p>
<s>
Venezuelská	venezuelský	k2eAgFnSc1d1	venezuelská
hymna	hymna	k1gFnSc1	hymna
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Venezuelská	venezuelský	k2eAgFnSc1d1	venezuelská
vlajka	vlajka	k1gFnSc1	vlajka
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
