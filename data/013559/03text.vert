<s>
Etna	Etna	k1gFnSc1
</s>
<s>
Etna	Etna	k1gFnSc1
Etna	Etna	k1gFnSc1
dominuje	dominovat	k5eAaImIp3nS
celé	celý	k2eAgFnSc3d1
východní	východní	k2eAgFnSc3d1
Sicílii	Sicílie	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pohled	pohled	k1gInSc1
na	na	k7c4
sopku	sopka	k1gFnSc4
od	od	k7c2
jihu	jih	k1gInSc2
ze	z	k7c2
zhruba	zhruba	k6eAd1
70	#num#	k4
km	km	kA
vzdáleného	vzdálený	k2eAgNnSc2d1
města	město	k1gNnSc2
Licodia	Licodium	k1gNnSc2
Eubea	Eubea	k1gMnSc1
</s>
<s>
Vrchol	vrchol	k1gInSc1
</s>
<s>
3323	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
(	(	kIx(
<g/>
proměnlivá	proměnlivý	k2eAgFnSc1d1
<g/>
,	,	kIx,
činná	činný	k2eAgFnSc1d1
sopka	sopka	k1gFnSc1
<g/>
)	)	kIx)
Prominence	prominence	k1gFnSc1
</s>
<s>
3323	#num#	k4
m	m	kA
↓	↓	k?
Středozemní	středozemní	k2eAgNnSc4d1
moře	moře	k1gNnSc4
Izolace	izolace	k1gFnSc2
</s>
<s>
998	#num#	k4
km	km	kA
→	→	k?
Marmolada	Marmolada	k1gFnSc1
Seznamy	seznam	k1gInPc4
</s>
<s>
Ultraprominentní	Ultraprominentní	k2eAgFnPc1d1
hory	hora	k1gFnPc1
Evropy	Evropa	k1gFnSc2
#	#	kIx~
<g/>
2	#num#	k4
Poloha	poloha	k1gFnSc1
Světadíl	světadíl	k1gInSc1
</s>
<s>
Evropa	Evropa	k1gFnSc1
Stát	stát	k1gInSc1
</s>
<s>
Itálie	Itálie	k1gFnSc1
Itálie	Itálie	k1gFnSc2
Pohoří	pohořet	k5eAaPmIp3nS
</s>
<s>
Masív	masív	k1gInSc1
Etny	Etna	k1gFnSc2
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
37	#num#	k4
<g/>
°	°	k?
<g/>
45	#num#	k4
<g/>
′	′	k?
<g/>
13	#num#	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
59	#num#	k4
<g/>
′	′	k?
<g/>
34	#num#	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
</s>
<s>
Etna	Etna	k1gFnSc1
</s>
<s>
Typ	typ	k1gInSc1
</s>
<s>
Stratovulkán	stratovulkán	k1gInSc1
Erupce	erupce	k1gFnSc2
</s>
<s>
2021	#num#	k4
(	(	kIx(
<g/>
stále	stále	k6eAd1
činná	činný	k2eAgFnSc1d1
<g/>
)	)	kIx)
Hornina	hornina	k1gFnSc1
</s>
<s>
latit	latit	k1gInSc1
<g/>
,	,	kIx,
tefrický	tefrický	k2eAgInSc1d1
bazanit	bazanit	k1gInSc1
<g/>
,	,	kIx,
čedičový	čedičový	k2eAgInSc1d1
latit	latit	k1gInSc1
<g/>
,	,	kIx,
čedič	čedič	k1gInSc1
<g/>
,	,	kIx,
trachyt	trachyt	k1gInSc1
<g/>
,	,	kIx,
trachydacit	trachydacit	k1gInSc1
<g/>
,	,	kIx,
fonotefrit	fonotefrit	k1gInSc1
<g/>
,	,	kIx,
tefrofonolit	tefrofonolit	k1gInSc1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Etna	Etna	k1gFnSc1
(	(	kIx(
<g/>
starořecky	starořecky	k6eAd1
Α	Α	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Aitné	Aitné	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
latině	latina	k1gFnSc6
Aetna	Aetna	k1gFnSc1
<g/>
,	,	kIx,
také	také	k9
známá	známý	k2eAgNnPc4d1
jako	jako	k8xS,k8xC
Mungibeddu	Mungibedd	k1gInSc2
(	(	kIx(
<g/>
krásná	krásný	k2eAgFnSc1d1
hora	hora	k1gFnSc1
<g/>
)	)	kIx)
v	v	k7c6
sicilštině	sicilština	k1gFnSc6
a	a	k8xC
Mongibello	Mongibello	k1gNnSc1
v	v	k7c6
italštině	italština	k1gFnSc6
(	(	kIx(
<g/>
z	z	k7c2
latinského	latinský	k2eAgInSc2d1
mons	mons	k6eAd1
a	a	k8xC
arabského	arabský	k2eAgInSc2d1
gibel	gibel	k1gFnSc1
<g/>
,	,	kIx,
oboje	oboj	k1gFnPc4
znamená	znamenat	k5eAaImIp3nS
hora	hora	k1gFnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgFnSc1d3
činná	činný	k2eAgFnSc1d1
sopka	sopka	k1gFnSc1
a	a	k8xC
druhá	druhý	k4xOgFnSc1
nejmohutnější	mohutný	k2eAgFnSc1d3
sopka	sopka	k1gFnSc1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nachází	nacházet	k5eAaImIp3nS
se	se	k3xPyFc4
v	v	k7c6
Itálii	Itálie	k1gFnSc6
–	–	k?
na	na	k7c6
východním	východní	k2eAgNnSc6d1
pobřeží	pobřeží	k1gNnSc6
ostrova	ostrov	k1gInSc2
Sicílie	Sicílie	k1gFnSc2
<g/>
,	,	kIx,
nedaleko	nedaleko	k7c2
měst	město	k1gNnPc2
Messina	Messin	k2eAgNnSc2d1
a	a	k8xC
Catania	Catanium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sopka	sopka	k1gFnSc1
byla	být	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
zapsána	zapsat	k5eAaPmNgFnS
mezi	mezi	k7c4
přírodní	přírodní	k2eAgFnPc4d1
památky	památka	k1gFnPc4
Světového	světový	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
UNESCO	UNESCO	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Její	její	k3xOp3gFnSc1
výška	výška	k1gFnSc1
k	k	k7c3
roku	rok	k1gInSc3
2009	#num#	k4
dosahuje	dosahovat	k5eAaImIp3nS
3329	#num#	k4
metrů	metr	k1gInPc2
nad	nad	k7c7
mořem	moře	k1gNnSc7
<g/>
,	,	kIx,
ale	ale	k8xC
ještě	ještě	k9
do	do	k7c2
erupce	erupce	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
1981	#num#	k4
měřila	měřit	k5eAaImAgFnS
o	o	k7c4
21	#num#	k4
metrů	metr	k1gInPc2
více	hodně	k6eAd2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
3350	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následkem	následkem	k7c2
sopečné	sopečný	k2eAgFnSc2d1
erupce	erupce	k1gFnSc2
a	a	k8xC
uvolnění	uvolnění	k1gNnSc1
magmatu	magma	k1gNnSc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
poklesu	pokles	k1gInSc3
sopky	sopka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
nejvyšší	vysoký	k2eAgFnSc4d3
horu	hora	k1gFnSc4
v	v	k7c6
Itálii	Itálie	k1gFnSc6
jižně	jižně	k6eAd1
od	od	k7c2
Alp	Alpy	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Etna	Etna	k1gFnSc1
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
na	na	k7c4
1190	#num#	k4
km²	km²	k?
s	s	k7c7
obvodem	obvod	k1gInSc7
báze	báze	k1gFnSc2
okolo	okolo	k7c2
140	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotná	samotný	k2eAgFnSc1d1
základna	základna	k1gFnSc1
sopky	sopka	k1gFnSc2
je	být	k5eAaImIp3nS
eliptického	eliptický	k2eAgInSc2d1
tvaru	tvar	k1gInSc2
o	o	k7c6
rozměrech	rozměr	k1gInPc6
38	#num#	k4
×	×	k?
47	#num#	k4
km	km	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
tak	tak	k9
o	o	k7c4
největší	veliký	k2eAgFnSc4d3
aktivní	aktivní	k2eAgFnSc4d1
sopku	sopka	k1gFnSc4
v	v	k7c6
Itálii	Itálie	k1gFnSc6
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
je	být	k5eAaImIp3nS
přibližně	přibližně	k6eAd1
dva	dva	k4xCgInPc4
a	a	k8xC
půl	půl	k1xP
krát	krát	k6eAd1
větší	veliký	k2eAgMnSc1d2
než	než	k8xS
druhá	druhý	k4xOgFnSc1
největší	veliký	k2eAgFnSc1d3
sopka	sopka	k1gFnSc1
Vesuv	Vesuv	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Evropě	Evropa	k1gFnSc6
předčí	předčít	k5eAaPmIp3nP,k5eAaBmIp3nP
Etnu	Etna	k1gFnSc4
pouze	pouze	k6eAd1
Pico	Pico	k6eAd1
de	de	k?
Teide	Teid	k1gInSc5
na	na	k7c4
ostrově	ostrov	k1gInSc6
Tenerife	Tenerif	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Platí	platit	k5eAaImIp3nS
to	ten	k3xDgNnSc1
ovšem	ovšem	k9
jen	jen	k9
z	z	k7c2
geopolitického	geopolitický	k2eAgNnSc2d1
hlediska	hledisko	k1gNnSc2
<g/>
,	,	kIx,
podle	podle	k7c2
geografického	geografický	k2eAgNnSc2d1
členění	členění	k1gNnSc2
Kanárské	kanárský	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
náležejí	náležet	k5eAaImIp3nP
již	již	k6eAd1
k	k	k7c3
africkému	africký	k2eAgInSc3d1
kontinentu	kontinent	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Etna	Etna	k1gFnSc1
je	být	k5eAaImIp3nS
jedna	jeden	k4xCgFnSc1
z	z	k7c2
nejvíce	hodně	k6eAd3,k6eAd1
aktivních	aktivní	k2eAgFnPc2d1
sopek	sopka	k1gFnPc2
na	na	k7c6
světě	svět	k1gInSc6
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
podstatě	podstata	k1gFnSc6
neustále	neustále	k6eAd1
aktivní	aktivní	k2eAgMnSc1d1
a	a	k8xC
u	u	k7c2
které	který	k3yIgFnSc2,k3yRgFnSc2,k3yQgFnSc2
existuje	existovat	k5eAaImIp3nS
nejdelší	dlouhý	k2eAgInSc1d3
doložený	doložený	k2eAgInSc1d1
záznam	záznam	k1gInSc1
erupcí	erupce	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Projevuje	projevovat	k5eAaImIp3nS
se	se	k3xPyFc4
celou	celý	k2eAgFnSc7d1
škálou	škála	k1gFnSc7
typů	typ	k1gInPc2
sopečných	sopečný	k2eAgInPc2d1
<g />
.	.	kIx.
</s>
<s hack="1">
erupcí	erupce	k1gFnPc2
od	od	k7c2
strombolského	strombolský	k2eAgInSc2d1
po	po	k7c4
pliniovský	pliniovský	k2eAgInSc4d1
<g/>
,	,	kIx,
jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
stratovulkán	stratovulkán	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Úrodná	úrodný	k2eAgFnSc1d1
sopečná	sopečný	k2eAgFnSc1d1
půda	půda	k1gFnSc1
podporuje	podporovat	k5eAaImIp3nS
extenzivní	extenzivní	k2eAgNnSc4d1
zemědělství	zemědělství	k1gNnSc4
na	na	k7c6
jejích	její	k3xOp3gInPc6
svazích	svah	k1gInPc6
<g/>
,	,	kIx,
pěstování	pěstování	k1gNnSc1
vinné	vinný	k2eAgFnSc2d1
révy	réva	k1gFnSc2
a	a	k8xC
provozování	provozování	k1gNnSc2
ovocných	ovocný	k2eAgInPc2d1
sadů	sad	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
jsou	být	k5eAaImIp3nP
rozmístěny	rozmístit	k5eAaPmNgInP
v	v	k7c6
nižších	nízký	k2eAgFnPc6d2
oblastech	oblast	k1gFnPc6
svahů	svah	k1gInPc2
sopky	sopka	k1gFnSc2
a	a	k8xC
na	na	k7c6
jižní	jižní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
v	v	k7c6
rovině	rovina	k1gFnSc6
Catania	Catanium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Etna	Etna	k1gFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
hustě	hustě	k6eAd1
obydlené	obydlený	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
zvyšuje	zvyšovat	k5eAaImIp3nS
její	její	k3xOp3gNnSc1
potenciální	potenciální	k2eAgNnSc1d1
nebezpečí	nebezpečí	k1gNnSc1
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
v	v	k7c6
případě	případ	k1gInSc6
silné	silný	k2eAgFnSc2d1
erupce	erupce	k1gFnSc2
by	by	kYmCp3nS
mohla	moct	k5eAaImAgFnS
způsobit	způsobit	k5eAaPmF
značné	značný	k2eAgFnPc4d1
materiální	materiální	k2eAgFnPc4d1
škody	škoda	k1gFnPc4
a	a	k8xC
oběti	oběť	k1gFnPc4
na	na	k7c6
životech	život	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
toho	ten	k3xDgInSc2
důvodu	důvod	k1gInSc2
byla	být	k5eAaImAgFnS
společně	společně	k6eAd1
s	s	k7c7
dalšími	další	k2eAgMnPc7d1
patnácti	patnáct	k4xCc7
světovými	světový	k2eAgFnPc7d1
sopkami	sopka	k1gFnPc7
zapsána	zapsat	k5eAaPmNgFnS
do	do	k7c2
seznamu	seznam	k1gInSc2
Decade	Decad	k1gInSc5
Volcanoes	Volcanoes	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
přináší	přinášet	k5eAaImIp3nS
větší	veliký	k2eAgFnSc4d2
pozornost	pozornost	k1gFnSc4
vědecké	vědecký	k2eAgFnSc2d1
obce	obec	k1gFnSc2
a	a	k8xC
umožní	umožnit	k5eAaPmIp3nS
podrobný	podrobný	k2eAgInSc4d1
průzkum	průzkum	k1gInSc4
její	její	k3xOp3gFnSc2
sopečné	sopečný	k2eAgFnSc2d1
historie	historie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Geografický	geografický	k2eAgInSc1d1
popis	popis	k1gInSc1
</s>
<s>
Fotografie	fotografie	k1gFnSc1
ukazuje	ukazovat	k5eAaImIp3nS
rozložení	rozložení	k1gNnSc4
základních	základní	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
sopky	sopka	k1gFnSc2
</s>
<s>
Etna	Etna	k1gFnSc1
dosahuje	dosahovat	k5eAaImIp3nS
výšky	výška	k1gFnSc2
přibližně	přibližně	k6eAd1
3329	#num#	k4
metrů	metr	k1gInPc2
nad	nad	k7c7
mořem	moře	k1gNnSc7
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
<g/>
;	;	kIx,
tento	tento	k3xDgInSc1
údaj	údaj	k1gInSc1
se	s	k7c7
vzhledem	vzhled	k1gInSc7
k	k	k7c3
aktivitě	aktivita	k1gFnSc3
sopky	sopka	k1gFnSc2
stále	stále	k6eAd1
mění	měnit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obvod	obvod	k1gInSc1
základny	základna	k1gFnSc2
měří	měřit	k5eAaImIp3nS
210	#num#	k4
km	km	kA
a	a	k8xC
sopka	sopka	k1gFnSc1
se	se	k3xPyFc4
rozkládá	rozkládat	k5eAaImIp3nS
na	na	k7c4
přibližně	přibližně	k6eAd1
1750	#num#	k4
km	km	kA
<g/>
2	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Etna	Etna	k1gFnSc1
je	být	k5eAaImIp3nS
stratovulkán	stratovulkán	k1gInSc4
<g/>
,	,	kIx,
ale	ale	k8xC
nejedná	jednat	k5eNaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
jednotné	jednotný	k2eAgNnSc4d1
sopečné	sopečný	k2eAgNnSc4d1
těleso	těleso	k1gNnSc4
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
vzniklo	vzniknout	k5eAaPmAgNnS
jako	jako	k8xS,k8xC
jedno	jeden	k4xCgNnSc1
kompaktní	kompaktní	k2eAgNnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznik	vznik	k1gInSc1
Etny	Etna	k1gFnSc2
je	být	k5eAaImIp3nS
spojen	spojit	k5eAaPmNgInS
s	s	k7c7
erupcemi	erupce	k1gFnPc7
více	hodně	k6eAd2
kuželovitých	kuželovitý	k2eAgInPc2d1
útvarů	útvar	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
se	se	k3xPyFc4
vzájemně	vzájemně	k6eAd1
do	do	k7c2
sebe	se	k3xPyFc2
zakomponovaly	zakomponovat	k5eAaPmAgFnP
erupcemi	erupce	k1gFnPc7
a	a	k8xC
gravitačními	gravitační	k2eAgInPc7d1
kolapsy	kolaps	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1911	#num#	k4
byl	být	k5eAaImAgInS
vrcholek	vrcholek	k1gInSc1
Etny	Etna	k1gFnSc2
tvořen	tvořit	k5eAaImNgInS
jen	jen	k9
jedním	jeden	k4xCgInSc7
sopečným	sopečný	k2eAgInSc7d1
kráterem	kráter	k1gInSc7
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
vyrůstal	vyrůstat	k5eAaImAgInS
nad	nad	k7c7
původním	původní	k2eAgInSc7d1
Centrálním	centrální	k2eAgInSc7d1
kráterem	kráter	k1gInSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
později	pozdě	k6eAd2
vznikly	vzniknout	k5eAaPmAgInP
krátery	kráter	k1gInPc1
Voragine	Voragin	k1gInSc5
a	a	k8xC
Bocca	Bocc	k2eAgNnPc4d1
Nuova	Nuovo	k1gNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Vrchol	vrchol	k1gInSc1
Etny	Etna	k1gFnSc2
v	v	k7c6
současnosti	současnost	k1gFnSc6
tvoří	tvořit	k5eAaImIp3nP
čtyři	čtyři	k4xCgInPc1
hlavní	hlavní	k2eAgInPc1d1
sopečné	sopečný	k2eAgInPc1d1
krátery	kráter	k1gInPc1
–	–	k?
250	#num#	k4
metrů	metr	k1gInPc2
vysoký	vysoký	k2eAgMnSc1d1
Bocca	Bocca	k1gMnSc1
Nuova	Nuov	k1gMnSc2
vzniklý	vzniklý	k2eAgInSc4d1
v	v	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
Voragine	Voragin	k1gInSc5
vzniklý	vzniklý	k2eAgInSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1964	#num#	k4
<g/>
,	,	kIx,
Severovýchodní	severovýchodní	k2eAgInSc4d1
vzniklý	vzniklý	k2eAgInSc4d1
v	v	k7c6
roce	rok	k1gInSc6
1911	#num#	k4
a	a	k8xC
Jihovýchodní	jihovýchodní	k2eAgInSc1d1
kráter	kráter	k1gInSc1
vzniklý	vzniklý	k2eAgInSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1971	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Předpokládá	předpokládat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
tyto	tento	k3xDgInPc1
sopečné	sopečný	k2eAgInPc1d1
krátery	kráter	k1gInPc1
jsou	být	k5eAaImIp3nP
vzájemně	vzájemně	k6eAd1
propojeny	propojen	k2eAgInPc1d1
systémem	systém	k1gInSc7
pravých	pravý	k2eAgFnPc2d1
žil	žíla	k1gFnPc2
<g/>
,	,	kIx,
ale	ale	k8xC
často	často	k6eAd1
v	v	k7c6
dobách	doba	k1gFnPc6
jednotlivých	jednotlivý	k2eAgFnPc2d1
erupcí	erupce	k1gFnPc2
se	se	k3xPyFc4
ukazuje	ukazovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
každý	každý	k3xTgInSc1
kráter	kráter	k1gInSc1
umí	umět	k5eAaImIp3nS
chovat	chovat	k5eAaImF
zcela	zcela	k6eAd1
samostatně	samostatně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Vrcholek	vrcholek	k1gInSc1
Etny	Etna	k1gFnSc2
je	být	k5eAaImIp3nS
celoročně	celoročně	k6eAd1
pokryt	pokrýt	k5eAaPmNgInS
sněhovou	sněhový	k2eAgFnSc7d1
pokrývkou	pokrývka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
vrcholku	vrcholek	k1gInSc6
sopky	sopka	k1gFnSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
několik	několik	k4yIc1
kalder	kaldra	k1gFnPc2
<g/>
,	,	kIx,
mezi	mezi	k7c4
nejvýznamnější	významný	k2eAgFnSc4d3
patří	patřit	k5eAaImIp3nS
Ellittico	Ellittico	k6eAd1
Caldera	Caldera	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
vznikla	vzniknout	k5eAaPmAgFnS
před	před	k7c7
14	#num#	k4
000	#num#	k4
až	až	k9
15	#num#	k4
000	#num#	k4
roky	rok	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Celé	celý	k2eAgNnSc1d1
těleso	těleso	k1gNnSc1
sopky	sopka	k1gFnSc2
protíná	protínat	k5eAaImIp3nS
v	v	k7c6
severovýchodním	severovýchodní	k2eAgInSc6d1
směru	směr	k1gInSc6
zlomová	zlomový	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
způsobuje	způsobovat	k5eAaImIp3nS
vznik	vznik	k1gInSc4
různých	různý	k2eAgFnPc2d1
prasklin	prasklina	k1gFnPc2
přivádějících	přivádějící	k2eAgFnPc2d1
často	často	k6eAd1
magma	magma	k1gNnSc4
na	na	k7c4
povrch	povrch	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Nad	nad	k7c7
těmito	tento	k3xDgFnPc7
puklinami	puklina	k1gFnPc7
často	často	k6eAd1
vznikají	vznikat	k5eAaImIp3nP
menší	malý	k2eAgInPc4d2
parazitické	parazitický	k2eAgInPc4d1
kužele	kužel	k1gInSc2
či	či	k8xC
kužele	kužel	k1gInSc2
z	z	k7c2
nasypané	nasypaný	k2eAgFnSc2d1
strusky	struska	k1gFnSc2
nebo	nebo	k8xC
tufů	tuf	k1gInPc2
<g/>
,	,	kIx,
celkový	celkový	k2eAgInSc1d1
počet	počet	k1gInSc1
takovýchto	takovýto	k3xDgNnPc2
těles	těleso	k1gNnPc2
je	být	k5eAaImIp3nS
přes	přes	k7c4
tři	tři	k4xCgNnPc4
sta	sto	k4xCgNnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Některé	některý	k3yIgFnPc1
jsou	být	k5eAaImIp3nP
ve	v	k7c6
skutečnosti	skutečnost	k1gFnSc6
nové	nový	k2eAgFnSc2d1
malé	malý	k2eAgFnSc2d1
sopky	sopka	k1gFnSc2
vysoké	vysoký	k2eAgFnPc4d1
pouze	pouze	k6eAd1
několik	několik	k4yIc4
desítek	desítka	k1gFnPc2
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
jiné	jiný	k2eAgInPc1d1
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
vysoké	vysoký	k2eAgNnSc4d1
až	až	k6eAd1
několik	několik	k4yIc4
stovek	stovka	k1gFnPc2
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odhaduje	odhadovat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
celkem	celkem	k6eAd1
se	se	k3xPyFc4
na	na	k7c6
svazích	svah	k1gInPc6
Etny	Etna	k1gFnSc2
nachází	nacházet	k5eAaImIp3nS
250	#num#	k4
až	až	k9
300	#num#	k4
samostatných	samostatný	k2eAgNnPc2d1
menších	malý	k2eAgNnPc2d2
těles	těleso	k1gNnPc2
<g/>
,	,	kIx,
ze	z	k7c2
kterých	který	k3yQgInPc2,k3yIgInPc2,k3yRgInPc2
příležitostně	příležitostně	k6eAd1
proudí	proudit	k5eAaPmIp3nS,k5eAaImIp3nS
láva	láva	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Mezi	mezi	k7c4
nejvýznamnější	významný	k2eAgFnPc4d3
patří	patřit	k5eAaImIp3nP
Monte	Mont	k1gInSc5
Barca	Barcum	k1gNnPc1
<g/>
,	,	kIx,
Monte	Mont	k1gMnSc5
Moio	Moia	k1gMnSc5
<g/>
,	,	kIx,
Monti	Monť	k1gFnSc3
Rossi	Rosse	k1gFnSc3
či	či	k8xC
Monti	Monť	k1gFnSc3
Silvestri	Silvestr	k1gFnSc2
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Vznikají	vznikat	k5eAaImIp3nP
jako	jako	k8xC,k8xS
výsledek	výsledek	k1gInSc4
svahových	svahový	k2eAgFnPc2d1
erupcí	erupce	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
pro	pro	k7c4
obyvatele	obyvatel	k1gMnPc4
žijící	žijící	k2eAgMnPc4d1
okolo	okolo	k7c2
Etny	Etna	k1gFnSc2
pravděpodobně	pravděpodobně	k6eAd1
nejvíce	nejvíce	k6eAd1,k6eAd3
nebezpečnou	bezpečný	k2eNgFnSc7d1
sopečnou	sopečný	k2eAgFnSc7d1
událostí	událost	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Tyto	tento	k3xDgInPc1
kužele	kužel	k1gInSc2
nejsou	být	k5eNaImIp3nP
okolo	okolo	k7c2
sopky	sopka	k1gFnSc2
rozmístěny	rozmístěn	k2eAgFnPc1d1
kruhově	kruhově	k6eAd1
<g/>
,	,	kIx,
ale	ale	k8xC
mají	mít	k5eAaImIp3nP
orientaci	orientace	k1gFnSc4
ve	v	k7c6
směru	směr	k1gInSc6
zlomů	zlom	k1gInPc2
S	s	k7c7
<g/>
30	#num#	k4
<g/>
°	°	k?
<g/>
V	V	kA
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
vzniká	vznikat	k5eAaImIp3nS
severovýchodní	severovýchodní	k2eAgInSc1d1
riftovový	riftovový	k2eAgInSc1d1
systém	systém	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Menší	malý	k2eAgFnSc1d2
část	část	k1gFnSc1
kuželů	kužel	k1gInPc2
vzniká	vznikat	k5eAaImIp3nS
ve	v	k7c6
směru	směr	k1gInSc6
S	s	k7c7
<g/>
15	#num#	k4
<g/>
°	°	k?
<g/>
Z	Z	kA
a	a	k8xC
S	s	k7c7
<g/>
60	#num#	k4
<g/>
°	°	k?
<g/>
V.	V.	kA
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pod	pod	k7c7
Etnou	Etna	k1gFnSc7
se	se	k3xPyFc4
pravděpodobně	pravděpodobně	k6eAd1
stýkají	stýkat	k5eAaImIp3nP
tři	tři	k4xCgInPc1
výrazné	výrazný	k2eAgInPc1d1
zlomové	zlomový	k2eAgInPc1d1
systémy	systém	k1gInPc1
procházející	procházející	k2eAgInPc1d1
větší	veliký	k2eAgFnSc7d2
částí	část	k1gFnSc7
Sicílie	Sicílie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInSc4
hlavní	hlavní	k2eAgInSc4d1
zlomový	zlomový	k2eAgInSc4d1
systém	systém	k1gInSc4
prochází	procházet	k5eAaImIp3nS
ve	v	k7c6
směru	směr	k1gInSc6
SSV-JJZ	SSV-JJZ	k1gFnPc2
<g/>
,	,	kIx,
druhý	druhý	k4xOgInSc1
pak	pak	k9
ve	v	k7c6
směru	směr	k1gInSc6
VSV-ZJZ	VSV-ZJZ	k1gMnSc2
a	a	k8xC
třetí	třetí	k4xOgFnSc2
menší	malý	k2eAgFnSc2d2
VJV-ZSZ	VJV-ZSZ	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
zlomy	zlom	k1gInPc4
Monte	Mont	k1gInSc5
Kumeta	Kumet	k5eAaPmNgFnS
–	–	k?
Alcantara	Alcantara	k1gFnSc1
<g/>
,	,	kIx,
Messina	Messina	k1gFnSc1
–	–	k?
Giardini	Giardin	k2eAgMnPc1d1
a	a	k8xC
Tindari	Tindare	k1gFnSc4
–	–	k?
Letojanni	Letojaneň	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
skutečnost	skutečnost	k1gFnSc4
může	moct	k5eAaImIp3nS
mít	mít	k5eAaImF
důležitý	důležitý	k2eAgInSc4d1
dopad	dopad	k1gInSc4
na	na	k7c4
přívod	přívod	k1gInSc4
magmatu	magma	k1gNnSc2
do	do	k7c2
sopky	sopka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Samotné	samotný	k2eAgNnSc1d1
těleso	těleso	k1gNnSc1
sopky	sopka	k1gFnSc2
je	být	k5eAaImIp3nS
taktéž	taktéž	k?
rozpraskané	rozpraskaný	k2eAgFnPc4d1
menšími	malý	k2eAgInPc7d2
mělkými	mělký	k2eAgInPc7d1
zlomy	zlom	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
mohou	moct	k5eAaImIp3nP
mít	mít	k5eAaImF
ale	ale	k9
spojitost	spojitost	k1gFnSc1
s	s	k7c7
gravitačním	gravitační	k2eAgInSc7d1
kolapsem	kolaps	k1gInSc7
kužele	kužel	k1gInSc2
namísto	namísto	k7c2
s	s	k7c7
většími	veliký	k2eAgInPc7d2
zlomovými	zlomový	k2eAgInPc7d1
systémy	systém	k1gInPc7
procházející	procházející	k2eAgMnPc1d1
Sicílií	Sicílie	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vrcholy	vrchol	k1gInPc1
</s>
<s>
3D	3D	k4
pohled	pohled	k1gInSc1
na	na	k7c4
těleso	těleso	k1gNnSc4
sopky	sopka	k1gFnSc2
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
měla	mít	k5eAaImAgFnS
Etna	Etna	k1gFnSc1
čtyři	čtyři	k4xCgInPc4
hlavní	hlavní	k2eAgInPc4d1
vrcholy	vrchol	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
jimi	on	k3xPp3gFnPc7
Bocca	Bocca	k1gFnSc1
Nuova	Nuovo	k1gNnPc1
<g/>
,	,	kIx,
Voragine	Voragin	k1gMnSc5
<g/>
,	,	kIx,
Severovýchodní	severovýchodní	k2eAgInSc4d1
a	a	k8xC
Jihovýchodní	jihovýchodní	k2eAgInSc4d1
kráter	kráter	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Centrální	centrální	k2eAgInSc1d1
kráter	kráter	k1gInSc1
</s>
<s>
Až	až	k6eAd1
do	do	k7c2
roku	rok	k1gInSc2
1911	#num#	k4
měla	mít	k5eAaImAgFnS
Etna	Etna	k1gFnSc1
jediný	jediný	k2eAgInSc4d1
vrchol	vrchol	k1gInSc4
s	s	k7c7
Centrálním	centrální	k2eAgInSc7d1
kráterem	kráter	k1gInSc7
v	v	k7c6
jeho	jeho	k3xOp3gFnSc6
nejvyšší	vysoký	k2eAgFnSc6d3
části	část	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednalo	jednat	k5eAaImAgNnS
se	se	k3xPyFc4
o	o	k7c4
kuželovité	kuželovitý	k2eAgNnSc4d1
těleso	těleso	k1gNnSc4
250	#num#	k4
metrů	metr	k1gInPc2
vysoké	vysoká	k1gFnSc2
na	na	k7c6
jehož	jehož	k3xOyRp3gInSc6
vrcholu	vrchol	k1gInSc6
byl	být	k5eAaImAgMnS
500	#num#	k4
metrů	metr	k1gInPc2
široký	široký	k2eAgInSc4d1
kráter	kráter	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
historických	historický	k2eAgInPc2d1
záznamů	záznam	k1gInPc2
vyplývá	vyplývat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
jednalo	jednat	k5eAaImAgNnS
o	o	k7c4
kužel	kužel	k1gInSc4
vzniklý	vzniklý	k2eAgInSc4d1
během	během	k7c2
18	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
na	na	k7c6
místě	místo	k1gNnSc6
katastrofického	katastrofický	k2eAgInSc2d1
sesuvu	sesuv	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
1669	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kráter	kráter	k1gInSc1
byl	být	k5eAaImAgInS
výrazně	výrazně	k6eAd1
aktivní	aktivní	k2eAgMnSc1d1
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1869	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
červenci	červenec	k1gInSc6
1787	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
tomto	tento	k3xDgInSc6
kráteru	kráter	k1gInSc6
odehrála	odehrát	k5eAaPmAgFnS
i	i	k9
nejsilnější	silný	k2eAgFnSc1d3
zdokumentovaná	zdokumentovaný	k2eAgFnSc1d1
erupce	erupce	k1gFnSc1
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
vzniku	vznik	k1gInSc3
lávové	lávový	k2eAgFnSc2d1
fontány	fontána	k1gFnSc2
tryskající	tryskající	k2eAgNnSc1d1
až	až	k9
do	do	k7c2
výšky	výška	k1gFnSc2
přes	přes	k7c4
3000	#num#	k4
metrů	metr	k1gInPc2
a	a	k8xC
lávového	lávový	k2eAgInSc2d1
proudu	proud	k1gInSc2
5	#num#	k4
km	km	kA
dlouhého	dlouhý	k2eAgInSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
druhé	druhý	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
19	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
začátku	začátek	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
se	se	k3xPyFc4
Centrální	centrální	k2eAgInSc1d1
kráter	kráter	k1gInSc1
stal	stát	k5eAaPmAgInS
jediným	jediný	k2eAgInSc7d1
útvarem	útvar	k1gInSc7
s	s	k7c7
téměř	téměř	k6eAd1
kolmými	kolmý	k2eAgFnPc7d1
stěnami	stěna	k1gFnPc7
zdvihajícími	zdvihající	k2eAgFnPc7d1
se	se	k3xPyFc4
do	do	k7c2
výšky	výška	k1gFnSc2
150	#num#	k4
až	až	k9
200	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
roky	rok	k1gInPc4
1916	#num#	k4
až	až	k8xS
1922	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
severní	severní	k2eAgFnSc6d1
části	část	k1gFnSc6
kráteru	kráter	k1gInSc2
vytvořil	vytvořit	k5eAaPmAgInS
nový	nový	k2eAgInSc1d1
menší	malý	k2eAgInSc1d2
kužel	kužel	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
zaplňoval	zaplňovat	k5eAaImAgInS
centrální	centrální	k2eAgFnSc4d1
část	část	k1gFnSc4
původního	původní	k2eAgInSc2d1
kráteru	kráter	k1gInSc2
eruptivním	eruptivní	k2eAgInSc7d1
materiálem	materiál	k1gInSc7
až	až	k6eAd1
vznikla	vzniknout	k5eAaPmAgFnS
rovná	rovný	k2eAgFnSc1d1
terasa	terasa	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yRgFnSc4,k3yQgFnSc4
zničil	zničit	k5eAaPmAgInS
pozdější	pozdní	k2eAgInSc1d2
kolaps	kolaps	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
40	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
docházelo	docházet	k5eAaImAgNnS
k	k	k7c3
podobným	podobný	k2eAgFnPc3d1
událostem	událost	k1gFnPc3
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
vzniklo	vzniknout	k5eAaPmAgNnS
několik	několik	k4yIc1
menších	malý	k2eAgInPc2d2
kuželů	kužel	k1gInPc2
a	a	k8xC
kolapsů	kolaps	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
se	se	k3xPyFc4
začal	začít	k5eAaPmAgInS
také	také	k9
pomalu	pomalu	k6eAd1
formovat	formovat	k5eAaImF
kužel	kužel	k1gInSc4
<g/>
,	,	kIx,
ze	z	k7c2
kterého	který	k3yIgMnSc2,k3yRgMnSc2,k3yQgMnSc2
později	pozdě	k6eAd2
vznikl	vzniknout	k5eAaPmAgInS
dnešní	dnešní	k2eAgInSc1d1
kužel	kužel	k1gInSc1
Voragine	Voragin	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1964	#num#	k4
vznikly	vzniknout	k5eAaPmAgInP
dva	dva	k4xCgInPc1
nové	nový	k2eAgInPc1d1
kužele	kužel	k1gInSc2
kolem	kolem	k7c2
prasklin	prasklina	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
dovršily	dovršit	k5eAaPmAgFnP
zasypání	zasypání	k1gNnSc4
Centrálního	centrální	k2eAgInSc2d1
kráteru	kráter	k1gInSc2
a	a	k8xC
jeho	jeho	k3xOp3gNnSc4
kompletní	kompletní	k2eAgNnSc4d1
pohřbení	pohřbení	k1gNnSc4
pod	pod	k7c4
nový	nový	k2eAgInSc4d1
materiál	materiál	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Voragine	Voraginout	k5eAaPmIp3nS
</s>
<s>
Vrchol	vrcholit	k5eAaImRp2nS
Voragine	Voragin	k1gMnSc5
se	se	k3xPyFc4
zformoval	zformovat	k5eAaPmAgMnS
do	do	k7c2
dnešní	dnešní	k2eAgFnSc2d1
podoby	podoba	k1gFnSc2
v	v	k7c6
60	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
vznikat	vznikat	k5eAaImF
začal	začít	k5eAaPmAgInS
již	již	k6eAd1
ve	v	k7c6
40	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1964	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
jeho	jeho	k3xOp3gInSc3
hlavnímu	hlavní	k2eAgInSc3d1
rozvoji	rozvoj	k1gInSc3
<g/>
,	,	kIx,
když	když	k8xS
z	z	k7c2
praskliny	prasklina	k1gFnSc2
<g/>
,	,	kIx,
nad	nad	k7c4
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
stál	stát	k5eAaImAgInS
<g/>
,	,	kIx,
začalo	začít	k5eAaPmAgNnS
proudit	proudit	k5eAaImF,k5eAaPmF
množství	množství	k1gNnSc1
materiálu	materiál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
jeho	jeho	k3xOp3gInSc2,k3xPp3gInSc2
vzniku	vznik	k1gInSc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
zániku	zánik	k1gInSc3
Centrálního	centrální	k2eAgInSc2d1
kráteru	kráter	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bocca	Bocc	k2eAgFnSc1d1
Nuova	Nuova	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
vrchol	vrchol	k1gInSc1
vznikl	vzniknout	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1968	#num#	k4
v	v	k7c6
okolí	okolí	k1gNnSc6
vrcholu	vrchol	k1gInSc2
Voragine	Voragin	k1gMnSc5
<g/>
,	,	kIx,
západně	západně	k6eAd1
od	od	k7c2
kužele	kužel	k1gInSc2
vzniklého	vzniklý	k2eAgInSc2d1
při	při	k7c6
erupci	erupce	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
1964	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Původní	původní	k2eAgFnSc1d1
velikost	velikost	k1gFnSc1
kráteru	kráter	k1gInSc2
byla	být	k5eAaImAgFnS
8	#num#	k4
metrů	metr	k1gInPc2
v	v	k7c6
průměru	průměr	k1gInSc6
<g/>
,	,	kIx,
ale	ale	k8xC
následkem	následkem	k7c2
kolapsů	kolaps	k1gInPc2
jeho	jeho	k3xOp3gInPc2
okrajů	okraj	k1gInPc2
během	během	k7c2
30	#num#	k4
let	léto	k1gNnPc2
existence	existence	k1gFnSc2
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
zvětšení	zvětšení	k1gNnSc3
až	až	k6eAd1
na	na	k7c4
350	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
kterých	který	k3yIgInPc2,k3yRgInPc2,k3yQgInPc2
dosáhl	dosáhnout	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
1997	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
menších	malý	k2eAgInPc2d2
kuželů	kužel	k1gInPc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
byla	být	k5eAaImAgFnS
zničena	zničit	k5eAaPmNgFnS
během	během	k7c2
kolapsů	kolaps	k1gInPc2
Bocca	Boccus	k1gMnSc2
Nuova	Nuov	k1gMnSc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
snížení	snížení	k1gNnSc3
nadmořské	nadmořský	k2eAgFnSc2d1
výšky	výška	k1gFnSc2
Etny	Etna	k1gFnSc2
na	na	k7c4
3310	#num#	k4
metrů	metr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c4
90	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
docházelo	docházet	k5eAaImAgNnS
pak	pak	k6eAd1
postupně	postupně	k6eAd1
k	k	k7c3
zaplňování	zaplňování	k1gNnSc3
tohoto	tento	k3xDgInSc2
kráteru	kráter	k1gInSc2
materiálem	materiál	k1gInSc7
<g/>
,	,	kIx,
až	až	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1999	#num#	k4
byl	být	k5eAaImAgInS
tento	tento	k3xDgInSc4
kráter	kráter	k1gInSc4
téměř	téměř	k6eAd1
kompletně	kompletně	k6eAd1
vyplněn	vyplnit	k5eAaPmNgInS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Severovýchodní	severovýchodní	k2eAgInSc1d1
a	a	k8xC
jihovýchodní	jihovýchodní	k2eAgInSc1d1
kráter	kráter	k1gInSc1
</s>
<s>
Oblast	oblast	k1gFnSc1
Valle	Valle	k1gFnSc2
de	de	k?
Bove	Bov	k1gFnSc2
v	v	k7c6
noci	noc	k1gFnSc6
během	během	k7c2
sopečné	sopečný	k2eAgFnSc2d1
činnosti	činnost	k1gFnSc2
</s>
<s>
Oba	dva	k4xCgInPc1
dva	dva	k4xCgInPc1
krátery	kráter	k1gInPc1
začaly	začít	k5eAaPmAgInP
vznikat	vznikat	k5eAaImF
podobně	podobně	k6eAd1
jako	jako	k8xS,k8xC
výsledek	výsledek	k1gInSc1
kolapsu	kolaps	k1gInSc2
kužele	kužel	k1gInSc2
na	na	k7c6
svahu	svah	k1gInSc6
sopky	sopka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jihovýchodní	jihovýchodní	k2eAgInSc1d1
kráter	kráter	k1gInSc1
začal	začít	k5eAaPmAgInS
vznikat	vznikat	k5eAaImF
v	v	k7c6
roce	rok	k1gInSc6
1971	#num#	k4
<g/>
,	,	kIx,
ale	ale	k8xC
pak	pak	k6eAd1
se	se	k3xPyFc4
až	až	k9
do	do	k7c2
roku	rok	k1gInSc2
1978	#num#	k4
odmlčel	odmlčet	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
se	se	k3xPyFc4
ozval	ozvat	k5eAaPmAgMnS
šesti	šest	k4xCc7
silnými	silný	k2eAgFnPc7d1
erupcemi	erupce	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1989	#num#	k4
se	se	k3xPyFc4
projevoval	projevovat	k5eAaImAgInS
převážně	převážně	k6eAd1
lávovými	lávový	k2eAgFnPc7d1
fontánami	fontána	k1gFnPc7
a	a	k8xC
5	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
1990	#num#	k4
hlasitou	hlasitý	k2eAgFnSc7d1
erupcí	erupce	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Jihovýchodní	jihovýchodní	k2eAgInSc1d1
kráter	kráter	k1gInSc1
byl	být	k5eAaImAgInS
aktivní	aktivní	k2eAgInSc1d1
následně	následně	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
dosáhl	dosáhnout	k5eAaPmAgInS
výšky	výška	k1gFnSc2
3300	#num#	k4
metrů	metr	k1gInPc2
nad	nad	k7c7
mořem	moře	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Oproti	oproti	k7c3
tomu	ten	k3xDgNnSc3
Severovýchodní	severovýchodní	k2eAgInSc4d1
kráter	kráter	k1gInSc4
byl	být	k5eAaImAgInS
v	v	k7c6
růstu	růst	k1gInSc6
poznamenán	poznamenat	k5eAaPmNgInS
opakovanými	opakovaný	k2eAgInPc7d1
kolapsy	kolaps	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yQgInPc1,k3yIgInPc1
se	se	k3xPyFc4
odehrály	odehrát	k5eAaPmAgInP
v	v	k7c6
50	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
takže	takže	k8xS
jeho	jeho	k3xOp3gInPc1
základy	základ	k1gInPc1
se	se	k3xPyFc4
zformovaly	zformovat	k5eAaPmAgInP
na	na	k7c6
strukturách	struktura	k1gFnPc6
vzniklých	vzniklý	k2eAgFnPc2d1
těmito	tento	k3xDgFnPc7
událostmi	událost	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc1d1
část	část	k1gFnSc1
kráteru	kráter	k1gInSc2
se	se	k3xPyFc4
pak	pak	k6eAd1
vyvinula	vyvinout	k5eAaPmAgFnS
až	až	k9
v	v	k7c6
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
tohoto	tento	k3xDgNnSc2
desetiletí	desetiletí	k1gNnSc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
erupcím	erupce	k1gFnPc3
na	na	k7c6
dolním	dolní	k2eAgInSc6d1
okraji	okraj	k1gInSc6
kužele	kužel	k1gInSc2
Severovýchodní	severovýchodní	k2eAgFnSc2d1
kráteru	kráter	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
začaly	začít	k5eAaPmAgFnP
tvořit	tvořit	k5eAaImF
další	další	k2eAgInSc4d1
menší	malý	k2eAgInSc4d2
kužel	kužel	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
lety	léto	k1gNnPc7
1991	#num#	k4
až	až	k8xS
1993	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
sérii	série	k1gFnSc3
kolapsů	kolaps	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
kráter	kráter	k1gInSc4
zmenšily	zmenšit	k5eAaPmAgInP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
1998	#num#	k4
došlo	dojít	k5eAaPmAgNnS
vlivem	vliv	k1gInSc7
silné	silný	k2eAgFnSc2d1
erupce	erupce	k1gFnSc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
Voragine	Voragin	k1gInSc5
k	k	k7c3
sesuvu	sesuv	k1gInSc3
jižního	jižní	k2eAgInSc2d1
svahu	svah	k1gInSc2
Severovýchodního	severovýchodní	k2eAgInSc2d1
kráteru	kráter	k1gInSc2
a	a	k8xC
překrytí	překrytí	k1gNnSc2
vrcholu	vrchol	k1gInSc2
několika	několik	k4yIc3
metry	metr	k1gInPc4
pyroklastického	pyroklastický	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
vzniku	vznik	k1gInSc2
</s>
<s>
Strukturní	strukturní	k2eAgInPc1d1
a	a	k8xC
seismické	seismický	k2eAgInPc1d1
údaje	údaj	k1gInPc1
naznačují	naznačovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
vznik	vznik	k1gInSc1
Etny	Etna	k1gFnSc2
je	být	k5eAaImIp3nS
spojen	spojit	k5eAaPmNgInS
s	s	k7c7
regionální	regionální	k2eAgFnSc7d1
deformací	deformace	k1gFnSc7
projevující	projevující	k2eAgFnSc2d1
se	se	k3xPyFc4
kompresním	kompresní	k2eAgInSc7d1
zlomem	zlom	k1gInSc7
v	v	k7c6
severojižním	severojižní	k2eAgInSc6d1
směru	směr	k1gInSc6
jako	jako	k8xC,k8xS
výsledek	výsledek	k1gInSc1
subdukce	subdukce	k1gFnSc2
africké	africký	k2eAgFnSc2d1
litosférické	litosférický	k2eAgFnSc2d1
desky	deska	k1gFnSc2
pod	pod	k7c4
euroasijskou	euroasijský	k2eAgFnSc4d1
desku	deska	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Celý	celý	k2eAgInSc1d1
proces	proces	k1gInSc1
započal	započnout	k5eAaPmAgInS
přibližně	přibližně	k6eAd1
před	před	k7c7
80	#num#	k4
milióny	milión	k4xCgInPc7
lety	léto	k1gNnPc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
Sicílie	Sicílie	k1gFnSc1
natáčet	natáčet	k5eAaImF
vlivem	vlivem	k7c2
tlaku	tlak	k1gInSc2
africké	africký	k2eAgFnSc2d1
desky	deska	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Samotná	samotný	k2eAgFnSc1d1
africká	africký	k2eAgFnSc1d1
deska	deska	k1gFnSc1
popraskala	popraskat	k5eAaPmAgFnS
a	a	k8xC
vytvořila	vytvořit	k5eAaPmAgFnS
chaotickou	chaotický	k2eAgFnSc4d1
strukturu	struktura	k1gFnSc4
protkanou	protkaný	k2eAgFnSc4d1
zlomy	zlom	k1gInPc1
<g/>
,	,	kIx,
kterými	který	k3yRgInPc7,k3yQgInPc7,k3yIgInPc7
mohlo	moct	k5eAaImAgNnS
na	na	k7c4
povrch	povrch	k1gInSc4
začít	začít	k5eAaPmF
stoupat	stoupat	k5eAaImF
magma	magma	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
vznik	vznik	k1gInSc4
Etny	Etna	k1gFnSc2
a	a	k8xC
dlouhotrvající	dlouhotrvající	k2eAgInSc1d1
vulkanismus	vulkanismus	k1gInSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
nebyl	být	k5eNaImAgInS
zcela	zcela	k6eAd1
vysvětlen	vysvětlen	k2eAgInSc1d1
a	a	k8xC
existuje	existovat	k5eAaImIp3nS
několik	několik	k4yIc4
proti	proti	k7c3
sobě	se	k3xPyFc3
stojících	stojící	k2eAgFnPc2d1
teorií	teorie	k1gFnSc7
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Stratovulkán	stratovulkán	k1gInSc4
Etna	Etna	k1gFnSc1
vznikl	vzniknout	k5eAaPmAgInS
v	v	k7c6
průběhu	průběh	k1gInSc6
pleistocénu	pleistocén	k1gInSc2
a	a	k8xC
holocénu	holocén	k1gInSc2
na	na	k7c6
starší	starý	k2eAgFnSc6d2
štítové	štítový	k2eAgFnSc6d1
sopce	sopka	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
Sopečná	sopečný	k2eAgFnSc1d1
aktivita	aktivita	k1gFnSc1
začala	začít	k5eAaPmAgFnS
v	v	k7c6
oblasti	oblast	k1gFnSc6
Etny	Etna	k1gFnSc2
přibližně	přibližně	k6eAd1
před	před	k7c4
půl	půl	k1xP
milionem	milion	k4xCgInSc7
let	let	k1gInSc4
podmořskou	podmořský	k2eAgFnSc7d1
erupcí	erupce	k1gFnSc7
v	v	k7c6
tehdejší	tehdejší	k2eAgFnSc6d1
příbřežní	příbřežní	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
Sicílie	Sicílie	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
]	]	kIx)
čímž	což	k3yQnSc7,k3yRnSc7
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
vzniku	vznik	k1gInSc3
submarinních	submarinní	k2eAgNnPc2d1
lávových	lávový	k2eAgNnPc2d1
těles	těleso	k1gNnPc2
s	s	k7c7
typickou	typický	k2eAgFnSc7d1
polštářovou	polštářový	k2eAgFnSc7d1
strukturou	struktura	k1gFnSc7
lávy	láva	k1gFnSc2
tholeiitického	tholeiitický	k2eAgNnSc2d1
složení	složení	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Pozůstatky	pozůstatek	k1gInPc1
této	tento	k3xDgFnSc2
fáze	fáze	k1gFnSc2
formování	formování	k1gNnSc2
Etny	Etna	k1gFnSc2
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
pozorovat	pozorovat	k5eAaImF
na	na	k7c6
odkryvech	odkryv	k1gInPc6
okolo	okolo	k7c2
města	město	k1gNnSc2
Acicestello	Acicestello	k1gNnSc1
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
vedle	vedle	k6eAd1
sebe	sebe	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
polštářová	polštářový	k2eAgFnSc1d1
láva	láva	k1gFnSc1
a	a	k8xC
hyaloklastika	hyaloklastika	k1gFnSc1
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
O	o	k7c4
300	#num#	k4
000	#num#	k4
let	léto	k1gNnPc2
později	pozdě	k6eAd2
se	se	k3xPyFc4
sopečná	sopečný	k2eAgFnSc1d1
aktivita	aktivita	k1gFnSc1
přesunula	přesunout	k5eAaPmAgFnS
do	do	k7c2
nové	nový	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
leží	ležet	k5eAaImIp3nS
jihozápadně	jihozápadně	k6eAd1
od	od	k7c2
současného	současný	k2eAgNnSc2d1
místa	místo	k1gNnSc2
erupcí	erupce	k1gFnPc2
<g/>
,	,	kIx,
kam	kam	k6eAd1
se	se	k3xPyFc4
přesunula	přesunout	k5eAaPmAgFnS
aktivita	aktivita	k1gFnSc1
přibližně	přibližně	k6eAd1
před	před	k7c7
170	#num#	k4
000	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
začala	začít	k5eAaPmAgFnS
vznikat	vznikat	k5eAaImF
přímá	přímý	k2eAgFnSc1d1
předchůdkyně	předchůdkyně	k1gFnSc1
dnešní	dnešní	k2eAgFnSc2d1
Etny	Etna	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Tehdejší	tehdejší	k2eAgFnSc2d1
erupce	erupce	k1gFnSc2
vybudovaly	vybudovat	k5eAaPmAgInP
první	první	k4xOgNnSc4
větší	veliký	k2eAgNnSc4d2
těleso	těleso	k1gNnSc4
sopky	sopka	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
odpovídalo	odpovídat	k5eAaImAgNnS
stratovulkánu	stratovulkán	k1gInSc3
vlivem	vlivem	k7c2
efuzivních	efuzivní	k2eAgFnPc2d1
a	a	k8xC
explozivních	explozivní	k2eAgFnPc2d1
erupcí	erupce	k1gFnPc2
tholeiitického	tholeiitický	k2eAgNnSc2d1
a	a	k8xC
alkalického	alkalický	k2eAgNnSc2d1
složení	složení	k1gNnSc2
hornin	hornina	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kráter	kráter	k1gInSc1
nedaleko	nedaleko	k7c2
Torre	torr	k1gInSc5
del	del	k?
Filosofo	Filosofa	k1gMnSc5
<g/>
,	,	kIx,
přibližně	přibližně	k6eAd1
450	#num#	k4
metrů	metr	k1gInPc2
pod	pod	k7c7
vrcholem	vrchol	k1gInSc7
Etny	Etna	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Následovalo	následovat	k5eAaImAgNnS
období	období	k1gNnSc1
tzv.	tzv.	kA
„	„	k?
<g/>
Trifoglietto	Trifoglietto	k1gNnSc1
II	II	kA
<g/>
“	“	k?
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
v	v	k7c6
oblasti	oblast	k1gFnSc6
existovalo	existovat	k5eAaImAgNnS
několik	několik	k4yIc1
stratovulkánů	stratovulkán	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
se	se	k3xPyFc4
vzájemně	vzájemně	k6eAd1
prorůstaly	prorůstat	k5eAaImAgFnP
a	a	k8xC
překrývaly	překrývat	k5eAaImAgFnP
sopečnou	sopečný	k2eAgFnSc7d1
aktivitou	aktivita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
tehdejší	tehdejší	k2eAgInPc4d1
aktivní	aktivní	k2eAgInPc4d1
stratovulkány	stratovulkán	k1gInPc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
patřily	patřit	k5eAaImAgFnP
Triforglietto	Triforglietto	k1gNnSc4
II	II	kA
<g/>
,	,	kIx,
Vavalaci	Vavalace	k1gFnSc6
a	a	k8xC
Cuvigghiuni	Cuvigghiueň	k1gFnSc6
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
byly	být	k5eAaImAgFnP
charakteristické	charakteristický	k2eAgFnPc4d1
alkalickými	alkalický	k2eAgNnPc7d1
magmaty	magma	k1gNnPc7
odpovídající	odpovídající	k2eAgFnSc1d1
trachyandezitům	trachyandezita	k1gMnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
vlivem	vlivem	k7c2
chemického	chemický	k2eAgNnSc2d1
složení	složení	k1gNnSc2
narůstala	narůstat	k5eAaImAgFnS
síla	síla	k1gFnSc1
explozivního	explozivní	k2eAgInSc2d1
vulkanismu	vulkanismus	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vedlo	vést	k5eAaImAgNnS
ke	k	k7c3
zmenšování	zmenšování	k1gNnSc3
sopky	sopka	k1gFnSc2
explozemi	exploze	k1gFnPc7
a	a	k8xC
<g />
.	.	kIx.
</s>
<s hack="1">
vznikem	vznik	k1gInSc7
výrazných	výrazný	k2eAgFnPc2d1
kalder	kaldra	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
základě	základ	k1gInSc6
datování	datování	k1gNnSc2
se	se	k3xPyFc4
odhaduje	odhadovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
tato	tento	k3xDgFnSc1
epizoda	epizoda	k1gFnSc1
ve	v	k7c6
vývoji	vývoj	k1gInSc6
Etny	Etna	k1gFnSc2
probíhala	probíhat	k5eAaImAgFnS
před	před	k7c7
80	#num#	k4
či	či	k8xC
63	#num#	k4
000	#num#	k4
lety	let	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Těleso	těleso	k1gNnSc4
sopky	sopka	k1gFnSc2
postupně	postupně	k6eAd1
narůstalo	narůstat	k5eAaImAgNnS
<g/>
,	,	kIx,
i	i	k8xC
když	když	k8xS
bylo	být	k5eAaImAgNnS
příležitostně	příležitostně	k6eAd1
zmenšeno	zmenšit	k5eAaPmNgNnS
explozí	exploze	k1gFnSc7
či	či	k8xC
gravitačním	gravitační	k2eAgInSc7d1
sesuvem	sesuv	k1gInSc7
způsobeným	způsobený	k2eAgInSc7d1
silnou	silný	k2eAgFnSc7d1
sopečnou	sopečný	k2eAgFnSc7d1
erupcí	erupce	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Přibližně	přibližně	k6eAd1
před	před	k7c7
35	#num#	k4
000	#num#	k4
až	až	k9
15	#num#	k4
000	#num#	k4
lety	léto	k1gNnPc7
Etna	Etna	k1gFnSc1
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
projevovat	projevovat	k5eAaImF
silnými	silný	k2eAgFnPc7d1
explozivními	explozivní	k2eAgFnPc7d1
erupcemi	erupce	k1gFnPc7
<g/>
,	,	kIx,
během	během	k7c2
kterých	který	k3yQgInPc2,k3yIgInPc2,k3yRgInPc2
vzniká	vznikat	k5eAaImIp3nS
obrovský	obrovský	k2eAgInSc1d1
pyroklastický	pyroklastický	k2eAgInSc1d1
proud	proud	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
uložil	uložit	k5eAaPmAgInS
do	do	k7c2
okolí	okolí	k1gNnSc2
silné	silný	k2eAgFnSc2d1
vrstvy	vrstva	k1gFnSc2
ignimbritových	ignimbritový	k2eAgFnPc2d1
usazenin	usazenina	k1gFnPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnPc4
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
pozorovat	pozorovat	k5eAaImF
v	v	k7c6
okolí	okolí	k1gNnSc6
města	město	k1gNnSc2
Biancavilla	Biancavill	k1gMnSc2
severozápadně	severozápadně	k6eAd1
od	od	k7c2
Etny	Etna	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
18	#num#	k4
<g/>
]	]	kIx)
Sopečný	sopečný	k2eAgInSc1d1
popel	popel	k1gInSc1
vyvržený	vyvržený	k2eAgInSc1d1
do	do	k7c2
atmosféry	atmosféra	k1gFnSc2
Země	zem	k1gFnSc2
byl	být	k5eAaImAgInS
následně	následně	k6eAd1
unášen	unášet	k5eAaImNgInS
na	na	k7c4
velké	velký	k2eAgFnPc4d1
vzdálenosti	vzdálenost	k1gFnPc4
a	a	k8xC
byl	být	k5eAaImAgInS
pozorován	pozorovat	k5eAaImNgInS
až	až	k9
v	v	k7c6
jezerních	jezerní	k2eAgInPc6d1
sedimentech	sediment	k1gInPc6
v	v	k7c6
Colli	Colle	k1gFnSc6
Albani	Albaň	k1gFnSc6
nedaleko	nedaleko	k7c2
Říma	Řím	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
vzdálený	vzdálený	k2eAgMnSc1d1
přes	přes	k7c4
800	#num#	k4
km	km	kA
severně	severně	k6eAd1
od	od	k7c2
sopky	sopka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Tato	tento	k3xDgFnSc1
událost	událost	k1gFnSc1
byla	být	k5eAaImAgFnS
pravděpodobně	pravděpodobně	k6eAd1
spojena	spojit	k5eAaPmNgFnS
se	s	k7c7
vznikem	vznik	k1gInSc7
3	#num#	k4
kilometry	kilometr	k1gInPc7
široké	široký	k2eAgFnSc2d1
kaldery	kaldera	k1gFnSc2
Ellittico	Ellittico	k1gNnSc1
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
byla	být	k5eAaImAgFnS
později	pozdě	k6eAd2
téměř	téměř	k6eAd1
zcela	zcela	k6eAd1
vyplněná	vyplněný	k2eAgFnSc1d1
mladším	mladý	k2eAgMnSc7d2
eruptivním	eruptivní	k2eAgInSc7d1
materiálem	materiál	k1gInSc7
a	a	k8xC
pohlcena	pohltit	k5eAaPmNgFnS
dalším	další	k2eAgInSc7d1
mladším	mladý	k2eAgInSc7d2
kolapsem	kolaps	k1gInSc7
a	a	k8xC
vznikem	vznik	k1gInSc7
další	další	k2eAgFnSc2d1
kaldery	kaldera	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Před	před	k7c7
několika	několik	k4yIc7
tisíci	tisíc	k4xCgInPc7
lety	léto	k1gNnPc7
došlo	dojít	k5eAaPmAgNnS
na	na	k7c6
východním	východní	k2eAgInSc6d1
svahu	svah	k1gInSc6
hory	hora	k1gFnSc2
ke	k	k7c3
katastrofickému	katastrofický	k2eAgInSc3d1
kolapsu	kolaps	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
se	se	k3xPyFc4
podobal	podobat	k5eAaImAgInS
události	událost	k1gFnSc2
při	při	k7c6
erupci	erupce	k1gFnSc6
Mount	Mount	k1gInSc1
St.	st.	kA
Helens	Helens	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
1980	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Sesuv	sesuv	k1gInSc1
zanechal	zanechat	k5eAaPmAgInS
za	za	k7c7
sebou	se	k3xPyFc7
obrovskou	obrovský	k2eAgFnSc4d1
depresi	deprese	k1gFnSc4
na	na	k7c6
straně	strana	k1gFnSc6
sopky	sopka	k1gFnSc2
známou	známý	k2eAgFnSc7d1
jako	jako	k8xC,k8xS
Valle	Valle	k1gNnSc7
del	del	k?
Bove	Bove	k1gFnSc1
(	(	kIx(
<g/>
údolí	údolí	k1gNnSc1
Býků	býk	k1gMnPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Takto	takto	k6eAd1
vzniklá	vzniklý	k2eAgFnSc1d1
kaldera	kaldera	k1gFnSc1
má	mít	k5eAaImIp3nS
v	v	k7c6
současnosti	současnost	k1gFnSc6
rozměry	rozměr	k1gInPc4
5	#num#	k4
×	×	k?
7	#num#	k4
km	km	kA
a	a	k8xC
je	být	k5eAaImIp3nS
otevřena	otevřít	k5eAaPmNgFnS
směrem	směr	k1gInSc7
k	k	k7c3
východu	východ	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
17	#num#	k4
<g/>
]	]	kIx)
Výzkum	výzkum	k1gInSc1
publikovaný	publikovaný	k2eAgInSc1d1
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
naznačuje	naznačovat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
k	k	k7c3
této	tento	k3xDgFnSc3
události	událost	k1gFnSc3
došlo	dojít	k5eAaPmAgNnS
přibližně	přibližně	k6eAd1
kolem	kolem	k7c2
roku	rok	k1gInSc2
6000	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
a	a	k8xC
vedl	vést	k5eAaImAgInS
ke	k	k7c3
vzniku	vznik	k1gInSc3
tsunami	tsunami	k1gNnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
zdevastovala	zdevastovat	k5eAaPmAgFnS
část	část	k1gFnSc4
východní	východní	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
Středomoří	středomoří	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
do	do	k7c2
dnešních	dnešní	k2eAgInPc2d1
dnů	den	k1gInPc2
po	po	k7c6
ní	on	k3xPp3gFnSc6
zachovaly	zachovat	k5eAaPmAgFnP
geologické	geologický	k2eAgFnPc1d1
důkazy	důkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mohlo	moct	k5eAaImAgNnS
by	by	kYmCp3nS
se	se	k3xPyFc4
jednat	jednat	k5eAaImF
i	i	k9
o	o	k7c4
důvod	důvod	k1gInSc4
<g/>
,	,	kIx,
proč	proč	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
náhlému	náhlý	k2eAgNnSc3d1
opuštění	opuštění	k1gNnSc3
osídlení	osídlení	k1gNnSc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
Atlit	Atlit	k2eAgInSc4d1
Jam	jam	k1gInSc4
v	v	k7c6
Izraeli	Izrael	k1gInSc6
v	v	k7c6
této	tento	k3xDgFnSc6
době	doba	k1gFnSc6
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
leží	ležet	k5eAaImIp3nP
v	v	k7c6
oblasti	oblast	k1gFnSc6
nižší	nízký	k2eAgMnSc1d2
<g/>
,	,	kIx,
než	než	k8xS
je	být	k5eAaImIp3nS
mořská	mořský	k2eAgFnSc1d1
hladina	hladina	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Strmé	strmý	k2eAgInPc1d1
svahy	svah	k1gInPc1
oblasti	oblast	k1gFnSc2
Valley	Vallea	k1gFnSc2
se	se	k3xPyFc4
zřítily	zřítit	k5eAaPmAgFnP
v	v	k7c6
historii	historie	k1gFnSc6
několikrát	několikrát	k6eAd1
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
se	se	k3xPyFc4
obnažily	obnažit	k5eAaPmAgFnP
jednotlivé	jednotlivý	k2eAgFnPc1d1
vrstvy	vrstva	k1gFnPc1
sopky	sopka	k1gFnSc2
dávající	dávající	k2eAgFnSc4d1
možnost	možnost	k1gFnSc4
snadno	snadno	k6eAd1
studovat	studovat	k5eAaImF
historii	historie	k1gFnSc4
erupcí	erupce	k1gFnPc2
Etny	Etna	k1gFnSc2
a	a	k8xC
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
změně	změna	k1gFnSc3
tvaru	tvar	k1gInSc2
původní	původní	k2eAgFnSc2d1
kaldery	kaldera	k1gFnSc2
do	do	k7c2
současného	současný	k2eAgMnSc2d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Nejmladší	mladý	k2eAgInSc1d3
kolaps	kolaps	k1gInSc1
stěny	stěna	k1gFnSc2
kráteru	kráter	k1gInSc2
Etny	Etna	k1gFnSc2
se	se	k3xPyFc4
odehrál	odehrát	k5eAaPmAgInS
přibližně	přibližně	k6eAd1
před	před	k7c7
2000	#num#	k4
lety	léto	k1gNnPc7
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
vytvoření	vytvoření	k1gNnSc3
současně	současně	k6eAd1
se	se	k3xPyFc4
vyvíjející	vyvíjející	k2eAgFnPc1d1
kaldery	kaldera	k1gFnPc1
známé	známý	k2eAgFnPc1d1
jako	jako	k8xC,k8xS
Piano	piano	k1gNnSc1
Caldera	Caldero	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpokládá	předpokládat	k5eAaImIp3nS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
vznik	vznik	k1gInSc1
této	tento	k3xDgFnSc2
dva	dva	k4xCgInPc4
a	a	k8xC
půl	půl	k1xP
kilometru	kilometr	k1gInSc2
široké	široký	k2eAgFnSc2d1
kaldery	kaldera	k1gFnSc2
se	se	k3xPyFc4
odehrál	odehrát	k5eAaPmAgInS
v	v	k7c6
roce	rok	k1gInSc6
122	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
ve	v	k7c6
spojitosti	spojitost	k1gFnSc6
s	s	k7c7
explozivní	explozivní	k2eAgFnSc7d1
erupcí	erupce	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
se	se	k3xPyFc4
toho	ten	k3xDgInSc2
roku	rok	k1gInSc2
odehrála	odehrát	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Tato	tento	k3xDgFnSc1
deprese	deprese	k1gFnSc1
byla	být	k5eAaImAgFnS
později	pozdě	k6eAd2
téměř	téměř	k6eAd1
celá	celý	k2eAgFnSc1d1
vyplněna	vyplněn	k2eAgFnSc1d1
mladší	mladý	k2eAgFnSc7d2
lávou	láva	k1gFnSc7
vzniklou	vzniklý	k2eAgFnSc4d1
mladšími	mladý	k2eAgFnPc7d2
erupcemi	erupce	k1gFnPc7
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
tak	tak	k9
je	být	k5eAaImIp3nS
její	její	k3xOp3gInSc4
okraj	okraj	k1gInSc4
stále	stále	k6eAd1
dobře	dobře	k6eAd1
patrný	patrný	k2eAgInSc1d1
jako	jako	k8xC,k8xS
příkře	příkro	k6eAd1
vystupující	vystupující	k2eAgInSc1d1
svah	svah	k1gInSc1
poblíž	poblíž	k7c2
základny	základna	k1gFnSc2
dnešního	dnešní	k2eAgInSc2d1
nového	nový	k2eAgInSc2d1
kužele	kužel	k1gInSc2
vznikajícího	vznikající	k2eAgInSc2d1
v	v	k7c6
oblasti	oblast	k1gFnSc6
kráteru	kráter	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
stále	stále	k6eAd1
probíhají	probíhat	k5eAaImIp3nP
pochody	pochod	k1gInPc1
měnící	měnící	k2eAgInSc1d1
vzhled	vzhled	k1gInSc4
kalder	kaldero	k1gNnPc2
a	a	k8xC
tvaru	tvar	k1gInSc2
sopky	sopka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedna	jeden	k4xCgFnSc1
menší	malý	k2eAgFnSc1d2
událost	událost	k1gFnSc1
se	se	k3xPyFc4
odehrála	odehrát	k5eAaPmAgFnS
například	například	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
1669	#num#	k4
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
dá	dát	k5eAaPmIp3nS
se	se	k3xPyFc4
předpokládat	předpokládat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
i	i	k9
do	do	k7c2
budoucna	budoucno	k1gNnSc2
podobné	podobný	k2eAgInPc1d1
pochody	pochod	k1gInPc1
budou	být	k5eAaImBp3nP
nastávat	nastávat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Výzkum	výzkum	k1gInSc1
podloží	podloží	k1gNnSc2
Etny	Etna	k1gFnSc2
ukázal	ukázat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
Mohorovičićova	Mohorovičićův	k2eAgFnSc1d1
diskontinuita	diskontinuita	k1gFnSc1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
hloubce	hloubka	k1gFnSc6
27	#num#	k4
km	km	kA
pod	pod	k7c7
Etnou	Etna	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Celá	celý	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
Etny	Etna	k1gFnSc2
se	se	k3xPyFc4
pozvolna	pozvolna	k6eAd1
vyzvedává	vyzvedávat	k5eAaImIp3nS
rychlostí	rychlost	k1gFnSc7
1	#num#	k4
mm	mm	kA
<g/>
/	/	kIx~
<g/>
rok	rok	k1gInSc4
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
dokládají	dokládat	k5eAaImIp3nP
pleistocéní	pleistocéní	k2eAgInPc1d1
mořské	mořský	k2eAgInPc1d1
sedimenty	sediment	k1gInPc1
na	na	k7c6
jihovýchodních	jihovýchodní	k2eAgInPc6d1
svazích	svah	k1gInPc6
Etny	Etna	k1gFnSc2
ve	v	k7c6
výšce	výška	k1gFnSc6
400	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Erupce	erupce	k1gFnSc1
</s>
<s>
Vyobrazení	vyobrazení	k1gNnSc1
erupce	erupce	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
1669	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Nejstarší	starý	k2eAgInPc1d3
záznamy	záznam	k1gInPc1
o	o	k7c6
erupcích	erupce	k1gFnPc6
Etny	Etna	k1gFnSc2
jsou	být	k5eAaImIp3nP
již	již	k6eAd1
ze	z	k7c2
starověku	starověk	k1gInSc2
(	(	kIx(
<g/>
135	#num#	k4
n.	n.	k?
l.	l.	k?
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
jedné	jeden	k4xCgFnSc3
z	z	k7c2
nejmohutnějších	mohutný	k2eAgFnPc2d3
erupcí	erupce	k1gFnPc2
došlo	dojít	k5eAaPmAgNnS
v	v	k7c6
roce	rok	k1gInSc6
1669	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
láva	láva	k1gFnSc1
dosáhla	dosáhnout	k5eAaPmAgFnS
města	město	k1gNnSc2
Catanie	Catanie	k1gFnSc2
<g/>
,	,	kIx,
jež	jenž	k3xRgFnSc1
částečně	částečně	k6eAd1
poničila	poničit	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Další	další	k2eAgFnPc1d1
výjimečné	výjimečný	k2eAgFnPc1d1
erupce	erupce	k1gFnPc1
byly	být	k5eAaImAgFnP
v	v	k7c6
roce	rok	k1gInSc6
1910	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
vytvořilo	vytvořit	k5eAaPmAgNnS
23	#num#	k4
nových	nový	k2eAgInPc2d1
kráterů	kráter	k1gInPc2
<g/>
,	,	kIx,
v	v	k7c6
roce	rok	k1gInSc6
1917	#num#	k4
(	(	kIx(
<g/>
láva	láva	k1gFnSc1
tryskala	tryskat	k5eAaImAgFnS
do	do	k7c2
výšky	výška	k1gFnSc2
až	až	k9
800	#num#	k4
m	m	kA
<g/>
)	)	kIx)
a	a	k8xC
v	v	k7c6
roce	rok	k1gInSc6
1923	#num#	k4
–	–	k?
po	po	k7c6
této	tento	k3xDgFnSc6
erupci	erupce	k1gFnSc6
láva	láva	k1gFnSc1
vydržela	vydržet	k5eAaPmAgFnS
horká	horký	k2eAgFnSc1d1
18	#num#	k4
měsíců	měsíc	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Poslední	poslední	k2eAgFnPc1d1
erupce	erupce	k1gFnPc1
jsou	být	k5eAaImIp3nP
z	z	k7c2
let	léto	k1gNnPc2
1928	#num#	k4
<g/>
,	,	kIx,
1954	#num#	k4
<g/>
,	,	kIx,
1964	#num#	k4
<g/>
,	,	kIx,
1971	#num#	k4
<g/>
,	,	kIx,
1974	#num#	k4
<g/>
,	,	kIx,
1978	#num#	k4
<g/>
,	,	kIx,
1979	#num#	k4
<g/>
,	,	kIx,
1981	#num#	k4
<g/>
,	,	kIx,
1983	#num#	k4
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
,	,	kIx,
1986	#num#	k4
<g/>
,	,	kIx,
1992	#num#	k4
<g/>
,	,	kIx,
2000	#num#	k4
<g/>
,	,	kIx,
2001	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
,	,	kIx,
2006	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
,	,	kIx,
2008	#num#	k4
<g/>
,	,	kIx,
2011	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
a	a	k8xC
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
je	být	k5eAaImIp3nS
sopka	sopka	k1gFnSc1
činná	činný	k2eAgFnSc1d1
v	v	k7c6
podstatě	podstata	k1gFnSc6
nepřetržitě	přetržitě	k6eNd1
<g/>
.	.	kIx.
</s>
<s>
Historické	historický	k2eAgFnPc1d1
erupce	erupce	k1gFnPc1
</s>
<s>
Erupce	erupce	k1gFnPc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
Etny	Etna	k1gFnSc2
mívají	mívat	k5eAaImIp3nP
různý	různý	k2eAgInSc4d1
charakter	charakter	k1gInSc4
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
se	se	k3xPyFc4
v	v	k7c6
průběhu	průběh	k1gInSc6
času	čas	k1gInSc2
měnil	měnit	k5eAaImAgInS
a	a	k8xC
stále	stále	k6eAd1
mění	měnit	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Některé	některý	k3yIgFnPc1
erupce	erupce	k1gFnPc1
se	se	k3xPyFc4
odehrávají	odehrávat	k5eAaImIp3nP
přímo	přímo	k6eAd1
v	v	k7c6
místě	místo	k1gNnSc6
jícnu	jícen	k1gInSc2
(	(	kIx(
<g/>
např.	např.	kA
erupce	erupce	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
2008	#num#	k4
<g/>
)	)	kIx)
tvořeného	tvořený	k2eAgInSc2d1
čtyřmi	čtyři	k4xCgNnPc7
krátery	kráter	k1gInPc1
–	–	k?
Severovýchodním	severovýchodní	k2eAgInSc7d1
kráterem	kráter	k1gInSc7
<g/>
,	,	kIx,
Jihovýchodním	jihovýchodní	k2eAgInSc7d1
kráterem	kráter	k1gInSc7
a	a	k8xC
krátery	kráter	k1gInPc1
Voragine	Voragin	k1gInSc5
a	a	k8xC
Bocca	Bocc	k2eAgNnPc4d1
Nuova	Nuovo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnPc1d1
erupce	erupce	k1gFnPc1
se	se	k3xPyFc4
odehrávají	odehrávat	k5eAaImIp3nP
na	na	k7c6
svazích	svah	k1gInPc6
sopky	sopka	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
více	hodně	k6eAd2
než	než	k8xS
300	#num#	k4
depresí	deprese	k1gFnPc2
od	od	k7c2
malých	malý	k2eAgFnPc2d1
děr	děra	k1gFnPc2
až	až	k9
po	po	k7c4
velké	velký	k2eAgInPc4d1
krátery	kráter	k1gInPc4
široké	široký	k2eAgFnSc2d1
stovky	stovka	k1gFnSc2
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
ze	z	k7c2
kterých	který	k3yQgFnPc2,k3yIgFnPc2,k3yRgFnPc2
se	se	k3xPyFc4
nepravidelně	pravidelně	k6eNd1
valí	valit	k5eAaImIp3nP
magma	magma	k1gNnSc4
na	na	k7c4
povrch	povrch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Erupce	erupce	k1gFnSc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
odehrávají	odehrávat	k5eAaImIp3nP
na	na	k7c6
vrcholu	vrchol	k1gInSc6
sopky	sopka	k1gFnSc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
často	často	k6eAd1
explozivního	explozivní	k2eAgInSc2d1
charakteru	charakter	k1gInSc2
s	s	k7c7
charakteristickým	charakteristický	k2eAgInSc7d1
sopečným	sopečný	k2eAgInSc7d1
oblakem	oblak	k1gInSc7
<g/>
,	,	kIx,
ale	ale	k8xC
jen	jen	k9
málokdy	málokdy	k6eAd1
jsou	být	k5eAaImIp3nP
nebezpečné	bezpečný	k2eNgFnPc1d1
pro	pro	k7c4
obyvatele	obyvatel	k1gMnPc4
žijící	žijící	k2eAgMnPc4d1
poblíž	poblíž	k7c2
sopky	sopka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naproti	naproti	k7c3
tomu	ten	k3xDgNnSc3
erupce	erupce	k1gFnPc1
na	na	k7c6
svazích	svah	k1gInPc6
sopky	sopka	k1gFnSc2
se	se	k3xPyFc4
občas	občas	k6eAd1
odehrávají	odehrávat	k5eAaImIp3nP
o	o	k7c4
stovky	stovka	k1gFnPc4
metrů	metr	k1gInPc2
níže	nízce	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
je	být	k5eAaImIp3nS
vrchol	vrchol	k1gInSc4
sopky	sopka	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
také	také	k9
blíže	blízce	k6eAd2
k	k	k7c3
obydleným	obydlený	k2eAgFnPc3d1
oblastem	oblast	k1gFnPc3
<g/>
,	,	kIx,
kde	kde	k6eAd1
mohou	moct	k5eAaImIp3nP
způsobit	způsobit	k5eAaPmF
materiální	materiální	k2eAgFnPc4d1
škody	škoda	k1gFnPc4
i	i	k8xC
oběti	oběť	k1gFnPc4
na	na	k7c6
životech	život	k1gInPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mnoho	mnoho	k6eAd1
vesnic	vesnice	k1gFnPc2
a	a	k8xC
malých	malý	k2eAgNnPc2d1
městeček	městečko	k1gNnPc2
leží	ležet	k5eAaImIp3nP
okolo	okolo	k7c2
kužele	kužel	k1gInSc2
sopky	sopka	k1gFnSc2
či	či	k8xC
přímo	přímo	k6eAd1
na	na	k7c6
ní	on	k3xPp3gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
tak	tak	k9
mohou	moct	k5eAaImIp3nP
být	být	k5eAaImF
těmito	tento	k3xDgFnPc7
výlevy	výlev	k1gInPc1
snadno	snadno	k6eAd1
zasaženy	zasažen	k2eAgInPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
roku	rok	k1gInSc2
1600	#num#	k4
se	se	k3xPyFc4
na	na	k7c6
Etně	Etna	k1gFnSc6
odehrálo	odehrát	k5eAaPmAgNnS
minimálně	minimálně	k6eAd1
60	#num#	k4
sopečných	sopečný	k2eAgFnPc2d1
erupcí	erupce	k1gFnPc2
<g/>
,	,	kIx,
skoro	skoro	k6eAd1
polovina	polovina	k1gFnSc1
z	z	k7c2
nich	on	k3xPp3gFnPc2
se	se	k3xPyFc4
odehrála	odehrát	k5eAaPmAgFnS
od	od	k7c2
začátku	začátek	k1gInSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
začátku	začátek	k1gInSc2
třetího	třetí	k4xOgNnSc2
tisíciletí	tisíciletí	k1gNnPc4
došlo	dojít	k5eAaPmAgNnS
na	na	k7c6
Etně	Etna	k1gFnSc6
již	již	k6eAd1
k	k	k7c3
5	#num#	k4
erupcím	erupce	k1gFnPc3
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
letech	léto	k1gNnPc6
2001	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
–	–	k?
<g/>
2003	#num#	k4
<g/>
,	,	kIx,
2004	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
<g/>
,	,	kIx,
2007	#num#	k4
a	a	k8xC
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
První	první	k4xOgInSc1
záznam	záznam	k1gInSc1
o	o	k7c6
erupci	erupce	k1gFnSc6
Etny	Etna	k1gFnSc2
pochází	pocházet	k5eAaImIp3nS
od	od	k7c2
řeckého	řecký	k2eAgMnSc2d1
historika	historik	k1gMnSc2
Diodóra	Diodór	k1gInSc2
Sicilského	sicilský	k2eAgInSc2d1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
žil	žít	k5eAaImAgInS
v	v	k7c6
prvním	první	k4xOgNnSc6
století	století	k1gNnSc6
před	před	k7c7
naším	náš	k3xOp1gInSc7
letopočtem	letopočet	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
nejspíše	nejspíše	k9
první	první	k4xOgInSc1
popis	popis	k1gInSc1
erupce	erupce	k1gFnSc2
pochází	pocházet	k5eAaImIp3nS
od	od	k7c2
římského	římský	k2eAgMnSc2d1
básníka	básník	k1gMnSc2
Publia	Publius	k1gMnSc2
Vergilia	Vergilius	k1gMnSc2
Marona	Maron	k1gMnSc2
v	v	k7c6
jeho	jeho	k3xOp3gInSc6
eposu	epos	k1gInSc6
Aeneis	Aeneis	k1gFnSc1
<g/>
:	:	kIx,
</s>
<s>
Erupce	erupce	k1gFnSc1
Etny	Etna	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
<g/>
,	,	kIx,
snímek	snímek	k1gInSc4
pořízen	pořízen	k2eAgInSc4d1
z	z	k7c2
Mezinárodní	mezinárodní	k2eAgFnSc2d1
vesmírné	vesmírný	k2eAgFnSc2d1
stanice	stanice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Portus	Portus	k1gInSc1
ab	ab	k?
accessu	access	k1gInSc2
ventorum	ventorum	k1gInSc1
immotus	immotus	k1gInSc1
et	et	k?
ingens	ingens	k6eAd1
</s>
<s>
ipse	ipse	k1gFnSc1
<g/>
;	;	kIx,
sed	sed	k1gInSc1
horrificis	horrificis	k1gFnPc2
iuxta	iuxt	k1gInSc2
tonat	tonat	k1gInSc4
Aetna	Aetno	k1gNnSc2
ruinis	ruinis	k1gFnSc2
<g/>
;	;	kIx,
</s>
<s>
interdumque	interdumque	k6eAd1
atram	atram	k6eAd1
prorumpit	prorumpit	k5eAaPmF
ad	ad	k7c4
aethera	aether	k1gMnSc4
nubem	nubem	k6eAd1
<g/>
,	,	kIx,
</s>
<s>
turbine	turbinout	k5eAaPmIp3nS
fumantem	fumant	k1gInSc7
piceo	piceo	k6eAd1
et	et	k?
candente	candent	k1gMnSc5
favilla	favillo	k1gNnPc5
<g/>
,	,	kIx,
</s>
<s>
attollitque	attollitque	k1gInSc1
globos	globos	k1gInSc1
flammarum	flammarum	k1gInSc4
et	et	k?
sidera	sidero	k1gNnSc2
lambit	lambit	k5eAaImF,k5eAaPmF
<g/>
;	;	kIx,
</s>
<s>
interdum	interdum	k1gNnSc1
scopulos	scopulosa	k1gFnPc2
avolsaque	avolsaquat	k5eAaPmIp3nS
viscera	viscera	k1gFnSc1
montis	montis	k1gFnSc1
</s>
<s>
erigit	erigit	k2eAgInSc1d1
eructans	eructans	k1gInSc1
<g/>
,	,	kIx,
liquefactaque	liquefactaquat	k5eAaPmIp3nS
saxa	saxa	k6eAd1
sub	sub	k7c4
auras	auras	k1gInSc4
</s>
<s>
cum	cum	k?
gemitu	gemita	k1gFnSc4
glomerat	glomerat	k5eAaPmF,k5eAaImF
<g/>
,	,	kIx,
fundoque	fundoque	k6eAd1
exaestuat	exaestuat	k1gInSc1
imo	imo	k?
<g/>
.	.	kIx.
</s>
<s>
(	(	kIx(
<g/>
3.39	3.39	k4
<g/>
)	)	kIx)
</s>
<s>
Přístav	přístav	k1gInSc1
nesmírný	smírný	k2eNgInSc1d1
a	a	k8xC
nedotknut	dotknout	k5eNaPmNgInS
přístupem	přístup	k1gInSc7
větrů	vítr	k1gInPc2
</s>
<s>
tam	tam	k6eAd1
sice	sice	k8xC
jest	být	k5eAaImIp3nS
<g/>
,	,	kIx,
ale	ale	k8xC
Aetny	Aetna	k1gFnSc2
oheň	oheň	k1gInSc1
v	v	k7c6
hoře	hora	k1gFnSc6
strašlivě	strašlivě	k6eAd1
hřímá	hřímat	k5eAaImIp3nS
<g/>
,	,	kIx,
</s>
<s>
někdy	někdy	k6eAd1
ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
k	k	k7c3
nebesům	nebesa	k1gNnPc3
vír	vír	k1gInSc1
dýmu	dým	k1gInSc3
vymršťuje	vymršťovat	k5eAaImIp3nS
černý	černý	k2eAgInSc4d1
</s>
<s>
a	a	k8xC
z	z	k7c2
chomolů	chomol	k1gInPc2
smolných	smolný	k2eAgNnPc2d1
sype	sypat	k5eAaImIp3nS
hustý	hustý	k2eAgInSc1d1
popela	popel	k1gInSc2
příval	příval	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
a	a	k8xC
znova	znova	k6eAd1
vyšlehaným	vyšlehaný	k2eAgInSc7d1
plamenem	plamen	k1gInSc7
nebes	nebesa	k1gNnPc2
oblohu	obloha	k1gFnSc4
líže	lízat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s>
Někdy	někdy	k6eAd1
celé	celý	k2eAgFnSc2d1
skaliny	skalina	k1gFnSc2
<g/>
,	,	kIx,
odtrhlé	odtrhlý	k2eAgFnSc6d1
útrobě	útroba	k1gFnSc6
vlastní	vlastní	k2eAgFnSc6d1
<g/>
,	,	kIx,
</s>
<s>
plije	plije	k1gFnSc1
a	a	k8xC
roztopené	roztopený	k2eAgNnSc1d1
kamení	kamení	k1gNnSc1
hází	házet	k5eAaImIp3nS
do	do	k7c2
povětří	povětří	k1gNnSc2
</s>
<s>
s	s	k7c7
ohromným	ohromný	k2eAgInSc7d1
stonotem	stonot	k1gInSc7
a	a	k8xC
řváním	řvání	k1gNnSc7
ze	z	k7c2
dna	dno	k1gNnSc2
propasti	propast	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
—	—	k?
<g/>
Vergilius	Vergilius	k1gMnSc1
<g/>
,	,	kIx,
Aeneis	Aeneis	k1gFnSc1
<g/>
,	,	kIx,
kniha	kniha	k1gFnSc1
třetí	třetí	k4xOgFnSc1
<g/>
,	,	kIx,
verše	verš	k1gInPc1
570	#num#	k4
<g/>
–	–	k?
<g/>
577	#num#	k4
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
396	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
erupce	erupce	k1gFnSc2
Etny	Etna	k1gFnSc2
údajně	údajně	k6eAd1
zmařila	zmařit	k5eAaPmAgFnS
snahu	snaha	k1gFnSc4
Kartáginců	Kartáginec	k1gInPc2
proniknout	proniknout	k5eAaPmF
do	do	k7c2
Syrakus	Syrakusy	k1gFnPc2
v	v	k7c6
době	doba	k1gFnSc6
první	první	k4xOgFnSc2
sicilské	sicilský	k2eAgFnSc2d1
války	válka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Pohled	pohled	k1gInSc1
na	na	k7c6
noční	noční	k2eAgFnSc6d1
erupci	erupce	k1gFnSc6
Etny	Etna	k1gFnSc2
v	v	k7c6
září	září	k1gNnSc6
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pozorovaná	pozorovaný	k2eAgFnSc1d1
z	z	k7c2
oblasti	oblast	k1gFnSc2
jihovýchodního	jihovýchodní	k2eAgInSc2d1
okraje	okraj	k1gInSc2
kráteru	kráter	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Silná	silný	k2eAgFnSc1d1
explozivní	explozivní	k2eAgFnSc1d1
erupce	erupce	k1gFnSc1
pliniovského	pliniovský	k2eAgInSc2d1
typu	typ	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
vytvořila	vytvořit	k5eAaPmAgFnS
pyroklastický	pyroklastický	k2eAgInSc4d1
sloup	sloup	k1gInSc4
tvořený	tvořený	k2eAgInSc4d1
plyny	plyn	k1gInPc4
<g/>
,	,	kIx,
popelem	popel	k1gInSc7
a	a	k8xC
dalšími	další	k2eAgFnPc7d1
pevnými	pevný	k2eAgFnPc7d1
částečkami	částečka	k1gFnPc7
<g/>
,	,	kIx,
se	se	k3xPyFc4
odehrála	odehrát	k5eAaPmAgFnS
kolem	kolem	k7c2
roku	rok	k1gInSc2
122	#num#	k4
př	př	kA
<g/>
.	.	kIx.
n.	n.	k?
l.	l.	k?
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
projevilo	projevit	k5eAaPmAgNnS
silným	silný	k2eAgInSc7d1
spadem	spad	k1gInSc7
tefry	tefra	k1gFnSc2
jihovýchodně	jihovýchodně	k6eAd1
od	od	k7c2
sopky	sopka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zasaženo	zasažen	k2eAgNnSc1d1
bylo	být	k5eAaImAgNnS
například	například	k6eAd1
město	město	k1gNnSc1
Catania	Catanium	k1gNnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
silná	silný	k2eAgFnSc1d1
vrstva	vrstva	k1gFnSc1
tefry	tefra	k1gFnSc2
způsobila	způsobit	k5eAaPmAgFnS
kolaps	kolaps	k1gInSc4
mnoha	mnoho	k4c2
střech	střecha	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
Jako	jako	k8xS,k8xC
pomoc	pomoc	k1gFnSc4
postižené	postižený	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
rozhodla	rozhodnout	k5eAaPmAgFnS
tehdejší	tehdejší	k2eAgFnSc1d1
římská	římský	k2eAgFnSc1d1
vláda	vláda	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
postižená	postižený	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
nebude	být	k5eNaImBp3nS
muset	muset	k5eAaImF
platit	platit	k5eAaImF
daně	daň	k1gFnPc4
po	po	k7c4
dobu	doba	k1gFnSc4
deseti	deset	k4xCc2
let	léto	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1669	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
nejsilnější	silný	k2eAgFnSc3d3
erupci	erupce	k1gFnSc3
jejímž	jejíž	k3xOyRp3gInSc7
vlivem	vliv	k1gInSc7
bylo	být	k5eAaImAgNnS
poničeno	poničen	k2eAgNnSc1d1
město	město	k1gNnSc1
Catania	Catanium	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
samotnou	samotný	k2eAgFnSc7d1
erupcí	erupce	k1gFnSc7
oblast	oblast	k1gFnSc4
zasáhlo	zasáhnout	k5eAaPmAgNnS
silné	silný	k2eAgNnSc1d1
zemětřesení	zemětřesení	k1gNnSc1
a	a	k8xC
následně	následně	k6eAd1
11	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
se	se	k3xPyFc4
otevřela	otevřít	k5eAaPmAgFnS
trhlina	trhlina	k1gFnSc1
z	z	k7c2
Nicolosi	Nicolose	k1gFnSc3
až	až	k9
k	k	k7c3
centrálnímu	centrální	k2eAgInSc3d1
kráteru	kráter	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
celé	celý	k2eAgFnSc6d1
délce	délka	k1gFnSc6
trhliny	trhlina	k1gFnSc2
byly	být	k5eAaImAgFnP
aktivní	aktivní	k2eAgFnPc1d1
malé	malý	k2eAgFnPc1d1
sopky	sopka	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
chrlily	chrlit	k5eAaImAgFnP
magma	magma	k1gNnSc4
na	na	k7c4
povrch	povrch	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Současné	současný	k2eAgFnPc1d1
erupce	erupce	k1gFnPc1
</s>
<s>
Erupce	erupce	k1gFnSc1
Etny	Etna	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2002	#num#	k4
při	při	k7c6
pohledu	pohled	k1gInSc6
na	na	k7c4
jihovýchodní	jihovýchodní	k2eAgInSc4d1
kráter	kráter	k1gInSc4
<g/>
,	,	kIx,
fotografie	fotografie	k1gFnSc1
pořízena	pořídit	k5eAaPmNgFnS
z	z	k7c2
oblasti	oblast	k1gFnSc2
Torre	torr	k1gInSc5
del	del	k?
Filosofo	Filosofo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgInSc1d1
velký	velký	k2eAgInSc1d1
lávový	lávový	k2eAgInSc1d1
proud	proud	k1gInSc1
způsobený	způsobený	k2eAgInSc1d1
erupcí	erupce	k1gFnSc7
v	v	k7c6
roce	rok	k1gInSc6
1928	#num#	k4
byl	být	k5eAaImAgInS
první	první	k4xOgInSc4
a	a	k8xC
od	od	k7c2
roku	rok	k1gInSc2
1669	#num#	k4
i	i	k8xC
jediný	jediný	k2eAgInSc1d1
lávový	lávový	k2eAgInSc1d1
proud	proud	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
svojí	svůj	k3xOyFgFnSc7
silou	síla	k1gFnSc7
zasáhl	zasáhnout	k5eAaPmAgMnS
hustě	hustě	k6eAd1
obydlenou	obydlený	k2eAgFnSc4d1
oblast	oblast	k1gFnSc4
v	v	k7c6
okolí	okolí	k1gNnSc6
Mascali	Mascali	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Erupce	erupce	k1gFnSc1
začala	začít	k5eAaPmAgFnS
2	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
1928	#num#	k4
vysoko	vysoko	k6eAd1
na	na	k7c6
severovýchodní	severovýchodní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
sopky	sopka	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
později	pozdě	k6eAd2
se	se	k3xPyFc4
otevřela	otevřít	k5eAaPmAgFnS
nová	nový	k2eAgFnSc1d1
trhlina	trhlina	k1gFnSc1
níže	níže	k1gFnSc2
<g/>
,	,	kIx,
ze	z	k7c2
které	který	k3yRgFnSc2,k3yIgFnSc2,k3yQgFnSc2
na	na	k7c4
povrch	povrch	k1gInSc4
začala	začít	k5eAaPmAgFnS
proudit	proudit	k5eAaImF,k5eAaPmF
další	další	k2eAgFnSc1d1
láva	láva	k1gFnSc1
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
se	se	k3xPyFc4
následně	následně	k6eAd1
otevřela	otevřít	k5eAaPmAgFnS
další	další	k2eAgFnSc1d1
trhlina	trhlina	k1gFnSc1
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
měl	mít	k5eAaImAgMnS
lávový	lávový	k2eAgInSc4d1
proud	proud	k1gInSc4
již	již	k6eAd1
tři	tři	k4xCgInPc4
zdroje	zdroj	k1gInPc4
roztaveného	roztavený	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
trhlina	trhlina	k1gFnSc1
se	se	k3xPyFc4
otevřela	otevřít	k5eAaPmAgFnS
ve	v	k7c6
výšce	výška	k1gFnSc6
1200	#num#	k4
metrů	metr	k1gInPc2
nad	nad	k7c7
mořem	moře	k1gNnSc7
<g/>
,	,	kIx,
zcela	zcela	k6eAd1
netypicky	typicky	k6eNd1
nízko	nízko	k6eAd1
<g/>
,	,	kIx,
v	v	k7c6
oblasti	oblast	k1gFnSc6
známé	známý	k2eAgFnPc4d1
jako	jako	k9
Ripe	Ripe	k1gFnSc4
della	dell	k1gMnSc2
Naca	Nacus	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
touto	tento	k3xDgFnSc7
oblastí	oblast	k1gFnSc7
se	se	k3xPyFc4
nacházela	nacházet	k5eAaImAgFnS
vesnice	vesnice	k1gFnSc1
Mascali	Mascali	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
během	během	k7c2
dvou	dva	k4xCgInPc2
dnů	den	k1gInPc2
téměř	téměř	k6eAd1
zcela	zcela	k6eAd1
zničena	zničit	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouze	pouze	k6eAd1
kostel	kostel	k1gInSc1
a	a	k8xC
několik	několik	k4yIc1
málo	málo	k4c1
okolo	okolo	k7c2
stojících	stojící	k2eAgFnPc2d1
budov	budova	k1gFnPc2
v	v	k7c6
severní	severní	k2eAgFnSc6d1
části	část	k1gFnSc6
obce	obec	k1gFnSc2
zvané	zvaný	k2eAgFnSc2d1
Sant	Sant	k2eAgInSc4d1
<g/>
'	'	kIx"
<g/>
Antonino	Antonin	k2eAgNnSc1d1
tuto	tento	k3xDgFnSc4
událost	událost	k1gFnSc4
přežilo	přežít	k5eAaPmAgNnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
několika	několik	k4yIc2
dní	den	k1gInPc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
erupce	erupce	k1gFnPc1
pokračovaly	pokračovat	k5eAaImAgFnP
<g/>
,	,	kIx,
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
přerušení	přerušení	k1gNnSc3
vlakového	vlakový	k2eAgNnSc2d1
spojení	spojení	k1gNnSc2
mezi	mezi	k7c7
Messinou	Messina	k1gFnSc7
a	a	k8xC
Catanií	Catanie	k1gFnSc7
a	a	k8xC
zničení	zničení	k1gNnSc1
vlakové	vlakový	k2eAgFnSc2d1
zastávky	zastávka	k1gFnSc2
v	v	k7c6
obci	obec	k1gFnSc6
Mascali	Mascali	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Událost	událost	k1gFnSc1
byla	být	k5eAaImAgFnS
následně	následně	k6eAd1
využita	využít	k5eAaPmNgFnS
italským	italský	k2eAgInSc7d1
fašistickým	fašistický	k2eAgInSc7d1
režimem	režim	k1gInSc7
Benita	Benit	k1gMnSc2
Musolinniho	Musolinni	k1gMnSc2
k	k	k7c3
propagandistickým	propagandistický	k2eAgInPc3d1
účelům	účel	k1gInPc3
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
byla	být	k5eAaImAgFnS
oslavována	oslavován	k2eAgFnSc1d1
rychlá	rychlý	k2eAgFnSc1d1
první	první	k4xOgFnSc1
pomoc	pomoc	k1gFnSc1
<g/>
,	,	kIx,
evakuace	evakuace	k1gFnSc2
a	a	k8xC
znovuobnova	znovuobnov	k1gInSc2
poničených	poničený	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obec	obec	k1gFnSc1
Mascali	Mascali	k1gFnSc1
byla	být	k5eAaImAgFnS
později	pozdě	k6eAd2
vybudována	vybudovat	k5eAaPmNgFnS
na	na	k7c6
jiném	jiný	k2eAgNnSc6d1
místě	místo	k1gNnSc6
a	a	k8xC
nad	nad	k7c7
sochou	socha	k1gFnSc7
Ježíše	Ježíš	k1gMnSc2
Krista	Kristus	k1gMnSc2
před	před	k7c7
přeživším	přeživší	k2eAgInSc7d1
kostelem	kostel	k1gInSc7
byla	být	k5eAaImAgFnS
umístěna	umístěn	k2eAgFnSc1d1
pochodeň	pochodeň	k1gFnSc1
–	–	k?
symbol	symbol	k1gInSc4
fašistů	fašista	k1gMnPc2
v	v	k7c6
Itálii	Itálie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
listopadu	listopad	k1gInSc6
2008	#num#	k4
proběhla	proběhnout	k5eAaPmAgFnS
v	v	k7c6
obci	obec	k1gFnSc6
připomínková	připomínkový	k2eAgFnSc1d1
akce	akce	k1gFnSc1
zničení	zničení	k1gNnSc2
obce	obec	k1gFnSc2
za	za	k7c2
účasti	účast	k1gFnSc2
řady	řada	k1gFnSc2
pamětníků	pamětník	k1gMnPc2
a	a	k8xC
očitých	očitý	k2eAgMnPc2d1
svědků	svědek	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
popisovali	popisovat	k5eAaImAgMnP
jejich	jejich	k3xOp3gInPc4
zážitky	zážitek	k1gInPc4
z	z	k7c2
tehdejší	tehdejší	k2eAgFnSc2d1
erupce	erupce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgFnPc1d1
významné	významný	k2eAgFnPc1d1
erupce	erupce	k1gFnPc1
ve	v	k7c6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
se	se	k3xPyFc4
odehrály	odehrát	k5eAaPmAgFnP
v	v	k7c6
letech	léto	k1gNnPc6
1949	#num#	k4
<g/>
,	,	kIx,
1971	#num#	k4
<g/>
,	,	kIx,
1981	#num#	k4
<g/>
,	,	kIx,
1983	#num#	k4
a	a	k8xC
1991	#num#	k4
<g/>
–	–	k?
<g/>
1993	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
1971	#num#	k4
láva	láva	k1gFnSc1
pohřbila	pohřbít	k5eAaPmAgFnS
astronomickou	astronomický	k2eAgFnSc4d1
observatoř	observatoř	k1gFnSc4
Etna	Etna	k1gFnSc1
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
vybudovanou	vybudovaný	k2eAgFnSc4d1
v	v	k7c4
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
zničila	zničit	k5eAaPmAgFnS
první	první	k4xOgFnSc4
generaci	generace	k1gFnSc4
lanovky	lanovka	k1gFnSc2
vedoucí	vedoucí	k2eAgFnSc4d1
k	k	k7c3
vrcholu	vrchol	k1gInSc3
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
vážně	vážně	k6eAd1
ohrožovala	ohrožovat	k5eAaImAgFnS
existenci	existence	k1gFnSc4
obcí	obec	k1gFnPc2
Sant	Sant	k2eAgMnSc1d1
Alfio	Alfio	k1gMnSc1
a	a	k8xC
Fornazzo	Fornazza	k1gFnSc5
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
na	na	k7c6
východním	východní	k2eAgNnSc6d1
úbočí	úbočí	k1gNnSc6
sopky	sopka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Touto	tento	k3xDgFnSc7
erupcí	erupce	k1gFnSc7
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
vzniku	vznik	k1gInSc3
nového	nový	k2eAgInSc2d1
sopečného	sopečný	k2eAgInSc2d1
vrcholu	vrchol	k1gInSc2
pojmenovaného	pojmenovaný	k2eAgMnSc2d1
Jihovýchodní	jihovýchodní	k2eAgInSc4d1
kráter	kráter	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
březnu	březen	k1gInSc6
1981	#num#	k4
město	město	k1gNnSc4
Randazzo	Randazza	k1gFnSc5
na	na	k7c6
severozápadním	severozápadní	k2eAgInSc6d1
svahu	svah	k1gInSc6
jen	jen	k9
těsně	těsně	k6eAd1
uniklo	uniknout	k5eAaPmAgNnS
zničení	zničení	k1gNnSc3
atypicky	atypicky	k6eAd1
rychlými	rychlý	k2eAgInPc7d1
lávovými	lávový	k2eAgInPc7d1
proudy	proud	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
se	se	k3xPyFc4
kolem	kolem	k7c2
města	město	k1gNnSc2
prohnaly	prohnat	k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byly	být	k5eAaImAgInP
velice	velice	k6eAd1
podobné	podobný	k2eAgInPc1d1
proudu	proud	k1gInSc2
z	z	k7c2
roku	rok	k1gInSc2
1928	#num#	k4
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
zničil	zničit	k5eAaPmAgInS
obec	obec	k1gFnSc4
Mascali	Mascali	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
roky	rok	k1gInPc4
1992	#num#	k4
až	až	k8xS
1993	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
ohrožení	ohrožení	k1gNnSc3
města	město	k1gNnSc2
Zafferana	Zafferan	k1gMnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
vlivem	vliv	k1gInSc7
značného	značný	k2eAgNnSc2d1
úsilí	úsilí	k1gNnSc2
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
město	město	k1gNnSc1
zachránit	zachránit	k5eAaPmF
jen	jen	k9
se	s	k7c7
ztrátou	ztráta	k1gFnSc7
jedné	jeden	k4xCgFnSc2
budovy	budova	k1gFnSc2
a	a	k8xC
několika	několik	k4yIc2
stovek	stovka	k1gFnPc2
metrů	metr	k1gInPc2
městské	městský	k2eAgFnSc2d1
zeleně	zeleň	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
záchraně	záchrana	k1gFnSc3
přispělo	přispět	k5eAaPmAgNnS
vybudování	vybudování	k1gNnSc1
kamenných	kamenný	k2eAgInPc2d1
valů	val	k1gInPc2
do	do	k7c2
cesty	cesta	k1gFnSc2
postupujícího	postupující	k2eAgInSc2d1
proudu	proud	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpokládalo	předpokládat	k5eAaImAgNnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
valy	val	k1gInPc4
vytvoří	vytvořit	k5eAaPmIp3nS
lávě	láva	k1gFnSc3
hráz	hráz	k1gFnSc4
a	a	k8xC
že	že	k8xS
přísun	přísun	k1gInSc4
nového	nový	k2eAgInSc2d1
materiálu	materiál	k1gInSc2
ustane	ustat	k5eAaPmIp3nS
dříve	dříve	k6eAd2
<g/>
,	,	kIx,
než	než	k8xS
za	za	k7c7
nimi	on	k3xPp3gInPc7
vznikne	vzniknout	k5eAaPmIp3nS
lávové	lávový	k2eAgNnSc1d1
jezero	jezero	k1gNnSc1
natolik	natolik	k6eAd1
silné	silný	k2eAgNnSc1d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
valy	val	k1gInPc1
silou	síla	k1gFnSc7
prorazilo	prorazit	k5eAaPmAgNnS
a	a	k8xC
láva	láva	k1gFnSc1
by	by	kYmCp3nS
mohla	moct	k5eAaImAgFnS
pokračovat	pokračovat	k5eAaImF
přímo	přímo	k6eAd1
na	na	k7c4
město	město	k1gNnSc4
Zafferana	Zafferan	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
erupce	erupce	k1gFnSc1
pokračovala	pokračovat	k5eAaImAgFnS
a	a	k8xC
hráze	hráze	k1gFnSc1
láva	láva	k1gFnSc1
časem	časem	k6eAd1
překonala	překonat	k5eAaPmAgFnS
a	a	k8xC
v	v	k7c6
tečení	tečení	k1gNnSc6
směrem	směr	k1gInSc7
k	k	k7c3
městu	město	k1gNnSc3
pokračovala	pokračovat	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
bylo	být	k5eAaImAgNnS
rozhodnuto	rozhodnout	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
blízko	blízko	k7c2
místa	místo	k1gNnSc2
erupce	erupce	k1gFnSc2
budou	být	k5eAaImBp3nP
použity	použit	k2eAgFnPc1d1
výbušniny	výbušnina	k1gFnPc1
k	k	k7c3
přerušení	přerušení	k1gNnSc3
lávových	lávový	k2eAgInPc2d1
kanálů	kanál	k1gInPc2
a	a	k8xC
tunelů	tunel	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yQgInPc1,k3yRgInPc1
velice	velice	k6eAd1
efektivně	efektivně	k6eAd1
transportovaly	transportovat	k5eAaBmAgFnP
lávu	láva	k1gFnSc4
na	na	k7c4
vzdálenost	vzdálenost	k1gFnSc4
7	#num#	k4
kilometrů	kilometr	k1gInPc2
<g/>
,	,	kIx,
aniž	aniž	k8xS,k8xC
by	by	kYmCp3nS
se	se	k3xPyFc4
láva	láva	k1gFnSc1
výrazně	výrazně	k6eAd1
chladila	chladit	k5eAaImAgFnS
a	a	k8xC
zpomalovala	zpomalovat	k5eAaImAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hlavní	hlavní	k2eAgFnSc1d1
exploze	exploze	k1gFnSc1
se	se	k3xPyFc4
odehrála	odehrát	k5eAaPmAgFnS
23	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
1992	#num#	k4
a	a	k8xC
vedla	vést	k5eAaImAgFnS
ke	k	k7c3
zničení	zničení	k1gNnSc3
lávového	lávový	k2eAgInSc2d1
tunelu	tunel	k1gInSc2
<g/>
,	,	kIx,
načež	načež	k6eAd1
láva	láva	k1gFnSc1
začala	začít	k5eAaPmAgFnS
téct	téct	k5eAaImF
novým	nový	k2eAgInSc7d1
uměle	uměle	k6eAd1
připraveným	připravený	k2eAgNnSc7d1
korytem	koryto	k1gNnSc7
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yQgNnSc1,k3yIgNnSc1
jí	on	k3xPp3gFnSc2
odvedlo	odvést	k5eAaPmAgNnS
daleko	daleko	k6eAd1
od	od	k7c2
Zafferana	Zafferan	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předpokládalo	předpokládat	k5eAaImAgNnS
se	se	k3xPyFc4
<g/>
,	,	kIx,
že	že	k8xS
vytvoření	vytvoření	k1gNnSc1
nového	nový	k2eAgNnSc2d1
koryta	koryto	k1gNnSc2
směrem	směr	k1gInSc7
k	k	k7c3
městu	město	k1gNnSc3
by	by	kYmCp3nS
lávě	láva	k1gFnSc3
trvalo	trvat	k5eAaImAgNnS
měsíce	měsíc	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krátce	krátce	k6eAd1
po	po	k7c6
explozi	exploze	k1gFnSc6
klesla	klesnout	k5eAaPmAgFnS
rychlost	rychlost	k1gFnSc1
lávové	lávový	k2eAgFnSc2d1
erupce	erupce	k1gFnSc2
a	a	k8xC
během	během	k7c2
jejího	její	k3xOp3gNnSc2
doznívání	doznívání	k1gNnSc2
až	až	k9
do	do	k7c2
30	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
1993	#num#	k4
se	se	k3xPyFc4
již	již	k6eAd1
nikdy	nikdy	k6eAd1
nepřiblížila	přiblížit	k5eNaPmAgFnS
do	do	k7c2
blízkosti	blízkost	k1gFnSc2
města	město	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Erupce	erupce	k1gFnPc1
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
při	při	k7c6
menším	malý	k2eAgNnSc6d2
přiblížení	přiblížení	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dobře	dobře	k6eAd1
viditelný	viditelný	k2eAgInSc1d1
je	být	k5eAaImIp3nS
pyroklastický	pyroklastický	k2eAgInSc1d1
mrak	mrak	k1gInSc1
unikajících	unikající	k2eAgInPc2d1
sopečných	sopečný	k2eAgInPc2d1
plynů	plyn	k1gInPc2
a	a	k8xC
drobných	drobný	k2eAgFnPc2d1
pevných	pevný	k2eAgFnPc2d1
částic	částice	k1gFnPc2
v	v	k7c6
atmosféře	atmosféra	k1gFnSc6
</s>
<s>
Mezi	mezi	k7c7
lety	let	k1gInPc7
1995	#num#	k4
až	až	k9
2001	#num#	k4
nastalo	nastat	k5eAaPmAgNnS
období	období	k1gNnSc1
netradičně	tradičně	k6eNd1
vysoké	vysoký	k2eAgFnSc2d1
aktivity	aktivita	k1gFnSc2
čtyř	čtyři	k4xCgInPc2
kráterů	kráter	k1gInPc2
na	na	k7c6
vrcholu	vrchol	k1gInSc6
Etny	Etna	k1gFnSc2
<g/>
,	,	kIx,
ale	ale	k8xC
až	až	k9
během	během	k7c2
června	červen	k1gInSc2
až	až	k8xS
srpna	srpen	k1gInSc2
2001	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
prvnímu	první	k4xOgInSc3
lávovému	lávový	k2eAgInSc3d1
výlevu	výlev	k1gInSc3
na	na	k7c6
svazích	svah	k1gInPc6
Etny	Etna	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Tato	tento	k3xDgFnSc1
erupce	erupce	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
byla	být	k5eAaImAgFnS
živena	živit	k5eAaImNgFnS
vystupujícím	vystupující	k2eAgNnSc7d1
magmatem	magma	k1gNnSc7
ze	z	k7c2
sedmi	sedm	k4xCc2
prasklin	prasklina	k1gFnPc2
převážně	převážně	k6eAd1
situovaných	situovaný	k2eAgFnPc2d1
na	na	k7c6
jižním	jižní	k2eAgInSc6d1
svahu	svah	k1gInSc6
<g/>
,	,	kIx,
byla	být	k5eAaImAgFnS
silně	silně	k6eAd1
sledována	sledovat	k5eAaImNgFnS
masmédii	masmédium	k1gNnPc7
<g/>
,	,	kIx,
jelikož	jelikož	k8xS
se	se	k3xPyFc4
odehrála	odehrát	k5eAaPmAgFnS
v	v	k7c6
období	období	k1gNnSc6
vrcholící	vrcholící	k2eAgFnSc2d1
turistické	turistický	k2eAgFnSc2d1
sezóny	sezóna	k1gFnSc2
a	a	k8xC
současně	současně	k6eAd1
se	se	k3xPyFc4
v	v	k7c6
Itálii	Itálie	k1gFnSc6
nacházelo	nacházet	k5eAaImAgNnS
velké	velký	k2eAgNnSc1d1
množství	množství	k1gNnSc1
reportérů	reportér	k1gMnPc2
a	a	k8xC
televizních	televizní	k2eAgInPc2d1
štábů	štáb	k1gInPc2
kvůli	kvůli	k7c3
zasedání	zasedání	k1gNnSc3
G8	G8	k1gFnSc2
v	v	k7c6
Janově	Janov	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Erupce	erupce	k1gFnSc1
probíhala	probíhat	k5eAaImAgFnS
taktéž	taktéž	k?
nedaleko	nedaleko	k7c2
jedné	jeden	k4xCgFnSc2
ze	z	k7c2
snadno	snadno	k6eAd1
dosažitelné	dosažitelný	k2eAgFnSc2d1
turistické	turistický	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
<g/>
,	,	kIx,
takže	takže	k8xS
mohla	moct	k5eAaImAgFnS
být	být	k5eAaImF
hojně	hojně	k6eAd1
pozorována	pozorovat	k5eAaImNgFnS
turisty	turist	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Erupce	erupce	k1gFnSc1
měla	mít	k5eAaImAgFnS
na	na	k7c6
svědomí	svědomí	k1gNnSc6
poničení	poničení	k1gNnSc2
horní	horní	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
lanovky	lanovka	k1gFnSc2
poblíž	poblíž	k6eAd1
Etna	Etna	k1gFnSc1
Sud	suda	k1gFnPc2
a	a	k8xC
dalšího	další	k2eAgNnSc2d1
turistického	turistický	k2eAgNnSc2d1
zázemí	zázemí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mezi	mezi	k7c4
roky	rok	k1gInPc4
2002	#num#	k4
a	a	k8xC
2003	#num#	k4
se	se	k3xPyFc4
odehrála	odehrát	k5eAaPmAgFnS
další	další	k2eAgFnSc1d1
silná	silný	k2eAgFnSc1d1
erupce	erupce	k1gFnSc1
<g/>
,	,	kIx,
jedna	jeden	k4xCgFnSc1
z	z	k7c2
nejsilnějších	silný	k2eAgFnPc2d3
za	za	k7c4
posledních	poslední	k2eAgNnPc2d1
150	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
vyvrhla	vyvrhnout	k5eAaPmAgFnS
do	do	k7c2
atmosféry	atmosféra	k1gFnSc2
obrovské	obrovský	k2eAgNnSc4d1
mračno	mračno	k1gNnSc4
materiálu	materiál	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yQgNnSc1,k3yRgNnSc1
bylo	být	k5eAaImAgNnS
lehce	lehko	k6eAd1
viditelné	viditelný	k2eAgNnSc1d1
z	z	k7c2
vesmíru	vesmír	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvodem	důvod	k1gInSc7
vzniku	vznik	k1gInSc2
takto	takto	k6eAd1
výrazného	výrazný	k2eAgNnSc2d1
mračna	mračno	k1gNnSc2
byl	být	k5eAaImAgInS
kontakt	kontakt	k1gInSc4
vystupujícího	vystupující	k2eAgNnSc2d1
magmatu	magma	k1gNnSc2
s	s	k7c7
podzemní	podzemní	k2eAgFnSc7d1
vodou	voda	k1gFnSc7
<g/>
,	,	kIx,
nárůstu	nárůst	k1gInSc3
tlaku	tlak	k1gInSc2
a	a	k8xC
explozi	exploze	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Část	část	k1gFnSc1
materiálu	materiál	k1gInSc2
z	z	k7c2
tohoto	tento	k3xDgNnSc2
mračna	mračno	k1gNnSc2
spadla	spadnout	k5eAaPmAgFnS
na	na	k7c4
území	území	k1gNnSc4
Libye	Libye	k1gFnSc2
a	a	k8xC
řeckého	řecký	k2eAgInSc2d1
ostrova	ostrov	k1gInSc2
Kefalonia	Kefalonium	k1gNnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
překonalo	překonat	k5eAaPmAgNnS
600	#num#	k4
km	km	kA
jižním	jižní	k2eAgInSc7d1
směrem	směr	k1gInSc7
přes	přes	k7c4
Středomoří	středomoří	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seismická	seismický	k2eAgFnSc1d1
aktivita	aktivita	k1gFnSc1
během	během	k7c2
erupce	erupce	k1gFnSc2
otevřela	otevřít	k5eAaPmAgFnS
mezi	mezi	k7c4
26	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
27	#num#	k4
<g/>
.	.	kIx.
říjnem	říjen	k1gInSc7
4	#num#	k4
kilometry	kilometr	k1gInPc4
dlouhou	dlouhý	k2eAgFnSc4d1
trhlinu	trhlina	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
projevovala	projevovat	k5eAaImAgFnS
havajským	havajský	k2eAgInSc7d1
typem	typ	k1gInSc7
erupcí	erupce	k1gFnPc2
i	i	k8xC
strombolským	strombolský	k2eAgNnSc7d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Otřesy	otřes	k1gInPc1
současně	současně	k6eAd1
způsobily	způsobit	k5eAaPmAgInP
sesuv	sesuv	k1gInSc4
východního	východní	k2eAgInSc2d1
svahu	svah	k1gInSc2
o	o	k7c4
dva	dva	k4xCgInPc4
metry	metr	k1gInPc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
způsobilo	způsobit	k5eAaPmAgNnS
množství	množství	k1gNnSc3
poškození	poškození	k1gNnSc2
budov	budova	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Erupce	erupce	k1gFnSc1
současně	současně	k6eAd1
zcela	zcela	k6eAd1
zničila	zničit	k5eAaPmAgFnS
turistickou	turistický	k2eAgFnSc4d1
stanici	stanice	k1gFnSc4
Piano	piano	k1gNnSc1
Provenzana	Provenzan	k1gMnSc2
na	na	k7c6
severovýchodním	severovýchodní	k2eAgInSc6d1
svahu	svah	k1gInSc6
a	a	k8xC
část	část	k1gFnSc1
stanice	stanice	k1gFnSc2
„	„	k?
<g/>
Etna	Etna	k1gFnSc1
Sud	suda	k1gFnPc2
<g/>
“	“	k?
okolo	okolo	k7c2
Rifugio	Rifugio	k1gMnSc1
Sapienza	Sapienz	k1gMnSc2
na	na	k7c6
jižním	jižní	k2eAgInSc6d1
svahu	svah	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Část	část	k1gFnSc1
erupce	erupce	k1gFnSc1
byla	být	k5eAaImAgFnS
nafilmována	nafilmovat	k5eAaPmNgFnS
společností	společnost	k1gFnSc7
Lucasfilm	Lucasfilma	k1gFnPc2
a	a	k8xC
následně	následně	k6eAd1
použita	použít	k5eAaPmNgFnS
v	v	k7c6
roce	rok	k1gInSc6
2005	#num#	k4
ve	v	k7c6
filmu	film	k1gInSc6
Star	Star	kA
Wars	Warsa	k1gFnPc2
<g/>
:	:	kIx,
Epizoda	epizoda	k1gFnSc1
III	III	kA
–	–	k?
Pomsta	pomsta	k1gFnSc1
Sithů	Sith	k1gInPc2
jako	jako	k8xS,k8xC
krajina	krajina	k1gFnSc1
planety	planeta	k1gFnSc2
Mustafar	Mustafara	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
Rifugio	Rifugio	k6eAd1
Sapienza	Sapienza	k1gFnSc1
je	být	k5eAaImIp3nS
poblíž	poblíž	k7c2
stanice	stanice	k1gFnSc2
lanovky	lanovka	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
byla	být	k5eAaImAgFnS
zničena	zničit	k5eAaPmNgFnS
předchozí	předchozí	k2eAgFnSc7d1
erupcí	erupce	k1gFnSc7
v	v	k7c6
roce	rok	k1gInSc6
1983	#num#	k4
a	a	k8xC
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
následně	následně	k6eAd1
znovuvybudována	znovuvybudovat	k5eAaPmNgFnS
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
září	září	k1gNnSc6
2004	#num#	k4
a	a	k8xC
v	v	k7c6
březnu	březen	k1gInSc6
2005	#num#	k4
se	se	k3xPyFc4
Etna	Etna	k1gFnSc1
částečně	částečně	k6eAd1
uklidnila	uklidnit	k5eAaPmAgFnS
a	a	k8xC
probíhaly	probíhat	k5eAaImAgInP
jen	jen	k9
pozvolné	pozvolný	k2eAgInPc1d1
lávové	lávový	k2eAgInPc1d1
výlevy	výlev	k1gInPc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
jihovýchodního	jihovýchodní	k2eAgInSc2d1
svahu	svah	k1gInSc2
ve	v	k7c6
vyšších	vysoký	k2eAgFnPc6d2
polohách	poloha	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Silnější	silný	k2eAgFnSc2d2
erupce	erupce	k1gFnSc2
se	se	k3xPyFc4
opět	opět	k6eAd1
projevily	projevit	k5eAaPmAgFnP
až	až	k9
mezi	mezi	k7c7
červencem	červenec	k1gInSc7
až	až	k9
prosincem	prosinec	k1gInSc7
v	v	k7c6
roce	rok	k1gInSc6
2006	#num#	k4
v	v	k7c6
oblasti	oblast	k1gFnSc6
Jihovýchodního	jihovýchodní	k2eAgInSc2d1
kráteru	kráter	k1gInSc2
<g/>
,	,	kIx,
načež	načež	k6eAd1
následovaly	následovat	k5eAaImAgFnP
čtyři	čtyři	k4xCgFnPc1
epizody	epizoda	k1gFnPc1
výtrysku	výtrysk	k1gInSc2
magmatu	magma	k1gNnSc2
pod	pod	k7c7
tlakem	tlak	k1gInSc7
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
vznikly	vzniknout	k5eAaPmAgFnP
lávové	lávový	k2eAgFnPc1d1
fontány	fontána	k1gFnPc1
v	v	k7c6
téže	týž	k3xTgFnSc6,k3xDgFnSc6
oblasti	oblast	k1gFnSc6
Jihozápadního	jihozápadní	k2eAgInSc2d1
kráteru	kráter	k1gInSc2
ve	v	k7c6
dnech	den	k1gInPc6
29	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
<g/>
,	,	kIx,
11	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
<g/>
,	,	kIx,
29	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
a	a	k8xC
7	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
polovině	polovina	k1gFnSc6
srpna	srpen	k1gInSc2
2007	#num#	k4
se	se	k3xPyFc4
částečně	částečně	k6eAd1
změnil	změnit	k5eAaPmAgInS
chemismus	chemismus	k1gInSc1
magmatu	magma	k1gNnSc2
a	a	k8xC
tím	ten	k3xDgNnSc7
i	i	k9
druh	druh	k1gInSc1
erupce	erupce	k1gFnSc2
<g/>
,	,	kIx,
Etna	Etna	k1gFnSc1
se	se	k3xPyFc4
začala	začít	k5eAaPmAgFnS
projevovat	projevovat	k5eAaImF
produkcí	produkce	k1gFnSc7
popela	popel	k1gInSc2
a	a	k8xC
strombolským	strombolský	k2eAgInSc7d1
typem	typ	k1gInSc7
erupcí	erupce	k1gFnPc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
východně	východně	k6eAd1
od	od	k7c2
Jihovýchodního	jihovýchodní	k2eAgInSc2d1
kráteru	kráter	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Dům	dům	k1gInSc1
zničený	zničený	k2eAgInSc1d1
lávovým	lávový	k2eAgInSc7d1
proudem	proud	k1gInSc7
na	na	k7c6
svahu	svah	k1gInSc6
Etny	Etna	k1gFnSc2
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2007	#num#	k4
Etna	Etna	k1gFnSc1
hlasitě	hlasitě	k6eAd1
explodovala	explodovat	k5eAaBmAgFnS
okolo	okolo	k7c2
20	#num#	k4
<g/>
:	:	kIx,
<g/>
0	#num#	k4
<g/>
0	#num#	k4
místního	místní	k2eAgInSc2d1
času	čas	k1gInSc2
za	za	k7c4
vyhození	vyhození	k1gNnSc4
lávy	láva	k1gFnSc2
přibližně	přibližně	k6eAd1
do	do	k7c2
výšky	výška	k1gFnSc2
400	#num#	k4
metrů	metr	k1gInPc2
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
se	se	k3xPyFc4
dostalo	dostat	k5eAaPmAgNnS
do	do	k7c2
atmosféry	atmosféra	k1gFnSc2
opět	opět	k6eAd1
značné	značný	k2eAgNnSc1d1
množství	množství	k1gNnSc1
materiálu	materiál	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
následně	následně	k6eAd1
transportován	transportovat	k5eAaBmNgInS
nad	nad	k7c4
okolní	okolní	k2eAgFnPc4d1
vesnice	vesnice	k1gFnPc4
a	a	k8xC
města	město	k1gNnPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
spadl	spadnout	k5eAaPmAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Erupce	erupce	k1gFnSc1
se	se	k3xPyFc4
opět	opět	k6eAd1
odehrála	odehrát	k5eAaPmAgFnS
v	v	k7c6
oblasti	oblast	k1gFnSc6
Jihovýchodního	jihovýchodní	k2eAgInSc2d1
kráteru	kráter	k1gInSc2
a	a	k8xC
byla	být	k5eAaImAgFnS
vidět	vidět	k5eAaImF
z	z	k7c2
velké	velký	k2eAgFnSc2d1
části	část	k1gFnSc2
Sicílie	Sicílie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Příštího	příští	k2eAgNnSc2d1
rána	ráno	k1gNnSc2
mezi	mezi	k7c7
5	#num#	k4
až	až	k8xS
7	#num#	k4
hodinou	hodina	k1gFnSc7
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
postupnému	postupný	k2eAgNnSc3d1
uklidnění	uklidnění	k1gNnSc3
erupce	erupce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvůli	kvůli	k7c3
erupci	erupce	k1gFnSc3
ale	ale	k8xC
muselo	muset	k5eAaImAgNnS
být	být	k5eAaImF
z	z	k7c2
bezpečnostních	bezpečnostní	k2eAgInPc2d1
důvodu	důvod	k1gInSc2
v	v	k7c6
průběhu	průběh	k1gInSc6
noci	noc	k1gFnSc2
uzavřeno	uzavřít	k5eAaPmNgNnS
letiště	letiště	k1gNnSc1
Catania-Fontanarossa	Catania-Fontanaross	k1gMnSc2
pro	pro	k7c4
veškeré	veškerý	k3xTgInPc4
lety	let	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podobná	podobný	k2eAgFnSc1d1
událost	událost	k1gFnSc1
se	se	k3xPyFc4
odehrála	odehrát	k5eAaPmAgFnS
později	pozdě	k6eAd2
v	v	k7c6
noci	noc	k1gFnSc6
ze	z	k7c2
23	#num#	k4
<g/>
.	.	kIx.
na	na	k7c4
24	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2007	#num#	k4
<g/>
,	,	kIx,
trvala	trvat	k5eAaImAgFnS
6	#num#	k4
hodin	hodina	k1gFnPc2
a	a	k8xC
způsobila	způsobit	k5eAaPmAgFnS
spad	spad	k1gInSc4
popela	popel	k1gInSc2
a	a	k8xC
lapill	lapilla	k1gFnPc2
na	na	k7c6
severní	severní	k2eAgFnSc6d1
straně	strana	k1gFnSc6
sopky	sopka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdrojem	zdroj	k1gInSc7
erupce	erupce	k1gFnSc2
byl	být	k5eAaImAgInS
podobně	podobně	k6eAd1
jako	jako	k9
v	v	k7c6
předchozích	předchozí	k2eAgInPc6d1
případech	případ	k1gInPc6
Jihovýchodní	jihovýchodní	k2eAgInSc4d1
kráter	kráter	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následujících	následující	k2eAgInPc2d1
několik	několik	k4yIc4
měsíců	měsíc	k1gInPc2
probíhala	probíhat	k5eAaImAgFnS
jen	jen	k9
minimální	minimální	k2eAgFnSc1d1
eruptivní	eruptivní	k2eAgFnSc1d1
i	i	k8xC
seismická	seismický	k2eAgFnSc1d1
aktivita	aktivita	k1gFnSc1
v	v	k7c6
oblasti	oblast	k1gFnSc6
Jihovýchodního	jihovýchodní	k2eAgInSc2d1
kráteru	kráter	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc1d1
silná	silný	k2eAgFnSc1d1
erupce	erupce	k1gFnSc1
proběhla	proběhnout	k5eAaPmAgFnS
pozdě	pozdě	k6eAd1
odpoledne	odpoledne	k1gNnSc4
10	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2008	#num#	k4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
ale	ale	k9
vlivem	vlivem	k7c2
špatného	špatný	k2eAgNnSc2d1
počasí	počasí	k1gNnSc2
nebyla	být	k5eNaImAgFnS
z	z	k7c2
větší	veliký	k2eAgFnSc2d2
části	část	k1gFnSc2
pozorovatelná	pozorovatelný	k2eAgFnSc1d1
v	v	k7c6
oblasti	oblast	k1gFnSc6
kráteru	kráter	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
erupci	erupce	k1gFnSc6
se	se	k3xPyFc4
do	do	k7c2
níže	nízce	k6eAd2
položené	položený	k2eAgFnSc2d1
Valle	Valle	k1gFnSc2
del	del	k?
Bove	Bov	k1gFnSc2
oblasti	oblast	k1gFnSc2
dostalo	dostat	k5eAaPmAgNnS
několik	několik	k4yIc1
lávových	lávový	k2eAgInPc2d1
proudů	proud	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Erupce	erupce	k1gFnSc1
trvala	trvat	k5eAaImAgFnS
přibližně	přibližně	k6eAd1
4	#num#	k4
hodiny	hodina	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
13	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2008	#num#	k4
ráno	ráno	k1gNnSc4
začala	začít	k5eAaPmAgFnS
nová	nový	k2eAgFnSc1d1
sopečná	sopečný	k2eAgFnSc1d1
erupce	erupce	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
byla	být	k5eAaImAgFnS
doprovázená	doprovázený	k2eAgFnSc1d1
rojem	roj	k1gInSc7
200	#num#	k4
menších	malý	k2eAgNnPc6d2
zemětřeseních	zemětřesení	k1gNnPc6
a	a	k8xC
deformací	deformace	k1gFnPc2
povrchu	povrch	k1gInSc2
v	v	k7c6
oblasti	oblast	k1gFnSc6
vrcholku	vrcholek	k1gInSc2
sopky	sopka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odpoledne	odpoledne	k6eAd1
téhož	týž	k3xTgInSc2
dne	den	k1gInSc2
se	se	k3xPyFc4
otevřely	otevřít	k5eAaPmAgFnP
dvě	dva	k4xCgFnPc1
nové	nový	k2eAgFnPc1d1
praskliny	prasklina	k1gFnPc1
v	v	k7c6
nadmořské	nadmořský	k2eAgFnSc6d1
výšce	výška	k1gFnSc6
2650	#num#	k4
respektive	respektive	k9
3050	#num#	k4
metrů	metr	k1gInPc2
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
s	s	k7c7
množstvím	množství	k1gNnSc7
depresí	deprese	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
začaly	začít	k5eAaPmAgFnP
projevovat	projevovat	k5eAaImF
strombolským	strombolský	k2eAgInSc7d1
typem	typ	k1gInSc7
erupcí	erupce	k1gFnPc2
a	a	k8xC
produkcí	produkce	k1gFnSc7
lávových	lávový	k2eAgInPc2d1
proudů	proud	k1gInPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
začaly	začít	k5eAaPmAgInP
klesat	klesat	k5eAaImF
do	do	k7c2
Valle	Valle	k1gFnSc2
del	del	k?
Bove	Bov	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
následujících	následující	k2eAgFnPc2d1
24	#num#	k4
hodin	hodina	k1gFnPc2
urazil	urazit	k5eAaPmAgInS
lávový	lávový	k2eAgInSc1d1
proud	proud	k1gInSc1
přibližně	přibližně	k6eAd1
6	#num#	k4
km	km	kA
východním	východní	k2eAgInSc7d1
směrem	směr	k1gInSc7
<g/>
,	,	kIx,
kde	kde	k6eAd1
náhle	náhle	k6eAd1
zpomalila	zpomalit	k5eAaPmAgFnS
až	až	k6eAd1
se	se	k3xPyFc4
zastavila	zastavit	k5eAaPmAgFnS
ve	v	k7c6
vzdálenosti	vzdálenost	k1gFnSc6
3	#num#	k4
km	km	kA
od	od	k7c2
nejbližší	blízký	k2eAgFnSc2d3
vesnice	vesnice	k1gFnSc2
Milo	milo	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
16	#num#	k4
<g/>
.	.	kIx.
až	až	k9
18	#num#	k4
<g/>
.	.	kIx.
květnem	květen	k1gInSc7
narostla	narůst	k5eAaPmAgFnS
produkce	produkce	k1gFnSc1
popela	popel	k1gInSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
vedlo	vést	k5eAaImAgNnS
ke	k	k7c3
vzniku	vznik	k1gInSc3
malého	malý	k2eAgNnSc2d1
mračna	mračno	k1gNnSc2
nad	nad	k7c7
sopkou	sopka	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ke	k	k7c3
konci	konec	k1gInSc3
května	květen	k1gInSc2
a	a	k8xC
v	v	k7c6
prvním	první	k4xOgInSc6
týdnu	týden	k1gInSc6
června	červen	k1gInSc2
pokračovala	pokračovat	k5eAaImAgFnS
aktivita	aktivita	k1gFnSc1
na	na	k7c6
nízké	nízký	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
láva	láva	k1gFnSc1
vytékala	vytékat	k5eAaImAgFnS
do	do	k7c2
vzdálenosti	vzdálenost	k1gFnSc2
jen	jen	k9
několika	několik	k4yIc2
set	sto	k4xCgNnPc2
metrů	metr	k1gMnPc2
od	od	k7c2
místa	místo	k1gNnSc2
úniku	únik	k1gInSc2
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
náhlému	náhlý	k2eAgNnSc3d1
zvýšení	zvýšení	k1gNnSc3
aktivity	aktivita	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Během	během	k7c2
následujícího	následující	k2eAgInSc2d1
týdne	týden	k1gInSc2
byla	být	k5eAaImAgFnS
tekoucí	tekoucí	k2eAgFnSc1d1
láva	láva	k1gFnSc1
již	již	k9
5	#num#	k4
kilometrů	kilometr	k1gInPc2
od	od	k7c2
svého	svůj	k3xOyFgInSc2
původního	původní	k2eAgInSc2d1
zdroje	zdroj	k1gInSc2
a	a	k8xC
po	po	k7c4
celý	celý	k2eAgInSc4d1
červen	červen	k1gInSc4
a	a	k8xC
červenec	červenec	k1gInSc4
pokračovaly	pokračovat	k5eAaImAgFnP
pozvolné	pozvolný	k2eAgFnPc1d1
erupce	erupce	k1gFnPc1
strombolského	strombolský	k2eAgInSc2d1
typu	typ	k1gInSc2
ze	z	k7c2
dvou	dva	k4xCgFnPc2
prasklin	prasklina	k1gFnPc2
ve	v	k7c6
výšce	výška	k1gFnSc6
2800	#num#	k4
m	m	kA
nad	nad	k7c7
mořem	moře	k1gNnSc7
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
vedlo	vést	k5eAaImAgNnS
ke	k	k7c3
vzniku	vznik	k1gInSc3
4	#num#	k4
km	km	kA
dlouhému	dlouhý	k2eAgInSc3d1
proudu	proud	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Aktivita	aktivita	k1gFnSc1
v	v	k7c6
polovině	polovina	k1gFnSc6
července	červenec	k1gInSc2
se	se	k3xPyFc4
projevovala	projevovat	k5eAaImAgFnS
hlasitými	hlasitý	k2eAgFnPc7d1
detonacemi	detonace	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
byly	být	k5eAaImAgFnP
zaznamenány	zaznamenat	k5eAaPmNgFnP
v	v	k7c6
několika	několik	k4yIc2
okolních	okolní	k2eAgNnPc6d1
městech	město	k1gNnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konci	konec	k1gInSc6
července	červenec	k1gInSc2
tyto	tento	k3xDgFnPc1
detonace	detonace	k1gFnPc1
ustaly	ustat	k5eAaPmAgFnP
<g/>
,	,	kIx,
ale	ale	k8xC
výlevy	výlev	k1gInPc1
lávy	láva	k1gFnSc2
stále	stále	k6eAd1
pokračovaly	pokračovat	k5eAaImAgFnP
pozvolnou	pozvolný	k2eAgFnSc7d1
rychlostí	rychlost	k1gFnSc7
tvořící	tvořící	k2eAgInPc1d1
jen	jen	k6eAd1
krátké	krátký	k2eAgInPc1d1
lávové	lávový	k2eAgInPc1d1
proudy	proud	k1gInPc1
do	do	k7c2
maximální	maximální	k2eAgFnSc2d1
délky	délka	k1gFnSc2
1	#num#	k4
km	km	kA
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
13	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2008	#num#	k4
<g/>
,	,	kIx,
šest	šest	k4xCc4
měsíců	měsíc	k1gInPc2
po	po	k7c6
utlumení	utlumení	k1gNnSc6
aktivity	aktivita	k1gFnSc2
<g/>
,	,	kIx,
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
opětovným	opětovný	k2eAgFnPc3d1
erupcím	erupce	k1gFnPc3
na	na	k7c6
svazích	svah	k1gInPc6
Etny	Etna	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
se	se	k3xPyFc4
projevovaly	projevovat	k5eAaImAgFnP
nízkou	nízký	k2eAgFnSc7d1
rychlostí	rychlost	k1gFnSc7
a	a	k8xC
stabilitou	stabilita	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Staly	stát	k5eAaPmAgInP
se	se	k3xPyFc4
nejdelší	dlouhý	k2eAgMnSc1d3
ze	z	k7c2
čtyř	čtyři	k4xCgFnPc2
erupcí	erupce	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
zatím	zatím	k6eAd1
ve	v	k7c6
třetím	třetí	k4xOgInSc6
tisíciletí	tisíciletí	k1gNnSc6
proběhly	proběhnout	k5eAaPmAgFnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předchozí	předchozí	k2eAgFnSc1d1
erupce	erupce	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
2001	#num#	k4
<g/>
,	,	kIx,
2002	#num#	k4
<g/>
–	–	k?
<g/>
2003	#num#	k4
a	a	k8xC
2004	#num#	k4
<g/>
–	–	k?
<g/>
2005	#num#	k4
trvaly	trvat	k5eAaImAgInP
pouze	pouze	k6eAd1
3	#num#	k4
týdny	týden	k1gInPc4
<g/>
,	,	kIx,
3	#num#	k4
měsíce	měsíc	k1gInPc1
<g/>
,	,	kIx,
respektive	respektive	k9
6	#num#	k4
měsíců	měsíc	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
7	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2009	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
erupci	erupce	k1gFnSc3
strombolského	strombolský	k2eAgInSc2d1
typu	typ	k1gInSc2
na	na	k7c6
východním	východní	k2eAgInSc6d1
svahu	svah	k1gInSc6
Jihozápadního	jihozápadní	k2eAgInSc2d1
kráteru	kráter	k1gInSc2
<g/>
,	,	kIx,
o	o	k7c4
den	den	k1gInSc4
později	pozdě	k6eAd2
se	se	k3xPyFc4
v	v	k7c6
hloubce	hloubka	k1gFnSc6
10	#num#	k4
km	km	kA
odehrálo	odehrát	k5eAaPmAgNnS
zemětřesení	zemětřesení	k1gNnSc1
o	o	k7c6
síle	síla	k1gFnSc6
4,4	4,4	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Neobvyklé	obvyklý	k2eNgInPc1d1
úkazy	úkaz	k1gInPc1
</s>
<s>
V	v	k7c6
70	#num#	k4
<g/>
.	.	kIx.
letech	léto	k1gNnPc6
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
bylo	být	k5eAaImAgNnS
údajně	údajně	k6eAd1
v	v	k7c6
okolí	okolí	k1gNnSc6
Etny	Etna	k1gFnSc2
pozorováno	pozorovat	k5eAaImNgNnS
několik	několik	k4yIc1
kouřových	kouřový	k2eAgInPc2d1
kroužků	kroužek	k1gInPc2
<g/>
,	,	kIx,
jež	jenž	k3xRgInPc1
jsou	být	k5eAaImIp3nP
u	u	k7c2
sopek	sopka	k1gFnPc2
velice	velice	k6eAd1
řídké	řídký	k2eAgFnPc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
roce	rok	k1gInSc6
2000	#num#	k4
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
opětovnému	opětovný	k2eAgNnSc3d1
pozorování	pozorování	k1gNnSc3
tohoto	tento	k3xDgInSc2
atypického	atypický	k2eAgInSc2d1
útvaru	útvar	k1gInSc2
v	v	k7c6
okolí	okolí	k1gNnSc6
sopky	sopka	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
Během	během	k7c2
erupce	erupce	k1gFnSc2
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
vypuštěný	vypuštěný	k2eAgInSc1d1
oblak	oblak	k1gInSc1
kouře	kouř	k1gInSc2
měl	mít	k5eAaImAgInS
kruhový	kruhový	k2eAgInSc1d1
tvar	tvar	k1gInSc1
podobně	podobně	k6eAd1
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
když	když	k8xS
kuřáci	kuřák	k1gMnPc1
vytvoří	vytvořit	k5eAaPmIp3nP
ústy	ústa	k1gNnPc7
kouřový	kouřový	k2eAgInSc4d1
kroužek	kroužek	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takto	takto	k6eAd1
vzniklý	vzniklý	k2eAgInSc1d1
útvar	útvar	k1gInSc1
stoupal	stoupat	k5eAaImAgInS
z	z	k7c2
jícnu	jícen	k1gInSc2
sopky	sopka	k1gFnSc2
vzhůru	vzhůru	k6eAd1
po	po	k7c4
dobu	doba	k1gFnSc4
až	až	k9
10	#num#	k4
minut	minuta	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
než	než	k8xS
se	se	k3xPyFc4
vlivem	vliv	k1gInSc7
proudění	proudění	k1gNnSc2
vzduchu	vzduch	k1gInSc2
rozpadl	rozpadnout	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kroužky	kroužek	k1gInPc1
byly	být	k5eAaImAgInP
pozorovány	pozorovat	k5eAaImNgInP
v	v	k7c6
oblasti	oblast	k1gFnSc6
kráteru	kráter	k1gInSc2
Bocca	Bocca	k1gFnSc1
Nuova	Nuova	k1gFnSc1
<g/>
,	,	kIx,
ze	z	k7c2
kterého	který	k3yRgInSc2,k3yIgInSc2,k3yQgInSc2
unikly	uniknout	k5eAaPmAgFnP
<g/>
,	,	kIx,
následně	následně	k6eAd1
dorostly	dorůst	k5eAaPmAgFnP
průměru	průměr	k1gInSc6
100	#num#	k4
až	až	k9
200	#num#	k4
metrů	metr	k1gInPc2
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
ve	v	k7c6
výšce	výška	k1gFnSc6
okolo	okolo	k7c2
1000	#num#	k4
metrů	metr	k1gInPc2
nad	nad	k7c7
povrchem	povrch	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Význam	význam	k1gInSc1
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1
zachycující	zachycující	k2eAgFnSc2d1
jednotlivé	jednotlivý	k2eAgFnSc2d1
zóny	zóna	k1gFnSc2
národního	národní	k2eAgInSc2d1
parku	park	k1gInSc2
</s>
<s>
Přínosy	přínos	k1gInPc1
</s>
<s>
Etna	Etna	k1gFnSc1
je	být	k5eAaImIp3nS
pro	pro	k7c4
svoji	svůj	k3xOyFgFnSc4
snadnou	snadný	k2eAgFnSc4d1
dosažitelnost	dosažitelnost	k1gFnSc4
hlavní	hlavní	k2eAgFnSc7d1
turistickou	turistický	k2eAgFnSc7d1
atrakcí	atrakce	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
přitahuje	přitahovat	k5eAaImIp3nS
každoročně	každoročně	k6eAd1
až	až	k9
milión	milión	k4xCgInSc4
(	(	kIx(
<g/>
údaj	údaj	k1gInSc4
pravděpodobně	pravděpodobně	k6eAd1
k	k	k7c3
roku	rok	k1gInSc3
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
turistů	turist	k1gMnPc2
do	do	k7c2
oblasti	oblast	k1gFnSc2
Sicílie	Sicílie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
Každoroční	každoroční	k2eAgInSc1d1
příliv	příliv	k1gInSc1
turistů	turist	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yIgMnPc1,k3yRgMnPc1
chtějí	chtít	k5eAaImIp3nP
spatřit	spatřit	k5eAaPmF
aktivní	aktivní	k2eAgFnSc4d1
sopku	sopka	k1gFnSc4
<g/>
,	,	kIx,
dává	dávat	k5eAaImIp3nS
práci	práce	k1gFnSc4
většině	většina	k1gFnSc3
lidí	člověk	k1gMnPc2
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
oblasti	oblast	k1gFnSc6
v	v	k7c6
terciárním	terciární	k2eAgInSc6d1
sektoru	sektor	k1gInSc6
služeb	služba	k1gFnPc2
<g/>
,	,	kIx,
ať	ať	k8xC,k8xS
už	už	k6eAd1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
pohostinství	pohostinství	k1gNnSc4
<g/>
,	,	kIx,
ubytovací	ubytovací	k2eAgFnPc4d1
kapacity	kapacita	k1gFnPc4
či	či	k8xC
prodej	prodej	k1gInSc4
suvenýrů	suvenýr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Etna	Etna	k1gFnSc1
je	být	k5eAaImIp3nS
společně	společně	k6eAd1
se	se	k3xPyFc4
Stromboli	Strombole	k1gFnSc3
po	po	k7c6
Havajských	havajský	k2eAgFnPc6d1
sopkách	sopka	k1gFnPc6
pravděpodobně	pravděpodobně	k6eAd1
nejnavštěvovanější	navštěvovaný	k2eAgFnSc1d3
sopečná	sopečný	k2eAgFnSc1d1
oblast	oblast	k1gFnSc1
na	na	k7c6
světě	svět	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Etna	Etna	k1gFnSc1
společně	společně	k6eAd1
se	se	k3xPyFc4
Stromboli	Strombole	k1gFnSc6
sehrála	sehrát	k5eAaPmAgFnS
důležitou	důležitý	k2eAgFnSc4d1
roli	role	k1gFnSc4
ve	v	k7c6
vývoji	vývoj	k1gInSc6
zařízení	zařízení	k1gNnSc2
a	a	k8xC
sopečné	sopečný	k2eAgFnSc2d1
turistiky	turistika	k1gFnSc2
v	v	k7c6
průběhu	průběh	k1gInSc6
historie	historie	k1gFnSc2
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
centrem	centrum	k1gNnSc7
turistického	turistický	k2eAgInSc2d1
ruchu	ruch	k1gInSc2
již	již	k6eAd1
od	od	k7c2
druhé	druhý	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Důvodem	důvod	k1gInSc7
zájmu	zájem	k1gInSc2
se	se	k3xPyFc4
stala	stát	k5eAaPmAgFnS
neobvyklá	obvyklý	k2eNgFnSc1d1
krajina	krajina	k1gFnSc1
<g/>
,	,	kIx,
okolní	okolní	k2eAgFnSc1d1
scenérie	scenérie	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
přitáhla	přitáhnout	k5eAaPmAgFnS
zájem	zájem	k1gInSc4
malířů	malíř	k1gMnPc2
a	a	k8xC
umělců	umělec	k1gMnPc2
18	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
19	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
oblast	oblast	k1gFnSc4
zprofanovali	zprofanovat	k5eAaPmAgMnP
mezi	mezi	k7c4
širokou	široký	k2eAgFnSc4d1
veřejnost	veřejnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nepravidelné	pravidelný	k2eNgFnPc1d1
turistické	turistický	k2eAgFnPc1d1
návštěvy	návštěva	k1gFnPc1
trvaly	trvat	k5eAaImAgFnP
až	až	k9
do	do	k7c2
první	první	k4xOgFnSc2
poloviny	polovina	k1gFnSc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Od	od	k7c2
60	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc1
do	do	k7c2
oblasti	oblast	k1gFnSc2
začala	začít	k5eAaPmAgFnS
směřovat	směřovat	k5eAaImF
masová	masový	k2eAgFnSc1d1
turistika	turistika	k1gFnSc1
organizovaná	organizovaný	k2eAgFnSc1d1
cestovními	cestovní	k2eAgFnPc7d1
kanceláři	kancelář	k1gFnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
Od	od	k7c2
50	#num#	k4
<g/>
.	.	kIx.
let	léto	k1gNnPc2
20	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc6
je	být	k5eAaImIp3nS
v	v	k7c6
oblasti	oblast	k1gFnSc6
Etny	Etna	k1gFnSc2
zaznamenán	zaznamenán	k2eAgInSc1d1
soustavný	soustavný	k2eAgInSc1d1
nárůst	nárůst	k1gInSc1
turistů	turist	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
listopadu	listopad	k1gInSc6
roku	rok	k1gInSc2
2002	#num#	k4
bylo	být	k5eAaImAgNnS
otevřené	otevřený	k2eAgNnSc1d1
nové	nový	k2eAgNnSc1d1
muzeum	muzeum	k1gNnSc1
věnované	věnovaný	k2eAgNnSc1d1
Etně	Etna	k1gFnSc3
ve	v	k7c6
městě	město	k1gNnSc6
Nicolosi	Nicolose	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejstarší	starý	k2eAgFnSc1d3
informační	informační	k2eAgNnSc4d1
centrum	centrum	k1gNnSc4
o	o	k7c6
Etně	Etna	k1gFnSc6
Refugio	Refugio	k1gMnSc1
Sapiensa	Sapiensa	k1gFnSc1
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
v	v	k7c6
ležící	ležící	k2eAgFnSc6d1
1900	#num#	k4
m	m	kA
n.	n.	k?
m.	m.	k?
<g/>
,	,	kIx,
odkud	odkud	k6eAd1
se	se	k3xPyFc4
řada	řada	k1gFnSc1
turistů	turist	k1gMnPc2
vydává	vydávat	k5eAaPmIp3nS,k5eAaImIp3nS
na	na	k7c4
vlastní	vlastní	k2eAgNnSc4d1
nebezpečí	nebezpečí	k1gNnSc4
zdolávat	zdolávat	k5eAaImF
vrcholky	vrcholek	k1gInPc4
Etny	Etna	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
současnosti	současnost	k1gFnSc6
přístup	přístup	k1gInSc1
na	na	k7c4
Etnu	Etna	k1gFnSc4
zajišťuje	zajišťovat	k5eAaImIp3nS
čtveřice	čtveřice	k1gFnSc1
silnic	silnice	k1gFnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
vedou	vést	k5eAaImIp3nP
do	do	k7c2
dvou	dva	k4xCgNnPc2
hlavních	hlavní	k2eAgNnPc2d1
turistických	turistický	k2eAgNnPc2d1
center	centrum	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
Samotný	samotný	k2eAgInSc1d1
vrchol	vrchol	k1gInSc1
s	s	k7c7
krátery	kráter	k1gInPc7
je	být	k5eAaImIp3nS
od	od	k7c2
úmrtí	úmrtí	k1gNnSc2
devíti	devět	k4xCc3
turistů	turist	k1gMnPc2
v	v	k7c6
roce	rok	k1gInSc6
1979	#num#	k4
pro	pro	k7c4
návštěvníky	návštěvník	k1gMnPc4
uzavřen	uzavřen	k2eAgMnSc1d1
<g/>
,	,	kIx,
ale	ale	k8xC
za	za	k7c2
dobrého	dobrý	k2eAgNnSc2d1
počasí	počasí	k1gNnSc2
a	a	k8xC
klidné	klidný	k2eAgFnSc2d1
Etny	Etna	k1gFnSc2
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
se	se	k3xPyFc4
na	na	k7c4
vrchol	vrchol	k1gInSc4
ke	k	k7c3
kráterům	kráter	k1gInPc3
vydat	vydat	k5eAaPmF
s	s	k7c7
turistickým	turistický	k2eAgMnSc7d1
průvodcem	průvodce	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
oblasti	oblast	k1gFnSc6
okolo	okolo	k7c2
Etny	Etna	k1gFnSc2
včetně	včetně	k7c2
jejího	její	k3xOp3gInSc2
masívu	masív	k1gInSc2
se	se	k3xPyFc4
nachází	nacházet	k5eAaImIp3nS
národní	národní	k2eAgInSc1d1
park	park	k1gInSc1
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
vyhlášený	vyhlášený	k2eAgInSc1d1
v	v	k7c6
roce	rok	k1gInSc6
1987	#num#	k4
jako	jako	k9
reakce	reakce	k1gFnPc1
na	na	k7c4
narůstající	narůstající	k2eAgInSc4d1
turismus	turismus	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sopečné	sopečný	k2eAgFnPc1d1
erupce	erupce	k1gFnPc1
vyvrhují	vyvrhovat	k5eAaImIp3nP
do	do	k7c2
okolí	okolí	k1gNnSc2
sopky	sopka	k1gFnSc2
materiál	materiál	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yIgInSc1,k3yQgInSc1
je	být	k5eAaImIp3nS
velice	velice	k6eAd1
úrodný	úrodný	k2eAgMnSc1d1
a	a	k8xC
zúrodňuje	zúrodňovat	k5eAaImIp3nS
okolí	okolí	k1gNnSc4
sopky	sopka	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yRnSc1,k3yQnSc1
v	v	k7c6
jejím	její	k3xOp3gNnSc6
okolí	okolí	k1gNnSc6
umožňuje	umožňovat	k5eAaImIp3nS
masivní	masivní	k2eAgNnSc4d1
pěstování	pěstování	k1gNnSc4
vinné	vinný	k2eAgFnSc2d1
révy	réva	k1gFnSc2
<g/>
,	,	kIx,
různých	různý	k2eAgInPc2d1
druhů	druh	k1gInPc2
ovoce	ovoce	k1gNnSc2
<g/>
,	,	kIx,
farmaření	farmaření	k1gNnSc2
atd.	atd.	kA
Každá	každý	k3xTgFnSc1
sopečná	sopečný	k2eAgFnSc1d1
erupce	erupce	k1gFnSc1
tak	tak	k6eAd1
přináší	přinášet	k5eAaImIp3nS
nové	nový	k2eAgFnPc4d1
živiny	živina	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
se	se	k3xPyFc4
v	v	k7c6
následujících	následující	k2eAgNnPc6d1
letech	léto	k1gNnPc6
po	po	k7c4
jejich	jejich	k3xOp3gNnSc4
vyvržení	vyvržení	k1gNnSc4
na	na	k7c4
povrch	povrch	k1gInSc4
budou	být	k5eAaImBp3nP
uvolňovat	uvolňovat	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nebezpečí	nebezpečí	k1gNnSc1
</s>
<s>
[[	[[	k?
<g/>
file	file	k1gNnPc6
<g/>
:	:	kIx,
<g/>
World	World	k1gInSc1
location	location	k1gInSc1
map	mapa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
svg	svg	k?
<g/>
|	|	kIx~
<g/>
360	#num#	k4
<g/>
px	px	k?
<g/>
|	|	kIx~
<g/>
{{	{{	k?
<g/>
#	#	kIx~
<g/>
if	if	k?
<g/>
:	:	kIx,
<g/>
||	||	k?
<g/>
]]	]]	k?
<g/>
Mauna	Mauen	k2eAgFnSc1d1
Loa	Loa	k1gFnSc1
</s>
<s>
Mount	Mount	k1gMnSc1
Rainier	Rainier	k1gMnSc1
</s>
<s>
Colima	Colima	k1gFnSc1
</s>
<s>
Santa	Santa	k1gMnSc1
María	María	k1gMnSc1
</s>
<s>
Galeras	Galeras	k1gMnSc1
</s>
<s>
Teide	Teide	k6eAd1
</s>
<s>
Vesuv	Vesuv	k1gInSc1
</s>
<s>
Etna	Etna	k1gFnSc1
</s>
<s>
Santorini	Santorin	k1gMnPc1
</s>
<s>
Nyiragongo	Nyiragongo	k6eAd1
</s>
<s>
Merapi	Merapi	k6eAd1
</s>
<s>
Ulawun	Ulawun	k1gMnSc1
</s>
<s>
Taal	Taal	k1gMnSc1
</s>
<s>
Sakuradžima	Sakuradžima	k1gFnSc1
</s>
<s>
Unzen	Unzen	k1gInSc1
</s>
<s>
Avačinskij	Avačinskít	k5eAaPmRp2nS
</s>
<s>
Korjacká	Korjacký	k2eAgFnSc1d1
sopka	sopka	k1gFnSc1
</s>
<s>
Decade	Decást	k5eAaPmIp3nS
Volcanoes	Volcanoes	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Les	les	k1gInSc1
na	na	k7c6
svahu	svah	k1gInSc6
Etny	Etna	k1gFnSc2
zničený	zničený	k2eAgInSc4d1
množstvím	množství	k1gNnSc7
spadlé	spadlý	k2eAgFnSc2d1
tefry	tefra	k1gFnSc2
</s>
<s>
Etna	Etna	k1gFnSc1
se	se	k3xPyFc4
projevuje	projevovat	k5eAaImIp3nS
několika	několik	k4yIc2
druhy	druh	k1gInPc1
nebezpečí	nebezpečí	k1gNnSc1
<g/>
,	,	kIx,
ať	ať	k8xC,k8xS
už	už	k6eAd1
se	se	k3xPyFc4
jedná	jednat	k5eAaImIp3nS
o	o	k7c4
erupce	erupce	k1gFnPc4
<g/>
,	,	kIx,
úniky	únik	k1gInPc4
sopečných	sopečný	k2eAgInPc2d1
plynů	plyn	k1gInPc2
<g/>
,	,	kIx,
gravitačními	gravitační	k2eAgInPc7d1
kolapsy	kolaps	k1gInPc7
či	či	k8xC
spadem	spad	k1gInSc7
tefry	tefra	k1gFnSc2
do	do	k7c2
okolí	okolí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Různé	různý	k2eAgFnPc4d1
formy	forma	k1gFnPc4
nebezpečí	nebezpečí	k1gNnSc2
mají	mít	k5eAaImIp3nP
rozdílný	rozdílný	k2eAgInSc4d1
rozsah	rozsah	k1gInSc4
<g/>
,	,	kIx,
množství	množství	k1gNnSc4
materiálních	materiální	k2eAgFnPc2d1
škod	škoda	k1gFnPc2
a	a	k8xC
možností	možnost	k1gFnSc7
jejich	jejich	k3xOp3gNnSc2
předpovídání	předpovídání	k1gNnSc2
a	a	k8xC
případné	případný	k2eAgFnSc6d1
obraně	obrana	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
ty	ten	k3xDgInPc4
<g/>
,	,	kIx,
se	s	k7c7
kterými	který	k3yIgInPc7,k3yQgInPc7,k3yRgInPc7
lidé	člověk	k1gMnPc1
mohou	moct	k5eAaImIp3nP
bojovat	bojovat	k5eAaImF
<g/>
,	,	kIx,
patří	patřit	k5eAaImIp3nP
lávové	lávový	k2eAgInPc1d1
proudy	proud	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
případě	případ	k1gInSc6
<g/>
,	,	kIx,
že	že	k8xS
směřují	směřovat	k5eAaImIp3nP
na	na	k7c6
obydlené	obydlený	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
,	,	kIx,
snaží	snažit	k5eAaImIp3nS
se	se	k3xPyFc4
je	být	k5eAaImIp3nS
odpovědné	odpovědný	k2eAgFnSc2d1
složky	složka	k1gFnSc2
odklonit	odklonit	k5eAaPmF
či	či	k8xC
případně	případně	k6eAd1
zastavit	zastavit	k5eAaPmF
a	a	k8xC
to	ten	k3xDgNnSc1
za	za	k7c2
pomoci	pomoc	k1gFnSc2
nového	nový	k2eAgNnSc2d1
koryta	koryto	k1gNnSc2
<g/>
,	,	kIx,
kterým	který	k3yIgInSc7,k3yRgInSc7,k3yQgInSc7
by	by	kYmCp3nP
láva	láva	k1gFnSc1
odtékala	odtékat	k5eAaImAgFnS
mimo	mimo	k7c4
ohroženou	ohrožený	k2eAgFnSc4d1
oblast	oblast	k1gFnSc4
či	či	k8xC
například	například	k6eAd1
explozemi	exploze	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
by	by	kYmCp3nP
měly	mít	k5eAaImAgFnP
přehradit	přehradit	k5eAaPmF
lávový	lávový	k2eAgInSc4d1
tunel	tunel	k1gInSc4
či	či	k8xC
koryto	koryto	k1gNnSc4
a	a	k8xC
donutit	donutit	k5eAaPmF
lávu	láva	k1gFnSc4
změnit	změnit	k5eAaPmF
směr	směr	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
oblasti	oblast	k1gFnSc6
Etny	Etna	k1gFnSc2
existuje	existovat	k5eAaImIp3nS
systém	systém	k1gInSc1
včasné	včasný	k2eAgFnSc2d1
výstrahy	výstraha	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
má	mít	k5eAaImIp3nS
varovat	varovat	k5eAaImF
obyvatelstvo	obyvatelstvo	k1gNnSc4
v	v	k7c6
případě	případ	k1gInSc6
náhlého	náhlý	k2eAgNnSc2d1
nebezpečí	nebezpečí	k1gNnSc2
způsobeného	způsobený	k2eAgNnSc2d1
erupcí	erupce	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
vážná	vážný	k2eAgNnPc4d1
nebezpečí	nebezpečí	k1gNnPc4
zasahující	zasahující	k2eAgFnSc2d1
velké	velký	k2eAgFnSc2d1
oblasti	oblast	k1gFnSc2
patří	patřit	k5eAaImIp3nS
spad	spad	k1gInSc1
tefry	tefra	k1gFnSc2
vznikající	vznikající	k2eAgFnSc2d1
během	během	k7c2
erupce	erupce	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Menší	malý	k2eAgFnPc1d2
částice	částice	k1gFnPc1
jsou	být	k5eAaImIp3nP
schopné	schopný	k2eAgFnPc1d1
cestovat	cestovat	k5eAaImF
desítky	desítka	k1gFnPc4
kilometrů	kilometr	k1gInPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
pak	pak	k6eAd1
padají	padat	k5eAaImIp3nP
a	a	k8xC
zasypávají	zasypávat	k5eAaImIp3nP
oblasti	oblast	k1gFnPc4
částicemi	částice	k1gFnPc7
v	v	k7c6
podobě	podoba	k1gFnSc6
prachu	prach	k1gInSc2
a	a	k8xC
lapil	lapit	k5eAaPmAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
oblak	oblak	k1gInSc4
představuje	představovat	k5eAaImIp3nS
nebezpečí	nebezpečí	k1gNnSc1
jak	jak	k8xS,k8xC
pro	pro	k7c4
leteckou	letecký	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
tak	tak	k6eAd1
v	v	k7c6
případě	případ	k1gInSc6
silného	silný	k2eAgInSc2d1
spadu	spad	k1gInSc2
i	i	k9
pro	pro	k7c4
spádové	spádový	k2eAgFnPc4d1
oblasti	oblast	k1gFnPc4
<g/>
,	,	kIx,
kde	kde	k6eAd1
narušuje	narušovat	k5eAaImIp3nS
automobilovou	automobilový	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
a	a	k8xC
může	moct	k5eAaImIp3nS
způsobit	způsobit	k5eAaPmF
i	i	k9
zřícení	zřícení	k1gNnSc1
střech	střecha	k1gFnPc2
budov	budova	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
v	v	k7c6
oblasti	oblast	k1gFnSc6
existuje	existovat	k5eAaImIp3nS
systém	systém	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
poloautomaticky	poloautomaticky	k6eAd1
vyhodnocuje	vyhodnocovat	k5eAaImIp3nS
potenciální	potenciální	k2eAgNnSc1d1
nebezpečí	nebezpečí	k1gNnSc1
spadu	spad	k1gInSc2
tefry	tefra	k1gFnSc2
a	a	k8xC
směru	směr	k1gInSc2
větru	vítr	k1gInSc2
a	a	k8xC
v	v	k7c6
případě	případ	k1gInSc6
potřeby	potřeba	k1gFnSc2
informuje	informovat	k5eAaBmIp3nS
příslušné	příslušný	k2eAgInPc4d1
orgány	orgán	k1gInPc4
civilní	civilní	k2eAgFnSc2d1
správy	správa	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Etna	Etna	k1gFnSc1
v	v	k7c6
kultuře	kultura	k1gFnSc6
</s>
<s>
Dle	dle	k7c2
řecké	řecký	k2eAgFnSc2d1
mytologie	mytologie	k1gFnSc2
je	být	k5eAaImIp3nS
sopka	sopka	k1gFnSc1
Etna	Etna	k1gFnSc1
spojována	spojovat	k5eAaImNgFnS
se	s	k7c7
soubojem	souboj	k1gInSc7
Dia	Dia	k1gFnSc2
s	s	k7c7
největším	veliký	k2eAgInSc7d3
z	z	k7c2
titánu	titán	k1gMnSc3
Týfónem	Týfón	k1gInSc7
<g/>
,	,	kIx,
synem	syn	k1gMnSc7
Tartara	Tartar	k1gMnSc2
a	a	k8xC
matky	matka	k1gFnSc2
země	zem	k1gFnSc2
Gai	Gai	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
souboji	souboj	k1gInSc6
mezi	mezi	k7c7
nimi	on	k3xPp3gMnPc7
byl	být	k5eAaImAgMnS
Týfón	Týfón	k1gMnSc1
poražen	porazit	k5eAaPmNgMnS
a	a	k8xC
Zeus	Zeus	k1gInSc4
utíkajícího	utíkající	k2eAgMnSc2d1
titána	titán	k1gMnSc2
dostihl	dostihnout	k5eAaPmAgMnS
v	v	k7c6
oblasti	oblast	k1gFnSc6
Sicílie	Sicílie	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
na	na	k7c4
něj	on	k3xPp3gMnSc4
svrhl	svrhnout	k5eAaPmAgMnS
horu	hora	k1gFnSc4
Etna	Etna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Když	když	k8xS
byl	být	k5eAaImAgMnS
Týfón	Týfón	k1gMnSc1
uvězněn	uvěznit	k5eAaPmNgMnS
pod	pod	k7c7
Etnou	Etna	k1gFnSc7
<g/>
,	,	kIx,
všech	všecek	k3xTgInPc2
100	#num#	k4
jeho	jeho	k3xOp3gFnPc2
hlav	hlava	k1gFnPc2
se	se	k3xPyFc4
okamžitě	okamžitě	k6eAd1
pokusilo	pokusit	k5eAaPmAgNnS
dostat	dostat	k5eAaPmF
na	na	k7c4
povrch	povrch	k1gInSc4
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
projevilo	projevit	k5eAaPmAgNnS
v	v	k7c6
množství	množství	k1gNnSc6
erupcí	erupce	k1gFnPc2
a	a	k8xC
sloupů	sloup	k1gInPc2
kouře	kouř	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokaždé	pokaždé	k6eAd1
když	když	k8xS
Etna	Etna	k1gFnSc1
začne	začít	k5eAaPmIp3nS
soptit	soptit	k5eAaImF
<g/>
,	,	kIx,
dle	dle	k7c2
pověstí	pověst	k1gFnPc2
se	se	k3xPyFc4
Týfón	Týfón	k1gInSc1
pokouší	pokoušet	k5eAaImIp3nS
uniknout	uniknout	k5eAaPmF
pod	pod	k7c4
tíhy	tíha	k1gFnPc4
Etny	Etna	k1gFnSc2
na	na	k7c4
povrch	povrch	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Turistika	turistika	k1gFnSc1
</s>
<s>
Dolní	dolní	k2eAgFnSc1d1
stanice	stanice	k1gFnSc1
lanovky	lanovka	k1gFnSc2
na	na	k7c4
Etnu	Etna	k1gFnSc4
(	(	kIx(
<g/>
1923	#num#	k4
m	m	kA
n.	n.	k?
<g/>
m.	m.	k?
<g/>
)	)	kIx)
</s>
<s>
Etna	Etna	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
současné	současný	k2eAgFnSc6d1
době	doba	k1gFnSc6
oblíbeným	oblíbený	k2eAgInSc7d1
turistickým	turistický	k2eAgInSc7d1
cílem	cíl	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výstup	výstup	k1gInSc1
až	až	k6eAd1
k	k	k7c3
hlavnímu	hlavní	k2eAgInSc3d1
kráteru	kráter	k1gInSc3
je	být	k5eAaImIp3nS
možný	možný	k2eAgInSc4d1
pouze	pouze	k6eAd1
s	s	k7c7
turistickým	turistický	k2eAgMnSc7d1
průvodcem	průvodce	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bez	bez	k7c2
průvodce	průvodce	k1gMnSc2
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
vystoupat	vystoupat	k5eAaPmF
pouze	pouze	k6eAd1
na	na	k7c4
Torre	torr	k1gInSc5
del	del	k?
Filosofo	Filosofo	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
méně	málo	k6eAd2
zdatné	zdatný	k2eAgMnPc4d1
turisty	turist	k1gMnPc4
je	být	k5eAaImIp3nS
možné	možný	k2eAgNnSc1d1
využít	využít	k5eAaPmF
dopravu	doprava	k1gFnSc4
lanovkou	lanovka	k1gFnSc7
a	a	k8xC
následně	následně	k6eAd1
autobusy	autobus	k1gInPc7
4	#num#	k4
<g/>
x	x	k?
<g/>
4	#num#	k4
na	na	k7c6
tuto	tento	k3xDgFnSc4
plošinu	plošina	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Mount	Mount	k1gMnSc1
Etna	Etna	k1gFnSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
The	The	k1gFnSc1
University	universita	k1gFnSc2
of	of	k?
Chicago	Chicago	k1gNnSc1
Library	Librara	k1gFnSc2
Woodhouse	Woodhouse	k1gFnSc2
<g/>
'	'	kIx"
<g/>
s	s	k7c7
English-Greek	English-Greek	k1gInSc1
Dictionary	Dictionar	k1gInPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Toponomastica	Toponomastica	k1gFnSc1
Generale	General	k1gMnSc5
e	e	k0
Toponomastica	Toponomastic	k2eAgFnSc1d1
Italiana	Italiana	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
italsky	italsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Sopka	sopka	k1gFnSc1
Etna	Etna	k1gFnSc1
je	být	k5eAaImIp3nS
zapsána	zapsat	k5eAaPmNgFnS
na	na	k7c6
seznamu	seznam	k1gInSc6
UNESCO	Unesco	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ekolist	Ekolist	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
11	#num#	k4
12	#num#	k4
13	#num#	k4
14	#num#	k4
15	#num#	k4
SEACH	SEACH	kA
<g/>
,	,	kIx,
John	John	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volcano	Volcana	k1gFnSc5
live	live	k1gNnPc1
-	-	kIx~
Mt	Mt	k1gFnSc1
Etna	Etna	k1gFnSc1
Volcano	Volcana	k1gFnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Italy	Ital	k1gMnPc4
volcanoes	volcanoesa	k1gFnPc2
and	and	k?
Volcanics	Volcanics	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
USGS	USGS	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
https://volcano.si.edu/volcano.cfm?vn=211060&	https://volcano.si.edu/volcano.cfm?vn=211060&	k1gMnSc1
<g/>
↑	↑	k?
https://www.volcanodiscovery.com/etna.html	https://www.volcanodiscovery.com/etna.html	k1gMnSc1
<g/>
↑	↑	k?
Decade	Decad	k1gInSc5
Volcanoes	Volcanoes	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
United	United	k1gMnSc1
States	States	k1gMnSc1
Geological	Geological	k1gMnSc1
Survey	Survea	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
CHESTER	CHESTER	kA
<g/>
,	,	kIx,
D.	D.	kA
K.	K.	kA
Mount	Mounta	k1gFnPc2
Etna	Etna	k1gFnSc1
<g/>
:	:	kIx,
the	the	k?
anatomy	anatom	k1gMnPc4
of	of	k?
a	a	k8xC
volcano	volcana	k1gFnSc5
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
vyd	vyd	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Stanford	Stanford	k1gMnSc1
Univ	Univ	k1gMnSc1
Pr	pr	k0
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
804713085	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
71	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
The	The	k1gFnPc2
morphology	morpholog	k1gMnPc4
of	of	k?
Etna	Etna	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
11	#num#	k4
12	#num#	k4
13	#num#	k4
Mt	Mt	k1gFnPc2
<g/>
.	.	kIx.
etna	etna	k1gMnSc1
morphologic	morphologic	k1gMnSc1
features	features	k1gMnSc1
<g/>
:	:	kIx,
Cones	Cones	k1gMnSc1
and	and	k?
craters	craters	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
Mount	Mounto	k1gNnPc2
Etna	Etna	k1gFnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
Sicilian	Sicilian	k1gMnSc1
volcano	volcana	k1gFnSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
FAVALLI	FAVALLI	kA
<g/>
,	,	kIx,
Massimilano	Massimilana	k1gFnSc5
<g/>
;	;	kIx,
KARÁTSON	KARÁTSON	kA
<g/>
,	,	kIx,
Dávid	Dávid	k1gInSc1
<g/>
;	;	kIx,
MAZZARINI	MAZZARINI	kA
<g/>
,	,	kIx,
Francesco	Francesco	k1gMnSc1
<g/>
;	;	kIx,
PARESCHI	PARESCHI	kA
<g/>
,	,	kIx,
Maria	Maria	k1gFnSc1
Teresa	Teresa	k1gFnSc1
<g/>
;	;	kIx,
BOSCHI	BOSCHI	kA
<g/>
,	,	kIx,
Enzo	Enzo	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Morphometry	Morphometr	k1gInPc7
of	of	k?
scoria	scorium	k1gNnSc2
cones	conesa	k1gFnPc2
located	located	k1gMnSc1
on	on	k3xPp3gMnSc1
a	a	k8xC
volcano	volcana	k1gFnSc5
flank	flank	k1gInSc4
<g/>
:	:	kIx,
A	a	k9
case	case	k6eAd1
study	stud	k1gInPc1
from	froma	k1gFnPc2
Mt	Mt	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Etna	Etna	k1gFnSc1
(	(	kIx(
<g/>
Italy	Ital	k1gMnPc7
<g/>
)	)	kIx)
<g/>
,	,	kIx,
based	based	k1gMnSc1
on	on	k3xPp3gMnSc1
high-resolution	high-resolution	k1gInSc4
LiDAR	LiDAR	k1gFnSc7
data	datum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
Volcanology	Volcanolog	k1gMnPc4
and	and	k?
Geothermal	Geothermal	k1gMnSc1
Research	Research	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
October	October	k1gInSc1
2009	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
320	#num#	k4
<g/>
–	–	k?
<g/>
330	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
j.	j.	k?
<g/>
jvolgeores	jvolgeores	k1gInSc1
<g/>
.2009	.2009	k4
<g/>
.07	.07	k4
<g/>
.011	.011	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
journal	journat	k5eAaPmAgMnS,k5eAaImAgMnS
<g/>
}}	}}	k?
označená	označený	k2eAgNnPc4d1
jako	jako	k8xC,k8xS
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
Chester	Chestra	k1gFnPc2
<g/>
,	,	kIx,
strana	strana	k1gFnSc1
74.1	74.1	k4
2	#num#	k4
3	#num#	k4
Chester	Chestra	k1gFnPc2
<g/>
;	;	kIx,
strana	strana	k1gFnSc1
73	#num#	k4
<g/>
↑	↑	k?
Etna	Etna	k1gFnSc1
Mountain	Mountain	k1gMnSc1
<g/>
:	:	kIx,
Tectonic	Tectonice	k1gFnPc2
setting	setting	k1gInSc1
and	and	k?
geological	geologicat	k5eAaPmAgInS
evolution	evolution	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
MARTIN-SCHUTZ	MARTIN-SCHUTZ	k1gFnPc2
<g/>
,	,	kIx,
ALICIA	ALICIA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mt	Mt	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Etna	Etna	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
10	#num#	k4
11	#num#	k4
12	#num#	k4
13	#num#	k4
14	#num#	k4
Mt	Mt	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Etna	Etna	k1gFnSc1
geological	geologicat	k5eAaPmAgMnS
history	histor	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
PARESCHI	PARESCHI	kA
<g/>
,	,	kIx,
Maria	Maria	k1gFnSc1
Teresa	Teresa	k1gFnSc1
<g/>
;	;	kIx,
BOSCHI	BOSCHI	kA
<g/>
,	,	kIx,
Enzo	Enzo	k1gMnSc1
<g/>
;	;	kIx,
FAVALLI	FAVALLI	kA
<g/>
,	,	kIx,
Massimiliano	Massimiliana	k1gFnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lost	Lost	k1gInSc1
tsunami	tsunami	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
L	L	kA
<g/>
22608	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geophysical	Geophysical	k1gMnSc1
Research	Research	k1gMnSc1
Letters	Letters	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2006-11-28	2006-11-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
33	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
22	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
L	L	kA
<g/>
22608	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
DOI	DOI	kA
<g/>
:	:	kIx,
<g/>
10.102	10.102	k4
<g/>
9	#num#	k4
<g/>
/	/	kIx~
<g/>
2006	#num#	k4
<g/>
GL	GL	kA
<g/>
0	#num#	k4
<g/>
27790	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
The	The	k1gFnPc2
Etna	Etna	k1gFnSc1
eruptions	eruptions	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Publius	Publius	k1gMnSc1
Vergilius	Vergilius	k1gMnSc1
Maro	Maro	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aeneida	Aeneida	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Překlad	překlad	k1gInSc1
Karel	Karel	k1gMnSc1
Vinařický	Vinařický	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
I.	I.	kA
L.	L.	kA
Kober	kobra	k1gFnPc2
<g/>
,	,	kIx,
1885	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kniha	kniha	k1gFnSc1
třetí	třetí	k4xOgFnSc1
<g/>
,	,	kIx,
s.	s.	k?
72	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Verše	verš	k1gInSc2
570	#num#	k4
<g/>
–	–	k?
<g/>
577	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Coltelli	Coltelle	k1gFnSc4
<g/>
,	,	kIx,
M.	M.	kA
<g/>
,	,	kIx,
Del	Del	k1gFnPc1
Carlo	Carlo	k1gNnSc1
<g/>
,	,	kIx,
P.	P.	kA
and	and	k?
Vezzoli	Vezzole	k1gFnSc3
<g/>
,	,	kIx,
L.	L.	kA
(	(	kIx(
<g/>
1998	#num#	k4
<g/>
)	)	kIx)
Discovery	Discovera	k1gFnPc1
of	of	k?
a	a	k8xC
Plinian	Plinian	k1gInSc1
basaltic	basaltice	k1gFnPc2
eruption	eruption	k1gInSc1
of	of	k?
Roman	Roman	k1gMnSc1
age	age	k?
at	at	k?
Etna	Etna	k1gFnSc1
Volcano	Volcana	k1gFnSc5
<g/>
,	,	kIx,
Italy	Ital	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Geology	geolog	k1gMnPc7
<g/>
,	,	kIx,
vol	vol	k6eAd1
<g/>
.	.	kIx.
26	#num#	k4
<g/>
,	,	kIx,
p.	p.	k?
1095	#num#	k4
<g/>
–	–	k?
<g/>
1098	#num#	k4
<g/>
↑	↑	k?
Barberi	Barberi	k1gNnSc1
<g/>
,	,	kIx,
F.	F.	kA
<g/>
,	,	kIx,
Carapezza	Carapezza	k1gFnSc1
<g/>
,	,	kIx,
M.	M.	kA
L.	L.	kA
<g/>
,	,	kIx,
Valenza	Valenza	k1gFnSc1
<g/>
,	,	kIx,
M.	M.	kA
<g/>
,	,	kIx,
Villari	Villari	k1gNnSc2
<g/>
,	,	kIx,
L.	L.	kA
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
The	The	k1gFnSc1
control	control	k1gInSc1
of	of	k?
lava	lava	k1gFnSc1
flow	flow	k?
during	during	k1gInSc1
the	the	k?
1991	#num#	k4
<g/>
–	–	k?
<g/>
1992	#num#	k4
eruption	eruption	k1gInSc1
of	of	k?
Mt	Mt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Etna	Etna	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Journal	Journal	k1gFnSc1
of	of	k?
Volcanology	Volcanolog	k1gMnPc4
and	and	k?
Geothermal	Geothermal	k1gMnSc1
Research	Research	k1gMnSc1
<g/>
,	,	kIx,
vol	vol	k6eAd1
<g/>
.	.	kIx.
56	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
1	#num#	k4
<g/>
–	–	k?
<g/>
34	#num#	k4
<g/>
↑	↑	k?
Star	Star	kA
Wars	Wars	k1gInSc4
<g/>
:	:	kIx,
Episode	Episod	k1gMnSc5
III	III	kA
-	-	kIx~
Revenge	Reveng	k1gMnSc2
of	of	k?
the	the	k?
Sith	Sith	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lucasfilm	Lucasfilm	k1gInSc1
Ltd	ltd	kA
<g/>
.	.	kIx.
&	&	k?
TM	TM	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Etna	Etna	k1gFnSc1
<g/>
:	:	kIx,
Attività	Attività	k1gFnSc1
in	in	k?
corso	corsa	k1gFnSc5
Archivováno	archivován	k2eAgNnSc4d1
7	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
Istituto	Istitut	k2eAgNnSc1d1
Nazionale	Nazionale	k1gMnPc7
di	di	k?
Geofisica	Geofisic	k1gInSc2
e	e	k0
Vulcanologia	Vulcanologium	k1gNnPc1
<g/>
,	,	kIx,
Sezione	Sezion	k1gInSc5
di	di	k?
Catania	Catanium	k1gNnSc2
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
Etna	Etna	k1gFnSc1
hoops	hoopsa	k1gFnPc2
it	it	k?
up	up	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
BBC	BBC	kA
News	Newsa	k1gFnPc2
<g/>
.	.	kIx.
2000	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2008	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
ETNA	Etna	k1gFnSc1
VOLCANO	VOLCANO	kA
PHOTOS	PHOTOS	kA
-	-	kIx~
Etna	Etna	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
smoke	smoke	k1gNnSc7
rings	rings	k1gInSc1
in	in	k?
July	Jula	k1gFnSc2
2000	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
5	#num#	k4
6	#num#	k4
7	#num#	k4
8	#num#	k4
9	#num#	k4
Volcano-tourism	Volcano-tourisma	k1gFnPc2
and	and	k?
its	its	k?
influence	influence	k1gFnSc2
on	on	k3xPp3gMnSc1
the	the	k?
territory	territor	k1gInPc1
of	of	k?
Mount	Mount	k1gInSc1
Etna	Etna	k1gFnSc1
(	(	kIx(
<g/>
Italy	Ital	k1gMnPc4
<g/>
)	)	kIx)
–	–	k?
explored	explored	k1gInSc1
with	witha	k1gFnPc2
digressions	digressions	k6eAd1
to	ten	k3xDgNnSc4
Stromboli	Strombole	k1gFnSc4
(	(	kIx(
<g/>
Italy	Ital	k1gMnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
www.vulkan-tourismus.de	www.vulkan-tourismus.de	k6eAd1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
O	O	kA
<g/>
'	'	kIx"
<g/>
ROURKE	ROURKE	kA
<g/>
,	,	kIx,
Peter	Peter	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
impact	impact	k1gMnSc1
of	of	k?
Mt	Mt	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Etna	Etna	k1gFnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
eruption	eruption	k1gInSc4
on	on	k3xPp3gMnSc1
Italian	Italian	k1gMnSc1
citizens	citizensa	k1gFnPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2011	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Proposal	Proposal	k1gMnSc1
<g/>
:	:	kIx,
Info	Info	k1gMnSc1
Center	centrum	k1gNnPc2
at	at	k?
Mt	Mt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Etna	Etna	k1gFnSc1
<g/>
,	,	kIx,
Italy	Ital	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2010	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Parco	Parco	k1gMnSc1
dell	dell	k1gMnSc1
<g/>
'	'	kIx"
<g/>
Etna	Etna	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Parks	Parks	k1gInSc1
<g/>
.	.	kIx.
<g/>
it	it	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ALPARONE	ALPARONE	kA
<g/>
,	,	kIx,
Salvatore	Salvator	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Alert	Alerta	k1gFnPc2
system	syst	k1gInSc7
to	ten	k3xDgNnSc1
mitigate	mitigat	k1gInSc5
tephra	tephra	k1gFnSc1
fallout	fallout	k5eAaPmF
hazards	hazards	k1gInSc4
at	at	k?
Mt	Mt	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Etna	Etna	k1gFnSc1
Volcano	Volcana	k1gFnSc5
<g/>
,	,	kIx,
Italy	Ital	k1gMnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Natural	Natural	k?
Hazards	Hazards	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
20	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
SIGURDSSON	SIGURDSSON	kA
<g/>
,	,	kIx,
Haraldur	Haraldur	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Encyclopedia	Encyclopedium	k1gNnSc2
of	of	k?
Volcanoes	Volcanoes	k1gMnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Academic	Academic	k1gMnSc1
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
1999	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
643140	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kapitola	kapitola	k1gFnSc1
The	The	k1gFnSc1
History	Histor	k1gInPc1
of	of	k?
Volcanology	Volcanolog	k1gMnPc7
<g/>
,	,	kIx,
s.	s.	k?
19	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Výstup	výstup	k1gInSc1
na	na	k7c4
Etnu	Etna	k1gFnSc4
<g/>
:	:	kIx,
vše	všechen	k3xTgNnSc1
<g/>
,	,	kIx,
co	co	k3yInSc4,k3yQnSc4,k3yRnSc4
potřebujete	potřebovat	k5eAaImIp2nP
vědět	vědět	k5eAaImF
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
My	my	k3xPp1nPc1
Wandertime	Wandertim	k1gInSc5
<g/>
,	,	kIx,
2019-06-24	2019-06-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Literatura	literatura	k1gFnSc1
</s>
<s>
CHESTER	CHESTER	kA
<g/>
,	,	kIx,
D.	D.	kA
K.	K.	kA
<g/>
,	,	kIx,
DUNCAN	DUNCAN	kA
<g/>
,	,	kIx,
A.	A.	kA
M.	M.	kA
<g/>
,	,	kIx,
GUEST	GUEST	kA
<g/>
,	,	kIx,
J.	J.	kA
E.	E.	kA
<g/>
,	,	kIx,
a	a	k8xC
KILBURN	KILBURN	kA
<g/>
,	,	kIx,
C.	C.	kA
R.	R.	kA
J.	J.	kA
<g/>
,	,	kIx,
Mount	Mount	k1gInSc1
Etna	Etna	k1gFnSc1
<g/>
:	:	kIx,
the	the	k?
anatomy	anatom	k1gMnPc4
of	of	k?
a	a	k8xC
volcano	volcana	k1gFnSc5
<g/>
,	,	kIx,
Stanford	Stanford	k1gMnSc1
Univ	Univ	k1gMnSc1
Pr	pr	k0
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
-	-	kIx~
<g/>
8047	#num#	k4
<g/>
-	-	kIx~
<g/>
1308	#num#	k4
<g/>
-	-	kIx~
<g/>
5	#num#	k4
<g/>
,	,	kIx,
1985	#num#	k4
<g/>
,	,	kIx,
412	#num#	k4
stran	strana	k1gFnPc2
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Etna	Etna	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
http://whc.unesco.org/en/news/1049/	http://whc.unesco.org/en/news/1049/	k4
</s>
<s>
Webové	webový	k2eAgFnPc1d1
kamery	kamera	k1gFnPc1
Osservatorio	Osservatorio	k6eAd1
Etneo	Etneo	k6eAd1
(	(	kIx(
<g/>
Istituto	Istitut	k2eAgNnSc1d1
Nazionale	Nazionale	k1gMnPc7
di	di	k?
Geofisica	Geofisic	k1gInSc2
e	e	k0
Vulcanologia	Vulcanologium	k1gNnPc4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
italsky	italsky	k6eAd1
a	a	k8xC
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gMnSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gMnSc1
<g/>
}	}	kIx)
Italské	italský	k2eAgFnSc2d1
památky	památka	k1gFnSc2
na	na	k7c6
seznamu	seznam	k1gInSc6
světového	světový	k2eAgNnSc2d1
dědictví	dědictví	k1gNnSc2
UNESCO	UNESCO	kA
</s>
<s>
Agrigento	Agrigento	k1gNnSc1
•	•	k?
Alberobello	Alberobello	k1gNnSc1
•	•	k?
Amalfinské	Amalfinský	k2eAgNnSc4d1
pobřeží	pobřeží	k1gNnSc4
•	•	k?
Aquileia	Aquileius	k1gMnSc2
•	•	k?
Assisi	Assise	k1gFnSc4
•	•	k?
Benátky	Benátky	k1gFnPc1
a	a	k8xC
jejich	jejich	k3xOp3gFnSc1
laguna	laguna	k1gFnSc1
•	•	k?
Benátské	benátský	k2eAgFnSc2d1
obranné	obranný	k2eAgFnSc2d1
stavby	stavba	k1gFnSc2
•	•	k?
Botanická	botanický	k2eAgFnSc1d1
zahrada	zahrada	k1gFnSc1
v	v	k7c6
Padově	Padova	k1gFnSc6
•	•	k?
Castel	Castela	k1gFnPc2
del	del	k?
Monte	Mont	k1gInSc5
•	•	k?
Cerveteri	Cerveter	k1gFnSc2
a	a	k8xC
Tarquinia	Tarquinium	k1gNnSc2
•	•	k?
Cilento	Cilento	k1gNnSc1
<g/>
,	,	kIx,
Vallo	Vallo	k1gNnSc1
di	di	k?
Diano	Diana	k1gFnSc5
<g/>
,	,	kIx,
Paestum	Paestum	k1gNnSc1
<g/>
,	,	kIx,
Velia	Velia	k1gFnSc1
a	a	k8xC
Certosa	Certosa	k1gFnSc1
di	di	k?
Padula	Padula	k1gFnSc1
•	•	k?
Crespi	Cresp	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
d	d	k?
<g/>
'	'	kIx"
<g/>
Adda	Add	k2eAgFnSc1d1
•	•	k?
Dolomity	Dolomity	k1gInPc4
•	•	k?
Etna	Etna	k1gFnSc1
•	•	k?
Ferrara	Ferrara	k1gFnSc1
•	•	k?
Florencie	Florencie	k1gFnSc2
•	•	k?
Hadriánova	Hadriánův	k2eAgFnSc1d1
vila	vila	k1gFnSc1
•	•	k?
Ivrea	Ivrea	k1gFnSc1
•	•	k?
Janov	Janov	k1gInSc1
<g/>
:	:	kIx,
Le	Le	k1gFnSc1
Strade	Strad	k1gInSc5
Nuove	Nuoev	k1gFnPc4
a	a	k8xC
systém	systém	k1gInSc4
Palazzi	Palazze	k1gFnSc4
dei	dei	k?
Rolli	Rolle	k1gFnSc3
•	•	k?
Kopce	kopec	k1gInSc2
prosecca	prosecca	k6eAd1
Conegliano	Conegliana	k1gFnSc5
a	a	k8xC
Valdobbiadene	Valdobbiaden	k1gInSc5
•	•	k?
královský	královský	k2eAgInSc1d1
palác	palác	k1gInSc1
Caserta	Caserta	k1gFnSc1
•	•	k?
Liparské	Liparský	k2eAgInPc4d1
ostrovy	ostrov	k1gInPc4
•	•	k?
Mantova	Mantova	k1gFnSc1
a	a	k8xC
Sabbioneta	Sabbionet	k2eAgFnSc1d1
•	•	k?
Matera	Matera	k1gFnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
mocenská	mocenský	k2eAgFnSc1d1
střediska	středisko	k1gNnPc1
Langobardů	Langobard	k1gMnPc2
v	v	k7c6
Itálii	Itálie	k1gFnSc6
•	•	k?
katedrála	katedrála	k1gFnSc1
Nanebevzetí	nanebevzetí	k1gNnPc2
Panny	Panna	k1gFnSc2
Marie	Marie	k1gFnSc1
<g/>
,	,	kIx,
brána	brána	k1gFnSc1
Torre	torr	k1gInSc5
Civica	Civica	k1gFnSc1
a	a	k8xC
náměstí	náměstí	k1gNnSc1
Piazza	Piazz	k1gMnSc2
Grande	grand	k1gMnSc5
v	v	k7c6
Modeně	Modena	k1gFnSc6
•	•	k?
Monte	Mont	k1gMnSc5
San	San	k1gMnSc5
Giorgio	Giorgia	k1gMnSc5
•	•	k?
Neapol	Neapol	k1gFnSc1
•	•	k?
Palermo	Palermo	k1gNnSc1
a	a	k8xC
katedrály	katedrála	k1gFnPc1
v	v	k7c6
Cefalú	Cefalú	k1gFnSc6
a	a	k8xC
v	v	k7c6
Monreale	Monreala	k1gFnSc6
•	•	k?
Piazza	Piazza	k1gFnSc1
del	del	k?
Duomo	Duoma	k1gFnSc5
<g/>
,	,	kIx,
Pisa	Pisa	k1gFnSc1
•	•	k?
Pienza	Pienza	k1gFnSc1
•	•	k?
Pompeje	Pompeje	k1gInPc1
<g/>
,	,	kIx,
Herculaneum	Herculaneum	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
a	a	k8xC
Torre	torr	k1gInSc5
Annunziata	Annunziat	k2eAgFnSc1d1
•	•	k?
Portovenere	Portovener	k1gMnSc5
<g/>
,	,	kIx,
Cinque	Cinquus	k1gMnSc5
Terre	Terr	k1gMnSc5
a	a	k8xC
ostrovy	ostrov	k1gInPc1
Palmaria	Palmarium	k1gNnSc2
<g/>
,	,	kIx,
Tino	Tina	k1gFnSc5
a	a	k8xC
Tinetto	Tinett	k2eAgNnSc4d1
•	•	k?
prehistorická	prehistorický	k2eAgNnPc1d1
kůlová	kůlový	k2eAgNnPc1d1
obydlí	obydlí	k1gNnPc1
v	v	k7c6
Alpách	Alpy	k1gFnPc6
•	•	k?
Původní	původní	k2eAgInPc4d1
bukové	bukový	k2eAgInPc4d1
lesy	les	k1gInPc4
Karpat	Karpaty	k1gInPc2
a	a	k8xC
dalších	další	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
Evropy	Evropa	k1gFnSc2
•	•	k?
Ravenna	Ravenna	k1gFnSc1
•	•	k?
rezidence	rezidence	k1gFnSc2
královského	královský	k2eAgInSc2d1
rodu	rod	k1gInSc2
Savojských	savojský	k2eAgInPc2d1
•	•	k?
Rhétská	Rhétský	k2eAgFnSc1d1
dráha	dráha	k1gFnSc1
•	•	k?
Řím	Řím	k1gInSc1
•	•	k?
Sacri	Sacr	k1gFnSc2
Monti	Monť	k1gFnSc2
•	•	k?
San	San	k1gMnSc1
Gimignano	Gimignana	k1gFnSc5
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Santa	Santa	k1gFnSc1
Maria	Maria	k1gFnSc1
delle	delle	k1gFnSc1
Grazie	Grazie	k1gFnSc1
•	•	k?
Siena	Siena	k1gFnSc1
•	•	k?
skalní	skalní	k2eAgFnPc4d1
kresby	kresba	k1gFnPc4
ve	v	k7c4
Val	val	k1gInSc4
Camonice	Camonice	k1gFnSc2
•	•	k?
Su	Su	k?
Nuraxi	Nuraxe	k1gFnSc4
di	di	k?
Barumini	Barumin	k2eAgMnPc1d1
•	•	k?
Syrakusy	Syrakusy	k1gFnPc4
a	a	k8xC
Pantalica	Pantalica	k1gMnSc1
•	•	k?
Urbino	Urbino	k1gNnSc4
•	•	k?
Val	val	k1gInSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Orcia	Orcia	k1gFnSc1
•	•	k?
Val	val	k1gInSc4
di	di	k?
Noto	nota	k1gFnSc5
•	•	k?
Verona	Verona	k1gFnSc1
•	•	k?
Vicenza	Vicenz	k1gMnSc2
a	a	k8xC
Palladiovy	Palladiovy	k?
vily	vila	k1gFnSc2
v	v	k7c6
Benátsku	Benátsko	k1gNnSc6
•	•	k?
Villa	Villa	k1gMnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
Este	Est	k1gMnSc5
•	•	k?
Villa	Vill	k1gMnSc4
Romana	Roman	k1gMnSc4
del	del	k?
Casale	Casala	k1gFnSc6
•	•	k?
vily	vila	k1gFnSc2
a	a	k8xC
zahrady	zahrada	k1gFnSc2
Medicejských	Medicejský	k2eAgMnPc2d1
v	v	k7c6
Toskánsku	Toskánsko	k1gNnSc6
•	•	k?
vinařská	vinařský	k2eAgFnSc1d1
krajina	krajina	k1gFnSc1
Piemontu	Piemont	k1gInSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Geografie	geografie	k1gFnSc1
|	|	kIx~
Itálie	Itálie	k1gFnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
ge	ge	k?
<g/>
305372	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
4084764-0	4084764-0	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85045454	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
18144647638815926529	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85045454	#num#	k4
</s>
