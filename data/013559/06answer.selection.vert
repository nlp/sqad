<s>
Etna	Etna	k1gFnSc1
(	(	kIx(
<g/>
starořecky	starořecky	k6eAd1
Α	Α	k?
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Aitné	Aitné	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
latině	latina	k1gFnSc6
Aetna	Aetna	k1gFnSc1
<g/>
,	,	kIx,
také	také	k9
známá	známý	k2eAgNnPc4d1
jako	jako	k8xS,k8xC
Mungibeddu	Mungibedd	k1gInSc2
(	(	kIx(
<g/>
krásná	krásný	k2eAgFnSc1d1
hora	hora	k1gFnSc1
<g/>
)	)	kIx)
v	v	k7c6
sicilštině	sicilština	k1gFnSc6
a	a	k8xC
Mongibello	Mongibello	k1gNnSc1
v	v	k7c6
italštině	italština	k1gFnSc6
(	(	kIx(
<g/>
z	z	k7c2
latinského	latinský	k2eAgInSc2d1
mons	mons	k6eAd1
a	a	k8xC
arabského	arabský	k2eAgInSc2d1
gibel	gibel	k1gFnSc1
<g/>
,	,	kIx,
oboje	oboj	k1gFnPc4
znamená	znamenat	k5eAaImIp3nS
hora	hora	k1gFnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
nejvyšší	vysoký	k2eAgFnSc1d3
činná	činný	k2eAgFnSc1d1
sopka	sopka	k1gFnSc1
a	a	k8xC
druhá	druhý	k4xOgFnSc1
nejmohutnější	mohutný	k2eAgFnSc1d3
sopka	sopka	k1gFnSc1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
<g/>
.	.	kIx.
</s>