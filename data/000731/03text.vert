<s>
San	San	k?	San
Marino	Marina	k1gFnSc5	Marina
(	(	kIx(	(
<g/>
oficiálně	oficiálně	k6eAd1	oficiálně
<g/>
:	:	kIx,	:
Republika	republika	k1gFnSc1	republika
San	San	k1gFnSc2	San
Marino	Marina	k1gFnSc5	Marina
<g/>
,	,	kIx,	,
italsky	italsky	k6eAd1	italsky
zní	znět	k5eAaImIp3nS	znět
plný	plný	k2eAgInSc1d1	plný
název	název	k1gInSc1	název
Serenissima	serenissimus	k1gMnSc2	serenissimus
Repubblica	Repubblicus	k1gMnSc2	Repubblicus
di	di	k?	di
San	San	k1gMnSc2	San
Marino	Marina	k1gFnSc5	Marina
-	-	kIx~	-
Nejvznešenější	vznešený	k2eAgFnSc1d3	nejvznešenější
republika	republika	k1gFnSc1	republika
San	San	k1gFnSc2	San
Marino	Marina	k1gFnSc5	Marina
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgInSc1d1	malý
jihoevropský	jihoevropský	k2eAgInSc1d1	jihoevropský
stát	stát	k1gInSc1	stát
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
počtu	počet	k1gInSc2	počet
obyvatel	obyvatel	k1gMnSc1	obyvatel
2	[number]	k4	2
<g/>
.	.	kIx.	.
nejmenší	malý	k2eAgInSc4d3	nejmenší
evropský	evropský	k2eAgInSc4d1	evropský
a	a	k8xC	a
5	[number]	k4	5
<g/>
.	.	kIx.	.
nejmenší	malý	k2eAgMnSc1d3	nejmenší
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
)	)	kIx)	)
ze	z	k7c2	z
všech	všecek	k3xTgFnPc2	všecek
stran	strana	k1gFnPc2	strana
obklopený	obklopený	k2eAgInSc4d1	obklopený
Itálií	Itálie	k1gFnSc7	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Leží	ležet	k5eAaImIp3nS	ležet
v	v	k7c6	v
italském	italský	k2eAgNnSc6d1	italské
vnitrozemí	vnitrozemí	k1gNnSc6	vnitrozemí
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
turistického	turistický	k2eAgNnSc2d1	turistické
centra	centrum	k1gNnSc2	centrum
Rimini	Rimin	k1gMnPc1	Rimin
<g/>
.	.	kIx.	.
</s>
<s>
Rozkládá	rozkládat	k5eAaImIp3nS	rozkládat
se	se	k3xPyFc4	se
na	na	k7c4	na
61	[number]	k4	61
km	km	kA	km
2	[number]	k4	2
a	a	k8xC	a
žije	žít	k5eAaImIp3nS	žít
zde	zde	k6eAd1	zde
přibližně	přibližně	k6eAd1	přibližně
32	[number]	k4	32
tisíc	tisíc	k4xCgInPc2	tisíc
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
je	být	k5eAaImIp3nS	být
San	San	k1gFnSc4	San
Marino	Marina	k1gFnSc5	Marina
a	a	k8xC	a
největší	veliký	k2eAgNnSc4d3	veliký
město	město	k1gNnSc4	město
je	být	k5eAaImIp3nS	být
Dogana	Dogana	k1gFnSc1	Dogana
<g/>
.	.	kIx.	.
</s>
<s>
Hospodářství	hospodářství	k1gNnSc1	hospodářství
země	zem	k1gFnSc2	zem
závisí	záviset	k5eAaImIp3nS	záviset
zejména	zejména	k9	zejména
na	na	k7c6	na
průmyslu	průmysl	k1gInSc6	průmysl
<g/>
,	,	kIx,	,
službách	služba	k1gFnPc6	služba
a	a	k8xC	a
cestovním	cestovní	k2eAgInSc6d1	cestovní
ruchu	ruch	k1gInSc6	ruch
<g/>
.	.	kIx.	.
</s>
<s>
San	San	k?	San
Marino	Marina	k1gFnSc5	Marina
je	být	k5eAaImIp3nS	být
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
nejbohatších	bohatý	k2eAgInPc2d3	nejbohatší
států	stát	k1gInPc2	stát
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
vysoce	vysoce	k6eAd1	vysoce
stabilní	stabilní	k2eAgFnSc4d1	stabilní
ekonomiku	ekonomika	k1gFnSc4	ekonomika
<g/>
,	,	kIx,	,
s	s	k7c7	s
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejnižších	nízký	k2eAgFnPc2d3	nejnižší
nezaměstnaností	nezaměstnanost	k1gFnPc2	nezaměstnanost
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Platidlem	platidlo	k1gNnSc7	platidlo
je	být	k5eAaImIp3nS	být
euro	euro	k1gNnSc1	euro
<g/>
.	.	kIx.	.
</s>
<s>
San	San	k?	San
Marino	Marina	k1gFnSc5	Marina
je	on	k3xPp3gMnPc4	on
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
nejstarší	starý	k2eAgInSc1d3	nejstarší
stále	stále	k6eAd1	stále
existující	existující	k2eAgFnSc7d1	existující
republikou	republika	k1gFnSc7	republika
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Bylo	být	k5eAaImAgNnS	být
údajně	údajně	k6eAd1	údajně
založeno	založit	k5eAaPmNgNnS	založit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
301	[number]	k4	301
zkušeným	zkušený	k2eAgMnPc3d1	zkušený
budovatelem	budovatel	k1gMnSc7	budovatel
jménem	jméno	k1gNnSc7	jméno
Marinus	Marinus	k1gInSc4	Marinus
(	(	kIx(	(
<g/>
známým	známý	k1gMnSc7	známý
jako	jako	k8xC	jako
Svatý	svatý	k2eAgInSc1d1	svatý
Marinus	Marinus	k1gInSc1	Marinus
<g/>
)	)	kIx)	)
<g/>
;	;	kIx,	;
oficiálně	oficiálně	k6eAd1	oficiálně
se	se	k3xPyFc4	se
jako	jako	k9	jako
den	den	k1gInSc1	den
založení	založení	k1gNnSc2	založení
udává	udávat	k5eAaImIp3nS	udávat
3	[number]	k4	3
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
<g/>
.	.	kIx.	.
</s>
<s>
Stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
legální	legální	k2eAgFnSc7d1	legální
součástí	součást	k1gFnSc7	součást
Římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
psaná	psaný	k2eAgFnSc1d1	psaná
ústava	ústava	k1gFnSc1	ústava
byla	být	k5eAaImAgFnS	být
přijata	přijat	k2eAgFnSc1d1	přijata
8	[number]	k4	8
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
1600	[number]	k4	1600
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
nejstarší	starý	k2eAgFnSc4d3	nejstarší
stále	stále	k6eAd1	stále
platnou	platný	k2eAgFnSc4d1	platná
ústavu	ústava	k1gFnSc4	ústava
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
malým	malý	k2eAgInSc7d1	malý
národem	národ	k1gInSc7	národ
navázala	navázat	k5eAaPmAgFnS	navázat
oficiální	oficiální	k2eAgInPc4d1	oficiální
styky	styk	k1gInPc4	styk
nejdříve	dříve	k6eAd3	dříve
Francie	Francie	k1gFnSc2	Francie
za	za	k7c2	za
vlády	vláda	k1gFnSc2	vláda
Napoleona	Napoleon	k1gMnSc2	Napoleon
roku	rok	k1gInSc2	rok
1797	[number]	k4	1797
<g/>
,	,	kIx,	,
další	další	k2eAgInPc1d1	další
evropské	evropský	k2eAgInPc1d1	evropský
národy	národ	k1gInPc1	národ
se	se	k3xPyFc4	se
připojily	připojit	k5eAaPmAgInP	připojit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1815	[number]	k4	1815
na	na	k7c6	na
Vídeňském	vídeňský	k2eAgInSc6d1	vídeňský
kongresu	kongres	k1gInSc6	kongres
<g/>
.	.	kIx.	.
</s>
<s>
San	San	k?	San
Marino	Marina	k1gFnSc5	Marina
sice	sice	k8xC	sice
bylo	být	k5eAaImAgNnS	být
svrchovaným	svrchovaný	k2eAgInSc7d1	svrchovaný
státem	stát	k1gInSc7	stát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
se	s	k7c7	s
silnou	silný	k2eAgFnSc7d1	silná
závislostí	závislost	k1gFnSc7	závislost
na	na	k7c6	na
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
bylo	být	k5eAaImAgNnS	být
úplně	úplně	k6eAd1	úplně
obklopeno	obklopit	k5eAaPmNgNnS	obklopit
od	od	k7c2	od
celkového	celkový	k2eAgNnSc2d1	celkové
sjednocení	sjednocení	k1gNnSc2	sjednocení
v	v	k7c6	v
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Giuseppe	Giuseppat	k5eAaPmIp3nS	Giuseppat
Garibaldi	Garibald	k1gMnPc1	Garibald
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
začal	začít	k5eAaPmAgInS	začít
sjednocovat	sjednocovat	k5eAaImF	sjednocovat
Itálii	Itálie	k1gFnSc4	Itálie
kolem	kolem	k7c2	kolem
roku	rok	k1gInSc2	rok
1860	[number]	k4	1860
<g/>
,	,	kIx,	,
musel	muset	k5eAaImAgInS	muset
na	na	k7c4	na
čas	čas	k1gInSc4	čas
najít	najít	k5eAaPmF	najít
útočiště	útočiště	k1gNnSc4	útočiště
v	v	k7c6	v
San	San	k1gFnSc6	San
Marinu	Marina	k1gFnSc4	Marina
před	před	k7c7	před
svými	svůj	k3xOyFgMnPc7	svůj
nepřáteli	nepřítel	k1gMnPc7	nepřítel
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
získal	získat	k5eAaPmAgMnS	získat
významnou	významný	k2eAgFnSc4d1	významná
pomoc	pomoc	k1gFnSc4	pomoc
<g/>
,	,	kIx,	,
peníze	peníz	k1gInPc4	peníz
a	a	k8xC	a
dodávky	dodávka	k1gFnPc4	dodávka
od	od	k7c2	od
občanů	občan	k1gMnPc2	občan
a	a	k8xC	a
vůdců	vůdce	k1gMnPc2	vůdce
San	San	k1gFnSc1	San
Marina	Marina	k1gFnSc1	Marina
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
pokračování	pokračování	k1gNnSc1	pokračování
jeho	jeho	k3xOp3gFnSc2	jeho
kampaně	kampaň	k1gFnSc2	kampaň
za	za	k7c4	za
sjednocení	sjednocení	k1gNnSc4	sjednocení
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
oplátku	oplátka	k1gFnSc4	oplátka
údajně	údajně	k6eAd1	údajně
Garibaldi	Garibald	k1gMnPc1	Garibald
garantoval	garantovat	k5eAaBmAgMnS	garantovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
San	San	k1gFnSc1	San
Marino	Marina	k1gFnSc5	Marina
bude	být	k5eAaImBp3nS	být
vždy	vždy	k6eAd1	vždy
nezávislým	závislý	k2eNgInSc7d1	nezávislý
svrchovaným	svrchovaný	k2eAgInSc7d1	svrchovaný
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
generální	generální	k2eAgFnSc1d1	generální
rada	rada	k1gFnSc1	rada
(	(	kIx(	(
<g/>
šedesátičlenná	šedesátičlenný	k2eAgFnSc1d1	šedesátičlenná
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
volena	volit	k5eAaImNgFnS	volit
všelidovým	všelidový	k2eAgNnSc7d1	všelidové
hlasováním	hlasování	k1gNnSc7	hlasování
každých	každý	k3xTgNnPc2	každý
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
Volí	volit	k5eAaImIp3nS	volit
ze	z	k7c2	z
svého	svůj	k3xOyFgInSc2	svůj
středu	střed	k1gInSc2	střed
dva	dva	k4xCgInPc4	dva
kapitány	kapitán	k1gMnPc4	kapitán
regenty	regens	k1gMnPc7	regens
<g/>
.	.	kIx.	.
</s>
<s>
Volí	volit	k5eAaImIp3nS	volit
také	také	k9	také
Radu	rada	k1gFnSc4	rada
dvanácti	dvanáct	k4xCc2	dvanáct
<g/>
.	.	kIx.	.
</s>
<s>
Kapitáni-regenti	Kapitániegent	k1gMnPc1	Kapitáni-regent
(	(	kIx(	(
<g/>
Capitani	Capitan	k1gMnPc1	Capitan
Reggenti	Reggent	k1gMnPc1	Reggent
<g/>
)	)	kIx)	)
vykonávají	vykonávat	k5eAaImIp3nP	vykonávat
funkci	funkce	k1gFnSc4	funkce
hlavy	hlava	k1gFnSc2	hlava
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
voleni	volit	k5eAaImNgMnP	volit
každých	každý	k3xTgInPc2	každý
šest	šest	k4xCc4	šest
měsíců	měsíc	k1gInPc2	měsíc
-	-	kIx~	-
1	[number]	k4	1
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
<g/>
.	.	kIx.	.
</s>
<s>
Společně	společně	k6eAd1	společně
řídí	řídit	k5eAaImIp3nS	řídit
Velkou	velký	k2eAgFnSc4d1	velká
generální	generální	k2eAgFnSc4d1	generální
radu	rada	k1gFnSc4	rada
(	(	kIx(	(
<g/>
Consiglio	Consiglia	k1gMnSc5	Consiglia
Grande	grand	k1gMnSc5	grand
e	e	k0	e
Generale	General	k1gMnSc5	General
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Státní	státní	k2eAgInSc1d1	státní
kongres	kongres	k1gInSc4	kongres
a	a	k8xC	a
Radu	rada	k1gFnSc4	rada
dvanácti	dvanáct	k4xCc2	dvanáct
(	(	kIx(	(
<g/>
Consiglio	Consiglio	k1gNnSc1	Consiglio
dei	dei	k?	dei
XII	XII	kA	XII
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
vůči	vůči	k7c3	vůči
sobě	se	k3xPyFc3	se
právo	právo	k1gNnSc4	právo
veta	veto	k1gNnSc2	veto
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgInSc1d1	státní
kongres	kongres	k1gInSc1	kongres
zastává	zastávat	k5eAaImIp3nS	zastávat
výkonnou	výkonný	k2eAgFnSc4d1	výkonná
moc	moc	k1gFnSc4	moc
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
vládu	vláda	k1gFnSc4	vláda
deseti	deset	k4xCc2	deset
ministrů	ministr	k1gMnPc2	ministr
-	-	kIx~	-
tří	tři	k4xCgInPc2	tři
tajemníků	tajemník	k1gInPc2	tajemník
a	a	k8xC	a
sedmi	sedm	k4xCc2	sedm
poslanců	poslanec	k1gMnPc2	poslanec
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
dvanácti	dvanáct	k4xCc2	dvanáct
zastává	zastávat	k5eAaImIp3nS	zastávat
funkci	funkce	k1gFnSc4	funkce
nejvyššího	vysoký	k2eAgInSc2d3	Nejvyšší
soudu	soud	k1gInSc2	soud
<g/>
.	.	kIx.	.
</s>
<s>
Soudci	soudce	k1gMnPc1	soudce
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
nestrannosti	nestrannost	k1gFnSc2	nestrannost
nemohou	moct	k5eNaImIp3nP	moct
být	být	k5eAaImF	být
občany	občan	k1gMnPc7	občan
San	San	k1gFnSc1	San
Marina	Marina	k1gFnSc1	Marina
(	(	kIx(	(
<g/>
výjimku	výjimka	k1gFnSc4	výjimka
tvoří	tvořit	k5eAaImIp3nP	tvořit
pouze	pouze	k6eAd1	pouze
smírčí	smírčí	k2eAgMnPc4d1	smírčí
soudce	soudce	k1gMnPc4	soudce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Funkční	funkční	k2eAgNnSc1d1	funkční
období	období	k1gNnSc1	období
rady	rada	k1gFnSc2	rada
trvá	trvat	k5eAaImIp3nS	trvat
5	[number]	k4	5
let	léto	k1gNnPc2	léto
<g/>
.	.	kIx.	.
</s>
<s>
San	San	k?	San
Marino	Marina	k1gFnSc5	Marina
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
devíti	devět	k4xCc2	devět
samosprávných	samosprávný	k2eAgFnPc2d1	samosprávná
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
nazývají	nazývat	k5eAaImIp3nP	nazývat
castelli	castell	k1gMnPc1	castell
<g/>
.	.	kIx.	.
</s>
<s>
San	San	k?	San
Marino	Marina	k1gFnSc5	Marina
<g/>
,	,	kIx,	,
tvoří	tvořit	k5eAaImIp3nP	tvořit
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
částí	část	k1gFnPc2	část
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
zároveň	zároveň	k6eAd1	zároveň
hlavním	hlavní	k2eAgNnSc7d1	hlavní
městem	město	k1gNnSc7	město
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
A	a	k9	a
dále	daleko	k6eAd2	daleko
osm	osm	k4xCc1	osm
menších	malý	k2eAgFnPc2d2	menší
samosprávných	samosprávný	k2eAgFnPc2d1	samosprávná
jednotek	jednotka	k1gFnPc2	jednotka
<g/>
:	:	kIx,	:
Acquaviva	Acquaviva	k1gFnSc1	Acquaviva
<g/>
,	,	kIx,	,
Borgo	Borga	k1gMnSc5	Borga
Maggiore	Maggior	k1gMnSc5	Maggior
<g/>
,	,	kIx,	,
Chiesanuova	Chiesanuovo	k1gNnPc1	Chiesanuovo
<g/>
,	,	kIx,	,
Domagnano	Domagnana	k1gFnSc5	Domagnana
<g/>
,	,	kIx,	,
Faetano	Faetana	k1gFnSc5	Faetana
<g/>
,	,	kIx,	,
Fiorentino	Fiorentina	k1gFnSc5	Fiorentina
<g/>
,	,	kIx,	,
Montegiardino	Montegiardina	k1gFnSc5	Montegiardina
<g/>
,	,	kIx,	,
Serravalle	Serravalle	k1gFnSc5	Serravalle
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgNnSc7d3	veliký
městem	město	k1gNnSc7	město
je	být	k5eAaImIp3nS	být
Dagona	Dagona	k1gFnSc1	Dagona
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
patří	patřit	k5eAaImIp3nS	patřit
do	do	k7c2	do
samosprávní	samosprávní	k2eAgFnSc2d1	samosprávní
části	část	k1gFnSc2	část
Serravalle	Serravalle	k1gFnSc2	Serravalle
<g/>
.	.	kIx.	.
</s>
<s>
San	San	k?	San
Marino	Marina	k1gFnSc5	Marina
je	být	k5eAaImIp3nS	být
uzavřená	uzavřený	k2eAgFnSc1d1	uzavřená
enkláva	enkláva	k1gFnSc1	enkláva
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
na	na	k7c6	na
hranici	hranice	k1gFnSc6	hranice
mezi	mezi	k7c7	mezi
italskými	italský	k2eAgInPc7d1	italský
regiony	region	k1gInPc7	region
Emilia-Romagna	Emilia-Romagno	k1gNnSc2	Emilia-Romagno
a	a	k8xC	a
Marche	Marche	k1gNnSc2	Marche
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
topografii	topografie	k1gFnSc6	topografie
dominuje	dominovat	k5eAaImIp3nS	dominovat
Apeninské	apeninský	k2eAgNnSc4d1	Apeninské
horské	horský	k2eAgNnSc4d1	horské
pásmo	pásmo	k1gNnSc4	pásmo
<g/>
,	,	kIx,	,
a	a	k8xC	a
proto	proto	k8xC	proto
má	mít	k5eAaImIp3nS	mít
poměrně	poměrně	k6eAd1	poměrně
drsný	drsný	k2eAgInSc4d1	drsný
terén	terén	k1gInSc4	terén
<g/>
.	.	kIx.	.
</s>
<s>
Nejvyšší	vysoký	k2eAgInSc1d3	Nejvyšší
bodem	bod	k1gInSc7	bod
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
hora	hora	k1gFnSc1	hora
-	-	kIx~	-
Monte	Mont	k1gInSc5	Mont
Titano	Titana	k1gFnSc5	Titana
(	(	kIx(	(
<g/>
live	live	k1gNnPc1	live
view	view	k?	view
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jejíž	jejíž	k3xOyRp3gFnSc1	jejíž
nadmořská	nadmořský	k2eAgFnSc1d1	nadmořská
výška	výška	k1gFnSc1	výška
je	být	k5eAaImIp3nS	být
755	[number]	k4	755
metrů	metr	k1gInPc2	metr
<g/>
.	.	kIx.	.
</s>
<s>
Součástí	součást	k1gFnSc7	součást
enklávy	enkláva	k1gFnSc2	enkláva
není	být	k5eNaImIp3nS	být
ani	ani	k9	ani
jedna	jeden	k4xCgFnSc1	jeden
větší	veliký	k2eAgFnSc1d2	veliký
vodní	vodní	k2eAgFnSc1d1	vodní
plocha	plocha	k1gFnSc1	plocha
o	o	k7c4	o
nějaké	nějaký	k3yIgFnPc4	nějaký
významné	významný	k2eAgFnPc4d1	významná
velikosti	velikost	k1gFnPc4	velikost
<g/>
.	.	kIx.	.
</s>
<s>
San	San	k?	San
Marino	Marina	k1gFnSc5	Marina
je	být	k5eAaImIp3nS	být
třetí	třetí	k4xOgFnPc4	třetí
nejmenší	malý	k2eAgFnPc4d3	nejmenší
země	zem	k1gFnPc4	zem
v	v	k7c6	v
Evropě	Evropa	k1gFnSc6	Evropa
<g/>
,	,	kIx,	,
menší	malý	k2eAgNnPc1d2	menší
jsou	být	k5eAaImIp3nP	být
jen	jen	k9	jen
Vatikán	Vatikán	k1gInSc4	Vatikán
a	a	k8xC	a
Monako	Monako	k1gNnSc4	Monako
<g/>
.	.	kIx.	.
</s>
<s>
Panuje	panovat	k5eAaImIp3nS	panovat
zde	zde	k6eAd1	zde
středomořské	středomořský	k2eAgNnSc1d1	středomořské
podnebí	podnebí	k1gNnSc1	podnebí
s	s	k7c7	s
kontinentálními	kontinentální	k2eAgInPc7d1	kontinentální
vlivy	vliv	k1gInPc7	vliv
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
jsou	být	k5eAaImIp3nP	být
horká	horký	k2eAgNnPc1d1	horké
léta	léto	k1gNnPc1	léto
a	a	k8xC	a
chladné	chladný	k2eAgFnPc1d1	chladná
zimy	zima	k1gFnPc1	zima
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc4d1	typické
pro	pro	k7c4	pro
vnitrozemské	vnitrozemský	k2eAgFnPc4d1	vnitrozemská
oblasti	oblast	k1gFnPc4	oblast
Apeninského	apeninský	k2eAgInSc2d1	apeninský
poloostrova	poloostrov	k1gInSc2	poloostrov
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Obyvatelstvo	obyvatelstvo	k1gNnSc4	obyvatelstvo
San	San	k1gFnSc2	San
Marina	Marina	k1gFnSc1	Marina
<g/>
.	.	kIx.	.
</s>
<s>
San	San	k?	San
Marino	Marina	k1gFnSc5	Marina
má	mít	k5eAaImIp3nS	mít
přibližně	přibližně	k6eAd1	přibližně
33	[number]	k4	33
000	[number]	k4	000
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
4	[number]	k4	4
800	[number]	k4	800
obyvatel	obyvatel	k1gMnPc2	obyvatel
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
jiné	jiný	k2eAgFnSc2d1	jiná
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Největší	veliký	k2eAgFnSc4d3	veliký
část	část	k1gFnSc4	část
tvoří	tvořit	k5eAaImIp3nP	tvořit
Italové	Ital	k1gMnPc1	Ital
<g/>
.	.	kIx.	.
</s>
<s>
Občané	občan	k1gMnPc1	občan
San	San	k1gFnSc1	San
Marina	Marina	k1gFnSc1	Marina
mají	mít	k5eAaImIp3nP	mít
jednu	jeden	k4xCgFnSc4	jeden
z	z	k7c2	z
nejvyšších	vysoký	k2eAgFnPc2d3	nejvyšší
průměrných	průměrný	k2eAgFnPc2d1	průměrná
délek	délka	k1gFnPc2	délka
života	život	k1gInSc2	život
na	na	k7c6	na
světě	svět	k1gInSc6	svět
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
tvoří	tvořit	k5eAaImIp3nS	tvořit
z	z	k7c2	z
více	hodně	k6eAd2	hodně
jak	jak	k8xS	jak
80	[number]	k4	80
<g/>
%	%	kIx~	%
Sanmariňané	Sanmariňan	k1gMnPc1	Sanmariňan
(	(	kIx(	(
<g/>
italsky	italsky	k6eAd1	italsky
sammarinesi	sammarinese	k1gFnSc3	sammarinese
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Dalších	další	k2eAgNnPc2d1	další
asi	asi	k9	asi
13	[number]	k4	13
000	[number]	k4	000
státních	státní	k2eAgMnPc2d1	státní
příslušníků	příslušník	k1gMnPc2	příslušník
tohoto	tento	k3xDgInSc2	tento
národa	národ	k1gInSc2	národ
žije	žít	k5eAaImIp3nS	žít
mimo	mimo	k7c4	mimo
území	území	k1gNnSc4	území
republiky	republika	k1gFnSc2	republika
<g/>
,	,	kIx,	,
neztrácejí	ztrácet	k5eNaImIp3nP	ztrácet
však	však	k9	však
volební	volební	k2eAgNnSc4d1	volební
právo	právo	k1gNnSc4	právo
<g/>
.	.	kIx.	.
</s>
<s>
Státní	státní	k2eAgNnSc1d1	státní
občanství	občanství	k1gNnSc1	občanství
se	se	k3xPyFc4	se
dědí	dědit	k5eAaImIp3nS	dědit
v	v	k7c6	v
mužské	mužský	k2eAgFnSc6d1	mužská
linii	linie	k1gFnSc6	linie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
nízká	nízký	k2eAgFnSc1d1	nízká
nezaměstnanost	nezaměstnanost	k1gFnSc1	nezaměstnanost
-	-	kIx~	-
méně	málo	k6eAd2	málo
než	než	k8xS	než
3	[number]	k4	3
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Úředním	úřední	k2eAgInSc7d1	úřední
jazykem	jazyk	k1gInSc7	jazyk
je	být	k5eAaImIp3nS	být
italština	italština	k1gFnSc1	italština
<g/>
.	.	kIx.	.
</s>
<s>
Turistů	turist	k1gMnPc2	turist
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
území	území	k1gNnSc6	území
San	San	k1gFnSc1	San
Marina	Marina	k1gFnSc1	Marina
téměř	téměř	k6eAd1	téměř
19	[number]	k4	19
<g/>
×	×	k?	×
víc	hodně	k6eAd2	hodně
než	než	k8xS	než
místních	místní	k2eAgMnPc2d1	místní
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
San	San	k?	San
Marino	Marina	k1gFnSc5	Marina
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
katolický	katolický	k2eAgInSc4d1	katolický
stát	stát	k1gInSc4	stát
<g/>
,	,	kIx,	,
tuto	tento	k3xDgFnSc4	tento
víru	víra	k1gFnSc4	víra
vyznává	vyznávat	k5eAaImIp3nS	vyznávat
více	hodně	k6eAd2	hodně
než	než	k8xS	než
97	[number]	k4	97
<g/>
%	%	kIx~	%
populace	populace	k1gFnSc2	populace
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgNnSc4d1	další
zastoupení	zastoupení	k1gNnSc4	zastoupení
má	mít	k5eAaImIp3nS	mít
např.	např.	kA	např.
protestantství	protestantství	k1gNnSc3	protestantství
1,1	[number]	k4	1,1
%	%	kIx~	%
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
vyznání	vyznání	k1gNnPc2	vyznání
0,7	[number]	k4	0,7
<g/>
%	%	kIx~	%
a	a	k8xC	a
Židé	Žid	k1gMnPc1	Žid
0,1	[number]	k4	0,1
<g/>
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
San	San	k1gFnSc6	San
Marinu	Marina	k1gFnSc4	Marina
je	být	k5eAaImIp3nS	být
220	[number]	k4	220
km	km	kA	km
silnic	silnice	k1gFnPc2	silnice
<g/>
.	.	kIx.	.
</s>
<s>
Není	být	k5eNaImIp3nS	být
zde	zde	k6eAd1	zde
veřejné	veřejný	k2eAgNnSc4d1	veřejné
letiště	letiště	k1gNnSc4	letiště
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
malé	malý	k2eAgNnSc1d1	malé
soukromé	soukromý	k2eAgNnSc1d1	soukromé
letiště	letiště	k1gNnSc1	letiště
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
obce	obec	k1gFnSc2	obec
Torraccia	Torraccius	k1gMnSc2	Torraccius
a	a	k8xC	a
heliport	heliport	k1gInSc4	heliport
v	v	k7c6	v
druhém	druhý	k4xOgNnSc6	druhý
největším	veliký	k2eAgNnSc6d3	veliký
městě	město	k1gNnSc6	město
Borgo	Borga	k1gMnSc5	Borga
Maggiore	Maggior	k1gMnSc5	Maggior
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
turistů	turist	k1gMnPc2	turist
přilétá	přilétat	k5eAaImIp3nS	přilétat
na	na	k7c4	na
mezinárodní	mezinárodní	k2eAgNnSc4d1	mezinárodní
letiště	letiště	k1gNnSc4	letiště
Federico	Federico	k6eAd1	Federico
Fellini	Fellin	k2eAgMnPc1d1	Fellin
jižně	jižně	k6eAd1	jižně
od	od	k7c2	od
Rimini	Rimin	k2eAgMnPc1d1	Rimin
a	a	k8xC	a
do	do	k7c2	do
země	zem	k1gFnSc2	zem
přijíždí	přijíždět	k5eAaImIp3nS	přijíždět
autobusem	autobus	k1gInSc7	autobus
<g/>
.	.	kIx.	.
</s>
<s>
Železnice	železnice	k1gFnSc1	železnice
zde	zde	k6eAd1	zde
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
před	před	k7c7	před
2	[number]	k4	2
<g/>
.	.	kIx.	.
světovou	světový	k2eAgFnSc7d1	světová
válkou	válka	k1gFnSc7	válka
vedla	vést	k5eAaImAgFnS	vést
z	z	k7c2	z
obce	obec	k1gFnSc2	obec
Valdragone	Valdragon	k1gInSc5	Valdragon
úzkorozchodná	úzkorozchodný	k2eAgFnSc1d1	úzkorozchodná
trať	trať	k1gFnSc1	trať
do	do	k7c2	do
Rimini	Rimin	k1gMnPc5	Rimin
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
byla	být	k5eAaImAgFnS	být
dána	dát	k5eAaPmNgFnS	dát
do	do	k7c2	do
provozu	provoz	k1gInSc2	provoz
12	[number]	k4	12
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1932	[number]	k4	1932
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
byla	být	k5eAaImAgFnS	být
zničena	zničit	k5eAaPmNgFnS	zničit
během	během	k7c2	během
britského	britský	k2eAgInSc2d1	britský
náletu	nálet	k1gInSc2	nálet
26	[number]	k4	26
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
1944	[number]	k4	1944
<g/>
.	.	kIx.	.
</s>
<s>
Mnoho	mnoho	k4c1	mnoho
pozůstatků	pozůstatek	k1gInPc2	pozůstatek
<g/>
,	,	kIx,	,
jako	jako	k8xS	jako
mosty	most	k1gInPc1	most
<g/>
,	,	kIx,	,
tunely	tunel	k1gInPc1	tunel
a	a	k8xC	a
stanice	stanice	k1gFnPc1	stanice
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
zachovány	zachovat	k5eAaPmNgInP	zachovat
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc1	tři
tunely	tunel	k1gInPc1	tunel
jsou	být	k5eAaImIp3nP	být
i	i	k9	i
osvětleny	osvětlit	k5eAaPmNgFnP	osvětlit
a	a	k8xC	a
slouží	sloužit	k5eAaImIp3nS	sloužit
pro	pro	k7c4	pro
pěší	pěší	k2eAgMnPc4d1	pěší
turisty	turist	k1gMnPc4	turist
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Borgo	Borgo	k6eAd1	Borgo
Maggiore	Maggior	k1gInSc5	Maggior
vede	vést	k5eAaImIp3nS	vést
1,5	[number]	k4	1,5
km	km	kA	km
dlouhá	dlouhý	k2eAgFnSc1d1	dlouhá
lanovka	lanovka	k1gFnSc1	lanovka
na	na	k7c4	na
nejvyšší	vysoký	k2eAgFnSc4d3	nejvyšší
horu	hora	k1gFnSc4	hora
Monte	Mont	k1gInSc5	Mont
Titano	Titana	k1gFnSc5	Titana
(	(	kIx(	(
<g/>
756	[number]	k4	756
m	m	kA	m
n.	n.	k?	n.
<g/>
m.	m.	k?	m.
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Má	mít	k5eAaImIp3nS	mít
dvě	dva	k4xCgFnPc4	dva
kabiny	kabina	k1gFnPc4	kabina
a	a	k8xC	a
jezdí	jezdit	k5eAaImIp3nP	jezdit
každou	každý	k3xTgFnSc4	každý
čtvrthodinu	čtvrthodina	k1gFnSc4	čtvrthodina
po	po	k7c4	po
celý	celý	k2eAgInSc4d1	celý
den	den	k1gInSc4	den
<g/>
.	.	kIx.	.
</s>
<s>
Zemí	zem	k1gFnSc7	zem
protékají	protékat	k5eAaImIp3nP	protékat
dvě	dva	k4xCgFnPc1	dva
řeky	řeka	k1gFnPc1	řeka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
není	být	k5eNaImIp3nS	být
zde	zde	k6eAd1	zde
žádná	žádný	k3yNgFnSc1	žádný
vodní	vodní	k2eAgFnSc1d1	vodní
doprava	doprava	k1gFnSc1	doprava
či	či	k8xC	či
přístav	přístav	k1gInSc1	přístav
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
není	být	k5eNaImIp3nS	být
San	San	k1gFnSc1	San
Marino	Marina	k1gFnSc5	Marina
oficiálním	oficiální	k2eAgMnSc7d1	oficiální
členem	člen	k1gMnSc7	člen
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
jako	jako	k9	jako
měnu	měna	k1gFnSc4	měna
euro	euro	k1gNnSc1	euro
<g/>
.	.	kIx.	.
</s>
<s>
Turistika	turistika	k1gFnSc1	turistika
tvoří	tvořit	k5eAaImIp3nS	tvořit
v	v	k7c4	v
San	San	k1gFnSc4	San
Marinu	Marina	k1gFnSc4	Marina
62,24	[number]	k4	62,24
<g/>
%	%	kIx~	%
HDP	HDP	kA	HDP
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1997	[number]	k4	1997
navštívilo	navštívit	k5eAaPmAgNnS	navštívit
San	San	k1gMnSc1	San
Marino	Marina	k1gFnSc5	Marina
více	hodně	k6eAd2	hodně
než	než	k8xS	než
3,3	[number]	k4	3,3
miliony	milion	k4xCgInPc1	milion
turistů	turist	k1gMnPc2	turist
<g/>
.	.	kIx.	.
</s>
<s>
Zemědělství	zemědělství	k1gNnSc1	zemědělství
má	mít	k5eAaImIp3nS	mít
jen	jen	k9	jen
nepatrný	patrný	k2eNgInSc4d1	patrný
význam	význam	k1gInSc4	význam
<g/>
.	.	kIx.	.
</s>
<s>
Pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
se	se	k3xPyFc4	se
obilí	obilí	k1gNnSc1	obilí
<g/>
,	,	kIx,	,
vinná	vinný	k2eAgFnSc1d1	vinná
réva	réva	k1gFnSc1	réva
a	a	k8xC	a
olivy	oliva	k1gFnPc1	oliva
<g/>
.	.	kIx.	.
</s>
<s>
Sanmarinské	Sanmarinský	k2eAgFnPc1d1	Sanmarinská
poštovní	poštovní	k2eAgFnPc1d1	poštovní
známky	známka	k1gFnPc1	známka
jsou	být	k5eAaImIp3nP	být
velmi	velmi	k6eAd1	velmi
hodnotné	hodnotný	k2eAgFnPc1d1	hodnotná
a	a	k8xC	a
mají	mít	k5eAaImIp3nP	mít
pro	pro	k7c4	pro
sběratele	sběratel	k1gMnSc4	sběratel
velkou	velký	k2eAgFnSc4d1	velká
cenu	cena	k1gFnSc4	cena
<g/>
.	.	kIx.	.
</s>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
San	San	k1gFnSc2	San
Marino	Marina	k1gFnSc5	Marina
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Galerie	galerie	k1gFnSc1	galerie
San	San	k1gFnSc1	San
Marino	Marina	k1gFnSc5	Marina
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
San	San	k1gFnSc2	San
Marino	Marina	k1gFnSc5	Marina
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc3	Wikislovník
San	San	k1gMnSc7	San
Marino	Marina	k1gFnSc5	Marina
na	na	k7c4	na
OpenStreetMap	OpenStreetMap	k1gMnSc1	OpenStreetMap
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Hymna	hymna	k1gFnSc1	hymna
San	San	k1gFnSc1	San
Marina	Marina	k1gFnSc1	Marina
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1894	[number]	k4	1894
San	San	k1gFnPc2	San
Marino	Marina	k1gFnSc5	Marina
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Freedom	Freedom	k1gInSc4	Freedom
House	house	k1gNnSc1	house
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Bureau	Burea	k2eAgFnSc4d1	Burea
of	of	k?	of
European	Europeana	k1gFnPc2	Europeana
and	and	k?	and
Eurasian	Eurasian	k1gInSc1	Eurasian
Affairs	Affairs	k1gInSc1	Affairs
<g/>
.	.	kIx.	.
</s>
<s>
Background	Background	k1gInSc1	Background
Note	Note	k1gInSc1	Note
<g/>
:	:	kIx,	:
San	San	k1gFnSc1	San
Marino	Marina	k1gFnSc5	Marina
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
U.	U.	kA	U.
<g/>
S.	S.	kA	S.
Department	department	k1gInSc1	department
of	of	k?	of
State	status	k1gInSc5	status
<g/>
,	,	kIx,	,
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
6	[number]	k4	6
<g/>
-	-	kIx~	-
<g/>
13	[number]	k4	13
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
The	The	k?	The
World	World	k1gInSc1	World
Factbook	Factbook	k1gInSc1	Factbook
-	-	kIx~	-
San	San	k1gFnSc1	San
Marino	Marina	k1gFnSc5	Marina
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Zastupitelský	zastupitelský	k2eAgInSc1d1	zastupitelský
úřad	úřad	k1gInSc1	úřad
ČR	ČR	kA	ČR
při	při	k7c6	při
Svatém	svatý	k2eAgInSc6d1	svatý
stolci	stolec	k1gInSc6	stolec
<g/>
.	.	kIx.	.
</s>
<s>
Souhrnná	souhrnný	k2eAgFnSc1d1	souhrnná
teritoriální	teritoriální	k2eAgFnSc1d1	teritoriální
informace	informace	k1gFnSc1	informace
<g/>
:	:	kIx,	:
San	San	k1gFnSc1	San
Marino	Marina	k1gFnSc5	Marina
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Businessinfo	Businessinfo	k1gNnSc1	Businessinfo
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
<g/>
,	,	kIx,	,
2009	[number]	k4	2009
<g/>
-	-	kIx~	-
<g/>
10	[number]	k4	10
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
San	San	k1gFnSc1	San
Marino	Marina	k1gFnSc5	Marina
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Encyclopaedia	Encyclopaedium	k1gNnPc1	Encyclopaedium
Britannica	Britannic	k2eAgNnPc1d1	Britannic
<g/>
,	,	kIx,	,
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
-	-	kIx~	-
<g/>
21	[number]	k4	21
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
