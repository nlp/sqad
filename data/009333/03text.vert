<p>
<s>
Alphabet	Alphabet	k1gMnSc1	Alphabet
Inc	Inc	k1gMnSc1	Inc
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
americký	americký	k2eAgMnSc1d1	americký
celosvětově	celosvětově	k6eAd1	celosvětově
působící	působící	k2eAgInSc1d1	působící
konglomerát	konglomerát	k1gInSc1	konglomerát
<g/>
,	,	kIx,	,
vytvořený	vytvořený	k2eAgInSc1d1	vytvořený
2	[number]	k4	2
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2015	[number]	k4	2015
dvěma	dva	k4xCgInPc7	dva
zakladateli	zakladatel	k1gMnPc7	zakladatel
společnosti	společnost	k1gFnSc2	společnost
Google	Google	k1gNnSc1	Google
<g/>
,	,	kIx,	,
Larrym	Larrymum	k1gNnPc2	Larrymum
Pagem	Pag	k1gMnSc7	Pag
a	a	k8xC	a
Sergeyem	Sergey	k1gMnSc7	Sergey
Brinem	Brin	k1gMnSc7	Brin
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
předtím	předtím	k6eAd1	předtím
v	v	k7c6	v
Googlu	Googl	k1gMnSc6	Googl
zastává	zastávat	k5eAaImIp3nS	zastávat
Page	Page	k1gFnSc7	Page
funkci	funkce	k1gFnSc4	funkce
CEO	CEO	kA	CEO
a	a	k8xC	a
Brin	Brin	k1gMnSc1	Brin
je	být	k5eAaImIp3nS	být
prezidentem	prezident	k1gMnSc7	prezident
společnosti	společnost	k1gFnSc2	společnost
<g/>
.	.	kIx.	.
</s>
<s>
Alphabet	Alphabet	k1gMnSc1	Alphabet
Inc	Inc	k1gMnSc1	Inc
<g/>
.	.	kIx.	.
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
především	především	k6eAd1	především
mateřskou	mateřský	k2eAgFnSc7d1	mateřská
společností	společnost	k1gFnSc7	společnost
Googlu	Googl	k1gInSc2	Googl
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
několika	několik	k4yIc2	několik
dalších	další	k2eAgFnPc2d1	další
firem	firma	k1gFnPc2	firma
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
do	do	k7c2	do
ní	on	k3xPp3gFnSc6	on
převedeny	převést	k5eAaPmNgInP	převést
z	z	k7c2	z
majetku	majetek	k1gInSc2	majetek
Googlu	Googl	k1gInSc2	Googl
<g/>
.	.	kIx.	.
</s>
<s>
Společnost	společnost	k1gFnSc1	společnost
má	mít	k5eAaImIp3nS	mít
sídlo	sídlo	k1gNnSc4	sídlo
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Mountain	Mountain	k1gMnSc1	Mountain
View	View	k1gMnSc1	View
v	v	k7c6	v
kalifornském	kalifornský	k2eAgNnSc6d1	kalifornské
Silicon	Silicon	kA	Silicon
Valley	Valley	k1gInPc7	Valley
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
centrála	centrála	k1gFnSc1	centrála
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
Googleplex	Googleplex	k1gInSc1	Googleplex
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Portfolio	portfolio	k1gNnSc1	portfolio
Alphabetu	Alphabet	k1gInSc2	Alphabet
==	==	k?	==
</s>
</p>
<p>
<s>
Portfolio	portfolio	k1gNnSc1	portfolio
Alphabetu	Alphabet	k1gInSc2	Alphabet
zahrnuje	zahrnovat	k5eAaImIp3nS	zahrnovat
firmy	firma	k1gFnPc1	firma
z	z	k7c2	z
několika	několik	k4yIc2	několik
průmyslových	průmyslový	k2eAgNnPc2d1	průmyslové
odvětví	odvětví	k1gNnPc2	odvětví
včetně	včetně	k7c2	včetně
technologického	technologický	k2eAgNnSc2d1	Technologické
odvětví	odvětví	k1gNnSc2	odvětví
<g/>
,	,	kIx,	,
životních	životní	k2eAgFnPc2d1	životní
věd	věda	k1gFnPc2	věda
<g/>
,	,	kIx,	,
investičního	investiční	k2eAgInSc2d1	investiční
kapitálu	kapitál	k1gInSc2	kapitál
a	a	k8xC	a
výzkumu	výzkum	k1gInSc2	výzkum
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
dceřiné	dceřiný	k2eAgFnPc4d1	dceřiná
společnosti	společnost	k1gFnPc4	společnost
patří	patřit	k5eAaImIp3nS	patřit
Google	Google	k1gFnSc1	Google
Calico	Calico	k1gMnSc1	Calico
<g/>
,	,	kIx,	,
GV	GV	kA	GV
<g/>
,	,	kIx,	,
CapitalG	CapitalG	k1gFnPc1	CapitalG
<g/>
,	,	kIx,	,
Verily	Veril	k1gInPc1	Veril
<g/>
,	,	kIx,	,
Waymo	Wayma	k1gFnSc5	Wayma
<g/>
,	,	kIx,	,
X	X	kA	X
a	a	k8xC	a
Google	Google	k1gFnSc1	Google
Fiber	Fibra	k1gFnPc2	Fibra
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
dceřiné	dceřiný	k2eAgFnPc1d1	dceřiná
společnosti	společnost	k1gFnPc1	společnost
Alphabetu	Alphabet	k1gInSc2	Alphabet
změnily	změnit	k5eAaPmAgFnP	změnit
svá	svůj	k3xOyFgNnPc4	svůj
jména	jméno	k1gNnPc4	jméno
po	po	k7c4	po
opuštění	opuštění	k1gNnSc4	opuštění
Googlu	Googl	k1gInSc2	Googl
<g/>
,	,	kIx,	,
Google	Google	k1gFnSc1	Google
Ventures	Ventures	k1gInSc1	Ventures
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
GV	GV	kA	GV
<g/>
,	,	kIx,	,
Google	Google	k1gFnSc2	Google
Life	Life	k1gFnPc2	Life
Sciences	Sciences	k1gMnSc1	Sciences
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
Verily	Verila	k1gFnSc2	Verila
a	a	k8xC	a
Google	Google	k1gFnSc2	Google
X	X	kA	X
dostal	dostat	k5eAaPmAgMnS	dostat
jméno	jméno	k1gNnSc4	jméno
jen	jen	k6eAd1	jen
X.	X.	kA	X.
V	v	k7c6	v
návaznosti	návaznost	k1gFnSc6	návaznost
na	na	k7c4	na
restrukturalizaci	restrukturalizace	k1gFnSc4	restrukturalizace
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgInS	stát
Page	Page	k1gInSc1	Page
CEO	CEO	kA	CEO
Alphabetu	Alphabet	k1gInSc2	Alphabet
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Sundar	Sundar	k1gInSc1	Sundar
Pichai	Picha	k1gFnSc2	Picha
převzal	převzít	k5eAaPmAgInS	převzít
jeho	jeho	k3xOp3gFnSc4	jeho
pozici	pozice	k1gFnSc4	pozice
jako	jako	k8xC	jako
CEO	CEO	kA	CEO
Googlu	Googl	k1gInSc2	Googl
<g/>
.	.	kIx.	.
</s>
<s>
Podíly	podíl	k1gInPc1	podíl
na	na	k7c6	na
akciích	akcie	k1gFnPc6	akcie
Googlu	Googl	k1gInSc2	Googl
byly	být	k5eAaImAgFnP	být
převedeny	převést	k5eAaPmNgInP	převést
na	na	k7c4	na
akcie	akcie	k1gFnPc4	akcie
Alphabetu	Alphabet	k1gInSc2	Alphabet
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
obchodují	obchodovat	k5eAaImIp3nP	obchodovat
burzovním	burzovní	k2eAgInSc7d1	burzovní
symbolem	symbol	k1gInSc7	symbol
"	"	kIx"	"
<g/>
GOOG	GOOG	kA	GOOG
<g/>
"	"	kIx"	"
a	a	k8xC	a
"	"	kIx"	"
<g/>
GOOGL	GOOGL	kA	GOOGL
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vznik	vznik	k1gInSc1	vznik
Alphabetu	Alphabeto	k1gNnSc3	Alphabeto
byl	být	k5eAaImAgInS	být
vyvolán	vyvolat	k5eAaPmNgInS	vyvolat
snahou	snaha	k1gFnSc7	snaha
vytvořit	vytvořit	k5eAaPmF	vytvořit
jádro	jádro	k1gNnSc4	jádro
podnikání	podnikání	k1gNnSc2	podnikání
v	v	k7c6	v
internetových	internetový	k2eAgFnPc6d1	internetová
službách	služba	k1gFnPc6	služba
Googlu	Googl	k1gInSc2	Googl
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
bylo	být	k5eAaImAgNnS	být
"	"	kIx"	"
<g/>
ryzejší	ryzí	k2eAgFnSc1d2	ryzejší
a	a	k8xC	a
odpovědnější	odpovědný	k2eAgFnSc1d2	odpovědnější
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zároveň	zároveň	k6eAd1	zároveň
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
umožnit	umožnit	k5eAaPmF	umožnit
větší	veliký	k2eAgFnSc4d2	veliký
autonomii	autonomie	k1gFnSc4	autonomie
té	ten	k3xDgFnSc3	ten
skupině	skupina	k1gFnSc3	skupina
dceřiných	dceřin	k2eAgFnPc2d1	dceřina
společností	společnost	k1gFnPc2	společnost
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
působí	působit	k5eAaImIp3nP	působit
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
než	než	k8xS	než
internetových	internetový	k2eAgFnPc6d1	internetová
službách	služba	k1gFnPc6	služba
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Reference	reference	k1gFnPc1	reference
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Alphabet	Alphabet	k1gMnSc1	Alphabet
Inc	Inc	k1gMnSc1	Inc
<g/>
.	.	kIx.	.
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Alphabet	Alphabeta	k1gFnPc2	Alphabeta
Inc	Inc	k1gFnSc2	Inc
<g/>
.	.	kIx.	.
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnPc1d1	oficiální
stránky	stránka	k1gFnPc1	stránka
</s>
</p>
