<s>
Mužská	mužský	k2eAgFnSc1d1
i	i	k8xC
ženská	ženský	k2eAgFnSc1d1
sexuální	sexuální	k2eAgFnSc1d1
aktivita	aktivita	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
Kazachstánu	Kazachstán	k1gInSc6
legální	legální	k2eAgFnSc1d1
od	od	k7c2
r.	r.	kA
1998	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Věk	věk	k1gInSc1
způsobilosti	způsobilost	k1gFnSc2
k	k	k7c3
pohlavnímu	pohlavní	k2eAgInSc3d1
styku	styk	k1gInSc3
je	být	k5eAaImIp3nS
stanoven	stanovit	k5eAaPmNgInS
na	na	k7c4
16	#num#	k4
let	léto	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
Před	před	k7c7
touto	tento	k3xDgFnSc7
novelizací	novelizace	k1gFnSc7
kazašský	kazašský	k2eAgInSc1d1
trestní	trestní	k2eAgInSc1d1
zákoník	zákoník	k1gInSc1
trestal	trestat	k5eAaImAgInS
dle	dle	k7c2
§	§	k?
104	#num#	k4
anální	anální	k2eAgInSc4d1
sex	sex	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Legální	legální	k2eAgInSc1d1
stejnopohlavní	stejnopohlavní	k2eAgInSc1d1
styk	styk	k1gInSc1
</s>
<s>
Zákony	zákon	k1gInPc1
týkající	týkající	k2eAgFnSc2d1
se	se	k3xPyFc4
stejnopohlavní	stejnopohlavní	k2eAgFnSc2d1
sexuální	sexuální	k2eAgFnSc2d1
aktivity	aktivita	k1gFnSc2
</s>