<s>
Hoří	hořet	k5eAaImIp3nS	hořet
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
panenko	panenka	k1gFnSc5	panenka
je	on	k3xPp3gMnPc4	on
česko-italský	českotalský	k2eAgInSc1d1	česko-italský
hraný	hraný	k2eAgInSc1d1	hraný
film	film	k1gInSc1	film
režiséra	režisér	k1gMnSc2	režisér
Miloše	Miloš	k1gMnSc2	Miloš
Formana	Forman	k1gMnSc2	Forman
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1967	[number]	k4	1967
<g/>
.	.	kIx.	.
</s>
<s>
Původně	původně	k6eAd1	původně
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
film	film	k1gInSc1	film
natáčet	natáčet	k5eAaImF	natáčet
na	na	k7c4	na
černobílý	černobílý	k2eAgInSc4d1	černobílý
filmový	filmový	k2eAgInSc4d1	filmový
materiál	materiál	k1gInSc4	materiál
<g/>
,	,	kIx,	,
Formanovi	Forman	k1gMnSc3	Forman
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
nabídnuto	nabídnout	k5eAaPmNgNnS	nabídnout
točit	točit	k5eAaImF	točit
na	na	k7c4	na
barevný	barevný	k2eAgInSc4d1	barevný
<g/>
,	,	kIx,	,
čehož	což	k3yRnSc2	což
Forman	Forman	k1gMnSc1	Forman
využil	využít	k5eAaPmAgInS	využít
<g/>
.	.	kIx.	.
</s>
<s>
Důležité	důležitý	k2eAgNnSc1d1	důležité
je	být	k5eAaImIp3nS	být
podotknout	podotknout	k5eAaPmF	podotknout
<g/>
,	,	kIx,	,
že	že	k8xS	že
postavy	postava	k1gFnPc1	postava
vystupující	vystupující	k2eAgFnPc1d1	vystupující
ve	v	k7c6	v
filmu	film	k1gInSc6	film
jsou	být	k5eAaImIp3nP	být
ztvárněny	ztvárněn	k2eAgInPc4d1	ztvárněn
převážně	převážně	k6eAd1	převážně
neherci	neherec	k1gMnPc7	neherec
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
vrchlabských	vrchlabský	k2eAgMnPc2d1	vrchlabský
hasičů	hasič	k1gMnPc2	hasič
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
Vrchlabí	Vrchlabí	k1gNnSc6	Vrchlabí
v	v	k7c6	v
Podkrkonoší	Podkrkonoší	k1gNnSc6	Podkrkonoší
se	se	k3xPyFc4	se
také	také	k9	také
natáčelo	natáčet	k5eAaImAgNnS	natáčet
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgInSc1d1	místní
společenský	společenský	k2eAgInSc1d1	společenský
sál	sál	k1gInSc1	sál
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yQgInSc6	který
byl	být	k5eAaImAgInS	být
film	film	k1gInSc1	film
natočen	natočit	k5eAaBmNgInS	natočit
<g/>
,	,	kIx,	,
zde	zde	k6eAd1	zde
stojí	stát	k5eAaImIp3nS	stát
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
poslední	poslední	k2eAgInSc4d1	poslední
film	film	k1gInSc4	film
Miloše	Miloš	k1gMnSc2	Miloš
Formana	Forman	k1gMnSc2	Forman
natočený	natočený	k2eAgInSc4d1	natočený
v	v	k7c6	v
Československu	Československo	k1gNnSc6	Československo
před	před	k7c7	před
jeho	jeho	k3xOp3gFnSc7	jeho
emigrací	emigrace	k1gFnSc7	emigrace
do	do	k7c2	do
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Jan	Jan	k1gMnSc1	Jan
Vostrčil	Vostrčil	k1gMnSc1	Vostrčil
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Kolb	kolba	k1gFnPc2	kolba
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Valnoha	valnoha	k1gMnSc1	valnoha
<g/>
,	,	kIx,	,
Josef	Josef	k1gMnSc1	Josef
Šebánek	Šebánek	k1gMnSc1	Šebánek
<g/>
,	,	kIx,	,
František	František	k1gMnSc1	František
Debelka	Debelka	k1gMnSc1	Debelka
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
Stöckl	Stöckl	k1gInSc1	Stöckl
<g/>
,	,	kIx,	,
Stanislav	Stanislav	k1gMnSc1	Stanislav
Holubec	Holubec	k1gMnSc1	Holubec
<g/>
,	,	kIx,	,
Alena	Alena	k1gFnSc1	Alena
Květová	květový	k2eAgFnSc1d1	květová
Děj	děj	k1gInSc4	děj
se	se	k3xPyFc4	se
odehrává	odehrávat	k5eAaImIp3nS	odehrávat
na	na	k7c6	na
hasičském	hasičský	k2eAgInSc6d1	hasičský
bále	bál	k1gInSc6	bál
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
baví	bavit	k5eAaImIp3nP	bavit
vesničtí	vesnický	k2eAgMnPc1d1	vesnický
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xC	jako
zpestření	zpestření	k1gNnSc1	zpestření
je	být	k5eAaImIp3nS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gFnPc4	on
připravena	připraven	k2eAgFnSc1d1	připravena
tombola	tombola	k1gFnSc1	tombola
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
je	být	k5eAaImIp3nS	být
však	však	k9	však
rozkradena	rozkraden	k2eAgFnSc1d1	rozkradena
<g/>
,	,	kIx,	,
a	a	k8xC	a
rovněž	rovněž	k9	rovněž
volba	volba	k1gFnSc1	volba
miss	miss	k1gFnSc1	miss
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
však	však	k9	však
také	také	k6eAd1	také
skončí	skončit	k5eAaPmIp3nS	skončit
neslavně	slavně	k6eNd1	slavně
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgInS	být
jako	jako	k9	jako
druhý	druhý	k4xOgInSc1	druhý
český	český	k2eAgInSc1d1	český
film	film	k1gInSc1	film
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
po	po	k7c6	po
Marketě	Market	k1gInSc6	Market
Lazarové	Lazarová	k1gFnSc2	Lazarová
digitálně	digitálně	k6eAd1	digitálně
restaurován	restaurovat	k5eAaBmNgInS	restaurovat
<g/>
.	.	kIx.	.
</s>
<s>
Uveden	uveden	k2eAgMnSc1d1	uveden
byl	být	k5eAaImAgInS	být
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
na	na	k7c6	na
MFF	MFF	kA	MFF
v	v	k7c6	v
Karlových	Karlův	k2eAgInPc6d1	Karlův
Varech	Vary	k1gInPc6	Vary
a	a	k8xC	a
v	v	k7c6	v
září	září	k1gNnSc6	září
2012	[number]	k4	2012
v	v	k7c6	v
distribuci	distribuce	k1gFnSc6	distribuce
kin	kino	k1gNnPc2	kino
<g/>
.	.	kIx.	.
</s>
<s>
Film	film	k1gInSc1	film
byl	být	k5eAaImAgMnS	být
nominován	nominovat	k5eAaBmNgMnS	nominovat
na	na	k7c4	na
Oscara	Oscar	k1gMnSc4	Oscar
v	v	k7c6	v
kategorii	kategorie	k1gFnSc6	kategorie
nejlepší	dobrý	k2eAgInSc4d3	nejlepší
cizojazyčný	cizojazyčný	k2eAgInSc4d1	cizojazyčný
film	film	k1gInSc4	film
<g/>
.	.	kIx.	.
</s>
<s>
Hoří	hořet	k5eAaImIp3nS	hořet
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
panenko	panenka	k1gFnSc5	panenka
na	na	k7c6	na
webu	web	k1gInSc6	web
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Hoří	hořet	k5eAaImIp3nS	hořet
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
panenko	panenka	k1gFnSc5	panenka
v	v	k7c6	v
Česko-Slovenské	českolovenský	k2eAgFnSc3d1	česko-slovenská
filmové	filmový	k2eAgFnSc3d1	filmová
databázi	databáze	k1gFnSc3	databáze
Hoří	hořet	k5eAaImIp3nS	hořet
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
panenko	panenka	k1gFnSc5	panenka
na	na	k7c6	na
Kinoboxu	Kinobox	k1gInSc2	Kinobox
<g/>
.	.	kIx.	.
<g/>
cz	cz	k?	cz
Hoří	hořet	k5eAaImIp3nS	hořet
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
panenko	panenka	k1gFnSc5	panenka
ve	v	k7c6	v
Filmové	filmový	k2eAgFnSc3d1	filmová
databázi	databáze	k1gFnSc3	databáze
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Hoří	hořet	k5eAaImIp3nP	hořet
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
panenko	panenka	k1gFnSc5	panenka
v	v	k7c6	v
Internet	Internet	k1gInSc1	Internet
Movie	Movie	k1gFnSc2	Movie
Database	Databasa	k1gFnSc3	Databasa
</s>
