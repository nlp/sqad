<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
pevné	pevný	k2eAgNnSc1d1	pevné
pletivo	pletivo	k1gNnSc1	pletivo
stonků	stonek	k1gInPc2	stonek
vyšších	vysoký	k2eAgFnPc2d2	vyšší
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
označujeme	označovat	k5eAaImIp1nP	označovat
jako	jako	k9	jako
dřeviny	dřevina	k1gFnPc4	dřevina
<g/>
?	?	kIx.	?
</s>
