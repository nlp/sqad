<p>
<s>
Dřevo	dřevo	k1gNnSc1	dřevo
je	být	k5eAaImIp3nS	být
pevné	pevný	k2eAgNnSc1d1	pevné
pletivo	pletivo	k1gNnSc1	pletivo
stonků	stonek	k1gInPc2	stonek
vyšších	vysoký	k2eAgFnPc2d2	vyšší
rostlin	rostlina	k1gFnPc2	rostlina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
označujeme	označovat	k5eAaImIp1nP	označovat
jako	jako	k9	jako
dřeviny	dřevina	k1gFnPc4	dřevina
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
v	v	k7c6	v
rostlinách	rostlina	k1gFnPc6	rostlina
z	z	k7c2	z
meristémových	meristémův	k2eAgFnPc2d1	meristémův
buněk	buňka	k1gFnPc2	buňka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dřevo	dřevo	k1gNnSc1	dřevo
je	být	k5eAaImIp3nS	být
zahrnováno	zahrnovat	k5eAaImNgNnS	zahrnovat
mezi	mezi	k7c4	mezi
obnovitelné	obnovitelný	k2eAgInPc4d1	obnovitelný
zdroje	zdroj	k1gInPc4	zdroj
energie	energie	k1gFnSc2	energie
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
druhů	druh	k1gInPc2	druh
biomasy	biomasa	k1gFnSc2	biomasa
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
snadno	snadno	k6eAd1	snadno
dostupný	dostupný	k2eAgInSc1d1	dostupný
přírodní	přírodní	k2eAgInSc1d1	přírodní
materiál	materiál	k1gInSc1	materiál
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
lidé	člověk	k1gMnPc1	člověk
široce	široko	k6eAd1	široko
využívají	využívat	k5eAaPmIp3nP	využívat
po	po	k7c4	po
celou	celý	k2eAgFnSc4d1	celá
dobu	doba	k1gFnSc4	doba
své	svůj	k3xOyFgFnSc2	svůj
historie	historie	k1gFnSc2	historie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Chemické	chemický	k2eAgNnSc1d1	chemické
složení	složení	k1gNnSc1	složení
dřeva	dřevo	k1gNnSc2	dřevo
==	==	k?	==
</s>
</p>
<p>
<s>
celulóza	celulóza	k1gFnSc1	celulóza
(	(	kIx(	(
<g/>
40	[number]	k4	40
<g/>
–	–	k?	–
<g/>
50	[number]	k4	50
%	%	kIx~	%
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
lignin	lignin	k1gInSc1	lignin
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
%	%	kIx~	%
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
hemicelulózy	hemicelulóza	k1gFnPc1	hemicelulóza
(	(	kIx(	(
<g/>
20	[number]	k4	20
<g/>
–	–	k?	–
<g/>
30	[number]	k4	30
%	%	kIx~	%
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
doprovodné	doprovodný	k2eAgFnPc4d1	doprovodná
složky	složka	k1gFnPc4	složka
</s>
</p>
<p>
<s>
další	další	k2eAgFnPc1d1	další
organické	organický	k2eAgFnPc1d1	organická
látky	látka	k1gFnPc1	látka
(	(	kIx(	(
<g/>
1	[number]	k4	1
<g/>
–	–	k?	–
<g/>
3	[number]	k4	3
%	%	kIx~	%
<g/>
,	,	kIx,	,
u	u	k7c2	u
tropických	tropický	k2eAgFnPc2d1	tropická
dřevin	dřevina	k1gFnPc2	dřevina
až	až	k9	až
15	[number]	k4	15
%	%	kIx~	%
<g/>
)	)	kIx)	)
<g/>
:	:	kIx,	:
terpeny	terpen	k1gInPc1	terpen
<g/>
,	,	kIx,	,
tuky	tuk	k1gInPc1	tuk
<g/>
,	,	kIx,	,
vosky	vosk	k1gInPc1	vosk
<g/>
,	,	kIx,	,
pektiny	pektin	k1gInPc1	pektin
<g/>
,	,	kIx,	,
třísloviny	tříslovina	k1gFnPc1	tříslovina
(	(	kIx(	(
<g/>
pouze	pouze	k6eAd1	pouze
u	u	k7c2	u
listnáčů	listnáč	k1gInPc2	listnáč
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
steroly	sterol	k1gInPc1	sterol
<g/>
,	,	kIx,	,
pryskyřice	pryskyřice	k1gFnSc1	pryskyřice
</s>
</p>
<p>
<s>
anorganické	anorganický	k2eAgFnPc1d1	anorganická
látky	látka	k1gFnPc1	látka
(	(	kIx(	(
<g/>
0,1	[number]	k4	0,1
<g/>
–	–	k?	–
<g/>
0,5	[number]	k4	0,5
%	%	kIx~	%
<g/>
,	,	kIx,	,
u	u	k7c2	u
tropických	tropický	k2eAgFnPc2d1	tropická
dřevin	dřevina	k1gFnPc2	dřevina
až	až	k9	až
5	[number]	k4	5
%	%	kIx~	%
<g/>
)	)	kIx)	)
–	–	k?	–
po	po	k7c6	po
spálení	spálení	k1gNnSc6	spálení
tvoří	tvořit	k5eAaImIp3nS	tvořit
popel	popel	k1gInSc1	popel
</s>
</p>
<p>
<s>
voda	voda	k1gFnSc1	voda
v	v	k7c6	v
různém	různý	k2eAgNnSc6d1	různé
množství	množství	k1gNnSc6	množství
(	(	kIx(	(
<g/>
podle	podle	k7c2	podle
ročního	roční	k2eAgNnSc2d1	roční
období	období	k1gNnSc2	období
<g/>
,	,	kIx,	,
stupně	stupeň	k1gInSc2	stupeň
vyschnutí	vyschnutí	k1gNnSc2	vyschnutí
dřeva	dřevo	k1gNnSc2	dřevo
atd.	atd.	kA	atd.
<g/>
)	)	kIx)	)
<g/>
Celulóza	celulóza	k1gFnSc1	celulóza
a	a	k8xC	a
hemicelulózy	hemicelulóza	k1gFnPc1	hemicelulóza
patří	patřit	k5eAaImIp3nP	patřit
mezi	mezi	k7c7	mezi
polysacharidy	polysacharid	k1gInPc7	polysacharid
a	a	k8xC	a
bývají	bývat	k5eAaImIp3nP	bývat
souhrnně	souhrnně	k6eAd1	souhrnně
označovány	označovat	k5eAaImNgFnP	označovat
jako	jako	k8xS	jako
holocelulóza	holocelulóza	k1gFnSc1	holocelulóza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stavba	stavba	k1gFnSc1	stavba
dřeva	dřevo	k1gNnSc2	dřevo
==	==	k?	==
</s>
</p>
<p>
<s>
Stavba	stavba	k1gFnSc1	stavba
dřeva	dřevo	k1gNnSc2	dřevo
se	se	k3xPyFc4	se
rozlišuje	rozlišovat	k5eAaImIp3nS	rozlišovat
dle	dle	k7c2	dle
měřítka	měřítko	k1gNnSc2	měřítko
zkoumání	zkoumání	k1gNnSc2	zkoumání
na	na	k7c4	na
<g/>
:	:	kIx,	:
</s>
</p>
<p>
<s>
makroskopická	makroskopický	k2eAgFnSc1d1	makroskopická
stavba	stavba	k1gFnSc1	stavba
dřevního	dřevní	k2eAgNnSc2d1	dřevní
pletiva	pletivo	k1gNnSc2	pletivo
–	–	k?	–
vše	všechen	k3xTgNnSc4	všechen
<g/>
,	,	kIx,	,
co	co	k3yInSc4	co
lze	lze	k6eAd1	lze
pozorovat	pozorovat	k5eAaImF	pozorovat
pouhým	pouhý	k2eAgNnSc7d1	pouhé
okem	oke	k1gNnSc7	oke
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
pomocí	pomocí	k7c2	pomocí
zvětšovacího	zvětšovací	k2eAgNnSc2d1	zvětšovací
skla	sklo	k1gNnSc2	sklo
</s>
</p>
<p>
<s>
mikroskopická	mikroskopický	k2eAgFnSc1d1	mikroskopická
stavba	stavba	k1gFnSc1	stavba
dřeva	dřevo	k1gNnSc2	dřevo
–	–	k?	–
úroveň	úroveň	k1gFnSc1	úroveň
buněk	buňka	k1gFnPc2	buňka
(	(	kIx(	(
<g/>
nutné	nutný	k2eAgNnSc1d1	nutné
použít	použít	k5eAaPmF	použít
mikroskop	mikroskop	k1gInSc4	mikroskop
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
submikroskopická	submikroskopický	k2eAgFnSc1d1	submikroskopická
stavba	stavba	k1gFnSc1	stavba
dřeva	dřevo	k1gNnSc2	dřevo
–	–	k?	–
odlišnosti	odlišnost	k1gFnSc2	odlišnost
v	v	k7c6	v
konkrétní	konkrétní	k2eAgFnSc6d1	konkrétní
stavbě	stavba	k1gFnSc6	stavba
buněčné	buněčný	k2eAgFnSc2d1	buněčná
stěny	stěna	k1gFnSc2	stěna
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc1	jejich
chemické	chemický	k2eAgNnSc1d1	chemické
složení	složení	k1gNnSc1	složení
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Dělení	dělení	k1gNnSc1	dělení
dřeva	dřevo	k1gNnSc2	dřevo
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Základní	základní	k2eAgInPc1d1	základní
druhy	druh	k1gInPc1	druh
dřeva	dřevo	k1gNnSc2	dřevo
===	===	k?	===
</s>
</p>
<p>
<s>
dřevo	dřevo	k1gNnSc1	dřevo
jehličnatých	jehličnatý	k2eAgFnPc2d1	jehličnatá
dřevin	dřevina	k1gFnPc2	dřevina
–	–	k?	–
např.	např.	kA	např.
smrk	smrk	k1gInSc1	smrk
<g/>
,	,	kIx,	,
jedle	jedle	k1gFnSc1	jedle
<g/>
,	,	kIx,	,
borovice	borovice	k1gFnSc1	borovice
<g/>
,	,	kIx,	,
modřín	modřín	k1gInSc1	modřín
<g/>
,	,	kIx,	,
douglaska	douglaska	k1gFnSc1	douglaska
<g/>
,	,	kIx,	,
jalovec	jalovec	k1gInSc1	jalovec
<g/>
,	,	kIx,	,
tis	tis	k1gInSc1	tis
</s>
</p>
<p>
<s>
dřevo	dřevo	k1gNnSc1	dřevo
listnatých	listnatý	k2eAgFnPc2d1	listnatá
dřevin	dřevina	k1gFnPc2	dřevina
</s>
</p>
<p>
<s>
s	s	k7c7	s
kruhovitě	kruhovitě	k6eAd1	kruhovitě
pórovitou	pórovitý	k2eAgFnSc7d1	pórovitá
stavbou	stavba	k1gFnSc7	stavba
–	–	k?	–
např.	např.	kA	např.
dub	dub	k1gInSc1	dub
<g/>
,	,	kIx,	,
jasan	jasan	k1gInSc1	jasan
<g/>
,	,	kIx,	,
akát	akát	k1gInSc1	akát
<g/>
,	,	kIx,	,
jilm	jilm	k1gInSc1	jilm
<g/>
,	,	kIx,	,
pajasan	pajasan	k1gInSc1	pajasan
<g/>
,	,	kIx,	,
morušovník	morušovník	k1gInSc1	morušovník
<g/>
,	,	kIx,	,
kaštanovník	kaštanovník	k1gInSc1	kaštanovník
</s>
</p>
<p>
<s>
s	s	k7c7	s
polokruhovitě	polokruhovitě	k6eAd1	polokruhovitě
pórovitou	pórovitý	k2eAgFnSc7d1	pórovitá
stavbou	stavba	k1gFnSc7	stavba
–	–	k?	–
např.	např.	kA	např.
ořešák	ořešák	k1gInSc1	ořešák
<g/>
,	,	kIx,	,
třešeň	třešeň	k1gFnSc1	třešeň
<g/>
,	,	kIx,	,
švestka	švestka	k1gFnSc1	švestka
</s>
</p>
<p>
<s>
s	s	k7c7	s
roztroušeně	roztroušeně	k6eAd1	roztroušeně
pórovitou	pórovitý	k2eAgFnSc7d1	pórovitá
stavbou	stavba	k1gFnSc7	stavba
dřeva	dřevo	k1gNnSc2	dřevo
–	–	k?	–
např.	např.	kA	např.
buk	buk	k1gInSc1	buk
<g/>
,	,	kIx,	,
platan	platan	k1gInSc1	platan
<g/>
,	,	kIx,	,
habr	habr	k1gInSc1	habr
<g/>
,	,	kIx,	,
olše	olše	k1gFnSc1	olše
<g/>
,	,	kIx,	,
lípa	lípa	k1gFnSc1	lípa
<g/>
,	,	kIx,	,
javor	javor	k1gInSc1	javor
<g/>
,	,	kIx,	,
bříza	bříza	k1gFnSc1	bříza
<g/>
,	,	kIx,	,
topol	topol	k1gInSc1	topol
<g/>
,	,	kIx,	,
vrba	vrba	k1gFnSc1	vrba
<g/>
,	,	kIx,	,
hrušeň	hrušeň	k1gFnSc1	hrušeň
</s>
</p>
<p>
<s>
===	===	k?	===
Objemová	objemový	k2eAgFnSc1d1	objemová
hmotnost	hmotnost	k1gFnSc1	hmotnost
dřeva	dřevo	k1gNnSc2	dřevo
===	===	k?	===
</s>
</p>
<p>
<s>
Dřeva	dřevo	k1gNnPc1	dřevo
s	s	k7c7	s
vysokou	vysoká	k1gFnSc7	vysoká
hustotou	hustota	k1gFnSc7	hustota
bývají	bývat	k5eAaImIp3nP	bývat
označována	označován	k2eAgNnPc1d1	označováno
jako	jako	k8xC	jako
těžká	těžký	k2eAgFnSc1d1	těžká
<g/>
,	,	kIx,	,
s	s	k7c7	s
nízkou	nízký	k2eAgFnSc7d1	nízká
jako	jako	k8xS	jako
lehká	lehký	k2eAgNnPc4d1	lehké
<g/>
.	.	kIx.	.
</s>
<s>
Konkrétní	konkrétní	k2eAgFnSc1d1	konkrétní
hodnota	hodnota	k1gFnSc1	hodnota
nezáleží	záležet	k5eNaImIp3nS	záležet
pouze	pouze	k6eAd1	pouze
na	na	k7c6	na
druhu	druh	k1gInSc6	druh
dřeviny	dřevina	k1gFnSc2	dřevina
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
na	na	k7c6	na
vlhkosti	vlhkost	k1gFnSc6	vlhkost
<g/>
,	,	kIx,	,
růstových	růstový	k2eAgFnPc6d1	růstová
podmínkách	podmínka	k1gFnPc6	podmínka
a	a	k8xC	a
místě	místo	k1gNnSc6	místo
odběru	odběr	k1gInSc2	odběr
vzorku	vzorek	k1gInSc2	vzorek
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Typická	typický	k2eAgFnSc1d1	typická
hodnota	hodnota	k1gFnSc1	hodnota
je	být	k5eAaImIp3nS	být
orientační	orientační	k2eAgInSc4d1	orientační
údaj	údaj	k1gInSc4	údaj
pro	pro	k7c4	pro
suché	suchý	k2eAgNnSc4d1	suché
dřevo	dřevo	k1gNnSc4	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Čerstvým	čerstvý	k2eAgNnSc7d1	čerstvé
dřevem	dřevo	k1gNnSc7	dřevo
(	(	kIx(	(
<g/>
neboli	neboli	k8xC	neboli
syrovým	syrový	k2eAgInPc3d1	syrový
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
míněn	míněn	k2eAgInSc1d1	míněn
stav	stav	k1gInSc1	stav
po	po	k7c6	po
poražení	poražení	k1gNnSc6	poražení
živého	živý	k2eAgInSc2d1	živý
stromu	strom	k1gInSc2	strom
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
sušení	sušení	k1gNnSc2	sušení
<g/>
.	.	kIx.	.
</s>
<s>
Suché	Suché	k2eAgNnSc1d1	Suché
dřevo	dřevo	k1gNnSc1	dřevo
je	být	k5eAaImIp3nS	být
přirozeně	přirozeně	k6eAd1	přirozeně
na	na	k7c6	na
vzduchu	vzduch	k1gInSc6	vzduch
vyschlé	vyschlý	k2eAgNnSc1d1	vyschlé
<g/>
,	,	kIx,	,
bez	bez	k7c2	bez
umělého	umělý	k2eAgNnSc2d1	umělé
dosušování	dosušování	k1gNnSc2	dosušování
(	(	kIx(	(
<g/>
obvykle	obvykle	k6eAd1	obvykle
bývá	bývat	k5eAaImIp3nS	bývat
udáváno	udávat	k5eAaImNgNnS	udávat
12	[number]	k4	12
nebo	nebo	k8xC	nebo
15	[number]	k4	15
<g/>
%	%	kIx~	%
vlhkosti	vlhkost	k1gFnSc2	vlhkost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
jednotlivé	jednotlivý	k2eAgInPc1d1	jednotlivý
botanické	botanický	k2eAgInPc1d1	botanický
druhy	druh	k1gInPc1	druh
dřevin	dřevina	k1gFnPc2	dřevina
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
rodu	rod	k1gInSc2	rod
výrazně	výrazně	k6eAd1	výrazně
liší	lišit	k5eAaImIp3nS	lišit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
uveden	uvést	k5eAaPmNgInS	uvést
i	i	k9	i
příslušný	příslušný	k2eAgInSc1d1	příslušný
botanický	botanický	k2eAgInSc1d1	botanický
druh	druh	k1gInSc1	druh
<g/>
.	.	kIx.	.
</s>
<s>
Pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
údaje	údaj	k1gInPc1	údaj
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
zdrojů	zdroj	k1gInPc2	zdroj
výrazněji	výrazně	k6eAd2	výrazně
liší	lišit	k5eAaImIp3nS	lišit
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
celkové	celkový	k2eAgNnSc1d1	celkové
rozpětí	rozpětí	k1gNnSc1	rozpětí
vyznačeno	vyznačen	k2eAgNnSc1d1	vyznačeno
v	v	k7c6	v
poslední	poslední	k2eAgFnSc6d1	poslední
kolonce	kolonka	k1gFnSc6	kolonka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Tvrdost	tvrdost	k1gFnSc1	tvrdost
dřeva	dřevo	k1gNnSc2	dřevo
===	===	k?	===
</s>
</p>
<p>
<s>
hlavní	hlavní	k2eAgInSc1d1	hlavní
článek	článek	k1gInSc1	článek
<g/>
:	:	kIx,	:
Tvrdost	tvrdost	k1gFnSc1	tvrdost
dřevaDřevo	dřevaDřevo	k6eAd1	dřevaDřevo
je	být	k5eAaImIp3nS	být
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
pro	pro	k7c4	pro
lidi	člověk	k1gMnPc4	člověk
velmi	velmi	k6eAd1	velmi
důležitým	důležitý	k2eAgInSc7d1	důležitý
materiálem	materiál	k1gInSc7	materiál
<g/>
.	.	kIx.	.
</s>
<s>
Každý	každý	k3xTgInSc1	každý
druh	druh	k1gInSc1	druh
dřeva	dřevo	k1gNnSc2	dřevo
má	mít	k5eAaImIp3nS	mít
svoje	svůj	k3xOyFgFnPc4	svůj
zvláštní	zvláštní	k2eAgFnPc4d1	zvláštní
vlastnosti	vlastnost	k1gFnPc4	vlastnost
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
ovlivňuje	ovlivňovat	k5eAaImIp3nS	ovlivňovat
možnosti	možnost	k1gFnPc4	možnost
jeho	jeho	k3xOp3gNnSc4	jeho
využití	využití	k1gNnSc4	využití
<g/>
.	.	kIx.	.
</s>
<s>
Měkké	měkký	k2eAgNnSc1d1	měkké
dřevo	dřevo	k1gNnSc1	dřevo
je	být	k5eAaImIp3nS	být
takové	takový	k3xDgNnSc4	takový
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
snáze	snadno	k6eAd2	snadno
opracovává	opracovávat	k5eAaImIp3nS	opracovávat
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
většinou	většinou	k6eAd1	většinou
z	z	k7c2	z
jehličnatých	jehličnatý	k2eAgInPc2d1	jehličnatý
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
z	z	k7c2	z
listnatých	listnatý	k2eAgInPc2d1	listnatý
například	například	k6eAd1	například
lipové	lipový	k2eAgFnPc1d1	Lipová
<g/>
,	,	kIx,	,
topolové	topolový	k2eAgFnPc1d1	topolová
<g/>
,	,	kIx,	,
vrbové	vrbový	k2eAgFnPc1d1	vrbová
a	a	k8xC	a
další	další	k2eAgFnPc1d1	další
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
tvrdé	tvrdý	k2eAgNnSc1d1	tvrdé
dřevo	dřevo	k1gNnSc1	dřevo
se	se	k3xPyFc4	se
získává	získávat	k5eAaImIp3nS	získávat
hlavně	hlavně	k9	hlavně
z	z	k7c2	z
listnatých	listnatý	k2eAgInPc2d1	listnatý
stromů	strom	k1gInPc2	strom
(	(	kIx(	(
<g/>
z	z	k7c2	z
jehličnatých	jehličnatý	k2eAgInPc2d1	jehličnatý
stromů	strom	k1gInPc2	strom
považujeme	považovat	k5eAaImIp1nP	považovat
za	za	k7c4	za
tvrdé	tvrdý	k2eAgNnSc4d1	tvrdé
dřevo	dřevo	k1gNnSc4	dřevo
borovice	borovice	k1gFnSc2	borovice
<g/>
,	,	kIx,	,
douglasky	douglaska	k1gFnSc2	douglaska
<g/>
,	,	kIx,	,
tisu	tis	k1gInSc2	tis
<g/>
...	...	k?	...
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krom	krom	k7c2	krom
několika	několik	k4yIc2	několik
výjimek	výjimka	k1gFnPc2	výjimka
měkká	měkký	k2eAgFnSc1d1	měkká
dřeva	dřevo	k1gNnSc2	dřevo
podléhají	podléhat	k5eAaImIp3nP	podléhat
hnilobě	hniloba	k1gFnSc3	hniloba
snáze	snadno	k6eAd2	snadno
než	než	k8xS	než
tvrdá	tvrdý	k2eAgFnSc1d1	tvrdá
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
jev	jev	k1gInSc1	jev
však	však	k9	však
lze	lze	k6eAd1	lze
omezit	omezit	k5eAaPmF	omezit
pomocí	pomocí	k7c2	pomocí
vhodného	vhodný	k2eAgNnSc2d1	vhodné
ošetření	ošetření	k1gNnSc2	ošetření
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Využití	využití	k1gNnSc1	využití
dřeva	dřevo	k1gNnSc2	dřevo
==	==	k?	==
</s>
</p>
<p>
<s>
výroba	výroba	k1gFnSc1	výroba
nástrojů	nástroj	k1gInPc2	nástroj
</s>
</p>
<p>
<s>
nábytek	nábytek	k1gInSc1	nábytek
</s>
</p>
<p>
<s>
stavební	stavební	k2eAgInSc1d1	stavební
materiál	materiál	k1gInSc1	materiál
</s>
</p>
<p>
<s>
výroba	výroba	k1gFnSc1	výroba
dopravních	dopravní	k2eAgInPc2d1	dopravní
prostředků	prostředek	k1gInPc2	prostředek
(	(	kIx(	(
<g/>
lodě	loď	k1gFnPc1	loď
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
palivo	palivo	k1gNnSc1	palivo
</s>
</p>
<p>
<s>
výroba	výroba	k1gFnSc1	výroba
řeziva	řezivo	k1gNnSc2	řezivo
</s>
</p>
<p>
<s>
výroba	výroba	k1gFnSc1	výroba
papíru	papír	k1gInSc2	papír
a	a	k8xC	a
celulózy	celulóza	k1gFnSc2	celulóza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
další	další	k2eAgFnPc1d1	další
chemické	chemický	k2eAgFnPc1d1	chemická
suroviny	surovina	k1gFnPc1	surovina
<g/>
,	,	kIx,	,
získávané	získávaný	k2eAgInPc1d1	získávaný
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
(	(	kIx(	(
<g/>
pryskyřice	pryskyřice	k1gFnSc1	pryskyřice
<g/>
,	,	kIx,	,
přírodní	přírodní	k2eAgInSc1d1	přírodní
kaučuk	kaučuk	k1gInSc1	kaučuk
<g/>
,	,	kIx,	,
tříslová	tříslový	k2eAgFnSc1d1	tříslová
kůra	kůra	k1gFnSc1	kůra
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Stavby	stavba	k1gFnSc2	stavba
na	na	k7c6	na
bázi	báze	k1gFnSc6	báze
dřeva	dřevo	k1gNnSc2	dřevo
===	===	k?	===
</s>
</p>
<p>
<s>
Hrázděné	hrázděný	k2eAgNnSc1d1	hrázděné
zdivo	zdivo	k1gNnSc1	zdivo
</s>
</p>
<p>
<s>
Hrázděná	hrázděný	k2eAgFnSc1d1	hrázděná
stavba	stavba	k1gFnSc1	stavba
</s>
</p>
<p>
<s>
Roubená	roubený	k2eAgFnSc1d1	roubená
stavba	stavba	k1gFnSc1	stavba
</s>
</p>
<p>
<s>
Lepená	lepený	k2eAgFnSc1d1	lepená
dřevěná	dřevěný	k2eAgFnSc1d1	dřevěná
konstrukce	konstrukce	k1gFnSc1	konstrukce
</s>
</p>
<p>
<s>
===	===	k?	===
Palivové	palivový	k2eAgNnSc1d1	palivové
dřevo	dřevo	k1gNnSc1	dřevo
===	===	k?	===
</s>
</p>
<p>
<s>
Dřevo	dřevo	k1gNnSc1	dřevo
se	se	k3xPyFc4	se
po	po	k7c6	po
vytěžení	vytěžení	k1gNnSc6	vytěžení
dělí	dělit	k5eAaImIp3nS	dělit
do	do	k7c2	do
několika	několik	k4yIc2	několik
základních	základní	k2eAgFnPc2d1	základní
skupin	skupina	k1gFnPc2	skupina
<g/>
.	.	kIx.	.
</s>
<s>
Jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
skupin	skupina	k1gFnPc2	skupina
je	být	k5eAaImIp3nS	být
i	i	k9	i
palivové	palivový	k2eAgNnSc4d1	palivové
dřevo	dřevo	k1gNnSc4	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
palivové	palivový	k2eAgNnSc4d1	palivové
dřevo	dřevo	k1gNnSc4	dřevo
je	být	k5eAaImIp3nS	být
obvykle	obvykle	k6eAd1	obvykle
považováno	považován	k2eAgNnSc1d1	považováno
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
má	mít	k5eAaImIp3nS	mít
až	až	k9	až
do	do	k7c2	do
70	[number]	k4	70
<g/>
%	%	kIx~	%
hniloby	hniloba	k1gFnSc2	hniloba
nebo	nebo	k8xC	nebo
je	být	k5eAaImIp3nS	být
různými	různý	k2eAgInPc7d1	různý
způsoby	způsob	k1gInPc7	způsob
znehodnoceno	znehodnocen	k2eAgNnSc4d1	znehodnoceno
(	(	kIx(	(
<g/>
pokřivení	pokřivení	k1gNnSc4	pokřivení
<g/>
,	,	kIx,	,
odřezky	odřezek	k1gInPc7	odřezek
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
a	a	k8xC	a
nevhodné	vhodný	k2eNgFnPc1d1	nevhodná
tak	tak	k9	tak
k	k	k7c3	k
dalšímu	další	k2eAgNnSc3d1	další
zpracování	zpracování	k1gNnSc3	zpracování
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Výhřevnost	výhřevnost	k1gFnSc1	výhřevnost
dřeva	dřevo	k1gNnSc2	dřevo
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
průměru	průměr	k1gInSc6	průměr
zhruba	zhruba	k6eAd1	zhruba
50	[number]	k4	50
<g/>
%	%	kIx~	%
výhřevnosti	výhřevnost	k1gFnSc2	výhřevnost
měrného	měrný	k2eAgNnSc2d1	měrné
paliva	palivo	k1gNnSc2	palivo
</s>
</p>
<p>
<s>
===	===	k?	===
Charakter	charakter	k1gInSc1	charakter
dřeva	dřevo	k1gNnSc2	dřevo
podle	podle	k7c2	podle
druhu	druh	k1gInSc2	druh
===	===	k?	===
</s>
</p>
<p>
<s>
====	====	k?	====
Tvrdá	tvrdý	k2eAgNnPc1d1	tvrdé
dřeva	dřevo	k1gNnPc1	dřevo
====	====	k?	====
</s>
</p>
<p>
<s>
Listnaté	listnatý	k2eAgInPc1d1	listnatý
stromy	strom	k1gInPc1	strom
</s>
</p>
<p>
<s>
habr	habr	k1gInSc1	habr
–	–	k?	–
z	z	k7c2	z
našich	náš	k3xOp1gFnPc2	náš
původních	původní	k2eAgFnPc2d1	původní
listnatých	listnatý	k2eAgFnPc2d1	listnatá
dřevin	dřevina	k1gFnPc2	dřevina
je	být	k5eAaImIp3nS	být
nejtvrdší	tvrdý	k2eAgNnSc1d3	nejtvrdší
<g/>
,	,	kIx,	,
v	v	k7c6	v
suchu	sucho	k1gNnSc6	sucho
velmi	velmi	k6eAd1	velmi
stálý	stálý	k2eAgInSc1d1	stálý
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nesnáší	snášet	k5eNaImIp3nS	snášet
vlhkost	vlhkost	k1gFnSc1	vlhkost
a	a	k8xC	a
ve	v	k7c6	v
venkovních	venkovní	k2eAgFnPc6d1	venkovní
podmínkách	podmínka	k1gFnPc6	podmínka
oproti	oproti	k7c3	oproti
jiným	jiný	k2eAgNnPc3d1	jiné
tvrdým	tvrdý	k2eAgNnPc3d1	tvrdé
dřevům	dřevo	k1gNnPc3	dřevo
extrémně	extrémně	k6eAd1	extrémně
rychle	rychle	k6eAd1	rychle
degraduje	degradovat	k5eAaBmIp3nS	degradovat
<g/>
;	;	kIx,	;
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
násad	násada	k1gFnPc2	násada
<g/>
,	,	kIx,	,
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
na	na	k7c4	na
vačkové	vačkový	k2eAgInPc4d1	vačkový
hřídele	hřídel	k1gInPc4	hřídel
<g/>
,	,	kIx,	,
palečná	palečný	k2eAgNnPc1d1	palečné
a	a	k8xC	a
ozubená	ozubený	k2eAgNnPc1d1	ozubené
kola	kolo	k1gNnPc1	kolo
ve	v	k7c6	v
mlýnech	mlýn	k1gInPc6	mlýn
<g/>
,	,	kIx,	,
habrové	habrový	k2eAgNnSc4d1	habrové
dřevo	dřevo	k1gNnSc4	dřevo
je	být	k5eAaImIp3nS	být
vynikající	vynikající	k2eAgNnSc4d1	vynikající
palivo	palivo	k1gNnSc4	palivo
s	s	k7c7	s
výhřevností	výhřevnost	k1gFnSc7	výhřevnost
o	o	k7c4	o
40	[number]	k4	40
<g/>
%	%	kIx~	%
vyšší	vysoký	k2eAgMnSc1d2	vyšší
než	než	k8xS	než
smrk	smrk	k1gInSc1	smrk
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
ostatním	ostatní	k2eAgFnPc3d1	ostatní
kvalitám	kvalita	k1gFnPc3	kvalita
a	a	k8xC	a
bezkonkurenčním	bezkonkurenční	k2eAgFnPc3d1	bezkonkurenční
vlastnostem	vlastnost	k1gFnPc3	vlastnost
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc7	jeho
pálení	pálení	k1gNnSc4	pálení
neekonomické	ekonomický	k2eNgFnSc2d1	neekonomická
</s>
</p>
<p>
<s>
babyka	babyka	k1gFnSc1	babyka
–	–	k?	–
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
našich	náš	k3xOp1gNnPc2	náš
nejtvrdších	tvrdý	k2eAgNnPc2d3	nejtvrdší
dřev	dřevo	k1gNnPc2	dřevo
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
vhodné	vhodný	k2eAgNnSc1d1	vhodné
k	k	k7c3	k
soustružení	soustružení	k1gNnSc3	soustružení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
málo	málo	k1gNnSc1	málo
dostupné	dostupný	k2eAgFnPc1d1	dostupná
(	(	kIx(	(
<g/>
babyky	babyka	k1gFnPc1	babyka
u	u	k7c2	u
nás	my	k3xPp1nPc4	my
nejsou	být	k5eNaImIp3nP	být
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
preference	preference	k1gFnSc2	preference
vápenitého	vápenitý	k2eAgNnSc2d1	vápenité
podloží	podloží	k1gNnSc2	podloží
příliš	příliš	k6eAd1	příliš
rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
k	k	k7c3	k
menším	malý	k2eAgInPc3d2	menší
stromům	strom	k1gInPc3	strom
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
dávají	dávat	k5eAaImIp3nP	dávat
méně	málo	k6eAd2	málo
dřevní	dřevní	k2eAgFnPc1d1	dřevní
hmoty	hmota	k1gFnPc1	hmota
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
dub	dub	k1gInSc1	dub
letní	letní	k2eAgFnSc2d1	letní
–	–	k?	–
tvrdé	tvrdá	k1gFnSc2	tvrdá
<g/>
,	,	kIx,	,
snáší	snášet	k5eAaImIp3nS	snášet
sucho	sucho	k6eAd1	sucho
i	i	k9	i
vodu	voda	k1gFnSc4	voda
<g/>
,	,	kIx,	,
z	z	k7c2	z
původních	původní	k2eAgFnPc2d1	původní
dřevin	dřevina	k1gFnPc2	dřevina
nejlépe	dobře	k6eAd3	dobře
snáší	snášet	k5eAaImIp3nS	snášet
změny	změna	k1gFnPc4	změna
vlhkosti	vlhkost	k1gFnSc2	vlhkost
<g/>
,	,	kIx,	,
při	při	k7c6	při
dlouhodobém	dlouhodobý	k2eAgNnSc6d1	dlouhodobé
potopení	potopení	k1gNnSc6	potopení
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
zkamení	zkamenit	k5eAaPmIp3nS	zkamenit
<g/>
,	,	kIx,	,
odolné	odolný	k2eAgNnSc4d1	odolné
vůči	vůči	k7c3	vůči
škůdcům	škůdce	k1gMnPc3	škůdce
<g/>
,	,	kIx,	,
výroba	výroba	k1gFnSc1	výroba
konstrukcí	konstrukce	k1gFnPc2	konstrukce
<g/>
,	,	kIx,	,
sudů	sud	k1gInPc2	sud
(	(	kIx(	(
<g/>
především	především	k9	především
na	na	k7c4	na
víno	víno	k1gNnSc4	víno
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pražců	pražec	k1gInPc2	pražec
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
nábytek	nábytek	k1gInSc1	nábytek
<g/>
,	,	kIx,	,
kola	kolo	k1gNnPc1	kolo
a	a	k8xC	a
z	z	k7c2	z
kůry	kůra	k1gFnSc2	kůra
se	se	k3xPyFc4	se
získávalo	získávat	k5eAaImAgNnS	získávat
tříslo	tříslo	k1gNnSc1	tříslo
pro	pro	k7c4	pro
koželužství	koželužství	k1gNnSc4	koželužství
</s>
</p>
<p>
<s>
buk	buk	k1gInSc4	buk
lesní	lesní	k2eAgInSc4d1	lesní
–	–	k?	–
tvrdé	tvrdý	k2eAgNnSc1d1	tvrdé
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
dubu	dub	k1gInSc3	dub
ale	ale	k8xC	ale
špatně	špatně	k6eAd1	špatně
snáší	snášet	k5eAaImIp3nS	snášet
vlhkost	vlhkost	k1gFnSc1	vlhkost
a	a	k8xC	a
střídání	střídání	k1gNnSc1	střídání
vlhka	vlhko	k1gNnSc2	vlhko
se	s	k7c7	s
suchem	sucho	k1gNnSc7	sucho
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nevyniká	vynikat	k5eNaImIp3nS	vynikat
trvanlivostí	trvanlivost	k1gFnSc7	trvanlivost
<g/>
;	;	kIx,	;
je	být	k5eAaImIp3nS	být
pevné	pevný	k2eAgNnSc1d1	pevné
<g/>
,	,	kIx,	,
méně	málo	k6eAd2	málo
pružné	pružný	k2eAgNnSc1d1	pružné
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
obrábí	obrábět	k5eAaImIp3nS	obrábět
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
dýh	dýha	k1gFnPc2	dýha
<g/>
,	,	kIx,	,
překližek	překližka	k1gFnPc2	překližka
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
na	na	k7c4	na
ohýbané	ohýbaný	k2eAgNnSc4d1	ohýbané
<g />
.	.	kIx.	.
</s>
<s>
židle	židle	k1gFnSc1	židle
thonetky	thonetka	k1gFnSc2	thonetka
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
vhodné	vhodný	k2eAgNnSc1d1	vhodné
k	k	k7c3	k
topení	topení	k1gNnSc3	topení
<g/>
,	,	kIx,	,
výrobě	výroba	k1gFnSc3	výroba
dřevěného	dřevěný	k2eAgNnSc2d1	dřevěné
uhlí	uhlí	k1gNnSc2	uhlí
<g/>
;	;	kIx,	;
chuťově	chuťově	k6eAd1	chuťově
neutrální	neutrální	k2eAgInPc1d1	neutrální
<g/>
,	,	kIx,	,
používané	používaný	k2eAgInPc1d1	používaný
v	v	k7c6	v
potravinářství	potravinářství	k1gNnSc6	potravinářství
(	(	kIx(	(
<g/>
dřívka	dřívko	k1gNnSc2	dřívko
k	k	k7c3	k
nanukům	nanuk	k1gInPc3	nanuk
<g/>
,	,	kIx,	,
vařečky	vařečka	k1gFnPc4	vařečka
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
buk	buk	k1gInSc4	buk
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
použít	použít	k5eAaPmF	použít
k	k	k7c3	k
uzení	uzení	k1gNnSc3	uzení
<g/>
,	,	kIx,	,
společně	společně	k6eAd1	společně
s	s	k7c7	s
habrem	habr	k1gInSc7	habr
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
našim	náš	k3xOp1gNnPc3	náš
nejvýhřevnějším	výhřevný	k2eAgNnPc3d3	výhřevný
dřevům	dřevo	k1gNnPc3	dřevo
</s>
</p>
<p>
<s>
jilm	jilm	k1gInSc1	jilm
–	–	k?	–
tvrdé	tvrdá	k1gFnSc2	tvrdá
těžké	těžký	k2eAgNnSc1d1	těžké
trvanlivé	trvanlivý	k2eAgNnSc1d1	trvanlivé
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
pevné	pevný	k2eAgNnSc1d1	pevné
<g/>
,	,	kIx,	,
neštípe	štípat	k5eNaImIp3nS	štípat
se	se	k3xPyFc4	se
a	a	k8xC	a
dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
ohýbá	ohýbat	k5eAaImIp3nS	ohýbat
<g/>
;	;	kIx,	;
dnes	dnes	k6eAd1	dnes
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
používané	používaný	k2eAgInPc4d1	používaný
kvůli	kvůli	k7c3	kvůli
špatné	špatná	k1gFnSc3	špatná
dostupnosti	dostupnost	k1gFnSc2	dostupnost
(	(	kIx(	(
<g/>
jilmové	jilmový	k2eAgInPc1d1	jilmový
porosty	porost	k1gInPc1	porost
byly	být	k5eAaImAgInP	být
zničeny	zničit	k5eAaPmNgInP	zničit
tzv.	tzv.	kA	tzv.
grafiózou	grafióza	k1gFnSc7	grafióza
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
bylo	být	k5eAaImAgNnS	být
používané	používaný	k2eAgNnSc1d1	používané
podobně	podobně	k6eAd1	podobně
jako	jako	k8xS	jako
dubové	dubový	k2eAgNnSc1d1	dubové
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
nábytku	nábytek	k1gInSc2	nábytek
(	(	kIx(	(
<g/>
především	především	k6eAd1	především
ohýbaného	ohýbaný	k2eAgInSc2d1	ohýbaný
<g/>
)	)	kIx)	)
a	a	k8xC	a
dýh	dýha	k1gFnPc2	dýha
</s>
</p>
<p>
<s>
trnovník	trnovník	k1gInSc1	trnovník
akát	akát	k1gInSc1	akát
–	–	k?	–
tvrdé	tvrdá	k1gFnSc2	tvrdá
houževnaté	houževnatý	k2eAgNnSc1d1	houževnaté
dřevo	dřevo	k1gNnSc1	dřevo
často	často	k6eAd1	často
s	s	k7c7	s
výraznou	výrazný	k2eAgFnSc7d1	výrazná
kresbou	kresba	k1gFnSc7	kresba
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
násad	násada	k1gFnPc2	násada
<g/>
,	,	kIx,	,
topůrek	topůrek	k1gInSc1	topůrek
<g/>
;	;	kIx,	;
důležitá	důležitý	k2eAgFnSc1d1	důležitá
je	být	k5eAaImIp3nS	být
správná	správný	k2eAgFnSc1d1	správná
příprava	příprava	k1gFnSc1	příprava
před	před	k7c7	před
zpracováním	zpracování	k1gNnSc7	zpracování
–	–	k?	–
dřevo	dřevo	k1gNnSc1	dřevo
má	mít	k5eAaImIp3nS	mít
tendenci	tendence	k1gFnSc4	tendence
při	při	k7c6	při
rychlejším	rychlý	k2eAgNnSc6d2	rychlejší
sušení	sušení	k1gNnSc6	sušení
praskat	praskat	k5eAaImF	praskat
<g/>
,	,	kIx,	,
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
lépe	dobře	k6eAd2	dobře
dostupný	dostupný	k2eAgInSc1d1	dostupný
než	než	k8xS	než
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
invazivní	invazivní	k2eAgFnSc1d1	invazivní
dřevina	dřevina	k1gFnSc1	dřevina
velkoplošně	velkoplošně	k6eAd1	velkoplošně
kácen	kácen	k2eAgInSc4d1	kácen
</s>
</p>
<p>
<s>
jasan	jasan	k1gInSc1	jasan
–	–	k?	–
z	z	k7c2	z
našich	náš	k3xOp1gNnPc2	náš
dřev	dřevo	k1gNnPc2	dřevo
je	být	k5eAaImIp3nS	být
nejpružnější	pružný	k2eAgNnSc1d3	nejpružnější
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
tvrdé	tvrdý	k2eAgNnSc1d1	tvrdé
a	a	k8xC	a
houževnaté	houževnatý	k2eAgNnSc1d1	houževnaté
<g/>
;	;	kIx,	;
v	v	k7c4	v
nábytkářství	nábytkářství	k1gNnSc4	nábytkářství
hlavně	hlavně	k9	hlavně
obklady	obklad	k1gInPc1	obklad
a	a	k8xC	a
dýhy	dýha	k1gFnPc1	dýha
(	(	kIx(	(
<g/>
především	především	k9	především
pro	pro	k7c4	pro
barvu	barva	k1gFnSc4	barva
a	a	k8xC	a
kresbu	kresba	k1gFnSc4	kresba
<g/>
)	)	kIx)	)
vhodné	vhodný	k2eAgFnPc4d1	vhodná
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
násad	násada	k1gFnPc2	násada
<g/>
,	,	kIx,	,
topůrek	topůrko	k1gNnPc2	topůrko
<g/>
,	,	kIx,	,
madel	madla	k1gNnPc2	madla
<g/>
,	,	kIx,	,
tělocvičného	tělocvičný	k2eAgNnSc2d1	tělocvičné
nářadí	nářadí	k1gNnSc2	nářadí
<g/>
,	,	kIx,	,
hokejek	hokejka	k1gFnPc2	hokejka
<g/>
,	,	kIx,	,
baseballových	baseballový	k2eAgFnPc2d1	baseballová
pálek	pálka	k1gFnPc2	pálka
<g/>
,	,	kIx,	,
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
používané	používaný	k2eAgInPc4d1	používaný
na	na	k7c4	na
lyže	lyže	k1gFnPc4	lyže
a	a	k8xC	a
saně	saně	k1gFnPc4	saně
</s>
</p>
<p>
<s>
javor	javor	k1gInSc1	javor
–	–	k?	–
jedno	jeden	k4xCgNnSc1	jeden
z	z	k7c2	z
našich	náš	k3xOp1gNnPc2	náš
nejsvětlejších	světlý	k2eAgNnPc2d3	nejsvětlejší
dřev	dřevo	k1gNnPc2	dřevo
<g/>
,	,	kIx,	,
relativně	relativně	k6eAd1	relativně
tvrdé	tvrdý	k2eAgInPc1d1	tvrdý
<g/>
,	,	kIx,	,
vhodné	vhodný	k2eAgInPc1d1	vhodný
k	k	k7c3	k
obrábění	obrábění	k1gNnSc3	obrábění
<g/>
,	,	kIx,	,
soustružení	soustružení	k1gNnSc1	soustružení
<g/>
,	,	kIx,	,
řezbářství	řezbářství	k1gNnSc1	řezbářství
<g/>
;	;	kIx,	;
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
(	(	kIx(	(
<g/>
strunných	strunný	k2eAgFnPc2d1	strunná
i	i	k9	i
na	na	k7c6	na
části	část	k1gFnSc6	část
klavírů	klavír	k1gInPc2	klavír
<g/>
)	)	kIx)	)
a	a	k8xC	a
parket	parket	k1gInSc1	parket
</s>
</p>
<p>
<s>
platan	platan	k1gInSc1	platan
–	–	k?	–
pevné	pevný	k2eAgInPc1d1	pevný
středně	středně	k6eAd1	středně
tvrdé	tvrdá	k1gFnPc1	tvrdá
a	a	k8xC	a
těžké	těžký	k2eAgNnSc1d1	těžké
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
exteriéry	exteriér	k1gInPc4	exteriér
ale	ale	k8xC	ale
málo	málo	k6eAd1	málo
trvanlivé	trvanlivý	k2eAgNnSc1d1	trvanlivé
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
v	v	k7c6	v
nábytkářství	nábytkářství	k1gNnSc6	nábytkářství
<g/>
,	,	kIx,	,
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
papíru	papír	k1gInSc2	papír
<g/>
,	,	kIx,	,
podlah	podlaha	k1gFnPc2	podlaha
<g/>
,	,	kIx,	,
hraček	hračka	k1gFnPc2	hračka
a	a	k8xC	a
výroby	výroba	k1gFnSc2	výroba
dýh	dýha	k1gFnPc2	dýha
ze	z	k7c2	z
světlé	světlý	k2eAgFnSc2d1	světlá
běli	běl	k1gFnSc2	běl
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
má	mít	k5eAaImIp3nS	mít
uplatnění	uplatnění	k1gNnSc4	uplatnění
v	v	k7c6	v
intarzii	intarzie	k1gFnSc6	intarzie
</s>
</p>
<p>
<s>
kaštanovník	kaštanovník	k1gInSc1	kaštanovník
(	(	kIx(	(
<g/>
jedlý	jedlý	k2eAgInSc1d1	jedlý
kaštan	kaštan	k1gInSc1	kaštan
<g/>
)	)	kIx)	)
–	–	k?	–
husté	hustý	k2eAgNnSc1d1	husté
kvalitní	kvalitní	k2eAgNnSc1d1	kvalitní
dřevo	dřevo	k1gNnSc1	dřevo
používané	používaný	k2eAgNnSc1d1	používané
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
trámů	trám	k1gInPc2	trám
a	a	k8xC	a
sudů	sud	k1gInPc2	sud
<g/>
,	,	kIx,	,
výhřevné	výhřevný	k2eAgFnSc2d1	výhřevná
<g/>
,	,	kIx,	,
vhodné	vhodný	k2eAgFnSc2d1	vhodná
k	k	k7c3	k
uzení	uzení	k1gNnSc3	uzení
</s>
</p>
<p>
<s>
ořech	ořech	k1gInSc1	ořech
(	(	kIx(	(
<g/>
ořešák	ořešák	k1gInSc1	ořešák
královský	královský	k2eAgInSc1d1	královský
<g/>
,	,	kIx,	,
"	"	kIx"	"
<g/>
vlašák	vlašák	k1gMnSc1	vlašák
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
–	–	k?	–
tvrdé	tvrdý	k2eAgNnSc1d1	tvrdé
dřevo	dřevo	k1gNnSc1	dřevo
s	s	k7c7	s
typickou	typický	k2eAgFnSc7d1	typická
kresbou	kresba	k1gFnSc7	kresba
<g/>
,	,	kIx,	,
poměrně	poměrně	k6eAd1	poměrně
ceněné	ceněný	k2eAgInPc4d1	ceněný
<g/>
,	,	kIx,	,
používané	používaný	k2eAgInPc4d1	používaný
na	na	k7c4	na
obklady	obklad	k1gInPc4	obklad
a	a	k8xC	a
dýhy	dýha	k1gFnPc4	dýha
<g/>
,	,	kIx,	,
dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
opracovává	opracovávat	k5eAaImIp3nS	opracovávat
</s>
</p>
<p>
<s>
ořešák	ořešák	k1gInSc1	ořešák
(	(	kIx(	(
<g/>
ořešák	ořešák	k1gInSc1	ořešák
černý	černý	k2eAgInSc1d1	černý
<g/>
)	)	kIx)	)
–	–	k?	–
podobné	podobný	k2eAgFnPc4d1	podobná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
jako	jako	k8xC	jako
ořech	ořech	k1gInSc4	ořech
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
esteticky	esteticky	k6eAd1	esteticky
působivé	působivý	k2eAgFnSc3d1	působivá
kontrastní	kontrastní	k2eAgFnSc3d1	kontrastní
kresbě	kresba	k1gFnSc3	kresba
dosahuje	dosahovat	k5eAaImIp3nS	dosahovat
na	na	k7c6	na
světových	světový	k2eAgInPc6d1	světový
trzích	trh	k1gInPc6	trh
vysokých	vysoký	k2eAgFnPc2d1	vysoká
cen	cena	k1gFnPc2	cena
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
dýhOvocné	dýhOvocný	k2eAgInPc4d1	dýhOvocný
listnáče	listnáč	k1gInPc4	listnáč
</s>
</p>
<p>
<s>
hrušeň	hrušeň	k1gFnSc1	hrušeň
–	–	k?	–
tvrdé	tvrdá	k1gFnSc2	tvrdá
houževnaté	houževnatý	k2eAgNnSc1d1	houževnaté
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
pevné	pevný	k2eAgNnSc1d1	pevné
<g/>
,	,	kIx,	,
nebortí	bortit	k5eNaImIp3nS	bortit
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
vhodné	vhodný	k2eAgNnSc1d1	vhodné
k	k	k7c3	k
soustružení	soustružení	k1gNnSc3	soustružení
<g/>
,	,	kIx,	,
dřevořezbám	dřevořezba	k1gFnPc3	dřevořezba
<g/>
,	,	kIx,	,
hudební	hudební	k2eAgInPc4d1	hudební
nástroje	nástroj	k1gInPc4	nástroj
<g/>
,	,	kIx,	,
používalo	používat	k5eAaImAgNnS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
dřevěných	dřevěný	k2eAgInPc2d1	dřevěný
závitů	závit	k1gInPc2	závit
(	(	kIx(	(
<g/>
svěráky	svěrák	k1gInPc1	svěrák
<g/>
,	,	kIx,	,
lisy	lis	k1gInPc1	lis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
geometrických	geometrický	k2eAgFnPc2d1	geometrická
pomůcek	pomůcka	k1gFnPc2	pomůcka
a	a	k8xC	a
v	v	k7c6	v
xylografii	xylografie	k1gFnSc6	xylografie
<g/>
,	,	kIx,	,
hrušeň	hrušeň	k1gFnSc1	hrušeň
lze	lze	k6eAd1	lze
použít	použít	k5eAaPmF	použít
i	i	k9	i
k	k	k7c3	k
uzení	uzení	k1gNnSc3	uzení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzhledem	vzhledem	k7c3	vzhledem
ke	k	k7c3	k
kvalitě	kvalita	k1gFnSc3	kvalita
dřeva	dřevo	k1gNnSc2	dřevo
a	a	k8xC	a
široké	široký	k2eAgFnSc2d1	široká
využitelnosti	využitelnost	k1gFnSc2	využitelnost
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
neekonomické	ekonomický	k2eNgNnSc1d1	neekonomické
</s>
</p>
<p>
<s>
jabloň	jabloň	k1gFnSc1	jabloň
–	–	k?	–
vzhledově	vzhledově	k6eAd1	vzhledově
podobné	podobný	k2eAgFnSc3d1	podobná
třešni	třešeň	k1gFnSc3	třešeň
a	a	k8xC	a
hrušni	hrušeň	k1gFnSc3	hrušeň
<g/>
,	,	kIx,	,
tvrdé	tvrdý	k2eAgNnSc1d1	tvrdé
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
dobře	dobře	k6eAd1	dobře
se	se	k3xPyFc4	se
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
<g/>
,	,	kIx,	,
vhodné	vhodný	k2eAgInPc1d1	vhodný
k	k	k7c3	k
soustružení	soustružení	k1gNnSc3	soustružení
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
golfových	golfový	k2eAgFnPc2d1	golfová
holí	hole	k1gFnPc2	hole
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
k	k	k7c3	k
uzení	uzení	k1gNnSc3	uzení
a	a	k8xC	a
pálení	pálení	k1gNnSc4	pálení
v	v	k7c6	v
krbech	krb	k1gInPc6	krb
</s>
</p>
<p>
<s>
třešeň	třešeň	k1gFnSc1	třešeň
–	–	k?	–
tvrdé	tvrdé	k1gNnSc1	tvrdé
<g/>
,	,	kIx,	,
pevné	pevný	k2eAgNnSc1d1	pevné
<g/>
,	,	kIx,	,
houževnaté	houževnatý	k2eAgNnSc1d1	houževnaté
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
je	být	k5eAaImIp3nS	být
ale	ale	k8xC	ale
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
švestka	švestka	k1gFnSc1	švestka
náročnější	náročný	k2eAgFnSc1d2	náročnější
na	na	k7c4	na
sušení	sušení	k1gNnSc4	sušení
<g/>
;	;	kIx,	;
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
dekorativních	dekorativní	k2eAgInPc2d1	dekorativní
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
v	v	k7c6	v
uměleckém	umělecký	k2eAgNnSc6d1	umělecké
truhlářství	truhlářství	k1gNnSc6	truhlářství
<g/>
,	,	kIx,	,
vhodné	vhodný	k2eAgFnPc1d1	vhodná
i	i	k9	i
k	k	k7c3	k
soustružení	soustružení	k1gNnSc3	soustružení
<g/>
,	,	kIx,	,
oproti	oproti	k7c3	oproti
višni	višeň	k1gFnSc3	višeň
je	být	k5eAaImIp3nS	být
méně	málo	k6eAd2	málo
vhodná	vhodný	k2eAgFnSc1d1	vhodná
k	k	k7c3	k
uzení	uzení	k1gNnSc3	uzení
kvůli	kvůli	k7c3	kvůli
vyššímu	vysoký	k2eAgInSc3d2	vyšší
obsahu	obsah	k1gInSc3	obsah
pryskyřice	pryskyřice	k1gFnSc2	pryskyřice
</s>
</p>
<p>
<s>
višeň	višeň	k1gFnSc1	višeň
–	–	k?	–
podobné	podobný	k2eAgFnPc4d1	podobná
vlastnosti	vlastnost	k1gFnPc4	vlastnost
jako	jako	k8xC	jako
švestka	švestka	k1gFnSc1	švestka
a	a	k8xC	a
třešeň	třešeň	k1gFnSc1	třešeň
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
vhodné	vhodný	k2eAgNnSc1d1	vhodné
k	k	k7c3	k
uzení	uzení	k1gNnSc3	uzení
</s>
</p>
<p>
<s>
švestka	švestka	k1gFnSc1	švestka
–	–	k?	–
velmi	velmi	k6eAd1	velmi
tvrdé	tvrdý	k2eAgNnSc1d1	tvrdé
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
však	však	k9	však
obtížně	obtížně	k6eAd1	obtížně
připravuje	připravovat	k5eAaImIp3nS	připravovat
ke	k	k7c3	k
zpracování	zpracování	k1gNnSc3	zpracování
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
má	mít	k5eAaImIp3nS	mít
tendenci	tendence	k1gFnSc4	tendence
praskat	praskat	k5eAaImF	praskat
a	a	k8xC	a
kroutit	kroutit	k5eAaImF	kroutit
se	se	k3xPyFc4	se
<g/>
;	;	kIx,	;
používané	používaný	k2eAgInPc4d1	používaný
k	k	k7c3	k
restaurátorským	restaurátorský	k2eAgFnPc3d1	restaurátorská
pracím	práce	k1gFnPc3	práce
<g/>
,	,	kIx,	,
výrobě	výroba	k1gFnSc3	výroba
dekoračních	dekorační	k2eAgInPc2d1	dekorační
předmětů	předmět	k1gInPc2	předmět
<g/>
,	,	kIx,	,
vhodné	vhodný	k2eAgFnSc2d1	vhodná
k	k	k7c3	k
soustružení	soustružení	k1gNnSc3	soustružení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
uzení	uzení	k1gNnSc1	uzení
</s>
</p>
<p>
<s>
meruňka	meruňka	k1gFnSc1	meruňka
–	–	k?	–
není	být	k5eNaImIp3nS	být
příliš	příliš	k6eAd1	příliš
vhodná	vhodný	k2eAgFnSc1d1	vhodná
k	k	k7c3	k
opracování	opracování	k1gNnSc3	opracování
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
bortí	bortit	k5eAaImIp3nS	bortit
a	a	k8xC	a
praská	praskat	k5eAaImIp3nS	praskat
<g/>
,	,	kIx,	,
dřevo	dřevo	k1gNnSc1	dřevo
je	být	k5eAaImIp3nS	být
použitelné	použitelný	k2eAgNnSc1d1	použitelné
k	k	k7c3	k
uzení	uzení	k1gNnSc3	uzení
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jen	jen	k9	jen
s	s	k7c7	s
výhradami	výhrada	k1gFnPc7	výhrada
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
vyšší	vysoký	k2eAgInSc4d2	vyšší
podíl	podíl	k1gInSc4	podíl
pryskyřice	pryskyřice	k1gFnSc1	pryskyřice
<g/>
,	,	kIx,	,
vhodené	vhodený	k2eAgNnSc1d1	vhodený
je	být	k5eAaImIp3nS	být
jako	jako	k9	jako
palivo	palivo	k1gNnSc4	palivo
</s>
</p>
<p>
<s>
broskvoň	broskvoň	k1gFnSc1	broskvoň
–	–	k?	–
podobný	podobný	k2eAgInSc1d1	podobný
charakterj	charakterj	k1gInSc1	charakterj
jako	jako	k8xC	jako
meruňka	meruňka	k1gFnSc1	meruňka
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
použitelnosti	použitelnost	k1gFnSc2	použitelnost
k	k	k7c3	k
uzeníDřeva	uzeníDřevo	k1gNnPc1	uzeníDřevo
z	z	k7c2	z
ovocných	ovocný	k2eAgFnPc2d1	ovocná
stromů-peckovin	stromůeckovina	k1gFnPc2	stromů-peckovina
(	(	kIx(	(
<g/>
švestka	švestka	k1gFnSc1	švestka
<g/>
,	,	kIx,	,
třešeň	třešeň	k1gFnSc1	třešeň
<g/>
,	,	kIx,	,
višeň	višeň	k1gFnSc1	višeň
<g/>
,	,	kIx,	,
meruňka	meruňka	k1gFnSc1	meruňka
<g/>
,	,	kIx,	,
broskev	broskev	k1gFnSc1	broskev
atp.	atp.	kA	atp.
<g/>
)	)	kIx)	)
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
důvodu	důvod	k1gInSc2	důvod
obsahu	obsah	k1gInSc2	obsah
kyanogenů	kyanogen	k1gInPc2	kyanogen
nevhodná	vhodný	k2eNgFnSc1d1	nevhodná
pro	pro	k7c4	pro
podávání	podávání	k1gNnSc4	podávání
domácím	domácí	k2eAgMnPc3d1	domácí
hlodavcům	hlodavec	k1gMnPc3	hlodavec
a	a	k8xC	a
zajícovcům	zajícovec	k1gMnPc3	zajícovec
(	(	kIx(	(
<g/>
jsou	být	k5eAaImIp3nP	být
pro	pro	k7c4	pro
ně	on	k3xPp3gFnPc4	on
toxická	toxický	k2eAgFnSc1d1	toxická
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
dřeviny	dřevina	k1gFnPc1	dřevina
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
se	se	k3xPyFc4	se
nedoporučují	doporučovat	k5eNaImIp3nP	doporučovat
ke	k	k7c3	k
skrmení	skrmený	k2eAgMnPc1d1	skrmený
a	a	k8xC	a
broušení	broušení	k1gNnSc3	broušení
zubů	zub	k1gInPc2	zub
<g/>
,	,	kIx,	,
jsou	být	k5eAaImIp3nP	být
uvedeny	uvést	k5eAaPmNgFnP	uvést
na	na	k7c6	na
specializovaných	specializovaný	k2eAgInPc6d1	specializovaný
serverech	server	k1gInPc6	server
<g/>
.	.	kIx.	.
<g/>
Listnaté	listnatý	k2eAgInPc1d1	listnatý
keře	keř	k1gInPc1	keř
</s>
</p>
<p>
<s>
zimolez	zimolez	k1gInSc1	zimolez
obecný	obecný	k2eAgInSc1d1	obecný
–	–	k?	–
velmi	velmi	k6eAd1	velmi
tvrdé	tvrdý	k2eAgNnSc1d1	tvrdé
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejhouževnatějším	houževnatý	k2eAgNnPc3d3	houževnatý
<g/>
,	,	kIx,	,
nejhustším	hustý	k2eAgMnSc6d3	nejhustší
a	a	k8xC	a
nejtěžším	těžký	k2eAgInSc6d3	nejtěžší
(	(	kIx(	(
<g/>
900	[number]	k4	900
kg	kg	kA	kg
<g/>
/	/	kIx~	/
<g/>
m3	m3	k4	m3
<g/>
)	)	kIx)	)
domácím	domácí	k2eAgFnPc3d1	domácí
dřevinám	dřevina	k1gFnPc3	dřevina
<g/>
;	;	kIx,	;
průmyslové	průmyslový	k2eAgNnSc1d1	průmyslové
využití	využití	k1gNnSc1	využití
dnes	dnes	k6eAd1	dnes
neexistuje	existovat	k5eNaImIp3nS	existovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
zimolezového	zimolezový	k2eAgNnSc2d1	zimolezový
dřeva	dřevo	k1gNnSc2	dřevo
je	být	k5eAaImIp3nS	být
nedostatek	nedostatek	k1gInSc1	nedostatek
(	(	kIx(	(
<g/>
keř	keř	k1gInSc1	keř
<g/>
,	,	kIx,	,
tenké	tenký	k2eAgInPc1d1	tenký
kmínky	kmínek	k1gInPc1	kmínek
<g/>
,	,	kIx,	,
pomalu	pomalu	k6eAd1	pomalu
roste	růst	k5eAaImIp3nS	růst
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
z	z	k7c2	z
něj	on	k3xPp3gMnSc2	on
vyráběly	vyrábět	k5eAaImAgInP	vyrábět
dřevěné	dřevěný	k2eAgInPc1d1	dřevěný
hřeby	hřeb	k1gInPc1	hřeb
<g/>
,	,	kIx,	,
brda	brdo	k1gNnPc1	brdo
ke	k	k7c3	k
tkalcovským	tkalcovský	k2eAgInPc3d1	tkalcovský
stavům	stav	k1gInPc3	stav
<g/>
,	,	kIx,	,
troubele	troubel	k1gFnPc1	troubel
k	k	k7c3	k
dýmkám	dýmka	k1gFnPc3	dýmka
<g/>
,	,	kIx,	,
používal	používat	k5eAaImAgInS	používat
se	se	k3xPyFc4	se
i	i	k9	i
k	k	k7c3	k
drobnému	drobný	k2eAgNnSc3d1	drobné
soustružení	soustružení	k1gNnSc3	soustružení
</s>
</p>
<p>
<s>
zimostráz	zimostráz	k1gInSc1	zimostráz
–	–	k?	–
velmi	velmi	k6eAd1	velmi
těžké	těžký	k2eAgNnSc1d1	těžké
<g/>
,	,	kIx,	,
tvrdé	tvrdý	k2eAgNnSc1d1	tvrdé
a	a	k8xC	a
homogenní	homogenní	k2eAgNnSc1d1	homogenní
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
používané	používaný	k2eAgNnSc1d1	používané
pro	pro	k7c4	pro
účely	účel	k1gInPc4	účel
xylografie	xylografie	k1gFnSc2	xylografie
<g/>
,	,	kIx,	,
vykládání	vykládání	k1gNnSc2	vykládání
<g/>
,	,	kIx,	,
intarzie	intarzie	k1gFnSc2	intarzie
<g/>
,	,	kIx,	,
flétny	flétna	k1gFnSc2	flétna
<g/>
,	,	kIx,	,
rukojeti	rukojeť	k1gFnSc3	rukojeť
pravítka	pravítko	k1gNnSc2	pravítko
<g/>
,	,	kIx,	,
sošky	soška	k1gFnSc2	soška
</s>
</p>
<p>
<s>
jeřáb	jeřáb	k1gMnSc1	jeřáb
–	–	k?	–
těžké	těžký	k2eAgNnSc1d1	těžké
tvrdé	tvrdý	k2eAgNnSc1d1	tvrdé
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
využívá	využívat	k5eAaPmIp3nS	využívat
se	se	k3xPyFc4	se
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
fléten	flétna	k1gFnPc2	flétna
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
zimolez	zimolez	k1gInSc4	zimolez
<g/>
:	:	kIx,	:
dřevěné	dřevěný	k2eAgInPc4d1	dřevěný
hřeby	hřeb	k1gInPc4	hřeb
<g/>
,	,	kIx,	,
tkalcovské	tkalcovský	k2eAgInPc4d1	tkalcovský
člunky	člunek	k1gInPc4	člunek
<g/>
,	,	kIx,	,
šrouby	šroub	k1gInPc4	šroub
<g/>
,	,	kIx,	,
válce	válec	k1gInPc4	válec
mandlů	mandl	k1gInPc2	mandl
<g/>
,	,	kIx,	,
řeznické	řeznický	k2eAgFnSc2d1	Řeznická
špalkyJehličnany	špalkyJehličnana	k1gFnSc2	špalkyJehličnana
</s>
</p>
<p>
<s>
tis	tis	k1gInSc4	tis
červený	červený	k2eAgInSc4d1	červený
–	–	k?	–
nejhustší	hustý	k2eAgNnSc1d3	nejhustší
dřevo	dřevo	k1gNnSc1	dřevo
ze	z	k7c2	z
všech	všecek	k3xTgInPc2	všecek
našich	náš	k3xOp1gInPc2	náš
stromů	strom	k1gInPc2	strom
<g/>
,	,	kIx,	,
zároveň	zároveň	k6eAd1	zároveň
náš	náš	k3xOp1gInSc1	náš
nejtvrdší	tvrdý	k2eAgInSc1d3	nejtvrdší
jehličnan	jehličnan	k1gInSc1	jehličnan
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
již	již	k6eAd1	již
prakticky	prakticky	k6eAd1	prakticky
nedostupný	dostupný	k2eNgInSc1d1	nedostupný
<g/>
,	,	kIx,	,
dřevo	dřevo	k1gNnSc1	dřevo
bylo	být	k5eAaImAgNnS	být
v	v	k7c6	v
minulosti	minulost	k1gFnSc6	minulost
vysoce	vysoce	k6eAd1	vysoce
ceněné	ceněný	k2eAgInPc1d1	ceněný
jako	jako	k8xC	jako
materiál	materiál	k1gInSc1	materiál
ideální	ideální	k2eAgInSc1d1	ideální
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
luků	luk	k1gInPc2	luk
(	(	kIx(	(
<g/>
z	z	k7c2	z
toho	ten	k3xDgInSc2	ten
důvodu	důvod	k1gInSc2	důvod
a	a	k8xC	a
kvůli	kvůli	k7c3	kvůli
otravám	otrava	k1gFnPc3	otrava
dobytka	dobytek	k1gInSc2	dobytek
byly	být	k5eAaImAgInP	být
vzrostlé	vzrostlý	k2eAgInPc1d1	vzrostlý
tisy	tis	k1gInPc1	tis
téměř	téměř	k6eAd1	téměř
vyhubeny	vyhuben	k2eAgInPc1d1	vyhuben
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
příležitostně	příležitostně	k6eAd1	příležitostně
se	se	k3xPyFc4	se
tisové	tisový	k2eAgNnSc1d1	Tisové
dřevo	dřevo	k1gNnSc1	dřevo
používalo	používat	k5eAaImAgNnS	používat
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
pravítek	pravítko	k1gNnPc2	pravítko
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
luxusního	luxusní	k2eAgInSc2d1	luxusní
nábytku	nábytek	k1gInSc2	nábytek
<g/>
;	;	kIx,	;
obsah	obsah	k1gInSc1	obsah
jedu	jed	k1gInSc2	jed
zvyšuje	zvyšovat	k5eAaImIp3nS	zvyšovat
odolnost	odolnost	k1gFnSc4	odolnost
proti	proti	k7c3	proti
škůdcům	škůdce	k1gMnPc3	škůdce
</s>
</p>
<p>
<s>
modřín	modřín	k1gInSc1	modřín
–	–	k?	–
náš	náš	k3xOp1gInSc4	náš
nejtvrdší	tvrdý	k2eAgInSc4d3	nejtvrdší
běžně	běžně	k6eAd1	běžně
dostupný	dostupný	k2eAgInSc4d1	dostupný
jehličnan	jehličnan	k1gInSc4	jehličnan
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
na	na	k7c4	na
výrobu	výroba	k1gFnSc4	výroba
šindelů	šindel	k1gInPc2	šindel
<g/>
,	,	kIx,	,
schodišť	schodiště	k1gNnPc2	schodiště
<g/>
,	,	kIx,	,
zábradlí	zábradlí	k1gNnSc2	zábradlí
<g/>
,	,	kIx,	,
obkladů	obklad	k1gInPc2	obklad
a	a	k8xC	a
na	na	k7c4	na
nábytek	nábytek	k1gInSc4	nábytek
<g/>
,	,	kIx,	,
celkově	celkově	k6eAd1	celkově
má	mít	k5eAaImIp3nS	mít
podobné	podobný	k2eAgNnSc1d1	podobné
využití	využití	k1gNnSc1	využití
jako	jako	k8xC	jako
borovice	borovice	k1gFnSc1	borovice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
snáze	snadno	k6eAd2	snadno
se	se	k3xPyFc4	se
obrábí	obrábět	k5eAaImIp3nS	obrábět
<g/>
;	;	kIx,	;
dříve	dříve	k6eAd2	dříve
používán	používat	k5eAaImNgInS	používat
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
vodovodních	vodovodní	k2eAgNnPc2d1	vodovodní
potrubí	potrubí	k1gNnPc2	potrubí
</s>
</p>
<p>
<s>
borovice	borovice	k1gFnSc1	borovice
–	–	k?	–
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
našim	náš	k3xOp1gInPc3	náš
tvrdším	tvrdý	k2eAgInPc3d2	tvrdší
jehličnanům	jehličnan	k1gInPc3	jehličnan
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
je	být	k5eAaImIp3nS	být
dřevo	dřevo	k1gNnSc1	dřevo
relativně	relativně	k6eAd1	relativně
měkké	měkký	k2eAgNnSc1d1	měkké
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
odolné	odolný	k2eAgFnPc1d1	odolná
<g/>
;	;	kIx,	;
oblíbené	oblíbený	k2eAgFnPc1d1	oblíbená
v	v	k7c4	v
nábytkářství	nábytkářství	k1gNnSc4	nábytkářství
pro	pro	k7c4	pro
svůj	svůj	k3xOyFgInSc4	svůj
odstín	odstín	k1gInSc4	odstín
<g/>
,	,	kIx,	,
používané	používaný	k2eAgFnPc4d1	používaná
na	na	k7c4	na
okna	okno	k1gNnPc4	okno
<g/>
,	,	kIx,	,
dveře	dveře	k1gFnPc1	dveře
<g/>
,	,	kIx,	,
trámy	trám	k1gInPc1	trám
<g/>
,	,	kIx,	,
pražce	pražec	k1gInPc1	pražec
<g/>
;	;	kIx,	;
obrábění	obrábění	k1gNnSc1	obrábění
komplikuje	komplikovat	k5eAaBmIp3nS	komplikovat
vysoký	vysoký	k2eAgInSc4d1	vysoký
obsah	obsah	k1gInSc4	obsah
pryskyřice	pryskyřice	k1gFnSc1	pryskyřice
</s>
</p>
<p>
<s>
jalovec	jalovec	k1gInSc1	jalovec
–	–	k?	–
uzení	uzení	k1gNnSc1	uzení
(	(	kIx(	(
<g/>
především	především	k9	především
v	v	k7c6	v
závěru	závěr	k1gInSc6	závěr
procesu	proces	k1gInSc2	proces
pro	pro	k7c4	pro
aroma	aroma	k1gNnSc4	aroma
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
====	====	k?	====
Měkká	měkký	k2eAgNnPc1d1	měkké
dřeva	dřevo	k1gNnPc1	dřevo
====	====	k?	====
</s>
</p>
<p>
<s>
Listnaté	listnatý	k2eAgInPc1d1	listnatý
stromy	strom	k1gInPc1	strom
</s>
</p>
<p>
<s>
topol	topol	k1gInSc1	topol
–	–	k?	–
měkké	měkký	k2eAgNnSc1d1	měkké
řídké	řídký	k2eAgNnSc1d1	řídké
dřevo	dřevo	k1gNnSc1	dřevo
(	(	kIx(	(
<g/>
topol	topol	k1gInSc1	topol
velmi	velmi	k6eAd1	velmi
rychle	rychle	k6eAd1	rychle
roste	růst	k5eAaImIp3nS	růst
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
porézní	porézní	k2eAgFnSc1d1	porézní
<g/>
,	,	kIx,	,
čerstvé	čerstvý	k2eAgNnSc1d1	čerstvé
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
mnoho	mnoho	k4c4	mnoho
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
suché	suchý	k2eAgFnSc6d1	suchá
dobře	dobře	k6eAd1	dobře
saje	sát	k5eAaImIp3nS	sát
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
má	mít	k5eAaImIp3nS	mít
málou	málý	k2eAgFnSc4d1	málý
výhřevnost	výhřevnost	k1gFnSc4	výhřevnost
<g/>
,	,	kIx,	,
využití	využití	k1gNnSc4	využití
není	být	k5eNaImIp3nS	být
široké	široký	k2eAgNnSc1d1	široké
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
při	při	k7c6	při
výrobě	výroba	k1gFnSc6	výroba
dýhy	dýha	k1gFnSc2	dýha
<g/>
,	,	kIx,	,
překližek	překližka	k1gFnPc2	překližka
<g/>
,	,	kIx,	,
zápalek	zápalka	k1gFnPc2	zápalka
<g/>
,	,	kIx,	,
celulózy	celulóza	k1gFnSc2	celulóza
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
dlabaných	dlabaný	k2eAgInPc2d1	dlabaný
okřínů	okřín	k1gInPc2	okřín
a	a	k8xC	a
dřeváků	dřevák	k1gInPc2	dřevák
</s>
</p>
<p>
<s>
osika	osika	k1gFnSc1	osika
–	–	k?	–
měkké	měkký	k2eAgNnSc4d1	měkké
dřevo	dřevo	k1gNnSc4	dřevo
<g/>
,	,	kIx,	,
po	po	k7c6	po
vyschnutí	vyschnutí	k1gNnSc6	vyschnutí
lehké	lehký	k2eAgFnPc1d1	lehká
<g/>
,	,	kIx,	,
v	v	k7c6	v
obráběcích	obráběcí	k2eAgInPc6d1	obráběcí
strojích	stroj	k1gInPc6	stroj
se	se	k3xPyFc4	se
netřepí	třepit	k5eNaImIp3nS	třepit
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
se	se	k3xPyFc4	se
využívá	využívat	k5eAaImIp3nS	využívat
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
zápalek	zápalka	k1gFnPc2	zápalka
<g/>
,	,	kIx,	,
tenké	tenký	k2eAgInPc4d1	tenký
proužky	proužek	k1gInPc4	proužek
pak	pak	k6eAd1	pak
v	v	k7c6	v
košíkářství	košíkářství	k1gNnSc6	košíkářství
</s>
</p>
<p>
<s>
lípa	lípa	k1gFnSc1	lípa
–	–	k?	–
měkké	měkký	k2eAgFnPc4d1	měkká
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
poměrně	poměrně	k6eAd1	poměrně
pevné	pevný	k2eAgNnSc1d1	pevné
dřevo	dřevo	k1gNnSc1	dřevo
oblíbené	oblíbený	k2eAgNnSc1d1	oblíbené
v	v	k7c6	v
řezbářství	řezbářství	k1gNnSc6	řezbářství
<g/>
,	,	kIx,	,
náchylné	náchylný	k2eAgFnSc2d1	náchylná
k	k	k7c3	k
napadení	napadení	k1gNnSc3	napadení
červotočem	červotoč	k1gMnSc7	červotoč
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
lipové	lipový	k2eAgNnSc1d1	lipové
dřevo	dřevo	k1gNnSc1	dřevo
hojně	hojně	k6eAd1	hojně
používalo	používat	k5eAaImAgNnS	používat
v	v	k7c6	v
modelářství	modelářství	k1gNnSc6	modelářství
a	a	k8xC	a
trvanlivé	trvanlivý	k2eAgNnSc1d1	trvanlivé
lýko	lýko	k1gNnSc1	lýko
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
pletiv	pletivo	k1gNnPc2	pletivo
(	(	kIx(	(
<g/>
dnes	dnes	k6eAd1	dnes
ho	on	k3xPp3gMnSc4	on
nahradil	nahradit	k5eAaPmAgInS	nahradit
plast	plast	k1gInSc1	plast
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
olše	olše	k1gFnSc1	olše
–	–	k?	–
vlastnostmi	vlastnost	k1gFnPc7	vlastnost
a	a	k8xC	a
využitím	využití	k1gNnSc7	využití
se	se	k3xPyFc4	se
blíží	blížit	k5eAaImIp3nS	blížit
lípě	lípa	k1gFnSc3	lípa
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
typické	typický	k2eAgNnSc1d1	typické
výraznější	výrazný	k2eAgFnSc7d2	výraznější
barvou	barva	k1gFnSc7	barva
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
používalo	používat	k5eAaImAgNnS	používat
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
nádobí	nádobí	k1gNnSc2	nádobí
(	(	kIx(	(
<g/>
mísy	mísa	k1gFnPc4	mísa
<g/>
,	,	kIx,	,
necky	necky	k1gFnPc4	necky
<g/>
,	,	kIx,	,
okříny	okřín	k1gInPc4	okřín
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
trvanlivé	trvanlivý	k2eAgFnSc2d1	trvanlivá
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
<g/>
,	,	kIx,	,
po	po	k7c6	po
delší	dlouhý	k2eAgFnSc6d2	delší
době	doba	k1gFnSc6	doba
pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
zčerná	zčernat	k5eAaPmIp3nS	zčernat
a	a	k8xC	a
zkamení	zkamenit	k5eAaPmIp3nS	zkamenit
<g/>
,	,	kIx,	,
olši	olše	k1gFnSc4	olše
je	být	k5eAaImIp3nS	být
možné	možný	k2eAgNnSc1d1	možné
použít	použít	k5eAaPmF	použít
k	k	k7c3	k
uzení	uzení	k1gNnSc3	uzení
</s>
</p>
<p>
<s>
bříza	bříza	k1gFnSc1	bříza
–	–	k?	–
dřevo	dřevo	k1gNnSc1	dřevo
s	s	k7c7	s
nízkou	nízký	k2eAgFnSc7d1	nízká
odolností	odolnost	k1gFnSc7	odolnost
proti	proti	k7c3	proti
vlhkosti	vlhkost	k1gFnSc3	vlhkost
a	a	k8xC	a
změnám	změna	k1gFnPc3	změna
počasí	počasí	k1gNnSc2	počasí
<g/>
,	,	kIx,	,
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
ale	ale	k9	ale
suší	sušit	k5eAaImIp3nS	sušit
i	i	k9	i
ve	v	k7c6	v
větších	veliký	k2eAgInPc6d2	veliký
průměrech	průměr	k1gInPc6	průměr
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
je	být	k5eAaImIp3nS	být
používané	používaný	k2eAgNnSc1d1	používané
pro	pro	k7c4	pro
soustružení	soustružení	k1gNnSc4	soustružení
uměleckých	umělecký	k2eAgInPc2d1	umělecký
a	a	k8xC	a
domácích	domácí	k2eAgInPc2d1	domácí
předmětů	předmět	k1gInPc2	předmět
(	(	kIx(	(
<g/>
kořenky	kořenka	k1gFnPc1	kořenka
<g/>
,	,	kIx,	,
ozdobné	ozdobný	k2eAgFnPc1d1	ozdobná
vázy	váza	k1gFnPc1	váza
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
výroba	výroba	k1gFnSc1	výroba
košťat	koště	k1gNnPc2	koště
(	(	kIx(	(
<g/>
větvičky	větvička	k1gFnSc2	větvička
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
velmi	velmi	k6eAd1	velmi
vhodné	vhodný	k2eAgNnSc1d1	vhodné
je	být	k5eAaImIp3nS	být
k	k	k7c3	k
topení	topení	k1gNnSc3	topení
<g/>
,	,	kIx,	,
hoří	hořet	k5eAaImIp3nS	hořet
i	i	k9	i
syrové	syrový	k2eAgNnSc4d1	syrové
</s>
</p>
<p>
<s>
vrba	vrba	k1gFnSc1	vrba
–	–	k?	–
měkké	měkký	k2eAgNnSc4d1	měkké
dřevo	dřevo	k1gNnSc4	dřevo
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
nemá	mít	k5eNaImIp3nS	mít
valného	valný	k2eAgNnSc2d1	Valné
využití	využití	k1gNnSc2	využití
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
používalo	používat	k5eAaImAgNnS	používat
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
nebyly	být	k5eNaImAgFnP	být
vysoké	vysoký	k2eAgInPc4d1	vysoký
nároky	nárok	k1gInPc4	nárok
na	na	k7c4	na
pevnost	pevnost	k1gFnSc4	pevnost
a	a	k8xC	a
tvrdost	tvrdost	k1gFnSc4	tvrdost
(	(	kIx(	(
<g/>
dlabané	dlabaný	k2eAgFnPc1d1	dlabaná
nádoby	nádoba	k1gFnPc1	nádoba
<g/>
,	,	kIx,	,
násady	násada	k1gFnPc1	násada
lehčího	lehký	k2eAgNnSc2d2	lehčí
nářadí	nářadí	k1gNnSc2	nářadí
apod.	apod.	kA	apod.
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
tradičně	tradičně	k6eAd1	tradičně
se	se	k3xPyFc4	se
vrbové	vrbový	k2eAgInPc1d1	vrbový
pruty	prut	k1gInPc1	prut
používají	používat	k5eAaImIp3nP	používat
k	k	k7c3	k
pletení	pletení	k1gNnSc3	pletení
pomlázek	pomlázka	k1gFnPc2	pomlázka
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
hlavně	hlavně	k9	hlavně
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
košíků	košík	k1gInPc2	košík
a	a	k8xC	a
košťat	koště	k1gNnPc2	koště
čarodějnic	čarodějnice	k1gFnPc2	čarodějnice
</s>
</p>
<p>
<s>
líska	líska	k1gFnSc1	líska
obecná	obecná	k1gFnSc1	obecná
–	–	k?	–
měkké	měkký	k2eAgNnSc1d1	měkké
pružné	pružný	k2eAgNnSc1d1	pružné
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
bez	bez	k7c2	bez
průmyslového	průmyslový	k2eAgNnSc2d1	průmyslové
využití	využití	k1gNnSc2	využití
<g/>
,	,	kIx,	,
dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
používaly	používat	k5eAaImAgInP	používat
především	především	k9	především
pařezové	pařezový	k2eAgInPc1d1	pařezový
výmladky	výmladek	k1gInPc1	výmladek
(	(	kIx(	(
<g/>
pruty	prut	k1gInPc1	prut
<g/>
)	)	kIx)	)
jako	jako	k8xC	jako
pletiva	pletivo	k1gNnSc2	pletivo
k	k	k7c3	k
výplni	výplň	k1gFnSc3	výplň
příhrad	příhrada	k1gFnPc2	příhrada
<g/>
,	,	kIx,	,
k	k	k7c3	k
pletení	pletení	k1gNnSc3	pletení
plotů	plot	k1gInPc2	plot
<g/>
,	,	kIx,	,
v	v	k7c6	v
košíkářství	košíkářství	k1gNnSc6	košíkářství
<g/>
,	,	kIx,	,
ke	k	k7c3	k
krytí	krytí	k1gNnSc3	krytí
selských	selský	k2eAgNnPc2d1	selské
stavení	stavení	k1gNnPc2	stavení
<g/>
,	,	kIx,	,
polních	polní	k2eAgFnPc2d1	polní
stodol	stodola	k1gFnPc2	stodola
a	a	k8xC	a
k	k	k7c3	k
trestání	trestání	k1gNnSc3	trestání
dětí	dítě	k1gFnPc2	dítě
</s>
</p>
<p>
<s>
kaštan	kaštan	k1gInSc1	kaštan
–	–	k?	–
dřevo	dřevo	k1gNnSc1	dřevo
charakterem	charakter	k1gInSc7	charakter
podobné	podobný	k2eAgFnSc2d1	podobná
dubu	dub	k1gInSc3	dub
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
měkčí	měkký	k2eAgInPc1d2	měkčí
<g/>
,	,	kIx,	,
vhodné	vhodný	k2eAgInPc1d1	vhodný
k	k	k7c3	k
soustružení	soustružení	k1gNnSc3	soustružení
<g/>
,	,	kIx,	,
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
ke	k	k7c3	k
konstrukcím	konstrukce	k1gFnPc3	konstrukce
schodišť	schodiště	k1gNnPc2	schodiště
<g/>
,	,	kIx,	,
výrobě	výroba	k1gFnSc3	výroba
sudů	sud	k1gInPc2	sud
na	na	k7c6	na
víno	víno	k1gNnSc4	víno
<g/>
,	,	kIx,	,
kuchyňských	kuchyňský	k2eAgInPc2d1	kuchyňský
nástrojů	nástroj	k1gInPc2	nástroj
<g/>
,	,	kIx,	,
rakví	rakev	k1gFnPc2	rakev
a	a	k8xC	a
pro	pro	k7c4	pro
venkovní	venkovní	k2eAgInPc4d1	venkovní
účelyJehličnany	účelyJehličnan	k1gInPc4	účelyJehličnan
</s>
</p>
<p>
<s>
smrk	smrk	k1gInSc4	smrk
–	–	k?	–
měkké	měkký	k2eAgFnSc2d1	měkká
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
relativně	relativně	k6eAd1	relativně
pevné	pevný	k2eAgNnSc1d1	pevné
a	a	k8xC	a
pružné	pružný	k2eAgNnSc1d1	pružné
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
dnes	dnes	k6eAd1	dnes
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
nejdostupnějším	dostupný	k2eAgNnPc3d3	nejdostupnější
a	a	k8xC	a
široce	široko	k6eAd1	široko
používaným	používaný	k2eAgNnSc7d1	používané
v	v	k7c4	v
tesařství	tesařství	k1gNnSc4	tesařství
i	i	k8xC	i
truhlářství	truhlářství	k1gNnSc4	truhlářství
<g/>
;	;	kIx,	;
používá	používat	k5eAaImIp3nS	používat
se	se	k3xPyFc4	se
i	i	k9	i
na	na	k7c4	na
výdřevy	výdřeva	k1gFnPc4	výdřeva
v	v	k7c6	v
dolech	dol	k1gInPc6	dol
<g/>
,	,	kIx,	,
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
papíru	papír	k1gInSc2	papír
<g/>
,	,	kIx,	,
dřevité	dřevitý	k2eAgFnSc2d1	dřevitá
vlny	vlna	k1gFnSc2	vlna
<g/>
,	,	kIx,	,
buničiny	buničina	k1gFnSc2	buničina
<g/>
,	,	kIx,	,
některých	některý	k3yIgInPc2	některý
hudebních	hudební	k2eAgInPc2d1	hudební
nástrojů	nástroj	k1gInPc2	nástroj
<g />
.	.	kIx.	.
</s>
<s>
<g/>
,	,	kIx,	,
hlavně	hlavně	k9	hlavně
houslí	housle	k1gFnPc2	housle
<g/>
,	,	kIx,	,
tzv.	tzv.	kA	tzv.
rezonanční	rezonanční	k2eAgInSc1d1	rezonanční
smrk	smrk	k1gInSc1	smrk
(	(	kIx(	(
<g/>
tj.	tj.	kA	tj.
dřevo	dřevo	k1gNnSc1	dřevo
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
na	na	k7c6	na
smrcích	smrk	k1gInPc6	smrk
narůstá	narůstat	k5eAaImIp3nS	narůstat
po	po	k7c6	po
120	[number]	k4	120
věku	věk	k1gInSc6	věk
a	a	k8xC	a
splňuje	splňovat	k5eAaImIp3nS	splňovat
řadu	řada	k1gFnSc4	řada
přísných	přísný	k2eAgFnPc2d1	přísná
podmínek	podmínka	k1gFnPc2	podmínka
<g/>
:	:	kIx,	:
pochází	pocházet	k5eAaImIp3nS	pocházet
ze	z	k7c2	z
smrků	smrk	k1gInPc2	smrk
z	z	k7c2	z
horských	horský	k2eAgFnPc2d1	horská
a	a	k8xC	a
podhorských	podhorský	k2eAgFnPc2d1	podhorská
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc1	který
stojí	stát	k5eAaImIp3nP	stát
v	v	k7c6	v
centru	centrum	k1gNnSc6	centrum
lesa	les	k1gInSc2	les
<g/>
,	,	kIx,	,
nikoli	nikoli	k9	nikoli
u	u	k7c2	u
kraje	kraj	k1gInSc2	kraj
<g/>
,	,	kIx,	,
nejlépe	dobře	k6eAd3	dobře
na	na	k7c6	na
severním	severní	k2eAgInSc6d1	severní
svahu	svah	k1gInSc6	svah
<g/>
,	,	kIx,	,
dřevo	dřevo	k1gNnSc1	dřevo
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
suky	suk	k1gInPc7	suk
a	a	k8xC	a
letokruhy	letokruh	k1gInPc7	letokruh
jsou	být	k5eAaImIp3nP	být
pravidelné	pravidelný	k2eAgInPc1d1	pravidelný
a	a	k8xC	a
husté	hustý	k2eAgInPc1d1	hustý
<g/>
,	,	kIx,	,
maximálně	maximálně	k6eAd1	maximálně
2,5	[number]	k4	2,5
mm	mm	kA	mm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
náchylné	náchylný	k2eAgFnSc2d1	náchylná
k	k	k7c3	k
napadení	napadení	k1gNnSc3	napadení
červotočem	červotoč	k1gMnSc7	červotoč
</s>
</p>
<p>
<s>
jedle	jedle	k1gFnSc1	jedle
–	–	k?	–
dříve	dříve	k6eAd2	dříve
používané	používaný	k2eAgFnSc2d1	používaná
k	k	k7c3	k
výrobě	výroba	k1gFnSc3	výroba
nábytku	nábytek	k1gInSc2	nábytek
<g/>
,	,	kIx,	,
lodní	lodní	k2eAgInPc1d1	lodní
stožáry	stožár	k1gInPc1	stožár
<g/>
,	,	kIx,	,
čluny	člun	k1gInPc1	člun
<g/>
,	,	kIx,	,
dnes	dnes	k6eAd1	dnes
špatně	špatně	k6eAd1	špatně
dostupné	dostupný	k2eAgInPc1d1	dostupný
(	(	kIx(	(
<g/>
vlivem	vlivem	k7c2	vlivem
exhalací	exhalace	k1gFnPc2	exhalace
klesl	klesnout	k5eAaPmAgInS	klesnout
podíl	podíl	k1gInSc1	podíl
v	v	k7c6	v
lesích	les	k1gInPc6	les
z	z	k7c2	z
15	[number]	k4	15
<g/>
%	%	kIx~	%
na	na	k7c4	na
1	[number]	k4	1
<g/>
%	%	kIx~	%
<g/>
,	,	kIx,	,
ze	z	k7c2	z
16	[number]	k4	16
největších	veliký	k2eAgFnPc2d3	veliký
jedlí	jedle	k1gFnPc2	jedle
uváděných	uváděný	k2eAgInPc2d1	uváděný
J.	J.	kA	J.
E.	E.	kA	E.
Chadtem	Chadt	k1gInSc7	Chadt
roku	rok	k1gInSc2	rok
1913	[number]	k4	1913
již	již	k6eAd1	již
neexistuje	existovat	k5eNaImIp3nS	existovat
žádná	žádný	k3yNgFnSc1	žádný
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
jinan	jinan	k1gInSc1	jinan
–	–	k?	–
v	v	k7c6	v
Českých	český	k2eAgFnPc6d1	Česká
zemích	zem	k1gFnPc6	zem
se	se	k3xPyFc4	se
pěstuje	pěstovat	k5eAaImIp3nS	pěstovat
jen	jen	k9	jen
krátce	krátce	k6eAd1	krátce
pro	pro	k7c4	pro
okrasné	okrasný	k2eAgInPc4d1	okrasný
účely	účel	k1gInPc4	účel
<g/>
,	,	kIx,	,
širší	široký	k2eAgNnSc4d2	širší
průmyslové	průmyslový	k2eAgNnSc4d1	průmyslové
využití	využití	k1gNnSc4	využití
nemá	mít	k5eNaImIp3nS	mít
<g/>
,	,	kIx,	,
dřevo	dřevo	k1gNnSc1	dřevo
je	být	k5eAaImIp3nS	být
ale	ale	k9	ale
měkké	měkký	k2eAgNnSc1d1	měkké
<g/>
,	,	kIx,	,
snadno	snadno	k6eAd1	snadno
se	se	k3xPyFc4	se
obrábí	obrábět	k5eAaImIp3nS	obrábět
a	a	k8xC	a
v	v	k7c6	v
menší	malý	k2eAgFnSc6d2	menší
míře	míra	k1gFnSc6	míra
se	se	k3xPyFc4	se
zpracovává	zpracovávat	k5eAaImIp3nS	zpracovávat
na	na	k7c4	na
dýhy	dýha	k1gFnPc4	dýha
a	a	k8xC	a
obkladyPomocí	obkladyPomoc	k1gFnSc7	obkladyPomoc
dřeva	dřevo	k1gNnSc2	dřevo
dubů	dub	k1gInPc2	dub
<g/>
,	,	kIx,	,
buků	buk	k1gInPc2	buk
a	a	k8xC	a
jedlí	jedlí	k1gNnSc6	jedlí
byly	být	k5eAaImAgInP	být
vytvořeny	vytvořit	k5eAaPmNgInP	vytvořit
tzv.	tzv.	kA	tzv.
letokruhové	letokruhový	k2eAgFnPc4d1	letokruhový
křivky	křivka	k1gFnPc4	křivka
pro	pro	k7c4	pro
střední	střední	k2eAgFnSc4d1	střední
Evropu	Evropa	k1gFnSc4	Evropa
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
slouží	sloužit	k5eAaImIp3nP	sloužit
k	k	k7c3	k
chronologickému	chronologický	k2eAgNnSc3d1	chronologické
datování	datování	k1gNnSc3	datování
historických	historický	k2eAgFnPc2d1	historická
památek	památka	k1gFnPc2	památka
a	a	k8xC	a
předmětů	předmět	k1gInPc2	předmět
vyrobených	vyrobený	k2eAgInPc2d1	vyrobený
ze	z	k7c2	z
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Stromy	strom	k1gInPc1	strom
stejného	stejný	k2eAgInSc2d1	stejný
druhu	druh	k1gInSc2	druh
ze	z	k7c2	z
stejného	stejný	k2eAgNnSc2d1	stejné
území	území	k1gNnSc2	území
totiž	totiž	k9	totiž
vykazují	vykazovat	k5eAaImIp3nP	vykazovat
velmi	velmi	k6eAd1	velmi
podobné	podobný	k2eAgFnPc4d1	podobná
letokruhové	letokruhový	k2eAgFnPc4d1	letokruhový
křivky	křivka	k1gFnPc4	křivka
(	(	kIx(	(
<g/>
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
mezi	mezi	k7c7	mezi
jednotlivými	jednotlivý	k2eAgInPc7d1	jednotlivý
letokruhy	letokruh	k1gInPc7	letokruh
vynesené	vynesený	k2eAgFnSc2d1	vynesená
do	do	k7c2	do
křivky	křivka	k1gFnSc2	křivka
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Díky	dík	k1gInPc1	dík
této	tento	k3xDgFnSc2	tento
vlastnosti	vlastnost	k1gFnSc2	vlastnost
mohly	moct	k5eAaImAgFnP	moct
být	být	k5eAaImF	být
složeny	složen	k2eAgFnPc1d1	složena
křivky	křivka	k1gFnPc1	křivka
jednotlivých	jednotlivý	k2eAgInPc2d1	jednotlivý
druhů	druh	k1gInPc2	druh
stromů	strom	k1gInPc2	strom
pro	pro	k7c4	pro
celá	celý	k2eAgNnPc4d1	celé
staletí	staletí	k1gNnPc4	staletí
a	a	k8xC	a
ty	ten	k3xDgInPc1	ten
zpětně	zpětně	k6eAd1	zpětně
využívány	využívat	k5eAaImNgInP	využívat
pro	pro	k7c4	pro
identifikaci	identifikace	k1gFnSc4	identifikace
stáří	stáří	k1gNnSc2	stáří
neznámého	známý	k2eNgNnSc2d1	neznámé
dřeva	dřevo	k1gNnSc2	dřevo
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
SVOBODA	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
Luboš	Luboš	k1gMnSc1	Luboš
<g/>
.	.	kIx.	.
</s>
<s>
Stavební	stavební	k2eAgFnPc1d1	stavební
hmoty	hmota	k1gFnPc1	hmota
(	(	kIx(	(
<g/>
volně	volně	k6eAd1	volně
dostupná	dostupný	k2eAgFnSc1d1	dostupná
elektronická	elektronický	k2eAgFnSc1d1	elektronická
kniha	kniha	k1gFnSc1	kniha
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
l.	l.	k?	l.
<g/>
]	]	kIx)	]
<g/>
:	:	kIx,	:
[	[	kIx(	[
<g/>
s.	s.	k?	s.
<g/>
n.	n.	k?	n.
<g/>
]	]	kIx)	]
<g/>
,	,	kIx,	,
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
978	[number]	k4	978
<g/>
-	-	kIx~	-
<g/>
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
260	[number]	k4	260
<g/>
-	-	kIx~	-
<g/>
4972	[number]	k4	4972
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
950	[number]	k4	950
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
xylém	xylém	k1gInSc1	xylém
</s>
</p>
<p>
<s>
řezivo	řezivo	k1gNnSc1	řezivo
</s>
</p>
<p>
<s>
letokruh	letokruh	k1gInSc1	letokruh
</s>
</p>
<p>
<s>
fládr	fládr	k1gInSc1	fládr
</s>
</p>
<p>
<s>
tvrdost	tvrdost	k1gFnSc1	tvrdost
dřeva	dřevo	k1gNnSc2	dřevo
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
dřevo	dřevo	k1gNnSc4	dřevo
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
dřevo	dřevo	k1gNnSc4	dřevo
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
<p>
<s>
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
<g/>
(	(	kIx(	(
<g/>
německy	německy	k6eAd1	německy
<g/>
)	)	kIx)	)
<g/>
Ústav	ústav	k1gInSc1	ústav
nauky	nauka	k1gFnSc2	nauka
o	o	k7c6	o
dřevě	dřevo	k1gNnSc6	dřevo
–	–	k?	–
Mendelova	Mendelův	k2eAgFnSc1d1	Mendelova
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
a	a	k8xC	a
lesnická	lesnický	k2eAgFnSc1d1	lesnická
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
</s>
</p>
<p>
<s>
Lexikon	lexikon	k1gNnSc1	lexikon
českých	český	k2eAgNnPc2d1	české
dřev	dřevo	k1gNnPc2	dřevo
–	–	k?	–
Mendelova	Mendelův	k2eAgFnSc1d1	Mendelova
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
a	a	k8xC	a
lesnická	lesnický	k2eAgFnSc1d1	lesnická
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
</s>
</p>
<p>
<s>
Lexikon	lexikon	k1gNnSc1	lexikon
tropických	tropický	k2eAgNnPc2d1	tropické
dřev	dřevo	k1gNnPc2	dřevo
–	–	k?	–
Mendelova	Mendelův	k2eAgFnSc1d1	Mendelova
zemědělská	zemědělský	k2eAgFnSc1d1	zemědělská
a	a	k8xC	a
lesnická	lesnický	k2eAgFnSc1d1	lesnická
univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Brně	Brno	k1gNnSc6	Brno
</s>
</p>
