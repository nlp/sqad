<s>
Inteligence	inteligence	k1gFnSc1
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
je	být	k5eAaImIp3nS
o	o	k7c4
schopnosti	schopnost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
O	o	k7c6
společenské	společenský	k2eAgFnSc6d1
třídě	třída	k1gFnSc6
pojednává	pojednávat	k5eAaImIp3nS
článek	článek	k1gInSc1
Inteligence	inteligence	k1gFnSc2
(	(	kIx(
<g/>
třída	třída	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Představa	představa	k1gFnSc1
lidského	lidský	k2eAgNnSc2d1
vnímání	vnímání	k1gNnSc2
rozděleného	rozdělený	k2eAgNnSc2d1
podle	podle	k7c2
jednotlivých	jednotlivý	k2eAgFnPc2d1
funkcí	funkce	k1gFnPc2
myšlení	myšlení	k1gNnSc2
podle	podle	k7c2
lékaře	lékař	k1gMnSc4
Roberta	Robert	k1gMnSc2
Fludda	Fludd	k1gMnSc2
z	z	k7c2
roku	rok	k1gInSc2
1619	#num#	k4
</s>
<s>
Inteligence	inteligence	k1gFnSc1
(	(	kIx(
<g/>
z	z	k7c2
lat.	lat.	k?
inter-legere	inter-legrat	k5eAaPmIp3nS
<g/>
,	,	kIx,
rozlišovat	rozlišovat	k5eAaImF
<g/>
,	,	kIx,
poznávat	poznávat	k5eAaImF
<g/>
,	,	kIx,
chápat	chápat	k5eAaImF
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
dispozice	dispozice	k1gFnSc1
pro	pro	k7c4
myšlení	myšlení	k1gNnSc4
<g/>
,	,	kIx,
učení	učení	k1gNnSc4
a	a	k8xC
adaptaci	adaptace	k1gFnSc4
a	a	k8xC
projevuje	projevovat	k5eAaImIp3nS
se	se	k3xPyFc4
intelektovým	intelektový	k2eAgInSc7d1
výkonem	výkon	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Spolu	spolu	k6eAd1
s	s	k7c7
osobností	osobnost	k1gFnSc7
tvoří	tvořit	k5eAaImIp3nS
zásadní	zásadní	k2eAgInSc4d1
zdroj	zdroj	k1gInSc4
individuálních	individuální	k2eAgInPc2d1
rozdílů	rozdíl	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přestože	přestože	k8xS
největším	veliký	k2eAgInSc7d3
faktorem	faktor	k1gInSc7
v	v	k7c6
inteligenci	inteligence	k1gFnSc6
zvířat	zvíře	k1gNnPc2
a	a	k8xC
člověka	člověk	k1gMnSc4
je	být	k5eAaImIp3nS
počet	počet	k1gInSc1
synapsí	synapse	k1gFnPc2
v	v	k7c6
mozku	mozek	k1gInSc6
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
,	,	kIx,
některé	některý	k3yIgFnPc1
známky	známka	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
s	s	k7c7
inteligentními	inteligentní	k2eAgFnPc7d1
bytostmi	bytost	k1gFnPc7
běžně	běžně	k6eAd1
spojujeme	spojovat	k5eAaImIp1nP
jako	jako	k9
učení	učení	k1gNnSc4
či	či	k8xC
sebereflexe	sebereflexe	k1gFnPc4
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
vykazovat	vykazovat	k5eAaImF
i	i	k9
dnes	dnes	k6eAd1
existující	existující	k2eAgInPc4d1
počítače	počítač	k1gInPc4
nebo	nebo	k8xC
organismy	organismus	k1gInPc4
vůbec	vůbec	k9
postrádající	postrádající	k2eAgFnSc7d1
centrální	centrální	k2eAgFnSc7d1
nervovou	nervový	k2eAgFnSc7d1
soustavu	soustava	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Nakolik	nakolik	k6eAd1
inteligence	inteligence	k1gFnSc1
skutečně	skutečně	k6eAd1
souvisí	souviset	k5eAaImIp3nS
s	s	k7c7
učením	učení	k1gNnSc7
<g/>
,	,	kIx,
vědomím	vědomí	k1gNnSc7
<g/>
,	,	kIx,
symbolickým	symbolický	k2eAgNnSc7d1
myšlením	myšlení	k1gNnSc7
<g/>
,	,	kIx,
sebeuvědomováním	sebeuvědomování	k1gNnSc7
nebo	nebo	k8xC
algoritmickým	algoritmický	k2eAgInSc7d1
systémem	systém	k1gInSc7
specifickým	specifický	k2eAgInSc7d1
pro	pro	k7c4
lidský	lidský	k2eAgInSc4d1
mozek	mozek	k1gInSc4
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
v	v	k7c6
rámci	rámec	k1gInSc6
filozofie	filozofie	k1gFnSc2
umělé	umělý	k2eAgFnSc2d1
inteligence	inteligence	k1gFnSc2
ovšem	ovšem	k9
stále	stále	k6eAd1
diskutovaná	diskutovaný	k2eAgFnSc1d1
otázka	otázka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
průběhu	průběh	k1gInSc6
studia	studio	k1gNnSc2
inteligence	inteligence	k1gFnSc2
vznikala	vznikat	k5eAaImAgFnS
řada	řada	k1gFnSc1
různých	různý	k2eAgFnPc2d1
teorií	teorie	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yRgFnPc4,k3yQgFnPc4
lze	lze	k6eAd1
rozdělit	rozdělit	k5eAaPmF
do	do	k7c2
čtyř	čtyři	k4xCgInPc2
hlavních	hlavní	k2eAgInPc2d1
směrů	směr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
psychometrické	psychometrický	k2eAgFnPc4d1
<g/>
,	,	kIx,
kognitivní	kognitivní	k2eAgFnPc4d1
<g/>
,	,	kIx,
sociální	sociální	k2eAgFnPc4d1
a	a	k8xC
fyziologické	fyziologický	k2eAgFnPc4d1
teorie	teorie	k1gFnPc4
inteligence	inteligence	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Počátky	počátek	k1gInPc4
výzkumu	výzkum	k1gInSc2
</s>
<s>
První	první	k4xOgInSc1
pokus	pokus	k1gInSc1
o	o	k7c4
zjišťování	zjišťování	k1gNnSc4
intelektových	intelektový	k2eAgFnPc2d1
schopností	schopnost	k1gFnPc2
podnikl	podniknout	k5eAaPmAgInS
Francis	Francis	k1gFnSc2
Galton	Galton	k1gInSc1
<g/>
,	,	kIx,
bratranec	bratranec	k1gMnSc1
Charlese	Charles	k1gMnSc2
Darwina	Darwin	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vycházel	vycházet	k5eAaImAgMnS
z	z	k7c2
předpokladu	předpoklad	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
inteligence	inteligence	k1gFnSc1
je	být	k5eAaImIp3nS
určena	určit	k5eAaPmNgFnS
přesností	přesnost	k1gFnSc7
smyslů	smysl	k1gInPc2
a	a	k8xC
vnímání	vnímání	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ačkoli	ačkoli	k8xS
se	se	k3xPyFc4
tato	tento	k3xDgFnSc1
teze	teze	k1gFnSc1
výzkumem	výzkum	k1gInSc7
nepotvrdila	potvrdit	k5eNaPmAgNnP
<g/>
,	,	kIx,
vytvořil	vytvořit	k5eAaPmAgMnS
pro	pro	k7c4
její	její	k3xOp3gInPc4
účely	účel	k1gInPc4
korelační	korelační	k2eAgInSc1d1
koeficient	koeficient	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
důležitý	důležitý	k2eAgInSc1d1
pro	pro	k7c4
další	další	k2eAgInSc4d1
výzkum	výzkum	k1gInSc4
(	(	kIx(
<g/>
nejen	nejen	k6eAd1
<g/>
)	)	kIx)
v	v	k7c6
oblasti	oblast	k1gFnSc6
psychologie	psychologie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sociologie	sociologie	k1gFnSc1
inteligence	inteligence	k1gFnSc2
</s>
<s>
Přestože	přestože	k8xS
v	v	k7c6
historii	historie	k1gFnSc6
existovala	existovat	k5eAaImAgFnS
tendence	tendence	k1gFnSc1
vyčleňovat	vyčleňovat	k5eAaImF
jednotlivé	jednotlivý	k2eAgInPc4d1
stupně	stupeň	k1gInPc4
inteligence	inteligence	k1gFnSc2
ve	v	k7c4
společnosti	společnost	k1gFnPc4
do	do	k7c2
jasně	jasně	k6eAd1
daných	daný	k2eAgFnPc2d1
skupin	skupina	k1gFnPc2
<g/>
,	,	kIx,
veřejné	veřejný	k2eAgInPc1d1
průzkumy	průzkum	k1gInPc1
prokazují	prokazovat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
její	její	k3xOp3gNnSc1
rozložení	rozložení	k1gNnSc1
více	hodně	k6eAd2
méně	málo	k6eAd2
odpovídá	odpovídat	k5eAaImIp3nS
normálnímu	normální	k2eAgInSc3d1
(	(	kIx(
<g/>
Gaussovu	Gaussův	k2eAgMnSc3d1
<g/>
)	)	kIx)
rozdělení	rozdělení	k1gNnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
Vlivem	vliv	k1gInSc7
sociální	sociální	k2eAgFnSc2d1
bubliny	bublina	k1gFnSc2
se	se	k3xPyFc4
ale	ale	k9
ve	v	k7c6
společnosti	společnost	k1gFnSc6
mohou	moct	k5eAaImIp3nP
přibližně	přibližně	k6eAd1
2	#num#	k4
%	%	kIx~
jedinců	jedinec	k1gMnPc2
s	s	k7c7
největším	veliký	k2eAgInSc7d3
potenciálem	potenciál	k1gInSc7
vyčlenit	vyčlenit	k5eAaPmF
do	do	k7c2
jakési	jakýsi	k3yIgFnSc2
menšiny	menšina	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
by	by	kYmCp3nS
podle	podle	k7c2
Moniky	Monika	k1gFnSc2
Stehlíkové	Stehlíková	k1gFnSc2
měla	mít	k5eAaImAgFnS
být	být	k5eAaImF
zastávána	zastáván	k2eAgFnSc1d1
<g/>
,	,	kIx,
tak	tak	k8xC,k8xS
jako	jako	k9
každá	každý	k3xTgFnSc1
přirozená	přirozený	k2eAgFnSc1d1
společenská	společenský	k2eAgFnSc1d1
menšina	menšina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
není	být	k5eNaImIp3nS
výchova	výchova	k1gFnSc1
vhodně	vhodně	k6eAd1
přizpůsobena	přizpůsoben	k2eAgFnSc1d1
<g/>
,	,	kIx,
mohou	moct	k5eAaImIp3nP
se	se	k3xPyFc4
proto	proto	k8xC
u	u	k7c2
některých	některý	k3yIgFnPc2
dětí	dítě	k1gFnPc2
s	s	k7c7
vrozeně	vrozeně	k6eAd1
vyšší	vysoký	k2eAgFnSc7d2
inteligencí	inteligence	k1gFnSc7
rozvíjet	rozvíjet	k5eAaImF
sociální	sociální	k2eAgFnSc2d1
fóbie	fóbie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Lidé	člověk	k1gMnPc1
se	s	k7c7
zvýšenou	zvýšený	k2eAgFnSc7d1
inteligencí	inteligence	k1gFnSc7
se	se	k3xPyFc4
vyznačují	vyznačovat	k5eAaImIp3nP
především	především	k6eAd1
divergentním	divergentní	k2eAgInSc7d1
způsobem	způsob	k1gInSc7
myšlení	myšlení	k1gNnSc2
<g/>
,	,	kIx,
vyšší	vysoký	k2eAgFnSc7d2
mentální	mentální	k2eAgFnSc7d1
flexibilitou	flexibilita	k1gFnSc7
<g/>
,	,	kIx,
snaží	snažit	k5eAaImIp3nS
se	se	k3xPyFc4
řešit	řešit	k5eAaImF
problémy	problém	k1gInPc4
do	do	k7c2
co	co	k9
nejširších	široký	k2eAgInPc2d3
důsledků	důsledek	k1gInPc2
<g/>
,	,	kIx,
tíhnou	tíhnout	k5eAaImIp3nP
k	k	k7c3
většímu	veliký	k2eAgInSc3d2
perfekcionismu	perfekcionismus	k1gInSc3
jak	jak	k8xC,k8xS
vůči	vůči	k7c3
sobě	se	k3xPyFc3
<g/>
,	,	kIx,
tak	tak	k8xC,k8xS
vůči	vůči	k7c3
ostatním	ostatní	k2eAgMnPc3d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bývají	bývat	k5eAaImIp3nP
často	často	k6eAd1
multipotenciální	multipotenciální	k2eAgMnPc1d1
(	(	kIx(
<g/>
renesanční	renesanční	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
<g/>
)	)	kIx)
a	a	k8xC
jsou	být	k5eAaImIp3nP
schopni	schopen	k2eAgMnPc1d1
lépe	dobře	k6eAd2
uvažovat	uvažovat	k5eAaImF
o	o	k7c6
věcech	věc	k1gFnPc6
abstraktně	abstraktně	k6eAd1
<g/>
,	,	kIx,
číst	číst	k5eAaImF
lépe	dobře	k6eAd2
nejen	nejen	k6eAd1
struktury	struktura	k1gFnPc1
<g/>
,	,	kIx,
ale	ale	k8xC
i	i	k9
metastruktury	metastruktura	k1gFnPc4
<g/>
,	,	kIx,
komplexnější	komplexní	k2eAgNnSc4d2
spojení	spojení	k1gNnSc4
mezi	mezi	k7c7
fakty	faktum	k1gNnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
S	s	k7c7
inteligencí	inteligence	k1gFnSc7
je	být	k5eAaImIp3nS
také	také	k9
spojena	spojit	k5eAaPmNgFnS
schopnost	schopnost	k1gFnSc1
sebeovládání	sebeovládání	k1gNnSc2
ve	v	k7c4
vlastní	vlastní	k2eAgInSc4d1
prospěch	prospěch	k1gInSc4
(	(	kIx(
<g/>
plánování	plánování	k1gNnSc1
úkolů	úkol	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Psychologie	psychologie	k1gFnSc1
inteligence	inteligence	k1gFnSc2
</s>
<s>
Inteligence	inteligence	k1gFnSc1
podle	podle	k7c2
terapeutky	terapeutka	k1gFnSc2
Mgr.	Mgr.	kA
Moniky	Monika	k1gFnSc2
Stehlíkové	Stehlíková	k1gFnSc2
<g/>
,	,	kIx,
autorky	autorka	k1gFnSc2
knihy	kniha	k1gFnSc2
Život	život	k1gInSc4
s	s	k7c7
vysokou	vysoký	k2eAgFnSc7d1
inteligencí	inteligence	k1gFnSc7
<g/>
,	,	kIx,
předznamenává	předznamenávat	k5eAaImIp3nS
vyšší	vysoký	k2eAgFnSc1d2
pravděpodobnost	pravděpodobnost	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
jedinec	jedinec	k1gMnSc1
úspěšně	úspěšně	k6eAd1
dokončí	dokončit	k5eAaPmIp3nS
vzdělání	vzdělání	k1gNnSc4
<g/>
,	,	kIx,
nalezne	naleznout	k5eAaPmIp3nS,k5eAaBmIp3nS
práci	práce	k1gFnSc4
<g/>
,	,	kIx,
bude	být	k5eAaImBp3nS
žít	žít	k5eAaImF
zdravěji	zdravě	k6eAd2
a	a	k8xC
vytvoří	vytvořit	k5eAaPmIp3nS
si	se	k3xPyFc3
silnější	silný	k2eAgMnSc1d2
mezilidské	mezilidský	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
<g/>
,	,	kIx,
pro	pro	k7c4
jistou	jistý	k2eAgFnSc4d1
menšinu	menšina	k1gFnSc4
nadprůměrně	nadprůměrně	k6eAd1
inteligentních	inteligentní	k2eAgMnPc2d1
jedinců	jedinec	k1gMnPc2
však	však	k9
naopak	naopak	k6eAd1
znamená	znamenat	k5eAaImIp3nS
riziko	riziko	k1gNnSc4
společenských	společenský	k2eAgInPc2d1
problémů	problém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
značné	značný	k2eAgFnSc2d1
míry	míra	k1gFnSc2
jsou	být	k5eAaImIp3nP
tyto	tento	k3xDgFnPc1
ztížené	ztížený	k2eAgFnPc1d1
podmínky	podmínka	k1gFnPc1
způsobeny	způsoben	k2eAgInPc4d1
předsudky	předsudek	k1gInPc4
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
potenciální	potenciální	k2eAgNnSc1d1
psychologické	psychologický	k2eAgInPc1d1
problémy	problém	k1gInPc1
nemusí	muset	k5eNaImIp3nP
vnímat	vnímat	k5eAaImF
jako	jako	k9
vážné	vážný	k2eAgNnSc4d1
riziko	riziko	k1gNnSc4
a	a	k8xC
věří	věřit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
s	s	k7c7
nimi	on	k3xPp3gMnPc7
inteligentní	inteligentní	k2eAgMnSc1d1
člověk	člověk	k1gMnSc1
vypořádá	vypořádat	k5eAaPmIp3nS
sám	sám	k3xTgMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zejména	zejména	k9
ženy	žena	k1gFnPc1
kolem	kolem	k7c2
padesátky	padesátka	k1gFnSc2
(	(	kIx(
<g/>
nebo	nebo	k8xC
v	v	k7c6
pubertě	puberta	k1gFnSc6
<g/>
)	)	kIx)
mají	mít	k5eAaImIp3nP
tendenci	tendence	k1gFnSc4
známky	známka	k1gFnSc2
své	svůj	k3xOyFgFnSc2
inteligence	inteligence	k1gFnSc2
potlačovat	potlačovat	k5eAaImF
jako	jako	k8xC,k8xS
společensky	společensky	k6eAd1
omezující	omezující	k2eAgFnPc1d1
a	a	k8xC
předpoklad	předpoklad	k1gInSc1
úspěšného	úspěšný	k2eAgInSc2d1
života	život	k1gInSc2
ostatních	ostatní	k2eAgMnPc2d1
lidí	člověk	k1gMnPc2
v	v	k7c6
nich	on	k3xPp3gFnPc6
mohou	moct	k5eAaImIp3nP
vyvolat	vyvolat	k5eAaPmF
pocit	pocit	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
žijí	žít	k5eAaImIp3nP
život	život	k1gInSc4
<g/>
,	,	kIx,
do	do	k7c2
kterého	který	k3yIgInSc2,k3yRgInSc2,k3yQgInSc2
byly	být	k5eAaImAgFnP
dotlačeny	dotlačen	k2eAgFnPc1d1
<g/>
,	,	kIx,
a	a	k8xC
snaží	snažit	k5eAaImIp3nS
se	se	k3xPyFc4
přizpůsobovat	přizpůsobovat	k5eAaImF
se	se	k3xPyFc4
většinové	většinový	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
navzdory	navzdory	k7c3
své	svůj	k3xOyFgFnSc3
přirozené	přirozený	k2eAgFnSc3d1
pozici	pozice	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s>
Zároveň	zároveň	k6eAd1
inteligentnější	inteligentní	k2eAgMnPc1d2
lidé	člověk	k1gMnPc1
dokáží	dokázat	k5eAaPmIp3nP
lépe	dobře	k6eAd2
vnímat	vnímat	k5eAaImF
své	svůj	k3xOyFgNnSc4
i	i	k9
cizí	cizí	k2eAgFnPc1d1
emoce	emoce	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
jednu	jeden	k4xCgFnSc4
stranu	strana	k1gFnSc4
to	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
dokáží	dokázat	k5eAaPmIp3nP
dobře	dobře	k6eAd1
odhadnout	odhadnout	k5eAaPmF
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
po	po	k7c6
nich	on	k3xPp3gMnPc6
druzí	druhý	k4xOgMnPc1
chtějí	chtít	k5eAaImIp3nP
<g/>
,	,	kIx,
na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
si	se	k3xPyFc3
své	svůj	k3xOyFgInPc4
neúspěchy	neúspěch	k1gInPc4
pamatují	pamatovat	k5eAaImIp3nP
déle	dlouho	k6eAd2
a	a	k8xC
obecně	obecně	k6eAd1
jim	on	k3xPp3gMnPc3
věnují	věnovat	k5eAaImIp3nP,k5eAaPmIp3nP
větší	veliký	k2eAgInSc4d2
mentální	mentální	k2eAgInSc4d1
prostor	prostor	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pomoci	pomoc	k1gFnPc1
může	moct	k5eAaImIp3nS
například	například	k6eAd1
metoda	metoda	k1gFnSc1
mindfulness	mindfulnessa	k1gFnPc2
<g/>
:	:	kIx,
Člověk	člověk	k1gMnSc1
si	se	k3xPyFc3
vyčlení	vyčlenit	k5eAaPmIp3nS
určitý	určitý	k2eAgInSc4d1
čas	čas	k1gInSc4
pro	pro	k7c4
reflexi	reflexe	k1gFnSc4
vlastních	vlastní	k2eAgFnPc2d1
dlouhodobých	dlouhodobý	k2eAgFnPc2d1
i	i	k8xC
momentálních	momentální	k2eAgFnPc2d1
emocí	emoce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Metoda	metoda	k1gFnSc1
údajně	údajně	k6eAd1
pomáhá	pomáhat	k5eAaImIp3nS
lidem	lid	k1gInSc7
s	s	k7c7
vyšším	vysoký	k2eAgInSc7d2
intelektem	intelekt	k1gInSc7
své	svůj	k3xOyFgInPc4
problémy	problém	k1gInPc4
nehromadit	hromadit	k5eNaImF
<g/>
,	,	kIx,
ale	ale	k8xC
naučit	naučit	k5eAaPmF
se	se	k3xPyFc4
s	s	k7c7
nimi	on	k3xPp3gFnPc7
spojenými	spojený	k2eAgFnPc7d1
emocemi	emoce	k1gFnPc7
pracovat	pracovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nP
nepodléhali	podléhat	k5eNaImAgMnP
tendenci	tendence	k1gFnSc4
přizpůsobit	přizpůsobit	k5eAaPmF
se	se	k3xPyFc4
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
pomoci	pomoct	k5eAaPmF
studium	studium	k1gNnSc4
psychologie	psychologie	k1gFnSc2
inteligence	inteligence	k1gFnSc2
<g/>
,	,	kIx,
protože	protože	k8xS
si	se	k3xPyFc3
totiž	totiž	k9
nemusí	muset	k5eNaImIp3nS
uvědomovat	uvědomovat	k5eAaImF
<g/>
,	,	kIx,
že	že	k8xS
jejich	jejich	k3xOp3gFnSc1
odlišnost	odlišnost	k1gFnSc1
není	být	k5eNaImIp3nS
poruchou	porucha	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Ve	v	k7c6
společnosti	společnost	k1gFnSc6
obecně	obecně	k6eAd1
panuje	panovat	k5eAaImIp3nS
dojem	dojem	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
lidé	člověk	k1gMnPc1
s	s	k7c7
vyšší	vysoký	k2eAgFnSc7d2
obecnou	obecný	k2eAgFnSc7d1
inteligencí	inteligence	k1gFnSc7
mají	mít	k5eAaImIp3nP
slabší	slabý	k2eAgInSc4d2
výkon	výkon	k1gInSc4
emoční	emoční	k2eAgFnSc2d1
inteligence	inteligence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jde	jít	k5eAaImIp3nS
ale	ale	k9
o	o	k7c4
klam	klam	k1gInSc4
vytvářený	vytvářený	k2eAgInSc1d1
střídmějším	střídmý	k2eAgNnSc7d2
užíváním	užívání	k1gNnSc7
silných	silný	k2eAgFnPc2d1
emocí	emoce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
emoční	emoční	k2eAgFnSc2d1
inteligence	inteligence	k1gFnSc2
se	se	k3xPyFc4
dnes	dnes	k6eAd1
ale	ale	k8xC
obecný	obecný	k2eAgInSc1d1
intelekt	intelekt	k1gInSc1
považuje	považovat	k5eAaImIp3nS
za	za	k7c4
převážně	převážně	k6eAd1
dědičný	dědičný	k2eAgInSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nelze	lze	k6eNd1
zatím	zatím	k6eAd1
vyvozovat	vyvozovat	k5eAaImF
žádné	žádný	k3yNgInPc4
jasné	jasný	k2eAgInPc4d1
závěry	závěr	k1gInPc4
<g/>
,	,	kIx,
ale	ale	k8xC
tento	tento	k3xDgInSc1
fakt	fakt	k1gInSc1
je	být	k5eAaImIp3nS
částečně	částečně	k6eAd1
zodpovědný	zodpovědný	k2eAgMnSc1d1
za	za	k7c2
nejasné	jasný	k2eNgFnSc2d1
interpretace	interpretace	k1gFnSc2
Flynnova	Flynnův	k2eAgInSc2d1
efektu	efekt	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Kromě	kromě	k7c2
lepší	dobrý	k2eAgFnSc2d2
schopnosti	schopnost	k1gFnSc2
číst	číst	k5eAaImF
emoce	emoce	k1gFnPc4
druhých	druhý	k4xOgInPc2
a	a	k8xC
výraznějšího	výrazný	k2eAgNnSc2d2
sebeuvědomění	sebeuvědomění	k1gNnSc2
se	se	k3xPyFc4
ale	ale	k9
lidé	člověk	k1gMnPc1
s	s	k7c7
vyšším	vysoký	k2eAgInSc7d2
intelektem	intelekt	k1gInSc7
vyznačují	vyznačovat	k5eAaImIp3nP
v	v	k7c6
rámci	rámec	k1gInSc6
emoční	emoční	k2eAgFnSc2d1
inteligence	inteligence	k1gFnSc2
větší	veliký	k2eAgFnSc2d2
schopností	schopnost	k1gFnSc7
sebeovládání	sebeovládání	k1gNnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
bylo	být	k5eAaImAgNnS
potvrzeno	potvrdit	k5eAaPmNgNnS
např.	např.	kA
při	při	k7c6
experimentu	experiment	k1gInSc6
<g/>
,	,	kIx,
ve	v	k7c6
kterém	který	k3yRgInSc6,k3yQgInSc6,k3yIgInSc6
byly	být	k5eAaImAgFnP
děti	dítě	k1gFnPc1
zavřeny	zavřít	k5eAaPmNgFnP
samy	sám	k3xTgInPc1
v	v	k7c6
pokoji	pokoj	k1gInSc6
s	s	k7c7
marshmellow	marshmellow	k?
<g/>
,	,	kIx,
jehož	jenž	k3xRgNnSc2,k3xOyRp3gNnSc2
pokušení	pokušení	k1gNnSc2
nepřekonaly	překonat	k5eNaPmAgFnP
zejména	zejména	k9
děti	dítě	k1gFnPc1
s	s	k7c7
nižším	nízký	k2eAgInSc7d2
intelektem	intelekt	k1gInSc7
a	a	k8xC
nižšími	nízký	k2eAgFnPc7d2
perspektivami	perspektiva	k1gFnPc7
do	do	k7c2
budoucna	budoucno	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Emoční	emoční	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
je	být	k5eAaImIp3nS
spojena	spojit	k5eAaPmNgFnS
s	s	k7c7
úspěchem	úspěch	k1gInSc7
právě	právě	k6eAd1
a	a	k8xC
hlavně	hlavně	k9
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
lidé	člověk	k1gMnPc1
s	s	k7c7
vyšší	vysoký	k2eAgFnSc7d2
inteligencí	inteligence	k1gFnSc7
jsou	být	k5eAaImIp3nP
schopni	schopen	k2eAgMnPc1d1
se	se	k3xPyFc4
lépe	dobře	k6eAd2
ovládat	ovládat	k5eAaImF
i	i	k9
v	v	k7c6
emoční	emoční	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
,	,	kIx,
tj.	tj.	kA
(	(	kIx(
<g/>
jinými	jiný	k2eAgNnPc7d1
slovy	slovo	k1gNnPc7
<g/>
)	)	kIx)
takoví	takový	k3xDgMnPc1
lidé	člověk	k1gMnPc1
lépe	dobře	k6eAd2
ovládajíc	ovládat	k5eAaImSgFnS
svoje	svůj	k3xOyFgNnSc4
emoce	emoce	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Teorie	teorie	k1gFnSc1
inteligence	inteligence	k1gFnSc2
</s>
<s>
Psychometrické	Psychometrický	k2eAgFnPc1d1
teorie	teorie	k1gFnPc1
</s>
<s>
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
nejstarší	starý	k2eAgInSc4d3
přístup	přístup	k1gInSc4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
cílem	cíl	k1gInSc7
je	být	k5eAaImIp3nS
měření	měření	k1gNnSc1
a	a	k8xC
testování	testování	k1gNnSc1
inteligence	inteligence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
První	první	k4xOgInPc1
test	testa	k1gFnPc2
intelektových	intelektový	k2eAgFnPc2d1
schopností	schopnost	k1gFnPc2
vytvořil	vytvořit	k5eAaPmAgMnS
francouzský	francouzský	k2eAgMnSc1d1
psycholog	psycholog	k1gMnSc1
Alfred	Alfred	k1gMnSc1
Binet	Binet	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
byl	být	k5eAaImAgMnS
současníkem	současník	k1gMnSc7
Francise	Francise	k1gFnSc2
Galtona	Galtona	k1gFnSc1
<g/>
,	,	kIx,
avšak	avšak	k8xC
tvrdil	tvrdit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
inteligence	inteligence	k1gFnSc1
se	se	k3xPyFc4
více	hodně	k6eAd2
než	než	k8xS
v	v	k7c6
oblasti	oblast	k1gFnSc6
smyslového	smyslový	k2eAgNnSc2d1
vnímání	vnímání	k1gNnSc2
projevuje	projevovat	k5eAaImIp3nS
schopností	schopnost	k1gFnSc7
vyřešit	vyřešit	k5eAaPmF
nějaký	nějaký	k3yIgInSc4
problém	problém	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Byl	být	k5eAaImAgMnS
pověřen	pověřit	k5eAaPmNgMnS
francouzskou	francouzský	k2eAgFnSc7d1
vládou	vláda	k1gFnSc7
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
z	z	k7c2
důvodu	důvod	k1gInSc2
zavádění	zavádění	k1gNnSc1
povinné	povinný	k2eAgFnSc2d1
školní	školní	k2eAgFnSc2d1
docházky	docházka	k1gFnSc2
vytvořil	vytvořit	k5eAaPmAgInS
test	test	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
by	by	kYmCp3nS
byl	být	k5eAaImAgInS
schopen	schopen	k2eAgMnSc1d1
vydělit	vydělit	k5eAaPmF
děti	dítě	k1gFnPc4
<g/>
,	,	kIx,
pro	pro	k7c4
které	který	k3yIgNnSc4,k3yQgNnSc4,k3yRgNnSc4
by	by	kYmCp3nS
povinná	povinný	k2eAgFnSc1d1
školní	školní	k2eAgFnSc1d1
docházka	docházka	k1gFnSc1
v	v	k7c6
běžné	běžný	k2eAgFnSc6d1
škole	škola	k1gFnSc6
vzhledem	vzhled	k1gInSc7
k	k	k7c3
jejich	jejich	k3xOp3gInSc3
mentálnímu	mentální	k2eAgInSc3d1
vývoji	vývoj	k1gInSc3
nebyla	být	k5eNaImAgFnS
již	již	k6eAd1
přínosem	přínos	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dětem	dítě	k1gFnPc3
zadal	zadat	k5eAaPmAgInS
řadu	řada	k1gFnSc4
položek	položka	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc4,k3yRgFnPc4,k3yIgFnPc4
následně	následně	k6eAd1
koreloval	korelovat	k5eAaImAgMnS
s	s	k7c7
jednotlivými	jednotlivý	k2eAgFnPc7d1
věkovými	věkový	k2eAgFnPc7d1
skupinami	skupina	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Binet	Binet	k1gMnSc1
předpokládal	předpokládat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
nadprůměrně	nadprůměrně	k6eAd1
inteligentní	inteligentní	k2eAgNnSc1d1
dítě	dítě	k1gNnSc1
splní	splnit	k5eAaPmIp3nS
úkol	úkol	k1gInSc4
jako	jako	k8xC,k8xS
starší	starý	k2eAgNnSc4d2
průměrné	průměrný	k2eAgNnSc4d1
dítě	dítě	k1gNnSc4
a	a	k8xC
podprůměrně	podprůměrně	k6eAd1
inteligentní	inteligentní	k2eAgNnSc1d1
dítě	dítě	k1gNnSc1
splní	splnit	k5eAaPmIp3nS
úkol	úkol	k1gInSc4
jako	jako	k8xS,k8xC
mladší	mladý	k2eAgNnSc4d2
průměrně	průměrně	k6eAd1
inteligentní	inteligentní	k2eAgNnSc4d1
dítě	dítě	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
základě	základ	k1gInSc6
toho	ten	k3xDgMnSc4
bylo	být	k5eAaImAgNnS
možné	možný	k2eAgNnSc1d1
stanovit	stanovit	k5eAaPmF
mentální	mentální	k2eAgInSc4d1
věk	věk	k1gInSc4
testovaného	testovaný	k2eAgNnSc2d1
<g/>
.	.	kIx.
</s>
<s>
Binetův	Binetův	k2eAgInSc4d1
výzkum	výzkum	k1gInSc4
sledoval	sledovat	k5eAaImAgMnS
německý	německý	k2eAgMnSc1d1
psycholog	psycholog	k1gMnSc1
William	William	k1gInSc1
Stern	sternum	k1gNnPc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
stanovil	stanovit	k5eAaPmAgInS
inteligenční	inteligenční	k2eAgInSc1d1
kvocient	kvocient	k1gInSc1
(	(	kIx(
<g/>
IQ	iq	kA
<g/>
)	)	kIx)
coby	coby	k?
podíl	podíl	k1gInSc4
mentálního	mentální	k2eAgInSc2d1
a	a	k8xC
chronologického	chronologický	k2eAgInSc2d1
věku	věk	k1gInSc2
násobený	násobený	k2eAgInSc1d1
stem	sto	k4xCgNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lewis	Lewis	k1gInSc4
Terman	Terman	k1gMnSc1
ze	z	k7c2
Stanfordovy	Stanfordův	k2eAgFnSc2d1
univerzity	univerzita	k1gFnSc2
přizpůsobil	přizpůsobit	k5eAaPmAgMnS
Binetovy	Binetův	k2eAgFnPc4d1
položky	položka	k1gFnPc4
a	a	k8xC
převzal	převzít	k5eAaPmAgInS
Sternův	Sternův	k2eAgInSc1d1
pojem	pojem	k1gInSc1
IQ	iq	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Test	test	k1gInSc1
se	se	k3xPyFc4
nazývá	nazývat	k5eAaImIp3nS
Stanfordova-Binetova	Stanfordova-Binetův	k2eAgFnSc1d1
škála	škála	k1gFnSc1
a	a	k8xC
zahrnuje	zahrnovat	k5eAaImIp3nS
následující	následující	k2eAgFnSc1d1
položky	položka	k1gFnPc1
<g/>
:	:	kIx,
verbální	verbální	k2eAgNnSc1d1
myšlení	myšlení	k1gNnSc1
<g/>
,	,	kIx,
kvantitativní	kvantitativní	k2eAgNnSc1d1
myšlení	myšlení	k1gNnSc1
<g/>
,	,	kIx,
abstraktně-vizuální	abstraktně-vizuální	k2eAgNnSc1d1
myšlení	myšlení	k1gNnSc1
<g/>
,	,	kIx,
krátkodobá	krátkodobý	k2eAgFnSc1d1
paměť	paměť	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Faktorová	faktorový	k2eAgFnSc1d1
analýza	analýza	k1gFnSc1
v	v	k7c6
psychometrických	psychometrický	k2eAgFnPc6d1
teoriích	teorie	k1gFnPc6
inteligence	inteligence	k1gFnSc2
</s>
<s>
Do	do	k7c2
roku	rok	k1gInSc2
1939	#num#	k4
měřily	měřit	k5eAaImAgFnP
všechny	všechen	k3xTgFnPc1
testy	testa	k1gFnPc1
inteligence	inteligence	k1gFnSc2
pouze	pouze	k6eAd1
jeden	jeden	k4xCgInSc1
faktor	faktor	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Následně	následně	k6eAd1
začaly	začít	k5eAaPmAgFnP
psychometrické	psychometrický	k2eAgFnPc1d1
teorie	teorie	k1gFnPc1
inteligence	inteligence	k1gFnSc2
využívat	využívat	k5eAaImF,k5eAaPmF
k	k	k7c3
odhalování	odhalování	k1gNnSc3
jednotlivých	jednotlivý	k2eAgFnPc2d1
složek	složka	k1gFnPc2
inteligence	inteligence	k1gFnSc2
metody	metoda	k1gFnSc2
tzv.	tzv.	kA
faktorové	faktorový	k2eAgFnSc2d1
analýzy	analýza	k1gFnSc2
<g/>
,	,	kIx,
s	s	k7c7
jejíž	jejíž	k3xOyRp3gFnSc7
pomocí	pomoc	k1gFnSc7
jsou	být	k5eAaImIp3nP
vyhledávány	vyhledáván	k2eAgFnPc1d1
souvislosti	souvislost	k1gFnPc1
v	v	k7c6
různých	různý	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
intelektových	intelektový	k2eAgInPc2d1
výkonů	výkon	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dává	dávat	k5eAaImIp3nS
odpověď	odpověď	k1gFnSc4
na	na	k7c4
to	ten	k3xDgNnSc4
<g/>
,	,	kIx,
pomocí	pomocí	k7c2
kolika	kolik	k4yIc2,k4yRc2,k4yQc2
faktorů	faktor	k1gInPc2
lze	lze	k6eAd1
vysvětlit	vysvětlit	k5eAaPmF
výkon	výkon	k1gInSc4
v	v	k7c6
jednotlivých	jednotlivý	k2eAgFnPc6d1
disciplínách	disciplína	k1gFnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Metodu	metoda	k1gFnSc4
faktorové	faktorový	k2eAgFnSc2d1
analýzy	analýza	k1gFnSc2
vytvořil	vytvořit	k5eAaPmAgMnS
Thurstone	Thurston	k1gInSc5
<g/>
,	,	kIx,
který	který	k3yRgInSc1,k3yQgInSc1,k3yIgInSc1
místo	místo	k7c2
pohlížení	pohlížení	k1gNnSc2
na	na	k7c4
inteligenci	inteligence	k1gFnSc4
jako	jako	k8xS,k8xC
na	na	k7c4
jednotnou	jednotný	k2eAgFnSc4d1
schopnost	schopnost	k1gFnSc4
na	na	k7c6
jejím	její	k3xOp3gInSc6
základě	základ	k1gInSc6
stanovil	stanovit	k5eAaPmAgInS
sedm	sedm	k4xCc4
nezávislých	závislý	k2eNgFnPc2d1
intelektových	intelektový	k2eAgFnPc2d1
schopností	schopnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
verbální	verbální	k2eAgNnSc4d1
porozumění	porozumění	k1gNnSc4
(	(	kIx(
<g/>
schopnost	schopnost	k1gFnSc4
chápat	chápat	k5eAaImF
význam	význam	k1gInSc4
různých	různý	k2eAgNnPc2d1
slov	slovo	k1gNnPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
induktivní	induktivní	k2eAgNnPc4d1
usuzování	usuzování	k1gNnPc4
(	(	kIx(
<g/>
schopnost	schopnost	k1gFnSc4
nalézt	nalézt	k5eAaBmF,k5eAaPmF
obecné	obecný	k2eAgNnSc4d1
pravidlo	pravidlo	k1gNnSc4
na	na	k7c6
základě	základ	k1gInSc6
jednotlivých	jednotlivý	k2eAgInPc2d1
případů	případ	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
rychlost	rychlost	k1gFnSc1
vnímání	vnímání	k1gNnSc2
(	(	kIx(
<g/>
schopnost	schopnost	k1gFnSc1
rychle	rychle	k6eAd1
vnímat	vnímat	k5eAaImF
detaily	detail	k1gInPc4
a	a	k8xC
všímat	všímat	k5eAaImF
si	se	k3xPyFc3
podobností	podobnost	k1gFnPc2
a	a	k8xC
rozdílů	rozdíl	k1gInPc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
numerické	numerický	k2eAgFnPc4d1
schopnosti	schopnost	k1gFnPc4
(	(	kIx(
<g/>
schopnost	schopnost	k1gFnSc4
provádět	provádět	k5eAaImF
výpočty	výpočet	k1gInPc4
<g/>
,	,	kIx,
řešit	řešit	k5eAaImF
jednoduché	jednoduchý	k2eAgFnPc4d1
matematické	matematický	k2eAgFnPc4d1
úlohy	úloha	k1gFnPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
verbální	verbální	k2eAgFnSc1d1
fluence	fluence	k1gFnSc1
(	(	kIx(
<g/>
schopnost	schopnost	k1gFnSc1
rychle	rychle	k6eAd1
uvažovat	uvažovat	k5eAaImF
o	o	k7c6
slovech	slovo	k1gNnPc6
<g/>
)	)	kIx)
<g/>
,	,	kIx,
asociační	asociační	k2eAgFnSc4d1
paměť	paměť	k1gFnSc4
(	(	kIx(
<g/>
projevuje	projevovat	k5eAaImIp3nS
se	se	k3xPyFc4
především	především	k9
kvalitou	kvalita	k1gFnSc7
vybavování	vybavování	k1gNnSc2
dříve	dříve	k6eAd2
prezentovaných	prezentovaný	k2eAgInPc2d1
verbálních	verbální	k2eAgInPc2d1
nebo	nebo	k8xC
zrakových	zrakový	k2eAgInPc2d1
podnětů	podnět	k1gInPc2
<g/>
)	)	kIx)
a	a	k8xC
prostorová	prostorový	k2eAgFnSc1d1
vizualizace	vizualizace	k1gFnSc1
(	(	kIx(
<g/>
schopnost	schopnost	k1gFnSc4
vytvářet	vytvářet	k5eAaImF
zrakové	zrakový	k2eAgFnPc4d1
představy	představa	k1gFnPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vytvořil	vytvořit	k5eAaPmAgInS
také	také	k9
Test	test	k1gInSc1
primárních	primární	k2eAgFnPc2d1
duševních	duševní	k2eAgFnPc2d1
schopností	schopnost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Vědci	vědec	k1gMnPc1
využívající	využívající	k2eAgFnSc2d1
metody	metoda	k1gFnSc2
faktorové	faktorový	k2eAgFnSc2d1
analýzy	analýza	k1gFnSc2
se	se	k3xPyFc4
dále	daleko	k6eAd2
dělí	dělit	k5eAaImIp3nS
podle	podle	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
zda	zda	k8xS
existuje	existovat	k5eAaImIp3nS
<g/>
,	,	kIx,
nebo	nebo	k8xC
neexistuje	existovat	k5eNaImIp3nS
společný	společný	k2eAgInSc1d1
nadřazený	nadřazený	k2eAgInSc1d1
faktor	faktor	k1gInSc1
inteligence	inteligence	k1gFnSc2
na	na	k7c6
„	„	k?
<g/>
lampers	lampers	k1gInSc1
<g/>
“	“	k?
(	(	kIx(
<g/>
scelovači	scelovač	k1gMnPc1
<g/>
)	)	kIx)
a	a	k8xC
„	„	k?
<g/>
splitters	splitters	k1gInSc1
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
scelovačů	scelovač	k1gMnPc2
existuje	existovat	k5eAaImIp3nS
jedna	jeden	k4xCgFnSc1
společná	společný	k2eAgFnSc1d1
schopnost	schopnost	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
projevuje	projevovat	k5eAaImIp3nS
v	v	k7c6
různých	různý	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Splitters	Splittersa	k1gFnPc2
připisují	připisovat	k5eAaImIp3nP
intelektový	intelektový	k2eAgInSc4d1
výkon	výkon	k1gInSc4
sadě	sada	k1gFnSc6
několika	několik	k4yIc2
nezávislých	závislý	k2eNgFnPc2d1
kognitivních	kognitivní	k2eAgFnPc2d1
schopností	schopnost	k1gFnPc2
<g/>
,	,	kIx,
nezávislých	závislý	k2eNgInPc2d1
na	na	k7c4
g.	g.	k?
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Charles	Charles	k1gMnSc1
Spearman	Spearman	k1gMnSc1
byl	být	k5eAaImAgMnS
jedním	jeden	k4xCgMnSc7
z	z	k7c2
prvních	první	k4xOgMnPc2
psychologů	psycholog	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
se	se	k3xPyFc4
zabývali	zabývat	k5eAaImAgMnP
zejména	zejména	k9
analytickou	analytický	k2eAgFnSc7d1
složkou	složka	k1gFnSc7
inteligence	inteligence	k1gFnSc2
a	a	k8xC
jejího	její	k3xOp3gNnSc2
testování	testování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
teorie	teorie	k1gFnSc1
ovlivnila	ovlivnit	k5eAaPmAgFnS
mnoho	mnoho	k4c4
dalších	další	k2eAgNnPc2d1
smýšlení	smýšlení	k1gNnPc2
o	o	k7c4
inteligenci	inteligence	k1gFnSc4
<g/>
;	;	kIx,
vyděluje	vydělovat	k5eAaImIp3nS
jeden	jeden	k4xCgInSc1
obecný	obecný	k2eAgInSc1d1
faktor	faktor	k1gInSc1
g	g	kA
(	(	kIx(
<g/>
general	generat	k5eAaImAgInS,k5eAaPmAgInS
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
považoval	považovat	k5eAaImAgMnS
za	za	k7c4
„	„	k?
<g/>
hlavní	hlavní	k2eAgFnSc4d1
mentální	mentální	k2eAgFnSc4d1
energii	energie	k1gFnSc4
<g/>
“	“	k?
<g/>
,	,	kIx,
a	a	k8xC
jeden	jeden	k4xCgInSc4
nebo	nebo	k8xC
více	hodně	k6eAd2
specifických	specifický	k2eAgInPc2d1
faktorů	faktor	k1gInPc2
s	s	k7c7
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
zodpovídají	zodpovídat	k5eAaPmIp3nP,k5eAaImIp3nP
za	za	k7c4
výkon	výkon	k1gInSc4
jednotlivců	jednotlivec	k1gMnPc2
v	v	k7c6
testech	test	k1gInPc6
inteligence	inteligence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Faktor	faktor	k1gInSc1
g	g	kA
se	se	k3xPyFc4
pak	pak	k6eAd1
projevuje	projevovat	k5eAaImIp3nS
v	v	k7c6
různých	různý	k2eAgFnPc6d1
úlohách	úloha	k1gFnPc6
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
faktor	faktor	k1gInSc4
s	s	k7c7
měří	měřit	k5eAaImIp3nS
specifické	specifický	k2eAgInPc4d1
testy	test	k1gInPc4
a	a	k8xC
subtesty	subtest	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Faktor	faktor	k1gInSc1
g	g	kA
je	být	k5eAaImIp3nS
zahrnut	zahrnout	k5eAaPmNgInS
v	v	k7c6
deduktivním	deduktivní	k2eAgNnSc6d1
usuzování	usuzování	k1gNnSc6
a	a	k8xC
uvažování	uvažování	k1gNnSc6
a	a	k8xC
je	být	k5eAaImIp3nS
úzce	úzko	k6eAd1
spjat	spjat	k2eAgMnSc1d1
se	s	k7c7
schopností	schopnost	k1gFnSc7
<g/>
,	,	kIx,
rychlostí	rychlost	k1gFnSc7
mentálních	mentální	k2eAgFnPc2d1
(	(	kIx(
<g/>
duševních	duševní	k2eAgFnPc2d1
<g/>
)	)	kIx)
dovedností	dovednost	k1gFnPc2
představujících	představující	k2eAgFnPc2d1
aspekt	aspekt	k1gInSc4
tvůrčí	tvůrčí	k2eAgFnSc2d1
schopnosti	schopnost	k1gFnSc2
spíše	spíše	k9
než	než	k8xS
aspekt	aspekt	k1gInSc4
reprodukční	reprodukční	k2eAgFnSc2d1
schopnosti	schopnost	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kognitivní	kognitivní	k2eAgFnPc1d1
schopnosti	schopnost	k1gFnPc1
spojené	spojený	k2eAgFnPc1d1
s	s	k7c7
obecnými	obecný	k2eAgFnPc7d1
mentálními	mentální	k2eAgFnPc7d1
schopnostmi	schopnost	k1gFnPc7
by	by	kYmCp3nS
mohly	moct	k5eAaImAgFnP
zahrnovat	zahrnovat	k5eAaImF
dovednost	dovednost	k1gFnSc4
popsání	popsání	k1gNnSc2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
dva	dva	k4xCgInPc1
koncepty	koncept	k1gInPc1
spolu	spolu	k6eAd1
souvisí	souviset	k5eAaImIp3nP
<g/>
,	,	kIx,
nebo	nebo	k8xC
dovednost	dovednost	k1gFnSc4
nalezení	nalezení	k1gNnSc3
druhé	druhý	k4xOgFnSc2
myšlenky	myšlenka	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
spojena	spojit	k5eAaPmNgFnS
ke	k	k7c3
konceptu	koncept	k1gInSc3
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
již	již	k6eAd1
byl	být	k5eAaImAgInS
navržen	navrhnout	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Testy	testa	k1gFnPc1
s	s	k7c7
vysokou	vysoký	k2eAgFnSc7d1
g	g	kA
zátěží	zátěž	k1gFnSc7
jsou	být	k5eAaImIp3nP
komplexní	komplexní	k2eAgInPc1d1
a	a	k8xC
zahrnují	zahrnovat	k5eAaImIp3nP
úkoly	úkol	k1gInPc4
<g/>
,	,	kIx,
které	který	k3yIgInPc1,k3yRgInPc1,k3yQgInPc1
obsahují	obsahovat	k5eAaImIp3nP
logické	logický	k2eAgNnSc4d1
myšlení	myšlení	k1gNnSc4
a	a	k8xC
testování	testování	k1gNnSc4
hypotéz	hypotéza	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Testy	testa	k1gFnPc1
s	s	k7c7
nižší	nízký	k2eAgFnSc7d2
g	g	kA
zátěží	zátěž	k1gFnSc7
jsou	být	k5eAaImIp3nP
méně	málo	k6eAd2
komplexní	komplexní	k2eAgFnPc1d1
a	a	k8xC
zahrnují	zahrnovat	k5eAaImIp3nP
úkoly	úkol	k1gInPc4
týkající	týkající	k2eAgInPc4d1
se	se	k3xPyFc4
především	především	k6eAd1
rozpoznávání	rozpoznávání	k1gNnSc2
<g/>
,	,	kIx,
připomínání	připomínání	k1gNnSc2
a	a	k8xC
rychlostí	rychlost	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Cattelův	Cattelův	k2eAgInSc1d1
a	a	k8xC
Hornův	Hornův	k2eAgInSc1d1
gf-gc	gf-gc	k1gInSc1
model	model	k1gInSc1
vyčleňuje	vyčleňovat	k5eAaImIp3nS
dva	dva	k4xCgInPc4
intelektové	intelektový	k2eAgInPc4d1
faktory	faktor	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
fluidní	fluidní	k2eAgFnSc4d1
(	(	kIx(
<g/>
gf	gf	k?
<g/>
)	)	kIx)
a	a	k8xC
krystalickou	krystalický	k2eAgFnSc7d1
(	(	kIx(
<g/>
gc	gc	k?
<g/>
)	)	kIx)
inteligenci	inteligence	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fluidní	fluidní	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
je	být	k5eAaImIp3nS
vrozená	vrozený	k2eAgFnSc1d1
a	a	k8xC
dosahuje	dosahovat	k5eAaImIp3nS
vrcholu	vrchol	k1gInSc2
okolo	okolo	k6eAd1
20	#num#	k4
let	léto	k1gNnPc2
<g/>
,	,	kIx,
s	s	k7c7
věkem	věk	k1gInSc7
se	se	k3xPyFc4
zhoršuje	zhoršovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahrnuje	zahrnovat	k5eAaImIp3nS
rychlost	rychlost	k1gFnSc1
myšlení	myšlení	k1gNnSc2
<g/>
,	,	kIx,
krátkodobou	krátkodobý	k2eAgFnSc4d1
paměť	paměť	k1gFnSc4
či	či	k8xC
usuzování	usuzování	k1gNnSc4
na	na	k7c6
neznámém	známý	k2eNgNnSc6d1
tématu	téma	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Krystalická	krystalický	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
je	být	k5eAaImIp3nS
naučená	naučený	k2eAgFnSc1d1
a	a	k8xC
představuje	představovat	k5eAaImIp3nS
aplikaci	aplikace	k1gFnSc4
znalostí	znalost	k1gFnPc2
a	a	k8xC
dovedností	dovednost	k1gFnPc2
na	na	k7c6
známém	známý	k2eAgInSc6d1
materiálu	materiál	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
se	se	k3xPyFc4
během	během	k7c2
dětství	dětství	k1gNnSc2
zlepšuje	zlepšovat	k5eAaImIp3nS
a	a	k8xC
stagnuje	stagnovat	k5eAaImIp3nS
nebo	nebo	k8xC
se	se	k3xPyFc4
mírně	mírně	k6eAd1
zhoršuje	zhoršovat	k5eAaImIp3nS
v	v	k7c6
dospělosti	dospělost	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úbytek	úbytek	k1gInSc1
fluidní	fluidní	k2eAgFnSc2d1
inteligence	inteligence	k1gFnSc2
může	moct	k5eAaImIp3nS
být	být	k5eAaImF
kompenzován	kompenzovat	k5eAaBmNgInS
expertními	expertní	k2eAgFnPc7d1
schopnostmi	schopnost	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ty	ten	k3xDgInPc4
se	se	k3xPyFc4
zlepšují	zlepšovat	k5eAaImIp3nP
praxí	praxe	k1gFnSc7
a	a	k8xC
dosahují	dosahovat	k5eAaImIp3nP
vrcholu	vrchol	k1gInSc3
až	až	k6eAd1
po	po	k7c4
40	#num#	k4
<g/>
.	.	kIx.
roce	rok	k1gInSc6
věku	věk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Sociální	sociální	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
</s>
<s>
Blokové	blokový	k2eAgNnSc1d1
schema	schema	k1gNnSc1
inteligentního	inteligentní	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sociální	sociální	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
kladou	klást	k5eAaImIp3nP
důraz	důraz	k1gInSc4
na	na	k7c4
užitečnost	užitečnost	k1gFnSc4
inteligence	inteligence	k1gFnSc2
v	v	k7c6
daném	daný	k2eAgNnSc6d1
sociálním	sociální	k2eAgNnSc6d1
prostředí	prostředí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Roberta	Robert	k1gMnSc2
Sternberga	Sternberg	k1gMnSc2
existují	existovat	k5eAaImIp3nP
tři	tři	k4xCgInPc1
základní	základní	k2eAgInPc1d1
mentální	mentální	k2eAgInPc1d1
procesy	proces	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yQgInPc1,k3yIgInPc1,k3yRgInPc1
zdůrazňují	zdůrazňovat	k5eAaImIp3nP
veškeré	veškerý	k3xTgNnSc4
inteligentní	inteligentní	k2eAgNnSc4d1
chování	chování	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Avšak	avšak	k8xC
co	co	k3yRnSc1,k3yQnSc1,k3yInSc1
je	být	k5eAaImIp3nS
vnímáno	vnímat	k5eAaImNgNnS
v	v	k7c6
jedné	jeden	k4xCgFnSc6
kultuře	kultura	k1gFnSc6
jako	jako	k9
inteligentní	inteligentní	k2eAgInSc1d1
<g/>
,	,	kIx,
nemusí	muset	k5eNaImIp3nS
zdaleka	zdaleka	k6eAd1
znamenat	znamenat	k5eAaImF
inteligentní	inteligentní	k2eAgInSc1d1
v	v	k7c6
kultuře	kultura	k1gFnSc6
jiné	jiný	k2eAgFnSc6d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Základní	základní	k2eAgInPc1d1
mentální	mentální	k2eAgInPc1d1
procesy	proces	k1gInPc1
se	se	k3xPyFc4
v	v	k7c6
různých	různý	k2eAgFnPc6d1
kulturách	kultura	k1gFnPc6
neliší	lišit	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Třemi	tři	k4xCgInPc7
základními	základní	k2eAgInPc7d1
mentálními	mentální	k2eAgInPc7d1
procesy	proces	k1gInPc7
jsou	být	k5eAaImIp3nP
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Exekutivní	exekutivní	k2eAgInSc1d1
–	–	k?
je	být	k5eAaImIp3nS
takový	takový	k3xDgInSc1
proces	proces	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
úzce	úzko	k6eAd1
spjat	spjat	k2eAgMnSc1d1
s	s	k7c7
realizováním	realizování	k1gNnSc7
a	a	k8xC
děláním	dělání	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Legislativní	legislativní	k2eAgFnSc1d1
–	–	k?
funkce	funkce	k1gFnSc1
je	být	k5eAaImIp3nS
spojena	spojit	k5eAaPmNgFnS
s	s	k7c7
plánováním	plánování	k1gNnSc7
<g/>
,	,	kIx,
představováním	představování	k1gNnSc7
<g/>
,	,	kIx,
tvořením	tvoření	k1gNnSc7
a	a	k8xC
formulováním	formulování	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Justiční	justiční	k2eAgInSc1d1
–	–	k?
proces	proces	k1gInSc1
je	být	k5eAaImIp3nS
spojen	spojit	k5eAaPmNgInS
s	s	k7c7
porovnáváním	porovnávání	k1gNnSc7
<g/>
,	,	kIx,
vyhodnocováním	vyhodnocování	k1gNnSc7
a	a	k8xC
usuzováním	usuzování	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
Sternberga	Sternberg	k1gMnSc2
inteligence	inteligence	k1gFnSc2
nemůže	moct	k5eNaImIp3nS
být	být	k5eAaImF
měřena	měřen	k2eAgFnSc1d1
testy	test	k1gInPc4
<g/>
,	,	kIx,
ale	ale	k8xC
měla	mít	k5eAaImAgFnS
by	by	kYmCp3nS
být	být	k5eAaImF
měřena	měřit	k5eAaImNgFnS
a	a	k8xC
definována	definovat	k5eAaBmNgFnS
v	v	k7c6
rámci	rámec	k1gInSc6
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
se	se	k3xPyFc4
chováme	chovat	k5eAaImIp1nP
v	v	k7c6
každodenním	každodenní	k2eAgInSc6d1
životě	život	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tímto	tento	k3xDgNnSc7
Sternberg	Sternberg	k1gInSc1
odkazuje	odkazovat	k5eAaImIp3nS
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
nazývá	nazývat	k5eAaImIp3nS
úspěšnou	úspěšný	k2eAgFnSc7d1
inteligencí	inteligence	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
jsou	být	k5eAaImIp3nP
úspěšně	úspěšně	k6eAd1
inteligentní	inteligentní	k2eAgMnPc1d1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
schopni	schopen	k2eAgMnPc1d1
definovat	definovat	k5eAaBmF
a	a	k8xC
dosáhnout	dosáhnout	k5eAaPmF
vlastních	vlastní	k2eAgFnPc2d1
idejí	idea	k1gFnPc2
úspěchu	úspěch	k1gInSc2
ve	v	k7c6
své	svůj	k3xOyFgFnSc6
vlastní	vlastní	k2eAgFnSc6d1
kultuře	kultura	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
jsou	být	k5eAaImIp3nP
úspěšně	úspěšně	k6eAd1
inteligentní	inteligentní	k2eAgMnPc1d1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
schopni	schopen	k2eAgMnPc1d1
se	se	k3xPyFc4
adaptovat	adaptovat	k5eAaBmF
a	a	k8xC
přizpůsobit	přizpůsobit	k5eAaPmF
se	se	k3xPyFc4
svému	svůj	k3xOyFgNnSc3
prostředí	prostředí	k1gNnSc3
tak	tak	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
to	ten	k3xDgNnSc1
co	co	k9
nejlépe	dobře	k6eAd3
uspokojovalo	uspokojovat	k5eAaImAgNnS
jejich	jejich	k3xOp3gFnPc4
potřeby	potřeba	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protože	protože	k8xS
jejich	jejich	k3xOp3gFnSc1
inteligence	inteligence	k1gFnSc1
je	být	k5eAaImIp3nS
vysoce	vysoce	k6eAd1
závislá	závislý	k2eAgFnSc1d1
na	na	k7c6
kultuře	kultura	k1gFnSc6
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yIgFnSc6,k3yRgFnSc6,k3yQgFnSc6
se	se	k3xPyFc4
nacházejí	nacházet	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
To	ten	k3xDgNnSc1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
inteligentní	inteligentní	k2eAgMnSc1d1
jednotlivec	jednotlivec	k1gMnSc1
v	v	k7c6
jedné	jeden	k4xCgFnSc6
kultuře	kultura	k1gFnSc6
nemusí	muset	k5eNaImIp3nS
být	být	k5eAaImF
považován	považován	k2eAgMnSc1d1
za	za	k7c4
inteligentního	inteligentní	k2eAgMnSc4d1
v	v	k7c6
kultuře	kultura	k1gFnSc6
jiné	jiný	k2eAgFnSc6d1
<g/>
.	.	kIx.
</s>
<s>
Jsou	být	k5eAaImIp3nP
zde	zde	k6eAd1
tři	tři	k4xCgFnPc4
základní	základní	k2eAgFnPc4d1
komponenty	komponenta	k1gFnPc4
úspěšné	úspěšný	k2eAgFnSc2d1
inteligence	inteligence	k1gFnSc2
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
je	být	k5eAaImIp3nS
hlavní	hlavní	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
Triarchické	Triarchický	k2eAgFnPc1d1
nebo	nebo	k8xC
také	také	k9
někdy	někdy	k6eAd1
zvané	zvaný	k2eAgFnPc4d1
Triangulární	triangulární	k2eAgFnPc4d1
či	či	k8xC
Trojúhelníkové	trojúhelníkový	k2eAgFnPc4d1
teorie	teorie	k1gFnPc4
<g/>
:	:	kIx,
</s>
<s>
Analytická	analytický	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
–	–	k?
Je	být	k5eAaImIp3nS
inteligence	inteligence	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
odkazuje	odkazovat	k5eAaImIp3nS
na	na	k7c4
schopnost	schopnost	k1gFnSc4
řešení	řešení	k1gNnSc2
problémů	problém	k1gInPc2
<g/>
,	,	kIx,
efektivního	efektivní	k2eAgNnSc2d1
zpracování	zpracování	k1gNnSc2
informací	informace	k1gFnPc2
a	a	k8xC
kompletování	kompletování	k1gNnSc2
akademických	akademický	k2eAgFnPc2d1
úloh	úloha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidé	člověk	k1gMnPc1
s	s	k7c7
vysokým	vysoký	k2eAgInSc7d1
analytickým	analytický	k2eAgInSc7d1
IQ	iq	kA
skládají	skládat	k5eAaImIp3nP
testy	test	k1gInPc1
inteligence	inteligence	k1gFnSc2
velice	velice	k6eAd1
zdatně	zdatně	k6eAd1
<g/>
,	,	kIx,
stejně	stejně	k6eAd1
tak	tak	k9
jako	jako	k9
různé	různý	k2eAgFnPc4d1
zkoušky	zkouška	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Takoví	takový	k3xDgMnPc1
lidé	člověk	k1gMnPc1
jsou	být	k5eAaImIp3nP
velice	velice	k6eAd1
schopni	schopen	k2eAgMnPc1d1
kritického	kritický	k2eAgNnSc2d1
a	a	k8xC
analytického	analytický	k2eAgNnSc2d1
myšlení	myšlení	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Kreativní	kreativní	k2eAgFnSc2d1
inteligence	inteligence	k1gFnSc2
–	–	k?
Díky	díky	k7c3
této	tento	k3xDgFnSc3
inteligenci	inteligence	k1gFnSc3
jsme	být	k5eAaImIp1nP
schopni	schopen	k2eAgMnPc1d1
využít	využít	k5eAaPmF
již	již	k6eAd1
existujících	existující	k2eAgFnPc2d1
zkušeností	zkušenost	k1gFnPc2
<g/>
,	,	kIx,
vědomostí	vědomost	k1gFnPc2
a	a	k8xC
schopností	schopnost	k1gFnPc2
k	k	k7c3
úspěšnému	úspěšný	k2eAgNnSc3d1
a	a	k8xC
efektivnímu	efektivní	k2eAgNnSc3d1
zvládání	zvládání	k1gNnSc3
nových	nový	k2eAgFnPc2d1
a	a	k8xC
neobvyklých	obvyklý	k2eNgFnPc2d1
situací	situace	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidé	člověk	k1gMnPc1
s	s	k7c7
vysokou	vysoký	k2eAgFnSc7d1
kreativní	kreativní	k2eAgFnSc7d1
inteligencí	inteligence	k1gFnSc7
mají	mít	k5eAaImIp3nP
výborný	výborný	k2eAgInSc4d1
pohled	pohled	k1gInSc4
na	na	k7c4
věc	věc	k1gFnSc4
<g/>
,	,	kIx,
představivost	představivost	k1gFnSc4
a	a	k8xC
jsou	být	k5eAaImIp3nP
schopni	schopen	k2eAgMnPc1d1
formulovat	formulovat	k5eAaImF
nové	nový	k2eAgInPc4d1
nápady	nápad	k1gInPc4
a	a	k8xC
myšlenky	myšlenka	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
inteligence	inteligence	k1gFnSc1
je	být	k5eAaImIp3nS
většinou	většina	k1gFnSc7
v	v	k7c6
testech	test	k1gInPc6
přehlížena	přehlížen	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Praktická	praktický	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
–	–	k?
Umožňuje	umožňovat	k5eAaImIp3nS
nám	my	k3xPp1nPc3
využívat	využívat	k5eAaPmF,k5eAaImF
vědomostí	vědomost	k1gFnSc7
a	a	k8xC
zkušeností	zkušenost	k1gFnPc2
získaných	získaný	k2eAgFnPc2d1
v	v	k7c6
minulosti	minulost	k1gFnSc6
k	k	k7c3
adaptování	adaptování	k1gNnSc3
<g/>
,	,	kIx,
formování	formování	k1gNnSc2
a	a	k8xC
přetváření	přetváření	k1gNnSc2
našeho	náš	k3xOp1gNnSc2
prostředí	prostředí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ti	ten	k3xDgMnPc1
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
mají	mít	k5eAaImIp3nP
vysokou	vysoký	k2eAgFnSc4d1
praktickou	praktický	k2eAgFnSc4d1
inteligenci	inteligence	k1gFnSc4
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
schopni	schopen	k2eAgMnPc1d1
identifikovat	identifikovat	k5eAaBmF
faktory	faktor	k1gInPc4
<g/>
,	,	kIx,
kterých	který	k3yQgInPc2,k3yRgInPc2,k3yIgInPc2
je	být	k5eAaImIp3nS
zapotřebí	zapotřebí	k6eAd1
k	k	k7c3
dosažení	dosažení	k1gNnSc3
úspěchu	úspěch	k1gInSc2
<g/>
,	,	kIx,
a	a	k8xC
jsou	být	k5eAaImIp3nP
dobří	dobrý	k2eAgMnPc1d1
v	v	k7c6
přizpůsobování	přizpůsobování	k1gNnSc6
se	se	k3xPyFc4
či	či	k8xC
měnění	měnění	k1gNnSc1
vlastního	vlastní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
za	za	k7c7
účelem	účel	k1gInSc7
dosažení	dosažení	k1gNnSc1
sebou	se	k3xPyFc7
stanovených	stanovený	k2eAgInPc2d1
cílů	cíl	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Podle	podle	k7c2
Sternbergovy	Sternbergův	k2eAgFnSc2d1
triarchické	triarchický	k2eAgFnSc2d1
teorie	teorie	k1gFnSc2
představuje	představovat	k5eAaImIp3nS
inteligence	inteligence	k1gFnSc1
získávání	získávání	k1gNnSc2
a	a	k8xC
konsolidaci	konsolidace	k1gFnSc4
sady	sada	k1gFnSc2
schopností	schopnost	k1gFnPc2
potřebných	potřebný	k2eAgInPc2d1
pro	pro	k7c4
mistrovství	mistrovství	k1gNnSc4
v	v	k7c6
jedné	jeden	k4xCgFnSc6
nebo	nebo	k8xC
více	hodně	k6eAd2
doménách	doména	k1gFnPc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahrnuje	zahrnovat	k5eAaImIp3nS
tři	tři	k4xCgFnPc4
subteorie	subteorie	k1gFnPc4
<g/>
:	:	kIx,
komponentovou	komponentový	k2eAgFnSc4d1
<g/>
,	,	kIx,
kontextuální	kontextuální	k2eAgFnSc4d1
a	a	k8xC
zkušenostní	zkušenostní	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s>
Stephen	Stephen	k1gInSc1
Ceci	Cec	k1gFnSc2
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS
roli	role	k1gFnSc4
kontextu	kontext	k1gInSc2
při	při	k7c6
intelektovém	intelektový	k2eAgInSc6d1
výkonu	výkon	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
něj	on	k3xPp3gInSc2
existují	existovat	k5eAaImIp3nP
mnohočetné	mnohočetný	k2eAgInPc1d1
kognitivní	kognitivní	k2eAgInPc1d1
potenciály	potenciál	k1gInPc1
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
mají	mít	k5eAaImIp3nP
biologickou	biologický	k2eAgFnSc4d1
podstatu	podstata	k1gFnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
jejich	jejich	k3xOp3gNnSc4
uplatnění	uplatnění	k1gNnSc4
je	být	k5eAaImIp3nS
závislé	závislý	k2eAgNnSc1d1
na	na	k7c6
ekologickém	ekologický	k2eAgInSc6d1
prostoru	prostor	k1gInSc6
jedince	jedinec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysoká	vysoký	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
se	se	k3xPyFc4
pak	pak	k6eAd1
odvíjí	odvíjet	k5eAaImIp3nS
od	od	k7c2
vědomostní	vědomostní	k2eAgFnSc2d1
základny	základna	k1gFnSc2
v	v	k7c6
určité	určitý	k2eAgFnSc6d1
oblasti	oblast	k1gFnSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
Testy	test	k1gInPc1
Spectrum	Spectrum	k1gNnSc4
assessment	assessment	k1gMnSc1
system	syst	k1gInSc7
a	a	k8xC
Bristing	Bristing	k1gInSc4
assessment	assessment	k1gInSc4
for	forum	k1gNnPc2
teaching	teaching	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
1983	#num#	k4
Howard	Howard	k1gMnSc1
Gardner	Gardner	k1gMnSc1
navrhl	navrhnout	k5eAaPmAgMnS
teorii	teorie	k1gFnSc4
<g/>
,	,	kIx,
jejímž	jejíž	k3xOyRp3gInSc7
cílem	cíl	k1gInSc7
bylo	být	k5eAaImAgNnS
rozšíření	rozšíření	k1gNnSc1
tradiční	tradiční	k2eAgFnSc2d1
definice	definice	k1gFnSc2
inteligence	inteligence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Věděl	vědět	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
koncept	koncept	k1gInSc1
inteligence	inteligence	k1gFnSc2
<g/>
,	,	kIx,
<g/>
tak	tak	k6eAd1
jak	jak	k6eAd1
byla	být	k5eAaImAgFnS
definována	definovat	k5eAaBmNgFnS
předtím	předtím	k6eAd1
<g/>
,	,	kIx,
nezachytil	zachytit	k5eNaPmAgMnS
pravdivě	pravdivě	k6eAd1
a	a	k8xC
úplně	úplně	k6eAd1
veškeré	veškerý	k3xTgInPc1
způsoby	způsob	k1gInPc1
<g/>
,	,	kIx,
odvětví	odvětví	k1gNnPc1
a	a	k8xC
obory	obor	k1gInPc1
<g/>
,	,	kIx,
ve	v	k7c6
kterých	který	k3yIgInPc6,k3yQgInPc6,k3yRgInPc6
může	moct	k5eAaImIp3nS
člověk	člověk	k1gMnSc1
vynikat	vynikat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gardner	Gardner	k1gMnSc1
argumentoval	argumentovat	k5eAaImAgMnS
proti	proti	k7c3
názoru	názor	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
lidská	lidský	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
je	být	k5eAaImIp3nS
jednotná	jednotný	k2eAgFnSc1d1
<g/>
,	,	kIx,
s	s	k7c7
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
naopak	naopak	k6eAd1
mnohočetná	mnohočetný	k2eAgFnSc1d1
<g/>
,	,	kIx,
složená	složený	k2eAgFnSc1d1
z	z	k7c2
mnoha	mnoho	k4c2
různých	různý	k2eAgInPc2d1
druhů	druh	k1gInPc2
inteligencí	inteligence	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
součástí	součást	k1gFnSc7
nezávislého	závislý	k2eNgInSc2d1
systému	systém	k1gInSc2
v	v	k7c6
mozku	mozek	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gardner	Gardner	k1gInSc1
kladl	klást	k5eAaImAgInS
menší	malý	k2eAgInSc1d2
důraz	důraz	k1gInSc1
na	na	k7c4
vysvětlování	vysvětlování	k1gNnSc4
výsledků	výsledek	k1gInPc2
testů	test	k1gMnPc2
inteligence	inteligence	k1gFnSc2
než	než	k8xS
na	na	k7c4
rozsah	rozsah	k1gInSc4
lidských	lidský	k2eAgFnPc2d1
schopností	schopnost	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
existují	existovat	k5eAaImIp3nP
v	v	k7c6
drtivé	drtivý	k2eAgFnSc6d1
většině	většina	k1gFnSc6
různých	různý	k2eAgFnPc2d1
kultur	kultura	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gardner	Gardner	k1gMnSc1
došel	dojít	k5eAaPmAgMnS
k	k	k7c3
závěrům	závěr	k1gInPc3
díky	díky	k7c3
mnoha	mnoho	k4c3
různým	různý	k2eAgInPc3d1
zdrojům	zdroj	k1gInPc3
a	a	k8xC
důkazům	důkaz	k1gInPc3
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
mu	on	k3xPp3gMnSc3
pomohly	pomoct	k5eAaPmAgInP
v	v	k7c6
upřesnění	upřesnění	k1gNnSc6
počtu	počet	k1gInSc2
inteligencí	inteligence	k1gFnSc7
v	v	k7c6
této	tento	k3xDgFnSc6
teorii	teorie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedním	jeden	k4xCgInSc7
z	z	k7c2
příkladů	příklad	k1gInPc2
je	být	k5eAaImIp3nS
studie	studie	k1gFnSc1
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
udělal	udělat	k5eAaPmAgMnS
na	na	k7c4
osoby	osoba	k1gFnPc4
s	s	k7c7
poškozením	poškození	k1gNnSc7
mozku	mozek	k1gInSc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
ztratily	ztratit	k5eAaPmAgFnP
jednu	jeden	k4xCgFnSc4
schopnost	schopnost	k1gFnSc4
<g/>
,	,	kIx,
jako	jako	k8xS,k8xC
například	například	k6eAd1
prostorové	prostorový	k2eAgNnSc4d1
myšlení	myšlení	k1gNnSc4
<g/>
,	,	kIx,
ale	ale	k8xC
dokázaly	dokázat	k5eAaPmAgFnP
si	se	k3xPyFc3
zachovat	zachovat	k5eAaPmF
dovednosti	dovednost	k1gFnPc1
jiné	jiný	k2eAgFnPc1d1
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
například	například	k6eAd1
jazykové	jazykový	k2eAgFnPc4d1
nebo	nebo	k8xC
motorické	motorický	k2eAgFnPc4d1
funkce	funkce	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Věřil	věřit	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
když	když	k8xS
dvě	dva	k4xCgFnPc1
rozdílné	rozdílný	k2eAgFnPc1d1
dovednosti	dovednost	k1gFnPc1
dokáží	dokázat	k5eAaPmIp3nP
operovat	operovat	k5eAaImF
bez	bez	k7c2
vzájemné	vzájemný	k2eAgFnSc2d1
závislosti	závislost	k1gFnSc2
<g/>
,	,	kIx,
tak	tak	k8xC,k8xS
to	ten	k3xDgNnSc1
potvrzuje	potvrzovat	k5eAaImIp3nS
jeho	jeho	k3xOp3gFnSc4
tezi	teze	k1gFnSc4
o	o	k7c6
mnohačetné	mnohačetný	k2eAgFnSc6d1
inteligenci	inteligence	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Typy	typ	k1gInPc1
inteligence	inteligence	k1gFnSc2
podle	podle	k7c2
Howarda	Howard	k1gMnSc2
Gardnera	Gardner	k1gMnSc2
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jazykově-verbální	Jazykově-verbální	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
–	–	k?
Schopnost	schopnost	k1gFnSc1
učit	učit	k5eAaImF
se	se	k3xPyFc4
jazyky	jazyk	k1gInPc1
a	a	k8xC
užití	užití	k1gNnSc1
vlastních	vlastní	k2eAgFnPc2d1
jazykových	jazykový	k2eAgFnPc2d1
schopností	schopnost	k1gFnPc2
efektivně	efektivně	k6eAd1
k	k	k7c3
dosažení	dosažení	k1gNnSc3
různých	různý	k2eAgInPc2d1
cílů	cíl	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jednoduše	jednoduše	k6eAd1
řečeno	řečen	k2eAgNnSc1d1
je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
dovednost	dovednost	k1gFnSc1
mluveného	mluvený	k2eAgMnSc2d1
<g/>
,	,	kIx,
psaného	psaný	k2eAgInSc2d1
projevu	projev	k1gInSc2
a	a	k8xC
efektivního	efektivní	k2eAgNnSc2d1
používání	používání	k1gNnSc2
slov	slovo	k1gNnPc2
a	a	k8xC
jazyka	jazyk	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osoby	osoba	k1gFnPc1
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
mají	mít	k5eAaImIp3nP
tuto	tento	k3xDgFnSc4
dovednost	dovednost	k1gFnSc4
značně	značně	k6eAd1
rozvinutu	rozvinut	k2eAgFnSc4d1
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
často	často	k6eAd1
spisovatelé	spisovatel	k1gMnPc1
<g/>
,	,	kIx,
básníci	básník	k1gMnPc1
<g/>
,	,	kIx,
právníci	právník	k1gMnPc1
<g/>
,	,	kIx,
učitelé	učitel	k1gMnPc1
atp.	atp.	kA
</s>
<s>
Logicko-matematická	logicko-matematický	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
–	–	k?
Je	být	k5eAaImIp3nS
to	ten	k3xDgNnSc1
schopnost	schopnost	k1gFnSc1
analyzovat	analyzovat	k5eAaImF
problémy	problém	k1gInPc4
logicky	logicky	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahrnuje	zahrnovat	k5eAaImIp3nS
schopnosti	schopnost	k1gFnPc4
jako	jako	k8xS,k8xC
řešení	řešení	k1gNnSc4
matematických	matematický	k2eAgFnPc2d1
operací	operace	k1gFnPc2
(	(	kIx(
<g/>
sčítání	sčítání	k1gNnSc1
<g/>
,	,	kIx,
odčítání	odčítání	k1gNnSc1
<g/>
…	…	k?
<g/>
)	)	kIx)
a	a	k8xC
řešení	řešení	k1gNnSc4
složitých	složitý	k2eAgFnPc2d1
matematických	matematický	k2eAgFnPc2d1
úloh	úloha	k1gFnPc2
a	a	k8xC
problémů	problém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysoká	vysoký	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
tohoto	tento	k3xDgInSc2
typu	typ	k1gInSc2
pomáhá	pomáhat	k5eAaImIp3nS
například	například	k6eAd1
v	v	k7c6
rychlém	rychlý	k2eAgNnSc6d1
posouzení	posouzení	k1gNnSc6
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
se	se	k3xPyFc4
vyplatí	vyplatit	k5eAaPmIp3nS
<g/>
,	,	kIx,
nebo	nebo	k8xC
v	v	k7c6
odhadu	odhad	k1gInSc6
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
jaká	jaký	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
politická	politický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
má	mít	k5eAaImIp3nS
největší	veliký	k2eAgFnSc4d3
šanci	šance	k1gFnSc4
získat	získat	k5eAaPmF
moc	moc	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Vizuálně-prostorová	Vizuálně-prostorový	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
–	–	k?
Vztahuje	vztahovat	k5eAaImIp3nS
se	se	k3xPyFc4
na	na	k7c4
schopnost	schopnost	k1gFnSc4
vnímání	vnímání	k1gNnSc1
a	a	k8xC
rozlišování	rozlišování	k1gNnSc1
barev	barva	k1gFnPc2
<g/>
,	,	kIx,
tvarů	tvar	k1gInPc2
<g/>
,	,	kIx,
velikostí	velikost	k1gFnPc2
a	a	k8xC
vzdáleností	vzdálenost	k1gFnPc2
mezi	mezi	k7c7
předměty	předmět	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Osoby	osoba	k1gFnSc2
nadané	nadaný	k2eAgInPc4d1
v	v	k7c6
tomto	tento	k3xDgInSc6
směru	směr	k1gInSc6
jsou	být	k5eAaImIp3nP
schopny	schopen	k2eAgFnPc1d1
rozeznat	rozeznat	k5eAaPmF
a	a	k8xC
zapamatovat	zapamatovat	k5eAaPmF
si	se	k3xPyFc3
různé	různý	k2eAgInPc4d1
obrazce	obrazec	k1gInPc4
<g/>
,	,	kIx,
mají	mít	k5eAaImIp3nP
trojrozměrnou	trojrozměrný	k2eAgFnSc4d1
představivost	představivost	k1gFnSc4
a	a	k8xC
výbornou	výborný	k2eAgFnSc4d1
orientaci	orientace	k1gFnSc4
v	v	k7c6
prostoru	prostor	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejčastějšími	častý	k2eAgInPc7d3
obory	obor	k1gInPc7
lidí	člověk	k1gMnPc2
s	s	k7c7
vysokou	vysoký	k2eAgFnSc7d1
inteligencí	inteligence	k1gFnSc7
tohoto	tento	k3xDgInSc2
typu	typ	k1gInSc2
jsou	být	k5eAaImIp3nP
v	v	k7c6
oblasti	oblast	k1gFnSc6
architektury	architektura	k1gFnSc2
<g/>
,	,	kIx,
stavebnictví	stavebnictví	k1gNnSc1
<g/>
,	,	kIx,
kartografie	kartografie	k1gFnSc1
<g/>
,	,	kIx,
geografie	geografie	k1gFnSc1
<g/>
,	,	kIx,
řízení	řízení	k1gNnSc1
lodí	loď	k1gFnPc2
či	či	k8xC
letadel	letadlo	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s>
Zvukově-hudební	Zvukově-hudební	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
–	–	k?
Tato	tento	k3xDgFnSc1
inteligence	inteligence	k1gFnSc1
představuje	představovat	k5eAaImIp3nS
schopnosti	schopnost	k1gFnPc4
tvořit	tvořit	k5eAaImF
<g/>
,	,	kIx,
vnímat	vnímat	k5eAaImF
a	a	k8xC
interpretovat	interpretovat	k5eAaBmF
hudbu	hudba	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hudebně	hudebně	k6eAd1
nadaní	nadaný	k2eAgMnPc1d1
lidé	člověk	k1gMnPc1
jsou	být	k5eAaImIp3nP
úspěšní	úspěšný	k2eAgMnPc1d1
ve	v	k7c6
skládání	skládání	k1gNnSc6
<g/>
,	,	kIx,
zpívání	zpívání	k1gNnSc6
<g/>
,	,	kIx,
hraní	hraní	k1gNnSc6
<g/>
,	,	kIx,
dirigování	dirigování	k1gNnSc6
a	a	k8xC
tanci	tanec	k1gInSc6
<g/>
,	,	kIx,
hudba	hudba	k1gFnSc1
je	být	k5eAaImIp3nS
důležitou	důležitý	k2eAgFnSc7d1
součástí	součást	k1gFnSc7
jejich	jejich	k3xOp3gInSc2
života	život	k1gInSc2
a	a	k8xC
nezbytnou	zbytný	k2eNgFnSc7d1,k2eAgFnSc7d1
kulisou	kulisa	k1gFnSc7
každodenních	každodenní	k2eAgFnPc2d1
aktivit	aktivita	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tato	tento	k3xDgFnSc1
inteligence	inteligence	k1gFnSc1
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
několika	několik	k4yIc2
mozkových	mozkový	k2eAgNnPc2d1
center	centrum	k1gNnPc2
–	–	k?
jedno	jeden	k4xCgNnSc1
reguluje	regulovat	k5eAaImIp3nS
hudební	hudební	k2eAgFnSc4d1
tvorbu	tvorba	k1gFnSc4
<g/>
,	,	kIx,
druhé	druhý	k4xOgFnSc3
interpretaci	interpretace	k1gFnSc3
<g/>
,	,	kIx,
třetí	třetí	k4xOgNnSc4
rozlišování	rozlišování	k1gNnSc4
melodií	melodie	k1gFnPc2
a	a	k8xC
tónin	tónina	k1gFnPc2
a	a	k8xC
čtvrté	čtvrtá	k1gFnSc2
rytmus	rytmus	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Tělesně-pohybová	Tělesně-pohybový	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
–	–	k?
Odkazuje	odkazovat	k5eAaImIp3nS
na	na	k7c4
schopnost	schopnost	k1gFnSc4
jedince	jedinko	k6eAd1
použít	použít	k5eAaPmF
mentálních	mentální	k2eAgFnPc2d1
dovedností	dovednost	k1gFnPc2
ke	k	k7c3
koordinaci	koordinace	k1gFnSc3
tělesných	tělesný	k2eAgInPc2d1
pohybů	pohyb	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Je	být	k5eAaImIp3nS
nazývána	nazývat	k5eAaImNgFnS
také	také	k9
jako	jako	k9
motorická	motorický	k2eAgFnSc1d1
a	a	k8xC
veškeré	veškerý	k3xTgInPc1
tělesné	tělesný	k2eAgInPc1d1
pohyby	pohyb	k1gInPc1
<g/>
,	,	kIx,
ať	ať	k8xS,k8xC
už	už	k6eAd1
snazší	snadný	k2eAgMnSc1d2
<g/>
,	,	kIx,
či	či	k8xC
obtížnější	obtížný	k2eAgMnSc1d2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
vykonávány	vykonávat	k5eAaImNgInP
díky	díky	k7c3
této	tento	k3xDgFnSc3
inteligenci	inteligence	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Atleti	atlet	k1gMnPc1
<g/>
,	,	kIx,
sportovci	sportovec	k1gMnPc1
<g/>
,	,	kIx,
tanečníci	tanečník	k1gMnPc1
<g/>
,	,	kIx,
chirurgové	chirurg	k1gMnPc1
a	a	k8xC
další	další	k2eAgFnSc7d1
většinou	většina	k1gFnSc7
vykazují	vykazovat	k5eAaImIp3nP
vysokou	vysoký	k2eAgFnSc4d1
úroveň	úroveň	k1gFnSc4
této	tento	k3xDgFnSc2
inteligence	inteligence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Společenská	společenský	k2eAgFnSc1d1
neboli	neboli	k8xC
interpersonální	interpersonální	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
–	–	k?
Určuje	určovat	k5eAaImIp3nS
schopnost	schopnost	k1gFnSc4
vycházet	vycházet	k5eAaImF
s	s	k7c7
ostatními	ostatní	k2eAgMnPc7d1
lidmi	člověk	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zahrnuje	zahrnovat	k5eAaImIp3nS
jedincovu	jedincův	k2eAgFnSc4d1
dovednost	dovednost	k1gFnSc4
pochopit	pochopit	k5eAaPmF
záměry	záměr	k1gInPc4
<g/>
,	,	kIx,
motivace	motivace	k1gFnPc4
a	a	k8xC
touhy	touha	k1gFnPc4
druhých	druhý	k4xOgMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Člověk	člověk	k1gMnSc1
s	s	k7c7
vysokou	vysoký	k2eAgFnSc7d1
společenskou	společenský	k2eAgFnSc7d1
inteligencí	inteligence	k1gFnSc7
se	se	k3xPyFc4
velice	velice	k6eAd1
dobře	dobře	k6eAd1
socializuje	socializovat	k5eAaBmIp3nS
<g/>
,	,	kIx,
snadno	snadno	k6eAd1
získává	získávat	k5eAaImIp3nS
druhé	druhý	k4xOgNnSc4
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
stranu	strana	k1gFnSc4
a	a	k8xC
je	být	k5eAaImIp3nS
velice	velice	k6eAd1
schopný	schopný	k2eAgMnSc1d1
v	v	k7c6
manipulaci	manipulace	k1gFnSc6
ostatními	ostatní	k2eAgFnPc7d1
osobami	osoba	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidé	člověk	k1gMnPc1
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
disponují	disponovat	k5eAaBmIp3nP
vysokou	vysoký	k2eAgFnSc7d1
úrovní	úroveň	k1gFnSc7
interpersonální	interpersonální	k2eAgFnSc2d1
inteligence	inteligence	k1gFnSc2
<g/>
,	,	kIx,
pracují	pracovat	k5eAaImIp3nP
většinou	většina	k1gFnSc7
v	v	k7c6
politice	politika	k1gFnSc6
<g/>
,	,	kIx,
marketingu	marketing	k1gInSc6
<g/>
,	,	kIx,
právničině	právničina	k1gFnSc6
<g/>
,	,	kIx,
psychologii	psychologie	k1gFnSc6
atp.	atp.	kA
</s>
<s>
Vnitřní	vnitřní	k2eAgFnSc1d1
neboli	neboli	k8xC
intrapersonální	intrapersonální	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
–	–	k?
Slouží	sloužit	k5eAaImIp3nS
k	k	k7c3
porozumění	porozumění	k1gNnSc3
sebe	sebe	k3xPyFc4
samému	samý	k3xTgMnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Díky	díky	k7c3
této	tento	k3xDgFnSc3
inteligenci	inteligence	k1gFnSc3
jsme	být	k5eAaImIp1nP
schopni	schopen	k2eAgMnPc1d1
se	se	k3xPyFc4
naučit	naučit	k5eAaPmF
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yQgFnPc1,k3yRgFnPc1
situace	situace	k1gFnPc1
v	v	k7c4
nás	my	k3xPp1nPc4
vyvolávají	vyvolávat	k5eAaImIp3nP
různé	různý	k2eAgFnPc1d1
emoce	emoce	k1gFnPc1
<g/>
,	,	kIx,
pocity	pocit	k1gInPc1
a	a	k8xC
myšlenky	myšlenka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysoce	vysoce	k6eAd1
rozvinutá	rozvinutý	k2eAgFnSc1d1
forma	forma	k1gFnSc1
této	tento	k3xDgFnSc2
inteligence	inteligence	k1gFnSc2
nám	my	k3xPp1nPc3
umožňuje	umožňovat	k5eAaImIp3nS
zvládnout	zvládnout	k5eAaPmF
stresové	stresový	k2eAgFnPc4d1
situace	situace	k1gFnPc4
a	a	k8xC
stát	stát	k5eAaImF,k5eAaPmF
se	se	k3xPyFc4
vyrovnanějším	vyrovnaný	k2eAgMnSc7d2
člověkem	člověk	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s>
Přírodní	přírodní	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
–	–	k?
Touto	tento	k3xDgFnSc7
inteligencí	inteligence	k1gFnSc7
jsou	být	k5eAaImIp3nP
obdařeni	obdařen	k2eAgMnPc1d1
biologové	biolog	k1gMnPc1
<g/>
,	,	kIx,
zoologové	zoolog	k1gMnPc1
<g/>
,	,	kIx,
botanici	botanik	k1gMnPc1
<g/>
,	,	kIx,
ekologové	ekolog	k1gMnPc1
i	i	k8xC
obyčejní	obyčejný	k2eAgMnPc1d1
milovníci	milovník	k1gMnPc1
přírody	příroda	k1gFnSc2
<g/>
,	,	kIx,
rostlin	rostlina	k1gFnPc2
a	a	k8xC
živočichů	živočich	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Její	její	k3xOp3gNnSc1
výše	vysoce	k6eAd2
ovlivňuje	ovlivňovat	k5eAaImIp3nS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
zda	zda	k8xS
vyhazujeme	vyhazovat	k5eAaImIp1nP
plast	plast	k1gInSc4
do	do	k7c2
smíšeného	smíšený	k2eAgInSc2d1
odpadu	odpad	k1gInSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
ne	ne	k9
<g/>
,	,	kIx,
zda	zda	k8xS
kupujeme	kupovat	k5eAaImIp1nP
výrobky	výrobek	k1gInPc4
z	z	k7c2
pravé	pravý	k2eAgFnSc2d1
kožešiny	kožešina	k1gFnSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
testované	testovaný	k2eAgMnPc4d1
na	na	k7c6
zvířatech	zvíře	k1gNnPc6
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Další	další	k2eAgFnPc1d1
teorie	teorie	k1gFnPc1
</s>
<s>
David	David	k1gMnSc1
Perkins	Perkins	k1gInSc4
udělal	udělat	k5eAaPmAgMnS
mnoho	mnoho	k4c4
výzkumných	výzkumný	k2eAgNnPc2d1
studií	studio	k1gNnPc2
jak	jak	k8xS,k8xC
na	na	k7c4
měření	měření	k1gNnSc4
IQ	iq	kA
<g/>
,	,	kIx,
tak	tak	k9
na	na	k7c4
různé	různý	k2eAgInPc4d1
studijní	studijní	k2eAgInPc4d1
programy	program	k1gInPc4
navržených	navržený	k2eAgMnPc2d1
za	za	k7c7
účelem	účel	k1gInSc7
zvýšení	zvýšení	k1gNnSc2
IQ	iq	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Prezentuje	prezentovat	k5eAaBmIp3nS
tři	tři	k4xCgFnPc4
základní	základní	k2eAgFnPc4d1
komponenty	komponenta	k1gFnPc4
<g/>
,	,	kIx,
resp.	resp.	kA
dimenze	dimenze	k1gFnSc2
IQ	iq	kA
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nervová	nervový	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
–	–	k?
odkazuje	odkazovat	k5eAaImIp3nS
na	na	k7c4
efektivnost	efektivnost	k1gFnSc4
a	a	k8xC
preciznost	preciznost	k1gFnSc4
jedincova	jedincův	k2eAgInSc2d1
nervového	nervový	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Zkušenostní	zkušenostní	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
–	–	k?
je	být	k5eAaImIp3nS
druh	druh	k1gInSc4
inteligence	inteligence	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yQgInSc1,k3yIgInSc1,k3yRgInSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
vědomosti	vědomost	k1gFnPc4
a	a	k8xC
zkušenosti	zkušenost	k1gFnPc4
v	v	k7c6
různých	různý	k2eAgFnPc6d1
oblastech	oblast	k1gFnPc6
daného	daný	k2eAgMnSc2d1
jedince	jedinec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Reflektivní	reflektivní	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
–	–	k?
se	se	k3xPyFc4
vztahuje	vztahovat	k5eAaImIp3nS
k	k	k7c3
jedincově	jedincův	k2eAgFnSc3d1
schopnosti	schopnost	k1gFnSc3
vytvářet	vytvářet	k5eAaImF
strategie	strategie	k1gFnPc4
na	na	k7c4
řešení	řešení	k1gNnSc4
problémů	problém	k1gInPc2
<g/>
,	,	kIx,
učení	učení	k1gNnSc1
a	a	k8xC
blížící	blížící	k2eAgInPc1d1
se	se	k3xPyFc4
testy	test	k1gInPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jejími	její	k3xOp3gFnPc7
součástmi	součást	k1gFnPc7
jsou	být	k5eAaImIp3nP
vytrvalost	vytrvalost	k1gFnSc4
<g/>
,	,	kIx,
systematizace	systematizace	k1gFnSc1
a	a	k8xC
představivost	představivost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Kognitivní	kognitivní	k2eAgFnPc1d1
vědy	věda	k1gFnPc1
či	či	k8xC
informatika	informatika	k1gFnSc1
definují	definovat	k5eAaBmIp3nP
inteligentní	inteligentní	k2eAgInSc4d1
systém	systém	k1gInSc4
obecně	obecně	k6eAd1
<g/>
,	,	kIx,
bez	bez	k7c2
nutného	nutný	k2eAgInSc2d1
vztahu	vztah	k1gInSc2
k	k	k7c3
živým	živý	k2eAgInPc3d1
organismům	organismus	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
nich	on	k3xPp3gMnPc2
musí	muset	k5eAaImIp3nS
inteligentní	inteligentní	k2eAgNnSc1d1
systém	systém	k1gInSc4
být	být	k5eAaImF
schopen	schopen	k2eAgMnSc1d1
reagovat	reagovat	k5eAaBmF
na	na	k7c4
měnící	měnící	k2eAgFnPc4d1
se	se	k3xPyFc4
podmínky	podmínka	k1gFnPc4
prostředí	prostředí	k1gNnSc2
<g/>
,	,	kIx,
tj.	tj.	kA
<g/>
:	:	kIx,
</s>
<s>
zajistit	zajistit	k5eAaPmF
si	se	k3xPyFc3
vlastní	vlastní	k2eAgFnSc4d1
schopnost	schopnost	k1gFnSc4
přežití	přežití	k1gNnSc2
</s>
<s>
zajistit	zajistit	k5eAaPmF
si	se	k3xPyFc3
vlastní	vlastní	k2eAgFnSc4d1
schopnost	schopnost	k1gFnSc4
reprodukce	reprodukce	k1gFnSc2
</s>
<s>
být	být	k5eAaImF
cílově	cílově	k6eAd1
orientovaný	orientovaný	k2eAgMnSc1d1
a	a	k8xC
mít	mít	k5eAaImF
schopnost	schopnost	k1gFnSc4
dosažení	dosažení	k1gNnSc2
tohoto	tento	k3xDgInSc2
cíle	cíl	k1gInSc2
</s>
<s>
být	být	k5eAaImF
schopen	schopen	k2eAgMnSc1d1
učit	učit	k5eAaImF
se	se	k3xPyFc4
</s>
<s>
Měření	měření	k1gNnSc1
inteligence	inteligence	k1gFnSc2
</s>
<s>
Podrobnější	podrobný	k2eAgFnPc4d2
informace	informace	k1gFnPc4
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
v	v	k7c6
článcích	článek	k1gInPc6
Inteligenční	inteligenční	k2eAgInSc4d1
test	test	k1gInSc4
a	a	k8xC
Inteligenční	inteligenční	k2eAgInSc4d1
kvocient	kvocient	k1gInSc4
<g/>
.	.	kIx.
</s>
<s>
Měření	měření	k1gNnSc1
inteligence	inteligence	k1gFnSc2
je	být	k5eAaImIp3nS
stejně	stejně	k6eAd1
problematické	problematický	k2eAgNnSc1d1
jako	jako	k8xC,k8xS
definice	definice	k1gFnSc1
inteligence	inteligence	k1gFnSc2
samotná	samotný	k2eAgFnSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Nejjednodušší	jednoduchý	k2eAgInSc1d3
model	model	k1gInSc1
navrhl	navrhnout	k5eAaPmAgMnS
Charles	Charles	k1gMnSc1
Spearman	Spearman	k1gMnSc1
(	(	kIx(
<g/>
1927	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
podle	podle	k7c2
kterého	který	k3yRgInSc2,k3yQgInSc2,k3yIgInSc2
existuje	existovat	k5eAaImIp3nS
obecná	obecný	k2eAgFnSc1d1
rozumová	rozumový	k2eAgFnSc1d1
schopnost	schopnost	k1gFnSc1
<g/>
,	,	kIx,
jediná	jediný	k2eAgFnSc1d1
veličina	veličina	k1gFnSc1
<g/>
,	,	kIx,
jejíž	jejíž	k3xOyRp3gFnSc1
hodnota	hodnota	k1gFnSc1
určuje	určovat	k5eAaImIp3nS
schopnost	schopnost	k1gFnSc4
řešit	řešit	k5eAaImF
veškeré	veškerý	k3xTgInPc4
typy	typ	k1gInPc4
problémů	problém	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Při	při	k7c6
sestavování	sestavování	k1gNnSc6
IQ	iq	kA
testu	test	k1gInSc2
se	se	k3xPyFc4
nejčastěji	často	k6eAd3
používají	používat	k5eAaImIp3nP
tzv.	tzv.	kA
Ravenovy	Ravenův	k2eAgFnSc2d1
matice	matice	k1gFnSc2
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Z	z	k7c2
výzkumů	výzkum	k1gInPc2
nejspíše	nejspíše	k9
(	(	kIx(
<g/>
oproti	oproti	k7c3
konkurenčním	konkurenční	k2eAgInPc3d1
modelům	model	k1gInPc3
<g/>
)	)	kIx)
vyplývá	vyplývat	k5eAaImIp3nS
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
co	co	k3yQnSc1,k3yRnSc1,k3yInSc1
od	od	k7c2
IQ	iq	kA
testu	test	k1gInSc6
očekáváme	očekávat	k5eAaImIp1nP
<g/>
,	,	kIx,
výsledná	výsledný	k2eAgFnSc1d1
hodnota	hodnota	k1gFnSc1
zahrnuje	zahrnovat	k5eAaImIp3nS
jak	jak	k6eAd1
jazykové	jazykový	k2eAgNnSc4d1
<g/>
,	,	kIx,
tak	tak	k6eAd1
matematické	matematický	k2eAgFnPc4d1
a	a	k8xC
kognitivní	kognitivní	k2eAgFnPc4d1
schopnosti	schopnost	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pravým	pravý	k2eAgInSc7d1
opakem	opak	k1gInSc7
této	tento	k3xDgFnSc2
teorie	teorie	k1gFnSc2
je	být	k5eAaImIp3nS
model	model	k1gInSc1
inteligence	inteligence	k1gFnSc2
J.	J.	kA
P.	P.	kA
Guilforda	Guilforda	k1gMnSc1
(	(	kIx(
<g/>
1967	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
ji	on	k3xPp3gFnSc4
rozčleňuje	rozčleňovat	k5eAaImIp3nS
do	do	k7c2
tří	tři	k4xCgFnPc2
kategorií	kategorie	k1gFnPc2
podle	podle	k7c2
sféry	sféra	k1gFnSc2
činnosti	činnost	k1gFnSc2
mozku	mozek	k1gInSc2
<g/>
:	:	kIx,
Vnímání	vnímání	k1gNnSc1
obsahu	obsah	k1gInSc2
<g/>
,	,	kIx,
zpracování	zpracování	k1gNnSc4
informací	informace	k1gFnPc2
a	a	k8xC
vyvozování	vyvozování	k1gNnSc2
závislostí	závislost	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Mentální	mentální	k2eAgFnPc1d1
operace	operace	k1gFnPc1
</s>
<s>
Obsah	obsah	k1gInSc1
</s>
<s>
Produkty	produkt	k1gInPc1
</s>
<s>
Hodnocení	hodnocení	k1gNnSc1
</s>
<s>
Obrazový	obrazový	k2eAgInSc1d1
</s>
<s>
Prvky	prvek	k1gInPc1
</s>
<s>
Odvození	odvození	k1gNnSc1
</s>
<s>
Symbolický	symbolický	k2eAgInSc4d1
</s>
<s>
Třídy	třída	k1gFnPc1
</s>
<s>
Kreativita	kreativita	k1gFnSc1
</s>
<s>
Sémantický	sémantický	k2eAgInSc1d1
</s>
<s>
Vztahy	vztah	k1gInPc1
</s>
<s>
Paměť	paměť	k1gFnSc1
</s>
<s>
Chování	chování	k1gNnSc1
</s>
<s>
Systémy	systém	k1gInPc1
</s>
<s>
Poznání	poznání	k1gNnSc1
</s>
<s>
Transformace	transformace	k1gFnSc1
</s>
<s>
Implikace	implikace	k1gFnSc1
</s>
<s>
Vzájemnou	vzájemný	k2eAgFnSc7d1
kombinací	kombinace	k1gFnSc7
všech	všecek	k3xTgInPc2
hodnocených	hodnocený	k2eAgInPc2d1
faktorů	faktor	k1gInPc2
dostaneme	dostat	k5eAaPmIp1nP
celkem	celkem	k6eAd1
120	#num#	k4
samostatných	samostatný	k2eAgFnPc2d1
složek	složka	k1gFnPc2
inteligence	inteligence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
kritiků	kritik	k1gMnPc2
to	ten	k3xDgNnSc1
svědčí	svědčit	k5eAaImIp3nS
o	o	k7c6
neúspěchu	neúspěch	k1gInSc6
testu	test	k1gInSc2
určit	určit	k5eAaPmF
obecné	obecný	k2eAgNnSc4d1
vlastnosti	vlastnost	k1gFnPc4
inteligence	inteligence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
S	s	k7c7
podobným	podobný	k2eAgInSc7d1
<g/>
,	,	kIx,
ale	ale	k8xC
jednodušším	jednoduchý	k2eAgInSc7d2
modelem	model	k1gInSc7
přišel	přijít	k5eAaPmAgMnS
H.	H.	kA
J.	J.	kA
Eysenck	Eysenck	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
již	již	k6eAd1
vycházel	vycházet	k5eAaImAgMnS
z	z	k7c2
konkrétních	konkrétní	k2eAgNnPc2d1
dat	datum	k1gNnPc2
z	z	k7c2
proběhlých	proběhlý	k2eAgInPc2d1
testů	test	k1gInPc2
inteligence	inteligence	k1gFnSc2
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mentální	mentální	k2eAgFnPc1d1
operace	operace	k1gFnPc1
</s>
<s>
Druhy	druh	k1gInPc1
testovaných	testovaný	k2eAgFnPc2d1
úloh	úloha	k1gFnPc2
</s>
<s>
Kvalita	kvalita	k1gFnSc1
</s>
<s>
Vnímání	vnímání	k1gNnSc1
</s>
<s>
Verbální	verbální	k2eAgFnSc1d1
</s>
<s>
Výkonnost	výkonnost	k1gFnSc1
</s>
<s>
Paměť	paměť	k1gFnSc1
</s>
<s>
Numerické	numerický	k2eAgNnSc1d1
</s>
<s>
Rychlost	rychlost	k1gFnSc1
</s>
<s>
Vyvozování	vyvozování	k1gNnSc1
</s>
<s>
Prostorové	prostorový	k2eAgNnSc1d1
</s>
<s>
Některé	některý	k3yIgInPc1
modely	model	k1gInPc1
přistupují	přistupovat	k5eAaImIp3nP
k	k	k7c3
problému	problém	k1gInSc3
i	i	k8xC
z	z	k7c2
hlediska	hledisko	k1gNnSc2
obecné	obecný	k2eAgFnSc2d1
funkce	funkce	k1gFnSc2
centrálního	centrální	k2eAgInSc2d1
nervového	nervový	k2eAgInSc2d1
systému	systém	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Gardnera	Gardner	k1gMnSc2
by	by	kYmCp3nS
měla	mít	k5eAaImAgFnS
například	například	k6eAd1
obecná	obecný	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
zahrnovat	zahrnovat	k5eAaImF
inteligenci	inteligence	k1gFnSc4
jazykově-verbální	jazykově-verbální	k2eAgFnSc4d1
<g/>
,	,	kIx,
hudební	hudební	k2eAgFnSc4d1
<g/>
,	,	kIx,
logicko-matematickou	logicko-matematický	k2eAgFnSc4d1
<g/>
,	,	kIx,
tělesně-pohybovou	tělesně-pohybový	k2eAgFnSc4d1
<g/>
,	,	kIx,
vizuálně-prostorovou	vizuálně-prostorový	k2eAgFnSc4d1
<g/>
,	,	kIx,
intrapersonální	intrapersonální	k2eAgFnSc4d1
<g/>
,	,	kIx,
interpersonální	interpersonální	k2eAgFnSc4d1
a	a	k8xC
přírodní	přírodní	k2eAgFnSc4d1
inteligenci	inteligence	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Gardnerova	Gardnerův	k2eAgFnSc1d1
teorie	teorie	k1gFnSc1
však	však	k9
nemá	mít	k5eNaImIp3nS
reálné	reálný	k2eAgInPc4d1
vědecké	vědecký	k2eAgInPc4d1
základy	základ	k1gInPc4
<g/>
,	,	kIx,
není	být	k5eNaImIp3nS
založena	založit	k5eAaPmNgFnS
na	na	k7c6
faktech	fakt	k1gInPc6
a	a	k8xC
nepopisuje	popisovat	k5eNaImIp3nS
dobře	dobře	k6eAd1
skutečné	skutečný	k2eAgNnSc1d1
fungování	fungování	k1gNnSc1
lidského	lidský	k2eAgInSc2d1
mozku	mozek	k1gInSc2
a	a	k8xC
mysli	mysl	k1gFnSc2
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s>
Psychologové	psycholog	k1gMnPc1
se	se	k3xPyFc4
dnes	dnes	k6eAd1
shodnou	shodnout	k5eAaBmIp3nP,k5eAaPmIp3nP
na	na	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
že	že	k8xS
jako	jako	k9
kompletně	kompletně	k6eAd1
jiné	jiný	k2eAgFnPc1d1
vlastnosti	vlastnost	k1gFnPc1
musíme	muset	k5eAaImIp1nP
minimálně	minimálně	k6eAd1
oddělovat	oddělovat	k5eAaImF
inteligenci	inteligence	k1gFnSc4
abstraktní	abstraktní	k2eAgFnSc4d1
<g/>
,	,	kIx,
sociální	sociální	k2eAgInSc4d1
a	a	k8xC
emoční	emoční	k2eAgInSc4d1
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Současné	současný	k2eAgInPc1d1
testy	test	k1gInPc1
inteligence	inteligence	k1gFnSc2
se	se	k3xPyFc4
snaží	snažit	k5eAaImIp3nS
měřit	měřit	k5eAaImF
hodnotu	hodnota	k1gFnSc4
inteligence	inteligence	k1gFnSc2
abstraktní	abstraktní	k2eAgFnSc2d1
<g/>
,	,	kIx,
obecně	obecně	k6eAd1
kognitivních	kognitivní	k2eAgFnPc2d1
schopností	schopnost	k1gFnPc2
<g/>
.	.	kIx.
<g/>
Ukázka	ukázka	k1gFnSc1
testu	test	k1gInSc2
inteligence	inteligence	k1gFnSc2
</s>
<s>
Kalkulace	kalkulace	k1gFnSc1
IQ	iq	kA
</s>
<s>
Metoda	metoda	k1gFnSc1
výpočtu	výpočet	k1gInSc2
</s>
<s>
Inteligenční	inteligenční	k2eAgInSc1d1
kvocient	kvocient	k1gInSc1
v	v	k7c6
dnešním	dnešní	k2eAgNnSc6d1
pojetí	pojetí	k1gNnSc6
popisuje	popisovat	k5eAaImIp3nS
roku	rok	k1gInSc2
1912	#num#	k4
William	William	k1gInSc1
Stern	sternum	k1gNnPc2
[	[	kIx(
<g/>
štern	štern	k1gInSc1
<g/>
]	]	kIx)
jako	jako	k8xS,k8xC
procentuální	procentuální	k2eAgInSc1d1
poměr	poměr	k1gInSc1
oproti	oproti	k7c3
vrstevníkům	vrstevník	k1gMnPc3
<g/>
:	:	kIx,
</s>
<s>
I	i	k9
</s>
<s>
Q	Q	kA
</s>
<s>
=	=	kIx~
</s>
<s>
M	M	kA
</s>
<s>
e	e	k0
</s>
<s>
n	n	k0
</s>
<s>
t	t	k?
</s>
<s>
a	a	k8xC
</s>
<s>
l	l	kA
</s>
<s>
n	n	k0
</s>
<s>
i	i	k9
</s>
<s>
(	(	kIx(
</s>
<s>
V	v	k7c6
</s>
<s>
e	e	k0
</s>
<s>
k	k	k7c3
</s>
<s>
)	)	kIx)
</s>
<s>
C	C	kA
</s>
<s>
h	h	k?
</s>
<s>
r	r	kA
</s>
<s>
o	o	k7c6
</s>
<s>
n	n	k0
</s>
<s>
o	o	k7c6
</s>
<s>
l	l	kA
</s>
<s>
o	o	k7c6
</s>
<s>
g	g	kA
</s>
<s>
i	i	k9
</s>
<s>
c	c	k0
</s>
<s>
k	k	k7c3
</s>
<s>
y	y	k?
</s>
<s>
(	(	kIx(
</s>
<s>
V	v	k7c6
</s>
<s>
e	e	k0
</s>
<s>
k	k	k7c3
</s>
<s>
)	)	kIx)
</s>
<s>
∗	∗	k?
</s>
<s>
100	#num#	k4
</s>
<s>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
displaystyle	displaystyl	k1gInSc5
IQ	iq	kA
<g/>
=	=	kIx~
<g/>
{	{	kIx(
<g/>
\	\	kIx~
<g/>
frac	frac	k1gFnSc1
{	{	kIx(
<g/>
Mentalni	Mentalni	k1gMnSc1
<g/>
(	(	kIx(
<g/>
Vek	veka	k1gFnPc2
<g/>
)	)	kIx)
<g/>
}	}	kIx)
<g/>
{	{	kIx(
<g/>
Chronologicky	chronologicky	k6eAd1
<g/>
(	(	kIx(
<g/>
Vek	veka	k1gFnPc2
<g/>
)	)	kIx)
<g/>
}}	}}	k?
<g/>
*	*	kIx~
<g/>
100	#num#	k4
<g/>
}	}	kIx)
</s>
<s>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Úlohy	úloha	k1gFnPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
testu	test	k1gInSc6
rozčleněny	rozčleněn	k2eAgInPc4d1
do	do	k7c2
obtížností	obtížnost	k1gFnPc2
podle	podle	k7c2
toho	ten	k3xDgNnSc2
<g/>
,	,	kIx,
jak	jak	k8xC,k8xS
staří	starý	k2eAgMnPc1d1
jedinci	jedinec	k1gMnPc1
jsou	být	k5eAaImIp3nP
v	v	k7c6
průměru	průměr	k1gInSc6
schopni	schopen	k2eAgMnPc1d1
je	on	k3xPp3gNnSc4
zvládnout	zvládnout	k5eAaPmF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mentální	mentální	k2eAgInSc1d1
věk	věk	k1gInSc1
vyjadřuje	vyjadřovat	k5eAaImIp3nS
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
náročné	náročný	k2eAgFnPc4d1
úlohy	úloha	k1gFnPc4
byl	být	k5eAaImAgInS
testovaný	testovaný	k2eAgInSc1d1
schopen	schopen	k2eAgInSc1d1
adekvátně	adekvátně	k6eAd1
řešit	řešit	k5eAaImF
<g/>
.	.	kIx.
</s>
<s>
Chronologický	chronologický	k2eAgInSc1d1
věk	věk	k1gInSc1
vyjadřuje	vyjadřovat	k5eAaImIp3nS
pak	pak	k6eAd1
skutečný	skutečný	k2eAgInSc1d1
věk	věk	k1gInSc1
testovaného	testovaný	k2eAgMnSc2d1
jedince	jedinec	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Pokud	pokud	k8xS
by	by	kYmCp3nP
tedy	tedy	k9
desetileté	desetiletý	k2eAgNnSc1d1
dítě	dítě	k1gNnSc1
vyřešilo	vyřešit	k5eAaPmAgNnS
nanejvýš	nanejvýš	k6eAd1
úlohy	úloha	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
dokáže	dokázat	k5eAaPmIp3nS
vyřešit	vyřešit	k5eAaPmF
i	i	k9
většina	většina	k1gFnSc1
desetiletých	desetiletý	k2eAgFnPc2d1
<g/>
,	,	kIx,
pak	pak	k6eAd1
je	být	k5eAaImIp3nS
poměr	poměr	k1gInSc1
věků	věk	k1gInPc2
roven	roven	k2eAgInSc1d1
1	#num#	k4
a	a	k8xC
po	po	k7c6
vynásobení	vynásobení	k1gNnSc6
100	#num#	k4
zjistíme	zjistit	k5eAaPmIp1nP
<g/>
,	,	kIx,
že	že	k8xS
jeho	jeho	k3xOp3gFnSc1
IQ	iq	kA
je	být	k5eAaImIp3nS
rovno	roven	k2eAgNnSc4d1
100	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kdyby	kdyby	kYmCp3nS
desetileté	desetiletý	k2eAgNnSc1d1
dítě	dítě	k1gNnSc1
zvládalo	zvládat	k5eAaImAgNnS
úlohy	úloha	k1gFnPc4
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
odpovídají	odpovídat	k5eAaImIp3nP
mentálnímu	mentální	k2eAgInSc3d1
věku	věk	k1gInSc3
dětí	dítě	k1gFnPc2
mezi	mezi	k7c4
12	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
13	#num#	k4
<g/>
.	.	kIx.
rokem	rok	k1gInSc7
<g/>
,	,	kIx,
pak	pak	k6eAd1
jeho	on	k3xPp3gInSc4,k3xOp3gInSc4
IQ	iq	kA
bude	být	k5eAaImBp3nS
mít	mít	k5eAaImF
hodnotu	hodnota	k1gFnSc4
125	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Později	pozdě	k6eAd2
bylo	být	k5eAaImAgNnS
průměrné	průměrný	k2eAgNnSc1d1
IQ	iq	kA
stanoveno	stanoven	k2eAgNnSc1d1
na	na	k7c4
hodnotu	hodnota	k1gFnSc4
100	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
matematické	matematický	k2eAgFnSc6d1
stránce	stránka	k1gFnSc6
byla	být	k5eAaImAgFnS
provedena	proveden	k2eAgFnSc1d1
normalizace	normalizace	k1gFnSc1
rozložení	rozložení	k1gNnSc2
N	N	kA
(	(	kIx(
<g/>
100,15	100,15	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Sternův	Sternův	k2eAgInSc1d1
výpočet	výpočet	k1gInSc1
však	však	k9
nelze	lze	k6eNd1
používat	používat	k5eAaImF
u	u	k7c2
dospělých	dospělí	k1gMnPc2
<g/>
,	,	kIx,
protože	protože	k8xS
ačkoli	ačkoli	k8xS
chronologický	chronologický	k2eAgInSc1d1
věk	věk	k1gInSc1
roste	růst	k5eAaImIp3nS
lineárně	lineárně	k6eAd1
s	s	k7c7
časem	čas	k1gInSc7
<g/>
,	,	kIx,
mentální	mentální	k2eAgInPc4d1
věk	věk	k1gInSc4
nikoli	nikoli	k9
a	a	k8xC
do	do	k7c2
hry	hra	k1gFnSc2
vstupují	vstupovat	k5eAaImIp3nP
další	další	k2eAgInPc1d1
faktory	faktor	k1gInPc1
<g/>
,	,	kIx,
např.	např.	kA
životní	životní	k2eAgFnSc1d1
zkušenost	zkušenost	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
současnosti	současnost	k1gFnSc6
se	se	k3xPyFc4
preferuje	preferovat	k5eAaImIp3nS
odvozený	odvozený	k2eAgInSc1d1
kvocient	kvocient	k1gInSc1
označovaný	označovaný	k2eAgInSc1d1
jako	jako	k8xS,k8xC
deviační	deviační	k2eAgInSc1d1
nebo	nebo	k8xC
odchylkové	odchylka	k1gMnPc1
IQ	iq	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Úroveň	úroveň	k1gFnSc1
rozumových	rozumový	k2eAgFnPc2d1
schopností	schopnost	k1gFnPc2
jedince	jedinko	k6eAd1
se	se	k3xPyFc4
posuzuje	posuzovat	k5eAaImIp3nS
vzhledem	vzhledem	k7c3
k	k	k7c3
průměrnému	průměrný	k2eAgInSc3d1
IQ	iq	kA
v	v	k7c6
populaci	populace	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Asi	asi	k9
68	#num#	k4
%	%	kIx~
populace	populace	k1gFnSc2
má	mít	k5eAaImIp3nS
hodnotu	hodnota	k1gFnSc4
IQ	iq	kA
mezi	mezi	k7c7
85-115	85-115	k4
(	(	kIx(
<g/>
tj.	tj.	kA
průměr	průměr	k1gInSc1
+	+	kIx~
<g/>
/	/	kIx~
<g/>
-	-	kIx~
směrodatná	směrodatný	k2eAgFnSc1d1
odchylka	odchylka	k1gFnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vysoce	vysoce	k6eAd1
nadprůměrné	nadprůměrný	k2eAgInPc1d1
IQ	iq	kA
(	(	kIx(
<g/>
vyšší	vysoký	k2eAgFnSc4d2
než	než	k8xS
130	#num#	k4
<g/>
)	)	kIx)
má	mít	k5eAaImIp3nS
naopak	naopak	k6eAd1
jen	jen	k9
2,5	2,5	k4
%	%	kIx~
populace	populace	k1gFnSc2
<g/>
,	,	kIx,
totéž	týž	k3xTgNnSc1
platí	platit	k5eAaImIp3nS
i	i	k9
u	u	k7c2
podprůměrného	podprůměrný	k2eAgNnSc2d1
IQ	iq	kA
nižšího	nízký	k2eAgNnSc2d2
než	než	k8xS
70	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
současnosti	současnost	k1gFnSc6
(	(	kIx(
<g/>
2011	#num#	k4
<g/>
)	)	kIx)
se	se	k3xPyFc4
při	při	k7c6
testování	testování	k1gNnSc6
inteligence	inteligence	k1gFnSc2
používají	používat	k5eAaImIp3nP
různé	různý	k2eAgFnPc1d1
metody	metoda	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedle	vedle	k7c2
Ravenových	Ravenův	k2eAgFnPc2d1
progresivních	progresivní	k2eAgFnPc2d1
matic	matice	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc4,k3yIgFnPc4,k3yQgFnPc4
„	„	k?
<g/>
měří	měřit	k5eAaImIp3nS
<g/>
“	“	k?
zejména	zejména	k9
analytickou	analytický	k2eAgFnSc4d1
složku	složka	k1gFnSc4
inteligence	inteligence	k1gFnSc2
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
to	ten	k3xDgNnSc4
zejména	zejména	k9
Wechslerovy	Wechslerův	k2eAgInPc1d1
testy	test	k1gInPc1
inteligence	inteligence	k1gFnSc2
(	(	kIx(
<g/>
nyní	nyní	k6eAd1
hlavně	hlavně	k9
WISC-III	WISC-III	k1gFnSc1
<g/>
,	,	kIx,
WAISR	WAISR	kA
nebo	nebo	k8xC
stále	stále	k6eAd1
používaný	používaný	k2eAgInSc4d1
PDW	PDW	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Stanford-Binet	Stanford-Binet	k1gMnSc1
Intelligence	Intelligence	k1gFnSc2
Scale	Scale	k1gFnSc2
(	(	kIx(
<g/>
v	v	k7c6
současnosti	současnost	k1gFnSc6
Stanford-Binet	Stanford-Bineta	k1gFnPc2
5	#num#	k4
<g/>
)	)	kIx)
nebo	nebo	k8xC
v	v	k7c6
Česku	Česko	k1gNnSc6
příliš	příliš	k6eAd1
nerozšířené	rozšířený	k2eNgInPc1d1
Schwartzovy	Schwartzův	k2eAgInPc1d1
barevné	barevný	k2eAgInPc1d1
čtverce	čtverec	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tyto	tento	k3xDgInPc4
testy	test	k1gInPc4
vykazují	vykazovat	k5eAaImIp3nP
výrazně	výrazně	k6eAd1
vyšší	vysoký	k2eAgFnSc4d2
validitu	validita	k1gFnSc4
nežli	nežli	k8xS
testy	test	k1gInPc4
volně	volně	k6eAd1
dostupné	dostupný	k2eAgInPc1d1
na	na	k7c6
internetu	internet	k1gInSc6
-	-	kIx~
ty	ten	k3xDgFnPc1
jsou	být	k5eAaImIp3nP
často	často	k6eAd1
neprofesionální	profesionální	k2eNgFnPc1d1
a	a	k8xC
neposkytují	poskytovat	k5eNaImIp3nP
interpretovatelné	interpretovatelný	k2eAgInPc4d1
výsledky	výsledek	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
http://phys.org/news/2016-04-intelligent-brainless-slime.html	http://phys.org/news/2016-04-intelligent-brainless-slime.html	k1gMnSc1
-	-	kIx~
Intelligent	Intelligent	k1gMnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Brainless	Brainless	k1gInSc1
slime	slimat	k5eAaPmIp3nS
can	can	k?
'	'	kIx"
<g/>
learn	learn	k1gMnSc1
<g/>
'	'	kIx"
<g/>
:	:	kIx,
study	stud	k1gInPc1
<g/>
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
ATKINSON	ATKINSON	kA
<g/>
,	,	kIx,
Rita	rito	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Psychologie	psychologie	k1gFnSc1
<g/>
.	.	kIx.
[	[	kIx(
<g/>
s.	s.	k?
<g/>
l.	l.	k?
<g/>
]	]	kIx)
<g/>
:	:	kIx,
Portál	portál	k1gInSc1
1	#num#	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
PLHÁKOVÁ	PLHÁKOVÁ	kA
<g/>
,	,	kIx,
Alena	Alena	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Učebnice	učebnice	k1gFnSc1
obecné	obecný	k2eAgFnSc2d1
psychologie	psychologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
ACADEMIA	academia	k1gFnSc1
<g/>
,	,	kIx,
2005	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Inteligence	inteligence	k1gFnSc2
a	a	k8xC
její	její	k3xOp3gNnPc1
měření	měření	k1gNnPc1
<g/>
,	,	kIx,
Časopis	časopis	k1gInSc1
Mensa	mensa	k1gFnSc1
<g/>
.	.	kIx.
casopis	casopis	k1gInSc1
<g/>
.	.	kIx.
<g/>
mensa	mensa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Vysoká	vysoká	k1gFnSc1
inteligence	inteligence	k1gFnSc2
je	být	k5eAaImIp3nS
pro	pro	k7c4
děti	dítě	k1gFnPc4
problém	problém	k1gInSc4
<g/>
,	,	kIx,
rodiče	rodič	k1gMnPc1
z	z	k7c2
nich	on	k3xPp3gFnPc2
můžou	můžou	k?
být	být	k5eAaImF
úplně	úplně	k6eAd1
hotoví	hotovit	k5eAaImIp3nS
<g/>
,	,	kIx,
říká	říkat	k5eAaImIp3nS
lektorka	lektorka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
TV	TV	kA
-	-	kIx~
Jen	jen	k9
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
co	co	k3yRnSc4,k3yQnSc4,k3yInSc4
musíte	muset	k5eAaImIp2nP
vidět	vidět	k5eAaImF
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://sciencemag.cz/sebeovladani-souvisi-s-inteligenci-i-u-simpanzu/	http://sciencemag.cz/sebeovladani-souvisi-s-inteligenci-i-u-simpanzu/	k?
-	-	kIx~
Sebeovládání	sebeovládání	k1gNnSc1
souvisí	souviset	k5eAaImIp3nS
s	s	k7c7
inteligencí	inteligence	k1gFnSc7
–	–	k?
i	i	k9
u	u	k7c2
šimpanzů	šimpanz	k1gMnPc2
<g/>
↑	↑	k?
Michael	Michaela	k1gFnPc2
J.	J.	kA
Beran	beran	k1gInSc1
<g/>
,	,	kIx,
William	William	k1gInSc1
D.	D.	kA
Hopkins	Hopkins	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Self-Control	Self-Control	k1gInSc1
in	in	k?
Chimpanzees	Chimpanzees	k1gMnSc1
Relates	Relates	k1gMnSc1
to	ten	k3xDgNnSc4
General	General	k1gFnSc7
Intelligence	Intelligence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Current	Current	k1gInSc1
Biology	biolog	k1gMnPc7
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
;	;	kIx,
DOI	DOI	kA
<g/>
:	:	kIx,
10.101	10.101	k4
<g/>
6	#num#	k4
<g/>
/	/	kIx~
<g/>
j.	j.	k?
<g/>
cub	cub	k?
<g/>
.2017	.2017	k4
<g/>
.12	.12	k4
<g/>
.0431	.0431	k4
2	#num#	k4
http://en.wikibooks.org/wiki/Applied_History_of_Psychology/Theories_on_Intelligence	http://en.wikibooks.org/wiki/Applied_History_of_Psychology/Theories_on_Intelligence	k1gFnSc1
<g/>
↑	↑	k?
P.	P.	kA
Beneš	Beneš	k1gMnSc1
<g/>
:	:	kIx,
Informace	informace	k1gFnSc1
o	o	k7c4
informaci	informace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Praha	Praha	k1gFnSc1
<g/>
:	:	kIx,
BEN	Ben	k1gInSc1
-	-	kIx~
technická	technický	k2eAgFnSc1d1
literatura	literatura	k1gFnSc1
<g/>
,	,	kIx,
2010	#num#	k4
<g/>
,	,	kIx,
ISBN	ISBN	kA
978	#num#	k4
<g/>
-	-	kIx~
<g/>
80	#num#	k4
<g/>
-	-	kIx~
<g/>
7300	#num#	k4
<g/>
-	-	kIx~
<g/>
263	#num#	k4
<g/>
-	-	kIx~
<g/>
3	#num#	k4
<g/>
↑	↑	k?
Sternberg	Sternberg	k1gMnSc1
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
J.	J.	kA
(	(	kIx(
<g/>
1985	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Beyond	Beyond	k1gInSc1
IQ	iq	kA
<g/>
:	:	kIx,
A	a	k8xC
Triarchic	Triarchic	k1gMnSc1
Theory	Theora	k1gFnSc2
of	of	k?
Human	Human	k1gInSc1
Intelligence	Intelligence	k1gFnSc1
<g/>
,	,	kIx,
New	New	k1gFnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Sternberg	Sternberg	k1gMnSc1
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
J.	J.	kA
(	(	kIx(
<g/>
1994	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
"	"	kIx"
<g/>
Thinking	Thinking	k1gInSc1
Styles	Styles	k1gInSc1
<g/>
:	:	kIx,
Theory	Theora	k1gFnPc1
and	and	k?
Assessment	Assessment	k1gMnSc1
at	at	k?
the	the	k?
Interface	interface	k1gInSc1
Between	Betwena	k1gFnPc2
Intelligence	Intelligence	k1gFnSc2
and	and	k?
Personality	personalita	k1gFnSc2
<g/>
,	,	kIx,
<g/>
"	"	kIx"
in	in	k?
<g/>
:	:	kIx,
Personality	personalita	k1gFnSc2
and	and	k?
Intelligence	Intelligence	k1gFnSc2
(	(	kIx(
<g/>
eds	eds	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Robert	Robert	k1gMnSc1
J.	J.	kA
Sternberg	Sternberg	k1gMnSc1
and	and	k?
Patricia	Patricius	k1gMnSc4
Ruzgis	Ruzgis	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Cambridge	Cambridge	k1gFnSc1
University	universita	k1gFnSc2
Press	Pressa	k1gFnPc2
<g/>
,	,	kIx,
169	#num#	k4
<g/>
-	-	kIx~
<g/>
187	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
http://psychology.about.com/od/cognitivepsychology/p/intelligence.htm	http://psychology.about.com/od/cognitivepsychology/p/intelligence.htm	k1gMnSc1
<g/>
↑	↑	k?
Gardner	Gardner	k1gMnSc1
<g/>
,	,	kIx,
H.	H.	kA
(	(	kIx(
<g/>
1983	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Frames	Frames	k1gMnSc1
of	of	k?
mind	mind	k1gMnSc1
<g/>
:	:	kIx,
The	The	k1gMnSc1
theory	theora	k1gFnSc2
of	of	k?
multiple	multipl	k1gInSc5
intelligences	intelligencesa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
New	New	k1gMnSc1
York	York	k1gInSc1
<g/>
:	:	kIx,
Basic	Basic	kA
Books	Books	k1gInSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
http://reynaldojrflores.wordpress.com/2013/06/14/the-whos-of-intelligence-theories-david-perkins/	http://reynaldojrflores.wordpress.com/2013/06/14/the-whos-of-intelligence-theories-david-perkins/	k4
<g/>
↑	↑	k?
http://otec.uoregon.edu/intelligence.htm	http://otec.uoregon.edu/intelligence.htm	k1gMnSc1
<g/>
↑	↑	k?
MINDLAB	MINDLAB	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Fetiš	fetiš	k1gInSc1
inteligence	inteligence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Psychologie	psychologie	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Inteligence	inteligence	k1gFnPc1
a	a	k8xC
její	její	k3xOp3gNnPc1
měření	měření	k1gNnPc1
<g/>
,	,	kIx,
Časopis	časopis	k1gInSc1
Mensa	mensa	k1gFnSc1
<g/>
.	.	kIx.
casopis	casopis	k1gInSc1
<g/>
.	.	kIx.
<g/>
mensa	mensa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Člověk	člověk	k1gMnSc1
má	mít	k5eAaImIp3nS
8	#num#	k4
druhů	druh	k1gInPc2
inteligence	inteligence	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
jste	být	k5eAaImIp2nP
na	na	k7c6
tom	ten	k3xDgNnSc6
vy	vy	k3xPp2nPc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
National	National	k1gFnSc1
Geographic	Geographice	k1gInPc2
Česko	Česko	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
STRAKA	Straka	k1gMnSc1
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
<g/>
;	;	kIx,
CÍGLER	CÍGLER	kA
<g/>
,	,	kIx,
Hynek	Hynek	k1gMnSc1
<g/>
;	;	kIx,
JABŮREK	Jabůrek	k1gMnSc1
<g/>
,	,	kIx,
Michal	Michal	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Matematické	matematický	k2eAgNnSc4d1
nadání	nadání	k1gNnSc4
z	z	k7c2
pohledu	pohled	k1gInSc2
neuropsychologie	neuropsychologie	k1gFnSc2
a	a	k8xC
kognitivní	kognitivní	k2eAgFnSc2d1
psychologie	psychologie	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
S.	S.	kA
327	#num#	k4
<g/>
-	-	kIx~
<g/>
358	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pedagogika	pedagogika	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
64	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
3	#num#	k4
<g/>
,	,	kIx,
s.	s.	k?
327	#num#	k4
<g/>
-	-	kIx~
<g/>
358	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Inteligence	inteligence	k1gFnSc2
a	a	k8xC
její	její	k3xOp3gNnPc1
měření	měření	k1gNnPc1
<g/>
,	,	kIx,
Časopis	časopis	k1gInSc1
Mensa	mensa	k1gFnSc1
<g/>
.	.	kIx.
casopis	casopis	k1gInSc1
<g/>
.	.	kIx.
<g/>
mensa	mensa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Mensa	mensa	k1gFnSc1
International	International	k1gFnSc2
</s>
<s>
Mentální	mentální	k2eAgInSc1d1
věk	věk	k1gInSc1
</s>
<s>
Umělá	umělý	k2eAgFnSc1d1
inteligence	inteligence	k1gFnSc1
</s>
<s>
Filozofie	filozofie	k1gFnSc1
umělé	umělý	k2eAgFnSc2d1
inteligence	inteligence	k1gFnSc2
</s>
<s>
Inteligence	inteligence	k1gFnSc1
hejna	hejno	k1gNnSc2
</s>
<s>
Kognitivní	kognitivní	k2eAgFnSc1d1
psychologie	psychologie	k1gFnSc1
</s>
<s>
Vnímání	vnímání	k1gNnSc1
</s>
<s>
Obecný	obecný	k2eAgInSc1d1
faktor	faktor	k1gInSc1
inteligence	inteligence	k1gFnSc2
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
inteligence	inteligence	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
inteligence	inteligence	k1gFnSc2
ve	v	k7c6
Wikislovníku	Wikislovník	k1gInSc6
</s>
<s>
Téma	téma	k1gNnSc1
Inteligence	inteligence	k1gFnSc2
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Mensa	mensa	k1gFnSc1
ČR	ČR	kA
–	–	k?
inteligence	inteligence	k1gFnSc2
a	a	k8xC
její	její	k3xOp3gNnPc1
měření	měření	k1gNnPc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4027251-5	4027251-5	k4
|	|	kIx~
PSH	PSH	kA
<g/>
:	:	kIx,
9424	#num#	k4
|	|	kIx~
LCCN	LCCN	kA
<g/>
:	:	kIx,
sh	sh	k?
<g/>
85067157	#num#	k4
|	|	kIx~
WorldcatID	WorldcatID	k1gFnSc1
<g/>
:	:	kIx,
lccn-sh	lccn-sh	k1gInSc1
<g/>
85067157	#num#	k4
</s>
