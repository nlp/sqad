<s>
Klima	klima	k1gNnSc1	klima
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
Skotsku	Skotsko	k1gNnSc6	Skotsko
<g/>
,	,	kIx,	,
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
v	v	k7c6	v
jiných	jiný	k2eAgFnPc6d1	jiná
částech	část	k1gFnPc6	část
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
<g/>
,	,	kIx,	,
ovlivněno	ovlivněn	k2eAgNnSc1d1	ovlivněno
Golfským	golfský	k2eAgInSc7d1	golfský
proudem	proud	k1gInSc7	proud
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
působí	působit	k5eAaImIp3nS	působit
od	od	k7c2	od
západu	západ	k1gInSc2	západ
<g/>
,	,	kIx,	,
od	od	k7c2	od
Atlantského	atlantský	k2eAgInSc2d1	atlantský
oceánu	oceán	k1gInSc2	oceán
<g/>
.	.	kIx.	.
</s>
