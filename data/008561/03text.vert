<p>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
Švédska	Švédsko	k1gNnSc2	Švédsko
jsou	být	k5eAaImIp3nP	být
dějinami	dějiny	k1gFnPc7	dějiny
území	území	k1gNnSc2	území
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgNnSc6	jenž
se	se	k3xPyFc4	se
dnes	dnes	k6eAd1	dnes
nachází	nacházet	k5eAaImIp3nS	nacházet
Švédské	švédský	k2eAgNnSc1d1	švédské
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
dějiny	dějiny	k1gFnPc1	dějiny
Švédského	švédský	k2eAgNnSc2d1	švédské
království	království	k1gNnSc2	království
mimo	mimo	k7c4	mimo
území	území	k1gNnSc4	území
současného	současný	k2eAgInSc2d1	současný
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Prehistorie	prehistorie	k1gFnSc1	prehistorie
9000	[number]	k4	9000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
–	–	k?	–
1000	[number]	k4	1000
n.	n.	k?	n.
l.	l.	k?	l.
==	==	k?	==
</s>
</p>
<p>
<s>
Švédsko	Švédsko	k1gNnSc1	Švédsko
má	mít	k5eAaImIp3nS	mít
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
sousední	sousední	k2eAgNnSc4d1	sousední
Norsko	Norsko	k1gNnSc4	Norsko
vysokou	vysoký	k2eAgFnSc4d1	vysoká
koncentraci	koncentrace	k1gFnSc4	koncentrace
nástěnných	nástěnný	k2eAgFnPc2d1	nástěnná
maleb	malba	k1gFnPc2	malba
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
nejvíce	nejvíce	k6eAd1	nejvíce
jich	on	k3xPp3gFnPc2	on
najdeme	najít	k5eAaPmIp1nP	najít
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Bohuslän	Bohusläna	k1gFnPc2	Bohusläna
<g/>
.	.	kIx.	.
</s>
<s>
Nejstarší	starý	k2eAgMnPc1d3	nejstarší
však	však	k9	však
můžeme	moct	k5eAaImIp1nP	moct
najít	najít	k5eAaPmF	najít
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Jämtland	Jämtlanda	k1gFnPc2	Jämtlanda
datovaných	datovaný	k2eAgFnPc2d1	datovaná
do	do	k7c2	do
let	léto	k1gNnPc2	léto
kolem	kolem	k7c2	kolem
9000	[number]	k4	9000
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
</s>
</p>
<p>
<s>
Počátky	počátek	k1gInPc4	počátek
švédského	švédský	k2eAgInSc2d1	švédský
národa	národ	k1gInSc2	národ
nelze	lze	k6eNd1	lze
určit	určit	k5eAaPmF	určit
přesně	přesně	k6eAd1	přesně
<g/>
.	.	kIx.	.
</s>
<s>
Existenci	existence	k1gFnSc4	existence
silných	silný	k2eAgInPc2d1	silný
kmenů	kmen	k1gInPc2	kmen
zaznamenává	zaznamenávat	k5eAaImIp3nS	zaznamenávat
na	na	k7c6	na
území	území	k1gNnSc6	území
Švédska	Švédsko	k1gNnSc2	Švédsko
již	již	k9	již
Tacitus	Tacitus	k1gInSc4	Tacitus
kolem	kolem	k7c2	kolem
100	[number]	k4	100
př	př	kA	př
<g/>
.	.	kIx.	.
n.	n.	k?	n.
l.	l.	k?	l.
Následují	následovat	k5eAaImIp3nP	následovat
zmínky	zmínka	k1gFnPc4	zmínka
o	o	k7c6	o
královstvích	království	k1gNnPc6	království
Svea	Sve	k1gInSc2	Sve
Rike	Rike	k1gNnSc2	Rike
a	a	k8xC	a
Göta	Götum	k1gNnSc2	Götum
Rike	Rik	k1gInSc2	Rik
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
z	z	k7c2	z
dob	doba	k1gFnPc2	doba
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
si	se	k3xPyFc3	se
Svea	Sve	k2eAgFnSc1d1	Sve
Rike	Rike	k1gFnSc1	Rike
podrobila	podrobit	k5eAaPmAgFnS	podrobit
druhou	druhý	k4xOgFnSc7	druhý
z	z	k7c2	z
říší	říš	k1gFnPc2	říš
<g/>
,	,	kIx,	,
pochází	pocházet	k5eAaImIp3nS	pocházet
současný	současný	k2eAgInSc1d1	současný
název	název	k1gInSc1	název
Sverige	Sverige	k1gFnSc1	Sverige
(	(	kIx(	(
<g/>
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vikingové	Viking	k1gMnPc5	Viking
==	==	k?	==
</s>
</p>
<p>
<s>
Vikingové	Viking	k1gMnPc1	Viking
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
během	během	k7c2	během
8	[number]	k4	8
<g/>
.	.	kIx.	.
<g/>
-	-	kIx~	-
<g/>
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
značnou	značný	k2eAgFnSc4d1	značná
část	část	k1gFnSc4	část
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
.	.	kIx.	.
</s>
<s>
Švédští	švédský	k2eAgMnPc1d1	švédský
Vikingové	Viking	k1gMnPc1	Viking
směřující	směřující	k2eAgFnSc2d1	směřující
hlavně	hlavně	k9	hlavně
na	na	k7c4	na
jihovýchod	jihovýchod	k1gInSc4	jihovýchod
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byli	být	k5eAaImAgMnP	být
označováni	označovat	k5eAaImNgMnP	označovat
jako	jako	k9	jako
Varjagové	Varjag	k1gMnPc1	Varjag
<g/>
,	,	kIx,	,
dobyli	dobýt	k5eAaPmAgMnP	dobýt
Kyjev	Kyjev	k1gInSc4	Kyjev
a	a	k8xC	a
Novgorod	Novgorod	k1gInSc4	Novgorod
<g/>
.	.	kIx.	.
</s>
<s>
Dostali	dostat	k5eAaPmAgMnP	dostat
se	se	k3xPyFc4	se
až	až	k9	až
do	do	k7c2	do
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkali	setkat	k5eAaPmAgMnP	setkat
s	s	k7c7	s
Araby	Arab	k1gMnPc7	Arab
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Sverkerovci	Sverkerovec	k1gMnPc7	Sverkerovec
<g/>
,	,	kIx,	,
Erikovci	Erikovec	k1gMnPc7	Erikovec
a	a	k8xC	a
Folkungovci	Folkungovec	k1gMnPc7	Folkungovec
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
11	[number]	k4	11
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
se	se	k3xPyFc4	se
na	na	k7c6	na
švédském	švédský	k2eAgInSc6d1	švédský
trůně	trůn	k1gInSc6	trůn
střídali	střídat	k5eAaImAgMnP	střídat
zástupci	zástupce	k1gMnPc1	zástupce
dvou	dva	k4xCgFnPc2	dva
dynastií	dynastie	k1gFnPc2	dynastie
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
rod	rod	k1gInSc4	rod
Sverkerovců	Sverkerovec	k1gMnPc2	Sverkerovec
a	a	k8xC	a
rod	rod	k1gInSc1	rod
Erikův	Erikův	k2eAgInSc1d1	Erikův
<g/>
,	,	kIx,	,
po	po	k7c6	po
kterých	který	k3yRgMnPc6	který
ve	v	k7c6	v
12	[number]	k4	12
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
nastoupila	nastoupit	k5eAaPmAgFnS	nastoupit
dynastie	dynastie	k1gFnSc1	dynastie
Folkungů	Folkung	k1gInPc2	Folkung
<g/>
.	.	kIx.	.
</s>
<s>
Folkungové	Folkung	k1gMnPc1	Folkung
jsou	být	k5eAaImIp3nP	být
známí	známý	k2eAgMnPc1d1	známý
svými	svůj	k3xOyFgFnPc7	svůj
vzájemnými	vzájemný	k2eAgFnPc7d1	vzájemná
<g/>
,	,	kIx,	,
často	často	k6eAd1	často
krvavými	krvavý	k2eAgFnPc7d1	krvavá
půtkami	půtka	k1gFnPc7	půtka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
krále	král	k1gMnSc2	král
Magnuse	Magnuse	k1gFnSc2	Magnuse
II	II	kA	II
<g/>
.	.	kIx.	.
žila	žít	k5eAaImAgFnS	žít
neslavnější	slavný	k2eNgFnSc1d2	neslavnější
švédská	švédský	k2eAgFnSc1d1	švédská
světice	světice	k1gFnSc1	světice
svatá	svatý	k2eAgFnSc1d1	svatá
Birgitta	Birgitta	k1gFnSc1	Birgitta
<g/>
.	.	kIx.	.
</s>
<s>
Tzv.	tzv.	kA	tzv.
jarl	jarl	k1gMnSc1	jarl
(	(	kIx(	(
<g/>
náčelník	náčelník	k1gMnSc1	náčelník
<g/>
)	)	kIx)	)
Birger	Birger	k1gMnSc1	Birger
Magnusson	Magnusson	k1gMnSc1	Magnusson
z	z	k7c2	z
Bjölbo	Bjölba	k1gFnSc5	Bjölba
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1250	[number]	k4	1250
vládl	vládnout	k5eAaImAgMnS	vládnout
jako	jako	k9	jako
regent	regent	k1gMnSc1	regent
za	za	k7c2	za
svého	svůj	k3xOyFgMnSc2	svůj
syna	syn	k1gMnSc2	syn
<g/>
,	,	kIx,	,
nezletilého	nezletilý	k1gMnSc2	nezletilý
krále	král	k1gMnSc2	král
Valdemara	Valdemara	k1gFnSc1	Valdemara
Birgerssona	Birgerssona	k1gFnSc1	Birgerssona
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
zasloužil	zasloužit	k5eAaPmAgMnS	zasloužit
o	o	k7c4	o
připojení	připojení	k1gNnSc4	připojení
Finska	Finsko	k1gNnSc2	Finsko
ke	k	k7c3	k
Švédsku	Švédsko	k1gNnSc3	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Jarl	jarl	k1gMnSc1	jarl
Birger	Birger	k1gMnSc1	Birger
také	také	k9	také
založil	založit	k5eAaPmAgMnS	založit
město	město	k1gNnSc4	město
Stockholm	Stockholm	k1gInSc1	Stockholm
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Kalmarská	Kalmarský	k2eAgFnSc1d1	Kalmarská
unie	unie	k1gFnSc1	unie
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1397	[number]	k4	1397
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
v	v	k7c6	v
Kalmaru	Kalmar	k1gInSc6	Kalmar
Kalmarská	Kalmarský	k2eAgFnSc1d1	Kalmarská
unie	unie	k1gFnSc1	unie
<g/>
.	.	kIx.	.
</s>
<s>
Nejvíc	hodně	k6eAd3	hodně
se	se	k3xPyFc4	se
o	o	k7c6	o
ní	on	k3xPp3gFnSc6	on
svými	svůj	k3xOyFgFnPc7	svůj
státnickými	státnický	k2eAgFnPc7d1	státnická
schopnostmi	schopnost	k1gFnPc7	schopnost
zasloužila	zasloužit	k5eAaPmAgFnS	zasloužit
Markéta	Markéta	k1gFnSc1	Markéta
I.	I.	kA	I.
Dánská	dánský	k2eAgFnSc1d1	dánská
<g/>
.	.	kIx.	.
</s>
<s>
Unie	unie	k1gFnSc1	unie
sjednotila	sjednotit	k5eAaPmAgFnS	sjednotit
Švédsko	Švédsko	k1gNnSc4	Švédsko
<g/>
,	,	kIx,	,
Norsko	Norsko	k1gNnSc4	Norsko
a	a	k8xC	a
Dánsko	Dánsko	k1gNnSc4	Dánsko
pod	pod	k7c4	pod
jednoho	jeden	k4xCgMnSc4	jeden
panovníka	panovník	k1gMnSc4	panovník
<g/>
.	.	kIx.	.
</s>
<s>
Švédům	Švéd	k1gMnPc3	Švéd
se	se	k3xPyFc4	se
ale	ale	k8xC	ale
často	často	k6eAd1	často
nezamlouvaly	zamlouvat	k5eNaImAgFnP	zamlouvat
dánské	dánský	k2eAgFnPc1d1	dánská
války	válka	k1gFnPc1	válka
s	s	k7c7	s
německými	německý	k2eAgNnPc7d1	německé
městy	město	k1gNnPc7	město
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
směřovala	směřovat	k5eAaImAgFnS	směřovat
většina	většina	k1gFnSc1	většina
švédského	švédský	k2eAgInSc2d1	švédský
obchodu	obchod	k1gInSc2	obchod
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
v	v	k7c4	v
15	[number]	k4	15
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
vedlo	vést	k5eAaImAgNnS	vést
k	k	k7c3	k
častým	častý	k2eAgInPc3d1	častý
rozporům	rozpor	k1gInPc3	rozpor
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1434	[number]	k4	1434
povstal	povstat	k5eAaPmAgMnS	povstat
majitel	majitel	k1gMnSc1	majitel
dolů	dol	k1gInPc2	dol
Engelbrekt	Engelbrekt	k1gInSc1	Engelbrekt
a	a	k8xC	a
od	od	k7c2	od
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
nebyl	být	k5eNaImAgMnS	být
král	král	k1gMnSc1	král
unie	unie	k1gFnSc2	unie
často	často	k6eAd1	často
na	na	k7c6	na
území	území	k1gNnSc6	území
Švédska	Švédsko	k1gNnSc2	Švédsko
ani	ani	k8xC	ani
vpuštěn	vpuštěn	k2eAgMnSc1d1	vpuštěn
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Roku	rok	k1gInSc2	rok
1477	[number]	k4	1477
byla	být	k5eAaImAgFnS	být
založena	založit	k5eAaPmNgFnS	založit
nejstarší	starý	k2eAgFnSc1d3	nejstarší
Univerzita	univerzita	k1gFnSc1	univerzita
v	v	k7c6	v
Uppsale	Uppsala	k1gFnSc6	Uppsala
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Švédská	švédský	k2eAgFnSc1d1	švédská
samostatnost	samostatnost	k1gFnSc1	samostatnost
a	a	k8xC	a
třicetiletá	třicetiletý	k2eAgFnSc1d1	třicetiletá
válka	válka	k1gFnSc1	válka
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1523	[number]	k4	1523
se	s	k7c7	s
králem	král	k1gMnSc7	král
stal	stát	k5eAaPmAgMnS	stát
Gustav	Gustav	k1gMnSc1	Gustav
I.	I.	kA	I.
Vasa	Vasa	k1gMnSc1	Vasa
a	a	k8xC	a
vytvořil	vytvořit	k5eAaPmAgInS	vytvořit
samostatný	samostatný	k2eAgInSc1d1	samostatný
švédský	švédský	k2eAgInSc1d1	švédský
stát	stát	k1gInSc1	stát
<g/>
.	.	kIx.	.
</s>
<s>
Dynastie	dynastie	k1gFnSc1	dynastie
Vásovců	Vásovec	k1gInPc2	Vásovec
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
pak	pak	k6eAd1	pak
vládla	vládnout	k5eAaImAgFnS	vládnout
do	do	k7c2	do
roku	rok	k1gInSc2	rok
1659	[number]	k4	1659
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
strůjcem	strůjce	k1gMnSc7	strůjce
velmocenského	velmocenský	k2eAgNnSc2d1	velmocenské
postavení	postavení	k1gNnSc2	postavení
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
tomu	ten	k3xDgNnSc3	ten
tak	tak	k6eAd1	tak
bylo	být	k5eAaImAgNnS	být
za	za	k7c4	za
krále	král	k1gMnSc4	král
Gustava	Gustav	k1gMnSc2	Gustav
Adolfa	Adolf	k1gMnSc2	Adolf
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
dovedl	dovést	k5eAaPmAgMnS	dovést
Švédy	Švéd	k1gMnPc4	Švéd
ke	k	k7c3	k
značným	značný	k2eAgInPc3d1	značný
úspěchům	úspěch	k1gInPc3	úspěch
v	v	k7c6	v
třicetileté	třicetiletý	k2eAgFnSc6d1	třicetiletá
válce	válka	k1gFnSc6	válka
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nakonec	nakonec	k6eAd1	nakonec
padl	padnout	k5eAaImAgInS	padnout
v	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
u	u	k7c2	u
Lützenu	Lützen	k2eAgFnSc4d1	Lützen
roku	rok	k1gInSc2	rok
1632	[number]	k4	1632
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
Švédsko	Švédsko	k1gNnSc1	Švédsko
bojovalo	bojovat	k5eAaImAgNnS	bojovat
proti	proti	k7c3	proti
Habsburkům	Habsburk	k1gInPc3	Habsburk
a	a	k8xC	a
německým	německý	k2eAgInPc3d1	německý
státům	stát	k1gInPc3	stát
<g/>
,	,	kIx,	,
které	který	k3yIgInPc1	který
byly	být	k5eAaImAgInP	být
spojenci	spojenec	k1gMnPc1	spojenec
Habsurků	Habsurek	k1gMnPc2	Habsurek
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
třicetileté	třicetiletý	k2eAgFnSc2d1	třicetiletá
války	válka	k1gFnSc2	válka
švédská	švédský	k2eAgNnPc1d1	švédské
vojska	vojsko	k1gNnPc1	vojsko
roku	rok	k1gInSc2	rok
1642	[number]	k4	1642
dobyla	dobýt	k5eAaPmAgFnS	dobýt
Olomouc	Olomouc	k1gFnSc1	Olomouc
a	a	k8xC	a
roku	rok	k1gInSc2	rok
1648	[number]	k4	1648
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
se	se	k3xPyFc4	se
Švédové	Švéd	k1gMnPc1	Švéd
dvakrát	dvakrát	k6eAd1	dvakrát
pokusili	pokusit	k5eAaPmAgMnP	pokusit
dobýt	dobýt	k5eAaPmF	dobýt
Brno	Brno	k1gNnSc4	Brno
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Švédsko	Švédsko	k1gNnSc1	Švédsko
ovšem	ovšem	k9	ovšem
získalo	získat	k5eAaPmAgNnS	získat
značná	značný	k2eAgNnPc4d1	značné
území	území	k1gNnSc4	území
<g/>
,	,	kIx,	,
např.	např.	kA	např.
pobaltské	pobaltský	k2eAgInPc1d1	pobaltský
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
Pomořansko	Pomořansko	k1gNnSc1	Pomořansko
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
riskantním	riskantní	k2eAgInSc6d1	riskantní
přechodu	přechod	k1gInSc6	přechod
zamrzlého	zamrzlý	k2eAgNnSc2d1	zamrzlé
Malého	malé	k1gNnSc2	malé
i	i	k9	i
Velkého	velký	k2eAgInSc2d1	velký
Beltu	Belt	k1gInSc2	Belt
vojsky	vojsky	k6eAd1	vojsky
krále	král	k1gMnSc4	král
Karla	Karel	k1gMnSc4	Karel
X.	X.	kA	X.
<g/>
,	,	kIx,	,
přiřkl	přiřknout	k5eAaPmAgMnS	přiřknout
Mír	mír	k1gInSc4	mír
z	z	k7c2	z
Roskilde	Roskild	k1gInSc5	Roskild
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1658	[number]	k4	1658
Švédsku	Švédsko	k1gNnSc6	Švédsko
jižní	jižní	k2eAgFnSc2d1	jižní
provincie	provincie	k1gFnSc2	provincie
Skå	Skå	k1gFnPc2	Skå
<g/>
,	,	kIx,	,
Blekinge	Blekinge	k1gFnPc2	Blekinge
a	a	k8xC	a
Halland	Hallanda	k1gFnPc2	Hallanda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Konec	konec	k1gInSc4	konec
velmocenského	velmocenský	k2eAgNnSc2d1	velmocenské
postavení	postavení	k1gNnSc2	postavení
Švédska	Švédsko	k1gNnSc2	Švédsko
==	==	k?	==
</s>
</p>
<p>
<s>
Nástupu	nástup	k1gInSc2	nástup
mladého	mladý	k2eAgMnSc4d1	mladý
panovníka	panovník	k1gMnSc4	panovník
Karla	Karel	k1gMnSc2	Karel
XII	XII	kA	XII
<g/>
.	.	kIx.	.
chtělo	chtít	k5eAaImAgNnS	chtít
využít	využít	k5eAaPmF	využít
tehdy	tehdy	k6eAd1	tehdy
rozmáhající	rozmáhající	k2eAgNnSc1d1	rozmáhající
se	se	k3xPyFc4	se
Rusko	Rusko	k1gNnSc1	Rusko
k	k	k7c3	k
upevnění	upevnění	k1gNnSc3	upevnění
svých	svůj	k3xOyFgFnPc2	svůj
pozic	pozice	k1gFnPc2	pozice
<g/>
.	.	kIx.	.
</s>
<s>
Car	car	k1gMnSc1	car
Petr	Petr	k1gMnSc1	Petr
I.	I.	kA	I.
Veliký	veliký	k2eAgMnSc1d1	veliký
začal	začít	k5eAaPmAgMnS	začít
obléhat	obléhat	k5eAaImF	obléhat
švédský	švédský	k2eAgInSc4d1	švédský
přístav	přístav	k1gInSc4	přístav
Narva	Narvo	k1gNnSc2	Narvo
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
švédský	švédský	k2eAgMnSc1d1	švédský
král	král	k1gMnSc1	král
mu	on	k3xPp3gMnSc3	on
přichystal	přichystat	k5eAaPmAgMnS	přichystat
zdrcující	zdrcující	k2eAgFnSc4d1	zdrcující
porážku	porážka	k1gFnSc4	porážka
(	(	kIx(	(
<g/>
Bitva	bitva	k1gFnSc1	bitva
u	u	k7c2	u
Narvy	Narva	k1gFnSc2	Narva
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tak	tak	k6eAd1	tak
začíná	začínat	k5eAaImIp3nS	začínat
Velká	velký	k2eAgFnSc1d1	velká
severní	severní	k2eAgFnSc1d1	severní
válka	válka	k1gFnSc1	válka
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
končí	končit	k5eAaImIp3nS	končit
Nystadským	Nystadský	k2eAgInSc7d1	Nystadský
mírem	mír	k1gInSc7	mír
z	z	k7c2	z
roku	rok	k1gInSc2	rok
1721	[number]	k4	1721
a	a	k8xC	a
Švédsko	Švédsko	k1gNnSc1	Švédsko
ztrácí	ztrácet	k5eAaImIp3nS	ztrácet
pobaltská	pobaltský	k2eAgNnPc4d1	pobaltské
území	území	k1gNnPc4	území
i	i	k9	i
své	svůj	k3xOyFgNnSc4	svůj
velmocenské	velmocenský	k2eAgNnSc4d1	velmocenské
postavení	postavení	k1gNnSc4	postavení
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
době	doba	k1gFnSc6	doba
panování	panování	k1gNnSc2	panování
následnice	následnice	k1gFnSc2	následnice
po	po	k7c4	po
Karlu	Karla	k1gFnSc4	Karla
XII	XII	kA	XII
<g/>
.	.	kIx.	.
</s>
<s>
Ulrika	Ulrika	k1gFnSc1	Ulrika
Eleanora	Eleanora	k1gFnSc1	Eleanora
a	a	k8xC	a
zejména	zejména	k9	zejména
pak	pak	k6eAd1	pak
jejího	její	k3xOp3gMnSc4	její
manžela	manžel	k1gMnSc4	manžel
<g/>
,	,	kIx,	,
švédského	švédský	k2eAgMnSc4d1	švédský
krále	král	k1gMnSc4	král
Fridricha	Fridrich	k1gMnSc4	Fridrich
I.	I.	kA	I.
byla	být	k5eAaImAgFnS	být
královská	královský	k2eAgFnSc1d1	královská
moc	moc	k1gFnSc1	moc
značně	značně	k6eAd1	značně
zredukována	zredukovat	k5eAaPmNgFnS	zredukovat
ve	v	k7c4	v
prospěch	prospěch	k1gInSc4	prospěch
parlamentu	parlament	k1gInSc2	parlament
Riksdagu	Riksdag	k1gInSc2	Riksdag
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
střetávaly	střetávat	k5eAaImAgInP	střetávat
zejména	zejména	k9	zejména
frakce	frakce	k1gFnSc2	frakce
čepců	čepec	k1gInPc2	čepec
a	a	k8xC	a
klobouků	klobouk	k1gInPc2	klobouk
<g/>
.	.	kIx.	.
</s>
<s>
Až	až	k9	až
puč	puč	k1gInSc4	puč
Gustava	Gustav	k1gMnSc2	Gustav
III	III	kA	III
<g/>
.	.	kIx.	.
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
absolutistická	absolutistický	k2eAgFnSc1d1	absolutistická
vláda	vláda	k1gFnSc1	vláda
vrátila	vrátit	k5eAaPmAgFnS	vrátit
moc	moc	k6eAd1	moc
pevně	pevně	k6eAd1	pevně
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
krále	král	k1gMnSc2	král
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Ztráta	ztráta	k1gFnSc1	ztráta
Finska	Finsko	k1gNnSc2	Finsko
a	a	k8xC	a
nástup	nástup	k1gInSc1	nástup
rodu	rod	k1gInSc2	rod
Bernadotte	Bernadott	k1gMnSc5	Bernadott
==	==	k?	==
</s>
</p>
<p>
<s>
Napoleonské	napoleonský	k2eAgFnPc1d1	napoleonská
války	válka	k1gFnPc1	válka
a	a	k8xC	a
zejména	zejména	k9	zejména
boje	boj	k1gInPc1	boj
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
vedly	vést	k5eAaImAgFnP	vést
až	až	k9	až
k	k	k7c3	k
ztrátě	ztráta	k1gFnSc3	ztráta
Finska	Finsko	k1gNnSc2	Finsko
roku	rok	k1gInSc2	rok
1809	[number]	k4	1809
<g/>
.	.	kIx.	.
</s>
<s>
Švédská	švédský	k2eAgFnSc1d1	švédská
šlechta	šlechta	k1gFnSc1	šlechta
sesadila	sesadit	k5eAaPmAgFnS	sesadit
málo	málo	k6eAd1	málo
schopného	schopný	k2eAgMnSc4d1	schopný
krále	král	k1gMnSc4	král
Gustava	Gustav	k1gMnSc2	Gustav
IV	IV	kA	IV
<g/>
.	.	kIx.	.
</s>
<s>
Adolfa	Adolf	k1gMnSc2	Adolf
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
i	i	k8xC	i
s	s	k7c7	s
nejstarším	starý	k2eAgMnSc7d3	nejstarší
synem	syn	k1gMnSc7	syn
Gustavem	Gustav	k1gMnSc7	Gustav
a	a	k8xC	a
celou	celý	k2eAgFnSc7d1	celá
rodinou	rodina	k1gFnSc7	rodina
musel	muset	k5eAaImAgMnS	muset
odejít	odejít	k5eAaPmF	odejít
do	do	k7c2	do
exilu	exil	k1gInSc2	exil
v	v	k7c6	v
Německu	Německo	k1gNnSc6	Německo
<g/>
.	.	kIx.	.
</s>
<s>
Králem	Král	k1gMnSc7	Král
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1810	[number]	k4	1810
stal	stát	k5eAaPmAgMnS	stát
jeho	jeho	k3xOp3gMnSc1	jeho
bezdětný	bezdětný	k2eAgMnSc1d1	bezdětný
strýc	strýc	k1gMnSc1	strýc
Karel	Karel	k1gMnSc1	Karel
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
</s>
<s>
Politikové	politik	k1gMnPc1	politik
hledali	hledat	k5eAaImAgMnP	hledat
pro	pro	k7c4	pro
něj	on	k3xPp3gMnSc4	on
dostatečně	dostatečně	k6eAd1	dostatečně
silného	silný	k2eAgMnSc4d1	silný
nástupce	nástupce	k1gMnSc4	nástupce
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
zároveň	zároveň	k6eAd1	zároveň
podřídil	podřídit	k5eAaPmAgMnS	podřídit
Rigsdagu	Rigsdaga	k1gFnSc4	Rigsdaga
<g/>
.	.	kIx.	.
</s>
<s>
Takového	takový	k3xDgNnSc2	takový
se	se	k3xPyFc4	se
stoupencům	stoupenec	k1gMnPc3	stoupenec
Francie	Francie	k1gFnSc2	Francie
podařilo	podařit	k5eAaPmAgNnS	podařit
hned	hned	k6eAd1	hned
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1810	[number]	k4	1810
najít	najít	k5eAaPmF	najít
ve	v	k7c6	v
francouzském	francouzský	k2eAgMnSc6d1	francouzský
maršálu	maršál	k1gMnSc6	maršál
Jeanu-Baptiste	Jeanu-Baptist	k1gMnSc5	Jeanu-Baptist
Bernadottovi	Bernadott	k1gMnSc6	Bernadott
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgMnS	být
bezdětným	bezdětný	k2eAgMnSc7d1	bezdětný
králem	král	k1gMnSc7	král
adoptován	adoptovat	k5eAaPmNgMnS	adoptovat
jako	jako	k9	jako
syn	syn	k1gMnSc1	syn
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Karla	Karel	k1gMnSc2	Karel
XIII	XIII	kA	XIII
<g/>
.	.	kIx.	.
nastoupil	nastoupit	k5eAaPmAgMnS	nastoupit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1818	[number]	k4	1818
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
jako	jako	k8xC	jako
Karel	Karel	k1gMnSc1	Karel
XIV	XIV	kA	XIV
<g/>
.	.	kIx.	.
</s>
<s>
Johann	Johann	k1gMnSc1	Johann
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgMnSc3	ten
se	se	k3xPyFc4	se
podařilo	podařit	k5eAaPmAgNnS	podařit
pro	pro	k7c4	pro
Švédsko	Švédsko	k1gNnSc4	Švédsko
získat	získat	k5eAaPmF	získat
Norsko	Norsko	k1gNnSc4	Norsko
na	na	k7c4	na
úkor	úkor	k1gInSc4	úkor
Dánska	Dánsko	k1gNnSc2	Dánsko
smlouvou	smlouva	k1gFnSc7	smlouva
z	z	k7c2	z
Kielu	Kiel	k1gInSc2	Kiel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
skončení	skončení	k1gNnSc2	skončení
napoleonských	napoleonský	k2eAgFnPc2d1	napoleonská
válek	válka	k1gFnPc2	válka
se	se	k3xPyFc4	se
Švédsko	Švédsko	k1gNnSc1	Švédsko
<g/>
,	,	kIx,	,
také	také	k9	také
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
poloze	poloha	k1gFnSc3	poloha
na	na	k7c6	na
severovýchodním	severovýchodní	k2eAgInSc6d1	severovýchodní
okraji	okraj	k1gInSc6	okraj
Evropy	Evropa	k1gFnSc2	Evropa
<g/>
,	,	kIx,	,
nezúčastnilo	zúčastnit	k5eNaPmAgNnS	zúčastnit
již	již	k6eAd1	již
žádného	žádný	k3yNgInSc2	žádný
ozbrojeného	ozbrojený	k2eAgInSc2d1	ozbrojený
konfliktu	konflikt	k1gInSc2	konflikt
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
mu	on	k3xPp3gMnSc3	on
umožnilo	umožnit	k5eAaPmAgNnS	umožnit
značnou	značný	k2eAgFnSc4d1	značná
hospodářskou	hospodářský	k2eAgFnSc4d1	hospodářská
prosperitu	prosperita	k1gFnSc4	prosperita
a	a	k8xC	a
stabilní	stabilní	k2eAgInPc4d1	stabilní
společenské	společenský	k2eAgInPc4d1	společenský
poměry	poměr	k1gInPc4	poměr
<g/>
.	.	kIx.	.
</s>
<s>
Rod	rod	k1gInSc1	rod
Bernadotte	Bernadott	k1gInSc5	Bernadott
vládne	vládnout	k5eAaImIp3nS	vládnout
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
dosud	dosud	k6eAd1	dosud
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Od	od	k7c2	od
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
a	a	k8xC	a
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
2	[number]	k4	2
<g/>
.	.	kIx.	.
polovině	polovina	k1gFnSc6	polovina
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
se	se	k3xPyFc4	se
Švédsko	Švédsko	k1gNnSc1	Švédsko
začalo	začít	k5eAaPmAgNnS	začít
měnit	měnit	k5eAaImF	měnit
z	z	k7c2	z
převážně	převážně	k6eAd1	převážně
zemědělské	zemědělský	k2eAgFnSc2d1	zemědělská
společnosti	společnost	k1gFnSc2	společnost
v	v	k7c4	v
silnou	silný	k2eAgFnSc4d1	silná
industriální	industriální	k2eAgFnSc4d1	industriální
zemi	zem	k1gFnSc4	zem
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
vývoj	vývoj	k1gInSc1	vývoj
přinesl	přinést	k5eAaPmAgInS	přinést
i	i	k9	i
odchod	odchod	k1gInSc4	odchod
desítek	desítka	k1gFnPc2	desítka
tisíců	tisíc	k4xCgInPc2	tisíc
lidí	člověk	k1gMnPc2	člověk
do	do	k7c2	do
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Rozchod	rozchod	k1gInSc1	rozchod
unie	unie	k1gFnSc2	unie
s	s	k7c7	s
Norskem	Norsko	k1gNnSc7	Norsko
==	==	k?	==
</s>
</p>
<p>
<s>
Ačkoliv	ačkoliv	k8xS	ačkoliv
mělo	mít	k5eAaImAgNnS	mít
Norsko	Norsko	k1gNnSc1	Norsko
v	v	k7c6	v
unii	unie	k1gFnSc6	unie
značnou	značný	k2eAgFnSc4d1	značná
autonomii	autonomie	k1gFnSc4	autonomie
<g/>
,	,	kIx,	,
sílily	sílit	k5eAaImAgFnP	sílit
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
tendence	tendence	k1gFnSc2	tendence
k	k	k7c3	k
osamostatnění	osamostatnění	k1gNnSc3	osamostatnění
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1884	[number]	k4	1884
vzniká	vznikat	k5eAaImIp3nS	vznikat
první	první	k4xOgFnSc1	první
Norská	norský	k2eAgFnSc1d1	norská
vláda	vláda	k1gFnSc1	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Hlavním	hlavní	k2eAgInSc7d1	hlavní
bodem	bod	k1gInSc7	bod
rozporu	rozpor	k1gInSc2	rozpor
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
snaha	snaha	k1gFnSc1	snaha
Norů	Nor	k1gMnPc2	Nor
mít	mít	k5eAaImF	mít
vlastní	vlastní	k2eAgMnPc4d1	vlastní
konzuly	konzul	k1gMnPc4	konzul
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
<g/>
,	,	kIx,	,
kterýžto	kterýžto	k?	kterýžto
návrh	návrh	k1gInSc1	návrh
král	král	k1gMnSc1	král
Oskar	Oskar	k1gMnSc1	Oskar
II	II	kA	II
<g/>
.	.	kIx.	.
třikrát	třikrát	k6eAd1	třikrát
vetoval	vetovat	k5eAaBmAgMnS	vetovat
<g/>
.	.	kIx.	.
</s>
<s>
Pomalu	pomalu	k6eAd1	pomalu
se	se	k3xPyFc4	se
schylovalo	schylovat	k5eAaImAgNnS	schylovat
k	k	k7c3	k
válce	válka	k1gFnSc3	válka
a	a	k8xC	a
obě	dva	k4xCgFnPc1	dva
země	zem	k1gFnPc1	zem
provedly	provést	k5eAaPmAgFnP	provést
několik	několik	k4yIc4	několik
vojenských	vojenský	k2eAgNnPc2d1	vojenské
cvičení	cvičení	k1gNnPc2	cvičení
<g/>
.	.	kIx.	.
</s>
<s>
Norsko	Norsko	k1gNnSc1	Norsko
se	se	k3xPyFc4	se
chtělo	chtít	k5eAaImAgNnS	chtít
odtrhnout	odtrhnout	k5eAaPmF	odtrhnout
<g/>
,	,	kIx,	,
což	což	k3yQnSc4	což
král	král	k1gMnSc1	král
podmiňoval	podmiňovat	k5eAaImAgMnS	podmiňovat
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgFnPc4d1	jiná
referendem	referendum	k1gNnSc7	referendum
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
v	v	k7c6	v
Norsku	Norsko	k1gNnSc6	Norsko
uskutečnilo	uskutečnit	k5eAaPmAgNnS	uskutečnit
s	s	k7c7	s
jednoznačným	jednoznačný	k2eAgInSc7d1	jednoznačný
výsledkem	výsledek	k1gInSc7	výsledek
pro	pro	k7c4	pro
rozdělení	rozdělení	k1gNnSc4	rozdělení
unie	unie	k1gFnSc2	unie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
schůzkách	schůzka	k1gFnPc6	schůzka
v	v	k7c6	v
Karlstadu	Karlstad	k1gInSc6	Karlstad
pak	pak	k6eAd1	pak
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
mírovému	mírový	k2eAgNnSc3d1	Mírové
rozdělení	rozdělení	k1gNnSc3	rozdělení
obou	dva	k4xCgInPc2	dva
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Norskou	norský	k2eAgFnSc4d1	norská
korunu	koruna	k1gFnSc4	koruna
nabídnutou	nabídnutý	k2eAgFnSc4d1	nabídnutá
rodu	rod	k1gInSc3	rod
Bernadotte	Bernadott	k1gInSc5	Bernadott
král	král	k1gMnSc1	král
Oskar	Oskar	k1gMnSc1	Oskar
II	II	kA	II
<g/>
.	.	kIx.	.
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Moderní	moderní	k2eAgFnPc1d1	moderní
dějiny	dějiny	k1gFnPc1	dějiny
==	==	k?	==
</s>
</p>
<p>
<s>
Švédsko	Švédsko	k1gNnSc1	Švédsko
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
neutrální	neutrální	k2eAgFnSc4d1	neutrální
v	v	k7c6	v
obou	dva	k4xCgFnPc6	dva
válkách	válka	k1gFnPc6	válka
a	a	k8xC	a
využilo	využít	k5eAaPmAgNnS	využít
svého	svůj	k3xOyFgNnSc2	svůj
nerostného	nerostný	k2eAgNnSc2d1	nerostné
bohatství	bohatství	k1gNnSc2	bohatství
(	(	kIx(	(
<g/>
zejména	zejména	k9	zejména
výroby	výroba	k1gFnSc2	výroba
oceli	ocel	k1gFnSc2	ocel
<g/>
)	)	kIx)	)
k	k	k7c3	k
posílení	posílení	k1gNnSc3	posílení
průmyslu	průmysl	k1gInSc2	průmysl
a	a	k8xC	a
stalo	stát	k5eAaPmAgNnS	stát
se	se	k3xPyFc4	se
jednou	jeden	k4xCgFnSc7	jeden
z	z	k7c2	z
nejbohatších	bohatý	k2eAgFnPc2d3	nejbohatší
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Roku	rok	k1gInSc2	rok
1995	[number]	k4	1995
vstoupilo	vstoupit	k5eAaPmAgNnS	vstoupit
do	do	k7c2	do
EU	EU	kA	EU
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
ve	v	k7c6	v
Švédsku	Švédsko	k1gNnSc6	Švédsko
vládne	vládnout	k5eAaImIp3nS	vládnout
král	král	k1gMnSc1	král
Karel	Karel	k1gMnSc1	Karel
XVI	XVI	kA	XVI
<g/>
.	.	kIx.	.
</s>
<s>
Gustav	Gustav	k1gMnSc1	Gustav
<g/>
,	,	kIx,	,
následnicí	následnice	k1gFnSc7	následnice
trůnu	trůn	k1gInSc2	trůn
je	být	k5eAaImIp3nS	být
princezna	princezna	k1gFnSc1	princezna
Victoria	Victorium	k1gNnSc2	Victorium
Švédská	švédský	k2eAgFnSc1d1	švédská
</s>
</p>
<p>
<s>
==	==	k?	==
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Dějiny	dějiny	k1gFnPc4	dějiny
Švédska	Švédsko	k1gNnSc2	Švédsko
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Švédské	švédský	k2eAgFnPc1d1	švédská
dějiny	dějiny	k1gFnPc1	dějiny
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
-	-	kIx~	-
video	video	k1gNnSc1	video
z	z	k7c2	z
cyklu	cyklus	k1gInSc2	cyklus
České	český	k2eAgFnSc2d1	Česká
televize	televize	k1gFnSc2	televize
Historický	historický	k2eAgInSc1d1	historický
magazín	magazín	k1gInSc1	magazín
</s>
</p>
<p>
<s>
https://www.youtube.com/watch?v=IvcN0saJoIM	[url]	k4	https://www.youtube.com/watch?v=IvcN0saJoIM
video	video	k1gNnSc1	video
územního	územní	k2eAgInSc2d1	územní
vývoje	vývoj	k1gInSc2	vývoj
Švédska	Švédsko	k1gNnSc2	Švédsko
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
je	být	k5eAaImIp3nS	být
zvýrazněn	zvýraznit	k5eAaPmNgInS	zvýraznit
každý	každý	k3xTgInSc1	každý
rok	rok	k1gInSc1	rok
<g/>
.	.	kIx.	.
</s>
</p>
