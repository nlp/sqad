<p>
<s>
Plšík	plšík	k1gMnSc1	plšík
lískový	lískový	k2eAgMnSc1d1	lískový
(	(	kIx(	(
<g/>
Muscardinus	Muscardinus	k1gMnSc1	Muscardinus
avellanarius	avellanarius	k1gMnSc1	avellanarius
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
malý	malý	k2eAgMnSc1d1	malý
hlodavec	hlodavec	k1gMnSc1	hlodavec
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
také	také	k9	také
jediným	jediný	k2eAgMnSc7d1	jediný
zástupcem	zástupce	k1gMnSc7	zástupce
rodu	rod	k1gInSc2	rod
Muscardinus	Muscardinus	k1gInSc1	Muscardinus
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
České	český	k2eAgFnSc6d1	Česká
republice	republika	k1gFnSc6	republika
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
nejhojnějšího	hojný	k2eAgMnSc4d3	nejhojnější
zástupce	zástupce	k1gMnSc4	zástupce
čeledi	čeleď	k1gFnSc3	čeleď
plchovití	plchovitý	k2eAgMnPc1d1	plchovitý
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Výskyt	výskyt	k1gInSc1	výskyt
==	==	k?	==
</s>
</p>
<p>
<s>
Plšík	plšík	k1gMnSc1	plšík
lískový	lískový	k2eAgMnSc1d1	lískový
je	být	k5eAaImIp3nS	být
obyvatelem	obyvatel	k1gMnSc7	obyvatel
lesů	les	k1gInPc2	les
severní	severní	k2eAgFnSc2d1	severní
a	a	k8xC	a
střední	střední	k2eAgFnSc2d1	střední
Evropy	Evropa	k1gFnSc2	Evropa
a	a	k8xC	a
Malé	Malé	k2eAgFnSc2d1	Malé
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Stavba	stavba	k1gFnSc1	stavba
těla	tělo	k1gNnSc2	tělo
==	==	k?	==
</s>
</p>
<p>
<s>
Jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
malého	malý	k2eAgMnSc4d1	malý
zástupce	zástupce	k1gMnSc4	zástupce
plchů	plch	k1gMnPc2	plch
<g/>
,	,	kIx,	,
dorůstá	dorůstat	k5eAaImIp3nS	dorůstat
sotva	sotva	k6eAd1	sotva
6	[number]	k4	6
<g/>
–	–	k?	–
<g/>
9	[number]	k4	9
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
poměru	poměr	k1gInSc3	poměr
těla	tělo	k1gNnSc2	tělo
má	mít	k5eAaImIp3nS	mít
velmi	velmi	k6eAd1	velmi
dlouhý	dlouhý	k2eAgInSc1d1	dlouhý
huňatý	huňatý	k2eAgInSc1d1	huňatý
ocas	ocas	k1gInSc1	ocas
(	(	kIx(	(
<g/>
5,7	[number]	k4	5,7
<g/>
–	–	k?	–
<g/>
7,5	[number]	k4	7,5
cm	cm	kA	cm
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
případě	případ	k1gInSc6	případ
nebezpečí	nebezpečí	k1gNnSc2	nebezpečí
(	(	kIx(	(
<g/>
plšíka	plšík	k1gMnSc4	plšík
chytí	chytit	k5eAaPmIp3nS	chytit
predátor	predátor	k1gMnSc1	predátor
za	za	k7c4	za
ocas	ocas	k1gInSc4	ocas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
může	moct	k5eAaImIp3nS	moct
stáhnout	stáhnout	k5eAaPmF	stáhnout
z	z	k7c2	z
kůže	kůže	k1gFnSc2	kůže
<g/>
.	.	kIx.	.
</s>
<s>
Váží	vážit	k5eAaImIp3nS	vážit
17	[number]	k4	17
<g/>
–	–	k?	–
<g/>
20	[number]	k4	20
g	g	kA	g
<g/>
,	,	kIx,	,
těsně	těsně	k6eAd1	těsně
před	před	k7c7	před
hibernací	hibernace	k1gFnSc7	hibernace
se	se	k3xPyFc4	se
však	však	k9	však
jeho	jeho	k3xOp3gFnSc1	jeho
hmotnost	hmotnost	k1gFnSc1	hmotnost
pohybuje	pohybovat	k5eAaImIp3nS	pohybovat
mezi	mezi	k7c7	mezi
30	[number]	k4	30
<g/>
–	–	k?	–
<g/>
40	[number]	k4	40
g.	g.	k?	g.
Výborný	výborný	k2eAgMnSc1d1	výborný
lezec	lezec	k1gMnSc1	lezec
a	a	k8xC	a
skokan	skokan	k1gMnSc1	skokan
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
oranžovožlutého	oranžovožlutý	k2eAgNnSc2d1	oranžovožlutý
zbarvení	zbarvení	k1gNnSc2	zbarvení
s	s	k7c7	s
poněkud	poněkud	k6eAd1	poněkud
světlejší	světlý	k2eAgFnSc7d2	světlejší
spodinou	spodina	k1gFnSc7	spodina
těla	tělo	k1gNnSc2	tělo
<g/>
.	.	kIx.	.
</s>
<s>
Zajímavostí	zajímavost	k1gFnSc7	zajímavost
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
plšíci	plšík	k1gMnPc1	plšík
lískoví	lískový	k2eAgMnPc1d1	lískový
jako	jako	k8xC	jako
jediní	jediný	k2eAgMnPc1d1	jediný
savci	savec	k1gMnPc1	savec
nemají	mít	k5eNaImIp3nP	mít
slepé	slepý	k2eAgNnSc1d1	slepé
střevo	střevo	k1gNnSc1	střevo
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
jejich	jejich	k3xOp3gFnSc1	jejich
potrava	potrava	k1gFnSc1	potrava
obsahuje	obsahovat	k5eAaImIp3nS	obsahovat
málo	málo	k1gNnSc4	málo
celulózy	celulóza	k1gFnSc2	celulóza
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Způsob	způsob	k1gInSc1	způsob
života	život	k1gInSc2	život
==	==	k?	==
</s>
</p>
<p>
<s>
Plšík	plšík	k1gMnSc1	plšík
lískový	lískový	k2eAgMnSc1d1	lískový
je	být	k5eAaImIp3nS	být
noční	noční	k2eAgMnSc1d1	noční
savec	savec	k1gMnSc1	savec
<g/>
.	.	kIx.	.
</s>
<s>
Přes	přes	k7c4	přes
léto	léto	k1gNnSc4	léto
si	se	k3xPyFc3	se
staví	stavit	k5eAaBmIp3nP	stavit
poměrně	poměrně	k6eAd1	poměrně
malá	malý	k2eAgNnPc1d1	malé
kulovitá	kulovitý	k2eAgNnPc1d1	kulovité
hnízda	hnízdo	k1gNnPc1	hnízdo
spletená	spletený	k2eAgNnPc1d1	spletené
z	z	k7c2	z
trávy	tráva	k1gFnSc2	tráva
a	a	k8xC	a
listí	listí	k1gNnSc1	listí
<g/>
,	,	kIx,	,
v	v	k7c6	v
zimě	zima	k1gFnSc6	zima
si	se	k3xPyFc3	se
staví	stavit	k5eAaBmIp3nS	stavit
rozměrnější	rozměrný	k2eAgNnSc4d2	rozměrnější
hnízdo	hnízdo	k1gNnSc4	hnízdo
v	v	k7c6	v
noře	nora	k1gFnSc6	nora
nebo	nebo	k8xC	nebo
pod	pod	k7c7	pod
mechem	mech	k1gInSc7	mech
a	a	k8xC	a
listím	listí	k1gNnSc7	listí
<g/>
,	,	kIx,	,
kam	kam	k6eAd1	kam
si	se	k3xPyFc3	se
nosí	nosit	k5eAaImIp3nP	nosit
zásoby	zásoba	k1gFnPc4	zásoba
na	na	k7c4	na
zimu	zima	k1gFnSc4	zima
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
nektarem	nektar	k1gInSc7	nektar
květin	květina	k1gFnPc2	květina
<g/>
,	,	kIx,	,
požírá	požírat	k5eAaImIp3nS	požírat
různé	různý	k2eAgInPc4d1	různý
plody	plod	k1gInPc4	plod
(	(	kIx(	(
<g/>
bobule	bobule	k1gFnSc1	bobule
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
hmyz	hmyz	k1gInSc1	hmyz
(	(	kIx(	(
<g/>
mšice	mšice	k1gFnPc1	mšice
<g/>
,	,	kIx,	,
housenky	housenka	k1gFnPc1	housenka
<g/>
)	)	kIx)	)
a	a	k8xC	a
pupeny	pupen	k1gInPc1	pupen
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
před	před	k7c7	před
hibernací	hibernace	k1gFnSc7	hibernace
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
probíhá	probíhat	k5eAaImIp3nS	probíhat
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
října	říjen	k1gInSc2	říjen
do	do	k7c2	do
dubna	duben	k1gInSc2	duben
nebo	nebo	k8xC	nebo
května	květen	k1gInSc2	květen
<g/>
,	,	kIx,	,
zejména	zejména	k9	zejména
lískové	lískový	k2eAgInPc1d1	lískový
ořechy	ořech	k1gInPc1	ořech
<g/>
,	,	kIx,	,
díky	díky	k7c3	díky
čemuž	což	k3yRnSc3	což
získal	získat	k5eAaPmAgMnS	získat
svůj	svůj	k3xOyFgInSc4	svůj
název	název	k1gInSc4	název
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
jazycích	jazyk	k1gInPc6	jazyk
<g/>
.	.	kIx.	.
</s>
<s>
Plšíci	plšík	k1gMnPc1	plšík
jsou	být	k5eAaImIp3nP	být
samotářští	samotářský	k2eAgMnPc1d1	samotářský
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
těsné	těsný	k2eAgFnSc6d1	těsná
blízkosti	blízkost	k1gFnSc6	blízkost
může	moct	k5eAaImIp3nS	moct
žít	žít	k5eAaImF	žít
i	i	k9	i
několik	několik	k4yIc1	několik
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
pak	pak	k6eAd1	pak
dělí	dělit	k5eAaImIp3nS	dělit
o	o	k7c4	o
potravu	potrava	k1gFnSc4	potrava
a	a	k8xC	a
dorozumívají	dorozumívat	k5eAaImIp3nP	dorozumívat
se	s	k7c7	s
širokou	široký	k2eAgFnSc7d1	široká
škálou	škála	k1gFnSc7	škála
vrčivých	vrčivý	k2eAgInPc2d1	vrčivý
až	až	k8xS	až
hvízdavých	hvízdavý	k2eAgInPc2d1	hvízdavý
zvuků	zvuk	k1gInPc2	zvuk
<g/>
.	.	kIx.	.
</s>
<s>
Samice	samice	k1gFnPc1	samice
jsou	být	k5eAaImIp3nP	být
březí	březí	k1gNnSc4	březí
22	[number]	k4	22
<g/>
–	–	k?	–
<g/>
24	[number]	k4	24
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
v	v	k7c6	v
jednom	jeden	k4xCgInSc6	jeden
vrhu	vrh	k1gInSc6	vrh
rodí	rodit	k5eAaImIp3nP	rodit
až	až	k9	až
sedm	sedm	k4xCc4	sedm
mláďat	mládě	k1gNnPc2	mládě
a	a	k8xC	a
ročně	ročně	k6eAd1	ročně
mívají	mívat	k5eAaImIp3nP	mívat
kolem	kolem	k7c2	kolem
dvou	dva	k4xCgInPc2	dva
vrhů	vrh	k1gInPc2	vrh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Hazel	Hazel	k1gInSc1	Hazel
Dormouse	Dormouse	k1gFnSc2	Dormouse
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
GAISLER	GAISLER	kA	GAISLER
<g/>
,	,	kIx,	,
Jiří	Jiří	k1gMnSc1	Jiří
<g/>
;	;	kIx,	;
DUNGEL	DUNGEL	kA	DUNGEL
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Atlas	Atlas	k1gInSc1	Atlas
savců	savec	k1gMnPc2	savec
České	český	k2eAgFnSc2d1	Česká
a	a	k8xC	a
Slovenské	slovenský	k2eAgFnSc2d1	slovenská
republiky	republika	k1gFnSc2	republika
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Academia	academia	k1gFnSc1	academia
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
152	[number]	k4	152
s.	s.	k?	s.
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
200	[number]	k4	200
<g/>
-	-	kIx~	-
<g/>
1026	[number]	k4	1026
<g/>
-	-	kIx~	-
<g/>
2	[number]	k4	2
<g/>
.	.	kIx.	.
</s>
<s>
S.	S.	kA	S.
84	[number]	k4	84
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Zvíře	zvíře	k1gNnSc1	zvíře
<g/>
.	.	kIx.	.
</s>
<s>
Obrazová	obrazový	k2eAgFnSc1d1	obrazová
encyklopedie	encyklopedie	k1gFnSc1	encyklopedie
živočichů	živočich	k1gMnPc2	živočich
všech	všecek	k3xTgInPc2	všecek
kontinentů	kontinent	k1gInPc2	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
<g/>
:	:	kIx,	:
Knižní	knižní	k2eAgInSc1d1	knižní
klub	klub	k1gInSc1	klub
<g/>
,	,	kIx,	,
2007	[number]	k4	2007
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80	[number]	k4	80
<g/>
-	-	kIx~	-
<g/>
242	[number]	k4	242
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
862	[number]	k4	862
<g/>
-	-	kIx~	-
<g/>
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
plšík	plšík	k1gMnSc1	plšík
lískový	lískový	k2eAgMnSc1d1	lískový
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Muscardinus	Muscardinus	k1gInSc1	Muscardinus
avellanarius	avellanarius	k1gInSc1	avellanarius
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
