<s>
AGESA	AGESA	kA
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
z	z	k7c2
anglického	anglický	k2eAgNnSc2d1
AMD	AMD	kA
Generic	Generic	k1gMnSc1
Encapsulated	Encapsulated	k1gMnSc1
Software	software	k1gInSc1
Architecture	Architectur	k1gMnSc5
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
v	v	k7c6
oboru	obor	k1gInSc6
osobních	osobní	k2eAgInPc2d1
počítačů	počítač	k1gInPc2
označení	označení	k1gNnSc1
pro	pro	k7c4
knihovnu	knihovna	k1gFnSc4
sloužící	sloužící	k1gFnSc2
na	na	k7c6
platformě	platforma	k1gFnSc6
x	x	k?
<g/>
86	#num#	k4
<g/>
-	-	kIx~
<g/>
64	#num#	k4
pro	pro	k7c4
inicializaci	inicializace	k1gFnSc4
základní	základní	k2eAgFnSc2d1
desky	deska	k1gFnSc2
<g/>
,	,	kIx,
především	především	k6eAd1
jader	jádro	k1gNnPc2
centrální	centrální	k2eAgFnSc2d1
procesorové	procesorový	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
<g/>
,	,	kIx,
řadiče	řadič	k1gMnSc2
operační	operační	k2eAgFnSc2d1
paměti	paměť	k1gFnSc2
a	a	k8xC
řadiče	řadič	k1gInSc2
HyperTransportu	HyperTransport	k1gInSc2
<g/>
.	.	kIx.
</s>