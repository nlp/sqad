<s>
AGESA	AGESA	kA
</s>
<s>
AGESA	AGESA	kA
(	(	kIx(
<g/>
zkratka	zkratka	k1gFnSc1
z	z	k7c2
anglického	anglický	k2eAgNnSc2d1
AMD	AMD	kA
Generic	Generic	k1gMnSc1
Encapsulated	Encapsulated	k1gMnSc1
Software	software	k1gInSc1
Architecture	Architectur	k1gMnSc5
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
v	v	k7c6
oboru	obor	k1gInSc6
osobních	osobní	k2eAgInPc2d1
počítačů	počítač	k1gInPc2
označení	označení	k1gNnSc1
pro	pro	k7c4
knihovnu	knihovna	k1gFnSc4
sloužící	sloužící	k1gFnSc2
na	na	k7c6
platformě	platforma	k1gFnSc6
x	x	k?
<g/>
86	#num#	k4
<g/>
-	-	kIx~
<g/>
64	#num#	k4
pro	pro	k7c4
inicializaci	inicializace	k1gFnSc4
základní	základní	k2eAgFnSc2d1
desky	deska	k1gFnSc2
<g/>
,	,	kIx,
především	především	k6eAd1
jader	jádro	k1gNnPc2
centrální	centrální	k2eAgFnSc2d1
procesorové	procesorový	k2eAgFnSc2d1
jednotky	jednotka	k1gFnSc2
<g/>
,	,	kIx,
řadiče	řadič	k1gMnSc2
operační	operační	k2eAgFnSc2d1
paměti	paměť	k1gFnSc2
a	a	k8xC
řadiče	řadič	k1gInSc2
HyperTransportu	HyperTransport	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společnost	společnost	k1gFnSc1
Advanced	Advanced	k1gMnSc1
Micro	Micro	k1gNnSc1
Devices	Devices	k1gMnSc1
(	(	kIx(
<g/>
AMD	AMD	kA
<g/>
)	)	kIx)
ji	on	k3xPp3gFnSc4
vyvíjí	vyvíjet	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2003	#num#	k4
a	a	k8xC
je	být	k5eAaImIp3nS
využívána	využívat	k5eAaPmNgFnS,k5eAaImNgFnS
výrobci	výrobce	k1gMnPc7
základních	základní	k2eAgFnPc2d1
desek	deska	k1gFnPc2
<g/>
,	,	kIx,
respektive	respektive	k9
společnostmi	společnost	k1gFnPc7
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yRgFnPc1,k3yIgFnPc1
pro	pro	k7c4
ně	on	k3xPp3gMnPc4
vyvíjejí	vyvíjet	k5eAaImIp3nP
základní	základní	k2eAgInSc1d1
firmware	firmware	k1gInSc1
<g/>
:	:	kIx,
takzvaný	takzvaný	k2eAgMnSc1d1
BIOS	BIOS	kA
<g/>
,	,	kIx,
v	v	k7c6
novější	nový	k2eAgFnSc6d2
době	doba	k1gFnSc6
implementaci	implementace	k1gFnSc4
standardu	standard	k1gInSc2
UEFI	UEFI	kA
<g/>
.	.	kIx.
</s>
<s>
Starší	starý	k2eAgFnSc1d2
verze	verze	k1gFnSc1
<g/>
,	,	kIx,
do	do	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
a	a	k8xC
nejpozději	pozdě	k6eAd3
pro	pro	k7c4
procesory	procesor	k1gInPc4
Opteron	Opteron	k1gMnSc1
v	v	k7c6
generacích	generace	k1gFnPc6
AMD	AMD	kA
K8	K8	k1gMnSc7
a	a	k8xC
AMD	AMD	kA
K	k	k7c3
<g/>
9	#num#	k4
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
implementovány	implementovat	k5eAaImNgFnP
v	v	k7c6
jazyce	jazyk	k1gInSc6
symbolických	symbolický	k2eAgFnPc2d1
adres	adresa	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Počínaje	počínaje	k7c7
generací	generace	k1gFnSc7
AMD	AMD	kA
10	#num#	k4
<g/>
h	h	k?
je	být	k5eAaImIp3nS
AGESA	AGESA	kA
implementována	implementován	k2eAgFnSc1d1
v	v	k7c6
C.	C.	kA
</s>
<s>
Vzhledem	vzhledem	k7c3
k	k	k7c3
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
AGESA	AGESA	kA
inicializuje	inicializovat	k5eAaImIp3nS
procesory	procesor	k1gInPc4
<g/>
,	,	kIx,
může	moct	k5eAaImIp3nS
sloužit	sloužit	k5eAaImF
například	například	k6eAd1
k	k	k7c3
vypnutí	vypnutí	k1gNnSc3
PSP	PSP	kA
<g/>
,	,	kIx,
tedy	tedy	k8xC
implementace	implementace	k1gFnSc2
TEE	Tea	k1gFnSc3
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
nebo	nebo	k8xC
k	k	k7c3
vyladění	vyladění	k1gNnSc3
výkonu	výkon	k1gInSc2
procesoru	procesor	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byly	být	k5eAaImAgInP
použity	použit	k2eAgInPc1d1
překlady	překlad	k1gInPc1
textů	text	k1gInPc2
z	z	k7c2
článků	článek	k1gInPc2
AMD	AMD	kA
Generic	Generic	k1gMnSc1
Encapsulated	Encapsulated	k1gMnSc1
Software	software	k1gInSc1
Architecture	Architectur	k1gMnSc5
na	na	k7c6
německé	německý	k2eAgFnSc3d1
Wikipedii	Wikipedie	k1gFnSc3
a	a	k8xC
AGESA	AGESA	kA
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
FIKAR	FIKAR	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
AMD	AMD	kA
umožňuje	umožňovat	k5eAaImIp3nS
vypnout	vypnout	k5eAaPmF
Platform	Platform	k1gInSc4
Security	Securita	k1gFnSc2
Processor	Processora	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Root	Root	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1212	#num#	k4
<g/>
-	-	kIx~
<g/>
8309	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
FIKAR	FIKAR	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktualizace	aktualizace	k1gFnSc1
AGESA	AGESA	kA
zlepší	zlepšit	k5eAaPmIp3nS
výkon	výkon	k1gInSc4
druhé	druhý	k4xOgFnSc2
generace	generace	k1gFnSc2
procesorů	procesor	k1gInPc2
Ryzen	ryzna	k1gFnPc2
v	v	k7c6
Linuxu	linux	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Root	Root	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1212	#num#	k4
<g/>
-	-	kIx~
<g/>
8309	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Prezentace	prezentace	k1gFnSc1
k	k	k7c3
projektu	projekt	k1gInSc3
AGESA	AGESA	kA
z	z	k7c2
roku	rok	k1gInSc2
2006	#num#	k4
projektu	projekt	k1gInSc2
coreboot	coreboot	k1gInSc1
(	(	kIx(
<g/>
PDF	PDF	kA
<g/>
,	,	kIx,
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
</s>
