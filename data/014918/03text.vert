<s>
Vestland	Vestland	k1gInSc1
</s>
<s>
Vestland	Vestland	k1gInSc1
Vestland	Vestland	k1gInSc1
na	na	k7c6
mapě	mapa	k1gFnSc6
Norska	Norsko	k1gNnSc2
</s>
<s>
znak	znak	k1gInSc1
</s>
<s>
Geografie	geografie	k1gFnSc1
Hlavní	hlavní	k2eAgNnSc1d1
město	město	k1gNnSc1
</s>
<s>
Bergen	Bergen	k1gMnSc1
<g/>
,	,	kIx,
Leikanger	Leikanger	k1gMnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
60	#num#	k4
<g/>
°	°	k?
<g/>
50	#num#	k4
<g/>
′	′	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
6	#num#	k4
<g/>
°	°	k?
<g/>
30	#num#	k4
<g/>
′	′	k?
v.	v.	k?
d.	d.	k?
Rozloha	rozloha	k1gFnSc1
</s>
<s>
33	#num#	k4
870,98	870,98	k4
km²	km²	k?
Geodata	Geodat	k1gMnSc2
(	(	kIx(
<g/>
OSM	osm	k4xCc1
<g/>
)	)	kIx)
</s>
<s>
OSM	osm	k4xCc1
<g/>
,	,	kIx,
WMF	WMF	kA
Obyvatelstvo	obyvatelstvo	k1gNnSc1
Počet	počet	k1gInSc1
obyvatel	obyvatel	k1gMnSc1
</s>
<s>
631	#num#	k4
594	#num#	k4
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
Hustota	hustota	k1gFnSc1
zalidnění	zalidnění	k1gNnSc2
</s>
<s>
18,6	18,6	k4
obyv	obyv	k1gInSc1
<g/>
.	.	kIx.
<g/>
/	/	kIx~
<g/>
km²	km²	k?
Správa	správa	k1gFnSc1
regionu	region	k1gInSc2
Podřízené	podřízený	k2eAgInPc1d1
celky	celek	k1gInPc1
</s>
<s>
43	#num#	k4
obcí	obec	k1gFnPc2
Vznik	vznik	k1gInSc4
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2020	#num#	k4
Fylkesordfø	Fylkesordfø	k1gFnPc2
</s>
<s>
Jon	Jon	k?
Askeland	Askeland	k1gInSc4
Mezinárodní	mezinárodní	k2eAgFnSc2d1
identifikace	identifikace	k1gFnSc2
ISO	ISO	kA
3166-2	3166-2	k4
</s>
<s>
NO-46	NO-46	k4
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.vlfk.no	www.vlfk.no	k6eAd1
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Vestland	Vestland	k1gInSc1
je	být	k5eAaImIp3nS
kraj	kraj	k1gInSc4
v	v	k7c6
západním	západní	k2eAgNnSc6d1
Norsku	Norsko	k1gNnSc6
(	(	kIx(
<g/>
Vestlandet	Vestlandet	k1gMnSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Začal	začít	k5eAaPmAgMnS
fungovat	fungovat	k5eAaImF
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2020	#num#	k4
sloučením	sloučení	k1gNnSc7
dvou	dva	k4xCgMnPc6
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
samostatných	samostatný	k2eAgInPc2d1
krajů	kraj	k1gInPc2
Hordaland	Hordaland	k1gInSc1
a	a	k8xC
Sogn	Sogn	k1gInSc1
og	og	k?
Fjordane	Fjordan	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
K	k	k7c3
reorganizaci	reorganizace	k1gFnSc3
územního	územní	k2eAgNnSc2d1
uspořádání	uspořádání	k1gNnSc2
došlo	dojít	k5eAaPmAgNnS
na	na	k7c6
základě	základ	k1gInSc6
rozhodnutí	rozhodnutí	k1gNnSc2
norského	norský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
(	(	kIx(
<g/>
Stortinget	stortinget	k1gInSc1
<g/>
)	)	kIx)
ze	z	k7c2
dne	den	k1gInSc2
8	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
důsledkem	důsledek	k1gInSc7
byla	být	k5eAaImAgFnS
redukce	redukce	k1gFnSc1
počtu	počet	k1gInSc2
územěsprávních	územěsprávní	k2eAgFnPc2d1
jednotek	jednotka	k1gFnPc2
z	z	k7c2
devatenácti	devatenáct	k4xCc2
na	na	k7c4
jedenáct	jedenáct	k4xCc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Sousedí	sousedit	k5eAaImIp3nS
s	s	k7c7
kraji	kraj	k1gInPc7
Mø	Mø	k1gInSc5
og	og	k?
Romsdal	Romsdal	k1gFnSc1
<g/>
,	,	kIx,
Innlandet	Innlandet	k1gInSc1
<g/>
,	,	kIx,
Viken	Viken	k1gInSc1
<g/>
,	,	kIx,
Vestfold	Vestfold	k1gInSc1
og	og	k?
Telemark	telemark	k1gInSc1
a	a	k8xC
Rogaland	Rogaland	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Obce	obec	k1gFnPc1
</s>
<s>
V	v	k7c6
lednu	leden	k1gInSc6
2020	#num#	k4
sestával	sestávat	k5eAaImAgInS
kraj	kraj	k1gInSc1
z	z	k7c2
43	#num#	k4
obcí	obec	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Alver	Alver	k1gMnSc1
</s>
<s>
Askvoll	Askvoll	k1gMnSc1
</s>
<s>
Askø	Askø	k1gFnPc1
</s>
<s>
Aurland	Aurland	k1gInSc1
</s>
<s>
Austevoll	Austevoll	k1gMnSc1
</s>
<s>
Austrheim	Austrheim	k6eAd1
</s>
<s>
Bergen	Bergen	k1gMnSc1
</s>
<s>
Bjø	Bjø	k1gFnPc2
</s>
<s>
Bremanger	Bremanger	k1gMnSc1
</s>
<s>
Bø	Bø	k1gNnSc1
</s>
<s>
Eidfjord	Eidfjord	k6eAd1
</s>
<s>
Etne	Etne	k6eAd1
</s>
<s>
Fedje	Fedje	k1gFnSc1
</s>
<s>
Fitjar	Fitjar	k1gMnSc1
</s>
<s>
Fjaler	Fjaler	k1gMnSc1
</s>
<s>
Gloppen	Gloppen	k2eAgInSc1d1
</s>
<s>
Gulen	Gulen	k1gInSc1
</s>
<s>
Hyllestad	Hyllestad	k6eAd1
</s>
<s>
Hø	Hø	k1gMnSc1
</s>
<s>
Kinn	Kinn	k1gMnSc1
</s>
<s>
Kvam	Kvam	k6eAd1
</s>
<s>
Kvinnherad	Kvinnherad	k1gInSc1
</s>
<s>
Luster	lustrum	k1gNnPc2
</s>
<s>
Læ	Læ	k5eAaPmAgMnS,k5eAaImAgMnS
</s>
<s>
Masfjorden	Masfjordna	k1gFnPc2
</s>
<s>
Modalen	Modalen	k2eAgInSc1d1
</s>
<s>
Osterø	Osterø	k1gFnPc1
</s>
<s>
Samnanger	Samnanger	k1gMnSc1
</s>
<s>
Sogndal	Sogndat	k5eAaPmAgMnS
</s>
<s>
Solund	Solund	k1gMnSc1
</s>
<s>
Stad	Stad	k6eAd1
</s>
<s>
Stord	Stord	k6eAd1
</s>
<s>
Stryn	Stryn	k?
</s>
<s>
Sunnfjord	Sunnfjord	k6eAd1
</s>
<s>
Sveio	Sveio	k6eAd1
</s>
<s>
Tysnes	Tysnes	k1gMnSc1
</s>
<s>
Ullensvang	Ullensvang	k1gMnSc1
</s>
<s>
Ulvik	Ulvik	k1gMnSc1
</s>
<s>
Vaksdal	Vaksdat	k5eAaImAgMnS,k5eAaPmAgMnS
</s>
<s>
VikVik	VikVik	k1gMnSc1
</s>
<s>
Voss	Voss	k6eAd1
herad	herad	k6eAd1
</s>
<s>
Ø	Ø	k1gFnPc2
</s>
<s>
Å	Å	k5eAaPmAgMnS,k5eAaImAgMnS
</s>
<s>
Fotogalerie	Fotogalerie	k1gFnSc1
</s>
<s>
ruiny	ruina	k1gFnPc1
kláštera	klášter	k1gInSc2
Selje	Selj	k1gFnPc1
</s>
<s>
město	město	k1gNnSc1
Berge	Berg	k1gInSc2
</s>
<s>
fjord	fjord	k1gInSc1
Sognefjord	Sognefjorda	k1gFnPc2
</s>
<s>
horský	horský	k2eAgInSc4d1
masiv	masiv	k1gInSc4
Kvam	Kvama	k1gFnPc2
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Nye	Nye	k1gMnSc1
fylker	fylker	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Regjeringen	Regjeringen	k1gInSc1
<g/>
.	.	kIx.
<g/>
no	no	k9
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-12-19	2019-12-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
norsky	norsky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Nye	Nye	k1gFnSc1
kommune-	kommune-	k?
og	og	k?
fylkesnummer	fylkesnummer	k1gInSc1
fra	fra	k?
2020	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
www.regjeringen.no	www.regjeringen.no	k1gNnSc1
-	-	kIx~
web	web	k1gInSc1
norské	norský	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
norsky	norsky	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Norsko	Norsko	k1gNnSc1
–	–	k?
Norge	Norge	k1gFnPc2
<g/>
,	,	kIx,
Noreg	Norega	k1gFnPc2
–	–	k?
(	(	kIx(
<g/>
N	N	kA
<g/>
)	)	kIx)
Kraje	kraj	k1gInPc1
(	(	kIx(
<g/>
fylker	fylker	k1gInSc1
<g/>
)	)	kIx)
<g/>
a	a	k8xC
jejich	jejich	k3xOp3gNnPc1
správní	správní	k2eAgNnPc1d1
centra	centrum	k1gNnPc1
</s>
<s>
Agder	Agder	k1gInSc1
(	(	kIx(
<g/>
Kristiansand	Kristiansand	k1gInSc1
a	a	k8xC
Arendal	Arendal	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Innlandet	Innlandet	k1gMnSc1
(	(	kIx(
<g/>
Hamar	Hamar	k1gMnSc1
a	a	k8xC
Lillehammer	Lillehammer	k1gMnSc1
<g/>
)	)	kIx)
•	•	k?
Mø	Mø	k1gInSc5
a	a	k8xC
Romsdal	Romsdal	k1gFnSc4
(	(	kIx(
<g/>
Molde	Mold	k1gMnSc5
<g/>
)	)	kIx)
•	•	k?
Nordland	Nordland	k1gInSc1
(	(	kIx(
<g/>
Bodø	Bodø	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
hlavní	hlavní	k2eAgNnSc4d1
město	město	k1gNnSc4
Oslo	Oslo	k1gNnSc2
•	•	k?
Rogaland	Rogaland	k1gInSc1
(	(	kIx(
<g/>
Stavanger	Stavanger	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Troms	Troms	k1gInSc1
a	a	k8xC
Finnmark	Finnmark	k1gInSc1
(	(	kIx(
<g/>
Tromsø	Tromsø	k1gFnSc1
a	a	k8xC
Vadsø	Vadsø	k1gFnSc1
<g/>
)	)	kIx)
•	•	k?
Trø	Trø	k1gInSc1
(	(	kIx(
<g/>
Trondheim	Trondheim	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Vestfold	Vestfold	k1gInSc1
a	a	k8xC
Telemark	telemark	k1gInSc1
(	(	kIx(
<g/>
Tø	Tø	k1gInSc1
a	a	k8xC
Skien	Skien	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Vestland	Vestland	k1gInSc1
(	(	kIx(
<g/>
Bergen	Bergen	k1gInSc1
a	a	k8xC
Leikanger	Leikanger	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Viken	Viken	k1gInSc1
(	(	kIx(
<g/>
Oslo	Oslo	k1gNnSc1
<g/>
,	,	kIx,
Drammen	Drammen	k1gInSc1
a	a	k8xC
Sarpsborg	Sarpsborg	k1gInSc1
<g/>
)	)	kIx)
Závislá	závislý	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Špicberky	Špicberky	k1gFnPc1
(	(	kIx(
<g/>
Longyearbyen	Longyearbyen	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Jan	Jan	k1gMnSc1
Mayen	Mayen	k2eAgMnSc1d1
(	(	kIx(
<g/>
Olonkinbyen	Olonkinbyen	k1gInSc1
<g/>
)	)	kIx)
•	•	k?
Bouvetův	Bouvetův	k2eAgInSc4d1
ostrov	ostrov	k1gInSc4
•	•	k?
ostrov	ostrov	k1gInSc1
Petra	Petr	k1gMnSc2
I.	I.	kA
<g/>
*	*	kIx~
•	•	k?
Země	zem	k1gFnSc2
královny	královna	k1gFnSc2
Maud	Maud	k1gInSc1
<g/>
*	*	kIx~
Poznámka	poznámka	k1gFnSc1
<g/>
:	:	kIx,
*	*	kIx~
součást	součást	k1gFnSc1
Antarktidy	Antarktida	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Norsko	Norsko	k1gNnSc1
</s>
