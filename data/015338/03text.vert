<s>
Registrované	registrovaný	k2eAgNnSc1d1
partnerství	partnerství	k1gNnSc1
ve	v	k7c6
Švýcarsku	Švýcarsko	k1gNnSc6
</s>
<s>
Tento	tento	k3xDgInSc1
článek	článek	k1gInSc1
potřebuje	potřebovat	k5eAaImIp3nS
úpravy	úprava	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s>
Můžete	moct	k5eAaImIp2nP
Wikipedii	Wikipedie	k1gFnSc4
pomoci	pomoct	k5eAaPmF
tím	ten	k3xDgNnSc7
<g/>
,	,	kIx,
že	že	k8xS
ho	on	k3xPp3gMnSc4
vylepšíte	vylepšit	k5eAaPmIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
by	by	kYmCp3nP
měly	mít	k5eAaImAgInP
články	článek	k1gInPc1
vypadat	vypadat	k5eAaImF,k5eAaPmF
<g/>
,	,	kIx,
popisují	popisovat	k5eAaImIp3nP
stránky	stránka	k1gFnPc1
Vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
<g/>
,	,	kIx,
Encyklopedický	encyklopedický	k2eAgInSc4d1
styl	styl	k1gInSc4
a	a	k8xC
Odkazy	odkaz	k1gInPc4
<g/>
.	.	kIx.
</s>
<s>
Konkrétní	konkrétní	k2eAgInPc4d1
problémy	problém	k1gInPc4
<g/>
:	:	kIx,
formulace	formulace	k1gFnPc4
<g/>
,	,	kIx,
vzhled	vzhled	k1gInSc4
a	a	k8xC
styl	styl	k1gInSc4
</s>
<s>
Švýcarsko	Švýcarsko	k1gNnSc1
umožnilo	umožnit	k5eAaPmAgNnS
stejnopohlavním	stejnopohlavní	k2eAgInPc3d1
párům	pár	k1gInPc3
uzavírat	uzavírat	k5eAaPmF,k5eAaImF
registrované	registrovaný	k2eAgNnSc4d1
partnerství	partnerství	k1gNnSc4
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2007	#num#	k4
po	po	k7c6
kladném	kladný	k2eAgInSc6d1
výsledku	výsledek	k1gInSc6
referenda	referendum	k1gNnSc2
z	z	k7c2
roku	rok	k1gInSc2
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ústavní	ústavní	k2eAgFnSc1d1
novela	novela	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yIgFnSc1,k3yQgFnSc1,k3yRgFnSc1
by	by	kYmCp3nS
jim	on	k3xPp3gMnPc3
zpřístupnila	zpřístupnit	k5eAaPmAgFnS
institut	institut	k1gInSc4
manželství	manželství	k1gNnSc2
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
momentálně	momentálně	k6eAd1
<g/>
[	[	kIx(
<g/>
kdy	kdy	k6eAd1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
v	v	k7c6
legislativním	legislativní	k2eAgInSc6d1
procesu	proces	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aby	aby	kYmCp3nS
prošla	projít	k5eAaPmAgFnS
<g/>
,	,	kIx,
je	být	k5eAaImIp3nS
zapotřebí	zapotřebí	k6eAd1
většinová	většinový	k2eAgFnSc1d1
podpora	podpora	k1gFnSc1
obyvatelstva	obyvatelstvo	k1gNnSc2
napříč	napříč	k7c7
všemi	všecek	k3xTgInPc7
kantony	kanton	k1gInPc7
v	v	k7c6
referendu	referendum	k1gNnSc6
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
případě	případ	k1gInSc6
změn	změna	k1gFnPc2
ústavy	ústava	k1gFnSc2
obligatorní	obligatorní	k2eAgFnSc2d1
<g/>
.	.	kIx.
</s>
<s>
Registrované	registrovaný	k2eAgNnSc1d1
partnerství	partnerství	k1gNnSc1
</s>
<s>
Stejnopohlavní	Stejnopohlavní	k2eAgNnPc1d1
soužití	soužití	k1gNnPc1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
</s>
<s>
Stejnopohlavní	Stejnopohlavní	k2eAgNnSc1d1
manželství	manželství	k1gNnSc1
</s>
<s>
Registrované	registrovaný	k2eAgNnSc1d1
partnerství	partnerství	k1gNnSc1
</s>
<s>
Neregistrované	registrovaný	k2eNgNnSc4d1
soužití	soužití	k1gNnSc4
</s>
<s>
Neuznáváno	uznáván	k2eNgNnSc1d1
či	či	k8xC
neznámo	znám	k2eNgNnSc1d1
</s>
<s>
Manželství	manželství	k1gNnSc1
striktně	striktně	k6eAd1
vymezeno	vymezit	k5eAaPmNgNnS
jako	jako	k8xC,k8xS
svazek	svazek	k1gInSc1
muže	muž	k1gMnSc2
a	a	k8xC
ženy	žena	k1gFnPc1
</s>
<s>
V	v	k7c6
celonárodním	celonárodní	k2eAgNnSc6d1
referendu	referendum	k1gNnSc6
konaném	konaný	k2eAgNnSc6d1
5	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2005	#num#	k4
podpořilo	podpořit	k5eAaPmAgNnS
58	#num#	k4
%	%	kIx~
Švýcarů	Švýcar	k1gMnPc2
návrh	návrh	k1gInSc4
zákona	zákon	k1gInSc2
o	o	k7c6
registrovaném	registrovaný	k2eAgNnSc6d1
partnerství	partnerství	k1gNnSc6
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
garantuje	garantovat	k5eAaBmIp3nS
stejnopohlavním	stejnopohlavní	k2eAgInPc3d1
párům	pár	k1gInPc3
žijícím	žijící	k2eAgInPc3d1
v	v	k7c6
něm	on	k3xPp3gMnSc6
vesměs	vesměs	k6eAd1
stejná	stejný	k2eAgNnPc1d1
práva	právo	k1gNnPc1
jako	jako	k9
mají	mít	k5eAaImIp3nP
manželé	manžel	k1gMnPc1
-	-	kIx~
status	status	k1gInSc1
blízké	blízký	k2eAgFnSc2d1
osoby	osoba	k1gFnSc2
<g/>
,	,	kIx,
rovné	rovný	k2eAgNnSc4d1
zdanění	zdanění	k1gNnSc4
<g/>
,	,	kIx,
sociální	sociální	k2eAgNnSc4d1
zabezpečení	zabezpečení	k1gNnSc4
<g/>
,	,	kIx,
pojišťovnictví	pojišťovnictví	k1gNnSc4
<g/>
,	,	kIx,
společný	společný	k2eAgInSc4d1
majetek	majetek	k1gInSc4
a	a	k8xC
bydlení	bydlení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zákon	zákon	k1gInSc1
o	o	k7c6
registrovaném	registrovaný	k2eAgNnSc6d1
partnerství	partnerství	k1gNnSc6
momentálně	momentálně	k6eAd1
negarantuje	garantovat	k5eNaBmIp3nS
homosexuálním	homosexuální	k2eAgInPc3d1
párům	pár	k1gInPc3
následující	následující	k2eAgFnSc2d1
<g/>
:	:	kIx,
</s>
<s>
Společné	společný	k2eAgNnSc1d1
osvojení	osvojení	k1gNnSc1
dětí	dítě	k1gFnPc2
</s>
<s>
Přístup	přístup	k1gInSc1
k	k	k7c3
asistované	asistovaný	k2eAgFnSc3d1
reprodukci	reprodukce	k1gFnSc3
</s>
<s>
Obtížnější	obtížný	k2eAgInSc1d2
způsob	způsob	k1gInSc1
získání	získání	k1gNnSc2
trvalého	trvalý	k2eAgInSc2d1
pobytu	pobyt	k1gInSc2
pro	pro	k7c4
cizince	cizinec	k1gMnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Manželé	manžel	k1gMnPc1
švýcarských	švýcarský	k2eAgInPc2d1
občanů	občan	k1gMnPc2
mají	mít	k5eAaImIp3nP
ze	z	k7c2
zákona	zákon	k1gInSc2
právo	právo	k1gNnSc4
na	na	k7c4
rychlé	rychlý	k2eAgNnSc4d1
získání	získání	k1gNnSc4
občanství	občanství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejný	stejný	k2eAgInSc1d1
přístup	přístup	k1gInSc1
není	být	k5eNaImIp3nS
garantovaný	garantovaný	k2eAgInSc1d1
ani	ani	k8xC
u	u	k7c2
stejnopohlavních	stejnopohlavní	k2eAgNnPc2d1
manželství	manželství	k1gNnPc2
uzavřených	uzavřený	k2eAgMnPc2d1
v	v	k7c6
zahraničí	zahraničí	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgNnPc1
jsou	být	k5eAaImIp3nP
ve	v	k7c6
zdejších	zdejší	k2eAgInPc6d1
zákonech	zákon	k1gInPc6
uznávána	uznáván	k2eAgNnPc4d1
jako	jako	k8xC,k8xS
registrovaná	registrovaný	k2eAgNnPc4d1
partnerství	partnerství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s>
Oficiálním	oficiální	k2eAgInSc7d1
názvem	název	k1gInSc7
institucionalizovaných	institucionalizovaný	k2eAgInPc2d1
stejnopohlavních	stejnopohlavní	k2eAgInPc2d1
svazků	svazek	k1gInPc2
je	být	k5eAaImIp3nS
v	v	k7c6
němčině	němčina	k1gFnSc6
„	„	k?
<g/>
Eingetragene	Eingetragen	k1gInSc5
Partnerschaft	Partnerschaft	k2eAgMnSc1d1
<g/>
“	“	k?
<g/>
,	,	kIx,
ve	v	k7c6
francouzštině	francouzština	k1gFnSc6
„	„	k?
<g/>
partenariat	partenariat	k1gInSc4
enregistré	enregistrý	k2eAgFnSc2d1
<g/>
“	“	k?
a	a	k8xC
„	„	k?
<g/>
unione	union	k1gInSc5
domestica	domesticus	k1gMnSc2
registrata	registrat	k2eAgFnSc1d1
<g/>
“	“	k?
v	v	k7c6
italštině	italština	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Návrh	návrh	k1gInSc1
přijala	přijmout	k5eAaPmAgFnS
Národní	národní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
v	v	k7c6
poměru	poměr	k1gInSc6
hlasů	hlas	k1gInPc2
111	#num#	k4
<g/>
:	:	kIx,
<g/>
72	#num#	k4
3	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2003	#num#	k4
a	a	k8xC
Rada	rada	k1gFnSc1
států	stát	k1gInPc2
3	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2004	#num#	k4
s	s	k7c7
drobnými	drobný	k2eAgFnPc7d1
úpravami	úprava	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Národní	národní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
jej	on	k3xPp3gInSc4
znovu	znovu	k6eAd1
přijala	přijmout	k5eAaPmAgFnS
10	#num#	k4
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
června	červen	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
konzervativní	konzervativní	k2eAgFnSc1d1
Federální	federální	k2eAgFnSc1d1
demokratické	demokratický	k2eAgFnSc3d1
unii	unie	k1gFnSc3
se	se	k3xPyFc4
podařilo	podařit	k5eAaPmAgNnS
získat	získat	k5eAaPmF
dostatečný	dostatečný	k2eAgInSc4d1
počet	počet	k1gInSc4
podpisů	podpis	k1gInPc2
pro	pro	k7c4
vypsání	vypsání	k1gNnSc4
celonárodního	celonárodní	k2eAgNnSc2d1
referenda	referendum	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
něm	on	k3xPp3gMnSc6
se	se	k3xPyFc4
dne	den	k1gInSc2
5	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2005	#num#	k4
vyslovilo	vyslovit	k5eAaPmAgNnS
58	#num#	k4
%	%	kIx~
občanů	občan	k1gMnPc2
pro	pro	k7c4
přijetí	přijetí	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Nový	nový	k2eAgInSc1d1
zákon	zákon	k1gInSc1
nabyl	nabýt	k5eAaPmAgInS
účinnost	účinnost	k1gFnSc4
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
lednu	leden	k1gInSc3
2007	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
Švýcarsko	Švýcarsko	k1gNnSc1
se	se	k3xPyFc4
tak	tak	k6eAd1
stalo	stát	k5eAaPmAgNnS
první	první	k4xOgNnSc1
zemí	zem	k1gFnPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	s	k7c7
zákonem	zákon	k1gInSc7
uznávané	uznávaný	k2eAgNnSc1d1
homosexuální	homosexuální	k2eAgInPc1d1
svazky	svazek	k1gInPc1
prosadily	prosadit	k5eAaPmAgInP
prostřednictvím	prostřednictvím	k7c2
plebiscitu	plebiscit	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Heterosexuální	heterosexuální	k2eAgInPc1d1
páry	pár	k1gInPc1
</s>
<s>
Jak	jak	k8xC,k8xS
volily	volit	k5eAaImAgInP
různé	různý	k2eAgInPc1d1
švýcarské	švýcarský	k2eAgInPc1d1
volební	volební	k2eAgInPc1d1
obvody	obvod	k1gInPc1
v	v	k7c6
referendu	referendum	k1gNnSc6
o	o	k7c6
registrovaném	registrovaný	k2eAgNnSc6d1
partnerství	partnerství	k1gNnSc6
<g/>
,	,	kIx,
2005	#num#	k4
</s>
<s>
>	>	kIx)
60	#num#	k4
%	%	kIx~
</s>
<s>
50	#num#	k4
%	%	kIx~
–	–	k?
59	#num#	k4
%	%	kIx~
</s>
<s>
<	<	kIx(
49	#num#	k4
%	%	kIx~
</s>
<s>
15	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2016	#num#	k4
přijala	přijmout	k5eAaPmAgFnS
Národní	národní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
dvě	dva	k4xCgFnPc4
novely	novela	k1gFnPc4
zákona	zákon	k1gInSc2
o	o	k7c6
registrovaném	registrovaný	k2eAgNnSc6d1
partnerství	partnerství	k1gNnSc6
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gInSc7
účelem	účel	k1gInSc7
zpřístupnění	zpřístupnění	k1gNnSc2
institutu	institut	k1gInSc2
pro	pro	k7c4
heterosexuální	heterosexuální	k2eAgInPc4d1
páry	pár	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
Prošly	projít	k5eAaPmAgInP
v	v	k7c6
poměru	poměr	k1gInSc6
hlasů	hlas	k1gInPc2
96	#num#	k4
<g/>
:	:	kIx,
<g/>
83	#num#	k4
a	a	k8xC
96	#num#	k4
<g/>
:	:	kIx,
<g/>
82	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Adopce	adopce	k1gFnSc1
a	a	k8xC
rodičovství	rodičovství	k1gNnSc1
</s>
<s>
Homosexuální	homosexuální	k2eAgMnSc1d1
jednotlivec	jednotlivec	k1gMnSc1
může	moct	k5eAaImIp3nS
podle	podle	k7c2
švýcarských	švýcarský	k2eAgInPc2d1
zákonů	zákon	k1gInPc2
se	se	k3xPyFc4
stát	stát	k1gInSc1
osvojitelem	osvojitel	k1gMnSc7
dítěte	dítě	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Návrh	návrh	k1gInSc1
zákona	zákon	k1gInSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
by	by	kYmCp3nS
umožnil	umožnit	k5eAaPmAgInS
přiosvojení	přiosvojení	k1gNnSc4
dítěte	dítě	k1gNnSc2
druhého	druhý	k4xOgMnSc2
z	z	k7c2
partnerů	partner	k1gMnPc2
přijal	přijmout	k5eAaPmAgMnS
spolkový	spolkový	k2eAgInSc4d1
parlament	parlament	k1gInSc4
na	na	k7c6
jaře	jaro	k1gNnSc6
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnPc1
odpůrci	odpůrce	k1gMnPc1
se	se	k3xPyFc4
neúspěšně	úspěšně	k6eNd1
pokusili	pokusit	k5eAaPmAgMnP
o	o	k7c4
vypsání	vypsání	k1gNnSc4
referenda	referendum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tento	tento	k3xDgInSc1
nový	nový	k2eAgInSc1d1
zákon	zákon	k1gInSc1
se	se	k3xPyFc4
stane	stanout	k5eAaPmIp3nS
účinným	účinný	k2eAgInSc7d1
od	od	k7c2
data	datum	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
ještě	ještě	k9
bude	být	k5eAaImBp3nS
upřesněno	upřesnit	k5eAaPmNgNnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Článek	článek	k1gInSc1
27	#num#	k4
zákona	zákon	k1gInSc2
o	o	k7c6
registrovaném	registrovaný	k2eAgNnSc6d1
partnerství	partnerství	k1gNnSc6
upravuje	upravovat	k5eAaImIp3nS
vztah	vztah	k1gInSc1
mezi	mezi	k7c7
dětmi	dítě	k1gFnPc7
a	a	k8xC
partnery	partner	k1gMnPc7
rodičů	rodič	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
zákona	zákon	k1gInSc2
je	být	k5eAaImIp3nS
partner	partner	k1gMnSc1
rodiče	rodič	k1gMnSc2
povinen	povinen	k2eAgMnSc1d1
přispívat	přispívat	k5eAaImF
na	na	k7c4
výživu	výživa	k1gFnSc4
jeho	jeho	k3xOp3gFnPc2
dětí	dítě	k1gFnPc2
<g/>
,	,	kIx,
biologických	biologický	k2eAgInPc2d1
i	i	k8xC
osvojených	osvojený	k2eAgInPc2d1
<g/>
,	,	kIx,
a	a	k8xC
zároveň	zároveň	k6eAd1
oprávněn	oprávnit	k5eAaPmNgInS
jej	on	k3xPp3gMnSc4
zastupovat	zastupovat	k5eAaImF
v	v	k7c6
běžných	běžný	k2eAgFnPc6d1
věcech	věc	k1gFnPc6
jako	jako	k8xS,k8xC
by	by	kYmCp3nS
byl	být	k5eAaImAgInS
jeho	jeho	k3xOp3gMnSc7
právoplatným	právoplatný	k2eAgMnSc7d1
zákonným	zákonný	k2eAgMnSc7d1
zástupcem	zástupce	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Partner	partner	k1gMnSc1
má	mít	k5eAaImIp3nS
rovněž	rovněž	k9
v	v	k7c6
případě	případ	k1gInSc6
rozvodu	rozvod	k1gInSc2
partnerství	partnerství	k1gNnSc2
právo	právo	k1gNnSc1
na	na	k7c4
styk	styk	k1gInSc4
s	s	k7c7
dítětem	dítě	k1gNnSc7
svého	svůj	k3xOyFgMnSc2
bývalého	bývalý	k2eAgMnSc2d1
partnera	partner	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
Díky	díky	k7c3
tomuto	tento	k3xDgNnSc3
ustanovení	ustanovení	k1gNnSc3
je	být	k5eAaImIp3nS
švýcarský	švýcarský	k2eAgInSc4d1
institut	institut	k1gInSc4
registrovaného	registrovaný	k2eAgNnSc2d1
partnerství	partnerství	k1gNnSc2
považovaný	považovaný	k2eAgInSc1d1
za	za	k7c4
jeden	jeden	k4xCgInSc4
z	z	k7c2
nejliberálnějších	liberální	k2eAgInPc2d3
<g/>
,	,	kIx,
neboť	neboť	k8xC
samotné	samotný	k2eAgNnSc1d1
partnerství	partnerství	k1gNnSc1
dává	dávat	k5eAaImIp3nS
partnerům	partner	k1gMnPc3
rodičů	rodič	k1gMnPc2
už	už	k6eAd1
faktickou	faktický	k2eAgFnSc4d1
roli	role	k1gFnSc4
právoplatného	právoplatný	k2eAgMnSc2d1
rodiče	rodič	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
r.	r.	kA
2010	#num#	k4
vypsala	vypsat	k5eAaPmAgFnS
švýcarská	švýcarský	k2eAgFnSc1d1
LGBT	LGBT	kA
organizace	organizace	k1gFnSc1
petici	petice	k1gFnSc3
s	s	k7c7
názvem	název	k1gInSc7
"	"	kIx"
<g/>
Rovné	rovný	k2eAgFnPc4d1
příležitosti	příležitost	k1gFnPc4
pro	pro	k7c4
všechny	všechen	k3xTgFnPc4
rodiny	rodina	k1gFnPc4
<g/>
"	"	kIx"
s	s	k7c7
požadavkem	požadavek	k1gInSc7
na	na	k7c4
rozšíření	rozšíření	k1gNnSc4
adopčních	adopční	k2eAgNnPc2d1
práv	právo	k1gNnPc2
<g/>
.	.	kIx.
30	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2011	#num#	k4
se	se	k3xPyFc4
dolní	dolní	k2eAgFnSc1d1
komora	komora	k1gFnSc1
federálního	federální	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
rozhodla	rozhodnout	k5eAaPmAgFnS
v	v	k7c6
poměru	poměr	k1gInSc6
hlasů	hlas	k1gInPc2
83	#num#	k4
<g/>
:	:	kIx,
<g/>
97	#num#	k4
požadavku	požadavek	k1gInSc2
petice	petice	k1gFnSc2
nevyhovět	vyhovět	k5eNaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
Nicméně	nicméně	k8xC
právě	právě	k9
díky	díky	k7c3
petici	petice	k1gFnSc3
se	se	k3xPyFc4
rozbouřila	rozbouřit	k5eAaPmAgFnS
široká	široký	k2eAgFnSc1d1
diskuse	diskuse	k1gFnSc1
na	na	k7c4
toto	tento	k3xDgNnSc4
téma	téma	k1gNnSc4
v	v	k7c6
řadách	řada	k1gFnPc6
poslanců	poslanec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
poslankyně	poslankyně	k1gFnSc1
Maja	Maja	k1gFnSc1
Ingoldová	Ingoldová	k1gFnSc1
za	za	k7c4
Švýcarskou	švýcarský	k2eAgFnSc4d1
evangelickou	evangelický	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
se	se	k3xPyFc4
vyjádřila	vyjádřit	k5eAaPmAgNnP
pro	pro	k7c4
liberálnější	liberální	k2eAgInSc4d2
přístup	přístup	k1gInSc4
ke	k	k7c3
gay	gay	k1gMnSc1
a	a	k8xC
lesbickému	lesbický	k2eAgNnSc3d1
rodičovství	rodičovství	k1gNnSc3
navzdory	navzdory	k7c3
faktu	fakt	k1gInSc3
<g/>
,	,	kIx,
že	že	k8xS
právě	právě	k9
její	její	k3xOp3gFnSc1
strana	strana	k1gFnSc1
byla	být	k5eAaImAgFnS
jedním	jeden	k4xCgInSc7
z	z	k7c2
odpůrců	odpůrce	k1gMnPc2
zákona	zákon	k1gInSc2
o	o	k7c6
registrovaném	registrovaný	k2eAgNnSc6d1
partnerství	partnerství	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bylo	být	k5eAaImAgNnS
však	však	k9
zřejmé	zřejmý	k2eAgNnSc1d1
<g/>
,	,	kIx,
že	že	k8xS
parlament	parlament	k1gInSc1
zaujímá	zaujímat	k5eAaImIp3nS
diametrálně	diametrálně	k6eAd1
odlišný	odlišný	k2eAgInSc1d1
přístup	přístup	k1gInSc1
k	k	k7c3
té	ten	k3xDgFnSc3
měkčí	měkký	k2eAgFnSc3d2
verzi	verze	k1gFnSc3
adopcí	adopce	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zatímco	zatímco	k8xS
u	u	k7c2
společného	společný	k2eAgNnSc2d1
osvojení	osvojení	k1gNnSc2
byla	být	k5eAaImAgFnS
podpora	podpora	k1gFnSc1
menšinová	menšinový	k2eAgFnSc1d1
<g/>
,	,	kIx,
tak	tak	k8xC,k8xS
při	při	k7c6
osvojení	osvojení	k1gNnSc6
dítěte	dítě	k1gNnSc2
partnera	partner	k1gMnSc2
mělo	mít	k5eAaImAgNnS
mnohem	mnohem	k6eAd1
větší	veliký	k2eAgFnSc4d2
podporu	podpora	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Rada	rada	k1gFnSc1
kantonů	kanton	k1gInPc2
<g/>
,	,	kIx,
horní	horní	k2eAgFnSc1d1
komora	komora	k1gFnSc1
federálního	federální	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
<g/>
,	,	kIx,
petici	petice	k1gFnSc4
přijala	přijmout	k5eAaPmAgFnS
a	a	k8xC
její	její	k3xOp3gFnPc1
komise	komise	k1gFnPc1
pro	pro	k7c4
právní	právní	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
přijala	přijmout	k5eAaPmAgFnS
návrh	návrh	k1gInSc4
otevřeně	otevřeně	k6eAd1
homosexuálního	homosexuální	k2eAgMnSc2d1
poslance	poslanec	k1gMnSc2
Claude	Claud	k1gInSc5
Janiaka	Janiak	k1gMnSc2
za	za	k7c4
Švýcarskou	švýcarský	k2eAgFnSc4d1
sociálně	sociálně	k6eAd1
demokratickou	demokratický	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
<g/>
,	,	kIx,
ze	z	k7c2
kterého	který	k3yRgInSc2,k3yQgInSc2,k3yIgInSc2
se	se	k3xPyFc4
odstranila	odstranit	k5eAaPmAgFnS
možnost	možnost	k1gFnSc1
společného	společný	k2eAgNnSc2d1
osvojování	osvojování	k1gNnSc2
dětí	dítě	k1gFnPc2
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
rodinný	rodinný	k2eAgInSc4d1
stav	stav	k1gInSc4
nebo	nebo	k8xC
sexuální	sexuální	k2eAgFnSc4d1
orientaci	orientace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
listopadu	listopad	k1gInSc6
2011	#num#	k4
komise	komise	k1gFnSc1
podpořila	podpořit	k5eAaPmAgFnS
jednomyslně	jednomyslně	k6eAd1
návrh	návrh	k1gInSc4
spolu	spolu	k6eAd1
s	s	k7c7
konzervativní	konzervativní	k2eAgFnSc7d1
Švýcarskou	švýcarský	k2eAgFnSc7d1
lidovou	lidový	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
únoru	únor	k1gInSc6
2012	#num#	k4
se	se	k3xPyFc4
federální	federální	k2eAgFnSc1d1
rada	rada	k1gFnSc1
<g/>
,	,	kIx,
orgán	orgán	k1gInSc1
výkonné	výkonný	k2eAgFnSc2d1
moci	moc	k1gFnSc2
<g/>
,	,	kIx,
vyjádřila	vyjádřit	k5eAaPmAgFnS
k	k	k7c3
postoji	postoj	k1gInSc3
Rady	rada	k1gFnSc2
kantonů	kanton	k1gInPc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
je	být	k5eAaImIp3nS
plně	plně	k6eAd1
pro	pro	k7c4
přiosvojení	přiosvojení	k1gNnSc4
dítěte	dítě	k1gNnSc2
partnera	partner	k1gMnSc2
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
ale	ale	k8xC
proti	proti	k7c3
plným	plný	k2eAgNnPc3d1
adopčním	adopční	k2eAgNnPc3d1
právům	právo	k1gNnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
14	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2012	#num#	k4
přijala	přijmout	k5eAaPmAgFnS
Rada	rada	k1gFnSc1
kantonů	kanton	k1gInPc2
v	v	k7c6
poměru	poměr	k1gInSc6
hlasů	hlas	k1gInPc2
21	#num#	k4
<g/>
:	:	kIx,
<g/>
19	#num#	k4
kompletní	kompletní	k2eAgFnSc1d1
plné	plný	k2eAgNnSc4d1
rozšíření	rozšíření	k1gNnSc4
adopčních	adopční	k2eAgNnPc2d1
práv	právo	k1gNnPc2
pro	pro	k7c4
stejnopohlavní	stejnopohlavní	k2eAgInPc4d1
páry	pár	k1gInPc4
bez	bez	k7c2
ohledu	ohled	k1gInSc2
na	na	k7c4
rodinný	rodinný	k2eAgInSc4d1
stav	stav	k1gInSc4
nebo	nebo	k8xC
sexuální	sexuální	k2eAgFnSc4d1
orientaci	orientace	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jelikož	jelikož	k8xS
toto	tento	k3xDgNnSc1
Národní	národní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
během	během	k7c2
debaty	debata	k1gFnSc2
v	v	k7c6
září	září	k1gNnSc6
2011	#num#	k4
odmítala	odmítat	k5eAaImAgFnS
<g/>
,	,	kIx,
tak	tak	k8xS,k8xC
se	se	k3xPyFc4
v	v	k7c6
ní	on	k3xPp3gFnSc6
o	o	k7c6
něm	on	k3xPp3gMnSc6
hlasovalo	hlasovat	k5eAaImAgNnS
ještě	ještě	k6eAd1
jednou	jednou	k6eAd1
<g/>
.	.	kIx.
13	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2012	#num#	k4
přijala	přijmout	k5eAaPmAgFnS
Národní	národní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
v	v	k7c6
poměru	poměr	k1gInSc6
hlasů	hlas	k1gInPc2
113	#num#	k4
<g/>
:	:	kIx,
<g/>
64	#num#	k4
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
možnost	možnost	k1gFnSc1
osvojení	osvojení	k1gNnSc2
dítěte	dítě	k1gNnSc2
partnera	partner	k1gMnSc2
<g/>
,	,	kIx,
jak	jak	k6eAd1
biologického	biologický	k2eAgInSc2d1
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
osvojeného	osvojený	k2eAgInSc2d1
před	před	k7c7
vstupem	vstup	k1gInSc7
do	do	k7c2
partnerství	partnerství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Národní	národní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
tedy	tedy	k9
podstatně	podstatně	k6eAd1
okleštila	okleštit	k5eAaPmAgFnS
verzi	verze	k1gFnSc4
přijatou	přijatý	k2eAgFnSc7d1
Radou	rada	k1gFnSc7
kantonů	kanton	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
4	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2013	#num#	k4
se	se	k3xPyFc4
rozhodla	rozhodnout	k5eAaPmAgFnS
verzi	verze	k1gFnSc4
Národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
ze	z	k7c2
13	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2012	#num#	k4
podpořit	podpořit	k5eAaPmF
i	i	k9
Rada	rada	k1gFnSc1
kantonů	kanton	k1gInPc2
(	(	kIx(
<g/>
poměr	poměr	k1gInSc1
hlasů	hlas	k1gInPc2
26	#num#	k4
<g/>
:	:	kIx,
<g/>
16	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
listopadu	listopad	k1gInSc6
2014	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
rámci	rámec	k1gInSc6
nadcházejících	nadcházející	k2eAgFnPc2d1
parlamentních	parlamentní	k2eAgFnPc2d1
voleb	volba	k1gFnPc2
rozhodla	rozhodnout	k5eAaPmAgFnS
federální	federální	k2eAgFnSc1d1
rada	rada	k1gFnSc1
přijmout	přijmout	k5eAaPmF
možnost	možnost	k1gFnSc4
adopce	adopce	k1gFnSc2
dítěte	dítě	k1gNnSc2
partnera	partner	k1gMnSc2
jakožto	jakožto	k8xS
součást	součást	k1gFnSc4
rozsáhlé	rozsáhlý	k2eAgFnSc2d1
adopční	adopční	k2eAgFnSc2d1
reformy	reforma	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Návrh	návrh	k1gInSc1
musí	muset	k5eAaImIp3nS
být	být	k5eAaImF
momentálně	momentálně	k6eAd1
odsouhlasen	odsouhlasen	k2eAgInSc1d1
i	i	k9
v	v	k7c6
parlamentu	parlament	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnPc4
odpůrci	odpůrce	k1gMnPc1
usilují	usilovat	k5eAaImIp3nP
o	o	k7c4
vypsání	vypsání	k1gNnSc4
referenda	referendum	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Pro	pro	k7c4
jeho	jeho	k3xOp3gNnSc4
uskutečnění	uskutečnění	k1gNnSc4
se	se	k3xPyFc4
musí	muset	k5eAaImIp3nS
podařit	podařit	k5eAaPmF
ve	v	k7c6
lhůtě	lhůta	k1gFnSc6
100	#num#	k4
dní	den	k1gInPc2
získat	získat	k5eAaPmF
50	#num#	k4
000	#num#	k4
podpisů	podpis	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
lednu	leden	k1gInSc6
2016	#num#	k4
hlasovala	hlasovat	k5eAaImAgFnS
v	v	k7c6
poměru	poměr	k1gInSc6
hlasů	hlas	k1gInPc2
7	#num#	k4
<g/>
:	:	kIx,
<g/>
3	#num#	k4
s	s	k7c7
jedním	jeden	k4xCgNnSc7
zdržením	zdržení	k1gNnSc7
komise	komise	k1gFnSc2
pro	pro	k7c4
právní	právní	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
při	při	k7c6
Radě	rada	k1gFnSc6
kantonů	kanton	k1gInPc2
pro	pro	k7c4
přijetí	přijetí	k1gNnSc4
návrhu	návrh	k1gInSc2
o	o	k7c6
adopcích	adopce	k1gFnPc6
dítěte	dítě	k1gNnSc2
partnera	partner	k1gMnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
8	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2016	#num#	k4
přijala	přijmout	k5eAaPmAgFnS
Rada	rada	k1gFnSc1
kantonů	kanton	k1gInPc2
návrh	návrh	k1gInSc4
v	v	k7c6
poměru	poměr	k1gInSc6
25	#num#	k4
<g/>
:	:	kIx,
<g/>
14	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	nový	k2eAgInSc1d1
zákon	zákon	k1gInSc1
se	se	k3xPyFc4
týká	týkat	k5eAaImIp3nS
i	i	k9
nesezdaných	sezdaný	k2eNgInPc2d1
párů	pár	k1gInPc2
<g/>
,	,	kIx,
homosexuálních	homosexuální	k2eAgInPc2d1
i	i	k8xC
heterosexuálních	heterosexuální	k2eAgInPc2d1
<g/>
,	,	kIx,
a	a	k8xC
snižuje	snižovat	k5eAaImIp3nS
věk	věk	k1gInSc4
způsobilosti	způsobilost	k1gFnSc2
k	k	k7c3
osvojení	osvojení	k1gNnSc3
z	z	k7c2
35	#num#	k4
na	na	k7c4
28	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
Bývalá	bývalý	k2eAgFnSc1d1
švýcarská	švýcarský	k2eAgFnSc1d1
prezidentka	prezidentka	k1gFnSc1
Simonetta	Simonetta	k1gFnSc1
Sommarugaová	Sommarugaová	k1gFnSc1
návrh	návrh	k1gInSc4
podporovala	podporovat	k5eAaImAgFnS
říkala	říkat	k5eAaImAgFnS
<g/>
,	,	kIx,
že	že	k8xS
je	být	k5eAaImIp3nS
<g />
.	.	kIx.
</s>
<s hack="1">
nezbytně	nezbytně	k6eAd1,k6eNd1
nutný	nutný	k2eAgInSc1d1
v	v	k7c6
zájmu	zájem	k1gInSc6
ochrany	ochrana	k1gFnSc2
dětí	dítě	k1gFnPc2
vyrůstajících	vyrůstající	k2eAgFnPc2d1
v	v	k7c6
homosexuálních	homosexuální	k2eAgInPc6d1
párech	pár	k1gInPc6
<g/>
.	.	kIx.
13	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2016	#num#	k4
přijala	přijmout	k5eAaPmAgFnS
návrh	návrh	k1gInSc4
komise	komise	k1gFnSc2
pro	pro	k7c4
právní	právní	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
při	při	k7c6
Národní	národní	k2eAgFnSc6d1
radě	rada	k1gFnSc6
v	v	k7c6
poměru	poměr	k1gInSc6
hlasů	hlas	k1gInPc2
15	#num#	k4
<g/>
:	:	kIx,
<g/>
9	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
14	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2016	#num#	k4
byl	být	k5eAaImAgInS
návrh	návrh	k1gInSc1
přijat	přijat	k2eAgInSc1d1
<g />
.	.	kIx.
</s>
<s hack="1">
Národní	národní	k2eAgFnSc7d1
radou	rada	k1gFnSc7
v	v	k7c6
poměru	poměr	k1gInSc6
hlasů	hlas	k1gInPc2
113	#num#	k4
<g/>
:	:	kIx,
<g/>
64	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
Různé	různý	k2eAgInPc1d1
texty	text	k1gInPc1
zpracované	zpracovaný	k2eAgInPc1d1
dvěma	dva	k4xCgFnPc7
komorami	komora	k1gFnPc7
způsobili	způsobit	k5eAaPmAgMnP
přepracování	přepracování	k1gNnSc4
návrhu	návrh	k1gInSc2
na	na	k7c6
finální	finální	k2eAgFnSc6d1
verzi	verze	k1gFnSc6
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc3
pak	pak	k9
schválil	schválit	k5eAaPmAgInS
parlament	parlament	k1gInSc1
17	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2016	#num#	k4
poměrem	poměr	k1gInSc7
hlasů	hlas	k1gInPc2
125.68	125.68	k4
se	s	k7c7
3	#num#	k4
zdrženími	zdržení	k1gNnPc7
se	se	k3xPyFc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
švýcarských	švýcarský	k2eAgInPc2d1
zákonů	zákon	k1gInPc2
mají	mít	k5eAaImIp3nP
odpůrci	odpůrce	k1gMnPc7
návrhu	návrh	k1gInSc2
100	#num#	k4
dní	den	k1gInPc2
na	na	k7c4
získání	získání	k1gNnSc4
50	#num#	k4
000	#num#	k4
podpisů	podpis	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
takovém	takový	k3xDgInSc6
případě	případ	k1gInSc6
by	by	kYmCp3nS
se	se	k3xPyFc4
v	v	k7c6
průběhu	průběh	k1gInSc6
legislativní	legislativní	k2eAgFnSc2d1
lhůty	lhůta	k1gFnSc2
muselo	muset	k5eAaImAgNnS
vypsat	vypsat	k5eAaPmF
referendum	referendum	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
finálním	finální	k2eAgNnSc7d1
hlasováním	hlasování	k1gNnSc7
založila	založit	k5eAaPmAgFnS
komise	komise	k1gFnSc1
pro	pro	k7c4
referenda	referendum	k1gNnPc4
komisi	komise	k1gFnSc4
složenou	složený	k2eAgFnSc4d1
ze	z	k7c2
členů	člen	k1gInPc2
několika	několik	k4yIc2
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
s	s	k7c7
cílem	cíl	k1gInSc7
vypořádání	vypořádání	k1gNnSc2
se	se	k3xPyFc4
s	s	k7c7
otázkou	otázka	k1gFnSc7
referenda	referendum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Žádná	žádný	k3yNgFnSc1
většinová	většinový	k2eAgFnSc1d1
politická	politický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
ji	on	k3xPp3gFnSc4
nepodpořila	podpořit	k5eNaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
4	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2016	#num#	k4
bylo	být	k5eAaImAgNnS
potvrzeno	potvrdit	k5eAaPmNgNnS
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
žádné	žádný	k3yNgNnSc1
referendum	referendum	k1gNnSc1
konat	konat	k5eAaImF
nebude	být	k5eNaImBp3nS
<g/>
,	,	kIx,
neboť	neboť	k8xC
se	se	k3xPyFc4
odpůrcům	odpůrce	k1gMnPc3
podařilo	podařit	k5eAaPmAgNnS
do	do	k7c2
100	#num#	k4
dní	den	k1gInPc2
získat	získat	k5eAaPmF
pouze	pouze	k6eAd1
20	#num#	k4
000	#num#	k4
podpisů	podpis	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
Federální	federální	k2eAgFnSc1d1
rada	rada	k1gFnSc1
se	se	k3xPyFc4
momentálně	momentálně	k6eAd1
rozhoduje	rozhodovat	k5eAaImIp3nS
o	o	k7c6
datu	datum	k1gNnSc6
<g/>
,	,	kIx,
ke	k	k7c3
kterému	který	k3yRgInSc3,k3yQgInSc3,k3yIgInSc3
se	se	k3xPyFc4
nový	nový	k2eAgInSc1d1
zákon	zákon	k1gInSc1
stane	stanout	k5eAaPmIp3nS
účinným	účinný	k2eAgInSc7d1
<g/>
.	.	kIx.
</s>
<s>
Snadnější	snadný	k2eAgNnSc1d2
získávání	získávání	k1gNnSc1
občanství	občanství	k1gNnSc2
</s>
<s>
14	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2016	#num#	k4
přijala	přijmout	k5eAaPmAgFnS
Národní	národní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
návrh	návrh	k1gInSc1
zákona	zákon	k1gInSc2
usnadňující	usnadňující	k2eAgFnSc2d1
párům	pár	k1gInPc3
žijícím	žijící	k2eAgInPc3d1
v	v	k7c6
registrovaném	registrovaný	k2eAgNnSc6d1
partnerství	partnerství	k1gNnSc6
snadnější	snadný	k2eAgNnSc4d2
získávání	získávání	k1gNnSc4
švýcarského	švýcarský	k2eAgNnSc2d1
občanství	občanství	k1gNnSc2
(	(	kIx(
<g/>
Švýcarsko	Švýcarsko	k1gNnSc1
má	mít	k5eAaImIp3nS
poměrně	poměrně	k6eAd1
liberální	liberální	k2eAgInPc4d1
naturalizační	naturalizační	k2eAgInPc4d1
zákony	zákon	k1gInPc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c2
současné	současný	k2eAgFnSc2d1
právní	právní	k2eAgFnSc2d1
úpravy	úprava	k1gFnSc2
má	mít	k5eAaImIp3nS
zahraniční	zahraniční	k2eAgMnSc1d1
manžel	manžel	k1gMnSc1
švýcarského	švýcarský	k2eAgMnSc2d1
občana	občan	k1gMnSc2
nárok	nárok	k1gInSc4
na	na	k7c4
získání	získání	k1gNnSc4
občanství	občanství	k1gNnSc2
buď	buď	k8xC
po	po	k7c6
pěti	pět	k4xCc6
letech	léto	k1gNnPc6
trvalého	trvalý	k2eAgInSc2d1
pobytu	pobyt	k1gInSc2
na	na	k7c6
území	území	k1gNnSc6
Švýcarska	Švýcarsko	k1gNnSc2
<g/>
,	,	kIx,
nebo	nebo	k8xC
po	po	k7c6
třech	tři	k4xCgNnPc6
letech	léto	k1gNnPc6
trvajícího	trvající	k2eAgNnSc2d1
manželství	manželství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
registrované	registrovaný	k2eAgMnPc4d1
partnery	partner	k1gMnPc4
se	se	k3xPyFc4
tato	tento	k3xDgNnPc1
ustanovení	ustanovení	k1gNnPc1
nevztahují	vztahovat	k5eNaImIp3nP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
Návrh	návrh	k1gInSc1
byl	být	k5eAaImAgInS
přijat	přijmout	k5eAaPmNgInS
v	v	k7c6
poměru	poměr	k1gInSc6
hlasů	hlas	k1gInPc2
122	#num#	k4
<g/>
:	:	kIx,
<g/>
62	#num#	k4
<g/>
.	.	kIx.
26	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2016	#num#	k4
rozhodla	rozhodnout	k5eAaPmAgFnS
Rada	rada	k1gFnSc1
kantonů	kanton	k1gInPc2
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nS
se	se	k3xPyFc4
o	o	k7c6
této	tento	k3xDgFnSc6
záležitosti	záležitost	k1gFnSc6
mělo	mít	k5eAaImAgNnS
rozhodnout	rozhodnout	k5eAaPmF
v	v	k7c6
rámci	rámec	k1gInSc6
hlasování	hlasování	k1gNnSc2
o	o	k7c6
stejnopohlavním	stejnopohlavní	k2eAgNnSc6d1
manželství	manželství	k1gNnSc6
<g/>
,	,	kIx,
k	k	k7c3
němuž	jenž	k3xRgNnSc3
byl	být	k5eAaImAgInS
dán	dát	k5eAaPmNgInS
podnět	podnět	k1gInSc1
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
(	(	kIx(
<g/>
viz	vidět	k5eAaImRp2nS
níže	nízce	k6eAd2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Penzijní	penzijní	k2eAgInPc1d1
benefity	benefit	k1gInPc1
</s>
<s>
Koncem	koncem	k7c2
srpna	srpen	k1gInSc2
2008	#num#	k4
rozhodl	rozhodnout	k5eAaPmAgInS
Federální	federální	k2eAgInSc1d1
soud	soud	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
by	by	kYmCp3nP
páry	pára	k1gFnPc1
stejného	stejný	k2eAgNnSc2d1
pohlaví	pohlaví	k1gNnSc2
žijící	žijící	k2eAgFnPc1d1
v	v	k7c6
dlouhodobém	dlouhodobý	k2eAgInSc6d1
svazku	svazek	k1gInSc6
mají	mít	k5eAaImIp3nP
nárok	nárok	k1gInSc4
na	na	k7c4
stejné	stejný	k2eAgFnPc4d1
podmínky	podmínka	k1gFnPc4
v	v	k7c6
oblasti	oblast	k1gFnSc6
sociálního	sociální	k2eAgNnSc2d1
zabezpečení	zabezpečení	k1gNnSc2
<g/>
,	,	kIx,
jako	jako	k8xC,k8xS
heterosexuální	heterosexuální	k2eAgInPc1d1
páry	pár	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zda	zda	k8xS
žijí	žít	k5eAaImIp3nP
<g/>
,	,	kIx,
či	či	k8xC
nežijí	žít	k5eNaImIp3nP
<g/>
,	,	kIx,
ve	v	k7c6
společné	společný	k2eAgFnSc6d1
domácnosti	domácnost	k1gFnSc6
zde	zde	k6eAd1
nehraje	hrát	k5eNaImIp3nS
roli	role	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s>
Švýcarské	švýcarský	k2eAgFnPc1d1
reformované	reformovaný	k2eAgFnPc1d1
církve	církev	k1gFnPc1
</s>
<s>
Některé	některý	k3yIgFnPc1
švýcarské	švýcarský	k2eAgFnPc1d1
reformované	reformovaný	k2eAgFnPc1d1
církve	církev	k1gFnPc1
žehnají	žehnat	k5eAaImIp3nP
homosexuálním	homosexuální	k2eAgNnSc7d1
registrovaným	registrovaný	k2eAgNnSc7d1
partnerstvím	partnerství	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c7
nimi	on	k3xPp3gFnPc7
jsou	být	k5eAaImIp3nP
například	například	k6eAd1
Reformovaná	reformovaný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
Aargau	Aargaus	k1gInSc2
<g/>
,	,	kIx,
Reformované	reformovaný	k2eAgFnPc1d1
církve	církev	k1gFnPc1
Kantonu	Kanton	k1gInSc2
Bern-Jura-Solothurn	Bern-Jura-Solothurn	k1gInSc1
<g/>
,	,	kIx,
Evangelická	evangelický	k2eAgFnSc1d1
reformovaná	reformovaný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
Graubünden	Graubündna	k1gFnPc2
<g/>
,	,	kIx,
Evangelická	evangelický	k2eAgFnSc1d1
reformovaná	reformovaný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
Kantonu	Kanton	k1gInSc2
Luzern	Luzerna	k1gFnPc2
<g/>
,	,	kIx,
Evangelická	evangelický	k2eAgFnSc1d1
reformovaná	reformovaný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
Kantonu	Kanton	k1gInSc2
Sv.	sv.	kA
Gallena	Gallena	k1gFnSc1
<g/>
,	,	kIx,
Evangelická	evangelický	k2eAgFnSc1d1
reformovaná	reformovaný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
Kantonu	Kanton	k1gInSc2
Schaffhausen	Schaffhausna	k1gFnPc2
<g/>
,	,	kIx,
Evangelická	evangelický	k2eAgFnSc1d1
reformovaná	reformovaný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
Ticina	Ticin	k2eAgFnSc1d1
<g/>
,	,	kIx,
Evangelická	evangelický	k2eAgFnSc1d1
reformovaná	reformovaný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
Kantonu	Kanton	k1gInSc2
Thurgau	Thurgaus	k1gInSc2
<g/>
,	,	kIx,
Evangelická	evangelický	k2eAgFnSc1d1
reformovaná	reformovaný	k2eAgFnSc1d1
církev	církev	k1gFnSc1
Kantonu	Kanton	k1gInSc2
Vaud	Vauda	k1gFnPc2
a	a	k8xC
Evangelická	evangelický	k2eAgFnSc1d1
církev	církev	k1gFnSc1
Kantonu	Kanton	k1gInSc2
Curych	Curych	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zbývající	zbývající	k2eAgFnSc2d1
reformované	reformovaný	k2eAgFnSc2d1
církve	církev	k1gFnSc2
takové	takový	k3xDgInPc4
kroky	krok	k1gInPc4
odmítají	odmítat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jedná	jednat	k5eAaImIp3nS
se	se	k3xPyFc4
o	o	k7c4
Reformovanou	reformovaný	k2eAgFnSc4d1
církev	církev	k1gFnSc4
Kantonu	Kanton	k1gInSc2
Neuchâtel	Neuchâtlo	k1gNnPc2
a	a	k8xC
Evangelickou	evangelický	k2eAgFnSc4d1
svobodnou	svobodný	k2eAgFnSc4d1
církev	církev	k1gFnSc4
Ženevy	Ženeva	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Od	od	k7c2
listopadu	listopad	k1gInSc2
2012	#num#	k4
do	do	k7c2
července	červenec	k1gInSc2
2017	#num#	k4
se	se	k3xPyFc4
dostalo	dostat	k5eAaPmAgNnS
pouze	pouze	k6eAd1
8	#num#	k4
homosexuálním	homosexuální	k2eAgNnSc7d1
partnerstvím	partnerství	k1gNnSc7
požehnání	požehnání	k1gNnPc2
ze	z	k7c2
strany	strana	k1gFnSc2
Reformované	reformovaný	k2eAgFnSc2d1
církve	církev	k1gFnSc2
Kantonu	Kanton	k1gInSc2
Vaud	Vaudo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Statistiky	statistika	k1gFnPc1
</s>
<s>
První	první	k4xOgNnSc1
registrované	registrovaný	k2eAgNnSc1d1
partnerství	partnerství	k1gNnSc1
bylo	být	k5eAaImAgNnS
uzavřeno	uzavřít	k5eAaPmNgNnS
2	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2007	#num#	k4
v	v	k7c6
italsky	italsky	k6eAd1
mluvícím	mluvící	k2eAgInSc6d1
kantonu	kanton	k1gInSc6
Ticino	Ticino	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
letech	léto	k1gNnPc6
2007-2017	2007-2017	k4
bylo	být	k5eAaImAgNnS
ve	v	k7c6
Švýcarsku	Švýcarsko	k1gNnSc6
uzavřeno	uzavřít	k5eAaPmNgNnS
celkem	celkem	k6eAd1
9	#num#	k4
507	#num#	k4
registrovaných	registrovaný	k2eAgNnPc2d1
partnerství	partnerství	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Federální	federální	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
(	(	kIx(
<g/>
podle	podle	k7c2
roku	rok	k1gInSc2
<g/>
)	)	kIx)
</s>
<s>
RokŽenské	RokŽenský	k2eAgNnSc1d1
páryMužské	páryMužský	k2eAgNnSc1d1
páryCelkem	páryCelek	k1gMnSc7
</s>
<s>
200757314312004	#num#	k4
</s>
<s>
2008271660931	#num#	k4
</s>
<s>
2009284588872	#num#	k4
</s>
<s>
2010221499720	#num#	k4
</s>
<s>
2011246426672	#num#	k4
</s>
<s>
2012267428695	#num#	k4
</s>
<s>
2013230463693	#num#	k4
</s>
<s>
2014270450720	#num#	k4
</s>
<s>
2015261440701	#num#	k4
</s>
<s>
2016224497721	#num#	k4
</s>
<s>
2017301477778	#num#	k4
</s>
<s>
Katonální	Katonální	k2eAgFnSc1d1
úroveň	úroveň	k1gFnSc1
(	(	kIx(
<g/>
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kanton	Kanton	k1gInSc1
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
<g/>
Počet	počet	k1gInSc1
partnerstvíKantonPočet	partnerstvíKantonPočet	k1gInSc1
partnerství	partnerství	k1gNnSc1
</s>
<s>
Aargau	Aargau	k6eAd1
<g/>
444	#num#	k4
<g/>
Nidwalden	Nidwaldna	k1gFnPc2
<g/>
19	#num#	k4
</s>
<s>
Appenzell	Appenzell	k1gInSc4
Ausserrhoden	Ausserrhodna	k1gFnPc2
<g/>
34	#num#	k4
<g/>
Obwalden	Obwaldna	k1gFnPc2
<g/>
19	#num#	k4
</s>
<s>
Appenzell	Appenzell	k1gInSc4
Innerrhoden	Innerrhodna	k1gFnPc2
<g/>
8	#num#	k4
<g/>
Schaffhausen	Schaffhausna	k1gFnPc2
<g/>
63	#num#	k4
</s>
<s>
Basel-Landschaft	Basel-Landschaft	k1gMnSc1
<g/>
303	#num#	k4
<g/>
Schwyz	Schwyz	k1gInSc1
<g/>
91	#num#	k4
</s>
<s>
Basel-Stadt	Basel-Stadt	k1gMnSc1
<g/>
361	#num#	k4
<g/>
Solothurn	Solothurn	k1gInSc1
<g/>
191	#num#	k4
</s>
<s>
Bern	Bern	k1gInSc4
<g/>
937	#num#	k4
<g/>
Sankt	Sankt	k1gInSc1
Gallen	Gallen	k2eAgInSc1d1
<g/>
281	#num#	k4
</s>
<s>
Fribourg	Fribourg	k1gInSc1
<g/>
213	#num#	k4
<g/>
Thurgau	Thurgaus	k1gInSc2
<g/>
182	#num#	k4
</s>
<s>
Ženeva	Ženeva	k1gFnSc1
<g/>
801	#num#	k4
<g/>
Ticino	Ticino	k1gNnSc4
<g/>
209	#num#	k4
</s>
<s>
Glarus	Glarus	k1gInSc1
<g/>
17	#num#	k4
<g/>
Uri	Uri	k1gFnPc2
<g/>
16	#num#	k4
</s>
<s>
Graubünden	Graubündna	k1gFnPc2
<g/>
86	#num#	k4
<g/>
Valais	Valais	k1gFnPc2
<g/>
172	#num#	k4
</s>
<s>
Jura	jura	k1gFnSc1
<g/>
39	#num#	k4
<g/>
Vaud	Vaud	k1gInSc1
<g/>
967	#num#	k4
</s>
<s>
Lucerne	Lucernout	k5eAaPmIp3nS
<g/>
286	#num#	k4
<g/>
Zug	Zug	k1gFnPc2
<g/>
109	#num#	k4
</s>
<s>
Neuchâtel	Neuchâtel	k1gMnSc1
<g/>
123	#num#	k4
<g/>
Curych	Curych	k1gInSc1
<g/>
2	#num#	k4
758	#num#	k4
</s>
<s>
Kantonální	kantonální	k2eAgInPc1d1
zákony	zákon	k1gInPc1
</s>
<s>
"	"	kIx"
<g/>
Stejnopohlavní	Stejnopohlavní	k2eAgNnSc1d1
partnerství	partnerství	k1gNnSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
Švýcarsku	Švýcarsko	k1gNnSc6
zcela	zcela	k6eAd1
normálním	normální	k2eAgInSc7d1
fenoménem	fenomén	k1gInSc7
<g/>
.	.	kIx.
<g/>
"	"	kIx"
Ilustrativní	ilustrativní	k2eAgInSc1d1
obrázek	obrázek	k1gInSc1
luzernské	luzernský	k2eAgFnSc2d1
kantonální	kantonální	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
pro	pro	k7c4
uprchlíky	uprchlík	k1gMnPc4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Soužití	soužití	k1gNnSc1
</s>
<s>
Současné	současný	k2eAgInPc1d1
švýcarské	švýcarský	k2eAgInPc1d1
kantonální	kantonální	k2eAgInPc1d1
ústavy	ústav	k1gInPc1
umožňují	umožňovat	k5eAaImIp3nP
a	a	k8xC
garantují	garantovat	k5eAaBmIp3nP
všem	všecek	k3xTgFnPc3
bez	bez	k7c2
rozdílu	rozdíl	k1gInSc2
právo	právo	k1gNnSc4
na	na	k7c4
soužití	soužití	k1gNnSc4
a	a	k8xC
založení	založení	k1gNnSc4
rodiny	rodina	k1gFnSc2
mimo	mimo	k7c4
manželství	manželství	k1gNnSc4
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
jak	jak	k6eAd1
heterosexuálním	heterosexuální	k2eAgInSc7d1
<g/>
,	,	kIx,
tak	tak	k9
i	i	k9
homosexuálním	homosexuální	k2eAgInPc3d1
párům	pár	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mezi	mezi	k7c4
tyto	tento	k3xDgInPc4
kantony	kanton	k1gInPc4
patří	patřit	k5eAaImIp3nS
také	také	k9
Vaud	Vaud	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
Curych	Curych	k1gInSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
Appenzell	Appenzell	k1gInSc1
Ausserrhoden	Ausserrhodna	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
Basel-Stadt	Basel-Stadt	k1gInSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
Bern	Bern	k1gInSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
Ženeva	Ženeva	k1gFnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
Zug	Zug	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
Schaffhausen	Schaffhausen	k1gInSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Fribourg	Fribourg	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Registrovaná	registrovaný	k2eAgNnPc1d1
partnerství	partnerství	k1gNnPc1
</s>
<s>
Kanton	Kanton	k1gInSc4
Ženeva	Ženeva	k1gFnSc1
přijal	přijmout	k5eAaPmAgInS
zákon	zákon	k1gInSc1
o	o	k7c4
partnerství	partnerství	k1gNnSc4
na	na	k7c6
kantonální	kantonální	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
2001	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgInSc1
garantuje	garantovat	k5eAaBmIp3nS
všem	všecek	k3xTgMnPc3
nemanželským	manželský	k2eNgMnPc3d1
párům	pár	k1gInPc3
<g/>
,	,	kIx,
homosexuálním	homosexuální	k2eAgInSc7d1
i	i	k8xC
heterosexuálním	heterosexuální	k2eAgInSc7d1
<g/>
,	,	kIx,
část	část	k1gFnSc4
práv	právo	k1gNnPc2
a	a	k8xC
povinností	povinnost	k1gFnPc2
plynoucích	plynoucí	k2eAgFnPc2d1
z	z	k7c2
manželství	manželství	k1gNnSc2
<g/>
,	,	kIx,
vyjma	vyjma	k7c2
daňových	daňový	k2eAgFnPc2d1
úlev	úleva	k1gFnPc2
<g/>
,	,	kIx,
sociálního	sociální	k2eAgNnSc2d1
zabezpečení	zabezpečení	k1gNnSc2
a	a	k8xC
zdravotních	zdravotní	k2eAgInPc2d1
benefitů	benefit	k1gInPc2
(	(	kIx(
<g/>
na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
federálního	federální	k2eAgInSc2d1
zákona	zákon	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ženevská	ženevský	k2eAgFnSc1d1
legislativa	legislativa	k1gFnSc1
se	se	k3xPyFc4
obsahově	obsahově	k6eAd1
velmi	velmi	k6eAd1
podobá	podobat	k5eAaImIp3nS
francouzskému	francouzský	k2eAgInSc3d1
PACS	PACS	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c4
podzim	podzim	k1gInSc4
2016	#num#	k4
přijalo	přijmout	k5eAaPmAgNnS
Ministerstvo	ministerstvo	k1gNnSc1
školství	školství	k1gNnSc2
Kantonu	Kanton	k1gInSc2
Ženeva	Ženeva	k1gFnSc1
nové	nový	k2eAgFnSc2d1
školské	školský	k2eAgFnSc2d1
směrnice	směrnice	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
plně	plně	k6eAd1
uznávají	uznávat	k5eAaImIp3nP
homoparentální	homoparentální	k2eAgNnSc4d1
rodičovství	rodičovství	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předtím	předtím	k6eAd1
nebyli	být	k5eNaImAgMnP
homosexuální	homosexuální	k2eAgMnPc4d1
rodiče	rodič	k1gMnPc4
uznávány	uznáván	k2eAgMnPc4d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
školské	školský	k2eAgInPc1d1
interní	interní	k2eAgInPc1d1
dokumenty	dokument	k1gInPc1
rozeznávaly	rozeznávat	k5eAaImAgInP
pouze	pouze	k6eAd1
matku	matka	k1gFnSc4
a	a	k8xC
otce	otec	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přijetím	přijetí	k1gNnSc7
těchto	tento	k3xDgFnPc2
směrnic	směrnice	k1gFnPc2
se	se	k3xPyFc4
přestalo	přestat	k5eAaPmAgNnS
pracovat	pracovat	k5eAaImF
s	s	k7c7
termíny	termín	k1gInPc7
"	"	kIx"
<g/>
otec	otec	k1gMnSc1
<g/>
"	"	kIx"
a	a	k8xC
"	"	kIx"
<g/>
matka	matka	k1gFnSc1
<g/>
"	"	kIx"
a	a	k8xC
zavedl	zavést	k5eAaPmAgMnS
se	se	k3xPyFc4
genderově	genderově	k6eAd1
neutrální	neutrální	k2eAgInSc1d1
výraz	výraz	k1gInSc1
"	"	kIx"
<g/>
rodiče	rodič	k1gMnPc1
<g/>
"	"	kIx"
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
22	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc4
2002	#num#	k4
přijal	přijmout	k5eAaPmAgInS
Kanton	Kanton	k1gInSc1
Curych	Curych	k1gInSc1
v	v	k7c6
referendu	referendum	k1gNnSc6
(	(	kIx(
<g/>
62,7	62,7	k4
%	%	kIx~
bylo	být	k5eAaImAgNnS
pro	pro	k7c4
<g/>
)	)	kIx)
zákon	zákon	k1gInSc4
o	o	k7c6
partnerství	partnerství	k1gNnSc6
párů	pár	k1gInPc2
stejného	stejný	k2eAgNnSc2d1
pohlaví	pohlaví	k1gNnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
je	být	k5eAaImIp3nS
v	v	k7c6
podstatě	podstata	k1gFnSc6
kopií	kopie	k1gFnPc2
ženevského	ženevský	k2eAgInSc2d1
zákona	zákon	k1gInSc2
<g/>
,	,	kIx,
ale	ale	k8xC
podmínkou	podmínka	k1gFnSc7
pro	pro	k7c4
vstup	vstup	k1gInSc4
do	do	k7c2
něj	on	k3xPp3gMnSc2
je	být	k5eAaImIp3nS
délka	délka	k1gFnSc1
soužití	soužití	k1gNnSc2
minimálně	minimálně	k6eAd1
6	#num#	k4
měsíců	měsíc	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
červenci	červenec	k1gInSc6
2004	#num#	k4
přijal	přijmout	k5eAaPmAgInS
Kanton	Kanton	k1gInSc1
Neuchâtel	Neuchâtel	k1gInSc1
v	v	k7c6
referendu	referendum	k1gNnSc6
(	(	kIx(
<g/>
65	#num#	k4
<g/>
:	:	kIx,
<g/>
38	#num#	k4
<g/>
)	)	kIx)
zákon	zákon	k1gInSc4
uznávající	uznávající	k2eAgNnSc4d1
nesezdané	sezdaný	k2eNgNnSc4d1
soužití	soužití	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Registrované	registrovaný	k2eAgNnSc1d1
partnerství	partnerství	k1gNnSc1
párů	pár	k1gInPc2
stejného	stejný	k2eAgNnSc2d1
pohlaví	pohlaví	k1gNnSc2
bylo	být	k5eAaImAgNnS
přijato	přijmout	k5eAaPmNgNnS
jako	jako	k8xS,k8xC
ústavní	ústavní	k2eAgInSc1d1
zákon	zákon	k1gInSc1
v	v	k7c6
Kantonu	Kanton	k1gInSc6
Fribourg	Fribourg	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
květnu	květen	k1gInSc6
2004	#num#	k4
si	se	k3xPyFc3
tamní	tamní	k2eAgMnPc1d1
voliči	volič	k1gMnPc1
odhlasovali	odhlasovat	k5eAaPmAgMnP
novou	nový	k2eAgFnSc4d1
podobu	podoba	k1gFnSc4
ústavy	ústava	k1gFnSc2
<g/>
,	,	kIx,
s	s	k7c7
níž	jenž	k3xRgFnSc7
souhlasilo	souhlasit	k5eAaImAgNnS
58,03	58,03	k4
%	%	kIx~
<g/>
.	.	kIx.
41,97	41,97	k4
%	%	kIx~
bylo	být	k5eAaImAgNnS
proti	proti	k7c3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
Učinným	Učinný	k2eAgInSc7d1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2005	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Manželství	manželství	k1gNnSc1
</s>
<s>
6	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2016	#num#	k4
hlasovala	hlasovat	k5eAaImAgFnS
Kantonální	kantonální	k2eAgFnSc1d1
rada	rada	k1gFnSc1
Curychu	Curych	k1gInSc2
v	v	k7c6
poměru	poměr	k1gInSc6
hlasů	hlas	k1gInPc2
110	#num#	k4
<g/>
:	:	kIx,
<g/>
52	#num#	k4
proti	proti	k7c3
návrhu	návrh	k1gInSc3
definujícím	definující	k2eAgInSc7d1
manželství	manželství	k1gNnPc4
jako	jako	k9
svazek	svazek	k1gInSc1
muže	muž	k1gMnSc2
a	a	k8xC
ženy	žena	k1gFnPc1
v	v	k7c6
ústavě	ústava	k1gFnSc6
Kantonu	Kanton	k1gInSc2
Curych	Curych	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
pozadí	pozadí	k1gNnSc6
návrhu	návrh	k1gInSc2
stála	stát	k5eAaImAgFnS
Federální	federální	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
unie	unie	k1gFnSc1
Švýcarska	Švýcarsko	k1gNnSc2
-	-	kIx~
EDU	Eda	k1gMnSc4
(	(	kIx(
<g/>
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
zahájila	zahájit	k5eAaPmAgFnS
sběr	sběr	k1gInSc4
podpisů	podpis	k1gInPc2
pro	pro	k7c4
vypsání	vypsání	k1gNnSc4
referenda	referendum	k1gNnSc2
o	o	k7c4
přijetí	přijetí	k1gNnSc4
zákona	zákon	k1gInSc2
o	o	k7c6
registrovaném	registrovaný	k2eAgNnSc6d1
partnerství	partnerství	k1gNnSc6
v	v	k7c6
roce	rok	k1gInSc6
2004	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
tímto	tento	k3xDgNnSc7
snažila	snažit	k5eAaImAgFnS
docílit	docílit	k5eAaPmF
přijetí	přijetí	k1gNnSc4
ústavního	ústavní	k2eAgInSc2d1
zákazu	zákaz	k1gInSc2
stejnopohlavního	stejnopohlavní	k2eAgNnSc2d1
manželství	manželství	k1gNnSc2
na	na	k7c6
kantonální	kantonální	k2eAgFnSc6d1
úrovni	úroveň	k1gFnSc6
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
o	o	k7c4
což	což	k3yQnSc4,k3yRnSc4
se	se	k3xPyFc4
už	už	k9
také	také	k9
pokoušela	pokoušet	k5eAaImAgFnS
manželská	manželský	k2eAgFnSc1d1
iniciativa	iniciativa	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
2013	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
EDU	Eda	k1gMnSc4
a	a	k8xC
většina	většina	k1gFnSc1
členů	člen	k1gMnPc2
Švýcarské	švýcarský	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
strany	strana	k1gFnSc2
byly	být	k5eAaImAgFnP
pro	pro	k7c4
přijetí	přijetí	k1gNnSc4
návrhu	návrh	k1gInSc2
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
všechny	všechen	k3xTgFnPc1
zbývající	zbývající	k2eAgFnPc1d1
strany	strana	k1gFnPc1
<g/>
,	,	kIx,
včetně	včetně	k7c2
Křesťanskodemokratické	křesťanskodemokratický	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
Evangelické	evangelický	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
strany	strana	k1gFnSc2
byly	být	k5eAaImAgFnP
proti	proti	k7c3
<g/>
.	.	kIx.
</s>
<s desamb="1">
EDU	Eda	k1gMnSc4
se	se	k3xPyFc4
však	však	k9
podařilo	podařit	k5eAaPmAgNnS
nasbírat	nasbírat	k5eAaPmF
6	#num#	k4
tisíc	tisíc	k4xCgInPc2
podpisů	podpis	k1gInPc2
za	za	k7c4
vypsání	vypsání	k1gNnSc4
kantonálního	kantonální	k2eAgNnSc2d1
referenda	referendum	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
pak	pak	k6eAd1
proběhlo	proběhnout	k5eAaPmAgNnS
27	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
něm	on	k3xPp3gNnSc6
se	se	k3xPyFc4
drtivá	drtivý	k2eAgFnSc1d1
většina	většina	k1gFnSc1
voličů	volič	k1gInPc2
80,9	80,9	k4
%	%	kIx~
vyslovila	vyslovit	k5eAaPmAgFnS
proti	proti	k7c3
ústavní	ústavní	k2eAgFnSc3d1
definici	definice	k1gFnSc3
manželství	manželství	k1gNnSc2
jako	jako	k8xC,k8xS
muže	muž	k1gMnSc4
a	a	k8xC
ženy	žena	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pouze	pouze	k6eAd1
19,1	19,1	k4
%	%	kIx~
bylo	být	k5eAaImAgNnS
pro	pro	k7c4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
některých	některý	k3yIgInPc6
volebních	volební	k2eAgInPc6d1
obvodech	obvod	k1gInPc6
kantonu	kanton	k1gInSc2
hlasovalo	hlasovat	k5eAaImAgNnS
"	"	kIx"
<g/>
ne	ne	k9
<g/>
"	"	kIx"
až	až	k9
92	#num#	k4
%	%	kIx~
voličů	volič	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
Všechny	všechen	k3xTgInPc1
otce	otec	k1gMnSc4
hlasovaly	hlasovat	k5eAaImAgInP
proti	proti	k7c3
<g/>
.	.	kIx.
</s>
<s>
Iniciativa	iniciativa	k1gFnSc1
"	"	kIx"
<g/>
Schutz	Schutz	k1gInSc1
der	drát	k5eAaImRp2nS
Ehe	ehe	k0
<g/>
"	"	kIx"
</s>
<s>
Možnost	možnost	k1gFnSc1
hlasováníPočet	hlasováníPočet	k1gInSc1
hlasůPočet	hlasůPočet	k1gInSc1
hlasů	hlas	k1gInPc2
(	(	kIx(
<g/>
%	%	kIx~
<g/>
)	)	kIx)
</s>
<s>
Ne	ne	k9
Ne	ne	k9
<g/>
319	#num#	k4
50180,91	50180,91	k4
</s>
<s>
Ano	ano	k9
<g/>
75	#num#	k4
36219,09	36219,09	k4
</s>
<s>
Celkem	celkem	k6eAd1
<g/>
394	#num#	k4
863100,00	863100,00	k4
</s>
<s>
Stejnopohlavní	Stejnopohlavní	k2eAgNnSc1d1
manželství	manželství	k1gNnSc1
</s>
<s>
V	v	k7c6
roce	rok	k1gInSc6
2012	#num#	k4
požádal	požádat	k5eAaPmAgInS
parlament	parlament	k1gInSc1
federální	federální	k2eAgFnSc4d1
radu	rada	k1gFnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
přezkoumala	přezkoumat	k5eAaPmAgFnS
zákon	zákon	k1gInSc4
o	o	k7c6
rodině	rodina	k1gFnSc6
<g/>
,	,	kIx,
respektive	respektive	k9
to	ten	k3xDgNnSc1
<g/>
,	,	kIx,
zda	zda	k8xS
reflektuje	reflektovat	k5eAaImIp3nS
změny	změna	k1gFnPc4
ve	v	k7c6
společnosti	společnost	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
březnu	březen	k1gInSc6
2015	#num#	k4
vydala	vydat	k5eAaPmAgFnS
rada	rada	k1gFnSc1
vládní	vládní	k2eAgFnSc1d1
report	report	k1gInSc4
manželství	manželství	k1gNnPc2
a	a	k8xC
nových	nový	k2eAgNnPc2d1
práv	právo	k1gNnPc2
rodin	rodina	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
doporučila	doporučit	k5eAaPmAgFnS
legalizaci	legalizace	k1gFnSc4
<g />
.	.	kIx.
</s>
<s hack="1">
registrovaného	registrovaný	k2eAgNnSc2d1
partnerství	partnerství	k1gNnSc2
pro	pro	k7c4
heterosexuální	heterosexuální	k2eAgInPc4d1
páry	pár	k1gInPc4
a	a	k8xC
manželství	manželství	k1gNnPc4
pro	pro	k7c4
homosexuální	homosexuální	k2eAgInPc4d1
páry	pár	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
Členka	členka	k1gFnSc1
federální	federální	k2eAgFnSc2d1
rady	rada	k1gFnSc2
a	a	k8xC
švýcarská	švýcarský	k2eAgFnSc1d1
prezidentka	prezidentka	k1gFnSc1
Simonetta	Simonetta	k1gFnSc1
Sommarguová	Sommarguová	k1gFnSc1
<g/>
,	,	kIx,
jíž	jenž	k3xRgFnSc3
byl	být	k5eAaImAgInS
svěřen	svěřen	k2eAgInSc1d1
resort	resort	k1gInSc1
spravedlnosti	spravedlnost	k1gFnSc2
<g/>
,	,	kIx,
řekla	říct	k5eAaPmAgFnS
<g/>
,	,	kIx,
že	že	k8xS
doufá	doufat	k5eAaImIp3nS
v	v	k7c4
brzkou	brzký	k2eAgFnSc4d1
legalizaci	legalizace	k1gFnSc4
sňatků	sňatek	k1gInPc2
pro	pro	k7c4
homosexuální	homosexuální	k2eAgInPc4d1
páry	pár	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Politické	politický	k2eAgFnPc1d1
strany	strana	k1gFnPc1
</s>
<s>
Stejnopohlavní	Stejnopohlavní	k2eAgNnPc1d1
manželství	manželství	k1gNnPc1
podporují	podporovat	k5eAaImIp3nP
Švýcarská	švýcarský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
zelených	zelená	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
Konzervativní	konzervativní	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
Sociálně	sociálně	k6eAd1
demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Švýcarska	Švýcarsko	k1gNnSc2
<g/>
,	,	kIx,
Zelená	zelený	k2eAgFnSc1d1
liberální	liberální	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
Švýcarská	švýcarský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
práce	práce	k1gFnSc1
<g/>
,	,	kIx,
Křesťanskosociální	křesťanskosociální	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Obwaldenu	Obwalden	k1gInSc2
a	a	k8xC
většina	většina	k1gFnSc1
liberálů	liberál	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Švýcarská	švýcarský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
Křesťanskodemokratická	křesťanskodemokratický	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Švýcarska	Švýcarsko	k1gNnSc2
(	(	kIx(
<g/>
CVP	CVP	kA
<g/>
/	/	kIx~
<g/>
PDC	PDC	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Evangelická	evangelický	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
Ticino	Ticino	k1gNnSc1
a	a	k8xC
Hnutí	hnutí	k1gNnSc1
ženevských	ženevský	k2eAgMnPc2d1
občanů	občan	k1gMnPc2
jsou	být	k5eAaImIp3nP
proti	proti	k7c3
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
r.	r.	kA
2017	#num#	k4
vyjádřil	vyjádřit	k5eAaPmAgMnS
předseda	předseda	k1gMnSc1
CVP	CVP	kA
Gerhard	Gerhard	k1gMnSc1
Pfister	Pfister	k1gMnSc1
důvěru	důvěra	k1gFnSc4
v	v	k7c6
zachování	zachování	k1gNnSc6
podpory	podpora	k1gFnSc2
výlučnosti	výlučnost	k1gFnSc2
manželství	manželství	k1gNnSc2
jako	jako	k8xC,k8xS
svazku	svazek	k1gInSc2
muže	muž	k1gMnSc2
a	a	k8xC
ženy	žena	k1gFnPc1
ze	z	k7c2
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
Pfistera	Pfister	k1gMnSc2
podporuje	podporovat	k5eAaImIp3nS
takovou	takový	k3xDgFnSc4
podobu	podoba	k1gFnSc4
manželství	manželství	k1gNnSc2
až	až	k6eAd1
dvě	dva	k4xCgFnPc1
třetiny	třetina	k1gFnPc1
poslanců	poslanec	k1gMnPc2
CVP	CVP	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
někteří	některý	k3yIgMnPc1
politici	politik	k1gMnPc1
CVP	CVP	kA
jsou	být	k5eAaImIp3nP
stejnopohlavnímu	stejnopohlavní	k2eAgInSc3d1
manželství	manželství	k1gNnPc2
nakloněni	naklonit	k5eAaPmNgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Například	například	k6eAd1
členka	členka	k1gFnSc1
Národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
města	město	k1gNnSc2
Luzern	Luzerna	k1gFnPc2
Andrea	Andrea	k1gFnSc1
Gmür-Schönenbergerová	Gmür-Schönenbergerová	k1gFnSc1
a	a	k8xC
bývalá	bývalý	k2eAgFnSc1d1
členka	členka	k1gFnSc1
federální	federální	k2eAgFnSc2d1
rady	rada	k1gFnSc2
Ruth	Ruth	k1gFnSc1
Metzlerová	Metzlerová	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
dubnu	duben	k1gInSc6
2018	#num#	k4
hlasovala	hlasovat	k5eAaImAgFnS
ženská	ženský	k2eAgFnSc1d1
část	část	k1gFnSc1
liberálů	liberál	k1gMnPc2
v	v	k7c6
poměru	poměr	k1gInSc6
hlasů	hlas	k1gInPc2
56	#num#	k4
<g/>
:	:	kIx,
<g/>
2	#num#	k4
pro	pro	k7c4
stejnopohlavní	stejnopohlavní	k2eAgNnSc4d1
manželství	manželství	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Parlamentní	parlamentní	k2eAgFnSc1d1
iniciativa	iniciativa	k1gFnSc1
Zelené	Zelené	k2eAgFnSc2d1
liberální	liberální	k2eAgFnSc2d1
strany	strana	k1gFnSc2
</s>
<s>
V	v	k7c6
prosinci	prosinec	k1gInSc6
2013	#num#	k4
předložila	předložit	k5eAaPmAgFnS
Zelená	zelený	k2eAgFnSc1d1
liberální	liberální	k2eAgFnSc1d1
strana	strana	k1gFnSc1
parlamentní	parlamentní	k2eAgFnSc4d1
iniciativu	iniciativa	k1gFnSc4
požadující	požadující	k2eAgFnSc4d1
ústavní	ústavní	k2eAgFnSc4d1
změnu	změna	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
by	by	kYmCp3nS
legalizovala	legalizovat	k5eAaBmAgFnS
stejnopohlavní	stejnopohlavní	k2eAgNnSc4d1
manželství	manželství	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
Před	před	k7c7
zahájením	zahájení	k1gNnSc7
legislativního	legislativní	k2eAgInSc2d1
procesu	proces	k1gInSc2
by	by	kYmCp3nS
musela	muset	k5eAaImAgFnS
být	být	k5eAaImF
taková	takový	k3xDgFnSc1
změna	změna	k1gFnSc1
odsouhlasená	odsouhlasený	k2eAgFnSc1d1
v	v	k7c6
referendu	referendum	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s>
20	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2015	#num#	k4
hlasoval	hlasovat	k5eAaImAgInS
Výbor	výbor	k1gInSc1
pro	pro	k7c4
právní	právní	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
Národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
pro	pro	k7c4
přijetí	přijetí	k1gNnSc4
této	tento	k3xDgFnSc2
iniciativy	iniciativa	k1gFnSc2
v	v	k7c6
poměru	poměr	k1gInSc6
hlasů	hlas	k1gInPc2
12	#num#	k4
<g/>
:	:	kIx,
<g/>
9	#num#	k4
a	a	k8xC
jedním	jeden	k4xCgNnSc7
zdržením	zdržení	k1gNnSc7
se	s	k7c7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
květnu	květen	k1gInSc6
2015	#num#	k4
byl	být	k5eAaImAgInS
zahájen	zahájit	k5eAaPmNgInS
sběr	sběr	k1gInSc1
podpisů	podpis	k1gInPc2
pro	pro	k7c4
vypsání	vypsání	k1gNnSc4
celonárodního	celonárodní	k2eAgNnSc2d1
referenda	referendum	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Petice	petice	k1gFnSc1
s	s	k7c7
nasbíranými	nasbíraný	k2eAgInPc7d1
podpisy	podpis	k1gInPc7
pak	pak	k6eAd1
byla	být	k5eAaImAgFnS
následně	následně	k6eAd1
doručená	doručený	k2eAgFnSc1d1
Radě	rada	k1gFnSc3
kantonů	kanton	k1gInPc2
ještě	ještě	k6eAd1
předtím	předtím	k6eAd1
<g/>
,	,	kIx,
než	než	k8xS
se	se	k3xPyFc4
o	o	k7c6
návrhu	návrh	k1gInSc6
vůbec	vůbec	k9
začalo	začít	k5eAaPmAgNnS
diskutovat	diskutovat	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
1	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc4
2015	#num#	k4
hlasoval	hlasovat	k5eAaImAgInS
Výbor	výbor	k1gInSc1
pro	pro	k7c4
právní	právní	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
Rady	rada	k1gFnSc2
kantonů	kanton	k1gInPc2
pro	pro	k7c4
přijetí	přijetí	k1gNnSc4
iniciativy	iniciativa	k1gFnSc2
v	v	k7c6
poměru	poměr	k1gInSc6
hlasů	hlas	k1gInPc2
7	#num#	k4
<g/>
:	:	kIx,
<g/>
5	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výbor	výbor	k1gInSc1
pro	pro	k7c4
právní	právní	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
Národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
pak	pak	k6eAd1
byl	být	k5eAaImAgInS
dotázán	dotázán	k2eAgInSc1d1
<g/>
,	,	kIx,
zda	zda	k8xS
zpracuje	zpracovat	k5eAaPmIp3nS
příslušný	příslušný	k2eAgInSc1d1
zákon	zákon	k1gInSc1
v	v	k7c6
časovém	časový	k2eAgInSc6d1
horizontu	horizont	k1gInSc6
dvou	dva	k4xCgInPc2
let	léto	k1gNnPc2
(	(	kIx(
<g/>
v	v	k7c6
souladu	soulad	k1gInSc6
se	s	k7c7
článkem	článek	k1gInSc7
111	#num#	k4
ústavy	ústava	k1gFnSc2
<g/>
)	)	kIx)
<g/>
,	,	kIx,
tedy	tedy	k9
do	do	k7c2
roku	rok	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
z	z	k7c2
důvodu	důvod	k1gInSc2
komplexnosti	komplexnost	k1gFnSc2
právní	právní	k2eAgFnSc2d1
úpravy	úprava	k1gFnSc2
navrhla	navrhnout	k5eAaPmAgFnS
11	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2017	#num#	k4
Národní	národní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
odklad	odklad	k1gInSc4
o	o	k7c4
další	další	k2eAgInPc4d1
dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
<g/>
,	,	kIx,
tedy	tedy	k8xC
na	na	k7c4
rok	rok	k1gInSc4
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Federální	federální	k2eAgFnSc1d1
rada	rada	k1gFnSc1
pak	pak	k6eAd1
byla	být	k5eAaImAgFnS
požádána	požádat	k5eAaPmNgFnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
ona	onen	k3xDgFnSc1,k3xPp3gFnSc1
zapojila	zapojit	k5eAaPmAgFnS
do	do	k7c2
řešení	řešení	k1gNnSc2
této	tento	k3xDgFnSc2
problematiky	problematika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
Menšinová	menšinový	k2eAgFnSc1d1
Švýcarská	švýcarský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
se	se	k3xPyFc4
tuto	tento	k3xDgFnSc4
iniciativu	iniciativa	k1gFnSc4
pokusila	pokusit	k5eAaPmAgFnS
zablokovat	zablokovat	k5eAaPmF
<g/>
.	.	kIx.
16	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2017	#num#	k4
hlasovala	hlasovat	k5eAaImAgFnS
Národní	národní	k2eAgInPc4d1
rady	rad	k1gInPc4
pro	pro	k7c4
návrh	návrh	k1gInSc4
pokračovat	pokračovat	k5eAaImF
v	v	k7c6
iniciativě	iniciativa	k1gFnSc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
poměru	poměr	k1gInSc6
hlasů	hlas	k1gInPc2
118	#num#	k4
<g/>
:	:	kIx,
<g/>
71	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
81	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
Iniciativa	iniciativa	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
2013	#num#	k4
je	být	k5eAaImIp3nS
ve	v	k7c6
Švýcarsku	Švýcarsko	k1gNnSc6
v	v	k7c6
procesu	proces	k1gInSc6
pomalé	pomalý	k2eAgFnSc2d1
legislativní	legislativní	k2eAgFnSc2d1
přípravy	příprava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Výbor	výbor	k1gInSc1
pro	pro	k7c4
právní	právní	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
Národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
se	se	k3xPyFc4
při	při	k7c6
příležitosti	příležitost	k1gFnSc6
Mezinárodního	mezinárodní	k2eAgInSc2d1
dne	den	k1gInSc2
proti	proti	k7c3
homofobii	homofobie	k1gFnSc3
<g/>
,	,	kIx,
bifobii	bifobie	k1gFnSc6
a	a	k8xC
transfobii	transfobie	k1gFnSc6
17	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2018	#num#	k4
sešel	sejít	k5eAaPmAgInS
za	za	k7c7
účelem	účel	k1gInSc7
diskuze	diskuze	k1gFnSc2
ohledně	ohledně	k7c2
právních	právní	k2eAgInPc2d1
aspektů	aspekt	k1gInPc2
legalizace	legalizace	k1gFnSc2
stejnopohlavního	stejnopohlavní	k2eAgNnSc2d1
manželství	manželství	k1gNnSc2
<g/>
,	,	kIx,
mezi	mezi	k7c4
něž	jenž	k3xRgFnPc4
patří	patřit	k5eAaImIp3nS
také	také	k9
nevyhnutelné	vyhnutelný	k2eNgNnSc1d1
novelizace	novelizace	k1gFnPc4
prováděcích	prováděcí	k2eAgInPc2d1
právních	právní	k2eAgInPc2d1
předpisů	předpis	k1gInPc2
(	(	kIx(
<g/>
iniciativa	iniciativa	k1gFnSc1
z	z	k7c2
roku	rok	k1gInSc2
2013	#num#	k4
znamená	znamenat	k5eAaImIp3nS
ústavní	ústavní	k2eAgFnSc4d1
novelu	novela	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
ale	ale	k9
s	s	k7c7
sebou	se	k3xPyFc7
nepřináší	přinášet	k5eNaImIp3nS
neznamená	znamenat	k5eNaImIp3nS
automatickou	automatický	k2eAgFnSc4d1
změnu	změna	k1gFnSc4
prováděcích	prováděcí	k2eAgInPc2d1
právních	právní	k2eAgInPc2d1
předpisů	předpis	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
skončení	skončení	k1gNnSc6
schůze	schůze	k1gFnSc2
se	se	k3xPyFc4
Výbor	výbor	k1gInSc1
shodl	shodnout	k5eAaPmAgInS,k5eAaBmAgInS
na	na	k7c6
doporučených	doporučený	k2eAgFnPc6d1
novelizacích	novelizace	k1gFnPc6
švýcarského	švýcarský	k2eAgInSc2d1
občanského	občanský	k2eAgInSc2d1
zákoníku	zákoník	k1gInSc2
<g/>
,	,	kIx,
mezi	mezi	k7c4
něž	jenž	k3xRgFnPc4
patří	patřit	k5eAaImIp3nS
odstranění	odstranění	k1gNnSc1
heteronormativní	heteronormativní	k2eAgFnSc2d1
zákonné	zákonný	k2eAgFnSc2d1
definice	definice	k1gFnSc2
manželství	manželství	k1gNnSc2
jako	jako	k8xC,k8xS
svazku	svazek	k1gInSc2
muže	muž	k1gMnSc2
a	a	k8xC
ženy	žena	k1gFnPc1
<g/>
,	,	kIx,
a	a	k8xC
její	její	k3xOp3gNnSc1
nahrazení	nahrazení	k1gNnSc1
genderově	genderově	k6eAd1
neutrálním	neutrální	k2eAgInSc7d1
svazkem	svazek	k1gInSc7
dvou	dva	k4xCgFnPc2
osob	osoba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgFnSc7d1
potřebnou	potřebný	k2eAgFnSc7d1
změnou	změna	k1gFnSc7
je	být	k5eAaImIp3nS
novela	novela	k1gFnSc1
zákona	zákon	k1gInSc2
o	o	k7c4
evidenci	evidence	k1gFnSc4
obyvatel	obyvatel	k1gMnPc2
z	z	k7c2
roku	rok	k1gInSc2
1953	#num#	k4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
taktéž	taktéž	k?
definuje	definovat	k5eAaBmIp3nS
manželství	manželství	k1gNnSc4
jako	jako	k8xC,k8xS
svazek	svazek	k1gInSc4
muže	muž	k1gMnSc2
a	a	k8xC
ženy	žena	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalšími	další	k2eAgInPc7d1
právními	právní	k2eAgInPc7d1
předpisy	předpis	k1gInPc7
<g/>
,	,	kIx,
které	který	k3yQgInPc4,k3yRgInPc4,k3yIgInPc4
bude	být	k5eAaImBp3nS
třeba	třeba	k9
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
iniciativou	iniciativa	k1gFnSc7
z	z	k7c2
roku	rok	k1gInSc2
2013	#num#	k4
změnit	změnit	k5eAaPmF
<g/>
,	,	kIx,
jsou	být	k5eAaImIp3nP
změny	změna	k1gFnPc1
zákona	zákon	k1gInSc2
o	o	k7c6
sociálním	sociální	k2eAgNnSc6d1
zabezpečení	zabezpečení	k1gNnSc6
a	a	k8xC
zákona	zákon	k1gInSc2
o	o	k7c6
získávání	získávání	k1gNnSc6
občanství	občanství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zákon	zákon	k1gInSc1
upravující	upravující	k2eAgInSc1d1
osvojován	osvojován	k2eAgInSc1d1
dětí	dítě	k1gFnPc2
není	být	k5eNaImIp3nS
podle	podle	k7c2
Výboru	výbor	k1gInSc2
pro	pro	k7c4
právní	právní	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
potřeba	potřeba	k6eAd1
měnit	měnit	k5eAaImF
<g/>
,	,	kIx,
neboť	neboť	k8xC
pracuje	pracovat	k5eAaImIp3nS
pouze	pouze	k6eAd1
s	s	k7c7
termínem	termín	k1gInSc7
"	"	kIx"
<g/>
manželství	manželství	k1gNnSc1
<g/>
"	"	kIx"
<g/>
,	,	kIx,
které	který	k3yIgFnPc1,k3yRgFnPc1,k3yQgFnPc1
ale	ale	k8xC
nijak	nijak	k6eAd1
nespecifikuje	specifikovat	k5eNaBmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
momentě	moment	k1gInSc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
budou	být	k5eAaImBp3nP
homosexuální	homosexuální	k2eAgInPc1d1
páry	pár	k1gInPc1
moci	moc	k1gFnSc2
uzavřít	uzavřít	k5eAaPmF
sňatek	sňatek	k1gInSc4
<g/>
,	,	kIx,
bude	být	k5eAaImBp3nS
automaticky	automaticky	k6eAd1
legalizovaná	legalizovaný	k2eAgFnSc1d1
i	i	k8xC
společná	společný	k2eAgFnSc1d1
adopce	adopce	k1gFnSc1
dětí	dítě	k1gFnPc2
homosexuálními	homosexuální	k2eAgInPc7d1
páry	pár	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Lidová	lidový	k2eAgFnSc1d1
iniciativa	iniciativa	k1gFnSc1
Křesťanských	křesťanský	k2eAgMnPc2d1
demokratů	demokrat	k1gMnPc2
"	"	kIx"
<g/>
Za	za	k7c4
páry	pár	k1gInPc4
a	a	k8xC
za	za	k7c4
rodinu	rodina	k1gFnSc4
<g/>
"	"	kIx"
</s>
<s>
Křesťaňskodemokratická	Křesťaňskodemokratický	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
CVP	CVP	kA
<g/>
/	/	kIx~
<g/>
PDC	PDC	kA
<g/>
)	)	kIx)
zahájila	zahájit	k5eAaPmAgFnS
v	v	k7c6
r.	r.	kA
2011	#num#	k4
sběr	sběr	k1gInSc4
podpisů	podpis	k1gInPc2
v	v	k7c6
lidové	lidový	k2eAgFnSc6d1
iniciativě	iniciativa	k1gFnSc6
"	"	kIx"
<g/>
Za	za	k7c4
páry	pár	k1gInPc4
a	a	k8xC
za	za	k7c4
rodinu	rodina	k1gFnSc4
-	-	kIx~
dost	dost	k6eAd1
penalizaci	penalizace	k1gFnSc4
manželství	manželství	k1gNnSc2
<g/>
"	"	kIx"
(	(	kIx(
<g/>
Německy	německy	k6eAd1
<g/>
:	:	kIx,
Für	Für	k1gFnSc1
Ehe	ehe	k0
und	und	k?
Familie	Familie	k1gFnSc1
-	-	kIx~
gegen	gegen	k1gInSc1
die	die	k?
heiratsstrafe	heiratsstraf	k1gMnSc5
<g/>
)	)	kIx)
<g/>
,	,	kIx,
(	(	kIx(
<g/>
Francouzsky	francouzsky	k6eAd1
<g/>
:	:	kIx,
Pour	Pour	k1gMnSc1
le	le	k?
couple	couple	k6eAd1
et	et	k?
la	la	k1gNnSc4
famille	famille	k1gNnPc2
-	-	kIx~
Non	Non	k1gMnPc2
à	à	k?
la	la	k1gNnPc2
pénalisation	pénalisation	k1gInSc1
du	du	k?
mariage	mariage	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Iniciativa	iniciativa	k1gFnSc1
požadovala	požadovat	k5eAaImAgFnS
změnu	změna	k1gFnSc4
článku	článek	k1gInSc2
14	#num#	k4
švýcarské	švýcarský	k2eAgFnSc2d1
federální	federální	k2eAgFnSc2d1
ústavy	ústava	k1gFnSc2
a	a	k8xC
rovná	rovnat	k5eAaImIp3nS
fiskální	fiskální	k2eAgFnSc1d1
a	a	k8xC
sociální	sociální	k2eAgNnPc1d1
práva	právo	k1gNnPc1
pro	pro	k7c4
manželské	manželský	k2eAgInPc4d1
a	a	k8xC
nemanželské	manželský	k2eNgInPc4d1
páry	pár	k1gInPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nicméně	nicméně	k8xC
součástí	součást	k1gFnSc7
požadavku	požadavek	k1gInSc2
na	na	k7c4
změnu	změna	k1gFnSc4
ústavy	ústava	k1gFnSc2
bylo	být	k5eAaImAgNnS
její	její	k3xOp3gNnSc1
doplnění	doplnění	k1gNnSc1
o	o	k7c4
přímou	přímý	k2eAgFnSc4d1
definici	definice	k1gFnSc4
manželství	manželství	k1gNnSc2
jako	jako	k8xC,k8xS
jedinečného	jedinečný	k2eAgInSc2d1
svazku	svazek	k1gInSc2
jednoho	jeden	k4xCgMnSc2
muže	muž	k1gMnSc2
a	a	k8xC
jedné	jeden	k4xCgFnSc2
ženy	žena	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
listopadu	listopad	k1gInSc6
2012	#num#	k4
byl	být	k5eAaImAgInS
sběr	sběr	k1gInSc1
podpisů	podpis	k1gInPc2
ukončen	ukončen	k2eAgMnSc1d1
a	a	k8xC
iniciativa	iniciativa	k1gFnSc1
zahájená	zahájený	k2eAgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Švýcarská	švýcarský	k2eAgFnSc1d1
federální	federální	k2eAgFnSc1d1
rada	rada	k1gFnSc1
iniciativu	iniciativa	k1gFnSc4
přezkoumala	přezkoumat	k5eAaPmAgFnS
a	a	k8xC
podpořila	podpořit	k5eAaPmAgFnS
ji	on	k3xPp3gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
říjnu	říjen	k1gInSc6
2013	#num#	k4
byl	být	k5eAaImAgInS
parlament	parlament	k1gInSc1
dotázán	dotázán	k2eAgInSc1d1
<g/>
,	,	kIx,
zda	zda	k8xS
doporučí	doporučit	k5eAaPmIp3nS
voličům	volič	k1gMnPc3
podpořit	podpořit	k5eAaPmF
iniciativu	iniciativa	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
10	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2014	#num#	k4
začala	začít	k5eAaPmAgFnS
dolní	dolní	k2eAgFnSc1d1
komora	komora	k1gFnSc1
na	na	k7c4
téma	téma	k1gNnSc4
iniciativy	iniciativa	k1gFnSc2
diskutovat	diskutovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zelení	Zelený	k1gMnPc1
navrhli	navrhnout	k5eAaPmAgMnP
garanci	garance	k1gFnSc4
ochrany	ochrana	k1gFnSc2
jakýchkoli	jakýkoli	k3yIgInPc2
svazků	svazek	k1gInPc2
před	před	k7c7
penalizací	penalizace	k1gFnSc7
a	a	k8xC
Zelení	zelený	k2eAgMnPc1d1
liberálové	liberál	k1gMnPc1
požadovali	požadovat	k5eAaImAgMnP
ochranu	ochrana	k1gFnSc4
před	před	k7c7
penalizací	penalizace	k1gFnSc7
pro	pro	k7c4
manželství	manželství	k1gNnPc4
a	a	k8xC
jiné	jiný	k2eAgFnPc4d1
formy	forma	k1gFnPc4
svazků	svazek	k1gInPc2
definovaných	definovaný	k2eAgInPc2d1
zákonem	zákon	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Debaty	debata	k1gFnPc4
odmítali	odmítat	k5eAaImAgMnP
zejména	zejména	k6eAd1
zákonodárci	zákonodárce	k1gMnPc1
za	za	k7c4
Švýcarskou	švýcarský	k2eAgFnSc4d1
lidovou	lidový	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
a	a	k8xC
Křesťanští	křesťanštět	k5eAaImIp3nP
demokraté	demokrat	k1gMnPc1
proti	proti	k7c3
Zeleným	zelený	k2eAgMnPc3d1
liberálům	liberál	k1gMnPc3
<g/>
,	,	kIx,
Zeleným	Zelená	k1gFnPc3
<g/>
,	,	kIx,
Sociální	sociální	k2eAgFnSc3d1
demokracii	demokracie	k1gFnSc3
a	a	k8xC
Konzervativním	konzervativní	k2eAgMnPc3d1
demokratům	demokrat	k1gMnPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Liberálové	liberál	k1gMnPc1
byli	být	k5eAaImAgMnP
v	v	k7c6
této	tento	k3xDgFnSc6
věci	věc	k1gFnSc6
rozpolceni	rozpolcen	k2eAgMnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
Švýcarská	švýcarský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
a	a	k8xC
Křesťanští	křesťanský	k2eAgMnPc1d1
demokraté	demokrat	k1gMnPc1
odmítali	odmítat	k5eAaImAgMnP
jakoukoli	jakýkoli	k3yIgFnSc4
formu	forma	k1gFnSc4
homofobii	homofobie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
druhou	druhý	k4xOgFnSc4
stranu	strana	k1gFnSc4
samy	sám	k3xTgFnPc1
některé	některý	k3yIgFnPc1
strany	strana	k1gFnPc1
poukazovaly	poukazovat	k5eAaImAgFnP
na	na	k7c4
fakt	fakt	k1gInSc4
<g/>
,	,	kIx,
že	že	k8xS
iniciativa	iniciativa	k1gFnSc1
je	být	k5eAaImIp3nS
sama	sám	k3xTgFnSc1
ze	z	k7c2
své	svůj	k3xOyFgFnSc2
podstaty	podstata	k1gFnSc2
diskriminační	diskriminační	k2eAgMnSc1d1
<g/>
,	,	kIx,
neboť	neboť	k8xC
uzavírá	uzavírat	k5eAaImIp3nS,k5eAaPmIp3nS
cestu	cesta	k1gFnSc4
budoucímu	budoucí	k2eAgInSc3d1
zpřístupnění	zpřístupnění	k1gNnSc2
institutu	institut	k1gInSc3
manželství	manželství	k1gNnSc2
homosexuálním	homosexuální	k2eAgInPc3d1
párům	pár	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hodně	hodně	k6eAd1
poslanců	poslanec	k1gMnPc2
nazvalo	nazvat	k5eAaPmAgNnS,k5eAaBmAgNnS
Křesťanskou	křesťanský	k2eAgFnSc4d1
demokracii	demokracie	k1gFnSc4
"	"	kIx"
<g/>
zpátečnickou	zpátečnický	k2eAgFnSc7d1
<g/>
"	"	kIx"
stranou	strana	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
89	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Po	po	k7c6
odmítnutí	odmítnutí	k1gNnSc6
obou	dva	k4xCgInPc2
protinávrhů	protinávrh	k1gInPc2
Zelených	Zelených	k2eAgMnPc2d1
a	a	k8xC
Zelených	Zelených	k2eAgMnPc2d1
liberálů	liberál	k1gMnPc2
se	se	k3xPyFc4
Národní	národní	k2eAgFnSc1d1
rada	rada	k1gFnSc1
finálně	finálně	k6eAd1
shodla	shodnout	k5eAaPmAgFnS,k5eAaBmAgFnS
na	na	k7c6
vlastním	vlastní	k2eAgInSc6d1
protinávrhu	protinávrh	k1gInSc6
zpracovaným	zpracovaný	k2eAgInSc7d1
Komisí	komise	k1gFnSc7
pro	pro	k7c4
ekonomické	ekonomický	k2eAgFnPc4d1
a	a	k8xC
daňové	daňový	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ta	ten	k3xDgFnSc1
měla	mít	k5eAaImAgFnS
na	na	k7c4
otázku	otázka	k1gFnSc4
heteronormativní	heteronormativní	k2eAgFnSc2d1
definice	definice	k1gFnSc2
manželství	manželství	k1gNnSc2
obdobný	obdobný	k2eAgInSc1d1
názor	názor	k1gInSc1
<g/>
,	,	kIx,
a	a	k8xC
proto	proto	k8xC
jí	on	k3xPp3gFnSc3
také	také	k9
z	z	k7c2
iniciativy	iniciativa	k1gFnSc2
vyňala	vynít	k5eAaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Protinávrh	protinávrh	k1gInSc1
byl	být	k5eAaImAgInS
přijat	přijmout	k5eAaPmNgInS
poměrem	poměr	k1gInSc7
hlasů	hlas	k1gInPc2
102	#num#	k4
<g/>
:	:	kIx,
<g/>
86	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidová	lidový	k2eAgFnSc1d1
iniciativa	iniciativa	k1gFnSc1
se	se	k3xPyFc4
tedy	tedy	k8xC
setkala	setkat	k5eAaPmAgFnS
s	s	k7c7
odmítnutím	odmítnutí	k1gNnSc7
a	a	k8xC
švýcarskému	švýcarský	k2eAgInSc3d1
elektorátu	elektorát	k1gInSc3
bylo	být	k5eAaImAgNnS
doporučeno	doporučen	k2eAgNnSc1d1
s	s	k7c7
ní	on	k3xPp3gFnSc7
nesouhlasit	souhlasit	k5eNaImF
a	a	k8xC
přijmout	přijmout	k5eAaPmF
protinávrh	protinávrh	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
90	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Rada	rada	k1gFnSc1
kantonů	kanton	k1gInPc2
(	(	kIx(
<g/>
horní	horní	k2eAgFnSc1d1
komora	komora	k1gFnSc1
<g/>
)	)	kIx)
protinávrh	protinávrh	k1gInSc1
odsouhlasený	odsouhlasený	k2eAgInSc1d1
10	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2014	#num#	k4
dolní	dolní	k2eAgFnSc7d1
komorou	komora	k1gFnSc7
přijala	přijmout	k5eAaPmAgFnS
4	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2015	#num#	k4
poměrem	poměr	k1gInSc7
hlasů	hlas	k1gInPc2
24	#num#	k4
<g/>
:	:	kIx,
<g/>
19	#num#	k4
<g/>
,	,	kIx,
čímž	což	k3yQnSc7,k3yRnSc7
se	se	k3xPyFc4
celá	celý	k2eAgFnSc1d1
podstata	podstata	k1gFnSc1
křesťanskodemokratické	křesťanskodemokratický	k2eAgFnSc2d1
iniciativy	iniciativa	k1gFnSc2
de	de	k?
facto	facto	k1gNnSc1
zrušila	zrušit	k5eAaPmAgFnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
91	#num#	k4
<g/>
]	]	kIx)
Také	také	k9
Rada	rada	k1gFnSc1
kantonů	kanton	k1gInPc2
hojně	hojně	k6eAd1
poukazovala	poukazovat	k5eAaImAgFnS
na	na	k7c4
kontroverzní	kontroverzní	k2eAgInSc4d1
požadavek	požadavek	k1gInSc4
ústavní	ústavní	k2eAgFnSc2d1
definice	definice	k1gFnSc2
manželství	manželství	k1gNnSc2
nepřipouštějící	připouštějící	k2eNgMnSc1d1
jeho	jeho	k3xOp3gNnSc2,k3xPp3gNnSc2
zpřístupnění	zpřístupnění	k1gNnSc2
homosexuálním	homosexuální	k2eAgInPc3d1
párům	pár	k1gInPc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Idea	idea	k1gFnSc1
rovných	rovný	k2eAgNnPc2d1
fiskálních	fiskální	k2eAgNnPc2d1
a	a	k8xC
sociálních	sociální	k2eAgNnPc2d1
práv	právo	k1gNnPc2
pro	pro	k7c4
manželské	manželský	k2eAgFnPc4d1
i	i	k8xC
nemanželské	manželský	k2eNgFnPc4d1
páry	pára	k1gFnPc4
nebyla	být	k5eNaImAgNnP
nijak	nijak	k6eAd1
odmítána	odmítán	k2eAgNnPc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Hlasování	hlasování	k1gNnSc1
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2016	#num#	k4
vyzvali	vyzvat	k5eAaPmAgMnP
křesťanští	křesťanský	k2eAgMnPc1d1
demokraté	demokrat	k1gMnPc1
švýcarské	švýcarský	k2eAgFnSc2d1
voliči	volič	k1gMnPc1
k	k	k7c3
účasti	účast	k1gFnSc3
v	v	k7c6
referendu	referendum	k1gNnSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
v	v	k7c6
němž	jenž	k3xRgInSc6
se	se	k3xPyFc4
rozhodovalo	rozhodovat	k5eAaImAgNnS
o	o	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
zda	zda	k8xS
by	by	kYmCp3nS
mělo	mít	k5eAaImAgNnS
být	být	k5eAaImF
manželství	manželství	k1gNnSc1
definované	definovaný	k2eAgNnSc1d1
jako	jako	k8xC,k8xS
jedinečný	jedinečný	k2eAgInSc1d1
trvalý	trvalý	k2eAgInSc1d1
svazek	svazek	k1gInSc1
jednoho	jeden	k4xCgMnSc2
muže	muž	k1gMnSc2
a	a	k8xC
jedné	jeden	k4xCgFnSc2
ženy	žena	k1gFnSc2
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
nesmí	smět	k5eNaImIp3nS
být	být	k5eAaImF
znevýhodňován	znevýhodňovat	k5eAaImNgInS
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
jinými	jiný	k2eAgMnPc7d1
životním	životnět	k5eAaImIp1nS
styly	styl	k1gInPc4
<g/>
[	[	kIx(
<g/>
94	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
čímž	což	k3yRnSc7,k3yQnSc7
by	by	kYmCp3nP
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
přijetí	přijetí	k1gNnSc3
ústavního	ústavní	k2eAgInSc2d1
zákazu	zákaz	k1gInSc2
stejnopohlavního	stejnopohlavní	k2eAgNnSc2d1
manželství	manželství	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Pokud	pokud	k8xS
jde	jít	k5eAaImIp3nS
o	o	k7c4
parlamentní	parlamentní	k2eAgFnPc4d1
strany	strana	k1gFnPc4
<g/>
,	,	kIx,
tak	tak	k6eAd1
křesťanští	křesťanský	k2eAgMnPc1d1
demokraté	demokrat	k1gMnPc1
(	(	kIx(
<g/>
vyjma	vyjma	k7c2
Mladých	mladý	k2eAgMnPc2d1
křesťanských	křesťanský	k2eAgMnPc2d1
demokratů	demokrat	k1gMnPc2
Kantonu	Kanton	k1gInSc2
Curych	Curych	k1gInSc1
a	a	k8xC
Kantonu	Kanton	k1gInSc6
Ženeva	Ženeva	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
kteří	který	k3yIgMnPc1,k3yQgMnPc1,k3yRgMnPc1
odmítali	odmítat	k5eAaImAgMnP
tuto	tento	k3xDgFnSc4
iniciativu	iniciativa	k1gFnSc4
<g/>
[	[	kIx(
<g/>
95	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
96	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
<g/>
,	,	kIx,
národně	národně	k6eAd1
konzervativní	konzervativní	k2eAgFnSc1d1
Švýcarská	švýcarský	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
a	a	k8xC
konzervativní	konzervativní	k2eAgFnSc1d1
Evangelická	evangelický	k2eAgFnSc1d1
lidová	lidový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
vedly	vést	k5eAaImAgFnP
kampaň	kampaň	k1gFnSc4
za	za	k7c4
hlasování	hlasování	k1gNnSc4
"	"	kIx"
<g/>
Ano	ano	k9
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Naopak	naopak	k6eAd1
sociální	sociální	k2eAgMnPc1d1
demokraté	demokrat	k1gMnPc1
<g/>
,	,	kIx,
liberálové	liberál	k1gMnPc1
<g/>
,	,	kIx,
zelení	zelený	k2eAgMnPc1d1
<g/>
,	,	kIx,
konzervativní	konzervativní	k2eAgMnPc1d1
demokraté	demokrat	k1gMnPc1
a	a	k8xC
zelení	zelený	k2eAgMnPc1d1
liberálové	liberál	k1gMnPc1
odmítali	odmítat	k5eAaImAgMnP
text	text	k1gInSc4
ústavní	ústavní	k2eAgFnSc2d1
změny	změna	k1gFnSc2
a	a	k8xC
vyzývali	vyzývat	k5eAaImAgMnP
spolu	spolu	k6eAd1
s	s	k7c7
Amnesty	Amnest	k1gInPc7
International	International	k1gFnSc2
Switzerland	Switzerland	k1gInSc1
<g/>
,	,	kIx,
Economiesuisse	Economiesuisse	k1gFnSc1
(	(	kIx(
<g/>
zaměstnanecká	zaměstnanecký	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Švýcarským	švýcarský	k2eAgInSc7d1
svazem	svaz	k1gInSc7
odborů	odbor	k1gInPc2
a	a	k8xC
Operation	Operation	k1gInSc4
Libero	Libero	k1gNnSc4
k	k	k7c3
hlasování	hlasování	k1gNnSc3
"	"	kIx"
<g/>
Ne	Ne	kA
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Měsíc	měsíc	k1gInSc1
před	před	k7c7
konáním	konání	k1gNnSc7
referenda	referendum	k1gNnSc2
proběhlo	proběhnout	k5eAaPmAgNnS
několik	několik	k4yIc1
průzkumů	průzkum	k1gInPc2
<g/>
,	,	kIx,
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
ukazovaly	ukazovat	k5eAaImAgFnP
67	#num#	k4
%	%	kIx~
podporu	podpora	k1gFnSc4
(	(	kIx(
<g/>
22	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
2016	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
53	#num#	k4
%	%	kIx~
podporu	podpora	k1gFnSc4
(	(	kIx(
<g/>
17	#num#	k4
<g/>
.	.	kIx.
únor	únor	k1gInSc1
2016	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
28	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2016	#num#	k4
byla	být	k5eAaImAgFnS
iniciativa	iniciativa	k1gFnSc1
odmítnutá	odmítnutý	k2eAgFnSc1d1
nejtěsnější	těsný	k2eAgFnSc4d3
možnou	možný	k2eAgFnSc4d1
většinu	většina	k1gFnSc4
50,8	50,8	k4
%	%	kIx~
(	(	kIx(
<g/>
1	#num#	k4
664	#num#	k4
217	#num#	k4
proti	proti	k7c3
1	#num#	k4
609	#num#	k4
328	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Voličská	voličský	k2eAgFnSc1d1
základna	základna	k1gFnSc1
se	se	k3xPyFc4
během	během	k7c2
hlasování	hlasování	k1gNnSc2
rozdělila	rozdělit	k5eAaPmAgFnS
na	na	k7c4
dva	dva	k4xCgInPc4
tábory	tábor	k1gInPc4
<g/>
,	,	kIx,
z	z	k7c2
nichž	jenž	k3xRgMnPc2
jedny	jeden	k4xCgFnPc1
byly	být	k5eAaImAgInP
téměř	téměř	k6eAd1
zajedno	zajedno	k6eAd1
v	v	k7c6
hlasování	hlasování	k1gNnSc6
"	"	kIx"
<g/>
Ano	ano	k9
<g/>
"	"	kIx"
a	a	k8xC
druzí	druhý	k4xOgMnPc1
v	v	k7c6
hlasování	hlasování	k1gNnSc6
"	"	kIx"
<g/>
Ne	ne	k9
<g/>
"	"	kIx"
<g/>
.	.	kIx.
</s>
<s desamb="1">
Většina	většina	k1gFnSc1
kantonů	kanton	k1gInPc2
hlasovala	hlasovat	k5eAaImAgFnS
pro	pro	k7c4
přijetí	přijetí	k1gNnSc4
iniciativy	iniciativa	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proti	proti	k7c3
iniciativě	iniciativa	k1gFnSc3
se	se	k3xPyFc4
vyslovily	vyslovit	k5eAaPmAgInP
kantony	kanton	k1gInPc1
Ženeva	Ženeva	k1gFnSc1
<g/>
,	,	kIx,
Vaud	Vaud	k1gInSc1
<g/>
,	,	kIx,
Bern	Bern	k1gInSc1
<g/>
,	,	kIx,
Curych	Curych	k1gInSc1
<g/>
,	,	kIx,
Grisons	Grisons	k1gInSc1
<g/>
,	,	kIx,
Basel-Stadt	Basel-Stadt	k1gInSc1
<g/>
,	,	kIx,
Basel-Landschaft	Basel-Landschaft	k1gInSc1
a	a	k8xC
Appenzel	Appenzel	k1gFnSc1
Ausserhoden	Ausserhodna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
98	#num#	k4
<g/>
]	]	kIx)
Díky	díky	k7c3
odmítnutí	odmítnutí	k1gNnSc3
výlučnosti	výlučnost	k1gFnSc2
manželství	manželství	k1gNnSc2
jako	jako	k8xS,k8xC
svazku	svazek	k1gInSc2
muže	muž	k1gMnSc2
a	a	k8xC
ženy	žena	k1gFnSc2
se	se	k3xPyFc4
otevřela	otevřít	k5eAaPmAgFnS
cesta	cesta	k1gFnSc1
iniciativě	iniciativa	k1gFnSc3
Zelené	Zelené	k2eAgFnSc2d1
liberální	liberální	k2eAgFnSc2d1
strany	strana	k1gFnSc2
usilující	usilující	k2eAgFnPc1d1
naopak	naopak	k6eAd1
o	o	k7c4
legalizaci	legalizace	k1gFnSc4
stejnopohlavního	stejnopohlavní	k2eAgNnSc2d1
manželství	manželství	k1gNnSc2
<g/>
,	,	kIx,
které	který	k3yRgNnSc1,k3yIgNnSc1,k3yQgNnSc1
je	být	k5eAaImIp3nS
teď	teď	k6eAd1
momentálně	momentálně	k6eAd1
v	v	k7c6
procesu	proces	k1gInSc6
příprav	příprava	k1gFnPc2
a	a	k8xC
politických	politický	k2eAgFnPc2d1
diskuzí	diskuze	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Veřejné	veřejný	k2eAgNnSc1d1
mínění	mínění	k1gNnSc1
</s>
<s>
Podle	podle	k7c2
průzkumu	průzkum	k1gInSc2
Ifop	Ifop	k1gInSc4
z	z	k7c2
května	květen	k1gInSc2
2013	#num#	k4
by	by	k9
63	#num#	k4
%	%	kIx~
Švýcarů	Švýcar	k1gMnPc2
souhlasilo	souhlasit	k5eAaImAgNnS
s	s	k7c7
legalizací	legalizace	k1gFnSc7
stejnopohlavního	stejnopohlavní	k2eAgNnSc2d1
manželství	manželství	k1gNnSc2
a	a	k8xC
adopce	adopce	k1gFnSc2
dětí	dítě	k1gFnPc2
homosexuálními	homosexuální	k2eAgInPc7d1
páry	pár	k1gInPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
99	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Poté	poté	k6eAd1
<g/>
,	,	kIx,
co	co	k9
Výbor	výbor	k1gInSc1
Národní	národní	k2eAgFnSc2d1
rady	rada	k1gFnSc2
pro	pro	k7c4
právní	právní	k2eAgFnPc4d1
záležitosti	záležitost	k1gFnPc4
přijal	přijmout	k5eAaPmAgMnS
rezoluci	rezoluce	k1gFnSc4
o	o	k7c6
stejnopohlavním	stejnopohlavní	k2eAgNnSc6d1
manželství	manželství	k1gNnSc1
<g/>
,	,	kIx,
byly	být	k5eAaImAgFnP
22	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2015	#num#	k4
publikovány	publikovat	k5eAaBmNgInP
dva	dva	k4xCgInPc1
průzkumy	průzkum	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
jednom	jeden	k4xCgInSc6
z	z	k7c2
nich	on	k3xPp3gInPc2
se	s	k7c7
54	#num#	k4
%	%	kIx~
respondentů	respondent	k1gMnPc2
vyslovilo	vyslovit	k5eAaPmAgNnS
pro	pro	k7c4
stejnopohlavní	stejnopohlavní	k2eAgNnPc4d1
manželství	manželství	k1gNnPc4
a	a	k8xC
adopce	adopce	k1gFnPc4
dětí	dítě	k1gFnPc2
homosexuálními	homosexuální	k2eAgInPc7d1
páry	pár	k1gInPc7
<g/>
[	[	kIx(
<g/>
100	#num#	k4
<g/>
]	]	kIx)
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
v	v	k7c6
tom	ten	k3xDgNnSc6
druhém	druhý	k4xOgNnSc6
to	ten	k3xDgNnSc4
bylo	být	k5eAaImAgNnS
71	#num#	k4
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
101	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Průzkum	průzkum	k1gInSc1
uskutečněný	uskutečněný	k2eAgInSc4d1
mezi	mezi	k7c7
dubnem	duben	k1gInSc7
a	a	k8xC
květnem	květen	k1gInSc7
2016	#num#	k4
ukázal	ukázat	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
69	#num#	k4
%	%	kIx~
Švýcarů	Švýcar	k1gMnPc2
podporuje	podporovat	k5eAaImIp3nS
stejnopohlavní	stejnopohlavní	k2eAgNnSc1d1
manželství	manželství	k1gNnSc1
<g/>
,	,	kIx,
25	#num#	k4
%	%	kIx~
je	být	k5eAaImIp3nS
proti	proti	k7c3
a	a	k8xC
6	#num#	k4
%	%	kIx~
je	být	k5eAaImIp3nS
nerozhodnutých	rozhodnutý	k2eNgFnPc2d1
<g/>
.	.	kIx.
94	#num#	k4
%	%	kIx~
voličů	volič	k1gMnPc2
Zelených	Zelených	k2eAgMnPc2d1
podporovalo	podporovat	k5eAaImAgNnS
tuto	tento	k3xDgFnSc4
legislativu	legislativa	k1gFnSc4
ve	v	k7c6
srovnání	srovnání	k1gNnSc6
s	s	k7c7
59	#num#	k4
%	%	kIx~
voličů	volič	k1gMnPc2
Švýcarské	švýcarský	k2eAgFnSc2d1
lidové	lidový	k2eAgFnSc2d1
strany	strana	k1gFnSc2
a	a	k8xC
63	#num#	k4
%	%	kIx~
voličů	volič	k1gMnPc2
Křesťanské	křesťanský	k2eAgFnSc2d1
demokracie	demokracie	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
102	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
103	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jiný	jiný	k2eAgInSc1d1
výzkum	výzkum	k1gInSc1
Tamedia	Tamedium	k1gNnSc2
uskutečněný	uskutečněný	k2eAgInSc1d1
5	#num#	k4
<g/>
.	.	kIx.
a	a	k8xC
6	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2017	#num#	k4
shledal	shledat	k5eAaPmAgMnS
<g/>
,	,	kIx,
že	že	k8xS
45	#num#	k4
%	%	kIx~
Švýcarů	Švýcar	k1gMnPc2
podporuje	podporovat	k5eAaImIp3nS
jak	jak	k6eAd1
stejnopohlavní	stejnopohlavní	k2eAgNnSc1d1
manželství	manželství	k1gNnSc1
<g/>
,	,	kIx,
tak	tak	k6eAd1
i	i	k9
adopce	adopce	k1gFnSc1
dětí	dítě	k1gFnPc2
homosexuálními	homosexuální	k2eAgInPc7d1
páry	pár	k1gInPc7
<g/>
.	.	kIx.
27	#num#	k4
%	%	kIx~
bylo	být	k5eAaImAgNnS
pouze	pouze	k6eAd1
pro	pro	k7c4
stejnopohlavní	stejnopohlavní	k2eAgNnPc4d1
manželství	manželství	k1gNnPc4
<g/>
,	,	kIx,
3	#num#	k4
%	%	kIx~
pouze	pouze	k6eAd1
pro	pro	k7c4
homoparentální	homoparentální	k2eAgNnSc4d1
osvojování	osvojování	k1gNnSc4
a	a	k8xC
24	#num#	k4
%	%	kIx~
proti	proti	k7c3
obojím	obojí	k4xRgMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
104	#num#	k4
<g/>
]	]	kIx)
Dalo	dát	k5eAaPmAgNnS
by	by	kYmCp3nP
se	se	k3xPyFc4
tedy	tedy	k9
ale	ale	k9
říci	říct	k5eAaPmF
<g/>
,	,	kIx,
že	že	k8xS
by	by	k9
72	#num#	k4
%	%	kIx~
většina	většina	k1gFnSc1
Švýcarů	Švýcar	k1gMnPc2
neměla	mít	k5eNaImAgFnS
problém	problém	k1gInSc4
se	s	k7c7
stejnopohlavním	stejnopohlavní	k2eAgNnSc7d1
manželstvím	manželství	k1gNnSc7
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
22	#num#	k4
%	%	kIx~
bylo	být	k5eAaImAgNnS
jasně	jasně	k6eAd1
proti	proti	k7c3
<g/>
.	.	kIx.
</s>
<s desamb="1">
U	u	k7c2
voličů	volič	k1gMnPc2
zelených	zelený	k2eAgMnPc2d1
<g/>
,	,	kIx,
sociálních	sociální	k2eAgMnPc2d1
demokratů	demokrat	k1gMnPc2
a	a	k8xC
u	u	k7c2
zelených	zelený	k2eAgMnPc2d1
liberálů	liberál	k1gMnPc2
byla	být	k5eAaImAgFnS
zaznamenána	zaznamenat	k5eAaPmNgFnS
větší	veliký	k2eAgFnSc1d2
podpora	podpora	k1gFnSc1
<g/>
:	:	kIx,
88	#num#	k4
%	%	kIx~
pro	pro	k7c4
<g/>
,	,	kIx,
9	#num#	k4
%	%	kIx~
proti	proti	k7c3
a	a	k8xC
3	#num#	k4
%	%	kIx~
nerozhodnutých	rozhodnutý	k2eNgInPc2d1
<g/>
.	.	kIx.
76	#num#	k4
%	%	kIx~
voličů	volič	k1gInPc2
liberálů	liberál	k1gMnPc2
podporovalo	podporovat	k5eAaImAgNnS
stejnopohlavní	stejnopohlavní	k2eAgNnSc4d1
manželství	manželství	k1gNnSc4
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
22	#num#	k4
%	%	kIx~
bylo	být	k5eAaImAgNnS
proti	proti	k7c3
<g/>
.	.	kIx.
66	#num#	k4
%	%	kIx~
voličů	volič	k1gMnPc2
křesťanských	křesťanský	k2eAgMnPc2d1
demokratů	demokrat	k1gMnPc2
a	a	k8xC
56	#num#	k4
%	%	kIx~
voličů	volič	k1gMnPc2
švýcarských	švýcarský	k2eAgMnPc2d1
lidovců	lidovec	k1gMnPc2
bylo	být	k5eAaImAgNnS
pro	pro	k7c4
stejnopohlavní	stejnopohlavní	k2eAgNnSc4d1
manželství	manželství	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
105	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Průzkum	průzkum	k1gInSc1
Pew	Pew	k1gMnSc1
Research	Research	k1gMnSc1
Center	centrum	k1gNnPc2
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yRgMnSc1,k3yIgMnSc1
proběhl	proběhnout	k5eAaPmAgInS
mezi	mezi	k7c7
dubnem	duben	k1gInSc7
a	a	k8xC
srpnem	srpen	k1gInSc7
2018	#num#	k4
ukázal	ukázat	k5eAaPmAgInS
<g/>
,	,	kIx,
že	že	k8xS
75	#num#	k4
%	%	kIx~
Švýcarů	Švýcar	k1gMnPc2
podporuje	podporovat	k5eAaImIp3nS
stejnopohlavní	stejnopohlavní	k2eAgNnSc1d1
manželství	manželství	k1gNnSc1
<g/>
,	,	kIx,
zatímco	zatímco	k8xS
24	#num#	k4
%	%	kIx~
je	být	k5eAaImIp3nS
proti	proti	k7c3
a	a	k8xC
1	#num#	k4
%	%	kIx~
neví	vědět	k5eNaImIp3nS
<g/>
,	,	kIx,
nebo	nebo	k8xC
nechce	chtít	k5eNaImIp3nS
odpovědět	odpovědět	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
106	#num#	k4
<g/>
]	]	kIx)
Co	co	k3yInSc1,k3yQnSc1,k3yRnSc1
se	se	k3xPyFc4
týče	týkat	k5eAaImIp3nS
náboženského	náboženský	k2eAgNnSc2d1
vyznání	vyznání	k1gNnSc2
<g/>
,	,	kIx,
tak	tak	k9
89	#num#	k4
%	%	kIx~
podpora	podpora	k1gFnSc1
stejnopohlavního	stejnopohlavní	k2eAgNnSc2d1
manželství	manželství	k1gNnSc2
je	být	k5eAaImIp3nS
u	u	k7c2
respondentů	respondent	k1gMnPc2
bez	bez	k7c2
vyznání	vyznání	k1gNnSc2
<g/>
,	,	kIx,
80	#num#	k4
%	%	kIx~
u	u	k7c2
nepraktikujících	praktikující	k2eNgMnPc2d1
křesťanů	křesťan	k1gMnPc2
a	a	k8xC
58	#num#	k4
%	%	kIx~
u	u	k7c2
praktikujících	praktikující	k2eAgMnPc2d1
křesťanů	křesťan	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
107	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
V	v	k7c6
tomto	tento	k3xDgInSc6
článku	článek	k1gInSc6
byl	být	k5eAaImAgInS
použit	použít	k5eAaPmNgInS
překlad	překlad	k1gInSc1
textu	text	k1gInSc2
z	z	k7c2
článku	článek	k1gInSc2
Registered	Registered	k1gMnSc1
partnership	partnership	k1gMnSc1
in	in	k?
Switzerland	Switzerland	k1gInSc1
na	na	k7c6
anglické	anglický	k2eAgFnSc6d1
Wikipedii	Wikipedie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s>
↑	↑	k?
Parliament	Parliament	k1gMnSc1
gives	gives	k1gMnSc1
green	green	k2eAgMnSc1d1
light	light	k5eAaPmF
to	ten	k3xDgNnSc4
"	"	kIx"
<g/>
gay	gay	k1gMnSc1
marriages	marriages	k1gMnSc1
<g/>
"	"	kIx"
<g/>
↑	↑	k?
Parliament	Parliament	k1gMnSc1
gives	gives	k1gMnSc1
its	its	k?
blessing	blessing	k1gInSc1
to	ten	k3xDgNnSc4
gay	gay	k1gMnSc1
couples	couples	k1gMnSc1
<g/>
↑	↑	k?
Fight	Fight	k1gMnSc1
goes	goes	k6eAd1
on	on	k3xPp3gMnSc1
for	forum	k1gNnPc2
gay	gay	k1gMnSc1
couples	couples	k1gMnSc1
<g/>
↑	↑	k?
Government	Government	k1gMnSc1
presses	presses	k1gMnSc1
for	forum	k1gNnPc2
gay-couple	gay-couple	k6eAd1
rights	rights	k6eAd1
<g/>
↑	↑	k?
Gay	gay	k1gMnSc1
couples	couples	k1gMnSc1
win	win	k?
partnership	partnership	k1gMnSc1
rights	rights	k6eAd1
<g/>
↑	↑	k?
First	First	k1gInSc1
same-sex	same-sex	k1gInSc1
union	union	k1gInSc1
registered	registered	k1gInSc1
in	in	k?
Switzerland	Switzerland	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Retrouvez	Retrouvez	k1gInSc1
le	le	k?
débat	débat	k1gInSc1
du	du	k?
National	National	k1gFnSc2
sur	sur	k?
un	un	k?
«	«	k?
<g/>
pacs	pacs	k1gInSc1
<g/>
»	»	k?
suisse	suisse	k1gFnSc2
Archivováno	archivován	k2eAgNnSc1d1
9	#num#	k4
<g/>
.	.	kIx.
7	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Les	les	k1gInSc1
couples	couples	k1gMnSc1
hétérosexuels	hétérosexuelsa	k1gFnPc2
devraient	devraient	k1gMnSc1
aussi	ausse	k1gFnSc4
pouvoir	pouvoira	k1gFnPc2
se	se	k3xPyFc4
pacser	pacser	k1gInSc1
en	en	k?
Suisse	Suisse	k1gFnSc2
<g/>
↑	↑	k?
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Adoptionsrecht	Adoptionsrecht	k2eAgInSc1d1
wieder	wieder	k1gInSc1
im	im	k?
Fokus	fokus	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
10	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2008	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Article	Article	k1gInSc1
27	#num#	k4
<g/>
:	:	kIx,
«	«	k?
<g/>
Partner	partner	k1gMnSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
children	childrna	k1gFnPc2
<g/>
»	»	k?
<g/>
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Le	Le	k1gFnSc1
National	National	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
ne	ne	k9
veut	veut	k2eAgInSc1d1
pas	pas	k1gInSc1
voir	voira	k1gFnPc2
les	les	k1gInSc1
couples	couples	k1gMnSc1
homosexuels	homosexuelsa	k1gFnPc2
adopter	adopter	k1gMnSc1
<g/>
,	,	kIx,
Swissinfo	Swissinfo	k1gMnSc1
<g/>
,	,	kIx,
accessed	accessed	k1gMnSc1
on	on	k3xPp3gMnSc1
15	#num#	k4
December	December	k1gInSc1
2012	#num#	k4
<g/>
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Coup	coup	k1gInSc1
de	de	k?
pouce	pouka	k1gFnSc3
des	des	k1gNnPc2
Sénateurs	Sénateurs	k1gInSc1
à	à	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
adoption	adoption	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
22	#num#	k4
<g/>
.	.	kIx.
3	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
360	#num#	k4
<g/>
,	,	kIx,
accessed	accessed	k1gInSc1
on	on	k3xPp3gInSc1
15	#num#	k4
December	December	k1gInSc1
2012	#num#	k4
<g/>
↑	↑	k?
Schweizer	Schweizer	k1gMnSc1
Regierung	Regierung	k1gMnSc1
gegen	gegna	k1gFnPc2
Adoptionsrecht	Adoptionsrecht	k1gMnSc1
für	für	k?
Homo-Paare	Homo-Paar	k1gMnSc5
<g/>
.	.	kIx.
www.queer.de	www.queer.de	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Queer	Queer	k1gInSc1
<g/>
.	.	kIx.
<g/>
de	de	k?
<g/>
,	,	kIx,
22	#num#	k4
February	Februara	k1gFnSc2
2012	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Le	Le	k1gFnSc1
Conseil	Conseil	k1gInSc1
des	des	k1gNnSc6
Etats	Etats	k1gInSc1
accepte	accepit	k5eAaPmRp2nP
l	l	kA
<g/>
'	'	kIx"
<g/>
adoption	adoption	k1gInSc1
des	des	k1gNnSc1
couples	couples	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
homosexuels	homosexuels	k1gInSc1
<g/>
,	,	kIx,
Le	Le	k1gMnSc1
Matin	Matin	k1gMnSc1
<g/>
↑	↑	k?
Swiss	Swiss	k1gInSc1
lawmakers	lawmakers	k6eAd1
vote	votat	k5eAaPmIp3nS
to	ten	k3xDgNnSc1
allow	allow	k?
some	somat	k5eAaPmIp3nS
gays	gays	k6eAd1
to	ten	k3xDgNnSc1
adopt	adopt	k5eAaPmF
Archivováno	archivovat	k5eAaBmNgNnS
26	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2020	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
France	Franc	k1gMnSc4
<g/>
24	#num#	k4
<g/>
,	,	kIx,
15	#num#	k4
December	December	k1gInSc1
2012	#num#	k4
<g/>
↑	↑	k?
Rainbow	Rainbow	k1gMnSc1
families	families	k1gMnSc1
<g/>
:	:	kIx,
Gays	Gays	k1gInSc1
granted	granted	k1gInSc1
more	mor	k1gInSc5
adoption	adoption	k1gInSc1
rights	rights	k1gInSc1
<g/>
,	,	kIx,
Swissinfo	Swissinfo	k1gNnSc1
<g/>
,	,	kIx,
15	#num#	k4
December	December	k1gInSc1
2012	#num#	k4
<g/>
↑	↑	k?
Motion	Motion	k1gInSc1
CAJ-CE	CAJ-CE	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Droit	Droit	k1gInSc1
de	de	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
adoption	adoption	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mê	Mê	k1gMnSc1
chances	chances	k1gMnSc1
pour	pour	k1gMnSc1
toutes	toutes	k1gMnSc1
les	les	k1gInSc4
familles	familles	k1gMnSc1
<g/>
,	,	kIx,
Council	Council	k1gMnSc1
of	of	k?
States	States	k1gMnSc1
<g/>
,	,	kIx,
retrieved	retrieved	k1gMnSc1
on	on	k3xPp3gMnSc1
21	#num#	k4
April	April	k1gInSc1
2013	#num#	k4
<g/>
↑	↑	k?
Suisse	Suissa	k1gFnSc6
<g/>
:	:	kIx,
Le	Le	k1gFnSc6
gouvernement	gouvernement	k1gNnSc4
propose	proposa	k1gFnSc3
d	d	k?
<g/>
’	’	k?
<g/>
ouvrir	ouvrir	k1gMnSc1
l	l	kA
<g/>
’	’	k?
<g/>
adoption	adoption	k1gInSc1
aux	aux	k?
couples	couples	k1gInSc1
de	de	k?
mê	mê	k5eAaPmIp3nS
sexe	sex	k1gInSc5
<g/>
.	.	kIx.
yagg	yagga	k1gFnPc2
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Yagg	Yagga	k1gFnPc2
<g/>
,	,	kIx,
2	#num#	k4
December	December	k1gInSc1
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Bundesrat	Bundesrat	k1gMnSc1
will	wilnout	k5eAaPmAgMnS
Stiefkindadoption	Stiefkindadoption	k1gInSc4
ermöglichen	ermöglichen	k2eAgInSc4d1
<g/>
.	.	kIx.
queer	queer	k1gInSc4
<g/>
.	.	kIx.
<g/>
ch	ch	k0
<g/>
.	.	kIx.
</s>
<s desamb="1">
Queer	Queer	k1gInSc1
<g/>
.	.	kIx.
<g/>
ch	ch	k0
<g/>
,	,	kIx,
28	#num#	k4
November	November	k1gInSc1
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Adoption	Adoption	k1gInSc1
<g/>
:	:	kIx,
les	les	k1gInSc1
opposants	opposants	k1gInSc1
en	en	k?
ordre	ordre	k1gInSc1
de	de	k?
bataille	bataille	k1gInSc1
<g/>
.	.	kIx.
360	#num#	k4
<g/>
.	.	kIx.
<g/>
ch	ch	k0
<g/>
.	.	kIx.
360	#num#	k4
<g/>
.	.	kIx.
<g/>
ch	ch	k0
<g/>
,	,	kIx,
7	#num#	k4
September	September	k1gInSc1
2014	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Ständeratskommission	Ständeratskommission	k1gInSc1
befürwortet	befürwortet	k1gMnSc1
Adoptionen	Adoptionen	k1gInSc1
<g/>
.	.	kIx.
queer	queer	k1gInSc1
<g/>
.	.	kIx.
<g/>
ch	ch	k0
<g/>
.	.	kIx.
</s>
<s desamb="1">
Queer	Queer	k1gInSc1
<g/>
.	.	kIx.
<g/>
ch	ch	k0
<g/>
,	,	kIx,
12	#num#	k4
January	Januara	k1gFnSc2
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Switzerland	Switzerland	k1gInSc1
takes	takes	k1gInSc1
step	step	k1gInSc1
towards	towards	k1gInSc1
adoption	adoption	k1gInSc4
equality	equalita	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Local	Local	k1gMnSc1
<g/>
,	,	kIx,
9	#num#	k4
March	March	k1gInSc1
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Homosexuelle	Homosexuelle	k1gInSc1
sollen	sollen	k2eAgMnSc1d1
Stiefkinder	Stiefkinder	k1gMnSc1
adoptieren	adoptierna	k1gFnPc2
dürfen	dürfen	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tages-Anzeiger	Tages-Anzeiger	k1gInSc1
<g/>
,	,	kIx,
March	March	k1gInSc1
8	#num#	k4
<g/>
,	,	kIx,
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Nationalratskommission	Nationalratskommission	k1gInSc1
für	für	k?
Adoptionsrecht	Adoptionsrecht	k1gInSc1
<g/>
.	.	kIx.
queer	queer	k1gInSc1
<g/>
.	.	kIx.
<g/>
ch	ch	k0
<g/>
.	.	kIx.
</s>
<s desamb="1">
Queer	Queer	k1gInSc1
<g/>
.	.	kIx.
<g/>
de	de	k?
<g/>
,	,	kIx,
13	#num#	k4
May	May	k1gMnSc1
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Schweiz	Schweiz	k1gInSc1
<g/>
:	:	kIx,
Stiefkindadoption	Stiefkindadoption	k1gInSc1
wird	wird	k1gMnSc1
Gesetz	Gesetz	k1gMnSc1
<g/>
.	.	kIx.
m-maenner	m-maenner	k1gMnSc1
<g/>
.	.	kIx.
<g/>
de	de	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Männer	Männra	k1gFnPc2
<g/>
,	,	kIx,
14	#num#	k4
May	May	k1gMnSc1
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Oui	Oui	k1gFnSc2
à	à	k?
l	l	kA
<g/>
’	’	k?
<g/>
adoption	adoption	k1gInSc1
par	para	k1gFnPc2
le	le	k?
conjoint	conjoint	k1gMnSc1
de	de	k?
mê	mê	k5eAaPmIp3nS
sexe	sex	k1gInSc5
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
360	#num#	k4
<g/>
°	°	k?
<g/>
,	,	kIx,
14	#num#	k4
May	May	k1gMnSc1
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Swiss	Swiss	k1gInSc1
Parliament	Parliament	k1gMnSc1
votes	votes	k1gMnSc1
in	in	k?
favor	favor	k?
of	of	k?
stepchild	stepchild	k1gInSc1
adoption	adoption	k1gInSc1
<g/>
.	.	kIx.
nelfa	nelf	k1gMnSc2
<g/>
.	.	kIx.
<g/>
org	org	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Touzain	Touzain	k1gInSc1
<g/>
,	,	kIx,
François	François	k1gFnSc1
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Oui	Oui	k1gFnSc1
<g/>
"	"	kIx"
final	final	k1gInSc1
à	à	k?
la	la	k1gNnSc1
réforme	réform	k1gInSc5
de	de	k?
l	l	kA
<g/>
’	’	k?
<g/>
adoption	adoption	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
360	#num#	k4
<g/>
°	°	k?
<g/>
,	,	kIx,
17	#num#	k4
June	jun	k1gMnSc5
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Le	Le	k1gFnSc1
référendum	référendum	k1gInSc1
contre	contr	k1gInSc5
la	la	k0
réforme	réform	k1gInSc5
du	du	k?
droit	droit	k1gInSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
adoption	adoption	k1gInSc1
n	n	k0
<g/>
'	'	kIx"
<g/>
aboutira	aboutira	k1gFnSc1
pas	pas	k1gInSc4
↑	↑	k?
Goumaz	Goumaz	k1gInSc1
<g/>
,	,	kIx,
Magalie	Magalie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Le	Le	k1gFnPc2
référendum	référendum	k1gInSc4
se	s	k7c7
précise	précis	k1gInSc5
contre	contr	k1gInSc5
l	l	kA
<g/>
’	’	k?
<g/>
adoption	adoption	k1gInSc1
par	para	k1gFnPc2
les	les	k1gInSc1
couples	couples	k1gMnSc1
homosexuels	homosexuels	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Le	Le	k1gFnSc1
Temps	Tempsa	k1gFnPc2
<g/>
,	,	kIx,
8	#num#	k4
June	jun	k1gMnSc5
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Echec	Echec	k1gInSc1
du	du	k?
référendum	référendum	k1gInSc1
contre	contr	k1gInSc5
l	l	kA
<g/>
'	'	kIx"
<g/>
homoparentalité	homoparentalitý	k2eAgFnPc4d1
<g/>
↑	↑	k?
Un	Un	k1gFnPc4
pas	pas	k6eAd1
vers	vers	k1gInSc1
la	la	k1gNnSc2
naturalisation	naturalisation	k1gInSc1
facilitée	facilité	k1gFnSc2
pour	poura	k1gFnPc2
les	les	k1gInSc1
partenaires	partenaires	k1gMnSc1
enregistrés	enregistrés	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
24	#num#	k4
heures	heures	k1gInSc1
(	(	kIx(
<g/>
Switzerland	Switzerland	k1gInSc1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
14	#num#	k4
March	March	k1gInSc1
2016	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
SUISSE	SUISSE	kA
MARIAGE	MARIAGE	kA
GAY	gay	k1gMnSc1
ET	ET	kA
NATURALISATION	NATURALISATION	kA
TRAITÉS	TRAITÉS	kA
EN	EN	kA
PARALLÈ	PARALLÈ	k1gMnSc1
<g/>
↑	↑	k?
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
německy	německy	k6eAd1
<g/>
)	)	kIx)
Kirchenordnung	Kirchenordnung	k1gInSc1
der	drát	k5eAaImRp2nS
Evangelisch-Reformierten	Evangelisch-Reformiertno	k1gNnPc2
Kirche	Kirch	k1gFnSc2
des	des	k1gNnSc1
Kantons	Kantonsa	k1gFnPc2
Luzern	Luzerna	k1gFnPc2
<g/>
,	,	kIx,
§	§	k?
37	#num#	k4
Archivováno	archivován	k2eAgNnSc4d1
10	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
ref	ref	k?
<g/>
.	.	kIx.
<g/>
ch	ch	k0
<g/>
:	:	kIx,
TI	ti	k0
<g/>
,	,	kIx,
Synode	synod	k1gInSc5
spricht	spricht	k1gInSc1
sich	sich	k1gMnSc1
für	für	k?
Segnung	Segnung	k1gMnSc1
homosexueller	homosexueller	k1gMnSc1
Paare	Paar	k1gInSc5
aus	aus	k?
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
L	L	kA
<g/>
'	'	kIx"
<g/>
Eglise	Eglise	k1gFnSc1
protestante	protestant	k1gMnSc5
vaudoise	vaudoise	k1gFnSc2
dit	dit	k?
oui	oui	k?
à	à	k?
un	un	k?
rite	rit	k1gInSc5
pour	poura	k1gFnPc2
les	les	k1gInSc1
couples	couples	k1gMnSc1
gays	gaysa	k1gFnPc2
<g/>
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Couples	Couples	k1gMnSc1
homosexuels	homosexuelsa	k1gFnPc2
devant	devant	k1gMnSc1
le	le	k?
pasteur	pasteur	k1gMnSc1
<g/>
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
L	L	kA
<g/>
’	’	k?
<g/>
Eglise	Eglise	k1gFnSc1
réformée	réformée	k1gFnSc1
vaudoise	vaudoise	k1gFnSc1
a	a	k8xC
célébré	célébrý	k2eAgNnSc1d1
huit	huit	k5eAaPmF
unions	unions	k1gInSc4
de	de	k?
couples	couples	k1gInSc1
homosexuels	homosexuels	k1gInSc1
<g/>
↑	↑	k?
First	First	k1gInSc1
same-sex	same-sex	k1gInSc1
union	union	k1gInSc1
registered	registered	k1gInSc1
in	in	k?
Switzerland	Switzerland	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
SWI	SWI	kA
swissinfo	swissinfo	k6eAd1
<g/>
.	.	kIx.
<g/>
ch	ch	k0
<g/>
,	,	kIx,
2	#num#	k4
January	Januara	k1gFnSc2
2007	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Registered	Registered	k1gInSc1
same-sex	same-sex	k1gInSc1
partnerships	partnerships	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Swiss	Swiss	k1gInSc1
Confederation	Confederation	k1gInSc4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
↑	↑	k?
Partenariats	Partenariats	k1gInSc1
enregistrés	enregistrés	k1gInSc1
et	et	k?
dissous	dissous	k1gInSc1
selon	selon	k1gInSc1
le	le	k?
sexe	sex	k1gInSc5
et	et	k?
le	le	k?
canton	canton	k1gInSc1
<g/>
,	,	kIx,
2007	#num#	k4
<g/>
-	-	kIx~
<g/>
2016	#num#	k4
<g/>
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Partenariats	Partenariats	k1gInSc1
enregistrés	enregistrés	k1gInSc1
et	et	k?
dissous	dissous	k1gInSc1
selon	selon	k1gInSc1
le	le	k?
sexe	sex	k1gInSc5
et	et	k?
le	le	k?
canton	canton	k1gInSc1
<g/>
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Partenariats	Partenariats	k1gInSc1
enregistrés	enregistrés	k1gInSc1
<g/>
,	,	kIx,
dissolutions	dissolutions	k1gInSc1
<g/>
↑	↑	k?
(	(	kIx(
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Constitution	Constitution	k1gInSc1
du	du	k?
canton	canton	k1gInSc1
de	de	k?
Vaud	Vaud	k1gInSc1
<g/>
↑	↑	k?
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Verfassung	Verfassung	k1gInSc1
des	des	k1gNnSc1
Kantons	Kantons	k1gInSc1
Zürich	Zürich	k1gMnSc1
<g/>
↑	↑	k?
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Verfassung	Verfassung	k1gInSc1
des	des	k1gNnSc1
Kantons	Kantons	k1gInSc1
Appenzell	Appenzell	k1gMnSc1
Ausserrhoden	Ausserrhodna	k1gFnPc2
<g/>
↑	↑	k?
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Verfassung	Verfassung	k1gInSc1
des	des	k1gNnSc1
Kantons	Kantons	k1gInSc1
Basel-Stadt	Basel-Stadt	k1gMnSc1
<g/>
↑	↑	k?
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
Verfassung	Verfassung	k1gInSc1
des	des	k1gNnSc2
Kantons	Kantonsa	k1gFnPc2
Bern	Bern	k1gInSc4
<g/>
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Constitution	Constitution	k1gInSc1
de	de	k?
la	la	k1gNnSc7
République	Républiqu	k1gFnSc2
et	et	k?
canton	canton	k1gInSc1
de	de	k?
Genè	Genè	k1gFnSc2
<g/>
↑	↑	k?
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Verfassung	Verfassung	k1gInSc1
des	des	k1gNnSc1
Kantons	Kantons	k1gInSc1
Zug	Zug	k1gMnSc1
<g/>
↑	↑	k?
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Verfassung	Verfassung	k1gInSc1
des	des	k1gNnSc1
Kantons	Kantonsa	k1gFnPc2
Schaffhausen	Schaffhausna	k1gFnPc2
<g/>
↑	↑	k?
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Verfassung	Verfassunga	k1gFnPc2
des	des	k1gNnPc2
<g />
.	.	kIx.
</s>
<s hack="1">
Kantons	Kantons	k1gInSc1
Freiburg	Freiburg	k1gInSc4
<g/>
↑	↑	k?
Homosexuals	Homosexuals	k1gInSc1
a	a	k8xC
step	step	k1gFnSc1
closer	closer	k1gInSc1
to	ten	k3xDgNnSc1
equal	equat	k5eAaBmAgInS,k5eAaImAgInS,k5eAaPmAgInS
rights	rights	k1gInSc1
<g/>
↑	↑	k?
Switzerland	Switzerland	k1gInSc1
<g/>
’	’	k?
<g/>
s	s	k7c7
first	first	k1gMnSc1
“	“	k?
<g/>
gay	gay	k1gMnSc1
marriages	marriages	k1gMnSc1
<g/>
”	”	k?
take	takat	k5eAaPmIp3nS
place	plac	k1gInSc6
in	in	k?
Geneva	Genev	k1gMnSc2
<g/>
↑	↑	k?
Registered	Registered	k1gInSc1
partnerships	partnerships	k1gInSc1
in	in	k?
Switzerland	Switzerland	k1gInSc1
<g/>
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Loi	Loi	k1gFnSc1
sur	sur	k?
le	le	k?
partenariat	partenariat	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
4	#num#	k4
<g/>
.	.	kIx.
<g />
.	.	kIx.
</s>
<s hack="1">
3	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
FICHES	FICHES	kA
INCLUSIVES	INCLUSIVES	kA
POUR	Pour	k1gMnSc1
LES	les	k1gInSc4
FAMILLES	FAMILLES	kA
ARC-EN-CIEL	ARC-EN-CIEL	k1gFnSc7
<g/>
.	.	kIx.
360	#num#	k4
<g/>
.	.	kIx.
<g/>
ch	ch	k0
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
23	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
Zurich	Zurich	k1gInSc1
grants	grantsa	k1gFnPc2
gay	gay	k1gMnSc1
couples	couples	k1gMnSc1
more	mor	k1gInSc5
rights	rightsit	k5eAaPmRp2nS
<g/>
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Partenariat	Partenariat	k1gInSc4
enregistré	enregistrý	k2eAgFnSc2d1
cantonal	cantonat	k5eAaImAgMnS,k5eAaPmAgMnS
<g/>
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Le	Le	k1gFnSc1
pacs	pacs	k6eAd1
gagne	gagnout	k5eAaImIp3nS,k5eAaPmIp3nS
du	du	k?
terrain	terrain	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
6	#num#	k4
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
Constitution	Constitution	k1gInSc1
du	du	k?
canton	canton	k1gInSc1
de	de	k?
Fribourg	Fribourg	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Canton	Canton	k1gInSc1
of	of	k?
Fribourg	Fribourg	k1gInSc1
<g/>
,	,	kIx,
16	#num#	k4
May	May	k1gMnSc1
2004	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
La	la	k1gNnSc1
nouvelle	nouvelle	k1gFnSc2
Constitution	Constitution	k1gInSc1
du	du	k?
canton	canton	k1gInSc1
de	de	k?
Fribourg	Fribourg	k1gInSc1
a	a	k8xC
été	été	k?
adoptée	adoptée	k1gNnSc2
par	para	k1gFnPc2
le	le	k?
<g />
.	.	kIx.
</s>
<s hack="1">
peuple	peuple	k6eAd1
fribourgeois	fribourgeois	k1gFnSc4
<g/>
↑	↑	k?
Touzain	Touzain	k1gInSc1
<g/>
,	,	kIx,
François	François	k1gFnSc1
<g/>
.	.	kIx.
"	"	kIx"
<g/>
Défense	Défense	k1gFnSc1
du	du	k?
mariage	mariage	k1gFnSc1
<g/>
"	"	kIx"
fait	fait	k1gInSc1
un	un	k?
flop	flop	k1gInSc1
au	au	k0
Parlement	Parlement	k1gInSc4
zurichois	zurichois	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
360	#num#	k4
<g/>
°	°	k?
<g/>
,	,	kIx,
7	#num#	k4
June	jun	k1gMnSc5
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Chatain	Chatain	k1gInSc1
<g/>
,	,	kIx,
Jean-Baptiste	Jean-Baptist	k1gMnSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
CANTON	CANTON	kA
DE	DE	k?
ZURICH	ZURICH	kA
-	-	kIx~
Rejet	Rejet	k1gMnSc1
d	d	k?
<g/>
'	'	kIx"
<g/>
une	une	k?
initiative	initiativ	k1gInSc5
contre	contr	k1gInSc5
le	le	k?
mariage	mariage	k1gNnPc2
homosexuel	homosexuel	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lepetitjournal	Lepetitjournal	k1gFnSc1
<g/>
.	.	kIx.
<g/>
com	com	k?
<g/>
,	,	kIx,
7	#num#	k4
June	jun	k1gMnSc5
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
)	)	kIx)
Keine	Kein	k1gInSc5
Definition	Definition	k1gInSc1
der	drát	k5eAaImRp2nS
Ehe	ehe	k0
zwischen	zwischen	k1gInSc1
Mann	Mann	k1gMnSc1
und	und	k?
Frau	Fraa	k1gFnSc4
in	in	k?
der	drát	k5eAaImRp2nS
Verfassung	Verfassung	k1gInSc1
<g/>
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Mariage	Mariage	k1gFnSc1
pour	pour	k1gMnSc1
tous	tous	k1gInSc1
<g/>
:	:	kIx,
"	"	kIx"
<g/>
non	non	k?
<g/>
"	"	kIx"
à	à	k?
la	la	k1gNnSc1
contre-attaque	contre-attaqu	k1gInSc2
de	de	k?
l	l	kA
<g/>
'	'	kIx"
<g/>
UDF	UDF	kA
zurichoise	zurichoise	k1gFnSc1
<g/>
↑	↑	k?
12.360	12.360	k4
<g/>
7	#num#	k4
Postulat	Postulat	k1gFnPc2
<g/>
:	:	kIx,
Code	Code	k1gNnSc7
civil	civil	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pour	Pour	k1gMnSc1
un	un	k?
droit	droit	k1gMnSc1
de	de	k?
la	la	k1gNnSc2
famille	famill	k1gMnSc2
moderne	modernout	k5eAaPmIp3nS
et	et	k?
cohérent	cohérent	k1gInSc1
<g/>
,	,	kIx,
Swiss	Swiss	k1gInSc1
Parliament	Parliament	k1gInSc1
<g/>
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Rapport	Rapport	k1gInSc1
du	du	k?
Conseil	Conseil	k1gInSc1
fédéral	fédérat	k5eAaPmAgInS,k5eAaImAgInS
-	-	kIx~
Modernisation	Modernisation	k1gInSc1
du	du	k?
droit	droit	k1gInSc1
de	de	k?
la	la	k1gNnSc1
famille	famille	k1gFnSc1
<g/>
,	,	kIx,
Federal	Federal	k1gFnSc1
Department	department	k1gInSc1
of	of	k?
Justice	justice	k1gFnSc2
and	and	k?
Police	police	k1gFnSc2
<g/>
,	,	kIx,
retrieved	retrieved	k1gMnSc1
on	on	k3xPp3gMnSc1
27	#num#	k4
May	May	k1gMnSc1
2015	#num#	k4
<g/>
↑	↑	k?
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Sommaruga	Sommaruga	k1gFnSc1
espè	espè	k1gInSc5
que	que	k?
les	les	k1gInSc1
homosexuels	homosexuels	k6eAd1
pourront	pourront	k1gInSc4
bientôt	bientôt	k2eAgInSc4d1
se	se	k3xPyFc4
marier	marier	k1gInSc4
<g/>
,	,	kIx,
L	L	kA
<g/>
'	'	kIx"
<g/>
Hebdo	Hebdo	k1gNnSc1
<g/>
,	,	kIx,
retrieved	retrieved	k1gMnSc1
on	on	k3xPp3gMnSc1
27	#num#	k4
May	May	k1gMnSc1
2015	#num#	k4
<g/>
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Green	Green	k1gInSc1
Party	parta	k1gFnSc2
-	-	kIx~
Equality	Equalita	k1gFnSc2
Policy	Polica	k1gFnSc2
Archivováno	archivovat	k5eAaBmNgNnS
12	#num#	k4
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
2011	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
↑	↑	k?
'	'	kIx"
<g/>
Man	mana	k1gFnPc2
plus	plus	k1gInSc1
man	man	k1gMnSc1
doesn	doesn	k1gMnSc1
<g/>
'	'	kIx"
<g/>
t	t	k?
go	go	k?
<g/>
'	'	kIx"
<g/>
:	:	kIx,
Swiss	Swiss	k1gInSc1
politician	politician	k1gInSc1
<g/>
'	'	kIx"
<g/>
s	s	k7c7
gay	gay	k1gMnSc1
marriage	marriage	k1gNnSc2
tweet	tweet	k1gMnSc1
<g/>
↑	↑	k?
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
FDP-Frauen	FDP-Frauen	k1gInSc1
für	für	k?
"	"	kIx"
<g/>
Ehe	ehe	k0
für	für	k?
alle	alle	k1gFnPc6
<g/>
"	"	kIx"
<g/>
,	,	kIx,
Luzerner	Luzerner	k1gMnSc1
Zeitung	Zeitung	k1gMnSc1
<g/>
,	,	kIx,
21	#num#	k4
April	April	k1gInSc1
2018	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
Archivováno	archivován	k2eAgNnSc4d1
25	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
13.468	13.468	k4
Initiative	Initiativ	k1gInSc5
parlementaire	parlementair	k1gInSc5
<g/>
:	:	kIx,
Mariage	Mariag	k1gMnSc2
civil	civil	k1gMnSc1
pour	pour	k1gMnSc1
tous	tous	k1gInSc1
<g/>
,	,	kIx,
Swiss	Swiss	k1gInSc1
Parliament	Parliament	k1gInSc1
<g/>
↑	↑	k?
Swiss	Swiss	k1gInSc1
Greens	Greens	k1gInSc1
pushing	pushing	k1gInSc1
for	forum	k1gNnPc2
gay	gay	k1gMnSc1
marriage	marriage	k1gNnSc2
and	and	k?
tax	taxa	k1gFnPc2
equality	equalita	k1gFnSc2
<g/>
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Entrée	entrée	k1gNnPc1
en	en	k?
matiè	matiè	k1gInSc5
sur	sur	k?
le	le	k?
projet	projet	k2eAgInSc4d1
de	de	k?
loi	loi	k?
sur	sur	k?
les	les	k1gInSc1
avoirs	avoirs	k1gInSc1
de	de	k?
potentats	potentats	k1gInSc1
<g/>
,	,	kIx,
National	National	k1gMnSc1
Council	Council	k1gMnSc1
<g/>
,	,	kIx,
retrieved	retrieved	k1gMnSc1
20	#num#	k4
February	Februara	k1gFnSc2
2015	#num#	k4
<g/>
↑	↑	k?
Ehe	ehe	k0
für	für	k?
Alle	All	k1gFnSc2
<g/>
↑	↑	k?
When	When	k1gMnSc1
will	will	k1gMnSc1
the	the	k?
Swiss	Swiss	k1gInSc1
vote	votat	k5eAaPmIp3nS
on	on	k3xPp3gInSc1
same-sex	same-sex	k1gInSc1
marriage	marriagat	k5eAaPmIp3nS
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
↑	↑	k?
Ständerat	Ständerat	k1gMnSc1
sagt	sagt	k1gMnSc1
Ja	Ja	k?
zur	zur	k?
Homo-Ehe	Homo-Ehe	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Blick	Blick	k1gInSc1
<g/>
.	.	kIx.
<g/>
ch	ch	k0
<g/>
,	,	kIx,
1	#num#	k4
September	September	k1gInSc1
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Müsste	Müsst	k1gMnSc5
die	die	k?
Verfassung	Verfassung	k1gMnSc1
geändert	geändert	k1gMnSc1
werden	werdno	k1gNnPc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
www.nzz.ch	www.nzz.cha	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neuze	Neuze	k1gFnSc1
Zürcher	Zürchra	k1gFnPc2
Zeitung	Zeitunga	k1gFnPc2
<g/>
,	,	kIx,
12	#num#	k4
May	May	k1gMnSc1
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Committee	Committee	k1gInSc1
report	report	k1gInSc1
<g/>
,	,	kIx,
Swiss	Swiss	k1gInSc1
Parliament	Parliament	k1gMnSc1
<g/>
↑	↑	k?
Gay	gay	k1gMnSc1
marriage	marriagat	k5eAaPmIp3nS
back	back	k6eAd1
on	on	k3xPp3gInSc1
the	the	k?
discussion	discussion	k1gInSc1
table	tablo	k1gNnSc6
in	in	k?
Swiss	Swiss	k1gInSc1
parliament	parliament	k1gInSc1
<g/>
.	.	kIx.
www.thelocal.ch	www.thelocal.ch	k1gInSc1
<g/>
.	.	kIx.
thelocal	thelocat	k5eAaPmAgInS
<g/>
.	.	kIx.
<g/>
ch	ch	k0
<g/>
,	,	kIx,
16	#num#	k4
June	jun	k1gMnSc5
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Débat	Débat	k1gInSc1
sur	sur	k?
le	le	k?
mariage	mariagat	k5eAaPmIp3nS
pour	pour	k1gInSc1
tous	tousa	k1gFnPc2
prolongé	prolongá	k1gFnPc1
jusqu	jusqu	k5eAaPmIp1nS
<g/>
’	’	k?
<g/>
en	en	k?
2019	#num#	k4
<g/>
.	.	kIx.
360	#num#	k4
<g/>
.	.	kIx.
<g/>
ch	ch	k0
<g/>
.	.	kIx.
360	#num#	k4
<g/>
.	.	kIx.
<g/>
ch	ch	k0
<g/>
,	,	kIx,
17	#num#	k4
June	jun	k1gMnSc5
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Warum	Warum	k1gInSc1
sich	sich	k1gInSc1
das	das	k?
Parlament	parlament	k1gInSc1
vor	vor	k1gInSc1
dem	dem	k?
Entscheid	Entscheid	k1gInSc1
zur	zur	k?
Homo-Ehe	Homo-Ehe	k1gInSc1
drückt	drückt	k1gInSc1
<g/>
.	.	kIx.
www.watson.ch	www.watson.ch	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Watson	Watson	k1gNnSc1
<g/>
,	,	kIx,
16	#num#	k4
June	jun	k1gMnSc5
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
«	«	k?
<g/>
Ehe	ehe	k0
für	für	k?
alle	all	k1gFnSc2
<g/>
»	»	k?
bringt	bringt	k1gMnSc1
volles	volles	k1gMnSc1
Adoptionsrecht	Adoptionsrecht	k1gMnSc1
<g/>
,	,	kIx,
Luzerner	Luzerner	k1gMnSc1
Zeitung	Zeitung	k1gMnSc1
<g/>
,	,	kIx,
3	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
June	jun	k1gMnSc5
2018	#num#	k4
<g/>
↑	↑	k?
Eidgenössische	Eidgenössische	k1gFnPc2
Volksinitiative	Volksinitiativ	k1gInSc5
'	'	kIx"
<g/>
Für	Für	k1gMnSc1
Ehe	ehe	k0
und	und	k?
Familie	Familie	k1gFnSc1
-	-	kIx~
gegen	gegen	k1gInSc1
die	die	k?
Heiratsstrafe	Heiratsstraf	k1gInSc5
<g/>
'	'	kIx"
<g/>
↑	↑	k?
Schweiz	Schweiz	k1gInSc1
<g/>
:	:	kIx,
Ehe-Verbot	Ehe-Verbot	k1gInSc1
für	für	k?
Schwule	Schwule	k1gFnSc2
und	und	k?
Lesben	Lesben	k2eAgMnSc1d1
geplant	geplant	k1gMnSc1
<g/>
,	,	kIx,
25	#num#	k4
October	October	k1gInSc1
2013	#num#	k4
<g/>
,	,	kIx,
queer	queer	k1gInSc1
<g/>
.	.	kIx.
<g/>
de	de	k?
<g/>
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
13.085	13.085	k4
n	n	k0
Pour	Pour	k1gMnSc1
le	le	k?
couple	couple	k6eAd1
et	et	k?
la	la	k1gNnSc4
famille	famille	k1gNnPc2
-	-	kIx~
Non	Non	k1gMnPc2
à	à	k?
la	la	k1gNnPc2
pénalisation	pénalisation	k1gInSc1
du	du	k?
mariage	mariage	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Initiative	Initiativ	k1gInSc5
populaire	populair	k1gInSc5
<g/>
,	,	kIx,
Swiss	Swiss	k1gInSc1
Parliament	Parliament	k1gInSc1
<g/>
,	,	kIx,
retrieved	retrieved	k1gMnSc1
on	on	k3xPp3gMnSc1
16	#num#	k4
January	Januara	k1gFnSc2
2015	#num#	k4
<g/>
↑	↑	k?
Swiss	Swiss	k1gInSc1
Political	Political	k1gMnSc1
Parties	Parties	k1gMnSc1
Reveal	Reveal	k1gMnSc1
Their	Their	k1gMnSc1
Colours	Colours	k1gInSc1
<g/>
.	.	kIx.
www.swissinfo.ch	www.swissinfo.ch	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Swissinfo	Swissinfo	k1gNnSc1
<g/>
,	,	kIx,
September	September	k1gInSc1
11	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
October	October	k1gInSc1
29	#num#	k4
<g/>
,	,	kIx,
2015	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
news	ws	k6eNd1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
<g/>
Sur	Sur	k1gFnSc1
l	l	kA
<g/>
’	’	k?
<g/>
imposition	imposition	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
des	des	k1gNnSc7
couples	couplesa	k1gFnPc2
<g/>
,	,	kIx,
le	le	k?
PDC	PDC	kA
est	est	k?
taxé	taxá	k1gFnSc2
de	de	k?
rétrograde	rétrograd	k1gInSc5
<g/>
,	,	kIx,
Tribune	tribun	k1gMnSc5
de	de	k?
Genè	Genè	k1gFnPc1
<g/>
,	,	kIx,
retrieved	retrieved	k1gInSc1
16	#num#	k4
January	Januara	k1gFnSc2
2015	#num#	k4
<g/>
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Vote	Vote	k1gFnSc1
n	n	k0
<g/>
°	°	k?
49.112	49.112	k4
<g/>
75	#num#	k4
Archivováno	archivován	k2eAgNnSc4d1
26	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
Swiss	Swiss	k1gInSc1
Parliament	Parliament	k1gInSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
retrieved	retrieved	k1gMnSc1
on	on	k3xPp3gMnSc1
16	#num#	k4
January	Januara	k1gFnSc2
2015	#num#	k4
<g/>
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Conseil	Conseil	k1gInSc1
des	des	k1gNnSc1
États	États	k1gInSc1
-	-	kIx~
Procè	Procè	k1gInSc1
de	de	k?
vote	vote	k1gInSc1
13.085	13.085	k4
<g/>
-	-	kIx~
<g/>
2	#num#	k4
<g/>
,	,	kIx,
Swiss	Swiss	k1gInSc1
Parliament	Parliament	k1gInSc1
<g/>
,	,	kIx,
retrieved	retrieved	k1gInSc1
on	on	k3xPp3gInSc1
5	#num#	k4
March	March	k1gInSc1
2015	#num#	k4
<g/>
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Le	Le	k1gFnSc3
Conseil	Conseila	k1gFnPc2
des	des	k1gNnSc2
États	Étatsa	k1gFnPc2
en	en	k?
bref	bref	k1gMnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
4	#num#	k4
mars	mars	k6eAd1
2015	#num#	k4
<g/>
)	)	kIx)
Archivováno	archivovat	k5eAaBmNgNnS
2	#num#	k4
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
Swiss	Swiss	k1gInSc1
Parliament	Parliament	k1gInSc1
<g/>
,	,	kIx,
retrieved	retrieved	k1gInSc1
on	on	k3xPp3gInSc1
5	#num#	k4
March	March	k1gInSc1
2015	#num#	k4
<g/>
↑	↑	k?
Kriminelle	Kriminelle	k1gNnSc1
Ausländer	Ausländer	k1gMnSc1
<g/>
,	,	kIx,
Gotthard	Gotthard	k1gMnSc1
und	und	k?
Heiratsstrafe	Heiratsstraf	k1gMnSc5
<g/>
,	,	kIx,
7	#num#	k4
October	October	k1gInSc1
2014	#num#	k4
<g/>
,	,	kIx,
NZZ	NZZ	kA
<g/>
↑	↑	k?
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
německy	německy	k6eAd1
<g/>
)	)	kIx)
Bundesbeschluss	Bundesbeschluss	k1gInSc1
über	über	k1gInSc1
die	die	k?
Volksinitiative	Volksinitiativ	k1gInSc5
«	«	k?
<g/>
Für	Für	k1gMnSc1
Ehe	ehe	k0
und	und	k?
Familie	Familie	k1gFnSc2
–	–	k?
gegen	gegen	k1gInSc1
die	die	k?
Heiratsstrafe	Heiratsstraf	k1gInSc5
<g/>
»	»	k?
<g/>
,	,	kIx,
retrieved	retrieved	k1gInSc1
on	on	k3xPp3gInSc1
10	#num#	k4
October	October	k1gInSc1
2015	#num#	k4
<g/>
↑	↑	k?
JCVP	JCVP	kA
Kanton	Kanton	k1gInSc1
Zürich	Zürich	k1gInSc1
<g/>
:	:	kIx,
Nein	Nein	k1gInSc1
zur	zur	k?
Ehedefinition	Ehedefinition	k1gInSc4
Archivováno	archivovat	k5eAaBmNgNnS
25	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2015	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
,	,	kIx,
6	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
November	November	k1gInSc1
2015	#num#	k4
<g/>
,	,	kIx,
queer	queer	k1gInSc1
<g/>
.	.	kIx.
<g/>
ch	ch	k0
<g/>
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Le	Le	k1gMnPc1
PDC	PDC	kA
genevois	genevois	k1gFnSc1
ne	ne	k9
défendra	défendra	k1gFnSc1
pas	pas	k6eAd1
son	son	k1gInSc4
initiative	initiativ	k1gInSc5
<g/>
,	,	kIx,
Le	Le	k1gMnSc1
Courrier	Courrier	k1gMnSc1
<g/>
,	,	kIx,
retrieved	retrieved	k1gMnSc1
on	on	k3xPp3gMnSc1
28	#num#	k4
February	Februara	k1gFnSc2
2016	#num#	k4
<g/>
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Les	les	k1gInSc1
Suisses	Suisses	k1gMnSc1
plutôt	plutôt	k1gMnSc1
défavorables	défavorables	k1gMnSc1
au	au	k0
texte	text	k1gInSc5
UDC	UDC	kA
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
,	,	kIx,
selon	selon	k1gInSc1
le	le	k?
2	#num#	k4
<g/>
è	è	k5eAaPmIp3nS
sondage	sondage	k1gInSc1
SSR	SSR	kA
<g/>
,	,	kIx,
SRG	SRG	kA
SSR	SSR	kA
<g/>
,	,	kIx,
retrieved	retrieved	k1gMnSc1
on	on	k3xPp3gMnSc1
28	#num#	k4
February	Februara	k1gFnSc2
2016	#num#	k4
<g/>
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Initiative	Initiativ	k1gInSc5
populaire	populair	k1gInSc5
du	du	k?
0	#num#	k4
<g/>
5.11	5.11	k4
<g/>
.2012	.2012	k4
«	«	k?
<g/>
Pour	Pour	k1gMnSc1
le	le	k?
couple	couple	k6eAd1
et	et	k?
la	la	k1gNnSc4
famille	famille	k1gNnPc2
-	-	kIx~
Non	Non	k1gMnPc2
à	à	k?
la	la	k1gNnPc2
pénalisation	pénalisation	k1gInSc1
du	du	k?
mariage	mariage	k1gInSc1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
»	»	k?
<g/>
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Enquê	Enquê	k1gInSc5
sur	sur	k?
la	la	k1gNnPc1
droitisation	droitisation	k1gInSc1
des	des	k1gNnSc1
opinions	opinions	k1gInSc1
publiques	publiques	k1gMnSc1
européennes	européennes	k1gMnSc1
<g/>
↑	↑	k?
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Mehrheit	Mehrheit	k1gInSc1
der	drát	k5eAaImRp2nS
Schweizer	Schweizer	k1gInSc1
für	für	k?
Ehe	ehe	k0
zwischen	zwischna	k1gFnPc2
Homosexuellen	Homosexuellen	k1gInSc1
<g/>
,	,	kIx,
blick	blick	k1gInSc1
<g/>
.	.	kIx.
<g/>
ch	ch	k0
<g/>
,	,	kIx,
retrieved	retrieved	k1gMnSc1
on	on	k3xPp3gMnSc1
22	#num#	k4
February	Februara	k1gFnSc2
2015	#num#	k4
<g/>
↑	↑	k?
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
71	#num#	k4
Prozent	Prozent	k1gInSc1
der	drát	k5eAaImRp2nS
Schweizer	Schweizer	k1gInSc1
für	für	k?
Homo-Ehe	Homo-Ehe	k1gInSc1
<g/>
,	,	kIx,
sonntagszeitung	sonntagszeitung	k1gInSc1
<g/>
.	.	kIx.
<g/>
ch	ch	k0
<g/>
,	,	kIx,
retrieved	retrieved	k1gMnSc1
on	on	k3xPp3gMnSc1
22	#num#	k4
February	Februara	k1gFnSc2
20152015	#num#	k4
<g/>
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
LARGE	LARGE	kA
CONSENSUS	CONSENSUS	kA
POUR	Pour	k1gMnSc1
LES	les	k1gInSc1
DROITS	DROITS	kA
DES	des	k1gNnPc2
LGBT	LGBT	kA
Archivováno	archivován	k2eAgNnSc4d1
18	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2016	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
(	(	kIx(
<g/>
francouzsky	francouzsky	k6eAd1
<g/>
)	)	kIx)
Les	les	k1gInSc1
Suisses	Suisses	k1gInSc1
pour	pour	k1gInSc4
l	l	kA
<g/>
'	'	kIx"
<g/>
introduction	introduction	k1gInSc1
du	du	k?
mariage	mariage	k1gInSc1
pour	pour	k1gMnSc1
tous	tous	k1gInSc1
<g/>
,	,	kIx,
selon	selon	k1gInSc1
un	un	k?
sondage	sondage	k1gInSc1
<g/>
↑	↑	k?
On	on	k3xPp3gInSc1
voit	voit	k2eAgInSc1d1
plus	plus	k1gInSc1
de	de	k?
musulmans	musulmans	k1gInSc1
qu	qu	k?
<g/>
'	'	kIx"
<g/>
il	il	k?
y	y	k?
en	en	k?
a	a	k8xC
réellement	réellement	k1gMnSc1
<g/>
↑	↑	k?
Baumann	Baumann	k1gMnSc1
<g/>
,	,	kIx,
Bastian	Bastian	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tamedia-Umfrage	Tamedia-Umfrage	k1gNnPc1
<g/>
:	:	kIx,
72	#num#	k4
<g/>
%	%	kIx~
für	für	k?
Ehe	ehe	k0
für	für	k?
alle	alle	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mannschaft	Mannschaft	k1gInSc1
Magazine	Magazin	k1gInSc5
<g/>
,	,	kIx,
21	#num#	k4
December	December	k1gInSc1
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Je	být	k5eAaImIp3nS
zde	zde	k6eAd1
použita	použit	k2eAgFnSc1d1
šablona	šablona	k1gFnSc1
{{	{{	k?
<g/>
Cite	cit	k1gInSc5
web	web	k1gInSc1
<g/>
}}	}}	k?
označená	označený	k2eAgFnSc1d1
jako	jako	k9
k	k	k7c3
„	„	k?
<g/>
pouze	pouze	k6eAd1
dočasnému	dočasný	k2eAgNnSc3d1
použití	použití	k1gNnSc3
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Religion	religion	k1gInSc1
and	and	k?
society	societa	k1gFnSc2
<g/>
,	,	kIx,
Pew	Pew	k1gMnSc1
Research	Research	k1gMnSc1
Center	centrum	k1gNnPc2
<g/>
,	,	kIx,
29	#num#	k4
May	May	k1gMnSc1
2018	#num#	k4
<g/>
↑	↑	k?
Being	Being	k1gMnSc1
Christian	Christian	k1gMnSc1
in	in	k?
Western	Western	kA
Europe	Europ	k1gInSc5
<g/>
,	,	kIx,
Pew	Pew	k1gMnPc1
Research	Researcha	k1gFnPc2
Center	centrum	k1gNnPc2
<g/>
,	,	kIx,
29	#num#	k4
May	May	k1gMnSc1
2018	#num#	k4
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
LGBT	LGBT	kA
práva	práv	k2eAgFnSc1d1
ve	v	k7c6
Švýcarsku	Švýcarsko	k1gNnSc6
</s>
<s>
Stejnopohlavní	Stejnopohlavní	k2eAgNnPc1d1
soužití	soužití	k1gNnPc1
v	v	k7c6
Evropě	Evropa	k1gFnSc6
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
(	(	kIx(
<g/>
německy	německy	k6eAd1
<g/>
)	)	kIx)
Bundesgesetz	Bundesgesetz	k1gMnSc1
über	über	k1gMnSc1
die	die	k?
eingetragene	eingetragen	k1gInSc5
Partnerschaft	Partnerschaft	k1gInSc1
gleichgeschlechtlicher	gleichgeschlechtlichra	k1gFnPc2
Paare	Paar	k1gInSc5
(	(	kIx(
<g/>
Partnerschaftsgesetz	Partnerschaftsgesetz	k1gMnSc1
<g/>
,	,	kIx,
PartG	PartG	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Stejnopohlavní	Stejnopohlavní	k2eAgNnPc1d1
soužití	soužití	k1gNnPc1
v	v	k7c6
evropských	evropský	k2eAgFnPc6d1
zemích	zem	k1gFnPc6
Země	zem	k1gFnSc2
</s>
<s>
Albánie	Albánie	k1gFnSc1
•	•	k?
Andorra	Andorra	k1gFnSc1
•	•	k?
Arménie	Arménie	k1gFnSc2
•	•	k?
Ázerbájdžán	Ázerbájdžán	k1gInSc1
•	•	k?
Belgie	Belgie	k1gFnSc1
•	•	k?
Bělorusko	Bělorusko	k1gNnSc1
•	•	k?
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
•	•	k?
Bulharsko	Bulharsko	k1gNnSc1
•	•	k?
Černá	černat	k5eAaImIp3nS
Hora	hora	k1gFnSc1
•	•	k?
Česko	Česko	k1gNnSc1
•	•	k?
Dánsko	Dánsko	k1gNnSc1
•	•	k?
Estonsko	Estonsko	k1gNnSc1
•	•	k?
Finsko	Finsko	k1gNnSc1
•	•	k?
Francie	Francie	k1gFnSc2
•	•	k?
Gruzie	Gruzie	k1gFnSc2
•	•	k?
Chorvatsko	Chorvatsko	k1gNnSc1
•	•	k?
Irsko	Irsko	k1gNnSc4
•	•	k?
Island	Island	k1gInSc1
•	•	k?
Itálie	Itálie	k1gFnSc2
•	•	k?
Kazachstán	Kazachstán	k1gInSc1
•	•	k?
Lichtenštejnsko	Lichtenštejnsko	k1gNnSc1
•	•	k?
Litva	Litva	k1gFnSc1
•	•	k?
Lotyšsko	Lotyšsko	k1gNnSc1
•	•	k?
Lucembursko	Lucembursko	k1gNnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Maďarsko	Maďarsko	k1gNnSc1
•	•	k?
Malta	Malta	k1gFnSc1
•	•	k?
Moldavsko	Moldavsko	k1gNnSc1
•	•	k?
Monako	Monako	k1gNnSc1
•	•	k?
Německo	Německo	k1gNnSc1
•	•	k?
Nizozemsko	Nizozemsko	k1gNnSc1
•	•	k?
Norsko	Norsko	k1gNnSc1
•	•	k?
Polsko	Polsko	k1gNnSc1
•	•	k?
Portugalsko	Portugalsko	k1gNnSc1
•	•	k?
Rakousko	Rakousko	k1gNnSc1
•	•	k?
Rumunsko	Rumunsko	k1gNnSc1
•	•	k?
Rusko	Rusko	k1gNnSc1
•	•	k?
Řecko	Řecko	k1gNnSc1
•	•	k?
San	San	k1gMnSc1
Marino	Marina	k1gFnSc5
•	•	k?
Severní	severní	k2eAgFnSc1d1
Makedonie	Makedonie	k1gFnSc1
•	•	k?
Slovensko	Slovensko	k1gNnSc1
•	•	k?
Slovinsko	Slovinsko	k1gNnSc1
•	•	k?
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
•	•	k?
Srbsko	Srbsko	k1gNnSc1
•	•	k?
Španělsko	Španělsko	k1gNnSc1
•	•	k?
Švédsko	Švédsko	k1gNnSc1
•	•	k?
Švýcarsko	Švýcarsko	k1gNnSc1
•	•	k?
Turecko	Turecko	k1gNnSc1
•	•	k?
Ukrajina	Ukrajina	k1gFnSc1
•	•	k?
Vatikán	Vatikán	k1gInSc1
Teritoria	teritorium	k1gNnSc2
<g/>
,	,	kIx,
koloniea	koloniea	k6eAd1
zámořská	zámořský	k2eAgNnPc1d1
území	území	k1gNnPc1
</s>
<s>
Faerské	Faerský	k2eAgInPc1d1
ostrovy	ostrov	k1gInPc1
(	(	kIx(
<g/>
DK	DK	kA
<g/>
)	)	kIx)
•	•	k?
Gibraltar	Gibraltar	k1gInSc1
(	(	kIx(
<g/>
UK	UK	kA
<g/>
)	)	kIx)
•	•	k?
Guernsey	Guernsea	k1gFnSc2
(	(	kIx(
<g/>
UK	UK	kA
<g/>
)	)	kIx)
•	•	k?
Jersey	Jersea	k1gFnSc2
(	(	kIx(
<g/>
UK	UK	kA
<g/>
)	)	kIx)
•	•	k?
Man	Man	k1gMnSc1
(	(	kIx(
<g/>
UK	UK	kA
<g/>
)	)	kIx)
<g/>
Území	území	k1gNnSc4
se	s	k7c7
sporným	sporný	k2eAgNnSc7d1
postavením	postavení	k1gNnSc7
<g/>
:	:	kIx,
Abcházie	Abcházie	k1gFnSc2
•	•	k?
Jižní	jižní	k2eAgFnSc2d1
Osetie	Osetie	k1gFnSc2
•	•	k?
Kosovo	Kosův	k2eAgNnSc1d1
•	•	k?
Podněstří	Podněstří	k1gMnPc7
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
7556551-1	7556551-1	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
181317970	#num#	k4
</s>
