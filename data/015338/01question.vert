<s>
Co	co	k3yQnSc1
je	být	k5eAaImIp3nS
ve	v	k7c6
Švýcarsku	Švýcarsko	k1gNnSc6
podmínkou	podmínka	k1gFnSc7
zahájení	zahájení	k1gNnSc3
legislativního	legislativní	k2eAgInSc2d1
procesu	proces	k1gInSc2
pro	pro	k7c4
schválení	schválení	k1gNnSc4
ústavní	ústavní	k2eAgFnSc2d1
změny	změna	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
by	by	kYmCp3nS
legalizovala	legalizovat	k5eAaBmAgFnS
stejnopohlavní	stejnopohlavní	k2eAgNnSc4d1
manželství	manželství	k1gNnSc4
<g/>
?	?	kIx.
</s>