<s>
Kvalifikovaná	kvalifikovaný	k2eAgFnSc1d1
většina	většina	k1gFnSc1
</s>
<s>
Souslovím	sousloví	k1gNnSc7
kvalifikovaná	kvalifikovaný	k2eAgFnSc1d1
většina	většina	k1gFnSc1
se	se	k3xPyFc4
v	v	k7c6
českém	český	k2eAgInSc6d1
ústavním	ústavní	k2eAgInSc6d1
řádu	řád	k1gInSc6
označuje	označovat	k5eAaImIp3nS
shoda	shoda	k1gFnSc1
víc	hodně	k6eAd2
než	než	k8xS
60	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
tedy	tedy	k9
víc	hodně	k6eAd2
než	než	k8xS
tří	tři	k4xCgFnPc2
pětin	pětina	k4gFnPc2
<g/>
,	,	kIx,
ze	z	k7c2
všech	všecek	k3xTgMnPc2
zvolených	zvolený	k2eAgMnPc2d1
poslanců	poslanec	k1gMnPc2
a	a	k8xC
senátorů	senátor	k1gMnPc2
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Tato	tento	k3xDgFnSc1
většina	většina	k1gFnSc1
je	být	k5eAaImIp3nS
(	(	kIx(
<g/>
podle	podle	k7c2
platných	platný	k2eAgFnPc2d1
ústavních	ústavní	k2eAgFnPc2d1
norem	norma	k1gFnPc2
<g/>
)	)	kIx)
nutná	nutný	k2eAgFnSc1d1
při	při	k7c6
přijímání	přijímání	k1gNnSc6
nejdůležitějších	důležitý	k2eAgInPc2d3
právních	právní	k2eAgInPc2d1
předpisů	předpis	k1gInPc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
nestačí	stačit	k5eNaBmIp3nS
pouhá	pouhý	k2eAgFnSc1d1
nadpoloviční	nadpoloviční	k2eAgFnSc1d1
většina	většina	k1gFnSc1
všech	všecek	k3xTgInPc2
hlasů	hlas	k1gInPc2
respektive	respektive	k9
mandátů	mandát	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kvalifikovaná	kvalifikovaný	k2eAgFnSc1d1
většina	většina	k1gFnSc1
se	se	k3xPyFc4
užívá	užívat	k5eAaImIp3nS
zejména	zejména	k9
při	při	k7c6
přijímání	přijímání	k1gNnSc6
ústavy	ústava	k1gFnSc2
<g/>
,	,	kIx,
změnách	změna	k1gFnPc6
ústavy	ústava	k1gFnSc2
<g/>
,	,	kIx,
přijímání	přijímání	k1gNnSc1
nových	nový	k2eAgInPc2d1
ústavních	ústavní	k2eAgInPc2d1
zákonů	zákon	k1gInPc2
apod.	apod.	kA
V	v	k7c6
praxi	praxe	k1gFnSc6
to	ten	k3xDgNnSc1
pak	pak	k6eAd1
znamená	znamenat	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
Poslanecké	poslanecký	k2eAgFnSc6d1
sněmovně	sněmovna	k1gFnSc6
Parlamentu	parlament	k1gInSc2
ČR	ČR	kA
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
60	#num#	k4
procent	procento	k1gNnPc2
z	z	k7c2
200	#num#	k4
hlasů	hlas	k1gInPc2
<g/>
,	,	kIx,
tedy	tedy	k8xC
nejméně	málo	k6eAd3
120	#num#	k4
poslaneckých	poslanecký	k2eAgInPc2d1
mandátů	mandát	k1gInPc2
a	a	k8xC
v	v	k7c6
Senátu	senát	k1gInSc6
Parlamentu	parlament	k1gInSc2
ČR	ČR	kA
60	#num#	k4
procent	procento	k1gNnPc2
z	z	k7c2
přítomných	přítomný	k2eAgMnPc2d1
senátorů	senátor	k1gMnPc2
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
bylo	být	k5eAaImAgNnS
možno	možno	k6eAd1
vytvářet	vytvářet	k5eAaImF
či	či	k8xC
měnit	měnit	k5eAaImF
ústavní	ústavní	k2eAgInPc4d1
zákony	zákon	k1gInPc4
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
komory	komora	k1gFnPc1
jsou	být	k5eAaImIp3nP
způsobilé	způsobilý	k2eAgFnPc1d1
se	se	k3xPyFc4
usnášet	usnášet	k5eAaImF
za	za	k7c2
přítomnosti	přítomnost	k1gFnSc2
alespoň	alespoň	k9
jedné	jeden	k4xCgFnSc2
třetiny	třetina	k1gFnSc2
svých	svůj	k3xOyFgMnPc2
členů	člen	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Kvalifikované	kvalifikovaný	k2eAgFnSc2d1
většiny	většina	k1gFnSc2
</s>
<s>
Na	na	k7c4
rozdíl	rozdíl	k1gInSc4
od	od	k7c2
prosté	prostý	k2eAgFnSc2d1
většiny	většina	k1gFnSc2
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
znamená	znamenat	k5eAaImIp3nS
více	hodně	k6eAd2
než	než	k8xS
polovinu	polovina	k1gFnSc4
přítomných	přítomný	k1gMnPc2
<g/>
,	,	kIx,
znamená	znamenat	k5eAaImIp3nS
absolutní	absolutní	k2eAgFnSc1d1
většina	většina	k1gFnSc1
(	(	kIx(
<g/>
někdy	někdy	k6eAd1
také	také	k9
kvalifikovaná	kvalifikovaný	k2eAgFnSc1d1
většina	většina	k1gFnSc1
<g/>
)	)	kIx)
více	hodně	k6eAd2
než	než	k8xS
polovinu	polovina	k1gFnSc4
ze	z	k7c2
všech	všecek	k3xTgMnPc2
oprávněných	oprávněný	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kromě	kromě	k7c2
toho	ten	k3xDgInSc2
se	se	k3xPyFc4
pro	pro	k7c4
některá	některý	k3yIgNnPc4
rozhodnutí	rozhodnutí	k1gNnPc4
vyžaduje	vyžadovat	k5eAaImIp3nS
většina	většina	k1gFnSc1
dvou	dva	k4xCgFnPc2
třetin	třetina	k1gFnPc2
nebo	nebo	k8xC
tří	tři	k4xCgFnPc2
pětin	pětina	k1gFnPc2
hlasů	hlas	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
opět	opět	k6eAd1
buď	buď	k8xC
prostá	prostý	k2eAgFnSc1d1
nebo	nebo	k8xC
častěji	často	k6eAd2
absolutní	absolutní	k2eAgFnSc1d1
<g/>
,	,	kIx,
tj.	tj.	kA
počítaná	počítaný	k2eAgFnSc1d1
ze	z	k7c2
všech	všecek	k3xTgMnPc2
oprávněných	oprávněný	k2eAgMnPc2d1
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Evropské	evropský	k2eAgFnSc6d1
komisi	komise	k1gFnSc6
se	se	k3xPyFc4
vyžaduje	vyžadovat	k5eAaImIp3nS
jednomyslná	jednomyslný	k2eAgFnSc1d1
shoda	shoda	k1gFnSc1
nebo	nebo	k8xC
třípětinová	třípětinový	k2eAgFnSc1d1
většina	většina	k1gFnSc1
ze	z	k7c2
všech	všecek	k3xTgMnPc2
ministrů	ministr	k1gMnPc2
<g/>
,	,	kIx,
v	v	k7c6
Radě	rada	k1gFnSc6
bezpečnosti	bezpečnost	k1gFnSc2
OSN	OSN	kA
15	#num#	k4
ze	z	k7c2
27	#num#	k4
(	(	kIx(
<g/>
více	hodně	k6eAd2
než	než	k8xS
tři	tři	k4xCgFnPc4
pětiny	pětina	k1gFnPc4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
v	v	k7c6
americkém	americký	k2eAgInSc6d1
Senátu	senát	k1gInSc6
je	být	k5eAaImIp3nS
třípětinová	třípětinový	k2eAgFnSc1d1
většina	většina	k1gFnSc1
třeba	třeba	k6eAd1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
ukončila	ukončit	k5eAaPmAgFnS
debata	debata	k1gFnSc1
o	o	k7c6
návrhu	návrh	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yQgInSc4,k3yRgInSc4,k3yIgInSc4
chce	chtít	k5eAaImIp3nS
menšina	menšina	k1gFnSc1
blokovat	blokovat	k5eAaImF
prodlužováním	prodlužování	k1gNnSc7
diskuse	diskuse	k1gFnSc2
(	(	kIx(
<g/>
filibuster	filibuster	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pro	pro	k7c4
přehlasování	přehlasování	k1gNnSc4
prezidentova	prezidentův	k2eAgNnSc2d1
veta	veto	k1gNnSc2
a	a	k8xC
pro	pro	k7c4
změnu	změna	k1gFnSc4
ústavy	ústava	k1gFnSc2
je	být	k5eAaImIp3nS
třeba	třeba	k6eAd1
dvoutřetinová	dvoutřetinový	k2eAgFnSc1d1
většina	většina	k1gFnSc1
v	v	k7c6
obou	dva	k4xCgFnPc6
komorách	komora	k1gFnPc6
Kongresu	kongres	k1gInSc2
<g/>
,	,	kIx,
změnu	změna	k1gFnSc4
ústavy	ústava	k1gFnSc2
musí	muset	k5eAaImIp3nS
mimo	mimo	k7c4
to	ten	k3xDgNnSc4
schválit	schválit	k5eAaPmF
tři	tři	k4xCgFnPc1
čtvrtiny	čtvrtina	k1gFnPc1
státních	státní	k2eAgInPc2d1
parlamentů	parlament	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
Německu	Německo	k1gNnSc6
se	se	k3xPyFc4
dvoutřetinová	dvoutřetinový	k2eAgFnSc1d1
většina	většina	k1gFnSc1
vyžaduje	vyžadovat	k5eAaImIp3nS
pro	pro	k7c4
změnu	změna	k1gFnSc4
ústavy	ústava	k1gFnSc2
a	a	k8xC
pro	pro	k7c4
vyjádření	vyjádření	k1gNnSc4
nedůvěry	nedůvěra	k1gFnSc2
vládě	vláda	k1gFnSc6
(	(	kIx(
<g/>
kancléři	kancléř	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Rakousku	Rakousko	k1gNnSc6
pro	pro	k7c4
změny	změna	k1gFnPc4
ústavy	ústava	k1gFnSc2
<g/>
,	,	kIx,
pro	pro	k7c4
odvolání	odvolání	k1gNnSc4
prezidenta	prezident	k1gMnSc2
a	a	k8xC
v	v	k7c6
případech	případ	k1gInPc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
spolkový	spolkový	k2eAgInSc1d1
sněm	sněm	k1gInSc1
zasahuje	zasahovat	k5eAaImIp3nS
do	do	k7c2
kompetence	kompetence	k1gFnSc2
zemí	zem	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s>
Ještě	ještě	k6eAd1
dokonalejší	dokonalý	k2eAgNnPc4d2
pravidla	pravidlo	k1gNnPc4
pro	pro	k7c4
ochranu	ochrana	k1gFnSc4
menšiny	menšina	k1gFnSc2
a	a	k8xC
menších	malý	k2eAgFnPc2d2
zemí	zem	k1gFnPc2
vůbec	vůbec	k9
před	před	k7c7
přehlasováním	přehlasování	k1gNnSc7
obsahuje	obsahovat	k5eAaImIp3nS
návrh	návrh	k1gInSc1
Lisabonské	lisabonský	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Absolutní	absolutní	k2eAgFnSc1d1
většina	většina	k1gFnSc1
</s>
<s>
Většina	většina	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Ottův	Ottův	k2eAgInSc1d1
slovník	slovník	k1gInSc1
naučný	naučný	k2eAgInSc1d1
<g/>
,	,	kIx,
heslo	heslo	k1gNnSc1
Hlasování	hlasování	k1gNnSc1
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Typy	typ	k1gInPc1
většin	většina	k1gFnPc2
Jednomandátový	jednomandátový	k2eAgInSc1d1
obvod	obvod	k1gInSc1
</s>
<s>
relativní	relativní	k2eAgFnSc1d1
většina	většina	k1gFnSc1
·	·	k?
většina	většina	k1gFnSc1
·	·	k?
absolutní	absolutní	k2eAgFnSc1d1
většina	většina	k1gFnSc1
·	·	k?
kvalifikovaná	kvalifikovaný	k2eAgFnSc1d1
většina	většina	k1gFnSc1
·	·	k?
dvojitá	dvojitý	k2eAgFnSc1d1
většina	většina	k1gFnSc1
Vícemandátový	Vícemandátový	k2eAgInSc1d1
obvod	obvod	k1gInSc1
(	(	kIx(
<g/>
uzavírací	uzavírací	k2eAgFnSc1d1
klauzule	klauzule	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
Hareova	Hareův	k2eAgFnSc1d1
kvóta	kvóta	k1gFnSc1
·	·	k?
Droopova	Droopův	k2eAgFnSc1d1
kvóta	kvóta	k1gFnSc1
·	·	k?
Imperialiho	Imperiali	k1gMnSc2
kvóta	kvóta	k1gFnSc1
·	·	k?
Hagenbach-Bischoffova	Hagenbach-Bischoffův	k2eAgFnSc1d1
kvóta	kvóta	k1gFnSc1
</s>
