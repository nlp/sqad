<s>
Souslovím	sousloví	k1gNnSc7
kvalifikovaná	kvalifikovaný	k2eAgFnSc1d1
většina	většina	k1gFnSc1
se	se	k3xPyFc4
v	v	k7c6
českém	český	k2eAgInSc6d1
ústavním	ústavní	k2eAgInSc6d1
řádu	řád	k1gInSc6
označuje	označovat	k5eAaImIp3nS
shoda	shoda	k1gFnSc1
víc	hodně	k6eAd2
než	než	k8xS
60	#num#	k4
%	%	kIx~
<g/>
,	,	kIx,
tedy	tedy	k9
víc	hodně	k6eAd2
než	než	k8xS
tří	tři	k4xCgFnPc2
pětin	pětina	k4gFnPc2
<g/>
,	,	kIx,
ze	z	k7c2
všech	všecek	k3xTgMnPc2
zvolených	zvolený	k2eAgMnPc2d1
poslanců	poslanec	k1gMnPc2
a	a	k8xC
senátorů	senátor	k1gMnPc2
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
</s>