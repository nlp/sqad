<s>
Cyril	Cyril	k1gMnSc1	Cyril
vytvořil	vytvořit	k5eAaPmAgMnS	vytvořit
složitější	složitý	k2eAgNnSc4d2	složitější
a	a	k8xC	a
starší	starý	k2eAgNnSc4d2	starší
písmo	písmo	k1gNnSc4	písmo
hlaholici	hlaholice	k1gFnSc4	hlaholice
v	v	k7c6	v
60	[number]	k4	60
<g/>
.	.	kIx.	.
letech	léto	k1gNnPc6	léto
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
cyrilice	cyrilice	k1gFnSc1	cyrilice
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
až	až	k6eAd1	až
koncem	koncem	k7c2	koncem
9	[number]	k4	9
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
v	v	k7c6	v
Bulharsku	Bulharsko	k1gNnSc6	Bulharsko
(	(	kIx(	(
<g/>
Cyrillus	Cyrillus	k1gMnSc1	Cyrillus
et	et	k?	et
Methudius	Methudius	k1gMnSc1	Methudius
inventis	inventis	k1gFnSc2	inventis
Bulgarorum	Bulgarorum	k1gInSc1	Bulgarorum
litteris	litteris	k1gFnSc4	litteris
<g/>
)	)	kIx)	)
jako	jako	k8xS	jako
výraz	výraz	k1gInSc1	výraz
jasného	jasný	k2eAgInSc2d1	jasný
příklonu	příklon	k1gInSc2	příklon
k	k	k7c3	k
byzantské	byzantský	k2eAgFnSc3d1	byzantská
kulturní	kulturní	k2eAgFnSc3d1	kulturní
sféře	sféra	k1gFnSc3	sféra
<g/>
,	,	kIx,	,
o	o	k7c4	o
nějž	jenž	k3xRgMnSc4	jenž
usilovali	usilovat	k5eAaImAgMnP	usilovat
zejména	zejména	k9	zejména
řečtí	řecký	k2eAgMnPc1d1	řecký
duchovní	duchovní	k1gMnPc1	duchovní
na	na	k7c6	na
slovanských	slovanský	k2eAgNnPc6d1	slovanské
územích	území	k1gNnPc6	území
<g/>
.	.	kIx.	.
</s>
