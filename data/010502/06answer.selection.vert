<s>
Karel	Karel	k1gMnSc1	Karel
také	také	k9	také
dosáhl	dosáhnout	k5eAaPmAgMnS	dosáhnout
významné	významný	k2eAgFnPc4d1	významná
územní	územní	k2eAgFnPc4d1	územní
expanze	expanze	k1gFnPc4	expanze
českého	český	k2eAgInSc2d1	český
státu	stát	k1gInSc2	stát
<g/>
,	,	kIx,	,
především	především	k6eAd1	především
severovýchodním	severovýchodní	k2eAgInSc7d1	severovýchodní
směrem	směr	k1gInSc7	směr
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
hlavně	hlavně	k9	hlavně
díky	díky	k7c3	díky
své	svůj	k3xOyFgFnSc3	svůj
obratné	obratný	k2eAgFnSc3d1	obratná
sňatkové	sňatkový	k2eAgFnSc3d1	sňatková
politice	politika	k1gFnSc3	politika
<g/>
.	.	kIx.	.
</s>
