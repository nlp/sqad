<s>
Statické	statický	k2eAgNnSc1d1	statické
lano	lano	k1gNnSc1	lano
je	být	k5eAaImIp3nS	být
nesprávné	správný	k2eNgNnSc1d1	nesprávné
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
nízkoprůtažné	nízkoprůtažný	k2eAgNnSc4d1	nízkoprůtažný
lano	lano	k1gNnSc4	lano
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgInSc4d1	patřící
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
textilních	textilní	k2eAgNnPc2d1	textilní
lan	lano	k1gNnPc2	lano
používaných	používaný	k2eAgNnPc2d1	používané
v	v	k7c4	v
lezectví	lezectví	k1gNnSc4	lezectví
<g/>
.	.	kIx.	.
</s>
<s>
Oproti	oproti	k7c3	oproti
horolezeckým	horolezecký	k2eAgFnPc3d1	horolezecká
(	(	kIx(	(
<g/>
dynamickým	dynamický	k2eAgFnPc3d1	dynamická
<g/>
)	)	kIx)	)
lanům	lano	k1gNnPc3	lano
se	se	k3xPyFc4	se
lana	lano	k1gNnSc2	lano
statická	statický	k2eAgFnSc1d1	statická
vyznačují	vyznačovat	k5eAaImIp3nP	vyznačovat
sníženou	snížený	k2eAgFnSc7d1	snížená
průtažností	průtažnost	k1gFnSc7	průtažnost
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
vyžadována	vyžadovat	k5eAaImNgFnS	vyžadovat
pro	pro	k7c4	pro
práce	práce	k1gFnPc4	práce
ve	v	k7c6	v
výškách	výška	k1gFnPc6	výška
na	na	k7c6	na
laně	lano	k1gNnSc6	lano
<g/>
,	,	kIx,	,
v	v	k7c6	v
záchranářství	záchranářství	k1gNnSc6	záchranářství
a	a	k8xC	a
ve	v	k7c6	v
speleoalpinismu	speleoalpinismus	k1gInSc6	speleoalpinismus
<g/>
.	.	kIx.	.
</s>
<s>
Lana	lano	k1gNnPc1	lano
jsou	být	k5eAaImIp3nP	být
označována	označován	k2eAgNnPc1d1	označováno
podle	podle	k7c2	podle
ČSN	ČSN	kA	ČSN
EN	EN	kA	EN
1891	[number]	k4	1891
jako	jako	k8xS	jako
"	"	kIx"	"
<g/>
Nízkoprůtažná	Nízkoprůtažný	k2eAgNnPc4d1	Nízkoprůtažný
lana	lano	k1gNnPc4	lano
s	s	k7c7	s
opláštěným	opláštěný	k2eAgNnSc7d1	opláštěné
jádrem	jádro	k1gNnSc7	jádro
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
této	tento	k3xDgFnSc2	tento
normy	norma	k1gFnSc2	norma
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
dva	dva	k4xCgInPc4	dva
typy	typ	k1gInPc4	typ
<g/>
.	.	kIx.	.
</s>
<s>
Lano	lano	k1gNnSc1	lano
typu	typ	k1gInSc2	typ
A	a	k9	a
má	mít	k5eAaImIp3nS	mít
požadovanou	požadovaný	k2eAgFnSc4d1	požadovaná
minimální	minimální	k2eAgFnSc4d1	minimální
pevnost	pevnost	k1gFnSc4	pevnost
22	[number]	k4	22
kN	kN	k?	kN
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
určeno	určit	k5eAaPmNgNnS	určit
pro	pro	k7c4	pro
činnosti	činnost	k1gFnPc4	činnost
ve	v	k7c6	v
výškách	výška	k1gFnPc6	výška
a	a	k8xC	a
nad	nad	k7c7	nad
volnou	volný	k2eAgFnSc7d1	volná
hloubkou	hloubka	k1gFnSc7	hloubka
<g/>
,	,	kIx,	,
pro	pro	k7c4	pro
záchranu	záchrana	k1gFnSc4	záchrana
<g/>
,	,	kIx,	,
speleologii	speleologie	k1gFnSc4	speleologie
a	a	k8xC	a
podobné	podobný	k2eAgFnPc4d1	podobná
činnosti	činnost	k1gFnPc4	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Lano	lano	k1gNnSc1	lano
typu	typ	k1gInSc2	typ
B	B	kA	B
má	mít	k5eAaImIp3nS	mít
nižší	nízký	k2eAgInPc4d2	nižší
parametry	parametr	k1gInPc4	parametr
<g/>
,	,	kIx,	,
např.	např.	kA	např.
jeho	jeho	k3xOp3gFnSc1	jeho
minimální	minimální	k2eAgFnSc1d1	minimální
pevnost	pevnost	k1gFnSc1	pevnost
je	být	k5eAaImIp3nS	být
18	[number]	k4	18
kN	kN	k?	kN
<g/>
.	.	kIx.	.
</s>
<s>
Lana	lano	k1gNnSc2	lano
typu	typ	k1gInSc2	typ
B	B	kA	B
jsou	být	k5eAaImIp3nP	být
většinou	většina	k1gFnSc7	většina
menších	malý	k2eAgInPc2d2	menší
průměrů	průměr	k1gInPc2	průměr
<g/>
,	,	kIx,	,
vyžadují	vyžadovat	k5eAaImIp3nP	vyžadovat
větší	veliký	k2eAgFnSc4d2	veliký
péči	péče	k1gFnSc4	péče
při	při	k7c6	při
užívání	užívání	k1gNnSc6	užívání
a	a	k8xC	a
jejich	jejich	k3xOp3gNnSc4	jejich
uplatnění	uplatnění	k1gNnSc4	uplatnění
je	být	k5eAaImIp3nS	být
hlavně	hlavně	k9	hlavně
tam	tam	k6eAd1	tam
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
hmotnost	hmotnost	k1gFnSc1	hmotnost
lana	lano	k1gNnSc2	lano
limitující	limitující	k2eAgFnSc2d1	limitující
(	(	kIx(	(
<g/>
např.	např.	kA	např.
expediční	expediční	k2eAgFnSc4d1	expediční
činnost	činnost	k1gFnSc4	činnost
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nízkoprůtažná	Nízkoprůtažný	k2eAgNnPc1d1	Nízkoprůtažný
lana	lano	k1gNnPc1	lano
se	se	k3xPyFc4	se
vyrábějí	vyrábět	k5eAaImIp3nP	vyrábět
v	v	k7c6	v
průměrech	průměr	k1gInPc6	průměr
od	od	k7c2	od
8,5	[number]	k4	8,5
mm	mm	kA	mm
do	do	k7c2	do
16	[number]	k4	16
mm	mm	kA	mm
hlavně	hlavně	k6eAd1	hlavně
z	z	k7c2	z
polyamidu	polyamid	k1gInSc2	polyamid
<g/>
,	,	kIx,	,
polyesteru	polyester	k1gInSc2	polyester
aromatického	aromatický	k2eAgInSc2d1	aromatický
polyamidu	polyamid	k1gInSc2	polyamid
(	(	kIx(	(
<g/>
kevlar	kevlar	k1gInSc1	kevlar
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
orientovaný	orientovaný	k2eAgInSc1d1	orientovaný
polyetylén	polyetylén	k1gInSc1	polyetylén
a	a	k8xC	a
vectran	vectran	k1gInSc1	vectran
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
požadavkem	požadavek	k1gInSc7	požadavek
normy	norma	k1gFnSc2	norma
je	být	k5eAaImIp3nS	být
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
musí	muset	k5eAaImIp3nS	muset
vyrábět	vyrábět	k5eAaImF	vyrábět
z	z	k7c2	z
materiálu	materiál	k1gInSc2	materiál
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
má	mít	k5eAaImIp3nS	mít
bod	bod	k1gInSc4	bod
tání	tání	k1gNnSc2	tání
větší	veliký	k2eAgFnSc2d2	veliký
než	než	k8xS	než
195	[number]	k4	195
°	°	k?	°
<g/>
C	C	kA	C
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
pro	pro	k7c4	pro
jejich	jejich	k3xOp3gFnSc4	jejich
výrobu	výroba	k1gFnSc4	výroba
nelze	lze	k6eNd1	lze
použít	použít	k5eAaPmF	použít
polyethylen	polyethylen	k1gInSc4	polyethylen
a	a	k8xC	a
polypropylen	polypropylen	k1gInSc4	polypropylen
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
tento	tento	k3xDgInSc4	tento
požadavek	požadavek	k1gInSc4	požadavek
nesplňují	splňovat	k5eNaImIp3nP	splňovat
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
hlediska	hledisko	k1gNnSc2	hledisko
dynamického	dynamický	k2eAgInSc2d1	dynamický
výkonu	výkon	k1gInSc2	výkon
jsou	být	k5eAaImIp3nP	být
lana	lano	k1gNnPc1	lano
zkoušena	zkoušen	k2eAgNnPc1d1	zkoušeno
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
zkušebního	zkušební	k2eAgInSc2d1	zkušební
vzorku	vzorek	k1gInSc2	vzorek
2	[number]	k4	2
m	m	kA	m
<g/>
,	,	kIx,	,
pádový	pádový	k2eAgInSc1d1	pádový
faktor	faktor	k1gInSc1	faktor
f	f	k?	f
<g/>
=	=	kIx~	=
<g/>
1	[number]	k4	1
a	a	k8xC	a
hmotnost	hmotnost	k1gFnSc4	hmotnost
zkušebního	zkušební	k2eAgNnSc2d1	zkušební
břemene	břemeno	k1gNnSc2	břemeno
je	být	k5eAaImIp3nS	být
u	u	k7c2	u
lana	lano	k1gNnSc2	lano
typu	typ	k1gInSc2	typ
A	a	k8xC	a
100	[number]	k4	100
kg	kg	kA	kg
a	a	k8xC	a
u	u	k7c2	u
lana	lano	k1gNnSc2	lano
typu	typ	k1gInSc2	typ
B	B	kA	B
80	[number]	k4	80
kg	kg	kA	kg
<g/>
.	.	kIx.	.
</s>
<s>
Lano	lano	k1gNnSc1	lano
musí	muset	k5eAaImIp3nS	muset
bez	bez	k7c2	bez
přetržení	přetržení	k1gNnSc2	přetržení
snést	snést	k5eAaPmF	snést
5	[number]	k4	5
zkušebních	zkušební	k2eAgInPc2d1	zkušební
pádů	pád	k1gInPc2	pád
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgInSc7d1	důležitý
parametrem	parametr	k1gInSc7	parametr
lana	lano	k1gNnSc2	lano
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gFnSc1	jeho
průtažnost	průtažnost	k1gFnSc1	průtažnost
-	-	kIx~	-
např.	např.	kA	např.
u	u	k7c2	u
lana	lano	k1gNnSc2	lano
typů	typ	k1gInPc2	typ
A	a	k9	a
při	při	k7c6	při
aplikaci	aplikace	k1gFnSc6	aplikace
zkušební	zkušební	k2eAgFnSc2d1	zkušební
hmoty	hmota	k1gFnSc2	hmota
150	[number]	k4	150
kg	kg	kA	kg
(	(	kIx(	(
<g/>
předchozí	předchozí	k2eAgNnSc4d1	předchozí
předpětí	předpětí	k1gNnSc4	předpětí
50	[number]	k4	50
kg	kg	kA	kg
<g/>
)	)	kIx)	)
nesmí	smět	k5eNaImIp3nS	smět
prodloužení	prodloužení	k1gNnSc4	prodloužení
překročit	překročit	k5eAaPmF	překročit
hodnotu	hodnota	k1gFnSc4	hodnota
5	[number]	k4	5
%	%	kIx~	%
<g/>
.	.	kIx.	.
</s>
<s>
Norma	Norma	k1gFnSc1	Norma
definuje	definovat	k5eAaBmIp3nS	definovat
i	i	k9	i
posuv	posuv	k1gInSc1	posuv
pláště	plášť	k1gInSc2	plášť
lana	lano	k1gNnSc2	lano
vůči	vůči	k7c3	vůči
jeho	jeho	k3xOp3gNnSc3	jeho
jádru	jádro	k1gNnSc3	jádro
<g/>
,	,	kIx,	,
uzlovatelnost	uzlovatelnost	k1gFnSc1	uzlovatelnost
(	(	kIx(	(
<g/>
flexibilnost	flexibilnost	k1gFnSc1	flexibilnost
<g/>
)	)	kIx)	)
a	a	k8xC	a
další	další	k2eAgInPc4d1	další
parametry	parametr	k1gInPc4	parametr
<g/>
.	.	kIx.	.
</s>
<s>
Uvnitř	uvnitř	k7c2	uvnitř
lana	lano	k1gNnSc2	lano
je	být	k5eAaImIp3nS	být
kontrolní	kontrolní	k2eAgFnSc1d1	kontrolní
páska	páska	k1gFnSc1	páska
probíhající	probíhající	k2eAgFnSc1d1	probíhající
celou	celý	k2eAgFnSc7d1	celá
délkou	délka	k1gFnSc7	délka
lana	lano	k1gNnSc2	lano
<g/>
,	,	kIx,	,
udávající	udávající	k2eAgNnSc1d1	udávající
označení	označení	k1gNnSc1	označení
výrobce	výrobce	k1gMnSc2	výrobce
<g/>
,	,	kIx,	,
typ	typ	k1gInSc1	typ
lana	lano	k1gNnSc2	lano
a	a	k8xC	a
číslo	číslo	k1gNnSc4	číslo
normy	norma	k1gFnSc2	norma
<g/>
,	,	kIx,	,
rok	rok	k1gInSc4	rok
výroby	výroba	k1gFnSc2	výroba
číslicí	číslice	k1gFnSc7	číslice
a	a	k8xC	a
materiál	materiál	k1gInSc1	materiál
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yQgInSc2	který
je	být	k5eAaImIp3nS	být
lano	lano	k1gNnSc1	lano
vyrobeno	vyrobit	k5eAaPmNgNnS	vyrobit
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
údaje	údaj	k1gInPc1	údaj
jsou	být	k5eAaImIp3nP	být
v	v	k7c4	v
maximální	maximální	k2eAgFnPc4d1	maximální
vzdálenosti	vzdálenost	k1gFnPc4	vzdálenost
od	od	k7c2	od
sebe	se	k3xPyFc2	se
1	[number]	k4	1
m	m	kA	m
a	a	k8xC	a
jsou	být	k5eAaImIp3nP	být
důležité	důležitý	k2eAgInPc1d1	důležitý
pro	pro	k7c4	pro
kontrolu	kontrola	k1gFnSc4	kontrola
<g/>
.	.	kIx.	.
</s>
