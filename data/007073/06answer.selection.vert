<s>
Statické	statický	k2eAgNnSc1d1	statické
lano	lano	k1gNnSc1	lano
je	být	k5eAaImIp3nS	být
nesprávné	správný	k2eNgNnSc1d1	nesprávné
označení	označení	k1gNnSc1	označení
pro	pro	k7c4	pro
nízkoprůtažné	nízkoprůtažný	k2eAgNnSc4d1	nízkoprůtažný
lano	lano	k1gNnSc4	lano
<g/>
,	,	kIx,	,
patřící	patřící	k2eAgInSc4d1	patřící
do	do	k7c2	do
kategorie	kategorie	k1gFnSc2	kategorie
textilních	textilní	k2eAgNnPc2d1	textilní
lan	lano	k1gNnPc2	lano
používaných	používaný	k2eAgNnPc2d1	používané
v	v	k7c4	v
lezectví	lezectví	k1gNnSc4	lezectví
<g/>
.	.	kIx.	.
</s>
