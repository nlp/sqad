<s>
Většina	většina	k1gFnSc1	většina
alavitů	alavit	k1gInPc2	alavit
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yIgFnPc3	který
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
Asad	Asad	k1gInSc1	Asad
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
<g/>
,	,	kIx,	,
syrských	syrský	k2eAgMnPc2d1	syrský
křesťanů	křesťan	k1gMnPc2	křesťan
a	a	k8xC	a
drúzů	drúz	k1gMnPc2	drúz
podporuje	podporovat	k5eAaImIp3nS	podporovat
syrskou	syrský	k2eAgFnSc4d1	Syrská
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
bojí	bát	k5eAaImIp3nP	bát
represí	represe	k1gFnPc2	represe
proti	proti	k7c3	proti
menšinám	menšina	k1gFnPc3	menšina
v	v	k7c6	v
případě	případ	k1gInSc6	případ
vítězství	vítězství	k1gNnSc2	vítězství
sunnitských	sunnitský	k2eAgMnPc2d1	sunnitský
islamistů	islamista	k1gMnPc2	islamista
<g/>
.	.	kIx.	.
</s>
