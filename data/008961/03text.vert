<p>
<s>
Syrská	syrský	k2eAgFnSc1d1	Syrská
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
je	být	k5eAaImIp3nS	být
trvající	trvající	k2eAgInSc4d1	trvající
vnitřní	vnitřní	k2eAgInSc4d1	vnitřní
ozbrojený	ozbrojený	k2eAgInSc4d1	ozbrojený
konflikt	konflikt	k1gInSc4	konflikt
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
Konflikt	konflikt	k1gInSc1	konflikt
začal	začít	k5eAaPmAgInS	začít
15	[number]	k4	15
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2011	[number]	k4	2011
demonstracemi	demonstrace	k1gFnPc7	demonstrace
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
tzv.	tzv.	kA	tzv.
arabského	arabský	k2eAgNnSc2d1	arabské
jara	jaro	k1gNnSc2	jaro
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
se	se	k3xPyFc4	se
syrská	syrský	k2eAgFnSc1d1	Syrská
armáda	armáda	k1gFnSc1	armáda
snažila	snažit	k5eAaImAgFnS	snažit
potlačit	potlačit	k5eAaPmF	potlačit
<g/>
,	,	kIx,	,
a	a	k8xC	a
rozvinul	rozvinout	k5eAaPmAgInS	rozvinout
se	se	k3xPyFc4	se
do	do	k7c2	do
povstání	povstání	k1gNnSc2	povstání
části	část	k1gFnSc2	část
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Později	pozdě	k6eAd2	pozdě
eskaloval	eskalovat	k5eAaImAgMnS	eskalovat
do	do	k7c2	do
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
výsledkem	výsledek	k1gInSc7	výsledek
je	být	k5eAaImIp3nS	být
faktické	faktický	k2eAgNnSc1d1	faktické
roztříštění	roztříštění	k1gNnSc1	roztříštění
země	zem	k1gFnSc2	zem
na	na	k7c4	na
velký	velký	k2eAgInSc4d1	velký
počet	počet	k1gInSc4	počet
oblastí	oblast	k1gFnPc2	oblast
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterých	který	k3yQgInPc6	který
mnoho	mnoho	k6eAd1	mnoho
navzájem	navzájem	k6eAd1	navzájem
nesourodých	sourodý	k2eNgFnPc2d1	nesourodá
frakcí	frakce	k1gFnPc2	frakce
bojuje	bojovat	k5eAaImIp3nS	bojovat
za	za	k7c4	za
rozličné	rozličný	k2eAgInPc4d1	rozličný
cíle	cíl	k1gInPc4	cíl
ve	v	k7c6	v
velmi	velmi	k6eAd1	velmi
nepřehledném	přehledný	k2eNgInSc6d1	nepřehledný
konfliktu	konflikt	k1gInSc6	konflikt
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvýznamnější	významný	k2eAgFnSc7d3	nejvýznamnější
stranou	strana	k1gFnSc7	strana
konfliktu	konflikt	k1gInSc2	konflikt
je	být	k5eAaImIp3nS	být
vláda	vláda	k1gFnSc1	vláda
prezidenta	prezident	k1gMnSc2	prezident
Bašára	Bašár	k1gMnSc2	Bašár
al-Asada	al-Asada	k1gFnSc1	al-Asada
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
potlačit	potlačit	k5eAaPmF	potlačit
povstání	povstání	k1gNnSc4	povstání
a	a	k8xC	a
udržet	udržet	k5eAaPmF	udržet
si	se	k3xPyFc3	se
nebo	nebo	k8xC	nebo
znovu	znovu	k6eAd1	znovu
získat	získat	k5eAaPmF	získat
kontrolu	kontrola	k1gFnSc4	kontrola
nad	nad	k7c7	nad
pokud	pokud	k8xS	pokud
možno	možno	k6eAd1	možno
velkou	velký	k2eAgFnSc7d1	velká
částí	část	k1gFnSc7	část
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
straně	strana	k1gFnSc6	strana
stojí	stát	k5eAaImIp3nS	stát
velká	velký	k2eAgFnSc1d1	velká
většina	většina	k1gFnSc1	většina
obyvatel	obyvatel	k1gMnPc2	obyvatel
alávitského	alávitský	k2eAgNnSc2d1	alávitský
vyznání	vyznání	k1gNnSc2	vyznání
a	a	k8xC	a
většina	většina	k1gFnSc1	většina
příslušníků	příslušník	k1gMnPc2	příslušník
ostatních	ostatní	k2eAgFnPc2d1	ostatní
náboženských	náboženský	k2eAgFnPc2d1	náboženská
menšin	menšina	k1gFnPc2	menšina
<g/>
.	.	kIx.	.
</s>
<s>
Vojenskou	vojenský	k2eAgFnSc4d1	vojenská
<g/>
,	,	kIx,	,
finanční	finanční	k2eAgFnSc4d1	finanční
a	a	k8xC	a
materiální	materiální	k2eAgFnSc4d1	materiální
podporu	podpora	k1gFnSc4	podpora
ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
dostává	dostávat	k5eAaImIp3nS	dostávat
především	především	k9	především
od	od	k7c2	od
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
,	,	kIx,	,
Íránu	Írán	k1gInSc2	Írán
a	a	k8xC	a
šíitské	šíitský	k2eAgFnSc2d1	šíitská
organizace	organizace	k1gFnSc2	organizace
Hizballáh	Hizballáha	k1gFnPc2	Hizballáha
z	z	k7c2	z
Libanonu	Libanon	k1gInSc2	Libanon
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
má	mít	k5eAaImIp3nS	mít
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
zhruba	zhruba	k6eAd1	zhruba
5	[number]	k4	5
000-10	[number]	k4	000-10
000	[number]	k4	000
příslušníků	příslušník	k1gMnPc2	příslušník
svých	svůj	k3xOyFgFnPc2	svůj
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
včetně	včetně	k7c2	včetně
leteckého	letecký	k2eAgInSc2d1	letecký
personálu	personál	k1gInSc2	personál
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
působí	působit	k5eAaImIp3nS	působit
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
větší	veliký	k2eAgInSc1d2	veliký
počet	počet	k1gInSc1	počet
ruských	ruský	k2eAgMnPc2d1	ruský
vojenských	vojenský	k2eAgMnPc2d1	vojenský
poradců	poradce	k1gMnPc2	poradce
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
se	se	k3xPyFc4	se
konfliktu	konflikt	k1gInSc6	konflikt
účastní	účastný	k2eAgMnPc1d1	účastný
syrští	syrský	k2eAgMnPc1d1	syrský
Kurdové	Kurd	k1gMnPc1	Kurd
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
usilují	usilovat	k5eAaImIp3nP	usilovat
o	o	k7c4	o
co	co	k9	co
největší	veliký	k2eAgFnSc4d3	veliký
autonomii	autonomie	k1gFnSc4	autonomie
až	až	k8xS	až
samostatnost	samostatnost	k1gFnSc4	samostatnost
svých	svůj	k3xOyFgNnPc2	svůj
zčásti	zčásti	k6eAd1	zčásti
roztříštěných	roztříštěný	k2eAgNnPc2d1	roztříštěné
teritorií	teritorium	k1gNnPc2	teritorium
<g/>
.	.	kIx.	.
</s>
<s>
Ze	z	k7c2	z
zahraničí	zahraničí	k1gNnSc2	zahraničí
se	se	k3xPyFc4	se
jim	on	k3xPp3gInPc3	on
dostává	dostávat	k5eAaImIp3nS	dostávat
podpory	podpora	k1gFnPc4	podpora
mj.	mj.	kA	mj.
od	od	k7c2	od
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
které	který	k3yQgInPc1	který
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2017	[number]	k4	2017
vyslaly	vyslat	k5eAaPmAgInP	vyslat
do	do	k7c2	do
Sýrie	Sýrie	k1gFnSc2	Sýrie
kolem	kolem	k7c2	kolem
950	[number]	k4	950
vojáků	voják	k1gMnPc2	voják
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
Islámskému	islámský	k2eAgInSc3d1	islámský
státu	stát	k1gInSc3	stát
a	a	k8xC	a
zřídily	zřídit	k5eAaPmAgFnP	zřídit
svoji	svůj	k3xOyFgFnSc4	svůj
základnu	základna	k1gFnSc4	základna
u	u	k7c2	u
severosyrského	severosyrský	k2eAgNnSc2d1	severosyrský
města	město	k1gNnSc2	město
Manbidž	Manbidž	k1gFnSc1	Manbidž
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
zde	zde	k6eAd1	zde
asi	asi	k9	asi
2000	[number]	k4	2000
příslušníků	příslušník	k1gMnPc2	příslušník
amerických	americký	k2eAgFnPc2d1	americká
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
<g/>
.	.	kIx.	.
</s>
<s>
Kurdové	Kurd	k1gMnPc1	Kurd
jsou	být	k5eAaImIp3nP	být
však	však	k9	však
od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
roku	rok	k1gInSc2	rok
2018	[number]	k4	2018
terčem	terč	k1gInSc7	terč
zesílených	zesílený	k2eAgInPc2d1	zesílený
útoků	útok	k1gInPc2	útok
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
města	město	k1gNnSc2	město
Afrín	Afrín	k1gInSc4	Afrín
na	na	k7c4	na
západ	západ	k1gInSc4	západ
od	od	k7c2	od
řeky	řeka	k1gFnSc2	řeka
Eufratu	Eufrat	k1gInSc2	Eufrat
<g/>
.	.	kIx.	.
</s>
<s>
Turecká	turecký	k2eAgFnSc1d1	turecká
armáda	armáda	k1gFnSc1	armáda
obsadila	obsadit	k5eAaPmAgFnS	obsadit
Afrín	Afrín	k1gInSc4	Afrín
a	a	k8xC	a
okolí	okolí	k1gNnSc4	okolí
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
určitých	určitý	k2eAgFnPc6d1	určitá
fázích	fáze	k1gFnPc6	fáze
ozbrojeného	ozbrojený	k2eAgInSc2d1	ozbrojený
konfliktu	konflikt	k1gInSc2	konflikt
spolupracovali	spolupracovat	k5eAaImAgMnP	spolupracovat
Kurdové	Kurd	k1gMnPc1	Kurd
také	také	k9	také
se	s	k7c7	s
syrskou	syrský	k2eAgFnSc7d1	Syrská
armádou	armáda	k1gFnSc7	armáda
<g/>
,	,	kIx,	,
zvláště	zvláště	k6eAd1	zvláště
při	při	k7c6	při
dobývání	dobývání	k1gNnSc6	dobývání
Aleppa	Aleppa	k1gFnSc1	Aleppa
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
konfliktu	konflikt	k1gInSc2	konflikt
stojí	stát	k5eAaImIp3nS	stát
větší	veliký	k2eAgInSc4d2	veliký
počet	počet	k1gInSc4	počet
skupin	skupina	k1gFnPc2	skupina
s	s	k7c7	s
různými	různý	k2eAgInPc7d1	různý
cíli	cíl	k1gInPc7	cíl
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
jednak	jednak	k8xC	jednak
relativně	relativně	k6eAd1	relativně
umírnění	umírněný	k2eAgMnPc1d1	umírněný
rebelové	rebel	k1gMnPc1	rebel
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
požadují	požadovat	k5eAaImIp3nP	požadovat
ukončení	ukončení	k1gNnSc4	ukončení
téměř	téměř	k6eAd1	téměř
pět	pět	k4xCc4	pět
desetiletí	desetiletí	k1gNnPc2	desetiletí
trvající	trvající	k2eAgFnSc2d1	trvající
vlády	vláda	k1gFnSc2	vláda
Socialistické	socialistický	k2eAgFnSc2d1	socialistická
strany	strana	k1gFnSc2	strana
arabské	arabský	k2eAgFnSc2d1	arabská
obrody	obroda	k1gFnSc2	obroda
a	a	k8xC	a
svržení	svržení	k1gNnSc4	svržení
vlády	vláda	k1gFnSc2	vláda
prezidenta	prezident	k1gMnSc2	prezident
Asada	Asad	k1gMnSc2	Asad
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jejich	jejich	k3xOp3gFnSc6	jejich
straně	strana	k1gFnSc6	strana
bojují	bojovat	k5eAaImIp3nP	bojovat
především	především	k9	především
většinoví	většinový	k2eAgMnPc1d1	většinový
sunnité	sunnita	k1gMnPc1	sunnita
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
dostávají	dostávat	k5eAaImIp3nP	dostávat
nebo	nebo	k8xC	nebo
dostávali	dostávat	k5eAaImAgMnP	dostávat
podporu	podpora	k1gFnSc4	podpora
např.	např.	kA	např.
od	od	k7c2	od
Saudské	saudský	k2eAgFnSc2d1	Saudská
Arábie	Arábie	k1gFnSc2	Arábie
a	a	k8xC	a
Kataru	katar	k1gInSc2	katar
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
jak	jak	k6eAd1	jak
finanční	finanční	k2eAgInSc1d1	finanční
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
ve	v	k7c6	v
formě	forma	k1gFnSc6	forma
vojenské	vojenský	k2eAgFnSc2d1	vojenská
výzbroje	výzbroj	k1gFnSc2	výzbroj
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
skupiny	skupina	k1gFnPc1	skupina
jsou	být	k5eAaImIp3nP	být
nebo	nebo	k8xC	nebo
byly	být	k5eAaImAgFnP	být
podporovány	podporovat	k5eAaImNgFnP	podporovat
také	také	k9	také
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
<g/>
,	,	kIx,	,
Tureckem	Turecko	k1gNnSc7	Turecko
a	a	k8xC	a
Francií	Francie	k1gFnSc7	Francie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Důležitou	důležitý	k2eAgFnSc7d1	důležitá
povstaleckou	povstalecký	k2eAgFnSc7d1	povstalecká
silou	síla	k1gFnSc7	síla
byly	být	k5eAaImAgFnP	být
až	až	k6eAd1	až
zhruba	zhruba	k6eAd1	zhruba
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2017	[number]	k4	2017
extrémní	extrémní	k2eAgMnPc1d1	extrémní
islamisté	islamista	k1gMnPc1	islamista
usilující	usilující	k2eAgMnSc1d1	usilující
o	o	k7c4	o
obnovení	obnovení	k1gNnSc4	obnovení
kalifátu	kalifát	k1gInSc2	kalifát
či	či	k8xC	či
vybudování	vybudování	k1gNnSc2	vybudování
jiné	jiný	k2eAgFnSc2d1	jiná
pravé	pravý	k2eAgFnSc2d1	pravá
islámské	islámský	k2eAgFnSc2d1	islámská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Tito	tento	k3xDgMnPc1	tento
jsou	být	k5eAaImIp3nP	být
reprezentováni	reprezentovat	k5eAaImNgMnP	reprezentovat
navzájem	navzájem	k6eAd1	navzájem
soupeřícími	soupeřící	k2eAgFnPc7d1	soupeřící
uskupeními	uskupení	k1gNnPc7	uskupení
Islámským	islámský	k2eAgInSc7d1	islámský
státem	stát	k1gInSc7	stát
(	(	kIx(	(
<g/>
IS	IS	kA	IS
<g/>
)	)	kIx)	)
a	a	k8xC	a
Al-Káidou	Al-Káida	k1gFnSc7	Al-Káida
<g/>
.	.	kIx.	.
</s>
<s>
Al-Káida	Al-Káida	k1gFnSc1	Al-Káida
se	se	k3xPyFc4	se
postupem	postup	k1gInSc7	postup
času	čas	k1gInSc2	čas
přetvořila	přetvořit	k5eAaPmAgFnS	přetvořit
ve	v	k7c6	v
Frontu	front	k1gInSc6	front
an-Nusrá	an-Nusrat	k5eAaPmIp3nS	an-Nusrat
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
její	její	k3xOp3gFnSc6	její
straně	strana	k1gFnSc6	strana
bojují	bojovat	k5eAaImIp3nP	bojovat
jednak	jednak	k8xC	jednak
místní	místní	k2eAgMnPc1d1	místní
radikálové	radikál	k1gMnPc1	radikál
<g/>
,	,	kIx,	,
jednak	jednak	k8xC	jednak
mudžáheddíni	mudžáheddín	k1gMnPc1	mudžáheddín
z	z	k7c2	z
celého	celý	k2eAgInSc2d1	celý
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
a	a	k8xC	a
podporu	podpora	k1gFnSc4	podpora
dostávají	dostávat	k5eAaImIp3nP	dostávat
-	-	kIx~	-
na	na	k7c6	na
neoficiální	neoficiální	k2eAgFnSc6d1	neoficiální
úrovni	úroveň	k1gFnSc6	úroveň
od	od	k7c2	od
tamních	tamní	k2eAgInPc2d1	tamní
radikálních	radikální	k2eAgInPc2d1	radikální
kruhů	kruh	k1gInPc2	kruh
-	-	kIx~	-
ze	z	k7c2	z
Saudské	saudský	k2eAgFnSc2d1	Saudská
Arábie	Arábie	k1gFnSc2	Arábie
<g/>
,	,	kIx,	,
Kataru	katar	k1gInSc2	katar
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
arabských	arabský	k2eAgInPc2d1	arabský
států	stát	k1gInPc2	stát
a	a	k8xC	a
z	z	k7c2	z
počátku	počátek	k1gInSc2	počátek
také	také	k9	také
od	od	k7c2	od
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Islámský	islámský	k2eAgInSc1d1	islámský
stát	stát	k1gInSc1	stát
je	být	k5eAaImIp3nS	být
terčem	terč	k1gInSc7	terč
útoků	útok	k1gInPc2	útok
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
celé	celý	k2eAgFnSc2d1	celá
řady	řada	k1gFnSc2	řada
zemí	zem	k1gFnPc2	zem
<g/>
,	,	kIx,	,
počínaje	počínaje	k7c7	počínaje
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
a	a	k8xC	a
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
konče	konče	k7c7	konče
Tureckem	Turecko	k1gNnSc7	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
Rakka	Rakko	k1gNnSc2	Rakko
bylo	být	k5eAaImAgNnS	být
16	[number]	k4	16
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2017	[number]	k4	2017
osvobozeno	osvobodit	k5eAaPmNgNnS	osvobodit
bojovníky	bojovník	k1gMnPc7	bojovník
Syrských	syrský	k2eAgFnPc2d1	Syrská
demokratických	demokratický	k2eAgFnPc2d1	demokratická
sil	síla	k1gFnPc2	síla
pod	pod	k7c7	pod
kurdským	kurdský	k2eAgNnSc7d1	kurdské
velením	velení	k1gNnSc7	velení
a	a	k8xC	a
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
IS	IS	kA	IS
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
již	již	k6eAd1	již
téměř	téměř	k6eAd1	téměř
poražen	porazit	k5eAaPmNgMnS	porazit
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
v	v	k7c6	v
sousedním	sousední	k2eAgInSc6d1	sousední
Iráku	Irák	k1gInSc6	Irák
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2016	[number]	k4	2016
se	se	k3xPyFc4	se
syrské	syrský	k2eAgFnSc3d1	Syrská
armádě	armáda	k1gFnSc3	armáda
podařilo	podařit	k5eAaPmAgNnS	podařit
zcela	zcela	k6eAd1	zcela
obsadit	obsadit	k5eAaPmF	obsadit
ty	ten	k3xDgFnPc4	ten
části	část	k1gFnPc4	část
druhého	druhý	k4xOgNnSc2	druhý
největšího	veliký	k2eAgNnSc2d3	veliký
syrského	syrský	k2eAgNnSc2d1	syrské
města	město	k1gNnSc2	město
Aleppa	Aleppa	k1gFnSc1	Aleppa
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
byly	být	k5eAaImAgFnP	být
do	do	k7c2	do
té	ten	k3xDgFnSc2	ten
doby	doba	k1gFnSc2	doba
kontrolovány	kontrolovat	k5eAaImNgFnP	kontrolovat
opozicí	opozice	k1gFnSc7	opozice
<g/>
.	.	kIx.	.
</s>
<s>
Bitva	bitva	k1gFnSc1	bitva
o	o	k7c4	o
Aleppo	Aleppa	k1gFnSc5	Aleppa
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
začala	začít	k5eAaPmAgFnS	začít
již	již	k6eAd1	již
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
tím	ten	k3xDgNnSc7	ten
skončila	skončit	k5eAaPmAgFnS	skončit
<g/>
.	.	kIx.	.
</s>
<s>
Byla	být	k5eAaImAgFnS	být
doposud	doposud	k6eAd1	doposud
největší	veliký	k2eAgFnSc7d3	veliký
bitvou	bitva	k1gFnSc7	bitva
syrské	syrský	k2eAgFnSc2d1	Syrská
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Okolnosti	okolnost	k1gFnPc1	okolnost
a	a	k8xC	a
začátky	začátek	k1gInPc1	začátek
nepokojů	nepokoj	k1gInPc2	nepokoj
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
==	==	k?	==
</s>
</p>
<p>
<s>
Sucha	sucho	k1gNnPc1	sucho
<g/>
,	,	kIx,	,
vyčerpání	vyčerpání	k1gNnSc6	vyčerpání
zásob	zásoba	k1gFnPc2	zásoba
spodní	spodní	k2eAgFnSc2d1	spodní
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
velký	velký	k2eAgInSc4d1	velký
nárůst	nárůst	k1gInSc4	nárůst
obyvatel	obyvatel	k1gMnPc2	obyvatel
přispěly	přispět	k5eAaPmAgFnP	přispět
ke	k	k7c3	k
zhoršení	zhoršení	k1gNnSc3	zhoršení
situace	situace	k1gFnSc2	situace
a	a	k8xC	a
vzniku	vznik	k1gInSc2	vznik
nepokojů	nepokoj	k1gInPc2	nepokoj
v	v	k7c4	v
Sýrii	Sýrie	k1gFnSc4	Sýrie
<g/>
.	.	kIx.	.
<g/>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2011	[number]	k4	2011
usiluje	usilovat	k5eAaImIp3nS	usilovat
politicky	politicky	k6eAd1	politicky
nesourodá	sourodý	k2eNgFnSc1d1	nesourodá
syrská	syrský	k2eAgFnSc1d1	Syrská
opozice	opozice	k1gFnSc1	opozice
o	o	k7c4	o
svržení	svržení	k1gNnSc4	svržení
prezidenta	prezident	k1gMnSc2	prezident
Bašára	Bašár	k1gMnSc2	Bašár
al-Asada	al-Asada	k1gFnSc1	al-Asada
<g/>
,	,	kIx,	,
aniž	aniž	k8xS	aniž
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
jasno	jasno	k6eAd1	jasno
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
toho	ten	k3xDgMnSc4	ten
dosáhnout	dosáhnout	k5eAaPmF	dosáhnout
<g/>
,	,	kIx,	,
vyjma	vyjma	k7c2	vyjma
těch	ten	k3xDgFnPc2	ten
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
preferují	preferovat	k5eAaImIp3nP	preferovat
ozbrojený	ozbrojený	k2eAgInSc4d1	ozbrojený
boj	boj	k1gInSc4	boj
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
protestů	protest	k1gInPc2	protest
přislíbil	přislíbit	k5eAaPmAgMnS	přislíbit
Asad	Asad	k1gMnSc1	Asad
některé	některý	k3yIgFnSc2	některý
reformy	reforma	k1gFnSc2	reforma
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
neuskutečnil	uskutečnit	k5eNaPmAgMnS	uskutečnit
je	být	k5eAaImIp3nS	být
<g/>
.	.	kIx.	.
</s>
<s>
Jediným	jediný	k2eAgMnSc7d1	jediný
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
dosti	dosti	k6eAd1	dosti
významným	významný	k2eAgInSc7d1	významný
ústupkem	ústupek	k1gInSc7	ústupek
bylo	být	k5eAaImAgNnS	být
zrušení	zrušení	k1gNnSc1	zrušení
výjimečného	výjimečný	k2eAgInSc2d1	výjimečný
stavu	stav	k1gInSc2	stav
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
platil	platit	k5eAaImAgMnS	platit
od	od	k7c2	od
nástupu	nástup	k1gInSc2	nástup
levicové	levicový	k2eAgFnSc2d1	levicová
strany	strana	k1gFnSc2	strana
Baas	Baasa	k1gFnPc2	Baasa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1963	[number]	k4	1963
a	a	k8xC	a
odvolán	odvolat	k5eAaPmNgMnS	odvolat
byl	být	k5eAaImAgMnS	být
al-Asadem	al-Asad	k1gInSc7	al-Asad
19	[number]	k4	19
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2011	[number]	k4	2011
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
však	však	k9	však
vládní	vládní	k2eAgFnPc4d1	vládní
armádní	armádní	k2eAgFnPc4d1	armádní
a	a	k8xC	a
policejní	policejní	k2eAgFnPc4d1	policejní
jednotky	jednotka	k1gFnPc4	jednotka
proti	proti	k7c3	proti
demonstrantům	demonstrant	k1gMnPc3	demonstrant
tvrdě	tvrdě	k6eAd1	tvrdě
zasahovaly	zasahovat	k5eAaImAgFnP	zasahovat
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
pěti	pět	k4xCc2	pět
měsíců	měsíc	k1gInPc2	měsíc
(	(	kIx(	(
<g/>
od	od	k7c2	od
března	březen	k1gInSc2	březen
do	do	k7c2	do
začátku	začátek	k1gInSc2	začátek
srpna	srpen	k1gInSc2	srpen
2011	[number]	k4	2011
<g/>
)	)	kIx)	)
mělo	mít	k5eAaImAgNnS	mít
podle	podle	k7c2	podle
nepotvrzených	potvrzený	k2eNgMnPc2d1	nepotvrzený
odhadů	odhad	k1gInPc2	odhad
britské	britský	k2eAgFnSc2d1	britská
televizní	televizní	k2eAgFnSc2d1	televizní
stanice	stanice	k1gFnSc2	stanice
BBC	BBC	kA	BBC
při	při	k7c6	při
demonstracích	demonstrace	k1gFnPc6	demonstrace
zahynout	zahynout	k5eAaPmF	zahynout
více	hodně	k6eAd2	hodně
než	než	k8xS	než
1600	[number]	k4	1600
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
vydala	vydat	k5eAaPmAgFnS	vydat
4	[number]	k4	4
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2011	[number]	k4	2011
prohlášení	prohlášení	k1gNnSc4	prohlášení
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
odsuzovalo	odsuzovat	k5eAaImAgNnS	odsuzovat
útoky	útok	k1gInPc4	útok
syrské	syrský	k2eAgFnSc2d1	Syrská
vlády	vláda	k1gFnSc2	vláda
na	na	k7c4	na
civilisty	civilista	k1gMnPc4	civilista
a	a	k8xC	a
také	také	k9	také
porušování	porušování	k1gNnSc1	porušování
lidských	lidský	k2eAgNnPc2d1	lidské
práv	právo	k1gNnPc2	právo
a	a	k8xC	a
vyzývalo	vyzývat	k5eAaImAgNnS	vyzývat
k	k	k7c3	k
zastavení	zastavení	k1gNnSc3	zastavení
násilí	násilí	k1gNnSc2	násilí
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
ke	k	k7c3	k
zdrženlivosti	zdrženlivost	k1gFnSc3	zdrženlivost
a	a	k8xC	a
zdržení	zdržení	k1gNnSc3	zdržení
se	se	k3xPyFc4	se
odvetných	odvetný	k2eAgFnPc2d1	odvetná
akcí	akce	k1gFnPc2	akce
opozicí	opozice	k1gFnPc2	opozice
<g/>
,	,	kIx,	,
především	především	k9	především
při	při	k7c6	při
útocích	útok	k1gInPc6	útok
na	na	k7c4	na
státní	státní	k2eAgFnPc4d1	státní
instituce	instituce	k1gFnPc4	instituce
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
prohlášení	prohlášení	k1gNnSc1	prohlášení
však	však	k9	však
nemělo	mít	k5eNaImAgNnS	mít
takovou	takový	k3xDgFnSc4	takový
váhu	váha	k1gFnSc4	váha
<g/>
,	,	kIx,	,
jakou	jaký	k3yIgFnSc4	jaký
by	by	kYmCp3nS	by
měla	mít	k5eAaImAgFnS	mít
její	její	k3xOp3gFnSc1	její
rezoluce	rezoluce	k1gFnSc1	rezoluce
<g/>
,	,	kIx,	,
na	na	k7c4	na
které	který	k3yQgFnPc4	který
se	se	k3xPyFc4	se
ale	ale	k9	ale
Rada	rada	k1gFnSc1	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
nedokázala	dokázat	k5eNaPmAgFnS	dokázat
shodnout	shodnout	k5eAaPmF	shodnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
počátku	počátek	k1gInSc2	počátek
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
hrozbu	hrozba	k1gFnSc4	hrozba
pro	pro	k7c4	pro
politickou	politický	k2eAgFnSc4d1	politická
stabilitu	stabilita	k1gFnSc4	stabilita
v	v	k7c6	v
sousedním	sousední	k2eAgInSc6d1	sousední
Libanonu	Libanon	k1gInSc6	Libanon
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
je	být	k5eAaImIp3nS	být
syrská	syrský	k2eAgFnSc1d1	Syrská
vláda	vláda	k1gFnSc1	vláda
patronem	patron	k1gMnSc7	patron
silného	silný	k2eAgNnSc2d1	silné
protiizraelského	protiizraelský	k2eAgNnSc2d1	protiizraelské
hnutí	hnutí	k1gNnSc2	hnutí
Hizballáh	Hizballáh	k1gInSc1	Hizballáh
<g/>
.	.	kIx.	.
</s>
<s>
Rovněž	rovněž	k9	rovněž
panují	panovat	k5eAaImIp3nP	panovat
obavy	obava	k1gFnPc1	obava
<g/>
,	,	kIx,	,
že	že	k8xS	že
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
mohla	moct	k5eAaImAgFnS	moct
vystupňovat	vystupňovat	k5eAaPmF	vystupňovat
konfrontace	konfrontace	k1gFnSc1	konfrontace
na	na	k7c6	na
syrsko-izraelské	syrskozraelský	k2eAgFnSc6d1	syrsko-izraelský
hranici	hranice	k1gFnSc6	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Důležitým	důležitý	k2eAgMnSc7d1	důležitý
spojencem	spojenec	k1gMnSc7	spojenec
syrské	syrský	k2eAgFnSc2d1	Syrská
vlády	vláda	k1gFnSc2	vláda
je	být	k5eAaImIp3nS	být
právě	právě	k9	právě
libanonské	libanonský	k2eAgNnSc1d1	libanonské
hnutí	hnutí	k1gNnSc1	hnutí
Hizballáh	Hizballáh	k1gInSc1	Hizballáh
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
Asadem	Asad	k1gInSc7	Asad
také	také	k9	také
sympatizuje	sympatizovat	k5eAaImIp3nS	sympatizovat
současná	současný	k2eAgFnSc1d1	současná
irácká	irácký	k2eAgFnSc1d1	irácká
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
je	být	k5eAaImIp3nS	být
vedena	vést	k5eAaImNgFnS	vést
příslušníky	příslušník	k1gMnPc7	příslušník
ší	ší	k?	ší
<g/>
'	'	kIx"	'
<g/>
itské	itský	k2eAgFnSc2d1	itská
většiny	většina	k1gFnSc2	většina
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Írán	Írán	k1gInSc1	Írán
syrské	syrský	k2eAgFnSc2d1	Syrská
vládě	vláda	k1gFnSc3	vláda
významně	významně	k6eAd1	významně
pomáhá	pomáhat	k5eAaImIp3nS	pomáhat
vojensky	vojensky	k6eAd1	vojensky
i	i	k9	i
finančně	finančně	k6eAd1	finančně
<g/>
.	.	kIx.	.
</s>
<s>
Hlavního	hlavní	k2eAgMnSc4d1	hlavní
spojence	spojenec	k1gMnSc4	spojenec
syrské	syrský	k2eAgFnSc2d1	Syrská
vlády	vláda	k1gFnSc2	vláda
ovšem	ovšem	k9	ovšem
představuje	představovat	k5eAaImIp3nS	představovat
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
dodává	dodávat	k5eAaImIp3nS	dodávat
zbraně	zbraň	k1gFnPc4	zbraň
syrské	syrský	k2eAgFnSc2d1	Syrská
armádě	armáda	k1gFnSc3	armáda
a	a	k8xC	a
navíc	navíc	k6eAd1	navíc
jí	jíst	k5eAaImIp3nS	jíst
pomocí	pomocí	k7c2	pomocí
svých	svůj	k3xOyFgMnPc2	svůj
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
rozmístěných	rozmístěný	k2eAgFnPc2d1	rozmístěná
leteckých	letecký	k2eAgFnPc2d1	letecká
sil	síla	k1gFnPc2	síla
poskytuje	poskytovat	k5eAaImIp3nS	poskytovat
vzdušnou	vzdušný	k2eAgFnSc4d1	vzdušná
podporu	podpora	k1gFnSc4	podpora
v	v	k7c6	v
boji	boj	k1gInSc6	boj
s	s	k7c7	s
povstalci	povstalec	k1gMnPc7	povstalec
<g/>
,	,	kIx,	,
označovanými	označovaný	k2eAgInPc7d1	označovaný
za	za	k7c4	za
teroristy	terorista	k1gMnPc4	terorista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Všechny	všechen	k3xTgFnPc4	všechen
hlavní	hlavní	k2eAgFnPc4d1	hlavní
mocenské	mocenský	k2eAgFnPc4d1	mocenská
složky	složka	k1gFnPc4	složka
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
mají	mít	k5eAaImIp3nP	mít
v	v	k7c6	v
rukou	ruka	k1gFnPc6	ruka
příslušníci	příslušník	k1gMnPc1	příslušník
alávitské	alávitský	k2eAgFnSc2d1	alávitský
komunity	komunita	k1gFnSc2	komunita
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
syrských	syrský	k2eAgMnPc2d1	syrský
sunnitů	sunnita	k1gMnPc2	sunnita
také	také	k9	také
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
vládnoucí	vládnoucí	k2eAgFnSc4d1	vládnoucí
elitu	elita	k1gFnSc4	elita
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
v	v	k7c6	v
ozbrojených	ozbrojený	k2eAgFnPc6d1	ozbrojená
složkách	složka	k1gFnPc6	složka
nejsou	být	k5eNaImIp3nP	být
na	na	k7c6	na
rozhodujících	rozhodující	k2eAgInPc6d1	rozhodující
postech	post	k1gInPc6	post
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Průběh	průběh	k1gInSc4	průběh
nepokojů	nepokoj	k1gInPc2	nepokoj
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
==	==	k?	==
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
nepokoje	nepokoj	k1gInSc2	nepokoj
pokračovaly	pokračovat	k5eAaImAgFnP	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
šéfa	šéf	k1gMnSc2	šéf
syrské	syrský	k2eAgFnSc2d1	Syrská
diplomacie	diplomacie	k1gFnSc2	diplomacie
<g/>
,	,	kIx,	,
ministra	ministr	k1gMnSc2	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Valída	Valíd	k1gMnSc2	Valíd
Mualima	Mualim	k1gMnSc2	Mualim
<g/>
,	,	kIx,	,
vláda	vláda	k1gFnSc1	vláda
byla	být	k5eAaImAgFnS	být
připravena	připraven	k2eAgFnSc1d1	připravena
dodržet	dodržet	k5eAaPmF	dodržet
podmínky	podmínka	k1gFnPc4	podmínka
mírového	mírový	k2eAgInSc2d1	mírový
plánu	plán	k1gInSc2	plán
zmocněnce	zmocněnec	k1gMnSc2	zmocněnec
OSN	OSN	kA	OSN
Kofiho	Kofi	k1gMnSc2	Kofi
Annana	Annan	k1gMnSc2	Annan
<g/>
.	.	kIx.	.
</s>
<s>
Asadův	Asadův	k2eAgInSc4d1	Asadův
režim	režim	k1gInSc4	režim
ale	ale	k8xC	ale
ignoroval	ignorovat	k5eAaImAgMnS	ignorovat
stažení	stažení	k1gNnSc4	stažení
vojenských	vojenský	k2eAgFnPc2d1	vojenská
sil	síla	k1gFnPc2	síla
do	do	k7c2	do
10	[number]	k4	10
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2012	[number]	k4	2012
z	z	k7c2	z
obydlených	obydlený	k2eAgNnPc2d1	obydlené
míst	místo	k1gNnPc2	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zemi	zem	k1gFnSc6	zem
začala	začít	k5eAaPmAgFnS	začít
působit	působit	k5eAaImF	působit
skupina	skupina	k1gFnSc1	skupina
pozorovatelů	pozorovatel	k1gMnPc2	pozorovatel
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
dostala	dostat	k5eAaPmAgFnS	dostat
za	za	k7c4	za
úkol	úkol	k1gInSc4	úkol
dohlížet	dohlížet	k5eAaImF	dohlížet
na	na	k7c4	na
dodržování	dodržování	k1gNnSc4	dodržování
klidu	klid	k1gInSc2	klid
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
stále	stále	k6eAd1	stále
probíhaly	probíhat	k5eAaImAgFnP	probíhat
demonstrace	demonstrace	k1gFnPc1	demonstrace
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mnohdy	mnohdy	k6eAd1	mnohdy
provázely	provázet	k5eAaImAgFnP	provázet
velké	velký	k2eAgFnPc1d1	velká
násilnosti	násilnost	k1gFnPc1	násilnost
<g/>
,	,	kIx,	,
střelba	střelba	k1gFnSc1	střelba
a	a	k8xC	a
ztráty	ztráta	k1gFnPc1	ztráta
na	na	k7c6	na
životech	život	k1gInPc6	život
jak	jak	k8xS	jak
povstalců	povstalec	k1gMnPc2	povstalec
resp.	resp.	kA	resp.
demonstrantů	demonstrant	k1gMnPc2	demonstrant
<g/>
,	,	kIx,	,
tak	tak	k9	tak
i	i	k9	i
vládních	vládní	k2eAgMnPc2d1	vládní
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
parlamentních	parlamentní	k2eAgFnPc6d1	parlamentní
volbách	volba	k1gFnPc6	volba
v	v	k7c6	v
květnu	květen	k1gInSc6	květen
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
vyhlášených	vyhlášený	k2eAgInPc2d1	vyhlášený
Asadovým	Asadův	k2eAgInSc7d1	Asadův
režimem	režim	k1gInSc7	režim
<g/>
,	,	kIx,	,
vyhrála	vyhrát	k5eAaPmAgFnS	vyhrát
přesvědčivě	přesvědčivě	k6eAd1	přesvědčivě
jeho	jeho	k3xOp3gFnSc1	jeho
strana	strana	k1gFnSc1	strana
Baas	Baasa	k1gFnPc2	Baasa
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
však	však	k9	však
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
na	na	k7c6	na
Západě	západ	k1gInSc6	západ
podezření	podezření	k1gNnSc2	podezření
<g/>
,	,	kIx,	,
že	že	k8xS	že
výsledky	výsledek	k1gInPc1	výsledek
voleb	volba	k1gFnPc2	volba
byly	být	k5eAaImAgInP	být
Asadem	Asad	k1gInSc7	Asad
a	a	k8xC	a
jeho	jeho	k3xOp3gMnPc7	jeho
stoupenci	stoupenec	k1gMnPc7	stoupenec
výrazně	výrazně	k6eAd1	výrazně
zmanipulovány	zmanipulovat	k5eAaPmNgFnP	zmanipulovat
<g/>
.	.	kIx.	.
</s>
<s>
Výsledky	výsledek	k1gInPc1	výsledek
voleb	volba	k1gFnPc2	volba
byly	být	k5eAaImAgInP	být
uznány	uznat	k5eAaPmNgInP	uznat
pouze	pouze	k6eAd1	pouze
Íránem	Írán	k1gInSc7	Írán
<g/>
,	,	kIx,	,
Ruskem	Rusko	k1gNnSc7	Rusko
a	a	k8xC	a
Venezuelou	Venezuela	k1gFnSc7	Venezuela
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
polovině	polovina	k1gFnSc6	polovina
května	květno	k1gNnSc2	květno
připustil	připustit	k5eAaPmAgMnS	připustit
generální	generální	k2eAgMnSc1d1	generální
tajemník	tajemník	k1gMnSc1	tajemník
OSN	OSN	kA	OSN
Pan	Pan	k1gMnSc1	Pan
Ki-mun	Kiun	k1gMnSc1	Ki-mun
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c7	za
nedávnými	dávný	k2eNgInPc7d1	nedávný
atentáty	atentát	k1gInPc7	atentát
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
nejspíše	nejspíše	k9	nejspíše
stála	stát	k5eAaImAgFnS	stát
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
islámská	islámský	k2eAgFnSc1d1	islámská
teroristická	teroristický	k2eAgFnSc1d1	teroristická
síť	síť	k1gFnSc1	síť
Al-Káida	Al-Káida	k1gFnSc1	Al-Káida
<g/>
.	.	kIx.	.
<g/>
Proti	proti	k7c3	proti
odpůrcům	odpůrce	k1gMnPc3	odpůrce
režimu	režim	k1gInSc2	režim
bylo	být	k5eAaImAgNnS	být
nasazeno	nasadit	k5eAaPmNgNnS	nasadit
i	i	k8xC	i
syrské	syrský	k2eAgNnSc1d1	syrské
letectvo	letectvo	k1gNnSc1	letectvo
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgInPc4	první
důkazy	důkaz	k1gInPc4	důkaz
o	o	k7c4	o
nasazení	nasazení	k1gNnSc4	nasazení
strojů	stroj	k1gInPc2	stroj
Mil	míle	k1gFnPc2	míle
Mi-	Mi-	k1gFnSc4	Mi-
<g/>
8	[number]	k4	8
<g/>
/	/	kIx~	/
<g/>
17	[number]	k4	17
v	v	k7c6	v
roli	role	k1gFnSc6	role
vrtulníků	vrtulník	k1gInPc2	vrtulník
palebné	palebný	k2eAgFnSc2d1	palebná
podpory	podpora	k1gFnSc2	podpora
u	u	k7c2	u
Azázu	Azáz	k1gInSc2	Azáz
se	se	k3xPyFc4	se
objevily	objevit	k5eAaPmAgFnP	objevit
22	[number]	k4	22
<g/>
.	.	kIx.	.
března	březen	k1gInSc2	březen
2012	[number]	k4	2012
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgNnSc1	první
nasazení	nasazení	k1gNnSc1	nasazení
bitevních	bitevní	k2eAgFnPc2d1	bitevní
Mil	míle	k1gFnPc2	míle
Mi-	Mi-	k1gFnPc2	Mi-
<g/>
25	[number]	k4	25
bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
u	u	k7c2	u
Rastanu	Rastan	k1gInSc2	Rastan
<g/>
.	.	kIx.	.
</s>
<s>
Asadovy	Asadův	k2eAgFnPc1d1	Asadova
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
se	se	k3xPyFc4	se
setkaly	setkat	k5eAaPmAgFnP	setkat
i	i	k9	i
s	s	k7c7	s
případy	případ	k1gInPc7	případ
dezerce	dezerce	k1gFnSc2	dezerce
<g/>
:	:	kIx,	:
10	[number]	k4	10
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
dezertoval	dezertovat	k5eAaBmAgMnS	dezertovat
743	[number]	k4	743
<g/>
.	.	kIx.	.
prapor	prapor	k1gInSc1	prapor
protiletadlové	protiletadlový	k2eAgFnSc2d1	protiletadlová
obrany	obrana	k1gFnSc2	obrana
u	u	k7c2	u
Talbisehu	Talbiseh	k1gInSc2	Talbiseh
severně	severně	k6eAd1	severně
od	od	k7c2	od
Homsu	Homs	k1gInSc2	Homs
a	a	k8xC	a
21	[number]	k4	21
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
dezertoval	dezertovat	k5eAaBmAgMnS	dezertovat
plukovník	plukovník	k1gMnSc1	plukovník
Hassan	Hassan	k1gMnSc1	Hassan
Hamada	Hamada	k1gFnSc1	Hamada
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
v	v	k7c6	v
letounu	letoun	k1gInSc6	letoun
MiG-	MiG-	k1gFnSc4	MiG-
<g/>
21	[number]	k4	21
uletěl	uletět	k5eAaPmAgInS	uletět
do	do	k7c2	do
Jordánska	Jordánsko	k1gNnSc2	Jordánsko
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
azyl	azyl	k1gInSc4	azyl
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Masakr	masakr	k1gInSc4	masakr
v	v	k7c4	v
Húlá	Húlé	k1gNnPc4	Húlé
===	===	k?	===
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
27	[number]	k4	27
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2012	[number]	k4	2012
informovali	informovat	k5eAaBmAgMnP	informovat
pozorovatelé	pozorovatel	k1gMnPc1	pozorovatel
OSN	OSN	kA	OSN
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Húlá	Húlá	k1gFnSc1	Húlá
se	se	k3xPyFc4	se
udál	udát	k5eAaPmAgMnS	udát
útok	útok	k1gInSc4	útok
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yQgInSc6	který
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
asi	asi	k9	asi
49	[number]	k4	49
dětí	dítě	k1gFnPc2	dítě
a	a	k8xC	a
59	[number]	k4	59
dospělých	dospělí	k1gMnPc2	dospělí
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
opozice	opozice	k1gFnSc2	opozice
byly	být	k5eAaImAgInP	být
za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
útok	útok	k1gInSc4	útok
zodpovědné	zodpovědný	k2eAgMnPc4d1	zodpovědný
syrská	syrský	k2eAgFnSc1d1	Syrská
armáda	armáda	k1gFnSc1	armáda
a	a	k8xC	a
provládní	provládní	k2eAgFnSc1d1	provládní
milice	milice	k1gFnSc1	milice
šabíha	šabíha	k1gFnSc1	šabíha
<g/>
.	.	kIx.	.
</s>
<s>
Syrská	syrský	k2eAgNnPc1d1	syrské
vládní	vládní	k2eAgNnPc1d1	vládní
média	médium	k1gNnPc1	médium
připsala	připsat	k5eAaPmAgNnP	připsat
vinu	vina	k1gFnSc4	vina
naopak	naopak	k6eAd1	naopak
opozici	opozice	k1gFnSc3	opozice
a	a	k8xC	a
Al-Káidě	Al-Káida	k1gFnSc3	Al-Káida
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
události	událost	k1gFnSc6	událost
v	v	k7c6	v
Húle	Húle	k1gNnSc6	Húle
prosazovaly	prosazovat	k5eAaImAgInP	prosazovat
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
nový	nový	k2eAgInSc1d1	nový
plán	plán	k1gInSc4	plán
na	na	k7c4	na
odstranění	odstranění	k1gNnSc4	odstranění
Bašára	Bašár	k1gMnSc2	Bašár
al-Asada	al-Asada	k1gFnSc1	al-Asada
<g/>
,	,	kIx,	,
mělo	mít	k5eAaImAgNnS	mít
jít	jít	k5eAaImF	jít
o	o	k7c4	o
řešení	řešení	k1gNnSc4	řešení
přijatelné	přijatelný	k2eAgNnSc4d1	přijatelné
pro	pro	k7c4	pro
syrskou	syrský	k2eAgFnSc4d1	Syrská
opozici	opozice	k1gFnSc4	opozice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
pro	pro	k7c4	pro
Rusko	Rusko	k1gNnSc4	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
plán	plán	k1gInSc1	plán
byl	být	k5eAaImAgInS	být
ve	v	k7c6	v
Washingtonu	Washington	k1gInSc6	Washington
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
v	v	k7c6	v
Moskvě	Moskva	k1gFnSc6	Moskva
nazván	nazván	k2eAgInSc1d1	nazván
"	"	kIx"	"
<g/>
jemenský	jemenský	k2eAgInSc1d1	jemenský
scénář	scénář	k1gInSc1	scénář
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Asad	Asad	k1gMnSc1	Asad
měl	mít	k5eAaImAgMnS	mít
odejít	odejít	k5eAaPmF	odejít
ze	z	k7c2	z
scény	scéna	k1gFnSc2	scéna
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
někdejší	někdejší	k2eAgMnSc1d1	někdejší
jemenský	jemenský	k2eAgMnSc1d1	jemenský
prezident	prezident	k1gMnSc1	prezident
Alí	Alí	k1gMnSc1	Alí
Abdalláh	Abdalláh	k1gMnSc1	Abdalláh
Sálih	Sálih	k1gMnSc1	Sálih
<g/>
,	,	kIx,	,
tedy	tedy	k9	tedy
řízeně	řízeně	k6eAd1	řízeně
a	a	k8xC	a
při	při	k7c6	při
zachování	zachování	k1gNnSc6	zachování
některých	některý	k3yIgInPc2	některý
prvků	prvek	k1gInPc2	prvek
dosavadního	dosavadní	k2eAgInSc2d1	dosavadní
režimu	režim	k1gInSc2	režim
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
v	v	k7c6	v
San	San	k1gFnSc6	San
<g/>
'	'	kIx"	'
<g/>
á	á	k0	á
převzal	převzít	k5eAaPmAgInS	převzít
Sálihovu	Sálihův	k2eAgFnSc4d1	Sálihův
roli	role	k1gFnSc4	role
jeho	jeho	k3xOp3gMnSc1	jeho
viceprezident	viceprezident	k1gMnSc1	viceprezident
Abdu	Abdus	k1gInSc2	Abdus
Rabbuh	Rabbuh	k1gMnSc1	Rabbuh
Mansúr	Mansúr	k1gMnSc1	Mansúr
Hádí	Hádí	k1gMnSc1	Hádí
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
ale	ale	k8xC	ale
s	s	k7c7	s
tímto	tento	k3xDgInSc7	tento
plánem	plán	k1gInSc7	plán
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
podpořil	podpořit	k5eAaPmAgInS	podpořit
také	také	k9	také
francouzský	francouzský	k2eAgMnSc1d1	francouzský
prezident	prezident	k1gMnSc1	prezident
François	François	k1gFnSc2	François
Hollande	Holland	k1gInSc5	Holland
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
nesouhlasilo	souhlasit	k5eNaImAgNnS	souhlasit
<g/>
.	.	kIx.	.
<g/>
Asad	Asad	k1gInSc1	Asad
dne	den	k1gInSc2	den
3	[number]	k4	3
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2012	[number]	k4	2012
vystoupil	vystoupit	k5eAaPmAgMnS	vystoupit
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
<g/>
.	.	kIx.	.
</s>
<s>
Obvinil	obvinit	k5eAaPmAgMnS	obvinit
zahraniční	zahraniční	k2eAgFnPc4d1	zahraniční
síly	síla	k1gFnPc4	síla
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
zničit	zničit	k5eAaPmF	zničit
Sýrii	Sýrie	k1gFnSc4	Sýrie
<g/>
,	,	kIx,	,
a	a	k8xC	a
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
za	za	k7c4	za
masakr	masakr	k1gInSc4	masakr
v	v	k7c4	v
Húlá	Húlé	k1gNnPc4	Húlé
<g/>
.	.	kIx.	.
</s>
<s>
Největším	veliký	k2eAgInSc7d3	veliký
problémem	problém	k1gInSc7	problém
země	zem	k1gFnSc2	zem
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
terorismus	terorismus	k1gInSc1	terorismus
<g/>
,	,	kIx,	,
vůči	vůči	k7c3	vůči
němuž	jenž	k3xRgNnSc3	jenž
bude	být	k5eAaImBp3nS	být
i	i	k9	i
nadále	nadále	k6eAd1	nadále
stát	stát	k5eAaPmF	stát
bojovat	bojovat	k5eAaImF	bojovat
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
proti	proti	k7c3	proti
těm	ten	k3xDgMnPc3	ten
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
rozdmýchávají	rozdmýchávat	k5eAaImIp3nP	rozdmýchávat
chaos	chaos	k1gInSc4	chaos
jako	jako	k8xC	jako
živnou	živný	k2eAgFnSc4d1	živná
půdu	půda	k1gFnSc4	půda
teroru	teror	k1gInSc2	teror
<g/>
.	.	kIx.	.
</s>
<s>
Asad	Asad	k1gMnSc1	Asad
také	také	k9	také
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
teroristé	terorista	k1gMnPc1	terorista
verbují	verbovat	k5eAaImIp3nP	verbovat
teenagery	teenager	k1gMnPc4	teenager
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
stávají	stávat	k5eAaImIp3nP	stávat
zabijáky	zabiják	k1gInPc1	zabiják
už	už	k9	už
za	za	k7c4	za
2000	[number]	k4	2000
syrských	syrský	k2eAgFnPc2d1	Syrská
liber	libra	k1gFnPc2	libra
(	(	kIx(	(
<g/>
650	[number]	k4	650
korun	koruna	k1gFnPc2	koruna
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zvláštního	zvláštní	k2eAgMnSc2d1	zvláštní
komisaře	komisař	k1gMnSc2	komisař
OSN	OSN	kA	OSN
Kofiho	Kofi	k1gMnSc2	Kofi
Annana	Annan	k1gMnSc2	Annan
stála	stát	k5eAaImAgFnS	stát
Sýrie	Sýrie	k1gFnSc1	Sýrie
již	již	k6eAd1	již
tehdy	tehdy	k6eAd1	tehdy
na	na	k7c6	na
pokraji	pokraj	k1gInSc6	pokraj
regulérní	regulérní	k2eAgFnSc2d1	regulérní
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
<g/>
Vyšetřování	vyšetřování	k1gNnSc1	vyšetřování
OSN	OSN	kA	OSN
dospělo	dochvít	k5eAaPmAgNnS	dochvít
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2012	[number]	k4	2012
k	k	k7c3	k
závěru	závěr	k1gInSc3	závěr
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
masakr	masakr	k1gInSc4	masakr
v	v	k7c6	v
Húlá	Húlé	k1gNnPc1	Húlé
byly	být	k5eAaImAgFnP	být
zodpovědné	zodpovědný	k2eAgFnPc4d1	zodpovědná
polovojenské	polovojenský	k2eAgFnPc4d1	polovojenská
a	a	k8xC	a
armádní	armádní	k2eAgFnPc4d1	armádní
jednotky	jednotka	k1gFnPc4	jednotka
režimu	režim	k1gInSc2	režim
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Sestřelení	sestřelení	k1gNnSc2	sestřelení
tureckého	turecký	k2eAgNnSc2d1	turecké
letadla	letadlo	k1gNnSc2	letadlo
===	===	k?	===
</s>
</p>
<p>
<s>
22	[number]	k4	22
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2012	[number]	k4	2012
byl	být	k5eAaImAgInS	být
sestřelen	sestřelen	k2eAgInSc1d1	sestřelen
turecký	turecký	k2eAgInSc1d1	turecký
průzkumný	průzkumný	k2eAgInSc1d1	průzkumný
letoun	letoun	k1gInSc1	letoun
RF-4E	RF-4E	k1gFnPc2	RF-4E
Phantom	Phantom	k1gInSc1	Phantom
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
pilot	pilot	k1gInSc4	pilot
i	i	k8xC	i
operátor	operátor	k1gInSc4	operátor
zahynuli	zahynout	k5eAaPmAgMnP	zahynout
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
syrské	syrský	k2eAgFnSc2d1	Syrská
armády	armáda	k1gFnSc2	armáda
neidentifikovaný	identifikovaný	k2eNgInSc4d1	neidentifikovaný
vzdušný	vzdušný	k2eAgInSc4d1	vzdušný
objekt	objekt	k1gInSc4	objekt
narušil	narušit	k5eAaPmAgMnS	narušit
syrské	syrský	k2eAgFnSc2d1	Syrská
teritoriální	teritoriální	k2eAgFnSc2d1	teritoriální
vody	voda	k1gFnSc2	voda
ze	z	k7c2	z
západu	západ	k1gInSc2	západ
v	v	k7c4	v
11	[number]	k4	11
<g/>
:	:	kIx,	:
<g/>
40	[number]	k4	40
místního	místní	k2eAgInSc2d1	místní
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Syrská	syrský	k2eAgFnSc1d1	Syrská
vzdušná	vzdušný	k2eAgFnSc1d1	vzdušná
obrana	obrana	k1gFnSc1	obrana
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
přímý	přímý	k2eAgInSc4d1	přímý
zásah	zásah	k1gInSc4	zásah
asi	asi	k9	asi
1	[number]	k4	1
km	km	kA	km
od	od	k7c2	od
pobřeží	pobřeží	k1gNnSc2	pobřeží
<g/>
.	.	kIx.	.
</s>
<s>
Turecká	turecký	k2eAgFnSc1d1	turecká
armáda	armáda	k1gFnSc1	armáda
ztratila	ztratit	k5eAaPmAgFnS	ztratit
rádiový	rádiový	k2eAgInSc4d1	rádiový
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
F-4	F-4	k1gFnSc7	F-4
v	v	k7c6	v
11	[number]	k4	11
<g/>
:	:	kIx,	:
<g/>
58	[number]	k4	58
místního	místní	k2eAgInSc2d1	místní
času	čas	k1gInSc2	čas
<g/>
.	.	kIx.	.
</s>
<s>
Letoun	letoun	k1gInSc1	letoun
dopadl	dopadnout	k5eAaPmAgInS	dopadnout
10	[number]	k4	10
km	km	kA	km
od	od	k7c2	od
vesnice	vesnice	k1gFnSc2	vesnice
Om	Om	k1gMnSc2	Om
al-Tujour	al-Tujoura	k1gFnPc2	al-Tujoura
provincie	provincie	k1gFnSc2	provincie
Latakíja	Latakíjum	k1gNnSc2	Latakíjum
<g/>
,	,	kIx,	,
hluboko	hluboko	k6eAd1	hluboko
v	v	k7c6	v
syrských	syrský	k2eAgFnPc6d1	Syrská
teritoriálních	teritoriální	k2eAgFnPc6d1	teritoriální
vodách	voda	k1gFnPc6	voda
středozemního	středozemní	k2eAgNnSc2d1	středozemní
moře	moře	k1gNnSc2	moře
<g/>
.	.	kIx.	.
<g/>
Čin	čin	k1gInSc1	čin
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
širokou	široký	k2eAgFnSc4d1	široká
odezvu	odezva	k1gFnSc4	odezva
na	na	k7c6	na
mezinárodním	mezinárodní	k2eAgNnSc6d1	mezinárodní
poli	pole	k1gNnSc6	pole
<g/>
.	.	kIx.	.
</s>
<s>
NATO	NATO	kA	NATO
(	(	kIx(	(
<g/>
Turecko	Turecko	k1gNnSc1	Turecko
je	být	k5eAaImIp3nS	být
členem	člen	k1gInSc7	člen
<g/>
)	)	kIx)	)
na	na	k7c6	na
mimořádné	mimořádný	k2eAgFnSc6d1	mimořádná
schůzce	schůzka	k1gFnSc6	schůzka
incident	incident	k1gInSc4	incident
odsoudilo	odsoudit	k5eAaPmAgNnS	odsoudit
a	a	k8xC	a
bylo	být	k5eAaImAgNnS	být
odhodláno	odhodlán	k2eAgNnSc1d1	odhodláno
svého	svůj	k3xOyFgMnSc4	svůj
spojence	spojenec	k1gMnSc4	spojenec
bránit	bránit	k5eAaImF	bránit
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
nepovažovalo	považovat	k5eNaImAgNnS	považovat
čin	čin	k1gInSc4	čin
Damašku	Damašek	k1gInSc2	Damašek
za	za	k7c4	za
záměrný	záměrný	k2eAgInSc4d1	záměrný
a	a	k8xC	a
vyzvalo	vyzvat	k5eAaPmAgNnS	vyzvat
alianci	aliance	k1gFnSc4	aliance
a	a	k8xC	a
Turecko	Turecko	k1gNnSc4	Turecko
ke	k	k7c3	k
zdrženlivosti	zdrženlivost	k1gFnSc3	zdrženlivost
<g/>
.	.	kIx.	.
</s>
<s>
Assad	Assad	k6eAd1	Assad
se	se	k3xPyFc4	se
za	za	k7c4	za
incident	incident	k1gInSc4	incident
omluvil	omluvit	k5eAaPmAgMnS	omluvit
<g/>
.	.	kIx.	.
</s>
<s>
Ankara	Ankara	k1gFnSc1	Ankara
však	však	k9	však
pohrozila	pohrozit	k5eAaPmAgFnS	pohrozit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c4	na
případné	případný	k2eAgNnSc4d1	případné
narušení	narušení	k1gNnSc4	narušení
svých	svůj	k3xOyFgFnPc2	svůj
hranic	hranice	k1gFnPc2	hranice
Sýrií	Sýrie	k1gFnPc2	Sýrie
odpoví	odpovědět	k5eAaPmIp3nS	odpovědět
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Turecké	turecký	k2eAgFnPc1d1	turecká
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
také	také	k9	také
přesunuly	přesunout	k5eAaPmAgFnP	přesunout
některé	některý	k3yIgFnPc1	některý
své	svůj	k3xOyFgFnPc1	svůj
jednotky	jednotka	k1gFnPc1	jednotka
k	k	k7c3	k
syrským	syrský	k2eAgFnPc3d1	Syrská
hranicím	hranice	k1gFnPc3	hranice
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Okolnosti	okolnost	k1gFnSc3	okolnost
incidentu	incident	k1gInSc2	incident
však	však	k9	však
nejsou	být	k5eNaImIp3nP	být
stále	stále	k6eAd1	stále
jasné	jasný	k2eAgInPc1d1	jasný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
se	se	k3xPyFc4	se
mezitím	mezitím	k6eAd1	mezitím
rozhořely	rozhořet	k5eAaPmAgInP	rozhořet
dosud	dosud	k6eAd1	dosud
největší	veliký	k2eAgInPc1d3	veliký
boje	boj	k1gInPc1	boj
mezi	mezi	k7c7	mezi
armádou	armáda	k1gFnSc7	armáda
a	a	k8xC	a
ozbrojenou	ozbrojený	k2eAgFnSc7d1	ozbrojená
opozicí	opozice	k1gFnSc7	opozice
v	v	k7c6	v
metropoli	metropol	k1gFnSc6	metropol
Damašku	Damašek	k1gInSc2	Damašek
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc2	jeho
bezprostřední	bezprostřední	k2eAgFnSc2d1	bezprostřední
blízkosti	blízkost	k1gFnSc2	blízkost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rezoluce	rezoluce	k1gFnSc2	rezoluce
RB	RB	kA	RB
OSN	OSN	kA	OSN
===	===	k?	===
</s>
</p>
<p>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
a	a	k8xC	a
Čína	Čína	k1gFnSc1	Čína
ve	v	k7c4	v
čtvrtek	čtvrtek	k1gInSc4	čtvrtek
19	[number]	k4	19
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2012	[number]	k4	2012
v	v	k7c6	v
Radě	rada	k1gFnSc6	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
vetovaly	vetovat	k5eAaBmAgFnP	vetovat
rezoluci	rezoluce	k1gFnSc4	rezoluce
k	k	k7c3	k
Sýrii	Sýrie	k1gFnSc3	Sýrie
hrozící	hrozící	k2eAgFnSc2d1	hrozící
sankcemi	sankce	k1gFnPc7	sankce
za	za	k7c4	za
neplnění	neplnění	k1gNnSc4	neplnění
dokumentu	dokument	k1gInSc2	dokument
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
začátku	začátek	k1gInSc2	začátek
syrské	syrský	k2eAgFnSc2d1	Syrská
krize	krize	k1gFnSc2	krize
Moskva	Moskva	k1gFnSc1	Moskva
a	a	k8xC	a
Peking	Peking	k1gInSc1	Peking
použily	použít	k5eAaPmAgInP	použít
v	v	k7c6	v
Radě	rada	k1gFnSc6	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
práva	právo	k1gNnSc2	právo
veto	veto	k1gNnSc4	veto
už	už	k6eAd1	už
potřetí	potřetí	k4xO	potřetí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
USA	USA	kA	USA
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
další	další	k2eAgInSc4d1	další
neúspěch	neúspěch	k1gInSc4	neúspěch
prohlásily	prohlásit	k5eAaPmAgFnP	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Rada	rada	k1gFnSc1	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
v	v	k7c6	v
řešení	řešení	k1gNnSc6	řešení
syrské	syrský	k2eAgFnSc2d1	Syrská
krize	krize	k1gFnSc2	krize
naprosto	naprosto	k6eAd1	naprosto
selhala	selhat	k5eAaPmAgFnS	selhat
<g/>
.	.	kIx.	.
</s>
<s>
Británie	Británie	k1gFnSc1	Británie
ihned	ihned	k6eAd1	ihned
navrhla	navrhnout	k5eAaPmAgFnS	navrhnout
novou	nový	k2eAgFnSc4d1	nová
rezoluci	rezoluce	k1gFnSc4	rezoluce
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
počítá	počítat	k5eAaImIp3nS	počítat
alespoň	alespoň	k9	alespoň
s	s	k7c7	s
30	[number]	k4	30
<g/>
denním	denní	k2eAgNnSc7d1	denní
prodloužením	prodloužení	k1gNnSc7	prodloužení
pozorovatelské	pozorovatelský	k2eAgFnSc2d1	pozorovatelská
mise	mise	k1gFnSc2	mise
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
rezoluce	rezoluce	k1gFnSc1	rezoluce
byla	být	k5eAaImAgFnS	být
nakonec	nakonec	k6eAd1	nakonec
schválena	schválit	k5eAaPmNgFnS	schválit
<g/>
,	,	kIx,	,
i	i	k8xC	i
když	když	k8xS	když
další	další	k2eAgNnSc4d1	další
prodloužení	prodloužení	k1gNnSc4	prodloužení
pozorovatelské	pozorovatelský	k2eAgFnSc2d1	pozorovatelská
mise	mise	k1gFnSc2	mise
je	být	k5eAaImIp3nS	být
vázáno	vázat	k5eAaImNgNnS	vázat
na	na	k7c4	na
sporný	sporný	k2eAgInSc4d1	sporný
fakt	fakt	k1gInSc4	fakt
<g/>
,	,	kIx,	,
že	že	k8xS	že
vyžaduje	vyžadovat	k5eAaImIp3nS	vyžadovat
stažení	stažení	k1gNnSc4	stažení
jen	jen	k9	jen
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
bojujících	bojující	k2eAgFnPc2d1	bojující
stran	strana	k1gFnPc2	strana
z	z	k7c2	z
pole	pole	k1gNnSc2	pole
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
textu	text	k1gInSc6	text
je	být	k5eAaImIp3nS	být
totiž	totiž	k9	totiž
ustanovení	ustanovení	k1gNnSc1	ustanovení
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
<g/>
,	,	kIx,	,
že	že	k8xS	že
ze	z	k7c2	z
svých	svůj	k3xOyFgFnPc2	svůj
pozic	pozice	k1gFnPc2	pozice
ustoupí	ustoupit	k5eAaPmIp3nS	ustoupit
vládní	vládní	k2eAgNnPc4d1	vládní
vojska	vojsko	k1gNnPc4	vojsko
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
disponují	disponovat	k5eAaBmIp3nP	disponovat
těžkými	těžký	k2eAgFnPc7d1	těžká
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Rada	rada	k1gFnSc1	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
na	na	k7c4	na
sobotu	sobota	k1gFnSc4	sobota
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	zářit	k5eAaImIp3nS	zářit
2013	[number]	k4	2013
jednomyslně	jednomyslně	k6eAd1	jednomyslně
schválila	schválit	k5eAaPmAgFnS	schválit
rezoluci	rezoluce	k1gFnSc4	rezoluce
požadující	požadující	k2eAgFnSc4d1	požadující
likvidaci	likvidace	k1gFnSc4	likvidace
syrského	syrský	k2eAgInSc2d1	syrský
arzenálu	arzenál	k1gInSc2	arzenál
chemických	chemický	k2eAgFnPc2d1	chemická
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Rezoluce	rezoluce	k1gFnSc1	rezoluce
neobsahuje	obsahovat	k5eNaImIp3nS	obsahovat
bezprostřední	bezprostřední	k2eAgFnSc4d1	bezprostřední
hrozbu	hrozba	k1gFnSc4	hrozba
vojenského	vojenský	k2eAgNnSc2d1	vojenské
potrestání	potrestání	k1gNnSc2	potrestání
režimu	režim	k1gInSc2	režim
Bašára	Bašár	k1gMnSc4	Bašár
Asada	Asad	k1gMnSc4	Asad
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
se	se	k3xPyFc4	se
požadavku	požadavek	k1gInSc2	požadavek
nepodřídí	podřídit	k5eNaPmIp3nS	podřídit
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
by	by	kYmCp3nS	by
musela	muset	k5eAaImAgFnS	muset
být	být	k5eAaImF	být
vydána	vydán	k2eAgFnSc1d1	vydána
nová	nový	k2eAgFnSc1d1	nová
rezoluce	rezoluce	k1gFnSc1	rezoluce
<g/>
.	.	kIx.	.
</s>
<s>
Rezoluce	rezoluce	k1gFnSc1	rezoluce
požaduje	požadovat	k5eAaImIp3nS	požadovat
potrestání	potrestání	k1gNnSc1	potrestání
viníků	viník	k1gMnPc2	viník
za	za	k7c4	za
použití	použití	k1gNnSc4	použití
chemických	chemický	k2eAgFnPc2d1	chemická
zbraní	zbraň	k1gFnPc2	zbraň
při	při	k7c6	při
srpnovém	srpnový	k2eAgInSc6d1	srpnový
útoku	útok	k1gInSc6	útok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
neurčuje	určovat	k5eNaImIp3nS	určovat
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
incident	incident	k1gInSc4	incident
zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
<g/>
.	.	kIx.	.
</s>
<s>
Tři	tři	k4xCgInPc4	tři
předchozí	předchozí	k2eAgInPc4d1	předchozí
návrhy	návrh	k1gInPc4	návrh
rezolucí	rezoluce	k1gFnPc2	rezoluce
proti	proti	k7c3	proti
syrskému	syrský	k2eAgInSc3d1	syrský
vládnoucímu	vládnoucí	k2eAgInSc3d1	vládnoucí
režimu	režim	k1gInSc3	režim
narážely	narážet	k5eAaImAgInP	narážet
na	na	k7c4	na
vytrvalá	vytrvalý	k2eAgNnPc4d1	vytrvalé
veta	veto	k1gNnPc4	veto
Ruska	Rusko	k1gNnSc2	Rusko
a	a	k8xC	a
Číny	Čína	k1gFnSc2	Čína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Letecký	letecký	k2eAgInSc1d1	letecký
útok	útok	k1gInSc1	útok
na	na	k7c4	na
Azáz	Azáz	k1gInSc4	Azáz
===	===	k?	===
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
15	[number]	k4	15
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2012	[number]	k4	2012
zaútočily	zaútočit	k5eAaPmAgFnP	zaútočit
letecké	letecký	k2eAgFnPc1d1	letecká
síly	síla	k1gFnPc1	síla
vládních	vládní	k2eAgFnPc2d1	vládní
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
složek	složka	k1gFnPc2	složka
Sýrie	Sýrie	k1gFnSc1	Sýrie
na	na	k7c4	na
město	město	k1gNnSc4	město
Azaz	Azaza	k1gFnPc2	Azaza
(	(	kIx(	(
<g/>
Azáz	Azáz	k1gInSc1	Azáz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nacházející	nacházející	k2eAgFnSc2d1	nacházející
se	se	k3xPyFc4	se
nedaleko	nedaleko	k7c2	nedaleko
syrsko-turecké	syrskourecký	k2eAgFnSc2d1	syrsko-turecký
hranice	hranice	k1gFnSc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Místní	místní	k2eAgMnPc1d1	místní
obyvatelé	obyvatel	k1gMnPc1	obyvatel
tvrdí	tvrdit	k5eAaImIp3nP	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zde	zde	k6eAd1	zde
nebyly	být	k5eNaImAgFnP	být
přítomné	přítomný	k2eAgFnPc1d1	přítomná
žádné	žádný	k3yNgFnPc1	žádný
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
síly	síla	k1gFnPc1	síla
<g/>
,	,	kIx,	,
a	a	k8xC	a
tudíž	tudíž	k8xC	tudíž
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
útok	útok	k1gInSc4	útok
mířený	mířený	k2eAgInSc4d1	mířený
jednoznačně	jednoznačně	k6eAd1	jednoznačně
proti	proti	k7c3	proti
civilnímu	civilní	k2eAgNnSc3d1	civilní
obyvatelstvu	obyvatelstvo	k1gNnSc3	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
(	(	kIx(	(
<g/>
ne	ne	k9	ne
<g/>
)	)	kIx)	)
<g/>
přítomnosti	přítomnost	k1gFnSc2	přítomnost
ozbrojených	ozbrojený	k2eAgFnPc2d1	ozbrojená
sil	síla	k1gFnPc2	síla
se	se	k3xPyFc4	se
však	však	k9	však
nepodařilo	podařit	k5eNaPmAgNnS	podařit
ověřit	ověřit	k5eAaPmF	ověřit
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
bylo	být	k5eAaImAgNnS	být
chloubou	chlouba	k1gFnSc7	chlouba
části	část	k1gFnSc2	část
Sýrie	Sýrie	k1gFnSc1	Sýrie
ovládané	ovládaný	k2eAgMnPc4d1	ovládaný
rebely	rebel	k1gMnPc4	rebel
<g/>
,	,	kIx,	,
zůstalo	zůstat	k5eAaPmAgNnS	zůstat
po	po	k7c6	po
náletu	nálet	k1gInSc6	nálet
v	v	k7c6	v
troskách	troska	k1gFnPc6	troska
<g/>
.	.	kIx.	.
</s>
<s>
Zemřela	zemřít	k5eAaPmAgFnS	zemřít
zde	zde	k6eAd1	zde
nebo	nebo	k8xC	nebo
byla	být	k5eAaImAgFnS	být
zraněna	zranit	k5eAaPmNgFnS	zranit
asi	asi	k9	asi
stovka	stovka	k1gFnSc1	stovka
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Použití	použití	k1gNnSc1	použití
chemických	chemický	k2eAgFnPc2d1	chemická
zbraní	zbraň	k1gFnPc2	zbraň
a	a	k8xC	a
hrozba	hrozba	k1gFnSc1	hrozba
útoku	útok	k1gInSc2	útok
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
USA	USA	kA	USA
a	a	k8xC	a
Francie	Francie	k1gFnSc2	Francie
==	==	k?	==
</s>
</p>
<p>
<s>
Podle	podle	k7c2	podle
vyšetřování	vyšetřování	k1gNnSc2	vyšetřování
OSN	OSN	kA	OSN
Asad	Asad	k1gMnSc1	Asad
použil	použít	k5eAaPmAgMnS	použít
chemické	chemický	k2eAgFnPc4d1	chemická
zbraně	zbraň	k1gFnPc4	zbraň
proti	proti	k7c3	proti
vlastnímu	vlastní	k2eAgInSc3d1	vlastní
lidu	lid	k1gInSc3	lid
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
2015	[number]	k4	2015
a	a	k8xC	a
2017	[number]	k4	2017
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2019	[number]	k4	2019
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
ze	z	k7c2	z
zprávy	zpráva	k1gFnSc2	zpráva
Organizace	organizace	k1gFnSc2	organizace
pro	pro	k7c4	pro
zákaz	zákaz	k1gInSc4	zákaz
chemických	chemický	k2eAgFnPc2d1	chemická
zbraní	zbraň	k1gFnPc2	zbraň
(	(	kIx(	(
<g/>
OPCW	OPCW	kA	OPCW
<g/>
)	)	kIx)	)
byl	být	k5eAaImAgInS	být
vynechán	vynechán	k2eAgInSc1d1	vynechán
nesouhlasný	souhlasný	k2eNgInSc1d1	nesouhlasný
posudek	posudek	k1gInSc1	posudek
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
rozporoval	rozporovat	k5eAaBmAgInS	rozporovat
tvrzení	tvrzení	k1gNnSc4	tvrzení
OPCW	OPCW	kA	OPCW
<g/>
,	,	kIx,	,
že	že	k8xS	že
režimní	režimní	k2eAgInPc1d1	režimní
vrtulníky	vrtulník	k1gInPc1	vrtulník
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
shodily	shodit	k5eAaPmAgInP	shodit
chemické	chemický	k2eAgInPc1d1	chemický
kanystry	kanystr	k1gInPc1	kanystr
na	na	k7c6	na
Dúmu	Dúmus	k1gInSc6	Dúmus
<g/>
.	.	kIx.	.
<g/>
Asadova	Asadův	k2eAgFnSc1d1	Asadova
vláda	vláda	k1gFnSc1	vláda
a	a	k8xC	a
povstalci	povstalec	k1gMnPc1	povstalec
se	se	k3xPyFc4	se
navzájem	navzájem	k6eAd1	navzájem
viní	vinit	k5eAaImIp3nS	vinit
z	z	k7c2	z
provedení	provedení	k1gNnSc2	provedení
tří	tři	k4xCgInPc2	tři
útoků	útok	k1gInPc2	útok
chemickými	chemický	k2eAgFnPc7d1	chemická
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgMnPc1	dva
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
se	se	k3xPyFc4	se
údajně	údajně	k6eAd1	údajně
odehrály	odehrát	k5eAaPmAgInP	odehrát
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
a	a	k8xC	a
třetí	třetí	k4xOgInSc4	třetí
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
<g/>
.	.	kIx.	.
</s>
<s>
Syrská	syrský	k2eAgFnSc1d1	Syrská
vláda	vláda	k1gFnSc1	vláda
oficiálně	oficiálně	k6eAd1	oficiálně
vyzvala	vyzvat	k5eAaPmAgFnS	vyzvat
generálního	generální	k2eAgMnSc4d1	generální
tajemníka	tajemník	k1gMnSc4	tajemník
OSN	OSN	kA	OSN
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
byla	být	k5eAaImAgFnS	být
vyslána	vyslat	k5eAaPmNgFnS	vyslat
mezinárodní	mezinárodní	k2eAgFnSc1d1	mezinárodní
neutrální	neutrální	k2eAgFnSc1d1	neutrální
mise	mise	k1gFnSc1	mise
expertů	expert	k1gMnPc2	expert
pro	pro	k7c4	pro
vyšetření	vyšetření	k1gNnSc4	vyšetření
těchto	tento	k3xDgFnPc2	tento
událostí	událost	k1gFnPc2	událost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
na	na	k7c6	na
konci	konec	k1gInSc6	konec
dubna	duben	k1gInSc2	duben
2013	[number]	k4	2013
na	na	k7c6	na
základě	základ	k1gInSc6	základ
nepotvrzených	potvrzený	k2eNgFnPc2d1	nepotvrzená
zpráv	zpráva	k1gFnPc2	zpráva
zpravodajců	zpravodajce	k1gMnPc2	zpravodajce
uvedly	uvést	k5eAaPmAgInP	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
bylo	být	k5eAaImAgNnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
použito	použit	k2eAgNnSc1d1	použito
menší	malý	k2eAgNnSc1d2	menší
množství	množství	k1gNnSc1	množství
chemických	chemický	k2eAgFnPc2d1	chemická
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
sarinu	sarin	k1gInSc2	sarin
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
Barack	Barack	k1gMnSc1	Barack
Obama	Obama	k?	Obama
tehdy	tehdy	k6eAd1	tehdy
připustil	připustit	k5eAaPmAgMnS	připustit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zatím	zatím	k6eAd1	zatím
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
tak	tak	k6eAd1	tak
učinil	učinit	k5eAaPmAgMnS	učinit
<g/>
.	.	kIx.	.
</s>
<s>
Nasazení	nasazení	k1gNnSc1	nasazení
chemických	chemický	k2eAgFnPc2d1	chemická
zbraní	zbraň	k1gFnPc2	zbraň
ale	ale	k8xC	ale
podle	podle	k7c2	podle
něj	on	k3xPp3gNnSc2	on
znamenalo	znamenat	k5eAaImAgNnS	znamenat
"	"	kIx"	"
<g/>
obrat	obrat	k1gInSc1	obrat
<g/>
"	"	kIx"	"
v	v	k7c6	v
přístupu	přístup	k1gInSc6	přístup
USA	USA	kA	USA
k	k	k7c3	k
řešení	řešení	k1gNnSc3	řešení
syrské	syrský	k2eAgFnSc2d1	Syrská
krize	krize	k1gFnSc2	krize
<g/>
.	.	kIx.	.
<g/>
Členka	členka	k1gFnSc1	členka
nezávislé	závislý	k2eNgFnSc2d1	nezávislá
vyšetřovací	vyšetřovací	k2eAgFnSc2d1	vyšetřovací
komise	komise	k1gFnSc2	komise
OSN	OSN	kA	OSN
Carla	Carl	k1gMnSc2	Carl
del	del	k?	del
Ponte	Pont	k1gInSc5	Pont
se	se	k3xPyFc4	se
k	k	k7c3	k
útoku	útok	k1gInSc3	útok
vyjádřila	vyjádřit	k5eAaPmAgFnS	vyjádřit
takto	takto	k6eAd1	takto
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Naši	náš	k3xOp1gMnPc1	náš
vyšetřovatelé	vyšetřovatel	k1gMnPc1	vyšetřovatel
v	v	k7c6	v
sousedních	sousední	k2eAgFnPc6d1	sousední
zemích	zem	k1gFnPc6	zem
vyslýchali	vyslýchat	k5eAaImAgMnP	vyslýchat
oběti	oběť	k1gFnPc4	oběť
<g/>
,	,	kIx,	,
lékaře	lékař	k1gMnPc4	lékař
<g/>
...	...	k?	...
a	a	k8xC	a
podle	podle	k7c2	podle
zprávy	zpráva	k1gFnSc2	zpráva
z	z	k7c2	z
minulého	minulý	k2eAgInSc2d1	minulý
týdne	týden	k1gInSc2	týden
<g/>
,	,	kIx,	,
kterou	který	k3yRgFnSc4	který
jsem	být	k5eAaImIp1nS	být
viděla	vidět	k5eAaImAgFnS	vidět
<g/>
,	,	kIx,	,
tu	tu	k6eAd1	tu
na	na	k7c6	na
základě	základ	k1gInSc6	základ
způsobu	způsob	k1gInSc2	způsob
léčby	léčba	k1gFnSc2	léčba
obětí	oběť	k1gFnPc2	oběť
jsou	být	k5eAaImIp3nP	být
silná	silný	k2eAgNnPc1d1	silné
<g/>
,	,	kIx,	,
konkrétní	konkrétní	k2eAgNnPc1d1	konkrétní
podezření	podezření	k1gNnPc1	podezření
<g/>
,	,	kIx,	,
zatím	zatím	k6eAd1	zatím
ale	ale	k9	ale
nikoli	nikoli	k9	nikoli
nevyvratitelný	vyvratitelný	k2eNgInSc1d1	nevyvratitelný
důkaz	důkaz	k1gInSc1	důkaz
použití	použití	k1gNnSc2	použití
sarinu	sarin	k1gInSc2	sarin
rebely	rebel	k1gMnPc4	rebel
<g/>
,	,	kIx,	,
žádné	žádný	k3yNgInPc1	žádný
důkazy	důkaz	k1gInPc1	důkaz
nenaznačují	naznačovat	k5eNaImIp3nP	naznačovat
použití	použití	k1gNnSc4	použití
syrskou	syrský	k2eAgFnSc7d1	Syrská
vládou	vláda	k1gFnSc7	vláda
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
</s>
</p>
<p>
<s>
Oficiální	oficiální	k2eAgFnSc1d1	oficiální
mise	mise	k1gFnSc1	mise
započala	započnout	k5eAaPmAgFnS	započnout
19	[number]	k4	19
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2013	[number]	k4	2013
po	po	k7c6	po
čtyřměsíčním	čtyřměsíční	k2eAgNnSc6d1	čtyřměsíční
vyjednávání	vyjednávání	k1gNnSc6	vyjednávání
<g/>
.	.	kIx.	.
</s>
<s>
Dva	dva	k4xCgInPc4	dva
dny	den	k1gInPc4	den
po	po	k7c6	po
zahájení	zahájení	k1gNnSc6	zahájení
mise	mise	k1gFnSc2	mise
se	se	k3xPyFc4	se
odehrál	odehrát	k5eAaPmAgInS	odehrát
chemický	chemický	k2eAgInSc1d1	chemický
útok	útok	k1gInSc1	útok
v	v	k7c6	v
Ghútě	Ghúta	k1gFnSc6	Ghúta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Chemické	chemický	k2eAgInPc4d1	chemický
útoky	útok	k1gInPc4	útok
v	v	k7c6	v
Ghútě	Ghúta	k1gFnSc6	Ghúta
se	se	k3xPyFc4	se
odehrály	odehrát	k5eAaPmAgFnP	odehrát
21	[number]	k4	21
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2013	[number]	k4	2013
v	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
Došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
nim	on	k3xPp3gFnPc3	on
na	na	k7c6	na
východních	východní	k2eAgNnPc6d1	východní
předměstích	předměstí	k1gNnPc6	předměstí
Damašku	Damašek	k1gInSc2	Damašek
<g/>
,	,	kIx,	,
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Sýrie	Sýrie	k1gFnSc2	Sýrie
<g/>
,	,	kIx,	,
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
Ghúta	Ghút	k1gInSc2	Ghút
v	v	k7c6	v
muháfaze	muháfaz	k1gInSc6	muháfaz
Ríf	Ríf	k1gFnSc2	Ríf
Dimašq	Dimašq	k1gFnSc2	Dimašq
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zjištění	zjištění	k1gNnSc2	zjištění
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
bylo	být	k5eAaImAgNnS	být
použito	použít	k5eAaPmNgNnS	použít
350	[number]	k4	350
litrů	litr	k1gInPc2	litr
sarinu	sarin	k1gInSc2	sarin
<g/>
.	.	kIx.	.
</s>
<s>
Počet	počet	k1gInSc1	počet
obětí	oběť	k1gFnPc2	oběť
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
stovek	stovka	k1gFnPc2	stovka
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
povstalecká	povstalecký	k2eAgFnSc1d1	povstalecká
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
syrská	syrský	k2eAgFnSc1d1	Syrská
armáda	armáda	k1gFnSc1	armáda
oznámila	oznámit	k5eAaPmAgFnS	oznámit
1729	[number]	k4	1729
mrtvých	mrtvý	k1gMnPc2	mrtvý
a	a	k8xC	a
až	až	k9	až
6000	[number]	k4	6000
zasažených	zasažený	k2eAgInPc2d1	zasažený
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
nezávislá	závislý	k2eNgFnSc1d1	nezávislá
organizace	organizace	k1gFnSc1	organizace
Lékaři	lékař	k1gMnPc1	lékař
bez	bez	k7c2	bez
hranic	hranice	k1gFnPc2	hranice
potvrdila	potvrdit	k5eAaPmAgFnS	potvrdit
k	k	k7c3	k
24	[number]	k4	24
<g/>
.	.	kIx.	.
srpnu	srpen	k1gInSc6	srpen
zhruba	zhruba	k6eAd1	zhruba
3600	[number]	k4	3600
ošetřených	ošetřený	k2eAgMnPc2d1	ošetřený
a	a	k8xC	a
355	[number]	k4	355
mrtvých	mrtvý	k1gMnPc2	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
největší	veliký	k2eAgNnSc4d3	veliký
užití	užití	k1gNnSc4	užití
chemických	chemický	k2eAgFnPc2d1	chemická
zbraní	zbraň	k1gFnPc2	zbraň
od	od	k7c2	od
iráckého	irácký	k2eAgInSc2d1	irácký
útoku	útok	k1gInSc2	útok
v	v	k7c6	v
Haladžbě	Haladžba	k1gFnSc6	Haladžba
proti	proti	k7c3	proti
Kurdům	Kurd	k1gMnPc3	Kurd
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1988	[number]	k4	1988
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
povstalců	povstalec	k1gMnPc2	povstalec
chemické	chemický	k2eAgFnSc2d1	chemická
zbraně	zbraň	k1gFnSc2	zbraň
použila	použít	k5eAaPmAgFnS	použít
syrská	syrský	k2eAgFnSc1d1	Syrská
armáda	armáda	k1gFnSc1	armáda
podporující	podporující	k2eAgInSc1d1	podporující
režim	režim	k1gInSc1	režim
Bašára	Bašár	k1gMnSc2	Bašár
al-Asada	al-Asada	k1gFnSc1	al-Asada
<g/>
,	,	kIx,	,
režim	režim	k1gInSc1	režim
naopak	naopak	k6eAd1	naopak
viní	vinit	k5eAaImIp3nS	vinit
z	z	k7c2	z
použití	použití	k1gNnSc2	použití
zbraní	zbraň	k1gFnPc2	zbraň
povstalce	povstalec	k1gMnPc4	povstalec
<g/>
.	.	kIx.	.
</s>
<s>
Sýrie	Sýrie	k1gFnSc1	Sýrie
byla	být	k5eAaImAgFnS	být
v	v	k7c6	v
době	doba	k1gFnSc6	doba
útoku	útok	k1gInSc2	útok
jedním	jeden	k4xCgMnSc7	jeden
z	z	k7c2	z
pěti	pět	k4xCc2	pět
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
nepodepsaly	podepsat	k5eNaPmAgFnP	podepsat
Úmluvu	úmluva	k1gFnSc4	úmluva
o	o	k7c6	o
chemických	chemický	k2eAgFnPc6d1	chemická
zbraních	zbraň	k1gFnPc6	zbraň
<g/>
,	,	kIx,	,
respektive	respektive	k9	respektive
jedním	jeden	k4xCgNnSc7	jeden
ze	z	k7c2	z
sedmi	sedm	k4xCc2	sedm
států	stát	k1gInPc2	stát
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
ji	on	k3xPp3gFnSc4	on
neratifikovaly	ratifikovat	k5eNaBmAgInP	ratifikovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
syrská	syrský	k2eAgFnSc1d1	Syrská
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
přiznala	přiznat	k5eAaPmAgFnS	přiznat
<g/>
,	,	kIx,	,
že	že	k8xS	že
chemické	chemický	k2eAgFnPc4d1	chemická
zbraně	zbraň	k1gFnPc4	zbraň
má	mít	k5eAaImIp3nS	mít
<g/>
.	.	kIx.	.
</s>
<s>
Přesvědčení	přesvědčení	k1gNnSc1	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
chemické	chemický	k2eAgInPc1d1	chemický
útoky	útok	k1gInPc1	útok
má	mít	k5eAaImIp3nS	mít
na	na	k7c6	na
svědomí	svědomí	k1gNnSc6	svědomí
vládnoucí	vládnoucí	k2eAgInSc1d1	vládnoucí
režim	režim	k1gInSc1	režim
<g/>
,	,	kIx,	,
vyjádřily	vyjádřit	k5eAaPmAgFnP	vyjádřit
ústy	ústa	k1gNnPc7	ústa
svých	svůj	k3xOyFgMnPc2	svůj
představitelů	představitel	k1gMnPc2	představitel
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgInPc4d1	jiný
Spojené	spojený	k2eAgInPc4d1	spojený
státy	stát	k1gInPc4	stát
americké	americký	k2eAgInPc4d1	americký
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
a	a	k8xC	a
zároveň	zároveň	k6eAd1	zároveň
začaly	začít	k5eAaPmAgFnP	začít
plánovat	plánovat	k5eAaImF	plánovat
nějakou	nějaký	k3yIgFnSc4	nějaký
formu	forma	k1gFnSc4	forma
vojenského	vojenský	k2eAgInSc2d1	vojenský
zásahu	zásah	k1gInSc2	zásah
<g/>
.	.	kIx.	.
<g/>
Naopak	naopak	k6eAd1	naopak
Rusko	Rusko	k1gNnSc1	Rusko
a	a	k8xC	a
Írán	Írán	k1gInSc1	Írán
<g/>
,	,	kIx,	,
které	který	k3yRgInPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
konfliktu	konflikt	k1gInSc6	konflikt
staví	stavit	k5eAaBmIp3nS	stavit
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
na	na	k7c4	na
stranu	strana	k1gFnSc4	strana
Asadova	Asadův	k2eAgInSc2d1	Asadův
režimu	režim	k1gInSc2	režim
<g/>
,	,	kIx,	,
vyjádřily	vyjádřit	k5eAaPmAgInP	vyjádřit
přesvědčení	přesvědčení	k1gNnSc4	přesvědčení
<g/>
,	,	kIx,	,
že	že	k8xS	že
za	za	k7c4	za
útoky	útok	k1gInPc4	útok
stojí	stát	k5eAaImIp3nP	stát
povstalci	povstalec	k1gMnPc1	povstalec
<g/>
,	,	kIx,	,
a	a	k8xC	a
Čínská	čínský	k2eAgFnSc1d1	čínská
lidová	lidový	k2eAgFnSc1d1	lidová
republika	republika	k1gFnSc1	republika
zaujala	zaujmout	k5eAaPmAgFnS	zaujmout
zdrženlivý	zdrženlivý	k2eAgInSc4d1	zdrženlivý
postoj	postoj	k1gInSc4	postoj
<g/>
.	.	kIx.	.
</s>
<s>
Vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
stálí	stálý	k2eAgMnPc1d1	stálý
členové	člen	k1gMnPc1	člen
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
Čína	Čína	k1gFnSc1	Čína
a	a	k8xC	a
Rusko	Rusko	k1gNnSc1	Rusko
se	se	k3xPyFc4	se
nechystali	chystat	k5eNaImAgMnP	chystat
podpořit	podpořit	k5eAaPmF	podpořit
odvetný	odvetný	k2eAgInSc4d1	odvetný
vojenský	vojenský	k2eAgInSc4d1	vojenský
zásah	zásah	k1gInSc4	zásah
proti	proti	k7c3	proti
Asadovu	Asadův	k2eAgInSc3d1	Asadův
režimu	režim	k1gInSc3	režim
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
západními	západní	k2eAgMnPc7d1	západní
spojenci	spojenec	k1gMnPc7	spojenec
v	v	k7c6	v
reakci	reakce	k1gFnSc6	reakce
na	na	k7c4	na
útoky	útok	k1gInPc4	útok
zvažována	zvažován	k2eAgFnSc1d1	zvažována
možnost	možnost	k1gFnSc1	možnost
zasáhnout	zasáhnout	k5eAaPmF	zasáhnout
bez	bez	k7c2	bez
mandátu	mandát	k1gInSc2	mandát
Organizace	organizace	k1gFnSc2	organizace
spojených	spojený	k2eAgInPc2d1	spojený
národů	národ	k1gInPc2	národ
podobně	podobně	k6eAd1	podobně
jako	jako	k8xC	jako
Severoatlantická	severoatlantický	k2eAgFnSc1d1	Severoatlantická
aliance	aliance	k1gFnSc1	aliance
zasáhla	zasáhnout	k5eAaPmAgFnS	zasáhnout
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
operace	operace	k1gFnSc2	operace
Spojenecká	spojenecký	k2eAgFnSc1d1	spojenecká
síla	síla	k1gFnSc1	síla
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1999	[number]	k4	1999
v	v	k7c4	v
Kosovu	Kosův	k2eAgFnSc4d1	Kosova
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
však	však	k9	však
v	v	k7c6	v
případě	případ	k1gInSc6	případ
Sýrie	Sýrie	k1gFnSc2	Sýrie
tehdy	tehdy	k6eAd1	tehdy
nestalo	stát	k5eNaPmAgNnS	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
29	[number]	k4	29
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2013	[number]	k4	2013
Dolní	dolní	k2eAgFnSc1d1	dolní
sněmovna	sněmovna	k1gFnSc1	sněmovna
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
zamítla	zamítnout	k5eAaPmAgFnS	zamítnout
návrh	návrh	k1gInSc4	návrh
vlády	vláda	k1gFnSc2	vláda
Davida	David	k1gMnSc2	David
Camerona	Cameron	k1gMnSc2	Cameron
na	na	k7c4	na
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
intervenci	intervence	k1gFnSc4	intervence
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
v	v	k7c6	v
poměru	poměr	k1gInSc6	poměr
272	[number]	k4	272
ku	k	k7c3	k
285	[number]	k4	285
hlasům	hlas	k1gInPc3	hlas
<g/>
.6	.6	k4	.6
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2013	[number]	k4	2013
se	se	k3xPyFc4	se
během	během	k7c2	během
summitu	summit	k1gInSc2	summit
zemí	zem	k1gFnPc2	zem
skupiny	skupina	k1gFnSc2	skupina
G20	G20	k1gFnSc2	G20
v	v	k7c6	v
Petrohradu	Petrohrad	k1gInSc3	Petrohrad
na	na	k7c6	na
schůzce	schůzka	k1gFnSc6	schůzka
mezi	mezi	k7c7	mezi
čtyřma	čtyři	k4xCgNnPc7	čtyři
očima	oko	k1gNnPc7	oko
setkali	setkat	k5eAaPmAgMnP	setkat
Barack	Barack	k1gMnSc1	Barack
Obama	Obama	k?	Obama
a	a	k8xC	a
Vladimir	Vladimir	k1gMnSc1	Vladimir
Putin	putin	k2eAgMnSc1d1	putin
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nP	aby
prodebatovali	prodebatovat	k5eAaPmAgMnP	prodebatovat
své	svůj	k3xOyFgInPc4	svůj
postoje	postoj	k1gInPc4	postoj
vůči	vůči	k7c3	vůči
krizi	krize	k1gFnSc3	krize
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
schůzka	schůzka	k1gFnSc1	schůzka
neodstranila	odstranit	k5eNaPmAgFnS	odstranit
jejich	jejich	k3xOp3gInPc4	jejich
rozdílné	rozdílný	k2eAgInPc4d1	rozdílný
názory	názor	k1gInPc4	názor
<g/>
,	,	kIx,	,
když	když	k8xS	když
USA	USA	kA	USA
preferovaly	preferovat	k5eAaImAgFnP	preferovat
řešení	řešení	k1gNnSc6	řešení
použitím	použití	k1gNnSc7	použití
vojenské	vojenský	k2eAgFnSc2d1	vojenská
intervence	intervence	k1gFnSc2	intervence
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
Rusko	Rusko	k1gNnSc1	Rusko
tehdy	tehdy	k6eAd1	tehdy
armádní	armádní	k2eAgInSc4d1	armádní
zásah	zásah	k1gInSc4	zásah
odmítlo	odmítnout	k5eAaPmAgNnS	odmítnout
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Následně	následně	k6eAd1	následně
ministr	ministr	k1gMnSc1	ministr
zahraničních	zahraniční	k2eAgFnPc2d1	zahraniční
věcí	věc	k1gFnPc2	věc
USA	USA	kA	USA
John	John	k1gMnSc1	John
Kerry	Kerra	k1gFnSc2	Kerra
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Bašár	Bašár	k1gInSc1	Bašár
Asad	Asada	k1gFnPc2	Asada
by	by	kYmCp3nS	by
mohl	moct	k5eAaImAgInS	moct
odvrátit	odvrátit	k5eAaPmF	odvrátit
útok	útok	k1gInSc4	útok
na	na	k7c4	na
Sýrii	Sýrie	k1gFnSc4	Sýrie
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
do	do	k7c2	do
týdne	týden	k1gInSc2	týden
odevzdal	odevzdat	k5eAaPmAgMnS	odevzdat
všechny	všechen	k3xTgFnPc4	všechen
chemické	chemický	k2eAgFnPc4d1	chemická
zbraně	zbraň	k1gFnPc4	zbraň
mezinárodnímu	mezinárodní	k2eAgInSc3d1	mezinárodní
společenství	společenství	k1gNnPc2	společenství
<g/>
.	.	kIx.	.
<g/>
Následně	následně	k6eAd1	následně
Sýrie	Sýrie	k1gFnSc1	Sýrie
přijala	přijmout	k5eAaPmAgFnS	přijmout
ruský	ruský	k2eAgInSc4d1	ruský
návrh	návrh	k1gInSc4	návrh
na	na	k7c4	na
předání	předání	k1gNnSc4	předání
chemických	chemický	k2eAgFnPc2d1	chemická
zbraní	zbraň	k1gFnPc2	zbraň
pod	pod	k7c4	pod
mezinárodní	mezinárodní	k2eAgFnSc4d1	mezinárodní
kontrolu	kontrola	k1gFnSc4	kontrola
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
odvoláním	odvolání	k1gNnSc7	odvolání
na	na	k7c4	na
syrského	syrský	k2eAgMnSc2d1	syrský
ministra	ministr	k1gMnSc2	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Valída	Valíd	k1gMnSc2	Valíd
Mualíma	Mualí	k2eAgFnPc7d1	Mualí
to	ten	k3xDgNnSc1	ten
oznámila	oznámit	k5eAaPmAgFnS	oznámit
ruská	ruský	k2eAgFnSc1d1	ruská
agentura	agentura	k1gFnSc1	agentura
Interfax	Interfax	k1gInSc1	Interfax
<g/>
.	.	kIx.	.
</s>
<s>
Krok	krok	k1gInSc4	krok
uvítala	uvítat	k5eAaPmAgFnS	uvítat
i	i	k9	i
Francie	Francie	k1gFnSc1	Francie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
v	v	k7c6	v
Radě	rada	k1gFnSc6	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
předložila	předložit	k5eAaPmAgFnS	předložit
návrh	návrh	k1gInSc4	návrh
rezoluce	rezoluce	k1gFnSc2	rezoluce
o	o	k7c6	o
chemických	chemický	k2eAgFnPc6d1	chemická
zbraních	zbraň	k1gFnPc6	zbraň
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
noci	noc	k1gFnSc6	noc
z	z	k7c2	z
27	[number]	k4	27
<g/>
.	.	kIx.	.
na	na	k7c4	na
28	[number]	k4	28
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2013	[number]	k4	2013
Rada	rada	k1gFnSc1	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
jednomyslně	jednomyslně	k6eAd1	jednomyslně
schválila	schválit	k5eAaPmAgFnS	schválit
rezoluci	rezoluce	k1gFnSc4	rezoluce
požadující	požadující	k2eAgFnSc4d1	požadující
likvidaci	likvidace	k1gFnSc4	likvidace
syrského	syrský	k2eAgInSc2d1	syrský
arzenálu	arzenál	k1gInSc2	arzenál
chemických	chemický	k2eAgFnPc2d1	chemická
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rezoluci	rezoluce	k1gFnSc6	rezoluce
bylo	být	k5eAaImAgNnS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
Sýrie	Sýrie	k1gFnSc1	Sýrie
nesmí	smět	k5eNaImIp3nS	smět
chemické	chemický	k2eAgFnPc4d1	chemická
zbraně	zbraň	k1gFnPc4	zbraň
používat	používat	k5eAaImF	používat
<g/>
,	,	kIx,	,
vyvíjet	vyvíjet	k5eAaImF	vyvíjet
či	či	k8xC	či
skladovat	skladovat	k5eAaImF	skladovat
a	a	k8xC	a
musí	muset	k5eAaImIp3nS	muset
plně	plně	k6eAd1	plně
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
s	s	k7c7	s
OSN	OSN	kA	OSN
na	na	k7c6	na
likvidaci	likvidace	k1gFnSc6	likvidace
svých	svůj	k3xOyFgFnPc2	svůj
zásob	zásoba	k1gFnPc2	zásoba
těchto	tento	k3xDgFnPc2	tento
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Rezoluce	rezoluce	k1gFnSc1	rezoluce
odsoudila	odsoudit	k5eAaPmAgFnS	odsoudit
použití	použití	k1gNnSc4	použití
chemických	chemický	k2eAgFnPc2d1	chemická
zbraní	zbraň	k1gFnPc2	zbraň
při	při	k7c6	při
srpnovém	srpnový	k2eAgInSc6d1	srpnový
útoku	útok	k1gInSc6	útok
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
nezmiňovala	zmiňovat	k5eNaImAgFnS	zmiňovat
se	se	k3xPyFc4	se
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
byl	být	k5eAaImAgInS	být
za	za	k7c4	za
incident	incident	k1gInSc4	incident
zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
<g/>
.	.	kIx.	.
</s>
<s>
Neobsahovala	obsahovat	k5eNaImAgFnS	obsahovat
ani	ani	k8xC	ani
bezprostřední	bezprostřední	k2eAgFnSc4d1	bezprostřední
hrozbu	hrozba	k1gFnSc4	hrozba
trestu	trest	k1gInSc2	trest
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
Sýrie	Sýrie	k1gFnSc1	Sýrie
požadavku	požadavek	k1gInSc2	požadavek
nepodřídila	podřídit	k5eNaPmAgFnS	podřídit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Do	do	k7c2	do
prvního	první	k4xOgInSc2	první
listopadu	listopad	k1gInSc2	listopad
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
podle	podle	k7c2	podle
rezoluce	rezoluce	k1gFnSc2	rezoluce
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
zajištěno	zajištěn	k2eAgNnSc4d1	zajištěno
a	a	k8xC	a
zničeno	zničen	k2eAgNnSc4d1	zničeno
kompletní	kompletní	k2eAgNnSc4d1	kompletní
výrobní	výrobní	k2eAgNnSc4d1	výrobní
zázemí	zázemí	k1gNnSc4	zázemí
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
vybavení	vybavení	k1gNnSc2	vybavení
pro	pro	k7c4	pro
plnění	plnění	k1gNnSc4	plnění
plynů	plyn	k1gInPc2	plyn
do	do	k7c2	do
raket	raketa	k1gFnPc2	raketa
a	a	k8xC	a
granátů	granát	k1gInPc2	granát
s	s	k7c7	s
nervovými	nervový	k2eAgFnPc7d1	nervová
látkami	látka	k1gFnPc7	látka
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
arzenál	arzenál	k1gInSc1	arzenál
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
poté	poté	k6eAd1	poté
zničen	zničit	k5eAaPmNgInS	zničit
nejpozději	pozdě	k6eAd3	pozdě
do	do	k7c2	do
poloviny	polovina	k1gFnSc2	polovina
roku	rok	k1gInSc2	rok
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zdrojů	zdroj	k1gInPc2	zdroj
OPCW	OPCW	kA	OPCW
syrská	syrský	k2eAgFnSc1d1	Syrská
vláda	vláda	k1gFnSc1	vláda
spolupracovala	spolupracovat	k5eAaImAgFnS	spolupracovat
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
31	[number]	k4	31
<g/>
.	.	kIx.	.
října	říjen	k1gInSc2	říjen
2013	[number]	k4	2013
již	již	k9	již
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
syrská	syrský	k2eAgNnPc4d1	syrské
přiznaná	přiznaný	k2eAgNnPc4d1	přiznané
zařízení	zařízení	k1gNnPc4	zařízení
pro	pro	k7c4	pro
výrobu	výroba	k1gFnSc4	výroba
chemických	chemický	k2eAgFnPc2d1	chemická
zbraní	zbraň	k1gFnPc2	zbraň
kompletně	kompletně	k6eAd1	kompletně
zničena	zničit	k5eAaPmNgFnS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Postupně	postupně	k6eAd1	postupně
byly	být	k5eAaImAgFnP	být
zničeny	zničit	k5eAaPmNgFnP	zničit
syrské	syrský	k2eAgFnPc1d1	Syrská
zásoby	zásoba	k1gFnPc1	zásoba
nervových	nervový	k2eAgInPc2d1	nervový
plynů	plyn	k1gInPc2	plyn
včetně	včetně	k7c2	včetně
sarinu	sarin	k1gInSc2	sarin
<g/>
,	,	kIx,	,
i	i	k9	i
yperitu	yperit	k1gInSc2	yperit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
tomu	ten	k3xDgNnSc3	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
chlor	chlor	k1gInSc1	chlor
je	být	k5eAaImIp3nS	být
používán	používat	k5eAaImNgInS	používat
i	i	k9	i
v	v	k7c6	v
civilní	civilní	k2eAgFnSc6d1	civilní
výrobě	výroba	k1gFnSc6	výroba
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
pro	pro	k7c4	pro
přípravu	příprava	k1gFnSc4	příprava
hnojiv	hnojivo	k1gNnPc2	hnojivo
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
výroba	výroba	k1gFnSc1	výroba
a	a	k8xC	a
zásoby	zásoba	k1gFnPc1	zásoba
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
<g/>
.	.	kIx.	.
</s>
<s>
Výroba	výroba	k1gFnSc1	výroba
chloru	chlor	k1gInSc2	chlor
navíc	navíc	k6eAd1	navíc
není	být	k5eNaImIp3nS	být
náročná	náročný	k2eAgFnSc1d1	náročná
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
jí	on	k3xPp3gFnSc3	on
v	v	k7c6	v
zásadě	zásada	k1gFnSc6	zásada
schopná	schopný	k2eAgFnSc1d1	schopná
jak	jak	k8xC	jak
syrská	syrský	k2eAgFnSc1d1	Syrská
vláda	vláda	k1gFnSc1	vláda
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
rebelové	rebel	k1gMnPc1	rebel
<g/>
.	.	kIx.	.
<g/>
Jeden	jeden	k4xCgInSc1	jeden
z	z	k7c2	z
členů	člen	k1gInPc2	člen
civilní	civilní	k2eAgFnSc2d1	civilní
obrany	obrana	k1gFnSc2	obrana
v	v	k7c6	v
Aleppu	Alepp	k1gInSc6	Alepp
dle	dle	k7c2	dle
stanice	stanice	k1gFnSc2	stanice
BBC	BBC	kA	BBC
prohlásil	prohlásit	k5eAaPmAgMnS	prohlásit
<g/>
,	,	kIx,	,
že	že	k8xS	že
6	[number]	k4	6
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2016	[number]	k4	2016
shodilo	shodit	k5eAaPmAgNnS	shodit
syrské	syrský	k2eAgNnSc1d1	syrské
letectvo	letectvo	k1gNnSc1	letectvo
na	na	k7c6	na
předměstí	předměstí	k1gNnSc6	předměstí
obklíčeného	obklíčený	k2eAgMnSc2d1	obklíčený
Aleppa	Alepp	k1gMnSc2	Alepp
čtyři	čtyři	k4xCgFnPc4	čtyři
barelové	barelový	k2eAgFnPc4d1	barelový
bomby	bomba	k1gFnPc4	bomba
s	s	k7c7	s
chlórem	chlór	k1gInSc7	chlór
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
exilové	exilový	k2eAgFnSc2d1	exilová
organizace	organizace	k1gFnSc2	organizace
SOHR	SOHR	kA	SOHR
i	i	k8xC	i
stanice	stanice	k1gFnSc2	stanice
BBC	BBC	kA	BBC
bylo	být	k5eAaImAgNnS	být
tak	tak	k6eAd1	tak
zraněno	zranit	k5eAaPmNgNnS	zranit
80	[number]	k4	80
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Syrská	syrský	k2eAgFnSc1d1	Syrská
vláda	vláda	k1gFnSc1	vláda
však	však	k9	však
nasazení	nasazení	k1gNnSc4	nasazení
chemikálií	chemikálie	k1gFnPc2	chemikálie
popřela	popřít	k5eAaPmAgFnS	popřít
<g/>
.4	.4	k4	.4
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2017	[number]	k4	2017
se	se	k3xPyFc4	se
odehrál	odehrát	k5eAaPmAgInS	odehrát
chemický	chemický	k2eAgInSc1d1	chemický
útok	útok	k1gInSc1	útok
v	v	k7c6	v
Chán	chán	k1gMnSc1	chán
Šajchúnu	Šajchúna	k1gFnSc4	Šajchúna
<g/>
,	,	kIx,	,
během	během	k7c2	během
kterého	který	k3yRgInSc2	který
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
přes	přes	k7c4	přes
100	[number]	k4	100
civilistů	civilista	k1gMnPc2	civilista
<g/>
.	.	kIx.	.
</s>
<s>
Chán	chán	k1gMnSc1	chán
Šajchún	Šajchún	k1gMnSc1	Šajchún
v	v	k7c6	v
době	doba	k1gFnSc6	doba
útoku	útok	k1gInSc2	útok
ovládali	ovládat	k5eAaImAgMnP	ovládat
povstalci	povstalec	k1gMnPc1	povstalec
z	z	k7c2	z
koalice	koalice	k1gFnSc2	koalice
Tahrír	Tahrír	k1gInSc1	Tahrír
aš-Šám	aš-Šat	k5eAaPmIp1nS	aš-Šat
(	(	kIx(	(
<g/>
bývalá	bývalý	k2eAgFnSc1d1	bývalá
fronta	fronta	k1gFnSc1	fronta
an-Nusra	an-Nusr	k1gInSc2	an-Nusr
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
a	a	k8xC	a
další	další	k2eAgInPc1d1	další
západní	západní	k2eAgInPc1d1	západní
státy	stát	k1gInPc1	stát
viní	vinit	k5eAaImIp3nP	vinit
z	z	k7c2	z
použití	použití	k1gNnSc2	použití
chemické	chemický	k2eAgFnSc2d1	chemická
látky	látka	k1gFnSc2	látka
sarinu	sarin	k1gInSc2	sarin
a	a	k8xC	a
chloru	chlor	k1gInSc2	chlor
vládu	vláda	k1gFnSc4	vláda
Bašára	Bašár	k1gMnSc2	Bašár
Asada	Asad	k1gMnSc2	Asad
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
syrské	syrský	k2eAgFnSc2d1	Syrská
vlády	vláda	k1gFnSc2	vláda
a	a	k8xC	a
Ruska	Rusko	k1gNnSc2	Rusko
pocházely	pocházet	k5eAaImAgFnP	pocházet
chemikálie	chemikálie	k1gFnPc1	chemikálie
ze	z	k7c2	z
skladu	sklad	k1gInSc2	sklad
povstalců	povstalec	k1gMnPc2	povstalec
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
po	po	k7c6	po
vládním	vládní	k2eAgInSc6d1	vládní
náletu	nálet	k1gInSc6	nálet
vybuchl	vybuchnout	k5eAaPmAgInS	vybuchnout
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
vyšetřovatelů	vyšetřovatel	k1gMnPc2	vyšetřovatel
OSN	OSN	kA	OSN
pro	pro	k7c4	pro
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
je	být	k5eAaImIp3nS	být
na	na	k7c6	na
vině	vina	k1gFnSc6	vina
z	z	k7c2	z
chemického	chemický	k2eAgInSc2d1	chemický
útoku	útok	k1gInSc2	útok
Assadův	Assadův	k2eAgInSc4d1	Assadův
režim	režim	k1gInSc4	režim
a	a	k8xC	a
podle	podle	k7c2	podle
OPCW	OPCW	kA	OPCW
zde	zde	k6eAd1	zde
lidé	člověk	k1gMnPc1	člověk
byli	být	k5eAaImAgMnP	být
vystaveni	vystavit	k5eAaPmNgMnP	vystavit
sarinu	sarin	k1gInSc3	sarin
<g/>
,	,	kIx,	,
zprávy	zpráva	k1gFnPc4	zpráva
však	však	k9	však
neupřesňují	upřesňovat	k5eNaImIp3nP	upřesňovat
<g/>
,	,	kIx,	,
kdo	kdo	k3yQnSc1	kdo
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
útok	útok	k1gInSc4	útok
zodpovědný	zodpovědný	k2eAgMnSc1d1	zodpovědný
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
odvetě	odveta	k1gFnSc6	odveta
za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
čin	čin	k1gInSc4	čin
provedly	provést	k5eAaPmAgFnP	provést
6	[number]	k4	6
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2017	[number]	k4	2017
americké	americký	k2eAgFnSc2d1	americká
torpédoborce	torpédoborec	k1gMnSc2	torpédoborec
útok	útok	k1gInSc4	útok
proti	proti	k7c3	proti
syrské	syrský	k2eAgFnSc3d1	Syrská
letecké	letecký	k2eAgFnSc3d1	letecká
základně	základna	k1gFnSc3	základna
Šajrát	Šajrát	k1gInSc4	Šajrát
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
USA	USA	kA	USA
z	z	k7c2	z
vypuštěných	vypuštěný	k2eAgFnPc2d1	vypuštěná
59	[number]	k4	59
střel	střela	k1gFnPc2	střela
dopadlo	dopadnout	k5eAaPmAgNnS	dopadnout
na	na	k7c4	na
základnu	základna	k1gFnSc4	základna
58	[number]	k4	58
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Ruska	Rusko	k1gNnSc2	Rusko
na	na	k7c4	na
základnu	základna	k1gFnSc4	základna
dopadlo	dopadnout	k5eAaPmAgNnS	dopadnout
jen	jen	k9	jen
23	[number]	k4	23
z	z	k7c2	z
59	[number]	k4	59
vystřelených	vystřelený	k2eAgInPc2d1	vystřelený
Tomahawků	tomahawk	k1gInPc2	tomahawk
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
pět	pět	k4xCc1	pět
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
syrských	syrský	k2eAgNnPc2d1	syrské
státních	státní	k2eAgNnPc2d1	státní
médií	médium	k1gNnPc2	médium
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
i	i	k9	i
dalších	další	k2eAgFnPc2d1	další
devět	devět	k4xCc1	devět
civilistů	civilista	k1gMnPc2	civilista
po	po	k7c6	po
zásahu	zásah	k1gInSc6	zásah
blízké	blízký	k2eAgFnSc2d1	blízká
vesnice	vesnice	k1gFnSc2	vesnice
<g/>
.7	.7	k4	.7
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2018	[number]	k4	2018
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
útoku	útok	k1gInSc3	útok
na	na	k7c4	na
město	město	k1gNnSc4	město
Dúmá	Dúmá	k1gFnSc1	Dúmá
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
poslední	poslední	k2eAgFnSc7d1	poslední
baštou	bašta	k1gFnSc7	bašta
povstalců	povstalec	k1gMnPc2	povstalec
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
východní	východní	k2eAgFnSc1d1	východní
Ghúta	Ghúta	k1gFnSc1	Ghúta
nedaleko	nedaleko	k7c2	nedaleko
Damašku	Damašek	k1gInSc2	Damašek
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
útoku	útok	k1gInSc6	útok
zemřelo	zemřít	k5eAaPmAgNnS	zemřít
42	[number]	k4	42
lidí	člověk	k1gMnPc2	člověk
a	a	k8xC	a
přes	přes	k7c4	přes
500	[number]	k4	500
lidí	člověk	k1gMnPc2	člověk
bylo	být	k5eAaImAgNnS	být
zraněno	zranit	k5eAaPmNgNnS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgFnPc1	některý
západní	západní	k2eAgFnPc1d1	západní
země	zem	k1gFnPc1	zem
včetně	včetně	k7c2	včetně
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Německa	Německo	k1gNnSc2	Německo
či	či	k8xC	či
Francie	Francie	k1gFnSc2	Francie
viní	vinit	k5eAaImIp3nS	vinit
syrské	syrský	k2eAgFnSc2d1	Syrská
vládní	vládní	k2eAgFnSc2d1	vládní
jednotky	jednotka	k1gFnSc2	jednotka
z	z	k7c2	z
použití	použití	k1gNnSc2	použití
chemických	chemický	k2eAgFnPc2d1	chemická
zbraní	zbraň	k1gFnPc2	zbraň
<g/>
,	,	kIx,	,
syrský	syrský	k2eAgInSc4d1	syrský
režim	režim	k1gInSc4	režim
i	i	k9	i
jeho	jeho	k3xOp3gMnPc1	jeho
spojenci	spojenec	k1gMnPc1	spojenec
Rusko	Rusko	k1gNnSc1	Rusko
a	a	k8xC	a
Írán	Írán	k1gInSc1	Írán
použití	použití	k1gNnSc2	použití
chemických	chemický	k2eAgFnPc2d1	chemická
zbraní	zbraň	k1gFnPc2	zbraň
popřely	popřít	k5eAaPmAgInP	popřít
a	a	k8xC	a
žádají	žádat	k5eAaImIp3nP	žádat
nezávislé	závislý	k2eNgNnSc4d1	nezávislé
vyšetření	vyšetření	k1gNnSc4	vyšetření
experty	expert	k1gMnPc7	expert
OPCW	OPCW	kA	OPCW
<g/>
.	.	kIx.	.
</s>
<s>
Americký	americký	k2eAgMnSc1d1	americký
prezident	prezident	k1gMnSc1	prezident
Donald	Donald	k1gMnSc1	Donald
Trump	Trump	k1gMnSc1	Trump
kvůli	kvůli	k7c3	kvůli
útoku	útok	k1gInSc3	útok
pohrozil	pohrozit	k5eAaPmAgMnS	pohrozit
odvetnou	odvetný	k2eAgFnSc7d1	odvetná
mezinárodní	mezinárodní	k2eAgFnSc7d1	mezinárodní
akcí	akce	k1gFnSc7	akce
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
obviněním	obvinění	k1gNnSc7	obvinění
z	z	k7c2	z
chemického	chemický	k2eAgInSc2d1	chemický
útoku	útok	k1gInSc2	útok
přišli	přijít	k5eAaPmAgMnP	přijít
islamisté	islamista	k1gMnPc1	islamista
z	z	k7c2	z
Džaiš	Džaiš	k1gMnSc1	Džaiš
al-Islam	al-Islam	k1gInSc1	al-Islam
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
byli	být	k5eAaImAgMnP	být
sami	sám	k3xTgMnPc1	sám
obviněni	obvinit	k5eAaPmNgMnP	obvinit
z	z	k7c2	z
použití	použití	k1gNnSc2	použití
chemických	chemický	k2eAgFnPc2d1	chemická
zbraní	zbraň	k1gFnPc2	zbraň
proti	proti	k7c3	proti
Kurdy	Kurd	k1gMnPc7	Kurd
obývané	obývaný	k2eAgFnSc2d1	obývaná
části	část	k1gFnSc2	část
Aleppa	Aleppa	k1gFnSc1	Aleppa
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2016	[number]	k4	2016
<g/>
.	.	kIx.	.
</s>
<s>
Syrská	syrský	k2eAgFnSc1d1	Syrská
armáda	armáda	k1gFnSc1	armáda
ovládla	ovládnout	k5eAaPmAgFnS	ovládnout
město	město	k1gNnSc4	město
Dúmá	Dúmé	k1gNnPc4	Dúmé
12	[number]	k4	12
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
završila	završit	k5eAaPmAgFnS	završit
boj	boj	k1gInSc4	boj
o	o	k7c4	o
celou	celý	k2eAgFnSc4d1	celá
oblast	oblast	k1gFnSc4	oblast
Ghúta	Ghút	k1gInSc2	Ghút
<g/>
.	.	kIx.	.
</s>
<s>
Syrská	syrský	k2eAgFnSc1d1	Syrská
opozice	opozice	k1gFnSc1	opozice
se	se	k3xPyFc4	se
tak	tak	k9	tak
nyní	nyní	k6eAd1	nyní
soustředí	soustředit	k5eAaPmIp3nS	soustředit
na	na	k7c6	na
severozápadě	severozápad	k1gInSc6	severozápad
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
<g/>
USA	USA	kA	USA
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
Británie	Británie	k1gFnSc1	Británie
14	[number]	k4	14
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2018	[number]	k4	2018
zaútočily	zaútočit	k5eAaPmAgFnP	zaútočit
bombardováním	bombardování	k1gNnSc7	bombardování
Damašku	Damašek	k1gInSc2	Damašek
a	a	k8xC	a
Homsu	Homs	k1gInSc2	Homs
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
útok	útok	k1gInSc1	útok
byl	být	k5eAaImAgInS	být
odvetou	odveta	k1gFnSc7	odveta
za	za	k7c4	za
údajné	údajný	k2eAgNnSc4d1	údajné
použití	použití	k1gNnSc4	použití
chemických	chemický	k2eAgFnPc2d1	chemická
zbraní	zbraň	k1gFnPc2	zbraň
na	na	k7c4	na
město	město	k1gNnSc4	město
Dúmá	Dúmá	k1gFnSc1	Dúmá
syrskou	syrský	k2eAgFnSc7d1	Syrská
armádou	armáda	k1gFnSc7	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
ministra	ministr	k1gMnSc2	ministr
zahraničí	zahraničí	k1gNnSc2	zahraničí
Stropnického	stropnický	k2eAgNnSc2d1	Stropnické
vnímá	vnímat	k5eAaImIp3nS	vnímat
Česko	Česko	k1gNnSc4	Česko
zásah	zásah	k1gInSc1	zásah
západních	západní	k2eAgMnPc2d1	západní
spojenců	spojenec	k1gMnPc2	spojenec
v	v	k7c4	v
Sýrii	Sýrie	k1gFnSc4	Sýrie
jako	jako	k8xC	jako
vzkaz	vzkaz	k1gInSc4	vzkaz
komukoli	kdokoli	k3yInSc3	kdokoli
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
by	by	kYmCp3nS	by
chtěl	chtít	k5eAaImAgMnS	chtít
použít	použít	k5eAaPmF	použít
chemické	chemický	k2eAgFnPc4d1	chemická
zbraně	zbraň	k1gFnPc4	zbraň
<g/>
.	.	kIx.	.
</s>
<s>
Odvetný	odvetný	k2eAgInSc1d1	odvetný
útok	útok	k1gInSc1	útok
byl	být	k5eAaImAgInS	být
prý	prý	k9	prý
legitimní	legitimní	k2eAgMnSc1d1	legitimní
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
i	i	k9	i
bez	bez	k7c2	bez
pověření	pověření	k1gNnSc2	pověření
Rady	rada	k1gFnSc2	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Ruska	Rusko	k1gNnSc2	Rusko
bylo	být	k5eAaImAgNnS	být
vypáleno	vypálit	k5eAaPmNgNnS	vypálit
103	[number]	k4	103
raket	raketa	k1gFnPc2	raketa
na	na	k7c4	na
cíle	cíl	k1gInPc4	cíl
u	u	k7c2	u
Damašku	Damašek	k1gInSc2	Damašek
a	a	k8xC	a
Homsu	Homs	k1gInSc2	Homs
a	a	k8xC	a
Sýrie	Sýrie	k1gFnSc1	Sýrie
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
díky	díky	k7c3	díky
ruským	ruský	k2eAgInPc3d1	ruský
protiraketovým	protiraketový	k2eAgInPc3d1	protiraketový
systémům	systém	k1gInPc3	systém
sestřelila	sestřelit	k5eAaPmAgFnS	sestřelit
71	[number]	k4	71
<g/>
.	.	kIx.	.
</s>
<s>
Dvě	dva	k4xCgFnPc1	dva
nevybuchlé	vybuchlý	k2eNgFnPc1d1	nevybuchlá
rakety	raketa	k1gFnPc1	raketa
z	z	k7c2	z
útoku	útok	k1gInSc2	útok
byly	být	k5eAaImAgFnP	být
převezeny	převézt	k5eAaPmNgFnP	převézt
do	do	k7c2	do
Ruska	Rusko	k1gNnSc2	Rusko
<g/>
.	.	kIx.	.
<g/>
Televize	televize	k1gFnSc1	televize
Rusko-	Rusko-	k1gFnSc2	Rusko-
<g/>
24	[number]	k4	24
odvysílala	odvysílat	k5eAaPmAgFnS	odvysílat
rozhovor	rozhovor	k1gInSc4	rozhovor
s	s	k7c7	s
klukem	kluk	k1gMnSc7	kluk
Hassanem	Hassan	k1gMnSc7	Hassan
Diabem	Diab	k1gMnSc7	Diab
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
se	se	k3xPyFc4	se
mělo	mít	k5eAaImAgNnS	mít
dostat	dostat	k5eAaPmF	dostat
ošetření	ošetření	k1gNnSc4	ošetření
ve	v	k7c6	v
videu	video	k1gNnSc6	video
z	z	k7c2	z
nemocnice	nemocnice	k1gFnSc2	nemocnice
po	po	k7c6	po
údajném	údajný	k2eAgInSc6d1	údajný
chemickém	chemický	k2eAgInSc6d1	chemický
útoku	útok	k1gInSc6	útok
<g/>
,	,	kIx,	,
ve	v	k7c6	v
kterém	který	k3yRgInSc6	který
se	se	k3xPyFc4	se
tvrdí	tvrdit	k5eAaImIp3nS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
chemický	chemický	k2eAgInSc1d1	chemický
útok	útok	k1gInSc1	útok
byl	být	k5eAaImAgInS	být
fingovaný	fingovaný	k2eAgMnSc1d1	fingovaný
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Důležité	důležitý	k2eAgFnPc1d1	důležitá
bitvy	bitva	k1gFnPc1	bitva
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Bitva	bitva	k1gFnSc1	bitva
o	o	k7c4	o
Damašek	Damašek	k1gInSc4	Damašek
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
poloviny	polovina	k1gFnSc2	polovina
července	červenec	k1gInSc2	červenec
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
probíhala	probíhat	k5eAaImAgFnS	probíhat
přibližně	přibližně	k6eAd1	přibližně
týden	týden	k1gInSc4	týden
bitva	bitva	k1gFnSc1	bitva
o	o	k7c4	o
syrské	syrský	k2eAgNnSc4d1	syrské
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
Damašek	Damašek	k1gInSc4	Damašek
<g/>
.	.	kIx.	.
</s>
<s>
Zpočátku	zpočátku	k6eAd1	zpočátku
povstalci	povstalec	k1gMnPc1	povstalec
zaznamenávali	zaznamenávat	k5eAaImAgMnP	zaznamenávat
četné	četný	k2eAgInPc4d1	četný
úspěchy	úspěch	k1gInPc4	úspěch
a	a	k8xC	a
dostali	dostat	k5eAaPmAgMnP	dostat
se	se	k3xPyFc4	se
až	až	k9	až
do	do	k7c2	do
centra	centrum	k1gNnSc2	centrum
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
bojovalo	bojovat	k5eAaImAgNnS	bojovat
se	se	k3xPyFc4	se
i	i	k9	i
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
některých	některý	k3yIgFnPc2	některý
vládních	vládní	k2eAgFnPc2d1	vládní
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
protiútoku	protiútok	k1gInSc6	protiútok
syrských	syrský	k2eAgNnPc2d1	syrské
vládních	vládní	k2eAgNnPc2d1	vládní
vojsk	vojsko	k1gNnPc2	vojsko
však	však	k9	však
byly	být	k5eAaImAgFnP	být
povstalecké	povstalecký	k2eAgFnPc4d1	povstalecká
jednotky	jednotka	k1gFnPc4	jednotka
nuceny	nucen	k2eAgFnPc4d1	nucena
se	se	k3xPyFc4	se
postupně	postupně	k6eAd1	postupně
stahovat	stahovat	k5eAaImF	stahovat
<g/>
.	.	kIx.	.
</s>
<s>
Objevilo	objevit	k5eAaPmAgNnS	objevit
se	se	k3xPyFc4	se
i	i	k9	i
mnoho	mnoho	k4c1	mnoho
zpráv	zpráva	k1gFnPc2	zpráva
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
armáda	armáda	k1gFnSc1	armáda
bombardovala	bombardovat	k5eAaImAgFnS	bombardovat
čtvrtě	čtvrt	k1gFnPc1	čtvrt
obývané	obývaný	k2eAgMnPc4d1	obývaný
civilisty	civilista	k1gMnPc4	civilista
<g/>
,	,	kIx,	,
tyto	tento	k3xDgFnPc4	tento
zprávy	zpráva	k1gFnPc4	zpráva
však	však	k9	však
nebylo	být	k5eNaImAgNnS	být
možné	možný	k2eAgNnSc1d1	možné
potvrdit	potvrdit	k5eAaPmF	potvrdit
<g/>
.	.	kIx.	.
<g/>
Dne	den	k1gInSc2	den
18	[number]	k4	18
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2012	[number]	k4	2012
provedli	provést	k5eAaPmAgMnP	provést
povstalci	povstalec	k1gMnPc1	povstalec
atentát	atentát	k1gInSc4	atentát
na	na	k7c4	na
nejbližší	blízký	k2eAgMnPc4d3	nejbližší
spolupracovníky	spolupracovník	k1gMnPc4	spolupracovník
syrského	syrský	k2eAgMnSc2d1	syrský
prezidenta	prezident	k1gMnSc2	prezident
Asada	Asad	k1gMnSc2	Asad
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
zemřel	zemřít	k5eAaPmAgMnS	zemřít
ministr	ministr	k1gMnSc1	ministr
obrany	obrana	k1gFnSc2	obrana
Dáud	Dáud	k1gMnSc1	Dáud
Rádžha	Rádžha	k1gMnSc1	Rádžha
<g/>
,	,	kIx,	,
při	při	k7c6	při
převozu	převoz	k1gInSc6	převoz
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
zraněním	zranění	k1gNnPc3	zranění
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
i	i	k9	i
jeho	jeho	k3xOp3gMnSc1	jeho
náměstek	náměstek	k1gMnSc1	náměstek
a	a	k8xC	a
prezidentův	prezidentův	k2eAgMnSc1d1	prezidentův
švagr	švagr	k1gMnSc1	švagr
Ásif	Ásif	k1gMnSc1	Ásif
Šaukát	Šaukát	k1gMnSc1	Šaukát
<g/>
.	.	kIx.	.
</s>
<s>
Zabiti	zabít	k5eAaPmNgMnP	zabít
byli	být	k5eAaImAgMnP	být
také	také	k9	také
bývalý	bývalý	k2eAgMnSc1d1	bývalý
ministr	ministr	k1gMnSc1	ministr
obrany	obrana	k1gFnSc2	obrana
a	a	k8xC	a
poradce	poradce	k1gMnSc2	poradce
viceprezidenta	viceprezident	k1gMnSc2	viceprezident
Hassan	Hassan	k1gMnSc1	Hassan
Turkmaní	Turkmaný	k2eAgMnPc1d1	Turkmaný
a	a	k8xC	a
šéf	šéf	k1gMnSc1	šéf
sekce	sekce	k1gFnSc2	sekce
vyšetřování	vyšetřování	k1gNnPc2	vyšetřování
Syrské	syrský	k2eAgFnSc2d1	Syrská
zpravodajské	zpravodajský	k2eAgFnSc2d1	zpravodajská
služby	služba	k1gFnSc2	služba
Hazífa	Hazíf	k1gMnSc2	Hazíf
Machlúfa	Machlúf	k1gMnSc2	Machlúf
<g/>
.	.	kIx.	.
</s>
<s>
Několik	několik	k4yIc1	několik
dalších	další	k2eAgInPc2d1	další
vysokých	vysoký	k2eAgInPc2d1	vysoký
syrských	syrský	k2eAgInPc2d1	syrský
činitelů	činitel	k1gInPc2	činitel
bylo	být	k5eAaImAgNnS	být
zraněno	zranit	k5eAaPmNgNnS	zranit
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
nimi	on	k3xPp3gInPc7	on
byli	být	k5eAaImAgMnP	být
i	i	k9	i
ministr	ministr	k1gMnSc1	ministr
vnitra	vnitro	k1gNnSc2	vnitro
Muhammad	Muhammad	k1gInSc4	Muhammad
Ibráhím	Ibráhí	k1gNnSc7	Ibráhí
Šaar	Šaara	k1gFnPc2	Šaara
a	a	k8xC	a
šéf	šéf	k1gMnSc1	šéf
syrské	syrský	k2eAgFnSc2d1	Syrská
rozvědky	rozvědka	k1gFnSc2	rozvědka
Hišám	Hiša	k1gFnPc3	Hiša
Bachtijar	Bachtijar	k1gInSc4	Bachtijar
<g/>
,	,	kIx,	,
klíčová	klíčový	k2eAgFnSc1d1	klíčová
postava	postava	k1gFnSc1	postava
represí	represe	k1gFnPc2	represe
proti	proti	k7c3	proti
povstalcům	povstalec	k1gMnPc3	povstalec
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
svým	svůj	k3xOyFgMnPc3	svůj
zraněním	zranění	k1gNnSc7	zranění
20	[number]	k4	20
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
podlehl	podlehnout	k5eAaPmAgMnS	podlehnout
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
odpovědnosti	odpovědnost	k1gFnSc3	odpovědnost
za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
útok	útok	k1gInSc4	útok
se	se	k3xPyFc4	se
přihlásily	přihlásit	k5eAaPmAgFnP	přihlásit
hned	hned	k6eAd1	hned
dvě	dva	k4xCgFnPc4	dva
skupiny	skupina	k1gFnPc1	skupina
povstalců	povstalec	k1gMnPc2	povstalec
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc4	který
proti	proti	k7c3	proti
režimu	režim	k1gInSc3	režim
bojovaly	bojovat	k5eAaImAgInP	bojovat
už	už	k6eAd1	už
od	od	k7c2	od
jara	jaro	k1gNnSc2	jaro
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
islamistická	islamistický	k2eAgFnSc1d1	islamistická
skupina	skupina	k1gFnSc1	skupina
Liwa	Liw	k1gInSc2	Liw
al-Islám	al-Islat	k5eAaImIp1nS	al-Islat
a	a	k8xC	a
prozápadní	prozápadní	k2eAgFnSc1d1	prozápadní
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
syrská	syrský	k2eAgFnSc1d1	Syrská
armáda	armáda	k1gFnSc1	armáda
<g/>
.	.	kIx.	.
<g/>
Objevovaly	objevovat	k5eAaImAgFnP	objevovat
se	se	k3xPyFc4	se
spekulace	spekulace	k1gFnPc1	spekulace
<g/>
,	,	kIx,	,
že	že	k8xS	že
prezident	prezident	k1gMnSc1	prezident
Asad	Asad	k1gMnSc1	Asad
uprchl	uprchnout	k5eAaPmAgMnS	uprchnout
do	do	k7c2	do
bezpečného	bezpečný	k2eAgNnSc2d1	bezpečné
přímořského	přímořský	k2eAgNnSc2d1	přímořské
letoviska	letovisko	k1gNnSc2	letovisko
Latákie	Latákie	k1gFnSc2	Latákie
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgFnPc1	ten
však	však	k9	však
byly	být	k5eAaImAgFnP	být
později	pozdě	k6eAd2	pozdě
samotným	samotný	k2eAgInSc7d1	samotný
Asadem	Asad	k1gInSc7	Asad
vyvráceny	vyvrátit	k5eAaPmNgFnP	vyvrátit
<g/>
,	,	kIx,	,
když	když	k8xS	když
se	se	k3xPyFc4	se
objevil	objevit	k5eAaPmAgMnS	objevit
na	na	k7c6	na
veřejnosti	veřejnost	k1gFnSc6	veřejnost
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Bitva	bitva	k1gFnSc1	bitva
o	o	k7c6	o
Aleppo	Aleppa	k1gFnSc5	Aleppa
===	===	k?	===
</s>
</p>
<p>
<s>
Od	od	k7c2	od
25	[number]	k4	25
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
2012	[number]	k4	2012
probíhaly	probíhat	k5eAaImAgInP	probíhat
boje	boj	k1gInPc1	boj
v	v	k7c6	v
největším	veliký	k2eAgNnSc6d3	veliký
městě	město	k1gNnSc6	město
Sýrie	Sýrie	k1gFnSc2	Sýrie
Aleppu	Alepp	k1gInSc2	Alepp
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
ovládli	ovládnout	k5eAaPmAgMnP	ovládnout
rebelové	rebel	k1gMnPc1	rebel
východní	východní	k2eAgFnSc4d1	východní
část	část	k1gFnSc4	část
města	město	k1gNnSc2	město
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
vládní	vládní	k2eAgFnPc1d1	vládní
jednotky	jednotka	k1gFnPc1	jednotka
měly	mít	k5eAaImAgFnP	mít
nadále	nadále	k6eAd1	nadále
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
západní	západní	k2eAgFnSc4d1	západní
část	část	k1gFnSc4	část
Aleppa	Alepp	k1gMnSc2	Alepp
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
2013	[number]	k4	2013
a	a	k8xC	a
2014	[number]	k4	2014
provedly	provést	k5eAaPmAgFnP	provést
vládě	vláda	k1gFnSc3	vláda
věrné	věrný	k2eAgFnPc1d1	věrná
jednotky	jednotka	k1gFnPc1	jednotka
rozsáhlou	rozsáhlý	k2eAgFnSc4d1	rozsáhlá
ofenzivu	ofenziva	k1gFnSc4	ofenziva
proti	proti	k7c3	proti
povstaleckým	povstalecký	k2eAgFnPc3d1	povstalecká
čtvrtím	čtvrt	k1gFnPc3	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Ofenziva	ofenziva	k1gFnSc1	ofenziva
však	však	k9	však
skončila	skončit	k5eAaPmAgFnS	skončit
neúspěchem	neúspěch	k1gInSc7	neúspěch
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Další	další	k2eAgFnSc4d1	další
ofenzivu	ofenziva	k1gFnSc4	ofenziva
zahájili	zahájit	k5eAaPmAgMnP	zahájit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
povstalci	povstalec	k1gMnPc7	povstalec
<g/>
.	.	kIx.	.
</s>
<s>
Podařilo	podařit	k5eAaPmAgNnS	podařit
se	se	k3xPyFc4	se
jim	on	k3xPp3gMnPc3	on
obsadit	obsadit	k5eAaPmF	obsadit
některé	některý	k3yIgFnPc4	některý
čtvrti	čtvrt	k1gFnPc4	čtvrt
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
byly	být	k5eAaImAgFnP	být
dosud	dosud	k6eAd1	dosud
pod	pod	k7c7	pod
kontrolou	kontrola	k1gFnSc7	kontrola
vlády	vláda	k1gFnSc2	vláda
prezidenta	prezident	k1gMnSc2	prezident
Asada	Asad	k1gMnSc2	Asad
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
vládní	vládní	k2eAgFnPc1d1	vládní
jednotky	jednotka	k1gFnPc1	jednotka
se	se	k3xPyFc4	se
v	v	k7c6	v
západních	západní	k2eAgFnPc6d1	západní
částech	část	k1gFnPc6	část
města	město	k1gNnSc2	město
celkově	celkově	k6eAd1	celkově
udržely	udržet	k5eAaPmAgFnP	udržet
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
2016	[number]	k4	2016
se	se	k3xPyFc4	se
jednotkám	jednotka	k1gFnPc3	jednotka
Syrské	syrský	k2eAgFnSc2d1	Syrská
armády	armáda	k1gFnSc2	armáda
podařilo	podařit	k5eAaPmAgNnS	podařit
zcela	zcela	k6eAd1	zcela
obklíčit	obklíčit	k5eAaPmF	obklíčit
povstalecké	povstalecký	k2eAgFnPc4d1	povstalecká
čtvrti	čtvrt	k1gFnPc4	čtvrt
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
ofenziva	ofenziva	k1gFnSc1	ofenziva
přišla	přijít	k5eAaPmAgFnS	přijít
na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
<g/>
.	.	kIx.	.
</s>
<s>
Povstalecká	povstalecký	k2eAgFnSc1d1	povstalecká
obrana	obrana	k1gFnSc1	obrana
se	se	k3xPyFc4	se
po	po	k7c6	po
masivním	masivní	k2eAgNnSc6d1	masivní
leteckém	letecký	k2eAgNnSc6d1	letecké
bombardování	bombardování	k1gNnSc6	bombardování
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
ruských	ruský	k2eAgFnPc2d1	ruská
a	a	k8xC	a
vládních	vládní	k2eAgFnPc2d1	vládní
sil	síla	k1gFnPc2	síla
zhroutila	zhroutit	k5eAaPmAgFnS	zhroutit
a	a	k8xC	a
vládním	vládní	k2eAgFnPc3d1	vládní
jednotkám	jednotka	k1gFnPc3	jednotka
se	s	k7c7	s
7	[number]	k4	7
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
podařilo	podařit	k5eAaPmAgNnS	podařit
dobýt	dobýt	k5eAaPmF	dobýt
historické	historický	k2eAgNnSc4d1	historické
jádro	jádro	k1gNnSc4	jádro
Aleppa	Alepp	k1gMnSc2	Alepp
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
14	[number]	k4	14
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2016	[number]	k4	2016
zbývalo	zbývat	k5eAaImAgNnS	zbývat
povstalcům	povstalec	k1gMnPc3	povstalec
již	již	k6eAd1	již
pouze	pouze	k6eAd1	pouze
2,5	[number]	k4	2,5
kilometru	kilometr	k1gInSc2	kilometr
čtverečních	čtvereční	k2eAgInPc2d1	čtvereční
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
byla	být	k5eAaImAgFnS	být
dohodnuta	dohodnout	k5eAaPmNgFnS	dohodnout
evakuace	evakuace	k1gFnSc1	evakuace
posledních	poslední	k2eAgFnPc2d1	poslední
skupin	skupina	k1gFnPc2	skupina
povstalců	povstalec	k1gMnPc2	povstalec
<g/>
,	,	kIx,	,
zakončená	zakončený	k2eAgFnSc1d1	zakončená
22	[number]	k4	22
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2016	[number]	k4	2016
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
armáda	armáda	k1gFnSc1	armáda
převzala	převzít	k5eAaPmAgFnS	převzít
na	na	k7c4	na
městem	město	k1gNnSc7	město
plnou	plný	k2eAgFnSc4d1	plná
kontrolu	kontrola	k1gFnSc4	kontrola
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
největší	veliký	k2eAgNnSc4d3	veliký
vítězství	vítězství	k1gNnSc4	vítězství
Syrské	syrský	k2eAgFnSc2d1	Syrská
armády	armáda	k1gFnSc2	armáda
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Bitva	bitva	k1gFnSc1	bitva
o	o	k7c4	o
Kusajr	Kusajr	k1gInSc4	Kusajr
===	===	k?	===
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
5	[number]	k4	5
<g/>
.	.	kIx.	.
června	červen	k1gInSc2	červen
2013	[number]	k4	2013
se	se	k3xPyFc4	se
syrské	syrský	k2eAgFnSc3d1	Syrská
armádě	armáda	k1gFnSc3	armáda
s	s	k7c7	s
podporou	podpora	k1gFnSc7	podpora
Hizballáhu	Hizballáh	k1gInSc2	Hizballáh
podařilo	podařit	k5eAaPmAgNnS	podařit
po	po	k7c6	po
téměř	téměř	k6eAd1	téměř
dvou	dva	k4xCgFnPc6	dva
letech	léto	k1gNnPc6	léto
bojů	boj	k1gInPc2	boj
dobýt	dobýt	k5eAaPmF	dobýt
město	město	k1gNnSc4	město
Kusajr	Kusajra	k1gFnPc2	Kusajra
u	u	k7c2	u
hranice	hranice	k1gFnSc2	hranice
s	s	k7c7	s
Libanonem	Libanon	k1gInSc7	Libanon
<g/>
.	.	kIx.	.
</s>
<s>
Povstalci	povstalec	k1gMnPc1	povstalec
v	v	k7c6	v
Homsu	Homs	k1gInSc6	Homs
tak	tak	k6eAd1	tak
byli	být	k5eAaImAgMnP	být
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
odříznuti	odříznout	k5eAaPmNgMnP	odříznout
od	od	k7c2	od
zásob	zásoba	k1gFnPc2	zásoba
<g/>
.	.	kIx.	.
</s>
<s>
Následkem	následkem	k7c2	následkem
bitvy	bitva	k1gFnSc2	bitva
o	o	k7c4	o
Kusajr	Kusajr	k1gInSc4	Kusajr
se	se	k3xPyFc4	se
Syrské	syrský	k2eAgFnSc3d1	Syrská
armádě	armáda	k1gFnSc3	armáda
podařilo	podařit	k5eAaPmAgNnS	podařit
osvobodit	osvobodit	k5eAaPmF	osvobodit
Homs	Homs	k1gInSc4	Homs
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zúčastněné	zúčastněný	k2eAgFnPc1d1	zúčastněná
strany	strana	k1gFnPc1	strana
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Vláda	vláda	k1gFnSc1	vláda
===	===	k?	===
</s>
</p>
<p>
<s>
Na	na	k7c6	na
jedné	jeden	k4xCgFnSc6	jeden
straně	strana	k1gFnSc6	strana
stojí	stát	k5eAaImIp3nS	stát
současná	současný	k2eAgFnSc1d1	současná
syrská	syrský	k2eAgFnSc1d1	Syrská
vláda	vláda	k1gFnSc1	vláda
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
Bašárem	Bašár	k1gMnSc7	Bašár
al-Asadem	al-Asad	k1gInSc7	al-Asad
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
podezřelý	podezřelý	k2eAgMnSc1d1	podezřelý
z	z	k7c2	z
válečných	válečný	k2eAgInPc2d1	válečný
zločinů	zločin	k1gInPc2	zločin
<g/>
.	.	kIx.	.
</s>
<s>
Asadova	Asadův	k2eAgFnSc1d1	Asadova
vláda	vláda	k1gFnSc1	vláda
je	být	k5eAaImIp3nS	být
podporována	podporovat	k5eAaImNgFnS	podporovat
syrskou	syrský	k2eAgFnSc7d1	Syrská
arabskou	arabský	k2eAgFnSc7d1	arabská
armádou	armáda	k1gFnSc7	armáda
(	(	kIx(	(
<g/>
SAA	SAA	kA	SAA
<g/>
)	)	kIx)	)
čítající	čítající	k2eAgFnSc4d1	čítající
125	[number]	k4	125
000	[number]	k4	000
aktivních	aktivní	k2eAgMnPc2d1	aktivní
bojovníků	bojovník	k1gMnPc2	bojovník
a	a	k8xC	a
její	její	k3xOp3gFnSc7	její
dobrovolnickou	dobrovolnický	k2eAgFnSc7d1	dobrovolnická
ozbrojenou	ozbrojený	k2eAgFnSc7d1	ozbrojená
milicí	milice	k1gFnSc7	milice
Národní	národní	k2eAgFnSc2d1	národní
obranné	obranný	k2eAgFnSc2d1	obranná
jednotky	jednotka	k1gFnSc2	jednotka
(	(	kIx(	(
<g/>
NDF	NDF	kA	NDF
<g/>
)	)	kIx)	)
čítající	čítající	k2eAgFnSc4d1	čítající
přibližně	přibližně	k6eAd1	přibližně
100	[number]	k4	100
000	[number]	k4	000
bojovníků	bojovník	k1gMnPc2	bojovník
<g/>
,	,	kIx,	,
stranickou	stranický	k2eAgFnSc7d1	stranická
milicí	milice	k1gFnSc7	milice
Šabíhá	Šabíhá	k1gFnSc2	Šabíhá
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
také	také	k9	také
podezřelá	podezřelý	k2eAgFnSc1d1	podezřelá
z	z	k7c2	z
vážných	vážný	k2eAgInPc2d1	vážný
zločinů	zločin	k1gInPc2	zločin
<g/>
,	,	kIx,	,
a	a	k8xC	a
libanonskou	libanonský	k2eAgFnSc7d1	libanonská
šíitskou	šíitský	k2eAgFnSc7d1	šíitská
milicí	milice	k1gFnSc7	milice
Hizballáh	Hizballáha	k1gFnPc2	Hizballáha
<g/>
,	,	kIx,	,
řazenou	řazený	k2eAgFnSc4d1	řazená
mezi	mezi	k7c4	mezi
teroristické	teroristický	k2eAgFnPc4d1	teroristická
organizace	organizace	k1gFnPc4	organizace
<g/>
.	.	kIx.	.
</s>
<s>
Ta	ten	k3xDgFnSc1	ten
se	se	k3xPyFc4	se
snaží	snažit	k5eAaImIp3nS	snažit
udržet	udržet	k5eAaPmF	udržet
u	u	k7c2	u
moci	moc	k1gFnSc2	moc
režim	režim	k1gInSc1	režim
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
ji	on	k3xPp3gFnSc4	on
dlouhá	dlouhý	k2eAgNnPc4d1	dlouhé
léta	léto	k1gNnPc4	léto
podporoval	podporovat	k5eAaImAgMnS	podporovat
ve	v	k7c6	v
střetech	střet	k1gInPc6	střet
s	s	k7c7	s
Izraelem	Izrael	k1gInSc7	Izrael
<g/>
.	.	kIx.	.
</s>
<s>
Většina	většina	k1gFnSc1	většina
alavitů	alavit	k1gInPc2	alavit
<g/>
,	,	kIx,	,
ke	k	k7c3	k
kterým	který	k3yIgFnPc3	který
patří	patřit	k5eAaImIp3nS	patřit
i	i	k9	i
Asad	Asad	k1gInSc1	Asad
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc1	jeho
rodina	rodina	k1gFnSc1	rodina
<g/>
,	,	kIx,	,
syrských	syrský	k2eAgMnPc2d1	syrský
křesťanů	křesťan	k1gMnPc2	křesťan
a	a	k8xC	a
drúzů	drúz	k1gMnPc2	drúz
podporuje	podporovat	k5eAaImIp3nS	podporovat
syrskou	syrský	k2eAgFnSc4d1	Syrská
vládu	vláda	k1gFnSc4	vláda
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
se	se	k3xPyFc4	se
bojí	bát	k5eAaImIp3nP	bát
represí	represe	k1gFnPc2	represe
proti	proti	k7c3	proti
menšinám	menšina	k1gFnPc3	menšina
v	v	k7c6	v
případě	případ	k1gInSc6	případ
vítězství	vítězství	k1gNnSc2	vítězství
sunnitských	sunnitský	k2eAgMnPc2d1	sunnitský
islamistů	islamista	k1gMnPc2	islamista
<g/>
.	.	kIx.	.
</s>
<s>
Asad	Asad	k6eAd1	Asad
má	mít	k5eAaImIp3nS	mít
podporu	podpora	k1gFnSc4	podpora
i	i	k9	i
některých	některý	k3yIgMnPc2	některý
umírněných	umírněný	k2eAgMnPc2d1	umírněný
sunnitů	sunnita	k1gMnPc2	sunnita
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
bojí	bát	k5eAaImIp3nS	bát
přílivu	příliv	k1gInSc3	příliv
džihádistů	džihádista	k1gMnPc2	džihádista
napojených	napojený	k2eAgMnPc2d1	napojený
na	na	k7c4	na
al-Kajdu	al-Kajda	k1gFnSc4	al-Kajda
<g/>
.	.	kIx.	.
</s>
<s>
Syrští	syrský	k2eAgMnPc1d1	syrský
křesťané	křesťan	k1gMnPc1	křesťan
vytvořili	vytvořit	k5eAaPmAgMnP	vytvořit
milice	milice	k1gFnPc4	milice
bojující	bojující	k2eAgFnPc1d1	bojující
proti	proti	k7c3	proti
povstalcům	povstalec	k1gMnPc3	povstalec
<g/>
.	.	kIx.	.
</s>
<s>
Odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
až	až	k9	až
třetina	třetina	k1gFnSc1	třetina
z	z	k7c2	z
250	[number]	k4	250
000	[number]	k4	000
alavitských	alavitský	k2eAgMnPc2d1	alavitský
mužů	muž	k1gMnPc2	muž
bojujících	bojující	k2eAgMnPc2d1	bojující
na	na	k7c6	na
straně	strana	k1gFnSc6	strana
vlády	vláda	k1gFnSc2	vláda
zahynula	zahynout	k5eAaPmAgFnS	zahynout
v	v	k7c6	v
občanské	občanský	k2eAgFnSc6d1	občanská
válce	válka	k1gFnSc6	válka
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
květnu	květen	k1gInSc6	květen
2013	[number]	k4	2013
podle	podle	k7c2	podle
jednoho	jeden	k4xCgInSc2	jeden
odhadu	odhad	k1gInSc2	odhad
z	z	k7c2	z
94	[number]	k4	94
000	[number]	k4	000
obětí	oběť	k1gFnPc2	oběť
války	válka	k1gFnSc2	válka
bylo	být	k5eAaImAgNnS	být
41	[number]	k4	41
000	[number]	k4	000
alavitů	alavit	k1gInPc2	alavit
<g/>
.	.	kIx.	.
<g/>
K	k	k7c3	k
nejvýznamnějším	významný	k2eAgMnPc3d3	nejvýznamnější
zahraničním	zahraniční	k2eAgMnPc3d1	zahraniční
podporovatelům	podporovatel	k1gMnPc3	podporovatel
režimu	režim	k1gInSc2	režim
patří	patřit	k5eAaImIp3nS	patřit
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
mu	on	k3xPp3gMnSc3	on
dodává	dodávat	k5eAaImIp3nS	dodávat
zbraně	zbraň	k1gFnPc4	zbraň
výměnou	výměna	k1gFnSc7	výměna
za	za	k7c4	za
peníze	peníz	k1gInPc4	peníz
a	a	k8xC	a
blokuje	blokovat	k5eAaImIp3nS	blokovat
všechny	všechen	k3xTgFnPc4	všechen
rezoluce	rezoluce	k1gFnPc4	rezoluce
namířené	namířený	k2eAgFnPc1d1	namířená
proti	proti	k7c3	proti
němu	on	k3xPp3gMnSc3	on
v	v	k7c6	v
radě	rada	k1gFnSc6	rada
bezpečnosti	bezpečnost	k1gFnSc2	bezpečnost
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
podzimu	podzim	k1gInSc2	podzim
2015	[number]	k4	2015
zasahuje	zasahovat	k5eAaImIp3nS	zasahovat
Rusko	Rusko	k1gNnSc4	Rusko
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
také	také	k9	také
nasazením	nasazení	k1gNnSc7	nasazení
svého	svůj	k3xOyFgNnSc2	svůj
vojenského	vojenský	k2eAgNnSc2d1	vojenské
letectva	letectvo	k1gNnSc2	letectvo
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
bombarduje	bombardovat	k5eAaImIp3nS	bombardovat
bojovníky	bojovník	k1gMnPc4	bojovník
Islámského	islámský	k2eAgInSc2d1	islámský
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
IS	IS	kA	IS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Fronty	fronta	k1gFnPc4	fronta
an-Nusrá	an-Nusrat	k5eAaPmIp3nS	an-Nusrat
a	a	k8xC	a
jiné	jiný	k2eAgMnPc4d1	jiný
povstalce	povstalec	k1gMnPc4	povstalec
<g/>
.	.	kIx.	.
</s>
<s>
Předseda	předseda	k1gMnSc1	předseda
vojenského	vojenský	k2eAgInSc2d1	vojenský
výboru	výbor	k1gInSc2	výbor
NATO	NATO	kA	NATO
generál	generál	k1gMnSc1	generál
Pavel	Pavel	k1gMnSc1	Pavel
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
na	na	k7c6	na
samém	samý	k3xTgInSc6	samý
začátku	začátek	k1gInSc6	začátek
vojenské	vojenský	k2eAgFnSc2d1	vojenská
akce	akce	k1gFnSc2	akce
Rusko	Rusko	k1gNnSc1	Rusko
útočilo	útočit	k5eAaImAgNnS	útočit
na	na	k7c6	na
ISIS	Isis	k1gFnSc1	Isis
jen	jen	k9	jen
ve	v	k7c6	v
třetině	třetina	k1gFnSc6	třetina
případů	případ	k1gInPc2	případ
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
britský	britský	k2eAgMnSc1d1	britský
ministr	ministr	k1gMnSc1	ministr
Philip	Philip	k1gMnSc1	Philip
Hammond	Hammond	k1gMnSc1	Hammond
<g/>
,	,	kIx,	,
humanitární	humanitární	k2eAgFnPc4d1	humanitární
organizace	organizace	k1gFnPc4	organizace
Amnesty	Amnest	k1gInPc1	Amnest
International	International	k1gFnSc2	International
a	a	k8xC	a
organizace	organizace	k1gFnSc2	organizace
Lékaři	lékař	k1gMnPc1	lékař
bez	bez	k7c2	bez
hranic	hranice	k1gFnPc2	hranice
obvinili	obvinit	k5eAaPmAgMnP	obvinit
Rusko	Rusko	k1gNnSc4	Rusko
z	z	k7c2	z
útoků	útok	k1gInPc2	útok
na	na	k7c4	na
civilisty	civilista	k1gMnPc4	civilista
<g/>
,	,	kIx,	,
humanitární	humanitární	k2eAgMnPc4d1	humanitární
pracovníky	pracovník	k1gMnPc4	pracovník
a	a	k8xC	a
informovali	informovat	k5eAaBmAgMnP	informovat
o	o	k7c6	o
bombardování	bombardování	k1gNnSc6	bombardování
několika	několik	k4yIc2	několik
nemocnic	nemocnice	k1gFnPc2	nemocnice
<g/>
.	.	kIx.	.
</s>
<s>
Bývalý	bývalý	k2eAgMnSc1d1	bývalý
syrský	syrský	k2eAgMnSc1d1	syrský
generál	generál	k1gMnSc1	generál
Ahmad	Ahmad	k1gInSc1	Ahmad
Rahal	Rahal	k1gInSc1	Rahal
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
pro	pro	k7c4	pro
dobrovolnickou	dobrovolnický	k2eAgFnSc4d1	dobrovolnická
iniciativu	iniciativa	k1gFnSc4	iniciativa
InformNapalm	InformNapalma	k1gFnPc2	InformNapalma
tvrdil	tvrdit	k5eAaImAgMnS	tvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
ruské	ruský	k2eAgFnPc1d1	ruská
intervence	intervence	k1gFnPc1	intervence
jsou	být	k5eAaImIp3nP	být
genocidou	genocida	k1gFnSc7	genocida
na	na	k7c6	na
syrském	syrský	k2eAgInSc6d1	syrský
národě	národ	k1gInSc6	národ
<g/>
.	.	kIx.	.
<g/>
Asada	Asado	k1gNnSc2	Asado
podporují	podporovat	k5eAaImIp3nP	podporovat
také	také	k9	také
v	v	k7c6	v
menším	malý	k2eAgNnSc6d2	menší
měřítku	měřítko	k1gNnSc6	měřítko
sousední	sousední	k2eAgInSc1d1	sousední
Irák	Irák	k1gInSc1	Irák
a	a	k8xC	a
hlavně	hlavně	k9	hlavně
Írán	Írán	k1gInSc1	Írán
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
mu	on	k3xPp3gNnSc3	on
dle	dle	k7c2	dle
některých	některý	k3yIgMnPc2	některý
zdrojů	zdroj	k1gInPc2	zdroj
dodává	dodávat	k5eAaImIp3nS	dodávat
zbraně	zbraň	k1gFnPc4	zbraň
a	a	k8xC	a
finančně	finančně	k6eAd1	finančně
jej	on	k3xPp3gMnSc4	on
dotuje	dotovat	k5eAaBmIp3nS	dotovat
<g/>
.	.	kIx.	.
</s>
<s>
Silné	silný	k2eAgFnPc1d1	silná
jednotky	jednotka	k1gFnPc1	jednotka
bojovníků	bojovník	k1gMnPc2	bojovník
zrekrutované	zrekrutovaný	k2eAgFnSc2d1	zrekrutovaný
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
Íránu	Írán	k1gInSc6	Írán
se	se	k3xPyFc4	se
nacházejí	nacházet	k5eAaImIp3nP	nacházet
přímo	přímo	k6eAd1	přímo
na	na	k7c6	na
území	území	k1gNnSc6	území
Sýrie	Sýrie	k1gFnSc2	Sýrie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
organizují	organizovat	k5eAaBmIp3nP	organizovat
místní	místní	k2eAgFnPc4d1	místní
milice	milice	k1gFnPc4	milice
a	a	k8xC	a
přímo	přímo	k6eAd1	přímo
bojují	bojovat	k5eAaImIp3nP	bojovat
s	s	k7c7	s
povstalci	povstalec	k1gMnPc7	povstalec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Opozice	opozice	k1gFnSc1	opozice
===	===	k?	===
</s>
</p>
<p>
<s>
Proti	proti	k7c3	proti
vládě	vláda	k1gFnSc3	vláda
stála	stát	k5eAaImAgFnS	stát
koalice	koalice	k1gFnSc1	koalice
syrské	syrský	k2eAgFnSc2d1	Syrská
opozice	opozice	k1gFnSc2	opozice
<g/>
,	,	kIx,	,
částečně	částečně	k6eAd1	částečně
reprezentovaná	reprezentovaný	k2eAgFnSc1d1	reprezentovaná
syrskou	syrský	k2eAgFnSc7d1	Syrská
exilovou	exilový	k2eAgFnSc7d1	exilová
vládou	vláda	k1gFnSc7	vláda
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
pořádala	pořádat	k5eAaImAgFnS	pořádat
vnitřní	vnitřní	k2eAgNnSc4d1	vnitřní
setkání	setkání	k1gNnSc4	setkání
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
i	i	k9	i
setkání	setkání	k1gNnSc1	setkání
se	s	k7c7	s
zástupci	zástupce	k1gMnPc7	zástupce
nezúčastněných	zúčastněný	k2eNgInPc2d1	nezúčastněný
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
sto	sto	k4xCgNnSc1	sto
států	stát	k1gInPc2	stát
světa	svět	k1gInSc2	svět
ji	on	k3xPp3gFnSc4	on
uznaly	uznat	k5eAaPmAgInP	uznat
za	za	k7c4	za
jediného	jediný	k2eAgMnSc4d1	jediný
legitimního	legitimní	k2eAgMnSc4d1	legitimní
reprezentanta	reprezentant	k1gMnSc4	reprezentant
syrského	syrský	k2eAgInSc2d1	syrský
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
konfliktu	konflikt	k1gInSc6	konflikt
je	být	k5eAaImIp3nS	být
opozice	opozice	k1gFnSc1	opozice
představovaná	představovaný	k2eAgFnSc1d1	představovaná
Národní	národní	k2eAgFnSc7d1	národní
koalicí	koalice	k1gFnSc7	koalice
sil	síla	k1gFnPc2	síla
revoluce	revoluce	k1gFnSc2	revoluce
a	a	k8xC	a
zastupována	zastupován	k2eAgFnSc1d1	zastupována
takzvanou	takzvaný	k2eAgFnSc7d1	takzvaná
Svobodnou	svobodný	k2eAgFnSc7d1	svobodná
syrskou	syrský	k2eAgFnSc7d1	Syrská
armádou	armáda	k1gFnSc7	armáda
<g/>
,	,	kIx,	,
<g/>
tvořenou	tvořený	k2eAgFnSc4d1	tvořená
35	[number]	k4	35
000	[number]	k4	000
až	až	k9	až
60	[number]	k4	60
000	[number]	k4	000
bojovníky	bojovník	k1gMnPc7	bojovník
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
tvořena	tvořit	k5eAaImNgFnS	tvořit
především	především	k6eAd1	především
zběhlými	zběhlý	k2eAgMnPc7d1	zběhlý
vojáky	voják	k1gMnPc7	voják
regulérní	regulérní	k2eAgFnSc2d1	regulérní
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
syrská	syrský	k2eAgFnSc1d1	Syrská
armáda	armáda	k1gFnSc1	armáda
byla	být	k5eAaImAgFnS	být
zpočátku	zpočátku	k6eAd1	zpočátku
největší	veliký	k2eAgFnSc1d3	veliký
vojenská	vojenský	k2eAgFnSc1d1	vojenská
síla	síla	k1gFnSc1	síla
opozice	opozice	k1gFnSc2	opozice
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
s	s	k7c7	s
přeběhem	přeběh	k1gInSc7	přeběh
jejich	jejich	k3xOp3gMnPc2	jejich
bojovníků	bojovník	k1gMnPc2	bojovník
do	do	k7c2	do
jiných	jiný	k2eAgFnPc2d1	jiná
islámských	islámský	k2eAgFnPc2d1	islámská
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
jasné	jasný	k2eAgNnSc1d1	jasné
<g/>
,	,	kIx,	,
že	že	k8xS	že
pouze	pouze	k6eAd1	pouze
spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
umírněnými	umírněný	k2eAgMnPc7d1	umírněný
islamisty	islamista	k1gMnPc7	islamista
by	by	kYmCp3nS	by
mohla	moct	k5eAaImAgNnP	moct
vytvořit	vytvořit	k5eAaPmF	vytvořit
dostatečně	dostatečně	k6eAd1	dostatečně
silnou	silný	k2eAgFnSc4d1	silná
koalici	koalice	k1gFnSc4	koalice
na	na	k7c4	na
boj	boj	k1gInSc4	boj
proti	proti	k7c3	proti
ostatním	ostatní	k2eAgFnPc3d1	ostatní
skupinám	skupina	k1gFnPc3	skupina
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Nejvýznamnějšími	významný	k2eAgMnPc7d3	nejvýznamnější
podporovateli	podporovatel	k1gMnPc7	podporovatel
opozice	opozice	k1gFnSc2	opozice
jsou	být	k5eAaImIp3nP	být
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
,	,	kIx,	,
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
a	a	k8xC	a
Katar	katar	k1gInSc1	katar
<g/>
.	.	kIx.	.
</s>
<s>
Dělí	dělit	k5eAaImIp3nS	dělit
svojí	svůj	k3xOyFgFnSc3	svůj
podporu	podpora	k1gFnSc4	podpora
mezi	mezi	k7c4	mezi
sekulární	sekulární	k2eAgFnSc4d1	sekulární
opozici	opozice	k1gFnSc4	opozice
a	a	k8xC	a
džihádisty	džihádista	k1gMnPc4	džihádista
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
Saúdská	saúdský	k2eAgFnSc1d1	Saúdská
Arábie	Arábie	k1gFnSc1	Arábie
<g/>
,	,	kIx,	,
Turecko	Turecko	k1gNnSc1	Turecko
a	a	k8xC	a
Katar	katar	k1gInSc1	katar
podporovaly	podporovat	k5eAaImAgInP	podporovat
zbraněmi	zbraň	k1gFnPc7	zbraň
<g/>
,	,	kIx,	,
penězi	peníze	k1gInPc7	peníze
a	a	k8xC	a
volnou	volný	k2eAgFnSc7d1	volná
cestou	cesta	k1gFnSc7	cesta
přes	přes	k7c4	přes
turecké	turecký	k2eAgNnSc4d1	turecké
území	území	k1gNnSc4	území
islamistické	islamistický	k2eAgNnSc4d1	islamistické
povstalce	povstalec	k1gMnSc2	povstalec
z	z	k7c2	z
fronty	fronta	k1gFnSc2	fronta
an-Nusrá	an-Nusrat	k5eAaPmIp3nS	an-Nusrat
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
napojená	napojený	k2eAgFnSc1d1	napojená
na	na	k7c4	na
teroristickou	teroristický	k2eAgFnSc4d1	teroristická
organizaci	organizace	k1gFnSc4	organizace
Al-Káida	Al-Káid	k1gMnSc2	Al-Káid
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc1	tento
státy	stát	k1gInPc1	stát
dodávají	dodávat	k5eAaImIp3nP	dodávat
opozici	opozice	k1gFnSc4	opozice
v	v	k7c6	v
masovém	masový	k2eAgNnSc6d1	masové
měřítku	měřítko	k1gNnSc6	měřítko
zbraně	zbraň	k1gFnSc2	zbraň
a	a	k8xC	a
nechávají	nechávat	k5eAaImIp3nP	nechávat
opoziční	opoziční	k2eAgFnPc1d1	opoziční
jednotky	jednotka	k1gFnPc1	jednotka
cvičit	cvičit	k5eAaImF	cvičit
na	na	k7c6	na
svém	svůj	k3xOyFgNnSc6	svůj
území	území	k1gNnSc6	území
či	či	k8xC	či
je	on	k3xPp3gNnSc4	on
dokonce	dokonce	k9	dokonce
cvičí	cvičit	k5eAaImIp3nP	cvičit
samy	sám	k3xTgInPc1	sám
<g/>
.	.	kIx.	.
</s>
<s>
Některé	některý	k3yIgMnPc4	některý
syrské	syrský	k2eAgMnPc4d1	syrský
povstalce	povstalec	k1gMnPc4	povstalec
vycvičila	vycvičit	k5eAaPmAgFnS	vycvičit
a	a	k8xC	a
vyzbrojila	vyzbrojit	k5eAaPmAgFnS	vyzbrojit
i	i	k9	i
americká	americký	k2eAgFnSc1d1	americká
CIA	CIA	kA	CIA
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
roku	rok	k1gInSc2	rok
2012	[number]	k4	2012
CIA	CIA	kA	CIA
investovala	investovat	k5eAaBmAgFnS	investovat
přibližně	přibližně	k6eAd1	přibližně
1	[number]	k4	1
miliardu	miliarda	k4xCgFnSc4	miliarda
dolarů	dolar	k1gInPc2	dolar
do	do	k7c2	do
výcviku	výcvik	k1gInSc2	výcvik
a	a	k8xC	a
výzbroje	výzbroj	k1gFnSc2	výzbroj
asi	asi	k9	asi
10	[number]	k4	10
000	[number]	k4	000
protivládních	protivládní	k2eAgMnPc2d1	protivládní
povstalců	povstalec	k1gMnPc2	povstalec
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
operace	operace	k1gFnSc2	operace
Timber	Timbra	k1gFnPc2	Timbra
Sycamore	Sycamor	k1gMnSc5	Sycamor
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
přeběhli	přeběhnout	k5eAaPmAgMnP	přeběhnout
k	k	k7c3	k
ISIL	ISIL	kA	ISIL
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dalším	další	k2eAgMnSc7d1	další
významným	významný	k2eAgMnSc7d1	významný
podporovatelem	podporovatel	k1gMnSc7	podporovatel
opozice	opozice	k1gFnSc2	opozice
je	být	k5eAaImIp3nS	být
Libye	Libye	k1gFnSc1	Libye
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
míří	mířit	k5eAaImIp3nS	mířit
k	k	k7c3	k
opozicí	opozice	k1gFnSc7	opozice
ovládaným	ovládaný	k2eAgInPc3d1	ovládaný
přístavům	přístav	k1gInPc3	přístav
lodě	loď	k1gFnSc2	loď
se	s	k7c7	s
zbraněmi	zbraň	k1gFnPc7	zbraň
a	a	k8xC	a
municí	munice	k1gFnSc7	munice
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
účetnictví	účetnictví	k1gNnSc2	účetnictví
Syrské	syrský	k2eAgFnSc2d1	Syrská
národní	národní	k2eAgFnSc2d1	národní
rady	rada	k1gFnSc2	rada
pochází	pocházet	k5eAaImIp3nS	pocházet
přes	přes	k7c4	přes
50	[number]	k4	50
%	%	kIx~	%
jejích	její	k3xOp3gInPc2	její
zdrojů	zdroj	k1gInPc2	zdroj
z	z	k7c2	z
libyjských	libyjský	k2eAgInPc2d1	libyjský
fondů	fond	k1gInPc2	fond
<g/>
,	,	kIx,	,
35	[number]	k4	35
%	%	kIx~	%
poté	poté	k6eAd1	poté
z	z	k7c2	z
Kataru	katar	k1gInSc2	katar
a	a	k8xC	a
zbylých	zbylý	k2eAgInPc2d1	zbylý
více	hodně	k6eAd2	hodně
než	než	k8xS	než
10	[number]	k4	10
%	%	kIx~	%
ze	z	k7c2	z
Spojených	spojený	k2eAgInPc2d1	spojený
arabských	arabský	k2eAgInPc2d1	arabský
emirátů	emirát	k1gInPc2	emirát
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
některých	některý	k3yIgFnPc2	některý
teorií	teorie	k1gFnPc2	teorie
se	se	k3xPyFc4	se
pod	pod	k7c7	pod
hlavičkou	hlavička	k1gFnSc7	hlavička
Libye	Libye	k1gFnSc2	Libye
schovává	schovávat	k5eAaImIp3nS	schovávat
nepřiznaná	přiznaný	k2eNgFnSc1d1	nepřiznaná
podpora	podpora	k1gFnSc1	podpora
z	z	k7c2	z
USA	USA	kA	USA
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Poukazují	poukazovat	k5eAaImIp3nP	poukazovat
přitom	přitom	k6eAd1	přitom
na	na	k7c4	na
podezřelé	podezřelý	k2eAgFnPc4d1	podezřelá
aktivity	aktivita	k1gFnPc4	aktivita
amerického	americký	k2eAgNnSc2d1	americké
velvyslanectví	velvyslanectví	k1gNnSc2	velvyslanectví
v	v	k7c6	v
Libyi	Libye	k1gFnSc6	Libye
či	či	k8xC	či
některých	některý	k3yIgFnPc2	některý
poradenských	poradenský	k2eAgFnPc2d1	poradenská
bezpečnostních	bezpečnostní	k2eAgFnPc2d1	bezpečnostní
společností	společnost	k1gFnPc2	společnost
pocházejících	pocházející	k2eAgFnPc2d1	pocházející
ze	z	k7c2	z
Spojených	spojený	k2eAgInPc2d1	spojený
států	stát	k1gInPc2	stát
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Podle	podle	k7c2	podle
uniklých	uniklý	k2eAgInPc2d1	uniklý
rozhovorů	rozhovor	k1gInPc2	rozhovor
egyptského	egyptský	k2eAgMnSc2d1	egyptský
prezidenta	prezident	k1gMnSc2	prezident
Mursího	Mursí	k2eAgMnSc2d1	Mursí
s	s	k7c7	s
katarským	katarský	k2eAgMnSc7d1	katarský
premiérem	premiér	k1gMnSc7	premiér
Al	ala	k1gFnPc2	ala
Táním	tání	k1gNnPc3	tání
podporuje	podporovat	k5eAaImIp3nS	podporovat
Egypt	Egypt	k1gInSc1	Egypt
po	po	k7c6	po
boku	bok	k1gInSc6	bok
Kataru	katar	k1gInSc2	katar
syrskou	syrský	k2eAgFnSc4d1	Syrská
opozici	opozice	k1gFnSc4	opozice
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
motivace	motivace	k1gFnSc1	motivace
podpory	podpora	k1gFnSc2	podpora
Turecka	Turecko	k1gNnSc2	Turecko
<g/>
,	,	kIx,	,
Saúdské	saúdský	k2eAgFnSc2d1	Saúdská
Arábie	Arábie	k1gFnSc2	Arábie
a	a	k8xC	a
Kataru	katar	k1gInSc2	katar
bývá	bývat	k5eAaImIp3nS	bývat
uváděna	uvádět	k5eAaImNgFnS	uvádět
snaha	snaha	k1gFnSc1	snaha
vyšachovat	vyšachovat	k5eAaPmF	vyšachovat
Írán	Írán	k1gInSc4	Írán
z	z	k7c2	z
pozice	pozice	k1gFnSc2	pozice
jedné	jeden	k4xCgFnSc2	jeden
z	z	k7c2	z
regionálních	regionální	k2eAgFnPc2d1	regionální
velmocí	velmoc	k1gFnPc2	velmoc
likvidací	likvidace	k1gFnPc2	likvidace
jediného	jediný	k2eAgInSc2d1	jediný
spojeneckého	spojenecký	k2eAgInSc2d1	spojenecký
režimu	režim	k1gInSc2	režim
a	a	k8xC	a
odstřižením	odstřižení	k1gNnSc7	odstřižení
od	od	k7c2	od
jím	jíst	k5eAaImIp1nS	jíst
organizovaného	organizovaný	k2eAgInSc2d1	organizovaný
Hizzballáhu	Hizzballáh	k1gInSc2	Hizzballáh
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Přímá	přímý	k2eAgFnSc1d1	přímá
podpora	podpora	k1gFnSc1	podpora
povstalců	povstalec	k1gMnPc2	povstalec
se	se	k3xPyFc4	se
na	na	k7c4	na
návrh	návrh	k1gInSc4	návrh
Francie	Francie	k1gFnSc2	Francie
a	a	k8xC	a
Spojeného	spojený	k2eAgNnSc2d1	spojené
království	království	k1gNnSc2	království
řešila	řešit	k5eAaImAgFnS	řešit
také	také	k9	také
na	na	k7c6	na
úrovni	úroveň	k1gFnSc6	úroveň
Evropské	evropský	k2eAgFnSc2d1	Evropská
unie	unie	k1gFnSc2	unie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
odpor	odpor	k1gInSc4	odpor
Německa	Německo	k1gNnSc2	Německo
a	a	k8xC	a
některých	některý	k3yIgFnPc2	některý
další	další	k2eAgInPc1d1	další
států	stát	k1gInPc2	stát
včetně	včetně	k7c2	včetně
Česka	Česko	k1gNnSc2	Česko
zamítnuta	zamítnut	k2eAgFnSc1d1	zamítnuta
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Fronta	fronta	k1gFnSc1	fronta
an-Nusrá	an-Nusrý	k2eAgFnSc1d1	an-Nusrý
===	===	k?	===
</s>
</p>
<p>
<s>
Džihádisty	Džihádista	k1gMnPc4	Džihádista
zastupuje	zastupovat	k5eAaImIp3nS	zastupovat
Fronta	fronta	k1gFnSc1	fronta
an-Nusrá	an-Nusrý	k2eAgFnSc1d1	an-Nusrý
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
sunnitská	sunnitský	k2eAgFnSc1d1	sunnitská
místní	místní	k2eAgFnSc1d1	místní
pobočka	pobočka	k1gFnSc1	pobočka
teroristické	teroristický	k2eAgFnSc2d1	teroristická
organizace	organizace	k1gFnSc2	organizace
Al-Káida	Al-Káida	k1gFnSc1	Al-Káida
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
osoby	osoba	k1gFnPc4	osoba
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
mají	mít	k5eAaImIp3nP	mít
mnohaleté	mnohaletý	k2eAgFnPc4d1	mnohaletá
zkušenosti	zkušenost	k1gFnPc4	zkušenost
z	z	k7c2	z
válek	válka	k1gFnPc2	válka
v	v	k7c6	v
Afghánistánu	Afghánistán	k1gInSc6	Afghánistán
<g/>
,	,	kIx,	,
Iráku	Irák	k1gInSc6	Irák
<g/>
,	,	kIx,	,
Alžírsku	Alžírsko	k1gNnSc6	Alžírsko
<g/>
,	,	kIx,	,
Jemenu	Jemen	k1gInSc6	Jemen
či	či	k8xC	či
Bosně	Bosna	k1gFnSc6	Bosna
<g/>
.	.	kIx.	.
</s>
<s>
Fronta	fronta	k1gFnSc1	fronta
an-Nusrá	an-Nusrý	k2eAgFnSc1d1	an-Nusrý
je	být	k5eAaImIp3nS	být
podezřelá	podezřelý	k2eAgFnSc1d1	podezřelá
z	z	k7c2	z
organizování	organizování	k1gNnSc2	organizování
teroristických	teroristický	k2eAgInPc2d1	teroristický
útoků	útok	k1gInPc2	útok
a	a	k8xC	a
válečných	válečný	k2eAgInPc2d1	válečný
zločinů	zločin	k1gInPc2	zločin
<g/>
,	,	kIx,	,
jako	jako	k8xC	jako
například	například	k6eAd1	například
mučení	mučení	k1gNnSc1	mučení
<g/>
,	,	kIx,	,
vraždění	vraždění	k1gNnSc1	vraždění
zajatců	zajatec	k1gMnPc2	zajatec
či	či	k8xC	či
znásilňování	znásilňování	k1gNnSc1	znásilňování
<g/>
.	.	kIx.	.
</s>
<s>
Kritizována	kritizován	k2eAgFnSc1d1	kritizována
je	být	k5eAaImIp3nS	být
také	také	k9	také
za	za	k7c4	za
perzekuci	perzekuce	k1gFnSc4	perzekuce
osob	osoba	k1gFnPc2	osoba
na	na	k7c6	na
základě	základ	k1gInSc6	základ
náboženského	náboženský	k2eAgNnSc2d1	náboženské
vyznání	vyznání	k1gNnSc2	vyznání
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInPc1	její
vztahy	vztah	k1gInPc1	vztah
se	s	k7c7	s
Svobodnou	svobodný	k2eAgFnSc7d1	svobodná
syrskou	syrský	k2eAgFnSc7d1	Syrská
armádou	armáda	k1gFnSc7	armáda
jsou	být	k5eAaImIp3nP	být
napjaté	napjatý	k2eAgFnPc1d1	napjatá
<g/>
.	.	kIx.	.
</s>
<s>
Džaíš	Džait	k5eAaPmIp2nS	Džait
al-Fatah	al-Fatah	k1gInSc1	al-Fatah
je	být	k5eAaImIp3nS	být
sunnitská	sunnitský	k2eAgFnSc1d1	sunnitská
koalice	koalice	k1gFnSc1	koalice
složená	složený	k2eAgFnSc1d1	složená
především	především	k9	především
z	z	k7c2	z
bojovníků	bojovník	k1gMnPc2	bojovník
an-Nusrá	an-Nusrat	k5eAaPmIp3nS	an-Nusrat
<g/>
,	,	kIx,	,
skupiny	skupina	k1gFnSc2	skupina
Ahrar	Ahrara	k1gFnPc2	Ahrara
aš-Šáh	aš-Šáha	k1gFnPc2	aš-Šáha
a	a	k8xC	a
dalších	další	k2eAgFnPc2d1	další
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
koalice	koalice	k1gFnSc1	koalice
je	být	k5eAaImIp3nS	být
aktivní	aktivní	k2eAgFnSc1d1	aktivní
hlavně	hlavně	k9	hlavně
v	v	k7c6	v
guvernorátu	guvernorát	k1gInSc6	guvernorát
Idlíb	Idlíba	k1gFnPc2	Idlíba
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc2	jeho
okolí	okolí	k1gNnSc2	okolí
a	a	k8xC	a
od	od	k7c2	od
března	březen	k1gInSc2	březen
2015	[number]	k4	2015
zaznamenala	zaznamenat	k5eAaPmAgFnS	zaznamenat
několik	několik	k4yIc4	několik
cenných	cenný	k2eAgInPc2d1	cenný
vítězství	vítězství	k1gNnSc4	vítězství
proti	proti	k7c3	proti
Syrské	syrský	k2eAgFnSc3d1	Syrská
armádě	armáda	k1gFnSc3	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Část	část	k1gFnSc1	část
bojovníků	bojovník	k1gMnPc2	bojovník
z	z	k7c2	z
an-Nusrá	an-Nusrý	k2eAgFnSc1d1	an-Nusrý
přešla	přejít	k5eAaPmAgFnS	přejít
také	také	k9	také
k	k	k7c3	k
ISIL	ISIL	kA	ISIL
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
níže	nízce	k6eAd2	nízce
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Islámský	islámský	k2eAgInSc1d1	islámský
stát	stát	k1gInSc1	stát
(	(	kIx(	(
<g/>
IS	IS	kA	IS
<g/>
)	)	kIx)	)
===	===	k?	===
</s>
</p>
<p>
<s>
Islámský	islámský	k2eAgInSc1d1	islámský
stát	stát	k1gInSc1	stát
byl	být	k5eAaImAgInS	být
pod	pod	k7c7	pod
zkratkou	zkratka	k1gFnSc7	zkratka
ISIL	ISIL	kA	ISIL
původně	původně	k6eAd1	původně
iráckou	irácký	k2eAgFnSc7d1	irácká
pobočkou	pobočka	k1gFnSc7	pobočka
organizace	organizace	k1gFnSc2	organizace
Al-Káida	Al-Káid	k1gMnSc2	Al-Káid
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
však	však	k9	však
začal	začít	k5eAaPmAgInS	začít
pronikat	pronikat	k5eAaImF	pronikat
i	i	k9	i
do	do	k7c2	do
Sýrie	Sýrie	k1gFnSc2	Sýrie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2014	[number]	k4	2014
už	už	k9	už
v	v	k7c4	v
podvečer	podvečer	k1gInSc4	podvečer
SELČ	SELČ	kA	SELČ
americké	americký	k2eAgNnSc1d1	americké
velitelství	velitelství	k1gNnSc1	velitelství
oznámilo	oznámit	k5eAaPmAgNnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
provedlo	provést	k5eAaPmAgNnS	provést
pět	pět	k4xCc1	pět
útoků	útok	k1gInPc2	útok
proti	proti	k7c3	proti
teroristům	terorista	k1gMnPc3	terorista
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
a	a	k8xC	a
Sýrii	Sýrie	k1gFnSc3	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
Terčem	terč	k1gInSc7	terč
dvou	dva	k4xCgInPc2	dva
náletů	nálet	k1gInPc2	nálet
byla	být	k5eAaImAgFnS	být
oblast	oblast	k1gFnSc1	oblast
jihovýchodně	jihovýchodně	k6eAd1	jihovýchodně
od	od	k7c2	od
Irbílu	Irbíl	k1gInSc2	Irbíl
v	v	k7c6	v
iráckém	irácký	k2eAgInSc6d1	irácký
Kurdistánu	Kurdistán	k1gInSc6	Kurdistán
<g/>
,	,	kIx,	,
další	další	k2eAgInPc4d1	další
dva	dva	k4xCgInPc4	dva
útoky	útok	k1gInPc4	útok
směřovaly	směřovat	k5eAaImAgFnP	směřovat
západně	západně	k6eAd1	západně
od	od	k7c2	od
Bagdádu	Bagdád	k1gInSc2	Bagdád
a	a	k8xC	a
jeden	jeden	k4xCgMnSc1	jeden
na	na	k7c4	na
pozice	pozice	k1gFnPc4	pozice
islamistů	islamista	k1gMnPc2	islamista
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
se	se	k3xPyFc4	se
zapojily	zapojit	k5eAaPmAgInP	zapojit
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
<g/>
,	,	kIx,	,
Spojené	spojený	k2eAgNnSc1d1	spojené
království	království	k1gNnSc1	království
<g/>
,	,	kIx,	,
Francie	Francie	k1gFnSc1	Francie
a	a	k8xC	a
Nizozemsko	Nizozemsko	k1gNnSc4	Nizozemsko
zahájením	zahájení	k1gNnSc7	zahájení
leteckých	letecký	k2eAgFnPc2d1	letecká
operací	operace	k1gFnPc2	operace
proti	proti	k7c3	proti
teroristickým	teroristický	k2eAgFnPc3d1	teroristická
skupinám	skupina	k1gFnPc3	skupina
Islámského	islámský	k2eAgInSc2d1	islámský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Přitom	přitom	k6eAd1	přitom
použily	použít	k5eAaPmAgFnP	použít
stíhačky	stíhačka	k1gFnPc1	stíhačka
<g/>
,	,	kIx,	,
bombardéry	bombardér	k1gInPc1	bombardér
a	a	k8xC	a
útočné	útočný	k2eAgFnPc1d1	útočná
rakety	raketa	k1gFnPc1	raketa
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
nutnosti	nutnost	k1gFnSc6	nutnost
úderů	úder	k1gInPc2	úder
proti	proti	k7c3	proti
Islámskému	islámský	k2eAgInSc3d1	islámský
státu	stát	k1gInSc3	stát
hovořil	hovořit	k5eAaImAgMnS	hovořit
francouzský	francouzský	k2eAgMnSc1d1	francouzský
prezident	prezident	k1gMnSc1	prezident
François	François	k1gFnSc2	François
Hollande	Holland	k1gInSc5	Holland
už	už	k6eAd1	už
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
září	zářit	k5eAaImIp3nS	zářit
2014	[number]	k4	2014
<g/>
.	.	kIx.	.
</s>
<s>
Francouzské	francouzský	k2eAgFnPc1d1	francouzská
stíhačky	stíhačka	k1gFnPc1	stíhačka
typu	typ	k1gInSc2	typ
Rafale	Rafala	k1gFnSc3	Rafala
poté	poté	k6eAd1	poté
zasáhly	zasáhnout	k5eAaPmAgFnP	zasáhnout
výcvikový	výcvikový	k2eAgInSc4d1	výcvikový
tábor	tábor	k1gInSc4	tábor
Islámského	islámský	k2eAgInSc2d1	islámský
státu	stát	k1gInSc2	stát
poblíž	poblíž	k7c2	poblíž
města	město	k1gNnSc2	město
Deir	Deir	k1gMnSc1	Deir
es-Zur	es-Zur	k1gMnSc1	es-Zur
u	u	k7c2	u
tureckých	turecký	k2eAgFnPc2d1	turecká
hranic	hranice	k1gFnPc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
město	město	k1gNnSc1	město
i	i	k9	i
v	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
kontrolují	kontrolovat	k5eAaImIp3nP	kontrolovat
vojenské	vojenský	k2eAgFnPc1d1	vojenská
jednotky	jednotka	k1gFnPc1	jednotka
syrské	syrský	k2eAgFnSc2d1	Syrská
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
okolní	okolní	k2eAgFnPc1d1	okolní
vesnice	vesnice	k1gFnPc1	vesnice
byly	být	k5eAaImAgFnP	být
tehdy	tehdy	k6eAd1	tehdy
obsazeny	obsazen	k2eAgMnPc4d1	obsazen
bojovníky	bojovník	k1gMnPc4	bojovník
Islámského	islámský	k2eAgInSc2d1	islámský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
====	====	k?	====
Údajná	údajný	k2eAgFnSc1d1	údajná
spolupráce	spolupráce	k1gFnSc1	spolupráce
s	s	k7c7	s
IS	IS	kA	IS
====	====	k?	====
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2014	[number]	k4	2014
jedna	jeden	k4xCgFnSc1	jeden
z	z	k7c2	z
největších	veliký	k2eAgFnPc2d3	veliký
protivládních	protivládní	k2eAgFnPc2d1	protivládní
sil	síla	k1gFnPc2	síla
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
<g/>
,	,	kIx,	,
takzvaná	takzvaný	k2eAgFnSc1d1	takzvaná
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
syrská	syrský	k2eAgFnSc1d1	Syrská
armáda	armáda	k1gFnSc1	armáda
<g/>
,	,	kIx,	,
uzavřela	uzavřít	k5eAaPmAgFnS	uzavřít
na	na	k7c6	na
bojišti	bojiště	k1gNnSc6	bojiště
na	na	k7c4	na
předměstí	předměstí	k1gNnSc4	předměstí
Damašku	Damašek	k1gInSc2	Damašek
dočasné	dočasný	k2eAgNnSc4d1	dočasné
příměří	příměří	k1gNnSc4	příměří
s	s	k7c7	s
ISIL	ISIL	kA	ISIL
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
společného	společný	k2eAgInSc2d1	společný
boje	boj	k1gInSc2	boj
proti	proti	k7c3	proti
syrské	syrský	k2eAgFnSc3d1	Syrská
vládě	vláda	k1gFnSc3	vláda
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
Islámskému	islámský	k2eAgInSc3d1	islámský
státu	stát	k1gInSc3	stát
přeběhlo	přeběhnout	k5eAaPmAgNnS	přeběhnout
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gFnSc2	jeho
vysoce	vysoce	k6eAd1	vysoce
postaveného	postavený	k2eAgMnSc2d1	postavený
velitele	velitel	k1gMnSc2	velitel
mnoho	mnoho	k6eAd1	mnoho
bojovníků	bojovník	k1gMnPc2	bojovník
Svobodné	svobodný	k2eAgFnSc2d1	svobodná
syrské	syrský	k2eAgFnSc2d1	Syrská
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
někteří	některý	k3yIgMnPc1	některý
bezpečnostní	bezpečnostní	k2eAgMnPc1d1	bezpečnostní
pracovníci	pracovník	k1gMnPc1	pracovník
USA	USA	kA	USA
potvrdili	potvrdit	k5eAaPmAgMnP	potvrdit
<g/>
,	,	kIx,	,
že	že	k8xS	že
mnoho	mnoho	k4c1	mnoho
z	z	k7c2	z
těchto	tento	k3xDgMnPc2	tento
bojovníků	bojovník	k1gMnPc2	bojovník
bylo	být	k5eAaImAgNnS	být
cvičeno	cvičit	k5eAaImNgNnS	cvičit
<g/>
,	,	kIx,	,
financováno	financovat	k5eAaBmNgNnS	financovat
a	a	k8xC	a
podporováno	podporován	k2eAgNnSc1d1	podporováno
z	z	k7c2	z
USA	USA	kA	USA
s	s	k7c7	s
vědomím	vědomí	k1gNnSc7	vědomí
bývalého	bývalý	k2eAgMnSc2d1	bývalý
prezidenta	prezident	k1gMnSc2	prezident
Baracka	Baracko	k1gNnSc2	Baracko
Obamy	Obama	k1gFnSc2	Obama
<g/>
.	.	kIx.	.
<g/>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
neověřených	ověřený	k2eNgFnPc2d1	neověřená
informací	informace	k1gFnPc2	informace
viní	vinit	k5eAaImIp3nP	vinit
někteří	některý	k3yIgMnPc1	některý
novináři	novinář	k1gMnPc1	novinář
režim	režim	k1gInSc4	režim
prezidenta	prezident	k1gMnSc2	prezident
Asada	Asad	k1gMnSc2	Asad
ze	z	k7c2	z
spolupráce	spolupráce	k1gFnSc2	spolupráce
s	s	k7c7	s
Islámským	islámský	k2eAgInSc7d1	islámský
státem	stát	k1gInSc7	stát
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
těchto	tento	k3xDgFnPc2	tento
informací	informace	k1gFnPc2	informace
začal	začít	k5eAaPmAgInS	začít
Asad	Asad	k1gInSc1	Asad
před	před	k7c7	před
vypuknutím	vypuknutí	k1gNnSc7	vypuknutí
občanské	občanský	k2eAgFnSc2d1	občanská
války	válka	k1gFnSc2	válka
údajně	údajně	k6eAd1	údajně
spolupracovat	spolupracovat	k5eAaImF	spolupracovat
se	s	k7c7	s
zástupci	zástupce	k1gMnPc7	zástupce
Islámského	islámský	k2eAgInSc2d1	islámský
státu	stát	k1gInSc2	stát
proti	proti	k7c3	proti
syrské	syrský	k2eAgFnSc3d1	Syrská
opozici	opozice	k1gFnSc3	opozice
a	a	k8xC	a
když	když	k8xS	když
IS	IS	kA	IS
získal	získat	k5eAaPmAgInS	získat
na	na	k7c6	na
síle	síla	k1gFnSc6	síla
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
svých	svůj	k3xOyFgFnPc2	svůj
mocenských	mocenský	k2eAgFnPc2d1	mocenská
snah	snaha	k1gFnPc2	snaha
obrátil	obrátit	k5eAaPmAgInS	obrátit
proti	proti	k7c3	proti
Asadovu	Asadův	k2eAgInSc3d1	Asadův
režimu	režim	k1gInSc3	režim
<g/>
.	.	kIx.	.
</s>
<s>
Velká	velký	k2eAgFnSc1d1	velká
část	část	k1gFnSc1	část
bojovníků	bojovník	k1gMnPc2	bojovník
Islámského	islámský	k2eAgInSc2d1	islámský
státu	stát	k1gInSc2	stát
pochází	pocházet	k5eAaImIp3nS	pocházet
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
členů	člen	k1gMnPc2	člen
armády	armáda	k1gFnSc2	armáda
iráckého	irácký	k2eAgMnSc4d1	irácký
diktátora	diktátor	k1gMnSc4	diktátor
Saddáma	Saddám	k1gMnSc4	Saddám
Husajna	Husajn	k1gMnSc4	Husajn
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
Islámský	islámský	k2eAgInSc1d1	islámský
stát	stát	k1gInSc1	stát
podporují	podporovat	k5eAaImIp3nP	podporovat
Státy	stát	k1gInPc1	stát
Perského	perský	k2eAgInSc2d1	perský
zálivu	záliv	k1gInSc2	záliv
a	a	k8xC	a
některé	některý	k3yIgInPc4	některý
západní	západní	k2eAgInPc4d1	západní
státy	stát	k1gInPc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Někteří	některý	k3yIgMnPc1	některý
komentátoři	komentátor	k1gMnPc1	komentátor
a	a	k8xC	a
politici	politik	k1gMnPc1	politik
viní	vinit	k5eAaImIp3nP	vinit
z	z	k7c2	z
podpory	podpora	k1gFnSc2	podpora
Islámského	islámský	k2eAgInSc2d1	islámský
státu	stát	k1gInSc2	stát
Saúdskou	saúdský	k2eAgFnSc4d1	Saúdská
Arábii	Arábie	k1gFnSc4	Arábie
<g/>
,	,	kIx,	,
Katar	katar	k1gInSc4	katar
a	a	k8xC	a
Turecko	Turecko	k1gNnSc4	Turecko
<g/>
.	.	kIx.	.
</s>
<s>
Saúdský	saúdský	k2eAgMnSc1d1	saúdský
král	král	k1gMnSc1	král
Salmán	Salmán	k2eAgMnSc1d1	Salmán
naopak	naopak	k6eAd1	naopak
obvinil	obvinit	k5eAaPmAgMnS	obvinit
ze	z	k7c2	z
spolupráce	spolupráce	k1gFnSc2	spolupráce
s	s	k7c7	s
Islámským	islámský	k2eAgInSc7d1	islámský
státem	stát	k1gInSc7	stát
vládu	vláda	k1gFnSc4	vláda
prezidenta	prezident	k1gMnSc2	prezident
Asada	Asad	k1gMnSc2	Asad
<g/>
.	.	kIx.	.
<g/>
Britská	britský	k2eAgFnSc1d1	britská
televizní	televizní	k2eAgFnSc1d1	televizní
stanice	stanice	k1gFnSc1	stanice
Sky	Sky	k1gMnPc2	Sky
News	Newsa	k1gFnPc2	Newsa
informovala	informovat	k5eAaBmAgFnS	informovat
o	o	k7c6	o
pozdější	pozdní	k2eAgFnSc6d2	pozdější
omezené	omezený	k2eAgFnSc6d1	omezená
spolupráci	spolupráce	k1gFnSc6	spolupráce
mezi	mezi	k7c7	mezi
syrskou	syrský	k2eAgFnSc7d1	Syrská
vládou	vláda	k1gFnSc7	vláda
a	a	k8xC	a
islamisty	islamista	k1gMnPc7	islamista
<g/>
.	.	kIx.	.
</s>
<s>
Mělo	mít	k5eAaImAgNnS	mít
se	se	k3xPyFc4	se
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
předání	předání	k1gNnSc4	předání
vlády	vláda	k1gFnSc2	vláda
nad	nad	k7c7	nad
městem	město	k1gNnSc7	město
Palmýra	Palmýra	k1gFnSc1	Palmýra
nebo	nebo	k8xC	nebo
o	o	k7c4	o
obchody	obchod	k1gInPc4	obchod
-	-	kIx~	-
například	například	k6eAd1	například
o	o	k7c4	o
výměnu	výměna	k1gFnSc4	výměna
ropy	ropa	k1gFnSc2	ropa
za	za	k7c4	za
hnojiva	hnojivo	k1gNnPc4	hnojivo
<g/>
.	.	kIx.	.
</s>
<s>
Mají	mít	k5eAaImIp3nP	mít
to	ten	k3xDgNnSc1	ten
alespoň	alespoň	k9	alespoň
dokazovat	dokazovat	k5eAaImF	dokazovat
dokumenty	dokument	k1gInPc4	dokument
<g/>
,	,	kIx,	,
k	k	k7c3	k
nimž	jenž	k3xRgFnPc3	jenž
se	se	k3xPyFc4	se
televize	televize	k1gFnSc1	televize
Sky	Sky	k1gMnPc2	Sky
News	Newsa	k1gFnPc2	Newsa
dostala	dostat	k5eAaPmAgFnS	dostat
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
dezertérů	dezertér	k1gMnPc2	dezertér
IS	IS	kA	IS
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
jí	on	k3xPp3gFnSc3	on
prý	prý	k9	prý
dříve	dříve	k6eAd2	dříve
poskytli	poskytnout	k5eAaPmAgMnP	poskytnout
pravdivá	pravdivý	k2eAgNnPc4d1	pravdivé
fakta	faktum	k1gNnPc4	faktum
<g/>
.	.	kIx.	.
</s>
<s>
Stanice	stanice	k1gFnSc1	stanice
však	však	k9	však
upozornila	upozornit	k5eAaPmAgFnS	upozornit
<g/>
,	,	kIx,	,
že	že	k8xS	že
věrohodnost	věrohodnost	k1gFnSc4	věrohodnost
dokumentů	dokument	k1gInPc2	dokument
nelze	lze	k6eNd1	lze
nijak	nijak	k6eAd1	nijak
ověřit	ověřit	k5eAaPmF	ověřit
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
obchodních	obchodní	k2eAgInPc2d1	obchodní
styků	styk	k1gInPc2	styk
s	s	k7c7	s
IS	IS	kA	IS
bylo	být	k5eAaImAgNnS	být
obviněno	obvinit	k5eAaPmNgNnS	obvinit
také	také	k9	také
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
.	.	kIx.	.
<g/>
Týdeník	týdeník	k1gInSc1	týdeník
Reflex	reflex	k1gInSc1	reflex
zveřejnil	zveřejnit	k5eAaPmAgInS	zveřejnit
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2016	[number]	k4	2016
informaci	informace	k1gFnSc4	informace
<g/>
,	,	kIx,	,
že	že	k8xS	že
došlo	dojít	k5eAaPmAgNnS	dojít
k	k	k7c3	k
uzavření	uzavření	k1gNnSc3	uzavření
strategické	strategický	k2eAgFnSc2d1	strategická
dohody	dohoda	k1gFnSc2	dohoda
mezi	mezi	k7c7	mezi
syrským	syrský	k2eAgInSc7d1	syrský
režimem	režim	k1gInSc7	režim
Bašára	Bašár	k1gMnSc2	Bašár
Asáda	Asád	k1gMnSc2	Asád
<g/>
,	,	kIx,	,
Islámským	islámský	k2eAgInSc7d1	islámský
státem	stát	k1gInSc7	stát
a	a	k8xC	a
Ruskou	ruský	k2eAgFnSc7d1	ruská
inženýrskou	inženýrský	k2eAgFnSc7d1	inženýrská
stavební	stavební	k2eAgFnSc7d1	stavební
firmou	firma	k1gFnSc7	firma
Strojtransgaz	Strojtransgaz	k1gInSc1	Strojtransgaz
<g/>
.	.	kIx.	.
</s>
<s>
Dohoda	dohoda	k1gFnSc1	dohoda
umožnila	umožnit	k5eAaPmAgFnS	umožnit
Islámskému	islámský	k2eAgInSc3d1	islámský
státu	stát	k1gInSc3	stát
těžbu	těžba	k1gFnSc4	těžba
ropy	ropa	k1gFnSc2	ropa
a	a	k8xC	a
zemního	zemní	k2eAgInSc2d1	zemní
plynu	plyn	k1gInSc2	plyn
v	v	k7c6	v
jím	on	k3xPp3gMnSc7	on
od	od	k7c2	od
ledna	leden	k1gInSc2	leden
2014	[number]	k4	2014
ovládané	ovládaný	k2eAgFnSc2d1	ovládaná
oblasti	oblast	k1gFnSc2	oblast
města	město	k1gNnSc2	město
Rakka	Rakko	k1gNnSc2	Rakko
<g/>
.	.	kIx.	.
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
firma	firma	k1gFnSc1	firma
Strojtransgaz	Strojtransgaz	k1gInSc4	Strojtransgaz
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k9	co
oblast	oblast	k1gFnSc4	oblast
ovládl	ovládnout	k5eAaPmAgInS	ovládnout
Islámský	islámský	k2eAgInSc1d1	islámský
stát	stát	k1gInSc1	stát
<g/>
,	,	kIx,	,
obnovila	obnovit	k5eAaPmAgFnS	obnovit
stavební	stavební	k2eAgFnPc4d1	stavební
práce	práce	k1gFnPc4	práce
<g/>
,	,	kIx,	,
přerušené	přerušený	k2eAgFnPc4d1	přerušená
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2012	[number]	k4	2012
<g/>
,	,	kIx,	,
když	když	k8xS	když
nad	nad	k7c7	nad
regionem	region	k1gInSc7	region
i	i	k8xC	i
těžebním	těžební	k2eAgInSc7d1	těžební
komplexem	komplex	k1gInSc7	komplex
převzala	převzít	k5eAaPmAgFnS	převzít
kontrolu	kontrola	k1gFnSc4	kontrola
koalice	koalice	k1gFnSc1	koalice
rebelů	rebel	k1gMnPc2	rebel
a	a	k8xC	a
Fronty	fronta	k1gFnSc2	fronta
an-Nusrá	an-Nusrat	k5eAaPmIp3nS	an-Nusrat
<g/>
.	.	kIx.	.
</s>
<s>
Celý	celý	k2eAgInSc1d1	celý
komplex	komplex	k1gInSc1	komplex
dokončila	dokončit	k5eAaPmAgFnS	dokončit
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
2014	[number]	k4	2014
a	a	k8xC	a
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
umožnila	umožnit	k5eAaPmAgFnS	umožnit
zajistit	zajistit	k5eAaPmF	zajistit
ničím	ničí	k3xOyNgMnPc3	ničí
nerušený	rušený	k2eNgInSc4d1	nerušený
chod	chod	k1gInSc4	chod
těžby	těžba	k1gFnSc2	těžba
<g/>
.	.	kIx.	.
</s>
<s>
Vládní	vládní	k2eAgInSc1d1	vládní
syrský	syrský	k2eAgInSc1d1	syrský
deník	deník	k1gInSc1	deník
Tišrín	Tišrína	k1gFnPc2	Tišrína
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2014	[number]	k4	2014
oficiálně	oficiálně	k6eAd1	oficiálně
potvrdil	potvrdit	k5eAaPmAgMnS	potvrdit
opětovné	opětovný	k2eAgNnSc4d1	opětovné
zahájení	zahájení	k1gNnSc4	zahájení
prací	práce	k1gFnPc2	práce
a	a	k8xC	a
explicitně	explicitně	k6eAd1	explicitně
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
Moskvou	Moskva	k1gFnSc7	Moskva
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
ocenil	ocenit	k5eAaPmAgMnS	ocenit
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Kurdové	Kurd	k1gMnPc5	Kurd
===	===	k?	===
</s>
</p>
<p>
<s>
Vojenské	vojenský	k2eAgInPc1d1	vojenský
síly	síl	k1gInPc1	síl
syrských	syrský	k2eAgMnPc2d1	syrský
Kurdů	Kurd	k1gMnPc2	Kurd
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
účastní	účastnit	k5eAaImIp3nS	účastnit
této	tento	k3xDgFnSc2	tento
války	válka	k1gFnSc2	válka
<g/>
.	.	kIx.	.
</s>
<s>
Jejich	jejich	k3xOp3gFnSc4	jejich
strategii	strategie	k1gFnSc4	strategie
lze	lze	k6eAd1	lze
chápat	chápat	k5eAaImF	chápat
jako	jako	k9	jako
snahu	snaha	k1gFnSc4	snaha
o	o	k7c4	o
bleskové	bleskový	k2eAgNnSc4d1	bleskové
ovládnutí	ovládnutí	k1gNnSc4	ovládnutí
oslabených	oslabený	k2eAgNnPc2d1	oslabené
měst	město	k1gNnPc2	město
obývaných	obývaný	k2eAgNnPc2d1	obývané
lidmi	člověk	k1gMnPc7	člověk
kurdské	kurdský	k2eAgFnSc2d1	kurdská
národnosti	národnost	k1gFnSc2	národnost
<g/>
,	,	kIx,	,
s	s	k7c7	s
minimem	minimum	k1gNnSc7	minimum
civilních	civilní	k2eAgFnPc2d1	civilní
ztrát	ztráta	k1gFnPc2	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
Kurdské	kurdský	k2eAgFnPc1d1	kurdská
síly	síla	k1gFnPc1	síla
jsou	být	k5eAaImIp3nP	být
rozděleny	rozdělit	k5eAaPmNgFnP	rozdělit
do	do	k7c2	do
dvou	dva	k4xCgFnPc2	dva
<g/>
,	,	kIx,	,
zhruba	zhruba	k6eAd1	zhruba
stejně	stejně	k6eAd1	stejně
velkých	velký	k2eAgFnPc2d1	velká
skupin	skupina	k1gFnPc2	skupina
<g/>
,	,	kIx,	,
které	který	k3yIgInPc4	který
mezi	mezi	k7c7	mezi
sebou	se	k3xPyFc7	se
politicky	politicky	k6eAd1	politicky
soupeří	soupeřit	k5eAaImIp3nP	soupeřit
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
po	po	k7c6	po
vojenské	vojenský	k2eAgFnSc6d1	vojenská
stránce	stránka	k1gFnSc6	stránka
spolupracují	spolupracovat	k5eAaImIp3nP	spolupracovat
<g/>
,	,	kIx,	,
a	a	k8xC	a
to	ten	k3xDgNnSc1	ten
kvůli	kvůli	k7c3	kvůli
vážnosti	vážnost	k1gFnSc3	vážnost
situace	situace	k1gFnSc2	situace
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
Sjednocenou	sjednocený	k2eAgFnSc4d1	sjednocená
demokratickou	demokratický	k2eAgFnSc4d1	demokratická
stranu	strana	k1gFnSc4	strana
(	(	kIx(	(
<g/>
PYD	PYD	kA	PYD
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
je	být	k5eAaImIp3nS	být
syrskou	syrský	k2eAgFnSc7d1	Syrská
odnoží	odnož	k1gFnSc7	odnož
Kurdské	kurdský	k2eAgFnSc2d1	kurdská
strany	strana	k1gFnSc2	strana
pracujících	pracující	k1gMnPc2	pracující
(	(	kIx(	(
<g/>
PKK	PKK	kA	PKK
<g/>
)	)	kIx)	)
a	a	k8xC	a
pod	pod	k7c7	pod
kterou	který	k3yIgFnSc7	který
spadají	spadat	k5eAaPmIp3nP	spadat
Lidové	lidový	k2eAgFnPc1d1	lidová
obranné	obranný	k2eAgFnPc1d1	obranná
jednotky	jednotka	k1gFnPc1	jednotka
(	(	kIx(	(
<g/>
YPG	YPG	kA	YPG
<g/>
)	)	kIx)	)
a	a	k8xC	a
Kurdskou	kurdský	k2eAgFnSc4d1	kurdská
národní	národní	k2eAgFnSc4d1	národní
radu	rada	k1gFnSc4	rada
(	(	kIx(	(
<g/>
KNC	KNC	kA	KNC
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
jsou	být	k5eAaImIp3nP	být
sdružena	sdružen	k2eAgNnPc1d1	sdruženo
všechna	všechen	k3xTgNnPc1	všechen
ostatní	ostatní	k2eAgNnPc1d1	ostatní
kurdská	kurdský	k2eAgNnPc1d1	kurdské
hnutí	hnutí	k1gNnPc1	hnutí
na	na	k7c4	na
území	území	k1gNnSc4	území
Sýrie	Sýrie	k1gFnSc2	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
Odmítavě	odmítavě	k6eAd1	odmítavě
se	se	k3xPyFc4	se
k	k	k7c3	k
aktivitám	aktivita	k1gFnPc3	aktivita
syrských	syrský	k2eAgMnPc2d1	syrský
Kurdů	Kurd	k1gMnPc2	Kurd
staví	stavit	k5eAaBmIp3nS	stavit
především	především	k9	především
Turecko	Turecko	k1gNnSc1	Turecko
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc4	který
je	být	k5eAaImIp3nS	být
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
precedens	precedens	k1gNnSc4	precedens
k	k	k7c3	k
narušení	narušení	k1gNnSc3	narušení
své	svůj	k3xOyFgFnSc2	svůj
územní	územní	k2eAgFnSc2d1	územní
integrity	integrita	k1gFnSc2	integrita
<g/>
.	.	kIx.	.
</s>
<s>
Kurdští	kurdský	k2eAgMnPc1d1	kurdský
novináři	novinář	k1gMnPc1	novinář
a	a	k8xC	a
analytici	analytik	k1gMnPc1	analytik
opakovaně	opakovaně	k6eAd1	opakovaně
obvinili	obvinit	k5eAaPmAgMnP	obvinit
Turecko	Turecko	k1gNnSc4	Turecko
z	z	k7c2	z
organizování	organizování	k1gNnSc2	organizování
genocidních	genocidní	k2eAgFnPc2d1	genocidní
protikurdských	protikurdský	k2eAgFnPc2d1	protikurdský
milic	milice	k1gFnPc2	milice
na	na	k7c6	na
druhé	druhý	k4xOgFnSc6	druhý
straně	strana	k1gFnSc6	strana
syrské	syrský	k2eAgFnSc2d1	Syrská
hranice	hranice	k1gFnSc2	hranice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
2014	[number]	k4	2014
podnikl	podniknout	k5eAaPmAgMnS	podniknout
Islámský	islámský	k2eAgMnSc1d1	islámský
stát	stát	k5eAaImF	stát
masivní	masivní	k2eAgInSc4d1	masivní
útok	útok	k1gInSc4	útok
proti	proti	k7c3	proti
městu	město	k1gNnSc3	město
Kobanî	Kobanî	k1gFnSc2	Kobanî
a	a	k8xC	a
okolním	okolní	k2eAgFnPc3d1	okolní
vesnicím	vesnice	k1gFnPc3	vesnice
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
bitvě	bitva	k1gFnSc6	bitva
o	o	k7c6	o
Kobani	Kobaň	k1gFnSc6	Kobaň
byl	být	k5eAaImAgInS	být
Islámský	islámský	k2eAgInSc1d1	islámský
stát	stát	k1gInSc1	stát
odražen	odrazit	k5eAaPmNgInS	odrazit
kurdskými	kurdský	k2eAgFnPc7d1	kurdská
milicemi	milice	k1gFnPc7	milice
YPG	YPG	kA	YPG
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
července	červenec	k1gInSc2	červenec
2015	[number]	k4	2015
Turecko	Turecko	k1gNnSc1	Turecko
letecky	letecky	k6eAd1	letecky
útočí	útočit	k5eAaImIp3nS	útočit
na	na	k7c4	na
pozice	pozice	k1gFnPc4	pozice
irácké	irácký	k2eAgFnSc2d1	irácká
PKK	PKK	kA	PKK
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
podniklo	podniknout	k5eAaPmAgNnS	podniknout
i	i	k9	i
útoky	útok	k1gInPc4	útok
na	na	k7c4	na
pozice	pozice	k1gFnPc4	pozice
syrských	syrský	k2eAgMnPc2d1	syrský
Kurdů	Kurd	k1gMnPc2	Kurd
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c4	mezi
spojence	spojenec	k1gMnPc4	spojenec
Kurdů	Kurd	k1gMnPc2	Kurd
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
patří	patřit	k5eAaImIp3nP	patřit
zejména	zejména	k9	zejména
ozbrojené	ozbrojený	k2eAgFnPc1d1	ozbrojená
skupiny	skupina	k1gFnPc1	skupina
z	z	k7c2	z
řad	řada	k1gFnPc2	řada
Asyřanů	Asyřan	k1gMnPc2	Asyřan
a	a	k8xC	a
některé	některý	k3yIgFnPc1	některý
jednotky	jednotka	k1gFnPc1	jednotka
Svobodné	svobodný	k2eAgFnSc2d1	svobodná
syrské	syrský	k2eAgFnSc2d1	Syrská
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
lednu	leden	k1gInSc6	leden
2018	[number]	k4	2018
začala	začít	k5eAaPmAgFnS	začít
turecká	turecký	k2eAgFnSc1d1	turecká
invaze	invaze	k1gFnSc1	invaze
do	do	k7c2	do
Afrínu	Afrín	k1gInSc2	Afrín
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
byl	být	k5eAaImAgInS	být
ovládán	ovládat	k5eAaImNgMnS	ovládat
Kurdy	Kurd	k1gMnPc7	Kurd
<g/>
.	.	kIx.	.
</s>
<s>
Afrín	Afrín	k1gInSc1	Afrín
byl	být	k5eAaImAgInS	být
tureckou	turecký	k2eAgFnSc7d1	turecká
armádou	armáda	k1gFnSc7	armáda
obsazen	obsadit	k5eAaPmNgInS	obsadit
v	v	k7c6	v
březnu	březen	k1gInSc6	březen
2018	[number]	k4	2018
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
straně	strana	k1gFnSc6	strana
Turecka	Turecko	k1gNnSc2	Turecko
proti	proti	k7c3	proti
Kurdům	Kurd	k1gMnPc3	Kurd
bojovaly	bojovat	k5eAaImAgInP	bojovat
Svobodná	svobodný	k2eAgFnSc1d1	svobodná
syrská	syrský	k2eAgFnSc1d1	Syrská
armáda	armáda	k1gFnSc1	armáda
a	a	k8xC	a
islamistická	islamistický	k2eAgFnSc1d1	islamistická
koalice	koalice	k1gFnSc1	koalice
Ahrar	Ahrara	k1gFnPc2	Ahrara
aš-Šám	aš-Šat	k5eAaImIp1nS	aš-Šat
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rusko	Rusko	k1gNnSc1	Rusko
a	a	k8xC	a
Francie	Francie	k1gFnSc1	Francie
===	===	k?	===
</s>
</p>
<p>
<s>
30	[number]	k4	30
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2015	[number]	k4	2015
schválila	schválit	k5eAaPmAgFnS	schválit
ruská	ruský	k2eAgFnSc1d1	ruská
Rada	rada	k1gFnSc1	rada
federace	federace	k1gFnSc2	federace
žádost	žádost	k1gFnSc1	žádost
ruského	ruský	k2eAgMnSc2d1	ruský
prezidenta	prezident	k1gMnSc2	prezident
Vladimira	Vladimir	k1gMnSc2	Vladimir
Putina	putin	k2eAgMnSc2d1	putin
o	o	k7c6	o
povolení	povolení	k1gNnSc6	povolení
nasadit	nasadit	k5eAaPmF	nasadit
ruské	ruský	k2eAgFnPc4d1	ruská
ozbrojené	ozbrojený	k2eAgFnPc4d1	ozbrojená
síly	síla	k1gFnPc4	síla
v	v	k7c6	v
zahraničí	zahraničí	k1gNnSc6	zahraničí
na	na	k7c6	na
základě	základ	k1gInSc6	základ
žádosti	žádost	k1gFnSc2	žádost
syrské	syrský	k2eAgFnSc2d1	Syrská
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
této	tento	k3xDgFnSc2	tento
ruské	ruský	k2eAgFnSc2d1	ruská
vojenské	vojenský	k2eAgFnSc2d1	vojenská
intervence	intervence	k1gFnSc2	intervence
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
bylo	být	k5eAaImAgNnS	být
ještě	ještě	k9	ještě
téhož	týž	k3xTgInSc2	týž
dne	den	k1gInSc2	den
nasazeno	nasazen	k2eAgNnSc1d1	nasazeno
ruské	ruský	k2eAgNnSc1d1	ruské
letectvo	letectvo	k1gNnSc1	letectvo
<g/>
.	.	kIx.	.
</s>
<s>
Ruská	ruský	k2eAgNnPc1d1	ruské
bojová	bojový	k2eAgNnPc1d1	bojové
letadla	letadlo	k1gNnPc1	letadlo
začala	začít	k5eAaPmAgNnP	začít
ze	z	k7c2	z
základen	základna	k1gFnPc2	základna
na	na	k7c6	na
území	území	k1gNnSc6	území
ovládaném	ovládaný	k2eAgNnSc6d1	ovládané
syrskou	syrský	k2eAgFnSc7d1	Syrská
vládou	vláda	k1gFnSc7	vláda
provádět	provádět	k5eAaImF	provádět
nálety	nálet	k1gInPc4	nálet
proti	proti	k7c3	proti
militantním	militantní	k2eAgFnPc3d1	militantní
skupinám	skupina	k1gFnPc3	skupina
Islámského	islámský	k2eAgInSc2d1	islámský
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
IS	IS	kA	IS
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
Fronty	fronta	k1gFnSc2	fronta
an-Nusrá	an-Nusrý	k2eAgFnSc1d1	an-Nusrý
(	(	kIx(	(
<g/>
Al-Káida	Al-Káida	k1gFnSc1	Al-Káida
v	v	k7c6	v
Levantě	Levanta	k1gFnSc6	Levanta
<g/>
)	)	kIx)	)
a	a	k8xC	a
Džaíš	Džaíš	k1gFnSc1	Džaíš
al-Fatah	al-Fataha	k1gFnPc2	al-Fataha
(	(	kIx(	(
<g/>
radikální	radikální	k2eAgMnPc1d1	radikální
sunnité	sunnita	k1gMnPc1	sunnita
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
<g/>
Ruský	ruský	k2eAgMnSc1d1	ruský
prezident	prezident	k1gMnSc1	prezident
Putin	putin	k2eAgMnSc1d1	putin
a	a	k8xC	a
francouzský	francouzský	k2eAgMnSc1d1	francouzský
prezident	prezident	k1gMnSc1	prezident
François	François	k1gFnSc2	François
Hollande	Holland	k1gInSc5	Holland
se	s	k7c7	s
17	[number]	k4	17
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2015	[number]	k4	2015
dohodli	dohodnout	k5eAaPmAgMnP	dohodnout
na	na	k7c6	na
vojenské	vojenský	k2eAgFnSc6d1	vojenská
spolupráci	spolupráce	k1gFnSc6	spolupráce
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
s	s	k7c7	s
úmyslem	úmysl	k1gInSc7	úmysl
koordinovat	koordinovat	k5eAaBmF	koordinovat
letecké	letecký	k2eAgInPc4d1	letecký
útoky	útok	k1gInPc4	útok
na	na	k7c4	na
Islámský	islámský	k2eAgInSc4d1	islámský
stát	stát	k1gInSc4	stát
<g/>
.	.	kIx.	.
</s>
<s>
Dohoda	dohoda	k1gFnSc1	dohoda
přišla	přijít	k5eAaPmAgFnS	přijít
nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
teroristických	teroristický	k2eAgInPc6d1	teroristický
útocích	útok	k1gInPc6	útok
v	v	k7c6	v
Paříži	Paříž	k1gFnSc6	Paříž
a	a	k8xC	a
výbuchu	výbuch	k1gInSc6	výbuch
ruského	ruský	k2eAgNnSc2d1	ruské
civilního	civilní	k2eAgNnSc2d1	civilní
letadla	letadlo	k1gNnSc2	letadlo
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
<g/>
.	.	kIx.	.
</s>
<s>
Francie	Francie	k1gFnSc1	Francie
začala	začít	k5eAaPmAgFnS	začít
útočit	útočit	k5eAaImF	útočit
především	především	k9	především
na	na	k7c4	na
město	město	k1gNnSc4	město
Rakka	Rakek	k1gInSc2	Rakek
<g/>
,	,	kIx,	,
považované	považovaný	k2eAgNnSc1d1	považované
za	za	k7c4	za
hlavní	hlavní	k2eAgNnSc4d1	hlavní
město	město	k1gNnSc4	město
IS	IS	kA	IS
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
oblasti	oblast	k1gFnSc2	oblast
připlula	připlout	k5eAaPmAgFnS	připlout
také	také	k9	také
francouzská	francouzský	k2eAgFnSc1d1	francouzská
námořní	námořní	k2eAgFnSc1d1	námořní
formace	formace	k1gFnSc1	formace
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
s	s	k7c7	s
letadlovou	letadlový	k2eAgFnSc7d1	letadlová
lodí	loď	k1gFnSc7	loď
Charles	Charles	k1gMnSc1	Charles
de	de	k?	de
Gaulle	Gaulle	k1gFnSc1	Gaulle
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
navázala	navázat	k5eAaPmAgFnS	navázat
přímé	přímý	k2eAgNnSc4d1	přímé
spojení	spojení	k1gNnSc4	spojení
<g/>
.	.	kIx.	.
</s>
<s>
Námořní	námořní	k2eAgInPc1d1	námořní
i	i	k8xC	i
letecké	letecký	k2eAgInPc1d1	letecký
údery	úder	k1gInPc1	úder
byly	být	k5eAaImAgInP	být
pak	pak	k6eAd1	pak
společně	společně	k6eAd1	společně
koordinovány	koordinován	k2eAgFnPc1d1	koordinována
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
24	[number]	k4	24
<g/>
.	.	kIx.	.
listopadu	listopad	k1gInSc2	listopad
2015	[number]	k4	2015
byl	být	k5eAaImAgInS	být
tureckým	turecký	k2eAgNnSc7d1	turecké
bojovým	bojový	k2eAgNnSc7d1	bojové
letadlem	letadlo	k1gNnSc7	letadlo
typu	typ	k1gInSc2	typ
F-16	F-16	k1gMnSc2	F-16
sestřelen	sestřelen	k2eAgMnSc1d1	sestřelen
ruský	ruský	k2eAgMnSc1d1	ruský
bombardér	bombardér	k1gMnSc1	bombardér
Su-	Su-	k1gFnSc2	Su-
<g/>
24	[number]	k4	24
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
poskytoval	poskytovat	k5eAaImAgInS	poskytovat
vzdušnou	vzdušný	k2eAgFnSc4d1	vzdušná
podporu	podpora	k1gFnSc4	podpora
vojákům	voják	k1gMnPc3	voják
syrské	syrský	k2eAgFnSc2d1	Syrská
armády	armáda	k1gFnSc2	armáda
v	v	k7c6	v
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
Frontě	fronta	k1gFnSc3	fronta
an-Nusrá	an-Nusrat	k5eAaPmIp3nS	an-Nusrat
a	a	k8xC	a
Brigádě	brigáda	k1gFnSc3	brigáda
syrských	syrský	k2eAgMnPc2d1	syrský
Turkmenů	Turkmen	k1gMnPc2	Turkmen
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Latákie	Latákie	k1gFnSc2	Latákie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
únoru	únor	k1gInSc6	únor
2016	[number]	k4	2016
oznámil	oznámit	k5eAaPmAgMnS	oznámit
Putin	putin	k2eAgMnSc1d1	putin
stáhnutí	stáhnutí	k1gNnPc4	stáhnutí
části	část	k1gFnSc2	část
ruského	ruský	k2eAgNnSc2d1	ruské
letectva	letectvo	k1gNnSc2	letectvo
ze	z	k7c2	z
Sýrie	Sýrie	k1gFnSc2	Sýrie
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
ruská	ruský	k2eAgFnSc1d1	ruská
intervence	intervence	k1gFnSc1	intervence
ještě	ještě	k6eAd1	ještě
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
10	[number]	k4	10
<g/>
.	.	kIx.	.
prosince	prosinec	k1gInSc2	prosinec
2017	[number]	k4	2017
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Putin	putin	k2eAgMnSc1d1	putin
ruskou	ruský	k2eAgFnSc4d1	ruská
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
základnu	základna	k1gFnSc4	základna
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
<g/>
,	,	kIx,	,
setkal	setkat	k5eAaPmAgMnS	setkat
se	se	k3xPyFc4	se
s	s	k7c7	s
prezidentem	prezident	k1gMnSc7	prezident
Bašárem	Bašár	k1gMnSc7	Bašár
al-Asadem	al-Asad	k1gInSc7	al-Asad
a	a	k8xC	a
oznámil	oznámit	k5eAaPmAgMnS	oznámit
stažení	stažení	k1gNnSc4	stažení
většiny	většina	k1gFnSc2	většina
ruských	ruský	k2eAgNnPc2d1	ruské
vojsk	vojsko	k1gNnPc2	vojsko
ze	z	k7c2	z
země	zem	k1gFnSc2	zem
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Turecko	Turecko	k1gNnSc1	Turecko
===	===	k?	===
</s>
</p>
<p>
<s>
Dne	den	k1gInSc2	den
23	[number]	k4	23
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
2016	[number]	k4	2016
vyšlo	vyjít	k5eAaPmAgNnS	vyjít
najevo	najevo	k6eAd1	najevo
<g/>
,	,	kIx,	,
že	že	k8xS	že
Turecko	Turecko	k1gNnSc1	Turecko
plánovalo	plánovat	k5eAaImAgNnS	plánovat
v	v	k7c6	v
nejbližší	blízký	k2eAgFnSc6d3	nejbližší
době	doba	k1gFnSc6	doba
zahájit	zahájit	k5eAaPmF	zahájit
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
intervenci	intervence	k1gFnSc4	intervence
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Sýrii	Sýrie	k1gFnSc6	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
Prvním	první	k4xOgInSc7	první
cílem	cíl	k1gInSc7	cíl
na	na	k7c6	na
syrském	syrský	k2eAgNnSc6d1	syrské
území	území	k1gNnSc6	území
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
pohraniční	pohraniční	k2eAgNnSc1d1	pohraniční
město	město	k1gNnSc1	město
Džarablu	Džarabl	k1gInSc2	Džarabl
<g/>
.	.	kIx.	.
</s>
<s>
Již	již	k6eAd1	již
toho	ten	k3xDgInSc2	ten
dne	den	k1gInSc2	den
vypálilo	vypálit	k5eAaPmAgNnS	vypálit
turecké	turecký	k2eAgNnSc1d1	turecké
dělostřelectvo	dělostřelectvo	k1gNnSc1	dělostřelectvo
60	[number]	k4	60
ran	rána	k1gFnPc2	rána
na	na	k7c4	na
pozice	pozice	k1gFnPc4	pozice
Islámského	islámský	k2eAgInSc2d1	islámský
státu	stát	k1gInSc2	stát
(	(	kIx(	(
<g/>
IS	IS	kA	IS
<g/>
)	)	kIx)	)
v	v	k7c6	v
Džarablu	Džarabl	k1gInSc6	Džarabl
<g/>
.	.	kIx.	.
</s>
<s>
Obyvatelé	obyvatel	k1gMnPc1	obyvatel
tureckého	turecký	k2eAgNnSc2d1	turecké
města	město	k1gNnSc2	město
Karkamis	Karkamis	k1gFnSc2	Karkamis
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc6	jeho
okolí	okolí	k1gNnSc6	okolí
byli	být	k5eAaImAgMnP	být
vyzváni	vyzvat	k5eAaPmNgMnP	vyzvat
k	k	k7c3	k
evakuaci	evakuace	k1gFnSc3	evakuace
<g/>
.	.	kIx.	.
<g/>
Svým	svůj	k3xOyFgInSc7	svůj
rozsáhlým	rozsáhlý	k2eAgInSc7d1	rozsáhlý
zásahem	zásah	k1gInSc7	zásah
s	s	k7c7	s
nasazením	nasazení	k1gNnSc7	nasazení
vojenského	vojenský	k2eAgNnSc2d1	vojenské
letectva	letectvo	k1gNnSc2	letectvo
a	a	k8xC	a
tankové	tankový	k2eAgFnSc2d1	tanková
brigády	brigáda	k1gFnSc2	brigáda
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
bezprostředně	bezprostředně	k6eAd1	bezprostředně
poté	poté	k6eAd1	poté
začal	začít	k5eAaPmAgInS	začít
<g/>
,	,	kIx,	,
zkomplikovalo	zkomplikovat	k5eAaPmAgNnS	zkomplikovat
Turecko	Turecko	k1gNnSc1	Turecko
beztak	beztak	k9	beztak
již	již	k6eAd1	již
nepřehlednou	přehledný	k2eNgFnSc4d1	nepřehledná
situaci	situace	k1gFnSc4	situace
v	v	k7c6	v
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
Spojené	spojený	k2eAgInPc1d1	spojený
státy	stát	k1gInPc1	stát
nebyly	být	k5eNaImAgInP	být
stavu	stav	k1gInSc6	stav
této	tento	k3xDgFnSc3	tento
invazi	invaze	k1gFnSc3	invaze
zabránit	zabránit	k5eAaPmF	zabránit
<g/>
.	.	kIx.	.
</s>
<s>
Turecko	Turecko	k1gNnSc1	Turecko
zprvu	zprvu	k6eAd1	zprvu
skutečně	skutečně	k6eAd1	skutečně
zaútočilo	zaútočit	k5eAaPmAgNnS	zaútočit
na	na	k7c4	na
pozice	pozice	k1gFnPc4	pozice
Islámského	islámský	k2eAgInSc2d1	islámský
státu	stát	k1gInSc2	stát
v	v	k7c6	v
severozápadní	severozápadní	k2eAgFnSc6d1	severozápadní
Sýrii	Sýrie	k1gFnSc6	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
posledních	poslední	k2eAgInPc6d1	poslední
dnech	den	k1gInPc6	den
srpna	srpen	k1gInSc2	srpen
2016	[number]	k4	2016
však	však	k9	však
jeho	jeho	k3xOp3gInPc1	jeho
masivní	masivní	k2eAgInPc1d1	masivní
útoky	útok	k1gInPc1	útok
směřovaly	směřovat	k5eAaImAgInP	směřovat
hlavně	hlavně	k9	hlavně
proti	proti	k7c3	proti
jednotkám	jednotka	k1gFnPc3	jednotka
syrských	syrský	k2eAgMnPc2d1	syrský
Kurdů	Kurd	k1gMnPc2	Kurd
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
měly	mít	k5eAaImAgFnP	mít
ve	v	k7c6	v
svém	svůj	k3xOyFgInSc6	svůj
boji	boj	k1gInSc6	boj
proti	proti	k7c3	proti
IS	IS	kA	IS
doposud	doposud	k6eAd1	doposud
podporu	podpora	k1gFnSc4	podpora
USA	USA	kA	USA
<g/>
.	.	kIx.	.
</s>
<s>
Postiženo	postižen	k2eAgNnSc1d1	postiženo
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
civilní	civilní	k2eAgNnSc1d1	civilní
obyvatelstvo	obyvatelstvo	k1gNnSc1	obyvatelstvo
této	tento	k3xDgFnSc2	tento
oblasti	oblast	k1gFnSc2	oblast
včetně	včetně	k7c2	včetně
sunnitských	sunnitský	k2eAgMnPc2d1	sunnitský
Arabů	Arab	k1gMnPc2	Arab
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Humanitární	humanitární	k2eAgFnSc1d1	humanitární
krize	krize	k1gFnSc1	krize
==	==	k?	==
</s>
</p>
<p>
<s>
Konflikt	konflikt	k1gInSc1	konflikt
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
si	se	k3xPyFc3	se
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
vyžádal	vyžádat	k5eAaPmAgInS	vyžádat
již	již	k9	již
přes	přes	k7c4	přes
130	[number]	k4	130
tisíc	tisíc	k4xCgInPc2	tisíc
obětí	oběť	k1gFnPc2	oběť
<g/>
,	,	kIx,	,
z	z	k7c2	z
nichž	jenž	k3xRgMnPc2	jenž
více	hodně	k6eAd2	hodně
než	než	k8xS	než
třetina	třetina	k1gFnSc1	třetina
jsou	být	k5eAaImIp3nP	být
civilisté	civilista	k1gMnPc1	civilista
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
dříve	dříve	k6eAd2	dříve
se	se	k3xPyFc4	se
centra	centrum	k1gNnPc1	centrum
konfliktu	konflikt	k1gInSc2	konflikt
soustředila	soustředit	k5eAaPmAgNnP	soustředit
především	především	k9	především
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
nyní	nyní	k6eAd1	nyní
se	se	k3xPyFc4	se
již	již	k6eAd1	již
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
dotýká	dotýkat	k5eAaImIp3nS	dotýkat
téměř	téměř	k6eAd1	téměř
celého	celý	k2eAgNnSc2d1	celé
území	území	k1gNnSc2	území
Sýrie	Sýrie	k1gFnSc2	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
během	během	k7c2	během
roku	rok	k1gInSc2	rok
2013	[number]	k4	2013
počet	počet	k1gInSc4	počet
uprchlíků	uprchlík	k1gMnPc2	uprchlík
z	z	k7c2	z
500	[number]	k4	500
tisíc	tisíc	k4xCgInPc2	tisíc
na	na	k7c4	na
téměř	téměř	k6eAd1	téměř
2,5	[number]	k4	2,5
milionu	milion	k4xCgInSc2	milion
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
OSN	OSN	kA	OSN
žije	žít	k5eAaImIp3nS	žít
nyní	nyní	k6eAd1	nyní
v	v	k7c6	v
podmínkách	podmínka	k1gFnPc6	podmínka
vyžadujících	vyžadující	k2eAgMnPc2d1	vyžadující
urgentní	urgentní	k2eAgFnSc4d1	urgentní
pomoc	pomoc	k1gFnSc4	pomoc
9,3	[number]	k4	9,3
milionu	milion	k4xCgInSc2	milion
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
je	být	k5eAaImIp3nS	být
téměř	téměř	k6eAd1	téměř
polovina	polovina	k1gFnSc1	polovina
všech	všecek	k3xTgMnPc2	všecek
občanů	občan	k1gMnPc2	občan
<g/>
,	,	kIx,	,
a	a	k8xC	a
jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
největší	veliký	k2eAgFnSc4d3	veliký
humanitární	humanitární	k2eAgFnSc4d1	humanitární
krizi	krize	k1gFnSc4	krize
současnosti	současnost	k1gFnSc2	současnost
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2015	[number]	k4	2015
počet	počet	k1gInSc4	počet
syrských	syrský	k2eAgMnPc2d1	syrský
uprchlíků	uprchlík	k1gMnPc2	uprchlík
v	v	k7c6	v
okolních	okolní	k2eAgInPc6d1	okolní
státech	stát	k1gInPc6	stát
vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
na	na	k7c4	na
4	[number]	k4	4
miliony	milion	k4xCgInPc7	milion
a	a	k8xC	a
dalších	další	k2eAgInPc2d1	další
7,6	[number]	k4	7,6
milionů	milion	k4xCgInPc2	milion
lidí	člověk	k1gMnPc2	člověk
jsou	být	k5eAaImIp3nP	být
uprchlíky	uprchlík	k1gMnPc4	uprchlík
uvnitř	uvnitř	k7c2	uvnitř
Sýrie	Sýrie	k1gFnSc2	Sýrie
<g/>
.	.	kIx.	.
</s>
<s>
Konflikt	konflikt	k1gInSc1	konflikt
si	se	k3xPyFc3	se
do	do	k7c2	do
konce	konec	k1gInSc2	konec
roku	rok	k1gInSc2	rok
2015	[number]	k4	2015
vyžádal	vyžádat	k5eAaPmAgInS	vyžádat
přes	přes	k7c4	přes
250	[number]	k4	250
tisíc	tisíc	k4xCgInPc2	tisíc
obětí	oběť	k1gFnPc2	oběť
<g/>
.	.	kIx.	.
</s>
<s>
Špatnou	špatný	k2eAgFnSc4d1	špatná
situaci	situace	k1gFnSc4	situace
však	však	k9	však
prohlubuje	prohlubovat	k5eAaImIp3nS	prohlubovat
i	i	k9	i
selhání	selhání	k1gNnSc4	selhání
světové	světový	k2eAgFnSc2d1	světová
diplomacie	diplomacie	k1gFnSc2	diplomacie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
je	být	k5eAaImIp3nS	být
působení	působení	k1gNnSc1	působení
humanitárních	humanitární	k2eAgFnPc2d1	humanitární
organizací	organizace	k1gFnPc2	organizace
velmi	velmi	k6eAd1	velmi
omezeno	omezen	k2eAgNnSc1d1	omezeno
a	a	k8xC	a
vzrůstá	vzrůstat	k5eAaImIp3nS	vzrůstat
také	také	k9	také
ohrožení	ohrožení	k1gNnSc1	ohrožení
jejich	jejich	k3xOp3gMnPc2	jejich
pracovníků	pracovník	k1gMnPc2	pracovník
-	-	kIx~	-
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2014	[number]	k4	2014
zemřeli	zemřít	k5eAaPmAgMnP	zemřít
v	v	k7c6	v
jednom	jeden	k4xCgNnSc6	jeden
z	z	k7c2	z
center	centrum	k1gNnPc2	centrum
konfliktu	konflikt	k1gInSc6	konflikt
tři	tři	k4xCgMnPc1	tři
zaměstnanci	zaměstnanec	k1gMnPc1	zaměstnanec
české	český	k2eAgFnSc2d1	Česká
organizace	organizace	k1gFnSc2	organizace
Člověk	člověk	k1gMnSc1	člověk
v	v	k7c6	v
tísni	tíseň	k1gFnSc6	tíseň
<g/>
,	,	kIx,	,
o	o	k7c4	o
týden	týden	k1gInSc4	týden
dříve	dříve	k6eAd2	dříve
bylo	být	k5eAaImAgNnS	být
uneseno	unést	k5eAaPmNgNnS	unést
pět	pět	k4xCc1	pět
pracovníků	pracovník	k1gMnPc2	pracovník
Lékařů	lékař	k1gMnPc2	lékař
bez	bez	k7c2	bez
hranic	hranice	k1gFnPc2	hranice
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Civilní	civilní	k2eAgFnPc4d1	civilní
oběti	oběť	k1gFnPc4	oběť
===	===	k?	===
</s>
</p>
<p>
<s>
Americký	americký	k2eAgInSc1d1	americký
nálet	nálet	k1gInSc1	nálet
na	na	k7c4	na
město	město	k1gNnSc4	město
Al-Báb	Al-Bába	k1gFnPc2	Al-Bába
v	v	k7c6	v
prosinci	prosinec	k1gInSc6	prosinec
2014	[number]	k4	2014
zabil	zabít	k5eAaPmAgMnS	zabít
více	hodně	k6eAd2	hodně
než	než	k8xS	než
50	[number]	k4	50
civilistů	civilista	k1gMnPc2	civilista
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
října	říjen	k1gInSc2	říjen
2015	[number]	k4	2015
bylo	být	k5eAaImAgNnS	být
zaznamenáno	zaznamenat	k5eAaPmNgNnS	zaznamenat
12	[number]	k4	12
náletů	nálet	k1gInPc2	nálet
na	na	k7c4	na
nemocnice	nemocnice	k1gFnPc4	nemocnice
a	a	k8xC	a
několik	několik	k4yIc4	několik
škol	škola	k1gFnPc2	škola
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yQgFnPc2	který
je	být	k5eAaImIp3nS	být
podezíráno	podezírán	k2eAgNnSc1d1	podezíráno
Rusko	Rusko	k1gNnSc1	Rusko
resp.	resp.	kA	resp.
syrský	syrský	k2eAgInSc4d1	syrský
režim	režim	k1gInSc4	režim
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k8xC	jak
Rusko	Rusko	k1gNnSc1	Rusko
<g/>
,	,	kIx,	,
tak	tak	k8xS	tak
Sýrie	Sýrie	k1gFnSc1	Sýrie
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
za	za	k7c4	za
tyto	tento	k3xDgInPc4	tento
útoky	útok	k1gInPc4	útok
odmítají	odmítat	k5eAaImIp3nP	odmítat
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnPc1d1	další
nemocnice	nemocnice	k1gFnPc1	nemocnice
byly	být	k5eAaImAgFnP	být
bombardovány	bombardovat	k5eAaImNgFnP	bombardovat
po	po	k7c6	po
tomto	tento	k3xDgInSc6	tento
datu	datum	k1gNnSc6	datum
<g/>
.	.	kIx.	.
</s>
<s>
Dne	den	k1gInSc2	den
20	[number]	k4	20
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2016	[number]	k4	2016
byly	být	k5eAaImAgInP	být
při	při	k7c6	při
náletu	nálet	k1gInSc6	nálet
na	na	k7c6	na
povstalecké	povstalecký	k2eAgFnSc6d1	povstalecká
čtvrti	čtvrt	k1gFnSc6	čtvrt
v	v	k7c6	v
syrském	syrský	k2eAgInSc6d1	syrský
Halabu	Halab	k1gInSc6	Halab
zasaženy	zasáhnout	k5eAaPmNgInP	zasáhnout
také	také	k9	také
dva	dva	k4xCgInPc1	dva
kamiony	kamion	k1gInPc1	kamion
s	s	k7c7	s
humanitární	humanitární	k2eAgFnSc7d1	humanitární
pomocí	pomoc	k1gFnSc7	pomoc
<g/>
.	.	kIx.	.
</s>
<s>
Útok	útok	k1gInSc1	útok
ostře	ostro	k6eAd1	ostro
odsoudily	odsoudit	k5eAaPmAgFnP	odsoudit
USA	USA	kA	USA
a	a	k8xC	a
hodlaly	hodlat	k5eAaImAgFnP	hodlat
přehodnotit	přehodnotit	k5eAaPmF	přehodnotit
spolupráci	spolupráce	k1gFnSc4	spolupráce
s	s	k7c7	s
Ruskem	Rusko	k1gNnSc7	Rusko
<g/>
.	.	kIx.	.
</s>
<s>
Moskva	Moskva	k1gFnSc1	Moskva
odpovědnost	odpovědnost	k1gFnSc1	odpovědnost
za	za	k7c4	za
útok	útok	k1gInSc4	útok
odmítá	odmítat	k5eAaImIp3nS	odmítat
<g/>
.	.	kIx.	.
</s>
<s>
OSN	OSN	kA	OSN
po	po	k7c6	po
vysvětlení	vysvětlení	k1gNnSc6	vysvětlení
Ruska	Ruska	k1gFnSc1	Ruska
upustila	upustit	k5eAaPmAgFnS	upustit
od	od	k7c2	od
pondělního	pondělní	k2eAgInSc2d1	pondělní
popisu	popis	k1gInSc2	popis
jako	jako	k8xC	jako
náletu	nálet	k1gInSc2	nálet
<g/>
;	;	kIx,	;
nyní	nyní	k6eAd1	nyní
hovoří	hovořit	k5eAaImIp3nS	hovořit
pouze	pouze	k6eAd1	pouze
o	o	k7c6	o
nespecifických	specifický	k2eNgInPc6d1	nespecifický
útocích	útok	k1gInPc6	útok
<g/>
.	.	kIx.	.
"	"	kIx"	"
<g/>
Nejsme	být	k5eNaImIp1nP	být
v	v	k7c6	v
pozici	pozice	k1gFnSc6	pozice
<g/>
,	,	kIx,	,
abychom	aby	kYmCp1nP	aby
určovali	určovat	k5eAaImAgMnP	určovat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
to	ten	k3xDgNnSc1	ten
byly	být	k5eAaImAgInP	být
nálety	nálet	k1gInPc1	nálet
<g/>
.	.	kIx.	.
</s>
<s>
Můžeme	moct	k5eAaImIp1nP	moct
pouze	pouze	k6eAd1	pouze
říci	říct	k5eAaPmF	říct
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
na	na	k7c4	na
konvoj	konvoj	k1gInSc4	konvoj
útočilo	útočit	k5eAaImAgNnS	útočit
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
sdělil	sdělit	k5eAaPmAgMnS	sdělit
mluvčí	mluvčí	k1gMnSc1	mluvčí
OSN	OSN	kA	OSN
Jens	Jens	k1gInSc1	Jens
Laerke	Laerke	k1gInSc1	Laerke
<g/>
.	.	kIx.	.
</s>
<s>
Tomuto	tento	k3xDgInSc3	tento
útoku	útok	k1gInSc3	útok
předcházela	předcházet	k5eAaImAgFnS	předcházet
akce	akce	k1gFnSc1	akce
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
vojenská	vojenský	k2eAgNnPc1d1	vojenské
letadla	letadlo	k1gNnPc1	letadlo
koalice	koalice	k1gFnSc2	koalice
vedené	vedený	k2eAgInPc1d1	vedený
Spojenými	spojený	k2eAgInPc7d1	spojený
státy	stát	k1gInPc7	stát
bojující	bojující	k2eAgFnSc7d1	bojující
proti	proti	k7c3	proti
Islámskému	islámský	k2eAgInSc3d1	islámský
státu	stát	k1gInSc3	stát
bombardovala	bombardovat	k5eAaImAgFnS	bombardovat
dne	den	k1gInSc2	den
17	[number]	k4	17
<g/>
.	.	kIx.	.
září	září	k1gNnSc2	září
2016	[number]	k4	2016
pozice	pozice	k1gFnSc2	pozice
syrské	syrský	k2eAgFnSc2d1	Syrská
armády	armáda	k1gFnSc2	armáda
blízko	blízko	k7c2	blízko
letiště	letiště	k1gNnSc2	letiště
u	u	k7c2	u
města	město	k1gNnSc2	město
Dajr	Dajr	k1gMnSc1	Dajr
az-Zaur	az-Zaur	k1gMnSc1	az-Zaur
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
organizace	organizace	k1gFnSc2	organizace
pro	pro	k7c4	pro
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
/	/	kIx~	/
<g/>
SOHR	SOHR	kA	SOHR
<g/>
/	/	kIx~	/
při	při	k7c6	při
útoku	útok	k1gInSc6	útok
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
nejméně	málo	k6eAd3	málo
80	[number]	k4	80
syrských	syrský	k2eAgMnPc2d1	syrský
vojáků	voják	k1gMnPc2	voják
<g/>
.	.	kIx.	.
</s>
<s>
Americká	americký	k2eAgFnSc1d1	americká
armáda	armáda	k1gFnSc1	armáda
poté	poté	k6eAd1	poté
oznámila	oznámit	k5eAaPmAgFnS	oznámit
<g/>
,	,	kIx,	,
že	že	k8xS	že
zastavila	zastavit	k5eAaPmAgFnS	zastavit
útoky	útok	k1gInPc4	útok
na	na	k7c4	na
cíle	cíl	k1gInPc4	cíl
<g/>
,	,	kIx,	,
o	o	k7c6	o
kterých	který	k3yIgInPc6	který
si	se	k3xPyFc3	se
myslela	myslet	k5eAaImAgFnS	myslet
<g/>
,	,	kIx,	,
že	že	k8xS	že
jsou	být	k5eAaImIp3nP	být
na	na	k7c4	na
pozici	pozice	k1gFnSc4	pozice
Islámského	islámský	k2eAgInSc2d1	islámský
státu	stát	k1gInSc2	stát
<g/>
.5	.5	k4	.5
<g/>
.	.	kIx.	.
května	květen	k1gInSc2	květen
2016	[number]	k4	2016
byl	být	k5eAaImAgInS	být
uskutečněn	uskutečněn	k2eAgInSc1d1	uskutečněn
nálet	nálet	k1gInSc1	nálet
na	na	k7c4	na
uprchlický	uprchlický	k2eAgInSc4d1	uprchlický
tábor	tábor	k1gInSc4	tábor
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Sýrie	Sýrie	k1gFnSc2	Sýrie
<g/>
,	,	kIx,	,
při	při	k7c6	při
kterém	který	k3yIgInSc6	který
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
nejméně	málo	k6eAd3	málo
28	[number]	k4	28
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
mnoha	mnoho	k4c2	mnoho
žen	žena	k1gFnPc2	žena
a	a	k8xC	a
dětí	dítě	k1gFnPc2	dítě
<g/>
.	.	kIx.	.
</s>
<s>
Nálet	nálet	k1gInSc1	nálet
ostře	ostro	k6eAd1	ostro
odsoudil	odsoudit	k5eAaPmAgInS	odsoudit
Západ	západ	k1gInSc1	západ
a	a	k8xC	a
OSN	OSN	kA	OSN
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
šéfa	šéf	k1gMnSc2	šéf
OSN	OSN	kA	OSN
Pan	Pan	k1gMnSc1	Pan
Ki-muna	Kiuna	k1gFnSc1	Ki-muna
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
útok	útok	k1gInSc1	útok
dal	dát	k5eAaPmAgInS	dát
hodnotit	hodnotit	k5eAaImF	hodnotit
jako	jako	k9	jako
válečný	válečný	k2eAgInSc4d1	válečný
zločin	zločin	k1gInSc4	zločin
<g/>
.	.	kIx.	.
</s>
<s>
Rusko	Rusko	k1gNnSc1	Rusko
i	i	k8xC	i
syrský	syrský	k2eAgInSc1d1	syrský
režim	režim	k1gInSc1	režim
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
je	být	k5eAaImIp3nS	být
podle	podle	k7c2	podle
nevládních	vládní	k2eNgFnPc2d1	nevládní
organizací	organizace	k1gFnPc2	organizace
nejvíce	nejvíce	k6eAd1	nejvíce
podezřelý	podezřelý	k2eAgInSc4d1	podezřelý
<g/>
,	,	kIx,	,
odpovědnost	odpovědnost	k1gFnSc1	odpovědnost
za	za	k7c4	za
útok	útok	k1gInSc4	útok
odmítají	odmítat	k5eAaImIp3nP	odmítat
<g/>
.15	.15	k4	.15
<g/>
.	.	kIx.	.
dubna	duben	k1gInSc2	duben
2017	[number]	k4	2017
zahynulo	zahynout	k5eAaPmAgNnS	zahynout
126	[number]	k4	126
civilistů	civilista	k1gMnPc2	civilista
<g/>
,	,	kIx,	,
z	z	k7c2	z
toho	ten	k3xDgNnSc2	ten
nejméně	málo	k6eAd3	málo
80	[number]	k4	80
dětí	dítě	k1gFnPc2	dítě
<g/>
,	,	kIx,	,
při	při	k7c6	při
teroristickém	teroristický	k2eAgInSc6d1	teroristický
útoku	útok	k1gInSc6	útok
na	na	k7c4	na
konvoj	konvoj	k1gInSc4	konvoj
odvážející	odvážející	k2eAgMnPc4d1	odvážející
šíitské	šíitský	k2eAgMnPc4d1	šíitský
uprchlíky	uprchlík	k1gMnPc4	uprchlík
ze	z	k7c2	z
dvou	dva	k4xCgNnPc2	dva
měst	město	k1gNnPc2	město
obléhaných	obléhaný	k2eAgFnPc2d1	obléhaná
povstaleckou	povstalecký	k2eAgFnSc7d1	povstalecká
skupinou	skupina	k1gFnSc7	skupina
Džaíš	Džaíš	k1gMnSc1	Džaíš
al-Fatah	al-Fatah	k1gMnSc1	al-Fatah
<g/>
.	.	kIx.	.
</s>
<s>
Civilisté	civilista	k1gMnPc1	civilista
byli	být	k5eAaImAgMnP	být
stoupenci	stoupenec	k1gMnPc1	stoupenec
syrské	syrský	k2eAgFnSc2d1	Syrská
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
2017	[number]	k4	2017
byla	být	k5eAaImAgFnS	být
kurdsko-arabskými	kurdskorabský	k2eAgFnPc7d1	kurdsko-arabský
jednotkami	jednotka	k1gFnPc7	jednotka
osvobozena	osvobodit	k5eAaPmNgFnS	osvobodit
Rakka	Rakka	k1gFnSc1	Rakka
<g/>
,	,	kIx,	,
hlavní	hlavní	k2eAgNnSc1d1	hlavní
město	město	k1gNnSc1	město
samozvaného	samozvaný	k2eAgInSc2d1	samozvaný
Islámského	islámský	k2eAgInSc2d1	islámský
státu	stát	k1gInSc2	stát
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
bojích	boj	k1gInPc6	boj
o	o	k7c4	o
město	město	k1gNnSc4	město
a	a	k8xC	a
při	při	k7c6	při
náletech	nálet	k1gInPc6	nálet
Američany	Američan	k1gMnPc4	Američan
vedené	vedený	k2eAgFnSc2d1	vedená
koalice	koalice	k1gFnSc2	koalice
bylo	být	k5eAaImAgNnS	být
podle	podle	k7c2	podle
OSN	OSN	kA	OSN
zničeno	zničen	k2eAgNnSc1d1	zničeno
přes	přes	k7c4	přes
80	[number]	k4	80
procent	procento	k1gNnPc2	procento
budov	budova	k1gFnPc2	budova
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
monitorovací	monitorovací	k2eAgFnSc2d1	monitorovací
skupiny	skupina	k1gFnSc2	skupina
Airwars	Airwarsa	k1gFnPc2	Airwarsa
<g/>
,	,	kIx,	,
sdružující	sdružující	k2eAgMnPc4d1	sdružující
nezávislé	závislý	k2eNgMnPc4d1	nezávislý
novináře	novinář	k1gMnPc4	novinář
<g/>
,	,	kIx,	,
si	se	k3xPyFc3	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2017	[number]	k4	2017
nálety	nálet	k1gInPc1	nálet
Američany	Američan	k1gMnPc4	Američan
vedené	vedený	k2eAgFnSc2d1	vedená
koalice	koalice	k1gFnSc2	koalice
na	na	k7c4	na
pozice	pozice	k1gFnPc4	pozice
Islámského	islámský	k2eAgInSc2d1	islámský
státu	stát	k1gInSc2	stát
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
a	a	k8xC	a
Iráku	Irák	k1gInSc6	Irák
vyžádaly	vyžádat	k5eAaPmAgFnP	vyžádat
životy	život	k1gInPc4	život
až	až	k9	až
6,000	[number]	k4	6,000
civilistů	civilista	k1gMnPc2	civilista
<g/>
.	.	kIx.	.
<g/>
Během	během	k7c2	během
invaze	invaze	k1gFnSc2	invaze
Turecka	Turecko	k1gNnSc2	Turecko
do	do	k7c2	do
Afrínu	Afrín	k1gInSc2	Afrín
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Sýrii	Sýrie	k1gFnSc6	Sýrie
<g/>
,	,	kIx,	,
která	který	k3yIgFnSc1	který
začala	začít	k5eAaPmAgFnS	začít
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
2018	[number]	k4	2018
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zabito	zabít	k5eAaPmNgNnS	zabít
přes	přes	k7c4	přes
500	[number]	k4	500
civilistů	civilista	k1gMnPc2	civilista
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
náletech	nálet	k1gInPc6	nálet
byla	být	k5eAaImAgFnS	být
zasažena	zasažen	k2eAgFnSc1d1	zasažena
i	i	k8xC	i
nemocnice	nemocnice	k1gFnSc1	nemocnice
<g/>
.	.	kIx.	.
<g/>
V	v	k7c6	v
syrských	syrský	k2eAgFnPc6d1	Syrská
věznicích	věznice	k1gFnPc6	věznice
pod	pod	k7c7	pod
assadovou	assadový	k2eAgFnSc7d1	assadový
kontrolou	kontrola	k1gFnSc7	kontrola
údajně	údajně	k6eAd1	údajně
končily	končit	k5eAaImAgInP	končit
během	během	k7c2	během
války	válka	k1gFnSc2	válka
desítky	desítka	k1gFnPc1	desítka
tisíc	tisíc	k4xCgInSc1	tisíc
odpůrců	odpůrce	k1gMnPc2	odpůrce
assadova	assadův	k2eAgInSc2d1	assadův
režimu	režim	k1gInSc2	režim
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
byli	být	k5eAaImAgMnP	být
podrobováni	podrobovat	k5eAaImNgMnP	podrobovat
krutým	krutý	k2eAgInPc3d1	krutý
výslechům	výslech	k1gInPc3	výslech
a	a	k8xC	a
mučení	mučení	k1gNnSc3	mučení
<g/>
.	.	kIx.	.
</s>
<s>
Mrtvá	mrtvý	k2eAgNnPc4d1	mrtvé
těla	tělo	k1gNnPc4	tělo
z	z	k7c2	z
věznic	věznice	k1gFnPc2	věznice
údajně	údajně	k6eAd1	údajně
vypadala	vypadat	k5eAaImAgFnS	vypadat
jako	jako	k9	jako
po	po	k7c6	po
holokaustu	holokaust	k1gInSc6	holokaust
a	a	k8xC	a
nesla	nést	k5eAaImAgFnS	nést
známky	známka	k1gFnPc4	známka
systematického	systematický	k2eAgNnSc2d1	systematické
mučení	mučení	k1gNnSc2	mučení
<g/>
.	.	kIx.	.
</s>
<s>
Běžné	běžný	k2eAgNnSc1d1	běžné
bylo	být	k5eAaImAgNnS	být
bití	bití	k1gNnSc1	bití
a	a	k8xC	a
znásilňování	znásilňování	k1gNnSc1	znásilňování
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
psychické	psychický	k2eAgNnSc1d1	psychické
týrání	týrání	k1gNnSc1	týrání
<g/>
,	,	kIx,	,
hlášeno	hlášen	k2eAgNnSc1d1	hlášeno
je	být	k5eAaImIp3nS	být
zapalování	zapalování	k1gNnSc1	zapalování
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Protirežimní	protirežimní	k2eAgFnSc1d1	protirežimní
organizace	organizace	k1gFnSc1	organizace
The	The	k1gFnSc2	The
Syrian	Syrian	k1gInSc1	Syrian
Network	network	k1gInSc2	network
for	forum	k1gNnPc2	forum
Human	Humana	k1gFnPc2	Humana
Rights	Rightsa	k1gFnPc2	Rightsa
<g/>
,	,	kIx,	,
sídlící	sídlící	k2eAgFnSc1d1	sídlící
ve	v	k7c6	v
Velké	velký	k2eAgFnSc6d1	velká
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
odhaduje	odhadovat	k5eAaImIp3nS	odhadovat
v	v	k7c6	v
roce	rok	k1gInSc6	rok
2019	[number]	k4	2019
počet	počet	k1gInSc1	počet
vězněných	vězněný	k2eAgFnPc2d1	vězněná
a	a	k8xC	a
usmrcených	usmrcený	k2eAgFnPc2d1	usmrcená
na	na	k7c6	na
128	[number]	k4	128
000	[number]	k4	000
a	a	k8xC	a
počet	počet	k1gInSc1	počet
umučených	umučený	k2eAgMnPc2d1	umučený
na	na	k7c4	na
14	[number]	k4	14
tisíc	tisíc	k4xCgInPc2	tisíc
<g/>
.	.	kIx.	.
</s>
<s>
Syrské	syrský	k2eAgInPc4d1	syrský
úřady	úřad	k1gInPc4	úřad
obvinění	obvinění	k1gNnSc2	obvinění
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
popírají	popírat	k5eAaImIp3nP	popírat
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
provádění	provádění	k1gNnSc4	provádění
hromadných	hromadný	k2eAgFnPc2d1	hromadná
poprav	poprava	k1gFnPc2	poprava
<g/>
,	,	kIx,	,
které	který	k3yRgFnPc1	který
se	se	k3xPyFc4	se
odehrávají	odehrávat	k5eAaImIp3nP	odehrávat
především	především	k9	především
ve	v	k7c6	v
věznici	věznice	k1gFnSc6	věznice
v	v	k7c6	v
Sidnáji	Sidnáj	k1gInSc6	Sidnáj
<g/>
.	.	kIx.	.
<g/>
Svědectví	svědectví	k1gNnSc6	svědectví
o	o	k7c6	o
mučení	mučení	k1gNnSc6	mučení
přinesl	přinést	k5eAaPmAgMnS	přinést
syrský	syrský	k2eAgMnSc1d1	syrský
dezertér	dezertér	k1gMnSc1	dezertér
pod	pod	k7c7	pod
pseudonymem	pseudonym	k1gInSc7	pseudonym
Caesar	Caesar	k1gMnSc1	Caesar
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
na	na	k7c4	na
Západ	západ	k1gInSc4	západ
propašoval	propašovat	k5eAaPmAgMnS	propašovat
velké	velký	k2eAgNnSc4d1	velké
množství	množství	k1gNnSc4	množství
fotografií	fotografia	k1gFnPc2	fotografia
vězeňských	vězeňský	k2eAgFnPc2d1	vězeňská
mrtvol	mrtvola	k1gFnPc2	mrtvola
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc1	jenž
nesly	nést	k5eAaImAgFnP	nést
tyto	tento	k3xDgFnPc1	tento
známky	známka	k1gFnPc1	známka
<g/>
.	.	kIx.	.
</s>
<s>
O	o	k7c6	o
hrůzách	hrůza	k1gFnPc6	hrůza
také	také	k9	také
vypovídají	vypovídat	k5eAaPmIp3nP	vypovídat
kresby	kresba	k1gFnPc1	kresba
Nadžá	Nadžá	k1gFnSc1	Nadžá
Bukaje	bukat	k5eAaImSgInS	bukat
<g/>
,	,	kIx,	,
který	který	k3yQgMnSc1	který
na	na	k7c6	na
vlastní	vlastní	k2eAgFnSc6d1	vlastní
kůži	kůže	k1gFnSc6	kůže
zažil	zažít	k5eAaPmAgMnS	zažít
krutost	krutost	k1gFnSc4	krutost
věznic	věznice	k1gFnPc2	věznice
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
začal	začít	k5eAaPmAgMnS	začít
malovat	malovat	k5eAaImF	malovat
výjevy	výjev	k1gInPc4	výjev
tamější	tamější	k2eAgFnSc2d1	tamější
bolesti	bolest	k1gFnSc2	bolest
a	a	k8xC	a
zoufalství	zoufalství	k1gNnSc2	zoufalství
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
byl	být	k5eAaImAgMnS	být
vězněn	věznit	k5eAaImNgMnS	věznit
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
190	[number]	k4	190
až	až	k9	až
220	[number]	k4	220
lidmi	člověk	k1gMnPc7	člověk
v	v	k7c6	v
místnosti	místnost	k1gFnSc6	místnost
16	[number]	k4	16
na	na	k7c4	na
3	[number]	k4	3
metry	metr	k1gInPc4	metr
<g/>
,	,	kIx,	,
v	v	k7c6	v
které	který	k3yQgFnSc6	který
se	se	k3xPyFc4	se
přímo	přímo	k6eAd1	přímo
odehrávala	odehrávat	k5eAaImAgNnP	odehrávat
rozličná	rozličný	k2eAgNnPc1d1	rozličné
mučení	mučení	k1gNnPc1	mučení
<g/>
.	.	kIx.	.
</s>
<s>
Denně	denně	k6eAd1	denně
tak	tak	k6eAd1	tak
nakládali	nakládat	k5eAaImAgMnP	nakládat
mrtvoli	mrtvole	k1gFnSc4	mrtvole
těch	ten	k3xDgMnPc2	ten
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
nevydrželi	vydržet	k5eNaPmAgMnP	vydržet
mučení	mučení	k1gNnSc4	mučení
nebo	nebo	k8xC	nebo
nemoci	nemoc	k1gFnPc4	nemoc
v	v	k7c6	v
otřesných	otřesný	k2eAgFnPc6d1	otřesná
hygienických	hygienický	k2eAgFnPc6d1	hygienická
podmínkách	podmínka	k1gFnPc6	podmínka
<g/>
.	.	kIx.	.
<g/>
Jedna	jeden	k4xCgFnSc1	jeden
žena	žena	k1gFnSc1	žena
například	například	k6eAd1	například
mluvila	mluvit	k5eAaImAgFnS	mluvit
o	o	k7c6	o
hromadném	hromadný	k2eAgNnSc6d1	hromadné
znásilnění	znásilnění	k1gNnSc6	znásilnění
pěti	pět	k4xCc2	pět
vojáky	voják	k1gMnPc4	voják
opakujícím	opakující	k2eAgFnPc3d1	opakující
se	se	k3xPyFc4	se
po	po	k7c4	po
14	[number]	k4	14
dní	den	k1gInPc2	den
<g/>
,	,	kIx,	,
potom	potom	k6eAd1	potom
přibylo	přibýt	k5eAaPmAgNnS	přibýt
bití	bití	k1gNnSc1	bití
po	po	k7c6	po
těle	tělo	k1gNnSc6	tělo
a	a	k8xC	a
do	do	k7c2	do
obličeje	obličej	k1gInSc2	obličej
nebo	nebo	k8xC	nebo
elektrické	elektrický	k2eAgInPc4d1	elektrický
šoky	šok	k1gInPc4	šok
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gFnSc1	její
cela	cela	k1gFnSc1	cela
byla	být	k5eAaImAgFnS	být
bez	bez	k7c2	bez
oken	okno	k1gNnPc2	okno
a	a	k8xC	a
miniaturních	miniaturní	k2eAgInPc2d1	miniaturní
rozměrů	rozměr	k1gInPc2	rozměr
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
přespávala	přespávat	k5eAaImAgFnS	přespávat
v	v	k7c6	v
místnosti	místnost	k1gFnSc6	místnost
4	[number]	k4	4
na	na	k7c4	na
5	[number]	k4	5
m	m	kA	m
se	s	k7c7	s
48	[number]	k4	48
dalšími	další	k2eAgFnPc7d1	další
ženami	žena	k1gFnPc7	žena
<g/>
.	.	kIx.	.
</s>
<s>
Toaleta	toaleta	k1gFnSc1	toaleta
byla	být	k5eAaImAgFnS	být
pro	pro	k7c4	pro
ně	on	k3xPp3gFnPc4	on
přístupná	přístupný	k2eAgFnSc1d1	přístupná
jednou	jeden	k4xCgFnSc7	jeden
za	za	k7c4	za
12	[number]	k4	12
hodin	hodina	k1gFnPc2	hodina
a	a	k8xC	a
sprcha	sprcha	k1gFnSc1	sprcha
jednou	jednou	k6eAd1	jednou
za	za	k7c4	za
40	[number]	k4	40
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
po	po	k7c6	po
4	[number]	k4	4
měsících	měsíc	k1gInPc6	měsíc
zkolabovala	zkolabovat	k5eAaPmAgFnS	zkolabovat
<g/>
,	,	kIx,	,
odvezli	odvézt	k5eAaPmAgMnP	odvézt
ji	on	k3xPp3gFnSc4	on
do	do	k7c2	do
nemocnice	nemocnice	k1gFnSc2	nemocnice
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
ji	on	k3xPp3gFnSc4	on
našli	najít	k5eAaPmAgMnP	najít
zápal	zápal	k1gInSc4	zápal
plic	plíce	k1gFnPc2	plíce
<g/>
,	,	kIx,	,
chudokrevnost	chudokrevnost	k1gFnSc4	chudokrevnost
a	a	k8xC	a
žloutenku	žloutenka	k1gFnSc4	žloutenka
<g/>
.	.	kIx.	.
</s>
<s>
Při	pře	k1gFnSc4	pře
sérií	série	k1gFnPc2	série
operací	operace	k1gFnPc2	operace
ji	on	k3xPp3gFnSc4	on
museli	muset	k5eAaImAgMnP	muset
lékaři	lékař	k1gMnPc1	lékař
opravit	opravit	k5eAaPmF	opravit
genitálie	genitálie	k1gFnPc4	genitálie
zničené	zničený	k2eAgFnSc2d1	zničená
dlouhodobým	dlouhodobý	k2eAgNnSc7d1	dlouhodobé
znásilňováním	znásilňování	k1gNnSc7	znásilňování
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgFnSc1d1	další
žena	žena	k1gFnSc1	žena
vypověděla	vypovědět	k5eAaPmAgFnS	vypovědět
<g/>
,	,	kIx,	,
že	že	k8xS	že
musela	muset	k5eAaImAgFnS	muset
v	v	k7c6	v
úzkých	úzký	k2eAgFnPc6d1	úzká
chodbách	chodba	k1gFnPc6	chodba
věznice	věznice	k1gFnSc2	věznice
procházet	procházet	k5eAaImF	procházet
blízko	blízko	k6eAd1	blízko
kolem	kolem	k7c2	kolem
mrtvých	mrtvý	k2eAgNnPc2d1	mrtvé
těl	tělo	k1gNnPc2	tělo
za	za	k7c2	za
křiku	křik	k1gInSc2	křik
mučených	mučený	k2eAgMnPc2d1	mučený
lidí	člověk	k1gMnPc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
srpnu	srpen	k1gInSc3	srpen
2017	[number]	k4	2017
došlo	dojít	k5eAaPmAgNnS	dojít
podle	podle	k7c2	podle
odhadů	odhad	k1gInPc2	odhad
organizace	organizace	k1gFnSc2	organizace
Právníci	právník	k1gMnPc1	právník
a	a	k8xC	a
lékaři	lékař	k1gMnPc1	lékař
pro	pro	k7c4	pro
lidská	lidský	k2eAgNnPc4d1	lidské
práva	právo	k1gNnPc4	právo
k	k	k7c3	k
65	[number]	k4	65
tisícům	tisíc	k4xCgInPc3	tisíc
úmrtí	úmrtí	k1gNnPc2	úmrtí
ve	v	k7c6	v
věznicích	věznice	k1gFnPc6	věznice
<g/>
.	.	kIx.	.
<g/>
Prodemokratický	Prodemokratický	k2eAgMnSc1d1	Prodemokratický
mladý	mladý	k2eAgMnSc1d1	mladý
aktivista	aktivista	k1gMnSc1	aktivista
Muhammad	Muhammad	k1gInSc4	Muhammad
Ghabáš	Ghabáš	k1gFnSc2	Ghabáš
prošel	projít	k5eAaPmAgMnS	projít
12	[number]	k4	12
věznicemi	věznice	k1gFnPc7	věznice
<g/>
,	,	kIx,	,
v	v	k7c6	v
jedné	jeden	k4xCgFnSc6	jeden
ho	on	k3xPp3gMnSc4	on
surově	surově	k6eAd1	surově
bili	bít	k5eAaImAgMnP	bít
a	a	k8xC	a
probouzel	probouzet	k5eAaImAgMnS	probouzet
se	se	k3xPyFc4	se
nahý	nahý	k2eAgMnSc1d1	nahý
ve	v	k7c6	v
studené	studený	k2eAgFnSc6d1	studená
chodbě	chodba	k1gFnSc6	chodba
<g/>
.	.	kIx.	.
</s>
<s>
Jindy	jindy	k6eAd1	jindy
mu	on	k3xPp3gMnSc3	on
vojád	vojáda	k1gFnPc2	vojáda
strčil	strčit	k5eAaPmAgMnS	strčit
pistoli	pistole	k1gFnSc4	pistole
do	do	k7c2	do
pusy	pusa	k1gFnSc2	pusa
<g/>
.	.	kIx.	.
</s>
<s>
Dozorčí	dozorčí	k1gFnSc1	dozorčí
s	s	k7c7	s
přezdívkou	přezdívka	k1gFnSc7	přezdívka
Hitler	Hitler	k1gMnSc1	Hitler
podle	podle	k7c2	podle
něj	on	k3xPp3gInSc2	on
pořádal	pořádat	k5eAaImAgMnS	pořádat
večerní	večerní	k2eAgInPc4d1	večerní
sadistické	sadistický	k2eAgInPc4d1	sadistický
zábavy	zábav	k1gInPc4	zábav
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
museli	muset	k5eAaImAgMnP	muset
vězni	vězeň	k1gMnPc1	vězeň
štěkat	štěkat	k5eAaImF	štěkat
<g/>
,	,	kIx,	,
mňoukat	mňoukat	k5eAaImF	mňoukat
nebo	nebo	k8xC	nebo
kokrhat	kokrhat	k5eAaImF	kokrhat
<g/>
,	,	kIx,	,
další	další	k2eAgMnPc1d1	další
klečeli	klečet	k5eAaImAgMnP	klečet
na	na	k7c6	na
čtyřech	čtyři	k4xCgNnPc6	čtyři
a	a	k8xC	a
dělali	dělat	k5eAaImAgMnP	dělat
dozorcům	dozorce	k1gMnPc3	dozorce
stoly	stol	k1gInPc4	stol
nebo	nebo	k8xC	nebo
židle	židle	k1gFnPc4	židle
<g/>
.	.	kIx.	.
</s>
<s>
Munír	Munír	k1gMnSc1	Munír
Fakír	fakír	k1gMnSc1	fakír
byl	být	k5eAaImAgMnS	být
zase	zase	k9	zase
se	s	k7c7	s
svými	svůj	k3xOyFgMnPc7	svůj
spoluvězni	spoluvězeň	k1gMnPc7	spoluvězeň
trestán	trestán	k2eAgInSc4d1	trestán
<g/>
,	,	kIx,	,
když	když	k8xS	když
mluvili	mluvit	k5eAaImAgMnP	mluvit
bez	bez	k7c2	bez
dovolení	dovolení	k1gNnSc2	dovolení
nebo	nebo	k8xC	nebo
usnuli	usnout	k5eAaPmAgMnP	usnout
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vojenské	vojenský	k2eAgFnSc6d1	vojenská
nemocnici	nemocnice	k1gFnSc6	nemocnice
nechali	nechat	k5eAaPmAgMnP	nechat
podle	podle	k7c2	podle
jeho	jeho	k3xOp3gNnSc2	jeho
svědectví	svědectví	k1gNnSc2	svědectví
zemřít	zemřít	k5eAaPmF	zemřít
člověka	člověk	k1gMnSc4	člověk
s	s	k7c7	s
cukrovkou	cukrovka	k1gFnSc7	cukrovka
kvůli	kvůli	k7c3	kvůli
nepodání	nepodání	k1gNnSc3	nepodání
inzulínu	inzulín	k1gInSc2	inzulín
<g/>
,	,	kIx,	,
druhého	druhý	k4xOgMnSc4	druhý
člověka	člověk	k1gMnSc4	člověk
ubili	ubít	k5eAaPmAgMnP	ubít
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
ranami	rána	k1gFnPc7	rána
do	do	k7c2	do
obličeje	obličej	k1gInSc2	obličej
<g/>
,	,	kIx,	,
když	když	k8xS	když
požádal	požádat	k5eAaPmAgMnS	požádat
o	o	k7c4	o
prášky	prášek	k1gInPc4	prášek
na	na	k7c4	na
bolest	bolest	k1gFnSc4	bolest
<g/>
.	.	kIx.	.
</s>
<s>
Další	další	k2eAgMnPc1d1	další
svědkové	svědek	k1gMnPc1	svědek
uvedli	uvést	k5eAaPmAgMnP	uvést
<g/>
,	,	kIx,	,
že	že	k8xS	že
museli	muset	k5eAaImAgMnP	muset
jíst	jíst	k5eAaImF	jíst
vlastní	vlastní	k2eAgInPc4d1	vlastní
exkrementy	exkrement	k1gInPc4	exkrement
<g/>
,	,	kIx,	,
pít	pít	k5eAaImF	pít
vlastní	vlastní	k2eAgFnSc4d1	vlastní
moč	moč	k1gFnSc4	moč
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
převáženi	převážen	k2eAgMnPc1d1	převážen
v	v	k7c6	v
řeznických	řeznický	k2eAgFnPc6d1	Řeznická
dodávkách	dodávka	k1gFnPc6	dodávka
zavěšeni	zavěšen	k2eAgMnPc1d1	zavěšen
na	na	k7c4	na
háky	hák	k1gInPc4	hák
<g/>
,	,	kIx,	,
objevilo	objevit	k5eAaPmAgNnS	objevit
se	se	k3xPyFc4	se
i	i	k9	i
svědectí	svědectit	k5eAaImIp3nS	svědectit
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
21	[number]	k4	21
letý	letý	k2eAgInSc1d1	letý
mladík	mladík	k1gInSc1	mladík
byl	být	k5eAaImAgInS	být
polit	polít	k5eAaPmNgInS	polít
benzínem	benzín	k1gInSc7	benzín
a	a	k8xC	a
zapálen	zapálen	k2eAgInSc1d1	zapálen
a	a	k8xC	a
následně	následně	k6eAd1	následně
ho	on	k3xPp3gMnSc4	on
nechali	nechat	k5eAaPmAgMnP	nechat
umírat	umírat	k5eAaImF	umírat
21	[number]	k4	21
dní	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
podpora	podpora	k1gFnSc1	podpora
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Vojenská	vojenský	k2eAgFnSc1d1	vojenská
účast	účast	k1gFnSc1	účast
==	==	k?	==
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
ROVENSKÝ	ROVENSKÝ	kA	ROVENSKÝ
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
<g/>
.	.	kIx.	.
</s>
<s>
Konflikt	konflikt	k1gInSc1	konflikt
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
<g/>
:	:	kIx,	:
Od	od	k7c2	od
pokojných	pokojný	k2eAgInPc2d1	pokojný
protestů	protest	k1gInPc2	protest
ke	k	k7c3	k
krvavé	krvavý	k2eAgFnSc3d1	krvavá
občanské	občanský	k2eAgFnSc3d1	občanská
válce	válka	k1gFnSc3	válka
<g/>
.	.	kIx.	.
</s>
<s>
ATM	ATM	kA	ATM
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
.	.	kIx.	.
2013	[number]	k4	2013
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
45	[number]	k4	45
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
9	[number]	k4	9
<g/>
,	,	kIx,	,
s.	s.	k?	s.
28	[number]	k4	28
až	až	k9	až
33	[number]	k4	33
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1802	[number]	k4	1802
<g/>
-	-	kIx~	-
<g/>
4823	[number]	k4	4823
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ROVENSKÝ	ROVENSKÝ	kA	ROVENSKÝ
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
<g/>
.	.	kIx.	.
</s>
<s>
Byznys	byznys	k1gInSc1	byznys
ve	v	k7c6	v
jménu	jméno	k1gNnSc6	jméno
džihádu	džihád	k1gInSc2	džihád
<g/>
:	:	kIx,	:
Zahraniční	zahraniční	k2eAgFnSc1d1	zahraniční
vojenská	vojenský	k2eAgFnSc1d1	vojenská
pomoc	pomoc	k1gFnSc1	pomoc
syrským	syrský	k2eAgMnSc7d1	syrský
povstalcům	povstalec	k1gMnPc3	povstalec
<g/>
.	.	kIx.	.
</s>
<s>
ATM	ATM	kA	ATM
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
.	.	kIx.	.
2014	[number]	k4	2014
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
46	[number]	k4	46
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
5	[number]	k4	5
<g/>
,	,	kIx,	,
s.	s.	k?	s.
44	[number]	k4	44
až	až	k9	až
48	[number]	k4	48
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1802	[number]	k4	1802
<g/>
-	-	kIx~	-
<g/>
4823	[number]	k4	4823
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
ROVENSKÝ	ROVENSKÝ	kA	ROVENSKÝ
<g/>
,	,	kIx,	,
Dušan	Dušan	k1gMnSc1	Dušan
<g/>
.	.	kIx.	.
</s>
<s>
Horké	Horké	k2eAgInPc4d1	Horké
okamžiky	okamžik	k1gInPc4	okamžik
na	na	k7c6	na
horké	horký	k2eAgFnSc6d1	horká
hranici	hranice	k1gFnSc6	hranice
<g/>
:	:	kIx,	:
Letecké	letecký	k2eAgInPc1d1	letecký
incidenty	incident	k1gInPc1	incident
na	na	k7c6	na
syrsko-tureckém	syrskourecký	k2eAgNnSc6d1	syrsko-turecký
pomezí	pomezí	k1gNnSc6	pomezí
<g/>
.	.	kIx.	.
</s>
<s>
ATM	ATM	kA	ATM
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
48	[number]	k4	48
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
,	,	kIx,	,
s.	s.	k?	s.
6	[number]	k4	6
až	až	k9	až
8	[number]	k4	8
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1802	[number]	k4	1802
<g/>
-	-	kIx~	-
<g/>
4823	[number]	k4	4823
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VISINGR	VISINGR	kA	VISINGR
<g/>
,	,	kIx,	,
Lukáš	Lukáš	k1gMnSc1	Lukáš
<g/>
.	.	kIx.	.
</s>
<s>
Sýrie	Sýrie	k1gFnSc1	Sýrie
<g/>
:	:	kIx,	:
Pragmatismus	pragmatismus	k1gInSc1	pragmatismus
v	v	k7c6	v
ohrožení	ohrožení	k1gNnSc6	ohrožení
<g/>
.	.	kIx.	.
</s>
<s>
ATM	ATM	kA	ATM
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
.	.	kIx.	.
2011	[number]	k4	2011
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
43	[number]	k4	43
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
8	[number]	k4	8
<g/>
,	,	kIx,	,
s.	s.	k?	s.
6	[number]	k4	6
až	až	k9	až
9	[number]	k4	9
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1802	[number]	k4	1802
<g/>
-	-	kIx~	-
<g/>
4823	[number]	k4	4823
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VISINGR	VISINGR	kA	VISINGR
<g/>
,	,	kIx,	,
Lukáš	Lukáš	k1gMnSc1	Lukáš
<g/>
.	.	kIx.	.
</s>
<s>
Ruské	ruský	k2eAgNnSc1d1	ruské
letectvo	letectvo	k1gNnSc1	letectvo
nad	nad	k7c7	nad
Sýrií	Sýrie	k1gFnSc7	Sýrie
<g/>
:	:	kIx,	:
Ukázka	ukázka	k1gFnSc1	ukázka
technického	technický	k2eAgInSc2d1	technický
i	i	k8xC	i
taktického	taktický	k2eAgInSc2d1	taktický
pokroku	pokrok	k1gInSc2	pokrok
<g/>
.	.	kIx.	.
</s>
<s>
ATM	ATM	kA	ATM
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
47	[number]	k4	47
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
12	[number]	k4	12
<g/>
,	,	kIx,	,
s.	s.	k?	s.
5	[number]	k4	5
až	až	k9	až
7	[number]	k4	7
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1802	[number]	k4	1802
<g/>
-	-	kIx~	-
<g/>
4823	[number]	k4	4823
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
VISINGR	VISINGR	kA	VISINGR
<g/>
,	,	kIx,	,
Lukáš	Lukáš	k1gMnSc1	Lukáš
<g/>
.	.	kIx.	.
</s>
<s>
Ruská	ruský	k2eAgFnSc1d1	ruská
intervence	intervence	k1gFnSc1	intervence
pokračuje	pokračovat	k5eAaImIp3nS	pokračovat
<g/>
:	:	kIx,	:
Bombardéry	bombardér	k1gInPc1	bombardér
<g/>
,	,	kIx,	,
námořnictvo	námořnictvo	k1gNnSc1	námořnictvo
a	a	k8xC	a
pozemní	pozemní	k2eAgFnSc1d1	pozemní
technika	technika	k1gFnSc1	technika
<g/>
.	.	kIx.	.
</s>
<s>
ATM	ATM	kA	ATM
<g/>
.	.	kIx.	.
2016	[number]	k4	2016
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
1	[number]	k4	1
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
8	[number]	k4	8
<g/>
,	,	kIx,	,
roč	ročit	k5eAaImRp2nS	ročit
<g/>
.	.	kIx.	.
48	[number]	k4	48
<g/>
,	,	kIx,	,
čís	čís	k3xOyIgInSc1wB	čís
<g/>
.	.	kIx.	.
1	[number]	k4	1
<g/>
,	,	kIx,	,
s.	s.	k?	s.
2	[number]	k4	2
až	až	k9	až
4	[number]	k4	4
<g/>
.	.	kIx.	.
</s>
<s>
ISSN	ISSN	kA	ISSN
1802	[number]	k4	1802
<g/>
-	-	kIx~	-
<g/>
4823	[number]	k4	4823
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Související	související	k2eAgInPc1d1	související
články	článek	k1gInPc1	článek
===	===	k?	===
</s>
</p>
<p>
<s>
Povstání	povstání	k1gNnSc1	povstání
v	v	k7c6	v
Iráku	Irák	k1gInSc6	Irák
(	(	kIx(	(
<g/>
2011	[number]	k4	2011
<g/>
–	–	k?	–
<g/>
současnost	současnost	k1gFnSc4	současnost
<g/>
)	)	kIx)	)
</s>
</p>
<p>
<s>
Islámský	islámský	k2eAgInSc1d1	islámský
stát	stát	k1gInSc1	stát
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
občanská	občanský	k2eAgFnSc1d1	občanská
válka	válka	k1gFnSc1	válka
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
Rozhovor	rozhovor	k1gInSc1	rozhovor
s	s	k7c7	s
odborníky	odborník	k1gMnPc7	odborník
o	o	k7c6	o
čínské	čínský	k2eAgFnSc6d1	čínská
<g/>
,	,	kIx,	,
íránské	íránský	k2eAgFnSc3d1	íránská
a	a	k8xC	a
ruské	ruský	k2eAgFnSc3d1	ruská
podpoře	podpora	k1gFnSc3	podpora
Asadova	Asadův	k2eAgInSc2d1	Asadův
režimu	režim	k1gInSc2	režim
ve	v	k7c6	v
Wikizprávách	Wikizpráva	k1gFnPc6	Wikizpráva
</s>
</p>
<p>
<s>
RODGERS	RODGERS	kA	RODGERS
<g/>
,	,	kIx,	,
Lucy	Luca	k1gFnSc2	Luca
<g/>
,	,	kIx,	,
David	David	k1gMnSc1	David
Gritten	Gritten	k2eAgMnSc1d1	Gritten
<g/>
,	,	kIx,	,
James	James	k1gMnSc1	James
Offer	Offer	k1gMnSc1	Offer
<g/>
,	,	kIx,	,
Patrick	Patrick	k1gMnSc1	Patrick
Asare	Asar	k1gInSc5	Asar
<g/>
.	.	kIx.	.
</s>
<s>
Syria	Syria	k1gFnSc1	Syria
<g/>
:	:	kIx,	:
The	The	k1gFnSc1	The
story	story	k1gFnSc2	story
of	of	k?	of
the	the	k?	the
conflict	conflict	k1gInSc1	conflict
[	[	kIx(	[
<g/>
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
bbc	bbc	k?	bbc
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
,	,	kIx,	,
rev	rev	k?	rev
<g/>
.	.	kIx.	.
2015-03-12	[number]	k4	2015-03-12
[	[	kIx(	[
<g/>
cit	cit	k1gInSc1	cit
<g/>
.	.	kIx.	.
2015	[number]	k4	2015
<g/>
-	-	kIx~	-
<g/>
0	[number]	k4	0
<g/>
7	[number]	k4	7
<g/>
-	-	kIx~	-
<g/>
15	[number]	k4	15
<g/>
]	]	kIx)	]
<g/>
.	.	kIx.	.
</s>
<s>
Dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
<g/>
.	.	kIx.	.
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
</p>
