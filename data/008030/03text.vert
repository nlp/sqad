<s>
Barvoslepost	barvoslepost	k1gFnSc1	barvoslepost
je	být	k5eAaImIp3nS	být
porucha	porucha	k1gFnSc1	porucha
barevného	barevný	k2eAgNnSc2d1	barevné
vidění	vidění	k1gNnSc2	vidění
lidského	lidský	k2eAgNnSc2d1	lidské
oka	oko	k1gNnSc2	oko
<g/>
.	.	kIx.	.
</s>
<s>
Její	její	k3xOp3gInSc1	její
odborný	odborný	k2eAgInSc1d1	odborný
název	název	k1gInSc1	název
daltonismus	daltonismus	k1gInSc1	daltonismus
je	být	k5eAaImIp3nS	být
odvozen	odvodit	k5eAaPmNgInS	odvodit
od	od	k7c2	od
jména	jméno	k1gNnSc2	jméno
anglického	anglický	k2eAgMnSc2d1	anglický
přírodovědce	přírodovědec	k1gMnSc2	přírodovědec
Johna	John	k1gMnSc2	John
Daltona	Dalton	k1gMnSc2	Dalton
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
touto	tento	k3xDgFnSc7	tento
poruchou	porucha	k1gFnSc7	porucha
sám	sám	k3xTgMnSc1	sám
trpěl	trpět	k5eAaImAgMnS	trpět
a	a	k8xC	a
vědecky	vědecky	k6eAd1	vědecky
ji	on	k3xPp3gFnSc4	on
popsal	popsat	k5eAaPmAgMnS	popsat
<g/>
.	.	kIx.	.
</s>
<s>
Celkem	celkem	k6eAd1	celkem
v	v	k7c6	v
populaci	populace	k1gFnSc6	populace
trpí	trpět	k5eAaImIp3nS	trpět
daltonismem	daltonismus	k1gInSc7	daltonismus
9	[number]	k4	9
%	%	kIx~	%
mužů	muž	k1gMnPc2	muž
a	a	k8xC	a
0,4	[number]	k4	0,4
%	%	kIx~	%
žen	žena	k1gFnPc2	žena
<g/>
.	.	kIx.	.
</s>
<s>
Dle	dle	k7c2	dle
závažnosti	závažnost	k1gFnSc2	závažnost
se	se	k3xPyFc4	se
dělí	dělit	k5eAaImIp3nS	dělit
na	na	k7c4	na
úplnou	úplný	k2eAgFnSc4d1	úplná
barvoslepost	barvoslepost	k1gFnSc4	barvoslepost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
člověk	člověk	k1gMnSc1	člověk
vnímá	vnímat	k5eAaImIp3nS	vnímat
pouze	pouze	k6eAd1	pouze
odstíny	odstín	k1gInPc4	odstín
šedi	šeď	k1gFnSc2	šeď
<g/>
,	,	kIx,	,
a	a	k8xC	a
částečnou	částečný	k2eAgFnSc4d1	částečná
barvoslepost	barvoslepost	k1gFnSc4	barvoslepost
<g/>
,	,	kIx,	,
při	při	k7c6	při
níž	jenž	k3xRgFnSc6	jenž
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
poruchu	porucha	k1gFnSc4	porucha
vnímání	vnímání	k1gNnSc2	vnímání
jen	jen	k9	jen
některé	některý	k3yIgNnSc1	některý
z	z	k7c2	z
barev	barva	k1gFnPc2	barva
(	(	kIx(	(
<g/>
částečná	částečný	k2eAgFnSc1d1	částečná
nebo	nebo	k8xC	nebo
úplná	úplný	k2eAgFnSc1d1	úplná
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
populaci	populace	k1gFnSc6	populace
je	být	k5eAaImIp3nS	být
porucha	porucha	k1gFnSc1	porucha
červené	červený	k2eAgFnSc2d1	červená
a	a	k8xC	a
zelené	zelený	k2eAgFnSc2d1	zelená
barvy	barva	k1gFnSc2	barva
gonosomálně	gonosomálně	k6eAd1	gonosomálně
recesivně	recesivně	k6eAd1	recesivně
dědičná	dědičný	k2eAgFnSc1d1	dědičná
(	(	kIx(	(
<g/>
zodpovědný	zodpovědný	k2eAgInSc1d1	zodpovědný
gen	gen	k1gInSc1	gen
leží	ležet	k5eAaImIp3nS	ležet
na	na	k7c6	na
chromozomu	chromozom	k1gInSc6	chromozom
X	X	kA	X
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
jsou	být	k5eAaImIp3nP	být
častější	častý	k2eAgMnPc1d2	častější
než	než	k8xS	než
porucha	porucha	k1gFnSc1	porucha
pro	pro	k7c4	pro
vidění	vidění	k1gNnSc4	vidění
modré	modrý	k2eAgFnSc2d1	modrá
barvy	barva	k1gFnSc2	barva
<g/>
.	.	kIx.	.
</s>
<s>
Typy	typ	k1gInPc1	typ
poruch	porucha	k1gFnPc2	porucha
barvocitu	barvocit	k1gInSc2	barvocit
jsou	být	k5eAaImIp3nP	být
<g/>
:	:	kIx,	:
Protanopie	Protanopie	k1gFnPc4	Protanopie
je	být	k5eAaImIp3nS	být
porucha	porucha	k1gFnSc1	porucha
barvocitu	barvocit	k1gInSc2	barvocit
<g/>
,	,	kIx,	,
při	při	k7c6	při
které	který	k3yRgFnSc6	který
postižený	postižený	k1gMnSc1	postižený
nevnímá	vnímat	k5eNaImIp3nS	vnímat
červenou	červený	k2eAgFnSc4d1	červená
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Částečná	částečný	k2eAgFnSc1d1	částečná
porucha	porucha	k1gFnSc1	porucha
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
protanomalie	protanomalie	k1gFnSc1	protanomalie
<g/>
.	.	kIx.	.
</s>
<s>
Deuteranopie	Deuteranopie	k1gFnSc1	Deuteranopie
–	–	k?	–
postižený	postižený	k1gMnSc1	postižený
nevnímá	vnímat	k5eNaImIp3nS	vnímat
zelenou	zelený	k2eAgFnSc4d1	zelená
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Částečná	částečný	k2eAgFnSc1d1	částečná
porucha	porucha	k1gFnSc1	porucha
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
deuteranomalie	deuteranomalie	k1gFnSc1	deuteranomalie
<g/>
.	.	kIx.	.
</s>
<s>
Tritanopie	Tritanopie	k1gFnSc1	Tritanopie
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
postižený	postižený	k1gMnSc1	postižený
nevnímá	vnímat	k5eNaImIp3nS	vnímat
modrou	modrý	k2eAgFnSc4d1	modrá
barvu	barva	k1gFnSc4	barva
<g/>
.	.	kIx.	.
</s>
<s>
Částečná	částečný	k2eAgFnSc1d1	částečná
porucha	porucha	k1gFnSc1	porucha
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
tritanomalie	tritanomalie	k1gFnSc1	tritanomalie
<g/>
.	.	kIx.	.
</s>
<s>
Barvoslepost	barvoslepost	k1gFnSc1	barvoslepost
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
odhalit	odhalit	k5eAaPmF	odhalit
pomocí	pomocí	k7c2	pomocí
speciálních	speciální	k2eAgNnPc2d1	speciální
schémat	schéma	k1gNnPc2	schéma
<g/>
.	.	kIx.	.
</s>
<s>
Pseudoizochromatické	Pseudoizochromatický	k2eAgFnPc1d1	Pseudoizochromatický
tabulky	tabulka	k1gFnPc1	tabulka
slouží	sloužit	k5eAaImIp3nP	sloužit
ke	k	k7c3	k
screeningovému	screeningový	k2eAgNnSc3d1	screeningové
vyšetření	vyšetření	k1gNnSc3	vyšetření
barvocitu	barvocit	k1gInSc2	barvocit
<g/>
.	.	kIx.	.
</s>
<s>
Tabulky	tabulka	k1gFnPc1	tabulka
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
body	bod	k1gInPc4	bod
různých	různý	k2eAgFnPc2d1	různá
barev	barva	k1gFnPc2	barva
a	a	k8xC	a
různého	různý	k2eAgInSc2d1	různý
jasu	jas	k1gInSc2	jas
<g/>
.	.	kIx.	.
</s>
<s>
Barevné	barevný	k2eAgInPc1d1	barevný
body	bod	k1gInPc1	bod
vytvářejí	vytvářet	k5eAaImIp3nP	vytvářet
určité	určitý	k2eAgFnPc4d1	určitá
číslice	číslice	k1gFnPc4	číslice
<g/>
,	,	kIx,	,
písmena	písmeno	k1gNnPc4	písmeno
či	či	k8xC	či
geometrické	geometrický	k2eAgInPc4d1	geometrický
tvary	tvar	k1gInPc4	tvar
na	na	k7c6	na
pozadí	pozadí	k1gNnSc6	pozadí
odlišně	odlišně	k6eAd1	odlišně
zbarvených	zbarvený	k2eAgInPc2d1	zbarvený
bodů	bod	k1gInPc2	bod
<g/>
.	.	kIx.	.
</s>
<s>
Osoba	osoba	k1gFnSc1	osoba
s	s	k7c7	s
porušeným	porušený	k2eAgInSc7d1	porušený
barvocitem	barvocit	k1gInSc7	barvocit
není	být	k5eNaImIp3nS	být
schopna	schopen	k2eAgFnSc1d1	schopna
znaky	znak	k1gInPc4	znak
úspěšně	úspěšně	k6eAd1	úspěšně
identifikovat	identifikovat	k5eAaBmF	identifikovat
<g/>
.	.	kIx.	.
</s>
