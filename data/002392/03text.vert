<s>
Publius	Publius	k1gMnSc1	Publius
Aelius	Aelius	k1gMnSc1	Aelius
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
(	(	kIx(	(
<g/>
24	[number]	k4	24
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
76	[number]	k4	76
v	v	k7c6	v
Italice	italika	k1gFnSc6	italika
-	-	kIx~	-
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
138	[number]	k4	138
nedaleko	nedaleko	k7c2	nedaleko
Neapole	Neapol	k1gFnSc2	Neapol
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
běžně	běžně	k6eAd1	běžně
známý	známý	k2eAgMnSc1d1	známý
jako	jako	k8xC	jako
Hadrianus	Hadrianus	k1gInSc1	Hadrianus
(	(	kIx(	(
<g/>
česky	česky	k6eAd1	česky
Hadrián	Hadrián	k1gMnSc1	Hadrián
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
římský	římský	k2eAgMnSc1d1	římský
císař	císař	k1gMnSc1	císař
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
117	[number]	k4	117
až	až	k9	až
138	[number]	k4	138
<g/>
.	.	kIx.	.
</s>
<s>
Náležel	náležet	k5eAaImAgMnS	náležet
k	k	k7c3	k
tzv.	tzv.	kA	tzv.
adoptivním	adoptivní	k2eAgMnPc3d1	adoptivní
císařům	císař	k1gMnPc3	císař
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nimiž	jenž	k3xRgMnPc7	jenž
vládl	vládnout	k5eAaImAgMnS	vládnout
jako	jako	k9	jako
třetí	třetí	k4xOgMnSc1	třetí
v	v	k7c6	v
pořadí	pořadí	k1gNnSc6	pořadí
<g/>
.	.	kIx.	.
</s>
<s>
Hadrianus	Hadrianus	k1gInSc1	Hadrianus
pocházel	pocházet	k5eAaImAgInS	pocházet
z	z	k7c2	z
význačného	význačný	k2eAgInSc2d1	význačný
rodu	rod	k1gInSc2	rod
Aeliů	Aeli	k1gMnPc2	Aeli
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
původně	původně	k6eAd1	původně
sídlil	sídlit	k5eAaImAgInS	sídlit
v	v	k7c6	v
italském	italský	k2eAgInSc6d1	italský
kraji	kraj	k1gInSc6	kraj
Picenum	Picenum	k1gInSc1	Picenum
a	a	k8xC	a
později	pozdě	k6eAd2	pozdě
se	se	k3xPyFc4	se
usadil	usadit	k5eAaPmAgInS	usadit
v	v	k7c6	v
římské	římský	k2eAgFnSc6d1	římská
kolonii	kolonie	k1gFnSc6	kolonie
Italice	italika	k1gFnSc6	italika
v	v	k7c6	v
Hispánii	Hispánie	k1gFnSc6	Hispánie
<g/>
.	.	kIx.	.
</s>
<s>
Hadrianův	Hadrianův	k2eAgMnSc1d1	Hadrianův
předchůdce	předchůdce	k1gMnSc1	předchůdce
Traianus	Traianus	k1gMnSc1	Traianus
byl	být	k5eAaImAgMnS	být
bratrancem	bratranec	k1gMnSc7	bratranec
jeho	on	k3xPp3gMnSc2	on
otce	otec	k1gMnSc2	otec
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
přes	přes	k7c4	přes
hojné	hojný	k2eAgInPc4d1	hojný
náznaky	náznak	k1gInPc4	náznak
přízně	přízně	k6eAd1	přízně
se	se	k3xPyFc4	se
Traianus	Traianus	k1gInSc1	Traianus
zdráhal	zdráhat	k5eAaImAgInS	zdráhat
oficiálně	oficiálně	k6eAd1	oficiálně
určit	určit	k5eAaPmF	určit
Hadriana	Hadriana	k1gFnSc1	Hadriana
za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
následníka	následník	k1gMnSc4	následník
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Pompeii	Pompeie	k1gFnSc3	Pompeie
Plotiny	Plotin	k2eAgFnSc2d1	Plotin
<g/>
,	,	kIx,	,
Traianovy	Traianův	k2eAgFnSc2d1	Traianova
manželky	manželka	k1gFnSc2	manželka
<g/>
,	,	kIx,	,
ho	on	k3xPp3gMnSc4	on
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
císařem	císař	k1gMnSc7	císař
teprve	teprve	k6eAd1	teprve
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
svou	svůj	k3xOyFgFnSc7	svůj
smrtí	smrt	k1gFnSc7	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Plotina	Plotina	k1gFnSc1	Plotina
projevovala	projevovat	k5eAaImAgFnS	projevovat
Hadrianovi	Hadrian	k1gMnSc3	Hadrian
své	svůj	k3xOyFgFnPc4	svůj
sympatie	sympatie	k1gFnPc4	sympatie
<g/>
,	,	kIx,	,
čemuž	což	k3yQnSc3	což
zřejmě	zřejmě	k6eAd1	zřejmě
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
vděčil	vděčit	k5eAaImAgMnS	vděčit
za	za	k7c4	za
svou	svůj	k3xOyFgFnSc4	svůj
vládu	vláda	k1gFnSc4	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Podpora	podpora	k1gFnSc1	podpora
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
Plotiny	Plotina	k1gFnSc2	Plotina
byla	být	k5eAaImAgFnS	být
obecně	obecně	k6eAd1	obecně
považována	považován	k2eAgFnSc1d1	považována
za	za	k7c4	za
příčinu	příčina	k1gFnSc4	příčina
Hadrianova	Hadrianův	k2eAgNnSc2d1	Hadrianovo
nástupnictví	nástupnictví	k1gNnSc2	nástupnictví
<g/>
.	.	kIx.	.
</s>
<s>
Existují	existovat	k5eAaImIp3nP	existovat
ovšem	ovšem	k9	ovšem
důkazy	důkaz	k1gInPc1	důkaz
svědčící	svědčící	k2eAgInPc1d1	svědčící
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
si	se	k3xPyFc3	se
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
vydobyl	vydobýt	k5eAaPmAgMnS	vydobýt
císařskou	císařský	k2eAgFnSc4d1	císařská
hodnost	hodnost	k1gFnSc4	hodnost
svými	svůj	k3xOyFgFnPc7	svůj
zásluhami	zásluha	k1gFnPc7	zásluha
při	při	k7c6	při
správě	správa	k1gFnSc6	správa
a	a	k8xC	a
velení	velení	k1gNnSc6	velení
ještě	ještě	k9	ještě
za	za	k7c2	za
doby	doba	k1gFnSc2	doba
trvání	trvání	k1gNnSc2	trvání
Traianova	Traianův	k2eAgInSc2d1	Traianův
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Mezi	mezi	k7c7	mezi
léty	léto	k1gNnPc7	léto
100	[number]	k4	100
až	až	k8xS	až
108	[number]	k4	108
podal	podat	k5eAaPmAgInS	podat
Traianus	Traianus	k1gInSc4	Traianus
několik	několik	k4yIc4	několik
veřejných	veřejný	k2eAgInPc2d1	veřejný
dokladů	doklad	k1gInPc2	doklad
své	svůj	k3xOyFgFnSc2	svůj
osobní	osobní	k2eAgFnSc2d1	osobní
náklonnosti	náklonnost	k1gFnSc2	náklonnost
vůči	vůči	k7c3	vůči
Hadrianovi	Hadrian	k1gMnSc3	Hadrian
<g/>
,	,	kIx,	,
když	když	k8xS	když
ho	on	k3xPp3gMnSc4	on
zasnoubil	zasnoubit	k5eAaPmAgMnS	zasnoubit
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
praneteří	praneteř	k1gFnSc7	praneteř
Vibií	Vibie	k1gFnSc7	Vibie
Sabinou	Sabina	k1gFnSc7	Sabina
<g/>
,	,	kIx,	,
zařadil	zařadit	k5eAaPmAgMnS	zařadit
ho	on	k3xPp3gMnSc4	on
mezi	mezi	k7c4	mezi
svůj	svůj	k3xOyFgInSc4	svůj
doprovod	doprovod	k1gInSc4	doprovod
<g/>
,	,	kIx,	,
obdaroval	obdarovat	k5eAaPmAgMnS	obdarovat
ho	on	k3xPp3gInSc4	on
diamantovým	diamantový	k2eAgInSc7d1	diamantový
prstenem	prsten	k1gInSc7	prsten
od	od	k7c2	od
Nervy	nerv	k1gInPc7	nerv
<g/>
,	,	kIx,	,
což	což	k3yRnSc4	což
si	se	k3xPyFc3	se
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
vykládal	vykládat	k5eAaImAgMnS	vykládat
jako	jako	k8xC	jako
znamení	znamení	k1gNnSc4	znamení
příslibu	příslib	k1gInSc2	příslib
následnictví	následnictví	k1gNnSc2	následnictví
<g/>
,	,	kIx,	,
doporučil	doporučit	k5eAaPmAgInS	doporučit
ho	on	k3xPp3gMnSc4	on
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
konzula	konzul	k1gMnSc2	konzul
(	(	kIx(	(
<g/>
consul	consul	k1gInSc1	consul
suffectus	suffectus	k1gInSc1	suffectus
<g/>
)	)	kIx)	)
a	a	k8xC	a
zahrnul	zahrnout	k5eAaPmAgInS	zahrnout
ho	on	k3xPp3gMnSc4	on
četnými	četný	k2eAgFnPc7d1	četná
jinými	jiný	k2eAgFnPc7d1	jiná
poctami	pocta	k1gFnPc7	pocta
a	a	k8xC	a
dary	dar	k1gInPc7	dar
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
Hadriana	Hadriana	k1gFnSc1	Hadriana
s	s	k7c7	s
Traianem	Traian	k1gInSc7	Traian
poutal	poutat	k5eAaImAgInS	poutat
příbuzenský	příbuzenský	k2eAgInSc1d1	příbuzenský
svazek	svazek	k1gInSc1	svazek
<g/>
,	,	kIx,	,
rozhodující	rozhodující	k2eAgInSc1d1	rozhodující
vliv	vliv	k1gInSc1	vliv
na	na	k7c4	na
jeho	jeho	k3xOp3gFnSc4	jeho
budoucnost	budoucnost	k1gFnSc4	budoucnost
měli	mít	k5eAaImAgMnP	mít
patrně	patrně	k6eAd1	patrně
Plotina	Plotin	k2eAgNnPc1d1	Plotin
a	a	k8xC	a
Lucius	Lucius	k1gMnSc1	Lucius
Licinius	Licinius	k1gMnSc1	Licinius
Sura	Sura	k1gMnSc1	Sura
<g/>
,	,	kIx,	,
Traianův	Traianův	k2eAgMnSc1d1	Traianův
blízký	blízký	k2eAgMnSc1d1	blízký
přítel	přítel	k1gMnSc1	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Hadrianovy	Hadrianův	k2eAgInPc1d1	Hadrianův
vztahy	vztah	k1gInPc1	vztah
se	s	k7c7	s
senátem	senát	k1gInSc7	senát
se	se	k3xPyFc4	se
vyznačovaly	vyznačovat	k5eAaImAgFnP	vyznačovat
určitým	určitý	k2eAgNnSc7d1	určité
napětím	napětí	k1gNnSc7	napětí
vyvolaným	vyvolaný	k2eAgNnSc7d1	vyvolané
popravou	poprava	k1gFnSc7	poprava
čtyř	čtyři	k4xCgMnPc2	čtyři
předních	přední	k2eAgMnPc2d1	přední
konzulárů	konzulár	k1gMnPc2	konzulár
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
údajně	údajně	k6eAd1	údajně
osnovali	osnovat	k5eAaImAgMnP	osnovat
spiknutí	spiknutí	k1gNnSc4	spiknutí
proti	proti	k7c3	proti
císaři	císař	k1gMnSc3	císař
<g/>
.	.	kIx.	.
</s>
<s>
Správní	správní	k2eAgFnPc1d1	správní
reformy	reforma	k1gFnPc1	reforma
provedené	provedený	k2eAgFnPc1d1	provedená
během	během	k7c2	během
jeho	jeho	k3xOp3gNnSc2	jeho
panování	panování	k1gNnSc2	panování
dále	daleko	k6eAd2	daleko
podnítily	podnítit	k5eAaPmAgInP	podnítit
opozici	opozice	k1gFnSc3	opozice
ze	z	k7c2	z
strany	strana	k1gFnSc2	strana
senátorů	senátor	k1gMnPc2	senátor
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
Hadrianus	Hadrianus	k1gInSc1	Hadrianus
uskutečnil	uskutečnit	k5eAaPmAgInS	uskutečnit
reorganizaci	reorganizace	k1gFnSc4	reorganizace
administrativního	administrativní	k2eAgInSc2d1	administrativní
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgMnSc6	jenž
zaujímali	zaujímat	k5eAaImAgMnP	zaujímat
stále	stále	k6eAd1	stále
důležitější	důležitý	k2eAgNnSc4d2	důležitější
postavení	postavení	k1gNnSc4	postavení
zkušení	zkušený	k2eAgMnPc1d1	zkušený
odborníci	odborník	k1gMnPc1	odborník
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
důsledku	důsledek	k1gInSc6	důsledek
toho	ten	k3xDgNnSc2	ten
byl	být	k5eAaImAgInS	být
výkon	výkon	k1gInSc1	výkon
mnoha	mnoho	k4c2	mnoho
oblastí	oblast	k1gFnPc2	oblast
státní	státní	k2eAgFnSc2d1	státní
správy	správa	k1gFnSc2	správa
přenesen	přenést	k5eAaPmNgInS	přenést
do	do	k7c2	do
rukou	ruka	k1gFnPc2	ruka
profesionálních	profesionální	k2eAgMnPc2d1	profesionální
úředníků	úředník	k1gMnPc2	úředník
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
byla	být	k5eAaImAgFnS	být
oslabena	oslabit	k5eAaPmNgFnS	oslabit
pozice	pozice	k1gFnSc1	pozice
senátorských	senátorský	k2eAgFnPc2d1	senátorská
a	a	k8xC	a
aristokratických	aristokratický	k2eAgFnPc2d1	aristokratická
elit	elita	k1gFnPc2	elita
<g/>
.	.	kIx.	.
</s>
<s>
Celá	celý	k2eAgFnSc1d1	celá
řada	řada	k1gFnSc1	řada
antických	antický	k2eAgInPc2d1	antický
pramenů	pramen	k1gInPc2	pramen
uvádí	uvádět	k5eAaImIp3nS	uvádět
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hadrianus	Hadrianus	k1gInSc1	Hadrianus
se	se	k3xPyFc4	se
narodil	narodit	k5eAaPmAgInS	narodit
24	[number]	k4	24
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
76	[number]	k4	76
v	v	k7c6	v
Italice	italika	k1gFnSc6	italika
(	(	kIx(	(
<g/>
poblíž	poblíž	k7c2	poblíž
dnešní	dnešní	k2eAgFnSc2d1	dnešní
Sevilly	Sevilla	k1gFnSc2	Sevilla
<g/>
)	)	kIx)	)
v	v	k7c6	v
provincii	provincie	k1gFnSc6	provincie
Hispania	Hispanium	k1gNnSc2	Hispanium
Baetica	Baetic	k1gInSc2	Baetic
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
v	v	k7c6	v
císařově	císařův	k2eAgInSc6d1	císařův
životopise	životopis	k1gInSc6	životopis
v	v	k7c6	v
Historii	historie	k1gFnSc6	historie
Augustě	Augusta	k1gFnSc6	Augusta
je	být	k5eAaImIp3nS	být
zmíněno	zmínit	k5eAaPmNgNnS	zmínit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hadrianovým	Hadrianův	k2eAgNnSc7d1	Hadrianovo
rodištěm	rodiště	k1gNnSc7	rodiště
byl	být	k5eAaImAgInS	být
Řím	Řím	k1gInSc1	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Hadrianovi	Hadrianův	k2eAgMnPc1d1	Hadrianův
předkové	předek	k1gMnPc1	předek
pocházeli	pocházet	k5eAaImAgMnP	pocházet
z	z	k7c2	z
Hadrie	Hadrie	k1gFnSc2	Hadrie
(	(	kIx(	(
<g/>
Atri	Atr	k1gFnSc2	Atr
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
města	město	k1gNnPc1	město
v	v	k7c6	v
Picenu	Picen	k1gInSc6	Picen
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
se	se	k3xPyFc4	se
přesunuli	přesunout	k5eAaPmAgMnP	přesunout
do	do	k7c2	do
hispánské	hispánský	k2eAgFnSc2d1	hispánská
Italiky	italika	k1gFnSc2	italika
nedlouho	dlouho	k6eNd1	dlouho
po	po	k7c6	po
jejím	její	k3xOp3gNnSc6	její
založení	založení	k1gNnSc6	založení
Scipionem	Scipion	k1gInSc7	Scipion
Africanem	African	k1gMnSc7	African
<g/>
.	.	kIx.	.
</s>
<s>
Hadrianův	Hadrianův	k2eAgMnSc1d1	Hadrianův
děd	děd	k1gMnSc1	děd
Aelius	Aelius	k1gMnSc1	Aelius
Maryllinus	Maryllinus	k1gMnSc1	Maryllinus
byl	být	k5eAaImAgMnS	být
prvním	první	k4xOgMnSc7	první
příslušníkem	příslušník	k1gMnSc7	příslušník
rodu	rod	k1gInSc3	rod
Aeliů	Aeli	k1gMnPc2	Aeli
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
stal	stát	k5eAaPmAgMnS	stát
členem	člen	k1gMnSc7	člen
římského	římský	k2eAgInSc2d1	římský
senátu	senát	k1gInSc2	senát
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc1	jeho
otec	otec	k1gMnSc1	otec
Publius	Publius	k1gMnSc1	Publius
Aelius	Aelius	k1gMnSc1	Aelius
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
Afer	Afer	k1gMnSc1	Afer
byl	být	k5eAaImAgMnS	být
taktéž	taktéž	k?	taktéž
senátorem	senátor	k1gMnSc7	senátor
a	a	k8xC	a
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
hodnosti	hodnost	k1gFnSc3	hodnost
praetora	praetor	k1gMnSc2	praetor
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
strávil	strávit	k5eAaPmAgInS	strávit
většinu	většina	k1gFnSc4	většina
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Aeliové	Aelius	k1gMnPc1	Aelius
byli	být	k5eAaImAgMnP	být
spřízněni	spřízněn	k2eAgMnPc1d1	spřízněn
s	s	k7c7	s
významnými	významný	k2eAgFnPc7d1	významná
aristokratickými	aristokratický	k2eAgFnPc7d1	aristokratická
rodinami	rodina	k1gFnPc7	rodina
<g/>
.	.	kIx.	.
</s>
<s>
Afer	Afer	k1gInSc1	Afer
byl	být	k5eAaImAgInS	být
z	z	k7c2	z
matčiny	matčin	k2eAgFnSc2d1	matčina
strany	strana	k1gFnSc2	strana
bratrancem	bratranec	k1gMnSc7	bratranec
Traiana	Traian	k1gMnSc2	Traian
<g/>
.	.	kIx.	.
</s>
<s>
Domitia	Domitia	k1gFnSc1	Domitia
Paulina	Paulin	k2eAgFnSc1d1	Paulina
<g/>
,	,	kIx,	,
Hadrianova	Hadrianův	k2eAgFnSc1d1	Hadrianova
matka	matka	k1gFnSc1	matka
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
dcerou	dcera	k1gFnSc7	dcera
hispano-římského	hispano-římský	k2eAgMnSc2d1	hispano-římský
senátora	senátor	k1gMnSc2	senátor
z	z	k7c2	z
města	město	k1gNnSc2	město
Gades	Gades	k1gInSc1	Gades
(	(	kIx(	(
<g/>
Cádiz	Cádiz	k1gInSc1	Cádiz
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Hadrianova	Hadrianův	k2eAgFnSc1d1	Hadrianova
sestra	sestra	k1gFnSc1	sestra
Aelia	Aelia	k1gFnSc1	Aelia
Domitia	Domitia	k1gFnSc1	Domitia
Paulina	Paulin	k2eAgFnSc1d1	Paulina
byla	být	k5eAaImAgFnS	být
provdána	provdán	k2eAgFnSc1d1	provdána
za	za	k7c2	za
trojnásobného	trojnásobný	k2eAgMnSc2d1	trojnásobný
konzula	konzul	k1gMnSc2	konzul
Lucia	Lucius	k1gMnSc2	Lucius
Julia	Julius	k1gMnSc2	Julius
Ursa	Ursus	k1gMnSc2	Ursus
Serviana	Servian	k1gMnSc2	Servian
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yQnSc4	co
Afer	Afer	k1gMnSc1	Afer
v	v	k7c6	v
roce	rok	k1gInSc6	rok
86	[number]	k4	86
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
,	,	kIx,	,
byli	být	k5eAaImAgMnP	být
Traianus	Traianus	k1gMnSc1	Traianus
a	a	k8xC	a
jezdec	jezdec	k1gMnSc1	jezdec
Publius	Publius	k1gMnSc1	Publius
Acilius	Acilius	k1gMnSc1	Acilius
Attianus	Attianus	k1gMnSc1	Attianus
ustaveni	ustavit	k5eAaPmNgMnP	ustavit
Hadrianovými	Hadrianův	k2eAgMnPc7d1	Hadrianův
poručníky	poručník	k1gMnPc7	poručník
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
věku	věk	k1gInSc6	věk
čtrnácti	čtrnáct	k4xCc2	čtrnáct
let	léto	k1gNnPc2	léto
navštívil	navštívit	k5eAaPmAgMnS	navštívit
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
Italiku	italika	k1gFnSc4	italika
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
pobyl	pobýt	k5eAaPmAgMnS	pobýt
sotva	sotva	k6eAd1	sotva
jeden	jeden	k4xCgInSc4	jeden
rok	rok	k1gInSc4	rok
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
byl	být	k5eAaImAgMnS	být
pozván	pozvat	k5eAaPmNgMnS	pozvat
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
Traianem	Traian	k1gInSc7	Traian
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
zastávajícím	zastávající	k2eAgInSc7d1	zastávající
konzulát	konzulát	k1gInSc4	konzulát
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
Italiky	italika	k1gFnSc2	italika
se	se	k3xPyFc4	se
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
již	již	k6eAd1	již
nikdy	nikdy	k6eAd1	nikdy
nevrátil	vrátit	k5eNaPmAgMnS	vrátit
<g/>
,	,	kIx,	,
později	pozdě	k6eAd2	pozdě
ji	on	k3xPp3gFnSc4	on
ale	ale	k9	ale
povýšil	povýšit	k5eAaPmAgMnS	povýšit
na	na	k7c4	na
kolonii	kolonie	k1gFnSc4	kolonie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
svých	svůj	k3xOyFgFnPc2	svůj
školních	školní	k2eAgFnPc2d1	školní
studií	studie	k1gFnPc2	studie
se	se	k3xPyFc4	se
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
velice	velice	k6eAd1	velice
podrobně	podrobně	k6eAd1	podrobně
zajímal	zajímat	k5eAaImAgInS	zajímat
o	o	k7c4	o
řeckou	řecký	k2eAgFnSc4d1	řecká
literaturu	literatura	k1gFnSc4	literatura
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
čemuž	což	k3yQnSc3	což
mu	on	k3xPp3gNnSc3	on
někteří	některý	k3yIgMnPc1	některý
lidé	člověk	k1gMnPc1	člověk
přezdívali	přezdívat	k5eAaImAgMnP	přezdívat
"	"	kIx"	"
<g/>
Řekáček	Řekáček	k1gInSc4	Řekáček
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
Graeculus	Graeculus	k1gInSc1	Graeculus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
završení	završení	k1gNnSc6	završení
vzdělávání	vzdělávání	k1gNnSc2	vzdělávání
započal	započnout	k5eAaPmAgInS	započnout
v	v	k7c6	v
roce	rok	k1gInSc6	rok
94	[number]	k4	94
svoji	svůj	k3xOyFgFnSc4	svůj
veřejnou	veřejný	k2eAgFnSc4d1	veřejná
dráhu	dráha	k1gFnSc4	dráha
(	(	kIx(	(
<g/>
cursus	cursus	k1gInSc1	cursus
honorum	honorum	k1gInSc1	honorum
<g/>
)	)	kIx)	)
působením	působení	k1gNnSc7	působení
ve	v	k7c6	v
sboru	sbor	k1gInSc6	sbor
vigintisexvirů	vigintisexvir	k1gInPc2	vigintisexvir
<g/>
.	.	kIx.	.
</s>
<s>
Následně	následně	k6eAd1	následně
vykonával	vykonávat	k5eAaImAgInS	vykonávat
hodnost	hodnost	k1gFnSc4	hodnost
vojenského	vojenský	k2eAgMnSc2d1	vojenský
tribuna	tribun	k1gMnSc2	tribun
v	v	k7c6	v
legii	legie	k1gFnSc6	legie
II	II	kA	II
Adiutrix	Adiutrix	k1gInSc1	Adiutrix
<g/>
,	,	kIx,	,
umístěné	umístěný	k2eAgInPc1d1	umístěný
ve	v	k7c6	v
městě	město	k1gNnSc6	město
Aquincum	Aquincum	k1gInSc1	Aquincum
(	(	kIx(	(
<g/>
Budapešť	Budapešť	k1gFnSc1	Budapešť
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Nato	nato	k6eAd1	nato
byl	být	k5eAaImAgInS	být
převelen	převelet	k5eAaPmNgInS	převelet
k	k	k7c3	k
legio	legio	k6eAd1	legio
V	v	k7c6	v
Macedonica	Macedonicum	k1gNnPc4	Macedonicum
na	na	k7c6	na
dolním	dolní	k2eAgInSc6d1	dolní
Dunaji	Dunaj	k1gInSc6	Dunaj
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Nervově	nervově	k6eAd1	nervově
smrti	smrt	k1gFnSc6	smrt
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
roku	rok	k1gInSc2	rok
98	[number]	k4	98
Hadrianus	Hadrianus	k1gInSc1	Hadrianus
spěšně	spěšně	k6eAd1	spěšně
odcestoval	odcestovat	k5eAaPmAgInS	odcestovat
za	za	k7c7	za
Traianem	Traian	k1gInSc7	Traian
do	do	k7c2	do
Horní	horní	k2eAgFnSc2d1	horní
Germánie	Germánie	k1gFnSc2	Germánie
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
ho	on	k3xPp3gMnSc4	on
osobně	osobně	k6eAd1	osobně
informoval	informovat	k5eAaBmAgMnS	informovat
o	o	k7c6	o
jeho	jeho	k3xOp3gInSc6	jeho
nástupu	nástup	k1gInSc6	nástup
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Zde	zde	k6eAd1	zde
setrval	setrvat	k5eAaPmAgMnS	setrvat
jako	jako	k9	jako
tribun	tribun	k1gMnSc1	tribun
u	u	k7c2	u
legie	legie	k1gFnSc2	legie
XXII	XXII	kA	XXII
Primigenia	Primigenium	k1gNnSc2	Primigenium
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
byla	být	k5eAaImAgFnS	být
posádkou	posádka	k1gFnSc7	posádka
v	v	k7c6	v
Moguntiacu	Moguntiacus	k1gInSc6	Moguntiacus
(	(	kIx(	(
<g/>
Mohuč	Mohuč	k1gFnSc1	Mohuč
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
101	[number]	k4	101
byl	být	k5eAaImAgInS	být
na	na	k7c4	na
přímluvu	přímluva	k1gFnSc4	přímluva
císaře	císař	k1gMnSc2	císař
určen	určit	k5eAaPmNgMnS	určit
za	za	k7c4	za
quaestora	quaestor	k1gMnSc4	quaestor
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
první	první	k4xOgFnSc2	první
dácké	dácký	k2eAgFnSc2d1	dácká
války	válka	k1gFnSc2	válka
Hadrianus	Hadrianus	k1gInSc1	Hadrianus
doprovázel	doprovázet	k5eAaImAgMnS	doprovázet
Traiana	Traian	k1gMnSc4	Traian
jako	jako	k9	jako
comes	comes	k1gInSc4	comes
Augusti	august	k1gMnPc1	august
do	do	k7c2	do
Dácie	Dácie	k1gFnSc2	Dácie
<g/>
,	,	kIx,	,
nezůstal	zůstat	k5eNaPmAgInS	zůstat
ovšem	ovšem	k9	ovšem
po	po	k7c6	po
jeho	jeho	k3xOp3gInSc6	jeho
boku	bok	k1gInSc6	bok
po	po	k7c4	po
celé	celý	k2eAgNnSc4d1	celé
trvání	trvání	k1gNnSc4	trvání
kampaně	kampaň	k1gFnSc2	kampaň
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
se	se	k3xPyFc4	se
odebral	odebrat	k5eAaPmAgMnS	odebrat
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
ucházel	ucházet	k5eAaImAgInS	ucházet
o	o	k7c4	o
úřad	úřad	k1gInSc4	úřad
tribuna	tribun	k1gMnSc2	tribun
lidu	lid	k1gInSc2	lid
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
vypuknutí	vypuknutí	k1gNnSc6	vypuknutí
druhé	druhý	k4xOgFnSc2	druhý
dácké	dácký	k2eAgFnSc2d1	dácká
války	válka	k1gFnSc2	válka
byl	být	k5eAaImAgMnS	být
Traianovi	Traian	k1gMnSc3	Traian
znovu	znovu	k6eAd1	znovu
nablízku	nablízku	k6eAd1	nablízku
<g/>
,	,	kIx,	,
tentokrát	tentokrát	k6eAd1	tentokrát
jako	jako	k9	jako
legát	legát	k1gMnSc1	legát
legio	legio	k1gMnSc1	legio
I	i	k8xC	i
Minervia	Minervia	k1gFnSc1	Minervia
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
ukončení	ukončení	k1gNnSc6	ukončení
konfliktu	konflikt	k1gInSc2	konflikt
velel	velet	k5eAaImAgMnS	velet
legii	legie	k1gFnSc4	legie
v	v	k7c6	v
Dolní	dolní	k2eAgFnSc6d1	dolní
Panonii	Panonie	k1gFnSc6	Panonie
a	a	k8xC	a
posléze	posléze	k6eAd1	posléze
byl	být	k5eAaImAgMnS	být
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
místodržitelem	místodržitel	k1gMnSc7	místodržitel
této	tento	k3xDgFnSc2	tento
provincie	provincie	k1gFnSc2	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Klidná	klidný	k2eAgFnSc1d1	klidná
zahraničněpolitická	zahraničněpolitický	k2eAgFnSc1d1	zahraničněpolitická
situace	situace	k1gFnSc1	situace
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
za	za	k7c4	za
Hadrianova	Hadrianův	k2eAgNnPc4d1	Hadrianovo
panování	panování	k1gNnPc4	panování
nijak	nijak	k6eAd1	nijak
zvlášť	zvlášť	k6eAd1	zvlášť
neprověřila	prověřit	k5eNaPmAgFnS	prověřit
jeho	jeho	k3xOp3gFnSc4	jeho
vojenskou	vojenský	k2eAgFnSc4d1	vojenská
zdatnost	zdatnost	k1gFnSc4	zdatnost
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
zkušenosti	zkušenost	k1gFnPc1	zkušenost
s	s	k7c7	s
velením	velení	k1gNnSc7	velení
vojska	vojsko	k1gNnSc2	vojsko
a	a	k8xC	a
prokázané	prokázaný	k2eAgFnSc2d1	prokázaná
řídící	řídící	k2eAgFnSc2d1	řídící
schopnosti	schopnost	k1gFnSc2	schopnost
však	však	k9	však
nasvědčují	nasvědčovat	k5eAaImIp3nP	nasvědčovat
určitému	určitý	k2eAgInSc3d1	určitý
talentu	talent	k1gInSc3	talent
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
strategie	strategie	k1gFnSc2	strategie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
108	[number]	k4	108
byl	být	k5eAaImAgMnS	být
zvolen	zvolit	k5eAaPmNgMnS	zvolit
konzulem	konzul	k1gMnSc7	konzul
(	(	kIx(	(
<g/>
consul	consout	k5eAaPmAgInS	consout
suffectus	suffectus	k1gInSc1	suffectus
<g/>
)	)	kIx)	)
a	a	k8xC	a
někdy	někdy	k6eAd1	někdy
na	na	k7c6	na
přelomu	přelom	k1gInSc6	přelom
let	léto	k1gNnPc2	léto
110	[number]	k4	110
a	a	k8xC	a
111	[number]	k4	111
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgInS	vydat
do	do	k7c2	do
Athén	Athéna	k1gFnPc2	Athéna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
se	se	k3xPyFc4	se
setkal	setkat	k5eAaPmAgMnS	setkat
s	s	k7c7	s
filosofem	filosof	k1gMnSc7	filosof
Epiktétem	Epiktét	k1gMnSc7	Epiktét
<g/>
,	,	kIx,	,
představitelem	představitel	k1gMnSc7	představitel
stoicismu	stoicismus	k1gInSc2	stoicismus
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgInSc7	jenž
ho	on	k3xPp3gMnSc4	on
pojilo	pojit	k5eAaImAgNnS	pojit
hluboké	hluboký	k2eAgNnSc1d1	hluboké
přátelství	přátelství	k1gNnSc1	přátelství
<g/>
.	.	kIx.	.
</s>
<s>
Přímý	přímý	k2eAgInSc4d1	přímý
kontakt	kontakt	k1gInSc4	kontakt
s	s	k7c7	s
helénskou	helénský	k2eAgFnSc7d1	helénská
kulturou	kultura	k1gFnSc7	kultura
na	na	k7c4	na
Hadriana	Hadrian	k1gMnSc4	Hadrian
velmi	velmi	k6eAd1	velmi
zapůsobil	zapůsobit	k5eAaPmAgMnS	zapůsobit
<g/>
.	.	kIx.	.
</s>
<s>
Dokladem	doklad	k1gInSc7	doklad
toho	ten	k3xDgNnSc2	ten
je	být	k5eAaImIp3nS	být
skutečnost	skutečnost	k1gFnSc1	skutečnost
<g/>
,	,	kIx,	,
že	že	k8xS	že
po	po	k7c6	po
vzoru	vzor	k1gInSc6	vzor
řeckých	řecký	k2eAgMnPc2d1	řecký
filozofů	filozof	k1gMnPc2	filozof
nosil	nosit	k5eAaImAgInS	nosit
vous	vous	k1gInSc1	vous
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
bylo	být	k5eAaImAgNnS	být
u	u	k7c2	u
Římanů	Říman	k1gMnPc2	Říman
zcela	zcela	k6eAd1	zcela
neobvyklé	obvyklý	k2eNgNnSc1d1	neobvyklé
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
byl	být	k5eAaImAgInS	být
zvolen	zvolit	k5eAaPmNgInS	zvolit
archóntem	archónt	k1gMnSc7	archónt
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
mu	on	k3xPp3gMnSc3	on
bylo	být	k5eAaImAgNnS	být
uděleno	udělit	k5eAaPmNgNnS	udělit
athénské	athénský	k2eAgNnSc1d1	athénské
občanství	občanství	k1gNnSc1	občanství
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Historie	historie	k1gFnSc2	historie
Augusty	Augusta	k1gMnSc2	Augusta
patřil	patřit	k5eAaImAgMnS	patřit
Hadrianus	Hadrianus	k1gInSc4	Hadrianus
mezi	mezi	k7c4	mezi
Traianovy	Traianův	k2eAgMnPc4d1	Traianův
oblíbence	oblíbenec	k1gMnPc4	oblíbenec
<g/>
.	.	kIx.	.
</s>
<s>
Přesto	přesto	k8xC	přesto
jejich	jejich	k3xOp3gInPc1	jejich
vztahy	vztah	k1gInPc1	vztah
nebyly	být	k5eNaImAgInP	být
vždy	vždy	k6eAd1	vždy
dobré	dobrý	k2eAgInPc1d1	dobrý
<g/>
,	,	kIx,	,
v	v	k7c6	v
čemž	což	k3yRnSc6	což
sehrála	sehrát	k5eAaPmAgFnS	sehrát
svoji	svůj	k3xOyFgFnSc4	svůj
roli	role	k1gFnSc4	role
Traianova	Traianův	k2eAgFnSc1d1	Traianova
slabost	slabost	k1gFnSc1	slabost
vůči	vůči	k7c3	vůči
mladým	mladý	k2eAgMnPc3d1	mladý
chlapcům	chlapec	k1gMnPc3	chlapec
<g/>
,	,	kIx,	,
o	o	k7c6	o
níž	jenž	k3xRgFnSc6	jenž
se	se	k3xPyFc4	se
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
rovněž	rovněž	k9	rovněž
Cassius	Cassius	k1gMnSc1	Cassius
Dio	Dio	k1gMnSc1	Dio
<g/>
.	.	kIx.	.
</s>
<s>
Traiana	Traiana	k1gFnSc1	Traiana
si	se	k3xPyFc3	se
Hadrianus	Hadrianus	k1gInSc4	Hadrianus
opět	opět	k6eAd1	opět
naklonil	naklonit	k5eAaPmAgMnS	naklonit
prostřednictvím	prostřednictvím	k7c2	prostřednictvím
Lucia	Lucius	k1gMnSc2	Lucius
Licinia	Licinium	k1gNnSc2	Licinium
Sury	Sura	k1gMnSc2	Sura
<g/>
,	,	kIx,	,
vlivného	vlivný	k2eAgMnSc2d1	vlivný
a	a	k8xC	a
bohatého	bohatý	k2eAgMnSc2d1	bohatý
senátora	senátor	k1gMnSc2	senátor
původem	původ	k1gInSc7	původ
z	z	k7c2	z
Tarraca	Tarrac	k1gInSc2	Tarrac
(	(	kIx(	(
<g/>
Tarragona	Tarragona	k1gFnSc1	Tarragona
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
který	který	k3yIgMnSc1	který
zemřel	zemřít	k5eAaPmAgMnS	zemřít
patrně	patrně	k6eAd1	patrně
v	v	k7c6	v
roce	rok	k1gInSc6	rok
108	[number]	k4	108
<g/>
.	.	kIx.	.
</s>
<s>
Navíc	navíc	k6eAd1	navíc
byl	být	k5eAaImAgMnS	být
chráněncem	chráněnec	k1gMnSc7	chráněnec
císařovny	císařovna	k1gFnSc2	císařovna
Pompeiy	Pompeia	k1gFnSc2	Pompeia
Plotiny	Plotina	k1gFnSc2	Plotina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
100	[number]	k4	100
upevnil	upevnit	k5eAaPmAgInS	upevnit
Hadrianus	Hadrianus	k1gInSc1	Hadrianus
své	svůj	k3xOyFgNnSc4	svůj
spojení	spojení	k1gNnSc4	spojení
s	s	k7c7	s
císařskou	císařský	k2eAgFnSc7d1	císařská
rodinou	rodina	k1gFnSc7	rodina
svatbou	svatba	k1gFnSc7	svatba
s	s	k7c7	s
Vibií	Vibie	k1gFnSc7	Vibie
Sabinou	Sabina	k1gFnSc7	Sabina
<g/>
,	,	kIx,	,
Traianovou	Traianový	k2eAgFnSc7d1	Traianový
praneteří	praneteř	k1gFnSc7	praneteř
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
tažení	tažení	k1gNnSc2	tažení
proti	proti	k7c3	proti
Parthům	Parth	k1gInPc3	Parth
působil	působit	k5eAaImAgMnS	působit
v	v	k7c6	v
Traianově	Traianův	k2eAgInSc6d1	Traianův
štábu	štáb	k1gInSc6	štáb
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
legáta	legát	k1gMnSc2	legát
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
počáteční	počáteční	k2eAgFnSc6d1	počáteční
vítězné	vítězný	k2eAgFnSc6d1	vítězná
fázi	fáze	k1gFnSc6	fáze
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
ani	ani	k8xC	ani
při	při	k7c6	při
následném	následný	k2eAgNnSc6d1	následné
povstání	povstání	k1gNnSc6	povstání
v	v	k7c6	v
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
nevykonal	vykonat	k5eNaPmAgMnS	vykonat
nic	nic	k6eAd1	nic
zaznamenání	zaznamenání	k1gNnSc4	zaznamenání
hodného	hodný	k2eAgInSc2d1	hodný
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc4	co
byl	být	k5eAaImAgMnS	být
dosavadní	dosavadní	k2eAgMnSc1d1	dosavadní
místodržitel	místodržitel	k1gMnSc1	místodržitel
Sýrie	Sýrie	k1gFnSc2	Sýrie
vyslán	vyslán	k2eAgMnSc1d1	vyslán
uklidnit	uklidnit	k5eAaPmF	uklidnit
opětovně	opětovně	k6eAd1	opětovně
rozjitřené	rozjitřený	k2eAgInPc4d1	rozjitřený
poměry	poměr	k1gInPc4	poměr
v	v	k7c6	v
Dácii	Dácie	k1gFnSc6	Dácie
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgMnS	být
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
jmenován	jmenovat	k5eAaBmNgMnS	jmenovat
na	na	k7c4	na
jeho	jeho	k3xOp3gNnSc4	jeho
místo	místo	k1gNnSc4	místo
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
té	ten	k3xDgFnSc6	ten
době	doba	k1gFnSc6	doba
už	už	k6eAd1	už
vážně	vážně	k6eAd1	vážně
nemocný	nemocný	k2eAgMnSc1d1	nemocný
Traianus	Traianus	k1gMnSc1	Traianus
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
ke	k	k7c3	k
zpáteční	zpáteční	k2eAgFnSc3d1	zpáteční
cestě	cesta	k1gFnSc3	cesta
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
měl	mít	k5eAaImAgMnS	mít
setrvat	setrvat	k5eAaPmF	setrvat
v	v	k7c4	v
Sýrii	Sýrie	k1gFnSc4	Sýrie
a	a	k8xC	a
udržovat	udržovat	k5eAaImF	udržovat
pořádek	pořádek	k1gInSc4	pořádek
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
kilického	kilický	k2eAgInSc2d1	kilický
Selinúntu	Selinúnt	k1gInSc2	Selinúnt
<g/>
,	,	kIx,	,
aniž	aniž	k8xC	aniž
mohl	moct	k5eAaImAgInS	moct
kvůli	kvůli	k7c3	kvůli
zhoršujícímu	zhoršující	k2eAgInSc3d1	zhoršující
se	se	k3xPyFc4	se
zdravotnímu	zdravotní	k2eAgInSc3d1	zdravotní
stavu	stav	k1gInSc3	stav
pokračovat	pokračovat	k5eAaImF	pokračovat
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
byl	být	k5eAaImAgMnS	být
zřejmě	zřejmě	k6eAd1	zřejmě
jediným	jediný	k2eAgMnSc7d1	jediný
možným	možný	k2eAgMnSc7d1	možný
nástupcem	nástupce	k1gMnSc7	nástupce
<g/>
,	,	kIx,	,
Traianus	Traianus	k1gMnSc1	Traianus
ho	on	k3xPp3gMnSc4	on
dosud	dosud	k6eAd1	dosud
neadoptoval	adoptovat	k5eNaPmAgMnS	adoptovat
a	a	k8xC	a
neustavil	ustavit	k5eNaPmAgMnS	ustavit
ho	on	k3xPp3gMnSc4	on
tedy	tedy	k9	tedy
svým	svůj	k3xOyFgMnSc7	svůj
dědicem	dědic	k1gMnSc7	dědic
<g/>
.	.	kIx.	.
</s>
<s>
Teprve	teprve	k6eAd1	teprve
na	na	k7c6	na
smrtelné	smrtelný	k2eAgFnSc6d1	smrtelná
posteli	postel	k1gFnSc6	postel
<g/>
,	,	kIx,	,
ošetřován	ošetřován	k2eAgInSc1d1	ošetřován
Plotinou	Plotina	k1gFnSc7	Plotina
<g/>
,	,	kIx,	,
Hadriana	Hadrian	k1gMnSc4	Hadrian
přijal	přijmout	k5eAaPmAgMnS	přijmout
za	za	k7c4	za
svého	svůj	k3xOyFgMnSc4	svůj
syna	syn	k1gMnSc4	syn
<g/>
.	.	kIx.	.
</s>
<s>
Jelikož	jelikož	k8xS	jelikož
příslušný	příslušný	k2eAgInSc1d1	příslušný
dokument	dokument	k1gInSc1	dokument
byl	být	k5eAaImAgInS	být
podepsán	podepsat	k5eAaPmNgInS	podepsat
Plotinou	Plotina	k1gFnSc7	Plotina
<g/>
,	,	kIx,	,
nabízí	nabízet	k5eAaImIp3nS	nabízet
se	se	k3xPyFc4	se
domněnka	domněnka	k1gFnSc1	domněnka
<g/>
,	,	kIx,	,
že	že	k8xS	že
Traianus	Traianus	k1gInSc1	Traianus
byl	být	k5eAaImAgInS	být
mrtev	mrtev	k2eAgInSc1d1	mrtev
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
stihl	stihnout	k5eAaPmAgMnS	stihnout
Hadriana	Hadrian	k1gMnSc4	Hadrian
skutečně	skutečně	k6eAd1	skutečně
adoptovat	adoptovat	k5eAaPmF	adoptovat
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Traianově	Traianův	k2eAgFnSc6d1	Traianova
smrti	smrt	k1gFnSc6	smrt
zveřejnili	zveřejnit	k5eAaPmAgMnP	zveřejnit
9	[number]	k4	9
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
117	[number]	k4	117
Plotina	Plotin	k2eAgFnSc1d1	Plotin
a	a	k8xC	a
pretoriánský	pretoriánský	k2eAgMnSc1d1	pretoriánský
prefekt	prefekt	k1gMnSc1	prefekt
Publius	Publius	k1gMnSc1	Publius
Acilius	Acilius	k1gMnSc1	Acilius
Attianus	Attianus	k1gMnSc1	Attianus
Hadrianovu	Hadrianův	k2eAgFnSc4d1	Hadrianova
adopci	adopce	k1gFnSc4	adopce
<g/>
.	.	kIx.	.
11	[number]	k4	11
<g/>
.	.	kIx.	.
srpna	srpen	k1gInSc2	srpen
byl	být	k5eAaImAgInS	být
pak	pak	k6eAd1	pak
Hadrianus	Hadrianus	k1gInSc1	Hadrianus
pozdraven	pozdravit	k5eAaPmNgInS	pozdravit
syrskými	syrský	k2eAgFnPc7d1	Syrská
legiemi	legie	k1gFnPc7	legie
jako	jako	k9	jako
císař	císař	k1gMnSc1	císař
a	a	k8xC	a
tento	tento	k3xDgInSc1	tento
den	den	k1gInSc1	den
slavil	slavit	k5eAaImAgInS	slavit
jako	jako	k8xC	jako
okamžik	okamžik	k1gInSc1	okamžik
svého	svůj	k3xOyFgInSc2	svůj
nástupu	nástup	k1gInSc2	nástup
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
(	(	kIx(	(
<g/>
dies	dies	k1gInSc4	dies
imperii	imperie	k1gFnSc4	imperie
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c4	po
uchopení	uchopení	k1gNnSc4	uchopení
moci	moc	k1gFnSc2	moc
si	se	k3xPyFc3	se
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
zabezpečil	zabezpečit	k5eAaPmAgMnS	zabezpečit
podporu	podpora	k1gFnSc4	podpora
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Propustil	propustit	k5eAaPmAgMnS	propustit
Lusia	Lusius	k1gMnSc4	Lusius
Quieta	Quiet	k2eAgMnSc4d1	Quiet
<g/>
,	,	kIx,	,
římského	římský	k2eAgMnSc4d1	římský
vojevůdce	vojevůdce	k1gMnSc4	vojevůdce
berberského	berberský	k2eAgInSc2d1	berberský
původu	původ	k1gInSc2	původ
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
se	se	k3xPyFc4	se
účastnil	účastnit	k5eAaImAgMnS	účastnit
výpravy	výprava	k1gFnSc2	výprava
na	na	k7c4	na
východ	východ	k1gInSc4	východ
a	a	k8xC	a
jehož	jehož	k3xOyRp3gInSc1	jehož
Traianus	Traianus	k1gInSc1	Traianus
určil	určit	k5eAaPmAgInS	určit
za	za	k7c4	za
správce	správce	k1gMnSc4	správce
Judeje	Judea	k1gFnSc2	Judea
<g/>
.	.	kIx.	.
</s>
<s>
Quietova	Quietův	k2eAgFnSc1d1	Quietův
vojenská	vojenský	k2eAgFnSc1d1	vojenská
proslulost	proslulost	k1gFnSc1	proslulost
z	z	k7c2	z
něho	on	k3xPp3gMnSc2	on
dělala	dělat	k5eAaImAgFnS	dělat
Hadrianova	Hadrianův	k2eAgMnSc4d1	Hadrianův
potenciálního	potenciální	k2eAgMnSc4d1	potenciální
soupeře	soupeř	k1gMnSc4	soupeř
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
aklamace	aklamace	k1gFnSc1	aklamace
vojskem	vojsko	k1gNnSc7	vojsko
učinila	učinit	k5eAaImAgFnS	učinit
Hadrianovo	Hadrianův	k2eAgNnSc4d1	Hadrianovo
povýšení	povýšení	k1gNnSc4	povýšení
do	do	k7c2	do
císařské	císařský	k2eAgFnSc2d1	císařská
hodnosti	hodnost	k1gFnSc2	hodnost
nezvratnou	zvratný	k2eNgFnSc7d1	nezvratná
skutečností	skutečnost	k1gFnSc7	skutečnost
<g/>
,	,	kIx,	,
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
respektoval	respektovat	k5eAaImAgMnS	respektovat
tradiční	tradiční	k2eAgFnPc4d1	tradiční
zvyklosti	zvyklost	k1gFnPc4	zvyklost
a	a	k8xC	a
požádal	požádat	k5eAaPmAgMnS	požádat
dopisem	dopis	k1gInSc7	dopis
senát	senát	k1gInSc1	senát
o	o	k7c6	o
potvrzení	potvrzení	k1gNnSc6	potvrzení
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
byly	být	k5eAaImAgFnP	být
senátorům	senátor	k1gMnPc3	senátor
předloženy	předložen	k2eAgFnPc4d1	předložena
písemnosti	písemnost	k1gFnPc4	písemnost
dokládající	dokládající	k2eAgFnSc4d1	dokládající
Hadrianovu	Hadrianův	k2eAgFnSc4d1	Hadrianova
adopci	adopce	k1gFnSc4	adopce
<g/>
,	,	kIx,	,
byla	být	k5eAaImAgFnS	být
i	i	k9	i
jimi	on	k3xPp3gMnPc7	on
jeho	jeho	k3xOp3gInPc6	jeho
vláda	vláda	k1gFnSc1	vláda
formálně	formálně	k6eAd1	formálně
akceptována	akceptován	k2eAgFnSc1d1	akceptována
<g/>
.	.	kIx.	.
</s>
<s>
Zvěsti	zvěst	k1gFnPc1	zvěst
o	o	k7c6	o
nepravosti	nepravost	k1gFnSc6	nepravost
dokumentu	dokument	k1gInSc2	dokument
nebyly	být	k5eNaImAgFnP	být
brány	brána	k1gFnPc1	brána
příliš	příliš	k6eAd1	příliš
vážně	vážně	k6eAd1	vážně
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
legitimita	legitimita	k1gFnSc1	legitimita
Hadrianovy	Hadrianův	k2eAgFnSc2d1	Hadrianova
vlády	vláda	k1gFnSc2	vláda
byla	být	k5eAaImAgFnS	být
odvozena	odvodit	k5eAaPmNgFnS	odvodit
ze	z	k7c2	z
souhlasu	souhlas	k1gInSc2	souhlas
senátu	senát	k1gInSc2	senát
a	a	k8xC	a
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Zaneprázdněn	zaneprázdnit	k5eAaPmNgInS	zaneprázdnit
úpravou	úprava	k1gFnSc7	úprava
administrativních	administrativní	k2eAgFnPc2d1	administrativní
záležitostí	záležitost	k1gFnPc2	záležitost
v	v	k7c6	v
provinciích	provincie	k1gFnPc6	provincie
na	na	k7c6	na
východě	východ	k1gInSc6	východ
a	a	k8xC	a
na	na	k7c6	na
Dunaji	Dunaj	k1gInSc6	Dunaj
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
doznívajícím	doznívající	k2eAgInSc7d1	doznívající
konfliktem	konflikt	k1gInSc7	konflikt
se	s	k7c7	s
Židy	Žid	k1gMnPc7	Žid
nevěnoval	věnovat	k5eNaImAgMnS	věnovat
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
dostatečnou	dostatečný	k2eAgFnSc4d1	dostatečná
pozornost	pozornost	k1gFnSc4	pozornost
konsolidaci	konsolidace	k1gFnSc4	konsolidace
své	svůj	k3xOyFgFnPc4	svůj
moci	moct	k5eAaImF	moct
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Konečně	konečně	k6eAd1	konečně
v	v	k7c6	v
létě	léto	k1gNnSc6	léto
118	[number]	k4	118
dorazil	dorazit	k5eAaPmAgMnS	dorazit
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
dal	dát	k5eAaPmAgMnS	dát
uctít	uctít	k5eAaPmF	uctít
památku	památka	k1gFnSc4	památka
svého	svůj	k3xOyFgMnSc2	svůj
zesnulého	zesnulý	k2eAgMnSc2d1	zesnulý
předchůdce	předchůdce	k1gMnSc2	předchůdce
jeho	jeho	k3xOp3gNnSc7	jeho
prohlášením	prohlášení	k1gNnSc7	prohlášení
za	za	k7c2	za
boha	bůh	k1gMnSc2	bůh
a	a	k8xC	a
uspořádáním	uspořádání	k1gNnSc7	uspořádání
velkolepého	velkolepý	k2eAgInSc2d1	velkolepý
triumfu	triumf	k1gInSc2	triumf
<g/>
.	.	kIx.	.
</s>
<s>
Hadrianův	Hadrianův	k2eAgInSc1d1	Hadrianův
příjezd	příjezd	k1gInSc1	příjezd
byl	být	k5eAaImAgInS	být
zastíněn	zastínit	k5eAaPmNgInS	zastínit
popravou	poprava	k1gFnSc7	poprava
někdejších	někdejší	k2eAgMnPc2d1	někdejší
konzulů	konzul	k1gMnPc2	konzul
Lusia	Lusius	k1gMnSc2	Lusius
Quieta	Quiet	k1gMnSc2	Quiet
<g/>
,	,	kIx,	,
Gaia	Gaius	k1gMnSc2	Gaius
Avidia	Avidium	k1gNnSc2	Avidium
Nigrina	Nigrino	k1gNnSc2	Nigrino
<g/>
,	,	kIx,	,
Aula	aula	k1gFnSc1	aula
Cornelia	Cornelium	k1gNnSc2	Cornelium
Palmy	palma	k1gFnSc2	palma
a	a	k8xC	a
Publilia	Publilius	k1gMnSc2	Publilius
Celsa	Cels	k1gMnSc2	Cels
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
byli	být	k5eAaImAgMnP	být
usmrceni	usmrtit	k5eAaPmNgMnP	usmrtit
za	za	k7c2	za
císařovy	císařův	k2eAgFnSc2d1	císařova
nepřítomnosti	nepřítomnost	k1gFnSc2	nepřítomnost
z	z	k7c2	z
iniciativy	iniciativa	k1gFnSc2	iniciativa
prefekta	prefekt	k1gMnSc2	prefekt
Attiana	Attian	k1gMnSc2	Attian
<g/>
.	.	kIx.	.
</s>
<s>
Příčina	příčina	k1gFnSc1	příčina
tkvěla	tkvět	k5eAaImAgFnS	tkvět
v	v	k7c6	v
domnělém	domnělý	k2eAgNnSc6d1	domnělé
spiknutí	spiknutí	k1gNnSc6	spiknutí
této	tento	k3xDgFnSc2	tento
čtveřice	čtveřice	k1gFnSc2	čtveřice
proti	proti	k7c3	proti
Hadrianovi	Hadrian	k1gMnSc3	Hadrian
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
vždy	vždy	k6eAd1	vždy
zříkal	zříkat	k5eAaImAgMnS	zříkat
odpovědnosti	odpovědnost	k1gFnSc2	odpovědnost
za	za	k7c4	za
jejich	jejich	k3xOp3gFnSc4	jejich
smrt	smrt	k1gFnSc4	smrt
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
jeho	jeho	k3xOp3gInPc1	jeho
vztahy	vztah	k1gInPc1	vztah
se	s	k7c7	s
senátem	senát	k1gInSc7	senát
byly	být	k5eAaImAgFnP	být
touto	tento	k3xDgFnSc7	tento
událostí	událost	k1gFnSc7	událost
vážně	vážně	k6eAd1	vážně
poznamenány	poznamenat	k5eAaPmNgInP	poznamenat
<g/>
.	.	kIx.	.
</s>
<s>
Nepochybně	pochybně	k6eNd1	pochybně
existovalo	existovat	k5eAaImAgNnS	existovat
nepřátelství	nepřátelství	k1gNnSc1	nepřátelství
některých	některý	k3yIgInPc2	některý
kruhů	kruh	k1gInPc2	kruh
vůči	vůči	k7c3	vůči
Hadrianovi	Hadrian	k1gMnSc3	Hadrian
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
brzy	brzy	k6eAd1	brzy
opustil	opustit	k5eAaPmAgMnS	opustit
Traianovu	Traianův	k2eAgFnSc4d1	Traianova
ambiciózní	ambiciózní	k2eAgFnSc4d1	ambiciózní
politiku	politika	k1gFnSc4	politika
expanze	expanze	k1gFnSc2	expanze
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
toho	ten	k3xDgNnSc2	ten
Hadrianovo	Hadrianův	k2eAgNnSc1d1	Hadrianovo
ustavení	ustavení	k1gNnSc1	ustavení
císařem	císař	k1gMnSc7	císař
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
proti	proti	k7c3	proti
němu	on	k3xPp3gNnSc3	on
žárlivost	žárlivost	k1gFnSc1	žárlivost
jiných	jiný	k2eAgMnPc2d1	jiný
možných	možný	k2eAgMnPc2d1	možný
kandidátů	kandidát	k1gMnPc2	kandidát
trůnu	trůn	k1gInSc2	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Všichni	všechen	k3xTgMnPc1	všechen
popravení	popravený	k2eAgMnPc1d1	popravený
konzulárové	konzulár	k1gMnPc1	konzulár
byli	být	k5eAaImAgMnP	být
Traianovými	Traianův	k2eAgMnPc7d1	Traianův
muži	muž	k1gMnPc7	muž
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
jejich	jejich	k3xOp3gNnSc1	jejich
odstranění	odstranění	k1gNnSc1	odstranění
poskytlo	poskytnout	k5eAaPmAgNnS	poskytnout
Hadrianovi	Hadrian	k1gMnSc3	Hadrian
větší	veliký	k2eAgNnSc1d2	veliký
volnost	volnost	k1gFnSc4	volnost
rozhodování	rozhodování	k1gNnSc2	rozhodování
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
odpor	odpor	k1gInSc1	odpor
senátorů	senátor	k1gMnPc2	senátor
za	za	k7c4	za
tento	tento	k3xDgInSc4	tento
čin	čin	k1gInSc4	čin
ho	on	k3xPp3gInSc4	on
pronásledoval	pronásledovat	k5eAaImAgInS	pronásledovat
po	po	k7c4	po
zbytek	zbytek	k1gInSc4	zbytek
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Nelibost	nelibost	k1gFnSc1	nelibost
senátu	senát	k1gInSc2	senát
zvyšovala	zvyšovat	k5eAaImAgFnS	zvyšovat
i	i	k9	i
císařova	císařův	k2eAgFnSc1d1	císařova
snaha	snaha	k1gFnSc1	snaha
získat	získat	k5eAaPmF	získat
principátu	principát	k1gInSc2	principát
širší	široký	k2eAgFnSc4d2	širší
podporu	podpora	k1gFnSc4	podpora
provinciálů	provinciál	k1gMnPc2	provinciál
<g/>
,	,	kIx,	,
čímž	což	k3yQnSc7	což
bylo	být	k5eAaImAgNnS	být
podkopáváno	podkopáván	k2eAgNnSc4d1	podkopáváno
dosavadní	dosavadní	k2eAgNnSc4d1	dosavadní
výsostné	výsostný	k2eAgNnSc4d1	výsostné
postavení	postavení	k1gNnSc4	postavení
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Bar	bar	k1gInSc1	bar
Kochbova	Kochbův	k2eAgNnSc2d1	Kochbovo
povstání	povstání	k1gNnSc2	povstání
byla	být	k5eAaImAgFnS	být
Hadrianova	Hadrianův	k2eAgFnSc1d1	Hadrianova
vláda	vláda	k1gFnSc1	vláda
prosta	prost	k2eAgFnSc1d1	prosta
náročnějších	náročný	k2eAgNnPc2d2	náročnější
vojenských	vojenský	k2eAgNnPc2d1	vojenské
střetnutí	střetnutí	k1gNnPc2	střetnutí
<g/>
.	.	kIx.	.
</s>
<s>
Traianovy	Traianův	k2eAgInPc1d1	Traianův
výboje	výboj	k1gInPc1	výboj
v	v	k7c6	v
Arménii	Arménie	k1gFnSc6	Arménie
a	a	k8xC	a
Mezopotámii	Mezopotámie	k1gFnSc6	Mezopotámie
pokládal	pokládat	k5eAaImAgMnS	pokládat
Hadrianus	Hadrianus	k1gInSc4	Hadrianus
<g/>
,	,	kIx,	,
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nadměrným	nadměrný	k2eAgFnPc3d1	nadměrná
logistickým	logistický	k2eAgFnPc3d1	logistická
těžkostem	těžkost	k1gFnPc3	těžkost
spojeným	spojený	k2eAgFnPc3d1	spojená
se	se	k3xPyFc4	se
stálým	stálý	k2eAgNnSc7d1	stálé
udržováním	udržování	k1gNnSc7	udržování
vojska	vojsko	k1gNnSc2	vojsko
v	v	k7c6	v
těchto	tento	k3xDgFnPc6	tento
odlehlých	odlehlý	k2eAgFnPc6d1	odlehlá
oblastech	oblast	k1gFnPc6	oblast
<g/>
,	,	kIx,	,
za	za	k7c4	za
neudržitelná	udržitelný	k2eNgNnPc4d1	neudržitelné
a	a	k8xC	a
proto	proto	k8xC	proto
je	být	k5eAaImIp3nS	být
už	už	k6eAd1	už
v	v	k7c6	v
úvodu	úvod	k1gInSc6	úvod
svého	svůj	k3xOyFgNnSc2	svůj
panování	panování	k1gNnSc2	panování
nechal	nechat	k5eAaPmAgMnS	nechat
vyklidit	vyklidit	k5eAaPmF	vyklidit
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
zvažoval	zvažovat	k5eAaImAgMnS	zvažovat
ústup	ústup	k1gInSc4	ústup
z	z	k7c2	z
Dácie	Dácie	k1gFnSc2	Dácie
<g/>
.	.	kIx.	.
</s>
<s>
Odstoupení	odstoupení	k1gNnSc1	odstoupení
teritoriálních	teritoriální	k2eAgInPc2d1	teritoriální
zisků	zisk	k1gInPc2	zisk
na	na	k7c6	na
východě	východ	k1gInSc6	východ
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
oponovala	oponovat	k5eAaImAgFnS	oponovat
značná	značný	k2eAgFnSc1d1	značná
část	část	k1gFnSc1	část
armády	armáda	k1gFnSc2	armáda
<g/>
,	,	kIx,	,
bylo	být	k5eAaImAgNnS	být
zaměřeno	zaměřit	k5eAaPmNgNnS	zaměřit
na	na	k7c4	na
vymezení	vymezení	k1gNnSc4	vymezení
přirozených	přirozený	k2eAgInPc2d1	přirozený
<g/>
,	,	kIx,	,
stabilních	stabilní	k2eAgInPc2d1	stabilní
a	a	k8xC	a
snadno	snadno	k6eAd1	snadno
hájitelných	hájitelný	k2eAgFnPc2d1	hájitelná
hranic	hranice	k1gFnPc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Ty	ten	k3xDgInPc1	ten
měly	mít	k5eAaImAgInP	mít
být	být	k5eAaImF	být
vyztuženy	vyztužit	k5eAaPmNgInP	vyztužit
důkladně	důkladně	k6eAd1	důkladně
propracovanými	propracovaný	k2eAgInPc7d1	propracovaný
pohraničními	pohraniční	k2eAgNnPc7d1	pohraniční
opevněními	opevnění	k1gNnPc7	opevnění
<g/>
.	.	kIx.	.
</s>
<s>
Nejproslulejším	proslulý	k2eAgMnSc7d3	nejproslulejší
z	z	k7c2	z
nich	on	k3xPp3gFnPc2	on
se	se	k3xPyFc4	se
stala	stát	k5eAaPmAgFnS	stát
zeď	zeď	k1gFnSc1	zeď
táhnoucí	táhnoucí	k2eAgFnSc1d1	táhnoucí
se	se	k3xPyFc4	se
na	na	k7c6	na
severu	sever	k1gInSc6	sever
Británie	Británie	k1gFnSc2	Británie
mezi	mezi	k7c4	mezi
Solway	Solwaa	k1gFnPc4	Solwaa
Firth	Firtha	k1gFnPc2	Firtha
a	a	k8xC	a
dnešním	dnešní	k2eAgMnSc7d1	dnešní
Newcastlem	Newcastl	k1gMnSc7	Newcastl
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k9	jako
Hadriánův	Hadriánův	k2eAgInSc4d1	Hadriánův
val	val	k1gInSc4	val
<g/>
.	.	kIx.	.
</s>
<s>
Touto	tento	k3xDgFnSc7	tento
obrannou	obranný	k2eAgFnSc7d1	obranná
linií	linie	k1gFnSc7	linie
byla	být	k5eAaImAgFnS	být
římská	římský	k2eAgFnSc1d1	římská
provincie	provincie	k1gFnSc1	provincie
na	na	k7c6	na
jihu	jih	k1gInSc6	jih
izolována	izolovat	k5eAaBmNgFnS	izolovat
od	od	k7c2	od
domorodých	domorodý	k2eAgMnPc2d1	domorodý
Kaledoňanů	Kaledoňan	k1gMnPc2	Kaledoňan
obývajících	obývající	k2eAgMnPc2d1	obývající
sever	sever	k1gInSc4	sever
Velké	velký	k2eAgFnSc2d1	velká
Británie	Británie	k1gFnSc2	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
svém	svůj	k3xOyFgNnSc6	svůj
vybudování	vybudování	k1gNnSc6	vybudování
byl	být	k5eAaImAgInS	být
val	val	k1gInSc1	val
spravován	spravovat	k5eAaImNgInS	spravovat
jedině	jedině	k6eAd1	jedině
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
barbarské	barbarský	k2eAgInPc1d1	barbarský
kmeny	kmen	k1gInPc1	kmen
obzvláště	obzvláště	k6eAd1	obzvláště
intenzivně	intenzivně	k6eAd1	intenzivně
ohrožovaly	ohrožovat	k5eAaImAgFnP	ohrožovat
římské	římský	k2eAgNnSc4d1	římské
území	území	k1gNnSc4	území
<g/>
.	.	kIx.	.
</s>
<s>
Četné	četný	k2eAgFnPc1d1	četná
fortifikace	fortifikace	k1gFnPc1	fortifikace
<g/>
,	,	kIx,	,
pevnosti	pevnost	k1gFnPc1	pevnost
a	a	k8xC	a
předsunuté	předsunutý	k2eAgFnPc1d1	předsunutá
strážní	strážní	k2eAgFnPc1d1	strážní
věže	věž	k1gFnPc1	věž
<g/>
,	,	kIx,	,
zajištěné	zajištěný	k2eAgFnPc1d1	zajištěná
silnicemi	silnice	k1gFnPc7	silnice
a	a	k8xC	a
obsazené	obsazený	k2eAgFnPc1d1	obsazená
posádkami	posádka	k1gFnPc7	posádka
<g/>
,	,	kIx,	,
střežily	střežit	k5eAaImAgInP	střežit
rovněž	rovněž	k9	rovněž
hranici	hranice	k1gFnSc4	hranice
na	na	k7c6	na
Rýně	Rýn	k1gInSc6	Rýn
a	a	k8xC	a
Dunaji	Dunaj	k1gInSc6	Dunaj
(	(	kIx(	(
<g/>
Limes	Limes	k1gMnSc1	Limes
Romanus	Romanus	k1gMnSc1	Romanus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
se	se	k3xPyFc4	se
snažil	snažit	k5eAaImAgMnS	snažit
zpevnit	zpevnit	k5eAaPmF	zpevnit
hranice	hranice	k1gFnSc2	hranice
také	také	k9	také
diplomatickými	diplomatický	k2eAgInPc7d1	diplomatický
prostředky	prostředek	k1gInPc7	prostředek
<g/>
,	,	kIx,	,
tudíž	tudíž	k8xC	tudíž
uzavíral	uzavírat	k5eAaPmAgMnS	uzavírat
spojenecké	spojenecký	k2eAgFnPc4d1	spojenecká
smlouvy	smlouva	k1gFnPc4	smlouva
s	s	k7c7	s
kmeny	kmen	k1gInPc7	kmen
žijícími	žijící	k2eAgInPc7d1	žijící
mimo	mimo	k7c4	mimo
hranice	hranice	k1gFnPc4	hranice
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Do	do	k7c2	do
oblastí	oblast	k1gFnPc2	oblast
nejvíce	hodně	k6eAd3	hodně
vystavených	vystavený	k2eAgFnPc2d1	vystavená
nepřátelským	přátelský	k2eNgNnSc7d1	nepřátelské
nájezdům	nájezd	k1gInPc3	nájezd
byly	být	k5eAaImAgFnP	být
vysílány	vysílán	k2eAgFnPc1d1	vysílána
detašované	detašovaný	k2eAgFnPc1d1	detašovaná
kohorty	kohorta	k1gFnPc1	kohorta
<g/>
,	,	kIx,	,
označované	označovaný	k2eAgFnPc1d1	označovaná
jako	jako	k8xS	jako
vexilace	vexilace	k1gFnPc1	vexilace
(	(	kIx(	(
<g/>
vexillationes	vexillationes	k1gInSc1	vexillationes
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Tímto	tento	k3xDgInSc7	tento
způsobem	způsob	k1gInSc7	způsob
byla	být	k5eAaImAgFnS	být
zabezpečována	zabezpečován	k2eAgFnSc1d1	zabezpečována
flexibilita	flexibilita	k1gFnSc1	flexibilita
nezbytná	zbytný	k2eNgFnSc1d1	zbytný
při	při	k7c6	při
obraně	obrana	k1gFnSc6	obrana
hranic	hranice	k1gFnPc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Vedlejším	vedlejší	k2eAgInSc7d1	vedlejší
důsledkem	důsledek	k1gInSc7	důsledek
vexilací	vexilace	k1gFnPc2	vexilace
byla	být	k5eAaImAgFnS	být
stabilizace	stabilizace	k1gFnSc1	stabilizace
rozložení	rozložení	k1gNnSc2	rozložení
legií	legie	k1gFnPc2	legie
podél	podél	k7c2	podél
hranic	hranice	k1gFnPc2	hranice
a	a	k8xC	a
těsnější	těsný	k2eAgInSc4d2	těsnější
kontakt	kontakt	k1gInSc4	kontakt
mezi	mezi	k7c7	mezi
vojáky	voják	k1gMnPc7	voják
a	a	k8xC	a
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
zázemí	zázemí	k1gNnSc2	zázemí
vojenských	vojenský	k2eAgInPc2d1	vojenský
táborů	tábor	k1gInPc2	tábor
<g/>
.	.	kIx.	.
</s>
<s>
To	ten	k3xDgNnSc1	ten
se	se	k3xPyFc4	se
často	často	k6eAd1	často
skládalo	skládat	k5eAaImAgNnS	skládat
z	z	k7c2	z
veteránů	veterán	k1gMnPc2	veterán
usazených	usazený	k2eAgMnPc2d1	usazený
v	v	k7c6	v
koloniích	kolonie	k1gFnPc6	kolonie
<g/>
,	,	kIx,	,
kteří	který	k3yQgMnPc1	který
napomáhali	napomáhat	k5eAaBmAgMnP	napomáhat
udržování	udržování	k1gNnSc4	udržování
pořádku	pořádek	k1gInSc2	pořádek
v	v	k7c6	v
okolních	okolní	k2eAgInPc6d1	okolní
krajích	kraj	k1gInPc6	kraj
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
svých	svůj	k3xOyFgFnPc6	svůj
opakovaných	opakovaný	k2eAgFnPc6d1	opakovaná
osobních	osobní	k2eAgFnPc6d1	osobní
inspekcích	inspekce	k1gFnPc6	inspekce
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
velice	velice	k6eAd1	velice
dbal	dbát	k5eAaImAgMnS	dbát
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
disciplína	disciplína	k1gFnSc1	disciplína
<g/>
,	,	kIx,	,
bojeschopnost	bojeschopnost	k1gFnSc1	bojeschopnost
a	a	k8xC	a
morálka	morálka	k1gFnSc1	morálka
mužstva	mužstvo	k1gNnSc2	mužstvo
byly	být	k5eAaImAgFnP	být
zachovány	zachován	k2eAgMnPc4d1	zachován
intenzivním	intenzivní	k2eAgInSc7d1	intenzivní
vojenským	vojenský	k2eAgInSc7d1	vojenský
drilem	dril	k1gInSc7	dril
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
měl	mít	k5eAaImAgInS	mít
zamezit	zamezit	k5eAaPmF	zamezit
propuknutí	propuknutí	k1gNnSc4	propuknutí
neklidu	neklid	k1gInSc2	neklid
<g/>
.	.	kIx.	.
</s>
<s>
Sám	sám	k3xTgMnSc1	sám
šel	jít	k5eAaImAgMnS	jít
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
ohledu	ohled	k1gInSc6	ohled
příkladem	příklad	k1gInSc7	příklad
<g/>
,	,	kIx,	,
když	když	k8xS	když
neváhal	váhat	k5eNaImAgMnS	váhat
snášet	snášet	k5eAaImF	snášet
stejné	stejný	k2eAgFnPc4d1	stejná
životní	životní	k2eAgFnPc4d1	životní
podmínky	podmínka	k1gFnPc4	podmínka
a	a	k8xC	a
jíst	jíst	k5eAaImF	jíst
tutéž	týž	k3xTgFnSc4	týž
stravu	strava	k1gFnSc4	strava
jako	jako	k8xC	jako
řadový	řadový	k2eAgMnSc1d1	řadový
legionář	legionář	k1gMnSc1	legionář
<g/>
.	.	kIx.	.
</s>
<s>
Všechna	všechen	k3xTgNnPc1	všechen
tato	tento	k3xDgNnPc1	tento
Hadrianova	Hadrianův	k2eAgNnPc1d1	Hadrianovo
opatření	opatření	k1gNnPc1	opatření
směřovala	směřovat	k5eAaImAgNnP	směřovat
k	k	k7c3	k
prosazení	prosazení	k1gNnSc3	prosazení
nové	nový	k2eAgFnSc2d1	nová
defenzivní	defenzivní	k2eAgFnSc2d1	defenzivní
strategické	strategický	k2eAgFnSc2d1	strategická
doktríny	doktrína	k1gFnSc2	doktrína
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gInSc7	jejíž
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
dosažení	dosažení	k1gNnSc1	dosažení
maximální	maximální	k2eAgFnSc2d1	maximální
vojenské	vojenský	k2eAgFnSc2d1	vojenská
efektivnosti	efektivnost	k1gFnSc2	efektivnost
ve	v	k7c6	v
všech	všecek	k3xTgFnPc6	všecek
okrajových	okrajový	k2eAgFnPc6d1	okrajová
částech	část	k1gFnPc6	část
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
Hadrianova	Hadrianův	k2eAgNnSc2d1	Hadrianovo
panování	panování	k1gNnSc2	panování
nenastala	nastat	k5eNaPmAgFnS	nastat
žádná	žádný	k3yNgFnSc1	žádný
podstatnější	podstatný	k2eAgFnSc1d2	podstatnější
inovace	inovace	k1gFnSc1	inovace
struktury	struktura	k1gFnSc2	struktura
armády	armáda	k1gFnSc2	armáda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
zkoumání	zkoumání	k1gNnSc2	zkoumání
historiků	historik	k1gMnPc2	historik
došlo	dojít	k5eAaPmAgNnS	dojít
však	však	k9	však
k	k	k7c3	k
prohloubení	prohloubení	k1gNnSc3	prohloubení
některých	některý	k3yIgInPc2	některý
stávajících	stávající	k2eAgInPc2d1	stávající
vojenských	vojenský	k2eAgInPc2d1	vojenský
trendů	trend	k1gInPc2	trend
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
vznikly	vzniknout	k5eAaPmAgInP	vzniknout
nové	nový	k2eAgInPc1d1	nový
oddíly	oddíl	k1gInPc1	oddíl
složené	složený	k2eAgInPc1d1	složený
z	z	k7c2	z
místních	místní	k2eAgMnPc2d1	místní
odvedenců	odvedenec	k1gMnPc2	odvedenec
bez	bez	k7c2	bez
římského	římský	k2eAgNnSc2d1	římské
občanství	občanství	k1gNnSc2	občanství
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
byly	být	k5eAaImAgFnP	být
posíleny	posílit	k5eAaPmNgInP	posílit
stavy	stav	k1gInPc1	stav
pomocných	pomocný	k2eAgMnPc2d1	pomocný
sborů	sbor	k1gInPc2	sbor
(	(	kIx(	(
<g/>
auxilia	auxilium	k1gNnSc2	auxilium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Vzrostl	vzrůst	k5eAaPmAgInS	vzrůst
tak	tak	k9	tak
jejich	jejich	k3xOp3gInSc1	jejich
podíl	podíl	k1gInSc1	podíl
na	na	k7c6	na
římské	římský	k2eAgFnSc6d1	římská
vojenské	vojenský	k2eAgFnSc6d1	vojenská
moci	moc	k1gFnSc6	moc
a	a	k8xC	a
souběžně	souběžně	k6eAd1	souběžně
pokračovala	pokračovat	k5eAaImAgFnS	pokračovat
tendence	tendence	k1gFnSc1	tendence
poklesu	pokles	k1gInSc2	pokles
počtu	počet	k1gInSc2	počet
italských	italský	k2eAgMnPc2d1	italský
branců	branec	k1gMnPc2	branec
v	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
legií	legie	k1gFnPc2	legie
<g/>
.	.	kIx.	.
</s>
<s>
Příčiny	příčina	k1gFnPc1	příčina
zvyšujícího	zvyšující	k2eAgMnSc2d1	zvyšující
se	se	k3xPyFc4	se
důrazu	důraz	k1gInSc2	důraz
kladeného	kladený	k2eAgMnSc2d1	kladený
na	na	k7c4	na
pomocné	pomocný	k2eAgInPc4d1	pomocný
sbory	sbor	k1gInPc4	sbor
spočívaly	spočívat	k5eAaImAgFnP	spočívat
ve	v	k7c6	v
specializovanějším	specializovaný	k2eAgNnSc6d2	specializovanější
a	a	k8xC	a
nekonvenčnějším	konvenční	k2eNgNnSc6d2	konvenční
vybavení	vybavení	k1gNnSc6	vybavení
a	a	k8xC	a
zaměření	zaměření	k1gNnSc4	zaměření
těchto	tento	k3xDgInPc2	tento
druhů	druh	k1gInPc2	druh
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
Velký	velký	k2eAgInSc1d1	velký
význam	význam	k1gInSc1	význam
náležel	náležet	k5eAaImAgInS	náležet
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
směru	směr	k1gInSc6	směr
především	především	k6eAd1	především
těžké	těžký	k2eAgFnSc6d1	těžká
jízdě	jízda	k1gFnSc6	jízda
<g/>
.	.	kIx.	.
</s>
<s>
Častějším	častý	k2eAgNnSc7d2	častější
zařazováním	zařazování	k1gNnSc7	zařazování
auxilií	auxilie	k1gFnPc2	auxilie
do	do	k7c2	do
římské	římský	k2eAgFnSc2d1	římská
armády	armáda	k1gFnSc2	armáda
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
rozšiřování	rozšiřování	k1gNnSc3	rozšiřování
rezervoáru	rezervoár	k1gInSc2	rezervoár
lidských	lidský	k2eAgFnPc2d1	lidská
sil	síla	k1gFnPc2	síla
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
potomci	potomek	k1gMnPc1	potomek
mužů	muž	k1gMnPc2	muž
působících	působící	k2eAgMnPc2d1	působící
v	v	k7c6	v
pomocných	pomocný	k2eAgInPc6d1	pomocný
sborech	sbor	k1gInPc6	sbor
směli	smět	k5eAaImAgMnP	smět
sloužit	sloužit	k5eAaImF	sloužit
jako	jako	k9	jako
legionáři	legionář	k1gMnPc1	legionář
<g/>
.	.	kIx.	.
</s>
<s>
Výdaje	výdaj	k1gInPc1	výdaj
vynakládané	vynakládaný	k2eAgInPc1d1	vynakládaný
na	na	k7c4	na
vydržování	vydržování	k1gNnSc4	vydržování
auxilií	auxilie	k1gFnPc2	auxilie
byly	být	k5eAaImAgFnP	být
navíc	navíc	k6eAd1	navíc
znatelně	znatelně	k6eAd1	znatelně
nižší	nízký	k2eAgFnSc1d2	nižší
ve	v	k7c6	v
srovnání	srovnání	k1gNnSc6	srovnání
s	s	k7c7	s
těmi	ten	k3xDgMnPc7	ten
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnPc4	jenž
byly	být	k5eAaImAgInP	být
spojeny	spojen	k2eAgInPc1d1	spojen
s	s	k7c7	s
legiemi	legie	k1gFnPc7	legie
<g/>
.	.	kIx.	.
</s>
<s>
Historik	historik	k1gMnSc1	historik
Ronald	Ronald	k1gMnSc1	Ronald
Syme	Syme	k1gInSc4	Syme
charakterizoval	charakterizovat	k5eAaBmAgMnS	charakterizovat
Hadriana	Hadrian	k1gMnSc4	Hadrian
jako	jako	k9	jako
jednoho	jeden	k4xCgMnSc2	jeden
z	z	k7c2	z
nejvšestrannějších	všestranný	k2eAgMnPc2d3	nejvšestrannější
římských	římský	k2eAgMnPc2d1	římský
císařů	císař	k1gMnPc2	císař
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
prokazoval	prokazovat	k5eAaImAgMnS	prokazovat
široké	široký	k2eAgFnSc3d1	široká
znalosti	znalost	k1gFnSc3	znalost
a	a	k8xC	a
dovednosti	dovednost	k1gFnSc3	dovednost
ve	v	k7c6	v
všech	všecek	k3xTgInPc6	všecek
intelektuálních	intelektuální	k2eAgInPc6d1	intelektuální
a	a	k8xC	a
uměleckých	umělecký	k2eAgInPc6d1	umělecký
oborech	obor	k1gInPc6	obor
a	a	k8xC	a
činnostech	činnost	k1gFnPc6	činnost
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
nadšený	nadšený	k2eAgMnSc1d1	nadšený
obdivovatel	obdivovatel	k1gMnSc1	obdivovatel
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
podporoval	podporovat	k5eAaImAgInS	podporovat
rozvoj	rozvoj	k1gInSc1	rozvoj
nových	nový	k2eAgFnPc2d1	nová
podob	podoba	k1gFnPc2	podoba
umění	umění	k1gNnSc2	umění
po	po	k7c6	po
celé	celý	k2eAgFnSc6d1	celá
říši	říš	k1gFnSc6	říš
<g/>
.	.	kIx.	.
</s>
<s>
Hadriánova	Hadriánův	k2eAgFnSc1d1	Hadriánova
vila	vila	k1gFnSc1	vila
v	v	k7c6	v
Tiburu	Tibur	k1gInSc6	Tibur
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgFnSc6d1	dnešní
Tivoli	Tivole	k1gFnSc6	Tivole
<g/>
)	)	kIx)	)
byla	být	k5eAaImAgFnS	být
jedním	jeden	k4xCgInSc7	jeden
z	z	k7c2	z
nejskvělejších	skvělý	k2eAgInPc2d3	nejskvělejší
římských	římský	k2eAgInPc2d1	římský
příkladů	příklad	k1gInPc2	příklad
zahrad	zahrada	k1gFnPc2	zahrada
vyvedených	vyvedený	k2eAgFnPc2d1	vyvedená
v	v	k7c6	v
alexandrijském	alexandrijský	k2eAgInSc6d1	alexandrijský
stylu	styl	k1gInSc6	styl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
současnosti	současnost	k1gFnSc6	současnost
je	být	k5eAaImIp3nS	být
tato	tento	k3xDgFnSc1	tento
vila	vila	k1gFnSc1	vila
z	z	k7c2	z
velké	velký	k2eAgFnSc2d1	velká
části	část	k1gFnSc2	část
zničena	zničen	k2eAgFnSc1d1	zničena
<g/>
,	,	kIx,	,
na	na	k7c6	na
čemž	což	k3yQnSc6	což
nesl	nést	k5eAaImAgInS	nést
značný	značný	k2eAgInSc1d1	značný
díl	díl	k1gInSc1	díl
odpovědnosti	odpovědnost	k1gFnSc2	odpovědnost
kardinál	kardinál	k1gMnSc1	kardinál
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Este	Est	k1gMnSc5	Est
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
nechal	nechat	k5eAaPmAgMnS	nechat
odstranit	odstranit	k5eAaPmF	odstranit
mnohé	mnohý	k2eAgInPc4d1	mnohý
mramorové	mramorový	k2eAgInPc4d1	mramorový
segmenty	segment	k1gInPc4	segment
<g/>
,	,	kIx,	,
jichž	jenž	k3xRgMnPc2	jenž
bylo	být	k5eAaImAgNnS	být
použito	použít	k5eAaPmNgNnS	použít
při	při	k7c6	při
výstavbě	výstavba	k1gFnSc6	výstavba
villy	villa	k1gMnSc2	villa
d	d	k?	d
<g/>
'	'	kIx"	'
<g/>
Este	Est	k1gMnSc5	Est
<g/>
.	.	kIx.	.
</s>
<s>
Pantheon	Pantheon	k1gInSc1	Pantheon
<g/>
,	,	kIx,	,
postavený	postavený	k2eAgInSc1d1	postavený
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
Agrippou	Agrippa	k1gFnSc7	Agrippa
a	a	k8xC	a
zničený	zničený	k2eAgMnSc1d1	zničený
velkým	velký	k2eAgInSc7d1	velký
požárem	požár	k1gInSc7	požár
v	v	k7c6	v
roce	rok	k1gInSc6	rok
80	[number]	k4	80
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
za	za	k7c2	za
Hadriana	Hadrian	k1gMnSc4	Hadrian
rekonstruován	rekonstruován	k2eAgInSc1d1	rekonstruován
a	a	k8xC	a
svůj	svůj	k3xOyFgInSc4	svůj
tehdejší	tehdejší	k2eAgInSc4d1	tehdejší
kupolovitý	kupolovitý	k2eAgInSc4d1	kupolovitý
vzhled	vzhled	k1gInSc4	vzhled
si	se	k3xPyFc3	se
podržel	podržet	k5eAaPmAgMnS	podržet
až	až	k9	až
do	do	k7c2	do
dnešních	dnešní	k2eAgInPc2d1	dnešní
dnů	den	k1gInPc2	den
<g/>
.	.	kIx.	.
</s>
<s>
Tento	tento	k3xDgInSc1	tento
monument	monument	k1gInSc1	monument
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
patří	patřit	k5eAaImIp3nS	patřit
mezi	mezi	k7c4	mezi
nejlépe	dobře	k6eAd3	dobře
dochované	dochovaný	k2eAgFnPc4d1	dochovaná
antické	antický	k2eAgFnPc4d1	antická
památky	památka	k1gFnPc4	památka
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
<g/>
,	,	kIx,	,
představoval	představovat	k5eAaImAgInS	představovat
důležitý	důležitý	k2eAgInSc1d1	důležitý
zdroj	zdroj	k1gInSc1	zdroj
inspirace	inspirace	k1gFnSc2	inspirace
pro	pro	k7c4	pro
architekty	architekt	k1gMnPc4	architekt
období	období	k1gNnSc1	období
renesance	renesance	k1gFnSc2	renesance
a	a	k8xC	a
baroka	baroko	k1gNnSc2	baroko
<g/>
.	.	kIx.	.
</s>
<s>
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
taktéž	taktéž	k?	taktéž
dokončil	dokončit	k5eAaPmAgMnS	dokončit
Trajánovo	Trajánův	k2eAgNnSc4d1	Trajánovo
fórum	fórum	k1gNnSc4	fórum
zřízením	zřízení	k1gNnSc7	zřízení
prostorného	prostorný	k2eAgInSc2d1	prostorný
Trajánova	Trajánův	k2eAgInSc2d1	Trajánův
chrámu	chrám	k1gInSc2	chrám
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
východním	východní	k2eAgInSc6d1	východní
konci	konec	k1gInSc6	konec
Fora	forum	k1gNnSc2	forum
Romana	Roman	k1gMnSc4	Roman
dal	dát	k5eAaPmAgInS	dát
vybudovat	vybudovat	k5eAaPmF	vybudovat
jedinečný	jedinečný	k2eAgInSc1d1	jedinečný
chrám	chrám	k1gInSc1	chrám
Venuše	Venuše	k1gFnSc2	Venuše
a	a	k8xC	a
Romy	Rom	k1gMnPc4	Rom
<g/>
,	,	kIx,	,
překonávající	překonávající	k2eAgMnSc1d1	překonávající
svou	svůj	k3xOyFgFnSc7	svůj
velikostí	velikost	k1gFnSc7	velikost
všechny	všechen	k3xTgFnPc4	všechen
dosavadní	dosavadní	k2eAgFnPc4d1	dosavadní
římské	římský	k2eAgFnPc4d1	římská
svatyně	svatyně	k1gFnPc4	svatyně
<g/>
.	.	kIx.	.
</s>
<s>
Zájem	zájem	k1gInSc1	zájem
o	o	k7c4	o
architekturu	architektura	k1gFnSc4	architektura
projevoval	projevovat	k5eAaImAgMnS	projevovat
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
již	již	k6eAd1	již
v	v	k7c6	v
dobách	doba	k1gFnPc6	doba
dávno	dávno	k6eAd1	dávno
předcházejících	předcházející	k2eAgNnPc6d1	předcházející
svému	svůj	k3xOyFgInSc3	svůj
nástupu	nástup	k1gInSc3	nástup
na	na	k7c4	na
trůn	trůn	k1gInSc4	trůn
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
zdá	zdát	k5eAaPmIp3nS	zdát
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
dychtivost	dychtivost	k1gFnSc1	dychtivost
nebyla	být	k5eNaImAgFnS	být
vždy	vždy	k6eAd1	vždy
vítána	vítat	k5eAaImNgFnS	vítat
<g/>
.	.	kIx.	.
</s>
<s>
Apollodóros	Apollodórosa	k1gFnPc2	Apollodórosa
z	z	k7c2	z
Damašku	Damašek	k1gInSc2	Damašek
<g/>
,	,	kIx,	,
slavný	slavný	k2eAgMnSc1d1	slavný
stavitel	stavitel	k1gMnSc1	stavitel
Trajánova	Trajánův	k2eAgNnSc2d1	Trajánovo
fóra	fórum	k1gNnSc2	fórum
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
odmítl	odmítnout	k5eAaPmAgMnS	odmítnout
zabývat	zabývat	k5eAaImF	zabývat
jeho	jeho	k3xOp3gInPc4	jeho
návrhy	návrh	k1gInPc4	návrh
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
jiné	jiný	k2eAgFnSc6d1	jiná
záležitosti	záležitost	k1gFnSc6	záležitost
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
Traianus	Traianus	k1gMnSc1	Traianus
konzultoval	konzultovat	k5eAaImAgMnS	konzultovat
s	s	k7c7	s
Apollodórem	Apollodór	k1gInSc7	Apollodór
jistý	jistý	k2eAgInSc4d1	jistý
architektonický	architektonický	k2eAgInSc4d1	architektonický
problém	problém	k1gInSc4	problém
<g/>
,	,	kIx,	,
přerušil	přerušit	k5eAaPmAgMnS	přerušit
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
jejich	jejich	k3xOp3gFnSc4	jejich
rozpravu	rozprava	k1gFnSc4	rozprava
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
sdělil	sdělit	k5eAaPmAgMnS	sdělit
svůj	svůj	k3xOyFgInSc4	svůj
názor	názor	k1gInSc4	názor
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
ho	on	k3xPp3gMnSc4	on
Apollodóros	Apollodórosa	k1gFnPc2	Apollodórosa
odbyl	odbýt	k5eAaPmAgInS	odbýt
slovy	slovo	k1gNnPc7	slovo
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Běž	běžet	k5eAaImRp2nS	běžet
kreslit	kreslit	k5eAaImF	kreslit
své	svůj	k3xOyFgFnPc4	svůj
báně	báně	k1gFnPc4	báně
<g/>
.	.	kIx.	.
</s>
<s>
Těmto	tento	k3xDgFnPc3	tento
věcem	věc	k1gFnPc3	věc
nerozumíš	rozumět	k5eNaImIp2nS	rozumět
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Báněmi	báně	k1gFnPc7	báně
měl	mít	k5eAaImAgInS	mít
Apollodóros	Apollodórosa	k1gFnPc2	Apollodórosa
na	na	k7c6	na
mysli	mysl	k1gFnSc6	mysl
Hadrianovy	Hadrianův	k2eAgInPc1d1	Hadrianův
náčrty	náčrt	k1gInPc1	náčrt
kupolí	kupole	k1gFnSc7	kupole
<g/>
,	,	kIx,	,
jakou	jaký	k3yRgFnSc4	jaký
bylo	být	k5eAaImAgNnS	být
třeba	třeba	k6eAd1	třeba
Serapeum	Serapeum	k1gNnSc1	Serapeum
v	v	k7c6	v
Hadriánově	Hadriánův	k2eAgFnSc6d1	Hadriánova
vile	vila	k1gFnSc6	vila
<g/>
.	.	kIx.	.
</s>
<s>
Kolovaly	kolovat	k5eAaImAgFnP	kolovat
určité	určitý	k2eAgFnPc1d1	určitá
zvěsti	zvěst	k1gFnPc1	zvěst
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nichž	jenž	k3xRgFnPc2	jenž
měl	mít	k5eAaImAgMnS	mít
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
<g/>
,	,	kIx,	,
jakmile	jakmile	k8xS	jakmile
se	se	k3xPyFc4	se
po	po	k7c6	po
Traianovi	Traian	k1gMnSc6	Traian
chopil	chopit	k5eAaPmAgInS	chopit
vlády	vláda	k1gFnSc2	vláda
<g/>
,	,	kIx,	,
poslat	poslat	k5eAaPmF	poslat
Apollodóra	Apollodóro	k1gNnPc4	Apollodóro
do	do	k7c2	do
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
a	a	k8xC	a
přikázat	přikázat	k5eAaPmF	přikázat
jeho	jeho	k3xOp3gNnSc4	jeho
zavraždění	zavraždění	k1gNnSc4	zavraždění
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
ovšem	ovšem	k9	ovšem
vysoce	vysoce	k6eAd1	vysoce
pravděpodobné	pravděpodobný	k2eAgNnSc1d1	pravděpodobné
<g/>
,	,	kIx,	,
že	že	k8xS	že
tento	tento	k3xDgInSc1	tento
pozdější	pozdní	k2eAgInSc1d2	pozdější
příběh	příběh	k1gInSc1	příběh
byl	být	k5eAaImAgInS	být
pokusem	pokus	k1gInSc7	pokus
o	o	k7c4	o
očernění	očernění	k1gNnSc4	očernění
císaře	císař	k1gMnSc2	císař
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
totiž	totiž	k9	totiž
Hadrianus	Hadrianus	k1gInSc1	Hadrianus
těšil	těšit	k5eAaImAgInS	těšit
poměrně	poměrně	k6eAd1	poměrně
vysoké	vysoký	k2eAgFnSc3d1	vysoká
oblibě	obliba	k1gFnSc3	obliba
mezi	mezi	k7c7	mezi
obyvateli	obyvatel	k1gMnPc7	obyvatel
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
nedostávalo	dostávat	k5eNaImAgNnS	dostávat
se	se	k3xPyFc4	se
mu	on	k3xPp3gMnSc3	on
všeobecného	všeobecný	k2eAgNnSc2d1	všeobecné
přijetí	přijetí	k1gNnSc2	přijetí
jak	jak	k8xS	jak
za	za	k7c2	za
života	život	k1gInSc2	život
<g/>
,	,	kIx,	,
tak	tak	k6eAd1	tak
ani	ani	k8xC	ani
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
<g/>
.	.	kIx.	.
</s>
<s>
Hadrianus	Hadrianus	k1gInSc1	Hadrianus
psal	psát	k5eAaImAgInS	psát
poezii	poezie	k1gFnSc3	poezie
latinsky	latinsky	k6eAd1	latinsky
i	i	k9	i
řecky	řecky	k6eAd1	řecky
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
se	se	k3xPyFc4	se
dochovalo	dochovat	k5eAaPmAgNnS	dochovat
několik	několik	k4yIc1	několik
jemu	on	k3xPp3gMnSc3	on
připisovaných	připisovaný	k2eAgFnPc2d1	připisovaná
básní	báseň	k1gFnPc2	báseň
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
latinských	latinský	k2eAgInPc2d1	latinský
veršů	verš	k1gInPc2	verš
složených	složený	k2eAgInPc2d1	složený
na	na	k7c6	na
smrtelné	smrtelný	k2eAgFnSc6d1	smrtelná
posteli	postel	k1gFnSc6	postel
<g/>
.	.	kIx.	.
</s>
<s>
Vyhotovil	vyhotovit	k5eAaPmAgMnS	vyhotovit
i	i	k9	i
vlastní	vlastní	k2eAgInSc4d1	vlastní
životopis	životopis	k1gInSc4	životopis
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
chtěl	chtít	k5eAaImAgMnS	chtít
vyvrátit	vyvrátit	k5eAaPmF	vyvrátit
určité	určitý	k2eAgFnPc4d1	určitá
spekulace	spekulace	k1gFnPc4	spekulace
a	a	k8xC	a
pověsti	pověst	k1gFnPc4	pověst
a	a	k8xC	a
ozřejmit	ozřejmit	k5eAaPmF	ozřejmit
jistá	jistý	k2eAgNnPc4d1	jisté
rozhodnutí	rozhodnutí	k1gNnPc4	rozhodnutí
učiněná	učiněný	k2eAgNnPc4d1	učiněné
za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
práce	práce	k1gFnSc1	práce
byla	být	k5eAaImAgFnS	být
sice	sice	k8xC	sice
ztracena	ztracen	k2eAgFnSc1d1	ztracena
<g/>
,	,	kIx,	,
patrně	patrně	k6eAd1	patrně
z	z	k7c2	z
ní	on	k3xPp3gFnSc2	on
ale	ale	k9	ale
čerpal	čerpat	k5eAaImAgInS	čerpat
Marius	Marius	k1gInSc1	Marius
Maximus	Maximus	k1gMnSc1	Maximus
nebo	nebo	k8xC	nebo
jiný	jiný	k2eAgMnSc1d1	jiný
antický	antický	k2eAgMnSc1d1	antický
historik	historik	k1gMnSc1	historik
<g/>
,	,	kIx,	,
z	z	k7c2	z
jehož	jenž	k3xRgInSc2	jenž
spisu	spis	k1gInSc2	spis
vycházel	vycházet	k5eAaImAgMnS	vycházet
autor	autor	k1gMnSc1	autor
Hadrianova	Hadrianův	k2eAgInSc2d1	Hadrianův
životopisu	životopis	k1gInSc2	životopis
v	v	k7c6	v
Historii	historie	k1gFnSc6	historie
Augustě	Augusta	k1gFnSc6	Augusta
<g/>
.	.	kIx.	.
</s>
<s>
Přinejmenším	přinejmenším	k6eAd1	přinejmenším
některá	některý	k3yIgNnPc4	některý
tvrzení	tvrzení	k1gNnPc4	tvrzení
obsažená	obsažený	k2eAgNnPc4d1	obsažené
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
díle	díl	k1gInSc6	díl
zřejmě	zřejmě	k6eAd1	zřejmě
pocházejí	pocházet	k5eAaImIp3nP	pocházet
z	z	k7c2	z
Hadrianovy	Hadrianův	k2eAgFnSc2d1	Hadrianova
autobiografie	autobiografie	k1gFnSc2	autobiografie
<g/>
.	.	kIx.	.
</s>
<s>
Kromě	kromě	k7c2	kromě
záliby	záliba	k1gFnSc2	záliba
v	v	k7c6	v
literatuře	literatura	k1gFnSc6	literatura
byl	být	k5eAaImAgInS	být
Hadrianus	Hadrianus	k1gInSc4	Hadrianus
už	už	k9	už
od	od	k7c2	od
svého	svůj	k3xOyFgInSc2	svůj
mládí	mládí	k1gNnSc4	mládí
vášnivým	vášnivý	k2eAgMnSc7d1	vášnivý
lovcem	lovec	k1gMnSc7	lovec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Asii	Asie	k1gFnSc6	Asie
údajně	údajně	k6eAd1	údajně
založil	založit	k5eAaPmAgMnS	založit
a	a	k8xC	a
zasvětil	zasvětit	k5eAaPmAgMnS	zasvětit
chrám	chrám	k1gInSc4	chrám
na	na	k7c4	na
oslavu	oslava	k1gFnSc4	oslava
medvědice	medvědice	k1gFnSc2	medvědice
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
skolil	skolit	k5eAaImAgMnS	skolit
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
zdokumentováno	zdokumentován	k2eAgNnSc1d1	zdokumentováno
<g/>
,	,	kIx,	,
že	že	k8xS	že
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
ulovil	ulovit	k5eAaPmAgMnS	ulovit
spolu	spolu	k6eAd1	spolu
se	s	k7c7	s
svým	svůj	k3xOyFgMnSc7	svůj
oblíbencem	oblíbenec	k1gMnSc7	oblíbenec
Antinoem	Antino	k1gMnSc7	Antino
lva	lev	k1gMnSc2	lev
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
nařídil	nařídit	k5eAaPmAgInS	nařídit
vystavět	vystavět	k5eAaPmF	vystavět
monument	monument	k1gInSc1	monument
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
byl	být	k5eAaImAgInS	být
na	na	k7c6	na
osmi	osm	k4xCc6	osm
reliéfech	reliéf	k1gInPc6	reliéf
vyobrazen	vyobrazen	k2eAgInSc4d1	vyobrazen
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
fázích	fáze	k1gFnPc6	fáze
lovu	lov	k1gInSc2	lov
<g/>
.	.	kIx.	.
</s>
<s>
Dalším	další	k2eAgInSc7d1	další
Hadrianovým	Hadrianův	k2eAgInSc7d1	Hadrianův
kulturním	kulturní	k2eAgInSc7d1	kulturní
příspěvkem	příspěvek	k1gInSc7	příspěvek
bylo	být	k5eAaImAgNnS	být
zavedení	zavedení	k1gNnSc1	zavedení
nošení	nošení	k1gNnSc2	nošení
vousů	vous	k1gInPc2	vous
<g/>
,	,	kIx,	,
symbolizujících	symbolizující	k2eAgInPc2d1	symbolizující
císařovo	císařův	k2eAgNnSc4d1	císařovo
filhelénství	filhelénství	k1gNnSc4	filhelénství
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
výjimkou	výjimka	k1gFnSc7	výjimka
Nerona	Nero	k1gMnSc2	Nero
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
byl	být	k5eAaImAgInS	být
rovněž	rovněž	k9	rovněž
velkým	velký	k2eAgMnSc7d1	velký
milovníkem	milovník	k1gMnSc7	milovník
řecké	řecký	k2eAgFnSc2d1	řecká
kultury	kultura	k1gFnSc2	kultura
<g/>
,	,	kIx,	,
bývali	bývat	k5eAaImAgMnP	bývat
všichni	všechen	k3xTgMnPc1	všechen
dřívější	dřívější	k2eAgMnPc1d1	dřívější
římští	římský	k2eAgMnPc1d1	římský
císaři	císař	k1gMnPc1	císař
oholení	oholení	k1gNnSc2	oholení
<g/>
.	.	kIx.	.
</s>
<s>
Naproti	naproti	k7c3	naproti
tomu	ten	k3xDgNnSc3	ten
řada	řada	k1gFnSc1	řada
Hadrianových	Hadrianův	k2eAgMnPc2d1	Hadrianův
následovníků	následovník	k1gMnPc2	následovník
až	až	k9	až
do	do	k7c2	do
doby	doba	k1gFnSc2	doba
vlády	vláda	k1gFnSc2	vláda
Konstantina	Konstantin	k1gMnSc2	Konstantin
I.	I.	kA	I.
Velikého	veliký	k2eAgInSc2d1	veliký
byla	být	k5eAaImAgFnS	být
portrétována	portrétovat	k5eAaImNgFnS	portrétovat
s	s	k7c7	s
vousy	vous	k1gInPc7	vous
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
nošení	nošení	k1gNnSc1	nošení
se	se	k3xPyFc4	se
za	za	k7c2	za
Hadriana	Hadrian	k1gMnSc2	Hadrian
stalo	stát	k5eAaPmAgNnS	stát
módní	módní	k2eAgFnSc7d1	módní
záležitostí	záležitost	k1gFnSc7	záležitost
<g/>
.	.	kIx.	.
</s>
<s>
Svým	svůj	k3xOyFgNnSc7	svůj
zaměřením	zaměření	k1gNnSc7	zaměření
byl	být	k5eAaImAgInS	být
Hadrianus	Hadrianus	k1gInSc1	Hadrianus
oddán	oddán	k2eAgInSc1d1	oddán
humanismu	humanismus	k1gInSc2	humanismus
a	a	k8xC	a
především	především	k9	především
filhelénství	filhelénství	k1gNnSc1	filhelénství
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filozofii	filozofie	k1gFnSc6	filozofie
se	se	k3xPyFc4	se
přikláněl	přiklánět	k5eAaImAgMnS	přiklánět
k	k	k7c3	k
Epiktétovi	Epiktét	k1gMnSc3	Epiktét
<g/>
,	,	kIx,	,
Heliodorovi	Heliodor	k1gMnSc3	Heliodor
a	a	k8xC	a
Favorinovi	Favorin	k1gMnSc3	Favorin
<g/>
,	,	kIx,	,
všeobecně	všeobecně	k6eAd1	všeobecně
byl	být	k5eAaImAgInS	být
však	však	k9	však
pokládán	pokládán	k2eAgInSc1d1	pokládán
za	za	k7c4	za
stoupence	stoupenec	k1gMnPc4	stoupenec
epikureismu	epikureismus	k1gInSc2	epikureismus
<g/>
,	,	kIx,	,
mezi	mezi	k7c4	mezi
něž	jenž	k3xRgFnPc4	jenž
patřili	patřit	k5eAaImAgMnP	patřit
také	také	k9	také
jeho	jeho	k3xOp3gMnPc1	jeho
přátelé	přítel	k1gMnPc1	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
filozofické	filozofický	k2eAgInPc1d1	filozofický
postoje	postoj	k1gInPc1	postoj
se	se	k3xPyFc4	se
odrážely	odrážet	k5eAaImAgInP	odrážet
i	i	k9	i
v	v	k7c6	v
jeho	jeho	k3xOp3gFnSc6	jeho
snaze	snaha	k1gFnSc6	snaha
o	o	k7c4	o
zlepšení	zlepšení	k1gNnSc4	zlepšení
životních	životní	k2eAgInPc2d1	životní
poměrů	poměr	k1gInPc2	poměr
obyvatelstva	obyvatelstvo	k1gNnSc2	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Tudíž	tudíž	k8xC	tudíž
zakázal	zakázat	k5eAaPmAgMnS	zakázat
mučení	mučení	k1gNnPc4	mučení
otroků	otrok	k1gMnPc2	otrok
a	a	k8xC	a
všeobecně	všeobecně	k6eAd1	všeobecně
zmírnil	zmírnit	k5eAaPmAgMnS	zmírnit
otroctví	otroctví	k1gNnSc4	otroctví
<g/>
,	,	kIx,	,
jež	jenž	k3xRgNnSc1	jenž
se	se	k3xPyFc4	se
stalo	stát	k5eAaPmAgNnS	stát
humánnějším	humánní	k2eAgMnSc7d2	humánnější
<g/>
.	.	kIx.	.
</s>
<s>
Dále	daleko	k6eAd2	daleko
nechal	nechat	k5eAaPmAgMnS	nechat
budovat	budovat	k5eAaImF	budovat
knihovny	knihovna	k1gFnPc4	knihovna
<g/>
,	,	kIx,	,
akvadukty	akvadukt	k1gInPc4	akvadukt
<g/>
,	,	kIx,	,
lázně	lázeň	k1gFnPc4	lázeň
a	a	k8xC	a
divadla	divadlo	k1gNnPc4	divadlo
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
pobytu	pobyt	k1gInSc6	pobyt
v	v	k7c6	v
Řecku	Řecko	k1gNnSc6	Řecko
v	v	k7c6	v
roce	rok	k1gInSc6	rok
128	[number]	k4	128
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
vytvořit	vytvořit	k5eAaPmF	vytvořit
určitý	určitý	k2eAgInSc4d1	určitý
druh	druh	k1gInSc4	druh
shromáždění	shromáždění	k1gNnSc2	shromáždění
<g/>
,	,	kIx,	,
reprezentující	reprezentující	k2eAgInPc1d1	reprezentující
dosud	dosud	k6eAd1	dosud
poloautonomní	poloautonomní	k2eAgInPc1d1	poloautonomní
městské	městský	k2eAgInPc1d1	městský
státy	stát	k1gInPc1	stát
Řecka	Řecko	k1gNnSc2	Řecko
a	a	k8xC	a
Iónie	Iónie	k1gFnSc2	Iónie
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
instituce	instituce	k1gFnSc1	instituce
<g/>
,	,	kIx,	,
známá	známý	k2eAgFnSc1d1	známá
jako	jako	k9	jako
Panhellenion	Panhellenion	k1gInSc1	Panhellenion
<g/>
,	,	kIx,	,
nepřinesla	přinést	k5eNaPmAgNnP	přinést
i	i	k9	i
přes	přes	k7c4	přes
císařovo	císařův	k2eAgNnSc4d1	císařovo
úsilí	úsilí	k1gNnSc4	úsilí
větší	veliký	k2eAgFnSc4d2	veliký
spolupráci	spolupráce	k1gFnSc4	spolupráce
mezi	mezi	k7c7	mezi
Helény	Helén	k1gMnPc7	Helén
<g/>
.	.	kIx.	.
</s>
<s>
Mnohými	mnohý	k2eAgMnPc7d1	mnohý
dějepisci	dějepisec	k1gMnPc7	dějepisec
je	být	k5eAaImIp3nS	být
Hadrianus	Hadrianus	k1gInSc1	Hadrianus
hodnocen	hodnotit	k5eAaImNgInS	hodnotit
jako	jako	k8xS	jako
moudrý	moudrý	k2eAgMnSc1d1	moudrý
a	a	k8xC	a
spravedlivý	spravedlivý	k2eAgMnSc1d1	spravedlivý
vládce	vládce	k1gMnSc1	vládce
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
byl	být	k5eAaImAgInS	být
nazván	nazván	k2eAgInSc1d1	nazván
"	"	kIx"	"
<g/>
prvním	první	k4xOgMnSc7	první
služebníkem	služebník	k1gMnSc7	služebník
říše	říš	k1gFnSc2	říš
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Britský	britský	k2eAgMnSc1d1	britský
historik	historik	k1gMnSc1	historik
Edward	Edward	k1gMnSc1	Edward
Gibbon	gibbon	k1gMnSc1	gibbon
se	se	k3xPyFc4	se
pochvalně	pochvalně	k6eAd1	pochvalně
vyslovoval	vyslovovat	k5eAaImAgMnS	vyslovovat
o	o	k7c6	o
císařově	císařův	k2eAgFnSc6d1	císařova
"	"	kIx"	"
<g/>
rozšafnosti	rozšafnost	k1gFnSc6	rozšafnost
a	a	k8xC	a
umírněnosti	umírněnost	k1gFnSc6	umírněnost
<g/>
"	"	kIx"	"
<g/>
,	,	kIx,	,
stejně	stejně	k6eAd1	stejně
jako	jako	k8xC	jako
o	o	k7c6	o
jeho	jeho	k3xOp3gFnSc6	jeho
"	"	kIx"	"
<g/>
neúnavné	únavný	k2eNgFnSc6d1	neúnavná
činnosti	činnost	k1gFnSc6	činnost
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
smrti	smrt	k1gFnSc6	smrt
byl	být	k5eAaImAgMnS	být
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
pohřben	pohřbít	k5eAaPmNgMnS	pohřbít
v	v	k7c6	v
mauzoleu	mauzoleum	k1gNnSc6	mauzoleum
<g/>
,	,	kIx,	,
nacházejícím	nacházející	k2eAgMnSc7d1	nacházející
se	se	k3xPyFc4	se
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
na	na	k7c6	na
pravém	pravý	k2eAgInSc6d1	pravý
břehu	břeh	k1gInSc6	břeh
řeky	řeka	k1gFnSc2	řeka
Tibery	Tibera	k1gFnSc2	Tibera
<g/>
,	,	kIx,	,
přes	přes	k7c4	přes
niž	jenž	k3xRgFnSc4	jenž
byl	být	k5eAaImAgInS	být
vybudován	vybudován	k2eAgInSc1d1	vybudován
Aeliův	Aeliův	k2eAgInSc1d1	Aeliův
most	most	k1gInSc1	most
(	(	kIx(	(
<g/>
Pons	Pons	k1gInSc1	Pons
Aelius	Aelius	k1gInSc1	Aelius
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Mauzoleum	mauzoleum	k1gNnSc1	mauzoleum
bylo	být	k5eAaImAgNnS	být
ve	v	k7c6	v
středověku	středověk	k1gInSc6	středověk
přetvořeno	přetvořen	k2eAgNnSc1d1	přetvořeno
v	v	k7c4	v
pevnost	pevnost	k1gFnSc4	pevnost
papežů	papež	k1gMnPc2	papež
<g/>
,	,	kIx,	,
známou	známý	k2eAgFnSc4d1	známá
nyní	nyní	k6eAd1	nyní
jako	jako	k8xS	jako
Andělský	andělský	k2eAgInSc4d1	andělský
hrad	hrad	k1gInSc4	hrad
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInPc1	jeho
původní	původní	k2eAgInPc1d1	původní
rozměry	rozměr	k1gInPc1	rozměr
byly	být	k5eAaImAgInP	být
úmyslně	úmyslně	k6eAd1	úmyslně
navrženy	navrhnout	k5eAaPmNgInP	navrhnout
tak	tak	k6eAd1	tak
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
velikostí	velikost	k1gFnPc2	velikost
předčil	předčít	k5eAaPmAgInS	předčít
dřívější	dřívější	k2eAgNnSc4d1	dřívější
Augustovo	Augustův	k2eAgNnSc4d1	Augustovo
mauzoleum	mauzoleum	k1gNnSc4	mauzoleum
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
Cassia	Cassius	k1gMnSc2	Cassius
Diona	Dion	k1gMnSc2	Dion
byla	být	k5eAaImAgFnS	být
po	po	k7c6	po
Hadrianově	Hadrianův	k2eAgNnSc6d1	Hadrianovo
úmrtí	úmrtí	k1gNnSc6	úmrtí
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
vztyčena	vztyčen	k2eAgFnSc1d1	vztyčena
obrovská	obrovský	k2eAgFnSc1d1	obrovská
jezdecká	jezdecký	k2eAgFnSc1d1	jezdecká
socha	socha	k1gFnSc1	socha
tohoto	tento	k3xDgMnSc2	tento
císaře	císař	k1gMnSc2	císař
<g/>
.	.	kIx.	.
</s>
<s>
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
si	se	k3xPyFc3	se
počínal	počínat	k5eAaImAgMnS	počínat
jako	jako	k9	jako
zdatný	zdatný	k2eAgMnSc1d1	zdatný
organizátor	organizátor	k1gMnSc1	organizátor
nejen	nejen	k6eAd1	nejen
ve	v	k7c6	v
vojenství	vojenství	k1gNnSc6	vojenství
<g/>
,	,	kIx,	,
nýbrž	nýbrž	k8xC	nýbrž
se	se	k3xPyFc4	se
projevil	projevit	k5eAaPmAgInS	projevit
taktéž	taktéž	k?	taktéž
jako	jako	k8xC	jako
vynikající	vynikající	k2eAgMnSc1d1	vynikající
administrátor	administrátor	k1gMnSc1	administrátor
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
byla	být	k5eAaImAgFnS	být
uskutečněna	uskutečnit	k5eAaPmNgFnS	uskutečnit
kompletní	kompletní	k2eAgFnSc1d1	kompletní
reforma	reforma	k1gFnSc1	reforma
císařského	císařský	k2eAgInSc2d1	císařský
správního	správní	k2eAgInSc2d1	správní
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
doprovázená	doprovázený	k2eAgFnSc1d1	doprovázená
transformací	transformace	k1gFnSc7	transformace
veřejných	veřejný	k2eAgFnPc2d1	veřejná
financí	finance	k1gFnPc2	finance
a	a	k8xC	a
zlepšením	zlepšení	k1gNnSc7	zlepšení
diplomatických	diplomatický	k2eAgInPc2d1	diplomatický
vztahů	vztah	k1gInPc2	vztah
se	s	k7c7	s
zahraničím	zahraničí	k1gNnSc7	zahraničí
<g/>
.	.	kIx.	.
</s>
<s>
Těmito	tento	k3xDgInPc7	tento
změnami	změna	k1gFnPc7	změna
císař	císař	k1gMnSc1	císař
usiloval	usilovat	k5eAaImAgInS	usilovat
o	o	k7c4	o
standardizaci	standardizace	k1gFnSc4	standardizace
státních	státní	k2eAgInPc2d1	státní
orgánů	orgán	k1gInPc2	orgán
<g/>
,	,	kIx,	,
jež	jenž	k3xRgFnSc1	jenž
měla	mít	k5eAaImAgFnS	mít
vést	vést	k5eAaImF	vést
ke	k	k7c3	k
konsolidaci	konsolidace	k1gFnSc3	konsolidace
hranic	hranice	k1gFnPc2	hranice
impéria	impérium	k1gNnSc2	impérium
<g/>
,	,	kIx,	,
čehož	což	k3yQnSc2	což
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
dosaženo	dosáhnout	k5eAaPmNgNnS	dosáhnout
odstraněním	odstranění	k1gNnSc7	odstranění
překážek	překážka	k1gFnPc2	překážka
účinné	účinný	k2eAgFnSc2d1	účinná
obrany	obrana	k1gFnSc2	obrana
státu	stát	k1gInSc2	stát
a	a	k8xC	a
vytyčením	vytyčení	k1gNnSc7	vytyčení
sfér	sféra	k1gFnPc2	sféra
vlivu	vliv	k1gInSc2	vliv
uzavíráním	uzavírání	k1gNnSc7	uzavírání
spojeneckých	spojenecký	k2eAgFnPc2d1	spojenecká
smluv	smlouva	k1gFnPc2	smlouva
s	s	k7c7	s
cizími	cizí	k2eAgInPc7d1	cizí
národy	národ	k1gInPc7	národ
<g/>
.	.	kIx.	.
</s>
<s>
Hadrianova	Hadrianův	k2eAgFnSc1d1	Hadrianova
politika	politika	k1gFnSc1	politika
se	se	k3xPyFc4	se
vyznačovala	vyznačovat	k5eAaImAgFnS	vyznačovat
tolerancí	tolerance	k1gFnSc7	tolerance
vůči	vůči	k7c3	vůči
příslušníkům	příslušník	k1gMnPc3	příslušník
odlišných	odlišný	k2eAgFnPc2d1	odlišná
kultur	kultura	k1gFnPc2	kultura
a	a	k8xC	a
náboženství	náboženství	k1gNnPc2	náboženství
<g/>
.	.	kIx.	.
</s>
<s>
Křesťané	křesťan	k1gMnPc1	křesťan
se	se	k3xPyFc4	se
tak	tak	k6eAd1	tak
za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
dočkali	dočkat	k5eAaPmAgMnP	dočkat
větší	veliký	k2eAgFnPc4d2	veliký
svobody	svoboda	k1gFnPc4	svoboda
<g/>
.	.	kIx.	.
</s>
<s>
Zásadní	zásadní	k2eAgFnSc7d1	zásadní
reformou	reforma	k1gFnSc7	reforma
prošly	projít	k5eAaPmAgInP	projít
stěžejní	stěžejní	k2eAgInPc1d1	stěžejní
instituty	institut	k1gInPc1	institut
římského	římský	k2eAgInSc2d1	římský
právního	právní	k2eAgInSc2d1	právní
systému	systém	k1gInSc2	systém
<g/>
,	,	kIx,	,
praetorský	praetorský	k2eAgInSc1d1	praetorský
edikt	edikt	k1gInSc1	edikt
(	(	kIx(	(
<g/>
edictum	edictum	k1gNnSc1	edictum
praetoriis	praetoriis	k1gFnSc2	praetoriis
<g/>
)	)	kIx)	)
a	a	k8xC	a
edikt	edikt	k1gInSc1	edikt
kurulských	kurulský	k2eAgInPc2d1	kurulský
aedilů	aedil	k1gInPc2	aedil
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
představovaly	představovat	k5eAaImAgFnP	představovat
souhrn	souhrn	k1gInSc4	souhrn
postupně	postupně	k6eAd1	postupně
propracovaných	propracovaný	k2eAgInPc2d1	propracovaný
právních	právní	k2eAgInPc2d1	právní
principů	princip	k1gInPc2	princip
<g/>
,	,	kIx,	,
jimiž	jenž	k3xRgInPc7	jenž
se	se	k3xPyFc4	se
magistráti	magistrát	k1gMnPc1	magistrát
při	při	k7c6	při
svém	svůj	k3xOyFgNnSc6	svůj
uvedení	uvedení	k1gNnSc6	uvedení
do	do	k7c2	do
úřadu	úřad	k1gInSc2	úřad
zavazovali	zavazovat	k5eAaImAgMnP	zavazovat
řídit	řídit	k5eAaImF	řídit
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
rámci	rámec	k1gInSc6	rámec
praetorského	praetorský	k2eAgInSc2d1	praetorský
ediktu	edikt	k1gInSc2	edikt
docházelo	docházet	k5eAaImAgNnS	docházet
k	k	k7c3	k
doplnění	doplnění	k1gNnSc3	doplnění
<g/>
,	,	kIx,	,
vyložení	vyložení	k1gNnSc3	vyložení
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
opravení	opravení	k1gNnSc1	opravení
občanského	občanský	k2eAgNnSc2d1	občanské
práva	právo	k1gNnSc2	právo
(	(	kIx(	(
<g/>
ius	ius	k?	ius
civile	civil	k1gMnSc5	civil
<g/>
)	)	kIx)	)
z	z	k7c2	z
vlastní	vlastní	k2eAgFnSc2d1	vlastní
iniciativy	iniciativa	k1gFnSc2	iniciativa
praetorů	praetor	k1gMnPc2	praetor
<g/>
.	.	kIx.	.
</s>
<s>
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
chtěl	chtít	k5eAaImAgMnS	chtít
však	však	k9	však
omezit	omezit	k5eAaPmF	omezit
veškerou	veškerý	k3xTgFnSc4	veškerý
normotvorbu	normotvorba	k1gFnSc4	normotvorba
jejím	její	k3xOp3gNnSc7	její
podřízením	podřízení	k1gNnSc7	podřízení
císařově	císařův	k2eAgFnSc3d1	císařova
kontrole	kontrola	k1gFnSc3	kontrola
<g/>
.	.	kIx.	.
</s>
<s>
Pověřil	pověřit	k5eAaPmAgMnS	pověřit
proto	proto	k8xC	proto
tehdejšího	tehdejší	k2eAgMnSc2d1	tehdejší
význačného	význačný	k2eAgMnSc2d1	význačný
právníka	právník	k1gMnSc2	právník
Publia	Publius	k1gMnSc2	Publius
Salvia	Salvius	k1gMnSc2	Salvius
Juliana	Julian	k1gMnSc2	Julian
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
doposud	doposud	k6eAd1	doposud
každoročně	každoročně	k6eAd1	každoročně
publikované	publikovaný	k2eAgInPc4d1	publikovaný
a	a	k8xC	a
obměňované	obměňovaný	k2eAgInPc4d1	obměňovaný
edikty	edikt	k1gInPc4	edikt
magistrátů	magistrát	k1gInPc2	magistrát
redigoval	redigovat	k5eAaImAgMnS	redigovat
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
vznikl	vzniknout	k5eAaPmAgInS	vzniknout
"	"	kIx"	"
<g/>
Hadriánův	Hadriánův	k2eAgInSc1d1	Hadriánův
věčný	věčný	k2eAgInSc1d1	věčný
edikt	edikt	k1gInSc1	edikt
<g/>
"	"	kIx"	"
(	(	kIx(	(
<g/>
edictum	edictum	k1gNnSc1	edictum
perpetuum	perpetuum	k1gNnSc3	perpetuum
Hadriani	Hadrian	k1gMnPc1	Hadrian
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
mohl	moct	k5eAaImAgMnS	moct
být	být	k5eAaImF	být
pozměňován	pozměňovat	k5eAaImNgMnS	pozměňovat
pouze	pouze	k6eAd1	pouze
se	s	k7c7	s
svolením	svolení	k1gNnSc7	svolení
císaře	císař	k1gMnSc2	císař
podle	podle	k7c2	podle
doporučení	doporučení	k1gNnSc2	doporučení
jemu	on	k3xPp3gMnSc3	on
podléhající	podléhající	k2eAgFnSc1d1	podléhající
a	a	k8xC	a
jím	jíst	k5eAaImIp1nS	jíst
vybírané	vybíraný	k2eAgFnPc4d1	vybíraná
skupiny	skupina	k1gFnPc4	skupina
expertů	expert	k1gMnPc2	expert
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
Hadrianova	Hadrianův	k2eAgFnSc1d1	Hadrianova
centralistická	centralistický	k2eAgFnSc1d1	centralistická
snaha	snaha	k1gFnSc1	snaha
měla	mít	k5eAaImAgFnS	mít
podstatný	podstatný	k2eAgInSc4d1	podstatný
dopad	dopad	k1gInSc4	dopad
na	na	k7c4	na
další	další	k2eAgInSc4d1	další
vývoj	vývoj	k1gInSc4	vývoj
římského	římský	k2eAgNnSc2d1	římské
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
přesné	přesný	k2eAgNnSc1d1	přesné
znění	znění	k1gNnSc1	znění
ediktu	edikt	k1gInSc2	edikt
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
právník	právník	k1gMnSc1	právník
Ulpianus	Ulpianus	k1gMnSc1	Ulpianus
k	k	k7c3	k
němu	on	k3xPp3gMnSc3	on
vyhotovil	vyhotovit	k5eAaPmAgMnS	vyhotovit
rozsáhlý	rozsáhlý	k2eAgInSc4d1	rozsáhlý
komentář	komentář	k1gInSc4	komentář
obsahující	obsahující	k2eAgFnSc2d1	obsahující
četné	četný	k2eAgFnSc2d1	četná
citace	citace	k1gFnSc2	citace
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
byly	být	k5eAaImAgInP	být
později	pozdě	k6eAd2	pozdě
zahrnuty	zahrnout	k5eAaPmNgInP	zahrnout
do	do	k7c2	do
Justiniánových	Justiniánův	k2eAgFnPc2d1	Justiniánova
Digest	Digest	k1gFnSc4	Digest
<g/>
.	.	kIx.	.
</s>
<s>
Ráznou	rázný	k2eAgFnSc7d1	rázná
proměnou	proměna	k1gFnSc7	proměna
prošla	projít	k5eAaPmAgFnS	projít
císařská	císařský	k2eAgFnSc1d1	císařská
administrativa	administrativa	k1gFnSc1	administrativa
<g/>
,	,	kIx,	,
nabývající	nabývající	k2eAgFnSc7d1	nabývající
za	za	k7c4	za
Hadriana	Hadrian	k1gMnSc4	Hadrian
stále	stále	k6eAd1	stále
byrokratičtější	byrokratický	k2eAgInSc4d2	byrokratický
ráz	ráz	k1gInSc4	ráz
<g/>
.	.	kIx.	.
</s>
<s>
Přesnějším	přesný	k2eAgNnSc7d2	přesnější
vymezením	vymezení	k1gNnSc7	vymezení
kompetencí	kompetence	k1gFnPc2	kompetence
úředníků	úředník	k1gMnPc2	úředník
a	a	k8xC	a
zavedením	zavedení	k1gNnSc7	zavedení
fixních	fixní	k2eAgInPc2d1	fixní
ročních	roční	k2eAgInPc2d1	roční
platů	plat	k1gInPc2	plat
se	se	k3xPyFc4	se
státní	státní	k2eAgFnSc1d1	státní
správa	správa	k1gFnSc1	správa
stala	stát	k5eAaPmAgFnS	stát
stabilnější	stabilní	k2eAgFnSc1d2	stabilnější
a	a	k8xC	a
přestala	přestat	k5eAaPmAgFnS	přestat
být	být	k5eAaImF	být
tolik	tolik	k6eAd1	tolik
ovlivňována	ovlivňovat	k5eAaImNgFnS	ovlivňovat
změnami	změna	k1gFnPc7	změna
na	na	k7c6	na
trůnu	trůn	k1gInSc6	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Poradní	poradní	k2eAgInSc1d1	poradní
sbor	sbor	k1gInSc1	sbor
císaře	císař	k1gMnSc2	císař
(	(	kIx(	(
<g/>
consilium	consilium	k1gNnSc1	consilium
principis	principis	k1gFnPc2	principis
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
existující	existující	k2eAgFnSc4d1	existující
od	od	k7c2	od
dob	doba	k1gFnPc2	doba
Augusta	August	k1gMnSc2	August
<g/>
,	,	kIx,	,
povýšil	povýšit	k5eAaPmAgMnS	povýšit
na	na	k7c4	na
stálý	stálý	k2eAgInSc4d1	stálý
orgán	orgán	k1gInSc4	orgán
<g/>
,	,	kIx,	,
složený	složený	k2eAgInSc4d1	složený
zvláště	zvláště	k6eAd1	zvláště
ze	z	k7c2	z
znalců	znalec	k1gMnPc2	znalec
v	v	k7c6	v
oboru	obor	k1gInSc6	obor
práva	právo	k1gNnSc2	právo
<g/>
.	.	kIx.	.
</s>
<s>
Správu	správa	k1gFnSc4	správa
Itálie	Itálie	k1gFnSc2	Itálie
přiblížil	přiblížit	k5eAaPmAgInS	přiblížit
Hadrianus	Hadrianus	k1gInSc1	Hadrianus
provinciím	provincie	k1gFnPc3	provincie
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
Itálii	Itálie	k1gFnSc6	Itálie
zřídil	zřídit	k5eAaPmAgMnS	zřídit
čtyři	čtyři	k4xCgInPc4	čtyři
soudní	soudní	k2eAgInPc4d1	soudní
obvody	obvod	k1gInPc4	obvod
<g/>
,	,	kIx,	,
do	do	k7c2	do
jejichž	jejichž	k3xOyRp3gNnSc2	jejichž
čela	čelo	k1gNnSc2	čelo
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
bývalé	bývalý	k2eAgMnPc4d1	bývalý
konzuly	konzul	k1gMnPc4	konzul
<g/>
.	.	kIx.	.
</s>
<s>
Císařovým	Císařův	k2eAgInSc7d1	Císařův
záměrem	záměr	k1gInSc7	záměr
nebylo	být	k5eNaImAgNnS	být
ovšem	ovšem	k9	ovšem
znehodnocení	znehodnocení	k1gNnSc1	znehodnocení
výsadního	výsadní	k2eAgNnSc2d1	výsadní
postavení	postavení	k1gNnSc2	postavení
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
zrovnoprávnění	zrovnoprávnění	k1gNnSc1	zrovnoprávnění
ostatních	ostatní	k2eAgFnPc2d1	ostatní
částí	část	k1gFnPc2	část
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Od	od	k7c2	od
příslušníků	příslušník	k1gMnPc2	příslušník
jezdeckého	jezdecký	k2eAgInSc2d1	jezdecký
stavu	stav	k1gInSc2	stav
(	(	kIx(	(
<g/>
equites	equites	k1gInSc1	equites
<g/>
)	)	kIx)	)
přestala	přestat	k5eAaPmAgFnS	přestat
být	být	k5eAaImF	být
vyžadována	vyžadován	k2eAgFnSc1d1	vyžadována
vojenská	vojenský	k2eAgFnSc1d1	vojenská
služba	služba	k1gFnSc1	služba
jako	jako	k8xC	jako
nezbytný	nezbytný	k2eAgInSc1d1	nezbytný
předpoklad	předpoklad	k1gInSc1	předpoklad
k	k	k7c3	k
civilní	civilní	k2eAgFnSc3d1	civilní
kariéře	kariéra	k1gFnSc3	kariéra
<g/>
.	.	kIx.	.
</s>
<s>
Schopní	schopný	k2eAgMnPc1d1	schopný
jezdci	jezdec	k1gMnPc1	jezdec
vytlačovali	vytlačovat	k5eAaImAgMnP	vytlačovat
z	z	k7c2	z
císařské	císařský	k2eAgFnSc2d1	císařská
administrativy	administrativa	k1gFnSc2	administrativa
propuštěnce	propuštěnec	k1gMnSc2	propuštěnec
a	a	k8xC	a
dokonce	dokonce	k9	dokonce
zastávali	zastávat	k5eAaImAgMnP	zastávat
členství	členství	k1gNnSc4	členství
v	v	k7c6	v
Hadrianově	Hadrianův	k2eAgInSc6d1	Hadrianův
poradním	poradní	k2eAgInSc6d1	poradní
sboru	sbor	k1gInSc6	sbor
<g/>
.	.	kIx.	.
</s>
<s>
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
velmi	velmi	k6eAd1	velmi
pečoval	pečovat	k5eAaImAgMnS	pečovat
o	o	k7c4	o
úsporné	úsporný	k2eAgNnSc4d1	úsporné
nakládání	nakládání	k1gNnSc4	nakládání
se	s	k7c7	s
státními	státní	k2eAgInPc7d1	státní
prostředky	prostředek	k1gInPc7	prostředek
<g/>
.	.	kIx.	.
</s>
<s>
Nechal	nechat	k5eAaPmAgMnS	nechat
prověřit	prověřit	k5eAaPmF	prověřit
finanční	finanční	k2eAgInPc4d1	finanční
účty	účet	k1gInPc4	účet
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
objevil	objevit	k5eAaPmAgMnS	objevit
ohromnou	ohromný	k2eAgFnSc4d1	ohromná
sumu	suma	k1gFnSc4	suma
daňových	daňový	k2eAgInPc2d1	daňový
nedoplatků	nedoplatek	k1gInPc2	nedoplatek
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gInPc1	jejichž
výše	výše	k1gFnSc1	výše
činila	činit	k5eAaImAgFnS	činit
kolem	kolem	k7c2	kolem
900	[number]	k4	900
milionů	milion	k4xCgInPc2	milion
sesterciů	sestercius	k1gInPc2	sestercius
<g/>
.	.	kIx.	.
</s>
<s>
Místo	místo	k7c2	místo
vymáhání	vymáhání	k1gNnSc2	vymáhání
těchto	tento	k3xDgFnPc2	tento
pohledávek	pohledávka	k1gFnPc2	pohledávka
se	se	k3xPyFc4	se
je	on	k3xPp3gFnPc4	on
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
odepsat	odepsat	k5eAaPmF	odepsat
jako	jako	k9	jako
nedobytné	dobytný	k2eNgInPc1d1	nedobytný
<g/>
.	.	kIx.	.
</s>
<s>
Záznamy	záznam	k1gInPc1	záznam
o	o	k7c6	o
těchto	tento	k3xDgInPc6	tento
dluzích	dluh	k1gInPc6	dluh
byly	být	k5eAaImAgFnP	být
následně	následně	k6eAd1	následně
veřejně	veřejně	k6eAd1	veřejně
spáleny	spálen	k2eAgInPc1d1	spálen
<g/>
,	,	kIx,	,
což	což	k3yQnSc1	což
zvýšilo	zvýšit	k5eAaPmAgNnS	zvýšit
císařovu	císařův	k2eAgFnSc4d1	císařova
popularitu	popularita	k1gFnSc4	popularita
mezi	mezi	k7c7	mezi
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
<g/>
.	.	kIx.	.
</s>
<s>
Jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
také	také	k9	také
úředníka	úředník	k1gMnSc4	úředník
s	s	k7c7	s
titulem	titul	k1gInSc7	titul
advocatus	advocatus	k1gInSc1	advocatus
fisci	fisce	k1gMnSc3	fisce
<g/>
,	,	kIx,	,
který	který	k3yRgMnSc1	který
zodpovídal	zodpovídat	k5eAaPmAgMnS	zodpovídat
za	za	k7c4	za
kontrolu	kontrola	k1gFnSc4	kontrola
hospodaření	hospodaření	k1gNnSc2	hospodaření
císařské	císařský	k2eAgFnSc2d1	císařská
pokladny	pokladna	k1gFnSc2	pokladna
(	(	kIx(	(
<g/>
fiscus	fiscus	k1gInSc1	fiscus
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
letech	léto	k1gNnPc6	léto
Hadrianovy	Hadrianův	k2eAgFnSc2d1	Hadrianova
vlády	vláda	k1gFnSc2	vláda
se	se	k3xPyFc4	se
dále	daleko	k6eAd2	daleko
setřely	setřít	k5eAaPmAgInP	setřít
rozdíly	rozdíl	k1gInPc1	rozdíl
mezi	mezi	k7c7	mezi
fiscem	fisce	k1gMnSc7	fisce
a	a	k8xC	a
senátory	senátor	k1gMnPc7	senátor
spravovanou	spravovaný	k2eAgFnSc7d1	spravovaná
státní	státní	k2eAgFnSc7d1	státní
kasou	kasa	k1gFnSc7	kasa
(	(	kIx(	(
<g/>
aerarium	aerarium	k1gNnSc1	aerarium
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
svého	svůj	k3xOyFgNnSc2	svůj
panování	panování	k1gNnSc2	panování
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
prokázal	prokázat	k5eAaPmAgMnS	prokázat
výjimečnou	výjimečný	k2eAgFnSc4d1	výjimečná
zálibu	záliba	k1gFnSc4	záliba
v	v	k7c6	v
cestování	cestování	k1gNnSc6	cestování
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
navštívil	navštívit	k5eAaPmAgMnS	navštívit
mnohé	mnohý	k2eAgInPc4d1	mnohý
vzdálené	vzdálený	k2eAgInPc4d1	vzdálený
kouty	kout	k1gInPc4	kout
impéria	impérium	k1gNnSc2	impérium
a	a	k8xC	a
po	po	k7c4	po
celá	celý	k2eAgNnPc4d1	celé
léta	léto	k1gNnPc4	léto
prodléval	prodlévat	k5eAaImAgInS	prodlévat
mimo	mimo	k7c4	mimo
Itálii	Itálie	k1gFnSc4	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
základě	základ	k1gInSc6	základ
zkušeností	zkušenost	k1gFnPc2	zkušenost
z	z	k7c2	z
vlastní	vlastní	k2eAgFnSc2d1	vlastní
vojenské	vojenský	k2eAgFnSc2d1	vojenská
služby	služba	k1gFnSc2	služba
podnikl	podniknout	k5eAaPmAgMnS	podniknout
řadu	řada	k1gFnSc4	řada
inspekcí	inspekce	k1gFnPc2	inspekce
vojenských	vojenský	k2eAgInPc2d1	vojenský
sborů	sbor	k1gInPc2	sbor
rozložených	rozložený	k2eAgInPc2d1	rozložený
v	v	k7c6	v
různých	různý	k2eAgFnPc6d1	různá
provinciích	provincie	k1gFnPc6	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Zatímco	zatímco	k8xS	zatímco
dřívější	dřívější	k2eAgMnPc1d1	dřívější
císařové	císař	k1gMnPc1	císař
opouštěli	opouštět	k5eAaImAgMnP	opouštět
Řím	Řím	k1gInSc4	Řím
většinou	většinou	k6eAd1	většinou
jen	jen	k9	jen
v	v	k7c6	v
případě	případ	k1gInSc6	případ
válečných	válečný	k2eAgNnPc2d1	válečné
střetnutí	střetnutí	k1gNnPc2	střetnutí
<g/>
,	,	kIx,	,
po	po	k7c6	po
jejichž	jejichž	k3xOyRp3gNnSc6	jejichž
skončení	skončení	k1gNnSc6	skončení
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
vraceli	vracet	k5eAaImAgMnP	vracet
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
strávil	strávit	k5eAaPmAgMnS	strávit
na	na	k7c6	na
cestách	cesta	k1gFnPc6	cesta
více	hodně	k6eAd2	hodně
než	než	k8xS	než
polovinu	polovina	k1gFnSc4	polovina
období	období	k1gNnSc2	období
své	svůj	k3xOyFgFnSc2	svůj
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
než	než	k8xS	než
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgMnS	vydat
z	z	k7c2	z
Říma	Řím	k1gInSc2	Řím
<g/>
,	,	kIx,	,
získal	získat	k5eAaPmAgMnS	získat
si	se	k3xPyFc3	se
loajalitu	loajalita	k1gFnSc4	loajalita
vyšších	vysoký	k2eAgFnPc2d2	vyšší
vrstev	vrstva	k1gFnPc2	vrstva
římské	římský	k2eAgFnSc2d1	římská
společnosti	společnost	k1gFnSc2	společnost
a	a	k8xC	a
jmenoval	jmenovat	k5eAaImAgMnS	jmenovat
pretoriánským	pretoriánský	k2eAgMnSc7d1	pretoriánský
prefektem	prefekt	k1gMnSc7	prefekt
spolehlivého	spolehlivý	k2eAgMnSc2d1	spolehlivý
vojenského	vojenský	k2eAgMnSc2d1	vojenský
veterána	veterán	k1gMnSc2	veterán
Quinta	Quint	k1gMnSc2	Quint
Marcia	Marcius	k1gMnSc2	Marcius
Turbona	Turbon	k1gMnSc2	Turbon
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
svěřil	svěřit	k5eAaPmAgInS	svěřit
dohled	dohled	k1gInSc1	dohled
nad	nad	k7c7	nad
svými	svůj	k3xOyFgInPc7	svůj
zájmy	zájem	k1gInPc7	zájem
v	v	k7c6	v
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tvrzení	tvrzení	k1gNnSc2	tvrzení
antických	antický	k2eAgInPc2d1	antický
pramenů	pramen	k1gInPc2	pramen
měly	mít	k5eAaImAgFnP	mít
za	za	k7c2	za
Hadrianovy	Hadrianův	k2eAgFnSc2d1	Hadrianova
nepřítomnosti	nepřítomnost	k1gFnSc2	nepřítomnost
udržovat	udržovat	k5eAaImF	udržovat
pořádek	pořádek	k1gInSc4	pořádek
v	v	k7c6	v
Římě	Řím	k1gInSc6	Řím
vojenské	vojenský	k2eAgInPc4d1	vojenský
oddíly	oddíl	k1gInPc4	oddíl
<g/>
,	,	kIx,	,
nazývané	nazývaný	k2eAgFnSc6d1	nazývaná
frumentarii	frumentarie	k1gFnSc6	frumentarie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
putování	putování	k1gNnSc6	putování
po	po	k7c6	po
provinciích	provincie	k1gFnPc6	provincie
Hadrianus	Hadrianus	k1gInSc1	Hadrianus
přikazoval	přikazovat	k5eAaImAgInS	přikazovat
budovat	budovat	k5eAaImF	budovat
veřejné	veřejný	k2eAgFnPc4d1	veřejná
stavby	stavba	k1gFnPc4	stavba
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
byl	být	k5eAaImAgMnS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
posílí	posílit	k5eAaPmIp3nS	posílit
stát	stát	k5eAaImF	stát
spíše	spíše	k9	spíše
zdokonalením	zdokonalení	k1gNnSc7	zdokonalení
jeho	jeho	k3xOp3gFnSc2	jeho
infrastruktury	infrastruktura	k1gFnSc2	infrastruktura
<g/>
,	,	kIx,	,
než	než	k8xS	než
dobýváním	dobývání	k1gNnSc7	dobývání
či	či	k8xC	či
anexí	anexe	k1gFnSc7	anexe
cizích	cizí	k2eAgFnPc2d1	cizí
zemí	zem	k1gFnPc2	zem
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgFnSc1d1	hlavní
náplň	náplň	k1gFnSc1	náplň
jeho	jeho	k3xOp3gFnPc2	jeho
cest	cesta	k1gFnPc2	cesta
obvykle	obvykle	k6eAd1	obvykle
spočívala	spočívat	k5eAaImAgFnS	spočívat
v	v	k7c6	v
iniciování	iniciování	k1gNnSc6	iniciování
nových	nový	k2eAgFnPc2d1	nová
stavebních	stavební	k2eAgFnPc2d1	stavební
děl	dělo	k1gNnPc2	dělo
<g/>
,	,	kIx,	,
navrhování	navrhování	k1gNnSc1	navrhování
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
zakládání	zakládání	k1gNnSc1	zakládání
osad	osada	k1gFnPc2	osada
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
zkrášlování	zkrášlování	k1gNnSc1	zkrášlování
řeckých	řecký	k2eAgNnPc2d1	řecké
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Hadriana	Hadriana	k1gFnSc1	Hadriana
doprovázel	doprovázet	k5eAaImAgInS	doprovázet
velký	velký	k2eAgInSc1d1	velký
počet	počet	k1gInSc1	počet
úředníků	úředník	k1gMnPc2	úředník
<g/>
,	,	kIx,	,
architektů	architekt	k1gMnPc2	architekt
a	a	k8xC	a
stavitelů	stavitel	k1gMnPc2	stavitel
<g/>
,	,	kIx,	,
jejichž	jejichž	k3xOyRp3gNnSc1	jejichž
ubytování	ubytování	k1gNnSc1	ubytování
a	a	k8xC	a
obstarání	obstarání	k1gNnSc1	obstarání
výživy	výživa	k1gFnSc2	výživa
způsobilo	způsobit	k5eAaPmAgNnS	způsobit
krajům	kraj	k1gInPc3	kraj
<g/>
,	,	kIx,	,
do	do	k7c2	do
nichž	jenž	k3xRgMnPc2	jenž
zavítali	zavítat	k5eAaPmAgMnP	zavítat
<g/>
,	,	kIx,	,
nemalé	malý	k2eNgInPc1d1	nemalý
dodatečné	dodatečný	k2eAgInPc1d1	dodatečný
výdaje	výdaj	k1gInPc1	výdaj
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
císařův	císařův	k2eAgInSc1d1	císařův
pobyt	pobyt	k1gInSc1	pobyt
přinášel	přinášet	k5eAaImAgInS	přinášet
velké	velký	k2eAgFnPc4d1	velká
části	část	k1gFnPc4	část
místních	místní	k2eAgMnPc2d1	místní
občanů	občan	k1gMnPc2	občan
značný	značný	k2eAgInSc4d1	značný
prospěch	prospěch	k1gInSc4	prospěch
<g/>
,	,	kIx,	,
ostatní	ostatní	k2eAgFnSc1d1	ostatní
populace	populace	k1gFnSc1	populace
byla	být	k5eAaImAgFnS	být
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
více	hodně	k6eAd2	hodně
zatížena	zatížit	k5eAaPmNgFnS	zatížit
daněmi	daň	k1gFnPc7	daň
a	a	k8xC	a
odvody	odvod	k1gInPc7	odvod
jiného	jiný	k2eAgInSc2d1	jiný
druhu	druh	k1gInSc2	druh
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
Hadrianově	Hadrianův	k2eAgFnSc6d1	Hadrianova
návštěvě	návštěva	k1gFnSc6	návštěva
Egypta	Egypt	k1gInSc2	Egypt
bylo	být	k5eAaImAgNnS	být
zabaveno	zabaven	k2eAgNnSc4d1	zabaveno
obrovské	obrovský	k2eAgNnSc4d1	obrovské
množství	množství	k1gNnSc4	množství
potravin	potravina	k1gFnPc2	potravina
<g/>
,	,	kIx,	,
takže	takže	k8xS	takže
nastal	nastat	k5eAaPmAgInS	nastat
hlad	hlad	k1gInSc1	hlad
a	a	k8xC	a
prudké	prudký	k2eAgNnSc1d1	prudké
zhoršení	zhoršení	k1gNnSc1	zhoršení
životních	životní	k2eAgFnPc2d1	životní
podmínek	podmínka	k1gFnPc2	podmínka
nejchudších	chudý	k2eAgMnPc2d3	nejchudší
obyvatel	obyvatel	k1gMnPc2	obyvatel
<g/>
.	.	kIx.	.
</s>
<s>
První	první	k4xOgFnSc1	první
Hadrianova	Hadrianův	k2eAgFnSc1d1	Hadrianova
cesta	cesta	k1gFnSc1	cesta
ve	v	k7c6	v
funkci	funkce	k1gFnSc6	funkce
císaře	císař	k1gMnSc2	císař
ho	on	k3xPp3gMnSc4	on
v	v	k7c6	v
roce	rok	k1gInSc6	rok
121	[number]	k4	121
zavedla	zavést	k5eAaPmAgFnS	zavést
do	do	k7c2	do
Germánie	Germánie	k1gFnSc2	Germánie
a	a	k8xC	a
Raetie	Raetie	k1gFnSc2	Raetie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
vykonal	vykonat	k5eAaPmAgInS	vykonat
kontrolu	kontrola	k1gFnSc4	kontrola
limitu	limit	k1gInSc2	limit
a	a	k8xC	a
vyčlenil	vyčlenit	k5eAaPmAgMnS	vyčlenit
zdroje	zdroj	k1gInSc2	zdroj
k	k	k7c3	k
jeho	jeho	k3xOp3gNnSc3	jeho
vylepšení	vylepšení	k1gNnSc3	vylepšení
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
se	se	k3xPyFc4	se
po	po	k7c6	po
naplnění	naplnění	k1gNnSc6	naplnění
cíle	cíl	k1gInSc2	cíl
této	tento	k3xDgFnSc2	tento
výpravy	výprava	k1gFnSc2	výprava
hodlal	hodlat	k5eAaImAgMnS	hodlat
zaobírat	zaobírat	k5eAaImF	zaobírat
svými	svůj	k3xOyFgInPc7	svůj
kulturními	kulturní	k2eAgInPc7d1	kulturní
zájmy	zájem	k1gInPc7	zájem
<g/>
,	,	kIx,	,
poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
se	se	k3xPyFc4	se
doslechl	doslechnout	k5eAaPmAgMnS	doslechnout
o	o	k7c6	o
probíhajícím	probíhající	k2eAgInSc6d1	probíhající
odboji	odboj	k1gInSc6	odboj
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
<g/>
,	,	kIx,	,
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
se	se	k3xPyFc4	se
odebrat	odebrat	k5eAaPmF	odebrat
do	do	k7c2	do
této	tento	k3xDgFnSc2	tento
provincie	provincie	k1gFnSc2	provincie
a	a	k8xC	a
osobně	osobně	k6eAd1	osobně
prověřit	prověřit	k5eAaPmF	prověřit
tamní	tamní	k2eAgInPc4d1	tamní
poměry	poměr	k1gInPc4	poměr
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
119	[number]	k4	119
vypuklo	vypuknout	k5eAaPmAgNnS	vypuknout
v	v	k7c6	v
Británii	Británie	k1gFnSc6	Británie
rozsáhlé	rozsáhlý	k2eAgNnSc1d1	rozsáhlé
povstání	povstání	k1gNnSc1	povstání
<g/>
,	,	kIx,	,
trvající	trvající	k2eAgMnSc1d1	trvající
více	hodně	k6eAd2	hodně
než	než	k8xS	než
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
<g/>
.	.	kIx.	.
</s>
<s>
Aby	aby	kYmCp3nS	aby
potlačil	potlačit	k5eAaPmAgMnS	potlačit
vzbouřence	vzbouřenec	k1gMnSc4	vzbouřenec
<g/>
,	,	kIx,	,
vyslal	vyslat	k5eAaPmAgMnS	vyslat
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
na	na	k7c4	na
ostrov	ostrov	k1gInSc4	ostrov
Quinta	Quint	k1gMnSc2	Quint
Pompeia	Pompeius	k1gMnSc2	Pompeius
Falcona	Falcon	k1gMnSc2	Falcon
<g/>
.	.	kIx.	.
</s>
<s>
Jakmile	jakmile	k8xS	jakmile
byli	být	k5eAaImAgMnP	být
Britanové	Britanový	k2eAgFnPc4d1	Britanový
zpacifikováni	zpacifikovat	k5eAaPmNgMnP	zpacifikovat
<g/>
,	,	kIx,	,
nařídil	nařídit	k5eAaPmAgMnS	nařídit
císař	císař	k1gMnSc1	císař
v	v	k7c6	v
roce	rok	k1gInSc6	rok
122	[number]	k4	122
zahájení	zahájení	k1gNnSc4	zahájení
výstavby	výstavba	k1gFnSc2	výstavba
Hadriánova	Hadriánův	k2eAgInSc2d1	Hadriánův
valu	val	k1gInSc2	val
<g/>
.	.	kIx.	.
</s>
<s>
Důvody	důvod	k1gInPc4	důvod
vytvoření	vytvoření	k1gNnSc2	vytvoření
tohoto	tento	k3xDgNnSc2	tento
díla	dílo	k1gNnSc2	dílo
se	se	k3xPyFc4	se
staly	stát	k5eAaPmAgFnP	stát
předmětem	předmět	k1gInSc7	předmět
debat	debata	k1gFnPc2	debata
moderních	moderní	k2eAgMnPc2d1	moderní
badatelů	badatel	k1gMnPc2	badatel
a	a	k8xC	a
historiků	historik	k1gMnPc2	historik
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
původně	původně	k6eAd1	původně
převládajícího	převládající	k2eAgInSc2d1	převládající
názoru	názor	k1gInSc2	názor
z	z	k7c2	z
konce	konec	k1gInSc2	konec
19	[number]	k4	19
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
sloužil	sloužit	k5eAaImAgInS	sloužit
val	val	k1gInSc1	val
pouze	pouze	k6eAd1	pouze
k	k	k7c3	k
vojenské	vojenský	k2eAgFnSc3d1	vojenská
obraně	obrana	k1gFnSc3	obrana
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
stanovisko	stanovisko	k1gNnSc1	stanovisko
bylo	být	k5eAaImAgNnS	být
však	však	k9	však
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
20	[number]	k4	20
<g/>
.	.	kIx.	.
století	století	k1gNnSc1	století
zpochybněno	zpochybněn	k2eAgNnSc1d1	zpochybněno
<g/>
.	.	kIx.	.
</s>
<s>
Mnozí	mnohý	k2eAgMnPc1d1	mnohý
autoři	autor	k1gMnPc1	autor
považovali	považovat	k5eAaImAgMnP	považovat
val	val	k1gInSc4	val
za	za	k7c4	za
symbolickou	symbolický	k2eAgFnSc4d1	symbolická
hranici	hranice	k1gFnSc4	hranice
římské	římský	k2eAgFnSc2d1	římská
moci	moc	k1gFnSc2	moc
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
za	za	k7c4	za
monument	monument	k1gInSc4	monument
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgInSc1	jenž
měl	mít	k5eAaImAgInS	mít
Hadrianovi	Hadrian	k1gMnSc3	Hadrian
zajistit	zajistit	k5eAaPmF	zajistit
slávu	sláva	k1gFnSc4	sláva
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
nedosáhl	dosáhnout	k5eNaPmAgInS	dosáhnout
ve	v	k7c6	v
válečném	válečný	k2eAgInSc6d1	válečný
konfliktu	konflikt	k1gInSc6	konflikt
<g/>
.	.	kIx.	.
</s>
<s>
Nelze	lze	k6eNd1	lze
vyloučit	vyloučit	k5eAaPmF	vyloučit
<g/>
,	,	kIx,	,
že	že	k8xS	že
vztyčením	vztyčení	k1gNnSc7	vztyčení
této	tento	k3xDgFnSc2	tento
zdi	zeď	k1gFnSc2	zeď
chtěl	chtít	k5eAaImAgMnS	chtít
císař	císař	k1gMnSc1	císař
zamezit	zamezit	k5eAaPmF	zamezit
zahálčivosti	zahálčivost	k1gFnSc3	zahálčivost
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
předejít	předejít	k5eAaPmF	předejít
tak	tak	k6eAd1	tak
možné	možný	k2eAgFnSc3d1	možná
vzpouře	vzpoura	k1gFnSc3	vzpoura
<g/>
.	.	kIx.	.
</s>
<s>
Případně	případně	k6eAd1	případně
se	se	k3xPyFc4	se
mohlo	moct	k5eAaImAgNnS	moct
jednat	jednat	k5eAaImF	jednat
o	o	k7c4	o
preventivní	preventivní	k2eAgNnPc4d1	preventivní
opatření	opatření	k1gNnPc4	opatření
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInSc7	jehož
cílem	cíl	k1gInSc7	cíl
bylo	být	k5eAaImAgNnS	být
zabránit	zabránit	k5eAaPmF	zabránit
pravděpodobným	pravděpodobný	k2eAgInPc3d1	pravděpodobný
budoucím	budoucí	k2eAgInPc3d1	budoucí
nájezdům	nájezd	k1gInPc3	nájezd
či	či	k8xC	či
nekontrolované	kontrolovaný	k2eNgFnSc3d1	nekontrolovaná
migraci	migrace	k1gFnSc3	migrace
kmenů	kmen	k1gInPc2	kmen
z	z	k7c2	z
Kaledonie	Kaledonie	k1gFnSc2	Kaledonie
(	(	kIx(	(
<g/>
dnešní	dnešní	k2eAgNnSc1d1	dnešní
Skotsko	Skotsko	k1gNnSc1	Skotsko
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
byl	být	k5eAaImAgMnS	být
přesvědčen	přesvědčit	k5eAaPmNgMnS	přesvědčit
<g/>
,	,	kIx,	,
že	že	k8xS	že
Kaledoňané	Kaledoňan	k1gMnPc1	Kaledoňan
odmítnou	odmítnout	k5eAaPmIp3nP	odmítnout
mírové	mírový	k2eAgNnSc4d1	Mírové
soužití	soužití	k1gNnSc4	soužití
s	s	k7c7	s
Římany	Říman	k1gMnPc7	Říman
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
druhou	druhý	k4xOgFnSc4	druhý
stranu	strana	k1gFnSc4	strana
si	se	k3xPyFc3	se
byl	být	k5eAaImAgMnS	být
vědom	vědom	k2eAgMnSc1d1	vědom
náročnosti	náročnost	k1gFnSc3	náročnost
a	a	k8xC	a
nákladnosti	nákladnost	k1gFnSc3	nákladnost
dobývání	dobývání	k1gNnSc2	dobývání
vysočiny	vysočina	k1gFnSc2	vysočina
na	na	k7c6	na
severu	sever	k1gInSc6	sever
<g/>
,	,	kIx,	,
stěží	stěží	k6eAd1	stěží
ospravedlnitelného	ospravedlnitelný	k2eAgInSc2d1	ospravedlnitelný
nepatrným	nepatrný	k2eAgInSc7d1	nepatrný
přínosem	přínos	k1gInSc7	přínos
<g/>
.	.	kIx.	.
</s>
<s>
Proto	proto	k8xC	proto
přikročil	přikročit	k5eAaPmAgMnS	přikročit
k	k	k7c3	k
přetnutí	přetnutí	k1gNnSc3	přetnutí
ostrova	ostrov	k1gInSc2	ostrov
obrannou	obranný	k2eAgFnSc7d1	obranná
linií	linie	k1gFnSc7	linie
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
byla	být	k5eAaImAgFnS	být
na	na	k7c4	na
rozdíl	rozdíl	k1gInSc4	rozdíl
od	od	k7c2	od
limitu	limit	k1gInSc2	limit
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
nedostatku	nedostatek	k1gInSc3	nedostatek
vhodného	vhodný	k2eAgNnSc2d1	vhodné
dřeva	dřevo	k1gNnSc2	dřevo
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
zkonstruována	zkonstruován	k2eAgNnPc1d1	zkonstruováno
z	z	k7c2	z
kamene	kámen	k1gInSc2	kámen
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
západní	západní	k2eAgInSc4d1	západní
úsek	úsek	k1gInSc4	úsek
valu	val	k1gInSc2	val
<g/>
,	,	kIx,	,
mezi	mezi	k7c7	mezi
nynějším	nynější	k2eAgNnSc7d1	nynější
městem	město	k1gNnSc7	město
Carlisle	Carlisle	k1gFnSc2	Carlisle
a	a	k8xC	a
řekou	řeka	k1gFnSc7	řeka
Irthing	Irthing	k1gInSc1	Irthing
<g/>
,	,	kIx,	,
tvořila	tvořit	k5eAaImAgFnS	tvořit
navršená	navršený	k2eAgFnSc1d1	navršená
zemina	zemina	k1gFnSc1	zemina
<g/>
.	.	kIx.	.
</s>
<s>
Vybudování	vybudování	k1gNnSc1	vybudování
tohoto	tento	k3xDgInSc2	tento
valu	val	k1gInSc2	val
<g/>
,	,	kIx,	,
jehož	jehož	k3xOyRp3gInPc1	jehož
pozůstatky	pozůstatek	k1gInPc1	pozůstatek
se	se	k3xPyFc4	se
dosud	dosud	k6eAd1	dosud
táhnou	táhnout	k5eAaImIp3nP	táhnout
krajinou	krajina	k1gFnSc7	krajina
v	v	k7c6	v
délce	délka	k1gFnSc6	délka
mnoha	mnoho	k4c2	mnoho
kilometrů	kilometr	k1gInPc2	kilometr
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
zřejmě	zřejmě	k6eAd1	zřejmě
nejproslulejším	proslulý	k2eAgInSc7d3	nejproslulejší
Hadrianovým	Hadrianův	k2eAgInSc7d1	Hadrianův
skutkem	skutek	k1gInSc7	skutek
<g/>
.	.	kIx.	.
</s>
<s>
Současně	současně	k6eAd1	současně
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c4	o
impozantní	impozantní	k2eAgInSc4d1	impozantní
doklad	doklad	k1gInSc4	doklad
Hadrianova	Hadrianův	k2eAgNnSc2d1	Hadrianovo
úsilí	úsilí	k1gNnSc2	úsilí
o	o	k7c6	o
ustálení	ustálení	k1gNnSc6	ustálení
hranic	hranice	k1gFnPc2	hranice
říše	říš	k1gFnSc2	říš
<g/>
,	,	kIx,	,
jemuž	jenž	k3xRgMnSc3	jenž
dal	dát	k5eAaPmAgInS	dát
přednost	přednost	k1gFnSc4	přednost
před	před	k7c7	před
vedením	vedení	k1gNnSc7	vedení
válek	válka	k1gFnPc2	válka
a	a	k8xC	a
podrobováním	podrobování	k1gNnSc7	podrobování
nových	nový	k2eAgNnPc2d1	nové
teritorií	teritorium	k1gNnPc2	teritorium
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c2	za
jeho	jeho	k3xOp3gFnSc2	jeho
vlády	vláda	k1gFnSc2	vláda
byla	být	k5eAaImAgFnS	být
ještě	ještě	k6eAd1	ještě
v	v	k7c6	v
Eboraku	Eborak	k1gInSc6	Eborak
(	(	kIx(	(
<g/>
York	York	k1gInSc1	York
<g/>
)	)	kIx)	)
zhotovena	zhotoven	k2eAgFnSc1d1	zhotovena
svatyně	svatyně	k1gFnSc1	svatyně
k	k	k7c3	k
uctívání	uctívání	k1gNnSc3	uctívání
bohyně	bohyně	k1gFnSc2	bohyně
Britannie	Britannie	k1gFnSc2	Britannie
<g/>
.	.	kIx.	.
</s>
<s>
Císař	Císař	k1gMnSc1	Císař
dal	dát	k5eAaPmAgMnS	dát
také	také	k9	také
razit	razit	k5eAaImF	razit
mince	mince	k1gFnPc4	mince
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
byla	být	k5eAaImAgFnS	být
vyobrazena	vyobrazen	k2eAgFnSc1d1	vyobrazena
ženská	ženský	k2eAgFnSc1d1	ženská
postava	postava	k1gFnSc1	postava
personifikující	personifikující	k2eAgFnSc4d1	personifikující
Británii	Británie	k1gFnSc4	Británie
<g/>
.	.	kIx.	.
</s>
<s>
Koncem	koncem	k7c2	koncem
roku	rok	k1gInSc2	rok
122	[number]	k4	122
Hadrianus	Hadrianus	k1gInSc1	Hadrianus
završil	završit	k5eAaPmAgInS	završit
svoji	svůj	k3xOyFgFnSc4	svůj
návštěvu	návštěva	k1gFnSc4	návštěva
na	na	k7c6	na
ostrově	ostrov	k1gInSc6	ostrov
a	a	k8xC	a
odplul	odplout	k5eAaPmAgMnS	odplout
na	na	k7c4	na
jih	jih	k1gInSc4	jih
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
123	[number]	k4	123
přicestoval	přicestovat	k5eAaPmAgMnS	přicestovat
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
do	do	k7c2	do
Mauretánie	Mauretánie	k1gFnSc2	Mauretánie
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
osobně	osobně	k6eAd1	osobně
vedl	vést	k5eAaImAgInS	vést
tažení	tažení	k1gNnSc4	tažení
proti	proti	k7c3	proti
domorodým	domorodý	k2eAgMnPc3d1	domorodý
povstalcům	povstalec	k1gMnPc3	povstalec
<g/>
.	.	kIx.	.
</s>
<s>
Císařův	Císařův	k2eAgInSc1d1	Císařův
pobyt	pobyt	k1gInSc1	pobyt
na	na	k7c6	na
tomto	tento	k3xDgInSc6	tento
místě	místo	k1gNnSc6	místo
byl	být	k5eAaImAgInS	být
brzy	brzy	k6eAd1	brzy
přerušen	přerušit	k5eAaPmNgInS	přerušit
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
ho	on	k3xPp3gMnSc4	on
tu	tu	k6eAd1	tu
zastihly	zastihnout	k5eAaPmAgFnP	zastihnout
zprávy	zpráva	k1gFnPc1	zpráva
o	o	k7c6	o
válečných	válečný	k2eAgFnPc6d1	válečná
přípravách	příprava	k1gFnPc6	příprava
Parthů	Parth	k1gMnPc2	Parth
<g/>
,	,	kIx,	,
kvůli	kvůli	k7c3	kvůli
čemuž	což	k3yQnSc3	což
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
vyrazil	vyrazit	k5eAaPmAgMnS	vyrazit
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
cesty	cesta	k1gFnSc2	cesta
učinil	učinit	k5eAaPmAgMnS	učinit
zastávku	zastávka	k1gFnSc4	zastávka
v	v	k7c6	v
Kyréně	Kyréna	k1gFnSc6	Kyréna
<g/>
,	,	kIx,	,
jejímž	jejíž	k3xOyRp3gMnPc3	jejíž
obyvatelům	obyvatel	k1gMnPc3	obyvatel
poskytl	poskytnout	k5eAaPmAgMnS	poskytnout
finanční	finanční	k2eAgInPc4d1	finanční
prostředky	prostředek	k1gInPc4	prostředek
k	k	k7c3	k
vojenskému	vojenský	k2eAgInSc3d1	vojenský
výcviku	výcvik	k1gInSc3	výcvik
zdejší	zdejší	k2eAgFnSc2d1	zdejší
mládeže	mládež	k1gFnSc2	mládež
<g/>
.	.	kIx.	.
</s>
<s>
Kyréné	Kyréné	k2eAgFnSc1d1	Kyréné
měla	mít	k5eAaImAgFnS	mít
již	již	k9	již
v	v	k7c6	v
předchozích	předchozí	k2eAgNnPc6d1	předchozí
letech	léto	k1gNnPc6	léto
prospěch	prospěch	k1gInSc4	prospěch
z	z	k7c2	z
císařovy	císařův	k2eAgFnSc2d1	císařova
štědrosti	štědrost	k1gFnSc2	štědrost
<g/>
,	,	kIx,	,
když	když	k8xS	když
v	v	k7c6	v
roce	rok	k1gInSc6	rok
119	[number]	k4	119
obdaroval	obdarovat	k5eAaPmAgMnS	obdarovat
město	město	k1gNnSc4	město
penězi	peníze	k1gInPc7	peníze
na	na	k7c4	na
rekonstrukci	rekonstrukce	k1gFnSc4	rekonstrukce
veřejných	veřejný	k2eAgFnPc2d1	veřejná
budov	budova	k1gFnPc2	budova
zničených	zničený	k2eAgFnPc2d1	zničená
při	při	k7c6	při
nedávném	dávný	k2eNgNnSc6d1	nedávné
židovském	židovský	k2eAgNnSc6d1	Židovské
povstání	povstání	k1gNnSc6	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
příchodu	příchod	k1gInSc6	příchod
k	k	k7c3	k
Eufratu	Eufrat	k1gInSc3	Eufrat
Hadrianus	Hadrianus	k1gInSc1	Hadrianus
urovnal	urovnat	k5eAaPmAgInS	urovnat
spory	spor	k1gInPc4	spor
s	s	k7c7	s
Parthy	Parth	k1gMnPc4	Parth
uzavřením	uzavření	k1gNnSc7	uzavření
smlouvy	smlouva	k1gFnSc2	smlouva
s	s	k7c7	s
králem	král	k1gMnSc7	král
Osroém	Osroý	k2eAgNnSc6d1	Osroý
I.	I.	kA	I.
Po	po	k7c6	po
vykonání	vykonání	k1gNnSc6	vykonání
kontroly	kontrola	k1gFnSc2	kontrola
římského	římský	k2eAgInSc2d1	římský
obranného	obranný	k2eAgInSc2d1	obranný
systému	systém	k1gInSc2	systém
se	se	k3xPyFc4	se
vydal	vydat	k5eAaPmAgMnS	vydat
na	na	k7c4	na
západ	západ	k1gInSc4	západ
podél	podél	k7c2	podél
pobřeží	pobřeží	k1gNnSc2	pobřeží
Černého	Černého	k2eAgNnSc2d1	Černého
moře	moře	k1gNnSc2	moře
<g/>
,	,	kIx,	,
načež	načež	k6eAd1	načež
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
Nikomédie	Nikomédie	k1gFnSc2	Nikomédie
<g/>
,	,	kIx,	,
hlavního	hlavní	k2eAgNnSc2d1	hlavní
města	město	k1gNnSc2	město
Bithýnie	Bithýnie	k1gFnSc2	Bithýnie
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
strávil	strávit	k5eAaPmAgMnS	strávit
nadcházející	nadcházející	k2eAgFnSc4d1	nadcházející
zimu	zima	k1gFnSc4	zima
<g/>
.	.	kIx.	.
</s>
<s>
Nikomédie	Nikomédie	k1gFnSc1	Nikomédie
se	se	k3xPyFc4	se
tehdy	tehdy	k6eAd1	tehdy
nacházela	nacházet	k5eAaImAgFnS	nacházet
v	v	k7c6	v
neutěšeném	utěšený	k2eNgInSc6d1	neutěšený
stavu	stav	k1gInSc6	stav
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
byla	být	k5eAaImAgNnP	být
zle	zle	k6eAd1	zle
poničena	poničit	k5eAaPmNgNnP	poničit
zemětřesením	zemětřesení	k1gNnSc7	zemětřesení
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgNnSc3	jenž
došlo	dojít	k5eAaPmAgNnS	dojít
krátce	krátce	k6eAd1	krátce
před	před	k7c7	před
císařovým	císařův	k2eAgInSc7d1	císařův
příjezdem	příjezd	k1gInSc7	příjezd
<g/>
.	.	kIx.	.
</s>
<s>
Hadrianus	Hadrianus	k1gInSc1	Hadrianus
opět	opět	k6eAd1	opět
projevil	projevit	k5eAaPmAgInS	projevit
svoji	svůj	k3xOyFgFnSc4	svůj
štědrost	štědrost	k1gFnSc4	štědrost
a	a	k8xC	a
věnoval	věnovat	k5eAaPmAgMnS	věnovat
městu	město	k1gNnSc3	město
potřebné	potřebný	k2eAgInPc1d1	potřebný
prostředky	prostředek	k1gInPc1	prostředek
na	na	k7c4	na
opravy	oprava	k1gFnPc4	oprava
<g/>
.	.	kIx.	.
</s>
<s>
Místním	místní	k2eAgNnSc7d1	místní
obyvatelstvem	obyvatelstvo	k1gNnSc7	obyvatelstvo
byl	být	k5eAaImAgInS	být
následně	následně	k6eAd1	následně
oslavován	oslavovat	k5eAaImNgInS	oslavovat
jako	jako	k9	jako
"	"	kIx"	"
<g/>
obnovitel	obnovitel	k1gMnSc1	obnovitel
Orientu	Orient	k1gInSc2	Orient
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Zřejmě	zřejmě	k6eAd1	zřejmě
navštívil	navštívit	k5eAaPmAgMnS	navštívit
rovněž	rovněž	k9	rovněž
město	město	k1gNnSc4	město
Claudiopolis	Claudiopolis	k1gFnSc2	Claudiopolis
(	(	kIx(	(
<g/>
Bolu	bol	k1gInSc2	bol
<g/>
)	)	kIx)	)
a	a	k8xC	a
zde	zde	k6eAd1	zde
se	se	k3xPyFc4	se
seznámil	seznámit	k5eAaPmAgMnS	seznámit
s	s	k7c7	s
pohledným	pohledný	k2eAgInSc7d1	pohledný
Antinoem	Antino	k1gInSc7	Antino
<g/>
,	,	kIx,	,
asi	asi	k9	asi
třinácti	třináct	k4xCc7	třináct
nebo	nebo	k8xC	nebo
čtrnáctiletým	čtrnáctiletý	k2eAgInSc7d1	čtrnáctiletý
mladíkem	mladík	k1gInSc7	mladík
<g/>
,	,	kIx,	,
s	s	k7c7	s
nímž	jenž	k3xRgMnSc7	jenž
udržoval	udržovat	k5eAaImAgMnS	udržovat
homosexuální	homosexuální	k2eAgInPc4d1	homosexuální
styky	styk	k1gInPc4	styk
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
setkání	setkání	k1gNnSc6	setkání
s	s	k7c7	s
Antinoem	Antinoum	k1gNnSc7	Antinoum
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
pokračoval	pokračovat	k5eAaImAgMnS	pokračovat
v	v	k7c6	v
cestě	cesta	k1gFnSc6	cesta
po	po	k7c6	po
Anatolii	Anatolie	k1gFnSc6	Anatolie
<g/>
.	.	kIx.	.
</s>
<s>
Při	při	k7c6	při
svém	svůj	k3xOyFgNnSc6	svůj
putování	putování	k1gNnSc6	putování
založil	založit	k5eAaPmAgMnS	založit
v	v	k7c6	v
Mýsii	Mýsie	k1gFnSc6	Mýsie
město	město	k1gNnSc1	město
pojmenované	pojmenovaný	k2eAgFnSc2d1	pojmenovaná
Hadrianutherae	Hadrianuthera	k1gFnSc2	Hadrianuthera
<g/>
,	,	kIx,	,
jímž	jenž	k3xRgNnSc7	jenž
oslavil	oslavit	k5eAaPmAgInS	oslavit
vydařený	vydařený	k2eAgInSc4d1	vydařený
lov	lov	k1gInSc4	lov
kance	kanec	k1gMnSc2	kanec
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	dlouho	k6eNd1	dlouho
nato	nato	k6eAd1	nato
nechal	nechat	k5eAaPmAgMnS	nechat
z	z	k7c2	z
oslnivě	oslnivě	k6eAd1	oslnivě
bílého	bílý	k2eAgInSc2d1	bílý
mramoru	mramor	k1gInSc2	mramor
zřídit	zřídit	k5eAaPmF	zřídit
chrám	chrám	k1gInSc1	chrám
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
byl	být	k5eAaImAgInS	být
uctíván	uctívat	k5eAaImNgInS	uctívat
spolu	spolu	k6eAd1	spolu
s	s	k7c7	s
Traianem	Traian	k1gInSc7	Traian
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c4	na
podzim	podzim	k1gInSc4	podzim
124	[number]	k4	124
se	se	k3xPyFc4	se
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
přesunul	přesunout	k5eAaPmAgMnS	přesunout
do	do	k7c2	do
Řecka	Řecko	k1gNnSc2	Řecko
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
byl	být	k5eAaImAgInS	být
zasvěcen	zasvětit	k5eAaPmNgInS	zasvětit
do	do	k7c2	do
eleusinských	eleusinský	k2eAgNnPc2d1	eleusinský
mystérií	mystérium	k1gNnPc2	mystérium
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
tradice	tradice	k1gFnSc2	tradice
měli	mít	k5eAaImAgMnP	mít
být	být	k5eAaImF	být
účastníci	účastník	k1gMnPc1	účastník
v	v	k7c6	v
určité	určitý	k2eAgFnSc6d1	určitá
fázi	fáze	k1gFnSc6	fáze
ceremonie	ceremonie	k1gFnSc2	ceremonie
ozbrojeni	ozbrojit	k5eAaPmNgMnP	ozbrojit
<g/>
,	,	kIx,	,
od	od	k7c2	od
čehož	což	k3yRnSc2	což
však	však	k9	však
bylo	být	k5eAaImAgNnS	být
upuštěno	upustit	k5eAaPmNgNnS	upustit
v	v	k7c6	v
zájmu	zájem	k1gInSc6	zájem
předejití	předejití	k1gNnSc2	předejití
možného	možný	k2eAgNnSc2d1	možné
ohrožení	ohrožení	k1gNnSc2	ohrožení
císařova	císařův	k2eAgInSc2d1	císařův
života	život	k1gInSc2	život
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
Hadrianus	Hadrianus	k1gInSc1	Hadrianus
vyhověl	vyhovět	k5eAaPmAgInS	vyhovět
žádosti	žádost	k1gFnSc3	žádost
Athéňanů	Athéňan	k1gMnPc2	Athéňan
a	a	k8xC	a
provedl	provést	k5eAaPmAgMnS	provést
revizi	revize	k1gFnSc4	revize
jejich	jejich	k3xOp3gFnSc2	jejich
ústavy	ústava	k1gFnSc2	ústava
<g/>
,	,	kIx,	,
kromě	kromě	k7c2	kromě
jiného	jiné	k1gNnSc2	jiné
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nová	nový	k2eAgFnSc1d1	nová
fýla	fýla	k1gFnSc1	fýla
nesoucí	nesoucí	k2eAgFnSc1d1	nesoucí
císařovo	císařův	k2eAgNnSc4d1	císařovo
jméno	jméno	k1gNnSc4	jméno
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
průběhu	průběh	k1gInSc6	průběh
zimy	zima	k1gFnSc2	zima
procestoval	procestovat	k5eAaPmAgInS	procestovat
Peloponés	Peloponés	k1gInSc1	Peloponés
<g/>
,	,	kIx,	,
o	o	k7c6	o
čemž	což	k3yRnSc6	což
se	se	k3xPyFc4	se
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
Pausaniás	Pausaniás	k1gInSc1	Pausaniás
<g/>
.	.	kIx.	.
</s>
<s>
Vděční	vděčný	k2eAgMnPc1d1	vděčný
občané	občan	k1gMnPc1	občan
Epidauru	Epidaur	k1gInSc2	Epidaur
věnovali	věnovat	k5eAaImAgMnP	věnovat
císaři	císař	k1gMnPc1	císař
sochu	socha	k1gFnSc4	socha
jako	jako	k8xC	jako
projev	projev	k1gInSc4	projev
díků	dík	k1gInPc2	dík
svému	svůj	k3xOyFgInSc3	svůj
"	"	kIx"	"
<g/>
obnoviteli	obnovitel	k1gMnSc6	obnovitel
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Mimořádnou	mimořádný	k2eAgFnSc4d1	mimořádná
štědrost	štědrost	k1gFnSc4	štědrost
projevil	projevit	k5eAaPmAgInS	projevit
Hadrianus	Hadrianus	k1gInSc1	Hadrianus
vůči	vůči	k7c3	vůči
Mantineii	Mantineie	k1gFnSc3	Mantineie
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
125	[number]	k4	125
vstoupil	vstoupit	k5eAaPmAgMnS	vstoupit
do	do	k7c2	do
Athén	Athéna	k1gFnPc2	Athéna
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
mu	on	k3xPp3gMnSc3	on
náleželo	náležet	k5eAaImAgNnS	náležet
přední	přední	k2eAgNnSc4d1	přední
místo	místo	k1gNnSc4	místo
při	při	k7c6	při
probíhajícím	probíhající	k2eAgInSc6d1	probíhající
festivalu	festival	k1gInSc6	festival
Dionýsii	Dionýsie	k1gFnSc4	Dionýsie
<g/>
.	.	kIx.	.
</s>
<s>
Taktéž	Taktéž	k?	Taktéž
inicioval	iniciovat	k5eAaBmAgInS	iniciovat
výstavbu	výstavba	k1gFnSc4	výstavba
četných	četný	k2eAgFnPc2d1	četná
veřejných	veřejný	k2eAgFnPc2d1	veřejná
budov	budova	k1gFnPc2	budova
<g/>
,	,	kIx,	,
včetně	včetně	k7c2	včetně
akvaduktu	akvadukt	k1gInSc2	akvadukt
<g/>
,	,	kIx,	,
a	a	k8xC	a
především	především	k6eAd1	především
dal	dát	k5eAaPmAgMnS	dát
podnět	podnět	k1gInSc4	podnět
k	k	k7c3	k
dokončení	dokončení	k1gNnSc3	dokončení
chrámu	chrám	k1gInSc2	chrám
Dia	Dia	k1gMnSc2	Dia
Olympského	olympský	k2eAgMnSc2d1	olympský
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
Řecka	Řecko	k1gNnSc2	Řecko
se	se	k3xPyFc4	se
Hadrianus	Hadrianus	k1gInSc1	Hadrianus
odebral	odebrat	k5eAaPmAgInS	odebrat
po	po	k7c6	po
moři	moře	k1gNnSc6	moře
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
při	při	k7c6	při
plavbě	plavba	k1gFnSc6	plavba
se	se	k3xPyFc4	se
zastavil	zastavit	k5eAaPmAgMnS	zastavit
na	na	k7c6	na
Sicílii	Sicílie	k1gFnSc6	Sicílie
<g/>
.	.	kIx.	.
</s>
<s>
Ačkoli	ačkoli	k8xS	ačkoli
tam	tam	k6eAd1	tam
bylo	být	k5eAaImAgNnS	být
nalezeno	naleznout	k5eAaPmNgNnS	naleznout
mnoho	mnoho	k4c1	mnoho
mincí	mince	k1gFnPc2	mince
oslavujících	oslavující	k2eAgInPc2d1	oslavující
císaře	císař	k1gMnSc2	císař
<g/>
,	,	kIx,	,
není	být	k5eNaImIp3nS	být
známo	znám	k2eAgNnSc1d1	známo
<g/>
,	,	kIx,	,
jakým	jaký	k3yIgInSc7	jaký
činem	čin	k1gInSc7	čin
si	se	k3xPyFc3	se
vysloužil	vysloužit	k5eAaPmAgMnS	vysloužit
tuto	tento	k3xDgFnSc4	tento
poctu	pocta	k1gFnSc4	pocta
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
návratu	návrat	k1gInSc6	návrat
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
byl	být	k5eAaImAgInS	být
přítomen	přítomen	k2eAgInSc1d1	přítomen
završení	završení	k1gNnSc2	završení
přestavby	přestavba	k1gFnPc4	přestavba
Pantheonu	Pantheon	k1gInSc2	Pantheon
<g/>
.	.	kIx.	.
</s>
<s>
Záhy	záhy	k6eAd1	záhy
potom	potom	k6eAd1	potom
byly	být	k5eAaImAgFnP	být
ukončeny	ukončit	k5eAaPmNgFnP	ukončit
také	také	k9	také
práce	práce	k1gFnPc1	práce
na	na	k7c6	na
Hadriánově	Hadriánův	k2eAgFnSc6d1	Hadriánova
vile	vila	k1gFnSc6	vila
v	v	k7c6	v
Sabinských	Sabinský	k2eAgFnPc6d1	Sabinský
horách	hora	k1gFnPc6	hora
<g/>
,	,	kIx,	,
v	v	k7c6	v
níž	jenž	k3xRgFnSc6	jenž
císař	císař	k1gMnSc1	císař
nalezl	nalézt	k5eAaBmAgMnS	nalézt
klidné	klidný	k2eAgNnSc4d1	klidné
útočiště	útočiště	k1gNnSc4	útočiště
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
března	březen	k1gInSc2	březen
127	[number]	k4	127
se	se	k3xPyFc4	se
Hadrianus	Hadrianus	k1gInSc1	Hadrianus
vypravil	vypravit	k5eAaPmAgInS	vypravit
na	na	k7c4	na
okružní	okružní	k2eAgFnSc4d1	okružní
jízdu	jízda	k1gFnSc4	jízda
po	po	k7c6	po
Itálii	Itálie	k1gFnSc6	Itálie
<g/>
.	.	kIx.	.
</s>
<s>
Stejně	stejně	k6eAd1	stejně
jako	jako	k9	jako
při	při	k7c6	při
předchozích	předchozí	k2eAgFnPc6d1	předchozí
cestách	cesta	k1gFnPc6	cesta
jsou	být	k5eAaImIp3nP	být
badatelé	badatel	k1gMnPc1	badatel
schopni	schopen	k2eAgMnPc1d1	schopen
určit	určit	k5eAaPmF	určit
trasu	trasa	k1gFnSc4	trasa
císařova	císařův	k2eAgInSc2d1	císařův
postupu	postup	k1gInSc2	postup
spíše	spíše	k9	spíše
z	z	k7c2	z
jeho	jeho	k3xOp3gInPc2	jeho
skutků	skutek	k1gInPc2	skutek
<g/>
,	,	kIx,	,
než	než	k8xS	než
z	z	k7c2	z
historických	historický	k2eAgInPc2d1	historický
záznamů	záznam	k1gInPc2	záznam
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
picenském	picenský	k2eAgNnSc6d1	picenský
městě	město	k1gNnSc6	město
Cupra	Cupr	k1gMnSc2	Cupr
Maritima	Maritim	k1gMnSc2	Maritim
opět	opět	k6eAd1	opět
zavedl	zavést	k5eAaPmAgInS	zavést
kult	kult	k1gInSc1	kult
bohyně	bohyně	k1gFnSc2	bohyně
Cupry	Cupra	k1gFnSc2	Cupra
a	a	k8xC	a
nechal	nechat	k5eAaPmAgInS	nechat
zdokonalit	zdokonalit	k5eAaPmF	zdokonalit
odvodňovací	odvodňovací	k2eAgInSc1d1	odvodňovací
systém	systém	k1gInSc1	systém
Fucinského	Fucinský	k2eAgNnSc2d1	Fucinský
jezera	jezero	k1gNnSc2	jezero
<g/>
.	.	kIx.	.
</s>
<s>
Nepříliš	příliš	k6eNd1	příliš
velké	velký	k2eAgNnSc1d1	velké
nadšení	nadšení	k1gNnSc1	nadšení
vyvolalo	vyvolat	k5eAaPmAgNnS	vyvolat
rozdělení	rozdělení	k1gNnSc4	rozdělení
Itálie	Itálie	k1gFnSc2	Itálie
na	na	k7c4	na
čtyři	čtyři	k4xCgInPc4	čtyři
správní	správní	k2eAgInPc4d1	správní
regiony	region	k1gInPc4	region
<g/>
,	,	kIx,	,
řízené	řízený	k2eAgInPc1d1	řízený
císařskými	císařský	k2eAgInPc7d1	císařský
legáty	legát	k1gInPc7	legát
v	v	k7c6	v
hodnosti	hodnost	k1gFnSc6	hodnost
bývalých	bývalý	k2eAgMnPc2d1	bývalý
konzulů	konzul	k1gMnPc2	konzul
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
toto	tento	k3xDgNnSc1	tento
nepopulární	populární	k2eNgNnSc1d1	nepopulární
opatření	opatření	k1gNnSc1	opatření
setrvalo	setrvat	k5eAaPmAgNnS	setrvat
v	v	k7c6	v
platnosti	platnost	k1gFnSc6	platnost
pouze	pouze	k6eAd1	pouze
za	za	k7c2	za
Hadrianovy	Hadrianův	k2eAgFnSc2d1	Hadrianova
vlády	vláda	k1gFnSc2	vláda
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
v	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
onemocněl	onemocnět	k5eAaPmAgMnS	onemocnět
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
povaha	povaha	k1gFnSc1	povaha
jeho	jeho	k3xOp3gFnSc2	jeho
choroby	choroba	k1gFnSc2	choroba
není	být	k5eNaImIp3nS	být
známa	znám	k2eAgFnSc1d1	známa
<g/>
.	.	kIx.	.
</s>
<s>
Zdravotní	zdravotní	k2eAgInSc1d1	zdravotní
stav	stav	k1gInSc1	stav
přesto	přesto	k8xC	přesto
císaři	císař	k1gMnSc3	císař
nezabránil	zabránit	k5eNaPmAgMnS	zabránit
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
zamýšlenou	zamýšlený	k2eAgFnSc4d1	zamýšlená
návštěvu	návštěva	k1gFnSc4	návštěva
Afriky	Afrika	k1gFnSc2	Afrika
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
došlo	dojít	k5eAaPmAgNnS	dojít
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
128	[number]	k4	128
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gInSc1	jeho
příchod	příchod	k1gInSc1	příchod
byl	být	k5eAaImAgInS	být
doprovázen	doprovázen	k2eAgInSc1d1	doprovázen
příznivým	příznivý	k2eAgNnSc7d1	příznivé
znamením	znamení	k1gNnSc7	znamení
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
deště	dešť	k1gInSc2	dešť
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
ukončil	ukončit	k5eAaPmAgMnS	ukončit
dlouhotrvající	dlouhotrvající	k2eAgNnSc4d1	dlouhotrvající
sucho	sucho	k1gNnSc4	sucho
<g/>
.	.	kIx.	.
</s>
<s>
Vedle	vedle	k7c2	vedle
své	svůj	k3xOyFgFnSc2	svůj
obvyklé	obvyklý	k2eAgFnSc2d1	obvyklá
úlohy	úloha	k1gFnSc2	úloha
patrona	patron	k1gMnSc2	patron
a	a	k8xC	a
obnovitele	obnovitel	k1gMnSc2	obnovitel
měst	město	k1gNnPc2	město
si	se	k3xPyFc3	se
vyhradil	vyhradit	k5eAaPmAgInS	vyhradit
čas	čas	k1gInSc4	čas
i	i	k9	i
na	na	k7c4	na
inspekci	inspekce	k1gFnSc4	inspekce
vojsk	vojsko	k1gNnPc2	vojsko
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
létě	léto	k1gNnSc6	léto
téhož	týž	k3xTgInSc2	týž
roku	rok	k1gInSc2	rok
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Itálie	Itálie	k1gFnSc2	Itálie
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
jeho	jeho	k3xOp3gInSc1	jeho
pobyt	pobyt	k1gInSc1	pobyt
zde	zde	k6eAd1	zde
měl	mít	k5eAaImAgInS	mít
jen	jen	k9	jen
krátké	krátký	k2eAgNnSc4d1	krátké
trvání	trvání	k1gNnSc4	trvání
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
se	se	k3xPyFc4	se
brzy	brzy	k6eAd1	brzy
vydal	vydat	k5eAaPmAgInS	vydat
znovu	znovu	k6eAd1	znovu
na	na	k7c4	na
východ	východ	k1gInSc4	východ
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
září	září	k1gNnSc6	září
128	[number]	k4	128
se	se	k3xPyFc4	se
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
opět	opět	k6eAd1	opět
účastnil	účastnit	k5eAaImAgMnS	účastnit
eleusinských	eleusinský	k2eAgNnPc2d1	eleusinský
mystérií	mystérium	k1gNnPc2	mystérium
<g/>
.	.	kIx.	.
</s>
<s>
Jak	jak	k6eAd1	jak
se	se	k3xPyFc4	se
zdá	zdát	k5eAaImIp3nS	zdát
<g/>
,	,	kIx,	,
při	při	k7c6	při
této	tento	k3xDgFnSc6	tento
návštěvě	návštěva	k1gFnSc6	návštěva
Řecka	Řecko	k1gNnSc2	Řecko
věnoval	věnovat	k5eAaPmAgInS	věnovat
hlavní	hlavní	k2eAgFnSc4d1	hlavní
pozornost	pozornost	k1gFnSc4	pozornost
Athénám	Athéna	k1gFnPc3	Athéna
a	a	k8xC	a
Spartě	Sparta	k1gFnSc3	Sparta
<g/>
,	,	kIx,	,
dvěma	dva	k4xCgMnPc3	dva
dávným	dávný	k2eAgMnPc3d1	dávný
rivalům	rival	k1gMnPc3	rival
<g/>
,	,	kIx,	,
kdysi	kdysi	k6eAd1	kdysi
soupeřícím	soupeřící	k2eAgInSc7d1	soupeřící
o	o	k7c6	o
hegemonii	hegemonie	k1gFnSc6	hegemonie
<g/>
.	.	kIx.	.
</s>
<s>
Třebaže	třebaže	k8xS	třebaže
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
zvažoval	zvažovat	k5eAaImAgMnS	zvažovat
myšlenku	myšlenka	k1gFnSc4	myšlenka
oživení	oživení	k1gNnSc2	oživení
delfské	delfský	k2eAgFnSc2d1	delfská
amfiktyonie	amfiktyonie	k1gFnSc2	amfiktyonie
<g/>
,	,	kIx,	,
nakonec	nakonec	k6eAd1	nakonec
se	se	k3xPyFc4	se
rozhodl	rozhodnout	k5eAaPmAgMnS	rozhodnout
jinak	jinak	k6eAd1	jinak
<g/>
.	.	kIx.	.
</s>
<s>
Jím	jíst	k5eAaImIp1nS	jíst
navržená	navržený	k2eAgFnSc1d1	navržená
rada	rada	k1gFnSc1	rada
<g/>
,	,	kIx,	,
nazvaná	nazvaný	k2eAgFnSc1d1	nazvaná
Panhellenion	Panhellenion	k1gInSc1	Panhellenion
<g/>
,	,	kIx,	,
měla	mít	k5eAaImAgFnS	mít
sdružovat	sdružovat	k5eAaImF	sdružovat
zástupce	zástupce	k1gMnPc4	zástupce
všech	všecek	k3xTgNnPc2	všecek
řeckých	řecký	k2eAgNnPc2d1	řecké
měst	město	k1gNnPc2	město
<g/>
.	.	kIx.	.
</s>
<s>
Shromáždění	shromáždění	k1gNnSc1	shromáždění
této	tento	k3xDgFnSc2	tento
rady	rada	k1gFnSc2	rada
měla	mít	k5eAaImAgFnS	mít
probíhat	probíhat	k5eAaImF	probíhat
v	v	k7c6	v
chrámu	chrám	k1gInSc6	chrám
Dia	Dia	k1gFnPc2	Dia
Olympského	olympský	k2eAgNnSc2d1	Olympské
<g/>
.	.	kIx.	.
</s>
<s>
Poté	poté	k6eAd1	poté
<g/>
,	,	kIx,	,
co	co	k3yRnSc1	co
zahájil	zahájit	k5eAaPmAgMnS	zahájit
přípravy	příprava	k1gFnSc2	příprava
ke	k	k7c3	k
vzniku	vznik	k1gInSc3	vznik
Panhellenia	Panhellenium	k1gNnSc2	Panhellenium
<g/>
,	,	kIx,	,
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
do	do	k7c2	do
Efesu	Efes	k1gInSc2	Efes
<g/>
,	,	kIx,	,
odkud	odkud	k6eAd1	odkud
pokračoval	pokračovat	k5eAaImAgInS	pokračovat
do	do	k7c2	do
východních	východní	k2eAgFnPc6d1	východní
provinciích	provincie	k1gFnPc6	provincie
<g/>
.	.	kIx.	.
</s>
<s>
Během	během	k7c2	během
Hadrianova	Hadrianův	k2eAgInSc2d1	Hadrianův
pobytu	pobyt	k1gInSc2	pobyt
v	v	k7c6	v
Egyptě	Egypt	k1gInSc6	Egypt
se	se	k3xPyFc4	se
Antinoos	Antinoos	k1gInSc1	Antinoos
v	v	k7c6	v
říjnu	říjen	k1gInSc6	říjen
130	[number]	k4	130
utopil	utopit	k5eAaPmAgMnS	utopit
v	v	k7c6	v
Nilu	Nil	k1gInSc6	Nil
<g/>
.	.	kIx.	.
</s>
<s>
Příčiny	příčina	k1gFnPc1	příčina
této	tento	k3xDgFnSc3	tento
tragické	tragický	k2eAgFnSc3d1	tragická
události	událost	k1gFnSc3	událost
zůstávají	zůstávat	k5eAaImIp3nP	zůstávat
nejasné	jasný	k2eNgInPc1d1	nejasný
<g/>
.	.	kIx.	.
</s>
<s>
Pravděpodobně	pravděpodobně	k6eAd1	pravděpodobně
se	se	k3xPyFc4	se
jednalo	jednat	k5eAaImAgNnS	jednat
o	o	k7c4	o
nešťastnou	šťastný	k2eNgFnSc4d1	nešťastná
náhodu	náhoda	k1gFnSc4	náhoda
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
vyskytly	vyskytnout	k5eAaPmAgFnP	vyskytnout
se	se	k3xPyFc4	se
rovněž	rovněž	k9	rovněž
domněnky	domněnka	k1gFnPc1	domněnka
o	o	k7c6	o
možné	možný	k2eAgFnSc6d1	možná
sebevraždě	sebevražda	k1gFnSc6	sebevražda
<g/>
,	,	kIx,	,
Antinoově	Antinoův	k2eAgNnSc6d1	Antinoův
sebeobětování	sebeobětování	k1gNnSc6	sebeobětování
či	či	k8xC	či
vraždě	vražda	k1gFnSc6	vražda
<g/>
.	.	kIx.	.
</s>
<s>
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
propadl	propadnout	k5eAaPmAgMnS	propadnout
hlubokému	hluboký	k2eAgInSc3d1	hluboký
zármutku	zármutek	k1gInSc3	zármutek
a	a	k8xC	a
nařídil	nařídit	k5eAaPmAgMnS	nařídit
vzdávat	vzdávat	k5eAaImF	vzdávat
zesnulému	zesnulý	k1gMnSc3	zesnulý
úctu	úcta	k1gFnSc4	úcta
jako	jako	k8xC	jako
bohovi	bůh	k1gMnSc3	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
Antinoovi	Antinoa	k1gMnSc6	Antinoa
byla	být	k5eAaImAgNnP	být
pojmenována	pojmenován	k2eAgNnPc1d1	pojmenováno
města	město	k1gNnPc1	město
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
byly	být	k5eAaImAgInP	být
vyraženy	vyražen	k2eAgInPc1d1	vyražen
medailony	medailon	k1gInPc1	medailon
s	s	k7c7	s
jeho	jeho	k3xOp3gInSc7	jeho
portrétem	portrét	k1gInSc7	portrét
a	a	k8xC	a
v	v	k7c6	v
mnoha	mnoho	k4c6	mnoho
částech	část	k1gFnPc6	část
říše	říš	k1gFnSc2	říš
byly	být	k5eAaImAgFnP	být
vztyčeny	vztyčit	k5eAaPmNgFnP	vztyčit
jeho	jeho	k3xOp3gFnPc1	jeho
sochy	socha	k1gFnPc1	socha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Bithýnii	Bithýnie	k1gFnSc6	Bithýnie
<g/>
,	,	kIx,	,
Mantineii	Mantineie	k1gFnSc6	Mantineie
a	a	k8xC	a
Athénách	Athéna	k1gFnPc6	Athéna
byly	být	k5eAaImAgInP	být
vytvořeny	vytvořen	k2eAgInPc1d1	vytvořen
chrámy	chrám	k1gInPc1	chrám
<g/>
,	,	kIx,	,
v	v	k7c6	v
nichž	jenž	k3xRgInPc6	jenž
byl	být	k5eAaImAgInS	být
uctíván	uctíván	k2eAgInSc1d1	uctíván
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
jeho	jeho	k3xOp3gFnSc6	jeho
počest	počest	k1gFnPc1	počest
byly	být	k5eAaImAgFnP	být
taktéž	taktéž	k?	taktéž
uspořádány	uspořádat	k5eAaPmNgFnP	uspořádat
četné	četný	k2eAgFnPc1d1	četná
slavnosti	slavnost	k1gFnPc1	slavnost
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
místě	místo	k1gNnSc6	místo
jeho	on	k3xPp3gInSc2	on
skonu	skon	k1gInSc2	skon
poručil	poručit	k5eAaPmAgMnS	poručit
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
založit	založit	k5eAaPmF	založit
město	město	k1gNnSc4	město
<g/>
,	,	kIx,	,
které	který	k3yQgNnSc1	který
bylo	být	k5eAaImAgNnS	být
pojmenováno	pojmenovat	k5eAaPmNgNnS	pojmenovat
Antinoúpolis	Antinoúpolis	k1gFnPc7	Antinoúpolis
<g/>
.	.	kIx.	.
</s>
<s>
Hadrianovy	Hadrianův	k2eAgInPc1d1	Hadrianův
kroky	krok	k1gInPc1	krok
po	po	k7c6	po
Antinoově	Antinoův	k2eAgFnSc6d1	Antinoův
smrti	smrt	k1gFnSc6	smrt
jsou	být	k5eAaImIp3nP	být
poněkud	poněkud	k6eAd1	poněkud
nejasné	jasný	k2eNgInPc1d1	nejasný
<g/>
.	.	kIx.	.
</s>
<s>
Zimu	zima	k1gFnSc4	zima
131	[number]	k4	131
až	až	k9	až
132	[number]	k4	132
přečkal	přečkat	k5eAaPmAgInS	přečkat
v	v	k7c6	v
Athénách	Athéna	k1gFnPc6	Athéna
a	a	k8xC	a
potom	potom	k6eAd1	potom
se	se	k3xPyFc4	se
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
134	[number]	k4	134
odcestoval	odcestovat	k5eAaPmAgMnS	odcestovat
na	na	k7c4	na
východ	východ	k1gInSc4	východ
kvůli	kvůli	k7c3	kvůli
židovskému	židovský	k2eAgNnSc3d1	Židovské
povstání	povstání	k1gNnSc3	povstání
<g/>
,	,	kIx,	,
k	k	k7c3	k
němuž	jenž	k3xRgInSc3	jenž
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
Judeji	Judea	k1gFnSc6	Judea
o	o	k7c4	o
dva	dva	k4xCgInPc4	dva
roky	rok	k1gInPc4	rok
dříve	dříve	k6eAd2	dříve
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
epigrafických	epigrafický	k2eAgInPc2d1	epigrafický
nápisů	nápis	k1gInPc2	nápis
lze	lze	k6eAd1	lze
doložit	doložit	k5eAaPmF	doložit
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
měl	mít	k5eAaImAgInS	mít
osobně	osobně	k6eAd1	osobně
vypravit	vypravit	k5eAaPmF	vypravit
v	v	k7c6	v
čele	čelo	k1gNnSc6	čelo
vojska	vojsko	k1gNnSc2	vojsko
proti	proti	k7c3	proti
povstalcům	povstalec	k1gMnPc3	povstalec
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
135	[number]	k4	135
pobýval	pobývat	k5eAaImAgMnS	pobývat
v	v	k7c6	v
Sýrii	Sýrie	k1gFnSc6	Sýrie
a	a	k8xC	a
odtud	odtud	k6eAd1	odtud
pak	pak	k6eAd1	pak
zamířil	zamířit	k5eAaPmAgMnS	zamířit
zpět	zpět	k6eAd1	zpět
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Související	související	k2eAgFnPc1d1	související
informace	informace	k1gFnPc1	informace
naleznete	nalézt	k5eAaBmIp2nP	nalézt
také	také	k9	také
v	v	k7c6	v
článku	článek	k1gInSc6	článek
Povstání	povstání	k1gNnSc2	povstání
Bar	bar	k1gInSc1	bar
Kochby	Kochba	k1gFnSc2	Kochba
<g/>
.	.	kIx.	.
</s>
<s>
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
navštívil	navštívit	k5eAaPmAgMnS	navštívit
v	v	k7c6	v
roce	rok	k1gInSc6	rok
130	[number]	k4	130
rozvaliny	rozvalina	k1gFnSc2	rozvalina
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
opuštěného	opuštěný	k2eAgInSc2d1	opuštěný
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
první	první	k4xOgFnSc2	první
židovské	židovský	k2eAgFnSc2d1	židovská
války	válka	k1gFnSc2	válka
<g/>
,	,	kIx,	,
probíhající	probíhající	k2eAgFnSc1d1	probíhající
v	v	k7c6	v
letech	léto	k1gNnPc6	léto
66	[number]	k4	66
až	až	k9	až
73	[number]	k4	73
<g/>
.	.	kIx.	.
</s>
<s>
Různé	různý	k2eAgInPc1d1	různý
antické	antický	k2eAgInPc1d1	antický
prameny	pramen	k1gInPc1	pramen
se	se	k3xPyFc4	se
rozcházejí	rozcházet	k5eAaImIp3nP	rozcházet
v	v	k7c6	v
otázce	otázka	k1gFnSc6	otázka
příčin	příčina	k1gFnPc2	příčina
Bar	bar	k1gInSc1	bar
Kochbova	Kochbův	k2eAgNnSc2d1	Kochbovo
povstání	povstání	k1gNnSc2	povstání
<g/>
.	.	kIx.	.
</s>
<s>
Cassius	Cassius	k1gMnSc1	Cassius
Dio	Dio	k1gMnSc1	Dio
připisoval	připisovat	k5eAaImAgMnS	připisovat
vypuknutí	vypuknutí	k1gNnSc4	vypuknutí
židovské	židovský	k2eAgFnSc2d1	židovská
revolty	revolta	k1gFnSc2	revolta
císařovu	císařův	k2eAgFnSc4d1	císařova
úmyslu	úmysl	k1gInSc6	úmysl
zřídit	zřídit	k5eAaPmF	zřídit
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
římskou	římský	k2eAgFnSc4d1	římská
kolonii	kolonie	k1gFnSc4	kolonie
<g/>
,	,	kIx,	,
pojmenovanou	pojmenovaný	k2eAgFnSc4d1	pojmenovaná
Aelia	Aelium	k1gNnPc1	Aelium
Capitolina	Capitolin	k2eAgFnSc1d1	Capitolin
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
troskách	troska	k1gFnPc6	troska
starého	starý	k2eAgInSc2d1	starý
židovského	židovský	k2eAgInSc2d1	židovský
Chrámu	chrám	k1gInSc2	chrám
vypáleného	vypálený	k2eAgInSc2d1	vypálený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
70	[number]	k4	70
měla	mít	k5eAaImAgFnS	mít
být	být	k5eAaImF	být
navíc	navíc	k6eAd1	navíc
vybudována	vybudován	k2eAgFnSc1d1	vybudována
Jovova	Jovův	k2eAgFnSc1d1	Jovova
svatyně	svatyně	k1gFnSc1	svatyně
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiných	jiný	k2eAgInPc2d1	jiný
zdrojů	zdroj	k1gInPc2	zdroj
vyvolal	vyvolat	k5eAaPmAgInS	vyvolat
mezi	mezi	k7c7	mezi
Židy	Žid	k1gMnPc7	Žid
nespokojenost	nespokojenost	k1gFnSc4	nespokojenost
Hadrianův	Hadrianův	k2eAgInSc4d1	Hadrianův
zákaz	zákaz	k1gInSc4	zákaz
tradičního	tradiční	k2eAgInSc2d1	tradiční
rituálu	rituál	k1gInSc2	rituál
obřízky	obřízka	k1gFnSc2	obřízka
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc7	jenž
Řekové	Řek	k1gMnPc1	Řek
a	a	k8xC	a
Římané	Říman	k1gMnPc1	Říman
pokládali	pokládat	k5eAaImAgMnP	pokládat
za	za	k7c4	za
barbarskou	barbarský	k2eAgFnSc4d1	barbarská
formu	forma	k1gFnSc4	forma
tělesného	tělesný	k2eAgNnSc2d1	tělesné
mrzačení	mrzačení	k1gNnSc2	mrzačení
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
ostře	ostro	k6eAd1	ostro
protižidovská	protižidovský	k2eAgFnSc1d1	protižidovská
politika	politika	k1gFnSc1	politika
zvedla	zvednout	k5eAaPmAgFnS	zvednout
v	v	k7c6	v
Judeji	Judea	k1gFnSc6	Judea
značný	značný	k2eAgInSc4d1	značný
odpor	odpor	k1gInSc4	odpor
<g/>
,	,	kIx,	,
do	do	k7c2	do
jehož	jenž	k3xRgNnSc2	jenž
čela	čelo	k1gNnSc2	čelo
se	se	k3xPyFc4	se
postavili	postavit	k5eAaPmAgMnP	postavit
Šimon	Šimon	k1gMnSc1	Šimon
bar	bar	k1gInSc4	bar
Kochba	Kochba	k1gMnSc1	Kochba
a	a	k8xC	a
Rabi	rabi	k1gMnSc1	rabi
Akiva	Akiva	k1gFnSc1	Akiva
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
132	[number]	k4	132
zachvátilo	zachvátit	k5eAaPmAgNnS	zachvátit
povstání	povstání	k1gNnSc1	povstání
s	s	k7c7	s
mimořádnou	mimořádný	k2eAgFnSc7d1	mimořádná
intenzitou	intenzita	k1gFnSc7	intenzita
celou	celý	k2eAgFnSc4d1	celá
zemi	zem	k1gFnSc4	zem
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
není	být	k5eNaImIp3nS	být
prokázáno	prokázat	k5eAaPmNgNnS	prokázat
<g/>
,	,	kIx,	,
zda	zda	k8xS	zda
se	se	k3xPyFc4	se
sám	sám	k3xTgMnSc1	sám
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
zapojil	zapojit	k5eAaPmAgMnS	zapojit
do	do	k7c2	do
konfliktu	konflikt	k1gInSc2	konflikt
<g/>
,	,	kIx,	,
povolal	povolat	k5eAaPmAgMnS	povolat
z	z	k7c2	z
Británie	Británie	k1gFnSc2	Británie
zdatného	zdatný	k2eAgMnSc2d1	zdatný
vojevůdce	vojevůdce	k1gMnSc2	vojevůdce
Sexta	sexta	k1gFnSc1	sexta
Julia	Julius	k1gMnSc2	Julius
Severa	Severa	k1gMnSc1	Severa
a	a	k8xC	a
do	do	k7c2	do
Judeje	Judea	k1gFnSc2	Judea
přikázal	přikázat	k5eAaPmAgInS	přikázat
přisunout	přisunout	k5eAaPmF	přisunout
početné	početný	k2eAgFnPc4d1	početná
posily	posila	k1gFnPc4	posila
z	z	k7c2	z
dunajské	dunajský	k2eAgFnSc2d1	Dunajská
hranice	hranice	k1gFnSc2	hranice
<g/>
.	.	kIx.	.
</s>
<s>
Římané	Říman	k1gMnPc1	Říman
nasadili	nasadit	k5eAaPmAgMnP	nasadit
proti	proti	k7c3	proti
povstalcům	povstalec	k1gMnPc3	povstalec
enormní	enormní	k2eAgNnSc4d1	enormní
množství	množství	k1gNnSc4	množství
vojáků	voják	k1gMnPc2	voják
a	a	k8xC	a
vojenské	vojenský	k2eAgFnSc2d1	vojenská
techniky	technika	k1gFnSc2	technika
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
při	při	k7c6	při
srážkách	srážka	k1gFnPc6	srážka
s	s	k7c7	s
fanatickými	fanatický	k2eAgMnPc7d1	fanatický
Židy	Žid	k1gMnPc7	Žid
utrpěli	utrpět	k5eAaPmAgMnP	utrpět
těžké	těžký	k2eAgFnPc4d1	těžká
ztráty	ztráta	k1gFnPc4	ztráta
<g/>
.	.	kIx.	.
</s>
<s>
Prameny	pramen	k1gInPc1	pramen
se	se	k3xPyFc4	se
dokonce	dokonce	k9	dokonce
zmiňují	zmiňovat	k5eAaImIp3nP	zmiňovat
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
Hadrianus	Hadrianus	k1gInSc1	Hadrianus
ve	v	k7c6	v
zprávě	zpráva	k1gFnSc6	zpráva
římskému	římský	k2eAgInSc3d1	římský
senátu	senát	k1gInSc3	senát
opomenul	opomenout	k5eAaPmAgInS	opomenout
uvést	uvést	k5eAaPmF	uvést
obvyklou	obvyklý	k2eAgFnSc4d1	obvyklá
frázi	fráze	k1gFnSc4	fráze
"	"	kIx"	"
<g/>
já	já	k3xPp1nSc1	já
a	a	k8xC	a
legie	legie	k1gFnSc1	legie
jsme	být	k5eAaImIp1nP	být
v	v	k7c6	v
pořádku	pořádek	k1gInSc6	pořádek
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Navzdory	navzdory	k7c3	navzdory
tomu	ten	k3xDgNnSc3	ten
byli	být	k5eAaImAgMnP	být
rebelové	rebel	k1gMnPc1	rebel
posléze	posléze	k6eAd1	posléze
zatlačeni	zatlačit	k5eAaPmNgMnP	zatlačit
do	do	k7c2	do
hornatějších	hornatý	k2eAgFnPc2d2	hornatější
oblastí	oblast	k1gFnPc2	oblast
Judeje	Judea	k1gFnSc2	Judea
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
135	[number]	k4	135
byli	být	k5eAaImAgMnP	být
po	po	k7c6	po
třech	tři	k4xCgInPc6	tři
letech	let	k1gInPc6	let
urputných	urputný	k2eAgInPc2d1	urputný
bojů	boj	k1gInPc2	boj
zdoláni	zdolán	k2eAgMnPc1d1	zdolán
<g/>
.	.	kIx.	.
</s>
<s>
K	k	k7c3	k
poslední	poslední	k2eAgFnSc3d1	poslední
bitvě	bitva	k1gFnSc3	bitva
došlo	dojít	k5eAaPmAgNnS	dojít
v	v	k7c6	v
Bejtaru	Bejtar	k1gInSc6	Bejtar
<g/>
,	,	kIx,	,
pevnosti	pevnost	k1gFnPc4	pevnost
nacházející	nacházející	k2eAgFnPc4d1	nacházející
se	se	k3xPyFc4	se
jihozápadně	jihozápadně	k6eAd1	jihozápadně
od	od	k7c2	od
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
.	.	kIx.	.
</s>
<s>
Město	město	k1gNnSc1	město
bylo	být	k5eAaImAgNnS	být
dobyto	dobýt	k5eAaPmNgNnS	dobýt
teprve	teprve	k6eAd1	teprve
po	po	k7c6	po
velice	velice	k6eAd1	velice
zdlouhavém	zdlouhavý	k2eAgNnSc6d1	zdlouhavé
obléhání	obléhání	k1gNnSc6	obléhání
<g/>
,	,	kIx,	,
po	po	k7c6	po
jehož	jehož	k3xOyRp3gNnSc6	jehož
skončení	skončení	k1gNnSc6	skončení
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
nedovolil	dovolit	k5eNaPmAgMnS	dovolit
Židům	Žid	k1gMnPc3	Žid
pohřbít	pohřbít	k5eAaPmF	pohřbít
své	svůj	k3xOyFgMnPc4	svůj
mrtvé	mrtvý	k1gMnPc4	mrtvý
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
nadsazených	nadsazený	k2eAgInPc2d1	nadsazený
údajů	údaj	k1gInPc2	údaj
Cassia	Cassius	k1gMnSc2	Cassius
Diona	Dion	k1gMnSc2	Dion
bylo	být	k5eAaImAgNnS	být
při	při	k7c6	při
tomto	tento	k3xDgNnSc6	tento
střetnutí	střetnutí	k1gNnSc1	střetnutí
zabito	zabít	k5eAaPmNgNnS	zabít
580	[number]	k4	580
000	[number]	k4	000
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
50	[number]	k4	50
opevněných	opevněný	k2eAgNnPc2d1	opevněné
měst	město	k1gNnPc2	město
a	a	k8xC	a
985	[number]	k4	985
vesnic	vesnice	k1gFnPc2	vesnice
mělo	mít	k5eAaImAgNnS	mít
být	být	k5eAaImF	být
zničeno	zničit	k5eAaPmNgNnS	zničit
<g/>
.	.	kIx.	.
</s>
<s>
Židé	Žid	k1gMnPc1	Žid
<g/>
,	,	kIx,	,
kteří	který	k3yRgMnPc1	který
přežili	přežít	k5eAaPmAgMnP	přežít
válečné	válečný	k2eAgNnSc4d1	válečné
běsnění	běsnění	k1gNnSc4	běsnění
a	a	k8xC	a
nebyli	být	k5eNaImAgMnP	být
prodáni	prodat	k5eAaPmNgMnP	prodat
do	do	k7c2	do
otroctví	otroctví	k1gNnSc2	otroctví
<g/>
,	,	kIx,	,
odešli	odejít	k5eAaPmAgMnP	odejít
do	do	k7c2	do
vyhnanství	vyhnanství	k1gNnSc2	vyhnanství
<g/>
,	,	kIx,	,
čímž	což	k3yRnSc7	což
byla	být	k5eAaImAgFnS	být
zcela	zcela	k6eAd1	zcela
redukována	redukovat	k5eAaBmNgFnS	redukovat
židovská	židovský	k2eAgFnSc1d1	židovská
populace	populace	k1gFnSc1	populace
ve	v	k7c6	v
Svaté	svatý	k2eAgFnSc6d1	svatá
zemi	zem	k1gFnSc6	zem
<g/>
.	.	kIx.	.
</s>
<s>
I	i	k9	i
po	po	k7c6	po
skončení	skončení	k1gNnSc6	skončení
války	válka	k1gFnSc2	válka
vytrval	vytrvat	k5eAaPmAgInS	vytrvat
Hadrianus	Hadrianus	k1gInSc1	Hadrianus
v	v	k7c6	v
perzekvování	perzekvování	k1gNnSc6	perzekvování
Židů	Žid	k1gMnPc2	Žid
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
se	se	k3xPyFc4	se
pokusil	pokusit	k5eAaPmAgMnS	pokusit
úplně	úplně	k6eAd1	úplně
vykořenit	vykořenit	k5eAaPmF	vykořenit
judaismus	judaismus	k1gInSc4	judaismus
<g/>
,	,	kIx,	,
v	v	k7c6	v
němž	jenž	k3xRgInSc6	jenž
spatřoval	spatřovat	k5eAaImAgMnS	spatřovat
hlavní	hlavní	k2eAgFnSc4d1	hlavní
příčinu	příčina	k1gFnSc4	příčina
opakujících	opakující	k2eAgFnPc2d1	opakující
se	se	k3xPyFc4	se
vzpour	vzpoura	k1gFnPc2	vzpoura
<g/>
.	.	kIx.	.
</s>
<s>
Židům	Žid	k1gMnPc3	Žid
tudíž	tudíž	k8xC	tudíž
pod	pod	k7c7	pod
hrozbou	hrozba	k1gFnSc7	hrozba
smrti	smrt	k1gFnSc2	smrt
zakázal	zakázat	k5eAaPmAgInS	zakázat
vstup	vstup	k1gInSc1	vstup
do	do	k7c2	do
Jeruzaléma	Jeruzalém	k1gInSc2	Jeruzalém
<g/>
,	,	kIx,	,
odepřel	odepřít	k5eAaPmAgMnS	odepřít
jim	on	k3xPp3gMnPc3	on
vyučovat	vyučovat	k5eAaImF	vyučovat
Tóru	tóra	k1gFnSc4	tóra
a	a	k8xC	a
používat	používat	k5eAaImF	používat
židovský	židovský	k2eAgInSc4d1	židovský
kalendář	kalendář	k1gInSc4	kalendář
a	a	k8xC	a
také	také	k9	také
nechal	nechat	k5eAaPmAgMnS	nechat
popravit	popravit	k5eAaPmF	popravit
židovské	židovský	k2eAgMnPc4d1	židovský
mudrce	mudrc	k1gMnPc4	mudrc
<g/>
.	.	kIx.	.
</s>
<s>
Na	na	k7c6	na
Chrámové	chrámový	k2eAgFnSc6d1	chrámová
hoře	hora	k1gFnSc6	hora
bylo	být	k5eAaImAgNnS	být
provedeno	provést	k5eAaPmNgNnS	provést
obřadné	obřadný	k2eAgNnSc1d1	obřadné
spálení	spálení	k1gNnSc1	spálení
posvátných	posvátný	k2eAgInPc2d1	posvátný
židovských	židovský	k2eAgInPc2d1	židovský
svitků	svitek	k1gInPc2	svitek
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
místě	místo	k1gNnSc6	místo
<g/>
,	,	kIx,	,
na	na	k7c6	na
němž	jenž	k3xRgInSc6	jenž
stával	stávat	k5eAaImAgInS	stávat
Chrám	chrám	k1gInSc1	chrám
<g/>
,	,	kIx,	,
byly	být	k5eAaImAgFnP	být
vystaveny	vystaven	k2eAgFnPc1d1	vystavena
dvě	dva	k4xCgFnPc4	dva
sochy	socha	k1gFnPc4	socha
zpodobňující	zpodobňující	k2eAgMnSc1d1	zpodobňující
Jova	Jova	k1gMnSc1	Jova
a	a	k8xC	a
Hadriana	Hadriana	k1gFnSc1	Hadriana
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
snaze	snaha	k1gFnSc6	snaha
vymazat	vymazat	k5eAaPmF	vymazat
cokoli	cokoli	k3yInSc4	cokoli
<g/>
,	,	kIx,	,
co	co	k3yInSc1	co
by	by	kYmCp3nS	by
připomínalo	připomínat	k5eAaImAgNnS	připomínat
Židy	Žid	k1gMnPc4	Žid
<g/>
,	,	kIx,	,
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
odstranil	odstranit	k5eAaPmAgMnS	odstranit
dosavadní	dosavadní	k2eAgInSc4d1	dosavadní
název	název	k1gInSc4	název
provincie	provincie	k1gFnSc2	provincie
<g/>
,	,	kIx,	,
jíž	jenž	k3xRgFnSc3	jenž
podle	podle	k7c2	podle
filištínských	filištínský	k2eAgMnPc2d1	filištínský
nepřátel	nepřítel	k1gMnPc2	nepřítel
Židů	Žid	k1gMnPc2	Žid
přejmenoval	přejmenovat	k5eAaPmAgMnS	přejmenovat
na	na	k7c4	na
Sýrii	Sýrie	k1gFnSc4	Sýrie
Palestinu	Palestina	k1gFnSc4	Palestina
(	(	kIx(	(
<g/>
Syria	Syria	k1gFnSc1	Syria
Palaestina	Palaestina	k1gFnSc1	Palaestina
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
136	[number]	k4	136
se	se	k3xPyFc4	se
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
vrátil	vrátit	k5eAaPmAgMnS	vrátit
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
ze	z	k7c2	z
své	svůj	k3xOyFgFnSc2	svůj
poslední	poslední	k2eAgFnSc2d1	poslední
cesty	cesta	k1gFnSc2	cesta
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
této	tento	k3xDgFnSc6	tento
době	doba	k1gFnSc6	doba
již	již	k6eAd1	již
trpěl	trpět	k5eAaImAgMnS	trpět
vážnými	vážný	k2eAgFnPc7d1	vážná
zdravotními	zdravotní	k2eAgFnPc7d1	zdravotní
obtížemi	obtíž	k1gFnPc7	obtíž
a	a	k8xC	a
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
začal	začít	k5eAaPmAgInS	začít
zaobírat	zaobírat	k5eAaImF	zaobírat
úpravou	úprava	k1gFnSc7	úprava
nástupnictví	nástupnictví	k1gNnSc2	nástupnictví
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc1	jeho
volba	volba	k1gFnSc1	volba
překvapivě	překvapivě	k6eAd1	překvapivě
padla	padnout	k5eAaPmAgFnS	padnout
na	na	k7c4	na
prostopášného	prostopášný	k2eAgMnSc4d1	prostopášný
Lucia	Lucius	k1gMnSc4	Lucius
Ceionia	Ceionium	k1gNnSc2	Ceionium
Commoda	Commod	k1gMnSc4	Commod
<g/>
,	,	kIx,	,
řádného	řádný	k2eAgMnSc4d1	řádný
konzula	konzul	k1gMnSc4	konzul
onoho	onen	k3xDgInSc2	onen
roku	rok	k1gInSc2	rok
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
po	po	k7c6	po
své	svůj	k3xOyFgFnSc6	svůj
adopci	adopce	k1gFnSc6	adopce
přijal	přijmout	k5eAaPmAgMnS	přijmout
jméno	jméno	k1gNnSc4	jméno
Lucius	Lucius	k1gMnSc1	Lucius
Aelius	Aelius	k1gMnSc1	Aelius
Caesar	Caesar	k1gMnSc1	Caesar
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gMnSc7	jeho
tchánem	tchán	k1gMnSc7	tchán
byl	být	k5eAaImAgMnS	být
Gaius	Gaius	k1gMnSc1	Gaius
Avidius	Avidius	k1gMnSc1	Avidius
Nigrinus	Nigrinus	k1gMnSc1	Nigrinus
<g/>
,	,	kIx,	,
jeden	jeden	k4xCgMnSc1	jeden
ze	z	k7c2	z
čtyř	čtyři	k4xCgMnPc2	čtyři
konzulárů	konzulár	k1gMnPc2	konzulár
popravených	popravený	k2eAgMnPc2d1	popravený
v	v	k7c6	v
roce	rok	k1gInSc6	rok
118	[number]	k4	118
<g/>
.	.	kIx.	.
</s>
<s>
Aelius	Aelius	k1gMnSc1	Aelius
Caesar	Caesar	k1gMnSc1	Caesar
nevynikal	vynikat	k5eNaImAgMnS	vynikat
ničím	ničí	k3xOyNgMnPc3	ničí
jiným	jiný	k1gMnPc3	jiný
kromě	kromě	k7c2	kromě
vlivných	vlivný	k2eAgFnPc2d1	vlivná
politických	politický	k2eAgFnPc2d1	politická
konexí	konexe	k1gFnPc2	konexe
<g/>
.	.	kIx.	.
</s>
<s>
Protože	protože	k8xS	protože
veškerá	veškerý	k3xTgFnSc1	veškerý
jeho	jeho	k3xOp3gFnSc1	jeho
dosavadní	dosavadní	k2eAgFnSc1d1	dosavadní
kariéra	kariéra	k1gFnSc1	kariéra
byla	být	k5eAaImAgFnS	být
úzce	úzko	k6eAd1	úzko
spjata	spjat	k2eAgFnSc1d1	spjata
se	s	k7c7	s
senátem	senát	k1gInSc7	senát
<g/>
,	,	kIx,	,
naprosto	naprosto	k6eAd1	naprosto
postrádal	postrádat	k5eAaImAgMnS	postrádat
vojenské	vojenský	k2eAgFnPc4d1	vojenská
zkušenosti	zkušenost	k1gFnPc4	zkušenost
<g/>
.	.	kIx.	.
</s>
<s>
Záhy	záhy	k6eAd1	záhy
byl	být	k5eAaImAgMnS	být
obdařen	obdařit	k5eAaPmNgMnS	obdařit
tribunskou	tribunský	k2eAgFnSc7d1	tribunská
mocí	moc	k1gFnSc7	moc
(	(	kIx(	(
<g/>
tribunitia	tribunitia	k1gFnSc1	tribunitia
potestas	potestas	k1gMnSc1	potestas
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
dále	daleko	k6eAd2	daleko
mu	on	k3xPp3gMnSc3	on
byla	být	k5eAaImAgFnS	být
svěřena	svěřen	k2eAgFnSc1d1	svěřena
správa	správa	k1gFnSc1	správa
Panonie	Panonie	k1gFnSc2	Panonie
a	a	k8xC	a
v	v	k7c6	v
roce	rok	k1gInSc6	rok
137	[number]	k4	137
znovu	znovu	k6eAd1	znovu
zastával	zastávat	k5eAaImAgMnS	zastávat
konzulát	konzulát	k1gInSc4	konzulát
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc4	jeho
chatrné	chatrný	k2eAgNnSc4d1	chatrné
zdraví	zdraví	k1gNnSc4	zdraví
ho	on	k3xPp3gInSc2	on
ale	ale	k8xC	ale
zradilo	zradit	k5eAaPmAgNnS	zradit
a	a	k8xC	a
1	[number]	k4	1
<g/>
.	.	kIx.	.
ledna	leden	k1gInSc2	leden
138	[number]	k4	138
zemřel	zemřít	k5eAaPmAgMnS	zemřít
<g/>
.	.	kIx.	.
</s>
<s>
Krátce	krátce	k6eAd1	krátce
po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Aelia	Aelius	k1gMnSc2	Aelius
Caesara	Caesar	k1gMnSc2	Caesar
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
adoptoval	adoptovat	k5eAaPmAgMnS	adoptovat
velmi	velmi	k6eAd1	velmi
váženého	vážený	k2eAgNnSc2d1	vážené
a	a	k8xC	a
bezdětného	bezdětný	k2eAgMnSc2d1	bezdětný
senátora	senátor	k1gMnSc2	senátor
Arria	Arria	k1gFnSc1	Arria
Antonina	Antonina	k1gFnSc1	Antonina
<g/>
,	,	kIx,	,
jenž	jenž	k3xRgMnSc1	jenž
vstoupil	vstoupit	k5eAaPmAgInS	vstoupit
do	do	k7c2	do
dějin	dějiny	k1gFnPc2	dějiny
jako	jako	k8xS	jako
Antoninus	Antoninus	k1gMnSc1	Antoninus
Pius	Pius	k1gMnSc1	Pius
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
sloužil	sloužit	k5eAaImAgMnS	sloužit
jako	jako	k8xS	jako
jeden	jeden	k4xCgMnSc1	jeden
z	z	k7c2	z
císařských	císařský	k2eAgInPc2d1	císařský
legátů	legát	k1gInPc2	legát
vykonávajících	vykonávající	k2eAgInPc2d1	vykonávající
správu	správa	k1gFnSc4	správa
Itálie	Itálie	k1gFnSc2	Itálie
a	a	k8xC	a
působil	působit	k5eAaImAgInS	působit
také	také	k9	také
jako	jako	k9	jako
prokonzul	prokonzul	k1gMnSc1	prokonzul
Asie	Asie	k1gFnSc2	Asie
<g/>
.	.	kIx.	.
25	[number]	k4	25
<g/>
.	.	kIx.	.
února	únor	k1gInSc2	únor
138	[number]	k4	138
Antoninus	Antoninus	k1gMnSc1	Antoninus
obdržel	obdržet	k5eAaPmAgMnS	obdržet
tribunskou	tribunský	k2eAgFnSc4d1	tribunská
moc	moc	k1gFnSc4	moc
a	a	k8xC	a
imperium	imperium	k1gNnSc4	imperium
<g/>
.	.	kIx.	.
</s>
<s>
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
přiměl	přimět	k5eAaPmAgMnS	přimět
Antonina	Antonin	k2eAgMnSc4d1	Antonin
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
adoptoval	adoptovat	k5eAaPmAgMnS	adoptovat
Lucia	Lucius	k1gMnSc4	Lucius
Ceionia	Ceionium	k1gNnSc2	Ceionium
Commoda	Commod	k1gMnSc4	Commod
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc4	syn
nedávno	nedávno	k6eAd1	nedávno
zemřelého	zemřelý	k2eAgMnSc4d1	zemřelý
Aelia	Aelius	k1gMnSc4	Aelius
Caesara	Caesar	k1gMnSc4	Caesar
<g/>
,	,	kIx,	,
a	a	k8xC	a
Marca	Marcus	k1gMnSc4	Marcus
Annia	Annius	k1gMnSc4	Annius
Vera	Verus	k1gMnSc4	Verus
<g/>
,	,	kIx,	,
příslušníka	příslušník	k1gMnSc4	příslušník
význačné	význačný	k2eAgFnSc2d1	význačná
rodiny	rodina	k1gFnSc2	rodina
a	a	k8xC	a
potomka	potomek	k1gMnSc4	potomek
blízkého	blízký	k2eAgMnSc4d1	blízký
Hadrianova	Hadrianův	k2eAgMnSc4d1	Hadrianův
přítele	přítel	k1gMnSc4	přítel
<g/>
.	.	kIx.	.
</s>
<s>
Annius	Annius	k1gMnSc1	Annius
Verus	Verus	k1gMnSc1	Verus
byl	být	k5eAaImAgMnS	být
už	už	k6eAd1	už
dříve	dříve	k6eAd2	dříve
zasnouben	zasnouben	k2eAgInSc1d1	zasnouben
s	s	k7c7	s
Ceionií	Ceionie	k1gFnSc7	Ceionie
Fabií	fabia	k1gFnSc7	fabia
<g/>
,	,	kIx,	,
dcerou	dcera	k1gFnSc7	dcera
Aelia	Aelius	k1gMnSc2	Aelius
Caesara	Caesar	k1gMnSc2	Caesar
<g/>
.	.	kIx.	.
</s>
<s>
Hadrianovy	Hadrianův	k2eAgInPc1d1	Hadrianův
záměry	záměr	k1gInPc1	záměr
ohledně	ohledně	k7c2	ohledně
uspořádání	uspořádání	k1gNnSc2	uspořádání
nástupnictví	nástupnictví	k1gNnSc2	nástupnictví
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
různým	různý	k2eAgFnPc3d1	různá
spekulacím	spekulace	k1gFnPc3	spekulace
<g/>
.	.	kIx.	.
</s>
<s>
Všeobecně	všeobecně	k6eAd1	všeobecně
panuje	panovat	k5eAaImIp3nS	panovat
shoda	shoda	k1gFnSc1	shoda
v	v	k7c6	v
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
chtěl	chtít	k5eAaImAgMnS	chtít
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
po	po	k7c6	po
Antoninovi	Antonin	k1gMnSc3	Antonin
přešla	přejít	k5eAaPmAgFnS	přejít
vláda	vláda	k1gFnSc1	vláda
na	na	k7c4	na
Annia	Annius	k1gMnSc4	Annius
Vera	Verus	k1gMnSc4	Verus
<g/>
.	.	kIx.	.
</s>
<s>
Vyskytují	vyskytovat	k5eAaImIp3nP	vyskytovat
se	se	k3xPyFc4	se
i	i	k9	i
názory	názor	k1gInPc1	názor
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
nichž	jenž	k3xRgMnPc2	jenž
ve	v	k7c6	v
skutečnosti	skutečnost	k1gFnSc6	skutečnost
doufal	doufat	k5eAaImAgMnS	doufat
především	především	k9	především
v	v	k7c6	v
následnictví	následnictví	k1gNnSc6	následnictví
Ceionia	Ceionium	k1gNnSc2	Ceionium
Commoda	Commod	k1gMnSc2	Commod
<g/>
,	,	kIx,	,
syna	syn	k1gMnSc2	syn
svého	svůj	k1gMnSc2	svůj
adoptovaného	adoptovaný	k2eAgMnSc2d1	adoptovaný
syna	syn	k1gMnSc2	syn
<g/>
.	.	kIx.	.
</s>
<s>
Nicméně	nicméně	k8xC	nicméně
vzhledem	vzhledem	k7c3	vzhledem
k	k	k7c3	k
silným	silný	k2eAgFnPc3d1	silná
vazbám	vazba	k1gFnPc3	vazba
Annia	Annium	k1gNnSc2	Annium
Vera	Ver	k1gInSc2	Ver
vůči	vůči	k7c3	vůči
hispano-narbonské	hispanoarbonský	k2eAgFnSc3d1	hispano-narbonský
aristokracii	aristokracie	k1gFnSc3	aristokracie
<g/>
,	,	kIx,	,
k	k	k7c3	k
níž	jenž	k3xRgFnSc3	jenž
Hadrianus	Hadrianus	k1gInSc1	Hadrianus
rovněž	rovněž	k6eAd1	rovněž
příslušel	příslušet	k5eAaImAgInS	příslušet
<g/>
,	,	kIx,	,
byl	být	k5eAaImAgInS	být
nucen	nutit	k5eAaImNgMnS	nutit
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
přízeň	přízeň	k1gFnSc4	přízeň
Anniu	Annius	k1gMnSc3	Annius
Verovi	Vera	k1gMnSc3	Vera
<g/>
.	.	kIx.	.
</s>
<s>
Mohl	moct	k5eAaImAgInS	moct
to	ten	k3xDgNnSc1	ten
být	být	k5eAaImF	být
tedy	tedy	k9	tedy
spíše	spíše	k9	spíše
Antoninus	Antoninus	k1gMnSc1	Antoninus
Pius	Pius	k1gMnSc1	Pius
<g/>
,	,	kIx,	,
strýc	strýc	k1gMnSc1	strýc
Annia	Annium	k1gNnSc2	Annium
Vera	Vera	k1gMnSc1	Vera
<g/>
,	,	kIx,	,
kdo	kdo	k3yRnSc1	kdo
ho	on	k3xPp3gMnSc4	on
povýšil	povýšit	k5eAaPmAgMnS	povýšit
do	do	k7c2	do
pozice	pozice	k1gFnSc2	pozice
hlavního	hlavní	k2eAgMnSc2d1	hlavní
pretendenta	pretendent	k1gMnSc2	pretendent
císařského	císařský	k2eAgInSc2d1	císařský
trůnu	trůn	k1gInSc2	trůn
<g/>
.	.	kIx.	.
</s>
<s>
Tomu	ten	k3xDgNnSc3	ten
by	by	kYmCp3nS	by
nasvědčovalo	nasvědčovat	k5eAaImAgNnS	nasvědčovat
i	i	k9	i
pozdější	pozdní	k2eAgNnSc1d2	pozdější
zrušení	zrušení	k1gNnSc1	zrušení
zasnoubení	zasnoubení	k1gNnSc2	zasnoubení
Annia	Annius	k1gMnSc2	Annius
Vera	Verus	k1gMnSc2	Verus
s	s	k7c7	s
Ceionií	Ceionie	k1gFnSc7	Ceionie
Fabií	fabia	k1gFnPc2	fabia
a	a	k8xC	a
Verův	Verův	k2eAgInSc4d1	Verův
sňatek	sňatek	k1gInSc4	sňatek
s	s	k7c7	s
Antoninovou	Antoninový	k2eAgFnSc7d1	Antoninový
dcerou	dcera	k1gFnSc7	dcera
Annií	Annie	k1gFnSc7	Annie
Faustinou	Faustina	k1gFnSc7	Faustina
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
smrti	smrt	k1gFnSc6	smrt
Antonina	Antonin	k2eAgMnSc2d1	Antonin
Pia	Pius	k1gMnSc2	Pius
se	se	k3xPyFc4	se
Annius	Annius	k1gMnSc1	Annius
Verus	Verus	k1gMnSc1	Verus
ujal	ujmout	k5eAaPmAgMnS	ujmout
vlády	vláda	k1gFnPc4	vláda
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
Marcus	Marcus	k1gMnSc1	Marcus
Aurelius	Aurelius	k1gMnSc1	Aurelius
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
Ceioniu	Ceionium	k1gNnSc3	Ceionium
Commodovi	Commodův	k2eAgMnPc1d1	Commodův
<g/>
,	,	kIx,	,
tehdy	tehdy	k6eAd1	tehdy
známému	známý	k2eAgInSc3d1	známý
jako	jako	k8xS	jako
Lucius	Lucius	k1gMnSc1	Lucius
Verus	Verus	k1gMnSc1	Verus
<g/>
,	,	kIx,	,
svěřil	svěřit	k5eAaPmAgInS	svěřit
z	z	k7c2	z
vlastního	vlastní	k2eAgInSc2d1	vlastní
popudu	popud	k1gInSc2	popud
podíl	podíl	k1gInSc4	podíl
na	na	k7c4	na
řízení	řízení	k1gNnSc4	řízení
státu	stát	k1gInSc2	stát
jako	jako	k8xS	jako
svému	svůj	k3xOyFgMnSc3	svůj
spolucísařovi	spolucísař	k1gMnSc3	spolucísař
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
pramenů	pramen	k1gInPc2	pramen
se	se	k3xPyFc4	se
Hadrianus	Hadrianus	k1gInSc1	Hadrianus
v	v	k7c6	v
posledních	poslední	k2eAgNnPc6d1	poslední
letech	léto	k1gNnPc6	léto
svého	svůj	k3xOyFgInSc2	svůj
života	život	k1gInSc2	život
choval	chovat	k5eAaImAgMnS	chovat
poměrně	poměrně	k6eAd1	poměrně
náladově	náladově	k6eAd1	náladově
a	a	k8xC	a
projevoval	projevovat	k5eAaImAgInS	projevovat
občasné	občasný	k2eAgInPc4d1	občasný
tyranské	tyranský	k2eAgInPc4d1	tyranský
sklony	sklon	k1gInPc4	sklon
<g/>
.	.	kIx.	.
</s>
<s>
Adopce	adopce	k1gFnSc1	adopce
Aelia	Aelius	k1gMnSc2	Aelius
Caesara	Caesar	k1gMnSc2	Caesar
se	se	k3xPyFc4	se
ukázala	ukázat	k5eAaPmAgNnP	ukázat
být	být	k5eAaImF	být
velice	velice	k6eAd1	velice
nepopulární	populární	k2eNgMnSc1d1	nepopulární
<g/>
,	,	kIx,	,
neboť	neboť	k8xC	neboť
nebrala	brát	k5eNaImAgFnS	brát
ohled	ohled	k1gInSc4	ohled
na	na	k7c4	na
jiné	jiný	k2eAgInPc4d1	jiný
dva	dva	k4xCgInPc4	dva
potenciální	potenciální	k2eAgMnPc4d1	potenciální
uchazeče	uchazeč	k1gMnPc4	uchazeč
o	o	k7c4	o
moc	moc	k1gFnSc4	moc
<g/>
:	:	kIx,	:
Hadrianova	Hadrianův	k2eAgMnSc2d1	Hadrianův
letitého	letitý	k2eAgMnSc2d1	letitý
švagra	švagr	k1gMnSc2	švagr
Lucia	Lucius	k1gMnSc2	Lucius
Julia	Julius	k1gMnSc2	Julius
Ursa	Ursus	k1gMnSc2	Ursus
Serviana	Servian	k1gMnSc2	Servian
a	a	k8xC	a
Servianova	Servianův	k2eAgMnSc2d1	Servianův
vnuka	vnuk	k1gMnSc2	vnuk
Gnaea	Gnaeus	k1gMnSc2	Gnaeus
Pedania	Pedanium	k1gNnSc2	Pedanium
Fusca	Fuscus	k1gMnSc2	Fuscus
Salinatora	Salinator	k1gMnSc2	Salinator
<g/>
.	.	kIx.	.
</s>
<s>
Servianus	Servianus	k1gMnSc1	Servianus
a	a	k8xC	a
někteří	některý	k3yIgMnPc1	některý
další	další	k2eAgMnPc1d1	další
usilovali	usilovat	k5eAaImAgMnP	usilovat
o	o	k7c4	o
to	ten	k3xDgNnSc4	ten
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
Pedanius	Pedanius	k1gMnSc1	Pedanius
Fuscus	Fuscus	k1gMnSc1	Fuscus
stal	stát	k5eAaPmAgMnS	stát
císařem	císař	k1gMnSc7	císař
po	po	k7c6	po
Hadrianovi	Hadrian	k1gMnSc6	Hadrian
<g/>
.	.	kIx.	.
</s>
<s>
Ten	ten	k3xDgMnSc1	ten
chtěl	chtít	k5eAaImAgMnS	chtít
zamezit	zamezit	k5eAaPmF	zamezit
hrozícím	hrozící	k2eAgInPc3d1	hrozící
nástupnickým	nástupnický	k2eAgInPc3d1	nástupnický
sporům	spor	k1gInPc3	spor
a	a	k8xC	a
nechal	nechat	k5eAaPmAgMnS	nechat
tudíž	tudíž	k8xC	tudíž
oba	dva	k4xCgMnPc1	dva
popravit	popravit	k5eAaPmF	popravit
<g/>
.	.	kIx.	.
</s>
<s>
Hadrianus	Hadrianus	k1gMnSc1	Hadrianus
umíral	umírat	k5eAaImAgMnS	umírat
dlouho	dlouho	k6eAd1	dlouho
a	a	k8xC	a
za	za	k7c2	za
nesnesitelných	snesitelný	k2eNgFnPc2d1	nesnesitelná
bolestí	bolest	k1gFnPc2	bolest
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
několika	několik	k4yIc6	několik
nezdařených	zdařený	k2eNgInPc6d1	nezdařený
pokusech	pokus	k1gInPc6	pokus
o	o	k7c4	o
sebevraždu	sebevražda	k1gFnSc4	sebevražda
skonal	skonat	k5eAaPmAgMnS	skonat
10	[number]	k4	10
<g/>
.	.	kIx.	.
července	červenec	k1gInSc2	červenec
138	[number]	k4	138
ve	v	k7c6	v
své	svůj	k3xOyFgFnSc6	svůj
vile	vila	k1gFnSc6	vila
v	v	k7c6	v
Bajích	Baj	k1gInPc6	Baj
<g/>
.	.	kIx.	.
</s>
<s>
Předpokládá	předpokládat	k5eAaImIp3nS	předpokládat
se	se	k3xPyFc4	se
<g/>
,	,	kIx,	,
že	že	k8xS	že
jeho	jeho	k3xOp3gFnSc1	jeho
smrt	smrt	k1gFnSc1	smrt
byla	být	k5eAaImAgFnS	být
způsobena	způsobit	k5eAaPmNgFnS	způsobit
vinou	vinou	k7c2	vinou
selhání	selhání	k1gNnSc2	selhání
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1980	[number]	k4	1980
byla	být	k5eAaImAgFnS	být
zveřejněna	zveřejněn	k2eAgFnSc1d1	zveřejněna
studie	studie	k1gFnSc1	studie
zabývající	zabývající	k2eAgMnPc4d1	zabývající
se	se	k3xPyFc4	se
Hadrianovými	Hadrianův	k2eAgFnPc7d1	Hadrianova
klasickými	klasický	k2eAgFnPc7d1	klasická
sochami	socha	k1gFnPc7	socha
<g/>
,	,	kIx,	,
na	na	k7c6	na
nichž	jenž	k3xRgInPc6	jenž
byly	být	k5eAaImAgFnP	být
zjištěny	zjištěn	k2eAgFnPc1d1	zjištěna
nepravidelnosti	nepravidelnost	k1gFnPc1	nepravidelnost
ušních	ušní	k2eAgMnPc2d1	ušní
lalůčků	lalůček	k1gInPc2	lalůček
<g/>
,	,	kIx,	,
jež	jenž	k3xRgInPc1	jenž
jsou	být	k5eAaImIp3nP	být
spojovány	spojovat	k5eAaImNgInP	spojovat
s	s	k7c7	s
ischemickou	ischemický	k2eAgFnSc7d1	ischemická
chorobou	choroba	k1gFnSc7	choroba
srdeční	srdeční	k2eAgFnSc7d1	srdeční
<g/>
.	.	kIx.	.
</s>
<s>
Senátoři	senátor	k1gMnPc1	senátor
chtěli	chtít	k5eAaImAgMnP	chtít
zrušit	zrušit	k5eAaPmF	zrušit
mnohá	mnohý	k2eAgNnPc4d1	mnohé
Hadrianova	Hadrianův	k2eAgNnPc4d1	Hadrianovo
ustanovení	ustanovení	k1gNnPc4	ustanovení
a	a	k8xC	a
hodlali	hodlat	k5eAaImAgMnP	hodlat
odsoudit	odsoudit	k5eAaPmF	odsoudit
jeho	jeho	k3xOp3gFnSc4	jeho
památku	památka	k1gFnSc4	památka
prohlášením	prohlášení	k1gNnSc7	prohlášení
damnatio	damnatio	k1gNnSc4	damnatio
memoriae	memoriae	k1gNnSc2	memoriae
<g/>
.	.	kIx.	.
</s>
<s>
Antoninus	Antoninus	k1gMnSc1	Antoninus
se	se	k3xPyFc4	se
však	však	k9	však
vyslovil	vyslovit	k5eAaPmAgMnS	vyslovit
proti	proti	k7c3	proti
zavržení	zavržení	k1gNnSc6	zavržení
svého	svůj	k3xOyFgMnSc2	svůj
adoptivního	adoptivní	k2eAgMnSc2d1	adoptivní
otce	otec	k1gMnSc2	otec
a	a	k8xC	a
místo	místo	k7c2	místo
toho	ten	k3xDgNnSc2	ten
dosáhl	dosáhnout	k5eAaPmAgInS	dosáhnout
jeho	jeho	k3xOp3gNnSc4	jeho
prohlášení	prohlášení	k1gNnSc4	prohlášení
za	za	k7c4	za
boha	bůh	k1gMnSc4	bůh
<g/>
.	.	kIx.	.
</s>
<s>
Hadrianovo	Hadrianův	k2eAgNnSc1d1	Hadrianovo
tělo	tělo	k1gNnSc1	tělo
bylo	být	k5eAaImAgNnS	být
nejprve	nejprve	k6eAd1	nejprve
pohřbeno	pohřbít	k5eAaPmNgNnS	pohřbít
v	v	k7c6	v
Puteolech	Puteol	k1gInPc6	Puteol
(	(	kIx(	(
<g/>
Pozzuoli	Pozzuole	k1gFnSc4	Pozzuole
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
nedaleko	nedaleko	k7c2	nedaleko
Bají	Bají	k1gFnSc2	Bají
<g/>
,	,	kIx,	,
na	na	k7c6	na
statku	statek	k1gInSc6	statek
kdysi	kdysi	k6eAd1	kdysi
vlastněném	vlastněný	k2eAgInSc6d1	vlastněný
Ciceronem	Cicero	k1gMnSc7	Cicero
<g/>
.	.	kIx.	.
</s>
<s>
Nedlouho	nedlouho	k1gNnSc1	nedlouho
poté	poté	k6eAd1	poté
bylo	být	k5eAaImAgNnS	být
přeneseno	přenést	k5eAaPmNgNnS	přenést
do	do	k7c2	do
Říma	Řím	k1gInSc2	Řím
a	a	k8xC	a
uloženo	uložit	k5eAaPmNgNnS	uložit
v	v	k7c6	v
Domitiiných	Domitiin	k2eAgFnPc6d1	Domitiin
zahradách	zahrada	k1gFnPc6	zahrada
<g/>
,	,	kIx,	,
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
takřka	takřka	k6eAd1	takřka
hotového	hotový	k2eAgNnSc2d1	hotové
Hadriánova	Hadriánův	k2eAgNnSc2d1	Hadriánovo
mauzolea	mauzoleum	k1gNnSc2	mauzoleum
<g/>
.	.	kIx.	.
</s>
<s>
Po	po	k7c6	po
dokončení	dokončení	k1gNnSc6	dokončení
této	tento	k3xDgFnSc2	tento
stavby	stavba	k1gFnSc2	stavba
v	v	k7c6	v
roce	rok	k1gInSc6	rok
139	[number]	k4	139
byly	být	k5eAaImAgInP	být
Hadrianovy	Hadrianův	k2eAgInPc1d1	Hadrianův
ostatky	ostatek	k1gInPc1	ostatek
spáleny	spálen	k2eAgInPc1d1	spálen
a	a	k8xC	a
jeho	jeho	k3xOp3gInSc1	jeho
popel	popel	k1gInSc1	popel
byl	být	k5eAaImAgInS	být
umístěn	umístit	k5eAaPmNgInS	umístit
do	do	k7c2	do
mauzolea	mauzoleum	k1gNnSc2	mauzoleum
společně	společně	k6eAd1	společně
s	s	k7c7	s
popelem	popel	k1gInSc7	popel
jeho	jeho	k3xOp3gFnSc2	jeho
manželky	manželka	k1gFnSc2	manželka
Vibie	Vibie	k1gFnSc2	Vibie
Sabiny	Sabina	k1gMnSc2	Sabina
a	a	k8xC	a
adoptivního	adoptivní	k2eAgMnSc2d1	adoptivní
syna	syn	k1gMnSc2	syn
Aelia	Aelius	k1gMnSc2	Aelius
Caesara	Caesar	k1gMnSc2	Caesar
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Hadrian	Hadriana	k1gFnPc2	Hadriana
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
<s>
BURIAN	Burian	k1gMnSc1	Burian
<g/>
,	,	kIx,	,
Jan	Jan	k1gMnSc1	Jan
<g/>
.	.	kIx.	.
</s>
<s>
Římské	římský	k2eAgNnSc1d1	římské
impérium	impérium	k1gNnSc1	impérium
:	:	kIx,	:
vrchol	vrchol	k1gInSc1	vrchol
a	a	k8xC	a
proměny	proměna	k1gFnPc1	proměna
antické	antický	k2eAgFnSc2d1	antická
civilizace	civilizace	k1gFnSc2	civilizace
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1997	[number]	k4	1997
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-205-0536-9	[number]	k4	80-205-0536-9
GIBBON	gibbon	k1gMnSc1	gibbon
<g/>
,	,	kIx,	,
Edward	Edward	k1gMnSc1	Edward
<g/>
.	.	kIx.	.
</s>
<s>
Úpadek	úpadek	k1gInSc1	úpadek
a	a	k8xC	a
pád	pád	k1gInSc1	pád
římské	římský	k2eAgFnSc2d1	římská
říše	říš	k1gFnSc2	říš
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Levné	levný	k2eAgFnSc2d1	levná
knihy	kniha	k1gFnSc2	kniha
KMa	KMa	k1gFnSc2	KMa
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7309-189-5	[number]	k4	80-7309-189-5
GRANT	grant	k1gInSc1	grant
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
antického	antický	k2eAgInSc2d1	antický
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
BB	BB	kA	BB
<g/>
/	/	kIx~	/
<g/>
art	art	k?	art
<g/>
,	,	kIx,	,
2006	[number]	k4	2006
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7341-930-0	[number]	k4	80-7341-930-0
GRANT	grant	k1gInSc1	grant
<g/>
,	,	kIx,	,
Michael	Michael	k1gMnSc1	Michael
<g/>
.	.	kIx.	.
</s>
<s>
Římští	římský	k2eAgMnPc1d1	římský
císařové	císař	k1gMnPc1	císař
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
BB	BB	kA	BB
art	art	k?	art
<g/>
,	,	kIx,	,
2002	[number]	k4	2002
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-7257-731-X	[number]	k4	80-7257-731-X
MAŠKIN	MAŠKIN	kA	MAŠKIN
<g/>
,	,	kIx,	,
Nikolaj	Nikolaj	k1gMnSc1	Nikolaj
A.	A.	kA	A.
Dějiny	dějiny	k1gFnPc1	dějiny
starověkého	starověký	k2eAgInSc2d1	starověký
Říma	Řím	k1gInSc2	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
SNPL	SNPL	kA	SNPL
<g/>
,	,	kIx,	,
1957	[number]	k4	1957
ZAMAROVSKÝ	ZAMAROVSKÝ	kA	ZAMAROVSKÝ
<g/>
,	,	kIx,	,
Vojtěch	Vojtěch	k1gMnSc1	Vojtěch
<g/>
.	.	kIx.	.
</s>
<s>
Dějiny	dějiny	k1gFnPc1	dějiny
psané	psaný	k2eAgFnSc2d1	psaná
Římem	Řím	k1gInSc7	Řím
<g/>
.	.	kIx.	.
</s>
<s>
Bratislava	Bratislava	k1gFnSc1	Bratislava
:	:	kIx,	:
Perfekt	perfektum	k1gNnPc2	perfektum
<g/>
,	,	kIx,	,
2005	[number]	k4	2005
<g/>
.	.	kIx.	.
</s>
<s>
ISBN	ISBN	kA	ISBN
80-8046-297-6	[number]	k4	80-8046-297-6
Portréty	portrét	k1gInPc1	portrét
světovládců	světovládce	k1gMnPc2	světovládce
I	i	k8xC	i
(	(	kIx(	(
<g/>
Od	od	k7c2	od
Hadriana	Hadrian	k1gMnSc2	Hadrian
po	po	k7c4	po
Alexandra	Alexandr	k1gMnSc4	Alexandr
Severa	Severa	k1gMnSc1	Severa
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Praha	Praha	k1gFnSc1	Praha
:	:	kIx,	:
Svoboda	Svoboda	k1gMnSc1	Svoboda
<g/>
,	,	kIx,	,
1982	[number]	k4	1982
Adoptivní	adoptivní	k2eAgMnPc1d1	adoptivní
císaři	císař	k1gMnPc1	císař
Povstání	povstání	k1gNnSc2	povstání
Bar	bar	k1gInSc4	bar
Kochby	Kochba	k1gFnSc2	Kochba
Hadriánův	Hadriánův	k2eAgInSc4d1	Hadriánův
val	val	k1gInSc4	val
Obrázky	obrázek	k1gInPc7	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc7	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
Hadrianus	Hadrianus	k1gInSc4	Hadrianus
ve	v	k7c6	v
Wikimedia	Wikimedium	k1gNnSc2	Wikimedium
Commons	Commons	k1gInSc4	Commons
Galerie	galerie	k1gFnSc2	galerie
Hadrianus	Hadrianus	k1gInSc4	Hadrianus
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
Hadrianus	Hadrianus	k1gInSc1	Hadrianus
(	(	kIx(	(
<g/>
Římské	římský	k2eAgNnSc1d1	římské
císařství	císařství	k1gNnSc1	císařství
<g/>
)	)	kIx)	)
Hadrianus	Hadrianus	k1gInSc1	Hadrianus
(	(	kIx(	(
<g/>
stránky	stránka	k1gFnSc2	stránka
Antika	antika	k1gFnSc1	antika
<g/>
)	)	kIx)	)
Hadrian	Hadrian	k1gInSc1	Hadrian
(	(	kIx(	(
<g/>
De	De	k?	De
Imperatoribus	Imperatoribus	k1gInSc1	Imperatoribus
Romanis	Romanis	k1gFnSc1	Romanis
<g />
.	.	kIx.	.
</s>
<s>
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Hadrianus	Hadrianus	k1gInSc1	Hadrianus
(	(	kIx(	(
<g/>
AllEmpires	AllEmpires	k1gInSc1	AllEmpires
<g/>
.	.	kIx.	.
<g/>
com	com	k?	com
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
British	British	k1gInSc1	British
Museum	museum	k1gNnSc1	museum
-	-	kIx~	-
Hadrian	Hadrian	k1gInSc1	Hadrian
<g/>
:	:	kIx,	:
Empire	empir	k1gInSc5	empir
and	and	k?	and
Conflict	Conflict	k1gInSc4	Conflict
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
Publius	Publius	k1gMnSc1	Publius
Aelius	Aelius	k1gMnSc1	Aelius
Hadrian	Hadrian	k1gMnSc1	Hadrian
(	(	kIx(	(
<g/>
CATHOLIC	CATHOLIC	kA	CATHOLIC
ENCYCLOPEDIA	ENCYCLOPEDIA	kA	ENCYCLOPEDIA
<g/>
)	)	kIx)	)
(	(	kIx(	(
<g/>
anglicky	anglicky	k6eAd1	anglicky
<g/>
)	)	kIx)	)
</s>
