<s>
Říši	říše	k1gFnSc4	říše
a	a	k8xC	a
dynastii	dynastie	k1gFnSc4	dynastie
Ming	Minga	k1gFnPc2	Minga
vyhlásil	vyhlásit	k5eAaPmAgMnS	vyhlásit
Ču	Ču	k1gMnSc1	Ču
Jüan-čang	Jüan-čang	k1gMnSc1	Jüan-čang
(	(	kIx(	(
<g/>
jako	jako	k8xS	jako
císař	císař	k1gMnSc1	císař
je	být	k5eAaImIp3nS	být
znám	znám	k2eAgMnSc1d1	znám
pod	pod	k7c7	pod
jménem	jméno	k1gNnSc7	jméno
své	svůj	k3xOyFgFnSc2	svůj
éry	éra	k1gFnSc2	éra
vlády	vláda	k1gFnSc2	vláda
–	–	k?	–
Chung-wu	Chungus	k1gInSc2	Chung-wus
<g/>
)	)	kIx)	)
v	v	k7c6	v
tehdejším	tehdejší	k2eAgNnSc6d1	tehdejší
hlavním	hlavní	k2eAgNnSc6d1	hlavní
městě	město	k1gNnSc6	město
Nankingu	Nanking	k1gInSc2	Nanking
v	v	k7c6	v
lednu	leden	k1gInSc6	leden
1368	[number]	k4	1368
<g/>
.	.	kIx.	.
</s>
