<p>
<s>
Raroh	raroh	k1gMnSc1	raroh
velký	velký	k2eAgMnSc1d1	velký
(	(	kIx(	(
<g/>
Falco	Falco	k1gMnSc1	Falco
cherrug	cherrug	k1gMnSc1	cherrug
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
ohrožený	ohrožený	k2eAgMnSc1d1	ohrožený
dravý	dravý	k2eAgMnSc1d1	dravý
pták	pták	k1gMnSc1	pták
z	z	k7c2	z
čeledi	čeleď	k1gFnSc2	čeleď
sokolovitých	sokolovitý	k2eAgMnPc2d1	sokolovitý
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
v	v	k7c6	v
rozmezí	rozmezí	k1gNnSc6	rozmezí
od	od	k7c2	od
východní	východní	k2eAgFnSc2d1	východní
Evropy	Evropa	k1gFnSc2	Evropa
východně	východně	k6eAd1	východně
přes	přes	k7c4	přes
Asii	Asie	k1gFnSc4	Asie
až	až	k9	až
po	po	k7c4	po
Mandžusko	Mandžusko	k1gNnSc4	Mandžusko
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
tažný	tažný	k2eAgInSc1d1	tažný
<g/>
,	,	kIx,	,
zimuje	zimovat	k5eAaImIp3nS	zimovat
v	v	k7c6	v
Etiopii	Etiopie	k1gFnSc6	Etiopie
<g/>
,	,	kIx,	,
na	na	k7c6	na
Arabském	arabský	k2eAgInSc6d1	arabský
poloostrově	poloostrov	k1gInSc6	poloostrov
<g/>
,	,	kIx,	,
v	v	k7c6	v
severní	severní	k2eAgFnSc6d1	severní
Indii	Indie	k1gFnSc6	Indie
<g/>
,	,	kIx,	,
Íránu	Írán	k1gInSc6	Írán
a	a	k8xC	a
západní	západní	k2eAgFnSc6d1	západní
Číně	Čína	k1gFnSc6	Čína
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Popis	popis	k1gInSc4	popis
==	==	k?	==
</s>
</p>
<p>
<s>
Raroh	raroh	k1gMnSc1	raroh
velký	velký	k2eAgMnSc1d1	velký
je	být	k5eAaImIp3nS	být
velký	velký	k2eAgMnSc1d1	velký
dravec	dravec	k1gMnSc1	dravec
dorůstající	dorůstající	k2eAgMnSc1d1	dorůstající
47	[number]	k4	47
–	–	k?	–
55	[number]	k4	55
cm	cm	kA	cm
s	s	k7c7	s
rozpětím	rozpětí	k1gNnSc7	rozpětí
křídel	křídlo	k1gNnPc2	křídlo
105	[number]	k4	105
–	–	k?	–
129	[number]	k4	129
cm	cm	kA	cm
<g/>
.	.	kIx.	.
</s>
<s>
Svrchu	svrchu	k6eAd1	svrchu
je	být	k5eAaImIp3nS	být
převážně	převážně	k6eAd1	převážně
hnědý	hnědý	k2eAgInSc1d1	hnědý
s	s	k7c7	s
šedými	šedý	k2eAgFnPc7d1	šedá
letkami	letka	k1gFnPc7	letka
<g/>
,	,	kIx,	,
temeno	temeno	k1gNnSc4	temeno
hlava	hlava	k1gFnSc1	hlava
je	být	k5eAaImIp3nS	být
zbarveno	zbarven	k2eAgNnSc1d1	zbarveno
hnědě	hnědě	k6eAd1	hnědě
<g/>
.	.	kIx.	.
</s>
<s>
Spodina	spodina	k1gFnSc1	spodina
těla	tělo	k1gNnSc2	tělo
je	být	k5eAaImIp3nS	být
světlá	světlý	k2eAgFnSc1d1	světlá
s	s	k7c7	s
hnědými	hnědý	k2eAgInPc7d1	hnědý
stříkanci	stříkanec	k1gInPc7	stříkanec
na	na	k7c6	na
břiše	břicho	k1gNnSc6	břicho
<g/>
.	.	kIx.	.
</s>
<s>
Obě	dva	k4xCgNnPc4	dva
pohlaví	pohlaví	k1gNnPc4	pohlaví
jsou	být	k5eAaImIp3nP	být
si	se	k3xPyFc3	se
velmi	velmi	k6eAd1	velmi
podobná	podobný	k2eAgNnPc4d1	podobné
<g/>
,	,	kIx,	,
mladí	mladý	k2eAgMnPc1d1	mladý
ptáci	pták	k1gMnPc1	pták
jsou	být	k5eAaImIp3nP	být
v	v	k7c6	v
porovnání	porovnání	k1gNnSc6	porovnání
s	s	k7c7	s
dospělci	dospělec	k1gMnPc7	dospělec
jednotvárněji	jednotvárně	k6eAd2	jednotvárně
hnědější	hnědý	k2eAgMnPc1d2	hnědší
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obývá	obývat	k5eAaImIp3nS	obývat
otevřené	otevřený	k2eAgFnPc4d1	otevřená
krajiny	krajina	k1gFnPc4	krajina
s	s	k7c7	s
řídkým	řídký	k2eAgInSc7d1	řídký
porostem	porost	k1gInSc7	porost
stromů	strom	k1gInPc2	strom
<g/>
.	.	kIx.	.
</s>
<s>
Živí	živit	k5eAaImIp3nS	živit
se	se	k3xPyFc4	se
zejména	zejména	k9	zejména
hlodavci	hlodavec	k1gMnPc1	hlodavec
a	a	k8xC	a
ptáky	pták	k1gMnPc4	pták
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
loví	lovit	k5eAaImIp3nS	lovit
přímým	přímý	k2eAgNnSc7d1	přímé
pronásledováním	pronásledování	k1gNnSc7	pronásledování
<g/>
.	.	kIx.	.
</s>
<s>
Hnízdí	hnízdit	k5eAaImIp3nS	hnízdit
na	na	k7c6	na
zemi	zem	k1gFnSc6	zem
<g/>
,	,	kIx,	,
holých	holý	k2eAgFnPc6d1	holá
skalních	skalní	k2eAgFnPc6d1	skalní
římsách	římsa	k1gFnPc6	římsa
nebo	nebo	k8xC	nebo
v	v	k7c6	v
opuštěných	opuštěný	k2eAgNnPc6d1	opuštěné
hnízdech	hnízdo	k1gNnPc6	hnízdo
jiných	jiný	k2eAgMnPc2d1	jiný
větších	veliký	k2eAgMnPc2d2	veliký
ptáků	pták	k1gMnPc2	pták
na	na	k7c6	na
stromech	strom	k1gInPc6	strom
<g/>
.	.	kIx.	.
</s>
<s>
Snáší	snášet	k5eAaImIp3nS	snášet
3	[number]	k4	3
-	-	kIx~	-
6	[number]	k4	6
vajec	vejce	k1gNnPc2	vejce
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Raroh	raroh	k1gMnSc1	raroh
velký	velký	k2eAgMnSc1d1	velký
je	být	k5eAaImIp3nS	být
v	v	k7c6	v
současné	současný	k2eAgFnSc6d1	současná
době	doba	k1gFnSc6	doba
vyhodnocen	vyhodnocen	k2eAgInSc1d1	vyhodnocen
jako	jako	k8xC	jako
ohrožený	ohrožený	k2eAgInSc1d1	ohrožený
druh	druh	k1gInSc1	druh
<g/>
.	.	kIx.	.
</s>
<s>
Důvodem	důvod	k1gInSc7	důvod
je	být	k5eAaImIp3nS	být
rychlý	rychlý	k2eAgInSc1d1	rychlý
populační	populační	k2eAgInSc1d1	populační
pokles	pokles	k1gInSc1	pokles
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
viditelný	viditelný	k2eAgInSc1d1	viditelný
zvláště	zvláště	k6eAd1	zvláště
na	na	k7c6	na
asijském	asijský	k2eAgInSc6d1	asijský
kontinentě	kontinent	k1gInSc6	kontinent
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
největší	veliký	k2eAgFnSc7d3	veliký
hrozbou	hrozba	k1gFnSc7	hrozba
je	být	k5eAaImIp3nS	být
ztráta	ztráta	k1gFnSc1	ztráta
přirozeného	přirozený	k2eAgInSc2d1	přirozený
biotopu	biotop	k1gInSc2	biotop
a	a	k8xC	a
lov	lov	k1gInSc4	lov
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
2004	[number]	k4	2004
byla	být	k5eAaImAgFnS	být
jeho	jeho	k3xOp3gFnSc1	jeho
populace	populace	k1gFnSc1	populace
odhadována	odhadovat	k5eAaImNgFnS	odhadovat
na	na	k7c4	na
7	[number]	k4	7
200	[number]	k4	200
-	-	kIx~	-
8	[number]	k4	8
800	[number]	k4	800
dospělých	dospělý	k2eAgMnPc2d1	dospělý
jedinců	jedinec	k1gMnPc2	jedinec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
článku	článek	k1gInSc6	článek
byl	být	k5eAaImAgInS	být
použit	použít	k5eAaPmNgInS	použít
překlad	překlad	k1gInSc1	překlad
textu	text	k1gInSc2	text
z	z	k7c2	z
článku	článek	k1gInSc2	článek
Saker	Saker	k1gMnSc1	Saker
Falcon	Falcon	k1gMnSc1	Falcon
na	na	k7c6	na
anglické	anglický	k2eAgFnSc6d1	anglická
Wikipedii	Wikipedie	k1gFnSc6	Wikipedie
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
raroh	raroh	k1gMnSc1	raroh
velký	velký	k2eAgMnSc1d1	velký
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Taxon	taxon	k1gInSc1	taxon
Falco	Falco	k1gMnSc1	Falco
cherrug	cherrug	k1gMnSc1	cherrug
ve	v	k7c6	v
Wikidruzích	Wikidruze	k1gFnPc6	Wikidruze
</s>
</p>
<p>
<s>
Raroh	raroh	k1gMnSc1	raroh
v	v	k7c6	v
ZOO	zoo	k1gFnSc6	zoo
Liberec	Liberec	k1gInSc1	Liberec
</s>
</p>
