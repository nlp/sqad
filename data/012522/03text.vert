<p>
<s>
Quickstep	quickstep	k1gInSc1	quickstep
je	být	k5eAaImIp3nS	být
jedním	jeden	k4xCgInSc7	jeden
ze	z	k7c2	z
standardních	standardní	k2eAgInPc2d1	standardní
tanců	tanec	k1gInPc2	tanec
nazývaný	nazývaný	k2eAgInSc1d1	nazývaný
také	také	k9	také
rychlý	rychlý	k2eAgInSc1d1	rychlý
foxtrot	foxtrot	k1gInSc1	foxtrot
<g/>
.	.	kIx.	.
</s>
<s>
Historii	historie	k1gFnSc4	historie
quickstepu	quickstep	k1gInSc2	quickstep
můžeme	moct	k5eAaImIp1nP	moct
sledovat	sledovat	k5eAaImF	sledovat
již	již	k6eAd1	již
od	od	k7c2	od
roku	rok	k1gInSc2	rok
1924	[number]	k4	1924
<g/>
.	.	kIx.	.
</s>
<s>
Quickstep	quickstep	k1gInSc1	quickstep
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
čtyřčtvrťovém	čtyřčtvrťový	k2eAgInSc6d1	čtyřčtvrťový
taktu	takt	k1gInSc6	takt
<g/>
,	,	kIx,	,
v	v	k7c6	v
tempu	tempo	k1gNnSc6	tempo
50-52	[number]	k4	50-52
taktů	takt	k1gInPc2	takt
za	za	k7c4	za
minutu	minuta	k1gFnSc4	minuta
<g/>
.	.	kIx.	.
</s>
<s>
Základním	základní	k2eAgInSc7d1	základní
pohybem	pohyb	k1gInSc7	pohyb
jsou	být	k5eAaImIp3nP	být
tzv.	tzv.	kA	tzv.
čtvrtotáčky	čtvrtotáček	k1gInPc1	čtvrtotáček
<g/>
,	,	kIx,	,
tančené	tančený	k2eAgInPc1d1	tančený
v	v	k7c6	v
uzavřeném	uzavřený	k2eAgNnSc6d1	uzavřené
<g/>
,	,	kIx,	,
těsném	těsný	k2eAgNnSc6d1	těsné
držení	držení	k1gNnSc6	držení
pro	pro	k7c4	pro
standardní	standardní	k2eAgInPc4d1	standardní
tance	tanec	k1gInPc4	tanec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Historie	historie	k1gFnSc1	historie
==	==	k?	==
</s>
</p>
<p>
<s>
Organizátoři	organizátor	k1gMnPc1	organizátor
mistrovství	mistrovství	k1gNnSc2	mistrovství
hvězd	hvězda	k1gFnPc2	hvězda
se	se	k3xPyFc4	se
rozhodli	rozhodnout	k5eAaPmAgMnP	rozhodnout
spojit	spojit	k5eAaPmF	spojit
nově	nově	k6eAd1	nově
vzniklý	vzniklý	k2eAgInSc4d1	vzniklý
quick	quick	k1gInSc4	quick
time	time	k1gFnSc1	time
foxtrot	foxtrot	k1gInSc1	foxtrot
se	se	k3xPyFc4	se
soutěží	soutěžit	k5eAaImIp3nS	soutěžit
v	v	k7c6	v
Charlestonu	charleston	k1gInSc6	charleston
<g/>
.	.	kIx.	.
</s>
<s>
Zvítězil	zvítězit	k5eAaPmAgInS	zvítězit
Frank	frank	k1gInSc1	frank
Ford	ford	k1gInSc1	ford
se	s	k7c7	s
svou	svůj	k3xOyFgFnSc7	svůj
nově	nově	k6eAd1	nově
vymyšlenou	vymyšlený	k2eAgFnSc7d1	vymyšlená
formou	forma	k1gFnSc7	forma
plochého	plochý	k2eAgInSc2d1	plochý
charlestonu	charleston	k1gInSc2	charleston
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
se	se	k3xPyFc4	se
sice	sice	k8xC	sice
v	v	k7c6	v
základu	základ	k1gInSc2	základ
rychlému	rychlý	k2eAgInSc3d1	rychlý
foxtrotu	foxtrot	k1gInSc3	foxtrot
neboli	neboli	k8xC	neboli
quickstepu	quickstep	k1gInSc3	quickstep
<g/>
,	,	kIx,	,
tak	tak	k8xC	tak
jak	jak	k6eAd1	jak
měl	mít	k5eAaImAgInS	mít
být	být	k5eAaImF	být
tehdy	tehdy	k6eAd1	tehdy
chápán	chápat	k5eAaImNgInS	chápat
<g/>
,	,	kIx,	,
mnoho	mnoho	k6eAd1	mnoho
nepodobal	podobat	k5eNaImAgMnS	podobat
<g/>
,	,	kIx,	,
avšak	avšak	k8xC	avšak
jeho	jeho	k3xOp3gFnPc1	jeho
variace	variace	k1gFnPc1	variace
snesly	snést	k5eAaPmAgFnP	snést
docela	docela	k6eAd1	docela
dobré	dobrý	k2eAgNnSc4d1	dobré
porovnání	porovnání	k1gNnSc4	porovnání
s	s	k7c7	s
variacemi	variace	k1gFnPc7	variace
dnešního	dnešní	k2eAgInSc2d1	dnešní
quickstepu	quickstep	k1gInSc2	quickstep
<g/>
.	.	kIx.	.
</s>
<s>
Základy	základ	k1gInPc1	základ
rychlého	rychlý	k2eAgInSc2d1	rychlý
foxtrotu	foxtrot	k1gInSc2	foxtrot
se	se	k3xPyFc4	se
vydělily	vydělit	k5eAaPmAgFnP	vydělit
z	z	k7c2	z
předcházející	předcházející	k2eAgFnSc2d1	předcházející
symbiózy	symbióza	k1gFnSc2	symbióza
pomalého	pomalý	k2eAgInSc2d1	pomalý
a	a	k8xC	a
rychlého	rychlý	k2eAgInSc2d1	rychlý
foxtrotu	foxtrot	k1gInSc2	foxtrot
a	a	k8xC	a
tak	tak	k6eAd1	tak
se	se	k3xPyFc4	se
v	v	k7c6	v
roce	rok	k1gInSc6	rok
1929	[number]	k4	1929
pouze	pouze	k6eAd1	pouze
doplnily	doplnit	k5eAaPmAgInP	doplnit
některými	některý	k3yIgInPc7	některý
prvky	prvek	k1gInPc7	prvek
a	a	k8xC	a
byl	být	k5eAaImAgInS	být
tu	tu	k6eAd1	tu
quickstep	quickstep	k1gInSc1	quickstep
<g/>
,	,	kIx,	,
který	který	k3yIgInSc1	který
se	se	k3xPyFc4	se
tančí	tančit	k5eAaImIp3nS	tančit
dodnes	dodnes	k6eAd1	dodnes
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1930	[number]	k4	1930
vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
řada	řada	k1gFnSc1	řada
dalších	další	k2eAgFnPc2d1	další
figur	figura	k1gFnPc2	figura
a	a	k8xC	a
variací	variace	k1gFnPc2	variace
vlivem	vlivem	k7c2	vlivem
swingu	swing	k1gInSc2	swing
a	a	k8xC	a
synkop	synkopa	k1gFnPc2	synkopa
<g/>
.	.	kIx.	.
</s>
<s>
Dnešní	dnešní	k2eAgFnSc3d1	dnešní
podobě	podoba	k1gFnSc3	podoba
quickstepu	quickstep	k1gInSc2	quickstep
se	se	k3xPyFc4	se
také	také	k9	také
někdy	někdy	k6eAd1	někdy
právem	právem	k6eAd1	právem
říká	říkat	k5eAaImIp3nS	říkat
<g/>
,	,	kIx,	,
že	že	k8xS	že
je	být	k5eAaImIp3nS	být
vrcholnou	vrcholný	k2eAgFnSc7d1	vrcholná
formou	forma	k1gFnSc7	forma
foxtrotu	foxtrot	k1gInSc2	foxtrot
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Literatura	literatura	k1gFnSc1	literatura
===	===	k?	===
</s>
</p>
<p>
<s>
Landsfeld	Landsfeld	k1gMnSc1	Landsfeld
<g/>
,	,	kIx,	,
Z.	Z.	kA	Z.
<g/>
,	,	kIx,	,
Plamínek	plamínek	k1gInSc1	plamínek
<g/>
,	,	kIx,	,
J.	J.	kA	J.
<g/>
;	;	kIx,	;
Technika	technika	k1gFnSc1	technika
standardních	standardní	k2eAgInPc2d1	standardní
tanců	tanec	k1gInPc2	tanec
<g/>
,	,	kIx,	,
Praha	Praha	k1gFnSc1	Praha
1999	[number]	k4	1999
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
Quickstep	quickstep	k1gInSc1	quickstep
</s>
</p>
