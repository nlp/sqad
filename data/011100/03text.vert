<p>
<s>
Vodník	vodník	k1gMnSc1	vodník
je	být	k5eAaImIp3nS	být
vodní	vodní	k2eAgMnSc1d1	vodní
démon	démon	k1gMnSc1	démon
ze	z	k7c2	z
slovanského	slovanský	k2eAgInSc2d1	slovanský
folklóru	folklór	k1gInSc2	folklór
obývající	obývající	k2eAgFnSc2d1	obývající
řeky	řeka	k1gFnSc2	řeka
<g/>
,	,	kIx,	,
potoky	potok	k1gInPc1	potok
<g/>
,	,	kIx,	,
jezera	jezero	k1gNnPc1	jezero
i	i	k8xC	i
rybníky	rybník	k1gInPc1	rybník
<g/>
.	.	kIx.	.
</s>
<s>
Podobá	podobat	k5eAaImIp3nS	podobat
se	se	k3xPyFc4	se
mu	on	k3xPp3gNnSc3	on
německý	německý	k2eAgInSc4d1	německý
nixe	nixe	k1gInSc4	nixe
nebo	nebo	k8xC	nebo
skotský	skotský	k1gInSc4	skotský
kelpie	kelpie	k1gFnSc2	kelpie
<g/>
.	.	kIx.	.
</s>
<s>
Vodník	vodník	k1gMnSc1	vodník
je	on	k3xPp3gNnPc4	on
znám	znát	k5eAaImIp1nS	znát
západním	západní	k2eAgInPc3d1	západní
i	i	k8xC	i
východním	východní	k2eAgInPc3d1	východní
Slovanům	Slovan	k1gInPc3	Slovan
<g/>
,	,	kIx,	,
z	z	k7c2	z
jižních	jižní	k2eAgFnPc2d1	jižní
pouze	pouze	k6eAd1	pouze
Slovincům	Slovinec	k1gMnPc3	Slovinec
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
ostatních	ostatní	k2eAgMnPc2d1	ostatní
Jihoslovanů	Jihoslovan	k1gMnPc2	Jihoslovan
se	se	k3xPyFc4	se
víra	víra	k1gFnSc1	víra
v	v	k7c4	v
něj	on	k3xPp3gMnSc4	on
neobjevuje	objevovat	k5eNaImIp3nS	objevovat
<g/>
,	,	kIx,	,
v	v	k7c6	v
Dalmácii	Dalmácie	k1gFnSc6	Dalmácie
však	však	k9	však
byli	být	k5eAaImAgMnP	být
známi	znám	k2eAgMnPc1d1	znám
morski	morski	k6eAd1	morski
ljudi	ljud	k1gMnPc1	ljud
<g/>
,	,	kIx,	,
bytosti	bytost	k1gFnPc1	bytost
napůl	napůl	k6eAd1	napůl
rybí	rybí	k2eAgFnPc1d1	rybí
<g/>
,	,	kIx,	,
napůl	napůl	k6eAd1	napůl
lidské	lidský	k2eAgFnPc1d1	lidská
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
partnerkou	partnerka	k1gFnSc7	partnerka
často	často	k6eAd1	často
bývá	bývat	k5eAaImIp3nS	bývat
vodní	vodní	k2eAgFnSc1d1	vodní
panna	panna	k1gFnSc1	panna
či	či	k8xC	či
rusalka	rusalka	k1gFnSc1	rusalka
<g/>
.	.	kIx.	.
</s>
<s>
Víra	víra	k1gFnSc1	víra
ve	v	k7c4	v
vodníka	vodník	k1gMnSc4	vodník
vychází	vycházet	k5eAaImIp3nS	vycházet
ze	z	k7c2	z
slovanského	slovanský	k2eAgInSc2d1	slovanský
kultu	kult	k1gInSc2	kult
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Etymologie	etymologie	k1gFnSc2	etymologie
==	==	k?	==
</s>
</p>
<p>
<s>
Slovo	slovo	k1gNnSc4	slovo
pro	pro	k7c4	pro
vodního	vodní	k2eAgMnSc4d1	vodní
démona	démon	k1gMnSc4	démon
je	být	k5eAaImIp3nS	být
ve	v	k7c6	v
většině	většina	k1gFnSc6	většina
slovanských	slovanský	k2eAgInPc2d1	slovanský
jazyků	jazyk	k1gInPc2	jazyk
jednoduše	jednoduše	k6eAd1	jednoduše
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
od	od	k7c2	od
vody	voda	k1gFnSc2	voda
<g/>
,	,	kIx,	,
příkladem	příklad	k1gInSc7	příklad
je	být	k5eAaImIp3nS	být
kromě	kromě	k7c2	kromě
českého	český	k2eAgMnSc2d1	český
vodníka	vodník	k1gMnSc2	vodník
ruský	ruský	k2eAgInSc1d1	ruský
vodjanoj	vodjanoj	k1gInSc1	vodjanoj
(	(	kIx(	(
<g/>
в	в	k?	в
<g/>
́	́	k?	́
<g/>
й	й	k?	й
<g/>
)	)	kIx)	)
nebo	nebo	k8xC	nebo
polský	polský	k2eAgInSc1d1	polský
wodnik	wodnik	k1gInSc1	wodnik
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Česku	Česko	k1gNnSc6	Česko
se	se	k3xPyFc4	se
také	také	k9	také
občas	občas	k6eAd1	občas
objevuje	objevovat	k5eAaImIp3nS	objevovat
výraz	výraz	k1gInSc4	výraz
vodní	vodní	k2eAgMnSc1d1	vodní
muž	muž	k1gMnSc1	muž
či	či	k8xC	či
vodní	vodní	k2eAgInSc1d1	vodní
mužíček	mužíček	k1gInSc1	mužíček
<g/>
,	,	kIx,	,
podobné	podobný	k2eAgNnSc1d1	podobné
je	být	k5eAaImIp3nS	být
slovinské	slovinský	k2eAgFnSc3d1	slovinská
povodnji	povodnj	k1gFnSc3	povodnj
mož	mož	k?	mož
nebo	nebo	k8xC	nebo
lužickosrbské	lužickosrbský	k2eAgFnSc2d1	lužickosrbská
wódny	wódna	k1gFnSc2	wódna
muž	muž	k1gMnSc1	muž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
V	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
lze	lze	k6eAd1	lze
nalézt	nalézt	k5eAaPmF	nalézt
také	také	k9	také
označení	označení	k1gNnSc4	označení
odvozené	odvozený	k2eAgNnSc4d1	odvozené
z	z	k7c2	z
německého	německý	k2eAgInSc2d1	německý
wasserman	wasserman	k1gMnSc1	wasserman
"	"	kIx"	"
<g/>
vodní	vodní	k2eAgMnSc1d1	vodní
muž	muž	k1gMnSc1	muž
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
jména	jméno	k1gNnPc4	jméno
jako	jako	k8xC	jako
hastrman	hastrman	k1gMnSc1	hastrman
<g/>
,	,	kIx,	,
vaserman	vaserman	k1gMnSc1	vaserman
<g/>
,	,	kIx,	,
vastrman	vastrman	k1gMnSc1	vastrman
<g/>
,	,	kIx,	,
na	na	k7c6	na
Moravě	Morava	k1gFnSc6	Morava
také	také	k9	také
bestrman	bestrman	k1gMnSc1	bestrman
<g/>
,	,	kIx,	,
ve	v	k7c6	v
Slezsku	Slezsko	k1gNnSc6	Slezsko
hasrman	hasrman	k1gMnSc1	hasrman
a	a	k8xC	a
na	na	k7c6	na
Ještědsku	Ještědsko	k1gNnSc6	Ještědsko
vosrmon	vosrmona	k1gFnPc2	vosrmona
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gNnSc1	jeho
použití	použití	k1gNnSc1	použití
je	být	k5eAaImIp3nS	být
doloženo	doložen	k2eAgNnSc1d1	doloženo
už	už	k6eAd1	už
ze	z	k7c2	z
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
německého	německý	k2eAgInSc2d1	německý
nixe	nix	k1gInSc2	nix
"	"	kIx"	"
<g/>
vodní	vodní	k2eAgMnSc1d1	vodní
démon	démon	k1gMnSc1	démon
<g/>
"	"	kIx"	"
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
lužické	lužický	k2eAgNnSc1d1	Lužické
nyks	nyks	k1gInSc4	nyks
a	a	k8xC	a
nykus	nykus	k1gInSc4	nykus
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
rozšířené	rozšířený	k2eAgFnSc2d1	rozšířená
víry	víra	k1gFnSc2	víra
v	v	k7c4	v
jeho	on	k3xPp3gInSc4	on
kapající	kapající	k2eAgInSc4d1	kapající
šos	šos	k1gInSc4	šos
je	být	k5eAaImIp3nS	být
odvozeno	odvozen	k2eAgNnSc1d1	odvozeno
označení	označení	k1gNnSc1	označení
šosáč	šosáč	k1gInSc4	šosáč
používané	používaný	k2eAgFnSc2d1	používaná
na	na	k7c4	na
Litovelsku	Litovelska	k1gFnSc4	Litovelska
<g/>
.	.	kIx.	.
</s>
<s>
Z	z	k7c2	z
topení	topení	k1gNnSc2	topení
<g/>
,	,	kIx,	,
jejich	jejich	k3xOp3gFnSc2	jejich
typické	typický	k2eAgFnSc2d1	typická
činnosti	činnost	k1gFnSc2	činnost
<g/>
,	,	kIx,	,
je	být	k5eAaImIp3nS	být
odvozeno	odvodit	k5eAaPmNgNnS	odvodit
polské	polský	k2eAgNnSc1d1	polské
topielec	topielec	k1gInSc4	topielec
a	a	k8xC	a
toplec	toplec	k1gInSc4	toplec
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Charakteristika	charakteristikon	k1gNnSc2	charakteristikon
==	==	k?	==
</s>
</p>
<p>
<s>
Představy	představa	k1gFnPc1	představa
o	o	k7c6	o
vodníkovi	vodník	k1gMnSc6	vodník
jsou	být	k5eAaImIp3nP	být
u	u	k7c2	u
Slovanů	Slovan	k1gInPc2	Slovan
v	v	k7c6	v
základě	základ	k1gInSc6	základ
podobné	podobný	k2eAgNnSc1d1	podobné
<g/>
.	.	kIx.	.
</s>
<s>
Mívá	mívat	k5eAaImIp3nS	mívat
podobu	podoba	k1gFnSc4	podoba
malého	malý	k2eAgMnSc2d1	malý
mužíka	mužík	k1gMnSc2	mužík
<g/>
,	,	kIx,	,
dospělého	dospělý	k2eAgMnSc2d1	dospělý
muže	muž	k1gMnSc2	muž
či	či	k8xC	či
starce	stařec	k1gMnSc2	stařec
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
mívá	mívat	k5eAaImIp3nS	mívat
zelené	zelený	k2eAgInPc4d1	zelený
vlasy	vlas	k1gInPc4	vlas
i	i	k8xC	i
vousy	vous	k1gInPc4	vous
<g/>
.	.	kIx.	.
</s>
<s>
U	u	k7c2	u
Západoslovanů	Západoslovan	k1gMnPc2	Západoslovan
a	a	k8xC	a
Slovinců	Slovinec	k1gMnPc2	Slovinec
bývá	bývat	k5eAaImIp3nS	bývat
pěkně	pěkně	k6eAd1	pěkně
oblečený	oblečený	k2eAgMnSc1d1	oblečený
<g/>
,	,	kIx,	,
nejčastěji	často	k6eAd3	často
v	v	k7c6	v
zelené	zelená	k1gFnSc6	zelená
či	či	k8xC	či
červené	červená	k1gFnSc6	červená
<g/>
,	,	kIx,	,
takovou	takový	k3xDgFnSc4	takový
barvu	barva	k1gFnSc4	barva
mívá	mívat	k5eAaImIp3nS	mívat
i	i	k9	i
kouzelný	kouzelný	k2eAgInSc1d1	kouzelný
prut	prut	k1gInSc1	prut
či	či	k8xC	či
metla	metla	k1gFnSc1	metla
<g/>
,	,	kIx,	,
kterým	který	k3yQgInSc7	který
ovládá	ovládat	k5eAaImIp3nS	ovládat
vodu	voda	k1gFnSc4	voda
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
se	se	k3xPyFc4	se
měnit	měnit	k5eAaImF	měnit
v	v	k7c4	v
člověka	člověk	k1gMnSc4	člověk
<g/>
,	,	kIx,	,
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
případě	případ	k1gInSc6	případ
jej	on	k3xPp3gInSc4	on
však	však	k9	však
lze	lze	k6eAd1	lze
identifikovat	identifikovat	k5eAaBmF	identifikovat
podle	podle	k7c2	podle
vody	voda	k1gFnSc2	voda
kapající	kapající	k2eAgFnSc2d1	kapající
ze	z	k7c2	z
šosu	šos	k1gInSc2	šos
<g/>
.	.	kIx.	.
</s>
<s>
Mění	měnit	k5eAaImIp3nS	měnit
se	se	k3xPyFc4	se
také	také	k9	také
v	v	k7c4	v
různá	různý	k2eAgNnPc4d1	různé
zvířata	zvíře	k1gNnPc4	zvíře
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
rybu	ryba	k1gFnSc4	ryba
<g/>
,	,	kIx,	,
koně	kůň	k1gMnSc4	kůň
či	či	k8xC	či
žábu	žába	k1gFnSc4	žába
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
v	v	k7c4	v
neživé	živý	k2eNgInPc4d1	neživý
předměty	předmět	k1gInPc4	předmět
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Obývá	obývat	k5eAaImIp3nS	obývat
vodní	vodní	k2eAgFnSc2d1	vodní
hlubiny	hlubina	k1gFnSc2	hlubina
<g/>
,	,	kIx,	,
obzvláště	obzvláště	k6eAd1	obzvláště
v	v	k7c6	v
blízkosti	blízkost	k1gFnSc6	blízkost
mlýnů	mlýn	k1gInPc2	mlýn
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
má	mít	k5eAaImIp3nS	mít
svůj	svůj	k3xOyFgInSc4	svůj
dům	dům	k1gInSc4	dům
či	či	k8xC	či
křišťálový	křišťálový	k2eAgInSc4d1	křišťálový
palác	palác	k1gInSc4	palác
<g/>
.	.	kIx.	.
</s>
<s>
Je	být	k5eAaImIp3nS	být
aktivní	aktivní	k2eAgNnSc1d1	aktivní
především	především	k9	především
v	v	k7c6	v
noci	noc	k1gFnSc6	noc
kdy	kdy	k6eAd1	kdy
na	na	k7c6	na
hladině	hladina	k1gFnSc6	hladina
tleská	tleskat	k5eAaImIp3nS	tleskat
<g/>
,	,	kIx,	,
hází	házet	k5eAaImIp3nS	házet
sebou	se	k3xPyFc7	se
jako	jako	k9	jako
ryba	ryba	k1gFnSc1	ryba
nebo	nebo	k8xC	nebo
si	se	k3xPyFc3	se
na	na	k7c6	na
mlýnském	mlýnský	k2eAgNnSc6d1	mlýnské
kole	kolo	k1gNnSc6	kolo
rozčesává	rozčesávat	k5eAaImIp3nS	rozčesávat
vlasy	vlas	k1gInPc7	vlas
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
vodě	voda	k1gFnSc6	voda
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
mocný	mocný	k2eAgMnSc1d1	mocný
<g/>
,	,	kIx,	,
zatímco	zatímco	k8xS	zatímco
na	na	k7c6	na
suchu	sucho	k1gNnSc6	sucho
zpravidla	zpravidla	k6eAd1	zpravidla
slabý	slabý	k2eAgMnSc1d1	slabý
<g/>
.	.	kIx.	.
</s>
<s>
Vládne	vládnout	k5eAaImIp3nS	vládnout
rybám	ryba	k1gFnPc3	ryba
<g/>
,	,	kIx,	,
vodním	vodní	k2eAgNnPc3d1	vodní
zvířatům	zvíře	k1gNnPc3	zvíře
i	i	k9	i
vodními	vodní	k2eAgFnPc7d1	vodní
ptactvu	ptactvo	k1gNnSc6	ptactvo
<g/>
.	.	kIx.	.
</s>
<s>
Vodník	vodník	k1gMnSc1	vodník
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
samotář	samotář	k1gMnSc1	samotář
nebo	nebo	k8xC	nebo
má	mít	k5eAaImIp3nS	mít
ženu	žena	k1gFnSc4	žena
a	a	k8xC	a
dcery	dcera	k1gFnPc4	dcera
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnSc7	jeho
nevěstou	nevěsta	k1gFnSc7	nevěsta
je	být	k5eAaImIp3nS	být
buď	buď	k8xC	buď
vodní	vodní	k2eAgFnSc1d1	vodní
panna	panna	k1gFnSc1	panna
nebo	nebo	k8xC	nebo
dívka	dívka	k1gFnSc1	dívka
kterou	který	k3yRgFnSc4	který
utopil	utopit	k5eAaPmAgMnS	utopit
či	či	k8xC	či
která	který	k3yIgFnSc1	který
byla	být	k5eAaImAgFnS	být
prokleta	proklít	k5eAaPmNgFnS	proklít
svým	svůj	k1gMnSc7	svůj
otcem	otec	k1gMnSc7	otec
nebo	nebo	k8xC	nebo
matkou	matka	k1gFnSc7	matka
<g/>
,	,	kIx,	,
ta	ten	k3xDgFnSc1	ten
je	být	k5eAaImIp3nS	být
většinou	většinou	k6eAd1	většinou
lidem	lid	k1gInSc7	lid
více	hodně	k6eAd2	hodně
nakloněna	naklonit	k5eAaPmNgFnS	naklonit
než	než	k8xS	než
její	její	k3xOp3gMnSc1	její
muž	muž	k1gMnSc1	muž
<g/>
.	.	kIx.	.
</s>
<s>
Když	když	k8xS	když
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
rodí	rodit	k5eAaImIp3nS	rodit
vybírá	vybírat	k5eAaImIp3nS	vybírat
si	se	k3xPyFc3	se
kmotry	kmotra	k1gFnPc1	kmotra
mezi	mezi	k7c7	mezi
lidmi	člověk	k1gMnPc7	člověk
<g/>
,	,	kIx,	,
které	který	k3yRgNnSc1	který
za	za	k7c4	za
to	ten	k3xDgNnSc4	ten
odměňuje	odměňovat	k5eAaImIp3nS	odměňovat
zlatem	zlato	k1gNnSc7	zlato
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
dcery	dcera	k1gFnPc1	dcera
bývají	bývat	k5eAaImIp3nP	bývat
velice	velice	k6eAd1	velice
krásné	krásný	k2eAgFnPc1d1	krásná
a	a	k8xC	a
svádějí	svádět	k5eAaImIp3nP	svádět
lidské	lidský	k2eAgMnPc4d1	lidský
muže	muž	k1gMnPc4	muž
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Vztah	vztah	k1gInSc1	vztah
k	k	k7c3	k
lidem	člověk	k1gMnPc3	člověk
je	být	k5eAaImIp3nS	být
zpravidla	zpravidla	k6eAd1	zpravidla
negativní	negativní	k2eAgMnSc1d1	negativní
<g/>
,	,	kIx,	,
ačkoliv	ačkoliv	k8xS	ačkoliv
v	v	k7c6	v
lidové	lidový	k2eAgFnSc6d1	lidová
a	a	k8xC	a
populární	populární	k2eAgFnSc6d1	populární
kultuře	kultura	k1gFnSc6	kultura
se	se	k3xPyFc4	se
později	pozdě	k6eAd2	pozdě
stal	stát	k5eAaPmAgMnS	stát
spíše	spíše	k9	spíše
komickou	komický	k2eAgFnSc7d1	komická
postavou	postava	k1gFnSc7	postava
<g/>
.	.	kIx.	.
</s>
<s>
Někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
jeho	jeho	k3xOp3gNnSc1	jeho
chování	chování	k1gNnSc1	chování
spíše	spíše	k9	spíše
šibalské	šibalský	k2eAgNnSc1d1	šibalské
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
strašení	strašení	k1gNnSc1	strašení
rybářů	rybář	k1gMnPc2	rybář
nebo	nebo	k8xC	nebo
protrhávání	protrhávání	k1gNnSc2	protrhávání
sítí	síť	k1gFnPc2	síť
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
vysloveně	vysloveně	k6eAd1	vysloveně
zlovolné	zlovolný	k2eAgNnSc1d1	zlovolné
<g/>
.	.	kIx.	.
</s>
<s>
Často	často	k6eAd1	často
je	být	k5eAaImIp3nS	být
obzvláště	obzvláště	k6eAd1	obzvláště
nebezpečný	bezpečný	k2eNgInSc1d1	nebezpečný
mlynářům	mlynář	k1gMnPc3	mlynář
<g/>
.	.	kIx.	.
</s>
<s>
Západoslovanští	západoslovanský	k2eAgMnPc1d1	západoslovanský
vodníci	vodník	k1gMnPc1	vodník
přechovávají	přechovávat	k5eAaImIp3nP	přechovávat
duše	duše	k1gFnPc4	duše
utopených	utopený	k1gMnPc2	utopený
v	v	k7c6	v
hrncích	hrnec	k1gInPc6	hrnec
či	či	k8xC	či
hrníčcích	hrníček	k1gInPc6	hrníček
<g/>
.	.	kIx.	.
</s>
<s>
Obzvláště	obzvláště	k6eAd1	obzvláště
nebezpečný	bezpečný	k2eNgMnSc1d1	nebezpečný
je	být	k5eAaImIp3nS	být
lidem	člověk	k1gMnPc3	člověk
kteří	který	k3yRgMnPc1	který
se	se	k3xPyFc4	se
koupou	koupat	k5eAaImIp3nP	koupat
v	v	k7c4	v
poledne	poledne	k1gNnSc4	poledne
či	či	k8xC	či
o	o	k7c6	o
půlnoci	půlnoc	k1gFnSc6	půlnoc
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
neškodný	škodný	k2eNgMnSc1d1	neškodný
měl	mít	k5eAaImAgMnS	mít
být	být	k5eAaImF	být
v	v	k7c4	v
pátek	pátek	k1gInSc4	pátek
<g/>
,	,	kIx,	,
což	což	k3yRnSc1	což
byl	být	k5eAaImAgMnS	být
den	den	k1gInSc4	den
který	který	k3yRgMnSc1	který
mu	on	k3xPp3gMnSc3	on
byl	být	k5eAaImAgMnS	být
zasvěcen	zasvěcen	k2eAgMnSc1d1	zasvěcen
<g/>
.	.	kIx.	.
<g/>
(	(	kIx(	(
<g/>
Váňa	Váňa	k1gMnSc1	Váňa
<g/>
,	,	kIx,	,
s.	s.	k?	s.
116	[number]	k4	116
<g/>
)	)	kIx)	)
Blahovolný	blahovolný	k2eAgMnSc1d1	blahovolný
vodník	vodník	k1gMnSc1	vodník
zase	zase	k9	zase
rybářům	rybář	k1gMnPc3	rybář
nahání	nahánět	k5eAaImIp3nS	nahánět
ryby	ryba	k1gFnPc1	ryba
do	do	k7c2	do
sítí	síť	k1gFnPc2	síť
<g/>
.	.	kIx.	.
</s>
<s>
Rozšířená	rozšířený	k2eAgFnSc1d1	rozšířená
je	být	k5eAaImIp3nS	být
víra	víra	k1gFnSc1	víra
že	že	k8xS	že
s	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
navštěvuje	navštěvovat	k5eAaImIp3nS	navštěvovat
trhy	trh	k1gInPc4	trh
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc4	jeho
přítomnost	přítomnost	k1gFnSc4	přítomnost
věští	věštit	k5eAaImIp3nS	věštit
výši	výše	k1gFnSc4	výše
úrody	úroda	k1gFnSc2	úroda
či	či	k8xC	či
cen	cena	k1gFnPc2	cena
zboží	zboží	k1gNnSc4	zboží
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
lidové	lidový	k2eAgFnSc6d1	lidová
kultuře	kultura	k1gFnSc6	kultura
se	se	k3xPyFc4	se
také	také	k9	také
objevují	objevovat	k5eAaImIp3nP	objevovat
oběti	oběť	k1gFnPc1	oběť
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc4	který
měli	mít	k5eAaImAgMnP	mít
zajistit	zajistit	k5eAaPmF	zajistit
vodníkovu	vodníkův	k2eAgFnSc4d1	Vodníkova
přízeň	přízeň	k1gFnSc4	přízeň
či	či	k8xC	či
ochránit	ochránit	k5eAaPmF	ochránit
před	před	k7c7	před
jeho	jeho	k3xOp3gInSc7	jeho
hněvem	hněv	k1gInSc7	hněv
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Původ	původ	k1gInSc1	původ
vodníků	vodník	k1gMnPc2	vodník
býval	bývat	k5eAaImAgInS	bývat
někdy	někdy	k6eAd1	někdy
odvozován	odvozovat	k5eAaImNgInS	odvozovat
od	od	k7c2	od
faljaronů	faljaron	k1gMnPc2	faljaron
<g/>
,	,	kIx,	,
utopeného	utopený	k1gMnSc2	utopený
vojska	vojsko	k1gNnSc2	vojsko
faraóna	faraón	k1gMnSc2	faraón
z	z	k7c2	z
biblického	biblický	k2eAgInSc2d1	biblický
Exodu	Exodus	k1gInSc2	Exodus
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
od	od	k7c2	od
andělů	anděl	k1gMnPc2	anděl
svržených	svržený	k2eAgMnPc2d1	svržený
z	z	k7c2	z
nebes	nebesa	k1gNnPc2	nebesa
<g/>
,	,	kIx,	,
kteří	který	k3yIgMnPc1	který
spadli	spadnout	k5eAaPmAgMnP	spadnout
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
<g/>
Podle	podle	k7c2	podle
jiného	jiný	k2eAgNnSc2d1	jiné
se	se	k3xPyFc4	se
jedná	jednat	k5eAaImIp3nS	jednat
o	o	k7c6	o
umrlce	umrlce	k?	umrlce
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterého	který	k3yIgInSc2	který
byl	být	k5eAaImAgInS	být
zatopením	zatopení	k1gNnPc3	zatopení
vodou	voda	k1gFnSc7	voda
hřbitova	hřbitov	k1gInSc2	hřbitov
<g/>
,	,	kIx,	,
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgInS	být
pohřben	pohřben	k2eAgInSc1d1	pohřben
<g/>
,	,	kIx,	,
smyta	smyt	k2eAgFnSc1d1	smyta
svěcená	svěcený	k2eAgFnSc1d1	svěcená
voda	voda	k1gFnSc1	voda
křtu	křest	k1gInSc2	křest
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Česko	Česko	k1gNnSc1	Česko
===	===	k?	===
</s>
</p>
<p>
<s>
Český	český	k2eAgMnSc1d1	český
vodník	vodník	k1gMnSc1	vodník
má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
malého	malý	k2eAgMnSc2d1	malý
muže	muž	k1gMnSc2	muž
se	s	k7c7	s
zelenými	zelený	k2eAgInPc7d1	zelený
vlasy	vlas	k1gInPc7	vlas
<g/>
,	,	kIx,	,
hezky	hezky	k6eAd1	hezky
oblečeného	oblečený	k2eAgMnSc2d1	oblečený
do	do	k7c2	do
zelené	zelený	k2eAgFnSc2d1	zelená
či	či	k8xC	či
červené	červený	k2eAgFnSc2d1	červená
<g/>
,	,	kIx,	,
kterému	který	k3yRgMnSc3	který
ze	z	k7c2	z
šosu	šos	k1gInSc2	šos
kape	kapat	k5eAaImIp3nS	kapat
voda	voda	k1gFnSc1	voda
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiného	jiný	k2eAgNnSc2d1	jiné
podání	podání	k1gNnSc2	podání
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc4	ten
ošklivý	ošklivý	k2eAgMnSc1d1	ošklivý
starý	starý	k2eAgMnSc1d1	starý
zelený	zelený	k2eAgMnSc1d1	zelený
muž	muž	k1gMnSc1	muž
se	s	k7c7	s
žabími	žabí	k2eAgInPc7d1	žabí
rysy	rys	k1gInPc7	rys
jako	jako	k8xC	jako
široká	široký	k2eAgNnPc4d1	široké
ústa	ústa	k1gNnPc4	ústa
<g/>
,	,	kIx,	,
blána	blána	k1gFnSc1	blána
mezi	mezi	k7c4	mezi
prsty	prst	k1gInPc4	prst
nebo	nebo	k8xC	nebo
vypoulené	vypoulený	k2eAgNnSc4d1	vypoulené
oči	oko	k1gNnPc4	oko
<g/>
,	,	kIx,	,
jindy	jindy	k6eAd1	jindy
má	mít	k5eAaImIp3nS	mít
pravé	pravý	k2eAgNnSc1d1	pravé
oko	oko	k1gNnSc1	oko
rudé	rudý	k2eAgNnSc1d1	Rudé
<g/>
,	,	kIx,	,
voda	voda	k1gFnSc1	voda
mu	on	k3xPp3gMnSc3	on
neustále	neustále	k6eAd1	neustále
kape	kapat	k5eAaImIp3nS	kapat
z	z	k7c2	z
vlasů	vlas	k1gInPc2	vlas
a	a	k8xC	a
jeho	jeho	k3xOp3gNnSc1	jeho
tělo	tělo	k1gNnSc1	tělo
je	být	k5eAaImIp3nS	být
měkké	měkký	k2eAgNnSc1d1	měkké
jako	jako	k8xS	jako
bláto	bláto	k1gNnSc1	bláto
<g/>
.	.	kIx.	.
</s>
<s>
Řeč	řeč	k1gFnSc1	řeč
má	mít	k5eAaImIp3nS	mít
huhňavou	huhňavý	k2eAgFnSc4d1	huhňavá
a	a	k8xC	a
"	"	kIx"	"
<g/>
překroucenou	překroucený	k2eAgFnSc7d1	překroucená
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Láká	lákat	k5eAaImIp3nS	lákat
k	k	k7c3	k
sobě	se	k3xPyFc3	se
především	především	k6eAd1	především
děti	dítě	k1gFnPc4	dítě
a	a	k8xC	a
mladé	mladý	k2eAgFnPc4d1	mladá
dívky	dívka	k1gFnPc4	dívka
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
tím	ten	k3xDgNnSc7	ten
že	že	k8xS	že
na	na	k7c6	na
břehu	břeh	k1gInSc6	břeh
rozvěsí	rozvěsit	k5eAaPmIp3nP	rozvěsit
barevné	barevný	k2eAgFnPc1d1	barevná
stuhy	stuha	k1gFnPc1	stuha
a	a	k8xC	a
zrcátka	zrcátko	k1gNnPc1	zrcátko
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
se	se	k3xPyFc4	se
promění	proměnit	k5eAaPmIp3nS	proměnit
v	v	k7c4	v
dítě	dítě	k1gNnSc4	dítě
<g/>
,	,	kIx,	,
které	který	k3yIgNnSc1	který
spadne	spadnout	k5eAaPmIp3nS	spadnout
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
a	a	k8xC	a
svého	svůj	k3xOyFgMnSc2	svůj
zachránce	zachránce	k1gMnSc2	zachránce
utopí	utopit	k5eAaPmIp3nP	utopit
<g/>
.	.	kIx.	.
</s>
<s>
Vodník	vodník	k1gMnSc1	vodník
později	pozdě	k6eAd2	pozdě
v	v	k7c6	v
lidové	lidový	k2eAgFnSc6d1	lidová
tradici	tradice	k1gFnSc6	tradice
a	a	k8xC	a
populární	populární	k2eAgFnSc3d1	populární
kultuře	kultura	k1gFnSc3	kultura
ztrácel	ztrácet	k5eAaImAgInS	ztrácet
svojí	svojit	k5eAaImIp3nS	svojit
zlovolnost	zlovolnost	k1gFnSc4	zlovolnost
a	a	k8xC	a
stal	stát	k5eAaPmAgMnS	stát
se	se	k3xPyFc4	se
podobně	podobně	k6eAd1	podobně
jako	jako	k9	jako
čert	čert	k1gMnSc1	čert
komickou	komický	k2eAgFnSc7d1	komická
postavou	postava	k1gFnSc7	postava
<g/>
.	.	kIx.	.
<g/>
Vodníka	vodník	k1gMnSc4	vodník
lze	lze	k6eAd1	lze
spoutat	spoutat	k5eAaPmF	spoutat
lýkovým	lýkový	k2eAgInSc7d1	lýkový
provazem	provaz	k1gInSc7	provaz
<g/>
,	,	kIx,	,
barevnými	barevný	k2eAgFnPc7d1	barevná
tkanicemi	tkanice	k1gFnPc7	tkanice
<g/>
,	,	kIx,	,
klokočím	klokočí	k2eAgInSc7d1	klokočí
posvěceným	posvěcený	k2eAgInSc7d1	posvěcený
o	o	k7c6	o
Květné	květný	k2eAgFnSc6d1	Květná
neděli	neděle	k1gFnSc6	neděle
či	či	k8xC	či
houžví	houžev	k1gFnSc7	houžev
z	z	k7c2	z
devíti	devět	k4xCc2	devět
druhů	druh	k1gInPc2	druh
dřev	dřevo	k1gNnPc2	dřevo
<g/>
.	.	kIx.	.
</s>
<s>
Může	moct	k5eAaImIp3nS	moct
ho	on	k3xPp3gMnSc4	on
také	také	k9	také
zneškodnit	zneškodnit	k5eAaPmF	zneškodnit
odvážný	odvážný	k2eAgMnSc1d1	odvážný
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
když	když	k8xS	když
mu	on	k3xPp3gMnSc3	on
odstřihne	odstřihnout	k5eAaPmIp3nS	odstřihnout
jeho	jeho	k3xOp3gInSc1	jeho
mokrý	mokrý	k2eAgInSc1d1	mokrý
šos	šos	k1gInSc1	šos
<g/>
,	,	kIx,	,
který	který	k3yQgInSc1	který
omotá	omotat	k5eAaPmIp3nS	omotat
růžencem	růženec	k1gInSc7	růženec
a	a	k8xC	a
zakope	zakopat	k5eAaPmIp3nS	zakopat
v	v	k7c6	v
posvěcené	posvěcený	k2eAgFnSc6d1	posvěcená
zemi	zem	k1gFnSc6	zem
nebo	nebo	k8xC	nebo
na	na	k7c6	na
rozcestí	rozcestí	k1gNnSc6	rozcestí
<g/>
.	.	kIx.	.
</s>
<s>
Vodník	vodník	k1gMnSc1	vodník
přitom	přitom	k6eAd1	přitom
člověka	člověk	k1gMnSc4	člověk
pronásleduje	pronásledovat	k5eAaImIp3nS	pronásledovat
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
pokud	pokud	k8xS	pokud
svůj	svůj	k3xOyFgInSc4	svůj
šos	šos	k1gInSc4	šos
nezíská	získat	k5eNaPmIp3nS	získat
do	do	k7c2	do
příštího	příští	k2eAgNnSc2d1	příští
svítání	svítání	k1gNnSc2	svítání
zhyne	zhynout	k5eAaPmIp3nS	zhynout
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k9	jako
ochrana	ochrana	k1gFnSc1	ochrana
před	před	k7c7	před
ním	on	k3xPp3gMnSc7	on
fungují	fungovat	k5eAaImIp3nP	fungovat
také	také	k9	také
byliny	bylina	k1gFnPc1	bylina
jako	jako	k8xC	jako
černobýl	černobýl	k1gInSc1	černobýl
<g/>
,	,	kIx,	,
tolita	tolita	k1gFnSc1	tolita
<g/>
,	,	kIx,	,
turan	turan	k1gInSc1	turan
<g/>
,	,	kIx,	,
kapradí	kapradí	k1gNnSc1	kapradí
nebo	nebo	k8xC	nebo
devětsil	devětsil	k1gInSc1	devětsil
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Polsko	Polsko	k1gNnSc1	Polsko
===	===	k?	===
</s>
</p>
<p>
<s>
V	v	k7c6	v
Polsku	Polsko	k1gNnSc6	Polsko
je	být	k5eAaImIp3nS	být
specifickou	specifický	k2eAgFnSc7d1	specifická
formou	forma	k1gFnSc7	forma
vodníka	vodník	k1gMnSc2	vodník
bagiennik	bagiennik	k1gInSc4	bagiennik
<g/>
,	,	kIx,	,
obývající	obývající	k2eAgFnSc1d1	obývající
údajně	údajně	k6eAd1	údajně
okolí	okolí	k1gNnSc3	okolí
řeky	řeka	k1gFnSc2	řeka
Biebrzy	Biebrza	k1gFnSc2	Biebrza
<g/>
.	.	kIx.	.
</s>
<s>
Obývá	obývat	k5eAaImIp3nS	obývat
mělké	mělký	k2eAgInPc4d1	mělký
močály	močál	k1gInPc4	močál
a	a	k8xC	a
rybníky	rybník	k1gInPc4	rybník
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
veliký	veliký	k2eAgInSc1d1	veliký
asi	asi	k9	asi
jeden	jeden	k4xCgMnSc1	jeden
metr	metr	k1gMnSc1	metr
<g/>
.	.	kIx.	.
</s>
<s>
Jeho	jeho	k3xOp3gFnPc1	jeho
nosní	nosní	k2eAgFnPc1d1	nosní
dírky	dírka	k1gFnPc1	dírka
jsou	být	k5eAaImIp3nP	být
umístěny	umístit	k5eAaPmNgFnP	umístit
mezi	mezi	k7c7	mezi
očima	oko	k1gNnPc7	oko
či	či	k8xC	či
na	na	k7c6	na
jeho	jeho	k3xOp3gNnSc6	jeho
čele	čelo	k1gNnSc6	čelo
a	a	k8xC	a
střílí	střílet	k5eAaImIp3nS	střílet
z	z	k7c2	z
nich	on	k3xPp3gMnPc2	on
zvláštní	zvláštní	k2eAgMnSc1d1	zvláštní
bláto	bláto	k1gNnSc1	bláto
způsobující	způsobující	k2eAgFnPc1d1	způsobující
popáleniny	popálenina	k1gFnPc1	popálenina
<g/>
.	.	kIx.	.
</s>
<s>
Tato	tento	k3xDgFnSc1	tento
látka	látka	k1gFnSc1	látka
má	mít	k5eAaImIp3nS	mít
však	však	k9	však
také	také	k9	také
léčivé	léčivý	k2eAgInPc4d1	léčivý
účinky	účinek	k1gInPc4	účinek
<g/>
.	.	kIx.	.
</s>
<s>
Setkání	setkání	k1gNnSc1	setkání
s	s	k7c7	s
ním	on	k3xPp3gInSc7	on
nebývá	bývat	k5eNaImIp3nS	bývat
smrtelné	smrtelný	k2eAgNnSc1d1	smrtelné
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Lužice	Lužice	k1gFnSc2	Lužice
===	===	k?	===
</s>
</p>
<p>
<s>
Lužičkosrbský	Lužičkosrbský	k2eAgMnSc1d1	Lužičkosrbský
wódny	wódna	k1gFnSc2	wódna
muž	muž	k1gMnSc1	muž
<g/>
,	,	kIx,	,
nyks	nyks	k1gInSc1	nyks
či	či	k8xC	či
nykus	nykus	k1gInSc1	nykus
mívá	mívat	k5eAaImIp3nS	mívat
podobu	podoba	k1gFnSc4	podoba
různou	různý	k2eAgFnSc4d1	různá
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Horní	horní	k2eAgFnSc6d1	horní
Lužici	Lužice	k1gFnSc6	Lužice
je	být	k5eAaImIp3nS	být
to	ten	k3xDgNnSc1	ten
dospělý	dospělý	k2eAgMnSc1d1	dospělý
muž	muž	k1gMnSc1	muž
s	s	k7c7	s
červeným	červený	k2eAgInSc7d1	červený
kabátcem	kabátec	k1gInSc7	kabátec
a	a	k8xC	a
čapkou	čapka	k1gFnSc7	čapka
<g/>
,	,	kIx,	,
jeho	jeho	k3xOp3gFnSc1	jeho
žena	žena	k1gFnSc1	žena
má	mít	k5eAaImIp3nS	mít
červené	červený	k2eAgFnPc4d1	červená
punčochy	punčocha	k1gFnPc4	punčocha
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Dolní	dolní	k2eAgFnSc6d1	dolní
Lužici	Lužice	k1gFnSc6	Lužice
má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
malého	malý	k2eAgNnSc2d1	malé
dítěte	dítě	k1gNnSc2	dítě
<g/>
,	,	kIx,	,
také	také	k9	také
s	s	k7c7	s
oblibou	obliba	k1gFnSc7	obliba
nosící	nosící	k2eAgFnSc4d1	nosící
červenou	červená	k1gFnSc4	červená
<g/>
.	.	kIx.	.
</s>
<s>
Vodníkem	vodník	k1gMnSc7	vodník
se	se	k3xPyFc4	se
také	také	k9	také
můžou	můžou	k?	můžou
stát	stát	k5eAaPmF	stát
utopení	utopený	k2eAgMnPc1d1	utopený
lidé	člověk	k1gMnPc1	člověk
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Rusko	Rusko	k1gNnSc1	Rusko
===	===	k?	===
</s>
</p>
<p>
<s>
Ruský	ruský	k2eAgMnSc1d1	ruský
vodník	vodník	k1gMnSc1	vodník
se	se	k3xPyFc4	se
nazývá	nazývat	k5eAaImIp3nS	nazývat
vodjanoj	vodjanoj	k1gInSc1	vodjanoj
<g/>
,	,	kIx,	,
vodovik	vodovik	k1gMnSc1	vodovik
či	či	k8xC	či
vodjanik	vodjanik	k1gMnSc1	vodjanik
<g/>
,	,	kIx,	,
někdy	někdy	k6eAd1	někdy
je	být	k5eAaImIp3nS	být
také	také	k9	také
titulován	titulován	k2eAgMnSc1d1	titulován
děduška	děduška	k1gMnSc1	děduška
<g/>
,	,	kIx,	,
a	a	k8xC	a
má	mít	k5eAaImIp3nS	mít
podobu	podoba	k1gFnSc4	podoba
holohlavého	holohlavý	k2eAgMnSc2d1	holohlavý
starce	stařec	k1gMnSc2	stařec
s	s	k7c7	s
odulým	odulý	k2eAgNnSc7d1	odulé
břichem	břicho	k1gNnSc7	břicho
<g/>
,	,	kIx,	,
opuchlou	opuchlý	k2eAgFnSc7d1	opuchlá
tváří	tvář	k1gFnSc7	tvář
<g/>
,	,	kIx,	,
vysokou	vysoký	k2eAgFnSc7d1	vysoká
čepicí	čepice	k1gFnSc7	čepice
ze	z	k7c2	z
sítiny	sítina	k1gFnSc2	sítina
a	a	k8xC	a
pásem	pásmo	k1gNnPc2	pásmo
z	z	k7c2	z
vodních	vodní	k2eAgFnPc2d1	vodní
trav	tráva	k1gFnPc2	tráva
<g/>
.	.	kIx.	.
</s>
<s>
Pod	pod	k7c7	pod
vodou	voda	k1gFnSc7	voda
chová	chovat	k5eAaImIp3nS	chovat
stáda	stádo	k1gNnPc1	stádo
dobytka	dobytek	k1gInSc2	dobytek
<g/>
,	,	kIx,	,
koní	kůň	k1gMnPc2	kůň
<g/>
,	,	kIx,	,
ovcí	ovce	k1gFnPc2	ovce
a	a	k8xC	a
vepřů	vepř	k1gMnPc2	vepř
<g/>
.	.	kIx.	.
</s>
<s>
Za	za	k7c4	za
svá	svůj	k3xOyFgNnPc4	svůj
stáda	stádo	k1gNnPc4	stádo
počítá	počítat	k5eAaImIp3nS	počítat
i	i	k9	i
ryby	ryba	k1gFnPc4	ryba
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
láká	lákat	k5eAaImIp3nS	lákat
do	do	k7c2	do
vod	voda	k1gFnPc2	voda
kde	kde	k6eAd1	kde
přebývá	přebývat	k5eAaImIp3nS	přebývat
<g/>
.	.	kIx.	.
</s>
<s>
Jezdí	jezdit	k5eAaImIp3nS	jezdit
na	na	k7c6	na
sumci	sumec	k1gMnSc6	sumec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
může	moct	k5eAaImIp3nS	moct
si	se	k3xPyFc3	se
osedlat	osedlat	k5eAaPmF	osedlat
také	také	k9	také
koně	kůň	k1gMnSc4	kůň
<g/>
,	,	kIx,	,
býka	býk	k1gMnSc4	býk
či	či	k8xC	či
krávu	kráva	k1gFnSc4	kráva
které	který	k3yRgMnPc4	který
uštve	uštvat	k5eAaPmIp3nS	uštvat
k	k	k7c3	k
smrti	smrt	k1gFnSc3	smrt
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
zimě	zima	k1gFnSc6	zima
spí	spát	k5eAaImIp3nS	spát
a	a	k8xC	a
na	na	k7c6	na
jaře	jaro	k1gNnSc6	jaro
při	při	k7c6	při
svém	svůj	k3xOyFgNnSc6	svůj
probuzení	probuzení	k1gNnSc6	probuzení
láme	lámat	k5eAaImIp3nS	lámat
led	led	k1gInSc1	led
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
Rozšířené	rozšířený	k2eAgFnPc1d1	rozšířená
byly	být	k5eAaImAgFnP	být
oběti	oběť	k1gFnPc4	oběť
vodníkovi	vodník	k1gMnSc3	vodník
<g/>
,	,	kIx,	,
například	například	k6eAd1	například
medem	med	k1gInSc7	med
pomazaný	pomazaný	k2eAgMnSc1d1	pomazaný
kůň	kůň	k1gMnSc1	kůň
s	s	k7c7	s
hřívou	hříva	k1gFnSc7	hříva
ozdobenou	ozdobený	k2eAgFnSc4d1	ozdobená
stuhami	stuha	k1gFnPc7	stuha
<g/>
,	,	kIx,	,
černá	černý	k2eAgFnSc1d1	černá
svině	svině	k1gFnSc1	svině
<g/>
,	,	kIx,	,
husa	husa	k1gFnSc1	husa
<g/>
,	,	kIx,	,
první	první	k4xOgInSc1	první
roj	roj	k1gInSc1	roj
včel	včela	k1gFnPc2	včela
v	v	k7c6	v
měchu	měch	k1gInSc6	měch
nebo	nebo	k8xC	nebo
máslo	máslo	k1gNnSc1	máslo
lité	litý	k2eAgNnSc1d1	Lité
do	do	k7c2	do
vody	voda	k1gFnSc2	voda
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
Rusku	Rusko	k1gNnSc6	Rusko
existovala	existovat	k5eAaImAgFnS	existovat
víra	víra	k1gFnSc1	víra
že	že	k8xS	že
do	do	k7c2	do
základů	základ	k1gInPc2	základ
mlýna	mlýn	k1gInSc2	mlýn
má	mít	k5eAaImIp3nS	mít
být	být	k5eAaImF	být
zakopána	zakopán	k2eAgFnSc1d1	zakopána
živá	živý	k2eAgFnSc1d1	živá
bytost	bytost	k1gFnSc1	bytost
<g/>
,	,	kIx,	,
zvíře	zvíře	k1gNnSc1	zvíře
nebo	nebo	k8xC	nebo
dokonce	dokonce	k9	dokonce
i	i	k9	i
člověk	člověk	k1gMnSc1	člověk
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
se	se	k3xPyFc4	se
mlynáři	mlynář	k1gMnSc3	mlynář
dařilo	dařit	k5eAaImAgNnS	dařit
<g/>
,	,	kIx,	,
na	na	k7c6	na
Ukrajině	Ukrajina	k1gFnSc6	Ukrajina
se	se	k3xPyFc4	se
zakopávala	zakopávat	k5eAaImAgFnS	zakopávat
do	do	k7c2	do
hráze	hráz	k1gFnSc2	hráz
koňská	koňský	k2eAgFnSc1d1	koňská
hlava	hlava	k1gFnSc1	hlava
aby	aby	k9	aby
ji	on	k3xPp3gFnSc4	on
vodník	vodník	k1gMnSc1	vodník
neprotrhl	protrhnout	k5eNaPmAgMnS	protrhnout
<g/>
.	.	kIx.	.
<g/>
Vysloveně	vysloveně	k6eAd1	vysloveně
negativní	negativní	k2eAgFnSc7d1	negativní
formou	forma	k1gFnSc7	forma
vodníka	vodník	k1gMnSc2	vodník
je	být	k5eAaImIp3nS	být
bolotjanyk	bolotjanyk	k1gInSc4	bolotjanyk
<g/>
,	,	kIx,	,
obývající	obývající	k2eAgFnPc4d1	obývající
bažiny	bažina	k1gFnPc4	bažina
<g/>
,	,	kIx,	,
a	a	k8xC	a
očeretjanyk	očeretjanyk	k1gInSc1	očeretjanyk
<g/>
,	,	kIx,	,
sídlící	sídlící	k2eAgFnSc1d1	sídlící
v	v	k7c6	v
močálech	močál	k1gInPc6	močál
a	a	k8xC	a
rákosí	rákosí	k1gNnSc6	rákosí
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
===	===	k?	===
Slovinsko	Slovinsko	k1gNnSc4	Slovinsko
===	===	k?	===
</s>
</p>
<p>
<s>
Slovinský	slovinský	k2eAgMnSc1d1	slovinský
povodnji	povodnt	k5eAaPmIp1nS	povodnt
mož	mož	k?	mož
<g/>
,	,	kIx,	,
vodeni	voden	k2eAgMnPc1d1	voden
mož	mož	k?	mož
<g/>
,	,	kIx,	,
gestrin	gestrin	k1gInSc1	gestrin
či	či	k8xC	či
muk	muk	k1gInSc1	muk
se	se	k3xPyFc4	se
objevuje	objevovat	k5eAaImIp3nS	objevovat
v	v	k7c6	v
podobě	podoba	k1gFnSc6	podoba
hezkého	hezký	k2eAgMnSc2d1	hezký
mladíka	mladík	k1gMnSc2	mladík
či	či	k8xC	či
myslivce	myslivec	k1gMnSc2	myslivec
<g/>
,	,	kIx,	,
muže	muž	k1gMnSc2	muž
s	s	k7c7	s
dlouhými	dlouhý	k2eAgInPc7d1	dlouhý
kníry	knír	k1gInPc7	knír
nebo	nebo	k8xC	nebo
velkého	velký	k2eAgMnSc2d1	velký
starce	stařec	k1gMnSc2	stařec
<g/>
.	.	kIx.	.
</s>
<s>
Obléká	oblékat	k5eAaImIp3nS	oblékat
si	se	k3xPyFc3	se
zelenou	zelený	k2eAgFnSc4d1	zelená
suknici	suknice	k1gFnSc4	suknice
a	a	k8xC	a
červenou	červený	k2eAgFnSc4d1	červená
čapku	čapka	k1gFnSc4	čapka
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
jiných	jiný	k2eAgNnPc2d1	jiné
podání	podání	k1gNnPc2	podání
je	být	k5eAaImIp3nS	být
od	od	k7c2	od
pasu	pas	k1gInSc2	pas
dolů	dolů	k6eAd1	dolů
rybou	ryba	k1gFnSc7	ryba
nebo	nebo	k8xC	nebo
zelený	zelený	k2eAgInSc1d1	zelený
<g/>
,	,	kIx,	,
má	mít	k5eAaImIp3nS	mít
zelené	zelený	k2eAgInPc4d1	zelený
vlasy	vlas	k1gInPc4	vlas
a	a	k8xC	a
vousy	vous	k1gInPc4	vous
<g/>
;	;	kIx,	;
a	a	k8xC	a
je	být	k5eAaImIp3nS	být
velmi	velmi	k6eAd1	velmi
chlupatý	chlupatý	k2eAgInSc1d1	chlupatý
<g/>
.	.	kIx.	.
</s>
<s>
Své	svůj	k3xOyFgFnPc4	svůj
oběti	oběť	k1gFnPc4	oběť
láká	lákat	k5eAaImIp3nS	lákat
na	na	k7c4	na
zlatou	zlatý	k2eAgFnSc4d1	zlatá
rybu	ryba	k1gFnSc4	ryba
<g/>
,	,	kIx,	,
která	který	k3yRgFnSc1	který
mu	on	k3xPp3gMnSc3	on
také	také	k9	také
svítí	svítit	k5eAaImIp3nS	svítit
v	v	k7c6	v
hlubinách	hlubina	k1gFnPc6	hlubina
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
oblibě	obliba	k1gFnSc6	obliba
má	mít	k5eAaImIp3nS	mít
především	především	k9	především
mladé	mladý	k2eAgFnPc4d1	mladá
dívky	dívka	k1gFnPc4	dívka
<g/>
,	,	kIx,	,
které	který	k3yQgFnPc4	který
si	se	k3xPyFc3	se
bere	brát	k5eAaImIp3nS	brát
za	za	k7c4	za
ženu	žena	k1gFnSc4	žena
<g/>
.	.	kIx.	.
</s>
<s>
Pouští	pouštět	k5eAaImIp3nS	pouštět
ji	on	k3xPp3gFnSc4	on
i	i	k9	i
na	na	k7c4	na
čas	čas	k1gInSc4	čas
k	k	k7c3	k
rodičům	rodič	k1gMnPc3	rodič
<g/>
,	,	kIx,	,
v	v	k7c6	v
tom	ten	k3xDgInSc6	ten
případě	případ	k1gInSc6	případ
ji	on	k3xPp3gFnSc4	on
však	však	k9	však
uváže	uvázat	k5eAaPmIp3nS	uvázat
na	na	k7c4	na
dlouhý	dlouhý	k2eAgInSc4d1	dlouhý
řetěz	řetěz	k1gInSc4	řetěz
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Hypotézy	hypotéza	k1gFnSc2	hypotéza
==	==	k?	==
</s>
</p>
<p>
<s>
Psychiatr	psychiatr	k1gMnSc1	psychiatr
Vladimír	Vladimír	k1gMnSc1	Vladimír
Vondráček	Vondráček	k1gMnSc1	Vondráček
považoval	považovat	k5eAaImAgMnS	považovat
za	za	k7c4	za
předobraz	předobraz	k1gInSc4	předobraz
vodníka	vodník	k1gMnSc2	vodník
především	především	k6eAd1	především
sumce	sumec	k1gMnSc4	sumec
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
také	také	k9	také
žábu	žába	k1gFnSc4	žába
a	a	k8xC	a
vydru	vydra	k1gFnSc4	vydra
<g/>
.	.	kIx.	.
</s>
<s>
Představu	představa	k1gFnSc4	představa
že	že	k8xS	že
topí	topit	k5eAaImIp3nS	topit
muže	muž	k1gMnPc4	muž
plavící	plavící	k2eAgMnPc4d1	plavící
koně	kůň	k1gMnPc4	kůň
vysvětluje	vysvětlovat	k5eAaImIp3nS	vysvětlovat
faktem	fakt	k1gInSc7	fakt
že	že	k8xS	že
v	v	k7c6	v
době	doba	k1gFnSc6	doba
živé	živý	k2eAgFnSc2d1	živá
víry	víra	k1gFnSc2	víra
ve	v	k7c4	v
vodníka	vodník	k1gMnSc4	vodník
bylo	být	k5eAaImAgNnS	být
plavení	plavení	k1gNnSc1	plavení
koní	koní	k2eAgNnSc1d1	koní
nejčastější	častý	k2eAgFnSc7d3	nejčastější
možností	možnost	k1gFnSc7	možnost
jak	jak	k8xC	jak
přijít	přijít	k5eAaPmF	přijít
do	do	k7c2	do
styku	styk	k1gInSc2	styk
s	s	k7c7	s
vodou	voda	k1gFnSc7	voda
<g/>
.	.	kIx.	.
</s>
<s>
Spojení	spojení	k1gNnSc1	spojení
vodníka	vodník	k1gMnSc2	vodník
a	a	k8xC	a
koně	kůň	k1gMnSc2	kůň
odvozuje	odvozovat	k5eAaImIp3nS	odvozovat
z	z	k7c2	z
řecké	řecký	k2eAgFnSc2d1	řecká
mytologie	mytologie	k1gFnSc2	mytologie
kde	kde	k6eAd1	kde
byl	být	k5eAaImAgMnS	být
bůh	bůh	k1gMnSc1	bůh
moře	moře	k1gNnSc2	moře
Poseidón	Poseidón	k1gMnSc1	Poseidón
také	také	k9	také
bohem	bůh	k1gMnSc7	bůh
koní	kůň	k1gMnPc2	kůň
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Vodník	vodník	k1gMnSc1	vodník
v	v	k7c6	v
kultuře	kultura	k1gFnSc6	kultura
==	==	k?	==
</s>
</p>
<p>
<s>
Vodník	vodník	k1gMnSc1	vodník
patří	patřit	k5eAaImIp3nS	patřit
k	k	k7c3	k
častým	častý	k2eAgFnPc3d1	častá
postavám	postava	k1gFnPc3	postava
v	v	k7c6	v
lidových	lidový	k2eAgFnPc6d1	lidová
pověstech	pověst	k1gFnPc6	pověst
a	a	k8xC	a
pohádkách	pohádka	k1gFnPc6	pohádka
<g/>
,	,	kIx,	,
a	a	k8xC	a
dílech	díl	k1gInPc6	díl
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
vycházející	vycházející	k2eAgFnPc1d1	vycházející
populární	populární	k2eAgFnPc1d1	populární
kultury	kultura	k1gFnPc1	kultura
<g/>
.	.	kIx.	.
</s>
</p>
<p>
<s>
==	==	k?	==
Odkazy	odkaz	k1gInPc4	odkaz
==	==	k?	==
</s>
</p>
<p>
<s>
===	===	k?	===
Reference	reference	k1gFnPc1	reference
===	===	k?	===
</s>
</p>
<p>
<s>
===	===	k?	===
Externí	externí	k2eAgInPc1d1	externí
odkazy	odkaz	k1gInPc1	odkaz
===	===	k?	===
</s>
</p>
<p>
<s>
kniha	kniha	k1gFnSc1	kniha
o	o	k7c4	o
mytologii	mytologie	k1gFnSc4	mytologie
starých	starý	k2eAgInPc2d1	starý
Slovanů	Slovan	k1gInPc2	Slovan
<g/>
,	,	kIx,	,
o	o	k7c6	o
vodnících	vodník	k1gMnPc6	vodník
<g/>
,	,	kIx,	,
topilcích	topilec	k1gMnPc6	topilec
v	v	k7c6	v
kapitole	kapitola	k1gFnSc6	kapitola
2	[number]	k4	2
-	-	kIx~	-
HOSTINSKÝ	hostinský	k1gMnSc1	hostinský
<g/>
,	,	kIx,	,
Peter	Peter	k1gMnSc1	Peter
Záboj	Záboj	k1gMnSc1	Záboj
<g/>
.	.	kIx.	.
</s>
<s>
Stará	Stará	k1gFnSc1	Stará
vieronauka	vieronauk	k1gMnSc2	vieronauk
slovenská	slovenský	k2eAgNnPc1d1	slovenské
:	:	kIx,	:
Vek	veka	k1gFnPc2	veka
1	[number]	k4	1
<g/>
:	:	kIx,	:
<g/>
kniha	kniha	k1gFnSc1	kniha
1	[number]	k4	1
<g/>
.	.	kIx.	.
[	[	kIx(	[
<g/>
1	[number]	k4	1
<g/>
.	.	kIx.	.
vyd	vyd	k?	vyd
<g/>
.	.	kIx.	.
<g/>
]	]	kIx)	]
Pešť	Pešť	k1gFnSc1	Pešť
<g/>
:	:	kIx,	:
Minerva	Minerva	k1gFnSc1	Minerva
<g/>
,	,	kIx,	,
1871	[number]	k4	1871
<g/>
.	.	kIx.	.
122	[number]	k4	122
s.	s.	k?	s.
-	-	kIx~	-
dostupné	dostupný	k2eAgNnSc1d1	dostupné
online	onlinout	k5eAaPmIp3nS	onlinout
v	v	k7c6	v
Digitální	digitální	k2eAgFnSc6d1	digitální
knihovně	knihovna	k1gFnSc6	knihovna
UKB	UKB	kA	UKB
</s>
</p>
<p>
<s>
Obrázky	obrázek	k1gInPc1	obrázek
<g/>
,	,	kIx,	,
zvuky	zvuk	k1gInPc1	zvuk
či	či	k8xC	či
videa	video	k1gNnSc2	video
k	k	k7c3	k
tématu	téma	k1gNnSc3	téma
vodník	vodník	k1gMnSc1	vodník
ve	v	k7c4	v
Wikimedia	Wikimedium	k1gNnPc4	Wikimedium
Commons	Commonsa	k1gFnPc2	Commonsa
</s>
</p>
<p>
<s>
Slovníkové	slovníkový	k2eAgNnSc1d1	slovníkové
heslo	heslo	k1gNnSc1	heslo
vodník	vodník	k1gMnSc1	vodník
ve	v	k7c6	v
Wikislovníku	Wikislovník	k1gInSc6	Wikislovník
</s>
</p>
