<s>
Česká	český	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
</s>
<s>
Česká	český	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Zkratka	zkratka	k1gFnSc1
</s>
<s>
Piráti	pirát	k1gMnPc1
Datum	datum	k1gNnSc4
založení	založení	k1gNnSc2
</s>
<s>
17	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2009	#num#	k4
Předseda	předseda	k1gMnSc1
</s>
<s>
Ivan	Ivan	k1gMnSc1
Bartoš	Bartoš	k1gMnSc1
1	#num#	k4
<g/>
.	.	kIx.
místopředsedkyně	místopředsedkyně	k1gFnSc1
</s>
<s>
Olga	Olga	k1gFnSc1
Richterová	Richterová	k1gFnSc1
Zakladatel	zakladatel	k1gMnSc1
</s>
<s>
Jiří	Jiří	k1gMnSc1
Kadeřávek	Kadeřávek	k1gMnSc1
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
Sídlo	sídlo	k1gNnSc4
</s>
<s>
Na	na	k7c6
Moráni	Moráni	k?
360	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
<g/>
,	,	kIx,
128	#num#	k4
00	#num#	k4
Praha	Praha	k1gFnSc1
2	#num#	k4
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
Ideologie	ideologie	k1gFnSc1
</s>
<s>
liberalismus	liberalismus	k1gInSc1
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
progresivismus	progresivismus	k1gInSc1
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
<g/>
pirátská	pirátský	k2eAgFnSc1d1
politika	politika	k1gFnSc1
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
participativní	participativní	k2eAgFnSc2d1
demokracie	demokracie	k1gFnSc2
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
<g/>
pro-evropanismus	pro-evropanismus	k1gInSc1
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
nenalezeno	nalezen	k2eNgNnSc1d1
v	v	k7c6
uvedeném	uvedený	k2eAgInSc6d1
zdroji	zdroj	k1gInSc6
<g/>
]	]	kIx)
Politická	politický	k2eAgFnSc1d1
pozice	pozice	k1gFnSc1
</s>
<s>
střed	střed	k1gInSc1
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
až	až	k9
středolevice	středolevice	k1gFnSc1
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Stát	stát	k1gInSc1
<g/>
.	.	kIx.
příspěvek	příspěvek	k1gInSc1
(	(	kIx(
<g/>
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kč	Kč	kA
<g/>
)	)	kIx)
</s>
<s>
35,2	35,2	k4
mil	míle	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kč	Kč	kA
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Mezinárodní	mezinárodní	k2eAgInSc1d1
org	org	k?
<g/>
.	.	kIx.
</s>
<s>
Pirátská	pirátský	k2eAgFnSc1d1
internacionála	internacionála	k1gFnSc1
Evropská	evropský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Politická	politický	k2eAgFnSc1d1
skupina	skupina	k1gFnSc1
EP	EP	kA
</s>
<s>
Zelení	zelení	k1gNnSc1
/	/	kIx~
Evropská	evropský	k2eAgFnSc1d1
svobodná	svobodný	k2eAgFnSc1d1
aliance	aliance	k1gFnSc1
Mládežnická	mládežnický	k2eAgFnSc1d1
org	org	k?
<g/>
.	.	kIx.
</s>
<s>
Mladé	mladý	k2eAgNnSc1d1
Pirátstvo	Pirátstvo	k1gNnSc1
Stranické	stranický	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
</s>
<s>
Pirátské	pirátský	k2eAgInPc1d1
listy	list	k1gInPc1
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Počet	počet	k1gInSc1
členů	člen	k1gMnPc2
</s>
<s>
1148	#num#	k4
(	(	kIx(
<g/>
k	k	k7c3
1	#num#	k4
<g/>
.	.	kIx.
5	#num#	k4
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
Slogan	slogan	k1gInSc1
</s>
<s>
„	„	k?
<g/>
Internet	Internet	k1gInSc1
je	být	k5eAaImIp3nS
naše	náš	k3xOp1gNnSc4
moře	moře	k1gNnSc4
<g/>
“	“	k?
Barvy	barva	k1gFnSc2
</s>
<s>
černá	černý	k2eAgFnSc1d1
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
Volební	volební	k2eAgInSc1d1
výsledek	výsledek	k1gInSc1
</s>
<s>
13,95	13,95	k4
%	%	kIx~
(	(	kIx(
<g/>
EP	EP	kA
2019	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
IČO	IČO	kA
</s>
<s>
71339698	#num#	k4
(	(	kIx(
<g/>
PSH	PSH	kA
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.pirati.cz	www.pirati.cz	k1gInSc1
Zisk	zisk	k1gInSc1
mandátů	mandát	k1gInPc2
ve	v	k7c6
volbách	volba	k1gFnPc6
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
<g/>
2017	#num#	k4
</s>
<s>
22	#num#	k4
<g/>
/	/	kIx~
<g/>
200	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
Senát	senát	k1gInSc1
</s>
<s>
2	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
p	p	k?
1	#num#	k4
<g/>
]	]	kIx)
Evropský	evropský	k2eAgInSc1d1
parlament	parlament	k1gInSc1
<g/>
2019	#num#	k4
</s>
<s>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
21	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Zastupitelstva	zastupitelstvo	k1gNnPc1
krajů	kraj	k1gInPc2
<g/>
2020	#num#	k4
</s>
<s>
99	#num#	k4
<g/>
/	/	kIx~
<g/>
675	#num#	k4
</s>
<s>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Zastupitelstva	zastupitelstvo	k1gNnPc1
obcí	obec	k1gFnPc2
<g/>
2018	#num#	k4
</s>
<s>
358	#num#	k4
<g/>
/	/	kIx~
<g/>
62300	#num#	k4
</s>
<s>
Zastupitelstvo	zastupitelstvo	k1gNnSc1
Prahy	Praha	k1gFnSc2
<g/>
2018	#num#	k4
</s>
<s>
13	#num#	k4
<g/>
/	/	kIx~
<g/>
65	#num#	k4
</s>
<s>
Vlajka	vlajka	k1gFnSc1
strany	strana	k1gFnSc2
</s>
<s>
Česká	český	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
registrovaná	registrovaný	k2eAgFnSc1d1
zkratka	zkratka	k1gFnSc1
Piráti	pirát	k1gMnPc1
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
česká	český	k2eAgFnSc1d1
liberálně	liberálně	k6eAd1
progresivní	progresivní	k2eAgFnSc1d1
politická	politický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
založená	založený	k2eAgFnSc1d1
v	v	k7c6
roce	rok	k1gInSc6
2009	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
V	v	k7c6
parlamentních	parlamentní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
2017	#num#	k4
vstoupila	vstoupit	k5eAaPmAgFnS
ČPS	ČPS	kA
poprvé	poprvé	k6eAd1
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
se	s	k7c7
ziskem	zisk	k1gInSc7
22	#num#	k4
z	z	k7c2
200	#num#	k4
mandátů	mandát	k1gInPc2
stala	stát	k5eAaPmAgFnS
třetí	třetí	k4xOgFnSc7
nejsilnější	silný	k2eAgFnSc7d3
stranou	strana	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pod	pod	k7c7
vedením	vedení	k1gNnSc7
předsedy	předseda	k1gMnSc2
Ivana	Ivan	k1gMnSc2
Bartoše	Bartoš	k1gMnSc2
je	být	k5eAaImIp3nS
součástí	součást	k1gFnSc7
opozice	opozice	k1gFnSc2
vůči	vůči	k7c3
menšinové	menšinový	k2eAgFnSc3d1
koaliční	koaliční	k2eAgFnSc3d1
vládě	vláda	k1gFnSc3
Andreje	Andrej	k1gMnSc2
Babiše	Babiš	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strana	strana	k1gFnSc1
nominovala	nominovat	k5eAaBmAgFnS
také	také	k9
pět	pět	k4xCc4
aktivních	aktivní	k2eAgMnPc2d1
senátorů	senátor	k1gMnPc2
<g/>
,	,	kIx,
naposledy	naposledy	k6eAd1
Adélu	Adéla	k1gFnSc4
Šípovou	šípový	k2eAgFnSc4d1
a	a	k8xC
Davida	David	k1gMnSc2
Smoljaka	Smoljak	k1gMnSc2
v	v	k7c6
roce	rok	k1gInSc6
2020	#num#	k4
a	a	k8xC
Lukáše	Lukáš	k1gMnSc4
Wagenknechta	Wagenknecht	k1gMnSc4
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
komunálních	komunální	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
uspěli	uspět	k5eAaPmAgMnP
Piráti	pirát	k1gMnPc1
v	v	k7c6
řadě	řada	k1gFnSc6
obcí	obec	k1gFnPc2
<g/>
,	,	kIx,
včetně	včetně	k7c2
největších	veliký	k2eAgNnPc2d3
měst	město	k1gNnPc2
<g/>
;	;	kIx,
v	v	k7c6
pražském	pražský	k2eAgNnSc6d1
zastupitelstvu	zastupitelstvo	k1gNnSc6
vytvořili	vytvořit	k5eAaPmAgMnP
vládnoucí	vládnoucí	k2eAgFnSc3d1
koalici	koalice	k1gFnSc3
a	a	k8xC
volební	volební	k2eAgMnSc1d1
lídr	lídr	k1gMnSc1
Zdeněk	Zdeněk	k1gMnSc1
Hřib	hřib	k1gInSc4
obsadil	obsadit	k5eAaPmAgMnS
post	post	k1gInSc4
primátora	primátor	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pirátští	pirátský	k2eAgMnPc1d1
zastupitelé	zastupitel	k1gMnPc1
také	také	k9
vstoupili	vstoupit	k5eAaPmAgMnP
do	do	k7c2
vládnoucích	vládnoucí	k2eAgFnPc2d1
koalic	koalice	k1gFnPc2
v	v	k7c6
městských	městský	k2eAgFnPc6d1
radách	rada	k1gFnPc6
Brna	Brno	k1gNnSc2
a	a	k8xC
Ostravy	Ostrava	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
2019	#num#	k4
získala	získat	k5eAaPmAgFnS
strana	strana	k1gFnSc1
tři	tři	k4xCgInPc4
mandáty	mandát	k1gInPc4
v	v	k7c6
Evropském	evropský	k2eAgInSc6d1
parlamentu	parlament	k1gInSc6
(	(	kIx(
<g/>
EP	EP	kA
<g/>
)	)	kIx)
a	a	k8xC
Marcel	Marcel	k1gMnSc1
Kolaja	Kolaja	k1gMnSc1
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
jeho	jeho	k3xOp3gMnSc7
místopředsedou	místopředseda	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
20	#num#	k4
<g/>
]	]	kIx)
Poslanci	poslanec	k1gMnPc1
Pirátů	pirát	k1gMnPc2
jsou	být	k5eAaImIp3nP
v	v	k7c6
EP	EP	kA
členy	člen	k1gMnPc7
skupiny	skupina	k1gFnSc2
Zelení	zelení	k1gNnSc1
/	/	kIx~
Evropská	evropský	k2eAgFnSc1d1
svobodná	svobodný	k2eAgFnSc1d1
aliance	aliance	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
zastupitelstev	zastupitelstvo	k1gNnPc2
krajů	kraj	k1gInPc2
v	v	k7c6
říjnu	říjen	k1gInSc6
2020	#num#	k4
získali	získat	k5eAaPmAgMnP
Piráti	pirát	k1gMnPc1
99	#num#	k4
ze	z	k7c2
675	#num#	k4
krajských	krajský	k2eAgNnPc2d1
zastupitelských	zastupitelský	k2eAgNnPc2d1
křesel	křeslo	k1gNnPc2
a	a	k8xC
následně	následně	k6eAd1
vytvořili	vytvořit	k5eAaPmAgMnP
vládnoucí	vládnoucí	k2eAgFnPc4d1
koalice	koalice	k1gFnPc4
s	s	k7c7
jinými	jiný	k2eAgFnPc7d1
stranami	strana	k1gFnPc7
v	v	k7c6
devíti	devět	k4xCc2
ze	z	k7c2
třinácti	třináct	k4xCc2
krajů	kraj	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Program	program	k1gInSc1
strany	strana	k1gFnSc2
se	se	k3xPyFc4
zaměřuje	zaměřovat	k5eAaImIp3nS
na	na	k7c4
politickou	politický	k2eAgFnSc4d1
transparentnost	transparentnost	k1gFnSc4
<g/>
,	,	kIx,
osobní	osobní	k2eAgFnSc4d1
odpovědnost	odpovědnost	k1gFnSc4
politiků	politik	k1gMnPc2
<g/>
,	,	kIx,
boj	boj	k1gInSc1
proti	proti	k7c3
korupci	korupce	k1gFnSc3
<g/>
,	,	kIx,
e-Government	e-Government	k1gInSc4
<g/>
,	,	kIx,
podporu	podpora	k1gFnSc4
malých	malý	k2eAgInPc2d1
a	a	k8xC
středních	střední	k2eAgInPc2d1
podniků	podnik	k1gInPc2
<g/>
,	,	kIx,
předcházení	předcházení	k1gNnSc2
daňovým	daňový	k2eAgInPc3d1
únikům	únik	k1gInPc3
a	a	k8xC
odlivu	odliv	k1gInSc3
kapitálu	kapitál	k1gInSc2
z	z	k7c2
Česka	Česko	k1gNnSc2
skrze	skrze	k?
firmy	firma	k1gFnSc2
zahraničních	zahraniční	k2eAgMnPc2d1
vlastníků	vlastník	k1gMnPc2
<g/>
,	,	kIx,
financování	financování	k1gNnSc3
místního	místní	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
<g/>
,	,	kIx,
účast	účast	k1gFnSc1
veřejnosti	veřejnost	k1gFnSc2
na	na	k7c6
demokratickém	demokratický	k2eAgNnSc6d1
rozhodování	rozhodování	k1gNnSc6
(	(	kIx(
<g/>
participativní	participativní	k2eAgFnSc4d1
demokracii	demokracie	k1gFnSc4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
svého	svůj	k3xOyFgInSc2
programu	program	k1gInSc2
klade	klást	k5eAaImIp3nS
strana	strana	k1gFnSc1
důraz	důraz	k1gInSc4
na	na	k7c4
zachování	zachování	k1gNnSc4
právního	právní	k2eAgInSc2d1
státu	stát	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strana	strana	k1gFnSc1
má	mít	k5eAaImIp3nS
dále	daleko	k6eAd2
za	za	k7c4
cíl	cíl	k1gInSc4
reformovat	reformovat	k5eAaBmF
zákony	zákon	k1gInPc4
v	v	k7c6
oblastech	oblast	k1gFnPc6
finančních	finanční	k2eAgInPc2d1
trhů	trh	k1gInPc2
a	a	k8xC
bankovnictví	bankovnictví	k1gNnSc2
<g/>
,	,	kIx,
lobbování	lobbování	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Piráti	pirát	k1gMnPc1
jsou	být	k5eAaImIp3nP
také	také	k9
proevropská	proevropský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
usiluje	usilovat	k5eAaImIp3nS
o	o	k7c4
řešení	řešení	k1gNnSc4
vnímaného	vnímaný	k2eAgInSc2d1
demokratického	demokratický	k2eAgInSc2d1
deficitu	deficit	k1gInSc2
v	v	k7c6
Evropské	evropský	k2eAgFnSc6d1
unii	unie	k1gFnSc6
prosazením	prosazení	k1gNnSc7
principu	princip	k1gInSc2
subsidiarity	subsidiarita	k1gFnSc2
<g/>
,	,	kIx,
o	o	k7c4
aktivní	aktivní	k2eAgNnSc4d1
zapojení	zapojení	k1gNnSc4
Česka	Česko	k1gNnSc2
v	v	k7c6
evropském	evropský	k2eAgNnSc6d1
rozhodování	rozhodování	k1gNnSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
reformu	reforma	k1gFnSc4
zdanění	zdanění	k1gNnSc2
nadnárodních	nadnárodní	k2eAgFnPc2d1
korporací	korporace	k1gFnPc2
a	a	k8xC
o	o	k7c4
ochranu	ochrana	k1gFnSc4
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mezi	mezi	k7c4
klíčové	klíčový	k2eAgFnPc4d1
hodnoty	hodnota	k1gFnPc4
strany	strana	k1gFnSc2
patří	patřit	k5eAaImIp3nS
demokracie	demokracie	k1gFnSc1
se	s	k7c7
zapojením	zapojení	k1gNnSc7
občanů	občan	k1gMnPc2
<g/>
,	,	kIx,
dobré	dobrý	k2eAgNnSc1d1
řízení	řízení	k1gNnSc1
státu	stát	k1gInSc2
<g/>
,	,	kIx,
ochrana	ochrana	k1gFnSc1
občanských	občanský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
a	a	k8xC
podnikání	podnikání	k1gNnSc2
před	před	k7c7
utlačováním	utlačování	k1gNnSc7
státní	státní	k2eAgMnPc1d1
byrokracií	byrokracie	k1gFnSc7
a	a	k8xC
záštita	záštita	k1gFnSc1
občanů	občan	k1gMnPc2
před	před	k7c7
koncentrovanou	koncentrovaný	k2eAgFnSc7d1
ekonomickou	ekonomický	k2eAgFnSc7d1
mocí	moc	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
Vnitřní	vnitřní	k2eAgFnSc1d1
organizace	organizace	k1gFnSc1
strany	strana	k1gFnSc2
funguje	fungovat	k5eAaImIp3nS
na	na	k7c6
principu	princip	k1gInSc6
přímé	přímý	k2eAgFnSc2d1
demokracie	demokracie	k1gFnSc2
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stranická	stranický	k2eAgFnSc1d1
diskuse	diskuse	k1gFnSc1
je	být	k5eAaImIp3nS
vedena	vést	k5eAaImNgFnS
na	na	k7c6
otevřeném	otevřený	k2eAgInSc6d1
online	onlinout	k5eAaPmIp3nS
fóru	fór	k1gInSc3
a	a	k8xC
ke	k	k7c3
stranickému	stranický	k2eAgNnSc3d1
rozhodování	rozhodování	k1gNnSc3
dochází	docházet	k5eAaImIp3nS
skrze	skrze	k?
online	onlinout	k5eAaPmIp3nS
volby	volba	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
Piráti	pirát	k1gMnPc1
používají	používat	k5eAaImIp3nP
veřejně	veřejně	k6eAd1
transparentní	transparentní	k2eAgInSc4d1
bankovní	bankovní	k2eAgInSc4d1
účet	účet	k1gInSc4
pro	pro	k7c4
stranické	stranický	k2eAgNnSc4d1
účetnictví	účetnictví	k1gNnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
zdroj	zdroj	k1gInSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
]	]	kIx)
</s>
<s>
Politické	politický	k2eAgFnPc1d1
pozice	pozice	k1gFnPc1
</s>
<s>
Politické	politický	k2eAgNnSc1d1
spektrum	spektrum	k1gNnSc1
a	a	k8xC
ideologie	ideologie	k1gFnSc1
</s>
<s>
Piráti	pirát	k1gMnPc1
jsou	být	k5eAaImIp3nP
středová	středový	k2eAgNnPc4d1
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
až	až	k6eAd1
středolevicová	středolevicový	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
Jejich	jejich	k3xOp3gFnSc1
hlavní	hlavní	k2eAgFnSc1d1
ideologická	ideologický	k2eAgFnSc1d1
platforma	platforma	k1gFnSc1
je	být	k5eAaImIp3nS
v	v	k7c6
oblasti	oblast	k1gFnSc6
liberalismu	liberalismus	k1gInSc2
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
progresivismu	progresivismus	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
Voliči	volič	k1gMnPc1
<g />
.	.	kIx.
</s>
<s hack="1">
strany	strana	k1gFnPc1
jsou	být	k5eAaImIp3nP
dle	dle	k7c2
studie	studie	k1gFnSc2
z	z	k7c2
roku	rok	k1gInSc2
2016	#num#	k4
rozmístění	rozmístění	k1gNnSc2
na	na	k7c6
obou	dva	k4xCgFnPc6
stranách	strana	k1gFnPc6
politického	politický	k2eAgNnSc2d1
spektra	spektrum	k1gNnSc2
<g/>
,	,	kIx,
pravice	pravice	k1gFnSc2
i	i	k8xC
levice	levice	k1gFnSc2
<g/>
[	[	kIx(
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
25	#num#	k4
<g/>
]	]	kIx)
Politolog	politolog	k1gMnSc1
Lukáš	Lukáš	k1gMnSc1
Jelínek	Jelínek	k1gMnSc1
v	v	k7c6
prosinci	prosinec	k1gInSc6
2020	#num#	k4
pro	pro	k7c4
iROZHLAS	iROZHLAS	k?
popsal	popsat	k5eAaPmAgMnS
politiku	politika	k1gFnSc4
Pirátské	pirátský	k2eAgFnSc2d1
strany	strana	k1gFnSc2
jako	jako	k8xS,k8xC
výrazně	výrazně	k6eAd1
sociální	sociální	k2eAgInPc1d1
a	a	k8xC
její	její	k3xOp3gInPc1
postoje	postoj	k1gInPc1
k	k	k7c3
ekonomice	ekonomika	k1gFnSc3
<g />
.	.	kIx.
</s>
<s hack="1">
a	a	k8xC
daním	danit	k5eAaImIp1nS
jako	jako	k9
spíše	spíše	k9
pravicové	pravicový	k2eAgFnPc1d1
<g/>
,	,	kIx,
z	z	k7c2
čehož	což	k3yRnSc2,k3yQnSc2
vyvozoval	vyvozovat	k5eAaImAgMnS
<g/>
,	,	kIx,
že	že	k8xS
strana	strana	k1gFnSc1
je	být	k5eAaImIp3nS
středové	středový	k2eAgInPc4d1
a	a	k8xC
liberálně	liberálně	k6eAd1
vyprofilované	vyprofilovaný	k2eAgNnSc1d1
uskupení	uskupení	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
26	#num#	k4
<g/>
]	]	kIx)
Mezi	mezi	k7c4
klíčové	klíčový	k2eAgFnPc4d1
hodnoty	hodnota	k1gFnPc4
strany	strana	k1gFnSc2
patří	patřit	k5eAaImIp3nS
demokracie	demokracie	k1gFnSc1
se	s	k7c7
zapojením	zapojení	k1gNnSc7
občanů	občan	k1gMnPc2
<g/>
,	,	kIx,
dobré	dobrý	k2eAgNnSc1d1
řízení	řízení	k1gNnSc1
státu	stát	k1gInSc2
<g/>
,	,	kIx,
ochrana	ochrana	k1gFnSc1
občanských	občanský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
a	a	k8xC
podnikání	podnikání	k1gNnSc2
před	před	k7c7
utlačováním	utlačování	k1gNnSc7
státní	státní	k2eAgFnSc2d1
byrokracií	byrokracie	k1gFnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
23	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Domácí	domácí	k2eAgFnSc1d1
politika	politika	k1gFnSc1
</s>
<s>
Program	program	k1gInSc1
strany	strana	k1gFnSc2
se	se	k3xPyFc4
zaměřuje	zaměřovat	k5eAaImIp3nS
na	na	k7c4
kontrolu	kontrola	k1gFnSc4
politické	politický	k2eAgFnSc2d1
moci	moc	k1gFnSc2
a	a	k8xC
státních	státní	k2eAgInPc2d1
výdajů	výdaj	k1gInPc2
skrze	skrze	k?
politickou	politický	k2eAgFnSc4d1
transparentnost	transparentnost	k1gFnSc4
<g/>
,	,	kIx,
zmenšení	zmenšení	k1gNnSc4
státní	státní	k2eAgFnSc2d1
byrokracie	byrokracie	k1gFnSc2
skrze	skrze	k?
e-Government	e-Government	k1gInSc1
<g/>
,	,	kIx,
osobní	osobní	k2eAgFnSc1d1
odpovědnost	odpovědnost	k1gFnSc1
politiků	politik	k1gMnPc2
<g/>
,	,	kIx,
boj	boj	k1gInSc1
proti	proti	k7c3
korupci	korupce	k1gFnSc3
a	a	k8xC
korporátnímu	korporátní	k2eAgNnSc3d1
lobbování	lobbování	k1gNnSc3
<g/>
,	,	kIx,
prevence	prevence	k1gFnSc1
proti	proti	k7c3
obcházení	obcházení	k1gNnSc3
daně	daň	k1gFnSc2
zahraničně	zahraničně	k6eAd1
vlastněnými	vlastněný	k2eAgFnPc7d1
firmami	firma	k1gFnPc7
<g/>
,	,	kIx,
podporu	podpora	k1gFnSc4
malých	malý	k2eAgInPc2d1
a	a	k8xC
středních	střední	k2eAgInPc2d1
podniků	podnik	k1gInPc2
<g/>
,	,	kIx,
financování	financování	k1gNnSc3
místního	místní	k2eAgInSc2d1
rozvoje	rozvoj	k1gInSc2
<g/>
,	,	kIx,
účast	účast	k1gFnSc1
veřejnosti	veřejnost	k1gFnSc2
na	na	k7c6
demokratickém	demokratický	k2eAgNnSc6d1
rozhodování	rozhodování	k1gNnSc6
<g/>
,	,	kIx,
ochranu	ochrana	k1gFnSc4
občanských	občanský	k2eAgFnPc2d1
svobod	svoboda	k1gFnPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
21	#num#	k4
<g/>
]	]	kIx)
důraz	důraz	k1gInSc1
na	na	k7c4
právní	právní	k2eAgInSc4d1
stát	stát	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pirátský	pirátský	k2eAgInSc1d1
program	program	k1gInSc1
má	mít	k5eAaImIp3nS
za	za	k7c4
cíl	cíl	k1gInSc4
usnadnit	usnadnit	k5eAaPmF
podnikání	podnikání	k1gNnSc4
ze	z	k7c2
strany	strana	k1gFnSc2
státu	stát	k1gInSc2
a	a	k8xC
úřadů	úřad	k1gInPc2
a	a	k8xC
zároveň	zároveň	k6eAd1
zabránit	zabránit	k5eAaPmF
narušování	narušování	k1gNnSc4
hospodářské	hospodářský	k2eAgFnSc2d1
soutěže	soutěž	k1gFnSc2
jako	jako	k8xS,k8xC
např.	např.	kA
zneužívání	zneužívání	k1gNnSc4
dominantního	dominantní	k2eAgNnSc2d1
postavení	postavení	k1gNnSc2
a	a	k8xC
ovlivňování	ovlivňování	k1gNnSc2
veřejné	veřejný	k2eAgFnSc2d1
moci	moc	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
28	#num#	k4
<g/>
]	]	kIx)
Dále	daleko	k6eAd2
se	se	k3xPyFc4
program	program	k1gInSc1
zasazuje	zasazovat	k5eAaImIp3nS
pro	pro	k7c4
zdanění	zdanění	k1gNnSc4
finančního	finanční	k2eAgInSc2d1
sektoru	sektor	k1gInSc2
a	a	k8xC
finančních	finanční	k2eAgFnPc2d1
institucí	instituce	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgInPc1,k3yIgInPc1,k3yQgInPc1
získaly	získat	k5eAaPmAgInP
značný	značný	k2eAgInSc4d1
<g />
.	.	kIx.
</s>
<s hack="1">
nedemokratický	demokratický	k2eNgInSc1d1
vliv	vliv	k1gInSc1
na	na	k7c4
ekonomiky	ekonomika	k1gFnPc4
<g/>
,	,	kIx,
státy	stát	k1gInPc4
a	a	k8xC
vlády	vláda	k1gFnSc2
skrze	skrze	k?
koncentraci	koncentrace	k1gFnSc3
kapitálu	kapitál	k1gInSc2
a	a	k8xC
globalizaci	globalizace	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
29	#num#	k4
<g/>
]	]	kIx)
Piráti	pirát	k1gMnPc1
chtějí	chtít	k5eAaImIp3nP
omezit	omezit	k5eAaPmF
a	a	k8xC
zdanit	zdanit	k5eAaPmF
vyvádění	vyvádění	k1gNnSc4
zisků	zisk	k1gInPc2
vytvořených	vytvořený	k2eAgInPc2d1
v	v	k7c6
Česku	Česko	k1gNnSc6
zahraničními	zahraniční	k2eAgFnPc7d1
firmami	firma	k1gFnPc7
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
dosahují	dosahovat	k5eAaImIp3nP
až	až	k6eAd1
700	#num#	k4
miliard	miliarda	k4xCgFnPc2
korun	koruna	k1gFnPc2
převedených	převedený	k2eAgFnPc2d1
do	do	k7c2
zahraničí	zahraničí	k1gNnSc2
ročně	ročně	k6eAd1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
31	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
programu	program	k1gInSc2
strany	strana	k1gFnSc2
„	„	k?
<g/>
Zisk	zisk	k1gInSc1
vytvořený	vytvořený	k2eAgInSc1d1
v	v	k7c6
Česku	Česko	k1gNnSc6
bude	být	k5eAaImBp3nS
u	u	k7c2
nás	my	k3xPp1nPc2
i	i	k9
daněn	danit	k5eAaImNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zlepšíme	zlepšit	k5eAaPmIp1nP
pravidla	pravidlo	k1gNnPc4
<g/>
,	,	kIx,
dohled	dohled	k1gInSc4
a	a	k8xC
sankce	sankce	k1gFnPc4
v	v	k7c6
případě	případ	k1gInSc6
vyvádění	vyvádění	k1gNnSc2
zisků	zisk	k1gInPc2
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
32	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pirátský	pirátský	k2eAgInSc1d1
program	program	k1gInSc1
vidí	vidět	k5eAaImIp3nS
kvalitu	kvalita	k1gFnSc4
a	a	k8xC
výsledky	výsledek	k1gInPc1
českého	český	k2eAgNnSc2d1
školství	školství	k1gNnSc2
v	v	k7c6
mezinárodním	mezinárodní	k2eAgNnSc6d1
měřítku	měřítko	k1gNnSc6
a	a	k8xC
bezplatnou	bezplatný	k2eAgFnSc4d1
a	a	k8xC
široce	široko	k6eAd1
přístupnou	přístupný	k2eAgFnSc4d1
vzdělanost	vzdělanost	k1gFnSc4
jako	jako	k8xS,k8xC
klíčové	klíčový	k2eAgInPc4d1
předpoklady	předpoklad	k1gInPc4
pro	pro	k7c4
vysoce	vysoce	k6eAd1
výkonnou	výkonný	k2eAgFnSc4d1
rozvinutou	rozvinutý	k2eAgFnSc4d1
znalostní	znalostní	k2eAgFnSc4d1
ekonomiku	ekonomika	k1gFnSc4
a	a	k8xC
informační	informační	k2eAgFnSc4d1
společnost	společnost	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Plošně	plošně	k6eAd1
vysoké	vysoký	k2eAgInPc1d1
platy	plat	k1gInPc1
pro	pro	k7c4
pedagogy	pedagog	k1gMnPc4
na	na	k7c6
všech	všecek	k3xTgInPc6
stupních	stupeň	k1gInPc6
škol	škola	k1gFnPc2
a	a	k8xC
stabilní	stabilní	k2eAgFnSc1d1
institucionální	institucionální	k2eAgFnSc1d1
podpora	podpora	k1gFnSc1
vědy	věda	k1gFnSc2
a	a	k8xC
výzkumu	výzkum	k1gInSc2
jsou	být	k5eAaImIp3nP
hlavní	hlavní	k2eAgInPc1d1
body	bod	k1gInPc1
programu	program	k1gInSc2
pro	pro	k7c4
školství	školství	k1gNnSc4
a	a	k8xC
vědu	věda	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
33	#num#	k4
<g/>
]	]	kIx)
Strana	strana	k1gFnSc1
usiluje	usilovat	k5eAaImIp3nS
o	o	k7c4
podporu	podpora	k1gFnSc4
vědeckých	vědecký	k2eAgNnPc2d1
pracovišť	pracoviště	k1gNnPc2
také	také	k9
rozšířením	rozšíření	k1gNnSc7
otevřeného	otevřený	k2eAgInSc2d1
přístupu	přístup	k1gInSc2
k	k	k7c3
výsledkům	výsledek	k1gInPc3
základního	základní	k2eAgInSc2d1
výzkumu	výzkum	k1gInSc2
na	na	k7c6
veřejných	veřejný	k2eAgFnPc6d1
institucích	instituce	k1gFnPc6
(	(	kIx(
<g/>
i	i	k9
na	na	k7c6
úrovni	úroveň	k1gFnSc6
EU	EU	kA
<g/>
)	)	kIx)
a	a	k8xC
o	o	k7c4
omezení	omezení	k1gNnSc4
patentového	patentový	k2eAgInSc2d1
systému	systém	k1gInSc2
v	v	k7c6
aplikovaném	aplikovaný	k2eAgInSc6d1
výzkumu	výzkum	k1gInSc6
v	v	k7c6
podnicích	podnik	k1gInPc6
<g/>
,	,	kIx,
jenž	jenž	k3xRgInSc1
dle	dle	k7c2
Pirátů	pirát	k1gMnPc2
častěji	často	k6eAd2
omezuje	omezovat	k5eAaImIp3nS
inovaci	inovace	k1gFnSc4
i	i	k8xC
konkurenci	konkurence	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stejně	stejně	k6eAd1
jako	jako	k8xS,k8xC
u	u	k7c2
ostatních	ostatní	k2eAgFnPc2d1
veřejných	veřejný	k2eAgFnPc2d1
a	a	k8xC
státních	státní	k2eAgFnPc2d1
institucí	instituce	k1gFnPc2
<g/>
,	,	kIx,
i	i	k9
u	u	k7c2
škol	škola	k1gFnPc2
usiluje	usilovat	k5eAaImIp3nS
program	program	k1gInSc1
o	o	k7c4
systémovou	systémový	k2eAgFnSc4d1
demokratizaci	demokratizace	k1gFnSc4
a	a	k8xC
zavedení	zavedení	k1gNnSc4
transparentní	transparentní	k2eAgFnSc2d1
organizace	organizace	k1gFnSc2
pro	pro	k7c4
zlepšení	zlepšení	k1gNnSc4
jejich	jejich	k3xOp3gNnSc2
fungování	fungování	k1gNnSc2
a	a	k8xC
výsledků	výsledek	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
34	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zemědělská	zemědělský	k2eAgFnSc1d1
politika	politika	k1gFnSc1
strany	strana	k1gFnSc2
navrhuje	navrhovat	k5eAaImIp3nS
podporu	podpora	k1gFnSc4
malým	malá	k1gFnPc3
a	a	k8xC
začínajícím	začínající	k2eAgMnPc3d1
zemědělcům	zemědělec	k1gMnPc3
namísto	namísto	k7c2
dotování	dotování	k1gNnSc2
velkých	velký	k2eAgInPc2d1
zemědělských	zemědělský	k2eAgInPc2d1
podniků	podnik	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Program	program	k1gInSc1
podporuje	podporovat	k5eAaImIp3nS
biologicky	biologicky	k6eAd1
rozmanité	rozmanitý	k2eAgFnPc1d1
plodiny	plodina	k1gFnPc1
<g/>
,	,	kIx,
lesní	lesní	k2eAgNnSc1d1
hospodářství	hospodářství	k1gNnSc1
a	a	k8xC
hospodaření	hospodaření	k1gNnSc1
s	s	k7c7
půdou	půda	k1gFnSc7
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
se	se	k3xPyFc4
zaměřují	zaměřovat	k5eAaImIp3nP
na	na	k7c4
řešení	řešení	k1gNnSc4
ekologických	ekologický	k2eAgInPc2d1
dopadů	dopad	k1gInPc2
intenzivního	intenzivní	k2eAgNnSc2d1
zemědělství	zemědělství	k1gNnSc2
a	a	k8xC
navrhuje	navrhovat	k5eAaImIp3nS
odklon	odklon	k1gInSc1
od	od	k7c2
dotování	dotování	k1gNnSc2
monokulturních	monokulturní	k2eAgFnPc2d1
plodin	plodina	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yQgFnPc1,k3yIgFnPc1
způsobují	způsobovat	k5eAaImIp3nP
degradaci	degradace	k1gFnSc4
půdy	půda	k1gFnSc2
a	a	k8xC
krajiny	krajina	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Piráti	pirát	k1gMnPc1
rovněž	rovněž	k9
navrhují	navrhovat	k5eAaImIp3nP
zjednodušit	zjednodušit	k5eAaPmF
výrobní	výrobní	k2eAgInSc4d1
a	a	k8xC
spotřebitelský	spotřebitelský	k2eAgInSc4d1
řetězec	řetězec	k1gInSc4
podporou	podpora	k1gFnSc7
infrastruktury	infrastruktura	k1gFnSc2
pro	pro	k7c4
prodej	prodej	k1gInSc4
místních	místní	k2eAgInPc2d1
a	a	k8xC
sezónních	sezónní	k2eAgInPc2d1
produktů	produkt	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
35	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Program	program	k1gInSc1
strany	strana	k1gFnSc2
má	mít	k5eAaImIp3nS
environmentální	environmentální	k2eAgFnSc4d1
platformu	platforma	k1gFnSc4
s	s	k7c7
názvem	název	k1gInSc7
„	„	k?
<g/>
ekologie	ekologie	k1gFnSc2
bez	bez	k7c2
ideologie	ideologie	k1gFnSc2
<g/>
“	“	k?
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
36	#num#	k4
<g/>
]	]	kIx)
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
se	se	k3xPyFc4
zaměřuje	zaměřovat	k5eAaImIp3nS
na	na	k7c4
odstranění	odstranění	k1gNnSc4
rozsáhlých	rozsáhlý	k2eAgFnPc2d1
státních	státní	k2eAgFnPc2d1
dotací	dotace	k1gFnPc2
<g/>
,	,	kIx,
které	který	k3yRgFnPc1,k3yIgFnPc1,k3yQgFnPc1
podporují	podporovat	k5eAaImIp3nP
fosilní	fosilní	k2eAgNnPc4d1
paliva	palivo	k1gNnPc4
(	(	kIx(
<g/>
uhlí	uhlí	k1gNnSc2
<g/>
,	,	kIx,
ropa	ropa	k1gFnSc1
apod.	apod.	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
zastavení	zastavení	k1gNnSc3
nevýhodného	výhodný	k2eNgNnSc2d1
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
pod	pod	k7c7
tržní	tržní	k2eAgFnSc7d1
cenou	cena	k1gFnSc7
<g/>
)	)	kIx)
prodeje	prodej	k1gInSc2
českých	český	k2eAgFnPc2d1
nerostných	nerostný	k2eAgFnPc2d1
surovin	surovina	k1gFnPc2
těžařským	těžařský	k2eAgFnPc3d1
společnostem	společnost	k1gFnPc3
<g/>
,	,	kIx,
dále	daleko	k6eAd2
na	na	k7c4
využití	využití	k1gNnSc4
alternativních	alternativní	k2eAgInPc2d1
zdrojů	zdroj	k1gInPc2
energie	energie	k1gFnSc2
(	(	kIx(
<g/>
tj.	tj.	kA
obnovitelné	obnovitelný	k2eAgInPc1d1
a	a	k8xC
jaderné	jaderný	k2eAgInPc1d1
<g/>
)	)	kIx)
na	na	k7c6
základě	základ	k1gInSc6
objektivního	objektivní	k2eAgInSc2d1
vědeckého	vědecký	k2eAgInSc2d1
výzkumu	výzkum	k1gInSc2
a	a	k8xC
vývoje	vývoj	k1gInSc2
<g/>
,	,	kIx,
dále	daleko	k6eAd2
na	na	k7c4
udržitelné	udržitelný	k2eAgNnSc4d1
nakládání	nakládání	k1gNnSc4
s	s	k7c7
materiály	materiál	k1gInPc7
od	od	k7c2
výroby	výroba	k1gFnSc2
produktů	produkt	k1gInPc2
až	až	k9
po	po	k7c4
recyklace	recyklace	k1gFnPc4
a	a	k8xC
odpady	odpad	k1gInPc4
<g/>
,	,	kIx,
na	na	k7c4
udržitelnou	udržitelný	k2eAgFnSc4d1
dopravu	doprava	k1gFnSc4
s	s	k7c7
upřednostňováním	upřednostňování	k1gNnSc7
veřejné	veřejný	k2eAgFnSc2d1
dopravy	doprava	k1gFnSc2
a	a	k8xC
udržitelné	udržitelný	k2eAgNnSc1d1
územní	územní	k2eAgNnSc1d1
plánování	plánování	k1gNnSc1
a	a	k8xC
rozvoj	rozvoj	k1gInSc1
měst	město	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
37	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
vede	vést	k5eAaImIp3nS
transparentní	transparentní	k2eAgInSc4d1
účet	účet	k1gInSc4
<g/>
[	[	kIx(
<g/>
38	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
účetnictví	účetnictví	k1gNnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
39	#num#	k4
<g/>
]	]	kIx)
používá	používat	k5eAaImIp3nS
svobodné	svobodný	k2eAgFnPc4d1
licence	licence	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
40	#num#	k4
<g/>
]	]	kIx)
Strana	strana	k1gFnSc1
funguje	fungovat	k5eAaImIp3nS
na	na	k7c6
principu	princip	k1gInSc6
přímé	přímý	k2eAgFnSc2d1
demokracie	demokracie	k1gFnSc2
a	a	k8xC
pro	pro	k7c4
vnitrostranická	vnitrostranický	k2eAgNnPc4d1
rozhodnutí	rozhodnutí	k1gNnPc4
používá	používat	k5eAaImIp3nS
on-line	on-lin	k1gInSc5
hlasování	hlasování	k1gNnSc6
<g/>
[	[	kIx(
<g/>
41	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
pro	pro	k7c4
přímou	přímý	k2eAgFnSc4d1
volbu	volba	k1gFnSc4
<g/>
,	,	kIx,
odvolání	odvolání	k1gNnSc2
a	a	k8xC
referenda	referendum	k1gNnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
42	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
únoru	únor	k1gInSc6
2011	#num#	k4
schválil	schválit	k5eAaPmAgInS
republikový	republikový	k2eAgInSc1d1
výbor	výbor	k1gInSc1
strany	strana	k1gFnSc2
základní	základní	k2eAgInSc4d1
programový	programový	k2eAgInSc4d1
dokument	dokument	k1gInSc4
Pirátské	pirátský	k2eAgNnSc1d1
dvanáctero	dvanáctero	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
43	#num#	k4
<g/>
]	]	kIx)
Piráti	pirát	k1gMnPc1
podporují	podporovat	k5eAaImIp3nP
LGBT	LGBT	kA
práva	právo	k1gNnPc4
v	v	k7c6
Česku	Česko	k1gNnSc6
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
44	#num#	k4
<g/>
]	]	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
ochranu	ochrana	k1gFnSc4
soukromí	soukromí	k1gNnSc3
<g/>
,	,	kIx,
legalizaci	legalizace	k1gFnSc3
konopí	konopí	k1gNnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
27	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
staví	stavit	k5eAaPmIp3nS,k5eAaBmIp3nS,k5eAaImIp3nS
se	se	k3xPyFc4
proti	proti	k7c3
masovému	masový	k2eAgNnSc3d1
sledování	sledování	k1gNnSc3
obyvatel	obyvatel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
45	#num#	k4
<g/>
]	]	kIx)
Strana	strana	k1gFnSc1
nesouhlasí	souhlasit	k5eNaImIp3nS
s	s	k7c7
používáním	používání	k1gNnSc7
výrazu	výraz	k1gInSc2
„	„	k?
<g/>
autorské	autorský	k2eAgNnSc4d1
právo	právo	k1gNnSc4
<g/>
“	“	k?
v	v	k7c6
případech	případ	k1gInPc6
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
nejde	jít	k5eNaImIp3nS
o	o	k7c4
autory	autor	k1gMnPc4
<g/>
,	,	kIx,
ale	ale	k8xC
o	o	k7c4
obchodní	obchodní	k2eAgInSc4d1
model	model	k1gInSc4
nahrávacích	nahrávací	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
a	a	k8xC
vydavatelů	vydavatel	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Program	program	k1gInSc1
do	do	k7c2
parlamentních	parlamentní	k2eAgFnPc2d1
voleb	volba	k1gFnPc2
2017	#num#	k4
</s>
<s>
Čtyři	čtyři	k4xCgInPc1
hlavní	hlavní	k2eAgInPc1d1
programové	programový	k2eAgInPc1d1
body	bod	k1gInPc1
definované	definovaný	k2eAgInPc1d1
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
před	před	k7c7
vstupem	vstup	k1gInSc7
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
47	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kontrola	kontrola	k1gFnSc1
moci	moc	k1gFnSc2
a	a	k8xC
státních	státní	k2eAgInPc2d1
výdajů	výdaj	k1gInPc2
prostřednictvím	prostřednictvím	k7c2
transparentního	transparentní	k2eAgNnSc2d1
vládnutí	vládnutí	k1gNnSc2
a	a	k8xC
osobní	osobní	k2eAgFnSc2d1
odpovědnosti	odpovědnost	k1gFnSc2
politiků	politik	k1gMnPc2
</s>
<s>
Zjednodušení	zjednodušení	k1gNnPc4
státní	státní	k2eAgFnSc1d1
byrokracie	byrokracie	k1gFnSc1
zavedením	zavedení	k1gNnSc7
e-Governmentu	e-Government	k1gInSc2
</s>
<s>
Podpora	podpora	k1gFnSc1
malých	malý	k2eAgInPc2d1
a	a	k8xC
středních	střední	k2eAgInPc2d1
podniků	podnik	k1gInPc2
a	a	k8xC
elektronického	elektronický	k2eAgNnSc2d1
obchodování	obchodování	k1gNnSc2
<g/>
,	,	kIx,
řešení	řešení	k1gNnSc4
odlivu	odliv	k1gInSc2
kapitálu	kapitál	k1gInSc2
z	z	k7c2
Česka	Česko	k1gNnSc2
<g/>
,	,	kIx,
ke	k	k7c3
kterému	který	k3yQgNnSc3,k3yIgNnSc3,k3yRgNnSc3
dochází	docházet	k5eAaImIp3nS
skrze	skrze	k?
zahraničně	zahraničně	k6eAd1
vlastněné	vlastněný	k2eAgFnSc2d1
společnosti	společnost	k1gFnSc2
<g/>
,	,	kIx,
prevence	prevence	k1gFnSc1
obcházení	obcházení	k1gNnSc2
daňových	daňový	k2eAgFnPc2d1
povinností	povinnost	k1gFnPc2
<g/>
,	,	kIx,
ke	k	k7c3
kterým	který	k3yRgFnPc3,k3yIgFnPc3,k3yQgFnPc3
dochází	docházet	k5eAaImIp3nS
skrze	skrze	k?
transakce	transakce	k1gFnSc1
zisků	zisk	k1gInPc2
do	do	k7c2
daňových	daňový	k2eAgInPc2d1
rájů	ráj	k1gInPc2
</s>
<s>
Ochrana	ochrana	k1gFnSc1
občanských	občanský	k2eAgFnPc2d1
svobod	svoboda	k1gFnPc2
<g/>
,	,	kIx,
svoboda	svoboda	k1gFnSc1
informací	informace	k1gFnPc2
<g/>
,	,	kIx,
svoboda	svoboda	k1gFnSc1
projevu	projev	k1gInSc2
<g/>
,	,	kIx,
demokracie	demokracie	k1gFnSc2
a	a	k8xC
zvyšování	zvyšování	k1gNnSc2
účasti	účast	k1gFnSc2
veřejnosti	veřejnost	k1gFnSc2
na	na	k7c6
rozhodování	rozhodování	k1gNnSc6
(	(	kIx(
<g/>
participativní	participativní	k2eAgFnSc2d1
demokracie	demokracie	k1gFnSc2
<g/>
)	)	kIx)
</s>
<s>
Program	program	k1gInSc1
do	do	k7c2
parlamentních	parlamentní	k2eAgFnPc2d1
voleb	volba	k1gFnPc2
2021	#num#	k4
</s>
<s>
Piráti	pirát	k1gMnPc1
s	s	k7c7
politickým	politický	k2eAgNnSc7d1
hnutím	hnutí	k1gNnSc7
Starostové	Starostová	k1gFnSc2
a	a	k8xC
nezávislí	závislý	k2eNgMnPc1d1
(	(	kIx(
<g/>
STAN	stan	k1gInSc1
<g/>
)	)	kIx)
uzavřeli	uzavřít	k5eAaPmAgMnP
v	v	k7c6
lednu	leden	k1gInSc6
2021	#num#	k4
předvolební	předvolební	k2eAgFnSc4d1
koaliční	koaliční	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
uvádí	uvádět	k5eAaImIp3nS
jako	jako	k9
programové	programový	k2eAgFnPc4d1
priority	priorita	k1gFnPc4
nízké	nízký	k2eAgFnSc2d1
daně	daň	k1gFnSc2
<g/>
,	,	kIx,
dostupnou	dostupný	k2eAgFnSc4d1
zdravotní	zdravotní	k2eAgFnSc4d1
péči	péče	k1gFnSc4
v	v	k7c6
regionech	region	k1gInPc6
<g/>
,	,	kIx,
ochranu	ochrana	k1gFnSc4
klimatu	klima	k1gNnSc2
<g/>
,	,	kIx,
inovace	inovace	k1gFnSc1
a	a	k8xC
transparentní	transparentní	k2eAgNnSc1d1
vládnutí	vládnutí	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
48	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Evropa	Evropa	k1gFnSc1
a	a	k8xC
mezinárodní	mezinárodní	k2eAgFnSc1d1
politika	politika	k1gFnSc1
</s>
<s>
Strana	strana	k1gFnSc1
je	být	k5eAaImIp3nS
proevropská	proevropský	k2eAgFnSc1d1
a	a	k8xC
podporuje	podporovat	k5eAaImIp3nS
integraci	integrace	k1gFnSc4
v	v	k7c6
rámci	rámec	k1gInSc6
EU	EU	kA
a	a	k8xC
zároveň	zároveň	k6eAd1
usiluje	usilovat	k5eAaImIp3nS
o	o	k7c4
zásadní	zásadní	k2eAgFnPc4d1
reformy	reforma	k1gFnPc4
EU	EU	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
49	#num#	k4
<g/>
]	]	kIx)
Strana	strana	k1gFnSc1
také	také	k9
respektuje	respektovat	k5eAaImIp3nS
stávající	stávající	k2eAgNnSc4d1
české	český	k2eAgNnSc4d1
členství	členství	k1gNnSc4
v	v	k7c6
NATO	NATO	kA
a	a	k8xC
pražský	pražský	k2eAgMnSc1d1
primátor	primátor	k1gMnSc1
Zdeněk	Zdeněk	k1gMnSc1
Hřib	hřib	k1gInSc4
považuje	považovat	k5eAaImIp3nS
vstup	vstup	k1gInSc4
do	do	k7c2
NATO	NATO	kA
za	za	k7c2
„	„	k?
<g/>
jeden	jeden	k4xCgInSc4
z	z	k7c2
klíčových	klíčový	k2eAgInPc2d1
kroků	krok	k1gInPc2
<g/>
,	,	kIx,
prostřednictvím	prostřednictvím	k7c2
<g />
.	.	kIx.
</s>
<s hack="1">
kterého	který	k3yRgMnSc4,k3yIgMnSc4,k3yQgMnSc4
jsme	být	k5eAaImIp1nP
potvrdili	potvrdit	k5eAaPmAgMnP
naše	náš	k3xOp1gNnPc4
směřování	směřování	k1gNnPc4
na	na	k7c4
západ	západ	k1gInSc4
a	a	k8xC
posílili	posílit	k5eAaPmAgMnP
svou	svůj	k3xOyFgFnSc4
obranyschopnost	obranyschopnost	k1gFnSc4
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
50	#num#	k4
<g/>
]	]	kIx)
Někteří	některý	k3yIgMnPc1
Piráti	pirát	k1gMnPc1
však	však	k9
kritizují	kritizovat	k5eAaImIp3nP
agrese	agrese	k1gFnPc1
ze	z	k7c2
strany	strana	k1gFnSc2
členů	člen	k1gMnPc2
NATO	NATO	kA
<g/>
,	,	kIx,
například	například	k6eAd1
válku	válka	k1gFnSc4
v	v	k7c6
Iráku	Irák	k1gInSc6
<g/>
,	,	kIx,
vojenskou	vojenský	k2eAgFnSc4d1
intervenci	intervence	k1gFnSc4
v	v	k7c4
Libyi	Libye	k1gFnSc4
nebo	nebo	k8xC
tureckou	turecký	k2eAgFnSc4d1
invazi	invaze	k1gFnSc4
do	do	k7c2
Sýrie	Sýrie	k1gFnSc2
<g/>
,	,	kIx,
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
poznamenávají	poznamenávat	k5eAaImIp3nP
<g/>
,	,	kIx,
že	že	k8xS
veškerá	veškerý	k3xTgFnSc1
angažovanost	angažovanost	k1gFnSc1
sil	síla	k1gFnPc2
NATO	NATO	kA
mimo	mimo	k7c4
území	území	k1gNnSc4
jejích	její	k3xOp3gInPc2
členských	členský	k2eAgInPc2d1
států	stát	k1gInPc2
by	by	kYmCp3nS
měla	mít	k5eAaImAgFnS
probíhat	probíhat	k5eAaImF
pouze	pouze	k6eAd1
tehdy	tehdy	k6eAd1
<g/>
,	,	kIx,
bude	být	k5eAaImBp3nS
<g/>
-li	-li	k?
podporována	podporovat	k5eAaImNgFnS
formálním	formální	k2eAgNnSc7d1
usnesením	usnesení	k1gNnSc7
Spojených	spojený	k2eAgInPc2d1
národů	národ	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
53	#num#	k4
<g/>
]	]	kIx)
Podle	podle	k7c2
poslance	poslanec	k1gMnSc2
Františka	František	k1gMnSc2
Kopřivy	Kopřiva	k1gMnSc2
by	by	kYmCp3nS
si	se	k3xPyFc3
Evropa	Evropa	k1gFnSc1
měla	mít	k5eAaImAgFnS
vytvořit	vytvořit	k5eAaPmF
vlastní	vlastní	k2eAgInSc4d1
obranný	obranný	k2eAgInSc4d1
systém	systém	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
54	#num#	k4
<g/>
]	]	kIx)
Strana	strana	k1gFnSc1
odsoudila	odsoudit	k5eAaPmAgFnS
anexi	anexe	k1gFnSc4
Krymu	Krym	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podle	podle	k7c2
předsedy	předseda	k1gMnSc2
strany	strana	k1gFnSc2
Bartoše	Bartoš	k1gMnSc2
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
sám	sám	k3xTgMnSc1
sebe	sebe	k3xPyFc4
označuje	označovat	k5eAaImIp3nS
za	za	k7c4
pacifistu	pacifista	k1gMnSc4
<g/>
,	,	kIx,
sankce	sankce	k1gFnPc1
jsou	být	k5eAaImIp3nP
jediný	jediný	k2eAgInSc4d1
mírový	mírový	k2eAgInSc4d1
způsob	způsob	k1gInSc4
<g/>
,	,	kIx,
jakým	jaký	k3yQgInSc7,k3yRgInSc7,k3yIgInSc7
lze	lze	k6eAd1
konflikt	konflikt	k1gInSc4
s	s	k7c7
Ruskem	Rusko	k1gNnSc7
řešit	řešit	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
Bartoš	Bartoš	k1gMnSc1
také	také	k9
kritizoval	kritizovat	k5eAaImAgMnS
anexi	anexe	k1gFnSc4
Východního	východní	k2eAgInSc2d1
Jeruzaléma	Jeruzalém	k1gInSc2
Izraelem	Izrael	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
56	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Piráti	pirát	k1gMnPc1
kritizují	kritizovat	k5eAaImIp3nP
úzkou	úzký	k2eAgFnSc4d1
spolupráci	spolupráce	k1gFnSc4
EU	EU	kA
s	s	k7c7
nedemokratickým	demokratický	k2eNgInSc7d1
režimem	režim	k1gInSc7
v	v	k7c6
Turecku	Turecko	k1gNnSc6
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
po	po	k7c6
neúspěšném	úspěšný	k2eNgInSc6d1
pokusu	pokus	k1gInSc6
o	o	k7c4
vojenský	vojenský	k2eAgInSc4d1
převrat	převrat	k1gInSc4
v	v	k7c6
červenci	červenec	k1gInSc6
2016	#num#	k4
zahájil	zahájit	k5eAaPmAgMnS
rozsáhlé	rozsáhlý	k2eAgFnPc4d1
politické	politický	k2eAgFnPc4d1
čistky	čistka	k1gFnPc4
proti	proti	k7c3
skutečným	skutečný	k2eAgInPc3d1
i	i	k8xC
domnělým	domnělý	k2eAgInPc3d1
odpůrcům	odpůrce	k1gMnPc3
prezidenta	prezident	k1gMnSc4
Erdoğ	Erdoğ	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
57	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
58	#num#	k4
<g/>
]	]	kIx)
Pirátští	pirátský	k2eAgMnPc1d1
poslanci	poslanec	k1gMnPc1
předali	předat	k5eAaPmAgMnP
české	český	k2eAgNnSc4d1
<g />
.	.	kIx.
</s>
<s hack="1">
vládě	vláda	k1gFnSc6
petici	petice	k1gFnSc4
požadující	požadující	k2eAgFnSc4d1
ukončení	ukončení	k1gNnSc4
rozhovorů	rozhovor	k1gInPc2
o	o	k7c6
vstupu	vstup	k1gInSc6
Turecka	Turecko	k1gNnSc2
do	do	k7c2
Evropské	evropský	k2eAgFnSc2d1
unie	unie	k1gFnSc2
a	a	k8xC
uvalení	uvalení	k1gNnSc2
ekonomických	ekonomický	k2eAgFnPc2d1
sankcí	sankce	k1gFnPc2
na	na	k7c4
Turecko	Turecko	k1gNnSc4
do	do	k7c2
doby	doba	k1gFnSc2
<g/>
,	,	kIx,
než	než	k8xS
turecká	turecký	k2eAgFnSc1d1
armáda	armáda	k1gFnSc1
ukončí	ukončit	k5eAaPmIp3nS
okupaci	okupace	k1gFnSc4
kurdských	kurdský	k2eAgFnPc2d1
oblastí	oblast	k1gFnPc2
na	na	k7c6
severu	sever	k1gInSc6
Sýrie	Sýrie	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
52	#num#	k4
<g/>
]	]	kIx)
Terčem	terč	k1gInSc7
kritiky	kritika	k1gFnSc2
Pirátů	pirát	k1gMnPc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgInS
také	také	k9
vývoz	vývoz	k1gInSc1
evropských	evropský	k2eAgFnPc2d1
zbraní	zbraň	k1gFnPc2
do	do	k7c2
Saúdské	saúdský	k2eAgFnSc2d1
Arábie	Arábie	k1gFnSc2
a	a	k8xC
Saúdy	Saúda	k1gFnSc2
vedená	vedený	k2eAgFnSc1d1
vojenská	vojenský	k2eAgFnSc1d1
intervence	intervence	k1gFnSc1
v	v	k7c6
Jemenu	Jemen	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
59	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Strana	strana	k1gFnSc1
podporuje	podporovat	k5eAaImIp3nS
manifest	manifest	k1gInSc4
panevropského	panevropský	k2eAgNnSc2d1
politického	politický	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
DieM	DieM	k1gFnSc2
<g/>
25	#num#	k4
<g/>
,	,	kIx,
jenž	jenž	k3xRgMnSc1
se	se	k3xPyFc4
zaměřuje	zaměřovat	k5eAaImIp3nS
na	na	k7c4
demokratickou	demokratický	k2eAgFnSc4d1
reformaci	reformace	k1gFnSc4
EU	EU	kA
a	a	k8xC
řešení	řešení	k1gNnSc4
demokratického	demokratický	k2eAgInSc2d1
deficitu	deficit	k1gInSc2
v	v	k7c6
Evropské	evropský	k2eAgFnSc6d1
unii	unie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
60	#num#	k4
<g/>
]	]	kIx)
Piráti	pirát	k1gMnPc1
jsou	být	k5eAaImIp3nP
proti	proti	k7c3
Transatlantickému	transatlantický	k2eAgNnSc3d1
obchodnímu	obchodní	k2eAgNnSc3d1
a	a	k8xC
investičnímu	investiční	k2eAgNnSc3d1
partnerství	partnerství	k1gNnSc3
(	(	kIx(
<g/>
TTIP	TTIP	kA
<g/>
)	)	kIx)
mezi	mezi	k7c7
EU	EU	kA
a	a	k8xC
Spojenými	spojený	k2eAgInPc7d1
státy	stát	k1gInPc7
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
61	#num#	k4
<g/>
]	]	kIx)
Strana	strana	k1gFnSc1
odmítá	odmítat	k5eAaImIp3nS
povinné	povinný	k2eAgFnPc4d1
kvóty	kvóta	k1gFnPc4
pro	pro	k7c4
přerozdělování	přerozdělování	k1gNnPc4
migrantů	migrant	k1gMnPc2
mezi	mezi	k7c4
členské	členský	k2eAgInPc4d1
státy	stát	k1gInPc4
EU	EU	kA
<g/>
,	,	kIx,
rozlišuje	rozlišovat	k5eAaImIp3nS
ale	ale	k8xC
mezi	mezi	k7c7
ekonomickými	ekonomický	k2eAgMnPc7d1
migranty	migrant	k1gMnPc7
a	a	k8xC
uprchlíky	uprchlík	k1gMnPc7
z	z	k7c2
válečné	válečný	k2eAgFnSc2d1
zóny	zóna	k1gFnSc2
<g/>
,	,	kIx,
kterým	který	k3yQgFnPc3,k3yIgFnPc3,k3yRgFnPc3
by	by	kYmCp3nS
se	se	k3xPyFc4
mělo	mít	k5eAaImAgNnS
pomáhat	pomáhat	k5eAaImF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
55	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
otázce	otázka	k1gFnSc6
integrace	integrace	k1gFnSc2
migrantů	migrant	k1gMnPc2
do	do	k7c2
společnosti	společnost	k1gFnSc2
Pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
zdůrazňuje	zdůrazňovat	k5eAaImIp3nS
důležitost	důležitost	k1gFnSc4
otázek	otázka	k1gFnPc2
jako	jako	k8xC,k8xS
je	být	k5eAaImIp3nS
ochrana	ochrana	k1gFnSc1
lidských	lidský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
a	a	k8xC
boj	boj	k1gInSc4
proti	proti	k7c3
rasismu	rasismus	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
62	#num#	k4
<g/>
]	]	kIx)
Piráti	pirát	k1gMnPc1
podpořili	podpořit	k5eAaPmAgMnP
Globální	globální	k2eAgInSc4d1
pakt	pakt	k1gInSc4
OSN	OSN	kA
o	o	k7c6
migraci	migrace	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
63	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Program	program	k1gInSc1
do	do	k7c2
evropských	evropský	k2eAgFnPc2d1
voleb	volba	k1gFnPc2
2019	#num#	k4
</s>
<s>
Program	program	k1gInSc1
do	do	k7c2
voleb	volba	k1gFnPc2
do	do	k7c2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2019	#num#	k4
byl	být	k5eAaImAgInS
zveřejněn	zveřejnit	k5eAaPmNgInS
pod	pod	k7c7
záštitou	záštita	k1gFnSc7
Evropské	evropský	k2eAgFnSc2d1
pirátské	pirátský	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gNnSc1
jádro	jádro	k1gNnSc1
spočívá	spočívat	k5eAaImIp3nS
v	v	k7c6
řešení	řešení	k1gNnSc6
demokratického	demokratický	k2eAgInSc2d1
deficitu	deficit	k1gInSc2
v	v	k7c6
Evropské	evropský	k2eAgFnSc6d1
unii	unie	k1gFnSc6
<g/>
,	,	kIx,
decentralizace	decentralizace	k1gFnSc1
a	a	k8xC
prosazení	prosazení	k1gNnSc1
principu	princip	k1gInSc2
subsidiarity	subsidiarita	k1gFnSc2
<g/>
,	,	kIx,
tj.	tj.	kA
přenechání	přenechání	k1gNnSc1
rozhodování	rozhodování	k1gNnSc2
o	o	k7c6
místních	místní	k2eAgFnPc6d1
záležitostech	záležitost	k1gFnPc6
národním	národní	k2eAgInPc3d1
a	a	k8xC
lokálním	lokální	k2eAgInPc3d1
stupňům	stupeň	k1gInPc3
veřejné	veřejný	k2eAgFnSc2d1
správy	správa	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yQgFnPc1,k3yIgFnPc1,k3yRgFnPc1
jsou	být	k5eAaImIp3nP
blízko	blízko	k6eAd1
občanům	občan	k1gMnPc3
členských	členský	k2eAgInPc2d1
států	stát	k1gInPc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
64	#num#	k4
<g/>
]	]	kIx)
reformu	reforma	k1gFnSc4
zdanění	zdanění	k1gNnSc2
nadnárodních	nadnárodní	k2eAgFnPc2d1
korporací	korporace	k1gFnPc2
a	a	k8xC
o	o	k7c6
řešení	řešení	k1gNnSc6
dopadu	dopad	k1gInSc2
lidských	lidský	k2eAgFnPc2d1
aktivit	aktivita	k1gFnPc2
na	na	k7c4
prostředí	prostředí	k1gNnSc4
a	a	k8xC
krajinu	krajina	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
22	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Kromě	kromě	k7c2
reformy	reforma	k1gFnSc2
autorských	autorský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
či	či	k8xC
digitální	digitální	k2eAgFnSc2d1
agendy	agenda	k1gFnSc2
pokrývá	pokrývat	k5eAaImIp3nS
témata	téma	k1gNnPc4
jako	jako	k8xS,k8xC
vzdělávání	vzdělávání	k1gNnSc4
<g/>
,	,	kIx,
ochranu	ochrana	k1gFnSc4
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
a	a	k8xC
zemědělství	zemědělství	k1gNnSc2
<g/>
,	,	kIx,
zahraniční	zahraniční	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
<g/>
,	,	kIx,
obranu	obrana	k1gFnSc4
<g/>
,	,	kIx,
dopravu	doprava	k1gFnSc4
a	a	k8xC
daňový	daňový	k2eAgInSc4d1
systém	systém	k1gInSc4
či	či	k8xC
vesmírný	vesmírný	k2eAgInSc4d1
program	program	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strana	strana	k1gFnSc1
uvedla	uvést	k5eAaPmAgFnS
jako	jako	k9
svoje	svůj	k3xOyFgInPc4
cíle	cíl	k1gInPc4
„	„	k?
<g/>
lidskou	lidský	k2eAgFnSc4d1
<g/>
,	,	kIx,
moderní	moderní	k2eAgFnSc4d1
Evropu	Evropa	k1gFnSc4
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yIgFnSc1,k3yQgFnSc1
bude	být	k5eAaImBp3nS
udávat	udávat	k5eAaImF
světu	svět	k1gInSc3
kurz	kurz	k1gInSc4
v	v	k7c6
otázkách	otázka	k1gFnPc6
využívání	využívání	k1gNnSc2
technologií	technologie	k1gFnPc2
<g/>
,	,	kIx,
inovací	inovace	k1gFnPc2
<g/>
,	,	kIx,
demokracie	demokracie	k1gFnSc2
<g/>
,	,	kIx,
občanské	občanský	k2eAgFnSc2d1
participace	participace	k1gFnSc2
a	a	k8xC
životního	životní	k2eAgNnSc2d1
prostředí	prostředí	k1gNnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
Čeští	český	k2eAgMnPc1d1
Piráti	pirát	k1gMnPc1
zveřejnili	zveřejnit	k5eAaPmAgMnP
svůj	svůj	k3xOyFgInSc4
vlastní	vlastní	k2eAgInSc4d1
program	program	k1gInSc4
priorit	priorita	k1gFnPc2
shrnutý	shrnutý	k2eAgInSc4d1
do	do	k7c2
pěti	pět	k4xCc2
bodů	bod	k1gInPc2
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
66	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Svoboda	svoboda	k1gFnSc1
<g/>
:	:	kIx,
ochrana	ochrana	k1gFnSc1
svobody	svoboda	k1gFnSc2
proti	proti	k7c3
hrozbám	hrozba	k1gFnPc3
zvenčí	zvenčí	k6eAd1
posílením	posílení	k1gNnSc7
evropské	evropský	k2eAgFnSc2d1
obrany	obrana	k1gFnSc2
a	a	k8xC
proti	proti	k7c3
vnitřním	vnitřní	k2eAgFnPc3d1
hrozbám	hrozba	k1gFnPc3
jako	jako	k8xS,k8xC
autoritářství	autoritářství	k1gNnSc1
<g/>
,	,	kIx,
extremismus	extremismus	k1gInSc1
a	a	k8xC
cenzura	cenzura	k1gFnSc1
</s>
<s>
Upgrade	upgrade	k1gInSc1
<g/>
:	:	kIx,
průhlednost	průhlednost	k1gFnSc1
evropského	evropský	k2eAgNnSc2d1
rozhodování	rozhodování	k1gNnSc2
<g/>
,	,	kIx,
umožnění	umožnění	k1gNnSc1
účasti	účast	k1gFnSc2
občanům	občan	k1gMnPc3
a	a	k8xC
přiblížení	přiblížení	k1gNnSc2
rozhodování	rozhodování	k1gNnSc2
k	k	k7c3
potřebám	potřeba	k1gFnPc3
občanů	občan	k1gMnPc2
</s>
<s>
Bohatství	bohatství	k1gNnSc1
<g/>
:	:	kIx,
reforma	reforma	k1gFnSc1
zdanění	zdanění	k1gNnSc2
korporací	korporace	k1gFnPc2
<g/>
,	,	kIx,
transparentní	transparentní	k2eAgNnSc1d1
hospodaření	hospodaření	k1gNnSc1
s	s	k7c7
evropskými	evropský	k2eAgInPc7d1
penězi	peníze	k1gInPc7
<g/>
,	,	kIx,
podpora	podpora	k1gFnSc1
malých	malý	k2eAgInPc2d1
a	a	k8xC
středních	střední	k2eAgInPc2d1
podniků	podnik	k1gInPc2
</s>
<s>
Udržitelnost	udržitelnost	k1gFnSc4
<g/>
:	:	kIx,
řešení	řešení	k1gNnSc4
příčin	příčina	k1gFnPc2
klimatické	klimatický	k2eAgFnSc2d1
změny	změna	k1gFnSc2
a	a	k8xC
jejích	její	k3xOp3gInPc2
dopadů	dopad	k1gInPc2
na	na	k7c4
krajinu	krajina	k1gFnSc4
<g/>
,	,	kIx,
řešení	řešení	k1gNnSc4
odpadů	odpad	k1gInPc2
a	a	k8xC
závislosti	závislost	k1gFnSc2
na	na	k7c6
fosilních	fosilní	k2eAgNnPc6d1
palivech	palivo	k1gNnPc6
<g/>
,	,	kIx,
rozvoj	rozvoj	k1gInSc4
venkova	venkov	k1gInSc2
a	a	k8xC
farmářů	farmář	k1gMnPc2
</s>
<s>
Spravedlnost	spravedlnost	k1gFnSc1
<g/>
:	:	kIx,
ochrana	ochrana	k1gFnSc1
spotřebitele	spotřebitel	k1gMnSc2
<g/>
,	,	kIx,
sociální	sociální	k2eAgFnSc4d1
spravedlnost	spravedlnost	k1gFnSc4
<g/>
,	,	kIx,
práva	právo	k1gNnPc4
pracovníků	pracovník	k1gMnPc2
</s>
<s>
Historie	historie	k1gFnSc1
strany	strana	k1gFnSc2
</s>
<s>
Založení	založení	k1gNnSc1
strany	strana	k1gFnSc2
roku	rok	k1gInSc2
2009	#num#	k4
</s>
<s>
Zakladatel	zakladatel	k1gMnSc1
Jiří	Jiří	k1gMnSc1
Kadeřávek	Kadeřávek	k1gMnSc1
se	se	k3xPyFc4
po	po	k7c6
založení	založení	k1gNnSc6
stal	stát	k5eAaPmAgMnS
prvním	první	k4xOgMnSc7
místopředsedou	místopředseda	k1gMnSc7
strany	strana	k1gFnSc2
<g/>
[	[	kIx(
<g/>
67	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pirátské	pirátský	k2eAgFnPc1d1
strany	strana	k1gFnPc1
vznikly	vzniknout	k5eAaPmAgFnP
v	v	k7c6
mnoha	mnoho	k4c6
zemích	zem	k1gFnPc6
v	v	k7c6
Evropě	Evropa	k1gFnSc6
i	i	k8xC
po	po	k7c6
celém	celý	k2eAgInSc6d1
světě	svět	k1gInSc6
jako	jako	k8xC,k8xS
výraz	výraz	k1gInSc1
odporu	odpor	k1gInSc2
proti	proti	k7c3
údajnému	údajný	k2eAgNnSc3d1
omezování	omezování	k1gNnSc3
základních	základní	k2eAgNnPc2d1
občanských	občanský	k2eAgNnPc2d1
práv	právo	k1gNnPc2
vlivnými	vlivný	k2eAgFnPc7d1
lobbistickými	lobbistický	k2eAgFnPc7d1
skupinami	skupina	k1gFnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
68	#num#	k4
<g/>
]	]	kIx)
Výzvu	výzev	k1gInSc2
k	k	k7c3
založení	založení	k1gNnSc3
České	český	k2eAgFnSc2d1
pirátské	pirátský	k2eAgFnSc2d1
strany	strana	k1gFnSc2
publikoval	publikovat	k5eAaBmAgMnS
programátor	programátor	k1gMnSc1
Jiří	Jiří	k1gMnSc1
Kadeřávek	Kadeřávek	k1gMnSc1
na	na	k7c6
portálu	portál	k1gInSc6
AbcLinuxu	AbcLinux	k1gInSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
19	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
.	.	kIx.
dubna	duben	k1gInSc2
2009	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
69	#num#	k4
<g/>
]	]	kIx)
Zanedlouho	zanedlouho	k6eAd1
se	se	k3xPyFc4
zformoval	zformovat	k5eAaPmAgInS
přípravný	přípravný	k2eAgInSc1d1
výbor	výbor	k1gInSc1
a	a	k8xC
vznikla	vzniknout	k5eAaPmAgFnS
online	onlinout	k5eAaPmIp3nS
petice	petice	k1gFnPc1
pro	pro	k7c4
získání	získání	k1gNnPc4
zákonem	zákon	k1gInSc7
stanoveného	stanovený	k2eAgInSc2d1
počtu	počet	k1gInSc2
podpisů	podpis	k1gInPc2
a	a	k8xC
aktivaci	aktivace	k1gFnSc3
budoucí	budoucí	k2eAgFnSc2d1
členské	členský	k2eAgFnSc2d1
základny	základna	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
70	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
71	#num#	k4
<g/>
]	]	kIx)
Tisíc	tisíc	k4xCgInSc1
podpisů	podpis	k1gInPc2
v	v	k7c4
online	onlinout	k5eAaPmIp3nS
petici	petice	k1gFnSc4
se	se	k3xPyFc4
straně	strana	k1gFnSc3
podařilo	podařit	k5eAaPmAgNnS
shromáždit	shromáždit	k5eAaPmF
během	během	k7c2
méně	málo	k6eAd2
než	než	k8xS
dvou	dva	k4xCgInPc2
dnů	den	k1gInPc2
od	od	k7c2
zprovoznění	zprovoznění	k1gNnSc2
webových	webový	k2eAgFnPc2d1
stránek	stránka	k1gFnPc2
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
do	do	k7c2
konce	konec	k1gInSc2
měsíce	měsíc	k1gInSc2
dubna	duben	k1gInSc2
pak	pak	k6eAd1
signovalo	signovat	k5eAaBmAgNnS
petici	petice	k1gFnSc4
na	na	k7c4
vznik	vznik	k1gInSc4
strany	strana	k1gFnSc2
přes	přes	k7c4
dva	dva	k4xCgInPc4
tisíce	tisíc	k4xCgInPc4
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
dubnu	duben	k1gInSc6
2009	#num#	k4
začal	začít	k5eAaPmAgInS
přípravný	přípravný	k2eAgInSc1d1
výbor	výbor	k1gInSc1
rozesílat	rozesílat	k5eAaImF
petiční	petiční	k2eAgFnPc4d1
archy	archa	k1gFnPc4
na	na	k7c4
získané	získaný	k2eAgFnPc4d1
e-mailové	e-mailové	k2eAgFnPc4d1
adresy	adresa	k1gFnPc4
za	za	k7c7
účelem	účel	k1gInSc7
získání	získání	k1gNnSc2
pro	pro	k7c4
založení	založení	k1gNnSc4
strany	strana	k1gFnSc2
potřebného	potřebné	k1gNnSc2
jednoho	jeden	k4xCgMnSc4
tisíce	tisíc	k4xCgInPc1
fyzických	fyzický	k2eAgMnPc2d1
podpisů	podpis	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Dne	den	k1gInSc2
27	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2009	#num#	k4
byl	být	k5eAaImAgInS
podán	podat	k5eAaPmNgInS
návrh	návrh	k1gInSc1
na	na	k7c6
Ministerstvu	ministerstvo	k1gNnSc6
vnitra	vnitro	k1gNnSc2
na	na	k7c4
registraci	registrace	k1gFnSc4
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
72	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
73	#num#	k4
<g/>
]	]	kIx)
17	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
téhož	týž	k3xTgInSc2
roku	rok	k1gInSc2
byla	být	k5eAaImAgFnS
strana	strana	k1gFnSc1
ministerstvem	ministerstvo	k1gNnSc7
vnitra	vnitro	k1gNnSc2
zaregistrována	zaregistrován	k2eAgFnSc1d1
pod	pod	k7c7
číslem	číslo	k1gNnSc7
MV-	MV-	k1gFnSc1
<g/>
39553	#num#	k4
<g/>
-	-	kIx~
<g/>
7	#num#	k4
<g/>
/	/	kIx~
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
VS-	VS-	k1gFnSc1
<g/>
2009	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
74	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
registrace	registrace	k1gFnSc2
změny	změna	k1gFnSc2
stanov	stanova	k1gFnPc2
4	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2011	#num#	k4
strana	strana	k1gFnSc1
vystupovala	vystupovat	k5eAaImAgFnS
pod	pod	k7c7
zkratkou	zkratka	k1gFnSc7
ČPS	ČPS	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
75	#num#	k4
<g/>
]	]	kIx)
28	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2009	#num#	k4
proběhlo	proběhnout	k5eAaPmAgNnS
v	v	k7c6
Průhonicích	Průhonice	k1gFnPc6
u	u	k7c2
Prahy	Praha	k1gFnSc2
ustavující	ustavující	k2eAgNnSc1d1
fórum	fórum	k1gNnSc1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
kde	kde	k6eAd1
bylo	být	k5eAaImAgNnS
zvoleno	zvolit	k5eAaPmNgNnS
předsednictvo	předsednictvo	k1gNnSc1
strany	strana	k1gFnSc2
a	a	k8xC
byla	být	k5eAaImAgNnP
určena	určen	k2eAgNnPc1d1
hlavní	hlavní	k2eAgNnPc1d1
témata	téma	k1gNnPc1
volebního	volební	k2eAgInSc2d1
programu	program	k1gInSc2
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předsedou	předseda	k1gMnSc7
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
Kamil	Kamil	k1gMnSc1
Horký	Horký	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
76	#num#	k4
<g/>
]	]	kIx)
Ke	k	k7c3
konci	konec	k1gInSc3
října	říjen	k1gInSc2
2009	#num#	k4
se	se	k3xPyFc4
v	v	k7c6
Albrechticích	Albrechtice	k1gFnPc6
nad	nad	k7c7
Orlicí	Orlice	k1gFnSc7
uskutečnilo	uskutečnit	k5eAaPmAgNnS
1	#num#	k4
<g/>
.	.	kIx.
zasedání	zasedání	k1gNnSc2
celostátního	celostátní	k2eAgNnSc2d1
fóra	fórum	k1gNnSc2
(	(	kIx(
<g/>
CF	CF	kA
<g/>
)	)	kIx)
<g/>
,	,	kIx,
na	na	k7c6
kterém	který	k3yQgInSc6,k3yRgInSc6,k3yIgInSc6
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
doplnění	doplnění	k1gNnSc3
stanov	stanova	k1gFnPc2
a	a	k8xC
volbě	volba	k1gFnSc3
nového	nový	k2eAgNnSc2d1
předsednictva	předsednictvo	k1gNnSc2
<g/>
,	,	kIx,
republikového	republikový	k2eAgInSc2d1
výboru	výbor	k1gInSc2
a	a	k8xC
komisí	komise	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předsedou	předseda	k1gMnSc7
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
Ivan	Ivan	k1gMnSc1
Bartoš	Bartoš	k1gMnSc1
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
77	#num#	k4
<g/>
]	]	kIx)
který	který	k3yRgMnSc1,k3yIgMnSc1,k3yQgMnSc1
funkci	funkce	k1gFnSc4
na	na	k7c6
zasedání	zasedání	k1gNnSc6
CF	CF	kA
v	v	k7c6
srpnu	srpen	k1gInSc6
2010	#num#	k4
v	v	k7c6
Brně	Brno	k1gNnSc6
obhájil	obhájit	k5eAaPmAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
78	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
třetím	třetí	k4xOgNnSc6
zasedání	zasedání	k1gNnSc6
CF	CF	kA
konaném	konaný	k2eAgInSc6d1
v	v	k7c6
červnu	červen	k1gInSc6
2011	#num#	k4
v	v	k7c6
Křižanově	Křižanův	k2eAgFnSc6d1
mimo	mimo	k7c4
jiné	jiný	k2eAgFnPc4d1
došlo	dojít	k5eAaPmAgNnS
ke	k	k7c3
změně	změna	k1gFnSc3
zkratky	zkratka	k1gFnSc2
z	z	k7c2
ČPS	ČPS	kA
na	na	k7c6
Piráti	pirát	k1gMnPc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
79	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
srpnu	srpen	k1gInSc6
2012	#num#	k4
proběhlo	proběhnout	k5eAaPmAgNnS
zasedání	zasedání	k1gNnSc1
CF	CF	kA
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
něm	on	k3xPp3gMnSc6
byly	být	k5eAaImAgFnP
schváleny	schválen	k2eAgFnPc1d1
změny	změna	k1gFnPc1
stanov	stanova	k1gFnPc2
<g/>
,	,	kIx,
v	v	k7c6
kterých	který	k3yRgInPc6,k3yQgInPc6,k3yIgInPc6
je	být	k5eAaImIp3nS
od	od	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
zanesena	zanesen	k2eAgFnSc1d1
explicitní	explicitní	k2eAgFnSc1d1
podpora	podpora	k1gFnSc1
svobodných	svobodný	k2eAgNnPc2d1
děl	dělo	k1gNnPc2
a	a	k8xC
svobodného	svobodný	k2eAgNnSc2d1
software	software	k1gInSc4
a	a	k8xC
také	také	k9
povinnost	povinnost	k1gFnSc4
veřejného	veřejný	k2eAgNnSc2d1
hlasování	hlasování	k1gNnSc2
volených	volený	k2eAgMnPc2d1
zástupců	zástupce	k1gMnPc2
<g/>
,	,	kIx,
kteří	který	k3yQgMnPc1,k3yRgMnPc1,k3yIgMnPc1
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
mohli	moct	k5eAaImAgMnP
volit	volit	k5eAaImF
i	i	k9
tajně	tajně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
zasedání	zasedání	k1gNnSc6
bylo	být	k5eAaImAgNnS
dále	daleko	k6eAd2
zvoleno	zvolen	k2eAgNnSc1d1
nové	nový	k2eAgNnSc1d1
předsednictvo	předsednictvo	k1gNnSc1
<g/>
,	,	kIx,
přičemž	přičemž	k6eAd1
pozici	pozice	k1gFnSc4
předsedy	předseda	k1gMnSc2
obhájil	obhájit	k5eAaPmAgMnS
Ivan	Ivan	k1gMnSc1
Bartoš	Bartoš	k1gMnSc1
<g/>
,	,	kIx,
a	a	k8xC
byl	být	k5eAaImAgInS
schválen	schválit	k5eAaPmNgInS
logotyp	logotyp	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
80	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Počáteční	počáteční	k2eAgInSc1d1
aktivismus	aktivismus	k1gInSc1
mezi	mezi	k7c7
lety	léto	k1gNnPc7
2010	#num#	k4
a	a	k8xC
2012	#num#	k4
</s>
<s>
Česká	český	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
21	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2010	#num#	k4
spustila	spustit	k5eAaPmAgFnS
server	server	k1gInSc4
PirateLeaks	PirateLeaksa	k1gFnPc2
po	po	k7c6
vzoru	vzor	k1gInSc6
WikiLeaks	WikiLeaksa	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
81	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Protest	protest	k1gInSc1
proti	proti	k7c3
dohodě	dohoda	k1gFnSc3
ACTA	ACTA	kA
konaný	konaný	k2eAgInSc1d1
v	v	k7c6
únoru	únor	k1gInSc6
2012	#num#	k4
v	v	k7c6
Praze	Praha	k1gFnSc6
</s>
<s>
Pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
vystoupila	vystoupit	k5eAaPmAgFnS
proti	proti	k7c3
připravované	připravovaný	k2eAgFnSc3d1
novele	novela	k1gFnSc3
autorského	autorský	k2eAgInSc2d1
zákona	zákon	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
měla	mít	k5eAaImAgFnS
zvýšit	zvýšit	k5eAaPmF
platby	platba	k1gFnPc4
knihoven	knihovna	k1gFnPc2
za	za	k7c2
kopírky	kopírka	k1gFnSc2
a	a	k8xC
znovu	znovu	k6eAd1
zavést	zavést	k5eAaPmF
povinnost	povinnost	k1gFnSc4
ohlašovat	ohlašovat	k5eAaImF
koncerty	koncert	k1gInPc4
OSA	osa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
82	#num#	k4
<g/>
]	]	kIx)
Ministr	ministr	k1gMnSc1
musel	muset	k5eAaImAgMnS
následně	následně	k6eAd1
novelu	novela	k1gFnSc4
z	z	k7c2
projednávání	projednávání	k1gNnSc2
stáhnout	stáhnout	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
83	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
<g />
.	.	kIx.
</s>
<s hack="1">
dubnu	duben	k1gInSc6
roku	rok	k1gInSc2
2010	#num#	k4
zaslala	zaslat	k5eAaPmAgFnS
Pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
svůj	svůj	k3xOyFgInSc4
návrh	návrh	k1gInSc4
kopírovacího	kopírovací	k2eAgInSc2d1
zákona	zákon	k1gInSc2
ministerstvu	ministerstvo	k1gNnSc3
kultury	kultura	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yQgNnSc1,k3yIgNnSc1,k3yRgNnSc1
pořádalo	pořádat	k5eAaImAgNnS
veřejnou	veřejný	k2eAgFnSc4d1
konzultaci	konzultace	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
46	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
roce	rok	k1gInSc6
2011	#num#	k4
pak	pak	k9
kritizovala	kritizovat	k5eAaImAgFnS
ministerstvo	ministerstvo	k1gNnSc4
kultury	kultura	k1gFnSc2
kvůli	kvůli	k7c3
prodlužování	prodlužování	k1gNnSc3
monopolu	monopol	k1gInSc2
na	na	k7c4
hudební	hudební	k2eAgFnPc4d1
nahrávky	nahrávka	k1gFnPc4
<g/>
[	[	kIx(
<g/>
84	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
85	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
zorganizovala	zorganizovat	k5eAaPmAgFnS
proti	proti	k7c3
němu	on	k3xPp3gInSc3
petici	petice	k1gFnSc4
Výzva	výzva	k1gFnSc1
na	na	k7c4
obranu	obrana	k1gFnSc4
kopírování	kopírování	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
86	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Když	když	k8xS
v	v	k7c6
červnu	červen	k1gInSc6
2011	#num#	k4
poslanec	poslanec	k1gMnSc1
Jan	Jan	k1gMnSc1
Farský	farský	k2eAgMnSc1d1
(	(	kIx(
<g/>
STAN	stan	k1gInSc1
<g/>
,	,	kIx,
resp.	resp.	kA
TOP	topit	k5eAaImRp2nS
0	#num#	k4
<g/>
9	#num#	k4
<g/>
)	)	kIx)
spolu	spolu	k6eAd1
s	s	k7c7
poslanci	poslanec	k1gMnPc7
ODS	ODS	kA
navrhl	navrhnout	k5eAaPmAgMnS
přílepek	přílepek	k1gInSc4
k	k	k7c3
loterijnímu	loterijní	k2eAgInSc3d1
zákonu	zákon	k1gInSc3
<g/>
,	,	kIx,
podle	podle	k7c2
nějž	jenž	k3xRgInSc2
by	by	kYmCp3nP
internetoví	internetový	k2eAgMnPc1d1
provideři	provider	k1gMnPc1
museli	muset	k5eAaImAgMnP
pod	pod	k7c7
pokutou	pokuta	k1gFnSc7
10	#num#	k4
milionů	milion	k4xCgInPc2
Kč	Kč	kA
blokovat	blokovat	k5eAaImF
weby	web	k1gInPc4
zahraničních	zahraniční	k2eAgFnPc2d1
sázkových	sázkový	k2eAgFnPc2d1
společností	společnost	k1gFnPc2
a	a	k8xC
všechny	všechen	k3xTgInPc1
weby	web	k1gInPc1
s	s	k7c7
jejich	jejich	k3xOp3gNnSc7
<g />
.	.	kIx.
</s>
<s hack="1">
reklamou	reklama	k1gFnSc7
<g/>
,	,	kIx,
Pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
proti	proti	k7c3
němu	on	k3xPp3gNnSc3
vyhlásila	vyhlásit	k5eAaPmAgFnS
kampaň	kampaň	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
87	#num#	k4
<g/>
]	]	kIx)
Petici	petice	k1gFnSc6
Internet	Internet	k1gInSc1
bez	bez	k7c2
cenzury	cenzura	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c4
které	který	k3yIgFnPc4,k3yQgFnPc4,k3yRgFnPc4
se	se	k3xPyFc4
podílel	podílet	k5eAaImAgMnS
člen	člen	k1gMnSc1
strany	strana	k1gFnSc2
Roman	Roman	k1gMnSc1
Kučera	Kučera	k1gMnSc1
<g/>
,	,	kIx,
podpořilo	podpořit	k5eAaPmAgNnS
během	během	k7c2
několika	několik	k4yIc2
dní	den	k1gInPc2
přes	přes	k7c4
13	#num#	k4
tisíc	tisíc	k4xCgInSc4
lidí	člověk	k1gMnPc2
<g/>
[	[	kIx(
<g/>
88	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
poslanci	poslanec	k1gMnPc1
návrh	návrh	k1gInSc4
zamítli	zamítnout	k5eAaPmAgMnP
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
89	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
90	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jedním	jeden	k4xCgInSc7
z	z	k7c2
prvních	první	k4xOgInPc2
úspěchů	úspěch	k1gInPc2
Pirátské	pirátský	k2eAgFnSc2d1
strany	strana	k1gFnSc2
byl	být	k5eAaImAgInS
podíl	podíl	k1gInSc1
na	na	k7c4
zamítnutí	zamítnutí	k1gNnSc4
mezinárodní	mezinárodní	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
ACTA	ACTA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proti	proti	k7c3
smlouvě	smlouva	k1gFnSc3
v	v	k7c6
únoru	únor	k1gInSc6
2012	#num#	k4
protestovaly	protestovat	k5eAaBmAgFnP
v	v	k7c6
Praze	Praha	k1gFnSc6
tisíce	tisíc	k4xCgInSc2
lidí	člověk	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
91	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
92	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
93	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
94	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
95	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
dubnu	duben	k1gInSc6
2012	#num#	k4
přesvědčili	přesvědčit	k5eAaPmAgMnP
Piráti	pirát	k1gMnPc1
ministra	ministr	k1gMnSc2
průmyslu	průmysl	k1gInSc2
a	a	k8xC
obchodu	obchod	k1gInSc2
Martina	Martin	k1gMnSc4
Kubu	Kuba	k1gMnSc4
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
se	se	k3xPyFc4
zavázal	zavázat	k5eAaPmAgInS
smlouvu	smlouva	k1gFnSc4
předložit	předložit	k5eAaPmF
Ústavnímu	ústavní	k2eAgInSc3d1
soudu	soud	k1gInSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
96	#num#	k4
<g/>
]	]	kIx)
K	k	k7c3
tomu	ten	k3xDgNnSc3
před	před	k7c7
tím	ten	k3xDgNnSc7
došlo	dojít	k5eAaPmAgNnS
pouze	pouze	k6eAd1
v	v	k7c6
případě	případ	k1gInSc6
Lisabonské	lisabonský	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nakonec	nakonec	k6eAd1
však	však	k9
byla	být	k5eAaImAgFnS
ACTA	ACTA	kA
velkou	velký	k2eAgFnSc7d1
většinou	většina	k1gFnSc7
zamítnuta	zamítnout	k5eAaPmNgFnS
Evropským	evropský	k2eAgInSc7d1
parlamentem	parlament	k1gInSc7
<g/>
,	,	kIx,
<g/>
[	[	kIx(
<g/>
97	#num#	k4
<g/>
]	]	kIx)
kde	kde	k6eAd1
proti	proti	k7c3
ní	on	k3xPp3gFnSc7
vystupovala	vystupovat	k5eAaImAgFnS
i	i	k9
europoslankyně	europoslankyně	k1gFnSc1
švédské	švédský	k2eAgFnSc2d1
Pirátské	pirátský	k2eAgFnSc2d1
strany	strana	k1gFnSc2
Amelia	Amelia	k1gFnSc1
Andersdotter	Andersdotter	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
98	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
únoru	únor	k1gInSc6
2012	#num#	k4
Piráti	pirát	k1gMnPc1
přišli	přijít	k5eAaPmAgMnP
s	s	k7c7
návrhem	návrh	k1gInSc7
ústavního	ústavní	k2eAgInSc2d1
zákona	zákon	k1gInSc2
o	o	k7c6
svobodném	svobodný	k2eAgInSc6d1
internetu	internet	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ten	ten	k3xDgMnSc1
vychází	vycházet	k5eAaImIp3nS
z	z	k7c2
principu	princip	k1gInSc2
<g/>
,	,	kIx,
že	že	k8xS
internet	internet	k1gInSc1
je	být	k5eAaImIp3nS
svobodným	svobodný	k2eAgNnSc7d1
médiem	médium	k1gNnSc7
a	a	k8xC
stát	stát	k1gInSc1
by	by	kYmCp3nS
do	do	k7c2
něj	on	k3xPp3gInSc2
neměl	mít	k5eNaImAgInS
zasahovat	zasahovat	k5eAaImF
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
je	být	k5eAaImIp3nS
podle	podle	k7c2
Pirátů	pirát	k1gMnPc2
nepřijatelná	přijatelný	k2eNgFnSc1d1
jakákoliv	jakýkoliv	k3yIgFnSc1
cenzura	cenzura	k1gFnSc1
internetu	internet	k1gInSc2
<g/>
,	,	kIx,
sledování	sledování	k1gNnSc2
či	či	k8xC
odpojování	odpojování	k1gNnSc2
jeho	jeho	k3xOp3gMnPc2
uživatelů	uživatel	k1gMnPc2
nebo	nebo	k8xC
porušování	porušování	k1gNnSc2
síťové	síťový	k2eAgFnSc2d1
neutrality	neutralita	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
99	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
100	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
říjnu	říjen	k1gInSc6
2012	#num#	k4
vystoupili	vystoupit	k5eAaPmAgMnP
Piráti	pirát	k1gMnPc1
proti	proti	k7c3
ministerstvem	ministerstvo	k1gNnSc7
vnitra	vnitro	k1gNnSc2
navržené	navržený	k2eAgFnSc6d1
novele	novela	k1gFnSc6
zákona	zákon	k1gInSc2
106	#num#	k4
<g/>
/	/	kIx~
<g/>
1999	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
o	o	k7c6
svobodném	svobodný	k2eAgInSc6d1
přístupu	přístup	k1gInSc6
k	k	k7c3
informacím	informace	k1gFnPc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
101	#num#	k4
<g/>
]	]	kIx)
Piráti	pirát	k1gMnPc1
připomínkovali	připomínkovat	k5eAaImAgMnP
návrh	návrh	k1gInSc4
novely	novela	k1gFnSc2
včetně	včetně	k7c2
předložení	předložení	k1gNnSc2
parafovaných	parafovaný	k2eAgInPc2d1
pozměňovacích	pozměňovací	k2eAgInPc2d1
návrhů	návrh	k1gInPc2
<g/>
[	[	kIx(
<g/>
102	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
na	na	k7c4
7	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
vyhlásili	vyhlásit	k5eAaPmAgMnP
„	„	k?
<g/>
Den	den	k1gInSc4
infožádostí	infožádost	k1gFnPc2
<g/>
“	“	k?
s	s	k7c7
heslem	heslo	k1gNnSc7
„	„	k?
<g/>
Zeptej	zeptat	k5eAaPmRp2nS
se	se	k3xPyFc4
svého	svůj	k3xOyFgMnSc2
úředníka	úředník	k1gMnSc2
<g/>
,	,	kIx,
dokud	dokud	k8xS
můžeš	moct	k5eAaImIp2nS
<g/>
“	“	k?
<g/>
,	,	kIx,
v	v	k7c6
jehož	jehož	k3xOyRp3gInSc6
rámci	rámec	k1gInSc6
měla	mít	k5eAaImAgFnS
veřejnost	veřejnost	k1gFnSc1
podávat	podávat	k5eAaImF
žádosti	žádost	k1gFnPc4
o	o	k7c4
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
103	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
První	první	k4xOgInPc4
úspěchy	úspěch	k1gInPc4
v	v	k7c6
senátních	senátní	k2eAgFnPc6d1
a	a	k8xC
komunálních	komunální	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
od	od	k7c2
roku	rok	k1gInSc2
2012	#num#	k4
</s>
<s>
Senátor	senátor	k1gMnSc1
Libor	Libor	k1gMnSc1
Michálek	Michálek	k1gMnSc1
na	na	k7c6
Pirátské	pirátský	k2eAgFnSc6d1
tiskové	tiskový	k2eAgFnSc6d1
konferenci	konference	k1gFnSc6
s	s	k7c7
Bartošem	Bartoš	k1gMnSc7
a	a	k8xC
Mikulášem	Mikuláš	k1gMnSc7
Ferjenčíkem	Ferjenčík	k1gMnSc7
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
dubnu	duben	k1gInSc6
2012	#num#	k4
uspořádala	uspořádat	k5eAaPmAgFnS
Česká	český	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
konferenci	konference	k1gFnSc6
Pirátské	pirátský	k2eAgFnSc2d1
internacionály	internacionála	k1gFnSc2
<g/>
,	,	kIx,
které	který	k3yIgNnSc1,k3yRgNnSc1,k3yQgNnSc1
se	se	k3xPyFc4
zúčastnilo	zúčastnit	k5eAaPmAgNnS
více	hodně	k6eAd2
než	než	k8xS
200	#num#	k4
zástupců	zástupce	k1gMnPc2
pirátských	pirátský	k2eAgFnPc2d1
stran	strana	k1gFnPc2
z	z	k7c2
27	#num#	k4
zemí	zem	k1gFnPc2
světa	svět	k1gInSc2
<g/>
,	,	kIx,
zakladatel	zakladatel	k1gMnSc1
pirátského	pirátský	k2eAgNnSc2d1
hnutí	hnutí	k1gNnSc2
Rickard	Rickard	k1gInSc1
Falkvinge	Falkvinge	k1gNnPc2
<g/>
,	,	kIx,
britsko-kanadský	britsko-kanadský	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
Cory	Cora	k1gFnSc2
Doctorow	Doctorow	k1gMnSc1
a	a	k8xC
švédská	švédský	k2eAgFnSc1d1
europoslankyně	europoslankyně	k1gFnSc1
Amelia	Amelium	k1gNnSc2
Andersdotter	Andersdottra	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
konferenci	konference	k1gFnSc6
se	se	k3xPyFc4
mimo	mimo	k7c4
jiné	jiný	k2eAgFnPc4d1
pirátské	pirátský	k2eAgFnPc4d1
strany	strana	k1gFnPc4
z	z	k7c2
Evropy	Evropa	k1gFnSc2
dohodly	dohodnout	k5eAaPmAgInP
<g/>
,	,	kIx,
že	že	k8xS
budou	být	k5eAaImBp3nP
postupovat	postupovat	k5eAaImF
společně	společně	k6eAd1
při	při	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
v	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
104	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
105	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
106	#num#	k4
<g/>
]	]	kIx)
Celostátní	celostátní	k2eAgNnSc1d1
fórum	fórum	k1gNnSc1
České	český	k2eAgFnSc2d1
pirátské	pirátský	k2eAgFnSc2d1
strany	strana	k1gFnSc2
20	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2012	#num#	k4
schválilo	schválit	k5eAaPmAgNnS
Pražskou	pražský	k2eAgFnSc4d1
deklaraci	deklarace	k1gFnSc4
stvrzující	stvrzující	k2eAgInSc4d1
tento	tento	k3xDgInSc4
záměr	záměr	k1gInSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
107	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Česká	český	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
postavila	postavit	k5eAaPmAgFnS
v	v	k7c6
senátních	senátní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
2012	#num#	k4
tři	tři	k4xCgMnPc4
kandidáty	kandidát	k1gMnPc4
<g/>
,	,	kIx,
jednoho	jeden	k4xCgMnSc4
z	z	k7c2
nich	on	k3xPp3gMnPc2
ve	v	k7c6
spolupráci	spolupráce	k1gFnSc6
s	s	k7c7
dalšími	další	k2eAgFnPc7d1
politickými	politický	k2eAgFnPc7d1
stranami	strana	k1gFnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ve	v	k7c6
volebním	volební	k2eAgInSc6d1
okrsku	okrsek	k1gInSc6
č.	č.	k?
26	#num#	k4
byl	být	k5eAaImAgMnS
kandidát	kandidát	k1gMnSc1
koalice	koalice	k1gFnSc2
Pirátů	pirát	k1gMnPc2
<g/>
,	,	kIx,
Strany	strana	k1gFnPc1
zelených	zelený	k2eAgMnPc2d1
a	a	k8xC
KDU-ČSL	KDU-ČSL	k1gFnPc2
Libor	Libor	k1gMnSc1
Michálek	Michálek	k1gMnSc1
zvolen	zvolit	k5eAaPmNgMnS
ve	v	k7c4
2	#num#	k4
<g/>
.	.	kIx.
kole	kolo	k1gNnSc6
senátorem	senátor	k1gMnSc7
a	a	k8xC
tím	ten	k3xDgNnSc7
se	se	k3xPyFc4
Česká	český	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
stala	stát	k5eAaPmAgFnS
parlamentní	parlamentní	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jsou	být	k5eAaImIp3nP
celosvětově	celosvětově	k6eAd1
první	první	k4xOgFnSc7
pirátskou	pirátský	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
<g/>
,	,	kIx,
která	který	k3yRgFnSc1,k3yQgFnSc1,k3yIgFnSc1
má	mít	k5eAaImIp3nS
zástupce	zástupce	k1gMnSc4
v	v	k7c6
celostátním	celostátní	k2eAgInSc6d1
orgánu	orgán	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
108	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
komunálních	komunální	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
2014	#num#	k4
strana	strana	k1gFnSc1
uspěla	uspět	k5eAaPmAgFnS
v	v	k7c6
několika	několik	k4yIc6
obcích	obec	k1gFnPc6
a	a	k8xC
městských	městský	k2eAgFnPc6d1
částech	část	k1gFnPc6
<g/>
,	,	kIx,
zejména	zejména	k9
v	v	k7c6
Praze	Praha	k1gFnSc6
–	–	k?
kde	kde	k6eAd1
získala	získat	k5eAaPmAgFnS
4	#num#	k4
mandáty	mandát	k1gInPc4
v	v	k7c6
zastupitelstvu	zastupitelstvo	k1gNnSc6
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
a	a	k8xC
10	#num#	k4
zastupitelů	zastupitel	k1gMnPc2
v	v	k7c6
místních	místní	k2eAgFnPc6d1
radnicích	radnice	k1gFnPc6
–	–	k?
a	a	k8xC
v	v	k7c6
Mariánských	mariánský	k2eAgFnPc6d1
Lázních	lázeň	k1gFnPc6
<g/>
,	,	kIx,
kde	kde	k6eAd1
volby	volba	k1gFnPc4
vyhrála	vyhrát	k5eAaPmAgFnS
a	a	k8xC
obsadila	obsadit	k5eAaPmAgFnS
post	post	k1gInSc4
starosty	starosta	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgMnPc1d1
pirátští	pirátský	k2eAgMnPc1d1
zastupitelé	zastupitel	k1gMnPc1
byli	být	k5eAaImAgMnP
zvoleni	zvolit	k5eAaPmNgMnP
například	například	k6eAd1
v	v	k7c6
Brně	Brno	k1gNnSc6
na	na	k7c6
kandidátce	kandidátka	k1gFnSc6
Žít	žít	k5eAaImF
Brno	Brno	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Celkem	celkem	k6eAd1
bylo	být	k5eAaImAgNnS
na	na	k7c6
kandidátkách	kandidátka	k1gFnPc6
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
na	na	k7c6
koaličních	koaliční	k2eAgFnPc6d1
kandidátkách	kandidátka	k1gFnPc6
a	a	k8xC
na	na	k7c6
kandidátkách	kandidátka	k1gFnPc6
jiných	jiný	k2eAgFnPc2d1
stran	strana	k1gFnPc2
zvoleno	zvolit	k5eAaPmNgNnS
34	#num#	k4
zastupitelů	zastupitel	k1gMnPc2
nominovaných	nominovaný	k2eAgFnPc2d1
Pirátskou	pirátský	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
či	či	k8xC
jejích	její	k3xOp3gMnPc2
členů	člen	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
109	#num#	k4
<g/>
]	]	kIx)
Tyto	tento	k3xDgInPc1
úspěchy	úspěch	k1gInPc1
vzbudily	vzbudit	k5eAaPmAgInP
ohlas	ohlas	k1gInSc4
v	v	k7c6
domácích	domácí	k2eAgInPc6d1
médiích	médium	k1gNnPc6
i	i	k8xC
v	v	k7c6
mezinárodním	mezinárodní	k2eAgNnSc6d1
pirátském	pirátský	k2eAgNnSc6d1
hnutí	hnutí	k1gNnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
110	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
111	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vstup	vstup	k1gInSc1
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
roku	rok	k1gInSc2
2017	#num#	k4
</s>
<s>
Zleva	zleva	k6eAd1
<g/>
:	:	kIx,
poslanci	poslanec	k1gMnPc1
Tomáš	Tomáš	k1gMnSc1
Vymazal	Vymazal	k1gMnSc1
<g/>
,	,	kIx,
Lukáš	Lukáš	k1gMnSc1
Kolářík	Kolářík	k1gMnSc1
<g/>
,	,	kIx,
Ivan	Ivan	k1gMnSc1
Bartoš	Bartoš	k1gMnSc1
<g/>
,	,	kIx,
Dana	Dana	k1gFnSc1
Balcarová	Balcarová	k1gFnSc1
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
V	v	k7c6
parlamentních	parlamentní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
2017	#num#	k4
obdrželi	obdržet	k5eAaPmAgMnP
Piráti	pirát	k1gMnPc1
10,79	10,79	k4
%	%	kIx~
hlasů	hlas	k1gInPc2
<g/>
,	,	kIx,
umístili	umístit	k5eAaPmAgMnP
se	se	k3xPyFc4
tak	tak	k9
ve	v	k7c6
volbách	volba	k1gFnPc6
po	po	k7c6
ANO	ano	k9
a	a	k8xC
ODS	ODS	kA
na	na	k7c6
třetím	třetí	k4xOgNnSc6
místě	místo	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poprvé	poprvé	k6eAd1
se	se	k3xPyFc4
tak	tak	k6eAd1
dostali	dostat	k5eAaPmAgMnP
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
Parlamentu	parlament	k1gInSc2
ČR	ČR	kA
a	a	k8xC
to	ten	k3xDgNnSc1
s	s	k7c7
celkem	celkem	k6eAd1
22	#num#	k4
mandáty	mandát	k1gInPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
112	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
Praze	Praha	k1gFnSc6
se	se	k3xPyFc4
strana	strana	k1gFnSc1
umístila	umístit	k5eAaPmAgFnS
s	s	k7c7
18	#num#	k4
%	%	kIx~
na	na	k7c6
druhém	druhý	k4xOgInSc6
místě	místo	k1gNnSc6
<g/>
,	,	kIx,
nejméně	málo	k6eAd3
hlasů	hlas	k1gInPc2
strana	strana	k1gFnSc1
získala	získat	k5eAaPmAgFnS
v	v	k7c6
Mostě	most	k1gInSc6
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc4
7,10	7,10	k4
%	%	kIx~
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
113	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
celé	celý	k2eAgFnSc6d1
zemi	zem	k1gFnSc6
pro	pro	k7c4
tuto	tento	k3xDgFnSc4
stranu	strana	k1gFnSc4
hlasovalo	hlasovat	k5eAaImAgNnS
546	#num#	k4
393	#num#	k4
voličů	volič	k1gMnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
114	#num#	k4
<g/>
]	]	kIx)
Úspěch	úspěch	k1gInSc1
Pirátů	pirát	k1gMnPc2
ve	v	k7c6
volbách	volba	k1gFnPc6
se	se	k3xPyFc4
očekával	očekávat	k5eAaImAgInS
<g/>
,	,	kIx,
ale	ale	k8xC
zdaleka	zdaleka	k6eAd1
ne	ne	k9
tak	tak	k6eAd1
velký	velký	k2eAgInSc1d1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
115	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Symbolem	symbol	k1gInSc7
předcházející	předcházející	k2eAgFnSc2d1
Pirátské	pirátský	k2eAgFnSc2d1
kampaně	kampaň	k1gFnSc2
2017	#num#	k4
byl	být	k5eAaImAgInS
„	„	k?
<g/>
vězeňský	vězeňský	k2eAgInSc1d1
<g/>
“	“	k?
autobus	autobus	k1gInSc1
<g/>
,	,	kIx,
se	s	k7c7
kterým	který	k3yRgInSc7,k3yQgInSc7,k3yIgInSc7
kandidáti	kandidát	k1gMnPc1
projížděli	projíždět	k5eAaImAgMnP
všechna	všechen	k3xTgNnPc4
okresní	okresní	k2eAgNnPc4d1
města	město	k1gNnPc4
Česka	Česko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nátěr	nátěr	k1gInSc4
na	na	k7c6
něm	on	k3xPp3gMnSc6
připomínal	připomínat	k5eAaImAgInS
různé	různý	k2eAgFnPc4d1
české	český	k2eAgFnPc4d1
politické	politický	k2eAgFnPc4d1
kauzy	kauza	k1gFnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
116	#num#	k4
<g/>
]	]	kIx)
Na	na	k7c6
polepu	polep	k1gInSc6
byl	být	k5eAaImAgMnS
Andrej	Andrej	k1gMnSc1
Babiš	Babiš	k1gMnSc1
znázorňující	znázorňující	k2eAgFnSc4d1
kauzu	kauza	k1gFnSc4
Čapí	čapět	k5eAaImIp3nP
Hnízdo	hnízdo	k1gNnSc4
<g/>
,	,	kIx,
Davida	David	k1gMnSc2
Ratha	Rath	k1gMnSc2
<g/>
,	,	kIx,
podvodný	podvodný	k2eAgInSc1d1
prodej	prodej	k1gInSc1
OKD	OKD	kA
Bohuslava	Bohuslav	k1gMnSc2
Sobotky	Sobotka	k1gMnSc2
<g/>
,	,	kIx,
Miroslava	Miroslav	k1gMnSc2
Kalouska	Kalousek	k1gMnSc2
s	s	k7c7
kauzou	kauza	k1gFnSc7
nákupu	nákup	k1gInSc2
padáků	padák	k1gInPc2
a	a	k8xC
letadel	letadlo	k1gNnPc2
do	do	k7c2
Armády	armáda	k1gFnSc2
ČR	ČR	kA
a	a	k8xC
kauzu	kauza	k1gFnSc4
Nagyová	Nagyová	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
způsobila	způsobit	k5eAaPmAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2013	#num#	k4
pád	pád	k1gInSc1
české	český	k2eAgFnSc2d1
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mottem	motto	k1gNnSc7
celé	celá	k1gFnSc2
kampaně	kampaň	k1gFnSc2
byla	být	k5eAaImAgFnS
věta	věta	k1gFnSc1
„	„	k?
<g/>
Pusťte	pustit	k5eAaPmRp2nP
nás	my	k3xPp1nPc4
na	na	k7c4
ně	on	k3xPp3gMnPc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s>
Před	před	k7c7
volbami	volba	k1gFnPc7
podle	podle	k7c2
předsedy	předseda	k1gMnSc2
Ivana	Ivan	k1gMnSc2
Bartoše	Bartoš	k1gMnSc2
strana	strana	k1gFnSc1
plánovala	plánovat	k5eAaImAgFnS
zůstat	zůstat	k5eAaPmF
jedno	jeden	k4xCgNnSc1
až	až	k6eAd1
dvě	dva	k4xCgNnPc4
volební	volební	k2eAgNnPc4d1
období	období	k1gNnPc4
v	v	k7c6
opozici	opozice	k1gFnSc6
<g/>
,	,	kIx,
s	s	k7c7
výjimkou	výjimka	k1gFnSc7
situace	situace	k1gFnSc2
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
by	by	kYmCp3nS
se	se	k3xPyFc4
vstupu	vstup	k1gInSc3
do	do	k7c2
vlády	vláda	k1gFnSc2
nemohli	moct	k5eNaImAgMnP
vyhnout	vyhnout	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
117	#num#	k4
<g/>
]	]	kIx)
Pokud	pokud	k8xS
by	by	kYmCp3nS
volby	volba	k1gFnPc4
vyhrál	vyhrát	k5eAaPmAgInS
Babiš	Babiš	k1gInSc1
<g/>
,	,	kIx,
vylučovali	vylučovat	k5eAaImAgMnP
jakoukoliv	jakýkoliv	k3yIgFnSc4
jeho	jeho	k3xOp3gInSc6
podporu	podpora	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
117	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Strategie	strategie	k1gFnSc1
PSP	PSP	kA
po	po	k7c6
parlamentních	parlamentní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
2017	#num#	k4
byla	být	k5eAaImAgFnS
odhlasována	odhlasovat	k5eAaPmNgFnS
a	a	k8xC
zveřejněna	zveřejnit	k5eAaPmNgFnS
ještě	ještě	k6eAd1
před	před	k7c7
volbami	volba	k1gFnPc7
<g/>
,	,	kIx,
15	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zvítězil	zvítězit	k5eAaPmAgMnS
návrh	návrh	k1gInSc4
C	C	kA
<g/>
,	,	kIx,
což	což	k3yQnSc1,k3yRnSc1
byla	být	k5eAaImAgFnS
pozměněná	pozměněný	k2eAgFnSc1d1
verze	verze	k1gFnSc1
původního	původní	k2eAgInSc2d1
návrhu	návrh	k1gInSc2
A	A	kA
<g/>
,	,	kIx,
předloženého	předložený	k2eAgInSc2d1
republikovým	republikový	k2eAgInSc7d1
výborem	výbor	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Věta	věta	k1gFnSc1
<g/>
:	:	kIx,
V	v	k7c6
žádném	žádný	k3yNgInSc6
případě	případ	k1gInSc6
nepodpoříme	podpořit	k5eNaPmIp1nP
vládu	vláda	k1gFnSc4
s	s	k7c7
účastí	účast	k1gFnSc7
nedemokratických	demokratický	k2eNgFnPc2d1
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
<g/>
,	,	kIx,
například	například	k6eAd1
KSČM	KSČM	kA
<g/>
.	.	kIx.
byla	být	k5eAaImAgFnS
v	v	k7c6
návrhu	návrh	k1gInSc6
C	C	kA
nahrazena	nahradit	k5eAaPmNgFnS
větou	věta	k1gFnSc7
<g/>
:	:	kIx,
V	v	k7c6
žádném	žádný	k3yNgInSc6
případě	případ	k1gInSc6
nepodpoříme	podpořit	k5eNaPmIp1nP
vládu	vláda	k1gFnSc4
s	s	k7c7
účastí	účast	k1gFnSc7
subjektů	subjekt	k1gInPc2
ohrožujících	ohrožující	k2eAgInPc2d1
základy	základ	k1gInPc4
liberální	liberální	k2eAgFnSc2d1
demokracie	demokracie	k1gFnSc2
nebo	nebo	k8xC
s	s	k7c7
historickým	historický	k2eAgNnSc7d1
dědictvím	dědictví	k1gNnSc7
likvidace	likvidace	k1gFnSc2
demokracie	demokracie	k1gFnSc2
v	v	k7c6
této	tento	k3xDgFnSc6
zemi	zem	k1gFnSc6
<g/>
,	,	kIx,
tj.	tj.	kA
zejména	zejména	k9
s	s	k7c7
účastí	účast	k1gFnSc7
KSČM	KSČM	kA
<g/>
,	,	kIx,
SPD	SPD	kA
nebo	nebo	k8xC
s	s	k7c7
převahou	převaha	k1gFnSc7
ANO	ano	k9
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
118	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Piráti	pirát	k1gMnPc1
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
v	v	k7c6
Poslanecké	poslanecký	k2eAgFnSc6d1
sněmovně	sněmovna	k1gFnSc6
navrhli	navrhnout	k5eAaPmAgMnP
zavedení	zavedení	k1gNnSc4
jmenovitého	jmenovitý	k2eAgNnSc2d1
hlasování	hlasování	k1gNnSc2
v	v	k7c6
městských	městský	k2eAgFnPc6d1
radách	rada	k1gFnPc6
<g/>
,	,	kIx,
protože	protože	k8xS
podle	podle	k7c2
Ivana	Ivan	k1gMnSc2
Bartoše	Bartoš	k1gMnSc2
„	„	k?
<g/>
Lidé	člověk	k1gMnPc1
mají	mít	k5eAaImIp3nP
právo	právo	k1gNnSc4
vědět	vědět	k5eAaImF
i	i	k8xC
zpětně	zpětně	k6eAd1
si	se	k3xPyFc3
jednoduše	jednoduše	k6eAd1
dohledat	dohledat	k5eAaPmF
<g/>
,	,	kIx,
jak	jak	k8xS,k8xC
jejich	jejich	k3xOp3gMnPc1
zvolení	zvolený	k2eAgMnPc1d1
zastupitelé	zastupitel	k1gMnPc1
hlasovali	hlasovat	k5eAaImAgMnP
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
[	[	kIx(
<g/>
119	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vedení	vedení	k1gNnSc1
Prahy	Praha	k1gFnSc2
od	od	k7c2
roku	rok	k1gInSc2
2018	#num#	k4
a	a	k8xC
koalice	koalice	k1gFnSc1
ve	v	k7c6
velkých	velký	k2eAgNnPc6d1
městech	město	k1gNnPc6
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1
Hřib	hřib	k1gInSc1
(	(	kIx(
<g/>
vpředu	vpředu	k6eAd1
uprostřed	uprostřed	k7c2
<g/>
)	)	kIx)
byl	být	k5eAaImAgInS
zvolen	zvolit	k5eAaPmNgInS
primátorem	primátor	k1gMnSc7
Prahy	Praha	k1gFnSc2
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
</s>
<s>
Ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Senátu	senát	k1gInSc2
v	v	k7c6
říjnu	říjen	k1gInSc6
2018	#num#	k4
byl	být	k5eAaImAgInS
zvolen	zvolit	k5eAaPmNgMnS
Lukáš	Lukáš	k1gMnSc1
Wagenknecht	Wagenknecht	k1gMnSc1
senátorem	senátor	k1gMnSc7
za	za	k7c4
volební	volební	k2eAgInSc4d1
obvod	obvod	k1gInSc4
č.	č.	k?
23	#num#	k4
a	a	k8xC
současně	současně	k6eAd1
skončil	skončit	k5eAaPmAgMnS
mandát	mandát	k1gInSc4
v	v	k7c6
Senátu	senát	k1gInSc6
Liboru	Libor	k1gMnSc3
Michálkovi	Michálek	k1gMnSc3
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
120	#num#	k4
<g/>
]	]	kIx)
Wagenknecht	Wagenknecht	k1gInSc1
se	se	k3xPyFc4
tím	ten	k3xDgNnSc7
ve	v	k7c6
věku	věk	k1gInSc6
40	#num#	k4
let	léto	k1gNnPc2
stal	stát	k5eAaPmAgMnS
nejmladším	mladý	k2eAgMnSc7d3
zvoleným	zvolený	k2eAgMnSc7d1
senátorem	senátor	k1gMnSc7
v	v	k7c6
historii	historie	k1gFnSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
121	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
komunálních	komunální	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
2018	#num#	k4
strana	strana	k1gFnSc1
dosáhla	dosáhnout	k5eAaPmAgFnS
výrazného	výrazný	k2eAgInSc2d1
úspěchu	úspěch	k1gInSc2
ve	v	k7c6
většině	většina	k1gFnSc6
velkých	velký	k2eAgNnPc2d1
měst	město	k1gNnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Praze	Praha	k1gFnSc6
se	se	k3xPyFc4
umístila	umístit	k5eAaPmAgFnS
druhá	druhý	k4xOgFnSc1
s	s	k7c7
13	#num#	k4
mandáty	mandát	k1gInPc7
v	v	k7c6
zastupitelstvu	zastupitelstvo	k1gNnSc6
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
a	a	k8xC
v	v	k7c6
rámci	rámec	k1gInSc6
povolebních	povolební	k2eAgNnPc2d1
vyjednávání	vyjednávání	k1gNnPc2
s	s	k7c7
koaličními	koaliční	k2eAgMnPc7d1
partnery	partner	k1gMnPc7
Praha	Praha	k1gFnSc1
sobě	se	k3xPyFc3
a	a	k8xC
Spojené	spojený	k2eAgInPc1d1
síly	síl	k1gInPc1
pro	pro	k7c4
Prahu	Praha	k1gFnSc4
dosáhla	dosáhnout	k5eAaPmAgFnS
dohody	dohoda	k1gFnPc4
o	o	k7c4
obsazení	obsazení	k1gNnSc4
postu	post	k1gInSc2
primátora	primátor	k1gMnSc2
<g/>
,	,	kIx,
kterým	který	k3yQgMnSc7,k3yIgMnSc7,k3yRgMnSc7
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Zdeněk	Zdeněk	k1gMnSc1
Hřib	hřib	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vládnoucí	vládnoucí	k2eAgFnSc1d1
koalice	koalice	k1gFnSc1
následně	následně	k6eAd1
kontroluje	kontrolovat	k5eAaImIp3nS
39	#num#	k4
z	z	k7c2
65	#num#	k4
křesel	křeslo	k1gNnPc2
v	v	k7c6
Zastupitelstvu	zastupitelstvo	k1gNnSc6
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
122	#num#	k4
<g/>
]	]	kIx)
Do	do	k7c2
jedenáctičlenné	jedenáctičlenný	k2eAgFnSc2d1
Rady	rada	k1gFnSc2
Prahy	Praha	k1gFnSc2
za	za	k7c4
Piráty	pirát	k1gMnPc4
zasedají	zasedat	k5eAaImIp3nP
vedle	vedle	k7c2
Hřiba	hřib	k1gMnSc2
také	také	k9
Vít	Vít	k1gMnSc1
Šimral	šimrat	k5eAaImAgMnS
a	a	k8xC
zastupitel	zastupitel	k1gMnSc1
Adam	Adam	k1gMnSc1
Zábranský	Zábranský	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
123	#num#	k4
<g/>
]	]	kIx)
Strana	strana	k1gFnSc1
také	také	k9
obhájila	obhájit	k5eAaPmAgFnS
vedení	vedení	k1gNnSc4
Mariánských	mariánský	k2eAgFnPc2d1
Lázní	lázeň	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
124	#num#	k4
<g/>
]	]	kIx)
Hřib	hřib	k1gInSc1
posléze	posléze	k6eAd1
v	v	k7c6
Praze	Praha	k1gFnSc6
prosadil	prosadit	k5eAaPmAgInS
politiku	politika	k1gFnSc4
zmrazení	zmrazení	k1gNnSc2
vztahů	vztah	k1gInPc2
s	s	k7c7
pevninskou	pevninský	k2eAgFnSc7d1
Čínou	Čína	k1gFnSc7
a	a	k8xC
zlepšení	zlepšení	k1gNnSc2
spolupráce	spolupráce	k1gFnSc2
s	s	k7c7
Tchaj-wanem	Tchaj-wan	k1gInSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pirátští	pirátský	k2eAgMnPc1d1
zastupitelé	zastupitel	k1gMnPc1
se	se	k3xPyFc4
také	také	k6eAd1
stali	stát	k5eAaPmAgMnP
součástí	součást	k1gFnSc7
vládnoucích	vládnoucí	k2eAgFnPc2d1
koalic	koalice	k1gFnPc2
v	v	k7c6
městských	městský	k2eAgFnPc6d1
radách	rada	k1gFnPc6
Brna	Brno	k1gNnSc2
<g/>
[	[	kIx(
<g/>
125	#num#	k4
<g/>
]	]	kIx)
a	a	k8xC
Ostravy	Ostrava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
126	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vstup	vstup	k1gInSc1
do	do	k7c2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
roku	rok	k1gInSc2
2019	#num#	k4
</s>
<s>
Členové	člen	k1gMnPc1
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
Marcel	Marcel	k1gMnSc1
Kolaja	Kolaja	k1gMnSc1
a	a	k8xC
Markéta	Markéta	k1gFnSc1
Gregorová	Gregorová	k1gFnSc1
zvoleni	zvolit	k5eAaPmNgMnP
ve	v	k7c6
volbách	volba	k1gFnPc6
roku	rok	k1gInSc2
2019	#num#	k4
</s>
<s>
V	v	k7c6
lednu	leden	k1gInSc6
2019	#num#	k4
byl	být	k5eAaImAgMnS
lídrem	lídr	k1gMnSc7
kandidátky	kandidátka	k1gFnSc2
pro	pro	k7c4
volby	volba	k1gFnPc4
do	do	k7c2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
v	v	k7c6
květnu	květen	k1gInSc6
2019	#num#	k4
zvolen	zvolit	k5eAaPmNgMnS
Marcel	Marcel	k1gMnSc1
Kolaja	Kolaja	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
se	se	k3xPyFc4
zasazuje	zasazovat	k5eAaImIp3nS
o	o	k7c4
ochranu	ochrana	k1gFnSc4
spotřebitele	spotřebitel	k1gMnSc2
<g/>
,	,	kIx,
technologickou	technologický	k2eAgFnSc4d1
konkurenceschopnost	konkurenceschopnost	k1gFnSc4
Evropy	Evropa	k1gFnSc2
na	na	k7c6
světovém	světový	k2eAgInSc6d1
trhu	trh	k1gInSc6
<g/>
,	,	kIx,
boj	boj	k1gInSc1
proti	proti	k7c3
korporátnímu	korporátní	k2eAgNnSc3d1
lobbování	lobbování	k1gNnSc3
v	v	k7c6
EU	EU	kA
a	a	k8xC
předcházení	předcházení	k1gNnSc2
rostoucí	rostoucí	k2eAgFnSc6d1
cenzuře	cenzura	k1gFnSc6
internetu	internet	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
127	#num#	k4
<g/>
]	]	kIx)
Druhé	druhý	k4xOgInPc1
místo	místo	k7c2
kandidátky	kandidátka	k1gFnSc2
obsadila	obsadit	k5eAaPmAgFnS
předsedkyně	předsedkyně	k1gFnSc1
Evropské	evropský	k2eAgFnSc2d1
pirátské	pirátský	k2eAgFnSc2d1
strany	strana	k1gFnSc2
Markéta	Markéta	k1gFnSc1
Gregorová	Gregorová	k1gFnSc1
a	a	k8xC
třetí	třetí	k4xOgNnSc1
místo	místo	k1gNnSc1
poslanec	poslanec	k1gMnSc1
Mikuláš	Mikuláš	k1gMnSc1
Peksa	Peksa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
128	#num#	k4
<g/>
]	]	kIx)
Česká	český	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
vytvořila	vytvořit	k5eAaPmAgFnS
společný	společný	k2eAgInSc4d1
program	program	k1gInSc4
s	s	k7c7
pirátskými	pirátský	k2eAgFnPc7d1
stranami	strana	k1gFnPc7
v	v	k7c6
EU	EU	kA
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
byl	být	k5eAaImAgInS
podepsán	podepsat	k5eAaPmNgInS
v	v	k7c6
Lucembursku	Lucembursko	k1gNnSc6
v	v	k7c6
únoru	únor	k1gInSc6
2019	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
65	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
těchto	tento	k3xDgFnPc6
volbách	volba	k1gFnPc6
obdržela	obdržet	k5eAaPmAgFnS
strana	strana	k1gFnSc1
330	#num#	k4
844	#num#	k4
hlasů	hlas	k1gInPc2
(	(	kIx(
<g/>
13,95	13,95	k4
<g/>
%	%	kIx~
<g/>
)	)	kIx)
a	a	k8xC
získala	získat	k5eAaPmAgFnS
tak	tak	k6eAd1
tři	tři	k4xCgInPc4
mandáty	mandát	k1gInPc4
–	–	k?
jako	jako	k8xC,k8xS
noví	nový	k2eAgMnPc1d1
poslanci	poslanec	k1gMnPc1
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
byli	být	k5eAaImAgMnP
zvoleni	zvolit	k5eAaPmNgMnP
Kolaja	Kolaja	k1gMnSc1
<g/>
,	,	kIx,
Gregorová	Gregorová	k1gFnSc1
a	a	k8xC
Peksa	Peksa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
129	#num#	k4
<g/>
]	]	kIx)
Po	po	k7c6
volbách	volba	k1gFnPc6
čeští	český	k2eAgMnPc1d1
poslanci	poslanec	k1gMnPc1
Pirátů	pirát	k1gMnPc2
vstoupili	vstoupit	k5eAaPmAgMnP
do	do	k7c2
čtvrté	čtvrtá	k1gFnSc2
nejsilnější	silný	k2eAgFnSc2d3
frakce	frakce	k1gFnSc2
Zelení	zeleň	k1gFnPc2
/	/	kIx~
Evropská	evropský	k2eAgFnSc1d1
svobodná	svobodný	k2eAgFnSc1d1
aliance	aliance	k1gFnSc1
(	(	kIx(
<g/>
Zelení	zelení	k1gNnSc1
<g/>
/	/	kIx~
<g/>
ESA	eso	k1gNnPc4
<g/>
)	)	kIx)
spolu	spolu	k6eAd1
s	s	k7c7
nově	nově	k6eAd1
zvoleným	zvolený	k2eAgMnSc7d1
poslancem	poslanec	k1gMnSc7
Pirátské	pirátský	k2eAgFnSc2d1
strany	strana	k1gFnSc2
Německa	Německo	k1gNnSc2
Patrickem	Patrick	k1gInSc7
Breyerem	Breyer	k1gInSc7
a	a	k8xC
Kolaja	Kolaja	k1gMnSc1
byl	být	k5eAaImAgMnS
zvolen	zvolit	k5eAaPmNgMnS
místopředsedou	místopředseda	k1gMnSc7
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
130	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Krajské	krajský	k2eAgFnSc2d1
koalice	koalice	k1gFnSc2
od	od	k7c2
roku	rok	k1gInSc2
2020	#num#	k4
a	a	k8xC
senátní	senátní	k2eAgFnPc1d1
volby	volba	k1gFnPc1
</s>
<s>
Advokátka	advokátka	k1gFnSc1
Adéla	Adéla	k1gFnSc1
Šípová	Šípová	k1gFnSc1
<g/>
,	,	kIx,
senátorka	senátorka	k1gFnSc1
za	za	k7c4
obvod	obvod	k1gInSc4
č.	č.	k?
30	#num#	k4
–	–	k?
Kladno	Kladno	k1gNnSc1
od	od	k7c2
roku	rok	k1gInSc2
2020	#num#	k4
</s>
<s>
Celostátní	celostátní	k2eAgNnSc1d1
fórum	fórum	k1gNnSc1
strany	strana	k1gFnSc2
v	v	k7c6
Ostravě	Ostrava	k1gFnSc6
v	v	k7c6
lednu	leden	k1gInSc6
zvolilo	zvolit	k5eAaPmAgNnS
do	do	k7c2
vedení	vedení	k1gNnSc2
opět	opět	k6eAd1
Bartoše	Bartoš	k1gMnSc4
a	a	k8xC
strategii	strategie	k1gFnSc4
volebních	volební	k2eAgFnPc2d1
kampaní	kampaň	k1gFnPc2
dostal	dostat	k5eAaPmAgInS
na	na	k7c6
starosti	starost	k1gFnSc6
senátor	senátor	k1gMnSc1
Wagenknecht	Wagenknecht	k1gMnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
131	#num#	k4
<g/>
]	]	kIx)
Ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
Senátu	senát	k1gInSc2
uspěli	uspět	k5eAaPmAgMnP
za	za	k7c7
Piráty	pirát	k1gMnPc7
Adéla	Adéla	k1gFnSc1
Šípová	Šípová	k1gFnSc1
za	za	k7c4
senátní	senátní	k2eAgInSc4d1
obvod	obvod	k1gInSc4
č.	č.	k?
30	#num#	k4
–	–	k?
Kladno	Kladno	k1gNnSc1
a	a	k8xC
David	David	k1gMnSc1
Smoljak	Smoljak	k1gMnSc1
za	za	k7c4
senátní	senátní	k2eAgInSc4d1
obvod	obvod	k1gInSc4
č.	č.	k?
24	#num#	k4
–	–	k?
Praha	Praha	k1gFnSc1
9	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
132	#num#	k4
<g/>
]	]	kIx)
Šípová	Šípová	k1gFnSc1
svým	svůj	k3xOyFgNnSc7
zvolením	zvolení	k1gNnSc7
těsně	těsně	k6eAd1
překonala	překonat	k5eAaPmAgFnS
Wagenknechtův	Wagenknechtův	k2eAgInSc4d1
rekord	rekord	k1gInSc4
nejmladšího	mladý	k2eAgMnSc2d3
člena	člen	k1gMnSc2
Senátu	senát	k1gInSc2
také	také	k9
ve	v	k7c6
svých	svůj	k3xOyFgInPc6
40	#num#	k4
letech	léto	k1gNnPc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
121	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
zastupitelstev	zastupitelstvo	k1gNnPc2
krajů	kraj	k1gInPc2
v	v	k7c6
říjnu	říjen	k1gInSc6
2020	#num#	k4
získali	získat	k5eAaPmAgMnP
Piráti	pirát	k1gMnPc1
99	#num#	k4
ze	z	k7c2
675	#num#	k4
krajských	krajský	k2eAgNnPc2d1
zastupitelských	zastupitelský	k2eAgNnPc2d1
křesel	křeslo	k1gNnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
19	#num#	k4
<g/>
]	]	kIx)
Pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
po	po	k7c6
krajských	krajský	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
následně	následně	k6eAd1
vytvořila	vytvořit	k5eAaPmAgFnS
vládnoucí	vládnoucí	k2eAgFnSc1d1
koalice	koalice	k1gFnSc1
s	s	k7c7
jinými	jiný	k2eAgFnPc7d1
stranami	strana	k1gFnPc7
v	v	k7c6
devíti	devět	k4xCc2
ze	z	k7c2
třinácti	třináct	k4xCc2
volených	volený	k2eAgInPc2d1
krajů	kraj	k1gInPc2
<g/>
:	:	kIx,
Středočeský	středočeský	k2eAgInSc1d1
<g/>
,	,	kIx,
Plzeňský	plzeňský	k2eAgInSc1d1
<g/>
,	,	kIx,
Karlovarský	karlovarský	k2eAgInSc1d1
<g/>
,	,	kIx,
Liberecký	liberecký	k2eAgInSc1d1
<g/>
,	,	kIx,
Královéhradecký	královéhradecký	k2eAgInSc1d1
<g/>
,	,	kIx,
Vysočina	vysočina	k1gFnSc1
<g/>
,	,	kIx,
Jihomoravský	jihomoravský	k2eAgMnSc1d1
<g/>
,	,	kIx,
Olomoucký	olomoucký	k2eAgMnSc1d1
a	a	k8xC
Zlínský	zlínský	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedoucí	vedoucí	k1gMnSc1
kandidát	kandidát	k1gMnSc1
koalice	koalice	k1gFnSc2
Piráti	pirát	k1gMnPc1
a	a	k8xC
STAN	stan	k1gInSc1
v	v	k7c6
Olomouckém	olomoucký	k2eAgInSc6d1
kraji	kraj	k1gInSc6
Josef	Josef	k1gMnSc1
Suchánek	Suchánek	k1gMnSc1
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
hejtmanem	hejtman	k1gMnSc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
133	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
134	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
135	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
V	v	k7c6
říjnu	říjen	k1gInSc6
hnutí	hnutí	k1gNnSc2
Starostové	Starostová	k1gFnSc2
a	a	k8xC
nezávislí	závislý	k2eNgMnPc1d1
(	(	kIx(
<g/>
STAN	stan	k1gInSc4
<g/>
)	)	kIx)
oznámilo	oznámit	k5eAaPmAgNnS
zájem	zájem	k1gInSc4
o	o	k7c6
vytvoření	vytvoření	k1gNnSc6
koalice	koalice	k1gFnSc2
dvou	dva	k4xCgMnPc2
rovnoprávných	rovnoprávný	k2eAgMnPc2d1
partnerů	partner	k1gMnPc2
pro	pro	k7c4
parlamentní	parlamentní	k2eAgFnPc4d1
volby	volba	k1gFnPc4
v	v	k7c6
roce	rok	k1gInSc6
2021	#num#	k4
s	s	k7c7
Piráty	pirát	k1gMnPc7
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
136	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
listopadu	listopad	k1gInSc6
Piráti	pirát	k1gMnPc1
ve	v	k7c6
stranickém	stranický	k2eAgNnSc6d1
hlasování	hlasování	k1gNnSc6
odsouhlasili	odsouhlasit	k5eAaPmAgMnP
zahájení	zahájení	k1gNnSc3
vyjednávání	vyjednávání	k1gNnSc2
o	o	k7c6
vytvoření	vytvoření	k1gNnSc6
jednorázové	jednorázový	k2eAgFnSc2d1
předvolební	předvolební	k2eAgFnSc2d1
koalice	koalice	k1gFnSc2
se	se	k3xPyFc4
STAN	stan	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
137	#num#	k4
<g/>
]	]	kIx)
Lídrem	lídr	k1gMnSc7
koalice	koalice	k1gFnSc1
Piráti	pirát	k1gMnPc1
+	+	kIx~
STAN	stan	k1gInSc1
do	do	k7c2
sněmovních	sněmovní	k2eAgFnPc2d1
voleb	volba	k1gFnPc2
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Ivan	Ivan	k1gMnSc1
Bartoš	Bartoš	k1gMnSc1
v	v	k7c6
prosinci	prosinec	k1gInSc6
2020	#num#	k4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
138	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
lednu	leden	k1gInSc6
2021	#num#	k4
strana	strana	k1gFnSc1
schválila	schválit	k5eAaPmAgFnS
hlasováním	hlasování	k1gNnSc7
předvolební	předvolební	k2eAgFnSc6d1
koalici	koalice	k1gFnSc6
se	se	k3xPyFc4
STAN	stan	k1gInSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
139	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Účast	účast	k1gFnSc1
ve	v	k7c6
volbách	volba	k1gFnPc6
</s>
<s>
Parlament	parlament	k1gInSc1
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Poslanecký	poslanecký	k2eAgInSc1d1
klub	klub	k1gInSc1
České	český	k2eAgFnSc2d1
pirátské	pirátský	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Mapa	mapa	k1gFnSc1
voličů	volič	k1gMnPc2
Pirátů	pirát	k1gMnPc2
v	v	k7c6
Česku	Česko	k1gNnSc6
u	u	k7c2
sněmovních	sněmovní	k2eAgFnPc2d1
voleb	volba	k1gFnPc2
2017	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
se	se	k3xPyFc4
strana	strana	k1gFnSc1
poprvé	poprvé	k6eAd1
dostala	dostat	k5eAaPmAgFnS
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
Parlamentu	parlament	k1gInSc2
ČR	ČR	kA
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Číslo	číslo	k1gNnSc1
</s>
<s>
Kandidátů	kandidát	k1gMnPc2
</s>
<s>
Hlasů	hlas	k1gInPc2
</s>
<s>
Mandáty	mandát	k1gInPc1
</s>
<s>
Státní	státní	k2eAgInSc1d1
příspěvek	příspěvek	k1gInSc1
</s>
<s>
celkem	celkem	k6eAd1
</s>
<s>
v	v	k7c6
%	%	kIx~
</s>
<s>
celkem	celkem	k6eAd1
</s>
<s>
změna	změna	k1gFnSc1
</s>
<s>
úhrada	úhrada	k1gFnSc1
nákladů	náklad	k1gInPc2
za	za	k7c4
hlasy	hlas	k1gInPc4
</s>
<s>
stálý	stálý	k2eAgInSc1d1
na	na	k7c4
činnost	činnost	k1gFnSc4
ročně	ročně	k6eAd1
</s>
<s>
2010	#num#	k4
<g/>
[	[	kIx(
<g/>
140	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2314042	#num#	k4
3230,80	3230,80	k4
%	%	kIx~
<g/>
0	#num#	k4
<g/>
▬	▬	k?
<g/>
0	#num#	k4
Kč	Kč	kA
<g/>
0	#num#	k4
Kč	Kč	kA
</s>
<s>
2013	#num#	k4
<g/>
[	[	kIx(
<g/>
141	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
3192132	#num#	k4
4172,66	4172,66	k4
%	%	kIx~
<g/>
0	#num#	k4
<g/>
▬	▬	k?
<g/>
13	#num#	k4
241	#num#	k4
700	#num#	k4
Kč	Kč	kA
<g/>
0	#num#	k4
Kč	Kč	kA
</s>
<s>
2017	#num#	k4
<g/>
[	[	kIx(
<g/>
142	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
15200546	#num#	k4
39310,79	39310,79	k4
%	%	kIx~
<g/>
22	#num#	k4
<g/>
+	+	kIx~
<g/>
22	#num#	k4
▲	▲	k?
<g/>
54	#num#	k4
639	#num#	k4
300	#num#	k4
Kč	Kč	kA
<g/>
[	[	kIx(
<g/>
143	#num#	k4
<g/>
]	]	kIx)
<g/>
4	#num#	k4
950	#num#	k4
000	#num#	k4
Kč	Kč	kA
<g/>
[	[	kIx(
<g/>
143	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2021	#num#	k4
</s>
<s>
bude	být	k5eAaImBp3nS
oznámenobude	oznámenobude	k6eAd1
oznámenobude	oznámenobude	k6eAd1
oznámenobude	oznámenobude	k6eAd1
oznámenobude	oznámenobude	k6eAd1
oznámenobude	oznámenobude	k6eAd1
oznámenobude	oznámenobude	k6eAd1
oznámenobude	oznámenobude	k6eAd1
oznámeno	oznámit	k5eAaPmNgNnS
</s>
<s>
Prominentní	prominentní	k2eAgMnPc1d1
členové	člen	k1gMnPc1
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
</s>
<s>
Ivan	Ivan	k1gMnSc1
Bartoš	Bartoš	k1gMnSc1
<g/>
,	,	kIx,
předseda	předseda	k1gMnSc1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
předseda	předseda	k1gMnSc1
Výboru	výbor	k1gInSc2
pro	pro	k7c4
veřejnou	veřejný	k2eAgFnSc4d1
správu	správa	k1gFnSc4
a	a	k8xC
regionální	regionální	k2eAgInSc4d1
rozvoj	rozvoj	k1gInSc4
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
<g/>
[	[	kIx(
<g/>
144	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Olga	Olga	k1gFnSc1
Richterová	Richterová	k1gFnSc1
<g/>
,	,	kIx,
1	#num#	k4
<g/>
.	.	kIx.
místopředsedkyně	místopředsedkyně	k1gFnSc1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
místopředsedkyně	místopředsedkyně	k1gFnSc1
Výboru	výbor	k1gInSc2
pro	pro	k7c4
sociální	sociální	k2eAgFnSc4d1
politiku	politika	k1gFnSc4
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
<g/>
[	[	kIx(
<g/>
145	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vojtěch	Vojtěch	k1gMnSc1
Pikal	pikat	k5eAaImAgMnS
<g/>
,	,	kIx,
2	#num#	k4
<g/>
.	.	kIx.
místopředseda	místopředseda	k1gMnSc1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
místopředseda	místopředseda	k1gMnSc1
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
<g/>
[	[	kIx(
<g/>
146	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Radek	Radek	k1gMnSc1
Holomčík	Holomčík	k1gMnSc1
<g/>
,	,	kIx,
3	#num#	k4
<g/>
.	.	kIx.
místopředseda	místopředseda	k1gMnSc1
strany	strana	k1gFnSc2
<g/>
[	[	kIx(
<g/>
147	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Jakub	Jakub	k1gMnSc1
Michálek	Michálek	k1gMnSc1
<g/>
,	,	kIx,
předseda	předseda	k1gMnSc1
poslaneckého	poslanecký	k2eAgInSc2d1
klubu	klub	k1gInSc2
Pirátů	pirát	k1gMnPc2
<g/>
[	[	kIx(
<g/>
148	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dana	Dana	k1gFnSc1
Balcarová	Balcarová	k1gFnSc1
<g/>
,	,	kIx,
předsedkyně	předsedkyně	k1gFnSc1
Výboru	výbor	k1gInSc2
pro	pro	k7c4
životní	životní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
<g/>
[	[	kIx(
<g/>
149	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Mikuláš	Mikuláš	k1gMnSc1
Ferjenčík	Ferjenčík	k1gMnSc1
<g/>
,	,	kIx,
vedoucí	vedoucí	k1gMnSc1
mediálního	mediální	k2eAgInSc2d1
odboru	odbor	k1gInSc2
Pirátů	pirát	k1gMnPc2
<g/>
[	[	kIx(
<g/>
150	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Senát	senát	k1gInSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	naleznout	k5eAaPmIp2nP,k5eAaBmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Senátorský	senátorský	k2eAgInSc1d1
klub	klub	k1gInSc1
SEN	sen	k1gInSc1
21	#num#	k4
a	a	k8xC
Piráti	pirát	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Kandidátů	kandidát	k1gMnPc2
</s>
<s>
Hlasů	hlas	k1gInPc2
<g/>
[	[	kIx(
<g/>
p	p	k?
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
výsledek	výsledek	k1gInSc4
</s>
<s>
Vráceno	vrátit	k5eAaPmNgNnS
kaucí	kauce	k1gFnSc7
</s>
<s>
Mandáty	mandát	k1gInPc1
</s>
<s>
Státní	státní	k2eAgInSc1d1
příspěvek	příspěvek	k1gInSc1
ročně	ročně	k6eAd1
</s>
<s>
Kandidát	kandidát	k1gMnSc1
</s>
<s>
obvod	obvod	k1gInSc1
</s>
<s>
hlasů	hlas	k1gInPc2
</s>
<s>
%	%	kIx~
</s>
<s>
získáno	získán	k2eAgNnSc1d1
</s>
<s>
změna	změna	k1gFnSc1
</s>
<s>
celkem	celkem	k6eAd1
</s>
<s>
2010	#num#	k4
<g/>
[	[	kIx(
<g/>
151	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
131	#num#	k4
</s>
<s>
Pavel	Pavel	k1gMnSc1
Přeučil	přeučit	k5eAaPmAgMnS
</s>
<s>
č.	č.	k?
19	#num#	k4
</s>
<s>
1	#num#	k4
131	#num#	k4
</s>
<s>
2,77	2,77	k4
<g/>
%	%	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
-	-	kIx~
▬	▬	k?
</s>
<s>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
0	#num#	k4
Kč	Kč	kA
</s>
<s>
2011	#num#	k4
<g/>
[	[	kIx(
<g/>
152	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1	#num#	k4
</s>
<s>
205	#num#	k4
</s>
<s>
Ivo	Ivo	k1gMnSc1
Vašíček	Vašíček	k1gMnSc1
</s>
<s>
č.	č.	k?
30	#num#	k4
</s>
<s>
205	#num#	k4
</s>
<s>
0,75	0,75	k4
<g/>
%	%	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
▬	▬	k?
</s>
<s>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
-	-	kIx~
</s>
<s>
2012	#num#	k4
<g/>
[	[	kIx(
<g/>
153	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
3	#num#	k4
</s>
<s>
7	#num#	k4
947	#num#	k4
</s>
<s>
Libor	Libor	k1gMnSc1
Michálek	Michálek	k1gMnSc1
</s>
<s>
č.	č.	k?
26	#num#	k4
</s>
<s>
5	#num#	k4
520	#num#	k4
</s>
<s>
24,31	24,31	k4
<g/>
%	%	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
+1	+1	k4
▲	▲	k?
</s>
<s>
0	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
285	#num#	k4
000	#num#	k4
Kč	Kč	kA
<g/>
[	[	kIx(
<g/>
p	p	k?
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2014	#num#	k4
<g/>
[	[	kIx(
<g/>
154	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1	#num#	k4
</s>
<s>
359	#num#	k4
</s>
<s>
Luděk	Luděk	k1gMnSc1
Maděra	Maděra	k1gMnSc1
</s>
<s>
č.	č.	k?
80	#num#	k4
</s>
<s>
359	#num#	k4
</s>
<s>
1,56	1,56	k4
<g/>
%	%	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
▬	▬	k?
</s>
<s>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
-	-	kIx~
</s>
<s>
2014	#num#	k4
<g/>
[	[	kIx(
<g/>
155	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
4	#num#	k4
<g/>
[	[	kIx(
<g/>
156	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
p	p	k?
4	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
5	#num#	k4
454	#num#	k4
</s>
<s>
Libor	Libor	k1gMnSc1
Špaček	Špaček	k1gMnSc1
</s>
<s>
č.	č.	k?
3	#num#	k4
</s>
<s>
1	#num#	k4
821	#num#	k4
</s>
<s>
5,88	5,88	k4
<g/>
%	%	kIx~
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
▬	▬	k?
</s>
<s>
1	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
285	#num#	k4
000	#num#	k4
Kč	Kč	kA
<g/>
[	[	kIx(
<g/>
p	p	k?
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2016	#num#	k4
<g/>
[	[	kIx(
<g/>
157	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
7	#num#	k4
<g/>
[	[	kIx(
<g/>
158	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
p	p	k?
5	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
p	p	k?
6	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Renata	Renata	k1gFnSc1
Chmelová	Chmelová	k1gFnSc1
</s>
<s>
č.	č.	k?
22	#num#	k4
</s>
<s>
5	#num#	k4
459	#num#	k4
</s>
<s>
22,8	22,8	k4
<g/>
%	%	kIx~
</s>
<s>
1	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
+2	+2	k4
▲	▲	k?
</s>
<s>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
285	#num#	k4
000	#num#	k4
Kč	Kč	kA
<g/>
[	[	kIx(
<g/>
p	p	k?
3	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2018	#num#	k4
<g/>
[	[	kIx(
<g/>
159	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
13	#num#	k4
<g/>
[	[	kIx(
<g/>
160	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
63,132	63,132	k4
</s>
<s>
Lukáš	Lukáš	k1gMnSc1
Wagenknecht	Wagenknecht	k1gMnSc1
</s>
<s>
č.	č.	k?
23	#num#	k4
</s>
<s>
8	#num#	k4
230	#num#	k4
</s>
<s>
18,15	18,15	k4
<g/>
%	%	kIx~
</s>
<s>
12	#num#	k4
</s>
<s>
1	#num#	k4
</s>
<s>
0	#num#	k4
▬	▬	k?
</s>
<s>
3	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
900	#num#	k4
000	#num#	k4
Kč	Kč	kA
<g/>
[	[	kIx(
<g/>
161	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2020	#num#	k4
<g/>
[	[	kIx(
<g/>
162	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
13	#num#	k4
<g/>
[	[	kIx(
<g/>
163	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
62,583	62,583	k4
</s>
<s>
Daniel	Daniel	k1gMnSc1
Kůs	Kůs	k1gMnSc1
</s>
<s>
č.	č.	k?
9	#num#	k4
</s>
<s>
8	#num#	k4
032	#num#	k4
</s>
<s>
21,33	21,33	k4
<g/>
%	%	kIx~
</s>
<s>
10	#num#	k4
</s>
<s>
2	#num#	k4
</s>
<s>
+2	+2	k4
▲	▲	k?
</s>
<s>
5	#num#	k4
<g/>
/	/	kIx~
<g/>
81	#num#	k4
</s>
<s>
1	#num#	k4
800	#num#	k4
000	#num#	k4
Kč	Kč	kA
<g/>
[	[	kIx(
<g/>
164	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Senátoři	senátor	k1gMnPc1
nominovaní	nominovaný	k2eAgMnPc1d1
Pirátskou	pirátský	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
</s>
<s>
Adéla	Adéla	k1gFnSc1
Šípová	Šípová	k1gFnSc1
<g/>
,	,	kIx,
senátorka	senátorka	k1gFnSc1
za	za	k7c4
Piráty	pirát	k1gMnPc4
od	od	k7c2
2020	#num#	k4
(	(	kIx(
<g/>
obvod	obvod	k1gInSc1
č.	č.	k?
30	#num#	k4
–	–	k?
Kladno	Kladno	k1gNnSc1
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
165	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
David	David	k1gMnSc1
Smoljak	Smoljak	k1gMnSc1
<g/>
,	,	kIx,
senátor	senátor	k1gMnSc1
podporovaný	podporovaný	k2eAgMnSc1d1
Piráty	pirát	k1gMnPc4
od	od	k7c2
2020	#num#	k4
(	(	kIx(
<g/>
Senátní	senátní	k2eAgInSc1d1
obvod	obvod	k1gInSc1
č.	č.	k?
24	#num#	k4
–	–	k?
Praha	Praha	k1gFnSc1
9	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Lukáš	Lukáš	k1gMnSc1
Wagenknecht	Wagenknecht	k1gMnSc1
<g/>
,	,	kIx,
senátor	senátor	k1gMnSc1
za	za	k7c4
Piráty	pirát	k1gMnPc4
od	od	k7c2
2018	#num#	k4
(	(	kIx(
<g/>
obvod	obvod	k1gInSc1
č.	č.	k?
23	#num#	k4
–	–	k?
Praha	Praha	k1gFnSc1
8	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
166	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Ladislav	Ladislav	k1gMnSc1
Kos	Kos	k1gMnSc1
<g/>
,	,	kIx,
senátor	senátor	k1gMnSc1
podporovaný	podporovaný	k2eAgMnSc1d1
Piráty	pirát	k1gMnPc4
od	od	k7c2
2016	#num#	k4
(	(	kIx(
<g/>
obvod	obvod	k1gInSc1
č.	č.	k?
19	#num#	k4
–	–	k?
Praha	Praha	k1gFnSc1
11	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
167	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Renata	Renata	k1gFnSc1
Chmelová	Chmelová	k1gFnSc1
<g/>
,	,	kIx,
senátorka	senátorka	k1gFnSc1
podporovaná	podporovaný	k2eAgFnSc1d1
Piráty	pirát	k1gMnPc4
od	od	k7c2
2016	#num#	k4
(	(	kIx(
<g/>
obvod	obvod	k1gInSc1
č.	č.	k?
22	#num#	k4
–	–	k?
Praha	Praha	k1gFnSc1
10	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
168	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Evropský	evropský	k2eAgInSc1d1
parlament	parlament	k1gInSc1
</s>
<s>
Související	související	k2eAgFnPc1d1
informace	informace	k1gFnPc1
naleznete	nalézt	k5eAaBmIp2nP,k5eAaPmIp2nP
také	také	k9
v	v	k7c6
článku	článek	k1gInSc6
Evropská	evropský	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Číslo	číslo	k1gNnSc1
</s>
<s>
Kandidátů	kandidát	k1gMnPc2
</s>
<s>
Hlasů	hlas	k1gInPc2
</s>
<s>
Mandáty	mandát	k1gInPc1
</s>
<s>
Státní	státní	k2eAgInSc1d1
příspěvek	příspěvek	k1gInSc1
</s>
<s>
Parlamentnífrakce	Parlamentnífrakce	k1gFnSc1
</s>
<s>
celkem	celkem	k6eAd1
</s>
<s>
v	v	k7c6
%	%	kIx~
</s>
<s>
získáno	získán	k2eAgNnSc1d1
</s>
<s>
změna	změna	k1gFnSc1
</s>
<s>
2014	#num#	k4
<g/>
[	[	kIx(
<g/>
169	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
32	#num#	k4
</s>
<s>
27	#num#	k4
</s>
<s>
72	#num#	k4
514	#num#	k4
</s>
<s>
4,78	4,78	k4
</s>
<s>
0	#num#	k4
</s>
<s>
▬	▬	k?
</s>
<s>
2	#num#	k4
175	#num#	k4
420	#num#	k4
Kč	Kč	kA
</s>
<s>
–	–	k?
</s>
<s>
2019	#num#	k4
<g/>
[	[	kIx(
<g/>
170	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
27	#num#	k4
</s>
<s>
28	#num#	k4
</s>
<s>
330	#num#	k4
844	#num#	k4
</s>
<s>
13,95	13,95	k4
</s>
<s>
3	#num#	k4
</s>
<s>
+3	+3	k4
▲	▲	k?
</s>
<s>
9	#num#	k4
925	#num#	k4
320	#num#	k4
Kč	Kč	kA
</s>
<s>
Zelení	zelení	k1gNnSc1
/	/	kIx~
Evropská	evropský	k2eAgFnSc1d1
svobodná	svobodný	k2eAgFnSc1d1
aliance	aliance	k1gFnSc1
</s>
<s>
Zvolení	zvolený	k2eAgMnPc1d1
kandidáti	kandidát	k1gMnPc1
voleb	volba	k1gFnPc2
do	do	k7c2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
2019	#num#	k4
</s>
<s>
Marcel	Marcel	k1gMnSc1
Kolaja	Kolaja	k1gMnSc1
<g/>
,	,	kIx,
místopředseda	místopředseda	k1gMnSc1
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
</s>
<s>
Markéta	Markéta	k1gFnSc1
Gregorová	Gregorová	k1gFnSc1
<g/>
,	,	kIx,
europoslankyně	europoslankyně	k1gFnSc1
</s>
<s>
Mikuláš	Mikuláš	k1gMnSc1
Peksa	Peksa	k1gFnSc1
<g/>
,	,	kIx,
europoslanec	europoslanec	k1gMnSc1
a	a	k8xC
předseda	předseda	k1gMnSc1
Evropské	evropský	k2eAgFnSc2d1
pirátské	pirátský	k2eAgFnSc2d1
strany	strana	k1gFnSc2
</s>
<s>
Zastupitelstva	zastupitelstvo	k1gNnPc1
krajů	kraj	k1gInPc2
</s>
<s>
Strana	strana	k1gFnSc1
se	se	k3xPyFc4
účastní	účastnit	k5eAaImIp3nS
krajských	krajský	k2eAgFnPc2d1
voleb	volba	k1gFnPc2
ve	v	k7c6
všech	všecek	k3xTgInPc6
krajích	kraj	k1gInPc6
<g/>
.	.	kIx.
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Číslo	číslo	k1gNnSc1
</s>
<s>
Kandidátů	kandidát	k1gMnPc2
</s>
<s>
Hlasů	hlas	k1gInPc2
</s>
<s>
Nejlepší	dobrý	k2eAgInSc4d3
výsledek	výsledek	k1gInSc4
</s>
<s>
Mandáty	mandát	k1gInPc1
</s>
<s>
Státní	státní	k2eAgInSc1d1
příspěvek	příspěvek	k1gInSc1
ročně	ročně	k6eAd1
</s>
<s>
celkem	celkem	k6eAd1
</s>
<s>
v	v	k7c6
%	%	kIx~
</s>
<s>
kraj	kraj	k1gInSc1
</s>
<s>
hlasů	hlas	k1gInPc2
</s>
<s>
%	%	kIx~
</s>
<s>
získáno	získán	k2eAgNnSc1d1
</s>
<s>
změna	změna	k1gFnSc1
</s>
<s>
2012	#num#	k4
<g/>
[	[	kIx(
<g/>
171	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
93	#num#	k4
</s>
<s>
172	#num#	k4
</s>
<s>
57	#num#	k4
805	#num#	k4
</s>
<s>
2,19	2,19	k4
</s>
<s>
Středočeský	středočeský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
10	#num#	k4
617	#num#	k4
</s>
<s>
3,03	3,03	k4
</s>
<s>
0	#num#	k4
</s>
<s>
▬	▬	k?
</s>
<s>
0	#num#	k4
Kč	Kč	kA
</s>
<s>
2016	#num#	k4
<g/>
[	[	kIx(
<g/>
172	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
82	#num#	k4
(	(	kIx(
<g/>
6	#num#	k4
<g/>
,	,	kIx,
21	#num#	k4
<g/>
,	,	kIx,
22	#num#	k4
<g/>
,	,	kIx,
56	#num#	k4
<g/>
,	,	kIx,
76	#num#	k4
<g/>
,	,	kIx,
77	#num#	k4
<g/>
)	)	kIx)
<g/>
[	[	kIx(
<g/>
173	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
371	#num#	k4
<g/>
[	[	kIx(
<g/>
174	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
44	#num#	k4
0	#num#	k4
<g/>
70	#num#	k4
<g/>
[	[	kIx(
<g/>
p	p	k?
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1,74	1,74	k4
<g/>
[	[	kIx(
<g/>
p	p	k?
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Karlovarský	karlovarský	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
3	#num#	k4
835	#num#	k4
</s>
<s>
5,46	5,46	k4
</s>
<s>
5	#num#	k4
</s>
<s>
+5	+5	k4
<g/>
▲	▲	k?
</s>
<s>
1	#num#	k4
250	#num#	k4
000	#num#	k4
Kč	Kč	kA
<g/>
[	[	kIx(
<g/>
175	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
2020	#num#	k4
<g/>
[	[	kIx(
<g/>
176	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
19	#num#	k4
(	(	kIx(
<g/>
Olomoucký	olomoucký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
21	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
699	#num#	k4
<g/>
[	[	kIx(
<g/>
177	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
333	#num#	k4
153	#num#	k4
<g/>
[	[	kIx(
<g/>
p	p	k?
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
12,02	12,02	k4
<g/>
[	[	kIx(
<g/>
p	p	k?
7	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Olomoucký	olomoucký	k2eAgInSc1d1
kraj	kraj	k1gInSc1
</s>
<s>
36	#num#	k4
549	#num#	k4
</s>
<s>
19,51	19,51	k4
</s>
<s>
99	#num#	k4
</s>
<s>
+94	+94	k4
▲	▲	k?
</s>
<s>
24	#num#	k4
750	#num#	k4
000	#num#	k4
Kč	Kč	kA
<g/>
[	[	kIx(
<g/>
175	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Zastupitelstva	zastupitelstvo	k1gNnPc1
obcí	obec	k1gFnPc2
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Kandidátů	kandidát	k1gMnPc2
</s>
<s>
Hlasů	hlas	k1gInPc2
</s>
<s>
Mandáty	mandát	k1gInPc1
</s>
<s>
celkem	celkem	k6eAd1
</s>
<s>
v	v	k7c6
%	%	kIx~
</s>
<s>
celkem	celkem	k6eAd1
</s>
<s>
změna	změna	k1gFnSc1
</s>
<s>
2010	#num#	k4
<g/>
[	[	kIx(
<g/>
178	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
408	#num#	k4
</s>
<s>
189	#num#	k4
360	#num#	k4
<g/>
[	[	kIx(
<g/>
179	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
0,21	0,21	k4
</s>
<s>
3	#num#	k4
</s>
<s>
+3	+3	k4
▲	▲	k?
</s>
<s>
2014	#num#	k4
<g/>
[	[	kIx(
<g/>
180	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
733	#num#	k4
</s>
<s>
1	#num#	k4
321	#num#	k4
908	#num#	k4
</s>
<s>
1,33	1,33	k4
</s>
<s>
21	#num#	k4
<g/>
[	[	kIx(
<g/>
p	p	k?
8	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
+18	+18	k4
▲	▲	k?
</s>
<s>
2018	#num#	k4
<g/>
[	[	kIx(
<g/>
181	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
1	#num#	k4
814	#num#	k4
</s>
<s>
8	#num#	k4
410	#num#	k4
203	#num#	k4
</s>
<s>
7,48	7,48	k4
</s>
<s>
358	#num#	k4
</s>
<s>
+337	+337	k4
▲	▲	k?
</s>
<s>
Zastupitelstvo	zastupitelstvo	k1gNnSc1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
</s>
<s>
Rok	rok	k1gInSc1
</s>
<s>
Kandidátů	kandidát	k1gMnPc2
</s>
<s>
Hlasů	hlas	k1gInPc2
</s>
<s>
Mandáty	mandát	k1gInPc1
</s>
<s>
celkem	celkem	k6eAd1
</s>
<s>
v	v	k7c6
%	%	kIx~
</s>
<s>
celkem	celkem	k6eAd1
</s>
<s>
změna	změna	k1gFnSc1
</s>
<s>
2010	#num#	k4
<g/>
[	[	kIx(
<g/>
182	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
63	#num#	k4
</s>
<s>
32	#num#	k4
901	#num#	k4
</s>
<s>
0,95	0,95	k4
</s>
<s>
0	#num#	k4
</s>
<s>
0	#num#	k4
</s>
<s>
2014	#num#	k4
<g/>
[	[	kIx(
<g/>
183	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
65	#num#	k4
</s>
<s>
1	#num#	k4
101	#num#	k4
081	#num#	k4
</s>
<s>
5,31	5,31	k4
</s>
<s>
4	#num#	k4
</s>
<s>
+4	+4	k4
▲	▲	k?
</s>
<s>
2018	#num#	k4
<g/>
[	[	kIx(
<g/>
184	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
65	#num#	k4
</s>
<s>
4	#num#	k4
326	#num#	k4
041	#num#	k4
</s>
<s>
17,07	17,07	k4
</s>
<s>
13	#num#	k4
</s>
<s>
+9	+9	k4
▲	▲	k?
</s>
<s>
Členové	člen	k1gMnPc1
Rady	rada	k1gFnSc2
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
</s>
<s>
Zdeněk	Zdeněk	k1gMnSc1
Hřib	hřib	k1gInSc1
<g/>
,	,	kIx,
Primátor	primátor	k1gMnSc1
hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
</s>
<s>
Adam	Adam	k1gMnSc1
Zábranský	Zábranský	k1gMnSc1
<g/>
,	,	kIx,
radní	radní	k1gMnSc1
Hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
</s>
<s>
Vít	Vít	k1gMnSc1
Šimral	šimrat	k5eAaImAgMnS
<g/>
,	,	kIx,
radní	radní	k1gMnSc1
Hlavního	hlavní	k2eAgNnSc2d1
města	město	k1gNnSc2
Prahy	Praha	k1gFnSc2
</s>
<s>
Republikové	republikový	k2eAgNnSc1d1
předsednictvo	předsednictvo	k1gNnSc1
</s>
<s>
Republikové	republikový	k2eAgNnSc1d1
předsednictvo	předsednictvo	k1gNnSc1
17	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2020	#num#	k4
zleva	zleva	k6eAd1
<g/>
:	:	kIx,
Holomčík	Holomčík	k1gMnSc1
<g/>
,	,	kIx,
Bartoš	Bartoš	k1gMnSc1
<g/>
,	,	kIx,
Richterová	Richterová	k1gFnSc1
<g/>
,	,	kIx,
Pikal	pikat	k5eAaImAgMnS
<g/>
,	,	kIx,
Kučera	Kučera	k1gMnSc1
</s>
<s>
Současné	současný	k2eAgNnSc1d1
</s>
<s>
předseda	předseda	k1gMnSc1
<g/>
:	:	kIx,
Ivan	Ivan	k1gMnSc1
Bartoš	Bartoš	k1gMnSc1
<g/>
,	,	kIx,
poslanec	poslanec	k1gMnSc1
PČR	PČR	kA
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místopředsedkyně	místopředsedkyně	k1gFnSc1
<g/>
:	:	kIx,
Olga	Olga	k1gFnSc1
Richterová	Richterová	k1gFnSc1
<g/>
,	,	kIx,
poslankyně	poslankyně	k1gFnSc1
PČR	PČR	kA
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místopředseda	místopředseda	k1gMnSc1
<g/>
:	:	kIx,
Vojtěch	Vojtěch	k1gMnSc1
Pikal	pikat	k5eAaImAgMnS
<g/>
,	,	kIx,
místopředseda	místopředseda	k1gMnSc1
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
PČR	PČR	kA
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místopředseda	místopředseda	k1gMnSc1
<g/>
:	:	kIx,
Radek	Radek	k1gMnSc1
Holomčík	Holomčík	k1gMnSc1
<g/>
,	,	kIx,
poslanec	poslanec	k1gMnSc1
PČR	PČR	kA
<g/>
,	,	kIx,
zastupitel	zastupitel	k1gMnSc1
města	město	k1gNnSc2
Strážnice	Strážnice	k1gFnSc2
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
místopředseda	místopředseda	k1gMnSc1
<g/>
:	:	kIx,
Martin	Martin	k1gMnSc1
Kučera	Kučera	k1gMnSc1
<g/>
,	,	kIx,
zastupitel	zastupitel	k1gMnSc1
městského	městský	k2eAgInSc2d1
obvodu	obvod	k1gInSc2
Moravská	moravský	k2eAgFnSc1d1
Ostrava	Ostrava	k1gFnSc1
a	a	k8xC
Přívoz	přívoz	k1gInSc1
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Předseda	předseda	k1gMnSc1
<g/>
:	:	kIx,
</s>
<s>
Kamil	Kamil	k1gMnSc1
Horký	Horký	k1gMnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ivan	Ivan	k1gMnSc1
Bartoš	Bartoš	k1gMnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Lukáš	Lukáš	k1gMnSc1
Černohorský	Černohorský	k1gMnSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
–	–	k?
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ivan	Ivan	k1gMnSc1
Bartoš	Bartoš	k1gMnSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
místopředseda	místopředseda	k1gMnSc1
<g/>
:	:	kIx,
</s>
<s>
Jiří	Jiří	k1gMnSc1
Kadeřávek	Kadeřávek	k1gMnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Kamil	Kamil	k1gMnSc1
Horký	Horký	k1gMnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Adam	Adam	k1gMnSc1
Šoukal	Šoukal	k1gMnSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
–	–	k?
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jakub	Jakub	k1gMnSc1
Michálek	Michálek	k1gMnSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Lenka	Lenka	k1gFnSc1
Matoušková	Matoušková	k1gFnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vojtěch	Vojtěch	k1gMnSc1
Pikal	pikat	k5eAaImAgMnS
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
–	–	k?
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Olga	Olga	k1gFnSc1
Richterová	Richterová	k1gFnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
2	#num#	k4
<g/>
.	.	kIx.
místopředseda	místopředseda	k1gMnSc1
<g/>
:	:	kIx,
</s>
<s>
Anežka	Anežka	k1gFnSc1
Bubeníčková	Bubeníčková	k1gFnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Michal	Michal	k1gMnSc1
Zadražil	Zadražil	k1gMnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Robert	Robert	k1gMnSc1
Adámek	Adámek	k1gMnSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
Rezek	Rezek	k1gMnSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Mikuláš	Mikuláš	k1gMnSc1
Ferjenčík	Ferjenčík	k1gMnSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
–	–	k?
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Lenka	Lenka	k1gFnSc1
Wagnerová	Wagnerová	k1gFnSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Mikuláš	Mikuláš	k1gMnSc1
Ferjenčík	Ferjenčík	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Michal	Michal	k1gMnSc1
Havránek	Havránek	k1gMnSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Dominika	Dominik	k1gMnSc4
Michailidu	Michailid	k1gInSc2
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
–	–	k?
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Martin	Martin	k1gMnSc1
Šmída	Šmíd	k1gMnSc2
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
–	–	k?
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Radek	Radek	k1gMnSc1
Holomčík	Holomčík	k1gMnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
–	–	k?
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Vojtěch	Vojtěch	k1gMnSc1
Pikal	pikat	k5eAaImAgMnS
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
3	#num#	k4
<g/>
.	.	kIx.
místopředseda	místopředseda	k1gMnSc1
<g/>
:	:	kIx,
</s>
<s>
Ivan	Ivan	k1gMnSc1
Bartoš	Bartoš	k1gMnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Robert	Robert	k1gMnSc1
Adámek	Adámek	k1gMnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
Rezek	Rezek	k1gMnSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Mikuláš	Mikuláš	k1gMnSc1
Ferjenčík	Ferjenčík	k1gMnSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ivo	Ivo	k1gMnSc1
Vašíček	Vašíček	k1gMnSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
–	–	k?
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Mikuláš	Mikuláš	k1gMnSc1
Ferjenčík	Ferjenčík	k1gMnSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ivo	Ivo	k1gMnSc1
Vašíček	Vašíček	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jana	Jana	k1gFnSc1
Michailidu	Michailid	k1gInSc2
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ivo	Ivo	k1gMnSc1
Vašíček	Vašíček	k1gMnSc1
(	(	kIx(
<g/>
2014	#num#	k4
–	–	k?
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Václav	Václav	k1gMnSc1
Fořtík	fořtík	k1gMnSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
–	–	k?
<g/>
.2016	.2016	k4
<g/>
)	)	kIx)
</s>
<s>
Jakub	Jakub	k1gMnSc1
Michálek	Michálek	k1gMnSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
–	–	k?
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Radek	Radek	k1gMnSc1
Holomčík	Holomčík	k1gMnSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
4	#num#	k4
<g/>
.	.	kIx.
místopředseda	místopředseda	k1gMnSc1
<g/>
:	:	kIx,
</s>
<s>
Ondřej	Ondřej	k1gMnSc1
Profant	profant	k1gInSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
Rezek	Rezek	k1gMnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Mikuláš	Mikuláš	k1gMnSc1
Ferjenčík	Ferjenčík	k1gMnSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jiří	Jiří	k1gMnSc1
Kadeřávek	Kadeřávek	k1gMnSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ingrid	Ingrid	k1gFnSc1
Romancová	Romancový	k2eAgFnSc1d1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Martin	Martin	k1gMnSc1
Brož	Brož	k1gMnSc1
(	(	kIx(
<g/>
2010	#num#	k4
<g/>
–	–	k?
<g/>
2012	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Marcel	Marcel	k1gMnSc1
Kolaja	Kolaja	k1gMnSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ivo	Ivo	k1gMnSc1
Vašíček	Vašíček	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Vymazal	Vymazal	k1gMnSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
–	–	k?
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ivo	Ivo	k1gMnSc1
Vašíček	Vašíček	k1gMnSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
–	–	k?
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Mikuláš	Mikuláš	k1gMnSc1
Peksa	Peks	k1gMnSc2
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
–	–	k?
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Martin	Martin	k1gMnSc1
Kučera	Kučera	k1gMnSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
–	–	k?
<g/>
dosud	dosud	k6eAd1
<g/>
)	)	kIx)
</s>
<s>
Kritika	kritika	k1gFnSc1
</s>
<s>
Odpor	odpor	k1gInSc1
svazů	svaz	k1gInPc2
nakladatelů	nakladatel	k1gMnPc2
a	a	k8xC
autorů	autor	k1gMnPc2
vyvolalo	vyvolat	k5eAaPmAgNnS
<g/>
,	,	kIx,
když	když	k8xS
Pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
zveřejnila	zveřejnit	k5eAaPmAgFnS
článek	článek	k1gInSc4
proti	proti	k7c3
připravované	připravovaný	k2eAgFnSc3d1
novele	novela	k1gFnSc3
autorského	autorský	k2eAgInSc2d1
zákona	zákon	k1gInSc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
185	#num#	k4
<g/>
]	]	kIx)
Organizace	organizace	k1gFnSc1
OSA	osa	k1gFnSc1
vydala	vydat	k5eAaPmAgFnS
před	před	k7c7
komunálními	komunální	k2eAgFnPc7d1
volbami	volba	k1gFnPc7
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
tiskovou	tiskový	k2eAgFnSc4d1
zprávu	zpráva	k1gFnSc4
kritizující	kritizující	k2eAgFnSc4d1
pirátskou	pirátský	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
186	#num#	k4
<g/>
]	]	kIx)
Jiří	Jiří	k1gMnSc1
Srstka	srstka	k1gFnSc1
z	z	k7c2
organizace	organizace	k1gFnSc2
DILIA	DILIA	kA
pak	pak	k8xC
Piráty	pirát	k1gMnPc7
kritizoval	kritizovat	k5eAaImAgInS
<g/>
,	,	kIx,
protože	protože	k8xS
se	se	k3xPyFc4
nepodařilo	podařit	k5eNaPmAgNnS
novelu	novela	k1gFnSc4
prosadit	prosadit	k5eAaPmF
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
187	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Bývalý	bývalý	k2eAgMnSc1d1
předseda	předseda	k1gMnSc1
TOP	topit	k5eAaImRp2nS
09	#num#	k4
Miroslav	Miroslav	k1gMnSc1
Kalousek	Kalousek	k1gMnSc1
<g/>
,	,	kIx,
bývalý	bývalý	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
obrany	obrana	k1gFnSc2
Alexandr	Alexandr	k1gMnSc1
Vondra	Vondra	k1gMnSc1
a	a	k8xC
Jakub	Jakub	k1gMnSc1
Janda	Janda	k1gMnSc1
z	z	k7c2
think-tanku	think-tanka	k1gFnSc4
Evropské	evropský	k2eAgFnSc2d1
hodnoty	hodnota	k1gFnSc2
kritizovali	kritizovat	k5eAaImAgMnP
Pirátskou	pirátský	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
za	za	k7c4
její	její	k3xOp3gInSc4
kritický	kritický	k2eAgInSc4d1
postoj	postoj	k1gInSc4
vůči	vůči	k7c3
NATO	NATO	kA
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
51	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
188	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
189	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Pražský	pražský	k2eAgMnSc1d1
primátor	primátor	k1gMnSc1
Zdeněk	Zdeněk	k1gMnSc1
Hřib	hřib	k1gInSc4
byl	být	k5eAaImAgMnS
kritizován	kritizovat	k5eAaImNgMnS
opozicí	opozice	k1gFnSc7
i	i	k8xC
některými	některý	k3yIgMnPc7
členy	člen	k1gMnPc7
vlastní	vlastní	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
,	,	kIx,
když	když	k8xS
20	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2019	#num#	k4
umožnil	umožnit	k5eAaPmAgInS
tajné	tajný	k2eAgNnSc4d1
hlasování	hlasování	k1gNnSc4
o	o	k7c6
geologickém	geologický	k2eAgInSc6d1
průzkumu	průzkum	k1gInSc6
linky	linka	k1gFnSc2
metra	metro	k1gNnSc2
D	D	kA
za	za	k7c4
1,5	1,5	k4
miliardy	miliarda	k4xCgFnSc2
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
190	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
191	#num#	k4
<g/>
]	]	kIx)
Primátor	primátor	k1gMnSc1
Hřib	hřib	k1gInSc4
společně	společně	k6eAd1
s	s	k7c7
radními	radní	k1gFnPc7
z	z	k7c2
Praha	Praha	k1gFnSc1
Sobě	se	k3xPyFc3
a	a	k8xC
TOP	topit	k5eAaImRp2nS
09	#num#	k4
hlasoval	hlasovat	k5eAaImAgMnS
pro	pro	k7c4
usnesení	usnesení	k1gNnSc4
<g/>
,	,	kIx,
kterým	který	k3yQgNnSc7,k3yRgNnSc7,k3yIgNnSc7
bylo	být	k5eAaImAgNnS
umožněno	umožnit	k5eAaPmNgNnS
tajné	tajný	k2eAgNnSc1d1
hlasování	hlasování	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
119	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Poznámky	poznámka	k1gFnPc1
</s>
<s>
↑	↑	k?
Ve	v	k7c6
funkčním	funkční	k2eAgNnSc6d1
období	období	k1gNnSc6
2020	#num#	k4
<g/>
–	–	k?
<g/>
2022	#num#	k4
zasedá	zasedat	k5eAaImIp3nS
v	v	k7c6
horní	horní	k2eAgFnSc6d1
komoře	komora	k1gFnSc6
pět	pět	k4xCc4
senátorů	senátor	k1gMnPc2
zvolených	zvolený	k2eAgMnPc2d1
za	za	k7c4
Piráty	pirát	k1gMnPc4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
tři	tři	k4xCgInPc4
na	na	k7c6
koaliční	koaliční	k2eAgFnSc6d1
kandidátce	kandidátka	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lukáš	Lukáš	k1gMnSc1
Wagenknecht	Wagenknecht	k1gMnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Adéla	Adéla	k1gFnSc1
Šípová	Šípová	k1gFnSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
získali	získat	k5eAaPmAgMnP
mandát	mandát	k1gInSc4
za	za	k7c4
volební	volební	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
Piráti	pirát	k1gMnPc1
<g/>
,	,	kIx,
David	David	k1gMnSc1
Smoljak	Smoljak	k1gMnSc1
za	za	k7c4
koalici	koalice	k1gFnSc4
STAN	stan	k1gInSc1
<g/>
,	,	kIx,
Piráti	pirát	k1gMnPc1
a	a	k8xC
TOP	topit	k5eAaImRp2nS
09	#num#	k4
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Ladislav	Ladislav	k1gMnSc1
Kos	Kos	k1gMnSc1
za	za	k7c4
koalici	koalice	k1gFnSc4
HPP	HPP	kA
11	#num#	k4
<g/>
,	,	kIx,
KDU-ČSL	KDU-ČSL	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
<g/>
,	,	kIx,
Zelení	Zelený	k1gMnPc1
<g/>
,	,	kIx,
Piráti	pirát	k1gMnPc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
Renata	Renata	k1gFnSc1
Chmelová	chmelový	k2eAgFnSc1d1
za	za	k7c4
koalici	koalice	k1gFnSc4
KDU-ČSL	KDU-ČSL	k1gMnPc1
<g/>
,	,	kIx,
Piráti	pirát	k1gMnPc1
<g/>
,	,	kIx,
DPD	DPD	kA
<g/>
,	,	kIx,
LES	les	k1gInSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Celkový	celkový	k2eAgInSc1d1
počet	počet	k1gInSc1
hlasů	hlas	k1gInPc2
pro	pro	k7c4
navržené	navržený	k2eAgMnPc4d1
kandidáty	kandidát	k1gMnPc4
strany	strana	k1gFnSc2
v	v	k7c6
1	#num#	k4
<g/>
.	.	kIx.
kole	kolo	k1gNnSc6
<g/>
1	#num#	k4
2	#num#	k4
<g />
.	.	kIx.
</s>
<s hack="1">
3	#num#	k4
Ze	z	k7c2
stálého	stálý	k2eAgInSc2d1
příspěvku	příspěvek	k1gInSc2
855	#num#	k4
000	#num#	k4
Kč	Kč	kA
za	za	k7c4
mandát	mandát	k1gInSc4
senátora	senátor	k1gMnSc2
strana	strana	k1gFnSc1
pravidelně	pravidelně	k6eAd1
odvádí	odvádět	k5eAaImIp3nS
dle	dle	k7c2
koaliční	koaliční	k2eAgFnSc2d1
smlouvy	smlouva	k1gFnSc2
2	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
koaličním	koaliční	k2eAgMnSc7d1
partnerům	partner	k1gMnPc3
SZ	SZ	kA
a	a	k8xC
KDU-ČSL	KDU-ČSL	k1gFnSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kromě	kromě	k7c2
čtyř	čtyři	k4xCgInPc2
vlastních	vlastní	k2eAgMnPc2d1
kandidátů	kandidát	k1gMnPc2
strana	strana	k1gFnSc1
různým	různý	k2eAgInPc3d1
způsobem	způsob	k1gInSc7
podpořila	podpořit	k5eAaPmAgNnP
dalších	další	k2eAgMnPc2d1
5	#num#	k4
kandidátů	kandidát	k1gMnPc2
<g/>
,	,	kIx,
jmenovitě	jmenovitě	k6eAd1
Miroslavu	Miroslava	k1gFnSc4
Pošvářovou	Pošvářův	k2eAgFnSc7d1
<g/>
,	,	kIx,
Pavla	Pavel	k1gMnSc4
Křížka	Křížek	k1gMnSc4
<g/>
,	,	kIx,
Česlavu	Česlava	k1gFnSc4
Lukaštíkovou	Lukaštíkův	k2eAgFnSc7d1
<g/>
,	,	kIx,
Václava	Václav	k1gMnSc4
Lásku	láska	k1gFnSc4
a	a	k8xC
Jitku	Jitka	k1gFnSc4
Seitlovou	Seitlový	k2eAgFnSc4d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kandidáti	kandidát	k1gMnPc1
Láska	láska	k1gFnSc1
a	a	k8xC
Seitlová	Seitlová	k1gFnSc1
postoupili	postoupit	k5eAaPmAgMnP
do	do	k7c2
2	#num#	k4
<g/>
.	.	kIx.
kola	kolo	k1gNnSc2
a	a	k8xC
v	v	k7c6
něm	on	k3xPp3gMnSc6
byli	být	k5eAaImAgMnP
nakonec	nakonec	k6eAd1
zvoleni	zvolit	k5eAaPmNgMnP
senátory	senátor	k1gMnPc4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Kromě	kromě	k7c2
čtyř	čtyři	k4xCgMnPc2
řádně	řádně	k6eAd1
zaregistrovaných	zaregistrovaný	k2eAgMnPc2d1
kandidátů	kandidát	k1gMnPc2
strana	strana	k1gFnSc1
nominovala	nominovat	k5eAaBmAgFnS
Lukáše	Lukáš	k1gMnSc4
Černohorského	Černohorský	k1gMnSc4
<g/>
,	,	kIx,
který	který	k3yQgMnSc1,k3yIgMnSc1,k3yRgMnSc1
byl	být	k5eAaImAgMnS
pro	pro	k7c4
nízký	nízký	k2eAgInSc4d1
věk	věk	k1gInSc4
odmítnut	odmítnout	k5eAaPmNgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strana	strana	k1gFnSc1
proti	proti	k7c3
tomuto	tento	k3xDgNnSc3
rozhodnutí	rozhodnutí	k1gNnSc3
plánuje	plánovat	k5eAaImIp3nS
podat	podat	k5eAaPmF
ústavní	ústavní	k2eAgFnSc1d1
stížnost	stížnost	k1gFnSc1
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Strana	strana	k1gFnSc1
dále	daleko	k6eAd2
podpořila	podpořit	k5eAaPmAgFnS
tři	tři	k4xCgMnPc4
další	další	k2eAgMnPc4d1
kandidáty	kandidát	k1gMnPc4
v	v	k7c6
různých	různý	k2eAgFnPc6d1
koalicích	koalice	k1gFnPc6
<g/>
.1	.1	k4
2	#num#	k4
3	#num#	k4
4	#num#	k4
Nezahrnuje	zahrnovat	k5eNaImIp3nS
hlasy	hlas	k1gInPc4
z	z	k7c2
koalic	koalice	k1gFnPc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
V	v	k7c6
roce	rok	k1gInSc6
2014	#num#	k4
bylo	být	k5eAaImAgNnS
ve	v	k7c6
volbách	volba	k1gFnPc6
do	do	k7c2
zastupitelstev	zastupitelstvo	k1gNnPc2
zvoleno	zvolit	k5eAaPmNgNnS
21	#num#	k4
zastupitelů	zastupitel	k1gMnPc2
navržených	navržený	k2eAgInPc2d1
Českou	český	k2eAgFnSc7d1
Pirátskou	pirátský	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dalších	další	k2eAgMnPc2d1
8	#num#	k4
zastupitelů	zastupitel	k1gMnPc2
navržených	navržený	k2eAgMnPc2d1
stranou	strana	k1gFnSc7
bylo	být	k5eAaImAgNnS
zvoleno	zvolit	k5eAaPmNgNnS
do	do	k7c2
zastupitelstev	zastupitelstvo	k1gNnPc2
městských	městský	k2eAgFnPc2d1
částí	část	k1gFnPc2
nebo	nebo	k8xC
obvodů	obvod	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
SEDLÁK	Sedlák	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
;	;	kIx,
ČÍŽEK	Čížek	k1gMnSc1
<g/>
,	,	kIx,
Jakub	Jakub	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
Živě	živě	k6eAd1
<g/>
:	:	kIx,
Česká	český	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Živě	živě	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CZECH	CZECH	kA
NEWS	NEWS	kA
CENTER	centrum	k1gNnPc2
<g/>
,	,	kIx,
2009-04-26	2009-04-26	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Roč	ročit	k5eAaImRp2nS
<g/>
.	.	kIx.
0	#num#	k4
<g/>
9	#num#	k4
<g/>
,	,	kIx,
čís	čís	k3xOyIgInSc1wB
<g/>
.	.	kIx.
15	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Kontaktní	kontaktní	k2eAgFnPc4d1
informace	informace	k1gFnPc4
<g/>
.	.	kIx.
pirati	pirat	k5eAaImF,k5eAaBmF,k5eAaPmF
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
1	#num#	k4
2	#num#	k4
TVRDOŇ	Tvrdoň	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pokud	pokud	k8xS
se	se	k3xPyFc4
výrazně	výrazně	k6eAd1
nezmění	změnit	k5eNaPmIp3nS
<g/>
,	,	kIx,
narazili	narazit	k5eAaPmAgMnP
Piráti	pirát	k1gMnPc1
na	na	k7c4
svůj	svůj	k3xOyFgInSc4
strop	strop	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
krajských	krajský	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
mají	mít	k5eAaImIp3nP
hendikep	hendikep	k1gInSc4
<g/>
,	,	kIx,
říká	říkat	k5eAaImIp3nS
politolog	politolog	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
N	N	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2020-01-11	2020-01-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
„	„	k?
<g/>
Piráti	pirát	k1gMnPc1
v	v	k7c6
české	český	k2eAgFnSc6d1
politice	politika	k1gFnSc6
reprezentují	reprezentovat	k5eAaImIp3nP
určitý	určitý	k2eAgInSc4d1
druh	druh	k1gInSc4
liberální	liberální	k2eAgFnSc2d1
strany	strana	k1gFnSc2
zaměřené	zaměřený	k2eAgNnSc1d1
hlavně	hlavně	k9
na	na	k7c6
městské	městský	k2eAgFnSc6d1
a	a	k8xC
velkoměstské	velkoměstský	k2eAgFnSc6d1
<g/>
,	,	kIx,
mnohdy	mnohdy	k6eAd1
vysokoškolsky	vysokoškolsky	k6eAd1
vzdělané	vzdělaný	k2eAgMnPc4d1
a	a	k8xC
mladší	mladý	k2eAgMnPc4d2
voliče	volič	k1gMnPc4
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
3	#num#	k4
TAIT	TAIT	kA
<g/>
,	,	kIx,
Robert	Robert	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zdeněk	Zdeněk	k1gMnSc1
Hřib	hřib	k1gInSc1
<g/>
:	:	kIx,
the	the	k?
Czech	Czech	k1gMnSc1
mayor	mayor	k1gMnSc1
who	who	k?
defied	defied	k1gInSc1
China	China	k1gFnSc1
<g/>
.	.	kIx.
the	the	k?
Guardian	Guardian	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2019-07-03	2019-07-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
„	„	k?
<g/>
Pirate	Pirat	k1gInSc5
party	parta	k1gFnPc4
<g/>
,	,	kIx,
a	a	k8xC
liberal	liberat	k5eAaPmAgMnS,k5eAaImAgMnS
group	group	k1gMnSc1
with	with	k1gMnSc1
roots	roots	k6eAd1
in	in	k?
civil	civil	k1gMnSc1
society	societa	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
BLICH	BLICH	kA
<g/>
,	,	kIx,
Slawek	Slawek	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Finally	Finallo	k1gNnPc7
a	a	k8xC
healthy	health	k1gInPc7
dose	dos	k1gInSc2
of	of	k?
anti-establishment	anti-establishment	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Political	Political	k1gFnSc1
Critique	Critique	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2018-01-08	2018-01-08	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
„	„	k?
<g/>
In	In	k1gFnSc1
the	the	k?
Pirate	Pirat	k1gInSc5
Party	parta	k1gFnPc1
<g/>
,	,	kIx,
we	we	k?
are	ar	k1gInSc5
progressive	progressiev	k1gFnPc1
liberals	liberals	k1gInSc1
and	and	k?
we	we	k?
respect	respect	k1gMnSc1
personal	personat	k5eAaPmAgMnS,k5eAaImAgMnS
freedoms	freedoms	k6eAd1
<g/>
,	,	kIx,
unlike	unlike	k6eAd1
ruling	ruling	k1gInSc1
parties	parties	k1gInSc1
in	in	k?
Poland	Poland	k1gInSc1
or	or	k?
Hungary	Hungara	k1gFnSc2
<g/>
.	.	kIx.
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ŠÁROVEC	ŠÁROVEC	kA
<g/>
,	,	kIx,
Daniel	Daniel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Assured	Assured	k1gInSc1
Newcomers	Newcomers	k1gInSc4
on	on	k3xPp3gMnSc1
a	a	k8xC
Squally	Squall	k1gInPc4
Sea	Sea	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
Czech	Czech	k1gInSc1
Pirate	Pirat	k1gInSc5
Party	part	k1gInPc1
Before	Befor	k1gInSc5
and	and	k?
After	After	k1gInSc1
Elections	Elections	k1gInSc1
2017	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
European	Europeana	k1gFnPc2
Consortium	Consortium	k1gNnSc4
for	forum	k1gNnPc2
Political	Political	k1gFnPc2
Research	Researcha	k1gFnPc2
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
Archivováno	archivovat	k5eAaBmNgNnS
29	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
<g/>
↑	↑	k?
Stanovy	stanova	k1gFnSc2
České	český	k2eAgFnSc2d1
pirátské	pirátský	k2eAgFnPc4d1
strany	strana	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
2013-09-07	2013-09-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
17	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
In	In	k1gMnSc1
the	the	k?
Czech	Czech	k1gInSc1
Republic	Republice	k1gFnPc2
<g/>
,	,	kIx,
almost	almost	k1gFnSc1
everyone	everyon	k1gInSc5
ran	rána	k1gFnPc2
against	against	k1gInSc1
the	the	k?
system	systo	k1gNnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
The	The	k1gMnSc1
Economist	Economist	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
28	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
mor	mor	k1gInSc1
<g/>
;	;	kIx,
mkk	mkk	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předsedou	předseda	k1gMnSc7
Pirátů	pirát	k1gMnPc2
je	být	k5eAaImIp3nS
znovu	znovu	k6eAd1
Ivan	Ivan	k1gMnSc1
Bartoš	Bartoš	k1gMnSc1
<g/>
,	,	kIx,
věří	věřit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
mohou	moct	k5eAaImIp3nP
porazit	porazit	k5eAaPmF
ANO	ano	k9
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
2018-01-06	2018-01-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Předsedou	předseda	k1gMnSc7
Pirátské	pirátský	k2eAgFnSc2d1
strany	strana	k1gFnSc2
byl	být	k5eAaImAgInS
zvolen	zvolit	k5eAaPmNgMnS
Ivan	Ivan	k1gMnSc1
Bartoš	Bartoš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2009-10-25	2009-10-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
16	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
z	z	k7c2
originálu	originál	k1gInSc6
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1213	#num#	k4
<g/>
-	-	kIx~
<g/>
1385	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ZEZULKOVÁ	ZEZULKOVÁ	kA
<g/>
,	,	kIx,
Barbora	Barbora	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Analýza	analýza	k1gFnSc1
České	český	k2eAgFnSc2d1
pirátské	pirátský	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
,	,	kIx,
2019	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Bakalářská	bakalářský	k2eAgFnSc1d1
práce	práce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Univerzita	univerzita	k1gFnSc1
Hradec	Hradec	k1gInSc1
Králové	Králová	k1gFnSc2
<g/>
,	,	kIx,
Filozofická	filozofický	k2eAgFnSc1d1
fakulta	fakulta	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vedoucí	vedoucí	k1gFnSc1
práce	práce	k1gFnSc2
prof.	prof.	kA
PhDr.	PhDr.	kA
Maxmilián	Maxmiliána	k1gFnPc2
Strmiska	Strmiska	k1gFnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
HINSHAW	HINSHAW	kA
<g/>
,	,	kIx,
Drew	Drew	k1gFnSc1
<g/>
;	;	kIx,
HEIJMANS	HEIJMANS	kA
<g/>
,	,	kIx,
Philip	Philip	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Upstart	Upstart	k1gInSc1
Pirate	Pirat	k1gInSc5
Party	party	k1gFnPc2
Remixes	Remixes	k1gInSc1
Czech	Czech	k1gMnSc1
Politics	Politics	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2017-10-11	2017-10-11	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
anglicky	anglicky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
https://www.mfcr.cz/cs/verejny-sektor/podpora-z-narodnich-zdroju/politicke-strany-a-hnuti/prispevky-ze-statniho-rozpoctu-uhrazene-37083	https://www.mfcr.cz/cs/verejny-sektor/podpora-z-narodnich-zdroju/politicke-strany-a-hnuti/prispevky-ze-statniho-rozpoctu-uhrazene-37083	k4
<g/>
↑	↑	k?
http://www.piratskelisty.cz/	http://www.piratskelisty.cz/	k?
<g/>
↑	↑	k?
https://lide.pirati.cz/	https://lide.pirati.cz/	k?
<g/>
↑	↑	k?
Grafický	grafický	k2eAgInSc1d1
manuál	manuál	k1gInSc1
<g/>
:	:	kIx,
Barevná	barevný	k2eAgFnSc1d1
paleta	paleta	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
2012-10-01	2012-10-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2013	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Volby	volba	k1gFnPc1
do	do	k7c2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
2019	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSÚ	ČSÚ	kA
na	na	k7c4
volby	volba	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
2017	#num#	k4
<g/>
,	,	kIx,
volby	volba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
1	#num#	k4
2	#num#	k4
Kandidáti	kandidát	k1gMnPc1
dle	dle	k7c2
navrhující	navrhující	k2eAgFnSc2d1
strany	strana	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSÚ	ČSÚ	kA
na	na	k7c4
volby	volba	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ire	ire	k?
<g/>
;	;	kIx,
ket	ket	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předsedou	předseda	k1gMnSc7
europarlamentu	europarlament	k1gInSc2
zvolen	zvolen	k2eAgMnSc1d1
Ital	Ital	k1gMnSc1
Sassoli	Sassole	k1gFnSc4
<g/>
,	,	kIx,
Charanzová	Charanzová	k1gFnSc1
a	a	k8xC
Kolaja	Kolaja	k1gMnSc1
budou	být	k5eAaImBp3nP
místopředsedy	místopředseda	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[[	[[	k?
<g/>
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
|	|	kIx~
<g/>
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
]]	]]	k?
<g/>
,	,	kIx,
2019-07-03	2019-07-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Volební	volební	k2eAgInSc1d1
program	program	k1gInSc1
černé	černá	k1gFnSc2
na	na	k7c6
bílém	bílé	k1gNnSc6
pro	pro	k7c4
volby	volba	k1gFnPc4
2017	#num#	k4
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
<g/>
1	#num#	k4
2	#num#	k4
han	hana	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Rozhovory	rozhovor	k1gInPc4
s	s	k7c7
lídry	lídr	k1gMnPc7
kandidátek	kandidátka	k1gFnPc2
do	do	k7c2
evropských	evropský	k2eAgFnPc2d1
voleb	volba	k1gFnPc2
<g/>
:	:	kIx,
Marcel	Marcel	k1gMnSc1
Kolaja	Kolaja	k1gMnSc1
(	(	kIx(
<g/>
Piráti	pirát	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
2019-05-07	2019-05-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
https://www.pirati.cz/program/	https://www.pirati.cz/program/	k?
<g/>
↑	↑	k?
WIRNITZER	WIRNITZER	kA
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Politická	politický	k2eAgFnSc1d1
mapa	mapa	k1gFnSc1
podle	podle	k7c2
čtenářů	čtenář	k1gMnPc2
<g/>
:	:	kIx,
Úsvit	úsvit	k1gInSc1
a	a	k8xC
ANO	ano	k9
neuchopitelní	uchopitelný	k2eNgMnPc1d1
<g/>
,	,	kIx,
o	o	k7c4
KSČM	KSČM	kA
máte	mít	k5eAaImIp2nP
jasno	jasno	k1gNnSc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2016-09-13	2016-09-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Slovak	Slovak	k1gMnSc1
Journal	Journal	k1gMnSc1
of	of	k?
Political	Political	k1gMnSc1
Sciences	Sciences	k1gMnSc1
<g/>
,	,	kIx,
Volume	volum	k1gInSc5
17	#num#	k4
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
,	,	kIx,
No	no	k9
<g/>
.	.	kIx.
1	#num#	k4
<g/>
,	,	kIx,
Ve	v	k7c6
webovém	webový	k2eAgInSc6d1
archivu	archiv	k1gInSc6
<g/>
.	.	kIx.
<g/>
↑	↑	k?
JELÍNEK	Jelínek	k1gMnSc1
<g/>
,	,	kIx,
Lukáš	Lukáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strašení	strašení	k1gNnSc1
Piráty	pirát	k1gMnPc7
se	se	k3xPyFc4
může	moct	k5eAaImIp3nS
Babišovi	Babiš	k1gMnSc3
vymstít	vymstít	k5eAaPmF
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
2020-12-09	2020-12-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
1	#num#	k4
2	#num#	k4
Česká	český	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pirátský	pirátský	k2eAgInSc1d1
program	program	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Svoboda	Svoboda	k1gMnSc1
podnikání	podnikání	k1gNnSc2
<g/>
.	.	kIx.
pirati	pirat	k5eAaBmF,k5eAaImF,k5eAaPmF
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Finance	finance	k1gFnPc1
<g/>
.	.	kIx.
pirati	pirat	k5eAaImF,k5eAaBmF,k5eAaPmF
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
JUŠKOVÁ	JUŠKOVÁ	kA
<g/>
,	,	kIx,
Kamila	Kamila	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k8xC,k8xS
zahraniční	zahraniční	k2eAgFnPc1d1
firmy	firma	k1gFnPc1
dojí	dojit	k5eAaImIp3nP
Česko	Česko	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odteče	odtéct	k5eAaPmIp3nS
až	až	k9
700	#num#	k4
miliard	miliarda	k4xCgFnPc2
ročně	ročně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
EMPRESA	EMPRESA	kA
MEDIA	medium	k1gNnSc2
<g/>
,	,	kIx,
2017-12-06	2017-12-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Novinky	novinka	k1gFnPc1
<g/>
;	;	kIx,
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
zahraničí	zahraničí	k1gNnSc2
odteklo	odtéct	k5eAaPmAgNnS
na	na	k7c6
dividendách	dividenda	k1gFnPc6
rekordních	rekordní	k2eAgFnPc2d1
289	#num#	k4
miliard	miliarda	k4xCgFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BORGIS	borgis	k1gInSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
HAVLIGEROVÁ	HAVLIGEROVÁ	kA
<g/>
,	,	kIx,
Jana	Jana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vyšší	vysoký	k2eAgFnPc4d2
výplaty	výplata	k1gFnPc4
a	a	k8xC
levnější	levný	k2eAgInPc4d2
nákupy	nákup	k1gInPc4
<g/>
?	?	kIx.
</s>
<s desamb="1">
Strany	strana	k1gFnSc2
nešetří	šetřit	k5eNaImIp3nP
sliby	slib	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
E15	E15	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CZECH	CZECH	kA
NEWS	NEWS	kA
CENTER	centrum	k1gNnPc2
<g/>
,	,	kIx,
2017-10-13	2017-10-13	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
kaš	kaša	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Piráti	pirát	k1gMnPc1
k	k	k7c3
tématu	téma	k1gNnSc3
Školství	školství	k1gNnSc2
<g/>
,	,	kIx,
sport	sport	k1gInSc1
<g/>
,	,	kIx,
věda	věda	k1gFnSc1
a	a	k8xC
mládež	mládež	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
2017-09-14	2017-09-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Věda	věda	k1gFnSc1
a	a	k8xC
výzkum	výzkum	k1gInSc1
<g/>
.	.	kIx.
pirati	pirat	k5eAaImF,k5eAaBmF,k5eAaPmF
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
mop	mop	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Piráti	pirát	k1gMnPc1
k	k	k7c3
tématu	téma	k1gNnSc3
Zemědělství	zemědělství	k1gNnSc2
a	a	k8xC
životní	životní	k2eAgNnSc4d1
prostředí	prostředí	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
2017-09-14	2017-09-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
TRNKA	Trnka	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ekologie	ekologie	k1gFnSc1
a	a	k8xC
ideologie	ideologie	k1gFnSc1
–	–	k?
Jedno	jeden	k4xCgNnSc1
z	z	k7c2
předvolebních	předvolební	k2eAgNnPc2d1
hesel	heslo	k1gNnPc2
České	český	k2eAgFnSc2d1
pirátské	pirátský	k2eAgFnSc2d1
strany	strana	k1gFnSc2
zní	znět	k5eAaImIp3nS
„	„	k?
<g/>
Ekologie	ekologie	k1gFnSc1
bez	bez	k7c2
ideologie	ideologie	k1gFnSc2
<g/>
“	“	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Co	co	k3yQnSc1,k3yInSc1,k3yRnSc1
se	se	k3xPyFc4
tím	ten	k3xDgNnSc7
strana	strana	k1gFnSc1
snaží	snažit	k5eAaImIp3nS
říct	říct	k5eAaPmF
<g/>
?	?	kIx.
</s>
<s desamb="1">
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
A2	A2	k1gFnSc1
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Životní	životní	k2eAgNnSc1d1
prostředí	prostředí	k1gNnSc1
<g/>
.	.	kIx.
piráti	pirát	k1gMnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Transparentní	transparentní	k2eAgInSc1d1
účet	účet	k1gInSc1
Pirátů	pirát	k1gMnPc2
<g/>
↑	↑	k?
Transparentní	transparentní	k2eAgNnSc1d1
účetnictví	účetnictví	k1gNnSc1
Pirátů	pirát	k1gMnPc2
<g/>
↑	↑	k?
Podmínky	podmínka	k1gFnSc2
použití	použití	k1gNnSc2
webu	web	k1gInSc2
<g/>
:	:	kIx,
Copyright	copyright	k1gInSc1
<g/>
↑	↑	k?
PERKNEROVÁ	PERKNEROVÁ	kA
<g/>
,	,	kIx,
Kateřina	Kateřina	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pirát	pirát	k1gMnSc1
<g/>
,	,	kIx,
který	který	k3yRgMnSc1,k3yQgMnSc1,k3yIgMnSc1
v	v	k7c6
Česku	Česko	k1gNnSc6
žádné	žádný	k3yNgNnSc1
spojence	spojenec	k1gMnSc4
nevidí	vidět	k5eNaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolaja	Kolaja	k1gMnSc1
je	být	k5eAaImIp3nS
lídrem	lídr	k1gMnSc7
do	do	k7c2
voleb	volba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
VLTAVA	Vltava	k1gFnSc1
LABE	Labe	k1gNnSc2
MEDIA	medium	k1gNnSc2
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Jednací	jednací	k2eAgInSc4d1
a	a	k8xC
volební	volební	k2eAgInSc4d1
řád	řád	k1gInSc4
celostátního	celostátní	k2eAgNnSc2d1
fóra	fórum	k1gNnSc2
<g/>
↑	↑	k?
Česká	český	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pirátské	pirátský	k2eAgNnSc4d1
dvanáctero	dvanáctero	k1gNnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2011	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
29	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
DVOŘÁKOVÁ	Dvořáková	k1gFnSc1
<g/>
,	,	kIx,
Petra	Petra	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Piráti	pirát	k1gMnPc1
milují	milovat	k5eAaImIp3nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pirátské	pirátský	k2eAgFnPc4d1
vlajky	vlajka	k1gFnPc4
tradičně	tradičně	k6eAd1
na	na	k7c4
Prague	Prague	k1gFnSc4
Pride	Prid	k1gInSc5
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pirátské	pirátský	k2eAgInPc1d1
listy	list	k1gInPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
2016-08-17	2016-08-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Pirátský	pirátský	k2eAgInSc4d1
program	program	k1gInSc4
<g/>
:	:	kIx,
Soukromí	soukromí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přístup	přístup	k1gInSc1
dne	den	k1gInSc2
29.7	29.7	k4
<g/>
.2012	.2012	k4
<g/>
.1	.1	k4
2	#num#	k4
Konzultace	konzultace	k1gFnSc1
ke	k	k7c3
kopírovacímu	kopírovací	k2eAgInSc3d1
zákonu	zákon	k1gInSc3
2011	#num#	k4
na	na	k7c4
Piratopedii	Piratopedie	k1gFnSc4
<g/>
,	,	kIx,
www.piratskastrana.cz	www.piratskastrana.cz	k1gInSc1
<g/>
↑	↑	k?
Volební	volební	k2eAgInSc1d1
program	program	k1gInSc1
černé	černá	k1gFnSc2
na	na	k7c6
bílém	bílé	k1gNnSc6
pro	pro	k7c4
volby	volba	k1gFnPc4
2017	#num#	k4
do	do	k7c2
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
↑	↑	k?
jh	jh	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Piráti	pirát	k1gMnPc1
a	a	k8xC
STAN	stan	k1gInSc1
se	se	k3xPyFc4
dohodli	dohodnout	k5eAaPmAgMnP
na	na	k7c6
koaliční	koaliční	k2eAgFnSc6d1
smlouvě	smlouva	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
2020-12-22	2020-12-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Mezinárodní	mezinárodní	k2eAgInPc4d1
vztahy	vztah	k1gInPc4
<g/>
.	.	kIx.
pirati	pirat	k5eAaImF,k5eAaBmF,k5eAaPmF
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Česko	Česko	k1gNnSc1
je	být	k5eAaImIp3nS
už	už	k9
20	#num#	k4
let	léto	k1gNnPc2
v	v	k7c6
NATO	NATO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Výročí	výročí	k1gNnSc1
si	se	k3xPyFc3
připomněla	připomnět	k5eAaPmAgFnS
i	i	k8xC
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
primátor	primátor	k1gMnSc1
namluvil	namluvit	k5eAaPmAgMnS,k5eAaBmAgMnS
zdravici	zdravice	k1gFnSc4
pro	pro	k7c4
cestující	cestující	k1gMnPc4
v	v	k7c6
metru	metro	k1gNnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
PražskýDEN	PražskýDEN	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jakub	Jakub	k1gMnSc1
Mitas	Mitas	k1gMnSc1
<g/>
,	,	kIx,
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
KOPECKÝ	Kopecký	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Piráti	pirát	k1gMnPc5
<g/>
,	,	kIx,
jak	jak	k6eAd1
je	on	k3xPp3gMnPc4
nejspíš	nejspíš	k9
neznáte	znát	k5eNaImIp2nP,k5eAaImIp2nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Strana	strana	k1gFnSc1
<g/>
,	,	kIx,
v	v	k7c6
níž	jenž	k3xRgFnSc6
mají	mít	k5eAaImIp3nP
problémy	problém	k1gInPc1
s	s	k7c7
NATO	NATO	kA
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
;	;	kIx,
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pusťte	pustit	k5eAaPmRp2nP
se	se	k3xPyFc4
do	do	k7c2
Turecka	Turecko	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Piráti	pirát	k1gMnPc1
předali	předat	k5eAaPmAgMnP
vládě	vláda	k1gFnSc3
petici	petice	k1gFnSc4
vyzývající	vyzývající	k2eAgFnSc4d1
k	k	k7c3
sankcím	sankce	k1gFnPc3
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2020	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
1	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Mikuláš	Mikuláš	k1gMnSc1
Ferjenčík	Ferjenčík	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Stanovisko	stanovisko	k1gNnSc1
Pirátů	pirát	k1gMnPc2
ke	k	k7c3
kolektivní	kolektivní	k2eAgFnSc3d1
obraně	obrana	k1gFnSc3
v	v	k7c6
rámci	rámec	k1gInSc6
EU	EU	kA
a	a	k8xC
NATO	NATO	kA
<g/>
.	.	kIx.
0	#num#	k4
<g/>
1	#num#	k4
<g/>
.	.	kIx.
0	#num#	k4
<g/>
6	#num#	k4
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
VESELÁ	Veselá	k1gFnSc1
<g/>
,	,	kIx,
Linda	Linda	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
NATO	nato	k6eAd1
lze	lze	k6eAd1
opustit	opustit	k5eAaPmF
<g/>
,	,	kIx,
když	když	k8xS
se	se	k3xPyFc4
EU	EU	kA
ubrání	ubránit	k5eAaPmIp3nS
sama	sám	k3xTgFnSc1
<g/>
,	,	kIx,
řekl	říct	k5eAaPmAgMnS
Pirát	pirát	k1gMnSc1
Kopřiva	Kopřiva	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Feri	Fer	k1gInSc6
byl	být	k5eAaImAgMnS
proti	proti	k7c3
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2017-10-24	2017-10-24	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
BOHUSLAVOVÁ	BOHUSLAVOVÁ	kA
<g/>
,	,	kIx,
Renáta	Renáta	k1gFnSc1
<g/>
;	;	kIx,
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Předseda	předseda	k1gMnSc1
Pirátů	pirát	k1gMnPc2
Bartoš	Bartoš	k1gMnSc1
<g/>
:	:	kIx,
Nejsme	být	k5eNaImIp1nP
vítači	vítač	k1gMnPc1
migrantů	migrant	k1gMnPc2
a	a	k8xC
jít	jít	k5eAaImF
do	do	k7c2
vlády	vláda	k1gFnSc2
se	se	k3xPyFc4
nebojíme	bát	k5eNaImIp1nP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BORGIS	borgis	k1gInSc1
<g/>
,	,	kIx,
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Osobnosti	osobnost	k1gFnPc1
upozornily	upozornit	k5eAaPmAgFnP
EU	EU	kA
na	na	k7c4
kontroverzní	kontroverzní	k2eAgInPc4d1
Dny	den	k1gInPc4
Jeruzaléma	Jeruzalém	k1gInSc2
v	v	k7c6
Plzni	Plzeň	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
VLTAVA	Vltava	k1gFnSc1
LABE	Labe	k1gNnSc2
MEDIA	medium	k1gNnSc2
<g/>
,	,	kIx,
21	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2015	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Piráti	pirát	k1gMnPc1
chtějí	chtít	k5eAaImIp3nP
změnu	změna	k1gFnSc4
EU	EU	kA
v	v	k7c6
postoji	postoj	k1gInSc6
k	k	k7c3
Turecku	Turecko	k1gNnSc3
a	a	k8xC
konec	konec	k1gInSc1
bojů	boj	k1gInPc2
v	v	k7c6
Sýrii	Sýrie	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
České	český	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
tisková	tiskový	k2eAgFnSc1d1
kancelář	kancelář	k1gFnSc1
<g/>
,	,	kIx,
15	#num#	k4
<g/>
.	.	kIx.
března	březen	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1213	#num#	k4
<g/>
-	-	kIx~
<g/>
5003	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Piráti	pirát	k1gMnPc1
<g/>
:	:	kIx,
Turecko	Turecko	k1gNnSc1
se	se	k3xPyFc4
odchýlilo	odchýlit	k5eAaPmAgNnS
z	z	k7c2
cesty	cesta	k1gFnSc2
slučitelné	slučitelný	k2eAgFnPc4d1
s	s	k7c7
hodnotami	hodnota	k1gFnPc7
EU	EU	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
EMPRESA	EMPRESA	kA
MEDIA	medium	k1gNnSc2
<g/>
,	,	kIx,
2018-03-15	2018-03-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Peksa	Peksa	k1gFnSc1
<g/>
:	:	kIx,
Postup	postup	k1gInSc1
Saúdské	saúdský	k2eAgFnSc2d1
Arábie	Arábie	k1gFnSc2
je	být	k5eAaImIp3nS
odsouzeníhodný	odsouzeníhodný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
EU	EU	kA
by	by	kYmCp3nS
měla	mít	k5eAaImAgFnS
vytvořit	vytvořit	k5eAaPmF
sankční	sankční	k2eAgInSc4d1
seznam	seznam	k1gInSc4
odpovědných	odpovědný	k2eAgFnPc2d1
osob	osoba	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pirati	Pirat	k2eAgMnPc1d1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
22	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
VARUFAKIS	VARUFAKIS	kA
<g/>
,	,	kIx,
Janis	Janis	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jaká	jaký	k3yRgNnPc4,k3yIgNnPc4,k3yQgNnPc4
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
Evropa	Evropa	k1gFnSc1
<g/>
?	?	kIx.
</s>
<s desamb="1">
Přečtěte	přečíst	k5eAaPmRp2nP
si	se	k3xPyFc3
manifest	manifest	k1gInSc4
Janise	Janise	k1gFnSc2
Varufakise	Varufakise	k1gFnSc2
<g/>
,	,	kIx,
k	k	k7c3
němuž	jenž	k3xRgInSc3
se	se	k3xPyFc4
přihlásili	přihlásit	k5eAaPmAgMnP
Piráti	pirát	k1gMnPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2017-10-27	2017-10-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1213	#num#	k4
<g/>
-	-	kIx~
<g/>
1385	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
;	;	kIx,
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Desítky	desítka	k1gFnPc1
tisíc	tisíc	k4xCgInSc1
Němců	Němec	k1gMnPc2
vyšly	vyjít	k5eAaPmAgFnP
do	do	k7c2
ulic	ulice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odmítají	odmítat	k5eAaImIp3nP
smlouvy	smlouva	k1gFnPc1
EU	EU	kA
s	s	k7c7
Kanadou	Kanada	k1gFnSc7
a	a	k8xC
USA	USA	kA
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2016-09-17	2016-09-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
<g/>
:	:	kIx,
Co	co	k3yQnSc4,k3yRnSc4,k3yInSc4
říkají	říkat	k5eAaImIp3nP
programy	program	k1gInPc1
českých	český	k2eAgFnPc2d1
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
o	o	k7c4
integraci	integrace	k1gFnSc4
cizinců	cizinec	k1gMnPc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Migraceonline	Migraceonlin	k1gInSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
22	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KOUBOVÁ	Koubová	k1gFnSc1
<g/>
,	,	kIx,
Karolína	Karolína	k1gFnSc1
<g/>
;	;	kIx,
ČIHÁK	Čihák	k1gMnSc1
<g/>
,	,	kIx,
Ondřej	Ondřej	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Migrace	migrace	k1gFnSc1
potřebuje	potřebovat	k5eAaImIp3nS
pravidla	pravidlo	k1gNnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Odmítnutím	odmítnutí	k1gNnSc7
paktu	pakt	k1gInSc2
OSN	OSN	kA
jsme	být	k5eAaImIp1nP
problém	problém	k1gInSc4
odložili	odložit	k5eAaPmAgMnP
<g/>
,	,	kIx,
věří	věřit	k5eAaImIp3nS
poslanec	poslanec	k1gMnSc1
Peksa	Peks	k1gMnSc2
(	(	kIx(
<g/>
Piráti	pirát	k1gMnPc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
Plus	plus	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Pirate	Pirat	k1gInSc5
Common	Common	k1gMnSc1
European	Europeana	k1gFnPc2
Elections	Elections	k1gInSc4
Programme	Programme	k1gFnSc4
2019	#num#	k4
<g/>
.	.	kIx.
wiki	wiki	k6eAd1
<g/>
.	.	kIx.
<g/>
ppeu	ppeu	k6eAd1
<g/>
.	.	kIx.
<g/>
net	net	k?
<g/>
.1	.1	k4
2	#num#	k4
ČTK	ČTK	kA
<g/>
,	,	kIx,
EKB	EKB	kA
-	-	kIx~
9	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Evropské	evropský	k2eAgFnPc1d1
pirátské	pirátský	k2eAgFnPc1d1
strany	strana	k1gFnPc1
spojují	spojovat	k5eAaImIp3nP
síly	síla	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
Lucemburku	Lucemburk	k1gInSc6
podepsaly	podepsat	k5eAaPmAgInP
program	program	k1gInSc4
pro	pro	k7c4
eurovolby	eurovolba	k1gFnPc4
<g/>
[	[	kIx(
<g/>
nedostupný	dostupný	k2eNgInSc1d1
zdroj	zdroj	k1gInSc1
<g/>
]	]	kIx)
<g/>
.	.	kIx.
info	info	k1gNnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Volební	volební	k2eAgInSc1d1
program	program	k1gInSc1
pro	pro	k7c4
volby	volba	k1gFnPc4
do	do	k7c2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
pirati	pirat	k5eAaPmF,k5eAaBmF,k5eAaImF
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
DOLEJŠÍ	Dolejší	k1gMnSc1
<g/>
,	,	kIx,
Václav	Václav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c4
Piráty	pirát	k1gMnPc4
se	se	k3xPyFc4
přišel	přijít	k5eAaPmAgMnS
podívat	podívat	k5eAaImF,k5eAaPmF
zakladatel	zakladatel	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Přeje	přát	k5eAaImIp3nS
si	se	k3xPyFc3
<g/>
,	,	kIx,
aby	aby	kYmCp3nP
se	se	k3xPyFc4
mladí	mladý	k1gMnPc1
u	u	k7c2
moci	moc	k1gFnSc2
nezkazili	zkazit	k5eNaPmAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
Zprávy	zpráva	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2019-01-27	2019-01-27	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Engström	Engström	k1gMnSc1
<g/>
,	,	kIx,
Christian	Christian	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
IFPI	IFPI	kA
strategie	strategie	k1gFnSc2
dětské	dětský	k2eAgFnSc2d1
pornografie	pornografie	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
,	,	kIx,
2010-04-28	2010-04-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
8	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
KADEŘÁVEK	Kadeřávek	k1gMnSc1
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Petice	petice	k1gFnSc1
pod	pod	k7c4
vznik	vznik	k1gInSc4
Česká	český	k2eAgFnSc1d1
pirátské	pirátský	k2eAgFnPc4d1
strany	strana	k1gFnPc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
AbcLinuxu	AbcLinux	k1gInSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nitemedia	Nitemedium	k1gNnPc4
<g/>
,	,	kIx,
2009-04-19	2009-04-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1214	#num#	k4
<g/>
-	-	kIx~
<g/>
1267	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
VOTRUBOVÁ	Votrubová	k1gFnSc1
<g/>
,	,	kIx,
Andrea	Andrea	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Čeští	český	k2eAgMnPc1d1
internetoví	internetový	k2eAgMnPc1d1
piráti	pirát	k1gMnPc1
zakládají	zakládat	k5eAaImIp3nP
politickou	politický	k2eAgFnSc4d1
stranu	strana	k1gFnSc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2009-04-21	2009-04-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
oL	oL	k?
<g/>
;	;	kIx,
ŠMAJLEROVÁ	ŠMAJLEROVÁ	kA
<g/>
,	,	kIx,
Zuzana	Zuzana	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikne	vzniknout	k5eAaPmIp3nS
nová	nový	k2eAgFnSc1d1
politická	politický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
<g/>
.	.	kIx.
”	”	k?
<g/>
Piráti	pirát	k1gMnPc1
z	z	k7c2
webu	web	k1gInSc2
<g/>
”	”	k?
mají	mít	k5eAaImIp3nP
1000	#num#	k4
podpisů	podpis	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářské	hospodářský	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
iHNed	ihned	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Economia	Economia	k1gFnSc1
<g/>
,	,	kIx,
2009-04-20	2009-04-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
MACICH	MACICH	kA
<g/>
,	,	kIx,
Jiří	Jiří	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
podala	podat	k5eAaPmAgFnS
žádost	žádost	k1gFnSc4
o	o	k7c4
registraci	registrace	k1gFnSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Internet	Internet	k1gInSc1
Info	Info	k6eAd1
<g/>
,	,	kIx,
2009-05-29	2009-05-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Česká	český	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
požádala	požádat	k5eAaPmAgFnS
o	o	k7c6
registraci	registrace	k1gFnSc6
mediafax	mediafax	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
ČÍŽEK	Čížek	k1gMnSc1
<g/>
,	,	kIx,
Jakub	Jakub	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
je	být	k5eAaImIp3nS
oficiální	oficiální	k2eAgFnSc1d1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CZECH	CZECH	kA
NEWS	NEWS	kA
CENTER	centrum	k1gNnPc2
<g/>
,	,	kIx,
2009	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Historie	historie	k1gFnSc1
strany	strana	k1gFnSc2
na	na	k7c6
stránkách	stránka	k1gFnPc6
MV	MV	kA
<g/>
↑	↑	k?
Česká	český	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
zvolila	zvolit	k5eAaPmAgFnS
vedení	vedení	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lupa	lupa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Internet	Internet	k1gInSc1
Info	Info	k6eAd1
<g/>
,	,	kIx,
2009-07-01	2009-07-01	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zasedání	zasedání	k1gNnSc1
v	v	k7c6
Albrechticích	Albrechtice	k1gFnPc6
<g/>
↑	↑	k?
Zasedání	zasedání	k1gNnSc1
v	v	k7c6
Brně	Brno	k1gNnSc6
<g/>
↑	↑	k?
Zasedání	zasedání	k1gNnPc1
v	v	k7c6
Křižanově	Křižanův	k2eAgNnSc6d1
<g/>
↑	↑	k?
Zasedání	zasedání	k1gNnSc1
v	v	k7c6
Praze	Praha	k1gFnSc6
<g/>
↑	↑	k?
JIŘIČKA	jiřička	k1gFnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
spustila	spustit	k5eAaPmAgFnS
PirateLeaks	PirateLeaks	k1gInSc4
<g/>
,	,	kIx,
ukázala	ukázat	k5eAaPmAgFnS
vliv	vliv	k1gInSc4
OSA	osa	k1gFnSc1
na	na	k7c4
autorský	autorský	k2eAgInSc4d1
zákon	zákon	k1gInSc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2010-12-21	2010-12-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
TITTL	TITTL	kA
<g/>
,	,	kIx,
Tomáš	Tomáš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nový	nový	k2eAgInSc1d1
zákon	zákon	k1gInSc1
naúčtuje	naúčtovat	k5eAaPmIp3nS
knihovnám	knihovna	k1gFnPc3
a	a	k8xC
školám	škola	k1gFnPc3
i	i	k9
za	za	k7c4
pár	pár	k4xCyI
tiskáren	tiskárna	k1gFnPc2
desetitisíce	desetitisíce	k1gInPc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2010-08-18	2010-08-18	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Ministerstvo	ministerstvo	k1gNnSc1
kultury	kultura	k1gFnSc2
zahajuje	zahajovat	k5eAaImIp3nS
veřejnou	veřejný	k2eAgFnSc4d1
konzultaci	konzultace	k1gFnSc4
k	k	k7c3
novele	novela	k1gFnSc3
autorského	autorský	k2eAgInSc2d1
zákona	zákon	k1gInSc2
<g/>
,	,	kIx,
mkcr	mkcr	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jak	jak	k6eAd1
ještě	ještě	k6eAd1
dlouho	dlouho	k6eAd1
žít	žít	k5eAaImF
z	z	k7c2
Beatles	Beatles	k1gFnSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
EMPRESA	EMPRESA	kA
MEDIA	medium	k1gNnSc2
<g/>
,	,	kIx,
2011-05-20	2011-05-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Tisková	tiskový	k2eAgFnSc1d1
konference	konference	k1gFnSc1
Pirátské	pirátský	k2eAgFnSc2d1
strany	strana	k1gFnSc2
k	k	k7c3
prodloužení	prodloužení	k1gNnSc3
kopírovacího	kopírovací	k2eAgInSc2d1
monopolu	monopol	k1gInSc2
na	na	k7c4
zvukové	zvukový	k2eAgFnPc4d1
nahrávky	nahrávka	k1gFnPc4
<g/>
,	,	kIx,
www.piratskastrana.cz	www.piratskastrana.cz	k1gInSc1
<g/>
↑	↑	k?
kopirovanijezadarmo	kopirovanijezadarmo	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
HN	HN	kA
Dialog	dialog	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tisíce	tisíc	k4xCgInPc1
lidí	člověk	k1gMnPc2
protestují	protestovat	k5eAaBmIp3nP
proti	proti	k7c3
návrhu	návrh	k1gInSc3
poslanců	poslanec	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hrozí	hrozit	k5eAaImIp3nS
cenzura	cenzura	k1gFnSc1
internetu	internet	k1gInSc2
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hospodářské	hospodářský	k2eAgFnPc4d1
noviny	novina	k1gFnPc4
iHNed	ihned	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Economia	Economia	k1gFnSc1
<g/>
,	,	kIx,
2011-06-20	2011-06-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
http://www.internetbezcenzury.cz/index_1.php	http://www.internetbezcenzury.cz/index_1.php	k1gInSc1
↑	↑	k?
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
:	:	kIx,
Cenzura	cenzura	k1gFnSc1
internetu	internet	k1gInSc2
ve	v	k7c6
sněmovně	sněmovna	k1gFnSc6
neprošla	projít	k5eNaPmAgFnS
<g/>
↑	↑	k?
KOPECKÝ	Kopecký	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cenzura	cenzura	k1gFnSc1
internetu	internet	k1gInSc2
neprošla	projít	k5eNaPmAgFnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Obce	obec	k1gFnPc1
dostaly	dostat	k5eAaPmAgFnP
šanci	šance	k1gFnSc4
vyhnat	vyhnat	k5eAaPmF
hrací	hrací	k2eAgInPc4d1
automaty	automat	k1gInPc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2011-06-21	2011-06-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Piráti	pirát	k1gMnPc1
na	na	k7c6
Klárově	Klárov	k1gInSc6
protestovali	protestovat	k5eAaBmAgMnP
proti	proti	k7c3
smlouvě	smlouva	k1gFnSc3
ACTA	ACTA	kA
<g/>
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Kolem	kolem	k7c2
dvou	dva	k4xCgInPc2
tisíc	tisíc	k4xCgInPc2
lidí	člověk	k1gMnPc2
protestovalo	protestovat	k5eAaBmAgNnS
proti	proti	k7c3
smlouvě	smlouva	k1gFnSc3
ACTA	ACTA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pražský	pražský	k2eAgInSc1d1
deník	deník	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
VLTAVA	Vltava	k1gFnSc1
LABE	Labe	k1gNnSc2
MEDIA	medium	k1gNnSc2
<g/>
,	,	kIx,
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
2	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ŠPULÁK	ŠPULÁK	kA
<g/>
,	,	kIx,
Jaroslav	Jaroslav	k1gMnSc1
<g/>
;	;	kIx,
Právo	právo	k1gNnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Piráti	pirát	k1gMnPc1
v	v	k7c6
Praze	Praha	k1gFnSc6
protestovali	protestovat	k5eAaBmAgMnP
proti	proti	k7c3
dohodě	dohoda	k1gFnSc3
ACTA	ACTA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Novinky	novinka	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
BORGIS	borgis	k1gInSc1
<g/>
,	,	kIx,
2012-02-02	2012-02-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
LANGER	Langer	k1gMnSc1
<g/>
,	,	kIx,
Jan	Jan	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Piráti	pirát	k1gMnPc1
demonstrovali	demonstrovat	k5eAaBmAgMnP
proti	proti	k7c3
ACTA	ACTA	kA
a	a	k8xC
za	za	k7c4
svobodný	svobodný	k2eAgInSc4d1
internet	internet	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Economia	Economia	k1gFnSc1
<g/>
,	,	kIx,
2012-02-02	2012-02-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Internetoví	internetový	k2eAgMnPc5d1
"	"	kIx"
<g/>
piráti	pirát	k1gMnPc5
<g/>
"	"	kIx"
vyrazili	vyrazit	k5eAaPmAgMnP
na	na	k7c4
Hrad	hrad	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
E15	E15	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CZECH	CZECH	kA
NEWS	NEWS	kA
CENTER	centrum	k1gNnPc2
<g/>
,	,	kIx,
2012-02-02	2012-02-02	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
;	;	kIx,
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ministerstvo	ministerstvo	k1gNnSc1
nechce	chtít	k5eNaImIp3nS
rozhodnout	rozhodnout	k5eAaPmF
<g/>
,	,	kIx,
ACTA	ACTA	kA
pošle	poslat	k5eAaPmIp3nS
Ústavnímu	ústavní	k2eAgInSc3d1
soudu	soud	k1gInSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2012-04-10	2012-04-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1213	#num#	k4
<g/>
-	-	kIx~
<g/>
1385	#num#	k4
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Evropský	evropský	k2eAgInSc1d1
parlament	parlament	k1gInSc1
odmítl	odmítnout	k5eAaPmAgInS
protipirátskou	protipirátský	k2eAgFnSc4d1
smlouvu	smlouva	k1gFnSc4
ACTA	ACTA	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT24	ČT24	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
televize	televize	k1gFnSc1
<g/>
,	,	kIx,
2012-07-04	2012-07-04	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Stanovisko	stanovisko	k1gNnSc4
politické	politický	k2eAgFnSc2d1
skupiny	skupina	k1gFnSc2
Greens	Greensa	k1gFnPc2
<g/>
/	/	kIx~
<g/>
EFA	EFA	kA
<g/>
:	:	kIx,
ACTA	ACTA	kA
-	-	kIx~
anti-counterfeiting	anti-counterfeiting	k1gInSc1
treaty	treata	k1gFnSc2
<g/>
↑	↑	k?
Piráti	pirát	k1gMnPc1
přišli	přijít	k5eAaPmAgMnP
s	s	k7c7
návrhem	návrh	k1gInSc7
ústavního	ústavní	k2eAgInSc2d1
zákona	zákon	k1gInSc2
o	o	k7c6
svobodném	svobodný	k2eAgInSc6d1
internetu	internet	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Mediafax	Mediafax	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-02-28	2012-02-28	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
MATĚJČEK	Matějček	k1gMnSc1
<g/>
,	,	kIx,
Petr	Petr	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Svobodný	svobodný	k2eAgInSc1d1
internet	internet	k1gInSc1
se	se	k3xPyFc4
zaručuje	zaručovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Anebo	anebo	k8xC
ne	ne	k9
<g/>
?	?	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Česká	český	k2eAgFnSc1d1
pozice	pozice	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2012-05-09	2012-05-09	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
ISSN	ISSN	kA
1213	#num#	k4
<g/>
-	-	kIx~
<g/>
1385	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
DOSTÁL	Dostál	k1gMnSc1
<g/>
,	,	kIx,
Vratislav	Vratislav	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Piráti	pirát	k1gMnPc5
<g/>
:	:	kIx,
Novela	novela	k1gFnSc1
zákona	zákon	k1gInSc2
o	o	k7c6
svobodném	svobodný	k2eAgInSc6d1
přístupu	přístup	k1gInSc6
k	k	k7c3
informacím	informace	k1gFnPc3
je	být	k5eAaImIp3nS
nepřijatelná	přijatelný	k2eNgFnSc1d1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
Referendum	referendum	k1gNnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vydavatelství	vydavatelství	k1gNnSc1
Referendum	referendum	k1gNnSc1
<g/>
,	,	kIx,
2012-10-29	2012-10-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2021	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
TZ	TZ	kA
Další	další	k2eAgInSc4d1
pokus	pokus	k1gInSc4
o	o	k7c4
zničení	zničení	k1gNnSc4
zákona	zákon	k1gInSc2
o	o	k7c6
svobodném	svobodný	k2eAgInSc6d1
přístupu	přístup	k1gInSc6
k	k	k7c3
informacím	informace	k1gFnPc3
<g/>
↑	↑	k?
Den	den	k1gInSc4
infožádostí	infožádost	k1gFnPc2
<g/>
↑	↑	k?
Piráti	pirát	k1gMnPc1
obsadili	obsadit	k5eAaPmAgMnP
o	o	k7c6
víkendu	víkend	k1gInSc6
Prahu	Praha	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
7	#num#	k4
–	–	k?
Radio	radio	k1gNnSc1
Praha	Praha	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
2012-04-16	2012-04-16	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
‚	‚	k?
<g/>
Piráti	pirát	k1gMnPc1
<g/>
‘	‘	k?
se	se	k3xPyFc4
v	v	k7c6
Praze	Praha	k1gFnSc6
dohodli	dohodnout	k5eAaPmAgMnP
na	na	k7c4
vytvoření	vytvoření	k1gNnSc4
evropské	evropský	k2eAgFnSc2d1
strany	strana	k1gFnSc2
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
2012-04-15	2012-04-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Slovíčkaření	slovíčkaření	k1gNnSc2
i	i	k8xC
QR	QR	kA
kódy	kód	k1gInPc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Piráti	pirát	k1gMnPc1
Praze	Praha	k1gFnSc6
předvedli	předvést	k5eAaPmAgMnP
tekutou	tekutý	k2eAgFnSc4d1
demokracii	demokracie	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
E15	E15	k1gFnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
CZECH	CZECH	kA
NEWS	NEWS	kA
CENTER	centrum	k1gNnPc2
<g/>
,	,	kIx,
2012-04-15	2012-04-15	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Piráti	pirát	k1gMnPc1
stvrdili	stvrdit	k5eAaPmAgMnP
dohodu	dohoda	k1gFnSc4
o	o	k7c6
evropské	evropský	k2eAgFnSc6d1
spolupráci	spolupráce	k1gFnSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
Společně	společně	k6eAd1
zaútočí	zaútočit	k5eAaPmIp3nS
na	na	k7c4
Evropský	evropský	k2eAgInSc4d1
parlament	parlament	k1gInSc4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Piráti	pirát	k1gMnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2012-07-20	2012-07-20	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
30	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Světový	světový	k2eAgInSc1d1
závod	závod	k1gInSc1
o	o	k7c4
prvního	první	k4xOgMnSc4
piráta	pirát	k1gMnSc4
v	v	k7c6
národním	národní	k2eAgInSc6d1
parlamentu	parlament	k1gInSc6
vyhráli	vyhrát	k5eAaPmAgMnP
nečekaně	nečekaně	k6eAd1
Češi	Čech	k1gMnPc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2012-22-10	2012-22-10	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2012	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
http://www.pirati.cz/zastupitele	http://www.pirati.cz/zastupitel	k1gMnSc2
<g/>
↑	↑	k?
Archivovaná	archivovaný	k2eAgFnSc1d1
kopie	kopie	k1gFnSc1
<g/>
.	.	kIx.
piratetimes	piratetimes	k1gInSc1
<g/>
.	.	kIx.
<g/>
net	net	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgFnSc2d1
v	v	k7c6
archivu	archiv	k1gInSc6
pořízeném	pořízený	k2eAgInSc6d1
dne	den	k1gInSc2
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
.	.	kIx.
↑	↑	k?
http://piratetimes.net/pirates-elected-to-municipalities-and-local-bodies-pirate-history/	http://piratetimes.net/pirates-elected-to-municipalities-and-local-bodies-pirate-history/	k?
<g/>
↑	↑	k?
"	"	kIx"
<g/>
Můžeš	moct	k5eAaImIp2nS
mě	já	k3xPp1nSc4
fotit	fotit	k5eAaImF
<g/>
,	,	kIx,
ale	ale	k8xC
s	s	k7c7
tím	ten	k3xDgInSc7
jointem	joint	k1gInSc7
<g/>
.	.	kIx.
<g/>
"	"	kIx"
Piráti	pirát	k1gMnPc1
předběhli	předběhnout	k5eAaPmAgMnP
i	i	k9
Okamuru	Okamura	k1gFnSc4
<g/>
,	,	kIx,
skončili	skončit	k5eAaPmAgMnP
ale	ale	k9
na	na	k7c6
suchu	sucho	k1gNnSc6
s	s	k7c7
internetem	internet	k1gInSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Economia	Economia	k1gFnSc1
<g/>
,	,	kIx,
2017-10-21	2017-10-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Síla	síla	k1gFnSc1
stran	strana	k1gFnPc2
-	-	kIx~
Piráti	pirát	k1gMnPc1
-	-	kIx~
volby	volba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
idnes	idnes	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Výsledky	výsledek	k1gInPc7
voleb	volba	k1gFnPc2
do	do	k7c2
poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
2017	#num#	k4
–	–	k?
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2017-10-21	2017-10-21	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Volby	volba	k1gFnPc1
v	v	k7c6
kostce	kostka	k1gFnSc6
<g/>
:	:	kIx,
Piráti	pirát	k1gMnPc1
na	na	k7c6
vrcholu	vrchol	k1gInSc6
<g/>
,	,	kIx,
snící	snící	k2eAgFnPc4d1
ODS	ODS	kA
a	a	k8xC
pád	pád	k1gInSc1
Zelených	Zelený	k1gMnPc2
či	či	k8xC
Realistů	realista	k1gMnPc2
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2017-10-22	2017-10-22	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Piráti	pirát	k1gMnPc1
jedou	jet	k5eAaImIp3nP
za	za	k7c7
voliči	volič	k1gInPc7
s	s	k7c7
vězeňským	vězeňský	k2eAgInSc7d1
autobusem	autobus	k1gInSc7
pro	pro	k7c4
Babiše	Babiš	k1gMnSc4
<g/>
,	,	kIx,
Sobotku	Sobotka	k1gMnSc4
či	či	k8xC
Kalouska	Kalousek	k1gMnSc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
2017-08-14	2017-08-14	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
22	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
KOPECKÝ	Kopecký	k1gMnSc1
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pirát	pirát	k1gMnSc1
Bartoš	Bartoš	k1gMnSc1
<g/>
:	:	kIx,
Čtyři	čtyři	k4xCgInPc1
roky	rok	k1gInPc1
chceme	chtít	k5eAaImIp1nP
být	být	k5eAaImF
v	v	k7c6
parlamentu	parlament	k1gInSc6
velmi	velmi	k6eAd1
ostrou	ostrý	k2eAgFnSc7d1
opozicí	opozice	k1gFnSc7
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
30	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2017	#num#	k4
0	#num#	k4
<g/>
:	:	kIx,
<g/>
27	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
CF	CF	kA
23	#num#	k4
<g/>
/	/	kIx~
<g/>
2017	#num#	k4
Povolební	povolební	k2eAgFnSc2d1
strategie	strategie	k1gFnSc2
PSP	PSP	kA
20171	#num#	k4
2	#num#	k4
Když	když	k8xS
Pirát	pirát	k1gMnSc1
prchá	prchat	k5eAaImIp3nS
do	do	k7c2
podpalubí	podpalubí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tajné	tajný	k2eAgInPc1d1
hlasování	hlasování	k1gNnSc4
o	o	k7c6
metru	metro	k1gNnSc6
D	D	kA
byl	být	k5eAaImAgInS
krok	krok	k1gInSc4
zpět	zpět	k6eAd1
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
26	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
Senátu	senát	k1gInSc2
Parlamentu	parlament	k1gInSc2
ČR	ČR	kA
konané	konaný	k2eAgFnSc6d1
dne	den	k1gInSc2
5.10	5.10	k4
<g/>
.	.	kIx.
–	–	k?
6.10	6.10	k4
<g/>
.2018	.2018	k4
<g/>
,	,	kIx,
Výsledky	výsledek	k1gInPc1
hlasování	hlasování	k1gNnSc2
<g/>
,	,	kIx,
Obvod	obvod	k1gInSc1
<g/>
:	:	kIx,
26	#num#	k4
–	–	k?
Praha	Praha	k1gFnSc1
2	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
statistický	statistický	k2eAgInSc1d1
úřad	úřad	k1gInSc1
<g/>
,	,	kIx,
2018	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
13	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Radek	Radek	k1gMnSc1
Bartoníček	Bartoníček	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tvrdě	tvrdě	k6eAd1
jsem	být	k5eAaImIp1nS
pracovala	pracovat	k5eAaImAgFnS
<g/>
,	,	kIx,
říká	říkat	k5eAaImIp3nS
nejmladší	mladý	k2eAgFnSc1d3
senátorka	senátorka	k1gFnSc1
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yIgFnSc1,k3yRgFnSc1
porazila	porazit	k5eAaPmAgFnS
matadora	matador	k1gMnSc4
Bendla	Bendla	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktuálně	aktuálně	k6eAd1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
10.10	10.10	k4
<g/>
.2020	.2020	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Prahu	Praha	k1gFnSc4
povede	vést	k5eAaImIp3nS,k5eAaPmIp3nS
Zdeněk	Zdeněk	k1gMnSc1
Hřib	hřib	k1gInSc1
z	z	k7c2
Pirátů	pirát	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Pospíšil	Pospíšil	k1gMnSc1
ani	ani	k9
Čižinský	Čižinský	k2eAgMnSc1d1
v	v	k7c6
radě	rada	k1gFnSc6
města	město	k1gNnSc2
nezasednou	zasednout	k5eNaPmIp3nP
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
26	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Primátorem	primátor	k1gMnSc7
Prahy	Praha	k1gFnSc2
bude	být	k5eAaImBp3nS
Pirát	pirát	k1gMnSc1
Hřib	hřib	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgMnPc1d1
koaliční	koaliční	k2eAgMnPc1d1
lídři	lídr	k1gMnPc1
jsou	být	k5eAaImIp3nP
mimo	mimo	k7c4
radu	rada	k1gFnSc4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
25	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Už	už	k6eAd1
jsme	být	k5eAaImIp1nP
celorepublikovou	celorepublikový	k2eAgFnSc7d1
stranou	strana	k1gFnSc7
<g/>
,	,	kIx,
hlásal	hlásat	k5eAaImAgInS
z	z	k7c2
lodi	loď	k1gFnSc2
Pirát	pirát	k1gMnSc1
Bartoš	Bartoš	k1gMnSc1
<g/>
.	.	kIx.
6	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2018	#num#	k4
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
https://www.brno.cz/sprava-mesta/volene-organy-mesta/rada-mesta-brna/	https://www.brno.cz/sprava-mesta/volene-organy-mesta/rada-mesta-brna/	k?
<g/>
↑	↑	k?
https://www.ostrava.cz/cs/urad/mesto-a-jeho-organy/rada-mesta/slozeni-rady-mesta	https://www.ostrava.cz/cs/urad/mesto-a-jeho-organy/rada-mesta/slozeni-rady-mesta	k1gMnSc1
<g/>
↑	↑	k?
Zdeňka	Zdeňka	k1gFnSc1
Trachtová	Trachtová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volební	volební	k2eAgInSc1d1
lídr	lídr	k1gMnSc1
Pirátů	pirát	k1gMnPc2
<g/>
:	:	kIx,
V	v	k7c6
europarlamentu	europarlament	k1gInSc6
chci	chtít	k5eAaImIp1nS
bojovat	bojovat	k5eAaImF
proti	proti	k7c3
korporátním	korporátní	k2eAgInPc3d1
tlakům	tlak	k1gInPc3
a	a	k8xC
cenzuře	cenzura	k1gFnSc6
internetu	internet	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
.	.	kIx.
19	#num#	k4
<g/>
.	.	kIx.
leden	leden	k1gInSc1
2019	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
zpravy	zprava	k1gFnSc2
<g/>
.	.	kIx.
<g/>
aktualne	aktualnout	k5eAaImIp3nS,k5eAaPmIp3nS
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
https://zpravy.aktualne.cz/zahranici/evropsky-parlament/volby-europarlament-evropsky-parlament-kandidatky/r~ff2116601e4b11e99182ac1f6b220ee8/?redirected=1552802933.	https://zpravy.aktualne.cz/zahranici/evropsky-parlament/volby-europarlament-evropsky-parlament-kandidatky/r~ff2116601e4b11e99182ac1f6b220ee8/?redirected=1552802933.	k4
27	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
2019	#num#	k4
-	-	kIx~
Volební	volební	k2eAgInSc1d1
speciál	speciál	k1gInSc1
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
5	#num#	k4
<g/>
-	-	kIx~
<g/>
27	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Předsedou	předseda	k1gMnSc7
europarlamentu	europarlament	k1gInSc6
zvolen	zvolen	k2eAgMnSc1d1
Ital	Ital	k1gMnSc1
Sassoli	Sassole	k1gFnSc4
<g/>
,	,	kIx,
Charanzová	Charanzová	k1gFnSc1
a	a	k8xC
Kolaja	Kolaja	k1gMnSc1
budou	být	k5eAaImBp3nP
místopředsedy	místopředseda	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT	ČT	kA
<g/>
24	#num#	k4
<g/>
.	.	kIx.
0	#num#	k4
<g/>
4.06	4.06	k4
<g/>
.19	.19	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Staronový	staronový	k2eAgInSc4d1
předseda	předseda	k1gMnSc1
Pirátů	pirát	k1gMnPc2
Ivan	Ivan	k1gMnSc1
Bartoš	Bartoš	k1gMnSc1
<g/>
:	:	kIx,
Do	do	k7c2
vlády	vláda	k1gFnSc2
chceme	chtít	k5eAaImIp1nP
<g/>
,	,	kIx,
ale	ale	k8xC
máme	mít	k5eAaImIp1nP
řadu	řada	k1gFnSc4
podmínek	podmínka	k1gFnPc2
<g/>
.	.	kIx.
12.1	12.1	k4
<g/>
.2020	.2020	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
https://www.seznamzpravy.cz/p/vysledky-voleb/2020/senatni-volby/kolo/2	https://www.seznamzpravy.cz/p/vysledky-voleb/2020/senatni-volby/kolo/2	k4
<g/>
↑	↑	k?
Jednání	jednání	k1gNnSc1
o	o	k7c6
krajských	krajský	k2eAgFnPc6d1
koalicích	koalice	k1gFnPc6
končí	končit	k5eAaImIp3nS
<g/>
,	,	kIx,
jasno	jasno	k6eAd1
stále	stále	k6eAd1
není	být	k5eNaImIp3nS
v	v	k7c6
Karlovarském	karlovarský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT	ČT	kA
<g/>
24	#num#	k4
<g/>
.	.	kIx.
14.10	14.10	k4
<g/>
.2020	.2020	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Přehled	přehled	k1gInSc1
krajských	krajský	k2eAgFnPc2d1
koalic	koalice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
Zprávy	zpráva	k1gFnSc2
<g/>
.	.	kIx.
5.10	5.10	k4
<g/>
.2020	.2020	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Po	po	k7c6
dvou	dva	k4xCgInPc6
měsících	měsíc	k1gInPc6
vznikla	vzniknout	k5eAaPmAgFnS
v	v	k7c6
Karlovarském	karlovarský	k2eAgInSc6d1
kraji	kraj	k1gInSc6
koalice	koalice	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Hejtmanem	hejtman	k1gMnSc7
by	by	kYmCp3nS
měl	mít	k5eAaImAgMnS
být	být	k5eAaImF
Kulhánek	Kulhánek	k1gMnSc1
ze	z	k7c2
STAN	stan	k1gInSc1
<g/>
.	.	kIx.
9	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
STAN	stan	k1gInSc1
bude	být	k5eAaImBp3nS
jednat	jednat	k5eAaImF
o	o	k7c6
koalici	koalice	k1gFnSc6
pro	pro	k7c4
sněmovní	sněmovní	k2eAgFnPc4d1
volby	volba	k1gFnPc4
výhradně	výhradně	k6eAd1
s	s	k7c7
Piráty	pirát	k1gMnPc7
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT	ČT	kA
<g/>
24	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Spojování	spojování	k1gNnSc1
opozice	opozice	k1gFnSc2
pokračuje	pokračovat	k5eAaImIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Piráti	pirát	k1gMnPc1
kývli	kývnout	k5eAaPmAgMnP
na	na	k7c6
jednání	jednání	k1gNnSc6
se	s	k7c7
Starosty	Starosta	k1gMnPc7
o	o	k7c6
předvolební	předvolební	k2eAgFnSc6d1
koalici	koalice	k1gFnSc6
<g/>
.	.	kIx.
23	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Deník	deník	k1gInSc1
N.	N.	kA
<g/>
↑	↑	k?
Lídrem	lídr	k1gMnSc7
koalice	koalice	k1gFnSc2
Pirátů	pirát	k1gMnPc2
a	a	k8xC
STAN	stan	k1gInSc1
má	mít	k5eAaImIp3nS
být	být	k5eAaImF
Ivan	Ivan	k1gMnSc1
Bartoš	Bartoš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČT	ČT	kA
<g/>
24	#num#	k4
<g/>
.	.	kIx.
14.12	14.12	k4
<g/>
.2020	.2020	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
MICHAELA	Michaela	k1gFnSc1
RAMBOUSKOVÁ	Rambousková	k1gFnSc1
,	,	kIx,
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Piráti	pirát	k1gMnPc1
půjdou	jít	k5eAaImIp3nP
do	do	k7c2
sněmovních	sněmovní	k2eAgFnPc2d1
voleb	volba	k1gFnPc2
se	s	k7c7
Starosty	Starosta	k1gMnPc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Chceme	chtít	k5eAaImIp1nP
vyhrát	vyhrát	k5eAaPmF
<g/>
,	,	kIx,
řekl	říct	k5eAaPmAgMnS
Bartoš	Bartoš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Seznam	seznam	k1gInSc1
Zprávy	zpráva	k1gFnSc2
<g/>
.	.	kIx.
12.01	12.01	k4
<g/>
.2021	.2021	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
volby	volba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSÚ	ČSÚ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
volby	volba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSÚ	ČSÚ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
volby	volba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSÚ	ČSÚ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Příspěvky	příspěvek	k1gInPc7
ze	z	k7c2
státního	státní	k2eAgInSc2d1
rozpočtu	rozpočet	k1gInSc2
uhrazené	uhrazený	k2eAgFnSc2d1
politickým	politický	k2eAgFnPc3d1
stranám	strana	k1gFnPc3
a	a	k8xC
politickým	politický	k2eAgNnPc3d1
hnutím	hnutí	k1gNnPc3
celkem	celkem	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
2017	#num#	k4
<g/>
.	.	kIx.
8	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ministerstvo	ministerstvo	k1gNnSc1
financí	finance	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Ivan	Ivan	k1gMnSc1
Bartoš	Bartoš	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
psp	psp	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Olga	Olga	k1gFnSc1
Richterová	Richterová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
psp	psp	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Vojtěch	Vojtěch	k1gMnSc1
Pikal	pikat	k5eAaImAgMnS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
psp	psp	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Radek	Radek	k1gMnSc1
Holomčík	Holomčík	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
psp	psp	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Jakub	Jakub	k1gMnSc1
Michálek	Michálek	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
psp	psp	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Dana	Dana	k1gFnSc1
Balcarová	Balcarová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
psp	psp	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Mikuláš	Mikuláš	k1gMnSc1
Ferjenčík	Ferjenčík	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
psp	psp	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
Senátu	senát	k1gInSc2
Parlamentu	parlament	k1gInSc2
ČR	ČR	kA
konané	konaný	k2eAgFnSc6d1
dne	den	k1gInSc2
15.10	15.10	k4
<g/>
.	.	kIx.
–	–	k?
16.10	16.10	k4
<g/>
.2010	.2010	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSÚ	ČSÚ	kA
na	na	k7c4
volby	volba	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Doplňovací	doplňovací	k2eAgFnPc4d1
volby	volba	k1gFnPc4
do	do	k7c2
Senátu	senát	k1gInSc2
Parlamentu	parlament	k1gInSc2
ČR	ČR	kA
konané	konaný	k2eAgFnSc6d1
dne	den	k1gInSc2
18.3	18.3	k4
<g/>
.	.	kIx.
–	–	k?
19.3	19.3	k4
<g/>
.2011	.2011	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSÚ	ČSÚ	kA
na	na	k7c4
volby	volba	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
{{{	{{{	k?
<g/>
1	#num#	k4
<g/>
}}}	}}}	k?
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
Senátu	senát	k1gInSc2
Parlamentu	parlament	k1gInSc2
ČR	ČR	kA
konané	konaný	k2eAgFnSc6d1
dne	den	k1gInSc2
12.10	12.10	k4
<g/>
.	.	kIx.
–	–	k?
13.10	13.10	k4
<g/>
.2012	.2012	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSÚ	ČSÚ	kA
na	na	k7c4
volby	volba	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
{{{	{{{	k?
<g/>
1	#num#	k4
<g/>
}}}	}}}	k?
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
Senátu	senát	k1gInSc2
Parlamentu	parlament	k1gInSc2
ČR	ČR	kA
konané	konaný	k2eAgFnSc6d1
dne	den	k1gInSc2
10.1	10.1	k4
<g/>
.	.	kIx.
–	–	k?
11.1	11.1	k4
<g/>
.2014	.2014	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSÚ	ČSÚ	kA
na	na	k7c4
volby	volba	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
[	[	kIx(
<g/>
{{{	{{{	k?
<g/>
1	#num#	k4
<g/>
}}}	}}}	k?
Dostupné	dostupný	k2eAgNnSc1d1
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
volby	volba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSÚ	ČSÚ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
volby	volba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSÚ	ČSÚ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
Senátu	senát	k1gInSc2
Parlamentu	parlament	k1gInSc2
ČR	ČR	kA
konané	konaný	k2eAgFnSc6d1
dne	den	k1gInSc2
7.10	7.10	k4
<g/>
.	.	kIx.
–	–	k?
8.10	8.10	k4
<g/>
.2016	.2016	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSÚ	ČSÚ	kA
na	na	k7c4
volby	volba	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
volby	volba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSÚ	ČSÚ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Volby	volba	k1gFnPc4
2018	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSÚ	ČSÚ	kA
na	na	k7c4
volby	volba	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
volby	volba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSÚ	ČSÚ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Příspěvky	příspěvek	k1gInPc1
ze	z	k7c2
státního	státní	k2eAgInSc2d1
rozpočtu	rozpočet	k1gInSc2
uhrazené	uhrazený	k2eAgFnSc2d1
politickým	politický	k2eAgFnPc3d1
stranám	strana	k1gFnPc3
a	a	k8xC
politickým	politický	k2eAgNnPc3d1
hnutím	hnutí	k1gNnPc3
celkem	celkem	k6eAd1
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ministerstvo	ministerstvo	k1gNnSc1
financí	finance	k1gFnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Volby	volba	k1gFnPc4
2020	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSÚ	ČSÚ	kA
na	na	k7c4
volby	volba	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
volby	volba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSÚ	ČSÚ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČSSD	ČSSD	kA
přijde	přijít	k5eAaPmIp3nS
po	po	k7c6
senátních	senátní	k2eAgFnPc6d1
volbách	volba	k1gFnPc6
o	o	k7c4
devět	devět	k4xCc4
milionů	milion	k4xCgInPc2
ročně	ročně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Polepší	polepšit	k5eAaPmIp3nS
si	se	k3xPyFc3
hlavně	hlavně	k9
Starostové	Starosta	k1gMnPc1
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
<g/>
.	.	kIx.
10	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
https://volby.cz/pls/senat/se2111?xjazyk=CZ&	https://volby.cz/pls/senat/se2111?xjazyk=CZ&	k?
<g/>
↑	↑	k?
Lukáš	Lukáš	k1gMnSc1
Wagenknecht	Wagenknecht	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Senát	senát	k1gInSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
senat	senat	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Ladislav	Ladislav	k1gMnSc1
Kos	Kos	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Senát	senát	k1gInSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
senat	senat	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Renata	Renata	k1gFnSc1
Chmelová	Chmelová	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Senát	senát	k1gInSc1
Parlamentu	parlament	k1gInSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
senat	senat	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
<g/>
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
konané	konaný	k2eAgFnPc4d1
na	na	k7c6
území	území	k1gNnSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
ve	v	k7c6
dnech	den	k1gInPc6
23.05	23.05	k4
<g/>
.	.	kIx.
–	–	k?
24.05	24.05	k4
<g/>
.2014	.2014	k4
volby	volba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
Evropského	evropský	k2eAgInSc2d1
parlamentu	parlament	k1gInSc2
konané	konaný	k2eAgFnPc4d1
na	na	k7c6
území	území	k1gNnSc6
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
ve	v	k7c6
dnech	den	k1gInPc6
24.05	24.05	k4
<g/>
.	.	kIx.
–	–	k?
25.05	25.05	k4
<g/>
.2019	.2019	k4
volby	volba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
volby	volba	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSÚ	ČSÚ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
volby	volba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSÚ	ČSÚ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
volby	volba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSÚ	ČSÚ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
volby	volba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSÚ	ČSÚ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
1	#num#	k4
2	#num#	k4
Za	za	k7c4
mandáty	mandát	k1gInPc4
zaplatí	zaplatit	k5eAaPmIp3nS
stát	stát	k5eAaPmF,k5eAaImF
stranám	strana	k1gFnPc3
přes	přes	k7c4
168	#num#	k4
milionů	milion	k4xCgInPc2
korun	koruna	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Nejvíc	nejvíc	k6eAd1,k6eAd3
ztratí	ztratit	k5eAaPmIp3nS
ČSSD	ČSSD	kA
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
4	#num#	k4
<g/>
.	.	kIx.
října	říjen	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
<g/>
↑	↑	k?
volby	volba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSÚ	ČSÚ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
volby	volba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSÚ	ČSÚ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
zastupitelstev	zastupitelstvo	k1gNnPc2
obcí	obec	k1gFnPc2
v	v	k7c6
Česku	Česko	k1gNnSc6
2014	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSÚ	ČSÚ	kA
na	na	k7c4
volby	volba	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
volby	volba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSÚ	ČSÚ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Volby	volba	k1gFnPc1
do	do	k7c2
zastupitelstev	zastupitelstvo	k1gNnPc2
obcí	obec	k1gFnPc2
v	v	k7c6
Česku	Česko	k1gNnSc6
2014	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSÚ	ČSÚ	kA
na	na	k7c4
volby	volba	k1gFnPc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2014	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
24	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
volby	volba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSÚ	ČSÚ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
volby	volba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSÚ	ČSÚ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
volby	volba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
ČSÚ	ČSÚ	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Výsledky	výsledek	k1gInPc1
voleb	volba	k1gFnPc2
za	za	k7c4
území	území	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Volby	volba	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
10	#num#	k4
<g/>
-	-	kIx~
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
(	(	kIx(
<g/>
česky	česky	k6eAd1
<g/>
)	)	kIx)
↑	↑	k?
Chci	chtít	k5eAaImIp1nS
prachy	prach	k1gInPc4
<g/>
,	,	kIx,
napíšu	napsat	k5eAaPmIp1nS,k5eAaBmIp1nS
zákon	zákon	k1gInSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Unikla	uniknout	k5eAaPmAgFnS
pracovní	pracovní	k2eAgFnSc1d1
verze	verze	k1gFnSc1
novely	novela	k1gFnSc2
autorského	autorský	k2eAgInSc2d1
zákona	zákon	k1gInSc2
<g/>
,	,	kIx,
piratskenoviny	piratskenovina	k1gFnSc2
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
,	,	kIx,
osa	osa	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
↑	↑	k?
Jiří	Jiří	k1gMnSc1
Srstka	srstka	k1gFnSc1
<g/>
:	:	kIx,
Autorské	autorský	k2eAgNnSc1d1
právo	právo	k1gNnSc1
a	a	k8xC
knihovny	knihovna	k1gFnPc1
<g/>
↑	↑	k?
Kalousek	Kalousek	k1gMnSc1
<g/>
:	:	kIx,
Piráti	pirát	k1gMnPc1
přiznali	přiznat	k5eAaPmAgMnP
vlajku	vlajka	k1gFnSc4
a	a	k8xC
plují	plout	k5eAaImIp3nP
do	do	k7c2
boje	boj	k1gInSc2
proti	proti	k7c3
kapitalismu	kapitalismus	k1gInSc2
a	a	k8xC
NATO	NATO	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
FORUM	forum	k1gNnSc1
24	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Forum	forum	k1gNnSc1
24	#num#	k4
<g/>
,	,	kIx,
19	#num#	k4
<g/>
.	.	kIx.
února	únor	k1gInSc2
2020	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Podporují	podporovat	k5eAaImIp3nP
Piráti	pirát	k1gMnPc1
Česko	Česko	k1gNnSc4
v	v	k7c6
NATO	NATO	kA
<g/>
?	?	kIx.
</s>
<s desamb="1">
Výrok	výrok	k1gInSc1
o	o	k7c6
nesmyslných	smyslný	k2eNgInPc6d1
výdajích	výdaj	k1gInPc6
vyvolal	vyvolat	k5eAaPmAgInS
pobouření	pobouření	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Echo	echo	k1gNnSc1
<g/>
24	#num#	k4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Echo	echo	k1gNnSc1
Media	medium	k1gNnSc2
<g/>
,	,	kIx,
12	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Otevřený	otevřený	k2eAgInSc1d1
magistrát	magistrát	k1gInSc1
se	se	k3xPyFc4
při	při	k7c6
schvalování	schvalování	k1gNnSc6
metra	metr	k1gMnSc4
D	D	kA
zavřel	zavřít	k5eAaPmAgMnS
<g/>
,	,	kIx,
radní	radní	k1gMnPc1
hlasovali	hlasovat	k5eAaImAgMnP
tajně	tajně	k6eAd1
<g/>
.	.	kIx.
iDNES	iDNES	k?
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
MAFRA	MAFRA	kA
<g/>
,	,	kIx,
22	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Popření	popření	k1gNnSc1
sebe	sebe	k3xPyFc4
sama	sám	k3xTgMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Piráti	pirát	k1gMnPc1
v	v	k7c6
Praze	Praha	k1gFnSc6
rozhodují	rozhodovat	k5eAaImIp3nP
o	o	k7c6
miliardách	miliarda	k4xCgFnPc6
tajně	tajně	k6eAd1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Týden	týden	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
EMPRESA	EMPRESA	kA
MEDIA	medium	k1gNnSc2
<g/>
,	,	kIx,
22	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
2019	#num#	k4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Investigativní	investigativní	k2eAgFnSc1d1
žurnalistika	žurnalistika	k1gFnSc1
</s>
<s>
Otevřená	otevřený	k2eAgFnSc1d1
společnost	společnost	k1gFnSc1
</s>
<s>
Rekonstrukce	rekonstrukce	k1gFnSc1
státu	stát	k1gInSc2
</s>
<s>
Transparency	Transparency	k1gInPc1
International	International	k1gFnSc2
–	–	k?
Česká	český	k2eAgFnSc1d1
republika	republika	k1gFnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Česká	český	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
České	český	k2eAgFnSc2d1
pirátské	pirátský	k2eAgFnPc4d1
strany	strana	k1gFnSc2
</s>
<s>
Česká	český	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
na	na	k7c6
Facebooku	Facebook	k1gInSc6
</s>
<s>
Česká	český	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
na	na	k7c4
YouTube	YouTub	k1gInSc5
</s>
<s>
Česká	český	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
na	na	k7c4
Flickru	Flickra	k1gFnSc4
</s>
<s>
Pirátské	pirátský	k2eAgInPc1d1
listy	list	k1gInPc1
–	–	k?
stranické	stranický	k2eAgFnSc2d1
noviny	novina	k1gFnSc2
</s>
<s>
Týden	týden	k1gInSc1
Živě	živě	k6eAd1
<g/>
:	:	kIx,
Česká	český	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
rozhovor	rozhovor	k1gInSc1
s	s	k7c7
iniciátorem	iniciátor	k1gMnSc7
platformy	platforma	k1gFnSc2
Jiřím	Jiří	k1gMnSc7
Kadeřávkem	Kadeřávek	k1gMnSc7
pro	pro	k7c4
zive	zive	k1gFnSc4
<g/>
.	.	kIx.
<g/>
cz	cz	k?
(	(	kIx(
<g/>
flashvideo	flashvideo	k1gMnSc1
a	a	k8xC
MP	MP	kA
<g/>
3	#num#	k4
<g/>
)	)	kIx)
(	(	kIx(
<g/>
na	na	k7c6
začátku	začátek	k1gInSc6
cca	cca	kA
30	#num#	k4
<g/>
s	s	k7c7
reklama	reklama	k1gFnSc1
<g/>
)	)	kIx)
</s>
<s>
BASTLOVÁ	BASTLOVÁ	kA
<g/>
,	,	kIx,
Marie	Marie	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Piráti	pirát	k1gMnPc1
na	na	k7c6
cestě	cesta	k1gFnSc6
za	za	k7c7
pokladem	poklad	k1gInSc7
aneb	aneb	k?
Pusťte	pustit	k5eAaPmRp2nP
nás	my	k3xPp1nPc4
na	na	k7c4
ně	on	k3xPp3gMnPc4
<g/>
!	!	kIx.
</s>
<s desamb="1">
<g/>
.	.	kIx.
</s>
<s desamb="1">
Neovlivní	ovlivnit	k5eNaPmIp3nS
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dead	Dead	k1gMnSc1
Line	linout	k5eAaImIp3nS
Media	medium	k1gNnPc4
<g/>
,	,	kIx,
2017-12-19	2017-12-19	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2017	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
21	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
–	–	k?
článek	článek	k1gInSc1
o	o	k7c6
historii	historie	k1gFnSc6
strany	strana	k1gFnSc2
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Česká	český	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Předsedové	předseda	k1gMnPc1
strany	strana	k1gFnPc1
</s>
<s>
Kamil	Kamil	k1gMnSc1
Horký	Horký	k1gMnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ivan	Ivan	k1gMnSc1
Bartoš	Bartoš	k1gMnSc1
(	(	kIx(
<g/>
2009	#num#	k4
<g/>
–	–	k?
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jakub	Jakub	k1gMnSc1
Michálek	Michálek	k1gMnSc1
(	(	kIx(
<g/>
úřadující	úřadující	k2eAgMnSc1d1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ivan	Ivan	k1gMnSc1
Bartoš	Bartoš	k1gMnSc1
(	(	kIx(
<g/>
2013	#num#	k4
<g/>
–	–	k?
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Jana	Jana	k1gFnSc1
Michailidu	Michailid	k1gInSc2
(	(	kIx(
<g/>
úřadující	úřadující	k2eAgMnSc1d1
<g/>
)	)	kIx)
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Lukáš	Lukáš	k1gMnSc1
Černohorský	Černohorský	k1gMnSc1
(	(	kIx(
<g/>
2014	#num#	k4
<g/>
–	–	k?
<g/>
2016	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ivan	Ivan	k1gMnSc1
Bartoš	Bartoš	k1gMnSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
–	–	k?
<g/>
doposud	doposud	k6eAd1
<g/>
)	)	kIx)
Volby	volba	k1gFnPc1
předsedů	předseda	k1gMnPc2
</s>
<s>
2009	#num#	k4
(	(	kIx(
<g/>
červen	červen	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2009	#num#	k4
(	(	kIx(
<g/>
říjen	říjen	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
2010	#num#	k4
</s>
<s>
2012	#num#	k4
</s>
<s>
2014	#num#	k4
</s>
<s>
2016	#num#	k4
</s>
<s>
2018	#num#	k4
</s>
<s>
2020	#num#	k4
Senátoři	senátor	k1gMnPc5
</s>
<s>
Adéla	Adéla	k1gFnSc1
Šípová	Šípová	k1gFnSc1
(	(	kIx(
<g/>
2020	#num#	k4
<g/>
–	–	k?
<g/>
2026	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
David	David	k1gMnSc1
Smoljak	Smoljak	k1gMnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
–	–	k?
<g/>
2024	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Lukáš	Lukáš	k1gMnSc1
Wagenknecht	Wagenknecht	k1gMnSc1
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
–	–	k?
<g/>
2024	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ladislav	Ladislav	k1gMnSc1
Kos	Kos	k1gMnSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
–	–	k?
<g/>
2022	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Renata	Renata	k1gFnSc1
Chmelová	Chmelová	k1gFnSc1
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
–	–	k?
<g/>
2022	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Libor	Libor	k1gMnSc1
Michálek	Michálek	k1gMnSc1
(	(	kIx(
<g/>
2012	#num#	k4
<g/>
–	–	k?
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
Poslanci	poslanec	k1gMnPc1
</s>
<s>
Jakub	Jakub	k1gMnSc1
Michálek	Michálek	k1gMnSc1
</s>
<s>
Dana	Dana	k1gFnSc1
Balcarová	Balcarová	k1gFnSc1
</s>
<s>
Ondřej	Ondřej	k1gMnSc1
Profant	profant	k1gInSc4
</s>
<s>
Olga	Olga	k1gFnSc1
Richterová	Richterová	k1gFnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Lipavský	Lipavský	k2eAgMnSc1d1
</s>
<s>
Ivan	Ivan	k1gMnSc1
Bartoš	Bartoš	k1gMnSc1
</s>
<s>
Lenka	Lenka	k1gFnSc1
Kozlová	Kozlová	k1gFnSc1
</s>
<s>
František	František	k1gMnSc1
Kopřiva	Kopřiva	k1gMnSc1
</s>
<s>
Lukáš	Lukáš	k1gMnSc1
Kolářík	Kolářík	k1gMnSc1
</s>
<s>
Lukáš	Lukáš	k1gMnSc1
Bartoň	Bartoň	k1gMnSc1
</s>
<s>
Petr	Petr	k1gMnSc1
Třešňák	Třešňák	k1gMnSc1
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Martínek	Martínek	k1gMnSc1
</s>
<s>
Martin	Martin	k1gMnSc1
Jiránek	Jiránek	k1gMnSc1
</s>
<s>
Mikuláš	Mikuláš	k1gMnSc1
Ferjenčík	Ferjenčík	k1gMnSc1
</s>
<s>
Jan	Jan	k1gMnSc1
Pošvář	Pošvář	k1gMnSc1
</s>
<s>
Radek	Radek	k1gMnSc1
Holomčík	Holomčík	k1gMnSc1
</s>
<s>
Tomáš	Tomáš	k1gMnSc1
Vymazal	Vymazal	k1gMnSc1
</s>
<s>
Vojtěch	Vojtěch	k1gMnSc1
Pikal	pikat	k5eAaImAgMnS
</s>
<s>
František	František	k1gMnSc1
Elfmark	Elfmark	k1gInSc4
</s>
<s>
Lukáš	Lukáš	k1gMnSc1
Černohorský	Černohorský	k1gMnSc1
</s>
<s>
Ondřej	Ondřej	k1gMnSc1
Polanský	Polanský	k1gMnSc1
</s>
<s>
František	František	k1gMnSc1
Navrkal	Navrkal	k1gMnSc1
Poslanci	poslanec	k1gMnPc1
EP	EP	kA
</s>
<s>
Marcel	Marcel	k1gMnSc1
Kolaja	Kolaja	k1gMnSc1
</s>
<s>
Markéta	Markéta	k1gFnSc1
Gregorová	Gregorová	k1gFnSc1
</s>
<s>
Mikuláš	Mikuláš	k1gMnSc1
Peksa	Peks	k1gMnSc2
Organizace	organizace	k1gFnSc2
</s>
<s>
Mladé	mladý	k2eAgNnSc1d1
Pirátstvo	Pirátstvo	k1gNnSc1
</s>
<s>
Pirátské	pirátský	k2eAgInPc1d1
listy	list	k1gInPc1
</s>
<s>
Pirátské	pirátský	k2eAgFnPc1d1
strany	strana	k1gFnPc1
Strany	strana	k1gFnSc2
</s>
<s>
Afrika	Afrika	k1gFnSc1
</s>
<s>
Maroko	Maroko	k1gNnSc1
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Afrika	Afrika	k1gFnSc1
•	•	k?
Tunis	Tunis	k1gInSc1
Amerika	Amerika	k1gFnSc1
</s>
<s>
Bolívie	Bolívie	k1gFnSc1
•	•	k?
Brazílie	Brazílie	k1gFnSc1
•	•	k?
Chile	Chile	k1gNnSc2
•	•	k?
Ekvádor	Ekvádor	k1gInSc1
•	•	k?
Guatemala	Guatemala	k1gFnSc1
•	•	k?
Kanada	Kanada	k1gFnSc1
•	•	k?
Kolumbie	Kolumbie	k1gFnSc1
•	•	k?
Kostarika	Kostarika	k1gFnSc1
•	•	k?
Mexiko	Mexiko	k1gNnSc1
•	•	k?
Peru	prát	k5eAaImIp1nS
•	•	k?
Spojené	spojený	k2eAgInPc4d1
státy	stát	k1gInPc4
americké	americký	k2eAgFnSc2d1
•	•	k?
Uruguay	Uruguay	k1gFnSc1
Asie	Asie	k1gFnSc2
</s>
<s>
Čína	Čína	k1gFnSc1
•	•	k?
Kazachstán	Kazachstán	k1gInSc1
•	•	k?
Nepál	Nepál	k1gInSc1
•	•	k?
Jižní	jižní	k2eAgFnSc1d1
Korea	Korea	k1gFnSc1
•	•	k?
Tchaj-wan	Tchaj-wan	k1gInSc1
Evropa	Evropa	k1gFnSc1
</s>
<s>
Belgie	Belgie	k1gFnSc1
•	•	k?
Bosna	Bosna	k1gFnSc1
a	a	k8xC
Hercegovina	Hercegovina	k1gFnSc1
•	•	k?
Bulharsko	Bulharsko	k1gNnSc1
•	•	k?
Česko	Česko	k1gNnSc1
•	•	k?
Černá	černat	k5eAaImIp3nS
Hora	hora	k1gFnSc1
•	•	k?
Chorvatsko	Chorvatsko	k1gNnSc1
•	•	k?
Dánsko	Dánsko	k1gNnSc1
•	•	k?
Estonsko	Estonsko	k1gNnSc1
•	•	k?
Finsko	Finsko	k1gNnSc1
•	•	k?
Francie	Francie	k1gFnSc2
•	•	k?
Galicie	Galicie	k1gFnSc2
•	•	k?
Irsko	Irsko	k1gNnSc4
•	•	k?
Island	Island	k1gInSc1
•	•	k?
Katalánsko	Katalánsko	k1gNnSc4
•	•	k?
Kypr	Kypr	k1gInSc1
•	•	k?
Itálie	Itálie	k1gFnSc2
•	•	k?
Litva	Litva	k1gFnSc1
•	•	k?
Lotyšsko	Lotyšsko	k1gNnSc1
•	•	k?
Lucembursko	Lucembursko	k1gNnSc1
•	•	k?
Maďarsko	Maďarsko	k1gNnSc1
•	•	k?
Německo	Německo	k1gNnSc1
•	•	k?
Nizozemsko	Nizozemsko	k1gNnSc1
•	•	k?
Polsko	Polsko	k1gNnSc1
•	•	k?
Portugalsko	Portugalsko	k1gNnSc1
•	•	k?
Rakousko	Rakousko	k1gNnSc1
•	•	k?
Rumunsko	Rumunsko	k1gNnSc1
•	•	k?
Rusko	Rusko	k1gNnSc1
•	•	k?
Řecko	Řecko	k1gNnSc1
•	•	k?
Slovensko	Slovensko	k1gNnSc1
•	•	k?
Slovinsko	Slovinsko	k1gNnSc1
•	•	k?
Španělsko	Španělsko	k1gNnSc1
•	•	k?
Spojené	spojený	k2eAgNnSc1d1
království	království	k1gNnSc1
•	•	k?
Srbsko	Srbsko	k1gNnSc1
•	•	k?
Švédsko	Švédsko	k1gNnSc1
•	•	k?
Švýcarsko	Švýcarsko	k1gNnSc1
•	•	k?
Turecko	Turecko	k1gNnSc1
•	•	k?
Ukrajina	Ukrajina	k1gFnSc1
Oceánie	Oceánie	k1gFnSc2
</s>
<s>
Austrálie	Austrálie	k1gFnSc1
•	•	k?
Nový	nový	k2eAgInSc1d1
Zéland	Zéland	k1gInSc1
</s>
<s>
Mezinárodní	mezinárodní	k2eAgInSc1d1
</s>
<s>
Evropská	evropský	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
•	•	k?
Pirátská	pirátský	k2eAgFnSc1d1
internacionála	internacionála	k1gFnSc1
Osobnosti	osobnost	k1gFnSc2
</s>
<s>
Samir	Samir	k1gMnSc1
Allioui	Allioui	k1gNnSc2
•	•	k?
Slim	Slima	k1gFnPc2
Amamou	Amama	k1gMnSc7
•	•	k?
Amelia	Amelia	k1gFnSc1
Andersdotter	Andersdotter	k1gMnSc1
•	•	k?
Carlos	Carlos	k1gMnSc1
Ayala	Ayala	k1gMnSc1
•	•	k?
Ivan	Ivan	k1gMnSc1
Bartoš	Bartoš	k1gMnSc1
•	•	k?
Angelika	Angelika	k1gFnSc1
Beer	Beer	k1gMnSc1
•	•	k?
Sven	Sven	k1gMnSc1
Clement	Clement	k1gMnSc1
•	•	k?
Grégory	Grégora	k1gFnSc2
Engels	Engels	k1gMnSc1
•	•	k?
Christian	Christian	k1gMnSc1
Engström	Engström	k1gMnSc1
•	•	k?
Rickard	Rickard	k1gMnSc1
Falkvinge	Falkving	k1gFnSc2
•	•	k?
Stefan	Stefan	k1gMnSc1
Flod	Flod	k1gMnSc1
•	•	k?
Harald	Harald	k1gInSc1
Haas	Haas	k1gInSc1
•	•	k?
Martin	Martin	k1gMnSc1
Haase	Haase	k1gFnSc2
•	•	k?
Dirk	Dirk	k1gMnSc1
Hillbrecht	Hillbrecht	k1gMnSc1
•	•	k?
Florian	Florian	k1gMnSc1
Hufsky	Hufska	k1gFnSc2
•	•	k?
Marcel	Marcel	k1gMnSc1
Kolaja	Kolaja	k1gMnSc1
•	•	k?
Christof	Christof	k1gMnSc1
Leng	Leng	k1gMnSc1
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Patrick	Patrick	k1gMnSc1
Mächler	Mächler	k1gMnSc1
•	•	k?
Pavel	Pavel	k1gMnSc1
Mayer	Mayer	k1gMnSc1
•	•	k?
Libor	Libor	k1gMnSc1
Michálek	Michálek	k1gMnSc1
•	•	k?
Jakub	Jakub	k1gMnSc1
Michálek	Michálek	k1gMnSc1
•	•	k?
Sebastian	Sebastian	k1gMnSc1
Nerz	Nerz	k1gMnSc1
•	•	k?
Mikkel	Mikkel	k1gMnSc1
Paulson	Paulson	k1gMnSc1
•	•	k?
Herbert	Herbert	k1gMnSc1
Rusche	Rusch	k1gFnSc2
•	•	k?
Jens	Jens	k1gInSc1
Seipenbusch	Seipenbusch	k1gInSc1
•	•	k?
Denis	Denisa	k1gFnPc2
Simonet	Simonet	k1gMnSc1
•	•	k?
Julia	Julius	k1gMnSc4
Reda	Red	k1gInSc2
•	•	k?
Jörg	Jörg	k1gInSc1
Tauss	Tauss	k1gInSc1
•	•	k?
Stanislav	Stanislav	k1gMnSc1
Shakirov	Shakirov	k1gInSc1
•	•	k?
Anna	Anna	k1gFnSc1
Troberg	Troberg	k1gInSc1
•	•	k?
Lola	Lola	k1gFnSc1
Voronina	Voronina	k1gFnSc1
•	•	k?
Henk	Henk	k1gInSc1
de	de	k?
Vries	Vries	k1gInSc1
•	•	k?
Jerry	Jerra	k1gFnSc2
Weyer	Weyer	k1gMnSc1
•	•	k?
Laurence	Laurence	k1gFnSc1
„	„	k?
<g/>
Loz	loza	k1gFnPc2
<g/>
“	“	k?
Kaye	Kaye	k1gNnSc4
•	•	k?
Pavel	Pavla	k1gFnPc2
Rassudov	Rassudov	k1gInSc4
Mládež	mládež	k1gFnSc4
</s>
<s>
Česko	Česko	k1gNnSc1
•	•	k?
Dánsko	Dánsko	k1gNnSc1
•	•	k?
Finsko	Finsko	k1gNnSc1
•	•	k?
Katalánsko	Katalánsko	k1gNnSc1
•	•	k?
Německo	Německo	k1gNnSc1
•	•	k?
Švédsko	Švédsko	k1gNnSc1
Příbuzná	příbuzná	k1gFnSc1
témata	téma	k1gNnPc4
</s>
<s>
Piratbyrå	Piratbyrå	k1gInSc1
•	•	k?
The	The	k1gMnSc5
Pirate	Pirat	k1gMnSc5
Bay	Bay	k1gMnSc5
•	•	k?
Anti-Counterfeiting	Anti-Counterfeiting	k1gInSc1
Trade	Trad	k1gInSc5
Agreement	Agreement	k1gInSc4
•	•	k?
Telecomix	Telecomix	k1gInSc1
</s>
<s>
Politické	politický	k2eAgFnPc1d1
strany	strana	k1gFnPc1
a	a	k8xC
hnutí	hnutí	k1gNnPc1
v	v	k7c6
České	český	k2eAgFnSc6d1
republice	republika	k1gFnSc6
Poslanecká	poslanecký	k2eAgFnSc1d1
sněmovna	sněmovna	k1gFnSc1
(	(	kIx(
<g/>
200	#num#	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
2017	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
ANO	ano	k9
2011	#num#	k4
(	(	kIx(
<g/>
78	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
63	#num#	k4
ANO	ano	k9
<g/>
,	,	kIx,
15	#num#	k4
bezpartijních	bezpartijní	k1gMnPc2
<g/>
)	)	kIx)
</s>
<s>
Občanská	občanský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
23	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
21	#num#	k4
ODS	ODS	kA
<g/>
,	,	kIx,
2	#num#	k4
bezpartijní	bezpartijní	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Česká	český	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
22	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Svoboda	Svoboda	k1gMnSc1
a	a	k8xC
přímá	přímý	k2eAgFnSc1d1
demokracie	demokracie	k1gFnSc1
(	(	kIx(
<g/>
19	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
18	#num#	k4
SPD	SPD	kA
<g/>
,	,	kIx,
1	#num#	k4
bezpartijní	bezpartijní	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Komunistická	komunistický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
Čech	Čechy	k1gFnPc2
a	a	k8xC
Moravy	Morava	k1gFnSc2
(	(	kIx(
<g/>
15	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Česká	český	k2eAgFnSc1d1
strana	strana	k1gFnSc1
sociálně	sociálně	k6eAd1
demokratická	demokratický	k2eAgFnSc1d1
(	(	kIx(
<g/>
14	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
13	#num#	k4
ČSSD	ČSSD	kA
<g/>
,	,	kIx,
1	#num#	k4
bezpartijní	bezpartijní	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
KDU-ČSL	KDU-ČSL	k?
(	(	kIx(
<g/>
10	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
TOP	topit	k5eAaImRp2nS
09	#num#	k4
(	(	kIx(
<g/>
7	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
6	#num#	k4
TOP	topit	k5eAaImRp2nS
0	#num#	k4
<g/>
9	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
bezpartijní	bezpartijní	k1gMnSc1
<g/>
)	)	kIx)
</s>
<s>
Starostové	Starosta	k1gMnPc1
a	a	k8xC
nezávislí	závislý	k2eNgMnPc1d1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
nezařazení	nezařazení	k1gNnSc1
(	(	kIx(
<g/>
6	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
3	#num#	k4
JAP	JAP	kA
<g/>
,	,	kIx,
3	#num#	k4
Trikolóra	trikolóra	k1gFnSc1
<g/>
)	)	kIx)
Senát	senát	k1gInSc1
(	(	kIx(
<g/>
81	#num#	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
2016	#num#	k4
<g/>
,	,	kIx,
2018	#num#	k4
<g/>
,	,	kIx,
2020	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
ODS	ODS	kA
a	a	k8xC
TOP	topit	k5eAaImRp2nS
09	#num#	k4
(	(	kIx(
<g/>
26	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
16	#num#	k4
ODS	ODS	kA
<g/>
,	,	kIx,
4	#num#	k4
TOP	topit	k5eAaImRp2nS
09	#num#	k4
a	a	k8xC
6	#num#	k4
bezpartijní	bezpartijní	k2eAgFnSc4d1
–	–	k?
3	#num#	k4
za	za	k7c4
ODS	ODS	kA
<g/>
,	,	kIx,
1	#num#	k4
za	za	k7c4
SEN	sen	k1gInSc4
21	#num#	k4
a	a	k8xC
2	#num#	k4
nezávislí	závislý	k2eNgMnPc1d1
<g/>
)	)	kIx)
</s>
<s>
Starostové	Starosta	k1gMnPc1
a	a	k8xC
nezávislí	závislý	k2eNgMnPc1d1
(	(	kIx(
<g/>
24	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
6	#num#	k4
STAN	stan	k1gInSc1
<g/>
,	,	kIx,
2	#num#	k4
SLK	SLK	kA
<g/>
,	,	kIx,
1	#num#	k4
Ostravak	Ostravak	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
OPAT	opat	k1gMnSc1
<g/>
,	,	kIx,
1	#num#	k4
MHS	MHS	kA
a	a	k8xC
13	#num#	k4
bezpartijních	bezpartijní	k1gMnPc2
–	–	k?
11	#num#	k4
za	za	k7c7
STAN	stan	k1gInSc1
<g/>
,	,	kIx,
1	#num#	k4
za	za	k7c4
SD	SD	kA
<g/>
–	–	k?
<g/>
SN	SN	kA
a	a	k8xC
1	#num#	k4
za	za	k7c4
TOP	topit	k5eAaImRp2nS
0	#num#	k4
<g/>
9	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
KDU-ČSL	KDU-ČSL	k?
a	a	k8xC
nezávislí	závislý	k2eNgMnPc1d1
(	(	kIx(
<g/>
12	#num#	k4
<g/>
,	,	kIx,
z	z	k7c2
toho	ten	k3xDgNnSc2
7	#num#	k4
KDU-ČSL	KDU-ČSL	k1gFnPc2
a	a	k8xC
5	#num#	k4
bezpartijní	bezpartijní	k2eAgFnSc4d1
–	–	k?
4	#num#	k4
za	za	k7c7
KDU-ČSL	KDU-ČSL	k1gFnSc7
<g/>
,	,	kIx,
1	#num#	k4
za	za	k7c4
NK	NK	kA
<g/>
)	)	kIx)
</s>
<s>
PROREGION	PROREGION	kA
(	(	kIx(
<g/>
9	#num#	k4
z	z	k7c2
toho	ten	k3xDgNnSc2
2	#num#	k4
ČSSD	ČSSD	kA
<g/>
,	,	kIx,
1	#num#	k4
ANO	ano	k9
a	a	k9
6	#num#	k4
bezpartijních	bezpartijní	k2eAgMnPc2d1
<g/>
,	,	kIx,
4	#num#	k4
za	za	k7c4
ANO	ano	k9
<g/>
,	,	kIx,
1	#num#	k4
za	za	k7c4
ČSSD	ČSSD	kA
a	a	k8xC
1	#num#	k4
za	za	k7c4
„	„	k?
<g/>
OSN	OSN	kA
<g/>
“	“	k?
<g/>
)	)	kIx)
</s>
<s>
SEN	sen	k1gInSc1
21	#num#	k4
a	a	k8xC
Piráti	pirát	k1gMnPc1
(	(	kIx(
<g/>
7	#num#	k4
z	z	k7c2
toho	ten	k3xDgNnSc2
1	#num#	k4
SEN	sena	k1gFnPc2
21	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
Zelení	zeleň	k1gFnPc2
<g/>
,	,	kIx,
1	#num#	k4
HPP	HPP	kA
<g/>
11	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
Česká	český	k2eAgFnSc1d1
pirátská	pirátský	k2eAgFnSc1d1
strana	strana	k1gFnSc1
a	a	k8xC
3	#num#	k4
bezpartijní	bezpartijní	k1gMnSc1
<g/>
;	;	kIx,
1	#num#	k4
za	za	k7c4
SEN	sen	k1gInSc4
21	#num#	k4
<g/>
,	,	kIx,
1	#num#	k4
za	za	k7c4
Piráty	pirát	k1gMnPc4
a	a	k8xC
1	#num#	k4
za	za	k7c4
HDK	HDK	kA
<g/>
)	)	kIx)
</s>
<s>
nezařazení	nezařazení	k1gNnSc1
(	(	kIx(
<g/>
3	#num#	k4
z	z	k7c2
toho	ten	k3xDgNnSc2
1	#num#	k4
S.	S.	kA
<g/>
cz	cz	k?
a	a	k8xC
2	#num#	k4
bezpartijní	bezpartijní	k2eAgFnSc4d1
–	–	k?
1	#num#	k4
nezávislí	závislý	k2eNgMnPc1d1
a	a	k8xC
1	#num#	k4
za	za	k7c4
Svobodní	svobodný	k2eAgMnPc1d1
<g/>
)	)	kIx)
Evropský	evropský	k2eAgInSc1d1
parlament	parlament	k1gInSc1
(	(	kIx(
<g/>
21	#num#	k4
<g/>
)	)	kIx)
<g/>
(	(	kIx(
<g/>
2019	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
ANO	ano	k9
(	(	kIx(
<g/>
6	#num#	k4
<g/>
)	)	kIx)
v	v	k7c4
RE	re	k9
</s>
<s>
ODS	ODS	kA
(	(	kIx(
<g/>
4	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
ECR	ECR	kA
</s>
<s>
Piráti	pirát	k1gMnPc1
(	(	kIx(
<g/>
3	#num#	k4
<g/>
)	)	kIx)
v	v	k7c4
Greens	Greens	k1gInSc4
<g/>
/	/	kIx~
<g/>
EFA	EFA	kA
</s>
<s>
TOP	topit	k5eAaImRp2nS
09	#num#	k4
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
a	a	k8xC
STAN	stan	k1gInSc1
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
EPP	EPP	kA
</s>
<s>
SPD	SPD	kA
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
ID	ido	k1gNnPc2
</s>
<s>
KDU-ČSL	KDU-ČSL	k?
(	(	kIx(
<g/>
2	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
EPP	EPP	kA
</s>
<s>
KSČM	KSČM	kA
(	(	kIx(
<g/>
1	#num#	k4
<g/>
)	)	kIx)
v	v	k7c6
GUE-NGL	GUE-NGL	k1gFnSc6
Neparlamentní	parlamentní	k2eNgFnSc2d1
</s>
<s>
Alternativa	alternativa	k1gFnSc1
</s>
<s>
ANK	ANK	kA
2020	#num#	k4
</s>
<s>
APAČI	Apač	k1gMnSc3
2017	#num#	k4
</s>
<s>
ČP	ČP	kA
</s>
<s>
EU	EU	kA
TROLL	troll	k1gMnSc1
</s>
<s>
DSSS	DSSS	kA
</s>
<s>
DV	DV	kA
2016	#num#	k4
</s>
<s>
HLAS	hlas	k1gInSc1
</s>
<s>
IO	IO	kA
</s>
<s>
JsmePRO	JsmePRO	k?
<g/>
!	!	kIx.
</s>
<s>
M	M	kA
</s>
<s>
NBPLK	NBPLK	kA
</s>
<s>
Nezávislí	závislý	k2eNgMnPc1d1
</s>
<s>
NeKa	NeKa	k6eAd1
</s>
<s>
ODA	ODA	kA
</s>
<s>
HPP	HPP	kA
</s>
<s>
PV	PV	kA
</s>
<s>
PZS	PZS	kA
</s>
<s>
Rozumní	rozumný	k2eAgMnPc1d1
</s>
<s>
SNK	SNK	kA
ED	ED	kA
</s>
<s>
SNK1	SNK1	k4
</s>
<s>
SPOLEHNUTÍ	spolehnutí	k1gNnSc1
</s>
<s>
Starostové	Starosta	k1gMnPc1
a	a	k8xC
osobnosti	osobnost	k1gFnPc1
pro	pro	k7c4
Moravu	Morava	k1gFnSc4
</s>
<s>
Strana	strana	k1gFnSc1
Práv	právo	k1gNnPc2
Občanů	občan	k1gMnPc2
</s>
<s>
Strana	strana	k1gFnSc1
soukromníků	soukromník	k1gMnPc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
</s>
<s>
T2020	T2020	k4
</s>
<s>
UFO	UFO	kA
</s>
<s>
USZ	USZ	kA
</s>
<s>
ZA	za	k7c4
OBČANY	občan	k1gMnPc4
</s>
<s>
Změna	změna	k1gFnSc1
</s>
<s>
Z	z	k7c2
2020	#num#	k4
Seznam	seznam	k1gInSc1
neparlamentních	parlamentní	k2eNgFnPc2d1
politických	politický	k2eAgFnPc2d1
stran	strana	k1gFnPc2
a	a	k8xC
hnutí	hnutí	k1gNnSc2
není	být	k5eNaImIp3nS
úplný	úplný	k2eAgInSc1d1
<g/>
.	.	kIx.
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kv	kv	k?
<g/>
2010580221	#num#	k4
|	|	kIx~
GND	GND	kA
<g/>
:	:	kIx,
115165485X	115165485X	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
147070139	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Politika	politika	k1gFnSc1
|	|	kIx~
Česko	Česko	k1gNnSc1
</s>
