<s>
Generální	generální	k2eAgFnSc1d1
inspekce	inspekce	k1gFnSc1
bezpečnostních	bezpečnostní	k2eAgInPc2d1
sborů	sbor	k1gInPc2
</s>
<s>
Na	na	k7c4
tento	tento	k3xDgInSc4
článek	článek	k1gInSc4
je	být	k5eAaImIp3nS
přesměrováno	přesměrován	k2eAgNnSc1d1
heslo	heslo	k1gNnSc1
GIBS	GIBS	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
GIBS	GIBS	kA
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Generální	generální	k2eAgFnSc1d1
inspekce	inspekce	k1gFnSc1
bezpečnostních	bezpečnostní	k2eAgInPc2d1
sborů	sbor	k1gInPc2
Znak	znak	k1gInSc1
Generální	generální	k2eAgFnSc2d1
inspekce	inspekce	k1gFnSc2
bezpečnostních	bezpečnostní	k2eAgInPc2d1
sborů	sbor	k1gInPc2
Vznik	vznik	k1gInSc1
</s>
<s>
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2012	#num#	k4
Právní	právní	k2eAgFnSc1d1
forma	forma	k1gFnSc1
</s>
<s>
organizační	organizační	k2eAgFnSc1d1
složka	složka	k1gFnSc1
státu	stát	k1gInSc2
Sídlo	sídlo	k1gNnSc1
</s>
<s>
Skokanská	skokanský	k2eAgFnSc1d1
2311	#num#	k4
<g/>
/	/	kIx~
<g/>
3	#num#	k4
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
<g/>
,	,	kIx,
169	#num#	k4
0	#num#	k4
<g/>
0	#num#	k4
<g/>
,	,	kIx,
Česko	Česko	k1gNnSc1
Souřadnice	souřadnice	k1gFnSc2
</s>
<s>
50	#num#	k4
<g/>
°	°	k?
<g/>
4	#num#	k4
<g/>
′	′	k?
<g/>
46,81	46,81	k4
<g/>
″	″	k?
s.	s.	k?
š.	š.	k?
<g/>
,	,	kIx,
14	#num#	k4
<g/>
°	°	k?
<g/>
22	#num#	k4
<g/>
′	′	k?
<g/>
29,16	29,16	k4
<g/>
″	″	k?
v.	v.	k?
d.	d.	k?
Ředitel	ředitel	k1gMnSc1
</s>
<s>
brig	briga	k1gFnPc2
<g/>
.	.	kIx.
gen.	gen.	kA
Radim	Radim	k1gMnSc1
Dragoun	dragoun	k1gMnSc1
Zaměstnanců	zaměstnanec	k1gMnPc2
</s>
<s>
308	#num#	k4
(	(	kIx(
<g/>
2018	#num#	k4
<g/>
)	)	kIx)
Oficiální	oficiální	k2eAgInSc1d1
web	web	k1gInSc1
</s>
<s>
www.gibs.cz	www.gibs.cz	k1gInSc1
Datová	datový	k2eAgFnSc1d1
schránka	schránka	k1gFnSc1
</s>
<s>
v	v	k7c6
<g/>
7	#num#	k4
<g/>
m	m	kA
<g/>
7926	#num#	k4
IČO	IČO	kA
</s>
<s>
72554495	#num#	k4
(	(	kIx(
<g/>
VR	vr	k0
<g/>
)	)	kIx)
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc1
můžou	můžou	k?
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Centrála	centrála	k1gFnSc1
GIBS	GIBS	kA
v	v	k7c6
Praze-Břevnově	Praze-Břevnov	k1gInSc6
</s>
<s>
Generální	generální	k2eAgFnSc1d1
inspekce	inspekce	k1gFnSc1
bezpečnostních	bezpečnostní	k2eAgInPc2d1
sborů	sbor	k1gInPc2
(	(	kIx(
<g/>
GIBS	GIBS	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
ozbrojený	ozbrojený	k2eAgInSc4d1
bezpečnostní	bezpečnostní	k2eAgInSc4d1
sbor	sbor	k1gInSc4
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
úkolem	úkol	k1gInSc7
je	být	k5eAaImIp3nS
kontrolní	kontrolní	k2eAgFnSc1d1
a	a	k8xC
vyšetřovací	vyšetřovací	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
příslušníků	příslušník	k1gMnPc2
a	a	k8xC
zaměstnanců	zaměstnanec	k1gMnPc2
některých	některý	k3yIgInPc2
českých	český	k2eAgInPc2d1
bezpečnostních	bezpečnostní	k2eAgInPc2d1
sborů	sbor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vznikla	vzniknout	k5eAaPmAgFnS
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2012	#num#	k4
na	na	k7c6
základě	základ	k1gInSc6
zákona	zákon	k1gInSc2
č.	č.	k?
341	#num#	k4
<g/>
/	/	kIx~
<g/>
2011	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
Generální	generální	k2eAgFnSc6d1
inspekci	inspekce	k1gFnSc6
bezpečnostních	bezpečnostní	k2eAgInPc2d1
sborů	sbor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
GIBS	GIBS	kA
provádí	provádět	k5eAaImIp3nS
kontrolu	kontrola	k1gFnSc4
<g/>
,	,	kIx,
odhalování	odhalování	k1gNnSc4
a	a	k8xC
vyšetřování	vyšetřování	k1gNnSc4
trestných	trestný	k2eAgInPc2d1
činů	čin	k1gInPc2
<g/>
,	,	kIx,
jestliže	jestliže	k8xS
je	on	k3xPp3gMnPc4
spáchal	spáchat	k5eAaPmAgMnS
příslušník	příslušník	k1gMnSc1
Policie	policie	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
celník	celník	k1gMnSc1
<g/>
,	,	kIx,
příslušník	příslušník	k1gMnSc1
Vězeňské	vězeňský	k2eAgFnSc2d1
správy	správa	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
nebo	nebo	k8xC
příslušník	příslušník	k1gMnSc1
GIBS	GIBS	kA
<g/>
,	,	kIx,
případně	případně	k6eAd1
civilní	civilní	k2eAgMnSc1d1
zaměstnanec	zaměstnanec	k1gMnSc1
těchto	tento	k3xDgInPc2
sborů	sbor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s>
Příslušníci	příslušník	k1gMnPc1
inspekce	inspekce	k1gFnSc2
jsou	být	k5eAaImIp3nP
ve	v	k7c6
služebním	služební	k2eAgInSc6d1
poměru	poměr	k1gInSc6
<g/>
,	,	kIx,
který	který	k3yIgInSc1,k3yQgInSc1,k3yRgInSc1
upravuje	upravovat	k5eAaImIp3nS
zákon	zákon	k1gInSc4
č.	č.	k?
361	#num#	k4
<g/>
/	/	kIx~
<g/>
2003	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
služebním	služební	k2eAgInSc6d1
poměru	poměr	k1gInSc6
příslušníků	příslušník	k1gMnPc2
bezpečnostních	bezpečnostní	k2eAgInPc2d1
sborů	sbor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Generální	generální	k2eAgFnSc1d1
inspekce	inspekce	k1gFnSc1
bezpečnostních	bezpečnostní	k2eAgInPc2d1
sborů	sbor	k1gInPc2
měla	mít	k5eAaImAgFnS
v	v	k7c6
roce	rok	k1gInSc6
2018	#num#	k4
celkem	celkem	k6eAd1
253	#num#	k4
příslušníků	příslušník	k1gMnPc2
a	a	k8xC
dalších	další	k2eAgNnPc2d1
55	#num#	k4
civilních	civilní	k2eAgMnPc2d1
zaměstnanců	zaměstnanec	k1gMnPc2
v	v	k7c6
pracovním	pracovní	k2eAgInSc6d1
poměru	poměr	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
1	#num#	k4
<g/>
]	]	kIx)
V	v	k7c6
čele	čelo	k1gNnSc6
Generální	generální	k2eAgFnSc2d1
inspekce	inspekce	k1gFnSc2
bezpečnostních	bezpečnostní	k2eAgInPc2d1
sborů	sbor	k1gInPc2
stojí	stát	k5eAaImIp3nS
ředitel	ředitel	k1gMnSc1
<g/>
,	,	kIx,
kterým	který	k3yIgMnSc7,k3yQgMnSc7,k3yRgMnSc7
je	být	k5eAaImIp3nS
od	od	k7c2
roku	rok	k1gInSc2
2018	#num#	k4
Radim	Radim	k1gMnSc1
Dragoun	dragoun	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s>
Účel	účel	k1gInSc1
</s>
<s>
Generální	generální	k2eAgFnSc1d1
inspekce	inspekce	k1gFnSc1
bezpečnostních	bezpečnostní	k2eAgInPc2d1
sborů	sbor	k1gInPc2
funguje	fungovat	k5eAaImIp3nS
dle	dle	k7c2
zákona	zákon	k1gInSc2
č.	č.	k?
341	#num#	k4
<g/>
/	/	kIx~
<g/>
2011	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
ve	v	k7c6
znění	znění	k1gNnSc6
pozdějších	pozdní	k2eAgInPc2d2
předpisů	předpis	k1gInPc2
<g/>
,	,	kIx,
a	a	k8xC
má	mít	k5eAaImIp3nS
za	za	k7c4
úkol	úkol	k1gInSc4
zejména	zejména	k9
vyhledávat	vyhledávat	k5eAaImF
<g/>
,	,	kIx,
odhalovat	odhalovat	k5eAaImF
a	a	k8xC
vyšetřovat	vyšetřovat	k5eAaImF
skutečnosti	skutečnost	k1gFnPc4
nasvědčující	nasvědčující	k2eAgFnPc4d1
tomu	ten	k3xDgNnSc3
<g/>
,	,	kIx,
že	že	k8xS
byl	být	k5eAaImAgInS
spáchán	spáchat	k5eAaPmNgInS
trestný	trestný	k2eAgInSc1d1
čin	čin	k1gInSc1
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gMnSc7
pachatelem	pachatel	k1gMnSc7
je	být	k5eAaImIp3nS
příslušník	příslušník	k1gMnSc1
Policie	policie	k1gFnSc2
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
celník	celník	k1gMnSc1
<g/>
,	,	kIx,
příslušník	příslušník	k1gMnSc1
Vězeňské	vězeňský	k2eAgFnSc2d1
služby	služba	k1gFnSc2
<g/>
,	,	kIx,
příslušník	příslušník	k1gMnSc1
inspekce	inspekce	k1gFnSc2
anebo	anebo	k8xC
zaměstnanci	zaměstnanec	k1gMnPc1
těchto	tento	k3xDgInPc2
útvarů	útvar	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
GIBS	GIBS	kA
začala	začít	k5eAaPmAgFnS
fungovat	fungovat	k5eAaImF
1	#num#	k4
<g/>
.	.	kIx.
ledna	leden	k1gInSc2
2012	#num#	k4
<g/>
,	,	kIx,
do	do	k7c2
té	ten	k3xDgFnSc2
doby	doba	k1gFnSc2
existovaly	existovat	k5eAaImAgFnP
zvláštní	zvláštní	k2eAgFnPc1d1
inspekce	inspekce	k1gFnPc1
pro	pro	k7c4
policii	policie	k1gFnSc4
<g/>
,	,	kIx,
zvláštní	zvláštní	k2eAgFnSc4d1
pro	pro	k7c4
vězeňské	vězeňský	k2eAgFnPc4d1
služby	služba	k1gFnPc4
či	či	k8xC
celníky	celník	k1gMnPc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
2	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Vedení	vedení	k1gNnSc1
</s>
<s>
V	v	k7c6
čele	čelo	k1gNnSc6
Generální	generální	k2eAgFnSc2d1
inspekce	inspekce	k1gFnSc2
bezpečnostních	bezpečnostní	k2eAgInPc2d1
sborů	sbor	k1gInPc2
stojí	stát	k5eAaImIp3nS
ředitel	ředitel	k1gMnSc1
<g/>
,	,	kIx,
kterého	který	k3yIgMnSc4,k3yQgMnSc4,k3yRgMnSc4
jmenuje	jmenovat	k5eAaImIp3nS,k5eAaBmIp3nS
a	a	k8xC
odvolává	odvolávat	k5eAaImIp3nS
na	na	k7c4
návrh	návrh	k1gInSc4
vlády	vláda	k1gFnSc2
a	a	k8xC
po	po	k7c6
projednání	projednání	k1gNnSc6
ve	v	k7c6
výboru	výbor	k1gInSc6
Poslanecké	poslanecký	k2eAgFnSc2d1
sněmovny	sněmovna	k1gFnSc2
příslušném	příslušný	k2eAgInSc6d1
ve	v	k7c6
věcech	věc	k1gFnPc6
bezpečnosti	bezpečnost	k1gFnSc2
předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Tomu	ten	k3xDgMnSc3
je	být	k5eAaImIp3nS
ředitel	ředitel	k1gMnSc1
z	z	k7c2
výkonu	výkon	k1gInSc2
své	svůj	k3xOyFgFnSc2
funkce	funkce	k1gFnSc2
odpovědný	odpovědný	k2eAgMnSc1d1
<g/>
.	.	kIx.
</s>
<s>
Seznam	seznam	k1gInSc1
ředitelů	ředitel	k1gMnPc2
</s>
<s>
plk.	plk.	kA
Mgr.	Mgr.	kA
Ing.	ing.	kA
Ivan	Ivan	k1gMnSc1
Bílek	Bílek	k1gMnSc1
(	(	kIx(
<g/>
prozatímní	prozatímní	k2eAgMnSc1d1
ředitel	ředitel	k1gMnSc1
25	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2011	#num#	k4
<g/>
[	[	kIx(
<g/>
3	#num#	k4
<g/>
]	]	kIx)
–	–	k?
30	#num#	k4
<g/>
.	.	kIx.
června	červen	k1gInSc2
2012	#num#	k4
<g/>
,	,	kIx,
ředitel	ředitel	k1gMnSc1
1	#num#	k4
<g/>
.	.	kIx.
července	červenec	k1gInSc2
2012	#num#	k4
<g/>
[	[	kIx(
<g/>
4	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
5	#num#	k4
<g/>
]	]	kIx)
–	–	k?
30	#num#	k4
<g/>
.	.	kIx.
listopadu	listopad	k1gInSc2
2015	#num#	k4
<g/>
[	[	kIx(
<g/>
6	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
</s>
<s>
plk.	plk.	kA
JUDr.	JUDr.	kA
Michal	Michal	k1gMnSc1
Murín	Murín	k1gMnSc1
(	(	kIx(
<g/>
7	#num#	k4
<g/>
.	.	kIx.
prosince	prosinec	k1gInSc2
2015	#num#	k4
<g/>
[	[	kIx(
<g/>
7	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
8	#num#	k4
<g/>
]	]	kIx)
–	–	k?
30	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2018	#num#	k4
<g/>
[	[	kIx(
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
10	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
</s>
<s>
brig	briga	k1gFnPc2
<g/>
.	.	kIx.
gen.	gen.	kA
JUDr.	JUDr.	kA
Bc.	Bc.	k1gMnSc1
Radim	Radim	k1gMnSc1
Dragoun	dragoun	k1gMnSc1
<g/>
,	,	kIx,
Ph	Ph	kA
<g/>
.	.	kIx.
<g/>
D.	D.	kA
(	(	kIx(
<g/>
od	od	k7c2
1	#num#	k4
<g/>
.	.	kIx.
září	září	k1gNnSc2
2018	#num#	k4
<g/>
[	[	kIx(
<g/>
11	#num#	k4
<g/>
]	]	kIx)
<g/>
)	)	kIx)
</s>
<s>
Historie	historie	k1gFnSc1
</s>
<s>
Při	při	k7c6
politické	politický	k2eAgFnSc6d1
krizi	krize	k1gFnSc6
v	v	k7c6
roce	rok	k1gInSc6
2010	#num#	k4
požadovala	požadovat	k5eAaImAgFnS
strana	strana	k1gFnSc1
Věci	věc	k1gFnSc2
veřejné	veřejný	k2eAgFnSc2d1
(	(	kIx(
<g/>
VV	VV	kA
<g/>
)	)	kIx)
odvolání	odvolání	k1gNnSc4
tehdejšího	tehdejší	k2eAgMnSc2d1
policejního	policejní	k2eAgMnSc2d1
prezidenta	prezident	k1gMnSc2
Oldřicha	Oldřich	k1gMnSc2
Martinů	Martinů	k1gMnSc1
a	a	k8xC
jejich	jejich	k3xOp3gMnSc1
tehdejší	tehdejší	k2eAgMnSc1d1
ministr	ministr	k1gMnSc1
vnitra	vnitro	k1gNnSc2
Radek	Radek	k1gMnSc1
John	John	k1gMnSc1
požadoval	požadovat	k5eAaImAgMnS
jmenování	jmenování	k1gNnSc4
nového	nový	k2eAgMnSc2d1
policejního	policejní	k2eAgMnSc2d1
prezidenta	prezident	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Situace	situace	k1gFnSc1
se	se	k3xPyFc4
vyhrotila	vyhrotit	k5eAaPmAgFnS
až	až	k9
do	do	k7c2
té	ten	k3xDgFnSc2
míry	míra	k1gFnSc2
<g/>
,	,	kIx,
že	že	k8xS
musel	muset	k5eAaImAgMnS
do	do	k7c2
jednání	jednání	k1gNnSc2
vstoupit	vstoupit	k5eAaPmF
prezident	prezident	k1gMnSc1
ČR	ČR	kA
Václav	Václav	k1gMnSc1
Klaus	Klaus	k1gMnSc1
a	a	k8xC
došlo	dojít	k5eAaPmAgNnS
k	k	k7c3
neveřejné	veřejný	k2eNgFnSc3d1
dohodě	dohoda	k1gFnSc3
<g/>
.	.	kIx.
</s>
<s desamb="1">
S	s	k7c7
odstupem	odstup	k1gInSc7
času	čas	k1gInSc2
se	se	k3xPyFc4
ukázalo	ukázat	k5eAaPmAgNnS
<g/>
,	,	kIx,
že	že	k8xS
obsahem	obsah	k1gInSc7
této	tento	k3xDgFnSc2
dohody	dohoda	k1gFnSc2
byla	být	k5eAaImAgFnS
skutečnost	skutečnost	k1gFnSc1
<g/>
,	,	kIx,
že	že	k8xS
VV	VV	kA
vyberou	vybrat	k5eAaPmIp3nP
nového	nový	k2eAgMnSc2d1
policejního	policejní	k2eAgMnSc2d1
prezidenta	prezident	k1gMnSc2
a	a	k8xC
Občanská	občanský	k2eAgFnSc1d1
demokratická	demokratický	k2eAgFnSc1d1
strana	strana	k1gFnSc1
(	(	kIx(
<g/>
ODS	ODS	kA
<g/>
)	)	kIx)
šéfa	šéf	k1gMnSc2
nově	nově	k6eAd1
vytvořené	vytvořený	k2eAgFnSc2d1
Generální	generální	k2eAgFnSc2d1
inspekce	inspekce	k1gFnSc2
bezpečnostních	bezpečnostní	k2eAgInPc2d1
sborů	sbor	k1gInPc2
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
12	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Na	na	k7c6
počátku	počátek	k1gInSc6
roku	rok	k1gInSc2
2018	#num#	k4
se	se	k3xPyFc4
GIBS	GIBS	kA
dostala	dostat	k5eAaPmAgFnS
do	do	k7c2
oblasti	oblast	k1gFnSc2
zájmu	zájem	k1gInSc2
veřejnosti	veřejnost	k1gFnSc2
<g/>
,	,	kIx,
když	když	k8xS
premiér	premiér	k1gMnSc1
Andrej	Andrej	k1gMnSc1
Babiš	Babiš	k1gMnSc1
projevil	projevit	k5eAaPmAgMnS
snahu	snaha	k1gFnSc4
zbavit	zbavit	k5eAaPmF
ředitele	ředitel	k1gMnSc4
GIBS	GIBS	kA
Michala	Michal	k1gMnSc4
Murína	Murín	k1gMnSc4
jeho	jeho	k3xOp3gMnSc3
funkce	funkce	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
březnu	březen	k1gInSc6
2018	#num#	k4
s	s	k7c7
ním	on	k3xPp3gNnSc7
zahájil	zahájit	k5eAaPmAgMnS
kázeňské	kázeňský	k2eAgNnSc4d1
řízení	řízení	k1gNnSc4
o	o	k7c6
přestupku	přestupek	k1gInSc6
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
13	#num#	k4
<g/>
]	]	kIx)
Tento	tento	k3xDgInSc1
jeho	jeho	k3xOp3gInSc1
krok	krok	k1gInSc1
vzbudil	vzbudit	k5eAaPmAgInS
kontroverze	kontroverze	k1gFnPc4
<g/>
,	,	kIx,
jednak	jednak	k8xC
kvůli	kvůli	k7c3
obavám	obava	k1gFnPc3
z	z	k7c2
domnělé	domnělý	k2eAgFnSc2d1
snahy	snaha	k1gFnSc2
Babiše	Babiš	k1gInSc2
ovlivnit	ovlivnit	k5eAaPmF
běžící	běžící	k2eAgNnSc4d1
vyšetřování	vyšetřování	k1gNnSc4
jeho	jeho	k3xOp3gFnSc2
osoby	osoba	k1gFnSc2
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
podezřením	podezření	k1gNnSc7
z	z	k7c2
dotačního	dotační	k2eAgInSc2d1
podvodu	podvod	k1gInSc2
v	v	k7c6
kauze	kauza	k1gFnSc6
Čapí	čapět	k5eAaImIp3nS
hnízdo	hnízdo	k1gNnSc1
<g/>
,	,	kIx,
<g />
.	.	kIx.
</s>
<s hack="1">
jednak	jednak	k8xC
proto	proto	k8xC
<g/>
,	,	kIx,
že	že	k8xS
v	v	k7c6
tomto	tento	k3xDgInSc6
případě	případ	k1gInSc6
předseda	předseda	k1gMnSc1
vlády	vláda	k1gFnSc2
použil	použít	k5eAaPmAgMnS
zákon	zákon	k1gInSc4
platný	platný	k2eAgInSc4d1
pro	pro	k7c4
řadové	řadový	k2eAgMnPc4d1
policisty	policista	k1gMnPc4
<g/>
,	,	kIx,
kterým	který	k3yQgMnPc3,k3yIgMnPc3,k3yRgMnPc3
Michal	Michal	k1gMnSc1
Murín	Murín	k1gMnSc1
nebyl	být	k5eNaImAgMnS
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
14	#num#	k4
<g/>
]	]	kIx)
Spekulacím	spekulace	k1gFnPc3
měl	mít	k5eAaImAgInS
nahrávat	nahrávat	k5eAaImF
i	i	k9
fakt	fakt	k1gInSc1
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
po	po	k7c6
tomto	tento	k3xDgInSc6
kroku	krok	k1gInSc6
GIBS	GIBS	kA
skutečně	skutečně	k6eAd1
začala	začít	k5eAaPmAgFnS
zajímat	zajímat	k5eAaImF
o	o	k7c4
novináře	novinář	k1gMnPc4
<g/>
,	,	kIx,
kteří	který	k3yRgMnPc1,k3yQgMnPc1,k3yIgMnPc1
v	v	k7c6
minulosti	minulost	k1gFnSc6
informovali	informovat	k5eAaBmAgMnP
veřejnost	veřejnost	k1gFnSc4
o	o	k7c6
některých	některý	k3yIgFnPc6
jednotlivostech	jednotlivost	k1gFnPc6
v	v	k7c6
souvislosti	souvislost	k1gFnSc6
s	s	k7c7
Babišovou	Babišův	k2eAgFnSc7d1
kauzou	kauza	k1gFnSc7
Čapí	čapět	k5eAaImIp3nS
hnízdo	hnízdo	k1gNnSc1
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
15	#num#	k4
<g/>
]	]	kIx)
<g/>
[	[	kIx(
<g/>
16	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Dne	den	k1gInSc2
17	#num#	k4
<g/>
.	.	kIx.
dubna	duben	k1gInSc2
2018	#num#	k4
se	se	k3xPyFc4
po	po	k7c6
dlouhých	dlouhý	k2eAgFnPc6d1
debatách	debata	k1gFnPc6
a	a	k8xC
spekulacích	spekulace	k1gFnPc6
rozhodl	rozhodnout	k5eAaPmAgMnS
někdejší	někdejší	k2eAgMnSc1d1
šéf	šéf	k1gMnSc1
GIBS	GIBS	kA
Michal	Michal	k1gMnSc1
Murín	Murín	k1gMnSc1
rezignovat	rezignovat	k5eAaBmF
ke	k	k7c3
konci	konec	k1gInSc3
dubna	duben	k1gInSc2
na	na	k7c4
svou	svůj	k3xOyFgFnSc4
funkci	funkce	k1gFnSc4
<g/>
.	.	kIx.
<g/>
[	[	kIx(
<g/>
17	#num#	k4
<g/>
]	]	kIx)
Oficiálním	oficiální	k2eAgInSc7d1
důvodem	důvod	k1gInSc7
byly	být	k5eAaImAgInP
zdravotní	zdravotní	k2eAgInPc1d1
problémy	problém	k1gInPc1
<g/>
,	,	kIx,
protože	protože	k8xS
Murín	Murín	k1gMnSc1
byl	být	k5eAaImAgMnS
několik	několik	k4yIc4
dní	den	k1gInPc2
před	před	k7c7
rezignací	rezignace	k1gFnSc7
hospitalizován	hospitalizován	k2eAgInSc1d1
v	v	k7c6
jedné	jeden	k4xCgFnSc6
z	z	k7c2
pražských	pražský	k2eAgFnPc2d1
nemocnic	nemocnice	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Opoziční	opoziční	k2eAgFnPc1d1
strany	strana	k1gFnPc1
se	se	k3xPyFc4
však	však	k9
domnívaly	domnívat	k5eAaImAgFnP
<g/>
,	,	kIx,
že	že	k8xS
pravým	pravý	k2eAgInSc7d1
důvodem	důvod	k1gInSc7
byl	být	k5eAaImAgInS
tlak	tlak	k1gInSc1
tehdejšího	tehdejší	k2eAgMnSc2d1
premiéra	premiér	k1gMnSc2
Andreje	Andrej	k1gMnSc2
Babiše	Babiš	k1gMnSc2
na	na	k7c4
Michala	Michal	k1gMnSc4
Murína	Murín	k1gMnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dle	dle	k7c2
opozičních	opoziční	k2eAgFnPc2d1
stran	strana	k1gFnPc2
se	se	k3xPyFc4
mělo	mít	k5eAaImAgNnS
jednat	jednat	k5eAaImF
o	o	k7c4
pokus	pokus	k1gInSc4
premiéra	premiér	k1gMnSc4
v	v	k7c6
demisi	demise	k1gFnSc6
o	o	k7c4
ovládnutí	ovládnutí	k1gNnSc4
policie	policie	k1gFnSc2
a	a	k8xC
kontroly	kontrola	k1gFnSc2
nad	nad	k7c7
ní	on	k3xPp3gFnSc7
<g/>
.	.	kIx.
</s>
<s>
Služební	služební	k2eAgFnSc1d1
medaile	medaile	k1gFnSc1
</s>
<s>
Podle	podle	k7c2
vyhlášky	vyhláška	k1gFnSc2
Ministerstva	ministerstvo	k1gNnSc2
vnitra	vnitro	k1gNnSc2
č.	č.	k?
433	#num#	k4
<g/>
/	/	kIx~
<g/>
2004	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
uděluje	udělovat	k5eAaImIp3nS
Generální	generální	k2eAgFnSc1d1
inspekce	inspekce	k1gFnSc1
bezpečnostních	bezpečnostní	k2eAgInPc2d1
sborů	sbor	k1gInPc2
svým	svůj	k3xOyFgMnPc3
příslušníkům	příslušník	k1gMnPc3
následující	následující	k2eAgFnPc4d1
dvě	dva	k4xCgFnPc4
služební	služební	k2eAgFnPc4d1
medaile	medaile	k1gFnPc4
<g/>
:	:	kIx,
<g/>
[	[	kIx(
<g/>
18	#num#	k4
<g/>
]	]	kIx)
</s>
<s>
Za	za	k7c4
zásluhy	zásluha	k1gFnPc4
o	o	k7c4
bezpečnostZa	bezpečnostZus	k1gMnSc4
věrnost	věrnost	k1gFnSc1
<g/>
(	(	kIx(
<g/>
I.	I.	kA
<g/>
,	,	kIx,
II	II	kA
<g/>
.	.	kIx.
a	a	k8xC
III	III	kA
<g/>
.	.	kIx.
stupeň	stupeň	k1gInSc1
<g/>
)	)	kIx)
</s>
<s>
Odkazy	odkaz	k1gInPc1
</s>
<s>
Reference	reference	k1gFnPc1
</s>
<s>
↑	↑	k?
Zpráva	zpráva	k1gFnSc1
o	o	k7c6
činnosti	činnost	k1gFnSc6
Generální	generální	k2eAgFnSc2d1
inspekce	inspekce	k1gFnSc2
bezpečnostních	bezpečnostní	k2eAgInPc2d1
sborů	sbor	k1gInPc2
za	za	k7c4
rok	rok	k1gInSc4
2018	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gibs	Gibs	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
25	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
GIBS	GIBS	kA
-	-	kIx~
Co	co	k3yInSc4,k3yRnSc4,k3yQnSc4
je	být	k5eAaImIp3nS
a	a	k8xC
jaké	jaký	k3yQgNnSc1,k3yRgNnSc1,k3yIgNnSc1
má	mít	k5eAaImIp3nS
pravomoci	pravomoc	k1gFnPc4
Generální	generální	k2eAgFnSc2d1
inspekce	inspekce	k1gFnSc2
bezpečnostních	bezpečnostní	k2eAgInPc2d1
sborů	sbor	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Reflex	reflex	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
3	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
9	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
↑	↑	k?
Prozatímním	prozatímní	k2eAgMnSc7d1
ředitelem	ředitel	k1gMnSc7
GIBS	GIBS	kA
se	se	k3xPyFc4
stal	stát	k5eAaPmAgMnS
Ivan	Ivan	k1gMnSc1
Bílek	Bílek	k1gMnSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlada	Vlada	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2011-11-25	2011-11-25	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Žádost	žádost	k1gFnSc1
o	o	k7c6
poskytnutí	poskytnutí	k1gNnSc6
informací	informace	k1gFnPc2
o	o	k7c6
činnosti	činnost	k1gFnSc6
GIBS	GIBS	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gibs	Gibs	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2014-12-30	2014-12-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Šéfem	šéf	k1gMnSc7
inspekce	inspekce	k1gFnSc2
bezpečnostních	bezpečnostní	k2eAgInPc2d1
sborů	sbor	k1gInPc2
bude	být	k5eAaImBp3nS
jmenován	jmenovat	k5eAaImNgInS,k5eAaBmNgInS
Bílek	bílek	k1gInSc1
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Denik	Denik	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2012-06-06	2012-06-06	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
ČTK	ČTK	kA
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ředitel	ředitel	k1gMnSc1
GIBS	GIBS	kA
Bílek	Bílek	k1gMnSc1
rezignoval	rezignovat	k5eAaBmAgMnS
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
neškodil	škodit	k5eNaImAgMnS
organizaci	organizace	k1gFnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Politický	politický	k2eAgInSc1d1
tlak	tlak	k1gInSc1
nezmínil	zmínit	k5eNaPmAgInS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Aktualne	Aktualn	k1gInSc5
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2015-11-03	2015-11-03	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Zpráva	zpráva	k1gFnSc1
o	o	k7c6
činnosti	činnost	k1gFnSc6
Generální	generální	k2eAgFnSc2d1
inspekce	inspekce	k1gFnSc2
bezpečnostních	bezpečnostní	k2eAgInPc2d1
sborů	sbor	k1gInPc2
za	za	k7c4
rok	rok	k1gInSc4
2015	#num#	k4
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gibs	Gibs	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2016	#num#	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Vláda	vláda	k1gFnSc1
schválila	schválit	k5eAaPmAgFnS
návrh	návrh	k1gInSc4
na	na	k7c4
jmenování	jmenování	k1gNnSc4
plukovníka	plukovník	k1gMnSc2
Michala	Michal	k1gMnSc2
Murína	Murína	k1gFnSc1
generálním	generální	k2eAgMnSc7d1
ředitelem	ředitel	k1gMnSc7
GIBS	GIBS	kA
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vlada	Vlada	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2015-12-07	2015-12-07	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Rezignace	rezignace	k1gFnSc2
plk.	plk.	kA
JUDr.	JUDr.	kA
Michala	Michal	k1gMnSc2
Murína	Murín	k1gMnSc2
na	na	k7c6
funkci	funkce	k1gFnSc6
ředitele	ředitel	k1gMnSc2
Generální	generální	k2eAgFnSc2d1
inspekce	inspekce	k1gFnSc2
bezpečnostních	bezpečnostní	k2eAgInPc2d1
sborů	sbor	k1gInPc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Gibs	Gibs	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2018-04-17	2018-04-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
TRACHTOVÁ	TRACHTOVÁ	kA
<g/>
,	,	kIx,
Zdeňka	Zdeněk	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
čele	čelo	k1gNnSc6
GIBS	GIBS	kA
končí	končit	k5eAaImIp3nS
Michal	Michal	k1gMnSc1
Murín	Murín	k1gMnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Jeho	jeho	k3xOp3gMnSc1
nástupce	nástupce	k1gMnSc1
se	se	k3xPyFc4
teprve	teprve	k6eAd1
hledá	hledat	k5eAaImIp3nS
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
2018-04-30	2018-04-30	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
kul	kula	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Do	do	k7c2
čela	čelo	k1gNnSc2
GIBS	GIBS	kA
míří	mířit	k5eAaImIp3nS
Dragoun	dragoun	k1gInSc4
a	a	k8xC
říká	říkat	k5eAaImIp3nS
<g/>
:	:	kIx,
Nejsem	být	k5eNaImIp1nS
člověk	člověk	k1gMnSc1
premiéra	premiér	k1gMnSc2
ani	ani	k8xC
justice	justice	k1gFnSc2
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Ceskatelevize	Ceskatelevize	k1gFnSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
<g/>
,	,	kIx,
2018-08-29	2018-08-29	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
14	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Výměna	výměna	k1gFnSc1
policejního	policejní	k2eAgMnSc2d1
šéfa	šéf	k1gMnSc2
<g/>
:	:	kIx,
Na	na	k7c4
promyšlený	promyšlený	k2eAgInSc4d1
kalkul	kalkul	k1gInSc4
ODS	ODS	kA
to	ten	k3xDgNnSc4
nevypadá	vypadat	k5eNaImIp3nS,k5eNaPmIp3nS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Lidovky	Lidovky	k1gFnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
2012-08-31	2012-08-31	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2016	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
7	#num#	k4
<g/>
-	-	kIx~
<g/>
19	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
↑	↑	k?
↑	↑	k?
↑	↑	k?
↑	↑	k?
Kázeňské	kázeňský	k2eAgNnSc4d1
řízení	řízení	k1gNnSc4
s	s	k7c7
ředitelem	ředitel	k1gMnSc7
GIBS	GIBS	kA
<g/>
?	?	kIx.
</s>
<s desamb="1">
Babiš	Babiš	k1gInSc1
má	mít	k5eAaImIp3nS
dva	dva	k4xCgInPc4
týdny	týden	k1gInPc4
na	na	k7c4
rozhodnutí	rozhodnutí	k1gNnSc4
<g/>
,	,	kIx,
pak	pak	k6eAd1
Murínova	Murínův	k2eAgFnSc1d1
odpovědnost	odpovědnost	k1gFnSc1
zaniká	zanikat	k5eAaImIp3nS
<g/>
.	.	kIx.
iROZHLAS	iROZHLAS	k?
[	[	kIx(
<g/>
online	onlinout	k5eAaPmIp3nS
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Český	český	k2eAgInSc1d1
rozhlas	rozhlas	k1gInSc1
<g/>
,	,	kIx,
2018-04-17	2018-04-17	k4
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2018	#num#	k4
<g/>
-	-	kIx~
<g/>
0	#num#	k4
<g/>
4	#num#	k4
<g/>
-	-	kIx~
<g/>
18	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgNnSc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
↑	↑	k?
Vyhláška	vyhláška	k1gFnSc1
č.	č.	k?
433	#num#	k4
<g/>
/	/	kIx~
<g/>
2004	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
kterou	který	k3yIgFnSc4,k3yQgFnSc4,k3yRgFnSc4
se	se	k3xPyFc4
stanoví	stanovit	k5eAaPmIp3nS
druh	druh	k1gInSc1
a	a	k8xC
vzor	vzor	k1gInSc1
služebních	služební	k2eAgFnPc2d1
medailí	medaile	k1gFnPc2
bezpečnostních	bezpečnostní	k2eAgInPc2d1
sborů	sbor	k1gInPc2
a	a	k8xC
důvody	důvod	k1gInPc1
pro	pro	k7c4
jejich	jejich	k3xOp3gNnSc4
udělení	udělení	k1gNnSc4
<g/>
.	.	kIx.
[	[	kIx(
<g/>
cit	cit	k1gInSc1
<g/>
.	.	kIx.
2019	#num#	k4
<g/>
-	-	kIx~
<g/>
12	#num#	k4
<g/>
-	-	kIx~
<g/>
31	#num#	k4
<g/>
]	]	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Související	související	k2eAgInPc1d1
články	článek	k1gInPc1
</s>
<s>
Hodnosti	hodnost	k1gFnPc1
příslušníků	příslušník	k1gMnPc2
bezpečnostních	bezpečnostní	k2eAgInPc2d1
sborů	sbor	k1gInPc2
(	(	kIx(
<g/>
Česko	Česko	k1gNnSc1
<g/>
)	)	kIx)
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Generální	generální	k2eAgFnSc2d1
inspekce	inspekce	k1gFnSc2
bezpečnostních	bezpečnostní	k2eAgInPc2d1
sborů	sbor	k1gInPc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Seznam	seznam	k1gInSc1
děl	dělo	k1gNnPc2
v	v	k7c6
Souborném	souborný	k2eAgInSc6d1
katalogu	katalog	k1gInSc6
ČR	ČR	kA
<g/>
,	,	kIx,
jejichž	jejichž	k3xOyRp3gMnSc7
autorem	autor	k1gMnSc7
nebo	nebo	k8xC
tématem	téma	k1gNnSc7
je	být	k5eAaImIp3nS
Generální	generální	k2eAgFnSc1d1
inspekce	inspekce	k1gFnSc1
bezpečnostních	bezpečnostní	k2eAgInPc2d1
sborů	sbor	k1gInPc2
</s>
<s>
Oficiální	oficiální	k2eAgFnPc1d1
stránky	stránka	k1gFnPc1
,	,	kIx,
gibs	gibs	k1gInSc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
Generální	generální	k2eAgFnSc1d1
inspekce	inspekce	k1gFnSc1
bezpečnostních	bezpečnostní	k2eAgInPc2d1
sborů	sbor	k1gInPc2
na	na	k7c6
Facebooku	Facebook	k1gInSc6
</s>
<s>
Zákon	zákon	k1gInSc1
č.	č.	k?
341	#num#	k4
<g/>
/	/	kIx~
<g/>
2011	#num#	k4
Sb	sb	kA
<g/>
.	.	kIx.
<g/>
,	,	kIx,
o	o	k7c6
Generální	generální	k2eAgFnSc6d1
inspekci	inspekce	k1gFnSc6
bezpečnostních	bezpečnostní	k2eAgInPc2d1
sborů	sbor	k1gInPc2
a	a	k8xC
o	o	k7c6
změně	změna	k1gFnSc6
souvisejících	související	k2eAgInPc2d1
zákonů	zákon	k1gInPc2
<g/>
,	,	kIx,
zakonyprolidi	zakonyprolid	k1gMnPc1
<g/>
.	.	kIx.
<g/>
cz	cz	k?
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Česko	Česko	k1gNnSc1
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
AUT	aut	k1gInSc1
<g/>
:	:	kIx,
kv	kv	k?
<g/>
2012704937	#num#	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
250685094	#num#	k4
</s>
