<s>
Generální	generální	k2eAgFnSc1d1
inspekce	inspekce	k1gFnSc1
bezpečnostních	bezpečnostní	k2eAgInPc2d1
sborů	sbor	k1gInPc2
(	(	kIx(
<g/>
GIBS	GIBS	kA
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
ozbrojený	ozbrojený	k2eAgInSc4d1
bezpečnostní	bezpečnostní	k2eAgInSc4d1
sbor	sbor	k1gInSc4
České	český	k2eAgFnSc2d1
republiky	republika	k1gFnSc2
<g/>
,	,	kIx,
jehož	jehož	k3xOyRp3gInSc7
úkolem	úkol	k1gInSc7
je	být	k5eAaImIp3nS
kontrolní	kontrolní	k2eAgFnSc1d1
a	a	k8xC
vyšetřovací	vyšetřovací	k2eAgFnSc1d1
činnost	činnost	k1gFnSc1
příslušníků	příslušník	k1gMnPc2
a	a	k8xC
zaměstnanců	zaměstnanec	k1gMnPc2
některých	některý	k3yIgInPc2
českých	český	k2eAgInPc2d1
bezpečnostních	bezpečnostní	k2eAgInPc2d1
sborů	sbor	k1gInPc2
<g/>
.	.	kIx.
</s>