<s>
Výčet	výčet	k1gInSc1	výčet
sedmi	sedm	k4xCc2	sedm
hlavních	hlavní	k2eAgInPc2d1	hlavní
hříchů	hřích	k1gInPc2	hřích
zavedl	zavést	k5eAaPmAgInS	zavést
na	na	k7c6	na
konci	konec	k1gInSc6	konec
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
papež	papež	k1gMnSc1	papež
Řehoř	Řehoř	k1gMnSc1	Řehoř
I.	I.	kA	I.
Veliký	veliký	k2eAgMnSc1d1	veliký
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
otázka	otázka	k1gFnSc1	otázka
jejich	jejich	k3xOp3gInSc2	jejich
vzniku	vznik	k1gInSc2	vznik
je	být	k5eAaImIp3nS	být
nejasná	jasný	k2eNgFnSc1d1	nejasná
<g/>
.	.	kIx.	.
</s>
