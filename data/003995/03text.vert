<s>
Sedm	sedm	k4xCc1	sedm
hlavních	hlavní	k2eAgInPc2d1	hlavní
hříchů	hřích	k1gInPc2	hřích
(	(	kIx(	(
<g/>
též	též	k9	též
sedm	sedm	k4xCc4	sedm
kardinálních	kardinální	k2eAgInPc2d1	kardinální
hříchů	hřích	k1gInPc2	hřích
či	či	k8xC	či
sedm	sedm	k4xCc1	sedm
smrtelných	smrtelný	k2eAgInPc2d1	smrtelný
hříchů	hřích	k1gInPc2	hřích
<g/>
)	)	kIx)	)
vypočítává	vypočítávat	k5eAaImIp3nS	vypočítávat
katolická	katolický	k2eAgFnSc1d1	katolická
tradice	tradice	k1gFnSc1	tradice
od	od	k7c2	od
časů	čas	k1gInPc2	čas
Řehoře	Řehoř	k1gMnSc2	Řehoř
I.	I.	kA	I.
Velikého	veliký	k2eAgNnSc2d1	veliké
(	(	kIx(	(
<g/>
asi	asi	k9	asi
540	[number]	k4	540
–	–	k?	–
12	[number]	k4	12
<g/>
.	.	kIx.	.
březen	březen	k1gInSc1	březen
604	[number]	k4	604
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Pojmem	pojem	k1gInSc7	pojem
hlavní	hlavní	k2eAgInSc4d1	hlavní
hřích	hřích	k1gInSc4	hřích
se	se	k3xPyFc4	se
nemyslí	myslet	k5eNaImIp3nS	myslet
hřích	hřích	k1gInSc1	hřích
<g/>
,	,	kIx,	,
který	který	k3yRgInSc1	který
je	být	k5eAaImIp3nS	být
před	před	k7c7	před
Bohem	bůh	k1gMnSc7	bůh
nejvážnější	vážní	k2eAgInPc4d3	nejvážnější
<g/>
,	,	kIx,	,
nejtěžší	těžký	k2eAgInSc4d3	nejtěžší
či	či	k8xC	či
"	"	kIx"	"
<g/>
nejsmrtelnější	smrtelní	k2eAgFnSc1d3	smrtelní
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Hlavní	hlavní	k2eAgNnSc1d1	hlavní
znamená	znamenat	k5eAaImIp3nS	znamenat
<g/>
,	,	kIx,	,
že	že	k8xS	že
hřích	hřích	k1gInSc1	hřích
vychází	vycházet	k5eAaImIp3nS	vycházet
z	z	k7c2	z
hlavy	hlava	k1gFnSc2	hlava
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
caput	caput	k1gInSc1	caput
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
proto	proto	k8xC	proto
se	se	k3xPyFc4	se
v	v	k7c6	v
jiných	jiný	k2eAgInPc6d1	jiný
jazycích	jazyk	k1gInPc6	jazyk
nazývají	nazývat	k5eAaImIp3nP	nazývat
"	"	kIx"	"
<g/>
kapitální	kapitální	k2eAgInPc1d1	kapitální
hříchy	hřích	k1gInPc1	hřích
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
kardinální	kardinální	k2eAgInSc4d1	kardinální
hřích	hřích	k1gInSc4	hřích
(	(	kIx(	(
<g/>
lat.	lat.	k?	lat.
cardo	cardo	k1gNnSc1	cardo
=	=	kIx~	=
pant	pant	k1gInSc1	pant
<g/>
)	)	kIx)	)
znamená	znamenat	k5eAaImIp3nS	znamenat
stavy	stav	k1gInPc4	stav
mysli	mysl	k1gFnSc2	mysl
<g/>
,	,	kIx,	,
okolo	okolo	k7c2	okolo
kterých	který	k3yQgInPc2	který
se	se	k3xPyFc4	se
"	"	kIx"	"
<g/>
otáčí	otáčet	k5eAaImIp3nS	otáčet
<g/>
"	"	kIx"	"
celá	celý	k2eAgFnSc1d1	celá
hříšnost	hříšnost	k1gFnSc1	hříšnost
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Jde	jít	k5eAaImIp3nS	jít
tedy	tedy	k9	tedy
o	o	k7c4	o
hříchy	hřích	k1gInPc4	hřích
a	a	k8xC	a
lidské	lidský	k2eAgFnPc4d1	lidská
slabosti	slabost	k1gFnPc4	slabost
<g/>
,	,	kIx,	,
ze	z	k7c2	z
kterých	který	k3yRgFnPc2	který
vycházejí	vycházet	k5eAaImIp3nP	vycházet
další	další	k2eAgInPc4d1	další
hříšné	hříšný	k2eAgInPc4d1	hříšný
činy	čin	k1gInPc4	čin
<g/>
.	.	kIx.	.
</s>
<s>
Zavádějící	zavádějící	k2eAgInSc1d1	zavádějící
význam	význam	k1gInSc1	význam
slova	slovo	k1gNnSc2	slovo
smrtelný	smrtelný	k2eAgInSc1d1	smrtelný
v	v	k7c6	v
tomto	tento	k3xDgInSc6	tento
případě	případ	k1gInSc6	případ
(	(	kIx(	(
<g/>
viz	vidět	k5eAaImRp2nS	vidět
smrtelný	smrtelný	k2eAgInSc1d1	smrtelný
hřích	hřích	k1gInSc4	hřích
<g/>
)	)	kIx)	)
je	být	k5eAaImIp3nS	být
důvodem	důvod	k1gInSc7	důvod
<g/>
,	,	kIx,	,
proč	proč	k6eAd1	proč
by	by	kYmCp3nS	by
se	se	k3xPyFc4	se
tradiční	tradiční	k2eAgNnSc1d1	tradiční
označení	označení	k1gNnSc1	označení
sedm	sedm	k4xCc4	sedm
smrtelných	smrtelný	k2eAgInPc2d1	smrtelný
hříchů	hřích	k1gInPc2	hřích
nemělo	mít	k5eNaImAgNnS	mít
používat	používat	k5eAaImF	používat
<g/>
.	.	kIx.	.
</s>
<s>
Výčet	výčet	k1gInSc1	výčet
sedmi	sedm	k4xCc2	sedm
hlavních	hlavní	k2eAgInPc2d1	hlavní
hříchů	hřích	k1gInPc2	hřích
zavedl	zavést	k5eAaPmAgInS	zavést
na	na	k7c6	na
konci	konec	k1gInSc6	konec
6	[number]	k4	6
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
papež	papež	k1gMnSc1	papež
Řehoř	Řehoř	k1gMnSc1	Řehoř
I.	I.	kA	I.
Veliký	veliký	k2eAgMnSc1d1	veliký
<g/>
,	,	kIx,	,
ovšem	ovšem	k9	ovšem
otázka	otázka	k1gFnSc1	otázka
jejich	jejich	k3xOp3gInSc2	jejich
vzniku	vznik	k1gInSc2	vznik
je	být	k5eAaImIp3nS	být
nejasná	jasný	k2eNgFnSc1d1	nejasná
<g/>
.	.	kIx.	.
</s>
<s>
Podle	podle	k7c2	podle
některých	některý	k3yIgMnPc2	některý
autorů	autor	k1gMnPc2	autor
jde	jít	k5eAaImIp3nS	jít
o	o	k7c4	o
upravenou	upravený	k2eAgFnSc4d1	upravená
verzi	verze	k1gFnSc4	verze
seznamu	seznam	k1gInSc2	seznam
osmi	osm	k4xCc2	osm
zlozvyků	zlozvyk	k1gInPc2	zlozvyk
<g/>
,	,	kIx,	,
vytvořeného	vytvořený	k2eAgInSc2d1	vytvořený
ve	v	k7c4	v
4	[number]	k4	4
<g/>
.	.	kIx.	.
století	století	k1gNnSc2	století
Evagriem	Evagrium	k1gNnSc7	Evagrium
z	z	k7c2	z
Pontu	Pont	k1gInSc2	Pont
<g/>
.	.	kIx.	.
</s>
<s>
Jednotlivé	jednotlivý	k2eAgFnPc1d1	jednotlivá
položky	položka	k1gFnPc1	položka
sice	sice	k8xC	sice
už	už	k6eAd1	už
zůstaly	zůstat	k5eAaPmAgFnP	zůstat
od	od	k7c2	od
vzniku	vznik	k1gInSc2	vznik
seznamu	seznam	k1gInSc2	seznam
neměnné	měnný	k2eNgNnSc1d1	neměnné
(	(	kIx(	(
<g/>
maximálně	maximálně	k6eAd1	maximálně
v	v	k7c6	v
některých	některý	k3yIgInPc6	některý
jazycích	jazyk	k1gInPc6	jazyk
se	se	k3xPyFc4	se
měnily	měnit	k5eAaImAgInP	měnit
termíny	termín	k1gInPc1	termín
za	za	k7c7	za
účelem	účel	k1gInSc7	účel
lepšího	dobrý	k2eAgNnSc2d2	lepší
vystižení	vystižení	k1gNnSc2	vystižení
podstaty	podstata	k1gFnSc2	podstata
daného	daný	k2eAgInSc2d1	daný
hříchu	hřích	k1gInSc2	hřích
<g/>
,	,	kIx,	,
v	v	k7c6	v
češtině	čeština	k1gFnSc6	čeština
např.	např.	kA	např.
nestřídmost-obžerství	nestřídmostbžerství	k1gNnSc4	nestřídmost-obžerství
<g/>
)	)	kIx)	)
<g/>
,	,	kIx,	,
pořadí	pořadí	k1gNnSc1	pořadí
však	však	k8xC	však
stálé	stálý	k2eAgNnSc1d1	stálé
nebylo	být	k5eNaImAgNnS	být
<g/>
.	.	kIx.	.
</s>
<s>
Jako	jako	k8xS	jako
první	první	k4xOgFnSc1	první
je	být	k5eAaImIp3nS	být
dnes	dnes	k6eAd1	dnes
standardně	standardně	k6eAd1	standardně
uváděna	uváděn	k2eAgFnSc1d1	uváděna
pýcha	pýcha	k1gFnSc1	pýcha
a	a	k8xC	a
většinou	většinou	k6eAd1	většinou
tomu	ten	k3xDgNnSc3	ten
tak	tak	k9	tak
bylo	být	k5eAaImAgNnS	být
i	i	k9	i
dříve	dříve	k6eAd2	dříve
<g/>
,	,	kIx,	,
jak	jak	k8xC	jak
ukazují	ukazovat	k5eAaImIp3nP	ukazovat
díla	dílo	k1gNnPc4	dílo
sv.	sv.	kA	sv.
Tomáše	Tomáš	k1gMnSc2	Tomáš
Akvinského	Akvinský	k2eAgMnSc2d1	Akvinský
nebo	nebo	k8xC	nebo
i	i	k9	i
Dantova	Dantův	k2eAgFnSc1d1	Dantova
Božská	božský	k2eAgFnSc1d1	božská
komedie	komedie	k1gFnSc1	komedie
<g/>
,	,	kIx,	,
nicméně	nicméně	k8xC	nicméně
nebylo	být	k5eNaImAgNnS	být
tomu	ten	k3xDgMnSc3	ten
vždy	vždy	k6eAd1	vždy
tak	tak	k6eAd1	tak
<g/>
:	:	kIx,	:
na	na	k7c6	na
počátku	počátek	k1gInSc6	počátek
14	[number]	k4	14
<g/>
.	.	kIx.	.
století	století	k1gNnSc6	století
byla	být	k5eAaImAgFnS	být
na	na	k7c6	na
prvním	první	k4xOgInSc6	první
místě	místo	k1gNnSc6	místo
uváděna	uvádět	k5eAaImNgFnS	uvádět
nestřídmost	nestřídmost	k1gFnSc1	nestřídmost
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
obžerství	obžerství	k1gNnSc1	obžerství
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
moderní	moderní	k2eAgFnSc6d1	moderní
době	doba	k1gFnSc6	doba
se	se	k3xPyFc4	se
vyskytly	vyskytnout	k5eAaPmAgInP	vyskytnout
i	i	k9	i
návrhy	návrh	k1gInPc1	návrh
na	na	k7c4	na
zavedení	zavedení	k1gNnSc4	zavedení
nového	nový	k2eAgInSc2d1	nový
výčtu	výčet	k1gInSc2	výčet
<g/>
,	,	kIx,	,
žádný	žádný	k3yNgInSc1	žádný
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
však	však	k9	však
výrazněji	výrazně	k6eAd2	výrazně
neuspěl	uspět	k5eNaPmAgMnS	uspět
<g/>
.	.	kIx.	.
</s>
<s>
Tomlin	Tomlin	k1gInSc1	Tomlin
zmiňuje	zmiňovat	k5eAaImIp3nS	zmiňovat
jako	jako	k9	jako
příklad	příklad	k1gInSc1	příklad
návrh	návrh	k1gInSc1	návrh
filosofa	filosof	k1gMnSc2	filosof
Juliana	Julian	k1gMnSc2	Julian
Bagginiho	Baggini	k1gMnSc2	Baggini
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
konstatuje	konstatovat	k5eAaBmIp3nS	konstatovat
<g/>
,	,	kIx,	,
že	že	k8xS	že
tyto	tento	k3xDgInPc1	tento
moderní	moderní	k2eAgInPc1d1	moderní
seznamy	seznam	k1gInPc1	seznam
mají	mít	k5eAaImIp3nP	mít
sklon	sklon	k1gInSc4	sklon
zaměňovat	zaměňovat	k5eAaImF	zaměňovat
příčiny	příčina	k1gFnPc4	příčina
s	s	k7c7	s
příznaky	příznak	k1gInPc7	příznak
<g/>
.	.	kIx.	.
</s>
<s>
Zvláštním	zvláštní	k2eAgInSc7d1	zvláštní
případem	případ	k1gInSc7	případ
je	být	k5eAaImIp3nS	být
novinářská	novinářský	k2eAgFnSc1d1	novinářská
kachna	kachna	k1gFnSc1	kachna
z	z	k7c2	z
března	březen	k1gInSc2	březen
2008	[number]	k4	2008
<g/>
,	,	kIx,	,
podle	podle	k7c2	podle
níž	jenž	k3xRgFnSc2	jenž
měla	mít	k5eAaImAgFnS	mít
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
seznam	seznam	k1gInSc4	seznam
aktualizovat	aktualizovat	k5eAaBmF	aktualizovat
<g/>
,	,	kIx,	,
viz	vidět	k5eAaImRp2nS	vidět
sekce	sekce	k1gFnSc1	sekce
"	"	kIx"	"
<g/>
Nové	Nové	k2eAgInPc1d1	Nové
smrtelné	smrtelný	k2eAgInPc1d1	smrtelný
hříchy	hřích	k1gInPc1	hřích
<g/>
"	"	kIx"	"
<g/>
.	.	kIx.	.
</s>
<s>
Pýcha	pýcha	k1gFnSc1	pýcha
je	být	k5eAaImIp3nS	být
definována	definovat	k5eAaBmNgFnS	definovat
jako	jako	k9	jako
touha	touha	k1gFnSc1	touha
po	po	k7c6	po
vyšší	vysoký	k2eAgFnSc6d2	vyšší
atraktivitě	atraktivita	k1gFnSc6	atraktivita
a	a	k8xC	a
důležitosti	důležitost	k1gFnSc6	důležitost
než	než	k8xS	než
mají	mít	k5eAaImIp3nP	mít
ostatní	ostatní	k2eAgMnPc1d1	ostatní
<g/>
.	.	kIx.	.
</s>
<s>
Slovo	slovo	k1gNnSc1	slovo
pýcha	pýcha	k1gFnSc1	pýcha
se	se	k3xPyFc4	se
dá	dát	k5eAaPmIp3nS	dát
také	také	k9	také
vyjádřit	vyjádřit	k5eAaPmF	vyjádřit
jako	jako	k8xS	jako
<g/>
:	:	kIx,	:
nadutost	nadutost	k1gFnSc1	nadutost
<g/>
,	,	kIx,	,
domýšlivost	domýšlivost	k1gFnSc1	domýšlivost
<g/>
,	,	kIx,	,
vyvýšenost	vyvýšenost	k1gFnSc1	vyvýšenost
<g/>
,	,	kIx,	,
přehnaná	přehnaný	k2eAgFnSc1d1	přehnaná
sebeúcta	sebeúcta	k1gFnSc1	sebeúcta
<g/>
,	,	kIx,	,
nerozumná	rozumný	k2eNgFnSc1d1	nerozumná
nadřazenost	nadřazenost	k1gFnSc1	nadřazenost
kvůli	kvůli	k7c3	kvůli
kráse	krása	k1gFnSc3	krása
<g/>
,	,	kIx,	,
bohatství	bohatství	k1gNnSc4	bohatství
<g/>
,	,	kIx,	,
postavení	postavení	k1gNnSc4	postavení
ve	v	k7c6	v
společnosti	společnost	k1gFnSc6	společnost
nebo	nebo	k8xC	nebo
nadání	nadání	k1gNnSc4	nadání
<g/>
.	.	kIx.	.
</s>
<s>
Pýcha	pýcha	k1gFnSc1	pýcha
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
nadřazeným	nadřazený	k2eAgNnSc7d1	nadřazené
chováním	chování	k1gNnSc7	chování
<g/>
.	.	kIx.	.
</s>
<s>
Jedná	jednat	k5eAaImIp3nS	jednat
se	se	k3xPyFc4	se
o	o	k7c4	o
chorobný	chorobný	k2eAgInSc4d1	chorobný
stav	stav	k1gInSc4	stav
srdce	srdce	k1gNnSc2	srdce
<g/>
.	.	kIx.	.
</s>
<s>
Opakem	opak	k1gInSc7	opak
pýchy	pýcha	k1gFnSc2	pýcha
je	být	k5eAaImIp3nS	být
pokora	pokora	k1gFnSc1	pokora
<g/>
.	.	kIx.	.
</s>
<s>
Lakomství	lakomství	k1gNnSc1	lakomství
<g/>
,	,	kIx,	,
též	též	k9	též
lakota	lakota	k1gFnSc1	lakota
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vyznačuje	vyznačovat	k5eAaImIp3nS	vyznačovat
neochotou	neochota	k1gFnSc7	neochota
se	se	k3xPyFc4	se
dělit	dělit	k5eAaImF	dělit
s	s	k7c7	s
bližními	bližní	k1gMnPc7	bližní
nebo	nebo	k8xC	nebo
potřebnými	potřebný	k2eAgFnPc7d1	potřebná
<g/>
.	.	kIx.	.
</s>
<s>
Závist	závist	k1gFnSc1	závist
je	být	k5eAaImIp3nS	být
pocit	pocit	k1gInSc4	pocit
nelibosti	nelibost	k1gFnSc2	nelibost
při	při	k7c6	při
úspěchu	úspěch	k1gInSc6	úspěch
nebo	nebo	k8xC	nebo
štěstí	štěstí	k1gNnSc1	štěstí
jiného	jiný	k2eAgMnSc2d1	jiný
člověka	člověk	k1gMnSc2	člověk
<g/>
.	.	kIx.	.
</s>
<s>
Vzniká	vznikat	k5eAaImIp3nS	vznikat
tehdy	tehdy	k6eAd1	tehdy
<g/>
,	,	kIx,	,
kdy	kdy	k6eAd1	kdy
určitá	určitý	k2eAgFnSc1d1	určitá
osoba	osoba	k1gFnSc1	osoba
vášnivě	vášnivě	k6eAd1	vášnivě
touží	toužit	k5eAaImIp3nS	toužit
po	po	k7c6	po
majetku	majetek	k1gInSc6	majetek
(	(	kIx(	(
<g/>
nebo	nebo	k8xC	nebo
vlastnostech	vlastnost	k1gFnPc6	vlastnost
<g/>
)	)	kIx)	)
druhé	druhý	k4xOgFnSc2	druhý
osoby	osoba	k1gFnSc2	osoba
<g/>
.	.	kIx.	.
<g/>
[	[	kIx(	[
<g/>
zdroj	zdroj	k1gInSc1	zdroj
<g/>
?	?	kIx.	?
</s>
<s>
<g/>
]	]	kIx)	]
Závist	závist	k1gFnSc1	závist
může	moct	k5eAaImIp3nS	moct
vyústit	vyústit	k5eAaPmF	vyústit
v	v	k7c4	v
poškozování	poškozování	k1gNnSc4	poškozování
druhé	druhý	k4xOgFnSc2	druhý
osoby	osoba	k1gFnSc2	osoba
<g/>
,	,	kIx,	,
ke	k	k7c3	k
krádeži	krádež	k1gFnSc3	krádež
<g/>
,	,	kIx,	,
nebo	nebo	k8xC	nebo
k	k	k7c3	k
sabotáži	sabotáž	k1gFnSc3	sabotáž
<g/>
.	.	kIx.	.
</s>
<s>
Hněv	hněv	k1gInSc1	hněv
se	se	k3xPyFc4	se
projevuje	projevovat	k5eAaImIp3nS	projevovat
nezvladatelnými	zvladatelný	k2eNgInPc7d1	nezvladatelný
pocity	pocit	k1gInPc7	pocit
zlosti	zlost	k1gFnSc2	zlost
a	a	k8xC	a
nenávisti	nenávist	k1gFnSc2	nenávist
vůči	vůči	k7c3	vůči
jiné	jiný	k2eAgFnSc3d1	jiná
osobě	osoba	k1gFnSc3	osoba
<g/>
.	.	kIx.	.
</s>
<s>
Smilstvo	smilstvo	k1gNnSc1	smilstvo
označuje	označovat	k5eAaImIp3nS	označovat
nevěru	nevěra	k1gFnSc4	nevěra
<g/>
,	,	kIx,	,
nestálost	nestálost	k1gFnSc4	nestálost
ve	v	k7c6	v
vztahu	vztah	k1gInSc6	vztah
k	k	k7c3	k
jednomu	jeden	k4xCgInSc3	jeden
jedinému	jediný	k2eAgInSc3d1	jediný
partnerovi	partner	k1gMnSc3	partner
v	v	k7c6	v
manželství	manželství	k1gNnSc6	manželství
nebo	nebo	k8xC	nebo
v	v	k7c6	v
jiném	jiný	k2eAgInSc6d1	jiný
podobném	podobný	k2eAgInSc6d1	podobný
svazku	svazek	k1gInSc6	svazek
<g/>
.	.	kIx.	.
</s>
<s>
Nestřídmost	Nestřídmost	k1gFnSc1	Nestřídmost
<g/>
,	,	kIx,	,
též	též	k9	též
obžerství	obžerství	k1gNnSc1	obžerství
<g/>
,	,	kIx,	,
se	se	k3xPyFc4	se
vztahuje	vztahovat	k5eAaImIp3nS	vztahovat
k	k	k7c3	k
přílišnému	přílišný	k2eAgNnSc3d1	přílišné
požívání	požívání	k1gNnSc3	požívání
nápojů	nápoj	k1gInPc2	nápoj
i	i	k8xC	i
potravin	potravina	k1gFnPc2	potravina
<g/>
.	.	kIx.	.
</s>
<s>
Lenost	lenost	k1gFnSc1	lenost
<g/>
,	,	kIx,	,
též	též	k9	též
malomyslnost	malomyslnost	k1gFnSc4	malomyslnost
<g/>
,	,	kIx,	,
duchovní	duchovní	k2eAgFnSc4d1	duchovní
znechucenost	znechucenost	k1gFnSc4	znechucenost
<g/>
.	.	kIx.	.
</s>
<s>
Projevuje	projevovat	k5eAaImIp3nS	projevovat
se	se	k3xPyFc4	se
jako	jako	k9	jako
absence	absence	k1gFnSc1	absence
víry	víra	k1gFnSc2	víra
v	v	k7c4	v
sebe	sebe	k3xPyFc4	sebe
<g/>
,	,	kIx,	,
víry	víra	k1gFnSc2	víra
v	v	k7c4	v
Boha	bůh	k1gMnSc4	bůh
<g/>
,	,	kIx,	,
snahy	snaha	k1gFnSc2	snaha
cokoliv	cokoliv	k3yInSc4	cokoliv
zlepšit	zlepšit	k5eAaPmF	zlepšit
i	i	k9	i
jako	jako	k9	jako
nezájem	nezájem	k1gInSc4	nezájem
uskutečnit	uskutečnit	k5eAaPmF	uskutečnit
svoje	svůj	k3xOyFgInPc4	svůj
dobré	dobrý	k2eAgInPc4d1	dobrý
úmysly	úmysl	k1gInPc4	úmysl
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
roce	rok	k1gInSc6	rok
1589	[number]	k4	1589
přiřadil	přiřadit	k5eAaPmAgMnS	přiřadit
Peter	Peter	k1gMnSc1	Peter
Binsfeld	Binsfeld	k1gMnSc1	Binsfeld
ke	k	k7c3	k
každému	každý	k3xTgInSc3	každý
z	z	k7c2	z
hříchů	hřích	k1gInPc2	hřích
jednoho	jeden	k4xCgMnSc4	jeden
z	z	k7c2	z
biblických	biblický	k2eAgMnPc2d1	biblický
démonů	démon	k1gMnPc2	démon
<g/>
.	.	kIx.	.
</s>
<s>
Jméno	jméno	k1gNnSc1	jméno
každého	každý	k3xTgInSc2	každý
z	z	k7c2	z
nich	on	k3xPp3gInPc2	on
je	být	k5eAaImIp3nS	být
uvedeno	uvést	k5eAaPmNgNnS	uvést
u	u	k7c2	u
příslušného	příslušný	k2eAgInSc2d1	příslušný
hříchu	hřích	k1gInSc2	hřích
<g/>
.	.	kIx.	.
</s>
<s>
Démonem	démon	k1gMnSc7	démon
spojovaným	spojovaný	k2eAgFnPc3d1	spojovaná
s	s	k7c7	s
pýchou	pýcha	k1gFnSc7	pýcha
je	být	k5eAaImIp3nS	být
Lucifer	Lucifer	k1gMnSc1	Lucifer
<g/>
.	.	kIx.	.
</s>
<s>
Démonem	démon	k1gMnSc7	démon
lakoty	lakota	k1gFnSc2	lakota
je	být	k5eAaImIp3nS	být
Mamon	mamon	k1gInSc1	mamon
<g/>
.	.	kIx.	.
</s>
<s>
Démonem	démon	k1gMnSc7	démon
závisti	závist	k1gFnSc2	závist
je	být	k5eAaImIp3nS	být
Leviatan	leviatan	k1gInSc1	leviatan
<g/>
.	.	kIx.	.
</s>
<s>
S	s	k7c7	s
hněvem	hněv	k1gInSc7	hněv
je	být	k5eAaImIp3nS	být
spojován	spojován	k2eAgInSc1d1	spojován
samotný	samotný	k2eAgInSc1d1	samotný
Satan	satan	k1gInSc1	satan
<g/>
.	.	kIx.	.
</s>
<s>
Démonem	démon	k1gMnSc7	démon
smilstva	smilstvo	k1gNnSc2	smilstvo
je	být	k5eAaImIp3nS	být
Asmodeus	Asmodeus	k1gInSc1	Asmodeus
<g/>
.	.	kIx.	.
</s>
<s>
Démonem	démon	k1gMnSc7	démon
obžerství	obžerství	k1gNnSc2	obžerství
je	být	k5eAaImIp3nS	být
Belzebub	Belzebub	k1gMnSc1	Belzebub
<g/>
.	.	kIx.	.
</s>
<s>
Démonem	démon	k1gMnSc7	démon
lenosti	lenost	k1gFnSc2	lenost
je	být	k5eAaImIp3nS	být
Belfegor	Belfegor	k1gInSc1	Belfegor
<g/>
.	.	kIx.	.
</s>
<s>
V	v	k7c6	v
březnu	březen	k1gInSc6	březen
2008	[number]	k4	2008
prolétla	prolétnout	k5eAaPmAgFnS	prolétnout
světovými	světový	k2eAgFnPc7d1	světová
médii	médium	k1gNnPc7	médium
zpráva	zpráva	k1gFnSc1	zpráva
o	o	k7c6	o
tom	ten	k3xDgNnSc6	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
katolická	katolický	k2eAgFnSc1d1	katolická
církev	církev	k1gFnSc1	církev
(	(	kIx(	(
<g/>
resp.	resp.	kA	resp.
její	její	k3xOp3gFnSc7	její
Apoštolská	apoštolský	k2eAgFnSc1d1	apoštolská
penitenciatura	penitenciatura	k1gFnSc1	penitenciatura
<g/>
)	)	kIx)	)
rozšířila	rozšířit	k5eAaPmAgFnS	rozšířit
počet	počet	k1gInSc4	počet
sedmi	sedm	k4xCc2	sedm
smrtelných	smrtelný	k2eAgInPc2d1	smrtelný
hříchů	hřích	k1gInPc2	hřích
či	či	k8xC	či
že	že	k8xS	že
nahradila	nahradit	k5eAaPmAgFnS	nahradit
jejich	jejich	k3xOp3gInSc4	jejich
seznam	seznam	k1gInSc4	seznam
7	[number]	k4	7
moderními	moderní	k2eAgInPc7d1	moderní
smrtelnými	smrtelný	k2eAgInPc7d1	smrtelný
hříchy	hřích	k1gInPc7	hřích
<g/>
,	,	kIx,	,
aby	aby	kYmCp3nS	aby
vedla	vést	k5eAaImAgFnS	vést
věřící	věřící	k1gFnSc1	věřící
k	k	k7c3	k
poznání	poznání	k1gNnSc3	poznání
<g/>
,	,	kIx,	,
že	že	k8xS	že
svým	svůj	k3xOyFgNnSc7	svůj
jednáním	jednání	k1gNnSc7	jednání
zasahují	zasahovat	k5eAaImIp3nP	zasahovat
své	svůj	k3xOyFgMnPc4	svůj
bližní	bližní	k1gMnPc4	bližní
<g/>
,	,	kIx,	,
své	svůj	k3xOyFgNnSc4	svůj
okolí	okolí	k1gNnSc4	okolí
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
i	i	k9	i
přírodu	příroda	k1gFnSc4	příroda
<g/>
.	.	kIx.	.
</s>
<s>
Mělo	mít	k5eAaImAgNnS	mít
jít	jít	k5eAaImF	jít
např.	např.	kA	např.
o	o	k7c6	o
znečišťování	znečišťování	k1gNnSc6	znečišťování
životního	životní	k2eAgNnSc2d1	životní
prostředí	prostředí	k1gNnSc2	prostředí
<g/>
,	,	kIx,	,
genetické	genetický	k2eAgFnPc4d1	genetická
modifikace	modifikace	k1gFnPc4	modifikace
<g/>
,	,	kIx,	,
pokusy	pokus	k1gInPc4	pokus
na	na	k7c6	na
lidech	člověk	k1gMnPc6	člověk
<g/>
,	,	kIx,	,
sociální	sociální	k2eAgFnSc4d1	sociální
nespravedlnost	nespravedlnost	k1gFnSc4	nespravedlnost
<g/>
,	,	kIx,	,
ožebračování	ožebračování	k1gNnPc4	ožebračování
lidí	člověk	k1gMnPc2	člověk
<g/>
,	,	kIx,	,
shromažďování	shromažďování	k1gNnSc1	shromažďování
nemravného	mravný	k2eNgNnSc2d1	nemravné
bohatství	bohatství	k1gNnSc2	bohatství
a	a	k8xC	a
distribuci	distribuce	k1gFnSc4	distribuce
drog	droga	k1gFnPc2	droga
<g/>
.	.	kIx.	.
</s>
<s>
Zpráva	zpráva	k1gFnSc1	zpráva
se	se	k3xPyFc4	se
ukázala	ukázat	k5eAaPmAgFnS	ukázat
jako	jako	k9	jako
nepravdivá	pravdivý	k2eNgFnSc1d1	nepravdivá
<g/>
.	.	kIx.	.
</s>
<s>
Vznikla	vzniknout	k5eAaPmAgFnS	vzniknout
nepochopením	nepochopení	k1gNnSc7	nepochopení
a	a	k8xC	a
zkreslením	zkreslení	k1gNnSc7	zkreslení
rozhovoru	rozhovor	k1gInSc2	rozhovor
biskupa	biskup	k1gMnSc2	biskup
Gianfranca	Gianfrancus	k1gMnSc2	Gianfrancus
Girottiho	Girotti	k1gMnSc2	Girotti
<g/>
,	,	kIx,	,
regenta	regens	k1gMnSc2	regens
Apoštolské	apoštolský	k2eAgFnSc2d1	apoštolská
penitenciatury	penitenciatura	k1gFnSc2	penitenciatura
pro	pro	k7c4	pro
L	L	kA	L
<g/>
'	'	kIx"	'
<g/>
Osservatore	Osservator	k1gMnSc5	Osservator
Romano	Romano	k1gMnSc5	Romano
<g/>
.	.	kIx.	.
</s>
<s>
Biskup	biskup	k1gMnSc1	biskup
Girotti	Girotť	k1gFnSc3	Girotť
žádné	žádný	k3yNgInPc4	žádný
nové	nový	k2eAgInPc4d1	nový
hlavní	hlavní	k2eAgInPc4d1	hlavní
hříchy	hřích	k1gInPc4	hřích
nedefinoval	definovat	k5eNaBmAgInS	definovat
ani	ani	k8xC	ani
nevytvořil	vytvořit	k5eNaPmAgInS	vytvořit
žádný	žádný	k3yNgInSc1	žádný
seznam	seznam	k1gInSc1	seznam
<g/>
,	,	kIx,	,
pouze	pouze	k6eAd1	pouze
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
(	(	kIx(	(
<g/>
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
<g/>
)	)	kIx)	)
mluvil	mluvit	k5eAaImAgMnS	mluvit
o	o	k7c6	o
sociálních	sociální	k2eAgInPc6d1	sociální
hříších	hřích	k1gInPc6	hřích
moderního	moderní	k2eAgInSc2d1	moderní
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
zdůrazňoval	zdůrazňovat	k5eAaImAgMnS	zdůrazňovat
jejich	jejich	k3xOp3gFnSc4	jejich
závažnost	závažnost	k1gFnSc4	závažnost
a	a	k8xC	a
vyjmenoval	vyjmenovat	k5eAaPmAgMnS	vyjmenovat
některé	některý	k3yIgInPc4	některý
jejich	jejich	k3xOp3gInPc4	jejich
příklady	příklad	k1gInPc4	příklad
<g/>
.	.	kIx.	.
</s>
<s>
Pojem	pojem	k1gInSc1	pojem
smrtelný	smrtelný	k2eAgInSc4d1	smrtelný
hřích	hřích	k1gInSc4	hřích
v	v	k7c6	v
celém	celý	k2eAgInSc6d1	celý
rozhovoru	rozhovor	k1gInSc6	rozhovor
vůbec	vůbec	k9	vůbec
nepadl	padnout	k5eNaPmAgMnS	padnout
<g/>
.	.	kIx.	.
</s>
<s>
Všechny	všechen	k3xTgInPc1	všechen
zveřejněné	zveřejněný	k2eAgInPc1d1	zveřejněný
seznamy	seznam	k1gInPc1	seznam
jsou	být	k5eAaImIp3nP	být
výtvory	výtvor	k1gInPc4	výtvor
novinářů	novinář	k1gMnPc2	novinář
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
některé	některý	k3yIgInPc4	některý
obsahují	obsahovat	k5eAaImIp3nP	obsahovat
položky	položka	k1gFnPc1	položka
<g/>
,	,	kIx,	,
které	který	k3yIgFnPc1	který
se	se	k3xPyFc4	se
v	v	k7c6	v
rozhovoru	rozhovor	k1gInSc6	rozhovor
vůbec	vůbec	k9	vůbec
neobjevily	objevit	k5eNaPmAgFnP	objevit
(	(	kIx(	(
<g/>
např.	např.	kA	např.
"	"	kIx"	"
<g/>
genetické	genetický	k2eAgFnPc4d1	genetická
modifikace	modifikace	k1gFnPc4	modifikace
potravin	potravina	k1gFnPc2	potravina
<g/>
"	"	kIx"	"
<g/>
)	)	kIx)	)
<g/>
.	.	kIx.	.
</s>
<s>
Sociální	sociální	k2eAgInPc1d1	sociální
hříchy	hřích	k1gInPc1	hřích
jsou	být	k5eAaImIp3nP	být
konkrétní	konkrétní	k2eAgInPc1d1	konkrétní
projevy	projev	k1gInPc1	projev
sedmi	sedm	k4xCc2	sedm
hlavních	hlavní	k2eAgMnPc2d1	hlavní
(	(	kIx(	(
<g/>
smrtelných	smrtelný	k2eAgInPc2d1	smrtelný
<g/>
)	)	kIx)	)
hříchů	hřích	k1gInPc2	hřích
a	a	k8xC	a
jejich	jejich	k3xOp3gInSc1	jejich
seznam	seznam	k1gInSc1	seznam
v	v	k7c6	v
žádném	žádný	k3yNgInSc6	žádný
případě	případ	k1gInSc6	případ
nerozšiřují	rozšiřovat	k5eNaImIp3nP	rozšiřovat
<g/>
.	.	kIx.	.
</s>
<s>
Mohou	moct	k5eAaImIp3nP	moct
být	být	k5eAaImF	být
ale	ale	k9	ale
smrtelnými	smrtelný	k2eAgInPc7d1	smrtelný
hříchy	hřích	k1gInPc7	hřích
ve	v	k7c6	v
smyslu	smysl	k1gInSc6	smysl
závažnosti	závažnost	k1gFnSc2	závažnost
<g/>
.	.	kIx.	.
</s>
<s>
Také	také	k9	také
se	se	k3xPyFc4	se
nejedná	jednat	k5eNaImIp3nS	jednat
o	o	k7c4	o
nic	nic	k3yNnSc4	nic
nově	nově	k6eAd1	nově
definovaného	definovaný	k2eAgMnSc2d1	definovaný
<g/>
,	,	kIx,	,
ale	ale	k8xC	ale
spíše	spíše	k9	spíše
jen	jen	k9	jen
o	o	k7c4	o
upozornění	upozornění	k1gNnSc4	upozornění
<g/>
,	,	kIx,	,
že	že	k8xS	že
význam	význam	k1gInSc1	význam
tohoto	tento	k3xDgInSc2	tento
druhu	druh	k1gInSc2	druh
hříchů	hřích	k1gInPc2	hřích
<g/>
,	,	kIx,	,
která	který	k3yQgFnSc1	který
církev	církev	k1gFnSc1	církev
odsuzovala	odsuzovat	k5eAaImAgFnS	odsuzovat
už	už	k6eAd1	už
dlouhodobě	dlouhodobě	k6eAd1	dlouhodobě
<g/>
,	,	kIx,	,
roste	růst	k5eAaImIp3nS	růst
<g/>
.	.	kIx.	.
</s>
<s>
Přestože	přestože	k8xS	přestože
římskokatolická	římskokatolický	k2eAgFnSc1d1	Římskokatolická
církev	církev	k1gFnSc1	církev
na	na	k7c6	na
mnoha	mnoho	k4c6	mnoho
místech	místo	k1gNnPc6	místo
tvrzení	tvrzení	k1gNnSc2	tvrzení
o	o	k7c6	o
nových	nový	k2eAgInPc6d1	nový
smrtelných	smrtelný	k2eAgInPc6d1	smrtelný
hříších	hřích	k1gInPc6	hřích
dementovala	dementovat	k5eAaBmAgFnS	dementovat
<g/>
,	,	kIx,	,
mimo	mimo	k7c4	mimo
jiné	jiný	k2eAgNnSc4d1	jiné
i	i	k9	i
oficiálním	oficiální	k2eAgNnSc7d1	oficiální
prohlášením	prohlášení	k1gNnSc7	prohlášení
České	český	k2eAgFnSc2d1	Česká
biskupské	biskupský	k2eAgFnSc2d1	biskupská
konference	konference	k1gFnSc2	konference
<g/>
,	,	kIx,	,
Česká	český	k2eAgFnSc1d1	Česká
televize	televize	k1gFnSc1	televize
odvysílala	odvysílat	k5eAaPmAgFnS	odvysílat
reportáž	reportáž	k1gFnSc4	reportáž
o	o	k7c4	o
zavedení	zavedení	k1gNnSc4	zavedení
nových	nový	k2eAgInPc2d1	nový
smrtelných	smrtelný	k2eAgInPc2d1	smrtelný
hříchů	hřích	k1gInPc2	hřích
a	a	k8xC	a
doplněnou	doplněná	k1gFnSc4	doplněná
o	o	k7c4	o
zlehčující	zlehčující	k2eAgFnPc4d1	zlehčující
sekvence	sekvence	k1gFnPc4	sekvence
ještě	ještě	k9	ještě
jednou	jednou	k6eAd1	jednou
<g/>
,	,	kIx,	,
v	v	k7c6	v
publicistickém	publicistický	k2eAgInSc6d1	publicistický
pořadu	pořad	k1gInSc6	pořad
168	[number]	k4	168
hodin	hodina	k1gFnPc2	hodina
<g/>
.	.	kIx.	.
</s>
<s>
Zejména	zejména	k9	zejména
tato	tento	k3xDgFnSc1	tento
prezentace	prezentace	k1gFnSc1	prezentace
ČT	ČT	kA	ČT
byla	být	k5eAaImAgFnS	být
chápána	chápat	k5eAaImNgFnS	chápat
řadou	řada	k1gFnSc7	řada
katolíků	katolík	k1gMnPc2	katolík
jako	jako	k8xC	jako
útok	útok	k1gInSc1	útok
na	na	k7c4	na
církev	církev	k1gFnSc4	církev
či	či	k8xC	či
příklad	příklad	k1gInSc4	příklad
nedbalosti	nedbalost	k1gFnSc2	nedbalost
při	při	k7c6	při
přebírání	přebírání	k1gNnSc6	přebírání
zpráv	zpráva	k1gFnPc2	zpráva
z	z	k7c2	z
druhé	druhý	k4xOgFnSc2	druhý
ruky	ruka	k1gFnSc2	ruka
<g/>
,	,	kIx,	,
nekompetence	nekompetence	k1gFnSc2	nekompetence
redaktorů	redaktor	k1gMnPc2	redaktor
v	v	k7c6	v
oblasti	oblast	k1gFnSc6	oblast
náboženské	náboženský	k2eAgFnSc2d1	náboženská
problematiky	problematika	k1gFnSc2	problematika
<g/>
,	,	kIx,	,
neochoty	neochota	k1gFnSc2	neochota
médií	médium	k1gNnPc2	médium
přiznat	přiznat	k5eAaPmF	přiznat
vlastní	vlastní	k2eAgFnSc4d1	vlastní
chybu	chyba	k1gFnSc4	chyba
a	a	k8xC	a
běžného	běžný	k2eAgInSc2d1	běžný
obrazu	obraz	k1gInSc2	obraz
našeho	náš	k3xOp1gInSc2	náš
zpravodajského	zpravodajský	k2eAgInSc2d1	zpravodajský
světa	svět	k1gInSc2	svět
<g/>
,	,	kIx,	,
případně	případně	k6eAd1	případně
obzvlášť	obzvlášť	k6eAd1	obzvlášť
okatou	okatý	k2eAgFnSc4d1	okatá
kombinaci	kombinace	k1gFnSc4	kombinace
obojího	obojí	k4xRgMnSc2	obojí
<g/>
.	.	kIx.	.
</s>
<s>
LaVeyův	LaVeyův	k2eAgInSc1d1	LaVeyův
satanismus	satanismus	k1gInSc1	satanismus
považuje	považovat	k5eAaImIp3nS	považovat
všech	všecek	k3xTgInPc2	všecek
sedm	sedm	k4xCc4	sedm
hříchů	hřích	k1gInPc2	hřích
křesťanství	křesťanství	k1gNnSc2	křesťanství
za	za	k7c4	za
pozitivní	pozitivní	k2eAgInSc4d1	pozitivní
<g/>
,	,	kIx,	,
pokud	pokud	k8xS	pokud
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
uspokojení	uspokojení	k1gNnSc3	uspokojení
hříšníka	hříšník	k1gMnSc2	hříšník
<g/>
.	.	kIx.	.
</s>
<s>
Označuje	označovat	k5eAaImIp3nS	označovat
je	být	k5eAaImIp3nS	být
za	za	k7c4	za
"	"	kIx"	"
<g/>
takzvané	takzvaný	k2eAgInPc4d1	takzvaný
<g/>
"	"	kIx"	"
hříchy	hřích	k1gInPc4	hřích
<g/>
,	,	kIx,	,
protože	protože	k8xS	protože
nesouhlasí	souhlasit	k5eNaImIp3nS	souhlasit
s	s	k7c7	s
názorem	názor	k1gInSc7	názor
na	na	k7c6	na
jejich	jejich	k3xOp3gInSc6	jejich
škodlivost	škodlivost	k1gFnSc4	škodlivost
<g/>
,	,	kIx,	,
naopak	naopak	k6eAd1	naopak
jejich	jejich	k3xOp3gMnPc1	jejich
potíraní	potíraný	k2eAgMnPc1d1	potíraný
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
zhoubné	zhoubný	k2eAgInPc4d1	zhoubný
<g/>
.	.	kIx.	.
</s>
<s>
Tyto	tento	k3xDgInPc4	tento
"	"	kIx"	"
<g/>
hříchy	hřích	k1gInPc4	hřích
<g/>
"	"	kIx"	"
považuje	považovat	k5eAaImIp3nS	považovat
za	za	k7c4	za
projev	projev	k1gInSc4	projev
přirozených	přirozený	k2eAgInPc2d1	přirozený
instinktů	instinkt	k1gInPc2	instinkt
<g/>
,	,	kIx,	,
které	který	k3yRgInPc4	který
není	být	k5eNaImIp3nS	být
třeba	třeba	k6eAd1	třeba
potlačovat	potlačovat	k5eAaImF	potlačovat
<g/>
.	.	kIx.	.
</s>
<s>
Satanská	satanský	k2eAgFnSc1d1	satanská
bible	bible	k1gFnSc1	bible
uvádí	uvádět	k5eAaImIp3nS	uvádět
jako	jako	k9	jako
osmý	osmý	k4xOgInSc4	osmý
bod	bod	k1gInSc4	bod
svého	svůj	k3xOyFgNnSc2	svůj
Devatera	devatero	k1gNnSc2	devatero
<g/>
:	:	kIx,	:
"	"	kIx"	"
<g/>
Satan	satan	k1gInSc1	satan
znamená	znamenat	k5eAaImIp3nS	znamenat
všechny	všechen	k3xTgInPc4	všechen
takzvané	takzvaný	k2eAgInPc4d1	takzvaný
hříchy	hřích	k1gInPc4	hřích
<g/>
,	,	kIx,	,
jelikož	jelikož	k8xS	jelikož
vedou	vést	k5eAaImIp3nP	vést
k	k	k7c3	k
fyzickému	fyzický	k2eAgNnSc3d1	fyzické
<g/>
,	,	kIx,	,
mentálnímu	mentální	k2eAgNnSc3d1	mentální
nebo	nebo	k8xC	nebo
emočnímu	emoční	k2eAgNnSc3d1	emoční
uspokojení	uspokojení	k1gNnSc3	uspokojení
<g/>
.	.	kIx.	.
<g/>
"	"	kIx"	"
Namísto	namísto	k7c2	namísto
sebeničící	sebeničící	k2eAgFnSc2d1	sebeničící
obětavosti	obětavost	k1gFnSc2	obětavost
<g/>
,	,	kIx,	,
pokořování	pokořování	k1gNnSc2	pokořování
a	a	k8xC	a
altruismu	altruismus	k1gInSc2	altruismus
staví	stavit	k5eAaImIp3nS	stavit
vzájemnost	vzájemnost	k1gFnSc4	vzájemnost
<g/>
,	,	kIx,	,
odpovědnost	odpovědnost	k1gFnSc4	odpovědnost
a	a	k8xC	a
společný	společný	k2eAgInSc4d1	společný
prospěch	prospěch	k1gInSc4	prospěch
<g/>
,	,	kIx,	,
přičemž	přičemž	k6eAd1	přičemž
v	v	k7c6	v
křesťanství	křesťanství	k1gNnSc6	křesťanství
a	a	k8xC	a
jeho	jeho	k3xOp3gFnSc3	jeho
morálce	morálka	k1gFnSc3	morálka
tyto	tento	k3xDgFnPc4	tento
hodnoty	hodnota	k1gFnPc4	hodnota
neshledává	shledávat	k5eNaImIp3nS	shledávat
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
svém	svůj	k3xOyFgNnSc6	svůj
díle	dílo	k1gNnSc6	dílo
Božská	božská	k1gFnSc1	božská
komedie	komedie	k1gFnSc2	komedie
využívá	využívat	k5eAaPmIp3nS	využívat
Dante	Dante	k1gMnSc1	Dante
Alighieri	Alighier	k1gFnSc2	Alighier
tyto	tento	k3xDgInPc4	tento
hříchy	hřích	k1gInPc4	hřích
jako	jako	k8xS	jako
stupně	stupeň	k1gInPc4	stupeň
v	v	k7c6	v
očistci	očistec	k1gInSc6	očistec
<g/>
,	,	kIx,	,
kterými	který	k3yQgMnPc7	který
musí	muset	k5eAaImIp3nP	muset
projít	projít	k5eAaPmF	projít
duše	duše	k1gFnPc4	duše
zemřelého	zemřelý	k1gMnSc2	zemřelý
a	a	k8xC	a
od	od	k7c2	od
hříchů	hřích	k1gInPc2	hřích
se	se	k3xPyFc4	se
očistit	očistit	k5eAaPmF	očistit
tím	ten	k3xDgNnSc7	ten
<g/>
,	,	kIx,	,
že	že	k8xS	že
se	se	k3xPyFc4	se
naučí	naučit	k5eAaPmIp3nP	naučit
protikladné	protikladný	k2eAgFnPc1d1	protikladná
ctnosti	ctnost	k1gFnPc1	ctnost
<g/>
.	.	kIx.	.
</s>
<s>
Jsou	být	k5eAaImIp3nP	být
to	ten	k3xDgNnSc4	ten
<g/>
:	:	kIx,	:
pokora	pokora	k1gFnSc1	pokora
<g/>
,	,	kIx,	,
štědrost	štědrost	k1gFnSc1	štědrost
<g/>
,	,	kIx,	,
přejícnost	přejícnost	k1gFnSc1	přejícnost
<g/>
,	,	kIx,	,
mírumilovnost	mírumilovnost	k1gFnSc1	mírumilovnost
<g/>
,	,	kIx,	,
cudnost	cudnost	k1gFnSc1	cudnost
<g/>
,	,	kIx,	,
střídmost	střídmost	k1gFnSc1	střídmost
a	a	k8xC	a
činorodost	činorodost	k1gFnSc1	činorodost
<g/>
.	.	kIx.	.
</s>
<s>
Podobně	podobně	k6eAd1	podobně
v	v	k7c6	v
Ráji	rája	k1gFnSc6	rája
jsou	být	k5eAaImIp3nP	být
z	z	k7c2	z
důvodů	důvod	k1gInPc2	důvod
literární	literární	k2eAgFnSc2d1	literární
paralely	paralela	k1gFnSc2	paralela
rozděleni	rozdělit	k5eAaPmNgMnP	rozdělit
ctností	ctnost	k1gFnPc2	ctnost
podle	podle	k7c2	podle
toho	ten	k3xDgNnSc2	ten
<g/>
,	,	kIx,	,
kterou	který	k3yIgFnSc4	který
z	z	k7c2	z
těchto	tento	k3xDgFnPc2	tento
ctností	ctnost	k1gFnPc2	ctnost
nejvíce	hodně	k6eAd3	hodně
projevili	projevit	k5eAaPmAgMnP	projevit
<g/>
.	.	kIx.	.
</s>
<s>
Toto	tento	k3xDgNnSc1	tento
pojetí	pojetí	k1gNnSc1	pojetí
je	být	k5eAaImIp3nS	být
sice	sice	k8xC	sice
plodem	plod	k1gInSc7	plod
básníkovy	básníkův	k2eAgFnSc2d1	básníkova
fantazie	fantazie	k1gFnSc2	fantazie
<g/>
,	,	kIx,	,
přesto	přesto	k8xC	přesto
se	se	k3xPyFc4	se
nijak	nijak	k6eAd1	nijak
nerozchází	rozcházet	k5eNaImIp3nS	rozcházet
s	s	k7c7	s
oficiálním	oficiální	k2eAgInSc7d1	oficiální
katolickým	katolický	k2eAgInSc7d1	katolický
pohledem	pohled	k1gInSc7	pohled
<g/>
.	.	kIx.	.
</s>
<s>
Ve	v	k7c6	v
filmu	film	k1gInSc6	film
Sedm	sedm	k4xCc4	sedm
byly	být	k5eAaImAgInP	být
hříchy	hřích	k1gInPc1	hřích
použity	použít	k5eAaPmNgInP	použít
jako	jako	k8xS	jako
inspirace	inspirace	k1gFnPc1	inspirace
a	a	k8xC	a
scénář	scénář	k1gInSc1	scénář
činnosti	činnost	k1gFnSc2	činnost
sériového	sériový	k2eAgMnSc2d1	sériový
vraha	vrah	k1gMnSc2	vrah
<g/>
.	.	kIx.	.
</s>
<s>
Sochy	socha	k1gFnPc1	socha
hříchů	hřích	k1gInPc2	hřích
a	a	k8xC	a
ctností	ctnost	k1gFnPc2	ctnost
od	od	k7c2	od
Matyáše	Matyáš	k1gMnSc2	Matyáš
Brauna	Braun	k1gMnSc2	Braun
zdobí	zdobit	k5eAaImIp3nP	zdobit
průčelí	průčelí	k1gNnSc4	průčelí
Hospitalu	Hospital	k1gMnSc6	Hospital
v	v	k7c6	v
areálu	areál	k1gInSc6	areál
Kuksu	Kuks	k1gInSc2	Kuks
<g/>
.	.	kIx.	.
</s>
<s>
Hieronymus	Hieronymus	k1gMnSc1	Hieronymus
Bosch	Bosch	kA	Bosch
roku	rok	k1gInSc2	rok
1485	[number]	k4	1485
namaloval	namalovat	k5eAaPmAgMnS	namalovat
olejem	olej	k1gInSc7	olej
na	na	k7c6	na
dřevě	dřevo	k1gNnSc6	dřevo
120	[number]	k4	120
×	×	k?	×
150	[number]	k4	150
cm	cm	kA	cm
obraz	obraz	k1gInSc4	obraz
Sedm	sedm	k4xCc4	sedm
smrtelných	smrtelný	k2eAgInPc2d1	smrtelný
hříchů	hřích	k1gInPc2	hřích
<g/>
.	.	kIx.	.
</s>
<s>
Dnes	dnes	k6eAd1	dnes
se	se	k3xPyFc4	se
nachází	nacházet	k5eAaImIp3nS	nacházet
v	v	k7c6	v
Museo	Museo	k6eAd1	Museo
del	del	k?	del
Prado	Prado	k1gNnSc1	Prado
<g/>
,	,	kIx,	,
Madrid	Madrid	k1gInSc1	Madrid
<g/>
.	.	kIx.	.
</s>
