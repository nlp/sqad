<s>
Cesta	cesta	k1gFnSc1
do	do	k7c2
středu	střed	k1gInSc2
Země	zem	k1gFnSc2
Frontispis	frontispis	k1gInSc1
francouzského	francouzský	k2eAgMnSc2d1
vydáníAutor	vydáníAutor	k1gInSc4
</s>
<s>
Cesta	cesta	k1gFnSc1
do	do	k7c2
středu	střed	k1gInSc2
Země	zem	k1gFnSc2
nebo	nebo	k8xC
také	také	k9
jen	jen	k9
Do	do	k7c2
středu	střed	k1gInSc2
Země	zem	k1gFnSc2
(	(	kIx(
<g/>
1864	#num#	k4
<g/>
,	,	kIx,
Le	Le	k1gFnSc1
voyage	voyag	k1gInSc2
au	au	k0
centre	centr	k1gInSc5
de	de	k?
la	la	k0
Terre	Terr	k1gMnSc5
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
dobrodružný	dobrodružný	k2eAgInSc1d1
vědeckofantastický	vědeckofantastický	k2eAgInSc1d1
román	román	k1gInSc1
francouzského	francouzský	k2eAgMnSc2d1
spisovatele	spisovatel	k1gMnSc2
Julesa	Jules	k1gFnSc2
Verna	Verne	k1gFnSc2
<g/>
.	.	kIx.
</s>