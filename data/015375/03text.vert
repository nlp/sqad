<s>
Cesta	cesta	k1gFnSc1
do	do	k7c2
středu	střed	k1gInSc2
Země	zem	k1gFnSc2
</s>
<s>
Další	další	k2eAgInPc1d1
významy	význam	k1gInPc1
jsou	být	k5eAaImIp3nP
uvedeny	uvést	k5eAaPmNgInP
na	na	k7c6
stránce	stránka	k1gFnSc6
Cesta	cesta	k1gFnSc1
do	do	k7c2
středu	střed	k1gInSc2
Země	zem	k1gFnSc2
(	(	kIx(
<g/>
rozcestník	rozcestník	k1gInSc1
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Cesta	cesta	k1gFnSc1
do	do	k7c2
středu	střed	k1gInSc2
Země	zem	k1gFnSc2
Frontispis	frontispis	k1gInSc1
francouzského	francouzský	k2eAgMnSc2d1
vydáníAutor	vydáníAutor	k1gInSc4
</s>
<s>
Jules	Jules	k1gMnSc1
Verne	Verne	k1gMnSc1
Původní	původní	k2eAgInSc1d1
název	název	k1gInSc4
</s>
<s>
Le	Le	k?
voyage	voyagat	k5eAaPmIp3nS
au	au	k0
centre	centr	k1gInSc5
de	de	k?
la	la	k0
Terre	Terr	k1gInSc5
Ilustrátor	ilustrátor	k1gMnSc1
</s>
<s>
Édouard	Édouard	k1gMnSc1
Riou	Rious	k1gInSc2
Země	zem	k1gFnSc2
</s>
<s>
Francie	Francie	k1gFnSc1
Jazyk	jazyk	k1gInSc1
</s>
<s>
francouzština	francouzština	k1gFnSc1
Edice	edice	k1gFnSc2
</s>
<s>
Podivuhodné	podivuhodný	k2eAgFnPc1d1
cesty	cesta	k1gFnPc1
Žánr	žánr	k1gInSc4
</s>
<s>
dobrodružný	dobrodružný	k2eAgInSc1d1
vědeckofantatický	vědeckofantatický	k2eAgInSc1d1
román	román	k1gInSc1
Ocenění	ocenění	k1gNnSc1
</s>
<s>
NPR	NPR	kA
Top	topit	k5eAaImRp2nS
100	#num#	k4
Science	Scienec	k1gInSc2
Fiction	Fiction	k1gInSc1
and	and	k?
Fantasy	fantas	k1gInPc1
Books	Books	k1gInSc1
Vydavatel	vydavatel	k1gMnSc1
</s>
<s>
Pierre-Jules	Pierre-Jules	k1gInSc4
Hetzel	Hetzel	k1gFnSc2
Datum	datum	k1gNnSc4
vydání	vydání	k1gNnSc2
</s>
<s>
1864	#num#	k4
Česky	česky	k6eAd1
vydáno	vydat	k5eAaPmNgNnS
</s>
<s>
1881	#num#	k4
Předchozí	předchozí	k2eAgInSc4d1
a	a	k8xC
následující	následující	k2eAgInSc4d1
díl	díl	k1gInSc4
</s>
<s>
Dobrodružství	dobrodružství	k1gNnSc1
kapitána	kapitán	k1gMnSc2
Hatterase	Hatterasa	k1gFnSc6
</s>
<s>
Ze	z	k7c2
Země	zem	k1gFnSc2
na	na	k7c4
Měsíc	měsíc	k1gInSc4
</s>
<s>
multimediální	multimediální	k2eAgInSc1d1
obsah	obsah	k1gInSc1
na	na	k7c4
Commons	Commons	k1gInSc4
Některá	některý	k3yIgNnPc1
data	datum	k1gNnPc4
mohou	moct	k5eAaImIp3nP
pocházet	pocházet	k5eAaImF
z	z	k7c2
datové	datový	k2eAgFnSc2d1
položky	položka	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Cesta	cesta	k1gFnSc1
do	do	k7c2
středu	střed	k1gInSc2
Země	zem	k1gFnSc2
nebo	nebo	k8xC
také	také	k9
jen	jen	k9
Do	do	k7c2
středu	střed	k1gInSc2
Země	zem	k1gFnSc2
(	(	kIx(
<g/>
1864	#num#	k4
<g/>
,	,	kIx,
Le	Le	k1gFnSc1
voyage	voyag	k1gInSc2
au	au	k0
centre	centr	k1gInSc5
de	de	k?
la	la	k0
Terre	Terr	k1gMnSc5
<g/>
)	)	kIx)
je	být	k5eAaImIp3nS
dobrodružný	dobrodružný	k2eAgInSc1d1
vědeckofantastický	vědeckofantastický	k2eAgInSc1d1
román	román	k1gInSc1
francouzského	francouzský	k2eAgMnSc2d1
spisovatele	spisovatel	k1gMnSc2
Julesa	Jules	k1gFnSc2
Verna	Verne	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Autor	autor	k1gMnSc1
ve	v	k7c6
svém	svůj	k3xOyFgInSc6
druhém	druhý	k4xOgInSc6
románu	román	k1gInSc6
z	z	k7c2
cyklu	cyklus	k1gInSc2
Podivuhodné	podivuhodný	k2eAgFnSc2d1
cesty	cesta	k1gFnSc2
(	(	kIx(
<g/>
Les	les	k1gInSc1
Voyages	Voyages	k1gMnSc1
extraordinaires	extraordinaires	k1gMnSc1
<g/>
)	)	kIx)
líčí	líčit	k5eAaImIp3nS
fantastickou	fantastický	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
třech	tři	k4xCgMnPc2
odvážlivců	odvážlivec	k1gMnPc2
<g/>
,	,	kIx,
německého	německý	k2eAgMnSc2d1
přírodovědce	přírodovědec	k1gMnSc2
a	a	k8xC
profesora	profesor	k1gMnSc2
mineralogie	mineralogie	k1gFnSc2
Otty	Otta	k1gMnSc2
Liddebbrocka	Liddebbrocko	k1gNnSc2
<g/>
,	,	kIx,
jeho	on	k3xPp3gMnSc2,k3xOp3gMnSc2
mladého	mladý	k2eAgMnSc2d1
synovce	synovec	k1gMnSc2
Axela	Axel	k1gMnSc2
a	a	k8xC
jejich	jejich	k3xOp3gMnSc2
islandského	islandský	k2eAgMnSc2d1
průvodce	průvodce	k1gMnSc2
Hanse	Hans	k1gMnSc2
Bjelkeho	Bjelke	k1gMnSc2
do	do	k7c2
středu	střed	k1gInSc2
Země	zem	k1gFnSc2
(	(	kIx(
<g/>
což	což	k3yRnSc1,k3yQnSc1
je	být	k5eAaImIp3nS
místo	místo	k1gNnSc4
v	v	k7c6
hloubce	hloubka	k1gFnSc6
asi	asi	k9
6	#num#	k4
378	#num#	k4
kilometrů	kilometr	k1gInPc2
kolmo	kolmo	k6eAd1
pod	pod	k7c7
povrchem	povrch	k1gInSc7
naší	náš	k3xOp1gFnSc2
planety	planeta	k1gFnSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s>
Na	na	k7c4
Vernův	Vernův	k2eAgInSc4d1
román	román	k1gInSc4
později	pozdě	k6eAd2
navázal	navázat	k5eAaPmAgMnS
český	český	k2eAgMnSc1d1
spisovatel	spisovatel	k1gMnSc1
Ludvík	Ludvík	k1gMnSc1
Souček	Souček	k1gMnSc1
trilogií	trilogie	k1gFnPc2
Cesta	cesta	k1gFnSc1
slepých	slepý	k2eAgMnPc2d1
ptáků	pták	k1gMnPc2
<g/>
.	.	kIx.
</s>
<s>
Obsah	obsah	k1gInSc1
románu	román	k1gInSc2
</s>
<s>
PřeskočitVarování	PřeskočitVarování	k1gNnSc1
<g/>
:	:	kIx,
Následující	následující	k2eAgFnSc1d1
část	část	k1gFnSc1
článku	článek	k1gInSc2
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Příběh	příběh	k1gInSc1
je	být	k5eAaImIp3nS
vyprávěn	vyprávěn	k2eAgInSc1d1
očima	oko	k1gNnPc7
šestnáctiletého	šestnáctiletý	k2eAgNnSc2d1
Axela	Axelo	k1gNnSc2
a	a	k8xC
začíná	začínat	k5eAaImIp3nS
přesně	přesně	k6eAd1
v	v	k7c6
neděli	neděle	k1gFnSc6
24	#num#	k4
<g/>
.	.	kIx.
května	květen	k1gInSc2
roku	rok	k1gInSc2
1863	#num#	k4
<g/>
,	,	kIx,
kdy	kdy	k6eAd1
profesor	profesor	k1gMnSc1
Liddenbrock	Liddenbrock	k1gMnSc1
náhodu	náhoda	k1gFnSc4
objeví	objevit	k5eAaPmIp3nS
zašifrovaný	zašifrovaný	k2eAgInSc1d1
dokument	dokument	k1gInSc1
islandského	islandský	k2eAgMnSc2d1
alchymisty	alchymista	k1gMnSc2
ze	z	k7c2
16	#num#	k4
<g/>
.	.	kIx.
století	století	k1gNnSc4
Arneho	Arne	k1gMnSc2
Saknussemma	Saknussemm	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Podaří	podařit	k5eAaPmIp3nS
se	se	k3xPyFc4
mu	on	k3xPp3gNnSc3
text	text	k1gInSc1
psaný	psaný	k2eAgInSc1d1
runovým	runový	k2eAgNnSc7d1
písmem	písmo	k1gNnSc7
rozluštit	rozluštit	k5eAaPmF
a	a	k8xC
zjistí	zjistit	k5eAaPmIp3nS
<g/>
,	,	kIx,
že	že	k8xS
Saknussemm	Saknussemm	k1gInSc1
v	v	k7c6
dokumentu	dokument	k1gInSc6
tvrdí	tvrdit	k5eAaImIp3nS
<g/>
,	,	kIx,
že	že	k8xS
sestoupil	sestoupit	k5eAaPmAgMnS
do	do	k7c2
středu	střed	k1gInSc2
Země	zem	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Profesor	profesor	k1gMnSc1
okamžitě	okamžitě	k6eAd1
zorganizuje	zorganizovat	k5eAaPmIp3nS
výpravu	výprava	k1gFnSc4
na	na	k7c4
Island	Island	k1gInSc4
<g/>
,	,	kIx,
protože	protože	k8xS
podle	podle	k7c2
Saknussemma	Saknussemmum	k1gNnSc2
je	být	k5eAaImIp3nS
vstupní	vstupní	k2eAgNnSc1d1
místo	místo	k1gNnSc1
pro	pro	k7c4
cestu	cesta	k1gFnSc4
do	do	k7c2
podzemí	podzemí	k1gNnSc2
v	v	k7c6
jednom	jeden	k4xCgInSc6
ze	z	k7c2
tří	tři	k4xCgInPc2
kráterů	kráter	k1gInPc2
vyhaslé	vyhaslý	k2eAgFnSc2d1
islandské	islandský	k2eAgFnSc2d1
sopky	sopka	k1gFnSc2
Snæ	Snæ	k1gFnPc2
<g/>
,	,	kIx,
a	a	k8xC
to	ten	k3xDgNnSc1
v	v	k7c6
tom	ten	k3xDgNnSc6
<g/>
,	,	kIx,
na	na	k7c4
nějž	jenž	k3xRgMnSc4
dopadne	dopadnout	k5eAaPmIp3nS
posledního	poslední	k2eAgInSc2d1
červnového	červnový	k2eAgInSc2d1
dne	den	k1gInSc2
stín	stín	k1gInSc1
horského	horský	k2eAgInSc2d1
štítu	štít	k1gInSc2
Scartarisu	Scartaris	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Cesta	cesta	k1gFnSc1
do	do	k7c2
středu	střed	k1gInSc2
Země	zem	k1gFnSc2
<g/>
,	,	kIx,
šifrovaný	šifrovaný	k2eAgInSc4d1
dokument	dokument	k1gInSc4
Arneho	Arne	k1gMnSc2
Saknusemma	Saknusemm	k1gMnSc2
<g/>
.	.	kIx.
</s>
<s>
Po	po	k7c6
určení	určení	k1gNnSc6
správného	správný	k2eAgInSc2d1
kráteru	kráter	k1gInSc2
začala	začít	k5eAaPmAgFnS
výprava	výprava	k1gFnSc1
sestupovat	sestupovat	k5eAaImF
do	do	k7c2
zemských	zemský	k2eAgFnPc2d1
hlubin	hlubina	k1gFnPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Světlo	světlo	k1gNnSc1
cestovatelům	cestovatel	k1gMnPc3
zajišťovaly	zajišťovat	k5eAaImAgFnP
elektrické	elektrický	k2eAgFnPc4d1
lampy	lampa	k1gFnPc4
a	a	k8xC
před	před	k7c7
žízní	žízeň	k1gFnSc7
je	být	k5eAaImIp3nS
zachránila	zachránit	k5eAaPmAgFnS
nalezená	nalezený	k2eAgFnSc1d1
podzemní	podzemní	k2eAgFnSc1d1
řeka	řeka	k1gFnSc1
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
dvacet	dvacet	k4xCc4
dní	den	k1gInPc2
ušli	ujít	k5eAaPmAgMnP
asi	asi	k9
130	#num#	k4
kilometrů	kilometr	k1gInPc2
a	a	k8xC
ocitli	ocitnout	k5eAaPmAgMnP
se	se	k3xPyFc4
v	v	k7c6
hloubce	hloubka	k1gFnSc6
kolem	kolem	k7c2
24	#num#	k4
kilometrů	kilometr	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Devátého	devátý	k4xOgInSc2
srpna	srpen	k1gInSc2
učinili	učinit	k5eAaPmAgMnP,k5eAaImAgMnP
cestovatelé	cestovatel	k1gMnPc1
neuvěřitelný	uvěřitelný	k2eNgInSc4d1
objev	objev	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Zhruba	zhruba	k6eAd1
v	v	k7c6
hloubce	hloubka	k1gFnSc6
55	#num#	k4
kilometrů	kilometr	k1gInPc2
pod	pod	k7c7
zemským	zemský	k2eAgInSc7d1
povrchem	povrch	k1gInSc7
vstoupili	vstoupit	k5eAaPmAgMnP
do	do	k7c2
gigantické	gigantický	k2eAgFnSc2d1
dutiny	dutina	k1gFnSc2
osvětlené	osvětlený	k2eAgFnSc2d1
jakousi	jakýsi	k3yIgFnSc7
elektrickou	elektrický	k2eAgFnSc7d1
září	zář	k1gFnSc7
(	(	kIx(
<g/>
kterou	který	k3yIgFnSc7,k3yQgFnSc7,k3yRgFnSc7
Verne	Vern	k1gInSc5
přirovnával	přirovnávat	k5eAaImAgMnS
k	k	k7c3
záři	zář	k1gFnSc3
polární	polární	k2eAgInSc1d1
<g/>
)	)	kIx)
<g/>
,	,	kIx,
ve	v	k7c6
které	který	k3yIgFnSc6,k3yQgFnSc6,k3yRgFnSc6
bylo	být	k5eAaImAgNnS
obrovské	obrovský	k2eAgNnSc4d1
podzemní	podzemní	k2eAgNnSc4d1
moře	moře	k1gNnSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Na	na	k7c6
jeho	jeho	k3xOp3gInPc6
březích	břeh	k1gInPc6
výprava	výprava	k1gFnSc1
nalezla	nalézt	k5eAaBmAgFnS,k5eAaPmAgFnS
les	les	k1gInSc4
obrovských	obrovský	k2eAgFnPc2d1
hub	houba	k1gFnPc2
a	a	k8xC
mnoho	mnoho	k4c4
dávno	dávno	k6eAd1
vymizelých	vymizelý	k2eAgInPc2d1
druhohorních	druhohorní	k2eAgInPc2d1
a	a	k8xC
třetihorních	třetihorní	k2eAgInPc2d1
rostlinných	rostlinný	k2eAgInPc2d1
i	i	k8xC
živočišných	živočišný	k2eAgInPc2d1
druhů	druh	k1gInPc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cestovatelé	cestovatel	k1gMnPc1
si	se	k3xPyFc3
postavili	postavit	k5eAaPmAgMnP
vor	vor	k1gInSc4
a	a	k8xC
13	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
se	se	k3xPyFc4
vydali	vydat	k5eAaPmAgMnP
na	na	k7c4
plavbu	plavba	k1gFnSc4
<g/>
,	,	kIx,
při	při	k7c6
které	který	k3yQgFnSc6,k3yRgFnSc6,k3yIgFnSc6
mohli	moct	k5eAaImAgMnP
pozorovat	pozorovat	k5eAaImF
zápas	zápas	k1gInSc4
ichtyosaura	ichtyosaurus	k1gMnSc2
s	s	k7c7
plesiosaurem	plesiosaur	k1gMnSc7
<g/>
.	.	kIx.
</s>
<s desamb="1">
Za	za	k7c4
týden	týden	k1gInSc4
urazili	urazit	k5eAaPmAgMnP
více	hodně	k6eAd2
než	než	k8xS
400	#num#	k4
kilometrů	kilometr	k1gInPc2
a	a	k8xC
o	o	k7c4
pět	pět	k4xCc4
dní	den	k1gInPc2
později	pozdě	k6eAd2
odhadovali	odhadovat	k5eAaImAgMnP
<g/>
,	,	kIx,
že	že	k8xS
již	již	k6eAd1
propluli	proplout	k5eAaPmAgMnP
pod	pod	k7c7
Anglií	Anglie	k1gFnSc7
a	a	k8xC
Francií	Francie	k1gFnSc7
(	(	kIx(
<g/>
celé	celý	k2eAgNnSc1d1
moře	moře	k1gNnSc1
bylo	být	k5eAaImAgNnS
nakonec	nakonec	k6eAd1
široké	široký	k2eAgNnSc1d1
téměř	téměř	k6eAd1
900	#num#	k4
kilometrů	kilometr	k1gInPc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Po	po	k7c6
přistání	přistání	k1gNnSc6
na	na	k7c6
druhém	druhý	k4xOgInSc6
břehu	břeh	k1gInSc6
nalezli	naleznout	k5eAaPmAgMnP,k5eAaBmAgMnP
dokonce	dokonce	k9
skupinu	skupina	k1gFnSc4
mastodontů	mastodont	k1gMnPc2
<g/>
,	,	kIx,
které	který	k3yQgMnPc4,k3yRgMnPc4,k3yIgMnPc4
sledoval	sledovat	k5eAaImAgMnS
pračlověk	pračlověk	k1gMnSc1
vysoký	vysoký	k2eAgMnSc1d1
asi	asi	k9
12	#num#	k4
stop	stopa	k1gFnPc2
(	(	kIx(
<g/>
tj.	tj.	kA
3,6	3,6	k4
metru	metr	k1gInSc2
<g/>
)	)	kIx)
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cestovatelé	cestovatel	k1gMnPc1
zde	zde	k6eAd1
také	také	k9
objevili	objevit	k5eAaPmAgMnP
Saknussemmovo	Saknussemmův	k2eAgNnSc4d1
znamení	znamení	k1gNnSc4
<g/>
,	,	kIx,
ukazující	ukazující	k2eAgFnSc4d1
další	další	k2eAgFnSc4d1
cestu	cesta	k1gFnSc4
do	do	k7c2
podzemí	podzemí	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
V	v	k7c6
té	ten	k3xDgFnSc6
jim	on	k3xPp3gMnPc3
však	však	k9
bránil	bránit	k5eAaImAgInS
zával	zával	k1gInSc1
<g/>
,	,	kIx,
který	který	k3yIgInSc4,k3yQgInSc4,k3yRgInSc4
se	se	k3xPyFc4
pokusili	pokusit	k5eAaPmAgMnP
odstřelit	odstřelit	k5eAaPmF
pomocí	pomocí	k7c2
střelného	střelný	k2eAgInSc2d1
prachu	prach	k1gInSc2
<g/>
.	.	kIx.
</s>
<s desamb="1">
Před	před	k7c7
výbuchem	výbuch	k1gInSc7
odpádlovali	odpádlovat	k5eAaImAgMnP,k5eAaPmAgMnP,k5eAaBmAgMnP
na	na	k7c6
moře	mora	k1gFnSc6
<g/>
,	,	kIx,
ale	ale	k8xC
exploze	exploze	k1gFnSc1
byla	být	k5eAaImAgFnS
větší	veliký	k2eAgFnSc1d2
<g/>
,	,	kIx,
než	než	k8xS
očekávali	očekávat	k5eAaImAgMnP
<g/>
.	.	kIx.
</s>
<s desamb="1">
Vor	vor	k1gInSc1
byl	být	k5eAaImAgInS
uvolněným	uvolněný	k2eAgInSc7d1
proudem	proud	k1gInSc7
vody	voda	k1gFnSc2
vehnán	vehnán	k2eAgInSc4d1
do	do	k7c2
velké	velký	k2eAgFnSc2d1
pukliny	puklina	k1gFnSc2
<g/>
,	,	kIx,
po	po	k7c6
určité	určitý	k2eAgFnSc6d1
době	doba	k1gFnSc6
se	se	k3xPyFc4
dostal	dostat	k5eAaPmAgMnS
do	do	k7c2
komína	komín	k1gInSc2
plného	plný	k2eAgInSc2d1
stoupající	stoupající	k2eAgFnSc4d1
vody	voda	k1gFnPc4
a	a	k8xC
magmatu	magma	k1gNnSc2
a	a	k8xC
nakonec	nakonec	k6eAd1
byl	být	k5eAaImAgInS
28	#num#	k4
<g/>
.	.	kIx.
srpna	srpen	k1gInSc2
vyvržen	vyvrhnout	k5eAaPmNgInS
na	na	k7c4
zemský	zemský	k2eAgInSc4d1
povrch	povrch	k1gInSc4
<g/>
.	.	kIx.
</s>
<s desamb="1">
Cestovatelé	cestovatel	k1gMnPc1
brzy	brzy	k6eAd1
zjistili	zjistit	k5eAaPmAgMnP
<g/>
,	,	kIx,
že	že	k8xS
se	se	k3xPyFc4
z	z	k7c2
hlubin	hlubina	k1gFnPc2
Země	zem	k1gFnSc2
dostali	dostat	k5eAaPmAgMnP
vedlejším	vedlejší	k2eAgInSc7d1
kráterem	kráter	k1gInSc7
sopky	sopka	k1gFnSc2
Stromboli	Strombole	k1gFnSc4
na	na	k7c6
Liparských	Liparský	k2eAgInPc6d1
ostrovech	ostrov	k1gInPc6
severně	severně	k6eAd1
od	od	k7c2
Sicílie	Sicílie	k1gFnSc2
<g/>
,	,	kIx,
a	a	k8xC
s	s	k7c7
velkou	velký	k2eAgFnSc7d1
slávou	sláva	k1gFnSc7
se	se	k3xPyFc4
vrátili	vrátit	k5eAaPmAgMnP
domů	domů	k6eAd1
<g/>
.	.	kIx.
</s>
<s>
Díky	díky	k7c3
mladému	mladý	k1gMnSc3
vypravěči	vypravěč	k1gMnSc3
má	mít	k5eAaImIp3nS
román	román	k1gInSc4
obrovský	obrovský	k2eAgInSc4d1
spád	spád	k1gInSc4
<g/>
,	,	kIx,
protože	protože	k8xS
Axelovy	Axelův	k2eAgFnPc1d1
vědomosti	vědomost	k1gFnPc1
nemohou	moct	k5eNaImIp3nP
být	být	k5eAaImF
díky	díky	k7c3
jeho	jeho	k3xOp3gInSc3
věku	věk	k1gInSc3
tak	tak	k8xC,k8xS
rozsáhlé	rozsáhlý	k2eAgFnSc2d1
<g/>
,	,	kIx,
aby	aby	kYmCp3nS
Jules	Jules	k1gInSc1
Verne	Vern	k1gInSc5
mohl	moct	k5eAaImAgInS
zaplnit	zaplnit	k5eAaPmF
své	svůj	k3xOyFgNnSc1
dílo	dílo	k1gNnSc1
vědeckými	vědecký	k2eAgInPc7d1
fakty	fakt	k1gInPc7
<g/>
,	,	kIx,
kterými	který	k3yRgFnPc7,k3yQgFnPc7,k3yIgFnPc7
mnohdy	mnohdy	k6eAd1
děj	děj	k1gInSc1
svých	svůj	k3xOyFgInPc2
příběhů	příběh	k1gInPc2
poněkud	poněkud	k6eAd1
rozmělňoval	rozmělňovat	k5eAaImAgInS
<g/>
.	.	kIx.
</s>
<s desamb="1">
Proto	proto	k8xC
patří	patřit	k5eAaImIp3nS
tato	tento	k3xDgFnSc1
kniha	kniha	k1gFnSc1
přes	přes	k7c4
neudržitelný	udržitelný	k2eNgInSc4d1
vědecký	vědecký	k2eAgInSc4d1
základ	základ	k1gInSc4
ke	k	k7c3
stále	stále	k6eAd1
velmi	velmi	k6eAd1
oblíbeným	oblíbený	k2eAgNnPc3d1
Vernovým	Vernův	k2eAgNnPc3d1
dílům	dílo	k1gNnPc3
</s>
<s>
Konec	konec	k1gInSc1
části	část	k1gFnSc2
článku	článek	k1gInSc2
<g/>
,	,	kIx,
která	který	k3yQgFnSc1,k3yRgFnSc1,k3yIgFnSc1
vyzrazuje	vyzrazovat	k5eAaImIp3nS
zápletku	zápletka	k1gFnSc4
nebo	nebo	k8xC
rozuzlení	rozuzlení	k1gNnSc4
díla	dílo	k1gNnSc2
<g/>
.	.	kIx.
</s>
<s>
Ilustrace	ilustrace	k1gFnSc1
</s>
<s>
Knihu	kniha	k1gFnSc4
Cesta	cesta	k1gFnSc1
do	do	k7c2
středu	střed	k1gInSc2
Země	zem	k1gFnSc2
ilustroval	ilustrovat	k5eAaBmAgMnS
Édouard	Édouard	k1gMnSc1
Riou	Rious	k1gInSc2
<g/>
.	.	kIx.
</s>
<s>
Filmové	filmový	k2eAgFnPc1d1
adaptace	adaptace	k1gFnPc1
</s>
<s>
Díky	díky	k7c3
své	svůj	k3xOyFgFnSc3
popularitě	popularita	k1gFnSc3
byl	být	k5eAaImAgInS
román	román	k1gInSc1
několikrát	několikrát	k6eAd1
zfilmován	zfilmovat	k5eAaPmNgInS
<g/>
:	:	kIx,
</s>
<s>
Journey	Journe	k1gMnPc4
to	ten	k3xDgNnSc1
the	the	k?
Center	centrum	k1gNnPc2
of	of	k?
the	the	k?
Earth	Earth	k1gInSc1
(	(	kIx(
<g/>
1959	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
USA	USA	kA
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Henry	Henry	k1gMnSc1
Levin	Levin	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Viaje	Viaje	k1gFnSc1
al	ala	k1gFnPc2
centro	centro	k6eAd1
de	de	k?
la	la	k1gNnSc1
Tierra	Tierra	k1gMnSc1
(	(	kIx(
<g/>
1976	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
Španělsko	Španělsko	k1gNnSc1
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Juan	Juan	k1gMnSc1
Piquer	Piquer	k1gMnSc1
Simón	Simón	k1gMnSc1
</s>
<s>
Journey	Journe	k1gMnPc4
to	ten	k3xDgNnSc1
the	the	k?
Center	centrum	k1gNnPc2
of	of	k?
the	the	k?
Earth	Earth	k1gInSc1
(	(	kIx(
<g/>
1989	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
USA	USA	kA
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Rusty	Rusta	k1gFnSc2
Lemorande	Lemorand	k1gInSc5
a	a	k8xC
Albert	Albert	k1gMnSc1
Pyun	Pyun	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Journey	Journe	k1gMnPc4
to	ten	k3xDgNnSc1
the	the	k?
Center	centrum	k1gNnPc2
of	of	k?
the	the	k?
Earth	Earth	k1gInSc1
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
USA	USA	kA
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
William	William	k1gInSc1
Dear	Dear	k1gInSc1
<g/>
,	,	kIx,
televizní	televizní	k2eAgInSc1d1
film	film	k1gInSc1
<g/>
,	,	kIx,
</s>
<s>
Willy	Willa	k1gFnPc1
Fog	Fog	k1gFnSc2
na	na	k7c6
cestě	cesta	k1gFnSc6
za	za	k7c7
dobrodružstvím	dobrodružství	k1gNnSc7
(	(	kIx(
<g/>
1993	#num#	k4
<g/>
)	)	kIx)
Španělsko	Španělsko	k1gNnSc1
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Claudio	Claudio	k1gMnSc1
Biern	Biern	k1gMnSc1
Boyd	Boyd	k1gMnSc1
<g/>
,	,	kIx,
animovaný	animovaný	k2eAgInSc1d1
seriál	seriál	k1gInSc1
</s>
<s>
Journey	Journe	k1gMnPc4
to	ten	k3xDgNnSc1
the	the	k?
Center	centrum	k1gNnPc2
of	of	k?
the	the	k?
Earth	Earth	k1gInSc1
(	(	kIx(
<g/>
1999	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
USA	USA	kA
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
George	George	k1gFnPc2
Miller	Miller	k1gMnSc1
<g/>
,	,	kIx,
dvoudílný	dvoudílný	k2eAgInSc1d1
televizní	televizní	k2eAgInSc1d1
film	film	k1gInSc1
na	na	k7c4
motivy	motiv	k1gInPc4
z	z	k7c2
knihy	kniha	k1gFnSc2
<g/>
.	.	kIx.
</s>
<s>
Journey	Journe	k1gMnPc4
to	ten	k3xDgNnSc1
the	the	k?
Center	centrum	k1gNnPc2
of	of	k?
the	the	k?
Earth	Earth	k1gInSc1
3D	3D	k4
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
USA	USA	kA
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
Eric	Eric	k1gFnSc1
Brevig	Brevig	k1gInSc4
<g/>
,	,	kIx,
film	film	k1gInSc4
<g/>
,	,	kIx,
natočen	natočit	k5eAaBmNgInS
ve	v	k7c6
speciální	speciální	k2eAgFnSc6d1
verzi	verze	k1gFnSc6
pro	pro	k7c4
kina	kino	k1gNnPc4
IMAX	IMAX	kA
<g/>
.	.	kIx.
</s>
<s>
Journey	Journe	k1gMnPc4
to	ten	k3xDgNnSc1
the	the	k?
Center	centrum	k1gNnPc2
of	of	k?
the	the	k?
Earth	Earth	k1gInSc1
(	(	kIx(
<g/>
2008	#num#	k4
<g/>
)	)	kIx)
<g/>
,	,	kIx,
USA	USA	kA
<g/>
,	,	kIx,
režie	režie	k1gFnSc1
T.J.	T.J.	k1gMnSc1
Scott	Scott	k1gMnSc1
<g/>
,	,	kIx,
televizní	televizní	k2eAgInSc1d1
film	film	k1gInSc1
<g/>
.	.	kIx.
</s>
<s>
Česká	český	k2eAgNnPc1d1
vydání	vydání	k1gNnPc1
</s>
<s>
Cesta	cesta	k1gFnSc1
do	do	k7c2
středu	střed	k1gInSc2
Země	zem	k1gFnSc2
<g/>
,	,	kIx,
František	František	k1gMnSc1
Kytka	kytka	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1881	#num#	k4
<g/>
,	,	kIx,
dle	dle	k7c2
J.	J.	kA
Vernea	Vernea	k1gMnSc1
vzdělal	vzdělat	k5eAaPmAgMnS
J.	J.	kA
Marek	Marek	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Cesta	cesta	k1gFnSc1
do	do	k7c2
středu	střed	k1gInSc2
Země	zem	k1gFnSc2
<g/>
,	,	kIx,
Alois	Alois	k1gMnSc1
R.	R.	kA
Lauermann	Lauermann	k1gMnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1882	#num#	k4
<g/>
,	,	kIx,
přeložil	přeložit	k5eAaPmAgMnS
J.K.J.	J.K.J.	k1gMnSc1
Dostupné	dostupný	k2eAgMnPc4d1
online	onlinout	k5eAaPmIp3nS
<g/>
.	.	kIx.
</s>
<s>
Do	do	k7c2
středu	střed	k1gInSc2
Země	zem	k1gFnSc2
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
R.	R.	kA
Vilímek	Vilímek	k1gMnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1896	#num#	k4
<g/>
,	,	kIx,
přeložil	přeložit	k5eAaPmAgMnS
J.	J.	kA
Wagner	Wagner	k1gMnSc1
<g/>
,	,	kIx,
znovu	znovu	k6eAd1
1900	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Cesta	cesta	k1gFnSc1
do	do	k7c2
středu	střed	k1gInSc2
Země	zem	k1gFnSc2
<g/>
,	,	kIx,
Bedřich	Bedřich	k1gMnSc1
Kočí	Kočí	k1gMnSc1
a	a	k8xC
Eduard	Eduard	k1gMnSc1
Beaufort	Beaufort	k1gInSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1909	#num#	k4
<g/>
,	,	kIx,
přeložil	přeložit	k5eAaPmAgMnS
J.	J.	kA
Berger	Berger	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Do	do	k7c2
středu	střed	k1gInSc2
Země	zem	k1gFnSc2
<g/>
,	,	kIx,
Josef	Josef	k1gMnSc1
R.	R.	kA
Vilímek	Vilímek	k1gMnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1929	#num#	k4
<g/>
,	,	kIx,
přeložil	přeložit	k5eAaPmAgMnS
J.	J.	kA
Wagner	Wagner	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Cesta	cesta	k1gFnSc1
do	do	k7c2
středu	střed	k1gInSc2
Země	zem	k1gFnSc2
<g/>
,	,	kIx,
SNDK	SNDK	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1965	#num#	k4
<g/>
,	,	kIx,
přeložil	přeložit	k5eAaPmAgMnS
Václav	Václav	k1gMnSc1
Netušil	Netušil	k1gMnSc1
<g/>
,	,	kIx,
znovu	znovu	k6eAd1
Albatros	albatros	k1gMnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
1992	#num#	k4
<g/>
,	,	kIx,
</s>
<s>
Do	do	k7c2
středu	střed	k1gInSc2
Země	zem	k1gFnSc2
<g/>
,	,	kIx,
Návrat	návrat	k1gInSc1
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
1997	#num#	k4
<g/>
,	,	kIx,
přeložil	přeložit	k5eAaPmAgMnS
J.	J.	kA
Berger	Berger	k1gMnSc1
<g/>
,	,	kIx,
znovu	znovu	k6eAd1
2008	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Cesta	cesta	k1gFnSc1
do	do	k7c2
středu	střed	k1gInSc2
Země	zem	k1gFnSc2
<g/>
,	,	kIx,
Computer	computer	k1gInSc1
Press	Press	k1gInSc1
<g/>
,	,	kIx,
Brno	Brno	k1gNnSc1
2007	#num#	k4
<g/>
,	,	kIx,
úprava	úprava	k1gFnSc1
původního	původní	k2eAgInSc2d1
textu	text	k1gInSc2
a	a	k8xC
překlad	překlad	k1gInSc1
Tomáš	Tomáš	k1gMnSc1
Cidlina	Cidlina	k1gFnSc1
<g/>
,	,	kIx,
</s>
<s>
Cesta	cesta	k1gFnSc1
do	do	k7c2
středu	střed	k1gInSc2
Země	zem	k1gFnSc2
<g/>
,	,	kIx,
XYZ	XYZ	kA
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2008	#num#	k4
<g/>
,	,	kIx,
přeložil	přeložit	k5eAaPmAgMnS
Jiří	Jiří	k1gMnSc1
Žák	Žák	k1gMnSc1
<g/>
,	,	kIx,
znovu	znovu	k6eAd1
2017	#num#	k4
<g/>
.	.	kIx.
</s>
<s>
Cesta	cesta	k1gFnSc1
do	do	k7c2
středu	střed	k1gInSc2
Země	zem	k1gFnSc2
<g/>
,	,	kIx,
Omega	omega	k1gFnSc1
<g/>
,	,	kIx,
Praha	Praha	k1gFnSc1
2015	#num#	k4
<g/>
,	,	kIx,
přeložil	přeložit	k5eAaPmAgMnS
J.	J.	kA
Berger	Berger	k1gMnSc1
<g/>
,	,	kIx,
</s>
<s>
Cesta	cesta	k1gFnSc1
do	do	k7c2
středu	střed	k1gInSc2
Země	zem	k1gFnSc2
<g/>
,	,	kIx,
Nakladatelství	nakladatelství	k1gNnSc1
Josef	Josef	k1gMnSc1
Vybíral	Vybíral	k1gMnSc1
<g/>
,	,	kIx,
Žalkovice	Žalkovice	k1gFnSc1
2015	#num#	k4
<g/>
,	,	kIx,
přeložil	přeložit	k5eAaPmAgMnS
J.	J.	kA
Wagner	Wagner	k1gMnSc1
</s>
<s>
Externí	externí	k2eAgInPc1d1
odkazy	odkaz	k1gInPc1
</s>
<s>
Obrázky	obrázek	k1gInPc1
<g/>
,	,	kIx,
zvuky	zvuk	k1gInPc1
či	či	k8xC
videa	video	k1gNnSc2
k	k	k7c3
tématu	téma	k1gNnSc3
Cesta	cesta	k1gFnSc1
do	do	k7c2
středu	střed	k1gInSc2
Země	zem	k1gFnSc2
na	na	k7c4
Wikimedia	Wikimedium	k1gNnPc4
Commons	Commonsa	k1gFnPc2
</s>
<s>
Dílo	dílo	k1gNnSc1
Dílo	dílo	k1gNnSc1
<g/>
:	:	kIx,
<g/>
Cesta	cesta	k1gFnSc1
do	do	k7c2
středu	střed	k1gInSc2
Země	zem	k1gFnSc2
ve	v	k7c6
Wikicitátech	Wikicitát	k1gInPc6
</s>
<s>
Dílo	dílo	k1gNnSc1
Cesta	cesta	k1gFnSc1
do	do	k7c2
středu	střed	k1gInSc2
Země	zem	k1gFnSc2
ve	v	k7c6
Wikizdrojích	Wikizdroj	k1gInPc6
</s>
<s>
Digitalizovaná	digitalizovaný	k2eAgNnPc1d1
vydání	vydání	k1gNnPc1
díla	dílo	k1gNnSc2
Cesta	cesta	k1gFnSc1
do	do	k7c2
středu	střed	k1gInSc2
Země	zem	k1gFnSc2
v	v	k7c6
digitální	digitální	k2eAgFnSc6d1
knihovně	knihovna	k1gFnSc6
Kramerius	Kramerius	k1gMnSc1
NK	NK	kA
ČR	ČR	kA
<g/>
.	.	kIx.
</s>
<s>
http://jv.gilead.org.il/pg/vcen/	http://jv.gilead.org.il/pg/vcen/	k?
Archivováno	archivován	k2eAgNnSc1d1
28	#num#	k4
<g/>
.	.	kIx.
2	#num#	k4
<g/>
.	.	kIx.
2006	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
–	–	k?
francouzský	francouzský	k2eAgInSc4d1
text	text	k1gInSc4
románu	román	k1gInSc2
</s>
<s>
http://jv.gilead.org.il/wolcott/CE-allc/	http://jv.gilead.org.il/wolcott/CE-allc/	k?
Archivováno	archivován	k2eAgNnSc1d1
22	#num#	k4
<g/>
.	.	kIx.
1	#num#	k4
<g/>
.	.	kIx.
2013	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
–	–	k?
anglický	anglický	k2eAgInSc1d1
překlad	překlad	k1gInSc1
</s>
<s>
http://jv.gilead.org.il/zydorczak/terre00.htm	http://jv.gilead.org.il/zydorczak/terre00.htm	k6eAd1
Archivováno	archivovat	k5eAaBmNgNnS
20	#num#	k4
<g/>
.	.	kIx.
11	#num#	k4
<g/>
.	.	kIx.
2006	#num#	k4
na	na	k7c4
Wayback	Wayback	k1gInSc4
Machine	Machin	k1gInSc5
–	–	k?
francouzský	francouzský	k2eAgInSc4d1
text	text	k1gInSc4
románu	román	k1gInSc2
</s>
<s>
Cesta	cesta	k1gFnSc1
do	do	k7c2
středu	střed	k1gInSc2
Země	zem	k1gFnSc2
–	–	k?
audiokniha	audiokniha	k1gMnSc1
na	na	k7c6
YouTube	YouTub	k1gInSc5
kanálu	kanál	k1gInSc2
Divadla	divadlo	k1gNnSc2
pohádek	pohádka	k1gFnPc2
</s>
<s>
Cesta	cesta	k1gFnSc1
do	do	k7c2
středu	střed	k1gInSc2
Země	zem	k1gFnSc2
v	v	k7c6
Československé	československý	k2eAgFnSc6d1
bibliografické	bibliografický	k2eAgFnSc6d1
databázi	databáze	k1gFnSc6
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k1gInSc1
.	.	kIx.
<g/>
navbox-title	navbox-title	k1gFnSc1
.	.	kIx.
<g/>
mw-collapsible-toggle	mw-collapsible-toggle	k1gFnSc1
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
normal	normal	k1gInSc1
<g/>
}	}	kIx)
Dílo	dílo	k1gNnSc1
Julese	Julese	k1gFnSc2
Verna	Verno	k1gNnSc2
</s>
<s>
Podivuhodné	podivuhodný	k2eAgFnPc1d1
cesty	cesta	k1gFnPc1
</s>
<s>
Pět	pět	k4xCc1
neděl	neděle	k1gFnPc2
v	v	k7c6
baloně	balon	k1gInSc6
(	(	kIx(
<g/>
1863	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Dobrodružství	dobrodružství	k1gNnSc2
kapitána	kapitán	k1gMnSc2
Hatterase	Hatterasa	k1gFnSc6
(	(	kIx(
<g/>
1866	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Cesta	cesta	k1gFnSc1
do	do	k7c2
středu	střed	k1gInSc2
Země	zem	k1gFnSc2
(	(	kIx(
<g/>
1864	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ze	z	k7c2
Země	zem	k1gFnSc2
na	na	k7c4
Měsíc	měsíc	k1gInSc4
(	(	kIx(
<g/>
1865	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Děti	dítě	k1gFnPc1
kapitána	kapitán	k1gMnSc2
Granta	Grant	k1gMnSc2
(	(	kIx(
<g/>
1867	#num#	k4
<g/>
–	–	k?
<g/>
68	#num#	k4
<g/>
)	)	kIx)
<g />
.	.	kIx.
</s>
<s hack="1">
•	•	k?
Dvacet	dvacet	k4xCc4
tisíc	tisíc	k4xCgInPc2
mil	míle	k1gFnPc2
pod	pod	k7c7
mořem	moře	k1gNnSc7
(	(	kIx(
<g/>
1869	#num#	k4
<g/>
–	–	k?
<g/>
70	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Okolo	okolo	k7c2
Měsíce	měsíc	k1gInSc2
(	(	kIx(
<g/>
1870	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Plující	plující	k2eAgNnSc4d1
město	město	k1gNnSc4
(	(	kIx(
<g/>
1871	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Dobrodružství	dobrodružství	k1gNnSc4
tří	tři	k4xCgInPc2
Rusů	Rus	k1gMnPc2
a	a	k8xC
tří	tři	k4xCgMnPc2
Angličanů	Angličan	k1gMnPc2
(	(	kIx(
<g/>
1872	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Země	zem	k1gFnSc2
kožešin	kožešina	k1gFnPc2
(	(	kIx(
<g/>
1873	#num#	k4
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
)	)	kIx)
•	•	k?
Cesta	cesta	k1gFnSc1
kolem	kolem	k7c2
světa	svět	k1gInSc2
za	za	k7c4
osmdesát	osmdesát	k4xCc4
dní	den	k1gInPc2
(	(	kIx(
<g/>
1873	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Tajuplný	tajuplný	k2eAgInSc4d1
ostrov	ostrov	k1gInSc4
(	(	kIx(
<g/>
1874	#num#	k4
<g/>
–	–	k?
<g/>
75	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Chancellor	Chancellor	k1gInSc1
(	(	kIx(
<g/>
1875	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Carův	carův	k2eAgMnSc1d1
kurýr	kurýr	k1gMnSc1
(	(	kIx(
<g/>
1876	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Na	na	k7c6
kometě	kometa	k1gFnSc6
(	(	kIx(
<g/>
1877	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Černé	Černá	k1gFnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
indie	indie	k1gFnSc1
(	(	kIx(
<g/>
1877	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Patnáctiletý	patnáctiletý	k2eAgMnSc1d1
kapitán	kapitán	k1gMnSc1
(	(	kIx(
<g/>
1878	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ocelové	ocelový	k2eAgNnSc4d1
město	město	k1gNnSc4
(	(	kIx(
<g/>
1879	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Číňanovy	Číňanův	k2eAgFnSc2d1
trampoty	trampota	k1gFnSc2
v	v	k7c6
Číně	Čína	k1gFnSc6
(	(	kIx(
<g/>
1879	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Zemí	zem	k1gFnSc7
šelem	šelma	k1gFnPc2
(	(	kIx(
<g/>
1880	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Tajemství	tajemství	k1gNnSc1
pralesa	prales	k1gInSc2
(	(	kIx(
<g/>
1881	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Škola	škola	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
robinsonů	robinson	k1gMnPc2
(	(	kIx(
<g/>
1882	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Zelený	zelený	k2eAgInSc1d1
paprsek	paprsek	k1gInSc1
(	(	kIx(
<g/>
1882	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Tvrdohlavý	tvrdohlavý	k2eAgInSc4d1
Turek	turek	k1gInSc4
(	(	kIx(
<g/>
1883	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Hvězda	hvězda	k1gFnSc1
jihu	jih	k1gInSc2
(	(	kIx(
<g/>
1884	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Archipel	archipel	k1gInSc1
v	v	k7c6
plamenech	plamen	k1gInPc6
(	(	kIx(
<g/>
1884	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Matyáš	Matyáš	k1gMnSc1
Sandorf	Sandorf	k1gMnSc1
(	(	kIx(
<g/>
1885	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Los	los	k1gMnSc1
číslo	číslo	k1gNnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
9672	#num#	k4
(	(	kIx(
<g/>
1886	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Robur	Robur	k1gMnSc1
Dobyvatel	dobyvatel	k1gMnSc1
(	(	kIx(
<g/>
1886	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Sever	sever	k1gInSc4
proti	proti	k7c3
Jihu	jih	k1gInSc3
(	(	kIx(
<g/>
1887	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Cesta	cesta	k1gFnSc1
do	do	k7c2
Francie	Francie	k1gFnSc2
(	(	kIx(
<g/>
1887	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Dva	dva	k4xCgInPc4
roky	rok	k1gInPc4
prázdnin	prázdniny	k1gFnPc2
(	(	kIx(
<g/>
1888	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bezejmenná	bezejmenný	k2eAgFnSc1d1
rodina	rodina	k1gFnSc1
(	(	kIx(
<g/>
1889	#num#	k4
<g/>
)	)	kIx)
•	•	k?
<g />
.	.	kIx.
</s>
<s hack="1">
Zmatek	zmatek	k1gInSc1
nad	nad	k7c4
zmatek	zmatek	k1gInSc4
(	(	kIx(
<g/>
1889	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Oceánem	oceán	k1gInSc7
na	na	k7c6
kře	kra	k1gFnSc6
ledové	ledový	k2eAgNnSc1d1
(	(	kIx(
<g/>
1890	#num#	k4
<g/>
)	)	kIx)
•	•	k?
V	v	k7c6
pustinách	pustina	k1gFnPc6
australských	australský	k2eAgMnPc2d1
(	(	kIx(
<g/>
1891	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Tajemný	tajemný	k2eAgInSc4d1
hrad	hrad	k1gInSc4
v	v	k7c6
Karpatech	Karpaty	k1gInPc6
(	(	kIx(
<g/>
1892	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Claudius	Claudius	k1gMnSc1
Bombarnak	Bombarnak	k1gMnSc1
(	(	kIx(
<g/>
1892	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Malý	Malý	k1gMnSc1
Dobráček	dobráček	k1gMnSc1
(	(	kIx(
<g/>
<g />
.	.	kIx.
</s>
<s hack="1">
1893	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Neobyčejná	obyčejný	k2eNgNnPc1d1
dobrodružství	dobrodružství	k1gNnPc1
mistra	mistr	k1gMnSc2
Antifera	Antifer	k1gMnSc2
(	(	kIx(
<g/>
1894	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Plovoucí	plovoucí	k2eAgInSc4d1
ostrov	ostrov	k1gInSc4
(	(	kIx(
<g/>
1895	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Milionář	milionář	k1gMnSc1
na	na	k7c6
cestách	cesta	k1gFnPc6
(	(	kIx(
<g/>
1896	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ledová	ledový	k2eAgFnSc1d1
sfinga	sfinga	k1gFnSc1
(	(	kIx(
<g/>
1897	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Na	na	k7c6
vlnách	vlna	k1gFnPc6
Orinoka	Orinoko	k1gNnSc2
(	(	kIx(
<g/>
1898	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Závěť	závěť	k1gFnSc1
výstředníka	výstředník	k1gMnSc2
<g />
.	.	kIx.
</s>
<s hack="1">
(	(	kIx(
<g/>
1899	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Druhá	druhý	k4xOgFnSc1
vlast	vlast	k1gFnSc1
(	(	kIx(
<g/>
1900	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ves	ves	k1gFnSc1
ve	v	k7c6
vzduchu	vzduch	k1gInSc6
(	(	kIx(
<g/>
1901	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Záhadné	záhadný	k2eAgNnSc4d1
dobrodružství	dobrodružství	k1gNnSc4
velrybářské	velrybářský	k2eAgFnSc2d1
lodi	loď	k1gFnSc2
(	(	kIx(
<g/>
1901	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Bratři	bratr	k1gMnPc1
Kipové	Kipová	k1gFnSc2
(	(	kIx(
<g/>
1902	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Cestovní	cestovní	k2eAgNnPc4d1
stipendia	stipendium	k1gNnPc4
(	(	kIx(
<g/>
1903	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Drama	drama	k1gFnSc1
<g />
.	.	kIx.
</s>
<s hack="1">
v	v	k7c6
Livonsku	Livonsko	k1gNnSc6
(	(	kIx(
<g/>
1904	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pán	pán	k1gMnSc1
světa	svět	k1gInSc2
(	(	kIx(
<g/>
1904	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Zatopená	zatopený	k2eAgFnSc1d1
Sahara	Sahara	k1gFnSc1
(	(	kIx(
<g/>
1905	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Maják	maják	k1gInSc1
na	na	k7c6
konci	konec	k1gInSc6
světa	svět	k1gInSc2
(	(	kIx(
<g/>
1905	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Zlatá	zlatý	k2eAgFnSc1d1
sopka	sopka	k1gFnSc1
(	(	kIx(
<g/>
1906	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Trampoty	trampota	k1gFnPc1
páně	páně	k2eAgInPc4d1
Thompsonovy	Thompsonův	k2eAgInPc4d1
(	(	kIx(
<g/>
1907	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Honba	honba	k1gFnSc1
za	za	k7c7
meteorem	meteor	k1gInSc7
(	(	kIx(
<g/>
1908	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Lodivod	lodivod	k1gMnSc1
dunajský	dunajský	k2eAgMnSc1d1
(	(	kIx(
<g/>
1908	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Trosečníci	trosečník	k1gMnPc1
z	z	k7c2
lodi	loď	k1gFnSc2
Jonathan	Jonathan	k1gMnSc1
(	(	kIx(
<g/>
1909	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Tajemství	tajemství	k1gNnSc1
Viléma	Vilém	k1gMnSc2
Storitze	Storitze	k1gFnSc2
(	(	kIx(
<g/>
1910	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Podivuhodná	podivuhodný	k2eAgNnPc1d1
dobrodružství	dobrodružství	k1gNnPc1
výpravy	výprava	k1gFnSc2
Barsacovy	Barsacův	k2eAgFnSc2d1
(	(	kIx(
<g/>
1919	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Ostatní	ostatní	k2eAgNnSc1d1
dílo	dílo	k1gNnSc1
Romány	román	k1gInPc7
</s>
<s>
Kněz	kněz	k1gMnSc1
v	v	k7c6
roce	rok	k1gInSc6
1839	#num#	k4
(	(	kIx(
<g/>
psáno	psát	k5eAaImNgNnS
1846	#num#	k4
<g/>
–	–	k?
<g/>
1847	#num#	k4
<g/>
,	,	kIx,
vydáno	vydat	k5eAaPmNgNnS
1992	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Cesta	cesta	k1gFnSc1
pozpátku	pozpátku	k6eAd1
do	do	k7c2
Anglie	Anglie	k1gFnSc2
a	a	k8xC
Skotska	Skotsko	k1gNnSc2
(	(	kIx(
<g/>
psáno	psát	k5eAaImNgNnS
1859	#num#	k4
<g/>
–	–	k?
<g/>
1860	#num#	k4
<g/>
,	,	kIx,
vydáno	vydat	k5eAaPmNgNnS
1989	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Paříž	Paříž	k1gFnSc1
ve	v	k7c6
dvacátém	dvacátý	k4xOgNnSc6
století	století	k1gNnSc6
(	(	kIx(
<g/>
psáno	psát	k5eAaImNgNnS
1863	#num#	k4
<g/>
,	,	kIx,
vydáno	vydat	k5eAaPmNgNnS
1994	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Hrabě	Hrabě	k1gMnSc1
de	de	k?
Chanteleine	Chantelein	k1gInSc5
(	(	kIx(
<g/>
1864	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Trosečník	trosečník	k1gMnSc1
z	z	k7c2
Cynthie	Cynthie	k1gFnSc2
(	(	kIx(
<g/>
1885	#num#	k4
<g/>
)	)	kIx)
Sbírky	sbírka	k1gFnSc2
</s>
<s>
Doktor	doktor	k1gMnSc1
Ox	Ox	k1gMnSc1
(	(	kIx(
<g/>
1874	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Včera	včera	k6eAd1
a	a	k8xC
zítra	zítra	k6eAd1
(	(	kIx(
<g/>
1910	#num#	k4
<g/>
)	)	kIx)
Povídky	povídka	k1gFnSc2
a	a	k8xC
novely	novela	k1gFnSc2
</s>
<s>
Drama	drama	k1gNnSc1
v	v	k7c6
Mexiku	Mexiko	k1gNnSc6
(	(	kIx(
<g/>
1851	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Drama	drama	k1gNnSc1
ve	v	k7c6
vzduchu	vzduch	k1gInSc6
(	(	kIx(
<g/>
1851	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Martin	Martin	k1gMnSc1
Paz	Paz	k1gMnSc1
(	(	kIx(
<g/>
1852	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Mistr	mistr	k1gMnSc1
Zachariáš	Zachariáš	k1gMnSc1
(	(	kIx(
<g/>
1854	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Zajatci	zajatec	k1gMnPc1
polárního	polární	k2eAgNnSc2d1
moře	moře	k1gNnSc2
(	(	kIx(
<g/>
1855	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Prorazili	prorazit	k5eAaPmAgMnP
blokádu	blokáda	k1gFnSc4
<g/>
(	(	kIx(
<g/>
1865	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Doktor	doktor	k1gMnSc1
Ox	Ox	k1gMnSc1
(	(	kIx(
<g/>
1872	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Vzbouřenci	vzbouřenec	k1gMnPc1
na	na	k7c6
lodi	loď	k1gFnSc6
Bounty	Bounta	k1gFnSc2
(	(	kIx(
<g/>
1879	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Deset	deset	k4xCc1
hodin	hodina	k1gFnPc2
na	na	k7c6
lovu	lov	k1gInSc6
(	(	kIx(
<g/>
1881	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Fffff	Fffff	k1gInSc1
<g/>
...	...	k?
<g/>
plesk	plesk	k1gInSc1
<g/>
!	!	kIx.
</s>
<s desamb="1">
(	(	kIx(
<g/>
1884	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Gil	Gil	k1gMnSc1
Braltar	Braltar	k1gMnSc1
(	(	kIx(
<g/>
1887	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ve	v	k7c6
XXIX	XXIX	kA
<g/>
.	.	kIx.
století	století	k1gNnPc2
(	(	kIx(
<g/>
1889	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Dobrodružství	dobrodružství	k1gNnSc4
rodiny	rodina	k1gFnSc2
Krysáků	Krysák	k1gMnPc2
(	(	kIx(
<g/>
1891	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Pan	Pan	k1gMnSc1
Dis	dis	k1gNnSc2
a	a	k8xC
slečna	slečna	k1gFnSc1
Es	es	k1gNnSc2
(	(	kIx(
<g/>
1893	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Osud	osud	k1gInSc1
Jeana	Jean	k1gMnSc2
Morénase	Morénasa	k1gFnSc6
(	(	kIx(
<g/>
1910	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Humbuk	humbuk	k1gInSc1
(	(	kIx(
<g/>
1910	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Věčný	věčný	k2eAgMnSc1d1
Adam	Adam	k1gMnSc1
(	(	kIx(
<g/>
1910	#num#	k4
<g/>
)	)	kIx)
Ostatní	ostatní	k2eAgInPc1d1
</s>
<s>
Edgard	Edgard	k1gMnSc1
Poe	Poe	k1gMnSc1
et	et	k?
ses	ses	k?
œ	œ	k1gInSc1
(	(	kIx(
<g/>
1864	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Ilustrovaný	ilustrovaný	k2eAgInSc4d1
zeměpis	zeměpis	k1gInSc4
Francie	Francie	k1gFnSc2
(	(	kIx(
<g/>
1866	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Historie	historie	k1gFnSc1
velkých	velký	k2eAgInPc2d1
objevů	objev	k1gInPc2
(	(	kIx(
<g/>
1870	#num#	k4
<g/>
–	–	k?
<g/>
1880	#num#	k4
<g/>
)	)	kIx)
•	•	k?
Cesty	cesta	k1gFnSc2
na	na	k7c6
divadle	divadlo	k1gNnSc6
(	(	kIx(
<g/>
1881	#num#	k4
<g/>
)	)	kIx)
</s>
<s>
Autoritní	Autoritní	k2eAgNnPc1d1
data	datum	k1gNnPc1
<g/>
:	:	kIx,
GND	GND	kA
<g/>
:	:	kIx,
4835489-2	4835489-2	k4
|	|	kIx~
VIAF	VIAF	kA
<g/>
:	:	kIx,
181861005	#num#	k4
</s>
<s>
<g/>
mw-parser-output	mw-parser-output	k2eAgInSc1d1
div	div	k1gInSc1
<g/>
/	/	kIx~
<g/>
**	**	k?
<g/>
/	/	kIx~
<g/>
#	#	kIx~
<g/>
portallinks	portallinks	k6eAd1
a	a	k8xC
<g/>
{	{	kIx(
<g/>
font-weight	font-weight	k1gInSc1
<g/>
:	:	kIx,
<g/>
bold	bold	k1gInSc1
<g/>
}	}	kIx)
<g/>
Portály	portál	k1gInPc1
<g/>
:	:	kIx,
Literatura	literatura	k1gFnSc1
</s>
